import fs from 'fs';
import path from 'path';
import { analyzeText } from '../helpers/analyzer'
import * as process from 'process'
import { createIndex, indexDocument } from 'picosearch'
import glob from 'glob'
import crypto from 'crypto'
import { WebVTTParser } from 'webvtt-parser';
const parser = new WebVTTParser();

const main = async () => {
  if (!process.env.QUESTION_DATA_REPO || !fs.existsSync(process.env.QUESTION_DATA_REPO)) {
    console.error('Env variable QUESTION_DATA_REPO must be set to an existing folder!')
    process.exit(1)
  }

  const schema = {
    videoId: 'keyword',
    type: 'keyword',
    ts: 'number',
    text: 'text',
    date: 'date',
    tsEnd: 'number',
    title: 'text'
  }

  const index = createIndex(schema as any)

  fs.mkdirSync(path.join(__dirname, '/../../public/data/source/transcript/'), { recursive: true });
  fs.mkdirSync(path.join(__dirname, '/../../public/data/source/qa/'), { recursive: true });

  console.time('indexing transcripts')

  const transcripts = glob.sync(path.join(__dirname, '../../../data/transcripts-whisper-tiny-en', '*.vtt'))
  const metadataFiles = glob.sync(path.join(__dirname, '../../../data/video-metadata', '*.json'))
  const s3Metadata = JSON.parse(fs.readFileSync(path.join(__dirname, '../../../data/s3_metadata.json'), { encoding: 'utf-8' }))
  const s3Keys = Object.keys(s3Metadata)

  transcripts.forEach(fn => {
    try {
      const id = fn.split('/').slice(-1)[0].replace('.vtt', '')
      const parsed = parser.parse(fs.readFileSync(fn, { encoding: 'utf-8' }))
      const doc = {
        _id: 'tr-' + id,
        type: 'transcript',
        videoId: id,
        title: id,
        text: parsed.cues.map(x => x.text),
        ts: parsed.cues.map(x => x.startTime)
      }
      const metaFile = metadataFiles.find(x => x.includes(`[${id.replace('.mp3', '')}]`))
      if (metaFile) {
        const metadata = JSON.parse(fs.readFileSync(metaFile, { encoding: 'utf-8' }))
        doc.title = metadata['title']
      }

      const s3Meta = s3Keys.find(x => x.endsWith(id.replace('.mp3', '')))
      if (s3Meta) {
        doc.videoId = s3Metadata[s3Meta]['audio_url'].replace('podcast-mirror/', '') // TODO: cleaner generation of links 
      }

      indexDocument(index, doc, analyzeText)
      fs.writeFileSync(
        path.join(__dirname, `/../../public/data/source/transcript/${doc._id}.json`),
        JSON.stringify(doc)
      )
    } catch (err) {
      console.error(err)
      console.error('Failed to processs ', fn)
    }
  })
  console.timeEnd('indexing transcripts')

  console.time('indexing questions')
  const questions = glob.sync(path.join(process.env.QUESTION_DATA_REPO, 'data/qa-on-screen/*.json'))
  questions.forEach(fn => {
    const parsed = JSON.parse(
      fs.readFileSync(fn, { encoding: 'utf-8' })
    )

    parsed.chapters.forEach((c, i) => {
      const doc = {
        _id: `qa-${parsed.id}-${i}`,
        type: 'qa',
        videoId: parsed.id,
        text: c[1],
        ts: c[0],
      }
      indexDocument(index, doc, analyzeText)
      fs.writeFileSync(
        path.join(__dirname, `/../../public/data/source/qa/${doc._id}.json`),
        JSON.stringify(doc)
      )
    })
  })
  console.timeEnd('indexing questions')

  const indexJson = JSON.stringify(index)
  fs.writeFileSync(
    path.join(__dirname, '/../../public/data/search_index.json'),
    indexJson 
  )

  fs.writeFileSync(
    path.join(__dirname, '/../../public/data/index.json'),
    JSON.stringify({
      'dataPath': '/data/search_index.json',
      'dataSize': Buffer.byteLength(indexJson, 'utf-8'),
      'dataMD5Sum': crypto.createHash('md5').update(indexJson).digest("hex"),
    })
  )
}

main()
  .then(() => process.exit())
  .catch(err => {
    console.error(err)
    process.exit(1)
  })
