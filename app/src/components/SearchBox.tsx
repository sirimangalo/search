import React, { useEffect, useState, useCallback } from 'react';
import { getUpdateStrategy } from '../helpers/storage';
import Icon from './Icon'

interface SearchBoxProps {
  initialSearch: string;
  resultsSearch: string;
  startDate: string;
  endDate: string;
  loading: boolean;
  tags: string[];
  sortAscending: boolean;
  onTagsChange: (types: string[]) => any;
  onStartDateChange: (date: string) => any;
  onEndDateChange: (date: string) => any;
  onSubmit: (search: string) => any;
  onShowSettings: () => any;
  onShowUpdates: () => any;
  onSortOrderChange: (asc: boolean) => any;
}

function SearchBox ({
  initialSearch,
  resultsSearch,
  startDate,
  endDate,
  loading,
  sortAscending,
  tags,
  onTagsChange,
  onStartDateChange,
  onEndDateChange,
  onSubmit,
  onShowSettings,
  onShowUpdates,
  onSortOrderChange
}: SearchBoxProps) {
  const [updateAvailable, setUpdateAvailable] = useState(false)
  const [search, setSearch] = useState('')
  const [showFilters, setShowFilters] = useState(false)

  useEffect(() => {
    setSearch(initialSearch)
    const main = async () => {
      setUpdateAvailable(
        (await getUpdateStrategy()).queueSize > 0
      )
    }

    main()
  }, [])

  const handleTagClick = useCallback((tag: string) => () => {
    if (tags.includes(tag)) {
      onTagsChange(tags.filter(t => t !== tag))
    } else {
      onTagsChange([...tags, tag])
    }
  }, [tags, onTagsChange])

  return (
    <div className='block'>
      <form onSubmit={(e) => {
        e.preventDefault()
        onSubmit(search)
      }}>
        <div className="field has-addons">
          <div className="control" style={{ flex: 1 }}>
            <input
              className={`input`}
              type="text"
              placeholder="Please enter your search here"
              onChange={(e) => setSearch(e.target.value)}
              readOnly={loading}
              value={search}
            />
          </div>
          <div className="control">
            {!(search && resultsSearch === search)
              ? <button disabled={!search} className={`button is-primary ${loading ? 'is-loading' : ''}`} type='submit'>
                Search
              </button>
              : <button className={`button`} type='button' onClick={() => {
                setSearch('')
                onSubmit('')
              }}>
                <Icon icon='rubbish' color='red' />
              </button>
            }
          </div>
        </div>
        <div className='is-flex is-align-items-center mt-2'>
          {/* <a onClick={() => setShowFilters(!showFilters)}>{showFilters ? 'hide' : 'show'} filters</a> */}

          <span style={{ flex: '1 1 auto' }}></span>
          { updateAvailable &&
          <button
            className='button is-info mr-2'
            title='New data available'
            onClick={() => onShowUpdates()}
          >
            <Icon
              icon='download'
              color='#fff'
            />
          </button>
          }
          <button className='button' onClick={() => onShowSettings()}>
            <Icon
              icon='gear'
              style={{ cursor: 'pointer' }}
            />
          </button>
          </div>
          <div className="tags are-large">
            <span
              onClick={handleTagClick('qa')} 
              className={`tag is-clickable ${tags.includes('qa') ? 'is-primary' : ''}`}
            >
              Questions
            </span>
            <span 
              onClick={handleTagClick('transcript')} 
              className={`tag is-clickable ${tags.includes('transcript') ? 'is-primary' : ''}`}
            >
              Transcripts
            </span>
          </div>
        <div className='is-flex is-align-items-end mt-2'>
          { showFilters && <>
            <div>
              <label className="label">Date from</label>
              <div className="control">
                <input
                  type='date'
                  value={startDate}
                  onChange={e => onStartDateChange(e.target.value)}
                />
              </div>
            </div>
            <div className="ml-2">
              <label className="label">Date to</label>
              <div className="control">
                <input
                  type='date'
                  value={endDate}
                  onChange={e => onEndDateChange(e.target.value)}
                />
              </div>
            </div>
            <div className="ml-2">
              <button 
                disabled={!!resultsSearch}
                onClick={() => onSortOrderChange(!sortAscending)}
                className='button'
                title='Change default sort order'
              >
                {sortAscending && <Icon icon='up_arrow' />}
                {!sortAscending && <Icon icon='down_arrow' />}
              </button>
          </div>
          </>}
        </div>
      </form>
    </div>
  )
}

export default SearchBox
