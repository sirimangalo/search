import React, { useState, useMemo } from 'react';
import Icon from './Icon'
import { Settings } from '../interfaces'
import { PODCAST_MIRROR_URL } from '../constants';
import type { SearchResultsHit } from 'picosearch'

interface ResultCardProps {
  item: SearchResultsHit;
  settings: Settings
}

function QaResultCard ({ 
  item,
  settings
}: ResultCardProps) {
  const [playAudio, setPlayAudio] = useState(false)
  const [loadingAudio, setLoadingAudio] = useState(false)

  const ytLink = useMemo(() => item._source.videoId.replace('.mp3', '').length === 11 
    ? `https://youtu.be/${item._source.videoId.replace('.mp3', '')}${Array.isArray(item._source.ts) ? '' : `?t=${item._source.ts}`}`
    : `${PODCAST_MIRROR_URL}/${item._source.videoId}`,
    [item._source.videoId, item._source.ts]
  )

  const audioLink = useMemo(() => {
    let link = `${PODCAST_MIRROR_URL}/${item._source.videoId}.mp3`
    link += `#t=${item._source.ts}${settings.autoStopPlaying && item._source.tsEnd ? ',' + item._source.tsEnd : ''}`
    return link
  }, [item._source.ts, item._source.tsEnd])

  const score = useMemo(() => Math.round(item._score * 1000) / 1000, [item._score])

  return (
    <div className="card block result-card">
      <div className="card-content">
        <div className="content">
          <div className='result-text'>
            { settings.highlightWords
              ? <p dangerouslySetInnerHTML={{ __html: (item.highlight.text as string) }}></p>
              : <p>{item._source.text}</p> }
            
            <div className='is-flex is-align-items-center'>
              <button
                className={`button ${loadingAudio ? 'is-loading' : ''} ${playAudio && !loadingAudio ? 'is-danger' : ''}`}
                onClick={() => {
                  setLoadingAudio(!playAudio)
                  setPlayAudio(!playAudio)
                }}
              >
              {!loadingAudio &&
                <Icon
                  icon={playAudio ? 'square' : 'play'}
                  size='15px'
                  color={!playAudio ? '#000' : '#fff'}
                />}
              </button>
              { playAudio && 
                <audio
                  src={audioLink}
                  autoPlay={true}
                  controls={false}
                  onLoadedData={() => setLoadingAudio(false)}
                  onPause={() => {
                    setPlayAudio(false)
                    setLoadingAudio(false)
                  }}
                  onEnded={() => {
                    setPlayAudio(false)
                    setLoadingAudio(false)
                  }}
                  onError={() => {
                    alert('Could not load audio')
                    setPlayAudio(false)
                    setLoadingAudio(false)
                  }}
                />
              }
              <a className='button ml-2' href={ytLink} target='_blank' rel='noreferrer nofollow'>
                <Icon icon='external' size='15px' color='#000' />
              </a>
              <span className='is-flex-grow-1' />
              {settings.showScore && <span>{score}</span>}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default QaResultCard
