import React, { useEffect, useState } from 'react';
import localForage from 'localforage'
import { LOCAL_SETTINGS_KEY, SETTINGS_DEFAULT } from '../constants';
import { Settings } from '../interfaces'

interface SettingsModalProps {
  onClose: () => any;
}

function SettingsModal ({ onClose }: SettingsModalProps) {
  const [initialLoading, setInitialLoading] = useState(true)
  const [settings, setSettings] = useState<Settings>(null)

  useEffect(() => {
    const main = async () => {
      setSettings((await localForage.getItem(LOCAL_SETTINGS_KEY)) || SETTINGS_DEFAULT)
      setInitialLoading(false)
    }
    main()
  }, [])

  const handleHardReset = () => {
    if (window.confirm('This deletes all local data. Are you sure?')) {
      localForage.clear()
        .then(() => {
          window.location.reload()
        })
        .catch(err => {
          console.error(err)
        })
    }
  }

  const handleUpdateSettings= (key: string, value: any) => {
    const updated = { ...settings, [key]: value }
    localForage.setItem(LOCAL_SETTINGS_KEY, updated).then(() => {
      setSettings(updated)
    })
  }

  return (
    <div className="modal is-active">
      <div className="modal-background"></div>
      <div className="modal-content">
        <div className='card'>
          <div className='card-content'>
            {initialLoading
              ? <progress className="progress is-medium is-info mt-6" max="100"></progress>
              : <>
                <h1 className='title is-4'>Settings</h1>
                <h2 className='title is-5 mt-6 mb-2'>Search Options</h2>
                {/* <label className="checkbox"> */}
                {/*   <input type="checkbox" checked={settings.highlightWords} onChange={(e) => handleUpdateSettings('highlightWords', e.target.checked)} /> */}
                {/*   {' '}<b>Highlighting</b> */}
                {/* </label> */}
                {/* <p> */}
                {/*   Highlight words in results that also appear in the query. */}
                {/* </p> */}
                {/* <br/> */}
                <label className="checkbox">
                  <input type="checkbox" checked={settings.showScore} onChange={(e) => handleUpdateSettings('showScore', e.target.checked)} />
                  {' '}<b>Show Score</b>
                </label>
                <p>
                  The score is some number that represents the relevance of the result compared to the search query. 
                </p>
                <br/>
                <label className="checkbox">
                  <input type="checkbox" checked={settings.autoStopPlaying} onChange={(e) => handleUpdateSettings('autoStopPlaying', e.target.checked)} />
                  {' '}<b>Auto Stop Playing</b>
                </label>
                <p>
                  Automatically stop the audio after the whole answer has been played.
                </p>
                <h2 className='title is-5 mt-6 mb-2'>Storage Management</h2>
                <p>You can clear all local data by clicking the button below.</p>
                <div className='mt-2'>
                  <button className='button is-danger' onClick={handleHardReset}>Clear Data</button>
                </div>
                <hr />
                <div className='mt-2 is-flex is-justify-content-end'>
                  <button className='button' onClick={() => onClose()}>Close</button>
                </div>
              </>}
          </div>
        </div>
      </div>
      <button className="modal-close is-large" aria-label="close" onClick={() => onClose()}></button>
    </div>
  )
}

export default SettingsModal
