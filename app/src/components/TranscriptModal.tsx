import React, { useEffect, useState, useMemo } from 'react';
import { Settings, TranscriptItem } from '../interfaces'
import { createIndex, indexDocument, searchIndex } from 'picosearch'
import { analyzeText } from '../helpers/analyzer'

interface TranscriptModalProps {
  onClose: () => any;
  doc: any;
}

function TranscriptModal ({ onClose, doc }: TranscriptModalProps) {
  const [transcriptIndex, setTranscriptIndex] = useState(null)

  useEffect(() => {
    const index = createIndex({  text: 'text' })
    indexDocument(index, { _id: 'doc', text: doc.text }, analyzeText)      
    setTranscriptIndex(index)
  }, [])

  return (
    <div className="modal is-active">
      <div className="modal-background"></div>
      <div className="modal-card">
        <header className="modal-card-head">
          <p 
            className="modal-card-title"
            dangerouslySetInnerHTML={{ __html: doc.title }}
          />
          <button 
            className="delete" 
            onClick={() => onClose()} 
            aria-label="close"
          ></button>
        </header>
        <section className="modal-card-body">
          <div className="notification is-warning">
            <b>Warning:</b> Transcript are automatically generated and may sometimes differ from what was actually said.
          </div>
          <div className='card-transcript'>
          {doc && doc.text  && doc.text.map((text, i) => 
            <span 
              className='mb-4' 
              key={`transcript-text-${i}`}
              dangerouslySetInnerHTML={{ __html: text + ' ' }}
            />
          )}
          </div>
          <div className='mt-2 is-flex is-justify-content-end'>
          </div>
        </section>
        <footer className="modal-card-foot">
          <button className='button' onClick={() => onClose()}>Close</button>
        </footer>
      </div>
    </div>
  )
}

export default TranscriptModal
