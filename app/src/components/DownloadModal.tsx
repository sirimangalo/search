import React, { useEffect, useState } from 'react';
import { downloadAndApplyUpdates, getLocalIndex, getUpdateStrategy } from '../helpers/storage';
import { DownloadStrategy } from '../interfaces';
import Icon from './Icon'

const formatSize = (bytes: number) => {
  return `${Math.ceil(bytes / (1024*1024))} MB`
}

const nothingTodoScreen = ({onClose}) => <div>
  <h1 className='title is-4'>Nothing to do!</h1>
  <hr />
  <p>All files are up to date.</p>
  <div className='is-flex mt-5'>
    <button className='button m-auto' onClick={() => onClose()}>Close</button>
  </div>
</div>

const successScreen = ({onClose}) => <div>
  <h1 className='title is-4'>Download Done!</h1>
  <hr />
  <p>All files have been updated.</p>
  <div className='is-flex mt-5'>
    <button className='button m-auto' onClick={() => onClose(true)}>Close</button>
  </div>
</div>

const downloadScreen = ({
  strategy,
  downloading,
  handleClickDownload,
  isFreshInstance,
  acceptedGdpr,
  onAcceptChange,
  onClose
}) => <div>
  { isFreshInstance
      ? <>
        <h1 className='title is-4'>Hello!</h1>
        <hr />
        <p className='mb-4'>
          This app provides a search interface for questions and video transcripts.
          Before continuing, it is necessary to download the texts and data required to run the application.
          This initial download will consume up to <b>{formatSize(strategy.queueSize)}</b> of traffic and local storage.
          Thus, please first make sure that you're using a stable and sufficient internet connection.
          The download might take a bit of time.
        </p>
        <p>By using this website you need to accept our privacy policy:</p>
        <label className="checkbox mt-3">
          <input
            type="checkbox"
            checked={acceptedGdpr}
            onChange={(e) => onAcceptChange(e.target.checked)}
          />
            &nbsp;
            I accept the
            &nbsp;
          <a
            href='https://gitlab.com/sirimangalo/search/-/wikis/privacy-policy'
            target='_blank'
            rel='noreferrer nofollow'
          >
            privacy policy 
            &nbsp;
            <Icon icon='external' size='15px' color='#485fc7' />
          </a>.
        </label>
      </>
      : <>
        <h1 className='title is-4'>Update available</h1>
        <hr />
        <p className='mb-4'>
          An update is available and will require download up to <b>{formatSize(strategy.queueSize)}</b> of data.
          Click the button below to download and apply it.
        </p>
      </>
  }

  <div className='is-flex mt-5'>
    {downloading && <progress className="progress is-large is-info" max="100">60%</progress>}
    {!downloading &&
      <button
        className='button is-info m-auto'
        onClick={handleClickDownload}
        disabled={downloading || (isFreshInstance && !acceptedGdpr)}
      >
        Download
      </button>
    }
  </div>
  { !isFreshInstance && <>
    <hr />
    <div className='is-flex is-justify-content-end'>
      <button className='button' onClick={() => onClose()}>Close</button>
    </div>
  </> }
</div>

interface DownloadModalProps {
  onClose: (reloadData?: boolean) => void
}

function DownloadModal ({ onClose }: DownloadModalProps) {
  const [initialLoading, setInitialLoading] = useState(true)
  const [downloading, setDownloading] = useState(false)
  const [downloadingDone, setDownloadingDone] = useState(false)
  const [strategy, setStrategy] = useState<DownloadStrategy>(null)
  const [isFreshInstance, setIsFreshInstance] = useState(false)
  const [acceptedGdpr, setAcceptedGdpr] = useState(false)

  useEffect(() => {
    const main = async () => {
      setStrategy(await getUpdateStrategy())
      setIsFreshInstance(!(await getLocalIndex()))
      setInitialLoading(false)
    }
    main()
  }, [])

  const handleClickDownload = () => {
    setDownloading(true)
    setDownloadingDone(false)

    downloadAndApplyUpdates(strategy)
      .then(() => {
        setDownloading(false)
        setDownloadingDone(true)
      })
      .catch(err => {
        setDownloading(false)
        console.error(err)
      })
  }

  if (initialLoading) {
    return null
  }

  return (
    <div className="modal is-active">
      <div className="modal-background"></div>
      <div className="modal-content">
        <div className='card'>
          <div className='card-content'>
            {!strategy?.queueSize
              ? nothingTodoScreen({ onClose })
              : (downloadingDone
                ? successScreen({ onClose })
                : downloadScreen({
                  strategy,
                  downloading,
                  handleClickDownload,
                  isFreshInstance,
                  acceptedGdpr,
                  onAcceptChange: (x) => setAcceptedGdpr(x),
                  onClose
                }))
            }
          </div>
        </div>
      </div>
      {!isFreshInstance && <button className="modal-close is-large" aria-label="close" onClick={() => onClose()}></button>}
    </div>
  )
}

export default DownloadModal
