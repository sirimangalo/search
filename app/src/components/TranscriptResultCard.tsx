import React, { useEffect, useState, useMemo } from 'react';
import Icon from './Icon'
import { Settings, TranscriptItem } from '../interfaces'
import { PODCAST_MIRROR_URL } from '../constants';
import TranscriptModal from './TranscriptModal'
import type { SearchResultsHit } from 'picosearch'

interface ResultCardProps {
  item: SearchResultsHit;
  settings: Settings
}

function TranscriptResultCard ({ item, settings }: ResultCardProps) {
  const [showingTranscriptModal, setShowingTranscriptModal] = useState(false)

  const ytLink = useMemo(() => item._source.videoId.replace('.mp3', '').length === 11 
    ? `https://youtu.be/${item._source.videoId.replace('.mp3', '')}${Array.isArray(item._source.ts) ? '' : `?t=${item._source.ts}`}`
    : `${PODCAST_MIRROR_URL}/${item._source.videoId}`,
    [item._source.videoId]
  )

  const score = useMemo(() => Math.round(item._score * 1000) / 1000, [item._score])

  const title = useMemo(() => settings.highlightWords && item.highlight?.title
    ? <p className='result-title' dangerouslySetInnerHTML={{ __html: (item.highlight.title as string) }}></p>
    : <p className='result-title'>item._source.title</p>, 
    [settings.highlightWords, item._source.title, item.highlight.title]
  )

  const snippets = useMemo(() => item.snippets
    ? (item.snippets.text as string[][])
      .flatMap((s) => s)
      .filter(s => s)
      .map(s => `... ${s} ...`)
    : [],
    [item.snippets]
  )
  return (
    <div className="card block result-card">
      <div className="card-content">
        <div className="content">
          <div className='result-text'>
            {title}
            {snippets.map(text => 
            settings.highlightWords
              ? <p dangerouslySetInnerHTML={{ __html: (text as string) }}></p>
              : <p>{text}</p>)}
            <div className='is-flex is-align-items-center'>
            <a className='button ml-2' target='_blank' rel='noreferrer nofollow' onClick={() => setShowingTranscriptModal(true)}>
              <Icon icon='book' size='15px' color='#000' />
            </a>
              <a className='button ml-2' href={ytLink} target='_blank' rel='noreferrer nofollow'>
                <Icon icon='external' size='15px' color='#000' />
              </a>
              <span className='is-flex-grow-1'></span>
              {settings.showScore && <span className='result-score'>{score}</span>}
            </div>
          </div>
        </div>
      </div>
      {showingTranscriptModal && 
        <TranscriptModal 
          doc={item.highlight} 
          onClose={() => setShowingTranscriptModal(false)}
        />}
    </div>
  )
}

export default TranscriptResultCard
