import { Settings } from "./interfaces"

export const LOCAL_INDEX_KEY = 'local-index'
export const LOCAL_DATA_KEY = 'local-search-index'
export const LOCAL_SETTINGS_KEY = 'settings'
export const PODCAST_MIRROR_URL = 'https://sirimangalo-public.nyc3.cdn.digitaloceanspaces.com/podcast-mirror'
export const SETTINGS_DEFAULT: Settings = {
  showScore: false,
  autoStopPlaying: false,
  highlightWords: true
}
const DATA_VERSION_KEY = 'data-version'
const VERSION_URL = process.env.PUBLIC_URL + 'data/version.json'

export {
  DATA_VERSION_KEY,
  VERSION_URL
}
