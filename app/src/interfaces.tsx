export interface DataIndex {
  dataPath: string;
  dataSize: number;
  dataMD5Sum: string;
}

export interface DownloadItem {
  filePath: string;
  fileSize: number;
  localKey: string;
  type: 'file';
  md5Sum: string;
}

export interface DownloadStrategy {
  queue: DownloadItem[];
  queueSize: number;
  newIndex: DataIndex;
}

export interface QuestionItem {
  _id: string;
  videoId: string;
  type: 'qa';
  ts: number;
  text: string;
  date: string;
  dateUnix?: number;
  tsEnd?: number;
}

export interface TranscriptItem {
  _id: string;
  videoId?: string;
  type: 'transcript';
  ts: number[];
  text: string[];
  title: string;
  date: string;
}

export interface Settings {
  showScore: boolean;
  autoStopPlaying: boolean;
  highlightWords: boolean;
}
