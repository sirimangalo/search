import React, { useEffect, useState, useRef, useMemo, useCallback } from 'react';
import localForage from 'localforage'
import scrollIntoView from 'scroll-into-view-if-needed'
import './App.css';

import axios from 'axios'

import TranscriptResultCard from './components/TranscriptResultCard';
import QaResultCard from './components/QaResultCard';
import Pagination from './components/Pagination';
import SearchBox from './components/SearchBox';
import DownloadModal from './components/DownloadModal';
import SettingsModal from './components/SettingsModal';
import Icon from './components/Icon';
import type { Index } from 'picosearch';
import { searchIndex } from 'picosearch'
import { analyzeText } from './helpers/analyzer'

import {
  getLocalIndex,
  loadSearchIndexFromLocal
} from './helpers/storage';

import { Settings } from './interfaces'

import {
  LOCAL_SETTINGS_KEY,
  SETTINGS_DEFAULT
} from './constants';

function App() {
  const [index, setIndex] = useState<Index>(null)
  const [initialLoading, setInitialLoading] = useState(true)
  const [settings, setSettings] = useState<Settings>(null)
  const [downloadModalOpened, setDownloadModelOpened] = useState(false)
  const [settingsModalOpened, setSettingsModalOpened] = useState(false)
  const [search, setSearch] = useState('')
  const [loading, setLoading] = useState(false)
  const [hits, setHits] = useState([] as any[])
  const [page, setPage] = useState(1)
  const [pageSize, setPageSize] = useState(10)
  const [maxPage, setMaxPage] = useState(10)
  const [tags, setTags] = useState(['qa', 'transcript'])
  const [startDate, setStartDate] = useState('');
  const [endDate, setEndDate] = useState('');
  const [resultsSearch, setResultsSearch] = useState('')
  const [resultsPage, setResultsPage] = useState(1)
  const [resultsStartDate, setResultsStartDate] = useState('');
  const [resultsEndDate, setResultsEndDate] = useState('');
  const [resultsTags, setResultsTags] = useState([]);
  const [sortAscending, setSortAscending] = useState(false);

  const loadDataForRuntime = useCallback(async () => {
    setIndex(await loadSearchIndexFromLocal())
    setInitialLoading(false)
    setLoading(true)
  }, [settings])

  const readyForLoadingRuntime = useMemo(
    () => (settings && !downloadModalOpened && !settingsModalOpened),
    [settings, downloadModalOpened, settingsModalOpened]
  )

  useEffect(() => {
    if (initialLoading && readyForLoadingRuntime) {
      loadDataForRuntime()
    }
  }, [initialLoading, readyForLoadingRuntime, loadDataForRuntime])

  useEffect(() => {
    const main = async () => {
      if (!downloadModalOpened) {
        // force open the modal again in case there is no data to show
        // and the download needs to happen.
        const needsDownload = !(await getLocalIndex())
        setDownloadModelOpened(needsDownload)
      }
    }

    main()
  }, [downloadModalOpened])

  useEffect(() => {
    const main = async () => {
      if (!settingsModalOpened) {
        // load potentially updated settings from local storage into the state
        // in order to be up to date after closing.
        const newSettings: Settings = (await localForage.getItem(LOCAL_SETTINGS_KEY)) || SETTINGS_DEFAULT

        if (!(
          settings &&
          newSettings.showScore === settings.showScore &&
          newSettings.autoStopPlaying === settings.autoStopPlaying &&
          newSettings.highlightWords === settings.highlightWords
        )) {
          setSettings(newSettings)
          setInitialLoading(true)
        }
      }
    }

    main()
  }, [settingsModalOpened, settings])

  const runSearch = useCallback(async () => {
    try {
      if (!index) {
        console.error('Index not found')
        return
      }

      const results = await searchIndex(index, search, {
        size: pageSize,
        offset: (page - 1) * pageSize,
        queryFields: {
          text: {
            highlight: true,
          },
          title: {
            highlight: true,
            snippet: true,
            weight: 2
          }
        },
        highlightTags: ['<b class="highlighted">', '</b>'],
        filter: {
          type: tags
        },
        fuzziness: {
          maxError: 0,
          prefixLength: 3
        },
        getDocument: async (_id: string) => {
          let url = _id.startsWith('qa-')
            ? `${process.env.PUBLIC_URL}/data/source/qa/${_id}.json`
            : `${process.env.PUBLIC_URL}/data/source/transcript/${_id}.json`

          const res = await axios.get(url)
          return res.data
        },
        bm25: {
          b: 0.8,
          k1: 1.2
        }
      }, analyzeText)
      console.log(results)

      setHits(results.hits)
      setMaxPage(Math.floor((results.total || 0) / pageSize))
      setResultsSearch(search)
      setResultsStartDate(startDate)
      setResultsEndDate(endDate)
      setResultsTags(tags)
      setLoading(false)
      setResultsPage(page)
    } catch(err) {
      console.error(err)
      alert('Error happened')
      setLoading(false)
    }
  }, [index, search, settings, startDate, endDate, tags, page, pageSize, sortAscending])

  useEffect(() => {
    if (loading && !initialLoading) {
      runSearch()
    }
  }, [initialLoading, loading, runSearch])

  useEffect(() => {
    if (initialLoading || (resultsEndDate === endDate && resultsStartDate === startDate)) return
    setLoading(true)
  }, [initialLoading, startDate, endDate, resultsStartDate, resultsEndDate])

  useEffect(() => {
    if (initialLoading || (tags.length === resultsTags.length)) return
    setLoading(true)
  }, [initialLoading, tags, resultsTags])

  useEffect(() => {
    if (initialLoading || (search === resultsSearch && page === resultsPage)) return
    if (search !== resultsSearch) setPage(1)
    setLoading(true)
  }, [initialLoading, search, resultsSearch, page, resultsPage])

  useEffect(() => {
    if (initialLoading || resultsSearch) return
    setLoading(true)
  }, [initialLoading, sortAscending, resultsSearch])

  const resultsTop: any = useRef()
  useEffect(() => {
    if (!resultsTop || !resultsTop.current || !resultsTop.current) return
    scrollIntoView(resultsTop.current, { behavior: 'smooth' })
  }, [page])

  const handleDownloadModalClose = useCallback((reload = false) => {
    setDownloadModelOpened(false)
    if (reload) {
      setInitialLoading(true)
    }
  }, [])

  if (downloadModalOpened) {
    return <DownloadModal onClose={handleDownloadModalClose} />
  }

  if (settingsModalOpened) {
    return <SettingsModal onClose={() => setSettingsModalOpened(false)} />
  }

  return (
    <div className="App">
      <div className="columns is-desktop m-0">
        <div className="column" />
        <div className="column is-half-desktop">
          {initialLoading
            ? <>
              <progress className="progress is-medium is-info mt-6" max="100"></progress>
            </>
            : <>
              <div className='header'>
                <h1 className='title is-3 mb-3'>
                  <Icon icon='search' size='22px' color='#28464B' className='mr-2' />
                  Search
                </h1>
              </div>

              <SearchBox
                initialSearch={search}
                resultsSearch={resultsSearch}
                loading={loading}
                tags={tags}
                startDate={startDate}
                endDate={endDate}
                sortAscending={sortAscending}
                onTagsChange={(val) => {
                  setPage(1)
                  setTags(val)
                }}
                onStartDateChange={(val) => setStartDate(val)}
                onEndDateChange={(val) => setEndDate(val)}
                onSubmit={(val) => setSearch(val)}
                onShowSettings={() => setSettingsModalOpened(true)}
                onShowUpdates={() => setDownloadModelOpened(true)}
                onSortOrderChange={(asc) => setSortAscending(asc)}
              />

              <span ref={resultsTop} />

              <Pagination
                page={page}
                maxPage={maxPage}
                onChange={(newPage) => setPage(newPage)}
              />

              <p className='m-2'>
                Showing <i>{hits.length}</i>
                {!resultsSearch
                  ? <> questions</>
                  : <> results for "<b>{resultsSearch}</b>"</>}
              </p>

              {hits && hits.map((hit, i) => {
                if (hit?._source?.type === 'qa') {
                  return <QaResultCard item={hit} settings={settings} />
                } else if (hit?._source?.type === 'transcript') {
                  return <TranscriptResultCard item={hit} settings={settings} />
                }
              })}

              {hits && hits.length > 0 && <Pagination
                page={page}
                maxPage={maxPage}
                onChange={(newPage) => setPage(newPage)}
              />}
            </>
          }
        </div>
        <div className="column" />
      </div>

      <footer className="footer">
        <div className="content has-text-centered">
          <p>
            <a href="https://gitlab.com/sirimangalo/search/-/wikis/privacy-policy" target='_blank' rel='noreferrer nofollow'>Privacy Policy <Icon icon='external' size='15px' color='#485fc7' /></a>
            {' | '}
            <a href="https://gitlab.com/sirimangalo/search" target='_blank' rel='noreferrer nofollow'>Source Code <Icon icon='external' size='15px' color='#485fc7' /></a>
          </p>
        </div>
      </footer>
    </div>
  );
}

export default App;
