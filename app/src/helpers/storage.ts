import axios from 'axios'
import localForage from 'localforage'

import {
  LOCAL_INDEX_KEY,
  LOCAL_DATA_KEY
} from '../constants'

import { DataIndex, DownloadStrategy } from '../interfaces';
import type { Index } from 'picosearch';


export const getLocalIndex = async (): Promise<DataIndex> => {
  return localForage.getItem(LOCAL_INDEX_KEY)
}

const setLocalIndex = async (indexData: DataIndex): Promise<void> => {
  await localForage.setItem(LOCAL_INDEX_KEY, indexData)
}

const getRemoteIndex = async (): Promise<DataIndex> => {
  const { data } = await axios.get(`${process.env.PUBLIC_URL}/data/index.json`)
  return data
}

export const getUpdateStrategy = async (): Promise<DownloadStrategy> => {
  const localIndex: DataIndex = await getLocalIndex()
  const remoteIndex: DataIndex = await getRemoteIndex()

  const strategy: DownloadStrategy = {
    queue: [],
    queueSize: 0,
    newIndex: remoteIndex
  }

  if (!localIndex?.dataMD5Sum || localIndex.dataMD5Sum !== remoteIndex.dataMD5Sum) {
    strategy.queue.push({
      filePath: process.env.PUBLIC_URL + remoteIndex.dataPath,
      fileSize: remoteIndex.dataSize,
      md5Sum: remoteIndex.dataMD5Sum,
      localKey: LOCAL_DATA_KEY,
      type: 'file'
    })
    strategy.queueSize += remoteIndex.dataSize
  }

  return strategy
}

export const downloadAndApplyUpdates = async (strategy: DownloadStrategy): Promise<void> => {
  if (strategy.queueSize === 0) {
    return
  }

  let backup = {}

  try {
    for (const item of strategy.queue) {
      if (item.type === 'file') {
        const { data } = await axios.get(
          item.filePath,
          item.filePath.endsWith('.bin') ? { responseType: 'arraybuffer' } : undefined
        )
        backup[item.localKey] = data
        await localForage.setItem(item.localKey, data)
      }
    }

    await setLocalIndex(strategy.newIndex)
  } catch(err) {
    // try to restore state as before update to prevent corruptions
    for (const [key, data] of Object.entries(backup)) {
      await localForage.setItem(key, data)
    }

    throw err
  }
}

export const loadSearchIndexFromLocal = async (): Promise<Index> => {
  const searchIndex: Index = (await localForage.getItem(LOCAL_DATA_KEY)) as Index
  return searchIndex
}
