const glob = require('glob')
const fs = require('fs')

const CACHE_PREFIX = '/question-data/'
const CACHE_FILES = [
  '',
  'bulma.min.css',
  'manifest.json',
  'asset-manifest.json',
  'favicon.ico',
  ...glob.sync('static/**/*', { cwd: __dirname + '/build', nodir: true }),
  ...glob.sync('icons/*.svg', { cwd: __dirname + '/build', nodir: true }),
  ...glob.sync('fonts/*', { cwd: __dirname + '/build', nodir: true }),
  ...glob.sync('**/vocab.txt', { cwd: __dirname + '/build', nodir: true }),
  ...glob.sync('logo*.png', { cwd: __dirname + '/build', nodir: true })
].map(p => CACHE_PREFIX + p)

const swOld = fs.readFileSync(__dirname + '/build/sw.js', { encoding: 'utf-8' })

fs.writeFileSync(__dirname + '/build/sw.js', `const CACHED_FILES=${JSON.stringify(CACHE_FILES)}\n` + swOld)
