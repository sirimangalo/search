1
00:00:00,000 --> 00:00:02,800
Is thinking a good thing or a bad thing?

2
00:00:02,800 --> 00:00:06,900
If we don't think how we live,

3
00:00:06,900 --> 00:00:11,900
well, it's living a good thing or a bad thing.

4
00:00:11,900 --> 00:00:15,700
If you need to think to live,

5
00:00:15,700 --> 00:00:18,900
then you have to ask yourself,

6
00:00:18,900 --> 00:00:22,200
do I need to live?

7
00:00:22,200 --> 00:00:24,800
If you need to live, then well,

8
00:00:24,800 --> 00:00:28,900
I guess that means you need to think.

9
00:00:28,900 --> 00:00:31,300
Of course, some people can live without thinking,

10
00:00:31,300 --> 00:00:33,700
I suppose, without thinking a lot.

11
00:00:33,700 --> 00:00:37,200
So, it's not true that you can't,

12
00:00:37,200 --> 00:00:39,200
you stop living if you stop thinking.

13
00:00:43,200 --> 00:00:46,400
But whether thinking is a good thing or a bad thing

14
00:00:46,400 --> 00:00:49,200
really depends on your perspective and on your belief,

15
00:00:49,200 --> 00:00:52,400
because it's actually not a good thing or a bad thing

16
00:00:52,400 --> 00:00:55,200
intrinsically, it's just a thing.

17
00:00:55,200 --> 00:00:59,200
And the Buddha's teaching is to realize that

18
00:00:59,200 --> 00:01:04,200
to let go of your attachments to thoughts as good or bad

19
00:01:04,200 --> 00:01:08,200
and desire for new thoughts,

20
00:01:08,200 --> 00:01:14,200
or even your desire for not having thoughts,

21
00:01:14,200 --> 00:01:32,200
to give up entirely thinking and then let go.

