Bante, could you please tell us how you would meet criticism regarding rebirth from modern materialists such as Richard Dawkins?
Okay, here's one that we can all sink our teeth into Richard Dawkins.
I'll go on the record saying I don't really, I'm not really fond of Richard Dawkins.
I like some of the things he says, but if you look at, I don't know, this is an ad hominem, isn't it?
But there is something from a Buddhist perspective to...
It's not answering the question at all, really.
It's hopefully kind of sitting with you.
Is that it's sad, really, to see how materialism destroys someone's spiritual qualities,
makes them seem very, very coarse and unpleasant in a sense.
I mean, to me, as refined as he sounds, it's clear that he's not a very refined individual from my brain.
But I'd like to open it up to other people.
So it seems that Richard Dawkins had a very tough English and childhood, you know?
Honestly.
But the really difficult thing is to convince or to show materialists that they're holding a faith,
that they believe in material, physical world is a belief that is not founded in actual experience.
Or physics.
Well, physics is all based on assumptions and theories that are not.
No, but I mean, modern physics shows that modern physics seems to suggest barring a multiverse or so on,
or anywhere worlds that are not perceived.
It seems to suggest that it is the case that entities don't exist, and it's all based on...
To some extent, based on it's true.
Yeah.
Well, how?
Believe it, yeah.
If you really want to look at it, material form doesn't even actually...
Color doesn't even actually exist.
It's what we perceive as color, but really in the outer world,
it's just vibrations being reflected off of surfaces.
It had...
There is no color.
We interpret the world.
We create this world from, you know, our interpretations of things
with solidity, where, and color, and form, and meaning where, really,
to another being with different senses, it's a completely different world.
And that's the problem with...
Not exactly you.
Because you can do the same experiment a million times and get the same...
Same physics experiment a million times and get the same result.
So obviously, there is not proof, but a clear indication that a physics would say
that there is the physical realm.
Oh, no, no, no.
I'm saying that there is the physical realm.
What I'm implying is that our interpretation as to what is the reality of this physical realm.
The perception, yeah.
There's a lot of psychological research.
It's me who's saying that there's no physical realm.
Okay, but you're actually giving fodder to their argument.
Because then they would say, well, yes, that's why we can't trust experience.
Why we can't trust meditation, because our perceptions are skewed.
We have to use...
This is why they fight against meditation as, quote unquote, a special kind of knowing,
or any special kind of knowing.
You can only know something from a materialist point of view through rigorous third,
like impersonal experimentation where you've taken a subject completely out of the picture.
So, you know, this idea of subjectivity is actually a bad thing.
And subjective experience is just that subjective in a bad way.
Well, that's quite all paid. So, yeah, quite all from... I mean, not problem.
It's in every psychology book, psychology book.
You can find the subject of perception, how the art works, and how we actually experience reality.
So, like, through our senses, how is it, how is it...
There's a lot of question marks. We don't know how, how it works.
Science, or we don't still don't know how it works.
Yeah, but we have materialists have pretty good evidence to support their view.
Like, at death, there's nothing.
There's nothing left of brain activity, for example.
So, just if we can get back to... What is the question?
The question is how do we... How would you deal with people?
How do you refute... How do you meet criticism regarding rebirth?
Anyone read Richard Doki's book that we got permission?
Yeah, but that's not about rebirth. Let's stay on track.
Yeah, I'll just ask him, anyone?
I listen. I'll do a book.
But that's not the good one.
I'll tell you, personally, I'm still...
I don't hold any belief on rebirth and reincarnation at all.
I'm still open on that.
I can't say I have a belief or a faith in rebirth.
I think it's in the next slide.
Yeah, I mean, we just wait long enough. I'll know the answer.
It's not really that important, is it?
Because you kind of have a choice.
If your outlook on life is that...
Well, you can even avoid the subject entirely and just say,
well, just in the present moment.
But there are problems with that as well.
So, if you have the view, if your worldview is based on permanent deaths,
not at the moment of death,
then the problems with that are what you see from the materialists,
which you see from secularists in general.
They tend to get caught up in addiction and following after their desires
and living in a very materialistic lifestyle.
And we can see the result that it's having on the world.
We can see the result that has on people's minds and their lives.
But the response, of course, is that, well, it's okay.
When you die, you've just got free.
All these negative habits of addiction and aversion that you've built that,
partialities and so on.
You give them up on your day.
It's not quite that simple because actually you have to deal with them even in this life as well.
It's if you ever stop and look at yourself in what you're doing for yourself.
It's quite unpleasant.
If you take, on the other hand, the view of continuity and the impotence,
the impotence of death, in that sense,
to deprive you of the fruit of your habits,
then you've got to be a lot more careful about how you act
and you really do have to take the mind and experience seriously.
You have to take the facts of your actions seriously.
That tends to, one would think,
more peace and harmony in the world.
On the one hand, and also, well, it leads to a different way of behavior,
I think, quite radically.
So it's not trivial.
I'd like to interrupt you and ask you a question, please.
And it has exactly to do with what we're asking about the question.
It's the ontological dilemma of what would take rebirth
if we don't have a permanent auto, a permanent cell.
What of the five scondas or all of the five scondas would take rebirth
that we would call an eye or a cell?
I'll answer that if you like.
That's why we bring that up right now because
it's the perfect thing for you to really elucidate
because that cuts a lot of confusion.
But it's not what I was trying to discuss
is the difference between the few views
and the different effect that it has on your mind.
Regardless of whether one is right or how you explain one or the other.
So you have these two views.
Then you also have the view that neither one is important
and you should just focus on the present moment.
I think there's potential there.
And it certainly seems to be very much with the Buddha taught many times
to just focus on the present moment.
But if the future or the repercussions of your actions,
in other words, the potential for this moment to create another moment
and another moment and to have no escape from the next moment,
if you don't have that,
and if you're just really sticking and saying,
well, Carpe Diem sees the day, live for the now, so on.
Without some sort of philosophical understanding,
or doctrine of continuity,
or maybe not even philosophical,
but the idea of cause and effect.
Let's put it this way.
Without the idea of cause and effect,
even a theory of just staying in the year and now
does in practical terms lead people to selfishness and to
hedonism, hedonism is I think very much living in the now know.
So I think that is really the key,
and it's also the key to rebirth.
Cause and effect.
Science works in terms of cause and effect.
The laws of science show very clearly
the idea of cause and effect.
Quantum physics shows cause and effect.
So if I get classical mechanics,
classical physics shows cause and effect,
but so does quantum physics.
It just changes the realm of realm in which we talk about cause and effect.
Buddhist meditation shows cause and effect.
It's quite clear from all aspects that cause and effect is reality.
That's what we mean by karma.
When people say karma is faith based,
this is a total misunderstanding of the concept of karma.
When people say that rebirth is faith based.
From a Buddhist perspective,
that's totally a total misunderstanding of our approach towards rebirth.
Rebirth is just an extrapolation of cause and effect.
We don't believe we don't accept the belief
that physical death changes anything in the causal continuum
of consciousness from one moment to the next,
which we clearly perceive to be occurring.
And it goes back to just our,
the framework within which we're working,
which is a mental framework.
We're not, we don't subscribe to the idea that
subjective reality is a negative thing or subjective experience is a negative thing.
We differentiate between the aspects of personal experience
and the subjective ones in a way that science doesn't.
Our materialist science says,
all experience is subjective and leaves it at that.
And it's certain that's certainly not the case.
It's possible to get into a state of mind,
aka what we know of as mindfulness,
and have the results and the observations
of that experience be reproducible in all cases for all people.
So you can have a thousand people performing the same experiments
and provided their truthful and so on and so on.
Provided they're sane, provided they are ordinary and ordinary human beings.
They will be able to achieve the same results.
This is what Sam Harris talks about in terms of everyone being able to see
that compassion, that it's possible to enter into these states
that he seems to think are truly objectively positive or so on.
I mean, he seems to get a little bit caught up in them.
But the point, he makes a good point that it's objectively,
there's an objective mindset.
It does truly occur.
And you could do materialist diagnostics or studies of it
to see whether it was having the same effect on the physical side.
So we simply take that objective reality
on a mental level, more on a mental level,
to be the basis of reality.
And so you would have to introduce the belief
that the belief of nihilism, of annihilation at death.
Whether this actually, you know,
a phases, a materialist is debatable.
I talked to the atheist association of Austin.
What are they called?
Do you know these guys that are on YouTube?
The atheists of...
Yeah, I know.
I heard a lot of them.
Yeah.
And so I started, I sent them an email
and they actually responded to me.
There was this mad guy.
Oh, he was horrible.
He was worse than Richard.
Like just the courses.
No, no, no, no, no.
I think these people were very smart.
There was a guy named Aaron Raw.
I remember an Aaron.
Matt Dillahonti, or whoever's name is, he's the host.
And you know, he's great when he's attacking theists.
But when...
But he just lumped meditation in with all that.
You know, he said to me at one point,
you could make the next exposed movie.
This ridiculous movie about teaching evolution in school.
He said, yeah, you want to teach mindfulness?
Well, you could write the next...
No, what was that?
Exposed, was that movie?
I didn't watch it, but...
No, but this Ben Stiller guy.
So the mad...
Sam Harris is an atheist who makes the distinction there
and who makes this distinction.
Not all...
Do you mean Ben?
Do you mean Ben Stein?
Right.
Ben Stein, not Ben Stein.
Ben Stein.
But Sam Harris, as I'm saying,
you know, there is the objective aspect of personal experience
that, you know, truly,
whatever theory you have about it, it truly holds
and, you know, practices based on it do have an effect.
You know, this happiest man in the world article.
This guy really did get happy.
Oh, but at the end of the day,
the acts of the reaper and karma
and all these teams are beyond what's going to happen.
It's not that important in what this is.
No, karma is incredibly important.
Karma is...
I mean, no, no, no, no, no, no.
You can say instead of karma,
you can...
Karma is a moment.
Yes, they are.
You can say suffering.
I mean...
But yes, but you see,
what is the problem with suffering?
There is the cause of suffering.
The cause of suffering is what?
That's...
Yeah.
The reason...
You do have bad deeds.
You feel horrible, right?
You don't feel bad about it.
Yes.
That's all you need.
But that's all you need.
Oh, okay.
But don't call it karma.
You just call it observation.
This is the problem.
This is the problem.
This is not you.
This is how good...
This is how people think the neighborhood is.
Well, that's all that's important.
This karma thing.
Throw that out.
But that's exactly...
What do you think karma is?
It's practical.
It's not a theory where you think,
wow, yeah, if I kill someone in this life,
I'll be killed in my next life.
All that is is an extrapolation.
If it's true or not true.
I mean, it makes sense.
If you do a bad thing, it hurts you now.
But it also has the potential to ripple into it.
It's the potential to ripple out and hurt you in the future.
It can hurt you in this life.
Maybe the person will come back and tell you in this life.
That makes sense.
Yeah, if you realize if you replace things like karma
with observation, we replace meditating with light training.
Oh, that's not...
No, I...
Karma is the way it is.
Observation is seeing how it is.
Two different things.
Yeah.
I mean, you know, you don't have to know the concept of karma.
Just to see the idea.
This is...
I mean...
Yeah.
So, I mean, you see it.
But you can see, like...
Straight away.
Sometimes...
Sometimes...
Sometimes not...
Sometimes not...
Sometimes...
Sometimes...
It's not necessary to see what's going to happen in your next life based on what you did in this life.
But it's...
That's right.
It's just part of the theory.
You know, you can't stay exactly, but it makes sense that,
you know, it's the bad effects of doing that.
Exactly.
Exactly.
It makes sense.
And I don't think that if we regard Buddhism as might not be religion,
that 80s shouldn't be attacking Buddhism.
Because there's no...
There's not even one thing that should be...
It's not...
Because it's...
But reason.
Karma.
Liberty and karma is something they do it.
Many people do it exactly for this reason.
They take it from...
Well, ask backwards.
I think it's the technical term.
They start, you know, from next life and work their way back,
which is not how we just explain it.
It starts from this moment of ripples out.
It happens in the next life.
Well, it's conjecture.
But it makes sense that it's based on, in some part,
on what we do in this life.
Yeah.
Makes sense.
Basically.
I'm not going to give you true, but it makes sense.
See?
