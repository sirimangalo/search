1
00:00:00,000 --> 00:00:24,240
Okay, good evening, everyone broadcasting live, November 14, 2015.

2
00:00:24,240 --> 00:00:38,240
Today I was in Toronto for the inauguration or the promotion ceremony at a Chinese monastery, a Chinese monastery.

3
00:00:38,240 --> 00:00:46,240
They have a new abbot, and the abbot is a very good friend of mine, someone is becoming a very good friend,

4
00:00:46,240 --> 00:01:00,240
who were good friends at this point, so he invited a bunch of monks, and I was one of them, and it was quite a ceremony.

5
00:01:00,240 --> 00:01:12,240
They have quite intricate ceremonies in that tradition, but it was quite well done,

6
00:01:12,240 --> 00:01:19,240
and then they gave us lunch, which wasn't nearly as well organized as the actual ceremony,

7
00:01:19,240 --> 00:01:26,240
but I think they were so busy trying to make perfect ceremony that.

8
00:01:26,240 --> 00:01:30,240
It was all good.

9
00:01:30,240 --> 00:01:43,240
And then I rushed back just in time to lead this, or to help with this presentation,

10
00:01:43,240 --> 00:01:54,240
to the residence life council, which is all these people who look after the first years in residence.

11
00:01:54,240 --> 00:02:02,240
And people came from all over Canada, actually, from various universities,

12
00:02:02,240 --> 00:02:13,240
so these are undergrad university students who work for their universities to support new students.

13
00:02:13,240 --> 00:02:28,240
And so we had a 50-minute presentation I talked for about 25 minutes, and it was good.

14
00:02:28,240 --> 00:02:30,240
They went really well.

15
00:02:30,240 --> 00:02:33,240
They were quite appreciative.

16
00:02:33,240 --> 00:02:39,240
A few people actually came up to me afterwards, and in the hallway, and took my booklet,

17
00:02:39,240 --> 00:02:45,240
and took my car, and I'm interested in learning more.

18
00:02:45,240 --> 00:02:53,240
One man from Dubai, actually, is, I guess, an immigrant.

19
00:02:53,240 --> 00:03:02,240
And he was saying, in Dubai, it's very, very consumeristic materialist.

20
00:03:02,240 --> 00:03:09,240
And so he just wants to get away from that lifestyle.

21
00:03:09,240 --> 00:03:12,240
He wants to simplify his life, give up his possessions, and so on.

22
00:03:12,240 --> 00:03:19,240
We had a good talk.

23
00:03:19,240 --> 00:03:21,240
That's about it.

24
00:03:21,240 --> 00:03:28,240
That's what happened in the Dhamma today.

25
00:03:28,240 --> 00:03:33,240
Anything new and exciting with you, Robin?

26
00:03:33,240 --> 00:03:37,240
No, I watched a lot of the news out of Paris today.

27
00:03:37,240 --> 00:03:39,240
Oh, Paris, right.

28
00:03:39,240 --> 00:03:43,240
I heard last night we were having a bonfire.

29
00:03:43,240 --> 00:03:45,240
That sort of thing is last night we had a bonfire.

30
00:03:45,240 --> 00:03:47,240
That's why I wasn't here.

31
00:03:47,240 --> 00:03:53,240
And right, there was something in Paris, what happened?

32
00:03:53,240 --> 00:03:58,240
There were, I think, seven or eight coordinated ISIS terrorists

33
00:03:58,240 --> 00:04:04,240
who three lube themselves up, and others shot people.

34
00:04:04,240 --> 00:04:06,240
They themselves up and shot people.

35
00:04:06,240 --> 00:04:14,240
Yeah, 129 people did, and so, like over 500 people involved,

36
00:04:14,240 --> 00:04:17,240
injured or killed, one way or another.

37
00:04:17,240 --> 00:04:22,240
So it's very sad.

38
00:04:22,240 --> 00:04:27,240
It's very sad.

39
00:04:27,240 --> 00:04:45,240
There's a line in the song,

40
00:04:45,240 --> 00:04:55,240
and why it has been made a Canadian band that comes.

41
00:04:55,240 --> 00:05:00,240
What makes a person so poisonous righteous that they'll send hell to anyone who just disagrees?

42
00:05:00,240 --> 00:05:04,240
That's a good line.

43
00:05:04,240 --> 00:05:06,240
It's an interesting song.

44
00:05:06,240 --> 00:05:08,240
It's a acapella, acapella.

45
00:05:08,240 --> 00:05:12,240
They did a lot of acapella songs, and it starts off.

46
00:05:12,240 --> 00:05:15,240
It's about the war in the Gulf, actually.

47
00:05:15,240 --> 00:05:18,240
So it's not taking sides, I don't think.

48
00:05:18,240 --> 00:05:23,240
But it says, we got a call to write a song about the war in the Gulf,

49
00:05:23,240 --> 00:05:26,240
but we couldn't hurt anyone's feelings.

50
00:05:26,240 --> 00:05:30,240
So we tried, then gave up, because there was no such song.

51
00:05:30,240 --> 00:05:33,240
But the trying was very revealing.

52
00:05:33,240 --> 00:05:40,240
What makes a person so poisonous righteous that they'll send hell to anyone who just disagrees?

53
00:05:40,240 --> 00:05:44,240
I don't know about the war in the Gulf.

54
00:05:44,240 --> 00:05:48,240
I'm not quite sure what they're referring to, but I get the sentiment.

55
00:05:48,240 --> 00:05:50,240
It's a really good question.

56
00:05:50,240 --> 00:05:56,240
I mean, the answer is not like it's hard to understand.

57
00:05:56,240 --> 00:06:01,240
It's just amazing how it's sad.

58
00:06:01,240 --> 00:06:08,240
That's the saddest part that people will be so poisonous righteous.

59
00:06:08,240 --> 00:06:12,240
Of course, and then of course the responses, so many people are saying,

60
00:06:12,240 --> 00:06:16,240
well, we have to go and kill these people.

61
00:06:16,240 --> 00:06:27,240
But of course, that's only going to just perpetuate this over and over this.

62
00:06:27,240 --> 00:06:38,240
Yeah.

63
00:06:38,240 --> 00:06:44,240
But I think the point is you have to turn to the people who are rational.

64
00:06:44,240 --> 00:06:51,240
And I don't think that's the people who are going to respond with violence.

65
00:06:51,240 --> 00:07:00,240
I mean, to some extent you have to understand that the people on the other side are complicit and responsible.

66
00:07:00,240 --> 00:07:06,240
And there's a lot of, I think, interest in war and fear and that kind of thing.

67
00:07:06,240 --> 00:07:10,240
It sells in the people who are the arms industry.

68
00:07:10,240 --> 00:07:20,240
Politicians tend to like it because it makes people vote for them.

69
00:07:20,240 --> 00:07:30,240
It's a good way of controlling people who are controlled and through fear.

70
00:07:30,240 --> 00:07:37,240
So you have to be clear where we stand, I think.

71
00:07:37,240 --> 00:07:42,240
We have to, we can't take sides.

72
00:07:42,240 --> 00:07:46,240
We take sides ideologically.

73
00:07:46,240 --> 00:07:53,240
We should take an ideological side in the sense that violence is wrong.

74
00:07:53,240 --> 00:07:55,240
And stealing fear in people is wrong.

75
00:07:55,240 --> 00:07:57,240
Biggest tree is wrong.

76
00:07:57,240 --> 00:08:01,240
The arrogance, righteousness, all of these things are wrong.

77
00:08:01,240 --> 00:08:06,240
Well, not righteousness, but this arrogance.

78
00:08:06,240 --> 00:08:11,240
Righteousness actually is good, but it has to be truly righteous.

79
00:08:11,240 --> 00:08:20,240
There's the idea being self-righteous, which is the sense of, you're right because you think it.

80
00:08:20,240 --> 00:08:22,240
Well, that's the only reason why.

81
00:08:22,240 --> 00:08:26,240
It's right because I think I'm right.

82
00:08:26,240 --> 00:08:28,240
You can't be right.

83
00:08:28,240 --> 00:08:32,240
It's the ideas you have are the right or wrong.

84
00:08:32,240 --> 00:08:35,240
The rightness exists in the ideas intrinsically.

85
00:08:35,240 --> 00:08:40,240
It's nothing to do with you being right.

86
00:08:40,240 --> 00:08:55,240
Well, I mean, there is that, but focusing on that is the problem when you feel conceded because you're right.

87
00:08:55,240 --> 00:09:07,240
I guess we also should talk about not being afraid of death because terrorism relies on people being afraid.

88
00:09:07,240 --> 00:09:12,240
I mean, killing a hundred and some people is not going to change the world, right?

89
00:09:12,240 --> 00:09:16,240
But it will, if everyone gets very afraid of it.

90
00:09:16,240 --> 00:09:26,240
So, I mean, I don't know whether that sounds kind of cruel, but my point being, this doesn't a war make.

91
00:09:26,240 --> 00:09:33,240
It's a horrible thing that a hundred and some people died, and that's just really horrific.

92
00:09:33,240 --> 00:09:40,240
I mean, tsunami was far more horrific than tsunami in that devastated Asia, right?

93
00:09:40,240 --> 00:09:41,240
Yes.

94
00:09:41,240 --> 00:09:45,240
Far more.

95
00:09:45,240 --> 00:09:52,240
Yeah, the president of France called it an act of war, and the Pope said it was the start of World War III.

96
00:09:52,240 --> 00:09:55,240
Uh-huh.

97
00:09:55,240 --> 00:09:56,240
It may very well be.

98
00:09:56,240 --> 00:10:01,240
I mean, that could be what's happening because, I mean, the point is,

99
00:10:01,240 --> 00:10:05,240
we don't have bad guys on one side and good guys on another.

100
00:10:05,240 --> 00:10:14,240
The bad guys on the other, on our side, are going to push for war, I suppose.

101
00:10:14,240 --> 00:10:19,240
I suppose there's probably a general sense of not wanting to go to war.

102
00:10:19,240 --> 00:10:22,240
I don't know how it is now, there's a yesterday.

103
00:10:22,240 --> 00:10:24,240
That's a big deal, right?

104
00:10:24,240 --> 00:10:29,240
Yeah.

105
00:10:29,240 --> 00:10:35,240
You know, and not so long ago, I mean, in the United States, you know, people were burnt out of war,

106
00:10:35,240 --> 00:10:42,240
but probably within the last week with ISIS had bombed a Russian plane,

107
00:10:42,240 --> 00:11:04,240
and now this, within one week's time, it frightens people, changes things.

108
00:11:04,240 --> 00:11:07,240
It's not good for Bernie Sanders.

109
00:11:07,240 --> 00:11:11,240
No, it's not good at all for Bernie Sanders before Bernie.

110
00:11:11,240 --> 00:11:16,240
Having a Democratic debate tonight, and it was supposed to be an economic debate.

111
00:11:16,240 --> 00:11:20,240
And in that, Bernie would have, you know, really shown that's really his thing.

112
00:11:20,240 --> 00:11:26,240
And they switched gears, and they're making the focus of foreign relations.

113
00:11:26,240 --> 00:11:28,240
You know, this incident.

114
00:11:28,240 --> 00:11:33,240
And they said on the news, one of his aides flipped out or something,

115
00:11:33,240 --> 00:11:35,240
because he didn't want that.

116
00:11:35,240 --> 00:11:39,240
I'm not sure if it was exactly what it was, but they portrayed him very poorly.

117
00:11:39,240 --> 00:11:44,240
So if they said his aide flipped out about the change in questioning or something, so.

118
00:11:44,240 --> 00:11:45,240
Yeah.

119
00:11:45,240 --> 00:11:50,240
Probably won't be as best night.

120
00:11:50,240 --> 00:11:55,240
Well, I guess I mean, the point is that he's very anti-war.

121
00:11:55,240 --> 00:11:57,240
He said he's a good guy.

122
00:11:57,240 --> 00:12:00,240
He seems to be one of us, you know?

123
00:12:00,240 --> 00:12:01,240
Oh, yes, definitely.

124
00:12:01,240 --> 00:12:08,240
So now that people are starting to want to go to war, that's not good for the peace movement.

125
00:12:08,240 --> 00:12:15,240
Yeah, and some people wanting to talk about, you know, this says the focus up tonight's debate, definitely.

126
00:12:15,240 --> 00:12:16,240
That's the deal.

127
00:12:16,240 --> 00:12:17,240
That's the deal.

128
00:12:17,240 --> 00:12:25,240
I mean, we almost played one to say it's a conspiracy against him.

129
00:12:25,240 --> 00:12:33,240
So we're all gonna die anyway.

130
00:12:33,240 --> 00:12:39,240
Near it is slowly but surely you've been closer to the Sun, or is the Sun gonna explode?

131
00:12:39,240 --> 00:12:40,240
I can't remember what she does.

132
00:12:40,240 --> 00:12:43,240
I think the Sun is gonna explode first.

133
00:12:43,240 --> 00:12:48,240
And burn us all to a crisp.

134
00:12:48,240 --> 00:12:51,240
Oh, obviously, we won't be here.

135
00:12:51,240 --> 00:12:55,240
But if you are, well, you'll be burned to a crisp.

136
00:12:55,240 --> 00:13:01,240
Actually, I think what'll happen is all the water will evaporate first, right?

137
00:13:01,240 --> 00:13:04,240
So you'll die of thirst.

138
00:13:04,240 --> 00:13:09,240
I don't know what you'll die of, you'll die of them.

139
00:13:09,240 --> 00:13:16,240
There's a good jotica quote.

140
00:13:16,240 --> 00:13:37,240
The something, something, the outcast and the well-born, the fool and the wise.

141
00:13:37,240 --> 00:13:44,240
For richer, poor, one end is sure each man among them dies.

142
00:13:44,240 --> 00:13:51,240
Very true.

143
00:13:51,240 --> 00:14:00,240
Do we have any questions to let?

144
00:14:00,240 --> 00:14:07,240
I think we do.

145
00:14:07,240 --> 00:14:14,240
When we're both on date, what is the meaning of observing the body internally, externally,

146
00:14:14,240 --> 00:14:17,240
and both internally and externally?

147
00:14:17,240 --> 00:14:24,240
I understand observing body internally, but not very clear on what is observing body externally

148
00:14:24,240 --> 00:14:25,240
means.

149
00:14:25,240 --> 00:14:30,240
Well, there's two senses in which it could be understood.

150
00:14:30,240 --> 00:14:37,240
I think the commentary favors the interpretation.

151
00:14:37,240 --> 00:14:40,240
The internal and external sense spaces.

152
00:14:40,240 --> 00:14:47,240
So there are, the eye is internal, and sights are external.

153
00:14:47,240 --> 00:14:57,240
So if you say, if you say light, light, then you're focusing on the external aspect.

154
00:14:57,240 --> 00:15:02,240
If you say eye, I mean, you wouldn't really say these things.

155
00:15:02,240 --> 00:15:08,240
But when you say seeing, if you're aware of the eye seeing, then that's internal.

156
00:15:08,240 --> 00:15:13,240
If you're aware of what you see, the light, that's external.

157
00:15:13,240 --> 00:15:15,240
It just means there's two aspects.

158
00:15:15,240 --> 00:15:22,240
Or when you hear, if you're aware of the ear hearing, that's the internal.

159
00:15:22,240 --> 00:15:26,240
If you're aware of the sound, it's just your focus.

160
00:15:26,240 --> 00:15:27,240
It can be both.

161
00:15:27,240 --> 00:15:30,240
It can be one or the other, it can be both.

162
00:15:30,240 --> 00:15:32,240
That's one way of interpreting.

163
00:15:32,240 --> 00:15:37,240
But the other one is if you look at the Satipatana Sutta, there are certain sections

164
00:15:37,240 --> 00:15:39,240
that are external.

165
00:15:39,240 --> 00:15:46,240
Not many of them, but there's the section on the cemetery front in places.

166
00:15:46,240 --> 00:15:49,240
So that's mindfulness of someone else's body.

167
00:15:49,240 --> 00:15:53,240
It's mindfulness of an external body.

168
00:15:53,240 --> 00:15:57,240
So that word internally and externally are both internally and externally.

169
00:15:57,240 --> 00:16:02,240
That phrase occurs at the end of each section.

170
00:16:02,240 --> 00:16:05,240
But this kind of happens in the polity where they just repeat it.

171
00:16:05,240 --> 00:16:12,240
It's just repeated after the end of each section, even though it only applies to certain sections.

172
00:16:12,240 --> 00:16:17,240
So it may very well be that all that means is in this Sutta, there are certain sections

173
00:16:17,240 --> 00:16:21,240
that are external and certain that are internal.

174
00:16:21,240 --> 00:16:26,240
Of course, most of them are internal.

175
00:16:26,240 --> 00:16:37,240
But you could think of ways in which you could be aware of external feelings and thoughts and emotions.

176
00:16:37,240 --> 00:16:40,240
If you're aware of someone else, you watch them get angry.

177
00:16:40,240 --> 00:16:43,240
That's kind of a mindfulness, or it could be.

178
00:16:43,240 --> 00:16:45,240
But it's conceptual.

179
00:16:45,240 --> 00:16:46,240
That's not webassant.

180
00:16:46,240 --> 00:16:50,240
That won't work for webassant, but it would work in certain instances for Sutta.

181
00:16:50,240 --> 00:16:56,240
Or it works for a general mental development in terms of seeing how anger affects other people.

182
00:16:56,240 --> 00:16:59,240
For example, seeing how greed affects other people.

183
00:16:59,240 --> 00:17:06,240
It's something you can be mindful of, you know, theoretically, or conceptually mindful of.

184
00:17:06,240 --> 00:17:08,240
And can help you in your spiritual development.

185
00:17:08,240 --> 00:17:09,240
It's not webassant.

186
00:17:09,240 --> 00:17:14,240
But the Sutta Sutta isn't just webassant.

187
00:17:14,240 --> 00:17:20,240
What about movement walking, sitting? Is that internal or external?

188
00:17:20,240 --> 00:17:22,240
That's internal.

189
00:17:22,240 --> 00:17:25,240
The body is internal, but external is the...

190
00:17:25,240 --> 00:17:32,240
So when you touch the floor, the floor aspect, the hardness aspect comes from the floor.

191
00:17:32,240 --> 00:17:38,240
The softness from the foot is internal, but the hardness from the floor is external.

192
00:17:38,240 --> 00:17:42,240
When your teeth go together, they're both hard, both internal.

193
00:17:42,240 --> 00:17:46,240
But if you put a spoon in your mouth and you bite on that, or food,

194
00:17:46,240 --> 00:17:50,240
if you put food in your mouth and you bite on that, the food is external.

195
00:17:50,240 --> 00:17:54,240
I mean, it gets blurred there because the food becomes internal,

196
00:17:54,240 --> 00:17:59,240
but it might be considered internal at that point.

197
00:17:59,240 --> 00:18:01,240
Thank you, Bob.

198
00:18:01,240 --> 00:18:03,240
Okay.

199
00:18:11,240 --> 00:18:13,240
A comment that we might cross,

200
00:18:13,240 --> 00:18:17,240
intra-meda, and the Milky Way will collapse with it before the sun turns Nova

201
00:18:17,240 --> 00:18:21,240
in a matter of billions of years.

202
00:18:21,240 --> 00:18:22,240
Perhaps.

203
00:18:22,240 --> 00:18:24,240
Okay.

204
00:18:24,240 --> 00:18:27,240
I'll put that on my calendar.

205
00:18:27,240 --> 00:18:34,240
One of those perpetual calendars for billions of years from now.

206
00:18:34,240 --> 00:18:42,240
I'm not sure if this is a question for you, but they are just the general discussion about the sun and all,

207
00:18:42,240 --> 00:18:46,240
but when that happens, I wonder if people will be reborn in the nearest,

208
00:18:46,240 --> 00:18:50,240
M-class planet, wherever that is.

209
00:18:50,240 --> 00:18:52,240
Wondering, wondering?

210
00:18:52,240 --> 00:18:56,240
Yes, wondering what an M-class planet is?

211
00:18:56,240 --> 00:19:00,240
Wondering, wondering, wondering.

212
00:19:02,240 --> 00:19:05,240
And someone asking, will you be answering questions on here?

213
00:19:05,240 --> 00:19:06,240
Yes.

214
00:19:06,240 --> 00:19:08,240
Submit your question.

215
00:19:08,240 --> 00:19:11,240
I wonder if they're not getting the audio or video.

216
00:19:11,240 --> 00:19:14,240
Maybe you can post a link to the YouTube?

217
00:19:14,240 --> 00:19:15,240
Sure.

218
00:19:15,240 --> 00:19:18,240
Do you have the link or only I do, right?

219
00:19:18,240 --> 00:19:21,240
I can get it through YouTube.

220
00:19:21,240 --> 00:19:26,240
Well, I got it here.

221
00:19:26,240 --> 00:19:31,240
Could be.

222
00:19:31,240 --> 00:19:32,240
Okay.

223
00:19:32,240 --> 00:19:36,240
So the YouTube is like, what, 30 seconds behind or something?

224
00:19:36,240 --> 00:20:00,240
Yeah.

225
00:20:00,240 --> 00:20:03,240
So how was the gunfire?

226
00:20:03,240 --> 00:20:06,240
Good. No one came, but it was good.

227
00:20:06,240 --> 00:20:10,240
We had two new people come, and they left early.

228
00:20:10,240 --> 00:20:14,240
We started talking about some fairly theoretical stuff,

229
00:20:14,240 --> 00:20:17,240
and then they just got up and left.

230
00:20:17,240 --> 00:20:17,240
That's it.

231
00:20:17,240 --> 00:20:18,240
Goodbye.

232
00:20:18,240 --> 00:20:20,240
Everyone wrote, did we scare them away?

233
00:20:20,240 --> 00:20:21,240
I don't know.

234
00:20:21,240 --> 00:20:30,240
Well, we stayed for two hours.

235
00:20:30,240 --> 00:20:33,240
It's an interesting group.

236
00:20:33,240 --> 00:20:40,240
Not all meditators, and most of them are interested in meditation.

237
00:20:40,240 --> 00:20:45,240
But we had about six or eight people.

238
00:20:45,240 --> 00:20:46,240
That's good.

239
00:20:46,240 --> 00:20:50,240
It sounds nice.

240
00:20:50,240 --> 00:20:51,240
Yeah, it's good.

241
00:20:51,240 --> 00:20:56,240
We did talk about the idea of doing public meditations

242
00:20:56,240 --> 00:21:00,240
in the student center.

243
00:21:00,240 --> 00:21:04,240
There's a big open area where there's a huge crowd of people walking through,

244
00:21:04,240 --> 00:21:09,240
and we could just sit down and do meditation in the public space there.

245
00:21:09,240 --> 00:21:12,240
We could even just use it to teach meditation.

246
00:21:12,240 --> 00:21:19,240
I could teach people as they came in.

247
00:21:19,240 --> 00:21:22,240
And then if we got a group going, we could start doing flash moms,

248
00:21:22,240 --> 00:21:25,240
meditation, flash moms.

249
00:21:25,240 --> 00:21:28,240
Did you ever see that video we did in Winnipeg?

250
00:21:28,240 --> 00:21:30,240
We did.

251
00:21:30,240 --> 00:21:32,240
I walked in sat down.

252
00:21:32,240 --> 00:21:35,240
Then 30 seconds later, a second person came and sat down,

253
00:21:35,240 --> 00:21:39,240
and 30 seconds we added a person every 30 seconds.

254
00:21:39,240 --> 00:21:43,240
It was only about 20 of us, but it was fun.

255
00:21:43,240 --> 00:21:45,240
Yeah.

256
00:21:45,240 --> 00:21:47,240
It definitely sounds a little more comfortable in the student center

257
00:21:47,240 --> 00:21:53,240
than outdoors this time of year.

258
00:21:53,240 --> 00:21:55,240
Yeah, so maybe we'll try that.

259
00:21:55,240 --> 00:21:57,240
It's just a matter of scheduling.

260
00:22:07,240 --> 00:22:10,240
Darwin explained him class playing it his life supporting planet.

261
00:22:10,240 --> 00:22:13,240
Thank you, Darwin.

262
00:22:13,240 --> 00:22:20,240
I haven't heard that term before.

263
00:22:20,240 --> 00:22:25,240
What is a good strategy when trying to get people to meditate?

264
00:22:25,240 --> 00:22:28,240
Friends and family.

265
00:22:28,240 --> 00:22:31,240
I acknowledge your desire for other people to meditate.

266
00:22:31,240 --> 00:22:35,240
That's important.

267
00:22:35,240 --> 00:22:39,240
Because I can't think of a good strategy.

268
00:22:39,240 --> 00:22:41,240
It's not about getting people to meditate.

269
00:22:41,240 --> 00:22:45,240
Meditation's hard if you don't really want to do it.

270
00:22:45,240 --> 00:22:48,240
It's not really likely to succeed.

271
00:22:48,240 --> 00:22:54,240
Maybe that's pessimistic, but it can't help

272
00:22:54,240 --> 00:22:57,240
it be a bit pessimistic or skeptical.

273
00:22:57,240 --> 00:23:00,240
Because you can get people to do a little bit of meditation,

274
00:23:00,240 --> 00:23:02,240
but you find you're pushing them in its effort,

275
00:23:02,240 --> 00:23:04,240
and then they just give it up anyway.

276
00:23:04,240 --> 00:23:06,240
The person wants to do it.

277
00:23:06,240 --> 00:23:08,240
It's something that you won't need to push them.

278
00:23:08,240 --> 00:23:12,240
I think a good strategy is to talk about it,

279
00:23:12,240 --> 00:23:16,240
but not have any emotional vestedness.

280
00:23:16,240 --> 00:23:19,240
Don't let it get to emotionally.

281
00:23:19,240 --> 00:23:21,240
I want them to meditate.

282
00:23:21,240 --> 00:23:23,240
Just do it as a matter of course.

283
00:23:23,240 --> 00:23:25,240
Of course, you're going to tell people to meditate.

284
00:23:25,240 --> 00:23:27,240
Of course, you're going to talk about it.

285
00:23:27,240 --> 00:23:29,240
Don't be afraid to talk about it.

286
00:23:36,240 --> 00:23:40,240
It's so hard when you see someone who is struggling

287
00:23:40,240 --> 00:23:43,240
and you know how much it would help them.

288
00:23:43,240 --> 00:23:48,240
That's the thing is for many people that wouldn't help them.

289
00:23:48,240 --> 00:23:56,240
They'd try to practice and they'd fail when they'd get discouraged.

290
00:23:56,240 --> 00:23:59,240
It would help people if they suddenly had no more defilements,

291
00:23:59,240 --> 00:24:04,240
but it's getting from here to there.

292
00:24:04,240 --> 00:24:07,240
For most people, it's just too far.

293
00:24:07,240 --> 00:24:10,240
So you have to be ready for it in other words?

294
00:24:10,240 --> 00:24:15,240
Yeah, I mean, you have to have some potentiality.

295
00:24:15,240 --> 00:24:26,240
We don't have time or resources to bring people from 0 to 100.

296
00:24:26,240 --> 00:24:29,240
It's not really an invest interest to do so.

297
00:24:29,240 --> 00:24:31,240
You better off taking people who are really ready

298
00:24:31,240 --> 00:24:36,240
because they'll turn around and help the people who are less ready

299
00:24:36,240 --> 00:24:41,240
and they'll work like that.

300
00:24:41,240 --> 00:24:46,240
That makes sense.

301
00:24:46,240 --> 00:24:48,240
How can one truly become enlightened

302
00:24:48,240 --> 00:24:50,240
if no matter how much he meditates,

303
00:24:50,240 --> 00:24:53,240
he will still require food?

304
00:24:53,240 --> 00:24:56,240
Is this a person who has done some annotation with us?

305
00:24:56,240 --> 00:25:00,240
I think this is a new person.

306
00:25:00,240 --> 00:25:03,240
Well, I'm going to ask that.

307
00:25:03,240 --> 00:25:08,240
I'm going to skip this question and say,

308
00:25:08,240 --> 00:25:12,240
you have to start meditating with us if you want me to answer your questions.

309
00:25:12,240 --> 00:25:15,240
It's not a hard and fast for all,

310
00:25:15,240 --> 00:25:23,240
but I want you to be meditating before you answer before you ask this question.

311
00:25:23,240 --> 00:25:25,240
Maybe you have a started meditating,

312
00:25:25,240 --> 00:25:28,240
but you should read my booklet if you haven't.

313
00:25:28,240 --> 00:25:33,240
And if you have, or once you have, and start meditating,

314
00:25:33,240 --> 00:25:36,240
and come back when you're green,

315
00:25:36,240 --> 00:25:41,240
because your question thing is yellow or orange,

316
00:25:41,240 --> 00:25:45,240
which means you haven't meditated.

317
00:25:45,240 --> 00:25:49,240
Why? Because the question is a little bit neat.

318
00:25:49,240 --> 00:25:51,240
It has to have a nuanced answer,

319
00:25:51,240 --> 00:25:56,240
and it's a question that may be not coming

320
00:25:56,240 --> 00:26:00,240
from lack of information or lack of understanding about meditation.

321
00:26:00,240 --> 00:26:04,240
So I'm going to be sure that you understand meditation before you ask that

322
00:26:04,240 --> 00:26:06,240
if you want to ask.

323
00:26:06,240 --> 00:26:10,240
Maybe you'll meditating, you'll say, oh, I don't need to ask that anymore.

324
00:26:16,240 --> 00:26:19,240
I think that's why people have been meditating for a while

325
00:26:19,240 --> 00:26:22,240
to ask fewer questions.

326
00:26:22,240 --> 00:26:27,240
Yeah, I mean, in the meditation course,

327
00:26:27,240 --> 00:26:28,240
that happens.

328
00:26:28,240 --> 00:26:31,240
The beginner meditators, you have a lot to tell them to explain,

329
00:26:31,240 --> 00:26:33,240
and you have to guide them.

330
00:26:33,240 --> 00:26:34,240
But as they get on in the course,

331
00:26:34,240 --> 00:26:37,240
there's really less and less for you to do,

332
00:26:37,240 --> 00:26:40,240
because I've just confirmed that they're doing okay,

333
00:26:40,240 --> 00:27:00,240
and giving them a new exercise.

334
00:27:00,240 --> 00:27:04,240
Someone asked me, and this man from Dubai asked me,

335
00:27:04,240 --> 00:27:07,240
he asked a question in the group,

336
00:27:07,240 --> 00:27:13,240
whether it's okay to listen to music when you meditate.

337
00:27:13,240 --> 00:27:16,240
And so I explain to him what depends what you mean by meditation,

338
00:27:16,240 --> 00:27:20,240
but in our meditation, we're trying to become objective.

339
00:27:20,240 --> 00:27:23,240
So everything is music.

340
00:27:23,240 --> 00:27:28,240
And the point is that if you are partial towards the music,

341
00:27:28,240 --> 00:27:31,240
and the music makes it easier for you to focus.

342
00:27:31,240 --> 00:27:33,240
Actually, he asked this in the beginning.

343
00:27:33,240 --> 00:27:35,240
I just said it like that, but then he came up later

344
00:27:35,240 --> 00:27:38,240
and he said, well, the music is like nature, music,

345
00:27:38,240 --> 00:27:40,240
with waterfalls, and that kind of thing.

346
00:27:40,240 --> 00:27:42,240
And I said, well, it makes it easier for you to focus

347
00:27:42,240 --> 00:27:45,240
than your eats of crunch for you.

348
00:27:45,240 --> 00:27:47,240
And we're not interested in that.

349
00:27:47,240 --> 00:27:53,240
We're interested in how you react when it's difficult.

350
00:27:53,240 --> 00:28:01,240
So we could make it very, very easy to meditate.

351
00:28:01,240 --> 00:28:03,240
One meditator here recently.

352
00:28:03,240 --> 00:28:06,240
I mean, always meditators will try to find a trick

353
00:28:06,240 --> 00:28:09,240
or a way to make the meditation easier.

354
00:28:09,240 --> 00:28:11,240
And I said, do I can make it really easy for you?

355
00:28:11,240 --> 00:28:12,240
And he said, yeah, you can.

356
00:28:12,240 --> 00:28:15,240
He said, yeah, you do 10 minutes of lying meditation

357
00:28:15,240 --> 00:28:17,240
a day, and that's your meditation.

358
00:28:17,240 --> 00:28:18,240
Very easy.

359
00:28:18,240 --> 00:28:20,240
But what do you get from it?

360
00:28:20,240 --> 00:28:21,240
You get nothing.

361
00:28:24,240 --> 00:28:25,240
We're trying to make it more difficult.

362
00:28:25,240 --> 00:28:27,240
We're trying to challenge ourselves.

363
00:28:27,240 --> 00:28:29,240
We're trying to see what happens,

364
00:28:29,240 --> 00:28:32,240
and we're trying to learn how we react when the going gets stuck.

365
00:28:32,240 --> 00:28:34,240
So that we can change the way we react.

366
00:28:34,240 --> 00:28:37,240
If you don't have things that trigger,

367
00:28:37,240 --> 00:28:40,240
then you'll never learn how to overcome the trigger.

368
00:28:40,240 --> 00:28:49,240
So making it easy to meditate is not useful.

369
00:28:54,240 --> 00:28:57,240
It's great that someone from Dubai is interested in meditation.

370
00:28:57,240 --> 00:29:04,240
And everything is so flashy, and so many spectacular things there.

371
00:29:04,240 --> 00:29:08,240
So it's great that somebody is interested in something

372
00:29:08,240 --> 00:29:11,240
that isn't impressed by that.

373
00:29:11,240 --> 00:29:28,240
I find it easier to note experiences during walking meditation,

374
00:29:28,240 --> 00:29:32,240
during walking meditation quietly out loud.

375
00:29:32,240 --> 00:29:33,240
Is this okay?

376
00:29:33,240 --> 00:29:35,240
Thanks.

377
00:29:35,240 --> 00:29:38,240
Yeah, again, you find it easier than that's something that you shouldn't do.

378
00:29:38,240 --> 00:29:41,240
We're not trying to make it easy.

379
00:29:41,240 --> 00:29:47,240
We're trying to get stronger.

380
00:29:47,240 --> 00:29:53,240
We'll never get stronger if you just make it easier on yourself.

381
00:29:55,240 --> 00:29:59,240
It's okay, but it's less useful.

382
00:29:59,240 --> 00:30:10,240
It's also distracting because then your mind isn't with the object.

383
00:30:10,240 --> 00:30:13,240
It's with your mouth, it's like, you know,

384
00:30:13,240 --> 00:30:16,240
it's a big stop trying to walk and talk at the same time.

385
00:30:16,240 --> 00:30:19,240
Your mind is doing two things.

386
00:30:19,240 --> 00:30:23,240
As you may, to get more distracted.

387
00:30:23,240 --> 00:30:25,240
You want to make it easy.

388
00:30:25,240 --> 00:30:26,240
Stop doing walking meditation.

389
00:30:26,240 --> 00:30:29,240
Just start dancing, turn on some music and start dancing.

390
00:30:29,240 --> 00:30:32,240
That'll be easy.

391
00:30:32,240 --> 00:30:37,240
I'm shadowing the lyrics to this song.

392
00:30:37,240 --> 00:30:40,240
It's not about making it easy.

393
00:30:40,240 --> 00:30:45,240
It's actually more about making it fairly difficult.

394
00:30:45,240 --> 00:30:50,240
Not to the extent that you should punch yourself to understand pain or something.

395
00:30:50,240 --> 00:30:58,240
But difficult in the sense of patience, requiring patience and focus.

396
00:30:58,240 --> 00:31:03,240
But if something is so distracting you that it won't leave you,

397
00:31:03,240 --> 00:31:10,240
then don't you recommend to stand and note that for a little bit?

398
00:31:10,240 --> 00:31:11,240
Yeah.

399
00:31:11,240 --> 00:31:16,240
I mean, that's only because again walking and noting something else are

400
00:31:16,240 --> 00:31:20,240
too diverse, better to be focused on one thing,

401
00:31:20,240 --> 00:31:22,240
so stopping makes sense.

402
00:31:42,240 --> 00:31:44,240
We caught up.

403
00:31:44,240 --> 00:31:50,240
I think we're all caught up, aren't they?

404
00:31:50,240 --> 00:31:52,240
Okay.

405
00:31:52,240 --> 00:31:55,240
Well, then, good night, everyone.

406
00:31:55,240 --> 00:31:57,240
Thank you, Bob.

407
00:31:57,240 --> 00:32:00,240
Thanks for your help.

408
00:32:00,240 --> 00:32:01,240
Thank you.

409
00:32:01,240 --> 00:32:15,240
Good night.

