1
00:00:00,000 --> 00:00:01,000
Hello.

2
00:00:01,000 --> 00:00:09,000
Today I will be beginning a new series of videos on a text called the Dhamapada.

3
00:00:09,000 --> 00:00:18,240
The Dhamapada is a group of 423 verses that are said to have been taught by the Lord Buddha

4
00:00:18,240 --> 00:00:21,240
at various times during his life.

5
00:00:21,240 --> 00:00:29,240
So it's generally considered to be a fairly good summary of the Buddha's teachings.

6
00:00:29,240 --> 00:00:36,320
So I thought it would be quite a good subject for videos in order to spread the Buddha's

7
00:00:36,320 --> 00:00:43,520
teaching and help more people to realize the benefits of the teaching.

8
00:00:43,520 --> 00:00:48,720
So what I'm going to do is read you here that I've got the polyverse.

9
00:00:48,720 --> 00:00:52,720
I'm going to read it in poly, one verse at a time.

10
00:00:52,720 --> 00:00:57,800
And after the verse I will translate it piece by piece into English, give you an understanding

11
00:00:57,800 --> 00:00:58,800
of what it means.

12
00:00:58,800 --> 00:01:05,480
Then I will tell the story which goes with the verse because each verse has a story that

13
00:01:05,480 --> 00:01:11,760
was passed down, that gives the occasion and the circumstances in which the Buddha gave

14
00:01:11,760 --> 00:01:14,080
this teaching.

15
00:01:14,080 --> 00:01:20,760
And after I give this sort of short summary of the story, then I'll explain how I think

16
00:01:20,760 --> 00:01:27,000
we should understand this verse and how it can relate to our lives and to our practice.

17
00:01:27,000 --> 00:01:32,760
So one note about the stories is that there's two ways we can go.

18
00:01:32,760 --> 00:01:37,960
Some people will pay more attention to the stories than the verses and remember the

19
00:01:37,960 --> 00:01:40,520
Dhamapada as a series of stories.

20
00:01:40,520 --> 00:01:44,880
Another group of people will never have heard or have read the stories and what they're

21
00:01:44,880 --> 00:01:48,680
going to think of the Dhamapada as a group of verses.

22
00:01:48,680 --> 00:01:55,120
And I think I'm going to try to hit a happy medium where we don't go to either extreme

23
00:01:55,120 --> 00:02:02,600
because the verses have a great benefit to them in providing a good example of how the

24
00:02:02,600 --> 00:02:06,720
teachings can be applied and how they apply to our lives.

25
00:02:06,720 --> 00:02:12,320
They provide us with encouragement when we hear about people practicing good things and

26
00:02:12,320 --> 00:02:19,360
help us to adjust ourselves when we hear about the results of bad things and so on.

27
00:02:19,360 --> 00:02:25,640
And they help put it into context to some extent.

28
00:02:25,640 --> 00:02:30,600
We should not let the stories dictate the context of the verse because even though they

29
00:02:30,600 --> 00:02:38,000
do provide some context, it's clear that the Buddha was not meaning to apply the verse

30
00:02:38,000 --> 00:02:40,680
only to the context in which it was taught.

31
00:02:40,680 --> 00:02:48,000
The teaching came as a result of a certain event or occurrence in the Buddha's time is true

32
00:02:48,000 --> 00:02:55,800
but the teaching of course is much more broad and deep often in the circumstance allows

33
00:02:55,800 --> 00:02:57,360
for.

34
00:02:57,360 --> 00:03:02,360
So today I'm going to give the first verse and this is of the Yamakawaga which is the

35
00:03:02,360 --> 00:03:04,360
first group of verses.

36
00:03:04,360 --> 00:03:12,680
It's the Dhamapada 123 verses is separated into different sections.

37
00:03:12,680 --> 00:03:16,280
So this is the first verse of the first section.

38
00:03:16,280 --> 00:03:26,960
The verse goes in Pali, Manopubangama dhamma, Manosita, Manomia, Manasati, Padutina, Manavasati

39
00:03:26,960 --> 00:03:35,080
wa prativa, tatun nangdukmanvati, takun wa wa hato padaam.

40
00:03:35,080 --> 00:03:38,840
This is the Pali, the translation.

41
00:03:38,840 --> 00:03:53,560
Manopubangama dhamma, all dhammas are preceded by the mind, all things, all reality, everything

42
00:03:53,560 --> 00:03:56,920
is preceded by the mind.

43
00:03:56,920 --> 00:04:02,680
Manosita, Manomia, they are governed by the mind and made by the mind.

44
00:04:02,680 --> 00:04:13,640
All things, this is the Buddhist words.

45
00:04:13,640 --> 00:04:23,880
Manasati, Padutina, if with a impure mind, Vasati wa karate wa, one acts or speaks.

46
00:04:23,880 --> 00:04:31,680
If one acts or speaks with an impure mind, tatun nangdukmanvati, takun wa wa hato padaam.

47
00:04:31,680 --> 00:04:40,080
Nothing follows there from just as the wheel of the kart follows the ox that pulls it.

48
00:04:40,080 --> 00:04:45,080
So if an ox is pulling a kart, the wheel has to follow the ox.

49
00:04:45,080 --> 00:04:51,240
There's no way that it can't stop, it can't change its course.

50
00:04:51,240 --> 00:04:53,080
It will have to follow after the foot.

51
00:04:53,080 --> 00:04:57,600
So on the path, you will see the footprint of the ox and you will see the footprint of

52
00:04:57,600 --> 00:05:03,280
the kart following it always, because as long as the ox is pulling the kart.

53
00:05:03,280 --> 00:05:09,120
In the same way, when we act or speak, if our heart is impure, suffering will follow

54
00:05:09,120 --> 00:05:14,880
just as the kart follows the ox, that's the same course.

55
00:05:14,880 --> 00:05:24,160
So this verse was given in relation to the venerable elder monk, takun bala.

56
00:05:24,160 --> 00:05:28,880
And pala, takun means I, so pala and pala means guardian.

57
00:05:28,880 --> 00:05:32,520
So his name means one who guards his I.

58
00:05:32,520 --> 00:05:37,200
And this is a name he was given, his original name was actually pala.

59
00:05:37,200 --> 00:05:43,600
But the story goes that he became ordained under the Buddha a little bit later in life.

60
00:05:43,600 --> 00:05:46,840
And so he didn't spend so much time studying.

61
00:05:46,840 --> 00:05:50,800
He stayed five years with the Buddha to do the basic training as a monk, but after that

62
00:05:50,800 --> 00:05:56,480
he has permission to go off and practice in the forest with some of his pala monks.

63
00:05:56,480 --> 00:06:05,160
And he spent three months of the rain season in the forest and he made a determination

64
00:06:05,160 --> 00:06:09,320
not to do lying, not to lie down for three months.

65
00:06:09,320 --> 00:06:15,000
So he undertook this practice to only do walking, standing and sitting.

66
00:06:15,000 --> 00:06:18,920
And so he would do walking meditation, he would do sitting meditation, he would do standing

67
00:06:18,920 --> 00:06:19,920
meditation.

68
00:06:19,920 --> 00:06:22,320
He would never lie down, not for three months, no sleeping.

69
00:06:22,320 --> 00:06:28,960
So it just means unless he would sleep sitting up or not, sitting up by accident.

70
00:06:28,960 --> 00:06:38,800
This is a practice that monks will undertake when they develop and they have progressed

71
00:06:38,800 --> 00:06:43,360
in their meditation or become proficient and confident in their practice.

72
00:06:43,360 --> 00:06:45,280
So he did this for three months.

73
00:06:45,280 --> 00:06:51,680
And during this time, he developed a sick disease of the eyes, a sickness in his eyes.

74
00:06:51,680 --> 00:06:55,920
And this doctor told him that he had to take this medicine and he said he would have

75
00:06:55,920 --> 00:07:02,240
to lie down to take this medicine and something like put it in his nose or something

76
00:07:02,240 --> 00:07:08,200
some ancient diabetic cure.

77
00:07:08,200 --> 00:07:12,360
And now Jacobali, he didn't say yes, he didn't say no, he took the medicine and he went

78
00:07:12,360 --> 00:07:13,360
home.

79
00:07:13,360 --> 00:07:16,920
He went back to the monastery and he thought to himself, you know, what should I do?

80
00:07:16,920 --> 00:07:22,040
He said, well, I've given this vow that I'm not going to lie down and I really want

81
00:07:22,040 --> 00:07:28,520
to carry out my vow and to really exert myself in the practice.

82
00:07:28,520 --> 00:07:32,840
And so as a result, he sat up and he kind of took the medicine sitting up but he didn't

83
00:07:32,840 --> 00:07:34,480
lie down.

84
00:07:34,480 --> 00:07:36,800
As a result, his sickness didn't get better.

85
00:07:36,800 --> 00:07:41,760
He had been back cut worse and he went back on arms round and the doctor came up to him

86
00:07:41,760 --> 00:07:47,600
and asked him, how is the sickness getting better and he said, oh, and the wind is still

87
00:07:47,600 --> 00:07:48,600
hurting my eyes.

88
00:07:48,600 --> 00:07:51,920
And he said, well, did you lie down to take the medicine and he said, and he didn't

89
00:07:51,920 --> 00:07:52,920
say anything.

90
00:07:52,920 --> 00:07:57,400
He just stood there and the doctor said, sure, you have to take the, you have to lie

91
00:07:57,400 --> 00:07:58,400
down to take it.

92
00:07:58,400 --> 00:08:01,640
And he said, well, thank you and he went away.

93
00:08:01,640 --> 00:08:05,320
The doctor started to get suspicious and so he went back to the monastery and followed

94
00:08:05,320 --> 00:08:12,800
after the elder and went and looked at his dwelling and saw that there was no bedding.

95
00:08:12,800 --> 00:08:14,680
And he said, venerable sir, where is your bed?

96
00:08:14,680 --> 00:08:19,240
And he said, and he didn't say anything and he said, venerable sir, you can't do this.

97
00:08:19,240 --> 00:08:21,640
You're going to go blind if you do this.

98
00:08:21,640 --> 00:08:24,680
You have to take care of your eyes.

99
00:08:24,680 --> 00:08:28,920
You need them if you want to be a monk, you're going to give them a little lecture.

100
00:08:28,920 --> 00:08:33,640
And the monk said, thank you but I will know what to do by myself, I'll figure out what

101
00:08:33,640 --> 00:08:34,640
to do by myself.

102
00:08:34,640 --> 00:08:39,320
And so the doctor said, fine, but don't tell anyone, then you don't, you tell anyone

103
00:08:39,320 --> 00:08:44,880
that I was the one who, who cured you or who gave you the medicine and don't tell them

104
00:08:44,880 --> 00:08:48,520
that I don't want to have anything to do with you.

105
00:08:48,520 --> 00:08:50,720
And he said, yes, thank you.

106
00:08:50,720 --> 00:08:51,960
And so the doctor went away.

107
00:08:51,960 --> 00:08:56,880
So, Chakupala or Pala, he sat down and he thought and he said, well, what do I do?

108
00:08:56,880 --> 00:09:00,800
I can either take care of my eyes or I can take care of my practice.

109
00:09:00,800 --> 00:09:08,560
I can either guard the physical body or I can guard the truth in the center of myself.

110
00:09:08,560 --> 00:09:13,480
If I don't, if I don't take this medicine, if I don't take it properly, then I might

111
00:09:13,480 --> 00:09:14,480
lose my eyes.

112
00:09:14,480 --> 00:09:16,120
My eyes will be ruined.

113
00:09:16,120 --> 00:09:24,000
And he said, but these eyes, these ears, this body, this self, this thing that I cling

114
00:09:24,000 --> 00:09:27,880
to, eventually it will all be ruined, it will all fall apart.

115
00:09:27,880 --> 00:09:33,720
He said, why would, why should I base my life on the well-being of something physical?

116
00:09:33,720 --> 00:09:38,800
And he said, it's much more important that I should guard the dumb, I should guard the

117
00:09:38,800 --> 00:09:42,040
truth, than that I should guard the physical body.

118
00:09:42,040 --> 00:09:45,880
And so he didn't line down and he didn't take the medicine and he continued on with

119
00:09:45,880 --> 00:09:48,200
his practice.

120
00:09:48,200 --> 00:09:50,520
As a result of this, two things happened.

121
00:09:50,520 --> 00:09:56,520
The first is he lost his eyes and the second is that he gained his eye.

122
00:09:56,520 --> 00:09:58,720
This is how the text goes.

123
00:09:58,720 --> 00:10:04,200
As he was doing walking meditation, at the same moment his eyes got worse and worse and

124
00:10:04,200 --> 00:10:05,200
worse.

125
00:10:05,200 --> 00:10:13,980
And finally, they deteriorated or something changed, something switched off, his eyes suddenly

126
00:10:13,980 --> 00:10:17,320
became useless and his physical eye.

127
00:10:17,320 --> 00:10:21,800
And at that same moment, as this was happening and as he was watching it and worrying

128
00:10:21,800 --> 00:10:26,280
about it and looking at his worrying and looking at his clinging to the body and letting

129
00:10:26,280 --> 00:10:33,320
it go and seeing the suffering inherent in this clinging mind, wanting and liking the

130
00:10:33,320 --> 00:10:40,400
fact that he can see in his eyes and the self and the ego and so on, seeing the suffering.

131
00:10:40,400 --> 00:10:42,080
This is the suffering.

132
00:10:42,080 --> 00:10:46,480
He saw the portable truth, he saw suffering and the cause of suffering.

133
00:10:46,480 --> 00:10:50,960
And when he saw that he let go and realized the cessation of suffering.

134
00:10:50,960 --> 00:10:58,240
So at the same moment, he saw the truth and he lost his vision forever, forever, his

135
00:10:58,240 --> 00:11:00,440
physical vision.

136
00:11:00,440 --> 00:11:04,960
He became an aura hunt at the same time.

137
00:11:04,960 --> 00:11:09,080
So at the end of the rain season after his practice and after he helped out the rest of

138
00:11:09,080 --> 00:11:14,600
the other monks to become an able to practice correctly as well, he made his way back to

139
00:11:14,600 --> 00:11:20,320
the Buddha and spent some time back at the Buddha at the monastery with the Buddha was

140
00:11:20,320 --> 00:11:21,320
staying.

141
00:11:21,320 --> 00:11:29,320
I believe in saw with him in India and in Jaitawana, the great monastery in the Buddha.

142
00:11:29,320 --> 00:11:32,520
And while he was there, of course, there were other monks taking care of him and met

143
00:11:32,520 --> 00:11:40,080
him and monks who he would teach and they would look after him physically and many visiting

144
00:11:40,080 --> 00:11:41,080
monks.

145
00:11:41,080 --> 00:11:46,360
And his name got around as a fairly proficient teacher, people thought that the rumor went

146
00:11:46,360 --> 00:11:48,640
that he was enlightened.

147
00:11:48,640 --> 00:11:55,240
And so monks would come to visit him one day, one night in the middle of the night he

148
00:11:55,240 --> 00:11:59,640
was doing walking meditation and he would do the walking meditation outside.

149
00:11:59,640 --> 00:12:01,640
And it was rain, it had rained.

150
00:12:01,640 --> 00:12:06,560
So it had rained heavily all night and then in the early morning, 3 a.m. or so on, he got

151
00:12:06,560 --> 00:12:09,040
up to do walking meditation.

152
00:12:09,040 --> 00:12:14,920
Now those times in India and even now in here in Sri Lanka and Thailand, or they have

153
00:12:14,920 --> 00:12:20,680
these, something like a termite or an ant that it's really the most useless animal in

154
00:12:20,680 --> 00:12:21,680
the world.

155
00:12:21,680 --> 00:12:26,560
They don't know how they managed to survive because they die in droves, they fly around

156
00:12:26,560 --> 00:12:33,480
and they lose their wings and then they just lie there and die, it seems like.

157
00:12:33,480 --> 00:12:41,760
And these insects were coming in the night, they come when it rains because their layers

158
00:12:41,760 --> 00:12:46,880
get flooded and so they were recovering this walking path and it took a while, it came

159
00:12:46,880 --> 00:12:51,120
out in the morning after the rains on the 3 a.m. and so on and started doing walking

160
00:12:51,120 --> 00:12:52,440
meditation.

161
00:12:52,440 --> 00:12:57,000
And as he was doing walking meditation, many of these insects died, as he was walking back

162
00:12:57,000 --> 00:12:58,000
and forth.

163
00:12:58,000 --> 00:13:06,960
He did no idea that they were there and they died, many, many of these insects were squashed.

164
00:13:06,960 --> 00:13:11,360
Even in the morning these monks came to see him, to meet him and said, where is the

165
00:13:11,360 --> 00:13:12,360
venerable Jekyll Bala?

166
00:13:12,360 --> 00:13:15,600
This is the name he was given.

167
00:13:15,600 --> 00:13:19,240
And the monks who looked at him said, that's his monastery, that's his cookie over there.

168
00:13:19,240 --> 00:13:25,760
So they went over to look and they saw this walking path that was covered with his footprints

169
00:13:25,760 --> 00:13:33,400
stepping on these termites or these ants or whatever they are.

170
00:13:33,400 --> 00:13:38,640
And these monks were terribly offended and they thought, this is how could this be an enlightened

171
00:13:38,640 --> 00:13:46,520
monk who is here engaging in one-ton slaughter of these innocent creatures.

172
00:13:46,520 --> 00:13:51,120
And so they were very offended and they went to see the Buddha and they said, this

173
00:13:51,120 --> 00:13:55,320
is right, this monk is, he should be taught how to practice correctly, how could he

174
00:13:55,320 --> 00:14:02,320
be an elder and still not know the correct, how wrong it is to kill.

175
00:14:02,320 --> 00:14:06,320
And this is where the Buddha gave this teaching which actually becomes a part of a very

176
00:14:06,320 --> 00:14:12,880
famous or the very important part of the Buddha's teaching is the teaching, the Buddha's

177
00:14:12,880 --> 00:14:18,960
teaching of karma which actually denies the efficacy of karma.

178
00:14:18,960 --> 00:14:23,360
So people say the Buddha taught the theory of karma, in fact you can say the Buddha taught

179
00:14:23,360 --> 00:14:28,880
against the theory of karma because the Buddha said, my son, Taghupala is not guilty

180
00:14:28,880 --> 00:14:34,680
of anything, he's innocent and then he said, bonopobangamadamam, the mind proceeds all

181
00:14:34,680 --> 00:14:35,920
down.

182
00:14:35,920 --> 00:14:39,320
The mind is what leads to suffering.

183
00:14:39,320 --> 00:14:44,960
If you act or speak with an impure mind, that's where suffering belongs.

184
00:14:44,960 --> 00:14:49,000
So the Buddha took this verse, this verse was given in a negative context.

185
00:14:49,000 --> 00:14:53,280
The point was not to say that suffering is going to, in the positive sense of suffering

186
00:14:53,280 --> 00:14:58,320
will lead to this, you're saying, if your mind doesn't have those things, then it can't

187
00:14:58,320 --> 00:14:59,800
lead to suffering.

188
00:14:59,800 --> 00:15:06,520
So it's the fact that karma cannot lead to unpleasant results.

189
00:15:06,520 --> 00:15:11,920
And this is an incredibly profound statement, I think, because it's not something that

190
00:15:11,920 --> 00:15:18,200
we would think of ourselves, if someone gets hurt because of something we do, then we

191
00:15:18,200 --> 00:15:23,040
think of ourselves as guilty, we feel bad and if we don't feel bad maybe the first person

192
00:15:23,040 --> 00:15:27,280
is angry at us and they can get angry at us whether we meant to do it or not.

193
00:15:27,280 --> 00:15:34,480
But the point that the Buddha is making here really shows the emphasis on the meditation

194
00:15:34,480 --> 00:15:43,040
practice, how the Buddha's teaching is really a practice of meditation and of contemplation.

195
00:15:43,040 --> 00:15:44,040
Why?

196
00:15:44,040 --> 00:15:50,920
Because we don't understand things in terms of beings, in terms of concepts, things that

197
00:15:50,920 --> 00:15:53,840
we think of that person and I hurt them and so on.

198
00:15:53,840 --> 00:15:58,200
So I think of it in terms of actual reality in the experience.

199
00:15:58,200 --> 00:16:01,600
So maybe the person does get angry at me for something that I did.

200
00:16:01,600 --> 00:16:05,560
Maybe these monks got angry at Chakupala and therefore they think, well, that was bad karma

201
00:16:05,560 --> 00:16:08,160
because it made people angry at him.

202
00:16:08,160 --> 00:16:12,520
But at the same time you can say, well, Chakupala doesn't face him at all, really.

203
00:16:12,520 --> 00:16:18,480
He had no bad intentions towards the insects, he had no bad intentions towards these monks.

204
00:16:18,480 --> 00:16:25,000
If they get angry at him, it's really water off a duck's back or it's like the Buddha

205
00:16:25,000 --> 00:16:32,120
said like a mustard seed on the needle or water off of a lotus leaf.

206
00:16:32,120 --> 00:16:34,440
It doesn't stick.

207
00:16:34,440 --> 00:16:39,240
Because his mind is pure, because his mind doesn't have any of his clinging, any of his anger.

208
00:16:39,240 --> 00:16:43,640
Whereas on the other hand, if he did have anger, if he did want bad intentions and if

209
00:16:43,640 --> 00:16:48,280
he didn't tend to kill these insects, that is what would lead him to suffering and that's

210
00:16:48,280 --> 00:16:50,000
what makes it unethical.

211
00:16:50,000 --> 00:16:56,360
An act is not unethical, as I've said before, simply because it fits into a category.

212
00:16:56,360 --> 00:16:58,600
Like killing is unethical or so on.

213
00:16:58,600 --> 00:17:02,840
Killing is only unethical because of the mind states that are required to kill in order

214
00:17:02,840 --> 00:17:04,960
to intentionally kill something.

215
00:17:04,960 --> 00:17:09,000
You have to give rise to a harmful and actually a perverted mind state.

216
00:17:09,000 --> 00:17:14,040
One that wants to, you know, doesn't want to die oneself and the end yet wants to cause

217
00:17:14,040 --> 00:17:16,520
harm to others.

218
00:17:16,520 --> 00:17:22,720
So the mind is of ultimate importance and this shows, as I said, shows the emphasis

219
00:17:22,720 --> 00:17:26,920
on meditation because it's only through meditation that we can affect the mind.

220
00:17:26,920 --> 00:17:30,640
And mind is, the mind is what is affected through meditation.

221
00:17:30,640 --> 00:17:37,000
As we practice meditation, we see the clinging, we see the craving, we see our distress

222
00:17:37,000 --> 00:17:45,840
that is caused by the impure mind and we come to differentiate and we come to affect the

223
00:17:45,840 --> 00:17:48,360
change on our minds.

224
00:17:48,360 --> 00:17:51,800
When we see things as they are, when we're doing walking meditation or we're doing

225
00:17:51,800 --> 00:17:58,440
sitting meditation, we learn to experience life in an interactive rather than a reactive

226
00:17:58,440 --> 00:18:04,040
way so that when we see and we hear and we smell and we experience things, we're able

227
00:18:04,040 --> 00:18:06,400
to experience them as an experience.

228
00:18:06,400 --> 00:18:09,160
So someone yells at us, we experience it as a sound.

229
00:18:09,160 --> 00:18:11,960
Someone hits it, hits us, we experience it as a feeling.

230
00:18:11,960 --> 00:18:15,480
We don't think of the person, we don't cling, we don't hold on.

231
00:18:15,480 --> 00:18:21,480
As we've seen the suffering, and this is what the Buddha was referring to, the mind that

232
00:18:21,480 --> 00:18:27,840
clings, the mind that is impure, the mind that is stress, has stress in it, this is what

233
00:18:27,840 --> 00:18:29,320
leads to suffering.

234
00:18:29,320 --> 00:18:35,760
Suffering only comes from the mind, all of the things that we create, all of the things

235
00:18:35,760 --> 00:18:44,080
that we do, you only have an influence on our minds if we cling to them, if we have some

236
00:18:44,080 --> 00:18:49,920
attachment in the mind at that moment, and this is a warning from the Buddha, that anything

237
00:18:49,920 --> 00:18:55,760
that we do with an impure mind will have this influence on our lives, an influence on

238
00:18:55,760 --> 00:19:01,440
our minds, it will lead to greater stress in our minds, greater clinging, greater suffering,

239
00:19:01,440 --> 00:19:05,240
just as the cart follows through the foot of the ox.

240
00:19:05,240 --> 00:19:09,320
So this is the meaning of the verse and the Buddha said, in fact, all things come from

241
00:19:09,320 --> 00:19:15,000
the mind, if you act with an impure mind, it will lead you to suffering, if you want

242
00:19:15,000 --> 00:19:19,880
to know where suffering comes from, this is where it and all other things come from,

243
00:19:19,880 --> 00:19:24,920
from the mind, and this is the first verse of the Dhamma Bhadeh, so I'd like to thank

244
00:19:24,920 --> 00:19:29,120
you for tuning in, and I hope that we will have many more of these, and I'll be able

245
00:19:29,120 --> 00:19:37,000
to get through all 423 verses before my time is up.

246
00:19:37,000 --> 00:19:39,400
So thank you for tuning in again, I wish you all the best.

