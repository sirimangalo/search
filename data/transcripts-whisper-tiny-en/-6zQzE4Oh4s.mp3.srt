1
00:00:00,000 --> 00:00:07,360
Here are asks in western countries there is a tendency for having small families, for example

2
00:00:07,360 --> 00:00:13,960
one or two children, so let's say that offspring or dain as monks or nuns, eventually

3
00:00:13,960 --> 00:00:20,080
for example the parents become ill, due to the sophisticated medical services, whatever

4
00:00:20,080 --> 00:00:25,240
physical problems can be temporary deferred over the years, however it leaves them in

5
00:00:25,240 --> 00:00:31,160
a vulnerable state, so to what extent a monks or nuns permitted to practically aid their

6
00:00:31,160 --> 00:00:36,960
vulnerable parents, for example as a duty, presumably there are some boundaries which are

7
00:00:36,960 --> 00:00:43,960
not suitable for someone who is ordained, so in some cases one may be left with a dilemma,

8
00:00:43,960 --> 00:00:49,960
not to practically help beyond a certain limit or disrobe, a tricky scenario, especially

9
00:00:49,960 --> 00:01:05,040
if one is not long ordained, but not to help may invite condemnation from society.

10
00:01:05,040 --> 00:01:15,440
We are allowed to help our parents, there are certainly some things that we can't do,

11
00:01:15,440 --> 00:01:25,640
so if I would go home and help my mother, if she ever needs it, I could go and do it

12
00:01:25,640 --> 00:01:34,200
without any problem, but there will be things like I could do things like helping her

13
00:01:34,200 --> 00:01:41,280
at home, I could even go so far and cook her meals, although I probably couldn't eat

14
00:01:41,280 --> 00:01:54,560
them myself then, what I could not do is go shopping for her, for example, or that

15
00:01:54,560 --> 00:02:09,440
should be solved otherwise, but I think there are solutions to do, but there is no problem,

16
00:02:09,440 --> 00:02:22,200
but the problem is more in the mind of the person who has to go and help or not and

17
00:02:22,200 --> 00:02:35,120
being afraid of the condemnation of society or not being willing to leave the seclusion

18
00:02:35,120 --> 00:02:41,120
of a forest or a monastery to go and help.

19
00:02:41,120 --> 00:02:50,760
But I think the Buddha was very clear that we can help and that the parents have a very

20
00:02:50,760 --> 00:02:59,000
high priority in a person's life, so if it's possible, we should always consider to do

21
00:02:59,000 --> 00:03:02,360
it.

22
00:03:02,360 --> 00:03:10,800
And I think, especially relating back to the earlier question about doing jobs for lay

23
00:03:10,800 --> 00:03:17,600
people, it might, I think it has to be said that there might come a situation where the

24
00:03:17,600 --> 00:03:21,320
person feels that they have to disrobe to take care of their parents.

25
00:03:21,320 --> 00:03:27,800
This happens a lot in Thailand, so I would say the number two reason, or it's up there

26
00:03:27,800 --> 00:03:32,320
anyway, the reasons why monks disrobe, it's even a reason why, as you say, long-term

27
00:03:32,320 --> 00:03:34,760
monks, they do actually disrobe.

28
00:03:34,760 --> 00:03:43,880
And I know one person in particular who was ordained for 17 years, I think, and then left

29
00:03:43,880 --> 00:03:45,840
to take care of his mother.

30
00:03:45,840 --> 00:03:50,440
And it's often an excuse that's used by monks because they don't really want to be

31
00:03:50,440 --> 00:03:56,560
a monk, but they don't have any, they don't want to just say, they feel like they're trapped

32
00:03:56,560 --> 00:04:00,840
in it, so they need a really good excuse and they use their parents as an excuse.

33
00:04:00,840 --> 00:04:04,880
And in his case, it's quite clear that that was the reason why he disrobe, because now

34
00:04:04,880 --> 00:04:07,800
he collects recycled items.

35
00:04:07,800 --> 00:04:13,400
He goes picking through garbage and looking for broken things, you know, discard in bottles

36
00:04:13,400 --> 00:04:22,960
by this side of the road, and he lives very much like Gattakarano, Gattakarada, the potter,

37
00:04:22,960 --> 00:04:30,680
who looked after his mother for the same reason, it was an anagarika, it was an anagami,

38
00:04:30,680 --> 00:04:34,160
and lived at home with his mother, making pots.

39
00:04:34,160 --> 00:04:39,920
So this guy, in Thailand, he really does live that way, and he was quite impressive to me,

40
00:04:39,920 --> 00:04:46,040
impossible to not be humbled by his state of being, that he lives very much like a monk,

41
00:04:46,040 --> 00:04:51,960
and he cooks and he makes food for his parents, and he eats cold rice when he gets home

42
00:04:51,960 --> 00:04:59,920
from his daily work, and he makes just enough money to take care of himself and his

43
00:04:59,920 --> 00:05:09,800
mother, and spends the rest of his time ostensibly in meditation and study and seclusion.

44
00:05:09,800 --> 00:05:15,520
But there, so that's one way, and you can clearly see that in the scriptures with Gattakar,

45
00:05:15,520 --> 00:05:21,280
he got the karik, Gattakarano, he looked after his mother in this way, but there are other

46
00:05:21,280 --> 00:05:27,160
examples, there's one example of a monk who looked after his parents as a monk.

47
00:05:27,160 --> 00:05:34,160
His parents were very rich, and he went forth as a monk, thinking that they would be well-taken

48
00:05:34,160 --> 00:05:42,440
care of, and the story goes that they slowly, slowly became destitute, and eventually

49
00:05:42,440 --> 00:05:50,040
lost their homes and became beggars in the same town where he had lived, and one time

50
00:05:50,040 --> 00:06:02,480
he was going back on Armstrong, and walked through the city, and suddenly saw his parents

51
00:06:02,480 --> 00:06:08,560
begging for food on the same streets, and they were, I think, blind, barely seen, barely

52
00:06:08,560 --> 00:06:15,000
seen, so they didn't recognize him, but he saw them, and he cried so much, watch looking

53
00:06:15,000 --> 00:06:22,040
at his parents in such a destitute condition, and just was totally in anguish and unsure

54
00:06:22,040 --> 00:06:26,520
of what to do because it was love for them, but his also his love for Buddhism, and his

55
00:06:26,520 --> 00:06:38,560
love for the monastic life, and so many days later the monk started noticing that he

56
00:06:38,560 --> 00:06:46,960
became very, very thin, and so some of them started to think, well, here he's living

57
00:06:46,960 --> 00:06:51,520
very close to the Buddha, and yet he's not able to get enough arms, it's quite amazing

58
00:06:51,520 --> 00:06:58,240
especially with so many devout followers of the Buddha who are so keen to support the monks

59
00:06:58,240 --> 00:07:02,480
with the requisites, and so they followed him on Armstrong, and they saw that what he did

60
00:07:02,480 --> 00:07:06,440
is went through the city getting it, collecting as much arms as he could, brought it back

61
00:07:06,440 --> 00:07:13,120
and fed his parents, and whatever was left to aid himself, and so as a result he wasn't

62
00:07:13,120 --> 00:07:22,200
getting adequate nutrition, or he was getting less nutrition than was ideal, and so they

63
00:07:22,200 --> 00:07:27,240
went right away, went back to the Buddha, and taddled on him, and said that here's this monk

64
00:07:27,240 --> 00:07:31,720
acting in this way, what are we going to do with him, and the Buddha said, call him to

65
00:07:31,720 --> 00:07:36,680
me, and so they called the monk to come to see the Buddha, and the Buddha said, is it true

66
00:07:36,680 --> 00:07:42,480
that you're acting in this way, and he said, yes, it's true, and of course he's so much

67
00:07:42,480 --> 00:07:47,120
in anguish because here the Buddha is going to tell something, maybe tell him not to do

68
00:07:47,120 --> 00:07:52,680
it, and the Buddha said, very good, well done, Buddha said, I did the same one, I have done

69
00:07:52,680 --> 00:07:57,520
the same myself in past lives, and he told us tale of the past, we're here done the

70
00:07:57,520 --> 00:08:06,320
same thing, so even to the extent of giving the point here is that if it's possible, there

71
00:08:06,320 --> 00:08:20,920
are many ways that you can overcome the conflict, so depending on conditions, if you are

72
00:08:20,920 --> 00:08:28,800
able to go on Armstrong, now if your parents live in a place like America, in rural America

73
00:08:28,800 --> 00:08:38,160
somewhere, you probably wouldn't be able to go live with them, and go on Armstrong, so

74
00:08:38,160 --> 00:08:42,600
you might have to resort to shopping, and so if you're an only child, and if they're in

75
00:08:42,600 --> 00:08:48,000
a society that doesn't take care of people and they're old and so on, so it may be the

76
00:08:48,000 --> 00:08:51,520
case, and I don't think, I think one answer to the question is that it may be necessary

77
00:08:51,520 --> 00:08:58,480
that they do have to disrobe, but this also points to the fact that one should not be

78
00:08:58,480 --> 00:09:06,640
negligent in one's course of action, one should be keeping some sort of tabs on one's

79
00:09:06,640 --> 00:09:13,040
parent, every year going back and seeing them or contacting them once a year, and making

80
00:09:13,040 --> 00:09:19,760
sure that they're all right, but also trying to bring them closer to where you're at,

81
00:09:19,760 --> 00:09:23,160
because there are cases where monks actually invite their parents to come and live with

82
00:09:23,160 --> 00:09:30,320
them in the monastery and take care of them or come to live near them in Thailand, for

83
00:09:30,320 --> 00:09:35,040
example, there are monks who will bring their parents to come and live with them in the

84
00:09:35,040 --> 00:09:45,280
monastery and take care of them when they're old, which, interesting, you may have some

85
00:09:45,280 --> 00:09:51,120
conflict there in terms of the monastic structure, but at least find some way to arrange

86
00:09:51,120 --> 00:09:55,560
your life and help them arrange their life so that you are able to take care of them and

87
00:09:55,560 --> 00:10:01,920
so that you don't get to that position, because it's really fairly rare where there isn't

88
00:10:01,920 --> 00:10:07,120
enough support for the parents to take care of themselves, if it happens, you have to take

89
00:10:07,120 --> 00:10:11,600
it as it goes, and you could consider that it simply means that you don't have the required

90
00:10:11,600 --> 00:10:19,360
perfections to stay as a monk, because I wouldn't recommend ignoring the fact that your

91
00:10:19,360 --> 00:10:26,080
parents are in a bad condition, and if that means that you'd have to disrobe, then you live

92
00:10:26,080 --> 00:10:31,680
your life like a monk as best you can and take care of your parents, which is a great thing.

