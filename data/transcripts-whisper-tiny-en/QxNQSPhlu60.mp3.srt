1
00:00:00,000 --> 00:00:14,000
Okay, good evening everyone.

2
00:00:14,000 --> 00:00:24,680
Welcome to our Saturday dumb session.

3
00:00:24,680 --> 00:00:30,680
Tonight we're looking at the fetters.

4
00:00:30,680 --> 00:00:38,680
In the sutte that we're looking at is called the Mahamam kya sutte.

5
00:00:38,680 --> 00:00:44,680
Because the focus of the sutte is...

6
00:00:44,680 --> 00:00:50,680
There are the premp, the introduction of the sutte is focused on a monk named Malunkiyahu.

7
00:00:50,680 --> 00:01:04,680
It seems to have been a bit of a failure as a meditator and as a Buddhist.

8
00:01:04,680 --> 00:01:19,680
The sutte before this one he has all sorts of questions about the world and the world eternal as the world infinite and so on.

9
00:01:19,680 --> 00:01:30,680
Lots of speculative questions and the Buddha was quite hard on him for that.

10
00:01:30,680 --> 00:01:39,680
In this sutte he appears that the commentary says he holds this view that.

11
00:01:39,680 --> 00:01:46,680
It's an important one to address that.

12
00:01:46,680 --> 00:01:53,680
The fetters, when we talk about fetters,

13
00:01:53,680 --> 00:01:59,680
a fetter is something that keeps you tied.

14
00:01:59,680 --> 00:02:06,680
Something that ties you, something that binds you.

15
00:02:06,680 --> 00:02:14,680
And what the Buddha means by this, we mean by this in Buddhism

16
00:02:14,680 --> 00:02:26,680
or those things that shackle us to problems keep us tied to suffering.

17
00:02:26,680 --> 00:02:39,680
When we're in the find ourselves in a situation where the results aren't what we want,

18
00:02:39,680 --> 00:02:51,680
we lose something we want, we like love, we become connected with something that we don't like.

19
00:02:51,680 --> 00:02:54,680
Something bad happens to us.

20
00:02:54,680 --> 00:02:59,680
While we're stuck, we're tied to it.

21
00:02:59,680 --> 00:03:08,680
We strive to find a way to free ourselves from the suffering.

22
00:03:08,680 --> 00:03:13,680
And there's so much of it, since some people's lives that it can feel overwhelming,

23
00:03:13,680 --> 00:03:17,680
that we really are in a prison.

24
00:03:17,680 --> 00:03:19,680
For many of us, life seems fine.

25
00:03:19,680 --> 00:03:25,680
It seems quite comfortable that there are certainly,

26
00:03:25,680 --> 00:03:33,680
at least we can see it certainly possible to become quite imprisoned by suffering.

27
00:03:33,680 --> 00:03:41,680
You know, someone who is a victim of a gun violence in the US, in America,

28
00:03:41,680 --> 00:03:47,680
there's so many people being shot and children shooting other children.

29
00:03:47,680 --> 00:03:54,680
And then if you're a parent who's lost your child from that, it can be crippling.

30
00:03:54,680 --> 00:03:59,680
People who are deeply in debt, poverty,

31
00:03:59,680 --> 00:04:12,680
countries in the world where they are hunted or are in constant fear of their safety or their lives.

32
00:04:12,680 --> 00:04:16,680
Life can be a lot of stress and suffering.

33
00:04:16,680 --> 00:04:25,680
In Puerto Rico, where there was a great disaster and loss of

34
00:04:25,680 --> 00:04:31,680
the basic necessities of life and all the suffering there.

35
00:04:31,680 --> 00:04:50,680
So the Buddha talks about fetters, as the things pointing out, trying to find the essence of what is really keeping us tied to suffering, keeping us bound to it.

36
00:04:50,680 --> 00:04:56,680
And so he has a list of ten, and I think it's a useful list.

37
00:04:56,680 --> 00:05:01,680
It's one of the core teachings of Buddhism.

38
00:05:01,680 --> 00:05:05,680
And this is where he goes over the first five.

39
00:05:05,680 --> 00:05:12,680
So the thing about Malunkia, is Malunkia had this idea that one is only fettered,

40
00:05:12,680 --> 00:05:22,680
one is only tied, one is only imprisoned when these things arise in the mind.

41
00:05:22,680 --> 00:05:25,680
At other times one is free.

42
00:05:25,680 --> 00:05:39,680
So what that means is that an ordinary person living their ordinary life is pure.

43
00:05:39,680 --> 00:05:49,680
And we only give rise, we only become imprisoned when we give rise to,

44
00:05:49,680 --> 00:05:55,680
we turn out to be states of mind, the fetters in Buddhism are not actually your experiences.

45
00:05:55,680 --> 00:06:01,680
It's not because you lost something you loved, it's because you loved the thing that you lost.

46
00:06:01,680 --> 00:06:24,680
And by loved we don't mean love, we are attached, we needed it, we required it in order to be happy.

47
00:06:24,680 --> 00:06:39,680
And so the Buddha explained, in Buddhism we see the ordinary state of a human being is one that is caught up with these fetters.

48
00:06:39,680 --> 00:06:54,680
And it's because they're based on not an active arising of mental illness for lack of better order, the problem in the mind.

49
00:06:54,680 --> 00:07:03,680
But they come about because of ignorance, that our ordinary state has some problems that we have to remove.

50
00:07:03,680 --> 00:07:13,680
So Buddhism isn't about avoiding bad states of mind, it's about improving our understanding really.

51
00:07:13,680 --> 00:07:26,680
Ultimately gaining wisdom, wisdom about reality that we don't have, naturally that isn't something we're born with.

52
00:07:26,680 --> 00:07:40,680
And this becomes quite clear in the first fetters, or the first fetter is what he calls sakei and deity, personality belief, the view that we have a personality.

53
00:07:40,680 --> 00:07:50,680
That's not something you sometimes take up, it's something that's quite ingrained in our psyche.

54
00:07:50,680 --> 00:07:59,680
You know, I know who I am, I am this sort of person and it's me.

55
00:07:59,680 --> 00:08:19,680
And this is a fetter in Buddhism because this is where ego comes from, this is where conceit comes from, it's where arrogance, it's where possessiveness comes from.

56
00:08:19,680 --> 00:08:28,680
We have so many ideas about the self that somehow self-worth is important.

57
00:08:28,680 --> 00:08:36,680
We recognize that someone who has low self-esteem is going to be prone to suffering.

58
00:08:36,680 --> 00:08:48,680
So we try to address that and say, well, you have to be confident, you know, you have to love yourself, you have to find, you have to think that you're special and worth something.

59
00:08:48,680 --> 00:08:56,680
As I've said before, you know, our philosophy is more, we're all worthless, really.

60
00:08:56,680 --> 00:09:01,680
Which is a very hard thing to say and to hear.

61
00:09:01,680 --> 00:09:06,680
But I can justify saying it.

62
00:09:06,680 --> 00:09:19,680
I mean, I can justify it immediately by pointing to the emotion that gives rise to. When I say we're all worthless, you're worthless, don't think you're spent.

63
00:09:19,680 --> 00:09:24,680
How upsetting that becomes and off-putting.

64
00:09:24,680 --> 00:09:31,680
And the reason is because we have a view of self, we have a personality belief in it.

65
00:09:31,680 --> 00:09:36,680
It's just one example of the many ways that we're vulnerable.

66
00:09:36,680 --> 00:09:39,680
We're vulnerable to suffering because of our belief.

67
00:09:39,680 --> 00:09:54,680
Because of our, this really deeply rooted primary primal perception, conception of reality.

68
00:09:54,680 --> 00:10:06,680
And reality is really quite simple. There is experience. I have no qualms about claiming, making claims about what reality is.

69
00:10:06,680 --> 00:10:17,680
It's quite simple. They're seeing, that's clear, there's hearing, smelling, there's tasting, there's feeling, and there's thinking.

70
00:10:17,680 --> 00:10:21,680
Those things exist. Those are real.

71
00:10:21,680 --> 00:10:35,680
But the personality, the eye, there's two things that are wrong with it.

72
00:10:35,680 --> 00:10:46,680
First of all, it's problematic. As I said, it's the consequences of having a view of self, having a sense of self.

73
00:10:46,680 --> 00:10:56,680
I would lean to slow self-esteem. I mean, you can't have low self-esteem if you don't have any concern about self.

74
00:10:56,680 --> 00:11:07,680
You can see it and arrogance, possessiveness.

75
00:11:07,680 --> 00:11:19,680
But on a deeper level, it just has nothing to do with reality. And it really takes you out of sync with reality.

76
00:11:19,680 --> 00:11:30,680
Because who we are is really just a complex sort of jumble of habit.

77
00:11:30,680 --> 00:11:40,680
So to say, I am this sort of person or that sort of person. If you want to be more technically accurate, you can say, I've developed this habit.

78
00:11:40,680 --> 00:11:50,680
And so that's a common occurrence for me. Maybe I have been very generous in my life or you've been a kind person.

79
00:11:50,680 --> 00:12:02,680
And so because of those actions and those experiences coming up repeatedly, that's not what you call a part of your personality.

80
00:12:02,680 --> 00:12:08,680
But technically, it's not a personality, an ego, and a soul of self.

81
00:12:08,680 --> 00:12:15,680
It's just the results of a habit. Maybe you're a person who thinks a lot, reads a lot.

82
00:12:15,680 --> 00:12:26,680
So when you're sitting in meditation, clearly it's because of the habit. I think a lot when you meditate, trying to focus and instead you're distracted.

83
00:12:26,680 --> 00:12:40,680
If you're a person who engages in great centrality, then you might be sitting thinking about food or thinking about romance or any number of things.

84
00:12:40,680 --> 00:13:01,680
Many meditators will hear music in their head because they're fond of listening to music at home.

85
00:13:01,680 --> 00:13:21,680
So the idea that we have a self, the idea of the personality, is quite contrary to what's actually happening, which is actually real.

86
00:13:21,680 --> 00:13:47,680
And that is experience, moment to moment, experience of seeing and hearing and so on. And habits that are based on these. So reaction, seeing and then liking the seeing or disliking the seeing, pain and then disliking the pain and the creation of habits, such that a little bit of pain is, for example, quite disturbing to us.

87
00:13:47,680 --> 00:14:09,680
You know, falling in love with someone, a lover or a child, and cultivating that detachment and then the results and causes and effects.

88
00:14:09,680 --> 00:14:26,680
So this is what you actually see. You actually can observe in reality the existence of habits, habit cultivation and the results of those habits, responses, reactions and the consequences.

89
00:14:26,680 --> 00:14:45,680
But you never find a me, a mine, a self. If you look and if you study, if you really are honest and take a look at yourself, you find that there is nothing really that you could call a self.

90
00:14:45,680 --> 00:15:03,680
And so it isn't so much that you come to view, there is no self, there is no personality, there is no soul. It's that you come to realize that that way of looking at things has nothing to do with what's really going on, which is, there's much more complex, on the one hand complex.

91
00:15:03,680 --> 00:15:20,680
Oh, it's the word, yeah, complex arrangement of very simple things, which is experience, the reaction to experience, which is just more experience.

92
00:15:20,680 --> 00:15:23,680
So that's the first one.

93
00:15:23,680 --> 00:15:37,680
So the idea that we're worthless just means that there is no thing in there to be worth anything. It's not that you should feel, you should hate yourself. That would be equally silly.

94
00:15:37,680 --> 00:15:42,680
So there's nothing there to find, if you look at experience, experiences come and they go.

95
00:15:42,680 --> 00:16:02,680
And so the consequences of seeing reality from an experiential point of view are non-attachment or that you're free, you're liberated from that. There's nothing that comes that you feel bad about and you feel sad about, nothing that goes that you feel a loss.

96
00:16:02,680 --> 00:16:20,680
It's not seen in terms of me and mine. I mean, this is all intellectual, I'm providing information for your intellect, but this is what you see through the meditation practice.

97
00:16:20,680 --> 00:16:37,680
You bring your mind to a very simple state, it's just an ordinary state. Here we are, sitting here, seeing, hearing, smelling, tasting, feeling, thinking, that's real.

98
00:16:37,680 --> 00:16:53,680
It's the nature. There is no self or soul in nature.

99
00:16:53,680 --> 00:17:22,680
And that's the first thread. I'll just go through these. I won't give the details of the suit exactly, but it's basically what he's talking about. The second feather is doubt, doubt about, particularly doubt about this, about suffering.

100
00:17:22,680 --> 00:17:47,680
Let's put it very simply, doubt about suffering, doubt about suffering just means that really uncertainty, uncertainty about suffering, uncertainty about what is suffering, and what that means is you just don't know.

101
00:17:47,680 --> 00:18:03,680
What it means is there are certain things that we strive for and cling to that in fact having obtained and attained make us suffer, bring us stress.

102
00:18:03,680 --> 00:18:12,680
We do things, we act in ways that hurt us and hurt others. Why do we get angry?

103
00:18:12,680 --> 00:18:26,680
Well, the argument is that we wouldn't, if we knew that anger was stressful and a cause for suffering, and not just up here, but if we knew in our hearts that it was a problem, we would never get angry.

104
00:18:26,680 --> 00:18:45,680
So a person who gets angry is someone who doesn't understand suffering. It's like a person who kicks, they stub their foot, or they bump into something and then they get angry and kick it. That person doesn't understand that the table or the chair isn't really responsible for your suffering.

105
00:18:45,680 --> 00:19:01,680
It isn't going to benefit, or more seriously, if someone does say something bad and then you hit them, or you attack them verbally, such a person doesn't understand suffering.

106
00:19:01,680 --> 00:19:15,680
It's those verbal and physical acts are not the way to find happiness. They're a cause for suffering. They'll create guilt, they'll create fear, they'll create turmoil in your life.

107
00:19:15,680 --> 00:19:32,680
They'll create a bad rebirth, someone who is mean to other people, cruel to others, someone who is manipulative or deceitful, dishonest.

108
00:19:32,680 --> 00:19:48,680
And the same goes with addiction. When we talk about addiction to things like food, people who are seriously addicted to food, there's great suffering. And on the one hand, they say, well, they know it's wrong, so why are they still doing it?

109
00:19:48,680 --> 00:20:06,680
This is really the point to this different kind of knowing. You don't really know that it's wrong. You can say, I know that eating ice cream is wrong, I'm still doing it, but I know it's wrong, why can't I not stop?

110
00:20:06,680 --> 00:20:19,680
Because eating ice cream is not the experience. The experience is the taste and you don't really understand those tastes. If I asked you, it's the taste of ice cream, happiness.

111
00:20:19,680 --> 00:20:44,680
You would say, most likely say, yes, a person, especially a person who is addicted to it, that's the problem. There's another problem is that you don't understand that it's not really happiness. And it's quite simple to show. It's not simple to see and to really understand and believe, have a firm conviction.

112
00:20:44,680 --> 00:21:00,680
Because that takes a repeated seeing and to finally say, okay, I get it. But it's easy to show you. If you just sit down and be mindful and then you maybe take a ice cream and practice a technique to try and be very mindful while eating ice cream.

113
00:21:00,680 --> 00:21:12,680
You can see there's really nothing there. There's nothing of happiness there. It's hard thing to even conceive of, but that can be so. I mean, take pain, for example.

114
00:21:12,680 --> 00:21:30,680
Pain is a bad thing, right? But there's nothing about pain that is bad. And if you learn the technique of mindfulness and you come to see this, you realize that pain is nothing wrong with pain.

115
00:21:30,680 --> 00:21:36,680
I mean, not more than anything else.

116
00:21:36,680 --> 00:21:47,680
And so doubt is this uncertainty. And so again, it's not something that you give rise to. It's not when you're sitting here and saying, is he right or is he wrong?

117
00:21:47,680 --> 00:21:54,680
It's the lack of certainty. And so this is something that we all have.

118
00:21:54,680 --> 00:22:13,680
Even when we, if we haven't, for those people who haven't cultivated mindfulness, it's something that it's in everyone. And it goes away as you, as you follow the path to freedom.

119
00:22:13,680 --> 00:22:24,680
And it is freedom. Once you have no doubt, once you have certainty, and it's a certainty that is based completely on experience. And that would be the only way.

120
00:22:24,680 --> 00:22:29,680
Because if you have complete certainty, that ice cream is something that makes you happy.

121
00:22:29,680 --> 00:22:51,680
We'd argue that you can't have complete certainty because it's not based on this deep, clear, real experience. If you actually experience ice eating ice cream as it's happening, moment by moment by moment, you can't maintain the attachment. It just never arises.

122
00:22:51,680 --> 00:23:03,680
There never arises the desire, the wanting, even the liking of the ice cream, which might seem kind of drab and bland, but it's actually such a relief.

123
00:23:03,680 --> 00:23:16,680
In actuality, if you experienced that state, it's so relieving and freeing and peaceful. It's quite the opposite of bland and drab.

124
00:23:16,680 --> 00:23:34,680
If there were no more ice cream, what a horrible world ran. It would be so wonderful if people were not so ice cream is apparently a very bad food. If you didn't know, one monk told me that it's apparently the worst thing you can possibly eat.

125
00:23:34,680 --> 00:23:45,680
I don't know. I mean, I think it's just the most fattening thing or something like that or for diabetes or anything. The point, that's not really the point.

126
00:23:45,680 --> 00:24:03,680
The point being they're our attachment to things like ice cream. That's a very good example because it's an easy one to imagine or to envision the great desire and liking and feeling of happiness that we have.

127
00:24:03,680 --> 00:24:16,680
You know, kids eating the ice cream can't tell how happy they are. We wonder why they fight and why they cry and they're not actually all that happy.

128
00:24:16,680 --> 00:24:30,680
The more ice cream you give them, it doesn't actually make them cry less, fight less. Act up less.

129
00:24:30,680 --> 00:24:40,680
That's the second one doubt about suffering, doubt about the way to become free from suffering. How do you free yourself?

130
00:24:40,680 --> 00:24:55,680
If you haven't done the work to cultivate mindfulness, if you haven't come to understand and put into practice mindfulness, you'll always have this uncertainty because from our point of view, there's no other way.

131
00:24:55,680 --> 00:25:04,680
If you're not mindful of reality, you're never going to become free from suffering.

132
00:25:04,680 --> 00:25:16,680
It's number two, number three. Number three is related to number two, but it's wrong practice.

133
00:25:16,680 --> 00:25:38,680
Yeah. Practice outside of the path. And what that means is we're never free until you become enlightened. Let's put it clearly.

134
00:25:38,680 --> 00:25:47,680
You're never free from the potential to practice the wrong way. And so in a worldly sense, this means doing the wrong thing in your life.

135
00:25:47,680 --> 00:26:06,680
It really means, well, if you act again in ways that create suffering, but it really refers to your religious practice, your spiritual practice.

136
00:26:06,680 --> 00:26:24,680
It's possible to be caught up in ritual. The ritual isn't a bad thing. It's not bad to light candles or incense or bow to the Buddha or something like that that they do in Buddhist country.

137
00:26:24,680 --> 00:26:46,680
But it's quite wrong to think that such things are going to lead to enlightenment. That in and of themselves, some ritual act like there are some places that have these prayer wheels blowing in the wind and they're turning in the wind.

138
00:26:46,680 --> 00:27:00,680
And the idea is because that prayer wheel is turning, something good happens. You're not even doing it. You're not even doing the act. Other places, you know, they have these prayer wheels that they turn in their hands or they turn with their hands.

139
00:27:00,680 --> 00:27:21,680
And the idea is that it's reciting the Buddha for you. The discourse is being recited because it's turning or something like that. And it, well, it's true that such acts have make a connection.

140
00:27:21,680 --> 00:27:41,680
I suppose you had this suit that we're studying tonight and you put it on a wheel and we spun it and we said, here I'm spinning this. Now, when you do that, you might feel great confidence and you know, great conviction and great happiness and great appreciation for the teaching and you're doing it because you appreciate the teaching so much.

141
00:27:41,680 --> 00:27:59,680
And all of that is, you could say, a part of right practice. But that's quite different from having this belief, this view, this idea that those practices that the actual turning and the actual activity.

142
00:27:59,680 --> 00:28:15,680
I mean, we have examples in our sculpture like knocking on wood. If you think knocking on wood is going to affect the outcome of your life besides give you pain, knuckles.

143
00:28:15,680 --> 00:28:27,680
I think that's what's being talked about here. I mean, quite clearly that's what the Buddha meant here. There's two aspects of it.

144
00:28:27,680 --> 00:28:41,680
Some people refrain from certain activities like, you know, the religions that refrain from, like my grandmother has plates for milk and meat.

145
00:28:41,680 --> 00:28:49,680
So if you have milk, if you ever have milk on this plate, you can never have meat on that plate.

146
00:28:49,680 --> 00:28:59,680
And we once went and we once had a pizza at her apartment and that pepperoni on it and she got a special knife and she kept that knife in a special place for us.

147
00:28:59,680 --> 00:29:09,680
So that if we ever came back for a pizza, I don't know if she kept that knife, but we couldn't use any of her cutlery for it.

148
00:29:09,680 --> 00:29:20,680
And there are other religious teachings that refrain from certain things that it's not necessary to refrain from.

149
00:29:20,680 --> 00:29:28,680
There's many examples, refrain from working on a Saturday, for example.

150
00:29:28,680 --> 00:29:38,680
But of course they have very different views, but all these ideas in India that if you stood on one leg,

151
00:29:38,680 --> 00:29:42,680
you would need to some kind of spiritual enlightenment.

152
00:29:42,680 --> 00:29:56,680
It might gain you a great fortitude of body and mind on the one hand, but the actual standing on one leg is probably not going to make you more enlightened than not.

153
00:29:56,680 --> 00:30:02,680
And the other side is, oh sorry, that's the other side is the actual acts doing things.

154
00:30:02,680 --> 00:30:21,680
And there's any number of these. But a person who has, so this is an ordinary for all of us were subject to this potential until we come to see the truth and then we're freed from it because we have no illusion about what is the right way to practice.

155
00:30:21,680 --> 00:30:32,680
It's clear that, well this is reality, we have these experiences and these reactions and it's just our misunderstanding of how this system works that's causing us suffering.

156
00:30:32,680 --> 00:30:39,680
So that's the path, there's no mystery or doubt or uncertainty.

157
00:30:39,680 --> 00:30:55,680
There's no possibility or potential for the standing on one leg to be an influencing factor because it has nothing to do with this, with reality.

158
00:30:55,680 --> 00:31:08,680
Or bowing a thousand times, or it has to do with the state of your mind, so it's possible to do all these things mindfully and therefore that being the important part.

159
00:31:08,680 --> 00:31:17,680
So that's number three. Number four, sensual lust or desire, sensuality.

160
00:31:17,680 --> 00:31:31,680
Well number four and number five, sensual lust and ill will, these are the two opposites, really.

161
00:31:31,680 --> 00:31:46,680
So these two are, you could say based on things like identity view and doubts, uncertainty, ignorance.

162
00:31:46,680 --> 00:31:52,680
But these two, what we call liking and disliking, we put it really simply.

163
00:31:52,680 --> 00:32:04,680
These really make up the core of the problem, or the core of the problem as it is experienced by us.

164
00:32:04,680 --> 00:32:19,680
So personality view in and of itself, you could say, well, you know, one university that's having a conversation with someone and she said,

165
00:32:19,680 --> 00:32:24,680
what's wrong with personality? Actually two people now have asked me that.

166
00:32:24,680 --> 00:32:32,680
And it's true, it seems quite innocuous. Yes, I have a view of self and so on.

167
00:32:32,680 --> 00:32:41,680
And so in and of itself, personality doesn't believe this view of personality, it doesn't seem like a problem.

168
00:32:41,680 --> 00:32:51,680
The problem is when reality rears its ugly head and it's nothing like what you thought, like suddenly your mind changes.

169
00:32:51,680 --> 00:32:57,680
Suddenly your possessions disappear or no longer under your control.

170
00:32:57,680 --> 00:33:05,680
Suddenly your ego is threatened by someone who says, you're this, you're that, insults you.

171
00:33:05,680 --> 00:33:15,680
Look at insults or even criticisms or manipulations, gaslighting and all these sorts of things.

172
00:33:15,680 --> 00:33:25,680
Look at how vulnerable we are, how sensitive we are, how we always feel, where we feel like we're being judged.

173
00:33:25,680 --> 00:33:32,680
We can feel like we're not measuring up or like we have to measure up or it's kind of competition and all these things.

174
00:33:32,680 --> 00:33:36,680
When that happens, of course, then there's liking and disliking.

175
00:33:36,680 --> 00:33:41,680
If I praise you and say you all, there's such wonderful people in your way.

176
00:33:41,680 --> 00:33:44,680
If you care what I say, then you feel happy.

177
00:33:44,680 --> 00:33:57,680
But then if on the other hand, I say you're all worthless and all, you feel threatened.

178
00:33:57,680 --> 00:34:07,680
So liking and disliking are what would really cause the problems, it's just that they have their basis and things like personality view.

179
00:34:07,680 --> 00:34:10,680
So these ones, these are natural, right?

180
00:34:10,680 --> 00:34:15,680
We can say, well, everybody has them. They're a part of nature.

181
00:34:15,680 --> 00:34:18,680
It's who we are. Yes, that's true.

182
00:34:18,680 --> 00:34:22,680
Buddhism doesn't claim to make your ordinary.

183
00:34:22,680 --> 00:34:32,680
It doesn't claim to be the ordinary state. When it claims that our ordinary state is kind of messed up, it's not perfect.

184
00:34:32,680 --> 00:34:38,680
We have this romanticized view of things like nature.

185
00:34:38,680 --> 00:34:51,680
Well, if you look at nature, it's really some kind of hell because wild animals don't really live in peace.

186
00:34:51,680 --> 00:35:00,680
The cruelty in nature, maybe not on par with the cruelty in the human realm.

187
00:35:00,680 --> 00:35:08,680
No, certainly not. There's not enough intelligence to be that cruel.

188
00:35:08,680 --> 00:35:16,680
But the violent nature and the animal realm shows us.

189
00:35:16,680 --> 00:35:25,680
And even just the physical natural realm of diseases and sicknesses and all this.

190
00:35:25,680 --> 00:35:30,680
I mean, you look no farther than mosquitoes to see it.

191
00:35:30,680 --> 00:35:49,680
The world is not all roses and sugar and spice and all things.

192
00:35:49,680 --> 00:36:09,680
So the point is not to romanticize this, even this earth because it's quite a small part of the universe to look at the broader picture.

193
00:36:09,680 --> 00:36:15,680
The human realm, there's nothing special about it, like somehow this is the way things should be.

194
00:36:15,680 --> 00:36:23,680
We have that sense in the West, anyway, because of culturally we've generally brought up.

195
00:36:23,680 --> 00:36:30,680
Well, our culture has very strong, theistic roots.

196
00:36:30,680 --> 00:36:36,680
And so, of course, theists believe that sort of thing.

197
00:36:36,680 --> 00:36:53,680
But there's no reason other than that, other than I believe in God to believe in the supremacy of the way things are that we were created in God's image, for example.

198
00:36:53,680 --> 00:37:01,680
And so disregarding that, we really just ask ourselves some of it, some of nature could be quite good.

199
00:37:01,680 --> 00:37:07,680
You know, things like love and compassion are certainly good and those are natural.

200
00:37:07,680 --> 00:37:14,680
But just because it's not good because it's natural, it's good because it's good because it leans to happiness.

201
00:37:14,680 --> 00:37:17,680
Truly leans to peace and happiness.

202
00:37:17,680 --> 00:37:22,680
And so that's what we aim to find and greed and anger do not lean to peace and happiness.

203
00:37:22,680 --> 00:37:29,680
There are things that we could in some ideal state be better off without.

204
00:37:29,680 --> 00:37:39,680
And I think for the most part, there's just a sense that we could never be free from them.

205
00:37:39,680 --> 00:37:46,680
And the great thing about this path, the practice, is that you certainly can be free from them.

206
00:37:46,680 --> 00:37:49,680
If you practice properly.

207
00:37:49,680 --> 00:37:56,680
There are only reactions, there are moments of experience where you judge something.

208
00:37:56,680 --> 00:38:04,680
Maybe you like something I'd say and you feel this kind of liking of it.

209
00:38:04,680 --> 00:38:11,680
Or maybe you dislike something as if there's a disliking of it.

210
00:38:11,680 --> 00:38:15,680
Maybe you're sitting here and you feel so happy to be in this place.

211
00:38:15,680 --> 00:38:23,680
And then when you get home, you'll be depressed because, oh, you've got to go back to your home life, your ordinary life.

212
00:38:23,680 --> 00:38:30,680
On the other hand, maybe you're sitting here when is he going to stop talking and you're suffering.

213
00:38:30,680 --> 00:38:34,680
Either way, the point is these lead to suffering.

214
00:38:34,680 --> 00:38:37,680
I mean, disliking, displeasure is suffering already.

215
00:38:37,680 --> 00:38:46,680
There's no question that when anger ill will disliking of any kind arises, it's stressful.

216
00:38:46,680 --> 00:38:49,680
It's something we would like to avoid.

217
00:38:49,680 --> 00:38:55,680
But liking is just the same. It's only because of liking certain things that we fall into suffering.

218
00:38:55,680 --> 00:39:02,680
Because we like, ultimately, what we actually like is experiences.

219
00:39:02,680 --> 00:39:16,680
And when experiences are different from that, it's when there comes the stress.

220
00:39:16,680 --> 00:39:31,680
If there were no liking, no partiality, no wanting or craving, there would be no disappointment or loss or sadness or stress.

221
00:39:31,680 --> 00:39:34,680
Or even the worry of losing the things that you still have.

222
00:39:34,680 --> 00:39:37,680
The worry about attaining things that haven't come yet.

223
00:39:37,680 --> 00:39:42,680
The worry about death and old age sickness when you're not sick.

224
00:39:42,680 --> 00:39:52,680
We wouldn't be worried if we didn't have a liking of being alive and healthy.

225
00:39:52,680 --> 00:40:01,680
So it turns out that this liking and disliking, this desire and aversion is not really all that useful.

226
00:40:01,680 --> 00:40:03,680
It's a part of who we are.

227
00:40:03,680 --> 00:40:10,680
And this is not something that I expect ordinary people to believe, ordinary people who aren't.

228
00:40:10,680 --> 00:40:14,680
Don't subscribe to this idea to believe, but it's not a challenge.

229
00:40:14,680 --> 00:40:23,680
The challenge is my claim or our claim that this is what you will see if you just be mindful.

230
00:40:23,680 --> 00:40:27,680
You don't have to believe or agree with anything, I'd say.

231
00:40:27,680 --> 00:40:34,680
But this is our claim of being a description of what you actually will see if you look.

232
00:40:34,680 --> 00:40:44,680
If you're just objectively for yourself observe experience, this is what you'll see.

233
00:40:44,680 --> 00:40:47,680
So that's really what we aim for.

234
00:40:47,680 --> 00:40:49,680
These are the five lower fetters.

235
00:40:49,680 --> 00:40:56,680
There are five more that are a little more, they're along the same lines, but they're a little more refined.

236
00:40:56,680 --> 00:41:05,680
In fact, I've talked sort of mentioned tangentially several of them, probably all of them.

237
00:41:05,680 --> 00:41:08,680
But basing that's what the suit is about.

238
00:41:08,680 --> 00:41:14,680
He talks about, there's a little more to it, but I'm not going to really recommend this one to read.

239
00:41:14,680 --> 00:41:22,680
I just recommend them all to read, but this one I think is quite a good one.

240
00:41:22,680 --> 00:41:29,680
He talks about going into states of meditative calm and then seeing things as impermanence suffering.

241
00:41:29,680 --> 00:41:35,680
He goes and gives a really detailed, not just impermanence suffering in non-self.

242
00:41:35,680 --> 00:41:44,680
Which means seeing things that we cling to or seeing reality is not being worth clinging to.

243
00:41:44,680 --> 00:42:02,680
Seeing that of those things that we try to fix or we try to maintain, we try to obtain, we try to hold the reality,

244
00:42:02,680 --> 00:42:09,680
underlying all of that is not anything worth holding or clinging to or maintaining.

245
00:42:09,680 --> 00:42:16,680
And this act of maintaining is not beneficial.

246
00:42:16,680 --> 00:42:25,680
Anyway, he goes into detail, I'm not going to go into screen detail, but if you're interested, I definitely recommend reading it.

247
00:42:25,680 --> 00:42:47,680
But for now that's the dumber for tonight, so thank you all for tuning in, have a good night.

