1
00:00:00,000 --> 00:00:06,000
I heard in Tibetan Buddhism, the soul will remain in Bado after death.

2
00:00:06,000 --> 00:00:18,000
In what form or manner is this entity after it detaches from the body at death perceives the physical reality?

3
00:00:18,000 --> 00:00:25,000
Yeah, I don't think that grammar is not so perfect.

4
00:00:25,000 --> 00:00:34,000
I mean, we can criticize that, but it's hard to understand exactly what's being asked there.

5
00:00:34,000 --> 00:00:44,000
Perceived that what manner is it perceived or what manner does it perceive the physical reality?

6
00:00:44,000 --> 00:00:54,000
Well, first of all, I'm not a Tibetan Buddhist, so I'm not going to comment exactly, and it may not be the exact answer to the question.

7
00:00:54,000 --> 00:01:00,000
But I don't actually think that Tibetan Buddhists believe in a soul.

8
00:01:00,000 --> 00:01:09,000
I'm not positive on that, but I would highly doubt it that the Tibetan Buddhists do believe such a thing.

9
00:01:09,000 --> 00:01:20,000
What they believe is that being continues, or the existence, it continues in what they call the Bardo.

10
00:01:20,000 --> 00:01:25,000
Now, maybe they do have the idea that there is a soul there.

11
00:01:25,000 --> 00:01:30,000
I'm sure some people, of course, many Buddhists do have attachment to the soul until you become a sword upon them.

12
00:01:30,000 --> 00:01:37,000
You still have the potential to cling to, secondly, a deity, cling to self.

13
00:01:37,000 --> 00:01:41,000
But the truth of what's going on there is there is experience.

14
00:01:41,000 --> 00:01:46,000
There's the arising and ceasing of seeing, hearing, smelling, taste, and thinking.

15
00:01:46,000 --> 00:01:50,000
In Theravada Buddhism, we might call that a ghost.

16
00:01:50,000 --> 00:01:55,000
And I know in Tibetan Buddhism, they say, 49 days is the time that they stay in the Bardo.

17
00:01:55,000 --> 00:02:02,000
I think in Theravada Buddhism, we're not so clear on the 49 days, but it may be the truth.

18
00:02:02,000 --> 00:02:11,000
There's, you know, people have actually kind of verified this that after, on the 50th day, the ghost comes and says goodbye or something like that.

19
00:02:11,000 --> 00:02:13,000
We would just say it's a ghost.

20
00:02:13,000 --> 00:02:15,000
The point is that it's not in between.

21
00:02:15,000 --> 00:02:20,000
It's not an underabawa in an ultimate sense.

22
00:02:20,000 --> 00:02:22,000
It's not between existences.

23
00:02:22,000 --> 00:02:25,000
It is an existence in and of itself.

24
00:02:25,000 --> 00:02:44,000
If the person who dies, if the experience continues on locally, or even remotely, or in whatever location, without the arising of a course material body,

25
00:02:44,000 --> 00:02:48,000
we consider that to be a Baba in and of itself.

26
00:02:48,000 --> 00:02:54,000
It's an existence of being as continued.

27
00:02:54,000 --> 00:03:01,000
And whether, how long it lasts, it might last a short time, it might last a long time.

28
00:03:01,000 --> 00:03:17,000
Usually, I would say, depending on the karma, if the person has done the karma to lead them to be a ghost for a long time, then they might stay in that state for longer than 49 days.

29
00:03:17,000 --> 00:03:23,000
But the Tibetans have interesting ideas about what that being perceives.

30
00:03:23,000 --> 00:03:28,000
Now, when they are getting ready to go on to a neck in the next life, they will perceive all sorts of crazy stuff.

31
00:03:28,000 --> 00:03:33,000
They'll perceive, technically, they'll perceive one of three things.

32
00:03:33,000 --> 00:03:36,000
They'll perceive the...

33
00:03:36,000 --> 00:03:41,000
So up until this point, they would perceive reality kind of like an angel.

34
00:03:41,000 --> 00:03:50,000
They would perceive people, maybe flying around, like floating around, and they would see the people crying over their dead body, and they'd be unable to communicate with these people.

35
00:03:50,000 --> 00:03:57,000
Or they might go here, or go there, based on their attachment to their past lives, past life.

36
00:03:57,000 --> 00:04:01,000
But then at the moment, when they're going to move on, one of three things will come to them.

37
00:04:01,000 --> 00:04:06,000
And this, of course, can also come at the moment of death in the body.

38
00:04:06,000 --> 00:04:11,000
So without actually leaving the body, a person can go directly.

39
00:04:11,000 --> 00:04:17,000
Apparently, it can go directly to Heaven or Hell or wherever, based on the arising of three things.

40
00:04:17,000 --> 00:04:18,000
So whether it's in...

41
00:04:18,000 --> 00:04:23,000
Well, still in the body, or whether it's after having left the body, there will be one of three experiences.

42
00:04:23,000 --> 00:04:30,000
One, an experience or a remembrance of the karma that one has done.

43
00:04:30,000 --> 00:04:36,000
I get to talk a while back, I should probably do a video. I don't know that I've ever done a video on the 12 types of karma.

44
00:04:36,000 --> 00:04:43,000
But there are four types that are pertinent here.

45
00:04:43,000 --> 00:04:53,000
There is karma that is neata, the garukam, the karma that is sure.

46
00:04:53,000 --> 00:04:56,000
After you've done this karma, that's what's going to lead to the rebirth.

47
00:04:56,000 --> 00:05:01,000
So if you've killed your father, killed your mother, killed Anara hand, hurt the Buddha,

48
00:05:01,000 --> 00:05:06,000
or caused a schism in the sangha.

49
00:05:06,000 --> 00:05:12,000
If you've done any of these things, then that's what's going to come to you.

50
00:05:12,000 --> 00:05:21,000
You're going to remember the karma, or you're going to remember a symbol of the karma.

51
00:05:21,000 --> 00:05:26,000
So there's a kamanimita, then there's a kamanimita.

52
00:05:26,000 --> 00:05:29,000
So a symbol of the kamanimita, you might not remember the actual act,

53
00:05:29,000 --> 00:05:33,000
think of the actual act, but instead you'll think of something that symbolizes it.

54
00:05:33,000 --> 00:05:37,000
So if you killed someone, you might have a symbol of a night.

55
00:05:37,000 --> 00:05:40,000
You might see a knife, or you might see blood, or so.

56
00:05:40,000 --> 00:05:49,000
If you've done this, you'll see something that reminds you of it, or that is symbolic of it.

57
00:05:49,000 --> 00:05:55,000
Or third, you might see kamanimita, you might see an image of the way that you're going to go.

58
00:05:55,000 --> 00:06:01,000
So some people actually feel like they're flying up to heaven, or they're taking a trip.

59
00:06:01,000 --> 00:06:08,000
The kids who were reborn, kids who remember their past lives, will say they remember traveling to the house that they were born in.

60
00:06:08,000 --> 00:06:15,000
And they're standing outside of this house and suddenly pull their born in the womb of the woman inside the house.

61
00:06:15,000 --> 00:06:24,000
This is called kamanimita, so the person sees the destination, a vision or a dream of the destination.

62
00:06:24,000 --> 00:06:34,000
So if it's a garukama, if you've done any really, really bad deeds, it'll be one, it'll be based on this.

63
00:06:34,000 --> 00:06:40,000
If you did something particularly potent at the moment of death, then you're more likely to remember.

64
00:06:40,000 --> 00:06:45,000
But you haven't done a garukama, then you're likely to remember that.

65
00:06:45,000 --> 00:06:50,000
If there's nothing you've done particularly at the moment of death, and you haven't performed garukama,

66
00:06:50,000 --> 00:06:55,000
then you'll remember some karma that you did repeatedly.

67
00:06:55,000 --> 00:06:59,000
That was a ritual, a tinakama.

68
00:06:59,000 --> 00:07:09,000
Something that, so if you were a thief and you stole regularly, or if you were a murderer, a hunter, or a butcher, so on,

69
00:07:09,000 --> 00:07:13,000
then this is what would come back to you as one of these three things.

70
00:07:13,000 --> 00:07:18,000
Or one of these three things would be based on that karma.

71
00:07:18,000 --> 00:07:34,000
And if not, then it will be just some sort of karma that you performed, meaning some experience that had an effect on your mind that caused you to attach to it,

72
00:07:34,000 --> 00:07:39,000
like it, or dislike it, or so, and so was some sort of karma in that sense.

73
00:07:39,000 --> 00:07:42,000
So that's the last perception.

74
00:07:42,000 --> 00:07:53,000
Then at that moment, and Tibetan Buddhism, as I understand, they talk about this as well, how there will be an image or a vision come up,

75
00:07:53,000 --> 00:07:55,000
and you'll be attracted to that.

76
00:07:55,000 --> 00:08:01,000
If you're not mindful at that moment, you'll get sucked into it and be reborn based on it.

77
00:08:01,000 --> 00:08:09,000
So that's the process, now as far as the existence up to that time,

78
00:08:09,000 --> 00:08:18,000
well that's the moment of death passing on, or else at the moment of death, there's an out of body experience, like the bar dough.

79
00:08:18,000 --> 00:08:32,000
And then eventual continuing on to the next layer.

