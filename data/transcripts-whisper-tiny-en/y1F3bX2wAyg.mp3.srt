1
00:00:00,000 --> 00:00:16,200
Okay, next question.

2
00:00:16,200 --> 00:00:20,600
I had an experience with the abdomen or the rising and falling game to look like waves

3
00:00:20,600 --> 00:00:23,200
in the water, rising and falling.

4
00:00:23,200 --> 00:00:27,600
Is this trying to cool the demeditation to make such a resemblance?

5
00:00:27,600 --> 00:00:34,600
Yeah, it very well could be considered tranquility meditation.

6
00:00:34,600 --> 00:00:43,800
Technically, it's probably not because tranquility meditation is where you are developing

7
00:00:43,800 --> 00:00:45,600
tranquility based on an object.

8
00:00:45,600 --> 00:00:52,400
You're picking an object and you're trying to focus on it and gain tranquility.

9
00:00:52,400 --> 00:01:01,120
What it sounds like rather you're getting into is the result of meditation.

10
00:01:01,120 --> 00:01:07,000
So because of your practice of insight meditation or whatever meditation you're practicing,

11
00:01:07,000 --> 00:01:10,840
this comes up, this experience arises.

12
00:01:10,840 --> 00:01:18,040
So whether you're going to practice tranquility or insight depends on how you approach it.

13
00:01:18,040 --> 00:01:25,400
If you begin to look at the waves and you examine the waves and you're supposed to use

14
00:01:25,400 --> 00:01:30,400
the word rising and falling based on the waves and the wave rises and you see rising

15
00:01:30,400 --> 00:01:37,720
and wave falls and you see falling, then it could be tranquility.

16
00:01:37,720 --> 00:01:45,080
But if you look at the feeling as being a sensation and you see the peace and you see the

17
00:01:45,080 --> 00:01:51,600
tranquility for what it is, so you say to yourself, calm, calm, or you have this feeling

18
00:01:51,600 --> 00:02:01,800
of the wave and you say feeling, feeling, if you like it and you say liking, then it

19
00:02:01,800 --> 00:02:09,360
can be inside because you'll see that it's changing and it's not permanent and so on.

20
00:02:09,360 --> 00:02:15,040
The problem is that at a certain point in your practice, if you're practicing correctly,

21
00:02:15,040 --> 00:02:20,680
all of these sort of mystical experiences will arise.

22
00:02:20,680 --> 00:02:26,000
So some people will feel great bliss and rapture, some people will feel great calm and

23
00:02:26,000 --> 00:02:35,680
tranquility, some people will get seemingly profound insights and some people will get great

24
00:02:35,680 --> 00:02:40,400
faith or great energy or so on, many, many different things will have clarity of mind

25
00:02:40,400 --> 00:02:49,280
and all sorts of seemingly good things will come and as a result one will stop practicing.

26
00:02:49,280 --> 00:02:54,240
I've talked about this before, I think some of you have seen the video I did on what

27
00:02:54,240 --> 00:02:58,120
the meditation is not, and it's not all of these things.

28
00:02:58,120 --> 00:03:05,800
So the seeing the waves in the ocean is not meditating, but it comes from meditating for

29
00:03:05,800 --> 00:03:11,920
it to be meditation again, or the meditate, insight meditation, you have to focus on the

30
00:03:11,920 --> 00:03:16,480
reality of it, and often that's not enough because the mind is clinging to it, often

31
00:03:16,480 --> 00:03:43,600
you have to put it aside or tell your mind stop and so on.

