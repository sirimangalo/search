1
00:00:00,000 --> 00:00:07,000
Okay, good evening, everyone. Welcome to our evening demo session.

2
00:00:07,000 --> 00:00:36,000
One very well-known description of the Buddhist teaching.

3
00:00:38,000 --> 00:00:43,000
It is as the middle way.

4
00:00:43,000 --> 00:00:54,000
And so all of us are on the middle way, trying to find the middle way.

5
00:00:54,000 --> 00:01:00,000
Sometimes it's described in that way.

6
00:01:00,000 --> 00:01:13,000
Most famously, in terms of not torturing yourself, but not becoming indulgent.

7
00:01:13,000 --> 00:01:16,000
I think that applies very well to the meditation practice.

8
00:01:16,000 --> 00:01:22,000
It's something that is an important lesson for us during the meditation course.

9
00:01:22,000 --> 00:01:27,000
Don't torture yourself. Don't force it.

10
00:01:27,000 --> 00:01:32,000
Don't even force yourself not to force it, right?

11
00:01:32,000 --> 00:01:38,000
I realize that forcing a disappointment under your control.

12
00:01:38,000 --> 00:01:45,000
It says it's something that you can just steamroll over.

13
00:01:45,000 --> 00:01:48,000
Don't torture yourself.

14
00:01:48,000 --> 00:01:50,000
But at the same time, don't become complacent.

15
00:01:50,000 --> 00:01:55,000
And so it seems actually it eventually becomes a razor's edge.

16
00:01:55,000 --> 00:01:58,000
When there's only really one way, there's no leeway.

17
00:01:58,000 --> 00:02:03,000
If you want to really be in the middle way,

18
00:02:03,000 --> 00:02:11,000
a nupagama, you have to not go at all in either direction.

19
00:02:11,000 --> 00:02:19,000
So what we're looking for is this state of balance and a sense on a razor's edge.

20
00:02:19,000 --> 00:02:27,000
And balance is a really good way to understand the middle way as well.

21
00:02:27,000 --> 00:02:35,000
Now balance doesn't mean a little bit of indulgence and a little bit of torture.

22
00:02:35,000 --> 00:02:40,000
Balance means to find a state that is free from such things,

23
00:02:40,000 --> 00:02:45,000
like this state that is neither one direction or the other.

24
00:02:45,000 --> 00:02:49,000
It's a very tricky thing.

25
00:02:49,000 --> 00:02:54,000
But there are many types of balance that are talked about.

26
00:02:54,000 --> 00:03:07,000
I think many of you have read in my booklet where I'm related to the teaching on balancing the faculties.

27
00:03:07,000 --> 00:03:15,000
So confidence has to balance with wisdom, wisdom has to balance with confidence.

28
00:03:15,000 --> 00:03:19,000
Effort has to balance with concentration and vice versa.

29
00:03:19,000 --> 00:03:26,000
Mindfulness alone is the one you don't have to balance, you don't have to moderate.

30
00:03:26,000 --> 00:03:33,000
Mindfulness, the more mindfulness you have, the better.

31
00:03:33,000 --> 00:03:36,000
There's no middle way with mindfulness.

32
00:03:36,000 --> 00:03:39,000
This is the middle way.

33
00:03:39,000 --> 00:03:52,000
Because mindfulness is what allows you to avoid extremes, avoid an excess or a deficiency.

34
00:03:52,000 --> 00:03:59,000
We have so many different qualities of mind that the Buddha talks about.

35
00:03:59,000 --> 00:04:05,000
Sometimes you read it and you think, well, how do I go about developing that one?

36
00:04:05,000 --> 00:04:09,000
It's like you read about all the parts of a motor.

37
00:04:09,000 --> 00:04:15,000
And so you open the hood of your car and you try to figure out how to turn each one on.

38
00:04:15,000 --> 00:04:18,000
That's not how it works.

39
00:04:18,000 --> 00:04:24,000
It's important to know all the parts, to be able to fix them if they get broken, to clean them and so on.

40
00:04:24,000 --> 00:04:27,000
But you don't go around starting all the parts in the engine.

41
00:04:27,000 --> 00:04:30,000
You really just turn the key.

42
00:04:30,000 --> 00:04:35,000
And so here we have this great vehicle of insight meditation and so many

43
00:04:35,000 --> 00:04:46,000
adornments like loving kindness and compassion and generosity and wisdom, of course.

44
00:04:46,000 --> 00:04:49,000
But you don't really have to go around turning all those things on.

45
00:04:49,000 --> 00:04:52,000
Mindfulness is the one.

46
00:04:52,000 --> 00:04:59,000
Mindfulness is the key.

47
00:04:59,000 --> 00:05:02,000
Mindfulness is the one that balances.

48
00:05:02,000 --> 00:05:05,000
And you see that again in an important set.

49
00:05:05,000 --> 00:05:10,000
Something I wanted to really wanted to talk about tonight.

50
00:05:10,000 --> 00:05:18,000
Our local meditators are all doing really well on track.

51
00:05:18,000 --> 00:05:26,000
So I thought it was also time to talk about the seven Bojungas.

52
00:05:26,000 --> 00:05:30,000
Bojungas.

53
00:05:30,000 --> 00:05:35,000
The factors of enlightenment.

54
00:05:35,000 --> 00:05:40,000
The factors of enlightenment allow us without going into too much detail.

55
00:05:40,000 --> 00:05:44,000
We don't want to get too caught up in theory.

56
00:05:44,000 --> 00:05:47,000
But they can describe to you the path.

57
00:05:47,000 --> 00:06:03,000
They describe to you the progress and the change that comes to pass.

58
00:06:03,000 --> 00:06:06,000
So they start with set theme.

59
00:06:06,000 --> 00:06:10,000
The one way to understand them is as a progression.

60
00:06:10,000 --> 00:06:14,000
It starts with set theme, of course, with mindfulness.

61
00:06:14,000 --> 00:06:21,000
It starts when you begin to practice, when you begin to cultivate an objective awareness,

62
00:06:21,000 --> 00:06:31,000
seeing, hearing, feeling, thinking, liking, disliking.

63
00:06:31,000 --> 00:06:40,000
When you recognize it, remind yourself it is what it is.

64
00:06:40,000 --> 00:06:45,000
That's the key that starts you on the path, and that's what you've been doing, right?

65
00:06:45,000 --> 00:06:49,000
So as you do that, number two is dhamma vichaya.

66
00:06:49,000 --> 00:06:57,000
And this is sort of an investigation or realization of the dhammas.

67
00:06:57,000 --> 00:07:04,000
Which means as you practice, you come to understand yourself.

68
00:07:04,000 --> 00:07:10,000
You learn many things about yourself, about reality.

69
00:07:10,000 --> 00:07:18,000
You see clearly your habits, good and bad, and you see clearly that.

70
00:07:18,000 --> 00:07:28,000
The objects which you react to, liking, disliking, etc., arrogant, conceited, and so on.

71
00:07:28,000 --> 00:07:31,000
You see that they're not what you thought.

72
00:07:31,000 --> 00:07:37,000
You see that whatever you cling to or hold on to is impermanent, suffering, and non-soft.

73
00:07:37,000 --> 00:07:43,000
Dhamma vichaya is about seeing three characteristics.

74
00:07:43,000 --> 00:07:55,000
It's about changing our predilection for forced ability and our misguided notion that we can find stability.

75
00:07:55,000 --> 00:08:00,000
Experience something pleasant, and we cling to it, thinking, oh, this will last forever.

76
00:08:00,000 --> 00:08:07,000
We don't think that, but that's how we approach it, so that when it's gone we feel very sad.

77
00:08:07,000 --> 00:08:08,000
We suffer.

78
00:08:08,000 --> 00:08:13,000
We're suffering because it changes.

79
00:08:13,000 --> 00:08:14,000
And you can't control it.

80
00:08:14,000 --> 00:08:17,000
You're not in charge.

81
00:08:17,000 --> 00:08:29,000
You find meditators when they begin to practice, they'll try very much to control their practice.

82
00:08:29,000 --> 00:08:32,000
But you can't control.

83
00:08:32,000 --> 00:08:38,000
So you see non-self, impermanent, suffering, non-self.

84
00:08:38,000 --> 00:08:41,000
Because the second step, this is what you've already begun as well, right?

85
00:08:41,000 --> 00:08:43,000
You've already seen some of this.

86
00:08:43,000 --> 00:08:48,000
You're watching, you're learning about this as we go.

87
00:08:48,000 --> 00:08:53,000
Now as you see that the third one arises, which is media.

88
00:08:53,000 --> 00:09:03,000
Now really, I hear really comes after you start to see the truth because, in the beginning, you're withholding your doubt.

89
00:09:03,000 --> 00:09:14,000
And so are the, giving the benefit of the doubt.

90
00:09:14,000 --> 00:09:17,000
And so you're not 100% into it, right?

91
00:09:17,000 --> 00:09:19,000
Why would you be?

92
00:09:19,000 --> 00:09:24,000
Because as you practice and you start to see the results, see the benefits, see the truth.

93
00:09:24,000 --> 00:09:26,000
See the usefulness of this.

94
00:09:26,000 --> 00:09:35,000
The usefulness of training in equanimity and objectivity.

95
00:09:35,000 --> 00:09:47,000
And you begin to become energetic, enthusiastic, intent upon it.

96
00:09:47,000 --> 00:09:51,000
Yeah.

97
00:09:51,000 --> 00:09:57,000
After video, then there's PT.

98
00:09:57,000 --> 00:10:05,000
So once you start to really put out effort, it becomes powerful, right?

99
00:10:05,000 --> 00:10:09,000
Because your other habits are getting weaker because you're not engaging in the

100
00:10:09,000 --> 00:10:17,000
mind as so engrossed in the meditation, then it starts to become a habit.

101
00:10:17,000 --> 00:10:22,000
And that the energy there, the sort of the charge that builds up, the power that builds

102
00:10:22,000 --> 00:10:24,000
up is called PT.

103
00:10:24,000 --> 00:10:35,000
PT is this power, this static charge, if you will, the power of inertia.

104
00:10:35,000 --> 00:10:40,000
Once you get going, it starts to go by itself.

105
00:10:40,000 --> 00:10:42,000
So PT is of many kinds.

106
00:10:42,000 --> 00:10:50,000
Of course, PT is just any kind of ecstatic or charged state of mind.

107
00:10:50,000 --> 00:10:52,000
There's many of them.

108
00:10:52,000 --> 00:11:03,000
But here it means being enthusiastic and engaged and strong in your practice.

109
00:11:03,000 --> 00:11:13,000
Once you're engaged in the practice and really set upon it, then there arises

110
00:11:13,000 --> 00:11:15,000
busity, tranquility.

111
00:11:15,000 --> 00:11:19,000
So some of you have already talked about how you feel calm sometimes.

112
00:11:19,000 --> 00:11:20,000
That's good.

113
00:11:20,000 --> 00:11:23,000
Don't cling to it, obviously.

114
00:11:23,000 --> 00:11:28,000
And then when you've come to see is that when you cling to it, it doesn't make you

115
00:11:28,000 --> 00:11:29,000
more calm.

116
00:11:29,000 --> 00:11:37,000
I'm so sure.

117
00:11:37,000 --> 00:11:44,000
But it's good to realize that this is a benefit that you calm down.

118
00:11:44,000 --> 00:11:51,000
I'm going to try to force it, but you start to see that by just being aware and objective

119
00:11:51,000 --> 00:11:57,000
and allowing the distress and the restlessness and so on, and not reacting to it,

120
00:11:57,000 --> 00:12:01,000
how that in and of itself calms you down.

121
00:12:01,000 --> 00:12:03,000
You no longer have to be anything.

122
00:12:03,000 --> 00:12:12,000
You no longer react to things.

123
00:12:12,000 --> 00:12:15,000
After passadim, there becomes samadhi.

124
00:12:15,000 --> 00:12:21,000
So once it's quiet, then your mind becomes very focused.

125
00:12:21,000 --> 00:12:25,000
So you've been very energetic and intent upon it and quiet down.

126
00:12:25,000 --> 00:12:30,000
And once you quiet down, all of these, the mind starting with mindfulness,

127
00:12:30,000 --> 00:12:34,000
they work together to focus your attention.

128
00:12:34,000 --> 00:12:35,000
Your mind becomes focused.

129
00:12:35,000 --> 00:12:40,000
And when you walk, you're only aware of the right foot moving and then the left foot moving

130
00:12:40,000 --> 00:12:48,000
or lifting, placing, or lifting hands on.

131
00:12:48,000 --> 00:13:03,000
And so there arises samadhi, the mind is no longer restless.

132
00:13:03,000 --> 00:13:05,000
After samadhi, then there is a becca.

133
00:13:05,000 --> 00:13:07,000
And this is what we're aiming for.

134
00:13:07,000 --> 00:13:10,000
Those of you, a couple of you have pinned to the going courses.

135
00:13:10,000 --> 00:13:12,000
He talks a lot about this, I think.

136
00:13:12,000 --> 00:13:15,000
But we're not trying to force a becca, right?

137
00:13:15,000 --> 00:13:21,000
The key for all of this is to not try and cultivate the goal.

138
00:13:21,000 --> 00:13:24,000
We have to cultivate the practice.

139
00:13:24,000 --> 00:13:28,000
And if it's right practice, the goal will come by itself.

140
00:13:28,000 --> 00:13:31,000
So through all of this, you should become more equanimals.

141
00:13:31,000 --> 00:13:32,000
Don't force that.

142
00:13:32,000 --> 00:13:33,000
That's not the goal.

143
00:13:33,000 --> 00:13:35,000
That's not the way.

144
00:13:35,000 --> 00:13:38,000
But it will by itself.

145
00:13:38,000 --> 00:13:49,000
It should become very much intent upon the practice and also as you start to learn more about yourself.

146
00:13:49,000 --> 00:13:53,000
You're less inclined to cling to things that can't make you happy.

147
00:13:53,000 --> 00:13:55,000
Can't satisfy you.

148
00:13:55,000 --> 00:14:04,000
Things that only lead to suffering when you cling to them.

149
00:14:04,000 --> 00:14:08,000
And the pick is the final one.

150
00:14:08,000 --> 00:14:13,000
Once you have a becca, true a becca, that's the highest of a becca.

151
00:14:13,000 --> 00:14:17,000
It's from there that you enter into nibana.

152
00:14:17,000 --> 00:14:20,000
Samkha and a becca.

153
00:14:20,000 --> 00:14:24,000
Equanimity about all Samkha's.

154
00:14:24,000 --> 00:14:27,000
Seeing them just as arising and seizing.

155
00:14:27,000 --> 00:14:32,000
No longer distress, no longer upset, no longer craving or yearning.

156
00:14:32,000 --> 00:14:37,000
Content, peaceful, focused.

157
00:14:37,000 --> 00:14:40,000
So there are real progression in that way.

158
00:14:40,000 --> 00:14:44,000
But the Buddha offers another way of understanding them.

159
00:14:44,000 --> 00:14:52,000
He says that an investigation of dhammas and energy and rapture

160
00:14:52,000 --> 00:14:57,000
these three relate to energy, effort.

161
00:14:57,000 --> 00:15:01,000
So they're the ones you should cultivate when you're tired.

162
00:15:01,000 --> 00:15:03,000
When you're lazy.

163
00:15:03,000 --> 00:15:09,000
Following a sleep while you cultivate dhammas which say a really a beauty.

164
00:15:09,000 --> 00:15:13,000
Focus on them.

165
00:15:13,000 --> 00:15:30,000
When you're in the other three, tranquility or quieted concentration and equanimity.

166
00:15:30,000 --> 00:15:35,000
These ones are related to concentration.

167
00:15:35,000 --> 00:15:42,000
So if you're restless, if you're distracted, you should cultivate these three.

168
00:15:42,000 --> 00:15:48,000
But again, we get back to this idea of, well, first we get back to this is the idea of balance.

169
00:15:48,000 --> 00:16:06,000
And balance these seven factors of enlightenment.

170
00:16:06,000 --> 00:16:14,000
But we get back to this idea of trying to go around and start all the tweak all the parts of the engine.

171
00:16:14,000 --> 00:16:19,000
And maybe that's a good analogy because there is some tweaking that can be done.

172
00:16:19,000 --> 00:16:26,000
But the meditation practice, the tweaking itself, comes about by mindfulness.

173
00:16:26,000 --> 00:16:31,000
In a way, it's just a way of highlighting the fact that mindfulness is in the center.

174
00:16:31,000 --> 00:16:36,000
Because this is where the Buddha said, Satin Chukwang, Bhikhavaya, Sabbatikha and Wandaami.

175
00:16:36,000 --> 00:16:46,000
Whereas the other six factors of enlightenment are only important to cultivate when you're imbalanced.

176
00:16:46,000 --> 00:16:52,000
Mindfulness is always useful.

177
00:16:52,000 --> 00:17:04,000
And so by being mindful of the imbalance, that's where balance comes from.

178
00:17:04,000 --> 00:17:07,000
So remember the Bhoganga.

179
00:17:07,000 --> 00:17:12,000
I think this is really the way it's to be done.

180
00:17:12,000 --> 00:17:15,000
You're not going to go around and tweak the engine.

181
00:17:15,000 --> 00:17:18,000
You don't go around and say, okay, I don't have enough effort.

182
00:17:18,000 --> 00:17:23,000
I'm going to cultivate effort so it could magically appear.

183
00:17:23,000 --> 00:17:26,000
It's important to know the Dhamma.

184
00:17:26,000 --> 00:17:33,000
We don't realize that just knowing these things already changes the way you look at them.

185
00:17:33,000 --> 00:17:34,000
Right?

186
00:17:34,000 --> 00:17:38,000
If I hadn't taught you how to meditate, how to be mindful.

187
00:17:38,000 --> 00:17:40,000
You wouldn't know how to look at the mind.

188
00:17:40,000 --> 00:17:45,000
When we talk about the three characteristics, if you hadn't heard of impermanence,

189
00:17:45,000 --> 00:17:49,000
suffering and non-selfie, what I thought this was just torture.

190
00:17:49,000 --> 00:17:51,000
Everyone, many people do run away.

191
00:17:51,000 --> 00:17:52,000
Well, not many.

192
00:17:52,000 --> 00:17:58,000
Some people do run away in the early stages of the course.

193
00:17:58,000 --> 00:17:59,000
Because they can't handle.

194
00:17:59,000 --> 00:18:02,000
They don't understand what's really going on.

195
00:18:02,000 --> 00:18:07,000
And they see impermanence suffering and non-selfie and it scares them.

196
00:18:32,000 --> 00:18:42,000
So it's important to know the Dhamma.

197
00:18:42,000 --> 00:18:47,000
It's important to be taught it, to be able to explain to you.

198
00:18:47,000 --> 00:18:53,000
It's just knowing it changes how you look at it.

199
00:18:53,000 --> 00:18:59,000
I stress that because what you don't want to do is try and control it.

200
00:18:59,000 --> 00:19:05,000
As I said, you don't have energies to run around doing jumping jacks or something.

201
00:19:05,000 --> 00:19:07,000
You don't force these things.

202
00:19:07,000 --> 00:19:08,000
You understand them.

203
00:19:08,000 --> 00:19:15,000
Because most importantly, it gives you a focus for your mindfulness.

204
00:19:15,000 --> 00:19:21,000
Highlighting all the Buddhists, you would highlight all the various aspects of experience.

205
00:19:21,000 --> 00:19:24,000
By highlighting them, it's like shining a light on them.

206
00:19:24,000 --> 00:19:30,000
When you hear about this and you reflect, how is my effort, how is my concentration,

207
00:19:30,000 --> 00:19:32,000
equanimity and so on.

208
00:19:32,000 --> 00:19:43,000
Then you have a light shine on, shine on, shine on.

209
00:19:43,000 --> 00:19:54,000
On the nature of reality, on reality.

210
00:19:54,000 --> 00:20:03,000
Makes it much more precise and clear when it goes to being mindful of it.

211
00:20:03,000 --> 00:20:16,000
Remember these seven mindfulness sati, the wisdom that comes from it, the realization of impermanence,

212
00:20:16,000 --> 00:20:21,000
suffering and non-self.

213
00:20:21,000 --> 00:20:48,000
They go into progression for the most part, but they also can't become imbalanced and it's seeing that imbalance that allows you to correct it.

214
00:20:48,000 --> 00:20:52,000
There you go, a little bit of dhamma tonight, something very important.

215
00:20:52,000 --> 00:20:59,000
It's quite simple, but those seven things, those are what needs to enlightenment.

216
00:20:59,000 --> 00:21:05,000
Thank you all for coming out.

217
00:21:05,000 --> 00:21:13,000
Keep practicing.

218
00:21:13,000 --> 00:21:38,000
We have more questions on the site.

219
00:21:38,000 --> 00:21:44,000
In relation to the meditation technique on sitting, I usually do it with my eyes open as with walking,

220
00:21:44,000 --> 00:21:50,000
because most of the day I'm with my eyes open on to relate my practice to daily life.

221
00:21:50,000 --> 00:21:55,000
I'm drawing much important as the practice.

222
00:21:55,000 --> 00:22:01,000
Also, sitting meditation with your eyes closed is the key factor of the practice.

223
00:22:01,000 --> 00:22:13,000
No, but it's a sense door that we can close and as a result it simplifies things.

224
00:22:13,000 --> 00:22:19,000
So closing the eyes is actually beneficial in that sense.

225
00:22:19,000 --> 00:22:21,000
It makes it easier.

226
00:22:21,000 --> 00:22:25,000
The meditation is hard enough without trying to make it harder.

227
00:22:25,000 --> 00:22:30,000
I get what you're saying, trying to make it like real life, but real life it's very hard to be mindful.

228
00:22:30,000 --> 00:22:34,000
So there's nothing wrong with having your eyes open.

229
00:22:34,000 --> 00:22:42,000
You probably get better, quicker results with your eyes closed, because it's easier.

230
00:22:42,000 --> 00:22:58,000
An easy allows you to extend, allows you to focus better, to progress quicker.

231
00:22:58,000 --> 00:23:04,000
What is Akasha? Akasa means space, yes.

232
00:23:04,000 --> 00:23:07,000
Akasa is the world, the world of space.

233
00:23:07,000 --> 00:23:09,000
It's one of the three worlds.

234
00:23:09,000 --> 00:23:13,000
There's Akasa Loka, the world of space.

235
00:23:13,000 --> 00:23:17,000
There's Sata Loka, which is the world of beings.

236
00:23:17,000 --> 00:23:25,000
And then there's Sankara Loka, which is the world of formation.

237
00:23:25,000 --> 00:23:34,000
There's a question about Zazan compared to Maasi.

238
00:23:34,000 --> 00:23:37,000
Sorry, I don't do such things.

239
00:23:37,000 --> 00:23:40,000
I don't really, because I don't know anything about what you're talking about.

240
00:23:40,000 --> 00:23:46,000
I don't know much about Zeninol, besides what you're reading books.

241
00:23:46,000 --> 00:23:51,000
Every once in a while I get dizzy and when I open my eyes my eyes are shaking.

242
00:23:51,000 --> 00:23:59,000
I'm trying to be mindful of the shaking and frustration, but it seems to be too intense for being big and meditated.

243
00:23:59,000 --> 00:24:08,000
Well, it seems to be too intense is just a judgment, so you can say judging, judging, or worried, worried, or so on.

244
00:24:08,000 --> 00:24:10,000
So such thing is too intense.

245
00:24:10,000 --> 00:24:18,000
Too intense is when you give into it.

246
00:24:18,000 --> 00:24:21,000
And it's going to be trying to be mindful of the shaking and frustration.

247
00:24:21,000 --> 00:24:31,000
What you're learning is impermanence suffering non-self impermanence, because it's strange, you know, impermanence is when you see that things can change in any time.

248
00:24:31,000 --> 00:24:32,000
It wasn't like this.

249
00:24:32,000 --> 00:24:34,000
Now it's like this.

250
00:24:34,000 --> 00:24:41,000
Suffering because you see that it's not the way you want it to be and makes you frustrated.

251
00:24:41,000 --> 00:24:48,000
And non-self suffering also that you can't make it the way you can't fix it.

252
00:24:48,000 --> 00:24:51,000
It doesn't go according to your plan.

253
00:24:51,000 --> 00:24:54,000
Non-self, right?

254
00:24:54,000 --> 00:25:05,000
Because you can't control it.

255
00:25:05,000 --> 00:25:08,000
Any tips for remaining mindful throughout the day?

256
00:25:08,000 --> 00:25:10,000
Thank you.

257
00:25:10,000 --> 00:25:15,000
I'll become enlightened, that's the best way.

258
00:25:15,000 --> 00:25:24,000
I'd encourage if you have time to do a meditation course or more than one.

259
00:25:24,000 --> 00:25:29,000
Intensive courses are really good for helping to be more mindful during the day.

260
00:25:29,000 --> 00:25:33,000
Live in a place where people are mindful, live in a monastery.

261
00:25:33,000 --> 00:25:34,000
That helps.

262
00:25:34,000 --> 00:25:36,000
That kind of thing.

263
00:25:36,000 --> 00:25:43,000
Study. Study the dumb, but that'll help.

264
00:25:43,000 --> 00:25:45,000
Okay, so that's all the questions.

265
00:25:45,000 --> 00:25:46,000
Thank you all for tuning in.

266
00:25:46,000 --> 00:25:52,000
Again, I'm not here tomorrow, probably through Sunday, because there's a big celebration.

267
00:25:52,000 --> 00:25:54,000
Tomorrow I'm going to teach.

268
00:25:54,000 --> 00:25:55,000
I mentioned this.

269
00:25:55,000 --> 00:25:57,000
I'm going to teach at University of Toronto.

270
00:25:57,000 --> 00:25:59,000
There's this celebration.

271
00:25:59,000 --> 00:26:05,000
It's a memorial for the head monks, dead mother.

272
00:26:05,000 --> 00:26:10,000
And then on Sunday, there's the Buddhist birthday celebration in Mississauga.

273
00:26:10,000 --> 00:26:12,000
Big thousand thousand people.

274
00:26:12,000 --> 00:26:17,000
We're going to have a meditation tent there.

275
00:26:17,000 --> 00:26:18,000
So that's all.

276
00:26:18,000 --> 00:26:42,000
Have a good night everyone.

