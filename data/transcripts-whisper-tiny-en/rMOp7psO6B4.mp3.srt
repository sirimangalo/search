1
00:00:00,000 --> 00:00:06,080
Scott, is it possible for someone with a mental illness, such as bipolar, this order or she

2
00:00:06,080 --> 00:00:09,840
schizophrenia, come on a meditation course with you in Sri Lanka?

3
00:00:12,080 --> 00:00:19,040
Yeah, for sure. That's certainly possible. I wouldn't stop such a person.

4
00:00:20,480 --> 00:00:25,040
The one issue that I would have is whether the person has to take medication,

5
00:00:25,040 --> 00:00:32,480
because we have had people take, I've watched people taking anti-depression meditation,

6
00:00:32,480 --> 00:00:42,400
medication. And medication in general is not advised during a meditation course,

7
00:00:44,000 --> 00:00:48,800
whether it be medication for physical ailments or medication for mental ailments.

8
00:00:48,800 --> 00:00:57,200
Now, if it's a physical ailment that requires medicine, for example, we have people who had

9
00:01:00,240 --> 00:01:06,560
had some sort of condition. Was it diabetes? No, it wasn't. It was something horrible where

10
00:01:06,560 --> 00:01:12,400
they had to do dialysis treatment. And so they had this portable dialysis machine, and they came

11
00:01:12,400 --> 00:01:18,480
into a meditation course and did dialysis every day. It was crazy. In that case, it was actually,

12
00:01:18,480 --> 00:01:20,640
you know, it was no problem. And they were able to finish the course.

13
00:01:21,360 --> 00:01:31,760
Well, undergoing dialysis treatment, but medication for the mind is certainly a problem.

14
00:01:31,760 --> 00:01:36,640
And if it's to the point where you have to take some sort of anti-depressant or anti-psychotic or

15
00:01:36,640 --> 00:01:45,520
something, then we would have to discuss that. Because I think in general, we'd have a

16
00:01:45,520 --> 00:01:52,000
role that people shouldn't take the present anti or shouldn't take anti-psychotic and they

17
00:01:52,000 --> 00:02:00,480
depressant or whatever medications during the meditation course. But otherwise, I mean bipolar,

18
00:02:00,480 --> 00:02:12,720
I think is really quite workable from our point of view. Skits of Frenia, I think it depends on

19
00:02:12,720 --> 00:02:18,800
the person. If they're having psychotic ideations, like they want to kill people or so on,

20
00:02:18,800 --> 00:02:26,240
then it may be difficult. But it's worth talking about it and it's something that we'd

21
00:02:26,240 --> 00:02:47,120
have to discuss before the person came.

