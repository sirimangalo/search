1
00:00:00,000 --> 00:00:08,280
Hello, I just have a short update about some of the things that I've been getting involved in and keeping busy with

2
00:00:09,880 --> 00:00:13,640
Men why I haven't been making videos for as come on

3
00:00:14,560 --> 00:00:19,840
There are two things that I've gotten involved in recently and they both have to do with the internet and they both have to do with

4
00:00:20,160 --> 00:00:22,160
the teaching of

5
00:00:22,160 --> 00:00:23,400
meditation and

6
00:00:23,400 --> 00:00:29,680
Engaging others in the practice of meditation and helping others in the practice of meditation

7
00:00:29,960 --> 00:00:36,280
The first is we've set up a bit of a social network on our website and if you haven't seen it

8
00:00:36,280 --> 00:00:43,680
Yeah, it's called my dot Siri mongolow and so the web address is my dot Siri mongolow.org and

9
00:00:44,280 --> 00:00:47,240
The address is in the description to this video if you can't get that

10
00:00:48,560 --> 00:00:51,320
And so it's it's based on

11
00:00:51,320 --> 00:00:58,480
You know the kind of format of Facebook or my space or something, but it's really very low-key and

12
00:00:59,200 --> 00:01:05,720
All it is is a chance for people who are following these videos and following what I do to get involved to get

13
00:01:05,720 --> 00:01:13,440
they're interested to post updates and to talk about their own meditation and to see what we're doing to see what other people are doing and

14
00:01:13,440 --> 00:01:17,720
Just have a bit of a social feel. I'm not sure where it'll go. Maybe we can add other

15
00:01:18,560 --> 00:01:20,080
functions and so on

16
00:01:20,080 --> 00:01:22,080
But it's something that

17
00:01:22,880 --> 00:01:24,880
That I think is really

18
00:01:24,880 --> 00:01:33,120
useful for for some people who are you know in a place where they don't have a community and are looking to have that kind of contact

19
00:01:33,120 --> 00:01:38,400
And so I do see people logging in and telling us what they're meditating and so on and I think that's really good

20
00:01:38,400 --> 00:01:44,080
And it's also a place for me to post my updates let to let everyone know if you're interested in what I do

21
00:01:44,800 --> 00:01:46,040
Instead of writing a blog

22
00:01:46,040 --> 00:01:50,360
Web blog post about it. I can just post an update say now. I'm doing this. I'm doing that

23
00:01:51,680 --> 00:01:57,400
Everyone can see it and know what I'm doing and I think it should go the same with the rest of the community

24
00:01:58,280 --> 00:02:02,080
So check that out you get involved sign up if you if it's interested

25
00:02:02,720 --> 00:02:05,680
There's not much going on there don't expect it to be really social

26
00:02:06,280 --> 00:02:08,000
But that's kind of the point

27
00:02:08,000 --> 00:02:13,040
It's an alternative to getting caught up in other social networks that are busy and

28
00:02:13,040 --> 00:02:16,880
And waste our time and so on it's a place where we can go just to

29
00:02:17,440 --> 00:02:24,640
Reaffirm what we're doing and to see that there are other people doing what we're doing and hopefully we can adjust it and and to make it

30
00:02:25,440 --> 00:02:29,120
Really conducive for that. That's the key for that one

31
00:02:29,600 --> 00:02:35,840
The second thing that I've been getting involved in or I've just started now initiated and just set up

32
00:02:37,040 --> 00:02:40,240
Is a way to engage with people in

33
00:02:40,240 --> 00:02:44,320
one-on-one or or directly live

34
00:02:45,840 --> 00:02:49,120
Because all the videos that I do are not live YouTube

35
00:02:49,920 --> 00:02:51,920
Hasn't rolled out this feature of

36
00:02:52,560 --> 00:02:57,360
Giving live videos. I don't even know I've been be able to do it on the internet connection that we have here

37
00:02:58,000 --> 00:03:01,280
But I can do live audio from here and so I've been doing that

38
00:03:01,280 --> 00:03:03,760
There's a radio site and that link is in

39
00:03:04,560 --> 00:03:08,000
In the description here. It's on our website if you haven't checked out that out

40
00:03:08,000 --> 00:03:11,760
I'm doing radio live radio. I've been doing

41
00:03:12,240 --> 00:03:17,440
Intermittently and I guess my plan right now is to do it every two weeks do a session and

42
00:03:18,000 --> 00:03:22,560
For people who want to take the precepts. I think I've talked about this before the refuges and precepts

43
00:03:23,040 --> 00:03:26,720
While we're doing it in a group and you're welcome to take part in that group

44
00:03:27,600 --> 00:03:33,280
Right now it's twice every two weeks and you're thinking I'm gonna do it on a Sunday and we can adjust that

45
00:03:34,320 --> 00:03:36,080
So this has been going on for a little bit, but

46
00:03:36,080 --> 00:03:40,480
Then the new the new thing that I'm just going to announce now is

47
00:03:41,680 --> 00:03:43,680
And I'm going to try to get back into

48
00:03:44,320 --> 00:03:46,320
giving one-on-one

49
00:03:46,320 --> 00:03:47,920
lessons and

50
00:03:47,920 --> 00:03:49,120
instruction

51
00:03:49,120 --> 00:03:51,120
via Skype or via

52
00:03:51,680 --> 00:03:52,960
We can do

53
00:03:52,960 --> 00:03:55,520
text chat Google talk or whatever

54
00:03:56,160 --> 00:04:01,200
But I think the preferred method is Skype because it's really easy and it works really well that

55
00:04:02,000 --> 00:04:04,480
The audio is always smooth and clear

56
00:04:04,480 --> 00:04:06,480
So

57
00:04:06,480 --> 00:04:08,480
if anyone's interested

58
00:04:08,880 --> 00:04:10,880
I've set up a

59
00:04:11,680 --> 00:04:13,680
Something on Google calendars

60
00:04:14,160 --> 00:04:20,640
Google calendars as this new feature that allows you to designate a time slot on your calendar when people can

61
00:04:22,400 --> 00:04:23,680
Make an appointment with you

62
00:04:23,680 --> 00:04:30,480
So if I have a time slot during this time and you want to take half an hour of that time slot you click on that half an hour

63
00:04:30,480 --> 00:04:34,480
That's applicable to you, but that's that's available for you or

64
00:04:35,680 --> 00:04:40,640
Convenient for you and you make an appointment with me and I'm assuming it sends me an email

65
00:04:40,640 --> 00:04:42,880
I haven't tried it yet. I've just set it up now

66
00:04:43,360 --> 00:04:46,640
There's a link to the calendar in the description to this video as well

67
00:04:46,640 --> 00:04:51,440
The all the links are down below this video, so you can check them out and

68
00:04:52,320 --> 00:04:56,720
They'll send me a notice and and I'll arrange it with you or will arrange

69
00:04:56,720 --> 00:05:00,320
Or I'll just confirm it with you or you can just show up at that time

70
00:05:01,120 --> 00:05:04,480
Please do make the appointment in advance and if you don't make an advance

71
00:05:04,480 --> 00:05:07,920
I may not show up and I may cancel some of the sessions

72
00:05:07,920 --> 00:05:11,760
So we'll see how it works, but hopefully this is a chance for

73
00:05:13,200 --> 00:05:17,920
Us to connect. I'm going to try to do in the beginning two times a day, but the schedule will be on this

74
00:05:17,920 --> 00:05:20,560
I did my change so follow that link and

75
00:05:20,560 --> 00:05:28,080
Sign up. Let me know when you'd like to talk if you'd like to talk. It has to be about the meditation or about

76
00:05:28,480 --> 00:05:34,400
What are the things I do about Buddhism or the dhamma if you just have questions or if you're meditating and you want advice

77
00:05:35,120 --> 00:05:42,080
We can do it one-on-one via Skype. I don't know how my availability, but I'm here. I'm just in my room doing my own

78
00:05:42,720 --> 00:05:44,720
meditation and teaching and so on

79
00:05:46,160 --> 00:05:48,160
So I'm available

80
00:05:48,160 --> 00:05:51,840
And you can check it out. See how that works

81
00:05:52,560 --> 00:05:58,000
If you like click on the link get download Skype and click on the link and

82
00:05:59,040 --> 00:06:01,040
Sign up for an appointment and we can talk

83
00:06:01,760 --> 00:06:09,280
Talking one-on-one is really the best. I've actually taught a couple of people through the course of meditation

84
00:06:10,000 --> 00:06:14,960
Via Skype. It takes a lot of dedication to be able to do it because you don't have

85
00:06:14,960 --> 00:06:19,520
You know, it's not the same as being with the teacher or being in a meditation

86
00:06:20,240 --> 00:06:27,600
Center, but people can progress in the meditation and you progress a lot quicker than if you don't have any contact with the teacher one-on-one is

87
00:06:28,160 --> 00:06:35,360
Is great because you're able to assess the person's needs and you're able to answer their questions directly

88
00:06:36,080 --> 00:06:38,560
Are you able to provide them with advice and encouragement?

89
00:06:38,560 --> 00:06:40,560
It's so encouraging to have some

90
00:06:40,800 --> 00:06:42,800
direct contact with someone

91
00:06:42,800 --> 00:06:44,800
You that you can talk to about your meditation

92
00:06:45,120 --> 00:06:48,560
So check that out. Those are the things that I've been involved in. This is an update

93
00:06:49,040 --> 00:06:51,040
and this is the latest post from my

94
00:06:51,760 --> 00:06:53,760
video blog

95
00:06:54,160 --> 00:07:00,320
And so again, thanks for everyone for getting involved with our new social network for getting involved with the

96
00:07:00,800 --> 00:07:04,720
radio and and for watching the videos for commenting for giving feedback

97
00:07:05,360 --> 00:07:07,600
asking questions on our ask site

98
00:07:07,600 --> 00:07:13,040
And so on. It's really good to see things are taking off here at the meditation center. We're

99
00:07:13,760 --> 00:07:19,360
Slowly but surely building a real-life meditation center and there are people coming

100
00:07:20,080 --> 00:07:22,080
slowly but surely

101
00:07:22,080 --> 00:07:24,080
at this point we're still not

102
00:07:24,960 --> 00:07:26,160
able to

103
00:07:26,160 --> 00:07:27,520
provide

104
00:07:27,520 --> 00:07:30,320
Real convenience to meditators so you have to be a pretty

105
00:07:30,880 --> 00:07:35,600
Brave person to come and to stay in one of our caves and to walk to the bathroom

106
00:07:35,600 --> 00:07:37,600
You don't have a bathroom in your in your

107
00:07:38,080 --> 00:07:39,040
Good day

108
00:07:39,040 --> 00:07:42,560
So you have to walk to the jungle and you know you've got spiders and

109
00:07:43,600 --> 00:07:45,600
bizzers and

110
00:07:46,240 --> 00:07:49,680
Rarely see anything poisonous. I haven't no longest time

111
00:07:50,000 --> 00:07:52,000
so I don't think there's any danger

112
00:07:52,240 --> 00:07:53,760
but

113
00:07:53,760 --> 00:07:57,760
You know, it's it's not as comfortable as say a

114
00:07:58,400 --> 00:08:01,040
meditation center in California or

115
00:08:02,160 --> 00:08:04,160
London or so

116
00:08:04,160 --> 00:08:07,920
If this interest you you're welcome to come here as well and

117
00:08:09,760 --> 00:08:13,120
Take part directly but otherwise we have these online

118
00:08:14,000 --> 00:08:15,840
facilities and

119
00:08:15,840 --> 00:08:21,040
The purpose here we're not trying to I'm not trying to become famous. We're not trying to become to make money

120
00:08:21,040 --> 00:08:22,640
It's all free

121
00:08:22,640 --> 00:08:27,440
The purpose is that is to spread what we believe to be good things to the world

122
00:08:27,680 --> 00:08:30,640
The world is is in a balance now, you know

123
00:08:30,640 --> 00:08:34,480
There are many good things in the world, but there are many forces that are dragging us down

124
00:08:35,280 --> 00:08:36,240
and even in

125
00:08:37,280 --> 00:08:40,880
Our individual lives there are forces that often drag us down

126
00:08:40,880 --> 00:08:45,760
So we're trying to counteract that we're trying to give people something good because that makes our world better

127
00:08:46,320 --> 00:08:49,760
I live in this world as well. Even though I live in in the jungle

128
00:08:49,760 --> 00:08:56,960
I'm not isolated and if we're negligent if we're not doing our part the world will collapse around us and and

129
00:08:56,960 --> 00:09:03,360
We'll be carried away by the flood. So if we want to live in peace and happiness, it's important that we

130
00:09:04,480 --> 00:09:10,320
Share peace and happiness with others. So that's what I'm doing. That's what we're doing. That's what this organization is formed

131
00:09:11,200 --> 00:09:15,920
So I'd like to thank you all for taking part cooperating and helping us to make the world a better place

132
00:09:15,920 --> 00:09:29,920
Thanks for tuning in. I wish you all peace, happiness, and freedom from suffering all of us

