1
00:00:00,000 --> 00:00:18,040
So here we are at Kijakuta in Rajagaha in the old city in the mountains, if you look

2
00:00:18,040 --> 00:00:24,240
on the map it's the only range of mountains in the area so it's a fairly special geographic

3
00:00:24,240 --> 00:00:32,720
geologically. It's called Gijakuta means vultures peak because supposedly these rocks in the

4
00:00:32,720 --> 00:00:42,640
background, 2600 years ago they looked like a vulture, they still kind of. So many of the places

5
00:00:42,640 --> 00:00:58,960
we've come to, we've come to many places outside of the four holy sites, the four places

6
00:00:58,960 --> 00:01:07,960
recommended by the Buddha or that the Buddha said were beneficial for one to come to some

7
00:01:07,960 --> 00:01:17,960
visit on a pilgrimage. We have the place of the Bodhisattva's birth, we've seen, we have

8
00:01:17,960 --> 00:01:23,920
the place where the Bodhisattva became enlightened that we'll be seeing next. The place

9
00:01:23,920 --> 00:01:30,320
where we first saw it and the place where we passed away, those are the four main sites.

10
00:01:30,320 --> 00:01:46,960
There's lots of other sites worth seeing that represent highlights of the Buddha's life and

11
00:01:46,960 --> 00:01:56,640
Kijakuta is one of those places. You could say that our visits to these sites are a celebration

12
00:01:56,640 --> 00:02:03,520
of the Buddha's life. The one thing that's missing from the four sites, we don't have anything

13
00:02:03,520 --> 00:02:11,120
that celebrates the Buddha's life and all the rest of these places do that. Gijakuta is a

14
00:02:11,120 --> 00:02:19,920
special place in that regard I think because it's the first place the Buddha chose as a dwelling

15
00:02:19,920 --> 00:02:33,160
place. Before coming here he chose places based on a reason for being there in the Bodhgaya

16
00:02:33,160 --> 00:02:38,280
where he became enlightened and Isipatana where he first saw it but as soon as he finished

17
00:02:38,280 --> 00:02:53,120
teaching. His decision, his choice of places to come was Bodhisattva. He went to teach somewhere

18
00:02:53,120 --> 00:03:05,280
in the hills. He taught Uruwehla Kasupan, Nadi Kasupan, Diakasupa. But once he had taught, this was the place

19
00:03:05,280 --> 00:03:20,840
where he chose to live in Vulture in Rajaka. Before, Vailuwana was given as a monastery and many

20
00:03:20,840 --> 00:03:25,840
places Jaita won which we've seen were given as monasteries in the Buddha acquiesced to live there

21
00:03:25,840 --> 00:03:33,520
but this place sort of reminds one much more of the sort of place the Buddha himself would

22
00:03:33,520 --> 00:03:44,040
choose. So today I thought I'd talk a little bit about life. Celebrating the Buddha's life,

23
00:03:44,040 --> 00:03:56,680
celebrating life seems a little odd. I mentioned earlier that a lot of Buddhists say that life

24
00:03:56,680 --> 00:04:02,840
is suffering, that the first noble truth is that life is suffering. I think I got it wrong

25
00:04:02,840 --> 00:04:11,440
earlier I said that what the Buddha taught, he actually makes a note of this. He says that a lot

26
00:04:11,440 --> 00:04:17,120
of people, so he got it right. A lot of people say this but he says actually that's not

27
00:04:17,120 --> 00:04:24,760
what the first noble truth is. I brought that up before and had people comment on it saying that

28
00:04:24,760 --> 00:04:30,880
well, it isn't that just semantics if you say that not all life is suffering, what about

29
00:04:30,880 --> 00:04:40,800
the Buddha saying the five aggregates are suffering. And so I think we should clear up one very

30
00:04:40,800 --> 00:04:45,800
important point in Buddhism of course because suffering is such an important teaching. His

31
00:04:45,800 --> 00:04:51,160
understanding would is meant by the word Luka actually. Some people are hesitant to translate it

32
00:04:51,160 --> 00:04:56,880
as suffering. I'm not so concerned about that but if you do translate it as suffering you have

33
00:04:56,880 --> 00:05:02,240
to explain what that means. We should also explain what we mean by the five aggregates

34
00:05:02,240 --> 00:05:11,280
because it's also such a very important teaching of the Buddha. So the five aggregates are

35
00:05:11,280 --> 00:05:22,000
Rupa, form, which is physical form, Vedana, feelings, Sanya, memory or recognition,

36
00:05:22,000 --> 00:05:29,400
Sankara meaning thoughts, formations but what you think of something how you react to it, extrapolate

37
00:05:29,400 --> 00:05:39,640
on it. And Vinyana meaning consciousness. And these are often taken to be thought of as what's

38
00:05:39,640 --> 00:05:45,520
in here, right? If you look at this body you say this body is made up of five aggregates.

39
00:05:45,520 --> 00:05:50,000
The ultimate reality isn't actually like that because of course the body is just a concept.

40
00:05:50,000 --> 00:05:56,080
If you look and you see the body, you get a conception in your mind on my body. You

41
00:05:56,080 --> 00:06:01,200
look at the hand and you get a conception that's a hand. And in reality there is moments

42
00:06:01,200 --> 00:06:07,800
of experience seeing, hearing, smelling, tasting, feeling, thinking. And all of these

43
00:06:07,800 --> 00:06:16,960
six types of experience give rise to or are made up of. That's the experiences that are

44
00:06:16,960 --> 00:06:26,160
made up of the five aggregates. And that constitutes life from a Buddhist perspective in

45
00:06:26,160 --> 00:06:32,000
these experiences, right? So one can say, well if those things are suffering then that

46
00:06:32,000 --> 00:06:36,800
means all of life is suffering, right? So it's an easy conclusion to come to. Unfortunately

47
00:06:36,800 --> 00:06:44,400
it's a simplistic conclusion to come to. It really is. First of all because the word

48
00:06:44,400 --> 00:06:52,480
dukkha. Dukkha could be used generally in two ways. It could mean something that is painful

49
00:06:52,480 --> 00:06:59,520
or it could mean the pain itself, physical or mental. If you're sad that's suffering, if you're

50
00:06:59,520 --> 00:07:09,280
in pain that's suffering. But as a philosophy it's like something being hot. Fire being

51
00:07:09,280 --> 00:07:17,440
hot. Fire is a really good example or analogy. Fire isn't really hot. But we say it's

52
00:07:17,440 --> 00:07:25,600
hot because when we touch it, when we grasp it, it burns us. We feel the heat. We feel

53
00:07:25,600 --> 00:07:37,200
hot. But that's because we're conscious. So the heat comes from our experience of it. And

54
00:07:37,200 --> 00:07:44,960
if we leave it alone, if we don't grasp it, it's not painful. Now with experience it's not the

55
00:07:44,960 --> 00:07:51,600
same in that we're not saying that every experience you have is painful. It's dukkha. But the dukkha comes

56
00:07:51,600 --> 00:07:57,840
because the Buddha didn't actually say the five aggregates are suffering. He said the five aggregates

57
00:07:57,840 --> 00:08:10,240
of clinging. Or the the aggregate of clinging to each of these five things is suffering.

58
00:08:12,080 --> 00:08:16,800
And so that's where the idea of grasping at the fire comes from. Experience is something like

59
00:08:16,800 --> 00:08:22,480
experiencing fire. If you see a fire in front of you, it's not hot. There's no heat that comes

60
00:08:22,480 --> 00:08:28,480
from it until you get close until and even then it's not painful until you grab put your hand in the

61
00:08:28,480 --> 00:08:37,760
fire. And we know this because of course an enlightened being has a life. We know this because of

62
00:08:37,760 --> 00:08:41,920
the Buddha's example. It's a very clear part of aspect of Buddha's and the Buddha didn't

63
00:08:43,360 --> 00:08:47,360
immediately disappear from Samsara and didn't immediately stop having experience

64
00:08:47,360 --> 00:08:57,840
is when he became free from suffering. So we can say in some sense he was free from suffering but

65
00:08:57,840 --> 00:09:05,360
he wasn't free from dukkha. That means he wasn't free from things that were and some people

66
00:09:05,360 --> 00:09:10,400
like to translate as unsatisfying. And he didn't have to be free from them because they weren't

67
00:09:10,400 --> 00:09:16,400
a problem because he wasn't putting his hand in them. And that's the truth. That's why mindfulness

68
00:09:16,400 --> 00:09:22,160
is so powerful. It's not about escaping suffering. The practice is not about escaping suffering.

69
00:09:22,160 --> 00:09:26,960
Even though you could say in some sense that's a goal, it's about understanding it.

70
00:09:27,600 --> 00:09:32,640
And understanding removes the problem without having to remove the thing that you thought was

71
00:09:32,640 --> 00:09:37,680
problematic. It turns out that our experiences aren't problems. It's our reactions to them.

72
00:09:37,680 --> 00:09:48,640
It's the clinging to them. Go ahead or bad. Me, mine. So in some sense, celebrating life is

73
00:09:48,640 --> 00:09:56,480
in the problem. Life can be perfectly wonderful. But and here's where we have to make an important

74
00:09:56,480 --> 00:10:05,520
distinction that there's a very great difference between celebrating life and living a life worth

75
00:10:05,520 --> 00:10:16,160
celebrating. A lot of non Buddhists talk about celebrating life. Non religious people believe in

76
00:10:18,080 --> 00:10:25,120
living life to its fullest, which isn't actually technically, you know, if you just take it at

77
00:10:25,120 --> 00:10:34,320
face value, that's not again against Buddhism. Some people talk about celebrating life. They have

78
00:10:34,320 --> 00:10:38,800
a spirituality, maybe not religion, but they would call it a spirituality where life is a

79
00:10:39,760 --> 00:10:47,200
some kind of vague sort of blessing to be celebrated, to be enjoyed, really, is the point.

80
00:10:50,400 --> 00:10:57,760
Celebrating life is very much tied up with enjoyment and sensuality.

81
00:10:57,760 --> 00:11:04,720
Living a life worth celebrating. Why we celebrate the Buddha's life is because he,

82
00:11:05,760 --> 00:11:13,520
his life was worth celebrating. And so our practice is not about celebrating life. It's about

83
00:11:15,600 --> 00:11:19,120
cultivating qualities of of mind.

84
00:11:19,120 --> 00:11:29,600
Moving towards a state of being that is worth celebrating. That is worth rejoicing over.

85
00:11:30,560 --> 00:11:35,840
Because there are many kinds of life. Life in hell is probably not worth celebrating.

86
00:11:37,600 --> 00:11:42,960
It's not really. You can you can argue for some kind of objectivity and say don't

87
00:11:42,960 --> 00:11:49,600
be hard on those people in hell, but the whole point of it being hell is it's caused by

88
00:11:49,600 --> 00:11:55,680
unwholesumness. It's caused by anger. It's not worth celebrating anger or the result of anger.

89
00:11:58,080 --> 00:12:03,120
It only causes great, great suffering in those people who live in hell. Animal realm,

90
00:12:03,120 --> 00:12:10,240
not really worth worth rejoicing in. Celebrating life as a monkey where they, what do they do?

91
00:12:10,240 --> 00:12:16,400
They bite each other's eyeballs off someone said this morning. I've never seen that. I heard

92
00:12:16,400 --> 00:12:24,160
that they, they will bite each other's faces off. It's pretty gruesome. Life as a ghost,

93
00:12:24,160 --> 00:12:29,600
not worth celebrating. Lots of ghosts out there apparently that there was a ghost that was

94
00:12:30,240 --> 00:12:36,160
backbighter. He would backbiting as an English term in English,

95
00:12:36,160 --> 00:12:42,400
idiom it means speaking behind people's back. You're all nice to their face, but you bite them

96
00:12:42,400 --> 00:12:47,680
on the back means you talk bad about them. You make everyone hate them. Spread rumors,

97
00:12:47,680 --> 00:12:54,160
bad rumors about them, that sort of thing. When he was reborn, he actually had long nails and he

98
00:12:54,160 --> 00:13:02,800
actually gouged flesh out of his back. It was just in such a crazed state of mind that this

99
00:13:02,800 --> 00:13:14,080
is what he would do. Life as a human might be worth celebrating, might not. Some humans obviously,

100
00:13:14,080 --> 00:13:19,440
their lives are not worth celebrating and that's objective. It's not a judgment or a hatred of

101
00:13:19,440 --> 00:13:26,560
those people. It's just unfortunate that they're wasting their lives. Most of us waste part of

102
00:13:26,560 --> 00:13:33,760
our parts of our lives and that's in Buddhism when we strive to change. We take people like the

103
00:13:33,760 --> 00:13:40,720
Buddha as our example of someone who didn't waste his life. He wasted 29 years of his life to some

104
00:13:40,720 --> 00:14:00,080
extent and then another six more he wasted. When he turned 35, he stopped wasting time.

105
00:14:02,720 --> 00:14:08,720
And so we remember the qualities of the Buddha. We remember the greatness in the 45 years that

106
00:14:08,720 --> 00:14:15,920
he spent teaching tirelessly. That's a celebration of his life.

107
00:14:21,280 --> 00:14:24,800
When we talk about ourselves living a life worth celebrating,

108
00:14:24,800 --> 00:14:42,560
we can think of the Buddha's words. In some religious people talk about eternal life.

109
00:14:46,640 --> 00:14:52,400
The problem with celebrating life from a Buddhist perspective is that

110
00:14:52,400 --> 00:14:58,560
it's wrong-headed whether it be celebrating the short time you have on life. Carpe diem,

111
00:14:58,560 --> 00:15:06,960
you only live once that sort of thing. Whether it be the Hindu sense of just enjoying the game

112
00:15:06,960 --> 00:15:12,160
that you're playing from life to life. It's just delusion. It's a game for whether it be the

113
00:15:13,200 --> 00:15:16,720
Christian idea or the Muslim idea of eternal heaven.

114
00:15:16,720 --> 00:15:30,000
It's because it focuses on results. It focuses on the enjoyment of happiness and people

115
00:15:30,000 --> 00:15:36,320
even whether they be humans or even angels. When they get caught up in enjoying happiness they

116
00:15:37,520 --> 00:15:42,160
lose track of what it is that allows them to find happiness.

117
00:15:42,160 --> 00:15:53,040
Why is it that we're even born humans, but more why are we surrounded by good people?

118
00:15:54,240 --> 00:16:03,200
Why are our relationships beautiful and harmonious and conversely why are they not?

119
00:16:03,200 --> 00:16:11,840
People who when we take up this philosophy of celebrating enjoying life

120
00:16:14,560 --> 00:16:19,520
even the idea of an eternal life you know how it would happen to be talked about it would be

121
00:16:19,520 --> 00:16:26,720
an eternal life of enjoyment of pleasure. Something about surrounded by beautiful women is a

122
00:16:26,720 --> 00:16:37,520
thing you hear in some crazy religious cults. We don't realize what the the consequences of

123
00:16:37,520 --> 00:16:42,000
that sort of central enjoyment are that the people who engage in those sorts of

124
00:16:43,840 --> 00:16:53,440
head andism are the first to fight with each other to manipulate others, to be jealous,

125
00:16:53,440 --> 00:17:01,120
to be frustrated and angry and irritated. We don't realize how how these qualities of mind

126
00:17:01,120 --> 00:17:11,600
arrives in in in proportion to our fixation on happiness on pleasure.

127
00:17:11,600 --> 00:17:24,880
And it it becomes more clear as you become more mindful that your happiness your actual

128
00:17:24,880 --> 00:17:33,280
true happiness is only in in in in proportion to your it's in proportion to the cause that you

129
00:17:33,280 --> 00:17:40,720
you you build you you grow you you plant

130
00:17:44,320 --> 00:17:50,480
when you cultivate the cause and so Buddhism that's why living a life worth celebrating is a

131
00:17:50,480 --> 00:17:58,080
much more way of better way of talking about it because the beautiful and wholesome and

132
00:17:58,080 --> 00:18:10,240
the good qualities that you develop are the cause of all happiness the harmony that comes

133
00:18:10,960 --> 00:18:16,000
in someone who is generous and kind and caring and thoughtful and mindful and wise.

134
00:18:16,000 --> 00:18:25,440
The interactions and the relationship with their surroundings

135
00:18:27,600 --> 00:18:35,360
you know how how peaceful we can be on this this pilgrimage even though we might be surrounded

136
00:18:35,360 --> 00:18:43,440
for some of us we've never experienced the craziness and the chaos and predictability of India

137
00:18:43,440 --> 00:18:56,640
how about of a developing country and yet even though this might be very foreign to you

138
00:18:56,640 --> 00:19:00,080
you can see how mindfulness helps you to stay calm and to stay peaceful.

139
00:19:03,280 --> 00:19:08,480
And also let's you see that not everyone lives in such luxury as we might have grown up with

140
00:19:08,480 --> 00:19:18,640
and we can see that simply engage indulging in the the pleasure that we seem to have been so lucky

141
00:19:18,640 --> 00:19:23,920
to just lucky almost we don't even know why we're here Buddhism we would say well there's karma

142
00:19:23,920 --> 00:19:33,280
involved right but regardless it's certainly not everyone who has the potential to enjoy life

143
00:19:33,280 --> 00:19:41,440
the way we might because one of the things that the Buddha saw they saw that even as a king he

144
00:19:42,800 --> 00:19:48,240
he couldn't escape this potential that he saw another people old people sick people

145
00:19:51,760 --> 00:19:55,600
he realized that it's not a it's not a tenable philosophy to just enjoy

146
00:19:55,600 --> 00:20:06,080
so celebrating life becomes problematic unless it means living a life worth celebrating which is of

147
00:20:06,080 --> 00:20:13,440
course much better so how do we live a life worth celebrating this is what I was getting at the

148
00:20:13,440 --> 00:20:29,360
Buddha's words in one of the Dhamapada verses he says

149
00:20:44,160 --> 00:20:53,440
Mahada means intoxication Bhamada is also intoxication but here it doesn't mean through

150
00:20:55,040 --> 00:20:58,960
it means the sort of metaphorical intoxication that we talk about figurative

151
00:21:00,080 --> 00:21:05,600
intoxication that we talk about being intoxicated by sensuality being intoxicated by youth

152
00:21:05,600 --> 00:21:17,520
strength being intoxicated with life being intoxicated with health feeling very healthy

153
00:21:19,040 --> 00:21:23,360
being negligent is how we often translate it but intoxicated is really the literal

154
00:21:23,360 --> 00:21:31,840
so upper Mahada means not not intoxicated having a clear mind is exactly it having a sober might

155
00:21:31,840 --> 00:21:38,800
not be sober is perfectly literal but it just has connotations it really means what is about

156
00:21:38,800 --> 00:21:44,560
sobriety it means your mind is still clear unaffected unenibrated unintoxic here

157
00:21:44,560 --> 00:22:04,160
upper Mahada it's the path the path to not dying so if you ever were to the Buddha never talked

158
00:22:04,160 --> 00:22:08,560
about eternal life it would be a little bit misleading but he talked about not dying

159
00:22:08,560 --> 00:22:15,600
you know if they die a pamado amatapadang pamado muchunupadang

160
00:22:19,600 --> 00:22:24,160
intoxication is the path to death the path of death

161
00:22:24,160 --> 00:22:37,040
a pamata an amianti those who are unintoxicated never die not meanti don't die

162
00:22:39,280 --> 00:22:43,440
yepamata yatamata yepamata yepamata yatamata

163
00:22:43,440 --> 00:22:54,160
those yepamata those who are intoxicated yata are like those

164
00:22:57,680 --> 00:23:03,440
mata who are dead someone who is intoxicated is already dead

165
00:23:06,000 --> 00:23:12,000
so it's a life not worse not worth celebrating it's not even really a life and so

166
00:23:12,000 --> 00:23:17,040
one of the problems people often have with Buddhism is it's about letting go it's about leaving

167
00:23:17,040 --> 00:23:21,040
some sara it's about freeing yourself from the rounds of rebirth

168
00:23:25,680 --> 00:23:35,360
but it turns out that it's really about finding a cultivating and attaining a state of life

169
00:23:35,360 --> 00:23:43,920
that is worth living that is worth celebrating that is how cause of peace and happiness

170
00:23:45,760 --> 00:23:52,640
that of all the lives that even though you might be born again and again and somehow rejoice in

171
00:23:52,640 --> 00:24:04,560
that fact without mindfulness without clarity of mind you're not really alive there's so much

172
00:24:04,560 --> 00:24:14,160
stress and suffering and blindness like a zombie you know people think of people they talk

173
00:24:14,160 --> 00:24:18,560
about us who practice meditation oh you look like zombies you must just be like zombies

174
00:24:19,440 --> 00:24:24,480
and it's exactly the opposite before we practice mindfulness we're like zombies

175
00:24:24,480 --> 00:24:34,000
and mindfulness is the the key component of sobriety or non-intoxication in Buddhism

176
00:24:36,960 --> 00:24:45,120
satiya alipo aso apamato tiyoty when do we call apamada when you're mindful when you never

177
00:24:45,120 --> 00:24:52,480
without mindfulness mindfulness meaning to remember mindfulness means the state of being having a clear

178
00:24:52,480 --> 00:24:57,840
mind being completely present with the experience means to remember literally right so what

179
00:24:57,840 --> 00:25:04,480
that means is you remember the present moment there's not a slipping away from the experience

180
00:25:04,480 --> 00:25:12,480
into reaction into extrapolation into identification and so we come right back to this idea of the

181
00:25:12,480 --> 00:25:22,400
five aggregates there are our life they are our reality they are experienced and so they are

182
00:25:22,400 --> 00:25:30,080
object of mindfulness our object of meditation when we see when we hear when we smell when we taste

183
00:25:30,080 --> 00:25:39,040
when we feel when we think the quality of mind that is clearly aware without judgment of any sort

184
00:25:39,040 --> 00:25:45,760
or identification or attachment without anything else without getting lost when the mind knows seeing

185
00:25:45,760 --> 00:25:53,840
is seeing deep-tay-ditamatang seeing is just seeing sutte sutamatang hearing is just hearing

186
00:25:55,280 --> 00:26:00,640
when we attain that it actually means we're just living it's true life

187
00:26:02,880 --> 00:26:08,160
what would it mean to live life to its fullest from a Buddhist perspective means you wouldn't

188
00:26:08,160 --> 00:26:14,160
when you walk you would be there when you go somewhere you are there when you talk to someone

189
00:26:14,160 --> 00:26:22,480
you are talking not you are talking but the act of talking is the reality not oh I hate this

190
00:26:22,480 --> 00:26:26,480
person I should stop talking to them oh I really like this person how can I manipulate them to

191
00:26:26,480 --> 00:26:34,640
like me oh I'm worried am I going to say the right thing in the wrong thing instead talking is

192
00:26:34,640 --> 00:26:41,680
your reality walking is your reality eating drinking is your reality when you eat you eat you don't

193
00:26:41,680 --> 00:27:01,520
talk or think crave or like and that's your presence you are here and now and though it might

194
00:27:04,880 --> 00:27:09,440
rationally speaking think that you lose a lot because oh then you can't like things or just

195
00:27:09,440 --> 00:27:17,600
know there's no personality there why we why we continue this is because of what we've seen

196
00:27:17,600 --> 00:27:24,800
how how peaceful that state is because the truth of it is that you let go of a lot you let go

197
00:27:24,800 --> 00:27:34,320
of all your baggage that our our attachments our partialities our judgments are mostly just a

198
00:27:34,320 --> 00:27:40,080
lot of baggage that causes us stress and suffering we come to meditation practice usually because

199
00:27:40,080 --> 00:27:48,240
we wonder why do I suffer so much why why is there stress in my mind people who have mental

200
00:27:48,240 --> 00:27:57,840
illnesses come wondering how can I free myself why are these why am I so depressed so anxious

201
00:27:57,840 --> 00:28:05,120
how can I free myself from these things please any way to free myself from these things and we

202
00:28:05,120 --> 00:28:13,120
realize that it's all just our inability to be present and we find great peace and happiness

203
00:28:14,560 --> 00:28:25,120
freedom from suffering we find life in the present moment so that's my thoughts on life so far

204
00:28:25,120 --> 00:28:33,680
we've done birth death life one more talking about Gaia and then we're going home thank you all

205
00:28:33,680 --> 00:28:57,120
for this

