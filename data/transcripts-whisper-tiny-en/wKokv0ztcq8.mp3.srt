1
00:00:00,000 --> 00:00:06,000
Could you please tell me what is the minimum amount of time to spend in meditation daily to become enlightened?

2
00:00:06,000 --> 00:00:09,000
Yes.

3
00:00:09,000 --> 00:00:15,000
It would be nice, you know. It was so easy.

4
00:00:15,000 --> 00:00:23,000
It's like a diet, you know. But it's not really like that.

5
00:00:23,000 --> 00:00:29,000
It's not a matter of time, it's a matter of quality of mind.

6
00:00:29,000 --> 00:00:42,000
If one is truly mindful, then it can be accomplished in a matter of days.

7
00:00:42,000 --> 00:00:49,000
But it's not a matter of time, it's a amount of times that you're mindful, because you can only be mindful in one moment.

8
00:00:49,000 --> 00:00:54,000
It's only cultivate meditation in the present moment, and that's once.

9
00:00:54,000 --> 00:01:04,000
So, meditation is cultivated based on times, number of times that you do something, and frequency.

10
00:01:04,000 --> 00:01:13,000
There's an optimum frequency for meditating that Masi Sayadha said once a second.

11
00:01:13,000 --> 00:01:30,000
If you're able to be aware of one thing per second, it's a good estimate or estimate of rough estimate of how often you should be.

12
00:01:30,000 --> 00:01:33,000
You optimally be mindful.

13
00:01:33,000 --> 00:01:43,000
Much of course he says it's not possible in the beginning, but it's something that you cultivate and develop.

14
00:01:43,000 --> 00:01:50,000
Actually, there are people who claim to get good results by doing meditation in the morning and in the evening, and are able to develop states.

15
00:01:50,000 --> 00:02:01,000
I've even led people to the point where I got the feeling, or got the impression from what they said, that they actually had some state of realization.

16
00:02:01,000 --> 00:02:20,000
You might even say, maybe they're in enlightened being, or sort upon or something, but not something that one can be sure of.

17
00:02:20,000 --> 00:02:24,000
The biggest problem with your question is that everyone is different.

18
00:02:24,000 --> 00:02:33,000
Put a judgment on that. Some people can be mindful just a few, one monk, three steps, and he was enlightened.

19
00:02:33,000 --> 00:02:43,000
So, frequency is important, but it should be frequency of times, and it should be quality of mind.

20
00:02:43,000 --> 00:03:04,000
The point of doing it frequently, and of doing it over the long term, is to create the habit, and to create the nature of the mind that is clear, that is alert, that is...

21
00:03:04,000 --> 00:03:12,000
There's no word for it in the English language that I know that is able to see things as they are.

22
00:03:12,000 --> 00:03:15,000
Thank you very much.

