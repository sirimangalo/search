1
00:00:00,000 --> 00:00:13,640
I can't believe anyone, tonight we are not just in line, but I'm not trying them to

2
00:00:13,640 --> 00:00:25,760
find it again. It's time 1.30. It's actually almost the same as 1.29. It's not too much to say that we're

3
00:00:25,760 --> 00:00:38,920
going an order or so. Hello and welcome back to our study of the demo panel. Today we continue

4
00:00:38,920 --> 00:01:07,680
on with 1.30 which reads as follows, which is almost the same as the last one. It means all

5
00:01:07,680 --> 00:01:15,400
tremble at the rod. This is different. This is the different. This is G. R. Dangbian. Life is

6
00:01:15,400 --> 00:01:24,760
dear, some day, some to all. Life is dear to all. And then the same as the last one,

7
00:01:24,760 --> 00:01:50,760
the Buddha laid down two rules. And so the last story, it's about how he laid down the

8
00:01:50,760 --> 00:02:00,040
rule against hitting other monks. The group of six monks who were infamous for their escapades breaking

9
00:02:00,040 --> 00:02:14,640
in a lot of rules and being the instigation for establishing many rules. They hit a group of 17

10
00:02:14,640 --> 00:02:22,200
monks who had taken a president. So this one doesn't say what actually happened, but for some

11
00:02:22,200 --> 00:02:32,280
reason rather they were angry at these satarasabhagi, a group of 17 monks. But in this case,

12
00:02:32,280 --> 00:02:38,040
it's interesting. The English translation, I think, gets it wrong. The English translation says

13
00:02:38,040 --> 00:02:46,560
the hit, and then this group of 17 monks held up their fist in response to scare them away.

14
00:02:46,560 --> 00:02:53,560
But I don't think that's what Pauli actually says. I can't quite get to get it for sure,

15
00:02:53,560 --> 00:02:58,920
but it certainly doesn't make it clear that it was in response. It appears to say that just

16
00:02:58,920 --> 00:03:05,400
like in the last one, where they actually hit the group of 17. In this one, the group of six

17
00:03:05,400 --> 00:03:14,440
monks just held up their fists, just scared to scare them. And again, the monks screamed. And

18
00:03:14,440 --> 00:03:23,880
again, the Buddha heard and asked what that scream all about, and someone told him. And he said,

19
00:03:23,880 --> 00:03:46,880
And then he explained that he says,

20
00:03:46,880 --> 00:04:13,080
Just as as I so others tremble at the rod. Just as to me, so to others, life is dear.

21
00:04:16,880 --> 00:04:44,600
And so this is sort of the, as I already said, this is sort of the Buddhist version of the

22
00:04:44,600 --> 00:04:51,280
golden rule of doing under weather, so they were doing to you. But there's two interesting points

23
00:04:51,280 --> 00:04:57,320
that I can talk about for this fun. The first is that they haven't actually done anything.

24
00:04:57,320 --> 00:05:07,720
You know, they didn't actually harm the other monks. And so often we place a lot of emphasis on

25
00:05:07,720 --> 00:05:14,920
the action, you know, when you actually harm someone. And it's not, it's not any deeper

26
00:05:14,920 --> 00:05:22,400
or esoteric teaching, but it's interesting to remind ourselves that it's not actually,

27
00:05:22,400 --> 00:05:30,520
it's not actually the acts itself. It's our own officiousness in mind. When you raise your hand

28
00:05:30,520 --> 00:05:39,240
against someone, you're just raising your hand. You know, a lot of abuse is not even physical. It's

29
00:05:39,240 --> 00:05:51,200
emotional. It's the fear. And oftentimes it's the fear itself that is more harmful, that the actual

30
00:05:51,200 --> 00:06:01,320
bruises, the actual scars, feel much easier, much more completely than the mental scars.

31
00:06:01,320 --> 00:06:08,280
When you hear stories about parents who tell their kids to go get them their belt, right,

32
00:06:08,280 --> 00:06:19,960
go get me my belt. You imagine knowing, like when you know what the meaning is, you have to

33
00:06:19,960 --> 00:06:32,920
actually go and bring them the weapon they're going to use against you. And trauma. So that's

34
00:06:32,920 --> 00:06:38,720
one thing is remembering, especially in the premeditation purposes, you know, remembering to

35
00:06:38,720 --> 00:06:45,640
focus on our mind states, our car intentions. Does it matter whether you actually kill or harm

36
00:06:45,640 --> 00:07:00,240
someone? It's the viciousness, cruelty, the lack of compassion, the slavery to anger, you know,

37
00:07:00,240 --> 00:07:07,280
getting lost in our own anger so we can't even see what we're doing. And that's the second

38
00:07:07,280 --> 00:07:15,320
thing to point out is that if this isn't just an intellectual exercise where we say, yes, I don't want

39
00:07:15,320 --> 00:07:19,880
to be harmed and therefore it makes sense that I shouldn't harm others. It's not like that.

40
00:07:19,880 --> 00:07:28,520
It's the very fact that you don't want to be harmed yourself makes it perverse for you to harm

41
00:07:28,520 --> 00:07:39,320
others. And that's what makes it, or that's one way of explaining what makes it. That when you

42
00:07:39,320 --> 00:07:46,040
harm others, it comes back to harm you and you subject yourself to harm because you know in

43
00:07:46,040 --> 00:07:53,440
your mind that this is wrong. There's a sentence that this is an evil thing. And by evil, I just mean

44
00:07:53,440 --> 00:07:59,360
something you wouldn't want to happen to you. And because you know that, when you do it to

45
00:07:59,360 --> 00:08:17,160
others, it leaves a scar on your mind. It perverts, your very reality, because doing things that

46
00:08:17,160 --> 00:08:22,800
you want to happen to you is easy because there's a sense of the goodness of them. This is a good

47
00:08:22,800 --> 00:08:28,440
thing to do. So you do it for someone else. Why? Because it's a good thing. You don't have to feel the

48
00:08:28,440 --> 00:08:32,480
other person's happiness. You know that this is something that leads to happiness. So when you do

49
00:08:32,480 --> 00:08:39,120
it for them, it makes sense. To do something that causes harm to someone else actually is perverse

50
00:08:39,120 --> 00:08:46,120
because you know the harm that you know that this is an evil thing. And by evil again, this is a

51
00:08:46,120 --> 00:08:53,440
suffering thing, a thing that leads to suffering. So anyone who says that you should try and

52
00:08:53,440 --> 00:09:01,800
maximize your own happiness, even at the expense of others, or who argues that anyone who says

53
00:09:01,800 --> 00:09:06,000
look out for yourself first is being selfish because then they go and harm others. That's

54
00:09:06,000 --> 00:09:12,120
what really understand how it works. You can't be selfish and work for your own benefit.

55
00:09:12,120 --> 00:09:17,640
Anyone who is selfish is not working for their own benefit because they're doing things

56
00:09:17,640 --> 00:09:23,080
that they know are wrong. And again, wrong simply means things that lead to suffering.

57
00:09:23,080 --> 00:09:30,640
It's wrong because it leads to suffering and there's no distinction that can be made between

58
00:09:30,640 --> 00:09:35,520
the suffering for oneself or the suffering for another. Because in your mind, there's an

59
00:09:35,520 --> 00:09:43,920
awareness, there's an understanding that this is a cause for yourself. There's that.

60
00:09:43,920 --> 00:09:56,120
So it it necessarily involves anger, involves unwholesome mind state. And it leads you to

61
00:09:56,120 --> 00:10:06,000
be susceptible to harm yourself when you harm others. I mean, the practical fallout is

62
00:10:06,000 --> 00:10:19,680
that you'll be afraid and you'll put yourself in the situation. You're paranoid about

63
00:10:19,680 --> 00:10:24,840
retribution. When you die, it will be an obsession and your mind is the kind of thing that

64
00:10:24,840 --> 00:10:31,120
comes back to haunt you when you harm the harm you've done to others. And so when you

65
00:10:31,120 --> 00:10:38,440
die, you end up with that as part of your rebirth. But that is part of who you are. Potentially

66
00:10:38,440 --> 00:10:44,200
leading you to hell even, but more likely just to lead you to a situation and abusive

67
00:10:44,200 --> 00:10:53,160
situation. Often this is what leads to these cycles of retribution where one person

68
00:10:53,160 --> 00:10:59,160
is one person harms another, then they're both reborn and the other person, one person

69
00:10:59,160 --> 00:11:04,920
is the aggressor, the others, the victim and the other cycles. It's kind of thing can happen.

70
00:11:04,920 --> 00:11:10,120
Because of the psychology of it, you harm others and you convert your own situation.

71
00:11:10,120 --> 00:11:19,160
There's a sense that that is how common it works. It's a kind of thing that we're looking

72
00:11:19,160 --> 00:11:30,920
to discover through our practice, how these things affect our mind. So it's not just a pretty

73
00:11:30,920 --> 00:11:35,080
saying, yes, don't harm others because you wouldn't want that to happen to you. You're

74
00:11:35,080 --> 00:11:42,840
actually some depth to it. That in fact, that's a very much intrinsic part of how the mind

75
00:11:42,840 --> 00:11:53,480
works. So too much to talk about because it's very similar, but we're continuing on. We've

76
00:11:53,480 --> 00:12:01,880
started the tenants chapter. This is first in the two. So next to number one, thank you

77
00:12:01,880 --> 00:12:17,800
all for tuning in. I'll show you all the best.

78
00:12:31,880 --> 00:12:44,760
Oh, does anyone have any questions? Pull up the meditation page. We're just doing our

79
00:12:44,760 --> 00:13:09,880
video tonight.

80
00:13:14,760 --> 00:13:27,640
You Americans are going to move to Canada after the election. It looks kind of scary. But

81
00:13:27,640 --> 00:13:33,720
you know America's been a scary place for quite some time. America has a very bad history

82
00:13:33,720 --> 00:13:41,320
if you believe what they say and it's not really hard to believe. Well, it's pretty clear

83
00:13:41,320 --> 00:13:53,560
what the U.S. has done to Central America, South and Central America. Yeah, what they've done

84
00:13:53,560 --> 00:14:01,080
around the world. They've done to the Middle East, why the Middle, why Middle Eastern people

85
00:14:01,080 --> 00:14:09,080
really dislike Americans a lot. They have some reason to dislike your country. So you're all welcome

86
00:14:09,080 --> 00:14:25,800
in Canada. I mean, from my perspective anyway. But I wouldn't be too concerned. It's

87
00:14:25,800 --> 00:14:31,680
not like it's going to get really, really bad right away. I mean, what's great about America

88
00:14:31,680 --> 00:14:45,480
is there's there's there are people who are young and have a new mindset and appear to be

89
00:14:45,480 --> 00:14:58,440
rather rather empathic, empathic, empathetic, sympathetic. At the very least appear to be fairly

90
00:14:58,440 --> 00:15:10,200
global in their thinking in terms of unpregidist and so on. Anyway, the real question,

91
00:15:10,200 --> 00:15:14,400
does consciousness correlate with defilement of a person's thoughts, speech and conduct

92
00:15:14,400 --> 00:15:19,640
with early defile, does that person's early unconscious person's thoughts, speech and

93
00:15:19,640 --> 00:15:30,480
conduct of a person's more conscious? No, there's no sense of that. It depends, I guess,

94
00:15:30,480 --> 00:15:38,400
what you mean by conscious. If by that you mean a mindful, you know, that's usually the

95
00:15:38,400 --> 00:15:49,600
word we use to describe someone. But I suppose, you know, but it's kind of more poetic

96
00:15:49,600 --> 00:15:56,400
way or what's the word figurative, not literally or in an ultimate sense. It's more of a

97
00:15:56,400 --> 00:16:02,400
conventional way of speaking. Like the Buddha said, the mindful are as though already dead.

98
00:16:02,400 --> 00:16:07,040
And that's really the truth. If you've never meditated before, you've never really practiced

99
00:16:07,040 --> 00:16:13,280
being mindful. It's like going through life asleep. And when you first start to meditate,

100
00:16:13,280 --> 00:16:20,320
it's like waking up. It's like a splash of cold water on your face. So the quality of consciousness,

101
00:16:20,320 --> 00:16:23,760
from a technical point of view, it's not that you're less conscious, it's that the quality

102
00:16:23,760 --> 00:16:33,200
of the consciousness is fairly low for someone who has not meditated. It's very weak,

103
00:16:33,200 --> 00:16:40,440
fairly weak, you can say, but it's not less conscious, technically speaking. Though in one

104
00:16:40,440 --> 00:16:47,400
manner of speaking, it does, it is, I think, proper to say. It's possible to say it that way.

105
00:16:51,880 --> 00:16:57,480
Spiritually conscious. Yeah, I mean, just generally, I think it's fine to say less conscious,

106
00:16:57,480 --> 00:17:02,520
more conscious. Like when that's how we use the word in English, conscious of what you're doing,

107
00:17:05,000 --> 00:17:09,000
think of a drunk person, a drunk person is not very conscious of what they're doing,

108
00:17:09,000 --> 00:17:15,800
they're conscious. And from Buddha's perspective, consciousness is still present. And they may be

109
00:17:15,800 --> 00:17:25,320
less conscious. They're certainly less conscientious. Some guys have a question.

110
00:17:28,840 --> 00:17:35,160
Can we say that Tanha is the liking of the sensation of Upa, Upa, Tanha is wanting it back.

111
00:17:35,160 --> 00:17:45,960
No, Tanha and Upa, Tanha are technically the same thing. Tanha is just craving, and Upa,

112
00:17:45,960 --> 00:17:57,480
Tanha is stronger craving. But see, this is why, but it just is Upa, it really gets too much

113
00:17:57,480 --> 00:18:08,360
focused. In one sense, people look to it as sort of like the be-all end-all of doctrine when

114
00:18:08,360 --> 00:18:15,640
it's not. It's very, fairly, fairly conventional in the way it presents things. I mean, it's

115
00:18:15,640 --> 00:18:22,360
deep, and it's profound, and it's important. But it's not the ultimate explanation. The Upa,

116
00:18:22,360 --> 00:18:29,640
Tanha is, and that explanation takes like 400 cartloads of books.

117
00:18:33,000 --> 00:18:39,320
So, what the Buddha is saying here is that craving leads to clinging. It's just, when you want something,

118
00:18:39,320 --> 00:18:45,400
you cling to it. But technically, the mind is doing the same thing whether it wants or whether it

119
00:18:45,400 --> 00:18:51,880
clings. So, it's a conventional manner of speech. There's no difference between Dhanana, Upa, Dhanana,

120
00:18:51,880 --> 00:18:56,520
from my understanding, except, and I'm pretty sure this is how the commentary explains it,

121
00:18:57,160 --> 00:19:03,160
except in terms of the intensity. So, Upa, Dhanana just is more intense. But conventionally, when we

122
00:19:03,160 --> 00:19:07,000
talk about it, we talk about craving and clinging. When you crave something, you'll cling to it.

123
00:19:07,960 --> 00:19:11,720
But the mind that craves and the mind that clings are actually technically the same.

124
00:19:11,720 --> 00:19:22,120
It's the same mind. So, a bit interesting about it is conventionally conventional in its

125
00:19:22,120 --> 00:19:27,640
presentation. Much of it is ultimate, and it's talking about ultimate realities. But it's not,

126
00:19:27,640 --> 00:19:41,640
it's not the ultimate explanation. The Abhidhamma is. That's what the Abhidhamma is for.

127
00:19:44,680 --> 00:19:47,560
Anyway, so are we.

128
00:19:47,560 --> 00:19:58,760
We all caught up to many questions. What am I doing these days? We have a couple of meditators

129
00:19:58,760 --> 00:20:08,920
here. I'm in Michael's doing a course in Mark from Germany. And Elaine, a local woman has been

130
00:20:08,920 --> 00:20:18,040
coming by. She's coming by again tomorrow morning. She's been quite interested and she calls me

131
00:20:18,040 --> 00:20:25,880
on the phone every software, which is great. It's nice to have to be able to help. She's going

132
00:20:25,880 --> 00:20:38,920
through some stress. I'm studying Sanskrit, but I'm hoping to, because I've got a Sanskrit class in

133
00:20:38,920 --> 00:20:47,800
the fall, I think, so I've got to review my 13-year-old knowledge. How long did I study Sanskrit

134
00:20:47,800 --> 00:21:00,440
many years ago? I'm hoping to work with some people to make this, to redo our meditation.serimungalow.org.

135
00:21:01,080 --> 00:21:07,560
Hopefully we can do that soon. This month, this month would be the time. We can also work on it

136
00:21:07,560 --> 00:21:17,400
over the summer. Also, going back to New York, we're going to visit Bhikkhu Bodhi. I've been corralled

137
00:21:17,400 --> 00:21:23,880
into trying to persuade him to come to Canada to give a talk. Let's see how that goes.

138
00:21:23,880 --> 00:21:40,200
Never met him, actually, so it'll be nice to go visit.

139
00:21:40,200 --> 00:21:46,920
Oh, you have a question. Crossing into Canada from the States, I don't have any

140
00:21:46,920 --> 00:21:56,680
kid in Canada passport. You need a passport, I think. I'm pretty sure you can get one. It might

141
00:21:56,680 --> 00:22:09,880
be difficult, but I think it's your legal right to get one. I mean, I don't know. I would assume.

142
00:22:09,880 --> 00:22:18,520
The mother's a monk here wants to invite him to give a talk and he wants my help. If I see

143
00:22:18,520 --> 00:22:22,920
invited him once and he refused, so I said, well, you should go see him. And he said, okay,

144
00:22:22,920 --> 00:22:29,240
well, you come with me. It looks like I'm going with him. He just called me from Winnipeg.

145
00:22:29,240 --> 00:22:36,840
He's a really nice guy. Good friend. I think he follows the lotus sutra. Unfortunately,

146
00:22:36,840 --> 00:22:41,880
so it's a little bit disconcerting because as it turns on, our Buddhism, our understanding of

147
00:22:41,880 --> 00:22:45,160
Buddhism is quite different, but it doesn't mean we can't be friends.

148
00:22:45,160 --> 00:23:10,440
Anyway, yes, that's all for tonight. Have a good night, everyone.

