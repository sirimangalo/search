1
00:00:00,000 --> 00:00:06,680
How does one take the middle way in regards, for example, to food?

2
00:00:06,680 --> 00:00:13,040
We should eat enough to have energy for all day, all our daily duties.

3
00:00:13,040 --> 00:00:17,240
But when we are not sure what is enough, we should rather eat more than less because negative

4
00:00:17,240 --> 00:00:23,680
effects from restraining are more destructive than negative effects from indulging ourselves.

5
00:00:23,680 --> 00:00:33,680
Is this right?

6
00:00:33,680 --> 00:00:38,880
Well, I think it depends on what you are doing because obviously you are still living in

7
00:00:38,880 --> 00:00:39,880
the world.

8
00:00:39,880 --> 00:00:47,280
I think that would make it easier to judge how much you need really because if you are doing

9
00:00:47,280 --> 00:00:52,840
this, not like every time you eat, you have to sit down and have this whole meal, you could

10
00:00:52,840 --> 00:00:57,920
eat small things at various times instead.

11
00:00:57,920 --> 00:01:05,800
I think that that would make it a lot easier to regulate how much you take in and see how

12
00:01:05,800 --> 00:01:11,320
much you really need unless you are keeping aid precepts than you have to eat before

13
00:01:11,320 --> 00:01:20,920
noon, but indulging or overly restraining can be bad.

14
00:01:20,920 --> 00:01:26,080
I think it is going to be a matter of experimentation, really.

15
00:01:26,080 --> 00:01:35,160
You just have to realize how much energy you are using during the day you need accordingly.

16
00:01:35,160 --> 00:01:52,800
Yeah, I had the same thought that when you are working, you have another need for calories

17
00:01:52,800 --> 00:01:58,640
or for nutrients, then when you are just sitting and meditating.

18
00:01:58,640 --> 00:02:06,720
So as a layperson, you need more food and you need eventually a third meal.

19
00:02:06,720 --> 00:02:14,400
When you are a meditator here on a retreat, one meal could be enough or two meals.

20
00:02:14,400 --> 00:02:22,000
When you are on eight precepts and you have to work and you feel hungry, then something

21
00:02:22,000 --> 00:02:31,720
like a juice or so in the afternoon helps to overcome that hunger or that feeling of getting

22
00:02:31,720 --> 00:02:34,720
dizzy or getting nervous.

23
00:02:34,720 --> 00:02:43,640
Before I ordain, I thought, I need three meals, I need to eat a lot and I found out that

24
00:02:43,640 --> 00:02:53,640
I am fine with one meal when I am in meditation retreat and two meals when I am doing

25
00:02:53,640 --> 00:02:57,640
duties.

26
00:02:57,640 --> 00:03:04,800
It is happening in the mind as well that you think you really need it.

27
00:03:04,800 --> 00:03:13,960
You may have a little bit hunger in the evening when you start to get for two or one meals,

28
00:03:13,960 --> 00:03:27,400
but after a while you will find that this is nothing bad.

29
00:03:27,400 --> 00:03:28,400
For sure.

30
00:03:28,400 --> 00:03:36,000
I have a couple of things to say sort of corollaries, I think the questions are not even answered.

31
00:03:36,000 --> 00:03:44,560
First of all, to make clear that there are negative effects from indulging, that that

32
00:03:44,560 --> 00:03:52,200
may not be clear, food is a lot more dangerous than we make it out to be or that then

33
00:03:52,200 --> 00:03:56,960
we actually realize, because a lot of food will have an effect on your brain chemistry

34
00:03:56,960 --> 00:04:02,120
as well, a lot of rich food and fatty food and so on.

35
00:04:02,120 --> 00:04:11,000
Obviously you have negative effects from restraining in terms of the weakness that comes

36
00:04:11,000 --> 00:04:12,000
from it.

37
00:04:12,000 --> 00:04:18,700
Now, if you are living an ordinary life, an ordinary, if you are living a life in the human

38
00:04:18,700 --> 00:04:30,240
world, a human society, then you need to work, you need to think, you need to speak.

39
00:04:30,240 --> 00:04:37,800
So not having enough food can be dangerous in that regard, but in terms of our spirituality,

40
00:04:37,800 --> 00:04:45,920
it is really more, I would say, the other way that indulging is actually a lot more dangerous.

41
00:04:45,920 --> 00:04:52,400
It leads to lust or it supports lust to get stronger in the mind.

42
00:04:52,400 --> 00:04:56,160
It supports laziness to get stronger in the mind.

43
00:04:56,160 --> 00:05:00,920
It doesn't create these things in the mind, but because of the brain chemistry and the

44
00:05:00,920 --> 00:05:07,680
processes in the body and the blood flow and so on required to digest food and the chemicals

45
00:05:07,680 --> 00:05:13,440
that are added to the body, these feelings become stronger.

46
00:05:13,440 --> 00:05:17,560
This is why in the Buddhist time everyone was starving themselves because when they starved

47
00:05:17,560 --> 00:05:21,320
themselves or all the spiritual people, because when they starved themselves, these feelings

48
00:05:21,320 --> 00:05:25,360
just went away totally and they thought they were enlightened, wow, I don't get greedy

49
00:05:25,360 --> 00:05:34,640
at all because there is no chemicals left to have lust arise for the hormones to work.

50
00:05:34,640 --> 00:05:41,120
That's not the way out of suffering, but it points very much to the problem with going

51
00:05:41,120 --> 00:05:48,240
the other way, is that you're developing an incredible amount of these states.

52
00:05:48,240 --> 00:05:58,120
The other thing I wanted to say is, and it relates to the necessity to eat food in life

53
00:05:58,120 --> 00:06:04,760
and society, is that the question is what is the food for and this is a question that

54
00:06:04,760 --> 00:06:13,080
especially monks and meditators are not very clear on and often miss.

55
00:06:13,080 --> 00:06:21,200
So people think they will see the effects of eating less than they're used to in terms

56
00:06:21,200 --> 00:06:25,240
of maybe their wounds don't heal as quickly, maybe they don't have as much energy, they

57
00:06:25,240 --> 00:06:31,600
can see their muscles, atrophy, atrophying, their muscles getting smaller, maybe they're

58
00:06:31,600 --> 00:06:37,880
losing the color and the beauty in their face is getting thin and they can see the bone

59
00:06:37,880 --> 00:06:45,480
sticking out of their shoulders and this and then a million other things are excuses

60
00:06:45,480 --> 00:06:48,800
that people come up with to eat more.

61
00:06:48,800 --> 00:06:54,000
And this is what constantly I was getting in Los Angeles, for example, you're too thin,

62
00:06:54,000 --> 00:06:57,160
you have to eat more, you're not eating enough and so on.

63
00:06:57,160 --> 00:07:03,880
And the question is enough for what, because the Buddha's idea of eating enough is enough

64
00:07:03,880 --> 00:07:09,120
to live, enough to survive, if the food that you're eating is not enough for you to survive

65
00:07:09,120 --> 00:07:16,440
on and not enough for you to carry out your duties correctly, then it's not enough.

66
00:07:16,440 --> 00:07:20,280
But if the food you eat is enough for you to carry out your duties, if with the food

67
00:07:20,280 --> 00:07:26,600
you eat you can walk back and forth five hours a day and do five hours of walking meditation

68
00:07:26,600 --> 00:07:31,560
then I would say that's enough food and that's really not a lot of food because you're

69
00:07:31,560 --> 00:07:36,080
walking quite slowly, so you do five hours of walking, five hours of sitting or six hours

70
00:07:36,080 --> 00:07:40,560
of walking, six hours of sitting, that's enough to live.

71
00:07:40,560 --> 00:07:45,120
Anything that you have to do apart from that, if you have to teach, if you have to work

72
00:07:45,120 --> 00:07:53,080
in the monastery, if you have to then go out and work in daily in society, then you add

73
00:07:53,080 --> 00:08:00,600
on to that extent, with this food, can I live, can I survive?

74
00:08:00,600 --> 00:08:10,520
Because that's really the point, the monk in the monastic is the ideal or the perfect example

75
00:08:10,520 --> 00:08:17,280
of the bare minimum of requirements, so we eat because we need the food, we wear clothes

76
00:08:17,280 --> 00:08:21,600
because we need something to cover our bodies, we have shelter because we need something

77
00:08:21,600 --> 00:08:28,640
to protect against the rain or also to give us solitude and so on.

78
00:08:28,640 --> 00:08:35,160
And we take medicine so that we don't suffer from horrible, painful feelings that are

79
00:08:35,160 --> 00:08:42,800
going to leave us or drive us crazy even, that are going to hurt our meditation practice

80
00:08:42,800 --> 00:08:47,120
or disrupt our meditation practice.

81
00:08:47,120 --> 00:08:55,360
So point being, as Polanyani also said, we need far less than we think that we need.

82
00:08:55,360 --> 00:09:03,560
And when you really think about what do I need this food for, you realize that you really

83
00:09:03,560 --> 00:09:06,600
need a lot less than you think.

84
00:09:06,600 --> 00:09:13,040
And the point is that we, rather than using this as a guide, we always will use things

85
00:09:13,040 --> 00:09:17,480
like hunger as a guide or partiality as a guide, right?

86
00:09:17,480 --> 00:09:27,320
I know I only need maybe some some crackers and some crackers and cheese and that's enough

87
00:09:27,320 --> 00:09:35,280
but chips are better, so I'll have chips instead or I know that I've had enough food

88
00:09:35,280 --> 00:09:42,600
but I'm still hungry, so I'll go out and eat more and we'll eat until we're not hungry.

89
00:09:42,600 --> 00:09:44,920
This is our measure.

90
00:09:44,920 --> 00:09:50,840
I had a friend when I was younger and he studied, he became a biologist, became a veterinarian

91
00:09:50,840 --> 00:09:55,880
actually and he once told me that the way this gland works and the brain, the hunger

92
00:09:55,880 --> 00:09:58,560
gland works, it's always firing.

93
00:09:58,560 --> 00:10:02,200
It's nature as to fire and say, I'm hungry, I'm hungry, I'm hungry, I'm hungry, I'm

94
00:10:02,200 --> 00:10:03,200
hungry.

95
00:10:03,200 --> 00:10:07,760
That's it's ordinary state and it's only by putting a whole bunch of food to make it

96
00:10:07,760 --> 00:10:17,600
shut up, somehow it gets something and then it switches off and then when the food disappears,

97
00:10:17,600 --> 00:10:22,440
it turns back on and starts saying, I'm hungry, I'm hungry, I'm hungry again.

98
00:10:22,440 --> 00:10:26,120
It doesn't necessarily have anything to do with your level of health or your physical

99
00:10:26,120 --> 00:10:29,480
requirements or even how much food you have in your stomach.

100
00:10:29,480 --> 00:10:33,800
You can have enough food in your stomach to survive and yet it hasn't hit this gland yet

101
00:10:33,800 --> 00:10:38,840
and so it's still saying, eat more, eat more, eat more, I'm hungry.

102
00:10:38,840 --> 00:10:45,800
Something we don't realize, hunger is not an indication that we have to eat more.

103
00:10:45,800 --> 00:10:49,080
It's not a perfect indication that we have to eat more.

104
00:10:49,080 --> 00:10:54,080
It's also something that has evolved biologically, it's not a spiritual gland, this gland

105
00:10:54,080 --> 00:10:56,120
is not our spiritual friend.

106
00:10:56,120 --> 00:11:06,240
If anything, it's a product produced by Mara, the factories of Mara because it tends

107
00:11:06,240 --> 00:11:11,200
to tell us we have to eat more and get fat and fat and save up for the winter or save

108
00:11:11,200 --> 00:11:16,560
up for the times when we have no food, how evolution has led us to have this gland.

109
00:11:16,560 --> 00:11:21,680
There's nothing to do with any sort of spiritual development.

110
00:11:21,680 --> 00:11:28,560
So a big point in learning how to find the middle way in regards as the example you give

111
00:11:28,560 --> 00:11:37,360
food is to stop listening to hunger and just be rational and understand how much food

112
00:11:37,360 --> 00:11:38,360
you need.

113
00:11:38,360 --> 00:11:46,440
Okay, sure when you're starving, have some food but don't use the hunger as a guide.

114
00:11:46,440 --> 00:11:50,760
And this is apparent when you come to practice meditation because if I know you've eaten

115
00:11:50,760 --> 00:11:51,760
enough, you don't need more.

116
00:11:51,760 --> 00:11:55,560
And yet in the evening you're still going, I'm hungry, I'm hungry, I'm hungry, I'm hungry.

117
00:11:55,560 --> 00:12:01,200
And I've had meditators tell me, I was in the evening, I was so hungry, I'll complain

118
00:12:01,200 --> 00:12:04,800
about it the first day, so you tell them, say hungry, hungry and they're, come on, I'm

119
00:12:04,800 --> 00:12:09,440
hungry, I need more food and then they'll finally try it and they'll come back and tell

120
00:12:09,440 --> 00:12:11,440
you that it's amazing.

121
00:12:11,440 --> 00:12:15,000
Last night I was so hungry, I thought I need some food, I'm going to die here, I can't

122
00:12:15,000 --> 00:12:20,440
stay here in another day and so, but finally I said to myself, hungry, hungry.

123
00:12:20,440 --> 00:12:24,680
I woke up in the morning, I wasn't hungry at all and they're able to see that actually

124
00:12:24,680 --> 00:12:29,680
it's just their body playing games with them because they're used to eating and because

125
00:12:29,680 --> 00:12:38,520
they're used to eating the mind says it's time for dinner, where's my dinner and the

126
00:12:38,520 --> 00:12:55,120
grandstand's working.

