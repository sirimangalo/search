Okay, hello and welcome back to our study of the Dhamma Padra.
Today we will be continuing on with verses 44 and 45 with a pair of verses which go as follows.
This is the Dhamma Padra.
This is the Dhamma Padra.
This is the Dhamma Padra.
This is the Dhamma Padra.
Who will bring to perfection this path of Dhamma or this practice of Dhamma that is well taught
just as a skillful person might bring to perfection a bouquet of flowers.
And then the answer is Saeko which means a trainee or one who is training.
One who trains themselves will overcome this earth, this world of Yama together with the angels,
together with its angels or all celestial beings.
The trainer will bring to perfection this path of Dhamma which is well-expounded just as a skillful person would bring to perfection a bouquet of flowers.
So this is actually something that is quite a profound question and answer, something that we can use here to expound the Dhamma or as a basis for teaching the Dhamma.
But the story, as there is a story behind all of these, we have to say where this verse came from, the story is that there were, it is actually a two paragraph story.
The story goes that there were a bunch of monks who were sitting around talking about the earth.
And as monks are people as well, their talk tends to wonder if they are not careful.
And so their talk had wandered into worldly subjects where they began to talk about the earth.
And so they were talking about how the earth in this district was black and the earth in that district was brown and how this district was a very muddy earth.
And this district had gravelly and sandy earth.
And the Buddha came in and asked them what they were talking about and they told them and they said that.
He used this verse as a reminder as a means of instilling some sort of sense of urgency in them.
He said, well that is the external earth but it really has no purpose for you to consider, to concern yourselves with when compared to the inner earth.
The earth that is inside of you.
And the real earth is this world of death. The physical realm that is subject to change.
This aging and fading away and this body that will eventually go back to the earth. The death that is ahead of us for this world which is encompassed from the lower realms all the way up to the angel realms.
So yama loca is considered to mean the lower realms of the hell realms, the animal realms, the ghost realms.
The day of a loca, the day of a con means the angel realms means the human realms, the angels and the gods and so on but those three.
So he said, this is what you should be focusing on. Here you have something that is much more important and you should be concerning yourself with how to understand this earth.
And so he gave this verse. He said, who can overcome this earth? I made them think about this. Think about the dhamma in this way and said, who will do this? Who will be able to overcome this?
Certainly not the people who sit around talking about use the subject. Who will be able to overcome this earth? Whether it be the lower realms or the higher realms.
Who will be able to put into practice and bring to perfection this path and dhamma this path of truth.
Path to realize the truth just as a skillful person would bring to perfection or make a perfect bouquet of flowers.
I think this is useful is because it asks the question of who is it or how can we address the problem that we are faced with?
This suffering that we have ahead of us, the old aim, sickness and death. How do we address the issue of being mortal or how do we address the issue of being subject to suffering?
And how do we understand what is the path? What do we need to do in order to perfect the practice and perfect the path to become free from suffering?
And the Buddha makes a direct definite statement as to what it requires, what it will require for us to do that. And this is the training that a person who trains themselves is one who will be able to overcome the earth, will be able to overcome old age sickness and death.
A person who trains their mind, who trains themselves in morality, who trains themselves in concentration, who trains themselves in wisdom. This is a person who will be able to overcome old age sickness and death.
This is a person who will be able to put into practice the Buddha's teaching, will be able to bring to perfection the Buddha's teaching, will be able to bring to perfection this path of dhamma.
The distinction that we have to make is between the mundane world and the ultimate reality. This is where the Buddha points them in the right direction.
An ordinary person will tend to think that there is no way to overcome old age sickness and death. These are part of life. You think, well, that's part of the bargain. You get to enjoy life.
This life that you have and you have to accept the fact that it's going to end in old age sickness and death.
And so we think in conventional sense, just as these monks were thinking about the conventional earth, so it's actually quite an apt story because there's a parallel in how we look at our lives as well.
We look at life as being sort of a one shot. Even people who claim to believe in the afterlife and so on will tend to focus all of their efforts.
For most of their efforts on finding pleasure and avoiding suffering in this life, trying their best to be free from sickness, trying their best to obtain sensual gratification in this life.
Without making any thought of the bigger picture and the ultimate reality, what's really going on and what's really happening.
Realize that actually old age sickness and death is not just an inevitability of life. It's a constant reoccurrence. And it comes with a cause. Or it comes because of a cause. The cause is birth.
Because that's like one wave crashing against the shore. If you're not able to overcome the things that lead you to be born again, then you have to put up with this again and again.
So putting up with it isn't enough or accepting it isn't enough. We create the cause for it to occur again and again for eternity.
With our addiction, with our attachment, with our concern for the mundane. With all of our attachments, all of our obsessions, all of our worries and our cares on a conventional level.
We lose sight of the ultimate reality. We lose sight of what's really going on or what we're really doing. We lose sight of the cycle, the bigger picture that what we're creating here is actually a cycle. It's not a one shot. It's not linear. It's not like birth goes to death in the line and then that's it.
It's a cycle. So birth goes to death, which leads to birth again. Just like waves crashing against the shore one after another, after another forever.
So the Buddha points out that something needs to change about the way we look at the world. We can't let ourselves follow after our habits and are following our heart, for example.
Something has to change about how we look at the world, how we look at our lives. This is what happens when someone comes to practice the Buddha's teaching, when we listen to the dhamma, when we read the Buddha's teaching.
When we start to affect this change, we realize that there's much more going on than meets the eye. That the things that we cling to are not actually bringing us happiness. They're creating a habit of clinging.
That the things that we bear with are not actually, it's not actually out of patience. It's actually with a lot of suffering and cultivating an aversion towards things.
When we escape from pain by taking medication or avoiding it, we actually cultivate greater and greater aversion towards the pain.
We start to realize that this is not the way out of suffering. We realize this when we begin to train ourselves, starting with morality.
We start to realize that, realize these habits are, well, habit-forming. We become magnified the more we cultivate them.
We undertake morality. We undertake to prevent ourselves, to stop ourselves from following after the habits. We train ourselves to be patient when we would otherwise chase after something.
We want something instead of chasing after it. We stop ourselves. We bring ourselves back to the experience of it.
When you want something, you're very little involved with that actual thing, and you're much more involved with the thoughts and the pleasures that arise in the mind.
When you're able to bring yourself back, this is the first step, is bringing the mind back to reality. It's called morality.
The true morality is in the mind. When you bring your mind back to what's really happening, what's really going on, the essence of the experience.
When you see something and you want it, you look at the seeing and you look at the feeling and you look at the wanting.
When you see that it's really just impersonal phenomena arising and ceasing, you break it apart, and you see that it's because for stress, it's for suffering, not a cause for happiness and contentment.
When you become content without you become with or without you create what we call morality.
As a result of the morality, you begin to develop concentrations, so you train yourself further.
Once you've called a bit morality, you train yourself to keep the mind.
Once you've brought the mind back to the present, you train yourself to keep the mind focused on the present.
When you see something, you train yourself focused on seeing. When you hear something hearing, when you smell, you train yourself to focus on the body, on the feelings, on the thoughts, on the emotions and the senses.
You train yourself to focus on reality, so you cultivate concentration.
As a result, not only are you pulling your mind back, but your mind begins to be disinclined to actually leave the present reality.
All of us sitting here now are sitting listening to this dog.
When we have two things going on, we have the ultimate reality, which is where we think we are, where we'd like to be.
We'd like to be here in ultimate reality sounds technical or special.
It actually just means this, what we think we're in at all times.
So the reality of seeing, the reality of hearing, smelling, tasting, feeling, the reality of our experience.
But something else that's going on is all the activity in the mind, the judgments, the projections, the extrapolations, the identifications.
All of the concepts that arise of people and places and things, all of the past and the future, this is all conceptual.
That's nothing to do with the ultimate reality.
So we begin to, the training that we're looking for is to keep ourselves with the reality.
Keep ourselves here. Here we all are sitting together.
The training is to keep ourselves here and now.
Once we do that, this is the training and concentration.
The third training and the training and wisdom is that once we keep ourselves here, we begin to understand here.
We can understand reality.
We can just see the distinction between reality and illusion.
We see how there's nothing essentially good or bad about anything.
We see that all of the people and places and things, even our own body, it's just a concept that experiences arise and sees come and go.
And because of wisdom, we begin to need less and less effort to focus ourselves.
And less and less effort to bring our minds back, our minds are less inclined to move away.
And more inclined towards focusing more inclined towards peace and calm and clarity of mind.
Not just because we pull our minds back or try to keep our minds fixed, but because we understand the uselessness of chasing after illusion.
This is the training and wisdom. We train ourselves not only to bring our minds back, not only to force our minds to stay, but to have our minds understand the benefit of staying.
The benefit of being in the present moment takes ourselves how much stress is involved with running away and chasing after illusions, chasing after concepts.
So these are the three trainings, and this, the Buddha said, is how one overcomes the bad things in life and cultivates the good things.
So the first part is overcomes the world, we change the deep, overcomes the earth, overcomes all the troubles and all the difficulties and all the stresses and sufferings that are inherent in life.
And how one cultivates the dhamabanda, the path of truth and the path of righteousness, and brings it to perfection.
When you have all of the Buddha's teachings or all of the good things in the world, how do you bring them to perfection?
This is the concise summary of how to do that. One becomes a trainee, one trains oneself.
So one way of understanding Buddhism is it's a training. It's not something that you can wish for or hope for or something that you can be when you put on robes or so on.
It's something that you train in, just as we train our bodies and we train our minds in a world they sense to become proficient at this skill or that skill.
So too we train our minds, we train our hearts in the dhamma, in wholesomeness. And we train them out of unwholesomeness unskillful states, states that cause stress and suffering for us and others.
So this is the teaching in these two verses quite a sort of pithy sort of summary of how one brings the Buddha's teaching or brings all good states to perfection, just as one skillful person would skillful flower maker.
The florist would put together a bouquet of flowers. So all of the good things in our mind, flower and may become in a sense perfect through our abandoning of evil and wholesome states and through the cultivation of good states.
This is the teaching in this verse and that's the dhammapanda. So very much a practical teaching. This is obviously something that we have to use in our meditation. It's something that the highest training in Buddhism is here.
It comes from these three, it comes from the meditative practice of morality, the meditative practice of concentration. Keep bringing the mind back, keeping the mind there and the meditative concentration, meditative practice or training of wisdom of understanding.
The understanding that keeps you out of illusion and delusion, keeps the mind from wandering, keeps the mind in tune with reality. So that's the teaching for today on verses 44 and 45. Thank you for tuning in and I hope you're able to put this into practice and follow the path of all the dhammapanda on your own.
Thank you.
