WEBVTT

00:00.000 --> 00:06.000
Okay, so this latest question is from an anonymous poster.

00:06.000 --> 00:07.000
Hi, Yuta Damo.

00:07.000 --> 00:10.000
Well, I'm very interested in Buddhism and meditation.

00:10.000 --> 00:14.000
I feel very confused and do not know what tradition of Buddhism is the best for me.

00:14.000 --> 00:16.000
How should I choose?

00:16.000 --> 00:21.000
Well, the first thing I'd say is that probably across the board,

00:21.000 --> 00:26.000
nobody would ever recommend you to pick a tradition of Buddhism,

00:26.000 --> 00:30.000
simply because within each of the major traditions of Buddhism,

00:30.000 --> 00:35.000
there is such an incredible variety.

00:35.000 --> 00:40.000
Some monasteries will be strict, some monasteries will be lax,

00:40.000 --> 00:46.000
and so you're always better off to pick a center or to pick a lineage,

00:46.000 --> 00:49.000
lineage meaning a certain set of teachings.

00:49.000 --> 00:54.000
Like for instance, the teachings that I teach and practice are based on Mahasisayada,

00:54.000 --> 01:00.000
who was a teacher in Burma who passed away about 30 years ago.

01:00.000 --> 01:05.000
And so all of the centers in the Mahasisayada lineage will practice a very similar technique

01:05.000 --> 01:09.000
and are generally pretty strict and pretty straightforward.

01:09.000 --> 01:15.000
They teach a very similar technique to what you find on my YouTube channel.

01:15.000 --> 01:18.000
And they'll vary from monastery to monastery,

01:18.000 --> 01:23.000
but you'll find that you can adapt from one to the other quite easily.

01:23.000 --> 01:32.000
So this is a much more benefit finding a center which you can really relate to.

01:32.000 --> 01:36.000
I wouldn't recommend anything except that which I've practiced,

01:36.000 --> 01:40.000
so it's probably useless for me to recommend anything,

01:40.000 --> 01:44.000
but you should pick according to the meditation practice.

01:44.000 --> 01:48.000
You should look at the teacher and ask yourself,

01:48.000 --> 01:53.000
whether what they're saying is not only convincing,

01:53.000 --> 01:56.000
but it's something that is practical.

01:56.000 --> 02:00.000
It's very easy for people to give these high sounding teachings

02:00.000 --> 02:04.000
on all of the various stages of enlightenment and so on,

02:04.000 --> 02:09.000
or give teachings which are inspiring or so on.

02:09.000 --> 02:12.000
But you have to ask yourself, is this practical?

02:12.000 --> 02:20.000
Are you actually able to put these fundamentals or these teachings into practice?

02:20.000 --> 02:25.000
Is it something that I can go ahead, go back to my room and sit down and practice?

02:25.000 --> 02:31.000
So meditation should be a key, and you should ask yourself whether the meditation is leading to progress,

02:31.000 --> 02:33.000
whether you're actually gaining anything when you sit,

02:33.000 --> 02:37.000
are you just sitting and closing your eyes and getting blissful feelings,

02:37.000 --> 02:40.000
or are you actually coming to understand things about yourself?

02:40.000 --> 02:43.000
Are you learning more about yourself and the world around you?

02:43.000 --> 02:50.000
Are you coming to understand anything that you didn't see before?

02:50.000 --> 02:53.000
And I guess in that sense, it's not really important what center you choose.

02:53.000 --> 02:55.000
You can go and try them all out.

02:55.000 --> 03:00.000
I had a friend who, before I met him,

03:00.000 --> 03:04.000
he had gone around to all of the centers in the area

03:04.000 --> 03:07.000
where I was staying before I came to stay there.

03:07.000 --> 03:10.000
He had gone around to all of the centers in the area

03:10.000 --> 03:13.000
and tried them all out and found none of them suitable.

03:13.000 --> 03:17.000
Until finally he came to see me, and I hadn't even set up a center.

03:17.000 --> 03:19.000
I was just staying in a Cambodian monastery,

03:19.000 --> 03:22.000
and he started practicing.

03:22.000 --> 03:25.000
I just showed in the teaching that my teacher had given to me,

03:25.000 --> 03:29.000
and given to me, and right away he realized that's the teaching that he was looking for.

03:29.000 --> 03:32.000
So since then I understand he's been practicing that teaching.

03:32.000 --> 03:35.000
I haven't seen him in a while, but he was convinced that this was correct.

03:35.000 --> 03:40.000
So I think you can just try out whatever is near you.

03:40.000 --> 03:45.000
And the other good thing about that is it often gives you a link with that tradition.

03:45.000 --> 03:47.000
As opposed to, you know, say, where should I go?

03:47.000 --> 03:52.000
Should I fly to Tibet or fly to Thailand or fly to Burma or so on?

03:52.000 --> 03:56.000
You can go to your local centers if there are any nearby,

03:56.000 --> 03:59.000
and talk to the people there, and if you start to practice with them

03:59.000 --> 04:03.000
and they see that you're sincere, they'll often encourage you to go to the center

04:03.000 --> 04:13.000
of that center, and they'll be able to recommend the more strict and traditional and real centers.

04:13.000 --> 04:17.000
The centers that will give you a real understanding of the Buddha's teaching.

04:17.000 --> 04:23.000
So check out what's local, and just use your own judgment,

04:23.000 --> 04:26.000
try to watch the teacher, and not too closely.

04:26.000 --> 04:28.000
I mean, listen to the teachings.

04:28.000 --> 04:31.000
Don't worry too much about the people because everybody's human,

04:31.000 --> 04:35.000
but look and see, you know, as the teaching giving you something that's useful.

04:35.000 --> 04:39.000
You're always welcome to come on out to California if you're anywhere nearby,

04:39.000 --> 04:44.000
and I could give you some pointers if you come and practice here.

04:44.000 --> 04:46.000
I could send you off to Thailand or wherever.

04:46.000 --> 04:51.000
I know a few places that you might be able to get your foot in the door.

04:51.000 --> 04:54.000
Okay, so I hope that helps, and wish you all the best.

04:54.000 --> 05:01.000
Good luck in your search.

