1
00:00:00,000 --> 00:00:07,960
Hello, this is an attempt to re-answer a question I was recently asked on

2
00:00:07,960 --> 00:00:16,040
Mongo radio about the nature of right and wrong. Specifically, in the case

3
00:00:16,040 --> 00:00:23,280
where it is true or if it were true that there were no meaning or purpose to

4
00:00:23,280 --> 00:00:30,960
anything, how could you have a sense of right or wrong? And so I think I'm going

5
00:00:30,960 --> 00:00:36,920
to have to clarify first of all my stance and what I understand to be the

6
00:00:36,920 --> 00:00:43,120
Buddhist stance on purpose and meaning and value because the Buddha did give

7
00:00:43,120 --> 00:00:46,800
some fairly specific teachings on what has purpose, what has meaning, what has

8
00:00:46,800 --> 00:00:53,720
value but it's the kind of teaching that you have to understand in context and

9
00:00:53,720 --> 00:01:00,120
so you have to understand some of the things that I've said about purpose and

10
00:01:00,120 --> 00:01:06,320
value in context and that was what I was trying to get across in my answers that

11
00:01:06,320 --> 00:01:16,160
the two criticisms that people will level against this idea of lack of purpose is

12
00:01:16,160 --> 00:01:23,120
first of all that having lack of purpose will most likely lead you to

13
00:01:23,120 --> 00:01:30,080
depression, apathy, hopelessness, it has a negative effect on one psyche

14
00:01:30,080 --> 00:01:39,240
whereas they would argue having a purpose gives you motivation and

15
00:01:39,240 --> 00:01:46,800
direction and so on which is all well and good and certainly not to be denied the

16
00:01:46,800 --> 00:01:52,280
other accusation that is leveled charged at its level is that a person who has

17
00:01:52,280 --> 00:01:56,280
no meaning, purpose, seasonal value in anything isn't able to differentiate

18
00:01:56,280 --> 00:02:06,840
between that which is valuable and that which is its value list worthless is that

19
00:02:06,840 --> 00:02:09,720
they won't have a sense of right or wrong and that's what this question was

20
00:02:09,720 --> 00:02:14,400
asking how can there be right or wrong if you don't have purpose or value if

21
00:02:14,400 --> 00:02:19,680
you are unable to make value judgments and so it's important to understand

22
00:02:19,680 --> 00:02:28,840
what the meaning behind this idea is. First of all these two accusations are

23
00:02:28,840 --> 00:02:38,720
unfounded but they're unfounded for a person who truly has you could say

24
00:02:38,720 --> 00:02:47,640
transcended meaning and value and so the problem is that we're projecting our

25
00:02:47,640 --> 00:02:57,120
own muddled unenlightened state when we answer this question so we say what

26
00:02:57,120 --> 00:03:03,160
would it be like if I had no purpose meet if I were to accept that philosophy as

27
00:03:03,160 --> 00:03:07,360
though it were just a philosophy that you could anyone could accept and that's

28
00:03:07,360 --> 00:03:13,160
not the case. The point is that as you understand reality you start to change

29
00:03:13,160 --> 00:03:19,440
you stop finding meaning and purpose and value in the things that you use to

30
00:03:19,440 --> 00:03:31,320
find meaning purpose and value in. As a result of those changes you have you lose

31
00:03:31,320 --> 00:03:40,800
your ability to perform both right and wrong acts and in a sense certainly

32
00:03:40,800 --> 00:03:47,920
the ability to perform evil deeds disappears but at the same time a person has

33
00:03:47,920 --> 00:03:52,440
no sense of good either in the sense that even when they do something to

34
00:03:52,440 --> 00:04:00,800
help others they're simply doing it as a functional act of response as kind

35
00:04:00,800 --> 00:04:05,080
of the path of least resistance because to deny someone is to create

36
00:04:05,080 --> 00:04:12,680
complication it requires a certain amount of evil to do so a person simply does

37
00:04:12,680 --> 00:04:18,360
good because the alternative would require something that is lacking in which

38
00:04:18,360 --> 00:04:26,400
is evil it would require friction and that sort of friction and

39
00:04:26,400 --> 00:04:31,800
defilement in the mind all the stress in the mind and disappears from the

40
00:04:31,800 --> 00:04:37,440
person who has become enlightened so the the the important thing to realize is

41
00:04:37,440 --> 00:04:42,640
that when you say that purposeless need and all of this leads to apathy or

42
00:04:42,640 --> 00:04:48,320
that it leads to immorality because a person who has no purpose can then

43
00:04:48,320 --> 00:04:54,480
just do whatever they want you are confusing to very different states the

44
00:04:54,480 --> 00:05:00,000
state of someone who's actually realized true you could call true

45
00:05:00,000 --> 00:05:04,240
purposelessness in the sense that they don't put value in anything they simply

46
00:05:04,240 --> 00:05:11,360
live their lives and a person who still has attachment to things as this is what

47
00:05:11,360 --> 00:05:17,000
I'm aiming for this is what I'm I'm hoping for this is what I'm wishing for

48
00:05:19,840 --> 00:05:26,720
because a person who has no who has given up all of that has no potential for

49
00:05:26,720 --> 00:05:34,120
evil to arise in their mind and they have no potential for apathy to arise

50
00:05:34,120 --> 00:05:38,960
in their mind they have no potential for any disappointment and the thing is

51
00:05:38,960 --> 00:05:44,000
it's funny to even suggest that purposelessness would lead to depression or

52
00:05:44,000 --> 00:05:49,560
or any negative emotion whatsoever when the negative emotions are based on

53
00:05:49,560 --> 00:05:54,240
disappointment they're based on the on a desire for value and desire for

54
00:05:54,240 --> 00:06:00,160
purpose the only reason a person people feels depressed when they have when

55
00:06:00,160 --> 00:06:04,880
they have no purpose is because they want one because they still crave for

56
00:06:04,880 --> 00:06:09,440
purpose a person who's gone beyond that has doesn't isn't able to feel depressed

57
00:06:09,440 --> 00:06:14,960
it's impossible for that to come to them because they have no expectations

58
00:06:14,960 --> 00:06:20,160
they have no desire for anything that's what purposelessness does purpose

59
00:06:20,160 --> 00:06:25,120
lessness does to you and as a result they they they could never do anything

60
00:06:25,120 --> 00:06:30,400
that would cause harm that would cause stress or suffering there is no

61
00:06:30,400 --> 00:06:34,160
attachment that would lead to that so that was all I was trying to say it's quite a

62
00:06:34,160 --> 00:06:38,080
simple answer and I think I muddled it up a little bit so here I am again

63
00:06:38,080 --> 00:07:07,120
thanks for tuning in everyone see you next

