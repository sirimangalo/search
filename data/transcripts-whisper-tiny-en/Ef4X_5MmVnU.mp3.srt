1
00:00:00,000 --> 00:00:06,400
Okay, good evening, welcome to our evening dhamma.

2
00:00:06,400 --> 00:00:13,120
Tonight we're looking at the fourth noble truth.

3
00:00:13,120 --> 00:00:23,000
And I think this gives us a good point to segue into a second list which is of course

4
00:00:23,000 --> 00:00:29,600
the eight full noble paths and the fourth noble truth has eight parts and because it's

5
00:00:29,600 --> 00:00:38,920
another list we could do one path factor every night.

6
00:00:38,920 --> 00:00:42,200
Start doing series and get into our second series.

7
00:00:42,200 --> 00:00:48,800
The thing about the Buddha's teaching is it all connects, I mean none of these things

8
00:00:48,800 --> 00:00:58,920
are really, really real and so it's not like we're putting, I mean many of the, many

9
00:00:58,920 --> 00:01:01,040
of them are not related.

10
00:01:01,040 --> 00:01:05,280
Many of these constructs are not real, not all of them, some are real.

11
00:01:05,280 --> 00:01:13,800
But the construct of the eight full noble path is one construct and so it becomes quite

12
00:01:13,800 --> 00:01:15,680
complicated.

13
00:01:15,680 --> 00:01:17,760
Everything sort of melds into each other.

14
00:01:17,760 --> 00:01:23,800
So the fourth noble truth is actually the eight full noble path.

15
00:01:23,800 --> 00:01:29,000
But the first noble truth is knowledge of the, or the first factor of the eight full noble

16
00:01:29,000 --> 00:01:34,000
path is knowledge of the four noble truths.

17
00:01:34,000 --> 00:01:42,720
So it, everything connects together because they're concepts and so it might seem quite

18
00:01:42,720 --> 00:01:47,420
complicated at times it's important to understand that it's not, it's not really

19
00:01:47,420 --> 00:01:54,280
describing ultimate reality in many cases, these are constructs that the Buddha put together.

20
00:01:54,280 --> 00:02:01,040
So when we talk about the eight full noble path, we're talking about a means of organizing

21
00:02:01,040 --> 00:02:11,080
reality and organizing experience and more importantly directing it into a way we're describing

22
00:02:11,080 --> 00:02:24,240
reality and the essential aspects of those experiences that incline towards peace, happiness

23
00:02:24,240 --> 00:02:26,240
and freedom from suffering.

24
00:02:26,240 --> 00:02:29,320
That we call the way.

25
00:02:29,320 --> 00:02:35,360
So tonight I'll just talk about the way in general and then I'll give a short description

26
00:02:35,360 --> 00:02:46,680
of each of the eight path factors one per night.

27
00:02:46,680 --> 00:02:57,520
So we have the four noble truths are laid out in terms of effect, cause, effect, cause.

28
00:02:57,520 --> 00:03:05,560
So suffering is the effect, craving is the cause, cessation of suffering is the effect,

29
00:03:05,560 --> 00:03:13,160
the path to the cessation of suffering is the cause.

30
00:03:13,160 --> 00:03:19,480
Roughly speaking, I mean, the path is that which leads to cessation of suffering, it's

31
00:03:19,480 --> 00:03:29,000
not actually the cause because cessation can be caused but it's what leads to the cessation

32
00:03:29,000 --> 00:03:30,000
experience.

33
00:03:30,000 --> 00:03:43,520
The cause is the experience of cessation, you might say.

34
00:03:43,520 --> 00:03:53,360
The first thing to know about the way on the path is to understand that how it, what sort

35
00:03:53,360 --> 00:03:57,640
of a path it is, how it functions as a path.

36
00:03:57,640 --> 00:04:02,440
When you think of a path, of course, a path is something that leads to a place, not where

37
00:04:02,440 --> 00:04:07,120
you are.

38
00:04:07,120 --> 00:04:17,920
And so we kind of think of it as going somewhere, at least in a symbolic way.

39
00:04:17,920 --> 00:04:22,120
But it's a m isn't a path in the sense of like the path to heaven, right?

40
00:04:22,120 --> 00:04:26,920
When we think of religious paths, we're usually thinking of this, how to go somewhere else.

41
00:04:26,920 --> 00:04:28,520
How to get out of this place?

42
00:04:28,520 --> 00:04:33,920
So we're talking about getting out of some sara and it becomes this sort of idea of here's

43
00:04:33,920 --> 00:04:43,520
some sara nibhana is outside of it, like in the same way that heaven is outside of earth.

44
00:04:43,520 --> 00:04:47,480
But it's not really like that.

45
00:04:47,480 --> 00:04:54,000
The way is much more like a way of life, it's a way of living your life rather than

46
00:04:54,000 --> 00:05:01,880
going somewhere, not like a way to New York City, it's a way to live.

47
00:05:01,880 --> 00:05:05,320
And so when we think about the way, it's important to remember, what is it a way towards?

48
00:05:05,320 --> 00:05:08,240
It's a way to freedom from suffering.

49
00:05:08,240 --> 00:05:11,760
We're talking about the suffering that we have.

50
00:05:11,760 --> 00:05:18,200
And when we talk about the way, what we mean is the means to become free from that.

51
00:05:18,200 --> 00:05:27,960
When you practice according to this way, you're suffering ceases, it becomes reduced and

52
00:05:27,960 --> 00:05:33,280
knowledge and understanding about suffering comes about, and eventually there's a cessation

53
00:05:33,280 --> 00:05:36,280
of suffering.

54
00:05:36,280 --> 00:05:37,280
That's what it leads to.

55
00:05:37,280 --> 00:05:41,440
But it doesn't go anywhere else, it doesn't happen somewhere else, it doesn't lead you

56
00:05:41,440 --> 00:05:43,800
somewhere else.

57
00:05:43,800 --> 00:05:48,520
The cessation happens right here and it's the mind not going anywhere, it's when the

58
00:05:48,520 --> 00:05:56,400
mind stops going.

59
00:05:56,400 --> 00:06:03,360
So we're going to talk about the 8 full noble path as the essence, he said, in whatever

60
00:06:03,360 --> 00:06:08,840
religion, it's one of the last things he said apparently, in whatever teaching, whatever

61
00:06:08,840 --> 00:06:18,120
dispensation that you find the 8 full noble path, in that religion you will find Sotapana,

62
00:06:18,120 --> 00:06:26,080
Sakantagami, Anagami and Aran, you will find enlightened being, whatever dispensation does

63
00:06:26,080 --> 00:06:32,160
not have the 8 full noble path, you will not find enlightened beings.

64
00:06:32,160 --> 00:06:38,440
This is important, it answers the question of whether all religions are the same, but it

65
00:06:38,440 --> 00:06:47,080
was clearly of the opinion that no, no, they're quite different.

66
00:06:47,080 --> 00:06:58,360
We think of religions generally as being of a certain nature, so if you think religion

67
00:06:58,360 --> 00:07:03,040
is beneficial, you think that all religions are beneficial, and we tend to lump them together

68
00:07:03,040 --> 00:07:09,880
and say, all religions teach people to be good, it's the corrupt, the corrupt, or the

69
00:07:09,880 --> 00:07:15,800
corruptions of religion that are the problem.

70
00:07:15,800 --> 00:07:19,520
And then on the other hand, if you see that religion is generally corrupt and you would

71
00:07:19,520 --> 00:07:27,440
tend to lump them all together and say, all religion is bad, religion is a poison, religion

72
00:07:27,440 --> 00:07:32,720
ruins everything, that kind of thing.

73
00:07:32,720 --> 00:07:39,760
But both of these are very simplistic ways of looking, and it has a lot more to do with

74
00:07:39,760 --> 00:07:55,720
our desire to simplify, you might say laziness, it's not the word I'm looking for, but

75
00:07:55,720 --> 00:08:03,840
we just don't have the energy to nuance our understanding of religion, it's much simpler

76
00:08:03,840 --> 00:08:09,480
and harmonious for us to just say, hey, you are of a different religion, well that's okay

77
00:08:09,480 --> 00:08:16,040
because we know that all religions are good and basically the same.

78
00:08:16,040 --> 00:08:22,600
And that's fine, I mean I think that's actually positive in a worldly sense, we accept

79
00:08:22,600 --> 00:08:31,320
that people's beliefs are their own, and in a general sense it's what allows them to

80
00:08:31,320 --> 00:08:45,320
be social and connected to a sense of purpose and a sense of normality.

81
00:08:45,320 --> 00:08:51,400
So in a general sort of societal sense, that's fine, we get along with all religions,

82
00:08:51,400 --> 00:08:56,280
I think that's a good thing, it's very useful anyway for people living in the world.

83
00:08:56,280 --> 00:09:03,040
But when we talk about truth, we have to call a spade a spade, and all religions are very

84
00:09:03,040 --> 00:09:09,920
different, some are more similar than others, I mean most religions deal with God, and so

85
00:09:09,920 --> 00:09:16,560
they tend to be fairly similar, God being somehow omnipotent, but even that, you know even

86
00:09:16,560 --> 00:09:23,200
take Christianity or Islam, and many different Christians think many different things about

87
00:09:23,200 --> 00:09:31,000
God.

88
00:09:31,000 --> 00:09:37,160
Buddhism is a specifically a religion of the eightfold noble path, that's what it is,

89
00:09:37,160 --> 00:09:45,520
nothing more, anything else is perhaps useful and helpful, but ultimately ancillary

90
00:09:45,520 --> 00:09:53,480
auxiliary or even superfluous, if it falls outside of the eightfold noble path, then it's

91
00:09:53,480 --> 00:10:00,200
one of two things, it's either contrary to Buddhism, or it's superfluous and unnecessary,

92
00:10:00,200 --> 00:10:05,360
unessential to Buddhism, right?

93
00:10:05,360 --> 00:10:11,080
So there are things that are outside of the noble path, like charity, it's a charity

94
00:10:11,080 --> 00:10:16,560
that doesn't actually fall in the eightfold noble path, but charity is quite useful,

95
00:10:16,560 --> 00:10:22,200
if you're a charitable kind person, certainly helps, cultivate the eightfold noble path,

96
00:10:22,200 --> 00:10:31,560
so it's a good thing, there might say study of Buddhism, it doesn't fall in the eightfold

97
00:10:31,560 --> 00:10:34,200
noble path, et cetera.

98
00:10:34,200 --> 00:10:44,200
There are many things, many aspects of religious activity, even monasticism, you might say.

99
00:10:44,200 --> 00:10:57,400
Anyway, when we talk about the eightfold noble path, there are two paths, so it's important

100
00:10:57,400 --> 00:11:02,880
to understand what we mean by the eightfold noble path, and Buddhism, there are two paths,

101
00:11:02,880 --> 00:11:09,960
and this is sort of the orthodox understanding, there's just called the pubangamanga and

102
00:11:09,960 --> 00:11:14,640
the ariamanga.

103
00:11:14,640 --> 00:11:22,760
pubangamanga is mundane, all of us are practicing the pubangamanga, every time we meditate,

104
00:11:22,760 --> 00:11:34,000
we're on the pubanga, pubanga means belonging to, and puba means prior or preliminary.

105
00:11:34,000 --> 00:11:38,720
So the pubanga is that which comes before, that which leads up to the eightfold noble

106
00:11:38,720 --> 00:11:40,200
path.

107
00:11:40,200 --> 00:11:48,400
You can think of it like the preparation or the training, right, where what we're working

108
00:11:48,400 --> 00:11:55,000
up to is like when you rub two sticks together for fire, the moment that it sparks, then

109
00:11:55,000 --> 00:12:01,840
you're on the path, up until that point you're on the preliminary path.

110
00:12:01,840 --> 00:12:07,520
So when we talk about the noble eightfold path, we're actually just talking about this moment

111
00:12:07,520 --> 00:12:15,720
when you are on the path, when you're following the way, and because that moment is the

112
00:12:15,720 --> 00:12:23,640
moment before nibhana, it's truly the path, because it's truly what causes nibhana.

113
00:12:23,640 --> 00:12:28,040
Up until that point, we don't know if we're going to get to nibhana.

114
00:12:28,040 --> 00:12:32,400
You practice for days and days on end, still not sure.

115
00:12:32,400 --> 00:12:36,320
You still can't say, well, this path is going to lead me to nibhana.

116
00:12:36,320 --> 00:12:41,200
Well, maybe not, maybe you stop early, maybe something gets in your way.

117
00:12:41,200 --> 00:12:44,680
So it's preliminary, it's not yet noble.

118
00:12:44,680 --> 00:12:54,600
You can't yet say, this is what causes one to realize nibhana, because it doesn't yet.

119
00:12:54,600 --> 00:12:57,600
It's leaning towards it, and so it's important.

120
00:12:57,600 --> 00:13:04,040
It's necessary, but it's called the pubangamanga, and that still involves the eightfold

121
00:13:04,040 --> 00:13:06,760
noble, the eight path factors.

122
00:13:06,760 --> 00:13:13,680
So these eight path factors are preliminary, they have a preliminary aspect and they have

123
00:13:13,680 --> 00:13:20,920
a noble aspect of that's not too confusing, but it means when we talk about cultivating

124
00:13:20,920 --> 00:13:29,080
right view and right thought and so on and so on, we usually mean in a mundane sense.

125
00:13:29,080 --> 00:13:32,880
And so when we normally talk about the eightfold path, we're normally not talking about

126
00:13:32,880 --> 00:13:38,240
the noble eightfold path, we're talking about the preliminary path.

127
00:13:38,240 --> 00:13:49,400
So the path has eight factors, there's different ideas about, even the Buddha proposes

128
00:13:49,400 --> 00:13:54,360
different ideas about how these eight factors interact, I'm not going to go into detail,

129
00:13:54,360 --> 00:13:59,880
I don't want these talks to be, there's just simple stuff, if you want more detail,

130
00:13:59,880 --> 00:14:05,960
there's lots of books written about this sort of thing, but there's the idea that it goes

131
00:14:05,960 --> 00:14:11,240
in order, so you can say, hey, you need right view first and it leads to right thought

132
00:14:11,240 --> 00:14:14,400
and it leads to and so on and so on.

133
00:14:14,400 --> 00:14:18,440
And they even had, the Buddha even adds two on the end, he says, well, right concentration

134
00:14:18,440 --> 00:14:26,440
will lead to right knowledge, which leads to right, we move to which is right liberation.

135
00:14:26,440 --> 00:14:30,280
And then there's the idea that it goes in a circle, so when you get right view all the

136
00:14:30,280 --> 00:14:35,760
way down to right concentration, we'll write concentration when you're focused, cultivates

137
00:14:35,760 --> 00:14:36,760
right view.

138
00:14:36,760 --> 00:14:41,040
And so you see, well, it's actually a circle and then there are small circles, like these

139
00:14:41,040 --> 00:14:46,960
three go together and they work, these two support, these, this one and so on.

140
00:14:46,960 --> 00:14:53,000
The path factors working together and you can make it quite complicated.

141
00:14:53,000 --> 00:14:58,120
So I think understanding these two different paths is important in the preliminary stage,

142
00:14:58,120 --> 00:15:03,400
you are working on the factors individually, some factors you focus on more at different

143
00:15:03,400 --> 00:15:06,200
times.

144
00:15:06,200 --> 00:15:11,280
Mostly we try to focus on mindfulness, it's really the most important, but you know, right

145
00:15:11,280 --> 00:15:16,520
view, sometimes studying will help clarify your view, asking questions will help to purify

146
00:15:16,520 --> 00:15:21,720
your view, practicing loving kindness will help to purify your thought and so on.

147
00:15:21,720 --> 00:15:26,040
Right livelihood is something, when we talk about right livelihood, we're normally talking

148
00:15:26,040 --> 00:15:32,040
about the preliminary path, okay, so I'm in wrong livelihood, I have to change my livelihood.

149
00:15:32,040 --> 00:15:43,960
But when it comes to the final on the path, all that it means is that you're in the

150
00:15:43,960 --> 00:15:51,280
perfect state of mind, you're being your essence is perfect, I mean, a person can go from

151
00:15:51,280 --> 00:15:57,200
being a murderer, a liar, a cheat theoretically, quite quickly to practicing the full

152
00:15:57,200 --> 00:16:03,520
noble path, it's not likely, but it's certainly possible.

153
00:16:03,520 --> 00:16:10,760
Because it's actually just, you know, the path is actually, it's just a mind state.

154
00:16:10,760 --> 00:16:16,960
And so when we talk about these eight factors, normally we're talking about working on them

155
00:16:16,960 --> 00:16:21,480
in a mundane sense, but when we talk about the eight full noble path, what we mean is

156
00:16:21,480 --> 00:16:27,520
when every aspect of our being is perfect and usually that comes from years and years

157
00:16:27,520 --> 00:16:37,240
or at least days or weeks of working on them individually and putting them all together.

158
00:16:37,240 --> 00:16:44,240
But it doesn't have to, it just means that in that moment we have no inclination towards

159
00:16:44,240 --> 00:16:55,040
wrong view and so we have nothing in us that is wrong in any way.

160
00:16:55,040 --> 00:17:00,960
So I'll go through, I'll go through all eight of these, one by one, see how much I don't

161
00:17:00,960 --> 00:17:04,440
know if I'll be able to talk a lot about them, but hopefully I'll have enough to give

162
00:17:04,440 --> 00:17:07,480
a talk for each of them.

163
00:17:07,480 --> 00:17:15,720
The last thing I'll say tonight about the eight full noble path is that it's basically

164
00:17:15,720 --> 00:17:21,160
organized into three parts, and many of you are probably aware of this, but if you want

165
00:17:21,160 --> 00:17:26,840
to simply understand the eight full noble path, we understand it in terms of morality,

166
00:17:26,840 --> 00:17:31,080
concentration, and wisdom.

167
00:17:31,080 --> 00:17:34,200
And I think this is probably the best way of organizing it.

168
00:17:34,200 --> 00:17:38,840
You put morality first, I mean this is the orthodox way of doing it.

169
00:17:38,840 --> 00:17:43,280
So the eight full noble path has right view at the beginning, and that's because it kind

170
00:17:43,280 --> 00:17:44,280
of works in a circle.

171
00:17:44,280 --> 00:17:50,480
Well, you need right view to really have right morality, but to start off on the paths,

172
00:17:50,480 --> 00:17:55,760
the best place to start is morality, you know?

173
00:17:55,760 --> 00:18:01,240
Because what's going to allow you to see things clearly is moral and ethical behavior

174
00:18:01,240 --> 00:18:08,240
when you stop doing, you know, chasing after all the things that distract you and then

175
00:18:08,240 --> 00:18:14,640
to slowly the mind.

176
00:18:14,640 --> 00:18:18,000
And so when we study that we see the manga, this is where he starts, he starts with

177
00:18:18,000 --> 00:18:19,000
morality.

178
00:18:19,000 --> 00:18:27,800
Because the Buddha's words, cile, patitaya, established on morality, jitang panyan

179
00:18:27,800 --> 00:18:35,160
tambhavayang, and then develops concentration in wisdom based on the morality.

180
00:18:35,160 --> 00:18:39,560
So the first two factors are wisdom, right?

181
00:18:39,560 --> 00:18:42,520
Right view and right thought, those are the wisdom ones.

182
00:18:42,520 --> 00:18:48,840
But they actually come at the end, really, in an ultimate sense, they're the goal.

183
00:18:48,840 --> 00:18:54,760
And they come when you're focused, when your mind is focused clearly.

184
00:18:54,760 --> 00:19:02,560
And so the focus aspects are right effort, right mindfulness, and right concentration.

185
00:19:02,560 --> 00:19:05,160
And those, of course, you can only be focused when you have morality.

186
00:19:05,160 --> 00:19:12,440
So those come from right morality, which is right speech, right action, and right livelihood.

187
00:19:12,440 --> 00:19:13,440
Right?

188
00:19:13,440 --> 00:19:19,240
So it starts with right speech, right action, and like, right livelihood, you put those

189
00:19:19,240 --> 00:19:25,400
together, and you'll start to become focused, and as you're focused, right effort, right

190
00:19:25,400 --> 00:19:31,720
mindfulness, right concentration, right focus, these arise, and then you start to see clearly.

191
00:19:31,720 --> 00:19:43,560
And once you start to see clearly, it will be right thought and ultimately right view.

192
00:19:43,560 --> 00:19:44,560
This is the path.

193
00:19:44,560 --> 00:19:45,560
This is Buddhism, really.

194
00:19:45,560 --> 00:19:51,000
It's the core, the heart of when we talk about, what do we do as Buddhists?

195
00:19:51,000 --> 00:19:57,440
It's really not much, you know, it's sometimes easy to get lost, you read too much, read

196
00:19:57,440 --> 00:19:59,840
so much that you can't really describe Buddhism.

197
00:19:59,840 --> 00:20:04,520
It's quite simple, we all know this, there's the eight full number path, don't lose sight

198
00:20:04,520 --> 00:20:08,560
of that, don't lose sight of the four number truths, don't lose sight of the eight full

199
00:20:08,560 --> 00:20:15,320
number path, the eight full number path is what we do as Buddhists, that's it, eight things.

200
00:20:15,320 --> 00:20:22,000
So if I had, there's only three things, right, my right morality, right effort, right

201
00:20:22,000 --> 00:20:28,560
morality, right focus, and right wisdom.

202
00:20:28,560 --> 00:20:34,240
So that's the path, and the next day is, the next eight days, I guess, I'll try to say

203
00:20:34,240 --> 00:20:41,800
something about each factor, I think that'll be good, so that's all for tonight, thank

204
00:20:41,800 --> 00:21:00,920
you all for tuning in, I guess if we have questions I'll take a look, two questions tonight.

205
00:21:00,920 --> 00:21:04,680
Shaking uncontrollably, kind of like when you're about to fall asleep, but I'm wide awake

206
00:21:04,680 --> 00:21:12,640
while in meditation, it's enjoyable feeling, but I just try to observe it, this is called

207
00:21:12,640 --> 00:21:23,120
BT Rapture, it's actually, it's not universal, but it's fairly common, it comes in different

208
00:21:23,120 --> 00:21:28,400
forms, some people rock back and forth, some people will shake, that kind of thing, some

209
00:21:28,400 --> 00:21:33,440
people feel this uplifting feeling, some people feel really light, there's many different

210
00:21:33,440 --> 00:21:40,720
states, you can tell it to stop, just say to yourself stop and shake yourself out of

211
00:21:40,720 --> 00:21:45,800
it, that sometimes helps, but otherwise, yeah, just note it and if you like it, say

212
00:21:45,800 --> 00:21:51,960
liking, if you feel happy, say happy, eventually it'll go away, it's not the path, it's

213
00:21:51,960 --> 00:21:57,480
not the way to freedom from suffering, just it's just another experience that comes and

214
00:21:57,480 --> 00:22:07,120
goes, it's impermanent, unsatisfying, uncontrollable, and we meditate, we become very good

215
00:22:07,120 --> 00:22:12,440
at controlling ourselves and not over reacting to emotional situations, but how do we know

216
00:22:12,440 --> 00:22:19,960
that we are not repressing emotions, yeah, so we don't try to control ourselves, we try

217
00:22:19,960 --> 00:22:25,960
to learn about ourselves, how we know is because we become more familiar with our emotions,

218
00:22:25,960 --> 00:22:31,640
and if anger were to arise or greed were to arise, we would understand it, we would

219
00:22:31,640 --> 00:22:36,840
not chase after it, we would see it and say, oh look, here's me getting angry, until

220
00:22:36,840 --> 00:22:42,680
eventually we just don't get angry because the idea of getting angry is just silly, we've

221
00:22:42,680 --> 00:22:50,080
seen that anger is useless, we've seen it, it doesn't lead to any benefit, we've stopped

222
00:22:50,080 --> 00:22:57,920
hankering after those things that we get angry about or greedy about, so if you're

223
00:22:57,920 --> 00:23:05,000
meditating to control yourself, that's quite possibly just suppressing, and you have

224
00:23:05,000 --> 00:23:09,800
to maybe, I mean it's fine in the beginning, but it's not sustainable in the long term,

225
00:23:09,800 --> 00:23:15,680
eventually you have to open up, let the emotions come up and learn about them and let

226
00:23:15,680 --> 00:23:23,520
them go with them, oh I forgot about the task, yes, the task associated with the fourth

227
00:23:23,520 --> 00:23:33,760
noble truth, so the task associated with the fourth noble truth is Bobena, means development,

228
00:23:33,760 --> 00:23:40,760
fourth noble truth is to be developed, bawaita bha, and once you have developed it and

229
00:23:40,760 --> 00:23:51,920
you know it has been developed, bawaitan, thanks Fernando, forgot, so the tasks in order

230
00:23:51,920 --> 00:23:58,160
then are, the first noble truth is to be fully understood, the second noble truth is

231
00:23:58,160 --> 00:24:04,000
to be abandoned, the third noble truth is to be experienced for oneself, and the fourth

232
00:24:04,000 --> 00:24:12,280
noble truth is to be cultivated, to be developed, the third noble path is to be developed,

233
00:24:12,280 --> 00:24:42,120
okay, so that's all for tonight, thank you for tuning in, have a good night.

