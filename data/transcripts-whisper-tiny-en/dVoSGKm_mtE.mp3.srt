1
00:00:00,000 --> 00:00:09,080
Okay, good evening, everyone.

2
00:00:09,080 --> 00:00:21,360
Welcome to our evening demo session.

3
00:00:21,360 --> 00:00:28,680
Last night we talked about having one good night, and I thought we'd talk about

4
00:00:28,680 --> 00:00:42,240
one exceptionally good night, which is the night the Buddha became enlightened.

5
00:00:42,240 --> 00:00:50,840
Both for simply how inspiring it is to think of the Buddha and to think of how we're

6
00:00:50,840 --> 00:00:58,000
following in his footsteps.

7
00:00:58,000 --> 00:01:09,240
And because of how instructive it is, what the Buddha's experience teaches us.

8
00:01:09,240 --> 00:01:17,480
So a little bit of background for those who don't know.

9
00:01:17,480 --> 00:01:24,480
And the Buddha was born in India, or what was, at the time, the Indus Valley.

10
00:01:24,480 --> 00:01:37,720
It's now modern Nepal, actually, about 2,500 years ago.

11
00:01:37,720 --> 00:01:44,880
So the world was quite different back then, and he wandered around on foot in throughout

12
00:01:44,880 --> 00:01:51,280
the Indus Valley.

13
00:01:51,280 --> 00:01:58,080
He left home at 29 years of age after living a life of luxury, and then wandered around

14
00:01:58,080 --> 00:02:07,840
India until he finally decided to just spend his time pushing his body to its limit, purposefully

15
00:02:07,840 --> 00:02:14,280
torturing himself in order to burn away defilements didn't work.

16
00:02:14,280 --> 00:02:25,200
And then after six years of that, he gave up torturing himself and he went back to eating

17
00:02:25,200 --> 00:02:34,800
solid food and taking better care of himself and practicing meditation, living a balanced

18
00:02:34,800 --> 00:02:35,800
life.

19
00:02:35,800 --> 00:02:43,040
This is why he came to talk about the middle way, because the ascetics who were practicing

20
00:02:43,040 --> 00:02:50,480
with him abandoned him, they said, oh, you've given up the holy life of torturing yourself

21
00:02:50,480 --> 00:02:57,760
so they gave him up.

22
00:02:57,760 --> 00:03:00,720
I don't think the Buddha was disturbed by that, because at this point, he was pretty

23
00:03:00,720 --> 00:03:07,280
sat on his own meditation, and imagine them leaving help in the end, because it gave him

24
00:03:07,280 --> 00:03:19,040
focus and allowed him to focus on his own practice.

25
00:03:19,040 --> 00:03:29,120
And he went and sat under the Bodhi tree, the tree that was to become, it's even there

26
00:03:29,120 --> 00:03:36,360
today, or a descendant of the original, they say, is still there to this day.

27
00:03:36,360 --> 00:03:47,520
And the forest in Bodhgaya, and he sat under this tree and spent the night there.

28
00:03:47,520 --> 00:03:52,080
So this is his one good night, didn't sleep.

29
00:03:52,080 --> 00:03:58,920
He said to himself, he wasn't going to get up from that seat until he became enlightened.

30
00:03:58,920 --> 00:04:12,280
And there are many stories about his time under the tree, of course, the most flowery

31
00:04:12,280 --> 00:04:20,200
one that many people have heard about, or read about, or watched in various depictions,

32
00:04:20,200 --> 00:04:27,480
is where Mara comes down with all of his armies.

33
00:04:27,480 --> 00:04:32,280
And attacks the Buddha and confronts the Buddha, and says to the Buddha, you don't deserve

34
00:04:32,280 --> 00:04:38,400
to sit under the tree of enlightenment when you're doing, thinking you can become enlightenment,

35
00:04:38,400 --> 00:04:45,880
and the Buddha reflected back on all of his perfections, and all the work he had done

36
00:04:45,880 --> 00:04:57,840
life, time after lifetime, to become a Buddha.

37
00:04:57,840 --> 00:05:09,360
And he put his hand, and so the story goes that Mara says, all of these people back

38
00:05:09,360 --> 00:05:18,960
me, all of his armies, many demons, or the idea is it's just a representation of all evil.

39
00:05:18,960 --> 00:05:25,480
Mara is in this legend, the armies of Mara are actually defilements in the mind, the

40
00:05:25,480 --> 00:05:29,920
daughters of Mara that come and tempt the Buddha, the daughters of Mara are just defilements,

41
00:05:29,920 --> 00:05:35,320
they're just attachment, addiction, and so on.

42
00:05:35,320 --> 00:05:37,640
But Mara says, all of these people are my witness.

43
00:05:37,640 --> 00:05:41,200
They're not worthy of being here, who is your witness?

44
00:05:41,200 --> 00:05:44,280
And the Buddha looked around and he had no witness, and so he put his hand on the earth,

45
00:05:44,280 --> 00:05:46,880
and he said, the earth is my witness.

46
00:05:46,880 --> 00:05:53,760
As the earth is my witness, I am worthy to become enlightened, and the earth shook and

47
00:05:53,760 --> 00:05:59,080
sent forth water that swept away the armies of Mara.

48
00:05:59,080 --> 00:06:03,280
I get the feeling that it was embellished over time to get to that state, that the original

49
00:06:03,280 --> 00:06:13,560
idea was that the Buddha was assailed by defilements, as we all are when we practice.

50
00:06:13,560 --> 00:06:20,160
Meditation centers are often hot, ends of defilement, because we're letting down our

51
00:06:20,160 --> 00:06:26,240
guards, and we're living in close quarters, and it's quite amazing that all six of us

52
00:06:26,240 --> 00:06:29,840
can live here without tearing each other's heads off, right?

53
00:06:29,840 --> 00:06:36,960
Imagine a hundred people living in the meditation center and long-term fights do break

54
00:06:36,960 --> 00:06:41,280
out from time to time.

55
00:06:41,280 --> 00:06:48,360
These are the armies of Mara, towards the armies of Mara that cause conflict.

56
00:06:48,360 --> 00:06:55,920
So with them gone, once the Buddha's mind was purified of the defilements, and understand

57
00:06:55,920 --> 00:07:01,320
that when you're mind is purified from defilements, it doesn't mean you're enlightened.

58
00:07:01,320 --> 00:07:05,800
You can have a very, and this is why the texts are quite clear, if you talk about enlightenment

59
00:07:05,800 --> 00:07:12,880
as being free freedom from defilements, you can't quite say that, because it's possible

60
00:07:12,880 --> 00:07:21,840
for a meditator to think they have no defilements, because they don't at that time.

61
00:07:21,840 --> 00:07:29,000
Once the Buddha's mind was pure, and he was sitting in states of clarity and bliss

62
00:07:29,000 --> 00:07:38,480
and calm, then he started to take stock of, and he finally was in a position to understand

63
00:07:38,480 --> 00:07:41,960
the universe, to understand the truth.

64
00:07:41,960 --> 00:07:47,840
And so he started by looking at the universe, while he started by looking at himself actually,

65
00:07:47,840 --> 00:07:50,560
looking at the past.

66
00:07:50,560 --> 00:07:56,160
He remembered all the way back to his birth, recollecting, reflecting on his life, the

67
00:07:56,160 --> 00:08:01,240
indulgence, the self torture.

68
00:08:01,240 --> 00:08:05,880
And then he broke through, went through, back through the six years of torture, back

69
00:08:05,880 --> 00:08:11,280
through the 20 and nine years of self-indulgement, all the way back to when he was six years

70
00:08:11,280 --> 00:08:17,720
old, six months old, how many months old they don't remember.

71
00:08:17,720 --> 00:08:23,880
Going and lying under a tree, and he entered into the John, when he was just a baby.

72
00:08:23,880 --> 00:08:30,080
Back to when he was five days old, and the ascetic ascita came and proclaimed that he would

73
00:08:30,080 --> 00:08:33,280
be a Buddha.

74
00:08:33,280 --> 00:08:39,440
Back to when he was born the day he was born in Loomvini Garden, and legend says he actually

75
00:08:39,440 --> 00:08:47,280
took seven steps and proclaimed himself to be a future Buddha.

76
00:08:47,280 --> 00:08:52,880
He went back before that day into his mother's womb and back into his past lives.

77
00:08:52,880 --> 00:08:57,880
The point is he went, this is if you want to remember your past lives, you need to remember

78
00:08:57,880 --> 00:09:03,280
like that, enter into high states of tranquility, and then send your mind back and back

79
00:09:03,280 --> 00:09:04,520
and back.

80
00:09:04,520 --> 00:09:07,120
We don't teach that here.

81
00:09:07,120 --> 00:09:11,280
Now I've just taught it, so someday if you ever want to try it when you're alone and

82
00:09:11,280 --> 00:09:13,400
you have time.

83
00:09:13,400 --> 00:09:20,280
I know a man who actually tried it once and said he was able to remember some things.

84
00:09:20,280 --> 00:09:25,120
And so the Buddha remembered all the way back, he remembered all of his past lives, not

85
00:09:25,120 --> 00:09:29,520
all of them, of course, is infinite, but he remembered as far back as he wished, and so

86
00:09:29,520 --> 00:09:36,920
he remembered things that can imagine the history of that were actually possible.

87
00:09:36,920 --> 00:09:42,600
Imagine what you could learn if you could remember not just this life, but lifetime, after

88
00:09:42,600 --> 00:09:52,080
lifetime, and really get an incredible sense of cause and effect, so we don't have that luxury.

89
00:09:52,080 --> 00:09:57,720
We can read about the Buddha's past life stories, luckily 500 or so of them have been

90
00:09:57,720 --> 00:10:06,680
written down and retold, and perhaps with a little embellishment, but interesting anyway.

91
00:10:06,680 --> 00:10:11,480
What the Buddha was able to actually remember.

92
00:10:11,480 --> 00:10:18,960
There are modern day gurus, write books about these things, and claim to be able to hypnotize

93
00:10:18,960 --> 00:10:21,680
people into remembering past lives.

94
00:10:21,680 --> 00:10:29,520
It's interesting to read, I want to take it with a grain of salt, but certainly an interesting

95
00:10:29,520 --> 00:10:31,960
thing to explore the idea of past lives.

96
00:10:31,960 --> 00:10:35,320
The real problem is, of course, it's in the past, and as we talked about last night it

97
00:10:35,320 --> 00:10:42,240
ends up being not intrinsically beneficial, it's quite easy to get lost in the past, caught

98
00:10:42,240 --> 00:10:45,760
up by it, and distracted by it.

99
00:10:45,760 --> 00:10:46,760
So he moved on.

100
00:10:46,760 --> 00:10:56,080
I mean, that was interesting and useful, but a part of his is coming to be a Buddha understanding

101
00:10:56,080 --> 00:10:58,320
this about rebirth.

102
00:10:58,320 --> 00:11:07,120
He moved on and he started to consider other beings, and he entered into, or he started

103
00:11:07,120 --> 00:11:12,920
to see through, I mean, it's all through quite powerful states of mind.

104
00:11:12,920 --> 00:11:20,480
He was able to apparently see beings arising in passing away, so being born and dying.

105
00:11:20,480 --> 00:11:28,800
He was able to send his mind out and to watch, to monitor the minds of others, and watch

106
00:11:28,800 --> 00:11:38,040
them die, and watch them be reborn by knowing their minds.

107
00:11:38,040 --> 00:11:45,640
He could watch them pass away from here and die there, so he was able to see the, on a

108
00:11:45,640 --> 00:11:50,040
broader sense, the nature of cause and effect.

109
00:11:50,040 --> 00:11:54,800
Simply remembering his own past lives was interesting, but watching all the many people

110
00:11:54,800 --> 00:12:00,920
and watching the tendencies, this is what gave him the understanding of karma, cause

111
00:12:00,920 --> 00:12:01,920
and effect.

112
00:12:01,920 --> 00:12:06,600
Again, it's very much all about cause and effect, and this is why the Buddha would all

113
00:12:06,600 --> 00:12:11,840
the time talk about cause and effect, and why the foreignable truths are framed in terms

114
00:12:11,840 --> 00:12:15,960
of cause and effect.

115
00:12:15,960 --> 00:12:20,680
And then finally, in the third watch of the night, so this was three different things

116
00:12:20,680 --> 00:12:21,680
that happened.

117
00:12:21,680 --> 00:12:28,720
The third thing that happened is he started to look closer at the general picture, or

118
00:12:28,720 --> 00:12:32,880
the bigger picture, and so this is where he came up with what we call dependent or

119
00:12:32,880 --> 00:12:44,880
intonation, or put it to Samupada, where he saw that in general, it's people's cravings,

120
00:12:44,880 --> 00:12:48,880
people's desires that lead them to be reborn again.

121
00:12:48,880 --> 00:12:54,080
People want something, and then they die wanting that, and they go on in that way, or

122
00:12:54,080 --> 00:12:59,720
they're attached to something, maybe they're afraid of something or a verse to that

123
00:12:59,720 --> 00:13:00,720
thing.

124
00:13:00,720 --> 00:13:15,000
Or that's the tension, the fuel that leads to rebirth, and he saw that it was because

125
00:13:15,000 --> 00:13:19,960
they thought those things were going to bring them happiness and peace, but he also saw

126
00:13:19,960 --> 00:13:27,280
that none of these people ever found happiness or peace, and he realized that it was ignorance,

127
00:13:27,280 --> 00:13:29,240
that this was wrong.

128
00:13:29,240 --> 00:13:30,680
This was where the problem was.

129
00:13:30,680 --> 00:13:37,920
The problem lay in thinking that wanting things or clinging to things, either positive,

130
00:13:37,920 --> 00:13:44,000
clinging, negative, clinging, and that was the problem.

131
00:13:44,000 --> 00:13:48,840
So that's where he came up with the four noble truths that craving is the cause of suffering,

132
00:13:48,840 --> 00:13:52,880
thirst is the cause of suffering.

133
00:13:52,880 --> 00:13:57,040
And he came up with putitya, Samupada is this very important teaching that I often talk

134
00:13:57,040 --> 00:14:01,160
about, which is Avinja, Pachea, Samkara.

135
00:14:01,160 --> 00:14:08,960
Samkara are all of our ambitions, you might say.

136
00:14:08,960 --> 00:14:15,760
So karma, really, when you want to do something good, do something evil, but all of

137
00:14:15,760 --> 00:14:26,240
our ambitions, they're caused by ignorance, they realize that there's no ambition.

138
00:14:26,240 --> 00:14:39,680
There's no inclination, no inclination, but no desire that can possibly satisfy

139
00:14:39,680 --> 00:14:41,880
lead dissatisfaction.

140
00:14:41,880 --> 00:14:49,160
And he realized that that's all it took, that once you got rid of that ignorance, once

141
00:14:49,160 --> 00:14:53,680
you got rid of your ignorance about reality, thinking that there was something you could

142
00:14:53,680 --> 00:15:02,920
find that was stable, satisfying, controllable, me, mine, going to hold on to this, once

143
00:15:02,920 --> 00:15:14,240
you give that up, you stop suffering, you're free.

144
00:15:14,240 --> 00:15:21,680
And I guess technically how it worked is, I mean, not guess, but how it works is you

145
00:15:21,680 --> 00:15:27,320
see, right, that this ignorance, how the Buddha came to understand this is because he

146
00:15:27,320 --> 00:15:35,000
got rid of his ignorance, he came to see, hey, these things that I'm clinging to, these

147
00:15:35,000 --> 00:15:42,080
things are not stable, not satisfying, not controllable, which is what you're all doing

148
00:15:42,080 --> 00:15:43,080
here, right?

149
00:15:43,080 --> 00:15:47,320
Every moment when you see the things that you cling to, this is good, this is bad, this

150
00:15:47,320 --> 00:15:55,400
is right, this is wrong, this is me, this is mine, all of our judgments, when you see

151
00:15:55,400 --> 00:16:02,600
how stressful they are, when you see that they're not really you, you're not the one in

152
00:16:02,600 --> 00:16:14,880
control of them, they're unwieldy, uncontrollable, unpredictable, then you let go of them,

153
00:16:14,880 --> 00:16:22,680
and that that ignorant, that knowledge, that over could dispell as the ignorance, that's

154
00:16:22,680 --> 00:16:36,480
enough to free you from all suffering, the Buddha had a really good night, and you know,

155
00:16:36,480 --> 00:16:42,000
this is our leader, right, I don't know, many of us call ourselves Buddhist, it doesn't

156
00:16:42,000 --> 00:16:47,680
really matter, but whether you just think of the Buddha as a great guy, or whether you

157
00:16:47,680 --> 00:16:56,320
really take the Buddha as your refuge, in the sense that you really feel that his teachings

158
00:16:56,320 --> 00:17:04,360
are something to dedicate your life to, it doesn't really matter, but nonetheless, this

159
00:17:04,360 --> 00:17:14,320
is the guy, this is our point of reference, it's quite inspiring to think of, right, because

160
00:17:14,320 --> 00:17:27,480
like other religions have their person, Jesus, Muhammad, Hindus have Krishna, and we have

161
00:17:27,480 --> 00:17:37,040
the Buddha, this is the story, but also the Buddha's enlightenment is the enlightenment,

162
00:17:37,040 --> 00:17:42,800
to many, in many ways the enlightenment that we're striving for, not completely, we may

163
00:17:42,800 --> 00:17:48,640
never remember our past lives or be able to see other people being born and dying, but

164
00:17:48,640 --> 00:17:55,280
we sure can understand the cause of suffering and free ourselves from it, through understanding

165
00:17:55,280 --> 00:18:04,360
suffering, so there you go, a little bit of Dhamma for tonight, thank you all for coming

166
00:18:04,360 --> 00:18:21,360
up, I have a few questions here,

167
00:18:21,360 --> 00:18:45,120
ok.

168
00:18:45,120 --> 00:19:14,960
Can you be totally free from attachments if you still want to eat an

169
00:19:14,960 --> 00:19:22,240
enough to live and have the intention to meditate?

170
00:19:22,240 --> 00:19:30,120
Want to eat enough to live, probably not, because that's kind of an attachment.

171
00:19:30,120 --> 00:19:35,960
So an enlightened being doesn't have that want, which means, I mean, we're talking

172
00:19:35,960 --> 00:19:41,960
someone who's gone to the end, right, because enlightenment is sort of a gradual thing.

173
00:19:41,960 --> 00:19:45,720
Once you've seen nibana, you still have attachments, it's just they're quite weakened

174
00:19:45,720 --> 00:19:49,000
because there's no wrong view to support them.

175
00:19:49,000 --> 00:19:54,440
Wrong view is gone, so you don't think it's right that you want to eat and want to live,

176
00:19:54,440 --> 00:19:55,440
but you still do.

177
00:19:55,440 --> 00:20:01,040
You still haven't worked at all that, because you don't think it's right, it'll eventually

178
00:20:01,040 --> 00:20:06,840
peter out and fade away.

179
00:20:06,840 --> 00:20:11,840
But once you become completely enlightened, then in many cases, yes, there is a

180
00:20:11,840 --> 00:20:15,120
just a starving to death, because he really aren't concerned with it.

181
00:20:15,120 --> 00:20:19,920
I mean, this was a thing in India already, we're starving to death is kind of a religious

182
00:20:19,920 --> 00:20:25,880
thing in India, because before the Buddha they were thinking, this is a way to free

183
00:20:25,880 --> 00:20:31,840
yourself from attachment, but because they still had the attachment to food, it was just

184
00:20:31,840 --> 00:20:34,960
a forcing and that doesn't work.

185
00:20:34,960 --> 00:20:41,000
But it's interesting, horrific from it, most people I think, how could you possibly do

186
00:20:41,000 --> 00:20:42,000
such a thing?

187
00:20:42,000 --> 00:20:47,080
I'm quite interesting from a philosophical point of view, I mean, if you're free from

188
00:20:47,080 --> 00:20:55,080
suffering, why would you worry about eating to live?

189
00:20:55,080 --> 00:21:03,640
At the strong emotions are dominating during my formal meditation, should I stay with

190
00:21:03,640 --> 00:21:05,040
them until they go away?

191
00:21:05,040 --> 00:21:09,520
As I'm doing now, the cost of doing much less walking during a session or ignore them

192
00:21:09,520 --> 00:21:11,400
and get back to focusing.

193
00:21:11,400 --> 00:21:18,920
If they don't go away, you can ignore them, but it's often good to change postures if

194
00:21:18,920 --> 00:21:20,880
they're really strong.

195
00:21:20,880 --> 00:21:23,680
I mean, it's something you generally just have to, there's no answer, I mean, I don't

196
00:21:23,680 --> 00:21:28,760
have a solution for you, so don't expect me to be able to fix that for you.

197
00:21:28,760 --> 00:21:32,960
It's something you have to be patient with, and it's great that you're seeing them and

198
00:21:32,960 --> 00:21:42,800
learning about them, just take your time to learn more about them and understand them better.

199
00:21:42,800 --> 00:21:47,440
In the end, it's not going to matter so much, what you do, just try to be flexible, and

200
00:21:47,440 --> 00:21:53,200
over time they will recede, they'll become more manageable, because a lot of those kind

201
00:21:53,200 --> 00:21:58,360
of things have to do with the time when before we were mindful, before we were practicing

202
00:21:58,360 --> 00:22:04,160
meditation, so just don't feed them.

203
00:22:04,160 --> 00:22:09,240
Would you say mindfulness is the same, I've already given a talk on mindfulness, so I'm

204
00:22:09,240 --> 00:22:16,760
not going to answer that because I gave my answer to what mindfulness is, so I'm not

205
00:22:16,760 --> 00:22:21,600
going to say mindfulness, because you're the second person to just ask this recently, makes

206
00:22:21,600 --> 00:22:27,560
me wonder whether I actually explained it well enough, maybe I didn't, but I hope that

207
00:22:27,560 --> 00:22:31,320
you go and read that and I hope it does explain it, if not, you could read my booklet

208
00:22:31,320 --> 00:22:41,800
because that should give you some idea, hopefully, it feels like there's no excuse to

209
00:22:41,800 --> 00:22:50,120
not drop everything, no excuse to drop, no excuse to not drop everything and follow a

210
00:22:50,120 --> 00:22:54,960
monk life or this feels wrong somehow, it's this something you work towards a result of

211
00:22:54,960 --> 00:23:09,160
craving or is this sense of extremism and wholesome, come up, check, remove, circle.

212
00:23:09,160 --> 00:23:18,160
I mean, it's a bit vague what you're saying, but I'm assuming that you mean, I'm

213
00:23:18,160 --> 00:23:27,000
assuming what's going on here is this conflicting sense of what's right, because of course

214
00:23:27,000 --> 00:23:31,600
it's probably, I would assume it's fairly new to you, this idea that becoming a monk

215
00:23:31,600 --> 00:23:36,640
is the right thing to do and what's been ingrained into your mind is that the right thing

216
00:23:36,640 --> 00:23:41,640
to do is be a responsible member of society, so it's just a question of when you can let

217
00:23:41,640 --> 00:23:49,640
go of that wrong view, a wrong perception, there's no reason to support society, society

218
00:23:49,640 --> 00:23:57,600
is just a bunch of people running around doing meaningless things, well, they may not

219
00:23:57,600 --> 00:24:05,240
entirely meaningless, but in the end, I mean, the end it's all meaningless, so there's

220
00:24:05,240 --> 00:24:13,920
no, I mean, meaningless in the sense that there's no reason to support it, the only reason

221
00:24:13,920 --> 00:24:20,800
one could have is in terms of helping each other, helping each other be free from suffering,

222
00:24:20,800 --> 00:24:28,480
which is a fairly big and complicated goal, so I mean, that's the sense that I think society

223
00:24:28,480 --> 00:24:38,480
is important, useful, where we have Buddhist society, monastics, society, and it's important,

224
00:24:38,480 --> 00:24:46,600
but to that end, I mean, what helps people the most, what helps you or anyone else the

225
00:24:46,600 --> 00:24:53,080
most, it's obviously becoming enlightened, so there should be no conflict here, it's

226
00:24:53,080 --> 00:24:59,480
just that most likely you're conflicted as a non-Buddhist, become Buddhist, and I mean,

227
00:24:59,480 --> 00:25:02,960
that's interesting, it's interesting how even in Asia, that's the case, people grew up

228
00:25:02,960 --> 00:25:07,120
as quote unquote Buddhists have the same conflict because they're also taught the wrong

229
00:25:07,120 --> 00:25:14,960
way, they're also taught that the way to do the right thing is to make lots of money

230
00:25:14,960 --> 00:25:22,200
and be successful and maybe support your parents as well, but become a monk, no, that's

231
00:25:22,200 --> 00:25:35,560
not responsible, that's irresponsible, hopefully you get over that, okay, that's the demo

232
00:25:35,560 --> 00:26:00,800
for tonight, thank you all for coming up.

