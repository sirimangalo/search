1
00:00:00,000 --> 00:00:07,000
Okay, good evening, everyone.

2
00:00:07,000 --> 00:00:14,000
August 30, 2015, broadcasting live.

3
00:00:14,000 --> 00:00:18,000
Today we have a story, not just a quote.

4
00:00:18,000 --> 00:00:21,000
So what do you say, well, it's two half each?

5
00:00:21,000 --> 00:00:22,000
Sure.

6
00:00:22,000 --> 00:00:28,000
You can go down to what then do I need?

7
00:00:28,000 --> 00:00:30,000
Okay.

8
00:00:30,000 --> 00:00:34,000
In due time, Keisa Gotama became pregnant,

9
00:00:34,000 --> 00:00:38,000
and after 10 lunar months, she gave birth to a son.

10
00:00:38,000 --> 00:00:42,000
But the child died as soon as he was able to walk.

11
00:00:42,000 --> 00:00:45,000
He's a goat to me, had not known death before,

12
00:00:45,000 --> 00:00:48,000
and when they came to remove the child's body for cremation,

13
00:00:48,000 --> 00:00:51,000
she refused to let them do so, saying to herself,

14
00:00:51,000 --> 00:00:54,000
I'll get medicine for my son.

15
00:00:54,000 --> 00:00:56,000
Placing the dead child on her hip,

16
00:00:56,000 --> 00:01:00,000
from house to house pleading, do you know a cure for my son?

17
00:01:00,000 --> 00:01:01,000
Everyone said to her,

18
00:01:01,000 --> 00:01:05,000
woman, you are completely mad in seeking medicine for your son.

19
00:01:05,000 --> 00:01:07,000
But she went away thinking,

20
00:01:07,000 --> 00:01:11,000
truly, I will find someone who knows the right medicine for my son.

21
00:01:11,000 --> 00:01:14,000
Now a certain wise man saw her and thought to himself,

22
00:01:14,000 --> 00:01:15,000
I must help her.

23
00:01:15,000 --> 00:01:19,000
So he said, woman, I do not know if there is a cure for your child,

24
00:01:19,000 --> 00:01:22,000
but there is one who will know, and I know him.

25
00:01:22,000 --> 00:01:25,000
Sir, who is it who will know?

26
00:01:25,000 --> 00:01:28,000
Woman, the Lord will know, go and ask him.

27
00:01:28,000 --> 00:01:31,000
So she went to the Lord, paid reverence to him,

28
00:01:31,000 --> 00:01:33,000
stood at one side and asked,

29
00:01:33,000 --> 00:01:35,000
venerable sir, is it true as people say

30
00:01:35,000 --> 00:01:37,000
that you will know a cure for my son?

31
00:01:37,000 --> 00:01:39,000
Yes, I know.

32
00:01:39,000 --> 00:01:44,000
Sorry, I'm okay, but probably.

33
00:01:44,000 --> 00:01:48,000
What then do I need?

34
00:01:48,000 --> 00:01:50,000
A few mustard seeds.

35
00:01:50,000 --> 00:01:53,000
I will get them venerable, sir, but in whose house?

36
00:01:53,000 --> 00:01:55,000
Get them from a house where no son or daughter

37
00:01:55,000 --> 00:01:58,000
or any other person has ever died.

38
00:01:58,000 --> 00:02:00,000
Very well, sir.

39
00:02:00,000 --> 00:02:02,000
He said, go to me, send.

40
00:02:02,000 --> 00:02:04,000
Very well, sir.

41
00:02:04,000 --> 00:02:06,000
He said, and having paid reverence to the Lord

42
00:02:06,000 --> 00:02:09,000
and having placed the dead child on her hip,

43
00:02:09,000 --> 00:02:13,000
she went to the village and stopped at the very first house.

44
00:02:13,000 --> 00:02:15,000
Have you any mustard seeds?

45
00:02:15,000 --> 00:02:17,000
They say they will cure my child.

46
00:02:17,000 --> 00:02:19,000
They gave her the seeds and then she asked friend

47
00:02:19,000 --> 00:02:22,000
has any son or daughter died in this house.

48
00:02:22,000 --> 00:02:25,000
What do you ask, woman?

49
00:02:25,000 --> 00:02:27,000
The living are few and the dead are many.

50
00:02:27,000 --> 00:02:30,000
Then take back your seeds for they will not cure my child,

51
00:02:30,000 --> 00:02:34,000
she said, and return to the seeds they had given her.

52
00:02:34,000 --> 00:02:37,000
In this way, she went from house to house,

53
00:02:37,000 --> 00:02:40,000
but never did she find one that had the mustard seeds

54
00:02:40,000 --> 00:02:41,000
that she needed.

55
00:02:41,000 --> 00:02:45,000
Then she thought, oh, it is a difficult task that I have.

56
00:02:45,000 --> 00:02:49,000
I thought it was only I who had lost a child,

57
00:02:49,000 --> 00:02:52,000
but in every village the dead are more than the living.

58
00:02:52,000 --> 00:02:56,000
While she reflected us, her heart which had trembled,

59
00:02:56,000 --> 00:02:58,000
now became still.

60
00:03:02,000 --> 00:03:05,000
It's funny all this talk about death.

61
00:03:05,000 --> 00:03:07,000
Today we had a full session.

62
00:03:07,000 --> 00:03:10,000
And we said, do you mind about death?

63
00:03:10,000 --> 00:03:12,000
Which was funny because this morning

64
00:03:12,000 --> 00:03:15,000
that they came to honor a dead person

65
00:03:15,000 --> 00:03:18,000
who they had just buried the day before

66
00:03:18,000 --> 00:03:21,000
and whose funeral I had gone to the day before.

67
00:03:21,000 --> 00:03:25,000
So yesterday I was walking through this, the graveyard.

68
00:03:25,000 --> 00:03:28,000
Gravyards are interesting places.

69
00:03:28,000 --> 00:03:31,000
You have so much work goes into them

70
00:03:31,000 --> 00:03:36,000
and it's such a full meaning.

71
00:03:36,000 --> 00:03:41,000
Each grave stone means something very important.

72
00:03:41,000 --> 00:03:47,000
If you think about it, a grave stone is,

73
00:03:47,000 --> 00:03:52,000
let's say it's got a huge, such a story.

74
00:03:52,000 --> 00:03:57,000
It's a piece in the puzzle of Samsara.

75
00:03:57,000 --> 00:03:59,000
Then if you think beyond that,

76
00:03:59,000 --> 00:04:01,000
then you think of the Buddha's words

77
00:04:01,000 --> 00:04:03,000
that there's nowhere on,

78
00:04:03,000 --> 00:04:07,000
there's no one on this earth that we haven't died.

79
00:04:07,000 --> 00:04:14,000
But that someone hasn't died.

80
00:04:14,000 --> 00:04:18,000
I think of all of the people who have ever died,

81
00:04:18,000 --> 00:04:27,000
the grave yards are only a very, very small portion of those people.

82
00:04:27,000 --> 00:04:33,000
Let me see, think about how many stories there are out there.

83
00:04:33,000 --> 00:04:36,000
How many stories have we lived?

84
00:04:36,000 --> 00:04:39,000
How many times have we played this game?

85
00:04:39,000 --> 00:04:43,000
How many times have we wept for a child?

86
00:04:43,000 --> 00:04:50,000
A parent, a sister, a brother, a loved one?

87
00:04:50,000 --> 00:04:52,000
You read the inscriptions,

88
00:04:52,000 --> 00:05:00,000
some of them are 100 years old.

89
00:05:00,000 --> 00:05:05,000
There are all sorts of ideas about what happens when you die.

90
00:05:05,000 --> 00:05:10,000
Death is an interesting thing.

91
00:05:10,000 --> 00:05:16,000
It's interesting that we have all this talk about death on it.

92
00:05:16,000 --> 00:05:25,000
The key to go to me,

93
00:05:25,000 --> 00:05:30,000
I guess the idea is she lived a sheltered life.

94
00:05:30,000 --> 00:05:34,000
She had never really gotten a sense of

95
00:05:34,000 --> 00:05:36,000
not getting her way really.

96
00:05:36,000 --> 00:05:41,000
She was a relative of the Buddha, I think,

97
00:05:41,000 --> 00:05:45,000
or belong to the same clan and she was royalty.

98
00:05:45,000 --> 00:05:49,000
I think, and so probably she always got what she wanted.

99
00:05:49,000 --> 00:05:51,000
She was certainly sheltered.

100
00:05:51,000 --> 00:05:53,000
Women probably didn't get very good education,

101
00:05:53,000 --> 00:05:59,000
so they just didn't understand about what death really meant.

102
00:05:59,000 --> 00:06:04,000
And that coupled with her grief,

103
00:06:04,000 --> 00:06:06,000
you know, the story.

104
00:06:06,000 --> 00:06:09,000
Have you ever seen there was a movie back in the 80s

105
00:06:09,000 --> 00:06:12,000
or 90s called Pet Cemetery?

106
00:06:12,000 --> 00:06:15,000
Where they bury their child.

107
00:06:15,000 --> 00:06:22,000
The lengths people will go to to

108
00:06:22,000 --> 00:06:27,000
not get to be reunited with their loved ones.

109
00:06:27,000 --> 00:06:29,000
The terrible movie.

110
00:06:49,000 --> 00:06:51,000
So I don't know.

111
00:06:51,000 --> 00:06:53,000
Not much to say.

112
00:06:53,000 --> 00:06:57,000
It becomes her mind, my heart that had trembled now became still,

113
00:06:57,000 --> 00:07:01,000
and then I was just reading what happened next.

114
00:07:01,000 --> 00:07:04,000
The Buddha says,

115
00:07:04,000 --> 00:07:09,000
when you realize that your son was dead,

116
00:07:09,000 --> 00:07:12,000
you realize that Dua Damo,

117
00:07:12,000 --> 00:07:16,000
a certain dumb,

118
00:07:16,000 --> 00:07:19,000
a constant reality.

119
00:07:19,000 --> 00:07:23,000
Dua Damo is a satan, and this is a durable,

120
00:07:23,000 --> 00:07:27,000
I guess the English equivalent of Dua,

121
00:07:27,000 --> 00:07:30,000
but a constant dhamma of beings.

122
00:07:30,000 --> 00:07:32,000
Machura, jahi,

123
00:07:32,000 --> 00:07:35,000
sabasate, aparipunan, jahi,

124
00:07:35,000 --> 00:07:38,000
se, jahi,

125
00:07:38,000 --> 00:07:42,000
avamahogo, vyat,

126
00:07:42,000 --> 00:07:46,000
parikatamando, vywa.

127
00:07:46,000 --> 00:07:49,000
Anyway, so, and he says that

128
00:07:49,000 --> 00:07:52,000
Machura jahi takes all beings,

129
00:07:52,000 --> 00:08:16,000
just like a great flood.

130
00:08:16,000 --> 00:08:19,000
Hey, this is in relation to the dhammapada.

131
00:08:19,000 --> 00:08:20,000
It's number 114.

132
00:08:20,000 --> 00:08:21,000
Maybe someday we'll get there.

133
00:08:21,000 --> 00:08:23,000
We'll actually tell this story.

134
00:08:23,000 --> 00:08:25,000
Yo, jua, sabasate, jahi,

135
00:08:25,000 --> 00:08:29,000
apasang, amatang, vidhamma.

136
00:08:29,000 --> 00:08:31,000
Ika, hang, ji, vidang, se,

137
00:08:31,000 --> 00:08:33,000
apasate, or amatang, vidham.

138
00:08:33,000 --> 00:08:36,000
Who should live?

139
00:08:36,000 --> 00:08:37,000
Wasa, sabthang,

140
00:08:37,000 --> 00:08:39,000
a hundred years,

141
00:08:39,000 --> 00:08:41,000
without seeing the path to the deathless,

142
00:08:41,000 --> 00:08:43,000
or the deathless,

143
00:08:43,000 --> 00:08:45,000
path to the undead.

144
00:08:45,000 --> 00:08:47,000
Like Stephen King,

145
00:08:47,000 --> 00:08:48,000
pet cemetery,

146
00:08:48,000 --> 00:08:50,000
where they bury this child.

147
00:08:50,000 --> 00:08:52,000
First, they bury their cat,

148
00:08:52,000 --> 00:08:53,000
and the cat comes back to life,

149
00:08:53,000 --> 00:08:55,000
but it's gond psychotic.

150
00:08:55,000 --> 00:08:57,000
And so then their kid gets run over.

151
00:08:57,000 --> 00:08:58,000
Their baby gets,

152
00:08:58,000 --> 00:09:00,000
toddler gets run over.

153
00:09:00,000 --> 00:09:02,000
And so they bury the toddler,

154
00:09:02,000 --> 00:09:03,000
and it comes back as this vicious,

155
00:09:03,000 --> 00:09:06,000
murdering creature.

156
00:09:06,000 --> 00:09:08,000
And it kills his wife,

157
00:09:08,000 --> 00:09:10,000
and so then he buries his wife.

158
00:09:10,000 --> 00:09:13,000
And then she comes back.

159
00:09:13,000 --> 00:09:15,000
I don't know why I'm talking about.

160
00:09:15,000 --> 00:09:16,000
I'll just say the idea of,

161
00:09:16,000 --> 00:09:17,000
is it's a curious word.

162
00:09:17,000 --> 00:09:19,000
Amatamid's undead,

163
00:09:19,000 --> 00:09:21,000
which is misleading,

164
00:09:21,000 --> 00:09:23,000
in modern English usage.

165
00:09:27,000 --> 00:09:29,000
Not dead.

166
00:09:29,000 --> 00:09:33,000
The state of being not dead.

167
00:09:33,000 --> 00:09:35,000
But it really means that,

168
00:09:35,000 --> 00:09:37,000
which that which never

169
00:09:37,000 --> 00:09:39,000
is not subject to death.

170
00:09:41,000 --> 00:09:42,000
The path to that,

171
00:09:42,000 --> 00:09:44,000
which is not subject to death.

172
00:09:44,000 --> 00:09:47,000
Is it better,

173
00:09:47,000 --> 00:09:50,000
is it to live one day,

174
00:09:50,000 --> 00:09:53,000
seeing the path to that,

175
00:09:53,000 --> 00:09:55,000
which is not subject to death?

176
00:10:08,000 --> 00:10:10,000
Brooklyn put a comment that the reading sounds

177
00:10:10,000 --> 00:10:12,000
straight out of the New Testament.

178
00:10:12,000 --> 00:10:14,000
I was noticing that too.

179
00:10:14,000 --> 00:10:15,000
A lot of these,

180
00:10:15,000 --> 00:10:17,000
they sound with the use of the word Lord.

181
00:10:17,000 --> 00:10:19,000
Is that a normal translation?

182
00:10:19,000 --> 00:10:21,000
Yeah, I was looking that up.

183
00:10:21,000 --> 00:10:22,000
It's not Lord.

184
00:10:22,000 --> 00:10:24,000
It's probably Blessed One.

185
00:10:24,000 --> 00:10:28,000
Yeah, they do sound kind of New Testament.

186
00:10:28,000 --> 00:10:30,000
Well, this acts, yeah.

187
00:10:30,000 --> 00:10:32,000
No, actually he's a fairly new one.

188
00:10:32,000 --> 00:10:34,000
You see,

189
00:10:34,000 --> 00:10:44,000
but many, many teachers would.

190
00:10:52,000 --> 00:10:54,000
Sata, why did he use Lord?

191
00:10:54,000 --> 00:10:56,000
This Sata means teacher.

192
00:10:56,000 --> 00:11:12,000
Yeah, very ridiculous.

193
00:11:12,000 --> 00:11:16,000
Sata comes from

194
00:11:16,000 --> 00:11:18,000
Shas, the root Shas,

195
00:11:18,000 --> 00:11:24,000
which is in regards to teaching them.

196
00:11:24,000 --> 00:11:36,000
I don't know why he used a teacher master.

197
00:11:36,000 --> 00:11:38,000
Sata is one who has a sassana.

198
00:11:38,000 --> 00:11:40,000
It comes from the same root as sassana.

199
00:11:40,000 --> 00:11:42,000
Sassana is that which is taught.

200
00:11:42,000 --> 00:12:02,000
It's nothing to do with being a Lord.

201
00:12:02,000 --> 00:12:12,000
Now, reflection on death is something that is beneficial,

202
00:12:12,000 --> 00:12:15,000
something that, you know, if you think yesterday,

203
00:12:15,000 --> 00:12:18,000
they're going to see the,

204
00:12:18,000 --> 00:12:22,000
there's people at the funeral and the sadness and the grief.

205
00:12:22,000 --> 00:12:34,000
It's undeniable.

206
00:12:34,000 --> 00:12:41,000
So there's when this idea of preparing ourselves for death doesn't mean just preparing ourselves for our own death.

207
00:12:41,000 --> 00:12:47,000
It means becoming a better surfer on the waves of samsara,

208
00:12:47,000 --> 00:12:52,000
being able to take life as it comes without getting upset,

209
00:12:52,000 --> 00:12:56,000
finding a way to live our lives without being dependent on anything.

210
00:12:56,000 --> 00:12:58,000
Anisito jibihalati.

211
00:12:58,000 --> 00:13:01,000
So that we don't grieve when something happens.

212
00:13:01,000 --> 00:13:03,000
I mean, how attached this woman was to her child.

213
00:13:03,000 --> 00:13:06,000
I can't even imagine what it's like to lose a child.

214
00:13:06,000 --> 00:13:13,000
But, nonetheless, there is, it's based on attachment,

215
00:13:13,000 --> 00:13:18,000
which we know can be avoided.

216
00:13:18,000 --> 00:13:26,000
Not avoided can be cleansed to the point where one is no longer subject to attachment.

217
00:13:26,000 --> 00:13:41,000
No longer gives rise to it.

218
00:13:41,000 --> 00:13:49,000
I think it's even worse if you think about it for the person who's passed away.

219
00:13:49,000 --> 00:13:56,000
We always see the people who are left behind are the ones who have trouble.

220
00:13:56,000 --> 00:14:00,000
And what we see is the grieving of those who have lost, right?

221
00:14:00,000 --> 00:14:02,000
What about those who have gotten lost?

222
00:14:02,000 --> 00:14:05,000
What about those who have left?

223
00:14:05,000 --> 00:14:11,000
That's where that's the real, where Buddhism really focuses its attention

224
00:14:11,000 --> 00:14:14,000
on the person who passes away.

225
00:14:14,000 --> 00:14:17,000
Because if you grieve, most people will get over it.

226
00:14:17,000 --> 00:14:21,000
But if you die, well, you don't get over that.

227
00:14:21,000 --> 00:14:28,000
It's non-negotiable.

228
00:14:28,000 --> 00:14:33,000
And it's monumentous.

229
00:14:33,000 --> 00:14:36,000
So preparation for death.

230
00:14:36,000 --> 00:14:42,000
It's important to think about the death of others, loss, and so on.

231
00:14:42,000 --> 00:14:47,000
Most important is to prepare yourself to serve the waves,

232
00:14:47,000 --> 00:14:50,000
to transition gracefully.

233
00:14:50,000 --> 00:14:53,000
So when you die, you're prepared for it.

234
00:14:53,000 --> 00:14:54,000
You're alert.

235
00:14:54,000 --> 00:15:00,000
You're not caught off guard by your own mind, really.

236
00:15:00,000 --> 00:15:03,000
Because your mind will lead you.

237
00:15:03,000 --> 00:15:06,000
If your mind is set in a bad way, it will lead you in a bad way.

238
00:15:06,000 --> 00:15:08,000
Do you know all what's in your mind?

239
00:15:08,000 --> 00:15:10,000
Do you know how your mind is like?

240
00:15:10,000 --> 00:15:12,000
This is what meditation helps.

241
00:15:12,000 --> 00:15:19,000
Much of what we would see when we pass away comes up when we meditate.

242
00:15:19,000 --> 00:15:26,000
So when you are in the present moment, your mind starts to bring up all the things

243
00:15:26,000 --> 00:15:30,000
that it's been holding on to that have been bouncing around inside.

244
00:15:30,000 --> 00:15:33,000
So bad deeds we've done, bad things that have happened to us.

245
00:15:33,000 --> 00:15:35,000
Anything that we're still holding on to.

246
00:15:35,000 --> 00:15:38,000
The kind of things that would flash before our eyes when we die.

247
00:15:38,000 --> 00:15:43,000
And they do that because of the concentration involved.

248
00:15:43,000 --> 00:15:50,000
When you meditate, there's the same sort of concentration and just general awareness.

249
00:15:50,000 --> 00:15:53,000
And so you'll see these sorts of things come up.

250
00:15:53,000 --> 00:15:58,000
Anything you are holding on to, anything you feel guilty about or feel angry about,

251
00:15:58,000 --> 00:16:01,000
anything you're attached to.

252
00:16:01,000 --> 00:16:07,000
And it's a chance to purify all of this and to become less clingy, less dependent,

253
00:16:07,000 --> 00:16:13,000
to be able to fly, to be able to surf.

254
00:16:13,000 --> 00:16:23,000
Surf the waves and not drown in the flood.

255
00:16:23,000 --> 00:16:24,000
Anyway.

256
00:16:24,000 --> 00:16:30,000
One interesting thing today was that the idea that to practice mindfulness of death,

257
00:16:30,000 --> 00:16:37,000
all you have to do is sit there and say, death, death, maroonam, maroonam.

258
00:16:37,000 --> 00:16:42,000
That's a valid meditation practice according to the city manga.

259
00:16:42,000 --> 00:16:48,000
So go ahead and you can all try it.

260
00:16:48,000 --> 00:16:50,000
Anyway, any questions?

261
00:16:50,000 --> 00:16:53,000
That's the recollection of death.

262
00:16:53,000 --> 00:16:58,000
That's recommended for the the engines of sleepiness, right?

263
00:16:58,000 --> 00:16:59,000
What?

264
00:16:59,000 --> 00:17:08,000
Recommended for to try to overcome the engines of sloth and torpor sleepiness.

265
00:17:08,000 --> 00:17:18,000
Lacing is lack of lack of initiative, lack of encouragement.

266
00:17:18,000 --> 00:17:21,000
That makes sense.

267
00:17:21,000 --> 00:17:23,000
You ready for a question, Bhante?

268
00:17:23,000 --> 00:17:24,000
Sure.

269
00:17:24,000 --> 00:17:26,000
Venerable sir.

270
00:17:26,000 --> 00:17:31,000
What is the main meaning of a pamada discipline?

271
00:17:31,000 --> 00:17:39,000
Pamada means non-intoxication.

272
00:17:39,000 --> 00:17:48,000
It's a state of having a clear mind, sober mind.

273
00:17:48,000 --> 00:17:50,000
Basically means to have mindfulness.

274
00:17:50,000 --> 00:17:52,000
Appamada isn't a real thing.

275
00:17:52,000 --> 00:17:56,000
It's just a conventional term.

276
00:17:56,000 --> 00:18:08,000
It's a characteristic of mindful state.

277
00:18:08,000 --> 00:18:14,000
Why are we able to pacify the mind by observing our thoughts and feelings?

278
00:18:14,000 --> 00:18:18,000
Not just by observing them, but becoming objective about them.

279
00:18:18,000 --> 00:18:21,000
By reminding ourselves it's just a feeling it's just that.

280
00:18:21,000 --> 00:18:26,000
Because the unpasified mind is the mind that is reacting.

281
00:18:26,000 --> 00:18:32,000
The mind that is making connections.

282
00:18:32,000 --> 00:18:40,000
Getting into loops that feed back and get bigger and bigger and more stressful.

283
00:18:40,000 --> 00:18:48,000
Mindfulness cuts off the chain and keeps us from becoming overwhelmed.

284
00:18:48,000 --> 00:18:54,000
Passivize because there's no continuation.

285
00:18:54,000 --> 00:18:58,000
But it was very clear on this.

286
00:18:58,000 --> 00:19:03,000
Then there's no attachment.

287
00:19:03,000 --> 00:19:08,000
There's no idea of me, mine, et cetera.

288
00:19:08,000 --> 00:19:20,000
If someone who doesn't grasp at particular or characteristics of the object, this is the direction that they're going to give.

289
00:19:20,000 --> 00:19:22,000
Don't get into the details.

290
00:19:22,000 --> 00:19:27,000
Just see things simply as they are very simple, basic.

291
00:19:27,000 --> 00:19:29,000
And it keeps you from reacting.

292
00:19:29,000 --> 00:19:35,000
It keeps you from getting caught up.

293
00:19:35,000 --> 00:19:40,000
There was an old old question from the Google moderator that we never got to.

294
00:19:40,000 --> 00:19:43,000
And I always hoped that we did because I was wondering about the answer.

295
00:19:43,000 --> 00:19:48,000
It was somebody that had asked when people listened to the Buddha.

296
00:19:48,000 --> 00:19:57,000
And if they were being mindful and just noting hearing, hearing, how would they have actually heard what he said?

297
00:19:57,000 --> 00:19:59,000
That actually is possible.

298
00:19:59,000 --> 00:20:06,000
I mean, the noting isn't all the time.

299
00:20:06,000 --> 00:20:10,000
When you're not hearing hearing, you actually can still get interrupted by the processing.

300
00:20:10,000 --> 00:20:12,000
But it's so very quick.

301
00:20:12,000 --> 00:20:16,000
We don't take nearly as long to process as we think.

302
00:20:16,000 --> 00:20:21,000
Getting the meaning is still possible.

303
00:20:21,000 --> 00:20:24,000
Can I go back and forth very quickly?

304
00:20:24,000 --> 00:20:28,000
Yeah, and the mind does that anyway.

305
00:20:28,000 --> 00:20:31,000
Because you won't process it very much.

306
00:20:31,000 --> 00:20:32,000
You don't need to.

307
00:20:32,000 --> 00:20:36,000
Even if you're not always hearing hearing, you're already on the right path.

308
00:20:36,000 --> 00:20:39,000
When do you need to hear?

309
00:20:39,000 --> 00:20:40,000
Yeah.

310
00:20:40,000 --> 00:20:50,000
Kind of what I thought, but I never really sure when I'm noting seeing, but actually seeing something if I'm doing around.

311
00:20:50,000 --> 00:20:56,000
But kind of go back and forth that makes sense.

312
00:20:56,000 --> 00:21:00,000
Speaking of death, how might you recommend one carry out the corpse meditations,

313
00:21:00,000 --> 00:21:08,000
considering that Toronto grounds and such are not readily available in the modern world?

314
00:21:08,000 --> 00:21:19,000
Well, you can get pictures or you can go to the morgue.

315
00:21:19,000 --> 00:21:22,000
Did they actually let people into the morgue, too?

316
00:21:22,000 --> 00:21:27,000
Did they do Instagram?

317
00:21:27,000 --> 00:21:29,000
You can do it at funerals.

318
00:21:29,000 --> 00:21:32,000
Oh, look at their dead body at the funeral.

319
00:21:32,000 --> 00:21:35,000
But they really kind of pretty them up at the funerals.

320
00:21:35,000 --> 00:21:37,000
They've got makeup on and...

321
00:21:37,000 --> 00:21:39,000
Yeah, but dead is dead.

322
00:21:39,000 --> 00:21:41,000
It's just seeing a dead body.

323
00:21:41,000 --> 00:21:43,000
It can shake people out.

324
00:21:43,000 --> 00:21:44,000
It can be useful.

325
00:21:44,000 --> 00:21:51,000
And if you meditate on it for a while, it can be helpful.

326
00:21:51,000 --> 00:21:53,000
I don't know.

327
00:21:53,000 --> 00:22:22,000
I mean, I don't recommend carrying out corpse meditation myself, so to find someone who does.

328
00:22:22,000 --> 00:22:28,000
So in a couple of days, we're moving, officially moving to the new location.

329
00:22:28,000 --> 00:22:36,000
But I guess I'll still be staying here until the 8th when we can actually get internet.

330
00:22:36,000 --> 00:22:39,000
The 8th, I think, is also the first day of classes.

331
00:22:39,000 --> 00:22:44,000
So I'll be back at school.

332
00:22:44,000 --> 00:22:49,000
I don't remember what the first day of classes, but the 8th.

333
00:22:49,000 --> 00:22:54,000
The 9th, I think, is this club's best that we're going to have a table.

334
00:22:54,000 --> 00:23:02,000
We're going to try to convert as many people as we can to Buddhism.

335
00:23:02,000 --> 00:23:06,000
Sign them up.

336
00:23:06,000 --> 00:23:13,000
We're going to have a clipboard to get people's emails, that kind of thing.

337
00:23:13,000 --> 00:23:17,000
And plan to have a meeting.

338
00:23:17,000 --> 00:23:21,000
All right, we had some ideas to hold monthly talks.

339
00:23:21,000 --> 00:23:27,000
And the question was whether they would be academic talks, or whether they would be spiritual, practical talks.

340
00:23:27,000 --> 00:23:30,000
And so we thought, maybe we'd do both.

341
00:23:30,000 --> 00:23:43,000
We can do one series of academic lectures on Buddhism, and one series of practical teachings on how to practice Buddhist teaching.

342
00:23:43,000 --> 00:23:53,000
It's hard because everyone's so busy that even people running it are far more focused on their studies, I think.

343
00:23:53,000 --> 00:23:58,000
So, that was it.

344
00:23:58,000 --> 00:24:09,000
I'm just going to hope to meet some people, and share the number.

345
00:24:09,000 --> 00:24:13,000
Are you taking classes on Buddhism?

346
00:24:13,000 --> 00:24:19,000
See, I wasn't, but I was just thinking now there's, I have no reason at all really to take German.

347
00:24:19,000 --> 00:24:30,000
Yes, it'd be nice to learn some German, but honestly, it's not really, not really, I don't know, justified.

348
00:24:30,000 --> 00:24:35,000
So, I was thinking I'll go back to taking, there was just something on Jesus.

349
00:24:35,000 --> 00:24:39,000
Am I justified? Is that more justified?

350
00:24:39,000 --> 00:24:43,000
I don't know. To get my degree, I think I need some various religious,

351
00:24:43,000 --> 00:24:49,000
I need a certain number of religion classes.

352
00:24:49,000 --> 00:25:04,000
Anyway, maybe we'll stop there.

353
00:25:04,000 --> 00:25:18,000
So, thank you all for checking in, having this nightly little chat.

354
00:25:18,000 --> 00:25:25,000
Maybe there'll be more to it when we're actually settled down and we'll have a live audience here as well.

355
00:25:25,000 --> 00:25:32,000
And we can talk with them live as well.

356
00:25:32,000 --> 00:25:41,000
That'll be nice.

357
00:25:41,000 --> 00:25:43,000
Okay, we're good night then.

358
00:25:43,000 --> 00:25:45,000
Thank you, Bhante. Good night.

359
00:25:45,000 --> 00:25:50,000
Thank you all tomorrow.

