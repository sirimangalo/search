1
00:00:00,000 --> 00:00:29,560
Good evening, broadcasting live, July 2nd, 2016, tonight's quote, tonight's quote is

2
00:00:29,560 --> 00:00:40,360
about the Brahmoviharas. The Brahmiharas, of course, are one of the more well-known Buddhist

3
00:00:40,360 --> 00:00:51,480
meditation practices besides insight meditation. Sometimes even more so than insight meditation.

4
00:00:51,480 --> 00:01:06,480
So sometimes equate or associate Buddhism more with compassion, love, then they do it

5
00:01:06,480 --> 00:01:13,280
inside. And with good reason, there are forms of Buddhism that focus more on Brahmoviharas,

6
00:01:13,280 --> 00:01:23,560
especially compassion. Compassion is considered to be key to my on the Buddhism because it's key

7
00:01:23,560 --> 00:01:30,840
to becoming a Buddha. Buddha doesn't just wish for their own enlightenment because of their strong

8
00:01:30,840 --> 00:01:46,200
passion, they wish for all beings to become in life. But in Teravada Buddhism, the four Brahmoviharas

9
00:01:46,200 --> 00:01:54,800
are what one might call a supportive meditation or auxiliary, if you want to use a technical

10
00:01:54,800 --> 00:02:04,520
culture. Meaning they're not the main, but they support one's main practice. They support

11
00:02:04,520 --> 00:02:11,560
the practice of insight meditation. It can also be used as a base to gain great concentration.

12
00:02:11,560 --> 00:02:20,240
So you could practice, may it have for its own for some time to focus the mind.

13
00:02:20,240 --> 00:02:29,200
Gatoranam would detail a good focus to focus on their own. But the best use they have

14
00:02:29,200 --> 00:02:37,280
and the most common use is as a supportive meditation. Meaning they keep your mind from getting

15
00:02:37,280 --> 00:02:50,720
off track. So if you get angry, if you're angry, if you meet, that helps you avoid being angry

16
00:02:50,720 --> 00:03:00,680
at someone. If you're cruel or irritable, then compassion helps you overcome that. If you're

17
00:03:00,680 --> 00:03:16,360
jealous, stingy, then joy, a sympathetic joy, appreciation, they call. And we'll deep down

18
00:03:16,360 --> 00:03:26,120
it. It helps you overcome that. And if you're partial or attached, if you care too much

19
00:03:26,120 --> 00:03:33,800
about others, that it makes you worried or anxious or upset when they're not as you expect

20
00:03:33,800 --> 00:03:45,560
them to be, then Upeka helps you with that. Equanimity. But these are part of a larger set of

21
00:03:45,560 --> 00:03:54,760
four meditations that are called Arakakamatana, the protective meditations. They protect your

22
00:03:54,760 --> 00:04:05,160
practice. And before our Buddha, Nusati, Asubha, Nusati, Mehta, Nusati, and Maranans,

23
00:04:07,160 --> 00:04:12,200
not maybe not in that order. Buddha, Nusati, mindfulness of the Buddha,

24
00:04:14,600 --> 00:04:20,440
thinking about the Buddha or reflecting on the Buddha, this is a good supportive meditation. These

25
00:04:20,440 --> 00:04:32,520
four are like when you're growing a small tree, you use wooden supports to keep it up. Otherwise,

26
00:04:32,520 --> 00:04:42,920
it gets blown over in the wind. So this support is not essential, but it's not the tree itself.

27
00:04:42,920 --> 00:04:51,800
But it keeps the tree from dying from being destroyed and falling in the same way these

28
00:04:51,800 --> 00:04:57,480
meditations help our main meditation. So Buddha, Nusati, thinking about the Buddha helps us

29
00:04:58,120 --> 00:05:09,800
cultivate confidence, reassurance, when we feel some kind of unsure about ourselves or ability,

30
00:05:09,800 --> 00:05:14,280
it gives us confidence. We think about the Buddha, what he had to do, what he went through as an

31
00:05:14,280 --> 00:05:20,280
example. Having role models, having an example, this is why it's great to be in the meditations,

32
00:05:20,280 --> 00:05:24,040
entry is the other people meditating, makes you want to meditate.

33
00:05:26,840 --> 00:05:31,720
Today we have two new meditators, coming a couple of days after I arrived,

34
00:05:31,720 --> 00:05:40,120
have Corey and Michelle. Michelle is an old meditator, she's been meditating with me since

35
00:05:40,920 --> 00:05:45,960
she was five. Many, many, many years ago her mother brought her and her sister

36
00:05:47,480 --> 00:05:55,320
to meditate here and can not with me with another teacher, but we meditated together.

37
00:05:55,320 --> 00:06:02,680
And Corey has been following me on the internet for just that for the first time.

38
00:06:06,360 --> 00:06:11,800
So yes, thinking about the Buddha is always one way of accomplishing this, when you have a

39
00:06:11,800 --> 00:06:18,840
Buddha image and you reflect on his great qualities that you're aspiring to to some extent.

40
00:06:18,840 --> 00:06:25,160
You sit there and say, Buddha, Buddha, thinking about the Buddha.

41
00:06:25,160 --> 00:06:40,840
The second one is assubhaym. Assubhaym means not beautiful. So assubhaym is reflecting on

42
00:06:40,840 --> 00:06:48,600
the not beautiful aspects of the body. And this one helps us in times when we have a great

43
00:06:48,600 --> 00:06:56,120
lust, a great lust for the body, for sexual desire that kind of thing. Assubhaym helps you overcome

44
00:06:56,120 --> 00:07:02,520
that. As you look at the body, not saying this is awful, this is disgusting, but you start to see

45
00:07:02,520 --> 00:07:09,960
how disgusting it is, how unpleasant it actually is, how there's nothing in here that is actually

46
00:07:09,960 --> 00:07:18,440
proper, the desire for the human body is a delusion, it's based on the illusion of beauty.

47
00:07:19,480 --> 00:07:23,880
There's nothing beautiful. So you take the body apart into pieces, as well as the hair beautiful,

48
00:07:23,880 --> 00:07:28,440
you have of course hair is beautiful. So you meditate on it, you say hair, hair,

49
00:07:30,520 --> 00:07:35,080
and you study about it and you think about it and you start to see that hair is actually quite

50
00:07:35,080 --> 00:07:43,880
disgusting. It's greasy and smelly and it's feeding off of the blood and the oil of the scalp.

51
00:07:43,880 --> 00:07:50,600
It's stuck in the scalp like rice. Those of us studying with sudumaganos of you studying it with me.

52
00:07:51,880 --> 00:07:56,600
Remember the description of the 32 parts of the body. It's quite vivid.

53
00:07:56,600 --> 00:08:04,280
When you go through the parts of the body, the hair, the skin, the nails, the teeth,

54
00:08:05,640 --> 00:08:13,560
and the hair, the body hair, the nails, the teeth, the skin, and all the inner parts, as well,

55
00:08:13,560 --> 00:08:29,320
the flesh and the blood, and so you just repeat to yourself, hair, hair, skin, skin. The third one,

56
00:08:29,320 --> 00:08:37,720
the third meditation is metas, of these four bambhyas, metas, single that is useful.

57
00:08:37,720 --> 00:08:46,120
Meant to help us overcome anger. We said we were upset about it, but I have said it

58
00:08:46,120 --> 00:08:53,880
someone, we have some kind of conflict with another person. It's a great source of stress and

59
00:08:53,880 --> 00:09:00,920
it's a hindrance in our practice, obviously. So of course being mindful is the best way to cure

60
00:09:00,920 --> 00:09:06,600
all of these, but when they're extreme and when we're just not able to shake them, it gives us a

61
00:09:06,600 --> 00:09:13,560
little boost. Practicing in this case, love gives us a boost to help us deal with it,

62
00:09:13,560 --> 00:09:18,520
excuse me. May that person be happy. Just as I want to be happy, may they be happy as well.

63
00:09:20,040 --> 00:09:22,200
May they be free from suffering, etc.

64
00:09:25,800 --> 00:09:32,280
And the fourth one is modern understood. So modernized for when you're lazy,

65
00:09:32,280 --> 00:09:38,200
this is why the Buddha said it's good to meditate on death daily every day, think about death.

66
00:09:39,000 --> 00:09:44,200
In fact, you should all the time be thinking about death. And in fact, we passed on as a way of

67
00:09:44,200 --> 00:09:50,040
thinking about death. So in an ultimate sense, we're born and die every moment. So we passed on

68
00:09:50,040 --> 00:10:00,120
as seeing us die, seeing them death, of being, realizing that there is no real stability or there's

69
00:10:00,120 --> 00:10:12,600
no constant entity. But so in general, we think about our deaths. We think about the physical death.

70
00:10:13,320 --> 00:10:16,760
This life is uncertain. I'm going to die one day. That is certain.

71
00:10:16,760 --> 00:10:28,360
I do want my team. We don't do long marinade. She we don't need a tiny marinade. Life is uncertain,

72
00:10:28,360 --> 00:10:36,600
death is certain. Death is the end of life for every being that comes to us all.

73
00:10:36,600 --> 00:10:47,080
Thinking about that helps give you a sense of urgency, because you realize that if you

74
00:10:47,080 --> 00:10:56,680
die unprepared, it has a great impact on what comes next. Your clarity of mind, your purity of

75
00:10:56,680 --> 00:11:04,600
mind, death is the end of everything. All these things that I depend upon that I care for.

76
00:11:04,600 --> 00:11:07,400
I can't take them with me when I die.

77
00:11:10,920 --> 00:11:14,920
So I mean, that probably only works for people to believe in an afterlife, but even still,

78
00:11:15,880 --> 00:11:20,360
if you think this is it, you know, the mother might as well make the best of it,

79
00:11:20,360 --> 00:11:28,440
the most of it. You think when we die, it's death and still best to become the best person you can

80
00:11:28,440 --> 00:11:35,640
to the best you can if this is the only chance you've got. It just seems sensible.

81
00:11:39,560 --> 00:11:42,520
Anyway, not too much to talk about there. You can read through the

82
00:11:43,560 --> 00:11:49,800
rebunga in this case. The rebunga, just the part of the abbey dhamma talks about

83
00:11:49,800 --> 00:11:55,160
before ramboli has. It's also in the Risudhi manga. There's really good section on the

84
00:11:55,160 --> 00:11:59,800
forebramoli eyes based on the rebunga based on the abbey dhamma.

85
00:12:02,840 --> 00:12:09,240
So questions, Fernando asks, how is the meaning with my teacher? It was brief. He's very busy,

86
00:12:10,920 --> 00:12:16,680
but I caught him as we always do, and took some pictures, though I haven't gotten any of the

87
00:12:16,680 --> 00:12:25,080
pictures yet, and gave him a whole bunch of robes again, 30-some, 30-some robe sets for him to

88
00:12:25,080 --> 00:12:33,880
give away to others. He expressed his appreciation more for the fact that I was going to

89
00:12:33,880 --> 00:12:42,120
Sri Lanka than anything curiously. He likes the idea of helping out Sri Lankan Buddhism.

90
00:12:42,120 --> 00:12:47,400
It was kind of just something that piqued his interest and has before because he's been

91
00:12:47,400 --> 00:12:52,920
Sri Lankan, and because of the sense of gratitude, this will be the right thing to say,

92
00:12:52,920 --> 00:13:05,400
or just sense of appreciation for Sri Lanka, sort of a responsibility to care for Buddhism

93
00:13:05,400 --> 00:13:08,760
there as well. So I was happy to hear that I was going to teach there.

94
00:13:12,920 --> 00:13:18,200
Didn't say too much. He was expressed appreciation for the new meditation center,

95
00:13:18,200 --> 00:13:23,240
but he already knew about it. So it wasn't all that much talk.

96
00:13:28,520 --> 00:13:36,760
Honestly, going to Asia was not probably all it was meant to be or cut out to be.

97
00:13:36,760 --> 00:13:42,840
They said before I wasn't totally sold on the idea of going in the first place.

98
00:13:42,840 --> 00:13:52,280
So maybe I downplayed my expectation, but it was just another trip.

99
00:13:53,960 --> 00:13:59,800
Teaching in Sri Lanka was nice. We had one really good session and a meditation center there.

100
00:14:01,000 --> 00:14:04,600
A whole bunch of people came out and they were surprised because I'm not there

101
00:14:05,240 --> 00:14:10,200
and because they don't know who I am really, they're surprised at the things I say.

102
00:14:10,200 --> 00:14:19,240
So it was a shock at the defense of this meditation technique because a lot of them have turned

103
00:14:19,240 --> 00:14:27,880
against it. It's become of sort of a rebel meditation, I guess. We've been believing that

104
00:14:27,880 --> 00:14:34,200
Anna Banas, that the mindfulness of the breath of the nose, is the most orthodox meditation

105
00:14:34,200 --> 00:14:42,760
practice and so they concentrate on that. So having to describe this meditation wherever I went,

106
00:14:42,760 --> 00:14:47,800
they said, do you know who I am? Are you sure you want me to come in, Todd? Because of course,

107
00:14:47,800 --> 00:14:56,920
last time I went, I had these problems. I would go places and they would be surprised at what I

108
00:14:56,920 --> 00:15:03,480
was saying and not in a happy way. There was one place to translate or actually refuse to translate

109
00:15:03,480 --> 00:15:09,320
because he didn't agree with what I said. That's pretty often.

110
00:15:14,200 --> 00:15:19,560
Thank you all for meditating. It's good to see green here. Green means that you guys have actually

111
00:15:19,560 --> 00:15:25,640
been meditating. I mean, yes, logging your meditation is not necessary. It's not like it actually

112
00:15:25,640 --> 00:15:32,040
means anything and no, you don't have to do it because it makes me feel good. But if you don't,

113
00:15:32,040 --> 00:15:41,080
then we start to get, I mean, as I said before, it gives me an excuse to weed out

114
00:15:41,080 --> 00:15:48,520
sort of speculative questions because I can say, you're an orange. So newcomers show up,

115
00:15:48,520 --> 00:15:53,240
they don't meditate and they start asking crazy questions. They can sort of dismiss them.

116
00:15:53,240 --> 00:16:04,360
And it would focus our questions because we were meditating. I don't mind if you were meditating

117
00:16:04,360 --> 00:16:11,000
earlier in the day and you just come and click on it later, click on it now.

118
00:16:13,960 --> 00:16:19,800
Suppose it's not ideal, but ideal as we do some meditation before. I mean, I guess the biggest

119
00:16:19,800 --> 00:16:25,400
thing is here we're coming together to talk about the dumber. So before we do that, we should

120
00:16:25,400 --> 00:16:29,320
meditate. So if you're saying, well, I'm not meditating right before, but I'm meditating earlier.

121
00:16:30,920 --> 00:16:37,080
Part of the point is to meditate just before we talk so that our minds are in the right place.

122
00:16:39,400 --> 00:16:45,400
It's going to be super reasonable. But as far as logging it, I mean, it allows me to make sure that

123
00:16:45,400 --> 00:16:54,040
no, I mean, the big point of this site in as a site is to provide encouragement. If you don't

124
00:16:54,040 --> 00:16:58,440
find that this is encouraging you to meditate, the fact that you've got to actually log it and

125
00:16:58,440 --> 00:17:04,920
commit to it and no news it. That means that the whole point here was to encourage people to meditate.

126
00:17:06,040 --> 00:17:11,480
I think some people do find that the site helps them. I assume if not, well,

127
00:17:11,480 --> 00:17:25,000
I'm going to just do away with it. So no other questions. For those of you joining us on YouTube

128
00:17:25,000 --> 00:17:31,960
and last night as well, the questions and stuff is going on at our own website at meditation.searing

129
00:17:31,960 --> 00:17:38,040
on going on.org. And usually someone's kind enough to put that in the comments section of

130
00:17:38,040 --> 00:17:46,440
YouTube video. We broadcast hopefully every night, usually every night.

131
00:17:49,560 --> 00:17:54,280
But I guess there's no questions tonight. I know I'm just recently back.

132
00:17:54,280 --> 00:18:03,080
All right, then we have a good night, everyone.

133
00:18:03,080 --> 00:18:28,360
See you all tomorrow.

