1
00:00:00,000 --> 00:00:07,000
Okay, welcome back to Ask a Monk. Today's question comes from Kusala Virou.

2
00:00:07,000 --> 00:00:16,000
Bandai is a layperson. How can we deal with excessive sexual desire without losing it for our wife or husband?

3
00:00:16,000 --> 00:00:23,000
No matter how much we love our partner, we still crave other people sexually on a physical level.

4
00:00:23,000 --> 00:00:26,000
Somebody would not question for a monk.

5
00:00:26,000 --> 00:00:37,000
But I was thinking about it and there's definitely an appropriate question from a meditative point of view.

6
00:00:37,000 --> 00:00:46,000
Or the answer, at least helps us to understand something about the nature of craving.

7
00:00:46,000 --> 00:00:54,000
This is really the problem with craving. One of the reasons why it became apparent to me that

8
00:00:54,000 --> 00:01:01,000
there's something wrong with the way we approach the world. There's something wrong with lust, with craving.

9
00:01:01,000 --> 00:01:07,000
Because there's nothing in the mind that says, only this.

10
00:01:07,000 --> 00:01:20,000
I'll only need this. And in fact, it's just the opposite that we need more intense stimuli

11
00:01:20,000 --> 00:01:28,000
to bring the same amount of gratification. And this is based on the way the brain works.

12
00:01:28,000 --> 00:01:35,000
It's just a part of reality. And that's from a Buddhist point of view to do with karma.

13
00:01:35,000 --> 00:01:41,000
When you do anything, when you commit an act, it's not a stationary thing.

14
00:01:41,000 --> 00:01:47,000
It's not static where you can keep performing the same act again and again and again and again at the same results.

15
00:01:47,000 --> 00:01:54,000
Every act that we perform when there's a volitional intention involved leads you in a certain direction.

16
00:01:54,000 --> 00:02:05,000
So craving something, chasing after it, and working to attain it, and finally attaining it, or obtaining it,

17
00:02:05,000 --> 00:02:12,000
leads you in that direction, the direction to want more and to develop the habit for craving.

18
00:02:12,000 --> 00:02:20,000
There's worse and worse. And there's no way to say that here you're going to draw limits and you're not going to want anything outside of this.

19
00:02:20,000 --> 00:02:28,000
This is the point that you'll find yourself craving more and more and more and never getting enough.

20
00:02:28,000 --> 00:02:37,000
So what do you do? From a Buddhist point of view, you obviously try to give up all craving.

21
00:02:37,000 --> 00:02:49,000
All desire. The problem of course comes that this is unacceptable for most people, for most of us there is still a craving in the mind.

22
00:02:49,000 --> 00:03:00,000
The way I would say it's possible from a late point of view, and obviously this is not the point of view that I have as a monk.

23
00:03:00,000 --> 00:03:13,000
But it's the same in the sense that you accept that you have certain cravings, and they're kind of a danger to you.

24
00:03:13,000 --> 00:03:19,000
There's something that has the potential to take your mind out of its peaceful, contented state.

25
00:03:19,000 --> 00:03:23,000
It's a fever of sorts. It's a sickness.

26
00:03:23,000 --> 00:03:30,000
Maybe you want to see there's a sickness. The Buddha said it's a fever. It intoxicates the mind.

27
00:03:30,000 --> 00:03:48,000
So you can see this outlet that you have of sexual gratification with your wife, our husband, as being kind of an outlet.

28
00:03:48,000 --> 00:03:55,000
It's a way of giving in to the craving.

29
00:03:55,000 --> 00:04:09,000
So as a late Buddhist, you're still trying to keep your mind free from this fever, this essential misunderstanding of the phenomena as being pleasant and desirable.

30
00:04:09,000 --> 00:04:18,000
And as getting this object of your desire is bringing true peace and happiness.

31
00:04:18,000 --> 00:04:38,000
But when you give in, you have this outlet. It's not going to send you to a bad place or take you down the wrong path if you give in and have sexual intercourse with your wife or girlfriend or partner.

32
00:04:38,000 --> 00:04:42,000
But another thing you can look at.

33
00:04:42,000 --> 00:04:53,000
So this is one way to approach this point in terms of not saying, okay, it's fine to have marital sex, so we're just going to go wild.

34
00:04:53,000 --> 00:05:01,000
That obviously doesn't work because going wild doesn't have boundaries.

35
00:05:01,000 --> 00:05:22,000
But when you're trying your best to be as clear-minded as possible and then your mind becomes clouded by lust and desire, you give in and you don't have to feel bad about it because you're married because you're not hurting anyone and you understand that you're human, that you give into these desires.

36
00:05:22,000 --> 00:05:33,000
Then you go back and trying to stay as clear-minded as you can. If you have that point of view, then I think it's a way of answering your question.

37
00:05:33,000 --> 00:05:45,000
And from my point of view, it probably will work a lot better than trying to just make a wall and say within this wall anything goes, outside this wall, nothing goes.

38
00:05:45,000 --> 00:05:52,000
You try to limit yourself and giving in as it was appropriate. I think that's easy to understand.

39
00:05:52,000 --> 00:06:00,000
But the second part is to understand why we might say something like lust is wrong with you people are offended by this.

40
00:06:00,000 --> 00:06:08,000
They think of the love that they have as a husband and wife and for their partners as being a positive thing.

41
00:06:08,000 --> 00:06:35,000
And so I've often given talks about this as well that there's a difference between love and attachment and the love that you feel for your partner is actually just a smaller part of the love that we feel or should feel for all beings and the love that we have for people for who they are and wishing to help them wishing to give peace happiness and freedom from suffering to them.

42
00:06:35,000 --> 00:06:45,000
But the attachment is something totally different. Attachment is wanting something from them wishing for them to bring you gratification and pleasure.

43
00:06:45,000 --> 00:06:57,000
And you find that the more Buddhist, the more advanced you become in Buddhist practice in the meditation practice, the more you feel this love and the less you feel the attachment.

44
00:06:57,000 --> 00:07:09,000
And so if a husband or a wife becomes enlightened, the stories that they have what happens is they say, look, I still love you and I'll treat you as a brother or as a sister.

45
00:07:09,000 --> 00:07:19,000
But if you want to get married again, you're welcome but I have no more lust or attachment towards you. But there's still a sense of love there.

46
00:07:19,000 --> 00:07:34,000
But it's just a much broader sort of love and understanding that beings are all precious and sacred and worthy of our love and our appreciation and so on.

47
00:07:34,000 --> 00:07:56,000
And so allows you to be free from this fever, this addiction, the need for gratification and more and more intense gratification which eventually can lead to infidelity and so on.

48
00:07:56,000 --> 00:08:07,000
So those are two ways of looking at the problem that might give you a hint as to where to go next. The third option, of course, is to just become a monk and be done within.

49
00:08:07,000 --> 00:08:19,000
But I think you're aware of that already. And if your content is a lay person, power to you, not everyone has to become a monk. Clearly you can become enlightened as a lay person.

50
00:08:19,000 --> 00:08:27,000
You just have to try to avoid the temptation as best you can and try to keep your mind as clear as possible.

51
00:08:27,000 --> 00:08:31,000
Don't feel guilty about these things as well. This is one last point.

52
00:08:31,000 --> 00:08:47,000
I'm sorry, guilt is not a helpful emotion. It's an unwholesome emotion from a Buddhist point of view. So it's something that is negative. It's causing more and more repression in your mind. It's actually making the problem worse.

53
00:08:47,000 --> 00:09:02,000
So when you feel guilty about these thoughts, it's kind of like denying them, not giving them the credit as accepting them as real that they really are there. It's denying their existence.

54
00:09:02,000 --> 00:09:16,000
It's not accepting their existence. And when you can come to accept and understand that, yes, these have arisen and accept where they're coming from, why they're there, then you can start to work on overcoming them.

55
00:09:16,000 --> 00:09:31,000
So realizing that this is just a cause of your high state of degree of lust that you have in the mind, then you'll be able to work on it.

56
00:09:31,000 --> 00:09:40,000
And from a point, from that point of view, from just examining and working through and straightening out your mind in regards to this lust.

57
00:09:40,000 --> 00:09:57,000
And to the point where your mind is clearer, you're freer from this addiction and you're able to control yourself so that even when the time comes, you have sexual intercourse with your partner and nothing else.

58
00:09:57,000 --> 00:10:11,000
So there's a mong take on it and that's about as far as I go with these things. So thanks for the question.

