During meditation, if one feels anger building up, breathing increases, frowning, mood
changes and pain, should one try to actively discard them with positive thoughts,
make the calming the breath, etc., or just watch it and see that also passes away.
Do you want to start, do you want to finish?
I think this could depend.
If it's just happening in meditation, then I say stick with it and watch it.
If a person is naturally angry person, maybe has an explosive temper, something just
a normal eye, then I think practicing meta and such, along with their normal practice could
be good, but if it's just something arising while you're sitting in meditation, then I just
say, sit with it and watch it.
It's a very good and succinct answer, I don't have much to add to it really, but I would
say one thing is that on top of that, if it continues to come up in meditation, you can
use loving kindness after you're supposed to practice, we pass in a meditation and watch
the anger saying to yourself, angry, angry, being clear about it is just being something
that is arisen and will cease and so on, not just being aware of the anger, but also being
aware of the pain and the other things involved, because anger is only a very small part
of what we ordinary consider to be angry.
The breathing increases, that's not anger, that's breathing increasing, the frowning, that's
not anger, that's physical, the pain in the head, the pain in the body, the tension in
the body, that's also none of its anger, it's a physical and it's body and it's feelings.
But when you see that this sort of thing is building up, you might want to incline
towards augmenting your practice with loving kindness and it's for this reason that we
have these meditations as the arakakamatana, the meditations which protect you.
So we will practice loving kindness to all beings, to beings that we have problems with
when you have a difficulty with someone that at the end of your meditation, you'll reaffirm,
you might not even call it loving kindness, but you could say it's a reaffirmation of your
intentions, you're setting your mind in the right way.
Because you might be practicing, suppose you're practicing Vipasana meditation and when
you think of someone, you get really angry and you say angry, angry and so your mind
is bent out of shape.
When your mind is bent out of shape, you're not really being mindful, you're saying angry
angry but you're really angry.
When you set your mind in the right way with loving kindness or what appears to be loving
kindness, it's also a determination, a detan, saying yourself, no, my intention is that
this person should be happy, not that they should suffer.
My intention is that they should gain, not that they should lose and so on.
When you send these thoughts, loving thoughts, you're reaffirming your intentions so that
when you go back and practice Vipasana, you're clear about this and when the anger comes
up, you're clear that that's not what you want and that really makes it easier.
It's the difference between view and defilement.
The view locks the defilement in place.
When you believe that your anger is justified, this person hurt me and they should suffer,
that proper for them to suffer because then it locks the anger.
You can't deal with it.
You can say angry, angry, angry as much as you want, it'll never go away.
This is an example of how loving kindness can be useful not only to suppress and to change
the mind states but also to change one's view because when you say to yourself, maybe
be happy, you're inclined to a view that they should be happy, that it's proper for them
to be happy.
You're saying to yourself, this is what is correct.
This is what I truly believe in as an example.
