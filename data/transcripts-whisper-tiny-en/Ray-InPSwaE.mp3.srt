1
00:00:00,000 --> 00:00:05,000
Good evening and welcome back to our study of the Damapanda.

2
00:00:05,000 --> 00:00:11,000
Today we continue on with 1st 147, which reads as follows.

3
00:00:35,000 --> 00:00:50,000
This painted image, this painted image, the body.

4
00:00:50,000 --> 00:00:57,000
Arukayang, that is a massive source, massive, of festering

5
00:00:57,000 --> 00:01:07,000
source, or wounds, or sufferings.

6
00:01:07,000 --> 00:01:18,000
But compound is where the common term may be compounded as better.

7
00:01:18,000 --> 00:01:37,000
Aturang, diseased, pajusankapan, pajusankapan, which means many thoughts with many intentions or purposes.

8
00:01:37,000 --> 00:01:44,000
It can be used for many purposes as maybe with the ideas here.

9
00:01:44,000 --> 00:02:06,000
There is nothing that stays permanent, nothing that stays stable.

10
00:02:06,000 --> 00:02:15,000
A rather harsh announcement of the body.

11
00:02:15,000 --> 00:02:21,000
The story told of Serima, the death of Serima.

12
00:02:21,000 --> 00:02:25,000
Serima has quite a colorful story.

13
00:02:25,000 --> 00:02:37,000
She was a cortisan, which I guess means a prostitute, or a similar to a geisha.

14
00:02:37,000 --> 00:02:41,000
People would pay money to spend the night with her.

15
00:02:41,000 --> 00:02:45,000
Or they'd hire her for whatever purpose.

16
00:02:45,000 --> 00:02:49,000
I think that's where the bahusankapan comes from.

17
00:02:49,000 --> 00:02:54,000
A beautiful body can be used for many purposes.

18
00:02:54,000 --> 00:03:02,000
A healthy body.

19
00:03:02,000 --> 00:03:06,000
She was bought by Utara.

20
00:03:06,000 --> 00:03:08,000
Utara was a saute upon, I think.

21
00:03:08,000 --> 00:03:11,000
She was a disciple of the Buddha.

22
00:03:11,000 --> 00:03:16,000
She really wanted to spend all of her time taking care of the Buddha and the monks and listening

23
00:03:16,000 --> 00:03:20,000
to the Buddha's teaching.

24
00:03:20,000 --> 00:03:32,000
The problem was her husband was taking a lot of her time, requiring her to please him.

25
00:03:32,000 --> 00:03:42,000
Because she wasn't interested in sleeping with her or engaging in sensuality with her husband,

26
00:03:42,000 --> 00:03:47,000
she hired this cortisan for her husband.

27
00:03:47,000 --> 00:03:56,000
She was so beautiful that her husband didn't even complain.

28
00:03:56,000 --> 00:04:03,000
She would go about Utara the other one, the wife would go about making all these preparations

29
00:04:03,000 --> 00:04:09,000
for the Buddha and spending all of her time making food for the monks and so on.

30
00:04:09,000 --> 00:04:18,000
Her husband spent all of his time with his prostitute in this cortisan.

31
00:04:18,000 --> 00:04:29,000
Until eventually the cortisan came to feel like she had some sort of ego in regards to her situation,

32
00:04:29,000 --> 00:04:35,000
like the husband preferred her to his wife.

33
00:04:35,000 --> 00:04:39,000
She started to cultivate this sense of jealousy towards the wife thinking,

34
00:04:39,000 --> 00:04:44,000
I'm the one who takes care of him and so on.

35
00:04:44,000 --> 00:04:53,000
So one day the husband was watching Utara watching her do all these,

36
00:04:53,000 --> 00:04:58,000
prepare all this food for the monks and he smiled and thought to himself,

37
00:04:58,000 --> 00:05:02,000
what a ridiculous woman she spends all her time when she couldn't be enjoying life.

38
00:05:02,000 --> 00:05:10,000
She worked really hard to give food to these beggars and so he smiled,

39
00:05:10,000 --> 00:05:13,000
just smiled at how silly it was.

40
00:05:13,000 --> 00:05:17,000
And Siri Masayam and thought he was smiling and his wife said,

41
00:05:17,000 --> 00:05:24,000
I do all this for him and still he's attracted to her while I'll take her out of the picture

42
00:05:24,000 --> 00:05:31,000
and she went over into the kitchen where Utara was boiling or heating up some butter

43
00:05:31,000 --> 00:05:37,000
on the stove and she picked up this cauldron of hot butter and threw it at Siri.

44
00:05:37,000 --> 00:05:42,000
And poured this scalding hot butter onto Utara.

45
00:05:42,000 --> 00:05:47,000
Utara was of course a practitioner of Buddhist teaching and she saw,

46
00:05:47,000 --> 00:05:55,000
she turned and saw this butter coming at her and immediately she entered into a genre of loving kindness.

47
00:05:55,000 --> 00:06:00,000
She thought to her, she immediately recognized this violence against her

48
00:06:00,000 --> 00:06:08,000
thought to herself wished her happiness and well-being for the, for Siri Ma.

49
00:06:08,000 --> 00:06:15,000
And as a result of the incredible power of her mind, they say the butter became completely cool

50
00:06:15,000 --> 00:06:24,000
and it was as though a cool water was washing over her cool butter I guess.

51
00:06:24,000 --> 00:06:31,000
And so she, if she wasn't heard at all and the servants who saw this happening

52
00:06:31,000 --> 00:06:36,000
who were helping in the kitchen immediately jumped on Siri Ma and started beating her.

53
00:06:36,000 --> 00:06:41,000
Utara pulled them off and said to Siri Ma and picked Siri Ma off the floor

54
00:06:41,000 --> 00:06:46,000
and started tending to her what wounds and asking her if she was okay.

55
00:06:46,000 --> 00:06:53,000
And Siri Ma became completely converted and ashamed of which she had done.

56
00:06:53,000 --> 00:07:01,000
And asked for goodness and eventually I think Utara the center of to ask for a goodness of the Buddha.

57
00:07:01,000 --> 00:07:05,000
Eventually she became Buddhist. She became a soda pun.

58
00:07:05,000 --> 00:07:09,000
The story isn't actually about her, it's actually about a monk.

59
00:07:09,000 --> 00:07:15,000
So she became Buddhist and spent a lot of her time listening to the Buddhist teachings,

60
00:07:15,000 --> 00:07:20,000
practicing meditation, became a soda pun.

61
00:07:20,000 --> 00:07:28,000
It would often provide food for the monks and listen to their teachings as well.

62
00:07:28,000 --> 00:07:34,000
And so the word got out and this one monk heard about Siri Ma.

63
00:07:34,000 --> 00:07:42,000
And he went and asked these monks, what's the deal with this Siri Ma?

64
00:07:42,000 --> 00:07:45,000
He'd heard how incredible she was.

65
00:07:45,000 --> 00:07:48,000
And this monk said, oh wow, the food, yes, the food she gives is wonderful.

66
00:07:48,000 --> 00:07:52,000
She's very good and she gives the best and the most highest quality of food.

67
00:07:52,000 --> 00:07:57,000
But apart from that she's also quite beautiful and pretty to look at.

68
00:07:57,000 --> 00:08:07,000
So I need to hear monks having this conversation but that's what the text says.

69
00:08:07,000 --> 00:08:17,000
And immediately he became attracted and he thought he should go and see her.

70
00:08:17,000 --> 00:08:30,000
And so he found his way to get into the queue to go and receive arms from this Siri Ma in her great arms house.

71
00:08:30,000 --> 00:08:36,000
But the night before she got very sick or the day before she got quite sick.

72
00:08:36,000 --> 00:08:43,000
And so she took off all of her makeup and jewels and she laid down and she had the servants prepare the food.

73
00:08:43,000 --> 00:08:54,000
And then as the monks were sitting to receive the food she came out, they carried her out on a pollen gun or whatever it is those things that you carry a stretcher I guess.

74
00:08:54,000 --> 00:08:59,000
And she was able to lie down, she was able to give food.

75
00:08:59,000 --> 00:09:06,000
And this monks are and he thought to himself, she's this beautiful when she's sick.

76
00:09:06,000 --> 00:09:09,000
Imagine how beautiful she would be when she's perfectly healthy.

77
00:09:09,000 --> 00:09:23,000
And he became completely enamored and the text says this lust that this lust that he accumulated during many millions of years arose within him.

78
00:09:23,000 --> 00:09:33,000
He became indifferent, was unable to take food and he went back to his kutti and shut the door and didn't listen to anyone, didn't go out.

79
00:09:33,000 --> 00:09:46,000
And just laid, you know, sick on his bed, sick with lust, sick with attraction to this woman.

80
00:09:46,000 --> 00:09:56,000
Now as nature goes, as things go, the nature took its course and Siri Ma passed away that day I believe.

81
00:09:56,000 --> 00:10:10,000
And we know it's to this monk. And because she was such a great lay disciple, she was actually the sister of Zebokai, it looks like the Buddha's physician.

82
00:10:10,000 --> 00:10:22,000
And so the king sent word to the Buddha that Siri Ma had died and the Buddha sent word back that Siri said, please tell the king not to bury Siri Ma.

83
00:10:22,000 --> 00:10:29,000
Not to nor cremate, I guess they wouldn't bury it, burned yesterday, did not cremate her.

84
00:10:29,000 --> 00:10:38,000
Instead put Siri Ma's body out in the channel ground and set up a guard so that the crow is in the jackals so that the animals don't eat her.

85
00:10:38,000 --> 00:10:42,000
And just leave the body there.

86
00:10:42,000 --> 00:10:47,000
And so the king had this done and he had a guard set up.

87
00:10:47,000 --> 00:10:57,000
And after two or three days, after four days, three days passed, and on the fourth day the body got all bloated of course.

88
00:10:57,000 --> 00:11:14,000
And from the nine openings of her body, they're oozed forth maggots so they couldn't give the flies away and the maggots began to pour out, pour out of the nine openings of the body.

89
00:11:14,000 --> 00:11:18,000
And then the king caused the proclamation, the king understood what the Buddha was doing here.

90
00:11:18,000 --> 00:11:24,000
He didn't know about this special case at this moment, but he understood what the Buddha was doing in general.

91
00:11:24,000 --> 00:11:28,000
And so he called for all everyone to come and see Siri Ma.

92
00:11:28,000 --> 00:11:36,000
He said, everyone in the city besides those who are busy working on some official business,

93
00:11:36,000 --> 00:11:43,000
he called an order that everyone should come and see if they don't, except for the watchmen, right?

94
00:11:43,000 --> 00:11:51,000
The police, everybody should go if not they would be fined eight pieces of gold.

95
00:11:51,000 --> 00:11:59,000
And then he sent the message to the Buddha that the monk should come and see Siri Ma as well.

96
00:11:59,000 --> 00:12:02,000
And that's how the proclamation went out.

97
00:12:02,000 --> 00:12:07,000
And it came to the monks and the monks started talking about how they were going to see Siri Ma.

98
00:12:07,000 --> 00:12:12,000
And this monk heard and immediately thought to himself, I'll get to see Siri Ma.

99
00:12:12,000 --> 00:12:19,000
I'll get to see this beautiful woman again and he just hadn't had to go and see her.

100
00:12:19,000 --> 00:12:24,000
Or they even came and asked him, they said, are you going to see Siri Ma?

101
00:12:24,000 --> 00:12:26,000
So of course I'll go.

102
00:12:26,000 --> 00:12:34,000
And they set up with the monks.

103
00:12:34,000 --> 00:12:38,000
Of course when they got there the situation was not as he expected.

104
00:12:38,000 --> 00:12:45,000
And the king and the Buddha came up to Siri Ma and saw her lying there and he said,

105
00:12:45,000 --> 00:12:48,000
they said, king who is this woman?

106
00:12:48,000 --> 00:12:50,000
This is Siri Ma.

107
00:12:50,000 --> 00:12:51,000
She was a sister.

108
00:12:51,000 --> 00:12:52,000
This is Siri Ma.

109
00:12:52,000 --> 00:12:53,000
Yes, this is Siri Ma.

110
00:12:53,000 --> 00:12:57,000
He said, well then, send a drum.

111
00:12:57,000 --> 00:12:59,000
I was a drum.

112
00:12:59,000 --> 00:13:01,000
This is how they had those proclamation.

113
00:13:01,000 --> 00:13:05,000
This is how they spread word in those times.

114
00:13:05,000 --> 00:13:13,000
And make a proclamation that we will give Siri Ma to anyone for 500 pieces,

115
00:13:13,000 --> 00:13:17,000
for a thousand pieces of gold for one night.

116
00:13:17,000 --> 00:13:22,000
Whoever wants her can have her for a thousand pieces of gold.

117
00:13:22,000 --> 00:13:25,000
Not a man said him or homelix thing was translation.

118
00:13:25,000 --> 00:13:28,000
I don't know what the poly is.

119
00:13:28,000 --> 00:13:34,000
Nobody said anything. Nobody even cleared their throat for fear of being

120
00:13:34,000 --> 00:13:39,000
considered interested in taking up the offer.

121
00:13:39,000 --> 00:13:43,000
Then he said, well then ask for 500.

122
00:13:43,000 --> 00:13:47,000
And then 500 nobody said anything.

123
00:13:47,000 --> 00:13:52,000
250, 200, 150, 25, 10, 5.

124
00:13:52,000 --> 00:13:58,000
And they reduced it to a penny, a half penny, a quarter of a penny, an eighth of a penny.

125
00:13:58,000 --> 00:14:02,000
And finally, they would have said, tell her they can have it for nothing.

126
00:14:02,000 --> 00:14:05,000
And no one said anything.

127
00:14:05,000 --> 00:14:11,000
The king said, but no one will take her even as a gift.

128
00:14:11,000 --> 00:14:17,000
And the teacher said, monks, do you see the value of a woman in the eyes of the multitude?

129
00:14:17,000 --> 00:14:23,000
The value of the female body, which is so well, so sexualized, right?

130
00:14:23,000 --> 00:14:29,000
And so highly esteemed for its sexual attraction.

131
00:14:29,000 --> 00:14:38,000
Such was her beauty who is now perished and gone.

132
00:14:38,000 --> 00:14:49,000
A warupang namarupang kayawaya patang, this root by it, subject to fading away,

133
00:14:49,000 --> 00:14:54,000
subject to loss.

134
00:14:54,000 --> 00:14:57,000
Passatabhikhawaya adwurangatabhava.

135
00:14:57,000 --> 00:15:05,000
Come and see the corruption, the disease, the affliction of being,

136
00:15:05,000 --> 00:15:11,000
of the self, of the body.

137
00:15:11,000 --> 00:15:18,000
And then he told this verse, passatjitakatang vimang natsaw.

138
00:15:18,000 --> 00:15:23,000
So for us, obviously, we're not practicing modern understood to you,

139
00:15:23,000 --> 00:15:32,000
or kayagatasati, or the cemetery contemplations.

140
00:15:32,000 --> 00:15:37,000
But it's still a good reminder to us, the idea of death.

141
00:15:37,000 --> 00:15:44,000
It's a good example of the misunderstanding that comes this monk.

142
00:15:44,000 --> 00:15:46,000
There's many things in here.

143
00:15:46,000 --> 00:15:51,000
There's the monks lust and his desire, which interestingly, it mentions,

144
00:15:51,000 --> 00:15:58,000
which is a fairly rare sort of thing to say that it's something that he'd had for countless lifetimes.

145
00:15:58,000 --> 00:15:59,000
But this is the truth.

146
00:15:59,000 --> 00:16:04,000
You know, a lot of our attachments, a lot of our habits are habitual not in this life,

147
00:16:04,000 --> 00:16:11,000
but over lifetimes, so they can be quite strong.

148
00:16:11,000 --> 00:16:21,000
And so if we often wonder, where did this come from, why am I so attached or attracted to this or attracted to that?

149
00:16:21,000 --> 00:16:28,000
In fact, it's just a matter of cultivation, and we've cultivated for so long.

150
00:16:28,000 --> 00:16:37,000
But the big thing here is this misunderstanding, this misconception about things,

151
00:16:37,000 --> 00:16:42,000
about, you know, why should the healthy body be any more attractive than the bloated body?

152
00:16:42,000 --> 00:16:44,000
It's only when we see the body bloated.

153
00:16:44,000 --> 00:16:48,000
There's nothing actually different from the bloated body with maggots coming out of it, right?

154
00:16:48,000 --> 00:16:50,000
Why isn't that beautiful?

155
00:16:50,000 --> 00:16:54,000
Why aren't we attracted to the bloated body with maggots coming out of nine holes?

156
00:16:54,000 --> 00:16:59,000
Why aren't we attracted to the smell of a bloated body?

157
00:16:59,000 --> 00:17:02,000
I remember once in Thailand there was a...

158
00:17:02,000 --> 00:17:06,000
I was sitting in my good deal and there was a terrible smell.

159
00:17:06,000 --> 00:17:10,000
Just a smell like nothing you'd ever smelled before.

160
00:17:10,000 --> 00:17:15,000
And then it was meditating through it, and first looked around the room,

161
00:17:15,000 --> 00:17:17,000
tried to find wood, and died.

162
00:17:17,000 --> 00:17:22,000
And then suddenly maggots started falling through the cracks in the ceiling.

163
00:17:22,000 --> 00:17:26,000
I guess some animal may be a big rat or something.

164
00:17:26,000 --> 00:17:30,000
I don't know, died in the ceiling.

165
00:17:30,000 --> 00:17:36,000
All I know of it was that maggots started falling through the ceiling.

166
00:17:36,000 --> 00:17:38,000
You've never been around a dead body.

167
00:17:38,000 --> 00:17:45,000
It doesn't have the most attractive smell, but it's simply our conditioning.

168
00:17:45,000 --> 00:17:50,000
I mean, you can argue that there's something the body is conditioned to react violently

169
00:17:50,000 --> 00:17:54,000
to certain smells, but certainly not certain sites.

170
00:17:54,000 --> 00:18:00,000
Maybe it may be in the brain somehow, but we have senses of symmetry and so on.

171
00:18:00,000 --> 00:18:03,000
And when the body is bloated, it loses that.

172
00:18:03,000 --> 00:18:07,000
But that's very much conditioning, whether it's physical or mental.

173
00:18:07,000 --> 00:18:13,000
It all comes from the mind and our buildup of this life.

174
00:18:13,000 --> 00:18:15,000
And so it takes this shock.

175
00:18:15,000 --> 00:18:20,000
It took this shock for this monk to snap him out of that,

176
00:18:20,000 --> 00:18:24,000
which is why watching the body even healthy,

177
00:18:24,000 --> 00:18:28,000
watching the body, watching when you walk, watching the stomach,

178
00:18:28,000 --> 00:18:31,000
you start to see, you start to lose this attraction,

179
00:18:31,000 --> 00:18:34,000
thinking originally that you were so handsome or so beautiful.

180
00:18:34,000 --> 00:18:40,000
And as you watch it, you start to see that the body is, you know,

181
00:18:40,000 --> 00:18:46,000
it's hanging, and it's tripping, and it's sweating, and it's bleeding,

182
00:18:46,000 --> 00:18:53,000
and it's infected, and it's pouring out, defined, and it's pouring out.

183
00:18:53,000 --> 00:18:57,000
Garbage constantly.

184
00:18:57,000 --> 00:19:00,000
I mean, of all things that we're going to be attached to,

185
00:19:00,000 --> 00:19:02,000
there are no diamonds and jewels inside.

186
00:19:02,000 --> 00:19:04,000
There's no sugar and spice.

187
00:19:04,000 --> 00:19:10,000
So there's nothing inside that's attractive or shouldn't be attractive in any way,

188
00:19:10,000 --> 00:19:18,000
but, you know, the greatest point for us as meditators is to learn to become objective,

189
00:19:18,000 --> 00:19:21,000
not to be repulsed by the body.

190
00:19:21,000 --> 00:19:23,000
But through our, through seeing how repulsed we can get,

191
00:19:23,000 --> 00:19:25,000
it's kind of hypocritical.

192
00:19:25,000 --> 00:19:31,000
There's no reason for us to be attracted to the body in one form.

193
00:19:31,000 --> 00:19:34,000
And this is a good example of how our misconceptions arise,

194
00:19:34,000 --> 00:19:35,000
not just about the body.

195
00:19:35,000 --> 00:19:37,000
We do the same with feelings.

196
00:19:37,000 --> 00:19:38,000
And we have happy feelings.

197
00:19:38,000 --> 00:19:43,000
Why are we attracted, attracted or attached to those?

198
00:19:43,000 --> 00:19:52,000
Why do painful feelings create such a violent aversion in us?

199
00:19:52,000 --> 00:19:53,000
So it's a bold claim.

200
00:19:53,000 --> 00:19:57,000
It's not that we should try to deny our attractions or attachments.

201
00:19:57,000 --> 00:20:01,000
We shouldn't deny the lust and the desire.

202
00:20:01,000 --> 00:20:05,000
We should just come to see how wrong it is, how silly it is.

203
00:20:05,000 --> 00:20:10,000
The bold claim means you don't have to have pre-judge.

204
00:20:10,000 --> 00:20:12,000
You don't have to have any preconceptions.

205
00:20:12,000 --> 00:20:16,000
You just have to look and see.

206
00:20:16,000 --> 00:20:20,000
And as you look at the body watching your own body,

207
00:20:20,000 --> 00:20:26,000
you start to see that there's nothing attractive or repulsive inherently about anything.

208
00:20:26,000 --> 00:20:31,000
It's us who become attracted or repulsive through conditioning,

209
00:20:31,000 --> 00:20:39,000
often through memories of pleasure or pain.

210
00:20:39,000 --> 00:20:43,000
So for example, if you eat some sort of food and it's poisoned

211
00:20:43,000 --> 00:20:49,000
and you get food poisoning, often eating that food or even looking at it,

212
00:20:49,000 --> 00:20:56,000
then it will cause you great nausea or aversion.

213
00:20:56,000 --> 00:21:02,000
I remember one time getting very, very, very drunk on peach snaps.

214
00:21:02,000 --> 00:21:07,000
And after that, the concept, the idea of peaches was just the smell of peaches,

215
00:21:07,000 --> 00:21:12,000
which is nauseating to me.

216
00:21:12,000 --> 00:21:17,000
Well, sort of details of my past.

217
00:21:17,000 --> 00:21:20,000
It's conditioning, and so we don't deny this.

218
00:21:20,000 --> 00:21:23,000
We don't try and deny our attractions and adversans who try to learn about them.

219
00:21:23,000 --> 00:21:26,000
And that's the power of this, the wonder of it.

220
00:21:26,000 --> 00:21:31,000
This is perfectly natural, objective.

221
00:21:31,000 --> 00:21:35,000
There's nothing subjective or particular about the Buddha's teaching.

222
00:21:35,000 --> 00:21:42,000
It takes reality, and it comes to see it as it is, boldly claiming that

223
00:21:42,000 --> 00:21:45,000
when you look at reality, this is what you'll see.

224
00:21:45,000 --> 00:21:49,000
It's a bold claim because it's fully open to investigation.

225
00:21:49,000 --> 00:21:52,000
If you investigate and find out that the Buddha's teaching was all wrong,

226
00:21:52,000 --> 00:21:55,000
well, that would be that.

227
00:21:55,000 --> 00:21:57,000
That is something you can do.

228
00:21:57,000 --> 00:21:58,000
There is no need for belief.

229
00:21:58,000 --> 00:22:05,000
There is no need to remain in a state of faith towards the Buddha's teaching.

230
00:22:05,000 --> 00:22:09,000
Once you look in see reality, you will have proof in your evidence

231
00:22:09,000 --> 00:22:17,000
and perfect confidence in the Buddha's teaching based on your understanding.

232
00:22:17,000 --> 00:22:20,000
So little food for thought, especially about the body,

233
00:22:20,000 --> 00:22:26,000
to make us all see how ridiculous we are with our attachment.

234
00:22:26,000 --> 00:22:31,000
Reminding us, this monk reminding us of ourselves.

235
00:22:31,000 --> 00:22:33,000
So there you go.

236
00:22:33,000 --> 00:22:35,000
That's the Damapada for tonight.

237
00:22:35,000 --> 00:22:40,000
Thank you all for tuning in.

