1
00:00:00,000 --> 00:00:07,000
Okay, good evening and welcome to our live broadcast November 3rd 2015.

2
00:00:07,000 --> 00:00:18,000
Again, we won't be doing the demo part of tonight, so tonight is just to say hello tonight.

3
00:00:18,000 --> 00:00:26,000
For now, it's nice from my perspective to not.

4
00:00:26,000 --> 00:00:34,000
You might have a couple questions though, because from the other things.

5
00:00:34,000 --> 00:00:40,000
The thing is we'll answer questions and say stuff.

6
00:00:40,000 --> 00:00:43,000
There might be some things to talk about.

7
00:00:43,000 --> 00:00:54,000
There was a problem on the website, around the appointments page, because this whole daylight savings time that was the culprit.

8
00:00:54,000 --> 00:01:04,000
Well, the real culprit is not having a system to deal with daylight savings time changes,

9
00:01:04,000 --> 00:01:06,000
because we're dealing with UT.

10
00:01:06,000 --> 00:01:19,000
We're talking in UTC time, so I just hacked it and missed a step, which made it difficult, but it just fixed them.

11
00:01:19,000 --> 00:01:29,000
Last night, I gave a talk to a whole room full of community advisor residents, community advisors and requester.

12
00:01:29,000 --> 00:01:32,000
This really neat. Actually, I wasn't really clear.

13
00:01:32,000 --> 00:01:35,000
I hadn't really been paying attention to what the meeting was.

14
00:01:35,000 --> 00:01:48,000
I just thought it was some run-on-the-mill conference, but it turns out this was a huge portion of the residents.

15
00:01:48,000 --> 00:01:58,000
People who work for the university to, like their second year or upper students,

16
00:01:58,000 --> 00:02:06,000
who work for the university to make sure that the resident's life is run smooth for people,

17
00:02:06,000 --> 00:02:11,000
so taking care of their new students basically, living in their residences with them,

18
00:02:11,000 --> 00:02:16,000
and taking care of any problems they might have, acting as supporters and so on.

19
00:02:16,000 --> 00:02:22,000
So it's really the best group that you could possibly reach with meditation,

20
00:02:22,000 --> 00:02:27,000
because these are people who are, whose job really is to help others,

21
00:02:27,000 --> 00:02:31,000
and who are generally the sort of people who like to help,

22
00:02:31,000 --> 00:02:35,000
who like to take care and to do good things.

23
00:02:35,000 --> 00:02:39,000
They tend to be really, really neat people.

24
00:02:39,000 --> 00:02:43,000
So it was a really good group. They were really loud.

25
00:02:43,000 --> 00:02:46,000
But I think that's from living on residence,

26
00:02:46,000 --> 00:02:52,000
and having to be the sort of the leader on the residents.

27
00:02:52,000 --> 00:02:56,000
So I told the most story about 16 years ago.

28
00:02:56,000 --> 00:02:58,000
I'm telling a lot of 16-year-old stories,

29
00:02:58,000 --> 00:03:07,000
because that's when I was in the university before I started practicing meditation.

30
00:03:07,000 --> 00:03:15,000
I ended up looking after one of the community advisors one night when he was really drunk,

31
00:03:15,000 --> 00:03:18,000
and his girl had broken up with him.

32
00:03:18,000 --> 00:03:27,000
I was using it as an example of how the caretakers have to be able to take care of themselves.

33
00:03:27,000 --> 00:03:33,000
It was really, really, really went well.

34
00:03:33,000 --> 00:03:43,000
They really appreciated it, and I was able to explain it properly.

35
00:03:43,000 --> 00:03:49,000
And that was only a test run within the university,

36
00:03:49,000 --> 00:03:56,000
and there's going to be another bigger version.

37
00:03:56,000 --> 00:04:03,000
In the week or two weeks, 11 days on the 14th,

38
00:04:03,000 --> 00:04:07,000
and there's these sort of people that are coming from all around,

39
00:04:07,000 --> 00:04:09,000
either the province or the country,

40
00:04:09,000 --> 00:04:14,000
probably just the province, but I can remember.

41
00:04:14,000 --> 00:04:19,000
And so we will be presenting to them again.

42
00:04:19,000 --> 00:04:26,000
It's there these two CAs, Comic Master organized it,

43
00:04:26,000 --> 00:04:33,000
and one of them is just for Lincoln.

44
00:04:33,000 --> 00:04:38,000
The other one is Latino,

45
00:04:38,000 --> 00:04:42,000
but they're really nice, too, really nice people.

46
00:04:42,000 --> 00:04:49,000
And we're doing it together.

47
00:04:49,000 --> 00:04:54,000
So that was it.

48
00:04:54,000 --> 00:05:05,000
I guess if we have questions, let's take questions.

49
00:05:05,000 --> 00:05:08,000
We have some questions.

50
00:05:08,000 --> 00:05:13,000
During walking meditation, should the act of turning at the end of the path be divided into

51
00:05:13,000 --> 00:05:15,000
lifting, turning, lowering?

52
00:05:15,000 --> 00:05:16,000
Thank you, Bante.

53
00:05:16,000 --> 00:05:20,000
No, it should be turning.

54
00:05:20,000 --> 00:05:23,000
I mean, as it's written in the book,

55
00:05:23,000 --> 00:05:28,000
I guess the question is whether, as you increase the walking step,

56
00:05:28,000 --> 00:05:31,000
you also increase the steps it takes to turn around,

57
00:05:31,000 --> 00:05:36,000
and the answer is no, not generally.

58
00:05:36,000 --> 00:05:45,000
Especially because of how unbalanced that would potentially make you as your turn.

59
00:05:45,000 --> 00:05:51,000
Well, they should meditators make an effort to suppress their essential desires to some degree,

60
00:05:51,000 --> 00:05:55,000
or is it better for one's practice to simply follow these desires as usual,

61
00:05:55,000 --> 00:05:57,000
but try to remain mindful.

62
00:05:57,000 --> 00:06:01,000
Well, the best is not to do either, right?

63
00:06:01,000 --> 00:06:07,000
The ideal and everything is not to repress or to follow, but to understand and observe.

64
00:06:07,000 --> 00:06:10,000
It actually, technically, it's a kind of suppression,

65
00:06:10,000 --> 00:06:14,000
but it's a suppression through wisdom, suppression through insight.

66
00:06:14,000 --> 00:06:21,000
So you're suppressing them by seeing the objects clearly as they are.

67
00:06:21,000 --> 00:06:26,000
And it's called suppression because it doesn't actually get rid of the problem.

68
00:06:26,000 --> 00:06:28,000
The problem can always come back.

69
00:06:28,000 --> 00:06:32,000
It's only when you attain Yibana that the problem is really gone.

70
00:06:32,000 --> 00:06:37,000
But it's, it is creating a new habit.

71
00:06:37,000 --> 00:06:43,000
And so it's not a matter of one another way being one or the other,

72
00:06:43,000 --> 00:06:45,000
which way one or the other.

73
00:06:45,000 --> 00:06:48,000
Neither one of them are really all that good.

74
00:06:48,000 --> 00:06:51,000
Though if I were to have to pick, I mean, it really doesn't,

75
00:06:51,000 --> 00:06:55,000
it's really not fair either because the way the addiction system works

76
00:06:55,000 --> 00:07:00,000
and the way the brain is set up, you can't really oppose your desires for very long.

77
00:07:00,000 --> 00:07:08,000
The yes, the yes impulses apparently much, much stronger than the no impulse.

78
00:07:08,000 --> 00:07:10,000
So you can only say no for so long.

79
00:07:10,000 --> 00:07:17,000
And they've got, I mean, what I'm saying is neuroscientists have observed this back

80
00:07:17,000 --> 00:07:20,000
and can point out, according to the brain,

81
00:07:20,000 --> 00:07:25,000
the brain makeup, why it is.

82
00:07:25,000 --> 00:07:28,000
The no part is, tends to be fairly.

83
00:07:28,000 --> 00:07:30,000
Or it depends on the person.

84
00:07:30,000 --> 00:07:33,000
Some people have a very strong no faculty, I think.

85
00:07:33,000 --> 00:07:36,000
But in the end, the no faculty can't.

86
00:07:36,000 --> 00:07:38,000
It's unsustainable.

87
00:07:38,000 --> 00:07:43,000
As soon as you stop saying no, it starts saying yes again.

88
00:07:43,000 --> 00:07:49,000
So it's not a sustainable solution, but probably yes better

89
00:07:49,000 --> 00:07:51,000
to say no than to say yes.

90
00:07:51,000 --> 00:07:53,000
I would have to say off the top of my head.

91
00:07:53,000 --> 00:07:57,000
I mean, even the Buddha seems to have appreciated that.

92
00:07:57,000 --> 00:08:02,000
In a pinch, saying no is better than chasing actual desires.

93
00:08:02,000 --> 00:08:05,000
It's just it's not a solution and in the end it can actually backfire

94
00:08:05,000 --> 00:08:08,000
if you rely on it too much.

95
00:08:08,000 --> 00:08:23,000
And that was it. I'll cut up with questions already.

96
00:08:23,000 --> 00:08:25,000
Some in lots.

97
00:08:25,000 --> 00:08:26,000
I thought so too.

98
00:08:26,000 --> 00:08:28,000
And there was a, there were a lot of comments.

99
00:08:28,000 --> 00:08:34,000
Maybe, maybe time for random, random announcements like,

100
00:08:34,000 --> 00:08:37,000
have you ever mentioned on, on the broadcast about the Saturday day

101
00:08:37,000 --> 00:08:40,000
of mindfulness for anyone who might be in the Hamilton area?

102
00:08:40,000 --> 00:08:41,000
Nope.

103
00:08:41,000 --> 00:08:42,000
We have.

104
00:08:42,000 --> 00:08:44,000
I mean, it's actually just a placeholder right now

105
00:08:44,000 --> 00:08:47,000
because we haven't actually done anything with it.

106
00:08:47,000 --> 00:08:50,000
This Saturday, I'm going again in the morning.

107
00:08:50,000 --> 00:08:52,000
I'm going to London.

108
00:08:52,000 --> 00:08:57,000
I think there might be some trying to come again in the Saturday day.

109
00:08:57,000 --> 00:08:58,000
Yes.

110
00:08:58,000 --> 00:09:01,000
If there was someone signed up and she was going to bring her daughter London.

111
00:09:01,000 --> 00:09:04,000
You don't mean London, England.

112
00:09:04,000 --> 00:09:07,000
You know, we have a London in Ontario.

113
00:09:07,000 --> 00:09:08,000
Okay.

114
00:09:08,000 --> 00:09:10,000
That's actually not that far away.

115
00:09:10,000 --> 00:09:12,000
Oh.

116
00:09:16,000 --> 00:09:20,000
But in general, you know, for people that are in the Hamilton area,

117
00:09:20,000 --> 00:09:23,000
you're going to designate the Saturdays as,

118
00:09:27,000 --> 00:09:29,000
try to, yes.

119
00:09:29,000 --> 00:09:32,000
So if you're in the Hamilton area, you can go to the monastery

120
00:09:32,000 --> 00:09:35,000
from, what is it, nine to three?

121
00:09:35,000 --> 00:09:36,000
Right.

122
00:09:36,000 --> 00:09:38,000
Nine to four, maybe.

123
00:09:38,000 --> 00:09:39,000
Nine to three, nine to four.

124
00:09:39,000 --> 00:09:42,000
Bring your lunch.

125
00:09:42,000 --> 00:09:43,000
Yeah.

126
00:09:43,000 --> 00:09:47,000
I mean, the idea was just to designate the Saturday as a sort of

127
00:09:47,000 --> 00:09:52,000
day that we would encourage people to come out together.

128
00:09:52,000 --> 00:09:55,000
So there would be a sense of meditating in a group.

129
00:09:55,000 --> 00:09:58,000
If you want to actually meet with me, it doesn't have to be on Saturday

130
00:09:58,000 --> 00:10:03,000
and it might not always be possible on Saturday.

131
00:10:03,000 --> 00:10:07,000
I'll be back in the afternoon on Saturday.

132
00:10:07,000 --> 00:10:11,000
And then on the 14th, which is a Saturday as well.

133
00:10:11,000 --> 00:10:14,000
That's it.

134
00:10:14,000 --> 00:10:15,000
Right.

135
00:10:15,000 --> 00:10:18,000
So this is Saturday and then next Saturday as well.

136
00:10:18,000 --> 00:10:22,000
Next Saturday, I've been invited to Toronto and then

137
00:10:22,000 --> 00:10:28,000
back here for the CA conference in the afternoon.

138
00:10:28,000 --> 00:10:31,000
So that next Saturday is right out.

139
00:10:31,000 --> 00:10:33,000
So there's a couple of Saturdays booked up.

140
00:10:33,000 --> 00:10:37,000
But eventually, once funding, it's sort of all the other Saturday

141
00:10:37,000 --> 00:10:38,000
obligations.

142
00:10:38,000 --> 00:10:42,000
There'll be a nice day of mindfulness every Saturday.

143
00:10:42,000 --> 00:10:46,000
That is nice.

144
00:10:46,000 --> 00:10:50,000
Have you thought about having any sort of a children's

145
00:10:50,000 --> 00:10:54,000
organization on those days?

146
00:10:54,000 --> 00:10:56,000
Like where people would bring their children to learn

147
00:10:56,000 --> 00:10:58,000
meditation?

148
00:10:58,000 --> 00:10:59,000
Yeah.

149
00:10:59,000 --> 00:11:02,000
I mean, we don't have that much of a community yet.

150
00:11:02,000 --> 00:11:06,000
No, but for anyone who's in the area, there's a meetup group.

151
00:11:06,000 --> 00:11:08,000
A website meetup.

152
00:11:08,000 --> 00:11:12,000
So if you just Google meetup, Hamilton, what's it called?

153
00:11:12,000 --> 00:11:16,000
Hamilton meditation group or something?

154
00:11:16,000 --> 00:11:20,000
I mean, if people want to, I'm not really, you know, adding

155
00:11:20,000 --> 00:11:24,000
more events.

156
00:11:24,000 --> 00:11:27,000
We're trying that with the McMaster Buddhism Association.

157
00:11:27,000 --> 00:11:31,000
If you set up an event, it's a bit of mental work to get it set

158
00:11:31,000 --> 00:11:33,000
up and then no one shows up.

159
00:11:33,000 --> 00:11:36,000
So I think for the time being, I'm just going to be a little

160
00:11:36,000 --> 00:11:38,000
bit more organic about it.

161
00:11:38,000 --> 00:11:39,000
People show up.

162
00:11:39,000 --> 00:11:42,000
They can work things out.

163
00:11:42,000 --> 00:11:43,000
Because I'm doing it.

164
00:11:43,000 --> 00:11:45,000
I'm doing quite a bit of scheduled stuff anyway.

165
00:11:45,000 --> 00:11:46,000
Definitely.

166
00:11:46,000 --> 00:11:49,000
This is just, it's a good format just to let people in the

167
00:11:49,000 --> 00:11:55,000
Hamilton area know that there is a monastery there now.

168
00:11:55,000 --> 00:11:57,000
And then the other thing is the website,

169
00:11:57,000 --> 00:12:00,000
SiriMunglo.org has a nice new layout on it.

170
00:12:00,000 --> 00:12:02,000
If anybody hasn't looked at it for a while.

171
00:12:02,000 --> 00:12:06,000
We're trying to, trying to get that a little more organized.

172
00:12:06,000 --> 00:12:08,000
To have some updated information.

173
00:12:08,000 --> 00:12:10,000
I thought we changed the website a while ago.

174
00:12:10,000 --> 00:12:12,000
And then when you mentioned it, I looked and

175
00:12:12,000 --> 00:12:15,000
know it's still got the old layout.

176
00:12:15,000 --> 00:12:16,000
Yeah.

177
00:12:16,000 --> 00:12:18,000
Quite sure I changed the layout quite some time ago.

178
00:12:18,000 --> 00:12:20,000
No, I've never seen it.

179
00:12:20,000 --> 00:12:21,000
Look anything different then.

180
00:12:21,000 --> 00:12:23,000
But I think it looks nice now.

181
00:12:23,000 --> 00:12:26,000
A lot of pressure of a look.

182
00:12:26,000 --> 00:12:28,000
Yeah, it's the Nirvana theme.

183
00:12:28,000 --> 00:12:29,000
Did you know this?

184
00:12:29,000 --> 00:12:31,000
I didn't know.

185
00:12:31,000 --> 00:12:33,000
That's cool.

186
00:12:33,000 --> 00:12:34,000
Perfect.

187
00:12:34,000 --> 00:12:40,000
We're also using it for the McMaster Buddhism Association.

188
00:12:40,000 --> 00:12:44,000
And I think Anthony had posted something in Slack.

189
00:12:44,000 --> 00:12:49,000
The British Columbia Buddhist Wuhara is using the same theme.

190
00:12:49,000 --> 00:12:52,000
So everybody likes the Nirvana theme.

191
00:12:52,000 --> 00:12:56,000
Understandable.

192
00:12:56,000 --> 00:13:05,000
If there's anything the website is missing.

193
00:13:05,000 --> 00:13:08,000
If anyone wants to post a comment.

194
00:13:08,000 --> 00:13:13,000
I'm doing some minor editing and would be happy to add anything

195
00:13:13,000 --> 00:13:38,000
if we're missing something blatant.

196
00:13:38,000 --> 00:13:45,000
Okay.

197
00:13:45,000 --> 00:14:06,000
Okay.

198
00:14:06,000 --> 00:14:10,000
That's all for tonight.

199
00:14:10,000 --> 00:14:15,000
The appointment schedule is working now.

200
00:14:15,000 --> 00:14:20,000
So if anyone wants to fill up the last three slots.

201
00:14:20,000 --> 00:14:25,000
So probably just drop out of university and add more slots to this.

202
00:14:25,000 --> 00:14:30,000
There you go.

203
00:14:30,000 --> 00:14:37,000
I don't know, there's something about getting my degree that still seems a little bit.

204
00:14:37,000 --> 00:14:38,000
It doesn't really.

205
00:14:38,000 --> 00:14:39,000
Like I would have never thought before.

206
00:14:39,000 --> 00:14:41,000
I heard about other monks getting a degree and I thought,

207
00:14:41,000 --> 00:14:42,000
what's that for?

208
00:14:42,000 --> 00:14:45,000
But everyone said, you know, being in the West.

209
00:14:45,000 --> 00:14:48,000
There's something about having a degree.

210
00:14:48,000 --> 00:14:52,000
So you still think it's a good thing to have?

211
00:14:52,000 --> 00:14:54,000
I don't know.

212
00:14:54,000 --> 00:14:56,000
I mean, I don't really care.

213
00:14:56,000 --> 00:15:00,000
I'm not going to feel pleased or something when I get a degree.

214
00:15:00,000 --> 00:15:04,000
You know, my father from Cleveland.

215
00:15:04,000 --> 00:15:06,000
That was one of the reasons.

216
00:15:06,000 --> 00:15:13,000
Because I did make a promise of an agreement with him many years ago that I never kept.

217
00:15:13,000 --> 00:15:22,000
Oh, that was where he was going to stop hunting and you were going to get a degree.

218
00:15:22,000 --> 00:15:27,000
I have to think about it.

219
00:15:27,000 --> 00:15:30,000
And the next semester I might be able to take evening courses.

220
00:15:30,000 --> 00:15:35,000
I might just do some evening courses or like a course in Buddhism or something.

221
00:15:35,000 --> 00:15:38,000
That was the sort of the idea that I'd be able to study Buddhism.

222
00:15:38,000 --> 00:15:47,000
It's always nice to have some formal systems.

223
00:15:47,000 --> 00:15:52,000
Today I had an interesting conversation with my philosophy.

224
00:15:52,000 --> 00:15:55,000
He's doing his thesis on pragmatism.

225
00:15:55,000 --> 00:15:56,000
Color.

226
00:15:56,000 --> 00:15:58,000
Color in pragmatism.

227
00:15:58,000 --> 00:16:02,000
Probably doesn't mean much if you're a philosopher.

228
00:16:02,000 --> 00:16:03,000
Didn't to me.

229
00:16:03,000 --> 00:16:05,000
But he explained pragmatism.

230
00:16:05,000 --> 00:16:06,000
It's very close to Buddhism.

231
00:16:06,000 --> 00:16:09,000
In fact, it's probably heavily influenced by Buddhism.

232
00:16:09,000 --> 00:16:13,000
And he was talking about like this guy, William James,

233
00:16:13,000 --> 00:16:17,000
who, you know, fairly famous guy.

234
00:16:17,000 --> 00:16:19,000
And he was influenced by Buddhism.

235
00:16:19,000 --> 00:16:26,000
And we're talking about sort of these readings that we've been doing in our philosophy class.

236
00:16:26,000 --> 00:16:32,000
And how they relate or how they differ from pragmatism.

237
00:16:32,000 --> 00:16:37,000
So apparently Locke and John Locke.

238
00:16:37,000 --> 00:16:39,000
You see a pragmatist?

239
00:16:39,000 --> 00:16:41,000
I think in some ways he was a pragmatist.

240
00:16:41,000 --> 00:16:46,000
A pragmatism is just empirical.

241
00:16:46,000 --> 00:16:48,000
John Locke was an empiricist.

242
00:16:48,000 --> 00:16:54,000
So he only looked at what you experienced.

243
00:16:54,000 --> 00:16:56,000
What is readily apparent.

244
00:16:56,000 --> 00:16:58,000
So there was no sense of substance.

245
00:16:58,000 --> 00:17:00,000
Things that actually exist.

246
00:17:00,000 --> 00:17:07,000
Here's what I'm studying in philosophy, which is quite interesting or a Buddhist perspective.

247
00:17:07,000 --> 00:17:10,000
The idea of whether entities exist, right?

248
00:17:10,000 --> 00:17:16,000
So Descartes and Spinoza, or the other two philosophers are looking at.

249
00:17:16,000 --> 00:17:19,000
They talk about substance.

250
00:17:19,000 --> 00:17:28,000
And Spinoza goes a step further and talks about something called modes and attributes.

251
00:17:28,000 --> 00:17:31,000
And it's all very weird.

252
00:17:31,000 --> 00:17:41,000
But then Locke comes along and says, what meaning is there in substance?

253
00:17:41,000 --> 00:17:44,000
Which is, you know, it's a fairly Buddhist thing to say.

254
00:17:44,000 --> 00:17:52,000
Just stop thinking of things as entities and more in terms of events.

255
00:17:52,000 --> 00:17:56,000
It's interesting.

256
00:17:56,000 --> 00:18:01,000
Did he have any knowledge of Buddhism to your knowledge?

257
00:18:01,000 --> 00:18:02,000
Or did he come up?

258
00:18:02,000 --> 00:18:08,000
I think he was a student of Hobbes, which was a materialist.

259
00:18:08,000 --> 00:18:10,000
And there was sort of a tendency.

260
00:18:10,000 --> 00:18:14,000
They were moving towards materialism.

261
00:18:14,000 --> 00:18:19,000
And they were very interested in the material world in ways that they hadn't been.

262
00:18:19,000 --> 00:18:20,000
They were trying to figure it out.

263
00:18:20,000 --> 00:18:23,000
They started realizing, hey, we can figure them out.

264
00:18:23,000 --> 00:18:31,000
And actually come to understand the natural world.

265
00:18:31,000 --> 00:18:34,000
Because before it was just like, well, it must be an act of God.

266
00:18:34,000 --> 00:18:36,000
God is running things.

267
00:18:36,000 --> 00:18:38,000
And they said, no, actually, it's not.

268
00:18:38,000 --> 00:18:40,000
It's actually running fairly mechanistic.

269
00:18:40,000 --> 00:18:46,000
And then it led to Newton and this whole idea of mechanistic physics.

270
00:18:46,000 --> 00:18:54,000
And which has now been, has been basically thrown out to some extent by quantum physics.

271
00:18:54,000 --> 00:18:59,000
Which is non mechanistic.

272
00:18:59,000 --> 00:19:02,000
The pretty big problem is it's a lot of thinking.

273
00:19:02,000 --> 00:19:05,000
And they seem to be doing a lot of thinking.

274
00:19:05,000 --> 00:19:10,000
I think my brain stopped working in that way many years ago.

275
00:19:10,000 --> 00:19:12,000
I think so much.

276
00:19:12,000 --> 00:19:19,000
Lots of thinking and philosophy.

277
00:19:19,000 --> 00:19:24,000
I'm learning Latin, which I'll probably never use.

278
00:19:24,000 --> 00:19:27,000
Are you able to keep straight the Latin and the poly?

279
00:19:27,000 --> 00:19:28,000
Yeah.

280
00:19:28,000 --> 00:19:29,000
I'm not learning poly.

281
00:19:29,000 --> 00:19:32,000
That'd make something if I could take poly first.

282
00:19:32,000 --> 00:19:35,000
But it would probably be.

283
00:19:35,000 --> 00:19:36,000
You see, that's the thing.

284
00:19:36,000 --> 00:19:41,000
If I do religious studies, I'll probably be able to use poly in the later courses.

285
00:19:41,000 --> 00:19:46,000
I'm just kind of this year was trying to do some easier courses.

286
00:19:46,000 --> 00:19:51,000
Maybe next semester, you can take an upper-year Buddhism course.

287
00:19:51,000 --> 00:19:54,000
But you know, I've done a couple of short papers this semester.

288
00:19:54,000 --> 00:20:01,000
And that's kind of been good to get back into trying to write a formal paper.

289
00:20:01,000 --> 00:20:05,000
It's just been kind of easy.

290
00:20:05,000 --> 00:20:11,000
It felt like it's problematic.

291
00:20:11,000 --> 00:20:15,000
There are other things I could be doing as opposed to.

292
00:20:15,000 --> 00:20:18,000
Do you have to take a visit?

293
00:20:18,000 --> 00:20:20,000
I did in college.

294
00:20:20,000 --> 00:20:26,000
They actually required us to have a couple of units of like a gym class.

295
00:20:26,000 --> 00:20:30,000
Remember to skiing and volleyball and swimming?

296
00:20:30,000 --> 00:20:35,000
I don't have such classes in university here, I don't think.

297
00:20:35,000 --> 00:20:38,000
Maybe in kinesiology, I don't know.

298
00:20:38,000 --> 00:20:40,000
No, I don't think so.

299
00:20:40,000 --> 00:20:46,000
I always said extra curricular intramural whatever you call it.

300
00:20:46,000 --> 00:20:48,000
You can do it all.

301
00:20:48,000 --> 00:20:52,000
And now these are something that was required just for all degrees.

302
00:20:52,000 --> 00:20:56,000
It's kind of strange, but not a bad thing.

303
00:20:56,000 --> 00:20:59,000
They were enjoyable.

304
00:20:59,000 --> 00:21:04,000
There's a question.

305
00:21:04,000 --> 00:21:07,000
I'm not sure if it's a hypothetical question.

306
00:21:07,000 --> 00:21:13,000
One thing will we ever know the answer to what is 1, 2, 3 from the domo panda?

307
00:21:13,000 --> 00:21:16,000
It's funny, I didn't even give you an answer, didn't I?

308
00:21:16,000 --> 00:21:17,000
Shoot.

309
00:21:17,000 --> 00:21:18,000
And I meant to as well.

310
00:21:18,000 --> 00:21:23,000
I just must have gotten caught up in something else.

311
00:21:23,000 --> 00:21:24,000
And it's good.

312
00:21:24,000 --> 00:21:28,000
It's good to leave you wondering what is one.

313
00:21:28,000 --> 00:21:31,000
I wonder if there's a translation of it on the internet.

314
00:21:31,000 --> 00:21:34,000
It must be somewhere.

315
00:21:34,000 --> 00:21:43,000
Maybe someone can provide the translation of the kumara panda.

316
00:21:43,000 --> 00:21:47,000
So let's say if I can't remember them all, because we don't really use them.

317
00:21:47,000 --> 00:21:53,000
I know the first one, economic king sub-based at the aha-de to tikka.

318
00:21:53,000 --> 00:21:56,000
All being subsist on food.

319
00:21:56,000 --> 00:22:00,000
It's one, one is food.

320
00:22:00,000 --> 00:22:10,000
And interestingly, again leading back to this idea of food.

321
00:22:10,000 --> 00:22:14,000
Food being somehow important and somehow central.

322
00:22:14,000 --> 00:22:18,000
But by aha-ra, it's meant different kinds of food.

323
00:22:18,000 --> 00:22:23,000
So there's the physical food, and then there's mental food, basically.

324
00:22:23,000 --> 00:22:27,000
There's four kinds.

325
00:22:27,000 --> 00:22:33,000
Two is, I think, namancha rupancha.

326
00:22:33,000 --> 00:22:38,000
Namancha rupancha.

327
00:22:38,000 --> 00:22:47,000
Namancha rupancha rupancha rupancha rupancha.

328
00:22:47,000 --> 00:22:52,000
And five aggregates of clinging.

329
00:22:52,000 --> 00:23:02,000
Chat namancha sex.

330
00:23:02,000 --> 00:23:10,000
What is Dasikyanis?

331
00:23:10,000 --> 00:23:13,000
Chat namanchan.

332
00:23:13,000 --> 00:23:16,000
Saced.

333
00:23:16,000 --> 00:23:16,000
ANS.

334
00:23:16,000 --> 00:23:23,000
Satanamakin, the seven boat jungle, seven factors of enlightenment.

335
00:23:23,000 --> 00:23:31,000
Atanamakin, Ario Artang Goma, the Eight Full Noble Path of the Eight.

336
00:23:31,000 --> 00:23:36,000
Now, when I'm a king, Nava, Satala, what is nine?

337
00:23:36,000 --> 00:23:42,000
The nine, abodes of beings, when I can't remember what those are, but it's basically human,

338
00:23:42,000 --> 00:23:46,000
and all sorts, angel, and a problem.

339
00:23:46,000 --> 00:23:48,000
I can't remember it.

340
00:23:48,000 --> 00:23:53,000
It's a sort of weird enumeration, but there are nine.

341
00:23:53,000 --> 00:23:55,000
Satal, Sat.

342
00:23:55,000 --> 00:24:02,000
Dasanamakin, the one who is ten, Dasan, Dasan-gahi,

343
00:24:02,000 --> 00:24:17,000
Samana Agatou, Arahad, the ten factors, and accomplished with, or associated with,

344
00:24:17,000 --> 00:24:38,000
if one goes by, then one has ten factors, one is in Arahad.

345
00:24:38,000 --> 00:24:45,000
So, here we have the commentary, let's go through and see.

346
00:24:45,000 --> 00:25:07,000
Dasanamakin, the eight full noble path, plus Samana and Samana,

347
00:25:07,000 --> 00:25:12,000
Mah 50 matimani.

348
00:25:12,000 --> 00:25:22,000
Let's see how many moves you can see departments, like that.

349
00:25:22,000 --> 00:25:31,000
Don't figure out.

350
00:25:31,000 --> 00:25:41,320
Yeah, it's the ten basic aspects of Buddhism fairly core things that the Buddha apparently

351
00:25:41,320 --> 00:26:10,120
is supposed to have taught to Rāhala, it's for Rāhala, I was claiming I had read a version

352
00:26:10,120 --> 00:26:14,840
where investigation was the first one and I was completely wrong, it was sati as he said

353
00:26:14,840 --> 00:26:21,480
of course and once I looked it up I was just remembering it completely wrong, so thank you for

354
00:26:21,480 --> 00:26:33,480
me and you for that.

355
00:26:33,480 --> 00:26:37,480
Okay, enough, thank you, have a good day.

356
00:26:37,480 --> 00:26:57,480
Thank you, Bhante, good night.

