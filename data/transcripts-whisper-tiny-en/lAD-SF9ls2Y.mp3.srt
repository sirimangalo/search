1
00:00:00,000 --> 00:00:04,480
Good evening and welcome back to our study of the Damapada.

2
00:00:04,480 --> 00:00:10,920
Tonight we continue on with verse number 82, which reads as follows.

3
00:00:10,920 --> 00:00:30,020
Here as a

4
00:00:30,020 --> 00:00:42,820
be what rahado just as a lake, gambiro that is deep. We pass ano that is clear and now we know

5
00:00:43,940 --> 00:00:54,500
that is undisturbed. Just as a calm, clear, deep lake, just like a calm, clear, deep lake,

6
00:00:54,500 --> 00:01:06,420
even the manis at one. So are those who have having listened to the dhamma, or so are the wise,

7
00:01:07,140 --> 00:01:13,860
having listened to the dhamma. So to the wise, having listened to the dhamma, we proceed anti,

8
00:01:13,860 --> 00:01:27,540
become tranquil, become calm. So the idea here is of a lake that becomes calm. A person who listens to

9
00:01:27,540 --> 00:01:36,820
the dhamma becomes calm just like a calm, deep lake. Gambiro is, depth is used the same way it is

10
00:01:36,820 --> 00:01:47,540
in English to talk about a person's depth, their wisdom. So a wise person will be deep or found.

11
00:01:50,340 --> 00:01:58,580
This story was, this verse was told in regards to a story about a woman named God and her mother.

12
00:01:58,580 --> 00:02:10,660
Now, Kana was married to a man and of course, she would have gone to live in her husband's house

13
00:02:11,780 --> 00:02:21,220
and one time she went back to visit her parents and stayed there for quite some time.

14
00:02:21,220 --> 00:02:28,100
Eventually, her husband called for her and said, Mr. was missing her and asked her to return.

15
00:02:29,860 --> 00:02:36,900
And Kana was preparing to return, but her mother said, oh no, you can't go back empty-handed

16
00:02:36,900 --> 00:02:44,580
after all this time. You should bring something back to give to your husband as a gift.

17
00:02:44,580 --> 00:02:54,500
So the mother was kind thinking, that good intentions. And Kana herself was a kind person.

18
00:02:55,860 --> 00:03:04,580
She had good intentions, so she was happy to do this. But Kana was also beyond that,

19
00:03:04,580 --> 00:03:15,620
she was a generous and compassionate person. And so she waited and when the cake was ready,

20
00:03:16,420 --> 00:03:21,700
sorry, it was a cake. So the mother said, wait, I'll bake you a cake and you can bring the cake back to

21
00:03:21,700 --> 00:03:29,220
your husband. So Kana waited and when the cake was ready, she was preparing to leave and then

22
00:03:29,220 --> 00:03:37,700
all of a sudden the monk showed up, a Buddhist monk showed up at her door. And so she had

23
00:03:37,700 --> 00:03:43,540
nothing else she thought, yeah, I'll give this cake to the monk, what a good opportunity to give

24
00:03:43,540 --> 00:03:52,340
arms. And so she gave him the cake, which would have been a real fine for him because

25
00:03:52,340 --> 00:03:57,380
cake wouldn't be something that you would get, especially a fine cake here that this woman was

26
00:03:57,380 --> 00:04:02,260
ready to give to her husband. So she gave him the cake and went back to her mother and said,

27
00:04:02,260 --> 00:04:09,620
look, I gave the cake to a monk and you can you make me another one? And her mother was so fine

28
00:04:09,620 --> 00:04:18,180
with that. Oh, that's great. Good for you. Meanwhile, this monk did something that he probably

29
00:04:18,180 --> 00:04:24,420
shouldn't have done. He was an older fellow and a little bit excited about the cake.

30
00:04:24,420 --> 00:04:32,420
And so he went back to the monastery and told the other old monks about it. They sit around

31
00:04:32,420 --> 00:04:40,980
a bunch of old old guys. Oh, yeah, I got a cake from this house. You got a cake. So a second monk

32
00:04:40,980 --> 00:04:50,820
heard this and he went to the house. And he got there just as the cake was baked and the woman

33
00:04:50,820 --> 00:04:58,740
was getting ready to return to her husband. And she saw him coming and again felt obligated

34
00:04:58,740 --> 00:05:08,740
to give another cake. So she gave the new cake to the second monk. And she was still happy,

35
00:05:08,740 --> 00:05:12,980
but she was like, okay, well, it's good that we're giving to monks and really better get going.

36
00:05:12,980 --> 00:05:21,380
So she asked her mother, please, mother. One more time. That's all just last time. One more cake

37
00:05:21,380 --> 00:05:27,380
and the mother was like, oh, well, okay, very well. Baked her a third cake. Of course, the second

38
00:05:27,380 --> 00:05:37,380
monk went back to the monastery and told his friends. And a third monk came and getting a little

39
00:05:37,380 --> 00:05:42,740
bit frustrated at this point, but still not wanting to turn anyone away empty handed. She gave

40
00:05:42,740 --> 00:05:55,140
a third cake to the third monk. Meanwhile, her husband was kind of frustrated at this point.

41
00:05:55,140 --> 00:06:00,660
I think this all happened on one day. It would sort of make more sense. It happened one per day,

42
00:06:00,660 --> 00:06:07,380
but I think it was the same day. And so her husband sent a messenger and said, okay, wait,

43
00:06:07,380 --> 00:06:14,020
wait, wait, wait, wait. She's coming, but I just have to wait a little bit longer because

44
00:06:14,020 --> 00:06:18,740
I'm baking the second cake and then baking the third cake. And they baked the third cake in the

45
00:06:18,740 --> 00:06:24,260
fourth month, a third monk came, right? And we said that third monk came, baked the fourth cake,

46
00:06:24,260 --> 00:06:32,020
baked the third cake away, baked the fourth cake, a fourth monk came. And at this point,

47
00:06:32,020 --> 00:06:36,980
the husband got fed up and was told that he should just find another wife. And indeed,

48
00:06:36,980 --> 00:06:42,900
that's what he did. He figured that his wife was never coming home. And so he went and found a new

49
00:06:42,900 --> 00:06:54,820
wife. Anyway, just a story of one of those things that happens, but the upshot of it was that

50
00:06:54,820 --> 00:07:02,340
Kanan was not happy. And she blamed the monks. That she felt sort of obligated to give food to these

51
00:07:02,340 --> 00:07:06,340
monks. And you could say the monks were to blame the Buddha. In fact, used this

52
00:07:09,140 --> 00:07:15,220
opportunity or this situation to create a rule. We have one of our rules in regards to this

53
00:07:16,740 --> 00:07:22,500
story. And the rule is that you have to have to take a limited number of cakes if people are

54
00:07:22,500 --> 00:07:28,900
offering cake. And I guess by extension, anything that's sort of a fine food, there's a limit on what

55
00:07:28,900 --> 00:07:33,700
you can take. And when you get something like that, you have to share it whenever you get

56
00:07:34,420 --> 00:07:39,220
with the other monks. No, it's not like I got some. So you go and get your own cake and everyone

57
00:07:39,220 --> 00:07:48,340
gets an entire cake for themselves. But anyway, kind of previous prior to this being a

58
00:07:51,380 --> 00:07:58,180
gentle and kind person became a mean and vicious person towards the monks anyway. And whenever

59
00:07:58,180 --> 00:08:08,260
she saw a monk, she would revile them, call them beggars and greedy and mean and nasty.

60
00:08:09,540 --> 00:08:12,740
So she would say mean things to them.

61
00:08:20,020 --> 00:08:25,060
It doesn't say what she actually said. But she said they have ruined these monks have ruined my

62
00:08:25,060 --> 00:08:30,900
married life because her husband went and got another wife. Her husband could sort of do what they

63
00:08:30,900 --> 00:08:35,940
wanted in India. Indeed, that's what he did in the fickle sort of fellow.

64
00:08:41,700 --> 00:08:46,100
The Buddha found out about this and he went to see Kana and he asked her, is it true that this is

65
00:08:46,100 --> 00:08:53,940
happening? And she said, yes, they destroyed my marriage and he said, well,

66
00:08:53,940 --> 00:08:58,660
did they steal the cakes from you? Or did you give them willingly? I gave them to them.

67
00:08:59,380 --> 00:09:02,580
No, did they even ask you for cake? No, they didn't ask me for cake.

68
00:09:04,820 --> 00:09:10,260
And so the Buddha just pointed out how silly she was being. And he said, in that case,

69
00:09:11,140 --> 00:09:18,180
who was to blame here? Who was the one who was to blame? Well, it's true. I guess I am to blame.

70
00:09:18,180 --> 00:09:25,380
And because of that, the Buddha, once she accepted that, the Buddha taught her the dhamma

71
00:09:26,660 --> 00:09:30,980
and she actually sitting there was able to put it into practice and become a sotupana.

72
00:09:33,780 --> 00:09:38,180
And then she asked us forgiveness for being ruined to the monks, even though you could argue that

73
00:09:38,180 --> 00:09:48,020
the monks didn't weren't completely properly behaved. It's true. They didn't even beg

74
00:09:48,020 --> 00:09:52,980
for anything. They just came to see whether they were giving. And indeed, she kept giving.

75
00:09:53,780 --> 00:09:58,500
I was up to her, she could have stopped and said no. On the other hand, it was kind of a neat

76
00:09:58,500 --> 00:10:08,020
thing to do. I couldn't say that well. Either way, it wasn't a very neat thing for her to

77
00:10:09,860 --> 00:10:14,660
revile and revile other people. And so this is why the Buddha taught this first.

78
00:10:14,660 --> 00:10:19,780
The monks were talking about it. The Buddha taught about how she had been a mouse in the past

79
00:10:19,780 --> 00:10:25,460
life, but I don't know. I'm not going to get into the Jataka story. She reviled a bunch of cats

80
00:10:26,580 --> 00:10:34,020
and the cats ended up dying. There's not much point to the Jataka story, except she was reviling cats.

81
00:10:34,020 --> 00:10:44,180
But then the Buddha taught the Jataka story, taught the verse. It's that our mind was

82
00:10:44,180 --> 00:10:52,900
turbid and became calm as a lake of still water. And then he taught the verse.

83
00:10:52,900 --> 00:11:07,940
Jatapi, rahado, gampiru, vipasanu, anavilo, and so on. Dhammani, sutwana is interesting. Dhammani means

84
00:11:07,940 --> 00:11:17,940
the dhammas. It's plural. It's neutral plural. It probably doesn't mean much to most people,

85
00:11:17,940 --> 00:11:26,820
but it's an interesting use of the word dhammani. But it means the dhammas, so truths, good teachings.

86
00:11:32,580 --> 00:11:38,740
So how does this relate to our practice? Well, immediately comes to mind for me as the

87
00:11:38,740 --> 00:11:47,300
whole concept of meditation interviews that we've been conducting. We have these weekly

88
00:11:47,300 --> 00:11:52,580
interviews, so people have been making appointments on our meditation page. And then we have a

89
00:11:52,580 --> 00:12:00,100
Google Hangout one-on-one and help people out, give them advice. And so I've been doing this for

90
00:12:00,900 --> 00:12:06,900
a few weeks down. It's worked really great. But before that, I've done this in person. And

91
00:12:06,900 --> 00:12:12,660
you know, just having someone there to remind you and to clarify your doubts is

92
00:12:12,660 --> 00:12:20,420
it's quite a stabilizer for your practice. I know it's easy to get caught up and we get all

93
00:12:20,420 --> 00:12:28,340
worked up about turning mountains into making mountains out of molehills,

94
00:12:29,940 --> 00:12:36,260
turning something very small into something very big because it's very hard for us to see our

95
00:12:36,260 --> 00:12:43,140
faults. It's very hard for us to guide ourselves on something, especially on something that is new

96
00:12:43,140 --> 00:12:54,900
to us, something unfamiliar. But hearing the dhamma is a great thing and it's amazing how in the

97
00:12:54,900 --> 00:13:00,980
Buddhist time just hearing the dhammas often enough to enlighten someone and people often

98
00:13:00,980 --> 00:13:07,060
question how this can be possible. But this is a part of it, really, that the dhamma is something

99
00:13:07,060 --> 00:13:15,220
that quiets your mind and focuses your mind. Hearing it is keeping it fresh. It's bringing it,

100
00:13:15,220 --> 00:13:20,820
bringing your attention back again and again to the truth, to the good things.

101
00:13:22,660 --> 00:13:26,500
So if someone teaches about the force of Di Patana or someone teaches about insight,

102
00:13:26,500 --> 00:13:35,460
someone teaches about the five aggregates, the nature of reality, at the same time the mind of the

103
00:13:35,460 --> 00:13:41,620
listener becomes not only calm but becomes clear. So I was able to see things clearly.

104
00:13:44,100 --> 00:13:48,260
Can we talk about how a meditator knows when they're sitting, that they're sitting,

105
00:13:48,980 --> 00:13:54,420
then everyone listening and suddenly becomes aware of the fact that they're sitting

106
00:13:54,420 --> 00:14:00,420
and becomes aware of the present moment. So the mind of the listeners brought back again and

107
00:14:00,420 --> 00:14:09,060
again to these and other good things. On top of that, listening is kind of a special interaction

108
00:14:09,060 --> 00:14:16,180
because the listening lies anyway, because you're forced to stay still. Those of you listening at

109
00:14:16,180 --> 00:14:21,540
home, there's something lacking here. You're still listening to the dhamma potentially, but you have

110
00:14:21,540 --> 00:14:30,020
the ability to get up or talk to other people in the room, make fun of me, check Facebook,

111
00:14:32,660 --> 00:14:36,660
make comments and so on because we can't see or hear you. But when you're sitting in a,

112
00:14:37,300 --> 00:14:40,660
there's something about sitting in front of a teacher, listening to them teach,

113
00:14:41,860 --> 00:14:48,260
that makes it a meditation in itself. If you can't move, you can't even respond. You just have to

114
00:14:48,260 --> 00:14:55,940
receive, receive, receive. As a result, becoming enlightened, when listening to the dhamma talk is a

115
00:14:55,940 --> 00:15:02,100
very viable thing, becoming so dependent, there's no reason to doubt that this was possible,

116
00:15:02,580 --> 00:15:06,580
that the Buddha can teach and people would become enlightened while they were sitting there.

117
00:15:08,020 --> 00:15:11,860
Sometimes all they needed was a little nudge and the Buddha was very good at nudging people

118
00:15:11,860 --> 00:15:22,100
in the right direction. So this is why we encourage listening to talks. That's why we encourage

119
00:15:22,100 --> 00:15:27,620
going to see people who can teach you the dhamma. That's why we encourage undertaking courses

120
00:15:27,620 --> 00:15:33,780
under a teacher as opposed to just reading about them. But even reading, you know, there's something

121
00:15:33,780 --> 00:15:40,580
just about hearing the truth, having someone teach you good things or reading or learning about

122
00:15:40,580 --> 00:15:47,460
good thing. So the Buddha said better than a thousand words that are meaningless is one word

123
00:15:47,460 --> 00:15:55,700
when you hear it that brings you peace. So quite a simple verse, a simple little story.

124
00:15:58,660 --> 00:16:03,780
One other thing you could say about this in regards to the idea of how it can help an angry

125
00:16:03,780 --> 00:16:10,180
person is often we wonder how to deal with conflict and how do I deal when someone's accusing me

126
00:16:10,180 --> 00:16:15,060
of things shouldn't I fight back when someone's attacking me shouldn't I defend myself?

127
00:16:16,340 --> 00:16:21,700
How do I convince someone of my position and so on? I think the key is in this verse, this is

128
00:16:21,700 --> 00:16:28,980
a good point as well that you teach that you say the truth. When people hear the truth, they become

129
00:16:28,980 --> 00:16:34,660
calm. You don't have to argue, you don't have to persuade. You listen and if you're mindful

130
00:16:34,660 --> 00:16:42,100
and your heart is pure and clear, when you speak it's the truth and just hearing the truth comes

131
00:16:42,100 --> 00:16:52,260
people. Arguing doesn't solve things, defeating the other person doesn't create peace.

132
00:16:55,220 --> 00:17:00,500
But hearing the truth creates peace. When you give people the truth that they need to hear,

133
00:17:00,500 --> 00:17:10,180
they're dumb, they're good teachings. It brings peace. That's why mindfulness is so important in

134
00:17:10,180 --> 00:17:16,580
a conversation, mindfulness is so important in our dealings with other people. It's difficult,

135
00:17:16,580 --> 00:17:20,580
but it is something that we should always think about when we're approaching a conversation,

136
00:17:20,580 --> 00:17:26,260
especially one that we know is going to be hostile or fraught with emotion. We should bring

137
00:17:26,260 --> 00:17:33,780
mindfulness to the four, parimukang, satin, gupada, pitua. Bring mindfulness to the four, first off,

138
00:17:33,780 --> 00:18:03,620
and then engage in our activities. Anyway, that's dumbapada verse 82. Thank you all for tuning in.

