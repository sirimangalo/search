1
00:00:00,000 --> 00:00:06,800
Can I use meditation to help deal with constant stress, anxiety, and self-consciousness to

2
00:00:06,800 --> 00:00:12,560
be more courageous and more at peace with myself?

3
00:00:12,560 --> 00:00:17,960
I think I answer a lot of these in my videos on why everyone should meditate, so I'm not

4
00:00:17,960 --> 00:00:22,520
going to go into much detail there if you want an answer to some of these things like

5
00:00:22,520 --> 00:00:26,560
stress, anxiety being at peace with yourself.

6
00:00:26,560 --> 00:00:30,520
I think the obvious and short answer is that yes, that's really what meditation is for,

7
00:00:30,520 --> 00:00:35,560
if you want to find more information about that, you can check out my videos.

8
00:00:35,560 --> 00:00:42,520
One aspect of what you're asking is the self-consciousness and the courage, which is a question

9
00:00:42,520 --> 00:00:49,520
I actually do get a lot, and it has to be answered I think a little bit differently, because

10
00:00:49,520 --> 00:00:53,040
courage does play an important part in Buddhism.

11
00:00:53,040 --> 00:00:57,280
Courage is something that comes from being a moral person, it's something that comes

12
00:00:57,280 --> 00:01:00,200
from being a wise person in the Buddha.

13
00:01:00,200 --> 00:01:05,000
Outline several key elements, they're called the way Sarajidhamma.

14
00:01:05,000 --> 00:01:10,920
They are things that tend to lead to courage, and they're things like morality, if you're

15
00:01:10,920 --> 00:01:13,600
a moral person, then it makes you courageous.

16
00:01:13,600 --> 00:01:15,440
If you're a wise person, it makes you courageous.

17
00:01:15,440 --> 00:01:19,600
If you're an energetic person, it makes you courageous.

18
00:01:19,600 --> 00:01:22,280
There are several there, and there's basically good qualities of the mind.

19
00:01:22,280 --> 00:01:27,720
So the practice of meditation does make you more courageous, but I think the more important

20
00:01:27,720 --> 00:01:34,920
in the long term is to give up your attachment to self, and your thought that you have

21
00:01:34,920 --> 00:01:42,320
to be assertive, and you have to get your way, and you have to be able to stand up for

22
00:01:42,320 --> 00:01:49,360
yourself and so on, because this has to do with the self, and this is not the way of Buddhism.

23
00:01:49,360 --> 00:01:56,720
And Buddhism we're teaching to give up the self so that you tend to accept things more

24
00:01:56,720 --> 00:02:03,360
than trying to be in charge and control, and so on.

25
00:02:03,360 --> 00:02:11,760
The best way to overcome self-consciousness and timidity is to let go of your fear and let

26
00:02:11,760 --> 00:02:14,120
go of your attachment to self.

27
00:02:14,120 --> 00:02:23,680
Let go of the idea that somehow it's important to be something or to be an other people's

28
00:02:23,680 --> 00:02:28,520
esteem, because then your happiness is always dependent on other people, it's dependent

29
00:02:28,520 --> 00:02:33,280
on what they think of you, it's dependent on external stimulus.

30
00:02:33,280 --> 00:02:45,040
If you're afraid that this is looking at things from the outside in, while we do

31
00:02:45,040 --> 00:02:47,760
is change that and look at things from the inside out.

32
00:02:47,760 --> 00:02:52,480
So instead of saying, oh, I'm in front of a large crowd of people, and they're looking

33
00:02:52,480 --> 00:02:57,920
at me and so on, you start from within, you look at how you're experiencing reality.

34
00:02:57,920 --> 00:03:02,120
When you feel scared, you say scared, scared, when you feel butterflies in the stomach,

35
00:03:02,120 --> 00:03:08,040
you say feeling, feeling, feeling, and you just look at that from the very start before

36
00:03:08,040 --> 00:03:10,600
you even feel afraid.

37
00:03:10,600 --> 00:03:15,320
You're looking at the people and you say, do you stop seeing, seeing, you walk out on

38
00:03:15,320 --> 00:03:21,560
stage, walking, walking, when people attack, you start from the point of view of experience

39
00:03:21,560 --> 00:03:26,520
and you let go of the idea that there's any person attacking you.

40
00:03:26,520 --> 00:03:29,880
Once you start from the point of experience hearing them, saying something, seeing them,

41
00:03:29,880 --> 00:03:35,960
feeling the emotions in the body, then you'll be able to take an inside out, an inside

42
00:03:35,960 --> 00:03:42,440
approach from the inside out, so you'll develop your reactions based on what's really

43
00:03:42,440 --> 00:03:45,680
happening, what's really going on in your world.

44
00:03:45,680 --> 00:03:53,640
When you say things to them, it'll be based on rational, scientific almost experience,

45
00:03:53,640 --> 00:03:57,360
so you'll be able to give them wisdom, you'll be able to help them to understand what's

46
00:03:57,360 --> 00:04:03,760
going on, you'll bring things back into focus, so I wouldn't get off track, they're trying

47
00:04:03,760 --> 00:04:10,520
to think that somehow you have to be brave, you have to be, you have to be sure of yourself

48
00:04:10,520 --> 00:04:11,520
and so on.

49
00:04:11,520 --> 00:04:15,680
The sureity comes from letting go of the self, when you become sure of reality, you become

50
00:04:15,680 --> 00:04:24,520
in tune with reality, you don't become some confident person, you become confident based

51
00:04:24,520 --> 00:04:29,560
on knowing the way things are, and the idea that no one can hurt you, you gain this

52
00:04:29,560 --> 00:04:33,080
understanding that no one can hurt you, because there's no you to hurt.

53
00:04:33,080 --> 00:04:43,320
If they hit you, they're hitting the physical, the reality of the physical manifestation

54
00:04:43,320 --> 00:04:46,400
of the body.

55
00:04:46,400 --> 00:04:51,040
If they speak, if they yell at you, then they're just creating sound that's vibrating

56
00:04:51,040 --> 00:04:52,920
at the ear and so on.

57
00:04:52,920 --> 00:05:01,360
So you become immune to all suffering, all difficulty, and you lose all sense of self-consciousness,

58
00:05:01,360 --> 00:05:05,320
like, oh, that was stupid of me to do or so, and you come to see that it rose because

59
00:05:05,320 --> 00:05:10,120
of cause and effect and you say, oh, that arose based on not being mindful, not having

60
00:05:10,120 --> 00:05:14,520
a clear mind, and you see that it was just a cause and effect, you see that it's in the

61
00:05:14,520 --> 00:05:19,640
past and that you can't change it, and you worry more about what's happening right here

62
00:05:19,640 --> 00:05:24,480
and now, because that's where the intervention, where the mind can actually intervene

63
00:05:24,480 --> 00:05:27,320
in the process of creation.

64
00:05:27,320 --> 00:05:30,840
Okay, so I hope that helps.

65
00:05:30,840 --> 00:05:57,920
Please do check out my other videos and wish you all the best in your practice.

