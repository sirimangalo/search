1
00:00:00,000 --> 00:00:06,760
Hello, we are happy to announce the Digital Poly Reader is completed and ready to use

2
00:00:06,760 --> 00:00:08,360
in your web browser.

3
00:00:08,360 --> 00:00:15,840
To start, go to digitalpolyreader.online.

4
00:00:15,840 --> 00:00:20,520
The Digital Poly Reader, originally created by Bantayu Tidalmo, is a tool that includes

5
00:00:20,520 --> 00:00:25,240
the Poly Cannon and Related Scriptures and has multiple interactive dictionaries to facilitate

6
00:00:25,240 --> 00:00:26,480
reading.

7
00:00:26,480 --> 00:00:31,560
It enables immersive study of the tiptica and the poly language at an advanced level.

8
00:00:31,560 --> 00:00:36,760
The DPR is used by Buddhist monks, scholars at universities around the world, and lay people

9
00:00:36,760 --> 00:00:38,680
for study at home.

10
00:00:38,680 --> 00:00:40,520
This is the welcome screen.

11
00:00:40,520 --> 00:00:44,480
Here you'll see the daily Buddha Watchana, which is a daily passage and can be a nice

12
00:00:44,480 --> 00:00:47,800
way to jump right into a text if you want.

13
00:00:47,800 --> 00:00:51,200
This button opens the sidebar.

14
00:00:51,200 --> 00:00:57,000
In the sidebar, you'll see three main tabs, navigation, search, and tools.

15
00:00:57,000 --> 00:01:00,480
In navigation, there are two ways to navigate to text.

16
00:01:00,480 --> 00:01:03,240
There are quick links and the hierarchy.

17
00:01:03,240 --> 00:01:07,080
With quick links, you can type in shorthand notation to find texts.

18
00:01:07,080 --> 00:01:11,120
This info box shows you how to use this notation.

19
00:01:11,120 --> 00:01:17,400
So for example, if we type mn13, it opens up Majimani Kaia13.

20
00:01:17,400 --> 00:01:23,920
You'll notice the cookie notification, you can accept this to remember your preferences.

21
00:01:23,920 --> 00:01:28,960
With the hierarchy, we can explore how the text is divided into sections and subsections.

22
00:01:28,960 --> 00:01:31,920
You can select a set and book.

23
00:01:31,920 --> 00:01:37,760
The MAT buttons allow you to select mola, the root text, a takata, the commentary, or tika,

24
00:01:37,760 --> 00:01:39,920
the sub commentary.

25
00:01:39,920 --> 00:01:42,440
Here's the book we've selected.

26
00:01:42,440 --> 00:01:47,520
If you click on it, the index of the book and all its subsections will be displayed.

27
00:01:47,520 --> 00:01:51,480
If you click this button, all the subsections will be written out in full combined on one

28
00:01:51,480 --> 00:01:59,000
page.

29
00:01:59,000 --> 00:02:04,440
Each of these drop downs contains the subsections of the sections selected above it.

30
00:02:04,440 --> 00:02:08,120
Just like before, you can click these buttons on the right to display all the subsections

31
00:02:08,120 --> 00:02:12,320
of a section combined on one page.

32
00:02:12,320 --> 00:02:19,120
To view the individual section selected at the bottom of the hierarchy, click here.

33
00:02:19,120 --> 00:02:22,160
Here are the history and bookmark menus.

34
00:02:22,160 --> 00:02:30,520
You can press the B key to make bookmarks.

35
00:02:30,520 --> 00:02:36,280
When we click on a word in the text, it sends it to the bottom pane into the DPR analysis.

36
00:02:36,280 --> 00:02:39,480
The analyzed word is broken into parts.

37
00:02:39,480 --> 00:02:45,600
This found in the Polytech Society's Polyd English Dictionary, or PED, are shown in red.

38
00:02:45,600 --> 00:02:49,400
The PED definition is shown in this space below.

39
00:02:49,400 --> 00:02:54,680
The blue words are from the concise Polyd English Dictionary, or CPED.

40
00:02:54,680 --> 00:03:03,680
Words from the dictionary of Poly proper names are linked to with a green end.

41
00:03:03,680 --> 00:03:15,240
Words underneath the PED words indicate multiple definitions of that word.

42
00:03:15,240 --> 00:03:19,120
This word breakdown menu lets you choose different ways to break up the analyzed word so

43
00:03:19,120 --> 00:03:24,480
you can find the breakdown you are looking for.

44
00:03:24,480 --> 00:03:29,440
The C button shows paradigm charts for conjugation of verbs and declension of nouns and

45
00:03:29,440 --> 00:03:31,120
adjectives.

46
00:03:31,120 --> 00:03:36,920
The analyzed word will appear red in the chart if it is in that paradigm.

47
00:03:36,920 --> 00:03:43,360
The pencil button allows you to edit the spelling of the analyzed word.

48
00:03:43,360 --> 00:03:50,000
These arrows go to the next or previous word alphabetically in the PED.

49
00:03:50,000 --> 00:03:54,600
History shows the words you have looked up.

50
00:03:54,600 --> 00:04:00,960
These side buttons open up this dictionary pane, a conversion or transliteration tool, a text

51
00:04:00,960 --> 00:04:08,040
pad, a translation tool, and the conjugator.

52
00:04:08,040 --> 00:04:11,240
This button opens up the context menu.

53
00:04:11,240 --> 00:04:14,800
The context menu gives you quick access to various features.

54
00:04:14,800 --> 00:04:20,400
For example, you can go to the next or previous section of text with these arrows.

55
00:04:20,400 --> 00:04:23,720
This goes to the book's index.

56
00:04:23,720 --> 00:04:28,480
The MAT buttons go to the root text and commentaries.

57
00:04:28,480 --> 00:04:32,520
You can switch between the tie and Myanmar slash Burmese tippeticas with the capital

58
00:04:32,520 --> 00:04:35,240
M and T buttons.

59
00:04:35,240 --> 00:04:41,320
Notice variant readings are shown in the text which can be toggled on and off in the settings.

60
00:04:41,320 --> 00:04:45,520
These icons open side by side English translations of the text.

61
00:04:45,520 --> 00:04:53,520
If you click on these diamonds, it sends a permalink to the clipboard.

62
00:04:53,520 --> 00:04:57,600
When you go to the permalink, the chosen portion of text will show a green diamond next

63
00:04:57,600 --> 00:05:01,800
to it.

64
00:05:01,800 --> 00:05:05,760
In the search tab, you can search for words in the texts.

65
00:05:05,760 --> 00:05:12,760
The search box accepts words typed in Balthuous or Unicode to give letters die critic marks.

66
00:05:12,760 --> 00:05:18,880
You can look at this info box for help with Balthuous.

67
00:05:18,880 --> 00:05:26,920
For example, if you type period M, it writes a nigahita or the M with a dot under it.

68
00:05:26,920 --> 00:05:33,440
If you type two A's in a row, it writes a long A or the A with a bar over it.

69
00:05:33,440 --> 00:05:40,040
When you click search, you can stop the search with this red button.

70
00:05:40,040 --> 00:05:44,160
You can click on different results of the search to see what texts those terms are found

71
00:05:44,160 --> 00:05:47,560
in.

72
00:05:47,560 --> 00:05:54,920
For our Xbox, let's you use regular expressions to search for patterns of text.

73
00:05:54,920 --> 00:05:58,760
This drop down allows you to choose where you want your search to take place, be it within

74
00:05:58,760 --> 00:06:05,080
multiple sets, books in a set, a single book or part of a book.

75
00:06:05,080 --> 00:06:09,720
The check selects all or deselects all.

76
00:06:09,720 --> 00:06:14,400
The tools tab gives you access to dictionary searches and other resources.

77
00:06:14,400 --> 00:06:18,760
You can type a word into the search box using Balthuous again.

78
00:06:18,760 --> 00:06:22,280
This drop down selects what dictionary you will search.

79
00:06:22,280 --> 00:06:27,000
You can also use the DPR analysis tool here.

80
00:06:27,000 --> 00:06:35,440
Words not found in any dictionary will appear black in the DPR analysis.

81
00:06:35,440 --> 00:06:41,240
The advanced options include Reg X again and a fuzzy search.

82
00:06:41,240 --> 00:06:45,360
When your search term does not appear in the selected dictionary, you'll get a few suggestions

83
00:06:45,360 --> 00:06:48,360
of what you may have meant.

84
00:06:48,360 --> 00:06:52,920
When you click fuzzy search, you get many more suggestions and approximations of your search

85
00:06:52,920 --> 00:06:56,600
term.

86
00:06:56,600 --> 00:07:11,200
Full text allows you to search for a term that appears within dictionary definition entries.

87
00:07:11,200 --> 00:07:21,200
And start of word only allows you to search for all words that start with your term.

88
00:07:21,200 --> 00:07:26,880
Poly language resources includes useful links.

89
00:07:26,880 --> 00:07:30,080
Then we have the buttons at the top of the sidebar.

90
00:07:30,080 --> 00:07:34,680
This button brings you to the welcome screen.

91
00:07:34,680 --> 00:07:38,520
This button allows you to install the DPR for offline use.

92
00:07:38,520 --> 00:07:41,920
Full Chrome is recommended for offline use.

93
00:07:41,920 --> 00:07:45,320
Select all the components you wish to download and click OK.

94
00:07:45,320 --> 00:07:48,320
You will see a progress bar begin in the sidebar.

95
00:07:48,320 --> 00:07:51,920
When it's finished, you will be able to use the downloaded components without a network

96
00:07:51,920 --> 00:07:52,920
connection.

97
00:07:52,920 --> 00:07:58,320
You may also install the DPR as an app by clicking here.

98
00:07:58,320 --> 00:08:06,840
This will add a shortcut to your device as well.

99
00:08:06,840 --> 00:08:10,760
The help button will bring you to this video.

100
00:08:10,760 --> 00:08:13,040
This button will open up preferences.

101
00:08:13,040 --> 00:08:17,080
Here you will see general settings, layout and text displays.

102
00:08:17,080 --> 00:08:25,160
In text script, you can choose to display the text in Roman, Thai, David Nagari, Burmese,

103
00:08:25,160 --> 00:08:33,280
Sinhala, Bongla or Telugu script.

104
00:08:33,280 --> 00:08:36,800
And this button collapses the sidebar.

105
00:08:36,800 --> 00:08:43,360
Press the question marquee for a list of keyboard shortcuts.

106
00:08:43,360 --> 00:08:48,280
Press the feedback button if you'd like to contribute comments, criticisms, questions,

107
00:08:48,280 --> 00:08:51,440
bugs, or anything else you'd like to share.

108
00:08:51,440 --> 00:08:57,120
You may also email feedback to digitalpolyreader at gmail.com.

109
00:08:57,120 --> 00:09:00,480
Thank you for your interest in the digital polyreader.

110
00:09:00,480 --> 00:09:04,200
Thank you to Bantayutadamo and all the hardworking volunteers who have brought us this

111
00:09:04,200 --> 00:09:05,520
tool.

112
00:09:05,520 --> 00:09:06,520
May all beings be happy.

