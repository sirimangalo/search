1
00:00:00,000 --> 00:00:10,000
Hello, and welcome to our evening broadcast.

2
00:00:10,000 --> 00:00:26,000
Welcome to everyone.

3
00:00:26,000 --> 00:00:41,000
So this year, I've been asked to get invited to give the talk at this TEDx conference in Hamilton.

4
00:00:41,000 --> 00:00:47,000
Last year, I signed up for it, thinking, oh, wow, that would be something.

5
00:00:47,000 --> 00:00:51,000
Ended up dropping out, realizing it wasn't really for me.

6
00:00:51,000 --> 00:01:01,000
This year, they actually sought me out and invited me.

7
00:01:01,000 --> 00:01:11,000
When I wrote a speech, the talk sent it to them and then decided it was really for me.

8
00:01:11,000 --> 00:01:30,000
The problem is, the whole idea of the conference is someone outside of the realm of the search of things that I normally talk about.

9
00:01:30,000 --> 00:01:37,000
They don't want me to come on stage and tell them a story from the demo pod there or something.

10
00:01:37,000 --> 00:01:44,000
Talking about Buddhism, it has to be a little bit different nature.

11
00:01:44,000 --> 00:01:51,000
After sending it to them, I said to them, look, what I'm really comfortable with is teaching meditation.

12
00:01:51,000 --> 00:01:57,000
I scrapped that talk and I sent them another one today.

13
00:01:57,000 --> 00:02:07,000
Today, I wrote a talk about meditation.

14
00:02:07,000 --> 00:02:16,000
I think the title was meditation deconstructed.

15
00:02:16,000 --> 00:02:45,000
The point is the idea was to describe meditation from a secular point of view or to give an explanation of meditation beyond simply the spiritual or religious description or point of view.

16
00:02:45,000 --> 00:02:58,000
To be able to actually explain how meditation works, why it works, in sort of scientific terms.

17
00:02:58,000 --> 00:03:11,000
It's funny how the closest we get to this normally is studying meditation scientifically, meaning to do these trials.

18
00:03:11,000 --> 00:03:16,000
They've actually done clinical trials on people who practice meditation.

19
00:03:16,000 --> 00:03:18,000
There's no meditation with this mysterious thing.

20
00:03:18,000 --> 00:03:21,000
We don't know how it works, but we'll look at this.

21
00:03:21,000 --> 00:03:25,000
We have results to show that it works.

22
00:03:25,000 --> 00:03:38,000
I don't think it's quite so common to actually explain why it works or what it does or what it is.

23
00:03:38,000 --> 00:03:49,000
It's kind of this feeling that meditation is somehow this mystical or spiritual practice when in fact it's quite simple how it works.

24
00:03:49,000 --> 00:03:56,000
There's two concepts that we have to understand about meditation.

25
00:03:56,000 --> 00:04:02,000
First, that it's a training.

26
00:04:02,000 --> 00:04:08,000
I think for most of us this should be fairly clear, but for someone who's never really practiced meditation,

27
00:04:08,000 --> 00:04:13,000
I think it's quite common to think of it as in sort of magical terms.

28
00:04:13,000 --> 00:04:16,000
Like, okay, I'll sit down and something will happen to me.

29
00:04:16,000 --> 00:04:21,000
I'll sit down and see how often waiting for these special things to happen to me.

30
00:04:21,000 --> 00:04:25,000
I'm doing it. Who has networking?

31
00:04:25,000 --> 00:04:33,000
When it's going to start working, that's not how it works.

32
00:04:33,000 --> 00:04:36,000
Meditation is a training.

33
00:04:36,000 --> 00:04:38,000
It's quite simple.

34
00:04:38,000 --> 00:04:44,000
You are cultivating something.

35
00:04:44,000 --> 00:04:46,000
Something you have to work at.

36
00:04:46,000 --> 00:04:49,000
Something when you start out you're not very good at.

37
00:04:49,000 --> 00:04:55,000
It's something that takes work. It's challenging.

38
00:04:55,000 --> 00:05:01,000
Something that provides you with skills, tools.

39
00:05:01,000 --> 00:05:05,000
It changes in a sense. It changes in a sense.

40
00:05:05,000 --> 00:05:17,000
The second and related point that we have to understand is meditation deals with what we generally call habits.

41
00:05:17,000 --> 00:05:22,000
A lot of meditation is just about cultivating new habits.

42
00:05:22,000 --> 00:05:25,000
Specifically the habits of calm.

43
00:05:25,000 --> 00:05:32,000
We're trying to cultivate the ability to stay calm, which means

44
00:05:32,000 --> 00:05:36,000
what that means really is to cultivate that as a habit.

45
00:05:36,000 --> 00:05:38,000
Because that's how the mind works.

46
00:05:38,000 --> 00:05:43,000
The mind doesn't work in terms of switches, or suddenly I'll turn on the calm switch.

47
00:05:43,000 --> 00:05:47,000
You try to become, well, you've got lots of other habits.

48
00:05:47,000 --> 00:05:50,000
Your mind has other ideas.

49
00:05:50,000 --> 00:05:53,000
This is what your face up against.

50
00:05:53,000 --> 00:05:58,000
My meditation is quite challenging because we've got lots of bad habits.

51
00:05:58,000 --> 00:06:05,000
Habits that are contrary to the habit that we're trying to cultivate in meditation.

52
00:06:05,000 --> 00:06:13,000
So in terms of, to understand this clearly, you have to separate meditation to two types.

53
00:06:13,000 --> 00:06:18,000
There's tranquility meditation, of course, inside meditation.

54
00:06:18,000 --> 00:06:24,000
So tranquility meditation is trying to cultivate the habit of calm.

55
00:06:24,000 --> 00:06:34,000
We're trying to practice staying calm or being calm repeatedly.

56
00:06:34,000 --> 00:06:38,000
So that eventually it's something you can slip into.

57
00:06:38,000 --> 00:06:44,000
It becomes more like a switch that you can just turn on because you've developed that habit

58
00:06:44,000 --> 00:06:53,000
and when you incline in that direction, the power of your training causes you down.

59
00:06:53,000 --> 00:07:00,000
It allows you to enter into peaceful state.

60
00:07:00,000 --> 00:07:12,000
And the practice of insight, the other one where you're trying to cultivate the habit.

61
00:07:12,000 --> 00:07:18,000
Well, you're trying to cultivate a habit, but an easier way of understanding it is you're trying to understand your habits.

62
00:07:18,000 --> 00:07:26,000
Rather than trying to change them specifically or directly, you're trying to study them.

63
00:07:26,000 --> 00:07:28,000
You're studying your own mind.

64
00:07:28,000 --> 00:07:35,000
The idea being that when you study and learn about all your bad habits, even bad and good habits,

65
00:07:35,000 --> 00:07:38,000
you start to change naturally.

66
00:07:38,000 --> 00:07:48,000
You'll be better informed and better aware of the sorts of things that become bad habits and thereby cause you suffering.

67
00:07:48,000 --> 00:07:54,000
And that which leads to good habits and happiness.

68
00:07:54,000 --> 00:08:02,000
So they're similar in some ways, and they're different in others.

69
00:08:02,000 --> 00:08:11,000
Tranquility is much more about cultivating these habits to the exclusion of better habits.

70
00:08:11,000 --> 00:08:19,000
Cultivating calm and more or less ignoring me or avoiding disturbance.

71
00:08:19,000 --> 00:08:24,000
But there are some meditations that incorporate both tranquility and insight.

72
00:08:24,000 --> 00:08:35,000
There's a lot of debate about these two categories, but definitely are meditations that just practice tranquility to the exclusion of other states.

73
00:08:35,000 --> 00:08:42,000
It's trying to keep the mind from getting involved in the problems.

74
00:08:42,000 --> 00:08:48,000
So loving kindness, for example, you cultivate love and that's a habit.

75
00:08:48,000 --> 00:08:51,000
It's a good habit because it calms you down.

76
00:08:51,000 --> 00:08:57,000
That's when you feel angry rather than delving into why you're angry and what it's like to be angry.

77
00:08:57,000 --> 00:09:00,000
You just say, nope, I'm not going to be angry.

78
00:09:00,000 --> 00:09:01,000
I'm going to love this person.

79
00:09:01,000 --> 00:09:03,000
I love these people.

80
00:09:03,000 --> 00:09:06,000
You're going to have friendliness toward them.

81
00:09:06,000 --> 00:09:15,000
It's called the suppression by opposites.

82
00:09:15,000 --> 00:09:19,000
And so you suppress the bad feelings.

83
00:09:19,000 --> 00:09:24,000
And you become calm yourself.

84
00:09:24,000 --> 00:09:31,000
With insight meditation, instead you, as you know, you try to learn about the habits.

85
00:09:31,000 --> 00:09:35,000
You're habit is simply to try to be objective.

86
00:09:35,000 --> 00:09:39,000
So you spend your time focusing on good habits.

87
00:09:39,000 --> 00:09:46,000
You put aside the idea and the preconception of what is good and bad and right and wrong.

88
00:09:46,000 --> 00:09:54,000
And you try to figure out, you try to learn objectively what is right and wrong.

89
00:09:54,000 --> 00:09:58,000
Good and bad.

90
00:09:58,000 --> 00:10:05,000
So this is the practice that we teach that we practice here.

91
00:10:05,000 --> 00:10:09,000
It's a little bit disappointing.

92
00:10:09,000 --> 00:10:16,000
No matter how hard we try, we come to meditation with this preconception that it should be pleasant and peaceful.

93
00:10:16,000 --> 00:10:19,000
But insight meditation isn't always pleasant or peaceful.

94
00:10:19,000 --> 00:10:22,000
In fact, it's meant to be challenging.

95
00:10:22,000 --> 00:10:24,000
It's meant to challenge you.

96
00:10:24,000 --> 00:10:33,000
It's meant to try your patience and push you to be more patient.

97
00:10:33,000 --> 00:10:39,000
It's meant to more or less force you into being patient and objective.

98
00:10:39,000 --> 00:10:42,000
Because if you're not, it's quite unpleasant.

99
00:10:42,000 --> 00:10:48,000
You're having to deal with all of the negative mind states.

100
00:10:48,000 --> 00:10:51,000
All of the things that normally cause you suffering,

101
00:10:51,000 --> 00:11:06,000
that cause you stress that make you react.

102
00:11:06,000 --> 00:11:10,000
But the simple idea is the way insight meditation works.

103
00:11:10,000 --> 00:11:20,000
To put it simply is by looking, by understanding, by watching all of the many inclinations of the mind,

104
00:11:20,000 --> 00:11:22,000
you're able to start them out.

105
00:11:22,000 --> 00:11:43,000
You're able to see how this one needs me to suffering this one needs me to happen.

106
00:11:43,000 --> 00:11:47,000
The problem with insight meditation, particularly,

107
00:11:47,000 --> 00:11:55,000
is that you can't directly observe your habits.

108
00:11:55,000 --> 00:12:04,000
I think important in helping understand the format that meditation takes,

109
00:12:04,000 --> 00:12:06,000
that insight meditation takes.

110
00:12:06,000 --> 00:12:10,000
For example, you take our tradition where we focus on the stomach.

111
00:12:10,000 --> 00:12:13,000
This is our basic objective of observation.

112
00:12:13,000 --> 00:12:16,000
Why are we focusing on the stomach?

113
00:12:16,000 --> 00:12:20,000
When we do walking meditation, what's the point?

114
00:12:20,000 --> 00:12:27,000
It can be difficult to connect this practice with the practice of insight,

115
00:12:27,000 --> 00:12:33,000
trying to understand your mind, why are we focused on the body?

116
00:12:33,000 --> 00:12:53,000
The general tradition is to compare the quest or the task of trying to chase the mind,

117
00:12:53,000 --> 00:12:57,000
trying to understand the habits of the mind.

118
00:12:57,000 --> 00:13:04,000
It's similar to a hunter trying to chase a wild animal through the forest.

119
00:13:04,000 --> 00:13:07,000
The hunters don't do this.

120
00:13:07,000 --> 00:13:10,000
We have this problem in meditation because it's similar.

121
00:13:10,000 --> 00:13:11,000
You try to chase the mind.

122
00:13:11,000 --> 00:13:16,000
You'll never catch it too fast.

123
00:13:16,000 --> 00:13:19,000
It's very difficult to catch the mind.

124
00:13:19,000 --> 00:13:22,000
When you're going to do sit down in mind,

125
00:13:22,000 --> 00:13:27,000
where is my mind? Not an easy thing.

126
00:13:27,000 --> 00:13:32,000
A good hunter, what they do is they won't go running through the forest.

127
00:13:32,000 --> 00:13:36,000
What they'll do is they'll go to where they know the animals will go.

128
00:13:36,000 --> 00:13:40,000
They'll go to the water hole, or they'll go under a fruit tree,

129
00:13:40,000 --> 00:13:48,000
or somewhere where there's a habitual resting ground or feeding ground of the animal.

130
00:13:48,000 --> 00:13:53,000
This is what we do.

131
00:13:53,000 --> 00:13:54,000
This is why we focus on the body.

132
00:13:54,000 --> 00:13:58,000
We're not interested in the stomach rising and falling.

133
00:13:58,000 --> 00:14:02,000
It's not of great interest to us.

134
00:14:02,000 --> 00:14:09,000
What's more interesting is how the mind interacts with the rising and falling of the stomach.

135
00:14:09,000 --> 00:14:13,000
When we walk, we're not interested so much in the foot.

136
00:14:13,000 --> 00:14:24,000
We're interested in how the mind interacts with it.

137
00:14:24,000 --> 00:14:27,000
The body is sort of this intermediary,

138
00:14:27,000 --> 00:14:30,000
this catalyst that allows us to understand the mind.

139
00:14:30,000 --> 00:14:32,000
When you watch the stomach,

140
00:14:32,000 --> 00:14:36,000
it's always interesting people watch the stomach and say,

141
00:14:36,000 --> 00:14:39,000
I'm not really comfortable with this technique.

142
00:14:39,000 --> 00:14:43,000
It's causing all sorts of problems for me.

143
00:14:43,000 --> 00:14:44,000
That's the point.

144
00:14:44,000 --> 00:14:46,000
We want to see that.

145
00:14:46,000 --> 00:14:49,000
We want to watch the mind when it's challenged.

146
00:14:49,000 --> 00:14:52,000
See how the mind deals with.

147
00:14:52,000 --> 00:14:59,000
Really, with things that it's not very clear about or it's not really comfortable with.

148
00:14:59,000 --> 00:15:03,000
The stomach is a thing of why am I focusing on this.

149
00:15:03,000 --> 00:15:08,000
I really know the stomach is not going to be anything interesting there.

150
00:15:08,000 --> 00:15:13,000
But what's interesting is to watch how the mind doesn't really know the stomach

151
00:15:13,000 --> 00:15:15,000
or doesn't really understand how the body works.

152
00:15:15,000 --> 00:15:17,000
It has a lot of misconceptions.

153
00:15:17,000 --> 00:15:19,000
So we try to control the body.

154
00:15:19,000 --> 00:15:22,000
We think that we're in control of the stomach rising and falling.

155
00:15:22,000 --> 00:15:24,000
We try to make it comfortable.

156
00:15:24,000 --> 00:15:27,000
We try to make it stable.

157
00:15:27,000 --> 00:15:30,000
We're really out of touch with how reality works.

158
00:15:30,000 --> 00:15:32,000
That's what we want to see.

159
00:15:32,000 --> 00:15:35,000
That's what we want to learn.

160
00:15:35,000 --> 00:15:38,000
We want to learn how the mind really works.

161
00:15:38,000 --> 00:15:40,000
Also how the body really works.

162
00:15:40,000 --> 00:15:42,000
How reality works.

163
00:15:56,000 --> 00:16:04,000
Because how are the last point that I want to talk about in this talk that I'm just sort of

164
00:16:04,000 --> 00:16:07,000
starting to write?

165
00:16:07,000 --> 00:16:15,000
Because inside meditation, inside meditation changes the way we look at the world.

166
00:16:15,000 --> 00:16:25,000
The big sort of overarching problem that we have is that our whole way of looking at the world.

167
00:16:25,000 --> 00:16:27,000
It's based on concept.

168
00:16:27,000 --> 00:16:30,000
It's based on what we think of things.

169
00:16:30,000 --> 00:16:36,000
For example, everyone that we know we have ideas about.

170
00:16:36,000 --> 00:16:41,000
We think of them as individuals, as entities.

171
00:16:41,000 --> 00:16:43,000
This person is like this.

172
00:16:43,000 --> 00:16:44,000
That person is like that.

173
00:16:44,000 --> 00:16:46,000
This is what I like about this person.

174
00:16:46,000 --> 00:16:48,000
This is what I dislike about them.

175
00:16:48,000 --> 00:16:53,000
We get these ideas in our minds about how things should be about how things are.

176
00:16:53,000 --> 00:17:01,000
But they're all conceptual when our ideas about a person are based on our experiences.

177
00:17:01,000 --> 00:17:04,000
But we never experience the person.

178
00:17:04,000 --> 00:17:09,000
We've never experienced another individual or experience another thing.

179
00:17:09,000 --> 00:17:13,000
When you see this computer in front of you, you're not experiencing the computer.

180
00:17:13,000 --> 00:17:21,000
You're experiencing sight and extrapolating that there's a computer actually in front of you.

181
00:17:21,000 --> 00:17:34,000
It may seem somewhat philosophical, but it's quite important because we're able to gain out to acquire all sorts of conceptual ideas about things.

182
00:17:34,000 --> 00:17:36,000
Things like this computer is mine.

183
00:17:36,000 --> 00:17:41,000
That person is my husband or wife or parent or child.

184
00:17:41,000 --> 00:17:45,000
That person is my friend, my enemy.

185
00:17:45,000 --> 00:17:49,000
Good, bad.

186
00:17:49,000 --> 00:17:52,000
Our expectations come from this.

187
00:17:52,000 --> 00:18:04,000
We cultivate all sorts of expectations and hopes and wishes.

188
00:18:04,000 --> 00:18:09,000
Without really paying much attention to the way things actually work.

189
00:18:09,000 --> 00:18:14,000
When things go not according to our expectations, we suffer.

190
00:18:14,000 --> 00:18:21,000
The shift that comes about is we start to look at the world in terms of experience.

191
00:18:21,000 --> 00:18:25,000
We start to see that well.

192
00:18:25,000 --> 00:18:30,000
My suffering has caused me because I have these conceptions about people.

193
00:18:30,000 --> 00:18:38,000
This person didn't act the way I wanted them to act or expected them to act.

194
00:18:38,000 --> 00:18:45,000
I thought it was mine and it disappeared or it was taken or stone.

195
00:18:45,000 --> 00:18:47,000
I can't control them.

196
00:18:47,000 --> 00:18:49,000
I can't keep.

197
00:18:49,000 --> 00:18:52,000
I've lost the things or things have changed.

198
00:18:52,000 --> 00:19:00,000
Even our own body is thinking we're in control of our bodies as entities.

199
00:19:00,000 --> 00:19:15,000
We suffer as a result of these unexpected expectations.

200
00:19:15,000 --> 00:19:20,000
Once we start to see the world in terms of experience, this starts to fade away.

201
00:19:20,000 --> 00:19:27,000
We're able to watch a moment by moment in our experience seeing it arise and see and

202
00:19:27,000 --> 00:19:30,000
realizing it's being able to see through our expectations.

203
00:19:30,000 --> 00:19:38,000
Instead of wanting or wishing for things to be a certain way, we're flexible and adaptive to the way things are.

204
00:19:38,000 --> 00:19:48,000
We see that reality isn't made up of things that we can control or depend upon.

205
00:19:48,000 --> 00:19:51,000
We see how suffering works, how it comes about.

206
00:19:51,000 --> 00:19:56,000
It comes because we want things to be a certain way in another way.

207
00:19:56,000 --> 00:20:08,000
We start to realize that these wants and these desires and conceptions of things are causing us suffering.

208
00:20:08,000 --> 00:20:25,000
We make this shift to look at reality and see the present moment and to learn how to live here and now.

209
00:20:25,000 --> 00:20:32,000
It becomes scientific inquiry.

210
00:20:32,000 --> 00:20:51,000
Science is very much against this idea of subjective reality that you could actually learn something from your own experience.

211
00:20:51,000 --> 00:21:00,000
Because we're so easily deceived by our senses.

212
00:21:00,000 --> 00:21:04,000
It goes back to Descartes, I've talked about this before.

213
00:21:04,000 --> 00:21:08,000
Descartes realized that he could be deceived about pretty much everything.

214
00:21:08,000 --> 00:21:11,000
He was trying to figure out what he couldn't be deceived about.

215
00:21:11,000 --> 00:21:13,000
That's where he came up with this, I think.

216
00:21:13,000 --> 00:21:16,000
Therefore, I am Kirito Echosum.

217
00:21:16,000 --> 00:21:20,000
I can't be deceived about that.

218
00:21:20,000 --> 00:21:22,000
It's a bit more important than it might sound.

219
00:21:22,000 --> 00:21:26,000
I think that's pretty minimal.

220
00:21:26,000 --> 00:21:31,000
But you can't be deceived about what you actually, about actual experience.

221
00:21:31,000 --> 00:21:34,000
If you experience seeing, you're actually seeing.

222
00:21:34,000 --> 00:21:41,000
There is actually seeing what you're seeing is up for debate.

223
00:21:41,000 --> 00:21:45,000
It's up for debate because those things are abstract.

224
00:21:45,000 --> 00:21:47,000
That's where the problem comes from.

225
00:21:47,000 --> 00:21:54,000
You can have a conception of something and reality could be totally different.

226
00:21:54,000 --> 00:22:01,000
Conceptions are static and reality is not static.

227
00:22:01,000 --> 00:22:10,000
But more importantly, when you start to see what's so important about this idea of actually knowing something,

228
00:22:10,000 --> 00:22:12,000
is that you can't be deceived.

229
00:22:12,000 --> 00:22:14,000
I'm suffering.

230
00:22:14,000 --> 00:22:17,000
This is causing me suffering.

231
00:22:17,000 --> 00:22:20,000
When you're angry, you will see the result of anger.

232
00:22:20,000 --> 00:22:24,000
And then one can tell you that that's not the result of anger because you're watching it happen.

233
00:22:24,000 --> 00:22:26,000
Then you watch it happen again and again.

234
00:22:26,000 --> 00:22:30,000
And then you have no doubt that you can't possibly question this.

235
00:22:30,000 --> 00:22:34,000
That anger leads to certain result.

236
00:22:34,000 --> 00:22:39,000
If you want something, you start to learn about wanting.

237
00:22:39,000 --> 00:22:45,000
One thing has certain effects. It changes us in certain ways.

238
00:22:45,000 --> 00:22:50,000
It has this effect on the mind.

239
00:22:50,000 --> 00:22:56,000
When you're clear, when your mind is clear and you see things as they are, it changes the way your mind works.

240
00:22:56,000 --> 00:23:01,000
It changes your reality. It changes your responses.

241
00:23:01,000 --> 00:23:03,000
And you can see that.

242
00:23:03,000 --> 00:23:13,000
You're actually able to make certain observations and come to conclusions without any doubt.

243
00:23:13,000 --> 00:23:20,000
There's more that you can know than when scientists are really willing to admit.

244
00:23:20,000 --> 00:23:27,000
The meditation is actually a process by which you can come to know things.

245
00:23:27,000 --> 00:23:35,000
You come to know the nature of the basis of experiential reality.

246
00:23:35,000 --> 00:23:39,000
You know that seeing is seeing, hearing is hearing.

247
00:23:39,000 --> 00:23:44,000
But then you also see cause and effect. You come to see what leads to suffering, what leads to happiness.

248
00:23:44,000 --> 00:23:48,000
And thereby what is good and what is bad.

249
00:23:48,000 --> 00:23:57,000
And we're often hesitant to think in terms of good and bad or to categorize experiences, right?

250
00:23:57,000 --> 00:24:02,000
Good and bad are so contentious.

251
00:24:02,000 --> 00:24:04,000
But not for a meditator.

252
00:24:04,000 --> 00:24:11,000
There's no need to dogmatize or to see orize about what is good or what is bad.

253
00:24:11,000 --> 00:24:15,000
Because when you practice meditation, you'll see for yourself, this is good and this is bad.

254
00:24:15,000 --> 00:24:19,000
You don't need someone to tell you. You don't have to believe it.

255
00:24:19,000 --> 00:24:25,000
You say, oh, this is bad. You have no question. There's no debate in the mind.

256
00:24:25,000 --> 00:24:29,000
This is bad. Someone could tell you, well, no, it's actually good for something.

257
00:24:29,000 --> 00:24:37,000
And you're very difficult to believe them. You have to ignore what you're seeing in order to believe them because it's clearly bad.

258
00:24:37,000 --> 00:24:41,000
It's causing suffering, it's causing stress, it has no benefit.

259
00:24:41,000 --> 00:24:44,000
And you can see that.

260
00:24:44,000 --> 00:24:46,000
This is in sight meditation.

261
00:24:46,000 --> 00:24:50,000
This is sort of what I'm trying to get at in this talk.

262
00:24:50,000 --> 00:24:54,000
I wrote it out. I have to go over it in the coming days.

263
00:24:54,000 --> 00:24:56,000
But I thought I'd share it with you.

264
00:24:56,000 --> 00:25:07,000
Sort of this idea of trying to understand meditation, not being content with it being some sort of magical practice that somehow benefits me in ways I'm not really clear, but

265
00:25:07,000 --> 00:25:21,000
maybe it seems to work, but to actually understand how it works, why it works, what it does, and how it's not something mysterious or hard to understand.

266
00:25:21,000 --> 00:25:45,000
So there you go. That's the demo for tonight. Thank you all for turning in.

267
00:25:45,000 --> 00:26:09,000
Any questions? I'm happy to take them a comment.

268
00:26:09,000 --> 00:26:33,000
Thank you.

269
00:26:33,000 --> 00:26:57,000
Thank you.

270
00:26:57,000 --> 00:27:21,000
Thank you.

271
00:27:21,000 --> 00:27:45,000
Thank you.

272
00:27:45,000 --> 00:28:11,000
Thank you all for coming out.

273
00:28:11,000 --> 00:28:31,000
Hello. Hello. Hello, and I lose my audio. Hello. How did I lose my audio?

274
00:28:31,000 --> 00:28:47,000
What was the last thing I said? How many questions? How much meditation would you recommend for a beginner?

275
00:28:47,000 --> 00:29:03,000
Some things say to start small and to increase gradually.

276
00:29:03,000 --> 00:29:27,000
It's easier to cultivate bad habits without feedback, so one good thing about moderating your practice is the ability to step back

277
00:29:27,000 --> 00:29:41,000
and see how it interacts with your life. Ideally you want to do lots and lots of meditation all day, but without a teacher to help you, you just get off track.

278
00:29:41,000 --> 00:29:59,000
That's sort of the criteria in terms of answering this question. For most people you don't want to do too much meditation because you won't be meditating properly, so it's sort of an exploratory practice.

279
00:29:59,000 --> 00:30:13,000
Basically don't push yourself too hard because you'll just cause yourself lots of stress.

280
00:30:13,000 --> 00:30:29,000
We want to do more and do as much as you can basically, but there's a limit on how much you can benefit from before you start to stress becomes too much and it's no longer all that beneficial.

281
00:30:29,000 --> 00:30:43,000
You'll have some instructions, some guidance and are able to deal with some deeper emotions that come out to the states that come out of intensive meditation practice.

282
00:30:43,000 --> 00:30:59,000
There's no, I can't quantify, just give you some guidelines in that sense. Don't push yourself too hard. It's not something to be afraid of, it's just to be aware that it's possible to meditate wrong.

283
00:30:59,000 --> 00:31:15,000
Doing moderate amounts, better allows you to sort of gauge what you're getting out of it, what it's doing to you, that kind of thing.

284
00:31:15,000 --> 00:31:31,000
Can you explain what it is when you are always meditating? This occurs most of my day. It's when I log my meditation hours and end up not logging them in because it would be always.

285
00:31:31,000 --> 00:31:47,000
I suppose I describe it as a remaining with self like I'm watching over my entire being for many moments of the day.

286
00:31:47,000 --> 00:32:02,000
The meditation for us is two parts. One is the quality of mind, the sort of thing that you seem to be describing and the other is the actual work that you do to cultivate it.

287
00:32:02,000 --> 00:32:19,000
Everything is always mindful, but in order to become mindful we have techniques and the technique we use is the use of a mantra. Our meditation practice all day would be reminding ourselves when you walk to say to yourself walking,

288
00:32:19,000 --> 00:32:35,000
you eat to say to yourself chewing, chewing, swallowing, and it's an important distinction because the quality of your state is highly subjective.

289
00:32:35,000 --> 00:32:55,000
You can say I'm enlightened or I'm mindful even all the time, but that's how you perceive it. It may very well not be that way. The key and insight meditation is to be completely objective and have a clear mind. It's very difficult to do.

290
00:32:55,000 --> 00:33:19,000
It's very easy to fall into the idea that you reach on to the conception that you're clear of mind when in fact you're not. It can be quite partial and biased and confused, conflating calm with clarity.

291
00:33:19,000 --> 00:33:41,000
So having this technique that keeps you honest and keeps you objective forces you to be objective. When you say to yourself seeing, see, it's not much room for any partiality subjectivity or getting off track. It pushes the mind to see seeing and seeing.

292
00:33:41,000 --> 00:33:50,000
It's quite useful in directing your mind in the right ways that it become more objective.

293
00:33:50,000 --> 00:34:05,000
So we would focus more on cultivating the technique throughout the day rather than trying purposefully to cultivate certain qualities of mind.

294
00:34:05,000 --> 00:34:24,000
We're much more concerned about the actual technique. The qualities are cultivating the qualities that's not really the practice. The qualities come from applying the technique.

295
00:34:24,000 --> 00:34:37,000
It's not just about saying, I'm going to be all, I'm going to be very mindful today and doing that intentionally. That's problematic. It's just a concept and you have this idea of what it means to be mindful.

296
00:34:37,000 --> 00:34:50,000
It's very much associated with the ego and self trying to control, trying to make your mind mindful.

297
00:34:50,000 --> 00:35:01,000
What is the difference between Rupa and Nama? Couldn't we say that there's only Nama as the awareness of the existence of Rupa has done through Nama thinking thinking?

298
00:35:01,000 --> 00:35:22,000
Well, you have to understand, Nama Rupa is a description of the aspects of experience. There's only experience.

299
00:35:22,000 --> 00:35:39,000
A conscious experience is made up of two parts. Up to two parts. Some is just mental, but the physical aspect is a part of the experience.

300
00:35:39,000 --> 00:35:56,000
So, yes, it's all, experience is all mental. It's no question, but that experience has two parts. It has the physical aspect and it generally has two parts. The physical and the mental aspect. So, the physical is, again, this hardness and softness. That's physical.

301
00:35:56,000 --> 00:36:04,000
The mental aspect is the consciousness and also the liking and disliking and so on.

302
00:36:04,000 --> 00:36:27,000
Can I do this audio again? Hello, hello, hello. Did I do this audio? What's going on? Oops. Hello, hello, hello, hello, hello.

303
00:36:27,000 --> 00:36:56,000
I don't know what happened. Am I still there? I mean, it's a problem a second life, baby. Hello, hello.

304
00:36:56,000 --> 00:37:15,000
How's this sound? Hello, hello, hello. Testing, testing.

305
00:37:15,000 --> 00:37:44,000
Alright, well, apologies for that. I don't know what's wrong.

306
00:37:44,000 --> 00:37:53,000
Those of you listening in the audio, just say goodnight. See you all next time.

