1
00:00:00,000 --> 00:00:03,360
Hello, welcome back to Askamunk.

2
00:00:03,360 --> 00:00:07,760
Now I have two questions that we're asked together.

3
00:00:07,760 --> 00:00:18,480
The first question is whether after realizing Nirvana, nibana, which means freedom or release

4
00:00:18,480 --> 00:00:26,280
or emancipation or whatever you want to translate it, is there anything left to do or

5
00:00:26,280 --> 00:00:33,160
is nibana it? I mean is that all you have to do? That's the first question.

6
00:00:33,160 --> 00:00:39,680
So answer this question first. At the answer, it's easy.

7
00:00:39,680 --> 00:00:46,880
Nibana nirvana is the last thing that we ever have to do. Once you realize nirvana,

8
00:00:46,880 --> 00:00:51,240
there's nothing left. There's nothing left that from a Buddhist point of view you need to

9
00:00:51,240 --> 00:01:00,240
do. The point is at that point you're free from suffering. So what does this mean?

10
00:01:00,240 --> 00:01:07,040
Nirvana is the cessation of suffering. This is what the definition of it is. It's possible

11
00:01:07,040 --> 00:01:15,160
for a person to not not easy, but it's possible for a human being to realize freedom

12
00:01:15,160 --> 00:01:22,520
from suffering, to come to be even temporarily, to be free from the arising of stress, the

13
00:01:22,520 --> 00:01:34,280
arising of any impermanent phenomenon that at that point the mind is perfectly free.

14
00:01:34,280 --> 00:01:42,960
It's possible to enter or to touch that state to experience or to see or to realize

15
00:01:42,960 --> 00:01:52,560
that state without being free from suffering, without being that way forever. This is the

16
00:01:52,560 --> 00:01:56,760
first thing to point out. This person we have names for them in the Sota Pandas, someone

17
00:01:56,760 --> 00:02:02,200
who has entered the stream because though they still have to suffer and they still have

18
00:02:02,200 --> 00:02:12,880
more to do, they've already cracked the shell to speak and it's only a matter of time

19
00:02:12,880 --> 00:02:18,760
before they become totally free from suffering. So this is one understanding of Nirvana.

20
00:02:18,760 --> 00:02:24,480
It's an experience that someone has and at that point one's life starts to go in the

21
00:02:24,480 --> 00:02:31,040
direction towards freedom from suffering. But what we mean by Nirvana or Parinibhana

22
00:02:31,040 --> 00:02:39,600
or so on is the complete freedom from suffering. So though a person might still live, they

23
00:02:39,600 --> 00:02:45,720
have no more greed, no more anger, no more delusion. These things can't arise. There's

24
00:02:45,720 --> 00:02:52,560
perfect understanding of reality, of the experiences around oneself. When you see, when

25
00:02:52,560 --> 00:02:56,800
hear, smell, taste, feel, think, there's only the awareness of the object, there's no

26
00:02:56,800 --> 00:03:02,920
attachment to it. So we say this is Nirvana. This comes from realizing this state of

27
00:03:02,920 --> 00:03:06,560
ultimate freedom. When you realize it again and again, you start to see that there's

28
00:03:06,560 --> 00:03:13,880
nothing that lasts, that all of the experiences that we have simply arise and see. There's

29
00:03:13,880 --> 00:03:17,880
nothing lasting, there's nothing permanent, there's nothing stable. And so one loses

30
00:03:17,880 --> 00:03:24,920
all of one's attachment. At that point one isn't subject to future rebirth. There's

31
00:03:24,920 --> 00:03:34,000
this physical reality that we somehow built up that has to last out its lifetime. But

32
00:03:34,000 --> 00:03:42,160
at the end of that cycle, that revolution, at death, there's nothing. So at death, there's

33
00:03:42,160 --> 00:03:51,640
no more arising. And at that point there's freedom from suffering, freedom from impermanence

34
00:03:51,640 --> 00:03:58,880
from the birth from having to come back again. So I think this is clear that the answer

35
00:03:58,880 --> 00:04:04,080
from a Buddhist point, if a doctrinal point of view is quite clear that after that there's

36
00:04:04,080 --> 00:04:09,480
nothing left to do. The Buddhist had got done, got a neon, done is what needs to be done.

37
00:04:09,480 --> 00:04:16,960
There's nothing left to do. There's nothing more here. But I think an important point to

38
00:04:16,960 --> 00:04:24,480
make that is often missed is that you don't somehow fall into this state. This is the

39
00:04:24,480 --> 00:04:31,520
ultimate. The final emancipation, it comes to someone who has had enough, who doesn't

40
00:04:31,520 --> 00:04:37,360
see any benefit in coming back. It doesn't see any benefit in recreating this human

41
00:04:37,360 --> 00:04:43,840
state or any state again and again and again. Most of us are not there. Most of us think

42
00:04:43,840 --> 00:04:47,760
that there's still something to be done. We want this. We want that. We've been taught

43
00:04:47,760 --> 00:04:53,640
that it's good to have a partner, to have a family, to have a job, to help people and

44
00:04:53,640 --> 00:04:59,560
so on. We think that there's somehow some benefit in this. On the other hand, we see that

45
00:04:59,560 --> 00:05:05,160
in ourselves there are certain attachments that we have that are causing us suffering.

46
00:05:05,160 --> 00:05:10,120
This is where we're at. We're at the point where we want to hold on to certain things,

47
00:05:10,120 --> 00:05:16,000
but we want to let go of other certain things. This just shows that we're somewhere along

48
00:05:16,000 --> 00:05:25,080
the path where somewhere between a useless person and an enlightened person. It's up to us

49
00:05:25,080 --> 00:05:30,760
which way we're going to go. If we take the side of letting go, we don't have to let

50
00:05:30,760 --> 00:05:34,160
go of those things we don't want to let go of. We let go of those things that we want

51
00:05:34,160 --> 00:05:39,080
to let go of. This is how the path works. People who are afraid of things like the

52
00:05:39,080 --> 00:05:46,920
Urbana, afraid of letting go, and afraid of practicing meditation because I think if I practice

53
00:05:46,920 --> 00:05:49,960
meditation, I'm going to let go of all these things I love and I love them. I want those

54
00:05:49,960 --> 00:05:54,520
things. I don't want to let go of them. The truth is if you want it, if it's something

55
00:05:54,520 --> 00:06:00,840
you're holding on to, you're never going to let it go. That's the point. What you have

56
00:06:00,840 --> 00:06:06,480
to realize is that the more you understand, the more mature you become, the more you realize

57
00:06:06,480 --> 00:06:12,120
that you're not benefiting anyone or anything by yourself or others, by holding on to

58
00:06:12,120 --> 00:06:19,640
anything, by clinging to others, by wanting things to be a certain way. It's not of a

59
00:06:19,640 --> 00:06:23,960
benefit to you and it's not of a benefit to other people. That true happiness doesn't come

60
00:06:23,960 --> 00:06:29,160
from these things. People when they hear about not coming back, they think, well, the

61
00:06:29,160 --> 00:06:33,840
great thing about Buddhism is that there is coming back. I remember talking with some

62
00:06:33,840 --> 00:06:40,120
with a Catholic woman and she said, oh, it's so great to hear about how in Buddhism you

63
00:06:40,120 --> 00:06:43,920
get a chance to come back because, of course, in the religion she had been brought up

64
00:06:43,920 --> 00:06:47,960
with, that's it. When you die, it's either having her health or whatever, but she was

65
00:06:47,960 --> 00:06:52,520
thinking, wow, you know, to be able to come back and have another chance. So for Westerners,

66
00:06:52,520 --> 00:06:56,440
I think this is common. We think, oh, it's great. I'll be able to come back and try again,

67
00:06:56,440 --> 00:07:02,040
like this, that movie Groundhog Day, you know, until you get it right. And that's really

68
00:07:02,040 --> 00:07:07,760
the point is until you get it right, you're going to keep coming back. But whether this

69
00:07:07,760 --> 00:07:15,200
should be looked at, seen as a good thing or not, is debatable. I mean, ask yourself, don't ask

70
00:07:15,200 --> 00:07:18,200
yourself, how are you feeling now? Because most people think, well, I'm okay and I've

71
00:07:18,200 --> 00:07:22,800
got plans. You know, if I can just this and this and this, there's a chance that I'll

72
00:07:22,800 --> 00:07:30,080
be stable and happy and living a good life. But then, you know, don't ask yourself, don't

73
00:07:30,080 --> 00:07:35,000
look at that. Ask yourself what it's taken to get even to the point that you're at now.

74
00:07:35,000 --> 00:07:39,560
And think of all the lessons you've had to learn. And if it's true that you come back again

75
00:07:39,560 --> 00:07:43,720
and again and again, then you're going to have to learn all of those. If you don't learn

76
00:07:43,720 --> 00:07:49,440
anything now and change anything about this state, you don't somehow increase your level

77
00:07:49,440 --> 00:07:56,360
of maturity in a way that you've never done before, in all your lives, then you're just

78
00:07:56,360 --> 00:07:59,200
going to have to learn these lessons again and go through all of the suffering that

79
00:07:59,200 --> 00:08:05,520
we had to go through as children, as teens, as young adults, as adults, and so on. And

80
00:08:05,520 --> 00:08:11,640
wait until you get old sick and die. I mean, see how that's going to be. And then don't

81
00:08:11,640 --> 00:08:16,160
stop there. Look at the people around you and in the world around us, who's to say next

82
00:08:16,160 --> 00:08:24,320
life, you won't be one of the people who suffer terribly in this life. So it's not

83
00:08:24,320 --> 00:08:28,280
as simple a thing as some people might think, they think, oh great, I'll come back, I'll

84
00:08:28,280 --> 00:08:32,280
get to do this again and I'll do this differently and I'll learn more and so no, it's not

85
00:08:32,280 --> 00:08:38,520
like that. If you're not careful, you know, some lifetimes down the road, it's very possible

86
00:08:38,520 --> 00:08:43,840
that you'll end up in a terrible situation, situation of intense suffering. Why do people

87
00:08:43,840 --> 00:08:51,040
suffer in such horrible ways that they do and who's to say you won't end up like them?

88
00:08:51,040 --> 00:08:58,840
So there is some, we can see some benefit in at least, you know, bringing ourselves

89
00:08:58,840 --> 00:09:04,080
somewhere, somewhat out of this state, to a state where we don't lose all of our memories

90
00:09:04,080 --> 00:09:10,520
every 50 to 100 years, where we're able to keep them and to continue developing. That's

91
00:09:10,520 --> 00:09:19,320
at the very least a good thing to do. And so, you know, the development of the mind shouldn't

92
00:09:19,320 --> 00:09:23,120
scare people. The practice of Buddhism shouldn't be a scary thing that you're going

93
00:09:23,120 --> 00:09:28,720
to let go of things you hold on to. Buddhism is about learning. It's about coming to grow,

94
00:09:28,720 --> 00:09:34,200
is growing up and coming to learn things and understand more about reality. It's not

95
00:09:34,200 --> 00:09:41,960
to brainwash us or to force some kind of detached state. It's to help us to learn what's

96
00:09:41,960 --> 00:09:49,000
causing us suffering. Because all of us can tell if we're not terribly blind, we can

97
00:09:49,000 --> 00:09:55,320
verify that there is a lot of suffering in this world. And the truth of the Buddha's teaching

98
00:09:55,320 --> 00:10:00,680
is that the suffering is unnecessary. We don't have to suffer in this way. The reason

99
00:10:00,680 --> 00:10:05,000
we do is because we don't understand and because we don't understand, we therefore do

100
00:10:05,000 --> 00:10:10,560
things that cause us suffering and that cause other people suffering. So slowly, we learn

101
00:10:10,560 --> 00:10:15,400
how to overcome those things, how to be free from them. And eventually, we learn how

102
00:10:15,400 --> 00:10:21,200
to be free from all suffering, how to never cause suffering for others or for ourselves,

103
00:10:21,200 --> 00:10:27,280
how to act in such a way that is not contradictory to our wishes, where we wish to be

104
00:10:27,280 --> 00:10:32,600
happy, we wish to live in peace. And yet we find ourselves acting and speaking and thinking

105
00:10:32,600 --> 00:10:39,360
in ways that cause us suffering and cause disharmony in the world around us. So this

106
00:10:39,360 --> 00:10:46,560
is the, I think, sort of a detailed answer of why freedom would be the last thing because

107
00:10:46,560 --> 00:10:54,520
once you have no claim, once you have no attachment and no delusion, you will not cause

108
00:10:54,520 --> 00:11:06,240
any suffering for yourself. And as a result, you will not have to come back and there's

109
00:11:06,240 --> 00:11:16,080
no more creating, no more forming, no more building, building up. You realize that there's

110
00:11:16,080 --> 00:11:20,480
nothing worth clinging to. That true happiness doesn't come from the objects of the

111
00:11:20,480 --> 00:11:27,040
sense or any of the objects that are inside of us or in the world around us, true happiness

112
00:11:27,040 --> 00:11:31,520
comes from freedom. And you come to see that this freedom is the ultimate happiness.

113
00:11:31,520 --> 00:11:37,760
So let's answer to that question. The second question, I'm going to have to deal with

114
00:11:37,760 --> 00:11:44,680
it here, so this video is going to be a little bit longer. The second question is whether

115
00:11:44,680 --> 00:11:55,840
karma has physical results only or affects the mind as well. And this is, these questions

116
00:11:55,840 --> 00:12:01,040
are actually fairly easy because they're just simple doctrinal questions. But they're

117
00:12:01,040 --> 00:12:06,800
interesting as well. Karma, first we have to understand that karma is not a thing. It doesn't

118
00:12:06,800 --> 00:12:13,200
exist in the world. You can't show me karma. Karma means action. So technically speaking,

119
00:12:13,200 --> 00:12:18,880
you can show me an action when you know, show me the karma of killing. So you kill something.

120
00:12:18,880 --> 00:12:24,520
There's karma for you. But the problem is that we come to take karma as this substance

121
00:12:24,520 --> 00:12:28,760
that exists like something you're carrying around like a weight over your shoulder or

122
00:12:28,760 --> 00:12:34,360
that's somehow a tattooed on your skin or something like that. That you carry around and

123
00:12:34,360 --> 00:12:40,480
you're just waiting to drop on your head or something. And that's not the case. Karma

124
00:12:40,480 --> 00:12:53,360
is a law. It's a law that says that there is an effect to our actions. And not just

125
00:12:53,360 --> 00:13:00,240
not our actions, actually, to our intentions, to our state of mind that the mind is able

126
00:13:00,240 --> 00:13:07,560
to affect the world around us. It's not a doctrine of determinism. If it were this substance

127
00:13:07,560 --> 00:13:12,440
or this thing that you could measure, then there might be some determinism. But it doesn't

128
00:13:12,440 --> 00:13:18,680
say that. Karma just says that you're, it doesn't say either way that there is a free

129
00:13:18,680 --> 00:13:24,480
will or determinism. That's not the point of karma. Karma says that when there is action,

130
00:13:24,480 --> 00:13:31,840
when you do kill or steal or lie or when, when in your mind, you develop these unwholesome

131
00:13:31,840 --> 00:13:39,040
tendencies to, you know, these ideas for the creation of disharmony and suffering and

132
00:13:39,040 --> 00:13:48,000
stress. Then there isn't the effect. The effect is that stress is created. When you have

133
00:13:48,000 --> 00:13:54,800
the intention to help people as well, there's another sort of stress created. It's not

134
00:13:54,800 --> 00:14:00,560
an unpleasant stress, but it's a stress of sorts that creates pleasure. It's a vibration.

135
00:14:00,560 --> 00:14:06,600
When you, it's like a vibration, when you want to help people, when you have the intention

136
00:14:06,600 --> 00:14:17,560
to do good things, to give, to help, to support, to teach, to even to just be kind. When

137
00:14:17,560 --> 00:14:23,280
you study, when you learn, when you practice meditation, all of these things have an effect.

138
00:14:23,280 --> 00:14:37,240
They change who we are. It's important to understand that aspect of karma. The short

139
00:14:37,240 --> 00:14:46,280
answer your question then is that karma, our actions will affect both the body and the mind.

140
00:14:46,280 --> 00:14:55,120
They're speaking, karma gives rise to experiences. It gives rise to seeing, hearing, smelling,

141
00:14:55,120 --> 00:15:00,520
tasting, feeling, thinking. This is the technical explanation that it's going to give rise

142
00:15:00,520 --> 00:15:07,440
to sensual experience in the future. If you, if you do something and you intend to do something,

143
00:15:07,440 --> 00:15:11,280
that's going to change the experiences that you have. You're not going to see the same

144
00:15:11,280 --> 00:15:15,440
things as if you hadn't done that, right? If you kill someone, you're going to see the

145
00:15:15,440 --> 00:15:21,040
inside of a jail and so on. Hearing, smelling, tasting, feeling, thinking, but from a

146
00:15:21,040 --> 00:15:25,200
technical point of view, that's what happens is there are different experiences that you

147
00:15:25,200 --> 00:15:35,600
have. Those experiences are both physical and mental. All it means is that we are affecting

148
00:15:35,600 --> 00:15:42,720
our destiny, whether this is based on free will or determinism isn't really the question.

149
00:15:42,720 --> 00:15:48,720
The point is that when we do do something, there is a result and so rather than worrying

150
00:15:48,720 --> 00:15:58,200
about such questions, whether it's free will or determinism or what does karma affect?

151
00:15:58,200 --> 00:16:06,440
Is it a physical or a mental thing? We should stop, we should train ourselves to give up

152
00:16:06,440 --> 00:16:11,360
the unful of some tendencies that cause us suffering. In the end, the interesting thing

153
00:16:11,360 --> 00:16:15,840
is that you give up both kinds of karma because you have no intention to create happiness.

154
00:16:15,840 --> 00:16:21,000
I have to create happiness since the sense of happy experiences. You come to see that even

155
00:16:21,000 --> 00:16:26,680
pleasurable experiences are temporary. Even if you create harmony in the world, even if

156
00:16:26,680 --> 00:16:32,880
I were able to with my great karma, create peace and harmony in the world, and with

157
00:16:32,880 --> 00:16:38,360
through some great act of minor series of act, it's temporary and unless there's wisdom

158
00:16:38,360 --> 00:16:43,200
and understanding that allows people to let go, they're just going to ruin it when I'm

159
00:16:43,200 --> 00:16:49,320
gone. For instance, the Buddha is teaching when the Buddha was around, there was a lot

160
00:16:49,320 --> 00:16:57,360
of good in India and it lasted for some time and then India went back and now it's an ordinary

161
00:16:57,360 --> 00:17:03,760
country again. But for some time in India, it was a special place and it was very Buddhist

162
00:17:03,760 --> 00:17:09,640
and there was very little killing and so I, from what we understand.

163
00:17:09,640 --> 00:17:13,760
That's just an example, but the point is that it's impermanent and no matter what good things

164
00:17:13,760 --> 00:17:18,160
you do, even the Buddha's teaching is impermanent. It's not going to last forever and this

165
00:17:18,160 --> 00:17:25,200
is the teaching of a perfectly enlightened Buddha as we understand. So, you know, most

166
00:17:25,200 --> 00:17:32,360
important is to become free and to become free from karma as well, to not have any

167
00:17:32,360 --> 00:17:37,600
attachment and that thing should turn out in a certain way, not to be expecting or have

168
00:17:37,600 --> 00:17:47,880
expectations about the future, to simply be content and in tune with reality, to see reality

169
00:17:47,880 --> 00:17:54,720
for what it is and to be at peace with that and to not cling and to not want and to not

170
00:17:54,720 --> 00:18:03,640
hope and care and worry and so on. But to live one's life in a dynamic way where you

171
00:18:03,640 --> 00:18:11,640
can accept and react and respond appropriately to every situation. Doesn't mean that you

172
00:18:11,640 --> 00:18:17,000
live in one place and do nothing, sit around and do nothing and means that you are able

173
00:18:17,000 --> 00:18:22,000
to live dynamically. You're not a stick in the mud and when it's time to move, can't

174
00:18:22,000 --> 00:18:32,640
move your person, your mind is able to respond appropriately to all experiences and react

175
00:18:32,640 --> 00:18:40,240
without attachment. So, you know, fairly detailed answers to your questions. I hope they

176
00:18:40,240 --> 00:18:56,160
did hit the mark to at least to some extent. So, thanks for the questions. All the best.

