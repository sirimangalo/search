1
00:00:00,000 --> 00:00:07,000
Good evening everyone.

2
00:00:07,000 --> 00:00:19,000
Remember on casting live, March 25th.

3
00:00:19,000 --> 00:00:26,000
Tonight's quote is about the mind.

4
00:00:26,000 --> 00:00:33,000
It's from the Eka-kani-pata, the Book of Ones,

5
00:00:33,000 --> 00:00:39,000
when you are good or any kind.

6
00:00:39,000 --> 00:00:44,000
There's a whole section about the mind, Eka-dhamma.

7
00:00:44,000 --> 00:00:51,000
Nahang-bikhuei, Nahang-bikhuei, Aun-yang-e kah-dhamma-m-pie,

8
00:00:51,000 --> 00:00:55,000
Samonupasani.

9
00:00:55,000 --> 00:01:04,000
I don't see one other, any other dhamma.

10
00:01:04,000 --> 00:01:28,600
which is so, which, when untrained, undeveloped, is a

11
00:01:28,600 --> 00:01:38,080
a company, a company, a company, a consumer, a camera, a car, something that you can or should

12
00:01:38,080 --> 00:01:51,200
or should be able to their eyes, amenable to being done, the sense of being used, the

13
00:01:51,200 --> 00:02:07,800
sense of workable, because the translation of our friend uses, so, something that is undeveloped,

14
00:02:07,800 --> 00:02:19,200
there's nothing else when you neglect it, it's so useless, so harmful, so unwieldy.

15
00:02:19,200 --> 00:02:48,120
But, luckily, in the next verse, now, a company, a company, a company, a company, a company,

16
00:02:48,120 --> 00:02:57,600
I don't say one, the other, dhamma, young yewang bhavitan, that, which, when developed,

17
00:02:57,600 --> 00:03:05,320
a company, a company, a company, is willy, malleable, like malleable, I think it's not

18
00:03:05,320 --> 00:03:15,600
literal, but it gets the idea across, yata yidang bikhavitan, jitang bikhavitan, a company

19
00:03:15,600 --> 00:03:32,160
yawang bikhavitan, the mind bikhavitan, because that is bhavitan, that is developed, bhav

20
00:03:32,160 --> 00:03:44,960
means to be bhavitan means to become more to be to progress, so, bhavitan comes from

21
00:03:44,960 --> 00:03:59,440
a company, young hote, hote is a company, a company used, and it goes on, there's more

22
00:03:59,440 --> 00:04:12,000
there, nahang bikhavitan, so, and yang yewang bhavitan, mahato anataya, samwatati, there's no

23
00:04:12,000 --> 00:04:32,480
one thing that, when undeveloped, works for the, for a great anataya, anataya means,

24
00:04:32,480 --> 00:04:43,040
aataya means purpose or benefit, so, anataya means, not just lack of benefit at harm, really,

25
00:04:43,040 --> 00:04:50,320
leads to so many things that are useless, leads to useless things, great uselessness,

26
00:04:50,320 --> 00:04:59,760
maybe, great, but it's actually more like great, loss in the sense of harm, guess what

27
00:04:59,760 --> 00:05:08,280
that one thing is, the untrained mind, jitang bikhavitan, when the mind is untrained, mahato

28
00:05:08,280 --> 00:05:18,800
anataya samwatati, it works for the, great detriment, really, is what you see, but there's

29
00:05:18,800 --> 00:05:33,240
nothing, when developed, that is, mahato anataya, samwatati goes for the great benefit, great

30
00:05:33,240 --> 00:05:48,720
purpose, a bhato bhata, there's no bhato bhata, bhata mahato bhata, mahato bhata, mahato bhata, mahato

31
00:05:48,720 --> 00:06:03,100
hmm I don't want you to get par to the next one, I didn't understand, the mind

32
00:06:03,100 --> 00:06:11,380
which is developed, which is par to bhata na, it doesn't like the translation.

33
00:06:11,380 --> 00:06:15,740
That's basically the same thing.

34
00:06:15,740 --> 00:06:19,980
Yeah, the rest is pretty much the same.

35
00:06:19,980 --> 00:06:27,900
It just has synonyms for bavita, which really means bavita and baholikha, which is a common

36
00:06:27,900 --> 00:06:37,140
coupling made much of basically, or not made much of.

37
00:06:37,140 --> 00:06:51,100
So the mind is that one thing that is of greatest use and greatest harm.

38
00:06:51,100 --> 00:06:59,500
And it goes back to the first verse of the Dhammapada, which is easy to miss the significant

39
00:06:59,500 --> 00:07:15,980
sense, but it's a minopubangamma, dhamma, minosita, minomaya, all things have mind as their precursor.

40
00:07:15,980 --> 00:07:24,220
Mind as their chief, mind as their maker, all things, I guess better, all things are

41
00:07:24,220 --> 00:07:34,580
preceded by the mind, are ruled by the mind, are formed by the mind.

42
00:07:34,580 --> 00:07:39,460
I think there's some qualifier there because it's not, it's actually, there's a specific

43
00:07:39,460 --> 00:07:46,620
context in terms of good dhammas and bad dhammas, like, in terms of morality and immorality,

44
00:07:46,620 --> 00:07:53,300
goodness and evil, that kind of thing, maybe a specific type of dhamma, but all through

45
00:07:53,300 --> 00:08:01,540
the Buddha's teaching, there runs this undercurrent, sort of the implicit understanding

46
00:08:01,540 --> 00:08:10,580
that the mind is the base, that there is no universal reality outside of the mind.

47
00:08:10,580 --> 00:08:26,420
This is just a characteristic of matter, space doesn't actually exist, it only comes

48
00:08:26,420 --> 00:08:36,780
in the being when there's the concept of matter, it's not the most fundamental aspect

49
00:08:36,780 --> 00:08:49,500
of reality, and more fundamental than that is the experience of the mind, and so the

50
00:08:49,500 --> 00:08:59,620
mind becomes very important because the mind works according to orderly nature and

51
00:08:59,620 --> 00:09:21,780
laws and then follows order and order, there's the order of the mind.

52
00:09:21,780 --> 00:09:34,140
So if you do bad things, bad things follow, if you do good things, good things follow.

53
00:09:34,140 --> 00:09:41,060
We don't think of the mind as all that useful or we don't, we underestimate, let's put

54
00:09:41,060 --> 00:09:47,420
it that way, we underestimate the value of the mind in general, just talking generally

55
00:09:47,420 --> 00:09:56,820
in society, and so when we use these things, these things have become our mind, rather

56
00:09:56,820 --> 00:10:05,060
than remember things, we just put them in our calendar rather than study things, we just

57
00:10:05,060 --> 00:10:13,820
rely on Google whenever we need to know something or Wikipedia, even our daily functioning

58
00:10:13,820 --> 00:10:19,900
rather than organizing things in our mind, we've got them organized on our smartphone.

59
00:10:19,900 --> 00:10:25,060
The smartphone is when you build a smartphone while, it's the most malleable thing

60
00:10:25,060 --> 00:10:31,660
there is, you can swipe and you can touch and you can pinch, there's nothing better than

61
00:10:31,660 --> 00:10:34,860
a smartphone, right?

62
00:10:34,860 --> 00:10:42,900
So we've outsourced a lot of our brain power to some extent anyway.

63
00:10:42,900 --> 00:10:48,500
Some people outsource more than others, I'm just thinking, some people aren't interested

64
00:10:48,500 --> 00:10:53,700
in using their mind, other people are interested, and obviously some people are interested

65
00:10:53,700 --> 00:10:58,100
in developing their mind through the practice of meditation.

66
00:10:58,100 --> 00:11:12,420
Not a lot though, most of us don't, we underestimate the importance of the mind, usefulness

67
00:11:12,420 --> 00:11:18,020
of the mind, and underestimate the power of the mind, and what is actually thinking

68
00:11:18,020 --> 00:11:24,820
here, the kinds of things, what he's thinking, but he's got to be very much a part of

69
00:11:24,820 --> 00:11:36,180
this is the powers of the mind to see the future, to remember past lives, to do all sorts

70
00:11:36,180 --> 00:11:46,820
of crazy things, and to free oneself, the power of the mind to free oneself from suffering,

71
00:11:46,820 --> 00:11:56,140
to become invincible, the power of the mind to let go of all causes for suffering, so

72
00:11:56,140 --> 00:12:08,500
that nothing that is bothersome bothers one.

73
00:12:08,500 --> 00:12:14,780
So our smartphones can be as useful as you like, but they're never going to rival the

74
00:12:14,780 --> 00:12:27,900
mind in any way shape or form, I think that's fairly obvious, that it's a sad thing to

75
00:12:27,900 --> 00:12:38,180
say that our smartphones are on the same level as our minds or our computers or anything

76
00:12:38,180 --> 00:12:53,780
or iPod, iPad, nothing to do with the power of the mind to harm or to help, the mind

77
00:12:53,780 --> 00:13:01,420
is what tortures us, mind is, and part of what he's saying is useless, the mind, the ordinary

78
00:13:01,420 --> 00:13:08,140
mind is useless, what can you do with it, you try and work, you can board, you try and

79
00:13:08,140 --> 00:13:17,100
study, you get distracted, you try and do good things, you get upset and annoyed and sad

80
00:13:17,100 --> 00:13:28,860
and burnt out useless mind, but what he's saying is that, it's just because it's untrained,

81
00:13:28,860 --> 00:13:33,980
it's not, you don't feel bad that your mind is, you don't feel bad about yourself that

82
00:13:33,980 --> 00:13:43,260
your mind is this way or that way, that your mind is unknowable, unwieldy, you just have

83
00:13:43,260 --> 00:13:51,100
to realize, well, it's through neglect and all you have to do is stop neglecting it to start

84
00:13:51,100 --> 00:13:56,300
training it, working on it, doing something with it, you know, like people say, get off

85
00:13:56,300 --> 00:14:02,660
the couch and go exercise, right, it's all you gotta do, except for meditation, it's more

86
00:14:02,660 --> 00:14:11,900
like, get back on that cabbage and exercise, get back on that sitting mat, don't just

87
00:14:11,900 --> 00:14:24,540
do something, sit there, funny things like that, but meditation is a strenuous exercise

88
00:14:24,540 --> 00:14:30,900
for the mind and for the brain I think as well, there's end for the body, there's no

89
00:14:30,900 --> 00:14:38,780
question, meditation is a very strenuous sort of thing, people lose a lot of weight meditating,

90
00:14:38,780 --> 00:14:44,780
they don't need as much either, but it's a lot of sweating, a lot of straining, a lot

91
00:14:44,780 --> 00:14:56,540
of crying, blood sweat and tears, the work that we do to train our minds, it's the most

92
00:14:56,540 --> 00:15:03,020
rewarding work there is, you can train your body, okay, great, so you've got a body that

93
00:15:03,020 --> 00:15:08,020
can do this or do that, they used to take karate, never really saw the point, I was just

94
00:15:08,020 --> 00:15:13,300
in it because I was afraid of getting beat up, but when I really liked about karate

95
00:15:13,300 --> 00:15:25,220
was the mental part and I got disgusted when we were sparring, this was late in the time

96
00:15:25,220 --> 00:15:40,220
when I was close before I quit and the head guy in the dojo and I were sparring, actually

97
00:15:40,220 --> 00:15:46,460
the sense was away, so he and I were sort of leading the class, I think, I was a brown

98
00:15:46,460 --> 00:15:49,980
belt, I don't think I ever deserved the brown belt, but I was just there long enough

99
00:15:49,980 --> 00:16:00,340
instead of eventually they gave it, and we were sparring and I was out of three points,

100
00:16:00,340 --> 00:16:06,140
I got two points, I got, I can't remember, so you got first to three points, I got

101
00:16:06,140 --> 00:16:17,980
two points on them and he freaked out and he just went ballistic on me, I said, Jesus man,

102
00:16:17,980 --> 00:16:24,620
it's just a game and he just freaked out and he won, he actually won the mat and he was

103
00:16:24,620 --> 00:16:35,620
really happy, but I lost all it, I was disgusted by the whole thing, and I think karate

104
00:16:35,620 --> 00:16:44,340
is martial arts can, there's a point to me telling that is that it's all in the mind,

105
00:16:44,340 --> 00:16:51,620
martial arts is a body training, part of it is a body training and it's possible to do martial

106
00:16:51,620 --> 00:16:59,860
arts without proper mental training, so you have to train your mind somehow, you train

107
00:16:59,860 --> 00:17:08,860
your mind to work with your body, but you haven't really trained your mind, and I think

108
00:17:08,860 --> 00:17:13,700
the greatest martial arts, lots and lots of martial arts, teachers and practitioners would

109
00:17:13,700 --> 00:17:19,940
say no, no, no, your mind has to be calm, anyone who freaks out like that or in a place

110
00:17:19,940 --> 00:17:28,020
mind game, it's just awful, yeah, it's a really practicing martial arts, but that's the

111
00:17:28,020 --> 00:17:31,860
point, the difference between training the body and training the mind, someone can be

112
00:17:31,860 --> 00:17:43,020
very much trained in the body, but not have a very well trained mind, not to say, and

113
00:17:43,020 --> 00:17:48,140
I'm not putting down martial arts, in fact, it's probably very good for disciplining the

114
00:17:48,140 --> 00:17:57,060
mind, but it only goes so far, because it's not, it's a body thing, the only training you're

115
00:17:57,060 --> 00:18:01,620
doing in your mind is kind of like a trick, how to keep your mind focused, how to keep

116
00:18:01,620 --> 00:18:10,020
your mind disciplined, that kind of thing, it's not really training, it's not training the

117
00:18:10,020 --> 00:18:17,140
mind in the way that the Buddha talked about, bahavita, Buddha's bahavana is something

118
00:18:17,140 --> 00:18:25,700
quite different, you know, that's a kind of a bahavana, but it's very superficial, you know,

119
00:18:25,700 --> 00:18:32,980
anyone, animals can train their minds to sit still, dogs can do it right, they go on their

120
00:18:32,980 --> 00:18:42,420
point and something, it's not real training, where you train your mind to see things as they

121
00:18:42,420 --> 00:18:50,940
are, you train your mind to be objective, you're never going to angry or upset, you

122
00:18:50,940 --> 00:18:57,620
are totally up a root, the potential, change the nature of your mind in something much

123
00:18:57,620 --> 00:19:03,740
deeper. Anyway, by the way, with martial arts, practitioners must agree that you need to

124
00:19:03,740 --> 00:19:11,460
train your mind as well, mind is most important, oh, that's what our sensei was French,

125
00:19:11,460 --> 00:19:19,380
French Canadian, I don't know if he doesn't anymore, but nice guy, and he used to say all

126
00:19:19,380 --> 00:19:23,860
the time, he said, mind first and the body will follow, it's a really nice guy, he taught

127
00:19:23,860 --> 00:19:30,860
us meditation, he was probably my first meditation teacher, mind first and the body will follow,

128
00:19:30,860 --> 00:19:40,100
he was a good guy, he's a good guy, I've seen him in a long time. Anyway, there you go, that's

129
00:19:40,100 --> 00:19:45,140
the dumb of her tonight, really good quote, where I remember him, there's a Buddha quote,

130
00:19:45,140 --> 00:19:56,420
if ever you heard one, not nothing fake about that, there's the link to the hangout

131
00:19:56,420 --> 00:20:04,820
if anybody wants to come on, posting it at meditation.seriumungala.org, you want to

132
00:20:04,820 --> 00:20:28,220
come on if anybody has questions? I mean, people do send me questions, I think must be

133
00:20:28,220 --> 00:20:36,460
every day, I don't keep track because I don't answer any of them, I guess so many questions.

134
00:20:36,460 --> 00:20:48,340
And you know, often it's, my life is crappy, how can I fix it? And you know, some of

135
00:20:48,340 --> 00:20:57,820
you know my opinion on these questions, it's not really, I don't have the answer, there's

136
00:20:57,820 --> 00:21:08,060
no answer to that. I mean, or this person is not, person asking this question is probably

137
00:21:08,060 --> 00:21:15,580
not going to, I mean they're not asking the right question, in my opinion really the

138
00:21:15,580 --> 00:21:25,540
only right question is how do I meditate? The answer is read my book, but lots of questions.

139
00:21:25,540 --> 00:21:31,700
People are having a difficult time, you know, but that doesn't mean I can put myself

140
00:21:31,700 --> 00:21:38,500
in a position I answer all these questions. I can't, I'm not in a position to help any

141
00:21:38,500 --> 00:21:43,220
of you. My booklets there, if you want to learn how to meditate, that's the answer, that's

142
00:21:43,220 --> 00:21:48,820
the path. I'm not to be arrogant to meditate, that booklet, this booklets just one of

143
00:21:48,820 --> 00:22:00,340
many, but it teaches what I was taught, and I believe that what I was taught works. I

144
00:22:00,340 --> 00:22:13,100
don't believe, I claim that what I was taught is the Buddha's teaching and it's authentic

145
00:22:13,100 --> 00:22:22,660
enough to need you to train your mind and for you from all of life's problems. It's

146
00:22:22,660 --> 00:22:33,740
really that simple. It's not easy, but it's pretty simple. So, you know, a lot of questions

147
00:22:33,740 --> 00:22:51,580
are either too unrelated to meditation or misguided in the sense that they're not being

148
00:22:51,580 --> 00:23:01,340
stressed. I mean, there's a, they're looking for a pill, basically, right? Looking for an answer,

149
00:23:01,340 --> 00:23:05,460
a quick fix. How can I fix my problems? No, you can't fix your problems. You have to

150
00:23:05,460 --> 00:23:11,460
stop trying to fix things. That's the problem. That's one of the problems, one of the

151
00:23:11,460 --> 00:23:22,340
big ones. So, I don't know. I'm just talking because people trying to get a hold of me and

152
00:23:22,340 --> 00:23:31,300
I'm stubbornly holding on to the fact that I'm a Buddhist monk, as my excuse not to have

153
00:23:31,300 --> 00:23:48,740
a 24-7 public presence. Something awesome happened that I'm assuming my, I'm pretty sure

154
00:23:48,740 --> 00:23:56,380
many of you are aware of. Robin, are you out there? You're going to come on here and say

155
00:23:56,380 --> 00:24:21,140
something? I don't know if I should say something. I can say it as well. No, Robin. Well,

156
00:24:21,140 --> 00:24:27,420
as you all know, some of you know, I was dragging my feet on the idea of going back to Asia

157
00:24:27,420 --> 00:24:44,020
this year, last year. But Robin thinks I should go and so she put together an online

158
00:24:44,020 --> 00:24:55,060
campaign to get me a ticket without, well, maybe that'll work. And it didn't even take

159
00:24:55,060 --> 00:25:05,260
24 hours and they've raised over $2,000. I think it was like more than it was actually

160
00:25:05,260 --> 00:25:20,660
trying to raise. So, I guess I got to go to Thailand. And Sri Lanka. And here.

161
00:25:20,660 --> 00:25:42,460
Who is that? I don't know. I don't know. I don't know.

162
00:25:42,460 --> 00:26:02,820
Oh, me, school. I don't know. Hello.

163
00:26:02,820 --> 00:26:27,180
Oh, it's Joanna. Yeah, you're enough to YouTube video before you join the hangout. Yeah,

164
00:26:27,180 --> 00:26:34,140
I'm just getting the hang of it. You're talking about meditation. And I've been really sick.

165
00:26:34,140 --> 00:26:40,220
I mean, I've been sick on and off for like over a month now. But I was, I went to the doctor

166
00:26:40,220 --> 00:26:46,900
and I got, he told me I had pneumonia, which explains a lot. So, and I, that kind of threw

167
00:26:46,900 --> 00:26:53,460
me for a loop. I just, anyway. And I know I should be meditating, but I'm having, I'm really

168
00:26:53,460 --> 00:27:02,300
having a hard time meditating feeling so crappy part of my language. Do you have any suggestions

169
00:27:02,300 --> 00:27:10,260
as to how to meditate when one is not, when one is ill? Well, yeah, I mean, it's still

170
00:27:10,260 --> 00:27:25,220
a reality. So you can still meditate on it. Sickness is actually a good thing to meditate on.

171
00:27:25,220 --> 00:27:37,700
But I don't know what. Okay. What about tomorrow on nothing? Tomorrow, tomorrow, nothing?

172
00:27:37,700 --> 00:27:45,780
Okay, it's asking. Oh, no, you don't have to. But if you do, let me know. Otherwise,

173
00:27:45,780 --> 00:28:04,020
I'll go out. I'll just go out. Let's go out every day. Hi, Robin.

174
00:28:04,020 --> 00:28:16,020
I hear you. Hi, Funte. Can you hear me, okay? Yep. Sorry, I meditate upstairs, not too close

175
00:28:16,020 --> 00:28:29,980
to the computer. So, wow. Yeah, that, that was awesome. We, I put out a small campaign

176
00:28:29,980 --> 00:28:37,100
early this morning, mid, mid morning or so. And within a couple of hours, there was enough

177
00:28:37,100 --> 00:28:46,380
support to bless here within a couple of hours. There was enough support to support your trip

178
00:28:46,380 --> 00:28:52,060
to Thailand in Sri Lanka. So that's awesome. Just wanted to say thank you to everyone who

179
00:28:52,060 --> 00:29:00,380
shared it, contributed to it, wished as well. That's awesome. Thank you, everyone. I can't

180
00:29:00,380 --> 00:29:13,100
actually comment on giving money. So, no, that's why I'm commenting. I'm just saying it. I can't

181
00:29:13,100 --> 00:29:19,580
say thank you. But I can say thank you. Thank you very much, everyone. So when do you think

182
00:29:19,580 --> 00:29:26,620
I'm grateful for the ticket and appreciative of the ticket? That'll be a great thing because

183
00:29:26,620 --> 00:29:41,740
I'll be able to go and promote our monastery to our teacher. There is someone who would like

184
00:29:41,740 --> 00:29:57,740
to join the hangout. I'm just posting the link to him now.

185
00:30:41,740 --> 00:31:01,640
Hello, let's have a

186
00:31:11,740 --> 00:31:18,740
Bontay.

187
00:31:18,740 --> 00:31:20,740
Bontay.

188
00:31:20,740 --> 00:31:22,740
Yeah.

189
00:31:22,740 --> 00:31:26,740
Did you finalize your trip to Manhattan or to New York?

190
00:31:26,740 --> 00:31:29,740
Hello, what's there?

191
00:31:29,740 --> 00:31:32,740
Hello, Robin.

192
00:31:32,740 --> 00:31:35,740
Hello, Abi.

193
00:31:35,740 --> 00:31:37,740
Welcome.

194
00:31:37,740 --> 00:31:38,740
Thank you.

195
00:31:38,740 --> 00:31:40,740
Thank you for your link.

196
00:31:40,740 --> 00:31:47,740
I was finding difficult to join in Hangout.

197
00:31:47,740 --> 00:32:02,740
Hi, I'm going to New York April 15th

198
00:32:02,740 --> 00:32:09,740
and returning.

199
00:32:09,740 --> 00:32:14,740
It's turning April 24th.

200
00:32:14,740 --> 00:32:17,740
What venue will you be?

201
00:32:17,740 --> 00:32:20,740
I don't know.

202
00:32:20,740 --> 00:32:23,740
No, where will you be staying?

203
00:32:23,740 --> 00:32:26,740
I think that this Sri Lankan temple

204
00:32:26,740 --> 00:32:28,740
in Queens?

205
00:32:28,740 --> 00:32:33,740
I don't know.

206
00:32:33,740 --> 00:32:35,740
Are you going to let us know when you know

207
00:32:35,740 --> 00:32:38,740
or you don't want people to visit you?

208
00:32:38,740 --> 00:32:39,740
Absolutely.

209
00:32:39,740 --> 00:32:45,740
You know, I'm not sure if they're waiting for me or not.

210
00:32:45,740 --> 00:32:47,740
I'm, you know, this is finals.

211
00:32:47,740 --> 00:32:51,740
I'm writing an essay on the Lotus Sutra.

212
00:32:51,740 --> 00:32:54,740
I have to study for my peace studies exam.

213
00:32:54,740 --> 00:33:00,740
I've got a lot and practice coming that I have to finish for Monday.

214
00:33:00,740 --> 00:33:02,740
And then we've got the actual peace symposium

215
00:33:02,740 --> 00:33:05,740
that we have to organize for April 5th.

216
00:33:05,740 --> 00:33:09,740
And then right in that, you know, I'm going to be busy up

217
00:33:09,740 --> 00:33:13,740
until I step on that plane.

218
00:33:13,740 --> 00:33:15,740
Pretty much.

219
00:33:15,740 --> 00:33:17,740
You're going to land in JFK.

220
00:33:17,740 --> 00:33:18,740
Look, Wardia.

221
00:33:18,740 --> 00:33:37,740
JFK.

222
00:33:37,740 --> 00:33:43,740
You said Sri Lankan temple?

223
00:33:43,740 --> 00:33:44,740
I'm sorry?

224
00:33:44,740 --> 00:33:48,740
You said a Sri Lankan temple?

225
00:33:48,740 --> 00:33:49,740
Yes.

226
00:33:49,740 --> 00:33:54,740
They mentioned that.

227
00:33:54,740 --> 00:33:59,740
I wonder if there's a lot of them in New York?

228
00:33:59,740 --> 00:34:03,740
I think that's true.

229
00:34:03,740 --> 00:34:13,740
I mean, I'll let you guys know if I know when I know.

230
00:34:13,740 --> 00:34:18,740
But I mean, I'll probably put something on Facebook.

231
00:34:18,740 --> 00:34:26,740
But I haven't earned anything yet.

232
00:34:26,740 --> 00:34:32,740
I'm sure I'll have internet welcome there so you can email me or you can talk on.

233
00:34:32,740 --> 00:34:38,740
You can let you know, Wardia.

234
00:34:38,740 --> 00:34:40,740
That's cool.

235
00:34:40,740 --> 00:34:57,740
Does anybody have a question?

236
00:34:57,740 --> 00:35:00,740
Getting back to when you're sick and you're meditating,

237
00:35:00,740 --> 00:35:03,740
should you do something differently?

238
00:35:03,740 --> 00:35:06,740
Well, you can do lying meditation.

239
00:35:06,740 --> 00:35:12,740
I mean, I want to do too much walking meditation, although if you can do it, it's good for the body.

240
00:35:12,740 --> 00:35:14,740
You shouldn't push too hard.

241
00:35:14,740 --> 00:35:18,740
It's not easy.

242
00:35:18,740 --> 00:35:21,740
Yeah, well, I completely didn't do it yesterday.

243
00:35:21,740 --> 00:35:23,740
I felt kind of bad.

244
00:35:23,740 --> 00:35:29,740
I felt bad because my emotions were getting the better of me today.

245
00:35:29,740 --> 00:35:33,740
It was probably because I didn't meditate.

246
00:35:33,740 --> 00:35:37,740
And then today I was going to skip it because I just got lethargic.

247
00:35:37,740 --> 00:35:41,740
But I think I'm going to just get off and then and just do the lies.

248
00:35:41,740 --> 00:35:51,740
So the lies, you lay in the just lay down with your palms up.

249
00:35:51,740 --> 00:35:58,740
Anyway, that you like just like comfortably in mind.

250
00:35:58,740 --> 00:36:03,740
Rising, falling, watch the stomach.

251
00:36:03,740 --> 00:36:07,740
Okay, thank you.

252
00:36:07,740 --> 00:36:08,740
Good night.

253
00:36:08,740 --> 00:36:09,740
Welcome.

254
00:36:09,740 --> 00:36:10,740
Good night.

255
00:36:10,740 --> 00:36:11,740
Hello.

256
00:36:11,740 --> 00:36:18,740
Hello.

257
00:36:18,740 --> 00:36:21,740
Hello.

258
00:36:21,740 --> 00:36:28,740
Hello, went here.

259
00:36:28,740 --> 00:36:29,740
Yeah.

260
00:36:29,740 --> 00:36:33,740
Can I call you, call you on a telephone?

261
00:36:33,740 --> 00:36:38,740
What's it about?

262
00:36:38,740 --> 00:36:46,740
The first practice, what if you got this in practice?

263
00:36:46,740 --> 00:36:52,740
Again, the meditation.

264
00:36:52,740 --> 00:36:55,740
Yes, went in meditation.

265
00:36:55,740 --> 00:36:56,740
Sure.

266
00:36:56,740 --> 00:36:58,740
Yeah, you can call me.

267
00:36:58,740 --> 00:37:04,740
It's about meditation.

268
00:37:04,740 --> 00:37:07,740
I am having the problem for the internet connection.

269
00:37:07,740 --> 00:37:12,740
That's why I'm choosing the option for a telephone.

270
00:37:12,740 --> 00:37:15,740
Okay.

271
00:37:15,740 --> 00:37:18,740
Is there any contact numbers on the website?

272
00:37:18,740 --> 00:37:19,740
There is nothing.

273
00:37:19,740 --> 00:37:23,740
I have posted a email on the website also.

274
00:37:23,740 --> 00:37:26,740
Hmm.

275
00:37:26,740 --> 00:37:29,740
Robin, do you have a number?

276
00:37:29,740 --> 00:37:32,740
Or the monastery?

277
00:37:32,740 --> 00:37:34,740
Yes, I think so.

278
00:37:34,740 --> 00:37:42,740
Can you send a turn?

279
00:37:42,740 --> 00:37:45,740
What to do?

280
00:37:45,740 --> 00:37:51,740
What to do to remove the fear from your heart, from someone's heart?

281
00:37:51,740 --> 00:37:55,740
Have you read my booklet on how to meditate?

282
00:37:55,740 --> 00:38:01,740
Yes, I have read it.

283
00:38:01,740 --> 00:38:11,740
Well, that's the practice that I recommend.

284
00:38:11,740 --> 00:38:16,740
I guess I can say it's not about removing the fear from your heart.

285
00:38:16,740 --> 00:38:19,740
That's not how it works.

286
00:38:19,740 --> 00:38:27,740
It's about learning to see the fear objectively and to stop cultivating it.

287
00:38:27,740 --> 00:38:29,740
It's just to have it.

288
00:38:29,740 --> 00:38:37,740
And if you start to look at it in a new way, you'll stop cultivating the habit.

289
00:38:37,740 --> 00:38:43,740
This is the only way to break the habit to observe the habit keenly, closely.

290
00:38:43,740 --> 00:38:56,740
Well, as I lay out in the booklet, that's the meditation practice that I teach.

291
00:38:56,740 --> 00:39:02,740
So when you're afraid, you would say to yourself, I'm afraid.

292
00:39:02,740 --> 00:39:07,740
But it isn't just to sit in front of Anna Pranavati because...

293
00:39:07,740 --> 00:39:09,740
This is the only question now.

294
00:39:09,740 --> 00:39:12,740
If you read my booklet, I teach what's in the booklet.

295
00:39:12,740 --> 00:39:14,740
I don't want to talk about Anna Pranavati.

296
00:39:14,740 --> 00:39:23,740
Not what I teach, I'm sorry.

297
00:39:23,740 --> 00:39:31,740
Sorry, I didn't really mean to interrupt you, but go ahead.

298
00:39:31,740 --> 00:39:45,740
And then there's this Stephen person, Stephen person who's all black.

299
00:39:45,740 --> 00:39:54,740
But there's someone there and they're listening to it.

300
00:39:54,740 --> 00:39:57,740
Hi, Stephen.

301
00:39:57,740 --> 00:40:02,740
What's up every night? It doesn't say anything.

302
00:40:02,740 --> 00:40:11,740
I know it was smoking one day, which I'm not trying to be a smoking, but I have to put down.

303
00:40:11,740 --> 00:40:13,740
No, they're good.

304
00:40:13,740 --> 00:40:16,740
Probably smoking.

305
00:40:16,740 --> 00:40:18,740
No smoking on a Buddhist hangout.

306
00:40:18,740 --> 00:40:19,740
I don't know why.

307
00:40:19,740 --> 00:40:22,740
I mean, it's not that it's not even bopping anti-smoking.

308
00:40:22,740 --> 00:40:26,740
It's just like if you were to sit there eating while we were on the hangout.

309
00:40:26,740 --> 00:40:33,740
I mean, you can make an exception for drinking water or something, drinking tea or juice or whatever.

310
00:40:33,740 --> 00:40:35,740
You know something.

311
00:40:35,740 --> 00:40:38,740
But...

312
00:40:38,740 --> 00:40:41,740
It's a little bit like going to a monastery.

313
00:40:41,740 --> 00:40:43,740
Rachel, Rachel, is it your monastery?

314
00:40:43,740 --> 00:40:46,740
Yeah, I mean, I don't mean to make a big deal out of it, but I think it's a rule.

315
00:40:46,740 --> 00:40:49,740
I think, dude, if you're going to come on the hangout,

316
00:40:49,740 --> 00:40:52,740
you shouldn't be smoking during the hangout.

317
00:40:52,740 --> 00:40:56,740
You're probably looking. You should take your hat off. I knew this.

318
00:40:56,740 --> 00:41:00,740
You know, in university, the kids come to learn meditation with their hats on.

319
00:41:00,740 --> 00:41:03,740
I tell them to take their hats off.

320
00:41:03,740 --> 00:41:05,740
One gave her the cowboy hat.

321
00:41:05,740 --> 00:41:09,740
They told them they said, one woman had a hat on and she wouldn't take it off.

322
00:41:09,740 --> 00:41:14,740
She said, he was having a bad hair day.

323
00:41:14,740 --> 00:41:17,740
Did you suggest she become a monk?

324
00:41:17,740 --> 00:41:21,740
She wouldn't have to worry about bad hair days.

325
00:41:21,740 --> 00:41:26,740
Well, I can shave that for you.

326
00:41:26,740 --> 00:41:27,740
No.

327
00:41:27,740 --> 00:41:28,740
No, there's some...

328
00:41:28,740 --> 00:41:30,740
There's got to be some protocol.

329
00:41:30,740 --> 00:41:33,740
It's a part of mindfulness.

330
00:41:33,740 --> 00:41:41,740
Being mindful enough to be respectful,

331
00:41:41,740 --> 00:41:44,740
to understand what it is that you're doing.

332
00:41:44,740 --> 00:41:47,740
It's not...

333
00:41:47,740 --> 00:41:51,740
There's lots of things like that. I don't eat.

334
00:41:51,740 --> 00:41:53,740
I usually take my robe off when I'm in my room,

335
00:41:53,740 --> 00:41:57,740
but I won't eat without something, without some kind of cloth on.

336
00:41:57,740 --> 00:41:59,740
Adjantang said that.

337
00:41:59,740 --> 00:42:02,740
He said, at least put your shoulder cloth on when you eat.

338
00:42:02,740 --> 00:42:05,740
These people have given you this food.

339
00:42:05,740 --> 00:42:08,740
I had a respect for their food,

340
00:42:08,740 --> 00:42:12,740
because we often go around with just our lower robe on in our rooms.

341
00:42:12,740 --> 00:42:15,740
That's a monk thing to do.

342
00:42:15,740 --> 00:42:19,740
I mean, that's more of a tie thing than anything,

343
00:42:19,740 --> 00:42:22,740
but it's the same as that sort of idea,

344
00:42:22,740 --> 00:42:26,740
where you have protocol and respect.

345
00:42:26,740 --> 00:42:31,740
It's a good thing.

346
00:42:31,740 --> 00:42:36,740
People just like to do away with protocol and respect.

347
00:42:36,740 --> 00:42:39,740
What's kind of like when you go to visit someone,

348
00:42:39,740 --> 00:42:41,740
you wouldn't bring your own food instead of there

349
00:42:41,740 --> 00:42:43,740
and eat your food in someone else's house.

350
00:42:43,740 --> 00:42:46,740
But we don't visit each other too much anymore,

351
00:42:46,740 --> 00:42:50,740
so we kind of forget these nice cities.

352
00:42:58,740 --> 00:43:02,740
Anyway, I guess that's it for our hangout.

353
00:43:02,740 --> 00:43:03,740
Well, come back.

354
00:43:03,740 --> 00:43:06,740
When do you feel be going to Thailand and Sri Lanka?

355
00:43:06,740 --> 00:43:09,740
Some people started to ask me.

356
00:43:09,740 --> 00:43:11,740
Yeah, we should probably work that out.

357
00:43:11,740 --> 00:43:17,740
I need to go to get a ticket probably for this day.

358
00:43:17,740 --> 00:43:21,740
You know, the trip to New York was a mistake

359
00:43:21,740 --> 00:43:23,740
because I actually have a meditator.

360
00:43:23,740 --> 00:43:26,740
I had a meditator coming during that time,

361
00:43:26,740 --> 00:43:30,740
and luckily he could change his ticket trip.

362
00:43:30,740 --> 00:43:32,740
I have to be very careful about my bookings.

363
00:43:32,740 --> 00:43:35,740
This is the problem.

364
00:43:35,740 --> 00:43:37,740
Probably don't.

365
00:43:37,740 --> 00:43:41,740
So the 28th of the moment is 8th.

366
00:43:41,740 --> 00:43:47,740
Okay.

367
00:43:47,740 --> 00:43:52,740
The 28th of May is the Buddha's birthday celebration

368
00:43:52,740 --> 00:43:56,740
in Toronto, the Mississauga.

369
00:43:56,740 --> 00:44:02,740
So I have to be there for that.

370
00:44:02,740 --> 00:44:09,740
But I think fairly soon after that would probably be a good idea.

371
00:44:09,740 --> 00:44:13,740
So if someone was potentially interested in going to meet you there

372
00:44:13,740 --> 00:44:17,740
or something, probably June, would that be the best chance?

373
00:44:17,740 --> 00:44:20,740
Or anything?

374
00:44:20,740 --> 00:44:23,740
Today, I can't arrange that.

375
00:44:23,740 --> 00:44:28,740
I'm sorry, that's not part of this trip.

376
00:44:28,740 --> 00:44:32,740
Maybe I'm assuming in May I'll have more breathing room

377
00:44:32,740 --> 00:44:35,740
and more time to think about that.

378
00:44:35,740 --> 00:44:44,740
But for right now, please don't ask me about coming to Thailand with me.

379
00:44:44,740 --> 00:44:49,740
I don't know what it's going to look like.

380
00:44:49,740 --> 00:44:52,740
I'm going to have to sit down at some point

381
00:44:52,740 --> 00:44:59,740
while I guess I'm going to have to give you guys some dates.

382
00:44:59,740 --> 00:45:03,740
But probably just give you dates for Thailand

383
00:45:03,740 --> 00:45:09,740
and I'm going to have to figure out Sri Lanka with Sangha.

384
00:45:09,740 --> 00:45:11,740
I don't know.

385
00:45:11,740 --> 00:45:14,740
I guess I can do a month.

386
00:45:14,740 --> 00:45:17,740
Maybe just a month of June.

387
00:45:17,740 --> 00:45:24,740
But I think I've already promised something in June or July.

388
00:45:24,740 --> 00:45:26,740
I don't remember.

389
00:45:26,740 --> 00:45:29,740
I have to check that out.

390
00:45:29,740 --> 00:45:31,740
But that's actually probably not.

391
00:45:31,740 --> 00:45:34,740
I can probably skip that if I have to.

392
00:45:34,740 --> 00:45:41,740
Anyway, maybe the month of June will be in Asia.

393
00:45:41,740 --> 00:45:47,740
Mid-July is already the rain.

394
00:45:47,740 --> 00:45:49,740
I should be back while in time for that.

395
00:45:49,740 --> 00:45:50,740
But that makes sense.

396
00:45:50,740 --> 00:45:51,740
No, the whole of June.

397
00:45:51,740 --> 00:45:53,740
Why am I thinking?

398
00:45:53,740 --> 00:45:58,740
What's going on in June?

399
00:45:58,740 --> 00:45:59,740
That probably works.

400
00:45:59,740 --> 00:46:02,740
Something like June 1st, June 30,

401
00:46:02,740 --> 00:46:04,740
it says September 1st.

402
00:46:04,740 --> 00:46:09,740
June 1st, like 30th, something like that?

403
00:46:09,740 --> 00:46:11,740
That's good.

404
00:46:11,740 --> 00:46:13,740
I don't think there are any meditators booked that far ahead either.

405
00:46:13,740 --> 00:46:16,740
So that works out well.

406
00:46:16,740 --> 00:46:18,740
I think there's meditators through May.

407
00:46:18,740 --> 00:46:20,740
So that works out well.

408
00:46:20,740 --> 00:46:21,740
Right.

409
00:46:21,740 --> 00:46:22,740
We have to check that.

410
00:46:22,740 --> 00:46:23,740
Yeah, okay.

411
00:46:23,740 --> 00:46:28,740
If nobody's booked to that works.

412
00:46:28,740 --> 00:46:45,740
Oh, that's right.

413
00:46:45,740 --> 00:46:47,740
There's the whole UK thing.

414
00:46:47,740 --> 00:46:53,740
That was the monkey wrench.

415
00:46:53,740 --> 00:46:54,740
I want me to go to UK.

416
00:46:54,740 --> 00:46:58,740
I asked if a week would be okay and they said a week would be okay.

417
00:46:58,740 --> 00:47:05,740
And we could be enough if that's all I can do.

418
00:47:05,740 --> 00:47:10,740
So maybe on the way back from Thailand and Sri Lanka.

419
00:47:10,740 --> 00:47:14,740
Would you have time to go to the UK then?

420
00:47:14,740 --> 00:47:19,740
Before the rain.

421
00:47:19,740 --> 00:47:21,740
Yeah, but then it's just lots.

422
00:47:21,740 --> 00:47:25,740
It's just more time away than maybe I should cut back on the.

423
00:47:25,740 --> 00:47:30,740
Well, no, I think a month in Asia is anything less than that.

424
00:47:30,740 --> 00:47:33,740
It doesn't make a lot of sense.

425
00:47:33,740 --> 00:47:38,740
Although, you know, I'm just going to see my teacher.

426
00:47:38,740 --> 00:47:41,740
That would be a chance to take a break though.

427
00:47:41,740 --> 00:47:50,740
Could hide.

428
00:47:50,740 --> 00:47:59,740
I don't know.

429
00:47:59,740 --> 00:48:08,740
Kind of in beautiful and probably shouldn't stay away too long.

430
00:48:08,740 --> 00:48:19,740
Maybe I shouldn't go to Sri Lanka.

431
00:48:19,740 --> 00:48:25,740
Maybe I'll talk to some good about that.

432
00:48:25,740 --> 00:48:30,740
Okay, well, let's talk in the next few days to figure this all out.

433
00:48:30,740 --> 00:48:34,740
You don't need to do this online.

434
00:48:34,740 --> 00:48:37,740
All right, have a good night everyone.

435
00:48:37,740 --> 00:48:38,740
Thank you, Bhantay.

436
00:48:38,740 --> 00:48:57,740
Thank you.

