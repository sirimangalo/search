1
00:00:00,000 --> 00:00:18,160
Okay, so this next installment is me writing a letter to the more park acorn, the local

2
00:00:18,160 --> 00:00:30,160
newspaper. There's the more park acorn and they have a letter to the editor

3
00:00:30,160 --> 00:00:37,600
section. So I figured after being stopped by two police officers in the past

4
00:00:37,600 --> 00:00:44,400
week that I should put everyone's try to help put everyone's minds at rest

5
00:00:44,400 --> 00:00:53,080
by going over explaining in what I am and so I've done that here and I'll

6
00:00:53,080 --> 00:01:04,440
have you check this out. Dear Citizens of More Park, I am writing to introduce

7
00:01:04,440 --> 00:01:08,720
myself as a new resident of your city. I'm a Buddhist monk and I teach

8
00:01:08,720 --> 00:01:12,920
insight meditation to students from around the world. I am also currently

9
00:01:12,920 --> 00:01:16,880
doing volunteer work as a chaplain at the Federal Detention Center in

10
00:01:16,880 --> 00:01:22,840
Los Angeles and I teach over the internet via YouTube at channel UTA Dhammo

11
00:01:22,840 --> 00:01:27,280
as well. Every morning I walked to the local Thai restaurants whose owners

12
00:01:27,280 --> 00:01:31,840
kindly give me food from my daily meal. I don't beg and I don't touch money

13
00:01:31,840 --> 00:01:37,340
at all. I don't practice martial arts or any sorts of religious ritual. I wear

14
00:01:37,340 --> 00:01:42,640
simple course robes that often draw attention to my person and I wish to make

15
00:01:42,640 --> 00:01:45,840
it clear that I'm not here to convert people or interfere with their lives in

16
00:01:45,840 --> 00:01:50,440
any way. Though I'm happy to teach meditation to those who ask. Otherwise I

17
00:01:50,440 --> 00:01:56,000
mostly keep to myself and attend to my student. Recently I was taught by a

18
00:01:56,000 --> 00:02:00,000
local police officer who politely asked who I was telling me that they had

19
00:02:00,000 --> 00:02:04,200
received a call by a concerned citizen wondering who I was and what I carry

20
00:02:04,200 --> 00:02:09,120
under my robes when I walk. As I explained to him and later to the sergeant at the

21
00:02:09,120 --> 00:02:14,640
police department whom I took it upon myself to visit I am a simple monk and I

22
00:02:14,640 --> 00:02:18,120
carry a simple arms ball under my robes that is used to carry the food I

23
00:02:18,120 --> 00:02:22,520
receive. I don't wear shoes when on arms round in order to maintain humility

24
00:02:22,520 --> 00:02:28,560
when receiving food otherwise I wear shoes as normal people do. I hope my

25
00:02:28,560 --> 00:02:32,320
presence in more park will be seen as an addition to the community rather than

26
00:02:32,320 --> 00:02:37,120
a threat the latter of which I guarantee that I am not. I am residing at the

27
00:02:37,120 --> 00:02:41,120
Wood Creek apartments on spring road and I'm in the process of setting up a

28
00:02:41,120 --> 00:02:45,960
small residence in the countryside nearby. This letter is only meant to ease the

29
00:02:45,960 --> 00:02:49,920
minds of people who see the first Buddhist monk ever to come to live in more

30
00:02:49,920 --> 00:02:56,160
park that I should present no danger or inconvenience to anyone and hope to

31
00:02:56,160 --> 00:02:59,760
be on friendly terms with the residents of this fine community. Thank you for

32
00:02:59,760 --> 00:03:04,200
reading this and thanks to the acorn for publishing it sincerely brother Noah

33
00:03:04,200 --> 00:03:13,800
Yuta Damo. That's my letter and that's what I'm going to send to you right now

34
00:03:13,800 --> 00:03:19,800
I'm going to email that to the more park acorn and see if that can help to

35
00:03:19,800 --> 00:03:25,000
ease people's minds when they see me walking down the road. Apparently it's a

36
00:03:25,000 --> 00:03:28,600
big sight I never see it but everyone tells me how when I walk down the street

37
00:03:28,600 --> 00:03:36,800
everyone is like their heads just go boom maybe something strange and I think a

38
00:03:36,800 --> 00:03:44,400
lot of people actually see it as kind of void exhibitionism where you know

39
00:03:44,400 --> 00:03:48,320
trying to it's a stunt like trying to gain attention which is surprising to me

40
00:03:48,320 --> 00:03:55,040
because or you know I didn't realize that that's how it would be seen and a

41
00:03:55,040 --> 00:04:01,600
lot of people are jeering and you get a lot of kids you know taking taking the

42
00:04:01,600 --> 00:04:08,720
opportunity to poke fun or kind of like play back you know interact with me

43
00:04:08,720 --> 00:04:14,840
as though it were some kind of stunt that I was pulling or playing which is a

44
00:04:14,840 --> 00:04:18,240
shame really but I'm thinking that it's going to get better and I think this is

45
00:04:18,240 --> 00:04:22,120
a lot better than the community that I was in before where I was actually

46
00:04:22,120 --> 00:04:26,040
arrested because someone claimed that I was doing things that I've never done.

47
00:04:26,040 --> 00:04:31,680
You're hoping that this community will be at least a little bit more open-minded

48
00:04:31,680 --> 00:04:39,520
and I won't be subject to such difficulties here so just another part of my

49
00:04:39,520 --> 00:04:44,320
life never never a dull moment thanks for tuning in this is the latest

50
00:04:44,320 --> 00:04:55,840
installment of a life in a day for a month.

