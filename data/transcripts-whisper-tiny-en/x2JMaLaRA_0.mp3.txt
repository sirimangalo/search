Okay, so today I'll talk a little bit about
We passana. We'll talk about septate.
We'll talk about anything we want now to talk about.
We passana, which is the second part of
We'll write a thing.
So to talk about we passana, it's useful to
Talk about it in the context of another word called
Sama-ta.
Sama-ta and we passana are two words that often go together.
It made very different things.
And so to put a point at that,
then the two qualities of meditation practice that are sort of
salient qualities.
In a way you can think of them as the outcome of the practice.
And I've said this before we actually don't practice.
We passana. We practice septate.
So we talk about septate, we passana.
And so you should understand these two parts.
septate, we passana is what we practice.
Whenever practice, we passana.
You can't practice seeing clearly just like you can't practice letting go.
You have to practice looking.
You have to practice opening your eyes.
Mindfulness is this act of grasping the object,
focusing on the object, facing the object.
If you never face it, you'll never see it clearly.
So once you've done that, seeing clearly comes right out of
it.
But there's this other word, Sama-ta.
And it's important to understand that word as well.
It helps to understand why septate is so important.
And exactly what quality our practice should take.
And what sort of results we should expect.
A lot is said about septate and we passana.
For those of you who know these two words,
you've probably heard many different explanations about them.
And there's actually controversy about the differences of opinion about them.
So I'll make it quite simple and you can believe you're not.
But here's my explanation of what these two words are based on the text that I read.
Coin often.
Well, first and foremost, you have to understand again that these are qualities of mind.
Sama-ta means tranquility.
It's a pretty standard translation.
We passana again means clearly.
So they're very different words.
But they're useful to understand because when we talk about them as types of meditation,
we have these words, Sama-ta, Kama-ta, and we passana-ta, Kama-ta, Kama-ta, Kama-ta.
And there's a whole problem where it meets meditation for maybe meditation practice.
Sama-ta, Kama-ta, Kama-ta, and I refers to a meditation that has, as its goal, tranquility.
Or more accurately, and here's where it's maybe a little controversial, but I don't think it should be.
Meditation that only has the capacity to come to deliver Sama-ta tranquility.
This is the claim that there are some types of meditation that just can't lead to we passana.
And so we passana-ta-ta-ta.
It's meditation that has, as its resolve, we passana.
It seems clearly, perhaps, the result of it, but
we passana meditation also cultivates Sama-ta.
And so honestly, it's the better of the two.
And so we are describing the difference that we claim exists between the Buddha's teaching and teaching outside of its dispensation.
If you remember, if any of you have heard the stories, after he left home,
after the Bodhisattva left home, he went and lived in the forest, found some teachers,
and he found two very famous teachers, and learned all he could from them.
And they taught him what must have been very powerful meditation practice,
but he said about these practices.
They only lead him to the Brahma realm, only, no.
He said, they don't lead to wisdom.
They don't lead to understanding.
This is the sense he got, that there are these practices that just,
no matter how powerful they might be,
how incredible the results might be.
They just don't lead to wisdom.
So we would say these don't, these are not the best of that government.
They don't have the capacity to allow you to see clearly.
And that's where there's some contention.
People say, oh, why can't?
Why isn't it?
Why can't you practice this way and still see clearly?
Any meditation should allow you to see clearly.
That's the claim.
And I can give a really good example then,
but you see that this isn't actually true.
But there are some meditations that just cannot.
Some, some it's hard to see,
and for some types of meditation, they're kind of borderline.
But let's take an easy example first.
An easy example is casino meditation.
Casino, casino means totality.
And what that refers to is trying to make your whole universe
the whole sphere of your perception, a single thing.
It's quite simple actually, it's simple that it sounds.
You take a simple object, a single object.
And you focus your attention on that simple single object.
The best example I can give you is a color.
Let's say the color yellow.
And so you focus on the color yellow,
and you just say to yourself yellow, yellow, yellow.
Red, red, let's say red, red, red, this is simple color, right?
Blue, blue.
And you just repeat to yourself blue, blue, whatever the color you've picked it.
Just keep repeating it to yourself.
And the way you do this is you actually get a piece of cloth or something
or to modern times maybe a piece of paper that's that color,
cut it into a circle, a perfect circle that's as smooth as you can get,
and just put it down in front of you, and just stare at it with your eyes open.
And then close your eyes and try to see it with your eyes closed.
When you can't see it with your eyes, open your eyes again and look at the blue.
But when you can see it with your eyes closed, just focus on the blue color with your eyes closed.
You do this long enough, you don't need the physical circle anymore,
you can just have your eyes closed.
Really have a clear blueness, a blue circle in your field of vision with your eyes closed.
And this blue object is controllable.
And you start to train yourself to be able to expand it.
First of all, it's stable, so eventually it's fixed.
You're able to see the blueness, whatever you want, stable.
It's quite pleasant as well.
And it's controllable, so you can expand it to infinity eventually.
When you get to go to these kinds of meditation, this isn't the only kind.
Any meditation like this, you can really control.
And then you're able to expand it into infinity.
And that's a very easy way to get into the, what we call the immaterial trance thing.
These are what sort of things they put us first, what is that this first teacher's talking.
But that type of meditation is, in some ways, antithetical.
Not only can I not like to rebass it, but it's antithetical because it leads to or it deals with perceptions of stability,
satisfaction, and control.
It's kind of like an illusion.
You work hard and you have this state, and it seems like you found something that is stable, satisfying, controllable.
Because your mind is inclined in that direction.
The problem is that it only works when your mind is inclined in that direction.
So it's actually not stable, satisfying, control, but all of it, you don't see that.
And when you stop practicing it disappears, then you lose your capacity.
It's like exercise, you stop weightlifting, your muscles start to deteriorate.
You're same goes with the mind.
It's still based on causes and conditions, but you don't see that while you're in it.
And there's no perception of the instability of reality.
So you're not able to help you see clearly the nature of reality for that reason.
And there's many types of meditation that fall into that category, met the meditation,
where you think kind of thoughts about other beings.
Your object becomes the happiness, like a being, a perception of them being happy.
And that object, that person that being is stable, and it's kind of satisfying to see them happy.
And to feel good about them.
So these kinds of meditation are called summative meditation.
As I said about borderline, it's a good borderline example, is the breath.
Is the breath summative? I think we possibly.
I've said there's a lot of contention.
I think I've gotten to trouble suggesting that the breath is a concept.
The problem is that breath, the breath going into your body, isn't real.
That's not, isn't real.
And since that's not what you experience, the idea of the breath going to your body is just an idea.
You have this idea that breath is going into your body.
So if, for example, you say, in one, one, or even just in, out,
it's more likely to lead to this subtle, fixed sensation of a concept of the breath.
And eventually, we do it to train the states as well, summative state.
Much more likely than it's to help you see the unpredictable, insubstantial and uncontrollable nature of things.
If, on the other hand, you focus on the sensations, like the heat and the cold feeling of the nose,
or the pressure and the chest, or the pressure and the stomach, which over time becomes quite
obvious, as you start to relax, this is where you naturally breathe from.
For those of you having trouble with the stomach, I recommend just as a test,
try lying down on your back.
Some people think, oh, my stomach just doesn't rise and fall, that must be just the way my body is made.
Lying down on your back, and you'll be quite surprised to see how gross, how obvious the rising and the falling is when you relax.
The problem is our ordinary sitting state were quite tense.
And over time, this changes as you become relaxed and proficient at it.
You will be able to relax, and you'll see it just naturally get forced, but over time will come.
But anyway, this experience is real, the tension in the stomach, the tension in the chest, the heat and the cold feeling of the nose.
These are real, and they're unpredictable, and they're uncontrollable, and you're going to start to see that.
The stomach is such a horrible object of meditation, and it doesn't feel comfortable.
It's something that will betray you.
It will thwart your efforts to control it, your efforts to find a stable, satisfying, comfortable state of practice.
And that's okay, that you shouldn't be trying to find a comfortable, stable, satisfying state of experience.
Because that's a reliance, then you become dependent on that state, and you fail to see the unpredictable nature of your body.
So to think that, if only I can maintain that, it's based on delusion, the idea that somehow it's sustainable.
And it's much inferior to the freedom that comes from being able to be at peace, no matter what happens.
It seems impossible, that seems like the only way you can be happier at peace is if you find that stable, satisfying state.
So that thing that's going to last forever, that thing that is going maybe not forever, but at least for a long time, that I'm going to be able to control and be happy with.
It would be nice if such a thing existed, but it doesn't in this world, there is no such thing.
But the only thing that we cling to like that is just going to make us stressed and so forth.
When we see that through seeing the three characteristics, which is the basis that we pass on it, we gain a different kind of happiness.
It's a happiness that's called nearly sasoka.
It's a happiness that isn't dependent on an object.
It's much more powerful, much more reliable, because you don't have to rely on anything to be happy.
If you're independent, little bit of said, in the Satyapatanas, this is why you said Anisei Dojali Harakti, through mindfulness, in the Satyapatanasum,
one dwells independent. If you're dependent on pleasant feelings, dependent on the absence of pain,
you're always going to be tossed to and frosting this change. You'll never truly be at peace.
So this is the difference between we pass in on some of the time. With the breath, again, it can be both.
But the most reliable way to understand that you're practicing we pass in on is when you're object is the five-eyed convenience.
When you're object, to put more simply when your object is experienced.
Why did the Buddha, and this is something people miss, why did the Buddha talk so much about the six senses of the five-eyed convenience?
You read books about these things, talking about trying to explain why the Buddha taught them.
If it's not from a meditation perspective, you might not understand how important it is.
This is the Buddha talking about experience, trying to describe the sorts of things you experience directly.
But therefore, the sorts of objects that can allow you to see clearly.
So this is a background of how we pass in on the place we pass in on the past in the Buddha's teaching. What we pass in on meanings.
In a sense, what is it that we're trying to see clearly? I've said this before, it's to see clearly three things.
But inside of ourselves and in the world around us, everything is impermanent, changing, unpredictable.
But inside of ourselves and in the world around us, everything is suffering, it's Dukkha.
And inside of ourselves and in the world around us, everything is unknown, sound.
To see these three things. This is the purpose. It doesn't sound very inspiring, really to see this.
And so it takes some, I mean, it really requires some depth of understanding to appreciate these.
You have to be able to appreciate the importance of being independent, but not being dependent or lie in unpredictable things.
You have to understand that life is unpredictable.
Many people who come to meet it to, we pass in a meditation, come because they're suffering, because they've seen the uncontrollable nature of things.
Because they've seen change, they realize that they just can't rely on stability and satisfaction or control.
So people who've already seen the three characteristics are able to appreciate these.
The reason why many people don't come and practice is because they either are happy, or they seek happiness.
They are uncomfortable, right? If you're lucky in the world, sometimes for some time bad things won't happen to you.
Maybe you have money, maybe you have a family, and it seems like everything's pretty good.
For some people, they've never had to experience the three characteristics as a real source of suffering for them.
And other people are maybe not in that state, but they just are clear in their mind enough.
They've been taught taught, right? We're taught by so many sources that if only you get this, if only you get that.
And so they're seeking out, seeking in the wrong direction.
So again, appreciation to all of you, I would say, are seeking in the right direction.
Often because you have experienced suffering in your lives, you've experienced the three characteristics,
and you want to understand what is this, understand more deeply.
So I'd like to talk a little bit about the three characteristics just to give a little bit more depth of understanding.
It's easy to misunderstand and get turned off or turned away.
You know, get discouraged.
They sound like they might not be something you ever want to see.
It's something you believe, if I say everything inside of you in the world around you is dukai.
That can't be true.
So you have to explain what we mean, exactly.
So the permanence is probably the easiest one to see.
It's already in much top culture.
Everything changes.
It's a while saying that you hear bandit around everything changes.
Change is the only constant.
We know that we are not who we were when we were young, and we've seen ourselves getting old.
We know that we're going to get old.
We know that life is impermanent.
We're going to die.
We often ignore this fact that we're often kind of purposefully trying to avoid thinking about death.
Or that we're getting old.
And we can get loved into a false sense of security.
But impermanence is not so hard to understand theoretically.
Except that in mindfulness, impermanence is not this kind of intellectual idea of things not being the same as they used to be.
It's not about things changing at all.
Because reality doesn't change.
Reality arises in seasons.
And so the truth of impermanence is what you're seeing in the meditation is the unpredictable and the momentary nature of reality.
It's part of a shift away from objects as lasting entities to experience as momentary phenomena.
Right, like your body, you think of yourself as sitting here, but sometimes you lose track of the body sitting.
Because there are only the sensations and the sensations come and go.
Thoughts, you find yourself thinking something and suddenly your mind is thinking something completely different.
One moment you're perfectly at peace and calm in the next moment you're mind is racing.
Or vice versa, you think the practice is impossible and taller and suddenly it's very peaceful and calm.
The characteristic of impermanence is sinelessness.
Sinelessness means no signal.
There's nothing and there's no precursor telling you, hey, we're about to change.
Here about to lose this.
Impermanence, learning impermanence is learning the flexibility.
We're not able to deal with all kinds of change.
And so we react. A lot of our reactions come from change.
Especially as we become accustomed to a certain state.
We feel very calm and we're kind of accustomed to that.
We're happy with it. We like it. And many of you have described being caught off guard.
When it's gone and wondering what you can do to bring it back.
Kind of being frustrated that you're not experiencing what you used to.
This is being caught off guard by change.
Be caught off guard by the disappearance of something that you relied upon.
You were dependent upon.
So the real peace and happiness that comes from meditation is much more challenging.
It isn't dependent upon a calm state.
It's a sense of peace no matter what happens, no matter what you experience,
whether it's pain, whether it's thoughts, whether it's even emotions.
Your mind is not reactionary to any of them.
And this is a great benefit of seeing clearly the impermanence.
Whereas you see impermanence born clearly.
It no longer bothers you the way you used to change loss.
Or the arising of things that you don't want to arise.
You become much less surprised, much less disturbed.
The characteristic throughout the course is going to be when something strange occurs.
Something you didn't expect.
That's a sign that you're starting to broaden your horizon to become more flexible.
You'll notice that you're surprised you really didn't expect it to change like this or change like that.
That's a sign of this process of growth.
You're seeing your privilege.
As far as suffering while there's a lot you can say about suffering.
And we have to always clarify what we mean here because people's understanding of what suffering is,
is usually restricted to what we want to convey to them.
There's four kinds of suffering.
The feeling of suffering,
the reality of suffering,
the characteristic of suffering and the truth of suffering.
And this is how it goes.
An ordinary person experiences suffering or perceives suffering as being a feeling.
In other words, a certain part,
a certain potential aspect of reality.
And most importantly, a part of reality that you can avoid.
So as long as you don't feel pain, you just get suffering.
This can be emotional pain or to be physical pain,
but as long as you have ways to avoid this,
then you'll be happy.
One of the things you're learning in meditation is that the more you try to avoid the pain,
the more unpleasant it becomes.
It actually isn't a solution to try and avoid it.
So this is an ordinary understanding.
And I said that many of you have experienced suffering.
What I meant is you experience what we call tukasabhava.
And tukasabhava means something like reality or existential reality.
But what this means is that a person who is inclined towards spirituality is usually inclined
because they've seen a suffering actually is in many cases unavoidable.
There are at least times in your life where there is no way out of suffering.
The people who get really bad headaches, they can only take so much pain medication.
When you're sick or have arthritis, you try and try to take this medication
and people go in Los Angeles, all the type of people who are going to these acupuncture,
acupuncture, etc., etc., massage therapists,
try massage is an interesting thing.
They were used to teach in Thailand and talk to people about time massage,
people who went to get time massage as people who studied time massage.
And I talked to them as Susan and we're an argument and she said, yeah, the truth is
I can teach them massage, but I know they're just going to come back in a week
or a month and need another one.
Because the tension is stressed, it's causing the pain.
It isn't going away.
They're not getting rid of that treatment of massage.
When we get rid of the accident, anyone who's been through great stress,
if you go through the laws, when you lose a loved one, when there's a breakout,
I'm going quite in love.
So much stress and suffering and you just feel like there's no end.
Nowadays, for sickness, there's a case recently, someone,
it lauded as a triumph.
That someone in Colombia, I think, was given assisted suicide
for a non-life threat and illness.
And so I'm against this, and I'm not going to stop someone from doing it,
but absolutely against it, and as much as I think we should be against it,
because of how great an opportunity it is first to be a human being.
But second, to go through the suffering that you're experiencing,
it's certainly not a great idea to try and avoid it by killing yourself.
But from a Buddhist perspective, because you end up being reborn in the place
that is based on that aversion, based on that disliking of the pain,
that disliking of the suffering.
But for many people, again, this is an impetus for them to come to spirituality.
Why? Because it's not just a part of reality that you can avoid.
The realization that you can't avoid suffering.
You can't avoid hunger, thirst.
One monk in Bangkok told a story about, he said, I heard about this monk who was
taking on the ferry in Bangkok.
They go on these ferries from one dock to the other, up and down the river.
And I think I went on one and one.
And this old monk, can he have taken some kind of, I don't know what he had to eat or something.
But he was on the boat, and suddenly he hadn't digested.
And like in the middle of the river, and all these people crowded around the boat.
So he just sat there and he expelled the diarrhea in his robes.
And just sat there.
And when they got to the dock, he jumped over the side of the boat into the river.
And swam to the dock.
No wash themselves off.
Swam to the dock.
And when the teacher said, this is a boy, if that's not suffering, I don't know what it is.
I'll avoid it home.
Sometimes unpleasant things.
I think you really want to avoid a candle point.
I don't know, maybe that's a non-self one of it.
I thought that was a part of suffering.
But this is an impetus for people to begin to find another way,
saying, look if I can't avoid suffering.
Is there another way?
And it's a very important question.
It's a very important perspective.
Because that's really the essence of this.
It's about facing.
The thing is that we really think we should be avoiding.
We really think we should be afraid of ourselves from.
Mindfulness is about facing.
The third is Dookalakana.
And that's what we're talking about here.
What you are discovering now as spiritual people.
Because that's what you are.
If you want to be secular about it as people training their minds.
Are you starting to see the characteristic of suffering?
And what does that mean?
That means the characteristic of every aspect of experience
as not being satisfied.
It's not actually that it's painful.
Obviously some experiences are quite pleasant.
But if they're Dookal, meaning they're not Sookal.
Because again, just like impermanent.
This is only meant to free us from our wrong perspective of satisfaction.
But this was satisfied.
That was satisfied.
If I get the new phone, that was satisfied.
After lunch, boy, I'm so hungry.
But once I eat, then I'll be satisfied.
And so much pain, my leg is in so much pain.
But when I move, my leg, then I will be happy.
Once I feel calm and not be happy.
That was satisfied.
And it doesn't.
Because then the calm goes away.
I got to find it again.
And when you can't find it, you're unhappy.
You start to see this that there isn't a refuge.
There isn't something that you can cling to and say, OK,
this will keep me safe, and I won't suffer.
The attitude of trying to find such a thing,
trying to cling to something as a refuge.
It's a very deep right.
From birth, what are we to add?
Clink to your parents.
Your parents will protect you.
Some people's parents do not protect them.
But for many, this is what we're taught.
Someone to protect me.
And something that I can cling to.
For many theorists, it's quietly.
They cling to God.
God is supposed to be that.
God is the protector.
And this is clinging.
Well, it is.
It doesn't bear through the fruit that you think it bears.
That's what you're starting to see.
Or more importantly, you're starting to see, actually,
a better alternative to be, again, anisito.
Anisito, juliyarati, dwell in the pendant.
Not checking juliyarati, or not clinging to anything in the world.
Or in the mongolism, that again puts us a dokadami,
jitanya sana kapati.
When one's mind doesn't waver in the dokadama,
dokadama, you could think of bad things, basically.
You experience the vicissitudes of the world,
vicissitudes of life, the changes.
And you're done.
A waver.
No, this is good or cold.
This is bad when you're able to experience
without reaction.
This is called dukasabhava.
That's right, dukalakana.
It seems a characteristic of everything.
No, that's not satisfying me.
You thought it was, but it turns out it's not.
You're starting to see this about everything.
This is like a bird in a tree, in a common tree.
You see, it's like a bird in a tree,
jumping from a fruit tree, something from branch to branch,
not from fruit.
No, it's branch, no fruit, that branch, no fruit.
That's dukalakana.
Starting to learn that this tree
ain't to branches that you would cling to
are not worth clinging to in the above one.
This is where you're starting to see.
Until finally, dukasatcha, the truth of suffering,
which we've all read, heard of, the first double tree.
All that means is that you finally realize that none of
the branches have any fruit on them.
And then what do you do?
You fly away.
When the bird flies from the tree,
that flying away is free from suffering.
It's the result of dukasatcha.
When the bird says, this tree has no fruit,
that's dukasatcha.
And this is what happened.
It's not intellectual, but the mind gets it.
There's a moment.
There will come a moment in your practice.
At some time, you're all on the path.
It may not be today or tomorrow.
This course, next course, there will come a time
where not intellectual.
Your mind suddenly understands,
appreciates nothing.
There's nothing.
Nothing here is going to satisfy.
And it doesn't fly away per se.
It lets go.
It doesn't go anywhere.
But it stops sinking out.
And it drops into the bottom.
There's this cessation experience.
It doesn't go anywhere.
I don't know anything to be afraid of.
Just cessation.
Just, OK.
Time out.
Peace.
And let's go.
That's called dukasatcha.
See you there.
As for Anatas, the heart is fun to explain.
Easily misunderstood.
Very frightening.
Confusing.
It's most confusing when you take it intellectually.
If you think of Anatas as an intellectual idea,
like I have no self, there is no self.
No, it's not very helpful.
So, I've said this before.
I remember saying this as you're like,
and I got a big trouble from the audience.
They were like, oh, it's your talk.
I said, the Buddha never said there is no self.
The Buddha never said there is no self.
The problem with saying that is it implies,
oh, so there is the possibility of a self.
I wouldn't say, no, there is no possibility of a self,
but it's not useful to think like that.
These kind of thoughts are conceptual.
It's like saying, is there a hat?
Or does this hat have a self?
Because it's not just my self.
It's also this hat having a self.
That seems kind of weird.
We don't think of self as the hat is having a self.
But that's basically what we're talking about.
It's only because we have this sort of religious
faith.
This is an entity of saying this, if you will.
The thing is sort of the best, right?
This is a thing, right?
Yeah, but it isn't.
It's two things now.
It's where the napkinel, right?
You saw the napkinel, where did it go?
It didn't have a exist in the first place.
It existed up here.
This is the kind of idea of self.
It was the entity.
You want a more visceral example?
What was this, Chris?
I think I missed it.
It's a fist.
Okay, everybody watch the fist.
Where's the fist?
What happened?
It's gone.
Oh, it's back, right?
This one is nice because it's five,
and it's five, I think.
This is the entity.
This is the five, I think.
entities don't exist.
Their heart is kind of, you could say,
lazy, but very practical when you're
looking at the world.
It's practical for me to say, oh, yeah, this is a hat.
I know, if I didn't know it was a hat,
I wouldn't know to put it on my head.
It can be more.
The napkinel I know is useful.
It was useful to wipe things up.
So in a world that says we have to,
and therefore we get into this kind of,
we fall into this sort of believing in the actual
existence of it.
And it's really just a useful convention to say
when you knit this together, it becomes a hat.
It becomes a thing in a certain shape
that is useful to be used to put on your head.
We do the same with a human being.
A human being can be dissected.
When a person dies, we start to wave
or you ever see a person in a coffin.
You have to ask yourself, is that a person
and you can feel this kind of tension
where your mind struggles to,
is it a, it isn't fun?
And so there are actual meditations that help you.
Conceptually, this isn't the best of them,
but they conceptually help you get for yourself
or see the conceptual nature when you watch a body decompose.
Monks would do walking meditation beside the decomposing corpse
so they could look at it and they might sit
and stare at the decomposing corpse.
And you can see at some point,
your mind starts to wave,
but it's not actually a sound.
So in brief,
in regards to being an entity,
it relates to how to experience things.
It isn't intellectually, is there a self?
Isn't there a self?
Do I have a self?
Do I not have a self?
Those kind of thoughts aren't based on reality.
They're abstract.
Let's just not think about that.
Not worry about that.
That isn't what this is about.
Isn't what any of this is about, right?
The small course is not about theory
or philosophy.
It's about experience and the truth of experience.
Is this really a hat?
No, it's seen, right?
And so the same goes with ourselves.
And this is an issue because our reliance on entities
has created some attachments.
My hat, my phone, my road, my body.
When I get sick and I'm afraid,
maybe I'll die.
This body, no, it's mine.
I will die.
So something that was supposed to be practical becomes a burden,
becomes a hindrance.
And then someone says something not nicest,
or someone criticizes you.
I don't deserve to be taught to talk to like that.
When we're criticized, we can see this quite clearly
how we become defensive.
When we feel inferior or insufficient,
we can have low self-esteem.
Or when we have high self-esteem,
we feel like we have so good.
When we think we're handsome,
or when we think we're ugly,
we think we're tall or short.
So on.
A lot of our suffering comes from attachment to self
and really you could say that we wouldn't worry
or we wouldn't strive for happiness
if it wasn't for us.
It wasn't meant to be for me to be happy.
If you can get rid of yourself,
there is no sense in your mind of identification with things.
And this is an important part.
You may not realize this,
this is an important part of how you come to experience suffering
without a strange pain, for example, without suffering.
Because pain goes from being, I'm in pain
to being just an, I think,
alien, a characteristic of a non-self,
is alien for me.
So when you start to see pain as just like a thing
that arises in front of you,
it's over there.
How's that might mean it's way over there?
Because that's the truth of it.
It is way over there.
It's that kind of problem over there.
I mean, you don't think like that,
but it starts to take on that perspective
and it's just that thing over there.
When you lose the perception of me and mine
in things in your experience.
So that's all an advantage.
One part of it, now it is complex.
It's not just a single thing.
There are like four different kinds of understanding
of self that we have to deal with.
But they all go away in the same sort of way
of seeing clearly when you start to see clearly
you stop identifying.
But another important aspect of non-self
is the uncontrollable nature.
Not having a word, not being in mind.
So our attendance to control things
are a huge part of the problem
and a huge part of the cause for us to suffer.
They encourage us to desire
and to get disappointed
when our efforts to control our benefit.
So our perspective,
an important part of the change
of our perspective has to be this move away
from trying to control.
And that's what you're going to,
that's going to be affected
because of what you're going to see.
As you see that your efforts for control
are either impotent or conducive
or suffering, you lead to more suffering.
You watch as you try to control the stomach, right?
I'll make it smooth.
And the more you try,
the more uncomfortable it becomes.
The more stressed it becomes.
You start to notice that there's a pattern
to anything that you try to control
leads to more tension,
more stress, more anxiety,
but that's you worry about
whether you're going to be successful
and whether it's going to disappear or so.
Whether it's something you've controlled
to get away from,
it's going to come back.
And then when it does come back,
there's this disappointment that takes away.
So a big part of becoming independent
is in giving up this idea
that you have a duty
or you have a relationship with the experience
as trying to control it or trying to fix it.
Seeing things as problems,
seeing things as issues to be solved,
your mind starts to change
and you start to see those things
that normally people would say as problems,
you see them just as experiences.
Remember, there was one case in our monastery
where sitting without John Tom,
I'm doing the recording.
Suddenly someone came in and said,
there's one of the nannos
that's gone crazy right outside of his critique.
He was like, no, no, no.
He was just going crazy.
And everyone went like,
this rubber neck came to look
and I just talked about this,
it's like this.
I just looked at him and he said,
just sitting there,
and then they said,
they said to him,
oh, you're telling him to be mindful.
It struck me very, very clearly
the idea of, you know,
it's another experience
when everyone else is stressed about everything.
Stress about the problems in the world, right?
Stress about problems in your life.
It's not easy and it's not pleasant to think of,
but the only actual solution
is to be at peace with your experience.
The good side is once you're at peace
with even bad experiences,
they tend to get better
because you're not messing up,
messing up, you're not creating animosity
with other people or tension with others.
But even having no food to eat
is still just an experience,
something that you can start to relate to here
when you're eblasting,
actually hunger is not as scary as I thought it was, you know?
I thought I had to sleep more and more.
Sleeping is a bad experience.
You sleep less.
You start to realize that actually,
if this has taken to its natural conclusion,
everything I experience,
even if I get in a car accident,
it will still just be experienced with similar
to what I'm experiencing here.
And so you start to change your perspective
as being a thing that you have to solve,
so this sense of identification or responsibility
starts to change.
And you can still act.
You still act in the world,
you still fix things and so on,
but your perspective is like,
okay, well now I'll do this.
And then when you can do something,
you don't have any need to fix your problems.
So you don't suffer.
If you have to go hungry, you go hungry.
If you get sick, you get sick.
If you die well, we all die.
You start to gain a perspective that is truly safe
from suffering.
There's nothing in the world that can harm you.
This is the same safety as a very important part
of the attainment of enlightenment.
So it could sound very radical,
but it's not something that you just jump into,
but what happens is you're gradually
freeing yourself from worries and stresses and fears.
It's not like something scary to worry about.
It's something that should be liberating at all times.
You start to see that the things that are causing you stress
and suffering don't happen to cause you stress and suffering.
You start to let go of them.
This comes.
This is the fruit and the benefit of the practice of
we pass it or ultimately leads to,
as I said, perfect letting go
or the mind that's completely, really finds peace.
So I think that's enough for today.
We'll talk a little bit about Samothan.
We pass it on.
The three characteristics which are the essence of the past
and that's what you're going through now.
You're just starting to gain this better perspective,
this perspective that allows you to let go of the independent
in the world.
Nothing complicated.
Nothing hard to understand.
Quite simple.
And yet, also, if I'm going to understand and sympathize.
I wish you all success in your practice and hope that this is a
fruitful and beneficial experience for all of you.
Thank you.
Good day.
Thank you.
