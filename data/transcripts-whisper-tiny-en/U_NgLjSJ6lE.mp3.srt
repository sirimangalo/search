1
00:00:00,000 --> 00:00:09,260
When you are standing, you keep your legs close together like this, try to relax, meditation

2
00:00:09,260 --> 00:00:16,800
practice, you don't want to force yourself, you don't want to press yourself, try to relax.

3
00:00:16,800 --> 00:00:17,800
How to do?

4
00:00:17,800 --> 00:00:25,640
If you feel you are tired, you are weak, you are just breathing, breathing deep, breathing

5
00:00:25,640 --> 00:00:30,280
about three times, you feel that you have more power, more energies.

6
00:00:30,280 --> 00:00:36,080
So you said, this moment you say standing, why are you standing?

7
00:00:36,080 --> 00:00:46,000
You close your eyes, standing, standing standing, standing be aware your body is standing.

8
00:00:46,000 --> 00:00:53,880
Please remember that mindfulness is very, very important for meditators.

9
00:00:53,880 --> 00:00:58,400
So while you are standing, please be mindful.

10
00:00:58,400 --> 00:01:03,880
If you have good mindfulness or good self-consciousness, you can have good concentration.

11
00:01:03,880 --> 00:01:12,040
If your concentration, if your consciousness or if your mindfulness is strong, your concentration

12
00:01:12,040 --> 00:01:18,800
will be strong, if your concentration is strong, you can sit longer.

13
00:01:18,800 --> 00:01:27,600
But opposite, if your concentration is weak, you can sit longer because you have strong pain.

14
00:01:27,600 --> 00:01:39,320
So this moment, I just show you, standing, standing, standing three times, and then you

15
00:01:39,320 --> 00:01:46,520
start to walk, because we are good, so we have to walk on the same legs, same time.

16
00:01:46,520 --> 00:01:51,760
When you start with your right leg, when you fall asleep, like this, when you fall

17
00:01:51,760 --> 00:02:04,680
sleeping, you start lifting, so provide, raising, dropping, lifting, raising, dropping,

18
00:02:04,680 --> 00:02:07,960
lifting, raising, dropping.

19
00:02:07,960 --> 00:02:15,640
You look overhead, about two minutes, you don't want to close your eyes, why you are walking?

20
00:02:15,640 --> 00:02:18,840
You don't want to close your eyes, why you are walking?

21
00:02:18,840 --> 00:02:22,360
Open your eyes, but not look around.

22
00:02:22,360 --> 00:02:27,600
You remember, not look around, just try to focus your mindfulness on your feet, but not

23
00:02:27,600 --> 00:02:31,440
look at your feet, look over here, about two minutes.

24
00:02:31,440 --> 00:02:38,120
Because if you are look around, you can have good concentration, because you see outside

25
00:02:38,120 --> 00:02:39,520
object.

26
00:02:39,520 --> 00:02:46,520
So please try to be mindful, when you fall asleep, it's lifting, it's lifting, raising,

27
00:02:46,520 --> 00:02:52,480
dropping, lifting, raising, dropping.

28
00:02:52,480 --> 00:02:59,360
Every time you stand in, you keep your legs close together, and you feel to be powerful.

29
00:02:59,360 --> 00:03:06,200
And then you will return back, before you are turning back, you have to stay inside your

30
00:03:06,200 --> 00:03:18,040
mind, you will turn back, before you are turning back, you have to stop with three

31
00:03:18,040 --> 00:03:19,040
times.

32
00:03:19,040 --> 00:03:25,440
Please watch, when you are turning, you don't want to press your foot on the floor, just

33
00:03:25,440 --> 00:03:31,560
touching a little bit, touch your heel on the floor a little bit, not pushing, because

34
00:03:31,560 --> 00:03:38,560
if you press in, it's difficult for you to turn, just touching, turning, turning, like

35
00:03:38,560 --> 00:03:52,520
this, this first up, and then second, turning, turning, then second, third time, turning,

36
00:03:52,520 --> 00:03:57,080
turning, three times to be straight, as you stand before.

37
00:03:57,080 --> 00:04:06,400
And then you say standing again, standing, standing, standing, standing three times, and

38
00:04:06,400 --> 00:04:16,360
then walking again, lifting, raising, dropping, lifting, raising, dropping, lifting,

39
00:04:16,360 --> 00:04:29,960
lifting, raising, dropping, lifting, raising, dropping, lifting, raising, dropping.

40
00:04:29,960 --> 00:04:38,200
This is walking meditation, and then after you finish walking, you are sitting, right?

41
00:04:38,200 --> 00:04:42,280
When you are sitting, you prepare your sitting question already.

42
00:04:42,280 --> 00:04:51,000
So lifting, raising, dropping, lifting, raising, dropping, you step around here, not in

43
00:04:51,000 --> 00:04:54,640
a middle, and you can see what, why?

44
00:04:54,640 --> 00:05:06,000
You step around here, and then lifting, raising, dropping, standing, standing, standing,

45
00:05:06,000 --> 00:05:09,680
it means after you finish walking, you are sitting, right?

46
00:05:09,680 --> 00:05:15,840
Before you are sitting, you have to move your hand hand by hand, that is, moving, moving,

47
00:05:15,840 --> 00:05:24,320
moving, be aware your hand is moving, moving, moving, moving, then sitting slowly, be mindful.

48
00:05:24,320 --> 00:05:28,520
When you are sitting, you open your knees, that is, because if you are not open, it's difficult

49
00:05:28,520 --> 00:05:30,200
when you are sitting.

50
00:05:30,200 --> 00:05:35,440
So you open, that is, there, see?

51
00:05:35,440 --> 00:05:43,240
After that, you point your feet back, then you move your left leg, put your right leg on

52
00:05:43,240 --> 00:05:50,640
your right leg, and then you sit in, okay?

53
00:05:50,640 --> 00:06:02,960
After that, you put your hands on your knees, like this, keep your body straight, relax, breathing,

54
00:06:02,960 --> 00:06:09,280
about two, three times, and then close your eyes, start to move your left hand side, start

55
00:06:09,280 --> 00:06:15,520
to move your left hand side, like this, moving, moving, moving, turning, turning, turning,

56
00:06:15,520 --> 00:06:22,640
be aware, lowering, lowering, touching, moving, moving, moving, turning, turning, turning,

57
00:06:22,640 --> 00:06:24,520
lowering, touching.

58
00:06:24,520 --> 00:06:32,880
This is sitting position, the proper way, your hands to be like this, why you have to

59
00:06:32,880 --> 00:06:33,880
do like this?

60
00:06:33,880 --> 00:06:42,240
Because if you, you left your hand like this, if you have good concentration, if you

61
00:06:42,240 --> 00:06:50,840
have good concentration, your hands will be like this all the time, it's not moving, your

62
00:06:50,840 --> 00:06:58,400
body will be straight all the time, but opposite, if your concentration is weak, all your

63
00:06:58,400 --> 00:07:08,280
five minutes your hand out, that is, and maybe that is, but you try, after you close your

64
00:07:08,280 --> 00:07:14,000
eyes, you fix your mind on the abdomen, stretch in order, rising, falling, rising, falling,

65
00:07:14,000 --> 00:07:17,440
just fix your mind on the abdomen, okay?

66
00:07:17,440 --> 00:07:24,520
If you can see your abdomen, rising and falling, moment to moment, you can have good concentration,

67
00:07:24,520 --> 00:07:32,640
opposite, if your concentration is weak, you cannot see your abdomen rising and falling,

68
00:07:32,640 --> 00:07:38,800
if you practice at home, you don't worry about that, sometime maybe you can see your abdomen

69
00:07:38,800 --> 00:07:44,200
rising and falling, but sometime you cannot see your abdomen rising and falling, but you

70
00:07:44,200 --> 00:07:54,440
don't worry about that, if you practice at home, you can note that counting your breathing,

71
00:07:54,440 --> 00:07:59,760
you can counting your breathing, breathing out, counting one, in, out two, in, out three,

72
00:07:59,760 --> 00:08:08,160
up to ten, and then recount again, and you can lay the meditation, you can practice meditation

73
00:08:08,160 --> 00:08:16,480
or lay the opposition too, so you don't worry about that, after you're sitting after yourself,

74
00:08:16,480 --> 00:08:24,120
you close your eyes, kill your body's head, fix your mind on the abdomen, rising, falling,

75
00:08:24,120 --> 00:08:30,280
rising, falling, but you don't want to pause, you don't want to press, you don't want to

76
00:08:30,280 --> 00:08:37,920
do that, because if you do like that, your weak, your tired, breathing normally, just try

77
00:08:37,920 --> 00:08:41,760
to note that you're breathing in and out, okay?

78
00:08:41,760 --> 00:08:47,040
And then after ten minutes, maybe after ten or fifteen minutes, your legs start to have

79
00:08:47,040 --> 00:08:52,040
some pain or numb, if your legs pain, you know pain, pain, pain, pain, three times, then

80
00:08:52,040 --> 00:08:57,200
come back to your abdomen, if your legs numb, you know that numbness, numbness, numbness,

81
00:08:57,200 --> 00:09:04,840
and then come back to your abdomen, if you are thinking of something, you're just not

82
00:09:04,840 --> 00:09:10,720
at thinking, thinking, thinking, if you're hearing something, you're not at hearing hearing,

83
00:09:10,720 --> 00:09:18,520
hearing, this is very important for all of you, remember, okay?

84
00:09:18,520 --> 00:09:26,200
Now after that, if you cannot sit in for longer, maybe because up, you are distracting

85
00:09:26,200 --> 00:09:33,600
or you're thinking too much, or if you cannot sit in because up your legs pain or numb,

86
00:09:33,600 --> 00:09:42,520
you will like to stand up, you have to open your eyes slowly, your life, opening, opening,

87
00:09:42,520 --> 00:09:48,440
opening, opening again, move your hand by hand, moving, moving, moving, moving, moving,

88
00:09:48,440 --> 00:09:55,000
moving, moving, that is, and then if your legs numb, you just stretch, move your legs

89
00:09:55,000 --> 00:10:02,080
a little bit, and then numbness will disappear, your pain will disappear, okay?

90
00:10:02,080 --> 00:10:14,280
This is the situation, but remember that after you sat, that is, if your legs pain, you

91
00:10:14,280 --> 00:10:19,320
don't want to move your hand, you don't want to move your body at all, please try to keep

92
00:10:19,320 --> 00:10:24,920
like this all the time, as long as possible, because if you move your hand at this and

93
00:10:24,920 --> 00:10:31,120
then sitting, you are concentration is broken already, you don't want to sit in quite

94
00:10:31,120 --> 00:10:37,920
the new eyes, again, so if you open your eyes and move your hands, move your body, move

95
00:10:37,920 --> 00:10:44,240
your legs, you have to restart again, walking again, because when you are walking, making

96
00:10:44,240 --> 00:10:52,760
meditation, it means you try to develop your concentration, if you have full concentration

97
00:10:52,760 --> 00:10:59,240
while you are walking, when you are sitting meditation is good for you to have full concentration,

98
00:10:59,240 --> 00:11:03,520
so try to be mindful while you are walking, okay?

99
00:11:03,520 --> 00:11:09,320
So after you are sitting for longer, you will let you stand up, open your eyes and then

100
00:11:09,320 --> 00:11:18,760
move your hand by hand, like this, moving, moving, moving and then move your leg, standing

101
00:11:18,760 --> 00:11:24,600
up, he is done about sitting, okay?

102
00:11:24,600 --> 00:11:31,040
I think you can remember, now we are walking and then we start to walk, everybody start

103
00:11:31,040 --> 00:11:44,640
your right leg, start your right leg, lifting, raising, dropping, lifting, raising, dropping,

104
00:11:44,640 --> 00:12:09,620
lifting, raising, dropping, lifting, rising, dropping, lifting, dropping, lifting,

105
00:12:09,620 --> 00:12:23,620
Raising, dropping, lifting, raising, dropping, lifting, raising, dropping, lifting,

106
00:12:23,620 --> 00:12:25,620
raising, dropping.

107
00:12:25,620 --> 00:12:34,180
One more step and step, lifting, raising, dropping, everybody standing, sit your legs across

108
00:12:34,180 --> 00:12:41,180
together, open your feet a little bit. Now you say inside your mind, please pay attention,

109
00:12:41,180 --> 00:12:52,180
standing, feel where your body's standing, standing, standing, then turn, turn on the right,

110
00:12:52,180 --> 00:13:05,180
turning, turning, turning, turning, turning, turning, turning, turning, turning, turning, turning,

111
00:13:05,180 --> 00:13:15,780
turning, turning three times, stopping to be spread okay, standing, standing, standing,

112
00:13:15,780 --> 00:13:30,140
Walking, lifting, raising, dropping, lifting, raising, dropping,

113
00:14:00,140 --> 00:14:13,280
Daping, lifting, racing, dropping, lifting, racing, dropping, lifting, wrapping,

114
00:14:13,280 --> 00:14:21,960
almost every stop, lifting, racing, dropping,standing, standing,

115
00:14:21,960 --> 00:14:45,580
standing, standing, turning, turning, turning, turning, turning, turning, turning, turning, turning, turning

116
00:14:45,580 --> 00:14:59,660
lifting, raising, dropping, lifting, raising, dropping, lifting,

117
00:14:59,660 --> 00:15:17,300
Reapping, raising, dropping, lifting, raising, dropping, lifting, raising, dropping,

118
00:15:47,300 --> 00:16:17,280
Standing, turning, turning, turning, turning, turning, turning, turning, turning, turning, turning, turning, turning, turning, standing, standing, standing, working, lifting, breathing, dropping,

119
00:16:17,280 --> 00:16:20,360
ACTING RATION

120
00:16:20,360 --> 00:16:21,200
DROPING

121
00:16:21,200 --> 00:16:22,540
RArite

122
00:16:22,540 --> 00:16:24,120
RATION

123
00:16:24,120 --> 00:16:26,560
LOVE VING

124
00:16:26,560 --> 00:16:35,180
ED

125
00:16:35,180 --> 00:16:35,860
play

126
00:16:35,860 --> 00:16:37,440
Acting

127
00:16:37,440 --> 00:16:39,240
ROCING

128
00:16:39,240 --> 00:16:40,580
PLAY VING

129
00:16:40,580 --> 00:16:41,960
Ruko

130
00:16:42,200 --> 00:16:44,240
Dvalping

131
00:16:44,240 --> 00:16:46,380
Litates

132
00:16:46,380 --> 00:17:01,520
dropping, lifting, raising, dropping, lifting, raising, dropping,

133
00:17:01,520 --> 00:17:18,760
lifting, raising, dropping, lifting, dropping, standing, standing

134
00:17:18,760 --> 00:17:33,140
turning turning turning turning turning turning turning

135
00:17:33,140 --> 00:17:52,420
standing walking, lifting, raising, dropping, lifting, raising, dropping, lifting, dropping,

136
00:17:52,420 --> 00:18:20,420
lifting, raising, dropping, lifting, raising, dropping,besking, raising, dropping, advancing,

137
00:18:20,420 --> 00:18:34,940
Drop it, lifting, raising, dropping, lifting, raising, dropping, lifting, raising, dropping,

138
00:18:34,940 --> 00:19:03,980
standing, standing, standing, turning, turning, turning, turning, standing, standing,

139
00:19:03,980 --> 00:19:32,060
walking, lifting, racing, dropping,

140
00:19:32,060 --> 00:19:39,340
lifted, pressing, dropping, lifting, raising,

141
00:19:39,340 --> 00:19:43,680
dropping, lifting, raising, dropping,

142
00:19:43,680 --> 00:19:55,740
lifting, raising, dropping,

143
00:19:55,740 --> 00:20:01,120
raising, dropping, later lifting,

144
00:20:01,120 --> 00:20:10,120
lifting, dropping, lifting, racing, dropping, lifting, racing,

145
00:20:10,120 --> 00:20:23,360
dropping, lifting, racing, dropping,

146
00:20:23,360 --> 00:20:30,360
Standing, standing, standing

147
00:20:53,360 --> 00:21:00,360
standing, standing, standing, standing

148
00:21:23,360 --> 00:21:30,360
standing, standing, standing, standing, standing, standing

149
00:21:53,360 --> 00:21:55,360
You

150
00:22:23,360 --> 00:22:25,360
You

151
00:22:53,360 --> 00:22:55,360
You

152
00:23:23,360 --> 00:23:25,360
You

153
00:23:53,360 --> 00:23:55,360
You

154
00:24:23,360 --> 00:24:25,360
You

155
00:24:53,360 --> 00:24:55,360
You

156
00:25:23,360 --> 00:25:25,360
You

157
00:25:53,360 --> 00:25:55,360
You

158
00:26:23,360 --> 00:26:25,360
You

159
00:26:53,360 --> 00:26:55,360
You

160
00:27:23,360 --> 00:27:25,360
You

161
00:27:53,360 --> 00:27:55,360
You

162
00:28:23,360 --> 00:28:25,360
You

163
00:28:53,360 --> 00:28:55,360
You

164
00:29:23,360 --> 00:29:25,360
You

165
00:29:39,480 --> 00:29:41,480
Okay, it comes over

166
00:29:43,040 --> 00:29:44,520
Hey, what do you open your eyes?

167
00:29:44,520 --> 00:29:59,520
Open your eye move your hand hand my hand moving moving moving moving moving and move your legs if your legs numb of pain you can stretch your legs

