1
00:00:00,000 --> 00:00:03,840
Meditation, progress, questions, and answers.

2
00:00:05,120 --> 00:00:07,040
Progress in the practice.

3
00:00:08,000 --> 00:00:12,480
How does one know if one is progressing in the practice?

4
00:00:13,440 --> 00:00:19,840
Progress is a tricky thing in a sense. The whole idea of progress is misleading.

5
00:00:19,840 --> 00:00:26,560
Where are you trying to progress to? What are you trying to achieve? Progress in meditation

6
00:00:26,560 --> 00:00:32,240
is about giving up and letting go, not becoming or obtaining something new.

7
00:00:32,240 --> 00:00:39,440
Any consent that you are not getting anything out of meditation should be noted objectively

8
00:00:39,440 --> 00:00:48,960
and discarded. I'll shoot my meditation progress after the initial stages of concentration

9
00:00:48,960 --> 00:00:54,800
of the breath and being witnessed to a clear thought, and when will I know I have arrived?

10
00:00:54,800 --> 00:01:02,240
The meditation is a long and gradual path gradually. We see that there is nothing we could

11
00:01:02,240 --> 00:01:09,280
hold onto cling to or try to shape and move into the perfect reality that would make us happy.

12
00:01:09,280 --> 00:01:15,680
And so we let go. The clear thought of mindfulness practice helps you to let go and be here

13
00:01:15,680 --> 00:01:21,440
and now without wanting to be somewhere else or something else than what you are.

14
00:01:21,440 --> 00:01:26,800
When you create a clear thought, you will see clearly if you practice correctly,

15
00:01:26,800 --> 00:01:32,000
you will see and learn things about yourself that you did not know before.

16
00:01:32,000 --> 00:01:36,720
You will come to see things about yourself that you could not see before,

17
00:01:36,720 --> 00:01:42,400
which will help you to deal with all of the challenges in life in a more rational,

18
00:01:42,400 --> 00:01:49,520
wise and productive way. In meditation practice, you will encounter many hindrances.

19
00:01:49,520 --> 00:01:53,760
Some hindrances will make you think that you have hit a roadblock,

20
00:01:53,760 --> 00:02:02,400
states of liking, disliking, boredom, worry, depression, etc. Your mind will change constantly

21
00:02:02,400 --> 00:02:08,560
as you practice and not every change will be positive. The mind is always changing.

22
00:02:08,560 --> 00:02:13,440
You might think that when you practice, you are just going to get happier and happier,

23
00:02:13,440 --> 00:02:42,000
but mindfulness forces you to face both positive and the negative inside of yourself. Also, of negative mind states can arrive during meditation practice and you should work to catch them and to apply mindfulness to each one as it arises. Progress in the path is working through these negative states until they are no longer triggered in the ways they used to be triggered.

24
00:02:42,000 --> 00:02:51,360
The best answer for when you have arrived at the goal is that you have no more potential for greed, anger or delusion to arise.

25
00:02:51,360 --> 00:03:06,000
When you just see things as they are, when you are content regardless of how things are and when nothing has the potential to trigger stress or suffering in your mind, that is when you've reached the goal.

26
00:03:06,000 --> 00:03:20,000
Every time you sit down, you should learn something new about reality. If you are practicing and never learning anything, no matter how small, then I would say that maybe you are doing something wrong.

27
00:03:20,000 --> 00:03:35,320
There are four things that we intend to learn from the practice. The first thing that you should learn from the practice is the nature of experiential reality, both inside and in the world around you.

28
00:03:35,320 --> 00:03:45,240
You can't learn that reality is made up of seeing, hearing, smelling, tasting, feeling and thinking.

29
00:03:45,240 --> 00:03:52,280
And that's this intent, a composed of physical and mental moments of experience.

30
00:03:52,280 --> 00:04:06,880
The second thing that you learn is impermanence, suffering and non-so. You will see that the things you cling to are not worth thinking to. They are impermanence, unsatisfying and uncontrollable.

31
00:04:06,880 --> 00:04:31,680
Lead to you to weaken your grasp on the things you experience. Finally, your learning will culminate in realization of the four noble truths, specifically the noble path and the noble fruit. The path is the eradication of certain defilements on reaching the path of Sotapana, Sakadadami, Anagami and Adahant.

32
00:04:31,680 --> 00:04:39,040
The fruition is the experience of freedom from suffering caused by those defilements.

33
00:04:39,040 --> 00:04:50,520
Once you realize the third and fourth knowledge's questions about progress, do not have the same weight because you already know what the goal is.

34
00:04:50,520 --> 00:05:02,840
You only work from then on if to attain these knowledges like gain and gain, we can aid your defilements more and more until realizing this super mundane knowledges.

35
00:05:02,840 --> 00:05:13,240
You should look to understand your experiences, sing them as body and mind that they are not worth clinging to and how clinging leads to suffering.

36
00:05:13,240 --> 00:05:20,240
Seeing these things is a sign of progress in the practice.

