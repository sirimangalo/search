1
00:00:00,000 --> 00:00:17,280
Good evening, everyone broadcasting live March 26th.

2
00:00:17,280 --> 00:00:29,360
The day's quote is a excuse me for saying it's a generic quote because there's nothing

3
00:00:29,360 --> 00:00:44,080
really generic about it except it is something that appears in the diptica in lots of different places.

4
00:00:44,080 --> 00:01:01,040
In fact, in fact, I'm not sure that this is where he's taking it from, but it's the references

5
00:01:01,040 --> 00:01:13,040
to the silica suta, which is to chan-chun-chun-da, sorry.

6
00:01:13,040 --> 00:01:23,520
The silica suta is a very interesting suta, whereas it's actually quite dense, but Mahasi

7
00:01:23,520 --> 00:01:29,680
Sayyada wrote a book on it, gave talks and then turned it into a book which is very much

8
00:01:29,680 --> 00:01:30,680
worth reading.

9
00:01:30,680 --> 00:01:35,680
So like a suta commentary by Mahasi Sayyada, very much worth reading.

10
00:01:35,680 --> 00:01:37,240
That's not what this quote is about, though.

11
00:01:37,240 --> 00:01:41,760
The quote is tacked on at the end.

12
00:01:41,760 --> 00:01:52,640
Young calls, sadhara, karini, karini and sawakan, whatever teacher, whatever should be

13
00:01:52,640 --> 00:02:14,200
done by a teacher for his students, it is in anukampakana anukampamupa daya, out of compassion,

14
00:02:14,200 --> 00:02:25,720
with reference to compassion, the teacher who has, who is compassionate, who is thinking

15
00:02:25,720 --> 00:02:40,720
of or desiring the welfare of those students, katang wotang maya, done for you that is by

16
00:02:40,720 --> 00:02:51,680
me, done for you that by me, it's very yoda, pali, katang dhan wul, for you dhan vat maya

17
00:02:51,680 --> 00:02:58,640
by me, katang wotang maya, I have done that for you, for that has been done by, for you

18
00:02:58,640 --> 00:03:06,880
by me, the very passive, they have a lot of passive in pali and Sanskrit, twistings around,

19
00:03:06,880 --> 00:03:22,000
instead of saying, I have done that for you, they say that was done for you by me, I have

20
00:03:22,000 --> 00:03:27,640
done what I have done for you and I need to do as your teacher, as I've taught you what

21
00:03:27,640 --> 00:03:45,760
you need to know, basically, itani over there to Rukamulani, those tree roots, itani

22
00:03:45,760 --> 00:04:04,760
is Sunyagara, Sunyagara, ni, over there empty huts, jaya tat, jaya tat, jaya tat, dunda, meditate,

23
00:04:04,760 --> 00:04:22,960
dunda, mābamāda tat, don't be negligent, mābach havipatisārino ahuvata, ahuvata

24
00:04:22,960 --> 00:04:29,960
is interesting, I think that's like a future perfect or something, don't be one who

25
00:04:29,960 --> 00:04:38,480
has, you know, ahuvata is, what is that, maybe that's in, that's some punch in me or

26
00:04:38,480 --> 00:04:59,880
something, I forget, don't be one who is Vipatisārino who feels guilty afterwards,

27
00:04:59,880 --> 00:05:15,800
or is vexed by their negligence afterwards, mābach havipatisārino ahuvata, ayanko

28
00:05:15,800 --> 00:05:31,580
amha kang anu sasa ni, this is my exhortation to all of you, this is the Buddhist

29
00:05:31,580 --> 00:05:45,280
standard exhortation and it's interesting for many reasons, but for the one reason

30
00:05:45,280 --> 00:06:03,400
specifically that, it hints at the idea of simplicity or contentment in learning, you

31
00:06:03,400 --> 00:06:17,520
say, look, I've touched you now, enough of that, it's basically saying enough, that's

32
00:06:17,520 --> 00:06:32,400
enough, now go meditate, directing the conversation, directing the attention of the students,

33
00:06:32,400 --> 00:06:37,680
and I teach meditation at McMaster, these five-minute meditation lessons, it's interesting

34
00:06:37,680 --> 00:06:44,920
the range of responses, I don't have enough feedback yet to gauge what the fruit of

35
00:06:44,920 --> 00:06:56,880
teaching is, but I get the feeling that for some people it's, it's, they're just looking

36
00:06:56,880 --> 00:07:07,760
for information, maybe the idea of meditation sounds good or something, but absolutely

37
00:07:07,760 --> 00:07:19,880
not all, many people are actually into it, but sometimes it's easy to say, I want to

38
00:07:19,880 --> 00:07:26,760
meditate, it's easy to say, I should meditate, it's easy to pick up a book and learn

39
00:07:26,760 --> 00:07:32,920
how to meditate, it's easy to take a lesson on meditation, but it's a whole other thing

40
00:07:32,920 --> 00:07:39,280
to sit down and say, oh yeah, you know, what that means is actually meditating, actually

41
00:07:39,280 --> 00:08:00,560
paying attention, actually doing something, jayatat, jayatat, jayatat, meditate, and

42
00:08:00,560 --> 00:08:06,720
this, why it's in, why that's interesting to me is because I get lots of questions and

43
00:08:06,720 --> 00:08:16,160
lots of inquiries, people, you know, I've said talked about many times asking questions

44
00:08:16,160 --> 00:08:24,280
about their life, how do I deal with this, how do I deal with that? And this quote speaks

45
00:08:24,280 --> 00:08:35,280
to that, it speaks to me in that way that it reflects some of the dilemma there of wanting

46
00:08:35,280 --> 00:08:51,880
to help people, but trying to explain to people what is really helpful, it's not talking

47
00:08:51,880 --> 00:08:58,080
or there's no solution, there's not like there's anything I'm going to say that's going

48
00:08:58,080 --> 00:09:04,280
to fix your problem and then aha, that information will allow me to solve my problems,

49
00:09:04,280 --> 00:09:05,280
it's not it.

50
00:09:05,280 --> 00:09:16,920
And then we want to fix things, we still want to fix everything, we're in fact that's really

51
00:09:16,920 --> 00:09:25,240
the wrong way of looking at things, there is no fix, we have to begin to accept that things

52
00:09:25,240 --> 00:09:33,040
are broken, that there's an intrinsic, like Leonard Cohen said, there's a crack in everything

53
00:09:33,040 --> 00:09:54,040
is broken, everything is broken and to stop trying to find solutions and to start understanding

54
00:09:54,040 --> 00:10:04,360
problems, to understanding the nature of the experiences behind the problems and so on.

55
00:10:04,360 --> 00:10:09,400
So in that vein, I'm not going to talk too much, probably already talked more than I should,

56
00:10:09,400 --> 00:10:16,120
should have just said I, you see Buddha says, go meditate, that's enough for tonight, but

57
00:10:16,120 --> 00:10:27,200
now I have to answer, it's the duty of a teacher to instruct and to advise and to explain

58
00:10:27,200 --> 00:10:38,200
and so here I've hopefully done a little bit of that, anyone has any questions about

59
00:10:38,200 --> 00:10:53,240
their meditation, I will post hang out, I missed second life today, though it sounds like

60
00:10:53,240 --> 00:10:57,320
maybe I had already discussed that I would be missing and I just, it just totally skipped

61
00:10:57,320 --> 00:11:02,520
my mind so probably at some point I did say I'm going to probably be too busy this

62
00:11:02,520 --> 00:11:17,520
we can remember, I'm writing my essay, slow going and so we've kind of worked out, sounds

63
00:11:17,520 --> 00:11:30,440
like a month of June, I'll be away in Asia, so if you want to meet me in Thailand and

64
00:11:30,440 --> 00:11:38,400
probably be there about two weeks, it's probably not worth going for two weeks to Thailand,

65
00:11:38,400 --> 00:11:48,280
if you're in Thailand or in Asia, welcome to come with me, I can even come meet my teacher

66
00:11:48,280 --> 00:12:04,760
but we'll probably just be a brief meeting, he's very old and probably still very tired,

67
00:12:04,760 --> 00:12:29,880
it's overworked, so yeah, that's something, that's everything else, that's all I can think

68
00:12:29,880 --> 00:12:36,680
of, no questions and I'm going to say good night, go meditate.

