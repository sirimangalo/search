1
00:00:00,000 --> 00:00:15,160
the mind still mostly on the mind. It's the one most important thing. First, when you practice

2
00:00:15,160 --> 00:00:22,360
meditation you get to taste the mind. All the things that you're tasting while you're here

3
00:00:22,360 --> 00:00:35,360
you get to taste the mind. Many people say there's that saying no, the mind is a terrible thing to taste, or is that waste, I'm not sure.

4
00:00:35,360 --> 00:00:50,360
There is a saying, the mind is a terrible thing to taste. It can be, you know, when you have to deal with what's going on in the mind.

5
00:00:50,360 --> 00:01:01,360
But from the sounds of it, at this point, if you think back, then you can see how the mind can be a terrible thing to taste.

6
00:01:01,360 --> 00:01:15,360
But now, after you've done the foundation course and you've done some retreats, you can see the difference.

7
00:01:15,360 --> 00:01:24,360
Anyone asks, is meditation of any value? Because you might forget, this is the nature of the mind. We forget what we've done in the past.

8
00:01:24,360 --> 00:01:34,360
So you have to keep this in mind and you have to do this. Now you have proof, and don't forget that meditation is valuable.

9
00:01:34,360 --> 00:01:40,360
Because it's now brought you to the point where the mind isn't such a bad thing to taste.

10
00:01:40,360 --> 00:01:51,360
Why is this? Why is it that actually the mind isn't? When you dig, dig, dig, dig, go very, very deep and eventually you come to the mind.

11
00:01:51,360 --> 00:02:00,360
It's not such a bad thing to experience or to be involved in. This is because the Buddha said, this is what we're getting in today.

12
00:02:00,360 --> 00:02:28,360
The Buddha taught that our mind is a bad thing. The mind is a rotten thing that we have to throw away.

13
00:02:28,360 --> 00:02:35,360
Because he often said that about the body. Because the body is something that we have to throw away.

14
00:02:35,360 --> 00:02:44,360
Before long, the body will lie on the earth. The Buddha said, what Kalingana?

15
00:02:44,360 --> 00:02:52,360
Nirakta, what Kalingana? Just like a child log. It's useless as a child piece of wood.

16
00:02:52,360 --> 00:03:05,360
There's no body that we cling to, you know, my body. Actually all that's happening is it's clinging to us and eventually it stops clinging.

17
00:03:05,360 --> 00:03:16,360
And we have to let it go. Some people can't, that's why we talk here about ghosts, no engraving earth. Because once in a while they can't let go and they're so clinging so much.

18
00:03:16,360 --> 00:03:25,360
At the body we have to let it go. It's something that is actually rotten inside or it's ripening and eventually it's going to go rotten and all the parts.

19
00:03:25,360 --> 00:03:32,360
But the mind on the other hand, the Buddha said, deep down in the mind, the mind is beautiful.

20
00:03:32,360 --> 00:03:39,360
It's radiant. The Buddha said, I'm going to be coming, deep down.

21
00:03:39,360 --> 00:03:46,360
So it's not that our mind is defiled. The mind is, it's nothing bad about our mind but whoa, what's wrong?

22
00:03:46,360 --> 00:04:07,360
He says, dancha, koh kah, kantu kah, upakil a se, upakil it comes, it becomes defiled.

23
00:04:07,360 --> 00:04:17,360
The mind becomes defiled by visiting a kantu kah, visiting defound.

24
00:04:17,360 --> 00:04:30,360
And the filaments that just pass through.

25
00:04:30,360 --> 00:04:34,360
And then he says that an ordinary person doesn't see that.

26
00:04:34,360 --> 00:04:42,360
So they might think there's something wrong with the mind when an ordinary person has depression or anger or greed.

27
00:04:42,360 --> 00:04:46,360
They can drive them crazy because they think that this is a part of them.

28
00:04:46,360 --> 00:04:50,360
This is why people take medication trying to change the mind.

29
00:04:50,360 --> 00:04:56,360
They take these medications and it attacks the brain but it doesn't get to the mind.

30
00:04:56,360 --> 00:05:12,360
But because they don't see that actually the mind is bright, the Buddha said, it's for that reason that I say there is no...

31
00:05:12,360 --> 00:05:20,360
There is no jitabalana, there is no development of mind for an ordinary person, for most people.

32
00:05:20,360 --> 00:05:25,360
Most people are unable to develop their mind because they can't see that deep down the mind.

33
00:05:25,360 --> 00:05:28,360
Deep down the mind is...

34
00:05:28,360 --> 00:05:36,360
And deep down the mind is radiant.

35
00:05:36,360 --> 00:05:45,360
But a person who has considered carefully and looked into the wise things is that this person is able to see that the mind is beautiful.

36
00:05:45,360 --> 00:05:51,360
The mind is radiant because they are able to practice meditation.

37
00:05:51,360 --> 00:06:00,360
And then the Buddha goes on, he says, in fact he gives an example of the mind of loving kindness.

38
00:06:00,360 --> 00:06:03,360
The mind of loving kindness for example.

39
00:06:03,360 --> 00:06:32,360
If Adshara, some katamatampeek, a bikhui, bikhui, maida kiptena, a sai wati, if a monk or anyone who takes on the life of a meditator,

40
00:06:32,360 --> 00:06:43,360
or to practice the mind of loving kindness, for example, even for an instant.

41
00:06:43,360 --> 00:06:58,360
So after you practice your meditation, you send love to the other meditators and your parents and your friends and your enemies.

42
00:06:58,360 --> 00:07:05,360
And to all beings, if you do it even just for a moment, just take a moment to send love.

43
00:07:05,360 --> 00:07:16,360
The Buddha said, this person, if it's this bikhui, their meditation is not in vain.

44
00:07:16,360 --> 00:07:31,360
We have Alita, Johnny, Janu, Alita, Janu, they are a person who practices Jana, not in vain, not without purpose.

45
00:07:31,360 --> 00:07:40,360
Practice is meditation. We have a team. They develop meditating with some purpose.

46
00:07:40,360 --> 00:07:52,360
They are a person who follows the teaching of the Buddha, Satu, Satsan, Nakarou, who undertakes the teaching of the Buddha, the teaching of the teacher.

47
00:07:52,360 --> 00:07:57,360
This is Buddha.

48
00:07:57,360 --> 00:08:25,360
He says, what do you think of, what could you say about when someone really develops it?

49
00:08:25,360 --> 00:08:38,360
He develops it extensively.

50
00:08:38,360 --> 00:08:50,360
The final thing he has to say is that, all Akusiladhamma and all Kuziladhamma, he was on this rabbit, must stop there.

51
00:08:50,360 --> 00:09:00,360
Kuziladhamma and Akusiladhamma, they have the mind as their manopu banga, manopu banga.

52
00:09:00,360 --> 00:09:08,360
They all have the mind as their forerunner. The mind comes before all Kuzilad and all Kuzilad.

53
00:09:08,360 --> 00:09:13,360
So you can put this all together. This is actually the same teaching of Kuziladhamma.

54
00:09:13,360 --> 00:09:24,360
We are talking about the mind. Our mind is beautiful. Our mind is not something that we should throw away or discard or try to run away from.

55
00:09:24,360 --> 00:09:31,360
The reason why it's so difficult to face the mind is that the mind is covered over in defilement.

56
00:09:31,360 --> 00:09:38,360
It's like spinning around like a top and throwing out all these defilements.

57
00:09:38,360 --> 00:09:44,360
In the beginning that's hard to see, but at this point you should be able to see that actually.

58
00:09:44,360 --> 00:09:48,360
The problem isn't the mind, the problem is the defilement.

59
00:09:48,360 --> 00:09:54,360
Actually you can feel good about yourself. You can feel good about the mind.

60
00:09:54,360 --> 00:10:00,360
Focus your life on the mind on keeping your mind free from these defilements.

61
00:10:00,360 --> 00:10:07,360
It's like you have to set up a guard as you have this wonderful treasure of a mind that can do so much good for you.

62
00:10:07,360 --> 00:10:17,360
But if it gets taken over by the defilements it will become your worst enemy.

63
00:10:17,360 --> 00:10:24,360
This is why he says that the development of the mind is great benefit.

64
00:10:24,360 --> 00:10:29,360
Even for just a moment you do a great thing.

65
00:10:29,360 --> 00:10:35,360
I think what's important about this teaching about a moment is to help us get over the idea of how long we're practicing.

66
00:10:35,360 --> 00:10:42,360
Even our day or week or month or years and just focus on the moment.

67
00:10:42,360 --> 00:10:51,360
Not think about how long it's going to take or how long we have to sit and meditate for how long we have to stay here or so.

68
00:10:51,360 --> 00:11:02,360
Think about just this moment. Don't think about oh in the past I was not a good meditator or I didn't meditate or I did a lot of bad things in the past.

69
00:11:02,360 --> 00:11:08,360
Only think of this one moment and you can you can be proud of yourself in that moment.

70
00:11:08,360 --> 00:11:15,360
You can feel good about yourself. You can feel good about your mind and confident about what you've done.

71
00:11:15,360 --> 00:11:21,360
Because when you work in moments then you're actually you're actually truly developing good things.

72
00:11:21,360 --> 00:11:25,360
You're truly developing good things. You can only develop goodness in a moment.

73
00:11:25,360 --> 00:11:29,360
You can't develop goodness an hour from now or an hour ago.

74
00:11:29,360 --> 00:11:33,360
Even a moment from now you can't develop it. You can only develop it now.

75
00:11:33,360 --> 00:11:36,360
And that's only one moment of goodness.

76
00:11:36,360 --> 00:11:47,360
The next moment maybe your mind changes and your into delusion or attachment or anger or hatred or so.

77
00:11:47,360 --> 00:11:52,360
So we should always think that this is a very important teaching to always think in terms of moments.

78
00:11:52,360 --> 00:11:56,360
This is why the Buddha said even for a moment we should think like that.

79
00:11:56,360 --> 00:11:59,360
Then we should count the moments not count the hours.

80
00:11:59,360 --> 00:12:04,360
Never count the hours of practice that you've done in weeks or the months or the years.

81
00:12:04,360 --> 00:12:10,360
Count the moments and then you think oh I've done a lot of meditation too much.

82
00:12:10,360 --> 00:12:13,360
I can't even count how much meditation.

83
00:12:13,360 --> 00:12:23,360
Or maybe you think I haven't even done many moments and done many hours but most of those hours were nodding off or thinking about other things.

84
00:12:23,360 --> 00:12:28,360
So you can see where the real meditation is.

85
00:12:28,360 --> 00:12:36,360
The Buddha said it's the mind that the mind that is defiled.

86
00:12:36,360 --> 00:12:42,360
All Akusana, Dhamma, all unwholesome Dhammas, all bad things we say.

87
00:12:42,360 --> 00:12:46,360
It all comes from when these defilements take over the mind.

88
00:12:46,360 --> 00:12:48,360
You can't have Akusana Dhamma without the mind.

89
00:12:48,360 --> 00:12:52,360
You can't have unwholesome without the judgments of the mind.

90
00:12:52,360 --> 00:12:59,360
There's the mind falls into liking and disliking, clinging to this one.

91
00:12:59,360 --> 00:13:05,360
It becomes averse to that.

92
00:13:05,360 --> 00:13:17,360
It leads us around in so much, so much delusion.

93
00:13:17,360 --> 00:13:24,360
So our practice is to find the pure mind inside, which is something for us to think about.

94
00:13:24,360 --> 00:13:27,360
That actually the mind is not the problem.

95
00:13:27,360 --> 00:13:38,360
You can see the mind and this is why it works for us to simply see things as they are because there's nothing wrong with things.

96
00:13:38,360 --> 00:13:44,360
There's nothing wrong with the world around us. There's nothing wrong with our mind.

97
00:13:44,360 --> 00:13:50,360
What's wrong is the judgments and the defilements that cover the mind up.

98
00:13:50,360 --> 00:14:00,360
When you can see things as they are in thinking or seeing, hearing, smelling, suddenly the mind is pure again.

99
00:14:00,360 --> 00:14:05,360
I think the mind is inherently dirty. It's not true.

100
00:14:05,360 --> 00:14:12,360
At the moment when you see seeing, you connect with the pure mind, your mind is pure.

101
00:14:12,360 --> 00:14:21,360
If you're clear, you'll know seeing. When you lift your foot, you'll know your lifting of what this is lifted.

102
00:14:21,360 --> 00:14:23,360
You have a clear mind.

103
00:14:23,360 --> 00:14:27,360
At that moment there's no defilements in the mind.

104
00:14:27,360 --> 00:14:35,360
When you know the movement and you know the object and you know the stomach,

105
00:14:35,360 --> 00:14:38,360
you know this is rising.

106
00:14:38,360 --> 00:14:41,360
Just knowing this is rising, the mind is pure.

107
00:14:41,360 --> 00:14:44,360
So we're not trying to create anything. We're trying to get rid of stuff.

108
00:14:44,360 --> 00:14:51,360
So then in the end all that's left is our awareness of things as they are.

109
00:14:51,360 --> 00:14:55,360
This, this, this, this, this.

110
00:14:55,360 --> 00:15:01,360
Certainly isn't something easy.

111
00:15:01,360 --> 00:15:08,360
But it's something we should feel confident that deep down inside we're perfectly pure.

112
00:15:08,360 --> 00:15:11,360
And just have to find that perfectly pure.

113
00:15:11,360 --> 00:15:13,360
Perfect pure.

114
00:15:13,360 --> 00:15:17,360
Get rid of all the interior. I think that's covering it up.

115
00:15:17,360 --> 00:15:22,360
Digging down to find the buried treasure.

116
00:15:22,360 --> 00:15:25,360
So just another short teaching.

117
00:15:25,360 --> 00:15:28,360
Something for us to think about us, we go through the,

118
00:15:28,360 --> 00:15:32,360
we try to just go through the Buddhist teaching.

119
00:15:32,360 --> 00:15:36,360
When you have an idea of what the Buddhist teach, I think even from just as brief,

120
00:15:36,360 --> 00:15:39,360
few teachings that we've gone through,

121
00:15:39,360 --> 00:15:44,360
you can see how much emphasis the Buddha placed on meditation and on, on the mind.

122
00:15:44,360 --> 00:15:49,360
So he didn't say studying or helping other people or someone.

123
00:15:49,360 --> 00:15:56,360
He said, developing the mind, even for just a moment, that's what makes your monastic life,

124
00:15:56,360 --> 00:16:00,360
the life of a monk worthwhile.

125
00:16:00,360 --> 00:16:04,360
Not to mention the life of all beings.

126
00:16:04,360 --> 00:16:07,360
So this is for us to develop.

127
00:16:07,360 --> 00:16:09,360
It's encouragement that we're on the right path.

128
00:16:09,360 --> 00:16:12,360
And we're doing, as the Buddha said in, just now he said,

129
00:16:12,360 --> 00:16:15,360
this is someone who follows his exhortation.

130
00:16:15,360 --> 00:16:20,360
This is someone who undertakes his teaching.

131
00:16:20,360 --> 00:16:25,360
Who performs the instruction in the Buddha.

132
00:16:25,360 --> 00:16:28,360
So we're practicing Buddhism here.

133
00:16:28,360 --> 00:16:32,360
And we have the Buddha to back us up.

134
00:16:32,360 --> 00:16:36,360
So without further ado, we'll move on.

135
00:16:36,360 --> 00:16:38,360
We'll get on with our meditation.

136
00:16:38,360 --> 00:17:03,360
We can all go back in the mind for frustration walking.

