WEBVTT

00:00.000 --> 00:10.160
It's a goal, another goal, not even having a measure of rice.

00:10.160 --> 00:20.840
Suppose they come into debt, because they have no money, they have to borrow money just

00:20.840 --> 00:31.160
to get food, maybe they have to borrow money to get a place to stay, borrow money

00:31.160 --> 00:36.320
just to get a bowl in which to eat out of.

00:36.320 --> 00:38.480
Isn't that great suffering?

00:38.480 --> 00:41.320
Debt, no?

00:41.320 --> 00:48.840
Many people, even if they're not incredibly poor, can understand the fear and the anguish

00:48.840 --> 00:50.960
that comes from being in debt.

00:50.960 --> 01:04.440
The feeling of helplessness, hopelessness, the feeling of slavery, of being in debt.

01:04.440 --> 01:24.440
So, listen to this also, suffering, and say, as indeed, and so, for one who has a debt,

01:24.440 --> 01:39.440
they have to pay interest on their debt, isn't that suffering, right, of course, because

01:39.440 --> 01:43.440
once you're in debt, it's not just about paying it back, sometimes all you can pay back

01:43.440 --> 01:44.440
is the interest.

01:44.440 --> 01:47.440
So, yes, indeed, that's greater suffering.

01:47.440 --> 01:51.720
He's building up to something here, it's actually not as simple as it sounds, he's got

01:51.720 --> 01:56.720
something, he's actually using this as a comparison to something, so there's more to this

01:56.720 --> 01:57.720
suit than this.

01:57.720 --> 02:05.520
Although even this, it's a good example of how sensuality can be used to suffering, because

02:05.520 --> 02:13.720
we often go into debt, and we suffer from debt due to our desires, wanting a big car,

02:13.720 --> 02:14.720
a big house.

02:14.720 --> 02:19.920
I have a couple of students in New Toronto, and they were telling, talking to me about this,

02:19.920 --> 02:24.920
they finally went to visit their house, and they might actually be listening, probably,

02:24.920 --> 02:25.920
they are listening.

02:25.920 --> 02:32.420
I mean, I'm explaining to me how they're going to sell this house, and before they

02:32.420 --> 02:41.220
bought this house, when they hadn't meditated yet, and many other people, they were caught

02:41.220 --> 02:46.920
up in the idea of having a really nice house, and indeed, their house is exceptionally nice.

02:46.920 --> 02:54.920
They don't have any children, so just the two of them in this big, nice, beautiful house,

02:54.920 --> 02:57.920
and then when they started meditating, they realized, you know, what is this?

02:57.920 --> 03:04.920
We've got, we're deeply in debt, and so they've decided to sell the house, and they're

03:04.920 --> 03:09.920
going to buy a house that is more reasonable, that they can still have to take care of their

03:09.920 --> 03:16.920
house, and have their parents move in with them, which is, of course, nice.

03:16.920 --> 03:23.920
But for those who are caught up in it, you see so many people buying these big, luxurious

03:23.920 --> 03:28.920
houses, living the rest of their lives in slavery.

03:28.920 --> 03:35.920
So interest, and for one who has interest, what else happens?

03:35.920 --> 03:46.920
They don't give the interest on time, it's really English, I'm going to mess this up.

03:46.920 --> 03:50.920
I was promised to pay interest, cannot pay it when it falls due.

03:50.920 --> 03:52.920
They reprove him.

03:52.920 --> 03:54.920
Isn't that suffering?

03:54.920 --> 03:55.920
Oh, yes.

03:55.920 --> 04:05.920
You've ever, I've sconded on a debt and had to deal with debt collection agencies.

04:05.920 --> 04:07.920
They are the worst.

04:07.920 --> 04:09.920
This once happened to my family.

04:09.920 --> 04:15.920
Some of them in our family had a long-standing debt student debt from many, many, many

04:15.920 --> 04:21.920
years before, and there was, they were still trying to catch up with her, with this person.

04:21.920 --> 04:31.920
They would call us and say nasty things to us, just anything they can do to make us upset.

04:31.920 --> 04:35.920
But you also have to deal with this as well.

04:35.920 --> 04:38.920
Anyway, isn't that suffering?

04:38.920 --> 04:39.920
Yes, it's suffering.

04:39.920 --> 04:45.920
And when they're not able to pay and they're chastised in this way, if they still don't

04:45.920 --> 04:51.920
pay, they'd persecute them, right? They'd take them to court.

04:51.920 --> 04:53.920
They find a way to make them pay.

04:53.920 --> 04:58.920
Maybe they repossessed their possessions, they do whatever they can, get a court in junction,

04:58.920 --> 05:03.920
they get them kicked out of their helm or whatever.

05:03.920 --> 05:07.920
And after being prosecuted, they even put them in prison.

05:07.920 --> 05:17.920
So this is the fate of a debtor, one who has debt.

05:17.920 --> 05:23.920
In and of itself, it's a somewhat stinging indictment of sensuality, right?

05:23.920 --> 05:26.920
Because this happens to many people.

05:26.920 --> 05:32.920
It happens to people even not living extravagant lifestyle, right?

05:32.920 --> 05:36.920
But it causes suffering when we have attachment to these things.

05:36.920 --> 05:40.920
When we've built up a stable life, and then this happens to us,

05:40.920 --> 05:42.920
I mean, it's great suffering in losing all that you have.

05:42.920 --> 05:46.920
Not having a home is certainly stressful, especially in the cold climate.

05:46.920 --> 05:47.920
What are you going to do?

05:47.920 --> 05:50.920
Having to go to homeless shelters or worse live on the street.

05:50.920 --> 05:52.920
It's great suffering.

05:52.920 --> 05:56.920
But it's much worse suffering for one who's attached to those things, right?

05:56.920 --> 06:01.920
It's devil suffering because those things had meaning to us, had value to us,

06:01.920 --> 06:10.920
because we clung to them, much more than just the functional value that they held.

06:10.920 --> 06:14.920
But it's more to the suit than actually the more wonderful thing about the suit is that

06:14.920 --> 06:22.920
he's only using this as a metaphor, an allegory.

06:22.920 --> 06:26.920
So he says, but it says,

06:26.920 --> 06:31.920
I suppose you have a person who doesn't have faith.

06:31.920 --> 06:34.920
He's like, this is what it's calling.

06:34.920 --> 06:40.920
It will be called even so monks.

06:40.920 --> 06:46.920
It will be.

06:46.920 --> 06:56.920
Even when they call it, even when they call it, they say that she has no sadha, no confidence.

06:56.920 --> 07:04.920
So in regards to the heteroism, person who doesn't think to keep moral precepts,

07:04.920 --> 07:08.920
doesn't have any interest in ethics, doesn't have any interest in meditation,

07:08.920 --> 07:12.920
has no thought to understand, to cultivate understanding.

07:12.920 --> 07:24.220
Here, in the tikus, the ace of dummies has no shame in regards to conscientiousness in regards to wholesome dummies.

07:24.220 --> 07:35.420
O, the pang that tikus, the ace of dummies, has no fear in regards to kus, no fear exactly, but something like that.

07:35.420 --> 07:43.420
In regards to wholesome dummies, the median that tikus, the ace of dummies, puts out no effort in regards to wholesome dummies.

07:43.420 --> 07:46.420
It doesn't work to cultivate wholesomeness.

07:46.420 --> 07:52.420
Banyan, tikus, the ace of dummies, has no wisdom in regards to wholesome dummies.

07:52.420 --> 07:56.420
Basically, it's devoid of any wholesome qualities.

07:56.420 --> 08:11.420
This is called umangks in the windy eye and the discipline of the noble ones.

08:11.420 --> 08:13.420
Alias, so windy.

08:13.420 --> 08:17.420
Dali, though, this is called a Dalit.

08:17.420 --> 08:22.420
We're in the Indian very familiar with the Dalit.

08:22.420 --> 08:26.420
This is called a Dalit in the Buddha's sassala.

08:26.420 --> 08:35.420
Asakou, one who has nothing, anahikou, not even a measure of rice, doesn't have the smallest bit of anything.

08:35.420 --> 08:36.420
Worthless.

08:36.420 --> 08:44.420
There's destitute, really, to point.

08:44.420 --> 08:54.420
Remember, this is the first one, it's a poor person.

08:54.420 --> 09:12.420
That poor destitute person in the dhamma, without any confidence, not having any shame or dread or energy with them.

09:12.420 --> 09:16.420
Such a person will kai in a duchary tung jharati.

09:16.420 --> 09:19.420
Will do evil deeds with the body.

09:19.420 --> 09:22.420
Wa jai a duchary tung jharati.

09:22.420 --> 09:25.420
Will do evil deeds with the speech.

09:25.420 --> 09:27.420
Manasa duchary tung jharati.

09:27.420 --> 09:29.420
Will do evil deeds with mind.

09:29.420 --> 09:36.420
Their mind will have evil thoughts.

09:36.420 --> 09:43.420
It's impoverished as someone who doesn't have wholesome dhammas right off the bat.

09:43.420 --> 09:50.420
But just like a person who's poor, the problem of being poor is you can't afford the things that you want.

09:50.420 --> 09:52.420
You can't afford good things.

09:52.420 --> 09:55.420
You can't afford things that bring happiness.

09:55.420 --> 10:03.420
Even much more so, a person devoid of wholesome qualities has no power to cultivate wholesome mind state,

10:03.420 --> 10:07.420
or to cultivate mind states that need to happiness.

10:07.420 --> 10:09.420
Happiness is not something that just falls on your lap.

10:09.420 --> 10:17.420
It doesn't even come just because you have a big house and a big car and beautiful clothes, delicious food.

10:17.420 --> 10:23.420
It doesn't come from any of that.

10:23.420 --> 10:26.420
Did you guys get chocolate today?

10:26.420 --> 10:29.420
Did you see the chocolate?

10:29.420 --> 10:35.420
It doesn't even come from chocolate.

10:35.420 --> 10:38.420
Huh?

10:38.420 --> 10:40.420
No, it's some chocolate.

10:40.420 --> 10:42.420
No, I said happiness doesn't come from chocolate.

10:42.420 --> 10:48.420
It was chocolate, but even chocolate doesn't bring happiness.

10:48.420 --> 10:51.420
It brings pleasure.

10:51.420 --> 10:54.420
Pleasure brings some happiness.

10:54.420 --> 10:59.420
But that's adulterated happiness.

10:59.420 --> 11:03.420
It's happiness that's associated with desire.

11:03.420 --> 11:05.420
It also makes you kind of edgy.

11:05.420 --> 11:11.420
You have to be careful not to eat too much chocolate as a meditator.

11:11.420 --> 11:13.420
Wherever it is.

11:13.420 --> 11:19.420
So a person as a result, they get in debt.

11:19.420 --> 11:26.420
So the debt in the Aryas had been in the Arya Vinaya as they do evil deeds.

11:26.420 --> 11:30.420
Not only that they can't do good deeds, but they also do evil deeds.

11:30.420 --> 11:33.420
Because they're devoid of any goodness.

11:33.420 --> 11:37.420
They fall prey to their own anger, their own greed, their own delusion.

11:37.420 --> 11:41.420
They hurt others, they hurt themselves, they hate others, they hate themselves,

11:41.420 --> 11:48.420
they desire things, they become addicted to things, they have conceited arrogance.

11:48.420 --> 11:51.420
And this is called debt.

11:51.420 --> 11:53.420
Karma is a debt.

11:53.420 --> 11:55.420
You ever heard of the karmic debt?

11:55.420 --> 12:00.420
A person who is in debt in Buddhism is a person who is cultivated unwholesomeness

12:00.420 --> 12:03.420
and just waiting to have to pay it back.

12:03.420 --> 12:06.420
Often they'll get into the cycle where they have to keep doing evil deeds

12:06.420 --> 12:12.420
just to push back the results.

12:12.420 --> 12:19.420
It's debelling down on their debt by running away from consequences,

12:19.420 --> 12:25.420
finding ways to dig themselves deeper and deeper.

12:25.420 --> 12:27.420
That's hard diction work.

12:27.420 --> 12:30.420
So you get addicted to something and then every time you indulge in the addiction

12:30.420 --> 12:36.420
you just dig yourself deeper and deeper, running away from the consequences.

12:36.420 --> 12:40.420
Of course the same goes with when you heard others and then in order to stop them

12:40.420 --> 12:47.420
burning you back you keep hurting them and keep acting in the nasty ways to keep them away.

12:47.420 --> 12:49.420
So that's debt.

12:49.420 --> 12:55.420
Debt in the Buddha's teaching is that evil karma that we do.

12:55.420 --> 12:59.420
Ida masai, yinada, nasmi, madami.

12:59.420 --> 13:05.420
This is called debt.

13:05.420 --> 13:10.420
I'm doing all those evil deeds.

13:10.420 --> 13:12.420
Let's see.

13:12.420 --> 13:15.420
Papi kangichang, pani dhati.

13:15.420 --> 13:18.420
Ma maang janyu, tea, tea.

13:18.420 --> 13:21.420
Ma maang janyu, tea, sang kapati.

13:21.420 --> 13:25.420
Ma maang janyu, tea wa jang vasa, tea.

13:25.420 --> 13:30.420
Ma maang janyu, kaii, napara kabati.

13:30.420 --> 13:32.420
They hide their evil deeds.

13:32.420 --> 13:34.420
This is what I was talking about.

13:34.420 --> 13:36.420
They hide their evil deeds.

13:36.420 --> 13:39.420
They may this person not.

13:39.420 --> 13:43.420
They wish, may people not know what I've done.

13:43.420 --> 13:50.420
They think they speak words to the effect or know they speak.

13:50.420 --> 13:53.420
The words may they not know me.

13:53.420 --> 13:55.420
They're not know what I've done.

13:55.420 --> 14:00.420
And they hide their karma data.

14:00.420 --> 14:04.420
They exert themselves with body.

14:04.420 --> 14:07.420
They do deeds to make sure that no one finds out what they've done.

14:07.420 --> 14:12.420
They speak words so that people live, for example.

14:12.420 --> 14:13.420
What is this called?

14:13.420 --> 14:15.420
This is the interest.

14:15.420 --> 14:20.420
This is compound interest on your loan.

14:20.420 --> 14:25.420
Making it worse, basically.

14:25.420 --> 14:31.420
The interest that you have to pay.

14:31.420 --> 14:36.420
You have to pay interest when you're not able to pay it back until you have wholesome.

14:36.420 --> 14:41.420
The wholesome qualities necessary to stop being such a terrible person.

14:41.420 --> 14:51.420
You pay the interest of having to hide it.

14:51.420 --> 14:58.420
That's number three, number four, well-behaved monks, fellow monks.

14:58.420 --> 15:00.420
That's not to say monks.

15:00.420 --> 15:01.420
It's not monks.

15:01.420 --> 15:04.420
Dummy nung bais salas, sabrammachari, your fellows in the holy life.

15:04.420 --> 15:06.420
So that's all of you guys.

15:06.420 --> 15:07.420
Fellows.

15:07.420 --> 15:08.420
It's all of us.

15:08.420 --> 15:09.420
It's a community.

15:09.420 --> 15:12.420
Our fellow, holy people.

15:12.420 --> 15:18.420
Fellow spiritual beings.

15:18.420 --> 15:23.420
Hello, religious.

15:23.420 --> 15:25.420
He were mung soom.

15:25.420 --> 15:27.420
They say, what do they say?

15:27.420 --> 15:28.420
They say good things.

15:28.420 --> 15:29.420
No, pay salas.

15:29.420 --> 15:31.420
These are the good people.

15:31.420 --> 15:32.420
The wise ones.

15:32.420 --> 15:33.420
Well, no, sorry.

15:33.420 --> 15:35.420
The well-behaved ones.

15:35.420 --> 15:38.420
I am just so I asma Ihwangari.

15:38.420 --> 15:45.420
Thus has this person done.

15:45.420 --> 15:48.420
Ihwang sabrammachari, they've acted in this way.

15:48.420 --> 15:52.900
Ihwang

15:52.900 --> 15:55.420
They say nasty things about this person.

15:55.420 --> 15:56.420
Not nasty things.

15:56.420 --> 15:57.420
They tell the truth about them.

15:57.420 --> 16:00.420
Everyone knows what they've done and the word spreads

16:00.420 --> 16:07.420
and who wants to be with such a person.

16:07.420 --> 16:09.420
When you go to meditation centers,

16:09.420 --> 16:12.420
you become known as the troublemaker.

16:12.420 --> 16:16.420
Thinking about it in society,

16:16.420 --> 16:21.420
as a mean and nasty person.

16:21.420 --> 16:25.420
People spread bad, bad report about you.

16:25.420 --> 16:28.420
This is when you're,

16:28.420 --> 16:30.420
because here's the word,

16:30.420 --> 16:35.420
a reprove to your reprove.

16:35.420 --> 16:37.420
Just as it's just like people,

16:37.420 --> 16:42.420
the people you've borrowed money from,

16:42.420 --> 16:45.420
hey, when are you going to pay me back?

16:45.420 --> 16:48.420
The person who's done evil deeds gets this.

16:48.420 --> 16:50.420
Hey, when are you going to pay back those evil deeds?

16:50.420 --> 16:55.420
When are you going to stop being such an evil, terrible person?

16:55.420 --> 16:59.420
They don't say that,

16:59.420 --> 17:02.420
but they're scolded for being such an evil person.

17:02.420 --> 17:04.420
No one likes.

17:04.420 --> 17:07.420
This is one of the reasons for just,

17:07.420 --> 17:12.420
for realizing the importance of true meditation.

17:12.420 --> 17:14.420
Just meditation that makes you feel happy with meditation.

17:14.420 --> 17:23.420
It helps you become a better person.

17:23.420 --> 17:30.420
That's number, number four.

17:30.420 --> 17:34.420
Number five,

17:34.420 --> 17:39.420
Ramyagatan wa rukkamulagatan wa sunyagaragatan wa

17:39.420 --> 17:42.420
having gone to the forest,

17:42.420 --> 17:44.420
having gone to the root of a tree,

17:44.420 --> 17:46.420
having gone to an empty place.

17:46.420 --> 17:53.420
Wipati sahara sahagata, papa kakus la vita kasamudajarati.

17:53.420 --> 17:59.420
They are overcome, they are remorseful.

17:59.420 --> 18:06.420
Evil and wholesome thoughts.

18:06.420 --> 18:12.420
Bad and wholesome thoughts accompanied by remorse.

18:12.420 --> 18:16.420
It's interesting, it's interesting use because it shows the Buddha's recognition

18:16.420 --> 18:20.420
that feeling remorse is not wholesome.

18:20.420 --> 18:22.420
It's not good that you feel remorse.

18:22.420 --> 18:26.420
It's naturally a feel remorse for doing bad deeds, but it's not good.

18:26.420 --> 18:30.420
So anyone who sits around feeling guilty about the bad things they've done

18:30.420 --> 18:32.420
isn't helping things.

18:32.420 --> 18:33.420
So I'm doing any benefit.

18:33.420 --> 18:35.420
And he makes this clear by saying there,

18:35.420 --> 18:40.420
a kusula vita ka, papa kadar, evil thoughts.

18:40.420 --> 18:42.420
So this person is so caught up in evil,

18:42.420 --> 18:45.420
and even the guilt that they feel is evil.

18:45.420 --> 18:49.420
They hate themselves from being such an evil person.

18:49.420 --> 18:51.420
They scold themselves, and that's more evil.

18:51.420 --> 18:56.420
It doesn't actually make you a better person.

18:56.420 --> 18:59.420
It might argue it comes from a knowledge of having,

18:59.420 --> 19:03.420
it's better than the opposite, which is feeling good about yourself

19:03.420 --> 19:06.420
because you've done evil things.

19:06.420 --> 19:10.420
But it's not the right response.

19:10.420 --> 19:12.420
It's a natural response.

19:12.420 --> 19:15.420
The better response is to start smartening up and working

19:15.420 --> 19:18.420
and becoming a better person than changing your way,

19:18.420 --> 19:21.420
realizing that what you've done is wrong and working

19:21.420 --> 19:27.420
and working as a result to change.

19:27.420 --> 19:32.420
It might not even be guilt, but it's fear and well,

19:32.420 --> 19:35.420
yeah, guilt, I guess it's remorse.

19:35.420 --> 19:36.420
So what do we call this?

19:36.420 --> 19:43.420
This is called being persecuted, prosecuted.

19:43.420 --> 19:48.420
Like the wording, no, this is like a court.

19:48.420 --> 19:49.420
It's not a person.

19:49.420 --> 19:50.420
It's not people.

19:50.420 --> 19:52.420
This is when you're off in the forest alone.

19:52.420 --> 19:58.420
That's when the law becomes persecute.

19:58.420 --> 20:01.420
The law persecutes you.

20:01.420 --> 20:05.420
You become an outlaw, a karmic outlaw,

20:05.420 --> 20:10.420
living off in the frontier.

20:10.420 --> 20:13.420
But you can't escape karma.

20:13.420 --> 20:15.420
No one can escape karma.

20:15.420 --> 20:20.420
It's my old friend Udi used to say.

20:20.420 --> 20:24.420
I'm going to be prosecuted at number six.

20:24.420 --> 20:25.420
I eventually put in jail.

20:25.420 --> 20:29.420
What does it mean to be put in jail in the Buddha's teaching?

20:29.420 --> 20:33.420
It's this poor number.

20:33.420 --> 20:37.420
We're talking about this person in terms of this destitute

20:37.420 --> 20:42.420
impoverished, penniless person having done evil deeds

20:42.420 --> 20:45.420
with body speech in mind.

20:45.420 --> 21:09.420
That's it.

