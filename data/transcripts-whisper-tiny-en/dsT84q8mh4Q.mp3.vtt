WEBVTT

00:00.000 --> 00:11.160
Okay, so welcome everyone. Happy New Year. I think some technical difficulties here.

00:11.160 --> 00:24.960
So we're talking about tonight the basics. Getting back into it. Back into the meditation.

00:24.960 --> 00:34.120
One is a training. Just not about taking a break from your problems. It's about gaining

00:34.120 --> 00:47.400
the skill of what we call mindfulness. So the idea is that by being here you are cultivating

00:47.400 --> 00:59.680
something new. You are changing something about yourself. Changing the way you look at

00:59.680 --> 01:19.200
things. The way you react to your experience is the way you react to the world.

01:19.200 --> 01:24.920
You're learning about your experiences. You're learning about your mind. And learning about

01:24.920 --> 01:34.640
the way you hurt and the way you help yourself. The ways in which we cause ourselves suffering,

01:34.640 --> 01:47.400
particularly. Because it's not the experiences that cause us suffering. It's our reactions

01:47.400 --> 01:56.280
to them. So that's how we can see that meditation is quite easy. It's quite meditation itself

01:56.280 --> 02:06.960
isn't the problem. Running away it doesn't solve things. It's always a shame when people

02:06.960 --> 02:11.800
leave the center because they come to a place where they can actually deal with their

02:11.800 --> 02:25.120
problems and they're not able to. It's not something to be ashamed of, but it's a shame.

02:25.120 --> 02:33.760
Let's shame that they're not able to take the right direction. So this is what we do in

02:33.760 --> 02:44.320
life. We find solutions. My body is in pain. I'll just move or I'll change my position.

02:44.320 --> 02:55.600
I'll find some way to alleviate the pain. We have bad thoughts. We find some way to distract

02:55.600 --> 03:12.560
ourselves. If we have hard and odious tasks, we find ways to, ways to avoid or to compensate.

03:12.560 --> 03:23.560
So we allow ourselves to feel such stress and suffering. Instead of actually addressing

03:23.560 --> 03:40.240
it and alleviating it, we find ways to, we find ways to balance it out with diversion

03:40.240 --> 03:52.000
and desire and central pleasure in the most part or pleasant ideas, pleasant thoughts. Things

03:52.000 --> 04:06.640
we enjoy. We have negative emotions while the old way was, of course, to distract yourself

04:06.640 --> 04:13.520
and find some way to make it feel happy now. Much more direct. We just take pills. There's

04:13.520 --> 04:20.720
something wrong that we have to fix. Or even worse, perhaps, is to think that there is

04:20.720 --> 04:28.440
nothing wrong. That anger is okay. It's okay to be angry and fly into a rage. It's okay

04:28.440 --> 04:42.560
to cling. It's okay to be attached. My cousin passed away recently. I wouldn't say we're

04:42.560 --> 04:54.400
quite close, but we've been close at times. Of course, quite dear. But it's still my

04:54.400 --> 05:01.660
strength when it's odd as a Buddhist to be upset when he dies, to be sad and yet there's

05:01.660 --> 05:14.920
such sadness because we're so attached. We have such clinging. So I was asked just

05:14.920 --> 05:20.240
tonight, I was asked how I feel, what I think about it. And I said, well, I don't feel

05:20.240 --> 05:27.240
sad. I understand. It's normal to feel sad. I mean, I understand. I'm not trying to say

05:27.240 --> 05:37.880
that there's something wrong and that you should try to avoid or deny that you're sad if

05:37.880 --> 05:45.400
you are. There's a Buddhist, of course. It's quite, if anything, it's quite exciting

05:45.400 --> 05:53.920
because the person who's passed away is on to noon and different things. So there's

05:53.920 --> 06:02.280
quite a bit of preparation and expectation involved in their lives. They have new challenges

06:02.280 --> 06:11.280
or perhaps the same ones over again. There's certainly no reason to feel sad except for

06:11.280 --> 06:22.960
the fact that we're so, so attached to those who are close to him. And so for his mother

06:22.960 --> 06:30.640
and father and his mother asked me, she wasn't quite impressed by my answer, I don't think.

06:30.640 --> 06:36.400
As I said, it's not the death that causes suffering. It's the attachment. I think for some

06:36.400 --> 06:44.200
people they appreciate this attachment. I think of it as part of life, a wholesome part

06:44.200 --> 06:54.280
of life. Pleasure and pain are to be in equal portions and you wouldn't enjoy the

06:54.280 --> 06:59.720
pleasure if you didn't have the pain. So this is how we solve our problems by seeking

06:59.720 --> 07:13.440
out the pleasure and rather than try to eradicate the suffering to accept it as a part

07:13.440 --> 07:20.560
of a part of life. So Buddhism, I think it's fair to say that we don't accept suffering

07:20.560 --> 07:28.600
as a part of life per se. Acceptance is to some extent a part of it, but it's not quite

07:28.600 --> 07:44.240
acceptance, right? And it's not even suffering. We have this thing called dukka and dukka.

07:44.240 --> 07:50.680
And so we get this idea that Buddhism says that everything is suffering. It's not quite

07:50.680 --> 08:03.880
so simple. Dukka simply means that which is a source of suffering. Like a fire, a fire

08:03.880 --> 08:10.800
can cause you great suffering if you fall into it. Of course, fire doesn't cause you

08:10.800 --> 08:18.600
suffering if you see it from afar. Just by looking at the suffering or acknowledging that

08:18.600 --> 08:23.400
the suffering is there, it's the fire. Just by looking at the fire and acknowledging that

08:23.400 --> 08:35.200
it's there doesn't make you suffering. And that's why the Buddha said the source of suffering,

08:35.200 --> 08:44.760
the cause of suffering is clinging, is craving, it's desire. When you have expectations

08:44.760 --> 08:53.680
or attachments to things when you wish for them to make you happy. So the training that

08:53.680 --> 09:05.160
we're undergoing, the training that we're looking to accomplish to achieve here is to

09:05.160 --> 09:10.280
not suffer. It's to free ourselves from all the baggage that we carry around about our

09:10.280 --> 09:26.920
experiences. We're not torturing you here and tying you to the wall and beating you.

09:26.920 --> 09:35.600
The pain we experience in sitting is actually quite minimal, but because of our bad habits,

09:35.600 --> 09:44.440
because of our lack of training, we create all sorts of suffering and it's the simplest

09:44.440 --> 09:57.200
thing. It's too hot, it's too cold, it's too loud, it's too quiet, it's too boring, too dull,

09:57.200 --> 10:16.320
it's stressful. We create baggage, create problems. And so a reassurance for you is at the

10:16.320 --> 10:22.000
beginning it can be quite difficult because you're untrained, not because the meditation

10:22.000 --> 10:30.240
is stressful, but as you train yourself, once you're present, when you experience things

10:30.240 --> 10:37.000
as they are, it's quite peaceful, it's quite effortless, it takes time to get there.

10:37.000 --> 10:43.920
When you get to the point where eventually you're able to experience things just as they

10:43.920 --> 10:59.880
are. And so our training is moment by moment to cultivate, to cultivate new habit, to cultivate

10:59.880 --> 11:04.880
the habit of mindfulness. See other part of what needs to be said is that this is about

11:04.880 --> 11:12.920
habit. Training is about changing our behavior, so it's something that has to be a habit

11:12.920 --> 11:20.320
and it's dealing with and combating our bad habit. So a lot of what you're dealing with

11:20.320 --> 11:31.800
here is going to be not just the wrong reactions or reactions that cause suffering but

11:31.800 --> 11:41.320
habits of reaction, meaning that you might think that at some point you've figured it out

11:41.320 --> 11:46.240
and you've solved your problems and you've changed your behavior, you've learned how to respond

11:46.240 --> 11:51.640
and how to react properly, but in the next moment or the next sitting or the next hour or

11:51.640 --> 12:00.120
the next day, it all comes back because it's about habits. This is why meditation takes

12:00.120 --> 12:10.800
patience. So what you're doing here and why you're doing it again and again and again is

12:10.800 --> 12:21.400
because the mind is only made up of tendencies, pattern, habit. That's all we are. Our

12:21.400 --> 12:28.360
whole personality is just habits, which is exciting because you can change them. You can

12:28.360 --> 12:35.880
cultivate new habits, cultivate good habits, and you can find it much easier to live, much

12:35.880 --> 12:45.720
easier to be, which brings us to the next thing that needs to be said is why we're doing

12:45.720 --> 12:50.560
this. Okay, we talk about being present. We talk about seeing things as they are. Why are

12:50.560 --> 12:57.840
we doing this? Why are we so concerned with, can't we just be happy with the way things

12:57.840 --> 13:02.440
are? If things weren't so bad and it come and meditate, that's one thing as it starts

13:02.440 --> 13:09.960
to seem, you know, things weren't so bad. Maybe that's just couldn't go back as it's challenging,

13:09.960 --> 13:15.320
right? Meditation is a challenge and there's a temptation to just go back and stuff

13:15.320 --> 13:23.280
well in your ordinary suffering. But there are very good reasons to meditate. Our bad habits

13:23.280 --> 13:30.080
are quite dangerous. There's much danger out there. Death is a big one. Maybe not your

13:30.080 --> 13:36.000
death but the death of someone else. The death of those you love, the loss of things you

13:36.000 --> 13:47.440
love. And there are all sorts of dangers that they're the danger of suffering. So much

13:47.440 --> 13:56.040
danger for those, for as long as we still have clinging, as long as we still have craving,

13:56.040 --> 14:00.920
the danger that we won't get what we want, the simple danger that we'll be put in situations.

14:00.920 --> 14:08.680
It's not even a hypothetical danger. It's not the danger we face every day. But maybe today

14:08.680 --> 14:13.320
we'll be put in a situation where we have to suffer, not because of the situation but

14:13.320 --> 14:22.920
because of our expectations. Because our patterns of behavior clash with the reality of

14:22.920 --> 14:29.920
the experience. At this moment I want to be entertained. I want to be pleased. I'm not

14:29.920 --> 14:42.920
being pleased. Reality doesn't accommodate our desires. It doesn't accommodate our mind.

14:42.920 --> 14:50.280
In fact, it can't because desire is something that accumulates. It's habitual and it becomes

14:50.280 --> 14:56.560
stronger and stronger. It feeds itself. So we become more and more desirous of things

14:56.560 --> 15:00.920
that we want and more and more inverse to the things that we dislike. That's why it's

15:00.920 --> 15:06.880
so stressful to sit. You're sitting at the cost, so much pain. It's not really. It's such

15:06.880 --> 15:14.520
a puny little pain but you repeat the disliking of it again and again and again. You don't

15:14.520 --> 15:20.480
like it and then it comes again and you don't like it again. Anything. The most minuscule

15:20.480 --> 15:27.080
thing can drive you crazy. This is why we might be sitting and you hear a noise and kind

15:27.080 --> 15:32.640
of irritates you. Then you hear the noise again and it significantly irritates you and

15:32.640 --> 15:38.560
you hear it again and again and eventually you just want to blow up and yell at someone.

15:38.560 --> 15:48.480
Nothing to do with the sound. Nothing to do with the pain. Nothing to do with the experience.

15:48.480 --> 15:55.040
Bad habits. You can see your habits forming and then they become long-term habits to the

15:55.040 --> 15:59.640
point where for most of us we think it's crazy the idea that you could somehow be at

15:59.640 --> 16:07.640
peace with pain. Pain is a problem. We're so ensconced in this idea. This bad habit.

16:07.640 --> 16:17.600
I'm believing that pain is a problem. So why we're doing it really in a nutshell is for

16:17.600 --> 16:31.600
this reason. Ordinary life is fraught with problems. It's fraught with difficulties. If

16:31.600 --> 16:41.600
not yet then to come. Looking at people who lose a loved one and how stressful and

16:41.600 --> 16:48.800
how much suffering comes from that. One in fact I mean from a Buddhist point it's like

16:48.800 --> 16:56.280
what? They move to Han. It's like be happy for them or at least not happy. Be more concerned

16:56.280 --> 17:05.120
for them other than sad. Something to be sad about. All we're sad about is our own at

17:05.120 --> 17:13.240
somewhat selfish in fact. We're not sad because all that poor person died. They're sad

17:13.240 --> 17:20.600
because all poor me I can't see that person anymore. How cruel am I at all? How cruel

17:20.600 --> 17:28.640
does that sound? It's not very compassionate I suppose. But it's the hard truth that we as

17:28.640 --> 17:38.440
meditators start to face. It's not even a hard truth. It's a wonderful liberating truth.

17:38.440 --> 17:45.960
It's realized that we're only causing ourselves suffering. There's no rational reason when

17:45.960 --> 17:53.160
you lose something or someone. There's no rational reason to be upset. I said this to a friend

17:53.160 --> 17:58.040
of mine. She got very upset at me. She said she thought I meant that we or she knows

17:58.040 --> 18:07.200
that some people know she knew it would come across as telling people to deny their sadness.

18:07.200 --> 18:17.040
It's not about denying your grief, your shock. It's about realizing that that's a problem

18:17.040 --> 18:22.560
in and of itself and it's nothing to do with the loss. It's like oh wow I'm terribly

18:22.560 --> 18:35.880
untrained. Unable to deal with reality. The weak, no that's really it. Weakness. So it's

18:35.880 --> 18:42.760
quite encouraging. Feel quite proud and not proud but confident and encouraged and excited

18:42.760 --> 18:50.280
about how great it is to become strong. The power that comes from just sitting through pain,

18:50.280 --> 18:57.960
even your body shakes, sweat pours out of your armpits, your head feels like it's on fire.

18:57.960 --> 19:10.720
To sit with that, to just say no, I'm not going to come. I'm not going to let it vanquish

19:10.720 --> 19:21.640
me. It's quite encouraging. That's sort of why we do it but the other part of why we

19:21.640 --> 19:26.040
do it is what do we get out of it, right? What are we going to get out of this? I always

19:26.040 --> 19:30.600
tell meditators not to think about what you're going to get out of it, not to sit there

19:30.600 --> 19:42.440
and say are we there yet? Are we there yet? Don't have expectations. They don't help. But

19:42.440 --> 19:48.200
if you're confused or a little bit unsure of whether you're actually going to get

19:48.200 --> 19:53.760
anything out of this, we can talk a little bit about what you get out of it. The first

19:53.760 --> 20:03.560
thing is purity. That should be a good standard for you to live by during your practice.

20:03.560 --> 20:07.640
The purity, because you'll feel you don't have to be told what is impure in the mind.

20:07.640 --> 20:15.840
You'll see that which is ridiculous and useless and harmful to you. That's the impurity.

20:15.840 --> 20:23.520
You can see your mind is being so full of crap, full of garbage. We don't have to be told

20:23.520 --> 20:27.760
that it's garbage. You don't have to believe in me that it's garbage. You'll see for yourself.

20:27.760 --> 20:35.560
You'll say to yourself that's garbage. That's useless. That's harmful. I'm hurting myself.

20:35.560 --> 20:41.160
So if you want to know what you get out of it or if you want to have a sense of what

20:41.160 --> 20:49.200
you're getting out of it, it's purity. They're simply seeing and as a result of adjusting

20:49.200 --> 21:03.840
the nature of your mind, your mind becomes pure. By degrees, pure and pure. Until so many

21:03.840 --> 21:10.240
other benefits of cruelty, you start to free yourself from depression, stress, anxiety,

21:10.240 --> 21:21.560
fear, worry, doubt. They're using this. You're able to overcome suffering. You'll find

21:21.560 --> 21:27.480
that when there's pain, you're not upset by it. You don't suffer from it. You'll find

21:27.480 --> 21:33.480
that when things come that would normally make you, normally make you sad, normally upset

21:33.480 --> 21:38.920
you, they don't upset you, normally be averse to, you're no longer averse to, you'd normally

21:38.920 --> 21:45.520
be bored or turned off by, and you're able to bear with them mindfully and be a piece

21:45.520 --> 21:59.400
with them. You gain confidence. You feel sure of yourself. You find the right way, period,

21:59.400 --> 22:12.960
and the right way. You find the way that is true and right, pure and clear and noble.

22:12.960 --> 22:20.280
And finally, you become free. You feel free. So right now, what you feel during the course

22:20.280 --> 22:26.760
for the most part, what you feel is entrapped. That's why many meditators run away because

22:26.760 --> 22:36.040
they start to feel quite trapped and they get overwhelmed by it. But it's not the meditation

22:36.040 --> 22:49.280
that's trapping you. Meditation allows you to stop and see how trapped you are because

22:49.280 --> 22:54.280
if you keep running around the cage, it's possible like a hamster in a wheel, right? Hamster

22:54.280 --> 22:58.520
doesn't know it's in a wheel. I don't know. I don't know if it does or not. Maybe they

22:58.520 --> 23:03.920
know and they're just getting exercise, but that's possible. If you run around in the cage

23:03.920 --> 23:12.920
enough, you can feel like you're free. Not realize that you're bound, which, you know, it sounds

23:12.920 --> 23:17.920
kind of nice, but the truth is that there's great suffering. If you just keep denying the

23:17.920 --> 23:26.720
fact that you're suffering, avoiding it, right? You suffer, but then you avoid it. You find

23:26.720 --> 23:34.800
a way to fix it temporarily. So when you stop trying to fix it, you get to see how much

23:34.800 --> 23:40.080
suffering there really is. You get to see what you've sort of been ignoring about your

23:40.080 --> 23:51.640
life. See how trapped you really are. So don't be discouraged and the meditation seems

23:51.640 --> 23:57.640
to actually make you feel more trapped. I know how to be free. I'll just get up off the

23:57.640 --> 24:07.000
mat to start. Stretch. Go sit in the comfy chair and find a soft bed. That's how I free

24:07.000 --> 24:13.960
myself. Hopefully eventually that changes generally through the practice. That's what

24:13.960 --> 24:17.840
changes. You start to see that's not the way out. That's not what's trapping me. It's

24:17.840 --> 24:25.840
not the lack of a soft bed or a comfortable chair. It's the need for those things. It's

24:25.840 --> 24:35.440
the inability to stand pain and the hardness and discomfort. It's the inability to stand

24:35.440 --> 24:49.320
our own mind, our own presence, our own reality. So there you go. There's some opening

24:49.320 --> 24:54.960
words because this is the new year and this is our first broadcast and this is my first

24:54.960 --> 25:03.280
talk to all of our new meditators, except we have Brenda's here. She reminds me that she

25:03.280 --> 25:09.080
was with us in Stony Creek. I couldn't remember where, but Brenda is here. Some of you

25:09.080 --> 25:16.560
know, Brenda, I think. Javen is here. Javen was here last year. Javen might be a long

25:16.560 --> 25:25.320
term. We'll see how long he lasts. We've got two new guys, one thing for various periods.

25:25.320 --> 25:34.000
Maybe you'll get to meet them. There you go. That's the demo for tonight. Thank you all

25:34.000 --> 26:00.160
for tuning in. Have a good day.

