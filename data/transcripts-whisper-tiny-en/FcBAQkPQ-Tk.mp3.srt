1
00:00:00,000 --> 00:00:07,000
I'm trying to observe my emotions, but I'm somewhat out of touch with them and I control them a lot.

2
00:00:07,000 --> 00:00:15,000
When an emotion goes away, how can I tell if it's subsided on its own or if I suppressed it?

3
00:00:15,000 --> 00:00:28,000
I wouldn't be so concerned or so analytical about it because the analysis itself is probably a misunderstanding of what's going on.

4
00:00:28,000 --> 00:00:39,000
The concern about suppressing things, the concern about causing yourself suffering, about not accepting the experience.

5
00:00:39,000 --> 00:00:51,000
This concern that you're suppressing, that you're not accepting and learning about the experience is often a misunderstanding of what's going on.

6
00:00:51,000 --> 00:00:58,000
The emotions themselves will cause stress and that's why they're stressed in the mind.

7
00:00:58,000 --> 00:01:02,000
It's not because necessarily because you're suppressing them.

8
00:01:02,000 --> 00:01:10,000
What happens is when you have certain emotions then because of the complications in the mind, we give rise to even more conflicting emotions.

9
00:01:10,000 --> 00:01:12,000
That's where it starts to get confusing.

10
00:01:12,000 --> 00:01:19,000
But the desire is stressful. This is something that you can see if your mind is quiet.

11
00:01:19,000 --> 00:01:31,000
Anger is not only anger is stressful, but desire itself is stressful and will give you tension in the body and suffering in the mind.

12
00:01:31,000 --> 00:01:44,000
It's probably what you're experiencing is quite a bit of suffering and try to be objective about that.

13
00:01:44,000 --> 00:01:47,000
These thoughts that you're having, the worries and the confusion and the uncertainty is also arising.

14
00:01:47,000 --> 00:01:56,000
Remember that part of this difficulty that you're having, the struggle that you're having, is the struggle to accept impermanent suffering and so on.

15
00:01:56,000 --> 00:02:07,000
That the struggle to accept things just as they are, thinking that something's wrong with your practice, is actually kind of absurd that anything could be wrong with your practice.

16
00:02:07,000 --> 00:02:12,000
There is nothing called practice and you're not doing it.

17
00:02:12,000 --> 00:02:22,000
There is only experience and it's coming and going and once you realize that, then all of these knots become untied.

18
00:02:22,000 --> 00:02:35,000
Every moment is a new knot. Every moment in your meditation is a new challenge and the suppression occurs when we fail to meet the challenge.

19
00:02:35,000 --> 00:02:42,000
When at any given moment we stop the mindfulness, we stop the awareness or we cling to something.

20
00:02:42,000 --> 00:02:52,000
It's because we have very one-track minds. We don't have the flexibility to be able to move from one challenge to the next.

21
00:02:52,000 --> 00:03:03,000
This is one explanation of why enlightened people are so bright and so clear in mind because they're able to move from one challenge to the next.

22
00:03:03,000 --> 00:03:17,000
You might get one challenge and you might get it and you unties some knot and then you get so caught up in that that the next challenge comes and you miss it and you try to react to the next moment in the same way.

23
00:03:17,000 --> 00:03:34,000
For example, you focus on pain and finally you get it pain and finally your mind realizes it's just pain and then some kind of lust comes up.

24
00:03:34,000 --> 00:03:49,000
The feeling or even going back to the pain and you're not able to deal with the new experience that you have to be dealt with in a totally different way.

25
00:03:49,000 --> 00:04:05,000
The first shift of awareness was to convince yourself that the pain is rather than being bad that it is just pain.

26
00:04:05,000 --> 00:04:24,000
Now the last, again you have to shift to see that it's just a feeling of pleasure and the chemicals arising in the body. You have to see it for what it is but it's coming from the other angle, the angle of it being a good thing and wanting to cling to it.

27
00:04:24,000 --> 00:04:39,000
So you have to convince yourself in another way or you have to teach yourself in another way that this is the way of letting letting go pain is teaching yourself in the way of letting come.

28
00:04:39,000 --> 00:05:05,000
For example, the difficulty is not in suppressing, the difficulty is in keeping up and sticking with the rhythm or keeping up with the rhythm in adapting to the situation.

29
00:05:05,000 --> 00:05:19,000
And which in the end simply means no matter which angle the mind is taking straightened out in the same direction.

30
00:05:19,000 --> 00:05:35,000
If it's crooked this way, straightened it back this way. If it's crooked this way straightened it back this way. Everything has to be straightened out. Whatever the mind is clinging to bring it back to center.

31
00:05:35,000 --> 00:05:48,000
Everything is just about learning how to do that and teaching yourself to do that with everything, to be clearly aware and simply clearly aware of everything.

32
00:05:48,000 --> 00:05:58,000
I think it's a red herring. It's a false problem that we come into thinking that we're suppressing things, thinking that we're controlling.

33
00:05:58,000 --> 00:06:22,000
And when that feeling of the you identify with some kind of suppression comes up, acknowledge it just as a feeling, acknowledge it for what it is. And as you see clearer, you should be able to see that actually it's just arising based on the defilements, based on the emotions, based on, and you know, based on, so for example.

34
00:06:22,000 --> 00:06:32,000
One thing that happens is people say, well, when I acknowledge thinking, thinking, it feels like I get a big headache. When I say to myself, thinking, thinking, I just get a big headache.

35
00:06:32,000 --> 00:06:43,000
You know, when I'm thinking a lot, I don't have the headache. But as soon as I say, thinking, thinking, thinking, I get a big headache. So you start to think, well, the technology is really just causing me suffering.

36
00:06:43,000 --> 00:06:56,000
Once you really get good at it, and once you really see what's going on, you say, no, no, no, that's not what's going on. Here I've been thinking for a half an hour, building up this huge huge headache without knowing it.

37
00:06:56,000 --> 00:07:03,000
And because I don't want to face the headache, I just think more, you know, I just distract my attention further and further, and it's so much fun to do that.

38
00:07:03,000 --> 00:07:13,000
But there's building, a building, a building of this huge headache, and then finally, I've been thinking, and you bring yourself back and say, thinking, thinking, and boom, suddenly you've got to deal with a headache.

39
00:07:13,000 --> 00:07:19,000
And so you say to yourself, saying, thinking, thinking, thinking, that's just suppressing it. That's not really facing it.

40
00:07:19,000 --> 00:07:23,000
You see, it's a misunderstanding of what's really going on.

41
00:07:23,000 --> 00:07:32,000
When you have lust or desire, and you focus on, you say, wanting, wanting, or pleasure, then suddenly you feel this tension in the body.

42
00:07:32,000 --> 00:07:44,000
You feel this tension, it's coming up, but it's this tension that was there because of the desire itself, or you feel the dullness of mind that comes with desire.

43
00:07:44,000 --> 00:07:53,000
And you think, oh, this is coming from the meditation. You think this meditation I'm suppressing, so it's giving me this feeling of dullness.

44
00:07:53,000 --> 00:08:05,000
But actually, the desire is what's bringing it. The emotions themselves are bringing these as a result. When you're mindful, you're just beginning to see objectively.

45
00:08:05,000 --> 00:08:12,000
And you'll see that, you'll see that, actually, the moment when you have desire, you're not seeing things objectively. That's why it feels wonderful.

46
00:08:12,000 --> 00:08:18,000
The desire feels wonderful until you stop, because suddenly you're objective again, and you're seeing both sides.

47
00:08:18,000 --> 00:08:27,000
Desire is like, here you have the good, here you have the bad, running towards the good, and running away from the bad, so that you never really experience it.

48
00:08:27,000 --> 00:08:34,000
It just builds up, builds up, builds up until you stop, and then you experience it, then you have to deal with it.

49
00:08:34,000 --> 00:08:42,000
So I wouldn't worry too much, even if you are suppressing, the most important thing is to watch the suppression, not to stop suppressing.

50
00:08:42,000 --> 00:08:49,000
It's just to watch and to see what's going on, because in the end, you'll see impermanence suffering, and once off, you'll see that you can't control it.

51
00:08:49,000 --> 00:09:16,000
You'll see that it's, even the suppressing is something that happens sometimes, it doesn't happen sometimes, it's worth getting caught up in, not worth getting attached to.

