1
00:00:00,000 --> 00:00:15,600
Okay, good evening everyone, we're doing something a little different here.

2
00:00:15,600 --> 00:00:29,840
This is YouTube's own live broadcast interface because just about everything about

3
00:00:29,840 --> 00:00:40,760
our broadcast system has broken down, I can't do very much now, so we're using

4
00:00:40,760 --> 00:00:49,200
this built-in system. Plus side is I now see chat on the right side, so

5
00:00:49,200 --> 00:00:57,440
then that's a plus or not, I don't know, we'll see. Anyway, do our meditators here in

6
00:00:57,440 --> 00:01:07,480
the center. I appreciate the work you're doing. I want to talk a little bit

7
00:01:07,480 --> 00:01:17,640
about meditation, so I've deleted several questions as usual. I'm not so

8
00:01:17,640 --> 00:01:24,520
keen on answering speculative questions or questions about other traditions.

9
00:01:24,520 --> 00:01:29,160
We want to have some kind of a flowing narrative where the whole evening is

10
00:01:29,160 --> 00:01:45,680
talking about meditation. So it's a big question, right? What is meditation? I think

11
00:01:45,680 --> 00:01:49,280
it's not a question people ask, people don't ask, what is meditation, but it is

12
00:01:49,280 --> 00:01:54,680
an important one, because the answer to the question answers a lot of the

13
00:01:54,680 --> 00:02:06,160
questions that I get, not directly, but helps to clear up the underlying issue

14
00:02:06,160 --> 00:02:12,720
behind the question. When you understand what meditation is, it's easier to

15
00:02:12,720 --> 00:02:20,000
understand the problems that come up during meditation, the issues, and how to

16
00:02:20,000 --> 00:02:28,040
deal with the challenges involved with meditation. So meditation is just

17
00:02:28,040 --> 00:02:37,960
generally some kind of activity that involves mental training. Our minds can

18
00:02:37,960 --> 00:02:49,600
be trained and are constantly training in various ways. We are being influenced

19
00:02:49,600 --> 00:02:57,640
by the world around us and we're developing habits based on the experiences

20
00:02:57,640 --> 00:03:09,080
we have. So you could say that it's not much different. Meditation isn't much

21
00:03:09,080 --> 00:03:17,680
different from ordinary life. It's just focused and directed in a particular

22
00:03:17,680 --> 00:03:31,640
way. So meditation isn't the act of sitting still. It isn't an hour that you

23
00:03:31,640 --> 00:03:38,320
spend. Meditation is the work that you do during the hour that you're sitting

24
00:03:38,320 --> 00:03:47,640
still. It can also be work that you do throughout your day. If your intention

25
00:03:47,640 --> 00:03:56,160
is directed in a particular way, you can call that meditation. So the

26
00:03:56,160 --> 00:04:06,240
meditation that we practice is using mantras and I use this word. It's not a

27
00:04:06,240 --> 00:04:09,680
word that people in my tradition generally use. It's not a translation that I've

28
00:04:09,680 --> 00:04:21,480
really ever heard used apart from myself. But to me it's the most clearly

29
00:04:21,480 --> 00:04:28,360
understandable description of what it is that we do because mantra

30
00:04:28,360 --> 00:04:35,000
meditation is something very ancient and what we do is very much in line with

31
00:04:35,000 --> 00:04:40,360
the descriptions in Buddhist texts about mantra meditation. It's the repeating

32
00:04:40,360 --> 00:04:47,400
of a word. A mantra I think often is a phrase and we don't do that so much. We

33
00:04:47,400 --> 00:04:55,160
just pick a word. So whatever you call it mantra otherwise, it's a word that has

34
00:04:55,160 --> 00:05:03,680
a power. It has a power because it directs the mind in a specific way. I mean a

35
00:05:03,680 --> 00:05:08,440
good example of mantra meditation directed in the specific way is loving

36
00:05:08,440 --> 00:05:20,680
kindness meditation. We have whole practices, groups of people who undertake to

37
00:05:20,680 --> 00:05:29,000
practice mid-demandation. People who do it on a daily basis will sit and

38
00:05:29,000 --> 00:05:36,880
cultivate love, friendliness. By using a phrase, by repeating to themselves may I

39
00:05:36,880 --> 00:05:41,080
be happy, may all beings be happy. That's a mantra when you repeat it to

40
00:05:41,080 --> 00:05:45,800
yourself. It has a power because it directs your mind in a particular way. Now

41
00:05:45,800 --> 00:05:52,640
the mantras that we use are meant to direct the mind but in a bit of a

42
00:05:52,640 --> 00:05:58,840
different way. There's no may, there's no wish, there's no may it be so may it be

43
00:05:58,840 --> 00:06:06,040
that. We're directing our minds towards objectivity. That's the intent behind

44
00:06:06,040 --> 00:06:14,240
our choice of mantras. Now the mantras themselves aren't objectivity. They're a

45
00:06:14,240 --> 00:06:21,200
an artificial construct and I think it's fair to say that all meditation is

46
00:06:21,200 --> 00:06:27,800
artificial. Saying to yourself may all beings be happy isn't friendliness but it's

47
00:06:27,800 --> 00:06:34,800
a tool that encourages the mind in a specific way. It encourages the mind to

48
00:06:34,800 --> 00:06:41,000
develop friendliness. So by our choice of words and the words themselves don't

49
00:06:41,000 --> 00:06:51,160
have power but the intention and the meaning behind it for us is object

50
00:06:51,160 --> 00:06:56,120
ivity. When we say to ourselves, for example pain, pain, the reason we're doing

51
00:06:56,120 --> 00:07:04,160
that is because we want to be objective. We're using this word as an

52
00:07:04,160 --> 00:07:16,400
augment for our intention. Let's say stabilizer, it's a means of keeping us on

53
00:07:16,400 --> 00:07:24,240
track. So if we understand the sorts of words that we want to use or the

54
00:07:24,240 --> 00:07:31,040
reasons why we use these words or our intentions, then it's easy for us to

55
00:07:31,040 --> 00:07:35,120
understand which words to use. It's easy for us to understand us to understand

56
00:07:35,120 --> 00:07:42,160
the the role that the words play. It doesn't make it easy to actually do it.

57
00:07:42,160 --> 00:07:47,160
Meditation, of course, whatever sort you do is is going to be difficult if

58
00:07:47,160 --> 00:07:52,120
it's something new. If it's different from your ordinary patterns of

59
00:07:52,120 --> 00:07:59,200
behavior, if you're normally very judgmental, partial, critical of yourself

60
00:07:59,200 --> 00:08:03,520
and others, then meditation, objectivity is going to be difficult. If you're

61
00:08:03,520 --> 00:08:07,840
normally a very angry person, loving kindness is going to be difficult and so

62
00:08:07,840 --> 00:08:14,120
on. There's no question that meditation generally. In fact, you might go

63
00:08:14,120 --> 00:08:17,960
further and say it's meant to be difficult. I mean, a loving and kind person

64
00:08:17,960 --> 00:08:22,600
wouldn't have much use for or reason for practicing loving kindness. A very

65
00:08:22,600 --> 00:08:27,360
objective person doesn't really need to practice mindfulness meditation as

66
00:08:27,360 --> 00:08:32,320
we teach it. So the reason we're doing it is because it's something difficult

67
00:08:32,320 --> 00:08:37,320
because we want to change. It's the difficulty that makes it valuable.

68
00:08:37,320 --> 00:08:41,960
Directly, I mean, particularly. It's not just to say, oh, it's difficult, get

69
00:08:41,960 --> 00:08:50,520
over it. No, the difficulty is what it's all about. Change comes from something

70
00:08:50,520 --> 00:08:54,320
difficult, something different, something that is more

71
00:08:54,320 --> 00:09:10,280
unaccustomed to. Otherwise, it wouldn't bring any sort of change. So there's

72
00:09:10,280 --> 00:09:13,880
much more I can say about meditation, but I think that's a good introduction to

73
00:09:13,880 --> 00:09:21,480
this. So let's talk about these six questions we have. Not so many tonight, but

74
00:09:21,480 --> 00:09:29,280
that should give us some opportunity to focus on topics on regarding

75
00:09:29,280 --> 00:09:38,280
meditation. So here's a question about walking meditation. So when you're

76
00:09:38,280 --> 00:09:43,320
watching the one foot move, what about the other foot that's also moving,

77
00:09:43,320 --> 00:09:48,720
right? There's a shifting of weight, there's movements of the other foot. This is

78
00:09:48,720 --> 00:09:54,240
why this is the sort of question why the sort of question was the reason why I

79
00:09:54,240 --> 00:10:03,280
wanted to give sort of a general idea of meditation because meditation isn't

80
00:10:03,280 --> 00:10:09,720
a replacement for awareness. It's about conditioning your awareness. So when we

81
00:10:09,720 --> 00:10:13,960
use these words, we're obviously not going to have a word for everything. We're

82
00:10:13,960 --> 00:10:19,000
not meant to have a word for everything. The word is a tool to remind us to be

83
00:10:19,000 --> 00:10:25,880
objective. So when you're saying to yourself, stepping right, yes, you potentially

84
00:10:25,880 --> 00:10:31,600
intermittently, also know that the left foot is moving. Now, of course, it's

85
00:10:31,600 --> 00:10:35,600
important that you mainly know that the right foot is moving, but there will

86
00:10:35,600 --> 00:10:39,280
also be other sorts of awareness. There will be awarenesses of all sorts of

87
00:10:39,280 --> 00:10:48,680
things. That's not really a problem. Now we do encourage meditators to move from

88
00:10:48,680 --> 00:10:54,000
object to object, which is why we then say stepping left, right? And if you get

89
00:10:54,000 --> 00:11:00,720
distracted by something else to note it as well, but it's not about becoming

90
00:11:00,720 --> 00:11:07,640
hyperactive, trying to note everything. It's not magic. And it's not the most

91
00:11:07,640 --> 00:11:10,840
important thing. The most important aspect of meditation is that you're

92
00:11:10,840 --> 00:11:15,040
becoming objective. And that's going to be independent of the actual words.

93
00:11:15,040 --> 00:11:24,160
It's encouraged. It's supported by using the words. That's the point of

94
00:11:24,160 --> 00:11:31,920
them. So all of the other kinds of knowing, other kinds of awareness are great.

95
00:11:31,920 --> 00:11:37,080
I mean, they're a part of it, but you don't have to note everything. You don't

96
00:11:37,080 --> 00:11:46,400
have to note more than once every second or something like that. Anyways, when

97
00:11:46,400 --> 00:11:52,440
you're walking, maybe a little slower, even step being right, step being left.

98
00:11:52,440 --> 00:12:03,840
So it's a few seconds. Would it be unfaithful to utilize mind altering

99
00:12:03,840 --> 00:12:08,560
pharmaceuticals while following a meditative practice? So again, we can refer

100
00:12:08,560 --> 00:12:14,400
back to what I said about what is meditation. So to answer this question, you

101
00:12:14,400 --> 00:12:19,360
have to ask, what is your intention? In what way are you focusing the mind? If

102
00:12:19,360 --> 00:12:23,880
your goal is objectivity, mind altering pharmaceuticals don't play any role in

103
00:12:23,880 --> 00:12:36,800
that. And they're quite, quite directly a, a concern and, and a detriment to

104
00:12:36,800 --> 00:12:42,120
objectivity because they're encouraging a specific state. I mean, it's an

105
00:12:42,120 --> 00:12:47,720
intention to give rise to altered states and states that are potentially

106
00:12:47,720 --> 00:12:54,320
confusing and liable to trigger thoughts and explorations, all of which have

107
00:12:54,320 --> 00:13:00,480
nothing to do with objectivity. Objectivity means not trying to change, not

108
00:13:00,480 --> 00:13:05,120
trying to condition in any way your experience. So it's letting ordinary

109
00:13:05,120 --> 00:13:13,520
experience continue, but changing your attitude towards it. I know people, there's

110
00:13:13,520 --> 00:13:19,760
many people who are great advocates of mind altering pharmaceuticals, but it's

111
00:13:19,760 --> 00:13:29,240
it's not evil. It certainly could be arguably a part of a valid meditation

112
00:13:29,240 --> 00:13:33,680
practice, just not this meditation practice. I think that should be fairly

113
00:13:33,680 --> 00:13:42,560
clear. Why that is. It's unnecessary to do walking meditation equally

114
00:13:42,560 --> 00:13:47,120
to sitting meditation. One mind is constantly on anxiety loop. It's hard to

115
00:13:47,120 --> 00:13:52,760
focus, need to do a lot of standing much better to focus on anxiety. Can I

116
00:13:52,760 --> 00:13:57,520
decrease walking time and increase sitting time? Yes, yes, you certainly can.

117
00:13:57,520 --> 00:14:03,080
Why do we try to do equal amounts of walking and sitting? A big part of that

118
00:14:03,080 --> 00:14:08,960
is again, our focus on objectivity, because if you incline towards one or

119
00:14:08,960 --> 00:14:13,280
the other, that's a sort of partiality. The difficulty that you're having in

120
00:14:13,280 --> 00:14:17,080
walking meditation could be interesting and should be something eventually

121
00:14:17,080 --> 00:14:23,360
that you explore when you're anxious and it's difficult. That's potentially a

122
00:14:23,360 --> 00:14:28,760
good thing to be challenged by anxiety coming up more frequently and more

123
00:14:28,760 --> 00:14:36,000
powerfully in walking meditation. Standing meditation is also a valid

124
00:14:36,000 --> 00:14:41,520
alternative. That being said, when anxiety is overwhelming, you're actually

125
00:14:41,520 --> 00:14:46,720
directed to focus more on sitting meditation, because as you can see, it's more

126
00:14:46,720 --> 00:14:53,240
productive. That being said, again, in the long term, I'd recommend to try and

127
00:14:53,240 --> 00:14:57,720
do standing meditation. Start with standing and once you're able to do

128
00:14:57,720 --> 00:15:03,240
standing and the anxiety isn't so powerful, then try and do walking as well.

129
00:15:03,240 --> 00:15:08,120
There are other reasons for doing walking. It's more active, so it teaches you

130
00:15:08,120 --> 00:15:14,640
how to be mindful throughout your life. It's better for the body in many ways

131
00:15:14,640 --> 00:15:19,240
walking meditation is healthy and that sort of thing.

132
00:15:21,720 --> 00:15:28,600
Here's three questions and I kept this one. I almost got rid of it, but I don't

133
00:15:28,600 --> 00:15:33,280
like questions or four questions. I don't like these compound questions because

134
00:15:33,280 --> 00:15:41,800
it's cheating, but they're all related. So that's regarding speech.

135
00:15:41,800 --> 00:15:49,120
Speech is interesting. For meditator speech is important. Why are these words?

136
00:15:49,120 --> 00:15:56,760
These mantras so useful is because speech has power. Speech is associated

137
00:15:56,760 --> 00:16:05,000
with intention. It's associated with inclination. The words we use can be used.

138
00:16:05,000 --> 00:16:09,240
I can say, I hate you and wish you would die without having any anger because

139
00:16:09,240 --> 00:16:12,840
they're just words. But generally, when I say those words, there's a lot of

140
00:16:12,840 --> 00:16:18,600
anger associated with them. I like oranges. Now I can say those words without

141
00:16:18,600 --> 00:16:23,760
actually liking oranges, but saying them tends to reaffirm my liking of

142
00:16:23,760 --> 00:16:30,360
oranges. Again, these were just random words that I came up with, but so

143
00:16:30,360 --> 00:16:40,560
speech is potentially innocuous, but generally in reinforces and is used most

144
00:16:40,560 --> 00:16:47,160
often to reinforce our intention. So are there topics? So regarding

145
00:16:47,160 --> 00:16:52,920
right speech, what is beneficial? It has much less to do with the actual

146
00:16:52,920 --> 00:16:57,280
content of the speech and much more to do with your intention. There are four

147
00:16:57,280 --> 00:17:01,400
kinds of wrong speech, but they're only wrong because of the intention that's

148
00:17:01,400 --> 00:17:06,720
behind them. If you lie, that's wrong speech. If you have harsh speech, that's wrong

149
00:17:06,720 --> 00:17:12,160
speech. If you have divisive speech trying to divide people, that's wrong

150
00:17:12,160 --> 00:17:18,760
speech. If you have useless speech, speaking just for no reason, again, it's

151
00:17:18,760 --> 00:17:24,960
because your mind is just wanting to waste time and past time, it's all wrong

152
00:17:24,960 --> 00:17:27,320
speech.

153
00:17:34,360 --> 00:17:40,720
Has for topics that are anything associated with the path, anything that has an

154
00:17:40,720 --> 00:17:48,520
intention and not just intention, but is skillful and helpful in cultivating

155
00:17:48,520 --> 00:17:57,160
further objectivity, awareness, wisdom, that's skillful. How can skillfulness

156
00:17:57,160 --> 00:18:01,480
be cultivated? Again, it has to do with the mind state behind it. So skillful

157
00:18:01,480 --> 00:18:07,320
mind states are brought about through through meditation, back practice,

158
00:18:07,320 --> 00:18:21,040
particularly objectivity, mindfulness. How do you speak kind-hearted?

159
00:18:21,040 --> 00:18:28,200
Understand their suffering and that you wish them happiness? Yeah, I'm not

160
00:18:28,200 --> 00:18:37,160
going to really touch that. I don't think it might be a complicated silence

161
00:18:37,160 --> 00:18:42,360
always in accordance with right speech. Well, yes, because there is no

162
00:18:42,360 --> 00:18:48,560
speech. I mean, it's like, if you don't act, is that right action? So right

163
00:18:48,560 --> 00:18:53,160
speech is only right speech if it's associated with the other seven path factors.

164
00:18:53,160 --> 00:19:00,640
Anything else is not, particularly right speech, it's just no speech. But I

165
00:19:00,640 --> 00:19:08,640
think that's all I have to say about that. Okay, the choice of selecting a

166
00:19:08,640 --> 00:19:13,280
major object of meditation when you're distracted. So we have our basic

167
00:19:13,280 --> 00:19:17,680
object and that's useful because it's when there's nothing else to note, you

168
00:19:17,680 --> 00:19:22,960
know, right away what to go to. Otherwise, you might be waiting and seeking and

169
00:19:22,960 --> 00:19:29,920
searching and that has problems associated with it. But besides this main

170
00:19:29,920 --> 00:19:33,520
object arising and the falling, for example, what happens when you're

171
00:19:33,520 --> 00:19:38,160
distracted? What if you're distracted by many different things? Now, I don't

172
00:19:38,160 --> 00:19:43,000
I don't normally use the word major object. I tend to, I mean, my teacher, I

173
00:19:43,000 --> 00:19:47,560
translate directly from what he said. And he would always say choose what is

174
00:19:47,560 --> 00:19:54,120
clearest. That makes perfect sense to me. And then so what if two things are

175
00:19:54,120 --> 00:19:58,440
clearest or if you don't know what is clearest? Again, back to the original

176
00:19:58,440 --> 00:20:03,040
talk about meditation. What is it? What is it for? It doesn't matter which one

177
00:20:03,040 --> 00:20:10,040
you choose. It's a tool that helps you become more objective. Pick one, pick

178
00:20:10,040 --> 00:20:16,120
something that's most prominent. If something's prominent, note it, it's not

179
00:20:16,120 --> 00:20:21,600
magic. It's a tool that you use to help you become more objective. Use it in

180
00:20:21,600 --> 00:20:25,920
that way. You shouldn't really have much problem, no matter what you do.

181
00:20:25,920 --> 00:20:32,040
Finally, we have a question from Siddesh who is keen to let me know that he's

182
00:20:32,040 --> 00:20:37,600
been practicing now for 16 to 17 months, which is good. So unique, we have

183
00:20:37,600 --> 00:20:43,000
this unique situation where he's keeping me updated on how many months he's

184
00:20:43,000 --> 00:20:49,680
been practicing, which is it's heartwarming. So his latest issue is that he

185
00:20:49,680 --> 00:20:55,960
feels something for a girl which he met recently. In the sense of meditation is

186
00:20:55,960 --> 00:20:59,760
consuming me and my thoughts, I think this, maybe I am attached to the

187
00:20:59,760 --> 00:21:06,240
sexuality of my very past experiences. So I mean, a lot of your questions

188
00:21:06,240 --> 00:21:10,560
seem to have this, the problem with them is they just are asking you

189
00:21:10,560 --> 00:21:14,880
asking me what to do about your problems. And you already know what my

190
00:21:14,880 --> 00:21:20,120
answer is going to be. It's just come to be more objective about it. Be

191
00:21:20,120 --> 00:21:26,920
objective about your habits, about your sexual desires, your romantic, your

192
00:21:26,920 --> 00:21:32,760
inclinations, your attraction towards this woman. I mean, it's not a quick fix

193
00:21:32,760 --> 00:21:37,880
and it's not a sure fix. Maybe you fail at it, but the work is there and if you

194
00:21:37,880 --> 00:21:42,520
do the work, the result is there as well. It's just up to you and it could take

195
00:21:42,520 --> 00:21:48,480
a long time, but if you're patient and if you're diligent, it seems like you're

196
00:21:48,480 --> 00:21:52,800
quite diligent. So just keep it up and make sure you're clear about the technique

197
00:21:52,800 --> 00:21:57,680
and if you're interested in going more in depth, we can do an online course, we

198
00:21:57,680 --> 00:22:06,760
can do an intensive course. That's always open. So those are the questions. How's

199
00:22:06,760 --> 00:22:13,160
the live broadcast going? Okay, we have youtubes really chatting up a storm, which

200
00:22:13,160 --> 00:22:17,600
I've said before is a bit of a problem. You know, here I am trying to teach and

201
00:22:17,600 --> 00:22:23,720
everyone's chatting. No, it's not a, I don't, I'm not insulted or anything. It's

202
00:22:23,720 --> 00:22:28,560
just something that has to be admonished. You know, when you listen to the

203
00:22:28,560 --> 00:22:34,560
dummy, you don't chat. And there's reason for that because chatting is a cause

204
00:22:34,560 --> 00:22:39,640
for unwholesome mind states. I've got a room here with people and they're not

205
00:22:39,640 --> 00:22:48,520
chatting while I talk. Consider that. Consider that when you sit here and chat while

206
00:22:48,520 --> 00:22:52,920
I'm giving a talk. How wholesome is that?

207
00:22:52,920 --> 00:23:06,760
So posting in the chat is not wrong. Don't feel guilty about it, but if you've

208
00:23:06,760 --> 00:23:12,400
been sitting there chatting, maybe you should feel a little guilty about it. Not

209
00:23:12,400 --> 00:23:20,480
guilty, but yeah, consider yourself admonished. Or not. I mean, maybe you disagree

210
00:23:20,480 --> 00:23:24,400
with me and think, I just want to sit here and chat. Maybe you've got Facebook

211
00:23:24,400 --> 00:23:31,400
open. Maybe you're smoking a cigarette or eating some food. I can't stop you. I

212
00:23:31,400 --> 00:23:36,440
can't police all that. That's the thing about the internet. It's very open. It's

213
00:23:36,440 --> 00:23:45,040
very broad audience. It's a bold and some people would say rash move to try and

214
00:23:45,040 --> 00:23:51,760
teach directly to the internet, which is why we kind of have some some buffer.

215
00:23:51,760 --> 00:23:57,840
Our questions are only answered through the website and they're vetted and so on.

216
00:23:57,840 --> 00:24:05,200
But there's something about being so open because the other side is that the

217
00:24:05,200 --> 00:24:12,200
Buddha was never so sheltered or he was he opened himself up to all sorts of

218
00:24:12,200 --> 00:24:17,840
people and of course was accused and confronted by people who had very bad

219
00:24:17,840 --> 00:24:25,920
intentions more so than just eating or chatting while he was talking. So it's

220
00:24:25,920 --> 00:24:35,600
I don't mind. I don't mean to suggest that I'm insulted or anything or upset by

221
00:24:35,600 --> 00:24:39,840
it. I just want you to consider because what is the point of listening to the

222
00:24:39,840 --> 00:24:46,120
Dhamma is to benefit you and your spiritual practice. So don't become

223
00:24:46,120 --> 00:24:52,800
distracted. Try and focus your attention and practice while you listen.

224
00:24:52,800 --> 00:25:07,600
Coming out. Have a good night.

