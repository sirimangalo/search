1
00:00:00,000 --> 00:00:19,520
Good evening everyone, welcome to our live broadcast evening done my session.

2
00:00:19,520 --> 00:00:25,520
So I'm just going, still going over my stocks that I have to give this weekend, thought

3
00:00:25,520 --> 00:00:39,200
it would be nice to go over a little more of it with you, be my test audience.

4
00:00:39,200 --> 00:00:47,160
So I'm going to give this talk on wrong mindfulness or wrong practice.

5
00:00:47,160 --> 00:00:55,480
And it's a somewhat difficult talk to get right because mindfulness is always good

6
00:00:55,480 --> 00:01:04,480
and, you know, we would have said Satin Chakwang, Bhikhavaya, Sabatikha, and Wandaan, mindfulness

7
00:01:04,480 --> 00:01:05,480
is always useful.

8
00:01:05,480 --> 00:01:15,400
It's the only quality if you take the seven bold junghas, seven factors of enlightenment,

9
00:01:15,400 --> 00:01:21,920
some of them are only useful when you're agitated to calm you down, some are only useful

10
00:01:21,920 --> 00:01:29,920
when you're lazy or overly concentrated to perk you up.

11
00:01:29,920 --> 00:01:37,920
But mindfulness is always useful, but it's it.

12
00:01:37,920 --> 00:01:50,280
And the idea that mindfulness practice could lead to problems, could be dangerous.

13
00:01:50,280 --> 00:02:04,560
Because especially, especially strange because mindfulness stops problems, that's what

14
00:02:04,560 --> 00:02:05,560
it's for.

15
00:02:05,560 --> 00:02:07,720
That's what it does.

16
00:02:07,720 --> 00:02:15,720
It stops, papan chat, stops us from making more out of things than they actually are.

17
00:02:15,720 --> 00:02:23,920
We would have said yani, sotani, low-customing, sati, taisang, niwarayan.

18
00:02:23,920 --> 00:02:26,320
Whatever streams there are in the world.

19
00:02:26,320 --> 00:02:31,800
Nobody used this word, stream, it's quite interesting.

20
00:02:31,800 --> 00:02:45,200
Whatever channels there are for problems basically, for stress, for suffering, for

21
00:02:45,200 --> 00:02:51,040
mental illness, if you will.

22
00:02:51,040 --> 00:02:59,640
Mindfulness stops them, mindfulness prevents them.

23
00:02:59,640 --> 00:03:04,880
Mindfulness stops all problems is basically it because mindfulness is about seeing things

24
00:03:04,880 --> 00:03:13,080
just as they are, it's about grasping experience for what it is.

25
00:03:13,080 --> 00:03:19,760
It's not getting lost in what we want it to be or what we think it is.

26
00:03:19,760 --> 00:03:32,840
What we think of it, right, wrong, good, bad, me, mine, all of those have no place in mindfulness.

27
00:03:32,840 --> 00:03:39,080
So how is it that people practice meditation and have problems?

28
00:03:39,080 --> 00:03:45,720
I'm happy to say, I mean maybe, verging on a little bit proud to say that I've never

29
00:03:45,720 --> 00:03:54,680
had a meditator go crazy on me, but I have seen meditators go crazy.

30
00:03:54,680 --> 00:04:03,720
I spent three nights in a mental hospital before I was a monk with a crazy meditator.

31
00:04:03,720 --> 00:04:12,000
Who had gone off the reservation, said, no, off the, what's the word, I'm gone crazy.

32
00:04:12,000 --> 00:04:16,880
And it was quite an experience.

33
00:04:16,880 --> 00:04:20,800
So how does it happen?

34
00:04:20,800 --> 00:04:25,880
And my last talk, I already talked about these, we'll go over them again.

35
00:04:25,880 --> 00:04:36,640
On your unmindful, your mindfulness is misdirected.

36
00:04:36,640 --> 00:04:42,280
Three, your mindfulness is lapsed.

37
00:04:42,280 --> 00:04:46,720
And four, your mindfulness is impotent or ineffectual.

38
00:04:46,720 --> 00:04:54,120
It's the only ways I can see that mindfulness could be, could go wrong.

39
00:04:54,120 --> 00:05:00,600
The first to be unmindful goes without saying that if you don't have a practice mindfulness,

40
00:05:00,600 --> 00:05:08,320
it can't possibly help you, but there are people who go to meditation centers and whatever

41
00:05:08,320 --> 00:05:11,480
reason don't actually practice mindfulness.

42
00:05:11,480 --> 00:05:18,400
I remember once I had one, it's my most shameful experience as a teacher, I wasn't

43
00:05:18,400 --> 00:05:25,560
going to teach her that long, so there was this young Thai man, and I said to him, okay,

44
00:05:25,560 --> 00:05:31,200
so I start off with this many minutes walking, this many minutes sitting, you know, start

45
00:05:31,200 --> 00:05:37,720
off, we start off with like 10 of walking and 10 of sitting, just do them together.

46
00:05:37,720 --> 00:05:41,120
And he came back the next day and everything's fine.

47
00:05:41,120 --> 00:05:44,720
The next day and the next day and it was about six, seven days, nothing.

48
00:05:44,720 --> 00:05:54,760
No pain, no stress, didn't have any challenges, like he wasn't dealing with anything.

49
00:05:54,760 --> 00:05:57,480
And so I asked him, how many rounds are you doing a day?

50
00:05:57,480 --> 00:05:59,520
He said one.

51
00:05:59,520 --> 00:06:06,360
I told him to do 10 minutes walking, 10 minutes sitting, that's what he did.

52
00:06:06,360 --> 00:06:10,160
I said, what did you do for the rest of the day?

53
00:06:10,160 --> 00:06:21,360
I said around the red, red comics or something, yeah, I didn't know what to do.

54
00:06:21,360 --> 00:06:28,400
He'd gone through half the course by this time, so I sent him off to one of the senior

55
00:06:28,400 --> 00:06:32,800
teachers and said, yeah, I'm making their problem.

56
00:06:32,800 --> 00:06:40,560
Well, I felt kind of bad, it was, it was personally my fault for not being cleared.

57
00:06:40,560 --> 00:06:47,480
So in 10 minutes, 10 minutes is per session and we don't, of course, time, we aren't

58
00:06:47,480 --> 00:06:53,080
exact, we aren't, of course, looking for a quote of hours a day or something like that because

59
00:06:53,080 --> 00:06:57,160
the other thing, of course, you can walk and sit for hours and get no benefit if you're

60
00:06:57,160 --> 00:07:04,520
not mindful.

61
00:07:04,520 --> 00:07:10,680
If anyone tells you how long they've been meditating, it's interesting information,

62
00:07:10,680 --> 00:07:16,640
but it's not all that useful because they haven't been meditating for 10 years or 20

63
00:07:16,640 --> 00:07:17,640
years.

64
00:07:17,640 --> 00:07:23,160
I don't know how many minutes, how many moments they've been mindful for, that's much

65
00:07:23,160 --> 00:07:28,040
more interesting.

66
00:07:28,040 --> 00:07:33,080
So if you're on mindful, of course, there's not much we can say, you certainly can't blame

67
00:07:33,080 --> 00:07:38,880
the meditation practice, but living in an environment like this without being mindful,

68
00:07:38,880 --> 00:07:43,640
that could very easily drive you crazy.

69
00:07:43,640 --> 00:07:49,240
The second is misdirected mindfulness.

70
00:07:49,240 --> 00:07:57,120
This is, I think potentially, where a lot of the problems come from, because if you're mindful

71
00:07:57,120 --> 00:08:01,000
of concepts, there is a lot more danger.

72
00:08:01,000 --> 00:08:08,400
I would go on a limb and say, summit to meditation, which focuses on concept, it's much

73
00:08:08,400 --> 00:08:13,640
more dangerous than we pass in the meditation, and I would bet there are statistics you

74
00:08:13,640 --> 00:08:20,000
could find that you could accumulate statistics to show that.

75
00:08:20,000 --> 00:08:28,120
Because reality is quite limited, I mean, isn't this, it can be quite boring at times.

76
00:08:28,120 --> 00:08:31,840
Reality turns out to be seeing, hearing, smelling, tasting, feeling, thinking, where's

77
00:08:31,840 --> 00:08:33,240
the fun in that?

78
00:08:33,240 --> 00:08:42,360
I mean, if you get into it, it's quite exciting, but there's none of the endless possibilities

79
00:08:42,360 --> 00:08:45,080
of tranquility meditation.

80
00:08:45,080 --> 00:08:46,080
What would you rather do?

81
00:08:46,080 --> 00:08:52,520
Remember your past dives or sit here saying rising, falling, rising, falling, right?

82
00:08:52,520 --> 00:09:00,360
Or again, magical powers, read people's minds, all these things, that would be more fun.

83
00:09:00,360 --> 00:09:04,440
The world of concepts is infinite.

84
00:09:04,440 --> 00:09:10,240
You can imagine a hair with horn, a rabbit with horns.

85
00:09:10,240 --> 00:09:13,000
You can imagine a rabbit with antlers.

86
00:09:13,000 --> 00:09:20,280
You can imagine a pink rabbit with antlers or a blue or a yellow, right?

87
00:09:20,280 --> 00:09:26,560
Because concepts are infinite, you can easily get lost very easily without strict guidance

88
00:09:26,560 --> 00:09:30,640
and regulation.

89
00:09:30,640 --> 00:09:35,480
When you're mindful of concepts, and by mindful here, again, we just mean when you grasp

90
00:09:35,480 --> 00:09:46,200
the concept, it's possible to get caught up in the concept, and I would still argue that

91
00:09:46,200 --> 00:09:48,960
mindfulness isn't to blame here.

92
00:09:48,960 --> 00:09:57,760
Because in Samatai, you still need mindfulness, and if you have mindfulness, it can't

93
00:09:57,760 --> 00:10:00,800
possibly go wrong, because you grasp the object.

94
00:10:00,800 --> 00:10:08,600
For example, a candle flame, say an easy example, you focus on the candle flame, and if

95
00:10:08,600 --> 00:10:14,240
you're mindful of it, then you know that this is fire, and you say fire, fire, fire,

96
00:10:14,240 --> 00:10:21,400
and you're grasping the concept of fire, well, that mindfulness is a good thing.

97
00:10:21,400 --> 00:10:31,360
That's what keeps you from seeing it as more or different, but the thing about concepts

98
00:10:31,360 --> 00:10:34,760
is that they can become, because they're in your mind, eventually you get the picture

99
00:10:34,760 --> 00:10:46,760
of the fire in your mind, they can more and become different things.

100
00:10:46,760 --> 00:10:54,840
You're mindful of the past, mindful of the future.

101
00:10:54,840 --> 00:11:01,400
Much more likely, and why this is really considered wrong, we talked about this, is that

102
00:11:01,400 --> 00:11:07,480
it's just not the way to enlightenment, there's nothing wrong with it, in fact, it can be

103
00:11:07,480 --> 00:11:14,280
actually indirectly helpful, because it gained great concentration.

104
00:11:14,280 --> 00:11:18,400
Much more common, and you do see this quite a bit, are people, meditators who've been

105
00:11:18,400 --> 00:11:26,400
practicing for years, and have a very strong meditation practice, but I've never attained

106
00:11:26,400 --> 00:11:34,480
Nibana, I've never realized cessation of suffering, because they have pleasant experiences,

107
00:11:34,480 --> 00:11:44,200
because they're focusing on concepts, and really clear on reality.

108
00:11:44,200 --> 00:11:52,280
The third lapsed mindfulness, lapsed mindfulness I think is where real danger for one's mental

109
00:11:52,280 --> 00:12:06,360
health is, and this you see, because when you meditate, it brings up, this is where meditation

110
00:12:06,360 --> 00:12:12,400
is potentially quite dangerous, simply because it brings up deep dark emotions that can

111
00:12:12,400 --> 00:12:19,040
be dangerous, both, I mean, I want to scare you, and most people, this isn't a problem,

112
00:12:19,040 --> 00:12:24,600
but it's important to go slow, and it's important, in some cases, it's really important

113
00:12:24,600 --> 00:12:31,440
to have a teacher, most people don't have really crazy things inside, but it can happen

114
00:12:31,440 --> 00:12:43,600
that things come up that you're not easily able to deal with, one of the great things

115
00:12:43,600 --> 00:12:51,400
of watching an experienced teacher in action, my teacher, there was one time where it

116
00:12:51,400 --> 00:12:57,640
stands out, I remember, sitting up in his kutti, waiting for meditators, and suddenly

117
00:12:57,640 --> 00:13:03,760
one of the nuns who worked in the office says, couple runs in and says, there's a meditator

118
00:13:03,760 --> 00:13:12,920
outside of the office, and she's going crazy, she's saying nonsense and running around

119
00:13:12,920 --> 00:13:18,000
like a crazy person, and he goes, tell her to be mindful, or no, he just said, be mindful,

120
00:13:18,000 --> 00:13:22,520
be mindful, because this nun herself was quite unmindful, and the way he just sat there

121
00:13:22,520 --> 00:13:30,440
and said, yeah, yeah, just be mindful, like we're all jumping up and getting ready to go

122
00:13:30,440 --> 00:13:42,840
and save the day, because things come up and you react to them.

123
00:13:42,840 --> 00:13:49,000
You get disturbed by them, you get taken off guard, it happens with both positive and negative

124
00:13:49,000 --> 00:13:54,400
situations, with positive conditions, you get lost in them, hey, that's interesting,

125
00:13:54,400 --> 00:14:00,200
that's nice, and because you stop being mindful, and both because you stop being mindful

126
00:14:00,200 --> 00:14:06,160
and because of the, they're so strong, and they're so powerful, they can really suck

127
00:14:06,160 --> 00:14:12,120
you in negative as well, if you have negative things from the most of us have something

128
00:14:12,120 --> 00:14:17,960
negative from the past, and it sucks you in, it comes as a result, you know, it shows

129
00:14:17,960 --> 00:14:23,160
itself as a result of the practice, so that's a good thing, both of them are really good,

130
00:14:23,160 --> 00:14:27,480
I mean, it's good that positive things come out, but negative things come out, it's good

131
00:14:27,480 --> 00:14:34,080
that the deep stuff comes out, because it's, it means you're becoming more in tune with

132
00:14:34,080 --> 00:14:44,360
yourself and more in tune with reality, and you're getting a deeper experience of who you are,

133
00:14:44,360 --> 00:14:50,240
but without a, a stabilizing presence, someone to remind you or without reminding yourself

134
00:14:50,240 --> 00:14:57,440
that, hey, it's still just an experience, it can be dangerous, there were these in the,

135
00:14:57,440 --> 00:15:04,120
in the texts, in the Senutany guy, I think, there are these monks who would have taught

136
00:15:04,120 --> 00:15:12,000
them mindfulness of, I think it was mindfulness of the load, soonness of the body, I can't

137
00:15:12,000 --> 00:15:15,360
remember what it was now, but they taught them something, and they all killed themselves

138
00:15:15,360 --> 00:15:21,160
as a result, but they had someone killed them, they hired some guy to kill them, because

139
00:15:21,160 --> 00:15:28,520
they were so revolted by, it was just such a bizarre story, like they reacted so violently

140
00:15:28,520 --> 00:15:33,960
to this, commentary says they had really bad karma, they had been hunters in past lives,

141
00:15:33,960 --> 00:15:43,040
but the point is that things can come out, and if you're not, if you're not ready for

142
00:15:43,040 --> 00:15:50,800
the truth, you can't handle the truth, and it can be potentially problematic, so not

143
00:15:50,800 --> 00:15:56,560
to be clear that mine, this is one way that mindfulness practice is, I've never seen it

144
00:15:56,560 --> 00:16:03,960
happen with any of my students, but mindfulness meditation is potentially dangerous.

145
00:16:03,960 --> 00:16:09,880
There was one, one meditator that, this meditator that I spent time in the mental hospital

146
00:16:09,880 --> 00:16:16,000
with, I was in her teacher, but the problem was she had, I think, too much instruction

147
00:16:16,000 --> 00:16:23,200
from wrong people, there were some people who weren't her teachers going in and going

148
00:16:23,200 --> 00:16:29,120
into her room and talking to her and feeding her all sorts of weird ideas, like there's

149
00:16:29,120 --> 00:16:34,880
a lot of superstition and folk Buddhism and telling her things about past lives and karma,

150
00:16:34,880 --> 00:16:43,400
and really getting her lost and winding her up, I think, because suddenly she's out

151
00:16:43,400 --> 00:16:49,560
running around and she was using the mantras, but she was saying, she was using them

152
00:16:49,560 --> 00:16:55,240
to wind herself up, so she would say to herself wisdom, wisdom, wisdom, I remember that

153
00:16:55,240 --> 00:17:01,720
was one, like she would just repeat a mantra to her, but it would wind her up instead

154
00:17:01,720 --> 00:17:08,640
of calmer down, because it wasn't based on any experience.

155
00:17:08,640 --> 00:17:15,360
This is an example, people get the mantra wrong, instead of using it to note what's there,

156
00:17:15,360 --> 00:17:25,040
they use it to note what they want to be there, and that'll wind you up.

157
00:17:25,040 --> 00:17:30,120
It's quite easy to get lost if you're not clear about the practice, and my mindfulness

158
00:17:30,120 --> 00:17:37,960
is just the reason I think you would, you don't see people going crazy practicing mindfulness

159
00:17:37,960 --> 00:17:53,920
is because it's so, well, just because it's such a calming influence, mindfulness itself

160
00:17:53,920 --> 00:18:01,440
stops all that anyway, so the lapse to mindfulness, it happens with good experiences,

161
00:18:01,440 --> 00:18:06,400
it happens with bad experiences, if you're not careful to catch it and keep going.

162
00:18:06,400 --> 00:18:15,400
At least at the least you'll get stuck, at the worst you can get caught up and go

163
00:18:15,400 --> 00:18:26,440
temporarily AWOL, and the fourth is, when I talked about impotent mindfulness, or probably

164
00:18:26,440 --> 00:18:33,320
what the Buddha would have explained as wrong mindfulness, and that is wrong mindfulness

165
00:18:33,320 --> 00:18:38,760
that is blocked by something.

166
00:18:38,760 --> 00:18:45,680
So one common example is with those people who have taken the body sat for vows, they've

167
00:18:45,680 --> 00:18:51,920
taken a vow to become a Buddha, they've taken a vow to not become enlightened, that's

168
00:18:51,920 --> 00:18:58,440
basically what it says, I vow not to enter into Nibbana until all beings are ready to become

169
00:18:58,440 --> 00:19:08,160
enlightened, and that prevents them, of course, from, I mean it actually does, this is not

170
00:19:08,160 --> 00:19:14,360
a theoretical thing, we've had meditators, I had one monk, another guy I took him to the

171
00:19:14,360 --> 00:19:22,120
mental hospital, I remember that night, he tried to kill himself on several occasions,

172
00:19:22,120 --> 00:19:27,240
because he was, on one hand, trying to become a Buddha, and on the other hand, trying

173
00:19:27,240 --> 00:19:35,240
to practice insight meditation to become an Aran, and he had other issues, it was probably

174
00:19:35,240 --> 00:19:47,280
fairly specific to him, but he didn't eventually drive himself greatly, but a more common

175
00:19:47,280 --> 00:19:52,920
one, I mean people who have a very strong belief in self, or belief in God, or that kind

176
00:19:52,920 --> 00:19:59,280
of thing, this prevents you from letting go and experiencing things objectively, if you have

177
00:19:59,280 --> 00:20:03,720
a sense that everything's going to work out, or God has a plan for us, or something like

178
00:20:03,720 --> 00:20:11,400
that, you can very much get in the way, I mean this is wrong view, according to Buddhism,

179
00:20:11,400 --> 00:20:16,160
and I went through them, you know, if you have a wrong thought, if you're wishing ill of

180
00:20:16,160 --> 00:20:25,360
other people, and it obstructs your path, or if you have lots of desires or ambitions,

181
00:20:25,360 --> 00:20:30,880
if you're arrogant to that kind, I think these will get in the way, if you have a wrong

182
00:20:30,880 --> 00:20:36,960
speech, if you're saying nasty things about people, if you're a liar, or a gossip, or that

183
00:20:36,960 --> 00:20:47,360
kind of thing, if you just talk too much, you know, that'll get in your way, wrong action,

184
00:20:47,360 --> 00:21:06,680
if you're a thief, a murderer, an adulterer, an adulterer, if you have wrong livelihood,

185
00:21:06,680 --> 00:21:17,120
if you make money through killing or stealing, that kind of thing, if you have wrong

186
00:21:17,120 --> 00:21:23,800
effort, if you're lazy, or if you just put your effort out to become to do bad things,

187
00:21:23,800 --> 00:21:37,320
if you have wrong concentration, if you're unfocused, or if you're focused on the wrong

188
00:21:37,320 --> 00:21:45,840
things, mindfulness needs all of these other qualities, and most especially things like

189
00:21:45,840 --> 00:21:53,040
right view and right action, I think if you don't have morality and wisdom, concentration

190
00:21:53,040 --> 00:22:04,040
aspect with mindfulness and so on, can't develop, so for most people the consequence of

191
00:22:04,040 --> 00:22:10,040
wrong mindfulness is simply they'll stop practicing, they'll be discouraged and they'll

192
00:22:10,040 --> 00:22:19,120
feel like they'll reach an impasse where they can get past people practice, whether

193
00:22:19,120 --> 00:22:28,160
they practice properly or improperly, without a solid training in mindfulness, a good

194
00:22:28,160 --> 00:22:37,040
teacher and a good mind that is able to receive it, it's quite common for people to just

195
00:22:37,040 --> 00:22:43,640
give it up, they get afraid or they get discouraged or they get disinterested because

196
00:22:43,640 --> 00:22:51,800
they don't get it, but as I said, another consequence is that they continue practicing

197
00:22:51,800 --> 00:22:57,640
but don't get anywhere from practice and they just feel peaceful or calm, that kind

198
00:22:57,640 --> 00:23:07,880
of thing, thought of letting go without ever becoming free from ego or desire, that kind

199
00:23:07,880 --> 00:23:18,120
of thing, and in extreme cases the consequences that people will crazy, the crazy is usually

200
00:23:18,120 --> 00:23:22,280
temporary, I don't think I've ever heard of meditation driving you crazy for the rest

201
00:23:22,280 --> 00:23:28,040
of your life, but it's temporary insanity, they get so wound up that they find it hard

202
00:23:28,040 --> 00:23:35,800
to come back down, and usually what happens is they get medicated, usually they go to,

203
00:23:35,800 --> 00:23:41,440
there's intervention because they start going crazy and hitting people and running around

204
00:23:41,440 --> 00:23:46,000
tearing their clothes off that kind of thing, so eventually they get institutionalized

205
00:23:46,000 --> 00:23:54,200
and medicated by society and then they gradually calm down and maybe they go see a therapist

206
00:23:54,200 --> 00:24:03,280
and people start to think Buddhism is maybe a bad thing, they're papers about this,

207
00:24:03,280 --> 00:24:09,840
they just, someone sent me a paper about how a mindfulness practice or meditation practice,

208
00:24:09,840 --> 00:24:15,480
I guess in general, I didn't really read it because I don't know how interested I am

209
00:24:15,480 --> 00:24:22,320
in what it says, but I can say as I've never had it happen to any of my students, so

210
00:24:22,320 --> 00:24:31,280
across our fingers, no, quite confident it won't happen, this technique is quite solid

211
00:24:31,280 --> 00:24:38,480
and concrete, right, it's hard to do it wrong, it's hard to have a wrong understanding

212
00:24:38,480 --> 00:24:44,080
of how it works after a few days of me drilling it into you, exactly what you have to

213
00:24:44,080 --> 00:24:49,720
do, it's pretty hard to get on the wrong path, so it's not something anyone should worry

214
00:24:49,720 --> 00:24:57,120
about, the only thing you have to worry about is if you're not doing it, that's all, so

215
00:24:57,120 --> 00:25:04,480
there you go, a little bit more done with today, hopefully I wasn't too much of a repetition,

216
00:25:04,480 --> 00:25:34,160
thank you all for coming up.

217
00:25:34,160 --> 00:25:52,560
Thank you all for coming up.

