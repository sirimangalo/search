1
00:00:00,000 --> 00:00:10,160
And there are people who are sickly, that means, who always have some disease or another.

2
00:00:10,160 --> 00:00:16,680
And there are people who are healthy, there are people who are ugly and who are beautiful,

3
00:00:16,680 --> 00:00:25,840
there are people who are uninfluentia and there are others who are influential, there are

4
00:00:25,840 --> 00:00:33,720
people who are poor and who are wealthy, there are people who are low born and others who

5
00:00:33,720 --> 00:00:44,160
are high born and there are people who lack in wisdom, that means, who are stupid and there

6
00:00:44,160 --> 00:00:47,720
are people who are wise or intelligent.

7
00:00:47,720 --> 00:00:55,080
So altogether, there are 14 kinds of people and he pointed out these people to the Buddha

8
00:00:55,080 --> 00:01:08,400
and ask why some people, if long and some people live short and so on, then the Buddha

9
00:01:08,400 --> 00:01:20,480
gave him the answer very briefly and Buddha said, my young man, beings have come up

10
00:01:20,480 --> 00:01:29,600
as their own property, beings have come up, beings are heir of the Akama, the originates

11
00:01:29,600 --> 00:01:40,240
from the Akama, they are bound to the Akama or the relatives to the Akama and they have

12
00:01:40,240 --> 00:01:50,320
come up as their refuge and it is Kama, that distinguishes beings as inferior and superior or

13
00:01:50,320 --> 00:01:58,920
it is Kama that makes people here, human beings different.

14
00:01:58,920 --> 00:02:08,240
So Buddha gave him a very brief answer and he said he did not understand well.

15
00:02:08,240 --> 00:02:18,560
So he said, and I do not understand the meaning of your statement which is a very brief

16
00:02:18,560 --> 00:02:21,520
without exposition.

17
00:02:21,520 --> 00:02:31,400
So it would be good if you would explain to me in detail and so the Buddha explained

18
00:02:31,400 --> 00:02:41,460
him in detail and Buddha gave very brief answer to him because Buddha knew that the

19
00:02:41,460 --> 00:02:50,560
Subha was a proud man, he thought he could understand anything the Buddha taught to him

20
00:02:50,560 --> 00:03:02,400
and so Buddha wanted to put him down a little so that he lost his pride and is able to

21
00:03:02,400 --> 00:03:04,920
follow the teachings of the Buddha.

22
00:03:04,920 --> 00:03:16,560
So when he requested for a detailed explanation, the Buddha gave the answer in some detail.

23
00:03:16,560 --> 00:03:36,480
So these are all put on the page and you can look at it and it is not easy to understand.

24
00:03:36,480 --> 00:03:46,640
And though these questions are put by Subha, these are the questions we always want to

25
00:03:46,640 --> 00:03:58,360
ask and we want to know the answer to these questions because we have met these problems

26
00:03:58,360 --> 00:04:13,640
in human beings, even born of the same parents and brought up under the same circumstances

27
00:04:13,640 --> 00:04:22,880
brothers and sisters are different, one may be intelligent while the other is dull, one

28
00:04:22,880 --> 00:04:29,680
may live long while the other doesn't and so on, so this is a question we always want

29
00:04:29,680 --> 00:04:38,560
to ask and we always want to know the answer to these questions.

30
00:04:38,560 --> 00:04:46,600
And if you ask these questions to a person who believes in creation, who believes in

31
00:04:46,600 --> 00:04:55,000
a Brahma or a God, you will end up with the answer because it was created by God or Brahma

32
00:04:55,000 --> 00:05:05,120
that way, but that is not satisfactory, there will be many questions following that, but

33
00:05:05,120 --> 00:05:18,720
the Buddha's answers are very convincing and very reasonable and so satisfactory.

34
00:05:18,720 --> 00:05:37,120
The Buddha gives answers to these one by one.

35
00:05:37,120 --> 00:05:50,160
Now, since the offspring of the same parents are brought up under the same circumstances

36
00:05:50,160 --> 00:05:59,600
or environment and they are born of the same parents, we cannot point to the environment

37
00:05:59,600 --> 00:06:09,920
as the reason or as a cause for their inequality or we cannot find out to heritage as

38
00:06:09,920 --> 00:06:15,480
a cause because they are born of the same parents under the same conditions.

39
00:06:15,480 --> 00:06:24,880
So the reason must be somewhere beyond the heritage and circumstances or environment and

40
00:06:24,880 --> 00:06:32,720
if we cannot find the reason or cause in this life then we have to go beyond this life

41
00:06:32,720 --> 00:06:47,200
backward and so Buddha found that it was something being ditched in the past that makes

42
00:06:47,200 --> 00:06:51,600
them different in this life.

43
00:06:51,600 --> 00:07:02,040
Now, Buddha said some people kill living beings, not just ones, many times they kill living

44
00:07:02,040 --> 00:07:12,560
beings and as a result of that killing they are reborn in full, woeful stage, they are

45
00:07:12,560 --> 00:07:22,000
reborn in hell and so on and if they come up to the human existence that means if they

46
00:07:22,000 --> 00:07:33,200
are reborn as human beings then they will live a very short life because they are in

47
00:07:33,200 --> 00:07:41,560
Dutch and killing living beings and so their minds are intent upon shortening the life.

48
00:07:41,560 --> 00:07:50,120
So that shortening the life, the intent is shortening the life give results back to him

49
00:07:50,120 --> 00:07:59,080
in being short lived and there are people who do not kill living beings who refrain from

50
00:07:59,080 --> 00:08:06,960
killing living beings and these people after they are dead they are reborn in the celestial

51
00:08:06,960 --> 00:08:12,400
walls and when they are reborn as human beings they live long lives.

52
00:08:12,400 --> 00:08:23,400
So people who live short lives are those who are practiced killing living beings in their

53
00:08:23,400 --> 00:08:29,440
past lives and those who live long are those who are afraid from killing living beings when

54
00:08:29,440 --> 00:08:39,840
you are afraid from killing living beings, your mind is on, lengthening lives and that desire

55
00:08:39,840 --> 00:08:52,440
to land in their lives causes your life to be long when you are reborn as a human being.

56
00:08:52,440 --> 00:09:05,080
Now there are people who are sickly, who have always one kind of illness or another all

57
00:09:05,080 --> 00:09:14,200
the time and who lead unhealthy lives and these people according to the teachings of the Buddha

58
00:09:14,200 --> 00:09:24,400
those who injure other beings, now these people do not kill living beings but they

59
00:09:24,400 --> 00:09:30,800
are cruel to living beings, they inflict pain on other beings, they torture other beings

60
00:09:30,800 --> 00:09:39,160
and so on and so people who injure other beings when they die are reborn in full woeful

61
00:09:39,160 --> 00:09:47,800
states and when they are reborn as human beings they are not healthy, they are always

62
00:09:47,800 --> 00:09:55,560
in illness and those who are healthy in this life there are some people who have now been

63
00:09:55,560 --> 00:10:11,080
to a doctor to have never been to a clinic who have never even taken a pill or something

64
00:10:11,080 --> 00:10:18,720
so they are very healthy and why are they healthy in this life because in their past lives

65
00:10:18,720 --> 00:10:29,960
they refrain from injuring other people, they do not inflict pain to other people so

66
00:10:29,960 --> 00:10:37,120
actually they are confident to other people and so these people are those who when they

67
00:10:37,120 --> 00:10:49,520
are reborn as human beings and they have a healthy life and why some are ugly and some

68
00:10:49,520 --> 00:11:01,080
are beautiful, our Buddha said a button is of an angry irritable character, such people

69
00:11:01,080 --> 00:11:14,520
even when criticized a little they are offended, they become angry and being of an angry

70
00:11:14,520 --> 00:11:23,600
nature they be reborn in the full woeful states and when they are reborn as human beings

71
00:11:23,600 --> 00:11:38,000
they will be ugly so anger is a cause of ugliness we can understand this very easily even

72
00:11:38,000 --> 00:11:52,960
when we are living now when we are angry we are ugly we do not look pleasant so if

73
00:11:52,960 --> 00:12:00,320
we want to know how ugly we are then when we are angry we may look ourselves in a mirror

74
00:12:00,320 --> 00:12:09,200
and we see how ugly we are at that time and those who are beautiful are not of angry

75
00:12:09,200 --> 00:12:19,040
nature so they are people who are kind to other people and who do not get angry easily

76
00:12:19,040 --> 00:12:34,000
so if you want to be beautiful even in this life try to try not to get angry or try

77
00:12:34,000 --> 00:12:48,720
to practice and mitter so the cause of ugliness is anger and the cause of beauty is absence

78
00:12:48,720 --> 00:12:59,960
of anger so it teaches us that if we want to be beautiful if we want to look pleasant

79
00:12:59,960 --> 00:13:13,680
then we are not to get angry easily and there are people who are uninfluential and there

80
00:13:13,680 --> 00:13:25,520
are others who are influential sometimes you see people who are powerful or who talk with

81
00:13:25,520 --> 00:13:38,640
authority and everybody wants to follow that person and also there are people who are powerless

82
00:13:38,640 --> 00:13:46,840
and who have no authority and so on so the cause of these is what those who are uninfluential

83
00:13:46,840 --> 00:13:55,200
are those who are envious in their past lives who envy the game the honor the respect

84
00:13:55,200 --> 00:14:03,640
the reverence the salutations the venerations other people receive now if we are envious

85
00:14:03,640 --> 00:14:11,800
of or if we are jealous of other people's gains on us and so on then we are sure to

86
00:14:11,800 --> 00:14:20,000
be an influential in our future lives and those who are influential in this life and those

87
00:14:20,000 --> 00:14:31,880
who are not envious who are not envious of the games the honor the respect the reverence

88
00:14:31,880 --> 00:14:42,000
the salutations the generations other people get so to be envious is the cause of being

89
00:14:42,000 --> 00:14:55,360
uninfluential and not to be envious is the cause of being influential in this life now

90
00:14:55,360 --> 00:15:01,600
there are people who are poor and also there are people who are wealthy those who are poor

91
00:15:01,600 --> 00:15:14,680
in this life are those who do not give food clothing and other things to to recluse

92
00:15:14,680 --> 00:15:22,680
as to Brahmins and to other people or who do not practice Ghana and those who are wealthy

93
00:15:22,680 --> 00:15:31,160
in this life are those who practice Ghana India past lives who give food strength clothing and

94
00:15:31,160 --> 00:15:46,680
other things to other people so practice of Ghana is the cause of being wealthy and not

95
00:15:46,680 --> 00:15:55,000
practicing of Ghana or being stingy so that we do not give anything to anybody is the

96
00:15:55,000 --> 00:16:09,200
cause of poverty and then there are people who are born lowly and who are born highly sometimes

97
00:16:09,200 --> 00:16:18,000
you miss fine people you may meet people who are born as outcasts and also people who are

98
00:16:18,000 --> 00:16:32,320
born in high families now the cause of a low birth is being obstinate and arrogant or

99
00:16:32,320 --> 00:16:51,800
pride such people do not be homage to those who are worthy of homage do not rise up for one

100
00:16:51,800 --> 00:17:00,320
in whose presence he should rise up who does not offer a seat to one who deserves a seat

101
00:17:00,320 --> 00:17:09,600
who does not make way for one for whom he should make way and who does not honor respect

102
00:17:09,600 --> 00:17:19,960
via and venerate one who should be honored and so on so not being respectful to others

103
00:17:19,960 --> 00:17:30,560
is the cause of a low birth and the cause of high birth is the opposite of that that is

104
00:17:30,560 --> 00:17:43,080
not obstinate and not being obstinate and not being arrogant that is being homage to those

105
00:17:43,080 --> 00:17:49,800
who deserves is homage being respectful to those who deserve his respect and rising from

106
00:17:49,800 --> 00:18:01,400
the seat and so on so being proud is the cause of being low born and absence of pride

107
00:18:01,400 --> 00:18:16,440
being not arrogant is the cause of being high born and then there are those who are

108
00:18:16,440 --> 00:18:22,760
lacking in wisdom who are not intelligent who are Dha who are stupid and also there are

109
00:18:22,760 --> 00:18:35,040
those who are wise intelligent quick with it and the difference is caused by asking questions

110
00:18:35,040 --> 00:18:41,640
and not asking questions so those who do not ask what is wholesome what is unwholesome

111
00:18:41,640 --> 00:18:50,440
and so on what is allowable and what is not allowed and so on so those who do not ask

112
00:18:50,440 --> 00:19:02,480
questions like these when they are reborn as a human being and intelligent they are Dha

113
00:19:02,480 --> 00:19:15,080
or stupid people and those who ask questions regarding wholesomeness of actions and others

114
00:19:15,080 --> 00:19:23,400
and they are reborn as human being they are intelligent and they have wisdom and they

115
00:19:23,400 --> 00:19:36,640
are wise so asking questions regarding regarding the dhamma actually so asking question is

116
00:19:36,640 --> 00:19:44,720
the cause of being wise of being intelligent and not asking question is a cause of lacking

117
00:19:44,720 --> 00:20:00,200
in wisdom so now you know which practice leads to which condition now killing living beings

118
00:20:00,200 --> 00:20:08,400
the practice of killing living beings lead to a short life and the practice of refraining

119
00:20:08,400 --> 00:20:20,200
from killing living beings leads to a long life and so here we have choice if you want

120
00:20:20,200 --> 00:20:27,200
to live long we need to refrain from killing living beings and if you don't mind or if

121
00:20:27,200 --> 00:20:35,120
we don't care to live long we may practice killing living beings in the same way if we want

122
00:20:35,120 --> 00:20:42,640
to be heavy you know a future life that we refrain from ensuring other people if we want

123
00:20:42,640 --> 00:20:53,360
to be beautiful then we are not to get angry if we want to be influential we should get

124
00:20:53,360 --> 00:21:00,680
rid of envy or jealousy if we want to be wealthy we should practice dana if we want

125
00:21:00,680 --> 00:21:09,000
to be high born persons then we should be humble in this life we should not be arrogant

126
00:21:09,000 --> 00:21:22,920
and if we want to be intelligent then we must ask questions so these are the reasons why

127
00:21:22,920 --> 00:21:32,280
some people live long and some people live short and so on so Buddha gave answers to

128
00:21:32,280 --> 00:21:44,120
the questions put by Subha and at the end of this discourse Buddha again said that beings

129
00:21:44,120 --> 00:21:52,040
have come as their own property and so on and it is it is come up that distinguishes

130
00:21:52,040 --> 00:22:05,400
human beings into superior inferior that make difference in human beings so the Buddha's

131
00:22:05,400 --> 00:22:17,120
answer is not not the creation of any deity or any court or whatever but one because

132
00:22:17,120 --> 00:22:26,640
of one's own actions I mean one's own come as in the past one and gets to live long

133
00:22:26,640 --> 00:22:38,640
or to live short in this life so it is the come up that makes people different in this

134
00:22:38,640 --> 00:22:59,640
life now what is come up that is a cause of the difference in beings now come up is usually

135
00:22:59,640 --> 00:23:16,560
translated as an act or an action or a deed protectively come up means a mental state

136
00:23:16,560 --> 00:23:25,200
a mental state called volition or in Pali Chitana now there is a mental state of mental

137
00:23:25,200 --> 00:23:37,160
factor called volition which which wheels by which people will to do something or whenever

138
00:23:37,160 --> 00:23:47,180
we do something by body or by speech or by mind there is this volition in our minds

139
00:23:47,180 --> 00:24:00,160
prompted by this volition we do actions by body or by speech or by mind but since every

140
00:24:00,160 --> 00:24:13,640
action either of body or of speech or of mind is always accompanied by this volition come

141
00:24:13,640 --> 00:24:25,720
up is come to be translated as action or a deed so we must understand that when we are

142
00:24:25,720 --> 00:24:34,240
not seriously talking we may say that come up means an action or a deed but if we want

143
00:24:34,240 --> 00:24:44,640
to be precise and technical we should say that come up is a volition now that volition

144
00:24:44,640 --> 00:24:57,140
is a mental state of mental factor that arises in our minds and as a mental state it arises

145
00:24:57,140 --> 00:25:04,840
and it disappears because everything in the world is impermanent so everything arises and

146
00:25:04,840 --> 00:25:12,400
disappears so it is a mental state and so as a mental state it arises and disappears but

147
00:25:12,400 --> 00:25:20,280
it is different from other kinds of mental states other mental states arise and disappear

148
00:25:20,280 --> 00:25:31,400
and leaving nothing behind but this volition when it disappears leaves something some

149
00:25:31,400 --> 00:25:42,560
potential to give results in the mental continuum of beings that is why when the conditions

150
00:25:42,560 --> 00:25:56,320
are favorable for it come up to give results the results appear once Kim Milinda asked

151
00:25:56,320 --> 00:26:05,280
the vulnerable Nagazena whether the vulnerable Nagazena can point out where that come up

152
00:26:05,280 --> 00:26:16,160
was stored and the vulnerable Nagazena said it is impossible to say exactly where they

153
00:26:16,160 --> 00:26:25,120
come up was stored but it depending upon the continuity or depending upon the mind and

154
00:26:25,120 --> 00:26:37,080
body it and when the conditions are favorable then it gives results so although we cannot

155
00:26:37,080 --> 00:26:46,800
say it is impossible to say where exactly comma is stored but when the conditions are

156
00:26:46,800 --> 00:26:53,760
favorable for a good comma to give results then we get good results and if the conditions

157
00:26:53,760 --> 00:27:08,920
are favorable for bad comma to give results we get bad results so this comma I mean

158
00:27:08,920 --> 00:27:19,280
this put the volition when it disappears it leaves something some potential in the continuity

159
00:27:19,280 --> 00:27:32,960
of beings and so when the conditions are favorable we get the results the vulnerable

160
00:27:32,960 --> 00:27:39,840
Nagazena give the similarly of the mango tree right so we do not know it is impossible

161
00:27:39,840 --> 00:27:46,360
to say where mangoes are stored whether in the roots or in the trunks or in the branches

162
00:27:46,360 --> 00:27:54,400
but when it is seasoned and when it gets the moisture temperature and so on and when

163
00:27:54,400 --> 00:28:04,320
the conditions are favorable then the fruits come up or then the tree bears fruits so

164
00:28:04,320 --> 00:28:10,720
in the same way although it is impossible to say where comma is stored when the conditions

165
00:28:10,720 --> 00:28:26,080
are favorable we get the results of comma after understanding what comma is we should

166
00:28:26,080 --> 00:28:35,520
understand what comma is not so comma is not fate of predestination if we take comma

167
00:28:35,520 --> 00:28:46,600
or predestination as something imposed on beings by some mysterious unknown power

168
00:28:46,600 --> 00:29:00,880
by a deity by a god or by a Brahma now whenever we suffer something or whenever we get

169
00:29:00,880 --> 00:29:08,400
something we want we say this is the result of comma or we just say this is my comma

170
00:29:08,400 --> 00:29:21,920
so it is something like fate but if fate of predestination is taken as something imposed

171
00:29:21,920 --> 00:29:34,320
upon us by others then comma is not fate of predestination because comma is something

172
00:29:34,320 --> 00:29:48,120
we did say we did in past lives and then we experience the results of this past comma

173
00:29:48,120 --> 00:30:03,880
in this present life and in future lives now the results of the comma are the natural outcome

174
00:30:03,880 --> 00:30:20,800
of the comma the results of the results good of that and not created by any deity or

175
00:30:20,800 --> 00:30:32,320
a Brahma or a god so the good results we get or the bad results we get are not the rewards

176
00:30:32,320 --> 00:30:42,400
the good results we get are not the rewards given by the deity or the bad results we

177
00:30:42,400 --> 00:30:52,000
we experience and not the punishment given out by the deity or by god or by Brahma it

178
00:30:52,000 --> 00:31:09,880
is our own doing so we get as a result of what we did so comma is not fate of predestination

179
00:31:09,880 --> 00:31:19,520
if we take fate and predestination as something imposed upon us by an external power but

180
00:31:19,520 --> 00:31:27,440
it is like fate and predestination because we enjoy or we suffer according to our own

181
00:31:27,440 --> 00:31:38,560
comma the entire suffer the results of our own comma but as I said the results are not

182
00:31:38,560 --> 00:31:52,840
the creation of anybody but it is the results are the natural outcome of these commas

183
00:31:52,840 --> 00:32:03,800
sometimes people think that comma is like defeat isn't except what is given to you by

184
00:32:03,800 --> 00:32:13,160
comma and you cannot do anything to to improve yourself but that is not correct now by

185
00:32:13,160 --> 00:32:21,240
doing good comma we can get good results even in this life and so we can improve ourselves

186
00:32:21,240 --> 00:32:32,440
by doing good comma so comma is not not defeat isn't not not accepting defeat without

187
00:32:32,440 --> 00:32:42,920
doing anything about it we accept that what we enjoy or what we suffer is the result

188
00:32:42,920 --> 00:32:51,520
of comma but we try to improve our condition by doing a good comma by doing good deeds

189
00:32:51,520 --> 00:33:06,440
and so a comma is not a defeat isn't as some people think it is now the next question

190
00:33:06,440 --> 00:33:20,080
is can we modify the workings of comma or can we interfere with the workings of comma now

191
00:33:20,080 --> 00:33:34,000
even the Buddha himself cannot interfere with the working of comma now when prince

192
00:33:34,000 --> 00:33:49,760
we do double tries to kill Buddha's relatives but I could not do anything because they

193
00:33:49,760 --> 00:33:58,720
are comma was such that and they had to they had to perish the heavy hands of a prince

194
00:33:58,720 --> 00:34:13,840
they do double so Buddha could not had a girl could not get here the fate to be suffered

195
00:34:13,840 --> 00:34:26,000
by his relatives and then that is because the law of comma is a natural law it is not made

196
00:34:26,000 --> 00:34:34,240
by any person it is not created by any god or deity but it is a natural law so since it is

197
00:34:34,240 --> 00:34:49,280
natural law nobody can interfere with the working of the law of comma but although we cannot

198
00:34:49,280 --> 00:35:00,720
interfere with the workings of comma we can do something about the working of comma that is we

199
00:35:00,720 --> 00:35:12,160
can create conditions that are unfavorable for the bed comma to give results and so we will be

200
00:35:12,160 --> 00:35:20,480
able to at least postpone the coming of the dead results of Akusala comma because as I said before

201
00:35:22,240 --> 00:35:31,920
only when the conditions are favorable does comma give results if conditions are not favorable

202
00:35:32,880 --> 00:35:40,400
they may lie and wait they comma will lie and wait so if we create the unfavorable conditions

203
00:35:40,400 --> 00:35:49,200
for Akusala comma bed comma to give results then we may not get the results of the bed comma

204
00:35:50,640 --> 00:35:57,040
so although we cannot interfere with the law of comma we are going to change the law of comma

205
00:35:57,760 --> 00:36:06,800
we are going to do something so that we do not get the bed results of Akusala comma

206
00:36:06,800 --> 00:36:19,200
but that is to some extent only because nobody can wipe out the comma or the results of comma

207
00:36:19,200 --> 00:36:32,400
altogether even the Buddha had to suffer the bed results of his bed comma he did in the past

208
00:36:32,400 --> 00:36:44,000
you know Buddha was a Buddha often suffered from headache or bedache sometimes he suffered from

209
00:36:44,880 --> 00:36:55,920
horrible fever and he suffered from a wound in his foot when his foot was

210
00:36:55,920 --> 00:37:14,480
hit by a rock a small rock which came from the big rock, the water, the first push down on the

211
00:37:14,480 --> 00:37:28,080
Buddha so even the Buddha was not immune from getting the bed results of his bed comma in the

212
00:37:28,080 --> 00:37:38,160
past so there are many stories in our books that show that there is no escape from the

213
00:37:38,160 --> 00:37:53,120
results of bed comma so we cannot interfere with the working of comma we cannot change the course

214
00:37:53,120 --> 00:38:04,320
of comma but we can do something so that we can postpone the coming of bed results in some cases

215
00:38:04,320 --> 00:38:09,840
or to some extent but nobody can wipe the comma altogether

216
00:38:15,600 --> 00:38:26,400
let us say nobody can wipe the the the first comma altogether or the result of first comma

217
00:38:26,400 --> 00:38:36,800
altogether so everybody has to to suffer or experience the result of his comma say in the past

218
00:38:42,400 --> 00:38:49,840
now there is a saying that at the moment of the attainment of a relationship

219
00:38:49,840 --> 00:38:57,680
all commas are exhausted now the the

220
00:39:01,920 --> 00:39:12,560
manga consciousness at the moment of the attainment of the foot stage of enlightenment

221
00:39:12,560 --> 00:39:32,640
is called one that performs the exhaustion of commas so at the moment of the attainment of

222
00:39:32,640 --> 00:39:42,400
at a hand shape it is said that all commas are exhausted now that does not mean that all past commas

223
00:39:42,400 --> 00:39:57,600
are destroyed and so one doesn't have to experience the results of these past commas now what

224
00:39:57,600 --> 00:40:06,800
have been at the moment of the attainment of an a relationship is that all mental defalments

225
00:40:08,080 --> 00:40:20,240
are eradicated at that moment when there are no mental defalments in the mind whatever that person

226
00:40:20,240 --> 00:40:31,120
does it's just doing that does not constitute comma because that person has no no attachment to

227
00:40:31,120 --> 00:40:40,880
any any life or any existence and that person has no ignorance and so whatever that person does

228
00:40:40,880 --> 00:40:48,080
is just doing and it does not constitute comma in other words those who have attained at

229
00:40:48,080 --> 00:41:01,680
hand shape the Buddhas and arhans do not accumulate fresh commas they do not get new commas although

230
00:41:01,680 --> 00:41:10,720
they continue to do good things about their thought beings and Buddha helps many people to get

231
00:41:10,720 --> 00:41:17,680
free from suffering and arhans also do good things to their fellow monks as well as to

232
00:41:17,680 --> 00:41:30,240
lay people but they do not get crucial when they do these and they are comma actions are called

233
00:41:30,240 --> 00:41:46,560
what kivya right just doing just happening so the exhaustion of comma accomplished by the

234
00:41:46,560 --> 00:42:04,480
full stage of enlightenment means that no comma is accumulated after the attainment of the

235
00:42:04,480 --> 00:42:13,920
full stage of enlightenment since there is no comma there will be no results no results in the

236
00:42:13,920 --> 00:42:22,480
future of comma then what about the the old commas the did in the past they still have to

237
00:42:23,760 --> 00:42:30,400
experience the result of those old commas like the Buddha suffering from headache and so on

238
00:42:30,400 --> 00:42:45,600
and also that the arhans suffering from illness and I think you you know then and the vulnerable

239
00:42:45,600 --> 00:43:00,000
Maglana was attacked by 500 robbers and he was he was killed so even though they do not accumulate

240
00:43:00,800 --> 00:43:08,240
new commas they still have the stew of old commas and so they have to suffer or enjoy

241
00:43:08,240 --> 00:43:19,200
and the consequences of and they are comma in the past but after their death there is no more

242
00:43:19,200 --> 00:43:33,040
rebirth they are not reborn anymore so the the commas they accumulated in the past cannot give

243
00:43:33,040 --> 00:43:47,600
results after their death you know the story of Angulimala so Angulimala killed how many people

244
00:43:49,360 --> 00:43:55,440
a thousand people maybe more than a thousand right so he killed many human beings and then

245
00:43:55,440 --> 00:44:02,320
he he Buddha went to him and Buddha thought him and he became first a monk and then later he

246
00:44:02,320 --> 00:44:14,480
became an arhans and as an arhans before he died he had to suffer some consequences of his

247
00:44:14,480 --> 00:44:25,760
bad comma but he did a lot of arkushala comma right if he did not become an arhans these

248
00:44:25,760 --> 00:44:36,720
arkushala cameras would have given him bad results for many many lives but since he he he was not

249
00:44:36,720 --> 00:44:49,120
reborn those arkushala cannot give results so for all for him these arkushala cameras become

250
00:44:49,120 --> 00:45:05,600
deformed it is like it wasn't commit a crime and then he dies so that one then then the police cannot

251
00:45:05,600 --> 00:45:13,120
do anything to to arrest him and to punish him because he is now out of their reach so in the same

252
00:45:13,120 --> 00:45:23,120
way when an arhans dies there is no more rebirth for him so since there is no more rebirth the

253
00:45:23,120 --> 00:45:32,960
past cameras cannot give any any results to him after his death and so they become deformed so

254
00:45:32,960 --> 00:45:45,280
when we say the arhans achieve exhaustion of karma we mean they do not acquire fresh or new

255
00:45:45,280 --> 00:45:57,200
karma but they still experience the the results of their karma they did in the past and many

256
00:45:57,200 --> 00:46:06,800
cameras become de-funked that means they cannot give results when arhans and also Buddha's

257
00:46:08,320 --> 00:46:23,040
passed away a diet because arhans or Buddha's are not they are not reborn or they do not have

258
00:46:23,040 --> 00:46:40,000
any more rebirths now we don't want to catch bad results of bad karma so if we don't want to

259
00:46:40,000 --> 00:46:49,840
get bad results we must not do bad karma but what if we have done bad karma what must we do

260
00:46:52,080 --> 00:47:06,240
regarding the bad karma we did now Buddha said even if even if we were to be remorseful

261
00:47:06,240 --> 00:47:17,680
for the bad karma we did in the past that bad karma will not be undone we have already done

262
00:47:17,680 --> 00:47:27,280
it and so we cannot undo it what is done is already done and so being remorseful makes our situation

263
00:47:27,280 --> 00:47:40,080
worse we accumulate more acusala more unwholesome karma by being regretful about the bad karma

264
00:47:40,080 --> 00:47:48,800
we did in the past so Buddha meant to say forget about it so don't think of that bad karma

265
00:47:48,800 --> 00:48:01,040
again and again and acquire a more acusala karma so forgetting about it is called abandoning

266
00:48:02,320 --> 00:48:11,520
that bad karma and then in the future to refrain from doing that bad karma

267
00:48:11,520 --> 00:48:19,120
so with regard to bad karma the first thing is not to think about it or not to not to

268
00:48:19,120 --> 00:48:26,000
be remorseful about that bad karma and then try to refrain from such bad karma in the future

269
00:48:26,720 --> 00:48:39,760
so that is called abandoning that evil or getting beyond that evil karma so if we have done

270
00:48:39,760 --> 00:48:45,760
something bad some some bad karma in the past then we should not think about it again and again

271
00:48:45,760 --> 00:48:55,360
and be remorseful but we should refrain from doing similar bad karma in the future and that way

272
00:48:55,360 --> 00:49:12,160
say we leave behind and the bad karma now Buddha talked about wrong views about karma

273
00:49:12,160 --> 00:49:25,360
and we must understand these wrong views now one wrong view is whatever a person experiences

274
00:49:26,800 --> 00:49:38,640
the it happiness pain or neutral feeling all that he experiences because of past action or past

275
00:49:38,640 --> 00:49:45,280
karma now that is said to be a wrong view whatever a person experiences

276
00:49:45,280 --> 00:49:52,320
happiness or pain or neutral feeling that he experiences as a result of his past karma

277
00:49:53,280 --> 00:49:58,960
and Buddha said it is a wrong view do you agree with that

278
00:49:58,960 --> 00:50:09,360
I say decay whatever a person experiences happiness or pain or neutral feeling

279
00:50:11,040 --> 00:50:14,560
he experiences as a result of past karma

280
00:50:14,560 --> 00:50:37,440
Isn't it correct so this is one wrong view now in order to understand this we must understand

281
00:50:37,440 --> 00:50:44,240
that feelings of whatever a company the the a company

282
00:50:46,800 --> 00:50:54,880
resultant consciousness the accompanying wholesome and unfulsome consciousness and the

283
00:50:54,880 --> 00:51:08,640
a company Kyria consciousness that arise in and both as an advance with regard to a feeling that

284
00:51:08,640 --> 00:51:21,040
a company's resultant consciousness it is true right because suppose I have pain here so I experience

285
00:51:21,040 --> 00:51:29,520
that pain the experience of that pain is the result of past karma right it is a result in consciousness

286
00:51:30,480 --> 00:51:40,720
so it is true but when I have crucial of mine or a crucial of mine there is still

287
00:51:40,720 --> 00:51:51,680
Somanasa or Domenasa or Ubiqua right that is not not not the result of any karma because it is

288
00:51:51,680 --> 00:52:00,080
itself karma we are we are acquiring new right and also the consciousness that arise in

289
00:52:00,080 --> 00:52:09,520
Buddha's and Arhans so Buddha's and Arhans also experience Somanasa or Domenasa or Doka or

290
00:52:09,520 --> 00:52:18,960
neutral feeling and these feelings or experience of these feelings are also not not the result

291
00:52:18,960 --> 00:52:28,560
of any karma so if you see whatever a present experience is whether it is happiness or pain or

292
00:52:28,560 --> 00:52:41,120
neutral it is because of past karma you are wrong so that is one wrong view about karma

293
00:52:42,400 --> 00:52:50,720
and wrong view here is whatever a person experiences whether it is happiness or pain or neutral

294
00:52:50,720 --> 00:53:00,800
feeling all that he experiences because of the creation of feelings by Isra or by God that means

295
00:53:01,360 --> 00:53:08,640
it doesn't experience as happiness or pain or neutral feelings because those feelings are created by God

296
00:53:09,520 --> 00:53:18,320
so God created happiness for him pain for him neutral feeling for him so if you take that way

297
00:53:18,320 --> 00:53:26,320
that is one another kind of wrong view about karma so this is easy to understand

298
00:53:33,120 --> 00:53:40,880
is that whatever person experiences whether it is happiness or pain or neutral feeling all that

299
00:53:40,880 --> 00:53:48,000
he experiences without a cause and without a condition so there is no cause or condition for

300
00:53:48,000 --> 00:53:55,600
his being happy or painful or being neutral so that is also a wrong understanding I mean a

301
00:53:55,600 --> 00:54:05,280
wrong view about karma now happiness or pain or neutral does not arise without any condition

302
00:54:05,280 --> 00:54:17,120
any one of these is dependent upon some condition without conditions they cannot arise

303
00:54:17,920 --> 00:54:26,320
so when we see happiness or pain or neutral feelings it doesn't experience without cause or

304
00:54:26,320 --> 00:54:34,000
without a condition that is wrong and when we see that happiness or pain or neutral feeling

305
00:54:34,000 --> 00:54:45,120
is created by a God or a Brahma we are also wrong because they are dependent on conditions

306
00:54:45,120 --> 00:54:54,800
and not they are created so the second and the third wrong views about karma are easy to

307
00:54:54,800 --> 00:55:02,080
understand but the first is a little difficult you have to understand a bidham in order to understand

308
00:55:02,080 --> 00:55:17,200
this we are now there is one one passage which is important and but difficult to understand

309
00:55:22,000 --> 00:55:29,520
that is number eight the Buddha said I do not teach the extension of karma

310
00:55:29,520 --> 00:55:39,120
that are will to perform and heat up without their results having been experienced

311
00:55:42,480 --> 00:55:48,320
and these results in this life or in the next life or in the lives after next

312
00:55:50,160 --> 00:55:52,000
I think that part is not so difficult

313
00:55:52,000 --> 00:55:59,360
karma do not come to extinction

314
00:56:04,320 --> 00:56:08,880
without the having been experienced

315
00:56:12,480 --> 00:56:19,120
that is until they give results they are not extinguished

316
00:56:19,120 --> 00:56:27,920
after they give results then they disappear like that so that is not difficult

317
00:56:27,920 --> 00:56:36,320
but the next sentence is I think difficult never do I teach making an end of suffering

318
00:56:36,320 --> 00:56:46,480
without the results of kamas that are world performed and heat up having been experienced

319
00:56:49,760 --> 00:56:58,560
I do not teach making an end of suffering without the results of karma having been experienced

320
00:56:58,560 --> 00:57:05,200
that means you have to experience the result of kamas before you can make an end of suffering

321
00:57:13,920 --> 00:57:20,240
it gives me a lot of a lot of thinking a lot of article

322
00:57:20,240 --> 00:57:32,000
do cut to me I do not teach making an end of suffering without the results of kamas having been

323
00:57:32,000 --> 00:57:38,480
experienced so if you do not have experienced the result of karma you cannot make an end of suffering

324
00:57:38,480 --> 00:57:44,080
so if you want to make an end of suffering you have to you have to experience all the results of your

325
00:57:44,080 --> 00:58:02,720
kamas no then what what is the meaning of this I do not know so I want you to think about it

326
00:58:02,720 --> 00:58:11,200
but not during the retreat so please read it again and again I think about it and we will talk

327
00:58:11,200 --> 00:58:16,640
about it at our next class

