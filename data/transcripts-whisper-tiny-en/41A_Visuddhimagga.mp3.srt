1
00:00:00,000 --> 00:00:12,000
When we practice Vipassana meditation, we try to be mindful of whatever is at the present moment.

2
00:00:12,000 --> 00:00:24,000
And that whatever is sometimes matter or root, and sometimes mind or numb.

3
00:00:24,000 --> 00:00:36,000
Sometimes we are mindful of the breath or the movements.

4
00:00:36,000 --> 00:00:41,000
And so, at that time, we are taking the matter as an object.

5
00:00:41,000 --> 00:00:53,000
And when we are mindful of our thoughts or emotions, we are mindful of the mind or the mental properties.

6
00:00:53,000 --> 00:01:03,000
So, in the beginning, we just try to be mindful of mind or matter whatever is at the present moment.

7
00:01:03,000 --> 00:01:20,000
And after some time, as the concentration gets better, we are able to see the individual characteristics of what we observe.

8
00:01:20,000 --> 00:01:28,000
So, there are two kinds of characteristics we have to understand with regard to Vipassana meditation.

9
00:01:28,000 --> 00:01:33,000
So, the first thing we will see is the individual characteristics of things.

10
00:01:33,000 --> 00:01:41,000
So, when we concentrate on the mind, then we will come to see that the mind is that which inclines towards the object.

11
00:01:41,000 --> 00:01:49,000
So, when we watch our mind or our consciousness, it seems to go to the object, it seems to incline towards the object.

12
00:01:49,000 --> 00:01:58,000
And that's inclination towards the object is the characteristic of mind.

13
00:01:58,000 --> 00:02:04,000
And when we concentrate on matter, then we will come to see that matter has no cognizing power.

14
00:02:04,000 --> 00:02:06,000
It doesn't recognize.

15
00:02:06,000 --> 00:02:08,000
So, that also we will come to see.

16
00:02:08,000 --> 00:02:15,000
And that is the characteristic of matter.

17
00:02:15,000 --> 00:02:29,000
So, during these stages, we have studied and during these stages of designing mind and matter.

18
00:02:29,000 --> 00:02:40,000
And during this stage of designing the causes of mind and matter, the EOE,

19
00:02:40,000 --> 00:02:46,000
the specific of the individual characteristics of things.

20
00:02:46,000 --> 00:03:03,000
Now, from that stage, in order to go to the next stage, EOE needs to see the general characteristics of common characteristic of things.

21
00:03:03,000 --> 00:03:11,000
Common characteristic means the intermanence, suffering and soullessness of things.

22
00:03:11,000 --> 00:03:23,000
Because the intermanence and others are common to all phenomena or mind and matter or formations we call it.

23
00:03:23,000 --> 00:03:40,000
So, in order to actually, during these stages, during this stage of seeing the causes, EOE comes to see the arising and disappearing of what he observed.

24
00:03:40,000 --> 00:03:47,000
So, when he observed his anger, then he knows that it has arisen.

25
00:03:47,000 --> 00:03:51,000
And also, when it disappears, he knows it disappears.

26
00:03:51,000 --> 00:03:54,000
So, arising and falling.

27
00:03:54,000 --> 00:03:58,000
And that is direct seeing.

28
00:03:58,000 --> 00:04:08,000
So, from the direct seeing, he infers the intermanence and so on of those he does not observe.

29
00:04:08,000 --> 00:04:11,000
That is, those of the past and those of the future.

30
00:04:11,000 --> 00:04:23,000
So, this chapter, chapter 20, deals with that kind of, we pass on that kind of insight meditation.

31
00:04:23,000 --> 00:04:35,000
And in the beginning of that chapter, it is said that one who desires to accomplish this field first of all of blood himself to the inductive insight.

32
00:04:35,000 --> 00:04:41,000
So, it is called inductive insight. Actually, it is influential. It is not direct insight.

33
00:04:41,000 --> 00:04:46,000
And influential insight can come only after the right insight.

34
00:04:46,000 --> 00:04:51,000
So, if we do not have direct insight, there can be no influential insight.

35
00:04:51,000 --> 00:04:54,000
First, we must see the present thing as impermanent.

36
00:04:54,000 --> 00:05:01,000
And then we infer that just as this present, as say this present, matter is impermanent.

37
00:05:01,000 --> 00:05:09,000
So, the first matter must be impermanent and also the future matter will be impermanent.

38
00:05:09,000 --> 00:05:19,000
So, from what we have seen directly, we infer what we do not see as impermanent suffering and so on.

39
00:05:19,000 --> 00:05:30,000
So, in order to develop that insight here, it is said that we must apply to the

40
00:05:30,000 --> 00:05:34,000
inductive insight called comprehension by groups.

41
00:05:34,000 --> 00:05:48,000
So, the beginning part of this chapter deals with this insight, comprehension by groups or inductive insight.

42
00:05:48,000 --> 00:06:06,000
And with regard to this insight, all the mentions, the three kinds of full understanding, the full understanding, as the known, full understanding,

43
00:06:06,000 --> 00:06:15,000
as investigating and full understanding as abandoning. Actually, we have consumed these parts.

44
00:06:15,000 --> 00:06:27,000
So, the full understanding, as the known, is when we see the individual characteristics of things.

45
00:06:27,000 --> 00:06:36,000
So, now, a yogi is to go into the second stage, which is full understanding as investigating.

46
00:06:36,000 --> 00:06:47,000
So, how does a yogi practice insight comprehension by group?

47
00:06:47,000 --> 00:07:04,000
Now, whenever matter or something is mentioned, both the mentions are described as of first future present, internal external groups,

48
00:07:04,000 --> 00:07:08,000
subtle and so on. So, there are 11 of those things.

49
00:07:08,000 --> 00:07:12,000
Those are given in paragraph 6.

50
00:07:12,000 --> 00:07:23,000
Any material in whatever, but the first future present, three, internal external, five, grows subtle, seven.

51
00:07:23,000 --> 00:07:41,000
But over four years, superior, nine, four, near 11. So, this, with regard to these 11 aspects, material, the D is described, and different kinds of material, the D is described.

52
00:07:41,000 --> 00:07:52,200
Now, when a yogi does comprehending by group, he does not take each one of the material

53
00:07:52,200 --> 00:08:01,200
probably separately, but just the materiality as a group as one, and then materiality which

54
00:08:01,200 --> 00:08:10,600
belongs to the first is impermanent, and materiality which belongs to the future is

55
00:08:10,600 --> 00:08:12,200
impermanent and so on.

56
00:08:12,200 --> 00:08:24,200
So, when he takes matter as a whole and not as not taking as an element, what an element

57
00:08:24,200 --> 00:08:31,400
and so on separately, and he is said to be doing the comprehend comprehension by groups.

58
00:08:31,400 --> 00:08:39,400
Actually, group means just as taking your ruba, I mean if it is ruba taking ruba as a whole.

59
00:08:39,400 --> 00:08:47,800
So, if a person can practice this meditation or ruba as, all ruba is impermanent, that is

60
00:08:47,800 --> 00:08:57,400
one kind of comprehension or he can take, he can take ruba with reference to the 11 aspects.

61
00:08:57,400 --> 00:09:05,000
So, ruba in the first is impermanent, ruba is impermanent, ruba is impermanent, ruba is impermanent,

62
00:09:05,000 --> 00:09:10,600
and then the internal ruba is impermanent, external ruba is impermanent and so on.

63
00:09:10,600 --> 00:09:14,600
If he does that way, then he is said to be doing in 11 different ways.

64
00:09:14,600 --> 00:09:19,400
So, there can be one comprehension or there can be 11 comprehension.

65
00:09:19,400 --> 00:09:26,200
So, we are described in the following paragraphs.

66
00:09:26,200 --> 00:09:39,800
Now, if you look at paragraph 13, it is said there, here is the application of directions

67
00:09:39,800 --> 00:09:44,400
dealing with the aggregates, any materiality, whatever, whether it's past future or present

68
00:09:44,400 --> 00:09:49,200
internal or external groups or subtle inferior or superior for or near.

69
00:09:49,200 --> 00:09:55,800
He defines all materiality as impermanent, this is one kind of comprehension.

70
00:09:55,800 --> 00:10:07,600
And then, one word is missing there or it is stated for that and it breathes an elliptical

71
00:10:07,600 --> 00:10:08,600
way.

72
00:10:08,600 --> 00:10:16,400
So, you see only two there, right, impermanent and not-self.

73
00:10:16,400 --> 00:10:18,200
What is missing?

74
00:10:18,200 --> 00:10:21,800
Dukha, I mean suffering is missing.

75
00:10:21,800 --> 00:10:31,000
So, there must be something like painful, he defines as painful and then thoughts, he defines

76
00:10:31,000 --> 00:10:36,600
as not-self, this is one kind of comprehension.

77
00:10:36,600 --> 00:10:45,800
So, this is taking ruba as a whole and then also taking ruba as a whole but we reference

78
00:10:45,800 --> 00:10:52,800
with ruba to first and so on, we get 11 kinds of contemplation.

79
00:10:52,800 --> 00:11:02,800
So, it was the end of the paragraph 14, it is said that, and all this is impermanent in

80
00:11:02,800 --> 00:11:04,200
the sense of this fraction.

81
00:11:04,200 --> 00:11:12,200
Accordingly, there is one kind of comprehension in this way but it is affected in 11 ways.

82
00:11:12,200 --> 00:11:17,600
So, it can be only one kind of contemplation or 11 kinds of contemplation.

83
00:11:17,600 --> 00:11:25,000
If we take ruba as a whole, then it is one kind of contemplation.

84
00:11:25,000 --> 00:11:34,280
If we take ruba with reverence to first, future present and so on, then we get 11 ways

85
00:11:34,280 --> 00:11:42,280
of contemplating on meta or materiality.

86
00:11:42,280 --> 00:11:49,480
Just as there is one or 11 contemplations with regard to impermanence, there are one or

87
00:11:49,480 --> 00:12:00,280
11 with regard to suffering or painful, paragraph 15 and the same with another.

88
00:12:00,280 --> 00:12:17,280
There are no cells in paragraph 16, so one contemplation or 11 contemplation.

89
00:12:17,280 --> 00:12:26,880
Now, the word another, in paragraph 16, we have one meaning of the word another and that

90
00:12:26,880 --> 00:12:35,280
is what, in the sense of having no core, so this is one meaning of another.

91
00:12:35,280 --> 00:12:42,880
The word another is defined as meaning, it might have different meanings.

92
00:12:42,880 --> 00:12:49,680
So, one meaning is that there is no core, that is why it is called another.

93
00:12:49,680 --> 00:12:57,480
So, when you see, ruba is another, we mean a ruba has no inner core or substance or whatever.

94
00:12:57,480 --> 00:13:06,080
So, this is one meaning or one interpretation of the word another, we will meet another

95
00:13:06,080 --> 00:13:15,560
interpretation later.

96
00:13:15,560 --> 00:13:26,800
And after doing this and also with regard to the other aggregates, feeling and so on, the

97
00:13:26,800 --> 00:13:39,600
yogi is instructed to view, mine and meta in 40 different ways, that is paragraph 18 and

98
00:13:39,600 --> 00:13:40,600
so on.

99
00:13:40,600 --> 00:13:47,000
There is some meaning of comprehension of impermanence, etc., in 40 ways.

100
00:13:47,000 --> 00:13:59,400
So, they are given in brief and also its term is defined, we are in paragraph 19.

101
00:13:59,400 --> 00:14:12,600
So, these begin with the word, the word in parlids, anachata, do carto, groganto, kangatto and

102
00:14:12,600 --> 00:14:13,600
so on.

103
00:14:13,600 --> 00:14:20,720
So, in behma, these are called 40 toes, you know.

104
00:14:20,720 --> 00:14:24,160
The word tor, tor in blemish means forest.

105
00:14:24,160 --> 00:14:31,160
So, it is something like a play of words, the word tor really is the body particle, it

106
00:14:31,160 --> 00:14:39,960
is suffix, which means s or fro or because of, but here s.

107
00:14:39,960 --> 00:14:45,600
So, the body water is taken to mean something like a half forest here and so we have 40

108
00:14:45,600 --> 00:14:57,760
toes. So, 40 ways of looking at mine and meta and these 40 ways are later divided into

109
00:14:57,760 --> 00:15:05,400
meaning impermanence, pain, painfulness and no self.

110
00:15:05,400 --> 00:15:18,560
So, as I said last week, we have to add, we have to make changes somewhere in paragraph

111
00:15:18,560 --> 00:15:29,240
19 at the bottom of paragraph 19 and if we come up to three lines, we see adversity

112
00:15:29,240 --> 00:15:37,600
and because of being. So, the word end should be rubbed out and then s terror should

113
00:15:37,600 --> 00:15:48,480
be put there. So, s terror because of being the basis for all kinds of terror and on

114
00:15:48,480 --> 00:15:59,520
page 712, about 15 lines down, what range should be in Italy, that is not not so important

115
00:15:59,520 --> 00:16:06,840
but it should be in Italy. So, we get all the other 40 different ways of contemplating

116
00:16:06,840 --> 00:16:18,400
on nama and ruba actually and in paragraph 20, these are grouped with regard to

117
00:16:18,400 --> 00:16:29,200
impermanence, suffering and not self. Now, there are 50 kinds of contemplation of impermanence

118
00:16:29,200 --> 00:16:34,800
here by taking the following thing in the case of each aggregate as impermanence, as

119
00:16:34,800 --> 00:16:42,960
a disintegrating as figgle and so on. There are 25 kinds of contemplation of not self

120
00:16:42,960 --> 00:16:48,000
and then there are 25 kinds of contemplation on pain by taking the rest beginning with

121
00:16:48,000 --> 00:16:58,000
the page 4 as a disease and so on. So, these 40 are divided into three groups, the impermanence,

122
00:16:58,000 --> 00:17:08,280
pain and not self. So, after viewing man and man and this way, the yogi is instructed

123
00:17:08,280 --> 00:17:17,960
to sharpen his faculties, that is, if he cannot bring this contemplation to success

124
00:17:17,960 --> 00:17:24,960
if he does not succeed in contemplating this, in this way, then he should sharpen his faculties

125
00:17:24,960 --> 00:17:35,880
by the way stated in paragraph 21. That is, one, this is only the destruction of

126
00:17:35,880 --> 00:17:42,440
arisen formations and in that occupation he makes sure of working carefully and so on.

127
00:17:42,440 --> 00:17:51,400
So, he has to sharpen his faculties by these methods and there number 8, wherein he

128
00:17:51,400 --> 00:18:01,080
overcomes pain by renunciation. What does that mean? Overcomes pain by renunciation.

129
00:18:01,080 --> 00:18:11,320
Here renunciation simply means effort. The value of what is sometimes misleading.

130
00:18:11,320 --> 00:18:18,800
They come, they come, means to get out of. So, we are to get out of laziness. So, that

131
00:18:18,800 --> 00:18:27,760
means a video or effort. So, to overcome pain by renunciation really means overcome pain

132
00:18:27,760 --> 00:18:45,200
by effort, making effort. So, he should avoid these 7 unsuitable things in the way stated

133
00:18:45,200 --> 00:18:52,240
in the description of Akasina, where they are explained before, can cultivate these 7 suitable

134
00:18:52,240 --> 00:18:58,520
things and he should comprehend the material at one time and the immaterial at another.

135
00:18:58,520 --> 00:19:05,080
So, at one time, he should concentrate, contemplate on Ruba, Meda, and at another time on

136
00:19:05,080 --> 00:19:13,660
Nama or immaterial things. Now, contemplation of the material, while comprehending

137
00:19:13,660 --> 00:19:21,360
materiality, he should see how materiality is generated. So, how it arises, how it is

138
00:19:21,360 --> 00:19:29,640
arising, each generation. That should be the object of his meditation at that time.

139
00:19:29,640 --> 00:19:35,320
So, that is to see how this materiality is generated by the four causes, beginning with

140
00:19:35,320 --> 00:19:43,240
karma. So, there are four causes of Meda, I think you have met them before, for a

141
00:19:43,240 --> 00:19:57,960
19, I mean, chapter 19, paragraph 9. Those are what? Karma, consciousness, temperature

142
00:19:57,960 --> 00:20:06,640
and nutrition. So, one materiality is being generated in any beginning, it is first generated

143
00:20:06,640 --> 00:20:17,320
from karma. So, the first method that arises at the beginning of a given life is generated

144
00:20:17,320 --> 00:20:24,080
by karma or is it result of karma. For at the actual moment of rebut linking of a child

145
00:20:24,080 --> 00:20:31,760
in the womb, first, 30 instances of materiality, that means 30 material properties are generated

146
00:20:31,760 --> 00:20:36,760
in the triple continuity. In other words, the decades of physical heart disease is body

147
00:20:36,760 --> 00:20:44,880
and sex. So, the decade of physical heart disease, the decade of body and the decade of sex.

148
00:20:44,880 --> 00:20:50,040
And those are generated at the actual instant of the rebut-leading consciousness as

149
00:20:50,040 --> 00:20:55,960
arising. And as at the instant of its arising, so too at the instant of its presence

150
00:20:55,960 --> 00:21:04,240
and at the instant of its dissolution, not. There are material properties generated by

151
00:21:04,240 --> 00:21:14,320
karma. And these material properties generated by karma arise at the very first moment

152
00:21:14,320 --> 00:21:26,520
in one's life. And each moment has three sub-moments. So, at every sub-moment also, the

153
00:21:26,520 --> 00:21:34,800
matter generated by karma arises. So, at every moment, they become upon whether arises,

154
00:21:34,800 --> 00:21:50,120
all through the life until very close to the moment of death. So, at all three instances,

155
00:21:50,120 --> 00:21:57,880
I mean three sub-moments, the karma-bombent matter is generated. So, at the first moment,

156
00:21:57,880 --> 00:22:04,880
at the arising moment, let's say, how many thirty, right? So, at the arising moment, there

157
00:22:04,880 --> 00:22:15,720
are thirty, right? And these thirty will exist for seventeen third moments. So, now, at

158
00:22:15,720 --> 00:22:24,560
the moment of sub-moment of presence or static moment, another thirty are produced. So,

159
00:22:24,560 --> 00:22:32,000
at that moment, there are sixty. The first thirty, and then, the new thirty. So, there

160
00:22:32,000 --> 00:22:43,600
are sixty. At the dissolution moment of relinking consciousness, another thirty are produced.

161
00:22:43,600 --> 00:22:49,160
And so, at that moment, there are nineteen. And then, at the next moment, there are one

162
00:22:49,160 --> 00:22:55,080
hundred and twenty and so on. So, until it goes to the seventeen third moments. After seventeen

163
00:22:55,080 --> 00:23:00,880
third moments, the number will be constant, because three produce and three disappear

164
00:23:00,880 --> 00:23:09,480
links, something like that. But that, I mean, thirty produce and that is disappear. Here

165
00:23:09,480 --> 00:23:15,960
and the suggestion of materiality is slow and it's transformation ponderous. We should

166
00:23:15,960 --> 00:23:24,160
note this. This means that materiality or matter is slow, slow in the solution. That

167
00:23:24,160 --> 00:23:35,160
means, matter, the life of matter is longer than the life of consciousness.

168
00:23:35,160 --> 00:23:49,240
It exists seventeen times that of the third moment. So, when matter arises, it will exist

169
00:23:49,240 --> 00:23:58,760
for, it will last for seventeen third moments, before it disappears. So, materialization

170
00:23:58,760 --> 00:24:06,240
of materiality is slow. That means, it has a longer like than a third moment. And it's

171
00:24:06,240 --> 00:24:10,600
transformation ponderous, while the suggestion of consciousness is swift and it's transformation

172
00:24:10,600 --> 00:24:17,560
quick. But the arising and disappearing of third moments are quick. Just one moment or say

173
00:24:17,560 --> 00:24:24,160
one big moment or three small moments. And it is said, because I see no other one thing

174
00:24:24,160 --> 00:24:29,040
that is so quickly transformed as the mind. That is the word of the border. For the life

175
00:24:29,040 --> 00:24:34,280
continuum consciousness arises and sees it sixteen times, while one material instance

176
00:24:34,280 --> 00:24:44,920
than yours. Now, those that are produced at the first moment of life relinking will last

177
00:24:44,920 --> 00:24:54,800
until the seventeen third moments. So, while sixteen third moments come and go, they still

178
00:24:54,800 --> 00:25:04,920
exist. With consciousness, the instant of writing, instant of presence, that in state

179
00:25:04,920 --> 00:25:15,200
existence, and instant of dissolution are equal, they are same. So, I said that the life of

180
00:25:15,200 --> 00:25:28,160
a material property is seventeen third moments. So, how many small moments, fifty-one

181
00:25:28,160 --> 00:25:38,600
small moments? Seventeen big moments, fifty-one small moments. Now, the instant of

182
00:25:38,600 --> 00:25:49,280
arising, the instant of presence, then the instant of dissolution. With regard to consciousness,

183
00:25:49,280 --> 00:25:59,720
these three instances are of equal to duration, equal to the same. But with regard to material

184
00:25:59,720 --> 00:26:08,240
property, the instant of arising is one small moment or one sub-moment, the instant

185
00:26:08,240 --> 00:26:19,040
of dissolution, one sub-moment. And the fourteen-nine third moments in between are one

186
00:26:19,040 --> 00:26:36,040
instance of one instant of presence for material property. You follow? Matili, let me say

187
00:26:36,040 --> 00:26:45,760
that, metal lasts for fifty-one small moments, fifty-one small moments of consciousness.

188
00:26:45,760 --> 00:26:57,440
So, the arising, the presence and the dissolution of metal is different from the arising

189
00:26:57,440 --> 00:27:06,840
presence and dissolution of consciousness. Now, the presence of, I mean, the arising

190
00:27:06,840 --> 00:27:15,920
and the dissolution of both men and men are equal, just one sub-moment. But the presence

191
00:27:15,920 --> 00:27:27,000
of consciousness is only one sub-moment. But the presence of metal is forty-nine sub-moment.

192
00:27:27,000 --> 00:27:31,480
So with consciousness, the instant of arising, instant of presence and instant of dissolution

193
00:27:31,480 --> 00:27:36,840
are equal. But with materiality, only the instant of arising and dissolution are quick,

194
00:27:36,840 --> 00:27:42,280
like those of consciousness, while the instant of each presence is long and last, while

195
00:27:42,280 --> 00:27:52,080
sixteen consciousness arise and sees. That means, after it's arising, it's sixteen consciousness.

196
00:27:52,080 --> 00:28:02,000
Now, but I have twenty-five, that is important. The second life continuum. But the other

197
00:28:02,000 --> 00:28:14,960
has not, not mentioned the first life continuum, right? If you, if you read it before, before

198
00:28:14,960 --> 00:28:24,720
you understand like that. Now, the, the, the actual meaning here is, the life continuum

199
00:28:24,720 --> 00:28:33,320
S is second consciousness arises. Now, the, the relating consciousness is the first consciousness.

200
00:28:33,320 --> 00:28:41,520
And then, life continuum is the second consciousness. So here, the translation should not

201
00:28:41,520 --> 00:28:49,880
be second life continuum. But the life continuum S is second consciousness arises. The

202
00:28:49,880 --> 00:28:55,520
party words, you, you, you, you have familiar with duty and be, that, you know, be right.

203
00:28:55,520 --> 00:28:59,800
But when you, when you take refuge, you say, duty and we both dance at an uncle, chameleon,

204
00:28:59,800 --> 00:29:05,640
so on. So from the word duty and be, if you take out P, you get duty. Now, that word

205
00:29:05,640 --> 00:29:15,360
can be an adjective or an adverb. Here, it is an adverb, not an adjective. So in the

206
00:29:15,360 --> 00:29:21,040
party, it is a duty and be one gun, be one gun means life continuum. So duty and be one gun.

207
00:29:21,040 --> 00:29:31,520
That does not mean the second be one gun. But S is second consciousness, be one gun arises.

208
00:29:31,520 --> 00:29:36,760
So here also, the life continuum S, second consciousness arises with the pre-niece and physical

209
00:29:36,760 --> 00:29:43,680
heartuses at its support. And which has already reached presence and arose at the rebirth

210
00:29:43,680 --> 00:29:57,680
of meaning consciousness as instant of rising. That also is not, not accurate. He put the,

211
00:29:57,680 --> 00:30:11,680
what is to be, what is to be at the end? He put it in front. Now, here, it should be,

212
00:30:11,680 --> 00:30:19,440
the translation should run like this. The life continuum as a second consciousness arises with

213
00:30:19,440 --> 00:30:26,880
the pre-niece and physical heartuses at its support. Which arose at the rebirth,

214
00:30:26,880 --> 00:30:33,280
meaning consciousness as instant of arising and has already reached presence.

215
00:30:33,280 --> 00:30:40,800
The sequence should be like that. We cannot change that sequence here. Because it must

216
00:30:40,800 --> 00:30:52,880
pass a rise. And then it must reach the instant of presence. So that means when life continuum arises,

217
00:30:52,880 --> 00:31:07,360
the heart base which arose at the moment of the linking has reached the instant of presence.

218
00:31:07,360 --> 00:31:17,760
Now, here is the linking consciousness one and two life continuum consciousness.

219
00:31:17,760 --> 00:31:30,240
So, the heart base rose with life-relinking consciousness. And so at the moment of the first moment,

220
00:31:30,240 --> 00:31:37,120
it is rising. But later on, it has reached presence or the static stage. So, when it is in its

221
00:31:37,120 --> 00:31:43,920
static stage, the second, I mean, the second consciousness or the bawanga arises and the dead bawanga

222
00:31:43,920 --> 00:31:55,920
arises depending upon this heart base. That is what is, what is, what the oldest is talking here.

223
00:31:57,280 --> 00:32:03,120
So, the life continuum arises with the pre-niece and physical heart disease at its support.

224
00:32:04,080 --> 00:32:09,200
That heart base is already risen and now it has reached the stage of presence.

225
00:32:09,200 --> 00:32:22,160
And at that moment, the life continuum arises. And so, it arises taking the heart base at its support.

226
00:32:22,160 --> 00:32:29,680
So, the brain-niece and physical heart disease at its support, which rose at the rebutling

227
00:32:29,680 --> 00:32:35,680
in consciousness as instant of arising and has already reached presence.

228
00:32:35,680 --> 00:32:44,240
Now, the dead life continuum to say, the life continuum has a third consciousness arises.

229
00:32:46,880 --> 00:32:55,760
Now, for your information, after rebinging consciousness, there are how many

230
00:32:55,760 --> 00:33:07,520
arising of life continuum, 16. So, there are 16 life continuum consciousness arising after

231
00:33:07,520 --> 00:33:15,200
rebinging consciousness. So, here, that means not that third life continuum because it is really

232
00:33:15,200 --> 00:33:21,360
the second continuum. So, the life continuum as the third consciousness arises with the

233
00:33:21,360 --> 00:33:27,920
brain-niece and physical disease at its support and so on. So, it goes this way, that way.

234
00:33:30,640 --> 00:33:40,560
With the arising movement of first life continuum, there arises heart basis.

235
00:33:42,240 --> 00:33:50,240
But when the second life continuum arises, that heart basis has reached its presence stage.

236
00:33:50,240 --> 00:34:04,160
And then, it forms a basis for the second life continuum. And like this, the later consciousness

237
00:34:05,840 --> 00:34:14,160
taking support as the heart basis which arose at this moment before it arrived.

238
00:34:14,160 --> 00:34:23,120
So, if you say put the numbers 1, 2, 3 and 4, that is the number 2 consciousness arises

239
00:34:24,320 --> 00:34:31,040
taking heart basis which arose with number 1 consciousness as its support.

240
00:34:31,600 --> 00:34:37,440
And then, number 3 consciousness arises taking the heart basis which arose with the second

241
00:34:37,440 --> 00:34:51,280
consciousness as its support and so on and so on. So, it can go all through our lives. So,

242
00:34:51,280 --> 00:34:57,680
it will happen in this way throughout life. But in one who is freezing death, 16 consciousness

243
00:34:57,680 --> 00:35:03,840
arises with the single brain-niece and physical heart basis as the support which has already reached

244
00:35:03,840 --> 00:35:14,960
presence. Now, when a person is about to die, the all-kama-born mother sees to arise with the 16th

245
00:35:16,240 --> 00:35:27,200
or maybe 17th, 17th consciousness, recon backward from the dead consciousness.

246
00:35:27,200 --> 00:35:33,920
Suppose, here is dead consciousness. Then, we recon backward taking dead consciousness as 1, 2, 3,

247
00:35:33,920 --> 00:35:45,040
4, 1, 17. So, with the dissolution of dead consciousness, no more kama-born

248
00:35:45,040 --> 00:35:54,560
matter produced in that life. But what is produced at that moment will last until the moment of death?

249
00:35:54,560 --> 00:36:02,480
So, there is only one heart base at the time for these 16 thought moments.

250
00:36:06,320 --> 00:36:12,160
So, but in one who is freezing death, 16 consciousness arises with the single brain-niece and

251
00:36:12,160 --> 00:36:16,160
physical heart basis as the support which has already reached presence.

252
00:36:16,160 --> 00:36:32,400
These are all available. And the materiality that arose at the instant of arising of the

253
00:36:32,400 --> 00:36:37,120
rebutling in consciousness ceases along with the 16 consciousness after the rebutling

254
00:36:37,120 --> 00:36:47,600
consciousness and so on. So, if you understand that material property last for 17 thought moments,

255
00:36:47,600 --> 00:36:55,600
then you can understand this. Now, next is kama-born materiality. These are all beat-a-ma.

256
00:36:58,880 --> 00:37:05,040
With regard to kama-born materiality, then we are going to understand how many, six, right?

257
00:37:05,040 --> 00:37:12,160
kama and what is originated by kama? What has kama as its condition? What is originated by

258
00:37:12,160 --> 00:37:18,560
consciousness? That has kama as its condition. What is originated by a human that has kama as its

259
00:37:18,560 --> 00:37:26,400
condition? And what is originated by a temperature that has kama as its condition? So, there are six,

260
00:37:26,400 --> 00:37:34,720
six comes to be understood with regard to kama-born material. Now, here kama is profitable

261
00:37:34,720 --> 00:37:41,840
and unprofitable volition. Now, that is very, very important. So, if we talk about kama,

262
00:37:41,840 --> 00:37:50,080
we often say kama is indeed a good deed or action. Actions are all kama. But technically speaking,

263
00:37:50,080 --> 00:37:57,600
kama means volition. Profitable and unprofitable of wholesome and wholesome volition,

264
00:37:57,600 --> 00:38:05,120
hustle and acusolatitinine party. And what is originated by kama is a kama result in advocates

265
00:38:05,120 --> 00:38:11,360
and the 70 instances of materiality beginning with the i-jaka and so on. You have to take it

266
00:38:12,000 --> 00:38:19,520
from a middle one. And then what has kama as its condition is the same as the last. Since kama

267
00:38:19,520 --> 00:38:25,520
is the condition that affirms what is originated by kama. And what is originated by consciousness?

268
00:38:25,520 --> 00:38:32,480
That has kama as its condition. His materiality originated by kama result in consciousness.

269
00:38:33,440 --> 00:38:40,560
Consciousness produces matter, right? So, the matter produced by

270
00:38:41,520 --> 00:38:48,800
result in consciousness is called what is originated by consciousness. That has kama as its condition.

271
00:38:48,800 --> 00:38:53,440
And then what is originated by a new treatment that has kama as its condition is so called,

272
00:38:53,440 --> 00:38:58,000
since the new treatment essence that has reached presence in the instances of materiality

273
00:38:58,880 --> 00:39:05,600
originated by kama originates a further octave with new treatment essence as eight. That means

274
00:39:05,600 --> 00:39:11,120
it's inseparable eight. And the new treatment essence there that has reached pretty presence

275
00:39:11,120 --> 00:39:16,240
also originates a further one and so on. So, it links a four or five occurrences of optics.

276
00:39:16,240 --> 00:39:23,360
What is originated by temperature? That has kama as its condition is called. It is so called

277
00:39:23,360 --> 00:39:28,800
since the kama bonfire element that has reached presence originates and opted with new treatment essence

278
00:39:28,800 --> 00:39:38,000
as eight and so on. So, with regard to kama bon materiality we have to understand

279
00:39:38,000 --> 00:39:46,160
six things. And then with regard to consciousness bon materiality, how many? Five. Consciousness,

280
00:39:46,160 --> 00:39:50,720
what is originated by consciousness? What is consciousness? It is condition. What is originated

281
00:39:50,720 --> 00:39:56,720
by new treatment that has consciousness? It is a condition. And what is originated by temperature?

282
00:39:56,720 --> 00:40:02,240
That has consciousness as its condition. Here in consciousness is the 89 kinds of consciousness.

283
00:40:02,240 --> 00:40:09,760
So, you take them from the TV chat at the end of the book of a manual of a beta one.

284
00:40:10,800 --> 00:40:15,920
So, consciousness 22 and 26 and 19 to a record to give but you may have done so on.

285
00:40:18,000 --> 00:40:27,120
Now, among them there are some consciousnesses which produce not only matter but

286
00:40:27,120 --> 00:40:32,720
postures and intimation and some just posture and so on. So,

287
00:40:37,120 --> 00:40:45,040
some types of consciousness can help us to maintain a posture for a long time.

288
00:40:46,480 --> 00:40:54,720
That is why when you have a good concentration and you can sit for two or three hours without

289
00:40:54,720 --> 00:41:01,280
feeling any discomfort, without feeling any weakness or without feeling sleepy or whatever.

290
00:41:01,920 --> 00:41:07,120
So, that happens to yogis when they reach to be higher stages of

291
00:41:07,120 --> 00:41:12,560
weapons and meditation. So, their meditation is very good and their concentration is strong

292
00:41:13,280 --> 00:41:20,080
and so they can sit for a long time. Sometimes they don't sleep at all for one or two days

293
00:41:20,080 --> 00:41:29,520
but they don't feel any effects of lack of sleep or whatever. So, that is because

294
00:41:30,720 --> 00:41:40,720
some types of consciousness have the ability to maintain the posture to keep

295
00:41:41,600 --> 00:41:49,280
our bodies in one posture for a long time and that is why when it wasn't in trance

296
00:41:49,280 --> 00:41:58,640
and we call it Samapadi and Jara that he could sit for seven days without changing his posture.

297
00:41:59,520 --> 00:42:06,880
So, there are some types of consciousness which helps to maintain postures and then there are two

298
00:42:06,880 --> 00:42:19,920
clues. The first group, right? The D2 consciousness namely the A profitable consciousness,

299
00:42:19,920 --> 00:42:25,680
the 12th I'm profitable, the 10th functional excluding the mind element and the two direct knowledge

300
00:42:25,680 --> 00:42:31,680
consciousnesses are profitable and functional, A price to materiality to postures and to

301
00:42:31,680 --> 00:42:39,760
animation three kinds and that the next 26 and they are mmm Javanagh moments of

302
00:42:41,200 --> 00:42:47,360
Rupa, Jara, Arupa, Javanagh and Vokodra. So, they give rise to materiality to post them but not

303
00:42:47,360 --> 00:42:55,920
to animation and so on. Now, the next paragraph, what is originated by consciousness is the

304
00:42:55,920 --> 00:43:05,600
three other material epigates, that is, feeling, perception and mental formation and 17 full materiality

305
00:43:05,600 --> 00:43:12,320
namely the sound and that bodily contamination, flower, animation and so on. Now, the students

306
00:43:12,320 --> 00:43:18,560
of Arbirama, do you find any discrepancy here with the manual of Arbirama?

307
00:43:18,560 --> 00:43:31,360
In the manual of Arbirama only 15 are said to be born of consciousness, right? Here they are given

308
00:43:31,360 --> 00:43:39,520
as 17. So, growth and continuity are not included in those originated by consciousness in the

309
00:43:39,520 --> 00:43:49,760
manual of Arbirama, but here they are also taken to be originated by consciousness.

310
00:43:51,360 --> 00:43:54,240
These two will be added to other groups too.

311
00:43:59,120 --> 00:44:03,440
And what does consciousness as its condition is the materiality of full full

312
00:44:03,440 --> 00:44:09,360
origination stated thus, whose nascent states of consciousness and consciousness complements are

313
00:44:09,360 --> 00:44:16,160
in condition, as whose nascent condition for its present or every nascent body, which is from

314
00:44:16,160 --> 00:44:22,240
Patana. And what is originated by a new treatment that is consciousness as its condition,

315
00:44:22,240 --> 00:44:27,120
the new treatment essence that is reached pre-nacence in consciousness originated material

316
00:44:27,120 --> 00:44:33,040
instances, originated father opted with new treatment essence as A and thus links up two or three

317
00:44:33,040 --> 00:44:46,720
or currents and so forth days. These are all of Birama. If you want to understand them,

318
00:44:47,520 --> 00:44:56,880
please read chapter 6 of the manual of Arbirama. So, after chapter 6 you come back here and then

319
00:44:56,880 --> 00:45:05,840
you understand. New treatment called materiality, the same, say, new treatment, what is

320
00:45:05,840 --> 00:45:13,040
originated by new treatment and so on. Here also a paragraph that is 6, what is originated

321
00:45:13,040 --> 00:45:19,760
by new treatment? There are 14 material properties mentioned here, but in the manual of

322
00:45:19,760 --> 00:45:28,880
Birama only 12 are mentioned without growth and continuity. And then in paragraph 37, one thing is

323
00:45:30,560 --> 00:45:36,240
worthy of notice and that is, new treatment is made on the body originated materiality.

324
00:45:36,240 --> 00:45:51,360
Now, normally we think that only food, eaten, eaten through month can originate materiality,

325
00:45:51,840 --> 00:45:56,640
but here they said that also new treatment is made on the body originated materiality.

326
00:45:58,160 --> 00:46:03,920
So, sometimes it may be possible to smash some food on our body and then

327
00:46:03,920 --> 00:46:10,160
it will cause the new treatment, let that arise.

328
00:46:19,680 --> 00:46:26,560
And then next, temperature bond materiality, same. We are also in paragraph 40,

329
00:46:26,560 --> 00:46:36,560
that materiality.

