1
00:00:00,000 --> 00:00:25,140
Good evening, everyone, broadcasting live January 21st, so I know Thursday I'm supposed

2
00:00:25,140 --> 00:00:37,040
to be doing something interesting. I have been here, I think. But I just spent the day

3
00:00:37,040 --> 00:00:45,960
pretty busy day today, so I think I'm not going to be doing it really well. And I've got

4
00:00:45,960 --> 00:00:59,160
these TEDx talk coming up, so maybe scaling back some of my activities for the next week.

5
00:00:59,160 --> 00:01:08,460
So next week I have an interview that I have to prepare for for this TEDx talk.

6
00:01:08,460 --> 00:01:17,260
And I'm going to talk isn't even that big of a deal to me, it's more about me and my work

7
00:01:17,260 --> 00:01:22,460
than about the dumbness, so it's not like it's going to be a teaching experience.

8
00:01:22,460 --> 00:01:30,180
Talk, I'm going to be giving this about. There's some of the interesting things I found

9
00:01:30,180 --> 00:01:38,660
about using the internet and social media and modern technology to spread the ancient

10
00:01:38,660 --> 00:01:48,960
teachings of the Buddha, so it's a juxtaposition of ancient and modern. Anyway, that's

11
00:01:48,960 --> 00:02:03,180
what I thought to talk to them about, so that's what I'll be talking about. Let me get

12
00:02:03,180 --> 00:02:13,640
started. Any questions here? At the end I can see the car, one end I can see the cars

13
00:02:13,640 --> 00:02:22,920
in the fact that my thoughts actually, the other end, it's not really so much what to

14
00:02:22,920 --> 00:02:32,080
me, the things that have it recycled. It's really frustrating. One can be frustrating when

15
00:02:32,080 --> 00:02:44,960
you have the idea of yourself, the idea of control. So what you're seeing is impermanence

16
00:02:44,960 --> 00:02:52,760
suffering in oneself. You should read my video or watch my video on, I think it's called

17
00:02:52,760 --> 00:03:11,640
experience of reality. So seeing the three characteristics of stuff, it's not comfortable.

18
00:03:11,640 --> 00:03:17,880
Seeing that you're not in control makes you frustrated. I mean the real answer to your

19
00:03:17,880 --> 00:03:38,320
question, simple answer to your question is, if you don't yourself frustrated, frustrated.

20
00:03:38,320 --> 00:03:48,480
Seeing that you help us to change is a good lesson. These aren't easy questions and not

21
00:03:48,480 --> 00:03:53,240
easy issues. They're not going to be solved in a day. You solve them a lot better when you

22
00:03:53,240 --> 00:04:00,160
do intensive practice, but if you're practicing just on a daily basis, takes time, can

23
00:04:00,160 --> 00:04:08,200
take lifetimes. It took the Buddha for uncountable, uncountable periods of time, plus a hundred

24
00:04:08,200 --> 00:04:35,800
thousand eons, which are relatively short compared to the four uncountable. Oh, and here's

25
00:04:35,800 --> 00:04:44,200
the thing. I'm so Saturday. Saturday, I'm giving, let's see, what am I giving to talk.

26
00:04:44,200 --> 00:05:04,920
Saturday, I'm giving a talk in second life. What did we agree on? What did we disagree

27
00:05:04,920 --> 00:05:15,920
about? It sounds like, I'll be giving a talk at 12 p.m. Just 12 noon, second life time.

28
00:05:15,920 --> 00:05:38,920
The second life.

