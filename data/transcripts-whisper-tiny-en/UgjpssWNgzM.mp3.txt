Hello and welcome back to our study of the Dhampada.
So today we are continuing with verse 254 and verse 255,
which we thus follows.
The Dhampada is the name of the Dhampada.
There is no path in the sky.
There is no summoner outside.
People delight in Babancha.
But the Buddhas are free from Babancha.
There is no path in the sky.
There is no summoner outside.
There is no formation, no sankara.
That is eternal.
And there is no shaking or vassal, no wavering of the Buddhas.
So these verses are supposed to have been a part of the teaching that the Buddha gave to Subhadha.
So it may be the first story where the story doesn't have anything to do with the actual content of the verses.
The story goes that,
Well, Subhadha is a special case.
He was in a past life.
He was a brother with another of the Buddhist disciples, Kundana.
So when the Buddha first decided who to teach,
he thought of the five ascetics who had cared for him and followed him
and depended on him for their teachings, but who had abandoned him.
And he went to find them, and Kundana was the eldest of them.
And when the Buddha first taught them, Kundana was immediately,
it attained salt upon it.
So he immediately understood the Buddha's teaching,
and attained the first stage of enlightenment.
He was the first.
And that's apparently because he had this sort of,
what we call Upanishaya, he had this basis for it from past lives.
So his character type was the sort to do things first.
And there's a story where he was brothers with the person who would eventually become Subhadha.
And together they grew rice, and they were also followers of some Buddha,
or maybe they were religious people,
so they were into giving alms to religious mendicants.
And as soon as the rice sprouted,
and more fruit, green fruit, it's called young rice.
Kundana, the man who was to become Kundana, said to his brother,
I think we should give the first part of our crop.
He was in a hurry and give to religious people
because you can eat the young rice.
I guess you can make milk out of it or something.
But the other, his brother said,
shook his head and said, you're crazy.
That's not, who's ever heard of giving young rice?
Wait until it's way until it's done.
Wait until the end of the harvest.
Once we've harvested it and portioned it out,
we'll set a portion aside and we'll give that portion as charity.
Kundana said, no way.
I think we should do things right away.
This is my intention is to give.
Be the first to give.
Give the first part, before you take for yourself,
in order to counter avarice, greed, stinginess,
miserliness.
Give first," he said.
His brother said, give last.
And then, so this is what they did.
And Kundana, the younger brother said,
in that case, we split our field in half.
You give me half the field.
You take half the field.
And all the what I want, you do what you want.
And so they did.
So they were both quite generous and spiritual people.
And they were both supportive of religious people.
But Subadha gave first.
Kundana gave first.
And Subadha gave last.
And so they say, this is the reason why Kundana was the first
to understand the Buddha's teaching, but Subadha.
Subadha came to see the Buddha when the Buddha was about
to pass away.
He was sick.
He was resting, recovering from his illness.
And Ananda forbid it, he said.
The blessed one is sick.
The blessed one is weak, he's resting.
No chance.
But the Buddha said, no.
The Buddha said, let Subadha in.
He understood that this was the time for Subadha at the very end.
And so Ananda led him in.
And this shows up in the Paridhi Banasut.
There's a different teaching.
So I guess we can understand that he taught more than one thing,
which is, that's OK.
We'll just understand it that way.
In this Kama-pa-da, it is said that he taught this verse as well.
He's two verses.
And that's that.
Apparently he asked questions in the Buddha answered these
in response to his questions.
So that's the story.
Now, the lesson, let's talk about lessons.
Before I get into the actual verse, let's talk about lessons.
Let's get from the story.
Because I think there are two important things
to just point out.
It's an interesting story.
But it does raise a couple of interesting points.
The first is don't wait.
There is fruit to doing things first.
You can take that lesson from it.
Most people would probably say,
Kundana, you had the right of it.
Because why would you wait?
If this is the fruit, wouldn't it be great to be the first?
Or if not the first, early, who wants to be the last
and have to risk that, right?
It shows the connection there, that there is a potential connection
if you're procrastinating.
Certainly that's a bad thing.
I think it's the idea is that Subhata didn't procrastinate.
It was just his way.
And as a result, you really shouldn't ultimately say
that one is better than the other.
But you can take from what you will.
If you want early results, early do things early.
Certainly don't procrastinate.
But the other thing that you can point to from this
is just that it's the way of different people.
And so you should feel reassured and encouraged
that you've all come when you're not at the end of your life.
And I'm not at the end of my life.
And so we're in good shape to practice.
And I'm in good shape to help you practice.
But on the other hand, sometimes there can be a sense
of discouragement that it might be too late.
And you maybe missed an opportunity feeling that feeling
discouraged when you see others who may be become proficient
in the practice more quickly.
For some practice is slow, for some it is quick.
And there's no sense that even one is better than the other
because to some extent it's just our way.
Some people will take many lives.
Even Mahamogalana took less time than Sariputa.
The two Buddhist, two cheap disciples.
And the one who has said to be the greater of the two,
took longer.
So the story of Subadha and Khodanya gives us this idea
or is an example of this idea of everything in its time.
People have different paths.
And rather than comparing ourselves with conceit,
thinking ourselves better or worse,
feeling disappointed in ourselves or discouraged
by our difficulties in the practice,
to understand that things come in their time.
Don't push too hard.
Sometimes we try and force it and we get frustrated.
When results don't come the way we want,
you can just think of Khodanya and Subadha who got their results
when they should, we don't know what our past karma is.
So we try and work with what we have.
So that's the story.
Now the verse itself is, I guess,
a little more profound and has some more profound lessons for us.
It's a little, I think, difficult to understand the verse
without making the connection.
So there are some connections you have to make,
especially with the first part.
Now the first part of the verses,
it repeats in the two verses, the first part is repeated.
But the first quarter of the verse
says that there is no path in the air,
which taken by itself seems like a crazy thing to say.
I mean, it's not wrong,
but it seems like a complete non sequitur.
What does this have to do with the Buddha's teaching?
Why are you teaching this?
So the way it should be understood,
I think, is in conjunction with the second part.
There's no path in the air.
It's actually another way of saying
that there is no summoner outside.
A summoner outside may sound strange as well,
but there's a simple explanation there.
Outside is just a word that means outside of the Buddha's teaching.
It's the way they often just especially in poetry
refer to the meaning of the word as a part from our religion.
A summoner, the word summoner is a word I leave untranslated
just because I don't have a great word for it
because I want you to understand that it's another
fairly simple word.
It just means a person who is spiritual.
It's somewhere the word shaman comes from.
So the Buddha used it.
In some cases, just to refer to any religious person,
but here is actually, of course,
talking about something a lot deeper.
So the Buddha says,
there is no enlightened person.
When he talks about summoner,
he means there is no enlightened person outside.
He is what in the Mahan Parnibanasu to the Buddha is said to have taught
that the Buddha asked him whether other religions
had enlightened beings in the Buddha said,
there are no summoner outside of the Buddha's teaching.
He said, in whatever sassana,
there is the eight full noble path.
When you have right view, right thought, and so on.
Then there will be a soda Parnasakatagami Anagami Aran,
there will be enlightened beings.
But he said, I don't see any religion outside of Buddhism that has that.
And this is where the first part comes in
because this is, of course,
unsurprising but not very reassuring to people
who are new to Buddhism.
Unsurprising because, well, it's what every religion says, right?
Our religion is the best. Everybody else is wrong.
We're right there, everyone else is wrong.
Then it's so not very reassuring because it sounds like a boast.
It sounds like an unsupported claim.
It even can be disappointing to some people who want to find similarities in religions.
And to some extent, it's a bit muddied in modern times
because, of course, what we call Hinduism was influenced by Buddhism.
It's not Buddhism, but it was certainly influenced by it.
And because Buddhism is many different things now, right?
There is even secular movements that drop on the Buddhist teaching.
Psychotherapy, that doesn't claim to be religious,
often incorporates the sorts of practices that we do.
So it's muddied by the fact that mindfulness is now a part of modern discourse.
Ordinary discourse, people talk about mindfulness, not in the original sense of the word,
but as a meditation practice.
As a means of cultivating objectivity in a meditative sense.
But at the time of the Buddha, and even in modern times,
if we talk about the differences between religions,
Buddhism was exceptional and is exceptional.
And it relates to the first part.
That there is no path in the sky.
I think that's a very powerful imagery to talk about the second part that
just as there is no path in the sky.
So too, you're not going to find an enlightened being outside of the Buddha's asana
because they're just in the sky.
It's not grounded as what we would say in English.
So there are some obvious examples of this.
The idea that the way to be free from suffering is belief in God, for example.
Well, that's like saying, hey, look at that path in the sky.
There's nothing there.
There's no connection.
There's no evidence.
There's no basis for that.
And you can say it's not grounded in reality.
So what is exceptional about Buddhism is the grounding in experience.
Right?
Even take somewhat to meditation, or any tradition that talks about absorption,
like transcendental paths, or a lot of Hindu meditative paths,
that transcend the illusion of experience, for example.
So because they are transcendental, they're dealing with a idea.
They're dealing with still often God, but quite often dealing with a single object,
a thing that you focus on.
And they're not at all connected with actual experience,
the things that lead to suffering.
The idea is you escape it, and somehow that is a permanent freedom,
but it's not because it doesn't address.
It's not grounded in the issues of suffering and the cause of suffering.
There's this story of an allegory, I guess, of a joke even,
of someone who is outside looking for something.
And someone comes and says, what are you doing?
Oh, I lost my glasses, or I lost my contact lens, so they can't even see.
And the person starts to help them and says, well, where exactly did you lose it?
Oh, I lost it inside.
Well, why are you looking out here for it?
Oh, because the light is better out here.
They use this as a term for looking in a place that is convenient.
And it relates here because God is convenient.
Some at a meditation is convenient.
It's comfortable.
You're not challenged when you're dealing with concepts.
When you have something you can cling to.
Even if it's just a color or a light, a flight candle flame,
if someone focuses on that.
You're not challenged in the way that you're challenged when you have to focus on your experiences,
when you have to actually take pain as a meditation object.
It seems absurd, horrifying that you should have to do that.
Be present with your emotions.
Don't run away from them.
You don't try and escape them.
It's not something we're used to.
We're used to fight or flight, fix or fleeing.
Make it better or find something better.
So we look in all these different places,
but it's like looking for a path in the sky.
Looking for your contact lens in a place that you didn't lose it.
It's like looking for a path in the sky.
It's like looking for enlightenment outside of actual experience.
So we can say with quite complete certainty.
We can appreciate even as newcomers.
You can appreciate the difference.
You see immediately the difference between focusing on reality like the Buddha explained
and trying to find freedom in some magical conceptual experience.
So that's the first two parts.
The third part, the third part of the first verse,
or the third and fourth parts they go together,
is that people delight in Babancha.
And I didn't translate this one either.
People delight in Babancha,
but Buddhas are free from Babancha or tatagatas.
Those who are thus gone, those who have gone in this way,
those who have traveled this path are free from Babancha.
And Babancha is a word that gives me trouble.
I know there's a movement to sort of translate it as mental proliferation.
But I don't want to go into too much detail.
I think that's a great way to understand it.
Babancha is translated sometimes as an obstacle or an obstruction of hindrance.
It's translated as diffusion.
And it's also translated as obsession.
It appears to be getting entangled, getting caught up in something.
It's not a good mind state.
It's a state of entanglement, let's say.
It means mental proliferation they translate it as.
It's sort of a modern or a Western, maybe a modern anyway.
We have to have translating it.
What exactly it means.
It definitely is a hindrance, so it's not a good thing.
It seems to involve some crossing some line, so it's related to obsession.
But people delight in it.
So no one delights in hindrances.
We delight in things that are hindrances.
And Babancha is what comes after thinking.
So when you think about things, you fall into Babancha,
which means it makes sense to talk about it as this proliferation.
It relates very much, I think, to the first part.
How the Buddha's teaching is very grounded in reality.
And it's what makes the practice of mindfulness.
Let's say very different from even the practice of somewhat
to meditation.
So this is useful as a lesson for people on a meditation course.
To get a sense of that distinction between objective observation of experience
and this diffuse state of mind, the proliferation,
which I think means that Babancha, a good definition of it,
is making more out of things than they actually are.
That's how I like to talk about it.
I think it's an important part of the problem.
And so I think it fits well with the idea of Babancha.
Making more out of things than they actually are.
It covers so much, right?
If you like something,
you absolutely have made more out of the thing than it actually is.
There's nothing intrinsically likable about experiences.
Seeing, hearing, smelling, tasting, feeling, thinking.
They are what they are.
Seeing is just seeing, hearing is just hearing.
And it gives you an idea of why we practice mindfulness
in the first place.
Why are you repeating a mantra to yourself?
It's because the word mindfulness, of course, the word sati
actually translates to something more like remembering.
And what do we mean by that?
I mean remembering what things are instead of forgetting that
and getting lost and what we think they are.
This happy feeling is good.
This painful feeling is bad.
Good and bad.
You've made more of it than it actually is.
This feeling is me.
This pain is mine.
Yeah.
Turning experiences into situations and situations into problems
is making more of the things.
So this is a problem, I think.
It has to be acknowledged with labeling mental illness
as depression or they say schizophrenia.
It's not that it's wrong.
And it's not that it's not descriptive.
It's just that it can become a reification.
You have created something.
More than what is actually there.
And this is something we have to catch ourselves with as meditators,
making a narrative that I am in this situation.
This has happened to me.
This problem has arisen.
When the actual reality is just moments of experience.
And if you're able to see those moments of experience,
the problem disappears.
It was never there.
It was Papancha.
It was extra.
It's not what's actually there.
So this is the clear indication of the difference,
whether people believe,
agree with the Buddha or not.
You can see there is this distinction
that are very clear goal
is to see things as they are right when this very powerful
teaching the Buddha gave to Bahia.
What is the simplest teaching you can give?
Deep-tay-dita-mat-dang-bavi-sity.
Let's see, just be seeing.
Hearing what is heard, let it just be heard.
No, nothing more, no more, no less.
Let's meet Papancha.
And it's powerful.
It's perfect.
It's pure.
And it creates clarity in the mind.
You'll see that a big part of the process of the practice
is realizing things about yourself that you didn't know.
And being forced to confront aspects of yourself
that weren't really clear to you before.
Getting a better sense of how your mind works.
And of course, ultimately, then freeing yourself
from bad habit, the aspects of your mind
that are causing you stress and suffering.
So that's the first verse.
The second verse starts the same.
Just as there are no paths in the sky,
there is no way to enlightenment outside of
the observation of experience or the clear vision
of experience as it is.
But then the second part changes.
There is no sun-car, no formation that is eternal.
But the Buddha is never waver.
So these two are in contrast as well.
And this is, of course, another important core aspect
of our practice, the impermanence of formation.
So the Buddha gives a contrast that
you're looking for impermanence.
That impermanence doesn't exist in experiences.
It doesn't exist in formations.
It exists in enlightenment.
It's a very profound sort of idea
because, of course, permanence is something that knowing
or not, that we seek out in our lives,
our ambitions, our goals.
It's the highest goal in many religious traditions.
This permanent state,
that might be permanent communion with God, for example.
The idea that no formation is permanent
and it is therefore an important rejoinder
to free us from following the wrong path.
Of course, the most glaring is following a wrong religious path,
thinking that if I follow this religious path,
I'll find a permanent home for myself in heaven, for example.
Or as God or something in certain religions.
But, of course, it is much more practically important
for ordinary life as well.
A reminder that the things that we cling to are impermanent.
Our families, our loved ones, our partners,
our possessions, even our own bodies.
And that when we cling to these things,
the eventual loss is what actually brings us stress and suffering.
You know, the impermanence of them is something we can never avoid.
For meditators, of course, it takes on another level,
with the same idea, but another order of magnitude,
because we see this constantly,
with anything that we try to cling to.
You might have a pleasant experience one moment
and really be happy with that
and kind of reassured by it,
complacent as a result of it.
And then struggle when it disappears.
So, I think there are two lessons here.
I mean, these are things you should already know.
We already know about the importance of seeing impermanence,
but the first is to not cling to pleasant or unpleasant experiences.
So, when we talk about Papancha,
we're talking about the difference between concepts
and ultimate reality, but once you make that distinction,
this can be seen as the next step,
where your experiences don't cling to them,
the good ones, the bad ones.
And the second part to be able to see the impermanence
of all experiences to free you from any attachment.
Senkhara sasatana ti, there is no eternal formation.
It is probably the most disconcerting thing,
because it's diametrically opposed to our ordinary inclination,
try and find a refuge, find something you can depend on,
some sort of stability.
We come to meditation for that.
So, we try to organize our practice and organize our minds
and focus our minds.
If only I can get my mind to a state that is stable, lasting,
so on, and this shift, when you realize that that's not even the goal,
is disconcerting.
It's like a growing up, in a sense,
because you have to be accountable.
You have to be flexible.
It's kind of like a good description of being an adult,
of being able to adapt rather than depend,
being impervious to the changes of life.
I don't think it's a good description of most adults,
but it is a kind of, there's a distinction,
and you could say that someone who is still dependent,
of course, is like a dependent infant.
But we depend upon stability if things are like this,
that is acceptable.
And so, an important idea is this idea of lack of dependence,
and you see, don't you, we had a deep dwelling independent.
That's, I think, the important teaching here,
and that is true eternity.
There's an unshakeability.
Not to Buddha and the Minjita.
There's no Injita.
Injita is like this wavering or vacillating,
which is in contrast to formations.
You can't find peace in things.
Your peace has to be independent of experiences
and things in general.
So that I think is the meaning of these verses.
That is the teaching that the Buddha gave
to Subhana, one of his last teachings.
And that's Dhamapada, verses 254 and 255.
So thank you for the same.
Sadhana, sadhana, sadhana.
