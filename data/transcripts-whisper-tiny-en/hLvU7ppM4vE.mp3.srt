1
00:00:00,000 --> 00:00:07,000
Hi, and welcome to this series of videos on how to meditate.

2
00:00:07,000 --> 00:00:12,000
In this first video, I'll be talking about what is the meaning of the word meditation.

3
00:00:12,000 --> 00:00:17,000
First of all, the word meditation can mean many different things to different people.

4
00:00:17,000 --> 00:00:21,000
For some people, meditation means simply a calming of the mind,

5
00:00:21,000 --> 00:00:26,000
a creating of a pleasurable or a peaceful state of being.

6
00:00:26,000 --> 00:00:32,000
For some time, a sort of like a vacation or an escape from the reality around us.

7
00:00:32,000 --> 00:00:41,000
For other people, meditation can mean a extraordinary or a special experience of some alternate state of reality,

8
00:00:41,000 --> 00:00:48,000
perhaps creating magical states of a or super mundane states of awareness.

9
00:00:48,000 --> 00:00:53,000
In this video, I'd like to take the word meditation, first of all, back to its root

10
00:00:53,000 --> 00:00:58,000
and explain it based on the meaning of the word meditation.

11
00:00:58,000 --> 00:01:04,000
When we look at the word meditation, we can see that it comes from a root very similar to the word medication.

12
00:01:04,000 --> 00:01:11,000
And so whereas medication means something which is used to cure the sicknesses which exist in the body,

13
00:01:11,000 --> 00:01:19,000
we then can understand meditation as something which is used to cure the sicknesses which exist in the mind.

14
00:01:19,000 --> 00:01:24,000
So as opposed to, when we look at medication, we can see that as opposed to a drug,

15
00:01:24,000 --> 00:01:30,000
it's not something that simply creates a state of pleasure or a state of happiness for some time.

16
00:01:30,000 --> 00:01:36,000
It's something which is actually used to create, to affect a change in the body.

17
00:01:36,000 --> 00:01:41,000
A state of greater bodily health, bring the body back to a state.

18
00:01:41,000 --> 00:01:44,000
It's natural state of health.

19
00:01:44,000 --> 00:01:54,000
So in this way, meditation, we can understand it as something which not simply brings about a state of peace or calm temporarily.

20
00:01:54,000 --> 00:02:05,000
It's something which returns the mind, which may for many of us be in a unnatural state of being.

21
00:02:05,000 --> 00:02:17,000
Based on its worries or stresses or conditioning, any kinds of conditioning from the experience of the world around.

22
00:02:17,000 --> 00:02:32,000
Instead, we can see that meditation is something which actually affects a change and brings the mind back to its natural state of peace and happiness.

23
00:02:32,000 --> 00:02:41,000
So, in these videos, it's important to understand that sometimes during the meditation it may not feel peaceful and calm.

24
00:02:41,000 --> 00:02:49,000
Sometimes the meditation is dealing with the very deep and unpleasant states which exist in our mind, in our minds.

25
00:02:49,000 --> 00:02:54,000
States of stress, states of worry, states of anger, states of addiction and so on.

26
00:02:54,000 --> 00:03:01,000
And so sometimes it might seem that this sort of meditation is not as peaceful as we might have wished for it.

27
00:03:01,000 --> 00:03:04,000
But we have to understand that this isn't simply a drug.

28
00:03:04,000 --> 00:03:08,000
This isn't simply a way to feel pleasure for a short time.

29
00:03:08,000 --> 00:03:14,000
It's something which is meant to affect a real change on our minds and on our hearts.

30
00:03:14,000 --> 00:03:21,000
To bring our minds and our hearts back to their natural state of clarity and their natural states of peace and happiness.

31
00:03:21,000 --> 00:03:42,000
So, the next question is how is it that we come to affect this change or this healing, this remedy for the states of anger or greed, of addiction, of delusion, of conceit and arrogance, of worry, of stress, of fear and so on?

32
00:03:42,000 --> 00:03:45,000
How is it that we come to cure these things?

33
00:03:45,000 --> 00:03:54,000
And the practice here in this set of videos, the basis of the practice of meditation, as I'll be explaining it, is what we call a clear thought.

34
00:03:54,000 --> 00:04:00,000
We're creating a clear awareness of the experience as it happens in every moment.

35
00:04:00,000 --> 00:04:06,000
And the way we do this is we use an ancient technique which we call mantra.

36
00:04:06,000 --> 00:04:19,000
We're using an ancient, ancient practice of mantra meditation, but instead of focusing it on some sort of mantra, a word, which is super mundane or out of the ordinary.

37
00:04:19,000 --> 00:04:26,000
We're going to be focusing our attention on the ordinary, on the reality around us as we experience it.

38
00:04:26,000 --> 00:04:41,000
But we'll be using a mantra, this word, this name, this label, for the experience which brings the mind to focus and to clearly experience that a phenomenon as it arises.

39
00:04:41,000 --> 00:04:55,000
To know it clearly, to see it for what it is and to not be addicted, attached, or upset, or a verse to that experience, to simply know it and to experience it for what it is.

40
00:04:55,000 --> 00:05:07,000
So, for instance, when we feel something in the body, or when we move our body, or when we think of something in the mind, or so on, we're going to create a clear thought, for instance, thinking.

41
00:05:07,000 --> 00:05:12,000
If we get angry, we're going to say to ourselves angry, if we feel pain, we're going to say to ourselves pain.

42
00:05:12,000 --> 00:05:22,000
We're going to pick a word which describes the experience accurately, and which therefore fixes the mind on the experience for what it is.

43
00:05:22,000 --> 00:05:27,000
Not becoming upset or attached to the experience.

44
00:05:27,000 --> 00:05:35,000
And to do this, we break, traditionally, we'll break experience, or break reality up into four parts.

45
00:05:35,000 --> 00:05:41,000
And so we can label reality in one of these four parts.

46
00:05:41,000 --> 00:05:45,000
Whatever we experience will be somewhere within these four parts.

47
00:05:45,000 --> 00:05:52,000
And on a basic level, these four parts are called body.

48
00:05:52,000 --> 00:05:55,000
The movements are the positions of the body.

49
00:05:55,000 --> 00:06:03,000
Feelings, the sensations which exist in the body, and in the mind, when we feel pain, or happiness, or calm.

50
00:06:03,000 --> 00:06:07,000
The mind, this is the thoughts which exist in the mind.

51
00:06:07,000 --> 00:06:13,000
When we're thinking about the past or the future, good thoughts, bad thoughts, whatever kind of thoughts we have.

52
00:06:13,000 --> 00:06:17,000
And number four, the mental states.

53
00:06:17,000 --> 00:06:30,000
The states of mind, whether they be emotional states, or whether they be states of clarity, or states of drowsiness, or straight states of worry, states of doubt, or so on and so on.

54
00:06:30,000 --> 00:06:40,000
These four, the body, the feelings, the thoughts, and the mental states, are the four foundations of the meditation practice.

55
00:06:40,000 --> 00:06:45,000
This is what we're going to use to create a clear thought in the present moment.

56
00:06:45,000 --> 00:06:54,000
So for instance, every moment when we move our body, suppose we lift our hand, when we raise our hand, during the time we're meditating, we can say to ourselves raising.

57
00:06:54,000 --> 00:06:58,000
When we're sitting still, we can say to ourselves sitting.

58
00:06:58,000 --> 00:07:03,000
Or whatever position the body is, and we can be simply aware of the position of the body.

59
00:07:03,000 --> 00:07:08,000
It's one part of reality that we can use to create a clear thought.

60
00:07:08,000 --> 00:07:12,000
The second is the feelings that exist in the body and in the mind.

61
00:07:12,000 --> 00:07:16,000
When we feel pain in the body, we can say to ourselves pain.

62
00:07:16,000 --> 00:07:18,000
We can repeat again and again to ourselves.

63
00:07:18,000 --> 00:07:26,000
We can say pain, pain, pain instead of allowing the anger or the upset to arise, because of the pain.

64
00:07:26,000 --> 00:07:31,000
We simply remind ourselves that it's merely a sensation which is arisen in the body.

65
00:07:31,000 --> 00:07:39,000
If we feel happy, we can say the same thing, happy, happy, happy, not pushing it away, but also not attaching to it.

66
00:07:39,000 --> 00:07:48,000
Giving it more significant than it actually has, and therefore not creating a state of addiction, a state of attachment, a state of need.

67
00:07:48,000 --> 00:07:52,000
When we feel calm, also we can say calm, calm and so on.

68
00:07:52,000 --> 00:07:55,000
When we're thinking, number three is the thoughts.

69
00:07:55,000 --> 00:08:02,000
So when we're thinking about the past, good thoughts, and from the past, bad thoughts from the past, we can say to ourselves thinking, thinking.

70
00:08:02,000 --> 00:08:10,000
Instead of letting them become something good or something bad, something to create attachment or something to create a version, we simply know that they are thinking.

71
00:08:10,000 --> 00:08:13,000
They are thoughts which are arisen.

72
00:08:13,000 --> 00:08:21,000
When we're thinking about the future, when we're thinking about anything, we can simply come to be aware of the fact that we're thinking.

73
00:08:21,000 --> 00:08:30,000
Instead of liking or disliking or becoming attached to the thoughts, allowing fear and worry and stress and so on to arise.

74
00:08:30,000 --> 00:08:49,000
And the emotional states or the mental states, when we feel angry, so once anger has arisen, instead of letting it build and snowball and become hatred or become an action where we attack or a speech where we shout, where we yell, where we argue.

75
00:08:49,000 --> 00:08:53,000
We simply know that we're angry and we stop at there and we don't allow it to go further.

76
00:08:53,000 --> 00:09:12,000
When we feel greed, when we want something very bad, instead of taking or trying to trick someone into, so that we can get the better of them stealing or tricking or creating trouble for someone else so that we can get what we want.

77
00:09:12,000 --> 00:09:23,000
When we see that we want something and we simply acknowledge it for what it is, and therefore we don't need to do bad needs to get the things that we want.

78
00:09:23,000 --> 00:09:26,000
We see it ourselves wanting wanting.

79
00:09:26,000 --> 00:09:31,000
When we feel lazy or so on and we don't want to do the things which we know we should be doing.

80
00:09:31,000 --> 00:09:34,000
We can say to ourselves lazy, lazy.

81
00:09:34,000 --> 00:09:40,000
When we're distracted or worried or stressed, we can say distracted or worried or stressed.

82
00:09:40,000 --> 00:09:46,000
When we feel doubt, we don't know if we can do what we need to do or we're not sure what to do or we're confused.

83
00:09:46,000 --> 00:09:50,000
We can say to ourselves doubting or confused, confused.

84
00:09:50,000 --> 00:10:18,000
This is the basis of the meditation practice as I'm going to explain it in these videos.

