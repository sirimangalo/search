1
00:00:00,000 --> 00:00:08,200
Hi. Today's question comes from minor players. I have heard you say we are trying to develop

2
00:00:08,200 --> 00:00:15,040
a clear thought sometimes when we meditate as opposed to using the term mindfulness. Did

3
00:00:15,040 --> 00:00:21,080
I misunderstand or are the two terms synonymous and of no real need for concern? Well,

4
00:00:21,080 --> 00:00:26,280
there's certainly no need real need for concern. Although it's helpful if we understand

5
00:00:26,280 --> 00:00:31,040
both of these terms. We should understand what is meant by clear thought and what is

6
00:00:31,040 --> 00:00:35,920
meant by mindfulness. So we don't get caught up thinking that we're being mindful when

7
00:00:35,920 --> 00:00:43,160
we're not or that we have a clear thought when we don't. So it's a good question to ask.

8
00:00:43,160 --> 00:00:47,880
The word clear thought is something that I've sort of put together to explain what's

9
00:00:47,880 --> 00:00:56,040
going on in a simple manner. And I would say it encompasses two terms that are used

10
00:00:56,040 --> 00:01:03,320
in the meditation tradition of the Buddha, which are sati and sampaganya. And these together

11
00:01:03,320 --> 00:01:11,160
I would call the clear thought. So sati is what we normally translate as mindfulness.

12
00:01:11,160 --> 00:01:16,560
And it's often said that the word mindfulness is a poor translation of the word. The word

13
00:01:16,560 --> 00:01:24,760
sati really means to remember or remember and in this and in that sense it can mean many

14
00:01:24,760 --> 00:01:29,880
different things and it's used in a variety of different contexts. In this context it's

15
00:01:29,880 --> 00:01:38,600
qualified by the prefix butti, which means specifically or exactly or only or bear.

16
00:01:38,600 --> 00:01:43,920
So in this case the word that we're looking for is bear remembrance or specifically remembering

17
00:01:43,920 --> 00:01:50,560
something for what it is. An easy way to understand this is in terms of recognizing something.

18
00:01:50,560 --> 00:01:56,560
This is very technical but we're asking about these words. The word mindfulness if we translate

19
00:01:56,560 --> 00:02:01,480
it from sati in terms of the meditation tradition, which should mean recognizing something

20
00:02:01,480 --> 00:02:07,200
for what it is, only for what it is and not making more of it than it actually is.

21
00:02:07,200 --> 00:02:13,880
This is the word sati. And so the use of this word, this mantra, that's the practice

22
00:02:13,880 --> 00:02:18,440
of mindfulness and that's the thought part of it. Because every time something that

23
00:02:18,440 --> 00:02:23,160
arise, every time something arises in our experience, we have thoughts about it. We say

24
00:02:23,160 --> 00:02:30,400
it's good, we say it's bad, we say it's me, we say it's mine, we create a story about

25
00:02:30,400 --> 00:02:35,640
everything. So we always have thoughts about things. Here when we're talking about creating

26
00:02:35,640 --> 00:02:42,560
a clear thought, this is where the word mindfulness and sampatanya, sati and sampatanya come

27
00:02:42,560 --> 00:02:47,840
in. Because when we say to ourselves, when we think something we say thinking, when

28
00:02:47,840 --> 00:02:51,760
we see something we say seeing, instead the thought that comes up is instead of saying,

29
00:02:51,760 --> 00:02:56,240
this is good, we're saying this is this. So specifically knowing something for what it

30
00:02:56,240 --> 00:03:02,600
is and that's it, stopping there. This is the recognizing something for what it is. That's

31
00:03:02,600 --> 00:03:07,760
the word sati or mindfulness. The clear part is sampatanya and that's what comes from

32
00:03:07,760 --> 00:03:13,360
the word. When you say this is this, when you remind yourself of what it is, when you

33
00:03:13,360 --> 00:03:18,240
know it for what it is exactly and nothing more, then your mind is clear. The thought that

34
00:03:18,240 --> 00:03:25,400
arises is a clear thought. That thought of this is this or knowing it for what it is. When

35
00:03:25,400 --> 00:03:31,560
you watch the breath rising, falling around your field pain and you say pain, pain, the

36
00:03:31,560 --> 00:03:36,000
thought that arises, it doesn't mean there's no thought. All you're doing is changing

37
00:03:36,000 --> 00:03:43,120
the thought away from a thought which is based on clinging, which is based on partiality,

38
00:03:43,120 --> 00:03:46,840
the thought which is based on wisdom, which is based on clear understanding. And that's

39
00:03:46,840 --> 00:03:54,840
the word sampatanya, which means clear understanding or full understanding or clarity of

40
00:03:54,840 --> 00:04:00,880
mind. Okay, so I hope that helps to clear this up. It's the same process. The most important

41
00:04:00,880 --> 00:04:06,320
thing is that you understand the process and that you understand why it is that we undertake

42
00:04:06,320 --> 00:04:11,880
this process of, you know, people say would say repeating in your mind this word.

43
00:04:11,880 --> 00:04:15,600
But actually what we're doing is we're changing the thought from a thought of attachment

44
00:04:15,600 --> 00:04:21,240
to a thought of wisdom of clear understanding. Okay, so thanks for the question. Hope

45
00:04:21,240 --> 00:04:46,280
that helps.

