1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study on the Damupanda.

2
00:00:05,000 --> 00:00:13,000
Today we continue on with verse 163, which reads as follows.

3
00:00:13,000 --> 00:00:20,000
Sukharani, asaduni, Atano, a Hita anhijah.

4
00:00:20,000 --> 00:00:27,000
Young way, Hitanjah, asadunjah, tung way, paramadukharam.

5
00:00:27,000 --> 00:00:36,000
Which means the bad, that which is not good, is easily done.

6
00:00:36,000 --> 00:00:39,000
Sukharani.

7
00:00:39,000 --> 00:00:41,000
Atano, a Hita anhijah.

8
00:00:41,000 --> 00:00:45,000
It is, and what is bad for oneself.

9
00:00:45,000 --> 00:00:52,000
What is, of no use to oneself.

10
00:00:52,000 --> 00:00:56,000
What is beneficial and good?

11
00:00:56,000 --> 00:01:02,000
That is the hardest to do.

12
00:01:02,000 --> 00:01:08,000
This verse was taught in regards to,

13
00:01:08,000 --> 00:01:10,000
they were that again.

14
00:01:10,000 --> 00:01:12,000
Last week we learned about Devadatta here.

15
00:01:12,000 --> 00:01:16,000
Specifically talking about the schism,

16
00:01:16,000 --> 00:01:22,000
where they were that proceeded to step,

17
00:01:22,000 --> 00:01:25,000
to divide the monks.

18
00:01:25,000 --> 00:01:31,000
We got some new monks under his tutelage,

19
00:01:31,000 --> 00:01:36,000
and he misled them into thinking that the Buddha was soft.

20
00:01:36,000 --> 00:01:41,000
The Buddha was not going to push them, challenge them,

21
00:01:41,000 --> 00:01:44,000
and was not practicing properly.

22
00:01:44,000 --> 00:01:52,000
So he made a determination to conduct the affairs of the

23
00:01:52,000 --> 00:01:59,000
Sangha part from the rest of the monks.

24
00:01:59,000 --> 00:02:01,000
And so he went and told Ananda this.

25
00:02:01,000 --> 00:02:04,000
They were that to found Ananda in the city,

26
00:02:04,000 --> 00:02:08,000
and said, today I am going to conduct my business.

27
00:02:08,000 --> 00:02:11,000
Basically we are talking about monastic affairs,

28
00:02:11,000 --> 00:02:13,000
and there is a structure to it.

29
00:02:13,000 --> 00:02:15,000
You do them together.

30
00:02:15,000 --> 00:02:18,000
Everybody in the area comes together every two weeks,

31
00:02:18,000 --> 00:02:24,000
and they do the chanting of the rules and so on.

32
00:02:24,000 --> 00:02:29,000
And Devadatta said, no, I am separating.

33
00:02:29,000 --> 00:02:34,000
I am going to divide the Sangha.

34
00:02:34,000 --> 00:02:37,000
Ananda went and told the Buddha,

35
00:02:37,000 --> 00:02:41,000
and the Buddha taught two verses.

36
00:02:41,000 --> 00:02:43,000
There is not much of a story here.

37
00:02:43,000 --> 00:02:45,000
This is what happened.

38
00:02:45,000 --> 00:02:47,000
He first taught a verse from the Odana,

39
00:02:47,000 --> 00:02:50,000
and it is going to be interesting to relate these two verses.

40
00:02:50,000 --> 00:02:54,000
So the verse from the Odana is very similar to the one that we read,

41
00:02:54,000 --> 00:02:57,000
except it is more specific.

42
00:02:57,000 --> 00:03:02,000
If you were a little bit unsure about the Dhamma part of verse,

43
00:03:02,000 --> 00:03:05,000
listen to the Odana, the Odana selves,

44
00:03:05,000 --> 00:03:09,000
buy the good.

45
00:03:09,000 --> 00:03:13,000
That which is good, is easy to do.

46
00:03:13,000 --> 00:03:15,000
Easy to do.

47
00:03:15,000 --> 00:03:19,000
Sukharang sadhu nasadhu.

48
00:03:19,000 --> 00:03:24,000
By the sadhu, sadhu means one who is good.

49
00:03:24,000 --> 00:03:29,000
Sukharang is easy to do.

50
00:03:29,000 --> 00:03:33,000
Sadhu papa nadhu kara.

51
00:03:33,000 --> 00:03:37,000
By the evil, the good is hard to do.

52
00:03:37,000 --> 00:03:39,000
So it is a little different actually.

53
00:03:39,000 --> 00:03:42,000
Papa nasu kara nasu kara nasu.

54
00:03:42,000 --> 00:03:45,000
By the evil, evil is easily done.

55
00:03:45,000 --> 00:03:49,000
Papa mari ahi, dukarang.

56
00:03:49,000 --> 00:03:53,000
By the noble ones, evil is difficult to do.

57
00:03:53,000 --> 00:04:02,000
And then he taught this verse, the Dhamma part of verse.

58
00:04:02,000 --> 00:04:05,000
So whether it happened exactly like this, I am not sure.

59
00:04:05,000 --> 00:04:09,000
But we do have these two verses in the Dvetica,

60
00:04:09,000 --> 00:04:14,000
one from the Odana, one from the Dhamma parta.

61
00:04:14,000 --> 00:04:16,000
So we will discuss them.

62
00:04:16,000 --> 00:04:19,000
I think there is an interesting point to be made in comparison.

63
00:04:19,000 --> 00:04:23,000
But the first, I would first just wanted to talk a little bit about schisms

64
00:04:23,000 --> 00:04:26,000
and communal harmony in general.

65
00:04:26,000 --> 00:04:29,000
I think there is a really interesting point in the fact that the Buddha

66
00:04:29,000 --> 00:04:32,000
never kicked Devadatta out of the sangha.

67
00:04:32,000 --> 00:04:37,000
It is an interesting point to the fact that the Buddha allowed Devadatta

68
00:04:37,000 --> 00:04:42,000
to ordain knowing full well that Devadatta was corrupt

69
00:04:42,000 --> 00:04:48,000
or that we let anyone ordain without first testing them.

70
00:04:48,000 --> 00:04:52,000
Nowadays, nowadays we are a little less confident

71
00:04:52,000 --> 00:04:54,000
than the Buddha in our ability to help people.

72
00:04:54,000 --> 00:04:56,000
I think it is the point.

73
00:04:56,000 --> 00:05:01,000
And so we put up lots of barriers to ordination.

74
00:05:01,000 --> 00:05:07,000
We have been through this experience with monks who behave very badly.

75
00:05:07,000 --> 00:05:13,000
And because we are not the Buddha, we are not confident in our ability to deal with that.

76
00:05:13,000 --> 00:05:23,000
I had been to monasteries where I thought the head monk was a great monk.

77
00:05:23,000 --> 00:05:30,000
But I wondered why so many bad monks were allowed to stay to come and to stay.

78
00:05:30,000 --> 00:05:34,000
My teacher said to us once he said,

79
00:05:34,000 --> 00:05:37,000
don't bring people to tell.

80
00:05:37,000 --> 00:05:41,000
I think he was talking about Germany actually

81
00:05:41,000 --> 00:05:50,000
because they were having a hard time with a German society.

82
00:05:50,000 --> 00:05:57,000
The monks going on the arms round was seen as begging, which is not allowed.

83
00:05:57,000 --> 00:05:59,000
So they had to be careful.

84
00:05:59,000 --> 00:06:01,000
Anyway, there was some concern.

85
00:06:01,000 --> 00:06:06,000
And he said, tell them, we will take all their bad people.

86
00:06:06,000 --> 00:06:08,000
Bring all their bad people to us.

87
00:06:08,000 --> 00:06:11,000
And we'll do a service to society.

88
00:06:11,000 --> 00:06:17,000
And he said, we take all the worst people and turn them into good people.

89
00:06:17,000 --> 00:06:21,000
I mean, maybe wishful thinking that they can become good people,

90
00:06:21,000 --> 00:06:27,000
but we can certainly make them better people.

91
00:06:27,000 --> 00:06:31,000
And so I think it's quite instructive.

92
00:06:31,000 --> 00:06:34,000
The fact that they were not to become a monk.

93
00:06:34,000 --> 00:06:42,000
The fact that the Buddha tolerated him and refused to be the one to create for their karma.

94
00:06:42,000 --> 00:06:53,000
It refused to be the one to instigate violence, right?

95
00:06:53,000 --> 00:07:01,000
Allowing given that the two torture himself until he finally realized that's all he was doing.

96
00:07:01,000 --> 00:07:04,000
And he couldn't hurt the Buddha or the Buddha's religion.

97
00:07:04,000 --> 00:07:10,000
And some people have said, David, that was made a great service to the Buddha's religion.

98
00:07:10,000 --> 00:07:18,000
To show how great and compassionate and moderate and tolerant the Buddha was.

99
00:07:18,000 --> 00:07:24,000
That even in the face of great evil was unmoved.

100
00:07:24,000 --> 00:07:26,000
He was patient.

101
00:07:26,000 --> 00:07:31,000
And still thinking only of the greater good.

102
00:07:31,000 --> 00:07:33,000
And the greater good for Devodata.

103
00:07:33,000 --> 00:07:36,000
He once said, I think, I'm not sure if it's...

104
00:07:36,000 --> 00:07:39,000
I can't remember where it is exactly or if it actually is,

105
00:07:39,000 --> 00:07:44,000
but I've heard people talk about this where he said he loved...

106
00:07:44,000 --> 00:07:47,000
He loved Devodata just as he loved his own son.

107
00:07:47,000 --> 00:07:50,000
Pretty sure it's in the particular or the commentary.

108
00:07:50,000 --> 00:07:53,000
But it's a good point.

109
00:07:53,000 --> 00:07:56,000
What would it say if...

110
00:07:56,000 --> 00:08:01,000
What does it say when we don't like someone?

111
00:08:01,000 --> 00:08:03,000
As Buddhists, I mean.

112
00:08:03,000 --> 00:08:08,000
Our Buddhist practice, and yet we commit to all these values,

113
00:08:08,000 --> 00:08:13,000
and yet we hate evil people.

114
00:08:13,000 --> 00:08:17,000
I'm thinking very much about the situation in the world now.

115
00:08:17,000 --> 00:08:25,000
There's a lot of modern occurring affairs that relate to this.

116
00:08:25,000 --> 00:08:30,000
Between different countries in the shadow of war,

117
00:08:30,000 --> 00:08:33,000
the specter of war.

118
00:08:33,000 --> 00:08:40,000
Within countries, civil war, civil disobedience.

119
00:08:40,000 --> 00:08:45,000
And not to take sides or to criticize or to attack.

120
00:08:45,000 --> 00:08:48,000
Or even to comment really on current affairs,

121
00:08:48,000 --> 00:08:53,000
but to point out this dumb of tolerance and forgiveness

122
00:08:53,000 --> 00:08:55,000
and thinking about the good of all being.

123
00:08:55,000 --> 00:08:59,000
Thinking only for good.

124
00:08:59,000 --> 00:09:04,000
We often strive for good,

125
00:09:04,000 --> 00:09:09,000
and yet when someone who is clearly steeped in evil comes along,

126
00:09:09,000 --> 00:09:14,000
we are willing to completely discard them as trash.

127
00:09:14,000 --> 00:09:19,000
We're willing to categorize them as evil,

128
00:09:19,000 --> 00:09:22,000
that have no interest in their well-being.

129
00:09:22,000 --> 00:09:24,000
Thinking, let's just get rid of them.

130
00:09:24,000 --> 00:09:27,000
Kick them out.

131
00:09:27,000 --> 00:09:30,000
Anyway, I think that's an important lesson here.

132
00:09:30,000 --> 00:09:32,000
It's not really the main lesson, obviously.

133
00:09:32,000 --> 00:09:34,000
It's part of it.

134
00:09:34,000 --> 00:09:38,000
It's hard for a noble one to kick someone out,

135
00:09:38,000 --> 00:09:42,000
because it's hard for them to do something evil.

136
00:09:42,000 --> 00:09:45,000
It's hard for them to hurt in it.

137
00:09:45,000 --> 00:09:50,000
So the Adonis says,

138
00:09:50,000 --> 00:09:53,000
the Adonis says something quite reasonable.

139
00:09:53,000 --> 00:09:54,000
And I like the dumb of a pot.

140
00:09:54,000 --> 00:09:55,000
I think the dumb of a pot of verses

141
00:09:55,000 --> 00:09:56,000
is actually more special,

142
00:09:56,000 --> 00:09:58,000
but it's special in a different way,

143
00:09:58,000 --> 00:10:01,000
because the Adonis says something quite simple.

144
00:10:01,000 --> 00:10:02,000
If you're a good person,

145
00:10:02,000 --> 00:10:04,000
it's hard for you to do evil.

146
00:10:04,000 --> 00:10:05,000
If you're an evil person,

147
00:10:05,000 --> 00:10:07,000
it's hard for you to do good.

148
00:10:07,000 --> 00:10:09,000
Good people do good easily.

149
00:10:09,000 --> 00:10:13,000
And evil people do evil easily.

150
00:10:13,000 --> 00:10:17,000
Again, to be clear,

151
00:10:17,000 --> 00:10:19,000
there's no such thing as an evil person

152
00:10:19,000 --> 00:10:21,000
or a good person.

153
00:10:21,000 --> 00:10:22,000
Evil is a mind-state.

154
00:10:22,000 --> 00:10:24,000
Good is a mind-state.

155
00:10:24,000 --> 00:10:27,000
We should never ask the question,

156
00:10:27,000 --> 00:10:29,000
am I evil or am I good?

157
00:10:29,000 --> 00:10:31,000
We should be clear about the good

158
00:10:31,000 --> 00:10:33,000
and the evil that we have inside.

159
00:10:33,000 --> 00:10:35,000
And that's the truth.

160
00:10:35,000 --> 00:10:37,000
It's not even that we have inside.

161
00:10:37,000 --> 00:10:40,000
It's that has the potential for arising

162
00:10:40,000 --> 00:10:41,000
because of our bad habits,

163
00:10:41,000 --> 00:10:44,000
because of our ignorance and so on.

164
00:10:44,000 --> 00:10:46,000
So if we're very good people,

165
00:10:46,000 --> 00:10:49,000
that sort of bad stuff doesn't arise as much.

166
00:10:49,000 --> 00:10:52,000
If we're very bad people,

167
00:10:52,000 --> 00:10:54,000
the good stuff doesn't arise as much.

168
00:10:54,000 --> 00:10:57,000
And so the potential to do good

169
00:10:57,000 --> 00:10:59,000
evil depends very much on your state,

170
00:10:59,000 --> 00:11:01,000
depends very much on your habits

171
00:11:01,000 --> 00:11:05,000
and your development of mind,

172
00:11:05,000 --> 00:11:07,000
your clarity of mind.

173
00:11:07,000 --> 00:11:10,000
But the Dhamapada says something a little bolder.

174
00:11:10,000 --> 00:11:12,000
It says, for anyone,

175
00:11:12,000 --> 00:11:14,000
basically in general,

176
00:11:14,000 --> 00:11:18,000
that which is good and to your benefit is hard to do.

177
00:11:18,000 --> 00:11:22,000
It's easy to do that, which is not to your benefit.

178
00:11:22,000 --> 00:11:25,000
And I don't want to make a huge deal of the difference,

179
00:11:25,000 --> 00:11:28,000
but I think it's interesting to talk about

180
00:11:28,000 --> 00:11:30,000
the difference between good

181
00:11:30,000 --> 00:11:33,000
and what is good for you.

182
00:11:33,000 --> 00:11:36,000
Because doing good is just a general thing

183
00:11:36,000 --> 00:11:39,000
that often has carbonic consequences.

184
00:11:39,000 --> 00:11:41,000
It's a good carbonic consequences,

185
00:11:41,000 --> 00:11:44,000
but it's not a direct practice

186
00:11:44,000 --> 00:11:47,000
leading to the realization of freedom from suffering.

187
00:11:47,000 --> 00:11:51,000
It doesn't purify your mind

188
00:11:51,000 --> 00:11:55,000
in any categorical or profound way.

189
00:11:55,000 --> 00:11:58,000
It just makes you happy

190
00:11:58,000 --> 00:12:00,000
if you're nice to people.

191
00:12:00,000 --> 00:12:01,000
That's a good thing.

192
00:12:01,000 --> 00:12:04,000
And it is supportive of your practice, certainly.

193
00:12:04,000 --> 00:12:06,000
There's a difference between good

194
00:12:06,000 --> 00:12:09,000
and what actually heals mind,

195
00:12:09,000 --> 00:12:11,000
actually is good for you.

196
00:12:15,000 --> 00:12:17,000
And so here, I think it is fair to say that

197
00:12:17,000 --> 00:12:21,000
for all of us, what is good for us is difficult to do,

198
00:12:21,000 --> 00:12:25,000
because what is good for us is change.

199
00:12:25,000 --> 00:12:28,000
It's a change of our bad habits.

200
00:12:28,000 --> 00:12:31,000
We're not talking actually about the good habits.

201
00:12:31,000 --> 00:12:34,000
We're talking about the stuff that we're trying to heal,

202
00:12:34,000 --> 00:12:36,000
the stuff that we're trying to cure.

203
00:12:36,000 --> 00:12:39,000
And those people have the most,

204
00:12:39,000 --> 00:12:44,000
unfortunately, those people have the most

205
00:12:44,000 --> 00:12:46,000
need of change.

206
00:12:46,000 --> 00:12:49,000
Those are the most,

207
00:12:49,000 --> 00:12:52,000
because in enlightened being has nothing that is

208
00:12:52,000 --> 00:12:54,000
to their benefit.

209
00:12:54,000 --> 00:12:57,000
A person, an area with the Buddha mentions here.

210
00:12:57,000 --> 00:12:58,000
The Buddha is enlightened.

211
00:12:58,000 --> 00:13:00,000
There's nothing that's to their benefit.

212
00:13:00,000 --> 00:13:01,000
Why?

213
00:13:01,000 --> 00:13:03,000
Because they've already done what needs to be done.

214
00:13:03,000 --> 00:13:06,000
There's nothing that can make them a better person,

215
00:13:06,000 --> 00:13:10,000
not fundamentally sure they can study more

216
00:13:10,000 --> 00:13:15,000
and become as smarter and more aware of the world

217
00:13:15,000 --> 00:13:20,000
that they can't become purer.

218
00:13:20,000 --> 00:13:21,000
And so,

219
00:13:21,000 --> 00:13:24,000
whereas it's easy for such people to do good,

220
00:13:24,000 --> 00:13:26,000
they have no need of it.

221
00:13:26,000 --> 00:13:29,000
And so,

222
00:13:29,000 --> 00:13:32,000
to the extent that we have things that need to be done,

223
00:13:32,000 --> 00:13:33,000
to that extent it's difficult.

224
00:13:33,000 --> 00:13:36,000
A person who is full of bad habits,

225
00:13:36,000 --> 00:13:39,000
very strong bad habits,

226
00:13:39,000 --> 00:13:43,000
who needs that which is good for the needs to do that,

227
00:13:43,000 --> 00:13:45,000
which is good for the most,

228
00:13:45,000 --> 00:13:49,000
has the hardest time doing it.

229
00:13:49,000 --> 00:13:53,000
And so, here we're not just talking about good deeds.

230
00:13:53,000 --> 00:13:55,000
We're talking about studying our habits,

231
00:13:55,000 --> 00:13:58,000
and learning about them,

232
00:13:58,000 --> 00:14:01,000
and the practice of meditation,

233
00:14:01,000 --> 00:14:07,000
by which we train the mind to be in the present moment

234
00:14:07,000 --> 00:14:11,000
and to be objective about things.

235
00:14:11,000 --> 00:14:17,000
And by doing so, to focus on reality and to see it as it is,

236
00:14:17,000 --> 00:14:20,000
and to change our perception of things,

237
00:14:20,000 --> 00:14:24,000
to change the way we look at our experiences.

238
00:14:24,000 --> 00:14:29,000
So, that instead of reacting to them,

239
00:14:29,000 --> 00:14:35,000
we see them clearly.

240
00:14:35,000 --> 00:14:41,000
We're always looking for a goal,

241
00:14:41,000 --> 00:14:44,000
trying to achieve something,

242
00:14:44,000 --> 00:14:46,000
even in meditation,

243
00:14:46,000 --> 00:14:48,000
wondering what we're going to get out of it,

244
00:14:48,000 --> 00:14:52,000
and concerned about what we're going to get out of it.

245
00:14:52,000 --> 00:14:56,000
It's very hard to change that habit.

246
00:14:56,000 --> 00:14:59,000
Enlightenment is not natural to anyone.

247
00:14:59,000 --> 00:15:02,000
It's not the way we...

248
00:15:02,000 --> 00:15:05,000
It's not going back to anything natural.

249
00:15:05,000 --> 00:15:09,000
What's natural really is all the bad habits that we have.

250
00:15:09,000 --> 00:15:12,000
It's our nature.

251
00:15:12,000 --> 00:15:17,000
We are made up of conflict

252
00:15:17,000 --> 00:15:23,000
and attachment to version, delusion.

253
00:15:23,000 --> 00:15:25,000
We're in the dark.

254
00:15:25,000 --> 00:15:27,000
We didn't start in the light.

255
00:15:27,000 --> 00:15:31,000
Light is something that has to come into being.

256
00:15:31,000 --> 00:15:34,000
Wisdom is the same.

257
00:15:34,000 --> 00:15:36,000
We'll be coming a night,

258
00:15:36,000 --> 00:15:39,000
and of course, as the hardest thing one can ever do.

259
00:15:39,000 --> 00:15:41,000
Most difficult.

260
00:15:41,000 --> 00:15:46,000
It's in direct opposition to everything that got us where we are now.

261
00:15:46,000 --> 00:15:49,000
We are now to becoming human,

262
00:15:49,000 --> 00:15:57,000
to being caught up in some sorrow and ambition

263
00:15:57,000 --> 00:16:04,000
and goals and wants and needs,

264
00:16:04,000 --> 00:16:10,000
just being alive with all our desires

265
00:16:10,000 --> 00:16:14,000
and wishes and wants and time.

266
00:16:14,000 --> 00:16:16,000
So it's a simple teaching,

267
00:16:16,000 --> 00:16:20,000
but I think it bears noting

268
00:16:20,000 --> 00:16:24,000
how, yes indeed,

269
00:16:24,000 --> 00:16:28,000
good people have an easier time doing good.

270
00:16:28,000 --> 00:16:30,000
But no matter whether you're good or evil,

271
00:16:30,000 --> 00:16:33,000
the hardest thing is to better yourself.

272
00:16:33,000 --> 00:16:35,000
Even for a good person.

273
00:16:35,000 --> 00:16:37,000
In fact, it's just as hard.

274
00:16:37,000 --> 00:16:41,000
Just as hard for a sotapana, sakatakami and anagami

275
00:16:41,000 --> 00:16:43,000
to better themselves.

276
00:16:43,000 --> 00:16:50,000
It's always going to be the things that we're least mindful of.

277
00:16:50,000 --> 00:16:55,000
That's why again and again we have to remind the meditators

278
00:16:55,000 --> 00:16:57,000
just because your practice is good.

279
00:16:57,000 --> 00:16:58,000
It's pleasant.

280
00:16:58,000 --> 00:17:01,000
It doesn't mean it's good.

281
00:17:01,000 --> 00:17:07,000
It's what your least mindful of that you have to pay most attention to.

282
00:17:07,000 --> 00:17:09,000
Because these are the bad habits.

283
00:17:09,000 --> 00:17:14,000
The causes of, if we saw things clearly, if and when

284
00:17:14,000 --> 00:17:19,000
and in those aspects of a life that we do see clearly,

285
00:17:19,000 --> 00:17:21,000
there will be no problem.

286
00:17:21,000 --> 00:17:23,000
There will be no suffering.

287
00:17:23,000 --> 00:17:25,000
There's no bettering yourself there.

288
00:17:25,000 --> 00:17:28,000
Bettering yourself comes from changing the bad habits,

289
00:17:28,000 --> 00:17:33,000
which are the ones we're your least mindful.

290
00:17:33,000 --> 00:17:36,000
So it's difficult to do.

291
00:17:36,000 --> 00:17:40,000
Different from saying that good people do good things.

292
00:17:40,000 --> 00:17:41,000
Yes, they do.

293
00:17:41,000 --> 00:17:42,000
That's what's good about them.

294
00:17:42,000 --> 00:17:44,000
But what's bad about them?

295
00:17:44,000 --> 00:17:46,000
That's what we're interested in.

296
00:17:46,000 --> 00:17:50,000
So until you become an arahant for a Buddha,

297
00:17:50,000 --> 00:17:53,000
it will always be difficult to better yourself.

298
00:17:53,000 --> 00:17:56,000
At the point when you become enlightened,

299
00:17:56,000 --> 00:17:58,000
it will be impossible to better yourself.

300
00:17:58,000 --> 00:18:00,000
So it goes from difficult to impossible.

301
00:18:00,000 --> 00:18:01,000
It never gets easy.

302
00:18:01,000 --> 00:18:05,000
It will be impossible because you've already done the work.

303
00:18:05,000 --> 00:18:09,000
So that's the dumber part of her tonight.

304
00:18:09,000 --> 00:18:36,000
Thank you all for tuning in.

