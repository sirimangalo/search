the mind still mostly on the mind. It's the one most important thing. First, when you practice
meditation you get to taste the mind. All the things that you're tasting while you're here
you get to taste the mind. Many people say there's that saying no, the mind is a terrible thing to taste, or is that waste, I'm not sure.
There is a saying, the mind is a terrible thing to taste. It can be, you know, when you have to deal with what's going on in the mind.
But from the sounds of it, at this point, if you think back, then you can see how the mind can be a terrible thing to taste.
But now, after you've done the foundation course and you've done some retreats, you can see the difference.
Anyone asks, is meditation of any value? Because you might forget, this is the nature of the mind. We forget what we've done in the past.
So you have to keep this in mind and you have to do this. Now you have proof, and don't forget that meditation is valuable.
Because it's now brought you to the point where the mind isn't such a bad thing to taste.
Why is this? Why is it that actually the mind isn't? When you dig, dig, dig, dig, go very, very deep and eventually you come to the mind.
It's not such a bad thing to experience or to be involved in. This is because the Buddha said, this is what we're getting in today.
The Buddha taught that our mind is a bad thing. The mind is a rotten thing that we have to throw away.
Because he often said that about the body. Because the body is something that we have to throw away.
Before long, the body will lie on the earth. The Buddha said, what Kalingana?
Nirakta, what Kalingana? Just like a child log. It's useless as a child piece of wood.
There's no body that we cling to, you know, my body. Actually all that's happening is it's clinging to us and eventually it stops clinging.
And we have to let it go. Some people can't, that's why we talk here about ghosts, no engraving earth. Because once in a while they can't let go and they're so clinging so much.
At the body we have to let it go. It's something that is actually rotten inside or it's ripening and eventually it's going to go rotten and all the parts.
But the mind on the other hand, the Buddha said, deep down in the mind, the mind is beautiful.
It's radiant. The Buddha said, I'm going to be coming, deep down.
So it's not that our mind is defiled. The mind is, it's nothing bad about our mind but whoa, what's wrong?
He says, dancha, koh kah, kantu kah, upakil a se, upakil it comes, it becomes defiled.
The mind becomes defiled by visiting a kantu kah, visiting defound.
And the filaments that just pass through.
And then he says that an ordinary person doesn't see that.
So they might think there's something wrong with the mind when an ordinary person has depression or anger or greed.
They can drive them crazy because they think that this is a part of them.
This is why people take medication trying to change the mind.
They take these medications and it attacks the brain but it doesn't get to the mind.
But because they don't see that actually the mind is bright, the Buddha said, it's for that reason that I say there is no...
There is no jitabalana, there is no development of mind for an ordinary person, for most people.
Most people are unable to develop their mind because they can't see that deep down the mind.
Deep down the mind is...
And deep down the mind is radiant.
But a person who has considered carefully and looked into the wise things is that this person is able to see that the mind is beautiful.
The mind is radiant because they are able to practice meditation.
And then the Buddha goes on, he says, in fact he gives an example of the mind of loving kindness.
The mind of loving kindness for example.
If Adshara, some katamatampeek, a bikhui, bikhui, maida kiptena, a sai wati, if a monk or anyone who takes on the life of a meditator,
or to practice the mind of loving kindness, for example, even for an instant.
So after you practice your meditation, you send love to the other meditators and your parents and your friends and your enemies.
And to all beings, if you do it even just for a moment, just take a moment to send love.
The Buddha said, this person, if it's this bikhui, their meditation is not in vain.
We have Alita, Johnny, Janu, Alita, Janu, they are a person who practices Jana, not in vain, not without purpose.
Practice is meditation. We have a team. They develop meditating with some purpose.
They are a person who follows the teaching of the Buddha, Satu, Satsan, Nakarou, who undertakes the teaching of the Buddha, the teaching of the teacher.
This is Buddha.
He says, what do you think of, what could you say about when someone really develops it?
He develops it extensively.
The final thing he has to say is that, all Akusiladhamma and all Kuziladhamma, he was on this rabbit, must stop there.
Kuziladhamma and Akusiladhamma, they have the mind as their manopu banga, manopu banga.
They all have the mind as their forerunner. The mind comes before all Kuzilad and all Kuzilad.
So you can put this all together. This is actually the same teaching of Kuziladhamma.
We are talking about the mind. Our mind is beautiful. Our mind is not something that we should throw away or discard or try to run away from.
The reason why it's so difficult to face the mind is that the mind is covered over in defilement.
It's like spinning around like a top and throwing out all these defilements.
In the beginning that's hard to see, but at this point you should be able to see that actually.
The problem isn't the mind, the problem is the defilement.
Actually you can feel good about yourself. You can feel good about the mind.
Focus your life on the mind on keeping your mind free from these defilements.
It's like you have to set up a guard as you have this wonderful treasure of a mind that can do so much good for you.
But if it gets taken over by the defilements it will become your worst enemy.
This is why he says that the development of the mind is great benefit.
Even for just a moment you do a great thing.
I think what's important about this teaching about a moment is to help us get over the idea of how long we're practicing.
Even our day or week or month or years and just focus on the moment.
Not think about how long it's going to take or how long we have to sit and meditate for how long we have to stay here or so.
Think about just this moment. Don't think about oh in the past I was not a good meditator or I didn't meditate or I did a lot of bad things in the past.
Only think of this one moment and you can you can be proud of yourself in that moment.
You can feel good about yourself. You can feel good about your mind and confident about what you've done.
Because when you work in moments then you're actually you're actually truly developing good things.
You're truly developing good things. You can only develop goodness in a moment.
You can't develop goodness an hour from now or an hour ago.
Even a moment from now you can't develop it. You can only develop it now.
And that's only one moment of goodness.
The next moment maybe your mind changes and your into delusion or attachment or anger or hatred or so.
So we should always think that this is a very important teaching to always think in terms of moments.
This is why the Buddha said even for a moment we should think like that.
Then we should count the moments not count the hours.
Never count the hours of practice that you've done in weeks or the months or the years.
Count the moments and then you think oh I've done a lot of meditation too much.
I can't even count how much meditation.
Or maybe you think I haven't even done many moments and done many hours but most of those hours were nodding off or thinking about other things.
So you can see where the real meditation is.
The Buddha said it's the mind that the mind that is defiled.
All Akusana, Dhamma, all unwholesome Dhammas, all bad things we say.
It all comes from when these defilements take over the mind.
You can't have Akusana Dhamma without the mind.
You can't have unwholesome without the judgments of the mind.
There's the mind falls into liking and disliking, clinging to this one.
It becomes averse to that.
It leads us around in so much, so much delusion.
So our practice is to find the pure mind inside, which is something for us to think about.
That actually the mind is not the problem.
You can see the mind and this is why it works for us to simply see things as they are because there's nothing wrong with things.
There's nothing wrong with the world around us. There's nothing wrong with our mind.
What's wrong is the judgments and the defilements that cover the mind up.
When you can see things as they are in thinking or seeing, hearing, smelling, suddenly the mind is pure again.
I think the mind is inherently dirty. It's not true.
At the moment when you see seeing, you connect with the pure mind, your mind is pure.
If you're clear, you'll know seeing. When you lift your foot, you'll know your lifting of what this is lifted.
You have a clear mind.
At that moment there's no defilements in the mind.
When you know the movement and you know the object and you know the stomach,
you know this is rising.
Just knowing this is rising, the mind is pure.
So we're not trying to create anything. We're trying to get rid of stuff.
So then in the end all that's left is our awareness of things as they are.
This, this, this, this, this.
Certainly isn't something easy.
But it's something we should feel confident that deep down inside we're perfectly pure.
And just have to find that perfectly pure.
Perfect pure.
Get rid of all the interior. I think that's covering it up.
Digging down to find the buried treasure.
So just another short teaching.
Something for us to think about us, we go through the,
we try to just go through the Buddhist teaching.
When you have an idea of what the Buddhist teach, I think even from just as brief,
few teachings that we've gone through,
you can see how much emphasis the Buddha placed on meditation and on, on the mind.
So he didn't say studying or helping other people or someone.
He said, developing the mind, even for just a moment, that's what makes your monastic life,
the life of a monk worthwhile.
Not to mention the life of all beings.
So this is for us to develop.
It's encouragement that we're on the right path.
And we're doing, as the Buddha said in, just now he said,
this is someone who follows his exhortation.
This is someone who undertakes his teaching.
Who performs the instruction in the Buddha.
So we're practicing Buddhism here.
And we have the Buddha to back us up.
So without further ado, we'll move on.
We'll get on with our meditation.
We can all go back in the mind for frustration walking.
