1
00:00:00,000 --> 00:00:05,000
For how long is one considered to be a beginner meditator?

2
00:00:05,000 --> 00:00:07,000
Is there any timeline for this?

3
00:00:07,000 --> 00:00:20,000
Well, one thing to note is that the meditation that I teach, that I've put on the internet,

4
00:00:20,000 --> 00:00:23,000
is very basic.

5
00:00:23,000 --> 00:00:33,000
It's not the be-all end-all of the meditation technique that we practice.

6
00:00:33,000 --> 00:00:42,000
So the best way to become an advanced meditator, and there's at least in the technical sense,

7
00:00:42,000 --> 00:00:51,000
is to undertake a meditation course.

8
00:00:51,000 --> 00:00:55,000
Because if you come and do a meditation course here, for example,

9
00:00:55,000 --> 00:01:00,000
I'll lead you through some higher stages, and we'll go through exercises anyway.

10
00:01:00,000 --> 00:01:04,000
So you've become an advanced meditator in that sense.

11
00:01:04,000 --> 00:01:08,000
Even more so, because of the intensity of your practice,

12
00:01:08,000 --> 00:01:15,000
you've become an advanced meditator in an ultimate sense as well.

13
00:01:15,000 --> 00:01:25,000
That being said, in an ultimate sense, it has totally to do with your state of mind.

14
00:01:25,000 --> 00:01:33,000
You become an advanced meditator when you have less agreed, less anger, less delusion,

15
00:01:33,000 --> 00:01:37,000
when your mind is more pure than it was before.

16
00:01:37,000 --> 00:01:42,000
To the extent that your mind is pure, and that you see things clearly,

17
00:01:42,000 --> 00:01:49,000
and that your experience of reality is interactive rather than reactive.

18
00:01:49,000 --> 00:01:52,000
To that extent, I would say you're an advanced meditator.

19
00:01:52,000 --> 00:02:01,000
It is nothing to do with your magical powers or so under strength of mind, however.

20
00:02:01,000 --> 00:02:15,000
It has to do with your purity, the amount of greed, anger, and delusion that is left in your mind.

