1
00:00:00,000 --> 00:00:14,000
So today we continue our study of the Dhamapada on the first number 24, which is as follows.

2
00:00:30,000 --> 00:00:50,000
Guirati, which means, through striving and heedfulness, restraint and training.

3
00:00:50,000 --> 00:00:56,000
One a wise man should make an island for themselves.

4
00:00:56,000 --> 00:01:24,000
So this was told in regards to a monk who became an arahan,

5
00:01:24,000 --> 00:01:37,000
and became an arahan, after failing to learn a single stanza.

6
00:01:37,000 --> 00:01:40,000
So how the story goes is he and his brother became monks.

7
00:01:40,000 --> 00:01:46,000
They had been raised by their grandparents.

8
00:01:46,000 --> 00:01:56,000
Their mother had run away with servant and had lost all of her repute with the family.

9
00:01:56,000 --> 00:02:00,000
So in the end she had no money and no where to live.

10
00:02:00,000 --> 00:02:03,000
So she had to send the children back to live with their grandparents.

11
00:02:03,000 --> 00:02:08,000
And so the grandfather was the disciple of the Buddha.

12
00:02:08,000 --> 00:02:12,000
So he would take his grandchildren to listen to the Buddha teach.

13
00:02:12,000 --> 00:02:18,000
And so the elder son Mahapan Pika became a monk.

14
00:02:18,000 --> 00:02:22,000
And through his practice of the Buddha's teaching became an arahan.

15
00:02:22,000 --> 00:02:26,000
And he thought to himself, wouldn't it be great if I could give this to my younger brother.

16
00:02:26,000 --> 00:02:35,000
So he convinced his grandfather to let his brother become a monk.

17
00:02:35,000 --> 00:02:39,000
And he taught his younger brother the stanza.

18
00:02:39,000 --> 00:02:44,000
And so he tried to teach in the stanza that was basically praise of the Buddha.

19
00:02:44,000 --> 00:02:49,000
And for four months, every time he would learn the first line,

20
00:02:49,000 --> 00:02:53,000
remember the first line, then he would go on to memorize the second line.

21
00:02:53,000 --> 00:02:56,000
And as soon as he memorized the second line, he'd forgotten the first line.

22
00:02:56,000 --> 00:03:03,000
So four lines stanza, just like the one that I gave was a two-line stanza.

23
00:03:03,000 --> 00:03:05,000
So just twice as much of that.

24
00:03:05,000 --> 00:03:08,000
For four months he couldn't memorize it.

25
00:03:08,000 --> 00:03:16,000
And story goes that in the past, in the past life he had been a good monk,

26
00:03:16,000 --> 00:03:20,000
but he had always looked down upon those monks and just disparaged those monks

27
00:03:20,000 --> 00:03:24,000
who couldn't memorize things.

28
00:03:24,000 --> 00:03:27,000
And land laugh about them and so on.

29
00:03:27,000 --> 00:03:29,000
And brag about it and so on.

30
00:03:29,000 --> 00:03:36,000
And so as a result he became dumb in his next life as a monk.

31
00:03:36,000 --> 00:03:40,000
And so he became quite discouraged.

32
00:03:40,000 --> 00:03:45,000
And his older brother figured that this monk was a dullard, was useless.

33
00:03:45,000 --> 00:03:49,000
And so he said, he said, look, for four months you've gained nothing.

34
00:03:49,000 --> 00:03:53,000
And he said, you're better off not to be a monk.

35
00:03:53,000 --> 00:03:55,000
And so the younger brother was quite dejected.

36
00:03:55,000 --> 00:04:00,000
But he didn't take his brother seriously until he heard that his brother was responsible for,

37
00:04:00,000 --> 00:04:02,000
his brother was responsible for organizing meals.

38
00:04:02,000 --> 00:04:09,000
So when they went to people's houses, the older brother would organize monks to go.

39
00:04:09,000 --> 00:04:16,000
And so one day this layperson, I think, I'm not a pindica, came to invite them to his house.

40
00:04:16,000 --> 00:04:21,000
And he said to the older brother, please, how many monks do you have?

41
00:04:21,000 --> 00:04:25,000
Bring all the monks together with the Buddha for tomorrow's meal.

42
00:04:25,000 --> 00:04:30,000
And the older brother said, well, my younger brother is not a real monk.

43
00:04:30,000 --> 00:04:34,000
So I'll accept the invitation for everybody but him.

44
00:04:34,000 --> 00:04:37,000
And the younger brother was sitting there and he heard that.

45
00:04:37,000 --> 00:04:39,000
And at that point he became quite dejected.

46
00:04:39,000 --> 00:04:42,000
And said, my brother says, I'm not a real monk.

47
00:04:42,000 --> 00:04:46,000
And so he decided that he better quit, he better disrobe.

48
00:04:46,000 --> 00:04:48,000
But the truth is he was happy being a monk.

49
00:04:48,000 --> 00:04:49,000
He didn't have a problem.

50
00:04:49,000 --> 00:04:55,000
He just, he and his brother had this idea that somehow you had to memorize.

51
00:04:55,000 --> 00:05:00,000
It may also been that his older brother was thinking that this would make him

52
00:05:00,000 --> 00:05:03,000
go to see the Buddha and the Buddha would give him a meditation object.

53
00:05:03,000 --> 00:05:05,000
Because that's what happened.

54
00:05:05,000 --> 00:05:08,000
The Buddha found out about this and saw him walking around,

55
00:05:08,000 --> 00:05:13,000
dejected or about to leave.

56
00:05:13,000 --> 00:05:16,000
And he went up to see him and he said, do much wrong.

57
00:05:16,000 --> 00:05:18,000
And he said, oh, I can't memorize even a single stanza.

58
00:05:18,000 --> 00:05:24,000
And my brother says, I'm useless and I should leave the leave and disrobe.

59
00:05:24,000 --> 00:05:27,000
And the Buddha said, that's not what makes a real monk.

60
00:05:27,000 --> 00:05:29,000
He said, you don't become a monk just for memorizing.

61
00:05:29,000 --> 00:05:33,000
He said, here, let me help you.

62
00:05:33,000 --> 00:05:34,000
He took a cloth.

63
00:05:34,000 --> 00:05:40,000
He took this white cloth, clean pure white cloth and he gave it over to him.

64
00:05:40,000 --> 00:05:45,000
And he said, go stand in the sun, hold this white cloth and say to yourself,

65
00:05:45,000 --> 00:05:51,000
and rub it, rub it between your hand and say,

66
00:05:51,000 --> 00:05:56,000
let Joe Haranang, let Joe Haranang, which means taking out the dirt,

67
00:05:56,000 --> 00:06:00,000
rubbing out the dirt.

68
00:06:00,000 --> 00:06:03,000
And he sent him off to practice this,

69
00:06:03,000 --> 00:06:05,000
meditations or repeating to himself.

70
00:06:05,000 --> 00:06:09,000
We're in Joe Haranang taking out the dirt, taking out the dirt.

71
00:06:09,000 --> 00:06:16,000
And then the Buddha went off to accept this invitation.

72
00:06:16,000 --> 00:06:20,000
Now, what happened was he went off into the sun

73
00:06:20,000 --> 00:06:22,000
and he's standing there.

74
00:06:22,000 --> 00:06:25,000
And of course, the sun comes up and it's getting hot and sweating.

75
00:06:25,000 --> 00:06:29,000
And as his hand start to sweat, the cloth becomes soiled,

76
00:06:29,000 --> 00:06:33,000
it becomes impure, it loses its white color.

77
00:06:33,000 --> 00:06:39,000
And he looks at this and he starts to become turned off, of course,

78
00:06:39,000 --> 00:06:42,000
because it's a beautiful white cloth and suddenly it starts to get ugly.

79
00:06:42,000 --> 00:06:44,000
And so he's standing there, saying,

80
00:06:44,000 --> 00:06:50,000
what taking out the dirt, taking out the dirt, and here's this cloth getting dirty right before his eyes.

81
00:06:50,000 --> 00:06:57,000
And as a result, it starts to open up his mind and his attachment to the cloth

82
00:06:57,000 --> 00:07:03,000
and the idea of purity starts to be loosened.

83
00:07:03,000 --> 00:07:06,000
So his mind starts to calm down as a result.

84
00:07:06,000 --> 00:07:10,000
The cravings in his mind start to go away.

85
00:07:10,000 --> 00:07:16,000
And the Buddha finds out about this, and the Buddha sends him this telepathic projection.

86
00:07:16,000 --> 00:07:20,000
So he sees suddenly, he sees the Buddha, and the Buddha says,

87
00:07:20,000 --> 00:07:48,000
it is Ragha, Ragha, Ragha, Ragha, Ragha, Ragha, Ragha, Ragha says,

88
00:07:48,000 --> 00:07:52,000
do anger is dirt and Moha is dirt.

89
00:07:52,000 --> 00:07:58,000
So these three things, and he helps him to look and then

90
00:07:58,000 --> 00:08:02,000
pull up under the looks inside, and he becomes an Ragha by listening to this teaching in the Buddha.

91
00:08:02,000 --> 00:08:07,000
The Buddha said, it's not this dirt on the cloth that we're talking about.

92
00:08:07,000 --> 00:08:11,000
The dirt that we want to take out is our cravings, our attachment.

93
00:08:11,000 --> 00:08:15,000
So attachment to the beauty of the cloth, attachment to the beauty of the body,

94
00:08:15,000 --> 00:08:19,000
and also helping to see the impurity in the body.

95
00:08:19,000 --> 00:08:23,000
And so as a result of seeing that, he became an Ragha.

96
00:08:23,000 --> 00:08:30,000
Now the Buddha was off at this invitation, and the Nathapindika came to offer the food.

97
00:08:30,000 --> 00:08:37,000
And he came over and he comes to offer this ceremonial water that they pour.

98
00:08:37,000 --> 00:08:40,000
They pour it when they give a gift.

99
00:08:40,000 --> 00:08:43,000
But the Buddha covered his bow with his hand and said,

100
00:08:43,000 --> 00:08:49,000
the Nathapindika, the Sangha is not complete.

101
00:08:49,000 --> 00:08:51,000
Not everyone is here.

102
00:08:51,000 --> 00:08:55,000
And the Nathapindika looked at Mahapantika and said,

103
00:08:55,000 --> 00:08:57,000
are all the monks here?

104
00:08:57,000 --> 00:09:01,000
Mahapantika said, all the monks are here?

105
00:09:01,000 --> 00:09:05,000
And the Buddha said, they're still a monk left in the monastery.

106
00:09:05,000 --> 00:09:10,000
And so Nathapindika sent a man to the monastery to find out.

107
00:09:10,000 --> 00:09:15,000
And the story goes that the Nathapindika, when he became an Raghanti,

108
00:09:15,000 --> 00:09:17,000
he also gained magical powers.

109
00:09:17,000 --> 00:09:19,000
So he had several.

110
00:09:19,000 --> 00:09:21,000
And also he gained knowledge of the Pitokas.

111
00:09:21,000 --> 00:09:24,000
So he gained quite an array of mental powers.

112
00:09:24,000 --> 00:09:31,000
He lost all of this cloudiness in the mind of the stupidity left him.

113
00:09:31,000 --> 00:09:36,000
He was no longer a dullard, and he was able to remember the whole of the Buddha's teaching.

114
00:09:36,000 --> 00:09:40,000
And he had ever heard the Buddha teaching, Buddha teach, suddenly it came back to him.

115
00:09:40,000 --> 00:09:45,000
When he became enlightened, because his mind became clear.

116
00:09:45,000 --> 00:09:47,000
And also he had these magical powers.

117
00:09:47,000 --> 00:09:49,000
So he knew it, his brother was saying.

118
00:09:49,000 --> 00:09:51,000
And he said, my brother said, there's no monks here.

119
00:09:51,000 --> 00:09:56,000
And suddenly, he made a resolve that the whole monastery should be full of monks.

120
00:09:56,000 --> 00:10:01,000
And there were thousands, suddenly there were a thousand monks filling Jaitawana at his monastery.

121
00:10:01,000 --> 00:10:02,000
Some of them were dying.

122
00:10:02,000 --> 00:10:03,000
They were closed.

123
00:10:03,000 --> 00:10:04,000
Some of them were washing.

124
00:10:04,000 --> 00:10:05,000
Some of them were sweeping.

125
00:10:05,000 --> 00:10:06,000
Some of them were meditating.

126
00:10:06,000 --> 00:10:07,000
Some of them were sitting.

127
00:10:07,000 --> 00:10:09,000
Some of them were doing walking.

128
00:10:09,000 --> 00:10:12,000
And this man came and saw all these monks.

129
00:10:12,000 --> 00:10:15,000
And he went back and he said, there's a thousand monks in the monastery.

130
00:10:15,000 --> 00:10:18,000
And to an attapindika, an attapindika told us to the Buddha.

131
00:10:18,000 --> 00:10:23,000
And the Buddha said, go and tell them that you want Tula-pantika.

132
00:10:23,000 --> 00:10:26,000
And so the man went back to Jaitawana.

133
00:10:26,000 --> 00:10:29,000
And he said, where is Tula-pantika?

134
00:10:29,000 --> 00:10:33,000
And all 1,000 of them said, I am Tula-pantika.

135
00:10:33,000 --> 00:10:35,000
I am Tula-pantika.

136
00:10:35,000 --> 00:10:37,000
And so he went back again.

137
00:10:37,000 --> 00:10:39,000
He told us, what to do?

138
00:10:39,000 --> 00:10:41,000
They're all Tula-pantika.

139
00:10:41,000 --> 00:10:47,000
So the whole point is to show his brother that he's not useless.

140
00:10:47,000 --> 00:10:54,000
To make clear what the truth is.

141
00:10:54,000 --> 00:10:58,000
And the Buddha said, the Buddha says, no, no, go and say it again.

142
00:10:58,000 --> 00:11:04,000
And the first one who says, I am Tula-pantika, grab onto his robe.

143
00:11:04,000 --> 00:11:06,000
And so he went back and he grabbed.

144
00:11:06,000 --> 00:11:08,000
And he said, where's Tula-pantika?

145
00:11:08,000 --> 00:11:10,000
And then they said, I am Tula-pantika.

146
00:11:10,000 --> 00:11:11,000
But he saw the one who said it first.

147
00:11:11,000 --> 00:11:13,000
And he grabbed him by the robe.

148
00:11:13,000 --> 00:11:14,000
And suddenly all the rest disappeared.

149
00:11:14,000 --> 00:11:15,000
And he said, please come.

150
00:11:15,000 --> 00:11:19,000
The Buddha is waiting for you.

151
00:11:19,000 --> 00:11:23,000
And so they invited him to go.

152
00:11:23,000 --> 00:11:30,000
And then after the meal, everyone was kind of not sure

153
00:11:30,000 --> 00:11:34,000
who is this guy, a real monk, or is he not a real monk?

154
00:11:34,000 --> 00:11:38,000
And the Buddha said, at the end of the meal,

155
00:11:38,000 --> 00:11:42,000
he said, Tula-pantika, please give the thanksgiving.

156
00:11:42,000 --> 00:11:43,000
So you give the talk after the meal.

157
00:11:43,000 --> 00:11:46,000
Because after they had a meal, they would always give a talk to them.

158
00:11:49,000 --> 00:11:51,000
And Tula-pantika and so the older brother and everyone was

159
00:11:51,000 --> 00:11:53,000
thinking, what's he going to talk about?

160
00:11:53,000 --> 00:11:55,000
He doesn't, you can't even remember a single stanza.

161
00:11:55,000 --> 00:11:57,000
And suddenly he gives this talk about everything the Buddha taught.

162
00:11:57,000 --> 00:12:01,000
And he goes through the whole of the three Tula-pantika.

163
00:12:01,000 --> 00:12:04,000
And everyone realizes, wow, something has changed

164
00:12:04,000 --> 00:12:07,000
that he has become in life.

165
00:12:07,000 --> 00:12:11,000
And the story goes on to explain why he became like he was.

166
00:12:11,000 --> 00:12:14,000
The story goes in the past, he was a king.

167
00:12:14,000 --> 00:12:18,000
And he had this idea of impurity.

168
00:12:18,000 --> 00:12:23,000
He was going on a voyage to the city as the king,

169
00:12:23,000 --> 00:12:27,000
going on a tour to see what was going on in the city.

170
00:12:27,000 --> 00:12:28,000
And it was very hot.

171
00:12:28,000 --> 00:12:29,000
And so he started sweating.

172
00:12:29,000 --> 00:12:31,000
And he wiped his face with his pure cloth.

173
00:12:31,000 --> 00:12:33,000
And he saw it get dirty.

174
00:12:33,000 --> 00:12:35,000
And he realized that this is the way with everything.

175
00:12:35,000 --> 00:12:38,000
Nothing stays pure and perfect.

176
00:12:38,000 --> 00:12:41,000
Everything is subject to change.

177
00:12:41,000 --> 00:12:44,000
And so that's what the Buddha saw when he gave him this cloth.

178
00:12:44,000 --> 00:12:50,000
He saw that in his past he had already used this meditation subject.

179
00:12:50,000 --> 00:12:52,000
So then the monks were talking about this.

180
00:12:52,000 --> 00:12:55,000
How could it be that he couldn't even memorize this single stanza?

181
00:12:55,000 --> 00:12:59,000
How could it be that he is able to make this refuge for himself

182
00:12:59,000 --> 00:13:03,000
and find the ultimate refuge and become free from suffering?

183
00:13:03,000 --> 00:13:06,000
Without even being able to memorize a single stanza.

184
00:13:06,000 --> 00:13:10,000
Buddha said, it's not by listening.

185
00:13:10,000 --> 00:13:11,000
It's not by memorizing.

186
00:13:11,000 --> 00:13:12,000
It's not by all this.

187
00:13:12,000 --> 00:13:15,000
The way a person is able to make a refuge for themselves

188
00:13:15,000 --> 00:13:20,000
and island for themselves is through four things,

189
00:13:20,000 --> 00:13:25,000
through effort, through heedfulness,

190
00:13:25,000 --> 00:13:28,000
through restraint and through training.

191
00:13:28,000 --> 00:13:31,000
And he said, this is how a wise one makes an island for themselves.

192
00:13:31,000 --> 00:13:33,000
And then he said the verse,

193
00:13:33,000 --> 00:13:47,000
which means precisely that, that these four things

194
00:13:47,000 --> 00:13:50,000
they'd want to make an island for themselves.

195
00:13:50,000 --> 00:13:54,000
So that's briefly the story.

196
00:13:54,000 --> 00:13:58,000
As to the meaning of it, this is a really important verse

197
00:13:58,000 --> 00:14:02,000
and one that is wonderful for us if you can remember to remember it.

198
00:14:02,000 --> 00:14:05,000
Or at least keep in mind what are these four things.

199
00:14:05,000 --> 00:14:10,000
Because even just reminding yourself of this teaching

200
00:14:10,000 --> 00:14:15,000
is a great use for us in meditation.

201
00:14:15,000 --> 00:14:19,000
The first one, Utanena, with effort.

202
00:14:19,000 --> 00:14:21,000
When we're able to put out the effort in the practice,

203
00:14:21,000 --> 00:14:23,000
this is necessary.

204
00:14:23,000 --> 00:14:26,000
This is the way that a person makes a refuge for themselves

205
00:14:26,000 --> 00:14:30,000
or makes an island of themselves.

206
00:14:30,000 --> 00:14:32,000
When a person...

207
00:14:32,000 --> 00:14:33,000
So what is it?

208
00:14:33,000 --> 00:14:35,000
First of all, what we mean by island here

209
00:14:35,000 --> 00:14:37,000
is we're talking about a person becoming enlightened.

210
00:14:37,000 --> 00:14:39,000
And why we mean island?

211
00:14:39,000 --> 00:14:40,000
And the Buddha uses this,

212
00:14:40,000 --> 00:14:43,000
similarly, of the island and the flood.

213
00:14:43,000 --> 00:14:46,000
So all beings,

214
00:14:46,000 --> 00:14:49,000
we understand Samsara is like a flood.

215
00:14:49,000 --> 00:14:51,000
There's like an ocean.

216
00:14:51,000 --> 00:14:53,000
And the ocean is drowning us.

217
00:14:53,000 --> 00:14:57,000
We're swimming about in this ocean aimlessly.

218
00:14:57,000 --> 00:15:00,000
The whole of the universe, the whole of Samsara,

219
00:15:00,000 --> 00:15:02,000
you can't find a purpose, you can't find an aim,

220
00:15:02,000 --> 00:15:04,000
whatever you aim for, whatever you go for,

221
00:15:04,000 --> 00:15:07,000
whatever you reach, whatever you attain,

222
00:15:07,000 --> 00:15:08,000
becomes meaningless.

223
00:15:08,000 --> 00:15:10,000
And it's like a carried away by the flood,

224
00:15:10,000 --> 00:15:13,000
whatever you build up, whatever great things,

225
00:15:13,000 --> 00:15:19,000
cities or palaces or learning that you gain at all disappears.

226
00:15:19,000 --> 00:15:21,000
Anything you remember eventually you forget,

227
00:15:21,000 --> 00:15:23,000
everything you create eventually is destroyed.

228
00:15:23,000 --> 00:15:26,000
So it's like an ocean and there's nothing to hold on to.

229
00:15:26,000 --> 00:15:29,000
There's nothing that is a secure refuge.

230
00:15:29,000 --> 00:15:31,000
You build up a refuge, it disappears.

231
00:15:31,000 --> 00:15:34,000
It leaves you without it.

232
00:15:34,000 --> 00:15:39,000
So the finding of the real refuge is like building an island

233
00:15:39,000 --> 00:15:43,000
or finding an island that the flood can't overcome.

234
00:15:43,000 --> 00:15:46,000
No matter how much the waves come or tsunami

235
00:15:46,000 --> 00:15:49,000
or any kind of tides of any sort,

236
00:15:49,000 --> 00:15:51,000
can't overcome this island.

237
00:15:51,000 --> 00:15:53,000
This is what we're all looking for in life.

238
00:15:53,000 --> 00:15:55,000
We're trying to find this stability and the security.

239
00:15:55,000 --> 00:15:59,000
The Buddha said the way to find this is by these four things.

240
00:15:59,000 --> 00:16:04,000
So Uthana means effort that we have to work hard.

241
00:16:04,000 --> 00:16:07,000
That this is the work that we see that Chulapanta

242
00:16:07,000 --> 00:16:10,000
could do that it wasn't effort and studying.

243
00:16:10,000 --> 00:16:17,000
It wasn't effort in arts or in being a monk or so.

244
00:16:17,000 --> 00:16:19,000
And it was effort in seeing the truth

245
00:16:19,000 --> 00:16:21,000
and in cleaning out the stains inside.

246
00:16:21,000 --> 00:16:26,000
So just this practice of meditation and then going through

247
00:16:26,000 --> 00:16:30,000
moment by moment, going through his mind, seeing the lust,

248
00:16:30,000 --> 00:16:33,000
seeing the anger, seeing the delusion in his mind

249
00:16:33,000 --> 00:16:36,000
and weeding it out, rooting it out.

250
00:16:36,000 --> 00:16:39,000
This is the effort that he put out.

251
00:16:39,000 --> 00:16:41,000
This is the effort that we all need.

252
00:16:41,000 --> 00:16:44,000
We have to do the work.

253
00:16:44,000 --> 00:16:46,000
So this is what it means to meditate.

254
00:16:46,000 --> 00:16:49,000
When we talk about meditation, some people they say

255
00:16:49,000 --> 00:16:52,000
being mindful doesn't require meditation.

256
00:16:52,000 --> 00:16:54,000
A person can just be mindful in daily life.

257
00:16:54,000 --> 00:16:56,000
And that's enough to become enlightened.

258
00:16:56,000 --> 00:17:03,000
And the truth is it's possible, but it's very, very difficult.

259
00:17:03,000 --> 00:17:05,000
It actually takes a lot of work

260
00:17:05,000 --> 00:17:09,000
and you have to actually do something to raise yourself up.

261
00:17:09,000 --> 00:17:11,000
People say that just naturally it comes.

262
00:17:11,000 --> 00:17:13,000
You can naturally become enlightened.

263
00:17:13,000 --> 00:17:15,000
And that's quite misleading and I think quite inaccurate.

264
00:17:15,000 --> 00:17:17,000
The person has to work.

265
00:17:17,000 --> 00:17:19,000
This is what we mean by the word practicing meditation.

266
00:17:19,000 --> 00:17:24,000
The word meditation means to apply yourself to the practice.

267
00:17:24,000 --> 00:17:26,000
The second one, a pamanda.

268
00:17:26,000 --> 00:17:30,000
Pamanda means the clarity of mind, the mindfulness.

269
00:17:30,000 --> 00:17:35,000
So not just pushing yourself to practice meditation,

270
00:17:35,000 --> 00:17:39,000
but actually bringing the mind back and guarding the mind,

271
00:17:39,000 --> 00:17:43,000
applying the mind to the object.

272
00:17:43,000 --> 00:17:46,000
When you see something clearly knowing that this is seeing,

273
00:17:46,000 --> 00:17:49,000
when you hear something knowing this is hearing,

274
00:17:49,000 --> 00:17:52,000
being with reality from moment to moment

275
00:17:52,000 --> 00:17:55,000
and not letting yourself step away.

276
00:17:55,000 --> 00:17:58,000
The mindfulness or what we call,

277
00:17:58,000 --> 00:18:01,000
what we call sati, reminding yourself,

278
00:18:01,000 --> 00:18:04,000
this act of reminding yourself of what's going on.

279
00:18:04,000 --> 00:18:06,000
This is what we mean by heedfulness.

280
00:18:06,000 --> 00:18:09,000
Being heedful, being aware of the object.

281
00:18:09,000 --> 00:18:12,000
The third one, sanyamena.

282
00:18:12,000 --> 00:18:15,000
Sanyamena means keeping the mind from getting distracted.

283
00:18:15,000 --> 00:18:19,000
So at the same time that we're watching the object,

284
00:18:19,000 --> 00:18:22,000
we have to watch ourselves when we get distracted.

285
00:18:22,000 --> 00:18:30,000
Sanyamena means, or it means keeping the mind within boundaries.

286
00:18:30,000 --> 00:18:34,000
So not letting the mind wander, not following after things that you see,

287
00:18:34,000 --> 00:18:35,000
or things that you hear.

288
00:18:35,000 --> 00:18:38,000
Not getting caught up in random thoughts or daydreams or so on.

289
00:18:38,000 --> 00:18:40,000
This is sanyamena.

290
00:18:40,000 --> 00:18:43,000
Keeping the mind restrained.

291
00:18:43,000 --> 00:18:46,000
And the fourth one that goes quite along with the sanyamena

292
00:18:46,000 --> 00:18:50,000
means very much the same thing as the main dhamma.

293
00:18:50,000 --> 00:18:52,000
But dhamma is more like training yourself,

294
00:18:52,000 --> 00:18:54,000
so that the mind doesn't run away,

295
00:18:54,000 --> 00:18:56,000
so that the mind doesn't get distracted.

296
00:18:56,000 --> 00:18:59,000
Sanyamena is guarding it, so it doesn't get distracted.

297
00:18:59,000 --> 00:19:03,000
Dhamma is the aspect of the practice that changes the mind,

298
00:19:03,000 --> 00:19:07,000
straightens the mind, so that the mind no longer wants to go away,

299
00:19:07,000 --> 00:19:10,000
wants to be distracted, no longer wants to follow after

300
00:19:10,000 --> 00:19:16,000
grid daydreams or addictions or desires or so.

301
00:19:16,000 --> 00:19:18,000
Basically this refers to the practice of meditation,

302
00:19:18,000 --> 00:19:20,000
put out the effort in the practice,

303
00:19:20,000 --> 00:19:25,000
and then apply yourself correctly in the practice.

304
00:19:25,000 --> 00:19:28,000
You guard yourself from getting distracted,

305
00:19:28,000 --> 00:19:33,000
and train yourself so that in the end you don't want to become distracted,

306
00:19:33,000 --> 00:19:37,000
so that your mind is focused and resonant,

307
00:19:37,000 --> 00:19:40,000
and intent upon the object.

308
00:19:40,000 --> 00:19:42,000
This is how really how julapantika,

309
00:19:42,000 --> 00:19:44,000
even though the text doesn't go into detail,

310
00:19:44,000 --> 00:19:46,000
this is how he became enlightened.

311
00:19:46,000 --> 00:19:49,000
Because when the Buddha started explaining to him

312
00:19:49,000 --> 00:19:53,000
what he was seeing this change that was going on in the clot,

313
00:19:53,000 --> 00:19:58,000
and also in his mind, when the mind started to lose its interest

314
00:19:58,000 --> 00:20:01,000
and its attachment to the beautiful clot,

315
00:20:01,000 --> 00:20:05,000
it was that it's the greed, it's the anger,

316
00:20:05,000 --> 00:20:09,000
it's the delusion that we're trying to change,

317
00:20:09,000 --> 00:20:12,000
that is changing here until eventually your mind

318
00:20:12,000 --> 00:20:14,000
simply knows the object what it is.

319
00:20:14,000 --> 00:20:18,000
So he applied himself to this with effort,

320
00:20:18,000 --> 00:20:21,000
with mindfulness, clearly where this is greed,

321
00:20:21,000 --> 00:20:23,000
this is anger, this is delusion,

322
00:20:23,000 --> 00:20:25,000
this is seeing, this is hearing,

323
00:20:25,000 --> 00:20:29,000
this is smelling, this is tasting, and so on.

324
00:20:29,000 --> 00:20:34,000
And guarding himself and training himself in this way,

325
00:20:34,000 --> 00:20:37,000
and that's the result he became in our heart.

326
00:20:37,000 --> 00:20:39,000
So just another small teaching,

327
00:20:39,000 --> 00:20:41,000
and one that's very important for us as meditators,

328
00:20:41,000 --> 00:20:44,000
something that we can all take to heart.

329
00:20:44,000 --> 00:20:47,000
So that's our teaching on the Dhammapanda for today.

330
00:20:47,000 --> 00:20:49,000
Thank you all for tuning in and listening,

331
00:20:49,000 --> 00:20:50,000
and for joining us today,

332
00:20:50,000 --> 00:21:08,000
where we can go back to our practice.

