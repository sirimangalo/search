Hello everyone, welcome to our Saturday broadcast.
So as usual, we'll start with 15 minutes of silent meditation during which time people
who have questions can post their questions.
We've also created a channel on our YouTube section of our Discord server.
So if people want to ask questions there, they can post questions in the ask channel
throughout the week and we'll try to get to them next week.
So for the future, you don't have to be listening or watching this live.
You can post your questions and get them answered.
So as usual, we're prioritizing questions related to meditation practice, questions that
have a great import, great importance to the spiritual welfare of the people asking.
So related to one's own practice and related to the practice of mindfulness meditation.
So 15 minutes starting now.
Alright, so we'll stop there with some meditation, move right into the Q&A.
So if you have questions, you can feel free to post them.
If you don't have questions, just sit back and stay mindful with us.
Thank you, Bantay.
We do have questions.
I've become very apathetic and tired of my ambitions.
Can you please advise on how to deal with disenchantment of worldly pleasures?
Disenchantment is an absence of enchantment.
There's nothing to deal with.
What you have to assess is what you're actually experiencing.
Experience disenchantment is actually the path to freedom.
When you lose your intoxication with impermanent things, your mind becomes stronger and
more free.
But when you have negative feelings towards them, that's a form of clinging.
So you have to be clear that there's a difference there.
If it's true apathy, I guess, means lack of pathos, lack of caring about interest in ambition.
There's nothing to deal with there.
That's a great success.
I'm vision, of course, is all about the future and it's deals in concepts, so ideas and
goals and the attainment and the enjoyment of attainment of goals and so on, achievements.
So it's all caught up with greed, anger, delusion, there's nothing good with ambition.
If you have disenchantment with worldly pleasures, there's nothing to deal with.
Now, it can be awkward, I suppose.
We're going to address the awkwardness of the disconnect between how you're accustomed
to living your life and what you know to be worthwhile and not worthwhile, so when the
way you live your life starts to feel pointless, so dealing with that, it's quite simple,
but it's important to recognize that the disconnect is, well, that the answer is to give
out to change, to recognize that there's something wrong with the way you live your life,
not something wrong with the way your mind is, which can sometimes be the case where
we feel like there's something wrong with us, why am I not keen on ambition, why am I
not caught up in sensual pleasures, why can I no longer find joy in the pleasures of life
may they simple or complex, and that's not the right question, the right, the answer
is to focus on changing your life, changing the way you live your life to fit what you
know is right and wrong, which is not as useful and useless.
While meditating this week, I started getting waves of euphoria, throughout my whole body
that lasted about three hours, it felt good, but I don't understand why it happened, do
you know what causes this?
Yeah, things feeling good, has no connection with wisdom, which is why you don't have
understanding in regards to it, and there's confusion and doubt and wonder and just ignorance
about it, so we encourage a emphasis on clarity and mindfulness as opposed to things like
states of euphoria, euphoria, we call it BD, it's a form of what we call BD, or the rapture
they sometimes call it like Christians will rock back and forth or experience waves of euphoria
like that, even in Christian religion and of course Hindu religion, it's a common thing
and most religions that practice some form of meditation, so something feeling good has no
reference to understanding or wisdom, so and therefore they're not related, there's the subtle
implication in your words that some other something good about the fact that it felt
good, and there is nothing good with the fact that it felt good, that's just a feeling,
it's impermanence offering, non-self, there's no actual satisfaction involved, so it's
important that you note both the feeling and your desire for it or attachment to it liking
of it, as for your actual question, and personally that's not what our focus is in Buddhism,
understanding the cause of things, what we're interested in is the causal relationship in
the present moment to see the cause and effect relationship between your experiences, this
good feeling and how you react to the good feeling, or not interested in past causes, not
in the same way, so if you want to understanding about it, it's not, the important question
isn't why it happened, the important question is release, what is it and how do I react
to it, how am I perceiving it, how am I reacting, and what are the consequences of my reactions
and that sort of thing?
My mind is often focused on the mantra, rather than the experience, instead of my mind
being at the stomach, it is actually with the mantra, is this a sign of wrong practice?
Well, technically not, but the feeling that you get, you have to be clear what it is
that you're actually experiencing, there may be a sense of tension involved or concentration
like a forcing or a fixing of the mind, there can also be visual seeing the words or hearing
the words in your mind and you should not seeing or hearing, but you can't actually
be with the object because it ceases, noting is something that happens right after,
it's a means of affirming a objective reaction to the experience as opposed to reacting
with judgment or partiality, so instead of liking or disliking, you remind yourself, and
also during that time, of course, you're focused on the reminder, but after that moment,
if you're obsessing over it or going over it, again, you have to see what's going on there,
that's not the actual mantra, that's the mind, there can also be worrying about the fact
that maybe the actual meta-analysis, oh, I am focused on the mantra more than the experience
and that's something you should be mindful of, your analyzing of the process and your doubting
about whether you're doing it right and worrying about whether you're doing it right
and that sort of thing, don't be concerned with the fact that the mantra is a reminder,
it's a state of mind that is certain about what the experience was and that's important,
it's a certainty, it creates a clarity, it puts the mind on the right course.
As the statement, we have no control and exception in the present moment, otherwise,
how would it make sense to say, try to see clearly, or in what way can you try something
without control?
Yeah, what control is the ability to dictate the presence or absence of experiences and that's
not what we do when we try to see clearly, we're not trying to dictate anything.
We are setting the mind in a certain way that is similar to how we set the mind in any way.
For example, when you try to control, that's a certain type of mindset, when you try to
see clearly, it's a different type of mindset and that's what is certainly possible in the
present moment, what is not possible is affecting a change in your experience, as in turning
something off, turning something on, making something stay, making something go, regulating
in your experiences, that sort of thing that's control, but trying to see clearly as well
within the realm of possibility, of course, as we can see.
Sometimes while I'm meditating, I fall asleep, but somehow I keep meditating while dreaming.
I say to myself, dreaming, is there somewhere I can read serious texts about this?
There's nothing really to read about, it's a dream, and there may be some potential
to be mindful, but there can also just be the imitation of it based on habit that the mind
continues even when you're not really mindful.
Some people have what they call lucid dreams, which apparently they can actually be mindful
during them, but there's nothing really to read about, you're either mindful or you're
not, and most of the time when you're asleep, you're not really that mindful.
How do we approach thoughts that cause us to react with fear or anxiety?
With thoughts, don't cause that, and this is a key in meditation, and what is mindfulness
practice, that the thoughts themselves have no power to cause that, however, our habits
have the power to cause that, so we have the habitual inclination to respond in certain
ways with fear or anxiety, so mindfulness is about changing those habits, and that's
really the answer, is change your habit so that when you experience those things that
you feel trigger fear and anxiety, you respond in a new way, you respond in a more objective
way by reminding yourself, okay, it's just thinking, and that's what we do with our meditation
practice.
I'd recommend if you haven't read our booklet, read the booklet, if you're interested,
you can do an at home course and learn how to practice in this tradition.
It's all the information on our website.
There is a colleague at work who saps my positive energy, I'm finding it hard to use this
as a lesson.
Can you offer any advice?
Well, yeah, there's a few things here, I mean, a person, another person's potential
to sap, anything is suspect at best, mostly what the actual experience is, for these
kinds of descriptions, is that the experiences that you have in regards to your colleague,
which are still limited to seeing, hearing, smelling, tasting, feeling, and thinking,
right, very objective experiences, are triggers for reactions in you that may be create
negative states of mind, harmful states of mind, states of mind that sap your energy,
but the whole idea of sapping positive energy has another problem.
There's some concern we might have over the idea of clinging to something that you
call positive energy as though, right, the perception that positive energy is somehow
a good thing, a necessary thing or an important thing.
There really is no positive or negative energy, there is energy, and being energetic
is generally a positive thing, as long as there's not too much of it, but if it's about
feeling positive about things, like having a sense that everything's going to turn out
all right and everything is good and being happy, that's unsustainable and uncontrollable
and it's untenable as a religious practice, because it's unpredictable, it's certainly
possible for some people to go most of their lives with only positive experiences and
a sense of positivity, but it's also quite likely that most people will have to be disconnected
from that, that ability to feel positive when horrible things happen, right, some people
go through life without horrible things happening while lucky for them, but it's just
luck and it's not sustainable, maybe in this life, if you get lucky, but not over the
long term life after life, and certainly not when it comes to death, where you have to come
to terms with your state of mind, one thing about positivity is it can lead to laziness,
complacency, and also attachment to sensuality, because of course positivity is tied up
with pleasure and happiness, and if you're even happy about simple things, it can lead
to liking and attachment, so it's important to recognize that rather than blaming other people
for moments where you might not feel so positive, stop trying to feel positive, and start
trying to see clearly whatever it is you do feel, you'll find it's much more natural,
much more sustainable, much more peaceful because it can of course get quite stressful whenever
your positivity is threatened and your ability to feel positive is threatened.
What about practicing mindfulness as you have taught but not using any labeling mantra while
doing so?
How can you ensure that you are going to be doing that if you're not using the mantra
because that's exactly what the mantra is for?
It's like saying without practicing, what if I would just become enlightened?
It's kind of the problem there is that what exactly are you doing to create this state
of just being aware of walking when walking because without it, it's much more likely
that you fall into the idea that I am walking, and that's a problem, meaning not just
using the words, but there actually is a sense of self because you can't cut through
that without a strong clarity of mind, there is a sense of attachment to it, positivity
or negative, negativity liking or disliking, and there is a general often sense of
conceit, so comparing experiences of other experiences and comparing yourself with others
and so on.
You can't cut through that without a strong clarity of mind and that's what mantra meditation
is for. I mean, mantra meditation is basically the go-to type of meditation in so many
different traditions.
So this idea that you could just be mindful without it, it's just sounds really great,
but has no basis in actual history of meditation practice or actual practice, it just
leads to a vague sense that somehow you're focused and you can get quick focused, but
no strength of clarity of mind that comes from being precise and concrete and certain about
what it is you're actually experiencing. You just get lost, you follow after ideas and
perceptions and narratives and so on because there's no grounding in actual experience.
That's what the mantra, that's what mantras do, both in samata practice, envipasana practice,
mantra meditation is of course very useful for the cultivation of tranquility, but it's most
useful in tranquility and I would say actually more important in tranquility or important
in for another reason because of how much easier it is to get lost in samata practice without
a rudder without a guide post without a solid foundation of the mantra because you're
dealing in concepts and so the use of a mantra, not only strength in samata practice but
also stabilizes it and keeps you from getting lost in what you want where you want to
go and getting lost and some of the craziness that can come up from in the conceptual realm
because concepts are infinite. But in mipasana it's equally important, it's a stabilizer
and it creates certainty and veracity in your objective experience of reality.
Is there such a thing as the dark night of the soul in Buddhism? I hear lots of people talking
about this on various forums and it's putting many people off meditation.
Yeah, no, there isn't, there is not and there is one teacher who's gotten quite a bit of,
I don't know if it's a bit of a bit of publicity about his teachings on that and there
are people who follow his teachings and so power to them for their ideas and power in
the sense that they're welcomed to that but it is not orthodox and I consider it to be
problematic from my own perspective like in our tradition because the stages of knowledge
that they're talking about should be liberating, they should be a positive experience.
So the emphasis of the negative potential negative experiences which of course can be a part
of practice is I think detrimental. The focus on those aspects of it prevents the sense
of liberation. Now, of course, there's arguments to be made on either side, I'm sure
that there are many arguments against this stance but I don't think there's any canonical
or reasonable, reasonably Buddhist source for that teaching and the idea behind it is that
in the middle of your practice it can be quite difficult but it's not the actual practice
that's difficult, that's just an observation of mostly what a beginner goes through in meditation
practice or maybe that's not quite fair but what would go through when we are confronted
with the hindrances, those things that are hindrances to our practice, the knowledge
themselves are not dark, they're not a delving into some unpleasant reaches of the
mind, they are challenging but liberating and the process of insight, progress of insight
should feel liberating, it should add every stage when done properly, it should feel
like a positive experience, it should feel like an improvement, a liberation, one step closer
to no longer have a new experience, dark nights. And of course, the use of the word
soul which I don't think is a part of the vocabulary of this group that I've heard but
the dark night, the soul of course has no place there but the idea behind the dark night
is just that I think the recognition that it can be challenging
is noting a form of thought construction and does this lead to becoming and rebirth because
the meditator is trying to control the experience by labeling it rather than just watching
it.
So basically this is a refutation of the entirety of the teaching that I've been taught
and that I teach in the form of a question, the answer of course is no and you should
know that it's going to be no because of course this is what I teach so if I were to say
yes I would have to give up everything that I've been taught and everything that I teach.
Maybe a better question and a more honest question would be how can you defend the
noting when it appears to be a thought construction and I believe thought construction
leads to becoming and rebirth because with thought constructions the meditator is trying
to control the experience or with labeling the meditator is trying to control the experience
which is patently false that's like saying a person who uses a mantra in some meditation
is trying to control the experience it's just ridiculous that is in no way what a mantra
is for so the idea that noting in any way is trying to control any experiences is way
out of left field like there's no basis for it whatsoever it's just not how a mantra
of works not what it's for is it a thought construction yeah maybe I don't know what that
word exactly means but it's something like that it's artificial and that's why a lot
of people don't like it but meditation is artificial the Buddha said if you want to
if you want to get a tree that is leaning the wrong way to lean the other way you need
a rope you can't just well let's say push it in the other direction you need something
artificial it's not you're not strong enough to just say I'm gonna lean in the other
direction I'm gonna incline towards nibana no the mind is inclining in the wrong direction
you need something external something artificial that's what meditation is it's an
artifice and that rubs people the wrong way they think it should be more natural let's
just bias really there is no basis for that meditation is a artifice something you add
to the equation that disrupt it's disruptive it's disruptive in the way a rope is disruptive
if of elephant is stuck in the mud you use a rope to pull it out so mindfulness is
like the it's the artifice the the thing you introduce but that's like that's that way
with all with all mantras there's no controlling as far as just watching the only the
only being who can just watch in in the sense that I suppose you're you're assuming you're
referring to is an aran so the practice can never be that that's the goal now the question
is what you what do you do to reach that goal where you're just watching and the implication
by that is without judging right without reacting without misunderstanding with with a clear
and wise understanding of the experience and and that's not something you can just tell yourself
to do okay I'm going to be wise about this experience it's like I'm going to be enlightened
when I when I experience seeing I'm going to have an enlightened perception of it it doesn't
work that that would be self that would be control so I think to some extent meant that people
go into meditation that you're the sort that you're inferring exists apart from this practice
they go into that with a sense of control and and and and inclination to try and control
their their minds which which mindfulness is it doesn't do mindfulness is not trying to control
it's a but acknowledging recognizing and reminding it's much more stable much more
tenable whatever whatever much much more viable as a solution yeah much more pure and you
perhaps explain more about the john is how can one really know that one has entered john
stages well john is something that means meditation now the Buddha talked about certain let's
say categories of meditative attainment so your mind does improve does progress it
gets it becomes more refined as you as you practice any type of meditation now the thing
is there's two different paths of meditation one that is based on concepts and leads
only to tranquility and the other which is based on ultimate reality and leads to wisdom
and enlightenment but either way there is the refinement of the mind and so these are stages
that one goes through and would often talk about stages that appear to be somewhat of a precursor
to actual inside actual repass and a practice actual wisdom so those would be trans states
that can be very powerful but not actually conducive to with not actually leading to wisdom
themselves so conducive actually but only if you put them to work later how can one really
know that one is entered john is stages well it's not something I really focus on with
my students where much more focused on yeah now which means knowledge so wisdom is taking
Valyrian route often used as an herbal sleep aid breaking the fifth precept I don't
know I don't think so if it's something that just makes you tired that's not likely to
be considered breaking the fifth precept breaking the fifth precept is those things that lead
to an altered state of mind alcohol is the most obvious because it's really just a poisoning
of the mind really impair it's a mental impairment and then pairs some important faculties
of the mind especially like ability to discern right for wrong and just the clarity it's
very hard if not impossible to be mindful once you've started drinking alcohol so if
it's something that impairs your ability to be mindful potentially I mean there's an issue
with for example sleep aids and it's because it doesn't it it masks the issues behind your
inability to sleep and your need to sleep your desire to sleep your fear of not sleeping all
those things are kind of masked and and ignored by taking things like Valyrian or whatever
I don't really know what that is so it's much much much more healthy for you much more
fruitful much more valuable for you to gain a better perspective I was just talking to my
father this morning and he I didn't know that he had problem sleeping and I guess he's just he
doesn't sleep much he's getting older but he said sometimes he practices my practice is watching
the rising and the falling to go to sleep and he said it's really incredibly good cure for
insomnia so we kind of agreed there but he just made that observation and he's not he's
only doing basic meditation practice and it's already seeing the benefits it I can't stress
enough how true that is that practice of mindfulness it it's not magic in the sense that it
just poof like it's like a pill that you take and suddenly your insomnia is gone it shows
you that your your your approach to insomnia is just ridiculous that that we're completely
missing the point missing we're completely misunderstanding the state that we call insomnia mindfulness
just shows you that and once you straighten that out as I oh it wasn't what I thought it was
and that thing that I thought was a problem isn't actually a problem and it's like poof suddenly
insomnia is no longer a thing how does the virtue gained from meditation compared with
virtue from non meditative practice like five precepts weren't non meditative practices
responsible for instant realization during the time of Buddha no it's not possible I mean
cut her and think about what you're asking see the five precepts are not a thing they don't
exist so what is it you mean by the five precepts the five precepts are in a practice now keeping
the five precepts can be thought of as a practice but still what is the actual practice what is
it that you're talking about let's suppose at the moment let's let's first talk about a person
who makes a vow to keep the five precepts so at that moment there is a positive wholesome
intention to keep the five precepts now is that going to lead to enlightenment how how is that
going to help you see the five the four noble truths the four noble truths there's just no
cause a relationship there there's no reasonable explanation as to why that there is a connection
there are there is a catalytic I don't know as a catalyst so the five precepts of course are
supportive because of the wholesome states of mind that they entail even just the wholesome
state of mind of intending to keep the precepts and then there's the moment where you keep the
five precepts like you want to kill and you don't kill there can be many reasons for that you
can decide not to kill because of fear because of a sense of obligation because you've
made a promise that that sort of thing and none of that really has any direct connection
with enlightenment either now there can be the case where you see clearly the experience
and as a result where where you would have liked to have killed before through wisdom
you don't kill and so that practice can be fairly directly related to enlightenment but
your question is a little simplistic and I really don't really mean to be condescending
or critical of it but you have to you have to understand it on a deeper level which
means understandable Buddhism is not simple there are stages and there are a there's
a place for the five precepts and they play a part just like somewhat to meditation plays
a part and you have to understand the part that things play so that you don't put them
out of place or make them out to be more or less than they are as for non-minitative
practice being responsible for instant realization how would that be possible again how
what is the reasonable explanation that that makes a causal relationship that points
out the causal relationship between one and the other in the time of the Buddha people
became enlightened fairly quickly well to put it lightly but that is said to be because
they were ready they had cultivated lifetime after lifetime the necessary practices to
be at the point where all they really had to do is be nudged in the right direction
and the Buddha was an expert at nudging people pretty much reminding them of things
they had lost track of as a result of being born as a human being is it normal we might
have to sorry we might have to end a little early so just we don't just stop quite yet
but maybe one or two more if there's any tier one this is one like that okay is it normal
along the path of a non practitioner to break the five precepts I try but I fail to keep
them well it can be that that sometimes there's a overly strict interpretation of the five
precepts you have to be clear about what you're actually promising for example if someone
asks you something you don't have to tell them the truth you just can't lie what that
means is you can stay quiet you can be even misleading without actually breaking the precepts
sometimes it can actually be wholesome to find ways to not have to tell people the truth
or of course lie to them but outright lying is of course breaking the precept that's often
the hardest one for some people not tricking drugs now call can be hard if they're an addict
I guess I would say try and prick up the practice of mindfulness it really helps you with
both of those and really with all five of them is it normal for a non-practitioner so there is no
path of a non-practitioner in Buddhism if you are a non-practitioner you are not a Buddhist
and is it normal for such people to break the five precepts sure so if you consider yourself to be
a non-practitioner then I don't have anything to say to you you're not on the path that we would
consider to be spiritually beneficial now I'm assuming that that's actually probably not the
case that you are at least interested or trying to or inclined towards an actual practice of some sort
but it may mean that you don't actually do meditation practice well there's no time like the
present and mindfulness is such a great thing that you can practice anytime but it has to be said
that either you're practicing or you're not and if you're not then yeah breaking the five precepts
is par for the course not easy to keep them for some people some people are naturally inclined
just because of their past life habit but it's rare
I keep on today those are the questions we're prepared to ask today okay thank you for your help
Chris if there's anybody else out there in our community who wants to help out we we're short
today we had no one to help us so we can always use more volunteers but thank you all for your
questions and I hope the answers were helpful so have a good week see you all next week
it's hard
