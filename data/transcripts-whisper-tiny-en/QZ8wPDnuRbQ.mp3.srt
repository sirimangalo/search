1
00:00:00,000 --> 00:00:02,860
Okay, I heard a dumbah talk about the middle way.

2
00:00:02,860 --> 00:00:07,040
Professor suggested the middle way should be applied to Buddhism itself.

3
00:00:07,040 --> 00:00:11,160
My G E G, not taking precepts too seriously.

4
00:00:11,160 --> 00:00:15,200
Alcohol now and again, not meditating too much, et cetera, et cetera, et cetera.

5
00:00:15,200 --> 00:00:17,400
That's not the middle way, by the way.

6
00:00:17,400 --> 00:00:19,560
That's the halfway.

7
00:00:19,560 --> 00:00:22,880
It's quite different, quite different.

8
00:00:22,880 --> 00:00:24,920
There is no halfway.

9
00:00:24,920 --> 00:00:29,200
Halfway is pointless, useless.

10
00:00:29,200 --> 00:00:30,800
You can think about it.

11
00:00:30,800 --> 00:00:41,400
If something is the right thing to do and brings benefit,

12
00:00:41,400 --> 00:00:45,520
well, you might say that you have to pace yourself.

13
00:00:45,520 --> 00:00:52,280
But if alcohol is wrong, what good could be

14
00:00:52,280 --> 00:00:56,160
could come of indulging in it once in a while.

15
00:00:56,160 --> 00:01:02,920
I think the intention here is good.

16
00:01:02,920 --> 00:01:06,200
The idea of not pushing yourself too hard.

17
00:01:06,200 --> 00:01:09,800
For example, forcing something on you.

18
00:01:09,800 --> 00:01:15,080
If you meditate too much without proper appreciation

19
00:01:15,080 --> 00:01:19,960
of the meditation practice, if you don't really want to meditate,

20
00:01:19,960 --> 00:01:24,000
meditating a lot is only so beneficial.

21
00:01:24,000 --> 00:01:27,120
If you're not really interested in doing something,

22
00:01:27,120 --> 00:01:28,520
it's not really so beneficial.

23
00:01:28,520 --> 00:01:33,280
So sometimes it's good to appreciate things

24
00:01:33,280 --> 00:01:34,640
before you engage in them.

25
00:01:34,640 --> 00:01:36,200
It's simply pushing yourself.

26
00:01:36,200 --> 00:01:38,800
It is not a great benefit.

27
00:01:38,800 --> 00:01:42,560
And sometimes you want to explore the reasons

28
00:01:42,560 --> 00:01:46,000
for doing things and the reasons for giving up things.

29
00:01:46,000 --> 00:01:49,360
For example, some people have claimed beneficial results

30
00:01:49,360 --> 00:01:52,440
from getting drunk or drinking alcohol

31
00:01:52,440 --> 00:01:56,640
in the sense that as meditators, it helps them realize

32
00:01:56,640 --> 00:01:58,960
how ridiculous it is to drink alcohol.

33
00:01:58,960 --> 00:02:01,560
If they were to just blindly accept the precept,

34
00:02:01,560 --> 00:02:03,880
then they wouldn't really understand why it's awful.

35
00:02:03,880 --> 00:02:09,880
It's the theory.

36
00:02:09,880 --> 00:02:19,200
And so the intention, the idea that somehow one should

37
00:02:19,200 --> 00:02:24,320
not blindly race ahead into keeping all of these rules

38
00:02:24,320 --> 00:02:27,720
or becoming a monk or doing intensive meditation,

39
00:02:27,720 --> 00:02:30,120
I think the intention there is a good one.

40
00:02:30,120 --> 00:02:40,360
But the problem is it's a conflation of this

41
00:02:40,360 --> 00:02:44,000
with another concept, and that is of regression

42
00:02:44,000 --> 00:02:51,120
or the degradation of the mind.

43
00:02:51,120 --> 00:02:52,640
Because every time you drink alcohol,

44
00:02:52,640 --> 00:02:56,280
you degrade your state of mind, your awareness,

45
00:02:56,280 --> 00:02:58,320
your clarity of mind.

46
00:02:58,320 --> 00:03:01,440
A meditator, what someone who is serious in meditation

47
00:03:01,440 --> 00:03:05,760
will be revolted at the very thought of drinking alcohol.

48
00:03:05,760 --> 00:03:14,720
So the idea that the middle way could somehow

49
00:03:14,720 --> 00:03:17,800
be alcohol now, and then is absurd to such a person.

50
00:03:17,800 --> 00:03:21,720
Just any alcohol whatsoever is just a revolting thought.

51
00:03:21,720 --> 00:03:25,280
It's, say, the whole one's whole being revolts

52
00:03:25,280 --> 00:03:29,200
against the idea, killing, stealing, lying, cheating,

53
00:03:29,200 --> 00:03:36,640
and drugs and alcohol, totally given up, 100%.

54
00:03:36,640 --> 00:03:39,520
So the middle way is not halfway.

55
00:03:39,520 --> 00:03:43,480
And in those senses, some people who

56
00:03:43,480 --> 00:03:46,840
will say, I've heard one man say, engaging in

57
00:03:46,840 --> 00:03:50,760
karmic, tantric sex really helped him

58
00:03:50,760 --> 00:03:55,080
to come to terms with sexuality.

59
00:03:55,080 --> 00:03:58,680
And I would say, I thought about that for quite a while.

60
00:03:58,680 --> 00:04:00,920
And it's a little bit difficult to refute,

61
00:04:00,920 --> 00:04:02,480
even though you deep down, you know,

62
00:04:02,480 --> 00:04:04,280
you know, there's something wrong with that.

63
00:04:04,280 --> 00:04:08,800
And what's wrong with it is that at the moment of indulging

64
00:04:08,800 --> 00:04:13,480
in sexuality, you're cultivating attachment

65
00:04:13,480 --> 00:04:15,440
to pleasure and addiction and so on.

66
00:04:15,440 --> 00:04:18,200
You're cultivating these pleasurable states.

67
00:04:18,200 --> 00:04:21,840
You're encouraging them.

68
00:04:21,840 --> 00:04:24,200
And that has an effect on the mind.

69
00:04:24,200 --> 00:04:25,800
It deteriorates the state of mind.

70
00:04:25,800 --> 00:04:28,960
So what helps in the dealing with sexuality

71
00:04:28,960 --> 00:04:34,680
is the objective observation of the sexual urge

72
00:04:34,680 --> 00:04:38,760
when it arises, the sexual urge, the pleasurable sensations

73
00:04:38,760 --> 00:04:45,440
and so on, the physical stimulus.

74
00:04:45,440 --> 00:04:50,760
But the actual intention to engage,

75
00:04:50,760 --> 00:04:55,280
in, for example, tantric sex or sex in general,

76
00:04:55,280 --> 00:04:59,080
is going to degrade one's state of mind.

77
00:04:59,080 --> 00:05:11,280
And the cultivation of the desires and the passion actually

78
00:05:11,280 --> 00:05:13,640
hurts one's clarity.

79
00:05:13,640 --> 00:05:15,040
It creates.

80
00:05:15,040 --> 00:05:17,720
And if one is truly mindful, one can't engage in it.

81
00:05:17,720 --> 00:05:23,440
One can't give rise to sexual urges as being mindful.

82
00:05:23,440 --> 00:05:24,920
This is what you see when you start to be mindful,

83
00:05:24,920 --> 00:05:29,720
is that as soon as you cultivate clear awareness,

84
00:05:29,720 --> 00:05:32,880
the urge disappears.

85
00:05:32,880 --> 00:05:35,240
Suddenly, it's no longer because this

86
00:05:35,240 --> 00:05:38,760
is what I mean by saying you can fundamentally

87
00:05:38,760 --> 00:05:46,480
misunderstand reality, or misinterpret reality.

88
00:05:46,480 --> 00:05:48,720
And all that takes is a moment of clarity

89
00:05:48,720 --> 00:05:52,360
for you to see things suddenly like a veil

90
00:05:52,360 --> 00:05:53,520
was lifted from your eyes.

91
00:05:58,120 --> 00:06:05,800
And so, for example, this sexuality

92
00:06:05,800 --> 00:06:09,200
and the example of drugs and alcohol,

93
00:06:09,200 --> 00:06:14,520
cannot be indulged by someone who is truly mindful.

94
00:06:14,520 --> 00:06:25,000
The middle way is quite different from a halfway

95
00:06:25,000 --> 00:06:26,280
taking things halfway.

96
00:06:26,280 --> 00:06:30,440
And it's totally, totally different.

97
00:06:30,440 --> 00:06:33,400
It's actually, and I've talked about this

98
00:06:33,400 --> 00:06:38,280
as far as actually not really the concept of being

99
00:06:38,280 --> 00:06:44,560
in the middle is not really a core doctrine,

100
00:06:44,560 --> 00:06:45,880
in the sense that it's not something

101
00:06:45,880 --> 00:06:55,200
that the Buddha insisted upon or reiterated frequently.

102
00:06:55,200 --> 00:06:56,880
It's not something that the Buddha taught

103
00:06:56,880 --> 00:06:59,360
on a very frequent basis.

104
00:06:59,360 --> 00:07:02,800
It happened to be quite famous because it was the first discourse.

105
00:07:02,800 --> 00:07:07,320
But one important point that you have to keep in mind

106
00:07:07,320 --> 00:07:09,120
is the Buddha's audience at the time.

107
00:07:09,120 --> 00:07:12,600
He was dealing with people who were torturing themselves,

108
00:07:12,600 --> 00:07:19,320
who were doing something that was leading them

109
00:07:19,320 --> 00:07:22,080
to an extreme state.

110
00:07:22,080 --> 00:07:27,600
And so he framed his teaching on the formal truths,

111
00:07:27,600 --> 00:07:29,320
which he couldn't just go into teaching

112
00:07:29,320 --> 00:07:31,640
the formal truths right away.

113
00:07:31,640 --> 00:07:35,320
They weren't in a position to appreciate

114
00:07:35,320 --> 00:07:38,760
the path that he was going to explain,

115
00:07:38,760 --> 00:07:41,600
until he pointed out that the state that they were in

116
00:07:41,600 --> 00:07:44,480
was an extreme state.

117
00:07:44,480 --> 00:07:50,280
So the middle way, first of all, just this,

118
00:07:50,280 --> 00:07:52,880
is that it was specifically directed

119
00:07:52,880 --> 00:07:56,760
toward these five ascetics and towards adjusting their minds,

120
00:07:56,760 --> 00:07:58,400
because they were not like any of us.

121
00:07:58,400 --> 00:08:00,800
And they were totally extreme.

122
00:08:00,800 --> 00:08:04,040
They were hardcore ascetics, torturing themselves,

123
00:08:04,040 --> 00:08:07,040
not eating and so on.

124
00:08:07,040 --> 00:08:09,160
And so the Buddha was explaining that this

125
00:08:09,160 --> 00:08:11,760
was an extreme to them.

126
00:08:11,760 --> 00:08:16,520
And then going into what was the more important Buddhist

127
00:08:16,520 --> 00:08:20,480
doctrine and that everything that arises ceases,

128
00:08:20,480 --> 00:08:24,240
so that there is no happiness to be found

129
00:08:24,240 --> 00:08:31,280
in the objects of experience that it is all dukkha,

130
00:08:31,280 --> 00:08:36,000
and it's all suffering that the first noble truth

131
00:08:36,000 --> 00:08:42,120
is to see and to experience things as they are and to give them up.

132
00:08:42,120 --> 00:08:45,880
Now what it means the middle way is quite clear.

133
00:08:45,880 --> 00:08:51,480
It's the avoiding of two states,

134
00:08:51,480 --> 00:08:56,680
and that is indulgence and repression,

135
00:08:56,680 --> 00:09:00,280
or rather than repression, because that's a Western word.

136
00:09:00,280 --> 00:09:09,400
You might say rejection, because this is really

137
00:09:09,400 --> 00:09:10,600
what asceticism was all about.

138
00:09:10,600 --> 00:09:14,160
It was the rejection of what they saw as an extreme state,

139
00:09:14,160 --> 00:09:18,640
the state of hedonism, of sensual indulgence,

140
00:09:18,640 --> 00:09:21,320
which totally corrupts the mind.

141
00:09:21,320 --> 00:09:23,040
So they thought, well, if that corrupts the mind,

142
00:09:23,040 --> 00:09:24,920
then you have to go to the other way

143
00:09:24,920 --> 00:09:29,040
and beat it out of yourself, torturing yourself.

144
00:09:29,040 --> 00:09:35,880
If happiness and pleasure is what leads to defilement,

145
00:09:35,880 --> 00:09:41,760
then the opposite torture must be what leads to enlightenment,

146
00:09:41,760 --> 00:09:45,000
not actually intentionally causing suffering on yourself.

147
00:09:45,000 --> 00:09:47,720
The middle way is the avoidance of both of these things,

148
00:09:47,720 --> 00:09:51,800
or in a Western sense, the avoidance of indulgence and repression,

149
00:09:51,800 --> 00:09:54,000
not indulging in things, but not repression,

150
00:09:54,000 --> 00:09:57,200
simply experiencing them for what they are,

151
00:09:57,200 --> 00:10:01,640
is the best way to understand the middle way.

152
00:10:01,640 --> 00:10:03,800
Now, the practice of the middle way,

153
00:10:03,800 --> 00:10:08,160
we only get to the middle way when we are totally extreme

154
00:10:08,160 --> 00:10:12,840
in our practice, in the sense of practicing

155
00:10:12,840 --> 00:10:14,840
to the utmost degree.

156
00:10:14,840 --> 00:10:22,720
So getting to the point where everything is practice,

157
00:10:22,720 --> 00:10:28,000
where every moment of our waking life or every moment of our lives,

158
00:10:28,000 --> 00:10:30,800
day and night, is meditative,

159
00:10:30,800 --> 00:10:34,080
is the cultivation of clear awareness.

160
00:10:34,080 --> 00:10:36,560
That's what leads to enlightenment.

161
00:10:36,560 --> 00:10:38,200
And so it's actually quite extreme.

162
00:10:38,200 --> 00:10:43,320
It's not a halfway moderate path.

163
00:10:43,320 --> 00:10:48,440
It's the middle way, which is a very power,

164
00:10:48,440 --> 00:10:50,600
it's the eight fold noble way.

165
00:10:50,600 --> 00:10:54,280
The middle way is that way.

166
00:10:54,280 --> 00:11:00,240
It has to do with our reaction and our interaction

167
00:11:00,240 --> 00:11:02,520
with the experience.

168
00:11:02,520 --> 00:11:06,760
That's what is meant by the middle way, not a lifestyle.

169
00:11:06,760 --> 00:11:08,240
It has nothing to do with a lifestyle.

170
00:11:08,240 --> 00:11:10,640
We should pick the lifestyle, which allows us

171
00:11:10,640 --> 00:11:16,400
to give up everything but this reaction to experience.

172
00:11:16,400 --> 00:11:18,480
And so you can see how ridiculous it would be

173
00:11:18,480 --> 00:11:21,520
to say alcohol and moderation, because it's totally

174
00:11:21,520 --> 00:11:22,520
in applicable.

175
00:11:22,520 --> 00:11:25,680
The middle way has nothing to do with drinking alcohol

176
00:11:25,680 --> 00:11:26,720
or not drinking alcohol.

177
00:11:26,720 --> 00:11:28,960
It has to do with the experience of reality,

178
00:11:28,960 --> 00:11:34,200
which is obviously hindered by the taking of alcohol.

179
00:11:34,200 --> 00:11:36,680
It's just a misunderstanding, because people

180
00:11:36,680 --> 00:11:40,520
tend to intellectualize Buddhism and try

181
00:11:40,520 --> 00:11:43,360
to incorporate it into their own delusions

182
00:11:43,360 --> 00:11:48,200
and their own lives, which are very much caught up

183
00:11:48,200 --> 00:11:53,200
in some sorrow, as opposed to trying to become free from some sorrow.

