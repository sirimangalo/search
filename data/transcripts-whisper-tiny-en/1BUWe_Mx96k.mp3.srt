1
00:00:00,000 --> 00:00:22,600
Good evening everyone from broadcasting live sometime in April.

2
00:00:22,600 --> 00:00:35,600
Today's quote is an important quote, so it doesn't say a lot.

3
00:00:35,600 --> 00:01:05,000
If you take the night when the guitar guitar became enlightened to

4
00:01:05,000 --> 00:01:17,400
an excelled, self enlightenment, yanchat, and the rakting an Upadhisi, saya, nibbana, dhatuya,

5
00:01:17,400 --> 00:01:33,000
very nibbaya, and the night of his becoming fully unbound through the element of unbinding

6
00:01:33,000 --> 00:01:39,000
that is without remainder.

7
00:01:39,000 --> 00:01:48,600
So the night when he passed away.

8
00:01:48,600 --> 00:01:57,840
So the body sat after making a determination at the foot of the Buddha Deepankara.

9
00:01:57,840 --> 00:02:11,600
So many countless number of yans and world periods ago made this determination and entered

10
00:02:11,600 --> 00:02:24,200
on the path to become a Buddha, and he spent four sangaya, four uncountable periods of time,

11
00:02:24,200 --> 00:02:31,600
not just one uncountable period of time, but four periods of time that have some meaning,

12
00:02:31,600 --> 00:02:43,600
but really can't measure them for just too long, and then a hundred thousand makapah, makapah,

13
00:02:43,600 --> 00:02:49,600
great yans on top of that.

14
00:02:49,600 --> 00:02:52,600
So this is being born and die again and again.

15
00:02:52,600 --> 00:02:58,800
So we have all these stories, only a very small number of stories, but there are a number

16
00:02:58,800 --> 00:03:08,680
of useful stories about the time the Buddha spent from birth to birth, perfecting the qualities

17
00:03:08,680 --> 00:03:14,400
of mind needed to become Buddha.

18
00:03:14,400 --> 00:03:23,200
And then he was born in his last life as Siddhartha, Kottama, and at the age of twenty-nine

19
00:03:23,200 --> 00:03:33,600
he left home, having seen that in life there are certain inevitabilities, we have to get

20
00:03:33,600 --> 00:03:38,800
old, we have to get sick, we have to die.

21
00:03:38,800 --> 00:03:43,360
And no matter what we do, no matter how we succeed in life, no matter whether we're good

22
00:03:43,360 --> 00:03:52,040
or bad, right or wrong, rich or poor, famous or infamous, in the end these things come

23
00:03:52,040 --> 00:03:57,320
to us all and they wash away everything we've done.

24
00:03:57,320 --> 00:04:06,400
And so he left home to try and find a way out of this mess, and for six years he tortured

25
00:04:06,400 --> 00:04:11,760
himself and tried all different meditations and ways of practicing, but he found that none

26
00:04:11,760 --> 00:04:21,000
of them led to freedom, they only led him to places he'd been before from birth to birth.

27
00:04:21,000 --> 00:04:30,640
And so finally he cleared his mind enough to be able to understand that he had to find

28
00:04:30,640 --> 00:04:40,080
the middle way, and to find an ordinary way, a natural way, where he was accepting of pain

29
00:04:40,080 --> 00:04:48,920
but also accepting of pleasure, and where he was able to see things objectively, and

30
00:04:48,920 --> 00:04:58,120
he looked at things for what they are, not with any goal or ulterior motive, not with

31
00:04:58,120 --> 00:05:01,040
any reaction.

32
00:05:01,040 --> 00:05:07,240
And so he began to see his mind began to clear and he saw his past lives and he saw

33
00:05:07,240 --> 00:05:14,360
the rising and ceasing of beings being born here and born there, finally he was able

34
00:05:14,360 --> 00:05:23,520
to make sense of everything and he saw how our birth and our life and our death is just

35
00:05:23,520 --> 00:05:47,000
part of a much greater system of a cycle of rebirth.

36
00:05:47,000 --> 00:05:53,040
And then he began to look at what causes the cycle of rebirth and he saw his own mind

37
00:05:53,040 --> 00:06:01,320
giving rise to attachment, giving rise to desire, giving rise to further rebirth.

38
00:06:01,320 --> 00:06:09,080
And as he saw the truth, he came to let it go, and when he let it go he was freed for himself,

39
00:06:09,080 --> 00:06:10,080
he became Buddha.

40
00:06:10,080 --> 00:06:14,720
So that's the night of his enlightenment, though a lot of stories of the Buddha stopped

41
00:06:14,720 --> 00:06:21,000
there and they're only interested in that time, that day, but that's the day the first

42
00:06:21,000 --> 00:06:23,760
day he's talking about.

43
00:06:23,760 --> 00:06:29,240
That's an important day, but it's funny how a lot of stories of the Buddha stopped there

44
00:06:29,240 --> 00:06:34,280
and say, well that was most important, end of story.

45
00:06:34,280 --> 00:06:42,160
It's actually not, and that part of the story is actually the less important part.

46
00:06:42,160 --> 00:06:52,080
If that was it, then you'd think, wow, okay, so he became Buddha, big deal, right?

47
00:06:52,080 --> 00:07:05,840
The other day, he is 45 years later, he lay down between the two solid trees, our flowering

48
00:07:05,840 --> 00:07:12,080
tree in northern India, actually all throughout India, there even in Sri Lanka, who

49
00:07:12,080 --> 00:07:21,120
is even one in, at least one that I know of in Thailand, but it's in a colder area.

50
00:07:21,120 --> 00:07:30,480
And he lay down and entered into finally the body passed away, and with the passing of

51
00:07:30,480 --> 00:07:38,360
the body there was no more arising of mental constructs, so I mean no more rebirth.

52
00:07:38,360 --> 00:07:50,920
Which is also a very important day, and those two together really describe what it is that

53
00:07:50,920 --> 00:07:59,440
were, I mean not even aiming for, but it's the ultimate, you know, because Buddhism

54
00:07:59,440 --> 00:08:07,000
is all about letting go, it's about freeing yourself from suffering.

55
00:08:07,000 --> 00:08:12,040
And sometimes scares people, I think, when they think of the Buddha's enlightenment, the

56
00:08:12,040 --> 00:08:21,720
Buddha's final freedom, I think, wow, you know, like never coming back, right?

57
00:08:21,720 --> 00:08:26,320
So it's not really our goal, because most people aren't interested in that, it's hard

58
00:08:26,320 --> 00:08:33,640
for us to think of course we don't want that, we want to be born again for the most part.

59
00:08:33,640 --> 00:08:37,320
And though we may say, oh man, I don't want to be born again, I have to go through this,

60
00:08:37,320 --> 00:08:43,800
but still we have desire, and so still we'll be reborn again, until we attain freedom

61
00:08:43,800 --> 00:08:50,040
from all that, and then we're not born again, but at the point is that's the end, that's

62
00:08:50,040 --> 00:08:55,280
the final, final letting go, the final freedom, until that point you're still not free,

63
00:08:55,280 --> 00:08:58,520
that's the point.

64
00:08:58,520 --> 00:09:01,920
And so it's not that we have to wish for it, but that's where you're going.

65
00:09:01,920 --> 00:09:05,920
Eventually, you let go of more, and so you wherever you're born, you're born in a good

66
00:09:05,920 --> 00:09:11,240
way, you let go of even more, and you're born in the even better way, until you let

67
00:09:11,240 --> 00:09:16,040
go of everything, and you're born inside it, you know, then you're not more, there's

68
00:09:16,040 --> 00:09:21,920
no suffering, no having to come back.

69
00:09:21,920 --> 00:09:26,840
So those two are two important days, but this quote is important because it talks about

70
00:09:26,840 --> 00:09:31,640
something entirely different, it talks about those two as the boundary.

71
00:09:31,640 --> 00:09:37,440
And it places emphasis on what happened in between those two, which as I say, it's a shame

72
00:09:37,440 --> 00:09:43,600
that most stories about the Buddha miss entirely.

73
00:09:43,600 --> 00:09:49,880
Part of the reason why they do, of course, is because that part of the Buddha's life

74
00:09:49,880 --> 00:09:53,720
has unfortunately been minimized.

75
00:09:53,720 --> 00:10:02,720
The Buddha spent 45 years teaching, 45 years, the Thai version of what's now come to

76
00:10:02,720 --> 00:10:07,200
be claimed to be his teachings, takes up 45 volumes.

77
00:10:07,200 --> 00:10:12,840
So they joke, it's like he made a volume a year, but it's here, it's spent 45 years and

78
00:10:12,840 --> 00:10:19,200
there's so much that we have of his teachings.

79
00:10:19,200 --> 00:10:24,760
And then you have the Lotus Citro, which I've been talking about quite a bit recently,

80
00:10:24,760 --> 00:10:28,000
trivializing it.

81
00:10:28,000 --> 00:10:35,760
And so you have a lot of later Buddhas in trivializing that, saying what he taught there,

82
00:10:35,760 --> 00:10:48,640
that was just conditional, I can't remember the word, there's a word for it, it was

83
00:10:48,640 --> 00:10:57,280
just, it was a part of the path.

84
00:10:57,280 --> 00:11:02,240
He wasn't teaching, in fact, it says he wasn't teaching the truth, basically.

85
00:11:02,240 --> 00:11:10,040
He was teaching an expedient means something that was not really true, but if you practice,

86
00:11:10,040 --> 00:11:16,080
if you follow it, it will help you see clear enough to see the whole truth.

87
00:11:16,080 --> 00:11:25,960
Anyway, there's generally a marginalization, but for our purposes, those 45 years are

88
00:11:25,960 --> 00:11:29,040
most important.

89
00:11:29,040 --> 00:11:33,360
And the question is, do we have what the Buddha taught during these times, do we still

90
00:11:33,360 --> 00:11:39,320
have it, do we know what the Buddha actually taught?

91
00:11:39,320 --> 00:11:55,000
The Buddha said, the young Aetas Ming and Taray Basati, lepati, nidisati, those, what, whatever

92
00:11:55,000 --> 00:12:06,600
Aetas Ming between these two, and Taray these two boundaries, Basati said, said, lepati

93
00:12:06,600 --> 00:12:20,440
is spoken, nidisati is pointed out, sambangtang tat hai wahoti, nohang tah.

94
00:12:20,440 --> 00:12:40,040
All of that is thus, just thus, tat hai wahoti, it is just as thus, just in that way, no

95
00:12:40,040 --> 00:12:45,240
Anyatta, not in another way, not otherwise.

96
00:12:45,240 --> 00:12:55,560
So whatever he says, it's true, it's thus, the smart tat hai wahoti wahoti, thus he is called

97
00:12:55,560 --> 00:13:05,520
the tat hai gata, tat hai means tas gata means gone, nyat hai wahoti bekeve tat hagat

98
00:13:05,520 --> 00:13:18,200
tat hagahoti, there's something else but he says, as he speaks, so he does, tat hagahoti

99
00:13:18,200 --> 00:13:25,040
tat hai wahoti, as he does, so he speaks, the smart tat hagata tat hagata tat hagata

100
00:13:25,040 --> 00:13:35,000
tat hagata tat hagata tat, but the question, sorry, relates to that, the quote, all

101
00:13:35,000 --> 00:13:40,520
of what he taught was truth for 45 years, so the question is, what do we have?

102
00:13:40,520 --> 00:13:42,160
Do we have that truth?

103
00:13:42,160 --> 00:13:48,880
Some people say, it's been changed, it's been altered, it's been recreated by

104
00:13:48,880 --> 00:13:55,680
sectarians, everybody are giving that they have the truth, so it's hard to be sure

105
00:13:55,680 --> 00:13:58,200
do you have the truth?

106
00:13:58,200 --> 00:14:04,000
It means a bold claim here, it's a great thing that the Buddha, great that the Buddha

107
00:14:04,000 --> 00:14:08,880
actually taught the truth, the question is, do we have the truth now?

108
00:14:08,880 --> 00:14:12,880
Where do we find the truth?

109
00:14:12,880 --> 00:14:22,320
And the curious thing that I find which gives great faith in what we claim to be the

110
00:14:22,320 --> 00:14:29,560
Buddha's teaching for the most part in a way, is that if you take a practice like the

111
00:14:29,560 --> 00:14:41,880
one we practice, which I have perfect faith in it, I mean, there's no question that it

112
00:14:41,880 --> 00:14:47,680
is objective, you can argue what you want, you can say this practice is dumb or wrong

113
00:14:47,680 --> 00:14:56,320
or misguided, but you can't say that it's partial or biased, not exactly, unless you

114
00:14:56,320 --> 00:15:05,640
want to say it's biased in a pedantic sort of way or it's overly technical or so on,

115
00:15:05,640 --> 00:15:07,680
but it's systematic and it's objective.

116
00:15:07,680 --> 00:15:16,280
When you sit and say, pain, pain, there's no bias there, you're seeing pain and pain,

117
00:15:16,280 --> 00:15:24,960
you're reminding yourself that's pain, when you say rise, there's no bias here, right?

118
00:15:24,960 --> 00:15:30,400
So what I'm trying to say is this is an objective practice, the practice of objectivity.

119
00:15:30,400 --> 00:15:35,400
You can just like it, you can say, it's not for me, it's not going to lead me anywhere,

120
00:15:35,400 --> 00:15:37,600
but you can't argue that it's not objective.

121
00:15:37,600 --> 00:15:43,400
And the interesting thing is, if you practice this way without any instruction in the

122
00:15:43,400 --> 00:15:52,040
greater context of the Buddha's teaching, you practice diligently for some time, you can

123
00:15:52,040 --> 00:15:58,360
go and read all the Buddha's teaching and you can find for yourself the truth, I would

124
00:15:58,360 --> 00:15:59,360
argue.

125
00:15:59,360 --> 00:16:05,600
I mean, what I mean to say is you react differently to different kinds of teaching.

126
00:16:05,600 --> 00:16:11,200
If you focus on objectivity, you focus on a practice that it's objective, not a practice

127
00:16:11,200 --> 00:16:20,040
that focuses on some God or some angel or some like love or compassion, you focus on truth.

128
00:16:20,040 --> 00:16:21,040
This is pain.

129
00:16:21,040 --> 00:16:25,080
If you say to yourself, pain, pain, you'll see, you can argue what you want, but you're

130
00:16:25,080 --> 00:16:29,880
seeing pain and pain, and you're telling yourself, pain and pain is that bias?

131
00:16:29,880 --> 00:16:30,880
No.

132
00:16:30,880 --> 00:16:32,480
It's an arbitrary no.

133
00:16:32,480 --> 00:16:38,240
So then when you go and look at any kind of teaching, you don't need someone to tell

134
00:16:38,240 --> 00:16:42,040
you that that's the truth.

135
00:16:42,040 --> 00:16:48,320
You can look at it, you can say, that is true.

136
00:16:48,320 --> 00:16:53,560
You can tell the difference between a teaching, that is, just saying good, saying nice

137
00:16:53,560 --> 00:17:03,960
things, saying things that are just sophism or intellectualism, versus teachings that

138
00:17:03,960 --> 00:17:06,960
actually teach something important.

139
00:17:06,960 --> 00:17:07,960
You can tell the difference.

140
00:17:07,960 --> 00:17:13,240
So it's an interesting thing as you react to teachings.

141
00:17:13,240 --> 00:17:18,360
Like if you read the diptica, for example, without having practiced this kind of meditation,

142
00:17:18,360 --> 00:17:20,960
I think you have a hard time with it.

143
00:17:20,960 --> 00:17:37,840
I think it's hard to find it even interesting, let alone comprehend, comprehendible, understandable.

144
00:17:37,840 --> 00:17:52,680
Meditation is that powerful, it shows the importance, the purity of meditation, the depth,

145
00:17:52,680 --> 00:18:11,080
the profundity of it, that it allows you to see, allows you to see in your life problems

146
00:18:11,080 --> 00:18:21,080
that you had, it allows you to see through them, to solve them, without doubt, and teaching's

147
00:18:21,080 --> 00:18:28,080
views, it allows you to see right view from wrong view, without bias, without doubt.

148
00:18:28,080 --> 00:18:35,680
You practice in this way, and that really goes for all sorts of kinds of practice.

149
00:18:35,680 --> 00:18:39,600
If you practice in that way, you'll see in that way.

150
00:18:39,600 --> 00:18:43,840
So that's why it's important that our practice be objective, our practice will be based

151
00:18:43,840 --> 00:18:48,320
on objectivity, because if you practice in a different religious tradition, you'll look

152
00:18:48,320 --> 00:18:52,280
at the world in a different way, and you'll gravitate towards different texts.

153
00:18:52,280 --> 00:18:55,480
You practice Christianity, you'll gravitate towards the Bible, you'll say, oh yes, the

154
00:18:55,480 --> 00:19:01,120
Bymos, because it resonates with you, but it resonates with you, because you become

155
00:19:01,120 --> 00:19:07,600
partial and biased, and you've taken up views and beliefs.

156
00:19:07,600 --> 00:19:18,960
So this is my test for a text, is see through the lens of objectivity, where you don't

157
00:19:18,960 --> 00:19:28,080
take any belief, you'll see things as they are, see pain as pain, see thoughts as thoughts,

158
00:19:28,080 --> 00:19:43,120
see anger as anger, see greed as greed, see delusion as delusion, anyway.

159
00:19:43,120 --> 00:19:46,480
Then you can see the truth, then you can find the belief that you're not relates to the

160
00:19:46,480 --> 00:19:59,240
other quotes that we've had, you know, who sees the dhamma sees the dhamma, if you don't

161
00:19:59,240 --> 00:20:04,320
see the dhamma, you can't see the Buddha, you can't find the Buddha's teaching, even

162
00:20:04,320 --> 00:20:09,600
you have the whole of the tepitika, you'll never find the teaching there, not unless

163
00:20:09,600 --> 00:20:16,040
you're seeing the dhamma, not unless you're practicing the dhamma, you can read all,

164
00:20:16,040 --> 00:20:22,640
you can memorize the 45 volumes and you still won't come close to the truth, but as soon

165
00:20:22,640 --> 00:20:33,000
as you practice, it's like it opens, it's like it's a cipher that deciphers the code suddenly

166
00:20:33,000 --> 00:20:38,680
you understand, otherwise people I think would find it boring, maybe interesting intellectually

167
00:20:38,680 --> 00:20:55,240
but incomprehensible. Anyway, that's our quote for tonight. Anybody have any questions?

168
00:20:55,240 --> 00:21:07,440
We're doing text questions again, so welcome to ask. You guys can go. Two meditators

169
00:21:07,440 --> 00:21:18,240
here, we can say hi to our meditators. I don't know if the Buddha is staying here today.

170
00:22:37,440 --> 00:22:44,800
All right, good night everyone. Thanks for tuning in.

