1
00:00:00,000 --> 00:00:27,000
Good evening everyone, welcome to evening dumber.

2
00:00:27,000 --> 00:00:37,000
We've finished talking about the purifications.

3
00:00:37,000 --> 00:00:54,000
I thought we'd go back to go over the Satipatanas of the discourse on foundations and mindfulness.

4
00:00:54,000 --> 00:01:16,000
So what we're practicing is insight meditation which is the insight part is a lot of what we've gone over in the past days.

5
00:01:16,000 --> 00:01:20,000
But it's based on the four foundations of mindfulness.

6
00:01:20,000 --> 00:01:26,000
So the actual practice we do is mindfulness.

7
00:01:26,000 --> 00:01:32,000
And when presented it's quite a simple practice.

8
00:01:32,000 --> 00:01:41,000
Remind yourself when you see something you remind yourself seeing.

9
00:01:41,000 --> 00:01:51,000
When you feel pain you remind yourself pain, pain.

10
00:01:51,000 --> 00:01:56,000
When you have thoughts you remind yourself thinking, thinking.

11
00:01:56,000 --> 00:02:02,000
Emotions you remind yourself liking or disliking, disliking, disliking.

12
00:02:02,000 --> 00:02:06,000
And that's your mantra you repeat it to yourself.

13
00:02:06,000 --> 00:02:10,000
That's the practice.

14
00:02:10,000 --> 00:02:12,000
There's not a lot more to it.

15
00:02:12,000 --> 00:02:18,000
I mean practically speaking that it's really all you need.

16
00:02:18,000 --> 00:02:22,000
But intellectually to help us understand why are we doing this.

17
00:02:22,000 --> 00:02:28,000
To help give us confidence and to remind us to be mindful.

18
00:02:28,000 --> 00:02:33,000
There's always a lot of explanation we can do.

19
00:02:33,000 --> 00:02:43,000
We can give and reassurance through through explanation.

20
00:02:43,000 --> 00:02:48,000
Giving talks to meditators is often just about reminding them.

21
00:02:48,000 --> 00:02:53,000
Giving them encouragement rather than actually imparting any kind of information.

22
00:02:53,000 --> 00:02:56,000
There's not a lot of information you need.

23
00:02:56,000 --> 00:03:04,000
Sometimes there are reminders and I need things to be pointed out to you.

24
00:03:04,000 --> 00:03:13,000
I certainly need to be reassured.

25
00:03:13,000 --> 00:03:20,000
So the Satipatanas who gives us the, it lays out the practice.

26
00:03:20,000 --> 00:03:27,000
In some detail about how to practice the four Satipatanas.

27
00:03:27,000 --> 00:03:34,000
It starts by telling where the Sutta was taught in the Kuru, the land of the Kuru.

28
00:03:34,000 --> 00:03:42,000
Which is interesting but only from an intellectual academic point of view.

29
00:03:42,000 --> 00:03:46,000
What's useful is the next section where the Buddha says,

30
00:03:46,000 --> 00:03:51,000
Aka Anoyan Bikoui Mughal.

31
00:03:51,000 --> 00:04:01,000
This is the Aka Anamanga.

32
00:04:01,000 --> 00:04:07,000
A lot has been said and argued about this phrase.

33
00:04:07,000 --> 00:04:11,000
It's quite often translated as the only way.

34
00:04:11,000 --> 00:04:15,000
This is the only way.

35
00:04:15,000 --> 00:04:18,000
I suppose literally it doesn't actually mean that.

36
00:04:18,000 --> 00:04:25,000
But it does appear to be in some way what the Buddha is saying.

37
00:04:25,000 --> 00:04:29,000
Where all the language might not be so strong as saying.

38
00:04:29,000 --> 00:04:32,000
There's no other way than this.

39
00:04:32,000 --> 00:04:37,000
In another place the Buddha did use such language to talk about the Eightfold Path.

40
00:04:37,000 --> 00:04:38,000
He said,

41
00:04:38,000 --> 00:04:45,000
He said,

42
00:04:45,000 --> 00:04:49,000
This is the Path, there is no other.

43
00:04:49,000 --> 00:04:55,000
Even that could be interpreted and interpreted differently.

44
00:04:55,000 --> 00:05:02,000
It's hard to imagine though that anyone could become enlightened without practicing mindfulness.

45
00:05:02,000 --> 00:05:08,000
If you want to see you have to look without looking you can't see.

46
00:05:08,000 --> 00:05:11,000
Without seeing you can't know.

47
00:05:11,000 --> 00:05:17,000
So it's looking in some form or another if you're not paying attention.

48
00:05:17,000 --> 00:05:19,000
If you're not present,

49
00:05:19,000 --> 00:05:30,000
it's hard to imagine how you could possibly become enlightened to cultivate understanding about reality.

50
00:05:30,000 --> 00:05:36,000
The commentary offers several definitions of aka and aka in the manga.

51
00:05:36,000 --> 00:05:38,000
What this means.

52
00:05:38,000 --> 00:05:45,000
The first one just means that it's one way.

53
00:05:45,000 --> 00:05:50,000
Not quite clear what that means.

54
00:05:50,000 --> 00:05:52,000
A single way.

55
00:05:52,000 --> 00:05:59,000
It's not a double way.

56
00:05:59,000 --> 00:06:02,000
The Buddha didn't teach many ways.

57
00:06:02,000 --> 00:06:05,000
He only taught one way.

58
00:06:05,000 --> 00:06:08,000
So it's not a question whether there might be other ways.

59
00:06:08,000 --> 00:06:13,000
But there is only this one way or this way is only one way.

60
00:06:13,000 --> 00:06:17,000
It's not either do this or do that.

61
00:06:17,000 --> 00:06:19,000
It really is just be mindful.

62
00:06:19,000 --> 00:06:28,000
The way of the Buddha is the way of being mindful or else the way of mindfulness is just one way.

63
00:06:28,000 --> 00:06:35,000
But then it gives them alternative definitions.

64
00:06:35,000 --> 00:06:38,000
I mean that one actually doesn't mean a lot.

65
00:06:38,000 --> 00:06:45,000
It's aka and a manga because one has to practice for oneself.

66
00:06:45,000 --> 00:06:52,000
It says it could mean that it's the way it has to be practiced by one.

67
00:06:52,000 --> 00:06:55,000
No one can practice for you.

68
00:06:55,000 --> 00:07:04,000
Unlike other spiritual traditions where you can be saved by others.

69
00:07:04,000 --> 00:07:20,000
Buddhist soteriology is you must save yourself.

70
00:07:20,000 --> 00:07:25,000
You must find freedom for yourself.

71
00:07:25,000 --> 00:07:32,000
No one can change your mind and medicines and drugs.

72
00:07:32,000 --> 00:07:39,000
A lot of people argue that psychedelic drugs are going to be a support for the practice.

73
00:07:39,000 --> 00:07:42,000
Not really.

74
00:07:42,000 --> 00:07:48,000
There are things that can support other people can support you on your path.

75
00:07:48,000 --> 00:08:04,000
But in the end it's got to be, you've got to practice on your own.

76
00:08:04,000 --> 00:08:10,000
Or alternatively it's the way of the one, meaning it's the way of the Buddha.

77
00:08:10,000 --> 00:08:14,000
You won't find this way anywhere else.

78
00:08:14,000 --> 00:08:22,000
It's kind of remarkable that no one else ever taught mindfulness the way the Buddha did.

79
00:08:22,000 --> 00:08:26,000
Remarkable I guess because now it's everywhere.

80
00:08:26,000 --> 00:08:29,000
Everyone is talking about mindfulness.

81
00:08:29,000 --> 00:08:35,000
I don't know if anyone could claim that there were other teachers besides the Buddha who ever taught.

82
00:08:35,000 --> 00:08:41,000
It's an interesting point when people try to say that all religions are basically the same.

83
00:08:41,000 --> 00:08:48,000
Well, did anyone ever get so far as to teach mindfulness besides the Buddha?

84
00:08:48,000 --> 00:08:52,000
Did they ever see Jesus Christ? Is there anything in the Bible?

85
00:08:52,000 --> 00:08:58,000
The Christian Bible, anything in the Quran?

86
00:08:58,000 --> 00:09:05,000
I don't even think you see that sort of thing in any other Asian religions.

87
00:09:05,000 --> 00:09:12,000
Thinking about Hinduism, all the Upanishads, is there anything about mindfulness?

88
00:09:12,000 --> 00:09:17,000
The Jainism, the Jainism?

89
00:09:17,000 --> 00:09:20,000
It's an interesting question.

90
00:09:20,000 --> 00:09:25,000
Of course we would generally just dismiss it and say of course not none of these people were Buddhas.

91
00:09:25,000 --> 00:09:31,000
And that's interesting because we don't normally associate that with the Buddhas omnipotent.

92
00:09:31,000 --> 00:09:38,000
Or the Buddha's great wisdom. Yes, it was mindfulness that was the great thing that the Buddha.

93
00:09:38,000 --> 00:09:41,000
I mean, it's a part of the great thing that the Buddha taught.

94
00:09:41,000 --> 00:09:49,000
It's the practice that the Buddha realized was important.

95
00:09:49,000 --> 00:09:52,000
The Buddha realized was the path.

96
00:09:52,000 --> 00:10:08,000
So it's the way of the Buddha.

97
00:10:08,000 --> 00:10:14,000
Or it just means it is the way that goes in one direction, a kang ayatin.

98
00:10:14,000 --> 00:10:22,000
It only leads to nibhana and you practice this and it doesn't lead anywhere else.

99
00:10:22,000 --> 00:10:27,000
Practicing mindfulness couldn't lead you to hell, for example.

100
00:10:27,000 --> 00:10:32,000
Mindfulness couldn't give any bad result.

101
00:10:32,000 --> 00:10:39,000
Unlike other potential, they hold some states like effort, concentration.

102
00:10:39,000 --> 00:10:45,000
This is why it's so hard to talk about wrong mindfulness is because mindfulness is always good.

103
00:10:45,000 --> 00:11:06,000
Mindfulness only leads in the right direction, only leads to good things.

104
00:11:06,000 --> 00:11:09,000
And so what are these good things that it leads to?

105
00:11:09,000 --> 00:11:13,000
I think I might talk about it quite a bit and I know my teacher does,

106
00:11:13,000 --> 00:11:17,000
but I think Buddhists in general aren't really...

107
00:11:17,000 --> 00:11:22,000
I think we should talk about why we're practicing mindfulness.

108
00:11:22,000 --> 00:11:28,000
You don't often hear it associated when people use the word mindfulness.

109
00:11:28,000 --> 00:11:31,000
Talk about Buddhist mindfulness.

110
00:11:31,000 --> 00:11:36,000
We don't often hear about the reasons for practicing mindfulness.

111
00:11:36,000 --> 00:11:40,000
And the first thing he said was why we're practicing.

112
00:11:40,000 --> 00:11:45,000
He gave these five reasons.

113
00:11:45,000 --> 00:11:51,000
So this is the path that leads to these five things.

114
00:11:51,000 --> 00:11:57,000
It sort of sets the tone and tells you what the Buddha's focus was.

115
00:11:57,000 --> 00:12:04,000
What was the Buddha teaching? What was he preaching?

116
00:12:04,000 --> 00:12:06,000
What was the purpose of his teachings?

117
00:12:06,000 --> 00:12:18,000
What was the goal he had in mind when he taught mindfulness?

118
00:12:18,000 --> 00:12:20,000
So the Buddha had these five reasons.

119
00:12:20,000 --> 00:12:33,000
First is the purification of beings. He taught mindfulness because it purifies.

120
00:12:33,000 --> 00:12:36,000
Purification, of course.

121
00:12:36,000 --> 00:12:43,000
And purification is something we see across the various religious traditions.

122
00:12:43,000 --> 00:12:49,000
In India, it's a famous example, they would purify themselves through water.

123
00:12:49,000 --> 00:12:57,000
In Christianity, baptism, I believe, must be something to do with the purification through water.

124
00:12:57,000 --> 00:13:10,000
There are ritual purifications, purification through smoke, that kind of thing in different tradition.

125
00:13:10,000 --> 00:13:18,000
There's purification by confession and so on.

126
00:13:18,000 --> 00:13:21,000
What about the purification comes through mindfulness?

127
00:13:21,000 --> 00:13:26,000
This is what we just went through, right, the seven purification.

128
00:13:26,000 --> 00:13:29,000
So first of all, he was very much concerned with purification.

129
00:13:29,000 --> 00:13:32,000
It's a point to make about the Buddha's.

130
00:13:32,000 --> 00:13:33,000
It's not unique.

131
00:13:33,000 --> 00:13:37,000
Purification was actually a big thing in the time of the Buddha.

132
00:13:37,000 --> 00:13:40,000
This was a key word that people were using.

133
00:13:40,000 --> 00:13:45,000
It seems, as a reason for why they would go off into the forest,

134
00:13:45,000 --> 00:13:53,000
they were trying to free themselves from the defilements of ordinary life.

135
00:13:53,000 --> 00:14:04,000
You get so defiled, disceled by desires and conflict,

136
00:14:04,000 --> 00:14:08,000
delusion and arrogance and attachment and all sorts of things.

137
00:14:08,000 --> 00:14:11,000
So how to purify yourself?

138
00:14:11,000 --> 00:14:13,000
It's just a question.

139
00:14:13,000 --> 00:14:15,000
Here is the Buddha's answer.

140
00:14:15,000 --> 00:14:20,000
Purification comes through mindfulness.

141
00:14:20,000 --> 00:14:29,000
Not from avoiding or rejecting or suppressing our mind states,

142
00:14:29,000 --> 00:14:31,000
but understanding them.

143
00:14:31,000 --> 00:14:36,000
I mean, there's something very profound and very important

144
00:14:36,000 --> 00:14:41,000
about the Satipatana Sutta in that even the bad states

145
00:14:41,000 --> 00:14:45,000
when it talks about the hindrances,

146
00:14:45,000 --> 00:14:51,000
the mind states that are clearly troubling and troublesome.

147
00:14:51,000 --> 00:14:54,000
It talks about being mindful of them.

148
00:14:54,000 --> 00:14:59,000
Not running away or trying to change them.

149
00:14:59,000 --> 00:15:03,000
And that's how the Buddha said true purification comes about.

150
00:15:03,000 --> 00:15:07,000
Because of course, through seeing them clearly as they are,

151
00:15:07,000 --> 00:15:12,000
one no longer reacts and through not reacting, they don't.

152
00:15:12,000 --> 00:15:18,000
And they don't have any power.

153
00:15:18,000 --> 00:15:36,000
The second result of practicing mindfulness is dukkadomanasana and tangkamaya.

154
00:15:36,000 --> 00:15:37,000
Wait, sorry.

155
00:15:37,000 --> 00:15:49,000
The second one is tukkaparideva nangkzamatikamaya,

156
00:15:49,000 --> 00:16:00,000
to overcome sorrow, lamentation, despair, all kinds of problems.

157
00:16:00,000 --> 00:16:05,000
And the implication here is that purification or a pure mind

158
00:16:05,000 --> 00:16:08,000
has been allows one to solve all of one's problems.

159
00:16:08,000 --> 00:16:11,000
I mean, this is very much one and the same.

160
00:16:11,000 --> 00:16:18,000
When your mind is pure, then the mind is not caught up with

161
00:16:18,000 --> 00:16:34,000
depression and anxiety and despair and disappointment.

162
00:16:34,000 --> 00:16:39,000
And there's a key difference here between trying to get what you want,

163
00:16:39,000 --> 00:16:49,000
trying to be happy all the time, through accomplishing or achieving all of your life goals and desires.

164
00:16:49,000 --> 00:16:59,000
Something quite different from between that and learning to free yourself from the desires,

165
00:16:59,000 --> 00:17:27,000
to free yourself from the need for the need for satiation or satisfaction or the need for always getting what you want.

166
00:17:27,000 --> 00:17:37,000
So these are the two alternative ways of always being happy to always get what you want

167
00:17:37,000 --> 00:17:42,000
or to learn to give up wanting.

168
00:17:42,000 --> 00:17:54,000
So when your mind is pure, there's no room for, there's no potential for depression, anxiety,

169
00:17:54,000 --> 00:18:18,000
no potential for disappointment, no potential for morning loss, fearing loss.

170
00:18:18,000 --> 00:18:24,000
The third result is freedom from suffering.

171
00:18:24,000 --> 00:18:43,000
Just very much seems to be talking about in a very practical sense.

172
00:18:43,000 --> 00:18:51,000
You feel pain? What's the cure for pain? Mindfulness.

173
00:18:51,000 --> 00:19:00,000
You have mental suffering. What's the cure for mental suffering? Mindfulness.

174
00:19:00,000 --> 00:19:04,000
It's quite curious. It's not what we, especially for physical pain,

175
00:19:04,000 --> 00:19:09,000
we certainly don't think that being mindful of the pain is going to cure it.

176
00:19:09,000 --> 00:19:19,000
And in fact, it's quite clear that when you begin to say pain, pain doesn't get rid of the pain.

177
00:19:19,000 --> 00:19:28,000
Even with mental anguish, in the beginning it seems quite clear that mindfulness isn't going to do anything about it.

178
00:19:28,000 --> 00:19:39,000
Sometimes when you're mindful, the anguish, the despair, the sadness gets worse.

179
00:19:39,000 --> 00:19:43,000
It's a different way of solving your problems.

180
00:19:43,000 --> 00:19:48,000
The idea is to see that they're not actually problems.

181
00:19:48,000 --> 00:19:51,000
Even your problems.

182
00:19:51,000 --> 00:19:57,000
When you're sad about something, sadness is a perception, hey, this is a problem.

183
00:19:57,000 --> 00:20:02,000
It's inherently problem-seeking.

184
00:20:02,000 --> 00:20:09,000
The mind state that is sad. Even the mind state that is in pain is inherently negative.

185
00:20:09,000 --> 00:20:19,000
It's a negative reaction. This is bad. That is bad.

186
00:20:19,000 --> 00:20:26,000
And that can't exist with mindfulness. When you're mindful, negativity can't exist.

187
00:20:26,000 --> 00:20:32,000
Because of that, and it sounds incredibly naive or simplistic to say,

188
00:20:32,000 --> 00:20:36,000
but it's simple. It's not simplistic.

189
00:20:36,000 --> 00:20:53,000
It's that there's no suffering. When you're mindful, when you stop being negative, you can't suffer.

190
00:20:53,000 --> 00:21:03,000
When we talk about overcoming or freeing ourselves from physical and mental suffering, we really mean

191
00:21:03,000 --> 00:21:07,000
we mean freeing, but in a very different way.

192
00:21:07,000 --> 00:21:13,000
It's not about some magic trick that makes the pain go away.

193
00:21:13,000 --> 00:21:17,000
It's about looking at that negativity. Hey, this is pain. This is bad.

194
00:21:17,000 --> 00:21:25,000
Or hey, this is experience is bad. It makes me angry or sad or frustrated.

195
00:21:25,000 --> 00:21:33,000
And changing that so that someone's yelling at you, it's just hearing.

196
00:21:33,000 --> 00:21:43,000
Someone's beating you, it's just feeling.

197
00:21:43,000 --> 00:21:53,000
As someone of a radical way of looking at the world, I guess, that sounds very radical.

198
00:21:53,000 --> 00:22:00,000
But here's the question. We have this thing we call reality.

199
00:22:00,000 --> 00:22:09,000
And the very first question we should ask, that we never ask, and was never posed to us.

200
00:22:09,000 --> 00:22:12,000
So what are we going to do about this thing called reality?

201
00:22:12,000 --> 00:22:18,000
What are we going to, what is our purpose? What is the point?

202
00:22:18,000 --> 00:22:25,000
We got caught up, right? Okay, here I am born, a child. These are my parents.

203
00:22:25,000 --> 00:22:29,000
We never thought to ask, what the heck is going on?

204
00:22:29,000 --> 00:22:40,000
We didn't have time. We had to eat. We were hungry. We had to play. We were playful.

205
00:22:40,000 --> 00:22:45,000
And then on and on and on through life. And we never stopped to say, wait, what am I doing?

206
00:22:45,000 --> 00:22:48,000
What is this all?

207
00:22:48,000 --> 00:22:52,000
We don't ask these fundamental questions until much later.

208
00:22:52,000 --> 00:22:56,000
And by then we're steeped in culture and wrong views.

209
00:22:56,000 --> 00:23:01,000
And I'll caught up in our own likes and dislikes.

210
00:23:01,000 --> 00:23:05,000
And it's very hard to be a philosopher.

211
00:23:05,000 --> 00:23:11,000
So it's very, we hold wise philosophers and greatest themes.

212
00:23:11,000 --> 00:23:18,000
It's not easy. That's very easy to have a wrong philosophy or a philosophy that is flawed.

213
00:23:18,000 --> 00:23:21,000
That's why you have philosophers arguing with each other.

214
00:23:21,000 --> 00:23:27,000
Even the really good ones.

215
00:23:27,000 --> 00:23:33,000
And then you have people who have a fairly deep philosophy, but can't actually practice it

216
00:23:33,000 --> 00:23:43,000
because they're too caught up in their own addictions and their own problems.

217
00:23:43,000 --> 00:23:48,000
Mindfulness is an answer to this very fundamental question.

218
00:23:48,000 --> 00:23:50,000
It's not a question of, hey, how am I going to live my life?

219
00:23:50,000 --> 00:23:54,000
No, it's like, what is going on right here and now?

220
00:23:54,000 --> 00:23:57,000
How am I going to relate to reality?

221
00:23:57,000 --> 00:24:04,000
It's not about being a good person in society. That's a much later question.

222
00:24:04,000 --> 00:24:11,000
That's a much more abstract question.

223
00:24:11,000 --> 00:24:15,000
The very real question is here now, what's going on?

224
00:24:15,000 --> 00:24:23,000
How do I relate to these experiences that I'm having here and now?

225
00:24:23,000 --> 00:24:32,000
And so it does address that this very fundamental level, the problem of suffering.

226
00:24:32,000 --> 00:24:35,000
Does that this level there is pain?

227
00:24:35,000 --> 00:24:38,000
How are you going to react to that?

228
00:24:38,000 --> 00:24:46,000
There is sadness, there is anger, there is attachment to all these things.

229
00:24:46,000 --> 00:24:51,000
Mindfulness is this low level interaction with reality.

230
00:24:51,000 --> 00:24:57,000
It's a very fundamental, basic level of interaction.

231
00:24:57,000 --> 00:25:09,000
That's of course what's so powerful about it because it then cascades and affects every part of our lives.

232
00:25:09,000 --> 00:25:20,000
Breathes us from all kinds of suffering because all kinds of suffering eventually come down to this experiential reality.

233
00:25:20,000 --> 00:25:26,000
The fourth result of mindfulness is that it sets you on the right path.

234
00:25:26,000 --> 00:25:28,000
I've talked about all these before.

235
00:25:28,000 --> 00:25:31,000
This is something that I talk about a lot.

236
00:25:31,000 --> 00:25:36,000
The right path and this is what we've gone over for the past weeks.

237
00:25:36,000 --> 00:25:42,000
The right path is more of a way of being rather than a way of a place that we're going.

238
00:25:42,000 --> 00:25:46,000
You're not really going anywhere.

239
00:25:46,000 --> 00:25:57,000
That probably doesn't need to be emphasized too much because it's quite clear.

240
00:25:57,000 --> 00:25:59,000
It's about changing who you are.

241
00:25:59,000 --> 00:26:08,000
The way is becoming to live your life in a certain way of experiencing reality in a certain way.

242
00:26:08,000 --> 00:26:16,000
But what it isn't is I must become a monk or even Buddhist or quit my job.

243
00:26:16,000 --> 00:26:20,000
This or that go on the ball from the forest.

244
00:26:20,000 --> 00:26:24,000
It doesn't mean any of that.

245
00:26:24,000 --> 00:26:29,000
The way is mindfulness.

246
00:26:29,000 --> 00:26:30,000
That's the key.

247
00:26:30,000 --> 00:26:32,000
What is the right way?

248
00:26:32,000 --> 00:26:34,000
It's not too complicated.

249
00:26:34,000 --> 00:26:38,000
You know, all that talk about the eightfold path and the seven purification.

250
00:26:38,000 --> 00:26:43,000
It's quite interesting and useful.

251
00:26:43,000 --> 00:26:45,000
But it's not even so complicated.

252
00:26:45,000 --> 00:26:51,000
All of that comes about through the practice of mindfulness.

253
00:26:51,000 --> 00:26:55,000
And finally, the fifth result of practicing mindfulness.

254
00:26:55,000 --> 00:26:57,000
What is mindfulness is the path.

255
00:26:57,000 --> 00:27:01,000
It's the path to Nibana.

256
00:27:01,000 --> 00:27:04,000
Mindfulness is the path to the deathless.

257
00:27:04,000 --> 00:27:09,000
Mindfulness is what leads to that which has no beginning and no end.

258
00:27:09,000 --> 00:27:12,000
That which doesn't arise and doesn't cease.

259
00:27:12,000 --> 00:27:17,000
That which is outside of Samsara.

260
00:27:17,000 --> 00:27:26,000
The mindfulness one experiences freedom from suffering in a very visceral and real way.

261
00:27:26,000 --> 00:27:30,000
It's not just some abstract, hey, I feel pain and I don't suffer.

262
00:27:30,000 --> 00:27:42,000
It's really and truly this freedom from this experience of complete release, complete cessation.

263
00:27:42,000 --> 00:27:45,000
It's the most peaceful thing.

264
00:27:45,000 --> 00:27:53,000
It's the only peaceful thing that is true and an adulterated piece.

265
00:27:53,000 --> 00:28:00,000
That's Nibana, freedom from suffering.

266
00:28:00,000 --> 00:28:02,000
So that's the first part of the Sutta.

267
00:28:02,000 --> 00:28:03,000
That's the first thing the Buddha says.

268
00:28:03,000 --> 00:28:04,000
It's by way of introduction.

269
00:28:04,000 --> 00:28:07,000
And then he says, what is this one way?

270
00:28:07,000 --> 00:28:11,000
It's the four Satipatana.

271
00:28:11,000 --> 00:28:14,000
And then he goes into explaining the Satipatana.

272
00:28:14,000 --> 00:28:19,000
So I'll go over all this and then we'll get into each one and each section.

273
00:28:19,000 --> 00:28:24,000
Then we'll go through each section one by one.

274
00:28:24,000 --> 00:28:30,000
Today we talk about something that should be familiar to many of you.

275
00:28:30,000 --> 00:28:32,000
Why we're practicing?

276
00:28:32,000 --> 00:28:33,000
What are the reasons?

277
00:28:33,000 --> 00:28:35,000
What are the benefits?

278
00:28:35,000 --> 00:28:40,000
What does it mean?

279
00:28:40,000 --> 00:28:41,000
What does it mean?

280
00:28:41,000 --> 00:28:46,000
This is the only way.

281
00:28:46,000 --> 00:28:58,000
There you go, that's the demo for tonight.

282
00:28:58,000 --> 00:29:01,000
Tomorrow is no meeting.

283
00:29:01,000 --> 00:29:09,000
Tomorrow we have Sutta study, I think.

284
00:29:09,000 --> 00:29:24,000
Questions?

285
00:29:24,000 --> 00:29:44,000
One question tonight, do psychic powers naturally develop through meditation or must they be actively

286
00:29:44,000 --> 00:29:54,000
worked towards?

287
00:29:54,000 --> 00:30:01,000
It can be both ways, but meditation is work.

288
00:30:01,000 --> 00:30:07,000
It's an important point in response to this question.

289
00:30:07,000 --> 00:30:13,000
Nothing comes naturally because meditation itself is work.

290
00:30:13,000 --> 00:30:21,000
I think we often get the idea that meditation is something where you don't work.

291
00:30:21,000 --> 00:30:29,000
You just sit and let things come to you.

292
00:30:29,000 --> 00:30:33,000
The question really is how are you meditating?

293
00:30:33,000 --> 00:30:42,000
Are you meditating in such a way that these psychic powers will arise?

294
00:30:42,000 --> 00:30:50,000
There are many types of meditation that are unlikely for the mind to go in that direction.

295
00:30:50,000 --> 00:30:58,000
Mindfulness isn't highly likely to give you magical powers because you're not working in such a way.

296
00:30:58,000 --> 00:31:00,000
It's not mysterious.

297
00:31:00,000 --> 00:31:12,000
These things come from highly cultivated and trained minds.

298
00:31:12,000 --> 00:31:16,000
If you're not training your mind in that way, they're not really going to arise.

299
00:31:16,000 --> 00:31:20,000
They can be incidental if your intention is a certain way.

300
00:31:20,000 --> 00:31:30,000
If you happen to be training yourself, your mind happens to be training or building up habits that have some connection with magical powers.

301
00:31:30,000 --> 00:31:39,000
Like any kind of summit to practice, your intention is not to gain magical powers, but your mind is becoming so strong and so fixed and focused.

302
00:31:39,000 --> 00:31:51,000
It's not hard for it to be nudged over into cultivating psychic powers.

303
00:31:51,000 --> 00:32:08,000
They don't come naturally, but the power of many kinds of meditation can be applied to various kinds of psychic power.

304
00:32:08,000 --> 00:32:26,000
Or you can just outright cultivate those in that state of mind that leans to psychic power.

305
00:32:26,000 --> 00:32:42,000
So one question tonight. Thank you all for tuning in. Have a good night.

