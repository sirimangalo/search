1
00:00:00,000 --> 00:00:09,800
So, when I read this message, I couldn't understand it.

2
00:00:09,800 --> 00:00:18,320
And so I look for an explanation in the commentaries, but commentary didn't see anything

3
00:00:18,320 --> 00:00:20,040
about this sentence.

4
00:00:20,040 --> 00:00:27,100
I was dissatisfied with the commentaries because we always expect the commentary to explain

5
00:00:27,100 --> 00:00:35,760
such points, and this place is silent, and what was sub-comendary saying?

6
00:00:35,760 --> 00:00:43,360
So, it gave me a lot of dukha, and so I wanted to give you to give dukha to YouTube, so

7
00:00:43,360 --> 00:00:48,000
you'll have to think about it until next class.

8
00:00:48,000 --> 00:00:53,200
Okay, next question.

9
00:00:53,200 --> 00:01:02,000
Do plans, trees, rocks, and animals have come, maybe due to many of you, it is a silly question,

10
00:01:02,000 --> 00:01:11,300
but there may be some people, hopefully not among you, who may think that there is

11
00:01:11,300 --> 00:01:17,060
a camera to plants, trees, and so on, so this question is put here.

12
00:01:17,060 --> 00:01:21,940
So, according to Thiravada Buddhism, plants, trees, and rocks are not living beings, they

13
00:01:21,940 --> 00:01:30,420
are not regarded as living beings, although in some way, plants may be said to have life,

14
00:01:30,420 --> 00:01:36,660
but that is not life taught in a beat-a-ma, so plants, trees, and rocks are not living beings

15
00:01:36,660 --> 00:01:48,900
and as such, they have no camera, although they have no camera, they are never that is dependent

16
00:01:48,900 --> 00:01:54,300
on other material conditions like moisture, temperature, and nutrition, and so on.

17
00:01:54,300 --> 00:02:08,420
So, although they have no camera, they depend on conditions to arise and to exist, so they

18
00:02:08,420 --> 00:02:20,580
are also dependent on some conditions, although they have no camera, and the causes

19
00:02:20,580 --> 00:02:36,500
of relationship between an enemy, things will be explained with reference to Patana.

20
00:02:36,500 --> 00:02:45,060
Now, any males have, any males are living beings, they have all five aggregates as

21
00:02:45,060 --> 00:02:54,660
the human beings do, and so they are living beings, as living beings, they have camera.

22
00:02:54,660 --> 00:03:02,580
Any males acquire a camera, or accumulate a camera, and any males are born as an image

23
00:03:02,580 --> 00:03:12,940
as a result of acutella camera in the past, so any males have camera and they do

24
00:03:12,940 --> 00:03:25,900
a deep of on-camera, but plants, trees, rocks, and other inanimate things have no camera.

25
00:03:25,900 --> 00:03:35,740
Now, there are some statements in visual demogas regarding camera and results, and they

26
00:03:35,740 --> 00:03:45,820
are not body, so we should, we should note them.

27
00:03:45,820 --> 00:03:53,020
Now, while camera and results, thus constantly maintaining their round, as seed and tree

28
00:03:53,020 --> 00:03:57,740
succeed and turn, no first beginning can be shown.

29
00:03:57,740 --> 00:04:04,340
Now, camera and result follow each other, and there is camera, and then there is a result

30
00:04:04,340 --> 00:04:14,220
of camera, no, say there is a result of a result of a result of an acutella camera, there

31
00:04:14,220 --> 00:04:24,580
is rebirth, as say, in full, full states or in human beings or in celestial beings,

32
00:04:24,580 --> 00:04:33,500
and during that existence, beings acquire newcomer, and then there is the result of that

33
00:04:33,500 --> 00:04:39,460
camera in the future, and so, camera and it results go on and on and on like that, like

34
00:04:39,460 --> 00:04:45,500
a seed and the tree, so there is a seed, and then it grows into a tree, and then that the

35
00:04:45,500 --> 00:04:51,420
tree gives seed, and then seed into tree, and so on, oh, we can say the hand and the

36
00:04:51,420 --> 00:04:57,900
egg, right, go on and on and on, so there is no first beginning, no first beginning can

37
00:04:57,900 --> 00:05:13,860
be shown, we can go back, but however far we will never reach the beginning, so there

38
00:05:13,860 --> 00:05:21,620
is no first beginning of camera and results, although camera results, there is the rolling

39
00:05:21,620 --> 00:05:28,420
on of camera and results in this round of rebuds, and now in the future round of birds

40
00:05:28,420 --> 00:05:38,820
can they be shown not to occur, and in the future they will surely arise, because when

41
00:05:38,820 --> 00:05:54,500
there is camera at present and there will be result of camera in the future, now in

42
00:05:54,500 --> 00:06:03,020
some chia philosophy, which is one of the six classical Hindu philosophies, it is believed

43
00:06:03,020 --> 00:06:13,780
that the result already exists in the cause, but in an unmanifest form, so when it becomes

44
00:06:13,780 --> 00:06:23,060
manifest, it becomes the result, so according to that philosophy, the result is already

45
00:06:23,060 --> 00:06:33,740
in the cause, so the cause, mature becomes mature, and when it reaches the highest stage

46
00:06:33,740 --> 00:06:44,340
of maturity, it becomes the result, so in that philosophy, camera or cause and result are

47
00:06:44,340 --> 00:06:50,620
same, but Buddhism does not accept that, and Buddhism, it is taught that, camera is one

48
00:06:50,620 --> 00:06:57,620
thing and result is another, so here, we showed you mother said, there is no camera in

49
00:06:57,620 --> 00:07:06,660
result, nor does result exist in camera, so there is no camera in result and no result

50
00:07:06,660 --> 00:07:14,340
in camera, they are just two different things, though they are right of one another, though

51
00:07:14,340 --> 00:07:21,740
they are different from one another, there is no fruit without the camera, but there

52
00:07:21,740 --> 00:07:29,780
can be no result without the camera, so they are related as cause and effect, but cause

53
00:07:29,780 --> 00:07:40,060
is one thing and effect is another, they are not identical, as fire does not exist inside

54
00:07:40,060 --> 00:07:48,380
the sun is gem called down, nor yet outside them, but it is brought to be by means of

55
00:07:48,380 --> 00:08:00,180
components or by means of conditions, now when there is sunshine and there is called

56
00:08:00,180 --> 00:08:07,380
down or small pieces of wood or a pieces of paper, and you better make me find glass

57
00:08:07,380 --> 00:08:17,380
between them and concentrate the rays on the pieces of paper or cow down, the fire

58
00:08:17,380 --> 00:08:29,380
is produced, so here it says fire does not exist inside the sun, inside the gem or inside

59
00:08:29,380 --> 00:08:36,620
cow down, because if there is, if fire exists in the sun, I mean the rays of the sun

60
00:08:36,620 --> 00:08:45,100
or the gem or cow down, they will be burning, they will always be burning, but fire is

61
00:08:45,100 --> 00:08:52,020
not in them, and so they are not burning when they are put separately, but when they

62
00:08:52,020 --> 00:09:00,540
are put together, then there is fire, so fire does not exist in the sun or in the gem

63
00:09:00,540 --> 00:09:09,020
or in the cow down, then where does it exist, outside there, we do not say, we cannot

64
00:09:09,020 --> 00:09:16,220
say that fire exists somewhere apart from these three things, but fire is brought to

65
00:09:16,220 --> 00:09:22,860
be, fire is produced by means of the components, by means of these conditions, the conditions

66
00:09:22,860 --> 00:09:30,900
you mean the sun, the gem or in making fine glass and cow down, so when these three are

67
00:09:30,900 --> 00:09:37,300
put together, then there is fire, but fire does not exist in the sun, the gem and cow

68
00:09:37,300 --> 00:09:44,580
down, and without these three things, fire cannot be produced, so in the same way there

69
00:09:44,580 --> 00:09:54,540
is no result in the comma and no comma in the result, results be found within the comma

70
00:09:54,540 --> 00:10:01,740
nor without, nor does the comma still persist, in the result it has produced, so there

71
00:10:01,740 --> 00:10:12,980
is no result found in the comma or of cyclic comma or without the comma, and the comma

72
00:10:12,980 --> 00:10:21,260
still persist, nor does the comma still persist in the result, so comma does not persist

73
00:10:21,260 --> 00:10:26,860
in the result, that means comma is not in the result, it has produced, the comma of its

74
00:10:26,860 --> 00:10:34,820
fruit is void, so comma is right of its fruit, comma is right of the result, no fruit

75
00:10:34,820 --> 00:10:41,820
exists yet in comma, and that is no fruit in the comma, and still the fruit is born

76
00:10:41,820 --> 00:10:51,140
from it, the result is born from comma, only depending on the comma, so only depending

77
00:10:51,140 --> 00:11:01,500
on the totally depending on the comma, the result is produced, for here there is no

78
00:11:01,500 --> 00:11:08,100
god of Brahma, creator of the round of reburge, so this round of reburge is not the creation

79
00:11:08,100 --> 00:11:18,940
of a god of a Brahma, but they are natural outcome of the comma, so comma and its results

80
00:11:18,940 --> 00:11:27,500
are the phenomena alone, flew on, so they flew on and on and on, cause and component

81
00:11:27,500 --> 00:11:32,580
and they are conditioned, so they are conditioned by their cause and they are conditioned

82
00:11:32,580 --> 00:11:40,220
by big conditions, and so depending upon the cause and conditions, these mandal and physical

83
00:11:40,220 --> 00:11:50,860
phenomena flew on and on and on, and in this, flew on of mandal and physical phenomena,

84
00:11:50,860 --> 00:12:00,700
which is called the round of reburge, there is no god or no brahman, I hope create this

85
00:12:00,700 --> 00:12:14,060
round of reburge, so it just mandal and physical phenomena dependent upon the conditions

86
00:12:14,060 --> 00:12:24,660
flowing on and on, some benefits of understanding the law of comma, so we as good as we

87
00:12:24,660 --> 00:12:31,060
understand the law of comma, following the teachings of the Buddha, and this understanding

88
00:12:31,060 --> 00:12:39,300
of law of comma has many benefits and here are some of them, so it satisfies our curiosity

89
00:12:39,300 --> 00:12:46,620
about inequality among beings, so we want to know why beings are different, why beings

90
00:12:46,620 --> 00:12:55,060
are not equal, although they are equal as beings, as human beings or animals, as animals,

91
00:12:55,060 --> 00:13:11,580
and this inequality among beings is satisfactorily explained by the law of comma, the understanding

92
00:13:11,580 --> 00:13:21,820
of law of comma satisfies our curiosity about inequality among beings, it is no wonder

93
00:13:21,820 --> 00:13:40,020
that understanding of law of comma has converted many people to Buddhism, and understanding

94
00:13:40,020 --> 00:13:51,260
of law of comma gives us hope, there is always hope that we can, we can be better,

95
00:13:51,260 --> 00:14:05,780
we are not to be satisfied with, say what condition we are in, but we can always improve

96
00:14:05,780 --> 00:14:15,740
our condition by doing a good, good comma, so it gives us the law or the understanding

97
00:14:15,740 --> 00:14:24,020
of the law of comma gives us hope, so we are never hopeless, and the understanding of

98
00:14:24,020 --> 00:14:31,980
law of comma can give us consolation in times of misfortune, now people may be missed

99
00:14:31,980 --> 00:14:42,100
virtue, may times in their lives, loss of wealth, loss of business, loss of job, loss

100
00:14:42,100 --> 00:14:51,540
of friends, loss of loved ones, and when they meet with such misfortunes, those who do

101
00:14:51,540 --> 00:15:01,140
not understand the law of comma cannot be controlled, they might even become dejected,

102
00:15:01,140 --> 00:15:09,620
and they might even take their own lives, but those who understand the law of comma

103
00:15:09,620 --> 00:15:17,260
can understand the misfortunes, because this misfortunes becomes as a result of what,

104
00:15:17,260 --> 00:15:26,180
say they did in the past, and so there is no other person to blame but themselves, and

105
00:15:26,180 --> 00:15:39,820
so people can be consoled with the understanding of law of comma, and it gives us strength

106
00:15:39,820 --> 00:15:47,220
to meet the ups and downs of life without agitation, now when we lead a life, there are ups

107
00:15:47,220 --> 00:15:57,260
and downs in our lives, we cannot avoid a meeting success, meeting failure in our lives,

108
00:15:57,260 --> 00:16:08,060
so whether we meet with success of failure, we can stand firm, we can meet these situations

109
00:16:08,060 --> 00:16:23,900
without much agitation, and it is important that without agitation when we meet with success

110
00:16:23,900 --> 00:16:33,060
of failure, because now when people meet with success, they are elated and when they

111
00:16:33,060 --> 00:16:41,740
meet with failure, they are dejected, and so their lives become miserable, but if we

112
00:16:41,740 --> 00:16:57,220
understand the law of comma, we can take them as they come without much agitation, it

113
00:16:57,220 --> 00:17:07,700
teaches us to realize our potential to create our own future, now we get the results

114
00:17:07,700 --> 00:17:20,420
of our own comma, so we can create our own future, we have the potential to create our

115
00:17:20,420 --> 00:17:36,860
own future if we understand the law of comma, and not only realizing the potential,

116
00:17:36,860 --> 00:17:46,740
but we can use this potential to make our future lives better than the present ones, so

117
00:17:46,740 --> 00:17:59,780
it teaches us that we beings have this potential to create our own future, and we need

118
00:17:59,780 --> 00:18:13,900
not look to other external powers or deities for our own future, actually we alone can

119
00:18:13,900 --> 00:18:30,300
create our future, and the understanding of law of comma teaches the individual responsibility,

120
00:18:30,300 --> 00:18:43,340
because the results we experience are caused by our own comma, we alone are responsible

121
00:18:43,340 --> 00:18:57,540
for our enjoyment or our suffering, so since we are responsible for our own heaviness

122
00:18:57,540 --> 00:19:14,060
or suffering, we are not to blame others for our conditions, so it teaches that we are responsible

123
00:19:14,060 --> 00:19:25,980
for ourselves, and if we want to be better in the future, we must do or we must acquire

124
00:19:25,980 --> 00:19:43,100
a good comma, so we become responsible for us, if we understand the law of comma, and

125
00:19:43,100 --> 00:19:49,740
it teaches us to avoid humming other beings and to be compassionate to other beings, now

126
00:19:49,740 --> 00:20:02,500
if we harm other beings, we will suffer the painful consequences of our comma, so if we

127
00:20:02,500 --> 00:20:10,700
do not want to suffer ourselves, then we do not inflict suffering on others, and so it helps

128
00:20:10,700 --> 00:20:19,660
us to avoid humming or enjoying the other beings, and it teaches us to be compassionate

129
00:20:19,660 --> 00:20:32,900
to other beings, because by being compassionate to other beings, we develop good comma,

130
00:20:32,900 --> 00:20:42,260
which will give good results in the future, so understanding the law of comma can help

131
00:20:42,260 --> 00:20:56,580
us to make the society more humane and more peaceful and more harmonious, there may be

132
00:20:56,580 --> 00:21:04,500
some more benefits of understanding the law of comma, so I want you to make additions

133
00:21:04,500 --> 00:21:18,660
to this list yourselves, so we have now studied comma in general, what comma is, and

134
00:21:18,660 --> 00:21:27,900
what we can do about comma and so on, next time we studied the different kinds of

135
00:21:27,900 --> 00:21:56,460
comma, you asked me on Sunday, you asked me on Sunday, you asked me on Sunday, I asked

