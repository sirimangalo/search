1
00:00:00,000 --> 00:00:05,820
Okay, so today we continue our study of the Dhamapada,

2
00:00:05,820 --> 00:00:12,460
tenuing onwards versus thirteen and fourteen.

3
00:00:12,460 --> 00:00:15,820
Watch where this follows.

4
00:00:15,820 --> 00:00:25,880
Jattat, Agaram, Tuchan Nang, Uthi, Uthi, Samati, Uthi, Uthjati,

5
00:00:25,880 --> 00:00:45,460
A1 Abhavitang Jitang, Raghus Samati, Uthi, Uthi, Naghuti, Naghamati, Uthi, Uthi, Uthi, Naghamati,

6
00:00:45,460 --> 00:00:55,180
and the meaning is as far as.

7
00:00:55,180 --> 00:01:01,980
Yata, Angarang, just as a house or a building.

8
00:01:01,980 --> 00:01:12,660
Duchanang, that is poorly attached or a roof, just as a roof that is poorly attached.

9
00:01:12,660 --> 00:01:19,700
The rain is able to penetrate wood and some activity.

10
00:01:19,700 --> 00:01:28,980
In the same way, an untrained mind, he wants to abba, wittang, zita, rago, some activity.

11
00:01:28,980 --> 00:01:40,340
The last, last is able to penetrate and number 14, just as in regards to a well-trained

12
00:01:40,340 --> 00:01:48,900
mind, sorry, in regards to a well-fetched hot, wookie, not some activity, rain is not able

13
00:01:48,900 --> 00:01:51,620
to penetrate.

14
00:01:51,620 --> 00:02:02,780
In the same way, a well-developed mind, last is not able to penetrate, so quite useful for meditators,

15
00:02:02,780 --> 00:02:12,180
it was taught in regards to the Buddha's cousin, the Buddha's cousin Nanda, when the

16
00:02:12,180 --> 00:02:19,300
Buddha went after the, after Siddhartha got them and became enlightened, he went back to

17
00:02:19,300 --> 00:02:25,900
his home of Kupada Vattu, and he taught all of his relatives, and so he taught his ex-wife,

18
00:02:25,900 --> 00:02:32,700
he taught his son, he taught his father, and they all were able to understand the teachings.

19
00:02:32,700 --> 00:02:38,700
But there was, there was one person who, it seems, the Buddha would understand, he had

20
00:02:38,700 --> 00:02:43,540
had the ability to penetrate the teachings, but was kind of stuck in a bit of a rut.

21
00:02:43,540 --> 00:02:48,380
And that is, that he was set to get married to the most beautiful woman in the country.

22
00:02:48,380 --> 00:02:53,780
And then, then the Bhadakani, which means, the beauty of the land.

23
00:02:53,780 --> 00:02:59,820
So in the area where they lived, she must have been the prize beauty, so she was well known

24
00:02:59,820 --> 00:03:05,500
for her beauty, and Nanda had become infatuated with his beauty.

25
00:03:05,500 --> 00:03:11,300
And so like ordinary human beings, he, he felt that that was going to bring him happiness

26
00:03:11,300 --> 00:03:20,380
to marry this woman and to live a life of, of central pleasure.

27
00:03:20,380 --> 00:03:25,540
But Buddhists, as Buddhists, we always throw monkey wrenches into this, as always, with

28
00:03:25,540 --> 00:03:29,940
the rain on people's brains.

29
00:03:29,940 --> 00:03:33,860
And the Buddha was certainly, certainly our leader in this regard.

30
00:03:33,860 --> 00:03:39,540
So on the day when they were to be, to be married, or during the marriage preparation,

31
00:03:39,540 --> 00:03:43,980
they invited the Buddha to come to receive arms.

32
00:03:43,980 --> 00:03:51,460
And when the Buddha finished his arms, he, he handed the bull to Nanda, when Buddha

33
00:03:51,460 --> 00:03:56,340
finished his meal, and had given a talk on the, on the teaching.

34
00:03:56,340 --> 00:04:00,340
He gave his bull to Nanda, and he started walking back to the monastery.

35
00:04:00,340 --> 00:04:04,820
And so when he got to the top of the stairs, Nanda thought, well, I guess he'll take his

36
00:04:04,820 --> 00:04:06,700
bull back now.

37
00:04:06,700 --> 00:04:11,700
But he didn't, he didn't say, excuse me, sir, can you take your bull back, because he, he

38
00:04:11,700 --> 00:04:12,700
felt that that would be rude.

39
00:04:12,700 --> 00:04:15,740
And he had a lot of respect for the Buddha.

40
00:04:15,740 --> 00:04:20,500
And he, he never had great respect for the Buddha's teaching as well.

41
00:04:20,500 --> 00:04:23,340
And, but the Buddha didn't take back his bone.

42
00:04:23,340 --> 00:04:26,540
So they got to the bottom of the stairs, and Nanda thought, oh, now he'll take back

43
00:04:26,540 --> 00:04:29,060
his bull, and the Buddha didn't take back his bull.

44
00:04:29,060 --> 00:04:31,500
Then he got to the door of the palace, and he thought, now he'll take his bull, and

45
00:04:31,500 --> 00:04:35,660
they got to the door to the city, and now he, and he kept thinking, following after him

46
00:04:35,660 --> 00:04:40,500
with his bull, thinking, why this, would he take back his bull?

47
00:04:40,500 --> 00:04:44,940
And so people saw this, and, and they started passing it around, and they sent word back

48
00:04:44,940 --> 00:04:49,860
to his, his, his bride to be saying, it looks like this.

49
00:04:49,860 --> 00:04:55,180
I'm going to go to my, has, has decided to capture your husband, your, your future

50
00:04:55,180 --> 00:04:57,380
husband, and he's going to spoil your, your whole plants.

51
00:04:57,380 --> 00:05:02,700
And so she ran after them, and she called out to him, oh, please, no, please, no

52
00:05:02,700 --> 00:05:08,740
boss, or hurry back soon in the most pitiful voice she can muster.

53
00:05:08,740 --> 00:05:13,020
And so this was, of course, tearing at his heart, because he was very much in love with

54
00:05:13,020 --> 00:05:20,180
her, and making Buddha look like a bit of a meaning, you know.

55
00:05:20,180 --> 00:05:25,700
And so the Buddha, when he got back to the, the monastery, Nanda still thought, okay, well,

56
00:05:25,700 --> 00:05:30,900
once even, if I'd write all the way back to the Buddha's kutti, and the Buddha turned

57
00:05:30,900 --> 00:05:37,140
the Nanda and said, so Nanda, would you like to become a monk, and then this thinking

58
00:05:37,140 --> 00:05:42,740
in his heart, no, no, no, no, no, I wouldn't, but he, he had so much respect for the Buddha

59
00:05:42,740 --> 00:05:47,620
as people in this situation often are, when confronted by a person of authority.

60
00:05:47,620 --> 00:05:53,020
And he had been so used to, of course, to going, going, according to the flow, because

61
00:05:53,020 --> 00:05:57,220
he was a prince, and he was, and living in luxury, and you just kind of go along with

62
00:05:57,220 --> 00:06:03,260
things, and everything is, is, is wonderful, because my year of prince.

63
00:06:03,260 --> 00:06:09,180
So, so right away he said, he said, sure, I'll become a monk, but in his heart he was,

64
00:06:09,180 --> 00:06:15,980
he was so mortified that he would have to become a monk.

65
00:06:15,980 --> 00:06:18,820
And so the Buddha said, okay, and he called the other monks over in the ordained of

66
00:06:18,820 --> 00:06:22,100
rights, and then there.

67
00:06:22,100 --> 00:06:27,700
And then the Buddha took him back, I believe, to Radhikaha.

68
00:06:27,700 --> 00:06:35,420
And at that, in, in that time, Nanda started getting quite perturbed and, and it was quite

69
00:06:35,420 --> 00:06:39,340
miserable as a monk, of course, because he was always thinking about his, his bride could

70
00:06:39,340 --> 00:06:44,220
be, who he was desperately in love with, and he thought, this is my soul mate, this is

71
00:06:44,220 --> 00:06:47,740
the one person who means anything to know.

72
00:06:47,740 --> 00:06:51,460
And here the Buddha has taken this from me, he wasn't upset at the Buddha, but he was,

73
00:06:51,460 --> 00:06:55,860
he was so miserable, because he was always thinking about the pleasure and the, the great,

74
00:06:55,860 --> 00:07:01,500
great love that he had for her.

75
00:07:01,500 --> 00:07:05,740
And so he said to his fellow monks, he finally had enough from him, and he said to them,

76
00:07:05,740 --> 00:07:09,700
he said, I've given up, I'm going to disrobe and become a lay person again, I can't

77
00:07:09,700 --> 00:07:10,700
take this anymore.

78
00:07:10,700 --> 00:07:15,100
This isn't, this isn't my role in life, this isn't my path in life, I'm in love with this

79
00:07:15,100 --> 00:07:17,500
woman and she means everything to me.

80
00:07:17,500 --> 00:07:20,260
Of course this is how I always justify it, right?

81
00:07:20,260 --> 00:07:26,500
When we have central pleasure, we always claim that there's a person who has some importance.

82
00:07:26,500 --> 00:07:29,900
We never, never get right down to it and say, I just think, see, it's beautiful, and

83
00:07:29,900 --> 00:07:35,900
I want to see her and touch her and, and receive pleasure from her, we say she, she has

84
00:07:35,900 --> 00:07:42,100
great meaning and, and she's my soul maintenance, so I, this is how we always feel.

85
00:07:42,100 --> 00:07:43,100
What happened?

86
00:07:43,100 --> 00:07:48,220
So they, they brought him to the Buddha and they said, Lord, Lord Buddha, this nanda

87
00:07:48,220 --> 00:07:55,700
year, he's, he says he's going to disrobe and the Buddha takes nanda, he, he said, okay,

88
00:07:55,700 --> 00:07:57,260
I'll, I'll, I'll look after this.

89
00:07:57,260 --> 00:08:05,460
He took nanda by his arm and brought him magically, first, first he brought him to this

90
00:08:05,460 --> 00:08:12,460
burnt out, burnt out for us, like it was a forest fire, it was totally burnt down and

91
00:08:12,460 --> 00:08:17,660
all there was left was this she monkey, this female monkey who was clinging to a stump

92
00:08:17,660 --> 00:08:22,860
of a tree that she had probably used to live in, but it was like the last standing burnt

93
00:08:22,860 --> 00:08:31,860
out tree and her tail had been burnt off and her, her ears had been burnt off and what else,

94
00:08:31,860 --> 00:08:36,180
her, her, her tails and her, her, her tail had her ears and her fur had been cinged and

95
00:08:36,180 --> 00:08:41,580
so on and there she was clinging in great suffering and pain and, and just looking like

96
00:08:41,580 --> 00:08:46,900
a total miserable sight and the Buddha said, you see that monkey over there and he said,

97
00:08:46,900 --> 00:08:52,180
yes, yes, I see that monkey and he didn't quite understand what the point was.

98
00:08:52,180 --> 00:08:57,420
This female monkey there and then he said, okay, now let's go and he took him magically

99
00:08:57,420 --> 00:09:03,380
up to the heavens and he took him to the dawah things that happened I think and there

100
00:09:03,380 --> 00:09:09,980
he showed him 500 dove-footed nymphs or pink-footed nymphs I think is the, I don't

101
00:09:09,980 --> 00:09:15,140
not sure what the Paulians did, something about their feet being pink, but just beautiful

102
00:09:15,140 --> 00:09:22,780
nymphs of course angels and the thing about nymphs of course is that they're, they're

103
00:09:22,780 --> 00:09:28,580
so much more beautiful, essentially attractive than any human being, it's hard to imagine

104
00:09:28,580 --> 00:09:35,060
it's not like just beautiful women, these are, what these are people with great merit

105
00:09:35,060 --> 00:09:41,740
and so they have become very beautiful as a result of their beauty of mind and so the

106
00:09:41,740 --> 00:09:48,860
physical has come to reflect that beyond anything most of us they have ever come across

107
00:09:48,860 --> 00:09:55,300
so Nanda was, Nanda was quite blown away in seeing these 500 nymphs playing around or

108
00:09:55,300 --> 00:10:02,420
doing their thing and the Buddha said, so tell me Nanda, which one is more beautiful

109
00:10:02,420 --> 00:10:10,140
your wife, Janapadakaliani or these 500 dove-footed nymphs, Nanda, who by this time had

110
00:10:10,140 --> 00:10:19,100
kind of forgotten about his wife, he said, venerable sir, if you, if you compare my wife

111
00:10:19,100 --> 00:10:28,180
in that monkey that we saw, my wife, these, these dove-footed nymphs are, are as much more

112
00:10:28,180 --> 00:10:35,660
beautiful than my wife as my wife is more beautiful than that, than that monkey and the

113
00:10:35,660 --> 00:10:42,860
Buddha said, well then so then I tell you what Nanda, if you stay as a monk and you try

114
00:10:42,860 --> 00:10:49,900
your best to follow my instruction and to live the holy life, I promise you these 500 nymphs

115
00:10:49,900 --> 00:10:59,220
will be yours, will be your entourage, will be reborn into this state to frolic around

116
00:10:59,220 --> 00:11:07,340
the nymphs, and Nanda, he wasn't, I guess at this point, they're very deep individual

117
00:11:07,340 --> 00:11:13,620
because, well like most of us, he, he, he gets caught up with, with emotions or with

118
00:11:13,620 --> 00:11:24,020
lust, you know, and this is because his mind is untrained, and so he, his mind would

119
00:11:24,020 --> 00:11:33,740
just wipe out the memory of his, his bride to be hers or his soon to be wife, and, and

120
00:11:33,740 --> 00:11:38,300
was, was overjoyed at this time, he said, well in that case I'll, I'll, I'll work really

121
00:11:38,300 --> 00:11:44,300
hard, because suddenly he had seen something that most human beings never see, this, this

122
00:11:44,300 --> 00:11:51,060
something that he thought was, well he, he thought his wife was, was on, on the top, and

123
00:11:51,060 --> 00:11:55,740
here he's seeing something that in his mind is much greater, which because the, the

124
00:11:55,740 --> 00:12:01,020
essential attraction would have been much greater, the perfection of these beings would

125
00:12:01,020 --> 00:12:09,020
have attracted him, and so he, he became quite excited and he became much, much more

126
00:12:09,020 --> 00:12:13,140
interested in living the holy life, because he realized that even in India, this, before,

127
00:12:13,140 --> 00:12:18,900
even before the Buddha, they, they believed this, that through the practice of austerity

128
00:12:18,900 --> 00:12:26,020
or whatever, you could go to heaven and enjoy great sensuality in heaven.

129
00:12:26,020 --> 00:12:29,900
So he thought, he thought, wow, it must be true, this must really be the result of

130
00:12:29,900 --> 00:12:34,900
practicing and when it's teaching, so he was really excited, and it just shows how

131
00:12:34,900 --> 00:12:39,500
thick of the mind is really, the whole idea of soul mates is kind of thrown out of the

132
00:12:39,500 --> 00:12:46,380
window, and so from that point on he practiced quite diligently, and the Buddha kind of

133
00:12:46,380 --> 00:12:50,140
helped him along, as I understand, because the Buddha let it be known what their

134
00:12:50,140 --> 00:12:54,780
agreement was, and so all the other monks came to know that Nanda is only being

135
00:12:54,780 --> 00:13:01,380
only practicing because he wants to have intercourse with a bunch of

136
00:13:01,380 --> 00:13:06,860
sensual, a bunch of celestial nymphs, and so they, they called him a

137
00:13:06,860 --> 00:13:11,220
higherling, from that point on, they said, oh Nanda, there goes Nanda the higherling,

138
00:13:11,220 --> 00:13:16,700
oh hello, Nanda, the higherling, because he has been, it seems that Nanda can be bought

139
00:13:16,700 --> 00:13:23,820
with a price, or can, can be, can be, bride, so they were, they were

140
00:13:23,820 --> 00:13:28,980
scorning him and so on, and so he felt quite ashamed about that, but nonetheless he

141
00:13:28,980 --> 00:13:32,940
worked really hard because he felt like he now had, he was now both in line with

142
00:13:32,940 --> 00:13:38,100
the Buddha and in line with his desires, because he, he had done what most

143
00:13:38,100 --> 00:13:42,660
people do when they, when this situation comes he had made a new decision that

144
00:13:42,660 --> 00:13:49,380
these were his soul mates, or this was his destiny to be with these ones, and so

145
00:13:49,380 --> 00:13:54,460
he worked very hard and, and as a result of working very hard slowly slowly his

146
00:13:54,460 --> 00:14:00,900
mind changed his mind calmed down, his mind became relaxed, his mind became

147
00:14:00,900 --> 00:14:06,780
clear, and he was able to see the, the, the arising and ceasing in phenomenon

148
00:14:06,780 --> 00:14:10,060
and he was able to follow the Buddha's teaching and understand the five

149
00:14:10,060 --> 00:14:15,420
aggregates, able to break them up, and to the point that he began, he began to

150
00:14:15,420 --> 00:14:21,380
lose his, his desires, and lose his lust, and that he was able to see that

151
00:14:21,380 --> 00:14:26,100
really, whether it's his wife or whether it's these celestial nymphs, it's

152
00:14:26,100 --> 00:14:32,380
all just, nama and group, all, all just experience, momentary experience that

153
00:14:32,380 --> 00:14:37,380
arises and ceasing, he came to say that this is impermanent, unsatisfying, and

154
00:14:37,380 --> 00:14:44,380
uncontrollable, and as a result he, he gave it up, and became an aura, he, he, he,

155
00:14:44,380 --> 00:14:49,180
he gained the realization and his mind let go, and entered into the bhana, and

156
00:14:49,180 --> 00:14:53,820
was able to become an aura hunt. Now at the moment when he became an aura hunt he

157
00:14:53,820 --> 00:14:58,340
went back to see the Buddha, and he said to the Buddha, you know that

158
00:14:58,340 --> 00:15:03,900
bargain we had, he wouldn't have been ashamed, but he said that bargain that we

159
00:15:03,900 --> 00:15:10,940
have, venerable sir, I release you from that, that, that bargain, and the Buddha

160
00:15:10,940 --> 00:15:13,740
said from the moment that you became an aura hunt, I was released from the

161
00:15:13,740 --> 00:15:21,340
bargain. This is the story of Nanda, Nanda, and then the monks heard about, or

162
00:15:21,340 --> 00:15:27,340
the monks, as usual, they would, they would tease him and they said, so

163
00:15:27,340 --> 00:15:32,420
Nanda, still thinking about, still thinking about some, some nymphs, or what

164
00:15:32,420 --> 00:15:38,060
are the nymphs for today, which the goal for today, still thinking about your

165
00:15:38,060 --> 00:15:42,220
wife, still thinking about those nymphs, and he said, no before I, before my mind

166
00:15:42,220 --> 00:15:46,580
was untrained, and so I thought about them, but now my mind is trained, and so I

167
00:15:46,580 --> 00:15:50,900
don't think about them, and they listened to this, and they thought and

168
00:15:50,900 --> 00:15:55,540
listened to him, because this is a serious offense if you lie about something

169
00:15:55,540 --> 00:15:59,260
like this, pretending to be enlightened, so they went to see the Buddha, and they

170
00:15:59,260 --> 00:16:02,500
said, you, you should hear what Nanda's saying, no, he's pretending to be enlightened,

171
00:16:02,500 --> 00:16:07,900
maybe he thinks he'll get better nymphs that way, and the Buddha said, my son

172
00:16:07,900 --> 00:16:14,420
is, is correct, before when his mind was untrained, it was like a roof, so that

173
00:16:14,420 --> 00:16:19,980
was ill-fetched, and now that his mind is trained, it is like a roof that is well

174
00:16:19,980 --> 00:16:26,340
-fetched, and so he told this verse, he said, just as poorly that

175
00:16:26,340 --> 00:16:34,500
roof is penetrated by the rain, and untrained mind is penetrated by less. I think

176
00:16:34,500 --> 00:16:39,820
part of the, part of what helped him, which is an interesting aspect of

177
00:16:39,820 --> 00:16:45,100
less, was the, would have been the realization that his mind was just, that his

178
00:16:45,100 --> 00:16:48,860
mind was really just playing tricks on me, because this, this is borne out in

179
00:16:48,860 --> 00:16:53,220
reality, people who think that they have met their soul match, or that they, they

180
00:16:53,220 --> 00:16:59,380
get the idea of self, and they think, I like this, I like that, or this is my

181
00:16:59,380 --> 00:17:08,980
preference, my wife, my, my lover, and so on, my soul mate, they can, they can, on a

182
00:17:08,980 --> 00:17:13,620
heart-deep turn and fall for something else, and the realization that this is

183
00:17:13,620 --> 00:17:20,620
the nature of the mind, that it jumps like a fire, the Buddha said, nati, raghus, nati

184
00:17:20,620 --> 00:17:27,820
raghus, samma, agi, there is no fire like, like raghali, like lust, because it

185
00:17:27,820 --> 00:17:32,640
jumps from one object to another, fire you can contain it, you can contain most

186
00:17:32,640 --> 00:17:38,780
fires, by digging pits, when you can trenches, fire won't jump to a rock, it

187
00:17:38,780 --> 00:17:45,740
won't jump to water, but lust jumps to anything, we can lust after, just about

188
00:17:45,740 --> 00:17:49,340
anything, this is why when you're born in a different realm you become, begin

189
00:17:49,340 --> 00:17:52,500
to lust, if you're born a dog, you lust after dogs, if you're born a dung, you

190
00:17:52,500 --> 00:17:58,100
know you lust after dung, you lust after dung, you lust, so what is the teaching

191
00:17:58,100 --> 00:18:03,220
here for us as meditators, this is the story part, as for what this can do for us

192
00:18:03,220 --> 00:18:10,420
as we come to see our minds in this way, we come to see our minds as having holes

193
00:18:10,420 --> 00:18:17,460
like the hole in a roof, our minds are permeable, and so our job as meditators

194
00:18:17,460 --> 00:18:23,140
is to touch the holes, to patch up the holes, and the way we do this we by

195
00:18:23,140 --> 00:18:29,940
developing our minds, just as the roof has to be patched, has to be well covered

196
00:18:29,940 --> 00:18:35,620
over, so to the mind has to be well sealed, the way to seal the mind is in three

197
00:18:35,620 --> 00:18:40,860
ways, to do development of the mind, or the development, first is a

198
00:18:40,860 --> 00:18:44,340
development of morality, and that's where we're starting off here, because at

199
00:18:44,340 --> 00:18:48,180
the beginning you don't have concentration, you just have your morality, what

200
00:18:48,180 --> 00:18:52,860
is your morality, you've stopped, so many things you've stopped connecting with

201
00:18:52,860 --> 00:18:56,980
the world, and also you've stopped letting your mind wander, so when you see

202
00:18:56,980 --> 00:19:01,140
something instead of looking at it, you're saying to yourself, see, that's

203
00:19:01,140 --> 00:19:09,660
morality, the narrowing, your activity, your mental activity, because that's

204
00:19:09,660 --> 00:19:14,820
what's going to bring concentration, normally when you walk, you're also

205
00:19:14,820 --> 00:19:21,860
thinking, you have no morality or no control over the mind, so now you're in

206
00:19:21,860 --> 00:19:28,580
some way trying to control, restrict yourself, morality, so when you walk, you see

207
00:19:28,580 --> 00:19:33,740
walking, walking, and even more so when you're practicing meditation, you're

208
00:19:33,740 --> 00:19:38,580
severely restricting your mind to just to bear awareness, when you hear a sound

209
00:19:38,580 --> 00:19:45,700
hearing, when you see, when you feel pain, pain, pain, not letting your mind dwell

210
00:19:45,700 --> 00:19:57,620
or develop or make more proliferate, so that develops concentration from

211
00:19:57,620 --> 00:20:03,060
morality, morality is the first layer, and morality is good because it prevents

212
00:20:03,060 --> 00:20:10,340
you from getting caught up in just in distraction and doing many things

213
00:20:10,340 --> 00:20:16,460
that would, that would hurt your meditation, but the next level, as you continue

214
00:20:16,460 --> 00:20:20,100
on to continue on, you'll develop concentration, concentration is the second

215
00:20:20,100 --> 00:20:28,260
way to patch up our minds, to seal up our minds, once you develop

216
00:20:28,260 --> 00:20:31,300
concentration, then your mind won't even give rise to liking or

217
00:20:31,300 --> 00:20:37,420
just liking, once you're able to see the object, for you watch the rising and

218
00:20:37,420 --> 00:20:40,100
you watch the falling and you know that it's rising, at the moment when you know

219
00:20:40,100 --> 00:20:45,460
that it's rising, your mind is perfectly clear, it's perfectly dry, the

220
00:20:45,460 --> 00:20:53,020
lusting desires can't come in, the fire can't, it can't drive you off to go

221
00:20:53,020 --> 00:20:58,180
and chase after celestial nymphs or whatever, and the point being that these

222
00:20:58,180 --> 00:21:04,780
things are all impermanent and they're all relative, they don't really have the

223
00:21:04,780 --> 00:21:09,020
ability to satisfy, they'll create pleasure for some time, and even being born in

224
00:21:09,020 --> 00:21:13,940
heaven is not something that is permanent, because it has a power and it has a force

225
00:21:13,940 --> 00:21:22,300
and that force arises and ceases, and so when you develop concentration, your

226
00:21:22,300 --> 00:21:26,860
mind will not give rise to these lusts, you'll just see things as they are, once

227
00:21:26,860 --> 00:21:31,580
you see things as they are, wisdom will begin to arise, and the question is why

228
00:21:31,580 --> 00:21:35,020
if concentration already stops these things from a rising, why isn't it

229
00:21:35,020 --> 00:21:40,500
wisdom necessary, there's concentration just like any roof, it's temporary, as

230
00:21:40,500 --> 00:21:47,900
long as you keep through, keep patching it up, it will protect you, but when you

231
00:21:47,900 --> 00:21:51,860
leave it alone, if you leave your roof alone eventually the elements will

232
00:21:51,860 --> 00:21:58,100
penetrate it, the mind is the same when you stop practicing meditation, when

233
00:21:58,100 --> 00:22:03,060
you leave it alone, all of it will come back, so this is a good way of

234
00:22:03,060 --> 00:22:05,940
protecting the mind that many people in the world use, but it's not the

235
00:22:05,940 --> 00:22:10,500
ultimate method of protecting the mind. The ultimate one is when you develop

236
00:22:10,500 --> 00:22:14,180
wisdom, as you're looking and you're seeing things clearly, you begin to

237
00:22:14,180 --> 00:22:19,140
penetrate the characteristics and the nature of things, the nature of reality,

238
00:22:19,140 --> 00:22:27,620
right? Our reality, even right here and now sitting here, is one of phenomena

239
00:22:27,620 --> 00:22:33,660
arising, sometimes we're seeing, sometimes we're hearing, smelling, tasting, feeling,

240
00:22:33,660 --> 00:22:41,660
thinking, we have our emotions and all of these things arising in season. We begin

241
00:22:41,660 --> 00:22:45,620
to see this and we're able to break them apart and see them from there, one by

242
00:22:45,620 --> 00:22:51,300
one by one, and we realize that there is no this idea of self, this idea of me

243
00:22:51,300 --> 00:22:56,700
being able to experience pleasure or become this or become that, it's really

244
00:22:56,700 --> 00:23:02,900
just a delusion. The attraction towards some being or something is just a

245
00:23:02,900 --> 00:23:10,140
mind state that arises and then arises in season. You begin to see that in the

246
00:23:10,140 --> 00:23:14,460
progression of mind states, when you give rise to lust, what happens? It creates

247
00:23:14,460 --> 00:23:18,380
attachment and you attach relief to disappointment when you don't get what you

248
00:23:18,380 --> 00:23:23,740
want, when you just like something that leads to anger and frustration and

249
00:23:23,740 --> 00:23:28,300
you're actually suffering? So when you see this, you start to lose interest, you

250
00:23:28,300 --> 00:23:32,820
start to give up, start to realize that nature doesn't admit a liking or

251
00:23:32,820 --> 00:23:39,820
disliking a good or bad, the liking and the disliking comes from creating ideas

252
00:23:39,820 --> 00:23:45,300
about it and ideas of self, that this is my experience and wanting that

253
00:23:45,300 --> 00:23:51,740
experience again for yourself. Once you just see it for what it is, your mind

254
00:23:51,740 --> 00:23:54,860
doesn't have anything to cling to, it doesn't have any interest in anything,

255
00:23:54,860 --> 00:23:59,940
doesn't say I want to be that, I want to be this, it's able to just experience.

256
00:23:59,940 --> 00:24:04,060
Once you do that, once you get to that point where you see things as they are

257
00:24:04,060 --> 00:24:09,500
because there's nothing positive or negative in anything really, you think

258
00:24:09,500 --> 00:24:14,540
just as good or you think this is bad, it's all in, it's all a concept that arises

259
00:24:14,540 --> 00:24:19,740
in your mind. When you see through that, then it doesn't matter what comes to you,

260
00:24:19,740 --> 00:24:26,220
this is the ultimate protection because nothing can penetrate. The reason why we

261
00:24:26,220 --> 00:24:31,820
have give rise to mics and dislikes, we give rise to must in the first place, is

262
00:24:31,820 --> 00:24:38,420
to ignorance, because ignorance is the default, not knowing, everything in the

263
00:24:38,420 --> 00:24:41,300
universe comes together, it doesn't come together out of knowledge, it just

264
00:24:41,300 --> 00:24:46,100
comes together in arises and develops. Wisdom is something that isn't inherent

265
00:24:46,100 --> 00:24:50,980
in the universe, wisdom has to come afterwards, and has to come as a result of

266
00:24:50,980 --> 00:24:59,140
all of this coming up, and the systematic observation and

267
00:24:59,140 --> 00:25:06,700
eventual understanding, once you understand, none of it can affect you, then you

268
00:25:06,700 --> 00:25:11,580
don't give rise to mics or this time, then your mind is perfectly well-fetched,

269
00:25:11,580 --> 00:25:15,220
and so that is the development that we're trying for here, we're trying to just

270
00:25:15,220 --> 00:25:20,220
come to understand things. It's not intellectual, it's by repeated observation

271
00:25:20,220 --> 00:25:29,580
and direct experience, once you see it again, then again, for what it is, your

272
00:25:29,580 --> 00:25:33,180
mind will change, your way of approaching things will change, you'll have no

273
00:25:33,180 --> 00:25:36,900
desire to chase after things, you're knowing that when you chase after it, you

274
00:25:36,900 --> 00:25:40,700
know what it leads to because you've seen that, you have no desire to be angry or

275
00:25:40,700 --> 00:25:45,260
upset at things or frustrated about things, because you know a frustration

276
00:25:45,260 --> 00:25:49,900
leads to, and furthermore you see these things for what they are, so you don't

277
00:25:49,900 --> 00:25:54,220
think of this as bad or this as good when something happens, you don't have any

278
00:25:54,220 --> 00:25:59,460
judgment for it, you see that it happens and you let it be, so this is the

279
00:25:59,460 --> 00:26:04,620
teaching for today, this is really what we're trying to start with morality,

280
00:26:04,620 --> 00:26:11,940
bringing a mind and then we develop a concentration which allows us to see clearly,

281
00:26:11,940 --> 00:26:18,340
our minds will be well-fetched, that's so well, in fact the word well is an

282
00:26:18,340 --> 00:26:26,580
understatement, they will be totally waterproof or lustproof in this case, so this

283
00:26:26,580 --> 00:26:37,660
is the teaching of the double body based on verses 13 and 14, thanks for tuning in and back to meditation for you.

