1
00:00:00,000 --> 00:00:10,000
Very good to see Bhikwuni Palanyani and I would like to request her to clarify my confusion.

2
00:00:10,000 --> 00:00:17,000
My confusion is how we Baba Tanha can be explained in the context of sentient beings

3
00:00:17,000 --> 00:00:23,000
striving to free themselves from Samsara, striving to attain Devan.

4
00:00:23,000 --> 00:00:33,000
Whether it is good to have we Baba Tanha to break free of Baba Jati.

5
00:00:33,000 --> 00:00:43,000
I think if you understand the question, I'm just pondering over if I understand the question, I think I do.

6
00:00:43,000 --> 00:00:49,000
Because I think the idea is that striving for Nibana is striving for extinction.

7
00:00:49,000 --> 00:00:52,000
So this could be considered, we Baba Tanha.

8
00:00:52,000 --> 00:00:55,000
That's the assumption here.

9
00:00:55,000 --> 00:01:20,000
I don't want to say, because I'm not the Pali expert, so I don't want to say that we Baba Tanha

10
00:01:20,000 --> 00:01:35,000
is the exact thing that you need to have in your mind in order to break free from the rounds of rebirth

11
00:01:35,000 --> 00:01:48,000
or to get rid of your clinging so that you can overcome becoming in birth.

12
00:01:48,000 --> 00:01:56,000
But what you need is a certain wanting to practice.

13
00:01:56,000 --> 00:02:08,000
You need to have that deep desire to practice and to meditate and to strive to be free from Samsara and to attain Nibana.

14
00:02:08,000 --> 00:02:22,000
Otherwise, you wouldn't have the power, the force to go through all the things that may arise during your course of meditation.

15
00:02:22,000 --> 00:02:35,000
There are many things coming up in your mind which will make you wanting to give up, wanting to run away, wanting to clean first,

16
00:02:35,000 --> 00:02:42,000
and wanting to do this and that.

17
00:02:42,000 --> 00:02:49,000
So it is important to keep the mind with the desire to become enlightened,

18
00:02:49,000 --> 00:03:07,000
fixed on the go, on the go Nibana. But I don't think that Tanha is the right word for it.

19
00:03:07,000 --> 00:03:20,000
So because Tanha is more like a lust or so and that's probably not the correct desire in that sense.

20
00:03:20,000 --> 00:03:44,000
We power Tanha is the desire to be, to not be, and I think that is both the desire to be free from suffering or the desire,

21
00:03:44,000 --> 00:03:52,000
not to be in this Samsara anymore, is really what it needs.

22
00:03:52,000 --> 00:04:01,000
It needs more clarity than the lust that Baha'u are.

23
00:04:01,000 --> 00:04:26,000
So I would say it's more a chanda. If we want to use poly terms, I would maybe use chanda in order to what you need to strive to get to Nibana.

24
00:04:26,000 --> 00:04:31,000
But I think Bantik can explain far better than I.

25
00:04:31,000 --> 00:04:35,000
So please start telling me about Nibana.

26
00:04:35,000 --> 00:04:40,000
I don't have much from a technical point of view to mention.

27
00:04:40,000 --> 00:04:47,000
I want to use this as an opportunity to talk about what we were talking about in regards to the meta thing.

28
00:04:47,000 --> 00:04:53,000
Because it came up this morning, this early this morning I was with a group from Los Angeles.

29
00:04:53,000 --> 00:04:58,000
And they asked a question, we have three questions here and I want to link them together.

30
00:04:58,000 --> 00:05:02,000
I'm going to show you the three questions and link them together with one concept.

31
00:05:02,000 --> 00:05:10,000
Because it seems to be more and more that this is the same problem that's occurring on many different fronts.

32
00:05:10,000 --> 00:05:14,000
This morning the question was about equanimity.

33
00:05:14,000 --> 00:05:21,000
We want to develop equanimity, but then what place does joy have in the practice?

34
00:05:21,000 --> 00:05:26,000
So if we're just going to be equanimity, what happens to our joy?

35
00:05:26,000 --> 00:05:38,000
The question today, or it wasn't a question, but the question that sort of arose in my mind was,

36
00:05:38,000 --> 00:05:46,000
if a person has defilements in their mind, what good is it to practice loving kindness meditation?

37
00:05:46,000 --> 00:05:52,000
What good is the actual practice of loving kindness meditation when they don't really mean it?

38
00:05:52,000 --> 00:05:57,000
You sit down and you say it, but it's an artifice.

39
00:05:57,000 --> 00:05:59,000
It's artificial, you say.

40
00:05:59,000 --> 00:06:03,000
You have these people, I wish for everyone to be happy.

41
00:06:03,000 --> 00:06:09,000
And then you get them practicing in the past and then they explode into anger and frustration.

42
00:06:09,000 --> 00:06:18,000
And then they get quite turned off by we pass in them because they think that it's making them an anger person.

43
00:06:18,000 --> 00:06:25,000
And the question here now is this question that always comes up.

44
00:06:25,000 --> 00:06:27,000
You say we want to give up craving.

45
00:06:27,000 --> 00:06:29,000
Well, what about the craving to become enlightened?

46
00:06:29,000 --> 00:06:31,000
What about the desire to become enlightened?

47
00:06:31,000 --> 00:06:34,000
And someone asked, I think we get all sorts of questions from everywhere.

48
00:06:34,000 --> 00:06:35,000
Most of them we don't answer.

49
00:06:35,000 --> 00:06:41,000
I don't answer like on YouTube or here, far too many, but there was one interesting one today about,

50
00:06:41,000 --> 00:06:46,000
I think, asked on the forum recently, something along this line.

51
00:06:46,000 --> 00:06:50,000
Anyway, we get this in many different forms.

52
00:06:50,000 --> 00:06:52,000
So let's go over these.

53
00:06:52,000 --> 00:06:56,000
The answer that comes to mind is the same in all three.

54
00:06:56,000 --> 00:07:02,000
We have to understand the difference between an emotive state and an intellectual state.

55
00:07:02,000 --> 00:07:05,000
I think it's quite important.

56
00:07:05,000 --> 00:07:13,000
The first thing you need to do with the Buddha said, before you begin to practice, is set your mind in right view and morality.

57
00:07:13,000 --> 00:07:15,000
Put morality aside.

58
00:07:15,000 --> 00:07:18,000
Let's look at this concept of right view.

59
00:07:18,000 --> 00:07:20,000
Right view is actually what comes at the end.

60
00:07:20,000 --> 00:07:22,000
It's the realization of the four noble truths.

61
00:07:22,000 --> 00:07:24,000
A sotepan that has right view.

62
00:07:24,000 --> 00:07:26,000
The person who has become enlightened, they have right view.

63
00:07:26,000 --> 00:07:31,000
But here the Buddha saying, before you start, you need to have right view.

64
00:07:31,000 --> 00:07:37,000
Most definitely, what he is referring to, is intellectual right view.

65
00:07:37,000 --> 00:07:41,000
This is why listening to the Dharma is important in the beginning.

66
00:07:41,000 --> 00:07:49,000
This is why reciting the Buddha's teaching is important to change the way you're looking at things.

67
00:07:49,000 --> 00:07:58,000
The question about Upeka, the answer there is, when a person is equanimus, it doesn't mean they feel equanimity or the equanimus feeling.

68
00:07:58,000 --> 00:08:04,000
Sankarrupekanyan, the highest state of Yipasana insight before you enter into Nibana.

69
00:08:04,000 --> 00:08:09,000
It doesn't mean you always have a neutral feeling.

70
00:08:09,000 --> 00:08:12,000
It means when there is a happy feeling, you see it with equanimity.

71
00:08:12,000 --> 00:08:14,000
That's a happy feeling.

72
00:08:14,000 --> 00:08:15,000
You don't judge it.

73
00:08:15,000 --> 00:08:22,000
Intellectually, there arises no intellectual activity.

74
00:08:22,000 --> 00:08:25,000
There's no, oh, this is my good feeling.

75
00:08:25,000 --> 00:08:29,000
When there's a painful feeling, you see it, equanimously.

76
00:08:29,000 --> 00:08:33,000
Indifferently, or impartially, sorry, is the word.

77
00:08:33,000 --> 00:08:38,000
We use the word equanimus, and you say an enlightened person is always equanimus.

78
00:08:38,000 --> 00:08:40,000
That's kind of dull.

79
00:08:40,000 --> 00:08:42,000
What about joy?

80
00:08:42,000 --> 00:08:46,000
And as we learned in Abhidhamma yesterday, enlightened people actually experience joy.

81
00:08:46,000 --> 00:08:47,000
They smile.

82
00:08:47,000 --> 00:08:49,000
And when they smile, there's joy.

83
00:08:49,000 --> 00:08:56,000
It's a soma nasa.

84
00:08:56,000 --> 00:08:59,000
Even that pleasant feeling, the arahan doesn't become attached to it.

85
00:08:59,000 --> 00:09:02,000
That's what we mean by equanimity.

86
00:09:02,000 --> 00:09:09,000
The mind, the intellectual activity, is seeing, oh, this is a happy feeling.

87
00:09:09,000 --> 00:09:12,000
Seeing it as a happy feeling.

88
00:09:12,000 --> 00:09:18,000
With loving kindness, the reason I think why it actually might be good for people to practice

89
00:09:18,000 --> 00:09:24,000
at certain times loving kindness meditation is not necessarily because they're going to develop

90
00:09:24,000 --> 00:09:33,000
loving kindness out of it, even though they might, is that it changes the way they look

91
00:09:33,000 --> 00:09:34,000
at this situation.

92
00:09:34,000 --> 00:09:40,000
So before they practiced the meditation, they were not only angry.

93
00:09:40,000 --> 00:09:43,000
This is what I explained in my idea today.

94
00:09:43,000 --> 00:09:49,000
They were not only angry at the person, but they also had this idea or this feeling or this

95
00:09:49,000 --> 00:09:57,000
intellectual idea that that person deserves or I didn't deserve what the person did to me

96
00:09:57,000 --> 00:10:01,000
or the idea that this is how it should be.

97
00:10:01,000 --> 00:10:08,000
I'm so angry at this person or just the idea, I'm so angry at this person.

98
00:10:08,000 --> 00:10:12,000
And when you sit down and say, may that person be happy, you're like, but no, I want the

99
00:10:12,000 --> 00:10:13,000
person to suffer.

100
00:10:13,000 --> 00:10:21,000
So you may not change the anger, but you change, and this is what you really feel, especially if

101
00:10:21,000 --> 00:10:26,000
you put it with repass in the meditation, because when you practice, we pass in the meditation

102
00:10:26,000 --> 00:10:30,000
is very easy to lose sight of the view.

103
00:10:30,000 --> 00:10:33,000
You see the anger, okay, I'm very angry at this person.

104
00:10:33,000 --> 00:10:41,000
So you say there's an angry angry, but deep down you're like, man, I want to really hurt

105
00:10:41,000 --> 00:10:44,000
that person.

106
00:10:44,000 --> 00:10:49,000
When you change the view or you feel self-righteous about you say, you know, that person,

107
00:10:49,000 --> 00:10:50,000
how could they do that to me?

108
00:10:50,000 --> 00:10:51,000
How could they do that to me?

109
00:10:51,000 --> 00:10:55,000
And you just say angry angry and it doesn't go away, because you're still thinking, how

110
00:10:55,000 --> 00:10:56,000
could they do that to me?

111
00:10:56,000 --> 00:11:00,000
That person's such an evil person or so on.

112
00:11:00,000 --> 00:11:04,000
And when you practice loving kindness, you say, may that person be happy, you're changing

113
00:11:04,000 --> 00:11:07,000
your idea, you're changing your outlook, you're changing the intellectual activity.

114
00:11:07,000 --> 00:11:10,000
So you might still be angry, but you say, oh, look at this.

115
00:11:10,000 --> 00:11:12,000
Look at this anger that I have inside.

116
00:11:12,000 --> 00:11:14,000
You're no longer thinking about the person.

117
00:11:14,000 --> 00:11:17,000
You've diffused that intellectual activity.

118
00:11:17,000 --> 00:11:23,000
You've changed the way you see the anger, the way you see the situation.

119
00:11:23,000 --> 00:11:25,000
And I think that can help.

120
00:11:25,000 --> 00:11:28,000
These two are not what you're asking.

121
00:11:28,000 --> 00:11:37,000
But when we talk about desire for becoming enlightened, wanting to practice,

122
00:11:37,000 --> 00:11:45,000
the desire or wanting or attachment even to the practice,

123
00:11:45,000 --> 00:11:50,000
you don't need a emotion of clinging.

124
00:11:50,000 --> 00:11:54,000
And I think I've said this before in a video or somewhere.

125
00:11:54,000 --> 00:11:57,000
When you say you want to become enlightened, what does that mean?

126
00:11:57,000 --> 00:11:59,000
That doesn't mean that here's enlightenment.

127
00:11:59,000 --> 00:12:01,000
You say, whoo, that's wonderful.

128
00:12:01,000 --> 00:12:02,000
That's mine.

129
00:12:02,000 --> 00:12:03,000
That's good.

130
00:12:03,000 --> 00:12:04,000
I want more of that.

131
00:12:04,000 --> 00:12:05,000
I want to get that.

132
00:12:05,000 --> 00:12:08,000
Why we say you have desire for becoming enlightened,

133
00:12:08,000 --> 00:12:14,000
and why people can appear to have great desire to become enlightened?

134
00:12:14,000 --> 00:12:17,000
Because consider the desire to become a movie star.

135
00:12:17,000 --> 00:12:21,000
Totally tainted with not the desire to be a movie star.

136
00:12:21,000 --> 00:12:24,000
The desire to have people say, oh, you're so beautiful.

137
00:12:24,000 --> 00:12:28,000
Oh, can I have your autograph and feeling good about yourself?

138
00:12:28,000 --> 00:12:34,000
And having people esteem you and having money and having fame and having power and so on.

139
00:12:34,000 --> 00:12:37,000
This is why we want to be famous.

140
00:12:37,000 --> 00:12:40,000
The reason why we want to become enlightened is totally different.

141
00:12:40,000 --> 00:12:42,000
There is none of that lust, none of that greed.

142
00:12:42,000 --> 00:12:44,000
There is none of the Tanha involved.

143
00:12:44,000 --> 00:12:45,000
Tanha takes an object.

144
00:12:45,000 --> 00:12:48,000
What is the object of wanting to become enlightened?

145
00:12:48,000 --> 00:12:50,000
If it's Tanha, what is the object of the Tanha?

146
00:12:50,000 --> 00:12:53,000
Nibana, you don't even know what Nibana is.

147
00:12:53,000 --> 00:12:59,000
It can arise for a meditator that they think, oh, if I were enlightened,

148
00:12:59,000 --> 00:13:02,000
then people would esteem me, then everyone would look at me,

149
00:13:02,000 --> 00:13:05,000
and then I'd be able to teach people and so on.

150
00:13:05,000 --> 00:13:07,000
And that's a kind of a Tanha.

151
00:13:07,000 --> 00:13:09,000
That's a kind of a craving.

152
00:13:09,000 --> 00:13:13,000
Or you can have this pain.

153
00:13:13,000 --> 00:13:17,000
We bow a Tanha, and you say, oh, this pain is so awful.

154
00:13:17,000 --> 00:13:19,000
My wish it would go away.

155
00:13:19,000 --> 00:13:20,000
That's we bow a Tanha.

156
00:13:20,000 --> 00:13:25,000
The pain is the object, and you want for it to go away.

157
00:13:25,000 --> 00:13:29,000
And this actually gets quite close to something positive, right?

158
00:13:29,000 --> 00:13:32,000
Because the Buddha said, well, we want to be free from suffering.

159
00:13:32,000 --> 00:13:34,000
So isn't it that when you experience pain?

160
00:13:34,000 --> 00:13:36,000
You think, no, no, go away.

161
00:13:36,000 --> 00:13:37,000
Absolutely not.

162
00:13:37,000 --> 00:13:39,000
That has no benefit.

163
00:13:39,000 --> 00:13:41,000
The Buddha said, parinayanti.

164
00:13:41,000 --> 00:13:46,000
Dukasam dukangariya said, jangparinayanti.

165
00:13:46,000 --> 00:13:51,000
This noble truth of suffering has to be fully understand, understood,

166
00:13:51,000 --> 00:13:53,000
seen thoroughly.

167
00:13:53,000 --> 00:13:57,000
It means when there's pain, you have to say, okay, let's look at the pain.

168
00:13:57,000 --> 00:14:01,000
When there's anything arises, you have to be willing to see it objectively

169
00:14:01,000 --> 00:14:04,000
for what it is thoroughly and completely.

170
00:14:04,000 --> 00:14:05,000
See it for what it is.

171
00:14:05,000 --> 00:14:08,000
What do we mean when we say desire to practice?

172
00:14:08,000 --> 00:14:10,000
And the intention to practice.

173
00:14:10,000 --> 00:14:13,000
And the strong, as Bhikkhuni said,

174
00:14:13,000 --> 00:14:17,000
what can become a very strong, you know,

175
00:14:17,000 --> 00:14:22,000
emphatic desire to become enlightened is intellectual.

176
00:14:22,000 --> 00:14:26,000
It's not quite intellectual, but it's in the mind.

177
00:14:26,000 --> 00:14:33,000
It has nothing to do with the sensation of liking something or wanting something.

178
00:14:33,000 --> 00:14:34,000
You see suffering.

179
00:14:34,000 --> 00:14:36,000
You see what's going on in your mind.

180
00:14:36,000 --> 00:14:37,000
You see your attachments.

181
00:14:37,000 --> 00:14:39,000
You know where they're going to lead.

182
00:14:39,000 --> 00:14:45,000
And the more you see, the more you know, the stronger is this chanda.

183
00:14:45,000 --> 00:14:53,000
But it isn't done, it's just a mind state that arises with intense energy and effort

184
00:14:53,000 --> 00:14:55,000
directed towards the practice.

185
00:14:55,000 --> 00:15:02,000
This single pointed goal of becoming enlightened,

186
00:15:02,000 --> 00:15:06,000
not because you think, oh, that's going to be delicious,

187
00:15:06,000 --> 00:15:08,000
or that's going to be beautiful or something,

188
00:15:08,000 --> 00:15:13,000
but saying, this is suffering, this is suffering.

189
00:15:13,000 --> 00:15:14,000
This is the cause of suffering.

190
00:15:14,000 --> 00:15:16,000
That's the cause of suffering.

191
00:15:16,000 --> 00:15:18,000
And letting it go.

192
00:15:18,000 --> 00:15:26,000
And leaving off the activity of attaining it.

193
00:15:26,000 --> 00:15:30,000
And seeing this is the way to cessation of suffering.

194
00:15:30,000 --> 00:15:32,000
This is the way to freedom.

195
00:15:32,000 --> 00:15:34,000
This is the correct thing to do.

196
00:15:34,000 --> 00:15:37,000
It's an intellectual.

197
00:15:37,000 --> 00:15:40,000
It's the knowledge that this is the right way.

198
00:15:40,000 --> 00:15:43,000
And that knowledge can be quite powerful.

199
00:15:43,000 --> 00:15:46,000
It leads to chanda, the contentment with the practice.

200
00:15:46,000 --> 00:15:48,000
It leads to media.

201
00:15:48,000 --> 00:15:50,000
It leads you to have great effort in the practice.

202
00:15:50,000 --> 00:15:52,000
But it's not like, I'm going to get away from this suffering.

203
00:15:52,000 --> 00:15:54,000
This is horrible.

204
00:15:54,000 --> 00:15:58,000
It's totally impartial, really.

205
00:15:58,000 --> 00:16:01,000
You're impartially active, right?

206
00:16:01,000 --> 00:16:03,000
People that we don't understand, because you think, well,

207
00:16:03,000 --> 00:16:08,000
if you have all that energy and all that, that, that, that zest and that zeal,

208
00:16:08,000 --> 00:16:10,000
you must want it.

209
00:16:10,000 --> 00:16:11,000
And that's what I think.

210
00:16:11,000 --> 00:16:13,000
If you're going to go off and live in the forest as a monk,

211
00:16:13,000 --> 00:16:15,000
man, those guys must be really attached to being a monk

212
00:16:15,000 --> 00:16:17,000
and being enlightened, becoming enlightened.

213
00:16:17,000 --> 00:16:19,000
But it doesn't have to be that way.

214
00:16:19,000 --> 00:16:24,000
When you see things clearly, the same effort arises out of knowledge,

215
00:16:24,000 --> 00:16:26,000
out of wisdom.

216
00:16:26,000 --> 00:16:29,000
The more you know that something is proper,

217
00:16:29,000 --> 00:16:32,000
the more of your energy you're going to put into it.

218
00:16:32,000 --> 00:16:35,000
And if you see this is the only thing that has meaning,

219
00:16:35,000 --> 00:16:37,000
that has benefit.

220
00:16:37,000 --> 00:16:41,000
The amount of effort that you can put into it is quite amazing, actually.

221
00:16:41,000 --> 00:16:45,000
Meditators, they come, sometimes we ask our meditators to,

222
00:16:45,000 --> 00:16:48,000
you know, practice meditation continuously.

223
00:16:48,000 --> 00:16:51,000
And maybe they, sometimes they go with little sleep,

224
00:16:51,000 --> 00:16:52,000
or they don't even sleep.

225
00:16:52,000 --> 00:16:54,000
And they think, oh, that's horrible, that's terrible.

226
00:16:54,000 --> 00:16:56,000
And then sometimes you find them later on saying,

227
00:16:56,000 --> 00:16:59,000
I'm just going to practice all night and just do meditation

228
00:16:59,000 --> 00:17:01,000
and doing it by themselves.

229
00:17:01,000 --> 00:17:03,000
And again, this is, this is great.

230
00:17:03,000 --> 00:17:04,000
This is wonderful.

231
00:17:04,000 --> 00:17:08,000
Because they see, they see that this is the way.

232
00:17:08,000 --> 00:17:11,000
They don't have, there's not something that is saying,

233
00:17:11,000 --> 00:17:14,000
oh, this is going to be nice.

234
00:17:14,000 --> 00:17:16,000
Or, and they also don't have this,

235
00:17:16,000 --> 00:17:18,000
sorry, on your side, you're talking about,

236
00:17:18,000 --> 00:17:19,000
we bhavatana.

237
00:17:19,000 --> 00:17:22,000
We bhavatana has an object that you get upset about.

238
00:17:22,000 --> 00:17:25,000
You want it not to exist.

239
00:17:25,000 --> 00:17:27,000
It can be associated with,

240
00:17:27,000 --> 00:17:31,000
or it can be followed by patiga, which is the anger minds.

241
00:17:31,000 --> 00:17:34,000
But even an anagami has vhavatana,

242
00:17:34,000 --> 00:17:37,000
which means they still have this desire for things not to be.

243
00:17:37,000 --> 00:17:40,000
When a person becomes an arahant,

244
00:17:40,000 --> 00:17:43,000
they become an arahant, not because of vhavatana,

245
00:17:43,000 --> 00:17:45,000
but in spite of it.

246
00:17:45,000 --> 00:17:48,000
The reason why they become the arahant

247
00:17:48,000 --> 00:17:51,000
is not because of their desire or this suffering

248
00:17:51,000 --> 00:17:54,000
or these bad things may they go away.

249
00:17:54,000 --> 00:18:01,000
It's the knowledge that this is meaningless

250
00:18:01,000 --> 00:18:03,000
and that this is the correct way.

251
00:18:03,000 --> 00:18:05,000
The path out of suffering is the correct way.

252
00:18:05,000 --> 00:18:10,000
The vhavatana is just a course side effect

253
00:18:10,000 --> 00:18:16,000
that is a desire-based mind.

254
00:18:16,000 --> 00:18:19,000
And it actually had the repercussions of it

255
00:18:19,000 --> 00:18:21,000
are that to be reborn.

256
00:18:21,000 --> 00:18:24,000
This is why the anagami will still be reborn

257
00:18:24,000 --> 00:18:26,000
because they have bhavatana and vhavatana.

258
00:18:26,000 --> 00:18:29,000
They still have partiality.

259
00:18:29,000 --> 00:18:34,000
Anyway, I hope that helped.

260
00:18:34,000 --> 00:18:41,000
I have thoughts when you were speaking about it.

261
00:18:41,000 --> 00:18:44,000
I remember the story of Ananda,

262
00:18:44,000 --> 00:18:50,000
who was that I unfortunately can't remember any

263
00:18:50,000 --> 00:18:53,000
of the Sutra where it was,

264
00:18:53,000 --> 00:18:57,000
but he explained very exactly

265
00:18:57,000 --> 00:19:03,000
that you need the wanting in order to overcome the wanting.

266
00:19:03,000 --> 00:19:08,000
The wanting to practice in order to become in light

267
00:19:08,000 --> 00:19:11,000
and lose the wanting.

268
00:19:11,000 --> 00:19:15,000
And the other thing was I was thinking of that

269
00:19:15,000 --> 00:19:21,000
the Buddha touched his cousin Nanda with a trick.

270
00:19:21,000 --> 00:19:24,000
We talked about this one before.

271
00:19:24,000 --> 00:19:26,000
No, he had this video before.

272
00:19:26,000 --> 00:19:29,000
So in the beginning,

273
00:19:29,000 --> 00:19:32,000
when the person is new to meditation,

274
00:19:32,000 --> 00:19:35,000
does not know anything about it.

275
00:19:35,000 --> 00:19:41,000
The desire can come from everywhere.

276
00:19:41,000 --> 00:19:44,000
It can even be caught by a trick

277
00:19:44,000 --> 00:19:47,000
and lead you into this direction

278
00:19:47,000 --> 00:19:51,000
as the Buddha did it with his cousin Nanda.

279
00:19:51,000 --> 00:19:53,000
And the Buddha told him,

280
00:19:53,000 --> 00:19:55,000
you can have celestial,

281
00:19:55,000 --> 00:20:00,000
beautiful being even more beautiful

282
00:20:00,000 --> 00:20:04,000
than your bride that is crying for you there now.

283
00:20:04,000 --> 00:20:08,000
I'm cutting this story very short now.

284
00:20:08,000 --> 00:20:13,000
And Nanda trusted kind of the Buddha

285
00:20:13,000 --> 00:20:16,000
and practiced meditation

286
00:20:16,000 --> 00:20:19,000
because of Bhavatana,

287
00:20:19,000 --> 00:20:23,000
because of wanting and desire.

288
00:20:23,000 --> 00:20:26,000
And he became enlightened.

289
00:20:26,000 --> 00:20:32,000
Yeah, but let's look first at Nanda.

290
00:20:32,000 --> 00:20:35,000
And Nanda's story,

291
00:20:35,000 --> 00:20:40,000
he's talking about a specific type of desire.

292
00:20:40,000 --> 00:20:43,000
And this is why it's not that he says

293
00:20:43,000 --> 00:20:45,000
you have desire for getting rid of desire.

294
00:20:45,000 --> 00:20:48,000
He says, this is the kind of desire

295
00:20:48,000 --> 00:20:53,000
that leads to that desire ceasing.

296
00:20:53,000 --> 00:20:58,000
It doesn't mean that you can have Dhanha lead you to be free from Dhanha.

297
00:20:58,000 --> 00:21:01,000
It means there is something that we call desire

298
00:21:01,000 --> 00:21:04,000
because Nanda even uses the simile.

299
00:21:04,000 --> 00:21:06,000
He inserts it in there.

300
00:21:06,000 --> 00:21:08,000
I don't know the poly words, but he said,

301
00:21:08,000 --> 00:21:09,000
did you have the desire?

302
00:21:09,000 --> 00:21:12,000
Didn't you have the desire, the intention?

303
00:21:12,000 --> 00:21:15,000
Because he's explaining it's not desire.

304
00:21:15,000 --> 00:21:17,000
It's just an intention to come here.

305
00:21:17,000 --> 00:21:19,000
You didn't think, whoa, that's sexy.

306
00:21:19,000 --> 00:21:21,000
Nanda, I'm going to go to see him.

307
00:21:21,000 --> 00:21:24,000
It would be good for me to go to see Nanda today.

308
00:21:24,000 --> 00:21:26,000
That's not Dhanha.

309
00:21:26,000 --> 00:21:30,000
There's not necessarily the craving for it to arise

310
00:21:30,000 --> 00:21:32,000
or the attachment to it.

311
00:21:32,000 --> 00:21:34,000
They can arise the thought, this is appropriate to do now.

312
00:21:34,000 --> 00:21:37,000
And then there's the walking and then there's the going.

313
00:21:37,000 --> 00:21:41,000
Once you get there, then the intention has been fulfilled.

314
00:21:41,000 --> 00:21:47,000
I still think, you know, I'm obviously not the authority on this.

315
00:21:47,000 --> 00:21:49,000
But that what is being talked about here

316
00:21:49,000 --> 00:21:52,000
is the intellectual, this would be good,

317
00:21:52,000 --> 00:21:54,000
or this would be appropriate, which doesn't necessarily

318
00:21:54,000 --> 00:21:55,000
have to have done that.

319
00:21:55,000 --> 00:22:01,000
Now, with Nanda, he didn't become enlightened thinking

320
00:22:01,000 --> 00:22:05,000
that he was going to become a God with celestial nymphs

321
00:22:05,000 --> 00:22:06,000
surrounding him.

322
00:22:06,000 --> 00:22:09,000
What the Buddha did was more devious than that.

323
00:22:09,000 --> 00:22:12,000
Nanda had Dhanha in him.

324
00:22:12,000 --> 00:22:20,000
His wife was Jana Padakaliani, the beauty of the land.

325
00:22:20,000 --> 00:22:21,000
That's what her name meant.

326
00:22:21,000 --> 00:22:23,000
It probably wasn't even her name.

327
00:22:23,000 --> 00:22:27,000
It was just the beauty of the most beautiful woman in the country.

328
00:22:27,000 --> 00:22:30,000
Miss India, you could say.

329
00:22:30,000 --> 00:22:34,000
And so who wouldn't do?

330
00:22:34,000 --> 00:22:36,000
He was always thinking about her,

331
00:22:36,000 --> 00:22:39,000
and he was always unhappy as a monk.

332
00:22:39,000 --> 00:22:41,000
And so what did the Buddha have to do?

333
00:22:41,000 --> 00:22:44,000
What the Buddha did is showed him his last.

334
00:22:44,000 --> 00:22:47,000
So it was okay.

335
00:22:47,000 --> 00:22:50,000
Took him to heaven, gave him all this last in these angels.

336
00:22:50,000 --> 00:22:51,000
Ooh, these angels.

337
00:22:51,000 --> 00:22:52,000
They're wonderful.

338
00:22:52,000 --> 00:22:53,000
These nymphs.

339
00:22:53,000 --> 00:22:54,000
These dove for the nymphs.

340
00:22:54,000 --> 00:22:55,000
Well, they will be yours.

341
00:22:55,000 --> 00:22:59,000
And so you think, well, somehow that was helpful.

342
00:22:59,000 --> 00:23:02,000
But no, what was helpful was what he did next.

343
00:23:02,000 --> 00:23:04,000
He went around to all the monks and said,

344
00:23:04,000 --> 00:23:06,000
hey, guess what?

345
00:23:06,000 --> 00:23:07,000
I guess what I did.

346
00:23:07,000 --> 00:23:12,000
Or it doesn't say that the Buddha did that, but the monks found out about it.

347
00:23:12,000 --> 00:23:15,000
And because the monks found out about it,

348
00:23:15,000 --> 00:23:17,000
what do you think that did to him?

349
00:23:17,000 --> 00:23:18,000
How do you think that made him feel?

350
00:23:18,000 --> 00:23:20,000
When all of the monks were saying,

351
00:23:20,000 --> 00:23:22,000
oh, so here's this higherling,

352
00:23:22,000 --> 00:23:27,000
this person who practices meditation for a bribe.

353
00:23:27,000 --> 00:23:31,000
And that was what made him change his mind.

354
00:23:31,000 --> 00:23:33,000
That's what made him feel ashamed

355
00:23:33,000 --> 00:23:38,000
and decide to realize how disgusting this was.

356
00:23:38,000 --> 00:23:40,000
The other part of how disgusting,

357
00:23:40,000 --> 00:23:42,000
you can see how disgusting is.

358
00:23:42,000 --> 00:23:45,000
One of the reasons why I ended up running away to Thailand

359
00:23:45,000 --> 00:23:49,000
to become a monk is because it was exactly that.

360
00:23:49,000 --> 00:23:51,000
I thought I was in love with someone.

361
00:23:51,000 --> 00:23:52,000
I love with this person.

362
00:23:52,000 --> 00:23:54,000
And then someone else comes along,

363
00:23:54,000 --> 00:23:55,000
oh, yes, this person.

364
00:23:55,000 --> 00:23:57,000
When someone else comes along, this person,

365
00:23:57,000 --> 00:24:02,000
I found myself, the mind was just going crazy with this desire

366
00:24:02,000 --> 00:24:04,000
for one thing and another.

367
00:24:04,000 --> 00:24:07,000
And you realize that truly what the Buddha said was right,

368
00:24:07,000 --> 00:24:10,000
the mind desire is like a fire.

369
00:24:10,000 --> 00:24:12,000
You can't save a fire, let it stay in this tree

370
00:24:12,000 --> 00:24:14,000
and not go to that tree.

371
00:24:14,000 --> 00:24:17,000
I'm going to light a fire and it's only going to stay in this wood.

372
00:24:17,000 --> 00:24:19,000
Even if this wood touches the fire,

373
00:24:19,000 --> 00:24:21,000
this wood isn't going to burn.

374
00:24:21,000 --> 00:24:23,000
Or you can light a fire and say,

375
00:24:23,000 --> 00:24:27,000
let it stay in the logs and not go to the grass,

376
00:24:27,000 --> 00:24:30,000
or let it not go to the leaves on the ground,

377
00:24:30,000 --> 00:24:34,000
or let it not spread to other things.

378
00:24:34,000 --> 00:24:36,000
You can't say this of desire,

379
00:24:36,000 --> 00:24:38,000
you can't say this of a fire.

380
00:24:38,000 --> 00:24:41,000
And so for sure he must have seen this,

381
00:24:41,000 --> 00:24:45,000
because this is the intellectual side of it.

382
00:24:45,000 --> 00:24:46,000
So he had the lust for her,

383
00:24:46,000 --> 00:24:48,000
but he also had the intellect thinking the view,

384
00:24:48,000 --> 00:24:50,000
thinking she's my wife,

385
00:24:50,000 --> 00:24:52,000
and she's my soul mate.

386
00:24:52,000 --> 00:24:54,000
People always talk about their soul mates,

387
00:24:54,000 --> 00:24:58,000
and it's kind of funny because the mind is so fickle.

388
00:24:58,000 --> 00:25:01,000
You can mourn about someone for years,

389
00:25:01,000 --> 00:25:05,000
and you can forget about someone in a moment.

390
00:25:05,000 --> 00:25:09,000
And so when the Buddha showed him the doubt,

391
00:25:09,000 --> 00:25:12,000
but his nymphs he must have felt kind of silly, right?

392
00:25:12,000 --> 00:25:13,000
About all of this.

393
00:25:13,000 --> 00:25:16,000
Oh, maybe she wasn't my soul mate after all.

394
00:25:16,000 --> 00:25:18,000
Well, then who's my soul mate?

395
00:25:18,000 --> 00:25:21,000
And you give up the view about this desire

396
00:25:21,000 --> 00:25:23,000
has some meaning that a wife,

397
00:25:23,000 --> 00:25:25,000
a husband and wife has some meaning

398
00:25:25,000 --> 00:25:28,000
that the life of a householder has some meaning,

399
00:25:28,000 --> 00:25:31,000
the life of human beings and procreation,

400
00:25:31,000 --> 00:25:32,000
and so on as a meaning,

401
00:25:32,000 --> 00:25:35,000
because suddenly it's totally taken out of context,

402
00:25:35,000 --> 00:25:38,000
and all that's left is his desire for doubt-fooded nymphs,

403
00:25:38,000 --> 00:25:41,000
which is ridiculous, really.

404
00:25:41,000 --> 00:25:48,000
And I would be willing to bet that that had much more of a part

405
00:25:48,000 --> 00:25:52,000
to play in his enlightenment, but I just don't want to say

406
00:25:52,000 --> 00:25:55,000
that the violence can lead to enlightenment.

407
00:25:55,000 --> 00:26:06,000
I mentioned that story because this is one thing.

408
00:26:06,000 --> 00:26:12,000
It is one thing that when you don't know anything about meditation,

409
00:26:12,000 --> 00:26:19,000
there can be a kind of incentive

410
00:26:19,000 --> 00:26:22,000
that makes you want to practice.

411
00:26:22,000 --> 00:26:26,000
And then the other thing is once a person becomes a stream

412
00:26:26,000 --> 00:26:31,000
and her, Nibana becomes the object,

413
00:26:31,000 --> 00:26:34,000
so things change.

414
00:26:34,000 --> 00:26:37,000
Once you get on a path,

415
00:26:37,000 --> 00:26:42,000
you won't do that for nymphs anymore,

416
00:26:42,000 --> 00:26:46,000
but you know what you're going for,

417
00:26:46,000 --> 00:26:48,000
because you have a glimpse of it.

418
00:26:48,000 --> 00:26:53,000
But it's an interesting idea,

419
00:26:53,000 --> 00:26:57,000
and it's one that I'm sure is endorsed.

420
00:26:57,000 --> 00:26:58,000
I could probably,

421
00:26:58,000 --> 00:27:00,000
it seems to me that there are many examples

422
00:27:00,000 --> 00:27:02,000
of how this sort of thing works,

423
00:27:02,000 --> 00:27:05,000
but you have to be careful in practice

424
00:27:05,000 --> 00:27:08,000
because you see in Buddhist countries,

425
00:27:08,000 --> 00:27:11,000
for example, where they do this kind of thing,

426
00:27:11,000 --> 00:27:15,000
and then you get very, very corrupt monks that come.

427
00:27:15,000 --> 00:27:18,000
The reason a person is able to become a Sotabana

428
00:27:18,000 --> 00:27:20,000
or not become a Sotabana,

429
00:27:20,000 --> 00:27:23,000
not every meditator or monk or so on

430
00:27:23,000 --> 00:27:25,000
will ever even come close.

431
00:27:25,000 --> 00:27:27,000
It has to be because of the good things

432
00:27:27,000 --> 00:27:28,000
that they've done in the past.

433
00:27:28,000 --> 00:27:31,000
It has to have the supportive conditions.

434
00:27:31,000 --> 00:27:34,000
Yes, the person who is practicing doesn't know

435
00:27:34,000 --> 00:27:37,000
what they're going towards.

436
00:27:37,000 --> 00:27:40,000
But they have the,

437
00:27:40,000 --> 00:27:43,000
they're able to figure it out as they go along,

438
00:27:43,000 --> 00:27:45,000
and when they're given the advice

439
00:27:45,000 --> 00:27:47,000
and the instruction and meditation,

440
00:27:47,000 --> 00:27:51,000
they're able to follow the advice

441
00:27:51,000 --> 00:27:53,000
and to follow after it.

442
00:27:53,000 --> 00:27:55,000
Now, craving,

443
00:27:55,000 --> 00:27:58,000
considering that it's a habit forming,

444
00:27:58,000 --> 00:28:00,000
mines day to know,

445
00:28:00,000 --> 00:28:16,000
it wouldn't lead one to understand reality.

