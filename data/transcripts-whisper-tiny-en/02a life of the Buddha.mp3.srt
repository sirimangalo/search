1
00:00:00,000 --> 00:00:26,000
Okay, last time I told you we will be with the Buddha for some more time, so today I'll tell

2
00:00:26,000 --> 00:00:33,800
you something more about the Buddha, the life of the Buddha.

3
00:00:33,800 --> 00:00:45,600
When we study the teachings of the Buddha, I think it is also important to know something

4
00:00:45,600 --> 00:00:58,040
about the life of the Buddha, and when we know the life of the Buddha, how he struggled

5
00:00:58,040 --> 00:01:08,760
and how he sacrificed in order to become the Buddha, we can have more appreciation and

6
00:01:08,760 --> 00:01:19,600
understanding of his teachings, and last time I told you a very brief account of the life

7
00:01:19,600 --> 00:01:32,000
of the Buddha, and this time we will go into a little more detail.

8
00:01:32,000 --> 00:01:53,880
So, Buddha was born 624 BC on the full moon day of the month of May, and the time of his

9
00:01:53,880 --> 00:02:03,400
Buddha was not given in the accounts, but it seemed that he was born in the morning,

10
00:02:03,400 --> 00:02:14,680
he was in the afternoon, the sage, Kala, Devala came to see the Bodhisattva, so probably

11
00:02:14,680 --> 00:02:21,200
it was in the morning that he was born, and the place he was born was the long beanie

12
00:02:21,200 --> 00:02:36,200
park, the garden or a small forest not far from the city of Kapilawatu, and his parents

13
00:02:36,200 --> 00:02:53,480
who are kings of Daudana and Queen Mahamaya, so he was born or Queen Kings of Daudana in Queen

14
00:02:53,480 --> 00:02:55,360
Mahamaya on a full moon day of May 624 BC in the morning, and the long beanie park near the

15
00:02:55,360 --> 00:03:13,640
city of Kapilawatu, and in the afternoon the acetic named Kala Devala or Acida came to

16
00:03:13,640 --> 00:03:26,960
the palace to see the newborn baby, Kala Devala was in the habit of coming to Kings of Daudana

17
00:03:26,960 --> 00:03:33,480
and took food there, and then, talk with him, and he went away, so this time he came

18
00:03:33,480 --> 00:03:45,600
to see the newborn baby, and when he arrived, Kings of Daudana made the newborn baby pay homage

19
00:03:45,600 --> 00:03:59,720
to the acetic, but the feed of the newborn baby got on the head of the acetic, and so

20
00:03:59,720 --> 00:04:09,960
acetic Kala Devala bowed down to the infant baby, and when the king saw this the king

21
00:04:09,960 --> 00:04:26,000
himself also bowed down to his own son, when the acetic saw the child first he laughed,

22
00:04:26,000 --> 00:04:35,720
and then he cried, the king was alarmed, and so asked the reason for his loving and crying,

23
00:04:35,720 --> 00:04:47,760
and the acetic said, I laughed because this child will definitely become a Buddha for

24
00:04:47,760 --> 00:04:57,560
the benefit of all beings, and he said I cried because I will not live to see this child

25
00:04:57,560 --> 00:05:08,440
become a Buddha, because he was to die before the child become the Buddha, so first he

26
00:05:08,440 --> 00:05:19,640
laughed and then he cried, and he told the king that the child will definitely become a Buddha.

27
00:05:19,640 --> 00:05:27,480
On the fifth day after birth of the child, the king held a name giving ceremony.

28
00:05:27,480 --> 00:05:40,400
Eight brahmanas were invited to read the science, special science in the child.

29
00:05:40,400 --> 00:05:53,680
Now seven brahmanas, seven older brahmanas, read the science and then they made a prophecy

30
00:05:53,680 --> 00:06:10,400
that if the child leads a household's life, then he would become a universal monarch, and

31
00:06:10,400 --> 00:06:23,520
if the child chose to become a monk, chose to renounce the world, then he would become

32
00:06:23,520 --> 00:06:35,520
a Buddha, but the youngest of them are content, if only one prophecy, and that is a person

33
00:06:35,520 --> 00:06:47,080
possessing such science will not remain in the household's life, but definitely renounce

34
00:06:47,080 --> 00:06:51,160
the world and become the Buddha.

35
00:06:51,160 --> 00:07:05,880
And the name Siddhartha was given to the child, Siddhartha means purpose accomplished.

36
00:07:05,880 --> 00:07:16,240
Another incident, important in the life of Siddhartha was deploying ceremony done by

37
00:07:16,240 --> 00:07:27,960
the king himself, in order to promote cultivation in the olden days, the king and his

38
00:07:27,960 --> 00:07:40,120
ministers and his officials did the ceremonial plowing of the fields.

39
00:07:40,120 --> 00:07:50,920
So at that ceremony, the king took his son with him and then left the son with the attendants

40
00:07:50,920 --> 00:08:01,520
and I tree and he went to participate in the plowing ceremony.

41
00:08:01,520 --> 00:08:09,280
Then the attendants, the nurses and others, wanted to see the grand performance of the king,

42
00:08:09,280 --> 00:08:15,160
so they left the child under the tree and went to see the plowing ceremony.

43
00:08:15,160 --> 00:08:25,800
When nobody was around, actually, the child was kept in a cover that the cutting was

44
00:08:25,800 --> 00:08:27,440
put around him.

45
00:08:27,440 --> 00:08:39,080
So when nobody was there, the child set up and then practiced the breathing and breathing

46
00:08:39,080 --> 00:08:47,120
of meditation and it is said that he attained the first journal.

47
00:08:47,120 --> 00:08:58,240
For some time, the attendants remembered that they had left the child and so they went

48
00:08:58,240 --> 00:09:05,520
back to the child and to their surprise, they saw the child sitting in meditation.

49
00:09:05,520 --> 00:09:15,280
So they were surprised and they informed to the king and the king came to his son and

50
00:09:15,280 --> 00:09:24,120
when he saw his son in a meditative posture, actually sitting in the first journal, he bowed

51
00:09:24,120 --> 00:09:26,000
down again to the infant.

52
00:09:26,000 --> 00:09:30,760
So this is the second bowing down of the father to the son.

53
00:09:30,760 --> 00:09:44,240
Now this incident is important because it was the recollection of this incident that the

54
00:09:44,240 --> 00:09:50,000
acetic gautama after some years he was becoming acetic gautama.

55
00:09:50,000 --> 00:10:04,240
So acetic gautama was able to discover the right path or the right practice.

56
00:10:04,240 --> 00:10:16,680
The books did not say when this flying ceremony took place but since bodhisattva was an

57
00:10:16,680 --> 00:10:24,320
infant, not old enough to go to the ceremony with his father and also since flying

58
00:10:24,320 --> 00:10:37,080
ceremony was done every year, we may take that this particular flying ceremony took

59
00:10:37,080 --> 00:10:47,160
place when bodhisattva was less than one year old.

60
00:10:47,160 --> 00:10:58,200
So Siddhartha grew up as a prince and at the age of 16 he was married to princess Yasodara

61
00:10:58,200 --> 00:11:09,880
and those times people married young and he enjoyed the life of a prince, he enjoyed

62
00:11:09,880 --> 00:11:19,200
the life of luxury for 13 years and he said in our books that King Siddhartha and his father

63
00:11:19,200 --> 00:11:28,120
did not want him to become a Buddha because he wanted him to become a universal monarch.

64
00:11:28,120 --> 00:11:41,520
So King Siddhartha provided his son with every kind of sensual pleasure available in those

65
00:11:41,520 --> 00:11:42,520
times.

66
00:11:42,520 --> 00:11:59,480
So for 13 years the prince Siddhartha enjoyed the life of a prince but in his 29th year

67
00:11:59,480 --> 00:12:12,000
the gods thought that it was time to remind prince Siddhartha of his mission.

68
00:12:12,000 --> 00:12:18,840
So they showed him the four signs, four great signs.

69
00:12:18,840 --> 00:12:25,760
So the prince was in the habit of going to his pleasure garden every now and then and

70
00:12:25,760 --> 00:12:40,200
so one day he went to his pleasure garden and on his way he saw an old man and then four

71
00:12:40,200 --> 00:12:49,520
months later he saw a sick man and still four months later he saw a dead man.

72
00:12:49,520 --> 00:13:00,520
So after seeing these three signs he came to see the realities of life that life is full

73
00:13:00,520 --> 00:13:11,880
of suffering, people have to become old, sick and at the end die and he cannot escape

74
00:13:11,880 --> 00:13:16,560
the old age, disease and death.

75
00:13:16,560 --> 00:13:26,720
So on the last trip to the pleasure garden he saw a recluse, maybe sitting peacefully

76
00:13:26,720 --> 00:13:29,920
under a tree and meditating.

77
00:13:29,920 --> 00:13:42,680
So when he saw the recluse he understood that he must renounce the walls in order to save

78
00:13:42,680 --> 00:13:53,200
beings or in order to help being safe themselves from the sufferings of the round of rebirth.

79
00:13:53,200 --> 00:14:07,640
So he turned back to his parents after seeing the records, so at that time when he had

80
00:14:07,640 --> 00:14:14,760
got into the chariot the news of the bud of a son came to him.

81
00:14:14,760 --> 00:14:24,520
Now normally if other would be very delighted when the news of the bud of a son and comes

82
00:14:24,520 --> 00:14:35,920
to him but said that I hear was not delighted because he was bent on renouncing the wall

83
00:14:35,920 --> 00:14:43,600
and so he considered a bud of a son as a bondage.

84
00:14:43,600 --> 00:14:54,800
So when he hurts the news he said, oh a rahu has been born, a bondage has been born.

85
00:14:54,800 --> 00:15:07,560
Then King Sudhana knew about what Siddhartha said and the news he named the child, the

86
00:15:07,560 --> 00:15:13,800
child of Siddhartha, Ra Hula.

87
00:15:13,800 --> 00:15:21,520
So after saying that he went back to the palace and as soon as he arrived at the palace

88
00:15:21,520 --> 00:15:31,720
he was treated to the central place as again by his right in the news.

89
00:15:31,720 --> 00:15:47,720
But when they were entertaining him he went to sleep and when he woke up he saw people

90
00:15:47,720 --> 00:15:58,800
especially women who are entertaining him in this array and sleeping here and there

91
00:15:58,800 --> 00:16:04,280
and so he saw it as a cemetery.

92
00:16:04,280 --> 00:16:15,880
So he decided then and there to renounce the world and before leaving the palace he wanted

93
00:16:15,880 --> 00:16:25,960
to see his wife and son so he went to be sleeping chamber of his wife and saw his wife

94
00:16:25,960 --> 00:16:32,280
sleeping and having an arm over the head of the newborn child.

95
00:16:32,280 --> 00:16:39,880
So he wanted to pick the child up and maybe kiss him or something like that but he thought

96
00:16:39,880 --> 00:16:48,000
if I did that Yasodra will wake up and I would not be able to renounce the world so he

97
00:16:48,000 --> 00:16:54,840
said to himself I will see him after I became the Buddha.

98
00:16:54,840 --> 00:17:05,240
So that night with one follower with one servant he left the palace and that was on the

99
00:17:05,240 --> 00:17:10,920
full moon day of July.

100
00:17:10,920 --> 00:17:21,320
At the city gate it is said that the city gate was very difficult to open about a thousand

101
00:17:21,320 --> 00:17:27,040
men would be neither to open the city gate but the gods opened the gate and so he was

102
00:17:27,040 --> 00:17:34,840
able to get out easily but at the time Mara the evil one came appeared to him and dissuaded

103
00:17:34,840 --> 00:17:47,160
him telling him that after seven days Bodhisattva would become a universal monarch but

104
00:17:47,160 --> 00:17:53,400
Bodhisattva said I do not want to become a universal monarch I want to become a data to

105
00:17:53,400 --> 00:18:02,600
help being saved themselves so he dismissed the Mara the evil one and went on and in one

106
00:18:02,600 --> 00:18:15,640
night it is said that he reached the river Anoma and he crossed he caused the the the horse

107
00:18:15,640 --> 00:18:28,040
to to jump to the other bank of Anoma and after after reaching the other bank say he he

108
00:18:28,040 --> 00:18:39,560
ordained he cut his hair and then put on the roads of an ascetic.

109
00:18:39,560 --> 00:18:51,160
After meeting King Bambisara he he met two teachers but not at the same time one after another

110
00:18:51,160 --> 00:19:01,440
one first he met the teacher called Alara Galama and he practiced with the teacher and

111
00:19:01,440 --> 00:19:14,200
he attained what the teacher has attained but he found out that that attainment did not

112
00:19:14,200 --> 00:19:25,760
lead to emancipation or did not lead to freedom from suffering and so he left that teacher

113
00:19:25,760 --> 00:19:36,400
and went to another teacher called Odaka Ramapoda he learned from that teacher the practice

114
00:19:36,400 --> 00:19:47,200
of meditation and he practiced and he reached what that teacher he taught and he found

115
00:19:47,200 --> 00:19:58,920
again that that attainment could not lead him to the eradication of mental defilements

116
00:19:58,920 --> 00:20:07,320
and to escape from suffering he left that teacher again so after that he was alone and

117
00:20:07,320 --> 00:20:13,440
he went from place to place and he reached a place called Uruwila a delightful I mean a peaceful

118
00:20:13,440 --> 00:20:28,080
place for a fit for meditation so he settled there and and practiced what I call practice

119
00:20:28,080 --> 00:20:43,680
is difficult to practice that austere practices and other time Kondana the bus in Hoot

120
00:20:43,680 --> 00:20:50,240
food that bodhisattva would definitely become the Buddha so Kondana went to the sons of

121
00:20:50,240 --> 00:20:56,320
other other Brahmins because the other Brahmins had died and their sons only lived at

122
00:20:56,320 --> 00:21:02,840
that time so he went to them and told them that he was going to where the where the

123
00:21:02,840 --> 00:21:12,240
acidity godama was and whether they would like to go with him and three did not want to

124
00:21:12,240 --> 00:21:21,920
go with him but four sons decided to go with them and so five of them went to where the

125
00:21:21,920 --> 00:21:33,760
acidity godama was practicing austereities and attended to to his needs his austere practices

126
00:21:34,960 --> 00:21:45,760
were related by himself where after he became the Buddha and we can read the practices and some

127
00:21:45,760 --> 00:21:56,000
disclosures and and the Magimani Kaya the collection of middle middle length things so he he practiced

128
00:21:56,800 --> 00:22:08,240
so severely or he he he practiced so much that he became very very weak and you may have seen the

129
00:22:08,240 --> 00:22:17,760
pictures of acidity godama practicing austereities just the bones and skin remain and so on

130
00:22:19,360 --> 00:22:24,000
now at that time Mara the evil one came to him and distributed him from the practice

131
00:22:25,280 --> 00:22:32,720
he said you are going to die you are too too thin now so there are pleasures to be enjoyed

132
00:22:32,720 --> 00:22:46,320
and so on but bodies other dismissed him and went on with his austere practices so that went on

133
00:22:46,320 --> 00:22:55,360
for nearly six years so towards the end of the six year bodies are the reviewed his practice

134
00:22:55,360 --> 00:23:04,560
he thought to himself I have been practicing this austere practices for many years and I have not

135
00:23:04,560 --> 00:23:11,920
become any nearer to my goal it might not be the right one and then he decided that

136
00:23:13,600 --> 00:23:21,760
these practices are not the rear I mean the the correct practices so at that critical moment

137
00:23:21,760 --> 00:23:32,960
he recollected the incident at the at the playing ceremony that he as a child he entered into

138
00:23:32,960 --> 00:23:39,840
the first genre and he was very peaceful and happy at that time and so he thought to himself

139
00:23:39,840 --> 00:23:48,400
might that be the right fact and then he decided that yeah that is the right path so he decided to

140
00:23:48,400 --> 00:24:00,480
follow that path but his body was very weak so in that body recognition he knew that he could not

141
00:24:00,480 --> 00:24:15,760
carry on the practice so he began to take food again formally he reduces intake of food little by

142
00:24:15,760 --> 00:24:27,360
little so that sometimes he ate only a handful of pea soup or sometimes he may he might even it's

143
00:24:28,240 --> 00:24:39,120
just one seed or one fruit a day so he after he decided to take food again and he did take food again

144
00:24:39,120 --> 00:24:47,600
the five passages the five disciples were the solutions with him they thought that he had

145
00:24:47,600 --> 00:24:57,680
the bodhisattva had abandoned the right path and now going after luxury so they left him in

146
00:24:57,680 --> 00:25:14,080
disgust and went to another place so bodhisattva was left alone again after bodhisattva gained strength

147
00:25:14,080 --> 00:25:31,920
one day that is on the full movie of me 589 BC he was sitting under a tree and at the time a lady

148
00:25:31,920 --> 00:25:44,800
named Sujata came to him and offered him rice porridge he accepted the rice porridge along with the

149
00:25:44,800 --> 00:25:58,000
bow and then he took butt in the in Niranjara river nearby and then he ate the porridge and put the

150
00:25:58,000 --> 00:26:09,120
bow in the river and it is said that the the the bow went upstream and then sank and during

151
00:26:09,120 --> 00:26:20,560
day time he said under a tree and in the evening he went towards what is known as bodhi tree

152
00:26:20,560 --> 00:26:28,880
bodhi tree is actually a banyan tree but send body or enlightenment was gained under the tree

153
00:26:28,880 --> 00:26:35,680
the tree was called a bodhi tree so he went towards the bodhi tree and on his way to the bodhi

154
00:26:35,680 --> 00:26:43,680
tree he crushed kata met him and offered him eight bundles of grass so he took those bundles of

155
00:26:43,680 --> 00:26:51,760
grass and then after reaching the bodhi tree he spread the grass on the ground and set

156
00:26:54,720 --> 00:27:05,200
at the foot of the bodhi tree facing east what and then he made a very firm resolution

157
00:27:05,200 --> 00:27:19,360
then let my skin see news and bones remain let my flesh and blood dry up but I will not give up

158
00:27:22,320 --> 00:27:33,440
effort to gain what can be gained only by mentally effort that means I will and then he he made

159
00:27:33,440 --> 00:27:44,400
a resolution I will not change this posture I will not get up from this post posture until I reach

160
00:27:44,400 --> 00:27:56,480
Buddha foot so he made this firm resolution and at that time it was about sunset

161
00:27:56,480 --> 00:28:06,720
Mara the evil one came again this time according to our books with his army at the great army

162
00:28:07,360 --> 00:28:19,920
and Mara tried to fight on the bodhisattva away from the place but bodhisattva stood firm

163
00:28:19,920 --> 00:28:30,000
I mean he said firm and in the end Mara was defeated so after the defeat of Mara

164
00:28:33,120 --> 00:28:41,360
bodhisattva practiced meditation to reach Buddhahood

165
00:28:41,360 --> 00:28:53,360
during the first watch of the night he practiced again the mindfulness of breathing meditation

166
00:28:54,240 --> 00:29:00,960
and he gained four Janas in succession for a second that fourth and also he gained the

167
00:29:00,960 --> 00:29:12,000
the full immaterial Janas and also he gained the the super normal knowledge by which he could

168
00:29:12,000 --> 00:29:22,880
remember all his past life so during the first watch of the night he gained the super normal knowledge

169
00:29:22,880 --> 00:29:32,400
of remembering all his past life and during the middle watch of the night he gained the

170
00:29:32,400 --> 00:29:44,000
super normal knowledge by which he could see beings dying from one existence and being reborn

171
00:29:44,000 --> 00:29:54,720
in another existence and he also saw that a particular being died from one existence

172
00:29:54,720 --> 00:30:03,840
and reborn in another existence according to his own karma this this being is born in

173
00:30:03,840 --> 00:30:16,800
hell as a result of and wholesome deeds he did in the previous life and this being is reborn

174
00:30:16,800 --> 00:30:23,360
in the celestial world as a result of wholesome deeds he did in his previous life and so on

175
00:30:23,360 --> 00:30:34,960
so Buddha he was still a bodhisattva at that end so bodhisattva could see all these beings dying

176
00:30:34,960 --> 00:30:47,440
and being reborn as if it were on the screen so this helps the Buddha to populate the law of karma

177
00:30:47,440 --> 00:30:56,240
now the law of karma taught by the Buddha was not founded on just logic actually it was founded

178
00:30:56,240 --> 00:31:06,800
on his own intuition of direct understanding of the law of karma new dependent

179
00:31:06,800 --> 00:31:14,720
origin nation before he became the Buddha so he practiced Vipasana on these 12 factors of dependent

180
00:31:14,720 --> 00:31:20,640
origin nation back and forth again and again and as a result of that Vipasana practice

181
00:31:22,960 --> 00:31:38,640
at dawn he gained the super normal knowledge by which he was able to eradicate all mental

182
00:31:38,640 --> 00:31:46,800
defilements and at the same moment he gained what we call omniscience knowing everything

183
00:31:47,920 --> 00:31:55,920
so with the attainment of the super normal knowledge of the destruction of

184
00:31:55,920 --> 00:32:09,120
mental defilements and omniscience he became the Buddha so bodhisattva became the Buddha

185
00:32:09,840 --> 00:32:18,400
on the full moon day of 500 and 89 BC at the age of 35

186
00:32:18,400 --> 00:32:31,040
after he became the Buddha he spent eight weeks under and near the body tree

187
00:32:31,920 --> 00:32:39,360
and exactly two months after his enlightenment he gave his first summon to the five disciples

188
00:32:39,360 --> 00:32:54,000
at Isipatana near Baranasi where they were now living and about midnight that night

189
00:32:54,000 --> 00:33:15,200
the Buddha gave his second summon to the yakas, himawata and satagiri and five days after the full moon

190
00:33:15,200 --> 00:33:24,800
day on the fifth day after the full moon day Buddha gave his third summon the summon on

191
00:33:26,000 --> 00:33:38,800
non-soul nature of all things and after the first summon one of the five disciples became

192
00:33:38,800 --> 00:33:47,920
a sautabana and on successive days each one gained sautabana hold and so on the fifth day

193
00:33:49,440 --> 00:33:58,800
Buddha taught them the summon on non-soul nature of things and they became out of hands

194
00:33:58,800 --> 00:34:11,360
and Buddha spent the first rainy season at Isipatana after that Buddha visited Rajagaja

195
00:34:15,280 --> 00:34:23,040
where king babies are lived so Buddha met babies are and king babies are offered him

196
00:34:23,040 --> 00:34:30,560
the bamboo grove Buddha accepted it and spent the second rainy season there

197
00:34:32,960 --> 00:34:41,520
then one year after his enlightenment Buddha visited his native city Kapila Watur to see his

198
00:34:41,520 --> 00:34:52,320
father and his relatives so he met his father and relatives and he preached to his father and

199
00:34:52,320 --> 00:35:01,200
his father became enlightened as his sautabana first and then as Isakadagami and then as an

200
00:35:01,200 --> 00:35:09,520
anagram so his father reached three stages of enlightenment on his first visit and also

201
00:35:09,520 --> 00:35:24,080
his half-brother Nanda and his son Rahula were ordained so they became monks and many

202
00:35:24,080 --> 00:35:43,280
princes of saucaries became monks and the hymn and Buddha taught for 45 years and when he

203
00:35:43,280 --> 00:35:54,720
taught he taught day and night now Buddha did not sleep as we do he just laid on on his right side

204
00:35:56,080 --> 00:36:10,160
and took rest now when we when we read his daily routine his daily schedule

205
00:36:10,160 --> 00:36:17,360
and the daily schedule is given in the commentaries so when we read the daily schedule

206
00:36:21,920 --> 00:36:28,640
we find that Buddha rested for about or not more than four hours a day

207
00:36:28,640 --> 00:36:39,840
and at other times he was doing something teaching lay people or teaching monks or teaching or

208
00:36:39,840 --> 00:36:51,920
answering questions of celestial beings so about 20 hours a day he worked and

209
00:36:51,920 --> 00:37:01,840
magically for the welfare of all beings now after becoming a Buddha he could easily retire

210
00:37:03,280 --> 00:37:17,920
but he did not retire and he taught day and night to all beings who came to him or who met him

211
00:37:17,920 --> 00:37:32,480
and he helped save I mean he helped countless beings save themselves during these 45 years

212
00:37:33,120 --> 00:37:46,720
so Buddha was the most energetic religious teacher known to history he walked a he he that's he

213
00:37:46,720 --> 00:37:58,560
worked he worked for 45 years and those 45 years he worked day and night just taking rest

214
00:37:59,120 --> 00:38:05,440
for about four hours a day or not more than four hours a day so when we study his daily routine

215
00:38:05,440 --> 00:38:27,200
we cannot but admire his compassion for all beings and his his effort his making effort to save

216
00:38:27,200 --> 00:38:39,440
I mean how does he say right to save as many beings as possible and here save means not saving himself

217
00:38:39,440 --> 00:38:50,720
but saving and helping beings save themselves so he taught for 45 years for the welfare of all beings

218
00:38:50,720 --> 00:39:01,520
not only human beings but all beings and for lay people as well as monks and at the age of 80

219
00:39:03,280 --> 00:39:13,040
he passed away on a full moon day of May in a growth of some three salad trees

220
00:39:13,040 --> 00:39:24,320
near the city of Kusinara so Buddha passed away at the age of 80 on the full moon day of May

221
00:39:24,320 --> 00:39:32,240
okay so full moon day of May comes to be very important to all Buddhists because it was on the

222
00:39:32,240 --> 00:39:41,200
full moon day of May that he was born and it was on the full moon day of May that he became

223
00:39:41,200 --> 00:39:48,960
enlightened as a Buddha and it was on this day that he passed away so we call it thrice

224
00:39:48,960 --> 00:40:01,760
blessed day in the life of the Buddha now some some things to understand about the Buddha

225
00:40:01,760 --> 00:40:13,680
as I said before in my first class Buddha was not in God not God with capital G because according

226
00:40:13,680 --> 00:40:23,920
to Buddha there was no God who creates the universe and its inhabitants then once he a God with

227
00:40:23,920 --> 00:40:35,520
his more G was he a celestial being no he was not a God or a celestial being and he was not a

228
00:40:35,520 --> 00:40:43,520
prophet of messenger of God simply again because according to him there was no God or God was

229
00:40:43,520 --> 00:40:55,920
non-existent then once he a savior did Buddha save beings not in the Christian sense

230
00:40:57,520 --> 00:41:06,160
so Buddha just helped being safe themselves so he was not a savior then what was he

231
00:41:06,160 --> 00:41:19,120
what was that just no me as a Buddha now he he was a human being right but not an ordinary human

232
00:41:19,120 --> 00:41:29,600
being he was an extraordinary human being human being so extraordinary that he excels even the gods

233
00:41:29,600 --> 00:41:39,760
the celestial beings and Brahma who are the highest celestial beings so Buddha came to be known

234
00:41:39,760 --> 00:41:49,360
as the best among all beings not the best among all human beings but the best among all beings

235
00:41:49,360 --> 00:42:05,040
including gods and Brahma's and Buddha possessed innumerable qualities among them we should

236
00:42:05,040 --> 00:42:13,360
understand the purity of my total purity of mind and omniscience these are very important qualities

237
00:42:13,360 --> 00:42:23,280
so Buddha's mind is totally pure without any trace of any mental departments and Buddha possessed

238
00:42:23,920 --> 00:42:31,120
what we call omniscience that is he knows everything and Buddha himself claimed

239
00:42:31,120 --> 00:42:44,560
to be omniscience when he when he said to Upaka the ascetic whom he met on his way to

240
00:42:45,600 --> 00:42:58,320
Baranasi and they are Buddha said I am the knower of all so that means I am omniscience

241
00:42:58,320 --> 00:43:05,360
so Buddha possessed and these qualities total purity of mind and omniscience

242
00:43:07,840 --> 00:43:18,400
where is the Buddha now does he exist in some form somewhere according to the teachings of

243
00:43:18,400 --> 00:43:37,040
Thiravada Buddha is no more just as a flame goes out Buddha died and there was no more existence

244
00:43:37,040 --> 00:43:44,880
or no more rebirth for him so according to Thiravada teachings there is no Buddha now

245
00:43:44,880 --> 00:44:00,560
then how can he help us when he is no more with us now somebody invented electricity I mean electric

246
00:44:00,560 --> 00:44:10,560
balsam make use of electricity they are no more right but we we are enjoying his the benefits of

247
00:44:10,560 --> 00:44:20,560
his invention so the man who invented wireless is no more now but we enjoy listening to radio

248
00:44:20,560 --> 00:44:27,680
and all these things so although Buddha is not with us now he left his teachings before he died

249
00:44:27,680 --> 00:44:34,480
he said when when I am gone my teachings will be teach us for you so we have his teachings now

250
00:44:34,480 --> 00:44:43,760
and so we enjoy his teachings at the benefits of his teachings so although Buddha is no more now

251
00:44:45,280 --> 00:44:53,600
since his teachings are available now we are as good as having the Buddha

252
00:44:53,600 --> 00:45:07,520
now can Buddha forgive sins now sin we are we are using this word in a Buddhist sense

253
00:45:08,560 --> 00:45:16,800
so and wholesome actions and wholesome deeds and wholesome speech and and wholesome thoughts

254
00:45:16,800 --> 00:45:27,280
these we call Akusala so let us call them sins can Buddha forgive sins say we we we do some

255
00:45:27,280 --> 00:45:32,800
Akusala and then can we ask pattern from the Buddha please forgive us our sins

256
00:45:34,480 --> 00:45:41,360
and can Buddha forgive us no Buddha cannot forgive forgive the sins

257
00:45:41,360 --> 00:45:49,440
simply because it is impossible to forgive and these are sins not because Buddha said these were

258
00:45:49,440 --> 00:45:55,600
sins but because they are sins so it is the other we wrong because they they are sins

259
00:45:55,600 --> 00:46:07,920
the Buddha said they are and Buddha did not create sins and when say we we do sins that is

260
00:46:07,920 --> 00:46:15,840
just our actions we do something and this is our our our doing our action and so this cannot be

261
00:46:16,560 --> 00:46:30,480
undone by anybody so Buddha cannot give sins means not because he did not want to forgive but because

262
00:46:30,480 --> 00:46:42,320
he cannot forgive sins or because it is impossible to forgive sins that is why we say Buddha

263
00:46:43,040 --> 00:46:51,360
Buddha say do not forgive sins each Buddha capable of love and compassion

264
00:46:51,360 --> 00:47:02,480
Buddha had great love a boundless love and compassion for all beings

265
00:47:03,440 --> 00:47:12,560
but the love Buddha had for being is not a love mixed with a company by

266
00:47:12,560 --> 00:47:28,320
attachment or ill will or delusion it is mitta the wholesome desire for the well-being

267
00:47:28,320 --> 00:47:35,760
the well-being of all beings and also Buddha had what we call the great compassion that means

268
00:47:35,760 --> 00:47:44,800
Buddha had compassion for all beings without any exception so in Buddhism

269
00:47:47,600 --> 00:47:53,440
we do not say that Buddha has compassion for this his followers only and not for other people

270
00:47:53,440 --> 00:47:58,880
who are now his followers so whether you are a follower his follower or not

271
00:47:58,880 --> 00:48:11,760
Buddha had compassion for you so Buddha's love and compassion for beings is for all beings without

272
00:48:11,760 --> 00:48:20,080
exception so Buddha is capable of the greatest love and greatest compassion

273
00:48:20,080 --> 00:48:30,880
Buddha capable of doing harm to others since Buddha poses total purity of mind

274
00:48:32,160 --> 00:48:40,960
Buddha never do any harm to any being at all even to those who attempted his life

275
00:48:40,960 --> 00:48:51,120
Buddha felt only compassion and not anger and Buddha did not harm any being at all so there

276
00:48:51,120 --> 00:49:02,240
is no no mention of Buddha killing any living being or killing and many people or inflicting

277
00:49:02,240 --> 00:49:25,440
punishment or suffering to beings so we we can we can lift and safety concerning the Buddha

278
00:49:25,440 --> 00:49:35,040
we will not get any harm or any punishment or whatever from the Buddha so Buddha is not capable

279
00:49:35,040 --> 00:49:43,120
of doing harm to any being so how many Buddhas are there is there only one Buddha

280
00:49:43,120 --> 00:49:55,760
yes there is only one Buddha at a time although there are many many Buddhas in the past

281
00:49:56,480 --> 00:49:59,040
and there will come many Buddhas in the future

282
00:50:04,640 --> 00:50:10,960
at one given present moment present time there can be only one Buddha in the world

283
00:50:10,960 --> 00:50:18,320
and it is explained in our books that the world cannot hold two Buddhas at a time

284
00:50:19,280 --> 00:50:26,320
if there were two Buddhas then there would be some kind of competition among his disciples

285
00:50:26,320 --> 00:50:36,720
and so there is only one Buddha at a time although many Buddhas appear in the past and many

286
00:50:36,720 --> 00:50:48,880
Buddhas will appear in the future now can you become a Buddha can anybody become a Buddha

287
00:50:51,680 --> 00:51:04,640
yes if he or she is willing to fulfill the the perfection of paramis if she or

288
00:51:04,640 --> 00:51:12,400
if she or she is willing to to suffer in the samsala for a long long time

289
00:51:14,160 --> 00:51:26,000
accumulating the perfection so in theory anybody can become a Buddha but not everybody becomes

290
00:51:26,000 --> 00:51:36,160
a Buddha in reality only very very few become Buddhas because to become a Buddha is not

291
00:51:36,160 --> 00:51:46,320
not an easy job so it takes a very very long time to to accumulate the necessary qualifications

292
00:51:46,320 --> 00:52:02,000
and also those who as part of a Buddha who had to to practice or to sacrifice many things in their

293
00:52:02,000 --> 00:52:12,000
lives even even their their life themselves and so it is very difficult to fulfill these qualifications

294
00:52:12,000 --> 00:52:23,200
and so it is difficult to become a Buddha just as not everybody I mean every everybody

295
00:52:23,200 --> 00:52:32,560
every what can I say that I cannot say every citizen right so every native citizen can become

296
00:52:32,560 --> 00:52:38,320
a president of the United States they have the right to become a the president of the United

297
00:52:38,320 --> 00:52:44,480
States but not everybody will become the president so only one will become a president at one

298
00:52:44,480 --> 00:52:52,080
time so in the same way although say we can say that being some potential to become a Buddha

299
00:52:52,080 --> 00:52:56,480
not everybody will become the Buddha only very very few will become

300
00:52:56,480 --> 00:53:13,360
since anybody who is willing to suffer in the samsara and accumulate the imperfections can

301
00:53:13,360 --> 00:53:21,840
become a Buddha must everybody try to become a Buddha now in tera water you are not forced to

302
00:53:21,840 --> 00:53:34,000
try to become a Buddha you are given a choice you can try to become a Buddha a

303
00:53:34,000 --> 00:53:43,920
a pachika Buddha or an arrow hand so at least there are three three three pats or three ways

304
00:53:43,920 --> 00:53:55,120
open to you and you can choose anyone anyone you like so according to tera water teachings

305
00:53:57,920 --> 00:54:04,880
we cannot say you must try to become a Buddha but if you want to if you are willing to

306
00:54:04,880 --> 00:54:15,040
fulfill the part of reason willing to save I mean have been saved themselves then you can try to

307
00:54:15,040 --> 00:54:20,960
become a Buddha if you don't want to spend a long long time in the samsara but want to get out of

308
00:54:20,960 --> 00:54:29,440
this samsara pretty soon that you may choose the the path of arrow hands so it is up to you

309
00:54:29,440 --> 00:54:39,920
to try to become a Buddha or a pachika Buddha or an arrow hand and how long does it take for a

310
00:54:39,920 --> 00:54:47,520
person to become a Buddha after the formal aspiration for a Buddha hold formal aspiration means

311
00:54:47,520 --> 00:54:54,160
making the aspiration and the presence of another Buddha as you know how long

312
00:54:54,160 --> 00:55:09,520
four incalculables and one hundred thousand eons or wall cycles right that is a shorter

313
00:55:09,520 --> 00:55:19,040
time for some Buddha's eight incalculables and one hundred thousand wall cycles and for another

314
00:55:19,040 --> 00:55:31,120
kind of Buddha 16 incalculables and one hundred thousand wall cycles so you have to spend

315
00:55:32,640 --> 00:55:39,840
that that number is for wall cycles and in one wall cycles there can be many many lines

316
00:55:39,840 --> 00:55:50,400
so you have to go through billions of lives accumulating the qualifications to become a Buddha

317
00:55:50,400 --> 00:55:53,600
so if you're prepared to do that you may aspire for a Buddha hold

318
00:55:56,160 --> 00:56:07,280
you talk about astronomy you talk not in years but you're not in miles not in kilometers but in

319
00:56:07,280 --> 00:56:14,160
like years millions of like years right so in the same way when we talk about the time

320
00:56:14,880 --> 00:56:24,080
a person actually relates parameters we talk in in-calculables so incalculable means

321
00:56:25,360 --> 00:56:33,280
the number is so big that it is almost incalculable it is almost cannot be counted

322
00:56:33,280 --> 00:56:46,640
and if you want to count then the number is the digit one followed by 140 zeros that is one incalculable

323
00:56:50,800 --> 00:56:57,760
okay these are some some important things to know about the Buddha

324
00:56:57,760 --> 00:57:10,640
now sometimes I may I may take more time because I want you to understand many things

325
00:57:11,760 --> 00:57:20,640
and also this class is only once a month so I wish we could have this class twice a month

326
00:57:20,640 --> 00:57:35,920
or four times a month or every weekday okay

