1
00:00:00,000 --> 00:00:04,280
I have a sincere question to ask you, and I don't mean to be disrespectful, but

2
00:00:05,120 --> 00:00:12,480
You have about 1,000 videos uploaded even videos from a video game. Could this be some sort of clinging to social media?

3
00:00:13,760 --> 00:00:15,760
Yes, it very well could be

4
00:00:16,800 --> 00:00:22,680
I have about 600 videos uploaded and that's getting closer to 1,000

5
00:00:23,800 --> 00:00:26,840
The videos are not from a video game. They're from a video

6
00:00:26,840 --> 00:00:35,520
Universal universe. I guess they call it a game, but you can trust me the people at that place

7
00:00:38,240 --> 00:00:41,080
Some of them see it as a game, but many of them are quite

8
00:00:44,440 --> 00:00:48,600
Serious to use it as a platform. It was actually designed as a platform

9
00:00:49,240 --> 00:00:55,520
mostly to play and and find central enjoyment, but it's been used for many other purposes as well

10
00:00:55,520 --> 00:00:57,520
There's a center that

11
00:00:59,920 --> 00:01:04,120
For for victims of suicide, you know, survivors of suicide

12
00:01:04,320 --> 00:01:06,320
I was talking with this woman there

13
00:01:06,720 --> 00:01:12,040
Universities use it for online collaboration and meetings and so it can be used for other purposes

14
00:01:12,040 --> 00:01:14,520
And I feel like I used it for other purposes

15
00:01:15,440 --> 00:01:17,760
that people didn't get something in the end. I did

16
00:01:18,320 --> 00:01:22,000
kind of lose interest because it does seem a little bit game-ish and

17
00:01:22,000 --> 00:01:26,000
And the quality of the audience is not as

18
00:01:28,640 --> 00:01:35,040
Quality I mean people they're they're interested in the demo is not as high as it might be on

19
00:01:36,000 --> 00:01:38,000
Well on YouTube, I don't know

20
00:01:38,400 --> 00:01:45,320
Google hangout now, but that kind of isn't answering the question the question as whether this could be some sort of clinging

21
00:01:45,320 --> 00:01:47,320
And yeah, it very well could be

22
00:01:47,320 --> 00:01:51,320
In my defense

23
00:01:53,000 --> 00:01:55,000
I'm not really

24
00:01:56,200 --> 00:02:02,400
Pushing to do this. I'm doing it as a means of making my

25
00:02:08,280 --> 00:02:11,480
Life I guess you could say life easier

26
00:02:11,480 --> 00:02:18,920
Because it's happens to be an easy way to teach people putting up 600 videos

27
00:02:19,480 --> 00:02:24,440
It may seem like a lot of work, but well, it's been over how many years now. I think I started in

28
00:02:25,640 --> 00:02:27,640
2007 I think so

29
00:02:28,440 --> 00:02:30,440
So that's five years

30
00:02:30,520 --> 00:02:35,760
Five years 600 videos isn't isn't a terrible. I'm you know, that's

31
00:02:35,760 --> 00:02:44,520
How many videos every a video every three days or something? That's a lot. No, but

32
00:02:45,200 --> 00:02:49,080
The other thing is a lot of those videos, you know, like I did 28 videos last Sunday

33
00:02:49,080 --> 00:02:53,000
It's just a number right means I answered 20 questions last Sunday

34
00:02:53,000 --> 00:02:57,680
But the point for example of answering questions is that I used to have to

35
00:02:58,400 --> 00:03:02,920
Answer people's emails, you know, I used to have to answer the same question

36
00:03:02,920 --> 00:03:08,840
Every week because you answered once you're answering it for one person

37
00:03:09,800 --> 00:03:13,400
By answering it on YouTube and putting a video on YouTube

38
00:03:14,200 --> 00:03:20,680
I can say I've answered the question for everyone who has everyone who has you know internet

39
00:03:22,680 --> 00:03:30,200
And so I don't feel the need to answer people's emails anymore, and I often don't answer people's questions via email unless it's urgent

40
00:03:30,200 --> 00:03:38,040
And they have some some pressing urgency or they're coming to practice meditation if it's some reason

41
00:03:38,760 --> 00:03:43,400
But ordinarily I I won't do it because I feel like I've done my duty

42
00:03:43,960 --> 00:03:49,600
The question of whether one has to do this or not does one have a responsibility to bring

43
00:03:53,720 --> 00:03:55,720
The Buddha's teaching to others and

44
00:03:55,720 --> 00:03:59,560
And I think I've talked about this before I from my point of view

45
00:03:59,560 --> 00:04:01,560
I don't think you have a responsibility

46
00:04:01,800 --> 00:04:05,240
But if one does do it, I don't think it's wrong either

47
00:04:05,880 --> 00:04:07,880
If it's a part of one's

48
00:04:08,120 --> 00:04:10,120
Buddhist practice to do so

49
00:04:11,000 --> 00:04:13,480
I think clearly it there's a place for it

50
00:04:15,400 --> 00:04:19,640
You know, I said it if you want to live in peace and happiness

51
00:04:21,080 --> 00:04:24,200
It behooves you to bring peace and happiness to the world around you

52
00:04:24,200 --> 00:04:28,920
If you're living in a place often the jungle somewhere where you don't have to

53
00:04:29,640 --> 00:04:31,000
teach people

54
00:04:31,000 --> 00:04:34,920
Then you know, there's no need there's peace and happiness in the world around you

55
00:04:37,240 --> 00:04:41,480
The problem with bringing with using that model for all of Buddhism is it doesn't work

56
00:04:42,200 --> 00:04:46,280
Buddhism is now spread throughout the globe and even here in Sri Lanka for example

57
00:04:47,160 --> 00:04:53,560
If the Sri Lankan people don't understand Buddhism it creates great disturbance for us as Buddhist monastics

58
00:04:53,560 --> 00:04:57,960
If the community that we're living in doesn't understand Buddhism

59
00:04:59,400 --> 00:05:02,680
Our Buddhist our our monastic lives are totally disrupted

60
00:05:03,560 --> 00:05:06,280
They will start cutting down the forest. They will start

61
00:05:08,920 --> 00:05:10,920
Requiring us to do things like

62
00:05:12,840 --> 00:05:15,880
Give blessings and go to their funerals and go to their

63
00:05:17,400 --> 00:05:20,280
Ceremonies and so on and make our lives very busy

64
00:05:20,280 --> 00:05:24,440
Just for us to just to allow us to to live as monks

65
00:05:25,400 --> 00:05:28,920
They will want us to do things that are not appropriate as monks to you know

66
00:05:29,480 --> 00:05:33,160
To give charms and emulates and whatever the things

67
00:05:33,720 --> 00:05:41,400
Horoscopes even perform marriage ceremonies and eventually become priests and eventually you know really actually work for the people

68
00:05:43,000 --> 00:05:47,000
If we don't teach them how to let go how to become free

69
00:05:47,000 --> 00:05:53,800
Moreover, they will start becoming evil people and they will go to war with each other and and they will

70
00:05:54,200 --> 00:05:59,000
Require monks to enlist in the army and so on and so on require monks to grow food and

71
00:05:59,880 --> 00:06:02,200
And so on and so on and make great difficulty for us

72
00:06:02,200 --> 00:06:05,320
This applies to the whole world at this point because now we're living

73
00:06:06,680 --> 00:06:08,680
in a global community our

74
00:06:08,680 --> 00:06:21,080
Lives our existence depends on the globe the the goodness existing in the world Buddhism requires goodness to exist in the world

75
00:06:21,080 --> 00:06:29,160
So there is an argument for it being our duty as Buddhists to protect the Buddha sassana and one very important

76
00:06:29,880 --> 00:06:32,120
way of protecting the Buddha sassana is by

77
00:06:33,400 --> 00:06:35,400
Creating goodness in the world

78
00:06:35,400 --> 00:06:43,000
Because it's like it's like a tidal wave if you've seen you know the evil and corruption that have destroyed Buddhist countries

79
00:06:45,240 --> 00:06:54,520
I think no one can can deny the Khmer Rouge in Cambodia how it destroyed Buddhism in that country or not destroyed but seriously

80
00:06:56,520 --> 00:07:00,440
Set Buddhism back in that country how war and colonization

81
00:07:00,440 --> 00:07:05,000
Here hurt Buddhism through example in the British came to Sri Lanka

82
00:07:06,520 --> 00:07:11,480
It would become I don't know how it happened, but it would become incredibly important to teach Buddhism to the to the British

83
00:07:12,840 --> 00:07:14,840
so that they can

84
00:07:15,000 --> 00:07:18,520
Let go and see that this is a horrible thing that they're doing

85
00:07:20,760 --> 00:07:25,560
For example, so I would say there is a defense to be made for

86
00:07:25,560 --> 00:07:31,400
Spreading news. I simply see that this is a part of the community now. Now. I'm not Sri Lankan

87
00:07:31,560 --> 00:07:37,800
I have to get a visa to stay here. I'm Canadian and that makes me very much a part of the Canadian

88
00:07:41,560 --> 00:07:44,280
What the part of Canada or the country the Western

89
00:07:45,000 --> 00:07:51,080
Society now if I can't reach Canadian people American people then it's very difficult

90
00:07:51,080 --> 00:07:56,040
Buddhism doesn't reach there. It's very difficult for me to go home even to visit my parents

91
00:07:56,680 --> 00:08:00,760
If I can't help my parents to understand, you know, these are these are defense

92
00:08:01,560 --> 00:08:07,640
These are arguments that could be made in favor of this sort of thing now if it's really something that I'm keen to do

93
00:08:08,440 --> 00:08:11,160
To you know to have a Google plus hangout and

94
00:08:11,880 --> 00:08:18,600
To you know really keen to to do these sorts of things then you can see well it starts to be in attachment like the idea last week of holding

95
00:08:18,600 --> 00:08:20,920
interviews well that's kind of

96
00:08:22,600 --> 00:08:26,360
Getting across the edge, you know where you start to go out of your way to

97
00:08:28,040 --> 00:08:29,480
Do Buddhist things

98
00:08:29,480 --> 00:08:35,000
But simply defending Buddhism by by means of spreading goodness spreading truth

99
00:08:35,720 --> 00:08:38,680
I think there is a defense there because it certainly has happened by

100
00:08:39,320 --> 00:08:46,360
Very wise and noble monks in the Buddhist time and and since then who have done their part to keep Buddhism alive

101
00:08:46,360 --> 00:08:48,360
So that's my defense

