1
00:00:00,000 --> 00:00:10,680
Okay, so continuing our study of the Damupanda, the way it's verse number 18, today I brought

2
00:00:10,680 --> 00:00:19,040
out a bigger book, this is the commentary on the Damupanda, and so it has the same verses,

3
00:00:19,040 --> 00:00:25,760
but it also has the story, because today's story has a bit of a problem to it, but it's

4
00:00:25,760 --> 00:00:31,680
very simple story, and the verse itself is very much the same as the past three verses,

5
00:00:31,680 --> 00:00:43,440
so this one is Ita nandati, page anandati, katapun yo uba yata nandati, kun yang me katanti nandati,

6
00:00:43,440 --> 00:00:59,880
katanti, katanti, katanti, katanti, katanti, katanti, so here she is blissful, and here after

7
00:00:59,880 --> 00:01:12,080
she is blissful, or has joy, atapun yo the person, the doer of goodness, rejoices in both

8
00:01:12,080 --> 00:01:22,080
places, blissful in both places, kun yang me katanti, thinking I have done good, she rejoices,

9
00:01:22,080 --> 00:01:30,240
biyo nandati, sukatini katanti, and even more she rejoices having gone to heaven, so I use the

10
00:01:30,240 --> 00:01:37,040
feminine here, there's no need to use the masculine all the time, but specifically here we're

11
00:01:37,040 --> 00:01:44,720
talking about a woman, it's a daughter of anatapindika, and anatapindika was one of the Buddha's

12
00:01:44,720 --> 00:01:52,000
chief lay disciples, and he would go to see the Buddha every day, and various reasons, the

13
00:01:52,000 --> 00:01:57,440
first one is of course listen to the Buddha's teaching, but he would never go and ask the Buddha

14
00:01:57,440 --> 00:02:02,880
to teach on this subject to that subject, because he would always think, oh the Buddha is so much

15
00:02:02,880 --> 00:02:10,080
work, and he's a prince, and he's very delicate, and he's a very special person, so I won't try

16
00:02:10,080 --> 00:02:22,400
to tire him, so he wouldn't ask, and the Buddha would never, the Buddha wouldn't be concerned

17
00:02:22,400 --> 00:02:25,680
about this, the Buddha would just look at the natapindika, and know it, and not the pindika

18
00:02:25,680 --> 00:02:30,720
needed to hear, and so he would give a talk anyway, so natapindika would go and listen to the

19
00:02:30,720 --> 00:02:38,160
Buddha's teaching every day, and natapindika means one who gives food, who feeds the poor,

20
00:02:38,160 --> 00:02:46,560
who feeds those without any refuge, and nata, so he was a very great and very rich man as well,

21
00:02:46,560 --> 00:02:53,840
so he was an example of a good rich person, which does exist in the world even today,

22
00:02:53,840 --> 00:03:01,120
and he was one such person who was very good to the person. Another reason is he'd go to feed

23
00:03:01,120 --> 00:03:05,200
the monks, whenever anybody wanted to feed the monks, they would bring nata, the pindika along,

24
00:03:05,200 --> 00:03:09,840
because he knew the monks very well, so whether he or Risaka, who was the other,

25
00:03:10,480 --> 00:03:16,480
female, they disciple, and bring them along, it was very busy, and certainly also busy with his work,

26
00:03:16,480 --> 00:03:23,760
and so he had his daughter, his eldest daughter, stay at home, and if monks came to the house,

27
00:03:23,760 --> 00:03:28,480
then she was to give them food and to listen to their teachings, and so she would give them

28
00:03:28,480 --> 00:03:32,560
food, and then listen to their teachings, and she, as a result of practicing the teachings that

29
00:03:32,560 --> 00:03:43,200
she got, she became a sotupanda, and then having grown up, she went off to, she got married,

30
00:03:43,200 --> 00:03:49,040
and went off to her husband's home and left home, and so he passed the job on to his second

31
00:03:49,040 --> 00:03:55,280
daughter, and the same thing happened, she also became a sotupanda, but she also was old enough to

32
00:03:55,280 --> 00:04:01,520
go off and get married again. Let me come to the third daughter, and the text says that having

33
00:04:01,520 --> 00:04:08,080
listening, saapana, namma, sutva, having listened to the dhamma, she not only became a sotupanda,

34
00:04:08,080 --> 00:04:16,640
but she became a saki-dagami, sakanda gami, which is the second stage, sakata means with only one

35
00:04:16,640 --> 00:04:24,000
rebirth, so she was almost to the point where she had no more greed, no more anger,

36
00:04:24,000 --> 00:04:29,440
her mind was very pure, and at most she would come back once to this world before we come in the night.

37
00:04:31,120 --> 00:04:34,880
And then it says something that the trans, the English translator interprets as

38
00:04:36,320 --> 00:04:41,360
she was upset, being so upset because she didn't, she couldn't find a husband.

39
00:04:41,360 --> 00:04:47,120
The other two found husbands, but she couldn't find a husband, and she was so upset that she stopped

40
00:04:47,120 --> 00:04:53,120
eating, but that's as far as I can see, that's not what this says. It says kumari, kava,

41
00:04:53,120 --> 00:04:59,360
who'd be having been barely having been a young girl, but it could also mean an unmarried girl,

42
00:04:59,360 --> 00:05:10,160
but it most likely just means that she was very young. She was afflicted by such a disease,

43
00:05:10,160 --> 00:05:15,120
so I don't know what such a disease is, it's not quite clear, and for that reason she stopped

44
00:05:15,120 --> 00:05:22,480
you there. She got very sick, probably she was young and had some sickness of being young,

45
00:05:23,200 --> 00:05:29,280
so having stopped eating, she wished to see her father. So she was at the point,

46
00:05:29,280 --> 00:05:33,680
basically she was at the point where she was about to die, wanting to see her father,

47
00:05:33,680 --> 00:05:44,720
she sent someone to the monastery, where of course he was listening to the dhamma. He was giving

48
00:05:44,720 --> 00:05:53,200
dhamma, giving food to the monks, but when he heard the call, he immediately went back home and asked

49
00:05:53,200 --> 00:06:02,240
asked her, what is it, Sumana? Sumana was her name, what's wrong? And Sumana was on the point

50
00:06:02,240 --> 00:06:09,600
of death, and so I guess she didn't hear him quite well, and she said, what did you say,

51
00:06:09,600 --> 00:06:16,160
younger brother? And he was shocked, this is her father of course, right? And he said,

52
00:06:16,160 --> 00:06:26,800
are you deranged? Have you lost, like, June at the port butcher? Have you, is your mind unclear?

53
00:06:26,800 --> 00:06:33,760
And she said, no, no, my mind is not deranged, younger brother. And then he said,

54
00:06:33,760 --> 00:06:37,840
I couldn't know what was, what was, what is this, she's calling a younger brother,

55
00:06:39,040 --> 00:06:44,480
and he said, are you afraid? Because he figured maybe she's so afraid that she's going,

56
00:06:44,480 --> 00:06:49,760
going a little bit hallucinating, or isn't able to recognize her own father. And she said,

57
00:06:49,760 --> 00:06:54,160
no, I'm not afraid younger brother. And with those words she passed away.

58
00:06:54,160 --> 00:06:59,840
All of them look assy, she did her time, made her time, and it shouldn't die.

59
00:07:01,440 --> 00:07:05,600
And so even being a Sotupana, not the pinnacle, was also a Sotupana. He had been

60
00:07:05,600 --> 00:07:11,760
practicing the Buddha's teaching for quite some time. And even though he was a Sotupana,

61
00:07:11,760 --> 00:07:17,440
there was someone who was quite meditated, he still couldn't bear to hear this, because

62
00:07:17,440 --> 00:07:24,880
of what she was saying. And so he started crying, and he went, or he was weeping, and he went

63
00:07:24,880 --> 00:07:31,600
to see the Buddha. The Buddha said, how is it not the pinnacle that you were crying? Not the

64
00:07:31,600 --> 00:07:37,840
pinnacle said, my younger daughter has passed away, my younger daughter. And the Buddha said,

65
00:07:37,840 --> 00:07:44,880
well, so, you know, haven't you been listening? Everyone has to die. She passed away. And

66
00:07:44,880 --> 00:07:51,200
it's, you know, that's just part of life. And he said, yes, I know. I understand that. But

67
00:07:53,840 --> 00:08:01,600
what makes me so sad is that even though she was such a good girl, and very conscientious,

68
00:08:01,600 --> 00:08:08,000
and very kind to everyone, and I'm quite interested in the Buddha's teaching, still.

69
00:08:08,560 --> 00:08:12,800
When she died, she was totally deranged. She was, her mind was not clear.

70
00:08:12,800 --> 00:08:19,760
I'm afraid of where she may have gone. And the Buddha said, that's not why she,

71
00:08:19,760 --> 00:08:23,920
that's, or he said, why do you think she was deranged? And not the pinnacle said, well,

72
00:08:23,920 --> 00:08:28,160
she called me younger brother. And the Buddha said, well, you are her younger brother.

73
00:08:29,120 --> 00:08:33,840
And he said, how is that? And the Buddha said, well, she has become a saccidic. She is older than

74
00:08:33,840 --> 00:08:40,800
you in the paths and fruition. So because of her attainment of becoming a saccidic, you are her

75
00:08:40,800 --> 00:08:45,200
younger brother as only a sotupana. So it seems like she may have actually been trying to give

76
00:08:45,200 --> 00:08:50,960
some teaching to an Atapindika to say to him, look, you're still only a sotupana. You should work hard

77
00:08:51,920 --> 00:08:57,040
to get to the higher path. And he said, and the Buddha said, and now she's gone to, and then

78
00:08:57,040 --> 00:09:02,080
Atapindika is where has she gone? And he said, the Buddha says, she's gone to Tousita.

79
00:09:02,720 --> 00:09:06,720
They have anywhere, all the future Buddhas live, and the Buddha's mother

80
00:09:06,720 --> 00:09:16,640
you know, reside. And he said, I guess that is not the case. Wow, she was, she was happy

81
00:09:16,640 --> 00:09:23,040
here and doing good deeds here. And now she's rejoicing up and having the Buddha said this

82
00:09:23,040 --> 00:09:27,840
verse as well. The same way, it's just the same verse that he always would say, but changed

83
00:09:27,840 --> 00:09:34,000
a little bit, even under the debates and under the things. So yes, it's the kid's true. Even

84
00:09:34,000 --> 00:09:40,480
the rejoice here and rejoice here after the word nandati. So last time we had more dati, more dati

85
00:09:40,480 --> 00:09:47,520
means to rejoice. It's the same as more dita, more dita means joy. When we have joy for someone else's

86
00:09:47,520 --> 00:09:52,400
good deeds, we're rejoicing another people's good deeds. It comes from the word more, the same

87
00:09:52,400 --> 00:10:05,840
root as more dati. Here we have nandati. Nandati means joy or bliss or the same sort of idea. So

88
00:10:05,840 --> 00:10:11,840
the story is very simple, and the lesson is very similar to the one who came. I'd like to talk

89
00:10:11,840 --> 00:10:18,000
about his punya. So he's talk a little bit about what it means by punya, because we might wonder

90
00:10:18,000 --> 00:10:24,560
how this relates to us, right? Here we are in the meditation center. We're not out there in the world.

91
00:10:24,560 --> 00:10:31,200
We're not like a natapindika giving gifts to the poor, giving alms to the poor. We're not even out there

92
00:10:32,720 --> 00:10:39,040
giving dana or giving food to monks or supporting the Buddha's teaching. We're not even listening

93
00:10:39,040 --> 00:10:45,040
to or reading or studying the Buddha's teaching. We have, actually, we are. No, we have a sutra

94
00:10:45,040 --> 00:10:52,880
every day, but we're not, we're not doing hard core studying. You just have one story every day.

95
00:10:53,840 --> 00:10:59,440
So how is it that we're doing good deeds here? I think the answer should be fairly obvious

96
00:10:59,440 --> 00:11:03,760
that we're practicing meditation, but it sometimes doesn't feel like that. It's like you're

97
00:11:03,760 --> 00:11:10,240
sitting here and what bit am I doing? Sitting here torturing myself, having to deal with the chaos in

98
00:11:10,240 --> 00:11:18,320
my mind and the pain in my back and the mosquitoes and the leeches. Only getting to eat once a day

99
00:11:18,320 --> 00:11:25,280
and so on. Well, I mean, the answer is not difficult. If you study Buddhism, it's easy to understand.

100
00:11:25,280 --> 00:11:30,480
Goodness, of course, exists in the heart. And so when we talk about goodness, we always say dana,

101
00:11:30,480 --> 00:11:36,480
charities, goodness, CLM, morality, goodness, and bhavana, mental development, just goodness.

102
00:11:36,480 --> 00:11:44,000
But even with charity, what it means is giving up. Dana means giving, but in a Buddha's sense

103
00:11:44,000 --> 00:11:51,040
means giving up. It has to do with greed, to giving up of greed. So here you are in a place where

104
00:11:51,040 --> 00:11:57,520
you can't indulge in any of your desires. When a desire comes up, you're forced to look at it

105
00:11:57,520 --> 00:12:02,800
and become objective about it and to see the greed from what it is, the wanting,

106
00:12:02,800 --> 00:12:08,720
wanting entertainment, wantingness, or wanting that, wanting good food, wanting nice bed,

107
00:12:08,720 --> 00:12:20,640
wanting to sleep more, wanting to, so many types of pleasure. But you can't have them. And by

108
00:12:22,160 --> 00:12:27,120
looking at them and by forcing yourself to stay strong with them, you're giving them up.

109
00:12:27,120 --> 00:12:30,720
And this is what you should be able to feel, is that actually your mind is becoming a straighter,

110
00:12:30,720 --> 00:12:35,840
this crookedness that comes from clinging, right? Clinging is like this, this looks like

111
00:12:35,840 --> 00:12:41,040
clinging. This is the hand crooked, but if the hand is straight, it can't cling to anything.

112
00:12:41,040 --> 00:12:44,720
And the mind is the same. In order to cling, it has to get all twisted out of shape,

113
00:12:45,440 --> 00:12:50,080
all bent out of shape. When the mind is straight, as the Buddha said, the straightening of the mind,

114
00:12:52,640 --> 00:12:55,120
it can't cling to anything, nothing, nothing clings to it.

115
00:12:55,120 --> 00:13:01,760
So this is really what we're doing is the greatest, the greatest gift that we could ever possibly

116
00:13:01,760 --> 00:13:09,760
give. We're giving freedom from our defilements to our beings. We're giving the world a great

117
00:13:09,760 --> 00:13:15,840
gift. It's our purity. So because we don't have the greed in our mind, because we train ourselves

118
00:13:15,840 --> 00:13:23,200
out of this, to not need so much. We less and less and even if we get to the point of summa

119
00:13:23,200 --> 00:13:29,280
now, or the other hands that we've been talking about, then we won't need anything and our lives

120
00:13:29,280 --> 00:13:37,840
will be a blessing to the world. And the same goes with anger. The reason why we practice

121
00:13:37,840 --> 00:13:44,960
morality is to give up anger, to the same goes with morality. Morality is not that we can

122
00:13:44,960 --> 00:13:50,480
brag about it or so we can feel proud that we're keeping precepts. But it's to stop our minds

123
00:13:50,480 --> 00:13:57,280
from becoming angry, basically, by not hurting, by not saying nasty things or

124
00:13:57,280 --> 00:14:01,840
and of creating suffering for others. We're giving up our

125
00:14:03,600 --> 00:14:09,760
our nastings, our evil, the evil that we have inside. And say, and the bow and that.

126
00:14:10,720 --> 00:14:15,440
We bow another purpose as well as to give up the defilements, in this case, delusion.

127
00:14:15,440 --> 00:14:21,280
The reason, the real reason why we practice men meditation is not to give up greed and not to give

128
00:14:21,280 --> 00:14:27,920
it anger, because you can give those up with an ordinary meditation, maybe transcendental

129
00:14:27,920 --> 00:14:33,840
meditation, so a delusion can't be rooted out. If you practice loving kindness meditation,

130
00:14:33,840 --> 00:14:40,720
you can get rid of your anger, if you practice mindfulness of load subness, or dead bodies and

131
00:14:40,720 --> 00:14:45,920
so on, you can get rid of your lust. But to get rid of the illusion, you have to practice

132
00:14:46,800 --> 00:14:52,880
with us and through bow and eye. You have to look at reality and look at these things, look at

133
00:14:52,880 --> 00:14:58,400
desire, look at anger, but also look at the objects of your desire, look at the things that you

134
00:14:58,400 --> 00:15:04,880
cling to. And so by doing this, our minds are going to become perfectly developed. And as I

135
00:15:04,880 --> 00:15:12,160
talked about in the one about the rain coming in through the roof, you can cover it over with

136
00:15:12,160 --> 00:15:18,560
concentration. Your mind can be completely secure from greed and anger, but you can never secure

137
00:15:18,560 --> 00:15:23,200
it from delusion. So in order to secure it from delusion, you actually have to take the roof off

138
00:15:24,080 --> 00:15:31,840
and be open to the whole world, the whole sky. And as a result of being open and understanding

139
00:15:31,840 --> 00:15:37,120
just everything. So then whatever comes in, you understand it and you see what it is.

140
00:15:38,240 --> 00:15:43,680
There's no partiality, no categorization. You see things from what they are. You see things

141
00:15:43,680 --> 00:15:48,160
as impermanent. And don't see anything as all this is going to last and this is going to be

142
00:15:48,720 --> 00:15:54,800
the satisfying to me. You don't see things as satisfying. You see things as unsatisfying. You see

143
00:15:54,800 --> 00:16:02,400
it. It's not going as satisfying. Why I've seen it, it comes, it comes, it comes, it comes. And so

144
00:16:02,400 --> 00:16:06,400
you let go of things. You don't say this is me, this is mine, you don't think of it as controllable

145
00:16:07,120 --> 00:16:14,000
because you've seen how uncontrollably it is. By seeing this and seeing repeatedly, the mind

146
00:16:14,000 --> 00:16:20,080
eventually lets go and gives up and the mind is able to live anywhere. There's a famous story

147
00:16:20,080 --> 00:16:30,400
that actually the whole idea of covering up is not a good metaphor. So the best thatch

148
00:16:30,400 --> 00:16:35,200
had is actually working through the roof off because there's a story. This man, he's saying,

149
00:16:35,200 --> 00:16:41,760
oh, you can reign if you want because my roof is well-fetched. And the Buddha said, you can reign

150
00:16:41,760 --> 00:16:48,560
if you want. I don't have a roof. He was talking simply about the fact that he could stay under a tree

151
00:16:48,560 --> 00:16:55,040
or out in the middle of the field even in the credit crane because he has no attachments. But for

152
00:16:55,040 --> 00:17:02,560
the mind it should be the same. The mind should be able to encounter anything without any prejudice.

153
00:17:03,920 --> 00:17:08,400
So in the beginning this will take concentration. It's true that you have to, first you do have to

154
00:17:08,400 --> 00:17:14,720
push away some of the problems. You can't deal with them all at once. Once you push them away,

155
00:17:14,720 --> 00:17:19,360
then you slowly bring them back. You slowly look at them. So someone who practice is samata for

156
00:17:19,360 --> 00:17:24,960
an example. They will push everything away and they'll feel real bliss and calm. But the purpose

157
00:17:24,960 --> 00:17:31,600
of samata meditation is not to feel this and calm. It's to give you the space, push everything

158
00:17:31,600 --> 00:17:37,120
under the way. So you can slowly pick things one by one and look at them. Once you practice samata,

159
00:17:37,120 --> 00:17:42,880
you should then open your mind up, reflect back on the five aggregates, the kanda. If you don't

160
00:17:42,880 --> 00:17:47,760
ever focus on the kanda, you will never understand and do kasek. You never understand the four

161
00:17:47,760 --> 00:17:53,440
form of the tree. So even after practicing samata, you have to come back. You have to pull the roof off

162
00:17:54,400 --> 00:18:00,080
slowly and begin to look until finally you don't need to move it all. You can have the roof not have to.

163
00:18:01,280 --> 00:18:10,720
But true mental development is the opening up of the heart and keeping the mind perfectly balanced

164
00:18:10,720 --> 00:18:23,040
and centered. But letting the open to anything, open mind it. So I was talking about business.

165
00:18:23,040 --> 00:18:26,640
So I think it should be clear that what we're doing here is the greatest difference.

166
00:18:27,280 --> 00:18:33,680
And what that means is that as you're practicing, even now today, yesterday, today, the day before

167
00:18:33,680 --> 00:18:39,120
and so on, you're changing as people and your minds are changing and you'll never go back.

168
00:18:39,120 --> 00:18:45,280
And that's where you decide, I'm going to be an evil person again. You can go back. But this is

169
00:18:45,280 --> 00:18:52,160
something that you carry with you. This is what the Buddha said. B-O, B-O, Nandati, Sukatinkapunya.

170
00:18:52,160 --> 00:18:58,320
You will go to goodness and you'll find this, that your mind is pure and your whole life will be better.

171
00:18:59,600 --> 00:19:06,000
And as I've said before, people often misunderstand this. They think and people practice meditation

172
00:19:06,000 --> 00:19:09,680
also to people who are traditional Buddhists. They think this and they hear this,

173
00:19:09,680 --> 00:19:15,040
oh meditation is good, good, Buddha. So they go and practice meditation, they finish, they think,

174
00:19:15,040 --> 00:19:19,200
now I'll get rich or now I'll have this. And then sometimes horrible things happen.

175
00:19:19,920 --> 00:19:25,680
Sometimes when people finish practicing meditation, they can hit by a car, break a leg or lose

176
00:19:25,680 --> 00:19:31,360
all their money or something. It can happen like that. And it can actually happen because of the

177
00:19:31,360 --> 00:19:39,520
meditation, because what you're doing is clearing out so much garbage. But the only thing that's

178
00:19:39,520 --> 00:19:45,280
left is the really strong ones. And really strong bad karma that was just waiting or had no

179
00:19:45,280 --> 00:19:49,840
opportunity can actually get an opportunity as a result. But once it has the opportunity,

180
00:19:50,480 --> 00:19:56,240
it's finished. And the point being that the goodness that comes from it is the goodness in your

181
00:19:56,240 --> 00:20:02,160
heart, you stop thinking so much about my suffering, my happiness. And you start thinking about my

182
00:20:02,160 --> 00:20:06,240
purity, my intention, because you know our happiness and you know our suffering come from.

183
00:20:07,040 --> 00:20:10,960
Some people who practice meditation can go back and have a great amount of suffering.

184
00:20:11,520 --> 00:20:16,960
Obviously people before they meditate also have a great amount of suffering. But the difference

185
00:20:16,960 --> 00:20:21,120
is that after you practice meditation, whether it's suffering or whether it's pleasure,

186
00:20:21,120 --> 00:20:27,760
that the mind doesn't suffer. The mind suffers far less. And as a result of being pure,

187
00:20:29,120 --> 00:20:33,920
the result of the meditation, obviously that evil that happens to you is nothing to do with

188
00:20:33,920 --> 00:20:38,960
the meditation. The meditation sometimes clears the way for it to come. It can also clear the way

189
00:20:38,960 --> 00:20:44,640
for good things to come. But what you'll find after you meditate is certainly you get the

190
00:20:44,640 --> 00:20:49,120
results of karma actually quite quicker, because your mind, your whole universe is much pure.

191
00:20:49,120 --> 00:20:53,760
But the point being that the goodness that comes from the meditation and then the goodness

192
00:20:53,760 --> 00:21:00,000
that you give to others has an incredible power. It takes time, it can often not come in,

193
00:21:00,000 --> 00:21:07,520
it takes time, maybe it doesn't come in this life. But even the part that doesn't come in

194
00:21:07,520 --> 00:21:13,360
this life, it's an incredible power that's building up inside of me. And it's going to show

195
00:21:13,360 --> 00:21:20,800
itself in your next life and in all lives to come. But also a very important thing is that

196
00:21:20,800 --> 00:21:26,480
you actually do generally, or you always do, get some goodness in this life. The person who

197
00:21:26,480 --> 00:21:30,960
does good deeds is never without, it's not like you have to wait until your next life to get

198
00:21:30,960 --> 00:21:38,480
out any benefit. What you start to feel as you practice meditation, as you do any goodness,

199
00:21:38,480 --> 00:21:43,520
is with the Buddha said, cut the nape when young, I have done good. And you start to feel good

200
00:21:43,520 --> 00:21:48,880
about yourself. Because before I practice meditation, it wasn't much good about myself that I could

201
00:21:48,880 --> 00:21:54,560
feel proud of. But once you practice meditation, you know for yourself that you're doing something

202
00:21:54,560 --> 00:22:00,320
great for the world, not only for yourself, but also for the whole of the universe really.

203
00:22:00,320 --> 00:22:05,040
As I said by giving people protection from your departments, you're making the world a better place.

204
00:22:05,040 --> 00:22:11,520
You're doing the training method that all beings should undergo. I'm training this necessary to

205
00:22:11,520 --> 00:22:19,200
be kind and compassionate and generous and helpful and beneficial to the world. Because without

206
00:22:19,200 --> 00:22:26,320
wisdom, there's only a very limited amount of benefit that you can give to the world. It's with wisdom

207
00:22:26,320 --> 00:22:32,240
that you're able to solve people's problems and give people answers and generally bring

208
00:22:32,240 --> 00:22:40,480
clarity to yourself and to other beings to the whole world. So really, you're doing a great thing

209
00:22:40,480 --> 00:22:47,680
here and I'd like to express my appreciation and give you encouragement and not to be discouraged

210
00:22:47,680 --> 00:22:52,560
by the fact that it seems impossible or it seems to be going very slowly. Because it's very

211
00:22:52,560 --> 00:22:57,520
difficult to see sometimes. And there's a lot of baggage still carrying around that you have

212
00:22:57,520 --> 00:23:04,320
to be patient with. It's not going to just fall off in one day. But the results are actually

213
00:23:05,840 --> 00:23:12,560
very have a great power to them. That's not evident in the beginning. And it becomes maybe more

214
00:23:12,560 --> 00:23:16,800
evident when and if you leave or when you start to go out and teach others or help others,

215
00:23:17,360 --> 00:23:21,520
you can see that it's incredible what you're able to give as compared to what you're able to

216
00:23:21,520 --> 00:23:27,680
give before the purity of mind that you bring to any situation as opposed to the impurity that

217
00:23:27,680 --> 00:23:33,360
you would bring before. So that's the end of these four verses. The next two verses are actually

218
00:23:33,360 --> 00:23:39,040
quite different. And that's the next story has two verses. After those two verses, we finished

219
00:23:39,040 --> 00:23:47,120
the Yamakalanga, the first chapter of the Damapanda. So almost done one of, I think, 22, 23 chapters.

220
00:23:47,120 --> 00:23:52,960
And then we'll get through them all. So thanks for listening and active meditation.

