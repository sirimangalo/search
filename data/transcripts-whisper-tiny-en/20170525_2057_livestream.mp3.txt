Okay, good evening everyone, welcome to our evening dumbest session.
One very well-known description of the Buddha's teaching is as the middle way,
and so all of us are on the middle way, trying to find the middle way.
Sometimes it's described in that way.
Most famously in terms of not torturing yourself, but not becoming indulgent.
I think that applies very well to the meditation practice.
It's something that is an important lesson for us during the meditation course.
Don't torture yourself, don't force it.
Don't even force yourself, not to force it.
You realize that forcing isn't even under your control.
It says it's something that you can just steamroll over.
Don't torture yourself.
But at the same time, don't become complacent.
It seems like an intervention becomes a razor's edge.
When there's only really one way, there's no leeway.
If you want to really be in the middle way,
a nupagama, you have to not go at all in either direction.
So what we're looking for is this state of balance and the sense on a razor's edge.
And balance is a really good way to understand the middle way as well.
Now balance doesn't mean a little bit of indulgence and a little bit of torture.
Balance means to find a state that is free from such things,
like the state that is neither one direction.
