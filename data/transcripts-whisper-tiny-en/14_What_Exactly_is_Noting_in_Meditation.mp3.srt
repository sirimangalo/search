1
00:00:00,000 --> 00:00:04,000
What exactly is noting in meditation?

2
00:00:04,000 --> 00:00:09,000
Question, what exactly is noting in meditation?

3
00:00:09,000 --> 00:00:14,000
Answer, noting is the straightening of the mind in regards to the object.

4
00:00:14,000 --> 00:00:20,000
Noting is reminding yourself of the essence and reality of the experience,

5
00:00:20,000 --> 00:00:27,000
as opposed to seeing one's experiences as entities with qualities that make one react,

6
00:00:27,000 --> 00:00:33,000
liking, deciding on interpretation as me, mine, etc.

7
00:00:33,000 --> 00:00:41,000
Noting is for the purpose of experiencing reality as it is by reminding yourself what it is.

8
00:00:41,000 --> 00:00:48,000
The word sati is translated as mindfulness, but the word sati means remembrance.

9
00:00:48,000 --> 00:00:55,000
Reminding, remindedness, recollectiveness of the ability to recognize something for what it is.

10
00:00:55,000 --> 00:01:03,000
Noting is the creation in the mind of a recognition, which is why the word you choose is not so important.

11
00:01:03,000 --> 00:01:13,000
The word itself is an act of recognizing the object as close as possible to what it actually is.

