1
00:00:00,000 --> 00:00:03,000
Okay, we'll come back to ask a moment.

2
00:00:03,000 --> 00:00:08,000
Today's question comes from Stiltson.

3
00:00:08,000 --> 00:00:13,000
If a person so comes to a craving for a drug, E.G. cannabis,

4
00:00:13,000 --> 00:00:16,000
perhaps because they are only beginning to understand

5
00:00:16,000 --> 00:00:20,000
how and why to control all addictions through meditation,

6
00:00:20,000 --> 00:00:24,000
is it right to continue meditating in that altered mind state?

7
00:00:24,000 --> 00:00:29,000
It's a difficult one.

8
00:00:29,000 --> 00:00:34,000
Certainly with all addictions,

9
00:00:34,000 --> 00:00:37,000
that's really the only way you can start.

10
00:00:37,000 --> 00:00:41,000
You can't be expected to,

11
00:00:41,000 --> 00:00:45,000
in many cases you can't be expected to stop cold turkey,

12
00:00:45,000 --> 00:00:50,000
and one of the first steps is to observe the addiction itself,

13
00:00:50,000 --> 00:00:55,000
observe the state of intoxication.

14
00:00:55,000 --> 00:00:58,000
Unfortunately, it's an intoxicated state,

15
00:00:58,000 --> 00:01:04,000
and therefore it's antithetical to mindfulness or to clear awareness.

16
00:01:04,000 --> 00:01:09,000
So the ability to do so is limited.

17
00:01:09,000 --> 00:01:12,000
But it's something to keep in mind with all addictions,

18
00:01:12,000 --> 00:01:17,000
is our acceptance will help us to overcome it.

19
00:01:17,000 --> 00:01:23,000
Our acceptance of our addiction, our acknowledgement of it,

20
00:01:23,000 --> 00:01:25,000
that it does exist.

21
00:01:25,000 --> 00:01:27,000
We have this addiction.

22
00:01:27,000 --> 00:01:32,000
Yes, we do want this object.

23
00:01:32,000 --> 00:01:35,000
We do like the sensations and so on.

24
00:01:35,000 --> 00:01:39,000
And that's why we're using this mantra,

25
00:01:39,000 --> 00:01:42,000
the clear thought reminding ourselves of what it is.

26
00:01:42,000 --> 00:01:45,000
It's an affirmation of the state.

27
00:01:45,000 --> 00:01:50,000
Not saying that this is good, but saying that this is how we feel about it,

28
00:01:50,000 --> 00:01:54,000
and this is how we perceive it, and this is the nature of it.

29
00:01:54,000 --> 00:01:57,000
Once we do that,

30
00:01:57,000 --> 00:02:01,000
what happens is we start to get bored of it.

31
00:02:01,000 --> 00:02:08,000
We start to see it for what it is.

32
00:02:08,000 --> 00:02:10,000
It loses its magic.

33
00:02:10,000 --> 00:02:12,000
It loses the allure.

34
00:02:12,000 --> 00:02:17,000
It loses this illusory attractiveness.

35
00:02:17,000 --> 00:02:24,000
The truth about it is that there's nothing really attractive

36
00:02:24,000 --> 00:02:27,000
or desirable about the object at all.

37
00:02:27,000 --> 00:02:31,000
We come to see that it's something that's made up of component states

38
00:02:31,000 --> 00:02:36,000
that arise and sees that come and go and that are no in no way,

39
00:02:36,000 --> 00:02:40,000
shape or form satisfying.

40
00:02:40,000 --> 00:02:44,000
And so we see that there's no reason to go after it.

41
00:02:44,000 --> 00:02:50,000
I would say that I'm worried that it's kind of a cop out here

42
00:02:50,000 --> 00:02:53,000
where people say, well, you know,

43
00:02:53,000 --> 00:02:57,000
I may be intoxicated, but at least I'm meditating on it.

44
00:02:57,000 --> 00:03:00,000
And that's sort of getting into the idea that you can somehow meditate

45
00:03:00,000 --> 00:03:02,000
and then intoxicated state.

46
00:03:02,000 --> 00:03:06,000
And I want to make it clear that the reason why we don't do drugs

47
00:03:06,000 --> 00:03:10,000
and take alcohol is because the state of mind that it creates

48
00:03:10,000 --> 00:03:14,000
is more or less the opposite of the kind of states of mind

49
00:03:14,000 --> 00:03:17,000
that we're trying to create, which are clarity of mind.

50
00:03:17,000 --> 00:03:22,000
I would say that cannabis is not a very addictive drug

51
00:03:22,000 --> 00:03:24,000
and alcohol as well.

52
00:03:24,000 --> 00:03:26,000
I think it's probably not so addictive either.

53
00:03:26,000 --> 00:03:29,000
Physically, though some people say it is

54
00:03:29,000 --> 00:03:33,000
and there are certainly people who say it's genetic and so on.

55
00:03:33,000 --> 00:03:37,000
But I would say you're not trying to wake yourself up

56
00:03:37,000 --> 00:03:40,000
and say to yourself, you know, look, this isn't helping me.

57
00:03:40,000 --> 00:03:43,000
This isn't providing any benefit.

58
00:03:43,000 --> 00:03:46,000
And it's certainly not giving me clarity of mind.

59
00:03:46,000 --> 00:03:49,000
And so giving it up and trying to find an alternative

60
00:03:49,000 --> 00:03:53,000
that brings clarity of mind to i.e. meditation.

61
00:03:53,000 --> 00:03:56,000
Okay, so I hope that helps out.

62
00:03:56,000 --> 00:04:03,000
Thanks for the question.

