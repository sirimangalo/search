1
00:00:00,000 --> 00:00:20,000
Okay, so today we're going to talk in English.

2
00:00:20,000 --> 00:00:49,000
One of the difficulties that we have in meditation.

3
00:00:49,000 --> 00:01:05,000
Often comes from trying to find a reason why we're meditating.

4
00:01:05,000 --> 00:01:31,000
Not being able to see the reason why we meditating.

5
00:01:31,000 --> 00:01:38,000
And this becomes a problem because meditation can be very difficult.

6
00:01:38,000 --> 00:01:51,000
And so we find ourselves often in climbing towards more peaceful and pleasurable states when we meditate.

7
00:01:51,000 --> 00:02:05,000
We find ourselves easily falling into non meditative states where we're not actually contemplating or considering the objects carefully.

8
00:02:05,000 --> 00:02:16,000
We are enjoying the experience of pleasant states.

9
00:02:16,000 --> 00:02:28,000
And when unpleasant states come, we are upset by them.

10
00:02:28,000 --> 00:02:43,000
And so when we're taught to acknowledge and to accept things as they are,

11
00:02:43,000 --> 00:03:00,000
there arises a question in the mind why, why sit in torture ourselves.

12
00:03:00,000 --> 00:03:04,000
This is a big question in the beginning.

13
00:03:04,000 --> 00:03:09,000
Later on, once we've become more efficient in the practice,

14
00:03:09,000 --> 00:03:17,000
the question might not arise, but it's still very easy to lose sight of why we're meditating.

15
00:03:17,000 --> 00:03:23,000
And lose our conviction and our exertion in the practice.

16
00:03:23,000 --> 00:03:46,000
We find ourselves slacking off, we're getting bored, losing interest, losing our interest in the practice.

17
00:03:46,000 --> 00:03:53,000
And I think we should always consider the results of what we do.

18
00:03:53,000 --> 00:04:05,000
And when we do this, we can see what is of real use and meditation becomes something that is very easy to see the benefit of.

19
00:04:05,000 --> 00:04:12,000
Because when we're not meditating, when we're not practicing, when we're not seeing things clearly,

20
00:04:12,000 --> 00:04:20,000
there are results as well.

21
00:04:20,000 --> 00:04:32,000
When we cling to things, when we desire for things, when we like things,

22
00:04:32,000 --> 00:04:41,000
these are actions don't go without result.

23
00:04:41,000 --> 00:04:47,000
When we dislike things and we're upset, when we get angry,

24
00:04:47,000 --> 00:05:10,000
this isn't without results as well.

25
00:05:10,000 --> 00:05:18,000
People who have agreed have a real hard time seeing the problem.

26
00:05:18,000 --> 00:05:21,000
People with anger tend to see the problem more.

27
00:05:21,000 --> 00:05:27,000
It's not to say that they can't fix it, but they're able to fix it.

28
00:05:27,000 --> 00:05:30,000
But it's easier to see.

29
00:05:30,000 --> 00:05:38,000
Agreed is something very difficult to see and it's very difficult to see the downside of it.

30
00:05:38,000 --> 00:05:44,000
We're going to get more and more pleasure and happiness.

31
00:05:44,000 --> 00:05:47,000
And in fact, the opposite is the case.

32
00:05:47,000 --> 00:05:50,000
What we don't see is that we slowly become ghosts.

33
00:05:50,000 --> 00:05:54,000
We're slowly become spirits.

34
00:05:54,000 --> 00:06:02,000
We waste the way we lose all of our substance, like a drug addict.

35
00:06:02,000 --> 00:06:09,000
And so this is why the Buddha said when you die, greed leads you to be born as a ghost.

36
00:06:09,000 --> 00:06:11,000
You can see it in people.

37
00:06:11,000 --> 00:06:24,000
They become demons, like ogres, like ghosts, wailing and complaining and clinging.

38
00:06:24,000 --> 00:06:37,000
Very much clinging.

39
00:06:37,000 --> 00:06:46,000
People who have anger.

40
00:06:46,000 --> 00:06:51,000
They put themselves in hell.

41
00:06:51,000 --> 00:06:54,000
When we get angry, you can see this in people.

42
00:06:54,000 --> 00:06:56,000
When they're angry, they're in hell.

43
00:06:56,000 --> 00:06:59,000
They're in the hellish state.

44
00:06:59,000 --> 00:07:10,000
They're in the state of deprivation, of loss, a state of pain and suffering.

45
00:07:10,000 --> 00:07:16,000
The Buddha said if you want to...

46
00:07:16,000 --> 00:07:19,000
If you want to hurt someone, make them angry.

47
00:07:19,000 --> 00:07:26,000
If someone who gets angry, does their enemy a favor?

48
00:07:26,000 --> 00:07:34,000
When we get angry at someone else, someone does something or says something nasty and trying to make us upset.

49
00:07:34,000 --> 00:07:39,000
When we get upset, we do them a favor.

50
00:07:39,000 --> 00:07:40,000
They want to hurt us.

51
00:07:40,000 --> 00:07:45,000
We hurt ourselves.

52
00:07:45,000 --> 00:07:54,000
When we get angry at someone, we've accomplished their goal.

53
00:07:54,000 --> 00:07:59,000
They've put ourselves in suffering.

54
00:07:59,000 --> 00:08:03,000
And so we go to hell.

55
00:08:03,000 --> 00:08:04,000
We fall into hell.

56
00:08:04,000 --> 00:08:13,000
Even in this life, people who are always angry look like devils, demons, hell beings.

57
00:08:13,000 --> 00:08:31,000
And when they die, the Buddha said this is where they go.

58
00:08:31,000 --> 00:08:41,000
People with delusion when we're full of ignorance and disinterest and arrogance and self-appreciation and so on.

59
00:08:41,000 --> 00:08:50,000
And views and opinions.

60
00:08:50,000 --> 00:08:54,000
This is the realm of animals.

61
00:08:54,000 --> 00:09:01,000
People who are ignorant, who have no interest in mental development, self-development,

62
00:09:01,000 --> 00:09:08,000
who are interested in higher things like philosophy and religion,

63
00:09:08,000 --> 00:09:17,000
who have no interest in science and understanding.

64
00:09:17,000 --> 00:09:25,000
People who are full of ideas and views and opinions,

65
00:09:25,000 --> 00:09:34,000
will have at these low states of minds of conceit and arrogance, bravado.

66
00:09:34,000 --> 00:09:39,000
These are animal states. This is the way of animals.

67
00:09:39,000 --> 00:09:42,000
Insects sometimes.

68
00:09:42,000 --> 00:09:47,000
Look at these people who have a great amount of intelligence.

69
00:09:47,000 --> 00:10:00,000
They can't avoid thinking that they're not like insects because they're insect-like behavior.

70
00:10:00,000 --> 00:10:04,000
They're animal-like behavior.

71
00:10:04,000 --> 00:10:13,000
People who boast and brag and show off all of these different states.

72
00:10:13,000 --> 00:10:16,000
It's the state of delusion.

73
00:10:16,000 --> 00:10:20,000
It's a very low state.

74
00:10:20,000 --> 00:10:24,000
It shows someone's inferiority.

75
00:10:24,000 --> 00:10:31,000
It's a sign of someone who is a superior human being.

76
00:10:31,000 --> 00:10:35,000
They don't show off.

77
00:10:35,000 --> 00:10:41,000
They don't hold opinions and views and beliefs.

78
00:10:41,000 --> 00:10:45,000
They don't try to impress themselves on others.

79
00:10:45,000 --> 00:10:48,000
And they're very keen on learning and understanding.

80
00:10:48,000 --> 00:10:56,000
They're very keen on improved self-improvement.

81
00:10:56,000 --> 00:11:01,000
When you see people like this, you can see that they're not like animals.

82
00:11:01,000 --> 00:11:12,000
They're not living their life just by eating and sleeping and indulging in central pleasures.

83
00:11:12,000 --> 00:11:17,000
They have something higher on their minds.

84
00:11:17,000 --> 00:11:20,000
They're higher purpose.

85
00:11:20,000 --> 00:11:25,000
So these things also have results in these states of mind.

86
00:11:25,000 --> 00:11:27,000
In this life, they have very clear results.

87
00:11:27,000 --> 00:11:30,000
And in the afterlife, they have very clear results.

88
00:11:30,000 --> 00:11:33,000
There's no question where these things lead.

89
00:11:33,000 --> 00:11:38,000
But it's a jit day, sankini day, tukati, patikanka.

90
00:11:38,000 --> 00:11:41,000
The mind that is defiled leads to suffering.

91
00:11:41,000 --> 00:12:03,000
It leads to hell, leads to a bad destination without doubt, but tukanka undoubtedly.

92
00:12:03,000 --> 00:12:09,000
And see, the problem is when we don't meditate these states are ever present.

93
00:12:09,000 --> 00:12:12,000
We don't have to do anything special for them to arise.

94
00:12:12,000 --> 00:12:17,000
All we have to do is live our lives as we ordinary would and they come up anyway.

95
00:12:17,000 --> 00:12:25,000
We get angry, we become greedy, and we're deluded all the time.

96
00:12:25,000 --> 00:12:30,000
It takes a special state of mind to overcome these.

97
00:12:30,000 --> 00:12:37,000
The special states of kindness, compassion, caring,

98
00:12:37,000 --> 00:12:49,000
states of morality and abstention,

99
00:12:49,000 --> 00:13:00,000
states of wisdom and understanding.

100
00:13:00,000 --> 00:13:04,000
So this is the really very important reason why we should practice meditation.

101
00:13:04,000 --> 00:13:07,000
Meditation is the development of the mind.

102
00:13:07,000 --> 00:13:16,000
Without a developed mind, our animal minds, our hell minds, our ghost minds will come up by itself.

103
00:13:16,000 --> 00:13:20,000
We don't have to bring it up or wish for it to come up.

104
00:13:20,000 --> 00:13:26,000
It's the natural state for us to be greedy, angry, and deluded again and again.

105
00:13:26,000 --> 00:13:29,000
We have to work hard to overcome these.

106
00:13:29,000 --> 00:13:34,000
So the meaning is then we have to work hard just to be happy, just to be at peace.

107
00:13:34,000 --> 00:13:41,000
Happiness and peace don't come from slacking off, not in the way we think that they do.

108
00:13:41,000 --> 00:13:47,000
I think that by taking it easy, good things will come and our minds will be happy.

109
00:13:47,000 --> 00:13:54,000
And it's not really the case we find ourselves becoming angry and greedy and deluded.

110
00:13:54,000 --> 00:14:09,000
But when we develop our minds, when we work on our minds, try to see things as they are.

111
00:14:09,000 --> 00:14:11,000
We're able to do away with these.

112
00:14:11,000 --> 00:14:12,000
We're able to overcome them.

113
00:14:12,000 --> 00:14:21,000
We're able to give rise to states of peace, of clarity of mind.

114
00:14:21,000 --> 00:14:26,000
We're no longer angry at bad things.

115
00:14:26,000 --> 00:14:29,000
When bad things come to us unpleasant things come to us.

116
00:14:29,000 --> 00:14:31,000
We no longer get angry at them.

117
00:14:31,000 --> 00:14:36,000
We no longer see them as unpleasant.

118
00:14:36,000 --> 00:14:41,000
We're retraining our minds to see things as they are.

119
00:14:41,000 --> 00:14:44,000
And it really is a retraining.

120
00:14:44,000 --> 00:14:49,000
You can't say, oh, I already see things as they are because we don't.

121
00:14:49,000 --> 00:14:52,000
Every time we see something, we have an judgment of it.

122
00:14:52,000 --> 00:14:54,000
Here's something, we have a judgment of it.

123
00:14:54,000 --> 00:14:55,000
That's our natural state.

124
00:14:55,000 --> 00:14:57,000
We don't have to work to judge.

125
00:14:57,000 --> 00:15:02,000
Judging is our ordinary way of behavior.

126
00:15:02,000 --> 00:15:05,000
We have these chain reactions.

127
00:15:05,000 --> 00:15:07,000
And we have to break the chain.

128
00:15:07,000 --> 00:15:11,000
The work we have to do is to break the chain.

129
00:15:11,000 --> 00:15:16,000
You can't just expect the chain to not form.

130
00:15:16,000 --> 00:15:18,000
It forms by nature.

131
00:15:18,000 --> 00:15:24,000
So meditation has a result.

132
00:15:24,000 --> 00:15:26,000
It has a very clear result.

133
00:15:26,000 --> 00:15:30,000
When we do acknowledge, we see things clearly.

134
00:15:30,000 --> 00:15:35,000
And that's an incredible benefit because then the chain is broken.

135
00:15:35,000 --> 00:15:39,000
We don't go into liking or disliking.

136
00:15:39,000 --> 00:15:44,000
We don't go on to judge the object.

137
00:15:44,000 --> 00:15:48,000
We simply see it for what it is.

138
00:15:48,000 --> 00:15:54,000
When we see seeing or hearing here.

139
00:15:54,000 --> 00:15:57,000
If we're clear about it, then there is no liking or disliking.

140
00:15:57,000 --> 00:15:59,000
There's no judging.

141
00:15:59,000 --> 00:16:03,000
There's no good or bad.

142
00:16:03,000 --> 00:16:08,000
There is what there is.

143
00:16:08,000 --> 00:16:15,000
There's no good.

144
00:16:15,000 --> 00:16:20,000
There's no good.

145
00:16:20,000 --> 00:16:25,000
There's no good.

146
00:16:25,000 --> 00:16:29,000
There's no good.

147
00:16:29,000 --> 00:16:42,000
The great thing about meditation is not only this,

148
00:16:42,000 --> 00:16:46,000
but at the moment when we're mindful, our mind is clear.

149
00:16:46,000 --> 00:16:49,000
The great thing is that it slowly changes our mind,

150
00:16:49,000 --> 00:16:52,000
so that that becomes the default.

151
00:16:52,000 --> 00:16:56,000
So that the default is seeing things clearly.

152
00:16:56,000 --> 00:16:58,000
Slowly, we begin to see things in a way

153
00:16:58,000 --> 00:17:03,000
that we didn't see them before and begin to understand things

154
00:17:03,000 --> 00:17:06,000
in a way that we didn't understand them before.

155
00:17:06,000 --> 00:17:17,000
And our greed and our anger and our delusion become foreign states

156
00:17:17,000 --> 00:17:21,000
that come in months in a while.

157
00:17:21,000 --> 00:17:25,000
And eventually don't come in at all.

158
00:17:25,000 --> 00:17:32,000
And then being replaced by the opposite.

159
00:17:40,000 --> 00:17:47,000
So this is really the difference between the negative states

160
00:17:47,000 --> 00:17:49,000
and the positive states.

161
00:17:49,000 --> 00:17:53,000
If you create enough wisdom and understanding,

162
00:17:53,000 --> 00:18:00,000
you can never give rise to unwholesome states again.

163
00:18:00,000 --> 00:18:06,000
Because you can't do away with your wisdom.

164
00:18:06,000 --> 00:18:09,000
You can't give up your understanding.

165
00:18:09,000 --> 00:18:13,000
Ignorance is something we can do away with by understanding.

166
00:18:13,000 --> 00:18:22,000
But once you understand something, you can't go back to being ignorant about it.

167
00:18:22,000 --> 00:18:29,000
Once you see the way things work, your whole universe,

168
00:18:29,000 --> 00:18:32,000
your whole existence changes.

169
00:18:32,000 --> 00:18:35,000
It becomes lighter.

170
00:18:35,000 --> 00:18:36,000
It becomes simpler.

171
00:18:36,000 --> 00:18:38,000
It becomes cleaner.

172
00:18:38,000 --> 00:18:39,000
It becomes clearer.

173
00:18:39,000 --> 00:18:48,000
And there's less of the complication

174
00:18:48,000 --> 00:18:56,000
that we find in ordinary life.

175
00:18:56,000 --> 00:18:59,000
And this is really leading to what the Buddha meant by Nibana.

176
00:18:59,000 --> 00:19:03,000
The real result of meditation is Nibana or freedom.

177
00:19:03,000 --> 00:19:06,000
And we don't ever go back.

178
00:19:06,000 --> 00:19:08,000
And the person enters into the light.

179
00:19:08,000 --> 00:19:12,000
They don't go back to the darkness.

180
00:19:12,000 --> 00:19:18,000
And the person sees clearly they can never be fooled again.

181
00:19:18,000 --> 00:19:23,000
All it takes is to see the truth once, clearly and completely.

182
00:19:23,000 --> 00:19:27,000
And you'll never go back to the default.

183
00:19:27,000 --> 00:19:29,000
Your whole universe changes.

184
00:19:29,000 --> 00:19:32,000
Your whole path changes.

185
00:19:32,000 --> 00:19:34,000
We talk about the afterlife.

186
00:19:34,000 --> 00:19:38,000
When you see things clearly, your afterlife changes.

187
00:19:38,000 --> 00:19:40,000
Your future changes.

188
00:19:40,000 --> 00:19:44,000
Simply by changing the present, you can change your whole life.

189
00:19:44,000 --> 00:19:48,000
When the future looks dismal or unclear.

190
00:19:48,000 --> 00:19:50,000
Simply by meditating.

191
00:19:50,000 --> 00:19:54,000
You can change the whole of your future.

192
00:19:54,000 --> 00:19:56,000
Everything changes for you.

193
00:19:56,000 --> 00:19:59,000
When you start to see things clearly.

194
00:19:59,000 --> 00:20:04,000
When you start to give rise to wisdom.

195
00:20:04,000 --> 00:20:07,000
So meditation certainly does have a result.

196
00:20:07,000 --> 00:20:09,000
It's something with Bennett with great benefit.

197
00:20:09,000 --> 00:20:13,000
And it's something that's necessary to retrain our minds,

198
00:20:13,000 --> 00:20:20,000
to keep our minds from falling back again and again into unwholesome state.

199
00:20:20,000 --> 00:20:24,000
This is something that we should think about often.

200
00:20:24,000 --> 00:20:28,000
And we should review our meditation and ask ourselves whether we really

201
00:20:28,000 --> 00:20:31,000
are putting our whole heart into the meditation.

202
00:20:31,000 --> 00:20:35,000
Or whether we've lost sight of the goal and the reason and the benefit.

203
00:20:35,000 --> 00:20:41,000
And therefore, not putting our whole heart into it.

204
00:20:41,000 --> 00:20:45,000
It's only with putting our whole heart into it that we can really expect results,

205
00:20:45,000 --> 00:20:48,000
not just by walking and sitting blindly.

206
00:20:48,000 --> 00:20:55,000
So it's worth encouraging in this way.

207
00:20:55,000 --> 00:21:00,000
And this is the teaching for today.

208
00:21:00,000 --> 00:21:03,000
I hope that's reduced to everyone.

209
00:21:03,000 --> 00:21:06,000
Now we'll continue on with the practical part.

210
00:21:06,000 --> 00:21:09,000
First of all, do mindful frustration.

211
00:21:09,000 --> 00:21:34,000
And then walking in this way.

