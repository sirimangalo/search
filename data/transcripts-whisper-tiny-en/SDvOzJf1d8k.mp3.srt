1
00:00:00,000 --> 00:00:07,000
Good evening everyone.

2
00:00:07,000 --> 00:00:29,000
I'm broadcasting live April 26th.

3
00:00:29,000 --> 00:00:33,000
Today is quote again about anger.

4
00:00:33,000 --> 00:00:42,000
It's time from Namisudimaga.

5
00:00:42,000 --> 00:00:45,000
It's under the teaching about Mehta.

6
00:00:45,000 --> 00:00:52,000
So in order to cultivate Mehta you have to be skillful and overcoming anger.

7
00:00:52,000 --> 00:01:03,000
And then there's this set of verses that forms tonight's quote.

8
00:01:03,000 --> 00:01:12,000
So the first stand is if someone hurts you,

9
00:01:12,000 --> 00:01:16,000
it hurts you insofar as they can.

10
00:01:16,000 --> 00:01:21,000
You've done whatever they can to hurt you.

11
00:01:21,000 --> 00:01:26,000
That makes you hurt once.

12
00:01:26,000 --> 00:01:34,000
You heard because of the things that they've done.

13
00:01:34,000 --> 00:01:38,000
And then the question is the question you should ask yourself.

14
00:01:38,000 --> 00:01:45,000
So this quote is actually supposed to be directed to yourself.

15
00:01:45,000 --> 00:01:50,000
When you get angry about something someone has done to you,

16
00:01:50,000 --> 00:01:55,000
you should ask yourself, why do I want to hurt myself again?

17
00:01:55,000 --> 00:02:00,000
Why am I sitting here hurting myself?

18
00:02:00,000 --> 00:02:02,000
Because someone has hurt me.

19
00:02:02,000 --> 00:02:04,000
Why am I doing it again?

20
00:02:04,000 --> 00:02:07,000
Why am I making it twice as bad?

21
00:02:07,000 --> 00:02:13,000
Why am I helping my enemies?

22
00:02:13,000 --> 00:02:21,000
Well people who want to bring me suffering, why do I suffer?

23
00:02:21,000 --> 00:02:25,000
Do you think that anger is a valid response or proper response?

24
00:02:25,000 --> 00:02:27,000
To anything really.

25
00:02:27,000 --> 00:02:31,000
But as said, if you're in pain and then you get upset about it,

26
00:02:31,000 --> 00:02:39,000
it's like you have a thorn or a splinter in your skin

27
00:02:39,000 --> 00:02:43,000
and you take another thorn and you try to gouge it out,

28
00:02:43,000 --> 00:02:50,000
you make it twice as bad.

29
00:02:50,000 --> 00:02:53,000
The second stand size.

30
00:02:53,000 --> 00:02:55,000
In tears you left your family.

31
00:02:55,000 --> 00:02:59,000
They have been kind and helpful to.

32
00:02:59,000 --> 00:03:06,000
So why not leave your enemy the anger that brings harm to you?

33
00:03:06,000 --> 00:03:11,000
So we left behind all that was dear to us to come to meditate to

34
00:03:11,000 --> 00:03:16,000
cultivate spirituality, we give up all the good things

35
00:03:16,000 --> 00:03:25,000
and all the good things or pleasurable things in the world.

36
00:03:25,000 --> 00:03:29,000
And yet we bring our enemies with us.

37
00:03:29,000 --> 00:03:34,000
Now when we get angry, why are we holding on to bad things?

38
00:03:34,000 --> 00:03:46,000
This is an admonishment, a self admonishment for someone who is angry.

39
00:03:46,000 --> 00:03:55,000
Actually it's generally easier to give up anger than it is to give up greed or craving.

40
00:03:55,000 --> 00:04:01,000
And this kind of admonishment is actually quite fairly simple.

41
00:04:01,000 --> 00:04:04,000
Just remind yourself that this is hurting you.

42
00:04:04,000 --> 00:04:06,000
This is harmful.

43
00:04:06,000 --> 00:04:17,000
The anger, the reaction, the judging.

44
00:04:17,000 --> 00:04:22,000
This anger that you entertain, the third stands of this anger that you entertain.

45
00:04:22,000 --> 00:04:26,000
And again I'm reading actually a neonomally translation,

46
00:04:26,000 --> 00:04:30,000
so it's a bit different from the one on our website.

47
00:04:30,000 --> 00:04:37,000
This anger that you entertain is gnawing at the very roots of all the virtue that you guard.

48
00:04:37,000 --> 00:04:41,000
Who is there such a fool as you?

49
00:04:41,000 --> 00:04:47,000
Anger, greed, they gnaw away at our goodness.

50
00:04:47,000 --> 00:04:55,000
They chew at it, they eat away at all the good that we've done.

51
00:04:55,000 --> 00:05:03,000
It gets spoiled. You can be helpful to people and charitable and kind.

52
00:05:03,000 --> 00:05:11,000
Then you're angry one time and you lose your reputation as a good person.

53
00:05:11,000 --> 00:05:24,000
And it inflames your mind and it prevents you from enjoying the peace and happiness that come from goodness.

54
00:05:24,000 --> 00:05:28,000
Remind ourselves of how it's eating away at our virtue.

55
00:05:28,000 --> 00:05:32,000
It's making us making us more coarse.

56
00:05:32,000 --> 00:05:42,000
It helps us to give up the inclination to be angry, to get angry.

57
00:05:42,000 --> 00:05:46,000
And the next stand, another does ignobled deeds.

58
00:05:46,000 --> 00:05:49,000
So you are angry. How is this?

59
00:05:49,000 --> 00:05:56,000
Do you then want to copy to the sort of acts that he commits?

60
00:05:56,000 --> 00:06:01,000
So this person has done something evil and it is evil.

61
00:06:01,000 --> 00:06:05,000
And so why in the world are you now?

62
00:06:05,000 --> 00:06:08,000
Who believes that it was wrong?

63
00:06:08,000 --> 00:06:13,000
Why are you now doing the same thing?

64
00:06:13,000 --> 00:06:20,000
That's a curious point because when you get angry at someone who's evil, you're joining them in their evil.

65
00:06:20,000 --> 00:06:25,000
In fact, the Buddha said, and it mentions in this up above that,

66
00:06:25,000 --> 00:06:28,000
it's worse to get angry back at someone.

67
00:06:28,000 --> 00:06:30,000
It's a worse evil.

68
00:06:30,000 --> 00:06:34,000
Someone's angry at you and you return the anger.

69
00:06:34,000 --> 00:06:37,000
That's worse because we all get angry.

70
00:06:37,000 --> 00:06:47,000
We anger comes and if we love each other, then we let each other vent.

71
00:06:47,000 --> 00:06:51,000
When someone gets angry at us, we appreciate what they're saying.

72
00:06:51,000 --> 00:06:57,000
We try to calm them down and appease them in some way.

73
00:06:57,000 --> 00:07:03,000
But when you get angry back, then you start to fight and start conflict.

74
00:07:03,000 --> 00:07:05,000
That's when the real problem is.

75
00:07:05,000 --> 00:07:07,000
And this is a good lesson.

76
00:07:07,000 --> 00:07:12,000
We judge each other very much and if someone gets angry, we get angry back.

77
00:07:12,000 --> 00:07:15,000
If someone is greedy, we get upset at them.

78
00:07:15,000 --> 00:07:22,000
If someone is arrogant and conceited, we get angry and upset about the bad things in others.

79
00:07:22,000 --> 00:07:26,000
And then that makes us just as bad.

80
00:07:26,000 --> 00:07:29,000
Moreover, we're too critical.

81
00:07:29,000 --> 00:07:33,000
Critical of ourselves and critical of each other.

82
00:07:33,000 --> 00:07:41,000
And in monasteries, it's quite common for new monks to come and just criticize around the criticize every other monk.

83
00:07:41,000 --> 00:07:49,000
All the old monks, all the monks who've been there for a long time because they're not perfect.

84
00:07:49,000 --> 00:07:55,000
With this kind of conception that you're not perfect, then you deserve to be criticized.

85
00:07:55,000 --> 00:08:07,000
Really, if you're not perfect, you deserve to be supported and directed and guided and pushed and prodded in the right direction.

86
00:08:07,000 --> 00:08:13,000
But to denounce someone because they're imperfect, that's not the Buddhist way.

87
00:08:13,000 --> 00:08:23,000
For more reasons than one, because it's useless, it's unhelpful and because it's harmful to yourself as well.

88
00:08:23,000 --> 00:08:35,000
The next stanza just says that when you get angry at something someone has done, you do what they would have you do.

89
00:08:35,000 --> 00:08:39,000
They want you to suffer and well, you can go to your suffering.

90
00:08:39,000 --> 00:08:44,000
And that's key because really other people can't make us suffer.

91
00:08:44,000 --> 00:08:50,000
They can hit you, they can denounce you, they can scold you, they can insult you.

92
00:08:50,000 --> 00:08:52,000
But they can't make you suffer.

93
00:08:52,000 --> 00:08:55,000
They can't make your mind suffer.

94
00:08:55,000 --> 00:09:00,000
They can't make you get angry.

95
00:09:00,000 --> 00:09:11,000
I mean, it depends how you look at it, but it's possible to be free from anger no matter what someone else does.

96
00:09:11,000 --> 00:09:17,000
But once you get angry, that's when the suffering comes.

97
00:09:17,000 --> 00:09:20,000
There's no question you suffer when you're angry.

98
00:09:20,000 --> 00:09:30,000
You suffer when you dislike something, that's where suffering comes from.

99
00:09:30,000 --> 00:09:37,000
Next stanza, if you get angry, then maybe you make him suffer, maybe not.

100
00:09:37,000 --> 00:09:42,000
The width, the hurt that anger brings you certainly are punished now.

101
00:09:42,000 --> 00:09:49,000
So when you get angry back, usually it's because you are thinking of ways to retaliate, right?

102
00:09:49,000 --> 00:09:51,000
To seek revenge.

103
00:09:51,000 --> 00:09:55,000
And so they're like, well maybe, maybe you can find revenge.

104
00:09:55,000 --> 00:09:59,000
Maybe you'll actually end up harming the other person and getting revenge.

105
00:09:59,000 --> 00:10:01,000
We don't know, it's possible.

106
00:10:01,000 --> 00:10:06,000
But what's certain is that you're punishing yourself with your anger.

107
00:10:06,000 --> 00:10:11,000
And you punish yourself more if you seek revenge.

108
00:10:11,000 --> 00:10:16,000
If anger blinded enemies set out to tread the path of woe,

109
00:10:16,000 --> 00:10:22,000
do you by getting angry too, intend to follow heel to toe?

110
00:10:22,000 --> 00:10:27,000
You intend to follow these anger blinded enemies.

111
00:10:27,000 --> 00:10:32,000
If hurt is done, you buy a foe because of anger on your part,

112
00:10:32,000 --> 00:10:39,000
then put your anger down for why should you be harassed groundlessly.

113
00:10:39,000 --> 00:10:54,000
That's why you weren't the one that did the evil, why are you suffering?

114
00:10:54,000 --> 00:10:59,000
Someone hurts you or attacks you or criticizes you or so.

115
00:10:59,000 --> 00:11:02,000
That's their problem.

116
00:11:02,000 --> 00:11:06,000
Why are you suffering?

117
00:11:06,000 --> 00:11:12,000
Since states last but a moment's time, those aggregates by which was done,

118
00:11:12,000 --> 00:11:14,000
the odious act have ceased.

119
00:11:14,000 --> 00:11:17,000
So now, what is it you are angry with?

120
00:11:17,000 --> 00:11:20,000
This is very much in meditation.

121
00:11:20,000 --> 00:11:23,000
The person who were angry doesn't exist.

122
00:11:23,000 --> 00:11:29,000
The states that arose that gave rise to the event that were angry at that,

123
00:11:29,000 --> 00:11:31,000
those have ceased.

124
00:11:31,000 --> 00:11:38,000
We're angry at something or worried about something or when we're attached to something we want something.

125
00:11:38,000 --> 00:11:41,000
That thing is only a concept.

126
00:11:41,000 --> 00:11:44,000
The experience is a rise and it's a good thing.

127
00:11:44,000 --> 00:11:47,000
Well, that good thing is not really good in any way.

128
00:11:47,000 --> 00:11:52,000
It just arises and it comes and goes in the same with bad things.

129
00:11:52,000 --> 00:11:55,000
There's no person who is our enemy.

130
00:11:55,000 --> 00:11:57,000
What are we angry at?

131
00:11:57,000 --> 00:11:59,000
What really are we angry at?

132
00:11:59,000 --> 00:12:02,000
What we're angry at is a memory, a thought.

133
00:12:02,000 --> 00:12:08,000
A thought that arises in our mind that is an echo of some past deed.

134
00:12:08,000 --> 00:12:14,000
And we get angry at that thought.

135
00:12:14,000 --> 00:12:19,000
Whom shall he hurt who seeks to hurt another in the others absence?

136
00:12:19,000 --> 00:12:21,000
Your presence is the cause of hurt.

137
00:12:21,000 --> 00:12:24,000
Why are you angry then with him?

138
00:12:24,000 --> 00:12:26,000
Yes, we're angry at ourselves actually.

139
00:12:26,000 --> 00:12:30,000
It's our own mind states, the thoughts that arise in our own mind.

140
00:12:30,000 --> 00:12:37,000
The person who did this deed, if someone sits in his very angry at another person,

141
00:12:37,000 --> 00:12:39,000
something to do with the other person.

142
00:12:39,000 --> 00:12:42,000
Let's do yourself for hurting yourself.

143
00:12:42,000 --> 00:12:49,000
You're angry at the thoughts that arise in your own mind, nothing to do with the other person.

144
00:12:49,000 --> 00:13:01,000
So this is to do with anger, but more generally it's the idea that only we can hurt ourselves.

145
00:13:01,000 --> 00:13:05,000
And happiness as well has to come from within.

146
00:13:05,000 --> 00:13:11,000
Happiness can't be based on a thing.

147
00:13:11,000 --> 00:13:13,000
And suffering doesn't come from things.

148
00:13:13,000 --> 00:13:16,000
Happiness and suffering don't come from the outside.

149
00:13:16,000 --> 00:13:18,000
They come from within.

150
00:13:18,000 --> 00:13:25,000
It's quite simple, our state of mind determines our state of happiness.

151
00:13:25,000 --> 00:13:28,000
We're not the world around us.

152
00:13:28,000 --> 00:13:30,000
This is crucial.

153
00:13:30,000 --> 00:13:33,000
It's a specific Buddhist doctrine,

154
00:13:33,000 --> 00:13:37,000
that happiness and suffering come from the mind.

155
00:13:37,000 --> 00:13:39,000
This is clear.

156
00:13:39,000 --> 00:13:51,000
So a big part of our practice is learning to be objective and to see the experiences that come from external.

157
00:13:51,000 --> 00:14:00,000
For that come from body and the mind.

158
00:14:00,000 --> 00:14:08,000
To see them with wisdom.

159
00:14:08,000 --> 00:14:19,000
To cultivate a relationship with experience that is free from likes and dislike.

160
00:14:19,000 --> 00:14:22,000
To be objective and to see things as they are.

161
00:14:22,000 --> 00:14:32,000
Because when you do, it's not intrinsic like ability or just like ability.

162
00:14:32,000 --> 00:14:39,000
Attraction, attractiveness or under attractiveness.

163
00:14:39,000 --> 00:14:49,000
It's not a quality of things.

164
00:14:49,000 --> 00:14:55,000
And so when we look at things as they are, we find that they're not attractive.

165
00:14:55,000 --> 00:14:59,000
We don't give rise to the attraction.

166
00:14:59,000 --> 00:15:03,000
Attraction and aversion, they come from not seeing things as they are.

167
00:15:03,000 --> 00:15:05,000
That's key as well.

168
00:15:05,000 --> 00:15:07,000
Because it could be otherwise.

169
00:15:07,000 --> 00:15:09,000
Mostly we think it's otherwise.

170
00:15:09,000 --> 00:15:13,000
We think that some things are intrinsically beautiful or desirable.

171
00:15:13,000 --> 00:15:15,000
Some of the things are not.

172
00:15:15,000 --> 00:15:20,000
Our intrinsically dislikeable.

173
00:15:20,000 --> 00:15:23,000
A cause for disliking.

174
00:15:23,000 --> 00:15:24,000
It's not true.

175
00:15:24,000 --> 00:15:28,000
When you see things as they are,

176
00:15:28,000 --> 00:15:30,000
it's only because delusion.

177
00:15:30,000 --> 00:15:32,000
Because of delusion that we.

178
00:15:32,000 --> 00:15:35,000
Because of misconception.

179
00:15:35,000 --> 00:15:39,000
That we have desire and aversion.

180
00:15:39,000 --> 00:15:45,000
So that's our dhamma for tonight.

181
00:15:45,000 --> 00:15:57,000
If you're interested in the Risudhi manga, we're actually studying it on week on Sundays.

182
00:15:57,000 --> 00:16:01,000
But we're most of the way through it already.

183
00:16:01,000 --> 00:16:04,000
Risudhi manga.

184
00:16:04,000 --> 00:16:08,000
I appreciate Risudhi manga.

185
00:16:08,000 --> 00:16:17,000
It's a sort of more like a manual or a reference guide.

186
00:16:17,000 --> 00:16:19,000
It's good for teachers.

187
00:16:19,000 --> 00:16:21,000
It's also good for meditators.

188
00:16:21,000 --> 00:16:24,000
Experience meditators, I think.

189
00:16:24,000 --> 00:16:27,000
There's just so much in it, so many different.

190
00:16:27,000 --> 00:16:31,000
Like this set of verses is quite useful.

191
00:16:31,000 --> 00:16:33,000
That's from the Risudhi manga.

192
00:16:37,000 --> 00:16:38,000
You too can go.

193
00:16:38,000 --> 00:16:40,000
Don't have to stay here.

194
00:16:40,000 --> 00:16:57,000
I'm just going to answer questions if people have them.

195
00:16:57,000 --> 00:17:03,000
So, I was in New York.

196
00:17:03,000 --> 00:17:11,000
The monk I was staying with in New York doesn't like the Risudhi manga.

197
00:17:11,000 --> 00:17:13,000
Or the commentaries.

198
00:17:13,000 --> 00:17:17,000
Or the abidhamma, which is very common with Western monks.

199
00:17:17,000 --> 00:17:20,000
Especially in the Ajahn chag group.

200
00:17:20,000 --> 00:17:24,000
Although he's not exactly with the Ajahn chag group.

201
00:17:24,000 --> 00:17:28,000
So, keen on the sutives, which is great.

202
00:17:28,000 --> 00:17:32,000
The sutives are just the most pure.

203
00:17:32,000 --> 00:17:39,000
But, you know, the sutives are not the only source of Buddhism.

204
00:17:39,000 --> 00:17:48,000
And the sutives are talks that were given mostly that were given to the monks,

205
00:17:48,000 --> 00:17:51,000
given to certain people at a certain time.

206
00:17:51,000 --> 00:17:59,000
And so, they have to be interpreted, especially because we're so far removed from that time in that place.

207
00:17:59,000 --> 00:18:03,000
And so, no matter what, you have to explain the sutives.

208
00:18:03,000 --> 00:18:08,000
And so, whether you follow the commentary explanation, we sutimanga explanation.

209
00:18:08,000 --> 00:18:11,000
Or you follow some modern commentators explanation.

210
00:18:11,000 --> 00:18:13,000
And it turns out to be no better.

211
00:18:13,000 --> 00:18:16,000
It turns out to be actually usually worse, I would say.

212
00:18:16,000 --> 00:18:21,000
I would argue worse in the sense that whether you agree with it or not,

213
00:18:21,000 --> 00:18:31,000
far less rigorous, far less objective, far less comprehensive, far less in-depth.

214
00:18:31,000 --> 00:18:35,000
And the reason you might get just against incredible work,

215
00:18:35,000 --> 00:18:38,000
if you disagree with it or agree with it or disagree with it,

216
00:18:38,000 --> 00:18:43,000
it's amazing that someone actually wrote it because it's huge and so in-depth.

217
00:18:43,000 --> 00:18:47,000
And it's got so much in it.

218
00:18:47,000 --> 00:18:51,000
Remember, I was giving a talk when I was in Sri Lanka last time.

219
00:18:51,000 --> 00:18:56,000
And the man who was translating for me refused to translate,

220
00:18:56,000 --> 00:18:59,000
because I was quoting the sutimanga and he said,

221
00:18:59,000 --> 00:19:02,000
oh, we don't follow the sutimanga here.

222
00:19:02,000 --> 00:19:04,000
And so, that was the first for me.

223
00:19:04,000 --> 00:19:08,000
I was giving a talk and that translator wouldn't translate.

224
00:19:08,000 --> 00:19:14,000
I think I was saying something mean or evil.

225
00:19:14,000 --> 00:19:17,000
So, it's a contentious. We see them at good tradition,

226
00:19:17,000 --> 00:19:20,000
it's fairly contentious to some people.

227
00:19:20,000 --> 00:19:24,000
Most of the Orthodox, most terrible societies.

228
00:19:24,000 --> 00:19:29,000
For the most part, they follow the sutimanga.

229
00:19:29,000 --> 00:19:33,000
In Thailand, it forms the latter part of one's poly studies.

230
00:19:33,000 --> 00:19:38,000
So, any monk who studies higher level poly will have to translate.

231
00:19:38,000 --> 00:19:43,000
The sutimanga into Thai and then translate Thai passages,

232
00:19:43,000 --> 00:19:46,000
Thai translations of the sutimanga back into the poly.

233
00:19:46,000 --> 00:19:49,000
So, they have to really memorize.

234
00:19:49,000 --> 00:19:54,000
Many of them basically I think will memorize the sutimanga in poly and English,

235
00:19:54,000 --> 00:19:57,000
which is impressive in itself.

236
00:19:57,000 --> 00:20:08,000
Maybe not memorize, but come close.

237
00:20:08,000 --> 00:20:15,000
Any requirements for taking a meditation course at our center?

238
00:20:15,000 --> 00:20:18,000
Yeah, probably.

239
00:20:18,000 --> 00:20:24,000
I mean, if you're coming alone in your under 16,

240
00:20:24,000 --> 00:20:27,000
I think we'd have to have a talk about that.

241
00:20:27,000 --> 00:20:28,000
I don't know.

242
00:20:28,000 --> 00:20:35,000
I would imagine having people under 16 coming and doing courses is problematic,

243
00:20:35,000 --> 00:20:37,000
unless we talk to their parents.

244
00:20:37,000 --> 00:20:39,000
I think 16 is probably.

245
00:20:39,000 --> 00:20:43,000
And as for upper age limit,

246
00:20:43,000 --> 00:20:45,000
it's more to do with your health.

247
00:20:45,000 --> 00:20:50,000
If you're able to do walking and sitting meditation for hours a day,

248
00:20:50,000 --> 00:21:01,000
it's fine. You know, you can sit on a chair if you have to.

249
00:21:01,000 --> 00:21:02,000
Oh, I don't know.

250
00:21:02,000 --> 00:21:08,000
Robin, I haven't really done about the future.

251
00:21:08,000 --> 00:21:12,000
I mean, there are some other things I'm looking at, actually.

252
00:21:12,000 --> 00:21:29,000
Actually, I'm good to know Kaya's a bit volume, and I've started maybe collecting some good sutas from it.

253
00:21:29,000 --> 00:21:32,000
A new set of robes for my trip to Asia.

254
00:21:32,000 --> 00:21:35,000
You know, the funny thing about these robes, they're fairly new, right?

255
00:21:35,000 --> 00:21:38,000
But they came with holes in them.

256
00:21:38,000 --> 00:21:41,000
Like, I think they were mouse eaten or they were ripped or something.

257
00:21:41,000 --> 00:21:44,000
Because in my back, there's actually little holes.

258
00:21:44,000 --> 00:21:48,000
I probably should sew up.

259
00:21:48,000 --> 00:21:51,000
Not that that's here and there.

260
00:21:51,000 --> 00:21:53,000
But, no, these are fine in Asia.

261
00:21:53,000 --> 00:21:56,000
It'll be a little hot, but, you know, this upper robe,

262
00:21:56,000 --> 00:21:57,000
I don't wear a lot of the time.

263
00:21:57,000 --> 00:22:02,000
When I'm in my room, in my room, I rarely wear this.

264
00:22:02,000 --> 00:22:06,000
Sometimes I just go bare chest or sometimes I have a basic,

265
00:22:06,000 --> 00:22:08,000
what's called an onset.

266
00:22:08,000 --> 00:22:15,000
It's just a very thin shoulder robe, shoulder cloth.

267
00:22:22,000 --> 00:22:23,000
But these aren't that hot.

268
00:22:23,000 --> 00:22:28,000
It's just cotton, so it's actually not that hot.

269
00:22:28,000 --> 00:22:31,000
But it's going to be hot in June.

270
00:22:31,000 --> 00:22:35,000
So I'll try to go up and live on the mountain.

271
00:22:35,000 --> 00:22:40,000
I'm going to try to go up on a mountain in Sri Lanka.

272
00:22:40,000 --> 00:22:42,000
I'm arguing with Sangha about that.

273
00:22:42,000 --> 00:22:46,000
Sangha wants me to stay in Colombo for two weeks.

274
00:22:46,000 --> 00:22:49,000
I'm trying to push to take a week out.

275
00:22:49,000 --> 00:22:52,000
He says, a week isn't enough.

276
00:22:52,000 --> 00:22:53,000
I know it isn't.

277
00:22:53,000 --> 00:22:54,000
I mean, a month isn't enough.

278
00:22:54,000 --> 00:22:58,000
I could spend months since Sri Lanka giving talks every day,

279
00:22:58,000 --> 00:23:03,000
but not really keen on it.

280
00:23:03,000 --> 00:23:13,000
And then in Thailand, maybe go back up on the mountain as well.

281
00:23:13,000 --> 00:23:15,000
Probably not staying in John Kong very long,

282
00:23:15,000 --> 00:23:18,000
because it's probably pretty hot in June.

283
00:23:18,000 --> 00:23:20,000
Unless it started raining, it should be OK.

284
00:23:25,000 --> 00:23:28,000
Do we only have a few months left?

285
00:23:28,000 --> 00:23:32,000
Have we really gone through most of the year?

286
00:23:32,000 --> 00:23:37,000
Have we started, when do we start August or something?

287
00:23:37,000 --> 00:23:41,000
July, maybe?

288
00:23:41,000 --> 00:23:44,000
Time flies.

289
00:23:44,000 --> 00:23:48,000
Tempest, tempos, forget.

290
00:23:48,000 --> 00:23:51,000
Time fleas.

291
00:23:51,000 --> 00:24:00,000
OK.

292
00:24:00,000 --> 00:24:03,000
Good night, everyone.

293
00:24:03,000 --> 00:24:25,000
See you all next time.

