1
00:00:00,000 --> 00:00:08,640
When overcoming a fear, how much of a part do fearful thoughts play in making the fear at reality and

2
00:00:09,640 --> 00:00:16,220
Creating that reality that started in my mind how to prevent this if we can't control our thoughts

3
00:00:22,240 --> 00:00:26,680
How much is the fear of reality? I guess the actual emotion is

4
00:00:26,680 --> 00:00:31,560
What are your experiences or?

5
00:00:33,720 --> 00:00:35,720
We don't have to control our thoughts

6
00:00:36,800 --> 00:00:39,240
Just know there about that word. We're not

7
00:00:40,560 --> 00:00:45,480
Ever trying to control our thoughts, but we can affect affect our thoughts

8
00:00:46,720 --> 00:00:52,120
Just because we can't control them doesn't mean one moment of mindfulness won't

9
00:00:52,120 --> 00:00:57,480
affect the next thought that comes so

10
00:01:01,840 --> 00:01:04,800
For sure that it's the thinking that's the problem it's the

11
00:01:05,760 --> 00:01:09,160
conceiving that creates the fear because ultimate reality is not

12
00:01:11,480 --> 00:01:13,480
Will not give rise to fear

13
00:01:13,480 --> 00:01:19,560
The ultimate reality will give rise to

14
00:01:21,200 --> 00:01:23,200
Dispassion

15
00:01:23,200 --> 00:01:27,720
But once you get right once you conceive something in the object, then it will give rise to fear

16
00:01:27,920 --> 00:01:33,920
So when you are clear in the ultimate reality clear of the ultimate reality they're about

17
00:01:34,400 --> 00:01:36,400
the ultimate reality of the experience

18
00:01:37,040 --> 00:01:38,560
Then it won't give

19
00:01:38,560 --> 00:01:44,400
Then the fear won't be able to rise the thoughts about it won't be able to arise that would lead to the fear

20
00:01:49,320 --> 00:01:53,040
So what can you do try to see the ultimate reality of the

21
00:01:53,560 --> 00:01:57,680
experience that's seeing hearing smelling tasting feeling or thinking

22
00:01:58,320 --> 00:02:00,320
Try and see it for what it is

23
00:02:00,320 --> 00:02:02,320
I

24
00:02:06,360 --> 00:02:13,280
Just thought if the thoughts I mean, it's really just thoughts of whatever you're thinking comes down to it

25
00:02:14,960 --> 00:02:19,000
But if you're feeling if you're feeling the fear, I would definitely you know recognize that

26
00:02:20,200 --> 00:02:22,200
note that fear and

27
00:02:22,720 --> 00:02:24,720
Then

28
00:02:24,720 --> 00:02:27,960
Yeah, I think it's just really thoughts when it comes down to it

29
00:02:27,960 --> 00:02:30,320
So it really is just thinking

30
00:02:38,080 --> 00:02:40,080
There's an interesting one I

31
00:02:40,800 --> 00:02:43,680
I'm still not understanding the point of meditation

32
00:02:43,680 --> 00:02:58,560
Am I supposed to remove all the motion and judgments from the one thing I am thinking of?

33
00:02:58,560 --> 00:03:06,080
But that's not the point right that's the activity the point of meditation is to be free from suffering

34
00:03:06,080 --> 00:03:18,520
Is that almost like the bare awareness that people talk about having bare awareness I

35
00:03:19,120 --> 00:03:25,440
Mean it sounds like it sounds like there's an understanding here of what meditation is but that's what meditation is

36
00:03:25,440 --> 00:03:31,120
It's not the point the point is not just to get rid of emotions and judgments the point is to be free from suffering

37
00:03:31,120 --> 00:03:37,680
Which comes from being non-judgmental just judgments lead to clinging and craving and partiality and

38
00:03:38,680 --> 00:03:40,680
disappointment and

39
00:03:42,440 --> 00:03:44,440
Suffering

40
00:03:48,520 --> 00:03:52,320
So like when you when you have a pleasant feeling and

41
00:03:53,560 --> 00:03:55,560
You like it

42
00:03:55,560 --> 00:03:58,560
Those are two different things the pleasant feeling isn't a problem

43
00:03:58,560 --> 00:04:02,640
There's nothing wrong with feeling and pleasant feeling and pleasure of any sort

44
00:04:04,200 --> 00:04:10,880
Central pleasure is not a negative state. It's not a bad thing. That's something we should feel ashamed of

45
00:04:11,960 --> 00:04:18,600
But the desire for it the attachment to it the liking of it. That's used to it. That's worse than use is that's dangerous

46
00:04:18,600 --> 00:04:30,760
That's what creates clinging. That's what creates need and striving and eventual disappointment. That's what sets you up for

47
00:04:33,680 --> 00:04:39,600
Suffering it's what creates anger. That's why we get angry and frustrated and

48
00:04:42,240 --> 00:04:44,240
Discontent bored

49
00:04:44,240 --> 00:04:47,960
All of this comes from desire

50
00:04:49,360 --> 00:04:58,760
When you feel a painful feeling there's also nothing wrong with that either there's nothing bad about feeling and pain for feeling there's nothing

51
00:04:59,400 --> 00:05:01,400
Wrong with you when you feel pain

52
00:05:02,680 --> 00:05:05,040
It's wrong is the aversion towards it

53
00:05:06,480 --> 00:05:10,960
The aversion towards the pain is something very different from the pain the pain is neutral

54
00:05:10,960 --> 00:05:14,340
It's it's ethically very ethically

55
00:05:16,240 --> 00:05:18,240
inert

56
00:05:23,920 --> 00:05:29,240
And it doesn't it is not even really suffering when you are pain you can be still very very happy or

57
00:05:30,240 --> 00:05:34,520
You can move at peace even when you have terrible pain

58
00:05:34,520 --> 00:05:47,000
So yeah, the key is to see the experience as it is when it's the greatest pleasure or the most terrible pain. It's still just an experience

59
00:05:49,400 --> 00:05:53,240
So the point of meditation is actually quite a very powerful

60
00:05:54,080 --> 00:05:59,440
result and wonderful result is that even with the most terrible pain you can

61
00:05:59,440 --> 00:06:08,160
And even with the greatest pleasure you can experience it without becoming addicted to it

62
00:06:10,920 --> 00:06:13,920
Because addiction is very dangerous thing

63
00:06:14,640 --> 00:06:24,080
It's a very wonderful result of meditation that it's able to free you from that where you can experience the pleasure without any attachment to it

64
00:06:24,080 --> 00:06:28,080
That's incredible

65
00:06:31,760 --> 00:06:33,760
Very good point

66
00:06:33,880 --> 00:06:35,880
Good reason to practice

67
00:06:36,920 --> 00:06:40,960
I always joke around if someone asked me, you know, what's the point?

68
00:06:41,800 --> 00:06:44,120
I say that's to see how crazy you actually are

69
00:06:45,480 --> 00:06:50,200
Because once you start meditating you see how crazy your mind actually is

70
00:06:51,120 --> 00:06:53,120
Why we

71
00:06:53,120 --> 00:06:55,120
Do things we'll be there

