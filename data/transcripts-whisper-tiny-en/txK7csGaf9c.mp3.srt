1
00:00:00,000 --> 00:00:13,000
I have been practicing this for all of a year. My family comes from a Christian background and is very unsure of my practice to each other and to intentionally interrupt my meditation suggestion.

2
00:00:15,000 --> 00:00:16,000
Oh, I don't.

3
00:00:23,000 --> 00:00:26,000
Intentionally interrupt my meditation. That's hard for it, isn't it?

4
00:00:26,000 --> 00:00:28,000
Yeah.

5
00:00:29,000 --> 00:00:34,000
Well, my grandmother, my Jewish grandmother, she did that and I was meditating. She got really freaked out.

6
00:00:42,000 --> 00:00:44,000
Well, do your meditation when they're not looking?

7
00:00:44,000 --> 00:00:51,000
Are you meditating at night? If I were Kate you stole from the room?

8
00:00:52,000 --> 00:00:55,000
I'm very keen to stop the room. What are you doing in there?

9
00:00:55,000 --> 00:00:57,000
Yeah.

10
00:01:10,000 --> 00:01:17,000
I've talked to them about it. It's not, the kingdom of God is within. I don't know.

11
00:01:18,000 --> 00:01:22,000
Is there a room for meditation in Buddhism? Tell them it makes you a better person.

12
00:01:22,000 --> 00:01:29,000
No. Actually, there are some Christians. They practice meditation.

13
00:01:30,000 --> 00:01:38,000
So I'm in Christians like Christian monks, even practicing meditation.

14
00:01:39,000 --> 00:01:46,000
As far as I heard about one guy, like that, he is practicing meditation.

15
00:01:46,000 --> 00:01:53,000
And it can go along with the chance to serve.

16
00:01:54,000 --> 00:02:01,000
The other thing is you can meditate on the interruptions, hearing, hearing.

17
00:02:02,000 --> 00:02:05,000
They poke you feeling, feeling if you're angry, angry, angry.

18
00:02:06,000 --> 00:02:11,000
Works wonders. When I first started practicing, I was really hardcore in a very bad way.

19
00:02:11,000 --> 00:02:18,000
And so when they started yelling at me about my being a zombie, I would just close my eyes and start saying rising.

20
00:02:19,000 --> 00:02:22,000
Right there in the middle of an argument.

21
00:02:23,000 --> 00:02:27,000
Or just tell them, you're a negative now. They've wronged you, right?

22
00:02:28,000 --> 00:02:31,000
That's it. Sounds constructive. Look as well.

23
00:02:32,000 --> 00:02:35,000
That must have taken a lot of time to think of.

24
00:02:35,000 --> 00:02:41,000
Introduce them to reach our dull pain.

25
00:02:42,000 --> 00:02:45,000
Okay. I fixed that for you.

26
00:02:46,000 --> 00:02:48,000
That's what you did.

27
00:02:49,000 --> 00:02:51,000
No, I don't mean paper.

28
00:02:52,000 --> 00:02:56,000
Well, tell them they are wronged.

29
00:02:56,000 --> 00:03:05,000
Oh, right.

30
00:03:06,000 --> 00:03:07,000
When we talked about Christianity, we got this.

31
00:03:08,000 --> 00:03:11,000
When we were talking about Christianity before, how do you deal with Christians?

32
00:03:12,000 --> 00:03:14,000
Well, it's certainly not by this. It's not by conflicting.

33
00:03:15,000 --> 00:03:17,000
You have to get on their side and bring them across.

34
00:03:21,000 --> 00:03:23,000
Convert them.

35
00:03:23,000 --> 00:03:26,000
That's it.

36
00:03:27,000 --> 00:03:33,000
I've met many Christians that could teach Buddhist many things.

37
00:03:34,000 --> 00:03:48,000
And honestly, the thing to sit here and be derogatory against Christians is really, you know, you're being derogatory against those that are maybe a bad example of Christianity.

38
00:03:48,000 --> 00:03:59,000
And those that are really sincere Christians, they are Buddhas just as much as real Buddhas have Christ consciousness.

39
00:04:00,000 --> 00:04:11,000
You know, I think this is a part of the problem because what we look at in terms of Christians being, you know, off kilter and not really enlightened.

40
00:04:11,000 --> 00:04:17,000
Well, if you look at the world of Buddhism, we're actually no better.

41
00:04:18,000 --> 00:04:37,000
In fact, we might be a little worse with our superstitions and our erroneous beliefs and our dogmatism and the fact that people that have never even sat down on a meditation cushion will want to explain to other people what it means to be an enlightened Buddha.

42
00:04:37,000 --> 00:04:40,000
You know, we do the same thing.

43
00:04:41,000 --> 00:04:43,000
We're no better.

44
00:04:46,000 --> 00:04:48,000
That's a little harsh, I think.

45
00:04:50,000 --> 00:04:53,000
Yeah, it's not harsh. It's just truth.

46
00:04:54,000 --> 00:05:00,000
You know, as broad Buddhists, something better than Christianity as broad Christians.

47
00:05:02,000 --> 00:05:05,000
I'm sorry, didn't understand the question would say it again.

48
00:05:05,000 --> 00:05:10,000
You don't think that Buddhism has brought Buddhist something better than Christianity as broad Christians.

49
00:05:12,000 --> 00:05:15,000
No, no, it's so ever.

50
00:05:16,000 --> 00:05:25,000
You know, not really, not anything better, not any outer sense of it.

51
00:05:25,000 --> 00:05:47,000
Buddhism, when I look at the outer manifestations of Buddhism in Christianity, most actually Christianity seems a little bit more modern, a little bit more compassionate, a little bit more willing to actually help beings and suffering.

52
00:05:47,000 --> 00:05:54,000
And what I see from Buddhism is a complacency of wishy-washiness.

53
00:05:55,000 --> 00:06:02,000
So, you know, let's be compassionate and mindful, but I see a lot of people just being self-absorbed and talking about their own meditation.

54
00:06:03,000 --> 00:06:06,000
And so, no, I don't. I see a lot of hypocrisy.

55
00:06:07,000 --> 00:06:15,000
But then I've been in Buddhist for 30 years. I'm no longer enamored and allured with this idea that Buddhism is better than anything else.

56
00:06:15,000 --> 00:06:30,000
I realize that it's not the case, each individual, you know, just because somebody professes that they are a Buddhist doesn't mean that they are anything like Buddha, and there we will agree, you know.

57
00:06:31,000 --> 00:06:34,000
They tend to think that Buddhism is better than Christianity, for example.

58
00:06:35,000 --> 00:06:36,000
Right.

59
00:06:36,000 --> 00:07:00,000
So, I think on the outer manifestations of it, none of the religions are perfect because, you know, it's trying to live up to a message, and you know, the real virtuous message of these religions, I don't think anybody would want to criticize.

60
00:07:00,000 --> 00:07:08,000
But, you know, it's like the idea of how do you throw away the bathwater and not throw away the baby.

61
00:07:08,000 --> 00:07:33,000
So, last time I said something about religions, I don't think religion is a good thing because there are these people calling them some Christians and Buddhists and, you know, some Christians are better than some Buddhists and so on and so on.

62
00:07:33,000 --> 00:07:43,000
I never came across any Christian, I don't know many Buddhists either.

63
00:07:44,000 --> 00:07:54,000
So, I don't know if somebody called himself a Buddhist and he's not, we're calling him some of the Buddhists, then he preferred.

64
00:07:54,000 --> 00:08:03,000
So, now he will complete everyone.

65
00:08:04,000 --> 00:08:09,000
Anyway, the question wasn't about all about looking down on any religion or holding religions above each other.

66
00:08:09,000 --> 00:08:25,000
It was about how you deal with people who, you know, whatever religion they may be, happened to be Christians in this case, who interrupt your meditation. So, anyway, I think that was kind of answered.

