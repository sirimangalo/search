1
00:00:00,000 --> 00:00:29,000
Good evening everyone. Nice to see you all here again and our meditators here locally.

2
00:00:29,000 --> 00:00:55,000
It's Friday night, Friday night I think is still the time when people go out and see happiness in their various ways.

3
00:00:55,000 --> 00:01:19,000
Sometimes meditators get a little bit discouraged thinking about all the fun they could be having out in the world.

4
00:01:19,000 --> 00:01:26,000
All the happiness that they're missing out on. So much happiness out there, no?

5
00:01:26,000 --> 00:01:42,000
Easy. It's like low hanging fruit dangling, just waiting to be plucked.

6
00:01:42,000 --> 00:01:48,000
What if you think about it if you're discerning and perceptive?

7
00:01:48,000 --> 00:01:57,000
It really isn't a lot of happiness out there to be had.

8
00:01:57,000 --> 00:02:08,000
All the happiness we might find is bound up by craving and clinging in the diction.

9
00:02:08,000 --> 00:02:19,000
We seek and we seek and we earn for happiness and we get little bits of it.

10
00:02:19,000 --> 00:02:33,000
And then it's gone and we're left with clinging and craving and yearning.

11
00:02:33,000 --> 00:02:42,000
So in Buddhism we are much concerned with happiness as well. It's not that happiness isn't important.

12
00:02:42,000 --> 00:02:49,000
That we're not interested in low hanging fruit and interested in the easy form of happiness.

13
00:02:49,000 --> 00:02:57,000
Because unfortunately the easy forms of happiness are not truly satisfying.

14
00:02:57,000 --> 00:03:07,000
Here's the ones that are all caught up with clinging and craving, like bait on a hook.

15
00:03:07,000 --> 00:03:18,000
They get you more caught up and more entangled, more stressed and they need to more suffering.

16
00:03:18,000 --> 00:03:23,000
But there are other kinds of happiness and this is what we feel great about on this Friday night.

17
00:03:23,000 --> 00:03:34,000
We come together to meditate, to consider the dumb by the Buddhist teaching.

18
00:03:34,000 --> 00:03:44,000
What kinds of happiness do we have in the Buddhist teaching?

19
00:03:44,000 --> 00:03:53,000
Well the first happiness we have is this gathering, our ability to be here.

20
00:03:53,000 --> 00:04:02,000
We have the Buddhist center, we have here a meditation center.

21
00:04:02,000 --> 00:04:13,000
We have the happiness of our community, of our place, of our physical experience that allows us to get away from the stresses

22
00:04:13,000 --> 00:04:21,000
and the meaninglessness of the now existence and come to a spiritual place,

23
00:04:21,000 --> 00:04:36,000
come to a gathering of spiritual intent where we all have the same goal and the same direction in our minds.

24
00:04:36,000 --> 00:04:50,000
Where there's wholesomeness, where there's goodness, where there's reassurance on our path.

25
00:04:50,000 --> 00:04:59,000
So even before we start to practice, even before we do anything wholesome, we've got this greatness of community,

26
00:04:59,000 --> 00:05:09,000
greatness of our ability to gather together in this way, just to be in this place,

27
00:05:09,000 --> 00:05:24,000
not physically the space, but to be in an environment that is encouraging of a spiritual practice.

28
00:05:24,000 --> 00:05:33,000
But there's more with all of the practice that we do. The second great happiness that we have is from our morality, from our ethics.

29
00:05:33,000 --> 00:05:37,000
We have this reassurance in our minds that we are good people.

30
00:05:37,000 --> 00:05:42,000
It's a question, am I a good person?

31
00:05:42,000 --> 00:05:51,000
Is there anything good about me, anything great, anything that wives people will esteem and appreciate and praise?

32
00:05:51,000 --> 00:05:54,000
Well, now there is.

33
00:05:54,000 --> 00:06:07,000
Here and now we're in a situation, we're in a position, we're on a path that leads towards greatness.

34
00:06:07,000 --> 00:06:19,000
That is made up of greatness, great things all along the way, starting with our ethics.

35
00:06:19,000 --> 00:06:34,000
We should be very happy, and we are in these situations quite content in our spiritual state of wholesomeness of mind,

36
00:06:34,000 --> 00:06:40,000
that we're not inclined towards greed or anger or delusion.

37
00:06:40,000 --> 00:06:53,000
That we're actually struggling to keep our minds free from defilement.

38
00:06:53,000 --> 00:06:58,000
You can be happy that at this moment we are good people.

39
00:06:58,000 --> 00:07:01,000
It's goodness, of course, is momentary.

40
00:07:01,000 --> 00:07:05,000
People as well are changing for a moment to moment.

41
00:07:05,000 --> 00:07:13,000
You hope that you will be happy to be happy to be happy to be happy to be happy to be happy to be happy to be happy.

42
00:07:13,000 --> 00:07:21,000
Whoever was negligent in the past, but later on becomes vigilant, becomes mindful.

43
00:07:21,000 --> 00:07:37,000
So, among low kung-pah-sei-di, abhomu-tovatandimah, such a person lights up the world like the moon coming out from behind the cloud.

44
00:07:37,000 --> 00:07:40,000
I don't have to worry about what we were in the past.

45
00:07:40,000 --> 00:07:42,000
We've come here with all sorts of baggage.

46
00:07:42,000 --> 00:07:45,000
We'll leave the baggage at the door.

47
00:07:45,000 --> 00:07:47,000
Now we are on the right path.

48
00:07:47,000 --> 00:07:51,000
For this time we are cultivating wholesomeness and we can feel great about that.

49
00:07:51,000 --> 00:07:57,000
We can feel good about ourselves.

50
00:07:57,000 --> 00:08:05,000
The third happiness we have is when we start to become mindful, focused in the present moment.

51
00:08:05,000 --> 00:08:07,000
We tap into reality again.

52
00:08:07,000 --> 00:08:16,000
We sink our roots into the earth, into the soil of understanding, into the soil of mindfulness.

53
00:08:16,000 --> 00:08:22,000
And we are nourished by the reality of it.

54
00:08:22,000 --> 00:08:33,000
We leave behind our concepts and our beliefs and views and opinions, personalities even.

55
00:08:33,000 --> 00:08:38,000
We leave it all behind for here and now.

56
00:08:38,000 --> 00:08:50,000
Being aware of the body, feelings, the mind, the emotions and the senses and so on.

57
00:08:50,000 --> 00:08:54,000
And this is happiness for us.

58
00:08:54,000 --> 00:08:56,000
This is great satisfaction.

59
00:08:56,000 --> 00:09:00,000
There's great satisfaction in being a present.

60
00:09:00,000 --> 00:09:05,000
No matter how much pain or stress there might be in the present moment,

61
00:09:05,000 --> 00:09:17,000
once you're truly present it all loses its edge.

62
00:09:17,000 --> 00:09:31,000
It all fades into just being experienced without any of the positive or negative qualities.

63
00:09:31,000 --> 00:09:36,000
It's just is its experience and we become quite peaceful.

64
00:09:36,000 --> 00:09:50,000
This is the third happiness that we get from staying in on a Friday night.

65
00:09:50,000 --> 00:09:55,000
The fourth we get is insight, wisdom.

66
00:09:55,000 --> 00:10:00,000
This is the fourth happiness that comes from being here today.

67
00:10:00,000 --> 00:10:05,000
So we start to think more about good teachings and listening to the teacher.

68
00:10:05,000 --> 00:10:09,000
We get some good information and tips and direction.

69
00:10:09,000 --> 00:10:15,000
We're able to direct our minds in the right direction and start to understand ourselves.

70
00:10:15,000 --> 00:10:19,000
We start to see the mind as impermanent and unsatisfying and controllable.

71
00:10:19,000 --> 00:10:24,000
We see the body as well as unstable and unpredictable.

72
00:10:24,000 --> 00:10:31,000
We start to see that everything that we cling to is not worth clinging to.

73
00:10:31,000 --> 00:10:35,000
And we start to let go.

74
00:10:35,000 --> 00:10:45,000
And again, true wisdom and insight into reality.

75
00:10:45,000 --> 00:10:56,000
And finally, the last thing we get is freedom.

76
00:10:56,000 --> 00:11:05,000
The greatest happiness of all is freedom.

77
00:11:05,000 --> 00:11:10,000
We come here to practice and as we gain insight,

78
00:11:10,000 --> 00:11:21,000
even just sitting here listening to the dhamma, it's quite reasonable to expect freedom.

79
00:11:21,000 --> 00:11:27,000
We start to free ourselves from first from our wrong views

80
00:11:27,000 --> 00:11:31,000
and then from our wrong thoughts and then from our wrong emotions.

81
00:11:31,000 --> 00:11:35,000
All the wrong wrong wrong the things that cause us suffering and stress

82
00:11:35,000 --> 00:11:38,000
or that cause us to hurt others.

83
00:11:38,000 --> 00:11:44,000
All that is wrong about us fades away and what is right comes up.

84
00:11:44,000 --> 00:11:48,000
We enter on the right path, we enter into right side.

85
00:11:48,000 --> 00:11:57,000
We cultivate right view and we become free from all the entanglement,

86
00:11:57,000 --> 00:12:09,000
to all the bonds of suffering that come from defoundment.

87
00:12:09,000 --> 00:12:13,000
And so there's much happiness to be had.

88
00:12:13,000 --> 00:12:15,000
The greatest happiness.

89
00:12:15,000 --> 00:12:18,000
Two very different kinds of happiness.

90
00:12:18,000 --> 00:12:27,000
One is very easy, low hanging fruit, easy for humans anyway.

91
00:12:27,000 --> 00:12:33,000
Easy for, I guess, most beings to just put a like in this to a leper.

92
00:12:33,000 --> 00:12:38,000
A leper whose fingers or limbs are falling off, they're getting scabs

93
00:12:38,000 --> 00:12:47,000
and they take the ends of their limbs and they cauterize them with flame.

94
00:12:47,000 --> 00:12:52,000
Apparently that brings some relief.

95
00:12:52,000 --> 00:12:57,000
That's the sort of happiness, the sort of sensual happiness.

96
00:12:57,000 --> 00:13:02,000
Trying to find relief like the leper.

97
00:13:02,000 --> 00:13:10,000
It's the desperate sort of happiness.

98
00:13:10,000 --> 00:13:17,000
Craving and clinging, one of my meditators pointed out something quite funny once,

99
00:13:17,000 --> 00:13:19,000
and it summed it up quite nicely.

100
00:13:19,000 --> 00:13:27,000
He said he was sweeping in his kuti one day and there were two cockroaches.

101
00:13:27,000 --> 00:13:29,000
Not wanting to hurt them.

102
00:13:29,000 --> 00:13:35,000
He started sweeping them out and they started running around as cockroaches do.

103
00:13:35,000 --> 00:13:37,000
And he got them sort of together near the door.

104
00:13:37,000 --> 00:13:40,000
And as soon as they got really, really close together,

105
00:13:40,000 --> 00:13:44,000
they jumped one of them jumped on the other and they started mating.

106
00:13:44,000 --> 00:13:54,000
And he said that about sums up the nature of craving.

107
00:13:54,000 --> 00:13:58,000
Life is short.

108
00:13:58,000 --> 00:14:00,000
Let's see gratification while we can.

109
00:14:00,000 --> 00:14:04,000
I mean, just the animal instinct, but it's in us all.

110
00:14:04,000 --> 00:14:07,000
We're suffering so much that we grasp at anything.

111
00:14:07,000 --> 00:14:13,000
We're grasping at straws or grasping at nothing.

112
00:14:13,000 --> 00:14:17,000
Grasping at anything.

113
00:14:17,000 --> 00:14:19,000
But the other happiness is more complicated.

114
00:14:19,000 --> 00:14:23,000
More not complicated, but more subtle.

115
00:14:23,000 --> 00:14:26,000
It takes, it's more delicate.

116
00:14:26,000 --> 00:14:30,000
It takes more delicate approach.

117
00:14:30,000 --> 00:14:37,000
It can't just be grasped at, it has to be cultivated carefully.

118
00:14:37,000 --> 00:14:41,000
That's everything to do with care, patience,

119
00:14:41,000 --> 00:14:48,000
sophistication, let's put a delicacy or a finement.

120
00:14:48,000 --> 00:14:52,000
And so it's subtle and profound.

121
00:14:52,000 --> 00:14:58,000
It's lasting and it's stable and it's leading only to greater and greater happiness.

122
00:14:58,000 --> 00:15:03,000
So the other thing is the easy happiness leads to less and less happiness.

123
00:15:03,000 --> 00:15:05,000
That's how the brain works.

124
00:15:05,000 --> 00:15:08,000
It's all caught up in the brain and chemicals.

125
00:15:08,000 --> 00:15:09,000
Tell some sorrow works.

126
00:15:09,000 --> 00:15:12,000
You might say, the more you get what you want,

127
00:15:12,000 --> 00:15:17,000
the more you want it and the less satisfied you are with what you get.

128
00:15:17,000 --> 00:15:22,000
It's the sort of the law of diminishing returns of karma

129
00:15:22,000 --> 00:15:29,000
or of calm, of sensuality.

130
00:15:29,000 --> 00:15:32,000
Whereas the happiness of the dhamma, it may be very little happiness at first

131
00:15:32,000 --> 00:15:36,000
because there's so much confusion in the mind,

132
00:15:36,000 --> 00:15:41,000
so much defilement that sitting still is not very pleasant.

133
00:15:41,000 --> 00:15:44,000
But the happiness increases over time.

134
00:15:44,000 --> 00:15:46,000
The more you practice, the more happiness,

135
00:15:46,000 --> 00:15:51,000
the more peace, the more freedom that comes.

136
00:15:51,000 --> 00:15:55,000
Two very different types of happiness.

137
00:15:55,000 --> 00:15:59,000
So congratulations and appreciation to everyone

138
00:15:59,000 --> 00:16:09,000
for choosing this way, which we consider to be far, far superior.

139
00:16:09,000 --> 00:16:11,000
And thank you for coming out tonight.

140
00:16:11,000 --> 00:16:14,000
There's the dhamma that's the dhamma for this evening,

141
00:16:14,000 --> 00:16:21,000
wishing you all a good practice.

142
00:16:21,000 --> 00:16:44,000
Here we got some questions here.

143
00:16:44,000 --> 00:16:48,000
If there are no thoughts between breaths in sitting meditation,

144
00:16:48,000 --> 00:16:54,000
I've started noting the calm and empty states with just noting calm and empty empty.

145
00:16:54,000 --> 00:16:56,000
After some time in meditation doing this,

146
00:16:56,000 --> 00:16:59,000
the thoughts seem to come only as shapes and colors later.

147
00:16:59,000 --> 00:17:02,000
It seems that the thoughts disappear completely.

148
00:17:02,000 --> 00:17:10,000
So they specially notice these states are just gone with calm and empty empty.

149
00:17:10,000 --> 00:17:18,000
Well, in between sitting, I mean, you might do rising, falling, and then not sitting.

150
00:17:18,000 --> 00:17:21,000
We start to make it more complex through the, as you go through the course,

151
00:17:21,000 --> 00:17:24,000
while that's sitting, so rising, falling, sitting.

152
00:17:24,000 --> 00:17:27,000
But, you know, it sounds like what you're doing is fine,

153
00:17:27,000 --> 00:17:31,000
except when you start to see things, you should say seeing, seeing.

154
00:17:31,000 --> 00:17:34,000
When you feel completely calm or quiet,

155
00:17:34,000 --> 00:17:37,000
you should not quiet, quiet when there's, when the thoughts disappear,

156
00:17:37,000 --> 00:17:42,000
you can actually just say disappearing or knowing knowing that they're disappearing.

157
00:17:42,000 --> 00:17:44,000
If you like it, you would say liking, like,

158
00:17:44,000 --> 00:17:47,000
but it sounds like you're doing fine.

159
00:17:47,000 --> 00:17:53,000
Very too much about what you're doing.

160
00:17:53,000 --> 00:17:56,000
Maybe good, if you came into the meditation course,

161
00:17:56,000 --> 00:18:01,000
it might sort of make things clear.

162
00:18:01,000 --> 00:18:03,000
Someone's re-asking a question.

163
00:18:03,000 --> 00:18:08,000
I must have been mean and not answered it the first time.

164
00:18:08,000 --> 00:18:12,000
I did the basic meditation course and another two courses.

165
00:18:12,000 --> 00:18:14,000
I wonder with who?

166
00:18:14,000 --> 00:18:16,000
With me?

167
00:18:16,000 --> 00:18:21,000
Someone using a pseudonym.

168
00:18:21,000 --> 00:18:24,000
I have a teacher I do reports to, okay, so it's not me.

169
00:18:24,000 --> 00:18:28,000
I would say 90% of the day is in the year I don't meditate.

170
00:18:28,000 --> 00:18:32,000
90% of the day is in the year that's not very good.

171
00:18:32,000 --> 00:18:39,000
90% of the years, 90% of the days in the year I don't meditate.

172
00:18:39,000 --> 00:18:41,000
That's a lot.

173
00:18:41,000 --> 00:18:43,000
I tried long all day meditations.

174
00:18:43,000 --> 00:18:45,000
I tried doing very short meditations every five minutes,

175
00:18:45,000 --> 00:18:48,000
and I never persevere more than a few days.

176
00:18:48,000 --> 00:18:49,000
I'm really frustrated with this.

177
00:18:49,000 --> 00:18:54,000
Can't find a solution.

178
00:18:54,000 --> 00:18:58,000
Yeah, that's interesting.

179
00:18:58,000 --> 00:19:01,000
Maybe you should start meditating on your daily life

180
00:19:01,000 --> 00:19:05,000
to sort of things that you're doing that may be not conducive

181
00:19:05,000 --> 00:19:08,000
for meditation practice.

182
00:19:08,000 --> 00:19:11,000
I mean, my answers are not going to give you the solution.

183
00:19:11,000 --> 00:19:14,000
There's nothing I can say that's going to make it right for you.

184
00:19:14,000 --> 00:19:17,000
You just need someone to give you a kick in the pants

185
00:19:17,000 --> 00:19:20,000
or to do it yourself and actually begin to meditate.

186
00:19:20,000 --> 00:19:23,000
Figure out what it is that you're doing that keeps you from meditating.

187
00:19:23,000 --> 00:19:27,000
Maybe there's a version to meditation.

188
00:19:27,000 --> 00:19:29,000
Maybe you need to come and do a course with me.

189
00:19:29,000 --> 00:19:31,000
Maybe I'll be a better teacher.

190
00:19:31,000 --> 00:19:34,000
Maybe your teachers are just really lousy.

191
00:19:34,000 --> 00:19:36,000
I don't know.

192
00:19:36,000 --> 00:19:40,000
I'm just teasing.

193
00:19:40,000 --> 00:19:44,000
But there's nothing to do, but just do it.

194
00:19:44,000 --> 00:19:46,000
Nike isn't that a Nike saying?

195
00:19:46,000 --> 00:19:50,000
Nike can get it right.

196
00:19:50,000 --> 00:19:54,000
In sensory deprivation, tanks be used for meditation.

197
00:19:54,000 --> 00:19:59,000
I've always found this sort of fascinating that idea because for us

198
00:19:59,000 --> 00:20:01,000
it's all about the senses.

199
00:20:01,000 --> 00:20:05,000
It's not really about shutting off the senses,

200
00:20:05,000 --> 00:20:07,000
where all the problems come from.

201
00:20:07,000 --> 00:20:10,000
Why would you want to deprive the senses when that's

202
00:20:10,000 --> 00:20:13,000
where all your studying has to be done?

203
00:20:17,000 --> 00:20:18,000
I don't know.

204
00:20:18,000 --> 00:20:21,000
I mean, that should be filed under the speculative sort of questions

205
00:20:21,000 --> 00:20:24,000
and nothing to do with our technique.

206
00:20:24,000 --> 00:20:27,000
I certainly never tell people to do.

207
00:20:31,000 --> 00:20:36,000
I certainly never tell people to enter into sensory deprivation.

208
00:20:42,000 --> 00:20:44,000
Since Buddhist and not believe in determinism,

209
00:20:44,000 --> 00:20:47,000
is it correct to say that the future cannot be predicted with

210
00:20:47,000 --> 00:20:51,000
a certain person's accuracy, except for certain events.

211
00:20:51,000 --> 00:20:56,000
The determinism, and you're asking still on the level of determinism

212
00:20:56,000 --> 00:20:57,000
or free will.

213
00:20:57,000 --> 00:20:59,000
We don't work on that level.

214
00:20:59,000 --> 00:21:03,000
That's not how Buddhism thinks, for lack of a matter of how

215
00:21:03,000 --> 00:21:04,000
where Buddhism acts.

216
00:21:04,000 --> 00:21:08,000
Buddhism is very much about here and now.

217
00:21:08,000 --> 00:21:11,000
It's about learning a true nature of reality.

218
00:21:11,000 --> 00:21:16,000
It's not about philosophizing about whether the future can or can't be predicted.

219
00:21:16,000 --> 00:21:18,000
It's not what Buddhism is about.

220
00:21:18,000 --> 00:21:20,000
So I'm going to cop out and say,

221
00:21:20,000 --> 00:21:22,000
it's not what we deal with.

222
00:21:22,000 --> 00:21:26,000
How do we sign up to take the course with you?

223
00:21:26,000 --> 00:21:30,000
You can go to our website at seriamongolow.org.

224
00:21:30,000 --> 00:21:35,000
Looks like someone's Robin has posted a link.

225
00:21:35,000 --> 00:21:39,000
How long did it take you to learn and be fluent and tie?

226
00:21:39,000 --> 00:21:41,000
That's not a question for me.

227
00:21:41,000 --> 00:21:44,000
But whatever, I'll indulge it because you're here in person.

228
00:21:44,000 --> 00:21:47,000
I wouldn't indulge these sorts of questions.

229
00:21:47,000 --> 00:21:50,000
Because there's just too many of them, right?

230
00:21:50,000 --> 00:21:53,000
Questions that are not related to meditation.

231
00:21:53,000 --> 00:21:57,000
Took me about a year to get fairly fluent and tie.

232
00:21:57,000 --> 00:22:00,000
But I was learning for six or seven years.

233
00:22:00,000 --> 00:22:05,000
It really got improved and staged.

234
00:22:05,000 --> 00:22:09,000
It got really good when I first learned to read.

235
00:22:09,000 --> 00:22:11,000
Then I started taking the tie down my classes

236
00:22:11,000 --> 00:22:14,000
and I had to take the exams in tie.

237
00:22:14,000 --> 00:22:17,000
So I had to write an essay, a two page essay in tie,

238
00:22:17,000 --> 00:22:21,000
which is a lot more difficult than it might sound.

239
00:22:21,000 --> 00:22:23,000
Spelling is very difficult.

240
00:22:23,000 --> 00:22:25,000
Then I started teaching people.

241
00:22:25,000 --> 00:22:30,000
It was actually asked to teach meditation to tie people.

242
00:22:30,000 --> 00:22:34,000
Then I had to really learn how to get my concepts across.

243
00:22:34,000 --> 00:22:38,000
Then I actually started giving talks in tie and that really got...

244
00:22:38,000 --> 00:22:41,000
So yeah, it went in stages.

245
00:22:41,000 --> 00:22:42,000
It became quite fluent.

246
00:22:42,000 --> 00:22:47,000
I guess I'm still probably fairly fluent.

247
00:22:47,000 --> 00:22:49,000
Seeing some lay people use the word sung guys,

248
00:22:49,000 --> 00:22:52,000
the word reserved for monks, nuns, and novices.

249
00:22:52,000 --> 00:22:55,000
Well, there's two kinds of sung others.

250
00:22:55,000 --> 00:22:59,000
The sungga, the sungga that you're thinking of is the winnaya

251
00:22:59,000 --> 00:23:03,000
sungga, which requires actually four monks in up.

252
00:23:03,000 --> 00:23:05,000
One monk is not a sungga.

253
00:23:05,000 --> 00:23:07,000
I once gave a talk and I was talking about the difference.

254
00:23:07,000 --> 00:23:09,000
And I said, any monk is sungga.

255
00:23:09,000 --> 00:23:11,000
But that's only a sungga.

256
00:23:11,000 --> 00:23:15,000
And this layman here, really, what a jerk.

257
00:23:15,000 --> 00:23:17,000
He came up to me later and he said,

258
00:23:17,000 --> 00:23:19,000
yeah, I just wanted to talk to you.

259
00:23:19,000 --> 00:23:20,000
You were wrong.

260
00:23:20,000 --> 00:23:23,000
This guy, Thai guy, he was a real sort of.

261
00:23:23,000 --> 00:23:25,000
He was very much full of himself.

262
00:23:25,000 --> 00:23:27,000
And so he came up to me and he had to correct me because I was wrong.

263
00:23:27,000 --> 00:23:28,000
And he was right.

264
00:23:28,000 --> 00:23:29,000
That was wrong.

265
00:23:29,000 --> 00:23:31,000
That one monk is not a sungga.

266
00:23:31,000 --> 00:23:34,000
It takes at least four monks to be a sungga.

267
00:23:34,000 --> 00:23:40,000
But he's wrong in the sense that the sungga is the entire monastic community.

268
00:23:40,000 --> 00:23:42,000
And we all belong to the sungga.

269
00:23:42,000 --> 00:23:44,000
One monk is a part of the sungga.

270
00:23:44,000 --> 00:23:49,000
It's what I was trying to say.

271
00:23:49,000 --> 00:23:51,000
His sungga, in Thai they would even say,

272
00:23:51,000 --> 00:23:53,000
a monk is sungga.

273
00:23:53,000 --> 00:23:56,000
It's like a soldier is military.

274
00:23:56,000 --> 00:23:58,000
Doesn't mean that soldier is the military.

275
00:23:58,000 --> 00:24:01,000
It means they are military.

276
00:24:01,000 --> 00:24:04,000
But that's one guy in the other kind.

277
00:24:04,000 --> 00:24:11,000
The sutta sungga is all those people who are on the path to enlightenment.

278
00:24:11,000 --> 00:24:16,000
So anyone who's already practicing the Buddha's teaching

279
00:24:16,000 --> 00:24:22,000
could be considered sungga because they're on the path to sotapana.

280
00:24:22,000 --> 00:24:25,000
They're doing the pubangamanga.

281
00:24:25,000 --> 00:24:30,000
And so it's just a matter of how

282
00:24:30,000 --> 00:24:32,000
what level you mean the word sungga.

283
00:24:32,000 --> 00:24:34,000
Because then there's also aria-sanga.

284
00:24:34,000 --> 00:24:38,000
Aria-sanga is, of course, enlightened beings.

285
00:24:38,000 --> 00:24:47,000
So aria-sanga you have to actually have reached manga-niana.

286
00:24:47,000 --> 00:24:53,000
That's the sawa-kasanga that's talked about in the sungga-nusati.

287
00:24:53,000 --> 00:24:55,000
But a sungga is just a community.

288
00:24:55,000 --> 00:24:56,000
The word just means community.

289
00:24:56,000 --> 00:25:01,000
So it could also be used in non-Buddhist sense, I suppose.

290
00:25:01,000 --> 00:25:05,000
I don't know that it ever is used like that in the poly.

291
00:25:05,000 --> 00:25:11,000
Can't remember really.

292
00:25:11,000 --> 00:25:13,000
Okay, well thank you everyone.

293
00:25:13,000 --> 00:25:33,000
Have a good night.

