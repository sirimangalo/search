1
00:00:00,000 --> 00:00:08,280
Hello, today we continue our study of the Dhamapada on with verse number six, which goes

2
00:00:08,280 --> 00:00:24,480
parachana vijananti, maya mehta yamamase, yajatata vijananti, tatto samanti mehta gah, which

3
00:00:24,480 --> 00:00:36,720
means there are some who don't see clearly that we will all have to die, but those who do see

4
00:00:36,720 --> 00:00:52,000
this clearly, yajatata vijananti for those who do see clearly, because of that tatto samanti

5
00:00:52,000 --> 00:01:00,880
mehta gah, because of that they settle their quarrels or settle their disputes, so people

6
00:01:00,880 --> 00:01:08,360
who don't see clearly or don't fully understand, yajananti, many people don't see this,

7
00:01:08,360 --> 00:01:14,320
for those who see it, and that see clearly that we all have to die, that nothing is

8
00:01:14,320 --> 00:01:21,400
permanent, and they're able to settle their quarrels when they realize this truth.

9
00:01:21,400 --> 00:01:28,320
This was told in regards to one of my personal favorites, which is kind of odd, but it's

10
00:01:28,320 --> 00:01:35,880
an interesting monk story, it's about a quarrel that arose between two monks who lived

11
00:01:35,880 --> 00:01:42,760
in Gosi Dharama from monastery in the Buddhist time. Gosi Dharama, there were two monks,

12
00:01:42,760 --> 00:01:48,400
one was very famous for teaching the Dhamma, which is the meditation practice, and the

13
00:01:48,400 --> 00:01:53,920
things that the Buddha taught us to do, we should do, and another one was very famous

14
00:01:53,920 --> 00:01:57,960
for the Vinayat, the Buddha's teaching on the monks' roles and the things that one

15
00:01:57,960 --> 00:02:05,760
shouldn't do, and so they were both very famous, they each had a group of very large

16
00:02:05,760 --> 00:02:10,920
group of students, and one day one of them was, the one who taught the Dhamma was coming

17
00:02:10,920 --> 00:02:15,280
out of the washroom, and the one who taught the Vinayat was going into the washroom,

18
00:02:15,280 --> 00:02:19,600
and the one to the Vinayat looked and saw that the one who teaches the Dhamma was, had

19
00:02:19,600 --> 00:02:30,760
left some water in the bucket in the washroom, which you're not supposed to do because mosquitoes

20
00:02:30,760 --> 00:02:36,520
will lay their eggs, and so he said this, he said, hey, that's an offense, you know, to

21
00:02:36,520 --> 00:02:40,960
do that, and he said, oh, I'm sorry, I didn't realize, then please let me confess it

22
00:02:40,960 --> 00:02:47,960
to you, when there's an offense against the rules, then you have to confess and say,

23
00:02:47,960 --> 00:02:53,800
you're promised that you try your best not to do it again, and just make it clear that

24
00:02:53,800 --> 00:02:57,920
you were aware of the fact that you did it, and the one who taught the Vinayat teaches

25
00:02:57,920 --> 00:03:05,600
the Vinayat, said, but if you didn't realize it was a, or if you didn't do it intentionally,

26
00:03:05,600 --> 00:03:12,720
then it's not an offense, and the other monks said, okay, then, and he left without

27
00:03:12,720 --> 00:03:13,720
confessing it.

28
00:03:13,720 --> 00:03:17,880
And the one who sat the Vinayat, that the Vinayat went around telling all his students

29
00:03:17,880 --> 00:03:23,320
that this other monk had an offense that he hadn't confessed, and so they went and told

30
00:03:23,320 --> 00:03:30,560
the students of the other teacher that this fact, this story, and those monks told the

31
00:03:30,560 --> 00:03:34,080
teacher, and the teacher said, wow, first he tells me it's not an offense, now he says

32
00:03:34,080 --> 00:03:40,040
it is an offense, that monks a liar, and so his students heard this, and they went and

33
00:03:40,040 --> 00:03:46,960
told the other students, hey, your teacher's a liar, and they told their teacher and

34
00:03:46,960 --> 00:03:51,720
the teacher got more angry and said nasty things until finally they were split up into

35
00:03:51,720 --> 00:03:53,720
two different groups.

36
00:03:53,720 --> 00:03:59,920
As a result of that, the novices and the Bikunese, the female monks were split into two

37
00:03:59,920 --> 00:04:05,040
different groups as well, the lay people who supported the monks split up into two groups,

38
00:04:05,040 --> 00:04:10,000
the angels who guarded the monasteries split up into two groups, the angels that were

39
00:04:10,000 --> 00:04:14,880
their friends who lived in the sky and in the various heavens, they all split up into

40
00:04:14,880 --> 00:04:20,800
two groups, and the story goes that this part of the universe split up all the way up

41
00:04:20,800 --> 00:04:28,720
into the Brahma, the god realms, and the gods themselves were split, believe it or not.

42
00:04:28,720 --> 00:04:32,760
However, some water being left in a bucket.

43
00:04:32,760 --> 00:04:39,600
Now the story goes on and on about how the Buddha tried to convince them to change their

44
00:04:39,600 --> 00:04:45,240
ways, and they wouldn't listen, and actually they said to him, oh, please don't bother

45
00:04:45,240 --> 00:04:49,520
yourself, we will take care of this on our own, and they wouldn't let the Buddha even

46
00:04:49,520 --> 00:04:53,960
get involved, and so the Buddha left, and went and stayed, and there's a story, there's

47
00:04:53,960 --> 00:04:58,200
a whole side story about the Buddha stayed in the forest with an elephant and the elephant

48
00:04:58,200 --> 00:05:05,080
gave him food, and took care of him, and then after that he came back, and then at the

49
00:05:05,080 --> 00:05:12,840
end he got all these monks, what happens, the best part of the Buddha left, all of the

50
00:05:12,840 --> 00:05:14,400
lay people said, well, where's the Buddha going?

51
00:05:14,400 --> 00:05:19,400
They said, oh, we told him not to get involved with our quarrel, and so he left, and all

52
00:05:19,400 --> 00:05:26,440
the lay people got angry, and upset, and said, how could you, now we don't have the opportunity

53
00:05:26,440 --> 00:05:31,760
to see the Buddha because of you, and so they stopped giving food to the monks, because

54
00:05:31,760 --> 00:05:39,800
of that the monks were at a very hard time getting arms food and just surviving, and they

55
00:05:39,800 --> 00:05:44,040
went and they apologized, and the people said, well, you have to get an apology from the

56
00:05:44,040 --> 00:05:48,800
forgiveness from the Buddha, because it was the rain season, they couldn't do that, so

57
00:05:48,800 --> 00:05:53,560
they had to wait three months, and they were very, very hard up for three months, and

58
00:05:53,560 --> 00:06:00,320
after this the Buddha came back, and then he put all the put the monks who were quarreling

59
00:06:00,320 --> 00:06:05,080
in a special place and didn't let any of the other monks get involved with them.

60
00:06:05,080 --> 00:06:07,440
When people came and asked, where are those quarreling monks, the Buddha would always

61
00:06:07,440 --> 00:06:11,800
point to them and say, there they are, there they are, and they were so ashamed that

62
00:06:11,800 --> 00:06:15,640
eventually they came and asked forgiveness from the Buddha and found really ashamed

63
00:06:15,640 --> 00:06:20,600
of what they had done, and that's when the Buddha told this verse.

64
00:06:20,600 --> 00:06:27,320
Now the verse has, I think, two important lessons that we can draw from it, and the first

65
00:06:27,320 --> 00:06:32,160
one is in regards to death, which I sort of went into last time, but I'd like to talk

66
00:06:32,160 --> 00:06:33,160
a little bit more.

67
00:06:33,160 --> 00:06:40,200
The second one is about the importance of wisdom and understanding, and how understanding

68
00:06:40,200 --> 00:06:47,600
is really the key, and overcoming quarrels, and overcoming all suffering, and all unwholesome

69
00:06:47,600 --> 00:06:49,880
is all evil.

70
00:06:49,880 --> 00:07:00,800
So first in regards to death, it's interesting how the theory or the ideology, the teaching

71
00:07:00,800 --> 00:07:08,080
on rebirth and how the mind continues, actually makes death a more an important event,

72
00:07:08,080 --> 00:07:12,400
and that this verse kind of makes that clear in a way that we might not think of the

73
00:07:12,400 --> 00:07:16,560
Buddha's telling us how important death is, and when you think, well, if the mind continues

74
00:07:16,560 --> 00:07:22,440
and how is it really so important, and the way it works is this, the body and the mind

75
00:07:22,440 --> 00:07:28,200
have two, they work together, but they have two different characteristics.

76
00:07:28,200 --> 00:07:32,280
The mind is very quick, the mind in one moment can be here, the next moment can be there,

77
00:07:32,280 --> 00:07:37,800
it can arise and move very quickly from one object to the next.

78
00:07:37,800 --> 00:07:44,520
The body on the other hand is very fixed, and so our state of being in this life on a

79
00:07:44,520 --> 00:07:50,840
physical level is quite fixed, but on a mental level it's quite fluid, so your mind can

80
00:07:50,840 --> 00:07:55,680
change, and if your mind becomes more and more corrupt, it can do that very quickly, and

81
00:07:55,680 --> 00:08:02,680
it can change from good to evil or from evil to good, relatively quickly, and there

82
00:08:02,680 --> 00:08:07,920
can be quite a disparity, therefore, between the physical realm and the mental realm.

83
00:08:07,920 --> 00:08:15,120
You can be surrounded by good things and get lots of good and wonderful pleasures and

84
00:08:15,120 --> 00:08:20,400
luxuries and have a very corrupt and evil mind that one would think this person doesn't

85
00:08:20,400 --> 00:08:27,160
deserve all of that luxury, but the truth is this is because of the difference, the body

86
00:08:27,160 --> 00:08:36,360
is a course construct, and the amount of energy that is involved in simply the creation

87
00:08:36,360 --> 00:08:42,960
of a human being or of a physical construct is immense, and so it has a great power, this

88
00:08:42,960 --> 00:08:51,200
power of inertia, where you can't just stop that inertia, now some people if they do

89
00:08:51,200 --> 00:08:57,480
really bad things or really good things, they might wind up cutting it off by dying, you

90
00:08:57,480 --> 00:09:04,160
hear about really good people who are killed because of their unwillingness to do evil

91
00:09:04,160 --> 00:09:09,800
and so on or die, because they're not willing to do evil in order to live on and so on,

92
00:09:09,800 --> 00:09:16,040
and you of course hear about evil people who do bad things and as a result are killed

93
00:09:16,040 --> 00:09:22,080
or dying unpleasant circumstances because of the way their minds are, but for most people

94
00:09:22,080 --> 00:09:30,480
or for many people the inertia and the power of the physical prevents the repercussions

95
00:09:30,480 --> 00:09:35,680
until the moment of death, now at the moment of death the body is totally reconstructed,

96
00:09:35,680 --> 00:09:45,600
there is a period that goes where the mind leaves the body behind and begins this immense

97
00:09:45,600 --> 00:09:51,520
degree or work required to rebuild another body or to create another human being starting

98
00:09:51,520 --> 00:09:59,560
at the moment of conception, now that of course is much more mental and the nature of

99
00:09:59,560 --> 00:10:05,880
that is going to depend almost entirely on the nature of the mind or the set of the mind

100
00:10:05,880 --> 00:10:11,360
but what sort of work is the mind going to undertake, for most people we're caught up

101
00:10:11,360 --> 00:10:16,280
in this programming of creating a human being in a human life and so as a result will

102
00:10:16,280 --> 00:10:20,200
be reborn as a human being but you can be reborn in many different ways based on your

103
00:10:20,200 --> 00:10:27,800
mind and therefore the importance of, as we've seen in the past stories, the importance

104
00:10:27,800 --> 00:10:34,720
of settling our disputes in this life and settling them in our mind anyway, sometimes

105
00:10:34,720 --> 00:10:42,040
we can't stop other people from being angry at us but most of greatest importance

106
00:10:42,040 --> 00:10:49,480
is that we are able to settle them in our own minds and this is the word semanti, semanti

107
00:10:49,480 --> 00:10:56,560
semanti which means to stabilize or to tranquilize them in our minds to make it so that

108
00:10:56,560 --> 00:11:06,480
they no longer, they're no longer a source of clinging, we no longer cling, we no longer

109
00:11:06,480 --> 00:11:15,200
regurgitate and go after and recreate this in our minds so which was obviously the

110
00:11:15,200 --> 00:11:19,880
case, the problem in the case with these two sets of monks which is a real danger and

111
00:11:19,880 --> 00:11:26,560
so the Buddha pointed out to them that, this is really useless, this is really futile

112
00:11:26,560 --> 00:11:31,680
and the reason people do this is because they don't see, they don't see clearly so this

113
00:11:31,680 --> 00:11:37,840
is the importance of death because it frees the mind to make choices again on a level

114
00:11:37,840 --> 00:11:44,120
that it's not free to in this life because of the inertia of its previous work and creating

115
00:11:44,120 --> 00:11:55,280
this being so and the second part is that importance of understanding and how understanding

116
00:11:55,280 --> 00:12:03,680
things like death is important on a meditative level, understanding the truth of birth

117
00:12:03,680 --> 00:12:13,000
and death as occurring on a momentary level is far more important and crucial in fact

118
00:12:13,000 --> 00:12:22,440
in freeing ourselves up from strife and quarrel and disputes and suffering and I've said

119
00:12:22,440 --> 00:12:29,200
it before but it's worth reiterating that this is really all you need to do to overcome

120
00:12:29,200 --> 00:12:34,560
your suffering to overcome quarrels and so on, you don't have to go to apologize to the

121
00:12:34,560 --> 00:12:41,240
people, I mean that's always a good way to overcome it but in on a general level in regards

122
00:12:41,240 --> 00:12:46,080
to our clings in regards to our cravings in regards to our attachment, all that needs

123
00:12:46,080 --> 00:12:49,360
to be done is to see it clearly.

124
00:12:49,360 --> 00:12:53,600
When you're addicted to something, you don't have to force yourself to stop or try to

125
00:12:53,600 --> 00:12:57,640
convince yourself that it's wrong or remind yourself again and again how bad you are

126
00:12:57,640 --> 00:13:03,280
for doing it, all you have to do is see the addiction clearly for what it is, see it objectively,

127
00:13:03,280 --> 00:13:08,080
let it be, let it come up and when you see it for what it is, you'll let go of it, you'll

128
00:13:08,080 --> 00:13:13,520
have no desire to cling to it because you'll see that everything ceases, so this Buddhist

129
00:13:13,520 --> 00:13:18,200
teaching on death was only the introduction, it's this reminder to bring the monks back

130
00:13:18,200 --> 00:13:23,120
to this idea that everything arises in ceases because once they can see that we die, they

131
00:13:23,120 --> 00:13:27,200
can see how futile it is, why are we fighting, what are we hoping to gain, we can come

132
00:13:27,200 --> 00:13:33,400
out on top as I said in the last video but you know what does it mean to be on top because

133
00:13:33,400 --> 00:13:42,040
eventually we all die and put on bottom, so once they start to do that then you can start

134
00:13:42,040 --> 00:13:45,480
to see that really why would we cling to anything and you start to able to look at your

135
00:13:45,480 --> 00:13:50,560
clinging and see that, when you cling to something you hold on to it, it maybe brings

136
00:13:50,560 --> 00:13:56,040
your pleasure temporarily but then it disappears and what is the benefit, what is the gain

137
00:13:56,040 --> 00:14:03,360
that comes from that, when you see on a phenomenological level of experiential or moment

138
00:14:03,360 --> 00:14:10,160
level, how everything arises in ceases, how this happiness, this pleasure that you're

139
00:14:10,160 --> 00:14:16,400
hoping to get is really actually bound up with suffering and stress because it conditions

140
00:14:16,400 --> 00:14:24,720
the mind to want more and to strive more and to be stressed and you can make yourself

141
00:14:24,720 --> 00:14:36,040
sick and uncomfortable and unwell as a result of the clinging, so when you practice meditation

142
00:14:36,040 --> 00:14:42,160
when you look at the objects of awareness on a moment, a moment basis, you're able to

143
00:14:42,160 --> 00:14:50,640
see that everything arises in ceases, simply that seeing allows you to, it changes your

144
00:14:50,640 --> 00:14:54,720
whole way of looking at things, you no longer look at things in terms of the universe in

145
00:14:54,720 --> 00:15:01,560
terms of us and them and I and me and mine and so on, all of the, so many of the points

146
00:15:01,560 --> 00:15:13,600
of reference that we take on an ordinary run of the meal frame of reference are gone,

147
00:15:13,600 --> 00:15:19,520
are lost, are given up by someone who takes up this practice, you really change your

148
00:15:19,520 --> 00:15:24,680
whole way of looking at the world and this is, I think it's important to pick up this

149
00:15:24,680 --> 00:15:30,720
word in this verse, we John and thee, which is really the same word as vipasana, vipasana

150
00:15:30,720 --> 00:15:35,720
means to seek clearly, we John and thee means to know clearly or to understand clearly,

151
00:15:35,720 --> 00:15:41,120
but it's really the same word and it has the exact same meaning and it's really the core

152
00:15:41,120 --> 00:15:45,160
of Buddhism and it's so easy when you read this verse to just skip by that and say,

153
00:15:45,160 --> 00:15:49,640
oh yeah, yeah, we have to think about death and that's the most important. Now the most important

154
00:15:49,640 --> 00:15:55,960
is the knowing, the understanding, when you see, with death as an example, when you see

155
00:15:55,960 --> 00:16:01,520
that everything ceases, when you see, you know, our quarrel, why are we quarreling, we

156
00:16:01,520 --> 00:16:08,080
have to die, it helps you see that everything ceases, when you see on a momentary level

157
00:16:08,080 --> 00:16:12,160
and here I'm clinging to this thing, why would I cling to it when it has to cease, when

158
00:16:12,160 --> 00:16:15,560
you're able to see things arising and ceasing, this is what the meaning is, when you're

159
00:16:15,560 --> 00:16:22,160
able to understand clearly that there is nothing in the world that you can hold on to,

160
00:16:22,160 --> 00:16:29,920
that lasts more than an instant, when you're able to see that it's futile to cling and

161
00:16:29,920 --> 00:16:36,480
it's useless, it's unbeneficial and actually it's a source of great stress and suffering

162
00:16:36,480 --> 00:16:42,560
by its very nature, by the stress caused from clinging, from wanting, from craving and so on

163
00:16:42,560 --> 00:16:51,840
and the building and building up of more and more craving and suffering when you don't

164
00:16:51,840 --> 00:16:57,760
get what you want and so on. So, this is another important verse and another good lesson

165
00:16:57,760 --> 00:17:09,960
for us, this is the Dhamma for today, from the Dhamma Tada, so thanks for tuning in,

