1
00:00:00,000 --> 00:00:05,200
Hello and welcome again to our study of the Dhamalthada.

2
00:00:05,200 --> 00:00:12,800
Today we go on to verses number 26 in 27, which read as follows.

3
00:00:12,800 --> 00:00:30,200
Today we can agree with Aupamana's She and Dhamaj.

4
00:00:30,200 --> 00:00:44,960
the santa-wang, apamato-hi-chayanto-papot-papot-hi-wipulang-sukhani, so two verses, again, on the practice

5
00:00:44,960 --> 00:00:50,040
of meditation. The apamada-waga is going to be mostly about the practice of

6
00:00:50,040 --> 00:00:54,560
meditation because the word apamada, which runs it as heedfulness, but it's

7
00:00:54,560 --> 00:00:59,000
really the core of what is meant by meditation or meditation practice and

8
00:00:59,000 --> 00:01:05,600
Buddhism. It's translated directly by the Buddha as mindfulness. It says, the

9
00:01:05,600 --> 00:01:09,880
meaning of the word apamada is to be mindful all the time, to never be

10
00:01:09,880 --> 00:01:16,160
without mindfulness or without sati, without remembrance or recollection. So the

11
00:01:16,160 --> 00:01:21,400
meaning of the direct meaning of these verses, pamada-manu-yun-janti, they

12
00:01:21,400 --> 00:01:30,280
indulge or they partake of heedlessness or negligence, the fools and the

13
00:01:30,280 --> 00:01:43,840
unwise people, but those who are wise made how we the wise partake in

14
00:01:43,840 --> 00:01:54,760
apamada, partake in heedfulness and guarded rakati, the nansetang like a high or

15
00:01:54,760 --> 00:02:03,480
a special treasure. So the wise, guard mindfulness or heedfulness as a great

16
00:02:03,480 --> 00:02:14,960
treasure. Then the Buddha says, don't engage in heedlessness, in negligence.

17
00:02:14,960 --> 00:02:25,080
Makama-rati-santava, don't partake or indulge in or don't become intoxicated

18
00:02:25,080 --> 00:02:35,280
by sensuality, because those who only those who are heedful and meditating

19
00:02:35,280 --> 00:02:45,000
attain great happiness or attain a profound happiness, pabu-ti, vipulang-sukan, a

20
00:02:45,000 --> 00:02:50,800
profound happiness and sustained. So this is a very important teaching to

21
00:02:50,800 --> 00:02:55,040
understand, and it's something that really cuts right to the heart of our

22
00:02:55,040 --> 00:03:01,760
attachments in the worldly sphere. Really good story, very short story, but

23
00:03:01,760 --> 00:03:10,080
the story goes that insawati, there was a holiday, and it kind of seems like a

24
00:03:10,080 --> 00:03:13,400
strange holiday, but there are parallels even in modern times, it was a

25
00:03:13,400 --> 00:03:20,000
holiday where anything went, anything was allowed. So no longer you had to

26
00:03:20,000 --> 00:03:25,520
there was no sense of respect for anyone. The people who partook in this

27
00:03:25,520 --> 00:03:32,080
holiday would abandon all sense of respect for mother, father, for boss, for

28
00:03:32,080 --> 00:03:37,160
teacher and so on, and they would go from house to house, probably drunk, and

29
00:03:37,160 --> 00:03:43,840
abuse and revile everyone. You pig, you cow, you go calling them all sorts of horrible

30
00:03:43,840 --> 00:03:49,120
names. And the game was that if you wanted them to go away, you had to give them

31
00:03:49,120 --> 00:03:52,880
stuff. So it was like trick-or-treat, basically, like Halloween, except they

32
00:03:52,880 --> 00:03:56,960
really did it. They were really mean and nasty, and they would stay there

33
00:03:56,960 --> 00:04:02,400
until you give them some money, or some wine, or I don't know, whatever food,

34
00:04:02,400 --> 00:04:09,360
and then they would go away. And so the thing was that insawati, there was a

35
00:04:09,360 --> 00:04:13,680
great proportion of people who were enlightened, or sotapana or sakita gami,

36
00:04:13,680 --> 00:04:17,640
you know, at some stage of enlightenment, so they obviously wouldn't partake in

37
00:04:17,640 --> 00:04:22,920
such a festival. And so they had a horrible time for seven days. And they asked

38
00:04:22,920 --> 00:04:26,000
the Buddha not to come to sawati, because if the Buddha went there, they would

39
00:04:26,000 --> 00:04:29,800
revile the Buddha, and that would be very bad karma for them. So they said to

40
00:04:29,800 --> 00:04:33,480
the Buddha, please stay and stay in jayta one, and we'll bring you food every day.

41
00:04:33,480 --> 00:04:37,520
So they brought food for seven days. And then after the seventh day they came to

42
00:04:37,520 --> 00:04:41,720
see the Buddha and said, oh, venerable sir, we stayed so awful for these seven

43
00:04:41,720 --> 00:04:45,840
days. It was so, so, so, so tiring for us to have to put up with this

44
00:04:45,840 --> 00:04:55,240
silliness. Why are people like this? Why do people act in such a way? Isn't it

45
00:04:55,240 --> 00:04:59,400
crazy that they do? And the Buddha said, oh, yes, this is the difference between

46
00:04:59,400 --> 00:05:04,800
those who are enlightened or those who have understood the truth and those who

47
00:05:04,800 --> 00:05:10,280
are still mired in delusion. And so the first verse here, and then he told

48
00:05:10,280 --> 00:05:15,000
these verses, and the first verse explains the difference between the two,

49
00:05:15,000 --> 00:05:20,560
that an ordinary person, a person who has no idea about what is right and what

50
00:05:20,560 --> 00:05:25,560
is wrong and has never contemplated these things or investigated what might be

51
00:05:25,560 --> 00:05:35,000
the truth. What the things that they value are the most base and useless

52
00:05:35,000 --> 00:05:41,560
qualities, for instance, negligence or drunkenness. So an ordinary person and

53
00:05:41,560 --> 00:05:47,400
people in ordinary life will actually, they will, the things that they will

54
00:05:47,400 --> 00:05:51,960
cherish are the times when they were the least mindful and the least clear in

55
00:05:51,960 --> 00:05:57,200
their mind. So when you think in lay life what would people or not in lay life

56
00:05:57,200 --> 00:06:00,680
for people who haven't practiced meditation, the kind of things that they

57
00:06:00,680 --> 00:06:03,440
cherish. They cherish those times when they were drunk or when they did

58
00:06:03,440 --> 00:06:07,760
something silly and they were having a great time and doing all sorts of crazy

59
00:06:07,760 --> 00:06:12,800
things when they were young and crazy and how wonderful it was and so on. They

60
00:06:12,800 --> 00:06:17,360
cherish these times when they do the craziest things. And so people who, for

61
00:06:17,360 --> 00:06:20,840
instance, in this festival, they would always think back to, oh, wasn't it a

62
00:06:20,840 --> 00:06:23,920
wonderful time where we could say whatever we want, didn't we have a great time

63
00:06:23,920 --> 00:06:29,960
then? And they make it out to be this wonderful thing. And a person who is in

64
00:06:29,960 --> 00:06:34,040
a person who has actually taken the time to see the consequences of our

65
00:06:34,040 --> 00:06:39,560
actions and the consequences of every individual mind-state is totally

66
00:06:39,560 --> 00:06:43,920
opposite and actually cherish us as a treasure those times when they are

67
00:06:43,920 --> 00:06:48,640
a cogent those times when they are aware. And one of the big reasons for this

68
00:06:48,640 --> 00:06:54,520
I think is that a person who has no idea or no skill in understanding

69
00:06:54,520 --> 00:07:00,520
experience will have a very difficult time just being with reality. Reality is

70
00:07:00,520 --> 00:07:03,960
the worst thing for them. The last thing they want to focus on because it's

71
00:07:03,960 --> 00:07:08,520
unpleasant, it's difficult to deal with. So pain comes up and they're in tears

72
00:07:08,520 --> 00:07:13,520
in their eyes and when someone's saying things to them and they don't like

73
00:07:13,520 --> 00:07:17,560
and they get angry when they have to do work and then they get become frustrated

74
00:07:17,560 --> 00:07:23,840
or bored or depressed or so on. The last thing they want to do is to focus on

75
00:07:23,840 --> 00:07:36,040
reality. So the whole goal or the point in Buddhism in Buddhist teaching is to

76
00:07:36,040 --> 00:07:42,680
come back to reality and to find a way to be happy at all times in any

77
00:07:42,680 --> 00:07:50,840
situation not have our happiness depend on some specific circumstance and

78
00:07:50,840 --> 00:07:58,440
not have it require some specific situation because when your happiness

79
00:07:58,440 --> 00:08:02,160
depends on something it becomes an addiction and it becomes a partiality where

80
00:08:02,160 --> 00:08:07,280
you aren't able to accept the rest of reality because you're thinking about

81
00:08:07,280 --> 00:08:16,920
or wanting or striving after a certain experience and the experience has to

82
00:08:16,920 --> 00:08:22,040
become further and more and more extravagant in order to satisfy the

83
00:08:22,040 --> 00:08:29,920
cravings because our pleasure stimulus isn't a static system. When you get

84
00:08:29,920 --> 00:08:33,760
what you want you want more of it. When you get more of it you want even more

85
00:08:33,760 --> 00:08:37,800
and it gets builds and builds and builds until you the only way you can be

86
00:08:37,800 --> 00:08:41,920
happy is to have some crazy festival like this where you go and do something

87
00:08:41,920 --> 00:08:47,640
because you're repressing these desires, you're repressing your greed, your

88
00:08:47,640 --> 00:08:54,880
anger and so on. I heard about this one society, this one group of people

89
00:08:54,880 --> 00:09:05,960
where they will engage in group sex. They will get all together and have a big

90
00:09:05,960 --> 00:09:11,840
orgy just like animals and for them I mean this may sound coarse and rude to

91
00:09:11,840 --> 00:09:15,800
say but it's actually a philosophy that they have that well if this is a way to

92
00:09:15,800 --> 00:09:22,320
find pleasure then you should strive to indulge in it to the most extreme

93
00:09:22,320 --> 00:09:27,880
extent possible and so it's a philosophy that they have and the idea being that

94
00:09:27,880 --> 00:09:31,720
as long as you can get it then what's wrong with it because you can be happy all

95
00:09:31,720 --> 00:09:36,360
the time this is the idea and this is for so this is for people who don't see

96
00:09:36,360 --> 00:09:39,040
what they're doing to their mind they think it's somehow static well if I

97
00:09:39,040 --> 00:09:42,040
wanted any time I can get it and then if I don't want it I can come back and

98
00:09:42,040 --> 00:09:45,840
not have it without seeing the the effect that it's having on your mind and

99
00:09:45,840 --> 00:09:50,480
on the the pleasure stimulus systems in the brain and so on and how actually

100
00:09:50,480 --> 00:09:55,640
you're creating an addiction and you're creating an intense partiality this

101
00:09:55,640 --> 00:09:59,440
is an example of an extreme state there are societies where they don't allow

102
00:09:59,440 --> 00:10:08,800
this sort of thing and where you have to be totally formal and polite and

103
00:10:08,800 --> 00:10:16,640
proper at all times conservative ethical and so on and any kind of

104
00:10:16,640 --> 00:10:22,720
showing of emotion is considered bad form but then they then so that

105
00:10:22,720 --> 00:10:25,720
they were they still have these desires and they repressed them and repressed

106
00:10:25,720 --> 00:10:29,760
them and then they have these these festivals so there I've heard places

107
00:10:29,760 --> 00:10:35,560
where everything has to be proper and everyone has a role and a hierarchy

108
00:10:35,560 --> 00:10:38,880
and so on but then they have these parties where they get drunk and anything

109
00:10:38,880 --> 00:10:42,600
goes and like you have I heard about the boss getting together with you know

110
00:10:42,600 --> 00:10:48,080
having crazy things so these are the things that I hear in the world and these

111
00:10:48,080 --> 00:10:53,760
things really do go on I mean it's actually kind of embarrassing to talk

112
00:10:53,760 --> 00:10:56,280
about these things but but this is what's really happening in the world

113
00:10:56,280 --> 00:11:03,120
people are so lost that they actually think these things are are a cause

114
00:11:03,120 --> 00:11:09,240
for happiness and so there are scientists and researchers who have

115
00:11:09,240 --> 00:11:13,720
studied this and see that it really goes step-by-step by step into finally

116
00:11:13,720 --> 00:11:17,640
it becomes such a perverse thing where you're involving you know these

117
00:11:17,640 --> 00:11:24,360
groups or or even they were saying on this one side this one this one article

118
00:11:24,360 --> 00:11:29,360
that I read it eventually gets to with animals and so on right where where

119
00:11:29,360 --> 00:11:33,840
you find this is where you find your pleasure and so this is the kind of thing

120
00:11:33,840 --> 00:11:37,000
that we would have seen in Sabati of course it just talks about reviling

121
00:11:37,000 --> 00:11:43,280
people but for sure that they they had other festivals in regards to

122
00:11:43,280 --> 00:11:47,160
sensuality and this is where the whole gamma sutra came up and so on and this

123
00:11:47,160 --> 00:11:51,920
is how people in the world think that they can find happiness the there are

124
00:11:51,920 --> 00:11:55,640
so there are there are two ways that we understand this for someone who

125
00:11:55,640 --> 00:11:58,520
practices meditation it's obvious that these things don't need to

126
00:11:58,520 --> 00:12:02,880
happiness you engage in them you indulge in them and you find that you're just

127
00:12:02,880 --> 00:12:06,880
back where you started from you're you're still unhappy inside you're still

128
00:12:06,880 --> 00:12:16,880
unsatisfied even even with these things but really the idea is that we're

129
00:12:16,880 --> 00:12:20,840
trying to find happiness here and now even if you can say that that well I'm

130
00:12:20,840 --> 00:12:25,040
maybe unhappy here but when I get what I want at that time I'm happy so what's

131
00:12:25,040 --> 00:12:28,080
wrong with it I'm I'm getting a happiness right at the time when I get it

132
00:12:28,080 --> 00:12:34,560
there's pleasure but the point is that the the whole fact that you bear

133
00:12:34,560 --> 00:12:38,920
fact that you want that is a sign that you're not happy if you are happy you

134
00:12:38,920 --> 00:12:43,040
wouldn't need to strive for something you wouldn't need to go for something you

135
00:12:43,040 --> 00:12:48,000
need to build something you wouldn't need to create something you wouldn't need to

136
00:12:48,000 --> 00:12:54,080
attain something or get something so the radical task that we have is to find

137
00:12:54,080 --> 00:12:58,880
happiness without getting that without getting anything to find happiness here

138
00:12:58,880 --> 00:13:03,320
and now and it's actually a really a simple thing and and if you think about it

139
00:13:03,320 --> 00:13:07,400
it's how it should be that we should find happiness here now why should our

140
00:13:07,400 --> 00:13:11,880
happiness depend on something over there why can't our happiness be here and

141
00:13:11,880 --> 00:13:16,680
now it's a very valid question but it's one that we never ask and we always

142
00:13:16,680 --> 00:13:21,800
overlook we think obviously if you want happiness you have to strive for it

143
00:13:21,800 --> 00:13:26,160
you have to go for it well the truth is that even to find happiness here and

144
00:13:26,160 --> 00:13:30,560
now you have to strive for it and so this is why actually often people will

145
00:13:30,560 --> 00:13:35,080
find that the practice of meditation is something that is is

146
00:13:35,080 --> 00:13:39,160
uncomfortable and unpleasant right the point being because you always want to go

147
00:13:39,160 --> 00:13:43,560
out for something you want to go after that and you're you're trying to train

148
00:13:43,560 --> 00:13:46,720
yourself out of that so sometimes you stop yourself and you say no and then

149
00:13:46,720 --> 00:13:51,520
you feel unhappy or sometimes you go after it and then you feel guilty or

150
00:13:51,520 --> 00:13:57,960
someone until you finally are able to change your mind and straighten your

151
00:13:57,960 --> 00:14:07,480
mind so that you're able to see the the goal to see the the the object of your

152
00:14:07,480 --> 00:14:13,640
desire and to simply see it for what it is and to be able to live with the the

153
00:14:13,640 --> 00:14:18,040
perception of something without having to chase after it so when you think of

154
00:14:18,040 --> 00:14:22,760
something that you normally would like you're just thinking of it when you hear a

155
00:14:22,760 --> 00:14:26,800
sound that you you you find pleasant you just hear the sound when you see

156
00:14:26,800 --> 00:14:32,360
something beautiful you just see it and you're able to to experience happiness in

157
00:14:32,360 --> 00:14:37,200
that moment without having your happiness be only when you get that object

158
00:14:37,200 --> 00:14:42,520
and you find yourself at peace here and now but it's something that you have to

159
00:14:42,520 --> 00:14:45,520
strive for so we can say that there are these two strivings this is what the

160
00:14:45,520 --> 00:14:50,440
Buddha said people who who strive after pleasure and trying to find some

161
00:14:50,440 --> 00:14:54,800
object of happiness and people who strive to be free from that to find

162
00:14:54,800 --> 00:14:58,200
happiness here and now it's really two different paths it's not that we're

163
00:14:58,200 --> 00:15:03,960
striving to achieve something or get something we're trying to stop striving to

164
00:15:03,960 --> 00:15:08,840
stop achieving to stop chasing after it's also very difficult so these are

165
00:15:08,840 --> 00:15:12,680
the two paths and this is what the Buddha lays out here that for ordinary

166
00:15:12,680 --> 00:15:18,840
people they will just let themselves go drink alcohol and take drugs and

167
00:15:18,840 --> 00:15:24,520
engage and all sorts of foolishness enjoying the happiness but they don't see

168
00:15:24,520 --> 00:15:28,520
how it's changing their minds and they don't see how it's building these these

169
00:15:28,520 --> 00:15:33,120
mindsings we're not static beings this is another thing is this shows how

170
00:15:33,120 --> 00:15:37,880
important the Buddha's teaching of karma is and it goes a lot deeper than we

171
00:15:37,880 --> 00:15:42,240
actually think because I don't have karma you don't have karma karma is the

172
00:15:42,240 --> 00:15:47,840
truth of reality they're only is karma we create ourselves out of karma the

173
00:15:47,840 --> 00:15:51,800
whole of our being is created out of karma the whole of our life is created out

174
00:15:51,800 --> 00:15:56,240
of karma there's no eye there's only the action the things that we do so

175
00:15:56,240 --> 00:16:01,800
everything that we do creates us the reason why we get angry is because we've

176
00:16:01,800 --> 00:16:05,720
built up anger the reason why we have greed and one thing is because we build

177
00:16:05,720 --> 00:16:10,400
up greed and one thing year after year that's all we are where these accumulation

178
00:16:10,400 --> 00:16:16,080
or these conglomeration aggregation of of of minds things both positive and

179
00:16:16,080 --> 00:16:20,040
negative why we have love and kindness is because we've developed this because

180
00:16:20,040 --> 00:16:24,040
we've come to see the truth because we've come to see how how horrible it is

181
00:16:24,040 --> 00:16:29,520
to suffer and so we don't want to see other beings suffer and so on we are

182
00:16:29,520 --> 00:16:34,120
what we do we are according to the the things that we do in the qualities that

183
00:16:34,120 --> 00:16:39,960
we build up so it's important to understand the difference here because I

184
00:16:39,960 --> 00:16:44,160
think all of us can can can see in our own lives and in our own past this

185
00:16:44,160 --> 00:16:49,160
attachment to negligence attachment to having fun and enjoying life and so on

186
00:16:49,160 --> 00:16:54,400
without seeing how it's creating addiction how it's actually feeding our

187
00:16:54,400 --> 00:16:58,680
dissatisfaction so it's making it harder for us to just be here and now

188
00:16:58,680 --> 00:17:04,120
when we come back to be just here now we see how radically opposed it that

189
00:17:04,120 --> 00:17:07,960
these two paths are being just being here and now is something totally

190
00:17:07,960 --> 00:17:12,920
different and completely opposed in the sense that the more we strive to

191
00:17:12,920 --> 00:17:18,440
achieve happiness and drive to achieve pleasure the harder it is for us to find

192
00:17:18,440 --> 00:17:22,200
happiness and peace here and now the more we find peace and happiness here and

193
00:17:22,200 --> 00:17:26,120
now the less we want the less we need of anything if you think just where we are

194
00:17:26,120 --> 00:17:31,040
right here right now when we find happiness here everything else disappears

195
00:17:31,040 --> 00:17:37,560
suddenly the whole world all of who we are our lives our friends our family all

196
00:17:37,560 --> 00:17:43,680
of our thoughts all of our ambitions have just disappeared vanished in

197
00:17:43,680 --> 00:17:49,760
there is the first verse the second verse is an injunction by the Buddha that

198
00:17:49,760 --> 00:17:54,360
once you see this if you're a wise person you should not engage in

199
00:17:54,360 --> 00:17:58,760
negligence don't engage in it because it will make it harder for you to find

200
00:17:58,760 --> 00:18:02,360
happiness it will make you less happy less satisfied less at peace with

201
00:18:02,360 --> 00:18:10,000
yourself don't be don't get caught up in delight in sensuality

202
00:18:10,000 --> 00:18:18,120
because this is sensuality is really that the worst of them it's the most

203
00:18:18,120 --> 00:18:22,640
course of our desires we may have ambitions or so on but our desire for

204
00:18:22,640 --> 00:18:28,680
sensuality sites and sounds and smells and tastes and feelings is the worst

205
00:18:28,680 --> 00:18:33,120
because it involves the body and it involves chemicals and hormones and so on

206
00:18:33,120 --> 00:18:39,600
and dopamine and the brain and so on and there's this addiction cycle which you

207
00:18:39,600 --> 00:18:45,040
know as I said is is actually creating what we call sankara it's forming our

208
00:18:45,040 --> 00:18:50,400
reality it's changing who we are from the very core it's not us doing this

209
00:18:50,400 --> 00:18:58,280
it's this doing becomes us this is who we become and he says why why should

210
00:18:58,280 --> 00:19:02,280
why should you follow what I say why should you not be negligent and why

211
00:19:02,280 --> 00:19:07,280
should you not indulge in delight in sensuality he says because it's only the

212
00:19:07,280 --> 00:19:15,800
upper motto only by being hateful giant oh and meditating does one attain

213
00:19:15,800 --> 00:19:22,200
bubble deep we belong to come up or found the profoundest happiness so the

214
00:19:22,200 --> 00:19:26,900
happiness here is the happiness of peace and Buddha said not these and deep

215
00:19:26,900 --> 00:19:31,880
but on so on there's no happiness without peace the happiness is that we call

216
00:19:31,880 --> 00:19:36,480
happiness the pleasures in the world the things that we say are our happiness or

217
00:19:36,480 --> 00:19:42,320
our way to find true satisfaction are not real happiness because they're

218
00:19:42,320 --> 00:19:48,880
impermanent they change they're dependent they require some attainment that

219
00:19:48,880 --> 00:19:54,200
is also important they require an object and they create partiality they

220
00:19:54,200 --> 00:20:00,080
create their opposite which is suffering and dissatisfaction and so one the more

221
00:20:00,080 --> 00:20:04,080
one indulges the more one goes back and forth between one and the other

222
00:20:04,080 --> 00:20:09,400
pleasure and pain and what is never at peace one is never able to just be here

223
00:20:09,400 --> 00:20:15,160
and now one one's life is always about seeking after this running away from

224
00:20:15,160 --> 00:20:19,760
that seeking running seeking running being pushed and pulled back and forth and

225
00:20:19,760 --> 00:20:28,240
one can never just be here and now one never has the the ability or the safety

226
00:20:28,240 --> 00:20:34,040
stability just being here and now they can build up some kind of pleasant state

227
00:20:34,040 --> 00:20:39,680
of being for some time and then see it washed away at the very end and when they

228
00:20:39,680 --> 00:20:43,640
die they will be in the state of loss because of their attachments because of

229
00:20:43,640 --> 00:20:47,880
the things that they still are clinging to so the profound happiness that the

230
00:20:47,880 --> 00:20:52,640
Buddha is talking about is peace here and now being able to be in here and now

231
00:20:52,640 --> 00:20:57,720
without any clinging or any attachment so that's the teaching today that's

232
00:20:57,720 --> 00:21:03,440
Damapada versus 26 and 27 thank you for tuning in I wish you all to find peace

233
00:21:03,440 --> 00:21:28,600
happiness and freedom for themselves thank you

