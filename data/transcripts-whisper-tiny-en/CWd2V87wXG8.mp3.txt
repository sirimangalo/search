When I practice sitting meditation, usually about 20 minutes into it, I feel very relaxed,
and my vision with my eyes closed gets darker and darker, and I have a feeling of falling.
When this happens to me, am I on the right track?
Do I have a focused concentration at this time, or it's just nothing?
Who was that?
First of all, you should just observe as it is when it is getting darker,
then observe seeing, seeing, because it's your vision.
It's getting darker and your vision you describe.
So I would say seeing, seeing at that time,
and then you have the feeling of falling,
and then I would say feeling, feeling, feeling, and not make a deal out of it.
So when you think of it, when you ponder over it
and want it to be something that your concentration is high,
or so, then you are looking for a gain,
and this can lead you into what we were talking earlier
into one of the Vipassana pakilesa.
So it is dangerous to want to know that.
Is that something good, or is that just nothing?
Be relaxed about it.
If you're relaxed, that's good.
Notice, relaxed, relaxed, and then you will be on the right track.
When you are looking for the gain, or when you are about to judge,
is this good, or is this not good, then you are not on the right track?
Again, this very basic principle that I mentioned,
it's the practice that is the right track.
It's the practice which leads to progress.
It's a very obvious principle,
and yet it's so easy to lose sight of it,
because we're talking about human beings
who are generally irrational.
We act in very irrational ways.
And so if you think about it,
I think if you think about it objectively,
you should be able to realize for yourself that,
no, how could you be on the right track?
At that moment, you're no longer meditating.
But because of our irrational attachment
to the strength, the mystical, the super mundane,
the special, we think that maybe now all my problems will be over.
Maybe now suffering will cease.
Maybe I don't have to practice anymore.
And this is all based on defilement and detachment.
The practice is that which leads to progress,
and only the practice.
Another thing you can always remind yourself of,
which it just said, this is not that.
This is a motto you should have.
This is not that.
Whatever it is, it's not that.
It's not what you're looking for.
So if you just keep reminding yourself of that,
then you won't cling to anything.
And everything that comes up,
you'll be able to let it go.
And the other thing that went to say,
though it was sort of already said,
is that another principle that we always have to remind people
of is we're not trying to gain concentration.
It's not a sign that you're on the right track
that you have concentration.
Concentration is a mine state that has to be balanced with energy.
True concentration, when balanced with energy,
doesn't feel concentrated at all.
You don't even notice it.
It's totally natural.
You feel that you're just present.
That's in a sense more focused than concentration.
Focus is where you're perfectly clear when a camera is focused.
It's not that it's all the way in its halfway.
It's right in the middle where you can see things clearly.
And this is very important because people get the wrong idea
that meditation is about concentration.
It's not what the word meditation means.
It's not what insight is.
It has nothing intrinsically to do with the development of understanding.
Understanding comes from focus, which means to focus the lens
and to balance your faculties.
Whenever you have some extreme experience of concentration or energy
or whatever, this isn't the path.
It's some by-product side phenomenon.
The path is being mindful of it.
