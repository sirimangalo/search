1
00:00:00,000 --> 00:00:07,000
So, what brought you to Thailand in the first place?

2
00:00:07,000 --> 00:00:12,000
I was just passing through Thailand travelling.

3
00:00:12,000 --> 00:00:15,000
And you came to see Deutsche Tep.

4
00:00:15,000 --> 00:00:18,000
I came to Deutsche Tep to study meditation.

5
00:00:18,000 --> 00:00:21,000
Really, how did you find out about it?

6
00:00:21,000 --> 00:00:27,000
What got you interested in, what made you think I'll go and practice meditation?

7
00:00:27,000 --> 00:00:32,000
I met some people who were Buddhists and they got me interested in Buddhism.

8
00:00:32,000 --> 00:00:33,000
Thai people.

9
00:00:33,000 --> 00:00:34,000
No foreigners.

10
00:00:34,000 --> 00:00:36,000
And I started reading about Buddhism.

11
00:00:36,000 --> 00:00:41,000
And then I met a person who said there's no reason to read about Buddhism.

12
00:00:41,000 --> 00:00:43,000
We have to go and practice.

13
00:00:43,000 --> 00:00:46,000
And what brought you to Deutsche Tep?

14
00:00:46,000 --> 00:00:49,000
I was looking for a course.

15
00:00:49,000 --> 00:00:53,000
I could do meditation in Chiang Mai in Thailand.

16
00:00:53,000 --> 00:00:57,000
And yeah, Deutsche Tep seemed to be the best one.

17
00:00:57,000 --> 00:01:00,000
Did we have, was it like internet or?

18
00:01:00,000 --> 00:01:03,000
Yeah, on the internet.

19
00:01:03,000 --> 00:01:05,000
And how was that?

20
00:01:05,000 --> 00:01:08,000
How was it practicing at Deutsche Tep?

21
00:01:08,000 --> 00:01:10,000
Obviously excellent.

22
00:01:10,000 --> 00:01:13,000
Yeah, I was the teacher.

23
00:01:13,000 --> 00:01:16,000
I don't even remember you, that's how good of a teacher.

24
00:01:16,000 --> 00:01:20,000
Now I do, I do, but too many people know.

25
00:01:20,000 --> 00:01:23,000
Yeah, well I was very quiet as she did.

26
00:01:23,000 --> 00:01:25,000
Yeah, it's the loud ones you remember.

27
00:01:25,000 --> 00:01:27,000
I was telling Nagasena after that.

28
00:01:27,000 --> 00:01:36,000
I was saying, for me, you know, I don't feel so bad about that because the truth of knowing your students.

29
00:01:36,000 --> 00:01:42,000
My teacher, when I first came and started living with him as a monk,

30
00:01:42,000 --> 00:01:44,000
he could never remember my name.

31
00:01:44,000 --> 00:01:52,000
I was quiet and, you know, I did the practice and I just smiled and was very keen to learn as much as I could.

32
00:01:52,000 --> 00:01:55,000
But he never remembered my name and it really made me upset.

33
00:01:55,000 --> 00:02:00,000
You know, or upset, it made me sad to know that my teacher could never remember my name at that.

34
00:02:00,000 --> 00:02:02,000
Oh, he remembers other people's names.

35
00:02:02,000 --> 00:02:06,000
I must be meaningless, I must be unimportant.

36
00:02:06,000 --> 00:02:15,000
And then some problem came up and I made a real mess of things.

37
00:02:15,000 --> 00:02:18,000
I started this big problem in the monastery, but there was a problem going on.

38
00:02:18,000 --> 00:02:23,000
And I, you know, aggravated it and started messing around in places that I shouldn't because I hadn't a clue.

39
00:02:23,000 --> 00:02:25,000
And I called, caused a big uproar.

40
00:02:25,000 --> 00:02:30,000
And this monk took, I went up to Ajentang and his monk started yelling at me in front of Ajentang and said,

41
00:02:30,000 --> 00:02:38,000
who are you? Who are you? Is that what I was saying? Because I was kind of acting a little bit, a little bit arrogant, I suppose.

42
00:02:38,000 --> 00:02:42,000
But I was just saying I didn't want to be involved with it. I said, I don't want to have anything to do with this.

43
00:02:42,000 --> 00:02:48,000
I don't agree with this because they wanted to make me whatever there was all politics.

44
00:02:48,000 --> 00:02:52,000
And from that point on he remembered my name very well.

45
00:02:52,000 --> 00:02:54,000
So I felt horrible.

46
00:02:54,000 --> 00:02:57,000
You know, this is what he remembers people for.

47
00:02:57,000 --> 00:03:01,000
So it's not so bad. But how is the practice that does it have?

48
00:03:01,000 --> 00:03:04,000
What's it like to practice your first meditation course?

49
00:03:04,000 --> 00:03:09,000
I mean, what would you, what should people know going into it?

50
00:03:09,000 --> 00:03:14,000
Well, actually, I went to the first course, it's supposed to be 21 days,

51
00:03:14,000 --> 00:03:17,000
but I came initially for 10 days.

52
00:03:17,000 --> 00:03:19,000
Right, and I wouldn't let you leave.

53
00:03:19,000 --> 00:03:22,000
No, I wouldn't want to leave.

54
00:03:22,000 --> 00:03:26,000
Why did you ask if I could stay longer? Wonderful.

55
00:03:26,000 --> 00:03:30,000
What was it that you learned in 10 days that you wanted to stay on?

56
00:03:30,000 --> 00:03:35,000
Well, I think when I came there, I was quite disillusioned with the world

57
00:03:35,000 --> 00:03:40,000
and finding a way to live in the world.

58
00:03:40,000 --> 00:03:43,000
Sorry, that would make me happy.

59
00:03:43,000 --> 00:03:47,000
And when I came to the meditation practice,

60
00:03:47,000 --> 00:03:51,000
I don't know, after 10 days things started to make sense.

61
00:03:51,000 --> 00:03:55,000
And I started to feel like I was really learning something.

62
00:03:55,000 --> 00:04:00,000
Yeah, that I felt more, I don't know.

63
00:04:00,000 --> 00:04:05,000
Like, I was seeing the truth more in the world than what I saw before.

64
00:04:05,000 --> 00:04:06,000
Wow.

65
00:04:06,000 --> 00:04:08,000
So I wanted to finish the 20 days.

66
00:04:08,000 --> 00:04:10,000
And you did, you finished the course?

67
00:04:10,000 --> 00:04:14,000
Yeah, I finished the course, but I think I said before that I left.

68
00:04:14,000 --> 00:04:19,000
With more of an idea that I changed than I actually had.

69
00:04:19,000 --> 00:04:24,000
So you thought you were enlightened and you went back home to change everyone else?

70
00:04:24,000 --> 00:04:27,000
I thought I knew better than everyone else.

71
00:04:27,000 --> 00:04:33,000
But I think I still had a lot more performance than I realized.

72
00:04:33,000 --> 00:04:36,000
What do you think you gained out of that first experience?

73
00:04:36,000 --> 00:04:41,000
One big thing I gained was the ability to see clearly,

74
00:04:41,000 --> 00:04:45,000
well, not as clearly as I thought, but more clearly,

75
00:04:45,000 --> 00:04:52,000
my actions and my feelings, like the best example is I actually have a phobia of flying.

76
00:04:52,000 --> 00:04:58,000
And after I finished that course, I took two flights to get back to England from Thailand.

77
00:04:58,000 --> 00:05:02,000
Without any drugs, which is the first time I've been able to do that.

78
00:05:02,000 --> 00:05:06,000
Well, before you needed to take drugs, what kind of drugs?

79
00:05:06,000 --> 00:05:14,000
A combination of some lorazapam and beta blockers to stop the adrenaline set to lorazapam.

80
00:05:14,000 --> 00:05:16,000
Beta blockers, wow.

81
00:05:16,000 --> 00:05:19,000
And you were able to do it without them.

82
00:05:19,000 --> 00:05:23,000
I've seen that I was on a plane once and I was just these two women sat down beside me

83
00:05:23,000 --> 00:05:25,000
and I started explaining the meditation.

84
00:05:25,000 --> 00:05:31,000
It's the same, I mean, they didn't take drugs, but just explaining it to them.

85
00:05:31,000 --> 00:05:37,000
And she said, in 35 years, I've never been able to land in a plane without being horrified,

86
00:05:37,000 --> 00:05:42,000
totally mortified or just overwhelmed by fear.

87
00:05:42,000 --> 00:05:45,000
And she said, I wasn't even afraid there.

88
00:05:45,000 --> 00:05:47,000
She gave me a high five.

89
00:05:47,000 --> 00:05:50,000
You rock, she said.

90
00:05:50,000 --> 00:05:52,000
And so I gave her the book and she started practicing.

91
00:05:52,000 --> 00:05:55,000
It's not that I rock, it's the meditation.

92
00:05:55,000 --> 00:05:57,000
That's how she took it.

93
00:05:57,000 --> 00:06:01,000
So for sure, those kind of things are really encouraging to see.

94
00:06:01,000 --> 00:06:03,000
But how do you feel now?

95
00:06:03,000 --> 00:06:07,000
What do you think after all this time, four years now?

96
00:06:07,000 --> 00:06:09,000
What is the change?

97
00:06:09,000 --> 00:06:11,000
Like, how has your life changed?

98
00:06:11,000 --> 00:06:13,000
If at all.

99
00:06:13,000 --> 00:06:20,000
It definitely has changed and I don't think I realized how much until I was talking to someone a few weeks ago

100
00:06:20,000 --> 00:06:24,000
and they asked me to tell them my 10 biggest regrets.

101
00:06:24,000 --> 00:06:27,000
And I said, I don't have any regrets.

102
00:06:27,000 --> 00:06:29,000
Why would I regret anything?

103
00:06:29,000 --> 00:06:33,000
I said, things happen in the past and you have to live with them.

104
00:06:33,000 --> 00:06:36,000
And if I regret it now, it would just make me suffer more.

105
00:06:36,000 --> 00:06:39,000
And they said, people don't think like that.

106
00:06:39,000 --> 00:06:43,000
And I said, surely everyone thinks like that.

107
00:06:43,000 --> 00:06:47,000
But I didn't realize actually some things I learned from meditation.

108
00:06:47,000 --> 00:06:52,000
I wasn't even aware, like, consciously that I learned from meditation.

109
00:06:52,000 --> 00:06:56,000
And did I talk to other people and realize that other people don't think like that.

110
00:06:56,000 --> 00:07:05,000
And other people do make themselves suffer and cling to things and cling to past, wrongs and cling to anger

111
00:07:05,000 --> 00:07:09,000
and make themselves suffer a lot more than they need to.

112
00:07:09,000 --> 00:07:14,000
I think that's an important, that's really a good point.

113
00:07:14,000 --> 00:07:21,000
And it's quite pertinent, quite the case.

114
00:07:21,000 --> 00:07:28,000
You start to forget that you used to be so confused and deluded in these sorts of ways.

115
00:07:28,000 --> 00:07:33,000
And you used to have regrets and think that you should cling to things and so on.

116
00:07:33,000 --> 00:07:36,000
And so you actually get shocked in ways that you didn't get shocked before.

117
00:07:36,000 --> 00:07:38,000
People actually do that kind of thing.

118
00:07:38,000 --> 00:07:41,000
And like, oh yeah, I used to do that kind of thing.

119
00:07:41,000 --> 00:07:46,000
You become in a way a little bit more sensitive.

120
00:07:46,000 --> 00:07:47,000
Okay, well, that's wonderful.

121
00:07:47,000 --> 00:07:52,000
I think that's more than what I was looking for.

122
00:07:52,000 --> 00:08:02,000
And I want to thank you for taking the, you know, humoring me in this and letting everyone see

123
00:08:02,000 --> 00:08:05,000
your smiling face.

124
00:08:05,000 --> 00:08:14,000
And I think everyone can clearly see that you have indeed what you've said is,

125
00:08:14,000 --> 00:08:18,000
I think, a real great encouragement to other people out there.

126
00:08:18,000 --> 00:08:24,000
And they can see that from your words and from your sincerity,

127
00:08:24,000 --> 00:08:29,000
that obviously there is something to be had for meditation practice.

128
00:08:29,000 --> 00:08:32,000
So thank you for your gift to all of us tonight.

129
00:08:32,000 --> 00:08:33,000
You're welcome.

130
00:08:33,000 --> 00:08:35,000
Can we get a picture of her in that one?

131
00:08:35,000 --> 00:08:38,000
I did.

132
00:08:38,000 --> 00:08:41,000
You're good at that.

133
00:08:41,000 --> 00:08:44,000
When I got asked that, I was like, oh no.

134
00:08:44,000 --> 00:08:48,000
Let her say hello to this camera.

135
00:08:48,000 --> 00:08:50,000
Okay.

136
00:08:50,000 --> 00:08:52,000
Another one.

137
00:08:52,000 --> 00:08:53,000
Hello.

138
00:08:53,000 --> 00:08:55,000
Can you repeat what you said?

139
00:08:55,000 --> 00:08:57,000
No, we recorded it.

140
00:08:57,000 --> 00:08:58,000
That's fine.

141
00:08:58,000 --> 00:09:01,000
I think she's an old student from Dyson Tech.

142
00:09:01,000 --> 00:09:02,000
And John Tom.

143
00:09:02,000 --> 00:09:03,000
And John Tom.

144
00:09:03,000 --> 00:09:04,000
And John Tom.

145
00:09:04,000 --> 00:09:05,000
And what, Sunku?

146
00:09:05,000 --> 00:09:29,000
And Sri Lanka.

