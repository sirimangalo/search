1
00:00:00,000 --> 00:00:04,400
Okay, hi and welcome back to Ask a Monk.

2
00:00:04,400 --> 00:00:10,240
Today's question comes from German 1184.

3
00:00:10,240 --> 00:00:16,080
You said in your Ask a Monk series, for people caught up in lust, it is good for them

4
00:00:16,080 --> 00:00:19,600
to contemplate the unpleasant aspects of the body.

5
00:00:19,600 --> 00:00:23,280
What is the proper way to do that?

6
00:00:23,280 --> 00:00:27,440
I did mention this and I think I mentioned as well that for people caught up in anger,

7
00:00:27,440 --> 00:00:30,560
it is good to practice loving kindness.

8
00:00:30,560 --> 00:00:38,600
These two meditations are a part of a set of four meditations called the Arrakakamatana.

9
00:00:38,600 --> 00:00:41,320
Arrakam means to guard.

10
00:00:41,320 --> 00:00:48,320
So the meaning can either be these four meditations are meant to guard the person's mind

11
00:00:48,320 --> 00:00:53,160
or there is something that protects you during the time of the practicing insight meditation

12
00:00:53,160 --> 00:00:57,840
or practicing real meditation to seek clearly.

13
00:00:57,840 --> 00:01:04,800
These are sort of like protection that will help to keep you from getting to the point

14
00:01:04,800 --> 00:01:09,560
where you can't get your mind to the point where you can't practice meditation, so stopping

15
00:01:09,560 --> 00:01:16,360
you from falling into great states of lust, great states of anger and so on.

16
00:01:16,360 --> 00:01:28,560
The four kamatana or meditations are mindfulness of the Buddha, mindfulness of the

17
00:01:28,560 --> 00:01:36,240
loteness or the ugliness of the body, the unpleasant aspects of the body, loving kindness

18
00:01:36,240 --> 00:01:39,560
and death.

19
00:01:39,560 --> 00:01:46,080
So these four meditations they have different qualities and of course these aren't necessary

20
00:01:46,080 --> 00:01:50,040
and I haven't included them in my meditation series but they can be quite helpful, they

21
00:01:50,040 --> 00:01:55,920
are quite helpful and so they are recommended if you're interested in practicing them.

22
00:01:55,920 --> 00:02:02,480
Mindfulness of the Buddha is practical for Buddhist obviously for people who consider the

23
00:02:02,480 --> 00:02:07,800
teachings of the Buddha to be correct, to be useful, to be beneficial.

24
00:02:07,800 --> 00:02:13,320
So thinking about the Buddha as an example, thinking about the Buddha as someone who was

25
00:02:13,320 --> 00:02:20,240
a really good teacher, who was the perfect teacher, someone who was free from defilement.

26
00:02:20,240 --> 00:02:26,720
So we often think of the characteristics of the Buddha, for instance that he had great wisdom.

27
00:02:26,720 --> 00:02:31,640
We think of the incredible wisdom and we read through his teachings and we gain great

28
00:02:31,640 --> 00:02:37,440
faith that yes indeed this is a person who has great wisdom and so we should follow his

29
00:02:37,440 --> 00:02:42,080
teachings because probably they are going to lead to the result that he says.

30
00:02:42,080 --> 00:02:47,520
Because he had great purity, so we read and we learn about the Buddha's purity and also

31
00:02:47,520 --> 00:02:51,800
through practicing his teachings we come to see that his teachings are incredibly pure.

32
00:02:51,800 --> 00:02:58,600
There is nothing dogmatic or there is nothing negative, nothing that is going to lead

33
00:02:58,600 --> 00:03:04,760
us to become brainwashed or whatever, that the Buddha was very open-minded and he was

34
00:03:04,760 --> 00:03:11,080
simply giving something that was of use to people as opposed to looking for students or

35
00:03:11,080 --> 00:03:14,560
trying to become famous or so on.

36
00:03:14,560 --> 00:03:18,640
And the third quality is that he had great compassion because he didn't obviously have

37
00:03:18,640 --> 00:03:23,600
to teach, he could have sat in the forest and meditated and passed away but when he was

38
00:03:23,600 --> 00:03:31,840
asked to teach he decided that he would give up his own peace and quiet to give something

39
00:03:31,840 --> 00:03:32,840
to other people.

40
00:03:32,840 --> 00:03:35,840
There are many qualities of the Buddha and this is obviously something that is more useful

41
00:03:35,840 --> 00:03:43,240
for Buddhists and I would encourage you to read about the Buddha.

42
00:03:43,240 --> 00:03:47,920
One of the easiest ways that Buddhists will do it is to go over the polyverses and think

43
00:03:47,920 --> 00:03:53,920
of consider each word for instance when we say it'd be so Bhakawa means the blessed one

44
00:03:53,920 --> 00:03:57,800
so he's blessed and you learn about why he's blessed and so on.

45
00:03:57,800 --> 00:04:02,480
But I won't go into detail, maybe in the future video I'll go through each of these

46
00:04:02,480 --> 00:04:04,920
individually otherwise I won't have time.

47
00:04:04,920 --> 00:04:14,440
The second meditation is mindfulness of the unpleasant aspects of the body and this is

48
00:04:14,440 --> 00:04:21,320
quite useful to do away with the irrational view that the body is something beautiful.

49
00:04:21,320 --> 00:04:25,480
There's really nothing beautiful about the body at all and it can be objected that there's

50
00:04:25,480 --> 00:04:34,680
nothing ugly about the body either but if you put it on a set of balances our idea that

51
00:04:34,680 --> 00:04:40,560
the body is beautiful is simply due to not paying enough attention to it whereas our idea

52
00:04:40,560 --> 00:04:46,040
that it's unpleasant is actually I would say more based in fact and I'm only saying

53
00:04:46,040 --> 00:04:53,360
that as a comparison if you compare our body to something like gold or diamonds or a flower

54
00:04:53,360 --> 00:05:03,720
say our body actually isn't on a scale of beauty isn't very high and the way you understand

55
00:05:03,720 --> 00:05:07,040
this is by going through the parts of the body so the unpleasant aspects of the body is

56
00:05:07,040 --> 00:05:11,560
actually everything start at the top you start with the hair on your head and ask yourself

57
00:05:11,560 --> 00:05:17,000
you know what is the hair like well it's like grass that's been planted in this skull

58
00:05:17,000 --> 00:05:23,600
and you know what color is it and how does it smell you know if you don't wash it obviously

59
00:05:23,600 --> 00:05:28,360
it's going to be unpleasant so we simply look at the hair and we're going to see that

60
00:05:28,360 --> 00:05:33,160
actually you know the hair is not very beautiful we think oh look at how beautiful that

61
00:05:33,160 --> 00:05:39,240
hair is but if you ever cut it off and put it on a plate you know would it look appetizing

62
00:05:39,240 --> 00:05:43,960
or would it look beautiful you know what we do when we cut our hair off it looks disgusting

63
00:05:43,960 --> 00:05:50,240
and people who keep their hair after they've cut it are often you know with revulsion

64
00:05:50,240 --> 00:05:56,200
and we go through with the hair on our head the hair on our body our nails our teeth

65
00:05:56,200 --> 00:06:06,200
our skin our our flesh our blood our bones our bone marrow our feces our urine our liver

66
00:06:06,200 --> 00:06:10,480
our spleen our heart and so on and we go through all of the parts of the body no normally

67
00:06:10,480 --> 00:06:15,440
monks will do this in Pali so we'll start with ques a loma na karata da jou monks

68
00:06:15,440 --> 00:06:20,960
a naha loo at the team junk and then goes on and we go through 32 parts of the body and

69
00:06:20,960 --> 00:06:24,160
then we'll break them up so we'll start with ques a ques a ques a ques a ques a which means

70
00:06:24,160 --> 00:06:28,560
hair of the hand and we'll just repeat that over and over to ourselves contemplating the

71
00:06:28,560 --> 00:06:32,400
hair on the head and much the same way as we practice insight meditation except now we're

72
00:06:32,400 --> 00:06:42,120
focusing on a conceptual piece of reality you know the hair or the bones or the skin or

73
00:06:42,120 --> 00:06:47,240
whatever and that does help you see it clearly you don't have to say this is disgusting

74
00:06:47,240 --> 00:06:51,520
you just look at it and you'll see well actually you know it's not very beautiful you

75
00:06:51,520 --> 00:06:56,600
don't have to you know agree that it's disgusting but it's certainly not the wonderful

76
00:06:56,600 --> 00:07:01,680
incredible attractive thing that we think it is and that helps to bring us back to our

77
00:07:01,680 --> 00:07:07,320
more rational state of mind in this regard the third meditation is loving kindness and

78
00:07:07,320 --> 00:07:13,880
this is very useful after we meditate you know when once you finish meditation you should

79
00:07:13,880 --> 00:07:19,080
use the power and the strength that you gain and the clarity of mind that you gain from

80
00:07:19,080 --> 00:07:25,800
meditation use it to express your appreciation for all their beings and to clear up a lot

81
00:07:25,800 --> 00:07:29,400
of the problems that we might have with other people this is something that really helps

82
00:07:29,400 --> 00:07:35,720
with your insight meditation helps with helps us to meditate it helps us to straighten

83
00:07:35,720 --> 00:07:42,240
out a lot of the the crookedness that we often have in our minds wishing first for our

84
00:07:42,240 --> 00:07:48,040
parents to be happy then for our relatives for our family for the people nearby for the

85
00:07:48,040 --> 00:07:54,320
people in this area in this city in this world and go from from you know my house to my

86
00:07:54,320 --> 00:08:01,200
city to my country to the whole world to the whole universe start with humans and animals

87
00:08:01,200 --> 00:08:08,320
angels God whatever to all beings extending it out one by one by one you can do that for

88
00:08:08,320 --> 00:08:12,960
like five minutes after you finish meditating or even just a minute or two is fine and

89
00:08:12,960 --> 00:08:21,080
that's useful according you know technically useful in terms of getting rid of anger

90
00:08:21,080 --> 00:08:27,160
it's in a lot in a broader context it's useful for clearing up our relationships and

91
00:08:27,160 --> 00:08:32,160
making it straightening our mind out the fourth one is mindfulness of death and this is quite

92
00:08:32,160 --> 00:08:39,880
useful to help us to give it get a sense of urgency you know to help us wake up to the

93
00:08:39,880 --> 00:08:44,240
fact that we can't just you know go on with our lives and live our lives as though we're

94
00:08:44,240 --> 00:08:48,920
dead or as though there were no tomorrow or so on because eventually there's the

95
00:08:48,920 --> 00:08:52,840
right day of reckoning and that's our death and then our whole life flashes before our

96
00:08:52,840 --> 00:08:58,520
eyes and we have to you know face up and whatever is strong in our minds we're going to

97
00:08:58,520 --> 00:09:02,040
cling to it and we're going to we're going to follow after that and we'll have to be

98
00:09:02,040 --> 00:09:09,200
born again if it's a bad thing we'll be born in a bad rebirth the way we would be mindful

99
00:09:09,200 --> 00:09:16,600
of death you know there's there's many ways that the the technical method is to simply

100
00:09:16,600 --> 00:09:25,240
save yourself that life is uncertain death is certain life is not sure death is sure life

101
00:09:25,240 --> 00:09:31,520
has death as its end all beings have to die I too will have to die one day but you can

102
00:09:31,520 --> 00:09:36,040
adapt this and you can you know think of I always used to when I was in a plane I would

103
00:09:36,040 --> 00:09:40,240
think about the plane crashing and I would examine my own emotions because obviously

104
00:09:40,240 --> 00:09:45,880
that would be something very traumatic for me and so on so you you can use these and you

105
00:09:45,880 --> 00:09:49,920
should use all of these these are all beneficial they're all meditations which can be

106
00:09:49,920 --> 00:09:54,640
used in in a accompaniment to the inside meditation okay so thanks for the question

107
00:09:54,640 --> 00:10:11,560
hope that helps

