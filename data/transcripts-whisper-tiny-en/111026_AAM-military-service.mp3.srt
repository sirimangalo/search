1
00:00:00,000 --> 00:00:07,880
How do Buddhists view military service?

2
00:00:07,880 --> 00:00:11,800
This is interesting because it actually relates to the monk question I think or the answer

3
00:00:11,800 --> 00:00:13,840
does.

4
00:00:13,840 --> 00:00:18,680
Because the word military service has come to mean so many different things, right?

5
00:00:18,680 --> 00:00:23,520
You don't have to necessarily kill people to be in the military.

6
00:00:23,520 --> 00:00:28,520
Military can stop floods, for example, right?

7
00:00:28,520 --> 00:00:33,480
If the military is put to use stopping floods, that's a great thing.

8
00:00:33,480 --> 00:00:37,720
You took away all the weapons from the military, took away all of their weapons and gave

9
00:00:37,720 --> 00:00:51,840
them shovels and what sandbags, then I think it would be a good thing.

10
00:00:51,840 --> 00:00:59,520
I have a standing army that looks after people's welfare.

11
00:00:59,520 --> 00:01:12,560
But no, obviously the military is for the purpose of doing battle, causing harm, hurting

12
00:01:12,560 --> 00:01:20,760
other beings, maybe in defense, but a person who undertakes who enters the military

13
00:01:20,760 --> 00:01:26,600
is doing it with the, I believe, with the understanding and the intention that they are

14
00:01:26,600 --> 00:01:37,280
going to bring violence to other beings, either, you know, and as I said, they can think

15
00:01:37,280 --> 00:01:48,480
of it as in defense, but it's a violent profession by its very nature.

16
00:01:48,480 --> 00:01:53,600
I don't know, I guess my question would be how, in what way would you think that military

17
00:01:53,600 --> 00:01:59,560
service would be a positive thing?

18
00:01:59,560 --> 00:02:06,200
Or is there any way that you can think of it as not being a negative thing, I suppose?

19
00:02:06,200 --> 00:02:20,800
Okay, my job would be to help children of foreign countries not hate Americans, but the

20
00:02:20,800 --> 00:02:25,000
question is, if there were a war, would you be asked to pick up a gun and fight?

21
00:02:25,000 --> 00:02:30,480
I mean, isn't, I don't really know how, that's what I said, the military has become something

22
00:02:30,480 --> 00:02:36,880
quite a bit more than just guys with guns, but I still think you have, you're given a

23
00:02:36,880 --> 00:02:44,560
duty to fight, are you not?

24
00:02:44,560 --> 00:02:50,160
There are many issues here because I know that you can, being in the American military,

25
00:02:50,160 --> 00:02:57,200
for example, can be a great way, theoretically, to help people.

26
00:02:57,200 --> 00:03:06,960
You're given a salary, you're given a lot of equipment and resources, and yeah, joining

27
00:03:06,960 --> 00:03:12,320
other organizations might be a great thing, but there's something to be said about the

28
00:03:12,320 --> 00:03:15,080
clout of the American military in that sense.

29
00:03:15,080 --> 00:03:20,520
The problem is that I don't understand it fully, but as I understand, when you join

30
00:03:20,520 --> 00:03:29,600
the military, no, I guess that's not true, no, if you're intelligence and in the Air Force,

31
00:03:29,600 --> 00:03:34,720
you're not involved with killing, more like a desk show, right?

32
00:03:34,720 --> 00:03:40,720
Well, then I guess there's the moral question, is there a moral question of joining an

33
00:03:40,720 --> 00:03:52,680
organization that at its very nature is for the purpose of, I don't want to say, the

34
00:03:52,680 --> 00:04:04,680
purpose is to engage in combat or to, well, it's a military.

35
00:04:04,680 --> 00:04:09,600
Are there ethical, are there ethical, ethical problems there?

36
00:04:09,600 --> 00:04:13,720
I guess I'm not, I don't have strong feelings about this.

37
00:04:13,720 --> 00:04:21,560
You take your job, and if your job is for the purpose of helping other people, and if

38
00:04:21,560 --> 00:04:26,520
through your job, you can help other people, then power to you.

39
00:04:26,520 --> 00:04:32,960
Even if your job doesn't help other people, I'm not of the bent that you have to be

40
00:04:32,960 --> 00:04:40,920
concerned with anything larger than doing your own task, and I know people are, and there's

41
00:04:40,920 --> 00:04:46,280
a lot of people who have this social conscience that they feel like they shouldn't work

42
00:04:46,280 --> 00:04:49,040
for an organization.

43
00:04:49,040 --> 00:04:53,600
This is the same debate about vegetarianism, right?

44
00:04:53,600 --> 00:04:58,200
You can't eat meat because you're contributing to the killing of animals.

45
00:04:58,200 --> 00:05:07,840
You can't use Gillette razors because you're contributing to the cruelty towards animals.

46
00:05:07,840 --> 00:05:13,800
You can't buy certain stocks, you can buy certain products, you can't, you know, don't

47
00:05:13,800 --> 00:05:18,560
buy Nike, don't buy this, don't buy that.

48
00:05:18,560 --> 00:05:26,040
These sort of things, and I don't buy it, it seems to put it in buy it, and there's

49
00:05:26,040 --> 00:05:30,760
even, we have this wonderful example that we always tell this woman, whose husband was

50
00:05:30,760 --> 00:05:32,840
a hunter.

51
00:05:32,840 --> 00:05:36,880
When she was young, she went to practice meditation, listened to the Buddha's teaching,

52
00:05:36,880 --> 00:05:46,040
and became a sotapana, which means she was, she was in a sense enlightened.

53
00:05:46,040 --> 00:05:49,960
And then she got married because in India they often, their marriage would have been

54
00:05:49,960 --> 00:05:56,600
decided for them, so she was married off to this man, who was a hunter.

55
00:05:56,600 --> 00:06:03,680
And every day she would clean his traps, clean the blood and the guts and the hair off

56
00:06:03,680 --> 00:06:07,880
of these traps, and put them out on the table for him.

57
00:06:07,880 --> 00:06:11,080
And every day he would go out and kill animals.

58
00:06:11,080 --> 00:06:16,240
And so they asked, how is this possible that she could do this?

59
00:06:16,240 --> 00:06:22,280
And the Buddha said, it's because he said, it's like if your hand has no cut, if you

60
00:06:22,280 --> 00:06:26,040
have no wound on your hand, you can handle poison.

61
00:06:26,040 --> 00:06:31,040
But if you have a wound on your hand, you can't handle poison.

62
00:06:31,040 --> 00:06:41,760
He says a little more poetically than that, but the point being that when a person has no

63
00:06:41,760 --> 00:06:52,480
bad intentions, the act of cleaning, cleaning blood and gore off of these killing machines,

64
00:06:52,480 --> 00:06:56,920
even to that extent, is not considered to be immoral.

65
00:06:56,920 --> 00:06:58,680
Because it's not, it's the same as eating meat.

66
00:06:58,680 --> 00:07:02,320
The act itself is, is never immoral.

67
00:07:02,320 --> 00:07:05,760
This is the Buddha actually taught against the idea of karma.

68
00:07:05,760 --> 00:07:10,280
He said karma is not the, not the important thing, it's the intention.

69
00:07:10,280 --> 00:07:14,640
So the woman is intending, oh here I'm helping my husband making him happy and I'm doing

70
00:07:14,640 --> 00:07:15,640
my job.

71
00:07:15,640 --> 00:07:19,560
You know, if I don't, he might beat me kind of thing.

72
00:07:19,560 --> 00:07:26,520
That might have had a part in it, because there's some horrible things to go on out there.

73
00:07:26,520 --> 00:07:33,000
But even, you know, he's a hunter, he's not the most cultured of beings, but even without

74
00:07:33,000 --> 00:07:42,320
that, just the idea that she's doing her job and doing what is her way and for a person

75
00:07:42,320 --> 00:07:50,120
who eats meat, they're just eating the food in order to gain the benefit.

76
00:07:50,120 --> 00:07:54,880
This is false, this is in line I think with, with, this is certainly in line with my understanding

77
00:07:54,880 --> 00:07:56,480
of the Buddha's teaching.

78
00:07:56,480 --> 00:08:08,320
Because we're dealing in terms of experience.

79
00:08:08,320 --> 00:08:13,520
Don't some monks practice some military aspects like Shaolin, I'll never live this one down.

80
00:08:13,520 --> 00:08:18,480
No monks, Buddhist monks don't practice Shaolin, Shaolin monks practice Shaolin.

81
00:08:18,480 --> 00:08:25,040
I'm sorry, they call themselves Buddhists, but in what way does, does, does Kung Fu have

82
00:08:25,040 --> 00:08:28,600
to do it, the Buddha's teaching?

83
00:08:28,600 --> 00:08:36,080
It's well to each their own, but no, Shaolin monks are a very specific type of monk.

84
00:08:36,080 --> 00:08:42,200
And I don't know, I guess I just can't answer that because it's so different from what

85
00:08:42,200 --> 00:08:43,200
I do.

86
00:08:43,200 --> 00:08:51,080
I used to practice karate, but that was a long time ago, the karate isn't Kung Fu, but it's

87
00:08:51,080 --> 00:08:54,000
not a part of the Buddha's teaching for sure.

88
00:08:54,000 --> 00:08:55,000
Those monks are doing it.

89
00:08:55,000 --> 00:08:59,320
I mean, there's a whole history there of how it came about because there was war in

90
00:08:59,320 --> 00:09:04,520
the monks had to defend themselves or whatever, not sure how it all came about.

91
00:09:04,520 --> 00:09:10,920
Anyway, the question on joining the military, I say, as I said, take it as your job because

92
00:09:10,920 --> 00:09:15,600
the American military is such a huge organization and so many people don't, aren't involved

93
00:09:15,600 --> 00:09:19,080
with the killing part of it.

94
00:09:19,080 --> 00:09:26,360
But you have to be fairly careful there as well because it's, you know, if you are put

95
00:09:26,360 --> 00:09:39,400
in a position where you have to be involved with or encourage the use of force or the distribution

96
00:09:39,400 --> 00:09:46,840
of weapons, for example, it's certainly something that you have to take into consideration.

97
00:09:46,840 --> 00:09:50,760
Be careful that you don't somehow get conscripted and, you know, your soldier going

98
00:09:50,760 --> 00:09:51,760
fight.

99
00:09:51,760 --> 00:09:58,360
It's kind of a thing, but, you know, we're living in hard times if you have a chance to

100
00:09:58,360 --> 00:10:03,160
help people then power to you and go out there and help because actually in Canada,

101
00:10:03,160 --> 00:10:08,560
the military is mostly involved in peacekeeping, helping to bring peace, or they were,

102
00:10:08,560 --> 00:10:23,560
I don't know what it's like, anyway, military.

