1
00:00:00,000 --> 00:00:09,000
Hi. In this video, I will be teaching you how to practice meditation on love.

2
00:00:09,000 --> 00:00:14,000
First, we have to understand what we mean by the word love.

3
00:00:14,000 --> 00:00:21,000
When we meditate on love, it doesn't mean we cling to people or want them to make us happy.

4
00:00:21,000 --> 00:00:26,000
When we meditate on love, it means we want other people to be happy.

5
00:00:26,000 --> 00:00:30,000
Even people who may not love us back.

6
00:00:30,000 --> 00:00:34,000
So, when you like people because they do things that make you happy,

7
00:00:34,000 --> 00:00:41,000
or when you really like some kind of food, this isn't what we mean by love.

8
00:00:41,000 --> 00:00:47,000
But when you want your parents, your friends, even the whole world to be happy,

9
00:00:47,000 --> 00:00:49,000
this is true love.

10
00:00:49,000 --> 00:00:58,000
And this is what we will be focusing on in this exercise.

11
00:00:58,000 --> 00:01:03,000
Love is an important meditation practice because when we wish for other people to be happy,

12
00:01:03,000 --> 00:01:06,000
it makes us happy as well.

13
00:01:06,000 --> 00:01:11,000
If we always want to hurt other people, or if we wish for them to suffer,

14
00:01:11,000 --> 00:01:14,000
we will be terribly unhappy ourselves.

15
00:01:14,000 --> 00:01:21,000
So, the best way to be happy is to always wish for the whole world to be happy as well.

16
00:01:21,000 --> 00:01:23,000
Ready?

17
00:01:23,000 --> 00:01:25,000
Here's how we'll do it.

18
00:01:25,000 --> 00:01:29,000
First, sit down either on the floor or in a chair,

19
00:01:29,000 --> 00:01:35,000
or even lie down if you like, and start by sending love to yourself.

20
00:01:35,000 --> 00:01:39,000
Why do we send love to ourselves first?

21
00:01:39,000 --> 00:01:42,000
Because of course, we want ourselves to be happy.

22
00:01:42,000 --> 00:01:49,000
So wishing it for ourselves would be the easiest thing in the world.

23
00:01:49,000 --> 00:01:55,000
When you're ready, start by saying to yourself in your mind, may I be happy.

24
00:01:55,000 --> 00:02:02,000
You can just repeat that to yourself again and again, until you really feel like you wish for yourself to be happy.

25
00:02:02,000 --> 00:02:06,000
Not angry at yourself or distracted by other things.

26
00:02:06,000 --> 00:02:13,000
Let's try it now.

27
00:02:36,000 --> 00:03:03,000
Once you can send love to yourself, you can move on to sending love to other people.

28
00:03:03,000 --> 00:03:09,000
Start with someone you already love very much, maybe your mother or your father,

29
00:03:09,000 --> 00:03:14,000
or first one than the other, maybe a friend or relative that is close to you,

30
00:03:14,000 --> 00:03:21,000
who it is doesn't matter, as long as you already really want them to be happy.

31
00:03:21,000 --> 00:03:25,000
Say to yourself, may they be happy.

32
00:03:25,000 --> 00:03:32,000
You can use their name, for example, may my mother be happy, may John be happy, and so on.

33
00:03:32,000 --> 00:03:40,000
You can also change or add to the words that you use, like may they have peace and calm,

34
00:03:40,000 --> 00:03:44,000
may they be healthy and secure, however you like.

35
00:03:44,000 --> 00:03:47,000
The most important thing is that you believe it.

36
00:03:47,000 --> 00:03:54,000
You can practice repeating the words again and again, until you really feel like you want that person to be happy.

37
00:03:54,000 --> 00:04:03,000
Try it now.

38
00:04:24,000 --> 00:04:43,000
Thank you.

39
00:04:55,000 --> 00:05:08,000
Next, you can move on to sending love to someone who you don't already love.

40
00:05:08,000 --> 00:05:12,000
Try to pick someone who you don't really have feelings for at all.

41
00:05:12,000 --> 00:05:18,000
Maybe someone in your neighborhood or at school, like a neighbor or a teacher.

42
00:05:18,000 --> 00:05:25,000
Don't pick someone who you don't like, but try not to pick someone who you already like either.

43
00:05:25,000 --> 00:05:30,000
Once you have that person in mind, make a wish in your mind for that person to be happy,

44
00:05:30,000 --> 00:05:34,000
just as you did with a person who you already love.

45
00:05:34,000 --> 00:05:42,000
This is a little more of a challenge because you have to develop love for someone who you've maybe never loved before.

46
00:05:42,000 --> 00:05:50,000
If you can do this, you are sure to become a special person in the world, someone who is able to love people without judging them.

47
00:05:50,000 --> 00:05:59,000
Try again and again, and if it gets too hard, go back to thinking about someone who you already love and wishing for them to be happy.

48
00:05:59,000 --> 00:06:07,000
Then, when you can do that again, try again with a person who you don't love until you are able to really wish for them to be happy too.

49
00:06:07,000 --> 00:06:12,000
Try now.

50
00:07:37,000 --> 00:07:45,000
Finally, try to think of someone who you are unhappy with or someone who is unhappy with you.

51
00:07:45,000 --> 00:07:54,000
Maybe it's someone in your family or one of your friends who made you sad, or maybe it's someone who you've always been enemies with.

52
00:07:54,000 --> 00:07:59,000
This is the hardest part of the exercise, but try to send them love as well.

53
00:07:59,000 --> 00:08:12,000
May they be happy. Think that to yourself again and again until you can let go of the anger and upset you have towards them and are actually able to love them as well.

54
00:08:12,000 --> 00:08:22,000
Again, if it gets too hard, go back to thinking about someone who you already love or someone who you don't have any feelings about until you are able to feel love again.

55
00:08:22,000 --> 00:08:30,000
Then, come back and try with the person who you are unhappy with. Try it now.

56
00:09:22,000 --> 00:09:45,000
Great. This is an easy way to change how you look at people.

57
00:09:45,000 --> 00:09:51,000
Whenever you have problems with others, just sit down and start to feel love like this until you can love that person as well.

58
00:09:51,000 --> 00:10:00,000
Once you get good at sending love to one person at a time, you can try sending it to the whole world all at once.

59
00:10:00,000 --> 00:10:17,000
Ready? Let's try. Start with everyone in your home. Say to yourself, may all the people in this building be happy, thinking about all those people at once.

60
00:10:17,000 --> 00:10:23,000
Thank you.

61
00:10:47,000 --> 00:11:01,000
Next, send love to all the people in your town or city. May all the people in this town be happy.

62
00:11:17,000 --> 00:11:46,000
Then, think of all the people in your entire country. May all people in this country be happy.

63
00:11:46,000 --> 00:11:54,000
Thank you.

64
00:12:16,000 --> 00:12:30,000
Finally, think of all the people in the whole world all at once and say to yourself, may all the people in the world be happy.

65
00:12:46,000 --> 00:13:05,000
Thank you.

66
00:13:16,000 --> 00:13:42,000
Great.

67
00:13:42,000 --> 00:13:48,000
If you've made it this far, you should be able to cultivate love for just about anyone.

68
00:13:48,000 --> 00:13:56,000
I hope you can see how loving others makes you happier, more peaceful and more content person than ever before.

69
00:13:56,000 --> 00:14:07,000
If you practice meditating on love often, you will see that it helps you make friends with everyone you meet and be free from anger and hatred towards anyone.

70
00:14:07,000 --> 00:14:16,000
Thanks for watching and don't forget to practice it on your own or come back and watch this video if you need a reminder. May you be happy.

