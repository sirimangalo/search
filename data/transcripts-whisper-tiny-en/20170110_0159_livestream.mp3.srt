1
00:00:00,000 --> 00:00:26,800
Hello, test, hello, test, good evening everyone, welcome to our evening broadcast.

2
00:00:26,800 --> 00:00:54,000
Today is the day I wanted to look at, I'm going to talk about the idea of what it is we need to know,

3
00:00:54,000 --> 00:01:00,800
what we need to learn from the practice. Right, we talk about

4
00:01:00,800 --> 00:01:08,000
Satyapatana is for the purpose of attaining Bhipastana, for the purpose of gaining insight.

5
00:01:08,000 --> 00:01:13,000
It's for the purpose of gaining Jana.

6
00:01:13,000 --> 00:01:21,000
Jana, Muddapadi, Rija, Uddapadi, Pandya, Uddapadi, Arlo, Kodapadi.

7
00:01:21,000 --> 00:01:35,000
It's for gaining wisdom.

8
00:01:35,000 --> 00:01:41,000
I think a bit of a misunderstanding or misconception of that is that

9
00:01:41,000 --> 00:01:47,000
some of there are many things that we have to learn or it's quite complex.

10
00:01:47,000 --> 00:01:50,000
A sense that we're not learning what we have to learn.

11
00:01:50,000 --> 00:01:52,000
You hear about the wisdom, right?

12
00:01:52,000 --> 00:01:55,000
And you think I'm practicing for a long time.

13
00:01:55,000 --> 00:02:02,000
I don't see any wisdom yet, because you have this idea that wisdom is something lofty.

14
00:02:02,000 --> 00:02:12,000
If wisdom is something perhaps intellectual or complicated, it's actually quite simple.

15
00:02:12,000 --> 00:02:26,000
It's somewhat sometimes deceivingly simple anyway.

16
00:02:26,000 --> 00:02:36,000
When you say rising, when you watch the stomach rise and you say rising,

17
00:02:36,000 --> 00:02:41,000
when you know that the stomach's rising, that's wisdom.

18
00:02:41,000 --> 00:02:45,000
Anything? That's it?

19
00:02:45,000 --> 00:02:48,000
Why am I here?

20
00:02:48,000 --> 00:02:52,000
I could do this at home.

21
00:02:52,000 --> 00:02:59,000
It's a very important statement. When you know that the stomach is rising, that's wisdom.

22
00:02:59,000 --> 00:03:03,000
There's a problem is that we're forgetful.

23
00:03:03,000 --> 00:03:07,000
We don't remember.

24
00:03:07,000 --> 00:03:09,000
We aren't wise, we're unwise.

25
00:03:09,000 --> 00:03:12,000
We have Ayoni Solmanasika.

26
00:03:12,000 --> 00:03:18,000
Instead of just knowing that the stomach is rising or knowing that seeing is seeing,

27
00:03:18,000 --> 00:03:22,000
we know a lot more, we know too much.

28
00:03:22,000 --> 00:03:24,000
Too much and not enough.

29
00:03:24,000 --> 00:03:27,000
Everything and nothing.

30
00:03:27,000 --> 00:03:33,000
We know all the details about so many things.

31
00:03:33,000 --> 00:03:39,000
As a human species seem bent on learning everything.

32
00:03:39,000 --> 00:03:46,000
Collecting as much useless information as we can mostly use this.

33
00:03:46,000 --> 00:03:51,000
And by useless, useless, I mean by Buddhist standards.

34
00:03:51,000 --> 00:03:55,000
Use this in a sense that they don't actually make us happy the things we learn.

35
00:03:55,000 --> 00:03:58,000
They don't actually bring peace.

36
00:03:58,000 --> 00:04:06,000
How much learning we need just to do some job that ends up fulfilling some function

37
00:04:06,000 --> 00:04:18,000
that does very little to promote well-being or learning skills that in the end are

38
00:04:18,000 --> 00:04:24,000
such a round about way of promoting any sort of goodness if at all.

39
00:04:24,000 --> 00:04:30,000
It didn't fact often promote unwholesumness.

40
00:04:30,000 --> 00:04:35,000
How many skills, how much learning, how much knowledge is there out there that ends up being totally useless?

41
00:04:35,000 --> 00:04:38,000
Or worse, harmful?

42
00:04:38,000 --> 00:04:59,000
When I was young, we used to play these computer games back before they had online computer games.

43
00:04:59,000 --> 00:05:07,000
We used to play Starcraft, Warcraft.

44
00:05:07,000 --> 00:05:13,000
And you could spend hours where we played these battle simulations.

45
00:05:13,000 --> 00:05:15,000
We could spend hours.

46
00:05:15,000 --> 00:05:16,000
We would spend all night.

47
00:05:16,000 --> 00:05:20,000
We would stay awake all night playing.

48
00:05:20,000 --> 00:05:25,000
I'm sure to some of you, this all sounds like, yeah, I mean, this is what we do now.

49
00:05:25,000 --> 00:05:29,000
I think this is what people do now.

50
00:05:29,000 --> 00:05:38,000
But you learn so much and you get these skills, wonderful skills and you get really good at these games

51
00:05:38,000 --> 00:05:53,000
or sports or acting or mathematics, physics, all these wonderful things

52
00:05:53,000 --> 00:06:03,000
were able to build computers and spacecraft.

53
00:06:03,000 --> 00:06:05,000
So much learning.

54
00:06:05,000 --> 00:06:07,000
Even languages.

55
00:06:07,000 --> 00:06:13,000
How much time we have to spend learning languages just to talk to each other.

56
00:06:13,000 --> 00:06:16,000
And then we die and forget it all.

57
00:06:16,000 --> 00:06:23,000
I lose the languages enough to gain them all again.

58
00:06:23,000 --> 00:06:28,000
So no, the knowledge that we hope to gain from meditation is quite different.

59
00:06:28,000 --> 00:06:35,000
We hope to come to know that, oh, right, yes, when the stomach rises, that's rising.

60
00:06:35,000 --> 00:06:43,000
It's not good, it's not bad, it's not me, it's not mine.

61
00:06:43,000 --> 00:06:49,000
But to be a little more precise, if you like to get some clear up the doubt,

62
00:06:49,000 --> 00:06:52,000
it's easy to find doubt in regards to this.

63
00:06:52,000 --> 00:06:56,000
What is the wisdom? What am I trying to learn?

64
00:06:56,000 --> 00:07:00,000
One time I joined Hong said to me that there are four things.

65
00:07:00,000 --> 00:07:06,000
There's only four things you have to learn.

66
00:07:06,000 --> 00:07:13,000
The first one is called Namarupa.

67
00:07:13,000 --> 00:07:16,000
Number one.

68
00:07:16,000 --> 00:07:18,000
Number two.

69
00:07:18,000 --> 00:07:21,000
Tila Kana.

70
00:07:21,000 --> 00:07:23,000
Number three.

71
00:07:23,000 --> 00:07:24,000
Maga.

72
00:07:24,000 --> 00:07:25,000
Number four.

73
00:07:25,000 --> 00:07:26,000
Pala.

74
00:07:26,000 --> 00:07:32,000
These are the poly word.

75
00:07:32,000 --> 00:07:34,000
Namarupa.

76
00:07:34,000 --> 00:07:38,000
Namam means the immaterial.

77
00:07:38,000 --> 00:07:43,000
Rupa means material.

78
00:07:43,000 --> 00:07:45,000
There are only two aspects to reality.

79
00:07:45,000 --> 00:07:50,000
The first thing you have to come to understand, if you want to practice meditation,

80
00:07:50,000 --> 00:07:53,000
is that there's only two parts to reality.

81
00:07:53,000 --> 00:07:56,000
There's the immaterial and there's the material.

82
00:07:56,000 --> 00:08:01,000
The material is the physical aspect of experience.

83
00:08:01,000 --> 00:08:07,000
When you walk, you feel the tension or the hardness or the softness and the heat and the cold.

84
00:08:07,000 --> 00:08:15,000
When you sit, you feel the tension and so on.

85
00:08:15,000 --> 00:08:21,000
Movements of the body, sensations in the body.

86
00:08:21,000 --> 00:08:24,000
These are material.

87
00:08:24,000 --> 00:08:30,000
When you touch something, the touching, the feeling of hardness or softness, that's material.

88
00:08:30,000 --> 00:08:35,000
The immaterial is the knowing of it.

89
00:08:35,000 --> 00:08:44,000
The knowing of it and all the concomitant qualities of the knowing.

90
00:08:44,000 --> 00:08:50,000
When you like and dislike and all that, that's all material.

91
00:08:50,000 --> 00:08:51,000
That's reality.

92
00:08:51,000 --> 00:08:55,000
That's what's real.

93
00:08:55,000 --> 00:08:58,000
The first thing you have to learn is what's real.

94
00:08:58,000 --> 00:09:04,000
The first step in meditation, you can progress until you're clear.

95
00:09:04,000 --> 00:09:19,000
When you move the right foot, there's an experience of the pressure and the cold or the wind and so on.

96
00:09:19,000 --> 00:09:26,000
That arises and that's physical and there's the knowing of it as well.

97
00:09:26,000 --> 00:09:29,000
When the stomach rises, your mind knows.

98
00:09:29,000 --> 00:09:37,000
There's the rising and there's the rising movement and there's the mind, but there's no stomach.

99
00:09:37,000 --> 00:09:41,000
The stomach is all produced in the brain, in the mind.

100
00:09:41,000 --> 00:09:43,000
Without the brain, the mind.

101
00:09:43,000 --> 00:09:46,000
The brain also doesn't exist.

102
00:09:46,000 --> 00:09:49,000
These are concepts we give rise to.

103
00:09:49,000 --> 00:09:56,000
If you think of the brain or the brain doesn't exist, what do you mean the brain doesn't exist?

104
00:09:56,000 --> 00:09:58,000
Besides, as a concept, there's no existence.

105
00:09:58,000 --> 00:10:00,000
We have the brain that's actually connected.

106
00:10:00,000 --> 00:10:03,000
It's just a part of the body.

107
00:10:03,000 --> 00:10:08,000
It extends into the central nervous system.

108
00:10:08,000 --> 00:10:10,000
Such thing is the brain.

109
00:10:10,000 --> 00:10:18,000
It's just a concept that we apply to certain mental and physical.

110
00:10:18,000 --> 00:10:23,000
There's no physical aspects of experience.

111
00:10:23,000 --> 00:10:25,000
There's no body.

112
00:10:25,000 --> 00:10:26,000
There's no room.

113
00:10:26,000 --> 00:10:28,000
We're not sitting in a room.

114
00:10:28,000 --> 00:10:30,000
There's not even any space.

115
00:10:30,000 --> 00:10:35,000
What space is interesting because space is only a part of matter.

116
00:10:35,000 --> 00:10:37,000
It doesn't actually exist.

117
00:10:37,000 --> 00:10:40,000
It only comes to being because of matter.

118
00:10:40,000 --> 00:10:43,000
It's a part, a derived quality of matter.

119
00:10:43,000 --> 00:10:48,000
It does exist, but only in regards to matter.

120
00:10:48,000 --> 00:10:54,000
The mind doesn't take up space.

121
00:10:54,000 --> 00:10:56,000
But not to get too complicated.

122
00:10:56,000 --> 00:11:01,000
Very simply, the only thing you have to know is that body and mind.

123
00:11:01,000 --> 00:11:04,000
Material immaterial.

124
00:11:04,000 --> 00:11:09,000
Reality is only made up of these two things.

125
00:11:09,000 --> 00:11:12,000
Reality is made up of experiences.

126
00:11:12,000 --> 00:11:17,000
When you see something, there's the physical light and there's the eye.

127
00:11:17,000 --> 00:11:20,000
Then there's the knowing of it.

128
00:11:20,000 --> 00:11:25,000
Sometimes with your eyes open, you don't actually see something in front of you.

129
00:11:25,000 --> 00:11:28,000
Your mind is somewhere else, even though your eyes are open.

130
00:11:28,000 --> 00:11:30,000
The mind isn't there.

131
00:11:30,000 --> 00:11:35,000
Sound, hearing requires sound, the physical ear.

132
00:11:35,000 --> 00:11:37,000
It also requires the mind.

133
00:11:37,000 --> 00:11:41,000
Of course, sometimes you're absorbed in something and someone calls your name and you don't hear it.

134
00:11:41,000 --> 00:11:43,000
Mind.

135
00:11:43,000 --> 00:11:45,000
That's the immaterial.

136
00:11:45,000 --> 00:11:47,000
Two things required for experience.

137
00:11:47,000 --> 00:11:51,000
For seeing, hearing, smelling, tasting, feeling, thinking.

138
00:11:51,000 --> 00:11:53,000
You need immaterial immaterial.

139
00:11:53,000 --> 00:11:56,000
This is the first thing you have to learn.

140
00:11:56,000 --> 00:12:02,000
The second thing we have to learn is the three characteristics.

141
00:12:02,000 --> 00:12:04,000
The three characteristics.

142
00:12:04,000 --> 00:12:05,000
Tila kala.

143
00:12:05,000 --> 00:12:06,000
Deep means three.

144
00:12:06,000 --> 00:12:08,000
La kala means characteristics.

145
00:12:08,000 --> 00:12:11,000
They're also called samanyalakana.

146
00:12:11,000 --> 00:12:19,000
Samanyam means universal or universal basically.

147
00:12:19,000 --> 00:12:25,000
Common to all means it's the three characteristics.

148
00:12:25,000 --> 00:12:28,000
Just about everything.

149
00:12:28,000 --> 00:12:35,000
The three characteristics, of course, are impermanence, instability, uncertainty,

150
00:12:35,000 --> 00:12:43,000
unreliability, unpredictability, all that.

151
00:12:43,000 --> 00:12:54,000
Suffering, stress, dissatisfaction, inability to satisfy unhappiness,

152
00:12:54,000 --> 00:13:16,000
basically not being happiness, and non-self, uncontrollability, instability.

153
00:13:16,000 --> 00:13:18,000
This is the second thing you have to learn.

154
00:13:18,000 --> 00:13:22,000
This is really what you start to see through the practice.

155
00:13:22,000 --> 00:13:28,000
You start to readjust your understanding about things.

156
00:13:28,000 --> 00:13:33,000
You see for the first time how much suffering we're causing ourselves.

157
00:13:33,000 --> 00:13:40,000
And you start to see, to see the mistakes you're making.

158
00:13:40,000 --> 00:13:45,000
You're making a mistake when you cling when you want, when you expect.

159
00:13:45,000 --> 00:13:53,000
Because reality is, in constant, unpredictable, unsatisfying,

160
00:13:53,000 --> 00:14:02,000
unsatisfying, and uncontrollable, in substantial, not-self.

161
00:14:02,000 --> 00:14:14,000
It has no entity, no, no, substantiality of its own.

162
00:14:14,000 --> 00:14:17,000
Again, these three things are not some mystery.

163
00:14:17,000 --> 00:14:22,000
Everyone reads about these and they think, I don't see those things.

164
00:14:22,000 --> 00:14:26,000
I've given this talk several times about how meditators will come and say,

165
00:14:26,000 --> 00:14:27,000
I can't.

166
00:14:27,000 --> 00:14:28,000
I'm not progressing.

167
00:14:28,000 --> 00:14:33,000
I just sit here and my mind is in chaos and it's unpleasant.

168
00:14:33,000 --> 00:14:36,000
I can't control that.

169
00:14:36,000 --> 00:14:39,000
How can I progress it in this way?

170
00:14:39,000 --> 00:14:41,000
This is progress.

171
00:14:41,000 --> 00:14:49,000
Seeing that your mind is unpredictable, chaotic, is a cause of great stress.

172
00:14:49,000 --> 00:14:54,000
And all the clinging is a cause of strict, great stress.

173
00:14:54,000 --> 00:14:57,000
We're clinging to things that can't possibly satisfy us.

174
00:14:57,000 --> 00:15:00,000
That's why we're stressed.

175
00:15:00,000 --> 00:15:03,000
It's out of control that you can't control.

176
00:15:03,000 --> 00:15:07,000
You can't predict expectations, have no bearing on reality.

177
00:15:07,000 --> 00:15:11,000
We act as though our expectations are going to somehow dictate reality.

178
00:15:11,000 --> 00:15:14,000
It's quite silly, isn't it?

179
00:15:14,000 --> 00:15:19,000
Somehow because we want things to be a certain way that they're going to be that way.

180
00:15:19,000 --> 00:15:24,000
Such an odd idea, an odd concept.

181
00:15:24,000 --> 00:15:27,000
That's how we act.

182
00:15:27,000 --> 00:15:29,000
So you start to change this.

183
00:15:29,000 --> 00:15:37,000
You start to see that once expectations, it's all highly problematic.

184
00:15:37,000 --> 00:15:42,000
The main cause of suffering is we can't predict.

185
00:15:42,000 --> 00:15:47,000
We can't expect it doesn't really turn out the way we want.

186
00:15:47,000 --> 00:15:50,000
It's unreliable.

187
00:15:50,000 --> 00:15:51,000
This is the main insight.

188
00:15:51,000 --> 00:15:54,000
This is what starts to loosen up the mind.

189
00:15:54,000 --> 00:16:02,000
Three us from our bondage, from this obsession that we have with pleasure and displeasure,

190
00:16:02,000 --> 00:16:13,000
attaining pleasure and removing or destroying, avoiding displeasure.

191
00:16:13,000 --> 00:16:22,000
We'll eventually start to see things with equanimity.

192
00:16:22,000 --> 00:16:30,000
So the second is these characteristics.

193
00:16:30,000 --> 00:16:32,000
This is the beginning of the past.

194
00:16:32,000 --> 00:16:36,000
The three characteristics are really the beginning of insight.

195
00:16:36,000 --> 00:16:44,000
As soon as you start to see these, you can say you've started on the path.

196
00:16:44,000 --> 00:16:47,000
Or you've opened the door, let's say.

197
00:16:47,000 --> 00:16:52,000
Seeing these is like the door right before, when you open the door and you say,

198
00:16:52,000 --> 00:16:53,000
that's where I want to go.

199
00:16:53,000 --> 00:16:55,000
So you open the door.

200
00:16:55,000 --> 00:16:57,000
The next one is manga.

201
00:16:57,000 --> 00:16:58,000
It's the path.

202
00:16:58,000 --> 00:17:00,000
And that's what you walk.

203
00:17:00,000 --> 00:17:03,000
That's what all the meditators here are on.

204
00:17:03,000 --> 00:17:05,000
They're cultivating the path.

205
00:17:05,000 --> 00:17:09,000
The first few days are just spent aligning yourself with the past.

206
00:17:09,000 --> 00:17:12,000
In fact, some of you are still on this stage.

207
00:17:12,000 --> 00:17:17,000
But as you go along, you start to see the three characteristics.

208
00:17:17,000 --> 00:17:18,000
It'll come.

209
00:17:18,000 --> 00:17:22,000
These are not things you have to look for.

210
00:17:22,000 --> 00:17:28,000
All of these insights are like the stripes on a tiger.

211
00:17:28,000 --> 00:17:30,000
You don't have to look for them.

212
00:17:30,000 --> 00:17:32,000
Once you see the tigers, there's the tiger.

213
00:17:32,000 --> 00:17:34,000
Yeah, I see it stripes.

214
00:17:34,000 --> 00:17:36,000
They're right there with the tiger.

215
00:17:36,000 --> 00:17:37,000
You don't have to look for them.

216
00:17:37,000 --> 00:17:38,000
They're not hard to fight.

217
00:17:38,000 --> 00:17:42,000
These are not complex or difficult things to understand.

218
00:17:42,000 --> 00:17:50,000
They're just hidden to us because we're blind, because we don't look.

219
00:17:50,000 --> 00:17:54,000
Once we look, we'll see.

220
00:17:54,000 --> 00:17:59,000
So manga, as you start to progress and you start to see deeper the three characteristics

221
00:17:59,000 --> 00:18:02,000
and I've talked about this before.

222
00:18:02,000 --> 00:18:07,000
In detail, the path, they won't go into detail here.

223
00:18:07,000 --> 00:18:16,000
But basically, you start to see that reality is in constant arises and ceases.

224
00:18:16,000 --> 00:18:22,000
It's not actually suffering itself, but only suffering because we cling to it.

225
00:18:22,000 --> 00:18:24,000
Because we have expectations about it.

226
00:18:24,000 --> 00:18:27,000
And if we stop that, we start to turn our way.

227
00:18:27,000 --> 00:18:34,000
We start to desire and incline towards peace and freedom.

228
00:18:34,000 --> 00:18:39,000
We start to lose our desire for any sort of a risen experience.

229
00:18:39,000 --> 00:18:43,000
And the mind starts to quiet.

230
00:18:43,000 --> 00:18:50,000
It starts to become quite certain and sure of nature of reality.

231
00:18:50,000 --> 00:18:57,000
It sees it so clearly and so consistently that there's clearly nothing worth clinging to.

232
00:18:57,000 --> 00:18:59,000
There's no benefit that comes from holding on.

233
00:18:59,000 --> 00:19:04,000
I mean, this is what you gain the culmination of the path.

234
00:19:04,000 --> 00:19:13,000
So this is the way you need to go through.

235
00:19:13,000 --> 00:19:20,000
For those of you practicing at home, it's much longer and slower and more difficult path.

236
00:19:20,000 --> 00:19:22,000
Coming here might seem more difficult.

237
00:19:22,000 --> 00:19:28,000
Those meditators here must certainly feel how difficult it is.

238
00:19:28,000 --> 00:19:31,000
But it's so much easier in comparison.

239
00:19:31,000 --> 00:19:38,000
There's not years and years or lifetimes of struggling just to gain basic insight into reality.

240
00:19:38,000 --> 00:19:42,000
There's so much insight that comes from being here.

241
00:19:42,000 --> 00:19:49,000
So much purification and cleansing that goes on in the mind, freeing yourself.

242
00:19:49,000 --> 00:19:50,000
It's such a short time.

243
00:19:50,000 --> 00:19:57,000
It's a great blessing.

244
00:19:57,000 --> 00:20:00,000
So all you need is patience and you have to walk the path.

245
00:20:00,000 --> 00:20:21,000
This is the main portion of undertaking is to follow the path and to see clearly and to cultivate and accumulate this understanding of the three characteristics and a nature of reality is not worth clinging to.

246
00:20:21,000 --> 00:20:25,000
Learning to let go.

247
00:20:25,000 --> 00:20:28,000
And so the fourth is Pala. This is the fruit.

248
00:20:28,000 --> 00:20:36,000
When the mind finally lets go, at the end of the path, the mind sees so clearly and it releases it.

249
00:20:36,000 --> 00:20:38,000
Let's go.

250
00:20:38,000 --> 00:20:43,000
No more seeking, no more racing out to see.

251
00:20:43,000 --> 00:20:46,000
What's that? I want to see it. What's that? I want to hear it.

252
00:20:46,000 --> 00:20:49,000
I mind no longer.

253
00:20:49,000 --> 00:20:53,000
Oh, no. What's going on over here? No longer.

254
00:20:53,000 --> 00:21:00,000
The mind gets fed up and says, enough, enough with you all.

255
00:21:00,000 --> 00:21:04,000
And it drops and it quiets.

256
00:21:04,000 --> 00:21:08,000
It becomes perfectly and completely silent.

257
00:21:08,000 --> 00:21:11,000
There's cessation even.

258
00:21:11,000 --> 00:21:25,000
There's cessation of a risen experience. This is nibanda.

259
00:21:25,000 --> 00:21:28,000
And then nibanda is something that is under risen.

260
00:21:28,000 --> 00:21:37,000
And so the mind enters into an under risen state, which is pure peace.

261
00:21:37,000 --> 00:21:44,000
This is the final wisdom. This is, it might not sound like wisdom, actually, but it is the most powerful wisdom.

262
00:21:44,000 --> 00:21:47,000
And maybe the word wisdom is in English.

263
00:21:47,000 --> 00:21:55,000
We use, we have too much baggage surrounding that term, but bunya, but means perfectly, rightly, strongly.

264
00:21:55,000 --> 00:22:00,000
Nya, knowledge, to know.

265
00:22:00,000 --> 00:22:04,000
So when you know nibana, nirana, it's the highest knowledge.

266
00:22:04,000 --> 00:22:08,000
To know nibana, there's nothing greater.

267
00:22:08,000 --> 00:22:11,000
There's nothing that even compares. There's nothing that's...

268
00:22:11,000 --> 00:22:18,000
Anything close to the experience of nibana in terms of changing who you are, changing your life,

269
00:22:18,000 --> 00:22:26,000
changing your direction, changing your mind, freeing you from stress and suffering.

270
00:22:26,000 --> 00:22:30,000
But still quite simple. There's nothing hard to understand.

271
00:22:30,000 --> 00:22:35,000
It's scary, I suppose. Ooh, cessation. But I don't want to cease. I want to go.

272
00:22:35,000 --> 00:22:39,000
What will happen to me, right?

273
00:22:39,000 --> 00:22:47,000
If there were a mean.

274
00:22:47,000 --> 00:22:54,000
But there's nothing hard to understand about these things. They're quite simple.

275
00:22:54,000 --> 00:22:57,000
Well, maybe that's not true. I think they are hard to understand.

276
00:22:57,000 --> 00:23:00,000
But the problem is that we make more out of them than they actually are.

277
00:23:00,000 --> 00:23:08,000
They're hard for us to understand because we act and we function on such a more complex level.

278
00:23:08,000 --> 00:23:15,000
And enlightened being is so much simpler. If you read the Buddhist text, they seem somewhat aggravatingly simple.

279
00:23:15,000 --> 00:23:20,000
The Buddha will give a talk just about seeing, hearing, smelling, tasting, feeling, thinking.

280
00:23:20,000 --> 00:23:23,000
And if you're not a Buddhist meditative, you think, this is dumb.

281
00:23:23,000 --> 00:23:29,000
You might think how simplistic this is, or meaningless.

282
00:23:29,000 --> 00:23:35,000
You just can't make head or tail of it. It's like, why is he talking about seeing?

283
00:23:35,000 --> 00:23:42,000
Remember hearing about Buddhism and it was in a guide book when I was in Thailand and it said,

284
00:23:42,000 --> 00:23:48,000
the Buddha taught that when you walk, just walk when you stand, just stand or something like that.

285
00:23:48,000 --> 00:23:54,000
I thought, well, that's kind of okay. I was trying to get it but inside I'm thinking, oh, that's it.

286
00:23:54,000 --> 00:23:58,000
What's even talking about? What does that mean?

287
00:23:58,000 --> 00:24:04,000
Doesn't look like wisdom. This doesn't seem anything to do with wisdom.

288
00:24:04,000 --> 00:24:08,000
I went to Thailand looking for wisdom when I went to the meditation center.

289
00:24:08,000 --> 00:24:13,000
They asked me, curiously, they asked me, why are you here? What do you hope to get from it?

290
00:24:13,000 --> 00:24:20,000
I said, I would like to gain wisdom. The first thing out of my mouth was what I was looking for.

291
00:24:20,000 --> 00:24:28,000
But boy, the change that went to my understanding of the change that came in my understanding of what wisdom was.

292
00:24:28,000 --> 00:24:36,000
I realized how foolish, how ignorant I was of what wisdom really means.

293
00:24:36,000 --> 00:24:43,000
Wisdom has nothing to do with knowledge, with thinking, concept.

294
00:24:43,000 --> 00:24:51,000
Wisdom is when the stomach rises, you know that it's rising, that's wisdom.

295
00:24:51,000 --> 00:24:57,000
Very profound, very simple.

296
00:24:57,000 --> 00:25:14,000
So there you go, that's the demo for tonight. Thank you all for tuning in.

297
00:25:14,000 --> 00:25:17,000
If there are any questions, I'm happy to take them.

298
00:25:17,000 --> 00:25:35,000
How's the volume, is it loud enough?

299
00:25:35,000 --> 00:25:42,000
Sorry about last night. I lost track of time, and then it was like 20 after nine, and I thought,

300
00:25:42,000 --> 00:25:51,000
oh well, one night, imagine it was full because Sunday night, it was usually a,

301
00:25:51,000 --> 00:25:58,000
oh I don't know, it seems, no, actually Sunday night was never that full.

302
00:25:58,000 --> 00:26:06,000
It was an elephant last night. Well, glad I missed that.

303
00:26:06,000 --> 00:26:13,000
Last night was full. Oh well, it's good, less than an impermanent.

304
00:26:13,000 --> 00:26:23,000
You can't predict even the most. I remember when, well, and I remember in Thailand,

305
00:26:23,000 --> 00:26:29,000
you would sit for like an hour or sometimes two hours, sometimes even longer,

306
00:26:29,000 --> 00:26:36,000
waiting for Ajandong because, you know, reporting was at 5 p.m. and so we'd sit there at 4 p.m. or whatever.

307
00:26:36,000 --> 00:26:43,000
We'd sit there at 4 p.m. and wait, and like two hours later, sometimes longer.

308
00:26:43,000 --> 00:26:56,000
Two hours later, someone will come out and say, oh he's not coming back today. We'll report again tomorrow.

309
00:26:56,000 --> 00:27:03,000
Oh tonight no one's here because Monday's is supposed to be at my off day, but schedule has changed.

310
00:27:03,000 --> 00:27:12,000
I'm now no longer here Wednesdays and Thursdays and whatever other day I lose track of time.

311
00:27:12,000 --> 00:27:25,000
I wasn't doing anything, I just finished meditating and lost track of time.

312
00:27:25,000 --> 00:27:40,000
So no questions, no questions, then it's a small group tonight. Thank you all for coming.

313
00:27:40,000 --> 00:27:48,000
I guess I'll see you all tomorrow, maybe.

314
00:27:48,000 --> 00:27:54,000
Oh, I'll probably be here tomorrow, but you know, I just sit and wait here for a couple of hours

315
00:27:54,000 --> 00:28:04,000
if I'm not here after a couple of hours you can pretty much be assured I'm not coming.

316
00:28:04,000 --> 00:28:14,000
But yeah, maybe I could just, I could have probably come on at 9.20 minutes, assume anyone would have waited.

317
00:28:14,000 --> 00:28:18,000
Were there still people here at 9.20?

318
00:28:18,000 --> 00:28:31,000
Will you wait for me if I'm late sometimes? I don't want to make you guys wait.

319
00:28:31,000 --> 00:28:37,000
Mm-hmm.

320
00:28:37,000 --> 00:28:44,000
Oh, you didn't miss anything, it'll all be up on YouTube.

321
00:28:44,000 --> 00:28:51,000
But it's over now, tonight is over. Wish you all a good night.

322
00:28:51,000 --> 00:29:14,000
See you all next time.

