1
00:00:00,000 --> 00:00:23,000
Good evening everyone, broadcasting live.

2
00:00:23,000 --> 00:00:30,000
March 20th.

3
00:00:30,000 --> 00:00:39,000
I've cut my meditation short this evening.

4
00:00:39,000 --> 00:01:05,000
It's been a long day, lots of different things.

5
00:01:05,000 --> 00:01:12,000
Home work.

6
00:01:12,000 --> 00:01:26,000
Anyway, here on time.

7
00:01:26,000 --> 00:01:41,000
So today's quote is quite an interesting one.

8
00:01:41,000 --> 00:02:06,000
Okay.

9
00:02:06,000 --> 00:02:21,000
I'm going to go to the kitchen and I'm going to go to the kitchen and I'm going to go to the kitchen.

10
00:02:21,000 --> 00:02:47,000
I'm going to go to the kitchen and I'm going to go to the kitchen and I'm going to go to the kitchen.

11
00:02:47,000 --> 00:02:57,000
Thank you very much.

12
00:02:57,000 --> 00:03:23,000
Thank you very much.

13
00:03:23,000 --> 00:03:44,000
Thank you very much.

14
00:03:44,000 --> 00:04:10,000
Thank you very much.

15
00:04:10,000 --> 00:04:36,000
Thank you very much.

16
00:04:36,000 --> 00:04:43,000
This is one seizes another.

17
00:04:43,000 --> 00:04:56,000
It's a simple philosophy and claim but it has deep meaning.

18
00:04:56,000 --> 00:05:14,000
You see, so science and ordinary people have this idea of a substratum, most religions as well.

19
00:05:14,000 --> 00:05:22,000
Science is quite convinced that there is a substratum of reality.

20
00:05:22,000 --> 00:05:37,000
One is usually quite convinced that there is a substratum of mentality and self-god.

21
00:05:37,000 --> 00:06:01,000
There is a lot of postulation of various substratum.

22
00:06:01,000 --> 00:06:15,000
That religion also does a better job according with mental reality sometimes than science.

23
00:06:15,000 --> 00:06:21,000
Nonetheless, the tendency is to postulate substratum.

24
00:06:21,000 --> 00:06:36,000
Some substratum means like a framework, like a mind, a body, a three-dimensional, four-dimensional reality that we live in.

25
00:06:36,000 --> 00:06:41,000
Buddhism doesn't do that.

26
00:06:41,000 --> 00:06:58,000
Buddhism has a very simple take on the universe that this moment is real, this moment is an experience, and then it ceases, and then this moment is real.

27
00:06:58,000 --> 00:07:25,000
That moment, so when we talk about mind in terabyte of Buddhism, we mean that moment of mind, one moment of mind, one moment of experience, one moment of awareness of contemplation of an object, observation of an object, one moment of experience.

28
00:07:25,000 --> 00:07:31,000
If you ask the question, who is right?

29
00:07:31,000 --> 00:07:35,000
It's not really the appropriate question.

30
00:07:35,000 --> 00:07:50,000
Scientists tend to scoff at Buddhism as being overly simple, simplistic, and they'll say, okay, well, maybe you're right, but you haven't said anything about reality.

31
00:07:50,000 --> 00:08:18,000
You haven't figured out subatomic particles, or how to build an atom bomb, or have you explored the best reaches of the universe, solar system, and the galaxy, the known universe, and then you haven't really explored reality.

32
00:08:18,000 --> 00:08:27,000
We can say, no, no, we haven't.

33
00:08:27,000 --> 00:08:30,000
The question is to what end?

34
00:08:30,000 --> 00:08:35,000
The question of all of this is to what end.

35
00:08:35,000 --> 00:08:45,000
Religion makes all sorts of claims about reality, and we can ask what is the result?

36
00:08:45,000 --> 00:09:03,000
If it were useful and positive to delude yourself, and make claims about reality that were not in accord with reality, that had no evidence,

37
00:09:03,000 --> 00:09:27,000
not based on evidence, if that were somehow beneficial, then there wouldn't be much of a problem, most religions would do fine, but because they tend to not relate to actual experience and reality, they tend to rely more on belief.

38
00:09:27,000 --> 00:09:36,000
They tend to be quite artificial and stilted or forced.

39
00:09:36,000 --> 00:09:48,000
If you step back, or say you step out of a meditation course where you've been studying your own mind and what's really here and now,

40
00:09:48,000 --> 00:09:57,000
and then you contemplate one of these religions, pick any religion or random, and you contemplate what they're actually saying.

41
00:09:57,000 --> 00:10:04,000
It's shocking how absurd it always.

42
00:10:04,000 --> 00:10:13,000
What are you talking about? What is this garbage?

43
00:10:13,000 --> 00:10:29,000
So much belief. There was this guy, he said this, he claimed this, he did this, and he will save us.

44
00:10:29,000 --> 00:10:45,000
Or there's this god up there, and we have to do his prey, or repeat his name, or chant this, or chant that, and he will save you.

45
00:10:45,000 --> 00:11:02,000
So they run into lots of problems, and I think wise people would tend to stay away from most of the religions out there because they tend to be not very closely aligned to reality.

46
00:11:02,000 --> 00:11:10,000
But science does a good job focusing on reality, aligning itself with experience.

47
00:11:10,000 --> 00:11:19,000
They are able to run experiments that actually accord with the way things are, but they've got a different problem.

48
00:11:19,000 --> 00:11:43,000
And we can argue about what is more real in the substratum, physical and even maybe mental reality, or experience. Which one is more real? We can argue about that, but it's not an important argument again to what end?

49
00:11:43,000 --> 00:11:47,000
What has science given us? What has it brought us?

50
00:11:47,000 --> 00:11:59,000
It's brought us all sorts of magical, wonderful things like the internet, the fact that I'm able to talk to people all around the world, kind of magical.

51
00:11:59,000 --> 00:12:05,000
It's brought us all this, very powerful, this kind of knowledge.

52
00:12:05,000 --> 00:12:13,000
But has it brought us happiness, has it brought us peace, has it brought us understanding?

53
00:12:13,000 --> 00:12:19,000
And I mean a specific, I'm thinking of a specific type of understanding, really.

54
00:12:19,000 --> 00:12:25,000
Has it brought us to understanding of ourselves? No.

55
00:12:25,000 --> 00:12:39,000
None people who study science get angry, become addicted to things, they have a hard time dealing with ordinary experiences, and so they suffer.

56
00:12:39,000 --> 00:12:51,000
They stress, they kill themselves, they drown their, soak their brains in alcohol, or drugs.

57
00:12:51,000 --> 00:12:56,000
Because they can't deal with ordinary reality, right?

58
00:12:56,000 --> 00:13:05,000
They study reality so much, but can't deal with it.

59
00:13:05,000 --> 00:13:18,000
And then you have this Buddhist, I'd say, a meditator who focuses on their reality, who looks at their experience.

60
00:13:18,000 --> 00:13:25,000
When they have emotions, they see them as emotions, when they have pain, they see it as pain.

61
00:13:25,000 --> 00:13:31,000
The experiences of seeing their hearings, they see them for what they are.

62
00:13:31,000 --> 00:13:37,000
They put aside any theories or thoughts about a some stratum about reality.

63
00:13:37,000 --> 00:13:43,000
Like, oh, here I'm in this room, they even lose track of being in a room.

64
00:13:43,000 --> 00:13:50,000
They just see the mind arising, and see seeing the mind arises, there's awareness of something.

65
00:13:50,000 --> 00:13:56,000
And see, it says, immediately there's another mind, but it's totally different.

66
00:13:56,000 --> 00:14:01,000
It's a whole new mind, whole new experience.

67
00:14:01,000 --> 00:14:15,000
When they see this, they're able to see a reality or a way of looking at reality for themselves.

68
00:14:15,000 --> 00:14:19,000
That leads to objectivity.

69
00:14:19,000 --> 00:14:25,000
They're able to fully comprehend reality, I guess you can say.

70
00:14:25,000 --> 00:14:29,000
Because science, you can study it, but you can't fully comprehend it.

71
00:14:29,000 --> 00:14:36,000
You can't look at the wall and comprehend that that wall is made up of mostly empty space.

72
00:14:36,000 --> 00:14:45,000
I need to make it a given example, but moreover, you're not able to comprehend all those things.

73
00:14:45,000 --> 00:14:49,000
I guess I would say it's not even worth, it wouldn't even be worth it if we could.

74
00:14:49,000 --> 00:14:54,000
And in fact, our comprehension of things is part of the problem.

75
00:14:54,000 --> 00:15:01,000
Because when you think about concepts, you think about entities, you have to abstract something.

76
00:15:01,000 --> 00:15:04,000
You have this mouse.

77
00:15:04,000 --> 00:15:07,000
What happens when I think of this mouse?

78
00:15:07,000 --> 00:15:11,000
There's something going on in my mind.

79
00:15:11,000 --> 00:15:13,000
There's an experience.

80
00:15:13,000 --> 00:15:23,000
There's an experience which says there is a mouse in your hand, but it has nothing to do directly with the experience of seeing the mouse.

81
00:15:23,000 --> 00:15:28,000
I think the seeing and the feeling that created it.

82
00:15:28,000 --> 00:15:30,000
It's all in the mind at that point.

83
00:15:30,000 --> 00:15:40,000
There are minds arising in safety, leading, therefore, therefore, one to the other.

84
00:15:40,000 --> 00:15:42,000
And I'm not aware of that.

85
00:15:42,000 --> 00:15:49,000
So there's a reality that is being ignored, because instead I'm focused on the idea of the mouse.

86
00:15:49,000 --> 00:15:54,000
So there's something that I'm missing.

87
00:15:54,000 --> 00:15:58,000
And as a result, when likes and dislikes come up, I miss them as well.

88
00:15:58,000 --> 00:15:59,000
So I like them now.

89
00:15:59,000 --> 00:16:00,000
So I dislike it.

90
00:16:00,000 --> 00:16:07,000
And that's what leads to frustration and addiction and so on.

91
00:16:07,000 --> 00:16:18,000
So it leads us attached to things and people and experiences and leads us to get frustrated and upset and bored.

92
00:16:18,000 --> 00:16:25,000
Disappointed when we don't get what we want.

93
00:16:25,000 --> 00:16:35,000
So when we pay attention to that, if I'm picking up the mouse, I pay attention to the experience and the thinking and the judging,

94
00:16:35,000 --> 00:16:47,000
then I am in a position to understand and to see the experience clearly and to not react.

95
00:16:47,000 --> 00:16:59,000
Or to understand my reactions and to see how they're hurting me and to slowly give them up.

96
00:16:59,000 --> 00:17:08,000
This is an important, an important quote to help us understand our focus should be.

97
00:17:08,000 --> 00:17:13,000
Our focus should be on experiential reality.

98
00:17:13,000 --> 00:17:17,000
This is what the Buddha talked about.

99
00:17:17,000 --> 00:17:20,000
So here, what do we have? This also it is interesting.

100
00:17:20,000 --> 00:17:22,000
The uninstracted world thing.

101
00:17:22,000 --> 00:17:25,000
The puturjana.

102
00:17:25,000 --> 00:17:29,000
The sutva.

103
00:17:29,000 --> 00:17:33,000
The sutva bekabe puturjana.

104
00:17:33,000 --> 00:17:36,000
Uninstracted puturjana.

105
00:17:36,000 --> 00:17:40,000
Might experience revulsion towards this body.

106
00:17:40,000 --> 00:17:45,000
It might become dispassionate towards it and be liberated.

107
00:17:45,000 --> 00:17:47,000
It might even be liberated.

108
00:17:47,000 --> 00:17:50,000
Because growth and decline are seen in this body.

109
00:17:50,000 --> 00:17:59,000
It is seen and taken up.

110
00:17:59,000 --> 00:18:04,000
He is saying it might be liberated from the body.

111
00:18:04,000 --> 00:18:10,000
The body is revol. It is easy to see. It is revolting as you get old and if you get sick and so on.

112
00:18:10,000 --> 00:18:15,000
It is possible for ordinary people to let go of it.

113
00:18:15,000 --> 00:18:17,000
But the mind.

114
00:18:17,000 --> 00:18:20,000
The mind we are unable to let go of.

115
00:18:20,000 --> 00:18:23,000
This is the ordinary uninstracted world thing.

116
00:18:23,000 --> 00:18:31,000
It is unable to experience revulsion towards it, unable to become dispassionate towards it and be liberated from it.

117
00:18:31,000 --> 00:18:35,000
Because for a long time this has been held by him appropriate and grasped us.

118
00:18:35,000 --> 00:18:37,000
This is me mine.

119
00:18:37,000 --> 00:18:42,000
This I am, this is my self.

120
00:18:42,000 --> 00:18:47,000
This is my self.

121
00:18:47,000 --> 00:19:05,000
This is what we think of the mind.

122
00:19:05,000 --> 00:19:10,000
Who doesn't think the mind is me and mine?

123
00:19:10,000 --> 00:19:26,000
The body we can let go. When the mind is pleased or displeased, that's me, that's mine.

124
00:19:26,000 --> 00:19:45,000
This is what we think of.

125
00:19:45,000 --> 00:19:59,000
It becomes dispassionate.

126
00:19:59,000 --> 00:20:10,000
It will be better to take. This is an interesting one.

127
00:20:10,000 --> 00:20:26,000
Take the body as self because the body is standing for one year, for two years, for three, four, five, or ten years, for twenty, thirty, forty, fifty years, for a hundred years, or even longer.

128
00:20:26,000 --> 00:20:34,000
But the mind and mentality and consciousness arises as one thing and this is the quote and ceases as another.

129
00:20:34,000 --> 00:20:44,000
The mind is fighting, the mind arises and this is quickly.

130
00:20:44,000 --> 00:20:50,000
And this is really what I'm talking about, is the body appears to be a thing.

131
00:20:50,000 --> 00:20:57,000
If you think about the body, it's understandable that you would cling to it, but how can you cling to the mind?

132
00:20:57,000 --> 00:21:04,000
This is a different nobody is exposing a different way of looking at reality from the point of view of the mind.

133
00:21:04,000 --> 00:21:12,000
Because from the point of view of the mind, even the body arises and sees it.

134
00:21:12,000 --> 00:21:28,000
And then he says, that there will be Kameh Sutula, a learned Ariya Saamako, disciple of the noble one.

135
00:21:28,000 --> 00:21:43,000
Well, with wisdom, considers a tikshasamupada, the dependent origination.

136
00:21:43,000 --> 00:22:02,000
If t, it must mean sati idhamo t, thus, when this exists, that comes to be, with the arising of this, that arises.

137
00:22:02,000 --> 00:22:19,000
If t must mean sati idhamo t, it must subbada idhamo tatit. When this arises, there is that, with the arising of this, that arises.

138
00:22:19,000 --> 00:22:28,000
He must mean, aseti with the non-existence of this, he doesn't know how t, there is not that.

139
00:22:28,000 --> 00:22:38,000
He must n melo daí'mang idhamino jiri, Kumessa Sasashun of this, this, that is the distance.

140
00:22:38,000 --> 00:22:49,840
India Didang there is say, ah dire bejap idea sankara sankara baj dirkmy interval and bhiti

141
00:22:49,840 --> 00:22:53,000
cyopity comic bhita samabada dependent origination.

142
00:22:53,000 --> 00:22:57,000
That's what he is saying here is, this is how experiential reality works.

143
00:22:57,000 --> 00:23:02,000
Because we're ignorant, we give rise to our partialities.

144
00:23:02,000 --> 00:23:04,000
We like some things in this language.

145
00:23:04,000 --> 00:23:06,000
We chase after some things.

146
00:23:06,000 --> 00:23:08,000
We chase away some things.

147
00:23:08,000 --> 00:23:09,000
Because we don't see things clear.

148
00:23:09,000 --> 00:23:13,000
It's a result we're born again and again.

149
00:23:13,000 --> 00:23:17,000
And we have anyway leads to suffering.

150
00:23:17,000 --> 00:23:21,000
And I'm going to detail with that.

151
00:23:21,000 --> 00:23:23,000
The important point is that we look at reality

152
00:23:23,000 --> 00:23:25,000
from the point of view of our experiences.

153
00:23:25,000 --> 00:23:27,000
Because if you do that, there's really nothing.

154
00:23:27,000 --> 00:23:28,000
You become invincible.

155
00:23:28,000 --> 00:23:34,000
There's nothing that can empower over you.

156
00:23:34,000 --> 00:23:37,000
Whether it be something pleasant that you want to.

157
00:23:37,000 --> 00:23:39,000
There's no you, there's no thing.

158
00:23:39,000 --> 00:23:44,000
You see it if you see it as an experience that arises.

159
00:23:44,000 --> 00:23:49,000
There's something that upsets you or frustrates you.

160
00:23:49,000 --> 00:23:51,000
That thing is just an experience.

161
00:23:51,000 --> 00:23:53,000
You see it arising.

162
00:23:53,000 --> 00:23:55,000
You see the experience.

163
00:23:55,000 --> 00:23:56,000
What does it mean?

164
00:23:56,000 --> 00:23:59,000
It's just an experience.

165
00:23:59,000 --> 00:24:00,000
You see the frustration.

166
00:24:00,000 --> 00:24:02,000
You see the upset.

167
00:24:02,000 --> 00:24:07,000
Arise within ceases.

168
00:24:07,000 --> 00:24:08,000
You stop hiding.

169
00:24:08,000 --> 00:24:10,000
You stop running.

170
00:24:10,000 --> 00:24:14,000
You stop living in conceptual reality.

171
00:24:14,000 --> 00:24:17,000
Things and people and places.

172
00:24:17,000 --> 00:24:23,000
You start living in reality in a true reality in a physical experience.

173
00:24:23,000 --> 00:24:27,000
You start seeing what's going on underneath that.

174
00:24:27,000 --> 00:24:34,000
A chrome plating of beautiful things and desirable things.

175
00:24:34,000 --> 00:24:38,000
And ugly things and scary things.

176
00:24:38,000 --> 00:24:44,000
And underneath it's all just experience.

177
00:24:44,000 --> 00:24:49,000
Anyway, there's the quote for this evening.

178
00:24:49,000 --> 00:24:53,000
That's the number for this evening.

179
00:24:53,000 --> 00:24:56,000
Now as usual I'm going to put the quote.

180
00:24:56,000 --> 00:24:59,000
I put the link to the hangout up.

181
00:24:59,000 --> 00:25:07,000
If you would like to join and ask a question you're welcome to click the link.

182
00:25:07,000 --> 00:25:12,000
Only if you have a question and only if you're in a respectful like no coming on naked

183
00:25:12,000 --> 00:25:19,000
or smoking or drinking alcohol or eating food.

184
00:25:19,000 --> 00:25:32,000
You should consider this to be a dumb hangout.

185
00:25:32,000 --> 00:25:47,000
Just scare everybody away.

186
00:25:47,000 --> 00:26:10,000
You can go the best I feel.

187
00:26:10,000 --> 00:26:33,000
Thank you.

188
00:26:33,000 --> 00:26:48,000
All right, nobody with burning questions sleeping to be to join the hangout.

189
00:26:48,000 --> 00:27:11,000
And I think I'll say good night.

190
00:27:11,000 --> 00:27:37,000
Thank you.

