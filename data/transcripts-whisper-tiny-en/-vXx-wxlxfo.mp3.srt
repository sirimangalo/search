1
00:00:00,000 --> 00:00:25,200
Good evening, everyone, broadcasting live to live 5th, here to review, discuss, to unravel

2
00:00:25,200 --> 00:00:38,520
the dumber. So there we have a rather odd sort of quote. It seems to me to be all that

3
00:00:38,520 --> 00:00:53,000
of an essential quote. That's about associating with good people. I don't think it's quite a good

4
00:00:53,000 --> 00:01:00,520
translation. Let me go about these translations. Seems more reasonable to me. There's a person

5
00:01:00,520 --> 00:01:11,920
who should be associated with. We follow them to be served. Kind of a person. Kind of a person

6
00:01:11,920 --> 00:01:26,800
should you follow. It starts off to start off by talking about those who should be looked upon

7
00:01:26,800 --> 00:01:37,720
with disgust and not associating with. So if someone is immoral or bad character, impure of suspect

8
00:01:37,720 --> 00:01:49,640
behavior, such a person one should not follow. Because even if one doesn't take their example,

9
00:01:49,640 --> 00:01:59,440
this is how big a body doesn't take their example. Still people will assume the worst. You

10
00:01:59,440 --> 00:02:13,040
will be branded tired when they say tired with the same brush. People will think, oh, you

11
00:02:13,040 --> 00:02:25,000
must be like that person that you've fallen. Among other things. There's lots of trouble

12
00:02:25,000 --> 00:02:33,960
that surrounds people who do bad things. Sometimes we remain friends and we kind of loyalty

13
00:02:33,960 --> 00:02:43,920
think that we should stick with people who are in a bad way for reasons other than their

14
00:02:43,920 --> 00:02:50,040
behavior. And kind of some of you agree with that. There's a certain amount of trouble

15
00:02:50,040 --> 00:03:03,960
that follows from associating with corrupt individuals or immoral impure. They do bad things.

16
00:03:03,960 --> 00:03:12,440
If you're a person in the end, such people should be avoided possible. There's lots of

17
00:03:12,440 --> 00:03:20,600
reasons for what it gives one of the, I guess the most obvious is the bad reputation. And

18
00:03:20,600 --> 00:03:25,560
it makes it hard to live your life because no one wants to be around you all of a sudden

19
00:03:25,560 --> 00:03:37,720
because you know who your friends are. And there's another type of person who should

20
00:03:37,720 --> 00:03:44,000
be looked upon with equanimity, but also not associated with. The first type of person

21
00:03:44,000 --> 00:03:55,800
should be looked upon with disgust. She could take, she could cheat about to be shunned.

22
00:03:55,800 --> 00:04:10,640
The second type of person should be seemingly equanimity, but also not associated. This

23
00:04:10,640 --> 00:04:19,720
is a person who though they may not do bad things. Say bad things, they still, but they

24
00:04:19,720 --> 00:04:29,760
don't do evil deeds, but they still have evil mind states. So they're prone to anger.

25
00:04:29,760 --> 00:04:34,560
If they're slightly criticized, they become, they lose their temper and become irritable

26
00:04:34,560 --> 00:04:43,080
hostile and stubborn. They display irritation, hatred and bitterness. Just as a festering

27
00:04:43,080 --> 00:04:52,160
sore, it's struck by a sticker, a shard, with this charge even more matter so too. Or

28
00:04:52,160 --> 00:05:01,520
just as a firebrand, with sizzle and crack, just as a pit of feces, if struck by a stick

29
00:05:01,520 --> 00:05:11,600
will become even more foul smelling. And these sort of people, you best just leave them

30
00:05:11,600 --> 00:05:20,720
alone. They're not, they're not identifiably evil in terms of giving you bad reputation

31
00:05:20,720 --> 00:05:31,360
or getting you caught up in evil deeds or even worse. Encouraging you yourself to engage

32
00:05:31,360 --> 00:05:38,120
in evil deeds, but there's a certain nastiness for people who get angry and for people

33
00:05:38,120 --> 00:05:48,920
who are greedy. They sort of people, you should still try to keep your distance, not because

34
00:05:48,920 --> 00:05:58,520
of the explicit evil, but because of the more, that's still explicit with the more inner

35
00:05:58,520 --> 00:06:11,800
corruption that's going to, they poke it with a stick, it smells even worse. If you get involved

36
00:06:11,800 --> 00:06:18,920
with such people, there's always conflict, there's always trouble. So the people are best

37
00:06:18,920 --> 00:06:25,200
best treated like a pile of crap. It's okay as long as they don't touch it, poke it with

38
00:06:25,200 --> 00:06:32,600
the stick or step in it. But in a such a person, it should be associated with followed

39
00:06:32,600 --> 00:06:38,520
and served. This is what the quote says, someone is virtuous and a good character you

40
00:06:38,520 --> 00:06:43,080
shouldn't hang out with. This isn't a sort of person you should stick around with.

41
00:06:43,080 --> 00:06:49,000
I think it was just curious, it's just a reflection of the other one, but it's still kind

42
00:06:49,000 --> 00:06:57,480
of curious that the only reason, but it is even if you don't, even if you don't follow

43
00:06:57,480 --> 00:07:06,440
the example and become a good person yourself, people will give a good reputation. Oh yes,

44
00:07:06,440 --> 00:07:13,640
this person has good friends. This person has good companions. I think it's a little bit

45
00:07:13,640 --> 00:07:21,600
superficial to say, but there's something a little more to it than that. It's a sign of

46
00:07:21,600 --> 00:07:31,840
good judgment. But I think more importantly, it's a doorway. Even if you don't become a good

47
00:07:31,840 --> 00:07:39,200
person by associating with good people, there's always the opportunity. The sort of superficial

48
00:07:39,200 --> 00:07:45,360
benefits of having people say good things about even being free from any of the dangers

49
00:07:45,360 --> 00:07:52,000
of the troubles that come from associating with corrupt people, all that's fairly superficial.

50
00:07:52,000 --> 00:07:58,560
Obviously more important is, even if you don't pick up their habits immediately, the

51
00:07:58,560 --> 00:08:04,760
opportunity to pick up good habits and the potential to become a better person is of course.

52
00:08:04,760 --> 00:08:10,600
It's greatly improved when you hang up with good people. So you may not be listening

53
00:08:10,600 --> 00:08:18,880
to feel inferior or unworthy as on. You shouldn't seek out good people with the thought

54
00:08:18,880 --> 00:08:28,800
that I will become a better person by self. Anyway, it's a fairly simple quote. Not much

55
00:08:28,800 --> 00:08:35,920
to say about that. But there's so much to say about friendship, hanging out with good

56
00:08:35,920 --> 00:08:44,800
people, being around good people, especially when you're doing something so personal as

57
00:08:44,800 --> 00:08:50,520
meditation, so much to do with the individual. It's important to associate with individuals

58
00:08:50,520 --> 00:08:59,080
for a good example. People who would like to become more similar to people who are engaged

59
00:08:59,080 --> 00:09:08,320
in activities that we're attempting to cultivate ourselves. When it's the other people

60
00:09:08,320 --> 00:09:16,760
meditating, it's more likely that you're going to meditate yourself. More importantly,

61
00:09:16,760 --> 00:09:24,120
we're good qualities of such a personal life, we're going to rub off and provide a good

62
00:09:24,120 --> 00:09:34,960
example. Sort of a roadside, something to look for when you're on the path to start becoming

63
00:09:34,960 --> 00:09:40,920
more likely as people. As you see, the goodness in them is a great benefit from being

64
00:09:40,920 --> 00:09:49,240
around good people. It's great to come to a meditation center. We've got lots of people

65
00:09:49,240 --> 00:09:55,800
coming this year, this great. We've got people signing up for courses all the way through

66
00:09:55,800 --> 00:10:08,120
October so far. It's really great to see such interest. We're still looking for a steward,

67
00:10:08,120 --> 00:10:13,920
if there's anybody out there who would like to come and stay for a longer period. They've

68
00:10:13,920 --> 00:10:19,880
had some people, there's one person from Israel who mentioned staying long term, so maybe

69
00:10:19,880 --> 00:10:26,520
that'll work out. I don't know. I'm coming in September, I think. If anyone would like

70
00:10:26,520 --> 00:10:34,760
to talk about that, then we'd be interested in helping out around here for months, some longer

71
00:10:34,760 --> 00:10:48,120
period of time. That'd be great. Lots of stuff going on. So any questions? Does meditating

72
00:10:48,120 --> 00:10:54,280
during an activity count does a formal practice? Would it be alright to log it as if time

73
00:10:54,280 --> 00:11:06,520
spent in meditation? Yeah, I mean, it's not like we're going to check on you as to how you meditated

74
00:11:06,520 --> 00:11:20,280
or so on. But I don't quite understand what you're saying during an activity. I don't understand

75
00:11:20,280 --> 00:11:31,000
what you mean by activity. Does there a shared responsibility impact for one another's lives

76
00:11:31,000 --> 00:11:44,200
than just one's own responsibility? Collective responsibility? Not really, but there's a sense of

77
00:11:44,200 --> 00:11:52,360
rightness. If you don't help other people in giving the opportunity, there's a sense of tension

78
00:11:52,360 --> 00:12:02,520
involved. And to some extent, you have your nervous stay at peace. You have to do some things

79
00:12:02,520 --> 00:12:12,680
to benefit others. If you are cold and ignore the benefit of benefit to others, it can be a

80
00:12:12,680 --> 00:12:24,920
hindrance to your practice. But no, we're not responsible for other people's being not directly.

81
00:12:28,120 --> 00:12:38,760
We just have certain duties that as individuals, it's proper to perform. It's a part of

82
00:12:38,760 --> 00:12:46,600
right, what's right, and the right action for the individual often involves helping other.

83
00:12:51,880 --> 00:12:55,880
We're looking for disciples for your center, other amongst our people seeking to become

84
00:12:55,880 --> 00:13:00,280
amongst how we've been interested. Being a steward would be the setup.

85
00:13:00,280 --> 00:13:10,600
Well, the setup, you'd have to do about a month of meditation with us first. And then you just stay

86
00:13:10,600 --> 00:13:17,320
on after that, but it would involve doing some of the things that intensive meditators, beginner

87
00:13:17,320 --> 00:13:25,800
meditators, anyway, aren't able to do like some amount of cleaning and organizing,

88
00:13:25,800 --> 00:13:35,560
caring for the monastery and helping me out, whatever I need. I mean, we had some recently

89
00:13:35,560 --> 00:13:42,920
had a car, which was great, so I could drive, he could drive me places. That's not entirely

90
00:13:42,920 --> 00:13:53,160
necessary, but it wouldn't be a plus. Some cooking made me, but something's to do with,

91
00:13:53,160 --> 00:13:59,800
you know, we need a local person to organize things that I can't organize. Things with money and

92
00:14:01,720 --> 00:14:07,480
buying basic supplies for the monastery. There's just, there's not a lot to do, but there are

93
00:14:07,480 --> 00:14:15,560
things to be done. It would help to have a steward. I think, yeah, I think part of it was the

94
00:14:15,560 --> 00:14:23,080
organization, one someone who had cooked for me, because right now I'm going to the restaurant, and

95
00:14:24,280 --> 00:14:32,920
taking advantage of people's generosity in offering gift cards for certain restaurants, but it's

96
00:14:32,920 --> 00:14:43,000
quite expensive. So it's not, it's not exactly, yeah, there you go. It's not efficient or economical

97
00:14:43,000 --> 00:14:50,360
for doing that. I make more sense to somehow do cooking at the monastery.

98
00:14:55,400 --> 00:15:01,880
As far as becoming a monk, I'm shying away from that, but long-term meditative is the option

99
00:15:01,880 --> 00:15:09,480
certainly open, but we're not really advertising because it's too easy to miss the point,

100
00:15:09,480 --> 00:15:15,080
ordaining should be a part of your meditation practice. It shouldn't be anything separate.

101
00:15:15,080 --> 00:15:21,800
If you want to meditate long-term mode, becoming a monk means that would allow you to do that,

102
00:15:21,800 --> 00:15:29,960
that's all. By calling someone's behavior, disgusting, is that not judging it? I mean, judging

103
00:15:29,960 --> 00:15:43,960
is not always bad, discerning. I mean, disgusting is probably the wrong word. It's just a word,

104
00:15:43,960 --> 00:15:53,080
but if you're actually disgusted by something, obviously that is a disliking event or a version

105
00:15:53,080 --> 00:16:06,840
towards it. But we use disgusting as just a word in regards to a different kind of repulsion

106
00:16:06,840 --> 00:16:13,720
in the sense of the inability to do it through wisdom and inability to get involved with something

107
00:16:14,680 --> 00:16:19,480
out of a knowledge that it's bad, that it's wrong, that it leads to suffering, basically.

108
00:16:19,480 --> 00:16:25,640
So if you know something that leads to suffering, the more you know the harder it is for you

109
00:16:25,640 --> 00:16:33,960
to engage in it and when the potential to engage with people, for example, who are doing bad

110
00:16:33,960 --> 00:16:40,280
things, arises when the opportunity arises. There's a certain recoiling that doesn't have anything

111
00:16:40,280 --> 00:16:48,040
to do necessarily with a version, but it's based on a knowledge that that's not the way

112
00:16:48,040 --> 00:16:51,400
to do things, getting involved with that person is not inefficient.

113
00:17:01,160 --> 00:17:06,520
I clean the bathroom, I do it in my mind, my mind plays possibly. Right.

114
00:17:08,280 --> 00:17:13,240
No, that's not quite what we're looking for. I mean, that's great. That's an awesome

115
00:17:13,240 --> 00:17:18,280
a dream of doing it, but we're really looking for formal meditation practice.

116
00:17:20,120 --> 00:17:24,440
That's what's meant here. I mean, do you actually do formal walking in Sydney?

117
00:17:32,040 --> 00:17:37,640
May as the you do not cook your own food, I meditate with preparing food as part of my practice.

118
00:17:37,640 --> 00:17:51,720
Amongst aren't allowed to keep food as part of a practice of renunciation as a lifestyle.

119
00:17:51,720 --> 00:17:59,240
We don't keep foods, cook food. We can't eat anything unless it's given to us.

120
00:17:59,240 --> 00:18:07,160
I mean, preparing your own food has its own problems because of the ability to choose

121
00:18:08,360 --> 00:18:18,360
the mental activity involved with choosing your own food and thinking about what food you're

122
00:18:18,360 --> 00:18:26,440
going to eat and some extent obsessing over the health and so on. There's something great about

123
00:18:26,440 --> 00:18:35,800
just having to eat, whatever is being offered, and moreover, it's sort of the scraps that you get

124
00:18:35,800 --> 00:18:43,480
by going through the village. That's the greatest. So that's sort of the model is for monks to go

125
00:18:43,480 --> 00:18:50,680
walking through the village or scraps of arms. Whenever people are willing to give us charity,

126
00:18:50,680 --> 00:18:59,320
because in India it was a big thing to give food to recklessness, to religious people,

127
00:19:00,440 --> 00:19:06,840
and then get enough to survive and that was it. You avoid a lot of the trouble of having to cook

128
00:19:06,840 --> 00:19:14,600
food and the potential, potential distraction involved in storing and cooking and having a kitchen

129
00:19:14,600 --> 00:19:24,680
in the first place. Some monks aren't allowed, monks being a monk is on a different level.

130
00:19:25,560 --> 00:19:28,920
Of course, for meditators cooking your own food is fine. There's just a sense that

131
00:19:29,640 --> 00:19:34,440
it's an unwarranted distraction. If you're able to be free from that, you can focus more on

132
00:19:35,560 --> 00:19:39,960
a deep meditation. It's too easy to get distracted. Sure, you can be mindful cooking.

133
00:19:39,960 --> 00:19:46,760
It's great. But during a meditation course, much better not to have to cook. It's too much of a

134
00:19:46,760 --> 00:19:50,520
distraction, especially for a beginner meditator.

135
00:19:58,680 --> 00:20:03,800
Lots of things the Buddha said don't get involved and don't get caught up in work.

136
00:20:03,800 --> 00:20:13,720
Don't get caught up in sleep. Don't get caught up in talk. Don't get caught up in

137
00:20:17,640 --> 00:20:22,200
socializing. Don't get caught up in eating.

138
00:20:22,200 --> 00:20:31,560
This thing is getting away of your practice.

139
00:20:36,760 --> 00:20:48,280
So unfortunately, I don't think our quote was all that, 50 to night and few questions.

140
00:20:48,280 --> 00:20:52,760
Doesn't look like there's any more. If you have any more questions, I'll be here again tomorrow.

141
00:20:54,280 --> 00:20:56,120
I'm broadcasting a little bit every day.

142
00:20:56,120 --> 00:21:25,960
So thank you all for tuning in. Keep up the good work.

