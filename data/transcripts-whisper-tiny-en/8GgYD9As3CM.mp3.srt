1
00:00:00,000 --> 00:00:06,400
Can walking meditation be as effective as possible as a sitting meditation?

2
00:00:06,400 --> 00:00:11,360
Of course, meditation is not walking or sitting meditation is the state of mind.

3
00:00:11,360 --> 00:00:21,000
Sitting meditation has the added benefit of having less object, fewer objects to focus on.

4
00:00:21,000 --> 00:00:27,000
But walking meditation or meditation in any position can be equally effective.

5
00:00:27,000 --> 00:00:34,000
The Buddha said, whatever, italian, tao, pani, pani, itang, something like that.

6
00:00:34,000 --> 00:00:42,000
However, the mind is, however, whatever posture you put the mind in, be mindful of that.

7
00:00:42,000 --> 00:00:48,000
So it doesn't matter what posture or what movement or what experience comes about.

8
00:00:48,000 --> 00:00:50,000
It can be effective.

9
00:00:50,000 --> 00:00:56,000
Now, walking meditation structurally, sitting meditation structurally is the most beneficial

10
00:00:56,000 --> 00:00:59,000
because there's very few objects to begin with.

