1
00:00:00,000 --> 00:00:04,480
Good evening and welcome back to our study of the Dhamapada.

2
00:00:04,480 --> 00:00:32,920
Tonight we continue on with first number 92, which reads as follows.

3
00:00:32,920 --> 00:00:47,400
Which means, for whom there is no storing, no hoarding, for whom there is no hoarding.

4
00:00:47,400 --> 00:00:58,400
Yay Paringata Mojanang, one who is fully aware or fully mindful as perfect understanding

5
00:00:58,400 --> 00:01:02,600
in regards to food.

6
00:01:02,600 --> 00:01:16,280
Tsunyoto amito amito, amito jia, vimoko yesangoutjaro, empty and sineless is the liberation.

7
00:01:16,280 --> 00:01:27,040
The empty and the sineless liberation are their pastures, are their habitat, are their

8
00:01:27,040 --> 00:01:29,720
dwelling place.

9
00:01:29,720 --> 00:01:32,200
It's really the meaning.

10
00:01:32,200 --> 00:01:56,360
Akasay wa sakuntanal, just as for birds in the sky, gatite san dura naya, their going, their destination

11
00:01:56,360 --> 00:02:02,840
is hard to know.

12
00:02:02,840 --> 00:02:08,840
So in regards to food, the point here is those people who don't hoard and are conscientious

13
00:02:08,840 --> 00:02:17,440
in regards to food, fully conscientious Paringata, Paringata means to know thoroughly.

14
00:02:17,440 --> 00:02:21,680
And you think, well, knowing food thoroughly, it's not all that, it doesn't sound all

15
00:02:21,680 --> 00:02:27,640
that impressive, until you really consider how important food is, what a role it plays

16
00:02:27,640 --> 00:02:28,640
in our lives.

17
00:02:28,640 --> 00:02:38,080
It's really the one thing that, along with water, water and air, I guess, but food is

18
00:02:38,080 --> 00:02:43,080
like the one thing that we really have to look for and work for.

19
00:02:43,080 --> 00:02:50,480
Water theoretically is pretty easy to find, air is still quite easy to find.

20
00:02:50,480 --> 00:02:52,960
But food is something you actually have to work at.

21
00:02:52,960 --> 00:03:03,040
So it is really the one thing that we shouldn't be, that we have to be concerned with.

22
00:03:03,040 --> 00:03:09,680
So it's about the most primal or basic requisite.

23
00:03:09,680 --> 00:03:14,560
And so it stands to reason that it's the one that is closest to our heart and most closely

24
00:03:14,560 --> 00:03:21,560
associated with things like craving and aversion, our likes and dislikes of food are, well,

25
00:03:21,560 --> 00:03:24,960
they're a bit of an issue.

26
00:03:24,960 --> 00:03:26,360
We obsess about food.

27
00:03:26,360 --> 00:03:31,400
It's the one thing that we have to think about, usually three, at least three times a day,

28
00:03:31,400 --> 00:03:32,400
usually more.

29
00:03:32,400 --> 00:03:35,960
We'll have the three meals a day that we are thinking about.

30
00:03:35,960 --> 00:03:42,320
And then on top of that, there are snacks and there are treats.

31
00:03:42,320 --> 00:03:49,600
And then there is the cooking and the preparing and the purchasing and the storing, thinking

32
00:03:49,600 --> 00:03:57,920
about the food that's in our refrigerator, what's in our freezer, what's in the oven,

33
00:03:57,920 --> 00:04:03,760
what foods we like and dislike, thinking about nutrition, it's really a huge subject.

34
00:04:03,760 --> 00:04:08,640
Now we have all sorts of information about food and if you look in the news, it is one

35
00:04:08,640 --> 00:04:13,040
of the topics that it comes up quite often.

36
00:04:13,040 --> 00:04:17,240
This food is found to be bad for you, that food is found to be good for you.

37
00:04:17,240 --> 00:04:27,240
This whole gluten-free thing that some people say is just a big, sort of, noximo or

38
00:04:27,240 --> 00:04:34,720
it doesn't really hurt most people and then other people say it does and it's a big deal

39
00:04:34,720 --> 00:04:37,760
of food is actually a big deal, it's just kind of funny.

40
00:04:37,760 --> 00:04:41,040
I don't think many people think of it like that, I think for the most part we think

41
00:04:41,040 --> 00:04:46,240
about food is just food, it's not a big deal, it's not something that a spiritual person

42
00:04:46,240 --> 00:04:49,360
should really concern themselves with.

43
00:04:49,360 --> 00:04:53,240
In fact, we would go the other way, I think a lot of spiritual people become more obsessed

44
00:04:53,240 --> 00:04:59,840
with food, right, worried because they want to be, they're concerned about themselves

45
00:04:59,840 --> 00:05:08,840
and somehow we equate physical health with mental health, right?

46
00:05:08,840 --> 00:05:14,480
In Buddhism I don't think that's the case, I think physical health is not considered

47
00:05:14,480 --> 00:05:19,760
to be necessarily associated with mental health, especially in regards to food, things

48
00:05:19,760 --> 00:05:30,120
like food, even sickness, like, well any kind of sickness called a flu or a terminal illness

49
00:05:30,120 --> 00:05:41,440
like cancer, so it's, I mean you'd have to say that, you'd have to say in that case

50
00:05:41,440 --> 00:05:46,840
there is some fairly, what sickness there is a clearer picture, a clearer association

51
00:05:46,840 --> 00:05:53,600
because for most people when you're sick it disturbs your mind more than when you're healthy,

52
00:05:53,600 --> 00:05:59,920
but you can argue the other way and say, healthy people have a disturbed mind in a different

53
00:05:59,920 --> 00:06:08,000
way because they become intoxicated with their body, they become considered about it.

54
00:06:08,000 --> 00:06:13,920
But with food, I think there's even less of an argument that can be made that have

55
00:06:13,920 --> 00:06:23,640
a clear indication or a clear relationship between direct relationship between one's nourishment

56
00:06:23,640 --> 00:06:30,400
and one's mental health because over nourishment can lead to this intoxication.

57
00:06:30,400 --> 00:06:43,240
The Buddha said, Manda naya, madaya, no madaya I think is the one, madaya, madaya, it makes

58
00:06:43,240 --> 00:06:55,440
you intoxicated, so, and much more simpler than that, much more basic than that, food is

59
00:06:55,440 --> 00:07:03,760
an obsession, good food, wanting good food, being disgusted by unpleasant food, having

60
00:07:03,760 --> 00:07:10,720
likes and dislikes and preferences, and an obsession about health can actually be a huge

61
00:07:10,720 --> 00:07:19,240
hindrance for the mind, worry and concern and distraction, so you're no longer mindful

62
00:07:19,240 --> 00:07:27,800
to some extent we obsess about it, so knowledge about food and Buddhism isn't like that,

63
00:07:27,800 --> 00:07:33,200
it isn't knowledge of what is nutritious and so on, that's not what is meant here at all.

64
00:07:33,200 --> 00:07:38,720
I haven't told the story yet, so I should probably do that before I talk too much about

65
00:07:38,720 --> 00:07:46,040
that, but this is a verse about food, the third, the second and third lines are not about

66
00:07:46,040 --> 00:07:52,600
food, but the core here is talking about a person who understands food, so what the story

67
00:07:52,600 --> 00:08:02,000
was, it's an interesting story very short, but the preceptor Anand is preceptor Bela, Belat

68
00:08:02,000 --> 00:08:13,720
Sisa, Belat Sisa, Belat Sisa, he was, I think, an arahant, I'm pretty sure, and that's

69
00:08:13,720 --> 00:08:21,400
what makes this story interesting, it's he was an arahant and see in the beginning, in the

70
00:08:21,400 --> 00:08:25,800
beginning, many times the Buddha didn't have rules for the monks, it was just assumed

71
00:08:25,800 --> 00:08:31,320
that they would live properly because they were all enlightened, people would practice,

72
00:08:31,320 --> 00:08:34,640
listen to the Buddha's teaching, become enlightened, he'd ordained them as monks, that's

73
00:08:34,640 --> 00:08:47,440
kind of how it went, but eventually problems arose and situations arose, mostly based on monks

74
00:08:47,440 --> 00:08:52,240
that were not enlightened, so it wasn't that they were should become enlightened first

75
00:08:52,240 --> 00:08:57,880
and then ordained, it's just that there were no unenlightened monks in the beginning, but

76
00:08:57,880 --> 00:09:13,880
eventually that changed, excuse me, and one monk being pestered by his wife, he had been

77
00:09:13,880 --> 00:09:18,840
married before he ordained and his wife came and pestered him to give her a child, actually

78
00:09:18,840 --> 00:09:23,200
to disrobe, but then when he wouldn't disrobe she pestered him to give her a child, and

79
00:09:23,200 --> 00:09:28,840
so he had sex with her, just to get rid of her basically, and she left and that was that,

80
00:09:28,840 --> 00:09:39,720
but the Buddha didn't really take kindly to that and instated the first rule that monks

81
00:09:39,720 --> 00:09:47,160
shall not have sex, and it went from there, in the end we got lots and lots of rules,

82
00:09:47,160 --> 00:09:54,320
but this was in a time when there weren't as many rules as the rules weren't all there yet,

83
00:09:54,320 --> 00:10:02,200
so we have this arrow hunt who was living off in the forest, or up on a mountain maybe,

84
00:10:02,200 --> 00:10:07,880
and he would go into the food for arms and get enough for himself to eat, but then he would

85
00:10:07,880 --> 00:10:13,360
continue on the arms, or he would go put that food, or go eat that food maybe, and then

86
00:10:13,360 --> 00:10:20,280
go on a second arms on a different street, and he would take that food that he got, just

87
00:10:20,280 --> 00:10:32,280
rice basically, and he would dry it in the sun, and then go and meditate, and he would

88
00:10:32,280 --> 00:10:37,720
enter into meditation for days, so he would be in, it said he was in meditation for a

89
00:10:37,720 --> 00:10:51,040
few days I think, and then when he'd come out of, I wouldn't he came out, actually I don't

90
00:10:51,040 --> 00:11:06,040
know, no it doesn't say that, the Dhammapada story says it, but in the wind there it doesn't

91
00:11:06,040 --> 00:11:14,480
say it just says he would, he would dry it, and leave it, put it aside, and then the

92
00:11:14,480 --> 00:11:24,200
next day he would put it in some water and eat it, but his reasoning being, he wouldn't

93
00:11:24,200 --> 00:11:28,280
have to go back into the village and you'd be able to focus on his meditation, it was

94
00:11:28,280 --> 00:11:36,040
really out of just a desire not to get caught up in the village life, and to focus on

95
00:11:36,040 --> 00:11:41,840
his meditation, just to stay in the forest and live in meditation, so somehow he thought

96
00:11:41,840 --> 00:11:48,120
this was a good idea, but it got back to the Buddha, and the Buddha actually scolded him,

97
00:11:48,120 --> 00:11:57,080
scolded an Arahat, wingarahat, wingarahat, wingarahat, wingarahat, it means to abuse, it can mean

98
00:11:57,080 --> 00:12:02,880
it's really scolding, the Buddha gave him, but actually that's what it says, the Buddha

99
00:12:02,880 --> 00:12:08,880
did for everybody who broke a rule, or who did something that was the instigation of a

100
00:12:08,880 --> 00:12:15,280
rule, this is I think one of the only examples of an Arahat being the reason for the creating

101
00:12:15,280 --> 00:12:24,120
of a rule, the Buddha did I think mention that he was, he understood that Belat Tasisa was

102
00:12:24,120 --> 00:12:31,160
blameless, and in fact he never, it was not an offence because there was no offence

103
00:12:31,160 --> 00:12:36,840
to break, and until the Buddha instated the offence, he said before that it wasn't

104
00:12:36,840 --> 00:12:41,000
actually an offence, so I knew when he broke it before that, it wasn't break, it wasn't

105
00:12:41,000 --> 00:12:44,800
doing anything, well, could still say they're doing something wrong, but it wasn't an

106
00:12:44,800 --> 00:13:14,040
offence, so that was the, that was the story behind it, but it says in the, yeah, it says

107
00:13:14,040 --> 00:13:22,720
in the Dhamapada apitchatang nisaya katata, he was doing it out of his, out of a desire for

108
00:13:22,720 --> 00:13:28,520
a fewness of wishes, or out of fewness of wishes, so he was a person who had little greed

109
00:13:28,520 --> 00:13:36,140
and little attachment, but it's a very dangerous precedent because then it leads to monks

110
00:13:36,140 --> 00:13:41,080
hoarding, you know, let's have stores of food so that if I get something good today I can

111
00:13:41,080 --> 00:13:44,640
also have it again tomorrow, if I get something good today and tomorrow I get something

112
00:13:44,640 --> 00:13:51,000
bad, well I can eat yesterday's food today, so that we can store up sweets and salts

113
00:13:51,000 --> 00:13:57,960
and spices and put salt on our food when we, when it's not salty and if it's too bland,

114
00:13:57,960 --> 00:14:05,480
you know, we can put spices in it and so on, if I get something sweet today, it's funny

115
00:14:05,480 --> 00:14:09,560
being among, because some days you get just so much food, like today I went to Stony Creek

116
00:14:09,560 --> 00:14:14,800
and there was so much food, but you can only eat so much and then that's it and the next

117
00:14:14,800 --> 00:14:20,520
day you still have nothing, which is important, it's an important part of the training

118
00:14:20,520 --> 00:14:39,960
to not to be content with whatever, to not have this hanging over you, sort of the sense

119
00:14:39,960 --> 00:14:47,600
of the past, the sense of what you're going to eat in the future, what you've stored

120
00:14:47,600 --> 00:14:57,040
up in the past and thinking about what you've stored up and concerned about it and so on.

121
00:14:57,040 --> 00:15:03,160
It's liberating and it's challenging, right, so it's liberating in the sense that you don't

122
00:15:03,160 --> 00:15:12,680
have to think about cooking or storing, protecting, you don't have to worry about food going

123
00:15:12,680 --> 00:15:18,760
bad or being spoiled, but it's challenging, because that's exactly it, one day you might

124
00:15:18,760 --> 00:15:26,360
not get what you want, quite often, you know, it's not about what you want to eat, it's

125
00:15:26,360 --> 00:15:33,640
about what you get, so I remember there was a period of time in Chom-Tong where all I would

126
00:15:33,640 --> 00:15:38,200
get was sweets, I was just going into the market and I couldn't figure out what I was doing

127
00:15:38,200 --> 00:15:45,400
wrong, but all I would get was, you know, some few sweets to eat and that was all I ate

128
00:15:45,400 --> 00:15:46,400
for the day.

129
00:15:46,400 --> 00:15:50,560
Of course they were rice sweets, so there was a little bit of substance to them, but nothing

130
00:15:50,560 --> 00:15:58,000
real, certainly not nothing like vegetables, protein, so it was a matter of picking through

131
00:15:58,000 --> 00:16:09,520
the sweets and figuring out which ones were most nutritious, but the other day is, you

132
00:16:09,520 --> 00:16:17,320
know, you just get so much food, but it's a complacency thing as well, right, if you've

133
00:16:17,320 --> 00:16:26,200
got lots of food then you begin to, you rely upon it, you take this as a refuge and

134
00:16:26,200 --> 00:16:33,220
it blinds you, or it hides from you the fact that this is ever present, this is an ever

135
00:16:33,220 --> 00:16:38,000
present gnawing on us.

136
00:16:38,000 --> 00:16:48,000
When you give up this support of having constant supply of food that forces you to let

137
00:16:48,000 --> 00:16:55,040
go, forces you not to cling, it forces you to be open to, well, starving, at least going

138
00:16:55,040 --> 00:17:02,560
without food for the day, and it's funny how you get to see how the mind works then because,

139
00:17:02,560 --> 00:17:09,040
you know, there are people who actually purposefully fast for days on end and they're fine

140
00:17:09,040 --> 00:17:14,680
with it, but when it's kind of forced upon you, right, I don't want to, you know, I was

141
00:17:14,680 --> 00:17:21,440
looking for food today and I didn't get any, how your mind winds and complains, right,

142
00:17:21,440 --> 00:17:27,320
because you feel that something's wrong, it's a sense of not getting what you want, whereas

143
00:17:27,320 --> 00:17:31,320
some people are so happy to not eat, they do it for days on end, they'll fast and just

144
00:17:31,320 --> 00:17:39,640
drink fruit juice or something, it's a real test, it shows you your defilements, not

145
00:17:39,640 --> 00:17:47,000
strong food is an awesome practice to keep, and you know, I've been with monks who you can

146
00:17:47,000 --> 00:17:53,080
see where they start to give up the training and they've got like biscuits that they keep

147
00:17:53,080 --> 00:17:57,440
and bread stuffs that they keep aside for the next day to have in the morning with their

148
00:17:57,440 --> 00:18:05,960
coffee, which they're also storing and so I remember, remember in John Tong during this

149
00:18:05,960 --> 00:18:11,200
time where things were, you know, I wasn't getting quite the food that I would assume

150
00:18:11,200 --> 00:18:18,200
to be a proper nutrition and I was struggling with it, but you know, being mindful and

151
00:18:18,200 --> 00:18:23,160
just really working through this and then I, there was a lay person living out behind

152
00:18:23,160 --> 00:18:30,400
the monastery and I went to see him for something he was one of the teachers at the time

153
00:18:30,400 --> 00:18:38,280
and he invited me in for toast, all grain toast and peanut butter and a bowl of cereal

154
00:18:38,280 --> 00:18:45,200
and milk and it just, it was just such a shock like it's quite a different practice I

155
00:18:45,200 --> 00:18:53,760
guess, you know, it's a real other level to your practice having to not having a fringe

156
00:18:53,760 --> 00:19:00,280
to store milk in and that kind of thing, so definitely worth practicing, definitely worth

157
00:19:00,280 --> 00:19:04,480
undertaking this apart, I mean there's so many aspects of the monastic life that are for

158
00:19:04,480 --> 00:19:12,040
that reason beneficial. Anyway, so that was sort of the reasoning behind the rule, I assume

159
00:19:12,040 --> 00:19:18,400
that the Buddha is my understanding of his reasoning for instating the rule and then he

160
00:19:18,400 --> 00:19:27,360
gives this verse on the importance of it, how supportive it is, actually of freedom.

161
00:19:27,360 --> 00:19:35,720
So then he says Sunyato, Animit Oja, Mimo Koye San Goa Chiro, such a person, I mean it's

162
00:19:35,720 --> 00:19:40,840
such a powerful practice, and the Buddha says this several times, he talks about mindfulness,

163
00:19:40,840 --> 00:19:51,720
it doesn't use the word mindfulness, uses Parinyatta or Mata Nuta, understanding, knowing

164
00:19:51,720 --> 00:19:58,080
moderation in food, but here he says, under fully understanding food, there's something

165
00:19:58,080 --> 00:20:04,360
else to do this that I'm also not saying, and Bodhjana can mean not just physical food,

166
00:20:04,360 --> 00:20:11,840
but it can also refer to mental food, right? So this would be sense impressions, sensual

167
00:20:11,840 --> 00:20:20,080
pleasures and suffering, pain and those things that give rise to likes and dislikes, that's

168
00:20:20,080 --> 00:20:25,560
a kind of a food as well, Vedana is a kind of food of feelings, of pleasant and painful

169
00:20:25,560 --> 00:20:37,960
feelings, thoughts, I think intentions are a kind of food because they create a result,

170
00:20:37,960 --> 00:20:44,880
they feed the future. So it's understanding that as well, but even just understanding physical

171
00:20:44,880 --> 00:20:53,280
gross food is important because it's the basic, one of the basic records. And the Buddha

172
00:20:53,280 --> 00:20:59,000
says this kind of understanding, but I would say, if you're going to get into its relationship

173
00:20:59,000 --> 00:21:03,000
between Nubana, probably have to go a little bit deeper and say, yeah, this relates

174
00:21:03,000 --> 00:21:10,760
to anything, because the word food in Pali, ahara means that which brings, hara is to

175
00:21:10,760 --> 00:21:20,040
carry, ahara means to bring. And so it brings about a result, like food, physical food brings

176
00:21:20,040 --> 00:21:26,120
about the body, right? Keep sustains the body. So it brings nourishment to the body in

177
00:21:26,120 --> 00:21:39,880
nourishes, feelings, nourish, you know, attachments, likes and dislikes, consciousness, nourishes

178
00:21:39,880 --> 00:21:46,160
experience. So there's four types of food, there's physical food, there's feelings,

179
00:21:46,160 --> 00:21:54,960
there's consciousness and then there's intentions. So intentions, nourish results. Four types

180
00:21:54,960 --> 00:22:02,120
of food and understanding them is considered, all four of them is considered a primary or

181
00:22:02,120 --> 00:22:07,120
a base for the understanding of reality. So when you understand all four, if you understand

182
00:22:07,120 --> 00:22:13,920
the mind and you understand karma, intentions, how they work, you understand feeling,

183
00:22:13,920 --> 00:22:21,080
you understand the physical aspects of experience. This leads to Vimalka, Vimalka, which

184
00:22:21,080 --> 00:22:28,000
is freedom, is talking about Nibana, Sunyato means signless. This is through seeing nonself.

185
00:22:28,000 --> 00:22:36,240
So it helps you let go of the idea of self. When you give up food, give up the obsession

186
00:22:36,240 --> 00:22:43,680
with pleasing and caring and tending for oneself. You know, often it's, you're better

187
00:22:43,680 --> 00:22:50,880
off being from time to time, malnourished because it tests you, it challenges you, it

188
00:22:50,880 --> 00:22:56,960
opens your mind up, it forces you to let go of this intoxication. I have a beautiful body,

189
00:22:56,960 --> 00:23:04,080
I have a strong body. So many people always tell me I'm getting thinner and I've been

190
00:23:04,080 --> 00:23:07,680
getting this for years that oh you're thinner than last time and have people who tell me

191
00:23:07,680 --> 00:23:15,420
this every year that I'm getting thinner. I just think of that because the way the body

192
00:23:15,420 --> 00:23:22,360
looks is interesting how we tend to apply judgments to the body saying it's like this,

193
00:23:22,360 --> 00:23:33,440
it's thin, it's fat. We're concerned about these sorts of things. And so we become obsessed

194
00:23:33,440 --> 00:23:40,320
with food because we're worried about our bodies and I would say to some extent the obsession,

195
00:23:40,320 --> 00:23:47,280
for example, people who are concerned with their weight and wish they were thinner. I think

196
00:23:47,280 --> 00:23:53,000
that kind of obsession actually makes it worse. It aggravates the situation, makes one more

197
00:23:53,000 --> 00:24:00,080
obsessed with food and as a result makes it harder to eat the right food because you

198
00:24:00,080 --> 00:24:04,880
repress, repress, repress, repress and then you indulge as opposed to just understanding

199
00:24:04,880 --> 00:24:09,440
and saying oh yeah, I only need so much food and as far as my needs go, I really don't

200
00:24:09,440 --> 00:24:18,280
need to eat so much and you just stop. When you understand food, actually food is a big

201
00:24:18,280 --> 00:24:23,240
distraction, right? It's actually huge suffering in their lives and we don't realize it.

202
00:24:23,240 --> 00:24:28,480
So that's a big part of part of not understanding that it's a huge bother. A meditators

203
00:24:28,480 --> 00:24:34,000
will get like this. After a while they say man, I've got to go eat again. I wouldn't

204
00:24:34,000 --> 00:24:45,920
be great if I didn't have to go eat. What a big bother that is. So this kind of understanding

205
00:24:45,920 --> 00:24:50,800
helps us give up self, helps us see impermanence. When you don't store, that's a big one.

206
00:24:50,800 --> 00:25:03,720
Animita means signless. So having no, no, what does it have? No, for knowledge? Was there

207
00:25:03,720 --> 00:25:11,960
no, no, no prior knowledge, not knowing in advance. Having no warning, right? Because normally

208
00:25:11,960 --> 00:25:18,240
when we're storing an imita means we have, we have in our mind a sense. Something tells

209
00:25:18,240 --> 00:25:21,840
us that tomorrow we're going to have pizza because there's pizza in the freezer. So

210
00:25:21,840 --> 00:25:27,240
we're able to know in advance. That's what an imita means. Animita means not being able

211
00:25:27,240 --> 00:25:34,920
to know in advance. It's signless means it comes without, comes unexpectedly without warning.

212
00:25:34,920 --> 00:25:39,160
And so that's how this is. When you stop storing food, then it is, you can't have any

213
00:25:39,160 --> 00:25:43,520
expectations. You can't say tomorrow I want to have pizza. You can, but you'll probably

214
00:25:43,520 --> 00:25:52,120
suffer because you'll probably not get pizza odds are. So it helps with seeing impermanence

215
00:25:52,120 --> 00:25:58,000
as well. And so such people, the Buddha says, this helps us, and along with all kinds

216
00:25:58,000 --> 00:26:07,160
of understanding about food, help us to dwell in this kind of freedom. Just as a bird,

217
00:26:07,160 --> 00:26:11,400
and then he uses this wonderful imagery of just as a bird that flies in the sky without

218
00:26:11,400 --> 00:26:18,880
leaving a trail. So too, it's hard to know the destination of someone. The meaning here

219
00:26:18,880 --> 00:26:25,520
is when they pass away. When they pass away at the very, at the very worst, they will

220
00:26:25,520 --> 00:26:30,920
go to a pure boat. So they'll make go to heaven or they might go to the pure boats where

221
00:26:30,920 --> 00:26:38,080
Anangami's go to or they might not return at all. So much harder to know where they're

222
00:26:38,080 --> 00:26:45,280
headed, because it's harder to know their minds, their minds are so deep and so profound

223
00:26:45,280 --> 00:27:00,800
in their self, in their knowing their possession, their calm and their quieted, that it's

224
00:27:00,800 --> 00:27:08,560
hard to know where they're headed. And when they die, it's hard to know where they've got.

225
00:27:08,560 --> 00:27:18,360
That's the meaning there. So useful for us, while useful for us to be mindful of food,

226
00:27:18,360 --> 00:27:22,080
you know, something that we don't talk about nearly enough, I think. My teacher was always

227
00:27:22,080 --> 00:27:26,240
going on about it. Of course, I think it had a lot to do with monks, storing up and eating

228
00:27:26,240 --> 00:27:31,040
food at the wrong times and that kind of thing. So he was always bringing it up without

229
00:27:31,040 --> 00:27:34,800
really saying, hey, I know you guys are eating at the wrong times and storing up food.

230
00:27:34,800 --> 00:27:40,360
But actually, it's using anyone who would just bring it up like this. But nonetheless,

231
00:27:40,360 --> 00:27:46,360
it is a big deal because food is primary. We don't pay enough attention to it. That's

232
00:27:46,360 --> 00:27:53,160
what eating meditation is very important. I mean, it's important. It should be a part of

233
00:27:53,160 --> 00:28:01,040
our practice. And then the idea that it helps with seeing non-self, because you can't

234
00:28:01,040 --> 00:28:07,400
attach to anything, because you have to stop worrying about yourself or being obsessed

235
00:28:07,400 --> 00:28:13,280
with getting the right nutrition and that kind of thing. And Animita, it shows you impermanence

236
00:28:13,280 --> 00:28:20,080
because you have to be flexible. And such a person, once they reach this, they become

237
00:28:20,080 --> 00:28:24,560
free. And when they're free, you can't, you get no sense of what kind of a person they

238
00:28:24,560 --> 00:28:31,000
are because they are simple. They're just free. I mean, you guess you do get a sense,

239
00:28:31,000 --> 00:28:37,720
you do get a sense that they're very deep and that there's some damage to them. Anyway,

240
00:28:37,720 --> 00:28:43,040
so right. So useful to us. And that's where we're headed. We're headed in that direction

241
00:28:43,040 --> 00:28:51,660
to see impermanence, to see non-self, to give up suffering, to give up the cause, to stop

242
00:28:51,660 --> 00:29:00,160
clinging. And the idea that in the end, we don't leave a trace. I think it's useful

243
00:29:00,160 --> 00:29:05,920
for us. For those people always wondering, how do I know that I'm progressing? Well,

244
00:29:05,920 --> 00:29:14,440
less of a trace you leave. You leave this, this, you know, just like the wind of the wings

245
00:29:14,440 --> 00:29:18,840
of a bird. That's the sort of trace that you leave. So you do affect people, but it doesn't

246
00:29:18,840 --> 00:29:27,080
bother them. You don't cause conflict or friction. You just buff it with your wings. Something

247
00:29:27,080 --> 00:29:38,400
very light and undisturbing. Okay. So that's our Dhamapada verse for tonight. Thank

248
00:29:38,400 --> 00:29:45,120
you all for tuning in and keep practicing. Be well, wishing you all peace, happiness and

249
00:29:45,120 --> 00:30:02,280
freedom from suffering.

