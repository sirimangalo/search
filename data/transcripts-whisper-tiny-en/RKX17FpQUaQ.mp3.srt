1
00:00:00,000 --> 00:00:15,480
The main goal of meditation, as I've said before, is enlightenment or freedom.

2
00:00:15,480 --> 00:00:20,160
The word enlightenment is an interesting word.

3
00:00:20,160 --> 00:00:21,920
It's, of course, an English word.

4
00:00:21,920 --> 00:00:27,880
It's not exactly the word that we used when we talk

5
00:00:27,880 --> 00:00:32,320
in other languages, but it's an interesting word.

6
00:00:32,320 --> 00:00:35,240
Because if you look at the actual word itself,

7
00:00:35,240 --> 00:00:37,000
it could mean one of two things.

8
00:00:37,000 --> 00:00:45,400
Being bright, being removed or taken out of the darkness,

9
00:00:45,400 --> 00:00:49,560
enlightening oneself in terms of gaining wisdom or understanding

10
00:00:49,560 --> 00:00:51,920
or light.

11
00:00:51,920 --> 00:00:54,800
And the other one becoming lighter or giving up

12
00:00:54,800 --> 00:00:57,520
one's burdens.

13
00:00:57,520 --> 00:01:00,840
And both of these definitions, I think, are quite

14
00:01:00,840 --> 00:01:08,720
apt for the meaning to explain the meaning of the result

15
00:01:08,720 --> 00:01:10,760
of meditation, the fruit of meditation,

16
00:01:10,760 --> 00:01:12,960
which we're all wishing to attain.

17
00:01:15,880 --> 00:01:20,160
The first one is because we're practicing to gain wisdom.

18
00:01:20,160 --> 00:01:23,600
We're not practicing just to sit around and feel peaceful

19
00:01:23,600 --> 00:01:28,440
or to gain a break or a rest from our troubles for a while.

20
00:01:28,440 --> 00:01:30,920
As I've said before, we're looking

21
00:01:30,920 --> 00:01:35,440
to really and truly understand how our reality works,

22
00:01:35,440 --> 00:01:41,760
how our mind works, how our physical surroundings work,

23
00:01:41,760 --> 00:01:43,680
and how we interact with them.

24
00:01:43,680 --> 00:01:45,600
And coming to see things as they are,

25
00:01:45,600 --> 00:01:47,760
coming to see the truth about our emotions,

26
00:01:47,760 --> 00:01:51,320
the truth about our obsessions, the truth about our versions,

27
00:01:51,320 --> 00:01:54,480
the truth about our fears and our worries and our concerns.

28
00:01:54,480 --> 00:01:56,520
And coming to see the true nature of the things which

29
00:01:56,520 --> 00:02:00,000
we worry and concern and are afraid and depressed

30
00:02:00,000 --> 00:02:02,760
and confused about.

31
00:02:02,760 --> 00:02:05,720
And once we come to see these things clearly, of course,

32
00:02:05,720 --> 00:02:09,120
we're able to approach every experience

33
00:02:09,120 --> 00:02:11,520
in a much more enlightened manner.

34
00:02:11,520 --> 00:02:15,720
It's like it feels like or it appears to the meditator

35
00:02:15,720 --> 00:02:18,920
or something akin to a person coming out of a dark room

36
00:02:18,920 --> 00:02:21,320
where before they were in total darkness,

37
00:02:21,320 --> 00:02:24,360
they were living their lives bumping into things,

38
00:02:24,360 --> 00:02:26,120
going around like a person in a dark room

39
00:02:26,120 --> 00:02:28,400
or a person blindfolded.

40
00:02:28,400 --> 00:02:34,120
And so they would act like a chicken with its head cut off

41
00:02:34,120 --> 00:02:38,280
or an animal that has no intelligence and no wisdom,

42
00:02:38,280 --> 00:02:39,440
no understanding.

43
00:02:39,440 --> 00:02:41,160
And after they practiced meditation,

44
00:02:41,160 --> 00:02:42,920
it's like suddenly someone turned on the light

45
00:02:42,920 --> 00:02:47,320
and they're able to see and they're able to avoid dangers

46
00:02:47,320 --> 00:02:51,000
and avoid suffering because they see the truth

47
00:02:51,000 --> 00:02:52,200
of the situation.

48
00:02:52,200 --> 00:02:55,080
They see things as they are and how they work.

49
00:02:55,080 --> 00:02:58,000
They see their own emotions and so on.

50
00:02:58,000 --> 00:03:01,160
The other definition, equally at apt,

51
00:03:01,160 --> 00:03:03,200
but for a totally different reason,

52
00:03:03,200 --> 00:03:09,840
is the idea of giving up on burdens.

53
00:03:09,840 --> 00:03:12,840
And that's sort of intertwined, but the idea

54
00:03:12,840 --> 00:03:16,400
is here that we're giving up our attachments

55
00:03:16,400 --> 00:03:21,400
or our predilections, our obsessions with things

56
00:03:21,560 --> 00:03:24,840
whereas normally we carry around so much baggage with us.

57
00:03:24,840 --> 00:03:26,960
We talk about emotional baggage,

58
00:03:26,960 --> 00:03:30,320
but we can also think of all of our physical possessions

59
00:03:30,320 --> 00:03:32,800
and the people and the places and the things

60
00:03:32,800 --> 00:03:35,880
which we cling to as a kind of a baggage

61
00:03:35,880 --> 00:03:39,840
or a burden that we have to carry around and care for

62
00:03:39,840 --> 00:03:43,680
and obsess about.

63
00:03:43,680 --> 00:03:44,960
We take even just the body,

64
00:03:44,960 --> 00:03:47,880
we're spending all of our time grooming and cleaning

65
00:03:47,880 --> 00:03:50,280
and fussing over our body,

66
00:03:50,280 --> 00:03:52,680
making sure that it's in perfect health,

67
00:03:52,680 --> 00:03:54,920
in perfect condition, perfect weight,

68
00:03:54,920 --> 00:03:56,760
perfect color and so on,

69
00:03:56,760 --> 00:03:59,120
even to the point of our hair and our clothes

70
00:03:59,120 --> 00:03:59,960
and so on.

71
00:03:59,960 --> 00:04:01,640
We have to decorate ourselves out.

72
00:04:01,640 --> 00:04:02,800
It becomes a real burden.

73
00:04:02,800 --> 00:04:07,600
It's something that we have to worry and fret over

74
00:04:07,600 --> 00:04:08,960
and stress over.

75
00:04:08,960 --> 00:04:10,760
All of the people around us are the same.

76
00:04:10,760 --> 00:04:13,680
They become a burden when we are concerned and worried

77
00:04:13,680 --> 00:04:16,880
and our happiness depends on theirs

78
00:04:16,880 --> 00:04:20,160
or our happiness depends on their appearance

79
00:04:20,160 --> 00:04:21,760
and on their behavior.

80
00:04:21,760 --> 00:04:26,760
Our happiness depends on our friendship with them

81
00:04:26,760 --> 00:04:31,520
when we are dependent on them acting in a certain way

82
00:04:31,520 --> 00:04:32,800
and not acting in another way.

83
00:04:32,800 --> 00:04:33,800
This is a real burden.

84
00:04:33,800 --> 00:04:35,240
It's a weight on us.

85
00:04:35,240 --> 00:04:36,400
And when we're able to give this up

86
00:04:36,400 --> 00:04:40,280
and be without say the most beautiful body

87
00:04:40,280 --> 00:04:44,080
or the most beautiful clothes or the greatest friends

88
00:04:44,080 --> 00:04:47,800
or without people who are pleasing to us,

89
00:04:47,800 --> 00:04:49,200
when we're able to just be

90
00:04:49,200 --> 00:04:51,560
and let things come and go

91
00:04:51,560 --> 00:04:53,800
when our possessions are not so important.

92
00:04:55,000 --> 00:04:57,120
And of course, when we're able to give up our emotional

93
00:04:57,120 --> 00:05:00,200
baggage as well, once we gain wisdom and understanding.

94
00:05:00,200 --> 00:05:04,120
Obviously, other important aspect of wisdom,

95
00:05:05,920 --> 00:05:08,360
meaning that wisdom is in and of itself,

96
00:05:08,360 --> 00:05:11,120
not truly a valid goal

97
00:05:11,120 --> 00:05:12,960
because just because your wife doesn't

98
00:05:12,960 --> 00:05:15,760
in and of itself mean happiness.

99
00:05:15,760 --> 00:05:17,400
But it necessitates happiness

100
00:05:17,400 --> 00:05:19,880
because it relieves the burden

101
00:05:19,880 --> 00:05:22,960
after you have wisdom and understanding

102
00:05:22,960 --> 00:05:25,040
you're freed from this whole emotional baggage

103
00:05:25,040 --> 00:05:28,520
that comes from our sessions and our attachments and things.

104
00:05:28,520 --> 00:05:32,960
As we have an understanding of the situations around us,

105
00:05:32,960 --> 00:05:35,280
we approach things without any emotional baggage.

106
00:05:35,280 --> 00:05:39,560
We approach things with wisdom with clear sense

107
00:05:39,560 --> 00:05:41,760
of how things are and what things are.

108
00:05:41,760 --> 00:05:46,000
Clear sense of the nature of the situation.

109
00:05:46,000 --> 00:05:50,200
So we live our lives free from all of the baggage

110
00:05:52,160 --> 00:05:53,240
as we become enlightened.

111
00:05:54,920 --> 00:05:57,000
So I wanted to explain this

112
00:05:57,000 --> 00:05:59,040
to sort of give an idea that enlightenment

113
00:05:59,040 --> 00:06:00,440
does not something foreign.

114
00:06:00,440 --> 00:06:02,840
It's not something mystical or magical.

115
00:06:02,840 --> 00:06:06,200
Even the idea of Nirvana or Nirvana is simply freedom

116
00:06:06,200 --> 00:06:08,600
from suffering because of enlightenment,

117
00:06:08,600 --> 00:06:11,320
because one has become free to one's burdens,

118
00:06:11,320 --> 00:06:14,520
because one has become wise

119
00:06:14,520 --> 00:06:16,680
and has turned on the light

120
00:06:16,680 --> 00:06:21,360
and their mind has gained the brilliance of wisdom.

121
00:06:22,200 --> 00:06:24,440
It's something that comes from simply observing,

122
00:06:24,440 --> 00:06:27,400
simply watching, learning, looking deeper

123
00:06:27,400 --> 00:06:31,840
and looking in a clear empirical,

124
00:06:31,840 --> 00:06:36,840
experimental way where we accept things

125
00:06:37,240 --> 00:06:40,200
or acknowledge things for what they are.

126
00:06:40,200 --> 00:06:42,800
So as we explained in meditation,

127
00:06:42,800 --> 00:06:45,360
we feel pain, we just know it as pain

128
00:06:45,360 --> 00:06:46,960
when we feel the movements of the body,

129
00:06:46,960 --> 00:06:49,440
even we see them just as the movements of the body.

130
00:06:49,440 --> 00:06:51,800
When we're thinking, we see it just as thinking.

131
00:06:51,800 --> 00:06:54,200
When we have emotions, we see them just as emotions.

132
00:06:54,200 --> 00:06:57,080
When we see, when we hear, when we smell,

133
00:06:57,080 --> 00:06:58,840
when we taste, when we feel and we think things,

134
00:06:58,840 --> 00:07:01,200
we see it simply for what it is as it is.

135
00:07:01,200 --> 00:07:02,880
And as we look closer and closer,

136
00:07:02,880 --> 00:07:07,120
as we see clear and clear, wisdom can't but arise

137
00:07:07,120 --> 00:07:09,400
because we're looking at things simply for what they are.

138
00:07:09,400 --> 00:07:12,160
We have an objective outlook

139
00:07:12,160 --> 00:07:16,320
and we are objectively studying, analyzing

140
00:07:16,320 --> 00:07:19,720
and learning more about ourselves,

141
00:07:19,720 --> 00:07:23,080
more about the phenomena that arise

142
00:07:23,080 --> 00:07:25,560
in our everyday experience.

143
00:07:25,560 --> 00:07:27,720
So I hope this has been of some help,

144
00:07:27,720 --> 00:07:29,600
just as sort of an addition to the videos

145
00:07:29,600 --> 00:07:33,360
and how to meditate, one more in this series

146
00:07:34,520 --> 00:07:38,040
on how and why everyone should practice meditation.

147
00:07:38,040 --> 00:07:39,520
So thanks for tuning in,

148
00:07:39,520 --> 00:07:43,720
and forward to more videos in the future.

149
00:07:43,720 --> 00:08:07,600
Thanks, have a good day.

