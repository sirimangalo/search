1
00:00:00,000 --> 00:00:04,340
Hello and welcome back to our study of the Dhamapada.

2
00:00:04,340 --> 00:00:13,480
Today we continue on with verses 1.53 and 1.54, which read its follows.

3
00:00:13,480 --> 00:00:41,000
Today we will continue on with the Dhamapada and the Dhamapada and the Dhamapada and the Dhamapada.

4
00:00:41,000 --> 00:00:51,000
We will continue on with the Dhamapada and the Dhamapada.

5
00:00:51,000 --> 00:01:06,160
Which means, in the Samsara wandering on through many lives,

6
00:01:06,160 --> 00:01:16,160
they kajati. Sendhawi's song running around wandering around Anibhi's song finding nothing.

7
00:01:16,160 --> 00:01:21,360
Seeking out in the house builder, the Gahakar,

8
00:01:21,360 --> 00:01:33,160
seeking out the builder of this house, suffering, suffering in birth, again and again,

9
00:01:33,160 --> 00:01:40,160
or having suffering born, again and again, suffering arising, again and again.

10
00:01:40,160 --> 00:01:45,160
So much suffering seeking out the house builder.

11
00:01:45,160 --> 00:01:51,160
Gahakaraka, Ditosi, I see you, house builder.

12
00:01:51,160 --> 00:01:55,160
You are seen, you are seen, house builder.

13
00:01:55,160 --> 00:02:03,160
Puna Gahakaraka, I see you will not build the house again, a home again.

14
00:02:03,160 --> 00:02:08,160
All of your roof beams are shattered.

15
00:02:08,160 --> 00:02:13,160
Your peak destroyed.

16
00:02:13,160 --> 00:02:25,160
Your peak has been demolished.

17
00:02:25,160 --> 00:02:34,160
The mind that has gone to Wisankara has been deconstructed, let's say.

18
00:02:34,160 --> 00:02:39,160
So your peak has been deconstructed, taken apart.

19
00:02:39,160 --> 00:02:48,160
Because then you use the same word, Wisankara, Ditang, the mind that has been taken apart, deconstructed,

20
00:02:48,160 --> 00:02:55,160
obtains the destruction of creating.

21
00:02:55,160 --> 00:03:10,160
Dhanhana and Kailamunjaka.

22
00:03:10,160 --> 00:03:13,160
It's an important pair of verses.

23
00:03:13,160 --> 00:03:21,160
Hopefully it sounds as wonderfully profound as it actually is.

24
00:03:21,160 --> 00:03:34,440
And he said, as a Buddha, and the kajati sangsara.

25
00:03:34,440 --> 00:03:37,360
The story of the Buddha is quite interesting.

26
00:03:37,360 --> 00:03:46,120
We can recap it briefly in a few minutes, a few seconds.

27
00:03:46,120 --> 00:03:56,400
Many lifetimes ago, Anika Jati, the Bodhisatta, this ascetic, Sumeda, heard about a Buddha.

28
00:03:56,400 --> 00:04:02,120
And he went to see the Buddha, but he looked at the Buddha and he thought to himself,

29
00:04:02,120 --> 00:04:09,720
if I were to learn from this Buddha, I could become enlightened.

30
00:04:09,720 --> 00:04:19,240
He thought to himself, I'm so powerful, so well-advanced in my spirituality,

31
00:04:19,240 --> 00:04:23,480
what if I were to try to become a Buddha myself?

32
00:04:23,480 --> 00:04:25,320
And so he made a vow then and there.

33
00:04:25,320 --> 00:04:30,640
That was affirmed by that Buddha, Deepankara, to become a Buddha himself.

34
00:04:30,640 --> 00:04:36,480
Then he spent countless lifetimes seeking out,

35
00:04:36,480 --> 00:04:42,240
seeking out the house builder.

36
00:04:42,240 --> 00:04:48,280
And till finally he found it under the Bodhi tree, he was born in Siddhartha,

37
00:04:48,280 --> 00:04:53,880
in the Gautama clan, and at 29 years old years of age,

38
00:04:53,880 --> 00:05:00,400
he left home, went into the forest, tried many ways of becoming enlightened

39
00:05:00,400 --> 00:05:08,040
for six years and failed, until finally he realized that he should give up extreme practices.

40
00:05:08,040 --> 00:05:14,280
He shouldn't strive to break himself, he should strive to understand himself.

41
00:05:14,280 --> 00:05:18,800
So he found a middle way.

42
00:05:18,800 --> 00:05:24,160
And lo and behold, he was broken.

43
00:05:24,160 --> 00:05:32,320
The house broke itself, he didn't have to destroy it through force,

44
00:05:32,320 --> 00:05:36,720
just through the understanding and knowledge and wisdom,

45
00:05:36,720 --> 00:05:45,600
broke down the walls and the roof.

46
00:05:45,600 --> 00:05:52,240
So this is a verse common to all Buddhas, it describes the attainment of Buddhahood.

47
00:05:52,240 --> 00:06:00,480
But more than that, it describes some very important quality of Buddhist meditation practice,

48
00:06:00,480 --> 00:06:08,440
or Buddhist theory, philosophy.

49
00:06:08,440 --> 00:06:17,480
And that is the constructing and deconstructing of the self, really, of identity.

50
00:06:17,480 --> 00:06:21,400
What the Buddha is talking here is about the self.

51
00:06:21,400 --> 00:06:32,280
This idea of self, identity that we have, and thinking and talking and mulling over debating

52
00:06:32,280 --> 00:06:39,240
this concept of identity, hearing a lot about identity, protecting people's identity

53
00:06:39,240 --> 00:06:48,240
and affirming one's own identity, clinging to one's own identity,

54
00:06:48,240 --> 00:06:53,560
and then defying with one's views, with one's opinions.

55
00:06:53,560 --> 00:07:01,280
It's perhaps a defilement or a problem that we don't pay enough attention to.

56
00:07:01,280 --> 00:07:07,440
We don't talk enough about in Buddhism that we should perhaps talk more about.

57
00:07:07,440 --> 00:07:11,840
Often much more concerned with greed and anger, they're very easy to.

58
00:07:11,840 --> 00:07:19,840
And anger are much easier to identify, and if I much easier to recognize, it's much more

59
00:07:19,840 --> 00:07:34,480
difficult to catch the subtle identification and attachment to self and personality.

60
00:07:34,480 --> 00:07:41,640
It's not something that's easy to be mindful of, it's something that requires clarity

61
00:07:41,640 --> 00:07:58,280
to overcome, requires a sharp mind seeing moment by moment, just like a person would

62
00:07:58,280 --> 00:08:08,280
take apart a Ruth piece by piece, one has to take apart one's reality moment by moment,

63
00:08:08,280 --> 00:08:17,920
a piece by piece, and understand not the self, but understand that which creates the

64
00:08:17,920 --> 00:08:29,880
self, the house builder, Gahakara, Gahakara.

65
00:08:29,880 --> 00:08:36,120
And so ultimately, identities are a very, very problematic thing in its, in no way any

66
00:08:36,120 --> 00:08:46,440
good identification, except in a functional sense where I say I'm among, has a functional

67
00:08:46,440 --> 00:08:53,880
practical reality, it's not, it's not even of itself a problem, but that's not identification

68
00:08:53,880 --> 00:09:04,440
that, that's, um, convenience. Identification means when you take something and you say this is

69
00:09:04,440 --> 00:09:10,760
me, this is mine, where you have some, some measure of clinging to it, where you place some

70
00:09:10,760 --> 00:09:21,840
importance on that, like if I say, well this is my body and this is my road, if I don't

71
00:09:21,840 --> 00:09:25,920
place any importance on that and I'm just clear that, okay, this is the road I can wear

72
00:09:25,920 --> 00:09:30,120
because it's not his road, I can't wear that road because it's his, otherwise it causes

73
00:09:30,120 --> 00:09:40,240
all sorts of confusion and perhaps problems. If I look at my hand and I see there's a cut

74
00:09:40,240 --> 00:09:43,880
or something, I have to recognize, oh that's my body, I have to clean that cut, I have

75
00:09:43,880 --> 00:09:54,640
to care for it, but if I don't describe importance to it, it's, it only becomes a problem

76
00:09:54,640 --> 00:10:01,800
when we, when we attach to it, when there's some importance, when it takes on some importance

77
00:10:01,800 --> 00:10:13,200
beyond the obvious, where we see something and we have some expectation or some clinging

78
00:10:13,200 --> 00:10:21,520
to it. I think it was rise then of course to liking it and disliking it and being pleased

79
00:10:21,520 --> 00:10:38,680
and displeased, preferring this, preferring that. Identification is sort of the root of

80
00:10:38,680 --> 00:10:53,160
all of the other problems that arise in the filaments of greed and anger because we

81
00:10:53,160 --> 00:11:01,280
wouldn't, we don't cling to things except to think of them as ours or to wish for them

82
00:11:01,280 --> 00:11:10,040
to be ours and someone else gets hurt. It only upsets us in so far as we can relate

83
00:11:10,040 --> 00:11:26,520
and so far as we can think of ourselves being hurt. Identity is a cause for such great suffering.

84
00:11:26,520 --> 00:11:34,880
When that which keeps us bound to some sort, we cling to ideas of who we are, what we

85
00:11:34,880 --> 00:11:57,720
are, the cling to ideas of how good we are, how of our worth, how worth we are, worthless we are.

86
00:11:57,720 --> 00:12:05,640
We can see that ourselves in many different ways as men, as women or neither men nor women

87
00:12:05,640 --> 00:12:12,040
but something else. Either way it's all identity. We identify ourselves politically, we

88
00:12:12,040 --> 00:12:19,880
identify ourselves socially, we identify ourselves by our age, by our skin color, by

89
00:12:19,880 --> 00:12:26,680
our ethnicity, by our religion, by our views and beliefs. We identify ourselves with

90
00:12:26,680 --> 00:12:38,840
ideas, we cling to them. It's very difficult to adapt. If someone calls into question

91
00:12:38,840 --> 00:12:46,360
your identity, which is what we see a lot now, when someone your identity is threatened,

92
00:12:46,360 --> 00:12:59,480
it becomes quite upset. No question, there's no question that this is a problem. There's

93
00:12:59,480 --> 00:13:05,040
no question that this is something to be understood and to free oneself from other than

94
00:13:05,040 --> 00:13:17,920
to cling to and to pride oneself on. Be proud that I'm a monk, proud that I was born

95
00:13:17,920 --> 00:13:29,560
Jewish, proud that I'm white, sort of white, pink maybe, proud that I'm a man, proud that

96
00:13:29,560 --> 00:13:37,440
I'm so smart, proud that I'm a teacher. So many things to be proud of. There's this

97
00:13:37,440 --> 00:13:46,040
old gay pride movement right? And that's the bad side of it really is that it does create

98
00:13:46,040 --> 00:13:54,520
sort of a pride or what it's meant to counter is the insecurity and sometimes self-hatred

99
00:13:54,520 --> 00:14:11,840
and loathing. And so it's important to be careful of these things. And it's an interesting

100
00:14:11,840 --> 00:14:20,560
sort of extreme example of gay pride, I think, because there's an issue being addressed

101
00:14:20,560 --> 00:14:29,160
whereby they're meant to feel worthless, perverted, evil. They're made to feel guilty people

102
00:14:29,160 --> 00:14:34,480
who are deviant. And that's, they really are deviant in the sense that most people, the

103
00:14:34,480 --> 00:14:42,880
majority of people don't have that attraction to the same gender or whatever, although

104
00:14:42,880 --> 00:14:51,840
funny, you know, we all have crazy when it's thrown crazy, but made to feel bad about it.

105
00:14:51,840 --> 00:14:59,480
And so this pride is addressing that. It's important to acknowledge that. It's important,

106
00:14:59,480 --> 00:15:05,160
and this is why I think it's important to acknowledge people's identity. It's important

107
00:15:05,160 --> 00:15:12,320
to allow people the freedom to breathe as they are and to not feel bad about who they

108
00:15:12,320 --> 00:15:20,160
are or feel somehow left out. It's important to make people feel proud of themselves

109
00:15:20,160 --> 00:15:30,280
to the sense, just as a means of creating the mental stability required to deconstruct

110
00:15:30,280 --> 00:15:35,160
the self, right? If you're constantly being told that you're worthless, that you're perverse,

111
00:15:35,160 --> 00:15:43,560
that you're bad and evil, it can't possibly ever. It's much more difficult to have a

112
00:15:43,560 --> 00:15:52,320
whole, a healthy state by which you can come to understand yourself, but who am I? If you

113
00:15:52,320 --> 00:15:59,760
have these desires and the desires that we all have, if you're told that they're wrong,

114
00:15:59,760 --> 00:16:09,840
which we often are, sexual desires, naughty and dirty. If you're constantly feel that and

115
00:16:09,840 --> 00:16:14,480
you feel the guilt, how could you ever possibly be objective about it and understand it?

116
00:16:14,480 --> 00:16:21,720
So that's what that's addressing, I think. I think it's important. But it's even more important

117
00:16:21,720 --> 00:16:30,320
and on a deeper level, important, that we be clear that our ultimate aim should be

118
00:16:30,320 --> 00:16:35,240
afraid to free ourselves from any sort of identity, because it's never good or useful.

119
00:16:35,240 --> 00:16:44,600
It doesn't have any ultimate purpose besides to create pride, conceit, vanity, none of

120
00:16:44,600 --> 00:16:53,360
which are useful and beneficial to us or others, all of which are harmful, all of which are

121
00:16:53,360 --> 00:17:01,120
the true problem, right? If you don't have any attachment, if you're not attached to your

122
00:17:01,120 --> 00:17:06,240
body, then when someone calls you fat or thin or white or black, it doesn't really bother you.

123
00:17:06,240 --> 00:17:21,640
You know, they even, sorry, put this set of, I think, sorry, put them in them. If someone

124
00:17:21,640 --> 00:17:30,480
were to beat you, someone were to hit you, what are they hitting? They're just hitting

125
00:17:30,480 --> 00:17:37,440
a body, it's just, they're giving rise to certain experiences. If you don't attach to the

126
00:17:37,440 --> 00:17:52,360
body, it's not possible to, it's not possible to be tortured. And so the ultimate goal

127
00:17:52,360 --> 00:18:02,800
and the ultimate solution is to free ourselves from any sort of identity. It really is

128
00:18:02,800 --> 00:18:12,760
this deconstructing that the Buddha is talking about. It's the most profound aspect of the

129
00:18:12,760 --> 00:18:17,600
meditation when you're able to break down who you are deconstruct who you are and to

130
00:18:17,600 --> 00:18:25,960
constituent parts and see that we're made up of, mainly a mishmash of habits and patterns

131
00:18:25,960 --> 00:18:33,240
of behavior that are very much out of our control. We have a power of their own and we've

132
00:18:33,240 --> 00:18:47,760
been feeding that power for far too long. And when we stop feeding it, when we get rid of

133
00:18:47,760 --> 00:18:55,200
this thirst, it's craving. And when we get rid of ignorance, when we understand craving,

134
00:18:55,200 --> 00:19:01,800
when we understand the results of craving, we understand the stress and the suffering that

135
00:19:01,800 --> 00:19:14,080
comes from craving and clinging. And when we understand identity, we understand the results

136
00:19:14,080 --> 00:19:27,440
of identifying with things with the consequences of and the vulnerability of identity. Then

137
00:19:27,440 --> 00:19:47,400
we let go of it. We drop it like a hot coal and break down the house. So a very interesting

138
00:19:47,400 --> 00:19:52,520
verse and interesting, and that it helps us remember this idea of identity, helps us think

139
00:19:52,520 --> 00:20:00,160
about it and apply this to our meditation practice to see how we identify with things

140
00:20:00,160 --> 00:20:05,480
and to remind us that none of this is ours or me or mine. And the identification with

141
00:20:05,480 --> 00:20:12,280
anything is just a cause for confusion, stress, suffering. When you think of the people in

142
00:20:12,280 --> 00:20:16,920
the world who are tortured because of their identity or their feeling they don't have

143
00:20:16,920 --> 00:20:23,440
an identity or that their identity is threatened or denied that kind of thing, how much

144
00:20:23,440 --> 00:20:30,080
they suffer. We should be able to empathize and sympathize and use it as an example for

145
00:20:30,080 --> 00:20:39,840
ourselves to realize that this is something that we'd be better off without any sort

146
00:20:39,840 --> 00:20:50,400
of identification. And sort of identity. So there you go. That's my take on 153 and 154.

147
00:20:50,400 --> 00:21:14,440
I want to thank you all for tuning in. Wish you all the best.

