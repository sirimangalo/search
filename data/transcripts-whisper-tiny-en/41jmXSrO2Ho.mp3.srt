1
00:00:00,000 --> 00:00:04,280
When I meditate, I usually do it laying down.

2
00:00:04,280 --> 00:00:08,480
I do it this way because I like lots of concentration.

3
00:00:08,480 --> 00:00:13,360
Sometimes I do change positions about of a need, out of need.

4
00:00:13,360 --> 00:00:14,360
Is it a bad thing?

5
00:00:14,360 --> 00:00:15,760
I do it this way.

6
00:00:15,760 --> 00:00:18,280
I think it's got a problem.

7
00:00:18,280 --> 00:00:23,880
The first key is the part where you say because I like lots of concentration.

8
00:00:23,880 --> 00:00:28,240
This kind of person I would be kind of mean and I would say stop lying down.

9
00:00:28,240 --> 00:00:30,800
That's what you like to do.

10
00:00:30,800 --> 00:00:35,720
We want to see what happens when you do something that you don't like to do.

11
00:00:35,720 --> 00:00:40,720
And we want to show you what you've been building up based on your attachments because

12
00:00:40,720 --> 00:00:44,120
an enlightened being can be happy anywhere.

13
00:00:44,120 --> 00:00:49,600
So if you have preference for something, let's see what happens when you don't get it.

14
00:00:49,600 --> 00:00:55,800
That's kind of mean I suppose, but it's pertinent.

15
00:00:55,800 --> 00:01:01,760
Looking down, there's no problem with doing lying meditation, theoretically.

16
00:01:01,760 --> 00:01:08,560
But in practice, it's quite dangerous because it can lead to falling asleep, it can

17
00:01:08,560 --> 00:01:10,320
lead to daydreaming.

18
00:01:10,320 --> 00:01:18,840
It's the position where the mind is most accustomed to freedom, not having any kind of troubles

19
00:01:18,840 --> 00:01:20,560
or difficulties.

20
00:01:20,560 --> 00:01:27,560
So there's very little potential to develop wisdom or to develop pronunciation, to develop

21
00:01:27,560 --> 00:01:34,040
patients, to develop all sorts of good qualities and to let go because there's nothing

22
00:01:34,040 --> 00:01:37,760
that needs to be let go at that time.

23
00:01:37,760 --> 00:01:42,480
What you can do is focus on the liking when you lie down, it can be quite pleasant and

24
00:01:42,480 --> 00:01:48,000
so it can be useful to focus on that experience and to try to free yourself from the

25
00:01:48,000 --> 00:01:50,920
clinging to it.

26
00:01:50,920 --> 00:02:04,240
Lying is best used for people who have stressful jobs or lifestyles, who need concentration,

27
00:02:04,240 --> 00:02:07,920
or their mind is working.

28
00:02:07,920 --> 00:02:12,840
People have, for example, ADHD might benefit a lot from doing lying meditation because

29
00:02:12,840 --> 00:02:16,760
they need the concentration, they need to balance it.

30
00:02:16,760 --> 00:02:21,560
So if your mind is racing, you can try doing lying meditation, but if you're tired or if

31
00:02:21,560 --> 00:02:29,560
you're kind of relaxed, I would work on the energy side, so do walking meditation.

32
00:02:29,560 --> 00:02:34,800
Walking meditation is probably not as peaceful or comfortable for you, but that's really

33
00:02:34,800 --> 00:02:35,800
the point.

34
00:02:35,800 --> 00:02:40,880
The point is to see what your mind does when the body is not comfortable or not it's

35
00:02:40,880 --> 00:02:49,640
not not comfortable when the body has to face difficulties.

36
00:02:49,640 --> 00:02:53,120
And you can sort of see how it's kind of spoiling you because the other part is where

37
00:02:53,120 --> 00:02:56,800
you talk about changing positions.

38
00:02:56,800 --> 00:03:03,440
So what you're experiencing is the fact that even though it's pleasant to lie down, it's

39
00:03:03,440 --> 00:03:12,920
really not satisfying, and if you were to lie down for, say, six hours without a sleep,

40
00:03:12,920 --> 00:03:17,320
if you were to lie there and meditate for, say, six hours, 12 hours, some people do it

41
00:03:17,320 --> 00:03:23,320
for 24 hours, I've heard of someone doing lying meditation for 24 hours.

42
00:03:23,320 --> 00:03:27,320
It becomes quite difficult to stay in one posture.

43
00:03:27,320 --> 00:03:29,800
The meaning is that it can't satisfy you.

44
00:03:29,800 --> 00:03:31,920
It's not really pleasant.

45
00:03:31,920 --> 00:03:36,560
You still have to always change your posture and find a new posture that's more comfortable

46
00:03:36,560 --> 00:03:44,520
and get a nicer pillow and switch from an ordinary mattress to a water bed or so on and

47
00:03:44,520 --> 00:03:45,920
so on and so on.

48
00:03:45,920 --> 00:03:50,240
Then it's too hot, then it's too cold, then you're thirsty, then you're hungry, then you

49
00:03:50,240 --> 00:03:54,800
have to go to the washroom, then you're bored and you want to get up and check Facebook

50
00:03:54,800 --> 00:04:01,040
or YouTube.

51
00:04:01,040 --> 00:04:06,080
So nothing wrong with lying meditation, but you should understand how it's used.

52
00:04:06,080 --> 00:04:11,560
The fact that you like concentration is a bit of a problem because it becomes an attachment.

53
00:04:11,560 --> 00:04:13,520
You should try to let go of that.

54
00:04:13,520 --> 00:04:19,280
It would be probably better to do some walking meditation.

55
00:04:19,280 --> 00:04:22,920
You'd get to see things about your mind that you couldn't see before, then you'll be

56
00:04:22,920 --> 00:04:36,080
able to see your attachments to the concentration, for example.

