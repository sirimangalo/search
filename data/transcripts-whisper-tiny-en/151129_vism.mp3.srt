1
00:00:00,000 --> 00:00:13,760
Okay, and we are starting chapter 11 in the Visudhi Maga page 337, and a brewer of ice cream.

2
00:00:13,760 --> 00:00:17,280
Could you start us off?

3
00:00:17,280 --> 00:00:24,760
Chapter 11, Concentration, Conclusion, Nutriment and the Elements, Perception of Repulsiveness

4
00:00:24,760 --> 00:00:27,200
and Nutriment.

5
00:00:27,200 --> 00:00:32,560
Now come the description of the development of the perception of repulsiveness in Nutriment,

6
00:00:32,560 --> 00:00:38,680
which was listed as the one perception next to the immaterial states.

7
00:00:38,680 --> 00:00:49,120
Here in it nourishes Ahara-ti, literally brings on, thus it is Nutriment Ahara, literally

8
00:00:49,120 --> 00:00:50,880
bringing on.

9
00:00:50,880 --> 00:00:57,600
That is of four kinds as physical Nutriment, Nutriment consisting of contact, Nutriment

10
00:00:57,600 --> 00:01:10,080
consisting of mental, volition, and Nutriment consisting of consciousness, consciousness.

11
00:01:10,080 --> 00:01:21,800
But what is it here that nourishes, bring on what, physical Nutriment, kavaline, karaara?

12
00:01:21,800 --> 00:01:30,360
Nourishes bring on the materiality of the octet that has Nutritip essence as eight.

13
00:01:30,360 --> 00:01:34,880
Contact as Nutriment nourishes, bring on the three kinds of fill.

14
00:01:34,880 --> 00:02:03,280
Now, when there is physical Nutriment, there is attachment, which brings value, when there

15
00:02:03,280 --> 00:02:11,640
is Nutriment as contact, there is approaching, which brings peril, when there is Nutriment

16
00:02:11,640 --> 00:02:19,320
as mental, volition, there is reveals linking, which brings peril.

17
00:02:19,320 --> 00:02:28,920
And to show how they bring fear of fear, thus, physical Nutriment should be illustrated

18
00:02:28,920 --> 00:02:34,240
by the female of a child's flesh.

19
00:02:34,240 --> 00:02:44,240
Contact as Nutriment by the female of the Heidel-less call, mental volition as Nutriment

20
00:02:44,240 --> 00:02:53,400
by the female of the pit of leaf calls, and consciousness as Nutriment by the female

21
00:02:53,400 --> 00:03:09,400
of the Heidel-less

22
00:03:09,400 --> 00:03:21,600
Hi Cathy, do you have a mic hooked up to be able to read for for us?

23
00:03:21,600 --> 00:03:27,240
I'm not hearing you so possibly your mic isn't hooked up just yet, we'll go on to the

24
00:03:27,240 --> 00:03:31,680
next person, but if you are able to, you know, just break in next time.

25
00:03:31,680 --> 00:03:32,680
Thank you.

26
00:03:32,680 --> 00:03:35,600
Last though, could you read for?

27
00:03:35,600 --> 00:03:36,600
Sure.

28
00:03:36,600 --> 00:03:43,760
But of these four kinds of Nutriment, it is only physical Nutriment, classed as what is

29
00:03:43,760 --> 00:03:51,360
eaten, drunk, chewed and tasted, that it is intended here as Nutriment in distance.

30
00:03:51,360 --> 00:03:57,600
The perception, a reason as the impression of the repulsive aspect in that Nutriment is

31
00:03:57,600 --> 00:04:01,360
perception of repulsiveness in Nutriment.

32
00:04:01,360 --> 00:04:08,920
One who wants to develop that perception of repulsiveness in Nutriment is knowing the

33
00:04:08,920 --> 00:04:09,920
meditate.

34
00:04:09,920 --> 00:04:12,680
I'm Marilynneo, we're not hearing you.

35
00:04:12,680 --> 00:04:15,600
Can you hear me now?

36
00:04:15,600 --> 00:04:16,600
Yes.

37
00:04:16,600 --> 00:04:17,600
Thank you.

38
00:04:17,600 --> 00:04:22,560
One who wants to develop that perception of repulsiveness in Nutriment should learn the

39
00:04:22,560 --> 00:04:27,960
meditation subject and see that he has no uncertainty about even a single word as what he

40
00:04:27,960 --> 00:04:28,960
is with.

41
00:04:28,960 --> 00:04:34,880
Then you should go into solitary retreat and review repulsiveness in 10 aspects in the physical

42
00:04:34,880 --> 00:04:41,280
Nutriment classified as what is eaten, drunk, chewed and tasted, that is to say, as to

43
00:04:41,280 --> 00:04:48,880
going, speaking, using, secretions, receptacles, what is uncooked, undigested, what is

44
00:04:48,880 --> 00:04:56,680
cooked, suggested, root, outflow, and CI.

45
00:04:56,680 --> 00:05:03,120
Here in as to going, even when a man has gone forth in so mighty a dispensation, still

46
00:05:03,120 --> 00:05:08,120
after he has perhaps spent all night reciting the enlightened ones' words or doing the

47
00:05:08,120 --> 00:05:13,400
ascetics work, even after he has risen early to do the duties connected with the shrine

48
00:05:13,400 --> 00:05:19,160
terrace and the enlightenment tree terrace, to set out the water for drinking and washing,

49
00:05:19,160 --> 00:05:22,760
to sweep the grounds and to see to the needs of the body.

50
00:05:22,760 --> 00:05:27,040
After he has sat down on his seat and given attention to his meditation subject twenty or

51
00:05:27,040 --> 00:05:32,880
thirty times and got up again, then he must take his bowl and out a robe.

52
00:05:32,880 --> 00:05:37,720
He must leave behind the ascetics woods that are not crowded with people, offer the bliss

53
00:05:37,720 --> 00:05:43,520
of seclusion, possess shade and water, and are clean, cool, delightful places.

54
00:05:43,520 --> 00:05:48,000
He must disregard the noble ones to light in seclusion, and he must set out for the village

55
00:05:48,000 --> 00:05:55,360
in order to get new trimming as a jackal for the char, as a jackal for the charnel ground.

56
00:05:55,360 --> 00:06:01,400
So it's trying to explain the repulsiveness in many aspects, it's going to, this is one

57
00:06:01,400 --> 00:06:07,440
example of exactly how detailed the commenters and how exact and precise they are.

58
00:06:07,440 --> 00:06:12,680
They really take absolutely every aspect, not just the eating part, but every aspect

59
00:06:12,680 --> 00:06:16,200
of food to be repulsive, starting from having to go for it.

60
00:06:16,200 --> 00:06:22,800
So just having to leave the forest, having to leave your pleasant place.

61
00:06:22,800 --> 00:06:28,480
We're going to see in the next paragraph even better.

62
00:06:28,480 --> 00:06:33,960
It already starts to be repulsive, something that you wouldn't want to do if you didn't

63
00:06:33,960 --> 00:06:37,640
rest here.

64
00:06:37,640 --> 00:06:43,960
Antigos starts from the time when he steps down from his bed, or chair, he has to tread

65
00:06:43,960 --> 00:06:50,920
on a carpet, covered with dust, a beast's feet, geckos, geckos, droppings, and so on.

66
00:06:50,920 --> 00:06:55,720
Next he has to see the doorstep, which is more repulsive than the inside of the room, since

67
00:06:55,720 --> 00:07:00,920
it has been often filed with the droppings of rats, bats and so on.

68
00:07:00,920 --> 00:07:06,240
This is the lower terrace, which is more repulsive than the terrace above, since it is all

69
00:07:06,240 --> 00:07:11,480
smeared with droppings of owls, pigeons and so on.

70
00:07:11,480 --> 00:07:16,280
Next the grounds which are more repulsive than the lower floor, since they are defiled by

71
00:07:16,280 --> 00:07:23,440
old grass and leaves blown about the wind by a sickness and zeroing, excrements, bitter

72
00:07:23,440 --> 00:07:28,840
and snop, and in the rainy season by water, mud and so on.

73
00:07:28,840 --> 00:07:45,400
Antigos has to see the road to the monastery, which is more repulsive than the grounds.

74
00:07:45,400 --> 00:08:07,920
Hi, Sanka, you are muted.

75
00:08:07,920 --> 00:08:08,920
Maybe not.

76
00:08:08,920 --> 00:08:14,800
I think it's quite late where Sanka is, so maybe we'll just bypass him for now.

77
00:08:14,800 --> 00:08:17,800
And are you able to read it?

78
00:08:17,800 --> 00:08:20,080
Yes.

79
00:08:20,080 --> 00:08:30,000
In due course, after standing in a debating lodge, when he has finished paying homage at the

80
00:08:30,000 --> 00:08:38,640
enlightenment tree and the shrine, he sets all thinking, instead of looking at the shrine

81
00:08:38,640 --> 00:08:51,760
that is like a cluster of pearls and the enlightenment tree, that is as lovely as a bouquet

82
00:08:51,760 --> 00:09:03,040
of peacock's tail feathers and a boat that is as fair as a god's palace.

83
00:09:03,040 --> 00:09:16,200
I must now turn my back, turn my back on such a charming place and go aboard for the sake

84
00:09:16,200 --> 00:09:29,720
of food and on the way to the village, the view of a row of stumps and thromes and an even

85
00:09:29,720 --> 00:09:40,280
row broken up by the force of water awaits him.

86
00:09:40,280 --> 00:09:53,920
Next, after he has put on his waistcloth as one who hides an abscess and tied his waistband

87
00:09:53,920 --> 00:09:59,640
as one who ties a bandage on a wound and roped himself in his upper robes as one who hides

88
00:09:59,640 --> 00:10:06,040
his skeleton and taken out his bowl as one who takes out a pan for medicine.

89
00:10:06,040 --> 00:10:11,040
When he reaches the vicinity of the village gate, perhaps the site of an elephant's carcass

90
00:10:11,040 --> 00:10:16,760
or horses carcass or buffalo's carcass, a human carcass, a snake's carcass or a dog's

91
00:10:16,760 --> 00:10:18,880
carcass awaits him.

92
00:10:18,880 --> 00:10:25,280
Not only that, but he has to suffer his nose, his nose to be assailed by the smell of them.

93
00:10:25,280 --> 00:10:29,440
Next as he stands in the village gateway, he must scan the village streets in order to avoid

94
00:10:29,440 --> 00:10:35,480
danger from savage elephants, horses, and so on.

95
00:10:35,480 --> 00:10:41,520
So this repulsive experience beginning with the carpet that has to be trodden on and ending

96
00:10:41,520 --> 00:10:47,440
with the various kinds of carcasses that have to be seen and smelled has to be undergone

97
00:10:47,440 --> 00:10:49,760
for the sake of Nutrament.

98
00:10:49,760 --> 00:10:53,240
Oh, Nutrament is indeed a repulsive thing.

99
00:10:53,240 --> 00:10:59,160
This is how repulsiveness should be reviewed as to going.

100
00:10:59,160 --> 00:11:01,560
2.

101
00:11:01,560 --> 00:11:04,400
How is to seeking?

102
00:11:04,400 --> 00:11:10,840
When he has endured the repulsiveness of going in this way and has gone into the village

103
00:11:10,840 --> 00:11:17,320
and is clouded in his cloak of batches, he has to wander in the village a street

104
00:11:17,320 --> 00:11:22,760
from house to house like a beggar with a ditch in his hand.

105
00:11:22,760 --> 00:11:29,960
And in the rainy season, wherever he threads his feet sinking to water and my job to the

106
00:11:29,960 --> 00:11:39,600
pledge of the cows, he has to hold the wall in one hand in his rock, pop with the other.

107
00:11:39,600 --> 00:11:46,160
In the hot season, he has to go about with this body cover, with the dear grass and

108
00:11:46,160 --> 00:11:49,720
those brown about by the wind.

109
00:11:49,720 --> 00:11:57,120
On rich in search and search a house door, he has to see an event to thread in gutters

110
00:11:57,120 --> 00:12:06,960
and says post cover with blue bottles and setting with all the species of worms.

111
00:12:06,960 --> 00:12:16,480
Help me stop with feature washing, meat washing, rice washing, a spill, a snot, dogs

112
00:12:16,480 --> 00:12:25,520
and pigs excrement and whatnot, from which he flies, come up and settle on his outer

113
00:12:25,520 --> 00:12:32,960
cloak of batches and on his bowl and on his head.

114
00:12:32,960 --> 00:12:39,360
And when he enters a house, some give us and some do not.

115
00:12:39,360 --> 00:12:48,160
And when they give some give just a day cook, dry and steal, stale cakes and burnt

116
00:12:48,160 --> 00:13:18,120
teeth, jelly, sauce and so on, some not even say, please pass on, burn over fear.

117
00:13:18,120 --> 00:13:29,320
Others keep silent as if they didn't not see him, some other day faces, others drinking

118
00:13:29,320 --> 00:13:36,440
with hash words such as go away, you blackhead.

119
00:13:36,440 --> 00:13:47,440
When he has one there for ounce in the village, in this way like a burger, bigger, he has

120
00:13:47,440 --> 00:14:14,200
to depart from it.

121
00:14:14,200 --> 00:14:18,200
Would I continue, obvious please?

122
00:14:18,200 --> 00:14:23,320
So this experience beginning with the entry into the village and ending with the departure

123
00:14:23,320 --> 00:14:28,840
from it, which is repulsive owing to the water, mud etc.

124
00:14:28,840 --> 00:14:35,120
That has to be trodden in and seen and endured, has to be undergone for the sake of

125
00:14:35,120 --> 00:14:40,120
new treatment, all new treatment is indeed a repulsive thing.

126
00:14:40,120 --> 00:14:45,120
This is how reposiveness should be reviewed as the seeking.

127
00:14:45,120 --> 00:14:53,240
Three, how as to use it, after he has stopped the mission as in this way and the sitting

128
00:14:53,240 --> 00:14:57,280
at ease in a comfortable place outside the village, and so on.

129
00:14:57,280 --> 00:15:02,520
I'm not able to hear Marylin, they are beyond very, very slightly, I can't make out what

130
00:15:02,520 --> 00:15:03,520
she's saying.

131
00:15:03,520 --> 00:15:05,280
Am I the only one?

132
00:15:05,280 --> 00:15:10,000
No, it's a very quiet, is there anything in your settings, Marylin, that you can turn

133
00:15:10,000 --> 00:15:11,000
up your output at all?

134
00:15:11,000 --> 00:15:17,000
I'll try.

135
00:15:17,000 --> 00:15:29,640
Yeah, I don't think so, but is this any better?

136
00:15:29,640 --> 00:15:35,240
That's a little better, yes, and I'll try and speak louder.

137
00:15:35,240 --> 00:15:42,360
So, three, how as to use it, after he has sought the new treatment in this way and is

138
00:15:42,360 --> 00:15:48,520
sitting at ease in a comfortable place outside the village, then so long as he has not dipped

139
00:15:48,520 --> 00:15:55,120
his hand into it, he would be able to invite a respected Deku or a decent person if he saw

140
00:15:55,120 --> 00:16:01,280
one to share it, but as soon as he has dipped his hand into it out of desire to eat, he

141
00:16:01,280 --> 00:16:04,280
would be ashamed to say, take something.

142
00:16:04,280 --> 00:16:09,240
And when he has dipped his hand in and is squeezing it up, the sweat trickling down his

143
00:16:09,240 --> 00:16:22,040
five fingers, what's any dry, crisp food there may be and makes it soggy.

144
00:16:22,040 --> 00:16:26,440
And when it's good appearance has been spoiled by his squeezing it up and it has faded

145
00:16:26,440 --> 00:16:31,680
to a ball and to his mouth, then the lower teeth function is a mortar, the upper teeth

146
00:16:31,680 --> 00:16:36,720
as a pestle, and the tongue as a hand, it gets pounded there with the pestle of the teeth

147
00:16:36,720 --> 00:16:42,600
like a dog's dinner and a dog's trap, while he turns it over and over with his tongue,

148
00:16:42,600 --> 00:16:47,840
then the thin spittle at the tip of the smears it, spittle in the middle of the tongue

149
00:16:47,840 --> 00:16:51,800
smears it, and then the filth from the teeth and the parts where tooth stick cannot

150
00:16:51,800 --> 00:16:55,400
reach the ears.

151
00:16:55,400 --> 00:17:04,160
When that smashed up and be smeared, this peculiar compound now destitute of the original

152
00:17:04,160 --> 00:17:10,840
of the colour and smell is reduced to a condition that's actually nauseating as a dog's

153
00:17:10,840 --> 00:17:18,000
vomit in a dog's trough, yet not withstanding that it is like this, it can be swallowed

154
00:17:18,000 --> 00:17:31,440
because it is no longer in the range of the highest focus, sorry, this is how repulsiveness

155
00:17:31,440 --> 00:17:38,480
should be reviewed as teasing.

156
00:17:38,480 --> 00:17:58,760
Now as two, secretion, buddhas, and pajekap buddhas, and will turning monage have only one

157
00:17:58,760 --> 00:18:18,040
of the four secretions consisting of biol, rapping, how do you say this word again?

158
00:18:18,040 --> 00:18:35,600
Flim, flim, pause and blood, but those with weak merit have all four, so when the food

159
00:18:35,600 --> 00:18:49,560
has arrived at the stage of being eaten and enters inside, then in one host, separation

160
00:18:49,560 --> 00:19:08,800
of biol is in excess, it becomes as utterly now sitting as if smeared with thick muddukka

161
00:19:08,800 --> 00:19:29,560
oil, in one host secretion of flim in excess, it is as if smeared with the juice of nagap

162
00:19:29,560 --> 00:19:44,200
pala leaves, in one host secretion of pause is in excess, it is as if smeared with rancid

163
00:19:44,200 --> 00:20:01,440
buttermilk, and in one host secretion of blood is in excess, it is as utterly now sitting

164
00:20:01,440 --> 00:20:19,760
as if smeared with dye, this is how repulsiveness should be reviewed as to secretion.

165
00:20:19,760 --> 00:20:25,720
How as to a receptacle, when it has gone inside the belly and is smeared with one of these

166
00:20:25,720 --> 00:20:32,200
secretions, then the receptacle it goes into is no gold dish or crystal or silver dish

167
00:20:32,200 --> 00:20:39,280
and so on, on the contrary, if it is swallowed by one hit by one ten years old, it finds

168
00:20:39,280 --> 00:20:45,200
itself in a place like a cesspit unwashed for ten years, if it is swallowed by one twenty

169
00:20:45,200 --> 00:20:55,280
years old, thirty, forty, fifty, sixty, seventy, eighty, ninety years old, if it is swallowed by one hundred years old it finds itself in a place like a

170
00:20:55,280 --> 00:21:02,180
cesspit unwashed for a hundred years, this is how repulsiveness should be reviewed as to

171
00:21:02,180 --> 00:21:06,800
receptacle, I think we're not going to look at, you're not going to look at food ever, the same

172
00:21:06,800 --> 00:21:26,320
over again after this, how as to what is uncooked undigested, after this

173
00:21:26,320 --> 00:21:33,360
new treatment has arrived at such a place for its receptacle, then for as long as it remains uncooked

174
00:21:33,360 --> 00:21:40,000
it stays in the same place just described which is shrouded in absolute darkness, provided by droughts

175
00:21:40,000 --> 00:21:50,000
tainted by various smells of orger and utterly fetid and loathsome and just as one the cloud out of

176
00:21:50,000 --> 00:21:57,360
season has rained during a drought and bits of grass and leaves and rushes and the carcasses of snakes, dogs

177
00:21:57,360 --> 00:22:02,080
and human beings that have collected in the pit at the gate of an outcast village remain there,

178
00:22:02,080 --> 00:22:10,800
mourn by the sun's heat until the pit becomes covered with froth and bubbles, so too what has been

179
00:22:10,800 --> 00:22:16,960
swallowed that day and yesterday and the day before remains there together and being smothered by

180
00:22:16,960 --> 00:22:22,400
the layer of swam and covered with froth and bubbles produced by digestion through being

181
00:22:22,400 --> 00:22:28,800
fermented by the heat of the bodily fires, it becomes quite loathsome, this is how repulsiveness

182
00:22:28,800 --> 00:22:38,880
should be reviewed as to what is uncooked. 7. How as to where is caught cooked?

183
00:22:39,920 --> 00:22:48,480
When it has been completely cooked there by the bodily fires, it does not turn into bones

184
00:22:48,480 --> 00:22:57,280
over etc as the earth of bones over etc through a smelchum, instead giving off growth and

185
00:22:57,280 --> 00:23:04,160
bubbles it turns into excrement and fills the receptacle for the gested food.

186
00:23:05,760 --> 00:23:13,040
Like brown clay, a squeeze with a smoothing trowel and packing to a tube,

187
00:23:13,040 --> 00:23:20,720
and it turns into urine and fills the blood, this is how repulsiveness should be reviewed

188
00:23:21,520 --> 00:23:31,920
as to where is cooked. How as to fruit? When it has been rightly cooked it produces the various

189
00:23:31,920 --> 00:23:40,800
kinds of other consisting of head hairs, body hairs, nails, teeth and the rest.

190
00:23:40,800 --> 00:23:48,320
When wrongly cooked it produces the hundred pieces beginning with each,

191
00:23:49,360 --> 00:23:57,920
ranked arms, smallpox, leprosy, plague, plaque, consumption,

192
00:23:57,920 --> 00:24:11,440
cast, flax and so on, such easy fruit, this is how repulsiveness should be reviewed as to fruit.

193
00:24:13,680 --> 00:24:18,640
Can you hear me? Yes, we can, thank you, thank you. Okay, I will try.

194
00:24:20,400 --> 00:24:27,760
How as to outflow on being swallowed it enters by one door after which it flows out by

195
00:24:27,760 --> 00:24:34,800
several doors in the way beginning, I dirt from the eye, ear dirt from the ear,

196
00:24:35,360 --> 00:24:40,560
and on being swallowed it is swallowed even in the company of large gatherings.

197
00:24:41,360 --> 00:24:50,400
But on flowing out, now converted into excrement, urine, etc. It is excreted only in solitude.

198
00:24:50,400 --> 00:24:57,040
On the first day one is delighted to eat it, elated and full of happiness and joy.

199
00:24:57,840 --> 00:25:05,200
On the second day one stops one's nose to avoid it, with a rye face disgusted and dismayed.

200
00:25:05,920 --> 00:25:13,760
On the first day one swallows it lustfully, greedily, gluttonlessly, infatuated tiddly.

201
00:25:13,760 --> 00:25:20,480
But on the second day after a single night has passed one excretes it with the taste,

202
00:25:21,120 --> 00:25:28,960
ashamed, humiliated and disgusted, hence the ancient say, the food and drink so greatly prized,

203
00:25:29,600 --> 00:25:35,280
the crisp to chew the soft to suck, go in all by a single door.

204
00:25:35,280 --> 00:25:45,280
But by nine doors come oozing out.

205
00:25:53,760 --> 00:25:59,440
Take care, ember, we'll see you next time.

206
00:25:59,440 --> 00:26:19,520
The food and drink so greatly prized the crisp to chew the soft to suck, men like to eat in

207
00:26:19,520 --> 00:26:29,840
company. The food and drink so greatly prized the crisp to chew the soft to suck, this

208
00:26:29,840 --> 00:26:37,840
a man eats with high light and then it creates with dumb disgust. The food and drink so great to

209
00:26:37,840 --> 00:26:47,680
rise the crisp to chew the soft to suck. A single night will be enough to bring them to do

210
00:26:47,680 --> 00:26:52,320
purity. This is how the pulse of nature will be reviewed as to upload.

211
00:26:54,800 --> 00:27:02,000
How is this mirroring? At the time of using it he smears his hands, lifts tongue and pallet

212
00:27:02,000 --> 00:27:07,760
and they become repulsive by being scared with it. And even when wash they have to be washed

213
00:27:07,760 --> 00:27:15,200
again and again in order to remove the smell. And just as when rice is being boiled, the husks,

214
00:27:15,200 --> 00:27:21,600
the red powder covering the grain, etc., rise up and smear the mouth, rim and lid of the caldrum.

215
00:27:22,400 --> 00:27:28,400
So too when eating it rises up during its cooking and simmering by the bodily fire that pervades

216
00:27:28,400 --> 00:27:34,880
the whole body, it turns into tartar which smears the teeth and it turns into spittle,

217
00:27:34,880 --> 00:27:42,160
hem, etc., which respectively smear the tongue, palate, etc. And it turns into eye dirt,

218
00:27:42,160 --> 00:27:50,720
ear dirt, snot, urine, excrement, etc., which respectively smear the eyes, ears, nose and

219
00:27:50,720 --> 00:27:58,000
nether passages. And when these doors are smeared by it they never become either clean or pleasing

220
00:27:58,000 --> 00:28:04,160
even though washed every day. And after one has washed a certain one of these the hand has to be

221
00:28:04,160 --> 00:28:10,880
washed again. And after one has washed a certain one of these repulsiveness does not depart from it,

222
00:28:10,880 --> 00:28:17,440
even after two or three washings with cow dung and clay and scented powder. This is how repulsiveness

223
00:28:17,440 --> 00:28:23,840
should be reviewed as disappearing. That's okay for Nando, I got mixed up too.

224
00:28:23,840 --> 00:28:33,920
Lancelot, could you reach 25?

225
00:28:33,920 --> 00:28:38,400
In this way, in 10 aspects, in strikes, edit with taut and apply thought. Physical

226
00:28:38,400 --> 00:28:46,800
nutrum and becomes evident to him in its repulsive aspect. He cultivates that sign again and again,

227
00:28:46,800 --> 00:28:54,880
develops and repeatedly practices it. As he does so, the hindrances are suppressed and his mind is

228
00:28:54,880 --> 00:29:02,160
concentrated in access concentration. But without reaching absorption because of the profundity

229
00:29:02,720 --> 00:29:10,640
of physical nutrum and as a state with an individual essence. But perception is evident here

230
00:29:10,640 --> 00:29:18,640
in the apprehension of the repulsive aspect, which is why this meditation subject goes by the name

231
00:29:18,640 --> 00:29:28,160
of perception of repulsiveness in nutrum. When Abiku devotes himself to this perception of

232
00:29:28,160 --> 00:29:36,160
repulsiveness and nutrum, his mind retracts, retreats, retracts and recoils from craving for flavors,

233
00:29:36,160 --> 00:29:42,320
he nourishes himself with nutrum without vanity and only for the purpose of crossing over suffering

234
00:29:42,320 --> 00:29:48,160
as one who seeks to cross over the desert eats his own dead child's flesh. Then his greed

235
00:29:48,160 --> 00:29:54,560
for the fire, five courts of sense desire comes to fully understand without difficulty by means

236
00:29:54,560 --> 00:30:00,640
of the full understanding of the physical nutrum. He fully understands the materiality aggregate

237
00:30:00,640 --> 00:30:06,640
by means of the full understanding of the five courts of sense desires. Development of mindfulness

238
00:30:06,640 --> 00:30:12,000
occupied with the body comes to perfection in him through the repulsiveness of what is uncooked

239
00:30:12,000 --> 00:30:17,520
and the rest. He has entered upon a way that is in conformity with the perception of

240
00:30:17,520 --> 00:30:23,360
foulness and by keeping to this way, even if he does not experience the deathless goal in this life,

241
00:30:23,360 --> 00:30:28,800
he is at least bound for a happy destiny. This is the detailed explanation of the development

242
00:30:28,800 --> 00:30:35,920
of the perception of repulsiveness and nutriment. I am going to have to ask that we quit there

243
00:30:35,920 --> 00:30:42,400
because I have something I have to do and I have been very distracted today by various things,

244
00:30:42,400 --> 00:30:51,600
so I have to go and edit and submit an essay. Okay, thank you, Bhante.

245
00:30:51,600 --> 00:31:06,080
Sadhu, Bhante. Sadhu. Sadhu, Bhante. No, Marilynne, don't worry, I got mixed up and I asked

246
00:31:06,080 --> 00:31:35,920
for Nanda to read. It was my mistake, so please don't apologize. Sometimes we sound alike, you know.

