1
00:00:00,000 --> 00:00:02,480
difficulty in socializing.

2
00:00:02,480 --> 00:00:03,760
Question.

3
00:00:03,760 --> 00:00:07,680
What is the purpose for seeking solitude for meditation?

4
00:00:07,680 --> 00:00:10,000
How does one seek solitude?

5
00:00:10,000 --> 00:00:11,360
Answer.

6
00:00:11,360 --> 00:00:14,200
True solitude is a state of mind.

7
00:00:14,200 --> 00:00:17,880
Solitude is something you can find anywhere.

8
00:00:17,880 --> 00:00:22,400
Solitude of mind is called Chitta-wi-wa-kah.

9
00:00:22,400 --> 00:00:25,560
Taya-wi-wa-kah means occlusion of the body,

10
00:00:25,560 --> 00:00:30,040
which is of course useful and makes finding tranquility easier,

11
00:00:30,040 --> 00:00:34,840
but if you are truly mindful, it can be in solitude anywhere.

12
00:00:34,840 --> 00:00:40,160
Solitude means being alone, solo or singular.

13
00:00:40,160 --> 00:00:42,760
We commonly say that a person is alone

14
00:00:42,760 --> 00:00:45,120
when there is no other being nearby,

15
00:00:45,120 --> 00:00:47,640
but another meaning of alone

16
00:00:47,640 --> 00:00:51,360
is alone in the moment in terms of time and space,

17
00:00:51,360 --> 00:00:54,360
not thinking about the past or future,

18
00:00:54,360 --> 00:00:57,320
or someplace other than where you are.

19
00:00:57,320 --> 00:00:59,800
If we get caught up in the past or future,

20
00:00:59,800 --> 00:01:02,480
feeling bad about past experiences,

21
00:01:02,480 --> 00:01:04,360
worrying about the future,

22
00:01:04,360 --> 00:01:07,400
or if we get caught up thinking about other people,

23
00:01:07,400 --> 00:01:09,160
places and things,

24
00:01:09,160 --> 00:01:25,960
we are not isolated or in solitude.

