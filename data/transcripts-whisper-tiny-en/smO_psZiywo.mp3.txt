Okay, good evening, everyone.
Welcome to our evening dhamma.
Today is question and answer.
So we're going to be going to the questions on our website.
You're welcome to go to the website at meditation.ceremungalow.org.
And you have to sign up. Just give your email.
It's purposefully making things difficult because it's easy to ask questions.
So we want to ensure a little bit of quality.
We make it a little challenging.
You have to sign up.
And it also ensures that you're actually a little bit interested in our community.
So if you're not interested in signing up for our website well,
this question and answer session is probably not for you anyway.
Which is fine trying to push anything on anybody.
It's just trying to focus our efforts on what's most beneficial to our community.
So get right into it and answering questions here.
If it works.
Which it doesn't.
I've got the questions up. I just can't do the whole clicking thing.
I'll just answer questions. I don't know what's wrong here.
Website's stalled again.
A teacher once told me that a neutral feeling is based on ignorance.
Otherwise, I would be able to determine a feeling as pleasant or painful.
He based this on the julawedah way to the suta,
where it says passion lurks and present feelings,
and ignorance in neutral feelings.
What I see in meditation now is that although neutral feelings can lead to ignorance,
it is not necessarily based on ignorance. Could you clarify this for me please?
What do I miss? You didn't miss anything.
It sounds like this teacher. If that's an accurate representation of what they said
is missing something.
If you look at the Abidama, a neutral feeling is...
I guess a neutral feeling you could say is most susceptible to delusion.
Or that's not even really fair.
But the point is it's not as common.
You might say.
It's not just speculation, but it seems reasonable to suggest
that a neutral feeling is not as susceptible to a reaction that is pleasant,
or that is desired, that is desiring it.
So, to have a neutral feeling give rise to desire is less common.
It still happens. It happens all the time.
If you're very calm, you can like it,
and you can get attached to the feeling of a calm feeling.
There's lots of calming sensations that you end up liking.
And in the Abidama, it's quite clear.
A feeling associated with...
Or a mind-state associate with greed can be either associated with a neutral feeling or a pleasant feeling,
meaning when you like something, it's either neutral or it's happy.
One or the other.
So, he's clearly wrong, according to the Abidama, because it's quite clear about the association with a neutral feeling in greed.
And on the other hand, a neutral feeling can also be associated with a wholesome state.
Hulsome states are either associated with a neutral feeling or a pleasant feeling.
And then there's the 4th Jana, which is also associated with a neutral feeling.
There's no pleasant feeling in the 4th Jana, and yet there's clearly no...
There's no delusion in the Jana state.
So, that's that.
But as I say, neutral feelings are probably more likely to be associated with pure delusion in the sense that you get views about them,
and that you have a neutral feeling and it gives rise to view, it gives rise to doubt, it gives rise to worry or whatever,
but it's less strongly associated with greed, I would say.
And it's certainly not associated with anger.
It's hard to get angry at a calm feeling, right?
It's something that makes you calm.
It's not likely to give rise to anger as a result.
So, there's a connection there, which is what the way Dallas it is,
the true the way Dallas it is saying.
That's all it's saying.
It's not saying that all neutral feelings are.
That would be absurd.
Really absurd because the Jana's especially are associated with calm feelings and meditation is associated with calm feelings.
So, your intuition and your experience, your conclusion based on your experience, is correct.
Again, I can't do anything about these questions answering them or anything.
The whole website thing is frozen for me, that I can read them.
Is it considered idle chatter to greet people?
I ask how they are doing and such?
No. No, it's not considered idle chatter.
There's a lot of talk that is functional and purposeful.
Asking someone out they are is functional.
At the very least it's functional in the sense that it adheres to convention.
If you look in the suit, it's a very important part of a lot of the dialogues.
The Buddha would meet someone in exchange pleasantries.
How it's often described or translated some more denying kataa, some more denier to pleasantries.
More does is to joy, some more does to, it's basically a pleasant dream.
It's a fairly literal translation.
Some more denier that should be enjoyed.
It should be pleasing.
How are you?
Were they actually spell it out?
Are you holding up?
Are you not being overwhelmed by pain?
The response is, yes, I'm holding up.
I'm not dying or anything.
It's fairly to the point, not very flowery or even all that pleasant.
When the monks, because they're very serious about what they do and they're trying very hard not to allow their minds to get caught up in attachment.
But no, idle chatter is something that is directed to no purpose.
So after, or apart from exchanging pleasantries, which have a functional purpose and also a purpose of creating friendship and that kind of thing.
Expressing friendship, expressing friendliness.
But then if you start to talk about what is the topic of your conversation?
If the topic of the conversation is each other's health and well-being and so on, that's positive.
But if it's, you know, the weather or if it's politics, that kind of thing.
Generally considered idle chatter.
My question is that when using auditory tracks like waterfall sounds, can these be used as an object for meditating?
Yes, that's under dumbah, it's under the fourth one.
So it doesn't spell that out in the booklet until the end, but if you read the last chapter it makes mention of it.
And we usually don't worry too much about that in the beginning, but yeah, as you practice on.
I mean, we try to discourage the use of auditory tracks when meditating because it's a partiality.
You're not allowing for ordinary sounds to rise or to bring your focus.
Usually because they're unpleasant, they're undesirable, and that's what we want to learn about.
We want to learn about our reactions.
If you're trying to control what sounds you hear by playing only pleasant sounds, you're never going to really understand how the mind reacts to negative things.
Is that Jantong's method different from others in my seat tradition?
You know, there's some technical differences, I would say.
Some of them are, you know, the be argument, so this teacher will say this, that teacher will say that.
I think it is, there are some fair size differences, I would say, in regards to, especially the touching, Jantong, or whoever it is,
whether it was him or his teachers who set this up, sort of elaborated on that and turned it into a systematic meditation.
The technical aspects of the practice are far less important in terms of determining the tradition than the actual principles behind it.
The principles are very much the same.
By each teacher is going to teach different technical technique.
Is it possible to remember only pleasant memories? Is it possible to throw away all unpleasant memories until there are only pleasant memories remaining?
A memory, can a memory be pleasant? Yes, a memory can be pleasant.
Let's go directly. Is it possible to remember only pleasant memories?
For how long?
I mean, temporarily, yes, it's theoretically possible. Is it possible to say for the rest of eternity,
I'm never going to remember anything unpleasant? No, that's not.
I mean, to only remember pleasant memories? No, that's not possible.
The only way for that to conceivably happen is if you do away with the cause for the arising of unpleasant memory.
That would mean becoming enlightened, right? Because what is the cause of unpleasant memories?
There is the cultivation of evil, you know, when you unwholesome this, when you do something bad, it gives rise to unpleasant memories.
Or simply having partiality of certain memories, right? You don't like someone.
But that's the same thing, really. You don't like someone and the anger is what gives rise to a memory that's unpleasant.
You remember someone and it's unpleasant.
But to go a little deeper, our intention is not to get rid of unpleasant memories, but that's not where our focus is in our minds, right?
Suppose theoretically you can say that, yes, in the end, by not doing an evil that you get rid of unpleasant memories, but that's not consequential.
What's consequential is the fact that evil leads to unpleasant memories that unwholesome this is a cause for suffering.
So we focus on our judgment of things. And this is very much done by stopping trying to change our perceptions, change our memories, stop trying to control our minds.
So if your focus is to try and control which memories you remember, you're actually increasing your partiality, increasing your aversion, increasing your judgmental nature of the mind, which is counterproductive.
So in mindfulness, we try to be open to the spectrum of experience.
If you were only to remember pleasant experiences, it would prevent you from understanding how your mind reacts to unpleasant experiences.
We're actually kind of keen to experience whatever unpleasantness there is in our minds and in our memories in order to learn to overcome our reactions to them.
If you're not, if there's no adversity, you're never tested, you're never challenged, and you can't grow.
I'm not sure I completely agree with that, and it's a very common design, but it's on the right track, I think.
I wouldn't say it's not possible. In fact, it's generally more difficult because if only good things come out, it's hard to be objective, right?
And it's easy to get complacent, lazy, it's hard to be clear of mind because everything's so easy. You're not challenged.
As the primary focus for Buddhist meditators to make the mind understand the true nature of impermanence, so the true nature of impermanence sounds a little bit lofty to me.
We're not trying to understand the true nature of impermanence, we're trying to understand the true nature of reality.
This, I mean reality is not something loftier. The true nature of this, right?
When I say this and you hear me say this, what is the true nature of that?
When I squeeze my hands like this and I feel this pressure, what is the true nature of that?
And the true nature of all of that, all of this, is it's impermanence. It's unsatisfying, it's uncontrollable.
None of it is going to be stable, satisfying, controllable, which is how we normally try to approach the world.
I mean, in terms of trying to achieve stability, satisfaction, control.
So meditation helps us see that that's not possible, not within samsara.
Everything is changing. And so the things that we cling to, the things that we strive for, we sort of get, well, we gradually reduce our obsession with them because we start to see that they're impermanent.
So that's really the focus of the meditation practice.
In ten precepts smoking is not mentioned wrong. Is it true that monks can smoke?
Yes, it's true that monks can smoke. I mean, there's a lot of effort to try and start monks from smoking.
That seems silly that that monks should be smoking, but many monasteries allow monks to smoke.
In Thailand, it's not so common in Sri Lanka. In fact, I don't know if it even happens in Sri Lanka.
Is it against the monks' precepts? I mean, I would be highly surprised if smoking cigarettes
wouldn't have been something that the Buddha would have disallowed, right? I mean, it seems quite glaringly, clearly something that should be disallowed for monks.
But I don't think there's any specific precept against it. I don't think it is encompassed by the fifth precept.
Though some monks disagree, they say it should be included in the fifth precept as being wrong.
If you take a vow for the fifth precept, you shouldn't smoke cigarettes, basically.
I think that might be going a little too far, but I understand the line of thinking.
Certainly not intoxicating the way alcohol is. That's just absurd.
Okay, afraid of brainwashing myself because of the change in the way I'm used to receiving the sentences.
It's deeply rooted in me to be that judgmental and absorbed an object.
I don't quite know what you're asking me for. Some suit that video talk about what exactly
about being afraid of brainwashing yourself. It's fear you should know the phrase.
Brainwashing is good. It means washing your mind, purifying your mind.
If you truly brainwashed, not in the way we normally use that word, it's a very good thing.
It's disturbing because it changes some of who you are.
We are attached very much to our personality.
It can be scary to think of letting that go.
Let's understand the moment. It's not in the end, it's not actually a problem.
Follow up question regarding cause and effect. I don't know what your first question was, but that's okay.
If everything has its cause, this kind of implies determinism.
How do you marry this idea with a referee will?
There'll also be something to start the cycle of cause and effect what would that be.
Buddhism doesn't say that everything has its cause. Buddhism talks about causality, which is clearly evident to their patterns.
Something comes and then something else comes.
We don't advance it into one of these theories of free will or determinism.
I think it's pretty clear that the Buddha didn't advance such theories purposefully
than being outside of reality.
When this comes, it doesn't matter whether that's an ultimate truth.
All it matters is that you see that because it's all psychological.
How does your mind perceive things?
If you observe and your mind perceives again and again that this happens,
it doesn't matter whether that's true that it's always going to be A than B.
All that matters is your mind saw that enough so that it let go.
Everyone said, okay, well I won't do A because B sucks.
Buddhism isn't that kind of philosophy where it claims free will or determinism or that sort of thing.
I think there are clear and obvious problems to either one of those presuppositions or stances, views.
Buddhism is, the Buddha said, I teach suffering, the cause of suffering.
He was very clever about this.
He was asked to sort of sing and he would say, look, all I teach is suffering.
The cause of suffering, the cessation of suffering and the path, the cessation of suffering.
That's important because that's what really happens.
Who cares what views you have?
What good are they for you?
What's really important is do you see things as they are?
No, what's really important is how is your mind?
And the whole practice is in order to purify the mind.
It's not in order to gain this or that view.
Right view in Buddhism is seeing things as they are.
That's the ultimate goal.
Is it problematic to only breathe through the nose during formal practice?
No, that's normally how I would think you would breathe.
Breathing through the mouth is also okay.
Some people do, but it's a little bit less comfortable.
I would say your mouth dries up.
So I would actually recommend breathing through the nose unless it's inconvenient.
It seems when the hands are held together and both thumbs are pointing up.
Formal practice goes by easier.
Should I do hand over hand so there isn't as much comfort?
You're getting into this idea of not making things so easy.
You shouldn't be worried about things being easy.
A bigger problem than things being easy is trying to make them easy.
Things being easy isn't a problem.
Trying to make things easy and worrying about finding tricks or making things easier is problematic.
I guess I would go a little bit further and say challenging yourself can be good.
So you don't want things, you don't want to be lazy, but that's more about doing more meditation.
You know, it's nothing to do with the hands.
In the long term you'll see that that doesn't actually make things easier.
It's a temporary, temporary effect that it's going to make things easy.
I currently can only do lying meditation due to medical condition.
I was wondering are the benefits from using the lion's pose?
The main benefit is that it keeps you from falling asleep.
I would say a strong position.
It's not easy to stay for a long time, but you're fairly strong.
You're not easily going to fall asleep.
There's also someone who's sitting on your right side.
You're not lying on the heart.
He was trying to explain to me the different postures.
It's time-month.
That was quite interesting.
If you're lying on the heart, it somehow makes you too hot inside.
I think that's what he was saying.
I think he actually had tied it in with lust or something like that, but it's interesting
that the Buddha would lie on the right side.
It may have had something to do with the heart being on the left side.
But I wouldn't deeply think into that much more important is the state of your mind.
Noting a loud during formal walking meditation has helped me.
There are downsides to this.
I can continue to use this, at least intermittently for some more time in my walking meditation.
Here's an example of what not to do.
I would say because it helped you in one way, made it easier.
There you go.
You're trying to make things easier.
Why?
What's wrong that you're trying to make things easier?
The answer to that question will be what you should be mindful of.
It's the difficult things that we really need to be mindful.
There were the greatest benefit comes from.
You talk about it helping you to make it more challenging.
Even if it did, I'd be skeptical because you're distracting yourself from the actual object
by focusing on the expression of words, which is a whole other experience.
The words in the mind are able to be with the object.
The words at the mouth may keep your focus better, but only because you're juggling.
You're having to speak and move at the same time.
That's it.
That's all the questions.
Not so many.
That's maybe a good sign.
Maybe everyone's doing such meditation that they started to realize.
There are no questions that my meditation won't answer for me.
It's not always true, but I do appreciate the questions and some really good ones this week.
My answers were okay not too short.
I didn't take too long.
I guess that's all for tonight then.
Have a good night.
Thank you.
