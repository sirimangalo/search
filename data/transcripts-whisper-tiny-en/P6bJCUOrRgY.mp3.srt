1
00:00:00,000 --> 00:00:02,000
Okay, go

2
00:00:02,400 --> 00:00:07,760
How do you deal with self-hate? I try to observe the hate, but it is so strong

3
00:00:08,520 --> 00:00:13,120
Do I need to practice meta and how do I do that if I hate myself?

4
00:00:13,680 --> 00:00:15,680
Thanks

5
00:00:15,680 --> 00:00:17,680
I

6
00:00:17,680 --> 00:00:28,240
I don't know that it actually that yeah, it can't certainly can't hurt to express love for yourself, but

7
00:00:31,640 --> 00:00:33,640
It's problematic

8
00:00:37,240 --> 00:00:39,240
You know sending love to yourself is

9
00:00:41,080 --> 00:00:46,360
Like trying to shoot yourself. No, no, no, that works actually you can shoot yourself, but

10
00:00:46,360 --> 00:00:50,200
It's like trying to see your your own eyes

11
00:00:51,080 --> 00:00:54,080
You know because you're doing the sending

12
00:00:54,800 --> 00:01:01,080
And people talk about it. It's certainly recommended by many teachers to wish for you and everyone recommends it

13
00:01:01,080 --> 00:01:03,640
But the text say it's just an example

14
00:01:04,480 --> 00:01:06,960
and it doesn't really do that much

15
00:01:08,280 --> 00:01:10,280
But sure it's certainly something

16
00:01:10,280 --> 00:01:19,560
What we're doing I mean the reason I hesitate about saying yes is that you know, there's a potential for it to become sort of

17
00:01:26,240 --> 00:01:28,240
Egotistical, right?

18
00:01:28,240 --> 00:01:30,240
You know the the whole idea of self

19
00:01:31,120 --> 00:01:33,120
It's already a problem

20
00:01:34,960 --> 00:01:38,080
Self-hatred is is very much time to ego

21
00:01:38,080 --> 00:01:40,080
it's

22
00:01:41,560 --> 00:01:47,360
It's different from hatred of others in the sense that it's a equitistical

23
00:01:48,200 --> 00:01:50,200
you are

24
00:01:50,720 --> 00:01:53,400
You have we have expectations for ourselves

25
00:01:54,480 --> 00:01:56,480
you know and

26
00:01:57,880 --> 00:01:59,880
we

27
00:02:00,720 --> 00:02:03,040
feel

28
00:02:03,040 --> 00:02:11,760
guilty and we feel bad when we feel ashamed of ourselves shame, you know, this sense of of improper shame

29
00:02:13,200 --> 00:02:15,200
There's proper shame

30
00:02:15,720 --> 00:02:17,320
which

31
00:02:17,320 --> 00:02:22,800
Recognizes when you do bad things, but there's this improper sense of this sort of obsession

32
00:02:23,840 --> 00:02:25,840
with

33
00:02:25,840 --> 00:02:42,120
self-hatred I'm I mean regardless whether you should send or shouldn't send metah

34
00:02:42,120 --> 00:02:44,120
It's not really the most important

35
00:02:45,240 --> 00:02:51,280
solution to the problem. That is the insight meditation is the greatest solution to the problem

36
00:02:51,280 --> 00:02:53,280
Uh

37
00:02:54,400 --> 00:03:00,720
The part of it is actually correct. I mean, it's a correct observation when you do something bad to to say that was a bad thing

38
00:03:01,800 --> 00:03:04,600
When you give rise to unwholesome mind states. It's

39
00:03:05,400 --> 00:03:13,320
Could to say you know that was an unwholesome mind state. Of course. It's it's a bigger problem when you say I'm ugly or

40
00:03:13,880 --> 00:03:15,880
I'm fat or I'm

41
00:03:15,880 --> 00:03:22,720
Stupid even I'm stupid because intelligence isn't I mean because none of these things including intelligence are

42
00:03:23,560 --> 00:03:30,800
Essential you know being fat isn't isn't evil. No being ugly isn't evil

43
00:03:33,760 --> 00:03:35,760
Being bald isn't evil

44
00:03:38,520 --> 00:03:42,200
So that kind of self-hatred is is worse that's damaging

45
00:03:42,200 --> 00:03:46,280
That's the kind of thing where that you really have to

46
00:03:47,480 --> 00:03:49,480
put aside but

47
00:03:52,200 --> 00:03:56,560
Knowing when you've done something wrong is important because there are other people on the other side who

48
00:03:57,120 --> 00:04:03,880
Don't see when they do something wrong and have self-righteousness when anyone tries to criticize them

49
00:04:05,480 --> 00:04:08,520
So an honest person. It's actually a sign of

50
00:04:08,520 --> 00:04:14,000
Of desire to develop to know that you have problems the Buddha said

51
00:04:14,640 --> 00:04:19,120
If a fool knows that they are a fool to that extent you can call them wise

52
00:04:20,120 --> 00:04:24,920
Right, but a fool who thinks they're wise. That's the real fool

53
00:04:27,240 --> 00:04:33,400
So there's definitely a benefit there to seeing the the your negative

54
00:04:33,400 --> 00:04:38,600
qualities certainly never see that and that's why I hesitate because

55
00:04:39,640 --> 00:04:44,400
It's not it's not always the case that you should get rid of every aspect of this and think yours

56
00:04:44,400 --> 00:04:50,840
And all these teachings where you should think I am special and you know, I am wonderful and I am perfect just the way I am

57
00:04:51,400 --> 00:04:53,720
It's all about to rubbish. It's certainly not Buddhist

58
00:04:54,400 --> 00:05:01,920
Buddhism you're useless you're you're yeah, I just told my meditative that I have a long conversation and at one point

59
00:05:01,920 --> 00:05:07,880
I said, you know a lot of this is just really just coming to the realization that you're useless or I didn't say

60
00:05:07,880 --> 00:05:10,240
It was there was some context and I said

61
00:05:11,120 --> 00:05:14,120
Eventually you just realized that you're useless and and

62
00:05:15,240 --> 00:05:17,240
hopeless

63
00:05:20,200 --> 00:05:26,320
Because that's really it we were actually going that far to give up this this body and this mind

64
00:05:26,320 --> 00:05:31,320
And we're using my teachers and the the mind

65
00:05:33,000 --> 00:05:35,000
Discards the mind

66
00:05:37,600 --> 00:05:41,400
Ting-ting-ting mind throws out the mind

67
00:05:47,560 --> 00:05:50,920
So don't don't discard that part of the teaching

68
00:05:50,920 --> 00:05:56,600
You know, don't don't just think well, I it's wrong if I criticize myself. That's not wrong

69
00:05:57,400 --> 00:06:01,240
Again wrong is the reaction to that. How do you react and

70
00:06:01,920 --> 00:06:05,680
It's really just a cop out to feel guilty to hate yourself

71
00:06:06,200 --> 00:06:08,200
It's just the easy way out

72
00:06:08,200 --> 00:06:13,680
You know, it's like the easy way out with anything when a baby cries the easy way out is to shake the baby to yell it

73
00:06:13,880 --> 00:06:16,600
You know to throw the baby out the window. I mean

74
00:06:16,600 --> 00:06:23,120
I that's a horrible thing to say, but but these thoughts arise in the parent. They get so frustrated. They just

75
00:06:25,720 --> 00:06:27,720
They start you know becoming

76
00:06:28,160 --> 00:06:30,760
Going crazy as there's no I'm supposed parents ever

77
00:06:31,440 --> 00:06:37,400
Get to the point where they want to strangle their kids, but they shake them, you know parents want to shake their kids sometimes

78
00:06:37,400 --> 00:06:48,200
Maybe not babies, but kids, you know hit their kids, of course

79
00:06:50,720 --> 00:06:52,720
Don't send me messages, please

80
00:06:53,640 --> 00:06:57,920
It's the fan of you want to send the message send it anyway, sir. We're live here

81
00:06:57,920 --> 00:07:05,920
There's a chat box you can use

82
00:07:08,920 --> 00:07:12,720
Right, so we take the easy way out you

83
00:07:14,160 --> 00:07:17,400
You hit you fight you yell you scream and

84
00:07:18,560 --> 00:07:21,360
That's all we're doing when we feel guilty when we feel

85
00:07:22,280 --> 00:07:24,280
self-hatred and so on

86
00:07:24,280 --> 00:07:30,440
We're taking the we're taking a cop we're copying out again

87
00:07:30,440 --> 00:07:32,920
It comes back to this. How do you react to the situation?

88
00:07:33,680 --> 00:07:39,480
You realize that was wrong and what is the reaction you can get angry or frustrated?

89
00:07:40,280 --> 00:07:43,440
There's a useless so the point is actually the

90
00:07:43,920 --> 00:07:48,840
That hatred is wrong the fact that it's directed to yourself is not an issue

91
00:07:49,400 --> 00:07:51,400
you know that the

92
00:07:51,400 --> 00:07:58,360
And the issue is that you react angrily towards that

93
00:07:58,840 --> 00:08:03,880
Which again, it just comes back down to any anger any sort of anger of course sending love helps

94
00:08:05,640 --> 00:08:10,920
Doing good deeds can help because it can help with the guilt, right? This is what I said

95
00:08:13,480 --> 00:08:15,160
Morality

96
00:08:15,160 --> 00:08:21,920
Well, it's specifically morality morality makes you feel less guilty so try it not to do bad things but

97
00:08:22,280 --> 00:08:25,440
Doing good things makes you feel good about yourself as well

98
00:08:26,280 --> 00:08:31,000
You know if you do lots of good deeds, you'll feel better about yourself. You feel like yeah, I'm a good person

99
00:08:34,760 --> 00:08:38,480
So that is another point especially morality

100
00:08:39,160 --> 00:08:42,920
And we'll stop doing things that make you feel make you hate yourself

101
00:08:42,920 --> 00:08:48,040
To some extent, but I think a lot more of it is just about hating things that

102
00:08:50,440 --> 00:08:54,880
That you can't easily change and so you shouldn't you have to stop with the anger and

103
00:08:56,520 --> 00:08:58,520
You have to accept

104
00:08:58,960 --> 00:09:03,480
Not that aspect of yourself, but you have to accept that that's

105
00:09:04,280 --> 00:09:08,920
You know the reality of it you have to accept it as reality because if you can't accept it

106
00:09:08,920 --> 00:09:14,920
You're going to react negatively towards it. All of our bad qualities. They are bad. No, no question

107
00:09:15,880 --> 00:09:20,760
There's some question, but quite often they're really bad bad qualities

108
00:09:23,080 --> 00:09:25,080
But

109
00:09:25,400 --> 00:09:27,400
Reacting to them doesn't solve some

110
00:09:29,800 --> 00:09:32,280
The answer is not in getting angry

111
00:09:32,280 --> 00:09:38,280
Anyway

