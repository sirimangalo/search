1
00:00:00,000 --> 00:00:17,680
The day I will continue the tops on the ass of that, and the development switch caused

2
00:00:17,680 --> 00:00:22,400
the mind to go bad.

3
00:00:22,400 --> 00:00:31,160
And today's topic is the subject on the subject of removing or discarding.

4
00:00:31,160 --> 00:00:36,080
We know Tanabahata path.

5
00:00:36,080 --> 00:00:45,320
Doing away with the defaments by discarding.

6
00:00:45,320 --> 00:00:52,600
And this means discarding the unwholesome states of mind as they arrive.

7
00:00:52,600 --> 00:00:59,360
Defaments in Buddhism are three kinds.

8
00:00:59,360 --> 00:01:11,200
The Kamakilesa, which are the actions which defile oneself, actions of killing or abusing,

9
00:01:11,200 --> 00:01:24,280
hurting other beings, actions of stealing or cheating or actions of speech, telling lies

10
00:01:24,280 --> 00:01:27,320
and so on.

11
00:01:27,320 --> 00:01:28,720
These are the course defilements.

12
00:01:28,720 --> 00:01:34,720
The moderate defilements are the ones which exist in the mind.

13
00:01:34,720 --> 00:01:38,960
You see the five hindrances.

14
00:01:38,960 --> 00:01:55,360
So we have Dhanamata, Dhanamata, Dhanamata, Uta-Takuku-chak, Uta-chakukicha, Uta-chakukicha, which are essential

15
00:01:55,360 --> 00:01:56,360
desire.

16
00:01:56,360 --> 00:02:03,800
You will spot and torpor worry and restlessness and doubt.

17
00:02:03,800 --> 00:02:09,080
In easy terms, when we're practicing, we use words which are easy to remember and easy

18
00:02:09,080 --> 00:02:17,160
to put into practice, liking, disliking, drowsiness, distraction and doubt.

19
00:02:17,160 --> 00:02:19,320
These are the, called the moderate defaments.

20
00:02:19,320 --> 00:02:25,760
These are the defaments which are more subtle than actions or speech or bad actions or

21
00:02:25,760 --> 00:02:30,480
bad speech, but they're still bad things as they lead to suffering inside and they can

22
00:02:30,480 --> 00:02:35,240
lead to action in the future.

23
00:02:35,240 --> 00:02:43,520
Then there are the subtle defalments which are the enusayat, which are called latent, in

24
00:02:43,520 --> 00:02:50,960
the sense that they aren't something which is arisen, but they are what causes the defalments

25
00:02:50,960 --> 00:03:06,440
to arise.

26
00:03:06,440 --> 00:03:29,840
The enusayat, which leads to the sensual lust, but the kat, which means the tendency

27
00:03:29,840 --> 00:03:35,920
to dislike things, and these are opposites, some things we have.

28
00:03:35,920 --> 00:03:42,280
We favor them some things we have a tendency to dislike by our nature.

29
00:03:42,280 --> 00:03:49,960
This is an inherent character trait, which leads us to anger and which leads us to greed.

30
00:03:49,960 --> 00:03:58,320
There is wrong view and there is conceit, there is restlessness, there is power-a-kat

31
00:03:58,320 --> 00:04:07,120
desire to be inclination to want to be this, to want to be that, to know each other,

32
00:04:07,120 --> 00:04:09,240
which is ignorance.

33
00:04:09,240 --> 00:04:14,080
These are the subtle defalments, but here we are talking about the moderate defaments,

34
00:04:14,080 --> 00:04:20,320
the ones which exist in the mind, and this is a very practical thing that in meditation

35
00:04:20,320 --> 00:04:29,040
we are practicing to remove the thoughts as they arise.

36
00:04:29,040 --> 00:04:34,400
So when a thought of sensuality arises, liking this, or liking that, wanting this, or

37
00:04:34,400 --> 00:04:45,560
wanting that, we don't allow it to build, we don't allow it to become more.

38
00:04:45,560 --> 00:04:56,040
The palli is, we know deity, we do away with it, we know deity, we remove it, or dispel

39
00:04:56,040 --> 00:04:57,040
it right away.

40
00:04:57,040 --> 00:05:08,200
We don't allow it to grow, send it off into nothingness, we do away with it, and we cause

41
00:05:08,200 --> 00:05:17,240
for it not to be, we cause it to drive it out of our minds.

42
00:05:17,240 --> 00:05:25,080
And this is accomplished in terms of sensuality, it is accomplished by focusing on the

43
00:05:25,080 --> 00:05:30,840
load sameness, of the body, or of the things around us.

44
00:05:30,840 --> 00:05:36,680
So when we have lust, a very simple way to do away with lust, or desires, to really examine

45
00:05:36,680 --> 00:05:41,200
the object, and come to see for ourselves whether it truly is desirable.

46
00:05:41,200 --> 00:05:50,840
We can look at it and see where actually it's something which is organic, and is perhaps

47
00:05:50,840 --> 00:05:54,360
undesirable in its real form.

48
00:05:54,360 --> 00:05:59,760
For instance, the human body, we look at it as something desirable.

49
00:05:59,760 --> 00:06:06,280
But if we consider carefully, we can see that this is only because we ourselves are human.

50
00:06:06,280 --> 00:06:15,840
If we look at animal body, we don't find it at all attractive, but if a certain animal

51
00:06:15,840 --> 00:06:20,040
looks at another animal's body of the same type, they will find it equally attractive.

52
00:06:20,040 --> 00:06:27,120
So when a dog looks at a dog, it seems attractive, when a pig looks at a pig seems attractive.

53
00:06:27,120 --> 00:06:30,200
And it's just so when a human looks at a human, the human looks attractive.

54
00:06:30,200 --> 00:06:36,120
But if we look at these bodies from an objective point of view, they don't seem really

55
00:06:36,120 --> 00:06:39,280
all that attractive in the end.

56
00:06:39,280 --> 00:06:49,880
They have all sorts of liquids and fluids pouring out and moving around and smells arising,

57
00:06:49,880 --> 00:06:52,080
falling apart.

58
00:06:52,080 --> 00:06:58,720
Something like a little bit of said like a very light, a very thin, flimsy bag with

59
00:06:58,720 --> 00:07:09,760
holes in it, full of rotten, disgusting, defiled things, you just pull the bag out of a

60
00:07:09,760 --> 00:07:16,200
cesspool, full of filth, full of excrement and so on, and the bag has holes in it when

61
00:07:16,200 --> 00:07:21,200
we carry it around, and so it's leaking all the time.

62
00:07:21,200 --> 00:07:26,800
This is a very simple way to do away with essential thoughts.

63
00:07:26,800 --> 00:07:31,640
When we have thoughts of anger or ill will arise in the mind, the way to do away with

64
00:07:31,640 --> 00:07:39,200
them, a very simple way is to bring up thoughts of loving kindness.

65
00:07:39,200 --> 00:07:44,800
So when we have anger towards someone or hatred towards someone, we can consider some of

66
00:07:44,800 --> 00:07:48,080
the good things, good qualities about that person.

67
00:07:48,080 --> 00:07:55,000
Perhaps they say bad things, but they don't do bad things, or they do bad things, but

68
00:07:55,000 --> 00:08:01,520
they say good things, or they do and they say bad things, but their minds are sometimes

69
00:08:01,520 --> 00:08:03,400
very pure.

70
00:08:03,400 --> 00:08:10,000
We think about some of the good things they have done, whereas this they do bad things

71
00:08:10,000 --> 00:08:16,240
in this way, but in this way they do good things, they say bad things here, but other

72
00:08:16,240 --> 00:08:23,320
times they will say good things, or we simply send out our love for them, wishing that

73
00:08:23,320 --> 00:08:28,560
they be happy and be free from suffering and realizing that all beings are heirs of their

74
00:08:28,560 --> 00:08:29,560
karma.

75
00:08:29,560 --> 00:08:35,800
When they do bad deeds, they will be the ones who get the bad result, and feeling sorry

76
00:08:35,800 --> 00:08:39,280
for them, wishing for them to be free from suffering.

77
00:08:39,280 --> 00:08:44,280
May they be able to leave behind their bad with their evil ways.

78
00:08:44,280 --> 00:08:47,960
This is a very simple way when we think about people in this way, wishing for them happiness,

79
00:08:47,960 --> 00:08:50,280
even though they may have done bad things for us.

80
00:08:50,280 --> 00:08:57,920
A very simple way to overcome thoughts of anger, but with thoughts of delusion, and

81
00:08:57,920 --> 00:09:05,200
delusion here is very hard to understand, very hard to pinpoint, very hard to catch and

82
00:09:05,200 --> 00:09:12,200
realize when it exists in our mind, it often takes someone else to point it out to us.

83
00:09:12,200 --> 00:09:17,520
Delusion is things like wrong few, believing that bad deeds are good deeds, good deeds

84
00:09:17,520 --> 00:09:21,640
or bad deeds, or there is no difference between a good deed or a bad deed.

85
00:09:21,640 --> 00:09:30,000
Bad deeds don't bring bad results, good deeds don't bring good results and so on.

86
00:09:30,000 --> 00:09:41,520
Concede, conceit or comparing or judging, for instance, we might hold on to wrong few, we

87
00:09:41,520 --> 00:09:51,360
might hold on to the view that killing is a good thing, stealing is a good thing or so on.

88
00:09:51,360 --> 00:09:58,960
And we might hold on to it as a view because of tradition, we can look at religious views

89
00:09:58,960 --> 00:10:00,160
and religious views often.

90
00:10:00,160 --> 00:10:06,040
The only reason people hold on to them is because they are our views, we can see people

91
00:10:06,040 --> 00:10:14,120
saying we are this religion or we are that religion and we do it this way, we do that.

92
00:10:14,120 --> 00:10:18,360
And we can see how really this conceit is the only reason often why people hold on to their

93
00:10:18,360 --> 00:10:19,360
views.

94
00:10:19,360 --> 00:10:23,680
There's nothing particular about their view which is more wholesome or more beneficial

95
00:10:23,680 --> 00:10:28,240
than the view of other people except for the fact that it is theirs and they hold on

96
00:10:28,240 --> 00:10:29,920
to it to be theirs.

97
00:10:29,920 --> 00:10:39,720
This is mine and they compare and they judge based simply on their conceit and they are

98
00:10:39,720 --> 00:10:46,160
judging of different views and different ways of being.

99
00:10:46,160 --> 00:10:52,320
We have conceit on a very simple level the conceit of comparing ourselves to other people

100
00:10:52,320 --> 00:11:01,360
and meditators comparing their practice to others, people in work or in society comparing

101
00:11:01,360 --> 00:11:06,680
themselves in terms of their social status and so on.

102
00:11:06,680 --> 00:11:14,960
Conceit and then we have ignorance, these things are all under the heading of delusion.

103
00:11:14,960 --> 00:11:19,480
Ignorance is simply not knowing when we allow our ignorance to come up and we say that

104
00:11:19,480 --> 00:11:30,880
we don't understand things or when we, out of ignorance, say something which is not true.

105
00:11:30,880 --> 00:11:38,640
Not knowing that it is false or knowing that it goes against truth saying that for instance

106
00:11:38,640 --> 00:11:49,040
that old age is not suffering, there is no suffering in this world.

107
00:11:49,040 --> 00:11:53,880
And the method to overcome delusion of course is in the same way to create the opposite.

108
00:11:53,880 --> 00:11:58,480
So with greed and with anger we have a way to do away with them on a temporary level

109
00:11:58,480 --> 00:12:05,600
but because they are also based in delusion due to ignorance, due to view and due to conceit

110
00:12:05,600 --> 00:12:12,320
we give rise to craving and rise to aversion because of that, because we haven't done

111
00:12:12,320 --> 00:12:19,760
away with the delusion which underlies them, we will always have the danger of further

112
00:12:19,760 --> 00:12:23,200
arising of greed and anger.

113
00:12:23,200 --> 00:12:30,760
So the method to overcome the delusion is also to create the opposite, the opposite

114
00:12:30,760 --> 00:12:37,800
of course is insight or wisdom, the opposite of ignorance is understanding, the opposite

115
00:12:37,800 --> 00:12:45,160
of delusion is clarity or insight, we pass on the clear seeing, seeing through or seeing

116
00:12:45,160 --> 00:12:51,360
into, seeing clearly.

117
00:12:51,360 --> 00:12:55,600
And how this is brought about is simply by creating the opposite thought.

118
00:12:55,600 --> 00:13:01,600
So when there is ignorance or there is darkness in the mind we create light and create

119
00:13:01,600 --> 00:13:02,600
clarity.

120
00:13:02,600 --> 00:13:08,080
And we say to ourselves, rising or falling, at the moment when the stomach is rising or

121
00:13:08,080 --> 00:13:14,520
falling instead of allowing for all sorts of use and conceits and ignorance and doubts

122
00:13:14,520 --> 00:13:21,080
and so on to arise in our mind which would then give rise to further distracted thoughts

123
00:13:21,080 --> 00:13:25,280
and then further anger and greed and so on.

124
00:13:25,280 --> 00:13:30,960
We create a clear thought which is clear which is devoid of delusion which is devoid

125
00:13:30,960 --> 00:13:37,080
of darkness and this is rising, it is a thought which arises in our mind and replaces

126
00:13:37,080 --> 00:13:45,520
the distracted unwholesome, errant thoughts which lead us down the wrong path.

127
00:13:45,520 --> 00:13:49,680
And we find our thoughts not leaving us anywhere, our thoughts are keeping us in the present

128
00:13:49,680 --> 00:13:56,080
moment allowing us to see clearer and clearer about the reality around us.

129
00:13:56,080 --> 00:14:02,160
And so this topic today is one of very great importance for meditators and one that we should

130
00:14:02,160 --> 00:14:06,840
always keep in mind that what we are on the lookout for as we practice rising, falling

131
00:14:06,840 --> 00:14:10,840
is we are on the lookout for greed, we are on the lookout for anger and most especially

132
00:14:10,840 --> 00:14:13,160
we are on the lookout for delusion.

133
00:14:13,160 --> 00:14:18,440
If anger is overwhelming we can take a break and send loving kindness, if we feel angry

134
00:14:18,440 --> 00:14:24,560
or frustrated or bored or so on, we can send love wishing ourselves to be happy wishing

135
00:14:24,560 --> 00:14:26,560
for all beings to be happy.

136
00:14:26,560 --> 00:14:31,640
If we find ourselves practicing and are confronted by lust, we can take a break and consider

137
00:14:31,640 --> 00:14:38,640
the repulsiveness of our body and the other bodies maybe if we have lust towards someone

138
00:14:38,640 --> 00:14:44,440
else's body, we can see well actually this is only because of our conditioning in the

139
00:14:44,440 --> 00:14:49,960
genes and the hormones which exist in the body, there is really no reason for that and

140
00:14:49,960 --> 00:14:57,920
there is no guarantee or there is no possible way really that it could lead to peace and

141
00:14:57,920 --> 00:15:02,960
happiness and satisfaction if I chase after this thing, this object of my attraction

142
00:15:02,960 --> 00:15:07,600
because it is something which has old age which has sickness which has death as it is

143
00:15:07,600 --> 00:15:15,240
final resting place, but it is not something which is free from dissolution.

144
00:15:15,240 --> 00:15:22,120
So we look at things in this way and these two meditations, these sort of alternative

145
00:15:22,120 --> 00:15:26,600
meditations and they do away with greed and anger temporarily and allow us to continue

146
00:15:26,600 --> 00:15:31,480
the deeper practice which is the practice to do away with delusion and at the moment

147
00:15:31,480 --> 00:15:38,240
when we say rise, when we say falling, we are creating the clear thought and replacing

148
00:15:38,240 --> 00:15:45,840
the deluded thought, the dark, the ignorant thought which is giving rise ever and again

149
00:15:45,840 --> 00:15:54,560
to more greed and more anger, more frustration, more hatred and more lust and more desire,

150
00:15:54,560 --> 00:15:58,760
keeping us from being content, keeping us from being at peace.

151
00:15:58,760 --> 00:16:03,560
When we are able to keep our minds and be simply content with keeping our minds in the

152
00:16:03,560 --> 00:16:11,600
present moment, this is when our life will truly have value and will truly seem to be

153
00:16:11,600 --> 00:16:13,920
really and truly satisfying to us.

154
00:16:13,920 --> 00:16:19,240
It is something which the meditators who come to practice here can see for themselves

155
00:16:19,240 --> 00:16:25,000
and are often able to remark for themselves and to how very real the results are of the

156
00:16:25,000 --> 00:16:30,680
best in the meditation and at the moment when we are practicing there seems to be no goal

157
00:16:30,680 --> 00:16:41,240
and there seems to be no external path and that simply all we are doing, all we are simply

158
00:16:41,240 --> 00:16:46,960
doing is coming back to the moment and letting go of our ambitions and our needs and

159
00:16:46,960 --> 00:16:55,320
our desires and our views and coming back to nature, coming back to reality and so this

160
00:16:55,320 --> 00:17:00,120
is an important topic, something for us to keep in mind when we practice and to always

161
00:17:00,120 --> 00:17:04,480
be trying to bring our minds back to the present moment, not being content that we are

162
00:17:04,480 --> 00:17:10,640
practicing one hour, two hours, three hours, how many hours a day, but how many moments

163
00:17:10,640 --> 00:17:16,960
are we clearly aware of it as a moment and when these moments gather together together,

164
00:17:16,960 --> 00:17:21,800
they will bring us to higher and higher clarity and understanding until finally we are able

165
00:17:21,800 --> 00:17:27,920
to break through the darkness and into the light and see clearly about the world around

166
00:17:27,920 --> 00:17:33,600
us at all times and this is the Lord Buddha's teaching and we know it in the upper half

167
00:17:33,600 --> 00:17:42,000
of that and we are doing away with their dispelling of the unwholesome thoughts which arise

168
00:17:42,000 --> 00:17:47,800
as a means of doing away with the asala, as a means of cleansing the mind of its impurities

169
00:17:47,800 --> 00:17:53,600
and purifying the mind of the things which make it go sour and go back and this is the

170
00:17:53,600 --> 00:17:58,600
talk for today.

