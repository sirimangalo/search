1
00:00:00,000 --> 00:00:10,680
She would come to the sessions crying, and she would cry, and I never asked once what the

2
00:00:10,680 --> 00:00:11,680
problem was.

3
00:00:11,680 --> 00:00:12,680
I didn't know what the problem was.

4
00:00:12,680 --> 00:00:17,640
Obviously, there was something intense that she actually wanted me to ask and was used

5
00:00:17,640 --> 00:00:20,400
to people asking, what's wrong with you and so on.

6
00:00:20,400 --> 00:00:32,040
So I didn't, and I challenged her in this way, and by being kind and really, I guess

7
00:00:32,040 --> 00:00:35,400
an important part is by being not interested.

8
00:00:35,400 --> 00:00:42,240
No kind of like, yes, yes, that's the way it is.

9
00:00:42,240 --> 00:00:46,880
And as a result teaching her that, yeah, it is like that and helping her to accept and

10
00:00:46,880 --> 00:00:51,080
overcome and let go.

11
00:00:51,080 --> 00:00:55,520
Finally after the course and after she had been with us for some time, I asked her or

12
00:00:55,520 --> 00:01:00,760
she told me, I didn't really ask, but she told me what had happened.

13
00:01:00,760 --> 00:01:11,280
And no, obviously, she was 41 at the time, and you wouldn't be able to tell it.

14
00:01:11,280 --> 00:01:16,600
It was really something that had crippled her for so many years for, let's say,

15
00:01:16,600 --> 00:01:21,680
if 30, 40 years, 30 years, she would have been crippled.

16
00:01:21,680 --> 00:01:27,240
She lost the better part of her life to that from looks of it.

17
00:01:27,240 --> 00:01:30,280
She left, and when she came back, she wanted to do another course.

18
00:01:30,280 --> 00:01:32,720
She was a totally different person.

19
00:01:32,720 --> 00:01:38,520
She had color in her face, her eyes, and then a look about her and so on.

20
00:01:38,520 --> 00:01:41,760
But clearly, this is something devastating.

21
00:01:41,760 --> 00:01:47,440
So it's wrong, and that's the first thing I want to say is just because I say this doesn't

22
00:01:47,440 --> 00:01:53,400
break a preset or that doesn't break a preset, doesn't mean by any sense that it's right.

23
00:01:53,400 --> 00:01:56,520
That's why I say the precepts are kind of curious in this way.

24
00:01:56,520 --> 00:02:02,800
The fifth precept actually, it's a very small rule for monks.

25
00:02:02,800 --> 00:02:09,680
For a monk to drink alcohol, it's not a small rule, but it's not one of the major rules.

26
00:02:09,680 --> 00:02:16,360
You're still a monk, a monk can get drunk, stinking drunk, pass out, and still not have

27
00:02:16,360 --> 00:02:23,560
to undergo any probation or anything of the sort.

28
00:02:23,560 --> 00:02:25,000
That doesn't mean it's terribly wrong.

29
00:02:25,000 --> 00:02:29,680
It just means that it puts it in its place.

30
00:02:29,680 --> 00:02:37,200
This is one of the precepts, and this is one of the things that you shouldn't do.

31
00:02:37,200 --> 00:02:43,960
The punishment side of things is, or the weight of the precepts, it's not indicative

32
00:02:43,960 --> 00:02:46,800
of how bad the act is.

33
00:02:46,800 --> 00:02:51,760
It's just filing things as being wrong and saying that this is something that you're not

34
00:02:51,760 --> 00:03:01,120
to do, if you do it, you should convince, you should, in the word, you should confess, confess

35
00:03:01,120 --> 00:03:02,120
that you've done it.

36
00:03:02,120 --> 00:03:08,880
See, too many languages.

37
00:03:08,880 --> 00:03:17,560
I would say in this case, it could be seen to fit in with the third precept.

38
00:03:17,560 --> 00:03:32,040
I would say any sensual engagement, romantic engagement, with another human

39
00:03:32,040 --> 00:03:50,760
being, that causes, that breaks a trust or something of that sort, so rape because it's

40
00:03:50,760 --> 00:03:58,200
without the permission of the other party, a adultery because it's without the permission

41
00:03:58,200 --> 00:04:00,000
or it's against the wishes.

42
00:04:00,000 --> 00:04:08,280
Let's say against the wishes of a third party, and so I would say that in general is how

43
00:04:08,280 --> 00:04:10,640
we should understand the third precept.

44
00:04:10,640 --> 00:04:37,720
I hope that answers your question.

