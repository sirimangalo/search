Hello everyone, just a bulletin here to let you know that if you haven't heard already
I've turned my video
Teachings on how to meditate into a book and here it is
this book is
Based on the transcriptions of the videos on how to meditate on YouTube
but with a lot of
Added it added clarified
Even changed
Some of the information to make it a little bit
More more clear and more correct
There's obviously only so much you can do with a video you can only record it once or twice
Especially when you're dealing with a an entire camera crew
so the book that was a lot easier and
hopefully I can update it
As I see fit and add more information, maybe adding there's even an appendix in here
You see it's got pictures of how to do the prostrations
How to do the walking meditation and sitting meditation, you know basic information how to stand and how to sit
the point really is that this booklet can be
It can be read without the need for a computer. You can print it up on my website. There is a link to
PDF versions that you can print up by their in letter paper or a four
and so it doesn't require the
any technology to
To use it and the other thing is of course as I said that it allows for added information
So I think this is I'm kind of excited about it
I think it's something that's gonna be really useful. I'm using it to teach
inmates in the Federal Detention Center
because they don't have access to a DVD player and I think it's going to be of similar use to
other people around the world so
I'm gonna probably print it up and maybe even publish it in a
proper booklet form. It's not that big. It's about 40
45 pages long and
You can print it up and put staples in it if you got a good stapler and
Make a video. Okay, I'll make a booklet. So
there you have it
That's all just that I'd let people know to
You know to get it out to more people. I'm not looking to
Sell myself. I'm not trying to be a star or or anything
I think this is something that's honestly useful for the world and I think it's
It's it's a good idea for anyone any one of us who's interested in in living in peace and happiness
I truly believe this that
The best way to live in peace and happiness is to not only help yourself
But to help those around you and to help the whole world and we can really turn this world into a better place if we if we all work at it like this
So and this is what I can do as a Buddhist monk
Thanks to everyone for your support your encouragement and your appreciation of the work
If you have questions, you're welcome to ask them. Please don't forget to subscribe for updates and
You'll keep in touch. Okay, that's all. See you soon.
