1
00:00:00,000 --> 00:00:06,360
Funte, you said that mental illness was on a spectrum, which is the same about sexuality.

2
00:00:06,360 --> 00:00:08,520
This is a growing opinion in the US.

3
00:00:08,520 --> 00:00:11,840
That's an interesting question.

4
00:00:11,840 --> 00:00:17,600
I've never gotten that question before, but I'd like that question.

5
00:00:17,600 --> 00:00:29,960
I would right away think that on an ultimate level, absolutely, but realistically,

6
00:00:29,960 --> 00:00:44,920
I really know why because the aspect of a man, what is it, the sign of a man is categorically

7
00:00:44,920 --> 00:00:52,200
different from the sign of a woman by definition.

8
00:00:52,200 --> 00:00:58,960
So for most people, there is an attraction to wonder the other.

9
00:00:58,960 --> 00:01:02,640
Now sometimes a man is attracted to the sign of a woman, sometimes a woman is attracted

10
00:01:02,640 --> 00:01:08,000
to the sign of a man, but they're not the same thing.

11
00:01:08,000 --> 00:01:12,600
It's just like some people are attracted to the sign of, and what I mean by sign is the characteristic,

12
00:01:12,600 --> 00:01:13,600
but we use the word sign.

13
00:01:13,600 --> 00:01:19,480
It means when you see something, like when you see this, there are rises in your mind

14
00:01:19,480 --> 00:01:29,760
the sign of a teapot, what we call the sign of a teapot, for me know the sign of a water bottle.

15
00:01:29,760 --> 00:01:36,360
But no, I mean I just, but this you have the sign of the McMaster University religious

16
00:01:36,360 --> 00:01:44,200
studies thumb drive, which apparently has some interesting things on it.

17
00:01:44,200 --> 00:01:49,320
There's a sign of a rises, when you see a man or someone, this is why when you see someone

18
00:01:49,320 --> 00:01:57,240
who is in it, what do you call, what's the word for someone who is a sexual, not a sexual,

19
00:01:57,240 --> 00:02:03,600
but who doesn't look like either a man or a woman, sometimes it's hard to tell.

20
00:02:03,600 --> 00:02:07,640
Sometimes you'll see someone and you'll say that's a man and then they'll turn around

21
00:02:07,640 --> 00:02:14,000
and it's a woman or usually that way because a few men dress up like women, you know,

22
00:02:14,000 --> 00:02:17,720
just are typically women's clothing, but from behind you'll often see someone who looks

23
00:02:17,720 --> 00:02:23,040
like a man and turns around and says, so the sign changes in your mind, but we have

24
00:02:23,040 --> 00:02:24,040
that attraction.

25
00:02:24,040 --> 00:02:27,200
Now there are other kinds of attraction, you could argue that someone could simply

26
00:02:27,200 --> 00:02:33,680
be attracted to the human body, to their aspects of the human body that have signs, like

27
00:02:33,680 --> 00:02:40,200
these kind of characteristics that are not neither male or female.

28
00:02:40,200 --> 00:02:47,880
So there are other ways of being attracted sexually to get a sexual arousal, people can

29
00:02:47,880 --> 00:02:54,720
I think, what could you argue, the sexual arousal towards what pain could you argue that?

30
00:02:54,720 --> 00:03:02,480
People get sexually aroused by pain, a sexual pain, I don't know, like not gender-based.

31
00:03:02,480 --> 00:03:04,160
I guess, right?

32
00:03:04,160 --> 00:03:09,040
But I think masochism is probably usually still associated with the fact that it's someone

33
00:03:09,040 --> 00:03:18,440
of the opposite gender or the same gender who is hurting you or that's, yeah.

34
00:03:18,440 --> 00:03:23,520
So it's not a spectrum in the same way at all, really, because mental illness by spectrum

35
00:03:23,520 --> 00:03:31,720
might just mean it's made up of mind states that are intense or less intense, right?

36
00:03:31,720 --> 00:03:37,000
But when we're talking about attachment, like there's attachment to heroin and there's attachment

37
00:03:37,000 --> 00:03:40,840
to cocaine and those are two different kinds of, they're two different categories of attachment

38
00:03:40,840 --> 00:03:48,400
because a person who is attached to heroin wants heroin, they don't want cocaine.

39
00:03:48,400 --> 00:03:52,760
Now they might be able to switch to take cocaine, it's not a very good example, but

40
00:03:52,760 --> 00:03:55,720
it's something you get the idea basically.

41
00:03:55,720 --> 00:03:57,720
Man and women are two different things.

42
00:03:57,720 --> 00:04:03,280
So I think it's going to always be more rare for people to be attracted to both genders.

43
00:04:03,280 --> 00:04:08,280
Now that being said, one person can develop attraction to both genders and we see that

44
00:04:08,280 --> 00:04:09,280
happening.

45
00:04:09,280 --> 00:04:15,640
It's becoming more and more blurred as bisexuality becomes a thing and people are less

46
00:04:15,640 --> 00:04:18,440
fixated on, I only like this gender.

47
00:04:18,440 --> 00:04:23,120
So they're reborn probably again and again is the same gender who is again and again attracted

48
00:04:23,120 --> 00:04:25,080
to the same, to the opposite gender.

49
00:04:25,080 --> 00:04:36,160
We're seeing less of that and more of this bisexuality, so it's becoming harder to really

50
00:04:36,160 --> 00:04:40,640
tell what your sexuality is and that's reasonable from a Buddhist point of view that makes

51
00:04:40,640 --> 00:04:45,960
perfect sense because it's not God didn't make us one way or the other, God doesn't

52
00:04:45,960 --> 00:04:49,840
want us, it's nothing silly like that.

53
00:04:49,840 --> 00:04:58,120
One is not more moral than the other or anything silly like that, but there are, to understand

54
00:04:58,120 --> 00:05:03,320
you have to understand the mechanics of it and it's different from mental illness.

55
00:05:03,320 --> 00:05:10,880
It's got different way of working, except that underneath it's still a mental illness,

56
00:05:10,880 --> 00:05:15,920
sexuality is a mental illness, boom, there you go.

57
00:05:15,920 --> 00:05:26,560
There's a quote for you.

