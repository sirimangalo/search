1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Damapada.

2
00:00:05,000 --> 00:00:12,000
Today we continue on with verse number 117, which reads as follows.

3
00:00:35,000 --> 00:00:44,800
You and Evil Deed, not Nangarapuna, one should not do such a deed again and again,

4
00:00:44,800 --> 00:00:57,800
Nangarapuna one should not become content or pleased by that Evil Deed, by such Evil Deeds.

5
00:00:57,800 --> 00:01:08,800
dukkul papa sogiyo, because suffering comes from the accumulation of evil.

6
00:01:08,800 --> 00:01:12,600
So this is quite actually quite an important verse.

7
00:01:12,600 --> 00:01:18,800
The story is quite simple, although to get the full story, we have to go back to the vinnaya.

8
00:01:18,800 --> 00:01:24,800
This is in relation to a monk called Saya Saka.

9
00:01:24,800 --> 00:01:40,800
Saya Saka was a roommate or a fellow resident of the monk, Udaya, or maybe Laludaya, or Laludaya,

10
00:01:40,800 --> 00:01:49,800
who was not a very good friend. They didn't have the best of habits himself.

11
00:01:49,800 --> 00:01:58,800
And Saya Saka was a monk who was discontent with the homeless life, discontent with the monastic life.

12
00:01:58,800 --> 00:02:12,800
He had been practicing, but was very much attached to sensuality and the home life, the worldly life,

13
00:02:12,800 --> 00:02:19,800
and all the things, all the wonderful bubbles and attractions that exist in the world that entice the senses.

14
00:02:19,800 --> 00:02:31,800
And so he became rather sick sickly. He wasn't able to eat the food because he would miss more luxurious food.

15
00:02:31,800 --> 00:02:37,800
He wasn't comfortable with his bedding. He wasn't comfortable with his lodging. He just wasn't happy.

16
00:02:37,800 --> 00:02:51,800
He was not being happy. His body shriveled up and his demeanor became unpleasant or he became less radiant.

17
00:02:51,800 --> 00:02:56,800
He lost the radiance and the glow about him.

18
00:02:56,800 --> 00:03:05,800
He became rather pale when they said the veins were sticking out of his skin and so he looked really, really bad.

19
00:03:05,800 --> 00:03:11,800
And so Laludaya, concerned, asked him, you know, what's wrong?

20
00:03:11,800 --> 00:03:15,800
He said, you look like you're having some trouble. He said, yeah, I am, I'm really not happy.

21
00:03:15,800 --> 00:03:23,800
And he said, well, here's what you should do. Eat whenever you want, sleep whenever you want, bathe whenever you want.

22
00:03:23,800 --> 00:03:31,800
And when you get up, when you get this desire, this sexual desire, come up.

23
00:03:31,800 --> 00:03:38,800
It's just, well, he basically tells him to just master me. Go ahead.

24
00:03:38,800 --> 00:03:42,800
And I mean, the story, this is a story of relation to the Vinaya.

25
00:03:42,800 --> 00:03:49,800
So it does get into some fairly intimate details in that regard.

26
00:03:49,800 --> 00:03:52,800
The Buddha eventually catches wind of it.

27
00:03:52,800 --> 00:03:56,800
Well, so what happens is, say a saka becomes quite radiant again.

28
00:03:56,800 --> 00:04:01,800
He regains his color and suddenly he looks normal again.

29
00:04:01,800 --> 00:04:06,800
And the other monks look at him and say, wow, hey, you look good.

30
00:04:06,800 --> 00:04:11,800
You were looking really kind of sickly and pale there for a while. What happened?

31
00:04:11,800 --> 00:04:21,800
And they said, oh, and he told them what the advice he'd been given and how he had changed his behavior.

32
00:04:21,800 --> 00:04:29,800
And the monks said, so do you use that same hand to eat the food that people give you?

33
00:04:29,800 --> 00:04:34,800
Is that the same hand that you use to, I mean, it's not that it mattered which hand,

34
00:04:34,800 --> 00:04:37,800
but basically making a point.

35
00:04:37,800 --> 00:04:42,800
And then you go ahead and eat food that's been given as a gift with the same hand.

36
00:04:42,800 --> 00:04:44,800
And he said, yeah.

37
00:04:44,800 --> 00:04:49,800
And everyone got very kind of ashamed and upset.

38
00:04:49,800 --> 00:04:55,800
And the thing is, this was back in the beginning of the sangha and there was no rule against masturbation.

39
00:04:55,800 --> 00:05:03,800
And so it wasn't actually against any rule that had been said down.

40
00:05:03,800 --> 00:05:11,800
But it clearly was unacceptable for many of the monks.

41
00:05:11,800 --> 00:05:16,800
And so the word got around to the Buddha and the Buddha called him up and admonished him and said,

42
00:05:16,800 --> 00:05:22,800
you worthless, you worthless man, worthless monk.

43
00:05:22,800 --> 00:05:28,800
I mean, it's just not something that, I mean, the whole idea of being a monk is to be celibate.

44
00:05:28,800 --> 00:05:32,800
And so masturbation just doesn't fit into the equation.

45
00:05:32,800 --> 00:05:40,800
And so the Buddha then laid down the rule that this was against the rule.

46
00:05:40,800 --> 00:05:48,800
That's the back story. But in this story in the Dhamapada, which led the Buddha to tell this verse,

47
00:05:48,800 --> 00:05:54,800
was apparently Sayyasaka, either during this time or after he was just having,

48
00:05:54,800 --> 00:05:57,800
he was unable to give up this habit.

49
00:05:57,800 --> 00:06:02,800
And so the Buddha then taught this verse to try and help him give up that habit.

50
00:06:02,800 --> 00:06:05,800
Whether he did or not, it's not clear, probably he didn't.

51
00:06:05,800 --> 00:06:07,800
But other people gained from the verse.

52
00:06:07,800 --> 00:06:15,800
It was official as a teaching because other people were able to see what comes of bad deeds.

53
00:06:15,800 --> 00:06:22,800
Now, the interesting thing in here with this verse and with the story is that it appears to be the opposite, right?

54
00:06:22,800 --> 00:06:28,800
Here was someone who was doing his best to refrain from unwholesomeness and suffering from it.

55
00:06:28,800 --> 00:06:36,800
And when he began to engage in unwholesome activity again, he got better, he got healthier, he got happier.

56
00:06:36,800 --> 00:06:45,800
So this is an argument that people give in favor of sexuality, in favor of all kinds of essential indulgence.

57
00:06:45,800 --> 00:06:53,800
They say, well, it leads to both happiness and even, you could say, physical health to have good food and so on.

58
00:06:53,800 --> 00:07:04,800
And there's arguments to be made in that regard both ways because, of course, eating good food eventually leads to an obsession with good food.

59
00:07:04,800 --> 00:07:14,800
And can lead to an obsession with taste. And as we know, our obsession with food and tastes often lead to great physical suffering.

60
00:07:14,800 --> 00:07:32,800
As for things like sexuality, well, it leads to great obsession and it does lead to irritation and can lead to other complications, sexually transmitted diseases.

61
00:07:32,800 --> 00:07:38,800
But, you know, the point being with this kind of argument is that that's not really the important point here.

62
00:07:38,800 --> 00:07:44,800
We're not really talking about the Buddha's talking about something quite a bit deeper that's hard to really understand.

63
00:07:44,800 --> 00:08:00,800
And the reason that white people are understandably turned away from turned off by religious teachings or are uninterested in things like self-control or celibacy

64
00:08:00,800 --> 00:08:05,800
or giving up, just in general, giving up desires, even if it were possible.

65
00:08:05,800 --> 00:08:07,800
They say, why would you want to give up desires?

66
00:08:07,800 --> 00:08:10,800
Why would you want to give up the things that bring you happiness?

67
00:08:10,800 --> 00:08:23,800
And so the Buddha cuts deeper than that and agree with him or not, he's making a bold claim that that actually,

68
00:08:23,800 --> 00:08:32,800
that the true result of unwholesumness is not happiness, but suffering, which appears to fly in the face of the evidence.

69
00:08:32,800 --> 00:08:41,800
And as a result, many people are not interested in spirituality and things like Buddhism, for example.

70
00:08:41,800 --> 00:08:43,800
It's quite a turn off of a thing.

71
00:08:43,800 --> 00:08:49,800
Maybe that's a wrong word in this context, but it's quite unpleasant for many people here.

72
00:08:49,800 --> 00:08:57,800
But I mean, even just the idea of celibacy for many people who vote repression and a lot of suffering.

73
00:08:57,800 --> 00:09:06,800
So we have to deal with this in this verse in the three parts.

74
00:09:06,800 --> 00:09:16,800
So he talks about not doing, if one should do things that are unwholesum, first of all,

75
00:09:16,800 --> 00:09:20,800
the real problem here is not actually the performance of the deed.

76
00:09:20,800 --> 00:09:29,800
The real problem that the Buddha is talking about is becoming pleased by them, or becoming attached to them.

77
00:09:29,800 --> 00:09:32,800
So that they become a habit.

78
00:09:32,800 --> 00:09:38,800
Because of course, the underlying problem is not the deed itself, but it's the unwholesumness.

79
00:09:38,800 --> 00:09:42,800
It's not even say unwholesum, but it's the desire attached.

80
00:09:42,800 --> 00:09:46,800
Now, why do we call things like desire unwholesum?

81
00:09:46,800 --> 00:09:48,800
Because they become an obsession.

82
00:09:48,800 --> 00:09:56,800
And while they do have, and the Buddha was quick to acknowledge, they have a benefit.

83
00:09:56,800 --> 00:09:59,800
There is the gratification of sensual desires.

84
00:09:59,800 --> 00:10:03,800
And that's what you could say, the good side of it.

85
00:10:03,800 --> 00:10:08,800
And yes, so there is pleasure that comes from the gratification of all kinds of sensual desires.

86
00:10:08,800 --> 00:10:14,800
They're sexual desire, but also food, and even beauty, music.

87
00:10:14,800 --> 00:10:16,800
All of these things bring pleasure.

88
00:10:16,800 --> 00:10:19,800
And that's certainly a gratification.

89
00:10:19,800 --> 00:10:28,800
Even the act of obtaining one of these desirable things is not actually a problem.

90
00:10:28,800 --> 00:10:31,800
It's not actually a problem to hear a beautiful sound.

91
00:10:31,800 --> 00:10:34,800
That's not unwholesum.

92
00:10:34,800 --> 00:10:38,800
It's not unwholesum to see something beautiful.

93
00:10:38,800 --> 00:10:44,800
It's not unwholesum even to feel, even to feel physical pleasure.

94
00:10:44,800 --> 00:10:46,800
It's not unwholesum to feel the pleasure.

95
00:10:46,800 --> 00:10:51,800
Now, the problem is, unless you're truly mindful and objective

96
00:10:51,800 --> 00:10:55,800
and seeing it as something that arises in ceases,

97
00:10:55,800 --> 00:10:59,800
absolutely we're going to become, we're going to become attached to it.

98
00:10:59,800 --> 00:11:01,800
We're going to desire to like it.

99
00:11:01,800 --> 00:11:06,800
And that's going to leave an imprint on the brain, which causes us to want it.

100
00:11:06,800 --> 00:11:11,800
And discontent when we don't have it,

101
00:11:11,800 --> 00:11:14,800
such that we think about how to get it, how to obtain it.

102
00:11:14,800 --> 00:11:19,800
And we enter into the cycle of addiction.

103
00:11:19,800 --> 00:11:24,800
And then we wonder why we're dissatisfied, why our lives are,

104
00:11:24,800 --> 00:11:28,800
why we have discontent in our lives, why we get frustrated,

105
00:11:28,800 --> 00:11:34,800
why we get angry, why we get bored, why we are given to rage,

106
00:11:34,800 --> 00:11:38,800
why we are given to argumentation, why we fight with each other.

107
00:11:38,800 --> 00:11:43,800
If we looked closely, if we looked carefully,

108
00:11:43,800 --> 00:11:47,800
we would see how closely related this is to our desire.

109
00:11:47,800 --> 00:11:48,800
We want something.

110
00:11:48,800 --> 00:11:52,800
We wish we could just enjoy all kinds of sensual pleasures all the time,

111
00:11:52,800 --> 00:12:02,800
like this monk was trying to do without realizing that when we can't get it,

112
00:12:02,800 --> 00:12:07,800
we become more upset when someone stands in our way.

113
00:12:07,800 --> 00:12:11,800
We get angry at them, when someone comes and provides us with a stimulus

114
00:12:11,800 --> 00:12:14,800
that is unpleasant telling us something, making us hear something,

115
00:12:14,800 --> 00:12:17,800
saying words that are unpleasant, make us angry.

116
00:12:17,800 --> 00:12:19,800
Because we just want to listen to music.

117
00:12:19,800 --> 00:12:21,800
We just want to see beautiful sights.

118
00:12:21,800 --> 00:12:25,800
We just want to feel beautiful feelings and so on.

119
00:12:25,800 --> 00:12:28,800
And so it leads directly to great suffering.

120
00:12:28,800 --> 00:12:32,800
Because of addiction, you see, you can say,

121
00:12:32,800 --> 00:12:34,800
well, I can always get what I want.

122
00:12:34,800 --> 00:12:36,800
Things are going to be fine.

123
00:12:36,800 --> 00:12:37,800
But you can never be sure.

124
00:12:37,800 --> 00:12:42,800
And you can never say that it's not going to change.

125
00:12:42,800 --> 00:12:44,800
It could change at any time.

126
00:12:44,800 --> 00:12:50,800
And in the meantime, all you're doing is building yourself up

127
00:12:50,800 --> 00:12:53,800
to eventual disappointment.

128
00:12:53,800 --> 00:12:54,800
You're not gaining anything else.

129
00:12:54,800 --> 00:12:56,800
There's nothing else changing in your life.

130
00:12:56,800 --> 00:12:58,800
You're not becoming more satisfied.

131
00:12:58,800 --> 00:13:01,800
You're not becoming happier.

132
00:13:01,800 --> 00:13:03,800
If anything, you become less happier

133
00:13:03,800 --> 00:13:07,800
because the way the addiction cycle works is the more you get what you want,

134
00:13:07,800 --> 00:13:09,800
the less satisfying it is.

135
00:13:09,800 --> 00:13:12,800
This is why we have to indulge in more and more

136
00:13:12,800 --> 00:13:16,800
and why our tastes become more and more exotic.

137
00:13:16,800 --> 00:13:21,800
Because it's diminishing returns.

138
00:13:21,800 --> 00:13:26,800
It becomes less and less satisfied, in fact.

139
00:13:26,800 --> 00:13:30,800
So anyone who says that this stuff leads you to happiness

140
00:13:30,800 --> 00:13:32,800
is incredibly short-sighted.

141
00:13:32,800 --> 00:13:33,800
Most of us are.

142
00:13:33,800 --> 00:13:36,800
There's no real criticism here.

143
00:13:36,800 --> 00:13:39,800
It's just a general criticism that we are missing,

144
00:13:39,800 --> 00:13:42,800
a very important piece of the puzzle in general.

145
00:13:42,800 --> 00:13:47,800
It's very hard for us to see deep enough to get past this

146
00:13:47,800 --> 00:13:50,800
while it makes me happy.

147
00:13:50,800 --> 00:13:55,800
If you look deeper, and if you look really deep,

148
00:13:55,800 --> 00:13:57,800
and it's not looking far,

149
00:13:57,800 --> 00:14:00,800
looking very closely at the present moment through meditation,

150
00:14:00,800 --> 00:14:04,800
which is how this verse very much relates to our practice.

151
00:14:04,800 --> 00:14:07,800
You will see that in fact, even pleasure,

152
00:14:07,800 --> 00:14:11,800
there's nothing about it.

153
00:14:11,800 --> 00:14:14,800
That makes it preferable to pain.

154
00:14:14,800 --> 00:14:16,800
If you think objectively,

155
00:14:16,800 --> 00:14:20,800
there's no reason to think that pleasure is better than pain.

156
00:14:20,800 --> 00:14:22,800
Why do we think that pleasure is better than pain?

157
00:14:22,800 --> 00:14:24,800
It's a good question.

158
00:14:24,800 --> 00:14:28,800
If you look closely, you'll see that it's just an experience.

159
00:14:28,800 --> 00:14:30,800
Pleasure is just pleasure.

160
00:14:30,800 --> 00:14:33,800
So in the meditation, the way we deal with,

161
00:14:33,800 --> 00:14:37,800
or the way we approach this real problem,

162
00:14:37,800 --> 00:14:40,800
the problem of addiction, the problem of attachment,

163
00:14:40,800 --> 00:14:43,800
this problem that sets us out for suffering,

164
00:14:43,800 --> 00:14:48,800
and keeps us so caught up in this cycle,

165
00:14:48,800 --> 00:14:51,800
is to see it clearly, to look clearly,

166
00:14:51,800 --> 00:14:54,800
to see the pleasure as pleasure,

167
00:14:54,800 --> 00:14:57,800
to see the liking as liking, the wanting as wanting,

168
00:14:57,800 --> 00:15:00,800
the seeing as seeing, the hearing as hearing,

169
00:15:00,800 --> 00:15:04,800
the feeling as feeling, to break it up

170
00:15:04,800 --> 00:15:07,800
and look at what's really happening.

171
00:15:07,800 --> 00:15:08,800
And when you see what's really happening,

172
00:15:08,800 --> 00:15:09,800
you see there's nothing about it.

173
00:15:09,800 --> 00:15:12,800
That's really worth clinging to.

174
00:15:12,800 --> 00:15:14,800
There's nothing in the world.

175
00:15:14,800 --> 00:15:18,800
Nothing in existence that is worth clinging to.

176
00:15:18,800 --> 00:15:21,800
When you see that,

177
00:15:21,800 --> 00:15:23,800
and it's not a matter of oppression.

178
00:15:23,800 --> 00:15:24,800
It's just a matter of letting go,

179
00:15:24,800 --> 00:15:26,800
a matter of freeing yourself

180
00:15:26,800 --> 00:15:28,800
from any need or any partiality.

181
00:15:28,800 --> 00:15:31,800
It's about rooting yourself in reality,

182
00:15:31,800 --> 00:15:35,800
as opposed to clinging to the past or the future,

183
00:15:35,800 --> 00:15:37,800
things that don't exist,

184
00:15:37,800 --> 00:15:42,800
always wanting, always being unsatisfied.

185
00:15:45,800 --> 00:15:47,800
Or having your satisfaction depend on things

186
00:15:47,800 --> 00:15:49,800
that are undependable,

187
00:15:49,800 --> 00:15:51,800
that are not dependable,

188
00:15:51,800 --> 00:15:54,800
that you can't depend upon in the end.

189
00:15:54,800 --> 00:15:57,800
So, this is in regards to,

190
00:15:59,800 --> 00:16:00,800
actually doing,

191
00:16:00,800 --> 00:16:02,800
no, that's really just the first part,

192
00:16:02,800 --> 00:16:04,800
Papan-Chi-Pouly-Soka-Hira.

193
00:16:04,800 --> 00:16:06,800
So, in regards to doing of unholes and deeds.

194
00:16:06,800 --> 00:16:09,800
Now, the second part,

195
00:16:09,800 --> 00:16:11,800
Nanam-Chi-Chi-Pouly-Pouly,

196
00:16:11,800 --> 00:16:13,800
deals with the habit.

197
00:16:13,800 --> 00:16:15,800
So, I talked a little bit about it,

198
00:16:15,800 --> 00:16:17,800
but just to go through this first,

199
00:16:17,800 --> 00:16:21,800
the second part is in regards to how it becomes habit forming.

200
00:16:21,800 --> 00:16:23,800
How addiction,

201
00:16:23,800 --> 00:16:26,800
the problem with addiction

202
00:16:26,800 --> 00:16:29,800
is that anything you perform,

203
00:16:29,800 --> 00:16:32,800
any activity that you perform,

204
00:16:32,800 --> 00:16:34,800
becomes habitual.

205
00:16:34,800 --> 00:16:37,800
So, wanting breeds more wanting.

206
00:16:37,800 --> 00:16:39,800
And the same goes with anger.

207
00:16:39,800 --> 00:16:42,800
If you're a person who gets angry or averse to certain things,

208
00:16:42,800 --> 00:16:45,800
you'll cultivate a habit of aversion.

209
00:16:45,800 --> 00:16:49,800
So, the idea of doing things again and again

210
00:16:49,800 --> 00:16:52,800
is a real issue in Buddhism, a real problem.

211
00:16:52,800 --> 00:16:55,800
Something that we have to be very careful

212
00:16:55,800 --> 00:16:59,800
when cultivating habits,

213
00:16:59,800 --> 00:17:01,800
that they are wholesome,

214
00:17:01,800 --> 00:17:03,800
that they are beneficial.

215
00:17:03,800 --> 00:17:06,800
So, the whole idea behind constant meditation,

216
00:17:06,800 --> 00:17:08,800
daily meditation,

217
00:17:08,800 --> 00:17:12,800
is to cultivate wholesome habits, positive habits,

218
00:17:12,800 --> 00:17:17,800
habits that do lead to true peace and happiness and clarity,

219
00:17:17,800 --> 00:17:20,800
that allow us to see things as they are,

220
00:17:20,800 --> 00:17:27,800
and not set ourselves up for real disappointment.

221
00:17:27,800 --> 00:17:30,800
And the third part is,

222
00:17:30,800 --> 00:17:35,800
not just doing things again and again,

223
00:17:35,800 --> 00:17:37,800
but on top of that,

224
00:17:37,800 --> 00:17:40,800
we become content with it.

225
00:17:40,800 --> 00:17:42,800
We like the fact that we like.

226
00:17:42,800 --> 00:17:46,800
We're happy about the fact that we want things.

227
00:17:46,800 --> 00:17:48,800
We become, and not just wanting,

228
00:17:48,800 --> 00:17:50,800
but with anger, with aversion,

229
00:17:50,800 --> 00:17:53,800
with arrogance and conceit,

230
00:17:53,800 --> 00:17:56,800
we hold on to these,

231
00:17:56,800 --> 00:17:58,800
and that's even worse.

232
00:17:58,800 --> 00:18:01,800
So, doing a bad deed,

233
00:18:01,800 --> 00:18:02,800
this is problematic.

234
00:18:02,800 --> 00:18:04,800
Doing something,

235
00:18:04,800 --> 00:18:05,800
if you hurt someone else,

236
00:18:05,800 --> 00:18:07,800
or if you steal from someone else,

237
00:18:07,800 --> 00:18:08,800
or if you take which is not yours,

238
00:18:08,800 --> 00:18:11,800
those are fairly obvious on wholesome deeds.

239
00:18:11,800 --> 00:18:12,800
We call those evil.

240
00:18:12,800 --> 00:18:13,800
They're evil, why?

241
00:18:13,800 --> 00:18:14,800
Because they hurt others,

242
00:18:14,800 --> 00:18:18,800
and they come from a point of view of hypocrisy

243
00:18:18,800 --> 00:18:20,800
where you don't want to experience it yourself,

244
00:18:20,800 --> 00:18:23,800
and you impose suffering on others,

245
00:18:23,800 --> 00:18:31,800
or any number of deed that stains your mind in some way,

246
00:18:31,800 --> 00:18:33,800
whether through anger or through greed,

247
00:18:33,800 --> 00:18:38,800
even if you just engage in some kind of addictive behavior.

248
00:18:38,800 --> 00:18:40,800
Well, then it leaves a stain on your mind.

249
00:18:40,800 --> 00:18:42,800
It affects your mind.

250
00:18:42,800 --> 00:18:45,800
Once off, it'll still consider harmful,

251
00:18:45,800 --> 00:18:47,800
but when you do it again and again,

252
00:18:47,800 --> 00:18:49,800
this is when the real trouble comes,

253
00:18:49,800 --> 00:18:51,800
when you get caught up in it,

254
00:18:51,800 --> 00:18:55,800
because it changes your mind,

255
00:18:55,800 --> 00:18:58,800
it changes your life, it changes many,

256
00:18:58,800 --> 00:19:00,800
changes your course.

257
00:19:00,800 --> 00:19:02,800
But worse than that, the worst of all,

258
00:19:02,800 --> 00:19:05,800
is when you become happy about it,

259
00:19:05,800 --> 00:19:07,800
when you're content with it.

260
00:19:07,800 --> 00:19:08,800
So sometimes you do something,

261
00:19:08,800 --> 00:19:09,800
and you're like, oh, yes, well,

262
00:19:09,800 --> 00:19:11,800
this is a problem I have,

263
00:19:11,800 --> 00:19:12,800
and I'm trying to work on it.

264
00:19:12,800 --> 00:19:15,800
An alcoholic who knows their alcoholic,

265
00:19:15,800 --> 00:19:19,800
better than an alcoholic who is in denial,

266
00:19:19,800 --> 00:19:22,800
and who is trying to change.

267
00:19:22,800 --> 00:19:28,800
People I think often dismiss this desire to change,

268
00:19:28,800 --> 00:19:30,800
because while the person wants to change,

269
00:19:30,800 --> 00:19:32,800
well, but they're not changing.

270
00:19:32,800 --> 00:19:33,800
And it's true.

271
00:19:33,800 --> 00:19:36,800
I think you could argue that sometimes we just use it

272
00:19:36,800 --> 00:19:38,800
as an excuse or we feel guilty about it

273
00:19:38,800 --> 00:19:41,800
instead of actually doing something about it.

274
00:19:41,800 --> 00:19:44,800
But there's a story that I'm trying to find,

275
00:19:44,800 --> 00:19:48,800
to a story that I plan on sharing with a group here,

276
00:19:48,800 --> 00:19:53,800
I'm pretty sure it's in the jot because about this bird

277
00:19:53,800 --> 00:19:56,800
who lived in this forest,

278
00:19:56,800 --> 00:19:58,800
and the forest caught on fire.

279
00:19:58,800 --> 00:20:03,800
And the bird didn't want to abandon the forest,

280
00:20:03,800 --> 00:20:07,800
so it flew all the way to the lake,

281
00:20:07,800 --> 00:20:09,800
plunged into the lake,

282
00:20:09,800 --> 00:20:11,800
and flew back to the forest

283
00:20:11,800 --> 00:20:14,800
and shook itself vigorously over the fire.

284
00:20:14,800 --> 00:20:17,800
And there was an angel or a god or something watching,

285
00:20:17,800 --> 00:20:20,800
and the god said, the angel said,

286
00:20:20,800 --> 00:20:22,800
what are you doing?

287
00:20:22,800 --> 00:20:24,800
And he said, I'm trying to put out the fire,

288
00:20:24,800 --> 00:20:26,800
and he said, what are you crazy?

289
00:20:26,800 --> 00:20:28,800
You're not going to put out the fire that way.

290
00:20:28,800 --> 00:20:30,800
And he said, well, what else can I do?

291
00:20:30,800 --> 00:20:32,800
Well, yeah, but what do you mean

292
00:20:32,800 --> 00:20:33,800
what else can do?

293
00:20:33,800 --> 00:20:34,800
It's hopeless.

294
00:20:34,800 --> 00:20:37,800
You're trying to do something that

295
00:20:37,800 --> 00:20:40,800
and you're never going to succeed at this point.

296
00:20:40,800 --> 00:20:42,800
And it gives some fairly wise teaching

297
00:20:42,800 --> 00:20:44,800
on why it's important to try.

298
00:20:44,800 --> 00:20:47,800
And this is, I think, important in Buddhism.

299
00:20:47,800 --> 00:20:50,800
The mind state, one's mind state.

300
00:20:50,800 --> 00:20:52,800
Because when you get, if you compare that

301
00:20:52,800 --> 00:20:55,800
to someone who gives in and says,

302
00:20:55,800 --> 00:20:58,800
not even just gives in,

303
00:20:58,800 --> 00:21:01,800
but goes full on and says, yes,

304
00:21:01,800 --> 00:21:04,800
it's good to eat, drink, and be merry, for example.

305
00:21:04,800 --> 00:21:07,800
Or like these two monks who got it in their heads

306
00:21:07,800 --> 00:21:10,800
that if they were having a problem with the holy life,

307
00:21:10,800 --> 00:21:16,800
they'll just stop being so holy.

308
00:21:16,800 --> 00:21:18,800
And somehow that would work.

309
00:21:18,800 --> 00:21:20,800
Really problematic.

310
00:21:20,800 --> 00:21:24,800
Because then when you're 100% behind something,

311
00:21:24,800 --> 00:21:27,800
you see it becomes much more powerful in the mind.

312
00:21:27,800 --> 00:21:30,800
And so the result is going to be much stronger.

313
00:21:30,800 --> 00:21:32,800
So consider these levels as well.

314
00:21:32,800 --> 00:21:33,800
I think that's important.

315
00:21:33,800 --> 00:21:36,800
Don't feel too guilty about bad things you do.

316
00:21:36,800 --> 00:21:41,800
They're bad, but just be careful about cultivating bad habits.

317
00:21:41,800 --> 00:21:44,800
And even if you cultivate bad habits,

318
00:21:44,800 --> 00:21:48,800
work, be clear about where you stand.

319
00:21:48,800 --> 00:21:53,800
Because the worst would be if you become content with complacent

320
00:21:53,800 --> 00:21:57,800
or if you become up the view

321
00:21:57,800 --> 00:22:00,800
that it's good to become addicted,

322
00:22:00,800 --> 00:22:03,800
good to be angry, good to be arrogant,

323
00:22:03,800 --> 00:22:06,800
good to cultivate unhostiveness.

324
00:22:06,800 --> 00:22:07,800
Why?

325
00:22:07,800 --> 00:22:09,800
And that's the fourth part.

326
00:22:09,800 --> 00:22:11,800
Because the accumulation of evil,

327
00:22:11,800 --> 00:22:15,800
especially when you're keen on it,

328
00:22:15,800 --> 00:22:22,800
when you're happy about it, is suffering.

329
00:22:22,800 --> 00:22:25,800
So that's the teaching for this verse.

330
00:22:25,800 --> 00:22:27,800
Very simple story.

331
00:22:27,800 --> 00:22:29,800
Very simple verse.

332
00:22:29,800 --> 00:22:31,800
And we have its pair coming tomorrow,

333
00:22:31,800 --> 00:22:33,800
which is a more of a lighter note,

334
00:22:33,800 --> 00:22:38,800
but a very good teaching, a verse to remember,

335
00:22:38,800 --> 00:22:41,800
if one should do evil, one should not do it again and again.

336
00:22:41,800 --> 00:22:44,800
One should not cultivate contentment

337
00:22:44,800 --> 00:22:47,800
or desire for that,

338
00:22:47,800 --> 00:22:52,800
or one should not be pleased by that deed.

339
00:22:52,800 --> 00:23:00,800
For suffering, for the accumulation of evil is suffering.

340
00:23:00,800 --> 00:23:03,800
So that's the Dhammapada teaching for tonight.

341
00:23:03,800 --> 00:23:28,800
Thank you all for tuning in.

