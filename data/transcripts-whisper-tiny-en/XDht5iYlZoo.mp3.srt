1
00:00:00,000 --> 00:00:07,840
So this might sound naive, but is there really such a thing as mood or mad, or is it just our minds that

2
00:00:07,840 --> 00:00:14,720
puts everything in those categories? Sorry, that's a silly question. It's not a silly question.

3
00:00:14,720 --> 00:00:17,520
We just discussed this, though, didn't we? We didn't record it.

4
00:00:19,680 --> 00:00:26,000
Something is good or bad if it causes suffering or happiness. That's the Buddhist definition.

5
00:00:26,000 --> 00:00:31,120
We do talk about good and bad, and the Buddha talked about good and bad, but he was

6
00:00:31,120 --> 00:00:36,960
scientific about it. So, whether you agree with him or not, good is something that brings happiness,

7
00:00:36,960 --> 00:00:44,400
bad is something that brings suffering. And there are, according to Buddhism, those things that

8
00:00:44,400 --> 00:00:50,720
do invariably bring suffering, and those things which do invariably bring happiness.

9
00:00:50,720 --> 00:00:59,120
So, that's what we mean. Now, what I was saying earlier is that agreeing with Larry that

10
00:01:01,920 --> 00:01:09,520
everything should be looked at, as you say, as objective realities. We shouldn't look at things

11
00:01:09,520 --> 00:01:14,080
as good or bad. That's in fact quite important, is that even the things that cause you suffering,

12
00:01:14,080 --> 00:01:24,640
you should not look at them as bad, because that intellectual cheating. It's begging the question,

13
00:01:24,640 --> 00:01:31,200
maybe, you're jumping the gun. You're coming to the conclusion before you have the premises,

14
00:01:31,920 --> 00:01:38,320
and meaning is you're pretending that you know that something is bad for you,

15
00:01:38,320 --> 00:01:44,720
without actually knowing that it's bad for you. The practice to see what is good and bad for you

16
00:01:44,720 --> 00:01:50,560
is to look at things objectively, not as good or bad. Look at them objectively and see whether

17
00:01:50,560 --> 00:01:56,880
they are good or bad. And so, it's actually two different definitions of good and bad, because

18
00:01:58,160 --> 00:02:03,040
it sounds like the same thing, but in practice it's two totally different things. Thinking of

19
00:02:03,040 --> 00:02:09,600
things as good or bad is judging them. Seeing that things are good or bad is understanding them,

20
00:02:09,600 --> 00:02:16,720
and it's two very different mind states. Judging is negative. It distracts you from the truth.

21
00:02:16,720 --> 00:02:22,320
It prevents you from seeing the truth, because you're no longer objective. But realizing that

22
00:02:22,320 --> 00:02:29,280
things are good or bad is liberating. It is the moment before you let go of something. Once you see

23
00:02:29,280 --> 00:02:35,760
that something is bad, you'll let go of it in the next moment. Here's a follow-up to-

