1
00:00:00,000 --> 00:00:04,760
Okay, Ian asks, are you familiar with astral projection?

2
00:00:04,760 --> 00:00:07,160
If so, what are your views on it?

3
00:00:11,120 --> 00:00:13,960
Well, I get a lot of questions like this and I,

4
00:00:13,960 --> 00:00:15,280
some of them I don't answer,

5
00:00:15,280 --> 00:00:17,920
but astral projection in particular,

6
00:00:17,920 --> 00:00:22,920
is one that I can comment on because when I was 13,

7
00:00:24,560 --> 00:00:28,000
I did some sort of astral projection.

8
00:00:28,000 --> 00:00:30,800
And this is part of why it's really interesting for me

9
00:00:30,800 --> 00:00:36,000
to think about doing a video for children on meditation

10
00:00:36,000 --> 00:00:38,280
because a lot of interesting things would come from it.

11
00:00:41,240 --> 00:00:43,360
When I was 13, I was doing this meditation

12
00:00:43,360 --> 00:00:45,080
where I would lie in bed

13
00:00:46,680 --> 00:00:50,280
and I would rock the bed back and forth

14
00:00:50,280 --> 00:00:52,920
like a hammock in my mind.

15
00:00:54,080 --> 00:00:56,000
If any of you are familiar with astral projection,

16
00:00:56,000 --> 00:00:57,520
I probably know where this is going to go

17
00:00:57,520 --> 00:01:01,000
and can understand how it went the way it did

18
00:01:02,480 --> 00:01:04,840
because they don't use this technique

19
00:01:04,840 --> 00:01:07,680
but they use this sort of technique.

20
00:01:07,680 --> 00:01:09,960
So, and eventually I could get to the point

21
00:01:09,960 --> 00:01:14,400
where my whole bed was rocking like a hammock

22
00:01:14,400 --> 00:01:16,040
and it felt, it really felt like it.

23
00:01:16,960 --> 00:01:18,040
And then I could get it to the point

24
00:01:18,040 --> 00:01:19,600
where the bed would do loops.

25
00:01:22,040 --> 00:01:23,120
I would be lying in bed

26
00:01:23,120 --> 00:01:26,720
and the whole bed would be doing loops like a hammock.

27
00:01:26,720 --> 00:01:27,560
That's why I was in a hammock

28
00:01:27,560 --> 00:01:29,440
but doing loops and I'd go back and forth.

29
00:01:30,480 --> 00:01:32,640
And I did this day after day.

30
00:01:32,640 --> 00:01:34,120
The reason I was doing it,

31
00:01:34,120 --> 00:01:35,640
I hadn't learned it from anywhere

32
00:01:35,640 --> 00:01:40,000
but it felt good and it made me feel calm.

33
00:01:40,000 --> 00:01:43,760
And it was an antidote for all of the suffering

34
00:01:43,760 --> 00:01:44,600
that I felt.

35
00:01:46,880 --> 00:01:50,240
Suffering at school and unstress as well from school.

36
00:01:50,240 --> 00:01:53,560
But just general depression

37
00:01:53,560 --> 00:01:57,240
being a teenager,

38
00:01:57,240 --> 00:01:59,960
no, enough said.

39
00:02:01,320 --> 00:02:06,480
And so I did this for quite some time.

40
00:02:06,480 --> 00:02:07,760
Got good at it.

41
00:02:07,760 --> 00:02:11,160
And then at one point I started doing it from head to toe

42
00:02:12,160 --> 00:02:14,440
where I would, it would be like a hammock

43
00:02:14,440 --> 00:02:15,400
but going the other way.

44
00:02:15,400 --> 00:02:19,880
So, going up at my toes and then up at my head

45
00:02:19,880 --> 00:02:22,120
and swinging in this direction.

46
00:02:22,120 --> 00:02:25,920
And then one day it happened that my mind popped out

47
00:02:25,920 --> 00:02:29,120
of my body, popped out of my head, just whoop.

48
00:02:30,760 --> 00:02:32,800
And you know, it's so long ago that it's,

49
00:02:32,800 --> 00:02:34,680
I'm just because I repeat this again

50
00:02:34,680 --> 00:02:36,760
and again I'm able to remember it.

51
00:02:36,760 --> 00:02:40,000
But I kind of get the feeling back now

52
00:02:40,000 --> 00:02:40,960
of what it was like.

53
00:02:44,880 --> 00:02:48,800
And I floated, I was upstairs, I floated downstairs.

54
00:02:48,800 --> 00:02:51,880
And my younger brother was pouring a bowl of cereal.

55
00:02:52,880 --> 00:02:57,680
And I watched him pour the bowl of cereal,

56
00:02:57,680 --> 00:03:00,240
pour the milk into the cereal.

57
00:03:00,240 --> 00:03:04,960
And then he was turning to put the cereal away

58
00:03:04,960 --> 00:03:07,800
or something like that, put something away.

59
00:03:07,800 --> 00:03:11,720
And his elbow or the milk or something hit the bowl of cereal

60
00:03:11,720 --> 00:03:14,840
and knocked it on the floor and smashed the bowl into pieces

61
00:03:14,840 --> 00:03:19,240
with the cereal going everywhere.

62
00:03:19,240 --> 00:03:21,600
And I saw this and after seeing it,

63
00:03:21,600 --> 00:03:25,160
I floated back upstairs and back into my head,

64
00:03:25,160 --> 00:03:27,400
back into my body.

65
00:03:27,400 --> 00:03:29,400
And then I got up and you know, kind of like,

66
00:03:29,400 --> 00:03:33,600
wow that was a big weird, went downstairs.

67
00:03:33,600 --> 00:03:37,200
And saw my brother pour a bowl of cereal,

68
00:03:37,200 --> 00:03:39,800
turned to put the cereal away and knocked the bowl of cereal

69
00:03:39,800 --> 00:03:44,280
on the floor, smashing it into pieces.

70
00:03:44,280 --> 00:03:47,720
So not only was it astral projection,

71
00:03:47,720 --> 00:03:51,960
but I think that's what the term astral projection is.

72
00:03:51,960 --> 00:03:53,520
I might be totally off base actually.

73
00:03:53,520 --> 00:03:56,240
You might be talking about something else.

74
00:03:56,240 --> 00:03:59,600
We, I think nowadays they refer to it as out-of-body

75
00:03:59,600 --> 00:04:04,600
experiences or O-O-B-E, there's this group of researchers

76
00:04:07,680 --> 00:04:13,400
in University of Georgia based on the work of Ian Stevenson

77
00:04:13,400 --> 00:04:15,000
and they study this kind of thing.

78
00:04:18,680 --> 00:04:21,240
Yeah, but not only was it an out-of-body experience,

79
00:04:21,240 --> 00:04:26,040
but it was also a, what do you call it,

80
00:04:27,080 --> 00:04:28,560
seeing into the future?

81
00:04:30,320 --> 00:04:35,080
It was such a banal experience or subject.

82
00:04:36,920 --> 00:04:39,760
But that was my one experience of something

83
00:04:39,760 --> 00:04:41,320
like astral projection or so on.

84
00:04:41,320 --> 00:04:43,360
So I believe it's possible.

85
00:04:43,360 --> 00:04:46,760
It seems, it seems quite logical and quite reasonable,

86
00:04:46,760 --> 00:04:48,520
quite simple.

87
00:04:49,480 --> 00:04:51,760
And I could, if you want, you could train in it.

88
00:04:51,760 --> 00:04:54,200
If you're interested, do the sort of thing that I did

89
00:04:54,200 --> 00:04:59,000
and take weeks or months to train like that

90
00:04:59,000 --> 00:05:00,880
and see what happens.

91
00:05:00,880 --> 00:05:03,160
There are easier ways, there are documented ways

92
00:05:03,160 --> 00:05:07,280
where you, I think they also recommend lying down.

93
00:05:07,280 --> 00:05:09,480
It's funny, I must have done it in the past life or something

94
00:05:09,480 --> 00:05:12,680
because it just came, I didn't read about it at all.

95
00:05:12,680 --> 00:05:14,560
They're talking about lying down

96
00:05:14,560 --> 00:05:17,720
and something about moving your mind,

97
00:05:17,720 --> 00:05:19,240
getting your mind out of your body anyway.

98
00:05:19,240 --> 00:05:20,520
They have, there are techniques I think

99
00:05:20,520 --> 00:05:21,920
on the internet even about it.

100
00:05:23,880 --> 00:05:25,240
So I don't have many views on it,

101
00:05:25,240 --> 00:05:26,800
but the other thing I want to say is,

102
00:05:26,800 --> 00:05:28,000
is what are my views on it?

103
00:05:28,000 --> 00:05:31,960
Is that, these are obviously not what I teach

104
00:05:31,960 --> 00:05:33,600
and not what I'm interested in.

105
00:05:33,600 --> 00:05:38,240
And in general, not what Buddhist teachers and practitioners

106
00:05:38,240 --> 00:05:41,240
are interested in, that's why I don't generally answer

107
00:05:41,240 --> 00:05:44,040
these sorts of questions when they come up.

108
00:05:44,040 --> 00:05:48,760
Questions about the third eye and the chakras

109
00:05:48,760 --> 00:05:52,840
and so on, because they're really beside the point.

110
00:05:52,840 --> 00:05:54,240
The Buddha taught four things.

111
00:05:54,240 --> 00:05:56,200
He taught the cause of suffering,

112
00:05:56,200 --> 00:05:59,200
the cause of suffering, the cessation of suffering

113
00:05:59,200 --> 00:06:02,560
and the path which leads to the cessation of suffering.

114
00:06:02,560 --> 00:06:06,360
And this astro projection is not in one of those.

115
00:06:06,360 --> 00:06:08,080
If anything, it's the truth of suffering

116
00:06:08,080 --> 00:06:10,640
because it's not going to satisfy you.

117
00:06:10,640 --> 00:06:13,640
It's not going to bring true peace and happiness.

118
00:06:13,640 --> 00:06:15,840
It's not going to bring wisdom and understanding.

119
00:06:17,440 --> 00:06:20,360
All it does is make you feel good

120
00:06:20,360 --> 00:06:23,800
and give me something to go, that's neat.

121
00:06:23,800 --> 00:06:39,720
Okay, so thanks for the question.

