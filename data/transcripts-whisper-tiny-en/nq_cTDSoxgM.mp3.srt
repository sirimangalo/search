1
00:00:00,000 --> 00:00:09,000
Bante, you mentioned the old monk will be exiting because he falls ill, where will the old monks go when they become ill, which is the usual monastic practice.

2
00:00:09,000 --> 00:00:16,000
The usual monastic practice is to incinerate them.

3
00:00:16,000 --> 00:00:19,000
Burn the bodies, do a funeral ceremony.

4
00:00:19,000 --> 00:00:25,000
Exit means die, he is very old and will take care of him until he dies.

5
00:00:25,000 --> 00:00:32,000
What I meant by exiting was just this euphemistic way of saying he is dying.

6
00:00:32,000 --> 00:00:34,000
Of course we are all dying.

7
00:00:34,000 --> 00:00:38,000
As long as people are still alive we take care of them.

8
00:00:38,000 --> 00:00:42,000
But when they die we call it quits.

9
00:00:42,000 --> 00:00:47,000
Burn them.

10
00:00:47,000 --> 00:00:54,000
And do away with the ashes and say our last goodbyes.

