1
00:00:00,000 --> 00:00:07,280
sublime abiding. As I meditate, I first want to develop a context of equanimity and loving kindness

2
00:00:07,280 --> 00:00:13,760
with respect to my thoughts and emotions as they arise. We live in a culture of attack or be

3
00:00:13,760 --> 00:00:20,640
attacked. Can you do a short guided meditation on calm abiding please? Calm abiding or

4
00:00:20,640 --> 00:00:26,080
meditations on altruistic emotions like friendliness can be very useful as companions to

5
00:00:26,080 --> 00:00:31,760
with pastana meditation. I would not recommend to practice developing loving kindness or equanimity

6
00:00:31,760 --> 00:00:37,600
alone because that can lead to avoiding the emotions they are meant to counter. When you are angry

7
00:00:37,600 --> 00:00:42,880
and pretend that you love everyone it is really not a way of gaining since your friendliness

8
00:00:42,880 --> 00:00:48,240
or appreciation of the suffering and other beings are going through. The best way to develop

9
00:00:48,240 --> 00:00:54,480
friendliness, compassion, appreciation and equanimity is together with the pastana meditation.

10
00:00:54,480 --> 00:00:59,520
I would caution against the view that you need to develop them before you can adequately develop

11
00:00:59,520 --> 00:01:04,560
insight meditation. We should examine the emotions that are causing you to think that way.

12
00:01:05,120 --> 00:01:10,640
Maybe you have states of great anger or hatred in your mind that you are trying to shy away from

13
00:01:10,640 --> 00:01:16,320
or maybe they are states of attachment to the peace and the happiness wanting to feel love and

14
00:01:16,320 --> 00:01:23,200
equanimity. In the end, you have to put aside such partiality and try to face your negative

15
00:01:23,200 --> 00:01:30,160
and positive emotions with equal objectivity. When you feel angry, you have to learn about anger.

16
00:01:30,160 --> 00:01:36,160
The best way to do away with it is to understand it. When you want to feel love or peace,

17
00:01:36,160 --> 00:01:41,360
or when you do not like the ups and downs of the mind, where the mind is liking,

18
00:01:41,360 --> 00:01:47,840
disliking, liking, disliking, and you wish you could be equanimous, you have to examine that

19
00:01:47,840 --> 00:01:54,080
wanting for equanimity and the aversion towards the turbulence in the mind. During the practice

20
00:01:54,080 --> 00:02:00,240
of a pastana meditation, you can switch to friendliness or compassion when overwhelming emotions arise

21
00:02:00,240 --> 00:02:06,080
when you feel hatred or rage towards other beings. That is an appropriate time to develop

22
00:02:06,080 --> 00:02:12,080
friendliness or compassion as a means of weakening the emotion, redirecting the mind towards

23
00:02:12,080 --> 00:02:18,880
wholesome mind states. A simple exercise in friendliness is to first focus on yourself and make

24
00:02:18,880 --> 00:02:26,400
an explicit wish that you may be happy and free from suffering. May I be happy. Next, make the same

25
00:02:26,400 --> 00:02:32,080
wish towards others starting with those who are closest to you. It can be the people who are in

26
00:02:32,080 --> 00:02:37,920
closest physical proximity to you or those who are closest to you emotionally. This is because

27
00:02:37,920 --> 00:02:43,920
either group is the easiest to send thoughts of friendliness to. Say to yourself, may they be happy,

28
00:02:43,920 --> 00:02:50,320
may they find peace, then gradually do this for beings who are further and further away from you,

29
00:02:50,320 --> 00:02:56,000
either physically or emotionally. Physically, you can start with beings in the same building,

30
00:02:56,000 --> 00:03:02,320
then those in the same city, country, and finally all beings in the entire world. You can also

31
00:03:02,320 --> 00:03:10,320
specify humans first, then do the same things for animals, angels, ghosts, etc. Emotionally,

32
00:03:10,320 --> 00:03:15,840
after those who are close to you, focus on those who you do not have either positive or negative

33
00:03:15,840 --> 00:03:21,840
feelings towards. And finally, focus on those who you do have negative feelings towards,

34
00:03:21,840 --> 00:03:27,360
or who have negative feelings towards you. If you are not able to send love to any group,

35
00:03:27,360 --> 00:03:33,680
you can return to focus on the previous group until you are stronger. Compassion is practiced in

36
00:03:33,680 --> 00:03:39,360
the same way as friendliness, except their friendliness is the wish for beings to be happy.

37
00:03:39,360 --> 00:03:44,960
Compassion is wishing for them to be free from suffering. Compassion is useful for when you feel

38
00:03:44,960 --> 00:03:51,120
sorry for someone or sad about the suffering of others. It is a way of calming your mind so that

39
00:03:51,120 --> 00:03:56,640
you are not incapacitated by your sadness, so you can be a grounded support for those who are in

40
00:03:56,640 --> 00:04:04,240
trouble. Appreciation, moodita, is appreciation for the attainment of other beings. This is often

41
00:04:04,240 --> 00:04:11,680
cultivated in ordinary life using the expression sadhu, which means good. When we tell someone that

42
00:04:11,680 --> 00:04:18,320
we are happy for them or offer congratulations, this is moodita. It is helpful in giving up jealousy

43
00:04:18,320 --> 00:04:24,720
and resentment, helping let go of comparing oneself with others, wishing for the things they have

44
00:04:24,720 --> 00:04:30,800
or not wanting them to get the things that we have. Appreciation is another important quality of

45
00:04:30,800 --> 00:04:37,840
mind that brings peace of mind and helps us to practice meditation. As for equanimity, there are two

46
00:04:37,840 --> 00:04:44,640
kinds, equanimity towards beings and equanimity towards experiences. When we pass in a meditation,

47
00:04:44,640 --> 00:04:50,480
we practice cultivating equanimity towards experiences. In the context of the other three

48
00:04:50,480 --> 00:04:56,960
qualities discussed here, the related practice is cultivating equanimity towards beings, which involves

49
00:04:56,960 --> 00:05:03,840
an acknowledgement of karma. When you see being suffering or benefiting and you remind yourself,

50
00:05:03,840 --> 00:05:09,840
they are going according to their karma. This is cultivating equanimity. It is a useful practice to

51
00:05:09,840 --> 00:05:15,120
do away with anger or sadness in the face of perceived injustice related to politics or

52
00:05:15,120 --> 00:05:21,360
economics. When you see the disparity between the very rich and very poor and you feel angry,

53
00:05:21,360 --> 00:05:28,000
reminding yourself, all beings go according to their karma can help you keep a level head so you

54
00:05:28,000 --> 00:05:34,720
are not consumed by negative emotions. When we see beings in great states of loss, suffering,

55
00:05:34,720 --> 00:05:40,800
or in great states of power, influence and affluence, we can remind ourselves that if these people

56
00:05:40,800 --> 00:05:46,240
in the state of great affluence continue to repress those who are impoverished and they themselves

57
00:05:46,240 --> 00:05:52,000
will be doomed to attain the same state of poverty in the future. It is a cycle of cause and effect

58
00:05:52,000 --> 00:05:58,080
wherein inevitably our destiny reflects our actions. When you think like this, you do away with

59
00:05:58,080 --> 00:06:04,240
any sense of unfairness. You are able to understand that life is ultimately fair and there is no

60
00:06:04,240 --> 00:06:09,680
need to get angry at those who oppress others as they are going to have to suffer the same fate

61
00:06:09,680 --> 00:06:14,880
in the future. And the reason why oppressed beings suffer in the present is because they have done

62
00:06:14,880 --> 00:06:20,880
it to others in the past. Reflecting in this way brings peace of mind and so it too is a support for

63
00:06:20,880 --> 00:06:27,200
the past and a meditation. Practicing the past and a meditation is likewise the best support for

64
00:06:27,200 --> 00:06:33,280
developing all four states of mind. In the past and a meditation, one becomes equanimous towards

65
00:06:33,280 --> 00:06:40,080
all things as perceptions of beings become secondary to the primary perspective of reality as

66
00:06:40,080 --> 00:06:46,160
moments of experience arising and seizing. As a result, you understand the experiences of other

67
00:06:46,160 --> 00:06:51,600
beings. When they are suffering, you can appreciate what they are going through. Even when they are

68
00:06:51,600 --> 00:06:58,080
angry or upset or enraged in immoral acts, you can understand them and you can feel pity for them

69
00:06:58,080 --> 00:07:03,520
that they are stuck in a cycle of karma, that they cannot break free from. And rather than blame

70
00:07:03,520 --> 00:07:08,800
the individual, you understand the cause to be the development of habitual tendencies through

71
00:07:08,800 --> 00:07:14,480
ignorance, through simply not knowing what they are doing. When beings are suffering, you can

72
00:07:14,480 --> 00:07:20,080
empathize with them. When beings are prosperous, you do not feel jealous. You appreciate the nature

73
00:07:20,080 --> 00:07:26,000
of good qualities because you know that they lead to happiness. You are happy and content yourself,

74
00:07:26,000 --> 00:07:31,040
so you do not feel the need to compare yourself to others and ultimately see other beings with

75
00:07:31,040 --> 00:07:38,480
equanimity, not preferring to be with this person or that person. You accept reality for what it is

76
00:07:38,480 --> 00:07:44,880
and you realize that partiality is not of any use or benefit as it does not lead to real happiness.

77
00:07:46,400 --> 00:07:52,560
If you want to develop a meditation based on loving kindness or equanimity, you should formulate

78
00:07:52,560 --> 00:07:59,360
as explained and cultivate the associated intonations according to your own situation. As mentioned,

79
00:07:59,360 --> 00:08:04,480
it can be based on those who are in physical or emotional proximity, but it is something that

80
00:08:04,480 --> 00:08:10,000
should come naturally and something that you should develop in tandem with the Bipassana meditation.

81
00:08:10,000 --> 00:08:15,280
I would not really recommend spending too much of your time in terms of developing intensive

82
00:08:15,280 --> 00:08:21,200
meditation in this area, simply because it is time consuming and you would be much better served

83
00:08:21,200 --> 00:08:27,440
by training in Bipassana meditation and when an emotion becomes overwhelming, use these practices

84
00:08:27,440 --> 00:08:57,280
as a support for your main meditation practice.

