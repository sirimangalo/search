1
00:00:00,000 --> 00:00:05,000
Hi, welcome back to our series Buddhism 101.

2
00:00:05,000 --> 00:00:11,000
Today we will be discussing the concept of morality in Buddhist context.

3
00:00:11,000 --> 00:00:18,000
Morality in Buddhism, as with generosity, as with all of the different concepts we'll be studying,

4
00:00:18,000 --> 00:00:24,000
has always a lot more to do with the effect that it has on one's own person than on other people.

5
00:00:24,000 --> 00:00:33,000
So we're not concerned so much with the result of our actions on other people, as we are concerned with the result of our actions on our own mind.

6
00:00:33,000 --> 00:00:40,000
We're also not concerned in morality in regards to morality about judgment from others.

7
00:00:40,000 --> 00:00:51,000
So much as we're concerned with, again, the effect that the judgment of ourselves and the effect that, or the reaction that we have to our actions.

8
00:00:51,000 --> 00:00:59,000
So in this sense, morality is concerned with the effect that unwholesome deeds have.

9
00:00:59,000 --> 00:01:02,000
The negative effect that unwholesome deeds will have on your mind.

10
00:01:02,000 --> 00:01:07,000
When you engage in unwholesome speech or unwholesome action or even unwholesome thought,

11
00:01:07,000 --> 00:01:14,000
it's something that affects you and takes you away from a centered objective, clear state of mind,

12
00:01:14,000 --> 00:01:17,000
which is what we're trying to create.

13
00:01:17,000 --> 00:01:29,000
And the third thing is that morality is not specifically concerned with rules or principles.

14
00:01:29,000 --> 00:01:35,000
So the first aspect of morality that I'll be talking about today is the rules,

15
00:01:35,000 --> 00:01:41,000
but morality, as a concept in Buddhism or the idea of morality in Buddhism,

16
00:01:41,000 --> 00:01:49,000
as much more to do with the general state of mind that gives rise to concentration.

17
00:01:49,000 --> 00:01:54,000
Morality is considered to be that which focuses the mind,

18
00:01:54,000 --> 00:02:00,000
that which leads the mind to become focused and able to see things clearly as they are.

19
00:02:00,000 --> 00:02:10,000
But today I'll be talking about specifically that aspect of morality that is the rules or the precepts that we take on to refrain from

20
00:02:10,000 --> 00:02:12,000
as Buddhists.

21
00:02:12,000 --> 00:02:16,000
And these can be considered kind of like fence posts.

22
00:02:16,000 --> 00:02:21,000
So you want to set up a clear boundary for yourself.

23
00:02:21,000 --> 00:02:25,000
It's kind of like a fence where outside of that you don't trespass.

24
00:02:25,000 --> 00:02:30,000
And the first thing you do when you set up a fence is you put up fence posts.

25
00:02:30,000 --> 00:02:34,000
Now the fence posts themselves won't keep anything in or out,

26
00:02:34,000 --> 00:02:44,000
but they set a marker. So these are clear indicators or markers of where the boundary exists.

27
00:02:44,000 --> 00:02:48,000
So for example, the basic Buddhist morality of the five precepts,

28
00:02:48,000 --> 00:02:55,000
not to kill, not to steal, not to lot cheat, not to lie, and not to take drugs and alcohol.

29
00:02:55,000 --> 00:03:02,000
These of course are not a comprehensive or an exhaustive list of things that one shouldn't do.

30
00:03:02,000 --> 00:03:07,000
You could never come up with such a list because again morality isn't the actions themselves,

31
00:03:07,000 --> 00:03:09,000
it's the intentions behind them.

32
00:03:09,000 --> 00:03:19,000
But they do make up a list of guidelines that give you some idea of where the boundary exists for

33
00:03:19,000 --> 00:03:24,000
between moral and immoral or between good for you and bad for you.

34
00:03:24,000 --> 00:03:28,000
So the understanding is that things like killing are bad for you.

35
00:03:28,000 --> 00:03:36,000
Killing is something that's very unwholesome and detrimental to one's own spiritual well-being.

36
00:03:36,000 --> 00:03:40,000
Stealing as well is in the same category.

37
00:03:40,000 --> 00:03:48,000
Cheating in the sense of cheating on a loved one or breaking up someone else's marriage or relationship.

38
00:03:48,000 --> 00:03:55,000
Lying, deceiving others, and using intoxicants which cloud the mind,

39
00:03:55,000 --> 00:04:01,000
all have the effect of creating states of mind that are antithetical to the meditation,

40
00:04:01,000 --> 00:04:05,000
practice, to clarity of mind and depurity and to freedom from suffering.

41
00:04:05,000 --> 00:04:14,000
They cloud the mind, they create states of lust, states of hatred and states of fear,

42
00:04:14,000 --> 00:04:17,000
states of worry and remorse and guilt and so on.

43
00:04:17,000 --> 00:04:20,000
So they're generally considered to be bad.

44
00:04:20,000 --> 00:04:26,000
As I said, this isn't exhaustive, so hurting other people is also unwholesome, but it's not in this list.

45
00:04:26,000 --> 00:04:35,000
These are, in one sense, a guide for the extreme, the most extreme immoral states.

46
00:04:35,000 --> 00:04:43,000
So anything beyond this or these are the most extreme that really have to be done away with even before you start to practice.

47
00:04:43,000 --> 00:04:48,000
So again, we're dealing with concepts here that are preliminary.

48
00:04:48,000 --> 00:04:56,000
Before you begin to practice, you actually do a new morality and at least to the extent of this first aspect of morality of keeping these precepts.

49
00:04:56,000 --> 00:05:07,000
Anyone who's going to begin to cultivate wholesome has to be assured of, has to assure themselves that they can keep these five precepts.

50
00:05:07,000 --> 00:05:17,000
If they're constantly engaging in breaking these precepts, they will be unable ever to cultivate concentration and wisdom.

51
00:05:17,000 --> 00:05:20,000
So that's the first step.

52
00:05:20,000 --> 00:05:26,000
Now, the next set, once you begin to practice meditation, you want to go a little bit further.

53
00:05:26,000 --> 00:05:33,000
So you bring the fence in closer, the boundary in closer, and you would take eight precepts.

54
00:05:33,000 --> 00:05:43,000
So a person who begins to actually actively practice meditation would it be expected to keep more than just the five precepts.

55
00:05:43,000 --> 00:05:47,000
So the third precept is not to cheat on others.

56
00:05:47,000 --> 00:05:56,000
Well, this changes, and instead of not just cheating on others, we don't engage in sexual or romantic activity at all during the meditation practice.

57
00:05:56,000 --> 00:06:00,000
Because again, this is something that distracts you from the meditation practice.

58
00:06:00,000 --> 00:06:10,000
Now we're not talking about things that in a worldly sense would be considered immoral, but we're talking about things that very much can and will get in the way of your meditation.

59
00:06:10,000 --> 00:06:17,000
It's distracting you, creating greater addiction and clinging and so on.

60
00:06:17,000 --> 00:06:22,000
And then we add three more precepts on top of that that we undertake to keep.

61
00:06:22,000 --> 00:06:27,000
So the sixth one is we undertake to only eat once a day or during the morning hours.

62
00:06:27,000 --> 00:06:38,000
We have enough food during the morning to keep us alive without obsessing over food or comforting ourselves with food.

63
00:06:38,000 --> 00:06:46,000
Simply having enough just to stay alive just to stay healthy enough to practice meditation.

64
00:06:46,000 --> 00:06:53,000
The seventh one we take is to discard any entertainment or beautification.

65
00:06:53,000 --> 00:07:07,000
So we don't engage in it, we don't listen to music, we don't sing, we don't dance, we don't watch shows or read fiction or serve the internet.

66
00:07:07,000 --> 00:07:13,000
You know, all sorts of things that during a meditation course will distract you from your meditation practice.

67
00:07:13,000 --> 00:07:20,000
Even though in a worldly sense they're not strictly speaking immoral.

68
00:07:20,000 --> 00:07:32,000
And we don't beautify ourselves, we give up using makeup and beautiful clothes and trying to make ourselves somehow attractive, which of course distracts ourselves and distracts other people.

69
00:07:32,000 --> 00:07:40,000
And keeps us tied to this and the world of romance and attraction and so on.

70
00:07:40,000 --> 00:07:48,000
And the eighth one is we undertake to sleep on a simple bedding, maybe sleeping on the floor or on a rug or something like that.

71
00:07:48,000 --> 00:07:55,000
And sleeping in a minimum so we recommend meditators to only sleep six hours or less during meditation course.

72
00:07:55,000 --> 00:08:06,000
So here we are bringing the markers in closer, we're still not talking about strictly speaking morality, which comes from the meditation practice itself.

73
00:08:06,000 --> 00:08:20,000
But by doing away with all these things, keeping away from all of these activities, we support our meditation and are able to gain greater states of concentration and thereby wisdom.

74
00:08:20,000 --> 00:08:33,000
Finally, if you want to take up the life of a meditator, if you want to take up the Buddhist life, you would go one step further and give up all economy or all commerce.

75
00:08:33,000 --> 00:08:51,000
So you would refrain from using money, refrain from buying and selling, refrain from trading, even any sort of economic activity. So however, and you would live the life of a religious person, a monk.

76
00:08:51,000 --> 00:09:06,000
So you would use an almost ball and receive the food that people give you and so on. And so it starts by adding one more precept of not using money, which is the precepts of a novice monk.

77
00:09:06,000 --> 00:09:31,000
And eventually once you're trained and that you would begin to take on this whole list of rules which serve to focus the mind or the meditators mind on the practice and keep on away from worldly thing, worldly endeavors and pursuits and distractions.

78
00:09:31,000 --> 00:09:46,000
And so there are 227 rules of the bikas, 311 of the bikinis and then you have hundreds of more, it becomes an endless list of rules where you live a prescribed life that keeps you on the straight and narrow.

79
00:09:46,000 --> 00:10:01,000
And so all these rules, breaking them is not a sign that you're going to go to hell or that you're going to be unable to meditate. You can always decide to take these again. They're guidelines or fence posts.

80
00:10:01,000 --> 00:10:18,000
String outside of them is going to be to your detriment, staying inside of them is going to be to your benefit and support for the meditation practice. So this is the first aspect of morality that most people think of when they think of morality is the idea of rules.

81
00:10:18,000 --> 00:10:32,000
So there are lots of rules that you can follow, but again they're only the one aspect of morality important, useful. And so a good introduction to the concept of Buddhist morality beyond this.

82
00:10:32,000 --> 00:10:50,000
There aren't any other rules that we have to keep you don't have to bow down in this way or chant in that way or light incense or this or that. We have very simple rules that are based on morality and based on how they affect your mind during your meditation practice.

83
00:10:50,000 --> 00:11:00,000
So that's the first aspect of morality, another part of our series on Buddhism 101. Tomorrow we'll be talking about the second aspect of morality. Thank you.

