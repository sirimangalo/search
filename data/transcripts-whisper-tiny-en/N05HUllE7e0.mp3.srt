1
00:00:00,000 --> 00:00:02,000
At least for me,

2
00:00:02,000 --> 00:00:04,000
vulnerable,

3
00:00:04,000 --> 00:00:05,000
get it on a vehicle,

4
00:00:05,000 --> 00:00:08,000
can I make real progress in

5
00:00:08,000 --> 00:00:11,000
the replacement practice

6
00:00:11,000 --> 00:00:12,000
in regards to the

7
00:00:12,000 --> 00:00:14,000
replacement of nonea

8
00:00:14,000 --> 00:00:16,000
without being able to establish

9
00:00:16,000 --> 00:00:17,000
Canika

10
00:00:17,000 --> 00:00:19,000
Samadhi

11
00:00:19,000 --> 00:00:20,000
if not,

12
00:00:20,000 --> 00:00:21,000
should I do

13
00:00:21,000 --> 00:00:23,000
Samatha first?

14
00:00:26,000 --> 00:00:28,000
Without being able to establish,

15
00:00:28,000 --> 00:00:29,000
how can you not be able to establish

16
00:00:29,000 --> 00:00:30,000
to establish Canika Samadhi?

17
00:00:30,000 --> 00:00:32,000
I mean, if you killed your parents,

18
00:00:32,000 --> 00:00:33,000
probably you couldn't,

19
00:00:33,000 --> 00:00:35,000
but otherwise,

20
00:00:35,000 --> 00:00:36,000
Canika Samadhi, you have to understand

21
00:00:36,000 --> 00:00:37,000
it's a moment.

22
00:00:37,000 --> 00:00:39,000
It's a single moment.

23
00:00:39,000 --> 00:00:41,000
It's actually quite easy to

24
00:00:41,000 --> 00:00:44,000
to establish

25
00:00:44,000 --> 00:00:46,000
because it's not established.

26
00:00:46,000 --> 00:00:47,000
It's a moment.

27
00:00:47,000 --> 00:00:48,000
Canika Samadhi is seeing

28
00:00:48,000 --> 00:00:50,000
impermanence ofering a non-cell.

29
00:00:50,000 --> 00:00:52,000
It's the moment where you see something arise

30
00:00:52,000 --> 00:00:53,000
or you see something

31
00:00:53,000 --> 00:00:55,000
sees where you both see something arise

32
00:00:55,000 --> 00:00:58,000
and sees where you see reality occurring.

33
00:00:58,000 --> 00:01:01,000
And more than just see it,

34
00:01:01,000 --> 00:01:03,000
so you are clearly aware

35
00:01:03,000 --> 00:01:05,000
of it as being that reality.

36
00:01:05,000 --> 00:01:06,000
So for example,

37
00:01:06,000 --> 00:01:07,000
when you say pain,

38
00:01:07,000 --> 00:01:08,000
pain,

39
00:01:08,000 --> 00:01:10,000
you're in your mind

40
00:01:10,000 --> 00:01:12,000
clear that that is pain,

41
00:01:12,000 --> 00:01:13,000
nothing more,

42
00:01:13,000 --> 00:01:15,000
nothing less.

43
00:01:15,000 --> 00:01:17,000
So I don't quite get

44
00:01:17,000 --> 00:01:20,000
the problem you're having.

45
00:01:20,000 --> 00:01:21,000
If you want to do

46
00:01:21,000 --> 00:01:22,000
Samatha first,

47
00:01:22,000 --> 00:01:24,000
go for it.

48
00:01:24,000 --> 00:01:26,000
It gives you a lot of power of mind

49
00:01:26,000 --> 00:01:27,000
and can be quite useful

50
00:01:27,000 --> 00:01:29,000
in developing later

51
00:01:29,000 --> 00:01:30,000
Canika Samadhi,

52
00:01:30,000 --> 00:01:31,000
or Vipassana Samadhi,

53
00:01:31,000 --> 00:01:32,000
where you see impermanence

54
00:01:32,000 --> 00:01:35,000
suffering a non-cell.

55
00:01:35,000 --> 00:01:38,000
So don't be too discouraged

56
00:01:38,000 --> 00:01:40,000
about not getting...

57
00:01:40,000 --> 00:01:42,000
One thing is,

58
00:01:42,000 --> 00:01:45,000
this is a very prominent

59
00:01:45,000 --> 00:01:46,000
theme that runs

60
00:01:46,000 --> 00:01:47,000
through all meditators,

61
00:01:47,000 --> 00:01:48,000
all new meditators,

62
00:01:48,000 --> 00:01:49,000
courses,

63
00:01:49,000 --> 00:01:51,000
and a lot of the questions

64
00:01:51,000 --> 00:01:54,000
that I get is

65
00:01:54,000 --> 00:01:55,000
this idea that your practice

66
00:01:55,000 --> 00:01:57,000
is futile.

67
00:01:57,000 --> 00:01:58,000
Your practice is not bearing

68
00:01:58,000 --> 00:01:59,000
fruit.

69
00:01:59,000 --> 00:02:00,000
And so even today

70
00:02:00,000 --> 00:02:01,000
one meditator

71
00:02:01,000 --> 00:02:02,000
who's been here quite a while

72
00:02:02,000 --> 00:02:04,000
is still wondering

73
00:02:04,000 --> 00:02:05,000
when is she...

74
00:02:05,000 --> 00:02:07,000
Or she was wondering

75
00:02:07,000 --> 00:02:08,000
whether...

76
00:02:08,000 --> 00:02:09,000
She didn't say it,

77
00:02:09,000 --> 00:02:10,000
but what she said was

78
00:02:10,000 --> 00:02:11,000
that she's wondering

79
00:02:11,000 --> 00:02:13,000
whether she should spend some time

80
00:02:13,000 --> 00:02:14,000
thinking about impermanence

81
00:02:14,000 --> 00:02:15,000
suffering and non-self.

82
00:02:15,000 --> 00:02:17,000
And she's been here for quite a while

83
00:02:17,000 --> 00:02:19,000
for at least two weeks,

84
00:02:19,000 --> 00:02:21,000
I think.

85
00:02:21,000 --> 00:02:24,000
And I think her practice is

86
00:02:24,000 --> 00:02:25,000
going well.

87
00:02:25,000 --> 00:02:27,000
It looks very clear

88
00:02:27,000 --> 00:02:28,000
as though her practice is going

89
00:02:28,000 --> 00:02:29,000
quite well,

90
00:02:29,000 --> 00:02:30,000
but we started talking

91
00:02:30,000 --> 00:02:31,000
about impermanence

92
00:02:31,000 --> 00:02:32,000
suffering a non-self

93
00:02:32,000 --> 00:02:33,000
and like most people

94
00:02:33,000 --> 00:02:35,000
still didn't get the fact

95
00:02:35,000 --> 00:02:36,000
that she was actually

96
00:02:36,000 --> 00:02:38,000
experiencing impermanence

97
00:02:38,000 --> 00:02:40,000
suffering a non-self

98
00:02:40,000 --> 00:02:43,000
quite clearly.

99
00:02:43,000 --> 00:02:45,000
And so I asked her,

100
00:02:45,000 --> 00:02:46,000
I said,

101
00:02:46,000 --> 00:02:47,000
well,

102
00:02:47,000 --> 00:02:48,000
let's take a very simple object

103
00:02:48,000 --> 00:02:49,000
is the rising and falling

104
00:02:49,000 --> 00:02:50,000
of the stomach.

105
00:02:50,000 --> 00:02:51,000
That's the basic object.

106
00:02:51,000 --> 00:02:52,000
So he's the rising

107
00:02:52,000 --> 00:02:53,000
and the falling

108
00:02:53,000 --> 00:02:54,000
of the stomach,

109
00:02:54,000 --> 00:02:55,000
always constant.

110
00:02:55,000 --> 00:02:56,000
And she said,

111
00:02:56,000 --> 00:02:57,000
no, of course it's not.

112
00:02:57,000 --> 00:02:58,000
Is the rising

113
00:02:58,000 --> 00:02:59,000
and falling

114
00:02:59,000 --> 00:03:00,000
of the stomach

115
00:03:00,000 --> 00:03:02,000
pleasant.

116
00:03:02,000 --> 00:03:03,000
And she said,

117
00:03:03,000 --> 00:03:04,000
no, no,

118
00:03:04,000 --> 00:03:05,000
actually it's quite suffering

119
00:03:05,000 --> 00:03:07,000
a lot of the time.

120
00:03:07,000 --> 00:03:08,000
And can you control it?

121
00:03:08,000 --> 00:03:09,000
Can you keep it stable?

122
00:03:09,000 --> 00:03:10,000
Can you change it?

123
00:03:10,000 --> 00:03:11,000
When it goes out of control,

124
00:03:11,000 --> 00:03:12,000
can you bring it back

125
00:03:12,000 --> 00:03:13,000
into control?

126
00:03:13,000 --> 00:03:14,000
And she said,

127
00:03:14,000 --> 00:03:15,000
no, actually,

128
00:03:15,000 --> 00:03:16,000
I can't.

129
00:03:16,000 --> 00:03:17,000
This is really clear

130
00:03:17,000 --> 00:03:18,000
because it's something

131
00:03:18,000 --> 00:03:19,000
that we tell meditators

132
00:03:19,000 --> 00:03:21,000
to watch as they're

133
00:03:21,000 --> 00:03:22,000
basic objects.

134
00:03:22,000 --> 00:03:23,000
And she said,

135
00:03:23,000 --> 00:03:25,000
set of questions to ask.

136
00:03:25,000 --> 00:03:26,000
And right away,

137
00:03:26,000 --> 00:03:27,000
she understood the point

138
00:03:27,000 --> 00:03:28,000
that she was seeing quite

139
00:03:28,000 --> 00:03:29,000
clearly impermanent

140
00:03:29,000 --> 00:03:30,000
suffering and

141
00:03:30,000 --> 00:03:31,000
on self.

142
00:03:31,000 --> 00:03:32,000
And this idea

143
00:03:32,000 --> 00:03:35,000
that you have to somehow

144
00:03:35,000 --> 00:03:36,000
consider them

145
00:03:36,000 --> 00:03:37,000
or look

146
00:03:37,000 --> 00:03:38,000
for them

147
00:03:38,000 --> 00:03:40,000
or ponder them

148
00:03:40,000 --> 00:03:41,000
or apply them

149
00:03:41,000 --> 00:03:42,000
to your practice

150
00:03:42,000 --> 00:03:43,000
in some way is totally

151
00:03:43,000 --> 00:03:45,000
erroneous, totally false.

152
00:03:45,000 --> 00:03:47,000
If you come

153
00:03:47,000 --> 00:03:48,000
to me and say

154
00:03:48,000 --> 00:03:49,000
that your practice

155
00:03:49,000 --> 00:03:50,000
is not going well

156
00:03:50,000 --> 00:03:51,000
and you can't get the hang

157
00:03:51,000 --> 00:03:51,000
of it and so on,

158
00:03:51,000 --> 00:03:53,000
then it's a very good

159
00:03:53,000 --> 00:03:54,000
sign to me

160
00:03:54,000 --> 00:03:55,000
that you're practicing

161
00:03:55,000 --> 00:03:55,000
quite well.

162
00:03:55,000 --> 00:03:56,000
You just don't realize it.

163
00:03:56,000 --> 00:03:57,000
If you come

164
00:03:57,000 --> 00:03:58,000
to me and tell me,

165
00:03:58,000 --> 00:03:59,000
oh, meditation is so wonderful

166
00:03:59,000 --> 00:04:00,000
and I'm just feeling

167
00:04:00,000 --> 00:04:01,000
peace and calm all the time

168
00:04:01,000 --> 00:04:02,000
and I really have

169
00:04:02,000 --> 00:04:03,000
no problems and so on.

170
00:04:03,000 --> 00:04:04,000
Then I think

171
00:04:04,000 --> 00:04:05,000
you're practicing

172
00:04:05,000 --> 00:04:05,000
some at that.

173
00:04:05,000 --> 00:04:06,000
And I say,

174
00:04:06,000 --> 00:04:07,000
you know,

175
00:04:07,000 --> 00:04:08,000
it's not a problem,

176
00:04:08,000 --> 00:04:09,000
but it could be,

177
00:04:09,000 --> 00:04:10,000
in some cases,

178
00:04:10,000 --> 00:04:11,000
could be a problem

179
00:04:11,000 --> 00:04:12,000
where you get stuck

180
00:04:12,000 --> 00:04:13,000
on something

181
00:04:13,000 --> 00:04:14,000
and fix,

182
00:04:14,000 --> 00:04:15,000
fixated on something

183
00:04:15,000 --> 00:04:16,000
and go flying

184
00:04:16,000 --> 00:04:20,000
off into conceptual

185
00:04:20,000 --> 00:04:23,000
illusion into illusion

186
00:04:23,000 --> 00:04:24,000
and can actually go crazy

187
00:04:24,000 --> 00:04:26,000
because you cling

188
00:04:26,000 --> 00:04:27,000
to things and follow after things

189
00:04:27,000 --> 00:04:28,000
rather than seeing them

190
00:04:28,000 --> 00:04:29,000
as they are.

191
00:04:29,000 --> 00:04:31,000
So we actually had a long talk

192
00:04:31,000 --> 00:04:32,000
about that as well

193
00:04:32,000 --> 00:04:34,000
where I was

194
00:04:34,000 --> 00:04:36,000
explaining to her

195
00:04:36,000 --> 00:04:37,000
that she was asking

196
00:04:37,000 --> 00:04:38,000
about images

197
00:04:38,000 --> 00:04:39,000
that she saw

198
00:04:39,000 --> 00:04:41,000
and explaining that

199
00:04:41,000 --> 00:04:43,000
the problem

200
00:04:43,000 --> 00:04:44,000
with the images

201
00:04:44,000 --> 00:04:46,000
just seeing really

202
00:04:46,000 --> 00:04:47,000
that's how we look at it

203
00:04:47,000 --> 00:04:48,000
but if you ask,

204
00:04:48,000 --> 00:04:49,000
she was asking,

205
00:04:49,000 --> 00:04:50,000
I said,

206
00:04:50,000 --> 00:04:51,000
well,

207
00:04:51,000 --> 00:04:52,000
you know,

208
00:04:52,000 --> 00:04:53,000
in the end,

209
00:04:53,000 --> 00:04:54,000
we see it just as seeing

210
00:04:54,000 --> 00:04:55,000
but the point being

211
00:04:55,000 --> 00:04:56,000
that whatever it is,

212
00:04:56,000 --> 00:04:57,000
there's no answer

213
00:04:57,000 --> 00:04:59,000
that you can't find

214
00:04:59,000 --> 00:05:00,000
an end answer

215
00:05:00,000 --> 00:05:01,000
to what is it.

216
00:05:01,000 --> 00:05:03,000
When you see something special,

217
00:05:03,000 --> 00:05:05,000
when you investigate it,

218
00:05:05,000 --> 00:05:06,000
you change it.

219
00:05:06,000 --> 00:05:07,000
This heisenberg's

220
00:05:07,000 --> 00:05:08,000
uncertainty principle

221
00:05:08,000 --> 00:05:09,000
actually works

222
00:05:09,000 --> 00:05:10,000
in reality.

223
00:05:10,000 --> 00:05:11,000
So when you look

224
00:05:11,000 --> 00:05:13,000
at the object,

225
00:05:13,000 --> 00:05:14,000
you change it

226
00:05:14,000 --> 00:05:15,000
and so what happens

227
00:05:15,000 --> 00:05:17,000
is you investigate

228
00:05:17,000 --> 00:05:18,000
and it changes

229
00:05:18,000 --> 00:05:19,000
like proliferates.

230
00:05:19,000 --> 00:05:20,000
It becomes something

231
00:05:20,000 --> 00:05:21,000
stronger

232
00:05:21,000 --> 00:05:22,000
because you're putting energy into it

233
00:05:22,000 --> 00:05:24,000
and because it's conceptual

234
00:05:24,000 --> 00:05:25,000
it can just change

235
00:05:25,000 --> 00:05:26,000
and alter

236
00:05:26,000 --> 00:05:27,000
and eventually

237
00:05:27,000 --> 00:05:28,000
you develop habits

238
00:05:28,000 --> 00:05:29,000
based on it

239
00:05:29,000 --> 00:05:30,000
and this is how

240
00:05:30,000 --> 00:05:31,000
meditators actually go crazy

241
00:05:31,000 --> 00:05:32,000
because they

242
00:05:32,000 --> 00:05:34,000
get totally lost

243
00:05:34,000 --> 00:05:35,000
and caught up

244
00:05:35,000 --> 00:05:37,000
in something that is conceptual.

245
00:05:37,000 --> 00:05:38,000
The point being

246
00:05:38,000 --> 00:05:39,000
that if you feel

247
00:05:39,000 --> 00:05:40,000
like your practice

248
00:05:40,000 --> 00:05:41,000
is stuck

249
00:05:41,000 --> 00:05:43,000
or not

250
00:05:43,000 --> 00:05:44,000
going as it should be,

251
00:05:44,000 --> 00:05:45,000
then it's

252
00:05:45,000 --> 00:05:46,000
there's a very good chance

253
00:05:46,000 --> 00:05:47,000
that you're actually

254
00:05:47,000 --> 00:05:48,000
impermanence

255
00:05:48,000 --> 00:05:49,000
suffering in oneself

256
00:05:49,000 --> 00:05:50,000
and should just be patient

257
00:05:50,000 --> 00:05:51,000
and you'll find yourself

258
00:05:51,000 --> 00:05:52,000
letting go as a result.

259
00:05:52,000 --> 00:05:53,000
When things don't

260
00:05:53,000 --> 00:05:54,000
go the way you want,

261
00:05:54,000 --> 00:05:55,000
you will generally

262
00:05:55,000 --> 00:05:57,000
tend to let go

263
00:05:57,000 --> 00:05:58,000
of them.

264
00:05:58,000 --> 00:05:59,000
You'll stop

265
00:05:59,000 --> 00:06:00,000
fussing about them,

266
00:06:00,000 --> 00:06:01,000
stop worrying about them.

267
00:06:01,000 --> 00:06:02,000
You'll start to learn

268
00:06:02,000 --> 00:06:03,000
that that's the nature

269
00:06:03,000 --> 00:06:04,000
of reality

270
00:06:04,000 --> 00:06:05,000
that things do

271
00:06:05,000 --> 00:06:06,000
go against your wishes,

272
00:06:06,000 --> 00:06:08,000
that things

273
00:06:08,000 --> 00:06:09,000
in general are not

274
00:06:09,000 --> 00:06:10,000
under your control

275
00:06:10,000 --> 00:06:11,000
and that's the truth

276
00:06:11,000 --> 00:06:18,000
in oneself.

