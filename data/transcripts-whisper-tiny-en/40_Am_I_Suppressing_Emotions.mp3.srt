1
00:00:00,000 --> 00:00:03,960
am I suppressing emotions?

2
00:00:03,960 --> 00:00:05,600
Question.

3
00:00:05,600 --> 00:00:10,400
I am trying to observe my emotions, but I am somewhat out of touch with them

4
00:00:10,400 --> 00:00:12,760
and I control them a lot.

5
00:00:12,760 --> 00:00:17,920
When any motion goes away, how can I tell if it has subsided on its own

6
00:00:17,920 --> 00:00:20,760
or if I suppressed it?

7
00:00:20,760 --> 00:00:22,760
Answer.

8
00:00:22,760 --> 00:00:27,760
I would not be so analytical about it because the analysis itself is somewhat

9
00:00:27,760 --> 00:00:30,240
of a misunderstanding.

10
00:00:30,240 --> 00:00:35,000
The concern that you are suppressing, not accepting, and not learning about the

11
00:00:35,000 --> 00:00:40,600
experience is often a misunderstanding of what is actually happening.

12
00:00:40,600 --> 00:00:45,400
The emotions themselves will cause stress, and that is why there is stress in

13
00:00:45,400 --> 00:00:46,960
the mind.

14
00:00:46,960 --> 00:00:50,760
It is not because you are somehow suppressing them.

15
00:00:50,760 --> 00:00:55,360
When certain emotions arise, we often give rise to additional conflicting

16
00:00:55,360 --> 00:01:00,160
emotions, and that is where it starts to get confusing.

17
00:01:00,160 --> 00:01:05,120
The desire to suppress is stressful, and this is something you can see if your

18
00:01:05,120 --> 00:01:07,760
mind is quiet.

19
00:01:07,760 --> 00:01:13,000
Anger is stressful, and the desire to suppress anger is stressful.

20
00:01:13,000 --> 00:01:17,400
This will lead to things like tension in the body and suffering in the mind, so

21
00:01:17,400 --> 00:01:22,640
what you are probably experiencing is quite a bit of suffering.

22
00:01:22,640 --> 00:01:27,960
I do think about the suffering objectively when these thoughts arise, along

23
00:01:27,960 --> 00:01:32,120
with the worries, confusion, and uncertainty.

24
00:01:32,120 --> 00:01:35,680
Remember that part of the difficulty that you are having is the struggle to see

25
00:01:35,680 --> 00:01:40,320
impermanence, suffering, and to non-self.

26
00:01:40,320 --> 00:01:44,800
It is a struggle to stop trying to fix things, thinking that something is

27
00:01:44,800 --> 00:01:48,880
wrong with your practice is actually kind of misleading.

28
00:01:48,880 --> 00:01:52,680
There is nothing called practice, and you are not doing it.

29
00:01:52,680 --> 00:01:57,120
There is only experience, and it is coming and going.

30
00:01:57,120 --> 00:02:00,840
Once you realize that, then all of your problems disappear, like not

31
00:02:00,840 --> 00:02:04,240
becoming untied.

32
00:02:04,240 --> 00:02:07,400
Every moment is a new not.

33
00:02:07,400 --> 00:02:12,240
Every moment in your meditation is a new challenge, and the stress occurs when

34
00:02:12,240 --> 00:02:14,600
we fail to meet the challenge.

35
00:02:14,600 --> 00:02:20,800
And at any given moment, we abandon mindfulness and cling to something.

36
00:02:20,800 --> 00:02:25,960
Without mindfulness and wisdom, we do not have the flexibility to be able to move

37
00:02:25,960 --> 00:02:28,880
from one challenge to the next.

38
00:02:28,880 --> 00:02:33,520
This flexibility is one reason that enlightened people are so bright and clear in

39
00:02:33,520 --> 00:02:35,320
mind.

40
00:02:35,320 --> 00:02:43,040
For example, when focusing on pain, thinking, pain, pain, after some time your

41
00:02:43,040 --> 00:02:48,280
mind finally realizes it is just pain, then maybe some kind of pleasure comes

42
00:02:48,280 --> 00:02:53,000
up, but you are still thinking about the pain, which makes you unable to deal

43
00:02:53,000 --> 00:02:56,120
with the new experience.

44
00:02:56,120 --> 00:03:01,080
Each experience has to be dealt with as it is, with no reference to past or

45
00:03:01,080 --> 00:03:03,200
future.

46
00:03:03,200 --> 00:03:07,400
The first shift of awareness regarding the pain was to convince yourself that

47
00:03:07,400 --> 00:03:12,840
the pain, rather than being bad, is just pain.

48
00:03:12,840 --> 00:03:17,960
Then pleasure arises, and again, you have to shift your perspective to see that

49
00:03:17,960 --> 00:03:23,800
it is just a feeling of pleasure, caused by chemicals arising in the body.

50
00:03:23,800 --> 00:03:28,280
You still have to see it for what it is, but the pleasure is coming from another

51
00:03:28,280 --> 00:03:36,440
angle of being, quote unquote, a good thing, making you want to cling to it.

52
00:03:36,440 --> 00:03:42,840
You have to teach yourself to be present and objective with each experience.

53
00:03:42,840 --> 00:03:46,120
The difficulty is not in suppressing.

54
00:03:46,120 --> 00:03:51,040
The difficulty is adapting to the situation, which simply means that no matter which

55
00:03:51,040 --> 00:03:55,480
angle the mind is taking, you must straighten it out.

56
00:03:55,480 --> 00:04:00,920
If it is crooked this way, when must straighten it back that way, everything has to

57
00:04:00,920 --> 00:04:06,600
be straightened out, whatever the mind is clinging to, one must bring it back to being

58
00:04:06,600 --> 00:04:13,040
straight and centered, clearly aware of everything just as it is.

59
00:04:13,040 --> 00:04:19,640
When the feeling that you identify as suppressing comes up, acknowledge it as just a feeling,

60
00:04:19,640 --> 00:04:26,040
and you should be able to see that it is just a feeling based on defilements and emotions,

61
00:04:26,040 --> 00:04:34,480
sometimes you might say, when I acknowledge thinking, thinking, it seems to give me a headache.

62
00:04:34,480 --> 00:04:40,600
When I am thinking a lot, I don't have the headache, but as soon as I start noting, thinking,

63
00:04:40,600 --> 00:04:47,000
I get this big headache, and you start to think that the noting is causing you suffering,

64
00:04:47,000 --> 00:04:51,640
but if you are observant, you will realize that you are building up the headache without

65
00:04:51,640 --> 00:04:57,480
knowing it during the time you were thinking, and when you realize you were thinking,

66
00:04:57,480 --> 00:05:05,000
and you start noting, thinking, thinking, you suddenly have to deal with the headache.

67
00:05:05,000 --> 00:05:12,640
When you have lust or desire, you might focus on it by saying wanting, wanting, or pleasure,

68
00:05:12,640 --> 00:05:18,640
pleasure, and you may feel tension in the body as you do so, but the tension is there because

69
00:05:18,640 --> 00:05:25,320
of the desire itself, instead of tension, you might feel the doneness of mind that comes

70
00:05:25,320 --> 00:05:31,920
with desire, and you may think, oh, this is coming for meditation, and I am suppressing

71
00:05:31,920 --> 00:05:37,680
this in the meditation, so it is giving me this feeling of doneness, but the desire is what

72
00:05:37,680 --> 00:05:40,240
causes the doneness.

73
00:05:40,240 --> 00:05:45,720
The emotions themselves bring the physical feelings as a byproduct.

74
00:05:45,720 --> 00:05:50,400
When you start to practice, you are just beginning to see objectively.

75
00:05:50,400 --> 00:05:56,480
The moment when you had desire, you were not seeing things objectively.

76
00:05:56,480 --> 00:06:03,080
That is why the desire feels wonderful until you are mindful of it, because suddenly you

77
00:06:03,080 --> 00:06:09,640
are objective again, and you are seeing both sides, the good and the bad.

78
00:06:09,640 --> 00:06:14,560
With desire, you are running towards the good and running away from the bad, so that you

79
00:06:14,560 --> 00:06:20,440
never really experience the bad until you stop running, and then you experience it, and

80
00:06:20,440 --> 00:06:22,800
have to deal with it.

81
00:06:22,800 --> 00:06:27,760
I would advise not to worry too much, even if you are suppressing.

82
00:06:27,760 --> 00:06:34,000
The most important thing is to watch the suppression, not to stop suppressing.

83
00:06:34,000 --> 00:06:39,920
Just watch and see what is going on, because inevitably you will come to see impermanence,

84
00:06:39,920 --> 00:06:47,040
suffering, and non-self, you will see that you cannot control your experiences, you will

85
00:06:47,040 --> 00:06:53,040
see that even the suppressing is something that only happens based on causes and conditions.

86
00:06:53,040 --> 00:07:18,160
It is not worth getting caught up in, or attached to two.

