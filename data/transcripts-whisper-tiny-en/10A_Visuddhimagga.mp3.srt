1
00:00:00,000 --> 00:00:10,000
Today, we study the exposition, the detailed exposition of the first journal,

2
00:00:12,000 --> 00:00:16,000
and it begins on page 142.

3
00:00:16,000 --> 00:00:41,000
So, so far, the yogi has prepared and then practiced until he could be accessed concentration.

4
00:00:41,000 --> 00:00:52,000
And with the gaining of access concentration, he called the counterpart sign in his mind.

5
00:00:53,000 --> 00:01:00,000
That is, the mental image of the casino disk appeared in his mind.

6
00:01:01,000 --> 00:01:07,000
And he dwells upon it, again and again, or he concentrates upon that mental image.

7
00:01:07,000 --> 00:01:21,000
So, that when the time is right, there arises in him a thought process containing the Jana consciousness.

8
00:01:22,000 --> 00:01:32,000
So, while he is guiding his mind in this way, confronting the sign, that means drilling on the counterpart sign again and again.

9
00:01:32,000 --> 00:01:37,000
Then knowing now absorption will succeed.

10
00:01:37,000 --> 00:01:44,000
Now, this is a place where most translators misunderstood the hardly idiom.

11
00:01:46,000 --> 00:01:53,000
The hardly idiom here means just when absorption is about to arise.

12
00:01:53,000 --> 00:02:18,000
So, we do not need the words in square brackets, then knowing, because the subject of this sentence is the mind or adversity, and not the person.

13
00:02:18,000 --> 00:02:25,000
And also, the person does not know when the absorption will arise.

14
00:02:26,000 --> 00:02:32,000
And so, this is the hardly idiom, which means just when absorption is about to arise.

15
00:02:33,000 --> 00:02:42,000
So, literally translated, it should be when it could be said that absorption will succeed or absorption will arise.

16
00:02:42,000 --> 00:02:58,000
So, when absorption for Jana is about to arise, what happens is the mind or advancing arises, cutting of the flow of life continuum of the wanga.

17
00:02:58,000 --> 00:03:10,000
And please look at the bottom of the sheet, the Jana thought process.

18
00:03:11,000 --> 00:03:17,000
I have given the diagram of thought process as before.

19
00:03:18,000 --> 00:03:24,000
Now, if you look at the first line, the first two are BH, BH, they are bawangas.

20
00:03:24,000 --> 00:03:27,000
And then M is mind or avoiding.

21
00:03:28,000 --> 00:03:36,000
So, when the absorption is about to arise, there arises in his mind first mind or avoiding.

22
00:03:37,000 --> 00:03:42,000
And after that, there are one, two, three, four, five.

23
00:03:43,000 --> 00:03:47,000
Five movements of impulsion or Jana.

24
00:03:47,000 --> 00:03:58,000
And the first four, one, two, three, four belong to Kama Vajara, since sphere consciousness.

25
00:03:59,000 --> 00:04:02,000
And number five belongs to Rupa Vajara.

26
00:04:03,000 --> 00:04:06,000
Fine material sphere.

27
00:04:06,000 --> 00:04:22,000
So, number one, two, three and four are actually the types of consciousness that we call beautiful sense sphere consciousness.

28
00:04:25,000 --> 00:04:28,000
If you remember the chat.

29
00:04:28,000 --> 00:04:35,000
I have it upstairs. It's okay.

30
00:04:36,000 --> 00:04:41,000
So, then the first is called, you may look at the book too.

31
00:04:42,000 --> 00:04:47,000
The number one is called preliminary work.

32
00:04:48,000 --> 00:04:51,000
And the number two is called access.

33
00:04:51,000 --> 00:04:57,000
Number three is called conformity. And number four, change of lineage.

34
00:04:58,000 --> 00:05:15,000
Now, the comradery explains that all one, two, three, four can be called by each of these names, preliminary work, access, conformity,

35
00:05:15,000 --> 00:05:28,000
and all, the all four can be called by these names, preliminary work, access, and conformity.

36
00:05:29,000 --> 00:05:42,000
Or if we want to give them separate names so that one excludes the other, then number one is to be called preliminary work, number two, access,

37
00:05:42,000 --> 00:05:46,000
number three, conformity, and number four, change of lineage.

38
00:05:47,000 --> 00:05:55,000
And it is at the full moment that the comradery lineage stops.

39
00:05:56,000 --> 00:05:59,000
And then number five becomes Rupa Vajara.

40
00:06:00,000 --> 00:06:01,000
Fine material lineage.

41
00:06:02,000 --> 00:06:05,000
So, that is why it is called change of lineage.

42
00:06:05,000 --> 00:06:15,000
And the second line, second diagram shows another variety of the thought process.

43
00:06:16,000 --> 00:06:20,000
But here, you do not have preliminary work.

44
00:06:21,000 --> 00:06:31,000
So, there are only three comradery genre or impulsions, and number four is Jana, the Rupa Vajara.

45
00:06:31,000 --> 00:06:42,000
So, in this, there are only three comradery genre numbers, number one, two, and three, and one Rupa Vajara genre, which is four.

46
00:06:43,000 --> 00:06:59,000
So, either the Jana arises either at the fifth moment or at the fourth moment.

47
00:06:59,000 --> 00:07:06,000
Now, here we do not have the normal duration of seven javanas.

48
00:07:07,000 --> 00:07:13,000
Mostly javanas repeat themselves seven times, and the normal conditions.

49
00:07:14,000 --> 00:07:21,000
But in these thought process, there are only five or four javanas movements.

50
00:07:21,000 --> 00:07:32,000
What is Javanas? In person, it is the, in the thought process, there are seven normal thought process.

51
00:07:33,000 --> 00:07:36,000
There are seven movements of impulsion or we call Javanas.

52
00:07:37,000 --> 00:07:43,000
And these are the movements when the object is really experienced.

53
00:07:43,000 --> 00:07:52,000
And they are the movements where karma is acquired at a good karma or bed karma.

54
00:07:53,000 --> 00:08:02,000
So, the comradery explains all these in paragraphs, 74, 75.

55
00:08:03,000 --> 00:08:04,000
Yes.

56
00:08:04,000 --> 00:08:14,000
And the barakras, 76, and all the 77, etc.

57
00:08:15,000 --> 00:08:27,000
Another view of the elder Gautarda, who said, the sixth and the second can be sixth and seventh movements of Javanas.

58
00:08:27,000 --> 00:08:37,000
So, there can be seven javanas or there can be six javanas. And that opinion is refuted by both the Gautarda or this book.

59
00:08:40,000 --> 00:08:50,000
Because if the Javanas were to go until sixth or seventh, it would not be able to stop as Jana.

60
00:08:50,000 --> 00:09:04,000
And the analogy is given as when a person runs fast towards the precipice, he would not be able to stop there.

61
00:09:05,000 --> 00:09:10,000
Because he has gained movement and so he would not be able to stop there.

62
00:09:10,000 --> 00:09:20,000
Because in Javanas one supports the other and so they become more and more powerful.

63
00:09:21,000 --> 00:09:27,000
But at the foot of the consciousness, the flow of consciousness can stop.

64
00:09:28,000 --> 00:09:33,000
And then the Jana consciousness arises.

65
00:09:33,000 --> 00:09:57,000
So, the common opinion of teachers is that Jana arises at the fifth or at the fourth moment and not at the sixth or seventh or not before the fifth or in another case, fourth.

66
00:09:57,000 --> 00:10:08,000
So, there must be at least four kamavajara movements or three kamavajara movements preceding Jana thought movements.

67
00:10:11,000 --> 00:10:16,000
So, this is how a Jana thought process arises.

68
00:10:16,000 --> 00:10:26,000
And in this book it is called cognitive series, that is what we are saying in the third process.

69
00:10:27,000 --> 00:10:32,000
So, we call third process and the Nyanamali called cognitive series, they are the same.

70
00:10:33,000 --> 00:10:37,000
In Bali it is called V-T, V-I-T-H-I.

71
00:10:37,000 --> 00:10:48,000
Now, when describing first Jana, Buddha used something like a stereotyped expression.

72
00:10:49,000 --> 00:10:54,000
And the comradry gave detailed exposition of that text.

73
00:10:55,000 --> 00:11:02,000
And the translation of that text to make meaning more clear, I gave on the sheet.

74
00:11:02,000 --> 00:11:12,000
It is different from what you see or what you are seeing in this part of purification, but they mean the same thing.

75
00:11:13,000 --> 00:11:21,000
So, the passage runs like this quite secluded from kamavas, secluded from Akushalas.

76
00:11:22,000 --> 00:11:27,000
He enters a bone and dwells in the first Jana, which is accompanied by V-T-H-H-I and V-T-H-I.

77
00:11:27,000 --> 00:11:38,000
And which is born of seclusion or arises in secluded state and which is with V-T-H-H-H-I.

78
00:11:39,000 --> 00:11:45,000
This is the description of the first Jana.

79
00:11:46,000 --> 00:11:54,000
Now, and I put the numbers one and two so that we can understand more clearly.

80
00:11:54,000 --> 00:12:03,000
Now, the first statement states, quite secluded from kamavas, and the second secluded from Akushalas.

81
00:12:03,000 --> 00:12:23,000
Now, the comradry explains that the word kites is to be understood in two also, because in the poly usage, they do not repeat a word again and again, but let it be understood in later expressions.

82
00:12:23,000 --> 00:12:37,000
So, here also, although it is not said that kites secluded from kamavas and kites secluded from Akushalas, we must understand that the kites is meant here also.

83
00:12:38,000 --> 00:12:45,000
And the word kites here means something like not otherwise.

84
00:12:45,000 --> 00:12:54,000
It definitely is. So, your mind must be secluded from kamavas and not otherwise in order to get Jana.

85
00:12:55,000 --> 00:13:01,000
So, without seclusion from kamavas, there can be no attainment of Jana.

86
00:13:02,000 --> 00:13:07,000
And also, the same thing, without seclusion from Akushalas, there can be no attainment of Jana.

87
00:13:07,000 --> 00:13:15,000
So, the word kites should be understood in number two, and number two phrase also.

88
00:13:17,000 --> 00:13:22,000
And he enters the born and was in the first Jana, which is accompanied by Vittaka and Vijara.

89
00:13:22,000 --> 00:13:41,000
Vittaka and Vijara are the Jana factors, or the mental factors. In initial, we call them initial application and sustained application.

90
00:13:42,000 --> 00:13:49,000
So, that is why you need a sudden knowledge of Abiram, to understand.

91
00:13:49,000 --> 00:13:58,000
So, the first Jana is accompanied by Vittaka and Vijara, initial application and sustained application.

92
00:13:59,000 --> 00:14:04,000
And the first Jana is born of seclusion, where we will come to seclusion later.

93
00:14:04,000 --> 00:14:20,000
Or, the first Jana arises in secluded states, that means arises with together with secluded states. And this first Jana is with PT and Sukha. PT is translated as joy and Sukha as happiness.

94
00:14:20,000 --> 00:14:33,000
So, you know, I think you remember the difference between PT and Sukha, or it is explained, and we should be madam in the other purification.

95
00:14:34,000 --> 00:14:40,000
Now, the word kama is important here.

96
00:14:40,000 --> 00:14:51,000
If you look a little further down, the word kama can mean objects of sense desire, or sense desires themselves.

97
00:14:52,000 --> 00:14:59,000
So, whenever we find the word kama, we have to understand what it means. Sometimes it may mean just the objects of sense desire.

98
00:14:59,000 --> 00:15:16,000
Sometimes it may mean sense desire. Sometimes it may mean both. So, here, the word kama can mean E or B. The objects of sense desire, or sense desires themselves.

99
00:15:17,000 --> 00:15:24,000
Now, if I am attached to this kama, then this is the object of my sense desire.

100
00:15:24,000 --> 00:15:35,000
So, sense desire is in me, and this is the object. So, this object is called kama, and the desire arising in my mind is also called kama.

101
00:15:35,000 --> 00:16:04,000
So, kama can mean the thing which we are attached to of the consciousness, or the consciousness, the attachment which we have. So, when we take the word kama to mean objects of sense desire, then the number one phrase shows that we must be secluded from these senses.

102
00:16:05,000 --> 00:16:16,000
These objects desire a little sense object, objects which we can be attached to. That means we should be practicing not in the middle of the city, where there are many attractions.

103
00:16:17,000 --> 00:16:26,000
So, we should be in secluded place in the forest or somewhere where there are no such attractive objects.

104
00:16:26,000 --> 00:16:36,000
So, the number one phrase shows that we must be secluded body weight.

105
00:16:37,000 --> 00:16:54,000
In that case, number two phrase, quite secluded from acusala means, we must be secluded from sense desires, and all acusala. That means, acusalas including sense desire.

106
00:16:54,000 --> 00:16:59,000
Because sense desire is lower, and so it is included in acusala.

107
00:17:04,000 --> 00:17:21,000
So, this is when we take kama to mean objects of sense desire. If we take kama to mean just sense desires, then number one shows seclusion from sense desires.

108
00:17:21,000 --> 00:17:31,000
Here, one shows we must be secluded from sense desires. We must not have sense desires arise in our minds.

109
00:17:32,000 --> 00:17:43,000
And, two number two shows seclusion from sense desires and all acusala. Because when we say secluded from kama, we mean secluded from lower.

110
00:17:43,000 --> 00:17:54,000
And secluded from acusala means, all acusala including, lower. So, we should understand this too clearly in this way.

111
00:17:54,000 --> 00:18:18,000
What is it? Global? Global? Global means attachment.

112
00:18:18,000 --> 00:18:26,000
First of all, what is the commentary of what the part of purification explains in these passages?

113
00:18:26,000 --> 00:18:54,000
In order to get jana, one must be secluded from sense objects and sense desires, and also be secluded from acusala.

114
00:18:54,000 --> 00:19:02,000
Especially the hindrances, what we call hindrances. There are five hindrances mentioned later in the book.

115
00:19:03,000 --> 00:19:17,000
Because if our minds are not free from sense desire and other hindrances, then concentration cannot arise. And when there is no concentration, there can be no attainment of jana.

116
00:19:17,000 --> 00:19:32,000
So, in order to get jana, at this first, this first jana, then our minds should be away from sense desires and sense objects and also from the mental hindrances.

117
00:19:32,000 --> 00:19:51,000
The sense desire comes again here, and then anger, sloth and double, and the restlessness and remorse and doubt. So, these are the five hindrances. We have mentioned later.

118
00:19:51,000 --> 00:20:20,000
So, and in the Indian translation, what is meant by sense desires as object?

119
00:20:21,000 --> 00:20:46,000
Sense desires as objects. Our object sense desires, I think that is not actually. So, this is sense object. This is object of sense.

120
00:20:46,000 --> 00:21:08,000
So, this is not sense desire. It is object of sense desire. So, I think we should not say object sense desires as object. If you want to say sense desires object or object of sense desire. I think it should be like that.

121
00:21:08,000 --> 00:21:24,000
Sense desires object. We are going to look at sense desires, not that. Have a look at desire, not an object.

122
00:21:24,000 --> 00:21:34,000
So, when you say object of sense desire, what do you understand right there?

123
00:21:34,000 --> 00:21:54,000
Yeah, Ruba, they can be Ruba or Nama also. So, the t could be object. So, the t could be the object. The t could be the object of sense desire. Anything can be the object of sense desire.

124
00:21:54,000 --> 00:22:10,000
But sense desire is the subject, rising end of mind. And the objects are something out of our mind and even in our minds, but they could be not the same thing at the same moment.

125
00:22:11,000 --> 00:22:19,000
We are just doing our thoughts too. Then the thought is mind. That thought is the object of our sense desire.

126
00:22:19,000 --> 00:22:32,000
Our arm is off days too. If our arms are off days, if our arms are off days, if our arms are off days, if our arms are off days.

127
00:22:33,000 --> 00:22:34,000
What?

128
00:22:34,000 --> 00:22:43,000
Dormos, are you talking about everything, that class of life. Everything is Dormos, and I thought Dormos were off days.

129
00:22:43,000 --> 00:22:53,800
in our region. Everything in the world can be the object. You know, there are six senses

130
00:22:53,800 --> 00:23:02,560
in the middle, the idea and so on. So the visible object is the object of the eye or

131
00:23:02,560 --> 00:23:12,400
see consciousness, and so on. And then the other objects are the objects of mine. So instead

132
00:23:12,400 --> 00:23:19,040
of seeing objects or a sense desires of object, I think we should say object of sense

133
00:23:19,040 --> 00:23:32,960
desires, and sense desires s, defile me, that's correct. But you know, we have the word sense

134
00:23:32,960 --> 00:23:45,960
desires object in paragraph 83, the second line. And that is correct. Sense desires

135
00:23:45,960 --> 00:24:03,800
object. And the apostrophe after sense desires, apostrophe, it is not the code. So there

136
00:24:03,800 --> 00:24:19,460
is confusion here. Okay. And then about the middle of the page, we find the word sense

137
00:24:19,460 --> 00:24:38,140
desire element. Yes. Sense desire element. It actually means the realm of sense desire.

138
00:24:38,140 --> 00:24:46,700
We have come across this word in the first. Although it is the translated as element,

139
00:24:46,700 --> 00:24:58,980
what is meant is the realm of sense desire. That means karma, whatever, a brand. Sometimes

140
00:24:58,980 --> 00:25:05,660
translators want to be very literate. So the word here, the word hatu is used. And the

141
00:25:05,660 --> 00:25:16,100
word the hatu, the word hatu is normally translated as element. So they are full, full

142
00:25:16,100 --> 00:25:26,540
as the great elements like earth element, water element, fire element, and wind element.

143
00:25:26,540 --> 00:25:32,260
So they are all elements. Because the word use is in power is desire to. And here also

144
00:25:32,260 --> 00:25:41,780
the word hatu is used, but in a different sense, not in the sense that the essential parts

145
00:25:41,780 --> 00:25:50,500
of something, but here karma hatu simply means the realm of karma. That means karma

146
00:25:50,500 --> 00:26:20,460
hatu is a world. Karma would give a brand. So they'd cover us.

147
00:26:20,460 --> 00:26:34,780
In some pages here. Now let's go to page 140. What is seven, the last paragraph. So far,

148
00:26:34,780 --> 00:26:41,100
the fact is abandoned by the first journal. We need a word first here. So far the fact

149
00:26:41,100 --> 00:26:50,100
is abandoned by the first journal have been shown. And now in order to show the fact that

150
00:26:50,100 --> 00:26:57,500
associated with it, which is accompanied by applied and sustained thought, etc. is said.

151
00:26:57,500 --> 00:27:09,100
So etc. is missing there. So now we come to the family grounds, initial application

152
00:27:09,100 --> 00:27:15,940
or here the applied thought and sustained thought. That means initial application and sustained

153
00:27:15,940 --> 00:27:31,020
application or embodied with that gap and which are two mental factors. And then the

154
00:27:31,020 --> 00:27:41,100
summary explains one by one, the initial application and sustained application. And then

155
00:27:41,100 --> 00:27:46,780
the difference between initial application and sustained application because they arise at

156
00:27:46,780 --> 00:27:52,420
the same time with the same type of consciousness, but they have different functions. And

157
00:27:52,420 --> 00:28:00,940
so they are different. So the paragraph edema and those sometimes not separate. That means

158
00:28:00,940 --> 00:28:13,380
though they arise together with some types of consciousness, where the initial application

159
00:28:13,380 --> 00:28:18,660
or applied thought and sustained thought arise together with many types of consciousness,

160
00:28:18,660 --> 00:28:25,940
actually 55 kinds of consciousness. They arise together. But in the second journal there

161
00:28:25,940 --> 00:28:36,540
is no applied thought or initial application. And the same with thought, food and food.

162
00:28:36,540 --> 00:28:45,140
So those sometimes not separate means in some with some types of consciousness they arise

163
00:28:45,140 --> 00:28:53,860
together, applied thought is one thing and sustained thought is another. And then the other

164
00:28:53,860 --> 00:29:02,580
gave the analogies of the difference between Vitaka and Vija. And I think I have told you

165
00:29:02,580 --> 00:29:17,700
about this difference when we study a bit of my year. You remember that? Yeah, about

166
00:29:17,700 --> 00:29:28,660
the middle of paragraph 89. So many are not at the class. So like a bird spreading out

167
00:29:28,660 --> 00:29:34,060
its wings when about to soar into the air. And like a piece diving towards a load that's

168
00:29:34,060 --> 00:29:39,700
when it is minded to follow up to the end of it. The behavior of sustained thought is

169
00:29:39,700 --> 00:29:45,740
quiet being the near and non-interference of consciousness. Like the birds, flaming with

170
00:29:45,740 --> 00:29:50,220
otspled wings after soaring into the air. And like the piece buzzing above the load

171
00:29:50,220 --> 00:29:56,140
after it is tight towards it. So initial application is going to the thing first. And then

172
00:29:56,140 --> 00:30:04,820
sustained application is staying on it and something like that. And there are other analogies

173
00:30:04,820 --> 00:30:14,700
too. How can they arise together? It seems that they arise together and they have different

174
00:30:14,700 --> 00:30:31,700
functions. The initial application is the one that takes the mind to the object. And sustained

175
00:30:31,700 --> 00:30:48,580
application is the one that keeps the mind there. They arise together. Initial application

176
00:30:48,580 --> 00:30:59,800
is not an exact translation of the word because there is a word initial. And so we might

177
00:30:59,800 --> 00:31:07,900
think that Whitaker comes first and then we reach our followers. But Whitaker means just

178
00:31:07,900 --> 00:31:18,340
thinking or thought. And Whitaker means, and Whitaker. Going here and there that is Whitaker.

179
00:31:18,340 --> 00:31:38,640
So they are together. But they have different functions. And then there is the word. We

180
00:31:38,640 --> 00:31:44,460
just translated here as born of seclusion. And there are two meanings to this word in

181
00:31:44,460 --> 00:31:53,460
Bali. Born of seclusion or arises in secluded states. That means arises with secluded

182
00:31:53,460 --> 00:32:06,320
states. Secluded states will remain the genre consciousness and other mental factors.

183
00:32:06,320 --> 00:32:15,820
And seclusion here means secluded disappearance of hindrances. So long as there are hindrances

184
00:32:15,820 --> 00:32:21,780
there can be no seclusion or there can be no arising of genre consciousness. So born

185
00:32:21,780 --> 00:32:32,380
of seclusion means the disappearance of the absence of mental hindrances. And then it

186
00:32:32,380 --> 00:32:42,920
is with the genre is with PT and Sukha, with joy and happiness. But here PT is translated

187
00:32:42,920 --> 00:33:02,360
as happiness and Sukha as bliss. And the commentary gave five kinds of PT here.

188
00:33:02,360 --> 00:33:09,720
And this kind of PT emerges experience during the practice of meditation. And there are

189
00:33:09,720 --> 00:33:19,040
set to be five kinds of such happiness or joy or zest or whatever you may call it. And

190
00:33:19,040 --> 00:33:26,080
there are five kinds of minor happy minor PT, momentary PT, showering PT, uplifting PT

191
00:33:26,080 --> 00:33:34,000
and pervading PT. And when minor PT is only able to raise the hairs on the body, sometimes

192
00:33:34,000 --> 00:33:43,520
you have this hairs standing on end. So that may cause from just cold or it is caused

193
00:33:43,520 --> 00:33:52,120
by PT in the mind. And momentary PT is a little stronger than minor PT, like fleshes

194
00:33:52,120 --> 00:34:01,040
of lightning at different moments. Sometimes you feel that thing when you practice meditation.

195
00:34:01,040 --> 00:34:06,240
The next is showering PT, breaks over the body again and again like waves on the sea

196
00:34:06,240 --> 00:34:15,720
shore. It is like the waves it comes over the body and then subsides and then again comes

197
00:34:15,720 --> 00:34:23,880
on. So that is showering PT. And then the fourth one is uplifting PT. So when you have

198
00:34:23,880 --> 00:34:33,600
this kind of PT your body can be levitated. And the two stories are given here, in which

199
00:34:33,600 --> 00:34:42,960
two persons levitated or flew through the air, not by the power of the Jana or Subanormal

200
00:34:42,960 --> 00:34:51,800
knowledge, but by the power of this PT. So these stories are not difficult to understand.

201
00:34:51,800 --> 00:35:01,040
One is of a monk and the others of a girl who was left behind because she was pregnant

202
00:35:01,040 --> 00:35:09,640
and then she was very desirous of coin to be a part of festival and so she got the

203
00:35:09,640 --> 00:35:18,440
PT from thinking of the people going around the bakoda and then from the bokoda and so on.

204
00:35:18,440 --> 00:35:25,800
And so she called this PT and she was lifted up and transported to the bokoda festival.

205
00:35:25,800 --> 00:35:36,520
And she reached there before her parents reached the place. So that was by the power of

206
00:35:36,520 --> 00:35:45,000
uplifting PT. And the last one is pervading PT. The whole body is completely pervaded

207
00:35:45,000 --> 00:35:51,920
like a field bledder, like a rock cavern, invaded by a huge inundation or like something

208
00:35:51,920 --> 00:35:57,800
like a surgical cotton put in the water, something like that. It is so totally soaked in

209
00:35:57,800 --> 00:36:05,280
water, something like that. So your whole body is soaked in this kind of happiness.

210
00:36:05,280 --> 00:36:24,640
Now our translate is sukha is heaviness. The PT is difficult to translate. Rapture.

211
00:36:24,640 --> 00:36:37,200
And when the comradery gives the difference between PT and sukha, the next one. And PT and

212
00:36:37,200 --> 00:36:44,320
sukha also arrives at the same time with some types of consciousness. Although they

213
00:36:44,320 --> 00:36:51,960
arrive together, in this case, there is difference because PT is not feeling according

214
00:36:51,960 --> 00:37:04,600
to habirama. PT belong to formation, aggregate and happiness or sukha belongs to feeling

215
00:37:04,600 --> 00:37:10,640
aggregate. So they are different. Sukha is feeling within a, but PT is not within a, but

216
00:37:10,640 --> 00:37:23,000
it is something like preceding, preceding sukha, also the arise at the same time. And PT is

217
00:37:23,000 --> 00:37:28,960
compared to that of a man exhausted and he does that so hard about the palm on the edge

218
00:37:28,960 --> 00:37:36,160
of the wood. That is PT. And if he went to the wood shade and used the water, he would

219
00:37:36,160 --> 00:37:42,180
have placed a sukha. So PT and sukha are something like that. That is why PT is also

220
00:37:42,180 --> 00:37:48,880
translated as pleasurable interest. So when you heard or see a, when you're going on a

221
00:37:48,880 --> 00:37:53,600
journey and you're tired and when you heard that there's a palm over there or when you

222
00:37:53,600 --> 00:38:01,680
see the palm, you have a kind of joy or something like that. And when you really reach

223
00:38:01,680 --> 00:38:09,960
the palm and it's made use of water and so on and you have this real experience of that

224
00:38:09,960 --> 00:38:16,560
thing. So one is like an interest in that thing and the other is a real experience of it,

225
00:38:16,560 --> 00:38:33,720
that would be a rise together. Now, there is an alternate translation of the test is there.

226
00:38:33,720 --> 00:38:40,560
And that is which is accompanied by Vidaka and Vijara and which is with PT and sukha

227
00:38:40,560 --> 00:38:52,480
which are born of seclusion. Now, the word born of seclusion can qualify the Jana or it

228
00:38:52,480 --> 00:39:00,920
can qualify PT and sukha. Although it is not so important, whatever we take, but we have

229
00:39:00,920 --> 00:39:11,000
to understand this. So born of seclusion qualifies Jana in the first translation. So it is Jana

230
00:39:11,000 --> 00:39:18,240
which is born of seclusion. But in the alternative translation, it is PT and sukha which

231
00:39:18,240 --> 00:39:32,120
are born of seclusion. They actually remain the same thing because PT and sukha are constituents

232
00:39:32,120 --> 00:39:43,160
of Jana. Do you remember the five factors of Jana? We talk with Jana, PT, sukha and the last

233
00:39:43,160 --> 00:40:03,920
one, one point in this. So these are the five Jana factors. So the one PT sukha and

234
00:40:03,920 --> 00:40:18,400
Bali, or we got Jana. We got Jana PT sukha and Bali is interpreted in two ways. So in the

235
00:40:18,400 --> 00:40:26,360
first way, it is the Jana which is born of seclusion and which is accompanied by PT and

236
00:40:26,360 --> 00:40:36,360
sukha. In the alternative interpretation, it is PT and sukha which are born of seclusion

237
00:40:36,360 --> 00:40:46,560
and it is Jana which is accompanied by that PT and that sukha. So if PT and sukha are

238
00:40:46,560 --> 00:40:59,160
born of seclusion, then the Jana is also born of seclusion because PT and sukha are part

239
00:40:59,160 --> 00:41:24,280
of the Jana. And then first Jana, this will be explained below. And thus a poem, arrives and reaches

240
00:41:24,280 --> 00:41:46,520
the, the literal meaning is having approached something like that. And then the what

241
00:41:46,520 --> 00:41:57,820
dwell is explained in paragraph 103. So by becoming possessed of Jana of the kind described

242
00:41:57,820 --> 00:42:03,000
above through dwelling in a poster favorable to that Jana, he produces a poster, because

243
00:42:03,000 --> 00:42:09,000
he is keeping an enduring, lasting a dwelling of the person, of the buzzing really means

244
00:42:09,000 --> 00:42:21,320
of the body. Now, the first Jana abandons five factors and possesses five factors. So five

245
00:42:21,320 --> 00:42:30,940
factors are abandoned by first Jana and it is, it has, it possesses five factors. So the

246
00:42:30,940 --> 00:42:42,200
five factors abandons are the five mental hindrances. Here this detail is lust, ill will, stiffness

247
00:42:42,200 --> 00:42:50,600
and topper, agitation and worry and uncertainty. They are the five mental hindrances. For

248
00:42:50,600 --> 00:42:57,800
no Jana arises until these have been abandoned. So there can be no Jana when there are

249
00:42:57,800 --> 00:43:14,400
these five mental hindrances. What is stiffness? Actually, it is sleepiness. It is a translation

250
00:43:14,400 --> 00:43:24,560
of the War Bollywood Tina, but it is not really stiffness. It is an ability of the mind

251
00:43:24,560 --> 00:43:36,880
to be awake, to be alive. So we just say sleepiness. So it is translated the slope and

252
00:43:36,880 --> 00:43:59,040
topper in other books. And in paragraph 105, second line, non-become concentrated on an

253
00:43:59,040 --> 00:44:10,560
object consisting in unity. Real means the single object, because if you have to get Jana,

254
00:44:10,560 --> 00:44:16,640
your mind has to be on one and the same object, single object. So object consisting in

255
00:44:16,640 --> 00:44:25,520
unity with the simply means one object or single essence of the object, which is the single

256
00:44:25,520 --> 00:44:35,680
essence. That just means the single object of one object. And also we have the one sense

257
00:44:35,680 --> 00:44:41,640
desire element. So it should be understood as meaning sense desire, a realm of sense desire

258
00:44:41,640 --> 00:45:00,240
to be alive. Now, what are the five factors possessed by first Jana? These are the

259
00:45:00,240 --> 00:45:15,120
five factors we met in every Jana. Initial application, sustained application. But in the passage

260
00:45:15,120 --> 00:45:26,360
here, Eka Gurda is not mentioned. No. But we must understand that Eka Gurda is also a

261
00:45:26,360 --> 00:45:33,360
factor of the first Jana, not only the first Jana, of all Jana. So that is stated here.

262
00:45:57,360 --> 00:46:08,760
So although one pointedness of mind is not mentioned in the passage, we should understand

263
00:46:08,760 --> 00:46:15,760
that the one pointedness is one of the factors of Jana or the constituent of Jana.

264
00:46:15,760 --> 00:46:33,120
What says unification? That is what is meant here. So translated as unification of mind.

265
00:46:33,120 --> 00:46:50,280
And what we call Jana is simply…

