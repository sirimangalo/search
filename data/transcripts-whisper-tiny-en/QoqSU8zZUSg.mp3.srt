1
00:00:00,000 --> 00:00:06,160
Hello, welcome back to a series of videos on the Dhamapada.

2
00:00:06,160 --> 00:00:11,160
Today I will continue with verses 3 and 4.

3
00:00:11,160 --> 00:00:17,440
But first I'd like to make a correction to the first video about the first verse, wherein

4
00:00:17,440 --> 00:00:28,240
I said that the reason the Buddha gave this teaching was because of this monks killing

5
00:00:28,240 --> 00:00:33,520
all these insects unknowingly, and then the monks were saying that he'd done a bad deed

6
00:00:33,520 --> 00:00:38,640
and the Buddha said that he indeed didn't, and he explained that suffering only follows

7
00:00:38,640 --> 00:00:41,520
from bad intentions.

8
00:00:41,520 --> 00:00:49,580
But actually, going over it again, I realized that specifically the Buddha gave the teaching

9
00:00:49,580 --> 00:00:54,880
in regards to the reason why this monk went blind in the first place, which was because

10
00:00:54,880 --> 00:01:02,720
in the past life he had out of revenge for a woman, he had treated, he was a doctor who had

11
00:01:02,720 --> 00:01:09,800
given this treatment to a woman, and out of revenge for her trying to cheat him, he gave

12
00:01:09,800 --> 00:01:14,680
her something that would make her blind, and as a result of that bad deed, it came back

13
00:01:14,680 --> 00:01:18,360
to him, it was stuck in his mind, because he must have felt very guilty, as a doctor,

14
00:01:18,360 --> 00:01:23,840
he was a good doctor and feeling guilty about it, and it's something that he naturally

15
00:01:23,840 --> 00:01:28,880
would feel guilty about, and so it eventually caught up to him and he realized what a horrible

16
00:01:28,880 --> 00:01:39,320
thing he had done, and it conditioned his life for him to have to go blind.

17
00:01:39,320 --> 00:01:45,720
So that's the origin that after that the Buddha said, you see, you can never escape your

18
00:01:45,720 --> 00:01:52,760
bad deeds, but it does go both ways, I mean obviously there is reference to the fact that

19
00:01:52,760 --> 00:02:00,680
this Chakupala wasn't guilty for the killing that he had done, or for the death of the

20
00:02:00,680 --> 00:02:05,240
insects, but he was on the other hand guilty for something that he intended to do to

21
00:02:05,240 --> 00:02:11,880
hurt this woman, and so he got the results of it.

22
00:02:11,880 --> 00:02:15,800
But I'm not going into these stories so much, I just wanted to correct something that

23
00:02:15,800 --> 00:02:21,800
I had said, I'm going to try to stick to a very simple version of the stories, and place

24
00:02:21,800 --> 00:02:25,480
more emphasis on the meaning of the verses and how they apply to our lives, because I think

25
00:02:25,480 --> 00:02:28,400
that is a more important.

26
00:02:28,400 --> 00:02:32,560
So in this next story it's the same, it's a very simple story, but it's extended and

27
00:02:32,560 --> 00:02:36,200
I'm going to avoid the extension.

28
00:02:36,200 --> 00:02:58,480
So verses three and four, number three goes,

29
00:02:58,480 --> 00:03:24,400
which means, this is a quote he or she or this person scolded me, they hurt me, they beat

30
00:03:24,400 --> 00:03:36,360
me, they defeated me, they destroyed me, a person who clings or grasps on or is bound,

31
00:03:36,360 --> 00:03:43,920
binds themselves to these thoughts, way wrong taste on the somebody, for them their quarrels

32
00:03:43,920 --> 00:03:51,960
never ceased, or never appeased, or never tranquilized, or never finished.

33
00:03:51,960 --> 00:04:02,480
But Akotimanga, what the man that he knew, he scolded me, he beat me, he hurt me, he defeated

34
00:04:02,480 --> 00:04:09,840
me, for a person who doesn't cling to these thoughts, way wrong taste on the somebody,

35
00:04:09,840 --> 00:04:17,320
their quarrels are appeased, and their quarrels are tranquilized, they're finished, the problems

36
00:04:17,320 --> 00:04:21,760
they have with people are solved, they're worked out.

37
00:04:21,760 --> 00:04:28,800
These two verses were given in regards to a monk named Tissa, Tissa, who was very fat,

38
00:04:28,800 --> 00:04:36,800
he was quite overweight, he had quite a lot of weight and he became a monk late in life

39
00:04:36,800 --> 00:04:44,040
and rather than practicing meditation, he took to a very indulgent monk's life, he was

40
00:04:44,040 --> 00:04:50,360
a relative of the Buddha, so he was quite spoiled and being a part of the royalty.

41
00:04:50,360 --> 00:04:56,760
And also because of his conceit, because he was related to the Buddha, he didn't bother

42
00:04:56,760 --> 00:05:04,320
to exert himself in the practice of meditation, he just would sit around and make a general

43
00:05:04,320 --> 00:05:11,280
nuisance of himself, which in fact you do find in monasteries even today believe it or

44
00:05:11,280 --> 00:05:14,280
not.

45
00:05:14,280 --> 00:05:22,240
And so there was one occasion where he was sitting in the reception cell, I guess, in

46
00:05:22,240 --> 00:05:26,160
the middle of the monastery, maybe it wasn't a cell, but in the very middle where he would

47
00:05:26,160 --> 00:05:28,320
expect visitors to come.

48
00:05:28,320 --> 00:05:31,800
And so these visiting monks came, who were actually quite senior, but were younger than

49
00:05:31,800 --> 00:05:36,520
him, because he had ordained when he was old.

50
00:05:36,520 --> 00:05:43,000
And they thought, well maybe he's an old monk, so they came up to him, they saw that

51
00:05:43,000 --> 00:05:47,040
he was just sitting there looking proud and so they came and paid respect to him and

52
00:05:47,040 --> 00:05:53,480
asked politely who he was and so on, and how many years he had been a monk.

53
00:05:53,480 --> 00:05:57,960
And he told them, years, he said, years, I've just ordained, I don't have any years,

54
00:05:57,960 --> 00:05:59,600
it's a monk.

55
00:05:59,600 --> 00:06:05,080
And they said, well then, how can you act like this, how can you just sit there?

56
00:06:05,080 --> 00:06:09,320
When visiting monks come, it's very important, there's a protocol.

57
00:06:09,320 --> 00:06:14,560
If the receiving monk, someone who sees the monk coming, is junior to the approaching

58
00:06:14,560 --> 00:06:19,800
monk, and they have a responsibility to take care of them, to take their ball, to bring

59
00:06:19,800 --> 00:06:24,800
them water, to show them to a room, and so on, and to make sure that they're well taken

60
00:06:24,800 --> 00:06:28,440
care of, and to pay respect to them.

61
00:06:28,440 --> 00:06:31,480
And they said, how come you're not following this protocol, you're not behaving like

62
00:06:31,480 --> 00:06:33,520
a proper monk.

63
00:06:33,520 --> 00:06:36,600
And they said, that's not appropriate, and he said, who are you?

64
00:06:36,600 --> 00:06:38,280
You think, who do you think you are?

65
00:06:38,280 --> 00:06:47,320
And he said, who, he snapped his fingers, this kind of rude gesture of the times in India.

66
00:06:47,320 --> 00:06:52,720
And they said, oh, come to see the Buddha, and he said, huh, and you think you can say

67
00:06:52,720 --> 00:06:57,520
nasty thing, he was very upset actually, he was very spoiled, and so he said, who do

68
00:06:57,520 --> 00:06:58,520
you think you are?

69
00:06:58,520 --> 00:07:04,720
You know who I am, and the relative of the Buddha, and his cousin, something like that,

70
00:07:04,720 --> 00:07:09,080
and he was very upset, and so he went to see the Buddha, and he started complaining.

71
00:07:09,080 --> 00:07:14,360
He started crying to the Buddha, and he said, these men have scolded me, and they've abused

72
00:07:14,360 --> 00:07:18,320
me, and the Buddha said, well, what did you do?

73
00:07:18,320 --> 00:07:22,400
He said, wow, who's just sitting there, and wow, and did you receive them when they came

74
00:07:22,400 --> 00:07:23,400
now?

75
00:07:23,400 --> 00:07:24,400
Did you take their balls?

76
00:07:24,400 --> 00:07:28,080
No, did you show them, and so on, and so on, no, no.

77
00:07:28,080 --> 00:07:33,560
And the Buddha said, well, then you're yourself, or to blame, you're the one who was acting

78
00:07:33,560 --> 00:07:40,240
inappropriately, and the Buddha said, you should apologize to them, and he said, apologize,

79
00:07:40,240 --> 00:07:41,240
what do you mean?

80
00:07:41,240 --> 00:07:44,440
They abused me, I would not apologize.

81
00:07:44,440 --> 00:07:49,600
So he's very obstinate, so the Buddha told the story of the past life, and this monk,

82
00:07:49,600 --> 00:07:53,560
the monks remarked how obstinate he was, and the obstinate he was, and the Buddha said,

83
00:07:53,560 --> 00:07:58,320
oh, he's been like this for a long time, it's kind of a habit of he is that extent,

84
00:07:58,320 --> 00:08:07,560
it had it, and so he told the story of the past life, and where he was, they were these

85
00:08:07,560 --> 00:08:13,800
two ascetics in a room together, and one of them is sleeping in front of the door, and

86
00:08:13,800 --> 00:08:18,520
one of them is sleeping inside, because it was a small room, and the one inside noticed

87
00:08:18,520 --> 00:08:23,360
that this monk had put his head down here, but in the middle of the night, this monk

88
00:08:23,360 --> 00:08:29,040
got up and put his head the other way, turned around and sleep the other way, and the monk

89
00:08:29,040 --> 00:08:33,040
inside had to go outside to the washroom, and so he went around where he thought the monks

90
00:08:33,040 --> 00:08:40,980
feet were, and ended up stepping on his hair, and so the monk was very angry, and the

91
00:08:40,980 --> 00:08:45,160
monk other monks said, oh, I'm sorry, or the ascetic, the other ascetic said, oh, I'm

92
00:08:45,160 --> 00:08:52,120
sorry, I didn't mean to, and he went outside, and on the way, when he was out, the monks

93
00:08:52,120 --> 00:08:56,600
said, I'm not going to let him do that again, and so he flipped over, and put his head

94
00:08:56,600 --> 00:09:01,520
the other way, again, back to where it originally was, and the other monk came back and

95
00:09:01,520 --> 00:09:05,320
thought to himself, okay, I'll avoid stepping on his head, so he ran around to the other

96
00:09:05,320 --> 00:09:10,880
side, and ended up stepping on the guy's neck, ah, there was a big fight that ensued,

97
00:09:10,880 --> 00:09:14,760
and he said, well look, you had your head there, what are you doing, changing all the

98
00:09:14,760 --> 00:09:17,760
time, how can I know where your head is, it's dark, I don't know if I can clean it on the

99
00:09:17,760 --> 00:09:24,080
flashlight, the monk, the monk wasn't the ascetic wasn't to be appeased, and he was

100
00:09:24,080 --> 00:09:30,000
very angry, and he held onto this, and he made a big fuss over, and he actually cursed

101
00:09:30,000 --> 00:09:35,680
the other ascetic, and then they were cursing back and forth, and so on, and so on, it's

102
00:09:35,680 --> 00:09:41,600
a long story, I don't want to get into it, but the point being that people do hold on

103
00:09:41,600 --> 00:09:46,200
to these things, and this is important, because this is what the Buddha emphasized, he

104
00:09:46,200 --> 00:09:53,600
didn't say, don't have these thoughts, he doesn't say, ah, for a person who has these

105
00:09:53,600 --> 00:10:00,200
thoughts, because these thoughts are wrong, it's wrong for us to think like this, why

106
00:10:00,200 --> 00:10:06,200
is it wrong, it's unpleasant, it creates suffering for us, and it builds up a bad habit,

107
00:10:06,200 --> 00:10:09,200
but what the Buddha is saying here is actually a little more specific, he's saying don't

108
00:10:09,200 --> 00:10:15,000
cling to them, he says for a person who is bound to them, like the word is like the

109
00:10:15,000 --> 00:10:22,000
same word we use for the Uis and Pali for a sandal, and you bind together a sandal,

110
00:10:22,000 --> 00:10:29,400
the bound, the rope of the sandal, and so on, so when you're tied to these things, when

111
00:10:29,400 --> 00:10:35,920
you're caught up in these thoughts, when you cling to them, basically, this is what

112
00:10:35,920 --> 00:10:42,000
where your problems never cease, because the point here that I think is important is that

113
00:10:42,000 --> 00:10:47,360
we do have problems, we do have disagreements, I've got an angry at people, we get angry

114
00:10:47,360 --> 00:10:52,720
at people, we have people do things we don't like, and we get angry at them, we get upset

115
00:10:52,720 --> 00:10:59,600
at each other, a good example is with our families, when our parents tell us to do something

116
00:10:59,600 --> 00:11:03,800
or our parents criticize, we can become very angry, even to the point where we yell at

117
00:11:03,800 --> 00:11:09,840
them and where we say nasty things and really hurt our parents, it can cause hurt for

118
00:11:09,840 --> 00:11:14,560
them or suffering for them, and we suffer ourselves and so on, but what we won't do is

119
00:11:14,560 --> 00:11:19,720
cling, generally, speaking in an ordinary family situation, we won't cling to it, so the

120
00:11:19,720 --> 00:11:24,320
next day or the next moment we'll forget about it, because we don't have this clinging

121
00:11:24,320 --> 00:11:30,480
to them, but we aren't this way in all cases, and there is the case where, in us with this

122
00:11:30,480 --> 00:11:36,640
monk, this sub, he conjured it, and he couldn't get over his own anger, his own upset

123
00:11:36,640 --> 00:11:41,200
at being criticized, it's very difficult to be criticized, it's very difficult for young

124
00:11:41,200 --> 00:11:46,480
monks, new monks to be criticized, because they can be very proud of the fact that now

125
00:11:46,480 --> 00:11:51,200
they're in a special state and now they have some kind of special lifestyle and people

126
00:11:51,200 --> 00:11:56,560
respect them and look up to them and give them free food and shelter and so on, and so

127
00:11:56,560 --> 00:12:01,680
they can be very proud of that instead of putting it to good use and really making themselves

128
00:12:01,680 --> 00:12:10,160
into something worthy of it, and so this is where the problem arises with the conceitum

129
00:12:10,160 --> 00:12:14,840
of the clinging to it, where you become self-righteous and you say, I don't deserve that

130
00:12:14,840 --> 00:12:21,480
and so on, you don't even, there's not even the rational thought there where you think

131
00:12:21,480 --> 00:12:25,160
you know, are they right, are they wrong and if they're wrong then you can say, well,

132
00:12:25,160 --> 00:12:28,280
I don't deserve that and you can reply to them.

133
00:12:28,280 --> 00:12:35,120
For instance, this Tissa, he had this complaint to the Buddha and he said, this monks

134
00:12:35,120 --> 00:12:39,320
have done something nasty, while the Buddha did respond, it's not that the Buddha just

135
00:12:39,320 --> 00:12:44,520
said, oh yes, he has letting him walk all over the Buddha, walk all over these monks letting

136
00:12:44,520 --> 00:12:48,400
him get away with it, the Buddha was quite clear that he was wrong and we can do that if

137
00:12:48,400 --> 00:12:52,440
someone criticizes us and says, we did something wrong, we can say no, I didn't do anything

138
00:12:52,440 --> 00:12:58,200
wrong, based on our rational thought, but we don't do this generally, generally we say

139
00:12:58,200 --> 00:13:05,200
no, we don't think about, we just out of anger, we refuse any criticism, we become upset

140
00:13:05,200 --> 00:13:09,200
and we become self-righteous and it festers in the mind.

141
00:13:09,200 --> 00:13:16,520
This is what gives rise to grudges and imiti and feuds, war.

142
00:13:16,520 --> 00:13:21,920
When we build hate in our mind and it's a self-righteous, equitistical hate, this is how

143
00:13:21,920 --> 00:13:26,400
religious wars are fought, it's not because we're angry at each other.

144
00:13:26,400 --> 00:13:32,960
If you look at ideological wars in the world where, you know, this country comes to hate

145
00:13:32,960 --> 00:13:38,280
that country and our whole mindset is that this country is evil, this country is bad,

146
00:13:38,280 --> 00:13:44,840
many people feel this way about America and they've cultivated this evil and that's what

147
00:13:44,840 --> 00:13:51,080
happened in this, all this terrorism that's been a fear of the American people, I mean

148
00:13:51,080 --> 00:13:56,120
to some extent it's justified because there's a lot of people out there who don't like,

149
00:13:56,120 --> 00:14:03,960
we just have this anger towards this country as a whole, as an example and you have this

150
00:14:03,960 --> 00:14:09,080
throughout the world, you know, our prejudice against people and so on.

151
00:14:09,080 --> 00:14:15,480
We build this up quite often and this is what we have to be careful about because we

152
00:14:15,480 --> 00:14:20,480
will have fights, we will have disagreements and we will get angry at each other until

153
00:14:20,480 --> 00:14:25,680
we get to the point where we are able to see that anger is unpleasant and unuseful

154
00:14:25,680 --> 00:14:33,160
and able to get rid of it entirely, but what is dangerous and what is a hindrance to

155
00:14:33,160 --> 00:14:38,880
our path towards giving up the anger is this attachment that's clinging to it.

156
00:14:38,880 --> 00:14:45,040
It covers up the whole nature of the anger, it prevents you from actually looking at the

157
00:14:45,040 --> 00:14:49,640
anger from what it is, when you're angry and you say, I don't, you know, this is right

158
00:14:49,640 --> 00:14:55,800
that I'm angry, I'm angry, these people deserve my wrath and so on, then you're justifying

159
00:14:55,800 --> 00:15:03,520
it and you're covering it up, it's like you have this great, this great, that, suppose

160
00:15:03,520 --> 00:15:11,040
you have this ball of hot iron or you have this hot coal and you have a holding in your

161
00:15:11,040 --> 00:15:16,760
hand and you say, and it's hurting you and so you grab it and you say, what is it that

162
00:15:16,760 --> 00:15:21,600
is hurting me, what is it that's hurting me when you say, you're looking for the cause

163
00:15:21,600 --> 00:15:26,640
outside of yourself, when actually it's right here, you know, when, you know, this person

164
00:15:26,640 --> 00:15:31,880
who's causing you suffering, causing you to get angry and then you say, you're the cause,

165
00:15:31,880 --> 00:15:35,440
you're the reason why I'm suffering and you're not looking at what you're holding

166
00:15:35,440 --> 00:15:39,160
here, you know, holding the anger, you're holding on to these thoughts, it's actually

167
00:15:39,160 --> 00:15:46,120
these thoughts making us suffer, we have to be very careful because the anger itself is

168
00:15:46,120 --> 00:15:53,560
not so dangerous, it's unpleasant but it's not the real problem, the danger in the

169
00:15:53,560 --> 00:16:02,000
anger is with the ego that comes along with it, that if we have ego and we attach our

170
00:16:02,000 --> 00:16:08,440
ego to the anger and it becomes self-righteous and we, every time we think of a person

171
00:16:08,440 --> 00:16:16,800
where we are upset by them, we're angry, we have hate towards people or we have a grudged

172
00:16:16,800 --> 00:16:19,960
towards people, we're biased against people and so on.

173
00:16:19,960 --> 00:16:24,120
This is what, every time we see them, everything they do will be, you know, something

174
00:16:24,120 --> 00:16:28,360
we can criticize, we'll always be looking for, they did that wrong and so on and every

175
00:16:28,360 --> 00:16:32,200
when they do things good, we'll feel upset, you know, what good things happen to them will

176
00:16:32,200 --> 00:16:38,240
be upset when they act in a good way, we'll try to minimize it and so on and we'll create

177
00:16:38,240 --> 00:16:43,680
great suffering for ourselves, this is a very important lesson and it's in general a very

178
00:16:43,680 --> 00:16:49,520
important lesson with the defilements of the mind but they're really not that bad, it'll

179
00:16:49,520 --> 00:16:54,360
greed, anger, greed, let's agree to anger, they're not that big of a deal, you know, you

180
00:16:54,360 --> 00:16:58,640
can watch them and you can look at them but the problem is we don't, probably we never learn

181
00:16:58,640 --> 00:17:03,600
about these things, people think pleasure is such a great thing, you know, attaching the

182
00:17:03,600 --> 00:17:07,520
things is so great when you have sensual pleasure and sexual pleasure and so on, we think

183
00:17:07,520 --> 00:17:14,840
it's so great because we cling to it because there's the idea that I like this, I want

184
00:17:14,840 --> 00:17:19,160
this and so on but if we actually just looked at the pleasure when it came up instead of

185
00:17:19,160 --> 00:17:24,680
saying, okay, I have to get this, if we just looked at the wanting of it, if we just

186
00:17:24,680 --> 00:17:29,160
examined it for what it is, we see that actually it's quite a bit of stress actually,

187
00:17:29,160 --> 00:17:33,960
it's this tension and the only way you can relieve that tension is either by seeing it

188
00:17:33,960 --> 00:17:38,320
letting it go or by chasing after and getting what you want and then saying, ah, no, I don't

189
00:17:38,320 --> 00:17:45,640
need it anymore, so the happiness doesn't come from the wanting, it comes from giving up

190
00:17:45,640 --> 00:17:50,440
the wanting, either in one of two ways, the problem of course with following is that you're

191
00:17:50,440 --> 00:17:57,320
developed, you're encouraging it and you're developing it more and more, you're clinging

192
00:17:57,320 --> 00:18:04,520
more and more and there's no understanding of the problem, another same goes with anger

193
00:18:04,520 --> 00:18:12,200
when you're angry at someone, it's suffering, yes, but it's actually not such a big deal

194
00:18:12,200 --> 00:18:20,880
until you do what this is and you come to identify with the anger and identify a source

195
00:18:20,880 --> 00:18:25,200
of the anger in another person, that they are the cause of the problem, that the problem

196
00:18:25,200 --> 00:18:29,280
is not the anger, the problem is the other person.

197
00:18:29,280 --> 00:18:32,960
So the real problem is our delusion and our ignorance, this is why the Buddha said,

198
00:18:32,960 --> 00:18:39,440
I'll read Japakta, I'll sankara, all formations in the mind, all judgments, your sankara

199
00:18:39,440 --> 00:18:47,400
hear means, our judgments, our partialities, our projections that we place on things, these

200
00:18:47,400 --> 00:18:53,440
are all caused by ignorance, it's ignorance that is the root and so that's really what this

201
00:18:53,440 --> 00:19:03,400
verse is talking about, it's the delusion, the identification with the thoughts, that is

202
00:19:03,400 --> 00:19:11,920
the problem, our defilements are in different levels, there is the misperception of something

203
00:19:11,920 --> 00:19:20,760
as pleasant or unpleasant, which comes to us all until you get to fully enlighten, you still

204
00:19:20,760 --> 00:19:24,480
will have this perception of something as being pleasant or unpleasant, something comes

205
00:19:24,480 --> 00:19:29,520
all that's nice, or something of the that's not nice, this is just a feeling that you get,

206
00:19:29,520 --> 00:19:34,560
you know if something is, oh this is experience as pleasant, this is experience as unpleasant,

207
00:19:34,560 --> 00:19:38,800
the next level is our thoughts, and this is what he's talking about here is this thought,

208
00:19:38,800 --> 00:19:44,240
oh this is I'm thinking, you know that's unpleasant thinking, this is pleasant, so

209
00:19:44,240 --> 00:19:49,000
person says something to you and think, oh he's hurting me, oh he's abusing me, this

210
00:19:49,000 --> 00:19:53,240
person saying that, see anything, oh that person saying that, see things, this is that

211
00:19:53,240 --> 00:19:58,080
the thought level, but the worst one is the views, when you have the view about it, that

212
00:19:58,080 --> 00:20:07,800
is good, this is bad, pleasure is good, pleasure, you know, pain is bad and so on, and

213
00:20:07,800 --> 00:20:16,120
so this is when you identify, when you build up this eye and you ask them to me and

214
00:20:16,120 --> 00:20:22,040
you hurt me and so on, when we build this up, this is what really, really causes problems,

215
00:20:22,040 --> 00:20:26,760
because as I said it obscures the nature of the experience, the only way we can come to

216
00:20:26,760 --> 00:20:31,360
be free is if we are able to see the nature of the anger, the nature of the grief and

217
00:20:31,360 --> 00:20:35,960
be able to work out for ourselves, ourselves what is truly right for us, the only way

218
00:20:35,960 --> 00:20:40,880
we can work out what is truly right is by seeing the individual experience and we can't

219
00:20:40,880 --> 00:20:48,640
do this if we're clinging, we can't do this if we are, if we have some eye, some idea

220
00:20:48,640 --> 00:20:56,120
or identification with the object as me and mine and so on, okay, so I think I've done

221
00:20:56,120 --> 00:21:03,040
over that enough, and this is a fairly important teaching, it helps us to, I think this

222
00:21:03,040 --> 00:21:10,080
will help us in our practice, because it also helps us to accept and to allow to come up

223
00:21:10,080 --> 00:21:14,840
the bad things in our mind, greed and anger are bad, but the only way we're going to come

224
00:21:14,840 --> 00:21:19,280
to see that they're really and truly bad is if we let them come up, we can't just say

225
00:21:19,280 --> 00:21:25,080
it's bad, it's good, until we have wisdom and we're able to see clearly, what is it,

226
00:21:25,080 --> 00:21:30,480
is it good, is it bad, not from here thinking, and so on and not because I say it because

227
00:21:30,480 --> 00:21:35,280
a book says, but simply by seeing it for yourself, so you don't have this view that

228
00:21:35,280 --> 00:21:39,880
well, I think anger is good, well, I think greed is good and then, or you don't have the idea

229
00:21:39,880 --> 00:21:45,640
that, oh, you're the dumbest and bad, you're the dumbest and it's good and so on, you know

230
00:21:45,640 --> 00:21:50,000
for yourself, and only when you know for yourself, are you going to be able to see, you're

231
00:21:50,000 --> 00:21:54,440
going to be able to let go of it and overcome it, and you'll never be able to do this again

232
00:21:54,440 --> 00:21:58,800
as I said again and again now, never be able to do this if you're clinging to it, if

233
00:21:58,800 --> 00:22:05,800
you're holding on to it, and therefore weigh it on days on the summit, the quarrels, the

234
00:22:05,800 --> 00:22:10,640
problems that that person encounters will never cease, so thanks for tuning in, it's

235
00:22:10,640 --> 00:22:40,480
been another verse from all the best.

