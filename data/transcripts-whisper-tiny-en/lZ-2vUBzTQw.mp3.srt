1
00:00:00,000 --> 00:00:06,000
I accidently stepped on a baby rabbit and killed it while working in the yard today.

2
00:00:06,000 --> 00:00:10,000
I immediately felt an intense confusing mix of thoughts and feelings.

3
00:00:10,000 --> 00:00:14,000
Mostly killed, grief, foolishness, unworthiness and so on.

4
00:00:14,000 --> 00:00:19,000
I might have worked through these.

5
00:00:19,000 --> 00:00:26,000
The interesting question for me here is why we feel guilt.

6
00:00:26,000 --> 00:00:29,000
Right?

7
00:00:29,000 --> 00:00:31,000
I mean, it's not baffling or anything.

8
00:00:31,000 --> 00:00:41,000
It's just curious as to why specifically, I'm interested in exploring why we feel guilt.

9
00:00:41,000 --> 00:00:46,000
We feel bad.

10
00:00:46,000 --> 00:00:49,000
We may not even feel guilt.

11
00:00:49,000 --> 00:00:56,000
But at first, the first part is the feeling bad.

12
00:00:56,000 --> 00:00:59,000
And I think we have to separate that out.

13
00:00:59,000 --> 00:01:04,000
Because I think initially, there's just a horror that something has died.

14
00:01:04,000 --> 00:01:10,000
When we hear about the baby rabbit being killed, we all feel bad.

15
00:01:10,000 --> 00:01:16,000
It's awful to think of a baby rabbit being stepped on.

16
00:01:16,000 --> 00:01:19,000
And I think you have to separate that out.

17
00:01:19,000 --> 00:01:27,000
Because otherwise, it makes you think that your guilt is somehow justified.

18
00:01:27,000 --> 00:01:33,000
So, I think that's where the...

19
00:01:33,000 --> 00:01:37,000
Well, first of all, that's a very useful, I think, in coming to terms with it.

20
00:01:37,000 --> 00:01:40,000
Our ability to separate these two.

21
00:01:40,000 --> 00:01:44,000
The awful feeling that something like a baby bunny rabbit,

22
00:01:44,000 --> 00:01:59,000
probably the epitome of cuteness, has been killed versus the judgment that we have killed this baby rabbit.

23
00:01:59,000 --> 00:02:03,000
Because there's a disconnect there.

24
00:02:03,000 --> 00:02:07,000
It's a non sequitur, meaning it's not related.

25
00:02:07,000 --> 00:02:13,000
The fact that the baby rabbit has died because you're stepped on it.

26
00:02:13,000 --> 00:02:19,000
It doesn't necessitate the conclusion that you are morally responsible.

27
00:02:19,000 --> 00:02:21,000
Why would you be...

28
00:02:21,000 --> 00:02:22,000
We think about it logically.

29
00:02:22,000 --> 00:02:28,000
Why would you be held morally responsible for stepping on a baby rabbit?

30
00:02:28,000 --> 00:02:30,000
Now, there may be answers there.

31
00:02:30,000 --> 00:02:33,000
Maybe you knew there were baby rabbits in your backyard.

32
00:02:33,000 --> 00:02:37,000
And people warned you about this and said,

33
00:02:37,000 --> 00:02:43,000
oh, trancing around in the backyard, because there's baby rabbits, and you said, I don't care,

34
00:02:43,000 --> 00:02:46,000
you know, whatever.

35
00:02:46,000 --> 00:02:49,000
And you got all drunk and went trancing around in your backyard.

36
00:02:49,000 --> 00:02:51,000
You didn't get drunk. Leave that out.

37
00:02:51,000 --> 00:02:54,000
But you went trancing around in your backyard,

38
00:02:54,000 --> 00:02:57,000
jumping here and there and everywhere.

39
00:02:57,000 --> 00:03:01,000
And lo and behold, you stepped on the baby rabbit.

40
00:03:01,000 --> 00:03:12,000
In the spirit of modern legal leaves or modern jurisprudence,

41
00:03:12,000 --> 00:03:14,000
you'd be guilty of negligence, right?

42
00:03:14,000 --> 00:03:21,000
So from a Buddhist point of view, if we were taking a page out of the jurisprudence book,

43
00:03:21,000 --> 00:03:24,000
we would say that you're a Carmicling negligent.

44
00:03:24,000 --> 00:03:28,000
And I think that would stick, Carmicling.

45
00:03:28,000 --> 00:03:41,000
That's really an awful sort of mind state to disregard the potential for stepping on baby rabbits.

46
00:03:41,000 --> 00:03:46,000
But much more likely is that you had no idea that there were baby rabbits out there.

47
00:03:46,000 --> 00:03:50,000
You were not being overly unmindful.

48
00:03:50,000 --> 00:03:58,000
And so that's something you have to keep in mind that how mindful you are,

49
00:03:58,000 --> 00:04:03,000
can often affect these situations.

50
00:04:03,000 --> 00:04:09,000
So if you're totally unmindful, no, you're going to head in the clouds.

51
00:04:09,000 --> 00:04:13,000
It's much easier to step on the baby rabbit,

52
00:04:13,000 --> 00:04:16,000
whereas if you're mindful and aware of what you're doing,

53
00:04:16,000 --> 00:04:18,000
it's less likely.

54
00:04:18,000 --> 00:04:23,000
So your guilty of that sort of thing, your potentially guilty of that sort of thing.

55
00:04:23,000 --> 00:04:28,000
But still, it may be that you were not unmindful,

56
00:04:28,000 --> 00:04:33,000
but you just were otherwise engaged because you don't always look at your feet.

57
00:04:33,000 --> 00:04:36,000
So you were doing something else and walking,

58
00:04:36,000 --> 00:04:39,000
and you could even be aware of your foot moving potentially.

59
00:04:39,000 --> 00:04:45,000
But you happen to step on in the killer baby rabbit.

60
00:04:45,000 --> 00:04:48,000
Now, why is that your fault?

61
00:04:48,000 --> 00:04:50,000
I think thinking about that helps.

62
00:04:50,000 --> 00:04:53,000
It helps you to realize that it's not your fault,

63
00:04:53,000 --> 00:05:00,000
because otherwise we get the sense that I wouldn't feel guilty about it if it weren't my fault.

64
00:05:00,000 --> 00:05:03,000
It's not actually the case, and that's what's interesting about this,

65
00:05:03,000 --> 00:05:08,000
because you'd feel that if I didn't do anything wrong,

66
00:05:08,000 --> 00:05:10,000
why do I feel guilty?

67
00:05:10,000 --> 00:05:14,000
That makes you feel like you deserve it.

68
00:05:14,000 --> 00:05:17,000
You know, I must have done something wrong because I feel guilty,

69
00:05:17,000 --> 00:05:19,000
and clearly that's not the case.

70
00:05:19,000 --> 00:05:23,000
And when you break it apart as with everything,

71
00:05:23,000 --> 00:05:26,000
why we like certain things, why we hate certain things,

72
00:05:26,000 --> 00:05:28,000
why we worry about certain things,

73
00:05:28,000 --> 00:05:33,000
and now why we feel guilty about certain things.

74
00:05:33,000 --> 00:05:47,000
It really has all to do with our the imprecision of our understanding of the situation.

75
00:05:47,000 --> 00:05:51,000
So once you deeply understand the situation by breaking it up in this way,

76
00:05:51,000 --> 00:05:57,000
and you can do that empirically or experientially phenomenologically,

77
00:05:57,000 --> 00:05:59,000
as these things come up in your mind,

78
00:05:59,000 --> 00:06:01,000
the horror of stepping on the baby rabbit,

79
00:06:01,000 --> 00:06:04,000
the memory of the feeling of stepping on the baby rabbit,

80
00:06:04,000 --> 00:06:09,000
maybe the sounds that it made, whatever.

81
00:06:09,000 --> 00:06:13,000
And then the thoughts about the baby rabbit's mother,

82
00:06:13,000 --> 00:06:16,000
and the pain that the baby rabbit went through,

83
00:06:16,000 --> 00:06:19,000
and then the anger and the frustration and the sadness,

84
00:06:19,000 --> 00:06:20,000
and then the guilt.

85
00:06:20,000 --> 00:06:23,000
You'll be able to break all that up.

86
00:06:23,000 --> 00:06:26,000
You'll find two things.

87
00:06:26,000 --> 00:06:28,000
And so far I've only talked about the first one,

88
00:06:28,000 --> 00:06:38,000
and that is that most likely you don't even deserve to feel guilty.

89
00:06:38,000 --> 00:06:41,000
You don't deserve to punish yourself for it.

90
00:06:41,000 --> 00:06:44,000
There's no karmic potency to that act,

91
00:06:44,000 --> 00:06:46,000
because there was no intention.

92
00:06:46,000 --> 00:06:48,000
But the second thing is,

93
00:06:48,000 --> 00:06:53,000
and this is something that we have to stress and make clear in Buddhism,

94
00:06:53,000 --> 00:06:56,000
is that even if you are culpable,

95
00:06:56,000 --> 00:07:00,000
even if you saw the rabbit there and decided to step on it,

96
00:07:00,000 --> 00:07:03,000
maybe even decided to torture it,

97
00:07:03,000 --> 00:07:07,000
draw out its death as long as possible.

98
00:07:07,000 --> 00:07:09,000
Surprise!

99
00:07:09,000 --> 00:07:13,000
There still is no benefit to feeling guilty about it.

100
00:07:13,000 --> 00:07:19,000
Now, it's highly likely that if you start to practice meditation,

101
00:07:19,000 --> 00:07:21,000
you're going to feel incredibly guilty about it.

102
00:07:21,000 --> 00:07:24,000
But even that guilt is unwholesome.

103
00:07:24,000 --> 00:07:26,000
It's an anger.

104
00:07:26,000 --> 00:07:29,000
It's an improper reaction.

105
00:07:29,000 --> 00:07:32,000
And in fact, even then,

106
00:07:32,000 --> 00:07:35,000
the best course of action is simply to realize

107
00:07:35,000 --> 00:07:40,000
that the states that led you to do that are a problem,

108
00:07:40,000 --> 00:07:44,000
are a cause of suffering for yourself.

109
00:07:44,000 --> 00:07:49,000
That in fact, it's not in your best interest to act cruelly.

110
00:07:49,000 --> 00:07:52,000
It's in your best interest to give up cruelly.

111
00:07:52,000 --> 00:07:55,000
That in fact, cruelly corrupts your mind,

112
00:07:55,000 --> 00:08:01,000
pollutes your mind, and leads you to state of suffering in the future.

113
00:08:01,000 --> 00:08:03,000
Not guilt.

114
00:08:03,000 --> 00:08:04,000
Guilting fact.

115
00:08:04,000 --> 00:08:06,000
So that is also quite useful,

116
00:08:06,000 --> 00:08:09,000
I think, in dealing with the problem,

117
00:08:09,000 --> 00:08:12,000
is realizing that even if you are morally culpable,

118
00:08:12,000 --> 00:08:15,000
and usually we can find some way to blame ourselves,

119
00:08:15,000 --> 00:08:17,000
even if we know it was an accident,

120
00:08:17,000 --> 00:08:20,000
we're still blame ourselves for being negligent.

121
00:08:20,000 --> 00:08:23,000
Even then, guilt is useless.

122
00:08:23,000 --> 00:08:28,000
Feeling guilty about something is actually unwholesome.

123
00:08:28,000 --> 00:08:32,000
It's a feeling of what we call guilt

124
00:08:32,000 --> 00:08:34,000
can be broken up into two things.

125
00:08:34,000 --> 00:08:36,000
One, the knowledge that it's wrong,

126
00:08:36,000 --> 00:08:38,000
and to the disliking of it,

127
00:08:38,000 --> 00:08:42,000
the pain, the suffering, the sadness that comes from it,

128
00:08:42,000 --> 00:08:44,000
the negative feeling about it.

129
00:08:44,000 --> 00:08:46,000
That negative feeling is the problem.

130
00:08:46,000 --> 00:08:48,000
Knowing that it's wrong isn't a problem.

131
00:08:48,000 --> 00:08:51,000
It's a bad extent, one aspect of guilt is okay,

132
00:08:51,000 --> 00:08:56,000
but once you feel guilty in the sense of bad feeling bad about it,

133
00:08:56,000 --> 00:08:59,000
or if you're so bad that I did eggs,

134
00:08:59,000 --> 00:09:00,000
that's not useful.

135
00:09:00,000 --> 00:09:01,000
It's not helpful.

136
00:09:01,000 --> 00:09:04,000
It's cultivating an unwholesome habit of self-hatred,

137
00:09:04,000 --> 00:09:06,000
which can eventually become debilitating,

138
00:09:06,000 --> 00:09:10,000
and is always unpleasant and useless.

139
00:09:10,000 --> 00:09:19,000
So, there you go.

