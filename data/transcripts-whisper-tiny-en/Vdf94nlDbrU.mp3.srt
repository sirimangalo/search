1
00:00:00,000 --> 00:00:09,920
choose one type of Buddhism over another, why choose one type of Buddhism over another?

2
00:00:09,920 --> 00:00:19,680
I personally am rather into that a little bit. I just like that tradition. Some people like

3
00:00:19,680 --> 00:00:33,920
it's just, I don't think you should pick one type of Buddhism or another. Pick the teachings,

4
00:00:33,920 --> 00:00:39,040
well, what do you do? You don't, you know, you can say I'm this Buddhist or that Buddhist, but

5
00:00:39,040 --> 00:00:42,880
really what you're doing is following one set of teachings or another, so which set of teachings

6
00:00:42,880 --> 00:00:48,000
should you follow? Well, you should follow the teachings of the most enlightened being you can find.

7
00:00:48,000 --> 00:00:54,240
Follow the teachings of the Buddha, then you're not picking one type or another, because you find

8
00:00:54,240 --> 00:01:01,760
that the whole type thing is really misleading and the best practitioners of all types of Buddhism

9
00:01:03,680 --> 00:01:11,680
are very similar and say very similar things and very similar goals. And so there's only one

10
00:01:11,680 --> 00:01:18,800
Buddhism, it's following the teachings of an enlightened Buddha or practicing to become an enlightened

11
00:01:18,800 --> 00:01:34,080
Buddha, one or the other.

