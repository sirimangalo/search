1
00:00:00,000 --> 00:00:13,000
I start to study seriously Buddhist teachings not so long ago, so it's still really new, so it's difficult to formulate questions.

2
00:00:13,000 --> 00:00:24,000
Would you explain what goes from life to life if me or ego, the Ataman, does not really exist?

3
00:00:24,000 --> 00:00:46,000
Imagine an apple, you eating an apple, this apple eating eaten up, you drop it on the ground somewhere and it falls on a futile soil.

4
00:00:46,000 --> 00:01:10,000
And after some time, the seed starts to grow again, and a new apple tree is there, and suddenly, after some time, there are new apples.

5
00:01:10,000 --> 00:01:36,000
So you can't say that the first apple and the last apple are the same, or that some thing from the first apple had gone into the second apple.

6
00:01:36,000 --> 00:01:55,000
But the first apple was necessary for the second apple to arise, to come to be, if there haven't been the first apple, the second apple wouldn't be there.

7
00:01:55,000 --> 00:02:08,000
So they are kind of interrelated, there is a connection, but you can't say there's something going from the first thing to the second thing.

8
00:02:08,000 --> 00:02:12,000
There is nothing you can grab, you can hold.

9
00:02:12,000 --> 00:02:34,000
But still, there is a not something that is needed, that the first becomes the second, or that one lot when one life ends, another life comes to be.

10
00:02:34,000 --> 00:02:53,000
You can imagine maybe a string, and this string would be your consciousness, and your subconsciousness.

11
00:02:53,000 --> 00:03:21,000
And every single moment, you experience, or better said, every single moment that is experienced is a beat on that string, and then death would just be another beat, and the next birth would just be another beat.

12
00:03:21,000 --> 00:03:49,000
And it's all just lined up on the string, which is basically not conscious, subconscious, some people say unconscious, and some of it becomes conscious, some moments become conscious.

13
00:03:49,000 --> 00:04:06,000
And you can't see where the string of consciousness started, when it started to be pearls that are put onto the string.

14
00:04:06,000 --> 00:04:17,000
And you can't say when it ends, but you can note every single moment, and that's what we are doing with the mindfulness practice.

15
00:04:17,000 --> 00:04:30,000
We are trying to be mindful of every single moment, make it conscious like that, and be in the present moment.

16
00:04:30,000 --> 00:04:56,000
So, and when you do this, you will understand that there is really not so much a thing that you could call me or ego, but it's just a moment after the other passing by, just a process ongoing and ongoing.

17
00:04:56,000 --> 00:05:00,000
Maybe you want to say more on it.

18
00:05:00,000 --> 00:05:04,000
I mean, I normally answer the same way, but there is something that has to be said in addition.

19
00:05:04,000 --> 00:05:09,000
That is exactly what different words how I would have said it.

20
00:05:09,000 --> 00:05:17,000
Death is just another moment, but there is something else that has to be said.

21
00:05:17,000 --> 00:05:29,000
So, I just like to clarify that death does have some significance, because birth is kind of like an explosion, or not birth, but conception.

22
00:05:29,000 --> 00:05:37,000
When a being takes a life, it takes a lot of energy to be born, especially in a coarse realm, like as a human.

23
00:05:37,000 --> 00:05:51,000
It takes a lot of energy, and there is this burst of energy that sets us on this course as a human being, because even in our hunt, when they give up the attachment to rebirth, they still have to live their life out.

24
00:05:51,000 --> 00:06:03,000
This is because of the power of the greed and the craving to be reborn, that is no longer with them, but that led them to be born.

25
00:06:03,000 --> 00:06:08,000
At the moment of death, we create this new life.

26
00:06:08,000 --> 00:06:17,000
We create the birth as a human being, so it is more like waves, I think.

27
00:06:17,000 --> 00:06:25,000
The waves on the ocean, our existence, all of this craving is forcing the water.

28
00:06:25,000 --> 00:06:30,000
It is like the power of the gravity, forcing the water into a wave.

29
00:06:30,000 --> 00:06:36,000
And then the wave crashes, but the next wave builds up.

30
00:06:36,000 --> 00:06:47,000
So once you get rid of the force that is creating the waves, then there is the end to the end of the waves.

31
00:06:47,000 --> 00:07:04,000
There is no moment of death that we can say, there was this life, and there was the next life.

32
00:07:04,000 --> 00:07:08,000
But there is some buildup of power, and then the release of power.

33
00:07:08,000 --> 00:07:17,000
And during this life, we build up more of this attachment, and so when we die, there is another burst.

34
00:07:17,000 --> 00:07:18,000
There is this buildup.

35
00:07:18,000 --> 00:07:20,000
On our hunt, in light and being doesn't do that.

36
00:07:20,000 --> 00:07:23,000
And so at the moment of death, there is nothing.

37
00:07:23,000 --> 00:07:26,000
So death is an important moment.

38
00:07:26,000 --> 00:07:31,000
It doesn't mean the death of a cell, for the death of anyone thing.

39
00:07:31,000 --> 00:07:35,000
It couldn't, because such a thing doesn't exist.

40
00:07:35,000 --> 00:07:42,000
There is nothing in experience that can be seen as a cell.

41
00:07:42,000 --> 00:07:45,000
There is nothing in reality that can be seen as a cell.

42
00:07:45,000 --> 00:07:52,000
But it has some power, and it is able to force us to be born again.

43
00:07:52,000 --> 00:08:04,000
The real point about, I think the real answer to if there is no self, how does this process occur?

44
00:08:04,000 --> 00:08:13,000
So, you know, you have to explain why things are, or have to help people to understand what Pallanyani is talking about,

45
00:08:13,000 --> 00:08:16,000
that it's just moment to moment experience.

46
00:08:16,000 --> 00:08:21,000
Reality doesn't admit of these philosophical arguments.

47
00:08:21,000 --> 00:08:23,000
It doesn't admit of views.

48
00:08:23,000 --> 00:08:27,000
It doesn't admit of the existence of entities.

49
00:08:27,000 --> 00:08:31,000
It doesn't admit of the existence of a three-dimensional space.

50
00:08:31,000 --> 00:08:34,000
So, all of these things arise in the mind.

51
00:08:34,000 --> 00:08:37,000
What is truly real is experience.

52
00:08:37,000 --> 00:08:45,000
It's that which is experience, that which is verifiable, that which is empirical,

53
00:08:45,000 --> 00:08:50,000
that which we come in contact with at every moment.

54
00:08:50,000 --> 00:08:59,000
So, the whole view of a self has no place in this.

55
00:08:59,000 --> 00:09:08,000
You see, the whole theory of a self, or the whole perception that there might be a self,

56
00:09:08,000 --> 00:09:12,000
has no place, can't fit into this.

57
00:09:12,000 --> 00:09:15,000
Because what you have is moment to moment experience.

58
00:09:15,000 --> 00:09:18,000
That's what we have, that's what we experience.

59
00:09:18,000 --> 00:09:28,000
Buddhism teaches us to become in harmony with this.

60
00:09:28,000 --> 00:09:30,000
So this is how we understand reality.

61
00:09:30,000 --> 00:09:34,000
To help us to understand the world and reality in terms of these things.

62
00:09:34,000 --> 00:09:36,000
Reality is just here.

63
00:09:36,000 --> 00:09:39,000
The question, is there a soul, is there an impermanent soul?

64
00:09:39,000 --> 00:09:42,000
What do these words mean?

65
00:09:42,000 --> 00:09:45,000
These are words on a page.

66
00:09:45,000 --> 00:09:50,000
They're thoughts that arise in the mind.

67
00:09:50,000 --> 00:09:53,000
There's no way to answer such questions.

68
00:09:53,000 --> 00:09:55,000
They have no meaning.

69
00:09:55,000 --> 00:09:56,000
They're meaningless.

70
00:09:56,000 --> 00:09:58,000
Like asking, is there an apple?

71
00:09:58,000 --> 00:10:04,000
If you have the apple and it falls from the tree, is there an apple?

72
00:10:04,000 --> 00:10:08,000
It's a totally meaningless question from a Buddhist point of view.

73
00:10:08,000 --> 00:10:10,000
There's the experience.

74
00:10:10,000 --> 00:10:16,000
When you talk about what is really there, there's the experience of seeing,

75
00:10:16,000 --> 00:10:19,000
or if you hear the apple fall, there's the experience of hearing.

76
00:10:19,000 --> 00:10:22,000
If you think of it as an apple, there's the experience of thinking.

77
00:10:22,000 --> 00:10:25,000
If you wonder whether the apple exists, there's the experience of wondering.

78
00:10:25,000 --> 00:10:29,000
So the question of whether the apple exists or not is meaningless.

79
00:10:29,000 --> 00:10:30,000
There's no answer.

80
00:10:30,000 --> 00:10:31,000
You can't give an answer to it.

81
00:10:31,000 --> 00:10:35,000
The only answer you could say is, well, according to theory, no, it doesn't exist.

82
00:10:35,000 --> 00:10:36,000
But it's meaningless.

83
00:10:36,000 --> 00:10:41,000
The words, does the soul, does the apple exist, or is there a soul?

84
00:10:41,000 --> 00:10:43,000
I have no meaning.

85
00:10:43,000 --> 00:10:49,000
It's like there was one monkey said, it's like if you ask an innocent man why he killed his wife,

86
00:10:49,000 --> 00:10:51,000
or why he beat his wife.

87
00:10:51,000 --> 00:10:53,000
He didn't beat his wife.

88
00:10:53,000 --> 00:10:56,000
There's no, you can't ask the question of why.

89
00:10:56,000 --> 00:10:59,000
These are questions that have no relationship to reality.

90
00:10:59,000 --> 00:11:02,000
Reality is a moment to moment experience.

91
00:11:02,000 --> 00:11:04,000
So the idea is there a self, isn't there a self?

92
00:11:04,000 --> 00:11:06,000
None of these questions apply.

93
00:11:06,000 --> 00:11:09,000
There is this moment to moment experience.

94
00:11:09,000 --> 00:11:13,000
So it goes on regardless of whether there is a self or not,

95
00:11:13,000 --> 00:11:18,000
asking what goes from life to life is also, can't be answered.

96
00:11:18,000 --> 00:11:20,000
Life to life doesn't exist.

97
00:11:20,000 --> 00:11:21,000
This life doesn't exist.

98
00:11:21,000 --> 00:11:23,000
What exists is moment to moment.

99
00:11:23,000 --> 00:11:25,000
And it functions in a certain way.

100
00:11:25,000 --> 00:11:29,000
And as I said, it gives rise to this next wave.

101
00:11:29,000 --> 00:11:43,000
No, the power, the impetus, the impulse, the force that is caused by the craving.

102
00:11:43,000 --> 00:11:46,000
But that's just a part of how this moment to moment experience worked.

103
00:11:46,000 --> 00:11:48,000
And you can see how that works.

104
00:11:48,000 --> 00:11:49,000
You can watch.

105
00:11:49,000 --> 00:11:54,000
When you die, you can watch your craving lead you to the next life.

106
00:11:54,000 --> 00:11:55,000
You can see that happening.

107
00:11:55,000 --> 00:11:57,000
The idea of what is it that's doing this?

108
00:11:57,000 --> 00:11:59,000
Or what is behind all of this?

109
00:11:59,000 --> 00:12:02,000
Or what is the substratum of this existence?

110
00:12:02,000 --> 00:12:04,000
It has no meaning.

111
00:12:04,000 --> 00:12:05,000
What could you say?

112
00:12:05,000 --> 00:12:08,000
You could make up some kind of theory about what is the substratum.

113
00:12:08,000 --> 00:12:12,000
And you could even see that it's verified in reality.

114
00:12:12,000 --> 00:12:15,000
The problem is that we go, this is exactly what science does.

115
00:12:15,000 --> 00:12:23,000
It theorizes and it tries to find things that find theories that mesh with reality.

116
00:12:23,000 --> 00:12:26,000
But none of that has anything to do with reality.

117
00:12:26,000 --> 00:12:27,000
Reality is the experience.

118
00:12:27,000 --> 00:12:32,000
Whether you figure it out, what is the substratum or how it works?

119
00:12:32,000 --> 00:12:35,000
Has no bearing on what it is.

120
00:12:35,000 --> 00:12:37,000
Has no bearing on reality.

121
00:12:37,000 --> 00:12:46,000
So these questions, therefore, the point being that as you practice meditation, these questions drop away.

122
00:12:46,000 --> 00:12:49,000
You don't ever think as a Buddhist, is there a self?

123
00:12:49,000 --> 00:12:50,000
Isn't there a self?

124
00:12:50,000 --> 00:12:56,000
Once you become developed in meditation, these questions drop away.

125
00:12:56,000 --> 00:12:58,000
There's no answer.

126
00:12:58,000 --> 00:13:03,000
There's no connection with what you're experiencing.

127
00:13:03,000 --> 00:13:05,000
The experience is what it is.

128
00:13:05,000 --> 00:13:07,000
It comes and it goes and arises.

129
00:13:07,000 --> 00:13:08,000
It ceases.

130
00:13:08,000 --> 00:13:09,000
It's a moment to moment.

131
00:13:09,000 --> 00:13:12,000
It's impermanent, unsatisfying, uncontrollable.

132
00:13:12,000 --> 00:13:14,000
It's not worth clinging to.

133
00:13:14,000 --> 00:13:17,000
And so eventually the clinging drops away.

134
00:13:17,000 --> 00:13:19,000
The clinging to anything drops away.

135
00:13:19,000 --> 00:13:23,000
And with the dropping away of clinging, there's the end of suffering, with the end of suffering.

136
00:13:23,000 --> 00:13:26,000
There's freedom.

137
00:13:26,000 --> 00:13:30,000
Martin has a related question.

138
00:13:30,000 --> 00:13:35,000
Can we say that the mind continues if someone asks?

139
00:13:35,000 --> 00:13:40,000
I wouldn't say that.

140
00:13:40,000 --> 00:13:43,000
Because it does not continue.

141
00:13:43,000 --> 00:13:46,000
The mind is not stable.

142
00:13:46,000 --> 00:13:52,000
The soul, talking about the soul or talking about the mind continues,

143
00:13:52,000 --> 00:14:00,000
is comes from the wish that there is something to hold on to grab.

144
00:14:00,000 --> 00:14:06,000
But this wish is irrelevant.

145
00:14:06,000 --> 00:14:08,000
It doesn't work.

146
00:14:08,000 --> 00:14:09,000
It doesn't work.

147
00:14:09,000 --> 00:14:12,000
It doesn't work that way.

148
00:14:12,000 --> 00:14:15,000
The mind does not continue.

149
00:14:15,000 --> 00:14:17,000
Look at your mind.

150
00:14:17,000 --> 00:14:23,000
This is the fastest thing that exists.

151
00:14:23,000 --> 00:14:31,000
It's going quickly in every second your mind changes.

152
00:14:31,000 --> 00:14:34,000
So you can't really say it continues.

153
00:14:34,000 --> 00:14:40,000
If someone asks, tell them to stop asking questions and go practice.

154
00:14:40,000 --> 00:14:49,000
Well, yeah.

