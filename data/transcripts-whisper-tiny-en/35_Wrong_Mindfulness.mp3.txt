Difficulties and Meditation Wrong Mindfulness
The bitter thought that mindfulness is always good.
In the Abhidharma it is good.
So Bana Chaita Sika, which means a beautiful mind state.
The Buddha himself did, however, use the term wrong mindfulness at times.
To answer the question of what wrong mindfulness means,
we can begin with a technical definition of mindfulness.
In the traditional terrified attacks,
all Dhammas have four qualities.
Each Dhamma will have a characteristic, a function,
a manifestation, and a proximate or nearby cause.
The characteristic of mindfulness is being unwavering.
The ordinary mind, wobbles and waivers.
It is not stable nor fixed on an object.
Mindfulness enables grasping the object firmly without wavering.
The function of mindfulness is to not forget.
Ordinary mindfulness refers to the ability to recollect things that happened long ago.
Then there is Sati Patana, the mindfulness meditation practice.
The meaning of Sati Patana is to not forget the object in the present moment.
Ordinarily, we experience something momentarily,
and then get caught up in judgment and reaction,
forgetting about the actual object.
We see something, but we are not interested in the seeing,
rather we are interested in what it means.
Wondering, is it good, is it bad, is it me, is it mine?
Mindfulness does not do that.
Mindfulness sticks with the pure experience of the object.
Mindfulness manifests in two ways,
firstly as guarding and secondly as confronting the objective field.
And no ordinary mind is unguarded and the farmers may enter easily.
Mindfulness guards against these departments.
Mindfulness also confronts the objective field.
An ordinary mind is not always able to confront objects of experience.
When an unpleasant experience comes, the ordinary mind shies away from it.
And when a pleasant experience arises, the mind immediately chases after it.
Carding the mind and confronting the object are signs of mindfulness,
which enables one to have positive and negative experiences without reacting.
The proximate cause that gives rise to mindfulness is strong perception.
When you perceive that you are seeing, that perception is called sana.
When you perceive hearing that cat meowing, that perception is called sana.
Theta sama is when you reaffirm the perception and it becomes strengthened.
This is accomplished by reminding yourself of the experience.
As seeing, seeing or hearing, hearing, reminding yourself of what you are experiencing,
strengthens the pure perception of the experience, giving rise to mindfulness.
The text saying that mindfulness is like a pillar because it is firmly founded.
The ordinary mind is like a bull floating on water that flits about here and there.
Mindfulness is like a pillar sunk in the bottom of a lake.
No matter how the wind or water buffets the pillar, it does not shake.
Mindfulness is also said to be like a gatekeeper.
It guards the eye, ear, and the other sense doors where all of our experience comes.
Mindfulness lets experiences enter without letting in the defamments.
What then is wrong mindfulness?
There are four ways we might think of wrong mindfulness.
Unmindfulness, misdirected mindfulness, lapsed mindfulness, and impotent mindfulness.
Unmindfulness is just the opposite of mindfulness.
The characteristic of unmindfulness is being wavering.
Its function is forgetfulness, its manifestation is not confronting
and its proximate cause is weak perception.
If you never go to a meditation center or you never take up the practice of meditation,
you are generally unmindful and therefore your mind wobbles.
Your mind is also forgetful so you cannot remember things that happened yesterday
and you can never remember the present moment.
You experience something and immediately you react.
You do not face objects of experience because of weak perception.
You perceive something but your mind is not trained to stick with the simple perception
so you get lost in your reactions.
Misdirected mindfulness means mindfulness that is focused on the wrong objects.
This type of mindfulness is not intrinsically wrong, just wrong for a specific practice.
If you want to go somewhere you need to go on the right road.
There is nothing wrong with the other roads.
They just do not take you where you are trying to go.
By the same token, if you practice mindfulness of the past,
remembering past life for example, it is never going to allow you to attain enlightenment
because it is focused on the wrong objects.
There is nothing technically wrong with the past, it is just a mundane conceptual object.
Likewise, mindfulness of the future, like when you plan ahead or have an experience
of precognition where you see something before it happens,
could also be considered a form of mindfulness, but it does not help you either
because it is also not focused on actual reality as you experience it.
If you focus on a concept, for example, a candle flame,
eventually you are able to see this object in your mind conceptually.
When this happens, the object is stable, satisfying and even controllable.
You can expand and contract it in your mind.
You can enter into very high states of calm taking a concept as an object,
but it is not going to lead you to enlightenment.
It is not any charm, impermanent, dukha, suffering, or anata, non-self.
You will never see these three characteristics, three mindfulness of concepts,
so for the purposes of enlightenment, it is misdirected.
Lapse to mindfulness is like unmindfulness, except that it arises in someone who is practicing correctly.
We, as meditators, are not robots or machines.
This is not an assembly line where we pass people through and they all come out in light.
Everyone has conditions in which they come to meditation,
and every meditator has a different experience.
Often, this shows itself in positive or negative states,
or in deep states that may have been hidden.
The ten imperfections of insight describe some of the positive states that result from proper practice.
These positive states will not lead to enlightenment,
although sometimes meditators start to think that they will and are led astring.
Impetant mindfulness relates to an absence of the other factors of the noble eightfold path.
If you have wrong with you, you will not see clearly no matter how mindful you are.
If you believe any of the five kandas are the self,
or if you do not believe in karma, or if you think that God created us,
all of these views will prevent you from seeing ordinary experiences clearly.
The same goes for wrong thought, bad intentions or ambitions and cruelty in the mind.
These will all get in the way of mindfulness practice.
If you lie, gossip, or just talk a lot, it is going to get in the way of mindfulness.
The same goes if you are a murderer, or a thief, or if you take drugs or alcohol.
You can try your best of being mindful, but if you are not keeping the five precepts,
you will not see clearly.
I would never guide someone through meditation course if they were not able to keep at least the
five precepts. It is just futile.
If you practice wrong livelihood, making a living from bad things,
or if you practice wrong effort, being lazy, or directing effort towards the cultivation of
unhoseness, all of these will prevent mindfulness from being effective.
If you are unfocused, through wrong concentration, or if you are focused on the wrong things,
it will make whatever mindfulness you have impotent.
The eightfold path must work together with mindfulness.
As mindfulness on his own is not enough, mindfulness is like the key that starts the engine.
He gets everything going, but if your engine is broken, the key does not do much.
So these are what would be considered wrong mindfulness.
The consequences of wrong mindfulness are regression, stagnation, and complication.
Regression happens when you get discouraged, and then instead of gaining wholesome states,
you regress and become unwindful, afraid of, or upset with your practice,
you may think the practice is useless because you are not practicing properly.
This may cause one to leave the practice and Buddhism, and it may even lead one to
disparage Buddhism as useless. Stagnation is another danger, and meditators may practice for years
without progressing if their mindfulness is not well directed.
Some meditators may experience great calm and peace in their practice,
but never gain insight because they fail to cultivate the force at a botana.
Complication is the third consequence of wrong mindfulness, and this is the most dangerous.
Mindfulness is designed to simplify things.
The Buddha said, when you see, let it just be seen.
This is how you should train yourself.
When you are hearing, let it just be hearing.
When you experience something, let it just be the experience.
When you are unmindful, or if you have a distorted state of mindfulness in which one of the path
factors is missing, your perception is complicated rather than simplified.
This can actually lead to mind states that are more tense, stressed, or poisonous,
and through repeated and intensive practice, you may lose your mindfulness entirely and become
temporarily insane, unable to control yourself.
I have seen this happen to meditators, though I have never had it happen to someone
practicing under my guidance.
Without close and proper guidance, some meditators may lose their mindfulness,
and that can be dangerous.
Wrong mindfulness is something you will have to be concerned with.
