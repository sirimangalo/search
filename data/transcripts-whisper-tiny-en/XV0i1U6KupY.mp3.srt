1
00:00:00,000 --> 00:00:02,800
Let's make a short video then.

2
00:00:02,800 --> 00:00:06,600
Are there any esoteric teachings in terror about a Buddhism?

3
00:00:06,600 --> 00:00:07,800
No.

4
00:00:07,800 --> 00:00:08,200
No.

5
00:00:08,200 --> 00:00:11,240
One profound statement that may not profound,

6
00:00:11,240 --> 00:00:14,800
but one moving statement that the Buddha made

7
00:00:14,800 --> 00:00:17,640
was right before he passed away.

8
00:00:17,640 --> 00:00:25,360
And that was in response to Anandah, who, if my memory serves me,

9
00:00:25,360 --> 00:00:27,800
try to get it exactly right.

10
00:00:27,800 --> 00:00:30,280
Anandah was saying, you know, the monks are assembled,

11
00:00:30,280 --> 00:00:32,600
waiting for, oh, yes, the monks are hoping

12
00:00:32,600 --> 00:00:35,520
that you'll give them some last words.

13
00:00:35,520 --> 00:00:39,120
You know, they said, the monks are, they're not upset.

14
00:00:39,120 --> 00:00:41,040
They're feeling okay, because they know well,

15
00:00:41,040 --> 00:00:44,560
the tatagata wouldn't go into Parinibana

16
00:00:44,560 --> 00:00:49,560
without giving some last instruction.

17
00:00:49,560 --> 00:00:55,680
And the Buddha said to Anandah, what does the community

18
00:00:55,680 --> 00:00:57,440
of monks expect for me?

19
00:00:57,440 --> 00:00:59,280
What do they want from me?

20
00:00:59,280 --> 00:01:02,720
And then he said, I have taught the dhamma

21
00:01:02,720 --> 00:01:09,280
without any distinction between the inner and the outer,

22
00:01:09,280 --> 00:01:13,000
without the closed fist of a teacher.

23
00:01:13,000 --> 00:01:14,960
I'm not sure exactly what the poly is,

24
00:01:14,960 --> 00:01:19,080
but the English translation is, I'm not someone

25
00:01:19,080 --> 00:01:21,000
who holds something back.

26
00:01:21,000 --> 00:01:24,640
I have taught the dhamma beautiful in the meaning,

27
00:01:24,640 --> 00:01:26,360
in the letter and in the meaning,

28
00:01:26,360 --> 00:01:28,760
without any distinction between the inner,

29
00:01:28,760 --> 00:01:35,480
between the esoteric and the exotary.

30
00:01:35,480 --> 00:01:36,880
I've given you everything.

31
00:01:36,880 --> 00:01:39,440
I've explained to you the beginning, the middle,

32
00:01:39,440 --> 00:01:41,720
and the end of the holy life.

33
00:01:41,720 --> 00:01:43,680
And I've done that in full.

34
00:01:43,680 --> 00:01:47,800
It's like, what else is there?

35
00:01:47,800 --> 00:01:55,240
And so no, in that sense, there is no esoteric teachings.

36
00:01:55,240 --> 00:01:56,280
But that's only one sense.

37
00:01:56,280 --> 00:01:57,800
The other sense of your question, of course,

38
00:01:57,800 --> 00:02:01,240
is you're asking, is there anything secret that we

39
00:02:01,240 --> 00:02:04,200
have to be initiated to learn?

40
00:02:04,200 --> 00:02:07,200
And in that sense, yes, there might be.

41
00:02:07,200 --> 00:02:09,360
But why is that?

42
00:02:09,360 --> 00:02:12,440
Because as I said, a person who's not meditating

43
00:02:12,440 --> 00:02:15,000
can't understand what I was talking about namarupa.

44
00:02:15,000 --> 00:02:16,720
They just won't understand it.

45
00:02:16,720 --> 00:02:18,000
If they haven't been meditating, it

46
00:02:18,000 --> 00:02:19,360
makes no sense to them.

47
00:02:19,360 --> 00:02:22,240
It sounds like an intellectual theory.

48
00:02:22,240 --> 00:02:26,760
And it's just one more theory to put on their shelf.

49
00:02:26,760 --> 00:02:29,080
As soon as you start meditating, that changes.

50
00:02:29,080 --> 00:02:31,560
And you are able to understand namarupa.

51
00:02:31,560 --> 00:02:33,760
And you listen to that and you say, yeah,

52
00:02:33,760 --> 00:02:35,040
that's what I'm experiencing.

53
00:02:35,040 --> 00:02:36,120
Oh, that makes sense.

54
00:02:36,120 --> 00:02:39,360
Yeah, that really is what's going on here.

55
00:02:39,360 --> 00:02:40,840
And then you practice a little bit more.

56
00:02:40,840 --> 00:02:42,800
And you reach the second stage of knowledge.

57
00:02:42,800 --> 00:02:46,200
And then we explain to you about that and talk to you.

58
00:02:46,200 --> 00:02:47,760
And you say, yeah, that is what's going on.

59
00:02:47,760 --> 00:02:49,800
But if you didn't reach the second stage of knowledge,

60
00:02:49,800 --> 00:02:51,360
it would be meaningless.

61
00:02:51,360 --> 00:02:55,520
And our words would have no significance to them.

62
00:02:55,520 --> 00:02:57,800
Then you reach the third and the fourth and the fifth

63
00:02:57,800 --> 00:02:59,720
and the sixth and the seventh, all the way up

64
00:02:59,720 --> 00:03:09,000
to the 13th student on at the 13th, the 11th stage of knowledge.

65
00:03:09,000 --> 00:03:10,720
You get all the way up to 11th stage of knowledge.

66
00:03:10,720 --> 00:03:12,800
And up to that point, we're able to talk to you about it.

67
00:03:12,800 --> 00:03:15,720
We're able to ask you how you're doing where you're at.

68
00:03:15,720 --> 00:03:18,080
And when we hear that you're in this stage of that stage,

69
00:03:18,080 --> 00:03:24,160
we are able to guesstimate that you're at this stage or that stage.

70
00:03:24,160 --> 00:03:30,560
Then after you hit the 11th stage, then it goes dark.

71
00:03:30,560 --> 00:03:33,720
Then there's, well, then there's cessation,

72
00:03:33,720 --> 00:03:35,320
the freedom from suffering.

73
00:03:35,320 --> 00:03:37,800
And that's something that's really beyond words.

74
00:03:37,800 --> 00:03:49,120
So my point being that we're not going to talk to you so much

75
00:03:49,120 --> 00:03:52,080
about those higher stages because if you haven't reached them,

76
00:03:52,080 --> 00:03:53,400
it's really useless for you.

77
00:03:53,400 --> 00:03:57,480
And it can be detrimental because as an intellectual theory,

78
00:03:57,480 --> 00:03:58,760
you're just going to sit there and say,

79
00:03:58,760 --> 00:04:00,120
when am I going to reach this one?

80
00:04:00,120 --> 00:04:01,360
When am I going to reach that one?

81
00:04:01,360 --> 00:04:02,560
Did I reach this one yet?

82
00:04:02,560 --> 00:04:03,640
Did I reach that one yet?

83
00:04:03,640 --> 00:04:05,160
Was that this?

84
00:04:05,160 --> 00:04:09,840
I had one student who I taught her the knowledge

85
00:04:09,840 --> 00:04:14,040
and this was after she had done a few courses.

86
00:04:14,040 --> 00:04:17,880
But she still wasn't getting it because I was kind of pushing her

87
00:04:17,880 --> 00:04:21,400
and I said, come on, you really have to come and meet with us.

88
00:04:21,400 --> 00:04:26,280
I would like you to come and see me, come and see me every day

89
00:04:26,280 --> 00:04:28,680
because I was kind of concerned that her practice

90
00:04:28,680 --> 00:04:32,280
might be going on the wrong track.

91
00:04:32,280 --> 00:04:35,120
And so I said to her, I said, so how are you doing?

92
00:04:35,120 --> 00:04:37,960
And she said, well, today I'm in this stage

93
00:04:37,960 --> 00:04:42,760
and a little bit of that stage and also this stage as well.

94
00:04:42,760 --> 00:04:46,280
And so she was giving me three totally different stages of practice

95
00:04:46,280 --> 00:04:51,600
that were in total different parts of the progression.

96
00:04:51,600 --> 00:04:54,680
And that was really a sign that probably I

97
00:04:54,680 --> 00:04:58,360
had made this mistake in giving her too much knowledge

98
00:04:58,360 --> 00:05:00,480
because it was obviously hitting her in the wrong way.

99
00:05:00,480 --> 00:05:03,760
She was misunderstanding and misinterpreting

100
00:05:03,760 --> 00:05:07,040
and therefore thinking somehow she could be in three places

101
00:05:07,040 --> 00:05:12,640
at once, which is really not possible.

102
00:05:12,640 --> 00:05:13,760
Technically, it could happen.

103
00:05:13,760 --> 00:05:15,760
Suppose in the morning, you're in this stage

104
00:05:15,760 --> 00:05:17,880
and you go to that stage and then you drop back down

105
00:05:17,880 --> 00:05:24,120
or whatever it can happen, but it doesn't really work that way.

106
00:05:24,120 --> 00:05:25,960
Not in practice.

107
00:05:25,960 --> 00:05:28,840
So these are the kind of things that it's better not to know.

108
00:05:28,840 --> 00:05:31,160
You can go and study them, but it's going to be meaningless

109
00:05:31,160 --> 00:05:33,160
to you, it can even be detrimental.

110
00:05:33,160 --> 00:05:36,000
My teacher said, if you've read about the knowledge,

111
00:05:36,000 --> 00:05:39,000
as he said, it'll block you.

112
00:05:39,000 --> 00:05:40,560
He was very harsh.

113
00:05:40,560 --> 00:05:44,120
He said, he said, a person who hasn't practiced

114
00:05:44,120 --> 00:05:46,960
and reads about the knowledge is, will be blocked

115
00:05:46,960 --> 00:05:52,680
from realizing the truth, will be blocked from the abandonment.

116
00:05:52,680 --> 00:05:55,480
So we all kind of sat there and we're like, whoa, that's,

117
00:05:55,480 --> 00:05:57,880
I think he was being a little bit over dramatic.

118
00:05:57,880 --> 00:06:00,680
I'm not sure he may be totally right,

119
00:06:00,680 --> 00:06:03,120
but I don't think it's quite so strict.

120
00:06:03,120 --> 00:06:05,480
I think you're not cursed for life

121
00:06:05,480 --> 00:06:07,520
just because you read about the knowledge is,

122
00:06:07,520 --> 00:06:09,320
but he was trying to make a valid point

123
00:06:09,320 --> 00:06:11,640
and it's carried out in practice.

124
00:06:12,720 --> 00:06:17,360
That you should be careful not to acquire too much knowledge

125
00:06:17,360 --> 00:06:20,160
without adequate meditation practice,

126
00:06:20,160 --> 00:06:30,160
preferably under a trained and qualified teacher.

