1
00:00:00,000 --> 00:00:15,000
Hello and welcome to our evening session.

2
00:00:30,000 --> 00:00:59,000
Welcome to our evening session.

3
00:00:59,000 --> 00:01:13,000
We should just keep practicing better for you.

4
00:01:13,000 --> 00:01:24,000
So we often talk about

5
00:01:24,000 --> 00:01:34,000
the sorts of things we're looking for in insight meditation practice.

6
00:01:34,000 --> 00:01:43,000
How we're looking to understand impermanence, suffering non-cell,

7
00:01:43,000 --> 00:02:00,000
to cultivate insight and understanding about reality deeper than before.

8
00:02:00,000 --> 00:02:07,000
What we should find, what we're looking for in the practice.

9
00:02:07,000 --> 00:02:16,000
At the same time, we need also to be aware of the things that we're not looking for,

10
00:02:16,000 --> 00:02:24,000
those things that are distraction from the practice.

11
00:02:24,000 --> 00:02:29,000
So we call these the vipasa nupa kilesa,

12
00:02:29,000 --> 00:02:31,000
the imperfections of insight.

13
00:02:31,000 --> 00:02:33,000
They're not bad.

14
00:02:33,000 --> 00:02:37,000
They're not unwholesome.

15
00:02:37,000 --> 00:02:41,000
Most of them aren't.

16
00:02:41,000 --> 00:02:45,000
But they're distracting.

17
00:02:45,000 --> 00:02:51,000
They take you away from your investigation,

18
00:02:51,000 --> 00:02:56,000
your objective observation of reality.

19
00:02:56,000 --> 00:02:58,000
So we have to be aware of what these are,

20
00:02:58,000 --> 00:03:02,000
and we have to be mindful of them when they come up.

21
00:03:02,000 --> 00:03:16,000
The first one is called obasa.

22
00:03:16,000 --> 00:03:21,000
This interesting phenomenon that occurs when you meditate to begin to see things.

23
00:03:21,000 --> 00:03:24,000
It's not just mental images.

24
00:03:24,000 --> 00:03:33,000
They actually really see lights, colors, pictures, a lot of strange things.

25
00:03:33,000 --> 00:03:38,000
Some meditators, even though their eyes are closed, it will feel like

26
00:03:38,000 --> 00:03:42,000
the broom has suddenly become brighter.

27
00:03:42,000 --> 00:03:44,000
Like the door is open.

28
00:03:44,000 --> 00:03:48,000
It's a common one meditator will be in their hut in the forest,

29
00:03:48,000 --> 00:03:51,000
and suddenly they'll see bright light,

30
00:03:51,000 --> 00:03:54,000
and it'll feel like someone at the door has swung open,

31
00:03:54,000 --> 00:03:57,000
and they'll actually open their eyes to look.

32
00:03:57,000 --> 00:04:02,000
Some it feels like the roof has disappeared,

33
00:04:02,000 --> 00:04:09,000
and the sun is pouring down, bright light like this, brightness.

34
00:04:09,000 --> 00:04:14,000
Some people see colors or pictures.

35
00:04:14,000 --> 00:04:22,000
They'll be quite distracting, and for some people,

36
00:04:22,000 --> 00:04:26,000
not just a distraction,

37
00:04:26,000 --> 00:04:32,000
but will be misunderstood as being a part of the path.

38
00:04:32,000 --> 00:04:36,000
For some people they'll see lights and colors and pictures,

39
00:04:36,000 --> 00:04:41,000
and want to investigate them.

40
00:04:41,000 --> 00:04:45,000
That really goes for all of 10 of these.

41
00:04:45,000 --> 00:04:47,000
They'll be passed on upicky days,

42
00:04:47,000 --> 00:04:50,000
so they're things that are not bad per se,

43
00:04:50,000 --> 00:04:52,000
but they're not the path.

44
00:04:52,000 --> 00:04:55,000
If you mistake them, if you get caught up in them,

45
00:04:55,000 --> 00:05:00,000
they'll take you off the path.

46
00:05:00,000 --> 00:05:02,000
This one comes for some people.

47
00:05:02,000 --> 00:05:05,000
Comes for people who have strong concentration,

48
00:05:05,000 --> 00:05:07,000
or others they might never see anything.

49
00:05:07,000 --> 00:05:10,000
It's not obviously not what we're looking for.

50
00:05:10,000 --> 00:05:12,000
It's a distraction.

51
00:05:12,000 --> 00:05:14,000
When you see, you have to note to yourself

52
00:05:14,000 --> 00:05:18,000
seeing whatever you see, no matter how long you see it for.

53
00:05:22,000 --> 00:05:26,000
An important theme here is that none of this is any special.

54
00:05:26,000 --> 00:05:29,000
It's all just experience.

55
00:05:29,000 --> 00:05:31,000
It comes and it goes.

56
00:05:31,000 --> 00:05:34,000
There's nothing special about seeing things.

57
00:05:34,000 --> 00:05:39,000
When you take it as special you lose track of what's really important in it,

58
00:05:39,000 --> 00:05:46,000
it's called the waiting insight and understanding.

59
00:05:46,000 --> 00:05:48,000
The second one is Petey.

60
00:05:48,000 --> 00:05:53,000
Petey can come in many different forms.

61
00:05:53,000 --> 00:05:58,000
For some people it's,

62
00:05:58,000 --> 00:06:03,000
rushes of energy through their body.

63
00:06:03,000 --> 00:06:05,000
Some people it'll be crying.

64
00:06:05,000 --> 00:06:10,000
Just cry for an hour or two hours even.

65
00:06:10,000 --> 00:06:15,000
Well, cry for a long time anyway.

66
00:06:15,000 --> 00:06:19,000
Some it's laughing or yawning or that kind of thing.

67
00:06:19,000 --> 00:06:21,000
That's somewhat rare but it does happen.

68
00:06:21,000 --> 00:06:24,000
A common one is,

69
00:06:24,000 --> 00:06:28,000
a common one is feeling very light.

70
00:06:28,000 --> 00:06:30,000
Your whole body might feel light.

71
00:06:30,000 --> 00:06:32,000
Do you feel as if you're floating?

72
00:06:32,000 --> 00:06:34,000
Some people apparently do actually float.

73
00:06:34,000 --> 00:06:38,000
It's apparently possible.

74
00:06:38,000 --> 00:06:41,000
I'm sure scientists would

75
00:06:41,000 --> 00:06:46,000
beg to differ but the meditators have

76
00:06:46,000 --> 00:06:53,000
reported that they've actually levitated off their pillows.

77
00:06:53,000 --> 00:06:56,000
If it ever happens to you,

78
00:06:56,000 --> 00:06:59,000
you're supposed to keep a pencil or a pen

79
00:06:59,000 --> 00:07:03,000
by your sitting mat

80
00:07:03,000 --> 00:07:06,000
so that if you float up and float all the way to the ceiling,

81
00:07:06,000 --> 00:07:09,000
make a mark on the ceiling just to prove

82
00:07:09,000 --> 00:07:14,000
that you were there.

83
00:07:14,000 --> 00:07:17,000
There was one guy who actually did this,

84
00:07:17,000 --> 00:07:19,000
so I don't know.

85
00:07:19,000 --> 00:07:20,000
I didn't even write,

86
00:07:20,000 --> 00:07:23,000
it's quite common for there to be a feeling of floating.

87
00:07:23,000 --> 00:07:25,000
Another really common one is this swaying

88
00:07:25,000 --> 00:07:28,000
where you rock back and forth.

89
00:07:28,000 --> 00:07:32,000
You feel your body rocking.

90
00:07:32,000 --> 00:07:34,000
When I first did my course,

91
00:07:34,000 --> 00:07:39,000
my body was really strongly rocking rocking.

92
00:07:39,000 --> 00:07:42,000
I know this is fun.

93
00:07:42,000 --> 00:07:45,000
It's easy to sit for a long time that way.

94
00:07:45,000 --> 00:07:51,000
You get into this pattern,

95
00:07:51,000 --> 00:07:54,000
but you're unable to see suffering.

96
00:07:54,000 --> 00:07:56,000
You're not able to see things clearly

97
00:07:56,000 --> 00:07:58,000
because you're enjoying it.

98
00:07:58,000 --> 00:08:01,000
There's a pleasure associated with the rocking.

99
00:08:01,000 --> 00:08:06,000
I went to my teacher

100
00:08:06,000 --> 00:08:10,000
when you were supposed to have people.

101
00:08:10,000 --> 00:08:11,000
These aren't bad things,

102
00:08:11,000 --> 00:08:13,000
but they're distracting.

103
00:08:13,000 --> 00:08:15,000
If you don't stop them,

104
00:08:15,000 --> 00:08:17,000
this one I don't know,

105
00:08:17,000 --> 00:08:18,000
this guy in Bangkok,

106
00:08:18,000 --> 00:08:19,000
he said,

107
00:08:19,000 --> 00:08:20,000
this very famous teacher,

108
00:08:20,000 --> 00:08:21,000
he said,

109
00:08:21,000 --> 00:08:22,000
if it doesn't stop,

110
00:08:22,000 --> 00:08:23,000
you have to tell it to stop,

111
00:08:23,000 --> 00:08:28,000
and if they stop.

112
00:08:28,000 --> 00:08:33,000
He said,

113
00:08:33,000 --> 00:08:34,000
this guy says,

114
00:08:34,000 --> 00:08:36,000
do you mean stubborn?

115
00:08:36,000 --> 00:08:39,000
The filaments are stubborn.

116
00:08:39,000 --> 00:08:58,000
This one we have.

117
00:08:58,000 --> 00:09:00,000
Then passadhi.

118
00:09:00,000 --> 00:09:01,000
Next one.

119
00:09:01,000 --> 00:09:03,000
Maybe these aren't in order,

120
00:09:03,000 --> 00:09:05,000
but passadhi is another one.

121
00:09:05,000 --> 00:09:07,000
Passadhi is calm or quiet.

122
00:09:07,000 --> 00:09:10,000
Passadhi is quiet.

123
00:09:10,000 --> 00:09:12,000
Up until this point,

124
00:09:12,000 --> 00:09:14,000
especially right before these begin to arise,

125
00:09:14,000 --> 00:09:17,000
the mind will have been tortured and chaotic,

126
00:09:17,000 --> 00:09:19,000
and then suddenly the mind calms down,

127
00:09:19,000 --> 00:09:20,000
quiet, it's down.

128
00:09:20,000 --> 00:09:22,000
This is a common one for those of you

129
00:09:22,000 --> 00:09:24,000
who aren't practicing in our tradition.

130
00:09:24,000 --> 00:09:27,000
This is a common one for you meditating on your own.

131
00:09:27,000 --> 00:09:29,000
It's a common one to fall into,

132
00:09:29,000 --> 00:09:31,000
because you get to a very quiet state,

133
00:09:31,000 --> 00:09:34,000
and you don't know how to proceed further.

134
00:09:34,000 --> 00:09:38,000
This quiet state is about the limit of summit to meditation.

135
00:09:38,000 --> 00:09:41,000
So without guidance in repass in the meditation,

136
00:09:41,000 --> 00:09:44,000
you get stuck there.

137
00:09:44,000 --> 00:09:47,000
So I've often given talks in places

138
00:09:47,000 --> 00:09:49,000
where people practice meditation,

139
00:09:49,000 --> 00:09:50,000
and someone asks this question,

140
00:09:50,000 --> 00:09:53,000
what do you do when you feel very calm

141
00:09:53,000 --> 00:09:55,000
or feel very quiet?

142
00:09:55,000 --> 00:09:56,000
There's just nothing.

143
00:09:56,000 --> 00:10:01,000
Nothing to note, nothing to be mindful of.

144
00:10:01,000 --> 00:10:04,000
And then simply you would note quiet, quiet.

145
00:10:04,000 --> 00:10:07,000
So I've often instructed in this way,

146
00:10:07,000 --> 00:10:12,000
and it's really inspiring to see people

147
00:10:12,000 --> 00:10:17,000
be able to get past that by noting quiet.

148
00:10:17,000 --> 00:10:20,000
And then of course the mind starts up again,

149
00:10:20,000 --> 00:10:22,000
because they get out of this.

150
00:10:22,000 --> 00:10:26,000
The thing about these states is they become

151
00:10:26,000 --> 00:10:28,000
sort of like a broken record

152
00:10:28,000 --> 00:10:31,000
where you get stuck in them.

153
00:10:31,000 --> 00:10:32,000
So when you pull out of them,

154
00:10:32,000 --> 00:10:56,000
you get back into a more ordinary state.

155
00:10:56,000 --> 00:10:59,000
So it's a good thing.

156
00:10:59,000 --> 00:11:02,000
There's nothing wrong with bliss or happiness.

157
00:11:02,000 --> 00:11:11,000
The problem is we become caught up in it.

158
00:11:11,000 --> 00:11:13,000
A meditator isn't mindful.

159
00:11:13,000 --> 00:11:15,000
It doesn't say to themselves,

160
00:11:15,000 --> 00:11:17,000
happy, happy.

161
00:11:17,000 --> 00:11:20,000
Or if they feel calm,

162
00:11:20,000 --> 00:11:25,000
equanimous, and they don't say calm, calm.

163
00:11:25,000 --> 00:11:28,000
And they'll get caught up in it.

164
00:11:28,000 --> 00:11:31,000
And then they can get lost in it.

165
00:11:31,000 --> 00:11:37,000
And they won't progress.

166
00:11:37,000 --> 00:11:39,000
I knew among once,

167
00:11:39,000 --> 00:11:40,000
I still know him,

168
00:11:40,000 --> 00:11:42,000
but I haven't seen him in a long time,

169
00:11:42,000 --> 00:11:45,000
totally and unabashedly,

170
00:11:45,000 --> 00:11:49,000
unashamedly addicted to happiness.

171
00:11:49,000 --> 00:11:54,000
And he'd find anywhere he ended up getting a

172
00:11:54,000 --> 00:11:55,000
meditator.

173
00:11:55,000 --> 00:11:58,000
I'm not ended up when I first met him.

174
00:11:58,000 --> 00:12:03,000
He had this CD player

175
00:12:03,000 --> 00:12:06,000
and he would play Pink Floyd or something.

176
00:12:06,000 --> 00:12:07,000
When he meditated,

177
00:12:07,000 --> 00:12:09,000
he would play some very,

178
00:12:09,000 --> 00:12:11,000
all sorts of different kinds of music

179
00:12:11,000 --> 00:12:12,000
when they think,

180
00:12:12,000 --> 00:12:14,000
which is not really allowed for monks.

181
00:12:14,000 --> 00:12:16,000
He'd put on these big headphones

182
00:12:16,000 --> 00:12:18,000
and he'd just lie there and meditate

183
00:12:18,000 --> 00:12:19,000
or sit there.

184
00:12:19,000 --> 00:12:20,000
He's actually a very strong meditator.

185
00:12:20,000 --> 00:12:25,000
This guy could sit rigid for a huge smile

186
00:12:25,000 --> 00:12:30,000
on his face for hours.

187
00:12:30,000 --> 00:12:36,000
I'm not too sure he got into insight.

188
00:12:36,000 --> 00:12:40,000
Nowadays he does yoga, I think.

189
00:12:40,000 --> 00:12:57,000
So I don't know.

190
00:12:57,000 --> 00:12:59,000
And then we have nana,

191
00:12:59,000 --> 00:13:01,000
nana I think comes earlier,

192
00:13:01,000 --> 00:13:04,000
but nana is knowledge.

193
00:13:04,000 --> 00:13:05,000
This is a common one.

194
00:13:05,000 --> 00:13:07,000
It's not usually that difficult

195
00:13:07,000 --> 00:13:08,000
for meditators,

196
00:13:08,000 --> 00:13:10,000
but it can be distracting nonetheless.

197
00:13:10,000 --> 00:13:13,000
Nana means knowledge about

198
00:13:13,000 --> 00:13:14,000
any number of things.

199
00:13:14,000 --> 00:13:16,000
It can be worldly knowledge.

200
00:13:16,000 --> 00:13:19,000
You're able to figure out all your problems in life,

201
00:13:19,000 --> 00:13:22,000
come up with all sorts of plans and solutions.

202
00:13:22,000 --> 00:13:26,000
And one guy who came up with a perfect business plan,

203
00:13:26,000 --> 00:13:28,000
he started doing meditation course

204
00:13:28,000 --> 00:13:32,000
and he just said he'd come up with the perfect business plan

205
00:13:32,000 --> 00:13:34,000
and he said in five years,

206
00:13:34,000 --> 00:13:36,000
he was sure to be a millionaire

207
00:13:36,000 --> 00:13:39,000
or billionaire, whatever millionaire I guess.

208
00:13:39,000 --> 00:13:42,000
And he said in five years I'll come back

209
00:13:42,000 --> 00:13:44,000
and I'll buy you,

210
00:13:44,000 --> 00:13:46,000
I'll build you all these,

211
00:13:46,000 --> 00:13:48,000
build you a meditation,

212
00:13:48,000 --> 00:13:49,000
you know what I think?

213
00:13:49,000 --> 00:13:50,000
Made me all these promises

214
00:13:50,000 --> 00:13:52,000
and I tried to keep them,

215
00:13:52,000 --> 00:13:53,000
tried to get them to stay

216
00:13:53,000 --> 00:13:56,000
and you left and I never saw them again.

217
00:13:56,000 --> 00:13:59,000
I'm pretty sure he never became a millionaire.

218
00:13:59,000 --> 00:14:01,000
I love he didn't, he's listening out there.

219
00:14:01,000 --> 00:14:04,000
You owe me some good teeth.

220
00:14:04,000 --> 00:14:09,000
And that's not likely.

221
00:14:09,000 --> 00:14:11,000
You'll get caught up.

222
00:14:11,000 --> 00:14:13,000
It's very easy to get caught up in the mind

223
00:14:13,000 --> 00:14:18,000
because you're all alone and reality,

224
00:14:18,000 --> 00:14:25,000
you get caught, you get pulled away from reality.

225
00:14:25,000 --> 00:14:27,000
You end up in fantasy,

226
00:14:27,000 --> 00:14:29,000
solving all your problems,

227
00:14:29,000 --> 00:14:33,000
only to find that the solutions don't pan out in reality.

228
00:14:33,000 --> 00:14:37,000
But whether they do or they don't

229
00:14:37,000 --> 00:14:38,000
is not the point there,

230
00:14:38,000 --> 00:14:40,000
this is not the practice.

231
00:14:40,000 --> 00:14:43,000
This gets you away from the practice.

232
00:14:58,000 --> 00:15:02,000
And then we have

233
00:15:02,000 --> 00:15:07,000
Bhagahat,

234
00:15:07,000 --> 00:15:09,000
which is energy.

235
00:15:09,000 --> 00:15:11,000
Bhagahat means very strong energy.

236
00:15:11,000 --> 00:15:14,000
Bhagahat is a good word.

237
00:15:14,000 --> 00:15:18,000
Very strong energy.

238
00:15:18,000 --> 00:15:21,000
This one comes in various degrees.

239
00:15:21,000 --> 00:15:22,000
I mean these things,

240
00:15:22,000 --> 00:15:26,000
many of them are only a problem for certain people.

241
00:15:26,000 --> 00:15:28,000
I always tell this story about this woman

242
00:15:28,000 --> 00:15:30,000
who just couldn't stop herself.

243
00:15:30,000 --> 00:15:33,000
She just got up and just started walking,

244
00:15:33,000 --> 00:15:35,000
rapidly walking around the monastery,

245
00:15:35,000 --> 00:15:37,000
around and around and around.

246
00:15:37,000 --> 00:15:38,000
She couldn't stop herself.

247
00:15:38,000 --> 00:15:41,000
She's just so much energy.

248
00:15:41,000 --> 00:15:44,000
That's maybe the extreme case.

249
00:15:44,000 --> 00:15:47,000
But it is common for people to have

250
00:15:47,000 --> 00:15:50,000
bouts of intense energy,

251
00:15:50,000 --> 00:15:53,000
feeling like they can practice all day and all night.

252
00:15:53,000 --> 00:15:55,000
Just not a bad thing.

253
00:15:55,000 --> 00:15:57,000
It's not a bad thing to have good energy.

254
00:15:57,000 --> 00:16:00,000
I said, we need to come a deity through effort.

255
00:16:00,000 --> 00:16:02,000
It will become suffering,

256
00:16:02,000 --> 00:16:04,000
but that effort has to be well-directed.

257
00:16:04,000 --> 00:16:06,000
If you get caught up in the energy

258
00:16:06,000 --> 00:16:09,000
and just be pleased,

259
00:16:09,000 --> 00:16:14,000
if you're pleased by the energy.

260
00:16:14,000 --> 00:16:16,000
And you get lost in it.

261
00:16:16,000 --> 00:16:18,000
In the track of your practice

262
00:16:18,000 --> 00:16:25,000
and you just sit there energetic and all.

263
00:16:25,000 --> 00:16:27,000
And you have Adimokha, which is sadha,

264
00:16:27,000 --> 00:16:32,000
which is confidence or faith.

265
00:16:32,000 --> 00:16:34,000
This is a common one.

266
00:16:34,000 --> 00:16:36,000
A meditator will come and tell you

267
00:16:36,000 --> 00:16:39,000
that they're just one meditator today

268
00:16:39,000 --> 00:16:41,000
come and tell me the very impressed

269
00:16:41,000 --> 00:16:43,000
with the practice and how great it is.

270
00:16:43,000 --> 00:16:45,000
Some people, the common one here

271
00:16:45,000 --> 00:16:51,000
is a meditator will start to make plans

272
00:16:51,000 --> 00:16:54,000
to ordain or leave home.

273
00:16:54,000 --> 00:16:57,000
They'll start thinking about family and friends

274
00:16:57,000 --> 00:17:01,000
and wanting to spread the teaching to them.

275
00:17:01,000 --> 00:17:03,000
This one's common.

276
00:17:03,000 --> 00:17:06,000
When you get to this certain stage,

277
00:17:06,000 --> 00:17:09,000
these things arise for the meditator.

278
00:17:09,000 --> 00:17:11,000
A big one is confidence.

279
00:17:11,000 --> 00:17:18,000
They get really caught up in this great faith,

280
00:17:18,000 --> 00:17:19,000
not even faith,

281
00:17:19,000 --> 00:17:21,000
but perfect confidence

282
00:17:21,000 --> 00:17:23,000
because of the results that they've seen.

283
00:17:23,000 --> 00:17:26,000
Even though it's only preliminary results,

284
00:17:26,000 --> 00:17:28,000
they gain great confidence.

285
00:17:28,000 --> 00:17:32,000
Get caught up in that.

286
00:17:32,000 --> 00:17:34,000
During that time, they get caught up

287
00:17:34,000 --> 00:17:36,000
and you get over confident

288
00:17:36,000 --> 00:17:38,000
and then you start meditating.

289
00:17:38,000 --> 00:17:40,000
You're sure, very sure of yourself

290
00:17:40,000 --> 00:17:43,000
and so you fail to be careful

291
00:17:43,000 --> 00:17:47,000
and meticulous about the practice.

292
00:17:47,000 --> 00:17:54,000
It actually takes away from your practice.

293
00:17:54,000 --> 00:17:58,000
The upper tongue is a little bit interesting.

294
00:17:58,000 --> 00:18:00,000
The upper tongue actually means mindfulness

295
00:18:00,000 --> 00:18:03,000
which you would think would be a good one,

296
00:18:03,000 --> 00:18:06,000
but this will be the first taste

297
00:18:06,000 --> 00:18:07,000
that the meditator has

298
00:18:07,000 --> 00:18:09,000
of being really and truly mindful.

299
00:18:09,000 --> 00:18:12,000
It'll be easy for them to get caught up in that

300
00:18:12,000 --> 00:18:17,000
sense of

301
00:18:17,000 --> 00:18:20,000
thinking suddenly that they're perfectly mindful

302
00:18:20,000 --> 00:18:23,000
and thus losing

303
00:18:23,000 --> 00:18:27,000
giving up the effort or failing

304
00:18:27,000 --> 00:18:30,000
to be again meticulous and careful.

305
00:18:30,000 --> 00:18:33,000
It can also refer potentially

306
00:18:33,000 --> 00:18:37,000
to being mindful about the past and the future.

307
00:18:37,000 --> 00:18:39,000
Some meditators will have memories

308
00:18:39,000 --> 00:18:40,000
about the past,

309
00:18:40,000 --> 00:18:41,000
even remembering past lives,

310
00:18:41,000 --> 00:18:43,000
but mostly about early childhood

311
00:18:43,000 --> 00:18:44,000
and that kind of thing.

312
00:18:44,000 --> 00:18:46,000
Memories that I thought were buried

313
00:18:46,000 --> 00:18:49,000
and were gone actually

314
00:18:49,000 --> 00:18:52,000
didn't realize they still kept around.

315
00:18:52,000 --> 00:18:54,000
Or again, it'll be planning

316
00:18:54,000 --> 00:18:55,000
about the future,

317
00:18:55,000 --> 00:18:57,000
making plans and just this really

318
00:18:57,000 --> 00:19:01,000
sharp ability to construct plans

319
00:19:01,000 --> 00:19:17,000
or memories about the past.

320
00:19:17,000 --> 00:19:19,000
And the last one is nikanti.

321
00:19:19,000 --> 00:19:22,000
Nikanti is where you get

322
00:19:22,000 --> 00:19:25,000
nikanti means like desire.

323
00:19:25,000 --> 00:19:29,000
It's when you like something.

324
00:19:29,000 --> 00:19:31,000
There's a very subtle sense of liking.

325
00:19:31,000 --> 00:19:33,000
Liking can be any of the other nine

326
00:19:33,000 --> 00:19:35,000
or pakile so but can also be

327
00:19:35,000 --> 00:19:37,000
something you see or hear

328
00:19:37,000 --> 00:19:41,000
smell or taste or feel or think.

329
00:19:41,000 --> 00:19:43,000
Nikanti, because the meditator

330
00:19:43,000 --> 00:19:45,000
is suddenly happy and so they have

331
00:19:45,000 --> 00:19:48,000
this subtle desire or liking

332
00:19:48,000 --> 00:19:51,000
of their situation.

333
00:19:51,000 --> 00:19:54,000
Because it's subtle, they may not note it.

334
00:19:54,000 --> 00:19:58,000
They may go with it and get lost in it.

335
00:19:58,000 --> 00:20:01,000
Nikanti is subtle desire.

336
00:20:01,000 --> 00:20:03,000
If you're very hard to see

337
00:20:03,000 --> 00:20:05,000
but if you're sitting there

338
00:20:05,000 --> 00:20:07,000
very pleased with yourself,

339
00:20:07,000 --> 00:20:09,000
very happy about your practice.

340
00:20:09,000 --> 00:20:11,000
You have to remember to note liking, liking.

341
00:20:11,000 --> 00:20:13,000
It's a common one at this stage.

342
00:20:13,000 --> 00:20:15,000
It's not liking something strongly

343
00:20:15,000 --> 00:20:18,000
but it's a liking meditation

344
00:20:18,000 --> 00:20:20,000
feeling happy about your practice

345
00:20:20,000 --> 00:20:23,000
and enjoying the practice.

346
00:20:23,000 --> 00:20:26,000
Because it takes away the

347
00:20:26,000 --> 00:20:28,000
sharp edge of mindfulness,

348
00:20:28,000 --> 00:20:30,000
the clarity.

349
00:20:30,000 --> 00:20:32,000
You start to get lazy

350
00:20:32,000 --> 00:20:36,000
and you start to get complacent

351
00:20:36,000 --> 00:20:40,000
and you lose the present moment.

352
00:20:40,000 --> 00:20:42,000
So for all of these,

353
00:20:42,000 --> 00:20:46,000
they're not an obstacle

354
00:20:46,000 --> 00:20:49,000
but they're a distraction

355
00:20:49,000 --> 00:20:51,000
and they have the potential

356
00:20:51,000 --> 00:20:52,000
to distract the meditator.

357
00:20:52,000 --> 00:20:53,000
It gets with everything,

358
00:20:53,000 --> 00:20:54,000
the bad things, the good things,

359
00:20:54,000 --> 00:20:56,000
the good things, it's all up to your mind,

360
00:20:56,000 --> 00:20:58,000
how you interact with them,

361
00:20:58,000 --> 00:21:03,000
how you respond to them.

362
00:21:03,000 --> 00:21:06,000
Meditation is about

363
00:21:06,000 --> 00:21:09,000
changing the way we look at things,

364
00:21:09,000 --> 00:21:11,000
changing us,

365
00:21:11,000 --> 00:21:14,000
changing the core of who we are.

366
00:21:14,000 --> 00:21:16,000
I was thinking,

367
00:21:16,000 --> 00:21:18,000
trying to think of an analogy

368
00:21:18,000 --> 00:21:19,000
and it made me think of something

369
00:21:19,000 --> 00:21:21,000
and at first I couldn't think of what it was

370
00:21:21,000 --> 00:21:23,000
but then I realized it was the Borg.

371
00:21:23,000 --> 00:21:26,000
There was this show,

372
00:21:26,000 --> 00:21:27,000
probably some of you heard of it

373
00:21:27,000 --> 00:21:29,000
called Star Trek

374
00:21:29,000 --> 00:21:31,000
and they had several versions of it

375
00:21:31,000 --> 00:21:32,000
but in one version,

376
00:21:32,000 --> 00:21:34,000
there were these group of beings

377
00:21:34,000 --> 00:21:35,000
called the Borg

378
00:21:35,000 --> 00:21:37,000
and they would assimilate

379
00:21:37,000 --> 00:21:39,000
all the other beings.

380
00:21:39,000 --> 00:21:42,000
That's what I was thinking of.

381
00:21:42,000 --> 00:21:44,000
Meditation is kind of like

382
00:21:44,000 --> 00:21:46,000
brainwashing or assimilating.

383
00:21:46,000 --> 00:21:49,000
It's kind of like a parasite.

384
00:21:49,000 --> 00:21:50,000
You have this host

385
00:21:50,000 --> 00:21:52,000
and this host has all sorts

386
00:21:52,000 --> 00:21:56,000
of as a real personality.

387
00:21:56,000 --> 00:21:58,000
How it relates is because

388
00:21:58,000 --> 00:22:00,000
we're not interested in the personality.

389
00:22:00,000 --> 00:22:03,000
We're not interested in your experiences.

390
00:22:03,000 --> 00:22:06,000
It's not like we're going to totally eradicate them.

391
00:22:06,000 --> 00:22:09,000
But meditation is a whole new aspect

392
00:22:09,000 --> 00:22:11,000
of who you are.

393
00:22:11,000 --> 00:22:12,000
It's creating habits

394
00:22:12,000 --> 00:22:16,000
that you may never have developed before.

395
00:22:16,000 --> 00:22:17,000
So in a sense,

396
00:22:17,000 --> 00:22:21,000
it's creating a new entity

397
00:22:21,000 --> 00:22:25,000
in the host

398
00:22:25,000 --> 00:22:27,000
and so everyone comes to meditation

399
00:22:27,000 --> 00:22:30,000
with quite different

400
00:22:30,000 --> 00:22:32,000
characters and personalities.

401
00:22:32,000 --> 00:22:36,000
None of that's all that important.

402
00:22:36,000 --> 00:22:39,000
It's not going to change

403
00:22:39,000 --> 00:22:41,000
completely or disappear completely

404
00:22:41,000 --> 00:22:44,000
but we're using this host

405
00:22:44,000 --> 00:22:48,000
like planting a seed in the earth

406
00:22:48,000 --> 00:22:50,000
but the seed is something quite different.

407
00:22:50,000 --> 00:22:52,000
A seed of meditation

408
00:22:52,000 --> 00:22:56,000
for many people is something quite new.

409
00:22:56,000 --> 00:23:01,000
And so you end up assimilated in a sense.

410
00:23:01,000 --> 00:23:04,000
Everyone will still look different

411
00:23:04,000 --> 00:23:05,000
speak different active

412
00:23:05,000 --> 00:23:08,000
but we have something in common.

413
00:23:08,000 --> 00:23:11,000
And that's all we're looking for.

414
00:23:11,000 --> 00:23:13,000
All of these different experiences

415
00:23:13,000 --> 00:23:14,000
one might have

416
00:23:14,000 --> 00:23:17,000
because every meditator is different.

417
00:23:17,000 --> 00:23:19,000
You see a thousand meditators

418
00:23:19,000 --> 00:23:21,000
you'll have a thousand different conditions.

419
00:23:21,000 --> 00:23:24,000
Everybody's a little bit different.

420
00:23:24,000 --> 00:23:27,000
And none of that's really all that important

421
00:23:27,000 --> 00:23:29,000
in the meditation practice.

422
00:23:29,000 --> 00:23:30,000
So as a meditation teacher,

423
00:23:30,000 --> 00:23:32,000
you're not really concerned

424
00:23:32,000 --> 00:23:35,000
with any of the particulars of the individual.

425
00:23:35,000 --> 00:23:38,000
You're concerned with creating a seed.

426
00:23:38,000 --> 00:23:40,000
Planting a seed.

427
00:23:40,000 --> 00:23:49,000
And growing and cultivating this seed,

428
00:23:49,000 --> 00:23:51,000
which is the seed of mindfulness,

429
00:23:51,000 --> 00:23:54,000
the seed of insight.

430
00:23:54,000 --> 00:23:58,000
And that's as meditator is how we should

431
00:23:58,000 --> 00:24:01,000
approach ourselves

432
00:24:01,000 --> 00:24:02,000
or approach our experience.

433
00:24:02,000 --> 00:24:04,000
Do not get sidetracked

434
00:24:04,000 --> 00:24:06,000
by specific experiences

435
00:24:06,000 --> 00:24:09,000
or thoughts or character types.

436
00:24:09,000 --> 00:24:13,000
Do not let our personality take over.

437
00:24:13,000 --> 00:24:14,000
There's a personality.

438
00:24:14,000 --> 00:24:16,000
It can be good and it can be bad,

439
00:24:16,000 --> 00:24:17,000
but it's always,

440
00:24:17,000 --> 00:24:20,000
if we've never practiced insight meditation,

441
00:24:20,000 --> 00:24:22,000
it's always made up of

442
00:24:22,000 --> 00:24:26,000
worldly half-truths.

443
00:24:26,000 --> 00:24:29,000
No matter how good and useful it might be,

444
00:24:29,000 --> 00:24:31,000
practically speaking,

445
00:24:31,000 --> 00:24:33,000
it can never truly be in touch

446
00:24:33,000 --> 00:24:36,000
with reality until we cultivate

447
00:24:36,000 --> 00:24:40,000
this practice of mindfulness,

448
00:24:40,000 --> 00:24:46,000
where we start to look perfectly objectively at thing.

449
00:24:46,000 --> 00:24:49,000
I'm just trying to explain

450
00:24:49,000 --> 00:24:53,000
the key here is

451
00:24:53,000 --> 00:24:57,000
not get caught up into who you are.

452
00:24:57,000 --> 00:24:59,000
Not get caught up in your views,

453
00:24:59,000 --> 00:25:00,000
your opinions, your emotions,

454
00:25:00,000 --> 00:25:04,000
your conceits, your identifications,

455
00:25:04,000 --> 00:25:07,000
not get caught up in your experiences,

456
00:25:07,000 --> 00:25:15,000
not get caught up in any of the good things that come.

457
00:25:15,000 --> 00:25:17,000
I'm trying to be objective

458
00:25:17,000 --> 00:25:22,000
and have a pure state of mind

459
00:25:22,000 --> 00:25:26,000
during the practice.

460
00:25:26,000 --> 00:25:28,000
So there you go.

461
00:25:28,000 --> 00:25:30,000
Some caution, some direction.

462
00:25:30,000 --> 00:25:32,000
I think that's one of the most important aspects

463
00:25:32,000 --> 00:25:35,000
of the practice is providing direction

464
00:25:35,000 --> 00:25:39,000
and correction

465
00:25:39,000 --> 00:25:42,000
when we start to get off track.

466
00:25:42,000 --> 00:25:47,000
So there you go.

467
00:25:47,000 --> 00:25:48,000
If there are any questions,

468
00:25:48,000 --> 00:26:10,000
I'm happy to answer them now.

469
00:26:10,000 --> 00:26:35,000
I'm happy to answer them now.

470
00:26:35,000 --> 00:27:00,000
I'm happy to answer them now.

471
00:27:00,000 --> 00:27:25,000
I'm happy to answer them now.

472
00:27:25,000 --> 00:27:50,000
Do we have a full host tonight?

473
00:27:50,000 --> 00:27:56,000
There's probably our fullest yet.

474
00:27:56,000 --> 00:28:00,000
Good turnout.

475
00:28:00,000 --> 00:28:01,000
All right.

476
00:28:01,000 --> 00:28:03,000
Well, if there are no questions,

477
00:28:03,000 --> 00:28:05,000
I wish you all a good night.

478
00:28:05,000 --> 00:28:28,000
Thank you for coming.

