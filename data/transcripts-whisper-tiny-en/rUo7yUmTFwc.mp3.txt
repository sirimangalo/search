Okay, good evening, welcome to the second, okay, good evening broadcasting live from
Stony Creek, Ontario, August 5th. Today's rather long quote is actually a story
that the Buddha told. Funny thing about this story is it's not clear where it came from,
it seems to have been passed down and used by many teachers from different religions,
or from different parts of India, different teachers have, this is a popped up in different places,
so it may not have been the Buddha who originally said it. It's an apt
allegory, I think it's what you call it. It's like he saw an ace ops favor of the
blind men in the elephant. In fact, maybe it is found in ace ops as well, I'm not sure. But you
can look it up on Wikipedia, there's interesting background about it. Anyway, it relates to Buddhism,
quite apt. We liken the situation that we find in the religious world to the idea of a blind
men in the elephant, blindness and the elephant. The interesting thing, there's two interesting
points in this story. The first is the blindness. How blindness leads to thinking something is
other than what it is. Blindness is what, of course, it creates confusion. But the other part of
the story is the idea of them getting angry at each other. That's not a physical blindness
doesn't necessarily make people angry individuals, doesn't make people fight. But why this
allegory is actually more apt is because blindness and Buddhism, the blindness of
defilements, the blindness of ignorance, blindness of true, wrong view and of Egypt is actually what
leads to getting angry. Just as these men get frustrated because they all have a different
idea of things. It's very much an interesting point that our blindness leads us to desire and
to aversion, leads us to fight with each other simply because we're blind. Because we don't think
you don't see things. If you think about it, as one way of explaining this concept and Buddhism
or the idea of the importance of wisdom and Buddhism is if they all saw the elephant clearly,
there would be no reason to fight. Likewise, if all of us understood things exactly as they were,
there would be no or we would lose this reason to fight. Our anger would disappear. It's a way of
understanding the dependence of anger on ignorance. Because when you understand things, when you see
things clearly, you have no reason to fight with anyone. All of our potential quarrels, potential
conflict disappears. If someone says, you're a buffalo, you're not a buffalo. And furthermore,
you know why they're saying they are a buffalo. If someone tries to manipulate you or to provoke
an angry response in you, you know, right away that this person is provoking an angry response.
More importantly, you know, the danger of getting angry back, you know what it's going to lead to,
you see, the more knowledge and understanding that we have, not necessarily knowledge,
but true knowledge, like really knowing something which comes from, from empirical understanding,
empirical observation. The less reason you have to get angry, the less reason you have to desire
things. As you see, desiring in desire is actually making me more frustrated, making me more angry
and so on. But this is something you find not just in, I mean, it's applicable to all
on all levels. This is something you find in lay life, right? Why we fight is because we have
all, we're all blind. Why we fight with our families, why we fight with our friends,
why we have enemies in the first place is completely due to blindness.
And just by seeing things clearly, this is the point of just by seeing things clearly,
could we do away with this, the quarrels, the arguments that we have.
But it's more prevalent in what he's talking about here,
the wonders of other sex when religious circles, even more prevalent, right? People will kill each
other because they believe in a different imaginary being. I believe in this image of this god.
I mean, by imaginary, I really do mean that because you may argue that it exists,
but even your argument that God exists is based on your imagination, even if he does it,
even if he issue it does exist because you don't have anything beyond your own imagination to go
by. This is why a lot of people give up things, a lot of theists and deists give up trying to
understand God because they say, well, you know, whatever I think it's completely based on
my own imagination, I can't look around and say, oh, God, there's a, you know, there's
a, something that tells me what God disliked. So it's an imaginary. And based on our idea of what
this being is, this entity is, we fight and we argue. So totally based on our, our lack of knowledge,
our lack of understanding. You'll see people fighting over God, you'll see people fighting over
beliefs and views. You see even see non-religious people fighting over their beliefs and views,
fighting over, over, we fight over ideas about, you know, we were fighting over whether slavery was
okay, whether women should have the right to vote, we're fighting over sexuality, we're fighting
over abortions. And we, it's, it's very difficult for us to, to agree. It's very difficult for us
to even ourselves know what is right. And so the best we get often is while I believe,
and which is dangerous, you know, often our belief is, is based somewhat on wisdom, understanding
and logic. But often it's emotional or arbitrary, I believe, can come to be a reason to believe,
you know, it just came to me and well, you know, whatever that's what I believe, without actually
thinking, well, it might be wrong and maybe I should change what I believe.
The way we've always done it, it's what we've always believed, etc. Not a good way to do things.
Blindness is very dangerous. You hurt yourself, you hurt other people, and you miss,
you miss, you miss conceive things. You can see what is, you also conceive what is not there,
in the case of God, for example, in many different ways. When you're blind, you might hear a sound
and think it's a lion or something.
Have you ever stepped on a rope and jumped because you thought it was a snake?
You know what I think, you know what I mean? You know what is meant here.
But you even find this within Buddhism. You'll find, we have a hard time agreeing with each other.
And this is one reason why we like to go by the texts and like to have an orthodoxy.
It's dangerous because orthodoxy can, can be wrong and can lead everyone in the wrong direction.
But there's often a sense, people sometimes have a sense that orthodoxy in and of itself is wrong,
and they don't like the idea of having one view, and that's much more dangerous.
Because this is the difference between the truth and imagination. The truth is singular.
There's only one reality. There's only one truth, but illusion is manifold.
So either we either we all agree on the same lie, all believe the same lie,
or we all believe the truth. Once you believe the truth, it's much easier than getting everyone to believe the same lie.
And maybe not, actually.
You can, you can get a lot of people to believe the same lie, but no, not really,
because eventually people have different ideas and there's, that's where arguments break out.
And if you focus, what I mean to say is if you focus on the truth, it's easier in the sense that there's only one truth.
So once everyone's, if you get people looking at the truth, you don't have to worry about what's truth.
This is why meditation is so adaptable. You just say, look, you know, you don't have to say,
become Buddhist, wear these, these funny clothes or this funny hat or it's on,
there's no specifics to it. It's very general and it works for everyone, because truth,
because truth is singular. You don't have to believe. This is a key sign of what is true
in a, it's not, it's not specific to any group. It's, and it's approachable by anyone.
Truth is what you can find by yourself.
Anyway, so some thoughts on this.
I don't have much more to say anyone have any questions.
Right. So our practice is getting to the truth. And it was thinking about something,
the idea of the difference with this simile and reality is that these guys are stuck being
blind. They're going to be blind who they're light their whole lives. We're not that way.
But you think how impossible it is for a blind person to come to see.
It's easier to become spiritually, and whatever the opposite of blind is,
see, see, but at least it's possible. It's possible for someone who's blind to come to see.
And once it was blind, and now I see this is, this is how religious people describe it.
And they come to experience something that they hadn't experienced before.
But it's a very apt comparison. Practically speaking, you experience this.
You start to do meditation and you just realize you don't go anywhere or become anything.
You just realize how amazing it is that a person, no, I'm the same person as I was
before. But the difference between me when I was blind, and me when I can see, it's just like
having your eyes closed versus having your eyes always a total difference.
How important sight is, it's even more profound, the difference between a non-meditator and a
meditator or someone who doesn't understand them, there's themselves, doesn't understand the
workings of the life, and one who has come to understand the workings of the life.
Okay, some questions.
In Woodway does not eating past noon, aid one in their practice, and in their meditation
specifically. Well, there's nothing about afternoon. A lot of the rules are just
convention. It's like a framework. Why does a car have to be this shape? Why can't it be a truck?
But the only eating once is much more important because it's the bare minimum.
It's the renunciation of any kind of attachment in the body. So the idea is you need
just this much food per day, and you do need to eat. So eating once a day
is enough, and any more than that, because eating more than that or eating in a more complicated
fashion would most likely give rise to further attachment, craving, desire, etc. This is a preferable
way. Eating once a day has quite effective in curing you a lot of attachment to the body.
And they say it. I mean, if you've ever been on a meditation retreat, where they have milk in
the evening, or where you eat in the evening, it's very difficult to meditate when you
start to feel drowsy.
To access an absorption concentration as they are understood in terms of the genus,
develop during the practice of vivasana. Again, not during the practice of vivasana, but during
the time when you think that you're practicing vivasana, you could actually be practicing
samata, and they could also arise based on past practice of samata, either in this life or
in the past life. They can rise spontaneously, but easily. You can step into them easily,
depending on your past practices.
Walking meditation briskly to help with sleepiness, sloth, torpor. They had works. I did
it once. I've got a story for this one. When I was in John Tang Thailand, there was a big monk,
this big, fairly fat, but also just big monk, who was a forest monk. He would walk barefoot,
wore these thick robes that I'm wearing right now, but in Thailand, you know, and had a vest
full of buddhas. He had like a hundred pockets in each one had a buddha in it, and at his waist,
he had his mother's ashes in the... I mean, this was... this guy was hardcore. Still one of my
favorite people. Like a character. Like just one of those people who was out of a book, who
you think, you know, he's not just any old monk. That's him, and he's iconic.
And then there was this other monk. Oh, I've got stories to tell. This other monk,
who was actually a new monk, whose parents were part of the Thai mafia, and he... I just
had a totally unrelated story, but he at one point was showing me pictures of how he went
he disrobed, because it was just a temporary ordination. He was going to start growing opium, I
think, or cocaine. I can't remember. The other one he had pictures, and he was pointing me out,
who was who, like, this guy was a... this guy was the gun for the gun, or the guy who kills people,
and this guy is the... whatever the head of the family, and so... anyway. So the three of us,
it's like a joke. He told... the other monk told me, hey, this
a gen Yud was... every night he goes walking up in the monastery, and I went with him last night,
and I was like, oh, that's interesting. Well, I'll come and see. And so that night, I went up with them,
and up in the front of the monastery, they put sand out, the lay people when they come to the
monastery, and it's a tradition to put sand out. The theory is, and it's an ancient, an old tradition,
that when you walk to the monastery, when you leave the monastery, you carry mud out,
and so with all these people coming and going, the monastery actually loses something. I mean,
the... as I've heard it, the idea is, you're stealing from the monastery, but I think it could
possibly just be... with lots of people coming, and it gets run down against the area, and the
olden days, the area and the monastery would get, and have this problem. So at the front of the
monastery, they have the sandy area, or a dirt area, and lay people every year who will bring sand,
or sometimes I think maybe every... every special day. So there's just... in that time,
there was a big area, and now they've closed it in a bit, and there's a big area of sand,
probably 50 meters, something, or I don't know, distance wise. Yeah, maybe 50 meters.
And so this is what we did, we walked. Quickly, 50 meters there and back there and back,
and we did this from like nine to three, or nine to two, maybe. I think we took a break,
but then nine p.m. or ten p.m. to two, or so they... there's probably three or three or four hours,
if I can, or I think about three or four hours. And it felt great. It was awesome.
It felt energetic, didn't need to sleep, it did end up going to sleep, I think, around two,
and I was excited about it. I thought, this is great. And I went to see our gentong.
And I told them about it. I said, yeah, at night we're going walking. Many looked at me.
And he said, I think he laughed about it, but he said, that's... that's the foreign way,
the Western way. Ex... he used the word exercise. He said,
I'm bad for long. We have exercise and he has the English word.
And then he got... he got rather serious and he said, the... the walking meditation in Buddhism,
the Buddha's way of walking meditation, you have to go slowly. You have to go very slowly.
You can't walk like that. The point is to understand... the point is actually not walking.
It's to... to experience this clearly. And then this clearly. And then this clearly.
This is what is about one thing, one movement. So our practice of walking meditation isn't about
walking at all. I've done it with people with kids in a room. Too many kids in the room. I didn't
want them to have to walk around. And I just had them lift their feet and put them down.
Same thing. There's nothing to do with walking. We're not walking. We're experiencing motion
clearly. And so if you don't do it slowly, there's nothing like meditation. Obviously what we were
doing was even worse. We were chatting. We were talking all night. So absolutely not meditation.
But you know, it's easy to get these ideas. So good walking briskly, be helpful. Sure, it's
probably helpful for the body to some extent. But it's not conducive to meditation. Not in a way
that we're looking at meditation. So we've got stack exchange questions.
You've got a bunch of answers already.
I've been dealing with negative mind states in my practice. When it gets overwhelming,
I lose my sense of mindfulness and get lost in the middle of all the noise. And I guess I identify
with it. Otherwise, I wouldn't feel pain when it happens. I mean, physical pain or mental pain.
Is it a good idea to bring myself back to the breath? Or should I face this? I usually try to
accept the experiences, experience in the moment, but it usually brings more pain because I can't
see clearly what I'm going through. Yeah, it can be a good idea to bring yourself back. These
kind of questions, there's no one answer. Meditation is a lot like boxing or kung fu or something.
It's a lot about improvising. And not one answer. Not one answer is going to solve the problem.
Not one answer is going to work all the time. So get that idea clear. Don't pick one solution.
Go back to the breath, watch the stomach rising and falling, and then attack it again.
Upside, upset, overwhelmed, overwhelmed, just remind yourself. And then it just becomes too
much. And then you can try something else. One way of doing the kung fu thing is lie down instead.
And then you feel stressed. So lie down. Do it lying meditation on your back rising.
You might fall asleep. And when you wake up, you're able to do, you have a clear mind and you're
able to do meditation. Or you might have a good meditation lying down. You might have just the
perfect balance or get up and walk. Changing posture is important. Sometimes there's little
things you can do like changing your diet, eating the right food, drinking lots of water.
I mean, these are little tricks. I don't even bring them up because just to point out that there's
it's like guerrilla warfare kind of, or like we're in a war. There's no blueprint for how it's
going to go. It's chaotic. In the end, you're going to be, it's like being in a war. You're fighting
for your life. Many people are in the state of such trauma and such immense mental pain that
it's like being in war. And if you think of it as being a war, it's a lot easier. So a lot more
dealable. You become a soldier. You start fighting. But you don't just run into battle or expect
to be able to blow everything up with a nuclear bomb. You've got to be clever, or you're going to
get overwhelmed. So take the techniques that you've learned, try to apply them, but don't expect
it to be form a leg. It's going to be chaotic, and you're going to have to improvise.
He said, we have to do this many minutes walking and sitting. I can't do some walking,
nothing just a sitting. I can't do a sitting. We had one month. I don't know if I mentioned this. I
said, he came to our teacher and he said, he was the worst trouble ever. He ended up setting himself
on fire and disrupting in the end. He went totally crazy. I took, I spent, now I took him to help
take him to the mental hospital. We got quite, we had quite a good relationship or familiar
relationship with the mental hospital and changed my, not when I was a teacher. I never had a
problem with my students. I've never had a student go crazy, but I've seen some craziness.
Anyway, he came to the teacher or teacher once, and he said, Adjana can't sit. He sits, and he
starts to stiffen up or something like that. Adjana said, well, then do walking. I said, I can't
walk. Then do walking. There's some other reason I couldn't do. I said, well, then lie down. I'll know
when I lie down. I suddenly find myself going, and Adjana said, well, then stand.
And then they got him. As he said, oh, yeah, I can stand. So changing the posture is useful.
A version and fear about being mindful about this, this is the, this is the, the cleverness.
We monks that you need to be clever in your meditation. It's not a break that you're going to
break with a hammer. A hammer doesn't work. So when you have, you have to be clever and ask yourself,
what's the essence of this? In this case, you have a version and fear, focus on the aversion,
fear, forget about the pain and whatever. Now you're afraid and you're, you have a version or
something. Focus on that. Maybe you feel guilty because you're not mindful. Focus on the guilt,
that feeling. Focus on whatever they're right then. That's the one formula that you can follow.
The problem is that it's, it's tricky because it changes in the nature of the experience changes.
So I'm getting yourself in a position where you can be mindful of it. That's the key.
And you have to keep yourself. You have to keep pulling yourself back to this moment, this present
moment. And your mind's going to keep slipping away and you have to just keep bringing it back.
But there's no place of mindfulness. There's no plane, sorry, plane of mindfulness.
You know, state of plane, plane, I think is about plane wrong.
But you can't have a state of pure mindfulness. Your mindfulness is now, it's momentary.
Once you've been mindful, that's gone. You only have the next moment, whether you can be mindful in
that moment or not.
Mindfulness is a moment by moment.
The goal of meditation is to see reality of nature is uncontrollable and permanent and unsatisfactory.
The goal of meditation is to banise freedom.
Seeing and permanent suffering in non-self is the path or it's what the, yeah, it's the path.
How about walking meditation? I'm going for arms then.
Yeah, it's not meditation, but walking meditation, arms around shouldn't be meditated, but then
it's just walking, walking, okay. It's not as powerful, not as effective.
I mean, can you become enlightened running? Yes, sure. You can. Is it easy? No.
It's very difficult to do it to become enlightened as a jogger. I mean, I suppose if you were jogging
and you kept it up eventually, you could get into a point where it was kind of meditative.
But still, it's nothing, nothing like what we're doing. There's one movement because that's very
easy to be aware of the arising and ceasing of that movement.
Experience. Can the mind be located at two places in the body at the same time?
The mind has no location. The mind doesn't take up space. The mind is mental. It's not physical.
Space and position is a derived quality of the matter. The mind is not material, so it doesn't
take up space. The body doesn't exist. The space only exists as a derived quality of matter.
It doesn't really, it's not truly real. All that's real is experience.
This is great. It's nice having so many people come and having a platform where we can do it.
So we have YouTube, people can watch, we have 32 viewers,
and then we have our own site with little chat box.
Now what I'm just looking at now is how to restart my dot-series mongolow.
Does anyone remember my dot-series mongolow? It's been a while.
I don't want to call it my dot-series mongolow. It's a bit
crass, isn't it? Anybody got a better one? How about sangha dot-series mongolow?
The idea is to make the most easy way to explain it is make a Facebook clone.
But that's not exactly it. But it has the format like Facebook, and so that's why it's easy to
explain it like that.
Is that it wouldn't be a feed where people could post links and pictures and
make a post, updates, make a form groups,
I don't know events, maybe, I don't know,
but it did well, no, it was useful. But there wasn't as much of a group than the
reason I thought about bringing it back is because now we've got
seems a little bit more of a group, and this chat is a little bit too ephemeral
and simple, right? It's nice that it's nice and it's simplicity, but
limited.
Is it possible that we are reborn outside of earth and somewhere else in the universe?
I don't know.
I'm glad the YouTube live feed on to meditation. It's a little bit more complicated because
the link changes each time, so I have to manually input the link each day. I'd have to find
somewhere where I could some way to input the link. It's not a static link every day, so because
each YouTube video has its own link to it. I mean, nice, if there was a static link,
but there isn't.
Yeah, I don't think I'd import the meditation group into that platform.
Not at first anyway. I can see the benefit of that, but it is kind of
there are two different beasts.
Anyway, I want to just get it up first and see how it goes.
So I'm thinking about sangha.ceremungalow.org.
I know the word sangha is complicated because it's hard to spell, but
it's the best word I can think of. sangha, that's what it would be.
What do you all think? I think there's a delay.
I think by the time you hear what I said, it's been a few seconds already. I'm going on to talking
about something else.
And Robin tells me that the YouTube video is not as best as it could be. It's unfortunate.
We should investigate that. See whether it's every day or not.
The recording, once it's recorded it, apparently,
and the recording is not very well done or something.
Yeah, so I'll just do that. I was headed up as sangha.ceremungalow.org.
I can figure it out. It's already set up actually. If you go to sangha.ceremungalow.org,
you'll see a blank page. And you know why you'll see a blank page?
Because that blank page is our Facebook clone. But there's an error. And I don't know what
that error is yet. So the next step is to figure why our Facebook clone is not loading.
Now YouTube does have a 30-second delay. Whoa, that's pretty big.
So if you want to, if you want real time, if you want to want to do this real time,
better not to watch YouTube, better to listen, much more losses. That's surprising.
I'm surprised that YouTube has such a buffer. Well, I guess it kind of makes sense.
No, it doesn't because if we were having a hey, I don't know. Anyway.
10 minutes disappearing. Was that from yesterday?
We did indeed. Okay, I'm going to end the YouTube video anyway. That's the broadcast for tonight.
And I think we'll get on. I'm going to get on to figure out this.
So next time tomorrow, hopefully I'll have an update. I'm saying we've got a new sangha,
a new online sangha, along with our meditation page. There'll be somehow a connection with them.
Hopefully. Okay, so thanks for joining in. Peace.
