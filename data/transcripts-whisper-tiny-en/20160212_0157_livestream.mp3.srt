1
00:00:00,000 --> 00:00:12,000
Leaving everyone broadcasting live, February 11, 2016.

2
00:00:12,000 --> 00:00:20,000
I'm actually just coming on to say that we're going to be broadcasting tonight.

3
00:00:20,000 --> 00:00:31,000
Well, not substantially. Sorry, but tomorrow morning I have an exam.

4
00:00:31,000 --> 00:00:40,000
And I should study tonight.

5
00:00:40,000 --> 00:00:53,000
And so, yeah, just a little busy.

6
00:00:53,000 --> 00:00:59,000
But we have a nice quote today.

7
00:00:59,000 --> 00:01:08,000
With an interesting word, limited, talking about limited actions.

8
00:01:08,000 --> 00:01:21,000
I don't have the source here, but, well, if I understand correctly, this verse is on the power of love.

9
00:01:21,000 --> 00:01:29,000
It's from the Vatican, so it's not.

10
00:01:29,000 --> 00:01:35,000
It's not talking about deeply Buddhist teachings.

11
00:01:35,000 --> 00:01:38,000
Like, the power of love is great and humble.

12
00:01:38,000 --> 00:01:44,000
You could argue that it's not essential or it's not sufficient.

13
00:01:44,000 --> 00:01:51,000
Neither necessary nor sufficient. Does it help for the path?

14
00:01:51,000 --> 00:01:53,000
For sure.

15
00:01:53,000 --> 00:01:59,000
And so, when one dwells with love in one's heart,

16
00:01:59,000 --> 00:02:06,000
it helps to spell feelings of guilt.

17
00:02:06,000 --> 00:02:13,000
It helps to spell feelings of enmity or vengeance.

18
00:02:13,000 --> 00:02:18,000
It helps us do away with grudges or conflicts in our mind.

19
00:02:18,000 --> 00:02:25,000
So, we practice loving kindness sort of surprisingly for our own benefit.

20
00:02:25,000 --> 00:02:30,000
We're not really expecting that by sending love, we're going to help those people that we wish well of,

21
00:02:30,000 --> 00:02:33,000
not directly in it.

22
00:02:33,000 --> 00:02:42,000
Of course, there is a very important benefit for the people who are sending into a net.

23
00:02:42,000 --> 00:02:47,000
We are going to appreciate them more.

24
00:02:47,000 --> 00:03:03,000
We're less inclined to fight with them to conflict with these people.

25
00:03:03,000 --> 00:03:08,000
And so, it goes with these types of meditation.

26
00:03:08,000 --> 00:03:11,000
There's many different protected meditations.

27
00:03:11,000 --> 00:03:16,000
But it did spend a lot of time talking about the Brahmoyah's.

28
00:03:16,000 --> 00:03:20,000
And I think that's important not to this track of.

29
00:03:20,000 --> 00:03:24,000
It's important not to overemphasize the importance of love,

30
00:03:24,000 --> 00:03:28,000
but it's also important not to neglect the importance of it.

31
00:03:28,000 --> 00:03:36,000
It has meditation practices, love, compassion, sympathy and equanimity are for a very important,

32
00:03:36,000 --> 00:03:45,000
very useful meditation for an aspiring Buddhist.

33
00:03:45,000 --> 00:03:48,000
Anyway, and there's not that much to say about the quote anyway,

34
00:03:48,000 --> 00:03:51,000
unless I wanted to go on about how to cultivate loving kindness.

35
00:03:51,000 --> 00:03:54,000
I do have videos that talk a little bit about it,

36
00:03:54,000 --> 00:04:00,000
but then I have that kids video if you ever want to learn about how to send love.

37
00:04:00,000 --> 00:04:03,000
But I'm going to go for tonight.

38
00:04:03,000 --> 00:04:07,000
Thanks for tuning in. Thanks for showing up to meditate,

39
00:04:07,000 --> 00:04:34,000
wishing you a good practice.

