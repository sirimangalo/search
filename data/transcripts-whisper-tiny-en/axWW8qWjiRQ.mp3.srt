1
00:00:00,000 --> 00:00:04,000
Hello and welcome back to my study of the Dhamupada.

2
00:00:04,000 --> 00:00:11,000
Today we continue on with verse number 70, which reads as follows.

3
00:00:34,000 --> 00:00:42,000
A month after month, if a fool should follow Boon-J, a Boon-J, a Boon-J, a Boon-J, a Boon-J should eat.

4
00:00:42,000 --> 00:00:55,000
Just the tip of a grass, could say grass worth of food as an austerity is the point.

5
00:00:55,000 --> 00:01:03,000
And so, Sankata, Dhamman, and Kalang, and Gacy, so, a Gacy, so the thing.

6
00:01:03,000 --> 00:01:06,000
Kalang, Kalang, and Gacy, so the thing.

7
00:01:06,000 --> 00:01:18,000
They are not worth, such a person is not worth one-sixteenth part of someone who has well weighed the Dhamma,

8
00:01:18,000 --> 00:01:25,000
someone by whom, those by whom the Dhamma is well measured.

9
00:01:25,000 --> 00:01:30,000
It seems a bit awkward to use the word well-measured, but you have to understand the context.

10
00:01:30,000 --> 00:01:41,000
It's in relation with the minuscule amount of food that you can take on a piece of grass.

11
00:01:41,000 --> 00:01:53,000
So, it's the contrast, because the worst means is the idea of putting significance on the fact that you're eating so very little.

12
00:01:53,000 --> 00:01:55,000
Like I said, that was some great thing.

13
00:01:55,000 --> 00:02:09,000
But it said, well, knowing the measurement and food to that extent and being so precise in the minuscule amount of food you take is nothing being able.

14
00:02:09,000 --> 00:02:15,000
Nothing compared to being able to well measure the Dhamma.

15
00:02:15,000 --> 00:02:21,000
So, this is a story, another one of these well-known Buddhist stories.

16
00:02:21,000 --> 00:02:35,000
This one is about a naked aesthetic, who, according to the backstory, was once a monk in a time of a past Buddha.

17
00:02:35,000 --> 00:02:40,000
And he wasn't a very good monk, but he couldn't have been that bad.

18
00:02:40,000 --> 00:02:45,000
He was just kind of, it seems a little bit lazy or a little bit attached to his supporters.

19
00:02:45,000 --> 00:02:51,000
So, he had a family or a group of families who were supporting him and he got kind of attached to them.

20
00:02:51,000 --> 00:03:02,000
And so, one day this enlightened Arahad monk came to stop at his monastery, he was on his way here or there.

21
00:03:02,000 --> 00:03:14,000
And stayed overnight, and while he was there, I guess in the afternoon he went in to the village and the late people saw them.

22
00:03:14,000 --> 00:03:29,000
And one of the families who supported this resident monk was so taken by the department of the enlightened monk that he gave him a robe, gave him cloth for a robe,

23
00:03:29,000 --> 00:03:33,000
and treated him nicely and gave him a nice seat.

24
00:03:33,000 --> 00:03:44,000
And treated him sort of quite privileged out of faith for the obvious attainments that this monk had.

25
00:03:44,000 --> 00:03:48,000
And the other monk was not happy with this.

26
00:03:48,000 --> 00:03:55,000
He got scared thinking, what's going to happen to me if this monk becomes, he got jealous.

27
00:03:55,000 --> 00:04:02,000
If this monk becomes well loved by this family, they're going to think I'm nothing because compared to him, I really am nothing.

28
00:04:02,000 --> 00:04:16,000
And so, rather than be happy that to have this enlightened monk with him, it's kind of ironic where most of us are just wishing for such a being to come near it, near he got quite

29
00:04:16,000 --> 00:04:35,000
crazed with jealousy. And in the morning, he thought to himself, well, if, before the morning he actually, in the evening when they go back to the monastery,

30
00:04:35,000 --> 00:04:43,000
he was so crazed with jealousy that he actually scolded the Aran monk and told him how he wasn't worthy of his robes.

31
00:04:43,000 --> 00:04:49,000
So the seeds were the bedding that they had given him and so on.

32
00:04:49,000 --> 00:04:56,000
And saying they would be better if you were to go naked than to wear the cloth, given in faith.

33
00:04:56,000 --> 00:05:08,000
One like you has no goodness inside, better if you were to eat only excrement than to eat the food provided by these faithful people.

34
00:05:08,000 --> 00:05:11,000
You have no goodness in you, that kind of thing.

35
00:05:11,000 --> 00:05:19,000
So he cursed him in all these horrible ways and totally uncalled for unwarranted.

36
00:05:19,000 --> 00:05:27,000
And then he went into his room and figured, well, that's enough, now he'll get the hint and he'll take off.

37
00:05:27,000 --> 00:05:40,000
And sure enough, or whatever he thought anyway, the monk, the Aran monk realized that this was a really bad thing for the other monk, if he were to stay,

38
00:05:40,000 --> 00:05:47,000
he was going to be a great source of demerit for him to have all this anger and jealousy inside.

39
00:05:47,000 --> 00:05:56,000
So even before the night was over, he left, he continued on his way, and without saying goodbye.

40
00:05:56,000 --> 00:06:01,000
And the morning actually, the resident monk thought the Aran was still there.

41
00:06:01,000 --> 00:06:13,000
And so he got really scared that if we go into the village for arms, then this monk is just going to show me up and he'll never want to leave.

42
00:06:13,000 --> 00:06:16,000
He'll take my place as the resident monk.

43
00:06:16,000 --> 00:06:22,000
So after sweeping out the monastery, so he was no king monk, he did his duties like sweeping.

44
00:06:22,000 --> 00:06:28,000
But then he goes, because it's a routine that when it's time to go for arms, he have to ring the bell.

45
00:06:28,000 --> 00:06:35,000
So he goes and he takes his fingernail and he just touches his fingernail to the bell and he says, well, there I've run the bell.

46
00:06:35,000 --> 00:06:38,000
And then he went off into the village.

47
00:06:38,000 --> 00:06:44,000
When he gets to the village, the supporting family says, oh, where is that other monk that came yesterday?

48
00:06:44,000 --> 00:06:47,000
We were going to offer food to him as well.

49
00:06:47,000 --> 00:06:53,000
The resident monk shook his head and said, oh, he was that monk, I don't know.

50
00:06:53,000 --> 00:06:57,000
He didn't hear me sweeping and he didn't hear me when I rang the bell.

51
00:06:57,000 --> 00:07:05,000
He must be sleeping in because of after getting all that luxurious bed that you gave him and being treated so well.

52
00:07:05,000 --> 00:07:08,000
It's probably still lazing around bed.

53
00:07:08,000 --> 00:07:13,000
Anyway, I shouldn't drag this out too much because we've got to get into the main story.

54
00:07:13,000 --> 00:07:22,000
But to make a long story short, the layperson gave him some food to bring back to the other monk and on the way back to the monastery.

55
00:07:22,000 --> 00:07:39,000
He saw that this food was so good and such rich and delicious quality that he figured because he had no understanding of enlightenment.

56
00:07:39,000 --> 00:07:46,000
That if this monk were to taste his food, he wouldn't be stuck in and would never want to leave.

57
00:07:46,000 --> 00:07:52,000
So instead of bringing it back to the monastery, he actually threw the food out on the side of the road.

58
00:07:52,000 --> 00:08:02,000
As a result of this bad, these bad deans that he did, it actually says that he had done many, many years of meditation.

59
00:08:02,000 --> 00:08:16,000
But the commentary specifically mentions or states that all of that meditation was unable to protect him from these evil deeds.

60
00:08:16,000 --> 00:08:23,000
And based on this one instance of doing evil deeds when he died, he went to hell and roasted for a long, long time.

61
00:08:23,000 --> 00:08:31,000
When he was born again as a human being, he still had the residue of the bad karma and this is where the story begins.

62
00:08:31,000 --> 00:08:42,000
He was born in a rich household, but he would only eat his own excrement from the day he was born.

63
00:08:42,000 --> 00:08:54,000
And all he would eat was his own excrement, but he wouldn't eat any food that people prepared for him.

64
00:08:54,000 --> 00:09:07,000
And more than that, he wouldn't eat, he wouldn't eat his own excrement as a result of this twisted mind that he had.

65
00:09:07,000 --> 00:09:17,000
It's an interesting aspect of karma that I'll get into a little bit later that it actually twists your mind and rather than other people, only feeding him excrement.

66
00:09:17,000 --> 00:09:19,000
It was all that he would eat.

67
00:09:19,000 --> 00:09:24,000
And so his family found out about this and realized that he wasn't changing his way.

68
00:09:24,000 --> 00:09:31,000
They thought he grew out of it, he didn't grow out of it, no matter what they did, bringing any sort of choice food to him.

69
00:09:31,000 --> 00:09:40,000
He was, his mind was so twisted with the bad karma that he had no taste for anything but his own excrement.

70
00:09:40,000 --> 00:09:49,000
And so they kicked in eventually they kicked him out and he had to go live on his own and then they could have said it, took him in.

71
00:09:49,000 --> 00:09:56,000
And he lived with them for a while, but eventually they found out that they also found out that he was eating excrement.

72
00:09:56,000 --> 00:10:01,000
They'd go in for arms and he'd stay back and he'd say, oh no, I'll get food by myself.

73
00:10:01,000 --> 00:10:06,000
And every day he did the same thing and they wondered where he was getting food, so they stopped.

74
00:10:06,000 --> 00:10:11,000
Instead of going, they snuck and took a look where he was getting his food from.

75
00:10:11,000 --> 00:10:24,000
It turns out he's getting his food from the local or from the monastery, the naked aesthetic monastery from the trains under the outhouses.

76
00:10:24,000 --> 00:10:27,000
So they kick him out as well.

77
00:10:27,000 --> 00:10:33,000
And he goes and stays by the public, the trains.

78
00:10:33,000 --> 00:10:42,000
And when people go and do their business, he hides and when they leave, he goes in and eats their excrement.

79
00:10:42,000 --> 00:10:44,000
This is how the story goes.

80
00:10:44,000 --> 00:10:46,000
This is the backstory.

81
00:10:46,000 --> 00:10:51,000
When the Buddha finds him, which of course the Buddha does.

82
00:10:51,000 --> 00:11:01,000
He's become quite a character in the area, because what he does is people are, of course, wondering why he's hanging out near the other trains.

83
00:11:01,000 --> 00:11:07,000
So he takes on this character and this is where the verse comes in.

84
00:11:07,000 --> 00:11:15,000
He takes the standing on one leg, leaning up against the side of this rock face,

85
00:11:15,000 --> 00:11:26,000
and standing on one leg, leaning up against the side of the cliff, and standing with his mouth, mouth opened, facing the wind.

86
00:11:26,000 --> 00:11:31,000
And so when people would come up, they'd say, who are you?

87
00:11:31,000 --> 00:11:33,000
I'm a wind eater.

88
00:11:33,000 --> 00:11:37,000
All I eat is the wind, and that's how I survive.

89
00:11:37,000 --> 00:11:41,000
So he concocted this tale and then they asked, well, why are you standing on one foot?

90
00:11:41,000 --> 00:11:50,000
Well, I have such a powerful aesthetic that if I were to place both feet on the earth, it couldn't support my weight and so on and so on.

91
00:11:50,000 --> 00:12:02,000
And he basically lies through his teeth and actually becomes a guru of sorts or a holy man of sorts in the area.

92
00:12:02,000 --> 00:12:14,000
Well, living in this area near the latrines is sort of smelly, filthy area, which would be considered a pretty extreme asceticism.

93
00:12:14,000 --> 00:12:18,000
He says, I never lie down, I just stand here all the time.

94
00:12:18,000 --> 00:12:23,000
But then when people left, of course, he would go and find a place to sleep or whatever.

95
00:12:23,000 --> 00:12:25,000
It was all just a big lie.

96
00:12:25,000 --> 00:12:31,000
It's just so he could stay near the trains and eat people's species and excrement.

97
00:12:31,000 --> 00:12:43,000
And so one morning, as the story of these stories always go, the Buddha was sending out his mind, trying to think of who in the universe, trying to see who in the universe,

98
00:12:43,000 --> 00:12:52,000
whether they be in heaven or whether they be on earth or in whatever realm who would most benefit from his teaching that day.

99
00:12:52,000 --> 00:13:03,000
And on one day, he saw this Jambuddika, this naked ascetic, sort of this sham ascetic.

100
00:13:03,000 --> 00:13:21,000
And right, so the last part that we get into the verse is people got so attracted and so encouraged and excited by his asceticism that they wanted to start.

101
00:13:21,000 --> 00:13:27,000
They came to offer him things and so people would offer him food and offer him robes and everything.

102
00:13:27,000 --> 00:13:34,000
And they pressed these upon him and tried again and again to get him to take them and to finally pretending to relent.

103
00:13:34,000 --> 00:13:42,000
He takes a blade of grass and he dips his blade of grass into their food and just takes a taste of it.

104
00:13:42,000 --> 00:14:00,000
And he said, that will suffice for your merit, for your goodness, because the theory of course is that they get good merit, if it's a good thing for them to give to holymen.

105
00:14:00,000 --> 00:14:08,000
And so thinking that he was a holyman giving him the food, if he were to reject it outright, he would be stopping them from doing a good deed.

106
00:14:08,000 --> 00:14:16,000
And so he said, well, that's enough for you to gain the goodness of giving such a holy person like me.

107
00:14:16,000 --> 00:14:30,000
And so that's how he became sort of known as this guy who ate from the tip of all he ate, all he would eat is enough to cover the tip of a blade of grass.

108
00:14:30,000 --> 00:14:32,000
So that's where the Buddha found him.

109
00:14:32,000 --> 00:14:41,000
There's a long story but I won't go into it about the meeting with this ascetic. But basically in the end, he tries to pull a fast one on the Buddha and says,

110
00:14:41,000 --> 00:14:46,000
oh, I'm this ascetic and I have to stand on one leg and I only win.

111
00:14:46,000 --> 00:14:55,000
And the Buddha said, basically, call them a liar and said, you know, the truth has you've just been lying to everybody this whole time, and now you're trying to lie to me.

112
00:14:55,000 --> 00:15:04,000
Truth is, you're not an aura hand and you have no clue. The truth is, the reason why you're here eating people's eczema is because of deeds that you've done in the past.

113
00:15:04,000 --> 00:15:13,000
And he told them about, he explains to him about deeds that he's done in the past, all the evil deeds that he did to this other monk.

114
00:15:13,000 --> 00:15:25,000
And then Jabuddika becomes a monk and then eventually an aura hand. So that's the background story.

115
00:15:25,000 --> 00:15:42,000
Getting into what this actually means to us, I guess here we have a story of the main aspect of it is the contrast between asceticism

116
00:15:42,000 --> 00:15:56,000
and the verse itself doesn't even talk about false asceticism. But it talks about ascetic act, like if it were the case that someone were only eating from the tip of a blade of grass.

117
00:15:56,000 --> 00:16:09,000
The contrast between that and the asceticism or extreme practice in Buddhism, which is really the extreme practice of renunciation and understanding.

118
00:16:09,000 --> 00:16:22,000
It's the extreme of knowledge and the sense of coming to understand the true nature or the entire nature of reality.

119
00:16:22,000 --> 00:16:31,000
And so the verse says that you can't compare the two. But there's a couple of other things we can understand from this verse.

120
00:16:31,000 --> 00:16:46,000
The story ends that how his meditation practice that we said wasn't able to stop him from going to hell actually comes back and is a support for him to become an aura hand.

121
00:16:46,000 --> 00:16:52,000
So he actually becomes enlightened once he gives up his asceticism and his false asceticism.

122
00:16:52,000 --> 00:17:03,000
He's actually able to become among very quickly, they say there's one of those cases where the robes just appeared out of nowhere, appeared very easily, maybe.

123
00:17:03,000 --> 00:17:10,000
They were easily found and he was able to become an aura hand quite quickly.

124
00:17:10,000 --> 00:17:28,000
So this is the first thing I think we're mentioning about this verse and the story actually before getting into the verse is how rightly and clearly it differentiates between the power of karma.

125
00:17:28,000 --> 00:17:41,000
So the power of evil deeds and how they last and how they affect the mind. So this gives us a hint but people wondering how karma works and what is it and where is it and so on.

126
00:17:41,000 --> 00:17:52,000
This is a sort of exemplary of the kind of results that you would expect from a really bad karma that it actually changes your mind.

127
00:17:52,000 --> 00:17:59,000
And why I think it's exemplary is you can see this sort of case in people in the world today.

128
00:17:59,000 --> 00:18:04,000
Sometimes not so extreme, sometimes just as extreme, but quite often not so extreme.

129
00:18:04,000 --> 00:18:14,000
But the person, for example, a stingy person will just not want to accept gifts, people will give them something and they will be uninterested in the gift.

130
00:18:14,000 --> 00:18:22,000
Even if it's something they can use, they'll be like, well, but I'm fine with this substandard that I have or I'm fine without it.

131
00:18:22,000 --> 00:18:30,000
Even though they're not, even though it's causing them suffering to be without it, it actually does twist your mind in this way.

132
00:18:30,000 --> 00:18:39,000
You can see this in people in the world. The biggest one I can think of is stingy people who aren't comfortable taking gifts from others.

133
00:18:39,000 --> 00:18:47,000
Whereas the opposite, of course, is true. You can see people who are generous have no problem taking gifts from others.

134
00:18:47,000 --> 00:18:57,000
I mean, it's actually kind of logical if a person is generous then they don't feel guilty taking from others because they know that they're just as generous as these people.

135
00:18:57,000 --> 00:19:01,000
They know what it means to give and there's an understanding there.

136
00:19:01,000 --> 00:19:06,000
Whereas a person who is stingy will feel guilty if they were to take too much.

137
00:19:06,000 --> 00:19:12,000
So the way they can rationalize being stingy is by not accepting gifts from others.

138
00:19:12,000 --> 00:19:19,000
So it's actually, it's something mystical about it, but this is a very important aspect of how karma works.

139
00:19:19,000 --> 00:19:26,000
It actually can get so far as to twist your mind in this way that you wind up eating your own excrement.

140
00:19:26,000 --> 00:19:31,000
And the other aspect, the other part of it, which is maybe more interesting for us as meditators,

141
00:19:31,000 --> 00:19:37,000
is how far reaching and long lasting the effects of your meditation are said to be.

142
00:19:37,000 --> 00:19:47,000
And this is sort of common wisdom shared by meditation teachers in most traditions that the meditation practice we do,

143
00:19:47,000 --> 00:19:55,000
especially because it's so involved with such a deep part of who we are.

144
00:19:55,000 --> 00:20:02,000
The very core of our psyche, it's something that lasts not only through this life, not only in the next life,

145
00:20:02,000 --> 00:20:10,000
but it's something that can potentially last for a long, long time until the seed that is in the ground until the water falls,

146
00:20:10,000 --> 00:20:14,000
the rain falls and the seed can grow.

147
00:20:14,000 --> 00:20:21,000
When the conditions are right, the person's past meditation can come up at any time to be a support.

148
00:20:21,000 --> 00:20:33,000
So it's kind of as a reassurance for us that our meditation is no matter whether we become enlightened in this life or not,

149
00:20:33,000 --> 00:20:39,000
it's not wasted. The goodness that you gain through your meditation is not in vain.

150
00:20:39,000 --> 00:20:46,000
So that worth mentioning those and kind of pointing that out and affirming that.

151
00:20:46,000 --> 00:20:54,000
But the main point here in all equally interesting is in regards to the difference in practices.

152
00:20:54,000 --> 00:21:01,000
I guess there's two points. The first one is in defeat in regards to fake asceticism.

153
00:21:01,000 --> 00:21:10,000
That's very common to find people focusing so much of their energy on external appearances.

154
00:21:10,000 --> 00:21:20,000
So religious people, you should never judge a religious person by their appearance, at least not in a positive way.

155
00:21:20,000 --> 00:21:29,000
If you see a person obviously doing bad deeds or totally unfocused in their behavior and their speech,

156
00:21:29,000 --> 00:21:36,000
then you can probably be sure that it's indicative of something wrong inside.

157
00:21:36,000 --> 00:21:49,000
But just because a person is sitting quiet and speaking softly or performing all sorts of seemingly amazing asceticisms,

158
00:21:49,000 --> 00:21:57,000
it's not a reason to think that the person isn't as the Buddha said a fool inside Balho, Bunjaya, Bunjaya, Bunjaya.

159
00:21:57,000 --> 00:22:06,000
So a fool can do these things, a fool can eat off the tip of a blade of grass.

160
00:22:06,000 --> 00:22:24,000
That kind of severity or that kind of explicitness or intricacy of practice is nothing compared to the intricacy or the subtlety of understanding.

161
00:22:24,000 --> 00:22:35,000
So the second part here is the difference in value between asceticism and wisdom or understanding or enlightenment.

162
00:22:35,000 --> 00:22:44,000
That asceticism will only ever be a vehicle or a catalyst. It really depends what you do with it.

163
00:22:44,000 --> 00:22:48,000
Not all asceticism is wrong.

164
00:22:48,000 --> 00:22:56,000
Certainly standing on one foot is probably useless or eating a wind probably won't help you.

165
00:22:56,000 --> 00:23:06,000
Food is necessary to keep the body in an ordinary state that you can observe the ordinary natural state.

166
00:23:06,000 --> 00:23:12,000
If you go without food you won't be able to see certain interesting...

167
00:23:12,000 --> 00:23:20,000
It's like you won't have the potential to see those things that cause defilements to arise.

168
00:23:20,000 --> 00:23:28,000
For example, the hormones in the body, for example, if you can't observe them, then you won't be able to...

169
00:23:28,000 --> 00:23:39,000
The understanding in regards to the feelings that arise, that understanding can never arise.

170
00:23:39,000 --> 00:23:47,000
As a result, you can never truly be free from things like lust or anger or fear or fear.

171
00:23:47,000 --> 00:23:53,000
Because you haven't given it the opportunity to arise, you've repressed it and you're in a very weak state.

172
00:23:53,000 --> 00:23:55,000
So you won't be able to...

173
00:23:55,000 --> 00:23:58,000
It will only come up very weakly.

174
00:23:58,000 --> 00:24:05,000
There's not a challenge necessary to overcome the problem because the problem never arises.

175
00:24:05,000 --> 00:24:09,000
If I'm not eating, this is not a good idea and so on.

176
00:24:09,000 --> 00:24:17,000
But even for a set of systems like only eating one meal a day or not never lying down,

177
00:24:17,000 --> 00:24:24,000
so only doing walking and sitting meditation or never lying down, this kind of thing.

178
00:24:24,000 --> 00:24:31,000
Even these practices are not in and of themselves of any real use, any intrinsic value.

179
00:24:31,000 --> 00:24:40,000
The only thing that has intrinsic value is, in the end, the wisdom and understanding of how things work.

180
00:24:40,000 --> 00:24:45,000
And Sankata Damanang, it's really an interesting kind of a rare form.

181
00:24:45,000 --> 00:24:52,000
But the idea here is the ability to tell the difference between good and evil and good and bad.

182
00:24:52,000 --> 00:25:12,000
So in Buddhism, the act itself is not that which we consider to be bad, but it's the intention behind it.

183
00:25:12,000 --> 00:25:23,000
So when a person has this understanding of what leads to happiness and what leads to suffering,

184
00:25:23,000 --> 00:25:26,000
this is considered to be the most valuable.

185
00:25:26,000 --> 00:25:39,000
I guess the obvious thing to point out here is that a person who practices a setticism or is devoted to a setticism is actually devoted to a practice that is potentially a cause for suffering.

186
00:25:39,000 --> 00:25:45,000
And so the question is, what is your goal if you're causing yourself suffering?

187
00:25:45,000 --> 00:25:47,000
Is your goal to suffer?

188
00:25:47,000 --> 00:25:51,000
Because that seems like a irrational goal.

189
00:25:51,000 --> 00:25:57,000
When the definition of suffering is something that is really by definition unwanted.

190
00:25:57,000 --> 00:26:00,000
So how can you say you want to suffer?

191
00:26:00,000 --> 00:26:02,000
There must be a reason why you're doing this.

192
00:26:02,000 --> 00:26:11,000
The reason for any rational person turns out to be they believe that this is somehow going to lead them to happiness.

193
00:26:11,000 --> 00:26:15,000
So this is actually what it means to be able to weigh the dumbness.

194
00:26:15,000 --> 00:26:22,000
It means to be able to understand and measure that which truly leads to happiness.

195
00:26:22,000 --> 00:26:29,000
So goodness in Buddhism, goodness in evil have everything to do with how they bring you happiness or suffering.

196
00:26:29,000 --> 00:26:36,000
And actually these kind of a setticism's, for the most part, only lead to suffering.

197
00:26:36,000 --> 00:26:38,000
So they're considered to be the one extreme.

198
00:26:38,000 --> 00:26:48,000
It's the kind of thing that people who have left the home life full of sensual desires seeing that extreme are liable to undertake.

199
00:26:48,000 --> 00:26:53,000
It's also something that even as I mentioned before in modern times we find people undertaking people who push themselves and do something just because it's a difficult thing.

200
00:26:53,000 --> 00:27:13,000
Thinking that there's some value to the hard work ethic that something is valuable just because it is hard to do or just because it is work hard work.

201
00:27:13,000 --> 00:27:20,000
And the Greeks had of course this nice legend of Sisyphus pushing the boulder up the hill.

202
00:27:20,000 --> 00:27:23,000
And we always refer back to that.

203
00:27:23,000 --> 00:27:29,000
And just pushing a boulder up the hill isn't any use because at the end it just falls back down.

204
00:27:29,000 --> 00:27:31,000
There's such thing as a useless endeavor.

205
00:27:31,000 --> 00:27:35,000
On the other hand something can be quite pleasant just because something is pleasant.

206
00:27:35,000 --> 00:27:37,000
It's not something to be afraid of.

207
00:27:37,000 --> 00:27:45,000
And so this is kind of where true aesthetics go wrong.

208
00:27:45,000 --> 00:27:55,000
They think that somehow it's wrong to indulge in pleasure or not indulge in it's wrong to allow pleasure to arise.

209
00:27:55,000 --> 00:28:05,000
And they aren't able to distinguish between the pleasure and the desire for the pleasure and enjoyment of the liking of it.

210
00:28:05,000 --> 00:28:22,000
It's possible to experience the pleasure as an ordinary or experience it as it is without attachment, without any kind of desire or liking of it, any kind of attachment to it.

211
00:28:22,000 --> 00:28:51,000
And so I think in essence the important point for us as meditators to keep in mind here is it really is about your understanding and your ability to discern that which is a benefit.

212
00:28:51,000 --> 00:28:56,000
You shouldn't do something just because someone tells you that this is going to be a benefit to you.

213
00:28:56,000 --> 00:29:07,000
If I tell you to walk back and forth or to sit still in your room that in and of itself isn't where your focus should be.

214
00:29:07,000 --> 00:29:09,000
It isn't where the benefit is going to come from.

215
00:29:09,000 --> 00:29:16,000
The benefit is going to come from your understanding which I am telling you is going to come as you're walking and as you're sitting.

216
00:29:16,000 --> 00:29:21,000
And that's where your focus should be when you're walking you should be trying to understand.

217
00:29:21,000 --> 00:29:36,000
You're not actively thinking about it but you should be trying to see things as they are and cultivate this clear awareness of this is lifting, this is moving, this is placing, this is rising, this is falling, this is pain.

218
00:29:36,000 --> 00:29:51,000
Just observing. Once you observe that's how you come to weigh things. You don't judge, you don't have prejudice, you don't intellectualize or fall back on logic or reason.

219
00:29:51,000 --> 00:30:01,000
Just observe. It's really enough because with enough observation you can detect patterns, you can detect cause and effect.

220
00:30:01,000 --> 00:30:08,000
And so you can tell the difference for yourself between that which leads to happiness and that which leads to suffering.

221
00:30:08,000 --> 00:30:19,000
Then you become someone who is able to weigh the dumbbells and measure states which is better than 16 times.

222
00:30:19,000 --> 00:30:28,000
There's more than 16 times better. 16 is just a round number. It's over hex decimal back in those days.

223
00:30:28,000 --> 00:30:35,000
So it just means like a fraction, a set of system isn't worth a fraction of the goodness.

224
00:30:35,000 --> 00:30:41,000
It's not worth a millionth part of the goodness that comes from understanding.

225
00:30:41,000 --> 00:30:59,000
So that's verse 70 in the story that goes with it. That's all for today. Thank you for tuning in and tune in next time for the next one.

