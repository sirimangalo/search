1
00:00:00,000 --> 00:00:27,840
Good evening, everyone, broadcasting line, March 31st.

2
00:00:27,840 --> 00:00:31,760
Today's quote from the Angodranikaya,

3
00:00:31,760 --> 00:00:46,000
Phabhasarangidangvikrejitam, luminous, radiant, is this mind.

4
00:00:46,000 --> 00:01:10,800
Phabhasarangvikrejitam, it is defiled by visiting defilements or incoming defilements.

5
00:01:10,800 --> 00:01:18,640
And that comes from without, it's a good explanation.

6
00:01:18,640 --> 00:01:22,080
Angandukas, that which comes.

7
00:01:22,080 --> 00:01:29,360
I'm going to, because often used to talk about a guest, monks are called Angandukas.

8
00:01:29,360 --> 00:01:40,080
When they come, Angandukas, Angandukas, Angandukas are quite sure, actually.

9
00:01:40,080 --> 00:01:50,000
I think in the past, I think in the past, I think in the past.

10
00:01:50,000 --> 00:01:56,000
We have this phrase, a sutava, a sutava, a sutava, a sutava, a sutava, a sutava, a

11
00:01:56,000 --> 00:01:59,520
sutava, a sutava, a sutava, a sutava, a un instructed world thing.

12
00:01:59,520 --> 00:02:03,060
It's a phrase that the body uses often.

13
00:02:03,060 --> 00:02:14,640
Talk about those who have never looked at their minds or ever thought about spiritual

14
00:02:14,640 --> 00:02:26,480
teachings, even not even spiritual teachings, so much as those who have never thought about

15
00:02:26,480 --> 00:02:32,200
the things that the Buddha taught, who have never looked at their own minds or learned

16
00:02:32,200 --> 00:02:39,680
anything, who spend their time in all this world, a sutava, a sutava, someone who has not

17
00:02:39,680 --> 00:02:49,880
heard the truth, and put to a Chanel, one who is full of developments.

18
00:02:49,880 --> 00:02:56,240
And yet, habutam pajana, they don't understand this as it is.

19
00:02:56,240 --> 00:03:05,840
So people think that the mind is, or that their person is static.

20
00:03:05,840 --> 00:03:14,760
We think of our characters, characteristics, our character types, as fixed.

21
00:03:14,760 --> 00:03:19,760
So I would say, I'm an angry person, or I get angry at this, or someone told me just

22
00:03:19,760 --> 00:03:26,840
today, really nice person, but she said, I get, let me just say, I get, ticked off at

23
00:03:26,840 --> 00:03:29,360
people's crap or something like that.

24
00:03:29,360 --> 00:03:36,540
It was in the context of a bigger statement, a larger statement, but she mentioned that

25
00:03:36,540 --> 00:03:44,280
in passing that she can get frustrated at people, and we say to things like this, like

26
00:03:44,280 --> 00:03:50,720
this is how we talk, and this is how we think, we think of ourselves as having a certain

27
00:03:50,720 --> 00:03:51,720
character type.

28
00:03:51,720 --> 00:04:01,960
So we think of the mind as being inherently idiosyncratic, but I said, no, no, the mind

29
00:04:01,960 --> 00:04:04,960
is luminous, the mind is pure, basically.

30
00:04:04,960 --> 00:04:12,600
And if any enough people, there are groups that misinterpret this text to me that the

31
00:04:12,600 --> 00:04:15,800
mind is like a glowing ball.

32
00:04:15,800 --> 00:04:21,040
The Damakaya movement in Thailand, I heard the vice habit, I sat with him for five hours

33
00:04:21,040 --> 00:04:28,400
while he explained to a small group of us about what this passage means, and it's that

34
00:04:28,400 --> 00:04:34,840
the mind is a bright light, which is coincidentally the meditation object that they use,

35
00:04:34,840 --> 00:04:37,640
and they imagine a bright light, a crystal.

36
00:04:37,640 --> 00:04:45,600
They say the mind is luminous, so they take it to be a physical light that you can see.

37
00:04:45,600 --> 00:04:59,440
The summit is meant here, it is related to Kileh's Upa Kileh's, the founding, so that's

38
00:04:59,440 --> 00:05:11,000
the first, this is the Akkani Bhata, so each sutra you can say is just a couple of sentences.

39
00:05:11,000 --> 00:05:17,840
So that first part is the first sutas, as Bhata, the first, the first sutas, just that

40
00:05:17,840 --> 00:05:18,840
part.

41
00:05:18,840 --> 00:05:29,040
And then the second sutras, just second sutas, just an inversion of it, where he says sutawat

42
00:05:29,040 --> 00:05:40,600
alayyasava kasajita, Bhavanat ati wadami, sorry, I didn't finish the first one.

43
00:05:40,600 --> 00:05:47,340
So an uninstracted ordinary world, and he doesn't understand this as it is, tasma a sutawat

44
00:05:47,340 --> 00:05:56,960
okutunjanasatitambhavananati, therefore for an uninstracted world, jitambhavananati, there

45
00:05:56,960 --> 00:06:10,200
is no cultivation of the mind, that's not a literal translation, a literal translation is,

46
00:06:10,200 --> 00:06:19,200
there is no, and it's in quotes, therefore I say wadami, tasma wadami, therefore I say

47
00:06:19,200 --> 00:06:29,280
a sutawato, for an uninstracted putunjanasat world, jitambhavananat, the training of the mind

48
00:06:29,280 --> 00:06:38,560
nati, doesn't exist, no, no, the word nati means right, nati is a story behind the word

49
00:06:38,560 --> 00:06:45,480
right, nati jitambhavananat, there is no jitambhavanan, there is no bhavananat for the jit

50
00:06:45,480 --> 00:06:58,760
and then the second one is the inversion, so sutawatawatawatawatambhavananati, that a instructed

51
00:06:58,760 --> 00:07:07,960
student of the enlightened ones understands it as it is, sutawatambhavananati, tasma, therefore

52
00:07:07,960 --> 00:07:23,840
so from that, sutawatawatawareasavakasa, jitambhavananati, for a instructed student of the enlightened

53
00:07:23,840 --> 00:07:33,880
ones, there is a t, there is jitambhavananat, cultivation of the mind, or the development

54
00:07:33,880 --> 00:07:46,680
of the mind, mental development, jitambhavananat, that is the second, and then it goes

55
00:07:46,680 --> 00:07:53,840
on, there is ten in this wagah, so the ingutarnika is sorted by wagah as which are like

56
00:07:53,840 --> 00:08:02,520
chapters sort of, but they don't, they are usually chapters, well sometimes they are

57
00:08:02,520 --> 00:08:09,520
sorted by subjects, sometimes they are just packed in there, here the third one is about

58
00:08:09,520 --> 00:08:30,520
made the jita, the mind of, the mind of made the jita, achara, sangha, tama kampi, anyway,

59
00:08:30,520 --> 00:08:39,720
but in a long day they had an exam this morning on peace, which is always a good thing

60
00:08:39,720 --> 00:08:53,120
to be tested on, but it was actually kind of disappointing, the questions on it were surprising,

61
00:08:53,120 --> 00:09:00,760
the questions were about, well I guess, which is kind of strange, it wasn't the things

62
00:09:00,760 --> 00:09:09,520
that I had studied, we were asked for meetings that had led up to UN resolution 70.1,

63
00:09:09,520 --> 00:09:15,240
70 slash one, like I don't know, I wasn't paying attention to the names of the meetings

64
00:09:15,240 --> 00:09:21,880
and some of which, but I guess I'm a little bit out of my element there because I'm not

65
00:09:21,880 --> 00:09:29,920
really interested in worldly affairs, more interested in the content than the stuff that's

66
00:09:29,920 --> 00:09:33,880
going on, which is a fault of my element, I can never really be a peace worker in the

67
00:09:33,880 --> 00:09:40,480
sense that they want me to be, they want us to be worldly peace workers I think, getting

68
00:09:40,480 --> 00:09:48,720
involved with governments and doing our civic duty I guess and getting involved, unfortunately

69
00:09:48,720 --> 00:09:56,480
I'm not a citizen, you know, out of society so, and then we had read an essay about

70
00:09:56,480 --> 00:10:05,120
we wanted three topics and we're talking about the happiness, which one did I talk about?

71
00:10:05,120 --> 00:10:19,680
Bizarre, yeah, and I think, just wondering if I wrote about the wrong topic, and I wrote

72
00:10:19,680 --> 00:10:28,120
about UN resolution 70 slash one, sustainable development report, just because it was easy,

73
00:10:28,120 --> 00:10:33,640
but I started thinking, you know, this resolution doesn't really hit at the root of the

74
00:10:33,640 --> 00:10:41,840
problem, which is, which is this quote, you know, the root of the problem is our minds

75
00:10:41,840 --> 00:10:48,480
are defiled, there was no call for meditation, how do we solve the world's problems?

76
00:10:48,480 --> 00:11:00,080
Meditation was a Douglas, so it was missing something.

77
00:11:00,080 --> 00:11:02,760
And then I just finished my essay, why I'm talking about this?

78
00:11:02,760 --> 00:11:10,560
Because I wanted to talk about my essay, so I did this essay, and I think it's somewhat

79
00:11:10,560 --> 00:11:22,360
interesting, it's a little too academic for my tastes, meaning like, at least I'm when

80
00:11:22,360 --> 00:11:27,640
I write for school, it's a little bit artificial, like you kind of have to make it fit with

81
00:11:27,640 --> 00:11:36,360
the course, and I'm not doing any courses on meditation, so I suppose I somehow play into

82
00:11:36,360 --> 00:11:41,160
their hands a little too much, I could probably have done a, found a way to do an essay

83
00:11:41,160 --> 00:11:49,960
on meditation, but, you know, East Asian Buddhism, I was interested in the lotus sutra,

84
00:11:49,960 --> 00:12:07,960
so I'm interested in making clear what is the dharma and what isn't the dharma.

85
00:12:07,960 --> 00:12:16,200
That you can't just get away with putting words in the Buddha's mouth, I was really kind

86
00:12:16,200 --> 00:12:22,040
of disgusted by the lotus sutra, for what it blatantly appears to do to the Buddha and

87
00:12:22,040 --> 00:12:32,680
to Sariputra, and to the dharma in general, you know, how it just throws out the forty

88
00:12:32,680 --> 00:12:39,000
five years of the Buddha's teaching, and it just makes bizarre claims, I mean of course

89
00:12:39,000 --> 00:12:45,040
there were lots of texts like this, but the lotus sutra became famous, and so I'm looking

90
00:12:45,040 --> 00:12:55,200
at why it became famous, you know, so why I'm talking about this, because I thought

91
00:12:55,200 --> 00:13:06,040
maybe somebody out there would like to read it, and hopefully look at it for me and

92
00:13:06,040 --> 00:13:17,440
find typos and stuff, so I don't want to put it like I'm on the internet, especially

93
00:13:17,440 --> 00:13:23,480
not what it's not finished, and because it would just been getting too many people writing

94
00:13:23,480 --> 00:13:28,960
comments on it, I've done that before, and why did I get lots and lots of comments that

95
00:13:28,960 --> 00:13:35,960
I had to just discard, so I'm posting it here on the site, if you want to go read it,

96
00:13:35,960 --> 00:13:43,040
it's fairly academic, I think, so hopefully it's not a turn-off, but I can write by writing

97
00:13:43,040 --> 00:13:50,240
something, it's good, I know my writing is good, so interested if there's anyone out there

98
00:13:50,240 --> 00:13:59,080
who would go ahead and read through it, and at least give me some feedback, programmer,

99
00:13:59,080 --> 00:14:02,520
you know, we're supposed to do this before we hand our work in, find someone who would

100
00:14:02,520 --> 00:14:08,680
read it, so I got all you guys to read it. Anyway, I post a link in case there's someone

101
00:14:08,680 --> 00:14:23,200
out there who has the time to go through it. That's about it. Not much, yeah, so I just

102
00:14:23,200 --> 00:14:28,480
finished that paper tonight, it's not a long paper, in fact, if it seems terse that it's

103
00:14:28,480 --> 00:14:34,400
like, wow, why did you write so little, and just, it feels like I felt like I was skipping

104
00:14:34,400 --> 00:14:41,240
from point to point, it's because we had a word limit of 2,000 words, and the papers that

105
00:14:41,240 --> 00:14:54,000
I came out with was 2,200, and that's being excruciatingly parsimonious, so I could

106
00:14:54,000 --> 00:14:58,520
have written it four times as much, probably, at least twice as much would have been pretty

107
00:14:58,520 --> 00:15:03,120
easy, would have been more comfortable writing twice as much, but it would have been

108
00:15:03,120 --> 00:15:10,320
more work, so I kind of thankful that it was only short, and he really likes this professor

109
00:15:10,320 --> 00:15:22,760
who was really big on a decision, yeah, because I guess a lot of people would just end up

110
00:15:22,760 --> 00:15:29,800
with word salad, and they just, he said they write an introduction that's totally unlimbed

111
00:15:29,800 --> 00:15:36,880
into their paper, and they just bladder on about this and that, so making it so short

112
00:15:36,880 --> 00:15:43,880
forces you to be on track, because you have to get through your whole argument, so anyway,

113
00:15:43,880 --> 00:15:50,880
you don't want to hear this, this is not related, but tonight's verse, getting back to

114
00:15:50,880 --> 00:15:56,480
the verse was awesome, it's an awesome little verse, and very much related to our meditation

115
00:15:56,480 --> 00:16:06,120
practice, we practice to not, not, not change the mind in the terms of cultivating

116
00:16:06,120 --> 00:16:14,360
a new eye, a new me that's Buddhist, but in terms of just coming back to a natural state,

117
00:16:14,360 --> 00:16:22,600
it's very important in Buddhism, in meditation, that we're just trying to sort everything

118
00:16:22,600 --> 00:16:29,320
out, untie all the knots, which is a bigger task than it may sound, I mean it's not like

119
00:16:29,320 --> 00:16:33,760
you're just going to go back to the ordinary person you were before, or you generally

120
00:16:33,760 --> 00:16:40,000
are, it is a profound change, but it's only a profound change, because really everything

121
00:16:40,000 --> 00:16:48,440
that makes us human is an artifice, the whole being a human is not natural, and we get back

122
00:16:48,440 --> 00:16:52,920
in this idea of what's natural, but it's not natural in the sense that we've made it,

123
00:16:52,920 --> 00:17:01,960
we've cultivated it, we've cultivated it arbitrarily, there's nothing particularly, it's not

124
00:17:01,960 --> 00:17:06,560
like you look at the universe and say, oh that needs humans, that the perfect representation

125
00:17:06,560 --> 00:17:12,640
of the universe, that being in the universe, human being, no, I mean that's what Christianity

126
00:17:12,640 --> 00:17:17,600
Judaism, theistic religions tried to do, they said, well why are humans here, well we

127
00:17:17,600 --> 00:17:25,840
must be the perfect, somehow they got to that, it's not true, anyone believes that is

128
00:17:25,840 --> 00:17:41,280
is grossly doing themselves, but there is something luminous and pure in us all, underneath

129
00:17:41,280 --> 00:17:46,960
all that garbage no matter who you are, no matter what you've done, even Devadatta in

130
00:17:46,960 --> 00:17:58,680
the end, gone on the right track, anyway, that's all for tonight, we'll take a rest,

131
00:17:58,680 --> 00:18:03,360
tomorrow I'm going to spend some of the day going over any comments that people have on

132
00:18:03,360 --> 00:18:08,080
my paper, if anyone does look at it, and going over it, and then I got to submit it

133
00:18:08,080 --> 00:18:14,560
before the end of tomorrow, so, and then I'm done, I've got a couple exams, but I'm on

134
00:18:14,560 --> 00:18:19,600
the symposium next week, it's the peace symposium, so yeah this week I'm going to be spending

135
00:18:19,600 --> 00:18:31,160
on organizing this symposium, that's all, have a good night, oh yeah have a good night,

136
00:18:31,160 --> 00:18:37,200
no questions tonight, not that anyone ever has them, but if you happen to have questions,

137
00:18:37,200 --> 00:18:52,200
come back tomorrow, bye.

