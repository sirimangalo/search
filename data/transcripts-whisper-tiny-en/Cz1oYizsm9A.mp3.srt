1
00:00:00,000 --> 00:00:07,000
Hi, my name is Brother Noah Yutudemo, and I'm a Buddhist monk.

2
00:00:07,000 --> 00:00:13,000
I was born in Canada, ordained in Thailand, and I'm currently living in the United States of America.

3
00:00:13,000 --> 00:00:16,000
This is my YouTube channel.

4
00:00:16,000 --> 00:00:20,000
If you're new here, I'd suggest you check out some of the videos on meditation.

5
00:00:20,000 --> 00:00:24,000
Those are the best to get a general idea of the sorts of things that I do.

6
00:00:24,000 --> 00:00:27,000
You can check out the series on how to meditate,

7
00:00:27,000 --> 00:00:31,000
and also check out the series on why everyone should practice meditation.

8
00:00:31,000 --> 00:00:35,000
Once you've checked those out, watched some of them,

9
00:00:35,000 --> 00:00:39,000
then there's another series that I'd recommend, and that's the Ask Among series,

10
00:00:39,000 --> 00:00:46,000
where you can ask questions about meditation, questions about Buddhism, or the monks life in general,

11
00:00:46,000 --> 00:00:52,000
and get real-time video answers as I upload them.

12
00:00:52,000 --> 00:00:56,000
Check out some of the talks that I've given, some of the things that I do,

13
00:00:56,000 --> 00:01:00,000
there's lots of different types of videos that you'll find here.

14
00:01:00,000 --> 00:01:04,000
Please leave your comments, friend, me.

15
00:01:04,000 --> 00:01:08,000
I accept most friend requests, subscribe.

16
00:01:08,000 --> 00:01:10,000
You can get video updates.

17
00:01:10,000 --> 00:01:14,000
If you have any questions and you'd like some personal advice,

18
00:01:14,000 --> 00:01:16,000
you're welcome to leave me a private message.

19
00:01:16,000 --> 00:01:17,000
Thanks for stopping in.

20
00:01:17,000 --> 00:01:27,000
I wish you all the best, and it's happiness and freedom from suffering.

