1
00:00:00,000 --> 00:00:06,000
It's becoming a sort of something spontaneous or is it gradual?

2
00:00:06,000 --> 00:00:14,000
Can one be a sort of one and not be aware of it or have some sort of doubt about it?

3
00:00:14,000 --> 00:00:24,000
Those are good questions.

4
00:00:24,000 --> 00:00:31,000
Well, it's both really, you know, becoming a sort of pun is one moment.

5
00:00:31,000 --> 00:00:36,000
You're not kind of a sort of pun. You're not partially a sort of pun.

6
00:00:36,000 --> 00:00:41,000
You either are or you aren't. It's a categorical difference.

7
00:00:41,000 --> 00:00:49,000
So the pun is someone who's seen the pun as someone who's seen the pun as someone who's not.

8
00:00:49,000 --> 00:00:55,000
That's means it's spontaneous or instantaneous.

9
00:00:55,000 --> 00:01:00,000
But it's not spontaneous in the sense that it is gradual practice.

10
00:01:00,000 --> 00:01:10,000
You have to cultivate awareness to a point where the mind is able to break away from

11
00:01:10,000 --> 00:01:25,000
some cars and reach the Asankata, the uninformed, which is the environment.

12
00:01:25,000 --> 00:01:38,000
The second question is more difficult because I don't want to say the wrong thing and misrepresent the truth.

13
00:01:38,000 --> 00:01:44,000
But to me, you know, so this is how am I going to be the arbiter of this question, right?

14
00:01:44,000 --> 00:01:47,000
This is a question you have to ask the Buddha.

15
00:01:47,000 --> 00:01:54,000
A sort of pun that doesn't have doubt.

16
00:01:54,000 --> 00:02:01,000
But my understanding and I think the commentary is understanding is that,

17
00:02:01,000 --> 00:02:07,000
and I may be given that the Pityka or Buddha's teaching, what we have in the Buddha's teaching,

18
00:02:07,000 --> 00:02:15,000
is understanding that that refers to specifically doubt in the Buddha, doubt in the Dhamma and doubt in the Sangha.

19
00:02:15,000 --> 00:02:18,000
Thus, the Buddha is enlightened.

20
00:02:18,000 --> 00:02:24,000
His teachings are the path to freedom from suffering.

21
00:02:24,000 --> 00:02:36,000
And a person who practices these teachings, a person who is enlightened is the person who has practiced these teachings.

22
00:02:36,000 --> 00:02:40,000
That's the doubt.

23
00:02:40,000 --> 00:02:50,000
That's the confidence of a Sotapana that they have no doubt in these three things.

24
00:02:50,000 --> 00:02:53,000
It doesn't say that they have no doubt in themselves.

25
00:02:53,000 --> 00:02:56,000
And I think that's reasonable.

26
00:02:56,000 --> 00:03:01,000
I don't want to give a categorical answer here.

27
00:03:01,000 --> 00:03:07,000
But to me, that seems reasonable because Sotapana is just a word.

28
00:03:07,000 --> 00:03:10,000
And this happens all the time in meditation.

29
00:03:10,000 --> 00:03:14,000
You get this idea that it's something, that it's an entity.

30
00:03:14,000 --> 00:03:18,000
Am I there yet? You're waiting for the signpost.

31
00:03:18,000 --> 00:03:23,000
And so you're looking, how do you know if you're a Sotapana?

32
00:03:23,000 --> 00:03:30,000
I had some experience that makes me think that I'm a Sotapana, but am I a Sotapana?

33
00:03:30,000 --> 00:03:37,000
So we don't tend to answer people's questions when they ask about how do you know your Sotapana and something.

34
00:03:37,000 --> 00:03:40,000
Do you have greed? Do you have anger? Do you have delusion?

35
00:03:40,000 --> 00:03:44,000
Well, then there's still further to go. That's all you should know.

36
00:03:44,000 --> 00:03:49,000
Because the only thing that it would do if you did know that you're a Sotapana is,

37
00:03:49,000 --> 00:03:54,000
it would give you the reassurance that would maybe make you stop practicing or go naked lazy.

38
00:03:54,000 --> 00:03:59,000
If you are a Sotapana and you have doubt about it, you're stuck in a work really hard

39
00:03:59,000 --> 00:04:04,000
to push on to cut off more defilements.

40
00:04:04,000 --> 00:04:08,000
But I would say, yes, a Sotapana can have doubt.

41
00:04:08,000 --> 00:04:17,000
My guess is, and I'm not sure that it's correct, a Sotapana can have doubt.

42
00:04:17,000 --> 00:04:27,000
Because it's not, it's not a sure thing.

43
00:04:27,000 --> 00:04:31,000
I would say an are a hunt.

44
00:04:31,000 --> 00:04:36,000
I'd say I don't want to make these categorical statements.

45
00:04:36,000 --> 00:04:41,000
I think it's reasonable soup to suggest that a Sotapana could have doubt.

46
00:04:41,000 --> 00:04:47,000
And I think it's reasonable to suggest that an are a hunt may not have doubt.

47
00:04:47,000 --> 00:04:50,000
Because the are a hunt is free from delusion.

48
00:04:50,000 --> 00:04:58,000
The are a hunt is in my mind more likely of the two to be free from doubt in themselves.

49
00:04:58,000 --> 00:05:09,000
Because in the Buddha said, we see dang Ramacharyan, katang kananyam, nati dani, punabhuoti, nati dani, tarat, pajanati.

50
00:05:09,000 --> 00:05:13,000
He knows for himself that there is nothing further.

51
00:05:13,000 --> 00:05:15,000
This is an are a hunt.

52
00:05:15,000 --> 00:05:17,000
The are a hot nose for themselves.

53
00:05:17,000 --> 00:05:25,000
I would say a Sotapana because they still have greed, anger and delusion, they might still have this confusion inside of themselves.

54
00:05:25,000 --> 00:05:26,000
I must still have doubt.

55
00:05:26,000 --> 00:05:29,000
The doubt that is disappeared is in the Buddha Damasanga.

56
00:05:29,000 --> 00:05:34,000
So whether they know that they themselves have reached it, that just be confused and doubt about themselves.

57
00:05:34,000 --> 00:05:49,000
But when they look deep down, they will be able to say that they have no doubt in the Buddha Damasanga.

