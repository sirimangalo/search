Hi, in this video, I'll teach you how to meditate on the most important thing in the whole
universe, yourself.
Meditating on yourself means watching the things you do, say, and think, and making sure
that you are always doing good deeds, speaking good speech, and thinking good thoughts.
When you meditate on yourself, you will be able to tell whether what you are doing is
good or bad, whether it is hurting yourself or other people, or whether it is helping.
This way, you will be able to make sure that you are only making yourself and other people
happy rather than unhappy.
The easiest part of ourselves to watch is the body, so that's where we start meditating.
Sit in a chair or on the floor, or even lie down, and focus your mind on the position
your body is in.
If you are sitting down, say in your mind, sitting.
If you are lying down, say in your mind, lying.
You can meditate like this as long as you like, just repeating to yourself, sitting,
sitting, sitting, or lying, lying, lying.
Another way of watching the body is to focus on the stomach as it rises and falls when
you breathe in and out.
If you can't feel it, you can just put your hand on your stomach, and focus on the feeling
of the stomach, rising and falling against your hand.
When the stomach rises, say in your mind, rising, and when it falls, think falling, rising,
falling, rising, falling.
You can meditate like this for as long as you like.
The second part of ourselves to watch is the feelings, pain, happiness, and calm.
When you feel pain, you can focus on the pain, and say pain, pain, pain.
When you feel happy, you can think to yourself, happy, happy, happy.
And when you feel calm, you can think to yourself, calm, calm, calm.
Meditating like this helps you let your feelings go so you don't worry about them when
they change.
When you feel pain, you don't have to get upset by it.
And when you feel happy or calm, you don't have to cling to it.
The third part of ourselves to watch is our thoughts.
Whenever you start to think about the past or the future, good thoughts or bad thoughts,
you can just think to yourself, thinking, thinking, and bring your mind back to what is
really going on.
The fourth part of ourselves is our emotions.
These are things like wanting and liking, anger and frustration, sadness and fear, drowsiness,
worry, restlessness, doubt, and confusion.
Whatever emotion comes up, just meditate on it, like anything else, so that it doesn't
get out of control.
Just think to yourself, liking, or angry, or sad, or confused, or whatever, depending
on what emotion comes to your mind.
The fifth part of ourselves is our senses, seeing, hearing, smelling, tasting, and feeling.
You can meditate on any of these whenever you see something, even with your eyes closed.
When you hear a sound, when you smell something good or bad, when you taste whatever it
is you're eating or drinking, or when you feel hot, cold, hard or soft on some part of
the body.
Think in your mind, seeing, seeing, hearing, hearing, smelling, smelling, tasting, tasting,
or feeling, feeling, whenever you experience one of these things.
So to repeat, there are five parts of ourselves that we can meditate on.
Our body, our feelings, our thoughts, our emotions, and our senses.
Now if you're ready, we can try to put them all together.
Start by meditating on your body.
Focus on the position, saying, sitting, sitting, or lying, lying, or focus on the stomach,
saying, rising, falling as it moves, then whenever feelings, thoughts, emotions, or senses
arise, meditate on them instead.
Once they are gone, go back to meditating on the body.
Ready?
Try now.
Well done.
If you keep practicing like this every day, you will see that you know yourself better and
better every day, and you will get better and better at doing, saying, and thinking,
the things that bring you and other people happiness and peace.
And if you've been watching all the videos in this series, you now know just about everything
you need to practice meditation.
Don't be afraid to come back and watch all these videos again to help remind you of how
to meditate whenever you forget.
Thank you for watching and I hope your meditation practice makes you happy and healthy
and free from all suffering.
Bye for now.
