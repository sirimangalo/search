Hello, and welcome back to our study of the Damapada.
Today, we continue on with verse number 76, which reads as follows.
Today, we continue on with verse number 76, which reads as follows.
Today, we continue on with verse number 76.
One who like one who unearth or uncovers buried treasure.
And whatever person seeing one's faults,
paste when seeing one's faults,
what did I see now?
Let's then be known.
That's one no one's fault.
That's one no one's own fault.
So point out one's fault.
Someone who niggas, this person, niggae,
wanting one who speaks in a censure or gives one
admonishment, admonishes one.
Made having a wise,
should be considered a wise person.
That is some banditangpati.
Such a person should be associated with, by the wise.
If a person is wise,
such a person is to be associated with,
by the wise people.
When one associates with such a person,
things get better, not worse.
Say you'll hold in a bop you, they get better.
You don't go to evil.
You don't get worse.
So associate with people who point out your faults
when you do, things get better, not worse.
This verse was told in regards to a student of Sariputa,
named Radha.
Radha was a poor brahman,
who listened, heard the Buddha's teaching,
and went to live with the monks.
But the monks were unwilling to give him the higher,
or to give him any ordination.
I think somewhere there was a comment that
it was because he took such great care of the monks,
and they were reluctant to give him the ordination
because they needed someone to look after them.
Which is sort of an interesting situation.
It doesn't say that in this version of the story,
but for some reason it may have been that he was old.
It may have been that they had no trust in him.
But the Buddha saw this, and the Buddha
discerned through his vast knowledge and understanding
that Radha would actually be capable of becoming an Arat
if he were to ordain.
So he went to the monks, and he said,
why is this man pretending that he didn't know
or not letting on that he knew?
And they told them who he was.
He said, why isn't he or why aren't you ordaining him?
I think maybe that's where it says it may actually
say why they didn't ordain him.
But anyway.
So he said,
does anyone, do any of you think of a reason
why you might be inclined to ordain him?
Can any of you remember something that he did for you?
And sorry, put a pipe up and said,
I remember once when he was living at home,
he gave me a spoonful of rice.
And the Buddha said, well, sorry, put that.
Don't you think it's appropriate to be grateful
and to repay people's kindness?
Which is interesting because he didn't do that
of a thing in a grand scheme of things.
All he did was give him a single spoonful of rice.
But it's a testament to the Buddha's sensitivity
and sorry, put us gratitude in remembering.
So when you ask someone, did they do something
that has this person done anything for you?
It's not common for people to think
to keep that in their mind that this person did
such a simple thing, but sorry, put it was a person
who hadn't this great sensitivity.
He didn't take for granted the fact
that he had been given even a single spoonful of rice.
And so he spoke up and the Buddha said,
well, that case, don't you think it's worth ordaining him?
And sorry, put to immediately said in that case,
I will ordain him.
And so sorry, put to give him the ordination.
And I guess there was some skepticism
that this monk would actually,
whether this man would actually make a good monk.
But sorry, put to was fair with him
and honest with him and found that actually
Radha was quite amenable to training.
So sorry, put to a tell him, don't do this, don't do that.
And he would take everything,
sorry, put to said to heart.
When there was only you have to do this,
you have to do that.
He would only have to hear it once
and he would immediately adjust himself.
And when sorry, put to admonish him saying,
you're doing this wrong, you're doing that wrong.
This is not right.
You have this fault, you have that fault.
He was completely amenable.
And so the Buddha asked him at one time,
how's your student going?
Do you find him amenable?
Sorry, put to said he is the perfect student.
When I tell him not to do something,
he stops doing it and never have to tell him twice.
And the same goes with telling him what he should do.
He is the perfect student.
And the Buddha said, if you could have other students
like him, what do you ordain?
People, others, if you knew that they were going to be like him
and he said, if I could have a thousand students,
I would gladly take them on if they were all like Radha.
And the monks got talking about this and they said
how amazing it was, how wonderful it was.
That sorry, put to had found such a wonderful student.
And that Radha also had found such a wonderful teacher
who was willing to point out his faults.
And in no long times, Radha became an Arahad as predicted.
And the Buddha heard the monks talking like this
and then he, as a result, spoke this verse,
saying indeed, Radha is very lucky.
Yes, sorry, put is very lucky, but Radha also is very lucky
because Sariput is someone who is like a person
who points out very treasure.
And he spoke this verse.
So that's the backstory.
It's a fairly well-known Buddhist story among Buddhists.
And it relates to our practice in regards to the role
of a teacher in one's relationship with one's teacher.
You could also say it relates to our own ability
to receive criticism in general
and therefore to our relationship with anyone
who gives us criticism.
Because we receive criticism from all sorts of sources,
from many people who are not qualified to criticize.
How do you deal with people who criticize you unjustly, for example?
This doesn't directly relate to that,
but that's the general subject that we're dealing with.
And it relates because this is so contrary
to the state of an ordinary human being,
which is to incline towards hiding one's faults
and becoming upset when people point out our faults,
from criticizing people for being critical.
Everyone's a critic, they say.
And to some, there are teachings that instructs students.
Teachers will sometimes instruct students
or there's books that are written that say you should accept criticism
and thank anyone who gives you criticism.
And I think there's some truth to that.
But the response and the skepticism is that
leads you to allow others to walk all over you.
And to give unjust criticism and to leave unjust criticism
unchallenged.
And I think that also is a good point.
And it goes against the Buddhist teaching.
In the monastic society,
there's a monk should be criticized,
not criticized, but among is wrong.
Is that fault when they get angry at criticism?
So respond angrily or criticize in return.
So you tell me, I did this wrong.
And I said, well, you did this other thing wrong.
Or to ignore people's criticism and not accept
their criticism.
All of this is faulty.
But what's also faulty is to not speak in one's defense.
One is at fault when one doesn't speak in one's defense.
So there's a sense of having to be mindful
and to be wise and to be balanced.
In many issues that we deal with in life,
Buddhism doesn't have a be-all-and-all answer.
Buddhism is much more dealing with the building blocks.
What are the things that make up any given situation?
So there are much, much more general principles.
And the general principle is to be able to discern
the truth from falsehood and right from wrong.
But here we come to this specific example
of when the criticism is warranted.
So we're dealing with someone who actually points out
true faults.
Because I think it can be said that there's quite often
a grain of truth in every criticism.
There's a reason why we're being criticized.
Often people will criticize us unwarranted
and not wishing to help us.
So people criticize us often looking to upset us
or looking to humiliate us or looking to hide their own fault.
We criticize others to hide our own faults, this kind of thing.
But even still, even in those cases,
it's much easier to see the faults of others.
And we hide our own faults, but we're very good at picking out
the faults of others.
But here, in the case of a teacher,
we have another dilemma.
And that's our inability to accept reasonable
and our inability to accept criticism,
not because it's not correct,
because it shows a fault.
It hurts our ego.
It attacks something that we are protecting,
and that is ourself.
We hold ourselves dear.
We cling to an image of who we are or who we want to be.
And when that image is threatened,
like anything, any belonging, anything we hold dear,
we react, we get upset.
And so this teaching is actually quite important
for meditators in a meditative setting,
because we need to take advice from others,
unless you're the kind of person who can become enlightened
miraculously by oneself.
We have to take advice from others.
And as a teacher, this is something that is quite familiar.
It often, it becomes so difficult
that teachers are often afraid to give advice.
And much of a teacher's role in duty
is to find ways to admonish one's students
without upsetting them.
Part of the great part of the skill of being a good teacher
is the ability to not upset one's students,
because anyone can give advice,
and a real sign, unfortunately,
of a poor teacher is not being able to.
It's not that they can't give advice,
but not being able to couch the advice
in such a delicate terms or means
that the student is actually able to accept it.
So you'll often hear people giving advice
and as a teacher, anyway, you cringe
because that's not going to work
and you're just making student upset.
What you're saying is, correct,
but it's not going to get through.
And that's unfortunate because it often makes it,
it makes the teacher's job more difficult
and it hampers the teacher's ability
to be frank with a student.
And often, a teacher's duty
is require subsetting the student
and sometimes you have no choice.
Your choice is to not teach them or to hurt them,
to upset them, and you have to gauge
how far you can push a student,
which is, unfortunately,
usually not very far.
And usually, unless you're dealing with a special person,
like Radha, which is why Sariputum is so happy,
but for most people,
it's very difficult for us to take criticism.
It's very hard for us to prevent the anger
and the self-righteousness
when people try to even well-meaning
try to help us.
And so we have to be aware of this.
And that's one of the important aspects of this teaching
that I think a lot of people react favorably to
when this imagery of pointing out very treasure.
So we try to remind ourselves of this
and to think of it as someone pointing out very treasure.
And that's why we'll come up with these teachings
where we tell people
that anyone who criticizes you,
you should thank them.
You should thank people who criticize you.
I don't think, I don't think it's,
as I said,
when a criticism is faulty,
I don't think it should be hesitant to point out
when it's faulty.
Like my teacher said,
if someone calls you a buffalo,
you just turn around and feel if you have a tail.
If you don't have a tail,
you should tell them I don't have a tail.
I didn't quite say it like that.
In fact, it's more annoying for yourself.
And I think that's the key that we should point out is
it requires wisdom.
You have to be wise and know
who is this person,
who is their motive for doing this?
First of all,
but regardless of their motive is their truth
behind what they're saying.
Criticism is a very big part of
the teaching dynamic, as mentioned,
but also a very big part of our practice
because of course it's dealing with ego.
It's a good test of our state of mind
and our purity of mind,
how well we're able to take criticism.
So, Criticism is actually a useful tool in our practice
to put ourselves in position
or to allow others to criticize us
and to be mindful and to meditate
on the criticism, on our reactions to the criticism,
seeing how our mind reacts.
And enlightened being is the same in both praise and blame.
When people praise them,
it's as though they didn't even hear it.
They don't have any pleasure.
They don't take pleasure when others.
They aren't excited when other people praise them.
But it's the same when others have insulted them
or criticized them.
It's also as though they didn't hear.
In a sense, they take it for what it is.
They take it at face value.
This person just said I'm doing something wrong
and said, okay, well, they look at it
and right, I was doing that thing wrong
or this person's upset at me
and they want me to do this
or they want me to stop doing that.
Okay, well, I'll stop doing it
because that would upset them.
You know, to a certain extent,
but they do everything mindfully
and with wisdom, knowing which is the right state.
It's not that there are pushover
and someone tells them, you know,
you're too fat, you should go on a diet.
They don't really see the point of that.
Why would I concern myself with my weight?
You're too skinny, you should eat more.
Well, that's not why I eat, so,
but they don't get upset, you see.
The point is they're in perturbed
in both praise and blame
and we have to look at both
because this is relating to the ego.
It's relating to our conceit,
our self, our view of self.
It's a good indicator.
And the other part of this quote
is in regards to staying with such a person.
The point, the specific point that things get better
and that's a very useful advice to give oneself
when one is angry
because you'll often get angry at your teacher.
It's a common thing.
I had one student recently who said,
I'm very angry at you.
You know, the reason I think that,
those are the words,
I'm very angry at you right now
because I wasn't even criticism.
It was asking to do something
that just seems that it seemed
that early ridiculous.
I've seen them totally above and beyond
what they were capable of.
And I said, well, I didn't.
What was it?
I didn't.
This was my idea.
I wasn't the one who created this decision.
And they said,
I'm very angry.
They're very angry about it.
And this is quite common.
It's quite common for meditators
to get angry at their teacher.
And so there's no shame in that.
And there's no reason to get upset.
It's why we ask forgiveness
when we do a meditation course.
We often formally ask forgiveness of the teacher
and the teacher asks forgiveness of the student
both when we start the course
and when we finish the course.
It's sort of clear the air.
But the point is,
this verse is quite useful.
It's been useful for me
to remind myself that no matter how difficult things get,
staying with a teacher and an meditation center.
We have to remember that
we want to do that which makes things better,
makes it better, makes us better people,
improves our situation, makes us happier.
We have to remember that we know this.
We know that staying in the meditation center
with a teacher is going to make things better.
We often make excuses.
I'm not ready or the environment's not right.
I'm not at the right point in my life.
I remember that this is all excuses
that the truth is staying
in the meditation center
with the meditation center.
Things will get better.
And whatever excuses we have for not staying
are invalid because here is a person
who is willing to help us with our faults.
The faults are what we're focusing on.
And people looking to criticize Buddhism
will use this as a criticism
and Buddhism is very pessimistic,
which is ridiculous.
Underly, ridiculous harm was a terrible, terrible criticism.
I mean by often by lazy people
who aren't interested in looking at their faults.
Because it's very damaging to say that.
The faults are the only reason
for self-development.
It is our faults that we want to focus on.
Not so that we can feel bad about ourselves
or feel horrible people,
but to actually fix something.
To make something better.
How do you make something better
without looking at what's wrong?
Looking at the problem.
And this is what we do.
If you don't have someone
who can show you your fault,
so you can point them out to.
You can help you fix them.
Obviously, it's not enough to point out one's faults,
but to actually help you fix them.
This is where things get better.
This is where true development and progress and goodness comes from.
So a very useful verse, something that we should remember.
Someone who points out your faults.
When they see them,
you should liken them to a person
who points out very treasure.
When you stay with,
you should associate with such a person.
The wise should associate with such a person.
When such a person,
when one associates with such a person,
things get better.
Not worse.
Say you whole team about you.
That's the verse for today.
That's our teaching of the Dhamupada.
Thank you for tuning in.
Wishing you all the best.
