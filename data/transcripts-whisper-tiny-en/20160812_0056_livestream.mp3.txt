Good evening and welcome to our live broadcast.
Today we are continuing with the Ungutrani Kaya.
I'm just going to go right to the next one because it's not terribly deep but it's interesting
for those wondering about the role the heavens play in Buddhism, so apologies that this
isn't a deep meditation teaching.
I don't have any meditators here right now so we're just going to go on from number
36 to number 37.
Apparently in Buddhism the Buddhist cosmology the Buddha described various levels of angels,
so there are Bhumadhi, which are angels that live around live on earth apparently.
They live in trees or whatever they delight in the fairies, I guess, you normally think
of the med.
And then there are the successively higher planes of angels.
There are the four great kings that if you maybe recognize some of you this from Chinese
mythology, which gets it from Buddhism.
Four great kings that look after the four directions, so apparently there are four angels
who also are involved with the earth but they take care of the four quadrants of the
earth.
Interactions doesn't really work because earth is around so I don't quite understand
it.
There are apparently four great kings and they report to the angels of the tawatings which
is the next level.
And tawatings I guess the highest angel realm that's directly interested in the earth.
tawatings at tawam means three and tings that means thirty, so it's the a, the ebb and of the
thirty-three.
It's called that because there were these thirty-three companions who went to heaven together
and they're led by Sakka.
Sakka is the king of the angels of the thirty-three, and so I guess a group of angels
who live in their own realm but somehow take an interest in the earth.
And so the four great kings report to them, four great kings are responsible for making
sure that everything on earth is, I guess, going according to whatever divine plan that
they have.
Of all this sounds fairly theistic or deistic, well, I suppose you could call it deistic,
although I'm not even sure quite what that word means, but there's, and there is a sense
of that in the time of the Buddha people who are obviously worshiping these angels.
And so the Buddha was acknowledging the existence of such angels, but what he denied was
the efficacy of prayer or the importance of prayer, that sure you could, you could pray
and, and beseech the gods for blessings or for mercy or so on, but there's no reason
to think that they would comply, nor is there any benefit from, or any spiritual benefit
to such activities.
They certainly didn't have the power to enlighten one or to bring the highest benefit.
And I guess that's really where Buddhism goes beyond a theistic religion, it's, it doesn't
deny the fact that angels can, and what you might call gods can potentially help human
beings.
It's, it's not even the point to suggest that they, that God can't move mountains as
they say in Christianity, which, anyway, it's that moving mountains wouldn't really
help you if, if you pray to God and, and God were to answer all of your prayers, still
wouldn't mean much because, well, the prayers that God can't answer are may I become enlightened,
may I be free from suffering, may I be free from greed, anger and delusion, may I become
a better person or just can't happen, doesn't come from the grace of God and the grace
of God, sure, maybe you could get rich, you could live in luxury, but you couldn't go
to heaven, God doesn't have the power to bring someone to heaven or prevent them from
entering heaven, nor does any God or angel have the power to send someone to hell, not
with some qualification because of course, beings can affect other beings, but the alternate
power comes from within, if your mind is pure, no one can send you to hell, but if your
mind is impure, it's possible to instigate, that's certainly possible that angels could
manipulate you and trick you into doing deeds that would send you to hell or manipulate
a person into sending them to heaven.
So, I mean, it just means that there is an acknowledgement whether you believe this or
not, that there are certain beings that are not quanted, not qualitatively different
from human beings in the sense that they still have defilements and they still have life
spans and they're still limited to affecting the physical realm.
They can't actually manipulate another person's mind, not directly, but anyway, there
is some order to the angel realms, for those of you who are interested and curious about
these things.
What's interesting about this suit, and what really makes it Buddhist, because obviously
talking about angels isn't particularly Buddhist, is that sake is the Buddhist.
The King of the Gods of the Thirty-Three is a follower of the Buddha and they're concerned
about the activities of human beings, the goodness of human beings.
It's a group, and it's not to say that all angels will be like this, but this group
of angels, particularly, is concerned as to whether human beings are honoring religious
people, honoring elders, behaving properly towards their father and mother, whether they're
keeping holy days in the sense of having religious observances, spiritual practice on
at least once a week, and doing good deeds, are they giving charity, or are they keeping
morality, or are they practicing meditation?
But it's kind of funny why they're interested in this, and somewhat self-serving or
pragmatic, I guess.
They're concerned with this because they say, if the four great kings report to them that
human beings are doing all these things, then they get really happy because they say,
oh, that means that's great, that means there'll be more angels in the future.
More people coming, and we'll have a greater retinue, it's actually somewhat of a base
desire to swell their ranks, and if they find out that people are doing bad things and
not doing good things, then they're sad because there won't be as many angels in the
future.
I mean, for us it simply means it's a reminder that the only way to go to heaven really
is to be a good person, is to have a pure mind, and all of that comes through doing good
deeds, being good to other people, being respectful to other people, being kind, having
a pure mind, not having ulterior motives or a crooked treacherous mind, not being obsessed
with base desires or hatred or aversion or grudging, grudges or that kind of thing.
If your mind can stay pure in this to file the world, then and only then can you
free yourself from it, from the coarseness, from the baseless, from the mundane, to enter
into something more pure, more peaceful, more sublime.
And then there's something funny that Sakha, the King of the Angels, reminds the angels,
says, the person who would be like me should basically do that, so good deeds.
The person who would be like me should keep the holy, keep the holy day, keep the
both at that.
Which means this day where people keep, by the eight precepts.
And the Buddha says, you know, when he said this, it wasn't well said, it wasn't a proper
verse to say, not because it's not good to keep the religious precepts or to practice
goodness and that kind of thing, but because Sakha, who should, who, no one should strive
to be like Sakha, because Sakha is still not devoid of last hatred and delusion.
And he says, but I say, the same thing a person would be like me should observe all of
these things.
Because I am, because, I know not me, sorry, it's not me, a bikhu who has, who has become
an Harahan, someone who has become enlightened, not just the Buddha, but anyone who has
become enlightened should therefore say, the person who would be like me, and who is
fitting.
So, I mean, this kind of puts angels in their place and it's really how is Buddhists,
we look at the world and we don't see angels, most people will live their lives never
having any evidence that these beings exist.
But insofar as you might think that they do exist, there's still, there's still subject
to the same laws and regulations of the universe that goodness, goodness is what leads
to heaven and evils what leads to hell, these beings don't have the power to make people
better people, but they are happy to know that people do, they rejoice as all good people
should.
I mean, we rejoice for we see more people meditating, it means there will be more meditators,
it's in fact not a terribly bad thing to think because it makes our practice easier, makes
it easier to do good deeds when other people are keen on good deeds.
When everyone's doing unwholesome deeds and subsessed with the filaments, it becomes more
difficult to do good things because you get sucked, you get drained by the negativity
of others.
Anyway, so that's a little bit of dhamma for today anyway, I'm just working anyway.
That's all about that.
Do we have any questions?
We do Monday.
Oh wait, one, sorry, one, we're just talking about I should mention it first.
Just a reminder to people that we may have young people coming on this site and any sort
of adult conversation about sexual intercourse or other things, I guess drugs, alcohol should
be moderated or moderate, tempered, so just be mindful of where we are and the fact that
we're not all of age, so not that anything inappropriate has been done or said, but
just a reminder that to keep it PG or there's potential problems with talking about such
things to people who are potentially underage, okay, that's all.
Okay, in practical insight meditation, Mahasi Saya Dot talks about reflecting on certain
things, but to also notice reflecting, to keep it to a minimum, but you seem to be saying
it's not necessary to think or reflect on all during meditation and it's only necessary
to teach, et cetera, so are you disagreeing with him or am I missing something and how
is it possible to garner insight without reflecting on your experience?
I don't think I said that it's not, like reflection has a place in terms of stepping back
and reflecting on the quality of your practice, but the only benefit there is to adjust
your practice.
If your practice is going fine, I can see, you know, there's no benefit to reflecting
on the things that you've learned, certainly reflecting is natural, that it's something
you should be mindful of, but it's not the practice, and I mean, read what the Mahasi
Saya Dot says about what we call nyana.
Nyana is one of the ten Upa Kiles, when your knowledge, the knowledge is that you gain
when you become obsessed with it, fixated on it, when you start thinking about it, that's
actually to the detriment of your practice.
So the question about how could insight arise if you don't reflect, I mean, that's not
how insight is, insight is a direct realization of things, it's not reflection, when you
see that things are arising and ceasing, that's insight, and the more you see that, the
closer you'll get to letting go, once you see it clearly, you'll just have kind of like
an epiphany where you're not even, not even a thought, but it's just an observation that
everything that arises is ceases, and there's nothing worth clinging to, and in the mind
let's go, there's no, there's no place in there for reflection, I think it's a misunderstanding
of what insight is, insight is not a thought, insight is actually weepasana, busana means
seeing, weet means clearly, or especially, or whatever you want to translate, weet really
means with wisdom, bhanya, bhanya actually just means bhanya, knowledge, but complete, you
know, when you know that this is rising, when you know that this is falling, that's actually
wisdom, when you see it arising and ceasing, that's wisdom, that's all you need, I don't
think Mahasya Saya does suggest otherwise, but you'd have to probably read the Burmese
or be sure that it was properly translated, and then you have to understand that, you know,
he generally makes allowances, you know, if he's actually advising people to stop and reflect
and I'd be quite surprised.
But on the other hand, there is an advice for stepping back and reflecting in so far as
it helps you see whether your practice is correct, whether you're actually doing things
properly and reflect on whether you're adequately, you know, you're really missing something
or whether something could be adjusted, that's called the Mungsah, it's one of the four
really deep onto one of the four roads to success.
I don't know, I mean, they're just words, you'd have to see what the actual state was, someone
who says, I don't care, I mean, it sounds like it actually exists, but it's just words
and it's just a view and it's a lot of ego in the sense of, you know, I am above that
kind of thing, but they're just words, I mean, when an experience occurs, are you actually
an unreactive towards it?
Because such people actually, when they are presented with something that does affect
them, they react, the point is, not everyone is reactive towards everything, for some
people, some things are not going to create a reaction, but that's, you know, it's not
a sign that they are, I mean, apathetic, it's just a sign that they, that that thing
is doesn't create, it doesn't have any kind of history with that person.
So, you know, for some people, loud noises, it's obnoxious here in the west, if your neighbors
are having a party, you get quite upset.
In Asia, if your neighbors are having a party, people don't even seem to hear it, it's
quite interesting to see some of the differences from culture to culture, the sort of things
that bothers, bother people from different cultures.
A general sense of apathy, I mean, there can be a delusion, delusion is generally associated
with, with equanimity, and I guess that's really the technical answer, is that the questions
whether it's associated with delusion, but the delusion would be kind of this distracted
thought, if a person is engaged in mental fermentation, if they're thinking about things
and pondering and wondering and confused and that kind of thing, that's delusion, and
there's, there's equanimity with it.
And then there's the, the other, the wholesome kind is with equanimity, one does something
good or one calculates wholesomeness in the mind through meditation, but the equanimity,
you know, I guess the point is the equanimity is the same, but it's nothing to do with
equanimity.
It's the delusion, whether in the mind there's ego or there's confusion or there's mental
distraction, whether the mind is clear or the mind is, is clouded.
Thank you, B
Bumptay.
Is there any point being grateful karma is going to manifest any ways, could or bad, how
should we include gratitude in our practice?
Absolutely.
I mean, that's a, that's not a good outlook.
I mean, you think, okay, I shouldn't be grateful because that person's going to get what's
coming to them anyway.
Who cares?
Who cares whether to get what's coming to them or not?
That's not the point.
Gratitude is a wonderful state of mind.
Gratitude is, we don't worry about other people.
We're not grateful, so that it helps the other person, we're grateful, so that we don't
become mean and terrible people who have no sense of appreciation for the good deeds of
others.
Gratitude is something that should come naturally.
And if you find yourself ungrateful or unmindful of the good deeds of others, you should
note that and try to understand why, is it because I'm selfish or self-absorbed?
It's generally based in due to either one of the two, a strong sense of greed and or a
strong sense of a strong delusion being self-absorbed and, or, you know, it can also come
from anger, I suppose, if you're totally obsessed with your own suffering so that when
people help you, you just fix and focus on your own problems and how it, how your own
problems are solved or not solved, rather than thinking about the person who helped you.
Gratitude is most apparent in those who have the least attachment.
Those who are least concerned about their own well-being, right?
People who are most able to appreciate the good things in others are those who have good
things in themselves or those who aren't concerned about their own good things in the
sense that they don't have feelings of guilt or worry, that people who have lots of unpleasant
things inside tend to be too self-absorbed, worried about their own well-being, concerned
and greedy or angry and frustrated and upset and deluded and conceded.
You can see how all of those things get in the way of gratitude.
So when all of those gone, it's very easy to appreciate the good things in others.
You're able to be mindful of the truth and that's that this person helped you.
I wouldn't try to cultivate it too much, I'd just be more aware as to whether you have it
or not and if you don't have it, there's a sign that you've got something blocking it.
Can you change the letter broadcast to a different time?
Is there someone else who would want that?
That's a good question.
I can change it to another time.
This time happens to be fairly convenient for me.
Won't be convenient on Monday soon, but apart from that, I can't think of a better time
for me.
I could do it early afternoon, but that would probably be a bad choice for most of our
viewers since most of our viewers are in North America, which I know doesn't really
help those of you who are in Europe, but if you look at the meditation list, the majority
of the USA, if you Canada and India and Europe, then we got a bunch of Mexico as well.
Which I mean, the question is what is the cause, what is the effect, whether it's because
we're broadcasting in the evening, that could just be because of the time, the Europeans
aren't going to be on much anymore, except for Rustlin.
Yeah, I just read down a little bit, it was because the person often has a lot of questions
to ask during the live broadcast, but decided that he would be patient and use it as
a practice, where you could get really fancy Monday and have Tuesday and Thursday's early,
and Monday, Wednesday, Friday, and the normal time.
I can't even keep track of my schedule as it is, I don't know which day it is, and most
days.
Keeping it simple is good too.
But during my last meditation session, this suddenly felt like my legs were not covered
by my skin.
The legs still not like mine, but not the skin, it was sickening to my stomach, and I still
prepped the same right.
Well that's not practice, you see, that's that's experience.
The practice is how you relate to that experience.
Be very clear that these are two distinct things, what you experience and what arises
based on your practice is not the practice, whether it be knowledge, or whether it be
strange experiences like this, or even whether it be unpleasant experiences, defilements.
If anger arises, that's not the practice, it's not like, oh, I'm very angry, I must
be doing something wrong.
Anger is an experience, it's a reaction, it is a bad thing, but don't worry about that,
that's not your concern.
What happened, your concern is what you're going to do about it.
So when you have this experience, how are you going to react to it?
In fact, they're disgusted, potentially problematic, because there's a version disliking.
So that's not the proper reaction, but now that that's happened, you should say disliking,
disliking or disgusted, disliking is probably better.
If you're just aware of it, then you would say knowing, knowing, aware, or something.
If it's a feeling, you might say feeling, feeling, then that's the practice.
Then you'll say, you know, it comes and it goes.
I think the problem is it's practice is terribly unremarkable.
The practice itself is too simple, it's unremarkable.
That's it, you just say, thinking, thinking, seeing, feeling, feeling, angry, angry.
But it's made up for the fact that when you do that, there's fireworks, the things that
you're observing are incredibly interesting, there's a real transformation that goes on.
So be clear that the practice is incredibly simple, it's not much to it, but the experiences
that you're going to have are diverse and very terribly exciting, if you let them be.
Which, of course, you shouldn't, you should be mindful of it.
Conting during eating meditation is the best practice to always keep your eyes open, or
can the eyes be closed while chewing and falling.
They certainly can be closed while chewing and falling, there's no rule, hard or fast,
but if you're really keen on it, closing your eyes is a good thing.
That's your focus on, focus more on the mouth and the throat, taste and so on.
Were the current android apps still be working when the new cycle is left?
No.
You know, we probably could make it work if someone wanted to make a better Android app
would be great, but on the other hand it might be nice just to use the website as it is
because then we only have to work on one thing.
But the new site looks a lot like the Android app, if you've seen it.
So yeah, give up the Android app once the new site goes live.
You know, there's an iOS app apparently, but I think that might work, see.
And you still can sign in to the regular site on a phone, so it's just a matter of wanting
to be portable.
You can still lock your time in, just through a regular browser on the phone, done that.
Well yeah, the new site will look a lot like the Android app anyway, it won't be as hard
to do.
Like this one you have to find a little button, but on the new one it'll be much more
mobile friendly.
My sitting meditation is usually stressful and it seems to be getting worse.
It feels like I'm forcing myself to stay seated.
Do I just continue to work through it?
Yeah, I mean, this is a case where you might want to use my lungs as step back and reflect
on what you might be doing wrong, what you might be missing, not doing wrong exactly, but
the aversion is what you should be meditating on, you know, if you feel stressed and focus
on the stress, it's not easy and this is where the fireworks happen, this is where the
simple technique does a number on you, it really messes with you, challenges you, it
is called, you know, it sounds like probably, you know, it sounds a bit, you're doing fine
because you're cultivating patience.
If it feels like you're forcing, then it's not that you're forcing, it's that you're
missing something, that you're not actually being mindful of the aversion, try and
be as mindful of the aversion as you can as quick, catch what's really happening because
there's no such thing as meditation.
There's no, you don't have sitting meditation, it doesn't exist.
What happens when you sit down and close your eyes and then you watch the stomach now?
The tendency probably is to try and force the stomach to rise and fall, which is an interesting
thing, it's an interesting fact that your mind, you know, this is the case that your mind
is doing that, you know, the things that your mind, that our mind does as what we're
interested in, we're interested to learn how the mind works, to learn about the good
things and the bad things in the mind, what we're doing right, what we're doing wrong,
it sounds like probably like all of us, you're doing something wrong, so just keep watching
and you'll slowly see, oh, I'm doing this all wrong, boy, I should just stop that.
Can I do mindful of meditation without mantras?
You can, but you'd have to find a different teacher and a different tradition, because
some people say that you shouldn't use mantras, in our tradition we say you should and
it's actually important to use mantras, so not in our tradition, sorry, I mean, why would
you want to?
I guess it's the question, of course, I understand why people want to, but to some
leaves you shaking your head because they're so beneficial.
That's the most powerful thing we have is this tool reminding ourselves that the best
meditation technique I can think of, people who don't use them are missing out, don't
know what they're missing out of.
You're all kind of my questions, Monday, alright then, that's all for tonight, thanks Robin
for your help, thanks everyone for coming out, for meditating and for your questions, have
a good night.
Thank you, Monday, good night.
