1
00:00:00,000 --> 00:00:07,200
Hi, the latest question is from Scott, who is in Belfast.

2
00:00:07,200 --> 00:00:11,200
I have been practicing the Theravada tradition for just under a year since taking refuge

3
00:00:11,200 --> 00:00:13,280
in the Buddha, Dhamma, and Sangha.

4
00:00:13,280 --> 00:00:18,440
My question is, why do we, in the Theravada tradition, not recognize the Buddha's and Bodhisattvas

5
00:00:18,440 --> 00:00:20,640
from other traditions?

6
00:00:20,640 --> 00:00:27,640
Well, the Theravada tradition, Theravada means the teaching of the elders, so we follow

7
00:00:27,640 --> 00:00:34,280
a fairly narrow set of teachings, it's the teachings that are considered by all schools

8
00:00:34,280 --> 00:00:46,640
to be the most authentic or most original teachings of the historical Buddha, and we follow

9
00:00:46,640 --> 00:00:51,360
those teachings to become enlightened, so it's not so much an issue whether we accept

10
00:00:51,360 --> 00:00:54,280
Buddhas and Bodhisattvas, we're not really interested.

11
00:00:54,280 --> 00:00:58,880
Here we have what we understand to be a fully enlightened Buddha, which simply means someone

12
00:00:58,880 --> 00:01:03,440
who's come to see the truth as it is, and we have his teachings in their pristine form.

13
00:01:03,440 --> 00:01:11,800
We don't really see the point of going after or even seeming like inventing Buddhas and Bodhisattvas

14
00:01:11,800 --> 00:01:16,080
or even becoming a Buddha or a Bodhisattva or a Bhadhisattva, or a Bodhisattva being someone

15
00:01:16,080 --> 00:01:18,160
who's trying to become a Buddha.

16
00:01:18,160 --> 00:01:23,440
As far as accepting Bodhisattvas, there certainly is an acceptance of Bodhisattva ideal

17
00:01:23,440 --> 00:01:27,520
in the Theravada tradition, in fact there's even an understanding that there's another

18
00:01:27,520 --> 00:01:33,240
Buddha going to come in this age, you know, probably not for a while, but somewhere in

19
00:01:33,240 --> 00:01:37,960
the distant future there's another Buddha who's going to come down right now, he's in

20
00:01:37,960 --> 00:01:45,840
his last life up in heaven and he's going to come down from there to become a Buddha.

21
00:01:45,840 --> 00:01:52,280
And also anyone, according to the Theravada tradition, anyone who makes a vow to become

22
00:01:52,280 --> 00:01:57,080
a Buddha is a Bodhisattva, that's the same in all traditions.

23
00:01:57,080 --> 00:02:06,280
The understanding is, in the Theravada anyway, that until you've had your vow verified

24
00:02:06,280 --> 00:02:12,600
by another Buddha, you're considered to be an Anyitha Bodhisattva, which means a Bodhisattva

25
00:02:12,600 --> 00:02:14,480
who is uncertain.

26
00:02:14,480 --> 00:02:17,480
So you've made this vow to become a Buddha, but who knows whether you're going to become

27
00:02:17,480 --> 00:02:22,120
a Buddha, you might forget about it in some future life and get off track.

28
00:02:22,120 --> 00:02:24,000
You might never become a Buddha.

29
00:02:24,000 --> 00:02:27,160
If you've had the only way to be sure that you're going to become a Buddha, obviously,

30
00:02:27,160 --> 00:02:33,280
is to have someone who can see that far ahead and to understand that you are set to become

31
00:02:33,280 --> 00:02:36,920
a Buddha and say to you, you're going to become a Buddha.

32
00:02:36,920 --> 00:02:41,440
And that's then called the Nyata Bodhisattva, which means someone who is sure to become

33
00:02:41,440 --> 00:02:42,960
a Buddha.

34
00:02:42,960 --> 00:02:50,360
So the acceptance of Bodhisattvas in the Theravada is on two levels.

35
00:02:50,360 --> 00:02:56,120
So we accept anyone who makes the vow as a Bodhisattva, but we don't believe that they

36
00:02:56,120 --> 00:02:58,320
are sure to become a Buddha.

37
00:02:58,320 --> 00:03:03,720
Only someone who has been guaranteed as the historic Buddha, suppose that there was.

38
00:03:03,720 --> 00:03:10,800
The story goes that way back in one of his past lives, he was an ascetic and he was verified

39
00:03:10,800 --> 00:03:11,800
by another Buddha.

40
00:03:11,800 --> 00:03:17,040
The Buddha said, this ascetic here is going to become a Buddha sometime in the future.

41
00:03:17,040 --> 00:03:20,200
And so from then on, he was assured to become a Buddha.

42
00:03:20,200 --> 00:03:24,480
It was sure because we had someone who could verify it.

43
00:03:24,480 --> 00:03:29,280
As far as other Buddhas, it's just not really of interest to us.

44
00:03:29,280 --> 00:03:30,280
Bodhisattvas also.

45
00:03:30,280 --> 00:03:35,640
If you want to become a Bodhisattva, if you're idealist to become a Buddha, which means someone

46
00:03:35,640 --> 00:03:40,960
who realizes the teachings for themselves, then that's fine.

47
00:03:40,960 --> 00:03:47,840
The difference between someone who follows a Buddha and someone who becomes a Buddha

48
00:03:47,840 --> 00:03:50,480
themselves is like this.

49
00:03:50,480 --> 00:03:53,320
To become enlightened, you have to let go of everything.

50
00:03:53,320 --> 00:03:56,160
This is true for a Buddha, a fully enlightened Buddha.

51
00:03:56,160 --> 00:04:01,000
It's also true for an Anubuddha, someone who becomes enlightened by following after, by

52
00:04:01,000 --> 00:04:02,520
following the teachings.

53
00:04:02,520 --> 00:04:05,800
But to become a Buddha, you not only have to let go of everything, you also have to know

54
00:04:05,800 --> 00:04:06,800
everything.

55
00:04:06,800 --> 00:04:08,960
You have to come to understand everything.

56
00:04:08,960 --> 00:04:14,480
So that's quite a more difficult task.

57
00:04:14,480 --> 00:04:19,600
It's something that takes a lot more time and a lot more effort, obviously.

58
00:04:19,600 --> 00:04:23,520
And it's not necessary to let go of everything you don't need to know everything.

59
00:04:23,520 --> 00:04:28,600
You simply have to come to understand that no matter what could arise has to cease, and

60
00:04:28,600 --> 00:04:30,840
there's nothing in the universe worth clinging.

61
00:04:30,840 --> 00:04:35,040
So you understand everything to the extent that's necessary to become enlightened.

62
00:04:35,040 --> 00:04:38,560
Okay, so I hope that helps and thanks for the question.

