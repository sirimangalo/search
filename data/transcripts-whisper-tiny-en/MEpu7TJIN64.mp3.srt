1
00:00:00,000 --> 00:00:14,840
Good evening everyone, welcome to our daily dhamma, almost daily because Wednesdays and

2
00:00:14,840 --> 00:00:28,760
Thursdays I'm off, but otherwise pretty much every day.

3
00:00:28,760 --> 00:00:39,400
Today I wanted to look at the very beginnings of insight meditation in our study group.

4
00:00:39,400 --> 00:00:51,800
Today we just finished the Banyaboomi, the soil of understanding or the foundation of

5
00:00:51,800 --> 00:00:52,800
understanding.

6
00:00:52,800 --> 00:01:01,240
So a lot of technical stuff that was really a slog and it's to the credit of the students

7
00:01:01,240 --> 00:01:07,600
that everyone made it through that, but next we're going to be looking at the beginnings

8
00:01:07,600 --> 00:01:17,080
of insight, the beginnings of practical and insight knowledge.

9
00:01:17,080 --> 00:01:20,640
And so it's good for us as meditators, it's not just the very first stages, it's the

10
00:01:20,640 --> 00:01:28,520
very foundation of knowledge on which we base the rest of our practice from here on in.

11
00:01:28,520 --> 00:01:36,320
So it's important that we're clear about this and always referring back to it, it's not

12
00:01:36,320 --> 00:01:44,320
just beginner stuff.

13
00:01:44,320 --> 00:01:50,400
The very base at the very foundation of insight meditation is on, it's called Dittiwi

14
00:01:50,400 --> 00:01:58,040
Sundi, it's based on the purification of view that's based on an understanding of what

15
00:01:58,040 --> 00:02:04,960
really exists.

16
00:02:04,960 --> 00:02:11,760
And not just an intellectual understanding, but it's starting to see the nature of reality

17
00:02:11,760 --> 00:02:22,800
of what it is, it's starting to see that in all of us, in each of us, in the universe

18
00:02:22,800 --> 00:02:32,680
in which each of us inhabits the reality of the nature of us occupies, there are two

19
00:02:32,680 --> 00:02:42,240
aspects, means are called Rupa, Nama, or Nama, and Rupa.

20
00:02:42,240 --> 00:02:55,200
Each and every one of us has two parts, made up of two things, Nama, and Rupa.

21
00:02:55,200 --> 00:03:01,640
Because all that each of us has is our experiences, if we get down to the very basics

22
00:03:01,640 --> 00:03:11,520
of what actually exists, always see in the end, is a moment after a moment of experience.

23
00:03:11,520 --> 00:03:14,680
That's as far down as we can go.

24
00:03:14,680 --> 00:03:23,240
Of course, based on these experience, we come to conjecture that a lot of other things exist.

25
00:03:23,240 --> 00:03:31,520
We have the idea that our bodies exist, of course, get the idea that the room around

26
00:03:31,520 --> 00:03:38,160
us, the world around us, exists, the objects that we possess, or that we come in contact

27
00:03:38,160 --> 00:03:43,840
with, so we assume that other people actually exist, but all this is conjecture.

28
00:03:43,840 --> 00:03:51,080
What we can know for sure is that experience exists, and that's what we can perceive and

29
00:03:51,080 --> 00:03:52,080
verify.

30
00:03:52,080 --> 00:03:55,560
That's it.

31
00:03:55,560 --> 00:03:58,080
Beyond that, it's all abstraction, and this is important.

32
00:03:58,080 --> 00:04:05,080
It's not a philosophical argument of whether anything, something exists or doesn't exist.

33
00:04:05,080 --> 00:04:14,040
It's that there's a qualitative difference, categorical difference, if you're an experiencing

34
00:04:14,040 --> 00:04:24,560
or knowing an experience, and knowing, say, a friend or an object, and you look into

35
00:04:24,560 --> 00:04:35,960
a tree and you say that tree exists, knowing that is far less concrete and real than knowing

36
00:04:35,960 --> 00:04:39,960
that they're seeing when you see the tree that there's an experience of seeing.

37
00:04:39,960 --> 00:04:48,520
Experience with seeing is far more real, so that's the basis of all of reality, but then

38
00:04:48,520 --> 00:05:02,840
we see something curious about reality, and it's made up of two parts.

39
00:05:02,840 --> 00:05:08,360
Normally we call these the physical and the mental, nama and rupa, nama means nama is

40
00:05:08,360 --> 00:05:16,320
the mental, it's called saramana, that which takes an object, different from rupa which

41
00:05:16,320 --> 00:05:24,880
is a nama, doesn't take an object, but that means as nama is able to know things, nama

42
00:05:24,880 --> 00:05:32,960
is that aspect of reality that is aware, conscious, consciousness.

43
00:05:32,960 --> 00:05:39,760
Rupa is that acknowledged aspect of reality that doesn't know anything, but it's a part

44
00:05:39,760 --> 00:05:50,280
of experience, all the same, when each of our experience is different, there's so much involved

45
00:05:50,280 --> 00:06:00,120
in experience, you can say it's all experience, but there's so much, and it seems seeing

46
00:06:00,120 --> 00:06:05,880
is different from hearing, hearing is different from spelling, liking is different from

47
00:06:05,880 --> 00:06:13,640
disliking, even the various experiences of the body are different, experiencing heat and

48
00:06:13,640 --> 00:06:23,840
cold is different from experiencing tension or hardness, softness, and when we breathe in

49
00:06:23,840 --> 00:06:31,360
our stomach, our body expands, the expansion is accompanied with tension, that tension is

50
00:06:31,360 --> 00:06:38,120
a part of the experience, I mean it is an experience, and then when the breath goes out

51
00:06:38,120 --> 00:06:45,320
there's a release, and that releases is of a different quality from the expansion as a

52
00:06:45,320 --> 00:07:08,780
different feeling to it, but the idea behind all of this is to remove from our minds and

53
00:07:08,780 --> 00:07:16,200
from our psyche, the views that we carry around about what's real, the confusion, the idea

54
00:07:16,200 --> 00:07:23,440
that there's a self or a soul, the idea that the body exists and allows us to differentiate

55
00:07:23,440 --> 00:07:34,560
between concept and reality and to see that, and to see that so much of what we think

56
00:07:34,560 --> 00:07:43,960
of is really just all in our mind, it's important because it sets the stage for our understanding

57
00:07:43,960 --> 00:07:49,800
of how reality works, which is in turn important because that's where all the problems

58
00:07:49,800 --> 00:07:56,520
come from, we really do have problems, we have bad habits, we cause our self suffering,

59
00:07:56,520 --> 00:08:03,760
we cause suffering to others, so just like if you want to fix a car or an engine you

60
00:08:03,760 --> 00:08:08,720
have to take it apart and look at what's really going on inside, you can't just kick

61
00:08:08,720 --> 00:08:19,440
it or keep trying to turn the key and hoping that it works, you can't pray to the gods,

62
00:08:19,440 --> 00:08:26,960
you have to actually look inside, it's the difference between a person who owns a car

63
00:08:26,960 --> 00:08:33,880
and the mechanic who fixes the car, ordinary people, people who drive cars and when it's broken

64
00:08:33,880 --> 00:08:41,960
they take it to the mechanic, the magic that gets fixed, so they think in terms of the car,

65
00:08:41,960 --> 00:08:46,560
but the mechanic looks at a car and they know all the pieces of the car, is what we're

66
00:08:46,560 --> 00:08:56,280
trying to become to our meditation practice, and learn how to fix the car, and so we have

67
00:08:56,280 --> 00:09:01,120
to see deeper than ordinary reality, ordinary reality will get you through the day, I mean

68
00:09:01,120 --> 00:09:08,120
if you think of this room existing, these people exist, it's very useful, what it's useful

69
00:09:08,120 --> 00:09:12,640
to think that the car exists because then if I go outside that car is there, you don't

70
00:09:12,640 --> 00:09:17,440
have to just stand there saying seeing, you know that if you pull the door will open and

71
00:09:17,440 --> 00:09:30,960
you can sit down and turn the key and drive away, you can't go the other route of understanding

72
00:09:30,960 --> 00:09:40,840
nature of reality and freeing yourself from suffering, and so what we're doing here is

73
00:09:40,840 --> 00:09:48,320
quite different, we're trying to take apart the motor, so to speak, take apart the engine

74
00:09:48,320 --> 00:09:56,840
and clean it and fix it and put it back together.

75
00:09:56,840 --> 00:10:07,080
So a route by, a route by basically is the four elements, what exists, what we experience

76
00:10:07,080 --> 00:10:15,400
hardness and softness, that theoress element, the Earth is just a name, it doesn't actually

77
00:10:15,400 --> 00:10:22,080
mean there's Earth there, it's just really a term for it, but we have this aspect of

78
00:10:22,080 --> 00:10:27,360
the physicality, when we experience what's physical sometimes there's softness, sometimes

79
00:10:27,360 --> 00:10:36,320
there's hardness, and there's the tension when you watch the stomach rising, there's the

80
00:10:36,320 --> 00:10:41,520
tension in the stomach, when you watch it falling, there's the acidity, the release of

81
00:10:41,520 --> 00:10:50,240
tension, there's the air element, that's what we call it, it's just a name, the fire element

82
00:10:50,240 --> 00:10:57,360
to experience the heat and the body are cold, both of those are the fire element, then

83
00:10:57,360 --> 00:11:03,320
the water element isn't really part of experience, but it's conceived to be there as well,

84
00:11:03,320 --> 00:11:10,080
it's the cohesion, the stickiness, you don't actually experience it, so that's just a technicality,

85
00:11:10,080 --> 00:11:18,280
the point being, this is what we mean by Rupa, so when you watch the stomach rising,

86
00:11:18,280 --> 00:11:36,440
what is it that's rising, the nama or the Rupa, what is it that's rising, does anyone

87
00:11:36,440 --> 00:11:45,840
have the answer, let me just confuse the heck out of you, what is it that rises, nama

88
00:11:45,840 --> 00:12:11,440
or Rupa, what is it that's rising, nama or Rupa,

89
00:12:11,440 --> 00:12:26,640
Rupa, Rupa rises, what is it that knows that the Rupa is rising, when you walk which

90
00:12:26,640 --> 00:12:38,040
one walks, Rupa or nama, which one knows that you're walking, when you talk which one

91
00:12:38,040 --> 00:12:48,000
talks Rupa or nama, Rupa, which one knows that you're talking, or knows what you're

92
00:12:48,000 --> 00:13:07,280
going to say, does Rupa know what you're going to say, no, very basis of insight to be able

93
00:13:07,280 --> 00:13:12,400
to see this, it doesn't really matter, you don't have to think about them too much, it's

94
00:13:12,400 --> 00:13:16,000
just the fact that you can see them means you've gone beyond the idea of me and mine

95
00:13:16,000 --> 00:13:26,720
and I, or I anyway, you've gone beyond views of self and so on, and we're not talking

96
00:13:26,720 --> 00:13:31,200
about that, we're not cultivating a view that the self doesn't exist or something like

97
00:13:31,200 --> 00:13:36,200
that, what we're saying is stop thinking in terms of views and start looking in terms

98
00:13:36,200 --> 00:13:45,120
of experiences and observations, to get beyond that, so many people who hear this, they

99
00:13:45,120 --> 00:13:49,080
start to worry and think, what's happening, what's going to happen to myself, they're

100
00:13:49,080 --> 00:13:53,480
missing the whole point because they're still thinking and not yet, they haven't yet

101
00:13:53,480 --> 00:14:01,720
penetrated this wall between intellectualization, reviews and ideas and abstractions about

102
00:14:01,720 --> 00:14:11,080
reality and an actual experience of reality, so the use of asking these questions and

103
00:14:11,080 --> 00:14:21,080
talking about this is to make clear the difference and help to see the distinction between

104
00:14:21,080 --> 00:14:30,680
reality and concept, so what you should start to see, you should start to see that Nama

105
00:14:30,680 --> 00:14:36,760
and Rupa arise and cease when you walk, stepping right, that there's the arising of the

106
00:14:36,760 --> 00:14:41,920
experience in the cessation, when you move your left foot, stepping left, there's the

107
00:14:41,920 --> 00:14:48,200
arising and there's the cessation, so contrary to what we might believe about reality,

108
00:14:48,200 --> 00:14:57,920
reality is born and dies every moment, anything that we think of as existing as lasting

109
00:14:57,920 --> 00:15:04,240
is really just a thought, a concept in our minds.

110
00:15:04,240 --> 00:15:10,640
We conceive of the same experiences or similar experiences again and again, and we conceive

111
00:15:10,640 --> 00:15:20,280
of them as being an entity, you see the same thing and you say, oh, that's a car, yep,

112
00:15:20,280 --> 00:15:26,240
car's still there, you look again, you see again the car, but the reality, the experience

113
00:15:26,240 --> 00:15:40,200
arises and ceases, it's born and dies.

114
00:15:40,200 --> 00:15:46,640
This is really what this is what gets through that idea, the view of self, the second aspect

115
00:15:46,640 --> 00:15:51,840
is how they relate to each other, in terms of cause and effect, and this gets into

116
00:15:51,840 --> 00:16:00,240
the idea of karma, talking about karma today, I mentioned that karma is not quite what

117
00:16:00,240 --> 00:16:01,240
we think it is.

118
00:16:01,240 --> 00:16:05,920
I think of karma as you do something like you kill someone and then you suffer the retribution

119
00:16:05,920 --> 00:16:13,200
in a future life, maybe someone kills you, but that's not what karma really means, or

120
00:16:13,200 --> 00:16:19,040
it's not the precise truth about karma.

121
00:16:19,040 --> 00:16:24,640
There's a moment when you have an ethical mind state and ethical, we just mean something

122
00:16:24,640 --> 00:16:33,720
that is going to have consequences, it's a value placed on the experience, you like something

123
00:16:33,720 --> 00:16:44,440
or you dislike something, or you have a clear objective mind, an attachment and a version

124
00:16:44,440 --> 00:16:54,400
or a delusion or there's clarity and contentment in different ways of reacting to experiences.

125
00:16:54,400 --> 00:17:04,400
That's karma, and that experience will change or will bring about results, but the result

126
00:17:04,400 --> 00:17:11,760
of karma is just more experience, so experiences are actually the result of karma.

127
00:17:11,760 --> 00:17:17,920
Last few begin to see, the second thing that you begin to see in the practice is you begin

128
00:17:17,920 --> 00:17:23,080
to see not only are these things arising and ceasing when I step over and I sit or when

129
00:17:23,080 --> 00:17:29,360
I think or when I feel, but they're related, if I get really angry I'm going to feel

130
00:17:29,360 --> 00:17:34,160
a lot of pain in the head, tension in the body, if I get worried I'm going to feel stress

131
00:17:34,160 --> 00:17:36,040
in the body, right?

132
00:17:36,040 --> 00:17:42,720
That's all that karma means, that we cause ourselves suffering.

133
00:17:42,720 --> 00:17:47,000
On the other hand, if I'm mindful, if I see things clearly it's like untying and not and suddenly

134
00:17:47,000 --> 00:17:55,960
I'm free and clear and peaceful in the mind, the result is positive, of course a very

135
00:17:55,960 --> 00:18:02,480
important thing to see, this is the beginning of the four noble truths, we start to see

136
00:18:02,480 --> 00:18:10,960
that suffering is caused by all sorts of defilements in the mind, any kind of clinging,

137
00:18:10,960 --> 00:18:19,200
any kind of attachment to the experience or reaction to it, and that peace and freedom

138
00:18:19,200 --> 00:18:48,440
from suffering comes from not reacting, but rather experiencing and seeing things as they are.

139
00:18:48,440 --> 00:18:56,760
There was one of, I was reading an article about childbirth, it was a really interesting

140
00:18:56,760 --> 00:19:08,280
article about childbirth, this woman, she gave birth in the hospital and she recounts

141
00:19:08,280 --> 00:19:21,680
the trauma involved, the pain and the stress and the physical torture involved, eventually

142
00:19:21,680 --> 00:19:29,200
there was, she had scarring by the end of it and had to have surgery and she was completely

143
00:19:29,200 --> 00:19:41,000
drugged up, it was just quite a shocking thing to read, just to think about how painful

144
00:19:41,000 --> 00:19:48,880
childbirth can be, so I mean reading it just made me think of this as part of life, part

145
00:19:48,880 --> 00:19:54,840
of the nature of life, but then she goes on to talk about how she got pregnant a second

146
00:19:54,840 --> 00:20:00,760
time and started thinking about the suffering, is this a part of life, is this what I have

147
00:20:00,760 --> 00:20:06,680
to look forward to and she was terrified of having to give birth and then she started

148
00:20:06,680 --> 00:20:15,160
to do some research and she found that this curious idea that people were talking about

149
00:20:15,160 --> 00:20:20,520
giving birth at home and anyway the idea is eventually she gave birth but she had her second

150
00:20:20,520 --> 00:20:29,000
pregnancy or second childbirth at home and the point, the reason why I'm telling you this

151
00:20:29,000 --> 00:20:36,360
is because of the lack of stress, the childbirth was completely different and it was peaceful

152
00:20:36,360 --> 00:20:45,320
she didn't have to have any medication, she, there was no problems or trauma or stress

153
00:20:45,320 --> 00:20:53,080
she didn't, it was a completely different story, so we're given this article to read

154
00:20:53,080 --> 00:20:59,200
one of our professors gave it to us, it just made me think of, that's a very good example

155
00:20:59,200 --> 00:21:05,040
of how the mind and the body work together, a very real example and a very powerful one

156
00:21:05,040 --> 00:21:13,240
and you often think that while childbirth is those who have experience with it, you often

157
00:21:13,240 --> 00:21:20,400
think that childbirth is just a traumatic experience, it's very painful, scary and scary

158
00:21:20,400 --> 00:21:26,000
because it's painful because the body is unprepared but the funny thing is is that the body

159
00:21:26,000 --> 00:21:34,680
can be quite prepared and the body is quite plastic as it relates to the mind meaning

160
00:21:34,680 --> 00:21:41,720
the mind has such power over the body, I think this is a great example where we can talk

161
00:21:41,720 --> 00:21:46,640
about how the mind can cause you stress and the body when you're anxious, your heart

162
00:21:46,640 --> 00:21:53,760
starts beating rapidly, you know, you ever think about how that works, what really happens

163
00:21:53,760 --> 00:22:00,680
is the defilements in the mind, the bad habits in the mind cause stress, cause physical

164
00:22:00,680 --> 00:22:09,120
harm but nothing says it more than this reality of childbirth, how stressful it can

165
00:22:09,120 --> 00:22:15,760
be and then you think it's completely, you know, it's mainly caused by the fact that

166
00:22:15,760 --> 00:22:24,200
my mind was stressed, that I was afraid, these are the consequences, this is the importance

167
00:22:24,200 --> 00:22:30,440
of understanding how Rupa and Nama work, the theory that I'm giving you is not important

168
00:22:30,440 --> 00:22:36,440
but it challenges you to ask yourself, am I seeing Nama and Rupa and that's what's important

169
00:22:36,440 --> 00:22:41,400
because once you see it, once you look and you see Nama and Rupa for yourself, that's

170
00:22:41,400 --> 00:22:46,400
where the doors open, that's where your life begins to change and where you enter on

171
00:22:46,400 --> 00:22:54,280
this path of purification, where gradually slowly over time, long time short time depends

172
00:22:54,280 --> 00:23:00,600
on you, you free yourself from all these bad habits and all these problems and you stop

173
00:23:00,600 --> 00:23:06,720
hurting yourself, stop causing yourself stress and suffering.

174
00:23:06,720 --> 00:23:12,880
So I think that's important, it's important at least to affirm this and to remind ourselves

175
00:23:12,880 --> 00:23:18,880
the basis of our practice is Nama and Rupa, the basis of our practice is the physical and

176
00:23:18,880 --> 00:23:30,520
mental aspects of experience, it's not me or mine or I, it's not beings or it's just

177
00:23:30,520 --> 00:23:36,280
moments, moments of experience, when we do walking meditation, we're not trying to walk

178
00:23:36,280 --> 00:23:41,640
to get to the walk, what's the point, you just have to turn around and walk back, are

179
00:23:41,640 --> 00:23:51,120
the point is to be aware of each movement, because that's Nama Rupa, when you sit it's

180
00:23:51,120 --> 00:23:56,600
not about getting control of the breath, it's about watching each movement of the breath

181
00:23:56,600 --> 00:24:02,240
because that's Nama Rupa, when you watch the Rupa, you're going to see Nama, as you watch

182
00:24:02,240 --> 00:24:06,840
the stomach rising and falling, you learn all sorts of things about the Nama, but you're

183
00:24:06,840 --> 00:24:13,680
mind, as he's sometimes there's stress over it, sometimes there's desire over it, expectations

184
00:24:13,680 --> 00:24:21,840
over it, fear or worry, and sometimes there's clarity, sometimes there's mindfulness,

185
00:24:21,840 --> 00:24:35,520
then this is where all of our insight meditation occurs, so there you go, little bit of

186
00:24:35,520 --> 00:24:54,320
Nama for tonight, thank you all for tuning in, we're sure all good practice.

