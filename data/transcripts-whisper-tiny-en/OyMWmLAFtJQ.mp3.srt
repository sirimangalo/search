1
00:00:00,000 --> 00:00:25,680
This is my latest project, it's called Ask a Monk, and this is something that I think

2
00:00:25,680 --> 00:00:34,080
I mentioned already, I put it together, based on these many other, ask somebody or ask

3
00:00:34,080 --> 00:00:38,280
somebody.

4
00:00:38,280 --> 00:00:54,080
And so here's a list of the questions that people have asked, and what they do is they just

5
00:00:54,080 --> 00:00:57,080
post questions, and then people can rate them.

6
00:00:57,080 --> 00:01:03,380
So they have all these questions that they want to ask me about Buddhism and meditation

7
00:01:03,380 --> 00:01:06,480
and so on.

8
00:01:06,480 --> 00:01:12,680
And then I just look at them and I see which ones are getting a lot of votes, or I'll say

9
00:01:12,680 --> 00:01:19,080
I look and see which ones I think are appropriate to answer, and I answer them.

10
00:01:19,080 --> 00:01:23,160
And how I answer them is I make YouTube videos.

11
00:01:23,160 --> 00:01:30,040
So I would, the reply to this, I haven't put any responses yet, but I would post a response

12
00:01:30,040 --> 00:01:35,240
and then just put a YouTube video and a link to the YouTube video in here.

13
00:01:35,240 --> 00:01:41,120
And then I make the video YouTube video post it and then post the response here.

14
00:01:41,120 --> 00:01:50,600
I've just started this and already I've got eight questions, let me see, so many questions,

15
00:01:50,600 --> 00:01:57,360
eight questions so far, and 43 votes, so people are voting on them as well, which means

16
00:01:57,360 --> 00:02:00,480
I better get at it and start answering them.

17
00:02:00,480 --> 00:02:18,960
Here's the first video, I'm the internet slow because I'm also uploading my, I'm

18
00:02:18,960 --> 00:02:20,960
putting my life in a day videos.

19
00:02:20,960 --> 00:02:49,960
Okay, so this is just one more thing that I do, and that's that.

