1
00:00:00,000 --> 00:00:08,000
I may be attached to the idea of becoming a monk. Is it wrong to want to become a monk because I think that lay life is too troublesome.

2
00:00:08,000 --> 00:00:12,000
I can't find anything worth pursuing as a lay person.

3
00:00:16,000 --> 00:00:18,000
What do you think that's good?

4
00:00:18,000 --> 00:00:36,000
I think it's possible to become attached to some idea you have of what it will be.

5
00:00:36,000 --> 00:00:41,000
But whether it's wrong to want to become it, I think that depends on what your reasons are.

6
00:00:41,000 --> 00:01:01,000
If you have the right reasons, clearly, then it won't be wrong to want it or to understand those reasons and pursue it for those reasons.

7
00:01:01,000 --> 00:01:08,000
There's this big discussion about running away.

8
00:01:08,000 --> 00:01:14,000
It goes on where people say, when you hear often, becoming a monk is just running away.

9
00:01:14,000 --> 00:01:17,000
That's a criticism that non-butists level at months.

10
00:01:17,000 --> 00:01:25,000
It's also a caution that even monks will give to people thinking of ordaining.

11
00:01:25,000 --> 00:01:34,000
That if you're just running away, becoming a monk to run away from your problems, you're not going to get very far.

12
00:01:34,000 --> 00:01:44,000
This story that I just translated, Tukkungwawa had to open and suffering follows you.

13
00:01:44,000 --> 00:01:56,000
If you're suffering, the results of your bad karma follow you.

14
00:01:56,000 --> 00:02:07,000
Even if the ox is to pull the card for a day, two days, five days, a month, a year, they'll never get away from the wheel.

15
00:02:07,000 --> 00:02:20,000
If the more they try to go forward, the more they'll have to pull, the yolk will press down on their neck and then press them there.

16
00:02:20,000 --> 00:02:23,000
Actually, it's the circle, right? They have these circles that they put on the ox.

17
00:02:23,000 --> 00:02:27,000
That will press against their neck and flick them in that way.

18
00:02:27,000 --> 00:02:36,000
If they try to back up, they will back into the wheel and dig into their legs.

19
00:02:36,000 --> 00:02:41,000
Anyway, too much information.

20
00:02:41,000 --> 00:02:43,000
But it follows you.

21
00:02:43,000 --> 00:02:56,000
The idea that you can somehow escape your suffering, but I think that in some ways, and often that misses the point that people who have become monks in this sense are what they're trying to run away from is not their own problems,

22
00:02:56,000 --> 00:03:16,000
or say, not necessarily, or I think often what they're running away from is the acquiring of further problems, or getting caught up in the creation of further confusion and seeing how the more you get caught up in it,

23
00:03:16,000 --> 00:03:27,000
the more suffering you create, the more harmony you create, the more complicated becomes, and also just how useless it is.

24
00:03:27,000 --> 00:03:40,000
If you see how useless the lay life is, what did the Buddha do? He ran away from many things, but for the purpose of confronting himself, confronting his own mind.

25
00:03:40,000 --> 00:03:49,000
You know, quite clear what he was doing was not running away from that which is important, or running away from suffering, running away from his problems.

26
00:03:49,000 --> 00:03:52,000
In fact, that's a good example.

27
00:03:52,000 --> 00:04:00,000
To say that someone's leaving the home life, because they want to run away from suffering is a little bit absurd, because there's a lot of suffering.

28
00:04:00,000 --> 00:04:04,000
There's a good healthy dose of suffering and becoming a monk.

29
00:04:04,000 --> 00:04:19,000
But it's leaving behind, I think, from the sounds of it, this person is, in my mind, very much, or dating for the right reasons.

30
00:04:19,000 --> 00:04:29,000
Get away from all that stuff, run away, see if that means you're somehow attached to...

31
00:04:29,000 --> 00:04:35,000
I wouldn't worry about such questions about does this mean I'm attached to that, or does that mean I'm attached to this, or so?

32
00:04:35,000 --> 00:04:48,000
You're attached to many, many things, unless you're already far beyond most applicants from anastic life, you've got a lot of attachments.

33
00:04:48,000 --> 00:04:53,000
So don't worry about are you attaching to becoming a monk? It's really trivial.

34
00:04:53,000 --> 00:05:02,000
Do you have lust? Do you have hatred? Do you have addiction to things? Do you have ego and conceit and arrogance and so on?

35
00:05:02,000 --> 00:05:05,000
Much more important. That's where you should be focusing.

36
00:05:05,000 --> 00:05:17,000
If becoming a monk means, for you, the indulgence in the attachment of becoming a monk, I would say, you know, really that's the least of your concerns.

37
00:05:17,000 --> 00:05:30,000
Unless you're already free from all the other ones, then you might say, wow, at this point in my spiritual practice, I really have to work on my attachment to the monastic life, because it's such a...

38
00:05:30,000 --> 00:05:43,000
You know, it's like the attachment to humor or something, or your attachment to things which are attachments, but you know, far far too subtle to be concerned about.

39
00:05:43,000 --> 00:05:54,000
And so for the most part, people who ask these sorts of questions about being attached to meditation, for example, you know, I really think I have to, or do you really think it's good for this person to be meditating so much?

40
00:05:54,000 --> 00:06:10,000
It's like they're attached to it. I'm like, that's the least of their concerns, really. If they've still got greed, anger and delusion, yes, let them attach to meditation for a little while longer anyway.

41
00:06:10,000 --> 00:06:25,000
So I can't speak highly enough of monastic. What I would say is that I have been very, very much attached to the monastic life. And I'm willing to do that. When I became a monk, I was like, this is so cool.

42
00:06:25,000 --> 00:06:44,000
This is the cat's me now. It was really, you know, just the bliss of being able to ordain is just wonderful. I mean, of course, it was intense suffering and it has been intense suffering in many ways, but just because of how difficult it has been.

43
00:06:44,000 --> 00:06:59,000
But the fact of being a monk is just so wonderful. So I can understand that. I can understand the concern that it might be there. I just don't say that it's really the most important thing.

44
00:06:59,000 --> 00:07:14,000
It can grow into ego and you can see it to the does. When people become monks, they can be very conceited, arrogant, think their teachers, think they know everything. That's a danger. That's something that you have to work with and deal with.

45
00:07:14,000 --> 00:07:34,000
But even that I would say is rather know the arrogance and the conceipt and so on, that's far more important than the underlying attachment, which is all in good time, eventually you'll get there.

46
00:07:34,000 --> 00:07:40,000
Start digging from the top down.

47
00:07:40,000 --> 00:07:56,000
One reason why people become more of the ordain is to protect themselves. I've got all these defilements and I know I'm not going to be a very good monk, but if I don't ordain for me, it was like that. If I don't ordain, I'm going to get married and that's a scary thing.

48
00:07:56,000 --> 00:08:18,000
It was kind of a no-brainer.

