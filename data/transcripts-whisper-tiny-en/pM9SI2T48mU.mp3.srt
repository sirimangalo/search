1
00:00:00,000 --> 00:00:08,560
During meditation when one phenomenon tends to come up strongly and repeatedly after several

2
00:00:08,560 --> 00:00:14,080
acknowledgements is it okay to perhaps with gentle effort break away from it, especially if one

3
00:00:14,080 --> 00:00:20,320
has seen Duke in it or is it more important to stay with it until one can clearly see it's passing

4
00:00:20,320 --> 00:00:33,600
away. I think it depends because you may have something like a itch or a pain that'll come

5
00:00:33,600 --> 00:00:38,240
in on stay there and you can be noting it for a very long time and it won't go away

6
00:00:38,240 --> 00:00:52,720
soon you just go back to rising falling, gentle effort break away, but I don't know if that

7
00:00:52,720 --> 00:01:05,760
means to break away from it. I would say stay until it's passing away always because that makes

8
00:01:05,760 --> 00:01:16,320
you see something very important in the entire process of arising and ceasing. When you've in

9
00:01:16,320 --> 00:01:31,680
that example of an itch for example, a scratch or try to focus on something else, then you miss

10
00:01:31,680 --> 00:01:44,000
for you miss probably that you are having a wanting in your mind, a wanting to scratch and that

11
00:01:44,000 --> 00:01:51,760
there is eventually, I shouldn't say that word, that there is probably the after the wanting

12
00:01:53,120 --> 00:02:02,720
the intent not to move the arm and that there might rise the anger about the itch because you don't

13
00:02:02,720 --> 00:02:13,200
want to move and if you turn your mind, your focus away from that because you have enough of it

14
00:02:13,200 --> 00:02:21,760
then you will not understand that it it has passed away already when you start having a wanting

15
00:02:23,040 --> 00:02:32,960
in might the feeling of the itch might come back, you might and you may think yet that you are

16
00:02:32,960 --> 00:02:43,200
still feeling the itching, but that's not true. It was feeling the itching, then next present moment

17
00:02:43,200 --> 00:02:54,400
was wanting something, then the itching became most prominent again, then anger arose, so don't go

18
00:02:54,400 --> 00:03:08,080
over these things by break away from them. Well said, the only thing is that most of the time

19
00:03:08,080 --> 00:03:15,280
it doesn't work that way for especially beginner meditators, you will scratch and when you have

20
00:03:15,280 --> 00:03:27,840
pain, you will move and when you have lots of thoughts, you will or whatever, you will find

21
00:03:27,840 --> 00:03:37,360
yourself avoiding them and running away from them. So it's also important for us to know how to deal

22
00:03:37,360 --> 00:03:42,400
with that when it occurs. So when you're scratching something, even Mahatshi Sayyada, he explained

23
00:03:42,400 --> 00:03:46,960
it exactly like that, but he said, if you do scratch, you know, if you don't and then acknowledge it

24
00:03:46,960 --> 00:03:52,240
and eventually the itching will go away, but if you do, just do it slowly, the rubbing, rubbing and

25
00:03:52,240 --> 00:03:59,040
so on. Be mindful of it lifting or even wanting, wanting, then raising, raising. If you want to move,

26
00:03:59,040 --> 00:04:04,640
say wanting to move and lifting your foot and moving it and so on, saying lifting, lifting,

27
00:04:04,640 --> 00:04:16,240
moving, moving. Mindful of every movement, every motion, you know, if you want to shift your posture

28
00:04:16,240 --> 00:04:21,440
or sit up straight or sit, slouch back down or so on, then be mindful of that as well. If you find

29
00:04:21,440 --> 00:04:31,120
your head nodding down and bring it back up, raising, raising. So the best thing is to be able to

30
00:04:31,120 --> 00:04:38,960
stay with the object, but it may be that you aren't able to. Another thing, there are cases where

31
00:04:38,960 --> 00:04:45,680
you might not want to focus on it for a long time and that can be where it becomes obsessive.

32
00:04:46,400 --> 00:04:53,440
For example, you hear a sound and, you know, you hear construction, for example. There's a lot

33
00:04:53,440 --> 00:05:04,640
of lenses now in this cave and there's construction right out front. And so, in the beginning,

34
00:05:04,640 --> 00:05:09,520
you say hearing and, but your mind keeps going back to it and your mind keeps getting frustrated

35
00:05:09,520 --> 00:05:14,640
about it. And every time it goes back, it's like more and more agitation and more and more

36
00:05:14,640 --> 00:05:24,720
stress. And in that instance, you might be quite a bit better off to ignore it. You know,

37
00:05:24,720 --> 00:05:29,600
as you say, hearing, hearing to put it aside and try to make a conscious effort to focus instead

38
00:05:29,600 --> 00:05:38,160
on the rising and the falling and build up your concentration, go back to it only when it becomes

39
00:05:38,160 --> 00:05:42,880
prominent, not sitting there every time you hear the noise. Oh, here it is again hearing. Because

40
00:05:42,880 --> 00:05:48,560
the noise will be constant and it will come back again and again and again. So, it's important not

41
00:05:48,560 --> 00:05:53,120
to be pedantic about the noting that you have to note everything, you have to catch everything,

42
00:05:53,120 --> 00:05:58,960
you don't really. You have to be present and you have to be mindful. And if it's a continuous

43
00:05:58,960 --> 00:06:06,000
experience, in the case of sound or in the case of your fans, suppose you're in a room and there's

44
00:06:06,000 --> 00:06:10,880
the fan blowing on you, but you don't have to be mindful of it every time you can. And even

45
00:06:10,880 --> 00:06:17,360
when you're hearing something, you can stay with it. But I think it comes back to the idea of being

46
00:06:17,360 --> 00:06:24,160
skillful and learning when to roll with the punches and how to cut your losses and so on.

47
00:06:25,040 --> 00:06:30,000
And not be the perfect meditator, not expect that you should be the perfect meditator because you

48
00:06:30,000 --> 00:06:45,920
won't be the perfect meditator, at least not for long, long, long time.

