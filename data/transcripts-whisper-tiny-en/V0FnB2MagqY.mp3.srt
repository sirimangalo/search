1
00:00:00,000 --> 00:00:29,680
Good evening, everyone broadcasting live, March 10th.

2
00:00:29,680 --> 00:00:58,440
Today's quote is one of these quotes about the right time.

3
00:00:58,440 --> 00:01:17,800
How do you find the right time, how do you find the good time, how do you find the happy time?

4
00:01:17,800 --> 00:01:26,520
We look for happy times, we look forward to them.

5
00:01:26,520 --> 00:01:29,360
We work towards them.

6
00:01:29,360 --> 00:01:40,400
Even meditation we have the idea that it's going to lead us to happy, happier times in the

7
00:01:40,400 --> 00:01:52,160
future, meditation should lead us to happiness, right?

8
00:01:52,160 --> 00:01:56,720
And if you know anything about Buddhism or the Buddha's teaching, you know, the sort

9
00:01:56,720 --> 00:02:07,000
of ideas that he presents in regards to time, that looking for a happy time, thinking

10
00:02:07,000 --> 00:02:15,640
of a specific time or play, so situation and it's being happy, it's the wrong way to go

11
00:02:15,640 --> 00:02:22,080
about it, the wrong way to go about finding happiness.

12
00:02:22,080 --> 00:02:33,000
And the Buddha says, Yay, Bikwah, Sattah, Uban, Samayang, Aya, Nasuk-jari-tang-jari-tang,

13
00:02:33,000 --> 00:02:46,200
wa-jaya-suk-jari-tang-jari-tang-jari-tang-jari-tang-jari-tang-jari-tang-jari-tang-jari-tang-jari-tang-jari.

14
00:02:46,200 --> 00:03:07,280
Whenever monks, or monks, or because, whatever being performs, when a being performs bodily

15
00:03:07,280 --> 00:03:17,240
Good conduct with the body, good conduct with speech and good conduct of the mind

16
00:03:17,240 --> 00:03:20,440
in the morning.

17
00:03:20,440 --> 00:03:29,880
When one does that in the morning, Suphabhan coffee va-hety samsatanyaun, it's a good morning

18
00:03:29,880 --> 00:03:36,880
morning monks. It's a good morning for those being.

19
00:03:41,760 --> 00:03:48,760
We make a way that I imagine he, because I'm a young, whatever beings in the middle of the day,

20
00:03:48,760 --> 00:03:58,600
in the middle of the day, behave well with body, behave well with speech, behave well

21
00:03:58,600 --> 00:04:09,600
with mind, so much unequal, big cavities and some time. That's a good midday for those

22
00:04:09,600 --> 00:04:29,100
in the same evening. Very important to you. True, somewhat simple, I know. But important

23
00:04:29,100 --> 00:04:42,240
is still important. What it tells us about, what is the approach to happiness in a good

24
00:04:42,240 --> 00:04:49,240
time, that a good moment. So not katang sumang alang suba, suba, bahatang su kitaang. It is

25
00:04:49,240 --> 00:04:56,240
a good season. It is a good blessing. It's a good moment. So put it down. It is a good

26
00:05:19,240 --> 00:05:26,240
moment. It is a good moment. Actually, I'm not going with it. It says it. Good behavior.

27
00:05:50,240 --> 00:05:57,240
A good state. Happiness is not to be found in time or place, or not particular to time or place,

28
00:06:04,960 --> 00:06:11,960
not because of the specific time or place. Happiness is to be found in every time, in every

29
00:06:11,960 --> 00:06:30,040
place, or not, or found in no place, dependent only on one's own state. If one behaves badly

30
00:06:30,040 --> 00:06:36,840
with body, badly in speech, badly in mind, that's not a good time, no matter when it

31
00:06:36,840 --> 00:06:56,840
is. But, the right moment, a happy time, happiness, and be found anywhere in any time,

32
00:06:56,840 --> 00:07:10,280
is when the mind is at peace, when there is. When the body is properly behaved, when

33
00:07:10,280 --> 00:07:22,320
killing or stealing or cheating, when the speech is proper, not lying or hurting others

34
00:07:22,320 --> 00:07:34,320
with speech or using useless speech. And when the mind is well-directed, mindful, pure,

35
00:07:34,320 --> 00:07:48,160
clear, when your mind is directed in the right way, aware of the body, aware of speech,

36
00:07:48,160 --> 00:07:55,160
aware of the thoughts, clear in the thoughts with no anger, with no greed, with no delusion.

37
00:07:55,160 --> 00:08:05,520
At that moment, that's where happiness is. That's where the right moment. The moment

38
00:08:05,520 --> 00:08:11,280
comes anywhere, you find it anywhere, you don't need to go far. On this ambition and the

39
00:08:11,280 --> 00:08:19,680
striving that we have for finding happiness, it's all misdirected, misguided. Find happiness

40
00:08:19,680 --> 00:08:29,880
here and now. We're ever here and now, it's all over the world.

41
00:08:29,880 --> 00:08:48,280
At the land has Kita, we'll have the SaaS, they have tamed something useful, and they are

42
00:08:48,280 --> 00:08:59,280
happy to tell us. So it's actually a blessing, I think. It's a blessing that we do, and

43
00:08:59,280 --> 00:09:05,880
I can't remember exactly how translate. We'll have the SaaS name, they prosper in the

44
00:09:05,880 --> 00:09:12,760
Buddha SaaS manner. This is a chant that we do quite often, so not katam, so manga, so katam,

45
00:09:12,760 --> 00:09:22,200
so katam, so kanos, so motto, tasu yitam, ramu jahari, so tatakinangay, kamangay, kamangay, kamangay,

46
00:09:22,200 --> 00:09:32,420
kamangay, kamangay, peekanongma No kamangay, waynidin, hutakinai, kaatradapan Administat 1987,

47
00:09:32,420 --> 00:09:40,960
lattakinai', it's just puban,cling watching, so it's an

48
00:09:40,960 --> 00:09:46,320
importance to it, because of what it says at the top.

49
00:09:46,320 --> 00:09:56,000
really evening. Doesn't matter when, doesn't matter where. Happiness, goodness, the happy day.

50
00:10:00,880 --> 00:10:04,880
It's quite simple. It comes from the body, comes from speech, and from your mind.

51
00:10:06,080 --> 00:10:10,560
So that's the demo for tonight. Thank you all for tuning in.

52
00:10:10,560 --> 00:10:17,840
I guess we can go if anybody has questions. I'm going to hang out.

53
00:10:24,000 --> 00:10:29,680
So you can click on the link. If you got questions, turn the hangout.

54
00:10:29,680 --> 00:10:39,680
There are no questions. I'm going to talk for tonight.

55
00:11:00,640 --> 00:11:24,080
How are you? I'm good. Thank you. We're having some pretty heavy weather down here in

56
00:11:24,080 --> 00:11:34,960
Mississippi today and tonight. And I don't expect to keep internet in a little while. We're probably

57
00:11:34,960 --> 00:11:43,360
maybe going to lose internet. But other than that, doing good. I have some kind of hurricane.

58
00:11:44,560 --> 00:11:53,280
Just a big frontal system moving across the southern, the western part of the south

59
00:11:53,280 --> 00:12:00,720
deep south. It seemed to be kind of regular here lately with real high winds and

60
00:12:01,760 --> 00:12:07,440
off and on rain and flash flood warnings and tornado watches and that sort of thing.

61
00:12:09,680 --> 00:12:11,440
I attribute it to climate change.

62
00:12:16,160 --> 00:12:19,040
But good to see you. Good evening. Hope you're having a good day.

63
00:12:19,040 --> 00:12:24,080
Yeah, you too. No question, I guess.

64
00:12:25,600 --> 00:12:33,200
Yeah. Looks like no one else can come on. I'm curious, walking meditation

65
00:12:36,800 --> 00:12:42,560
and doing half and half walk and half sitting. So if I want to sit from 45 minutes or an hour,

66
00:12:42,560 --> 00:12:50,320
then I need to walk for 45 minutes or an hour. And I wonder what the teravada

67
00:12:51,280 --> 00:13:05,040
forest traditions view is on walking meditate using one of these labyrinths that you see

68
00:13:05,040 --> 00:13:13,520
against a walking pattern that's kind of, you know, typically out of doors, a large walking

69
00:13:13,520 --> 00:13:21,280
pattern that you do kind of weave from the outside, follow this path intricately to get to

70
00:13:21,280 --> 00:13:28,320
the middle and then you turn around and walk back out. And it's not one that you get lost in. It's

71
00:13:28,320 --> 00:13:39,440
one that, you know, you kind of meditatively walk slowly along rather than as an option to just

72
00:13:39,440 --> 00:13:47,280
walking in a straight line, say 10 or 15 feet walking, turn around, come back, go back and forth.

73
00:13:48,160 --> 00:13:50,400
And it's just a curiosity question.

74
00:13:50,400 --> 00:14:03,360
Why would you walk in a different way? Well, it's a way of having,

75
00:14:06,800 --> 00:14:17,200
it appears, I really don't know that much about them, but I say an area that's maybe 20, 30, 40 feet

76
00:14:17,200 --> 00:14:28,320
across the conference. And you start at a point on the outside and it's like a little, a defined

77
00:14:28,320 --> 00:14:36,080
path that goes around, kind of back around on itself, because they, and you eventually work to

78
00:14:36,080 --> 00:14:44,240
the middle of the pattern that would point you turn around and follow that same path,

79
00:14:44,240 --> 00:14:54,640
a little trail back out, just kind of a novel looking thing. And I didn't know if it was something

80
00:14:54,640 --> 00:15:08,640
that's contrary, evidently, a number of faiths or religions make use of it around the world,

81
00:15:08,640 --> 00:15:18,240
presumably some Buddhist and some Christian, you know, like Orthodox Christian churches and such

82
00:15:18,240 --> 00:15:22,480
as that. So it's really just a curiosity thing.

83
00:15:26,400 --> 00:15:27,440
Never heard of it.

84
00:15:28,320 --> 00:15:33,920
Okay. I don't have a, I don't have a picture of one handy.

85
00:15:33,920 --> 00:15:46,400
And I do want to ask about the walking meditation instruction,

86
00:15:50,480 --> 00:15:57,040
you instructed me to pick my foot up for the next step, pick it straight up,

87
00:15:57,040 --> 00:16:04,880
take a step and place it so that I'm lifting, placing, lifting, placing, lifting, placing.

88
00:16:05,840 --> 00:16:14,560
And to lift my foot, flat footed straight up and then go forward with it and place it straight

89
00:16:14,560 --> 00:16:26,560
down, is causing me to take some pretty small steps. And it doesn't bother me, but it's almost,

90
00:16:26,560 --> 00:16:34,400
it makes the walking process much more tedious than it's requiring a lot,

91
00:16:34,400 --> 00:16:41,920
well, it's requiring a little more attention to to the mechanics of the walking process.

92
00:16:42,640 --> 00:16:47,920
And I'm just wondering if there's a, if that's what's intended,

93
00:16:47,920 --> 00:16:57,360
and I think I'm doing it right. Well, watch the video, you can see how I do it in the video.

94
00:16:59,360 --> 00:17:08,080
I, um, I kind of lost the video that I was able to watch it that one time, about six of the

95
00:17:08,080 --> 00:17:18,720
eight seconds and then it, it didn't continue and I don't have it to be in the hangout. Okay,

96
00:17:18,720 --> 00:17:25,920
nor hangout. It'll be a Lincoln hangout. Yeah. Okay, super.

97
00:17:28,960 --> 00:17:31,280
And also go ahead.

98
00:17:31,280 --> 00:17:42,320
And then one other question, I, you're saying during the, um,

99
00:17:43,360 --> 00:17:54,000
Domitalk in the evenings that some are watching it on YouTube. Um, so is the live stream,

100
00:17:54,000 --> 00:18:02,000
um, live broadcast that's available on the meditation plus site. Is it also available as a

101
00:18:02,000 --> 00:18:15,040
YouTube visual? This one is. Okay. Okay. Okay. Okay. And how do, how would I reach that?

102
00:18:15,040 --> 00:18:25,360
Go to the YouTube, YouTube, my YouTube channel. Okay. Okay. Well, I, I evidently didn't do something

103
00:18:25,360 --> 00:18:36,080
right because I, I went, I went to it, but all I saw available were previous, uh, previous episodes.

104
00:18:36,880 --> 00:18:41,440
Yeah, I don't, I'm not that familiar with it. Actually, I just pretty sure that it works.

105
00:18:41,440 --> 00:18:50,320
It's not, it was not always a video. Like, okay. And I don't always record video. Okay. No big deal.

106
00:18:50,320 --> 00:18:59,360
I was just curious about that too. Okay. Well, I know you're busy. So I won't, I look like I'm the

107
00:18:59,360 --> 00:19:06,000
only one hot years. So I won't hold you up. All right. Take care. Take care. Practice on. Thank you very

108
00:19:06,000 --> 00:19:16,000
much. Bye. Good night.

