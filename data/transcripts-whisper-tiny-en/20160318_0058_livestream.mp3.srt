1
00:00:00,000 --> 00:00:26,000
Good evening everyone, broadcasting live March 17th, hopefully the audio is working fine.

2
00:00:26,000 --> 00:00:45,000
Tonight's quote actually seems a lot like a repeat and on the face of it is pretty simple quote.

3
00:00:45,000 --> 00:00:56,000
But as Nanda asks, it's an interesting question.

4
00:00:56,000 --> 00:01:05,000
If we only look at the first part of the quote, we might not jump to the answer.

5
00:01:05,000 --> 00:01:10,000
We may not immediately think of the answer.

6
00:01:10,000 --> 00:01:17,000
I think I did, I think when I read it I was like oh I know what he's going to say.

7
00:01:17,000 --> 00:01:18,000
Of course.

8
00:01:18,000 --> 00:01:20,000
I don't know actually once he said the eight things he said there are eight things.

9
00:01:20,000 --> 00:01:30,000
I said what conditions when developed and practiced lead to Nirvana have Nirvana as their goal culminated in Nirvana?

10
00:01:30,000 --> 00:01:39,000
And he says there are Nanda eight things and then of course we know what he's going to say.

11
00:01:39,000 --> 00:01:46,000
Eight things when you develop eight things and you practice eight things and leads to Nirvana.

12
00:01:46,000 --> 00:01:48,000
They have Nirvana as their goal.

13
00:01:48,000 --> 00:01:53,000
They culminate in Nirvana.

14
00:01:53,000 --> 00:01:57,000
And of course then he says the eight full noble paths.

15
00:01:57,000 --> 00:02:06,000
Right view, right thought, right speech, right action, right livelihood, right effort, right mindfulness,

16
00:02:06,000 --> 00:02:17,000
and right concentration.

17
00:02:17,000 --> 00:02:22,000
It's a simple quote.

18
00:02:22,000 --> 00:02:25,000
We might not have much to say about it except.

19
00:02:25,000 --> 00:02:32,000
It interests me particularly having just written an article.

20
00:02:32,000 --> 00:02:41,000
Some when I would consider non-mainstream Buddhist teaching.

21
00:02:41,000 --> 00:02:56,000
And so it gives opportunity to sort of highlight this quote as an example of really right Buddhism.

22
00:02:56,000 --> 00:03:03,000
It's easy to lose sight.

23
00:03:03,000 --> 00:03:11,000
The core practices that we're aiming for.

24
00:03:11,000 --> 00:03:20,000
We got caught up in the theory and my argumentation really.

25
00:03:20,000 --> 00:03:30,000
And there's so many new doctrines, new practices, new ideas.

26
00:03:30,000 --> 00:03:42,000
The idea that things have to be that there is a degradation or more it seems that

27
00:03:42,000 --> 00:03:52,000
the old, well the old stops working and so rather than try to figure out why what we're doing wrong,

28
00:03:52,000 --> 00:04:04,000
we change the parameters.

29
00:04:04,000 --> 00:04:15,000
And so on a sort of the microscopic or the personal level.

30
00:04:15,000 --> 00:04:26,000
An example of this is where we lose sight of mindfulness.

31
00:04:26,000 --> 00:04:38,000
And something happens and we're unable to resolve the situation.

32
00:04:38,000 --> 00:04:43,000
For example, we might have bright lights or colors or eyes.

33
00:04:43,000 --> 00:04:58,000
We might have rapturous experiences of excitement in the body or in the mind.

34
00:04:58,000 --> 00:05:06,000
We might have sounds or smells or tastes or feelings.

35
00:05:06,000 --> 00:05:14,000
I don't know how to deal with it.

36
00:05:14,000 --> 00:05:20,000
And we lose sight of the very basic practice we can get lost in exceptional experiences.

37
00:05:20,000 --> 00:05:25,000
The ten Upa Kieleces are good examples of it.

38
00:05:25,000 --> 00:05:33,000
Some of them I mentioned actually.

39
00:05:33,000 --> 00:05:42,000
Where we can get caught up in trying to find causes for our problems and creating stories

40
00:05:42,000 --> 00:05:46,000
for while I'm this way because of this and this and this.

41
00:05:46,000 --> 00:05:52,000
A lot of people come with the idea that they're stuck in their practice.

42
00:05:52,000 --> 00:06:09,000
I hear that a lot, I've just gotten stuck in my practice, which is probably in ninety percent of the cases.

43
00:06:09,000 --> 00:06:17,000
It's just a miss and a wrong focus.

44
00:06:17,000 --> 00:06:23,000
I've been lost focus of the core practice and there's no way to get stuck.

45
00:06:23,000 --> 00:06:25,000
There's no such thing as things.

46
00:06:25,000 --> 00:06:26,000
We're all stuck.

47
00:06:26,000 --> 00:06:27,000
We're stuck in these bodies.

48
00:06:27,000 --> 00:06:37,000
We're stuck with these minds that are imperfect malfunctioning.

49
00:06:37,000 --> 00:06:50,000
It's not a really a matter even of getting unstuck in the situation.

50
00:06:50,000 --> 00:06:55,000
If you're stuck, what you should really be asking is does that bother you?

51
00:06:55,000 --> 00:06:58,000
Does it bother you that you're stuck?

52
00:06:58,000 --> 00:07:00,000
How do you feel about that?

53
00:07:00,000 --> 00:07:04,000
Do you identify with that experience?

54
00:07:04,000 --> 00:07:08,000
And moreover, I guess what do you mean by being stuck because that's just a concept

55
00:07:08,000 --> 00:07:12,000
and gets back to this idea of losing sight of what's really happening.

56
00:07:12,000 --> 00:07:16,000
What does it mean you have something sticky on your fingers?

57
00:07:16,000 --> 00:07:17,000
No.

58
00:07:17,000 --> 00:07:20,000
When you say I'm stuck, it doesn't mean anything.

59
00:07:20,000 --> 00:07:23,000
It's a conclusion that you come into.

60
00:07:23,000 --> 00:07:26,000
It's not an observation.

61
00:07:26,000 --> 00:07:28,000
Observations are momentary.

62
00:07:28,000 --> 00:07:29,000
They rise and they cease.

63
00:07:29,000 --> 00:07:32,000
There's nothing to stick to.

64
00:07:32,000 --> 00:07:37,000
The only thing sticky is our reactions to things.

65
00:07:37,000 --> 00:07:43,000
It relates to this quote because of how simple the quote is.

66
00:07:43,000 --> 00:07:47,000
If you have these eight things, that's all you need.

67
00:07:47,000 --> 00:07:49,000
You're reading that question and you're wondering,

68
00:07:49,000 --> 00:07:51,000
yeah, what is it that leads to it around?

69
00:07:51,000 --> 00:07:52,000
What do you need to get?

70
00:07:52,000 --> 00:07:57,000
You're like, all right, it's just the hateful normal path.

71
00:07:57,000 --> 00:08:07,000
It's kind of almost anti-climatic because you think

72
00:08:07,000 --> 00:08:09,000
there must be more than that.

73
00:08:09,000 --> 00:08:10,000
That's it.

74
00:08:10,000 --> 00:08:12,000
That's all of the news.

75
00:08:12,000 --> 00:08:17,000
This is why people create new teachings, new ideas because,

76
00:08:17,000 --> 00:08:19,000
well, yeah, I know the eight full normal path,

77
00:08:19,000 --> 00:08:21,000
but there must be something more than that.

78
00:08:21,000 --> 00:08:25,000
Besides the full normal path.

79
00:08:25,000 --> 00:08:28,000
It's like I remember I went to a teacher once,

80
00:08:28,000 --> 00:08:31,000
and I was really sort of depressed about my situation,

81
00:08:31,000 --> 00:08:36,000
and I'd gotten real trouble with my,

82
00:08:36,000 --> 00:08:39,000
with a layman who was my teacher at the time.

83
00:08:39,000 --> 00:08:42,000
They were saying all sorts of nasty things about me,

84
00:08:42,000 --> 00:08:46,000
and I felt kind of like, gee, maybe I'm an evil person.

85
00:08:46,000 --> 00:08:49,000
So I went to this one of the monks, and I said,

86
00:08:49,000 --> 00:08:52,000
how do you know if you're an evil person?

87
00:08:52,000 --> 00:08:56,000
I was really in my midst, and he said,

88
00:08:56,000 --> 00:08:58,000
and he gave this very stock answer.

89
00:08:58,000 --> 00:09:01,000
He said, well, there are these ten types of evil,

90
00:09:01,000 --> 00:09:05,000
the Akusula Kamapada, which are,

91
00:09:05,000 --> 00:09:08,000
and they relate actually to the eight full normal path

92
00:09:08,000 --> 00:09:11,000
in some ways, basically wrong action,

93
00:09:11,000 --> 00:09:14,000
wrong speech, and wrong thought.

94
00:09:14,000 --> 00:09:21,000
And he said, if you're breaking any of those,

95
00:09:21,000 --> 00:09:22,000
that's evil.

96
00:09:22,000 --> 00:09:23,000
And I thought about it, and I said,

97
00:09:23,000 --> 00:09:25,000
well, what if you're okay with all of those,

98
00:09:25,000 --> 00:09:28,000
but you still feel like you're an evil person?

99
00:09:28,000 --> 00:09:32,000
It's like that kind of, it was a silly question,

100
00:09:32,000 --> 00:09:33,000
really.

101
00:09:33,000 --> 00:09:37,000
I think too much about the things.

102
00:09:37,000 --> 00:09:42,000
I think there's a problem.

103
00:09:42,000 --> 00:09:44,000
I think something's wrong.

104
00:09:44,000 --> 00:09:47,000
When in fact, it comes down to reality,

105
00:09:47,000 --> 00:09:48,000
is it good or is it bad?

106
00:09:48,000 --> 00:09:49,000
Is it right?

107
00:09:49,000 --> 00:09:51,000
Is it wrong?

108
00:09:51,000 --> 00:09:53,000
It is or it isn't?

109
00:09:53,000 --> 00:09:54,000
Very simple.

110
00:09:54,000 --> 00:09:56,000
Dhamma is very simple.

111
00:09:56,000 --> 00:10:06,000
And so I made a statement about one text recently

112
00:10:06,000 --> 00:10:09,000
as being sofistic,

113
00:10:09,000 --> 00:10:10,000
sofism,

114
00:10:10,000 --> 00:10:15,000
and that's disagreeable to people who like that text,

115
00:10:15,000 --> 00:10:17,000
and I understand that,

116
00:10:17,000 --> 00:10:21,000
it's probably problematic to be bringing these things up

117
00:10:21,000 --> 00:10:22,000
on the internet,

118
00:10:22,000 --> 00:10:26,000
but the point is,

119
00:10:26,000 --> 00:10:28,000
is this,

120
00:10:28,000 --> 00:10:29,000
you know,

121
00:10:29,000 --> 00:10:30,000
this is not sofistic,

122
00:10:30,000 --> 00:10:33,000
this is simple.

123
00:10:33,000 --> 00:10:35,000
And there's no,

124
00:10:35,000 --> 00:10:38,000
there's no high or exalted teachings here.

125
00:10:38,000 --> 00:10:40,000
There's a perfect teaching,

126
00:10:40,000 --> 00:10:42,000
and it's a very simple teaching,

127
00:10:42,000 --> 00:10:43,000
right view.

128
00:10:43,000 --> 00:10:45,000
Do you understand the foreign-able interests?

129
00:10:45,000 --> 00:10:46,000
You know what is suffering?

130
00:10:46,000 --> 00:10:48,000
Do you know those things that are suffering?

131
00:10:48,000 --> 00:10:50,000
Do you know them as suffering?

132
00:10:50,000 --> 00:10:52,000
No, do you?

133
00:10:52,000 --> 00:10:54,000
Are you?

134
00:10:54,000 --> 00:10:55,000
Yeah.

135
00:10:55,000 --> 00:10:56,000
Wow, basically when you do,

136
00:10:56,000 --> 00:10:57,000
you give up.

137
00:10:57,000 --> 00:10:58,000
The causes of it,

138
00:10:58,000 --> 00:11:00,000
do you understand the cause of suffering?

139
00:11:00,000 --> 00:11:02,000
Do you understand the sensations of it?

140
00:11:02,000 --> 00:11:05,000
Do you understand the path that leads to the cessation of suffering?

141
00:11:05,000 --> 00:11:08,000
Which is the ageful moment of that?

142
00:11:08,000 --> 00:11:09,000
That's right view.

143
00:11:09,000 --> 00:11:11,000
If you know those things.

144
00:11:11,000 --> 00:11:12,000
That's really,

145
00:11:12,000 --> 00:11:14,000
it's quite simple.

146
00:11:14,000 --> 00:11:19,000
There's nothing complicated about it.

147
00:11:19,000 --> 00:11:21,000
You know, the taste of the dhamma,

148
00:11:21,000 --> 00:11:23,000
the taste of this ageful-normal path,

149
00:11:23,000 --> 00:11:26,000
or this small quote,

150
00:11:26,000 --> 00:11:29,000
the flavor of it is unique.

151
00:11:29,000 --> 00:11:30,000
You can,

152
00:11:30,000 --> 00:11:32,000
this is why when you read these texts,

153
00:11:32,000 --> 00:11:36,000
you can smell something right away when it's fishing.

154
00:11:36,000 --> 00:11:38,000
When you know something,

155
00:11:38,000 --> 00:11:41,000
when something is not like this,

156
00:11:41,000 --> 00:11:43,000
you can catch it.

157
00:11:43,000 --> 00:11:46,000
And write salt.

158
00:11:46,000 --> 00:11:57,000
And then it's trying to go straight.

159
00:11:57,000 --> 00:12:04,000
You see, I get called day and night.

160
00:12:04,000 --> 00:12:05,000
Write salt,

161
00:12:05,000 --> 00:12:07,000
so you don't have thoughts without greed.

162
00:12:07,000 --> 00:12:09,000
We went over this a few days ago,

163
00:12:09,000 --> 00:12:10,000
thoughts without greed,

164
00:12:10,000 --> 00:12:14,000
thoughts without anger, thoughts without delusion.

165
00:12:14,000 --> 00:12:15,000
Write speech,

166
00:12:15,000 --> 00:12:16,000
not lying,

167
00:12:16,000 --> 00:12:18,000
not back-biting,

168
00:12:18,000 --> 00:12:21,000
not for gossiping,

169
00:12:21,000 --> 00:12:25,000
not referring from harsh speech,

170
00:12:25,000 --> 00:12:29,000
or referring from useless speech.

171
00:12:29,000 --> 00:12:30,000
Write action,

172
00:12:30,000 --> 00:12:31,000
not killing,

173
00:12:31,000 --> 00:12:32,000
not stealing,

174
00:12:32,000 --> 00:12:36,000
not cheating,

175
00:12:36,000 --> 00:12:38,000
write livelihood,

176
00:12:38,000 --> 00:12:41,000
and not engaging in wrong speech,

177
00:12:41,000 --> 00:12:44,000
or wrong action for your livelihood.

178
00:12:44,000 --> 00:12:46,000
Write effort,

179
00:12:46,000 --> 00:12:49,000
the effort to,

180
00:12:49,000 --> 00:12:53,000
the effort to prevent it

181
00:12:53,000 --> 00:12:56,000
and eradicate,

182
00:12:56,000 --> 00:12:58,000
to file on all some states

183
00:12:58,000 --> 00:13:00,000
and to cultivate

184
00:13:00,000 --> 00:13:03,000
and maintain or protect

185
00:13:03,000 --> 00:13:05,000
wholesome states.

186
00:13:05,000 --> 00:13:07,000
Write mindfulness means

187
00:13:07,000 --> 00:13:09,000
mindfulness of the body,

188
00:13:09,000 --> 00:13:11,000
mindfulness of feelings,

189
00:13:11,000 --> 00:13:13,000
mindfulness of the mind,

190
00:13:13,000 --> 00:13:16,000
mindfulness of the dumbness.

191
00:13:16,000 --> 00:13:19,000
And write concentration means concentration

192
00:13:19,000 --> 00:13:22,000
that frees you from the five hindrances,

193
00:13:22,000 --> 00:13:26,000
usually sort of equated to the four genres,

194
00:13:26,000 --> 00:13:29,000
or concentration that is associated

195
00:13:29,000 --> 00:13:34,000
with the other seven path factors.

196
00:13:34,000 --> 00:13:36,000
However you look at it,

197
00:13:36,000 --> 00:13:38,000
it's quite simple,

198
00:13:38,000 --> 00:13:40,000
quite straightforward,

199
00:13:40,000 --> 00:13:42,000
pure in that sense.

200
00:13:42,000 --> 00:13:44,000
There's nothing extra,

201
00:13:44,000 --> 00:13:46,000
there's nothing quirky,

202
00:13:46,000 --> 00:13:50,000
or controversial even about it.

203
00:13:50,000 --> 00:13:52,000
This is why the Buddha said

204
00:13:52,000 --> 00:13:55,000
any religion which has this eight full noble path in it

205
00:13:55,000 --> 00:13:57,000
that's considered that

206
00:13:57,000 --> 00:14:00,000
to be the religion where you'll find a line means.

207
00:14:00,000 --> 00:14:04,000
Religion is without these eight full noble path factors,

208
00:14:04,000 --> 00:14:07,000
and consider that that religion is not going to have

209
00:14:07,000 --> 00:14:09,000
enlightened being.

210
00:14:09,000 --> 00:14:13,000
These are the things that lead to Nirvana.

211
00:14:13,000 --> 00:14:16,000
By Nirvana means freedom from suffering,

212
00:14:16,000 --> 00:14:20,000
that's explicitly literally

213
00:14:20,000 --> 00:14:23,000
what Nirvana means.

214
00:14:23,000 --> 00:14:25,000
Something we should not something

215
00:14:25,000 --> 00:14:26,000
hard to understand either,

216
00:14:26,000 --> 00:14:29,000
which I guess that's one thing you can look at this and say,

217
00:14:29,000 --> 00:14:34,000
yeah, those Buddhists, they believe in this state called Nirvana,

218
00:14:34,000 --> 00:14:36,000
it's just their belief.

219
00:14:36,000 --> 00:14:40,000
It's not a belief, it's a term

220
00:14:40,000 --> 00:14:44,000
that refers to freedom from suffering,

221
00:14:44,000 --> 00:14:48,000
cessation of suffering.

222
00:14:48,000 --> 00:14:50,000
And that's not a belief.

223
00:14:50,000 --> 00:14:52,000
I mean we have a belief that that's attainable.

224
00:14:52,000 --> 00:14:55,000
Most people would argue that it's not attainable,

225
00:14:55,000 --> 00:14:56,000
okay?

226
00:14:56,000 --> 00:14:58,000
We have a belief that it's attainable,

227
00:14:58,000 --> 00:15:00,000
that's our belief.

228
00:15:00,000 --> 00:15:02,000
It's claimable.

229
00:15:02,000 --> 00:15:04,000
You want to find out,

230
00:15:04,000 --> 00:15:07,000
well here's eight things that you should do,

231
00:15:07,000 --> 00:15:10,000
cultivate these eight things.

232
00:15:10,000 --> 00:15:14,000
You can see for yourself

233
00:15:14,000 --> 00:15:16,000
whether there is such a thing,

234
00:15:16,000 --> 00:15:18,000
freedom from suffering,

235
00:15:18,000 --> 00:15:20,000
cessation of suffering.

236
00:15:20,000 --> 00:15:27,000
That's the number for tonight.

237
00:15:27,000 --> 00:15:32,000
And Vanessa's back from Austria.

238
00:15:32,000 --> 00:15:43,000
And this is back Robin left.

239
00:15:43,000 --> 00:15:50,000
More people coming, I don't know.

240
00:15:50,000 --> 00:15:54,000
We have room if anyone wants to come

241
00:15:54,000 --> 00:15:56,000
and do a course here.

242
00:15:56,000 --> 00:16:05,000
You also have room on the meeting pages.

243
00:16:05,000 --> 00:16:08,000
If anyone wants to do an online course,

244
00:16:08,000 --> 00:16:18,000
which is really a great way to start.

245
00:16:18,000 --> 00:16:21,000
If you want to learn how to meditate,

246
00:16:21,000 --> 00:16:27,000
if you want to start, but coming to do an intensive course

247
00:16:27,000 --> 00:16:29,000
seems to daunting,

248
00:16:29,000 --> 00:16:33,000
or you don't have the ability to come out here.

249
00:16:33,000 --> 00:16:34,000
Do an online course.

250
00:16:34,000 --> 00:16:35,000
It's a good way to start.

251
00:16:35,000 --> 00:16:40,000
Good way to get a...

252
00:16:40,000 --> 00:16:51,000
We may get a basic understanding of it teaching.

253
00:16:51,000 --> 00:16:56,000
Anyway, that's all for tonight.

254
00:16:56,000 --> 00:16:57,000
If anyone has questions,

255
00:16:57,000 --> 00:16:59,000
you're welcome to join me.

256
00:16:59,000 --> 00:17:02,000
Hang out.

257
00:17:02,000 --> 00:17:04,000
Otherwise,

258
00:17:04,000 --> 00:17:08,000
we have someone on the hangout.

259
00:17:08,000 --> 00:17:15,000
Someone with the name of a fixed end.

260
00:17:15,000 --> 00:17:18,000
Hello, do you have a question?

261
00:17:18,000 --> 00:17:27,000
Your mic is muted.

262
00:17:27,000 --> 00:17:36,000
You have to unmute yourself.

263
00:17:36,000 --> 00:17:43,000
Again, here you are still.

264
00:17:43,000 --> 00:17:46,000
Here you can go.

265
00:17:46,000 --> 00:17:49,000
We'll meet tomorrow.

266
00:17:49,000 --> 00:17:50,000
What's tomorrow Friday?

267
00:17:50,000 --> 00:17:52,000
We'll meet tomorrow at five.

268
00:17:52,000 --> 00:17:54,000
I should give you the first paper.

269
00:17:54,000 --> 00:17:56,000
You have ten days?

270
00:17:56,000 --> 00:17:57,000
Two more.

271
00:17:57,000 --> 00:17:58,000
Two more.

272
00:17:58,000 --> 00:17:59,000
Five minutes.

273
00:17:59,000 --> 00:18:00,000
Two.

274
00:18:00,000 --> 00:18:01,000
Two.

275
00:18:01,000 --> 00:18:02,000
Two.

276
00:18:02,000 --> 00:18:03,000
Two.

277
00:18:03,000 --> 00:18:04,000
Two.

278
00:18:04,000 --> 00:18:07,000
Five minutes.

279
00:18:07,000 --> 00:18:08,000
You've done these before?

280
00:18:08,000 --> 00:18:09,000
No.

281
00:18:09,000 --> 00:18:11,000
Did these last time you were here, right?

282
00:18:11,000 --> 00:18:14,000
Yeah.

283
00:18:14,000 --> 00:18:17,000
I know.

284
00:18:17,000 --> 00:18:24,000
Good.

285
00:18:24,000 --> 00:18:26,000
Everything you need would happen to your foot.

286
00:18:26,000 --> 00:18:27,000
Yeah, sure.

287
00:18:27,000 --> 00:18:28,000
Ah, you told me.

288
00:18:28,000 --> 00:18:35,000
I've been walking.

289
00:18:35,000 --> 00:18:36,000
Walking is okay?

290
00:18:36,000 --> 00:18:37,000
Yeah.

291
00:18:37,000 --> 00:18:42,000
And I can't really contact you.

292
00:18:42,000 --> 00:18:44,000
I don't strain it.

293
00:18:44,000 --> 00:18:51,000
Do it again.

294
00:18:51,000 --> 00:18:56,000
Okay.

295
00:18:56,000 --> 00:19:06,000
So, the kind of the throwback of how many you come back here,

296
00:19:06,000 --> 00:19:08,000
the throwback of how many you come back here,

297
00:19:08,000 --> 00:19:11,000
the throwback of how many you come back here.

298
00:19:11,000 --> 00:19:15,000
I don't know where things back home.

299
00:19:15,000 --> 00:19:20,000
What?

300
00:19:20,000 --> 00:19:27,000
I think that it will kind of hard actually.

301
00:19:27,000 --> 00:19:30,000
I don't need to try to work with it.

302
00:19:30,000 --> 00:19:33,000
Like, I need to, I don't know,

303
00:19:33,000 --> 00:19:35,000
I don't know if I really,

304
00:19:35,000 --> 00:19:37,000
this may be like,

305
00:19:37,000 --> 00:19:39,000
you must put them in some scary way.

306
00:19:39,000 --> 00:19:41,000
Do you want to do anything with it?

307
00:19:41,000 --> 00:19:43,000
Like, when I can make it to sleep,

308
00:19:43,000 --> 00:19:46,000
do I do want to go out?

309
00:19:46,000 --> 00:19:47,000
Yeah.

310
00:19:47,000 --> 00:19:49,000
My family was the same.

311
00:19:49,000 --> 00:19:52,000
It called me a zombie.

312
00:19:52,000 --> 00:19:55,000
Yeah.

313
00:19:55,000 --> 00:19:59,000
I don't know.

314
00:19:59,000 --> 00:20:02,000
But did you lose your job?

315
00:20:02,000 --> 00:20:03,000
Did that go?

316
00:20:03,000 --> 00:20:04,000
No?

317
00:20:04,000 --> 00:20:07,000
You're okay?

318
00:20:07,000 --> 00:20:08,000
Yeah.

319
00:20:08,000 --> 00:20:12,000
I think there's like something,

320
00:20:12,000 --> 00:20:14,000
I kind of like,

321
00:20:14,000 --> 00:20:17,000
I feel like I have to be like strong

322
00:20:17,000 --> 00:20:19,000
or kind of,

323
00:20:19,000 --> 00:20:20,000
like this,

324
00:20:20,000 --> 00:20:23,000
but like in the thunderstorm,

325
00:20:23,000 --> 00:20:24,000
maybe.

326
00:20:24,000 --> 00:20:25,000
How do you know?

327
00:20:25,000 --> 00:20:28,000
I have to be in the 10 hours

328
00:20:28,000 --> 00:20:31,000
and I'm really important to do that.

329
00:20:31,000 --> 00:20:32,000
Yeah.

330
00:20:32,000 --> 00:20:35,000
I think if it has a duty,

331
00:20:35,000 --> 00:20:36,000
you know, if I don't do it,

332
00:20:36,000 --> 00:20:37,000
I don't need it.

333
00:20:37,000 --> 00:20:38,000
If I don't,

334
00:20:38,000 --> 00:20:39,000
I don't need,

335
00:20:39,000 --> 00:20:41,000
so you do it.

336
00:20:41,000 --> 00:20:42,000
Just don't,

337
00:20:42,000 --> 00:20:43,000
you know,

338
00:20:43,000 --> 00:20:45,000
remember it's all about our reactions.

339
00:20:45,000 --> 00:20:48,000
So concerned about our situation.

340
00:20:48,000 --> 00:20:49,000
I mean,

341
00:20:49,000 --> 00:20:49,000
yeah,

342
00:20:49,000 --> 00:20:50,000
you can't be as mindful

343
00:20:50,000 --> 00:20:52,000
when you're working many hours a day,

344
00:20:52,000 --> 00:20:53,000
but,

345
00:20:53,000 --> 00:20:54,000
you know,

346
00:20:54,000 --> 00:20:57,000
take time after work,

347
00:20:57,000 --> 00:20:58,000
between,

348
00:20:58,000 --> 00:20:59,000
you know,

349
00:20:59,000 --> 00:21:00,000
at lunch and so on,

350
00:21:00,000 --> 00:21:02,000
to resenter yourself,

351
00:21:02,000 --> 00:21:05,000
get yourself more mindful.

352
00:21:05,000 --> 00:21:06,000
Eventually,

353
00:21:06,000 --> 00:21:07,000
you'll just shave off your hair

354
00:21:07,000 --> 00:21:08,000
and become a moke.

355
00:21:08,000 --> 00:21:11,000
I hope you guys have enough of them.

356
00:21:11,000 --> 00:21:13,000
I don't believe in this life.

357
00:21:13,000 --> 00:21:15,000
But,

358
00:21:15,000 --> 00:21:16,000
if you want to,

359
00:21:16,000 --> 00:21:17,000
this life,

360
00:21:17,000 --> 00:21:18,000
you know,

361
00:21:18,000 --> 00:21:19,000
I'll move you.

362
00:21:19,000 --> 00:21:22,000
Okay.

363
00:21:22,000 --> 00:21:23,000
Yeah,

364
00:21:23,000 --> 00:21:24,000
it's like,

365
00:21:24,000 --> 00:21:25,000
it's like,

366
00:21:25,000 --> 00:21:26,000
it's like,

367
00:21:26,000 --> 00:21:28,000
it's like,

368
00:21:28,000 --> 00:21:29,000
it's like,

369
00:21:29,000 --> 00:21:30,000
it's like,

370
00:21:30,000 --> 00:21:31,000
it's like,

371
00:21:31,000 --> 00:21:32,000
it's like,

372
00:21:32,000 --> 00:21:35,000
I'm over the evening and there,

373
00:21:35,000 --> 00:21:36,000
so it's strange.

374
00:21:36,000 --> 00:21:38,000
Take your time.

375
00:21:38,000 --> 00:21:39,000
You don't,

376
00:21:39,000 --> 00:21:41,000
you don't come by itself.

377
00:21:41,000 --> 00:21:42,000
Mindfulness is,

378
00:21:42,000 --> 00:21:43,000
it's,

379
00:21:43,000 --> 00:21:45,000
stay on the path.

380
00:21:45,000 --> 00:21:46,000
Everything else.

381
00:21:46,000 --> 00:21:47,000
Just detail.

382
00:21:47,000 --> 00:21:48,000
Yeah,

383
00:21:48,000 --> 00:21:49,000
it takes me here,

384
00:21:49,000 --> 00:21:50,000
actually.

385
00:21:50,000 --> 00:21:51,000
You want?

386
00:21:51,000 --> 00:21:52,000
I'm really lucky.

387
00:21:52,000 --> 00:21:53,000
I'm glad to have you.

388
00:21:53,000 --> 00:21:54,000
Maybe.

389
00:21:54,000 --> 00:21:55,000
Just,

390
00:21:55,000 --> 00:21:56,000
so,

391
00:21:56,000 --> 00:21:57,000
in time,

392
00:21:57,000 --> 00:21:58,000
something like,

393
00:21:58,000 --> 00:21:59,000
you know,

394
00:21:59,000 --> 00:22:00,000
it's like,

395
00:22:00,000 --> 00:22:01,000
it's like,

396
00:22:01,000 --> 00:22:02,000
it's like,

397
00:22:02,000 --> 00:22:04,000
um,

398
00:22:04,000 --> 00:22:05,000
but,

399
00:22:05,000 --> 00:22:06,000
you know,

400
00:22:06,000 --> 00:22:07,000
like,

401
00:22:07,000 --> 00:22:08,000
trying to make enough pressure

402
00:22:08,000 --> 00:22:09,000
so that all people

403
00:22:09,000 --> 00:22:11,000
can work more and more

404
00:22:11,000 --> 00:22:12,000
with it.

405
00:22:12,000 --> 00:22:13,000
It's like,

406
00:22:13,000 --> 00:22:14,000
and

407
00:22:14,000 --> 00:22:15,000
and

408
00:22:15,000 --> 00:22:16,000
and

409
00:22:16,000 --> 00:22:17,000
really,

410
00:22:17,000 --> 00:22:19,000
I like them all.

411
00:22:19,000 --> 00:22:21,000
If we see them now to,

412
00:22:21,000 --> 00:22:24,000
I see they're all suffering in their own needs to,

413
00:22:24,000 --> 00:22:25,000
um,

414
00:22:25,000 --> 00:22:26,000
I,

415
00:22:26,000 --> 00:22:27,000
I,

416
00:22:27,000 --> 00:22:29,000
I noticed that still,

417
00:22:29,000 --> 00:22:31,000
I can't really speak that conscious

418
00:22:31,000 --> 00:22:32,000
like in the evening

419
00:22:32,000 --> 00:22:33,000
when they come home

420
00:22:33,000 --> 00:22:34,000
and work at that

421
00:22:34,000 --> 00:22:35,000
flyer.

422
00:22:35,000 --> 00:22:36,000
Yeah.

423
00:22:36,000 --> 00:22:37,000
Right.

424
00:22:37,000 --> 00:22:38,000
Right.

425
00:22:38,000 --> 00:22:45,000
Do what you can.

426
00:22:45,000 --> 00:22:51,000
Do what you can with what you got.

427
00:22:51,000 --> 00:22:53,000
But that's the thing.

428
00:22:53,000 --> 00:22:54,000
You know,

429
00:22:54,000 --> 00:22:55,000
if you stay in the world,

430
00:22:55,000 --> 00:22:57,000
you got to deal with that.

431
00:22:57,000 --> 00:22:58,000
I mean,

432
00:22:58,000 --> 00:22:59,000
you're lucky.

433
00:22:59,000 --> 00:23:00,000
Some people have

434
00:23:00,000 --> 00:23:01,000
really hard,

435
00:23:01,000 --> 00:23:02,000
right?

436
00:23:02,000 --> 00:23:03,000
Some countries,

437
00:23:03,000 --> 00:23:04,000
some places.

438
00:23:04,000 --> 00:23:06,000
It's quite scary actually.

439
00:23:06,000 --> 00:23:09,000
Very little opportunity for

440
00:23:09,000 --> 00:23:14,000
reflection and mindfulness.

441
00:23:14,000 --> 00:23:15,000
Right.

442
00:23:15,000 --> 00:23:17,000
No.

443
00:23:17,000 --> 00:23:20,000
Let's see the,

444
00:23:20,000 --> 00:23:24,000
um,

445
00:23:24,000 --> 00:23:25,000
whether they call

446
00:23:25,000 --> 00:23:27,000
their,

447
00:23:27,000 --> 00:23:32,000
you have to,

448
00:23:32,000 --> 00:23:33,000
look at the word.

449
00:23:33,000 --> 00:23:34,000
It's not perfect.

450
00:23:34,000 --> 00:23:36,000
You know, you live in the world,

451
00:23:36,000 --> 00:23:38,000
so you can't be mindful all the time.

452
00:23:38,000 --> 00:23:39,000
You have to work.

453
00:23:39,000 --> 00:23:43,000
You have to interact.

454
00:23:43,000 --> 00:23:44,000
I'm glad you're here.

455
00:23:44,000 --> 00:23:45,000
You get started.

456
00:23:45,000 --> 00:23:48,000
We'll talk again tomorrow at five.

457
00:23:48,000 --> 00:23:49,000
Okay.

458
00:23:49,000 --> 00:23:51,000
Get settled in.

459
00:23:51,000 --> 00:23:52,000
You ready to start?

460
00:23:52,000 --> 00:23:53,000
Do you want to start tomorrow?

461
00:23:53,000 --> 00:23:55,000
Are you okay to start now?

462
00:23:55,000 --> 00:23:56,000
You've got time.

463
00:23:56,000 --> 00:23:58,000
You don't have to start tonight if you want to.

464
00:23:58,000 --> 00:24:00,000
Take it easy in the beginning.

465
00:24:00,000 --> 00:24:01,000
I'll see.

466
00:24:01,000 --> 00:24:02,000
Okay.

467
00:24:02,000 --> 00:24:03,000
See,

468
00:24:03,000 --> 00:24:04,000
you're still jet-like.

469
00:24:04,000 --> 00:24:05,000
I guess you flew today.

470
00:24:05,000 --> 00:24:06,000
You know,

471
00:24:06,000 --> 00:24:07,000
you've got,

472
00:24:07,000 --> 00:24:08,000
you have lots of time.

473
00:24:08,000 --> 00:24:09,000
If you don't want to start today,

474
00:24:09,000 --> 00:24:10,000
you can start tomorrow.

475
00:24:10,000 --> 00:24:11,000
We can meet.

476
00:24:11,000 --> 00:24:13,000
We can meet the next day.

477
00:24:13,000 --> 00:24:14,000
No.

478
00:24:14,000 --> 00:24:15,000
We can meet tomorrow.

479
00:24:15,000 --> 00:24:16,000
But if you don't want to switch,

480
00:24:16,000 --> 00:24:17,000
we can,

481
00:24:17,000 --> 00:24:19,000
you don't do enough practice on this one.

482
00:24:19,000 --> 00:24:21,000
We'll meet tomorrow at five,

483
00:24:21,000 --> 00:24:22,000
either way.

484
00:24:22,000 --> 00:24:23,000
I will meet after.

485
00:24:23,000 --> 00:24:24,000
Oh, you see how it goes.

486
00:24:24,000 --> 00:24:25,000
Yeah.

487
00:24:25,000 --> 00:24:26,000
Okay.

488
00:24:26,000 --> 00:24:28,000
I would think just like I'm here.

489
00:24:28,000 --> 00:24:29,000
Are you ready?

490
00:24:29,000 --> 00:24:30,000
I would.

491
00:24:30,000 --> 00:24:31,000
Yeah.

492
00:24:31,000 --> 00:24:32,000
I would.

493
00:24:32,000 --> 00:24:33,000
Yeah.

494
00:24:33,000 --> 00:24:34,000
Yeah.

495
00:24:34,000 --> 00:24:35,000
Yeah.

496
00:24:35,000 --> 00:24:36,000
Yeah.

497
00:24:36,000 --> 00:24:37,000
You know where food is?

498
00:24:37,000 --> 00:24:38,000
Yeah.

499
00:24:38,000 --> 00:24:39,000
Yeah.

500
00:24:39,000 --> 00:24:40,000
Yeah.

501
00:24:40,000 --> 00:24:42,000
You know where food is?

502
00:24:42,000 --> 00:24:43,000
Everything.

503
00:24:43,000 --> 00:24:44,000
Cool.

504
00:24:44,000 --> 00:24:45,000
Thank you.

505
00:24:45,000 --> 00:24:46,000
That's a food.

506
00:24:46,000 --> 00:24:47,000
If you need anything special,

507
00:24:47,000 --> 00:24:50,000
there's anything missing in the food or something.

508
00:24:50,000 --> 00:24:51,000
You know,

509
00:24:51,000 --> 00:24:52,000
it's just,

510
00:24:52,000 --> 00:24:53,000
you know,

511
00:24:53,000 --> 00:24:54,000
you know,

512
00:24:54,000 --> 00:24:55,000
like,

513
00:24:55,000 --> 00:24:56,000
you know,

514
00:24:56,000 --> 00:24:57,000
you know,

515
00:24:57,000 --> 00:24:58,000
you know,

516
00:24:58,000 --> 00:24:59,000
you know,

517
00:24:59,000 --> 00:25:00,000
you know,

518
00:25:00,000 --> 00:25:01,000
there's really not.

519
00:25:01,000 --> 00:25:02,000
I'm,

520
00:25:02,000 --> 00:25:03,000
I don't,

521
00:25:03,000 --> 00:25:04,000
I don't really know.

522
00:25:04,000 --> 00:25:05,000
There's not much.

523
00:25:05,000 --> 00:25:06,000
Yeah.

524
00:25:06,000 --> 00:25:08,000
It's good if you make sure like if something needs,

525
00:25:08,000 --> 00:25:11,000
you see something's dirty or need sweeping or so on.

526
00:25:11,000 --> 00:25:12,000
Great.

527
00:25:12,000 --> 00:25:13,000
You have time.

528
00:25:13,000 --> 00:25:15,000
You have time every day to do a little bit of sweeping or a little bit

529
00:25:15,000 --> 00:25:16,000
of something.

530
00:25:16,000 --> 00:25:17,000
Yeah.

531
00:25:17,000 --> 00:25:20,000
It's good.

532
00:25:20,000 --> 00:25:22,000
And if there's something,

533
00:25:22,000 --> 00:25:25,000
it's just an issue.

534
00:25:25,000 --> 00:25:27,000
We're getting a blender.

535
00:25:27,000 --> 00:25:28,000
So you can make,

536
00:25:28,000 --> 00:25:29,000
there's frozen fruit.

537
00:25:29,000 --> 00:25:32,000
You can make smoothies if you want.

538
00:25:32,000 --> 00:25:33,000
Yeah.

539
00:25:33,000 --> 00:25:34,000
Frozen fruits,

540
00:25:34,000 --> 00:25:38,000
apparently easiest because it doesn't go bad.

541
00:25:38,000 --> 00:25:39,000
Yeah.

542
00:25:39,000 --> 00:25:41,000
So there's Robin brought frozen fruit.

543
00:25:41,000 --> 00:25:42,000
When we get the thing,

544
00:25:42,000 --> 00:25:43,000
you can,

545
00:25:43,000 --> 00:25:44,000
I don't know how it works.

546
00:25:44,000 --> 00:25:46,000
Maybe this makes me.

547
00:25:46,000 --> 00:25:47,000
Yeah.

548
00:25:47,000 --> 00:25:48,000
Yeah.

549
00:25:48,000 --> 00:25:50,000
Yeah.

550
00:25:50,000 --> 00:25:51,000
Okay.

551
00:25:51,000 --> 00:25:53,000
Good.

552
00:25:53,000 --> 00:25:54,000
All right.

553
00:25:54,000 --> 00:25:56,000
Have a good night.

554
00:25:56,000 --> 00:25:57,000
Thank you.

555
00:25:57,000 --> 00:25:58,000
See you tomorrow.

556
00:25:58,000 --> 00:26:05,000
All right.

557
00:26:05,000 --> 00:26:07,000
Try to figure out what's going on here.

558
00:26:07,000 --> 00:26:12,000
I've had like three people trying to call me.

559
00:26:12,000 --> 00:26:14,000
Is this people actually trying to get there?

560
00:26:14,000 --> 00:26:17,000
Is this just a malfunction of my phone?

561
00:26:17,000 --> 00:26:19,000
People actually trying to get into the,

562
00:26:19,000 --> 00:26:20,000
you're trying to get into the hangout.

563
00:26:20,000 --> 00:26:22,000
If you're watching this and thinking,

564
00:26:22,000 --> 00:26:23,000
how can I join?

565
00:26:23,000 --> 00:26:24,000
How can I join?

566
00:26:24,000 --> 00:26:25,000
You have to click on the link.

567
00:26:25,000 --> 00:26:27,000
There's a link.

568
00:26:27,000 --> 00:26:28,000
I know.

569
00:26:28,000 --> 00:26:30,000
I hope they need to join this one.

570
00:26:30,000 --> 00:26:31,000
No.

571
00:26:31,000 --> 00:26:32,000
I'm,

572
00:26:32,000 --> 00:26:34,000
I'm talking to people actually on the internet right now.

573
00:26:34,000 --> 00:26:35,000
There's people watching this.

574
00:26:35,000 --> 00:26:36,000
Oh, sorry.

575
00:26:36,000 --> 00:26:37,000
It's okay.

576
00:26:37,000 --> 00:26:38,000
Okay.

577
00:26:38,000 --> 00:26:39,000
And then you don't,

578
00:26:39,000 --> 00:26:40,000
you don't have to pay attention to this.

579
00:26:40,000 --> 00:26:42,000
But there's people I think trying to get on.

580
00:26:42,000 --> 00:26:43,000
If you want to get on,

581
00:26:43,000 --> 00:26:47,000
you have to go to meditation.serimongolo.org

582
00:26:47,000 --> 00:26:49,000
and click on the link.

583
00:26:49,000 --> 00:26:52,000
It's the only way.

584
00:26:52,000 --> 00:27:01,000
It's the night.

585
00:27:01,000 --> 00:27:06,000
And since no one's joining except for this mute,

586
00:27:06,000 --> 00:27:14,000
and mysterious picture of this person,

587
00:27:14,000 --> 00:27:20,000
I think we're going to say good night.

588
00:27:20,000 --> 00:27:23,000
If you were trying to get on,

589
00:27:23,000 --> 00:27:26,000
try again tomorrow.

590
00:27:26,000 --> 00:27:51,000
Good night, everyone.

