1
00:00:00,000 --> 00:00:12,040
So, we've been talking about meditation like me and how to practice and why to practice.

2
00:00:12,040 --> 00:00:22,080
But the real reason for meditating is because you want to be happy.

3
00:00:22,080 --> 00:00:25,160
We all want to find happiness.

4
00:00:25,160 --> 00:00:29,000
We all want to be free from suffering.

5
00:00:29,000 --> 00:00:34,000
Nobody wants to suffer, and yet we suffer.

6
00:00:34,000 --> 00:00:43,760
So, one of the promises that we have from the meditation practice, from Buddhism, is that

7
00:00:43,760 --> 00:00:49,000
it helps us become free from suffering.

8
00:00:49,000 --> 00:00:55,480
This is something that we're not taught very often in my… something that we're taught

9
00:00:55,480 --> 00:01:04,480
to avoid, and we're taught as wrong, that we have taught ourselves as wrong.

10
00:01:04,480 --> 00:01:10,680
When the baby first comes out of the womb, most often the first thing they do is start

11
00:01:10,680 --> 00:01:24,480
crying, complaining, of course they say it's instinct for the baby to cry, but I actually

12
00:01:24,480 --> 00:01:33,480
see it comes with a quite a bit of unhappiness, and we learn that the best way to get what

13
00:01:33,480 --> 00:01:39,680
we want is to start crying, or we know that the best way we can get what we want when

14
00:01:39,680 --> 00:01:49,480
we want something, when we're suffering, when we're hungry, when we're thirsty, when we want

15
00:01:49,480 --> 00:02:11,960
this or we want that, we want attention, we want warmth, they're cool and so on.

16
00:02:11,960 --> 00:02:20,200
That suffering is a very intrinsic part of our lives, no matter how we try to run away

17
00:02:20,200 --> 00:02:28,760
from it, even just sitting here right now, you will find that, for a moment, there are

18
00:02:28,760 --> 00:02:34,360
many things that arise that could very well bring us suffering.

19
00:02:34,360 --> 00:02:44,240
Maybe we find it too hot, maybe we find it too cold, maybe we're bored, because we don't

20
00:02:44,240 --> 00:02:54,520
have the excitement or the pleasure that we're accustomed to or that we're hoping for

21
00:02:54,520 --> 00:03:04,320
expecting when we leave here, maybe there's pain in our legs or pain in the back,

22
00:03:04,320 --> 00:03:13,800
or pain in the head, maybe we're tired, and maybe we're hungry, right here and now

23
00:03:13,800 --> 00:03:24,880
we can see there's quite a bit of suffering, so our first instinct for overcoming suffering

24
00:03:24,880 --> 00:03:48,880
is to see it as a feeling that truth of suffering is that very feeling that arises of suffering.

25
00:03:48,880 --> 00:03:55,920
You see, that's the truth of suffering, so the way to escape suffering is to run away from

26
00:03:55,920 --> 00:04:05,960
the feelings, is to not have those feelings arise.

27
00:04:05,960 --> 00:04:10,520
Actually it would be very, it would be quite terrible if that's where the case, because

28
00:04:10,520 --> 00:04:17,560
we can't be free from feelings, we can't be free from painful feelings, it's really

29
00:04:17,560 --> 00:04:26,240
the problem with our approach to the whole idea of suffering, so we think when we have

30
00:04:26,240 --> 00:04:31,480
a headache, we take some feel and the headache goes away or we have a pain in the legs,

31
00:04:31,480 --> 00:04:41,880
we just adjust ourselves, we adjust our position and then we're free from suffering,

32
00:04:41,880 --> 00:04:47,400
but this isn't the truth of suffering, it's just called dukawedana, it's just a suffering

33
00:04:47,400 --> 00:04:52,600
feeling.

34
00:04:52,600 --> 00:05:00,360
And so the approach to trying to escape these painful feelings is wrong, even when people

35
00:05:00,360 --> 00:05:07,800
hear about suffering in a Buddhist context, this is how they think of it, so they think

36
00:05:07,800 --> 00:05:14,800
that somehow meditation is going to free them from painful feelings, or they don't want

37
00:05:14,800 --> 00:05:20,360
to meditate at all because they find that meditation brings painful feelings, they don't

38
00:05:20,360 --> 00:05:26,760
want to sit still, they don't want to listen to the dhamma, they want to find pleasure

39
00:05:26,760 --> 00:05:37,360
all the time, when pain comes up they don't know how to deal with it, they think something's

40
00:05:37,360 --> 00:05:41,960
wrong when the pain is right, so of course this is natural, this is what we brought up

41
00:05:41,960 --> 00:05:49,960
to think, to believe, it's reinforced by our whole society, all of the things that

42
00:05:49,960 --> 00:05:56,960
you can buy to escape suffering, all the pleasures you can obtain to be free from suffering,

43
00:06:00,960 --> 00:06:09,960
as humans we have many diversions to keep us away from the painful feelings, you're

44
00:06:09,960 --> 00:06:14,240
in the other way still experienced them and they still come back and we can't ever escape

45
00:06:14,240 --> 00:06:19,680
them completely.

46
00:06:19,680 --> 00:06:26,680
And this is what separates a person who is, you might say, special, a person who is capable

47
00:06:34,360 --> 00:06:41,360
of understanding the truth from a person who is not ready to understand the truth, this

48
00:06:42,880 --> 00:06:49,400
is really the defining factor, whether they realize it, whether they realize that suffering

49
00:06:49,400 --> 00:06:55,720
is actually a part of life, whether they realize that running away from suffering only

50
00:06:55,720 --> 00:07:02,720
makes you more and more averse to it, that trying to find pleasure constantly is only

51
00:07:07,640 --> 00:07:14,640
a cause for more suffering, greater suffering, because I was talking about these people

52
00:07:14,640 --> 00:07:21,240
when they get sick or when they're dying, you see them pleading looks in their eyes,

53
00:07:21,240 --> 00:07:27,840
it's sad but you think, well, you did this to yourself really, did you not think that

54
00:07:27,840 --> 00:07:34,840
one day you were going to have to face such suffering, always running away from it, this

55
00:07:35,840 --> 00:07:42,840
is what we, one thing that we hope to avoid is, but this fear, this anxiety, this intense

56
00:07:42,840 --> 00:07:49,840
suffering, it comes when we can no longer avoid pain, and this starts from realizing that

57
00:08:00,120 --> 00:08:07,120
this isn't the way out of suffering, this is not the solution, and so this is why we begin

58
00:08:07,120 --> 00:08:14,120
and so this is why we begin to practice meditation, because we see, we have to look closer,

59
00:08:14,120 --> 00:08:20,000
you see meditation is just studying, studying the problem closer, while we have suffering,

60
00:08:20,000 --> 00:08:27,000
so let's meditate on it, let's study it, and so we understand that suffering is a part of life,

61
00:08:27,000 --> 00:08:34,000
and so we understand that suffering is a part of life, now for some people this is enough

62
00:08:34,200 --> 00:08:41,200
and this makes them sort of stoic about it, they bear with it, some people are very

63
00:08:43,400 --> 00:08:50,120
good at bearing with suffering, rather than trying to seek out pleasure, they just grit

64
00:08:50,120 --> 00:08:55,000
and bear it, so their life is quite miserable, but they're not taken aback by suffering,

65
00:08:55,000 --> 00:09:02,000
you might say that this is a bit better than the hedonistic path, because at least when

66
00:09:03,680 --> 00:09:10,280
suffering comes, they're not surprised by it, but on the other hand, you might think it's

67
00:09:10,280 --> 00:09:14,480
more miserable because they're not enjoying it either way, they're not free from suffering,

68
00:09:14,480 --> 00:09:21,480
but they're also not enjoying the pleasure of life, I think well, they're just

69
00:09:21,480 --> 00:09:28,840
living a life full of suffering, really, but still you might say that there's some sort

70
00:09:28,840 --> 00:09:34,880
of peace that comes from understanding that life is suffering, now this isn't what the

71
00:09:34,880 --> 00:09:38,680
Buddha had in mind, either it's regards to the truth of suffering, this is what people

72
00:09:38,680 --> 00:09:45,680
will often accuse Buddhists of, avoiding pleasure and just living a life of suffering,

73
00:09:45,680 --> 00:09:50,640
life is suffering, right, this is what they say about Buddhists, Buddhists believe that

74
00:09:50,640 --> 00:09:57,640
life is suffering, it's not really true, the Buddha never said that life is suffering,

75
00:10:00,480 --> 00:10:06,000
Buddha was trying to find a response to the problem of the existence of suffering, that

76
00:10:06,000 --> 00:10:13,000
suffering exists, and the answer isn't just to say it's part of life, that's not the

77
00:10:13,000 --> 00:10:20,000
answer, that's not the answer, but it gets you thinking and it gets you realizing that you have

78
00:10:20,000 --> 00:10:26,760
to go further, so this is an important step, it's not enough, but this is the step that starts

79
00:10:26,760 --> 00:10:31,880
you looking closer and saying, well, if it is a part of life, then how do I escape it, not just

80
00:10:31,880 --> 00:10:37,480
it's a part of life, so live with it, but if I can't escape the pain, if I can't escape painful

81
00:10:37,480 --> 00:10:45,160
feelings, how do I escape from suffering, it's a bit of deeper, and as you look deeper you start to

82
00:10:49,720 --> 00:10:58,760
realize that actually there's no difference between painful or pleasant experiences,

83
00:10:58,760 --> 00:11:12,440
there's nothing good about pleasure, there's nothing bad about pain, you can see that the truth

84
00:11:12,440 --> 00:11:19,480
about pleasure, the truth about pain is they arise, they stay for a short time and then they

85
00:11:19,480 --> 00:11:31,240
cease, that's really all you can say about them, well you should say about them, we can say a

86
00:11:31,240 --> 00:11:38,360
lot about them, but the more you cling to or project on these experiences like this is good,

87
00:11:38,360 --> 00:11:43,560
this is bad, the more suffering that, the more upset there is in the mind,

88
00:11:43,560 --> 00:11:51,480
in the mind is simply aware of the experience as it is, this is this, that is that,

89
00:11:55,560 --> 00:12:01,000
we realize that is truly all it is, this is pleasure, this is pain, there's not much more you can

90
00:12:01,000 --> 00:12:11,640
say about it without projecting, without adding something that isn't there, so you say this

91
00:12:11,640 --> 00:12:18,040
pleasure is good, but you realize that it's actually not good, there's nothing good about it,

92
00:12:18,040 --> 00:12:25,240
it's just an experience, comes and it goes, and you realize more over that when you say it's

93
00:12:25,240 --> 00:12:38,600
good, when you appreciate it, put some value on it, to begin to create an addiction to

94
00:12:38,600 --> 00:12:45,400
the attachment to it, your mind remembers that and says oh well that will make me happy again

95
00:12:46,040 --> 00:12:51,240
and later on it comes back and says hey remember that, that made me happy, let's go get that,

96
00:12:52,600 --> 00:12:55,720
so this is why when we sit down to meditate we are always thinking about

97
00:12:57,160 --> 00:13:04,040
pleasant things, what can I do it, we're doing this thing, it doesn't sight, it's pleasant sounds,

98
00:13:04,040 --> 00:13:09,720
pleasant experiences that we've had, because the mind keeps them and reminds us of, when does

99
00:13:09,720 --> 00:13:16,680
such a helpful thing to remind us of all the things that can make us happy, and so we're constantly

100
00:13:16,680 --> 00:13:22,200
bombarded by these memories, these reminders, hey remember that, that'll make you happy,

101
00:13:23,560 --> 00:13:29,480
this is addiction, this is the mind, this is how the brain works, the brain experiences

102
00:13:29,480 --> 00:13:37,640
pleasure and keeps a memory of what it was that brought pleasure, and then it just fires again

103
00:13:37,640 --> 00:13:44,280
and again randomly, even sitting here you can think of, you can realize that your mind is creating

104
00:13:46,760 --> 00:13:51,960
all of the pleasurable things in our lives just by sitting here we see that our mind is

105
00:13:51,960 --> 00:13:59,640
reminding us of constant, hey go do that, hey what are you doing sitting here, doing nothing,

106
00:14:00,760 --> 00:14:08,280
let's go, or when are we going to have a chance to do this, when can we do that, when can we

107
00:14:08,280 --> 00:14:19,480
find pleasure and enjoy the things that we enjoy, and so we're never really at peace,

108
00:14:19,480 --> 00:14:26,040
we're never really happy, the mind is always wanting something that doesn't have,

109
00:14:27,400 --> 00:14:30,920
even the point that it gets in our dreams and it's reminding us in our dreams,

110
00:14:30,920 --> 00:14:34,280
we have all sorts of crazy dreams about the things that we like,

111
00:14:36,520 --> 00:14:42,440
what's other mind works, it just keeps firing and reminding us,

112
00:14:43,400 --> 00:14:47,240
it was addiction, how addiction works, and so when we don't get what we want, we're unhappy,

113
00:14:47,240 --> 00:14:55,000
the mind is expecting, and there are these receptors in the brain that are waiting for

114
00:14:55,800 --> 00:15:01,400
their drugs, because the brain is full of drugs, they've got all sorts of chemicals that

115
00:15:02,280 --> 00:15:09,880
react with each other, give us a nice feeling, but we have to trigger it, and if we don't

116
00:15:09,880 --> 00:15:20,360
trigger it, then the brain is upset, the brain is displeased, the brain is stressed,

117
00:15:23,800 --> 00:15:29,400
and so either we can keep chasing after pleasure or we can learn to go overcome this,

118
00:15:29,400 --> 00:15:43,880
so this is what we start to realize that this is all not only unhelpful or painful,

119
00:15:44,920 --> 00:15:51,480
but also meaningless and totally unrelated to reality, so we come to see not only that

120
00:15:51,480 --> 00:15:55,800
suffering is a part of life, but suffering is inherent in every single thing,

121
00:15:55,800 --> 00:16:02,440
suffering is caused by everything, every experience that we cling to,

122
00:16:04,200 --> 00:16:11,640
creates a suffering, moreover we see that nothing, or put it another way that nothing

123
00:16:12,280 --> 00:16:17,560
is worth clinging to, is this seeing that suffering is a characteristic of all things,

124
00:16:17,560 --> 00:16:28,600
something like just like how a hot coal pain is a characteristic of hot coal's

125
00:16:28,600 --> 00:16:35,880
hot burning of fire, let's say, pain is a characteristic of fire, well not directly,

126
00:16:37,560 --> 00:16:45,240
but as a human being it certainly is, anyone who touches fire with their hand will feel pain,

127
00:16:45,240 --> 00:16:49,800
so when the Buddha talked about suffering being inherent in all things,

128
00:16:50,520 --> 00:16:55,480
he didn't mean that their mere appearance was suffering, he meant that when we cling to them,

129
00:16:56,600 --> 00:17:02,120
their cause for suffering, because you see, or he meant that they're useless,

130
00:17:02,120 --> 00:17:11,320
they're totally meaningless, without any value, worthless,

131
00:17:11,320 --> 00:17:18,760
you see they're not worth clinging to, you saw that clinging to anything is just

132
00:17:19,480 --> 00:17:22,120
giving rise to suffering, giving rise to addictions,

133
00:17:26,840 --> 00:17:32,280
and this is really the practice that we're looking for, when we see clearly, when we realize

134
00:17:32,280 --> 00:17:41,800
this, then all of our experiences are just an experience that they're not good and they're not bad,

135
00:17:45,080 --> 00:17:47,240
this is what we're aiming for in the practice,

136
00:17:50,280 --> 00:17:56,680
but it isn't yet the truth of suffering, of course it's really, you know, sounds kind of

137
00:17:56,680 --> 00:18:02,440
dismal right, this is what you get to see is that everything is useless, as meaningless, worthless,

138
00:18:03,880 --> 00:18:10,760
but you see it's this practice that leads you to the truth of suffering, which is the fourth category,

139
00:18:11,640 --> 00:18:21,720
the true truth of suffering is the realization that this is true of all things,

140
00:18:21,720 --> 00:18:27,800
so the practice is to see clearer and clearer that this is suffering and don't cling to the

141
00:18:27,800 --> 00:18:32,040
clinging to this cause of suffering, and that causes suffering, this is the practice that

142
00:18:32,920 --> 00:18:38,360
we, or the wisdom that we gain from the practice, but the truth of suffering is when the mind

143
00:18:38,360 --> 00:18:45,880
lets go, when the mind changes its opinion about, about samsara, about existence,

144
00:18:45,880 --> 00:18:53,880
and it says, wow, yes, all of the, all of existence experience is really pointless,

145
00:18:55,080 --> 00:19:01,320
it's meaningless, it's worthless, or this is a very difficult realization to come to,

146
00:19:01,320 --> 00:19:08,200
this is why nibana or nuvana is such a difficult and sometimes fearsome thing,

147
00:19:09,320 --> 00:19:12,840
because it's hard to find someone who wants to be free from experience, right,

148
00:19:12,840 --> 00:19:20,680
but you see that's the whole problem, very difficult to understand, very difficult to accept,

149
00:19:22,920 --> 00:19:32,360
but true, true and unknown, and eventually a person who practices meditation realizes this,

150
00:19:32,360 --> 00:19:40,280
who's to see that, yes, indeed experiences, or it rejects experience, it rejects

151
00:19:40,280 --> 00:19:46,520
the arising and ceasing of the five aggregates, it rejects the arising and ceasing of the six

152
00:19:46,520 --> 00:19:56,600
senses, and it lets go, and it enters into a state of cessation, the mind you might say ceases,

153
00:19:58,200 --> 00:20:03,400
whether it's total bliss and peace and freedom from suffering, which we call nibana and nuvana,

154
00:20:03,400 --> 00:20:15,720
no arising, it's the most peaceful, most cool, what I said cool is a good way to describe it,

155
00:20:15,720 --> 00:20:23,800
it's the ultimate cool, the ultimate cool, you know what I say, the coolest thing in existence,

156
00:20:23,800 --> 00:20:36,760
because it's absolutely cool, there's no heat of defilement or heat of suffering,

157
00:20:39,160 --> 00:20:43,160
and so a meditator who realizes this even just for a moment, this experience,

158
00:20:45,000 --> 00:20:52,680
they have a great sense of the existence of this, the existence of peace, the possibility of being

159
00:20:52,680 --> 00:21:01,160
free from suffering, this is what makes them on a soda pan, or so on this thing, one state of

160
00:21:01,160 --> 00:21:12,360
seeing nibana realize nuvana, even just for a few moments, they realize that there is something

161
00:21:12,360 --> 00:21:18,200
outside of experience, something to compare it with, if you've never experienced true peace,

162
00:21:18,200 --> 00:21:25,560
then of course you think of peace as being something that you have experienced,

163
00:21:26,280 --> 00:21:34,200
pieces these peaceful feelings that arise, that's peace, because you've never experienced true peace,

164
00:21:36,280 --> 00:21:41,320
but once you experience and you have this profound shift of your understanding of

165
00:21:41,320 --> 00:21:48,360
suffering and understanding of peace, you realize the true peace is within us, it does nothing

166
00:21:48,360 --> 00:21:55,560
to do with experience, it's right here and right now nothing to do with any experience, any one

167
00:21:55,560 --> 00:22:03,960
experience or any experience whatsoever, it's freedom from all of this, all of these external

168
00:22:03,960 --> 00:22:12,200
phenomena, bombarding us, moment by moment by moment, so this is what means by the truth of

169
00:22:12,200 --> 00:22:19,720
suffering, and this is what we're trying to gain in the practice, it's freedom from self,

170
00:22:20,760 --> 00:22:26,440
this peace that's happening, this is the true goal of the meditation practice, this is why we want

171
00:22:26,440 --> 00:22:37,400
to free ourselves from delusion, free ourselves from our ignorance, this is why we want to study

172
00:22:37,400 --> 00:22:43,640
ourselves, because when we study ourselves we'll come to see these, this truth, the truth of

173
00:22:43,640 --> 00:22:48,200
suffering, it's really the only truth that you need once you realize the truth of suffering,

174
00:22:48,200 --> 00:22:52,040
then you give up your attachment to suffering, you give up your attachment to suffering,

175
00:22:52,040 --> 00:22:58,680
that's the path that leads to the cessation of suffering, once you're on the path and you attain

176
00:22:59,640 --> 00:23:03,560
the cessation of suffering, this is what the four noble truths arise altogether,

177
00:23:05,320 --> 00:23:13,800
just like they say when you light a candle, when you light a candle, the weak burns up,

178
00:23:13,800 --> 00:23:22,520
and the wax melts, the flame arises and the darkness, or light arises and the darkness is

179
00:23:22,520 --> 00:23:28,760
dispelled, all at the same time the four things happen, in the same way the four noble truths

180
00:23:29,560 --> 00:23:38,440
arise altogether, once you see the truth of suffering, so this is what we're aiming for,

181
00:23:38,440 --> 00:23:43,960
it's an important thing to understand, at least to the extent of not running away from suffering,

182
00:23:43,960 --> 00:23:49,160
studying it, finding a better way out, even when a person just says to themselves,

183
00:23:49,800 --> 00:23:56,600
pain, pain when a person is sick or dying or in great pain, if only we could teach them all this,

184
00:23:56,600 --> 00:24:02,120
how to remind themselves and look at it and come to see, right, the pain isn't the problem,

185
00:24:02,120 --> 00:24:08,280
the problem is the way I'm looking at it, the problem is my inability to accept the pain,

186
00:24:09,880 --> 00:24:13,480
the inability to see the pain is just pain,

187
00:24:16,760 --> 00:24:21,000
my projections, my reactions to the pain, this is this real problem,

188
00:24:23,800 --> 00:24:27,160
and come to see that there's no reason to concern yourself with the pain at all,

189
00:24:27,160 --> 00:24:34,680
and slowly slowly, if you're really clear on it and you can even let go of the pain and

190
00:24:34,680 --> 00:24:40,120
not even have to experience the pain, so in turn people when they're dying, it's quite common

191
00:24:40,840 --> 00:24:47,000
for meditators, or relatively common for meditators to become enlightened when they're

192
00:24:47,000 --> 00:24:54,840
passing away, and because of the intensity of the experience and the immediacy of it

193
00:24:54,840 --> 00:25:05,800
requires them to either cling or let go, that forces them to see suffering directly,

194
00:25:08,280 --> 00:25:11,720
and encourages them to let go because there's nothing to cling to,

195
00:25:14,440 --> 00:25:22,600
so for all of us this is an important teaching to keep in mind when we do practice meditations

196
00:25:22,600 --> 00:25:29,400
so that we're not partial to anyone experience, we're able to experience the whole range of

197
00:25:31,960 --> 00:25:37,720
phenomena that arise with that partiality, we're able to see them as they are,

198
00:25:39,240 --> 00:25:46,040
so that's the demo for tonight, now we can put it into practice by studying ourselves through

199
00:25:46,040 --> 00:25:56,040
meditation, we'll practice together.

