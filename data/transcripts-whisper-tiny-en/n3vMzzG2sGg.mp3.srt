1
00:00:00,000 --> 00:00:10,640
I've heard that it is a good idea for a lay person to keep more than the five precepts.

2
00:00:10,640 --> 00:00:16,600
Once a week, such as not sleeping in a high place or luxury is bed, do you agree?

3
00:00:16,600 --> 00:00:19,560
What additional precepts should a lay person keep?

4
00:00:19,560 --> 00:00:23,680
As this was actually a practice in the time of the Buddha, the people would ask the Buddha

5
00:00:23,680 --> 00:00:31,440
what to do on the holy days, because in India at the time there were the full moons, and I believe

6
00:00:31,440 --> 00:00:33,440
there were also the half moons.

7
00:00:33,440 --> 00:00:41,760
Again, it's not clear in my mind anyway, you have these holy days that they had recognized

8
00:00:41,760 --> 00:00:48,920
in Hinduism, in the religion that we now know as Hinduism, Brahminism or whatever.

9
00:00:48,920 --> 00:00:51,760
And so people would ask the Buddha, what do we do on these holidays?

10
00:00:51,760 --> 00:00:56,360
How should we Buddhists behave, and so the Buddha said there are many ways of keeping the

11
00:00:56,360 --> 00:01:03,640
opposite, keeping the holidays, but taking the aid precepts was how the Buddha recommended

12
00:01:03,640 --> 00:01:04,640
it.

13
00:01:04,640 --> 00:01:09,800
So, if that translates in the West to every Sunday, that I think is a fine thing to do.

14
00:01:09,800 --> 00:01:10,800
Monks can't do that.

15
00:01:10,800 --> 00:01:17,600
We are still traditionally bound to the moons, so every two, every full moon and every empty

16
00:01:17,600 --> 00:01:23,200
moon we have our holy days, but for lay people I would say, in a world where Sunday and

17
00:01:23,200 --> 00:01:28,080
Saturday and Sunday are the holidays, take one day or the other, and every week, keep

18
00:01:28,080 --> 00:01:29,080
the aid precepts.

19
00:01:29,080 --> 00:01:33,320
It's a great way to keep up your practice and to intensify your practice, to intensify

20
00:01:33,320 --> 00:01:41,480
your commitment and your dedication to Buddhism, talking about what's going to make us closer

21
00:01:41,480 --> 00:01:46,640
to the Dhamma, keeping the aid precepts once a week is an excellent way to bring you closer

22
00:01:46,640 --> 00:01:47,640
to the Dhamma.

23
00:01:47,640 --> 00:01:51,880
So, it's the aid precepts, you have the five precepts, and then instead of just the third

24
00:01:51,880 --> 00:01:58,880
one being wrong sexual activity, you engage in no sexual or romantic activity, and then

25
00:01:58,880 --> 00:02:04,720
you add three more which are not to eat outside of the morning hours, not to engage

26
00:02:04,720 --> 00:02:11,200
in entertainment or beautification, and not to sleep on a high bed, so to try to sleep

27
00:02:11,200 --> 00:02:12,200
on the floor.

28
00:02:12,200 --> 00:02:14,400
It's simple bending.

29
00:02:14,400 --> 00:02:19,080
So keeping those are the meditators precepts, and they push you into a meditative state,

30
00:02:19,080 --> 00:02:29,280
so they're quite useful and beneficial for a meditator, and a great way to intensify

31
00:02:29,280 --> 00:02:35,960
your commitment, just as a Christian or a Jew, or Muslim, whatever week, go to their holy

32
00:02:35,960 --> 00:02:40,920
place in order to reaffirm their commitment, remind themselves about the teachings and

33
00:02:40,920 --> 00:02:42,000
so on.

34
00:02:42,000 --> 00:02:46,280
Something you can do even at home, put in some practice meditation for that day.

35
00:02:46,280 --> 00:02:51,240
Like here in Stony Creek, actually, we're going to take Saturdays, I think, so every Saturday

36
00:02:51,240 --> 00:02:57,360
once I get back from Florida, we're going to try to do a meditation course every Saturday,

37
00:02:57,360 --> 00:03:03,160
so that'll be all day nine to four, everyone can come and they'll know that every Saturday

38
00:03:03,160 --> 00:03:04,160
they have that.

39
00:03:04,160 --> 00:03:08,000
Of course, if you don't have a place near you, just do it at home, keep the aid precepts

40
00:03:08,000 --> 00:03:15,000
and try to practice as much as you can.

