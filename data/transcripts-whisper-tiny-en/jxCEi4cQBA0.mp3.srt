1
00:00:00,000 --> 00:00:07,680
How did you come to terms with the idea of reincarnation along with the idea that one goes to

2
00:00:07,680 --> 00:00:16,560
nibana after death being a reality only if you're enlightened? I don't quite understand.

3
00:00:16,560 --> 00:00:22,560
I think the grammar is not quite correct there. Okay, so there's the idea that reincarnation

4
00:00:22,560 --> 00:00:29,440
and the idea that one goes to nibana after death are only realizable if you're enlightened,

5
00:00:29,440 --> 00:00:34,720
like you can only know these things if you're enlightened. So nibana is only something you can

6
00:00:34,720 --> 00:00:39,920
know if you're enlightened. As a monk, I think that's what he's saying here, he or she? He.

7
00:00:40,720 --> 00:00:45,760
As a monk, did you just decide to accept nibana and reincarnation without any evidence?

8
00:00:48,160 --> 00:00:54,640
Evidence. Well, there's lots of evidence for reincarnation or rebirth, first of all.

9
00:00:54,640 --> 00:01:07,040
But I mean, I can't talk about myself from where I am at, whether I'm this or that,

10
00:01:07,920 --> 00:01:14,960
but there are different levels. Even someone who hasn't seen nibana can have great faith,

11
00:01:14,960 --> 00:01:25,760
but it still has to be faith, but they can have great faith based on the people that they look up to.

12
00:01:25,760 --> 00:01:36,320
So for example, my teacher, the kind of thing where many people have lots of faith in him,

13
00:01:37,760 --> 00:01:42,640
even though they haven't become enlightened, they haven't really seen nibana for themselves.

14
00:01:42,640 --> 00:01:49,680
And that's simply because he's unlike anybody else. He's got something that other people don't

15
00:01:49,680 --> 00:01:54,880
have and people can feel it. Usually it's hard for, if they haven't practiced meditation, it's hard

16
00:01:54,880 --> 00:02:02,480
for them to see it, but they get the idea that something's there. And they know that an ordinary

17
00:02:02,480 --> 00:02:18,480
person is not like this, it's not as kind as generous as pure, as unflappable, as constant and steady,

18
00:02:19,920 --> 00:02:31,520
and imperturbable as this. And so there's great faith there. People who read the Buddha's teaching,

19
00:02:31,520 --> 00:02:42,080
simply reading the tepitika gives you an incredible sense of awe and inspiration and confidence

20
00:02:42,720 --> 00:02:48,880
in whoever it was who taught these things. Of course, obviously much more so if you've actually

21
00:02:48,880 --> 00:02:55,120
practiced and realized them for yourselves, but even without having done that, people become amazed

22
00:02:55,120 --> 00:03:02,640
when they see just how profound. I mean, the Buddha's teaching is nothing like the Bible,

23
00:03:02,640 --> 00:03:07,920
for example. It's nothing like the Christian Bible, nothing like it. They're very little in

24
00:03:07,920 --> 00:03:17,680
common these two books, for example, or take the Quran very unsimilar. You might, I guess you'd

25
00:03:17,680 --> 00:03:22,960
have a better time comparing them to the Upanishads, for example. And they are in some ways similar

26
00:03:22,960 --> 00:03:32,080
to the Upanishads. But even the Upanishads, there is something different with Buddhism. It's much more,

27
00:03:33,280 --> 00:03:39,680
well, we would say much sharper, much more mundane even in the sense of simply dealing with

28
00:03:39,680 --> 00:03:44,080
the building blocks of reality. It doesn't have anything superfluous. It's not flowery.

29
00:03:44,800 --> 00:03:51,600
It's not designed to enchant you. It's designed to wake you up. And so by reading this,

30
00:03:51,600 --> 00:03:57,440
many people have a good faith. And I think that really just answers your question because

31
00:04:00,400 --> 00:04:06,800
we accept reincarnation with lots of evidence, but we also accept, sorry, that's what we

32
00:04:06,800 --> 00:04:11,520
might not consider something else about reincarnation, but we accept nibana on these grounds.

33
00:04:11,520 --> 00:04:18,720
We accept that even though individually, we may not have become enlightened, we accept that

34
00:04:18,720 --> 00:04:25,360
there's something special about this path. And so that leads us on. Now, ideally, that leads us

35
00:04:25,360 --> 00:04:31,440
on until we practice and once we practice, then we see nibana for ourselves. And so it's

36
00:04:32,160 --> 00:04:36,960
kind of like when you're walking to the forest and someone comes along who seems to know where

37
00:04:36,960 --> 00:04:42,240
they're going and you watch them and you see that they know they can point out things and they

38
00:04:42,240 --> 00:04:47,440
know how to avoid beasts and how to avoid pitfalls. So you say, wow, this person really knows

39
00:04:47,440 --> 00:04:53,040
this forest, probably there's someone who could take me out of it. So you trust them.

40
00:04:54,160 --> 00:04:56,960
And it doesn't mean you have blind faith in them and say, okay, well, I'm just going to sit here

41
00:04:56,960 --> 00:05:01,760
because I'm happy now. I found someone who knows this forest and can find the way out. So now I

42
00:05:01,760 --> 00:05:07,200
can rest in peace. Obviously, they don't do that. I mean, that's sort of the picture that you're

43
00:05:07,200 --> 00:05:12,560
painting or many people painting with still blind faith and so on. Well, that's blind faith.

44
00:05:12,560 --> 00:05:16,560
It's just sitting around and saying, well, he'll save us. You know, I'll just sit here until he

45
00:05:16,560 --> 00:05:21,120
gets me out of here. But that's not what you do. A person who believes in this person,

46
00:05:21,120 --> 00:05:27,760
they follow them or they follow the path that this person points out and find the way out for

47
00:05:27,760 --> 00:05:33,600
themselves. They're hedging their bets because this person seems to be, it's like that with

48
00:05:33,600 --> 00:05:38,640
anyone when you choose a teacher, you can only go by seems to be until you know the person very,

49
00:05:38,640 --> 00:05:44,320
very well. You just go by what you can see and what you believe and it can be wrong.

50
00:05:44,320 --> 00:05:49,920
But that's what we get the faith in there. But reincarnation rebirth is actually different

51
00:05:50,560 --> 00:06:00,320
because on the first hit, on the first, in the first place, there is much evidence, not proof

52
00:06:00,320 --> 00:06:06,400
but evidence. Remember, there's a difference. There's lots of evidence. There's no proof

53
00:06:06,400 --> 00:06:10,320
unless you remember a past type. But even then, it's you're not really proving anything.

54
00:06:10,320 --> 00:06:16,080
Of course, it can just be a fake memory. But, you know, the evidence of people pointing things

55
00:06:16,080 --> 00:06:20,320
out about their past lives telling, oh, this was mine and I know that and I know this person

56
00:06:20,320 --> 00:06:25,120
and calling people by their names and things that they could never know. There's cases of that,

57
00:06:25,120 --> 00:06:31,760
you know, anecdotal cases still, but clinical cases. So there's lots of well-documented cases.

58
00:06:33,840 --> 00:06:38,720
And second of all, reincarnation is something that comes, you know, talked about this before.

59
00:06:38,720 --> 00:06:45,360
It's death that requires faith. And so we can actually turn the tables on people who say this

60
00:06:45,360 --> 00:06:52,240
sort of thing and say, well, what we know is cause and effect. We know that our experience

61
00:06:53,120 --> 00:06:58,880
now and how we react to it and interact with it affects the experience in the future. We know

62
00:06:58,880 --> 00:07:03,920
that that's the case. So extrapolating that ahead into the future is not a leap of faith.

63
00:07:03,920 --> 00:07:13,120
It's the most obvious, it's the most parsimonious conclusion. I've gotten real,

64
00:07:13,120 --> 00:07:18,240
a real heated argument with the materialists about this. He ridiculed me and thought,

65
00:07:18,240 --> 00:07:23,200
you know, obviously they would ridicule this and say how ridiculous it is. But only if you've

66
00:07:23,200 --> 00:07:29,040
already taken as taken for granted that the material world is all there is, if you've already

67
00:07:29,040 --> 00:07:37,440
accepted on faith, or not even on faith, I suppose. But it is kind of, there is a jump there

68
00:07:38,160 --> 00:07:43,920
that this around us is real, that there is something, there are atoms and subatomic particles

69
00:07:43,920 --> 00:07:50,320
that actually somehow exist outside of experience. If you take that, and that is a leap of faith,

70
00:07:52,000 --> 00:07:57,520
compared to the Buddhist concept of simply believing in what is experienced,

71
00:07:57,520 --> 00:08:03,120
like seeing, hearing, smelling, tasting, feeling, thinking, this you can believe in because it's

72
00:08:03,120 --> 00:08:07,520
there. No matter what it is that I'm seeing, you can't deny that there is the experience

73
00:08:08,320 --> 00:08:14,160
that is somehow best labeled as seeing. It's a name and it's an experience. You can't deny that

74
00:08:14,160 --> 00:08:20,320
there's some experience there. That's the most indeniable thing. Now, whether we're in the matrix,

75
00:08:20,320 --> 00:08:25,520
hooked up to a machine, or a brain in the vat, or whether we're actually in a three-dimensional

76
00:08:25,520 --> 00:08:31,920
reality, I'm actually in this room, that is something that you can't answer, or it's less

77
00:08:31,920 --> 00:08:36,720
easy to answer, and it's more of a leap of faith. So materialists do that, and this is what's

78
00:08:36,720 --> 00:08:46,720
required to have this idea that when the physical body dies, because the visible body exists,

79
00:08:46,720 --> 00:08:49,760
and because it's the cause of the mind, two things which you haven't proved,

80
00:08:49,760 --> 00:08:58,480
therefore death equals the end of existence, right? So you've actually taken leaps of faith there.

81
00:08:58,480 --> 00:09:04,880
Now, reincarnation doesn't require that. Reincarnation says what we experience now just continues,

82
00:09:04,880 --> 00:09:10,720
which is in every case the most parsimonious answer. So based on Occam's razor,

83
00:09:10,720 --> 00:09:21,280
the law that if it's more complicated without any need, you cut it off in favor of a more

84
00:09:21,280 --> 00:09:26,560
parsimonious answer or a conclusion. So the most parsimonious is that what we experience now

85
00:09:26,560 --> 00:09:33,360
continues, and that's that there is no death, and so therefore the idea of rebirth will

86
00:09:33,360 --> 00:09:41,520
how that works, that's more theory, and that's really speculation. So the idea that the mind creates

87
00:09:41,520 --> 00:09:48,960
a new body, and there's a new being, that we don't have proof of, but it is still in line with

88
00:09:48,960 --> 00:09:53,680
the most parsimonious, because it's what we observe. So we have to put these two things together,

89
00:09:54,640 --> 00:10:00,560
what we observe and experiential, and what we see in others, oh, that being was born there,

90
00:10:00,560 --> 00:10:06,560
seeming out of nothing, then we have the idea that they were reborn, and that is just theory. But

91
00:10:09,920 --> 00:10:17,040
point being that once we meditate, even if we haven't become enlightened, and reincarnation

92
00:10:17,040 --> 00:10:23,280
actually isn't proven when someone becomes a Sotapanda, or when it becomes an Arahan, even,

93
00:10:23,280 --> 00:10:29,920
you don't prove it there, but you understand it, and you realize why it's so reasonable,

94
00:10:29,920 --> 00:10:34,720
once you start to meditate, because you see how the mind works, and how reality works, how

95
00:10:34,720 --> 00:10:40,880
reality really isn't based on the material world, it's based on experience. Once you see that,

96
00:10:40,880 --> 00:10:47,120
once you break through that and are able to just let go of all these views and ideas and beliefs

97
00:10:47,120 --> 00:10:54,720
and concepts, and just see things as their experience, your whole paradigm shifts, and the idea

98
00:10:54,720 --> 00:11:00,720
of rebirth is just the most reasonable, it's just the most obvious answer.

