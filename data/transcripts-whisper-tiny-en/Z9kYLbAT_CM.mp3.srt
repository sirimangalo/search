1
00:00:00,000 --> 00:00:05,000
Jesse asks how important is meta-meditation?

2
00:00:05,000 --> 00:00:10,000
It's not something I've done often. I'm not even certain I know how to do it correctly.

3
00:00:16,000 --> 00:00:21,000
It is very important because meta is very important for this world.

4
00:00:21,000 --> 00:00:40,000
And it is something that for somebody being on the Buddhist path or practicing the Buddhist teaching should be developed.

5
00:00:40,000 --> 00:01:05,000
We can practice meta, either as a separate meta-meditation or include it in our meditation of Vipassana by examine, examine the mice, not the word,

6
00:01:05,000 --> 00:01:11,000
by making sure that it arises from time to time.

7
00:01:11,000 --> 00:01:36,000
And I think Bhante has a very good talk on how and when meta-meditation should be done and can be done.

8
00:01:41,000 --> 00:01:52,000
And maybe he wants to say more on that data, but for your question, yes, it is very important.

9
00:01:52,000 --> 00:02:10,000
If you want a really good discussion or explanation of all four of the Brahmavihara is Mahasizaya, I did a book on the Brahmavihara, so you can look him up.

10
00:02:10,000 --> 00:02:16,000
I can never recommend anything more than Mahasizaya does teaching, so I would recommend looking up that book.

11
00:02:16,000 --> 00:02:22,000
It is quite a big volume and it actually gives detailed instructions on how to develop loving kindness.

12
00:02:22,000 --> 00:02:29,000
I guess he was asked these questions as well and so they had to come up with it, but I think he is quite clear about putting it in its place.

13
00:02:29,000 --> 00:02:48,000
As I said just before this, there are two ways of understanding that one, you can use it as a support for your meditation or you can use it to develop

14
00:02:48,000 --> 00:03:02,000
some of the tranquility meditation, but the latter should be done with the qualified teacher who is going to lead you through that, lead you to the Janus.

15
00:03:02,000 --> 00:03:19,000
We often do it after our meditation practice, so after you finish your practice, you can do it. You can also do it before your practice to clear up some thoughts.

16
00:03:19,000 --> 00:03:29,000
You can also do it during your practice when you get angry about someone, you can make a determination when you're really frustrated about someone, you can make a determination to set your mind straight.

17
00:03:29,000 --> 00:03:39,000
That was what I was talking about in the talk that Mita actually has a benefit, not just of developing love, but straightening out your mind, because you have a misunderstanding.

18
00:03:39,000 --> 00:03:52,000
You have this crookedness in your mind and so it's helping you to realize that's not the correct way to relate to people, not just to give up the anger but to give up the delusion as well.

19
00:03:52,000 --> 00:04:08,000
The delusion which says this person is bad and so on. When you said Mita, you realize you develop right view, the right view that what is proper is for all beings to be happy and so on.

20
00:04:08,000 --> 00:04:12,000
So you're not just developing love, you're also developing right view, which is quite useful.

21
00:04:12,000 --> 00:04:32,000
This is the same with focusing on the ugly parts of the body, it doesn't just give up lust, it gives up wrong view of the wrong view that this body is beautiful, the wrong view that gives rise to lust, that says that lust is good and so on, or attachment to the body is good.

22
00:04:32,000 --> 00:04:41,000
I think there is a talk but it's probably hard to find because I wasn't naming them back during those talks on the audio page.

23
00:04:41,000 --> 00:04:46,000
I wanted to mention that, it was an audio talk, not a video talk.

24
00:04:46,000 --> 00:04:48,000
I'm trying to find it and name it.

25
00:04:48,000 --> 00:05:10,000
And the other thing I wanted to mention is that when you do it after the meditation practice that you want to add some meta meditation or before you start your vipassana meditation, I would recommend to make a clear break not to confuse both parts.

26
00:05:10,000 --> 00:05:36,000
To clearly know now I'm starting meta meditation and getting out of the mental noting what you can do and send out meta then.

27
00:05:36,000 --> 00:05:50,000
What you can do in your daily life is that you can, when you become aware through your mindfulness that you have throughout the day.

28
00:05:50,000 --> 00:06:17,000
And you know that you don't have meta or that you know that you are angry or upset or hurt or whatever, you can direct your mind towards meta for yourself or for another being that can be done without making a break in whatever you are doing.

29
00:06:17,000 --> 00:06:27,000
But when you do it before or after the vipassana meditation with the noting then you should make a clear break in your mind.

30
00:06:27,000 --> 00:06:33,000
So may that ask me how do we note when we're practicing loving kindness meditation?

31
00:06:33,000 --> 00:06:38,000
What is the noting that we do when we're saying?

32
00:06:38,000 --> 00:06:49,000
But I think at one point you were asking me how we do it to try to figure it out.

33
00:06:49,000 --> 00:06:55,000
I think you could just like when you're giving a dhamma talk when you're teaching.

34
00:06:55,000 --> 00:07:05,000
When I'm speaking right now I can be mindful of the, you know, I'm not saying a bragging about myself, but it's possible that when you're talking to be mindful of the movements of your lips.

35
00:07:05,000 --> 00:07:16,000
It's possible that when you're practicing meta meditation to suddenly switch to vipassana and watch the feelings and be mindful of them.

36
00:07:16,000 --> 00:07:20,000
But it's not the point of meta meditation.

37
00:07:20,000 --> 00:07:25,000
Mahasi Sayada was explaining when you're giving a gift.

38
00:07:25,000 --> 00:07:35,000
You shouldn't, some people will say the only way to give a gift is to say moving, moving, placing, you know, you give arms to the monks, scooping, raising, placing.

39
00:07:35,000 --> 00:07:36,000
And he said that's not the point.

40
00:07:36,000 --> 00:07:43,000
When someone gives a gift they want to make a determination to be born in heaven and so on to have to get rich and so on or whatever.

41
00:07:43,000 --> 00:07:59,000
Whenever you're saying to make a determination like in the old times in stories and ancient times monks who became enlightened did it because they made a determination and gave a gift and then they made a determination.

42
00:07:59,000 --> 00:08:04,000
May I become enlightened because of it based on this gift or so on.

43
00:08:04,000 --> 00:08:18,000
And at the time when you're making determination it shouldn't be with mindfulness. It shouldn't be with full intention that this is for this person and for the benefit of my own mind that I may become free from suffering.

44
00:08:18,000 --> 00:08:24,000
So you're in the conceptual realm and that's a conceptual practice of giving is a conceptual practice.

45
00:08:24,000 --> 00:08:34,000
And may it is a conceptual practice. So during that time you practice may it. In the end all you need is a sweet person.

46
00:08:34,000 --> 00:08:44,000
But the point is that we live in a conceptual reality most of the time. And so to come to terms with those concepts to allow us to develop and we pass.

47
00:08:44,000 --> 00:08:54,000
You have to use you have to use you have no audio no video.

48
00:08:54,000 --> 00:08:58,000
Yeah that's okay. I mean this is recording the audio.

49
00:08:58,000 --> 00:09:22,000
So anyway that's fine. That's something.

