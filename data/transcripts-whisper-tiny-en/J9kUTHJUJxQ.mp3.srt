1
00:00:00,000 --> 00:00:04,500
It's very prominent in the United States right now.

2
00:00:04,500 --> 00:00:13,000
A volunteer you recommend lay Buddhists or people who are trying to move towards a more meditative lifestyle

3
00:00:13,000 --> 00:00:21,000
not to become involved and to concern with politics.

4
00:00:21,000 --> 00:00:24,000
I don't think there's a difficult answer.

5
00:00:24,000 --> 00:00:27,000
Of course, don't get involved with anything.

6
00:00:27,000 --> 00:00:33,000
Do you think politics has some benefit for your meditation?

7
00:00:33,000 --> 00:00:40,000
I would say outside of their meditative sessions,

8
00:00:40,000 --> 00:00:43,000
he's talking about life in general.

9
00:00:43,000 --> 00:00:46,000
You're in the rest of your day or whatever.

10
00:00:46,000 --> 00:00:49,000
Is it going to get in the way with your meditation?

11
00:00:49,000 --> 00:00:54,000
In some sense, reading the news and knowing what's going on is useful.

12
00:00:54,000 --> 00:01:03,000
If you think of it as a functional or a duty that you have to keep informed

13
00:01:03,000 --> 00:01:10,000
so that you're able to respond and interact and know what people are talking about

14
00:01:10,000 --> 00:01:14,000
when they talk about the present or so on,

15
00:01:14,000 --> 00:01:17,000
they can actually be useful.

16
00:01:17,000 --> 00:01:23,000
The point is useful on a functional level for someone who's dwelling in society.

17
00:01:23,000 --> 00:01:26,000
If you're trying to move towards a more meditative lifestyle,

18
00:01:26,000 --> 00:01:27,000
then of course.

19
00:01:27,000 --> 00:01:35,000
But maybe the question could be how important is it to move away from it?

20
00:01:35,000 --> 00:01:40,000
Because obviously giving up more and more things is a great benefit,

21
00:01:40,000 --> 00:01:42,000
focusing more and more on meditation.

22
00:01:42,000 --> 00:01:46,000
But where is it on the scale of giving up politics?

23
00:01:46,000 --> 00:01:49,000
Giving up interest in politics?

24
00:01:49,000 --> 00:01:53,000
I think that depends how involved you are in politics.

25
00:01:53,000 --> 00:01:57,000
If you're running someone's campaign or out campaigning for someone,

26
00:01:57,000 --> 00:02:02,000
then yeah, probably it's something that you should make a decision on.

27
00:02:02,000 --> 00:02:10,000
If it's just watching the polls and looking and seeing who's winning and so on,

28
00:02:10,000 --> 00:02:15,000
then it's just an obsession which a lot of people seem to have.

29
00:02:15,000 --> 00:02:21,000
That among many obsessions you have to see,

30
00:02:21,000 --> 00:02:25,000
is it the last one that I have where I've given up everything about politics?

31
00:02:25,000 --> 00:02:30,000
Maybe it's next to go, but I wouldn't worry too much about it.

32
00:02:30,000 --> 00:02:35,000
No, I guess there's more to the question and a better answer is talking about

33
00:02:35,000 --> 00:02:39,000
how important is politics and what place does politics play?

34
00:02:39,000 --> 00:02:42,000
Because it really doesn't play that big of a part.

35
00:02:42,000 --> 00:02:51,000
It doesn't affect your happiness to any great degree.

36
00:02:51,000 --> 00:02:56,000
I don't know, I mean, if you elect,

37
00:02:56,000 --> 00:03:00,000
if your country elects a government that is corrupt and so on,

38
00:03:00,000 --> 00:03:02,000
that's a really, really bad thing.

39
00:03:02,000 --> 00:03:06,000
But kind of sly as a Buddhist how we can get around this,

40
00:03:06,000 --> 00:03:09,000
and I think it's totally valid,

41
00:03:09,000 --> 00:03:15,000
because you have to ask yourself why do countries elect corrupt leaders?

42
00:03:15,000 --> 00:03:16,000
Because it's a very simple answer.

43
00:03:16,000 --> 00:03:18,000
It's because the people are corrupt.

44
00:03:18,000 --> 00:03:19,000
Because the people are corrupted.

45
00:03:19,000 --> 00:03:24,000
The people are easily influenced by liars.

46
00:03:24,000 --> 00:03:29,000
People are not stupid and most of the politicians seem to think that they are

47
00:03:29,000 --> 00:03:35,000
because they put out these ads and arguments that are really...

48
00:03:35,000 --> 00:03:42,000
You know, I mean, we watch Hollywood, people watch Hollywood movies.

49
00:03:42,000 --> 00:03:44,000
They know what a good actor is,

50
00:03:44,000 --> 00:03:47,000
what someone who really feels what they're saying,

51
00:03:47,000 --> 00:03:49,000
and politicians don't.

52
00:03:49,000 --> 00:03:56,000
They're totally rehashing old lines and so on.

53
00:03:56,000 --> 00:04:06,000
And so, what I mean to say is, if the people of a country are meditative,

54
00:04:06,000 --> 00:04:08,000
let's just go straight to the point,

55
00:04:08,000 --> 00:04:12,000
but are pure, are clear in their minds and so on,

56
00:04:12,000 --> 00:04:16,000
then there's no way that they would elect the wrong guy,

57
00:04:16,000 --> 00:04:17,000
so to speak.

58
00:04:17,000 --> 00:04:21,000
They would elect someone who is kind and who has a clear mind

59
00:04:21,000 --> 00:04:25,000
and who is intelligent and who's helping people and so on.

60
00:04:25,000 --> 00:04:31,000
In a democracy, that's what would happen.

61
00:04:31,000 --> 00:04:35,000
The problem is that people are selfish and bigoted

62
00:04:35,000 --> 00:04:40,000
and full of views and opinions and horrible

63
00:04:40,000 --> 00:04:43,000
have these horrible ideas about what is proper.

64
00:04:43,000 --> 00:04:45,000
So where do we have to start?

65
00:04:45,000 --> 00:04:47,000
It's not getting involved in politics.

66
00:04:47,000 --> 00:04:50,000
It's about changing these people's minds, waking them up.

67
00:04:50,000 --> 00:04:53,000
And that's totally Buddhism.

68
00:04:53,000 --> 00:04:58,000
If the question is, should I be concerned

69
00:04:58,000 --> 00:05:03,000
and involved in politics, for the reason that if I'm not,

70
00:05:03,000 --> 00:05:09,000
it might lead me to have to live in a country with a dictator

71
00:05:09,000 --> 00:05:14,000
or someone who is totally wreaking havoc

72
00:05:14,000 --> 00:05:18,000
on the country's social structure,

73
00:05:18,000 --> 00:05:20,000
then no, you don't have to.

74
00:05:20,000 --> 00:05:23,000
You don't have to. You have to help to change people.

75
00:05:23,000 --> 00:05:25,000
Because the more people you change,

76
00:05:25,000 --> 00:05:28,000
the more people you're going to have a vote for the right guy

77
00:05:28,000 --> 00:05:30,000
or at least don't vote for the wrong guy.

78
00:05:30,000 --> 00:05:33,000
Don't vote for someone who is going to,

79
00:05:33,000 --> 00:05:40,000
whatever who's corrupt and evil and so on.

80
00:05:40,000 --> 00:05:43,000
So if you can change people,

81
00:05:43,000 --> 00:05:46,000
oh, you change so much in a society.

82
00:05:46,000 --> 00:05:48,000
You change the entertainment.

83
00:05:48,000 --> 00:05:51,000
You change people's attitudes.

84
00:05:51,000 --> 00:05:54,000
Suddenly the entertainment starts to reflect that.

85
00:05:54,000 --> 00:05:58,000
And as an example.

86
00:05:58,000 --> 00:06:03,000
And it's a good example because entertainment affects everyone.

87
00:06:03,000 --> 00:06:04,000
It affects politicians.

88
00:06:04,000 --> 00:06:06,000
They mirror themselves.

89
00:06:06,000 --> 00:06:09,000
They want to be like the movie stars.

90
00:06:09,000 --> 00:06:13,000
I don't know the entertainment would be the perfect category,

91
00:06:13,000 --> 00:06:15,000
but I'm thinking of America now.

92
00:06:15,000 --> 00:06:18,000
If you can change the entertainment in America,

93
00:06:18,000 --> 00:06:23,000
maybe it's just because America is so fixed on and fixated on entertainment,

94
00:06:23,000 --> 00:06:25,000
then you can change everything.

95
00:06:25,000 --> 00:06:26,000
You can change politics.

96
00:06:26,000 --> 00:06:29,000
You can change society.

97
00:06:29,000 --> 00:06:30,000
You can change peoples.

98
00:06:30,000 --> 00:06:32,000
You can change the whole.

99
00:06:32,000 --> 00:06:36,000
That's the scary thing, I suppose.

100
00:06:36,000 --> 00:06:40,000
Because everyone wants to be like the movie stars.

101
00:06:40,000 --> 00:06:43,000
Anyway, maybe that's going over the top,

102
00:06:43,000 --> 00:06:47,000
but the point is sound, I think,

103
00:06:47,000 --> 00:06:50,000
that you help the people.

104
00:06:50,000 --> 00:06:51,000
Change the people.

105
00:06:51,000 --> 00:06:52,000
Change people's thoughts.

106
00:06:52,000 --> 00:06:53,000
Change the direction.

107
00:06:53,000 --> 00:06:56,000
Give them new ideas, good ideas.

108
00:06:56,000 --> 00:07:00,000
And you'll change the political system much,

109
00:07:00,000 --> 00:07:02,000
much better than you could by voting

110
00:07:02,000 --> 00:07:05,000
or even campaigning for someone who is in general.

111
00:07:05,000 --> 00:07:09,000
There's no such thing as a pure politician, I don't think.

112
00:07:09,000 --> 00:07:11,000
Or it's very hard to find.

113
00:07:11,000 --> 00:07:13,000
Everyone gets corrupted by it

114
00:07:13,000 --> 00:07:15,000
and has to compromise themselves

115
00:07:15,000 --> 00:07:18,000
by the nature of the system.

116
00:07:18,000 --> 00:07:22,000
But the nature of the fact that corruption is built into it

117
00:07:22,000 --> 00:07:24,000
in general.

118
00:07:24,000 --> 00:07:28,000
And the only way to change that is to change people's minds.

119
00:07:28,000 --> 00:07:30,000
Change the people around you.

120
00:07:30,000 --> 00:07:33,000
Change, start making YouTube videos

121
00:07:33,000 --> 00:07:35,000
where you tell people good things.

122
00:07:35,000 --> 00:07:37,000
Teach people good stuff.

123
00:07:37,000 --> 00:07:41,000
And yeah, self-congratulatory.

