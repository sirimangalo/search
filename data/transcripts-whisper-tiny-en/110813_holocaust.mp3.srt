1
00:00:00,000 --> 00:00:06,800
Welcome back to Ask a Monk. This is, I think, a short, hopefully a short video, talking

2
00:00:06,800 --> 00:00:13,760
about the, it's an old question that I've just been putting off about the, the working

3
00:00:13,760 --> 00:00:23,240
of karma in regards to a holocaust. How does karma work when you're talking about mass

4
00:00:23,240 --> 00:00:30,720
monitoring, mass realizations of, of a certain state. I think actually a holocaust is a bad

5
00:00:30,720 --> 00:00:36,880
example, but it does highlight some of part of the answer, so I'll take it as a part of it.

6
00:00:36,880 --> 00:00:48,800
But let's consider several, in several examples. The holocaust, a tsunami, and the planes crashing

7
00:00:48,800 --> 00:00:53,560
into the world trade center. Let's examine all three of these. The, the holocaust is actually

8
00:00:53,560 --> 00:00:58,760
the easiest one to see, because the answer in all three cases is that not, no one dies in

9
00:00:58,760 --> 00:01:04,160
the same way. No one experiences things in the same way or has the same sort of suffering.

10
00:01:04,160 --> 00:01:08,520
This is really easy to see in a holocaust, because even though so many people were killed

11
00:01:08,520 --> 00:01:12,880
in similar ways, none of them died in the same way, and none of them experienced it in the

12
00:01:12,880 --> 00:01:17,080
same way. Some of them might have been calm, some of them would have been very frightened,

13
00:01:17,080 --> 00:01:25,920
some of them would have been enraged, and so on. The death of each person is totally dependent

14
00:01:25,920 --> 00:01:31,200
on their mind state. So to say that all of these people died in the same way, and therefore,

15
00:01:31,200 --> 00:01:39,280
it's hard to believe that it was all based on their karma is really a superficial examination.

16
00:01:39,280 --> 00:01:44,480
Tsunami is another good example, because you think all of these people ground, well, that's,

17
00:01:44,480 --> 00:01:50,320
it's not karma, that's an act of nature. But it really is, it's hard to say one way

18
00:01:50,320 --> 00:01:57,480
or the other, because the people died based on their situation, individual situations,

19
00:01:57,480 --> 00:02:02,320
and though everyone, it's true that many people drowned, their each individual's drowning

20
00:02:02,320 --> 00:02:10,960
was quite different. So their experience of the, of the affair or the event was, would

21
00:02:10,960 --> 00:02:17,320
be quite different as well. Also their, their experience and their response to it would

22
00:02:17,320 --> 00:02:22,080
have been quite different. As I said, some people will be, quite disturbed by it, some

23
00:02:22,080 --> 00:02:28,280
people will be quite calm or reasonably calm. Some people will die instantly, some people

24
00:02:28,280 --> 00:02:33,840
would take a long time to die and so on. It totally depends, I think it, it mainly depends

25
00:02:33,840 --> 00:02:39,160
on the person's karma, and you may say there are other, you know, in fact, the word

26
00:02:39,160 --> 00:02:43,320
karma, it's, first of all, as I've said, the Buddha, the Buddha didn't, didn't talk

27
00:02:43,320 --> 00:02:47,320
about karma, he talked about the mind and the intentions that we have in the mind. And

28
00:02:47,320 --> 00:02:52,000
then he said, only a Buddha could possibly understand karma. It's one of those things that

29
00:02:52,000 --> 00:02:56,120
you should never think about, the workings of karma, what caused this, what caused that?

30
00:02:56,120 --> 00:03:01,000
Because all it means is that things go by cause and effect and the mind is, is caudually

31
00:03:01,000 --> 00:03:05,560
effective. That's all it means. The mind is not an epiphenomenon, it's not something that

32
00:03:05,560 --> 00:03:14,240
is created by the brain, it's an active participant in reality. But what causes lead to

33
00:03:14,240 --> 00:03:20,000
what results is so intricately intertwined that you can never really break it apart. So

34
00:03:20,000 --> 00:03:25,040
you can say, what was the karma that caused those people to be brought to the tsunami?

35
00:03:25,040 --> 00:03:33,160
Well, you're talking about a whole confluence, I think it's the word, of different causes

36
00:03:33,160 --> 00:03:37,960
that bring everyone together just in that one instant. And it's, it's just a fluctuation

37
00:03:37,960 --> 00:03:42,360
of the universe. It's, it's scientific, you know, science would explain this tsunami and

38
00:03:42,360 --> 00:03:46,880
all the people dying in terms of atoms coming together and just came to get happened to

39
00:03:46,880 --> 00:03:53,920
come together in that way. Karma simply, you know, explains the part that the mind played

40
00:03:53,920 --> 00:03:59,080
in all that, to bring people together into this one place and then to separate. And again,

41
00:03:59,080 --> 00:04:03,960
as I say, it's not really that they were all in one place or one experience, because each

42
00:04:03,960 --> 00:04:10,920
person's experience would be an indeed quite different. The third example is in a plane crash,

43
00:04:10,920 --> 00:04:17,160
which is the hardest to see, I think. But even there, you still have differences of experience

44
00:04:17,160 --> 00:04:23,120
and more importantly, differences of reaction to the experience. And therefore, different

45
00:04:23,120 --> 00:04:33,120
results and, and you could say different, different karma that led to it. But there is

46
00:04:33,120 --> 00:04:40,120
an element of, of group and mutual karma. And it has to, it has to do with how we are

47
00:04:40,120 --> 00:04:44,640
intertwined with each other, why we are born to the parents that we are, why we live in

48
00:04:44,640 --> 00:04:49,640
the same house with certain people, why we work with certain people are coming together

49
00:04:49,640 --> 00:04:55,160
and, you know, why wouldn't we equally ask, isn't it strange that we should all be working

50
00:04:55,160 --> 00:05:00,080
in the same job or isn't it strange that we should all be born into the same family?

51
00:05:00,080 --> 00:05:05,400
It's all the same. It all has to do with our path and we meet up and we experience certain

52
00:05:05,400 --> 00:05:14,040
events, death being one of them, sometimes tragic and, and simultaneous death. But it all

53
00:05:14,040 --> 00:05:19,040
depends on, on so many different factors that bring you together. And of course, it has

54
00:05:19,040 --> 00:05:26,880
to do with the nature of the physical body which isn't able to bring or, or, or prevents

55
00:05:26,880 --> 00:05:32,120
the mind from bringing about results and spontaneously. If we're all mental, you, you

56
00:05:32,120 --> 00:05:37,880
receive karmic results much quicker for instance angels and ghosts and so on. They receive

57
00:05:37,880 --> 00:05:45,480
karmic results much quicker when, when an angel gets angry, I think they fall, I think

58
00:05:45,480 --> 00:05:50,320
it angry enough they die simply by being angry. Now it takes a lot of anger for a human

59
00:05:50,320 --> 00:05:55,040
being to die. But for an angel, it's not that much because it changes their mind and

60
00:05:55,040 --> 00:06:03,160
because they're mostly mental with a very limited physical, simply a little amount of anger

61
00:06:03,160 --> 00:06:09,200
can cause them to, to change their state because the mind is very quick to change. But

62
00:06:09,200 --> 00:06:15,840
the body is not because the body is so coarse and as I said before has such inertia. You

63
00:06:15,840 --> 00:06:22,240
can take a long time for it to catch up with us. And as a result, it can all build up

64
00:06:22,240 --> 00:06:26,320
buildup and build up until the body has a chance in the case of a plane crash and all

65
00:06:26,320 --> 00:06:31,000
this buildup of karma and so on that brings people together and pushes them all into the

66
00:06:31,000 --> 00:06:35,800
same position and then suddenly snaps. This is the way the physical works because of the

67
00:06:35,800 --> 00:06:43,800
buildup of power and the, the capacity it has to, to collect static, static energy.

68
00:06:43,800 --> 00:06:48,640
Though that's how the physical works, the physical has so much static energy in it that this

69
00:06:48,640 --> 00:06:54,800
is how nuclear atomic bombs are created because there's so much energy and matter. And

70
00:06:54,800 --> 00:07:01,200
this is all just a part of it, how, how matter works in ways that somehow mask the, the

71
00:07:01,200 --> 00:07:06,600
workings of the mind because of, simply because of inertia and static energy. So that's

72
00:07:06,600 --> 00:07:11,600
really all that, it's, it shouldn't be a great mystery as to why people all die together

73
00:07:11,600 --> 00:07:17,960
and it shouldn't be certainly a, a, this proof of the act, you know, the workings of

74
00:07:17,960 --> 00:07:22,400
karma because karma is such a Buddhist concept of karma is such an intricate thing.

75
00:07:22,400 --> 00:07:27,200
All you can say is that when you do good things, your, your, your life changes and your

76
00:07:27,200 --> 00:07:33,800
path changes in a good way. When you do bad things, it changes in a bad way. You know,

77
00:07:33,800 --> 00:07:42,360
what exactly that will be and the, the scale and the nature is very, very, very difficult

78
00:07:42,360 --> 00:07:47,400
to understand because it involves so many different things working. Once I'm obviously,

79
00:07:47,400 --> 00:07:53,760
it could never be so simple as a leads to B and C leads to D because you have so many

80
00:07:53,760 --> 00:08:00,560
different causes every moment creating new cause, creating new effects and so on.

81
00:08:00,560 --> 00:08:06,320
So I hope that helps to give an explanation of how karma can possibly work in the case

82
00:08:06,320 --> 00:08:12,440
of a Holocaust doesn't seem to be at all out of line with the idea of, of cause and

83
00:08:12,440 --> 00:08:18,320
effect. I mean, it obviously is cause and effect. There, there, there is obviously something

84
00:08:18,320 --> 00:08:21,800
that is a cause for this. It's something that we don't see, we don't think about this

85
00:08:21,800 --> 00:08:26,080
whole idea of karma, how, you know, all of these people who died in the Holocaust could

86
00:08:26,080 --> 00:08:32,280
possibly be guilty of something. But it really, you know, if you look at how many people

87
00:08:32,280 --> 00:08:37,840
are guilty of such horrible atrocities, even in this life and where it can possibly lead

88
00:08:37,840 --> 00:08:45,880
them in the future and how, you know, how, where these people will be going in the future.

89
00:08:45,880 --> 00:08:53,600
Again, you know, you can have people who are developing on wholesome mind states and setting

90
00:08:53,600 --> 00:08:58,040
themselves up for great suffering in the future. On a massive scale, this sort of thing

91
00:08:58,040 --> 00:09:03,000
occurs. People who engage in racism, people who engage in bigotry and you know, you can

92
00:09:03,000 --> 00:09:07,440
engage in, you know, who have great hatred in their mind. I mean, even those places where

93
00:09:07,440 --> 00:09:12,800
people don't actively hurt others, but simply build up and cultivate prejudice and hate.

94
00:09:12,800 --> 00:09:17,680
I think there are a lot of people like this in the world. There's cultures of like this

95
00:09:17,680 --> 00:09:23,280
in the world, and I'm not going to name names, but many cultures in many different places,

96
00:09:23,280 --> 00:09:27,640
many different countries in the world have a great amount of hate and prejudice for

97
00:09:27,640 --> 00:09:35,440
things that are foreign or for a certain group of people and so on, certain type of people.

98
00:09:35,440 --> 00:09:40,880
And that's sort of thing that will lead you to become a victim of your own anger and

99
00:09:40,880 --> 00:09:47,560
your own hatred and, of course, other people's taking advantage of that, taking advantage

100
00:09:47,560 --> 00:10:00,720
of your or becoming a part of that stream of karmic energy. So that's my take on the Holocaust.

101
00:10:00,720 --> 00:10:17,600
So thanks for tuning in on all the best.

