1
00:00:00,000 --> 00:00:10,840
Okay, good evening, everyone broadcasting live, December 5th, it's the King of Thailand's

2
00:00:10,840 --> 00:00:16,240
birthday today.

3
00:00:16,240 --> 00:00:26,880
Wonder what's happening in the kingdom?

4
00:00:26,880 --> 00:00:31,760
There's news today, any interesting news?

5
00:00:31,760 --> 00:00:38,800
I didn't watch too much news today, I was out most of the day getting things done.

6
00:00:38,800 --> 00:00:56,800
All the coverage here is still on the shootings in California, but getting a lot of calls.

7
00:00:56,800 --> 00:01:04,560
I've gotten calls from people today and yesterday, hey, I found you on meetup.

8
00:01:04,560 --> 00:01:09,760
Ah, good, meetup is really actually a big thing.

9
00:01:09,760 --> 00:01:10,760
Yeah.

10
00:01:10,760 --> 00:01:21,800
Oh, tomorrow, I think I'd better skip the Visudimaga class, we better just cancel it, I guess.

11
00:01:21,800 --> 00:01:35,640
Because there's a interfaith peace meeting, meeting of the interfaith peace group in Hamilton.

12
00:01:35,640 --> 00:01:41,160
One of the leaders is an ex from a master professor, her husband was one of my professor

13
00:01:41,160 --> 00:01:48,560
16 years ago and I've just reconnected with him and so I'm on their email list and there

14
00:01:48,560 --> 00:01:51,400
was one a few weeks ago and I didn't go.

15
00:01:51,400 --> 00:02:01,480
But tomorrow, I think someone's coming maybe to pick me up here on their way, so I should

16
00:02:01,480 --> 00:02:02,480
probably go.

17
00:02:02,480 --> 00:02:08,720
Sure, I'll send out a message to cancel the class for tomorrow, but I think I'll be able

18
00:02:08,720 --> 00:02:17,320
to make it for the two meetings, is that the thing is at 2.30, oh yeah, yeah, I'll make

19
00:02:17,320 --> 00:02:23,160
it for the meetings, maybe I'll have to leave the second one a bit early, I don't know.

20
00:02:23,160 --> 00:02:25,600
That sounds good.

21
00:02:25,600 --> 00:02:36,720
The next week I have exams, one exam and then I have to start studying for the other two.

22
00:02:36,720 --> 00:02:43,640
So I have to read Descartes, Locke and Spinoza.

23
00:02:43,640 --> 00:02:53,240
I mean, so that's good to read about, sorry, I think some of the things these guys believe

24
00:02:53,240 --> 00:03:00,960
are kind of weird, but it's good to know.

25
00:03:00,960 --> 00:03:08,760
It gives you language, learning the way Western philosophers thought and spoke and the things

26
00:03:08,760 --> 00:03:14,400
they discussed, it allows us to explain Buddhism, and many of them were influenced, not

27
00:03:14,400 --> 00:03:19,080
many of them, some of them were influenced by Buddhism, so some of them have actually

28
00:03:19,080 --> 00:03:25,200
interesting ways of describing and explaining things that we also describe and explain,

29
00:03:25,200 --> 00:03:31,040
but in a Western context, so there's benefit there.

30
00:03:31,040 --> 00:03:33,440
Was the most modern philosopher that you're studying?

31
00:03:33,440 --> 00:03:41,360
Well, it's early modern philosophy, so these three are the cusp of early modern thought

32
00:03:41,360 --> 00:03:48,920
when people started to question the church and started to try to find ways of knowing

33
00:03:48,920 --> 00:04:00,600
that were outside of the Bible says, so they mostly believed in God still, but they started

34
00:04:00,600 --> 00:04:14,600
to question many things, started to figure out how to know, how to learn.

35
00:04:14,600 --> 00:04:19,200
Probably wouldn't take the class again if I had to do it over.

36
00:04:19,200 --> 00:04:22,440
Is there a part two with more modern philosophy?

37
00:04:22,440 --> 00:04:30,720
More philosophy courses I can take, kind of turned off a philosophy courses, though.

38
00:04:30,720 --> 00:04:42,880
Too much, too speculative, really, for me, because Buddhist, there's a woman of Chinese

39
00:04:42,880 --> 00:04:47,880
ethnicity, I guess you could say she's Canadian, but her parents I think are from China

40
00:04:47,880 --> 00:04:54,720
in my class and I asked her what she thought of the class and she said, she kind of

41
00:04:54,720 --> 00:05:02,240
gave a disgusted noise, and we were talking about I asked her, she's in Eastern philosophy,

42
00:05:02,240 --> 00:05:11,000
Chinese philosophy, and I said, yeah, I think I had enough with Western philosophy, maybe

43
00:05:11,000 --> 00:05:17,280
I should have taken that one instead, not sure why I didn't, maybe it's a night class

44
00:05:17,280 --> 00:05:18,280
or something.

45
00:05:18,280 --> 00:05:29,640
Do I think I'll probably stick with it for another semester, because stopping in January

46
00:05:29,640 --> 00:05:35,040
seems a bit troublesome?

47
00:05:35,040 --> 00:05:40,200
With the whole, I mean, they're paid for my tuition, the government paid for my tuition

48
00:05:40,200 --> 00:05:46,560
for the whole year, I'm not sure how they look at it if I dropped it.

49
00:05:46,560 --> 00:05:47,560
It's complicated, I think.

50
00:05:47,560 --> 00:05:49,560
I think so.

51
00:05:49,560 --> 00:05:54,400
Have you signed up for classes for next semester?

52
00:05:54,400 --> 00:05:55,400
Is it too early?

53
00:05:55,400 --> 00:05:57,080
Oh, but I'm probably going to change it.

54
00:05:57,080 --> 00:06:05,240
I'm probably going to take the second half of Latin, because it is useful, it's the base

55
00:06:05,240 --> 00:06:11,720
of English, it's one of the bases of English, but also because it's pretty easy.

56
00:06:11,720 --> 00:06:16,720
I'm currently getting, I think, 103% in the class.

57
00:06:16,720 --> 00:06:22,880
I've got over 100% in each of the quizzes, so it's, I wasn't going to take the second

58
00:06:22,880 --> 00:06:26,920
half of it, and now I thought, well, it's so easy, you know, it's one last thing I have

59
00:06:26,920 --> 00:06:31,560
to really expend a lot of energy in.

60
00:06:31,560 --> 00:06:33,960
They don't have poly or Sanskrit?

61
00:06:33,960 --> 00:06:38,960
They have Sanskrit, but I was afraid to take it, because my Sanskrit's very rusty, and

62
00:06:38,960 --> 00:06:41,280
this was going to be my first go.

63
00:06:41,280 --> 00:06:47,560
So I can't take it now in January, I'd have to start it next semester, next September.

64
00:06:47,560 --> 00:06:52,600
Probably should have taken it in retrospect, but that was cautious.

65
00:06:52,600 --> 00:06:58,240
Also I was thinking I'd be in Stony Creek, so yeah.

66
00:06:58,240 --> 00:07:07,720
Now, it's good to be cautious, your first semester back, don't want to get overwhelmed.

67
00:07:07,720 --> 00:07:12,720
So I came back, I could take it, fourth year Sanskrit, it's really hardcore.

68
00:07:12,720 --> 00:07:19,000
Oh, you might not be, might not be, might not be, probably going to take it, because I can't

69
00:07:19,000 --> 00:07:27,200
remember much of Sanskrit grammar, they'd be really rusty.

70
00:07:27,200 --> 00:07:31,400
They don't have anything more introductory than fourth year?

71
00:07:31,400 --> 00:07:37,680
Well, they, it's strange how they made it the first year, the first class is third year.

72
00:07:37,680 --> 00:07:41,680
And the second class is fourth year, there's no first or second year.

73
00:07:41,680 --> 00:07:46,760
I think the point being that it's more, more designed, they don't want first years

74
00:07:46,760 --> 00:07:47,760
taking it.

75
00:07:47,760 --> 00:07:52,640
I think they did that, because half the people end up failing the course, or when I took

76
00:07:52,640 --> 00:07:57,320
it, I think half the people dropped out or failed it.

77
00:07:57,320 --> 00:08:01,800
And that's when it was set as a third year class.

78
00:08:01,800 --> 00:08:11,520
So I think the point is, if they made it a first year class, it's just a recipe for disaster.

79
00:08:11,520 --> 00:08:12,520
Yeah.

80
00:08:12,520 --> 00:08:15,840
And make it a third year class, I guess there's a lot of prerequisites, I think you might

81
00:08:15,840 --> 00:08:19,200
have to be enough for you to take it or something.

82
00:08:19,200 --> 00:08:27,480
You ready for, ready for a question, Monday?

83
00:08:27,480 --> 00:08:29,280
Sure.

84
00:08:29,280 --> 00:08:34,320
How does one note wanting to not do something as in trying to suppress a behavior?

85
00:08:34,320 --> 00:08:36,840
Can I say not wanting?

86
00:08:36,840 --> 00:08:44,720
Hmm, sure.

87
00:08:44,720 --> 00:08:55,080
There was once this non-time woman who, I think she wasn't a nun in the beginning, I

88
00:08:55,080 --> 00:09:00,000
said, that sounds very good at ordaining people.

89
00:09:00,000 --> 00:09:08,920
She was bulimic, I think, and so she couldn't keep food down, she would throw it in.

90
00:09:08,920 --> 00:09:12,760
And maybe bulimic, or maybe she just couldn't keep it down, I don't know, maybe it was

91
00:09:12,760 --> 00:09:16,200
a sickness.

92
00:09:16,200 --> 00:09:23,720
And so she explained this to her, many had her noting, you know, a tien-noa throwing up, throwing

93
00:09:23,720 --> 00:09:36,960
up, vomiting, vomiting, and she started, she was practicing, but it was really difficult

94
00:09:36,960 --> 00:09:37,960
for her.

95
00:09:37,960 --> 00:09:49,960
And so she came back one day and said, and Jan, and Jan, I want to die, she said, I can't

96
00:09:49,960 --> 00:09:51,160
take it, that's something.

97
00:09:51,160 --> 00:10:01,800
And I want to die, or something like, I'm not, I think it started off, I'm not, was it

98
00:10:01,800 --> 00:10:06,760
my why, which means I can't take it, basically.

99
00:10:06,760 --> 00:10:12,160
And they said, I can't take it, just say to yourself, can't take it, can't take it.

100
00:10:12,160 --> 00:10:19,440
And he said, and Jan, I want to kill my, I want to die, I want to die, I want to die.

101
00:10:19,440 --> 00:10:26,600
And Jan, I don't want to live, not wanting to live, no, he really doesn't get phased,

102
00:10:26,600 --> 00:10:36,960
he gave her no quarter, and finally she started laughing, and she broke down, she let

103
00:10:36,960 --> 00:10:43,880
go a little bit, you know, and started laughing, and Jan, and he started laughing.

104
00:10:43,880 --> 00:10:51,880
And I think then she became an aunt, she ended up becoming an aunt, she really was able

105
00:10:51,880 --> 00:11:03,120
to overcome it, but that, that's, it was kind of being facetious, I think, I mean, you

106
00:11:03,120 --> 00:11:07,200
really shouldn't say, I don't want to live, not wanting to live, not wanting to live,

107
00:11:07,200 --> 00:11:12,520
it should be a little more specific than that, like you're upset or sad or depressed,

108
00:11:12,520 --> 00:11:22,960
it's a bit better, but you see how sometimes that's the best way to, when people are

109
00:11:22,960 --> 00:11:26,560
stubborn, especially, you don't want to live, well, then just say, no, I want to think

110
00:11:26,560 --> 00:11:34,240
of living, no one, I mean, it's kind of this kind of question, it's very much about semantics,

111
00:11:34,240 --> 00:11:40,000
it's not really a real question, it's not really an important one, I'm not sorry, I don't

112
00:11:40,000 --> 00:11:45,120
mean to, to critical, but I think if you look closer, you'll see that there's, there's

113
00:11:45,120 --> 00:11:51,880
realities that are a bit more specific and real than whether you, you're, you want something

114
00:11:51,880 --> 00:11:56,480
or you don't want it, and so I'll just words, what's the state of mind is the question,

115
00:11:56,480 --> 00:12:01,520
what's going on in the mind, that's the question, if it's a, if you want something, if you

116
00:12:01,520 --> 00:12:06,680
want, or if you have desire or rise, then it's desire, if you have a version or rise in

117
00:12:06,680 --> 00:12:10,920
it's a version, a very distinct mind state, and you can tell which one, not often they're

118
00:12:10,920 --> 00:12:17,280
mixed up with each other, there's desire, then there's a version, there's delusion, there's

119
00:12:17,280 --> 00:12:20,360
lots of states.

120
00:12:20,360 --> 00:12:28,440
A follow-up comment from the questioner, it's, it's, it's, it's clearly wanting, but

121
00:12:28,440 --> 00:12:31,680
he's not sure if he needs to emphasize the negation.

122
00:12:31,680 --> 00:12:38,680
I think you have to say to yourself, thinking, thinking, wondering, wondering, the whole

123
00:12:38,680 --> 00:12:43,680
thing without being, sounds more, more important.

124
00:12:43,680 --> 00:12:55,920
I have a 10 year old niece who always asks me questions about Buddhism.

125
00:12:55,920 --> 00:13:00,080
I was thinking about getting her a book on jot at the stories, I think you have experienced

126
00:13:00,080 --> 00:13:01,720
teaching kids.

127
00:13:01,720 --> 00:13:02,720
What do you think?

128
00:13:02,720 --> 00:13:03,880
Is it age appropriate?

129
00:13:03,880 --> 00:13:06,560
Can you recommend any additions?

130
00:13:06,560 --> 00:13:16,720
There are jot because for children, by Ken and Risako Kawasaki, you find them on BuddhaNet.net.

131
00:13:16,720 --> 00:13:20,280
Jot because stories for kids, most of them are pretty good.

132
00:13:20,280 --> 00:13:26,080
They're retold in a way that kids find helpful.

133
00:13:26,080 --> 00:13:32,240
Someone sent me a book for children, jot at the stories, retold, there was a monk, it's

134
00:13:32,240 --> 00:13:40,880
actually even by this book apparently, it's like Buddhist bedtime stories or something.

135
00:13:40,880 --> 00:13:52,880
But yeah, the one by Ken and Risako Kawasaki is probably a good bet.

136
00:13:52,880 --> 00:14:03,120
We've been looking for that.

137
00:14:03,120 --> 00:14:10,120
Have you, another thing is maybe show her my DVD on how to meditate for kids, or some

138
00:14:10,120 --> 00:14:14,560
good lessons in there on how, on different ways of meditating, opening kids eyes up, getting

139
00:14:14,560 --> 00:14:17,240
them an idea of what meditation is.

140
00:14:17,240 --> 00:14:30,880
It's not just insight meditation, but meditation primer for kids, for videos, for the

141
00:14:30,880 --> 00:14:31,880
world.

142
00:14:31,880 --> 00:14:32,880
I think it's good.

143
00:14:32,880 --> 00:14:39,800
I've seen good results with it for a three and a five-year-old kid that's, they were

144
00:14:39,800 --> 00:14:44,120
able to understand it, or just my book on how to meditate.

145
00:14:44,120 --> 00:15:01,000
She's 11 and she can already start to learn how to meditate from that book, but probably.

146
00:15:01,000 --> 00:15:10,200
If an habit dies or leaves some monastery, how is the next habit selected?

147
00:15:10,200 --> 00:15:15,760
Habits aren't, habits aren't a part of Buddhism.

148
00:15:15,760 --> 00:15:19,560
We don't have habits in a minute, we may.

149
00:15:19,560 --> 00:15:25,360
So that's totally dependent on the country, and there's nothing to do with Buddhism directly.

150
00:15:25,360 --> 00:15:30,920
Many monks have complained about that, or not many monks have complained about that, that

151
00:15:30,920 --> 00:15:34,000
habits in most countries have too much power.

152
00:15:34,000 --> 00:15:38,200
It's not according to the windy at all.

153
00:15:38,200 --> 00:15:39,200
According to the windy.

154
00:15:39,200 --> 00:15:43,600
The sangha has to be in charge, not for one person.

155
00:15:43,600 --> 00:15:46,320
Isn't habit different than a head monk?

156
00:15:46,320 --> 00:15:52,240
There's no such thing as a head monk or an habit, just that term doesn't exist.

157
00:15:52,240 --> 00:15:53,240
Okay.

158
00:15:53,240 --> 00:16:02,760
It goes by seniority always, so if it's more senior monk comes, he's more senior.

159
00:16:02,760 --> 00:16:18,400
It goes by seniority in some cases for decision making and has to go by the sangha.

160
00:16:18,400 --> 00:16:23,040
And even still, you know, there's not, well, if you're staying in a place where there's

161
00:16:23,040 --> 00:16:26,960
not four monks, then you can't have a sangha, so then it's just, you know, you try to

162
00:16:26,960 --> 00:16:39,520
get along and try to be respectful to your seniors, that kind of thing.

163
00:16:39,520 --> 00:16:48,000
Well, we hear those terms all the time, head monk and habit, so it's just something

164
00:16:48,000 --> 00:16:53,280
that just kind of came to be, but it's not really supposed to be.

165
00:16:53,280 --> 00:16:55,280
Really?

166
00:16:55,280 --> 00:17:07,560
You have to be careful, it can be an ego trip in the head monk.

167
00:17:07,560 --> 00:17:11,520
Seniority with monks, is it years as a monk or years of life?

168
00:17:11,520 --> 00:17:22,400
It's moments as a monk, so I was ordained with 18 monks and I think I was the second to last.

169
00:17:22,400 --> 00:17:30,920
So all the others were senior to me, actually I'm not sure if that's true because we

170
00:17:30,920 --> 00:17:32,840
ordained in threes, so I don't know how that works.

171
00:17:32,840 --> 00:17:40,800
I think it still works according to, according to, there's an order, but we ordained

172
00:17:40,800 --> 00:17:44,840
three at a time, so let's not sure how that quaverics.

173
00:17:44,840 --> 00:17:48,400
Anyway, none of those, I'm the only one left for those 18.

174
00:17:48,400 --> 00:17:56,080
So now you're the senior, well, they're all disrogued, yes, but yeah, you have to, so when

175
00:17:56,080 --> 00:17:59,760
you meet a monk, you have to ask when they were ordained and sometimes you have to calculate.

176
00:17:59,760 --> 00:18:06,000
Usually you say how many, how many reigns are you and then, even if it's an equal number

177
00:18:06,000 --> 00:18:09,360
then you have to ask when would they be ordained?

178
00:18:09,360 --> 00:18:17,760
I'm older than most monks, my age because I wasn't most monks at least in Thailand because

179
00:18:17,760 --> 00:18:23,600
most high monks ordained before the reigns, but I ordained for the king's birthday.

180
00:18:23,600 --> 00:18:29,840
Oh, today's my birthday, no, I ordained yesterday, December 4th, I miss my birthday.

181
00:18:29,840 --> 00:18:33,680
Oh, congratulations, or stop thinking about it.

182
00:18:33,680 --> 00:18:39,120
So that's, I was trying to think how many is that that's 14 now, 14 years a monk yesterday,

183
00:18:39,120 --> 00:18:43,600
because I ordained for the king's birthday, which is today, but we ordained the day before

184
00:18:43,600 --> 00:18:49,360
because on the king's birthday, there's too much going on.

185
00:18:49,360 --> 00:18:54,000
So yeah, my birthday was yesterday.

186
00:18:54,000 --> 00:18:56,240
Well, happy belated birthday, won't they?

187
00:18:56,240 --> 00:19:00,400
Thank you.

188
00:19:01,200 --> 00:19:04,720
Do you have a sangha in Hamilton?

189
00:19:04,720 --> 00:19:11,920
Well, there's, in Stony Creek, there's at least three, sometimes four monks, so

190
00:19:11,920 --> 00:19:18,080
together with them we can, we can form a sangha, then there's a lot of ocean monk,

191
00:19:18,080 --> 00:19:22,880
and there's a bunch more lot of ocean monks and Sri Lankan monks in Toronto area.

192
00:19:24,000 --> 00:19:28,960
There's lots of monks around if we want to perform a sangha, but I'm alone, so I don't have a sangha here.

193
00:19:28,960 --> 00:19:37,920
Do any of them get together to recite the Patimoka?

194
00:19:37,920 --> 00:19:44,880
Not that I know of.

195
00:19:49,680 --> 00:19:55,200
So I'm going to try to give you the, the cake emote in the meditation chat, but it's not working.

196
00:19:55,200 --> 00:20:00,320
Doesn't work. No, I, I had noticed that too. I was trying to do that for someone's birthday at

197
00:20:00,320 --> 00:20:06,320
some at one point, but I can fix that.

198
00:20:11,520 --> 00:20:17,440
Okay, cake to you must just not be showing it for some reason on the

199
00:20:17,440 --> 00:20:33,360
strain. Not the cakes, indeed.

200
00:21:47,440 --> 00:22:03,360
Let's find that symbol.

201
00:22:17,440 --> 00:22:45,360
So strange.

202
00:22:47,920 --> 00:23:09,680
It should be, it should work actually. It looks like, funny looking at programming code.

203
00:23:09,680 --> 00:23:20,480
It's all gibberish. Computers are crazy things. Yes, they are. We have some fairly inappropriate

204
00:23:21,280 --> 00:23:28,320
emoticons for meditation chat panel. There's like a cocktail and they can get rid of them.

205
00:23:28,320 --> 00:23:45,520
Someone, I can't quite see what they are. Maybe a pizza. Oh, okay, pizza. Pizza's fine.

206
00:23:59,280 --> 00:24:11,120
Lots of angry faces, devil, and the, we tend not to use those. I'm pretty happy bunch here.

207
00:24:11,120 --> 00:24:33,040
I'm going to try something to tell me if it works.

208
00:24:41,120 --> 00:25:10,960
Oh, I wonder if, no, I don't know.

209
00:25:11,520 --> 00:25:19,040
I don't know what's happening. Bump for this. I don't really know what's going on.

210
00:25:23,200 --> 00:25:31,520
Place that. Oh, wait, it's insert smiley that we want. This is the problem. Message,

211
00:25:31,520 --> 00:25:41,440
vowel, vowel, plus S, insert smiley. Okay, so each smiley box, we have to find a smiley box.

212
00:25:44,880 --> 00:25:51,280
Oh, but it's, there's a populate smiley thing with it. We populate the box.

213
00:25:53,840 --> 00:25:58,960
Is this done on WordPress as well, buddy? No, it's just something I put together.

214
00:25:58,960 --> 00:26:03,520
Oh, probably not something we want to work on in a live forum.

215
00:26:08,160 --> 00:26:09,440
Everyone's trying the cake.

216
00:26:13,280 --> 00:26:17,760
All right, I'll see you. Can you work on it and also answer a question at the same time?

217
00:26:18,640 --> 00:26:21,040
I shouldn't do two things at once. What's the question?

218
00:26:21,920 --> 00:26:24,160
Is intense fear common in meditation?

219
00:26:24,160 --> 00:26:30,080
I mean, any question that asks whether something's common is really

220
00:26:31,680 --> 00:26:36,800
not really, you can't really answer that. I mean, there's no such thing as common.

221
00:26:38,400 --> 00:26:40,160
Everyone has different experiences.

222
00:26:42,480 --> 00:26:47,440
I mean, what would it matter if it was common? What does that mean? That doesn't really mean

223
00:26:47,440 --> 00:26:51,920
anything? Maybe that's another way of saying, this is happening to me.

224
00:26:51,920 --> 00:26:54,880
Am I doing something wrong? Yeah, am I doing something wrong?

225
00:26:56,960 --> 00:27:00,160
I mean, even asking whether you're doing something wrong is in this understanding.

226
00:27:02,480 --> 00:27:07,600
Generally, because if you're being mindful, then you're doing it right. If you're not being mindful,

227
00:27:07,600 --> 00:27:12,720
then it doesn't matter what happens. Doing it wrong doesn't have anything to do with what happens.

228
00:27:13,840 --> 00:27:15,520
It's how you react to what happens.

229
00:27:15,520 --> 00:27:26,480
That is a good point. If you're doing it right, you think, why is this fear coming up in me?

230
00:27:26,480 --> 00:27:47,040
I'm meditating, but that's really a limitation. It often helps you to see things that you don't want to see.

231
00:27:47,040 --> 00:27:56,720
But can we meditate to help others be mindful?

232
00:28:05,600 --> 00:28:11,200
No, not directly. I mean, if you're mindful, it encourages other people and it provides a good

233
00:28:11,200 --> 00:28:18,320
example for them, but I don't really see how it would be possible to certainly

234
00:28:18,320 --> 00:28:29,280
not think about meditating down. Why would that be something you think possible?

235
00:28:29,280 --> 00:28:40,000
It may be helping someone meditating to give a wish that someone could be mindful.

236
00:28:41,680 --> 00:28:47,760
Maybe like sending meta, sending a wish that someone can be mindful. Is that possible?

237
00:28:48,480 --> 00:28:54,080
Well, if wishes were fishes, then we'd have a fry.

238
00:28:54,080 --> 00:29:02,080
It must be a Canadian saying, that's an American saying, I was talking about an American.

239
00:29:03,920 --> 00:29:07,200
I don't know. It could be. I'm sure I haven't heard all the sayings.

240
00:29:09,440 --> 00:29:14,640
Okay, you know what's happening? This is as nothing to do with the smileies. The smileies are being

241
00:29:14,640 --> 00:29:18,480
put correctly. So I can turn all those things into cakes. I do have to figure out

242
00:29:18,480 --> 00:29:26,560
something's going on somewhere else. We're formatting the chat improperly.

243
00:29:30,560 --> 00:29:37,680
Okay, so for our meditation group, we have smileies that indicate swearing, punching, angry,

244
00:29:37,680 --> 00:29:50,160
envy, devil. I think we need some new smileies. Vamiting.

245
00:29:54,400 --> 00:30:01,200
Okay, you know what I'm going to do? I'm going to send myself.

246
00:30:01,200 --> 00:30:10,960
This is just good and you're all going to get a bunch of alerts now that we're going to make

247
00:30:10,960 --> 00:30:15,760
you think it's going crazy. It's going to go crazy for a bit just because I have to test and see

248
00:30:15,760 --> 00:30:25,280
what's going on. I mean, just don't refresh your browser.

249
00:30:45,760 --> 00:31:02,000
Okay, that's fine.

250
00:31:45,760 --> 00:32:14,040
Okay, it is removing that, it is removing that thing from

251
00:32:14,040 --> 00:32:18,640
it is a cake, the only one not working, did anyone know that anything else

252
00:32:18,640 --> 00:32:25,640
that is not working? I think that is the only one I have come across.

253
00:32:48,640 --> 00:33:16,640
Oh, well, it is beyond me. No cake. It is a good lesson for us all. Let me remove that.

254
00:33:16,640 --> 00:33:23,640
And we want to remove the martini. Well, there is a bunch of them, there is like punching

255
00:33:23,640 --> 00:33:31,640
and swearing and hitting and puking and puking. Puking is part of the meditation process.

256
00:33:31,640 --> 00:33:36,640
That is true. I am not sure that anyone is ever used that in chat though.

257
00:33:36,640 --> 00:33:44,640
But it is kind of cool, it does take the all characters, I didn't realize that or hadn't thought about that in a while.

258
00:33:44,640 --> 00:33:52,640
It faces and then we got drink, probably the drink one with it.

259
00:33:52,640 --> 00:33:55,640
Is it a deep with princesses? Is that the one?

260
00:33:55,640 --> 00:33:58,640
It looks like double princesses.

261
00:33:58,640 --> 00:34:05,640
Yeah, it looks like ninja, can you keep the ninja?

262
00:34:05,640 --> 00:34:15,640
Which is kind of cool. It was playful.

263
00:34:15,640 --> 00:34:22,640
Got rid of the finger and then the drunk smoking beer and mooning those ones are coming.

264
00:34:22,640 --> 00:34:31,640
It is like deleted already. It is a coffee phone.

265
00:34:31,640 --> 00:34:38,640
It is punching right diagonal from the green one.

266
00:34:38,640 --> 00:34:40,640
That is the code for that.

267
00:34:40,640 --> 00:34:46,640
Double princesses punch.

268
00:34:46,640 --> 00:34:51,640
Punch.

269
00:34:51,640 --> 00:35:01,640
Oh, cute.

270
00:35:01,640 --> 00:35:04,640
You are going to give up the swearing one?

271
00:35:04,640 --> 00:35:06,640
I don't think anyone is ever used that.

272
00:35:06,640 --> 00:35:10,640
Swear and full bar will get rid of punch.

273
00:35:10,640 --> 00:35:14,640
Based on fingers crossed.

274
00:35:14,640 --> 00:35:18,640
Got the cash money sign there.

275
00:35:18,640 --> 00:35:21,640
If sometimes you have to talk about money.

276
00:35:21,640 --> 00:35:27,640
Sure.

277
00:35:27,640 --> 00:35:30,640
Maybe. We are kind of just wasting time, aren't we?

278
00:35:30,640 --> 00:35:34,640
Well, I think there was another question. I am sorry.

279
00:35:34,640 --> 00:35:38,640
Falling down on my duties here.

280
00:35:38,640 --> 00:35:47,640
When needing someone for the first time, what question do you find most helpful to develop and understanding between each other?

281
00:35:47,640 --> 00:35:56,640
I am so interested in questioning.

282
00:35:56,640 --> 00:36:00,640
I don't think of such things. I don't worry about such things.

283
00:36:00,640 --> 00:36:03,640
I think it should be mindful when you meet people.

284
00:36:03,640 --> 00:36:05,640
That is more important.

285
00:36:05,640 --> 00:36:09,640
Can you imagine if you had one question that you asked everybody?

286
00:36:09,640 --> 00:36:13,640
Wouldn't it come off really like a canned, you know?

287
00:36:13,640 --> 00:36:18,640
I like a pickup line.

288
00:36:18,640 --> 00:36:27,640
You must say that to all the Buddhists.

289
00:36:27,640 --> 00:36:31,640
I don't have a list of questions that I have a list of questions that I ask meditators.

290
00:36:31,640 --> 00:36:35,640
Got some fairly wrote questions there.

291
00:36:35,640 --> 00:36:39,640
When I meet people, I try to be as natural as I can.

292
00:36:39,640 --> 00:36:47,640
I think being natural is important, not having an agenda, not having anything in behind, you know?

293
00:36:47,640 --> 00:36:49,640
Not coming as a Buddhist, even.

294
00:36:49,640 --> 00:36:59,640
Coming at people from the point of view of human being, as natural as possible.

295
00:36:59,640 --> 00:37:04,640
So that's what we are aiming for, is nature beyond nature.

296
00:37:04,640 --> 00:37:09,640
Because the nature, as we know it, is almost up.

297
00:37:09,640 --> 00:37:11,640
It's not really natural.

298
00:37:11,640 --> 00:37:17,640
But the nature that we understand is like rape is natural and murder is natural.

299
00:37:17,640 --> 00:37:19,640
Is it all part of the natural world?

300
00:37:19,640 --> 00:37:22,640
But they're very unnatural things.

301
00:37:22,640 --> 00:37:26,640
So, through meditation we become quite natural in a sense.

302
00:37:26,640 --> 00:37:54,640
In a sense of eddies and without pretense or agenda, without artifice, without any sort.

303
00:37:54,640 --> 00:37:57,640
I think we should just say goodnight.

304
00:37:57,640 --> 00:37:59,640
Thank you, Bhante.

305
00:37:59,640 --> 00:38:00,640
Thank you, Robin.

306
00:38:00,640 --> 00:38:01,640
Thanks, everyone.

307
00:38:01,640 --> 00:38:02,640
Have a good night.

308
00:38:02,640 --> 00:38:31,640
Good night.

