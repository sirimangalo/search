1
00:00:00,000 --> 00:00:21,000
In Vinaya, they are called medicine, oil, ghee, oil, honey, and molasses,

2
00:00:21,000 --> 00:00:28,000
and ghee, ghee, molasses, oil, honey, and these five are termed medicine in Vinaya.

3
00:00:28,000 --> 00:00:57,000
So, they can be taken as medicine, so we can, months can take honey in the afternoon,

4
00:00:58,000 --> 00:01:11,000
only as medicine.

5
00:01:12,000 --> 00:01:19,000
On the next page, the one molasses appear.

6
00:01:19,000 --> 00:01:26,000
It will be the end of the first five paragraphs, and he argues it for life consisting of oil, honey, molasses,

7
00:01:26,000 --> 00:01:36,000
ghee, etc. That is allowed by a medical man as suitable for the city as what is meant.

8
00:01:37,000 --> 00:01:46,000
Now, in paragraph 97, in the second part, the explanation of the word,

9
00:01:46,000 --> 00:01:55,000
pachea and pari. The word meaning here is this, because breathing things go, move, proceed,

10
00:01:55,000 --> 00:02:07,000
using what they use in dependence on these ropes, etc. These ropes, etc. are therefore called requisites.

11
00:02:07,000 --> 00:02:18,000
In fact, instead of saying, move, proceed, we should say, live.

12
00:02:19,000 --> 00:02:23,000
Breathing things means beings, living beings.

13
00:02:23,000 --> 00:02:40,000
So, living beings go or living beings live or are alive, in dependence on these ropes and other things, using them.

14
00:02:40,000 --> 00:02:52,000
That is why they are called pachea in pari, but it is not important whether you don't understand the meaning of what pachea.

15
00:02:52,000 --> 00:02:57,000
If you know that it means requisites, then it is enough.

16
00:02:57,000 --> 00:03:12,000
Now, in the next paragraph, the full-full module has to be undertaken by means of faith and so on.

17
00:03:12,000 --> 00:03:22,000
So, in this paragraph, that is accomplished by faith, since the announcing of training precepts is outside the disciples' province.

18
00:03:22,000 --> 00:03:33,000
It is very important. That's why we cannot change the rules. We cannot add anything to the rules or we cannot take anything out of the rules.

19
00:03:33,000 --> 00:03:41,000
Because the announcing or laying down the rules is not in the province of disciples.

20
00:03:41,000 --> 00:03:46,000
Disciples cannot do that, only the border can do that.

21
00:03:46,000 --> 00:04:02,000
So, if a monk has no faith or no devotion to the border and his teachings, then he will not want to follow or to keep all these rules.

22
00:04:02,000 --> 00:04:16,000
That is why the particular restraint of the first virtue is to be undertaken by means of faith, by means of devotion to the border.

23
00:04:16,000 --> 00:04:26,000
And the evidence here is the refusal of the request to allow disciples to announce training precepts.

24
00:04:26,000 --> 00:04:33,000
And that is not the, those in the brackets are to be stricken out.

25
00:04:33,000 --> 00:04:36,000
Seeing failure.

26
00:04:36,000 --> 00:04:44,000
Refusal of the request to announce training precepts, not to allow disciples to know.

27
00:04:44,000 --> 00:05:08,000
It refers to Vinya. Sorry Buddha asked Buddha, how the dispensation of different Buddhas, some dispensation of some Buddhas last long and some did not last long.

28
00:05:08,000 --> 00:05:15,000
And so, Buddha answered that there were precepts and so on.

29
00:05:15,000 --> 00:05:20,000
So, sorry Buddha requested the Buddha to lay down rules.

30
00:05:20,000 --> 00:05:26,000
So please lay down rules so that the dispensation of Buddha can endure long.

31
00:05:26,000 --> 00:05:33,000
Then Buddha said, no, it is not yet time.

32
00:05:33,000 --> 00:05:39,000
And that means you do not know when to lay down rules, but I know Buddha would have meant.

33
00:05:39,000 --> 00:05:45,000
So, laying down of rule is not in the province of disciples.

34
00:05:45,000 --> 00:05:56,000
So, the watch of the brackets are not in the origin, but put by the translator, but wrongly.

35
00:05:56,000 --> 00:06:11,000
So, here is the, the evidence here is the refusal of the request to announce training precepts, but to lay down rules.

36
00:06:11,000 --> 00:06:23,000
Having therefore undertaken through faith, the training precepts without exception as announced wanted completely perfect them without regard, even for life.

37
00:06:23,000 --> 00:06:28,000
So, let us put the word even here, even for life.

38
00:06:28,000 --> 00:06:38,000
For this is said, as a hand-guts or eggs or as a yak or tail or like a darling child or like an only eye.

39
00:06:38,000 --> 00:06:43,000
So, you who are engaged, your virtue do perfect, be prudent.

40
00:06:43,000 --> 00:06:52,000
Be prudent really means be fond of your virtue at all times and ever scrupulous.

41
00:06:52,000 --> 00:07:08,000
Scrupulous really means, but the follower really means have respect for the rules, have respect for the Bora, Dhamma, Sangha and so on.

42
00:07:08,000 --> 00:07:10,000
And then we have the story.

43
00:07:10,000 --> 00:07:15,000
So, the story is not difficult to understand.

44
00:07:15,000 --> 00:07:25,000
Now, he augmented his insight. I hope you understand that. He augmented his insight.

45
00:07:25,000 --> 00:07:32,000
What's that?

46
00:07:32,000 --> 00:07:35,000
He saw in the future.

47
00:07:35,000 --> 00:07:45,000
Actually, he practiced Wipasana Matitez.

48
00:07:45,000 --> 00:07:53,000
And the second story, also they bought another elder in Tambapani Island with string creepers and made him lie down.

49
00:07:53,000 --> 00:08:00,000
When a forest by a king and the creepers were not cut, no, here it should be.

50
00:08:00,000 --> 00:08:06,000
After the creepers came, without cutting the creepers, he established insight.

51
00:08:06,000 --> 00:08:08,000
Because he was born with creepers.

52
00:08:08,000 --> 00:08:15,000
And when the forest by a king, he could easily cut the creepers and escape.

53
00:08:15,000 --> 00:08:19,000
But to cut the creepers means to break the rule of the Bora.

54
00:08:19,000 --> 00:08:25,000
So, he would as soon a give of his life then break the rules of the Bora.

55
00:08:25,000 --> 00:08:31,000
So, without cutting the creepers, he practiced meditation.

56
00:08:31,000 --> 00:08:34,000
And he became an arrowhand.

57
00:08:34,000 --> 00:08:44,000
So, when a forest by a king, without cutting the creepers, he established insight and attain nib on a simultaneously with his death.

58
00:08:44,000 --> 00:08:49,000
So, simultaneously real means almost simultaneously.

59
00:08:49,000 --> 00:08:59,000
Not at the same moment, but you know mine works very fast and so it is almost instantaneous.

60
00:08:59,000 --> 00:09:06,000
So, he became an arrowhand.

61
00:09:06,000 --> 00:09:14,000
So, the advice given with regard to this story is,

62
00:09:14,000 --> 00:09:19,000
meaning the rules of conduct pure renouncing life if there be nibs,

63
00:09:19,000 --> 00:09:23,000
rather than break fudges restraint by the world's severe decrease.

64
00:09:23,000 --> 00:09:28,000
So, you should give of your life, rather than breaking the rules,

65
00:09:28,000 --> 00:09:33,000
split down by the Buddha.

66
00:09:33,000 --> 00:09:36,000
Okay.

67
00:09:36,000 --> 00:09:42,000
Do you have any questions?

68
00:09:42,000 --> 00:09:49,000
I am a little curious in reading all of this about essentially about asceticism.

69
00:09:49,000 --> 00:09:52,000
And how asceticism is life and work.

70
00:09:52,000 --> 00:09:57,000
It is a traditional story of the Buddha renouncing asceticism.

71
00:09:57,000 --> 00:10:01,000
How are those two related?

72
00:10:01,000 --> 00:10:02,000
Yeah.

73
00:10:02,000 --> 00:10:08,000
Now, when they decided Buddha renounced asceticism,

74
00:10:08,000 --> 00:10:21,000
it means he renounced the unnecessarily inflicting pain on oneself or self-modification.

75
00:10:21,000 --> 00:10:28,000
For example, when he was in the forest practicing to become the Buddha,

76
00:10:28,000 --> 00:10:31,000
he reduced his food little by little.

77
00:10:31,000 --> 00:10:38,000
First, he went out for arms and ate the food.

78
00:10:38,000 --> 00:10:43,000
And then he took fruit from trees and ate them.

79
00:10:43,000 --> 00:10:49,000
And later, he took only the fruits that had fallen.

80
00:10:49,000 --> 00:10:57,000
And later on, he took the fruit only from the tree and of which he was living.

81
00:10:57,000 --> 00:11:07,000
And so, little by little, he reduced eating so that his body became very thin and initiated.

82
00:11:07,000 --> 00:11:13,000
That is an unnecessarily inflicting suffering on himself.

83
00:11:13,000 --> 00:11:19,000
So, that kind of asceticism Buddha refused a whole day night.

84
00:11:19,000 --> 00:11:28,000
But the ascetic practices given here are not that severe, not that rigorous.

85
00:11:28,000 --> 00:11:36,000
Like eating in one bowl only or going out for arms and then when going for arms,

86
00:11:36,000 --> 00:11:39,000
you do not skip any house and so on.

87
00:11:39,000 --> 00:11:44,000
So, these are called ascetic practices.

88
00:11:44,000 --> 00:11:59,000
But not like those practiced by the other hummus of sages during his time.

89
00:11:59,000 --> 00:12:07,000
We will come to the ascetic practices in the second chapter.

90
00:12:07,000 --> 00:12:13,000
One thing I want to say is, when I went to a Zen Center,

91
00:12:13,000 --> 00:12:21,000
in Japan, after eating in their bowls, they rinse the bowl

92
00:12:21,000 --> 00:12:25,000
and then they drink the water from the bowls.

93
00:12:25,000 --> 00:12:37,000
So, it reminded me of the one of the ascetic practices that take over the months and practice.

94
00:12:37,000 --> 00:12:40,000
And there is eating in one bowl only.

95
00:12:40,000 --> 00:12:48,000
If you have only to use one bowl, then you eat in that bowl and you drink in that bowl.

96
00:12:48,000 --> 00:13:04,000
So, I think there is some practices carried to countries far away from India and then we have changed a little.

97
00:13:04,000 --> 00:13:13,000
And so, it seemed to become a very different practice.

98
00:13:13,000 --> 00:13:19,000
But I think there is something common in both practices.

99
00:13:19,000 --> 00:13:24,000
When we eat in the Zen Del, we eat that way with bowls and with wash your bowls.

100
00:13:24,000 --> 00:13:36,000
And also, you are always served, and it is like arms.

101
00:13:36,000 --> 00:13:42,000
We don't do begging, but the way we eat in the Zen Del, it is very similar.

102
00:13:42,000 --> 00:13:43,000
That is right.

103
00:13:43,000 --> 00:13:53,000
And when monks are invited to cultures of lay people during the time of the Buddha,

104
00:13:53,000 --> 00:14:05,000
or even at the present time in Sri Lanka, they sit at the monks on the floor in the row.

105
00:14:05,000 --> 00:14:11,000
And then lay people take food and then put in their bowls one by one.

106
00:14:11,000 --> 00:14:15,000
Like they are eating in 10 Zen centers.

107
00:14:15,000 --> 00:14:27,000
So, that is the practice done in India and also in Sri Lanka, but in Vermont, it is different.

108
00:14:27,000 --> 00:14:40,000
So, we can see many similarities of many common practices that have become a little changed,

109
00:14:40,000 --> 00:14:48,000
depending on the country and on the people, on the races.

110
00:14:48,000 --> 00:14:53,000
But the original intention seems to be there and every...

111
00:14:53,000 --> 00:14:54,000
That is right.

112
00:14:54,000 --> 00:14:55,000
Yeah.

113
00:14:55,000 --> 00:15:01,000
These are all men, men, to not do Tanga means because they shake off.

114
00:15:01,000 --> 00:15:08,000
It is to be shake off the filaments that these practices have to be taken.

115
00:15:08,000 --> 00:15:14,000
And fewness of wands and no attachment to food and all these things.

116
00:15:14,000 --> 00:15:30,000
So, probably by practicing that particular act, I guess keep reminding yourself of why you are eating.

117
00:15:30,000 --> 00:15:44,000
You always have to be on your guard to avoid mental defilements from coming to your mind.

118
00:15:44,000 --> 00:15:54,000
Many people talk about if they don't like their job or if they don't feel good about their livelihood.

119
00:15:54,000 --> 00:16:05,000
So, it is something we can see here how most of them were warned or to be careful.

120
00:16:05,000 --> 00:16:06,000
That is how we have it.

121
00:16:06,000 --> 00:16:07,000
Oh, yes.

122
00:16:07,000 --> 00:16:13,000
So, in a lot of ways, that is another reflection about how they are more complex society.

123
00:16:13,000 --> 00:16:17,000
How we can be careful about our behavior.

124
00:16:17,000 --> 00:16:25,000
So, maybe in the exact same way, but...

125
00:16:25,000 --> 00:16:27,000
That is right now.

126
00:16:27,000 --> 00:16:31,000
...how an each act, how easy it is to be selfish.

127
00:16:31,000 --> 00:16:38,000
Even if it is not in the very little thing.

128
00:16:38,000 --> 00:16:49,000
So, try to put Buddhism in some way and every day life.

129
00:16:49,000 --> 00:16:52,000
That is right.

130
00:16:52,000 --> 00:17:01,000
And in one of the soldiers in Angutra, the daily reflections are given there.

131
00:17:01,000 --> 00:17:08,000
If Mankari or a live person must make these reflections.

132
00:17:08,000 --> 00:17:15,000
That means I am getting old and I cannot avoid getting old and I will get disease and

133
00:17:15,000 --> 00:17:19,000
I cannot avoid that every one day, I cannot get away from it and so on.

134
00:17:19,000 --> 00:17:28,000
So, these reflections that we made every day both by months and lay people so that they can get rid of pride.

135
00:17:28,000 --> 00:17:38,000
And they are used, pride in their health and pride in their belongings and so on.

136
00:17:38,000 --> 00:17:41,000
That is a very good soda.

137
00:17:41,000 --> 00:17:44,000
Both are bunks and lemons.

138
00:17:44,000 --> 00:17:55,000
This is in the Angutra, the gradual scenes.

139
00:17:55,000 --> 00:17:59,000
I do not remember the name of the soda.

140
00:17:59,000 --> 00:18:05,000
But do you think that might be some problem or something?

141
00:18:05,000 --> 00:18:08,000
It might not be for everyone.

142
00:18:08,000 --> 00:18:12,000
Some person with a depressive tendency.

143
00:18:12,000 --> 00:18:19,000
You do that too much and you might just go to the grocery store.

144
00:18:19,000 --> 00:18:26,000
No, no, no.

145
00:18:26,000 --> 00:18:29,000
No.

146
00:18:29,000 --> 00:18:36,000
These reflections are to be made not to get depressed.

147
00:18:36,000 --> 00:18:44,000
It is to get rid of pride in yourself or in your appearance or in your body or whatever.

148
00:18:44,000 --> 00:18:56,000
So, it is for the bubbles of getting rid of undesirable mental traits that these have to be practiced.

149
00:18:56,000 --> 00:19:02,000
Not to be carried too far.

150
00:19:02,000 --> 00:19:15,000
You know, once when Buddha thought about the foundness of the body, monks got so, so difficult.

151
00:19:15,000 --> 00:19:21,000
So, disgusted with their bodies that they killed themselves and they asked other people to kill them.

152
00:19:21,000 --> 00:19:25,000
That really happened during the time of the Buddha.

153
00:19:25,000 --> 00:19:38,000
And Buddha knew that it would have been, but he could not avoid it because in the commentaries it is explained that the comma they did in the past, they did together in the past.

154
00:19:38,000 --> 00:19:42,000
Got the opportunity to give result at that time.

155
00:19:42,000 --> 00:19:51,000
And so, Buddha thought if they had to die, let them die with this kind of meditation, it would have them.

156
00:19:51,000 --> 00:19:56,000
They had a good rebirth.

157
00:19:56,000 --> 00:20:00,000
So, Buddha thought the foundness of the body meditation.

158
00:20:00,000 --> 00:20:07,000
And then he said, I must not be approached by any month of fifteen days, two weeks.

159
00:20:07,000 --> 00:20:09,000
I want to be alone.

160
00:20:09,000 --> 00:20:13,000
Only the monk who brings food must approach me.

161
00:20:13,000 --> 00:20:18,000
So, after fifteen days he came out of his inclusion.

162
00:20:18,000 --> 00:20:22,000
And then there are a few monks now.

163
00:20:22,000 --> 00:20:30,000
And then I said, because you taught that now, you taught that meditation.

164
00:20:30,000 --> 00:20:36,000
And so, another asked Buddha to teach some other kind of meditation.

165
00:20:36,000 --> 00:20:42,000
So, he taught me breathing meditation at the time.

166
00:20:42,000 --> 00:20:48,000
Yeah, that's in the book of discipline.

167
00:20:48,000 --> 00:21:06,000
If you want to read the book of discipline, I cannot tell the page, but the third of the, the first four defeat rooms.

168
00:21:06,000 --> 00:21:13,000
So, the third one is not to kill human beings.

169
00:21:13,000 --> 00:21:20,000
So, killing human beings is a grave offense for the monks.

170
00:21:20,000 --> 00:21:28,000
So, that story is given there.

171
00:21:28,000 --> 00:21:37,000
Okay, thank you, thank you.

172
00:21:37,000 --> 00:21:49,000
So, please read to the end of our chapter for next week.

173
00:21:49,000 --> 00:22:06,000
Okay, thank you.

174
00:22:06,000 --> 00:22:26,000
Thank you.

