1
00:00:00,000 --> 00:00:21,800
Good evening, broadcasting live.

2
00:00:21,800 --> 00:00:38,840
So today's quote starts with the word eta.

3
00:00:38,840 --> 00:00:50,360
It is a very powerful word, it's very iconically Buddhist, it's a really good dhamma

4
00:00:50,360 --> 00:01:08,120
verse, it starts with eta, eta pass eta passatimang lo kang, kang look at this world, and then

5
00:01:08,120 --> 00:01:25,320
see if I can find that verse, jit dang rang, jah rang, jah rang, jah rang, jah rang, jah rang,

6
00:01:25,320 --> 00:01:43,280
jah brang, jah brang, jah brang, jah brang, jah brang, jah brang, jah brang,

7
00:01:43,280 --> 00:01:53,760
Tambala, we see Dantin, this song goes with Janata.

8
00:01:53,760 --> 00:02:04,960
Come, look, come, eta, eta means come, it's the imperative form of the verb.

9
00:02:04,960 --> 00:02:16,600
The root is just letter I, it becomes 80, 80 is he goes, he shing, or he comes,

10
00:02:16,600 --> 00:02:21,440
or he goes, or comes, I think it can be neither.

11
00:02:21,440 --> 00:02:40,240
eta, come, passata, see, look, eta, passata is a core part of Buddhism, come and see.

12
00:02:40,240 --> 00:02:57,280
The dhamma of the Buddha is called ahi-pasikum, ahi-pasa, ahi-pasa, that, bhasati, and anika.

13
00:02:57,280 --> 00:03:03,760
Bhasika, something that can be seen, ahi-pasika is something that can be seen,

14
00:03:03,760 --> 00:03:08,640
something that you can come and see, to write on the word.

15
00:03:08,640 --> 00:03:12,400
Because ahi is actually imperative, come.

16
00:03:12,400 --> 00:03:16,320
Ahi-pasika, something that you can say about it, come and see,

17
00:03:16,320 --> 00:03:23,920
something that invites you to come and see, something about which you can say,

18
00:03:23,920 --> 00:03:32,880
this is come and see him all, and it's interesting that that becomes a word,

19
00:03:32,880 --> 00:03:39,360
because this is something that the Buddha says often, eta, passata.

20
00:03:39,360 --> 00:03:45,280
And so, the dhamma of the verses, eta, passata, come and look at this world.

21
00:03:45,280 --> 00:03:53,680
Jitangra, jharatu, pamang, that is decked out in the mind,

22
00:03:53,680 --> 00:04:00,240
like a, like a king's chariot, it's all.

23
00:04:00,240 --> 00:04:06,320
It appears to the mind to be, bejeweled, to be golden, to be royal,

24
00:04:06,320 --> 00:04:10,320
like a royal chariot, all decked out.

25
00:04:12,000 --> 00:04:21,440
Yatambala, we see danti, this world which the full sinks into,

26
00:04:21,440 --> 00:04:37,040
we see danti, becomes mired in, nuti, sango, vijan, etang, but for in regards to which,

27
00:04:37,040 --> 00:04:53,520
there is no association by the wise, vijanata, vijanata, people who see clearly,

28
00:04:53,520 --> 00:05:05,440
or no, who understand, who have, vijanata is an interesting word to know clearly,

29
00:05:05,440 --> 00:05:12,000
like we have, we passana means to secretly, vijanata means to know clearly,

30
00:05:12,000 --> 00:05:13,920
the same thing, means wisdom.

31
00:05:16,560 --> 00:05:24,080
So fools, bala, become immeshed and mired in this world because it's shiny,

32
00:05:24,080 --> 00:05:34,480
but there is no association for, with the world for those who see clearly, no clearly.

33
00:05:36,160 --> 00:05:40,160
That's not our verse today, that's not the passage that we're looking at today,

34
00:05:40,160 --> 00:05:43,680
but it's interesting sort of apropos.

35
00:05:46,320 --> 00:05:49,040
So let's switch and look at our verse here.

36
00:05:49,040 --> 00:05:53,280
Our verse today is from nyangutarani kaya,

37
00:05:53,280 --> 00:06:20,160
sorry, it's from the nyangutarani kaya, it's actually a set of verses.

38
00:06:23,760 --> 00:06:30,640
A set of passages, five passages.

39
00:06:30,640 --> 00:06:38,880
So the quote that we have on our page doesn't adequately represent what's actually going on.

40
00:06:39,920 --> 00:06:44,160
This isn't actually the Buddha saying to the monks, come,

41
00:06:44,160 --> 00:06:52,480
what you say, it says, it's yay te anana nyutrabhajita.

42
00:06:52,480 --> 00:06:56,320
Adunagata, imang tamavindayana,

43
00:06:58,240 --> 00:07:07,440
whoever monk is newly gone forth, adunagata, what does that, adunah, adunah.

44
00:07:07,440 --> 00:07:17,440
Adunah, agata, have recently come, imang tamavindayana to this tamavindayana.

45
00:07:17,440 --> 00:07:22,160
Devo ananda, they buy you ananda.

46
00:07:22,160 --> 00:07:28,080
Devo ananda beku, those beku's ananda buy you.

47
00:07:28,080 --> 00:07:40,800
Samatapetabha should be enjoined, should be incited, should be rallied, should be exhorted.

48
00:07:42,480 --> 00:07:46,400
But just so dhammi in regards to five dhammas.

49
00:07:47,200 --> 00:07:50,800
So the neat thing about this quote is there's actually five things that's part of a bigger

50
00:07:50,800 --> 00:08:01,360
set. You just take that quote out of context, it actually, it's a good one, it's an important one,

51
00:08:01,360 --> 00:08:06,640
but there's five aspects to this. It's why you can't really easily quote the Buddha

52
00:08:06,640 --> 00:08:13,520
is because you have to understand the context and understand it's much deeper than just quoting

53
00:08:13,520 --> 00:08:21,680
something. In this case, there's five, five parts. There's five things new, you should tell

54
00:08:21,680 --> 00:08:27,920
them, people who come new. So as with much of the Buddha's teachings, it's talking about monks,

55
00:08:27,920 --> 00:08:35,600
but it fits just as well with lay people, people who have come to meditate for the first time.

56
00:08:35,600 --> 00:08:43,840
They should also be exhorted with these sorts of things.

57
00:08:47,200 --> 00:08:59,360
In which five, oh, Samatapetabha, they should be enjoined or they should be caused to take up

58
00:08:59,360 --> 00:09:07,120
Samatapetabha, you should encourage them to take up. Niywe say tabha, you should establish them,

59
00:09:07,120 --> 00:09:14,080
you should encourage them to become settled in these things. But the tabetabha, you should cause

60
00:09:14,080 --> 00:09:23,920
them to be established in these things, settle, established, and to take up these things,

61
00:09:23,920 --> 00:09:33,200
which five, and then he gives five quotes, eta tung Hei, also, come new friend,

62
00:09:35,920 --> 00:09:39,840
Silawahu tah, be moral, be ethical.

63
00:09:43,600 --> 00:09:52,720
Butimoka Samwara Samuata, we had a tah, indwell guarding the rules, guarding ethical precepts. Among

64
00:09:52,720 --> 00:09:57,760
things that we got lots of rules, the patimoka, that's what it's referring to, but you can adapt it,

65
00:09:58,480 --> 00:10:01,760
it's just be ethical.

66
00:10:01,760 --> 00:10:10,000
Ah, tah, rangu, tah, rasam, no your pasture, no what is your pasture and what is not.

67
00:10:10,000 --> 00:10:22,880
And of ah, tah, rangu, tah, rasam, maybe mindful of ah, tah, means conduct. Go tah,

68
00:10:22,880 --> 00:10:29,040
rangu means pasture. Go is a cow, so go tah, is a place that a cow goes, it's literally

69
00:10:29,040 --> 00:10:39,200
means pasture. But it's adapted to mean your limits or your boundaries, so know whatever it's

70
00:10:39,200 --> 00:10:43,920
proper to go, or it's not proper to go, what did it's proper to do, the sorts of people that's

71
00:10:43,920 --> 00:10:48,320
proper to hang out with, proper to seek advice from that kind of thing.

72
00:10:51,840 --> 00:11:01,280
Anumatesu, what desu be a desa, you know, dwell as one who sees the danger by a desa,

73
00:11:01,280 --> 00:11:07,520
you know, desa, you know, who sees the danger. Anumatesu, what desu, in the smallest

74
00:11:07,520 --> 00:11:14,240
of faults, the smallest of faults. See the danger in the smallest of rangu.

75
00:11:17,440 --> 00:11:27,840
Samadhaya sikha, tah, sikha, but these sloop, train yourselves in the precepts

76
00:11:27,840 --> 00:11:40,000
having accepted them. So the first thing he says is be ethical, be aware of the proper conduct,

77
00:11:40,560 --> 00:11:49,040
behavior, speech of a monk, I mean he's talking about monk, how is it proper, what is proper

78
00:11:49,040 --> 00:11:56,960
behavior for a monk? Understand that, because for meditators as well, when you come to do a meditation

79
00:11:56,960 --> 00:12:02,640
course, you have to know it is proper etiquette in the monastery, in the meditation center, and just

80
00:12:02,640 --> 00:12:10,720
in general, as we're at a much higher standard as Buddhists. Buddhism isn't a practice for

81
00:12:12,080 --> 00:12:26,000
mediocrity. Buddhists are and should be held to a higher standard, just because of calling themselves

82
00:12:26,000 --> 00:12:33,040
Buddhists. Once you call yourself Buddhist or you follow the Buddhist way, it has to be held to

83
00:12:33,040 --> 00:12:38,080
a higher standard, because that's all it is. That's what being a Buddhist means. It's not about faith

84
00:12:38,080 --> 00:12:48,880
or belief or names or anything. You're a Buddhist if you cultivate higher things.

85
00:12:50,400 --> 00:12:53,520
That's the first one. The second one is the one we have in the court.

86
00:12:53,520 --> 00:13:03,600
Atatumhiya was a come-of-friend in Rheesu-gutadwara, we had at that, dwell as one who

87
00:13:05,120 --> 00:13:17,120
has guarded the doors of the senses. Gutadwara with the doors guarded in Rheesu of the senses.

88
00:13:17,120 --> 00:13:26,480
So the senses are doors, guard them. This is how mindfulness works. The Buddha talks often about

89
00:13:26,480 --> 00:13:33,040
this. It works as a guard. Like if you have a gate to a city, you only let in people who are

90
00:13:33,040 --> 00:13:38,720
trustworthy. In the same way, it's like a filter. So in the same way mindfulness stands at the

91
00:13:38,720 --> 00:13:47,120
door of the senses, and only let's in this seeing, the hearing, the smelling, doesn't let in the

92
00:13:47,120 --> 00:13:54,800
defilements, doesn't let their defilements rise, doesn't let in any of their baggage. All that

93
00:13:54,800 --> 00:14:01,120
it lets come to come to the consciousness is the experience. So it creates objectivity.

94
00:14:01,120 --> 00:14:13,920
Arakasatino with guard, the guard of mindfulness. Nipakasatino. Nipakasan, interesting word.

95
00:14:13,920 --> 00:14:25,040
I don't really know it. Nipaka means infused, boiled. I'm not quite sure about that. Oh,

96
00:14:25,040 --> 00:14:37,440
there's a variant. Nipaka. Let's look at Nipaka. It's called Pauli lesson tonight.

97
00:14:39,280 --> 00:14:52,000
Prudence, careful mindfulness. Nipaka is strange one. Nipaka is from Nipaka means carefulness.

98
00:14:52,000 --> 00:14:58,480
Careful mindfulness. One of the mindfulness and caution.

99
00:15:03,280 --> 00:15:11,600
So this idea that mindfulness works or functions through caution. Your

100
00:15:11,600 --> 00:15:17,200
caution is mindfulness. Mindfulness is caution means approaching things carefully,

101
00:15:17,200 --> 00:15:38,720
meticulously. And then he launches into this, well, that's really part of the quote,

102
00:15:38,720 --> 00:15:55,760
sara kita manasasatara kena jita sasamunagata. V. One who has, who is accomplished or has attained

103
00:15:55,760 --> 00:16:09,120
a mind guarded by mindfulness. With a guard of mind, become one who has a mind guarded with mindfulness.

104
00:16:09,120 --> 00:16:13,520
It's a bit awkward in English. It's just the way they speak in Pauli. And it's the way the Buddha

105
00:16:13,520 --> 00:16:20,080
would speak. You speak, it's a retro rhetorical tool of repeating yourself in different ways.

106
00:16:20,080 --> 00:16:27,920
So sara kita manasa means be one, if I'm reading this correctly, be one who has a mind

107
00:16:29,520 --> 00:16:42,480
that is well guarded. Satara kena jita sasamunagata be one who is who goes with or who has

108
00:16:42,480 --> 00:16:50,560
a mind guarded by mindfulness. I think sattara kena sattara kena jita.

109
00:16:56,000 --> 00:17:00,240
So they should be established in guarding the senses. So the first one is established in

110
00:17:00,240 --> 00:17:04,000
guarding the patimoka. The second one is guarding the faculties.

111
00:17:04,000 --> 00:17:15,840
And then the third one, eta tome hai also apabasa hoda. Be one who talks a little apabasa,

112
00:17:16,560 --> 00:17:30,080
one who talks only a little. Bhasay paryantakarino, one who makes an end to speech or knows the end

113
00:17:30,080 --> 00:17:39,360
to speech maybe, baryantakarino restricted, who has limited speech. There you go. The Buddha actually

114
00:17:39,360 --> 00:17:48,400
does say bhasay paryantakarino, one who limits one speech, puts a boundary on one's speech.

115
00:17:48,400 --> 00:18:00,240
So bhasay paryantakarino, one who limits one speech, should be limited to the dhamma to good things.

116
00:18:02,560 --> 00:18:10,000
eta tome hai, number four eta tome hai also aran nika huna be one who dwells in the forest, aran

117
00:18:10,000 --> 00:18:23,440
nya, one apatani, one dhaani say nasana, but they say wata, but they say wata means use or indulgent

118
00:18:23,440 --> 00:18:36,000
in, makes you serve or makes you serve dwellings that are secluded in the forest, aran nika be

119
00:18:36,000 --> 00:18:57,840
one who dwells in the forest. In general kaya wo bhasay seclusion wo bhasa be one so you should

120
00:18:57,840 --> 00:19:04,000
exhort them in bodily seclusion, seclusion of the body because there's two kinds of seclusion,

121
00:19:04,000 --> 00:19:08,880
there's seclusion of the mind and there's seclusion of the body. It's a question of the mind is

122
00:19:08,880 --> 00:19:14,080
most important. That's through meditation practice. That seclusion of the body is important for

123
00:19:14,080 --> 00:19:18,320
the practice. That's why people come here. I mean just put them in their room and leave them for

124
00:19:19,680 --> 00:19:26,240
all day on their own. There's the ideas to be secluded. It's important from a physical perspective as

125
00:19:26,240 --> 00:19:34,080
well. Just include yourself. It's like in a lab, not so that you can run away from your problems,

126
00:19:34,080 --> 00:19:40,320
but so that you can get into lab mode and learn about yourself, study yourself.

127
00:19:43,120 --> 00:19:52,960
Number four, number five, eta tumayau so samadhika, hoda be one who has right views.

128
00:19:52,960 --> 00:20:02,640
samadha sannin, one who sees rightly. samadha gata. When it goes, there's a accomplished in

129
00:20:03,360 --> 00:20:06,400
right vision and right view.

130
00:20:17,520 --> 00:20:20,480
In right view I've talked to, I actually talked about it last Saturday.

131
00:20:20,480 --> 00:20:26,160
Right view, there are five types of right view, actually. I didn't mention this last time. We talked

132
00:20:26,160 --> 00:20:30,720
about it a bit differently. There's the right view of karma, the responsibility of karma.

133
00:20:31,440 --> 00:20:40,800
There's right view of Jana, right view in regards to Jana, or regards to tranquility meditation.

134
00:20:42,640 --> 00:20:47,040
We passed on as samadha, did you write view of insight? Right view about the path, right view

135
00:20:47,040 --> 00:20:52,640
about the fruition. It's not so keen on that distinction of right view, but

136
00:20:53,520 --> 00:20:57,600
right view means to see clearly. It's really the core of teravada Buddhism,

137
00:20:59,120 --> 00:21:05,840
wisdom, to cultivate understanding, understand things as they are.

138
00:21:07,200 --> 00:21:14,480
Anyway, so that's the dhamma for tonight. The quote was specifically about one of the five,

139
00:21:14,480 --> 00:21:23,520
but all together it puts together sort of a nice guide for training new monks, new meditators.

140
00:21:24,400 --> 00:21:29,840
They should be mindful, but they should also be ethical first, and then mindful,

141
00:21:31,040 --> 00:21:42,240
and they should speak only a little. They should be secluded physically,

142
00:21:42,240 --> 00:21:51,200
and they should be instructed and right view. They should be encouraged to cultivate right

143
00:21:51,200 --> 00:21:56,320
view. So focused on that, really that's the focus of our meditation. The meditation isn't just

144
00:21:56,320 --> 00:22:02,160
to calm down, it isn't just to relax. Focused on trying to see clearly. It's an important

145
00:22:03,920 --> 00:22:07,200
aim for us to focus on it, our meditation.

146
00:22:07,200 --> 00:22:12,000
Okay, so that's the dhamma for tonight.

147
00:22:16,960 --> 00:22:25,360
I'll post the hangout. Come on if you want to ask a question. We just want to say hi, you can

148
00:22:25,360 --> 00:22:45,840
come on, nice and pose. Pile on, I don't mind. But if you have a question, please come on,

149
00:22:45,840 --> 00:22:55,360
you're welcome to join. We get consistently 25-ish viewers on YouTube. It's just good.

150
00:22:56,720 --> 00:23:02,240
It means we've got a small community here. Hello, Bounseh. Hey Simon.

151
00:23:02,240 --> 00:23:26,720
Oh wait a minute. My sound is not good. Let me just- There are no questions.

152
00:23:26,720 --> 00:23:30,640
I'll just wish y'all a good night.

153
00:23:41,360 --> 00:23:46,880
Hello, Bounseh. Hi. Do you have a question? No, I just want to say hello.

154
00:23:50,480 --> 00:23:53,120
Thank you for the dhamma talk today. That was a wonderful quote.

155
00:23:53,120 --> 00:24:00,000
Yeah. Which one did you think was wonderful, the quote or the whole passage?

156
00:24:01,520 --> 00:24:06,960
I mean, the whole passage really, but the quote really struck me as well, just the two lines there.

157
00:24:08,640 --> 00:24:13,360
I wasn't all that struck by it. I found it kind of confusing. Oh, how was that?

158
00:24:14,080 --> 00:24:18,640
Well, watchfully mindful, carefully mindful with the ways of the mind well watched.

159
00:24:18,640 --> 00:24:23,840
Maybe not confusing, but I just knew right away it wasn't quite what was being said.

160
00:24:28,720 --> 00:24:35,840
Because it starts out being watchfully mindful and then watching the ways of the mind,

161
00:24:37,840 --> 00:24:43,920
with the ways of the mind well watched, possessed of a mind. So it's saying two different things.

162
00:24:43,920 --> 00:24:47,760
First, you're watching the mind and then you've got the mind that's watching.

163
00:24:49,680 --> 00:24:55,280
But it's not really wrong. That's why I went to the poly because I wanted to see what's

164
00:24:55,280 --> 00:25:02,800
really being said here. I mean, it's important to frame it in regards to the senses, which he does,

165
00:25:02,800 --> 00:25:10,480
the quote does say, guarding the senses. But it's Arrakasatino Nipakasat, which is just this

166
00:25:10,480 --> 00:25:17,440
rhetoric rhetorical tool repeating the same thing in different words with mindfulness as a guard,

167
00:25:17,440 --> 00:25:27,120
with mindfulness in careful mindfulness, with guarding mindfulness.

168
00:25:29,920 --> 00:25:36,800
With the mind well guarded and then with the mind that is guarded by mindfulness.

169
00:25:36,800 --> 00:25:44,320
So I'm not really sure about that translation as usual.

170
00:25:45,120 --> 00:25:48,800
Well, that makes sense. Yeah, so actually the thing that really struck me was like

171
00:25:49,520 --> 00:25:54,000
just the first line there, come live with the doors of the senses guarded.

172
00:25:54,880 --> 00:25:59,680
As soon as I read that, it was like, that's so nice, cool. But again, definitely understand

173
00:25:59,680 --> 00:26:05,680
what you meant there. So you see watchfully mindful isn't correct. I mean, it's nice.

174
00:26:05,680 --> 00:26:10,080
It's that that part I really like actually, but it's not watchfully mindful. Watchfully,

175
00:26:10,080 --> 00:26:16,080
Arrakasatino, it's not watchfully, it's a guard, Arrakasatino's a guard or two,

176
00:26:17,520 --> 00:26:25,040
it may be to to care for you could say, but maybe maybe he's doing okay. I mean, it's all fine.

177
00:26:25,040 --> 00:26:29,040
It's he's certainly got license. There's nothing wrong with changing around.

178
00:26:30,160 --> 00:26:34,800
It's just like going to the poly to find exact things and that's why it's nice to learn the

179
00:26:34,800 --> 00:26:38,800
the original because you get a taste of what the Buddha was actually saying.

180
00:26:38,800 --> 00:26:44,800
Yeah. There's no replacement for the original.

181
00:26:44,800 --> 00:26:48,800
That's true. That's always so wonderful to get the pali.

182
00:26:48,800 --> 00:26:54,800
Yeah, I just got thrown off by the second half of the quote.

183
00:26:54,800 --> 00:27:00,800
I was a translation because it's a bit wonky.

184
00:27:00,800 --> 00:27:04,800
I see.

185
00:27:04,800 --> 00:27:06,800
Well, thanks for making their career as well.

186
00:27:06,800 --> 00:27:10,800
And five points of pride you as well.

187
00:27:10,800 --> 00:27:14,800
For adding them in.

188
00:27:14,800 --> 00:27:15,800
Okay.

189
00:27:15,800 --> 00:27:18,800
Good night, everyone. Good night, Simon.

190
00:27:18,800 --> 00:27:28,800
Good night. Thank you. Bye bye.

