Good evening, everyone broadcasting live July 3, 2016 as usual.
Today's quote, this quote is about, would he, would he mean growth or progress and expansion
increase, would he, would he want, would he, would one have means to get to grow to, to grow,
to increase?
So we're talking about the area one day, area means noble, so the noble, the betterment,
noble growth I guess, it just sounds weird because we use the word growth to mean something
else, but to grow to, to progress, spiritual, spiritual growth.
And curiously, it's directed towards a woman, no, I'm not sure what that's all about.
It doesn't use the word woman, but it uses the feminine forms of the word sawika, sawika
means, means a female disciple, so you think these might be a little gender stereotyped.
Well, I don't see that uses the word opacity as well, so it is, it is directed towards women.
So maybe he was talking to a bunch of women, could have been, he uses the word bikave,
which is masculine, so he's talking to, but that doesn't really mean anything, no, suppose
it does, and be a stretch to say humans talking to women here, probably talking to the monks,
talking about women, which is, oh, yeah, because this is the mantu gama, which is a
discursive about mothers, about house mothers, which means household women, of course
is, I guess, a favorite, subject of monks, probably not, there are things to be said, how
to relate, how, how male, celibate, monks, heterosexual, celibate, males, should relate to the
women that they have to come in contact with, because for some time there is only men,
there is only male monks, then he read, as I said, I don't think this is terribly gender
specific, these five things, sadha, sadha, sanda, sila, supta, jagapanya, I'm a pretty
standard, so we can treat this just as five, five dumb ones that we should all cultivate,
we cultivate these, it leads to our spiritual well-being, our spiritual growth, increase
in all things spiritual, allow us to grasp the, grasp the sara, the sara, daya, sara daya,
and grasp the essence to grasp the highest, that which is special, excellent, so sadha
means we try to have confidence, we cultivate confidence not only in ourselves, but in the
practice, not only in the practice, I suppose, but also in the Buddha, mainly in the practice
if you want to streamline it, the most important confidence you have is in the path that
you're following, of course it helps to think that the Buddha knew what he was teaching,
but you don't have to quite go that, you don't have to necessarily go that abstract,
you can take the practice for what it is as long as you are clear about the benefits
of it, and our focus on the goodness of it, is why I tell people not to, not to tell
students, not to worry about success or progress, am I doing better than I was yesterday
and is this meditation helping me as well, don't look at it in those terms, look at
it as it happens, when you're meditating, when you have a clear mind, look at the quality
of that mind, and remind yourself of how pure the mind is when you're objective, and
how it solves the situation, it resolves the problems that arise, the challenges that
come, the conflicts that arise in the mind, it resolves them and see how it does that,
and then you don't have to worry about what the results are going to mean, because clearly
that's a positive change in your life, and building up that habit is, of course, going
to bring good things, there's no question that it might bring bad things, and so you just
be patient, let the good things come rather than checking and seeing how they come in,
and they come in and they haven't come giving rise to irrational doubt, I think it's irrational
because if you look at what you're doing, you're doing something positive, if you want real
confidence, focus on what you're doing, are you doing something positive and don't worry
about the outcome, you should be very confident if you're doing something positive,
you're doing something that's purely good for you and good for others? Seela, we need to have
ethics and we want to reach the essence, we want to get to the core of the truth of the
nature of reality, we need some kind of ethical guidelines to keep you sober, to keep
you sane, to keep your mind settled and balanced so that you're not overcome by negative
emotions, of inventions and fear and mistrust, distrust, let me feel that, let me show
you and kill, steal, lie, cheat, take drugs or alcohol, we shouldn't gamble, we shouldn't
engage in unethical practices, unethical business practices, etc. It will help our minds
focus if we avoid these things, we feel more confident in ourselves. Number three,
sootah, we have to learn, sootah means listening, that which is heard, which should be
full of that, which is heard, so knowledge, it's back then they would all be password
fast, word of mouth, teachings would be passed on from teacher to disciple, so you have
to listen, listen carefully, doesn't mean just sitting down and keeping your ears open,
it means inclining your ear, in a sense of being interested and focusing your attention,
you're not like listening to me and checking Facebook or playing, whatever it is people
play, farmbell, you should be focused, and then not only should you be focused, but you
should keep it in mind, what you've learned, but I talked about someone who carries the
dumb on their lap, and that means like if a person carries something like their soup on
their lap, and if they forget about it and they stand up, it flies all over the place,
spills all over, the person who carries the dumb on their lap is someone who when they're
sitting listening, they keep it in mind, but as soon as they get up, they forget about it,
so they don't take it into their lives, they don't take it seriously, it doesn't become
a part of who they are, so soup doesn't just mean listening, it means the whole spectrum
of attention and retention of knowledge and wisdom. Number four is Jaga. Jaga is, this
is a specific, fairly specific layperson quality, so it's not just for women, it's for men
as well, but Jaga means generosity because, I mean it's not that monks aren't generous
and don't be generous, but it's more visibly and obviously a quality of labor because
they support them, the monks, and they keep them alive with food and monks named in the
robes or medicine, they provide them, but I guess more importantly, they have material
wealth, so to be generous with that, not just with other Buddhists, but with poor people,
with their family, with relatives, even with strangers, you know hospitality to people
who come to visit. Many cultures, when you enter their house, you immediately offer you
something, food or drink, find something, to share with you. They're very least asking
what they need, is there anything I can get for you? Someone comes into your house to
be generous, to be giving, monks can do it as well, we all can, but this is a great support
for our religious practice, because again it builds confidence, it builds happiness, in
a sense of not just a happy feeling, but feeling good about yourself, in a sense of giving
energy, not depressing you, making it hard for you to making you uninterested, unresolute,
not able to pull yourself together and move forward.
And number five wisdom, we want to find the truth, we need wisdom, banya, banya means
to nya means knowledge, but it means thoroughly, they're all complete, perfect, so real
knowledge, wisdom, banya means wisdom. Wisdom is different from learning,
true wisdom comes from seeing, doesn't come from speculating or extrapolating or rationalizing,
true wisdom comes from just seeing, clearing up, clearing your mind so perfectly that
you can see everything, calm your mind down through the meditation, not letting things
distract you or not reacting to things, and eventually seeing things as they are, and
seeing the things that we cling to or not worth clinging to, seeing the difference between
wholesome mind states and unwholesome mind states, seeing that nothing is worth clinging
to, and finally seeing nymana, so these five things are, that which leads to growth,
so that's what we're all concerned with here, right? We want progress, we want to better
ourselves, so why we practice meditation, we're not perfect, we are people who have problems,
who react, who have reactions, who have problems inside, who ourselves are not yet satisfactory,
we're not satisfied with our own state. Okay, so that's the demo for tonight, if you
have any questions, there was a question from earlier, someone asking, do you have a breathing
technique that helps you breathe better? Well, no, it's not about better, better as a judgment,
so again, we're trying not to judge, trying not to react. We're not concerned about breathing
better, we're concerned about how we react to the way we breathe naturally, whether
that be good or bad. If it's really causing your problems, then sometimes you can address
it, but mostly you just address your reactions to it, your mind flow that it feels not
nice, feels strange, feeling, feeling, but if you dislike it or you're frustrated by
it, so just liking, just liking. How much of the Buddha's teaching is confirmed as authentic
and not invented, well, it was all invented, the Buddha invented it. But if you mean invented
by someone other than the Buddha, well, it doesn't really matter, what matters is whether
it works, right? You practice it and you see that it's called holes in it, then you
can doubt as to whether a Buddha taught it. If you practice it and you see that it does
actually do what it says, and it is more or less cohesive than you can say, well, it's
probably taught by Buddha. How beneficial is it to participate in society and still hope
for spiritual progress? Well, it's not beneficial to participate in society except in terms
of duties, but you can still hope, you can still hope for spiritual progress. You don't
have to leave it, but of course dedicating your life to spiritual progress is of course
much better. So no, you don't have to become a monk against spiritual
man. It's just easier because you don't have to do things that are unrelated to spiritual
housing, you don't have to go to work, you don't have to deal with family, you don't
have to take care of a house, possessions and all those things.
Let's move on to questions. Can we try to do it down the pond at tomorrow or what some
day soon? I think the weekend is probably better for, I assume it was better for questions
because people were not working, but I'm not so many questions yet. Here's one.
What are the chances of enlightenment in the present day and age? Well, it's not a matter
of chance, it's up to you. Are you able to become enlightened? That's all. I mean, yeah,
you could probably do some statistic analysis if you could figure out who's enlightened,
not an easy task itself.
And enlightenment isn't just about, you know, it isn't just the thing where you flick
a switch, it just happens. So it's a gradual process. So you can start to practice. There's
really no idea whether you can become enlightened. It's not putting one foot in front of
the other and doing work.
I want to donate something to your establishment. Well, we do function through donations
so that's always appreciated, not expected, but if it doesn't come, we'll just have
to shut down. So with different things, you can donate. I mean, I have to stay alive to
eat. So I've gone back to going to restaurants that have been given money for me. So you
can go to my web blog. And there's a wish list. That's about as direct as giving to me.
But then we have this organization. And there's a support page for the organization. That's
on the main theory mongolow.org site.
If you go to serimongolow.org, you'll find probably 500 you need. Is there, is it right
to say that you're in control of decision making? One is born as an animal. It can't make
a decision like a human. There's no you to be in control of decision making. It's a hard
subject to talk about because reality isn't quite the way we think in terms of there being
a soul or a self. It's not exactly deterministic either. Reality is this. It's experience.
So in any conventional terms, yes, you are responsible for each choice to make. But it's
hard to be specific about it. Hard to be exact. Animals can still make decisions. They're
just much less aware. You know, most humans don't make decisions. Most humans just go
according to habit. It's hard to define someone with mindfulness. But even animals can
cultivate it. I think actually I'm not sure every demo might tell otherwise. It's a good
question because I know they talk about them being I hate to cause something. How can we deal
with judgmental thoughts? For example, noticing how attached to someone else's. Well,
you start by the disliking of it. Because if you're guilty, if you're mad about it, that's
also a problem that prevents you from seeing it clearly. Remember, meditation is not about
judging things. So even judging the bad things. But seeing them. And if you see them enough,
you'll start to change. So if you see yourself being judgmental again and again, and you
really start to see how stressful that is and how unpleasant that is, it'll start to give
it out. How useless it is, I guess, most important. And we shouldn't keep ourselves over
the fact that we still have defilements. We should learn about them, study them. Well,
lots of questions. What are the benefits of Dutanga aesthetic practices? Well, they push you,
they challenge you. It's like if you have someone who's a sport who plays a sport, they might
do challenges, you know, or someone that's a BMX cross-country biking or something that
challenges themselves to, or when I was doing rock climbing, we would challenge ourselves
to harder and harder climbs. So it's like a challenge. It pushes you. It's like that. You don't
have to do it. You can do ordinary biking, but you want to do tricks on your bike. Because
it challenges. No, and then there's speaking of bicycling in the woods, bicycling in the woods,
can it be a meditation? Nothing is meditation. Meditation is not what you're doing. Walking isn't
meditation unless you're being meditative. So biking in the woods is not meditation unless you're
being meditative. And the question is, does it mean by being meditative? And of course, there are
many different answers to that. In our tradition, we have a very specific definition of the word
meditation, not that we don't accept other things are meditation. But when we talk about meditation,
we mean something fairly specific. So if you're doing that when you're biking, then yes, you're
meditative. If not, no. My real question, how does one put into tongue of practices into effect
in today's society, or an unceation, the willingness is not looked highly upon? Well, if we were all
concerned about what was looked highly upon, we wouldn't ever get anything spiritual done.
We will all be capitalists and materialists. Just put them into effect who cares what people
think. If you're worried about the century of others, then maybe you've got a
time to look for new friends and you're associated with it. Maybe that's being a little bit facetious
but a condescending, I don't know what the word is. Get to a position where you can, but don't worry
so much about the two tongueers. If you're, I mean yes, they're mostly for people who are living
in forests. So if you're worried about, I can't practice the dutanga. They're not, they're not
essential. I mean, I was going to stop you from meeting one meal a day. I was going to stop you
from not wearing extra robes. Society might, I only wear one set of robes, so that's a dutanga,
isn't it? I don't have a change. But it gets problematic. You just have to remember to wash your
robes often, as soon as they start to smell. How does becoming a monk affect one's connection
with family members? It's pretty profound, it affects, as you can probably surmise, it affects
your many aspects of your life fairly profoundly. So you're no longer a part of the family unit
from your own perspective. Now your family might see it otherwise, so there are, there are
allowances made for family members. There's quite a few allowances made that make it more
comfortable, but there's a change in the dynamic because you're no longer able to interact
in the same way. So it takes some adjustment, but eventually it seems to be a good thing. I mean,
my family, I think, it's been a great thing for me to become a monk. It's enriched their lives
just as many different things do, but I think they would say it enriches their lives to have
another exotic piece of the puzzle, you know, to have me as part of their family. And Buddhist
monk is, it's not the best thing about their family, but it's one kind of like a jewel that they
have. I think most of my family feels that way now that it's something pretty neat, that they
have a monk who is teaching meditation, who seems to be doing good things for people, helping the
world, you know, maybe makes them feel good to know that they're, I think. I don't suppose all my
family members feel like that all the time, but in general, it seemed, I mean, it's always thought
it was a great thing. Obviously, I think it is becoming a monk is great for your family in the long
run. It just causes some tension as change does because people have a hard time dealing with change.
Animals I care for is they're bad karma and not letting them be wild. Bad karma is not in such
general things, bad karma is in moments. So at the moments when you give rise to greed and
anger or delusion, there's bad karma. The moments you give rise to non-greed, non-engured, non-delution,
as in wisdom, love and pronunciation, then there is good karma. The problem with animals is it's
very easy to give rise to bad karma in regards to it. So either in terms of our dependence on them,
or in terms of our anger and frustration when they don't do what we want, when we have to
discipline them that kind of thing. It's just like small children. I wouldn't ever want to have
children because I wouldn't ever want to have students who didn't ask me to be their teacher.
Loving children to me seems horrific. Having kids, you don't know, you don't know whether they're
going to listen to or not. They have no reason to. Well, besides self-preservation,
that's maybe not so bad because kids do tend to listen to your parents if you're good parents,
but not certainly. And the kids can be terrible.
Again, so as regards to not letting them be wild, you have to see, does it necessitate
states? And probably it does because animals are fairly unmanageable, except if you are
unwholesome towards them, like hitting them, scolding them, punishing them that can do.
And then there's also the coddling room, which is certainly unwholesome when you coddle someone,
coddling them. Make sure it always has pleasure so it doesn't ever get angry or frustrated
or depressed, diverting its attention with pleasure, not really wholesome.
Meditation on the body.
Would it be appropriate to look into the mirror when noting hair or nose nose? Don't say nose
nose, but hair or hair, yes, nose isn't one of them. Well, you start by looking at it. That's a
great way to start looking at the mirror to start. But a good thing to do if you're really interested
is read the Wisudimanga. This is one part, one meditation where reading the Wisudimanga
would be a great help because there's some really in-depth descriptions of the 32 parts of the body
that go beyond with anyone that I would have ever thought of to describe these things. It's so
very clear, you know, excruciatingly clear and precise about the description of these things
that really helps you keep them in mind. So this is a manual that's been designed to
facilitate this. Of course, a mirror can help. But the mirror is, it's still said we didn't
be inferior to the Wisudimanga description. But the idea is eventually then you don't need
the mirror. You wouldn't need it. You wouldn't go and close your eyes.
We began looking into the forest tradition, concerned that this desire is based on attachment
for natural environment. It's a great aversion for people. I already have an issue with feeling
inversion towards groups of people, so perhaps they'd be compounding those.
See, the thing about the Thai forest tradition is that that's just their name for it.
And it's somewhat objectionable because there are lots of forest monks in Thailand that have
nothing to do with that group. They just got famous. And rightly so, you know, they are a good
group of monks. But it's somewhat objectionable that they should call themselves the forest tradition.
I've lived in the forest. You know, my teacher spent a lot of time in the forest.
You have to understand where that word came from. There was one monk who, a gen man,
and his sort of other monks who were strict about staying in the forest. And so they started
calling them what bah, what bah. And their their monasteries would be called what bah, this, what
bah, that bah means forest. And because their monasteries all took on the name what bah,
what bah, they called it sai what bah. And then today they still call it sai what bah. But it's
just a short form of this. The sai means the tradition or the need, but what bah means the forest
monasteries. But it was referring to specific forest monasteries, a specific group before it's
monasteries. And so it's come to be a little bit deceptive in that sense. So if you ask, you're
actually asking two questions or you have to separate those that question out because
there's nothing to do with the time forest tradition. That's true that your question doesn't
exactly. But let's just focus on the idea of living in the forest. It's living in the forest
greater version potentially. But I think the benefits of living in the forest far outweigh that
because any aversion that you have towards something can be worked out through introspection now
living in the forest helps with introspection. So in the beginning it might be just an indulgence
by attachment because of attachment to nature. But over the long term, living that the benefits
will come and they'll wash away all that. It's true living in the forest long term. You gain a lot,
you gain a lot of strength of mind and wisdom. And that wisdom is very applicable in society among
groups of people. I wouldn't say that person who's lived in the forest for a long time is a verse
too. They may be disinclined, but they'll be much less likely to give rise to aversion because
they've been able to work it out. I mean, not just by living in the forest, but by doing the things
that are meant to be done in the forest. Practice of insight meditation, which is much better done
in the forest, I would say. Not necessary to be in the forest, but there are benefits.
I think that's enough for tonight, lots of questions. That's another question. Okay, I'm going to cut
it off there. Thank you all for coming tonight. We'll see you out tomorrow.
