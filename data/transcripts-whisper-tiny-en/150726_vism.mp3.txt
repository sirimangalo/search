Yeah, we're ready to start, so good afternoon everyone, we're ready to go, we see the manga,
Robin you want to take it.
Sure, we're on chapter 6, page 169, foundness as a meditation subject, and Aurora, can you start us off?
Now, ten kinds of foundness is corpses without consciousness, we're listed next after the casinos thus.
The bloated, the vivid, the festering, the cut up, the nod, the scattered, the hacked and scattered, the bleeding, the warm and pasted, a skeleton.
The bloated, it is bloated because bloated by gradual dilation and swelling after the close of life as a bellows is with wind.
What is bloated is the same as the bloated, or alternatively, what is bloated is vile because of repulsiveness, but it is the bloated.
This is a term for corpse in that particular state.
The livid, what has patchy discoloration is called livid, what is livid is the same as the livid.
Or alternatively, what is livid is vile because of repulsiveness, thus it is the livid.
This is a term for a corpse that is reddish colored in places where flesh is prominent, whitish colored in places where pus has collected, but mostly blue black as if draped with
blue black cloth in the blue black places.
The festering, what is trickling with pus in broken places is festering.
What is festering is the same as the festering, or alternatively, what is festering is vile because of repulsiveness, thus it is the festering.
This is the term for corpse in that particular state.
So can you be the next one?
This is the term for a corpse cut in the middle.
No cannot, what has been chewed here and there in various way by dogs, jackals, etc., is what is not.
What is not is the same as the gnote or alternatively, what is not is vile because of repulsiveness, thus it is the gnote.
This is a term for a corpse in that particular state.
The scattered, what is strewn about is scattered, what is scattered is the same as the scattered.
Or alternatively, what is scattered is vile because of repulsiveness that it is the scattered.
What is trying to do here is explain the cut on the end, which isn't really all that necessary.
This is just a derivation, but I guess the point is we keep the, and so on, can stand on its own as an adjective.
So the question is why do you have to add cut on the ending?
When we keep the cut, it could be considered the noun, we keep it as an adjective meaning scattered in this example.
This is a result of the nyka, nyka suffix, which becomes cut.
I am not quite sure about that, but anyway, it comes to be a cut on the ending.
It doesn't really have any to do with good chita.
I think that is more of an instructive etymology than historical.
It probably isn't where it comes from, it's just adding cut on the end, it makes it that much is.
Anyway, this is a term for a corpse that is strewn here and there in this way.
Here, hand, there are foot, there, the head.
The half in scatter is hacked and it is scattered in the way just described, thus it is hacked and scattered.
This is a term for a corpse scattered in the way just described after it has been hacked with a knife in a crow's foot pattern on every limb.
The bleeding, its sprinkles, karate, scatters, blood, and it trickles here and there, thus it is the bleeding.
This is a term for a corpse smeared with trickling blood.
The worm invested, it is maggots that are called worms, its sprinkles worms, thus it is worm invested. This is the term for a corpse full of maggots.
A skeleton, bone, at the, is the same as skeleton, at tikka, or alternately bone, at t, is wild, kochita, because of reposiveness.
Thus it is a skeleton at tikka. This is a term for both, for a single bone and for a framework of bones.
These names are also used both for the signs that arise with the loaded etc as their support and for the Janas obtained in the signs.
Here in when a meditator wants to develop the Janas called Over the Bloated by arousing the sign of the bloated on a bloated body, he should in the way already described, approach a teacher of the kind mentioned under the earth casino, and learn the meditation, meditation subject from him.
In explaining the meditation subject to him, the teacher should explain it all, that is the directions for going with the aim of acquiring the sign of foulness, the characterizing of the surrounding signs, the 11 ways of apprehending the sign, the reviewing of the path gone by and come by, concluding with the directions for absorption.
And when the meditator has learned it all well, he should go to an abode of the kind already described and live there while seeking the sign of the bloated. So it's bypassing much, as I said it would, that was already described in the section on Patuikka senior, the rest are just going to be, in many ways they're similar, the development is similar.
Meanwhile, when he hears people saying that at some village gate or in some road, or at some forest edge, or at the base of some rock, or at the root of some tree, or in some carnal ground, a bloated corpse is lying, he should not go there at once, like one who plunges into a river, where there is no forward.
Why not? Because this foulness is beset by wild beasts and non-human beings, and he might risk his life there, or perhaps the way to go to it goes by a village gate or a bathing place or an irrigated field, and there are visible object of the opposite sex might come into focus, or perhaps the body is of the opposite sex for a female body is unsuitable for a man and a male body for a woman.
If only recently did, it may even look beautiful, hence there might be danger to the life of purity, but if he judges himself, this is not difficult for one like me, then he can go there.
And when he goes, and when he goes, he should do so only after he has spoken to the senior elder of the community, or to some well-known beaker.
At the channel ground, or if his goat rises when he is confronted with disagreeable objects, such as visible forms and sounds of non-human beings, lions, tigers, etc., or something else afflicts him.
Then he whom he told will have his bowl and drop well looked after in the monastery, or he will care for him by sending him because one of us has to him.
Besides, robbers may mention this thinking, a channel ground, a safe place for them, whether or not they have done anything wrong, and when men chase them, they drop their goods near the beaker and wrong away.
Perhaps the men sees the beaker saying, we have found the thief with the goods and only them, only him.
And he whom he told will explain to the men, do not bother him. He went to do this special work after telling me, and he will rescue him.
This is the advantage of going only after informing someone.
Therefore, he should inform a beaker of the kind described, and then set out eager to see the sign, and as happy and joyful as a warrior noble on his way to the scene of anointing, as one going to offer libations at the hall of sacrifice, or as a popper on his way to unearth the hidden treasure.
And he should go there in the way advised by the commentaries.
For this is said, one who is learning the bloated sign of foulness goes along with no companion, with unremitting mindfulness established, with his sense faculties turned inwards, with his mind not turned outwards, reviewing the path gone by and come by.
And the place where the bloated sign of foulness has been left, he notes any stone or termite mound or tree or bush or creeper, there each with its particular sign and in relation to the object.
When he has done this, he characterizes the bloated sign of foulness by the fact of its having attained that particular individual essence.
Then he sees that the sign is properly apprehended, and that it is properly remembered, that it is properly defined by its color, by its mark, by its shape, by its direction, by its location, by its delimitation, by its joints, by its openings, by its concavities, by its convexities, and all around.
When he has properly apprehended the sign, properly remembered it, properly defined it, he goes alone with no companion, with unremitting mindfulness established with his sense faculties turned inwards, with his mind not turned outwards, reviewing the path gone by and come by.
When he walks, he resolves, that his walk is oriented towards it, when he sits, he prepares a seat that is oriented towards it.
What is the purpose? What is the advantage of characterizing the surrounding signs?
Characterizing the surrounding signs has known the lotion for his purpose. It has known the lotion for his advantage.
What is the purpose? What is the advantage of apprehending the sign in the other 11 ways?
Apprehending the sign in the other 11 ways has anchoring the mind for his purpose.
It has anchoring the mind for his advantage. What is the purpose? What is the advantage of reviewing the path gone by and come by?
Reviewing the path gone by and come by has keeping the mind on the track for his purpose. It has keeping the mind on track for his advantage.
When he has established reverence for it by seeing its advantages and by perceiving it as a treasure, and so come to love it, he anchors his mind upon that object.
Surely in this way I shall be liberated from aging and death.
Quite secluded from sense desires, secluded from unprofitable things, he enters upon and dwells in the first jhana, seclusion.
He has arrived in the first jhana at the fine material sphere. His is a heavenly abiding and an instance of the meritorious action consisting and meditative development.
So if he goes to the jhana ground to test his control of mind, let him do so or just strike in the gong or summoning a chapter. If he goes there mainly for developing that meditation or meditation subject, let him go learn with no companion without renouncing his basic meditation subject and keeping it always in mind, taking a walking stick or a staff to keep off attacks by dogs, etc.
Ensuring, unremitting mindfulness by establishing it well, which is mine not turned out towards because he has ensured that his faculties of which his mind is the sixth are turned in words.
As he goes out of the monastery, he should not negate. I have gone out in such a direction by such a gate. After that he should define a path by which he goes.
This path goes in an easter really direction, westerly, north, northern, gnarly, south,rly, direction, or it goes in an intermediate direction.
And in this place it goes to the left, in this place to the right, and in this place there is a stone, in this term, termite mount.
In this tree, in this bush, in this scraper, he should go to the place where the sign is defined, making this way path by which he goes.
The point here is to keep mindfulness and to set up a continuous mindfulness. It's kind of an easing into, so that by the time a meditator arrives at the journal ground, they are able to be objective because the idea is, in some ways, this is going to be a difficult subject.
But this is sort of a common idea throughout the meditation teachings of continuity, the importance of continuity, so not ever giving up one's mindfulness, even when not doing formal practice.
And he should not approach it upwind, for if he did so, and the smell of corpse is a sale, a sale of his nose, his brain might get upset, or he might throw up his food, or he might repent his coming thinking, what a place of corpses I have come to.
So instead of approaching it upwind, he should go downwind. If he cannot go by a downwind path, if there is a mountain or a ravine or a rock or a fence or a patch of thorns, or a water or a bog in this way, then he should go stopping his nose with the corner of his robe. These are the duties in going.
When he has gone there in this way, he should not at once look at the sign of foulness. He should make sure of the direction.
For perhaps if he stands in a certain direction, the object does not appear clearly to him, and his mind is not willy.
So rather than there, he should stand where the object appears clearly, and his mind is willy. And he should avoid standing to leeward or to winward of it. For if he stands to leeward, he is bothered by the corpse smell and his mind's trays.
And if he stands to winward and non-human beings are dwelling there, they may get annoyed and do him a mischief. So he should move around a little and not stand too much to winward.
Then he should stand not too far off or to near, or too much towards the feet or the head. If he stands too far off, the object is not clear to him, and if he stands to near, he may get frightened. If he stands too much toward the feet or the head, not all the foulness becomes manifest to her equally.
So she should not stand too far off or to near, opposite the middle of the body, and a place convenient for her to look at.
Then he should characterize the surrounding signs in the way it stated to us. In the place where the bloating sign of foulness has been left, he notes any stones or creeper there in its sign.
In this way, this rock is high or low, small or large, brown or black, or white, long or round, after which he should observe the relative positions thus.
In this place, this is a rock, this is the sign of foulness, this is the sign of foulness, this is a rock.
If there is a termite mound, he should define it in this way, this is high or low, small or large, brown or black, or white, long or round, after which he should observe the relative positions thus.
In this place, there is a termite mound, this is the sign of foulness.
If there is a tree, he should define it in this way, this is a pipe, thick tree, or a banyan thick tree, or a chaga thick tree, or a teeta thick tree.
It is tall or short, small or large, black or white, after which he should observe the relative positions thus.
In this place, this is a tree, this is the sign of foulness.
If there is a bush, he should define it in this way, this is a cindy bush, or a caramanda bush, or a caramira bush, or a caranda bush.
It is tall or short, small or large, after which he should observe the relative positions thus.
In this place, there is a bush, this is the sign of foulness.
If there is a creeper, he should define it in this way, this is a pumpkin creeper, or a gourd keeper, creeper, or a brown creeper, or a black creeper, or a stinking creeper, after which he should observe the relative positions thus.
In this place, this is a creeper, this is the sign of foulness, this is the sign of foulness, this is a creeper.
The point here is to separate them. He is not saying that the creeper is foul, etc. He is saying, here is this corpse, and here is this other thing.
A lot of this is incredibly, incredibly, pedantic, and in my everyday, overly rigid, but the point is to not leave anything to chance, and to make it totally scientific.
It is like setting up a double-blind study, or a sterile lab, or something like that.
I mean, this isn't nearly, you could go a lot worse than this, but this is taking extra precautions to be completely short.
We should appreciate it. I think we shouldn't discard this sort of preparation as being unnecessary.
We often are quick to just jump in and try something, which is really the lazy way. Setting it up like this is definitely this idea of setting things up in such a rich, structured fashion is definitely not to be underestimated.
I think actually if I was approaching a dead body, I'd be just taking my time and observing everything and trying to buy a little time too.
They are not the sign of foulness. They are other than the sign of foulness. The sign of foulness is referring to the corpse.
They don't identify as them, right? They don't identify as them as what they are as being separate to the actual sign of foulness, which is the body. So he's defining different parts to his surroundings. Here's the body. Here's a brush. Here's the body. Here's a tree.
I see. I can't thank you.
Whenever I see them, I kind of am affected by that. I don't know if I've ever had a chance to actually go to a carnal ground, but that kind of is like an opportunistic carnal ground, I suppose. Not quite this detailed though.
But it's actually, I think, quite different. That being affected by it is something you want to avoid. It's not actually, I don't think, considered beneficial to have that reaction. So you're trying to avoid that.
You're trying to gain.
You're trying to hear cultivate the objectivity in advance before you actually look at the body so that when you look at it, you're able to enter into actually the genre, the idea here, maybe not the genre, but we're trying to get to a very calm state. It might just be a good job.
Thank you.
You're not quite almost, but it's more, it would bring about agitation.
That's probably more akin to the effectiveness that I feel when I see just a random dead animal or something.
Yeah, being affected like that may not be whole, so I'm not sure.
Yeah, I suppose, I mean, any experience like that can be usually helpful.
Everything is perfect and dominant and lasting.
That actually is, is that it may instigate the reflection. That is, oh, yes, I too will die, but often it's just propulsion, which is, I think negative. I think I'm also
going to say, maybe it should be the same as Prince, what Prince he thought when he saw dead body.
They're seeing, it was a surprise to him that such a thing existed. He didn't realize that it was possible to get all sick of died. You know, think of many people that when they see a dead body, they just don't want to see it and they run away from it and they totally shut it down and actually becomes a cause for increased aversion to even thinking about death.
Well, many people to see a dead body, it just makes them hated with hate death even more, you know, be less prepared for death.
So this seems like it's really just really taking your time and just making sure you're ready for it when you when you're finally get there.
It's an argument for that being important that it's not always good. It's not it's often not good to see dead people or so on that if your mind's not in the right place, it can traumatize you and it can make you hate death even more and be less ready to accept and to let go at the moment of death.
I think there's an argument for that.
When I drive in a room and if I see animal body on the side, I usually say to myself, take refuge to Buddha, take refuge to Sangha, take refuge to Gama and wish them not to become animal.
Well, the next life it's kind of blessing to them, come myself down.
There you go.
I think that actually adds to the point of how important it is to calm yourself down and how much better it is to enter into the calm state.
But what we're talking about here is having approached it from that calm state where you can actually be objective because the point is that it's so shocking to us.
But the key is to use that power of the experience from an objective point of view without any of the reactivity to let it open you up and to let the power just pull push you forward towards at least a calm state.
One day, are these type of meditation subjects still done today still taught to monks and things?
Oh, yeah, absolutely.
This one in particular, there's lots of meditation on the dead bodies going on, especially in Sri Lanka.
Not that much, a little bit in Thailand, but a lot in Sri Lanka from what I hear.
Yeah, but I don't think it's easy to find a bloated body in Sri Lanka.
Right, not mostly just dead and cut up, but in the more.
Right.
I think we're on number 34.
Also, with its particular sign and in relation to the object was said.
But that is included by what has just been said for he characterizes it with its particular sign.
When he divines it again and again and he characterizes it in relation to the object.
When he defines it by combining it each time in pairs thus.
This is a rock.
This is the sign of foulness.
This is the sign of foulness.
This is a rock.
Having done this, again, he should bring to mind the fact that it has an individual essence.
Its own state of being bloated, which is not common to anything else.
Since it was said that he defines it by the fact of it having attained that particular individual essence.
The meaning is that it should be defined according to individual essence, according to its own nature, as the inflated, the bloated.
Having defined it in this way, he should apprehend the sign in the following six ways.
That is to say by its color, by its mark, by its shape, by its direction, by its location, by its delineation.
How?
The meditator should define it by its color thus.
This is the body of one who is black or white or yellow skinned.
Instead of defining it by female mark or the male mark, he should define it by its mark thus.
This is the body of one who was in the first phase of life, in the middle phase of, in the middle phase, in the last phase.
By its shape, he should define it only by the shape of the bloated thus.
This is the shape of its head, this is the shape of its neck, this is the shape of its pen, this is the shape of its chest.
This is the shape of its belly, this is the shape of its novel.
This is the shape of its hips, this is the shape of its thigh, this is the shape of its cough, this is the shape of its foot.
He should define it by its direction thus.
There are two directions in this body that is down from the navel as the lower direction, and up from it as the upper direction.
For alternatively, he can define a thus.
I am standing in this direction.
The sign of foulness is in that direction.
He should define it by its location thus.
The hand is in this location, the foot in this, the head in this, the middle of the body in this.
Or alternatively, he can define a thus.
I am in this location.
The sign of foulness is in that.
He should define it by its delimitation thus.
This body is delimited below by the soles of the feet, above by the tips of the hair, all around by the skin.
The space so delimited is filled up with 32 pieces of corpse.
Or alternatively, he can define a thus.
This is the delimitation of its hand.
This is the delimitation of its foot.
This is the delimitation of its head.
This is the delimitation of the middle part of its body.
Or alternatively, he can delimit as much of it as he has apprehended thus.
Just this much of the bloated is like this.
Okay, I am going to suggest that we stop there where we started wailing, but some people probably have to leave, so I don't think we should go on too much longer.
We almost got half done there.
Hopefully next time we'll finish it off.
So let's take a break.
Five minutes or ten minutes and we'll come back and do poly.
