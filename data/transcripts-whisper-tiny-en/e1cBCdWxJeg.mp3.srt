1
00:00:00,000 --> 00:00:25,200
Okay, good evening, once again, August 4, 2015, broadcasting from Stanley Creek, Ontario.

2
00:00:25,200 --> 00:00:38,880
Today we have a quote, a simple little quote,

3
00:00:38,880 --> 00:01:02,880
to help others who have to help yourselves. You have to have gone the distance yourself to any others.

4
00:01:02,880 --> 00:01:14,880
It talks specifically about being unrestrained, unrestrained, and undisciplined, and unquenched.

5
00:01:14,880 --> 00:01:40,880
It is certainly possible for one who has tamed oneself as freed oneself, released oneself,

6
00:01:40,880 --> 00:01:58,880
and that they should restrain another restraint exactly.

7
00:01:58,880 --> 00:02:11,880
And they should tame. The word is not exactly a restraint, it's tamedama.

8
00:02:11,880 --> 00:02:21,880
It's tamed their mind.

9
00:02:21,880 --> 00:02:27,880
This is really about it's about taming the mind.

10
00:02:27,880 --> 00:02:37,880
It's particularly in that way because there's many things that you could potentially teach someone without being perfect in it.

11
00:02:37,880 --> 00:02:46,880
You can, it's one thing to do, it's another thing to teach, but what we're talking about here is teaching to have a clear mind.

12
00:02:46,880 --> 00:03:01,880
You see, and without a clear mind yourself, without having attained or having worked on the progress yourself, without having attained this clarity of mind,

13
00:03:01,880 --> 00:03:17,880
the visibility to pass it on to others is really hampered. It's unique in that way because it's not teaching.

14
00:03:17,880 --> 00:03:30,880
It's not about helping others to develop something, it's helping other people to develop the mind.

15
00:03:30,880 --> 00:03:37,880
Without clarity of mind, it's very difficult.

16
00:03:37,880 --> 00:03:44,880
This isn't about information, it's not something you can just tell someone or explain to them.

17
00:03:44,880 --> 00:04:10,880
It's something that they can accomplish just by listening.

18
00:04:10,880 --> 00:04:30,880
It's also a big reason for some of the things that Buddha said like this was the close proximity to other religious teachers.

19
00:04:30,880 --> 00:04:51,880
I guess it's quite similar to how things are today. There are lots of different religious leaders teaching all sorts of crazy and mutually contradictory teaching.

20
00:04:51,880 --> 00:05:01,880
One person will say this, another one will say that.

21
00:05:01,880 --> 00:05:23,880
Just because someone is a religious teacher, just because they have an idea, just because they have a following, just because they're impressive, charismatic, it's not a good enough reason to follow them, because if their minds are not clear,

22
00:05:23,880 --> 00:05:43,880
if they haven't tamed themselves, what good is what they teach, what good are they? They can't help you, they have to help yourself.

23
00:05:43,880 --> 00:05:56,880
Okay, I'm going to leave it there too much to say about it. It's a nice little verse.

24
00:05:56,880 --> 00:06:14,880
Welcome to ask. So you have a question about, and we're asking questions, not on YouTube, but just to repeat again on meditation.ceerymongerlow.org, if you want to ask a question, you have to join our meditation community.

25
00:06:14,880 --> 00:06:18,880
And hopefully do some meditation with us.

26
00:06:18,880 --> 00:06:30,880
Publishing your app source on Bitbucket or GitHub. So that the community can help you enhance them. I'm pretty sure everything I do is on GitHub already.

27
00:06:30,880 --> 00:06:42,880
Maybe not in the newest forum, but when I was, I haven't done that much programming, but most everything I do does end up on GitHub.

28
00:06:42,880 --> 00:06:48,880
And yeah, if someone wants to support something I did to iPhone, I don't mean.

29
00:06:48,880 --> 00:06:52,880
I thought about doing it myself at one point when I was kind of going home about all that.

30
00:06:52,880 --> 00:07:03,880
I thought about getting a Mac and doing some coding, but someone asked me if I could make it to Bitbucket app or iPhone.

31
00:07:03,880 --> 00:07:17,880
I didn't think it's possible. I never, never knew anything about programming before I became a monk. That's possible. Certainly much, you know, it's obviously not the ideal past time for a monk.

32
00:07:17,880 --> 00:07:35,880
I've not been that interested in it. But we're more looking for questions about meditation, I think.

33
00:07:35,880 --> 00:07:49,880
But we're ahead because this daily thing is a daily thing. So we don't have to, I don't expect to be taking an hour each night to do this. I'm happy to, but there's nothing.

34
00:07:49,880 --> 00:08:13,880
Just say hello. Thank you all for joining. Thank you all for meditating with us and see you all next time.

35
00:08:13,880 --> 00:08:31,880
Certainly getting more people now that we've opened it up to YouTube.

36
00:08:31,880 --> 00:08:39,880
Mostly from the US looks like.

37
00:08:39,880 --> 00:09:05,880
Yeah.

38
00:09:05,880 --> 00:09:25,880
Okay, thank you all. Thanks for tuning in. Peace. We'll see you all tomorrow.

