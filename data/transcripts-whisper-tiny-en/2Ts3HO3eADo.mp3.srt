1
00:00:00,000 --> 00:00:03,360
What is your view on having sex as a Buddhist?

2
00:00:03,360 --> 00:00:07,000
Are you, for example, allowed to have multiple sexual relationships?

3
00:00:07,000 --> 00:00:09,000
I put the emphasis wrong.

4
00:00:09,000 --> 00:00:13,000
I'm not allowed to have any sexual relationships, but are you?

5
00:00:13,000 --> 00:00:18,000
It means in general, right?

6
00:00:18,000 --> 00:00:22,000
So in general, a lay person, put it in general,

7
00:00:22,000 --> 00:00:27,000
because sensual desire is evil.

8
00:00:27,000 --> 00:00:28,000
It's an evil.

9
00:00:28,000 --> 00:00:32,000
This is Buddhist word evil.

10
00:00:32,000 --> 00:00:36,000
Because the meaning of the word evil is something that is going to lead you to suffer.

11
00:00:36,000 --> 00:00:41,000
Now, the key Buddhist, the core of Buddhism, is that desire leads you to suffer.

12
00:00:41,000 --> 00:00:46,000
Well, wanting something leads you to try to get it.

13
00:00:46,000 --> 00:00:51,000
When you try to get it, it creates, it changes your life.

14
00:00:51,000 --> 00:00:54,000
We want to get something so you have to work for it.

15
00:00:54,000 --> 00:00:59,000
And that changes your whole universe in some small way.

16
00:00:59,000 --> 00:01:01,000
And that's going to create more stress.

17
00:01:01,000 --> 00:01:07,000
It's going to create more dissatisfaction and suffering.

18
00:01:07,000 --> 00:01:11,000
So undeniably, it's a bad thing.

19
00:01:11,000 --> 00:01:15,000
But the question is, how bad is it really going to get in the way of your meditation practice?

20
00:01:15,000 --> 00:01:17,000
No, probably not.

21
00:01:17,000 --> 00:01:24,000
Excessive sex or multiple sexual relationships can get in you.

22
00:01:24,000 --> 00:01:26,000
Whether it's with one person or with many people.

23
00:01:26,000 --> 00:01:31,000
In fact, many people would be more deleterious for your practice.

24
00:01:31,000 --> 00:01:37,000
It's not a horrible thing in Buddhism to have multiple sexual relationships.

25
00:01:37,000 --> 00:01:40,000
The problem comes where you are deceiving people.

26
00:01:40,000 --> 00:01:45,000
This is where real bad, this is where it gets really bad.

27
00:01:45,000 --> 00:01:48,000
It gets really where you might really start to say, this is evil.

28
00:01:48,000 --> 00:01:51,000
It's really evil when you start deceiving people.

29
00:01:51,000 --> 00:01:54,000
And that's where it really cuts your practice.

30
00:01:54,000 --> 00:01:56,000
And starts heading you in the opposite direction.

31
00:01:56,000 --> 00:01:58,000
It doesn't just slow you down.

32
00:01:58,000 --> 00:02:01,000
It starts heading you in the wrong direction.

33
00:02:01,000 --> 00:02:04,000
When you're sleeping around.

34
00:02:04,000 --> 00:02:11,000
When you have a monogamous relationship and are committed to it and then are cheating on that person.

35
00:02:11,000 --> 00:02:18,000
Or someone else is in a monogamous relationship and you're sleeping with them.

36
00:02:18,000 --> 00:02:21,000
This kind of thing, this is where it really gets trouble.

37
00:02:21,000 --> 00:02:30,000
If you're in a polygamous relationship, this is not a corrupt evil in Buddhism.

38
00:02:30,000 --> 00:02:33,000
And really, I don't know.

39
00:02:33,000 --> 00:02:35,000
I've thought about this a lot.

40
00:02:35,000 --> 00:02:43,000
And you look at it, it's really what never was seen as such.

41
00:02:43,000 --> 00:02:44,000
And I don't know where it became.

42
00:02:44,000 --> 00:02:52,000
But it kind of this maybe in Protestantism that turned it into such a horrible thing.

43
00:02:52,000 --> 00:02:56,000
To have a polygamous relationship.

44
00:02:56,000 --> 00:03:01,000
If anything, it seems to be what God designed us for.

45
00:03:01,000 --> 00:03:05,000
One man, many women, look at nature.

46
00:03:05,000 --> 00:03:11,000
It's not always, but with monkeys, for example, of chimpanzees.

47
00:03:11,000 --> 00:03:15,000
It's very common to see polygamous relationships.

48
00:03:15,000 --> 00:03:20,000
All the time in the forest, one big monkey and he's going around, he's got all these women.

49
00:03:20,000 --> 00:03:29,000
So the problem doesn't, isn't that it's a matter of it's in a category of its own.

50
00:03:29,000 --> 00:03:32,000
It's not in a category, it's just fairly extreme.

51
00:03:32,000 --> 00:03:37,000
The more sex and the more diverse sex you have, the more it's going to get in the way of your practice.

52
00:03:37,000 --> 00:03:42,000
It hasn't crossed the line of being immoral in the sense of leading you backwards yet.

53
00:03:42,000 --> 00:03:51,000
It's not leading you in the wrong, it is, but it can still be worked out through meditation.

54
00:03:51,000 --> 00:03:57,000
If you start cheating on people, if you start lying and deceiving people, breaking up relationships,

55
00:03:57,000 --> 00:04:00,000
that makes your meditation very, very difficult in a very different way.

56
00:04:00,000 --> 00:04:03,000
So that's categorically different.

57
00:04:03,000 --> 00:04:08,000
This has to be understood, but it has to also be understood.

58
00:04:08,000 --> 00:04:11,000
I don't want you to say that, oh, polygamy is fine, go for it.

59
00:04:11,000 --> 00:04:16,000
It's really going to get in your way, and it's going to make you no sex in general,

60
00:04:16,000 --> 00:04:22,000
but for sure promiscuity and polygamy, well, for sure, drive you crazy.

61
00:04:22,000 --> 00:04:26,000
Your mind will become more and more and more attached to it.

62
00:04:26,000 --> 00:04:29,000
The best thing is to give it up and start looking at the sexual desire.

63
00:04:29,000 --> 00:04:32,000
Give it up, be celibate, and start looking at the celibate.

64
00:04:32,000 --> 00:04:33,000
Looking at the desire.

65
00:04:33,000 --> 00:04:37,000
If you may happen to break your celibacy, just don't become a monk yet,

66
00:04:37,000 --> 00:04:40,000
but start looking at it and try to be celibate for a while.

67
00:04:40,000 --> 00:04:41,000
See what happens?

68
00:04:41,000 --> 00:04:45,000
Watch the sexual urges and really learn about them.

69
00:04:45,000 --> 00:04:47,000
Take it as a laboratory experiment.

70
00:04:47,000 --> 00:04:49,000
It's much more conducive.

71
00:04:49,000 --> 00:04:51,000
There's no reason to be promiscuous.

72
00:04:51,000 --> 00:04:53,000
There's no reason to be polygamous.

73
00:04:53,000 --> 00:04:56,000
It doesn't do you any good. It doesn't make you more happy.

74
00:04:56,000 --> 00:04:57,000
What good is it?

75
00:04:57,000 --> 00:04:59,000
It just makes you less satisfied.

76
00:04:59,000 --> 00:05:01,000
It doesn't make you a better person.

77
00:05:01,000 --> 00:05:02,000
It doesn't make you a happier person.

78
00:05:02,000 --> 00:05:04,000
It doesn't make you more content.

79
00:05:04,000 --> 00:05:06,000
What good is it?

80
00:05:06,000 --> 00:05:30,000
It's better to go the other direction.

