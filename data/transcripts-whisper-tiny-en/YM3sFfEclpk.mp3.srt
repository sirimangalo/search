1
00:00:00,000 --> 00:00:06,400
Okay, good evening.

2
00:00:06,400 --> 00:00:29,000
Tonight's question is about pride, more generally it's about dealing with special conditions.

3
00:00:29,000 --> 00:00:34,000
Special problems.

4
00:00:34,000 --> 00:00:48,000
Because the question remarks that anger is easy, easier to note to deal with.

5
00:00:48,000 --> 00:00:53,000
But pride is difficult.

6
00:00:53,000 --> 00:00:58,600
So how can you recognize this is an interesting part of the question is how can you

7
00:00:58,600 --> 00:01:05,600
recognize the early signs of pride so you can get it before it escalates?

8
00:01:05,600 --> 00:01:10,600
That's insightful I think.

9
00:01:10,600 --> 00:01:16,600
But then how can you work to a brute pride in the long run?

10
00:01:16,600 --> 00:01:20,600
That's the question.

11
00:01:20,600 --> 00:01:27,600
What are the practices I think it says?

12
00:01:27,600 --> 00:01:34,600
And so in one sense, this isn't a very good question.

13
00:01:34,600 --> 00:01:40,600
But it's not uncommon.

14
00:01:40,600 --> 00:01:52,600
Not an uncommon sort of question because it's recognizing something as a problem.

15
00:01:52,600 --> 00:02:00,600
Even in the practice of meditation, in sight meditation, which is meant to be problem solving.

16
00:02:00,600 --> 00:02:04,600
In a sense it's meant to be an answer to this question.

17
00:02:04,600 --> 00:02:06,600
How do you deal with suffering?

18
00:02:06,600 --> 00:02:09,600
How do you deal with defilement?

19
00:02:09,600 --> 00:02:13,600
But even in that practice you've got problems.

20
00:02:13,600 --> 00:02:14,600
Anger is not a problem.

21
00:02:14,600 --> 00:02:18,600
It's a problem but within the framework you can deal with it.

22
00:02:18,600 --> 00:02:21,600
We've got a problem for the problem solving.

23
00:02:21,600 --> 00:02:25,600
How do you deal with these special problems?

24
00:02:25,600 --> 00:02:27,600
Pride.

25
00:02:27,600 --> 00:02:38,600
So it's not a very good question because it suggests a lack of understanding

26
00:02:38,600 --> 00:02:46,600
in regards to mindfulness, in regards to seeing clearly rep us in the...

27
00:02:46,600 --> 00:02:54,600
There are no categorically different problems like pride.

28
00:02:54,600 --> 00:03:01,600
The way to deal with all of our problems is in the end not about fixing them

29
00:03:01,600 --> 00:03:08,600
and asking for a practice to rid yourself of them is not the right way.

30
00:03:08,600 --> 00:03:11,600
It's not about reading yourself of problems.

31
00:03:11,600 --> 00:03:15,600
It's about understanding experiences.

32
00:03:15,600 --> 00:03:21,600
The problem is in our perspective and pride is a part of wrong perspective.

33
00:03:21,600 --> 00:03:32,600
The observation is astute because there is a difference between pride and anger.

34
00:03:32,600 --> 00:03:54,600
The good distinction to understand is that anger, anger-based emotions like fear, boredom, depression, frustration, hatred, sadness, all of these ones.

35
00:03:54,600 --> 00:04:01,600
They are highly blamed by oneself and by others.

36
00:04:01,600 --> 00:04:04,600
But they are quick to change.

37
00:04:04,600 --> 00:04:06,600
And so they're acute.

38
00:04:06,600 --> 00:04:12,600
They're the kind of problem that you are quickly able to recognize.

39
00:04:12,600 --> 00:04:17,600
But they're also quite easy to deal with because they go away.

40
00:04:17,600 --> 00:04:19,600
It appears that they're easy to deal with.

41
00:04:19,600 --> 00:04:24,600
You fix them in a sense quite easily because they're quick to change.

42
00:04:24,600 --> 00:04:26,600
It's not actually that we fix them.

43
00:04:26,600 --> 00:04:28,600
It's that they're quick to change.

44
00:04:28,600 --> 00:04:38,600
They're not lasting in that sense because they're uncomfortable.

45
00:04:38,600 --> 00:04:45,600
People with deep chronic depression or anger issues might disagree.

46
00:04:45,600 --> 00:04:53,600
But relatively speaking, when you're actually being mindful of them, they're much easier to deal with.

47
00:04:53,600 --> 00:05:09,600
But they keep coming back and they're acute in the sense that yourself are displeased by them and other people are displeased when they see them in you.

48
00:05:09,600 --> 00:05:16,600
Greed on the other hand is slow to change, but it's little blamed.

49
00:05:16,600 --> 00:05:22,600
It's lightly blamed by oneself and by others.

50
00:05:22,600 --> 00:05:33,600
So Greed is, well, these two qualities go together because the fact that we don't see it as such a problem generally.

51
00:05:33,600 --> 00:05:38,600
Instead, of course, it lasts. It's not acute.

52
00:05:38,600 --> 00:05:44,600
The stress that comes from wanting, from craving is a little bit subdued.

53
00:05:44,600 --> 00:05:47,600
It's not acute.

54
00:05:47,600 --> 00:05:53,600
So I can blunt. We don't even realize that we're suffering because there's so much pleasure often involved with craving.

55
00:05:53,600 --> 00:05:58,600
When you get what you want, you fixate on that because of the craving.

56
00:05:58,600 --> 00:06:06,600
So your mind in your mind, craving is a good thing because your mind is fixed on the getting.

57
00:06:06,600 --> 00:06:13,600
Until you can change your perspective, it's hard to find blame in it.

58
00:06:13,600 --> 00:06:22,600
Once you start to observe it, you start to see how much stress and suffering is involved with craving.

59
00:06:22,600 --> 00:06:28,600
But for the most part, it's not very blame for the other people as well.

60
00:06:28,600 --> 00:06:33,600
Of course, extreme greed has criticized.

61
00:06:33,600 --> 00:06:39,600
But generally speaking, we celebrate it often in each other, in ourselves and in each other,

62
00:06:39,600 --> 00:06:54,600
describing our likes, our partialities, romance, and even cardinal pleasures, food, sex, music, celebrating all of these.

63
00:06:54,600 --> 00:06:58,600
Wow, I love that. I love this.

64
00:06:58,600 --> 00:07:10,600
We say it as though it's something to be proud of.

65
00:07:10,600 --> 00:07:14,600
Yes, greed is therefore different from anger.

66
00:07:14,600 --> 00:07:21,600
In delusion, the bad news is that delusion, those states that are like pride,

67
00:07:21,600 --> 00:07:28,600
or delusion, unlike greed and anger, relates to distortion.

68
00:07:28,600 --> 00:07:30,600
It's not a liking or a disliking.

69
00:07:30,600 --> 00:07:34,600
It's some sort of distortion of reality.

70
00:07:34,600 --> 00:07:37,600
Many kinds, pride is one common example.

71
00:07:37,600 --> 00:07:50,600
Pride is a distorted perception of worth that's related to possession and ownership, identification,

72
00:07:50,600 --> 00:07:56,600
identify with something. This is me and you're proud of it.

73
00:07:56,600 --> 00:08:05,600
You esteem it. It's often caught up in things like low self esteem and worry and fear of failure and so on.

74
00:08:05,600 --> 00:08:12,600
Fear of being inferior or being lacking.

75
00:08:12,600 --> 00:08:22,600
But there are many other kinds of ignorance. Of course, the overarching one, wrong view.

76
00:08:22,600 --> 00:08:30,600
Even worry and doubt, these are in the ignorance category.

77
00:08:30,600 --> 00:08:42,600
No delusion is the bad news is that it's the worst. It's both slow to change and very much blamed.

78
00:08:42,600 --> 00:08:49,600
People who are conceited, arrogant, with wrong views and obstinate.

79
00:08:49,600 --> 00:08:55,600
It's very unpleasant for oneself and for others.

80
00:08:55,600 --> 00:09:11,600
People who get caught up in it live very uncomfortable lives, both internally and in relation to the people around them.

81
00:09:11,600 --> 00:09:19,600
But this is all descriptive. None of it is prescriptive in the sense that it doesn't define how we practice.

82
00:09:19,600 --> 00:09:30,600
It's just a way of helping to understand and in fact tell you the opposite that this is just descriptive.

83
00:09:30,600 --> 00:09:40,600
The difference is that you're experiencing and that you're recognizing in anger and delusion.

84
00:09:40,600 --> 00:09:59,600
Are just the differences that they belong within a framework of reality that is caught up in misunderstanding.

85
00:09:59,600 --> 00:10:12,600
The way they're related to each other is that delusion gives rise to greed and anger. Without the basis of distortion, you can't get greedy and angry.

86
00:10:12,600 --> 00:10:18,600
And once you've gotten rid of the delusion, you'll see that greed and anger still come up.

87
00:10:18,600 --> 00:10:31,600
There's still some delusion there, but even when you're parting the veil and you're mindful, you're going to still see it push through. There's delusion there as well.

88
00:10:31,600 --> 00:10:39,600
But even when you're working against it, you're going to be fighting with greed and anger.

89
00:10:39,600 --> 00:10:48,600
You're going to try in the Sati Bhatanasu, to the Buddha, says, Vineyar Lokaya, Abhidja, Dohmanasam.

90
00:10:48,600 --> 00:10:57,600
He says, one is working to get rid of greed and anger, craving and diversion.

91
00:10:57,600 --> 00:11:02,600
But it doesn't mention delusion because delusion is the opposite of mindfulness.

92
00:11:02,600 --> 00:11:11,600
When you're mindful, all of the things like pride and arrogance and wrong view and so on, they have no room to arise.

93
00:11:11,600 --> 00:11:19,600
You'll see them arise when you're not mindful, but your mindfulness will stop them from proliferating.

94
00:11:19,600 --> 00:11:30,600
And you'll very much see the greed and the anger and seeing the greed and anger is lack of delusion, is the freedom from delusion.

95
00:11:30,600 --> 00:11:38,600
So they are related and there is a difference in terms of how the practice deals with them.

96
00:11:38,600 --> 00:11:51,600
Delusion in one sense, things like pride and so on are not something you can ever really be mindful of because they're the opposite of being mindful.

97
00:11:51,600 --> 00:12:00,600
They're not easy to catch because any time you're in a state of being able to catch them, they're not going to be there.

98
00:12:00,600 --> 00:12:05,600
But anger and greed as well will not technically be there.

99
00:12:05,600 --> 00:12:10,600
But mindfulness is not really about being mindful of what's in the actual present moment.

100
00:12:10,600 --> 00:12:13,600
It feels like that, but it's actually a response.

101
00:12:13,600 --> 00:12:15,600
And that's what it's meant to be.

102
00:12:15,600 --> 00:12:19,600
It's meant to be a replacement for our ordinary ways of responding to things.

103
00:12:19,600 --> 00:12:24,600
Ordinarily something ends up, we respond to greed, we respond to anger, we respond to delusion.

104
00:12:24,600 --> 00:12:32,600
Even greed, anger and delusion come up, we respond further with more of the same or something in reaction to it or something.

105
00:12:32,600 --> 00:12:37,600
And we feel guilty about them, so mindfulness is a replacement for all that.

106
00:12:37,600 --> 00:12:41,600
So rather than feeling guilty about your anger, you're mindful of it.

107
00:12:41,600 --> 00:12:50,600
Rather than being identifying with the craving or self-righteous about your desire, acknowledging it.

108
00:12:50,600 --> 00:12:57,600
Rather than perpetuating the delusion, cutting it off with mindfulness.

109
00:12:57,600 --> 00:13:02,600
Yani, Sotani, Lokas, Ming, Saty, Desan, Niwari.

110
00:13:02,600 --> 00:13:07,600
Whatever streams there are in the world, mindfulness is there.

111
00:13:07,600 --> 00:13:13,600
Stop, that's what stops them.

112
00:13:13,600 --> 00:13:19,600
So in brief, I guess the answer to the question is that there is no difference.

113
00:13:19,600 --> 00:13:24,600
And the fact that you're trying to find a difference is a part of the problem.

114
00:13:24,600 --> 00:13:44,600
And the best solution is to become more accomplished and more familiar with mindfulness, with the power and the efficacy of mindfulness.

115
00:13:44,600 --> 00:13:51,600
Once you understand and appreciate and experience how powerful it is to be present,

116
00:13:51,600 --> 00:13:56,600
you will see that there's nothing else you need because at that moment there's no room for pride.

117
00:13:56,600 --> 00:14:00,600
There's no room for any of these things.

118
00:14:00,600 --> 00:14:14,600
But if you want a more perhaps satisfying answer, then we have to answer a more direct answer, I suppose.

119
00:14:14,600 --> 00:14:24,600
We have to answer more generally in terms of how do you get rid of any defoundment, because the same will apply to anger, the same will apply to pride.

120
00:14:24,600 --> 00:14:28,600
And the answer is wisdom.

121
00:14:28,600 --> 00:14:33,600
When you seek clearly, you come to understand.

122
00:14:33,600 --> 00:14:42,600
And when you understand, understanding is the opposite of pride, it's the opposite of delusion.

123
00:14:42,600 --> 00:14:52,600
And so with no delusion, with no distortion, there is no room for greed or anger to rise as well.

124
00:14:52,600 --> 00:14:55,600
Wisdom is of three types, of course.

125
00:14:55,600 --> 00:15:06,600
So if you want a comprehensive sort of regiment of practice, you need three kinds of wisdom.

126
00:15:06,600 --> 00:15:20,600
The first is sutamayapanya, you have to gain knowledge. So listening to a talk like this might give you some knowledge you didn't know, might give you some perspective that you didn't have, intellectual perspective.

127
00:15:20,600 --> 00:15:30,600
You have to hear, you have to listen or read or gain some contact with an answer to your question, for example.

128
00:15:30,600 --> 00:15:41,600
sutamayapanya, sutamayapanya, which means from thinking, so you have to reflect and understand the teachings you have to learn.

129
00:15:41,600 --> 00:15:48,600
Once you hear how to practice, you have to get an understanding about a practice and appreciation of the practice.

130
00:15:48,600 --> 00:16:05,600
They have to understand the need for a change of perspective rather than trying to fix your problems. Stop looking at them as problems to start seeing reality as experiences and try to understand your experiences.

131
00:16:05,600 --> 00:16:22,600
Try to see them clearly so that you can understand them, not in an intellectual sense, but understand them as being just what they are. Understand them as an experience, as being an experience rather than being a problem.

132
00:16:22,600 --> 00:16:38,600
So the intellectual understanding of all that is jintamayapanya. And then finally, of course, bhavanamayapanya, you have to gain the actual clarity. There's no multiplicity of paths.

133
00:16:38,600 --> 00:17:00,600
The only path is this kind of wisdom when you actually do set yourself to the practice of mindfulness. Mindfulness being the confronting and the facing of your experiences, seeing them just as they are, seeing is just seeing, hearing is just hearing.

134
00:17:00,600 --> 00:17:16,600
Once you gain that perspective, when you are present, when your experiences are your reality, they will arise with them. And it's not intellectual wisdom, it's not a thought, it's not an insight, a flash of insight, it's a perspective.

135
00:17:16,600 --> 00:17:39,600
It's a clarity of understanding, because the opposite of all of these bad things, greed, anger, and delusion is clarity. Is a perfect, perfect attunement to reality. It is what it is, and I see it as that, not as me as mine as this as that as good as bad.

136
00:17:39,600 --> 00:18:04,600
As anything that it's not, there's nothing extraneous, there's just what there is. And when you're there, when you are experiencing, seeing, and seeing, and hearing is hearing, then there's no further proliferation of all these bad things.

137
00:18:04,600 --> 00:18:14,600
They become weaker, they don't gain traction, they fade away because they're as habits, they have to be reinforced in order to be maintained.

138
00:18:14,600 --> 00:18:26,600
Because you're not maintaining them, they fade away, and eventually the perfect clarity changes your perspective, but it's deeper than that.

139
00:18:26,600 --> 00:18:55,600
It changes your fundamental outlook on reality. It's the change of perspective, that so fundamental, so profound that your craving for things is shattered, your aversion to things is shattered, and your identification with things, your delusion about them is just shattered, because you have had this experience of complete freedom from suffering, that is pure, that is perfect.

140
00:18:55,600 --> 00:19:00,600
That is absolute.

141
00:19:00,600 --> 00:19:27,600
So, there's an answer to that question. Thank you for listening.

