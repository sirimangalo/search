1
00:00:00,000 --> 00:00:23,560
Hi, so this is in answer to the second of the questions, I believe, or maybe I've gotten

2
00:00:23,560 --> 00:00:29,760
them in the wrong order, but the other question is, what do you love?

3
00:00:29,760 --> 00:00:40,600
Yeah, well, off the top of my head, I think it's important to point out that one

4
00:00:40,600 --> 00:00:53,680
practice that monks or Buddhists are very set on is the practice of unlimited loving

5
00:00:53,680 --> 00:00:56,040
kindness or love for all beings.

6
00:00:56,040 --> 00:01:00,600
So I practice that every day, and I really, I think if I were to answer it in ordinary

7
00:01:00,600 --> 00:01:03,240
terms, I would say, I really love people.

8
00:01:03,240 --> 00:01:05,360
I love, love beings.

9
00:01:05,360 --> 00:01:06,360
I really do.

10
00:01:06,360 --> 00:01:10,440
I really think it's, you know, through the meditation practice that something I've come to

11
00:01:10,440 --> 00:01:18,120
really appreciate is people's essence or the essence of beings, even the smallest beings.

12
00:01:18,120 --> 00:01:29,760
I mean, I love animals, I love babies, I love old people, this feeling of love is something

13
00:01:29,760 --> 00:01:35,280
that really comes from meditation, and I think other meditators would agree with me that

14
00:01:35,280 --> 00:01:43,640
this is something that we can all agree that is very lovable or is very wonderful or brings

15
00:01:43,640 --> 00:01:46,120
forth a great amount of love.

16
00:01:46,120 --> 00:01:51,080
Being more specific, I just wanted to show you a few things that I love here.

17
00:01:51,080 --> 00:02:00,760
So we're going to zoom in, let's see, and I'm going to, first of all, the first thing

18
00:02:00,760 --> 00:02:04,480
I love is my parents.

19
00:02:04,480 --> 00:02:08,080
This is my father.

20
00:02:08,080 --> 00:02:11,160
I asked him to send me a picture of himself, I realized I don't have any pictures of

21
00:02:11,160 --> 00:02:19,400
him, so he sent me this, it's the picture of my father with a guitar.

22
00:02:19,400 --> 00:02:24,800
This is my mother, another person I love.

23
00:02:24,800 --> 00:02:26,800
My parents have been really good to me.

24
00:02:26,800 --> 00:02:32,920
They are two people who I think, but weren't for them, I wouldn't be here today.

25
00:02:32,920 --> 00:02:37,680
Of course, I was saying, but no really, you know, just the great things that they've done

26
00:02:37,680 --> 00:02:44,320
for me, bringing me up, giving me all the things that I need.

27
00:02:44,320 --> 00:02:52,560
Sometimes when I wanted to do something, go on a trip or get involved in some organization,

28
00:02:52,560 --> 00:03:02,880
my parents were always there for me to support me both monetarily and emotionally.

29
00:03:02,880 --> 00:03:05,840
It was a little more difficult when I decided I wanted to become a Buddhist and a Buddhist

30
00:03:05,840 --> 00:03:10,920
monk, once I decided that I wanted to shave my head and put on robes and walk around

31
00:03:10,920 --> 00:03:13,200
barefoot.

32
00:03:13,200 --> 00:03:18,320
It was a little more difficult for them to, well, let's say a lot more difficult for

33
00:03:18,320 --> 00:03:25,960
them to accept and be supportive of, but I think recently, or in the past while they become

34
00:03:25,960 --> 00:03:32,960
much more supportive, and now I think I really have to thank them for just being

35
00:03:32,960 --> 00:03:36,600
so understanding and open-minded about this all.

36
00:03:36,600 --> 00:03:39,880
There's my father again.

37
00:03:39,880 --> 00:03:40,880
He's actually a lawyer.

38
00:03:40,880 --> 00:03:48,400
He's not a musician by trade, but, well, you know, and there's a close up of my father.

39
00:03:48,400 --> 00:03:50,120
So that's two things that I love.

40
00:03:50,120 --> 00:03:57,560
A third thing is my teacher is something or someone that I really, really love, and I'm

41
00:03:57,560 --> 00:04:03,480
going to just show you some pictures here of him, but I'm going to also play an audio

42
00:04:03,480 --> 00:04:09,800
talk, just so you can hear why my teacher is so lovable, you can hear how lovable he

43
00:04:09,800 --> 00:04:10,800
is.

44
00:04:10,800 --> 00:04:12,840
Let's see if it's going to start.

45
00:04:12,840 --> 00:04:42,800
Let's see if it's going to start, but I think it's going to start, but I think it's going

46
00:04:42,800 --> 00:04:45,800
to start.

47
00:04:45,800 --> 00:05:07,540
He had a little close to them.

48
00:05:07,540 --> 00:05:12,860
aling.

49
00:05:12,920 --> 00:05:31,080
One day to eat one and one life at the time,

50
00:05:31,080 --> 00:05:34,880
They keep down with one...

51
00:05:35,680 --> 00:05:39,480
one job of total life, poor, poor country.

52
00:05:42,880 --> 00:05:44,280
They were...

53
00:05:44,480 --> 00:05:46,880
a whole day I don't...

54
00:05:46,880 --> 00:05:49,680
but total life don't...

55
00:05:51,480 --> 00:05:54,480
get life in a chardonnay tunnel.

56
00:05:55,680 --> 00:05:58,280
They like to eat none of my diet.

57
00:05:58,280 --> 00:06:01,620
Free, poor, poor, poor, Victorian.

58
00:06:02,520 --> 00:06:05,160
My diet.

59
00:06:06,280 --> 00:06:10,560
Why does not you do that?

60
00:06:11,640 --> 00:06:13,840
Why does that sound?

61
00:06:17,640 --> 00:06:22,800
If the 19 were

62
00:06:23,200 --> 00:06:24,040
..

63
00:06:24,040 --> 00:06:28,560
A lot of times you may see less than me tongue.

64
00:06:29,160 --> 00:06:32,460
L02.

65
00:06:34,220 --> 00:06:38,840
Many days you'll lose a guess to your Gained Devil.

66
00:06:38,880 --> 00:06:40,820
The fecal man.

67
00:06:40,920 --> 00:06:39,000
R

68
00:06:39,280 --> 00:06:44,320
St.

69
00:06:44,580 --> 00:06:46,120
Nipar.

70
00:06:46,120 --> 00:06:49,620
report no reason to teach a King.

71
00:06:49,920 --> 00:06:52,600
But you must

72
00:06:52,600 --> 00:06:56,560
And I said, you know, it's a difficult time to see.

73
00:06:56,560 --> 00:06:59,160
And you know, all mine.

74
00:06:59,160 --> 00:07:03,720
So, my third wife, yes, I walked online,

75
00:07:03,720 --> 00:07:07,680
or on to mine, and again, I didn't buy.

76
00:07:07,680 --> 00:07:12,680
So, to my second, no, it's time to see.

77
00:07:12,680 --> 00:07:15,640
Time to see, it's a difficult time to see.

78
00:07:15,640 --> 00:07:18,320
For general, I'm not gonna lie.

79
00:07:18,320 --> 00:07:21,600
Don't don't get married, I will need.

80
00:07:21,600 --> 00:07:27,600
But me, my mom, and I actually give a time going on to go

81
00:07:27,600 --> 00:07:29,600
now, so I'm gonna take a bite.

82
00:07:32,600 --> 00:07:35,600
Okay, so there's just a few of these many images.

83
00:07:35,600 --> 00:07:38,600
And that's just a talk he gave in Thai.

84
00:07:38,600 --> 00:07:43,600
That's really good talk, but I don't suppose any of that was

85
00:07:43,600 --> 00:07:48,600
for most people that it's not understandable.

86
00:07:48,600 --> 00:07:50,600
He's talking about doing good deeds.

87
00:07:50,600 --> 00:07:55,600
I just wanted to hear his voice because he has a very soft,

88
00:07:55,600 --> 00:08:00,600
very distinguished voice.

89
00:08:00,600 --> 00:08:05,600
And he was talking about the New Year's resolution of the Thai

90
00:08:05,600 --> 00:08:09,600
government many years ago to do for the whole country to set

91
00:08:09,600 --> 00:08:11,600
themselves in good deeds.

92
00:08:11,600 --> 00:08:16,600
And he said that's in line with the Buddhist teaching that says

93
00:08:16,600 --> 00:08:21,600
that we should set our minds, it is the minds, which is most

94
00:08:21,600 --> 00:08:24,600
important, the mission set our minds on doing good deeds.

95
00:08:24,600 --> 00:08:31,600
That if our mind, if someone does a bad deed, it hurts them.

96
00:08:31,600 --> 00:08:33,600
And it hurts them in this life.

97
00:08:33,600 --> 00:08:37,600
But if their, when their mind is set on doing bad deeds,

98
00:08:37,600 --> 00:08:41,600
it's something that carries with them their whole life and even

99
00:08:41,600 --> 00:08:43,600
into the next life.

100
00:08:43,600 --> 00:08:47,600
So he said it's most important that we set our minds on

101
00:08:47,600 --> 00:08:50,600
goodness, we set our minds on doing good things.

102
00:08:50,600 --> 00:08:57,600
That we set our minds away from doing evil deeds.

103
00:08:57,600 --> 00:09:03,600
Okay, I think that's all.

104
00:09:03,600 --> 00:09:06,600
So there's three things that I love.

105
00:09:06,600 --> 00:09:10,600
The final thing that I would say I love is I love being a monk.

106
00:09:10,600 --> 00:09:14,600
And I hope that's clear from the videos that I've been

107
00:09:14,600 --> 00:09:15,600
recording.

108
00:09:15,600 --> 00:09:21,600
This is the one thing that I could ever think of doing in my

109
00:09:21,600 --> 00:09:22,600
life.

110
00:09:22,600 --> 00:09:25,600
I couldn't ask for anything more and I couldn't ask to be

111
00:09:25,600 --> 00:09:27,600
doing it something better.

112
00:09:27,600 --> 00:09:30,600
There's nothing else in the world that I want to be more

113
00:09:30,600 --> 00:09:34,600
than what I am right now and that's a monk as far as

114
00:09:34,600 --> 00:09:35,600
externally.

115
00:09:35,600 --> 00:09:40,600
If I could think of one situation that I'd like to be in,

116
00:09:40,600 --> 00:09:42,600
it's the situation of the monks life.

117
00:09:42,600 --> 00:09:44,600
And so this is one thing that I really love.

118
00:09:44,600 --> 00:09:45,600
I love being a monk.

119
00:09:45,600 --> 00:09:49,600
I love the structure and the rigor and the lifestyle.

120
00:09:49,600 --> 00:09:52,600
And the fact that I'm able to help people, I'm able to give

121
00:09:52,600 --> 00:09:53,600
something to people that's useful.

122
00:09:53,600 --> 00:09:56,600
Okay, so that's the answer to my answer to the question,

123
00:09:56,600 --> 00:09:58,600
what do you love?

