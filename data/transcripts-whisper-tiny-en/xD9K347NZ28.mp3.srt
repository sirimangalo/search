1
00:00:00,000 --> 00:00:05,760
Welcome back to Ask a Monk, today's question.

2
00:00:05,760 --> 00:00:11,760
You said you don't recommend your practicing yoga, however you recommend walking meditation.

3
00:00:11,760 --> 00:00:17,040
But in my opinion, you can do yoga very slowly and meditative mindfully.

4
00:00:17,040 --> 00:00:18,720
So it's like walking meditation.

5
00:00:18,720 --> 00:00:24,640
You agree and would you recommend this kind of yoga?

6
00:00:24,640 --> 00:00:32,520
The only reason why we practice walking meditation really is because sitting around all

7
00:00:32,520 --> 00:00:36,320
day is not good for the body.

8
00:00:36,320 --> 00:00:43,240
And moreover is quite difficult to maintain because of the stress on the body.

9
00:00:43,240 --> 00:00:54,600
So walking meditation is simply a relief of that strain and for the purposes of exercising

10
00:00:54,600 --> 00:01:02,160
the body in the limited sense of allowing the blood to flow and the food to digest and

11
00:01:02,160 --> 00:01:03,160
so on.

12
00:01:03,160 --> 00:01:07,880
For instance, after you eat, if you go and sit down, the food in your body doesn't digest.

13
00:01:07,880 --> 00:01:12,480
And some of the things that I went over in the how to meditate videos.

14
00:01:12,480 --> 00:01:17,720
I don't think the same can be said for yoga.

15
00:01:17,720 --> 00:01:23,120
It's of course obvious that you could practice yoga mindfully.

16
00:01:23,120 --> 00:01:25,360
The question is why are you practicing yoga?

17
00:01:25,360 --> 00:01:30,200
Why aren't you simply carrying out your daily life as normal?

18
00:01:30,200 --> 00:01:36,000
The reason we do any sort of formal meditation is simply to limit the number of objects

19
00:01:36,000 --> 00:01:37,320
that we have to be mindful of.

20
00:01:37,320 --> 00:01:38,760
We sit down, we close our eyes.

21
00:01:38,760 --> 00:01:42,600
It's not because there's something special about sitting down and closing your eyes that's

22
00:01:42,600 --> 00:01:45,960
going to bring about some special state of enlightenment.

23
00:01:45,960 --> 00:01:54,160
It's like controlling the variables in a laboratory experiment.

24
00:01:54,160 --> 00:02:00,520
There's less to be taken to consideration and therefore it's easier to carry out your

25
00:02:00,520 --> 00:02:01,520
experiments.

26
00:02:01,520 --> 00:02:06,760
In the same way, with your eyes closed, it's easier to carry out the mindfulness.

27
00:02:06,760 --> 00:02:09,840
The same can be said with walking meditation.

28
00:02:09,840 --> 00:02:14,960
There's nothing special about it because it's slow or in some way meditative.

29
00:02:14,960 --> 00:02:19,680
It's just that it's a piece by piece motion so you're able to watch one movement at a time.

30
00:02:19,680 --> 00:02:29,000
You're able to watch your mind's reaction to each movement and because it's repetitive,

31
00:02:29,000 --> 00:02:38,200
it forces your mind to be patient and it forces out a lot of our attachments and our

32
00:02:38,200 --> 00:02:46,560
addictions and allows us to see quite clearly our addiction to pleasure and so on.

33
00:02:46,560 --> 00:02:50,040
I don't personally think that the same can be said of yoga.

34
00:02:50,040 --> 00:02:58,600
I think that it's a spiritual practice with its own theory and philosophy and goals and

35
00:02:58,600 --> 00:03:04,920
I'm not 100% sure what those are but I get the feeling from what I've learned that they're

36
00:03:04,920 --> 00:03:09,480
varied and so you couldn't say that yoga is for x, y or z.

37
00:03:09,480 --> 00:03:17,320
It depends on which yoga teacher and which yoga tradition you're following but I think

38
00:03:17,320 --> 00:03:18,840
you could practice yoga mindfully.

39
00:03:18,840 --> 00:03:22,640
The question is why are you practicing yoga and I would say the answer to that question

40
00:03:22,640 --> 00:03:30,080
probably makes it on some level incompatible with the your practice of Buddhism in the

41
00:03:30,080 --> 00:03:38,280
sense that it's it's on a different path, a path which has its own spiritual tradition

42
00:03:38,280 --> 00:03:44,200
and goals as I said so I've talked about yoga before but I know this has come up again

43
00:03:44,200 --> 00:04:13,640
and probably is going to keep coming up so there's more thoughts on yoga so all the best.

