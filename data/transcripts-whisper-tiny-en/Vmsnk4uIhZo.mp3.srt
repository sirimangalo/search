1
00:00:00,000 --> 00:00:15,920
Good evening, everyone, for those of you who are watching this or joining us live on YouTube.

2
00:00:15,920 --> 00:00:24,840
I'm just recording a demo from the video and now we can answer some questions if anybody

3
00:00:24,840 --> 00:00:40,520
has them. I didn't I didn't mention that I don't think did I mention about the digital

4
00:00:40,520 --> 00:00:46,760
poly reader? I don't think I did. No. The digital poly reader doesn't really work anymore.

5
00:00:46,760 --> 00:00:58,320
It does, but it may not soon. What changed Monday? Firefox Mozilla started being somewhat

6
00:00:58,320 --> 00:01:12,120
a little fascist dictatorial, but they're add-on policy. So now we have to actually submit

7
00:01:12,120 --> 00:01:18,920
our add-ons for their review or Firefox won't even install them. It's kind of addictive.

8
00:01:18,920 --> 00:01:32,720
I had to upload all my add-ons to their site. Many people have contacted me. Don't play

9
00:01:32,720 --> 00:01:38,080
me. But there's a way to get around it for those of you who are wondering. I just had to

10
00:01:38,080 --> 00:01:43,960
just tonight I had to get around it again. I think you have to go into your configuration

11
00:01:43,960 --> 00:01:51,200
and switch something to false. Something about signatures. You can read about it. There's

12
00:01:51,200 --> 00:02:03,680
sites that tell you how to do it. Be any easier to use Google instead of Firefox? No,

13
00:02:03,680 --> 00:02:14,040
it's integrated with Firefox. I made a decision because of how open Mozilla was. That

14
00:02:14,040 --> 00:02:35,400
Firefox was easier and more powerful. But now it seems like that is a bit of a mistake.

15
00:02:35,400 --> 00:02:46,640
Just a comment that someone is having trouble with your meditation timer not working. Anyone

16
00:02:46,640 --> 00:02:56,280
wants to troubleshoot the website or reprogram it or free? At this point, as is until we get

17
00:02:56,280 --> 00:03:03,760
some real developers? I wonder if there was someone who was working on a meditation app

18
00:03:03,760 --> 00:03:12,040
for iPhone for you. Did that ever be finished? No, I didn't hear back at all about that.

19
00:03:12,040 --> 00:03:21,360
It was kind of slick what they were doing. Yeah. I remember seeing the just a little

20
00:03:21,360 --> 00:03:31,480
preview. Okay. Maybe we'll call it a night.

21
00:03:31,480 --> 00:03:39,720
Thank you all for tuning in. Thank you very much to see here. We'll have a demo part of

22
00:03:39,720 --> 00:03:48,880
video episode. See you all tomorrow. Oh, tomorrow here is something. We can have a meditate

23
00:03:48,880 --> 00:03:54,800
into the new year. How about that? That would be nice. Tomorrow we'll have broadcast at

24
00:03:54,800 --> 00:04:05,880
9 and then after the broadcast, we'll just meditate until midnight together. That sounds

25
00:04:05,880 --> 00:04:14,080
good. Thank you, Bante. Okay. So everyone come on tomorrow at 9.30 or 9. We'll have the

26
00:04:14,080 --> 00:04:20,880
broadcast and we'll go all the way to midnight. We will broadcast a midnight, but we'll

27
00:04:20,880 --> 00:04:26,800
do it on our meditation site. You don't have to meditate for all the hours, but

28
00:04:26,800 --> 00:04:34,080
you can meditate on and off. And midnight will all come back on. The meditation

29
00:04:34,080 --> 00:04:40,760
site and just say happy new year. Wish each other happy new. Just an excuse to

30
00:04:40,760 --> 00:04:49,480
get together, get together and do good deeds. Okay, good night. Good night, Bante.

