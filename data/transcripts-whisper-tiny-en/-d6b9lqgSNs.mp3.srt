1
00:00:00,000 --> 00:00:29,000
I'm a hero of me, I'm a hero of me

2
00:00:29,000 --> 00:00:33,000
Hi, my name is Pratnoa Yutatamo

3
00:00:33,000 --> 00:00:37,000
and I've been a monk in Thailand for the past seven years

4
00:00:37,000 --> 00:00:40,000
I was ordained in Chantong

5
00:00:40,000 --> 00:00:44,000
by my preceptor, Ajantong, Serimangalo

6
00:00:44,000 --> 00:00:49,000
and I have since moved around to try to find a suitable place

7
00:00:49,000 --> 00:00:52,000
to start an international meditation center

8
00:00:52,000 --> 00:01:04,000
The Lord Buddha said that we should always strive to go on arms around every morning for our food

9
00:01:04,000 --> 00:01:07,000
so it's something that I've taken to heart

10
00:01:07,000 --> 00:01:18,000
and I always try my best to make it a part of my daily routine

11
00:01:24,000 --> 00:01:28,000
The Buddhism in Thai culture is very important to do good news

12
00:01:28,000 --> 00:01:30,000
This is a source of happiness

13
00:01:30,000 --> 00:01:33,000
So because of the teaching, it just becomes a nature

14
00:01:33,000 --> 00:01:38,000
for someone to deserve something, yes, this monk is practicing in a way

15
00:01:38,000 --> 00:01:42,000
which is noble, which is honorable

16
00:01:42,000 --> 00:01:45,000
and we immediately move for it

17
00:01:45,000 --> 00:01:49,000
and they were somebody to support

18
00:01:49,000 --> 00:02:06,000
and to make merit we say to the Buddha

19
00:02:06,000 --> 00:02:10,000
Being a monk is a lot more about what you aren't than anything else

20
00:02:10,000 --> 00:02:19,000
I mean, the probably word for ordaining means going forth

21
00:02:19,000 --> 00:02:20,000
and leaving behind

22
00:02:20,000 --> 00:02:23,000
and we leave behind the ordinary everyday life

23
00:02:23,000 --> 00:02:28,000
we leave behind pretty much everything that makes us the average ordinary person who we are

24
00:02:28,000 --> 00:02:32,000
and all that's left is something like a nobody

25
00:02:32,000 --> 00:02:34,000
a bomb if you wanted to say

26
00:02:34,000 --> 00:02:38,000
someone who doesn't vote

27
00:02:38,000 --> 00:02:42,000
who doesn't speak up on current events while sometimes

28
00:02:42,000 --> 00:02:45,000
but doesn't get involved with the world

29
00:02:45,000 --> 00:02:48,000
someone who has left the world behind

30
00:02:48,000 --> 00:02:51,000
and all that's left is someone who walks, who sits, who eats

31
00:02:51,000 --> 00:02:55,000
who does things which everyone else does

32
00:02:55,000 --> 00:02:58,000
in a very ordinary way

33
00:02:58,000 --> 00:03:01,000
you become very ordinary as a monk, I think

34
00:03:01,000 --> 00:03:04,000
and maybe that's what's so fascinating about it

35
00:03:04,000 --> 00:03:09,000
you know, you're just being, I mean, how can you do that

36
00:03:09,000 --> 00:03:13,000
and of course often it involves living in the forest

37
00:03:13,000 --> 00:03:16,000
and so on which is always somewhat romantic

38
00:03:16,000 --> 00:03:21,000
but in the end, I think that's a lot to do with

39
00:03:21,000 --> 00:03:23,000
what the meditation is as well

40
00:03:23,000 --> 00:03:25,000
it's coming back to the ordinary

41
00:03:25,000 --> 00:03:28,000
because most of our lives as ordinary people

42
00:03:28,000 --> 00:03:31,000
and most of the things that get us into trouble

43
00:03:31,000 --> 00:03:33,000
are the things that take us out of the ordinary everyday life

44
00:03:33,000 --> 00:03:38,000
you know, our wishes, our desires, our wants, our needs

45
00:03:38,000 --> 00:03:41,000
things that in the future are attachment to the future

46
00:03:41,000 --> 00:03:43,000
or attachment to the past

47
00:03:43,000 --> 00:03:46,000
thinking or mourning or worrying

48
00:03:46,000 --> 00:03:49,000
about things which have nothing to do with

49
00:03:49,000 --> 00:03:51,000
right here and right now

50
00:03:51,000 --> 00:03:54,000
so what you get when you leave all this behind

51
00:03:54,000 --> 00:03:56,000
is simply an ordinary state of being

52
00:03:56,000 --> 00:04:00,000
which is in the end really

53
00:04:00,000 --> 00:04:02,000
really just about nothing

54
00:04:02,000 --> 00:04:05,000
it's just being, that's

55
00:04:05,000 --> 00:04:32,000
to me, that's what it means to be in mind

