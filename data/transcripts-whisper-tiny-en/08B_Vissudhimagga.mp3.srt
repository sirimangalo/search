1
00:00:00,000 --> 00:00:09,500
Now, suppose you have all five Janas and also all Aruba Vajara Janas.

2
00:00:09,500 --> 00:00:16,540
And also you can experience what we call a super normal knowledge of Abinya.

3
00:00:16,540 --> 00:00:22,940
Abinya is especially developed fifth Janas.

4
00:00:22,940 --> 00:00:36,800
Abinas are those dead and able yogis to see things far away and to hear sounds far away and so on, or to perform some miracles.

5
00:00:36,800 --> 00:00:48,480
Now, suppose you want to hear the sounds far away.

6
00:00:48,480 --> 00:00:57,580
Now, suppose you have all five Janas and also all Aruba Vajara Janas.

7
00:00:57,580 --> 00:01:04,500
And also you can experience what we call a super normal knowledge of Abinya.

8
00:01:04,500 --> 00:01:10,940
Abinya is especially developed fifth Janas.

9
00:01:10,940 --> 00:01:25,040
And Abinas are those dead and able yogis to see things far away and to hear sounds far away and so on, or to perform some miracles.

10
00:01:25,040 --> 00:01:32,600
Now, suppose you want to hear the sounds far away.

11
00:01:32,600 --> 00:01:40,400
And it depends on how large you extend the casino sign.

12
00:01:40,400 --> 00:01:50,800
If you extend the casino sign as big as San Francisco City, then you may hear sounds in San Francisco City and not outside.

13
00:01:50,800 --> 00:01:57,520
So, the 10 casinos among these 40 meditations subjects need to be extended.

14
00:01:57,520 --> 00:02:06,520
For it is within just so much space as one is intent upon, that means one covers one extends over.

15
00:02:06,520 --> 00:02:15,560
With the casino, that one can hear sounds with the divine ear elements, see visible objects with the divine eye and know the minds of other beings with the mind.

16
00:02:15,560 --> 00:02:33,960
So, before you experience the super normal knowledge, you have to practice, you have to extend the sign with the counterpart sign.

17
00:02:33,960 --> 00:02:48,040
That means you are defining and the area within which your super normal knowledge will apply.

18
00:02:48,040 --> 00:02:51,400
So, they need big standards.

19
00:02:51,400 --> 00:02:56,280
Mindfulness occupied with the body and the 10 kinds of founders need not be extended.

20
00:02:56,280 --> 00:03:00,800
So, these subjects need not be extended while because they have a definite location.

21
00:03:00,800 --> 00:03:14,080
You have to define these things and so, you cannot extend them and if you extend them, there is no benefit because there is no benefit in it.

22
00:03:14,080 --> 00:03:19,560
The definiteness of their location will become clear in explaining the mental development.

23
00:03:19,560 --> 00:03:31,480
Now, when the other comes to explaining how to practice meditation on dead bodies and so on, it will become clear.

24
00:03:31,480 --> 00:03:38,920
If the letter are extended, it is only a quantity of corpses that is extended with regard to folders' meditation.

25
00:03:38,920 --> 00:03:46,960
You will extend the corpses, I mean in your mind, causes, but there is no benefit.

26
00:03:46,960 --> 00:03:56,400
And it is said, in answer to the question of so power, perception of visible forms is quite clear, there is that one perception of bone is not clear.

27
00:03:56,400 --> 00:04:01,840
For here, the perception of visible form is called quite clear in the sense of extension of the sign.

28
00:04:01,840 --> 00:04:10,560
When the perception of bone is called not quite clear, perception of bones has to do with the founder's meditation.

29
00:04:10,560 --> 00:04:23,360
In the sense, it's non-extension. For the words, I was intended upon this whole earth with the perception of his skeleton that was uttered by one elder.

30
00:04:23,360 --> 00:04:28,240
Now, they are said, in the manner of appearance to one who has acquired dead perception.

31
00:04:28,240 --> 00:04:37,600
That means one who has acquired dead perception before and now he extends this perception.

32
00:04:37,600 --> 00:04:50,400
So, it is all right because he is not practicing to get that perception, he has already got that perception and so he extends.

33
00:04:50,400 --> 00:04:54,400
For just as in Damasoka's time, the Karavika, but other is sweet.

34
00:04:54,400 --> 00:04:59,680
So, when it's all reflection in the looking glass walls all round and perceived,

35
00:04:59,680 --> 00:05:09,520
Karavika in every direction, so we elder, singular Peter thought when he saw the sign appearing in all directions through his acquisition of perception of his skeleton

36
00:05:09,520 --> 00:05:22,000
that the whole earth was covered with bones. And then there's the foot non-Kara Vika, but it's interesting, but it's difficult to believe.

37
00:05:22,000 --> 00:05:31,280
It's a kind of word and it is that it's sound is very sweet. And so, the Queen asked the community,

38
00:05:31,280 --> 00:05:46,000
who is voice or who is sound, the community said, the Karavika, but so the Queen wanted to listen to the sound of the Karavika, but

39
00:05:46,000 --> 00:05:58,240
then so he asked, she asked his King, King Ahsoka to bring a Karavika, but so what Ahsoka did was just send a cage.

40
00:05:58,240 --> 00:06:10,640
The cage flew through the air and lay down near the bird and the bird got in the cage and the cage flew back to the city and so on.

41
00:06:10,640 --> 00:06:23,440
But after reaching the city, he would not understand, because he was depressed. So, the King asked why, why did not make any sound?

42
00:06:23,440 --> 00:06:32,560
So, they said, because he was lonely. If the bird had a companion, then he would make noise.

43
00:06:32,560 --> 00:06:41,040
So, the King put the mirrors around him and so he did the bird saw his images in the mirror and he saw the other birds.

44
00:06:41,040 --> 00:06:52,560
So, he was heavy and so he made a sound. And the Queen, when she heard the sound, she was so pleased and she was very heavy.

45
00:06:52,560 --> 00:07:08,560
And dwelling on that heaviness, practicing meditation on that heaviness, she practiced with Basana and she became a stream winner established in the

46
00:07:08,560 --> 00:07:38,400
diffusion of string entry. That means he became a soda banner. This is just an example. Just as the Karavika, but so many other birds and the mirror and so make sound with the elder here, when he saw the sand appearing in all directions through his acquisition of the perception of a skeleton, he thought the who are those covered with bones.

47
00:07:38,400 --> 00:07:58,560
So, he appeared to him as that. It is not that he extended the casino sign. And then next paragraph, if that is so, then if what is called the measurelessness of the object of

48
00:07:58,560 --> 00:08:23,520
the general produce on foulness contracted. Now, the general produce on foulness is mentioned as measureless or if it is mentioned as with measure. So, is that contradicted then the answer is no.

49
00:08:23,520 --> 00:08:45,280
When a person looks at these small cogs, then his object is said to be with measure. And if he looks at a big cogs, then his object is said to be measureless, although it is not really measureless, but measureless here means large.

50
00:08:45,280 --> 00:09:14,240
So, another large object and small object. And then, paragraph 115, as regards the immaterial states as objects. It should read as regards objects of the immaterial states, not immaterial states and objects.

51
00:09:14,240 --> 00:09:44,160
But objects of the immaterial states, space need not be extended, since it is the mere removal of the casino. So, with regard to the objects of Aruba Vadirajana. Now, the object of the first Aruba Vadirajana is what? Infinite space. So, that cannot be extended, because it is nothing.

52
00:09:44,160 --> 00:09:55,360
It is obtained through the removal of casino. And removal of casino means not paying attention to the casino sign.

53
00:09:55,360 --> 00:10:10,360
First, there is casino sign in his mind and then he stops paying attention to that casino sign. So, the casino sign disappears and in the place of that casino sign just the space remains.

54
00:10:10,360 --> 00:10:17,360
So, in that space is space, so that cannot be extended.

55
00:10:17,360 --> 00:10:37,760
If he extends it, nothing further happens. So, nothing will happen. And consciousness need not be extended. Actually, consciousness should not be, or could not be extended.

56
00:10:37,760 --> 00:10:51,760
For instance, it is a state consisting in an individual essence. That means, it is a paramata. It is an ultimate reality, a reality which is its own characteristic or individual essence.

57
00:10:51,760 --> 00:11:16,360
Only the concept can be extended, not the real, the ultimate reality. Ultimate reality is just ultimate reality and it does not lend itself to being extended. So, the consciousness or the first consciousness cannot be the first Aruba Vadirajana consciousness cannot be extended.

58
00:11:16,360 --> 00:11:27,960
And the disappearance of consciousness need not be extended, because actually, it is concept and it is non-existent. So, what is non-existent cannot be extended.

59
00:11:27,960 --> 00:11:44,760
And then the last one, the base consisting of another perception or non-perception. Here also, the object of the base consisting of another perception or non-perception need not be extended since it is, it too is a state consisting in an individual essence.

60
00:11:44,760 --> 00:12:01,360
Now, do you remember the object of the fourth Aruba Vadirajana? The object of the fourth Aruba Vadirajana is the third Aruba Vadirajana consciousness.

61
00:12:01,360 --> 00:12:25,960
So, the third Aruba Vadirajana consciousness is again an ultimate reality having its own individual essence. So, it cannot be extended, because it is not a concept. So, they cannot be extended. So, this is as to whether they can be extended or not.

62
00:12:25,960 --> 00:12:45,740
And then as to the object, paragraph 117 of these 40 meditations of JV-22 have quantified sign as object. Now, on the chart, under the column, NIMITA,

63
00:12:45,740 --> 00:13:06,940
a counterpart sign, a PT means counterpart sign. So, it says, 22 have counterpart signs. Then,

64
00:13:06,940 --> 00:13:24,340
Kaseenas, 10, Asuba as a fallen as meditation, and these two, that is to say, the 10 Kaseenas, the 10 kinds of fallen as mindfulness of breathing and mindfulness occupied with the body.

65
00:13:24,340 --> 00:13:35,840
The rest do not have counterpart signs as object. Then, 12 have a stage consisting an individual essence object, that is to say, 8 of the 10 recollections,

66
00:13:35,840 --> 00:13:43,340
except mindfulness of breathing and mindfulness occupied with the body, the perception of the possessiveness and nutrient, the defining of the full elements.

67
00:13:43,340 --> 00:13:51,740
The base consisting of boundless consciousness and the base consciousness and neither perception or non-perception.

68
00:13:51,740 --> 00:13:58,940
So, they have the ultimate reality as objects.

69
00:13:58,940 --> 00:14:14,540
And the 22 have quantified signs as object, that is to say, the 10 Kaseenas, the 10 kinds of fallen as mindfulness of breathing and mindfulness of mindfulness occupied with the body, while the remaining 6 have not so classifiable objects.

70
00:14:14,540 --> 00:14:20,640
So, these are the description of the objects of meditation in different ways.

71
00:14:20,640 --> 00:14:31,240
And the 8 of mobile objects in the early stage through the counterpart, though the counterpart is stationary.

72
00:14:31,240 --> 00:14:44,640
That is to say, the fastering the bleeding, the warm fasted mindfulness of breathing and water cleaner, the fire guessing at the air guessing at the case of light guessing at the object consisting of a circle of sunlight, etc.

73
00:14:44,640 --> 00:14:48,840
So, they are shaking objects, they can be shaking objects.

74
00:14:48,840 --> 00:14:54,440
The rest have mobile objects.

75
00:14:54,440 --> 00:15:07,140
Now, they have shaking objects only in the preliminary stage, but when the yogi reaches the counterpart signs states, then they are stationary.

76
00:15:07,140 --> 00:15:17,840
So, there is only in the preliminary stage, that they have shaking objects or mobile objects.

77
00:15:17,840 --> 00:15:23,340
And as to the plane, that means as to the different 31 planes of existence.

78
00:15:23,340 --> 00:15:33,840
12, namely the 10 kinds of fallenness, mindfulness occupied with the body and perception of robustness in new human, do not occur among deity.

79
00:15:33,840 --> 00:15:55,840
That means they are in the fourth child of the breathing stops.

80
00:15:55,840 --> 00:16:07,840
So, come to that description of the breathing meditation, so that is right.

81
00:16:07,840 --> 00:16:19,840
And then as to apprehending, that means as to taking the object by side, by hearing, by seeing and so on.

82
00:16:19,840 --> 00:16:27,840
So, here, according to side touch and here's the, here's the means just hearing something.

83
00:16:27,840 --> 00:16:35,840
And these 19, that is to say, 9 casinos permitting the air casino and the 10 kinds of fallenness must be apprehended by sides.

84
00:16:35,840 --> 00:16:40,840
So, that means you look at something and practice meditation.

85
00:16:40,840 --> 00:16:46,840
The meaning is that in the early stage, the assigned must be apprehended by constantly looking with the eye.

86
00:16:46,840 --> 00:16:55,840
In the case of mindfulness occupied with the body, the five parts ending with skin must be apprehended by side and the rest by here, see.

87
00:16:55,840 --> 00:17:00,840
Head hair, body hair, new, steep skin.

88
00:17:00,840 --> 00:17:06,840
These, you look at with your eyes and practice meditation on them.

89
00:17:06,840 --> 00:17:22,840
And the others, some, some you cannot see like liver, intestines and other things, so that you practice through hearsay.

90
00:17:22,840 --> 00:17:25,840
Mindfulness of breathing must be apprehended by touch.

91
00:17:25,840 --> 00:17:33,840
So, when you practice mindfulness meditation, you keep your mind here and be mindful of the, the sensation of touch here.

92
00:17:33,840 --> 00:17:40,840
Be the air going, going in and out of the nostrils.

93
00:17:40,840 --> 00:17:50,840
The air casino by side and touch, it will become clearer when we come to the description of how to practice air casino.

94
00:17:50,840 --> 00:18:06,840
Sometimes you look at something moving, say a branches of tree or a banner in the wind and you, and you practice air casino on that.

95
00:18:06,840 --> 00:18:13,840
But sometimes the wind is blowing against your body and you have the feeling of touch here.

96
00:18:13,840 --> 00:18:20,840
So, to concentrate on this and concentrate on the wind, air element here.

97
00:18:20,840 --> 00:18:29,840
So, in that case, you practice by the sense of touch, the remaining 18 by hearsay, that means just by hearing.

98
00:18:29,840 --> 00:18:36,840
The divine abiding of equanimity and the four immaterial states are not applicable by a beginner.

99
00:18:36,840 --> 00:18:49,960
So, you cannot practice upika and the four are about over-j

100
00:18:49,960 --> 00:18:54,840
at the beginning because in order to get out of the barrajana, you must have, you must have called the five blue barrajana.

101
00:18:54,840 --> 00:19:04,840
In order to practice real upika, upika, brahma, we are and you have to practice.

102
00:19:04,840 --> 00:19:09,720
have to have practiced the first three loving kindness,

103
00:19:09,720 --> 00:19:12,240
confession, and sympathetic joy.

104
00:19:12,240 --> 00:19:21,240
So as a real divine abiding equanimity cannot be practiced

105
00:19:21,240 --> 00:19:22,880
at the beginning.

106
00:19:22,880 --> 00:19:25,280
Only after you have practiced the other three

107
00:19:25,280 --> 00:19:28,560
can you practice equanimity.

108
00:19:28,560 --> 00:19:34,800
And then as to condition, the 10 casinos

109
00:19:34,800 --> 00:19:39,040
I mean nine casinos, omitting the space

110
00:19:39,040 --> 00:19:42,800
casinos are conditions for immaterial states.

111
00:19:42,800 --> 00:19:46,280
That means if you want to get Aruba with drajanas

112
00:19:46,280 --> 00:19:51,120
and you practice one of the nine casinos

113
00:19:51,120 --> 00:19:53,000
omitting the space casino.

114
00:19:53,000 --> 00:20:01,800
Because you have to practice the removing of the casino

115
00:20:01,800 --> 00:20:04,440
object and get the space.

116
00:20:04,440 --> 00:20:07,120
So space cannot be removed to space space.

117
00:20:07,120 --> 00:20:12,480
So space casino is exempted from those

118
00:20:12,480 --> 00:20:15,560
that are conditions for immaterial states

119
00:20:15,560 --> 00:20:18,000
or Aruba with drajanas.

120
00:20:18,000 --> 00:20:23,480
The 10 casinos are conditions for the kinds of direct knowledge.

121
00:20:23,480 --> 00:20:26,840
So if you want to get the direct knowledge or Abbina,

122
00:20:26,840 --> 00:20:29,960
that means supernormal power.

123
00:20:29,960 --> 00:20:34,600
Then you practice first the one of the 10 casinos.

124
00:20:34,600 --> 00:20:41,240
And actually, if you want to get different results,

125
00:20:41,240 --> 00:20:47,840
then you practice different casinos.

126
00:20:53,000 --> 00:20:56,840
If you want to shake something, suppose you

127
00:20:56,840 --> 00:21:04,200
want to shake the city hall building

128
00:21:04,200 --> 00:21:08,080
by your supernormal power, then first,

129
00:21:08,080 --> 00:21:13,920
you must practice water casino or air casino,

130
00:21:13,920 --> 00:21:17,040
but not the earth casino.

131
00:21:17,040 --> 00:21:19,760
If you practice earth casino and try to shake it,

132
00:21:19,760 --> 00:21:22,200
it will not shake.

133
00:21:22,200 --> 00:21:25,920
There is a story of a novice who went up to heaven.

134
00:21:25,920 --> 00:21:28,080
I mean, the abode of cards.

135
00:21:28,080 --> 00:21:32,840
And he said, I will shake your mansion.

136
00:21:32,840 --> 00:21:35,920
And he tried to shake it, and he could not.

137
00:21:35,920 --> 00:21:41,600
So the celestial names make fun of him.

138
00:21:41,600 --> 00:21:44,840
And so he was ashamed, and he went back to his teacher.

139
00:21:44,840 --> 00:21:48,200
And he told his teacher that he was ashamed by the names,

140
00:21:48,200 --> 00:21:53,480
because he could not shake the dimension.

141
00:21:53,480 --> 00:21:55,240
And he asked his teacher why.

142
00:21:55,240 --> 00:21:59,320
So his teacher said, look at something there.

143
00:21:59,320 --> 00:22:04,480
So a cow done was floating in the river.

144
00:22:04,480 --> 00:22:06,000
So he got the hint.

145
00:22:06,000 --> 00:22:08,240
And so next time he went, he went back

146
00:22:08,240 --> 00:22:10,360
to the celestial abode.

147
00:22:10,360 --> 00:22:14,520
And this time he practiced water, I mean,

148
00:22:14,520 --> 00:22:16,480
water casino first.

149
00:22:16,480 --> 00:22:20,640
And then maybe maybe mansion shake.

150
00:22:20,640 --> 00:22:26,200
So according to what you want from the casinos,

151
00:22:26,200 --> 00:22:28,400
you have to practice different kinds of casinos.

152
00:22:28,400 --> 00:22:30,480
And we have to mention in a little chapter.

153
00:22:34,440 --> 00:22:39,200
So the 10 casinos are conditions for the kinds of direct knowledge.

154
00:22:39,200 --> 00:22:42,960
Three divine abodeings are conditions for the fourth divine abode.

155
00:22:42,960 --> 00:22:46,360
So the fourth divine abode can not be practiced at the beginning,

156
00:22:46,360 --> 00:22:49,600
but only after the first three.

157
00:22:49,600 --> 00:22:53,480
Each lower immaterial state is a condition for each higher one.

158
00:22:53,480 --> 00:22:56,040
The base consisting of neither perception or non-possession

159
00:22:56,040 --> 00:22:58,800
is a condition for the attainment of cessation.

160
00:22:58,800 --> 00:23:02,720
That means cessation of mental activities,

161
00:23:02,720 --> 00:23:09,520
cessation of perception and feeling,

162
00:23:09,520 --> 00:23:12,960
actually, cessation of mental activities.

163
00:23:12,960 --> 00:23:15,360
All our conditions were living in bliss.

164
00:23:15,360 --> 00:23:19,800
That means living in bliss in this very life for insight

165
00:23:19,800 --> 00:23:21,960
and for the fortunate kinds of becoming,

166
00:23:21,960 --> 00:23:26,440
that means for a good life in the future.

167
00:23:26,440 --> 00:23:29,920
As to suitability to temperament from the important,

168
00:23:29,920 --> 00:23:32,000
here the exposition should be understood according

169
00:23:32,000 --> 00:23:34,680
to what is suitable to the temperament.

170
00:23:34,680 --> 00:23:40,360
And then describe which subjects of meditation

171
00:23:40,360 --> 00:23:46,360
are suitable for which kind of temperaments.

172
00:23:46,360 --> 00:23:49,960
Can I just question?

173
00:23:49,960 --> 00:23:53,560
Is there three divine abodeings and conditions for the fourth?

174
00:23:53,560 --> 00:23:54,160
Yes.

175
00:23:54,160 --> 00:23:59,080
Does that mean which you are constructing with practice of third

176
00:23:59,080 --> 00:24:02,920
before the first or practice of the second or before the first?

177
00:24:02,920 --> 00:24:06,600
Do it doesn't say that you have to practice all of them.

178
00:24:06,600 --> 00:24:08,400
Right.

179
00:24:08,400 --> 00:24:19,120
But the normal procedure is to practice the loving kind as first

180
00:24:19,120 --> 00:24:23,280
and then practice the second one and the third one.

181
00:24:23,280 --> 00:24:28,080
And this means you practice so that you get

182
00:24:28,080 --> 00:24:31,440
Jana from this practice.

183
00:24:31,440 --> 00:24:35,800
If you do not get to the state of Jana,

184
00:24:35,800 --> 00:24:39,760
then even equanimity you can practice.

185
00:24:39,760 --> 00:24:48,240
But here it is meant for Jana because the equanimity

186
00:24:48,240 --> 00:24:50,440
leads to the fifth Jana.

187
00:24:50,440 --> 00:24:52,120
So in order to get the fifth Jana,

188
00:24:52,120 --> 00:24:56,080
you need to get the first and fourth Jana.

189
00:24:56,080 --> 00:25:00,200
And those can be obtained through the practice of the lower

190
00:25:00,200 --> 00:25:03,640
three or the other three divine abodeings.

191
00:25:03,640 --> 00:25:06,680
But you don't preach Jana from the other three.

192
00:25:06,680 --> 00:25:09,440
You reach Jana, but not to the fifth Jana.

193
00:25:09,440 --> 00:25:13,360
You reach to the fourth Jana, oh no.

194
00:25:13,360 --> 00:25:17,560
And by the practice of equanimity, you reach the fifth Jana.

195
00:25:21,760 --> 00:25:28,600
And now the different kinds of meditation,

196
00:25:28,600 --> 00:25:32,640
suitable for different types of temperaments.

197
00:25:32,640 --> 00:25:37,640
And all this has been stated in the form of direct opposition

198
00:25:37,640 --> 00:25:39,760
and complete suitability.

199
00:25:39,760 --> 00:25:43,960
That means if you say this meditation is suitable

200
00:25:43,960 --> 00:25:48,680
for this temperament, it means that this meditation

201
00:25:48,680 --> 00:25:50,920
is the direct opposite of that temperament.

202
00:25:50,920 --> 00:25:53,480
And it is very suitable for it.

203
00:25:53,480 --> 00:25:58,680
But it does not mean that you cannot practice other meditation.

204
00:25:58,680 --> 00:26:01,640
So but there is actually no profitable development

205
00:26:01,640 --> 00:26:04,280
that does not suppress, create, et cetera,

206
00:26:04,280 --> 00:26:06,280
and help for it, and so on.

207
00:26:06,280 --> 00:26:09,680
So in fact, you can practice any meditation.

208
00:26:10,760 --> 00:26:14,680
But here, the meditations,

209
00:26:14,680 --> 00:26:18,040
subjects of meditation and temperaments are given

210
00:26:18,040 --> 00:26:23,040
to show that they are the direct opposite of the temperaments.

211
00:26:26,120 --> 00:26:27,760
And they are the most suitable,

212
00:26:27,760 --> 00:26:32,760
suppose I am of diluted temperaments,

213
00:26:34,920 --> 00:26:39,160
then the most suitable meditation for me

214
00:26:39,160 --> 00:26:41,880
is breathing meditation.

215
00:26:41,880 --> 00:26:44,520
But that does not mean that I cannot practice

216
00:26:44,520 --> 00:26:45,760
any other meditation.

217
00:26:47,200 --> 00:26:49,760
Because any meditation will help me

218
00:26:49,760 --> 00:26:53,560
to suppress mental defilements

219
00:26:53,560 --> 00:26:58,040
and to develop wholesome mental states.

220
00:27:00,960 --> 00:27:04,080
Even in the sootas, Buddha was advising

221
00:27:05,200 --> 00:27:07,760
Rahul and other persons to practice

222
00:27:07,760 --> 00:27:09,000
different kinds of meditation,

223
00:27:09,000 --> 00:27:10,320
not only one meditation,

224
00:27:10,320 --> 00:27:13,640
but different kinds of meditation to one and only person.

225
00:27:13,640 --> 00:27:18,280
So any meditation can be practiced by anyone.

226
00:27:19,280 --> 00:27:21,600
But if you want to get the best out of it,

227
00:27:21,600 --> 00:27:25,360
then you choose the one which is most suitable

228
00:27:25,360 --> 00:27:26,560
for your temperaments.

229
00:27:31,840 --> 00:27:34,360
All casinas are as object,

230
00:27:34,360 --> 00:27:39,360
they imply seeing consciousness before.

231
00:27:41,320 --> 00:27:44,240
And I saw process.

232
00:27:45,760 --> 00:27:50,040
Yes, casinas should be practiced

233
00:27:50,040 --> 00:27:52,640
first by looking at them.

234
00:27:52,640 --> 00:27:54,320
So when you look at them,

235
00:27:54,320 --> 00:27:56,840
then you have to be seeing consciousness.

236
00:27:56,840 --> 00:27:58,480
And then you try to memorize,

237
00:27:58,480 --> 00:28:02,480
or you try to take it into your mind.

238
00:28:02,480 --> 00:28:04,360
That means you close your eyes

239
00:28:04,360 --> 00:28:07,400
and try to take that out, the image.

240
00:28:07,400 --> 00:28:11,160
So when you can get the image clearly in your mind,

241
00:28:11,160 --> 00:28:20,640
then you are set to get the, what sign?

242
00:28:20,640 --> 00:28:23,680
Learning, they call learning sign.

243
00:28:23,680 --> 00:28:28,040
Actually the cross sign.

244
00:28:28,040 --> 00:28:33,040
And after you get the learning sign,

245
00:28:33,040 --> 00:28:38,080
and you don't, you no longer need the actual disc,

246
00:28:38,080 --> 00:28:40,240
actual object.

247
00:28:40,240 --> 00:28:45,520
But you develop, you dwell on the sign,

248
00:28:45,520 --> 00:28:48,000
you get in your mind.

249
00:28:48,000 --> 00:28:51,120
So at that moment, at that time on,

250
00:28:51,120 --> 00:28:53,720
it is not seeing consciousness.

251
00:28:53,720 --> 00:28:58,240
But the other manorvara, I mean,

252
00:28:58,240 --> 00:29:04,680
through mind or not through idola.

253
00:29:04,680 --> 00:29:08,160
So first, through idola, you look at the,

254
00:29:08,160 --> 00:29:11,080
the kasina and practice meditation.

255
00:29:11,080 --> 00:29:14,160
And after you get the learning sign,

256
00:29:14,160 --> 00:29:19,120
then your meditation is through mind or,

257
00:29:19,120 --> 00:29:21,360
you see through mind, but not through the idol.

258
00:29:25,040 --> 00:29:28,320
And then, say,

259
00:29:31,320 --> 00:29:35,880
dedicating oneself to the blessed one or to the teacher.

260
00:29:35,880 --> 00:29:41,880
That means giving or rallying, pushing oneself.

261
00:29:41,880 --> 00:29:46,440
And to the blessed one or to the vora, it's all right.

262
00:29:46,440 --> 00:29:50,400
But to the teacher, I do not recommend.

263
00:29:54,240 --> 00:30:00,800
Because, not all teachers have to be trusted.

264
00:30:00,800 --> 00:30:07,080
So considering what's happening these days,

265
00:30:07,080 --> 00:30:11,680
so on the page 119, say,

266
00:30:11,680 --> 00:30:14,640
when he dedicates himself to a teacher,

267
00:30:14,640 --> 00:30:17,440
I run and quiz this, my person to you.

268
00:30:17,440 --> 00:30:20,240
So I give myself up to you.

269
00:30:22,920 --> 00:30:25,960
It may be dangerous if a teacher has,

270
00:30:26,960 --> 00:30:28,840
what are your motives?

271
00:30:28,840 --> 00:30:33,840
So it is better to give yourself to the vora,

272
00:30:33,840 --> 00:30:40,840
not to the teacher these days, okay.

273
00:30:41,840 --> 00:30:48,840
And then, sincere inclination or resolution,

274
00:30:48,840 --> 00:30:50,840
I think they are not so difficult.

275
00:30:50,840 --> 00:30:57,840
Now, in paragraph 128,

276
00:30:57,840 --> 00:30:59,920
about four lines.

277
00:30:59,920 --> 00:31:02,800
For it is one of such sincere inclination,

278
00:31:02,800 --> 00:31:07,160
which arrives at one of the three kinds of enlightenment.

279
00:31:07,160 --> 00:31:09,360
That means enlightenment as a vora.

280
00:31:10,320 --> 00:31:13,440
And enlightenment as a pachigaboda.

281
00:31:13,440 --> 00:31:15,800
And enlightenment as an aracham.

282
00:31:15,800 --> 00:31:18,040
So these are three kinds of enlightenment.

283
00:31:19,480 --> 00:31:22,400
And then, six kinds of inclination lead to the maturing

284
00:31:22,400 --> 00:31:25,120
of the enlightenment of the bodhisattvas.

285
00:31:25,120 --> 00:31:29,280
This may be something like parameters found in Mahayana.

286
00:31:35,560 --> 00:31:39,200
Is non-greet, non-delution, and so on.

287
00:31:39,200 --> 00:31:41,120
These are the six inclinations.

288
00:31:41,120 --> 00:31:44,800
I mean, six qualities,

289
00:31:44,800 --> 00:31:49,480
which bodhisattvas, especially develop.

290
00:31:50,720 --> 00:31:53,160
So with the inclination to non-greet,

291
00:31:53,160 --> 00:31:57,360
bodhisattvas see the foot, see the foot in gree.

292
00:31:57,360 --> 00:31:59,160
With the inclination to non-hate,

293
00:31:59,160 --> 00:32:01,640
bodhisattvas see the foot in hate and so on.

294
00:32:04,480 --> 00:32:08,520
They are not found, although it is a quotation,

295
00:32:08,520 --> 00:32:13,520
we cannot trace this quotation to any text available nowadays.

296
00:32:14,160 --> 00:32:16,880
So some text may have been lost.

297
00:32:16,880 --> 00:32:21,680
Or it may refer to some other,

298
00:32:21,680 --> 00:32:25,280
some soul that is not belonging to the tara vara.

299
00:32:31,680 --> 00:32:33,960
And then the last,

300
00:32:33,960 --> 00:32:36,000
bhara-grav-wanta-dhi-tu,

301
00:32:36,000 --> 00:32:38,000
apprehend the sign.

302
00:32:38,000 --> 00:32:41,840
Now, in Parli-dhi,

303
00:32:41,840 --> 00:32:44,880
the word nimitta is used in different, different meanings.

304
00:32:44,880 --> 00:32:47,720
So here, apprehend the sign means

305
00:32:47,720 --> 00:32:52,080
just paying close attention to what you hear from the teacher.

306
00:32:52,080 --> 00:32:54,000
So this is the preceding clause,

307
00:32:54,000 --> 00:32:55,640
this is the subsequent clause,

308
00:32:55,640 --> 00:32:58,320
this is the meaning, this is the intention,

309
00:32:58,320 --> 00:33:00,160
this is the simile and so on.

310
00:33:00,160 --> 00:33:03,120
So, carrying close attention and trying to understand

311
00:33:03,120 --> 00:33:07,520
first, hear the words of the teacher,

312
00:33:07,520 --> 00:33:09,440
and then trying to understand it is called

313
00:33:09,440 --> 00:33:12,000
here, apprehending the sign.

314
00:33:12,000 --> 00:33:13,840
So it is not like apprehending the sign

315
00:33:13,840 --> 00:33:15,960
when you practice meditation.

316
00:33:15,960 --> 00:33:18,080
So apprehending your sign will come later

317
00:33:18,080 --> 00:33:20,120
and chapter four.

318
00:33:21,440 --> 00:33:23,240
So here, apprehending the sign means

319
00:33:23,240 --> 00:33:28,240
just paying close attention to what the teacher said.

320
00:33:31,240 --> 00:33:32,560
So this is preceding clause,

321
00:33:32,560 --> 00:33:33,840
this is subsequent clause,

322
00:33:33,840 --> 00:33:35,600
this is its meaning and so on.

323
00:33:35,600 --> 00:33:37,400
When he listens attentively,

324
00:33:37,400 --> 00:33:39,280
apprehending the sign in this way,

325
00:33:39,280 --> 00:33:42,040
his meditation subject is well apprehended.

326
00:33:42,040 --> 00:33:47,040
So he knows, he knows what he should know about meditation

327
00:33:49,320 --> 00:33:53,040
and because of that he successfully attains distinction.

328
00:33:53,040 --> 00:33:58,520
Attains distinction means attains genres, attains

329
00:33:58,520 --> 00:34:02,680
supernormal knowledge and attains enlightenment.

330
00:34:03,760 --> 00:34:07,680
But not others, not otherwise, but not others.

331
00:34:07,680 --> 00:34:12,680
He will successfully attain this thinking distinction,

332
00:34:13,920 --> 00:34:17,800
but not others who do not listen attentively

333
00:34:17,800 --> 00:34:19,360
and apprehend the sign and so on.

334
00:34:19,360 --> 00:34:24,720
So otherwise you'd be corrected to others,

335
00:34:24,720 --> 00:34:29,720
not otherwise, on page 101, first line.

336
00:34:32,240 --> 00:34:34,440
He successfully attains distinction,

337
00:34:34,440 --> 00:34:38,360
but not others, on this page here.

338
00:34:46,320 --> 00:34:51,320
Okay, that is the end of that chapter.

339
00:34:51,560 --> 00:34:55,760
So we are still preparing.

340
00:34:55,760 --> 00:35:00,760
I'm preparing is not over yet.

341
00:35:07,840 --> 00:35:12,840
You have to avoid 18, 40 monasteries and all these things

342
00:35:12,960 --> 00:35:15,160
and find a suitable place for meditation.

343
00:35:15,160 --> 00:35:16,000
Okay.

344
00:35:24,520 --> 00:35:26,240
You have to understand about the parmitas

345
00:35:26,240 --> 00:35:28,760
that almost works.

346
00:35:29,760 --> 00:35:31,720
It works for four of them in the end of two,

347
00:35:31,720 --> 00:35:35,080
it's a little bit of a stretch, but interesting.

348
00:35:41,480 --> 00:35:42,600
Thank you very much.

349
00:35:42,600 --> 00:35:46,400
Thank you for, we're going to continue in March,

350
00:35:46,400 --> 00:35:50,640
sometime in March, we'll do another 12 weeks

351
00:35:50,640 --> 00:35:53,440
and that will go from, we hope,

352
00:35:54,840 --> 00:35:59,080
chapter four, chapter eight,

353
00:35:59,080 --> 00:36:01,840
chapter three, three, two, three, two, three, two,

354
00:36:01,840 --> 00:36:03,880
three, two, three, two, three, three, three, two, three.

355
00:36:07,760 --> 00:36:10,520
Is there something we can start reading in the meeting?

356
00:36:10,520 --> 00:36:13,360
Yes.

357
00:36:40,520 --> 00:36:41,360
Okay.

358
00:37:10,520 --> 00:37:11,360
Okay.

