1
00:00:00,000 --> 00:00:16,240
Welcome to my Q&A series.

2
00:00:16,240 --> 00:00:29,760
Today's question is in regards to dealing with the effects of meditation practice, specifically

3
00:00:29,760 --> 00:00:39,680
the unexpected, undesired unpleasant effects of meditation.

4
00:00:39,680 --> 00:00:50,080
So the questioner was asking about dealing with people when they feel unable to communicate

5
00:00:50,080 --> 00:01:01,080
with them after meditating, I feel strong states of concentration after meditating and overwhelming

6
00:01:01,080 --> 00:01:14,000
physical sensations, and how do you deal with those experiences and stop them from getting

7
00:01:14,000 --> 00:01:20,840
in the way of dealing with other people.

8
00:01:20,840 --> 00:01:37,120
So the first thing, of course, to mention is what we talk about is the fact that there

9
00:01:37,120 --> 00:01:44,560
are as many types of meditation as there are mind states.

10
00:01:44,560 --> 00:01:55,880
And so when you ask a question about meditation, the answer is going to be often highly dependent

11
00:01:55,880 --> 00:02:01,200
on the sort of meditation that you're practicing.

12
00:02:01,200 --> 00:02:13,440
There are types of meditation that will lead people to go insane, usually just temporarily,

13
00:02:13,440 --> 00:02:16,000
but it does happen.

14
00:02:16,000 --> 00:02:19,200
You know, meditation means training your mind.

15
00:02:19,200 --> 00:02:27,560
You can train your mind in ways that wind you too tight and drive you crazy.

16
00:02:27,560 --> 00:02:39,400
This meditation doesn't, but a lot of meditation does lead to overwhelming states and

17
00:02:39,400 --> 00:02:49,520
inhibiting states that get in the way of living your life in any ordinary way.

18
00:02:49,520 --> 00:02:55,320
So the first thing to check is your meditation practice itself.

19
00:02:55,320 --> 00:03:01,160
If you're having difficulty, it sounds like fairly extreme difficulty dealing with people

20
00:03:01,160 --> 00:03:07,360
after you meditate, not just feeling awkward or like you want to be alone, but actually

21
00:03:07,360 --> 00:03:15,520
physically unable to communicate, feeling that your brain is on fire, or so on.

22
00:03:15,520 --> 00:03:25,680
Too much concentration is a very common one.

23
00:03:25,680 --> 00:03:30,680
You want to check your technique, what technique are you practicing?

24
00:03:30,680 --> 00:03:34,840
Check the posture, are you just doing sitting meditation, or are you been walking as well,

25
00:03:34,840 --> 00:03:44,840
or just walking and getting help to break up some of those overly strong states of concentration.

26
00:03:44,840 --> 00:03:57,640
And also importantly, you want to check your progress in your direction in that meditation.

27
00:03:57,640 --> 00:04:02,240
It's the easiest way to do this, of course, is with the teacher.

28
00:04:02,240 --> 00:04:13,640
You have a teacher and it allows you to let go of some of the doubt and fear in favor

29
00:04:13,640 --> 00:04:18,680
of the psychological reassurance of someone who knows what they're doing and knows what

30
00:04:18,680 --> 00:04:19,680
you're doing.

31
00:04:19,680 --> 00:04:23,680
We don't know what we're doing, someone else who's a teacher often doesn't know what

32
00:04:23,680 --> 00:04:29,840
we're doing and can help us.

33
00:04:29,840 --> 00:04:38,200
But it also allows us to give up control in terms of controlling the direction of our

34
00:04:38,200 --> 00:04:48,880
practice, because self-directed practice is often like trying to perform surgery on

35
00:04:48,880 --> 00:04:53,480
yourself, for example, or let's say self therapy, for example.

36
00:04:53,480 --> 00:04:58,120
It is a sort of self therapy, right?

37
00:04:58,120 --> 00:05:07,760
It's like trying to see yourself without a mirror.

38
00:05:07,760 --> 00:05:13,600
Because we're biased, because we're too close to the situation, it's our situation.

39
00:05:13,600 --> 00:05:20,960
We often, with self-directed practice, we miss things, we're not objective.

40
00:05:20,960 --> 00:05:26,400
So a teacher giving control to the teacher, and then by control, I just mean, what am I

41
00:05:26,400 --> 00:05:27,400
going to do today?

42
00:05:27,400 --> 00:05:28,720
How am I going to react to this?

43
00:05:28,720 --> 00:05:30,440
What is the direction?

44
00:05:30,440 --> 00:05:32,640
What is the instruction?

45
00:05:32,640 --> 00:05:38,480
Having it come from someone else, especially someone who knows what they're doing, it

46
00:05:38,480 --> 00:05:46,280
makes a much more objective, and much less likely to lead to problematic states of mind.

47
00:05:46,280 --> 00:05:53,360
So all those things I would say probably address this person's concerns, and I would

48
00:05:53,360 --> 00:05:55,280
recommend taking a meditation course.

49
00:05:55,280 --> 00:06:00,040
We do online meditation courses kind of for this reason, because there's a lot of people

50
00:06:00,040 --> 00:06:07,000
who don't follow my videos and don't come to do meditation courses.

51
00:06:07,000 --> 00:06:12,600
So we do an online course, but they can at least get some direction and get some idea of

52
00:06:12,600 --> 00:06:18,160
a proper way to practice that doesn't have the potential to lead to extreme states.

53
00:06:18,160 --> 00:06:27,680
But the other thing about meditation, and I don't think it really is involved with this

54
00:06:27,680 --> 00:06:34,920
particular question, but it's important to note, is that I can't say that mindfulness

55
00:06:34,920 --> 00:06:40,000
meditation when practice properly doesn't interfere with your life.

56
00:06:40,000 --> 00:06:46,480
I think the conditions described by the person posing this question have a lot to do

57
00:06:46,480 --> 00:06:51,840
with how they were practicing and can be fixed, and I don't think mindfulness meditation

58
00:06:51,840 --> 00:07:00,160
has drastic impact on your interaction with other people, but that being said, it does change

59
00:07:00,160 --> 00:07:01,160
a lot of things.

60
00:07:01,160 --> 00:07:05,400
If you practice properly, it changes a lot of things about your life.

61
00:07:05,400 --> 00:07:13,840
The first one being ethics and morality, maybe the people you're surrounding yourself

62
00:07:13,840 --> 00:07:22,360
with are quite out of touch with the pure state of mind that comes from meditation, so

63
00:07:22,360 --> 00:07:27,000
when you interact with them, it can be quite distressing for both of you, really.

64
00:07:27,000 --> 00:07:28,000
Why are you a zombie?

65
00:07:28,000 --> 00:07:32,640
Why are you no longer exciting and entertaining and interested in drugs and alcohol and

66
00:07:32,640 --> 00:07:37,800
partying and music and sex and so on?

67
00:07:37,800 --> 00:07:41,640
Distressing to both sides.

68
00:07:41,640 --> 00:07:46,800
When you stop killing and stealing and lying and cheating and picking drugs and alcohol

69
00:07:46,800 --> 00:07:57,520
and let go of entertainment, reduce your entertainment and purification and your sexual

70
00:07:57,520 --> 00:08:03,720
and romantic desire to get along with the majority of the people in the world, so you

71
00:08:03,720 --> 00:08:12,960
do find yourself becoming more inclusive, life does change as a result of meditation, and

72
00:08:12,960 --> 00:08:17,400
so this person says, I don't want to go and live in a cave well, living in a cave would

73
00:08:17,400 --> 00:08:22,960
be great, you know, if you could, if you really barring all the problems that come from

74
00:08:22,960 --> 00:08:29,960
practicing alone, living in seclusion is a great thing and it's not something to be disregarded.

75
00:08:29,960 --> 00:08:34,240
When you complain about the trouble interacting with other people, you have to keep that

76
00:08:34,240 --> 00:08:40,080
in mind that meditation is not going to allow you to live your life as normal, it's

77
00:08:40,080 --> 00:08:43,200
going to change many things about your life.

78
00:08:43,200 --> 00:08:55,840
The upside is that what we claim, to believe it or not, as you like, is that the changes

79
00:08:55,840 --> 00:09:03,240
are natural and are based on the purity of mind that comes from meditation, I don't

80
00:09:03,240 --> 00:09:10,520
want to sound like a cult leader or that you have to give up all your old friends and become

81
00:09:10,520 --> 00:09:22,960
part of this meditation cult, but anything that changes your mind is going to change your

82
00:09:22,960 --> 00:09:27,000
environment as well, it's going to change the people you want to hang out with, so any type

83
00:09:27,000 --> 00:09:37,000
of meditation is going to do that, any type of mental cultivation is going to do that

84
00:09:37,000 --> 00:09:42,520
no matter what form it takes, if you decide to become a business person and you're going

85
00:09:42,520 --> 00:09:46,480
to change the people you hang out with, you're probably not going to hang around Buddhist

86
00:09:46,480 --> 00:09:53,480
meditators very much, or you won't hang out with people who are anti-capitalist for example,

87
00:09:53,480 --> 00:09:59,960
if you're a business person, so if you hang out, if you become a meditator, being around

88
00:09:59,960 --> 00:10:05,800
people who are not meditating, there's going to be some dissonance, some of them you'll

89
00:10:05,800 --> 00:10:12,120
be able to interact with, some of them not, so two sides to this problem I would say,

90
00:10:12,120 --> 00:10:20,360
simply laid out in that way, first there's the meditation side, changing your meditation,

91
00:10:20,360 --> 00:10:23,680
changing the meditation technique you do, sometimes maybe you're just doing a meditation

92
00:10:23,680 --> 00:10:30,560
technique that is simply forcing the mind into high states of concentration, sometimes

93
00:10:30,560 --> 00:10:37,480
that can be profitable if done properly, but it's also the kind of meditation that should

94
00:10:37,480 --> 00:10:49,320
be done in seclusion, in a cave or forest or mountain or something, because it doesn't

95
00:10:49,320 --> 00:10:54,560
translate well to dealing with people. On the other hand, mindfulness as a meditation

96
00:10:54,560 --> 00:11:03,000
practice does, translate fairly well with dealing with people, and it takes skill and

97
00:11:03,000 --> 00:11:10,120
training, but when you feel high states of concentration, you would note them, when you feel

98
00:11:10,120 --> 00:11:17,400
sweating, I think this person mentioned and something else hard to breathe, or I can't

99
00:11:17,400 --> 00:11:25,760
remember you would note all these sensations. When done properly, when done well, it doesn't

100
00:11:25,760 --> 00:11:33,440
need to get in the way of the day of the night, but also even once you've chosen a proper

101
00:11:33,440 --> 00:11:41,640
meditation technique, it has to be done properly, and so you often need guidance, you

102
00:11:41,640 --> 00:11:47,120
need to keep ethical precepts, but you're not, your meditation is going to get all messed

103
00:11:47,120 --> 00:11:55,640
up, so that's on the meditation side, and on the other side, recognizing that, and

104
00:11:55,640 --> 00:12:00,280
that your interactions with people are going to change most likely. Over time, you're going

105
00:12:00,280 --> 00:12:05,160
to be less interested and less interesting, but people are going to be less interested

106
00:12:05,160 --> 00:12:11,760
in you because you're no longer in the person. I mean, sometimes it's just the stress

107
00:12:11,760 --> 00:12:16,960
of change. Some good people will be still very shocked by the change, and sometimes

108
00:12:16,960 --> 00:12:22,280
distressed by the fact that you've changed until they realize that, or admit, hopefully

109
00:12:22,280 --> 00:12:28,000
that the change is for the better. They can, or then you have to ask yourself, is that

110
00:12:28,000 --> 00:12:31,720
because of them or because of me, maybe I'm not changing the better. And if you're not,

111
00:12:31,720 --> 00:12:36,360
well, you shouldn't continue doing that. This shouldn't be something that you put belief

112
00:12:36,360 --> 00:12:42,280
in, and people start seeing you as some kind of cult member. It should be something that

113
00:12:42,280 --> 00:12:48,600
you can see the benefit in, and hopefully eventually are able to show other people the benefit.

114
00:12:48,600 --> 00:12:53,240
What does it do? Does it make you a better person? Does it make you happier, more peaceful?

115
00:12:53,240 --> 00:13:01,960
Does it turn you into a mindless zombie? Does something sense it's going to be perceived

116
00:13:01,960 --> 00:13:10,600
as both? Because people who are deeply invested in entertainment and excitement and central

117
00:13:10,600 --> 00:13:14,280
pleasure, basically. They're not going to be entertained by you anymore. They're not

118
00:13:14,280 --> 00:13:26,400
going to be interested in you anymore. They don't light their fire anymore. But on the

119
00:13:26,400 --> 00:13:35,600
other hand, you'll become more, you should be able to be more people, and you'll meet

120
00:13:35,600 --> 00:13:45,080
and your bonds with people who are headed in positive direction, it's going to be better

121
00:13:45,080 --> 00:14:00,120
able to relate to good people. You'll be more pleasing and more more of a positive for

122
00:14:00,120 --> 00:14:13,360
people who are on a good path. If you can understand what is a good path, it depends on

123
00:14:13,360 --> 00:14:20,840
your understanding. For some people, meditation and mindfulness are not so interesting.

124
00:14:20,840 --> 00:14:33,760
We're going to find it hard to relate to. We just have to find those people who have an

125
00:14:33,760 --> 00:14:41,800
understanding similarly or that peace is good and contentment is good, the quiet and the

126
00:14:41,800 --> 00:14:54,080
quiet beneficial understanding oneself and finding the truth in life. I would say find

127
00:14:54,080 --> 00:15:00,440
a teacher, reassess her meditation practice, do some walking meditation, often a good

128
00:15:00,440 --> 00:15:11,240
care to those sorts of states. Don't worry too much about what other people think. We don't

129
00:15:11,240 --> 00:15:17,480
worry too much about fitting in with society because the Buddha said the world is blind.

130
00:15:17,480 --> 00:15:23,560
Unfortunately, you're not going to be able to please everyone. You can meditate. There's

131
00:15:23,560 --> 00:15:32,320
not going to suddenly make you look, you're peeling to everyone. So there's a lot of

132
00:15:32,320 --> 00:15:56,840
plans for today.

