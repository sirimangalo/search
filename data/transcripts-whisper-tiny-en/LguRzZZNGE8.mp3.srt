1
00:00:00,000 --> 00:00:08,720
Okay, whenever I attempt to meditate, I get distracted very easily, about five to ten minutes

2
00:00:08,720 --> 00:00:11,560
in, and I feel that I've gained nothing.

3
00:00:11,560 --> 00:00:18,760
How do I train myself to focus?

4
00:00:18,760 --> 00:00:28,040
Don't train yourself to focus, don't train yourself to focus.

5
00:00:28,040 --> 00:00:36,560
Bring yourself to focus on, first, the disappointment, that having gained nothing, because

6
00:00:36,560 --> 00:00:40,520
that's clinging, wanting something.

7
00:00:40,520 --> 00:00:47,880
You want to gain something that's just consumerism and capitalism, you put your capital

8
00:00:47,880 --> 00:00:52,920
in, you should get something out of it, it's kind of frustrating now, and you realize that

9
00:00:52,920 --> 00:00:58,600
meditation doesn't give you anything, gives you nothing.

10
00:00:58,600 --> 00:01:01,560
All this work you put in, you get nothing out of it.

11
00:01:01,560 --> 00:01:11,320
So focus on that, focus on this goal-based mind, this results-based mind, the deadlines

12
00:01:11,320 --> 00:01:21,360
and quotas that we're taught to, the bottom line, focus on that, is greed, this frustration

13
00:01:21,360 --> 00:01:23,720
and so on.

14
00:01:23,720 --> 00:01:27,200
Then once you've overcome that, then you can start to focus on the distraction and learn

15
00:01:27,200 --> 00:01:36,000
about the distraction, and by the time you do that, then your meditation should get better.

16
00:01:36,000 --> 00:01:40,480
Because you're actually learning a lot, during those five and ten minutes of distraction,

17
00:01:40,480 --> 00:01:44,440
you're learning a lot, you're learning how your mind works, you're learning that it's

18
00:01:44,440 --> 00:01:48,440
not under your control, you're learning three things, that it's impermanent, it's

19
00:01:48,440 --> 00:01:56,680
suffering, and it's non-self, it's unstable, it's unsatisfying, and it's uncontrollable.

20
00:01:56,680 --> 00:01:59,520
That's the nature of your mind, and the more you practice, the more you'll see that,

21
00:01:59,520 --> 00:02:03,520
after you practice for five to ten minutes, you'll actually be able to let go of things

22
00:02:03,520 --> 00:02:09,320
a lot better, or a little bit better, five to ten minutes better, or five to ten minutes

23
00:02:09,320 --> 00:02:11,560
worth.

24
00:02:11,560 --> 00:02:15,880
Your mind will be less inclined to try to force things because you will see that your

25
00:02:15,880 --> 00:02:22,080
mind is not subject to being forced, you'll see that you can't control your mind, you'll

26
00:02:22,080 --> 00:02:26,920
be more inclined to think, you'll be more inclined to let your lesson climb to make your

27
00:02:26,920 --> 00:02:33,400
mind wander, allow your mind to wander, lesson climb to daydream, and so on, because

28
00:02:33,400 --> 00:02:48,520
now you see that it just winds you up and gets you upset and leads to suffering.

