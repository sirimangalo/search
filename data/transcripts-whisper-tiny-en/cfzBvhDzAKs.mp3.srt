1
00:00:00,000 --> 00:00:14,680
Okay, so this is an interview I'd like to, because we're curious, and I think something

2
00:00:14,680 --> 00:00:23,400
that people could benefit from, in general, maybe people who are curious, or people who

3
00:00:23,400 --> 00:00:31,720
are interested, or people who are preparing even to make such a step.

4
00:00:31,720 --> 00:00:40,120
What is it that makes you come to this decision, sort of what salient points make you

5
00:00:40,120 --> 00:00:48,280
come to this decision of, which is really a, quite a decision that not only will you come

6
00:00:48,280 --> 00:00:53,640
to do meditation courses, or even stay at a monastery, but to shave your head and put on

7
00:00:53,640 --> 00:00:54,640
robes.

8
00:00:54,640 --> 00:01:00,240
What is it that, that he actually even interests you, or what's the benefit that you see

9
00:01:00,240 --> 00:01:03,280
in that?

10
00:01:03,280 --> 00:01:09,080
Because you've spent time in monasteries and meditations after, so as a layperson.

11
00:01:09,080 --> 00:01:12,280
That is a very good question.

12
00:01:12,280 --> 00:01:19,000
As you've told before, I've tried it out many times, and I can only recommend to everybody

13
00:01:19,000 --> 00:01:29,880
who's thinking about it to go to many retreats, think about this very deeply.

14
00:01:29,880 --> 00:01:40,080
I found myself making retreats, being very, very interested in ordaining, and then I

15
00:01:40,080 --> 00:01:50,480
step back many times, I found myself still thinking that out there is something that not

16
00:01:50,480 --> 00:01:59,400
as much to do, but clinging to many things, and wondering if I could make it, if I could

17
00:01:59,400 --> 00:02:04,720
actually live this life, and be happy with it, and I think this is one of the reasons

18
00:02:04,720 --> 00:02:09,280
why I want to ordain.

19
00:02:09,280 --> 00:02:17,000
As much as I try to have no expectations, because the expectations usually are not met

20
00:02:17,000 --> 00:02:25,480
the way you want them to, but when you mentioned shaving your head, I didn't think it

21
00:02:25,480 --> 00:02:30,520
was a big thing, but you do notice once you do it, it is a big thing, and another thing

22
00:02:30,520 --> 00:02:38,400
is where I find myself with a new name, my ego is somehow identified with another name,

23
00:02:38,400 --> 00:02:44,120
and this does create some confusion, something I have to deal with.

24
00:02:44,120 --> 00:02:48,320
I find it interesting, I find it very challenging, and I'm looking forward to whatever

25
00:02:48,320 --> 00:02:55,280
is coming, but is that what I want to say, I've spent three months from December till

26
00:02:55,280 --> 00:03:00,480
February here, and I thought that there was still something to do from where I come

27
00:03:00,480 --> 00:03:01,480
from.

28
00:03:01,480 --> 00:03:05,760
I'd really enjoy going back to Bird, and I thought it was going to be very exciting.

29
00:03:05,760 --> 00:03:12,840
We talked about it, and I found it totally uninteresting, and I couldn't wait to just

30
00:03:12,840 --> 00:03:17,800
finally stop that worldly wanting wanting.

31
00:03:17,800 --> 00:03:44,920
Well, okay, I don't have anything else to say.

