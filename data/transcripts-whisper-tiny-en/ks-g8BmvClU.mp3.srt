1
00:00:00,000 --> 00:00:11,520
If Nirvana is not arisen, how can consciousness take it as an object?

2
00:00:11,520 --> 00:00:15,040
To my understanding, it cannot.

3
00:00:15,040 --> 00:00:29,680
A person has to have a first glimpse of Nirvana before the consciousness can take

4
00:00:29,680 --> 00:00:31,720
Nirvana as an object.

5
00:00:31,720 --> 00:00:43,240
So a stream enter, for example, can take Nirvana as an object, but someone who has not had

6
00:00:43,240 --> 00:00:59,240
any experience whatsoever length cannot take Nirvana as an object for meditation, but

7
00:00:59,240 --> 00:01:04,200
bunt is back, I, uh, the question, I say, I got it.

8
00:01:04,200 --> 00:01:05,200
You got it?

9
00:01:05,200 --> 00:01:06,200
Okay.

10
00:01:06,200 --> 00:01:16,040
So I said my part, how can consciousness take it as an object?

11
00:01:16,040 --> 00:01:17,240
It's just a manner of speaking.

12
00:01:17,240 --> 00:01:23,400
I don't know what the poly is exactly that says, mine takes nibana as an object.

13
00:01:23,400 --> 00:01:29,000
See, this is a technical nitty gritty question that I have no idea about.

14
00:01:29,000 --> 00:01:31,920
What does the text, what did the text actually say?

15
00:01:31,920 --> 00:01:33,800
What did the abidama texts actually say?

16
00:01:33,800 --> 00:01:39,080
Because the objects of mine are called a Ramana.

17
00:01:39,080 --> 00:01:45,360
Now is nibana considered an a Ramana?

18
00:01:45,360 --> 00:01:49,240
Suppose it would be considered a dhamma Ramana, but it's just a word.

19
00:01:49,240 --> 00:01:56,560
It means the focus of the mind, where is the mind bent upon?

20
00:01:56,560 --> 00:01:57,880
What is the mind set upon?

21
00:01:57,880 --> 00:02:00,080
What has the mind averted to?

22
00:02:00,080 --> 00:02:01,880
In which direction has the mind gone?

23
00:02:01,880 --> 00:02:11,600
Has the mind connected with the eye or has it connected with the ear?

24
00:02:11,600 --> 00:02:21,720
Has it connected with the tongue, the nose, the tongue, the body or the heart, the thoughts?

25
00:02:21,720 --> 00:02:24,440
Or has it connected with non-arising?

26
00:02:24,440 --> 00:02:35,000
So it's really just a figure of speech, from a scientific point of view.

27
00:02:35,000 --> 00:02:49,960
And really the point is, as Sariputta said, as the Buddha taught, says the nibana is

28
00:02:49,960 --> 00:02:52,520
the non-arising of the five aggregates.

29
00:02:52,520 --> 00:02:56,160
So that's the non-arising of vinyana as well.

30
00:02:56,160 --> 00:02:59,560
Vinyana doesn't arise.

31
00:02:59,560 --> 00:03:09,320
So it's all just technical as to whether you say that the mind has taken nibana as an

32
00:03:09,320 --> 00:03:14,920
object because that mind hasn't arisen or the mind has entered into cessation is what

33
00:03:14,920 --> 00:03:17,240
you might say.

34
00:03:17,240 --> 00:03:21,000
And so in order to stay technically correct, you have to say that the mind is taking

35
00:03:21,000 --> 00:03:29,240
x as an object, but really all it means is the mind has come to a state of cessation during

36
00:03:29,240 --> 00:03:30,240
that time.

37
00:03:30,240 --> 00:03:34,840
There's no thinking, there's no awareness, there's no, ooh, this is what nibana feels

38
00:03:34,840 --> 00:03:35,840
like.

39
00:03:35,840 --> 00:03:38,960
No, there can't be otherwise, it's not nibana.

40
00:03:38,960 --> 00:03:44,120
If it ever comes that you have an experience that this is nibana or a realization, this

41
00:03:44,120 --> 00:03:48,360
is the experience of freedom of enlightenment, then it's not the experience of freedom

42
00:03:48,360 --> 00:03:49,360
of enlightenment.

43
00:03:49,360 --> 00:03:52,240
Because it's arisen.

44
00:03:52,240 --> 00:04:01,440
I am not sure where exactly I read it, but I'm very sure.

45
00:04:01,440 --> 00:04:14,160
I think it was Mahasizairo, he wrote that, or possibly somebody else wrote, that only

46
00:04:14,160 --> 00:04:23,400
a stream enter can take nibana as an object, not before, let's explain that clearly.

47
00:04:23,400 --> 00:04:31,520
A stream enter is the moment of the first moment of the realization of nibana.

48
00:04:31,520 --> 00:04:39,200
So all that means is that what it means to be a stream enter is someone who's seen nibana.

49
00:04:39,200 --> 00:04:43,720
So it's redundant to say that because it's the definition, the very definition of stream

50
00:04:43,720 --> 00:04:50,680
enter is one who has come to the realization of nibana.

51
00:04:50,680 --> 00:04:53,360
The first moment is magbanyana.

52
00:04:53,360 --> 00:04:56,160
The next moment in any subsequent moments are palanyana.

53
00:04:56,160 --> 00:05:00,800
So the first one is already a sotapana.

54
00:05:00,800 --> 00:05:06,720
The first moment of realizing nibana, that's called sotapati pala.

55
00:05:06,720 --> 00:05:13,040
I can't remember what the person is, sotapati, sotapati manga pugala or something like that.

56
00:05:13,040 --> 00:05:15,680
The person who has reached the path of sotapana.

57
00:05:15,680 --> 00:05:18,160
So for one moment, they're the first.

58
00:05:18,160 --> 00:05:23,520
They're actually, you know, this, the sotapati pano, you know what I'm talking about, at

59
00:05:23,520 --> 00:05:27,120
the police at pugala, they're actually eight people.

60
00:05:27,120 --> 00:05:29,800
So what are the eight holy people?

61
00:05:29,800 --> 00:05:34,680
The eight holy people is because the first moment, that person is different from the next

62
00:05:34,680 --> 00:05:36,040
moment.

63
00:05:36,040 --> 00:05:43,520
The first moment, the defilements are being cut off.

64
00:05:43,520 --> 00:05:46,760
And this is different from the second moment where the defilements are already cut off.

65
00:05:46,760 --> 00:05:50,880
So it's very technical and you have to know a little bit about abidama to understand

66
00:05:50,880 --> 00:05:55,600
it, but the first moment is a different person from the next moment.

67
00:05:55,600 --> 00:06:00,200
But the meaning is the moment of realizing nibana.

68
00:06:00,200 --> 00:06:05,040
So if an ordinary person did realize nibana, that would mean they're now sotapana.

69
00:06:05,040 --> 00:06:12,520
They're now in the first moment, sotapati manga, so that becoming of a sotapana is the

70
00:06:12,520 --> 00:06:15,760
realization of nibana is taking nibana as the object.

71
00:06:15,760 --> 00:06:22,240
But then from then on, when the person, when that person meditates as there is, that

72
00:06:22,240 --> 00:06:31,560
first glimpse of nibana can take nibana as an object for further meditations, that's

73
00:06:31,560 --> 00:06:33,760
how I understood the question.

74
00:06:33,760 --> 00:06:42,000
Not actually cannot actually take nibana as an object, can infer the piece of nibana.

75
00:06:42,000 --> 00:06:51,920
If you read, there's actually upasamanu city, which is reflection or recollection of

76
00:06:51,920 --> 00:06:52,920
peace.

77
00:06:52,920 --> 00:06:58,360
And the vesudimage, it's one of the ten annusates, upasamanu city, which really doesn't mean

78
00:06:58,360 --> 00:07:01,600
taking nibana as an object.

79
00:07:01,600 --> 00:07:05,760
But it has to be through inference, because you can't think back to what nibana is like,

80
00:07:05,760 --> 00:07:09,120
you can only think back to the piece that comes from it.

81
00:07:09,120 --> 00:07:17,600
And somehow refer back to it or connect with the idea of it, the state of your own

82
00:07:17,600 --> 00:07:20,480
piece that you've gained from nibana.

83
00:07:20,480 --> 00:07:23,280
So in that sense, yeah, only a sotapana.

84
00:07:23,280 --> 00:07:28,160
I remember reading through that about upasamanu city, and I think it's clear that only

85
00:07:28,160 --> 00:07:32,640
a sotapana can can practice that meditation.

86
00:07:32,640 --> 00:07:36,640
But the actual taking, that's not the taking of nibana as an object, the taking of nibana

87
00:07:36,640 --> 00:07:43,160
as an object would be entering into palesamapati, where the sotapana goes back and goes

88
00:07:43,160 --> 00:07:59,120
back to the 15th stage of knowledge, enters into palesamapati.

89
00:07:59,120 --> 00:08:19,160
Thank you.

