1
00:00:00,000 --> 00:00:12,440
Meaning, everyone, broadcasting live, a pro-second.

2
00:00:12,440 --> 00:00:24,520
Today's quote is about the unskillful, comes from a suit that talks about both the skillful

3
00:00:24,520 --> 00:00:26,160
and the unskillful.

4
00:00:26,160 --> 00:00:40,480
In fact, it's a bit of a paraphrase, I think, or a summary.

5
00:00:40,480 --> 00:00:57,040
Actually, I'm not sure if this is the proper, it actually is from this part of the city.

6
00:00:57,040 --> 00:00:58,840
Looks like it.

7
00:00:58,840 --> 00:01:04,880
Anyway, the suit just starts off with the Buddha saying there are three roots and wholesome

8
00:01:04,880 --> 00:01:33,480
that's a better translation here, because it's not, well, unskillful is a part of it, but it

9
00:01:33,480 --> 00:01:43,240
doesn't quite have the same feeling as unskillful is too cold and technical, but it's useful

10
00:01:43,240 --> 00:01:45,200
because that's really what we're talking about.

11
00:01:45,200 --> 00:01:46,200
What is unwholesome?

12
00:01:46,200 --> 00:01:47,520
What is bad?

13
00:01:47,520 --> 00:01:52,280
It's really just a matter of doing something unskillful, doing something that goes against

14
00:01:52,280 --> 00:01:56,680
your own interests, really.

15
00:01:56,680 --> 00:02:04,080
There are three things that, in the basis, three bases of roots, of things that go against

16
00:02:04,080 --> 00:02:13,960
our own interests that cause us to do and say and think things that are against whatever

17
00:02:13,960 --> 00:02:23,560
interests we may have, that cause us to, well, cause us to suffer, need us to have improper

18
00:02:23,560 --> 00:02:32,600
interests and interests that are self-conflicting in the sense we want them to bear positive

19
00:02:32,600 --> 00:02:34,960
fruit, they end up bearing negative fruit.

20
00:02:34,960 --> 00:02:41,840
So, three roots are those, low bandosa and moa.

21
00:02:41,840 --> 00:02:54,000
So, moa means desire to get one thing, those, I mean desire to harm or to hurt, or basically

22
00:02:54,000 --> 00:03:01,040
to not get desire to destroy, to eradicate, to do away with something, to change something,

23
00:03:01,040 --> 00:03:05,360
you don't like disliking, basically.

24
00:03:05,360 --> 00:03:14,920
Moa, moa is delusion, so anything like arrogance or conceit, self-righteousness, wrong

25
00:03:14,920 --> 00:03:24,720
view, ignorance, confusion, doubt, all these things, under the mind that is muddled, moa,

26
00:03:24,720 --> 00:03:32,880
moa, it's like muddled, it's the same, probably the same ancient root.

27
00:03:32,880 --> 00:03:43,560
So, then he talks about these three in detail, he says, moa, moa, moa is a coastal moa,

28
00:03:43,560 --> 00:03:50,200
whatever greed there may be that has, get up, you'll be coming, moa, whatever greed

29
00:03:50,200 --> 00:04:02,520
there is, that is the root of unwholesome, whatever the greedy one should do is body

30
00:04:02,520 --> 00:04:19,480
speech, or mind, or no, I'll be some conversation intent, not do right, or not to do that,

31
00:04:19,480 --> 00:04:31,400
yeah, right, with body speech, or mind, that is unwholesome, so the crucial and this

32
00:04:31,400 --> 00:04:38,520
should be fairly common knowledge to our community by now, but this is the crucial understanding

33
00:04:38,520 --> 00:04:51,280
of good and evil, how they exist in the mind, or they're based on the mind, they're based

34
00:04:51,280 --> 00:05:01,440
on the quality of the mind, not only acts themselves, and hence the, as I say many times,

35
00:05:01,440 --> 00:05:12,840
the importance of meditation in terms of changing these aspects of our mind, our reactions

36
00:05:12,840 --> 00:05:26,400
to things, to cultivate habits, of objectivity, or simplicity, it's very simple to see things

37
00:05:26,400 --> 00:05:32,120
as they are, it's not easy, but it's simple, if you can get to the point, to that simple

38
00:05:32,120 --> 00:05:39,680
state of just seeing things as they are, which seems actually kind of silly, because

39
00:05:39,680 --> 00:05:45,120
we're so used to complicating things, and we think truth must be complicated, but it's

40
00:05:45,120 --> 00:05:52,360
really not, truth is when seeing is seeing, it's such a powerful statement of the Buddha,

41
00:05:52,360 --> 00:06:04,320
it sounds like a child, it sounds childlike, but the idea is when you see, when seeing

42
00:06:04,320 --> 00:06:13,600
is just seeing, when there's no root around wholesomeness, there's no bad intention

43
00:06:13,600 --> 00:06:18,960
to say that you then could do bad things, or say bad things, or even think bad things

44
00:06:18,960 --> 00:06:25,160
is not possible, it's not correct, even though people might still blame you and probably

45
00:06:25,160 --> 00:06:33,920
will still blame you, the blame of others is not the reason for something being deemed

46
00:06:33,920 --> 00:06:41,280
wrong, questions, what makes something wrong, or what makes you right, and so Buddhism

47
00:06:41,280 --> 00:06:49,880
only goes so far as to say, it's right if it actually does induce to proper and to coherent

48
00:06:49,880 --> 00:06:57,160
goals, greed, anger, and delusion don't do that, but greed you want to be happy with

49
00:06:57,160 --> 00:07:04,240
anger you want to be free from suffering, with delusion you don't know what you want,

50
00:07:04,240 --> 00:07:11,960
but because greed and anger and delusion don't need to get what you want, instead they

51
00:07:11,960 --> 00:07:20,080
need to get what you don't want, not getting what you want, therefore they're unskillful,

52
00:07:20,080 --> 00:07:26,960
they're unhelpful, problematic, causing inherently causing problems, inherently problem

53
00:07:26,960 --> 00:07:41,080
causing, so these are the three, there's only three bases, and this is so when we

54
00:07:41,080 --> 00:07:46,720
meditate we also meditate on, directly on women, but as ever in your anger is in the mind

55
00:07:46,720 --> 00:07:54,520
you know this anger is in the mind, simply say to ourselves, angry, angry, not thinking

56
00:07:54,520 --> 00:08:02,120
of it as anything else, you want something wanting, wanting, and you confuse or deluded

57
00:08:02,120 --> 00:08:13,640
and you can see confused, confused, doubting, doubting, anxious, whatever, the kind of

58
00:08:13,640 --> 00:08:28,160
illusion it is, and then you make so similarly, it talks a little bit more, but in summary

59
00:08:28,160 --> 00:08:35,280
it gives a similarly of a tree, suppose there was a tree that was choked and enveloped

60
00:08:35,280 --> 00:08:44,120
by three mile of creepers, you know you think about vines, our country doesn't have

61
00:08:44,120 --> 00:08:51,280
vines really, but other countries they have vines that strangle and can actually take down

62
00:08:51,280 --> 00:08:59,160
trees, they go into the rainforest, it's amazing, they seek vines that not just cover

63
00:08:59,160 --> 00:09:06,080
the tree, just from, and it's just from like a seed, a bird goes up into the tree and

64
00:09:06,080 --> 00:09:13,200
defecates out a seed, and that one little seed ends up killing a huge, like a hundred

65
00:09:13,200 --> 00:09:22,400
foot tree, I mean I guess it usually doesn't kill the tree, but it can, and so here it

66
00:09:22,400 --> 00:09:30,520
talks about these mile of creepers that if the tree was choked with the creepers it would

67
00:09:30,520 --> 00:09:39,760
meet with calamity with disaster, this is like a, remember the last nights or the recent

68
00:09:39,760 --> 00:09:46,840
one, they said the mind is luminous, but the, the filaments will visit the mind, so this

69
00:09:46,840 --> 00:09:51,680
is the, he's giving it the same teaching in another way, he says there's nothing wrong

70
00:09:51,680 --> 00:09:57,320
with the tree, but it's these creepers, when the creepers enter into the tree they,

71
00:09:57,320 --> 00:10:03,360
they strangle it, and so our desires are like that, the desires are reversed, are unholes

72
00:10:03,360 --> 00:10:11,440
and roots, are, have like a stranglehold and we become slaves, they become slaves to

73
00:10:11,440 --> 00:10:20,240
our defilements, that's the idea, they can't act other than one thing, certain things

74
00:10:20,240 --> 00:10:26,440
and angry about certain other things, feeling confused or deluded about other certain

75
00:10:26,440 --> 00:10:33,280
thing, and we're controlled by this, our actions are speech and our thoughts are

76
00:10:33,280 --> 00:10:40,080
under control of these habits, basically, we've just built up habits, reinforced them

77
00:10:40,080 --> 00:10:53,840
with our emotions, with the power of our, our mental activity.

78
00:10:53,840 --> 00:11:11,080
Or choked like a tree by the creepers, and then you've got, you've got another tree,

79
00:11:11,080 --> 00:11:15,760
then you can keep as the opposite, which is common in the suit, the first half is about

80
00:11:15,760 --> 00:11:21,280
the unholesome, and that's I think where I'm quoted from, the second half is, but a person

81
00:11:21,280 --> 00:11:28,480
who has abandoned unholesome qualities born of greed, anger, and to the future, cut them

82
00:11:28,480 --> 00:11:34,320
off at the root, made them like a palm stem, obliterated them so that they are no more subject

83
00:11:34,320 --> 00:11:40,160
to future arising, and I'll find this one.

84
00:11:40,160 --> 00:11:57,720
But he is, you know, being as abandoned, right, or eliminated destroyed.

85
00:11:57,720 --> 00:12:08,160
Uchinamula, it's cut the root, mula again is root, this idea of cutting the root is important

86
00:12:08,160 --> 00:12:13,680
in Buddhism, because some people will say, well, enlightenment is Buddhism is to be free

87
00:12:13,680 --> 00:12:17,440
from the filement, free from craving, right?

88
00:12:17,440 --> 00:12:19,920
No craving, no suffering, but that's wrong.

89
00:12:19,920 --> 00:12:24,200
We see the manga points out that that's wrong because, well, we don't always have craving,

90
00:12:24,200 --> 00:12:28,040
so then at times when I don't have craving, does that mean I'm enlightened?

91
00:12:28,040 --> 00:12:33,960
No, it doesn't.

92
00:12:33,960 --> 00:12:40,040
So there's, and it's important because there are people who feel like they're, well,

93
00:12:40,040 --> 00:12:44,680
maybe I'm, you know, impamined enlightened because I don't feel any greed.

94
00:12:44,680 --> 00:13:00,160
There was a story of this ascetic who was so, had such powerful meditation and that

95
00:13:00,160 --> 00:13:08,080
he could fly through the air, and then one day he went to stay with this king, and every

96
00:13:08,080 --> 00:13:13,360
day he would go in for arms, the king invited him to stay, so it was one of the Jatakas

97
00:13:13,360 --> 00:13:22,680
Eddie, and the king was so happy to have this ascetic there and very powerful ascetic

98
00:13:22,680 --> 00:13:31,080
who was like, like he was enlightened, right?

99
00:13:31,080 --> 00:13:32,720
But one day the king had to go on business, and so he told the queen to serve the ascetic,

100
00:13:32,720 --> 00:13:40,160
and so they said it came into the room to get arms, and the queen had fallen asleep waiting

101
00:13:40,160 --> 00:13:45,120
for him, and so when she heard him, she stood up, and actually stood up, her robe fell

102
00:13:45,120 --> 00:13:51,120
off, and she was naked, and she was actually quite, but she was quite, you know, as

103
00:13:51,120 --> 00:13:58,600
a beautiful witch, she was considered in a worldly sentence to be beautiful.

104
00:13:58,600 --> 00:14:05,200
And so he was, you know, he was just a hadn't, I've never seen something, and it really

105
00:14:05,200 --> 00:14:07,400
is familiar, isn't it?

106
00:14:07,400 --> 00:14:13,800
And I think the text is like it came up like a snake, his desire, and bit him.

107
00:14:13,800 --> 00:14:22,600
Yes, in that moment he was bitten, and totally infatuated with his queen, and normally

108
00:14:22,600 --> 00:14:27,840
when he came to get the arms he would fly in through a window, and then fly back out

109
00:14:27,840 --> 00:14:32,840
through the window, but he found this time after he took the food from her that he had

110
00:14:32,840 --> 00:14:41,840
to walk up the door, and ended up getting very sick, he couldn't eat, he couldn't think

111
00:14:41,840 --> 00:14:49,000
of anything except the queen, so he ended up lying sick on his bed, and the king came back

112
00:14:49,000 --> 00:14:54,440
and found out what happened, went and told the queen, and the queen said, I've got

113
00:14:54,440 --> 00:15:00,560
an idea, we'll solve this problem, and so they called the ascetic, or they went to see

114
00:15:00,560 --> 00:15:06,960
the ascetic, and the king said, I understand what the problem is, and I'm going to give

115
00:15:06,960 --> 00:15:12,920
you my queen as your wife, so you can go and be a layperson, and I'll give you a house,

116
00:15:12,920 --> 00:15:17,400
and I'll give you a salary, and a job, and everything you need.

117
00:15:17,400 --> 00:15:20,520
You can have what you want, we appreciate you so much.

118
00:15:20,520 --> 00:15:26,280
Anyway, this story goes on that the queen ended up giving him so much work to do, and

119
00:15:26,280 --> 00:15:33,240
telling him, do this, do that, and nagging him, and which he does, he sent him back to the

120
00:15:33,240 --> 00:15:37,280
palace to fetch this, fetch that, and he ended up spending all the time just fetching

121
00:15:37,280 --> 00:15:42,280
them into finally she grabbed him by his beard, because the ascetics of course had blown

122
00:15:42,280 --> 00:15:53,120
beard, and said, wake up, and gave him a shock, and said, look at you, you used to be able

123
00:15:53,120 --> 00:15:58,600
to fly through the air, you used to wield magical power, and now you're reduced to a servant,

124
00:15:58,600 --> 00:16:09,960
messenger boy, I'm going to be ashamed, so the point is that there's a lot of meditation

125
00:16:09,960 --> 00:16:16,080
that can make you think that you're enlightened, and even this meditation, you'll get a certain

126
00:16:16,080 --> 00:16:20,440
way, and then you'll think wow, I really accomplished something, and it's easy to

127
00:16:20,440 --> 00:16:38,880
over, and esteem your practice, overestimate yourself, Talawatukata having made like a

128
00:16:38,880 --> 00:16:50,040
palms, the bread like a Talawat, something like that, and number one kata having made me to not exist,

129
00:16:50,040 --> 00:17:00,200
I attend a nupada dhamma to make it not a rise, to not have any a rise in the future,

130
00:17:00,200 --> 00:17:07,760
today what dhamma is, this is referring to the defileman, someone who has made these

131
00:17:07,760 --> 00:17:14,120
through enlightenment, see enlightenment makes it so that these things can't arise, and

132
00:17:14,120 --> 00:17:21,360
it's not like a somewhat of practice where you can suppress them, make it seem like they're

133
00:17:21,360 --> 00:17:27,080
not going to arise, now this is enlightenment means wisdom, through understanding the things

134
00:17:27,080 --> 00:17:33,720
that would normally cause a reaction, and understanding them, understanding them as impermanent,

135
00:17:33,720 --> 00:17:39,400
suffering as long as so, the things that we meditate on, understanding even just the rising

136
00:17:39,400 --> 00:17:45,880
and falling of the stomach, if you understand it, and learn to see it just as rising,

137
00:17:45,880 --> 00:17:50,880
you can become enlightened just on that, all it takes is really understanding that there's

138
00:17:50,880 --> 00:17:57,600
oh yeah, it's just arises and ceases, once you can look at things that way, then these

139
00:17:57,600 --> 00:18:02,560
things, there's no, you find no reason to get angry, no reason to be greedy, there's

140
00:18:02,560 --> 00:18:07,880
nothing to want or like, there's nothing to be angry, upset about.

141
00:18:07,880 --> 00:18:19,680
And then did they would dhamma, so kang vi, Harati, a vigatang, a nupaya, so, a bhary daha, they

142
00:18:19,680 --> 00:18:26,800
dwell such a person dwells in happiness in this present reality, a vigatang they are

143
00:18:26,800 --> 00:18:42,720
unvextering, and annoyed and distressed, a nupaya sang, and a bhayasu is sorrowless,

144
00:18:42,720 --> 00:18:56,400
parilahan, unfievered, parilah has a fever, so this is common, whatever you call it,

145
00:18:56,400 --> 00:19:02,080
the common word, the parilah has the fevers, so we have mental fevers, they're likened

146
00:19:02,080 --> 00:19:08,040
to a fever, greed is like having a fever because it's like being sick, anger is like

147
00:19:08,040 --> 00:19:12,640
being sick, it is being sick, so they're going to call them fevers.

148
00:19:12,640 --> 00:19:23,960
The tehvadhamme, parilah nibaya teh, nibaya teh, one is fully freed or extinguished in this

149
00:19:23,960 --> 00:19:33,800
very reality, the tehvadhamme, the teh means that which is seen dhamma is the dhamma, so you

150
00:19:33,800 --> 00:19:50,120
can see here and now basically, it's such a person, it's like a person who, suppose there

151
00:19:50,120 --> 00:19:57,120
was a tree that was strangled by three creepers, this is the three development, then the

152
00:19:57,120 --> 00:20:01,800
man would come along bringing a shovel and a basket, he would cut down the creepers at their

153
00:20:01,800 --> 00:20:02,800
roots.

154
00:20:02,800 --> 00:20:08,360
I did this once in Thailand, I didn't do it actually because we're not allowed to cut

155
00:20:08,360 --> 00:20:19,880
them, but I kind of hinted at the problem when one of the laymen cut the vine, I don't

156
00:20:19,880 --> 00:20:24,720
remember how I did it, but I didn't actually say cut that vine, kind of went around it

157
00:20:24,720 --> 00:20:30,160
because we were concerned that if the tree fell over, if the tree died and fell over, it

158
00:20:30,160 --> 00:20:36,080
hit one of the buildings and it turned out that there was no problem with the vine, they

159
00:20:36,080 --> 00:20:40,440
actually, some of them have symbiosis with the tree, so someone told me it was ashamed

160
00:20:40,440 --> 00:20:47,040
to cut the vine, anyway, they do this, you cut and then dig it up, pull out the roots,

161
00:20:47,040 --> 00:20:52,640
so what did I do, not only does he cut down the tree, creeper, but he dug them up, pulled

162
00:20:52,640 --> 00:20:58,880
out the roots, even the fine root that's in root fiber, he would cut the creepers into pieces,

163
00:20:58,880 --> 00:21:04,040
split the pieces and reduce them to slivers, so basically, I don't know, and then it's not

164
00:21:04,040 --> 00:21:08,520
even done, then he would dry the slivers in the wind and sun, burn them in the fire,

165
00:21:08,520 --> 00:21:15,240
reduce them to ashes, and wind of the ashes in a strong wind, that means blow them in

166
00:21:15,240 --> 00:21:22,800
the wind, I'll let them be carried away by the swift current of a river, basically totally

167
00:21:22,800 --> 00:21:29,920
overboard, that absolutely, the point is absolutely did a very end, gotten to the point

168
00:21:29,920 --> 00:21:38,880
where you've completely removed and the potential for regrowth, and so then you could

169
00:21:38,880 --> 00:21:42,760
say they were cut off at the root, made like a palm study, but obliterated so that they

170
00:21:42,760 --> 00:21:45,240
are no more subject to future rise.

171
00:21:45,240 --> 00:21:55,400
This is a new Good Durnic I am, Book of Three's, and that's how these discourses go,

172
00:21:55,400 --> 00:21:59,640
the Buddha gives a teaching and they're just listed, it's a wonderful book, a good

173
00:21:59,640 --> 00:22:04,360
Durnic I is very much worth, and I couldn't read it through it, but skimming and dipping

174
00:22:04,360 --> 00:22:12,120
into, it's enough for a lifetime, the Buddha's teaching is not like the Bible or something,

175
00:22:12,120 --> 00:22:16,760
most people don't even read the Bible, but so much more the Buddha's teaching, how could

176
00:22:16,760 --> 00:22:22,680
you read it all, most people don't have to have the time, but there's so much, it's like

177
00:22:22,680 --> 00:22:30,560
an ocean, diving down in the ocean and you come across such wonderful things that surprise

178
00:22:30,560 --> 00:22:53,280
you, anyway, that's the dhamma for tonight, thank you all for tuning in, okay I'm looking

179
00:22:53,280 --> 00:23:05,400
at the chat now, I've just put the hangout in, Monday is back, I'm Monday, Simon read

180
00:23:05,400 --> 00:23:14,320
my essay, it was very nice, good, that's good to hear, thing about writing essays, I mean

181
00:23:14,320 --> 00:23:21,600
it was for school, right, it was so, it was more an exercise of getting back into the habit

182
00:23:21,600 --> 00:23:34,440
or even refining the ability to actually write a scholarly paper, so that's something

183
00:23:34,440 --> 00:23:44,640
I've never really been that good at, so anything in the heart's future that goes against

184
00:23:44,640 --> 00:23:50,000
the Buddha's teaching, I don't even remember as the heart's future, which means the

185
00:23:50,000 --> 00:24:16,000
heart's future, that's the Buddha, I think that's the root angshunya tana, right, sorry

186
00:24:16,000 --> 00:24:34,640
root angshunya tana, shunya tana, root angshunya tana, so what I take, what I take, what's

187
00:24:34,640 --> 00:24:43,600
the word, take issue with is, it's not the dhamma, the dhamma is that form is void, not

188
00:24:43,600 --> 00:24:51,600
form is voidness, and I know, I'm absolutely, anything I could say about this is going

189
00:24:51,600 --> 00:25:02,400
to be derided or just rejected by Mahayana, they say oh no, because they have very sophisticated

190
00:25:02,400 --> 00:25:08,680
arguments to explain why form is voidness or what is meant by that, but as I said to

191
00:25:08,680 --> 00:25:14,000
me it's office and yeah, yeah, talk all you want, it's not the truth, form is void, it's

192
00:25:14,000 --> 00:25:19,000
the teaching, that's an important teaching, because it's void of self, it's void of

193
00:25:19,000 --> 00:25:27,720
pleasure, it's void of stability, let the heart's future, it does something that is not

194
00:25:27,720 --> 00:25:36,040
the Buddha's teaching, so, you know, you can quote me on that, you know, couldn't you interpret

195
00:25:36,040 --> 00:25:41,440
this in a darmic way, sure, probably, but look, I mean, where does it go, sir, good to

196
00:25:41,440 --> 00:25:48,000
the carers of voidness, of all darmers is non-arising, non-ceasing, non-defiled, non-pure,

197
00:25:48,000 --> 00:25:53,560
non-ceasing, non-defiled, non-duer, it's, it's eel replaying, it's like neither this nor

198
00:25:53,560 --> 00:25:59,680
that, in void, there is no form feeling constant, so in the void, well that's kind of

199
00:25:59,680 --> 00:26:08,000
it, we're talking about nimbana, then in the void, but then look, it's kind of that

200
00:26:08,000 --> 00:26:16,480
weird, no ignorance and also no ending of ignorance, no old age and death, and no ending

201
00:26:16,480 --> 00:26:22,000
of old age and death, I mean, yeah, yeah, I could write something like this, a six-year-old

202
00:26:22,000 --> 00:26:36,840
could write something like this, sorry, that's not going to be taken real up, therefore

203
00:26:36,840 --> 00:26:43,760
the gradual permeant is the great magic spell, I think the irony is the word, right, so

204
00:26:43,760 --> 00:26:48,240
the idea is that if you chant this, it's a protection, which we do in ours, and then there's

205
00:26:48,240 --> 00:26:52,840
this weird thing at the end, getting a tape, where I got tape, where I sang at table,

206
00:26:52,840 --> 00:27:02,040
which is a spell, and maybe people like these, this is not high, or high religion, this

207
00:27:02,040 --> 00:27:10,240
is low religion, people like these, in Thailand they have all these, gota, gota, this means

208
00:27:10,240 --> 00:27:15,560
verse, but in Thailand it becomes a, it has a different meaning, and gota is a magic

209
00:27:15,560 --> 00:27:23,240
spell, of course. So, if you say he got, gota, gota, gota dot a Inventory, people believe

210
00:27:23,240 --> 00:27:24,880
that's powerful.

211
00:27:24,880 --> 00:27:35,440
Anyway, sorry to trash talk it but I don't value this, it's not valuable to me, it's

212
00:27:35,440 --> 00:27:44,920
very short, huh?

213
00:27:44,920 --> 00:28:03,960
Should meditators have a pet, they don't want to get into it, but now I think meditators

214
00:28:03,960 --> 00:28:07,400
should not have pets, they are just a burden.

215
00:28:07,400 --> 00:28:11,720
I mean, I've given talks on this, so again this is why I don't answer questions in

216
00:28:11,720 --> 00:28:18,240
text because most of the questions you ask, I've given videos on this again and again

217
00:28:18,240 --> 00:28:26,880
the same thing, but briefly if you've got an animal or if you're going to take care of an

218
00:28:26,880 --> 00:28:32,560
animal, that's fine, but for someone to say, hey, I'm going to go get a pet, a bad idea.

219
00:28:32,560 --> 00:28:42,360
It's just a burden, especially for meditators, it ends up causing distraction and it's

220
00:28:42,360 --> 00:28:43,360
a long answer.

221
00:28:43,360 --> 00:28:48,720
I've talked about this and not even agrees with the things I say, but the part of the

222
00:28:48,720 --> 00:28:52,840
thing is you're trying to help a being that is very difficult to help, there's only so

223
00:28:52,840 --> 00:28:58,160
much you can do for an animal, and if you're going out of your way to do that, you

224
00:28:58,160 --> 00:29:03,200
say, hey, I'll go to the pound and pick up an animal or something.

225
00:29:03,200 --> 00:29:09,200
It's all about efficiency, it's end up spending so much time and money and effort and

226
00:29:09,200 --> 00:29:14,440
you get attached, of course there's all the defilements associated with pet, and for very

227
00:29:14,440 --> 00:29:23,440
a little benefit, as opposed to Michael who's here now from America, so I put my effort

228
00:29:23,440 --> 00:29:27,840
into him and it's actually very little effort and he works very hard and I just have

229
00:29:27,840 --> 00:29:30,400
to teach him and the benefit is awesome.

230
00:29:30,400 --> 00:29:36,360
His life could change from this, we'll have an interview when he's done the course and

231
00:29:36,360 --> 00:29:40,120
hopefully he'll be able to do that and you can all hear what he has to say.

232
00:29:40,120 --> 00:29:47,320
So Michael's here, he's planning on sticking around for some months, hopefully he can

233
00:29:47,320 --> 00:29:53,360
act as a steward for a while, it's from America so he can't stay long term, or maybe he

234
00:29:53,360 --> 00:30:18,680
can, maybe we can work better, but he'll stay somewhere better, but hey Ken, you've joined

235
00:30:18,680 --> 00:30:35,680
the hang of, do you have a question for me, your life, do I know you, it looks familiar,

236
00:30:35,680 --> 00:31:02,000
you're muted again, okay, not in that case, I'm going to say good night, I'm looking

237
00:31:02,000 --> 00:31:08,000
at the night everyone, we said you might get tomorrow at, I don't remember when it

238
00:31:08,000 --> 00:31:34,000
's two o'clock right, tomorrow at two, good night.

