1
00:00:00,000 --> 00:00:26,680
I don't know if everybody could have a turn on me, but I'm so starting tomorrow, I won't

2
00:00:26,680 --> 00:00:36,120
be giving talks and phone a clump. At this point, you're getting more focused on your

3
00:00:36,120 --> 00:00:48,000
practice. I hope that you start to want to rely more and more on your own experience. Of course,

4
00:00:48,000 --> 00:00:58,920
we meet every day to talk one-on-one if you have any questions. I don't have a lot more to say,

5
00:00:58,920 --> 00:01:07,240
if you do a lot of information and direction. At this point, it's about refining your

6
00:01:07,240 --> 00:01:27,520
practice from deeper. So, I thought I'd be always teaching from the Goethe or Nikayan book of

7
00:01:27,520 --> 00:01:35,240
sixes. It just means the part of the difficultness, the list of six things, but there's

8
00:01:35,240 --> 00:01:47,320
one list as they kind of appropriate at this point. It's called the Mahantatasutat. It's

9
00:01:47,320 --> 00:02:01,440
just change the Knee of the Kimming. The Mahantatas say, we're pulling the Tana-ari

10
00:02:01,440 --> 00:02:17,560
There are the six things, and down with these six things, and I'm equal in someone who

11
00:02:17,560 --> 00:02:26,080
has become aware of the need to free themselves from the dangers of clinging.

12
00:02:26,080 --> 00:02:41,320
When I down with these six qualities in no long time, they will attain to behind that

13
00:02:41,320 --> 00:02:54,560
Mahantah, which is two words, Mahantah, and the way you pull up, Mahantah means great,

14
00:02:54,560 --> 00:03:08,000
Mahantah, and greatness, and the way you pull up that means depth, or profanity, and the

15
00:03:08,000 --> 00:03:13,160
way under that what we're doing here is what you're all practicing is.

16
00:03:13,160 --> 00:03:22,680
Something quite special, quite normal, very deep and profound.

17
00:03:22,680 --> 00:03:28,560
This isn't a hobby, something that you do on the side, and actually affect effects,

18
00:03:28,560 --> 00:03:38,560
in a very positive way, in the very essence of who you are, purifying your habits,

19
00:03:38,560 --> 00:03:45,360
bringing you from some of your bad habits, so much stress and suffering along the way.

20
00:03:45,360 --> 00:03:54,560
This isn't just an ordinary thing, so something that leads to greatness, it leads

21
00:03:54,560 --> 00:04:12,320
to a profound sense of familiarity and understanding and wisdom about the world of reality.

22
00:04:12,320 --> 00:04:20,600
So there are six qualities that can take with you, and keep in mind as you practice.

23
00:04:20,600 --> 00:04:28,800
There's a support for your journey on the path.

24
00:04:28,800 --> 00:04:48,520
The first is aloka long ago, having a lot of light, brightness, brilliance.

25
00:04:48,520 --> 00:04:56,600
And by brilliance, the Buddha means wisdom, but use of the imagery of light, being

26
00:04:56,600 --> 00:05:05,120
full of light, gives a sense of how the Buddha understood wisdom, wisdom is clarity,

27
00:05:05,120 --> 00:05:11,560
it's brightness, it's like turning on a light in a dark room.

28
00:05:11,560 --> 00:05:18,560
So the wisdom that you need is not something I can give you, or you can read in books.

29
00:05:18,560 --> 00:05:29,520
The wisdom that you need is the clarity of understanding the experience moment moment.

30
00:05:29,520 --> 00:05:38,320
It relates directly to your experience of problems, difficulties, challenges, and whether

31
00:05:38,320 --> 00:05:46,240
they have power over you, whether you're dependent on how it becomes dependent on situations

32
00:05:46,240 --> 00:05:47,240
and circumstances.

33
00:05:47,240 --> 00:05:55,680
Whether you're under the power of the change of light, it's about the clarity of having

34
00:05:55,680 --> 00:06:03,560
the person's very much the core of what we're working towards in our cultivation of

35
00:06:03,560 --> 00:06:04,560
mindfulness.

36
00:06:04,560 --> 00:06:15,280
It's a big part of it is breaking up, pretty and allotino, seeing permanence, seeing

37
00:06:15,280 --> 00:06:23,960
that the experiences we thought, stayed with us, day to day, moment to hour to hour,

38
00:06:23,960 --> 00:06:38,680
are really just momentary, seeing the experiences are unpredictable, seeing how our expectations

39
00:06:38,680 --> 00:06:46,160
of stability lead to stress when things change where I'm prepared, where I'm accepting

40
00:06:46,160 --> 00:07:00,880
of change, this understanding, the clarity of permanence of reality is free, it's a part

41
00:07:00,880 --> 00:07:09,800
of the depth and the greatness, suffering, seeing the suffering involved with cleaning,

42
00:07:09,800 --> 00:07:16,440
mostly because of the environment, the reality was permanent, quite into anything, it's

43
00:07:16,440 --> 00:07:22,840
pleasant, but it's not perfect, and non-self, you can't control it, it can't change

44
00:07:22,840 --> 00:07:32,440
this, you can't prevent things from coming or going, changing, just seeing clearly these

45
00:07:32,440 --> 00:07:38,640
things, this is wisdom, this is wisdom, you don't need to go looking for them, it's

46
00:07:38,640 --> 00:07:55,160
like opening your eyes, turning on the light, a look about them, number two, I joke about

47
00:07:55,160 --> 00:08:04,760
a look, I think a lot of yoga, yoga is an interesting word, it's coming to me one thing

48
00:08:04,760 --> 00:08:15,840
in water, speech, yoga means something I'm committed, it's described in the commentaries

49
00:08:15,840 --> 00:08:21,960
as referred to effort, but effort is a bit, can be a bit misleading to put it up a lot

50
00:08:21,960 --> 00:08:33,640
of effort, right, we need to communicate through effort, we free ourselves from suffering,

51
00:08:33,640 --> 00:08:39,920
but commitment is an interesting word, and it describes the sort of effort that's important,

52
00:08:39,920 --> 00:08:44,960
in some ways you don't have to push hard, you just have to not give up, if you're stubborn

53
00:08:44,960 --> 00:08:57,040
enough to keep trying, pace you enough to stick with challenges and difficulties, it changes

54
00:08:57,040 --> 00:09:03,560
something about your outlook, because we're not looking for results in the sense of

55
00:09:03,560 --> 00:09:11,440
fixing our problems, we're looking for a new perspective, we're trying to become a person

56
00:09:11,440 --> 00:09:20,040
who is patient, who is able to experience without reacting, so we have pain, it's not

57
00:09:20,040 --> 00:09:25,760
about getting rid of the pain, pain is a problem we have to fix at all, pain is an experience

58
00:09:25,760 --> 00:09:34,640
if you understand it, and see it clearly, and it's not a problem, so the energy, the effort

59
00:09:34,640 --> 00:09:41,000
that you need is to not give up, you'll often find yourself giving up, feeling like, how

60
00:09:41,000 --> 00:09:47,600
you fail, sometimes you want to sit for an hour, you just can't do it, so effort, the

61
00:09:47,600 --> 00:10:00,640
best sort of effort, it doesn't quit, it doesn't quit, I failed, and then you try again,

62
00:10:00,640 --> 00:10:06,640
sometimes failing is a very important part of succeeding, because we can push and push and

63
00:10:06,640 --> 00:10:11,280
stress, and when you give up, it helps you let go, and you're able to try again with a

64
00:10:11,280 --> 00:10:20,280
fresh perspective, but when you give up, or when you give in, when you give up with the

65
00:10:20,280 --> 00:10:30,120
practice, that's what you see, you only fail to check it out, so the effort to be patient,

66
00:10:30,120 --> 00:10:43,800
it's an effort, and you can prevent it again, yoga, it's commitment, number three, wait

67
00:10:43,800 --> 00:10:48,520
at all, you have to have a lot of weight, that will lead to your meaning, feel it, you

68
00:10:48,520 --> 00:11:01,480
have to have a lot of feeling, and here the meaning is feeling for it, you have to feel

69
00:11:01,480 --> 00:11:13,400
like doing it roots in English, do it with feeling once again with feeling, and put your

70
00:11:13,400 --> 00:11:28,400
heart into it, quite many times in our practice, it's going, this is going a bit of challenge,

71
00:11:28,400 --> 00:11:33,640
we can find ourselves discouraged, you probably fail, especially in the ad home course

72
00:11:33,640 --> 00:11:38,760
when you're practicing in daily life, you can start to, in the beginning, resent the

73
00:11:38,760 --> 00:11:44,760
practice, just like having to do it, do so much walking and sitting, and you come here,

74
00:11:44,760 --> 00:11:49,220
it can be the same, you have pain, and so the hours that you have to do start to

75
00:11:49,220 --> 00:12:05,760
cry on, sometimes, this is a reminder of the issue in those realities, that there's

76
00:12:05,760 --> 00:12:09,960
standing between us and the practice, because in order to succeed, you really have to

77
00:12:09,960 --> 00:12:17,360
get into it, you have to have china, you have to in a sense want to do it, it's not

78
00:12:17,360 --> 00:12:24,800
exactly wanting your craving, but you have to feel it, you have to feel like doing it, and

79
00:12:24,800 --> 00:12:28,800
so the best advice I can give you is of course not to force yourself, pretend that you

80
00:12:28,800 --> 00:12:34,520
want to do it, but to address those things that are standing in your way, the dislike,

81
00:12:34,520 --> 00:12:42,400
the boredom, because they are not default, they are not caused by any experiences, experiences

82
00:12:42,400 --> 00:12:53,760
are boring, experiences are challenging, we react, our reactions are reality, you get

83
00:12:53,760 --> 00:12:59,280
become bored, and the boredom is all that's standing in the way between you and peace,

84
00:12:59,280 --> 00:13:05,760
you and contentment, a person sitting up on the mountain peacefully meditating on a

85
00:13:05,760 --> 00:13:13,080
mountain side, can either be content or bored, and the only thing that's standing in between

86
00:13:13,080 --> 00:13:19,080
them and the peace of the tranquility of being up on a mountain all alone, it's the

87
00:13:19,080 --> 00:13:26,640
boredom, craving for something else, you give those things up if they take those things

88
00:13:26,640 --> 00:13:35,480
away, what you're left with this piece, so a reminder that if you're not into it, you

89
00:13:35,480 --> 00:13:41,840
have to be conscious of those ingredients, you're standing in your way that are preventing

90
00:13:41,840 --> 00:13:47,080
you from being okay with what you're doing, especially when you're doing something so great,

91
00:13:47,080 --> 00:13:57,240
so helpful, so beneficial, as I think you all have seen through your practice, that's

92
00:13:57,240 --> 00:14:07,040
it, number three, number four, asymptotee bahamu, and this is an interesting one, the Buddha

93
00:14:07,040 --> 00:14:14,800
says you have to have a lot of discontent, which I've been talking about about contentment,

94
00:14:14,800 --> 00:14:20,880
and it's an important teaching, there are two kinds of contentment, there's contentment

95
00:14:20,880 --> 00:14:30,400
with your experiences, in the sense of not being averse to what you experience, your

96
00:14:30,400 --> 00:14:37,000
experience in contentment, regardless of what you get, what you don't get, the obvious

97
00:14:37,000 --> 00:14:45,040
example is contentment with food, contentment with large changes, contentment with clothing,

98
00:14:45,040 --> 00:14:52,360
contentment with medicine, sort of the things that we all have to do with our normal cities.

99
00:14:52,360 --> 00:14:57,640
Contentment with the practice, whether the mean discontent, as I've said, but being full

100
00:14:57,640 --> 00:15:04,000
of discontent here is the other kind of discontent, in the sense of not being content

101
00:15:04,000 --> 00:15:12,800
with your wholesome qualities, never being content with how far you've come on with

102
00:15:12,800 --> 00:15:18,080
on the kind of, a sign of greatness is someone who seeks to better themselves, even when

103
00:15:18,080 --> 00:15:26,320
they're an okay person, a sign of greatness is to focus not on your successes, but focus

104
00:15:26,320 --> 00:15:34,800
on what is yet to be done, to never be contentment, the Buddha wants admonished a group

105
00:15:34,800 --> 00:15:40,720
of honor balance, who work in ten things like, you know, we've done a lot now, we've become

106
00:15:40,720 --> 00:15:45,880
honored, which is a very high level of enlightenment from the city or have done yet, don't

107
00:15:45,880 --> 00:15:48,280
be content, don't be complacent.

108
00:15:48,280 --> 00:15:56,880
So it's a good reminder to it, a son could be, don't focus on your accomplishments.

109
00:15:56,880 --> 00:16:04,720
You can pick at them as like a rat, and when you have a rat, you use it to cross a river

110
00:16:04,720 --> 00:16:06,720
or lake or whatever.

111
00:16:06,720 --> 00:16:12,200
When you get to the other side, you don't pick it up and carry it with you, and you put

112
00:16:12,200 --> 00:16:15,840
it down and you continue on your way.

113
00:16:15,840 --> 00:16:21,880
What you gained in the practice is like a rat, it was great, it helped you get to where

114
00:16:21,880 --> 00:16:26,680
you are now, but don't carry it around with you, it just becomes a burden because it

115
00:16:26,680 --> 00:16:35,080
distracts you, and it makes you complacent, slows you down.

116
00:16:35,080 --> 00:16:44,240
Number five, Anikita Duro, Anikita Duro, relationship commitment, Anikita means not giving

117
00:16:44,240 --> 00:16:52,640
up, not putting down, but here it's much more, I think, in the sense of continuous practice,

118
00:16:52,640 --> 00:16:53,640
right?

119
00:16:53,640 --> 00:17:00,240
One of the fundamentals of mindfulness practice, if you read in a booklet, is a continuing

120
00:17:00,240 --> 00:17:10,280
continuation, continuity, so that you're practicing constantly, and don't put it down, Anikita

121
00:17:10,280 --> 00:17:13,760
Nikita means where you put it down, you take a break.

122
00:17:13,760 --> 00:17:19,560
It's a great thing about this sort of technique, and the teachings in this, that you

123
00:17:19,560 --> 00:17:23,120
can plan a surface of the Buddha, so when you're drinking, you can practice, when you're

124
00:17:23,120 --> 00:17:29,600
eating, you can practice, when you're urinating and death-icating, you can practice, and

125
00:17:29,600 --> 00:17:34,680
everything you do, and you're speaking, you can even be mindful of your lips moving, hearing

126
00:17:34,680 --> 00:17:43,360
your sound of your own voice, and feeling your voice pops, rattle, then I do encourage

127
00:17:43,360 --> 00:17:51,400
you to not talk very much, again, as we said, only as this is, trying to be mindful

128
00:17:51,400 --> 00:17:56,640
throughout your day, don't put it down even for a moment, it doesn't mean you have to

129
00:17:56,640 --> 00:18:03,440
be walking and sitting for as many hours as you can, when you take breaks, just be mindful

130
00:18:03,440 --> 00:18:08,800
when you're drinking, when you're eating, when you're walking, when you're sitting, even

131
00:18:08,800 --> 00:18:14,400
when you're lying down at night, trying to do lying meditation.

132
00:18:14,400 --> 00:18:09,080
I think I don't have to be more dishall

133
00:18:09,080 --> 00:18:22,160
of me, and I can see many of you, and all of you are engaged in this, and this is

134
00:18:22,160 --> 00:18:28,160
should be great encouragement to you, that you're fulfilling this, and an important aspect

135
00:18:28,160 --> 00:18:34,960
of practice that really does outline to you, success, and you can do a little, don't

136
00:18:34,960 --> 00:18:40,080
put it down even for a moment.

137
00:18:40,080 --> 00:18:52,040
And number six, kusoleus utamis utamis utamis utamis, make efforts to reach higher and

138
00:18:52,040 --> 00:19:00,720
higher kusum, to always be going to the next step.

139
00:19:00,720 --> 00:19:11,600
This is, I guess, related to contentment, it means going stage by stage, going from one

140
00:19:11,600 --> 00:19:26,000
hostess to the next, so relax to the others, we're being content, working to attain

141
00:19:26,000 --> 00:19:38,720
higher and higher levels of kusumis, so in a sense of trying to refine your practice,

142
00:19:38,720 --> 00:19:47,360
so not being content again with accomplishments in regards to problems you might have,

143
00:19:47,360 --> 00:19:52,560
so when you come to be, when you first come to practice, you see your course to follow,

144
00:19:52,560 --> 00:19:59,920
you'll see some major issues you might have of greed and greed division, as you practice,

145
00:19:59,920 --> 00:20:09,600
the idea here is to go further, this often takes the form of trying to engage in new practices,

146
00:20:10,320 --> 00:20:17,440
so an example for a moment would be deciding to practice under a tree, like sleep under the tree,

147
00:20:17,440 --> 00:20:21,920
and no longer sleep in a quick day, or to decide to only eat one meal.

148
00:20:28,720 --> 00:20:35,680
There are many, many practices, some months will decide to not never lie down, ever, day or night,

149
00:20:36,480 --> 00:20:42,720
it's kind of thing, so thinking to yourself, what can I do more, what more can I do,

150
00:20:42,720 --> 00:20:48,000
and striving to attain higher and higher refinement into your practice?

151
00:20:48,800 --> 00:20:53,920
Again, it relates to the others about working and going further,

152
00:20:54,720 --> 00:21:00,480
with this involves seeing the idea of practicing new things, which is a part of why we give you

153
00:21:00,480 --> 00:21:06,080
exercises everyday, it's like if you're doing weightlifting, weightlifting, training you

154
00:21:06,080 --> 00:21:13,360
every day, or every week, okay, don't have a 20 lift for this week, and you add another 10 pounds,

155
00:21:15,600 --> 00:21:22,640
every day I give you a two more touching point, and this is an interest of helping you to

156
00:21:22,640 --> 00:21:30,480
gain greater and greater than someone. So these are the six Mahantatatas,

157
00:21:30,480 --> 00:21:38,000
they've been to greatness and they've been to perform the team, recommended by the Buddha

158
00:21:38,000 --> 00:21:47,280
himself, I sort of last group parting and give to you, remind you, the sorts of things to focus

159
00:21:47,280 --> 00:21:52,560
on from here on in, and we'll still meet again tomorrow at the same time when you're starting

160
00:21:52,560 --> 00:22:02,800
to work again. So that's the number of work today. Thank you.

