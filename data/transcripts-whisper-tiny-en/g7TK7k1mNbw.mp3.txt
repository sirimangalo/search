Okay, good evening, everyone.
Welcome to our evening demo session live here from Hamilton, Ontario, Canada.
So tonight, let me go on and talk about the third discourse of the Buddha.
It's not actually the third discourse, but it really is the third discourse that we have recorded chronologically,
as far as I can think of.
So to recap, the Buddha gave his first discourse,
which was called the Turning of the Wheel of the Dhamma.
So at that point, he had said in motion this knowledge, right?
He had given this to the world.
There was no turning back.
Not that there was any desire to turn back, but at that point, the cat was out of the bag and so to speak.
It was inevitable that Buddhism would spread because the teaching is powerful.
The power was in the realization of the five monks, which ensured that it wouldn't be spread.
Five days later, he gave the second discourse, and they all became our hunt,
which means they became enlightened.
They were no longer subject to rebirths and freed themselves from all the foundment.
And they spent the three months of the reigns in Isipatana.
Isipatana was this, some kind of grove, some kind of park,
where religious people hung out ascetics, maybe.
And during the reigns, there was a man called Yasa who was very rich.
His parents were very rich.
And so he lived in the lap of luxury and had everything he could ever want.
One night, and he lived in Varanasi, which is near Isipatana, was just outside of Varanasi,
which was a big city in India, still to this day.
The ancient city, the city of Gaia, really.
I know the city of Varanasi, but the river Ganges, it's where the Ganga River is.
Yasa, I'm thinking of, it's also called Yasa, he used to me.
And so one night, Yasa wakes up in the middle of the night,
and he looks around his palace, and he sees his consorts,
the women who are meant to please him as a very, very rich man.
It's concubines, maybe. I don't know, just dancing girls, maybe.
And he sees the mall sleep, and they look like corpses.
And he gets this strong sense of death.
This is what it's like to die.
It's curious, I can empathize.
I had this sort of feeling myself when I was young.
Everyone would go to sleep, and I think they were all dead.
Makes one wonder whether this is a echo of that of our experiences of death,
which of course, very strong, and would be one of the stronger memories
that might carry over with us in some form.
Anyway, he became quite distraught, left his palace or his mansion,
and wandered into Isipatana, and wandered out of the city, went for a walk,
and ended up in Isipatana, muttering to himself.
This place is, and this world is crazy,
and this world is all messed up.
And the Buddha heard him and said to him,
yes, that come here. Here it is not messed up.
Here it is not confused.
It is not entangled.
And yes, I heard this voice, and so he went to the Buddha,
and he sat down in the Buddha taught him the Dharma,
and he became Isotapana.
And his parents came looking for him, found him, found the Buddha,
the Buddha taught them, and they became Isotapana,
and while he was teaching them, yes, I became an Aran.
And he heard the Dharma the second time.
And then yes, his friends,
54 of his friends became monks.
They heard that yes, I had gone forth,
and left the home life, because as an Aran,
he became a monk right away.
And they heard this, and so 54 of them,
he had a large society in the city,
became monks just on his reputation.
And they all became our hands as well.
So at that time, there was the Buddha,
plus 5, plus 55.
No, the Buddha, plus 5.
That was 55, so there was 61.
At that time, 61 are our hands in the world.
And then the Buddha, at the end of the rains,
he said to the monks,
Jaraata, Bhikkhui,
go forth, monks,
for the benefit of all,
for the benefit of the many, Bhauchana,
Sakaya, Bhikkhuchana, Hitaya.
But don't go by the same road.
He said, go separate, go your separate ways,
meaning in order for this to spread,
you're all enlightened,
there's no need for you to depend on anyone,
so be a refuge to others.
And he sent them all in 60 different directions.
Pretty powerful.
That was the start of the dispensation of the Buddha.
And the Buddhist missionaries,
and the people say Buddhism is a missionary religion,
which isn't really,
it's a bit misleading because these monks didn't go out
and knock on people's door and say,
hey, have you heard the good word?
They would just go and live in various places.
For example, Asaji left.
And when Sariputa saw him,
he just saw him.
He knew right away that this was someone
with listening to, and you came and listened to his teaching.
That happens today in Buddhism.
You know, monks don't have to go to their way to teach.
There's so many people interested in learning.
People are teaching out there.
Many people, not everyone.
Most people don't, but there are always people who want to hear.
Always people who will come and ask and learn and ask to learn.
And the Buddha himself went to Uruwehla.
And the Uruwehla, he met these.
Well, first why he was going to Uruwehla,
is he wanted to go to Rajagaha to meet Bhisara.
And Bhimisara was the king of Rajagaha,
and so he knew that this would be a great place to start.
Spreading his teaching a great place to live in,
Bhimisara would be a great ally,
which, of course, eventually was.
Because what I want to see was perhaps too much entrenched
in Brahmanism, and it would be very difficult
and there would be too much conflict.
It's like a general nose that he has to retreat
and build up his forces because there were only 60 of them.
And so he wanted to start.
He wanted to come in with a bang.
But he knew that if he went to Rajagaha,
there would be similar situation.
And so instead of going directly to Rajagaha,
he went to Uruwehla,
where the three most famous ascetics were living.
It was just outside of Rajagaha.
And I guess with the idea,
we guessed with the idea that if he converted these three,
then everyone not just Bhimisara,
but the whole of the kingdom of Rajagaha
would know how powerful
or how great the Buddha's teaching was.
And so he converted these three ascetics through various ways
and all of their disciples,
which ended up being 500 followers of the oldest brother.
There were the three brothers, the 500 followers of him,
the second youngest brother, the second brother, 300 followers,
and the youngest brother, 200 followers.
All together are 1,000 monks.
And then he took them up to the Gaiya sisap on the hill,
outside of Gaiya.
And he taught them the fire sermon.
And he taught them the dita paryayasud.
And to recap, the first one was about the four noble truths,
the eight full noble path.
That was about the content of it.
The second discourse is a little bit more technical and detailed,
so it's on the five aggregates.
The third discourse is on the six senses.
And so it's good to talk about these two ways of looking at reality.
The five aggregates and the six senses are really a good way together
to sum up a reality according to Buddhism,
or according to Teravada Buddhism.
So the six senses are the base.
Every experience we have is going to be based on one of these six senses.
Some experiences will be visual, some will be audible,
some will be nasal, olfactory,
some will be smell, some will be taste,
some will be feelings on the body and some will be thoughts.
It's just saying that there are many different kinds of experience,
these are the doors by which experience arises,
but each one of those experiences is made up of the five aggregates in various forms.
So at each of the six senses, every moment of experience or every series,
cognitive series that describes an experience,
is made up of the five aggregates.
So when you see something, there is the light,
then there is the eye, and that's root, but it's the first anchor.
There's the feeling of neutral about the object,
or if you like it, there might be a happiness about it.
If it's beautiful, it's ugly, there might be an unhappiness.
Although, technically, it's all neutral,
because when the light touches the eye, there's no pleasureable
until you cognize it.
So there's some technicality there, but basically speaking,
if it's beautiful, there is going to be a pleasure involved with that.
And that's talked about in this suit, we'll get into it.
Then there's recognition, you see something, and you recognize it.
That's sunya. You think about it, you like it,
you dislike it, that's sunkara.
And just the fact that you experience it,
the awareness of it, the ability to see at all
is windy and nice and it's consciousness.
So what these things do, I mean, it's not even all that useful.
I don't think to get really technical and try to say,
oh, this is this, which aggregate is it or so on.
But what this does by mapping this out and giving us a framework,
it's what it's not.
It's not the way we ordinarily look at reality.
I mean, there's no people, places, things involved here.
This provides a framework, a cage, so to speak,
within which we are able to fix our,
what's the word epistemology, I think,
or how we cultivate knowledge,
our awareness, so that we don't look at things
in terms of people, places, and things.
We look at our experiences, what is reality?
It's this.
You hear my voice, that sound, that's an experience of sound,
it arises in cases.
You see my face, that's an experience of seeing.
You smell whatever you smell, you feel something on the body,
you taste when you eat your food.
Every thought you have,
each one of these is a different kind of experience,
but they're all experiences.
So without really getting into it,
he sets the stage right off the bat,
first for them to look at reality in this way,
and why it's useful is because it takes us out of our heads.
There's a real difference, people, places, and things,
whether they exist or not philosophically.
When you're concentrated, the effect of,
on the mind, of thinking of things conceptually,
is in the abstract.
It's all up in your head.
So you think about people.
You're not experiencing them.
What you're experiencing is seeing,
and then there's the interpretation,
so it's always a step removed from reality.
As a result, you never really get a sense
of what's really going on now.
What's going on behind the scenes?
What am I doing to myself?
You never are clearly aware of the states
that you're cultivating.
When you focus on experiences rather than people,
places, and things, you do get a clear understanding
of what you're doing.
Am I benefiting myself in the way I'm acting
or speaking or thinking,
or am I harming myself?
Am I doing harm?
Is this pure?
Is this impure?
What are my motivations?
What's going on at this moment?
When you focus on this framework
that the Buddha taught,
the six senses and the five aggregates,
you really do get an understanding
of what's going on,
how your mind is working,
what you're doing right,
what you're doing wrong,
what you're doing to hurt yourself,
what you're doing to benefit yourself.
So that's the start of this,
that's the background of this,
but it's the fire sermon,
and so it is somewhat specific.
I mean, it's the fire sermon,
because these guys were fire worshipers.
So the Buddha turns this on its head
and says, look, you want to talk about fire?
Everything's on fire.
Sabang bikouwe, aditang.
Everything is on fire.
And what do I mean by everything or what everything?
Well, and this is where he pulls in this paradigm
and pulls them into the paradigm.
He says, the eye is on fire.
We chant this.
We used to chant it at doi suthep.
It was really great.
Doi suthep is the only place
that I've been that does all these suthep.
Chakou bikouwe, aditang,
rupang,
rupa,
aditang,
jakouinang,
aditang,
jakou sampas,
so aditou.
The eye is on fire.
The form is on fire.
Eye consciousness is on fire.
A contact with the eye is on fire.
His burning.
Whatever arises based on the contact,
the contact here means contact between the consciousness
and the body.
So when the eye and the light touch
and the consciousness goes there,
there's contact that's how experience arises.
Whatever arises based on that,
whether it be pleasant, painful,
or neither pleasant or painful,
so when you see something beautiful,
it becomes pleasant or feeling arises.
See something ugly,
you don't like unpleasant feeling arises.
Most of the time, a neutral feeling arises.
All of that is on fire.
All of that is burning.
So this is what he says.
So he's doing two things.
First he's saying,
what is all?
What is everything?
Everything is not the universe,
the solar system and so on.
Everything is seeing,
hearing, smelling, tasting, feeling,
thinking it really is.
That's all there is.
And then the second thing he's doing
is saying, there's something wrong.
This is where fire is.
You're telling,
you guys are sitting here worshiping fire.
There's a fire that you're ignoring.
And there's two kinds of fire.
He goes into.
He says, list of them,
but they can be separated into two parts.
So the first kind of fire is
defilement,
raga, gina,
with the fire of anger,
with the fire of delusion,
their own fire,
their burning.
So these are considered fire in Buddhism.
I think this is the first important
teaching because this isn't always seen
and it's not always seen this way in the world.
Most often we think passion is something wonderful.
Anger is something important.
Something that's a part of us and delusion
might be bad,
but we don't see how these are fires.
What's wrong with these things?
What's the real problem?
Or maybe we just don't ever think about it.
And so when we get angry,
we just think of it as something,
something to do with me.
There are many clarity of mind really
that allows us to see this as a problem.
So we just act on impulse,
on habit, passion as well.
It's not like we intend,
we say, I think today I'll be passionate.
Although we do reaffirm it,
we say, I'm passionate about this.
I'm passionate about this.
That is a very good thing.
But when it comes up, it comes by itself.
The passion comes just by habit.
That's really the first problem
is that it's not really under our control.
These are like wild fires, really.
You can't say of the wildfire,
okay, only as long as it only stays in the trees
and doesn't go to the grass
or only stays in the grass
and doesn't go to the trees.
Or it only stays in this tree
and doesn't go to that tree.
That's not how fireworks.
Fire goes where it will.
Wherever there's fuel.
It's a wildfire.
Chumps from object to object.
Paragagi-na-dosagi-na-mohagi-na also.
Passion, anger, and delusion.
All three of them.
And the other burning involved
is really the real reason.
Or the other reason,
perhaps some more important reason
to fountains are problematic
is because our experiences
or reality isn't amenable to these.
When you want something,
it's not amenable to that wanting.
The wanting can't be satisfied.
Why?
Because these things are on fire
with birth, old age, death.
Change, really.
We could even separate this into two parts.
This is the second part.
This is old age, sickness, and death.
This is suffering.
Now, let's keep them together.
And the rest are so gay.
This is actually the Buddhist definition of suffering.
All these different things.
Old age, birth is suffering.
Old age is suffering.
Death is suffering.
Sorrow is suffering.
Limitation is suffering.
Pain is suffering.
Displeasure is suffering.
And despair is suffering.
Not getting what you want, getting what you don't want.
This is all what's caught up in the six senses.
The eye, the ear, the nose, the tongue, the body,
and the heart of this is where you try to find happiness.
They're in for a problem, right?
So we talk about old age, sickness, and death.
How this creates such suffering is through the senses.
You see a person, you hear them, and you cultivate this attachment.
The raga dosa moha.
You cultivate these attachments.
And if you really are attached to the person in a positive way,
then when they get old sick and die,
they can be quite stressful, unpleasant.
It's a cause of great sadness and sorrow.
Even for ourselves, when we get old sick and die,
it's something that we wouldn't wish on ourselves.
We wish we could live forever in the...
Because of change, right?
Because of what the Buddha talked about in the second discourse,
impermanent suffering non-self,
because you can't control, because you're not in charge,
because you can't fix, because you can't predict what you're going to see,
what you're going to hear.
You want to see a person certain people,
and then they leave you or they pass away,
and you never get to see them again.
It's quite stressful.
You want to hear pleasant sounds,
and then someone is banging or making noise in the next room,
and you're displeased because of the impermanence,
because you're not in charge of what you hear.
And so on.
So this is a meditation teaching.
This is a introduction to meditation.
It's where we start, because once you understand this,
once you see what's going on in your mind,
you see or in your experience, you see them changing.
Then we come to the second part, and the Buddha says,
we're not talking about the eye anymore.
Someone who is a disciple of the whole of the noble ones.
So you see this, and you see how much stress is caused by your attachment.
You see this during meditation.
You watch yourself cling to something and not be satisfied,
cling to it in the stress and the fire,
the burning that comes from wanting,
or you dislike something and the burning that comes from disliking it,
or you have conceit or arrogance or delusions or even just distraction,
thinking too much worry for an example.
And you see how much stress is involved.
You start to get tired of it.
Meditation is the feeling that the meditator gets after sometimes I'm tired of all this.
I've had enough of all this.
And that's when they start.
That's when they make this shift that we talked about in the last discourse as well,
where they stop clinging so much.
Are they able to let go when they see how much suffering they're causing themselves?
And they really understand it, and they really appreciate it.
They begin to let go of all the eye,
they be like, oh, forms that like of contact, they like, oh, the feelings.
Nibindang virajati, viraga vimuchati.
Again, this is the same as the last discourse.
When they are disenchanted,
they become dispassionate with the freedom from passion.
They are free.
Vimuchasming vimuchamitinya nangoti.
With freedom, there is knowledge.
That one is, there is knowledge of the freedom.
Kina jati, vucy dang brahmacharyang katang karunyang.
Destroyed his birth, lived as the holy life.
Katang karunyang dhan is what needs to be done.
Naparang tatayati, pajana ti.
He knows, or they know, there's nothing more to be done here.
There's nothing, there's no further than here.
There's no going further than this.
Again, getting better than this, basically.
At this moment, there is peace, freedom,
and nothing further to be done.
There's nothing more to hope for, to wish for, to want for, to strive for.
One is freed oneself from all of the stress and all the suffering
that could ever possibly arise.
Simply because one is no longer concerned,
is no longer moved, no longer reacting, or reactive,
to one's experience.
So when the Buddha taught this, all those monks became our hands,
according to the commentary, according to the sutta.
So that's the third discourse.
Really, these three together are called the three cardinal discourses.
If you would like, it's on the internet.
You can read them, and there's a good commentary.
I think a fairly good commentary on the sutta.
Look up the three cardinal discourses of the Buddha.
It's a nice compilation.
Together, they talk about the four noble truths.
They explain them, and what it means,
what the four noble truths mean in a basic sense.
And then they talk about the five aggregates and how you should approach,
and how we understand non-self,
because this is something that people often have trouble with.
And then the six senses.
And the six senses are I think something that we maybe don't give enough attention to,
and the Buddha certainly did give a lot of attention to them.
So we should always remember these two go together.
The five aggregates are dependent on the six senses.
And the six senses are really much better to be the base.
So we look at the five aggregates, because we think, oh, yes, I made up of five things,
but that's not what it means.
It means an experience as made up of five things.
I am not the part of this equation.
There's only seeing, hearing, smelling, tasting, feeling, thinking,
and each one of those has the five aggregates in it.
So together, these really make up a basic.
If you just know these three, it's a really good base
for then going on to practice mindfulness.
It's a four-sided baton and insight meditation.
So there you go.
There's the dhamma for tonight.
Thank you all for tuning in.
Oh, I guess we'll take some questions off the side as well.
But I think I'm only going to take questions off of the meditation side,
otherwise it's too chaotic.
So if you want to ask questions wherever you are,
you want to go to our meditation site,
and you've got to ask them there.
The site is meditation.ceremungalow.org.
And yes, you have to sign in, so we're not making it easy.
If we make it too easy, we get a lot of random questions.
People who are not willing to put out the effort.
If you're not willing to put out the effort,
of course, this is all the way to easy anyway.
And the olden days you had to actually come here and meditate
in order to ask questions.
The olden days you had to go to Asia,
maybe learn a foreign language.
You guys can go, you don't have, you better not stay for this.
We have four meditators here today.
And I can't load the site.
I think we have to take issue with this service provider.
It's not like Amazon. When we were on Amazon, it was very fast.
Now I can't even load the ask panel.
So that's no good.
Somebody please take this up with Doug.
Tell them that we can't, we have to maybe switch providers
because this provider is just, I can't be anything but that
because we never had the problem on Amazon.
I don't know, it could be anything but it seems pretty clear
that this provider is not nearly as good as Amazon.
Or it is not nearly as expensive either.
So there is that.
Well, the ask panel is not working.
So I'm going to take that as a sign that we will have no questions today.
I'd like to thank you all for tuning in.
