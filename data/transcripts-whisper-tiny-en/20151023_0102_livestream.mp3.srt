1
00:00:00,000 --> 00:00:16,720
Okay, good evening everyone, we're broadcasting live October 22nd, 2015.

2
00:00:16,720 --> 00:00:26,320
And today we have the Dhamapada story, so we'll start off with that.

3
00:00:26,320 --> 00:00:52,560
Good evening and welcome back to our study of the Dhamapada.

4
00:00:52,560 --> 00:00:59,120
Tonight we continue on with verse number ninety-five, which reads as follows.

5
00:00:59,120 --> 00:01:11,180
Patavizamoh, no virojati, Indakhi lupamoh tadisubhato, rahado apitakadamoh,

6
00:01:11,180 --> 00:01:19,600
sanzara namboantita dino, which means

7
00:01:19,600 --> 00:01:32,240
like the earth, one is not disturbed, like the, or just as the earth is not disturbed,

8
00:01:32,240 --> 00:01:41,920
is not, just like the earth that is undisturbed, or just like just as the earth is not

9
00:01:41,920 --> 00:01:52,880
undisturbed, is not obstructed, whatever that means. Just as a foundation pillar in the

10
00:01:52,880 --> 00:02:07,120
Kila, just like an Indakila or a foundation post, is such a one of, is such a one of good,

11
00:02:07,120 --> 00:02:18,880
behavior or good deeds, subhato. So I guess this is translated, he is undisturbed, like the earth,

12
00:02:18,880 --> 00:02:27,680
that's what it is. Patavizamoh, no virojati, he is undisturbed like the earth, one who is of good

13
00:02:27,680 --> 00:02:44,240
behavior or good manners, is like a foundation post, is firmly, you know, well founded, unshakable,

14
00:02:44,240 --> 00:02:56,960
rahado apitakadamoh, like a lake that has become unmudied, that is clear and free from cloudiness,

15
00:02:56,960 --> 00:03:08,080
kadama is mud, samsara and abavantita, you know, there is no wandering on, there is no round of

16
00:03:08,080 --> 00:03:18,000
samsara, there is no, no transmigration from one life to the next born old sick and die,

17
00:03:18,000 --> 00:03:26,160
being born old, getting old, getting sick and dying again and again for such a person.

18
00:03:29,280 --> 00:03:42,000
So like the earth, it is unobstructed, unobstructed, untroubled maybe,

19
00:03:42,000 --> 00:03:48,400
founded like a foundation post,

20
00:03:57,040 --> 00:04:05,760
like a lake that is a clear and unmudied, for such a person, there is no more becoming.

21
00:04:05,760 --> 00:04:15,200
This is a description of sari puta, so the story is about a famous incident that occurred

22
00:04:16,160 --> 00:04:24,240
in regards to sari puta, sari puta got the idea that he would go off on a wandering tour,

23
00:04:25,120 --> 00:04:30,480
so he had one story where the puta went off on a wandering tour and makaspa turned back,

24
00:04:30,480 --> 00:04:36,560
there is a story about makaspa, here is a story about sari puta and it seems that

25
00:04:39,280 --> 00:04:45,600
sari puta was having monks to go with him, talking to monks about going with him or staying behind,

26
00:04:45,600 --> 00:04:56,400
and there was one monk who was sari puta didn't know by name or his name doesn't even show up,

27
00:05:00,880 --> 00:05:07,120
and sari puta was addressing monks by name, and this monk thought in terms of sari puta

28
00:05:07,120 --> 00:05:18,400
will address me by name and he never did, sari puta just said, okay, in the rest of you, do this or do that.

29
00:05:23,360 --> 00:05:29,200
He didn't call him, he didn't call him out, which it seems like such a small thing really,

30
00:05:29,760 --> 00:05:35,200
but it reminds me when I was a new monk and it's a story that I'll always remember,

31
00:05:35,200 --> 00:05:41,360
when I returned to stay with my teacher as a monk,

32
00:05:48,480 --> 00:05:57,280
I was always wanted him to, it was that he didn't remember my name, that's what it was,

33
00:05:57,280 --> 00:06:02,640
I was always kind of disappointed that he didn't remember my name and he would always ask,

34
00:06:02,640 --> 00:06:09,680
what's his name? And when I told him, my name he said, no, what kind of a name is that,

35
00:06:11,600 --> 00:06:17,760
it's going to be fun to me actually, and I could never remember my name every time I go to

36
00:06:17,760 --> 00:06:26,160
see him with this monk, where did he or a day? Your day and with you, because after I or day

37
00:06:26,160 --> 00:06:33,120
in that left and went back to Canada for a year, it was an odd situation, we stayed with a monk

38
00:06:33,120 --> 00:06:42,480
in Canada, and then there was this big bruha about the international department,

39
00:06:43,280 --> 00:06:52,800
and I kind of caused a ruckus demanding that, demanding something or I was put in a position

40
00:06:52,800 --> 00:06:59,440
because there was all politics and there were two groups, the monastery was sort of

41
00:07:01,120 --> 00:07:07,760
split between those who wanted the international students to go with a group of lay people,

42
00:07:07,760 --> 00:07:12,560
and those who wanted the international students to practice with the monks through a translator.

43
00:07:14,080 --> 00:07:19,360
And anyway, I was being put on one side and there was another monk being put on the other side

44
00:07:19,360 --> 00:07:24,320
and we were the two people, and I said, this is splitting up the song guys I refuse to do it,

45
00:07:24,320 --> 00:07:29,280
and I went in front of Edgentong, and the other monk yelled at me and said, who do you think you are?

46
00:07:31,520 --> 00:07:37,120
Anyway, I felt, and then Edgentong scolded me as well and said this isn't splitting up the

47
00:07:37,120 --> 00:07:42,640
song that this is trying to make things work, and he said, you have wrong thought he scolded me for it,

48
00:07:42,640 --> 00:07:51,600
and I got in, you know, there were stories going around the monastery about my bad behavior.

49
00:07:53,680 --> 00:07:57,920
I was quite adamant, and I guess kind of you could say kind of arrogant about it,

50
00:07:59,120 --> 00:08:03,200
kind of how Westerners tend to be when we think something's not right, we get up and say

51
00:08:03,920 --> 00:08:08,000
in front of everybody, and from then on he never forgot, I knew.

52
00:08:08,000 --> 00:08:19,280
So yeah, I know how that is, but after that I wasn't so it was kind of ruined for me after that,

53
00:08:19,280 --> 00:08:23,520
because I know why he remembered my name, not for the best reason,

54
00:08:26,160 --> 00:08:30,640
but again it goes back to how monks can get a little bit crazy, you know, when your life is so

55
00:08:30,640 --> 00:08:41,440
simple, I mean for most people in the world they have a very extreme outputs for pleasure,

56
00:08:41,440 --> 00:08:45,840
you know, so you have all sorts of complex activities if you want to go to a movie,

57
00:08:46,480 --> 00:08:52,960
or if you want to go dancing, or if you want to go to the opera, or I don't know what people do,

58
00:08:52,960 --> 00:08:59,280
you want to go to a rave and they still do those, or you can do drugs or that kind of thing,

59
00:08:59,280 --> 00:09:03,680
but for monks there's very little of an outlet, and so you tend to get,

60
00:09:05,920 --> 00:09:15,760
you tend to get petty, and this is an example of this monk being very petty because his ego

61
00:09:15,760 --> 00:09:19,440
didn't have much to react to, so it's simple sliding and not calling him by name,

62
00:09:20,960 --> 00:09:24,960
set him off, and from then on he had a grudge against, sorry put on, you hear about this a lot,

63
00:09:24,960 --> 00:09:30,560
monks who had grudges, they tend to be recognized, again in harkens back to my own experience,

64
00:09:30,560 --> 00:09:38,080
I can verify this, what is it, I don't know if there is even an adage for something like the

65
00:09:38,080 --> 00:09:44,240
greasy wheel, get the squeaky wheel, gets the grease, but that's supposed to be a good thing,

66
00:09:47,360 --> 00:09:52,560
you know, sometimes it's better to go unknown because the reasons for becoming known,

67
00:09:52,560 --> 00:10:00,000
this monk got in the Dhamapada for his, and they didn't even put his name in, insult to injury,

68
00:10:01,280 --> 00:10:05,920
they put him here and they refused to include his name or just, you know, refused to remember his

69
00:10:05,920 --> 00:10:13,600
name, I don't know, or just forgot it, I'm yet thrown down, I'm going to have a say, nah,

70
00:10:13,600 --> 00:10:21,120
apakkato beku, whatever that means, apakkato, unknown, right, so they refused to remember,

71
00:10:21,120 --> 00:10:27,600
they just, no one knew his name, or it was unknown by sorry put digas, so sorry put it didn't

72
00:10:27,600 --> 00:10:34,880
call him an opinion, anyway, being petty and now he's remembered forever immortalized in the Dhamapada

73
00:10:34,880 --> 00:10:46,960
for it, so then on top of that to add insult to injury, not at all really, but I wanted to get

74
00:10:46,960 --> 00:10:56,080
even more, let's say, to be even worse about this whole situation, even more petty to an extreme

75
00:10:56,080 --> 00:11:07,040
degree, as they were preparing, sorry put to walk past him, and the corner of his robe touched

76
00:11:07,040 --> 00:11:19,680
the upset monk on the ear or something, and the monk, either getting angry about it or just

77
00:11:19,680 --> 00:11:25,760
using it as an excuse, started going around and telling people, or he went straight to the Buddha

78
00:11:25,760 --> 00:11:38,400
maybe and told the Buddha, what do you think he said to the Buddha, he said, yeah, he went straight

79
00:11:38,400 --> 00:11:47,600
to the Buddha, and said, reverence or venerable, sorry puta, doubtless thinking to himself,

80
00:11:47,600 --> 00:11:56,400
I am your chief disciple, struck me a blow that almost broke my eardrum or something, and some

81
00:11:56,400 --> 00:12:05,680
part of my ear, and then without even asking forgiveness he set out on his arms, set out for arms,

82
00:12:05,680 --> 00:12:10,640
so it was before the arms round. Would you imagine the goal of this guy

83
00:12:10,640 --> 00:12:18,160
going to the Buddha to inform upon, sorry puta, who had done nothing wrong,

84
00:12:19,600 --> 00:12:25,120
I mean the amount of ignorance you need to do that is pretty astounding, and yet,

85
00:12:28,160 --> 00:12:35,760
I mean it happens, people get blinded, but as we'll see, it doesn't last long, it can't last long,

86
00:12:35,760 --> 00:12:42,160
there's such purity, and as we'll see in regards to how sorry puta responds, there's such a

87
00:12:42,160 --> 00:12:49,440
profundity to the beat to these beings that they've kept last, eventually he asks forgiveness,

88
00:12:49,440 --> 00:12:56,720
so all the monks hear about this, the Buddha calls someone, go and tell sorry puta to come,

89
00:12:58,080 --> 00:13:02,240
and all the other monks finding out about this, they gather together in

90
00:13:02,240 --> 00:13:11,200
Mogulana and went around to all the monks and said come come and see, you want to see something

91
00:13:11,200 --> 00:13:16,320
neat, see how sorry puta deals with this guy, and so all the monks came out to listen,

92
00:13:18,160 --> 00:13:27,280
and the Buddha said, so sorry puta this monk says that you have struck him, and without even

93
00:13:27,280 --> 00:13:36,080
apologizing, went away, you hit him, and then to find out what sorry puta says, we actually

94
00:13:36,080 --> 00:13:47,840
have to go to the Angutarani kaya, this is actually one story that is supported by the actual

95
00:13:47,840 --> 00:13:56,960
canonical texts, remember the stories we're reading are sort of recreations, recreated from

96
00:13:57,760 --> 00:14:05,680
the verses, so they may have been exaggerated, and more likely they've been exaggerated,

97
00:14:05,680 --> 00:14:16,240
perhaps some might even say made up, and just based on folklore, but regardless they are

98
00:14:16,240 --> 00:14:23,920
less, they are not canonical, whether they're true or not, but here we have one that is

99
00:14:24,640 --> 00:14:32,720
based on the Angutarani kaya book of nines, because the Buddha says nine things,

100
00:14:34,480 --> 00:14:43,760
he doesn't say I didn't hit the guy, instead, he says one who has not established mindfulness

101
00:14:43,760 --> 00:14:59,680
directed to the body in regards to his own body, so let's see how he says that.

102
00:14:59,680 --> 00:15:21,680
So one who is not mindful of the body, and this can be in one of various ways,

103
00:15:22,800 --> 00:15:29,120
but you could relate it back to our practice and say one who is not aware when they're walking,

104
00:15:29,120 --> 00:15:35,680
that they know they're walking, not aware of the movements of their body, where their hands

105
00:15:35,680 --> 00:15:44,480
are, not aware of when they, where their mind is, and it's not objective about their actions,

106
00:15:45,120 --> 00:15:52,560
such a person could very easily strike someone out of anger, but the point is when your mindfulness,

107
00:15:52,560 --> 00:15:58,800
you actually are unable to get angry. When you're truly mindful, as an R-100 is always,

108
00:16:01,760 --> 00:16:05,360
it's not possible for you to strike someone, and that's what's very Buddha says.

109
00:16:07,520 --> 00:16:13,280
And he gives nine similes as to why it wouldn't be possible, or as to what it's like to be someone

110
00:16:13,280 --> 00:16:18,400
who is mindful of the body, who is aware of the movements of the body, who wouldn't know

111
00:16:18,400 --> 00:16:23,760
when they were raising their fist and would be fully mindful, and thus unable to give rise to the

112
00:16:23,760 --> 00:16:34,960
range required, the cruelty required, the hatred required to hurt someone. And so he says just as water,

113
00:16:37,360 --> 00:16:43,280
just as you can wash impure things in water, and the water is not repulsed by it,

114
00:16:43,280 --> 00:16:53,360
the water doesn't get upset by that. He said, my mind is like water, without enmity or ill will

115
00:16:53,360 --> 00:17:01,120
towards anyone, no matter what they do. Just as fire burns impure things, but it's not upset by it,

116
00:17:01,760 --> 00:17:07,920
so too I dwell like fire, not upset when people, whatever they may say or do to me.

117
00:17:07,920 --> 00:17:24,640
Just as air blows upon impure things, and it's not repulsed by that. So too I can meet with

118
00:17:24,640 --> 00:17:30,960
anything come in contact with anyone and not be upset. My mind I dwell with a mind like air.

119
00:17:30,960 --> 00:17:38,000
I dwell with a mind like a duster, a dust rag, I guess. The dust rag is used to clean up all

120
00:17:38,000 --> 00:17:45,680
sorts of rag, it's used to clean up all sorts of things, and yet the rag is not repelled.

121
00:17:48,640 --> 00:17:57,360
And then just as an outcast boy or girl, when I walk, I think of myself as an outcast boy or girl

122
00:17:57,360 --> 00:18:10,880
holding a bowl and wandering around for arms or for charity, begging. He says, that's how I see

123
00:18:10,880 --> 00:18:19,040
myself. I put myself on that level when I go for arms. When I walk, when I live, because an outcast

124
00:18:19,040 --> 00:18:24,400
boy or girl in India, they would be, they're not allowed to work, they're not allowed to

125
00:18:24,400 --> 00:18:30,240
do so many things. They have to live in special areas or had to. This is an ancient time.

126
00:18:35,360 --> 00:18:41,680
Just as a bull with his horns cut, mild well tamed and well trained. So a bull without its horns

127
00:18:41,680 --> 00:18:53,920
isn't going to attack anyone. And then, and so I am like this bull without horns, I have no horns

128
00:18:53,920 --> 00:19:00,000
with which to attack, meaning he can't even. He couldn't, he couldn't hit someone even if well,

129
00:19:00,000 --> 00:19:05,280
if he wanted to do because he couldn't want to. It could never happen. It's not possible.

130
00:19:05,280 --> 00:19:12,240
He's not capable of it. It's not that he doesn't even want to. It's that it's, he's not capable of it.

131
00:19:12,240 --> 00:19:19,600
And number eight, just as a woman or a man, a young woman or man, think of a young woman, a young

132
00:19:19,600 --> 00:19:24,960
man who like to dress up, like to be clean, like to put on perfumes, like to put on makeup.

133
00:19:27,280 --> 00:19:34,000
Imagine if such a person had a carcass of a snake, a carcass of a dog, or a carcass of a human being,

134
00:19:34,000 --> 00:19:41,360
slung around their neck. Did you imagine, put a, put a carcass of a dog around your neck,

135
00:19:42,400 --> 00:19:49,920
what they would think of that. And he said, he says, Monday, just in that way, I am repelled,

136
00:19:49,920 --> 00:19:57,280
humiliated and disgusted by this foul body. Interesting wording though, means he has no,

137
00:19:57,280 --> 00:20:03,040
it's not even, you have to take that kind of with a bit of license because I think it's unfair to

138
00:20:03,040 --> 00:20:10,000
say he's disgusted by it. Disgusted, it would be, you know, an upset, but he's certainly not upset.

139
00:20:10,800 --> 00:20:15,120
He just does no, no thought that there's anything good inside it. I mean, the body is made up of

140
00:20:15,120 --> 00:20:17,200
all sorts of icky things.

141
00:20:17,200 --> 00:20:32,080
And number nine, seeing as he does that, just as a person might carry around a cracked bowl,

142
00:20:32,880 --> 00:20:39,600
or imagine someone with a garbage bank full of, from a restaurant, from behind a restaurant with

143
00:20:39,600 --> 00:20:50,720
fat and, and refuse and so on, with a leaking garbage bank. This is a perforated bowl, a cracked

144
00:20:50,720 --> 00:20:57,600
bowl with fat in it that oozes and drips. And the body is like this with all sorts of holes in it

145
00:20:57,600 --> 00:21:10,640
that oozes and drips and sweat and smell. And so he said, one who has in conclusion, one who

146
00:21:10,640 --> 00:21:15,840
has not established mindfulness directed to the body in regards to his own body, might strike a

147
00:21:15,840 --> 00:21:20,560
fellow monk here and then set out onto her without apologizing. So it's called the lion's roar.

148
00:21:20,560 --> 00:21:29,200
It's one of the many lions roars that we hear about. This one is by Sariput. And so it's kind of

149
00:21:29,200 --> 00:21:37,680
impressive to read. And then Sariput has no qualms about putting this monk in his place, so to speak,

150
00:21:38,640 --> 00:21:44,160
has no, it's interesting that this came right after criticism yesterday, we were talking about

151
00:21:44,160 --> 00:21:51,200
criticism. And reading through it today made me think, okay, this is the emulation required.

152
00:21:51,200 --> 00:21:57,840
This is what we have to work towards to be like Sariput. It was so mindful that he has no ill will.

153
00:22:01,200 --> 00:22:09,200
So this is the, this is Sariput's argument as to why that such a thing wouldn't be done.

154
00:22:09,200 --> 00:22:19,920
And so you think that would be enough. And it was enough. The monk immediately was shaken,

155
00:22:21,760 --> 00:22:25,840
and got up on his hands and knees and prostrated himself at the foot of the Buddha and said,

156
00:22:25,840 --> 00:22:33,280
please forgive me. I was stupid. I was wrong. I transmitted a transgression that I so foolishly,

157
00:22:33,280 --> 00:22:39,440
stupidly, and unskillfully slandered the venerable suit. Sorry, Buddha. On grounds that were untrue.

158
00:22:39,440 --> 00:22:40,560
Peaceless, false.

159
00:22:45,200 --> 00:22:49,840
And the Buddha acknowledged that. And then he turns the Sariput, and he says,

160
00:22:49,840 --> 00:22:53,680
sorry, Buddha, forgive him before his head explodes.

161
00:22:54,320 --> 00:23:00,000
So apparently it was a thing. There is a thing. If you insult someone who is enlightened,

162
00:23:00,000 --> 00:23:03,520
you can get into some serious head exploding.

163
00:23:06,880 --> 00:23:14,160
And so if it was, as if it wasn't enough, Sariput puts the icing on the cake by saying,

164
00:23:14,160 --> 00:23:23,600
I will pardon him, because of what he has said. I will pardon him if he says that to me.

165
00:23:23,600 --> 00:23:31,520
If he asked me forgiveness, meaning he hasn't yet asked the Buddha,

166
00:23:31,520 --> 00:23:34,640
asked Sariput to forgiveness, he asked the Buddha forgiveness. So the point is,

167
00:23:34,640 --> 00:23:39,520
Sariput has nothing to forgive. It has not no pardon to give until the monk says,

168
00:23:39,520 --> 00:23:43,440
sorry, not the Sariput needs it, but he's saying, well, if he came to me, of course,

169
00:23:43,440 --> 00:23:49,680
I apologize. All right, forgive him. I have no hard feelings. And then he says, and then,

170
00:23:49,680 --> 00:23:59,760
and let him pardon me as well, let him forgive me as well, meaning for anything I might have

171
00:23:59,760 --> 00:24:05,600
done to him. This is actually quite standard. When you, when you apologize this to a monk,

172
00:24:06,640 --> 00:24:10,960
we do this often, we'll apologize to each other, and then I apologize to this monk,

173
00:24:10,960 --> 00:24:15,760
and the monk turns around and apologizes to me. We actually have a,

174
00:24:15,760 --> 00:24:24,800
we have at the end of the opening ceremony we do this, opening and closing ceremonies for a

175
00:24:24,800 --> 00:24:30,400
meditator. We have this ceremony of asking forgiveness of the teacher, and then the teacher

176
00:24:30,400 --> 00:24:38,160
turns around and asks forgiveness of us in this tradition. So this is an example for us to emulate.

177
00:24:40,000 --> 00:24:45,200
Then we switch back to the Dhamapada story, then apparently the Buddha, the monks started

178
00:24:45,200 --> 00:24:53,600
commenting on this, and we're terribly impressed. And in awe and reverence for

179
00:24:53,600 --> 00:25:01,120
Sariput and his ability to just put this guy in his place, but to not be at all upset or

180
00:25:02,080 --> 00:25:10,960
disturbed or angry towards the other monk. And the teacher heard what they were saying,

181
00:25:10,960 --> 00:25:19,520
and came and said to the Mo, it's not unusual. It's not hard to understand. He said,

182
00:25:19,520 --> 00:25:26,240
it's impossible for Sariput and people like him to have hatred, and Sariput does mind is like the

183
00:25:26,240 --> 00:25:32,640
great earth. It's like an indicate, like a foundation post, like a pool of still water,

184
00:25:32,640 --> 00:25:43,120
and then he taught this verse. So how this relates to us, again, it's about emulating this,

185
00:25:43,120 --> 00:25:47,920
these are qualities to emulate, which should read the whole suit and the anguather book of nines

186
00:25:48,960 --> 00:25:53,840
for when people accuse us of things that we didn't do, or even of things that we didn't do.

187
00:25:57,200 --> 00:26:00,880
I guess more, it's harder when you didn't do it, when you didn't do something.

188
00:26:00,880 --> 00:26:06,160
And you notice that Sariput, and in general, you'll notice that it's not that they

189
00:26:08,160 --> 00:26:14,800
don't refute it. They do, but they do so without anger, and they do it for the purpose of

190
00:26:14,800 --> 00:26:19,680
setting the record straight so people don't get the wrong idea. They don't do it so that it makes

191
00:26:19,680 --> 00:26:21,520
them look, for sure.

192
00:26:21,520 --> 00:26:34,720
And so we emulate these qualities. We emulate the imagery that Sariput had talked about.

193
00:26:38,320 --> 00:26:43,200
But most importantly, we emulate them in their practice. Mindfulness of the body. It's a huge one.

194
00:26:45,120 --> 00:26:49,920
Well, it's mindfulness is the huge one, but he's talking about mindfulness of the body

195
00:26:49,920 --> 00:26:55,680
as because we're referring to the body. He hits someone. Sariput just points out, you know, it's

196
00:26:55,680 --> 00:27:00,320
just a funny thing to think about because I'm so mindful with my body, he'd say, you know.

197
00:27:02,720 --> 00:27:10,080
It's not really possible that such a thing, watching, observing his own actions, which is,

198
00:27:10,080 --> 00:27:15,360
you know, the proper way to deal with a accusation. Someone accuses you of something, and

199
00:27:15,360 --> 00:27:20,160
just say, I would never do that. He reflects and say, could I do something? Did I do that?

200
00:27:20,160 --> 00:27:27,200
Could I do that? And he looked and he said, it's not possible because I'm so mindful all the time.

201
00:27:27,920 --> 00:27:35,040
How could you do that if you're mindful? And this is the key. Our theory is, and the power of

202
00:27:35,040 --> 00:27:42,640
mindfulness is that you can't be angry and mindful at the same time when you're clearly aware

203
00:27:42,640 --> 00:27:49,280
and objectively aware. The objectivity overcomes any anger or any greed, and you just

204
00:27:50,400 --> 00:27:51,760
are, you're just present.

205
00:27:54,960 --> 00:27:59,120
Makes you like the earth undisturbed, untroubled.

206
00:28:01,360 --> 00:28:07,360
The earth is, I mean, it's the imagery of the great earth, Mahapatavi, the earth as a planet

207
00:28:07,360 --> 00:28:14,480
that, well, it does have earthquakes, but it's pretty much undisturbed. Just sits there.

208
00:28:15,120 --> 00:28:17,600
It's totally grounded, I could say.

209
00:28:20,240 --> 00:28:25,680
And like a foundation post, they use this Indikila to mean something.

210
00:28:25,680 --> 00:28:32,480
Kila means a post, Inda is of Indra. It's an expression that means the foundation of a building,

211
00:28:32,480 --> 00:28:41,040
the first post that sort of keeps the building up, I guess. And so it's the most stable part of

212
00:28:41,040 --> 00:28:48,720
a house with someone who is firm and unmoved. So if someone calls them and asks them to name

213
00:28:48,720 --> 00:28:52,240
them, they don't get upset. If someone says nice things to them, they don't get pleased by it.

214
00:28:54,080 --> 00:28:58,480
Good things come. They're not elated, bad things come. They're not depressed. They live at peace,

215
00:28:58,480 --> 00:29:05,360
and they live in the world without being out of the world, without being moved by the world,

216
00:29:05,360 --> 00:29:13,520
without falling under the power of samsara. They become like a pool of water, still pool of water

217
00:29:13,520 --> 00:29:20,160
free from mud. You can think of mud as all of the mental defilements. This is what we're aiming for

218
00:29:20,160 --> 00:29:29,280
and be fascinated as to have a clear mind. Such a person, the rounds of existence do not

219
00:29:29,280 --> 00:29:37,200
exist. Do not exist. Samsara does not exist. There is no samsara. Samsara, Nabokantita, you know.

220
00:29:39,440 --> 00:29:47,520
Samsara means wandering on. They have nowhere left to wander. They have no desires or no lessons

221
00:29:47,520 --> 00:29:57,040
left to learn. No goals yet to achieve. They've done it all, or they've risen above it all.

222
00:29:57,040 --> 00:30:05,200
They've gone beyond it all. Anyway, so that's the Dhamapada for tonight. Nice story.

223
00:30:05,200 --> 00:30:09,200
So he's nice to hear about Saripuddha. He has other stories like this. There's another story

224
00:30:09,200 --> 00:30:15,200
about a monk actually coming and hitting him over the, hitting him on the back. Saripuddha was

225
00:30:15,200 --> 00:30:20,080
wandering on arms and this Brahman, I think. He'd heard that the Saripuddha was like this,

226
00:30:20,080 --> 00:30:24,640
probably heard from this story and he thought, wow, wonder if it's true. How could it be possible

227
00:30:24,640 --> 00:30:28,880
that this guy doesn't get upset no matter what happens? And he said, well, let's test him. So he

228
00:30:28,880 --> 00:30:35,760
took a stick. He wanders that walks up behind Saripuddha and he's on arms and smacks him one on the back.

229
00:30:35,760 --> 00:30:43,280
And Saripuddha just turns around and looks at him and then turns and goes on his way.

230
00:30:45,040 --> 00:30:51,680
And then the Brahman ends up apologizing to Saripuddha. See imagine just walking up and smacking him.

231
00:30:52,960 --> 00:30:56,800
He said, it's really true. He doesn't get angry. He doesn't get upset no matter what happened.

232
00:30:56,800 --> 00:31:05,040
So if you're really interested in Saripuddha, you should read there's an English

233
00:31:06,640 --> 00:31:12,560
on the internet. There's an article. I think it's in great disciples of the Buddha,

234
00:31:13,120 --> 00:31:18,080
but it's also under the life of Saripuddha. I think it was originally published in the Buddhist

235
00:31:18,080 --> 00:31:23,760
publication society, but I think it's on the internet. Very much worth reading because very much

236
00:31:23,760 --> 00:31:30,800
worth emulating. Saripuddha and Mogulana and all the great disciples. If you want to know what a

237
00:31:30,800 --> 00:31:38,800
true Buddhist is like, what are Buddhists like when they go all the way? It's pretty impressive.

238
00:31:39,680 --> 00:31:49,200
So that in mind, this is what we worked towards to be impressive. Not exactly, but to be free,

239
00:31:49,200 --> 00:32:01,040
to be pure, to be untroubled and that's impressive. So thank you all for tuning in, wishing you all

240
00:32:01,040 --> 00:32:15,920
good practice and have a good night. Okay, I probably have to go, I think no questions tonight

241
00:32:15,920 --> 00:32:24,080
because I have, this is the last time, but tonight I have to go back to Stony Creek and this

242
00:32:24,080 --> 00:32:31,280
won't happen again next week, but the head monk who's coming to pick me up and he didn't go to

243
00:32:31,280 --> 00:32:36,480
class because he's also in school. He has one class a week because he said there was no class,

244
00:32:36,480 --> 00:32:42,400
but I'm pretty sure anyway. I assumed he was coming. He's not, so instead he's coming to pick me

245
00:32:42,400 --> 00:32:49,120
up specifically and he'll probably be here fairly soon. So I have to get ready.

246
00:32:49,120 --> 00:33:06,240
Okay, thanks Robin for coming up and thanks everyone for tuning in. Have a good night everyone.

