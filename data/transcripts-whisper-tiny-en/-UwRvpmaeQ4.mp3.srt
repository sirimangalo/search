1
00:00:00,000 --> 00:00:08,680
Meditating, I noticed that fabricating an eye, eye was feeling, etc., was untenable.

2
00:00:08,680 --> 00:00:11,160
There was only the experience.

3
00:00:11,160 --> 00:00:14,000
This seemed to clearly disprove the self.

4
00:00:14,000 --> 00:00:16,800
This notion is so fundamentally simple.

5
00:00:16,800 --> 00:00:21,000
Why then is it so elusive, my gratitude?

6
00:00:21,000 --> 00:00:27,640
Well, it's only elusive because we've cultivated a sense of self.

7
00:00:27,640 --> 00:00:35,440
We have a very strong sense of self as human beings.

8
00:00:35,440 --> 00:00:42,200
We are born with it in something we've cultivated lifetime after lifetime, but it's something

9
00:00:42,200 --> 00:00:45,000
we reaffirm throughout our life.

10
00:00:45,000 --> 00:00:54,840
The idea of this is mine, the idea of being given things, identifying with things.

11
00:00:54,840 --> 00:01:03,160
When you recognize something, you desire it, and so you cling to it, and you get the idea

12
00:01:03,160 --> 00:01:05,960
that you're somehow in control of it.

13
00:01:05,960 --> 00:01:15,640
We cultivate habits of misperception, and it's not just identification with self.

14
00:01:15,640 --> 00:01:16,640
It's greed.

15
00:01:16,640 --> 00:01:21,160
There's actually no, if you think about it logically, there's nothing different between pleasure

16
00:01:21,160 --> 00:01:25,000
and pain, and yet we prefer one and a bore the other.

17
00:01:25,000 --> 00:01:26,000
Why is that?

18
00:01:26,000 --> 00:01:33,320
It's through misapprehension, it's through misconception, misunderstanding, about reality.

19
00:01:33,320 --> 00:01:34,320
It's only habitual.

20
00:01:34,320 --> 00:01:37,400
It's actually only you that's like that.

21
00:01:37,400 --> 00:01:39,960
There are people out there who aren't like that.

22
00:01:39,960 --> 00:01:43,840
There are people who are actually born with very little attachment to self.

23
00:01:43,840 --> 00:01:52,200
And most of us aren't so lucky, but when you say it being elusive, it's only elusive

24
00:01:52,200 --> 00:02:02,400
to you and to most of us, but it's only an artificial, it's not natural to, it doesn't

25
00:02:02,400 --> 00:02:04,400
mean it's natural to believe in a self.

26
00:02:04,400 --> 00:02:09,760
It just means that we've gotten so lost and so caught up in misunderstanding that we actually

27
00:02:09,760 --> 00:02:12,000
are born like this.

28
00:02:12,000 --> 00:02:15,760
It comes back to the idea that being human is somehow natural.

29
00:02:15,760 --> 00:02:18,760
There's nothing natural about having 10 fingers and 10 toes.

30
00:02:18,760 --> 00:02:29,280
It's a ridiculous situation that we're in, that we have to mash up physical matter in certain

31
00:02:29,280 --> 00:02:35,720
types of physical matter in our mouths and then have it pass down into our chests and

32
00:02:35,720 --> 00:02:44,480
be digested in our abdomens and then excreted as smelly, awful, fecal matter.

33
00:02:44,480 --> 00:02:47,520
There's nothing natural about that.

34
00:02:47,520 --> 00:02:53,520
Our whole existence, that we should have to bump into each other in order to, to, to

35
00:02:53,520 --> 00:02:59,720
procreate, to, in order for someone to be born, there has to be two beings of certain

36
00:02:59,720 --> 00:03:11,600
types coming together and having orgasms and then the, the, the, on the off chance that

37
00:03:11,600 --> 00:03:13,320
an egg and a sperm are going to get together.

38
00:03:13,320 --> 00:03:16,880
I mean, it's just, it's a ridiculous situation that we find ourselves in there.

39
00:03:16,880 --> 00:03:20,160
There's nothing natural about any of this.

40
00:03:20,160 --> 00:03:28,800
So the view of self is kind of, it's one of the major glues that holds it all together.

41
00:03:28,800 --> 00:03:33,480
It's one of the, the major points where we could say this is where we went wrong and yet

42
00:03:33,480 --> 00:03:34,480
we keep affirming it.

43
00:03:34,480 --> 00:03:40,760
We keep making the mistake and augmenting the mistake and, and cultivating that mistake.

44
00:03:40,760 --> 00:03:42,920
Now for some people, it's not that case.

45
00:03:42,920 --> 00:03:47,920
They're, they're on the, they're on the path away from that, especially those people

46
00:03:47,920 --> 00:03:53,120
who practice the Buddhist teaching and practice mindfulness meditation.

47
00:03:53,120 --> 00:03:58,600
They find themselves seeing clearly and realizing, oh, this is horrible thing that

48
00:03:58,600 --> 00:04:06,920
we've gotten ourselves into the situation that we've got ourselves into due to our misapprehension.

49
00:04:06,920 --> 00:04:12,600
So it's in a, in short, it's just because of your habitual misunderstanding, but once

50
00:04:12,600 --> 00:04:16,320
you start to read about it, it just seems so obvious and you realize, oh, what an idiot

51
00:04:16,320 --> 00:04:17,680
I've been, right?

52
00:04:17,680 --> 00:04:21,560
So, so often when the Buddha would teach, at the end of the topic would say, it's like

53
00:04:21,560 --> 00:04:25,960
I was blind and now I could see it's like you've, something was turned over and you've

54
00:04:25,960 --> 00:04:31,280
just put it like a piece of furniture was tipped over and you've just righted it for me.

55
00:04:31,280 --> 00:04:34,600
And, and, and now everything seems right again.

56
00:04:34,600 --> 00:04:36,760
That was quite common in the Buddha's teaching.

57
00:04:36,760 --> 00:04:42,520
It was just like putting things back in order, straightening out ones, understanding about

58
00:04:42,520 --> 00:05:01,920
reality because we get crooked otherwise, we get lost and confused and tired but not.

