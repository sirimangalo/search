1
00:00:00,000 --> 00:00:16,480
Okay, good evening, broadcasting live, September 29, 2015.

2
00:00:16,480 --> 00:00:23,520
The next topic is annihilationism or nihilism, I think it may actually translate it sometimes

3
00:00:23,520 --> 00:00:25,240
as nihilism.

4
00:00:25,240 --> 00:00:31,240
So, Robin, would you read the quote for us, please?

5
00:00:31,240 --> 00:00:40,520
Yes, in what way could one say the monk Gautama is an annihilationist, he teaches the

6
00:00:40,520 --> 00:00:44,600
doctrine of annihilation, and we speak incorrectly.

7
00:00:44,600 --> 00:00:50,360
I teach the annihilation of greed, hatred and delusion, I proclaim the annihilation of

8
00:00:50,360 --> 00:00:57,840
people on skilled states. It is in this way that one could say the monk Gautama is an annihilationist,

9
00:00:57,840 --> 00:01:05,840
he teaches the doctrine of annihilation, and he's speaking correctly.

10
00:01:05,840 --> 00:01:21,200
He teaches the one who teaches, who has the most speaks of cutting off Ucheda, she

11
00:01:21,200 --> 00:01:26,680
means cutting, I guess, off sort of.

12
00:01:26,680 --> 00:01:38,320
Gautama is a view and Buddhism of nihilism, but not exactly nihilism, annihilationism.

13
00:01:38,320 --> 00:01:46,680
I don't actually know what nihilism is, maybe nihilism, any way annihilationism is more

14
00:01:46,680 --> 00:01:53,720
precise, I think, annihilationism means we exist, but we exist only until death.

15
00:01:53,720 --> 00:01:59,320
And that death were cut off, life was cut off, and that's it.

16
00:01:59,320 --> 00:02:06,280
So, this says it comes from the Anguetunikaya, but I believe the same passage or something

17
00:02:06,280 --> 00:02:28,280
very similar to it is found in one of the Chupdas, which one? My memory is going anyway.

18
00:02:28,280 --> 00:02:36,680
There's a Suta where this Brahman comes to the Buddha, and oh, that's right, it's in the

19
00:02:36,680 --> 00:02:40,760
introduction to the Vinaya. Let's see here.

20
00:02:40,760 --> 00:02:42,760
We're Anjakanda.

21
00:02:42,760 --> 00:02:43,760
We're Anjakanda.

22
00:02:43,760 --> 00:02:44,760
We're Anjakanda.

23
00:02:44,760 --> 00:02:45,760
We're Anjakanda.

24
00:02:45,760 --> 00:02:46,760
We're Anjakanda.

25
00:02:46,760 --> 00:02:47,760
We're Anjakanda.

26
00:02:47,760 --> 00:02:54,880
We chant this actually, this very beginning, we chant the beginning of the Vinaya, the

27
00:02:54,880 --> 00:03:00,320
beginning of the Dikanikaya, which is the Suta, and the beginning of the Abhidhamma,

28
00:03:00,320 --> 00:03:03,200
and it's our way of chanting the whole Epitika, it's just a little bit.

29
00:03:03,200 --> 00:03:05,320
They do in Thailand.

30
00:03:05,320 --> 00:03:09,400
So, this first paragraph of the Vinaya, Daena, Samayi, Nambudho, Bhagavaya,

31
00:03:09,400 --> 00:03:17,560
Viranjayang, Viranjayang, Viranjayang, Viranjayang, Viranjayang, Viranjayang, Viranjayang, Viranjayang, Viranjayang,

32
00:03:17,560 --> 00:03:32,000
Viranjayang, Jarang, spirits, Nalaihi,

33
00:03:32,000 --> 00:03:34,120
Viranjayang.

34
00:03:34,120 --> 00:03:36,920
And then Ramana, Viranjayang came to see the Buddha, because they heard Samano-kalo-bogo

35
00:03:36,920 --> 00:03:37,880
wo.

36
00:03:37,880 --> 00:03:54,240
The sama-nagotama who is a sakya-puta, son of the sakians, and the sakian family has gone

37
00:03:54,240 --> 00:04:01,620
forth and is dwelling in Wiranja at the foot of the Nelei-rupuchi's tree, or maybe it's

38
00:04:01,620 --> 00:04:10,640
in Nelei-rupuchi-manda tree, with a great number of 500 monks.

39
00:04:10,640 --> 00:04:21,720
And about this blessed teacher, a kittisada, fame, or good reputation, has been spread.

40
00:04:21,720 --> 00:04:35,440
In the Bhisobhagoa, Indian, he is a blessed one, Arahang, worthy, sama, sambhunto, a fully

41
00:04:35,440 --> 00:04:37,440
self-enlighted Buddha.

42
00:04:37,440 --> 00:04:45,360
Wirja, Jairana, sambhanno, endowed with conduct, knowledge and conduct, sukato, well gone,

43
00:04:45,360 --> 00:04:58,980
soka-ri-nu, the knower of worlds, Anut-dharo, is unexceled or unsurpassed, Puri-sandam-masarati,

44
00:04:58,980 --> 00:05:08,980
the trainer of people who are trainable, he is able to tame those who can be tamed.

45
00:05:08,980 --> 00:05:19,300
Satha, Deva-manu-sandam, the teacher of angels and men, or angels and humans, sorry.

46
00:05:19,300 --> 00:05:31,340
Buddha, he is awake or enlightened, Bhagavas, blessed or fortunate.

47
00:05:31,340 --> 00:05:39,760
No Imanu-no kungsadeva, kungsamar, kungsamra-makungsatsamamra-mani, the jangsadeva-manu-sang, sorry, this is

48
00:05:39,760 --> 00:05:45,620
so long, he is just talking about, he teaches in this world with its angels and men,

49
00:05:45,620 --> 00:05:48,460
and maras and so on.

50
00:05:48,460 --> 00:05:55,100
Sayyang-a-bina, Satya-kattua-pa-baiditi, he makes clear having realized, realized the truth for

51
00:05:55,100 --> 00:05:57,800
himself, realized, higher knowledge for himself.

52
00:05:57,800 --> 00:06:08,740
He teaches the dhamma, adi-kalyan, namma, ji-kalyan, namma, baryosa, nakalyan, sorry, I'm getting

53
00:06:08,740 --> 00:06:15,220
in deep detail with the suta and not getting to the quote, but this is a common repeated

54
00:06:15,220 --> 00:06:17,580
phrase that people have heard about the Buddha.

55
00:06:17,580 --> 00:06:21,460
He teaches the dhamma that is good in its beginning, good in the middle, and good in its

56
00:06:21,460 --> 00:06:31,020
consummation, together with its meaning and the letter and the meaning and the letter,

57
00:06:31,020 --> 00:06:40,460
gabel a paripunang, perfect in all ways, parisudang, pure, he teaches the pure holy life

58
00:06:40,460 --> 00:06:47,300
that is pure and complete in all ways.

59
00:06:47,300 --> 00:06:57,460
Nukho-pa-nattata-rupa-nang, a-ra-hatang-das-nang-ho-ti, zhksa-dumin's good, good indeed, and indeed

60
00:06:57,460 --> 00:07:08,300
it is good to see das-nang, the seeing of such, of an a-ra-hat, a holy one who is like

61
00:07:08,300 --> 00:07:11,940
this is good, the seeing of such a one is good.

62
00:07:11,940 --> 00:07:18,580
And so he went to the Buddha and he accuses the Buddha.

63
00:07:18,580 --> 00:07:27,140
He says, so I've heard that you don't pay respect to two old brahmanas.

64
00:07:27,140 --> 00:07:30,820
When old brahmanas come to see you or when you go to see them, you don't stand up and

65
00:07:30,820 --> 00:07:36,940
salute them, you don't pay, give them this respect of holding your hands up, you don't

66
00:07:36,940 --> 00:07:43,420
treat them as your superiors, even though they're old and advanced and so on.

67
00:07:43,420 --> 00:07:45,060
Is that true?

68
00:07:45,060 --> 00:07:48,420
What do you think the Buddha said?

69
00:07:48,420 --> 00:07:59,100
Nukhmanas-nahang, brahmana-pa-sa-mi, I don't see brahmana in this world, yada-yada-yada, someone

70
00:07:59,100 --> 00:08:04,660
who I should pay respect, someone who should I should stand up for, someone who I should

71
00:08:04,660 --> 00:08:18,620
give the higher seat to, not a very compliant thing to say or a bold thing to say, I don't

72
00:08:18,620 --> 00:08:23,660
see anyone that I should pay respect to, if anyone else said that, we wouldn't get

73
00:08:23,660 --> 00:08:24,660
away with it.

74
00:08:24,660 --> 00:08:29,220
In fact, he doesn't get away with it, because the brahmana doesn't realize that this

75
00:08:29,220 --> 00:08:40,460
is a fully enlightened Buddha and, no, you can't be higher than a Buddha.

76
00:08:40,460 --> 00:08:50,500
And he says, yan-hi, brahmana-ta-ta-ga-ta-ta-ta-ta-ta-ta-ta-ta-ta-ta-ta-ta-ta, a Buddha, were to bow down

77
00:08:50,500 --> 00:09:03,980
to, or stand up for, or give a higher seat to, anyone, that person's head would split,

78
00:09:03,980 --> 00:09:09,820
that person's head would explode, basically.

79
00:09:09,820 --> 00:09:13,500
And so then he starts accusing the Buddha of some things.

80
00:09:13,500 --> 00:09:18,580
They're proposed as questions here, I think there's a bit of uncertainty as to whether

81
00:09:18,580 --> 00:09:25,460
their questions or accusations, I suppose they're questions, because it sounds like this

82
00:09:25,460 --> 00:09:29,100
guy wasn't hostile towards the Buddha.

83
00:09:29,100 --> 00:09:32,700
But he asks, are you tasteless, or are us a root bull?

84
00:09:32,700 --> 00:09:39,420
I think it means tasteless, anyway, there's meaning to it, I'm not going to go into detail.

85
00:09:39,420 --> 00:09:45,780
Kneebogo, are you one who is useless, are you useless, deserted?

86
00:09:45,780 --> 00:09:52,140
And the Buddha uses plays a play on words for each, one of these nibbogat, and I don't

87
00:09:52,140 --> 00:09:53,140
want to go into it.

88
00:09:53,140 --> 00:09:57,700
If you want to read the Vinaya, read the Vinaya, but he gets, because it's just, I'd have

89
00:09:57,700 --> 00:10:04,980
to read the English, I don't have, why do I have the English elsewhere, I don't want

90
00:10:04,980 --> 00:10:05,980
to get too much into it.

91
00:10:05,980 --> 00:10:14,180
But at some point he says, uchay dawad, or bhavangotamu, and this is, then it repeats what

92
00:10:14,180 --> 00:10:16,500
we have in the Anghutta.

93
00:10:16,500 --> 00:10:23,380
So he asks, are you teach the annihilation, do you teach annihilationism?

94
00:10:23,380 --> 00:10:27,100
And the Buddha says, well, there is something that you should annihilate in Buddhism.

95
00:10:27,100 --> 00:10:39,780
So he sidesteps the question, he's the play on words or it gives another meaning to it.

96
00:10:39,780 --> 00:10:43,900
And that is that we call for the annihilation of green anger and delusion.

97
00:10:43,900 --> 00:10:52,060
The whole section is worth reading a kiriyawada, do you teach the non, do you teach inaction?

98
00:10:52,060 --> 00:10:56,060
Because there were people who taught at the time that the way to become enlightened is

99
00:10:56,060 --> 00:11:03,580
to not do anything, to stop doing anything, don't eat, don't walk, lie down and die.

100
00:11:03,580 --> 00:11:07,340
That's the best way, the giants taught this sort of thing, a kiriyawada.

101
00:11:07,340 --> 00:11:16,340
Or, no, maybe a kiriyawada is, anyway, there are different kiriyawada, there's no karma,

102
00:11:16,340 --> 00:11:21,700
there's no result of karma, I'm not sure, but there were different views.

103
00:11:21,700 --> 00:11:26,180
And the Buddha says, well, I teach the non-doing of any evil, don't do any bodily

104
00:11:26,180 --> 00:11:31,820
misteeds, don't do any verbal misteeds, don't do any mental misteeds, that's the kind

105
00:11:31,820 --> 00:11:50,060
of non-action that I teach, but so the Buddha doesn't teach the annihilation at the moment

106
00:11:50,060 --> 00:11:51,060
of death.

107
00:11:51,060 --> 00:11:58,700
On the other hand, it's an easy one to misunderstand because he doesn't teach the existence

108
00:11:58,700 --> 00:12:15,220
of a self to become annihilated, so anyway, it gets into complicated explanations, but

109
00:12:15,220 --> 00:12:21,220
really that's it, I mean, Buddhism is very practical, it's all about doing good deeds, not

110
00:12:21,220 --> 00:12:26,340
doing evil deeds, it's what you hear in other religions, but other religions have

111
00:12:26,340 --> 00:12:34,500
so much baggage, looking at you, Christianity, Christianity is a lot of, it's good teachings,

112
00:12:34,500 --> 00:12:41,300
you know, powerful love, kindness, goodness, but there's so much baggage, even from just

113
00:12:41,300 --> 00:12:46,900
Jesus himself, it's not like it came later, you have to believe in God and you have to

114
00:12:46,900 --> 00:12:58,180
praise God and you have to, then I don't know, then you have all the rules, I posted a quote

115
00:12:58,180 --> 00:13:05,060
recently where Jesus said, let he who is without sin cast the first stone, they're going

116
00:13:05,060 --> 00:13:13,900
to stone this woman and Jesus is basically before you accuse her, take a look at yourself.

117
00:13:13,900 --> 00:13:22,260
And someone else pointed out that in another part of the Bible, it's all accusatory,

118
00:13:22,260 --> 00:13:35,820
but you know, homosexuals don't go to heaven and yeah, and so on, so yeah, I mean, absolutely

119
00:13:35,820 --> 00:13:45,460
I'm not a Christian apologist, but Buddhism has all those good things and in the end,

120
00:13:45,460 --> 00:13:50,820
it comes down to just goodness, even meditation is just considered the highest purest form

121
00:13:50,820 --> 00:13:56,140
of goodness, like giving gifts can have can be done with impure thought, keeping morality

122
00:13:56,140 --> 00:14:02,540
can still be done impurely with ego and so on, even certain types of meditation can be

123
00:14:02,540 --> 00:14:09,660
performed with ego, but inside meditation mindfulness can't be accompanied by ego, can't

124
00:14:09,660 --> 00:14:11,540
be accompanied by the highest on this.

125
00:14:11,540 --> 00:14:23,660
So it's the greatest purest goodness.

126
00:14:23,660 --> 00:14:28,180
So a greed anger and delusion, those are the three things that lead to badness.

127
00:14:28,180 --> 00:14:35,940
But when we get rid of them, we have no more badness, only goodness.

128
00:14:35,940 --> 00:14:39,940
Do you have any questions today?

129
00:14:39,940 --> 00:14:40,940
You do.

130
00:14:40,940 --> 00:14:48,820
Just want me to go back to the earlier ones or I don't know, are there any good ones?

131
00:14:48,820 --> 00:14:51,180
No, let's say this is a long one.

132
00:14:51,180 --> 00:14:57,100
I have a theoretical question about transcending the lack of a better word to some sire.

133
00:14:57,100 --> 00:15:03,940
If one has given up desire and is basically has no reproductive value as an organism, making

134
00:15:03,940 --> 00:15:08,420
it kind of pointless to rebirth because the same thing will happen in the next slide,

135
00:15:08,420 --> 00:15:10,780
ultimate suicide as I see.

136
00:15:10,780 --> 00:15:15,540
How do we know that what will have, how do we know what will happen to such consciousness?

137
00:15:15,540 --> 00:15:22,140
What information do we have on the subject and are there any flaws?

138
00:15:22,140 --> 00:15:23,140
Sorry.

139
00:15:23,140 --> 00:15:27,500
Is there what you mean when it moves down to the bottom?

140
00:15:27,500 --> 00:15:28,500
Yeah.

141
00:15:28,500 --> 00:15:30,300
It just flies off the screen.

142
00:15:30,300 --> 00:15:38,700
Are there any flaws in my thinking of the power in context of what I said?

143
00:15:38,700 --> 00:16:00,620
I mean, I can parse it down to a Buddhist question and answer that.

144
00:16:00,620 --> 00:16:07,420
So part where you say if one has given up desire, so if someone has no more desire, someone

145
00:16:07,420 --> 00:16:16,420
has no more desire, then yes, they are in our heart and they will just not be reborn

146
00:16:16,420 --> 00:16:20,540
because the causes for consciousness are limited.

147
00:16:20,540 --> 00:16:24,100
Consciousness can be caused by physical causes.

148
00:16:24,100 --> 00:16:34,260
It can be caused by functional mental causes and it can be caused by wholesome and unwholesome

149
00:16:34,260 --> 00:16:36,700
mental causes, which are karmic.

150
00:16:36,700 --> 00:16:38,780
It can be caused by karma.

151
00:16:38,780 --> 00:16:46,460
So functionally, I believe consciousness can create consciousness, but that functionality I think

152
00:16:46,460 --> 00:16:52,460
is still dependent on them, no, not really, but it's limited.

153
00:16:52,460 --> 00:17:03,460
At the moment of death, the only process that is able to continue because the physical body

154
00:17:03,460 --> 00:17:13,020
breaks down and loses its ability to create consciousness, the only power that can continue

155
00:17:13,020 --> 00:17:17,300
to create consciousness is wholesome and unwholesome intentions.

156
00:17:17,300 --> 00:17:19,100
Now an arrow hand doesn't have either.

157
00:17:19,100 --> 00:17:24,660
They don't have any intention for future becoming, which requires desire.

158
00:17:24,660 --> 00:17:28,980
So they have no desires, they have no hopes, they have no wants.

159
00:17:28,980 --> 00:17:40,500
Because of that, there is the ending of the succession of experiences.

160
00:17:40,500 --> 00:17:43,700
So that just happens naturally.

161
00:17:43,700 --> 00:18:00,660
It must be the difference between Vitaka, her life thought, and Vulikara sustained thought.

162
00:18:00,660 --> 00:18:05,460
If attention is always applied for one moment at a time, what is sustained?

163
00:18:05,460 --> 00:18:08,460
Thank you, Manti.

164
00:18:08,460 --> 00:18:30,740
So Vitaka is, no, I don't really know, that's a good question because you can't say

165
00:18:30,740 --> 00:18:34,060
one of them is sustained, it's not true.

166
00:18:34,060 --> 00:18:38,420
But it's something like the Vitaka is the going to the object.

167
00:18:38,420 --> 00:18:45,020
It's like starting from zero again, so the flavor of the consciousness would be, you

168
00:18:45,020 --> 00:18:52,860
would have to, there's a repeated mind that adverts to the consciousness.

169
00:18:52,860 --> 00:19:00,660
I think it's, what you really should do is look at the Abidama mind states for the first

170
00:19:00,660 --> 00:19:08,260
jhana and see exactly what's going on there, because the, the, the suited description, I think

171
00:19:08,260 --> 00:19:19,460
is, I'm not sure if it's conventional or not, Vitaka and Vitaka and Vitaka.

172
00:19:19,460 --> 00:19:25,940
But there's something, there's a sense that Vitaka is still moving, Vitaka is still

173
00:19:25,940 --> 00:19:29,340
not fixed on the object.

174
00:19:29,340 --> 00:19:45,820
So the quality of consciousness will be less, less fixed or certain, I don't know.

175
00:19:45,820 --> 00:19:49,380
I don't, you know, it's not something we, that's not something we deal with that much in

176
00:19:49,380 --> 00:19:54,980
this tradition, but I wouldn't worry too much about it, because in the suit, especially

177
00:19:54,980 --> 00:19:58,660
the Buddha puts them together, Vitaka and Vitaka and Vitaka, and they disappear together

178
00:19:58,660 --> 00:20:00,340
in the suit does.

179
00:20:00,340 --> 00:20:04,620
So you consider that like the thinking about the object, when you enter the second jhana,

180
00:20:04,620 --> 00:20:10,420
that, that discursive thought or that, that thinking about the object disappears.

181
00:20:10,420 --> 00:20:20,180
When I finish meditating, I kind of work calm for a while, about twice as long as I meditate,

182
00:20:20,180 --> 00:20:23,900
and slowly we'll lose that calm the longer I'm not as mindful.

183
00:20:23,900 --> 00:20:28,580
This effect I'm feeling is, is it cumulative based on how long I meditate?

184
00:20:28,580 --> 00:20:50,140
You know, I don't want to encourage the attachment to such calm states, but it's not really,

185
00:20:50,140 --> 00:20:55,060
what you should have is an overall general sense of greater calm in your life and greater

186
00:20:55,060 --> 00:20:56,060
stability.

187
00:20:56,060 --> 00:21:03,620
Of course, that too will fade.

188
00:21:03,620 --> 00:21:10,780
That may be what you're referring to, but you think it would last longer than that.

189
00:21:10,780 --> 00:21:15,940
It's more generally, you know, you cultivate the habit of being able to deal with things,

190
00:21:15,940 --> 00:21:16,940
which will fade.

191
00:21:16,940 --> 00:21:17,940
It's true.

192
00:21:17,940 --> 00:21:22,180
If you don't practice meditation every day, it'll be worse the next day, it'll be less

193
00:21:22,180 --> 00:21:24,260
powerful the next day.

194
00:21:24,260 --> 00:21:31,020
And if you have a real sense of calm, it may just be incidental or having to do with

195
00:21:31,020 --> 00:21:35,540
your strong state of concentration, but you have to be careful with it because it can

196
00:21:35,540 --> 00:21:37,140
lead to the liking of it.

197
00:21:37,140 --> 00:21:42,300
And if you like it, then you want it and you aspire towards it, and then when you don't

198
00:21:42,300 --> 00:21:57,420
get it, you're dissatisfied and so on.

199
00:21:57,420 --> 00:22:00,900
One day, I was thinking about your early exposure to Buddhist life that you were talking

200
00:22:00,900 --> 00:22:01,900
about yesterday.

201
00:22:01,900 --> 00:22:06,820
Was there a certain event or realization that elicited your devotion to the Dharma or was

202
00:22:06,820 --> 00:22:08,460
more gradual?

203
00:22:08,460 --> 00:22:15,340
I'm not allowed to talk about my realization, sorry.

204
00:22:15,340 --> 00:22:20,780
I mean, if any potential realizations, should I have them?

205
00:22:20,780 --> 00:22:28,820
I could talk, we're allowed to talk about it amongst monks together, and we do, there was

206
00:22:28,820 --> 00:22:34,140
one monk who used to come up and say, how many minutes have you had said so?

207
00:22:34,140 --> 00:22:42,020
I'm going to really say what he got like 12 hours or something like that.

208
00:22:42,020 --> 00:22:45,100
Some monks do talk about it, not money.

209
00:22:45,100 --> 00:22:49,500
I think he was a wonderful monk.

210
00:22:49,500 --> 00:23:00,300
I really liked him, but he was a little bit wacky, big fat guy, big guy, and sorry, fat

211
00:23:00,300 --> 00:23:04,460
is actually not an insult in Thailand, not as much as it is in the West.

212
00:23:04,460 --> 00:23:12,700
I don't know how, whether it's rude to say he was fat, but he was big, but really not

213
00:23:12,700 --> 00:23:20,700
so fat actually, just big, and he could walk like 30 kilometers or something.

214
00:23:20,700 --> 00:23:27,260
He was the monk, I walked at night with for hours before my teacher said, that's just

215
00:23:27,260 --> 00:23:33,940
exercise, it's an exercise, a bit final, it's a Western way, exercise, it's not walking

216
00:23:33,940 --> 00:23:36,940
meditation.

217
00:23:36,940 --> 00:23:47,460
So, yeah, he was just, I mean, a lot of interesting people in the Thai, monastic, in the

218
00:23:47,460 --> 00:23:58,700
Thai, sangha, feeling such as pain or itching or eyes is a better turn out than with more

219
00:23:58,700 --> 00:24:04,980
specifically, like pain and itching or more general labels, like feeling.

220
00:24:04,980 --> 00:24:11,020
I would prefer pain and itching.

221
00:24:11,020 --> 00:24:15,380
Feeling is more for when you're not sure, you just get a feeling out of it, you don't

222
00:24:15,380 --> 00:24:19,500
have any worse, specific word for it, and if you have a specific word, you're better

223
00:24:19,500 --> 00:24:21,780
off seeing pain or itching.

224
00:24:21,780 --> 00:24:27,300
One thing lately, when I meditate, I notice thoughts that seem to arise in my mind without

225
00:24:27,300 --> 00:24:28,300
any cause.

226
00:24:28,300 --> 00:24:32,980
Their appearance seems to be so random and inexplicable that I cannot discern what causes

227
00:24:32,980 --> 00:24:35,140
them to arise in my mind.

228
00:24:35,140 --> 00:24:39,340
Does this observation represent anything about my meditation practice?

229
00:24:39,340 --> 00:24:46,260
For example, maybe I'm just not mindful enough to see the cause of these thoughts arising?

230
00:24:46,260 --> 00:24:50,180
We're not worried about the cause, we're just worried about the nature.

231
00:24:50,180 --> 00:24:54,700
Cause and effect is only an early knowledge, it's not really important in the long run.

232
00:24:54,700 --> 00:24:59,180
Eventually karma doesn't become important at all, what becomes important is the nature

233
00:24:59,180 --> 00:25:00,180
of things.

234
00:25:00,180 --> 00:25:05,500
See, the whole idea of the formable truth is that we cling to things, thinking that

235
00:25:05,500 --> 00:25:09,300
they are pleasant, thinking that they are going to satisfy us.

236
00:25:09,300 --> 00:25:13,740
Once we realize that they aren't going to satisfy us, because as you can see, they arise

237
00:25:13,740 --> 00:25:17,820
in seas and they arise in seas constantly.

238
00:25:17,820 --> 00:25:23,940
Therefore you let go of them, you stop craving for them.

239
00:25:23,940 --> 00:25:24,940
So don't worry about it.

240
00:25:24,940 --> 00:25:29,660
The fact that you're seeing these thoughts, what that's showing you is impermanence

241
00:25:29,660 --> 00:25:31,220
and non-self.

242
00:25:31,220 --> 00:25:35,380
You could say it's also showing you suffering, but mainly there you're seeing impermanence

243
00:25:35,380 --> 00:25:39,100
and that things are arising and you're seeing non-self and that you didn't cause those

244
00:25:39,100 --> 00:25:42,540
thoughts to arise.

245
00:25:42,540 --> 00:25:47,700
So it's actually a great thing to see and the more you see that, the more the less clinging

246
00:25:47,700 --> 00:25:48,700
you're going to have.

247
00:25:48,700 --> 00:25:55,740
It's a sign that you've got fairly good concentration to be able to see that.

248
00:25:55,740 --> 00:26:01,340
This is kind of a silly question, but what might I trust you'd want you to, when the feeling

249
00:26:01,340 --> 00:26:08,140
of bloating arises, rising, bloating?

250
00:26:08,140 --> 00:26:10,700
Even your sound is getting strange.

251
00:26:10,700 --> 00:26:11,700
Oh.

252
00:26:11,700 --> 00:26:22,580
Yeah, I think that's actually out on the road, I think there's some big vehicle going

253
00:26:22,580 --> 00:26:23,580
by.

254
00:26:23,580 --> 00:26:24,580
Okay.

255
00:26:24,580 --> 00:26:30,100
Because your sound is a little distorted as well, it's not as clear or this is a different

256
00:26:30,100 --> 00:26:31,100
headset, right?

257
00:26:31,100 --> 00:26:32,100
Yeah.

258
00:26:32,100 --> 00:26:37,460
Well, the same one that I had last night, the other one was working.

259
00:26:37,460 --> 00:26:38,460
Okay.

260
00:26:38,460 --> 00:26:41,060
It's not giving the hissing, so that's good.

261
00:26:41,060 --> 00:26:44,060
It's just a problem.

262
00:26:44,060 --> 00:26:49,380
Sorry, what was the question?

263
00:26:49,380 --> 00:26:50,460
I'm bloating, right?

264
00:26:50,460 --> 00:27:01,140
If you feel bloated, you can say feeling, feeling or bloated, bloated.

265
00:27:01,140 --> 00:27:04,260
Feeling works fine there because otherwise it gets a little awkward.

266
00:27:04,260 --> 00:27:09,220
You actually feel something called bloated, or you do just have a feeling that you know

267
00:27:09,220 --> 00:27:14,780
is caused by bloated bloating.

268
00:27:14,780 --> 00:27:20,660
Feeling is fine there.

269
00:27:20,660 --> 00:27:22,580
Are all enlightenment the same?

270
00:27:22,580 --> 00:27:29,500
In other words, if it is explained, attained through Zen or Tibetan Buddhism, are they different?

271
00:27:29,500 --> 00:27:34,220
Curtis, I'm going to click on your profile and yes, you've never meditated with

272
00:27:34,220 --> 00:27:35,500
us.

273
00:27:35,500 --> 00:27:44,140
So I'm going to ask you to read my booklet on how to meditate and take some time to meditate

274
00:27:44,140 --> 00:27:49,020
and you know, you don't have to, but that this community is a meditation community in our

275
00:27:49,020 --> 00:27:50,020
tradition.

276
00:27:50,020 --> 00:27:59,220
Your question isn't bad, I just, it made me think this is probably someone who's not meditating

277
00:27:59,220 --> 00:28:00,220
with us.

278
00:28:00,220 --> 00:28:13,540
And it's actually not a bad question, so the answer, it's just a hard to answer, no.

279
00:28:13,540 --> 00:28:14,540
It depends who you ask.

280
00:28:14,540 --> 00:28:20,980
Each tradition is going to have its own idea of enlightenment, in fact, each tradition

281
00:28:20,980 --> 00:28:25,940
in every type of Buddhism is going to have its own way of dealing with enlightenment,

282
00:28:25,940 --> 00:28:27,580
understanding enlightenment.

283
00:28:27,580 --> 00:28:31,700
Sometimes they are similar, sometimes they are the same, sometimes they are drastically

284
00:28:31,700 --> 00:28:39,860
different, sometimes some traditions feel it's selfish to become enlightened and they

285
00:28:39,860 --> 00:28:50,140
want to stay unenlightened or they say, anyway, they go different ideas.

286
00:28:50,140 --> 00:28:55,460
Just a question that came in yesterday, just as we were signing off, someone was asking

287
00:28:55,460 --> 00:29:00,980
if you would be interested in a donation of books written by the Dalai Lama or if that would

288
00:29:00,980 --> 00:29:03,780
not be appropriate for the monster.

289
00:29:03,780 --> 00:29:10,140
Not really interested, okay, I don't know, no, not really.

290
00:29:10,140 --> 00:29:16,740
We don't have that much room for books, so whether it's related to our tradition or we don't

291
00:29:16,740 --> 00:29:18,540
need it.

292
00:29:18,540 --> 00:29:25,020
One thing, should I practice walking then sitting meditation as you teach in your booklet

293
00:29:25,020 --> 00:29:32,060
for some allotted time before I go into the chapter on Fundamentals or Mindful Prostriction?

294
00:29:32,060 --> 00:29:36,980
Seth, you have to do Robin a favor and put the question in the question mark in front

295
00:29:36,980 --> 00:29:40,180
of your message.

296
00:29:40,180 --> 00:29:46,860
So before you type in the message, click that green question mark, puts a Q colon space

297
00:29:46,860 --> 00:29:52,460
in front of your thing and then Robin can easily see that it's a question.

298
00:29:52,460 --> 00:30:00,740
No, you can read the whole book.

299
00:30:00,740 --> 00:30:06,260
I would read at least the first four chapters, right?

300
00:30:06,260 --> 00:30:10,780
The fourth chapter is on Fundamentals.

301
00:30:10,780 --> 00:30:17,100
Yes, I think that's what he's asking after the should he go out of you.

302
00:30:17,100 --> 00:30:26,020
So you should read, it's true, actually, if you read the first three chapters and then

303
00:30:26,020 --> 00:30:29,820
practice them for some time, it'll make more sense because the Fundamentals doesn't really

304
00:30:29,820 --> 00:30:33,660
make that much sense until you actually try it.

305
00:30:33,660 --> 00:30:36,660
That is a good point, yeah, that makes sense.

306
00:30:36,660 --> 00:30:40,420
Practice the first three for a while, but doesn't have to be that long because the fourth

307
00:30:40,420 --> 00:30:48,940
chapter is quite important and then the last chapter, the sixth chapter, you have to learn

308
00:30:48,940 --> 00:30:50,180
that one.

309
00:30:50,180 --> 00:30:53,940
In fact, you know, read it all through.

310
00:30:53,940 --> 00:30:57,500
I would recommend reading the whole book through, then start to practice.

311
00:30:57,500 --> 00:31:01,060
You know, if you have time, if not, you can do them.

312
00:31:01,060 --> 00:31:05,180
Read one chapter practice, read another chapter practice, but read it all as soon as you

313
00:31:05,180 --> 00:31:09,660
have the time to do so, it's not that long, then practice and then go back to the fourth

314
00:31:09,660 --> 00:31:14,980
chapter, you know, it's the one, the fourth and I guess the sixth as well, because they're

315
00:31:14,980 --> 00:31:21,740
the ones that you have to remember things from concepts, the first chapter I suppose as

316
00:31:21,740 --> 00:31:25,420
well, because it's the one that's got the fourth study botanine, the four foundations

317
00:31:25,420 --> 00:31:37,780
of mindfulness, it's not enough a book, is that a criticism of it or does that just mean

318
00:31:37,780 --> 00:31:45,700
it's not enough to call it a book, I don't think he's critical, do you think it should

319
00:31:45,700 --> 00:31:46,700
be longer?

320
00:31:46,700 --> 00:31:47,700
No.

321
00:31:47,700 --> 00:31:55,820
No, it's good enough, no, we have a second volume coming out sometime in the uncertain

322
00:31:55,820 --> 00:31:56,820
future.

323
00:31:56,820 --> 00:31:57,820
Yeah.

324
00:31:57,820 --> 00:32:02,660
If I don't know, since we don't just read about meditation, I never hit around to meditation,

325
00:32:02,660 --> 00:32:05,380
so a nice booklet is just the right size.

326
00:32:05,380 --> 00:32:10,180
Yeah, you give people very little and push them and say, I'm not telling you any more until

327
00:32:10,180 --> 00:32:17,420
you meditate, you don't want one of these thick books on sati botanine thinking, why am

328
00:32:17,420 --> 00:32:22,180
I going to read that for the heart of Buddhist meditation is like this thick, I mean, it's

329
00:32:22,180 --> 00:32:28,300
a good book, but Mahasi Sai does books on meditation are very thin and that you can't read

330
00:32:28,300 --> 00:32:32,740
them through because you get a little ways and you think, okay, I better go meditate.

331
00:32:32,740 --> 00:32:36,380
That's a book, when you read a book and you can't finish it because you feel guilty

332
00:32:36,380 --> 00:32:39,900
because you're not meditating.

333
00:32:39,900 --> 00:32:48,340
That's what's great about Mahasi Sai does books, I can never read them through.

334
00:32:48,340 --> 00:33:03,540
I'm not sure if it's a question or a comment, I'm not sure if I'm right, but any way

335
00:33:03,540 --> 00:33:15,660
I'm not sure if that's a comment or a question, if it is a question, maybe you can just

336
00:33:15,660 --> 00:33:20,820
type it in again, just go on.

337
00:33:20,820 --> 00:33:25,500
And like this in life, there is the seeing, then the wanting is suffering the only reason

338
00:33:25,500 --> 00:33:48,900
that to pursue, I'm sorry, maybe my mind is blown is fried, but I can't, can't do that.

339
00:33:48,900 --> 00:33:54,860
Is suffering the only reason not to pursue what you want or want to?

340
00:33:54,860 --> 00:34:09,020
Yes, suffering is the reason not to pursue, it's not worth one thing, so we've got these

341
00:34:09,020 --> 00:34:13,020
neat little hands on the right hand side, you can click if you like people's comments

342
00:34:13,020 --> 00:34:18,380
or questions, don't see many people doing it.

343
00:34:18,380 --> 00:34:31,180
We'd have to make it a little bit different, look a little different.

344
00:34:31,180 --> 00:34:39,300
I don't think it was criticism of the book just encouraging somebody to read it because it

345
00:34:39,300 --> 00:34:57,780
doesn't take any longer to read than a typical podcast.

346
00:34:57,780 --> 00:35:11,580
But I said George, George don't send them to that links and people.org, it's a hot link

347
00:35:11,580 --> 00:35:17,900
to a page.

348
00:35:17,900 --> 00:35:27,700
Should work right, HTM, are you typing it wrong anyway, well, you need to put the HTTP before

349
00:35:27,700 --> 00:35:41,420
it will come up as a link.

350
00:35:41,420 --> 00:35:45,700
So how many languages is the book transcribed into now?

351
00:35:45,700 --> 00:35:52,940
Which reminds me, someone just did a Swedish translation that I have to put up, forgot about

352
00:35:52,940 --> 00:35:53,940
it, where is that?

353
00:35:53,940 --> 00:35:58,500
I think that's on Facebook or something.

354
00:35:58,500 --> 00:36:03,100
So yeah, a big announcement that I should have told everyone weeks ago or a week at

355
00:36:03,100 --> 00:36:04,100
least.

356
00:36:04,100 --> 00:36:08,300
We have the Swedish translation, so there you go.

357
00:36:08,300 --> 00:36:13,980
Are there other languages that you're looking for or are you going to that link that

358
00:36:13,980 --> 00:36:20,020
you just posted, Robin, you'll see translations in progress?

359
00:36:20,020 --> 00:36:25,940
And, you know, progress is a bit loose, there's a bit loosely there, I'm not sure how

360
00:36:25,940 --> 00:36:28,300
many of them are actually progressing.

361
00:36:28,300 --> 00:36:37,300
Okay, so the Hindi one is supposed to be done, but I don't know what happened to that.

362
00:36:37,300 --> 00:36:43,980
So if I chance someone was able to translate into Croatian, check, finish, Persian,

363
00:36:43,980 --> 00:36:45,780
Polish, or time?

364
00:36:45,780 --> 00:36:50,060
Well, they might want to get in touch with me because we have at least partial translations

365
00:36:50,060 --> 00:36:52,100
of some of those.

366
00:36:52,100 --> 00:36:53,100
So don't do it.

367
00:36:53,100 --> 00:36:59,260
If you have in language that's not on either of the lists, that is something that is definitely

368
00:36:59,260 --> 00:37:04,980
interesting of interest, you know, adding languages to the list.

369
00:37:04,980 --> 00:37:11,380
But ones that are already underway, we should get in touch with us first because it might

370
00:37:11,380 --> 00:37:16,580
already be underway.

371
00:37:16,580 --> 00:37:28,700
Okay, Dante, I hope it's okay that I made a video of how to meditate reading from your

372
00:37:28,700 --> 00:37:31,500
booklet for those who have trouble reading.

373
00:37:31,500 --> 00:37:36,260
I went into the Gaya, we did not cheat to end down this a bit more explaining what they

374
00:37:36,260 --> 00:37:39,620
mean from my own sake, but remember, I hope it's okay.

375
00:37:39,620 --> 00:37:43,540
I'll make sure to add them to the book as well.

376
00:37:43,540 --> 00:37:48,020
We made a book, not an audio, a book, right, or not an audio book.

377
00:37:48,020 --> 00:38:01,860
I think an audio book, a video, to a video, I'm sorry.

378
00:38:01,860 --> 00:38:07,620
The hands together on the right are showing a maximum of one, that's interesting.

379
00:38:07,620 --> 00:38:16,860
Yeah, that's fine, sure, make videos absolutely, it's in the public domain, so do what you

380
00:38:16,860 --> 00:38:18,260
want with it.

381
00:38:18,260 --> 00:38:23,260
Apparently, I said that about the Chaco Poly, you know, the Chaco Poly workbook, apparently

382
00:38:23,260 --> 00:38:25,460
someone's selling it on Amazon.

383
00:38:25,460 --> 00:38:38,260
Really, it's got to be that comma, yeah, okay, so maybe those hand things on the right

384
00:38:38,260 --> 00:38:39,260
aren't working properly.

385
00:38:39,260 --> 00:38:40,260
I don't know.

386
00:38:40,260 --> 00:38:42,700
Well, we can get one on there.

387
00:38:42,700 --> 00:39:03,780
I think I'm going to figure out how to put the Swedish translation up and why the hand

388
00:39:03,780 --> 00:39:08,780
things aren't working, but first, I'm going to say goodnight.

389
00:39:08,780 --> 00:39:09,780
Thank you, Bhante.

390
00:39:09,780 --> 00:39:15,220
Thank you, everyone, for tuning in, and we'll see you all tomorrow, bye.

