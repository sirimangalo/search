1
00:00:30,000 --> 00:00:52,720
Okay, so here we are coming to the Srimahambodhi tree in Anarada Pura, where we will

2
00:00:52,720 --> 00:00:57,800
continue our meditation practice, those of you who are on meditation course and those of

3
00:00:57,800 --> 00:01:05,960
us who are staying at the monastery and as long term meditators, we've come here to see

4
00:01:05,960 --> 00:01:12,360
one of the most important Buddhist sites that exist in the world who could save the

5
00:01:12,360 --> 00:01:17,360
most important Buddhist side outside of India.

6
00:01:17,360 --> 00:01:26,320
This is the site where the Bodhi tree that was taken from India was planted and that's

7
00:01:26,320 --> 00:01:32,800
the Bodhi tree that you've seen behind me.

8
00:01:32,800 --> 00:01:37,400
And more importantly, it's the Bodhi tree that was brought back to India, to Bongayah

9
00:01:37,400 --> 00:01:44,240
where the original Bodhi tree stood when it was killed.

10
00:01:44,240 --> 00:01:51,040
There were various stories about how the Bodhi tree was killed and was brought back at

11
00:01:51,040 --> 00:01:57,000
least on one occasion too, seems on several occasions was brought back to India to be

12
00:01:57,000 --> 00:01:59,040
replanted.

13
00:01:59,040 --> 00:02:07,160
So this is actually the Bodhi ancestor and both the descendant and the ancestor of the Bodhi

14
00:02:07,160 --> 00:02:10,240
tree in India, since it was brought back to India.

15
00:02:10,240 --> 00:02:18,960
It's a very important place and has great significance for both the Sri Lankan Buddhists

16
00:02:18,960 --> 00:02:22,760
and Buddhist around the world.

17
00:02:22,760 --> 00:02:34,800
So today I thought I'd talk a little bit about what this means for us and what exactly

18
00:02:34,800 --> 00:02:40,000
the Bodhi tree means for Buddhists.

19
00:02:40,000 --> 00:02:47,920
Because on the one hand it doesn't mean anything, it's just a symbol, it's just a physical

20
00:02:47,920 --> 00:02:59,560
manifestation that occurred as a result of the Bodhi and his teachings and the followers

21
00:02:59,560 --> 00:03:06,120
who decided to bring the tree here to Sri Lanka as a symbol for the planting of Buddhism

22
00:03:06,120 --> 00:03:09,320
in this country.

23
00:03:09,320 --> 00:03:11,960
But in the end it is really just a tree.

24
00:03:11,960 --> 00:03:16,040
It doesn't have consciousness.

25
00:03:16,040 --> 00:03:21,240
And so some people might not understand even people who practice the Buddhist teachings,

26
00:03:21,240 --> 00:03:33,640
might not understand the significant or the importance of the tree.

27
00:03:33,640 --> 00:03:50,000
But on the other hand it really is a focusing entity, just as meditation objects are things

28
00:03:50,000 --> 00:03:56,880
that focus our attention, bring our minds back to a single pointedness, keep our minds

29
00:03:56,880 --> 00:04:05,120
from being distracted, keep our minds from being pulled away by the objects of the sense,

30
00:04:05,120 --> 00:04:16,320
pulled away by the world, pulled away by worldliness.

31
00:04:16,320 --> 00:04:23,520
And so for a cynical person they might say it's simply a physical object, really is meaningless

32
00:04:23,520 --> 00:04:31,360
and that people who come here to worship or make determinations under the Bodhi tree make

33
00:04:31,360 --> 00:04:34,040
wishes and so on.

34
00:04:34,040 --> 00:04:36,960
People who come to meditate here are just fooling themselves, they could do the same

35
00:04:36,960 --> 00:04:43,840
at home if they really want to meditate, they can meditate at home.

36
00:04:43,840 --> 00:04:53,200
But this is really I think overly cynical and disingenuous really because our mind

37
00:04:53,200 --> 00:04:59,120
relies on concept, our whole life depends on concept.

38
00:04:59,120 --> 00:05:06,600
And so depending on the concepts that we choose, the concepts that we live by, this will

39
00:05:06,600 --> 00:05:10,560
determine our practice, it will determine our course.

40
00:05:10,560 --> 00:05:15,440
So it's one thing to say, claim to know that everything is impermanent suffering in

41
00:05:15,440 --> 00:05:16,440
non-self.

42
00:05:16,440 --> 00:05:21,840
It's only body and mind and the five aggregates and so on and it's only seeing hearings

43
00:05:21,840 --> 00:05:24,680
now in tasting feeling and thinking.

44
00:05:24,680 --> 00:05:28,560
But unless you're actually an hour hunt and actually become enlightened, you're still

45
00:05:28,560 --> 00:05:34,320
living in concept and you'll say such things and then you'll still become intoxicated

46
00:05:34,320 --> 00:05:41,960
by concept, you'll become intoxicated by pleasant sights, pleasant sounds, pleasant smells

47
00:05:41,960 --> 00:05:49,640
and so on and you'll become, you'll still become deluded by other concepts by the concepts

48
00:05:49,640 --> 00:05:56,800
of a human being, of male and female, you'll become intoxicated by food and drink and

49
00:05:56,800 --> 00:06:04,240
entertainment by beauty and so on.

50
00:06:04,240 --> 00:06:10,080
And so this concept of the tree, regardless of what you think of it, it has the effect

51
00:06:10,080 --> 00:06:14,200
of bringing our minds back to the dhamma, of reminding us of the dhamma.

52
00:06:14,200 --> 00:06:22,720
Not a better object to be the inspiration for a dhamma talk, like this one, for example.

53
00:06:22,720 --> 00:06:25,080
Without this tree, I wouldn't be giving this talk.

54
00:06:25,080 --> 00:06:33,000
I wouldn't have the significance in the meaning and this wouldn't capture the attention

55
00:06:33,000 --> 00:06:40,560
of the audience in the same way, but because this is such a holy place, it's considered

56
00:06:40,560 --> 00:06:46,720
to be such an important symbol or an important object, an important entity for Buddhist

57
00:06:46,720 --> 00:06:48,920
people.

58
00:06:48,920 --> 00:06:53,440
When we come here, we give our whole attention to the dhamma and we forget about the world,

59
00:06:53,440 --> 00:07:03,200
we forget about intoxicating sights and sounds and so on.

60
00:07:03,200 --> 00:07:08,960
So on the way up here, I was in order to prepare myself and get kind of in the mood

61
00:07:08,960 --> 00:07:15,960
myself, I was reading through the Majima Nikaya and I just happened upon the dhamma diadasuta,

62
00:07:15,960 --> 00:07:23,520
which I thought would be a good coincidentally, a good basis for this talk, to talk about

63
00:07:23,520 --> 00:07:25,680
these sorts of things.

64
00:07:25,680 --> 00:07:31,880
Because in the dhamma diadasuta, the Buddha distinguishes between someone who is an error

65
00:07:31,880 --> 00:07:37,800
of the Buddha in regards to material character, material requisite and one who is an error

66
00:07:37,800 --> 00:07:42,320
to the Buddha in regards to the dhamma.

67
00:07:42,320 --> 00:07:48,480
So the Buddha said, be an error to my dhamma, inherit my dhamma, my teaching, don't inherit

68
00:07:48,480 --> 00:08:05,480
my physical, a material wealth, my material riches, be an inheritan, be an error to my dhamma,

69
00:08:05,480 --> 00:08:10,920
my teaching.

70
00:08:10,920 --> 00:08:17,040
So this is useful to actually remind us not to take these symbols too seriously or not

71
00:08:17,040 --> 00:08:25,760
to take the object, physical object too seriously.

72
00:08:25,760 --> 00:08:29,640
So how we should see the Bodhi tree is as a symbol and as something to remind us and

73
00:08:29,640 --> 00:08:37,880
something to encourage us in a good way, we shouldn't see it as the highest goal or the

74
00:08:37,880 --> 00:08:43,000
highest object of worship, actually the highest object of worship in the Buddha's teaching

75
00:08:43,000 --> 00:08:49,640
is the dhamma, even more so than the Buddha, certainly more so than the Bodhi tree.

76
00:08:49,640 --> 00:08:52,920
The Buddha himself, when he became enlightened, he thought, who will I worship now?

77
00:08:52,920 --> 00:08:54,920
Who will I pay respect to?

78
00:08:54,920 --> 00:08:59,440
I mean, that there's no one in the world who I could possibly pay respect to, who would

79
00:08:59,440 --> 00:09:04,440
be worthy of my respect, but this dhamma, this reality that I realized, this is worthy

80
00:09:04,440 --> 00:09:06,120
of my respect.

81
00:09:06,120 --> 00:09:16,040
So even the Buddha paid respect and put the dhamma as higher than himself.

82
00:09:16,040 --> 00:09:20,080
So it would be wrong for us to come here and think that by coming to Anaradapur, by coming

83
00:09:20,080 --> 00:09:24,640
to the Bodhi tree, now we are real Buddhists, so now we're living by the dhamma or

84
00:09:24,640 --> 00:09:31,200
we're heirs to the Buddha, we're descendants of the Buddha because we come and bow down

85
00:09:31,200 --> 00:09:42,840
and they respect to the Bodhi tree, rather we should take it as a encouragement to practice

86
00:09:42,840 --> 00:09:47,000
the Buddha's teaching and become true heirs to the Buddha, true heirs to his dhamma.

87
00:09:47,000 --> 00:09:53,720
So he said in this way, the heirs to my dhamma, don't be heirs to my material requisites,

88
00:09:53,720 --> 00:10:02,120
because the Buddha, in the Buddha's time and since the time in the Buddha, there has been

89
00:10:02,120 --> 00:10:08,840
quite a great gain accumulated by the Buddha and his followers.

90
00:10:08,840 --> 00:10:15,200
If you look, this is something, this is some great gain for us to have such a beautiful

91
00:10:15,200 --> 00:10:26,720
position, Buddhist holy place, the stone and the architecture, and even the money in here

92
00:10:26,720 --> 00:10:33,440
that comes in and all of the requisites that we get by being involved with such a place

93
00:10:33,440 --> 00:10:42,680
and involved with the Buddha's teaching, it's very easy for us to become intoxicated

94
00:10:42,680 --> 00:10:45,320
by the material gain.

95
00:10:45,320 --> 00:10:49,400
So even in the Buddha's time, amongst who were living with the Buddha would find that they

96
00:10:49,400 --> 00:11:00,920
were quite well taken care of by the lay people and this can happen even more so nowadays

97
00:11:00,920 --> 00:11:05,880
when the Buddha isn't around to remind us that entire monasteries will become intoxicated

98
00:11:05,880 --> 00:11:15,200
and caught up by gain and fame and praise and esteem by the lay people, when a place like

99
00:11:15,200 --> 00:11:26,880
this you might find sometimes people becoming intoxicated by the opulence and the greatness.

100
00:11:26,880 --> 00:11:30,960
It can also happen that people become intoxicated as I said with the place itself thinking

101
00:11:30,960 --> 00:11:36,840
that by being and living in the holy place, they're somehow associated with the Buddha.

102
00:11:36,840 --> 00:11:41,520
The Buddha said like a spoon that never tastes the flavor of the soup, it doesn't matter

103
00:11:41,520 --> 00:11:43,640
how close you get.

104
00:11:43,640 --> 00:11:50,040
You could live under the Bodhi tree like the birds in the monkeys and the squirrels and

105
00:11:50,040 --> 00:11:53,760
still never become enlightened like them.

106
00:11:53,760 --> 00:11:57,560
It's just like those people who believe that by bathing in the river Ganges, the Ganga

107
00:11:57,560 --> 00:12:03,280
river, by bathing in the river you become purified and the Buddha said well in that case

108
00:12:03,280 --> 00:12:11,920
why aren't the turtles and the fishes all purified, why aren't they enlightened as well.

109
00:12:11,920 --> 00:12:17,640
And so the material is just material, in the end it is just rope-by-nana and it's something

110
00:12:17,640 --> 00:12:21,400
that we should see that in this way.

111
00:12:21,400 --> 00:12:27,960
And so the Buddha described this, why this soup I think is a really good basis for our discussion

112
00:12:27,960 --> 00:12:34,640
today is because the Buddha used this as a basis to remind the monks or tell them, relate

113
00:12:34,640 --> 00:12:38,280
to the monks, his own search.

114
00:12:38,280 --> 00:12:43,280
And this also helps to explain to the audience and people listening, what is the significance

115
00:12:43,280 --> 00:12:44,280
of the Bodhi tree?

116
00:12:44,280 --> 00:12:45,280
Where does it come from?

117
00:12:45,280 --> 00:12:51,800
Where does the Buddha explain in the Dhamma Dayada soup to give one explanation of where

118
00:12:51,800 --> 00:13:00,000
the dog came from, who he was and what his search was?

119
00:13:00,000 --> 00:13:04,800
He said before I became enlightened, before he became enlightened, he also was searching

120
00:13:04,800 --> 00:13:09,440
for things, for material things.

121
00:13:09,440 --> 00:13:15,640
He was also intoxicated by worldly things.

122
00:13:15,640 --> 00:13:23,320
And the way he put it was like this, when I was unenlightened, the things I was looking for

123
00:13:23,320 --> 00:13:34,000
were caught up with so much suffering and so much danger.

124
00:13:34,000 --> 00:13:38,960
He said, myself being subject to birth, I was seeking after things subject to birth.

125
00:13:38,960 --> 00:13:44,720
My self-subject to old age, I was seeking after things subject to aging.

126
00:13:44,720 --> 00:13:48,200
My self-subject to sickness, I was seeking out things subject to sickness.

127
00:13:48,200 --> 00:13:53,760
My self-subject to death, I was seeking after things subject to death.

128
00:13:53,760 --> 00:14:04,520
It was ridiculous on both sides, my self-subject to sadness, sorrow, suffering.

129
00:14:04,520 --> 00:14:13,000
My self-subject to defilement, I was seeking after things subject to defilement.

130
00:14:13,000 --> 00:14:19,200
So the realization that he got was that this is absurd on both sides.

131
00:14:19,200 --> 00:14:28,680
Even if the things you were seeking after were permanent, you yourself are subject to aging.

132
00:14:28,680 --> 00:14:32,480
You know, yourself are subject to death, so you can't keep them.

133
00:14:32,480 --> 00:14:37,600
But not in none of them are themselves permanent, so either you die first or the things

134
00:14:37,600 --> 00:14:43,840
that you cling to disappear from.

135
00:14:43,840 --> 00:14:46,600
You yourself are defile, you cling to other things that are defile.

136
00:14:46,600 --> 00:14:52,600
You cling to people, you cling to your spouse, you cling to your family, to your friends,

137
00:14:52,600 --> 00:15:00,360
you cling to places, you cling to money, you cling to wealth and possessions.

138
00:15:00,360 --> 00:15:07,360
And all of these things were subject to defilement, subject to sickness, subject to death,

139
00:15:07,360 --> 00:15:10,160
subject to disappearance.

140
00:15:10,160 --> 00:15:15,560
If it's people, then they get old sick and die.

141
00:15:15,560 --> 00:15:21,160
And this is called the Anariya Parigasana, the ignobo quest.

142
00:15:21,160 --> 00:15:27,320
And he realized that this was a quest that was not of any benefit.

143
00:15:27,320 --> 00:15:32,200
It was actually in Hindu and Buddhist culture, as the Buddha was coming from a Hindu culture.

144
00:15:32,200 --> 00:15:37,240
He would have realized that, actually, this isn't just a one-time thing.

145
00:15:37,240 --> 00:15:44,520
This is a cycle where continuing this cycle, it was something that was actually quite well-known

146
00:15:44,520 --> 00:15:49,280
at the time, that this was the cycle of Reber.

147
00:15:49,280 --> 00:15:56,320
If a person clings to these things, it's not just death once, it's death again and again and again.

148
00:15:56,320 --> 00:16:02,640
And again and again, we cling to these things, we seek out for these things, we're born

149
00:16:02,640 --> 00:16:07,420
and we're taught from birth to seek out these things.

150
00:16:07,420 --> 00:16:11,580
As women were taught at the seekout men as men were taught to seek out women.

151
00:16:11,580 --> 00:16:18,740
We're taught to seek out money, we're taught to seek out possessions, we're taught

152
00:16:18,740 --> 00:16:24,100
to seek out a house and a family to seek out children, we're taught to seek out luxury,

153
00:16:24,100 --> 00:16:30,100
we're taught to seek out wealth. We're taught to seek out learning and skill and handicraft.

154
00:16:30,100 --> 00:16:35,300
We're taught to seek out a job. We were able to realize that actually what we're

155
00:16:35,300 --> 00:16:40,340
taught to seek out is all day sickness and death. We're taught to seek out

156
00:16:40,340 --> 00:16:48,700
negligence and defilement. And so we're taught to seek out rebirth again and

157
00:16:48,700 --> 00:16:53,380
again and again. The things that we're taught in our culture, by our

158
00:16:53,380 --> 00:17:01,180
society, by media, by the world. We're taught to seek out a never-ending cycle

159
00:17:01,180 --> 00:17:10,540
of dissatisfaction and disappointment. And again and again we forget. So again and

160
00:17:10,540 --> 00:17:15,100
again we think this is something new and we cling to it and then find only

161
00:17:15,100 --> 00:17:21,620
disappointment. Meaninglessness. If I'm nothing, if I know benefit and

162
00:17:21,620 --> 00:17:26,180
ourselves subject and defilement, we become angry, we become greedy, we become

163
00:17:26,180 --> 00:17:34,660
addicted and attached and conceded and confused and run around in circles.

164
00:17:35,220 --> 00:17:40,340
We call the suffering for ourselves suffering forever. And in the end we

165
00:17:40,340 --> 00:17:46,340
it's it's meaning that and we get old, we get sick and we die, and then it

166
00:17:46,340 --> 00:17:52,820
starts all over again. So then he taught himself, well what is the answer? There's

167
00:17:52,820 --> 00:17:58,620
only one answer and that's the seek out. What is free from this thing? The

168
00:17:58,620 --> 00:18:02,660
seek out something that is free from birth, free from old age, free from sickness,

169
00:18:02,660 --> 00:18:09,060
free from death, free from sorrow and free from defilement.

170
00:18:09,060 --> 00:18:18,740
And so this is the story of the Buddha that when he was living as a prince, he

171
00:18:18,740 --> 00:18:21,980
decided that he would give up all of this. All of the luxury and the wonders

172
00:18:21,980 --> 00:18:32,300
that he had, the palace, his wife, his child, his father, his mother, his

173
00:18:32,300 --> 00:18:40,060
his title as a prince, to give it all up, seeing that all of this was just

174
00:18:40,060 --> 00:18:46,660
just ephemeral manifestations of physical and mental reality of experience,

175
00:18:46,660 --> 00:18:51,580
coming and going. None of it was going to last, none of it was going to satisfy

176
00:18:51,580 --> 00:18:57,700
him. There's nothing in the world that could possibly satisfy him. So he

177
00:18:57,700 --> 00:19:02,740
decided to seek out the non-arism, to find something that hadn't arisen,

178
00:19:02,740 --> 00:19:09,300
because everything that arises is passes away. And so he shaved off his head

179
00:19:09,300 --> 00:19:15,900
and he shaved off his hair and beard, gave up his princely robe, princely

180
00:19:15,900 --> 00:19:23,740
clothes, put on rags, wandered for arms, and he says he wandered the whole country

181
00:19:23,740 --> 00:19:30,340
looking for a teacher. And he found two teachers, the first teacher he found

182
00:19:30,340 --> 00:19:44,180
Alara Kalama, taught him how to gain some immaterial, formless John or

183
00:19:44,180 --> 00:19:49,740
practice some kind of tranquility meditation that focused the mind and

184
00:19:49,740 --> 00:19:54,860
to enter into the sphere of nothingness. And he practiced it, he learned it by

185
00:19:54,860 --> 00:19:58,260
heart, the teaching, and then he practiced it, and eventually he realized this

186
00:19:58,260 --> 00:20:04,020
state. So he went, the teacher would have been someone, some sort of Hindu yogi

187
00:20:04,020 --> 00:20:10,540
who had some high attainment of the sphere of nothingness, because in India

188
00:20:10,540 --> 00:20:16,300
even now you'll find such, such teacher. And he attained the same state that

189
00:20:16,300 --> 00:20:20,140
his teacher attained. But he realized that this wasn't something that was

190
00:20:20,140 --> 00:20:23,900
under reason, it was something that that was arisen. And it didn't lead the

191
00:20:23,900 --> 00:20:29,180
freedom from suffering, it only led to rebirth in the formless realm. And

192
00:20:29,180 --> 00:20:32,940
because of his attainments and his perfection of mind, he knew that he had been

193
00:20:32,940 --> 00:20:38,700
there before and passed away from it. He knew that this wasn't the way out of

194
00:20:38,700 --> 00:20:43,540
suffering, this wasn't eternal, because it arose. The perception of nothingness

195
00:20:43,540 --> 00:20:52,180
arose in his mind, and then it ceases. And so he left that teacher and he

196
00:20:52,180 --> 00:21:00,060
went to find another teacher and it was Utaka, Utaka Ramakupa, who himself hadn't

197
00:21:00,060 --> 00:21:03,660
realized any special attainment that had a teacher who had who had passed away

198
00:21:03,660 --> 00:21:10,020
Rama. And he taught the bodies after the Ramas teachings that led all the way

199
00:21:10,020 --> 00:21:16,260
to the highest, you say the highest tranquility, spiritual attainment, which is

200
00:21:16,260 --> 00:21:24,420
the attainment of neither perception or non-perception. And he actually was

201
00:21:24,420 --> 00:21:28,500
able to attain this as well, even though his own teacher, Utaka Ramakupa,

202
00:21:28,500 --> 00:21:38,060
wasn't able to attain this result. But he realized that this also wasn't

203
00:21:38,060 --> 00:21:43,300
an end of suffering. It wasn't freedom from suffering. The state of it only led

204
00:21:43,300 --> 00:21:47,200
to the state of neither perception nor non-perception, which is the highest

205
00:21:47,200 --> 00:21:54,220
tranquility state. And if he died there, he would be born in the Brahma realm, the

206
00:21:54,220 --> 00:21:59,140
formless God realm of neither perception nor non-perception. But it's still a

207
00:21:59,140 --> 00:22:06,300
state that arises in the mind. It still has mind arising. And he realized that

208
00:22:06,300 --> 00:22:13,660
any state that he entered into was still subject to what you might call old age

209
00:22:13,660 --> 00:22:18,060
sickness and death. In the sense that it gets old, it starts to break up and it

210
00:22:18,060 --> 00:22:26,220
passes away. The state disappeared. The state ended. And this later came to be

211
00:22:26,220 --> 00:22:31,740
the Buddha's teaching on Sankara Dooka, that even spiritual attainment of any

212
00:22:31,740 --> 00:22:39,300
kind. Even the attainment of Godhood can be considered suffering because it's

213
00:22:39,300 --> 00:22:44,140
something you have to work very very hard at. It doesn't text don't talk

214
00:22:44,140 --> 00:22:47,820
about it and the Buddha didn't talk about how difficult it was for him to reach

215
00:22:47,820 --> 00:22:54,700
those states. But he must have worked quite hard to purify his mind to focus his

216
00:22:54,700 --> 00:23:02,860
mind and be able to enter those states. And yet he realized that it was all

217
00:23:02,860 --> 00:23:12,140
pointless and it had no lasting effect. It had the result of whatever

218
00:23:12,140 --> 00:23:16,660
whatever work he had put into it and when the work was finished and the results

219
00:23:16,660 --> 00:23:31,540
were finished. And so he decided that there was no way that he could find this

220
00:23:31,540 --> 00:23:34,700
teaching. You'd find this out from from anyone else. He had to give up

221
00:23:34,700 --> 00:23:39,020
everything. He had to give up his teachers. He had to give up all of his learning

222
00:23:39,020 --> 00:23:44,180
all of his knowledge. He had to in the end give up all of of reality. All of

223
00:23:44,180 --> 00:23:49,420
that experience. He realized that every experience has to arise. That arises

224
00:23:49,420 --> 00:23:53,940
has to see. And so he knew intuitively that this was something he had to find

225
00:23:53,940 --> 00:24:00,380
for himself. There was no one on earth who could teach it. And so he went and

226
00:24:00,380 --> 00:24:05,020
found this place. He wandered through India and looking for no longer looking for

227
00:24:05,020 --> 00:24:10,020
a teacher but looking for a suitable place to strive. And he found a place that

228
00:24:10,020 --> 00:24:18,780
was most likely very similar to this place in Sainani Gama, near Bodhana, near

229
00:24:18,780 --> 00:24:25,460
Gaya, in a place that is now called Bodhagaya. And there was Nir Uruwehla at

230
00:24:25,460 --> 00:24:31,540
the time there was a place called Uruwehla near there. And this is where he

231
00:24:31,540 --> 00:24:38,180
laid out. He sat down to strive. Under a tree which later became called

232
00:24:38,180 --> 00:24:41,980
Bodhi Tree. And it's the type of tree that you see behind me and that we have

233
00:24:41,980 --> 00:24:49,140
all around here. Many Bodhi trees. And in India if you go to India especially in

234
00:24:49,140 --> 00:24:56,540
Uttar Pradesh and Bihar, you know, see Bodhi trees lining the road. It's the

235
00:24:56,540 --> 00:25:01,500
natural habitat of Bodhi tree. So it was a tree that was actually quite common in

236
00:25:01,500 --> 00:25:05,660
that area almost likely. At least now it is. If you go there today you'll see

237
00:25:05,660 --> 00:25:10,980
Bodhi trees throughout the country.

238
00:25:13,020 --> 00:25:18,860
And he said and I found that. I found the supreme freedom from bondage. I found

239
00:25:18,860 --> 00:25:25,020
enlightenment. I found freedom from suffering from myself. And this is what I

240
00:25:25,020 --> 00:25:27,300
teach.

241
00:25:27,300 --> 00:25:38,020
And the Sutta goes on to talk about something that is even perhaps more

242
00:25:38,020 --> 00:25:43,620
important. It's the actual teaching and the actual development and cultivation of

243
00:25:43,620 --> 00:25:52,380
tranquility and insight. And it relates to the topic of this, this talk that I'm

244
00:25:52,380 --> 00:25:58,260
giving in regards to how important a place like this is. And as I said it's

245
00:25:58,260 --> 00:26:03,740
important because it takes us away from the world. It takes us away from our

246
00:26:03,740 --> 00:26:08,700
intoxications with the five senses with sight, sound, smell, taste, and feeling.

247
00:26:08,700 --> 00:26:14,580
And this is the teaching that the Buddha gave in a Dhamma Dayadha Sutta. After

248
00:26:14,580 --> 00:26:18,620
after reminding the monks of his attainment of enlightenment just really

249
00:26:18,620 --> 00:26:24,100
the significance of the Bodhi tree in a historical context. He began to

250
00:26:24,100 --> 00:26:28,300
explain to them the Dhamma that he had realized and that is that there are

251
00:26:28,300 --> 00:26:40,380
these five kamangun which can be translated as these chords or these ropes of

252
00:26:40,380 --> 00:26:46,420
sensuality. This is sights that are intoxicating sounds that are intoxicating

253
00:26:46,420 --> 00:26:56,580
smells that are intoxicating tastes that are intoxicating. And he pointed this

254
00:26:56,580 --> 00:27:03,540
out as the defining factor of our spiritual life or a defining factor of our

255
00:27:03,540 --> 00:27:06,300
spiritual life.

256
00:27:06,300 --> 00:27:21,940
He taught that any meditator who is still caught up in these things will not be

257
00:27:21,940 --> 00:27:27,060
able to understand the truth, will be caught up by Mara. He said just like a

258
00:27:27,060 --> 00:27:36,700
deer that is caught up in a snare or many snare, a heap of snare as he said. A

259
00:27:36,700 --> 00:27:39,620
person who is caught up with these things will never be able to become free,

260
00:27:39,620 --> 00:27:46,740
will never be able to live as they play. Will never be free. The key to why we're

261
00:27:46,740 --> 00:27:51,820
not able to understand the truth is our infatuation with the senses. Even as

262
00:27:51,820 --> 00:27:57,020
meditator, when we undertake a meditation course, the first thing we have to do

263
00:27:57,020 --> 00:28:03,100
is become free from these things. Our first task is to pull ourselves out of

264
00:28:03,100 --> 00:28:09,020
these so that the things that we see are just seeing the things that we hear are

265
00:28:09,020 --> 00:28:14,620
just hearing. This is why the Buddha gave to Bahia. This is the very core of his

266
00:28:14,620 --> 00:28:20,460
teaching. Deep pei, deep tamat dang dang, bhui sakti. What do you see, let it be just

267
00:28:20,460 --> 00:28:27,820
seeing. What you hear, let it be just hearing, what you sense, let it be just sensing. If

268
00:28:27,820 --> 00:28:33,100
you can do this, there will be no attachment for you. You will find no self in

269
00:28:33,100 --> 00:28:39,500
these things. Without doing this, we can't hope to understand the truth and we

270
00:28:39,500 --> 00:28:43,980
can't hope to live in freedom from suffering. We can't hope to understand our

271
00:28:43,980 --> 00:28:50,980
causes of suffering. We can't hope to understand our problem. Our minds will be caught up by

272
00:28:50,980 --> 00:29:03,700
Mara. And so the way that we become free from suffering is to free ourselves from

273
00:29:03,700 --> 00:29:11,060
the infatuation with world in this infatuation with the world around us. Our

274
00:29:11,060 --> 00:29:16,900
infatuation with sights and sounds and smells and tastes and feeling. Our infatuation

275
00:29:16,900 --> 00:29:22,060
with objects of the sense. Even our infatuation with ideas of becoming this, of

276
00:29:22,060 --> 00:29:28,060
becoming that, of not being this, and not being that. And this is what we do in the

277
00:29:28,060 --> 00:29:32,220
practice of meditation. This is why we teach the meditators to do something that

278
00:29:32,220 --> 00:29:39,060
seems so menial and even meat pointless. When you see something to just

279
00:29:39,060 --> 00:29:46,020
remind yourself seeing when you hear something to remind yourself hearing. When

280
00:29:46,020 --> 00:29:49,020
you walk to just know that you're walking, when you stand to just know that

281
00:29:49,020 --> 00:29:56,220
you're standing. Whenever experience arises to see it just for what it is. This is

282
00:29:56,220 --> 00:30:03,140
to pull ourselves out of the infatuation with senses. In fact, all of our

283
00:30:03,140 --> 00:30:09,620
addiction, all of our sufferings, all of our problems in life. Come back and

284
00:30:09,620 --> 00:30:14,740
stem from the five senses or the six senses. This is the Buddha's teaching on

285
00:30:14,740 --> 00:30:18,660
Petitya Samupada. He said also in the diadas of the dhamma-diadas

286
00:30:18,660 --> 00:30:22,780
earlier. He said, after he became enlightened, the reason why he didn't, he

287
00:30:22,780 --> 00:30:27,220
decided not to teach. The reason why he decided not to teach is because he

288
00:30:27,220 --> 00:30:31,700
saw that the world was infatuated with like, world in this, you call, caught up by

289
00:30:31,700 --> 00:30:35,380
worldiness. And he said, there's no way they can understand dependent

290
00:30:35,380 --> 00:30:39,340
origination. This is actually the words that he used, was what they won't be

291
00:30:39,340 --> 00:30:43,860
able to understand is dependent origination. And by these words of the

292
00:30:43,860 --> 00:30:46,620
Buddha, we can see how important this doctrine is of

293
00:30:46,620 --> 00:30:58,220
Iedapachayata. The arising according to cause and effect, causeual or

294
00:30:58,220 --> 00:31:03,220
co-erising, that something's arise based on other things. And without those

295
00:31:03,220 --> 00:31:11,980
causes arising, they will not arise to effect. This is the teaching on how

296
00:31:11,980 --> 00:31:18,620
Namarupa leads to the six senses and the six senses lead to contact and

297
00:31:18,620 --> 00:31:22,140
contact leads to feeling and feeling leads to craving and craving leads to

298
00:31:22,140 --> 00:31:25,340
clinging and clinging leads to seeking and seeking leads to becoming

299
00:31:25,340 --> 00:31:37,980
or becoming leads to birth. And if we can understand this teaching, if

300
00:31:37,980 --> 00:31:44,220
if we can practice and see experience piece by piece by piece that the

301
00:31:44,220 --> 00:31:48,500
sights and the sounds and the smells are just sights and sounds and smells. The

302
00:31:48,500 --> 00:31:52,580
feelings that arise are just feeling. When you see something, they will arise

303
00:31:52,580 --> 00:32:00,700
a pleasurable or a displeasure. An unpleasant sensation, pleasant,

304
00:32:00,700 --> 00:32:05,580
unpleasant sensation, or else they will arise a neutral sensation. And then

305
00:32:05,580 --> 00:32:10,580
because of that, they will arise craving for intercleaning to desire for it

306
00:32:10,580 --> 00:32:14,900
or attachment. When it's there, you'll become attached to it, when it's gone,

307
00:32:14,900 --> 00:32:23,820
you'll want it, or when it's there, you'll become averted and become displeased

308
00:32:23,820 --> 00:32:29,460
with it, and when it's gone, you'll become afraid of it. All of our problems in

309
00:32:29,460 --> 00:32:36,140
life, they come down to this cause and effect relationship. So our practice is

310
00:32:36,140 --> 00:32:39,220
simply when we see to know that we're seeing, when we have the feelings, to

311
00:32:39,220 --> 00:32:42,220
know that we have the feeling, when we have the craving, to know that we have

312
00:32:42,220 --> 00:32:46,740
the craving, when we have the clinging, when we want something, when we want to

313
00:32:46,740 --> 00:32:51,060
chase after something, when we're seeking something to know that we're seeking it,

314
00:32:51,060 --> 00:32:57,820
to break up this sequence, to change the sequence, to interfere with this

315
00:32:57,820 --> 00:33:04,020
sequence, so that it doesn't continue. So that when seeing is just seeing and

316
00:33:04,020 --> 00:33:11,220
singing is not something pleasurable, when pleasure arises, it's only pleasure

317
00:33:11,220 --> 00:33:17,420
and to not, it's not something worth clinging to. When craving arises, to know

318
00:33:17,420 --> 00:33:20,100
that it's not worth clinging, when clinging arises, to know that it's not worth

319
00:33:20,100 --> 00:33:26,220
seeking, when seeking arises, to know that we should not continue seeking, to

320
00:33:26,220 --> 00:33:31,580
know that we are seeking, and to see the suffering that comes from seeking. If we

321
00:33:31,580 --> 00:33:35,420
can break experience up in this way, with everything that we're clinging to,

322
00:33:35,420 --> 00:33:40,260
things that we like, and things that we dislike, our whole lives will become

323
00:33:40,260 --> 00:33:45,220
free from suffering, and we'll be able to realize the truth of the Buddhist

324
00:33:45,220 --> 00:33:49,620
teaching, the truth of reality, we'll be able to live according to the truth of

325
00:33:49,620 --> 00:33:55,740
reality, we'll be able to see this whole world just for what it is, just how we

326
00:33:55,740 --> 00:33:59,420
think we live our lives. We think that we live in the world, seeing, hearing,

327
00:33:59,420 --> 00:34:04,580
smelling, tasting, thinking, but so much of our lives is not here, it's not now.

328
00:34:04,580 --> 00:34:10,740
Even though we're here and now sitting on the sand, seeing, hearing, smelling, tasting,

329
00:34:10,740 --> 00:34:17,020
feeling, and thinking, our minds are so often caught up in what is just, in the

330
00:34:17,020 --> 00:34:21,860
end, the sixth sense, the sense of the mind thinking, caught up in ideas and

331
00:34:21,860 --> 00:34:27,260
papan chas, what is it? This diversification, or making more of things than they

332
00:34:27,260 --> 00:34:35,940
actually are, called the vitting projections and ideas and concepts. The concepts

333
00:34:35,940 --> 00:34:41,100
like the Bodhi tree or concepts like the Buddha, even these concepts in the end

334
00:34:41,100 --> 00:34:45,260
we have to be free from, and we have to see that even here under the Bodhi tree,

335
00:34:45,260 --> 00:34:52,820
all that we have is body and mind. So this is perhaps one of the most ideal

336
00:34:52,820 --> 00:34:57,980
places for coming to realize this. Here we have this symbol of enlightenment to

337
00:34:57,980 --> 00:35:02,020
remind us, anytime our mind wanders, we have light, it's as though we have the

338
00:35:02,020 --> 00:35:05,860
Buddha watching over us, and we know that anytime our mind wanders into

339
00:35:05,860 --> 00:35:11,260
sensuality or a version, we will become embarrassed and remind ourselves and

340
00:35:11,260 --> 00:35:17,100
we will be reminded of the severity and the importance of our practice. And

341
00:35:17,100 --> 00:35:21,060
also it's a place that is quite peaceful and free from sensuality for this

342
00:35:21,060 --> 00:35:26,940
reason, because no one here dares to do or say things that are chaotic or

343
00:35:26,940 --> 00:35:33,700
destructive. And so we should rather than just coming here to bow down in

344
00:35:33,700 --> 00:35:37,060
their respect, even though these are useful and worthwhile things, we

345
00:35:37,060 --> 00:35:40,940
should now take this opportunity. Now we have this great opportunity, we should

346
00:35:40,940 --> 00:35:47,780
take it and practice meditation. So now we will continue on and together we

347
00:35:47,780 --> 00:35:52,820
can do a group meditation, we'll try to do first mind for frustration and then

348
00:35:52,820 --> 00:36:18,980
walking and then sitting. So that's all for today.

