1
00:00:00,000 --> 00:00:05,480
Is it possible that nibana after death does not exist?

2
00:00:05,480 --> 00:00:11,960
And even if you do become, even if you become enlightened, there is nothing after death.

3
00:00:11,960 --> 00:00:19,760
And if this is so, wouldn't this make living as a monk trying to become enlightened, pointless,

4
00:00:19,760 --> 00:00:23,640
as when you die your consciousness like sitting in your brain decays and is proven to

5
00:00:23,640 --> 00:00:29,080
not exist anymore, the universe and life can be eternal no matter if you're enlightened

6
00:00:29,080 --> 00:00:30,080
or not.

7
00:00:30,080 --> 00:00:31,080
Wow.

8
00:00:31,080 --> 00:00:37,400
That's a breathful mouthful.

9
00:00:37,400 --> 00:00:43,960
Anything's possible if you haven't experienced reality, if you haven't come to see the truth

10
00:00:43,960 --> 00:00:49,080
for yourself, then yeah, anything's possible, it's possible that God's waiting to receive

11
00:00:49,080 --> 00:00:57,320
you in heaven for eternity or send you the hell for eternity, not logical, but it's just as

12
00:00:57,320 --> 00:01:00,160
possible as anything else before you have the facts.

13
00:01:00,160 --> 00:01:11,200
But once you have the facts from empirical observation, I mean the corollary is kind

14
00:01:11,200 --> 00:01:21,640
of funding me because obviously I do have a view that nibana after death does exist for

15
00:01:21,640 --> 00:01:33,480
an arahad, not for anyone else, but wait, so if you become enlightened, there is nothing

16
00:01:33,480 --> 00:01:34,480
after death.

17
00:01:34,480 --> 00:01:40,600
I think you're misunderstanding your nibana is not a place that you go after death, nibana

18
00:01:40,600 --> 00:01:46,680
does mean there is, in a sense, nothing after death because there's no more arising of

19
00:01:46,680 --> 00:01:53,640
that which is impermanenceaffering and not itself, but I think you're hitting on a very

20
00:01:53,640 --> 00:02:02,320
important point that considering that it's nothing, there is really no point.

21
00:02:02,320 --> 00:02:05,760
The only point that you can say, there is a kind of a point and the only way you can say

22
00:02:05,760 --> 00:02:09,840
that we've talked about this many times before, the only way you can say there's a point

23
00:02:09,840 --> 00:02:20,440
to say nibana is that it's internally consistent, you want to be free from suffering and

24
00:02:20,440 --> 00:02:26,440
you don't suffer, any other path is internally inconsistent because you want to be free

25
00:02:26,440 --> 00:02:31,280
from suffering and yet you're acting in such a way that brings about suffering.

26
00:02:31,280 --> 00:02:41,720
So the attainment of nibana is the only way to be internally consistent and be consistent

27
00:02:41,720 --> 00:02:54,000
to your own desires, your own wishes, your own intentions.

28
00:02:54,000 --> 00:02:57,800
But the point is that if you're not enlightened, this doesn't happen.

29
00:02:57,800 --> 00:03:03,480
If you're not enlightened, when you die, you have to come back.

30
00:03:03,480 --> 00:03:07,440
That's our view as Buddhists, agree with it or don't agree with it.

31
00:03:07,440 --> 00:03:13,160
That's really the problem because if it weren't the case, then there wouldn't be any

32
00:03:13,160 --> 00:03:14,160
point in meditating.

33
00:03:14,160 --> 00:03:17,120
You just wait until you die or even commit suicide.

34
00:03:17,120 --> 00:03:21,520
There's really no point in life at all.

35
00:03:21,520 --> 00:03:26,440
That's why they say things like Buddhism are wishful thinking or religion is all wishful

36
00:03:26,440 --> 00:03:28,200
thinking.

37
00:03:28,200 --> 00:03:33,800
You want it just because you want it to be true, it doesn't make it true, that's true.

38
00:03:33,800 --> 00:03:41,000
It certainly doesn't, but it doesn't make it false either.

39
00:03:41,000 --> 00:04:10,840
So seeing the truth, observing objectively is what, well, obviously, you just did the truth.

