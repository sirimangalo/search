1
00:00:00,000 --> 00:00:04,560
Okay, so we'll just get started.

2
00:00:04,560 --> 00:00:15,160
Today I will be discussing, we will be discussing the Satipatana Sutta, the discourse on

3
00:00:15,160 --> 00:00:25,240
the establishment or the setting up or the foundation of mindfulness.

4
00:00:25,240 --> 00:00:34,760
And this is one of the most loved and best known discourses of the Lord Buddha because

5
00:00:34,760 --> 00:00:44,360
it's considered to be a fairly complete and lucid explanation of the practice of Buddhist

6
00:00:44,360 --> 00:00:53,080
meditation and this elusive word that we always translate as mindfulness.

7
00:00:53,080 --> 00:00:59,680
It's really so because when we look at the Lord Buddha's teaching as a whole, at least

8
00:00:59,680 --> 00:01:14,160
in the early forms, in the early traditions, we see that mindfulness or this sort of

9
00:01:14,160 --> 00:01:19,280
mind state or this quality of mind which we're translating as mindfulness.

10
00:01:19,280 --> 00:01:24,960
We can see that it's very much at the core of the Buddha's teaching.

11
00:01:24,960 --> 00:01:29,320
We can think of when the Lord Buddha passed away right before he passed away.

12
00:01:29,320 --> 00:01:42,960
His last words were upamatana sampatita which means, look, oh you who see the danger in

13
00:01:42,960 --> 00:01:55,280
samsara, all things that arise have to in the end cease or pass away, everything that

14
00:01:55,280 --> 00:01:57,760
comes in the end has to go.

15
00:01:57,760 --> 00:02:09,960
An upamatana sampatita means to work hard to become perfect in heedfulness or religiosity,

16
00:02:09,960 --> 00:02:18,040
the legends which is the opposite of negligent and so we see that the core of the Buddha's

17
00:02:18,040 --> 00:02:23,720
teaching wherever we look we'll see that this idea of not being negligent or this word

18
00:02:23,720 --> 00:02:30,400
that I've coined religions which basically means having a religious attitude or being

19
00:02:30,400 --> 00:02:35,240
of a religious nature which simply means the opposite of negligence, paying attention

20
00:02:35,240 --> 00:02:44,000
and giving life some measure of importance instead of taking it to be some kind of

21
00:02:44,000 --> 00:02:49,320
a game where we can just do and say and think whatever we want that we have to have some

22
00:02:49,320 --> 00:02:54,240
sort of what has come to be known as mindfulness.

23
00:02:54,240 --> 00:03:04,120
And that's exactly what is meant by upamatana not being negligent, the correct explanation

24
00:03:04,120 --> 00:03:10,920
of the word upamatana is to be mindful at all times.

25
00:03:10,920 --> 00:03:13,920
So this is a very important discourse and a very important teaching that the Lord

26
00:03:13,920 --> 00:03:20,240
Buddha gave so if you have time to read through it you can sort of get a feeling for

27
00:03:20,240 --> 00:03:24,840
how it is that one establishes what we call mindfulness.

28
00:03:24,840 --> 00:03:30,160
First of all the word mindfulness as I've said before is a fairly poor translation, fairly

29
00:03:30,160 --> 00:03:37,920
misleading and vague rendering of the word sati, sati means to remember and it's used

30
00:03:37,920 --> 00:03:43,240
often to talk about remembering things in the past in a sense of recollecting or thinking

31
00:03:43,240 --> 00:03:51,840
over, bringing to mind it can also be used to talk about the future thinking about or

32
00:03:51,840 --> 00:03:57,200
remembering things that we have to do in the future.

33
00:03:57,200 --> 00:04:05,080
Here it simply means to remember reality or remember oneself or remember the experience

34
00:04:05,080 --> 00:04:15,240
which one is faced with the word sati as we'll see means the clear remembrance or recognition

35
00:04:15,240 --> 00:04:25,000
of something for what it is and it's brought about by a recalling, calling to mind

36
00:04:25,000 --> 00:04:33,120
just like memory when you see something and you remember it to be a person, you remember

37
00:04:33,120 --> 00:04:39,800
the color, you recognize some factor about it, maybe when you recognize it it's something

38
00:04:39,800 --> 00:04:42,920
that has caused you suffering in the past or something that has caused you pleasure

39
00:04:42,920 --> 00:04:48,640
and then it leads to liking or disliking of it.

40
00:04:48,640 --> 00:04:53,600
But here what we're talking about is recognizing something for what it is and as I've

41
00:04:53,600 --> 00:04:56,800
said before not making more of it than it actually is.

42
00:04:56,800 --> 00:05:03,400
So when we see remembering that that is an experience of seeing, when an emotion arises

43
00:05:03,400 --> 00:05:08,280
remembering that that's an emotion, remembering it for what it is, reminding ourselves of

44
00:05:08,280 --> 00:05:10,920
the reality of it.

45
00:05:10,920 --> 00:05:14,160
This is what is meant by the word sati.

46
00:05:14,160 --> 00:05:17,760
So we're going to use the word mindfulness but we have to understand that it really means

47
00:05:17,760 --> 00:05:24,400
to remember or to have a sort of a clear awareness and clear recognition of something

48
00:05:24,400 --> 00:05:27,200
for what it is.

49
00:05:27,200 --> 00:05:34,960
So the sutra starts a bhame sutra, which means, let's have I heard or this is what

50
00:05:34,960 --> 00:05:40,640
I have heard and so this is something that has been passed on from generation to generation

51
00:05:40,640 --> 00:05:46,880
that at one time the blessed one was living in the land of the kuru's, have come as

52
00:05:46,880 --> 00:05:53,760
a dhamma which they have identified as most likely a place in a suburb of New Delhi in

53
00:05:53,760 --> 00:05:58,680
India.

54
00:05:58,680 --> 00:06:04,440
And so the talk that the Lord Buddha gave starts, this is the only way of bhikkhu's for

55
00:06:04,440 --> 00:06:09,040
the purification of beings for the overcoming of sorrow and lamentation, for the destruction

56
00:06:09,040 --> 00:06:13,760
of suffering and grief, for the reaching the right path, for the attainment of nibhana,

57
00:06:13,760 --> 00:06:19,440
namely the four arousings of mindfulness.

58
00:06:19,440 --> 00:06:22,840
And they say that the reason the Lord Buddha gave this talk in the land of the kuru's is

59
00:06:22,840 --> 00:06:32,960
because the land of the kuru's or the people of the kuru of this land were a fairly exceptional

60
00:06:32,960 --> 00:06:36,160
group of people and the land was a fairly exceptional place.

61
00:06:36,160 --> 00:06:41,000
The weather was always nice, never too hot or too cold, there was no famine, there was

62
00:06:41,000 --> 00:06:47,760
no war, there was no political turmoil, it was all in a fairly nice place to live.

63
00:06:47,760 --> 00:06:59,000
And so the people were barely well cultured individuals, there was not a lot of time spent

64
00:06:59,000 --> 00:07:05,480
bhikkhu's and worrying about the lower aspects of life.

65
00:07:05,480 --> 00:07:11,600
So there was a lot of more higher intellectual and spiritual discussion going on.

66
00:07:11,600 --> 00:07:16,200
And once Buddhism reached this place, as the Lord Buddha went again and again to the

67
00:07:16,200 --> 00:07:22,400
kuru, the land of the kuru's to teach, they were able to as a society become very mindful

68
00:07:22,400 --> 00:07:28,840
and very interested in the Buddha's teaching, very able to apply it on a practical basis,

69
00:07:28,840 --> 00:07:35,240
such that not only the monks and the nuns, but also the ordinary people living there

70
00:07:35,240 --> 00:07:43,200
everyday lives were able to practice the four foundations of mindfulness.

71
00:07:43,200 --> 00:07:47,800
So when they would ask themselves, they would ask each other, so what part of the Buddha's

72
00:07:47,800 --> 00:07:51,680
teaching are you practicing today, are you being mindful of your body, the things that

73
00:07:51,680 --> 00:07:59,560
you do, are you being mindful of your speech, mindful of your emotions and so on.

74
00:07:59,560 --> 00:08:04,880
And they would, the story goes that whenever somebody said, oh no, I haven't been

75
00:08:04,880 --> 00:08:07,880
mindful, I haven't had any time today, they would yell at you, they would scold each

76
00:08:07,880 --> 00:08:13,360
other and say, oh, you're like a person who's dead, having lived your life without being

77
00:08:13,360 --> 00:08:21,800
mindful without taking the time, even though they're working, to become aware and to

78
00:08:21,800 --> 00:08:30,320
sort of keep the mind present in the here and now, it's like being dead, they would

79
00:08:30,320 --> 00:08:32,440
scold each other as being dead.

80
00:08:32,440 --> 00:08:39,520
So it was a fairly cultured and high-minded group of people, so this is the reason the Lord

81
00:08:39,520 --> 00:08:48,200
Buddha preached many discourses in that area, especially this one, which is of course

82
00:08:48,200 --> 00:08:52,800
very, as I've said, a very important teaching.

83
00:08:52,800 --> 00:08:58,240
So first of all, the word beku here is generally used to refer to the monks and it may

84
00:08:58,240 --> 00:09:05,280
very well be that the Lord Buddha was addressing directly the monks, but it's also quite

85
00:09:05,280 --> 00:09:11,520
possible and likely even in this case that he was talking to the group of practitioners

86
00:09:11,520 --> 00:09:19,800
as a whole, because of course just becoming a monk doesn't mean that one will really

87
00:09:19,800 --> 00:09:25,080
and truly take it to heart the Lord Buddha's teaching and really truly practice it.

88
00:09:25,080 --> 00:09:28,520
And just because one who's not a monk doesn't mean one doesn't have the opportunity

89
00:09:28,520 --> 00:09:36,280
to practice and to see clearly the truth that the Lord Buddha taught.

90
00:09:36,280 --> 00:09:43,360
So here it means one who has seen ikati, which means to see ikati, one who has seen the

91
00:09:43,360 --> 00:09:52,000
fear or the danger, as I said, the danger in samsara, payang ikati, piku, one who sees the

92
00:09:52,000 --> 00:09:59,600
danger. So one who has come in their life to see the suffering inherent in existence, to

93
00:09:59,600 --> 00:10:06,960
come to some realization that life is not just a game where we can do and say and think

94
00:10:06,960 --> 00:10:11,800
whatever we want, because it has repercussions and these repercussions can be devastating.

95
00:10:11,800 --> 00:10:17,120
People kill themselves, people have to take medication, people have to go see psychotherapists

96
00:10:17,120 --> 00:10:23,400
because they just aren't able to deal with the suffering in which they encounter in their

97
00:10:23,400 --> 00:10:29,160
lives. They've been negligent. And so these are people who see the danger of being negligent,

98
00:10:29,160 --> 00:10:36,200
the danger of being unmindful. And so this is what that word means.

99
00:10:36,200 --> 00:10:40,920
And so here the Lord Buddha is making a fairly bold claim, he's saying this is the only

100
00:10:40,920 --> 00:10:46,200
way, the four foundations of mindfulness or the four arousings of mindfulness are the only

101
00:10:46,200 --> 00:10:53,840
way to realize these five goals which the Lord Buddha has outlined.

102
00:10:53,840 --> 00:10:59,120
And what does it mean to say that something is the only way? Well, in this instance,

103
00:10:59,120 --> 00:11:05,240
it's actually explained in some detail in the commentaries. It's something that they were

104
00:11:05,240 --> 00:11:11,920
sort of, maybe not having difficulty agreeing on, but didn't pinpoint down to having

105
00:11:11,920 --> 00:11:17,880
one meaning. So they give it all together four or five meanings. First of all, the only

106
00:11:17,880 --> 00:11:26,760
way means that it is only one way. And many people object to this thinking that there

107
00:11:26,760 --> 00:11:33,320
are many ways that you can practice and all these paths that lead to the same goal and

108
00:11:33,320 --> 00:11:39,040
everyone wants to think that there are many ways to reach the same goal because of course

109
00:11:39,040 --> 00:11:42,800
we see in the world that there are many different practices. But here we're not talking

110
00:11:42,800 --> 00:11:49,560
about any particular tradition or school of Buddhism or any particular religion even.

111
00:11:49,560 --> 00:11:54,280
What we're talking about again is focusing on reality, putting our attention on reality

112
00:11:54,280 --> 00:11:57,800
and seeing it for what it is. And so what the Buddha is saying here, it's not saying

113
00:11:57,800 --> 00:12:01,920
that Buddhism is the only way or my Buddhism is the only way or this Buddhism or that

114
00:12:01,920 --> 00:12:07,040
Buddhism. What he's saying is the only way to become free from suffering and to attain

115
00:12:07,040 --> 00:12:16,560
these five great benefits, these five great goals is to look and to see reality clearly

116
00:12:16,560 --> 00:12:25,040
for what it is, to see things as they are and not become confused and distracted by

117
00:12:25,040 --> 00:12:33,320
concepts and delusion. The second meaning is that it's the only way means it's the one

118
00:12:33,320 --> 00:12:40,680
way meaning that one has to go by oneself, that the practice of meditation is not something

119
00:12:40,680 --> 00:12:45,880
that you can learn from somebody else, no one else can enlighten you simply by listening,

120
00:12:45,880 --> 00:12:53,440
by studying or by joining a group, becoming a monk or a nun or so. Without practicing

121
00:12:53,440 --> 00:12:59,400
it's not of any use at all, it's not something which can bring one to any state of release

122
00:12:59,400 --> 00:13:10,240
or freedom. It's the one way meaning it's the only way only of the Buddha or it's the

123
00:13:10,240 --> 00:13:16,960
way of only someone who is a Buddha. So someone who is not a Buddha could not teach this,

124
00:13:16,960 --> 00:13:23,880
could not explain it, could not come up with this because they wouldn't have seen clearly

125
00:13:23,880 --> 00:13:28,440
the nature of reality because of their confusion and their misunderstandings or still

126
00:13:28,440 --> 00:13:33,640
exist in their mind. They wouldn't be able to have the right to be called a Buddha

127
00:13:33,640 --> 00:13:37,520
and because they were not a Buddha, they wouldn't be able to teach this. So it's only

128
00:13:37,520 --> 00:13:48,040
only a Buddha could teach this, that's another meaning. And finally, it leads to only

129
00:13:48,040 --> 00:13:54,280
one goal. It leads to Nirvana or freedom from suffering. So it's not by practicing this

130
00:13:54,280 --> 00:13:59,120
path. You don't have to say, maybe it'll lead you in this direction, maybe it'll lead

131
00:13:59,120 --> 00:14:04,280
you in that direction. As people who practice meditation can see the more they practice,

132
00:14:04,280 --> 00:14:09,000
the closer they become to freedom and the more freedom and peace of mind they're able

133
00:14:09,000 --> 00:14:18,680
to obtain from the practice. So some people object to the idea that this is the only

134
00:14:18,680 --> 00:14:24,160
way that the practice of mindfulness could be the only way and any other way is not

135
00:14:24,160 --> 00:14:32,320
perfect, it's not right. Well, there are many other ways you can constrict this to translate

136
00:14:32,320 --> 00:14:37,520
this word, aka, you know, you could translate it simply as not leading to any other destination.

137
00:14:37,520 --> 00:14:42,200
If you practice this, it's the direct way or so on. But really, I think it's clear that

138
00:14:42,200 --> 00:14:53,200
if you're not being mindful, if you're not being present and alert and attend on reality,

139
00:14:53,200 --> 00:15:00,720
it's not possible by any stretch of the imagination for you to think that you could come

140
00:15:00,720 --> 00:15:06,480
to see clearly about reality or you could become free from suffering. Because our sufferings

141
00:15:06,480 --> 00:15:15,240
of course come directly from a misunderstanding from a misapprehension of things other than

142
00:15:15,240 --> 00:15:25,040
what they are. Okay, so there are five great goals in Buddhism and this is what the Lord

143
00:15:25,040 --> 00:15:29,720
Buddha starts off by saying, he says, through the practice of the four foundations of mindfulness,

144
00:15:29,720 --> 00:15:38,000
we can come to overcome, we can, we come to the purification of our minds, we come to

145
00:15:38,000 --> 00:15:43,400
overcome sorrow and lamentation, we're able to destroy suffering and grief, we're able

146
00:15:43,400 --> 00:15:49,880
to reach the right path, we're able to attain Nirvana. Now anyone who's watched some

147
00:15:49,880 --> 00:15:58,120
of the videos that I made on YouTube may recognize these as being the basis for my top five

148
00:15:58,120 --> 00:16:02,160
reasons why everyone should practice meditation. And I don't mention in those videos

149
00:16:02,160 --> 00:16:07,800
where I got them from, but this is directly where they came from. These are not some

150
00:16:07,800 --> 00:16:12,480
arbitrary list that I came up with, it's the Buddha's teaching and these are the top

151
00:16:12,480 --> 00:16:17,760
five reasons why everyone should practice meditation. I think these are the greatest

152
00:16:17,760 --> 00:16:22,240
reason why anyone should ever have for practicing meditation. First of all, that it purifies

153
00:16:22,240 --> 00:16:28,480
the mind that when we are able to see things and to acknowledge things for what they

154
00:16:28,480 --> 00:16:35,640
are and not make more of them than they really are, then our minds become pure, there's

155
00:16:35,640 --> 00:16:40,080
no greed, there's no anger, there's no wanting things to be other than they are and

156
00:16:40,080 --> 00:16:45,520
so on. And this is the first great reason why we practice meditation. We're not overwhelmed

157
00:16:45,520 --> 00:16:50,800
by our emotions. As a result of that, our sorrow and lamentation is done away with any

158
00:16:50,800 --> 00:17:00,840
kind of upset, any kind of mental depression or anxiety, worries, fears are all done

159
00:17:00,840 --> 00:17:08,800
away with as a result of having no impurities in the mind. As a result of not getting

160
00:17:08,800 --> 00:17:15,600
upset at things, then there's no suffering. So, even bodily suffering can be overcome

161
00:17:15,600 --> 00:17:23,320
as we practice meditation, we're able to see through the pain that arises in meditation.

162
00:17:23,320 --> 00:17:31,440
So when there's pain, we can see it simply as a sensation and not become upset or distressed

163
00:17:31,440 --> 00:17:38,560
by it. This is considered to be the right path, so it means attaining the right path. Once

164
00:17:38,560 --> 00:17:45,920
we're living our lives in this way where we see and understand everything the way it is,

165
00:17:45,920 --> 00:17:51,120
then this is considered to be the right way to live one's life, no matter what you're doing.

166
00:17:51,120 --> 00:17:56,120
So you can be a monk or not a monk, a nun or not a nun, you can be Buddhist or not

167
00:17:56,120 --> 00:18:01,520
a Buddhist, but if you're living your life alert and aware, this is considered to be

168
00:18:01,520 --> 00:18:07,400
the right path. And finally, the attainment of Nirvana, which simply means freedom from suffering,

169
00:18:07,400 --> 00:18:18,320
which is being totally free from any sort of stress or suffering at all.

170
00:18:18,320 --> 00:18:24,080
And that is kind of like a supreme state. So it's not just saying, oh, now I don't have

171
00:18:24,080 --> 00:18:29,240
any pain in my back, so now that's Nirvana. It's a supreme state where there is no absolutely

172
00:18:29,240 --> 00:18:36,680
no more suffering ever again. It's kind of like the end goal is. It's fine if that's not

173
00:18:36,680 --> 00:18:41,960
easy to understand because it shouldn't be. It's something that takes time and effort

174
00:18:41,960 --> 00:18:47,320
through the practices to be realized. So what are the four arousings of mindfulness?

175
00:18:47,320 --> 00:18:55,160
The four are the body, the feelings, and here by feelings we simply mean physical sensations,

176
00:18:55,160 --> 00:19:02,040
and the mind, and then dhammas. Here dhammas translated as mental objects. Actually mental

177
00:19:02,040 --> 00:19:07,520
objects is a very poor translation in this case. What really means is teachings, dhamma

178
00:19:07,520 --> 00:19:13,480
here is the teachings of the Lord Buddha, various teachings, various groups of things that

179
00:19:13,480 --> 00:19:21,200
the Lord Buddha has taught, various important groups of realities or mind states or even

180
00:19:21,200 --> 00:19:27,760
physical phenomenon that we have to keep in mind. And we'll get to that next week, this

181
00:19:27,760 --> 00:19:34,240
week I'm going to try to just skim through the body. So these four foundations are considered

182
00:19:34,240 --> 00:19:42,160
to be a good basis of reality. If we want to understand what is reality, these four foundations

183
00:19:42,160 --> 00:19:46,560
sort of put it all together and there's nothing that we will encounter in meditation

184
00:19:46,560 --> 00:19:51,320
that we can't put under one of these categories. The first three are pretty obvious. We

185
00:19:51,320 --> 00:19:57,240
have the body and that's the physical. We have sensations of pain and happiness and

186
00:19:57,240 --> 00:20:02,160
equanimity. And then there's the mind which is thinking. dhammas just means everything

187
00:20:02,160 --> 00:20:07,320
else. It's got emotions in there. There's the senses, seeing, hearing, smelling, tasting,

188
00:20:07,320 --> 00:20:12,960
feeling, thinking. There's even the eightfold noble path is in there. So these are things

189
00:20:12,960 --> 00:20:18,560
which are, the dhammas are just sort of a miscellaneous things which are going to be useful

190
00:20:18,560 --> 00:20:23,280
and going to come into play during the meditation that is sort of going to lead us up

191
00:20:23,280 --> 00:20:27,800
and onward in the practice. And there's something that often teachers will bring up slowly

192
00:20:27,800 --> 00:20:34,280
as the meditator progresses. And in the beginning, we can focus on the fourth one as

193
00:20:34,280 --> 00:20:39,080
being emotions because in the very beginning that's all that becomes readily apparent

194
00:20:39,080 --> 00:20:45,040
to the meditator. So we understand it to be negative emotions like liking, anger, frustration,

195
00:20:45,040 --> 00:20:56,640
boredom, sadness, wanting, drowsiness, distraction, doubt. So these are states of mind.

196
00:20:56,640 --> 00:21:01,000
So we can look at this as mental, at least in the beginning, or mind states. We can understand

197
00:21:01,000 --> 00:21:09,600
it to be in the beginning. Okay, and what do we do? We contemplate the body in the body.

198
00:21:09,600 --> 00:21:14,200
This is number one. Contemplating the body in the body. What does this mean? It's simply

199
00:21:14,200 --> 00:21:20,120
a palli colloquialism. It's a way of saying that in regards to the body, we focus on

200
00:21:20,120 --> 00:21:27,800
the body. Or when focusing on the body, we focus specifically on the body, on the body.

201
00:21:27,800 --> 00:21:33,600
If we're going to look at the body, then we don't mix it with the feelings. We separate

202
00:21:33,600 --> 00:21:38,440
the four out. So in regards to the body, we look at the body. When we're using the body

203
00:21:38,440 --> 00:21:42,480
as our meditation object, we're focusing on the body. That's all this means. And it can

204
00:21:42,480 --> 00:21:47,160
be quite confusing, and people don't understand. In fact, there are some schools of meditation

205
00:21:47,160 --> 00:21:51,600
that have actually taken this to me. That inside of this body, there is another body.

206
00:21:51,600 --> 00:21:54,960
And inside of that body, there is another body. And they go deeper and deeper and deeper

207
00:21:54,960 --> 00:21:59,520
trying to find these bodies inside of bodies. And they go, you can find this on the internet.

208
00:21:59,520 --> 00:22:03,880
It's kind of a humorous understanding of what it means by body and the body when it's

209
00:22:03,880 --> 00:22:11,360
just a palli grammatical structure, which of course, is the way it would have to be.

210
00:22:11,360 --> 00:22:15,680
Palli is a much different language than English, and the way it constructs itself is

211
00:22:15,680 --> 00:22:21,160
different. And that's all. It doesn't mean anything spiritual at all. It just means we separate

212
00:22:21,160 --> 00:22:26,320
the four out. For instance, when we're walking, if you've ever seen people do walking

213
00:22:26,320 --> 00:22:30,800
meditation, what they do is they focus on their feet as the foot is moving. You're focusing

214
00:22:30,800 --> 00:22:35,200
only on the foot. You're not focusing on your thoughts. You're not focusing on feelings.

215
00:22:35,200 --> 00:22:39,480
You're focusing on a part of the body. So in regards to the whole body, you pick a piece

216
00:22:39,480 --> 00:22:45,520
of the body and you focus on that. And when the foot moves, they say, step, being right,

217
00:22:45,520 --> 00:22:49,760
then when the left foot moves, step, being left or so on. When lifting the foot, they say

218
00:22:49,760 --> 00:22:56,720
lifting, when placing the foot, they say placing, and so on. Using the body as a meditation

219
00:22:56,720 --> 00:23:02,120
object, normally when we're sitting meditation, we focus on the abdomen. When the stomach

220
00:23:02,120 --> 00:23:07,480
rises, we say to ourselves, rising. When the stomach falls, we say falling, just focusing

221
00:23:07,480 --> 00:23:15,480
on a piece of the body, because when you're sitting, there's not much movement going on.

222
00:23:15,480 --> 00:23:19,920
And the same with the feelings, the same with the mind, the same with the dhamma, the mental

223
00:23:19,920 --> 00:23:30,040
mind states or so on. And then there's this phrase, having overcome in this world, covetousness

224
00:23:30,040 --> 00:23:37,840
and grief. And there's a lot of argument over the truth of the translation of this one.

225
00:23:37,840 --> 00:23:44,360
It most likely means, for the purpose of overcoming covetousness and grief. And the argument

226
00:23:44,360 --> 00:23:49,760
goes less, that if it were to mean having overcome, then the point of meditating would

227
00:23:49,760 --> 00:23:53,160
have already been accomplished. Why are you sitting in meditating when you've already overcome

228
00:23:53,160 --> 00:24:00,280
covetousness and grief or liking and disliking, all kinds of mental defilements, all kinds

229
00:24:00,280 --> 00:24:07,120
of anger and depression, all kinds of suffering states. If they've already been overcome,

230
00:24:07,120 --> 00:24:13,720
then why are you practicing mindfulness? And that is the goal, as we talked about above.

231
00:24:13,720 --> 00:24:18,360
And the word vinaya could mean either way. It could either mean having overcome or it could

232
00:24:18,360 --> 00:24:22,600
mean for the purpose of overcoming. And the general agreement is that, no, this is a

233
00:24:22,600 --> 00:24:26,880
poor translation. It should mean for the purpose of overcoming. But you could look at it as

234
00:24:26,880 --> 00:24:33,560
having given up one's attachments to the world. It means gone off and just said enough.

235
00:24:33,560 --> 00:24:38,760
I no longer want to get involved in the world. I'm going to try to give this up. And so

236
00:24:38,760 --> 00:24:47,280
in a sense, you have given up or gone beyond your worldly cares, at least for a time.

237
00:24:47,280 --> 00:24:52,240
So you can look at it either way. But I think it is quite proper to say that, no, you

238
00:24:52,240 --> 00:24:57,480
haven't given these things up. But this is what our purpose is. And this is a very important

239
00:24:57,480 --> 00:25:02,880
purpose in the practicing of meditation. When we practice, we should try to give up our

240
00:25:02,880 --> 00:25:26,520
likes and dislikes, our attachments and aversions.

