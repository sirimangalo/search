1
00:00:00,000 --> 00:00:09,000
And the technique you teach about it, it's based on mindfulness of the breath or the abdomen.

2
00:00:11,000 --> 00:00:20,000
The question is, would you agree that it's a more advanced technique would be to be mindful of thought?

3
00:00:20,000 --> 00:00:30,000
The idea is that the mind tends to see five senses as external.

4
00:00:31,000 --> 00:00:41,000
It's easier for the mind to rotate and define with them, so you can be mindful of the movements of your abdomen, for example.

5
00:00:41,000 --> 00:01:10,000
It depends on the person, but advanced might not quite be the word that I would use, but it's getting there.

6
00:01:11,000 --> 00:01:17,000
The point of putting the body first is that it's more obvious.

7
00:01:18,000 --> 00:01:35,000
The Visudimaga does a really good job of summing this up by saying that you don't have to go, and you don't have to pay any attention or to trying to be mindful of the mind.

8
00:01:35,000 --> 00:01:51,000
Because the more clear the body becomes, or as the body becomes clear, to an equal degree, the mind becomes clear.

9
00:01:51,000 --> 00:02:02,000
So as you're watching the abdomen, you're in the best position to catch the mind, to catch your thoughts.

10
00:02:03,000 --> 00:02:06,000
So that's the reason for starting there.

11
00:02:07,000 --> 00:02:18,000
But as to thinking that the mind is self or that thoughts are self, the theory of it is that it depends on the person.

12
00:02:18,000 --> 00:02:23,000
Some people are more attached to the mind, some people are more attached to the body.

13
00:02:23,000 --> 00:02:25,000
I mean, I'm just going by theory.

14
00:02:26,000 --> 00:02:45,000
I know what I teach and I know how I teach, and I don't put emphasis on any one of the four sati botanas, mostly because I'm obviously not qualified to say which person has, which tendency.

15
00:02:45,000 --> 00:02:50,000
Is this person more likely to attach to the body or more likely attached to the mind?

16
00:02:51,000 --> 00:02:56,000
Are they more of a passionate person or a views-based person?

17
00:02:56,000 --> 00:03:17,000
I was just editing one of my talks and reminds me of a reminder of it where I was talking about how a person can change quite quickly from one day to the next or from one course to the next at least.

18
00:03:17,000 --> 00:03:30,000
So to say that at one point there'll be passion and another point there'll be intellectual, and a person might be outwardly intellectual, but inside be very passionate and so on.

19
00:03:31,000 --> 00:03:43,000
So to focus on one or the other of the sati botanas, you don't see that in the Buddha's teaching, you don't see any emphasis being placed in that regard.

20
00:03:43,000 --> 00:03:51,000
So starting with the body, what you can see is that there's a lot more in the sati botanas on the body.

21
00:03:51,000 --> 00:04:09,000
So you could say, well, in that case, focusing on the body, for example, the breath is probably the best place to start the Buddha said by practicing on upon a sati, you're actually practicing the four foundations, all four of the foundations of mindfulness, for example.

22
00:04:09,000 --> 00:04:22,000
But beyond that, the feelings, the mind, and the demos, I would say, I wouldn't have any reason to put one of them ahead of the of the other.

23
00:04:22,000 --> 00:04:23,000
You see how it goes.

24
00:04:23,000 --> 00:04:38,000
I mean, there's a teacher, a very famous teacher in the world who focuses on weight and he comes up with all these quotes from the Buddha's teaching about how weight and the second sati botanas the most important.

25
00:04:38,000 --> 00:04:46,000
There's a teacher in Burma who practices jitha nupas and he's famous for being a master in practicing jitha nupas.

26
00:04:46,000 --> 00:04:49,000
And I don't buy this really.

27
00:04:49,000 --> 00:04:57,000
I mean, their arguments are sound, but it seems to be like the tittia suta, the blind men and the elephant.

28
00:04:57,000 --> 00:05:01,000
I agree, that's maybe pushing it too far and I don't want to really criticize.

29
00:05:01,000 --> 00:05:14,000
But it's in danger of that and it doesn't seem to be a particularly, that doesn't seem to be the perfect way to focus on any one of the four to focus on anything.

30
00:05:14,000 --> 00:05:18,000
I mean, take the general and try to be as flexible as possible.

31
00:05:18,000 --> 00:05:35,000
I think the four sati botanas, if you look at what the Buddha said about it is being like the four posts of a corral to keep the mind in check, like the four trees that this monkey is going through.

32
00:05:35,000 --> 00:05:50,000
So the four sati botanas being the the monks or the meditators go chara, their resort, their pasture and not to go outside of the four sati botanas.

33
00:05:50,000 --> 00:05:54,000
So I really push the four sati botanas.

34
00:05:54,000 --> 00:06:01,000
My teacher really pushes the four sati botanas and that's one of the great reasons why I have such faith and respect it.

35
00:06:01,000 --> 00:06:06,000
So through his teachings, I've come to see how much I agree with that.

36
00:06:06,000 --> 00:06:12,000
The four sati botanas are not too much and not too little.

37
00:06:12,000 --> 00:06:17,000
So I stick with stick by that all four.

38
00:06:17,000 --> 00:06:31,000
So I guess it depends on which case I think for example, who's going to say, who's going to say what the meditator needs to focus on in a moment besides what is there.

39
00:06:31,000 --> 00:06:38,000
If there's thought there, why, why, you know, of course they should focus on thought if there's waiting another at that moment they should focus on waiting.

40
00:06:38,000 --> 00:06:49,000
But if the meditator is the one who says I should focus on this or this, then 99% of the time they're just following their own bias and it's not what they should be doing.

41
00:06:49,000 --> 00:07:10,000
And then I thought that your practice, one that you teach, it was based on focusing always when you're often regardless of what's happening in your language.

42
00:07:10,000 --> 00:07:20,000
But only because there's it's a base, there's nothing else when there's nothing else there go, well, something comes up.

43
00:07:20,000 --> 00:07:41,000
Well, not in the beginning maybe, but not when you get, when you get quiet and down.

44
00:07:41,000 --> 00:08:03,000
There's no reason to stay with the abdomen per se except that it's easy to find that it helps you calm down and helps keep you focused so you don't get too carried away with, you know, you can get easily get overwhelmed by thoughts and so on and very easy to get carried away and no longer be mindful.

45
00:08:03,000 --> 00:08:15,000
Yeah, that's one of the reasons I find it more useful because it's more transferable to my life where you have lots of thoughts.

46
00:08:15,000 --> 00:08:41,000
And it's easier to put your thoughts on that now.

47
00:08:41,000 --> 00:08:52,000
Could be, but it may not be, you may not be progressing as a result in the daily life, why is it not easy to focus on the body.

48
00:08:52,000 --> 00:09:03,000
It's easy to get lost in the thoughts and easy to stop being objective and start being, you know, giving rise to the ego of the eye.

49
00:09:03,000 --> 00:09:12,000
But no, it's certainly possible there's nothing wrong with it per se.

50
00:09:12,000 --> 00:09:17,000
I guess all I would say, you know, rather than argue one way or the other.

51
00:09:17,000 --> 00:09:25,000
Very, very much recommend to keep in mind all four of the Saty bitana and of course to use the mantras as I teach it.

52
00:09:25,000 --> 00:09:35,000
If you're not using the mantra, then it's actually a different tradition and I don't have a lot to say.

53
00:09:35,000 --> 00:09:56,000
Yeah, well, I have a question about the mantras and it relates to the question, there is some now that's a good chance that someone asked about about all the languages and you go to use the English.

54
00:09:56,000 --> 00:10:07,000
It's not important. The word is just a tool to remind you, whatever is going to remind you of the object as it is, whatever is going to be all set together.

55
00:10:07,000 --> 00:10:17,000
We're about the consciousness of the object itself that's a reminder of the object and the reminder of the nature of the object.

56
00:10:17,000 --> 00:10:28,000
This is all over the place. Consciousness is aware of the object as being good, as being bad, as being me, as being mine. Consciousness is not pure all the time.

57
00:10:28,000 --> 00:10:35,000
You need a consciousness that is clearly aware, this is just what it is.

58
00:10:35,000 --> 00:10:46,000
This is what mindfulness is that grass is the true nature of the object without grasping the characteristics or the particulars or judging it at all.

59
00:10:46,000 --> 00:10:56,000
So you don't have to use the word, but it's far preferable in according to our tradition.

60
00:10:56,000 --> 00:10:59,000
But trying to find a word is simple.

61
00:10:59,000 --> 00:11:21,000
If you look for a rising in many years, it's a whole expansion or a touch. If you look at the evidence that that's in your account, for example, you may go into more details of what the sensation is the best of that each month.

62
00:11:21,000 --> 00:11:24,000
Sorry, I don't get it. Can you ask it again?

63
00:11:24,000 --> 00:11:37,000
Because a rising sounds like a concept, there's no really feeling of rising, it's more like an expansion or the ravine you can see your points.

64
00:11:37,000 --> 00:11:42,000
Yeah, well, as a non, sorry, go ahead.

65
00:11:42,000 --> 00:11:56,000
Yeah, I don't know what could be like the concept of receiving.

66
00:11:56,000 --> 00:12:13,000
But if you're not a native English speaker, it doesn't seem like it's rising at all. But it's not a rising motion. It's nothing new at the word rising at all. It's just that that's what we call that in English.

67
00:12:13,000 --> 00:12:18,000
That's what reminds us. That's what it reminds us of.

68
00:12:18,000 --> 00:12:26,000
But in French, they say, go and play, they go and play, which is much more technically correct.

69
00:12:26,000 --> 00:12:32,000
Because in fact, when you're standing particularly your other thing is rising up.

70
00:12:32,000 --> 00:12:39,000
I know, but in English, it's a different kind of rising. The rising is the inflation. It's just an English in you.

71
00:12:39,000 --> 00:12:45,000
That's something that we should keep in mind to let people know because there's a lot of non-native.

72
00:12:45,000 --> 00:12:52,000
I mean, here I get a lot of non-native English speakers and I still tell them to use rising and falling, but it's funny.

73
00:12:52,000 --> 00:12:57,000
It's interesting that we could maybe have better words because in Thai, it's pong na yupna pong na.

74
00:12:57,000 --> 00:13:06,000
It's inflating less and yupna is deflating, inflating deflating, which is very correct.

75
00:13:06,000 --> 00:13:16,000
It's just that in English, we do understand it to be rising and falling for some reason.

