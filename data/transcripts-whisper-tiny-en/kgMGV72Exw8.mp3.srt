1
00:00:00,000 --> 00:00:09,720
Okay then I guess I'll just get started here. Welcome everyone. Today is the

2
00:00:09,720 --> 00:00:16,040
first time I'll be teaching this early in the morning and the idea being that

3
00:00:16,040 --> 00:00:19,920
there are many people from other parts of the world for whom it's no longer

4
00:00:19,920 --> 00:00:30,160
morning and so it session seems a little bit more accessible to people from say

5
00:00:30,160 --> 00:00:37,600
Europe. So glad to see so many people coming on the first day that's great. Today I

6
00:00:37,600 --> 00:00:44,280
thought I'd do something a little bit different because I'm involved in a

7
00:00:44,280 --> 00:00:54,440
few different projects in my teaching and it gets to be a little bit repetitive

8
00:00:54,440 --> 00:01:01,380
where I find myself saying the same thing in different forums. So one of the

9
00:01:01,380 --> 00:01:06,360
great things about the internet is that you can record stuff and leave it for

10
00:01:06,360 --> 00:01:11,480
people to play later and it's just as though they were right there listening.

11
00:01:11,480 --> 00:01:16,880
And so that's what I'm trying to do now I'm recording this and I'm going to try

12
00:01:16,880 --> 00:01:25,680
to use this session to answer some questions for my Ask a Monk series on

13
00:01:25,680 --> 00:01:32,560
YouTube. So people will ask questions and I put up a video answering their

14
00:01:32,560 --> 00:01:36,840
questions. So I'm going to try and experiment to do that here to answer some

15
00:01:36,840 --> 00:01:42,840
people's questions that they've already asked and if anyone here has questions

16
00:01:42,840 --> 00:01:49,080
please bring them up and this will be a special Ask a Monk session. So before

17
00:01:49,080 --> 00:01:56,480
anybody asks a question I'm going to go go to my Ask a Monk page and pick the

18
00:01:56,480 --> 00:02:03,640
question that that someone asked recently. This is a question by Drivoseng.

19
00:02:03,640 --> 00:02:09,200
In your video about walking meditation you mentioned the possibility of

20
00:02:09,200 --> 00:02:13,960
curing the diseases through meditation. Could you say more about it and about

21
00:02:13,960 --> 00:02:20,280
the link between practice and health? I think this is a good topic it's

22
00:02:20,280 --> 00:02:24,480
something that we should all understand when we come to practice meditation

23
00:02:24,480 --> 00:02:32,440
that indeed meditation does have several physical health benefits. And on

24
00:02:32,440 --> 00:02:37,600
the one hand we don't want to focus too much attention or too much of our

25
00:02:37,600 --> 00:02:45,320
concern on the physical side but on the other hand the idea that meditation

26
00:02:45,320 --> 00:02:52,200
helps our physical health shows that gives a good example of how the

27
00:02:52,200 --> 00:02:57,800
meditation works and shows quite clearly how meditation does affect a

28
00:02:57,800 --> 00:03:07,120
change in one's being. Why I say that we don't want to focus too much

29
00:03:07,120 --> 00:03:12,280
attention on physical health is because one of the most important things for

30
00:03:12,280 --> 00:03:17,760
mental health is to not worry and fret and concern ourselves about the

31
00:03:17,760 --> 00:03:27,000
physical considering that the physical is not stable it's not permanent and it's it's not

32
00:03:27,000 --> 00:03:36,080
possible to to affect a permanent transformation on the body so as to say

33
00:03:36,080 --> 00:03:42,560
that this body is going to last forever this body is going to be always pleasant

34
00:03:42,560 --> 00:03:47,960
and agreeable to me it's never going to be subject to suffering considering that

35
00:03:47,960 --> 00:03:56,560
that's not possible we have to train our minds to let go. The great thing is

36
00:03:56,560 --> 00:04:02,160
just that when we find that when we let go of our our physical body in many

37
00:04:02,160 --> 00:04:07,160
ways that our physical health actually improves because of the effect that

38
00:04:07,160 --> 00:04:12,840
the meditation practice and mental development because of the effect that our

39
00:04:12,840 --> 00:04:21,320
mind has on the body the ability of our minds to create suffering and to create

40
00:04:21,320 --> 00:04:34,240
well-being. So how is it that meditation brings about an effect on the physical

41
00:04:34,240 --> 00:04:42,280
body? In Buddhism according to the Buddha there are four kinds of sickness and

42
00:04:42,280 --> 00:04:47,400
each of these types of sickness can be influenced into a varying degree so it's

43
00:04:47,400 --> 00:04:51,040
not to say that meditation is going to cure every sickness. I remember when I

44
00:04:51,040 --> 00:04:58,680
first started meditating they explained why we're doing walking meditation and

45
00:04:58,680 --> 00:05:05,240
how it helps you out physically as well and so I remember I remember doing

46
00:05:05,240 --> 00:05:10,240
walking meditation and thinking it was going to cure my eyesight and fix my

47
00:05:10,240 --> 00:05:21,880
teeth and ridiculous things like that so I think perhaps it's a little bit

48
00:05:21,880 --> 00:05:26,720
extreme but I think for many of us we get this idea as well that some

49
00:05:26,720 --> 00:05:31,320
meditation is going to be a cure-all and I even mentioned in in one of my

50
00:05:31,320 --> 00:05:36,440
videos that that walking meditation has been known to cure cancer I think

51
00:05:36,440 --> 00:05:40,400
that may have been going a little bit too far I think at least I don't have

52
00:05:40,400 --> 00:05:44,800
any proof that that meditation you know there's no statistical proof what you

53
00:05:44,800 --> 00:05:48,880
can see for yourself is that meditation does change the body and I would say

54
00:05:48,880 --> 00:05:55,720
that even things like cancer can be ameliorated that some effect can be can be

55
00:05:55,720 --> 00:06:07,960
can be found through the practice but on the one hand we're not we have to be

56
00:06:07,960 --> 00:06:14,040
very careful that we don't try to to expect too much that the meditation is

57
00:06:14,040 --> 00:06:18,120
not going to somehow make us in perfect physical health especially since

58
00:06:18,120 --> 00:06:27,360
that's not possible and eventually the body has to has to die but okay so

59
00:06:27,360 --> 00:06:32,640
there are four kinds of physical ailments four kinds of sicknesses and these

60
00:06:32,640 --> 00:06:39,040
kinds of sickness depend on they're different according to their cause so

61
00:06:39,040 --> 00:06:44,960
the various illnesses that arise come from various types of cause and the

62
00:06:44,960 --> 00:06:52,400
first type of cause is through food through the ingestion of external

63
00:06:52,400 --> 00:07:01,040
physical elements either either through through the mouth or through

64
00:07:01,040 --> 00:07:10,560
through contact with our body in terms of you know maybe something you

65
00:07:10,560 --> 00:07:17,640
could you could imagine I don't know whether radiation and so on would fit

66
00:07:17,640 --> 00:07:22,800
under this but let's let's maybe stick just a food to physical physically

67
00:07:22,800 --> 00:07:32,320
eating this is the first type of illness the second is from the environment

68
00:07:32,320 --> 00:07:43,520
and so this would include all sorts of viruses and pollution and so on the

69
00:07:43,520 --> 00:07:50,200
third type of illness is caused by the mind directly and so this would

70
00:07:50,200 --> 00:07:59,920
include stress-based illness illness that comes from from anger illness that

71
00:07:59,920 --> 00:08:06,520
comes from depression and so on and the fourth one is illness that comes

72
00:08:06,520 --> 00:08:10,960
from karma this is the kind of illness that comes spontaneously in this

73
00:08:10,960 --> 00:08:19,600
life and doesn't have any particular cause genetic defects and ailments and

74
00:08:19,600 --> 00:08:26,480
so on cancer cancer some cancers probably can be attributed to this I would say

75
00:08:26,480 --> 00:08:37,880
not all a no not AIDS very very genetic disorders not exactly disorders but

76
00:08:37,880 --> 00:08:42,920
you know the the tendency that people have to get diabetes or heart disease

77
00:08:42,920 --> 00:08:48,080
more than others so even though they're their food and their their environment

78
00:08:48,080 --> 00:08:51,920
might be ordinary they still end up getting diseases where others don't

79
00:08:51,920 --> 00:08:59,760
people who have allergies and so on so these these four factors contribute to

80
00:08:59,760 --> 00:09:09,560
our physical well-being or physical malays our physical illness or

81
00:09:09,560 --> 00:09:15,200
sickness and so that how does the meditation effect affect these things

82
00:09:15,200 --> 00:09:28,640
the the first one as far as food goes I think is is pretty easy to explain the

83
00:09:28,640 --> 00:09:33,600
reason why we get sick because of food is generally and you know far and wide

84
00:09:33,600 --> 00:09:41,160
for them for the for the far greater part is because of our our attachments to

85
00:09:41,160 --> 00:09:47,000
taste our attachments deter certain types of food which obviously is is very

86
00:09:47,000 --> 00:09:51,360
much affected by meditation this doesn't have anything to do with walking

87
00:09:51,360 --> 00:09:56,360
meditation but it's important to mention that it's not as though sitting is

88
00:09:56,360 --> 00:10:02,200
somehow going to change change this or it's not it's not the most important

89
00:10:02,200 --> 00:10:06,600
thing that meditation suddenly you know say solves all of our our problems

90
00:10:06,600 --> 00:10:11,760
from overeating and so on I have meditators who came to meditate and thought

91
00:10:11,760 --> 00:10:16,280
somehow it was going to make them thin and so on and it doesn't really seem

92
00:10:16,280 --> 00:10:20,960
to do that because you just sit around a lot but what it can do is change your

93
00:10:20,960 --> 00:10:26,920
eating habits and and that's very important as well the other thing that

94
00:10:26,920 --> 00:10:31,240
particularly walking meditation can do and this is important to mention as

95
00:10:31,240 --> 00:10:37,600
well is that the walking meditation allows you to digest your food better and

96
00:10:37,600 --> 00:10:42,800
this is one thing I talked about in my videos when we practice walking

97
00:10:42,800 --> 00:10:47,800
meditation we're moving the body and we're we're allowing the body its chance

98
00:10:47,800 --> 00:10:52,240
to to digest it's like a massage but it's not just a massage of the muscles

99
00:10:52,240 --> 00:10:57,000
it's a massage of the whole body loosening up the digestive tract and you'll

100
00:10:57,000 --> 00:11:00,160
find that sometimes when you're doing walking meditation suddenly you have to

101
00:11:00,160 --> 00:11:03,840
go to the washroom you have to use the toilet if you do do it early in the

102
00:11:03,840 --> 00:11:11,280
morning or in the evening you'll find that it's it's actually in many ways

103
00:11:11,280 --> 00:11:16,560
it's it has an effect similar to a laxative it allows the food to to digest and

104
00:11:16,560 --> 00:11:22,440
to to be released freely because it loosens up the body I think a lot of

105
00:11:22,440 --> 00:11:26,800
people fail to to take into account this sort of benefit of walking meditation

106
00:11:26,800 --> 00:11:31,560
and so they do a lot of sitting meditation if you just do mainly sitting

107
00:11:31,560 --> 00:11:38,000
meditation you can find that your your your bodily functions are can be

108
00:11:38,000 --> 00:11:45,560
inhibited you find it I mean this is according to the Buddha that walking

109
00:11:45,560 --> 00:11:49,440
meditation has a great purpose the Buddha himself practice walking meditation

110
00:11:49,440 --> 00:11:54,680
every day in alternation with sitting and he encouraged his monks to do it

111
00:11:54,680 --> 00:11:58,880
as well and you can find this is when you practice that walking meditation

112
00:11:58,880 --> 00:12:07,640
has this benefit the second type of illness in terms of the environment

113
00:12:07,640 --> 00:12:11,800
well we're starting to get into that this is probably one where directly

114
00:12:11,800 --> 00:12:20,600
meditation cannot influence your well-being cannot cannot help you the

115
00:12:20,600 --> 00:12:24,040
environment is something that often we have very little control over we

116
00:12:24,040 --> 00:12:29,560
find ourselves we have to live in an area in in in coming to contact with with

117
00:12:29,560 --> 00:12:33,840
other people come into contact with viruses and so on and it can just be a

118
00:12:33,840 --> 00:12:39,280
matter of chance and therefore often can be attributed to karma as to whether

119
00:12:39,280 --> 00:12:46,400
or not we become sick but here meditation can can certainly help though you

120
00:12:46,400 --> 00:12:50,680
know whether there is statistical evidence or not I can't say but certainly

121
00:12:50,680 --> 00:12:54,640
you can feel the effect when you practice meditation you can feel that you

122
00:12:54,640 --> 00:12:59,040
know you're you're able to burn up as common the common cold and flu and so

123
00:12:59,040 --> 00:13:05,520
on if you ever get have a flu or a fever and you you meditate on it you can

124
00:13:05,520 --> 00:13:14,040
find that it you're able to you're able to overcome it and you find that it

125
00:13:14,040 --> 00:13:18,640
actually burns up the sickness through the meditation because your body is

126
00:13:18,640 --> 00:13:28,280
able to build up the power and the the it's able to just flow smoothly the blood

127
00:13:28,280 --> 00:13:32,400
and the systems in the body are able to function properly because the mind is

128
00:13:32,400 --> 00:13:38,760
is is coordinating them the mind has has become harmonious and just by the

129
00:13:38,760 --> 00:13:44,800
simple harmony we can find that we overcome many sicknesses that arise I

130
00:13:44,800 --> 00:13:49,360
wouldn't say that that things like AIDS or so on can somehow be cured but

131
00:13:49,360 --> 00:13:52,800
I wouldn't deny it and I'm gonna talk about that in the in the in the last one

132
00:13:52,800 --> 00:14:01,560
here but the next one is sicknesses that come from from the mind and this

133
00:14:01,560 --> 00:14:08,160
is where we directly affect our physical well-being people who have

134
00:14:08,160 --> 00:14:15,520
have tension in the body people who have headaches insomnia-based diseases

135
00:14:15,520 --> 00:14:23,120
sicknesses even even sicknesses that that can be very very physical and

136
00:14:23,120 --> 00:14:26,720
seem to have some sort of physical basis I know a man who has heart

137
00:14:26,720 --> 00:14:34,320
palpitations I know a woman who has a urinal tract infection or or

138
00:14:34,320 --> 00:14:38,880
something some problem with their urinary tract and they've both managed

139
00:14:38,880 --> 00:14:42,560
to pinpoint it down to their stress that it comes from anxiety that it comes

140
00:14:42,560 --> 00:14:45,080
from stress and because there's nothing physically wrong with them and they've

141
00:14:45,080 --> 00:14:51,560
done tests these kind of tests where you you you take some kind of stress

142
00:14:51,560 --> 00:14:56,240
inhibitor medication and suddenly the symptoms go away so you can see that

143
00:14:56,240 --> 00:15:00,200
it's based on stress once you take the medication the symptoms go away and

144
00:15:00,200 --> 00:15:04,840
you're perfectly healthy and and the medication is a is a stress inhibitor

145
00:15:04,840 --> 00:15:11,240
that's all it is it has nothing to do with the physical body so what you find

146
00:15:11,240 --> 00:15:13,960
when you meditate is that not on this meditation do away with your mental

147
00:15:13,960 --> 00:15:21,880
illness is like depression or or stress or worry or fear or anxiety sadness

148
00:15:21,880 --> 00:15:26,640
and so on it also does away with many physical ailments that arise they're

149
00:15:26,640 --> 00:15:31,080
from they can make your whole system the system the body work much better as a

150
00:15:31,080 --> 00:15:37,800
result the fourth type of illness and one that I suppose is pretty

151
00:15:37,800 --> 00:15:45,800
controversial and it's much more of a Buddhist sort of sort of concept is the

152
00:15:45,800 --> 00:15:51,440
idea that that meditation can cure a karmic based disease karmic based

153
00:15:51,440 --> 00:16:02,240
sicknesses like cancer or even even some sort of genetic defect or or or

154
00:16:02,240 --> 00:16:07,080
allergies or so and that the meditation can actually affect a change and I'd

155
00:16:07,080 --> 00:16:14,600
say there's not much documented evidence in this regard but the theory in

156
00:16:14,600 --> 00:16:18,480
the meditation is that there are certain things that are based on karma

157
00:16:18,480 --> 00:16:23,880
that based on on things that we've done in the past and through our good

158
00:16:23,880 --> 00:16:31,360
deeds through our development of goodness we can actually combat and we can

159
00:16:31,360 --> 00:16:38,480
counteract the the the evil that we've done there are cases that have been

160
00:16:38,480 --> 00:16:43,160
documented by meditation centers of people who have had tumors now whether

161
00:16:43,160 --> 00:16:46,520
they were malignant or not I don't know and had these tumors disappear before

162
00:16:46,520 --> 00:16:50,960
their surgery through meditation before they went to the surgery they went to

163
00:16:50,960 --> 00:16:54,720
meditate and they meditated strenuously and suddenly the tumor disappeared and

164
00:16:54,720 --> 00:17:02,960
the doctor was shocked and didn't aware it had gone these are documented there

165
00:17:02,960 --> 00:17:08,840
are cases of various ailments again which might be mind-based or might be karmic

166
00:17:08,840 --> 00:17:14,080
karmic based that can be cured through the meditation walking meditation

167
00:17:14,080 --> 00:17:18,880
particularly helps the body and I want to stress this because of the movements

168
00:17:18,880 --> 00:17:25,440
when you're sitting you can you can focus the mind on a specific point and it

169
00:17:25,440 --> 00:17:30,480
can often help in terms of tumors and so on but walking meditation affects

170
00:17:30,480 --> 00:17:37,760
the whole body it it moves your your awareness around the body and it changes

171
00:17:37,760 --> 00:17:42,520
the way we move our body much in the way that something like Tai Chi does so in

172
00:17:42,520 --> 00:17:47,160
general meditation I think has a great effect on the body and I would encourage

173
00:17:47,160 --> 00:17:53,560
people to undertake walking meditation and to see that meditation in

174
00:17:53,560 --> 00:18:00,720
general has a great benefit for the body with the caution that it might not

175
00:18:00,720 --> 00:18:04,600
have as big of a new factor as immediate of an effect as one would like and

176
00:18:04,600 --> 00:18:08,920
if you're still clinging to the body especially it's not likely to have a

177
00:18:08,920 --> 00:18:13,000
great impact on your physical well-being because it's through letting go that

178
00:18:13,000 --> 00:18:18,080
we become free from suffering okay so that's a question I took quite a bit of

179
00:18:18,080 --> 00:18:24,360
time there so if anyone has any questions here you're welcome to ask them

180
00:18:28,640 --> 00:18:36,000
questions about Buddhism but meditation about the monks life otherwise I've

181
00:18:36,000 --> 00:18:39,080
got more on my website

182
00:18:50,280 --> 00:19:01,640
isn't it distracting to walk and meditate no when you meditate it I suppose it

183
00:19:01,640 --> 00:19:07,600
could be if your mind isn't focused but when you walk you focus your mind on

184
00:19:07,600 --> 00:19:15,000
the foot to say that it's distracting to walk and meditate is to limit

185
00:19:15,000 --> 00:19:19,080
yourself to meditating in a various in a various

186
00:19:19,080 --> 00:19:25,840
circumscribed set of conditions meditation is something that we should

187
00:19:25,840 --> 00:19:33,040
undertake at all times and distraction is in the mind when you focus your

188
00:19:33,040 --> 00:19:36,240
mind on something your mind is focused it doesn't really matter what the body is

189
00:19:36,240 --> 00:19:42,400
doing you can be walking and have your mind in a totally different zone people

190
00:19:42,400 --> 00:19:46,880
who practice karate or martial arts dancing and so on can focus their mind

191
00:19:46,880 --> 00:19:51,680
greatly on what they're doing I don't think there should be any question that

192
00:19:51,680 --> 00:19:57,080
you can't focus your mind it's it's not what we're used to obviously

193
00:19:57,080 --> 00:20:00,680
sitting meditation is the easiest to focus your mind but sitting meditation

194
00:20:00,680 --> 00:20:06,200
can also be the hardest for bringing forth the effort necessary in

195
00:20:06,200 --> 00:20:14,400
meditation for for keeping up with the various things that arise because

196
00:20:14,400 --> 00:20:23,480
your mind is is easily inclined towards laziness if you're outside is this

197
00:20:23,480 --> 00:20:29,240
not dangerous no no I think if you try it if you're if you're outside walking

198
00:20:29,240 --> 00:20:32,240
around just say walking walking walking to yourself and you find yourself

199
00:20:32,240 --> 00:20:37,000
actually much more alert and it's when you're not mindful that you actually

200
00:20:37,000 --> 00:20:41,000
get distracted I've been walking down the street and when I'm not mindful suddenly

201
00:20:41,000 --> 00:20:45,880
I find I'm I'm crossing the street in a red light or so on but when you're

202
00:20:45,880 --> 00:20:49,400
perfectly mindful you're aware at every I've at every moment what's going on

203
00:20:49,400 --> 00:20:53,000
and you know right away when you've reached the the curb and when to look and

204
00:20:53,000 --> 00:21:00,280
see what the light is and so on yeah when when you walk you say walking walking

205
00:21:00,280 --> 00:21:03,600
just knowing or you can say right left right left just knowing which foot is

206
00:21:03,600 --> 00:21:08,760
moving keeping the mind with the foot you find that actually that that opens you

207
00:21:08,760 --> 00:21:14,680
up to the rest of the experience as well it doesn't it doesn't you

208
00:21:14,680 --> 00:21:23,880
welcome it doesn't limit you to just the foot it takes practice I suppose but

209
00:21:23,880 --> 00:21:28,000
it's a lot better than letting your mind drift when you walk because you can be

210
00:21:28,000 --> 00:21:31,600
walking and suddenly space out and there's track of where you're walking but

211
00:21:31,600 --> 00:21:36,320
when you're focusing the difficulty of it it keeps you even more alert more

212
00:21:36,320 --> 00:21:45,560
aware of what's going on okay if there's no other questions please ask

213
00:21:45,560 --> 00:21:50,520
questions if you're interested I'm just gonna go through some other here

214
00:21:50,520 --> 00:21:59,160
here's one from Simon or CC pathless this hi again you to demo I was also

215
00:21:59,160 --> 00:22:03,200
wondering about what we hope to achieve through meditation and Buddhism is

216
00:22:03,200 --> 00:22:07,240
there a bigger picture to it than being free of suffering and delusions sometimes

217
00:22:07,240 --> 00:22:16,000
I feel I need some past memories to define my path Simon this one comes often

218
00:22:16,000 --> 00:22:22,560
where people feel the need to for some reason to remember their past lives in

219
00:22:22,560 --> 00:22:28,920
order to progress on the path or in order to be encouraged to continue meditation

220
00:22:28,920 --> 00:22:33,200
practice for non Buddhist this doesn't really is really an issue for people who

221
00:22:33,200 --> 00:22:36,560
don't believe in past lives and so on it's for them it's just yeah I'm here to

222
00:22:36,560 --> 00:22:40,040
meditate because I see the benefit of it and actually that's the easiest that

223
00:22:40,040 --> 00:22:45,480
makes it really easy to progress on the path the problem is when we start

224
00:22:45,480 --> 00:22:51,840
learning about Buddhist concepts and applying our defilements to them if you

225
00:22:51,840 --> 00:22:58,160
feel the need to remember past lives say or gain magical powers or so on then

226
00:22:58,160 --> 00:23:02,480
the obvious answer is that you still have need and you still have desire and

227
00:23:02,480 --> 00:23:08,080
you're never going to progress on the Buddhist path if you if you feel like you

228
00:23:08,080 --> 00:23:16,240
somehow require these things to gain faith and confidence then I would say

229
00:23:16,240 --> 00:23:20,640
you're trying to gain faith and confidence in in the wrong thing our faith

230
00:23:20,640 --> 00:23:24,720
and confidence should be in the benefits of the meditation practice that

231
00:23:24,720 --> 00:23:31,520
meditation clears our mind and and the immediate benefits not some sort of

232
00:23:31,520 --> 00:23:37,960
you know at next life benefit or so on as I've always said Buddhism doesn't

233
00:23:37,960 --> 00:23:43,680
teach this life next life last life not not on a deep sense the in the deepest

234
00:23:43,680 --> 00:23:48,960
sense Buddhism teaches the continuation of reality that reality continues

235
00:23:48,960 --> 00:23:52,680
for a moment to moment to moment to moment when we die nothing changes so since

236
00:23:52,680 --> 00:23:57,240
nothing changes it's really not anything special to think about the past life

237
00:23:57,240 --> 00:24:01,640
or the next life these things are are just extensions of of the reality here

238
00:24:01,640 --> 00:24:06,640
and now we don't need to think about them in order to be fulfilled in order to

239
00:24:06,640 --> 00:24:11,080
be successful in our meditation

240
00:24:11,080 --> 00:24:24,600
and the other funny thing about this question is that you're asking is

241
00:24:24,600 --> 00:24:29,200
there something greater than freedom from suffering and delusion and and you

242
00:24:29,200 --> 00:24:32,880
know you got to ask what could be greater than freedom from suffering and

243
00:24:32,880 --> 00:24:44,200
delusion isn't isn't that enough what's what's the bigger picture but don't

244
00:24:44,200 --> 00:24:52,680
we need them to burn off our karma no actually we don't we don't have to burn

245
00:24:52,680 --> 00:24:57,400
off our karma at all we have to burn off our defilements is when we have no

246
00:24:57,400 --> 00:25:03,400
defilements we won't perform further karma karma means action we've done

247
00:25:03,400 --> 00:25:07,200
actions in the past and they're going to bring result here in the in the

248
00:25:07,200 --> 00:25:13,800
present and in the future but or they may bring results in the present or in

249
00:25:13,800 --> 00:25:18,080
the future but whether we receive those results or not is not really the

250
00:25:18,080 --> 00:25:23,480
problem the problem is whether we create more causes whether we perform

251
00:25:23,480 --> 00:25:28,520
future karma and the only way to stop that is from is by getting rid of the

252
00:25:28,520 --> 00:25:35,680
cause there's a technical issue here that if all karma all actions all

253
00:25:35,680 --> 00:25:41,640
all ethically charged actions that we did did bring necessarily bring an

254
00:25:41,640 --> 00:25:45,840
effect and there would be no escape from suffering because we'd have to

255
00:25:45,840 --> 00:25:53,840
continue on and on and on forever but what the Buddha said is that the our

256
00:25:53,840 --> 00:25:59,760
actions if they give a result then the nature of those results is based on the

257
00:25:59,760 --> 00:26:04,480
action not that they must give a result based on the nature of the action so

258
00:26:04,480 --> 00:26:09,800
if we do a good deed the only result that can possibly bring is a good

259
00:26:09,800 --> 00:26:14,120
result if we do a bad deed the only possible result that can bring is a bad

260
00:26:14,120 --> 00:26:17,440
result it doesn't mean that they are going to bring results but the

261
00:26:17,440 --> 00:26:21,480
important point here is that we're not worried about the results we're worried

262
00:26:21,480 --> 00:26:30,360
about the causes we're not trying to stop ourselves from from falling into

263
00:26:30,360 --> 00:26:35,200
suffering based on things we've done in the past

264
00:26:35,200 --> 00:26:43,360
we have defilements in the mind and that's what we're trying to be free from

265
00:27:05,200 --> 00:27:33,280
okay I'm going to stop recording there

