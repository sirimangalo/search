1
00:00:00,000 --> 00:00:13,600
Welcome to our evening, Dhamma.

2
00:00:13,600 --> 00:00:21,400
So last night, one of the things that came up was friendship.

3
00:00:21,400 --> 00:00:25,320
Friendship was a good friendship with the evil.

4
00:00:25,320 --> 00:00:35,920
Good friends, bad friends, thought in due to spend a little bit of time talking about

5
00:00:35,920 --> 00:00:40,120
friendship, according to Buddhism.

6
00:00:40,120 --> 00:00:44,520
Friendship is a curious thing.

7
00:00:44,520 --> 00:00:59,760
I would say friendship, in many cases, is something that misleads us from right and

8
00:00:59,760 --> 00:01:06,760
wrong, from good and bad.

9
00:01:06,760 --> 00:01:10,840
We find ourselves caught up with people because they're my friend.

10
00:01:10,840 --> 00:01:16,840
It's a curious phrase, it's a curious excuse, or reason for doing something.

11
00:01:16,840 --> 00:01:28,120
Now, if we mean that we owe them something, a debt of gratitude, perhaps, that's reasonable.

12
00:01:28,120 --> 00:01:31,240
But it tends to be more than that.

13
00:01:31,240 --> 00:01:40,040
We identify with individuals, from Buddhist perspective, it's quite possible that we've

14
00:01:40,040 --> 00:01:47,040
had some connection coming from past lives with certain people.

15
00:01:47,040 --> 00:01:54,440
Now, you notice how sometimes you're just unable to make a connection with people.

16
00:01:54,440 --> 00:01:58,680
You might think, well, that's someone I'd like to get to know, but maybe they don't

17
00:01:58,680 --> 00:02:06,840
have anything to do with you or you look at someone and there's just no connection.

18
00:02:06,840 --> 00:02:11,840
It just faces in the crowd.

19
00:02:11,840 --> 00:02:16,400
When you talk to them, there's no connection, but other people, you find yourself caught

20
00:02:16,400 --> 00:02:30,080
up with them, perhaps, your whole life, and you identify with them as being a friend.

21
00:02:30,080 --> 00:02:36,720
Even with a debt of gratitude, though, I don't think there's any reason we should let

22
00:02:36,720 --> 00:02:44,400
that color of the way we relate to others.

23
00:02:44,400 --> 00:02:49,960
Let people drag us down the wrong path just because they've done good for us in the past.

24
00:02:49,960 --> 00:02:52,960
We don't want to abandon them.

25
00:02:52,960 --> 00:03:02,080
Certainly, not the right way, it doesn't help us, it doesn't help them.

26
00:03:02,080 --> 00:03:12,680
Even spend all our time and energy trying to help an individual who doesn't want to

27
00:03:12,680 --> 00:03:13,680
be helped.

28
00:03:13,680 --> 00:03:20,240
They're going down a destructive path and spend all your energy trying to dissuade them,

29
00:03:20,240 --> 00:03:30,240
even when the evidence clearly shows that they're not to be dissuaded from their

30
00:03:30,240 --> 00:03:36,800
path.

31
00:03:36,800 --> 00:03:42,320
Friendship is taken quite seriously, it's something important in Buddhism, something we

32
00:03:42,320 --> 00:03:50,960
have to see in its proper light.

33
00:03:50,960 --> 00:03:54,640
Friendship is two things.

34
00:03:54,640 --> 00:03:58,840
First of all, friendship involves friendliness.

35
00:03:58,840 --> 00:04:03,320
Ideally, we're friendly towards everyone, it would be nice if we could all be friends.

36
00:04:03,320 --> 00:04:07,000
There's no question about that.

37
00:04:07,000 --> 00:04:11,680
We should be a good friend, really to everyone.

38
00:04:11,680 --> 00:04:17,240
Having the attitude of being friendly towards all people, I think, is perfectly valid.

39
00:04:17,240 --> 00:04:30,880
There's never a reason to be cruel or mean or unkind to others, sometimes your kindness

40
00:04:30,880 --> 00:04:39,720
takes the form of limiting your engagement with people for their benefit, for your benefit,

41
00:04:39,720 --> 00:04:52,280
for the general goodness of you both, but it should never be cruel or mean, and sometimes

42
00:04:52,280 --> 00:05:03,800
karma and past life relationships means, most of us, it means we have to be involved

43
00:05:03,800 --> 00:05:11,680
with people who we wouldn't otherwise be inclined to be involved with, and in fact, that

44
00:05:11,680 --> 00:05:17,720
makes it all the more important to be kind and to be friendly because it'll be a source

45
00:05:17,720 --> 00:05:26,320
for constant or common conflict, if we have bad karma with someone and our inclination

46
00:05:26,320 --> 00:05:33,120
is to constantly be in conflict with them, then that should be a very important part of

47
00:05:33,120 --> 00:05:40,360
our meditation practice is learning to overcome that conflict, learning to change the course

48
00:05:40,360 --> 00:05:48,360
of our journey and some sorrow so that we don't get caught up in the same cycles of

49
00:05:48,360 --> 00:05:56,960
vengeance and conflict that we were caught up in before.

50
00:05:56,960 --> 00:06:11,600
But the second thing is that it's important to be careful, careful who we, who's a

51
00:06:11,600 --> 00:06:20,720
company and who's friendship we cultivate, for some people we be a good friend and that

52
00:06:20,720 --> 00:06:28,360
can be great if we have something to provide to them and if they're keen to improve through

53
00:06:28,360 --> 00:06:32,760
our friendship, that's great.

54
00:06:32,760 --> 00:06:58,760
And then there are people who seek only.

