1
00:00:00,000 --> 00:00:17,320
Being a monk is, as we understand it, when you become a monk you leave mainstream society.

2
00:00:17,320 --> 00:00:45,320
So when the monks become mainstream, you have to leave that society as well.

3
00:00:47,320 --> 00:01:03,320
I appreciate what I'm doing this way of living.

4
00:01:03,320 --> 00:01:32,320
I'm a monk, and I'm a monk, and I'm a monk, and I'm a monk.

5
00:01:32,320 --> 00:01:41,320
But the great thing about Thailand is the support of the laypeople.

6
00:01:41,320 --> 00:01:47,320
In the end, you can really do the better a monk you are.

7
00:01:47,320 --> 00:01:49,320
Forget about what other people say.

8
00:01:49,320 --> 00:01:58,320
The better monk you are, the more the laypeople are sure that you're a good recipient of their gifts.

9
00:01:58,320 --> 00:02:18,500
My name is

10
00:02:18,500 --> 00:02:24,500
to

11
00:02:24,500 --> 00:02:27,500
To make concealer.

12
00:02:34,000 --> 00:02:33,220
I feel like I can't even hold the

13
00:02:33,600 --> 00:02:35,440
astered

14
00:02:35,460 --> 00:02:35,540
within me.

15
00:02:35,720 --> 00:02:37,180
And that I definitely taste like

16
00:02:37,200 --> 00:02:39,380
no way for my

17
00:02:39,900 --> 00:02:41,020
young

18
00:02:41,020 --> 00:02:43,620
growing

19
00:02:43,620 --> 00:02:44,180
and my

20
00:02:44,180 --> 00:02:45,420
Excuse me.

21
00:02:45,420 --> 00:02:47,260
These are crazy

22
00:02:47,080 --> 00:02:46,880
has to be

23
00:02:46,740 --> 00:02:28,660
on some

24
00:02:28,660 --> 00:02:48,760
away

25
00:02:48,760 --> 00:02:50,260
but now all

26
00:02:50,260 --> 00:02:51,080
work

27
00:02:51,720 --> 00:02:53,000
sing

28
00:02:53,000 --> 00:02:53,960
You have to

29
00:02:53,960 --> 00:03:19,600
It makes you happy, it doesn't make you wise.

30
00:03:19,600 --> 00:03:32,280
In the meditation, it helps to bring a lot of these things out.

31
00:03:32,280 --> 00:03:43,560
Things like money, things like property, fame, even the body, even oneself, there's no

32
00:03:43,560 --> 00:04:12,240
beautiful or ugly, it's all in the mind.

33
00:04:12,240 --> 00:04:19,320
I was born in Northern Ontario, Canada, and I was born in the forest.

34
00:04:19,320 --> 00:04:24,240
Somehow you could think that it's my destiny to live in the forest, I was born in the

35
00:04:24,240 --> 00:04:28,600
hospital, I was born in the house in the forest.

36
00:04:28,600 --> 00:04:33,640
I was homeschooled, my parents didn't think to send us to public school.

37
00:04:33,640 --> 00:04:39,000
And then in high school, which is when I started getting the idea that there might be

38
00:04:39,000 --> 00:04:42,320
something beyond this.

39
00:04:42,320 --> 00:04:48,760
So around grade 11, which is about 16, I think, I started getting the idea to drop out

40
00:04:48,760 --> 00:04:54,560
of school to not finish my education, at which my father said to me, I had to learn

41
00:04:54,560 --> 00:05:02,640
to be more Buddhist and try to overcome things or try to rise above things or something.

42
00:05:02,640 --> 00:05:08,280
And I think I always think that if he knew the impact of those words, he would have never

43
00:05:08,280 --> 00:05:14,880
sent that to me at the time.

44
00:05:14,880 --> 00:05:41,560
No, I've always been very extreme, do it or don't do it, there's no, there's no halfway.

