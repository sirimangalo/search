1
00:00:00,000 --> 00:00:09,120
And we're live here in the Buddhist Center in Deer Park on a wonderful Sunday afternoon

2
00:00:09,120 --> 00:00:17,600
evening, morning, midnight, depending on your location and the settings you've chosen

3
00:00:17,600 --> 00:00:20,800
for second life.

4
00:00:20,800 --> 00:00:32,480
So everyone, let's get in the mood, no, everyone put your meditator face on.

5
00:00:32,480 --> 00:00:43,760
Sit down, make sure you're sitting in real life, turn off the music, shut down Facebook,

6
00:00:43,760 --> 00:00:49,560
cross your legs, straighten your back, close your eyes, and you can open your eyes if you

7
00:00:49,560 --> 00:01:08,800
want, but let's get meditator.

8
00:01:08,800 --> 00:01:21,360
The teachings of the Lord Buddha are meant to address a problem.

9
00:01:21,360 --> 00:01:27,840
That's really, I think, how we should look at the teachings of the Buddha.

10
00:01:27,840 --> 00:01:37,560
It proposes that there's a problem and offers a solution.

11
00:01:37,560 --> 00:01:44,920
I suppose you could generalize and say that's the nature of religion.

12
00:01:44,920 --> 00:01:54,840
And you might even be so, if you want it to be negative about it.

13
00:01:54,840 --> 00:02:05,640
You could say it's the religion game, it's how people, how religions attract followers.

14
00:02:05,640 --> 00:02:17,400
Because by pointing out a problem, well, always pointing out a problem that people weren't

15
00:02:17,400 --> 00:02:26,040
aware of, but it seems often actually creating a problem where there was no, proposing

16
00:02:26,040 --> 00:02:36,680
a problem where it isn't obvious that there is one, in order to make people afraid

17
00:02:36,680 --> 00:02:50,840
or concerned, and then proposing to have the answer in the way out.

18
00:02:50,840 --> 00:02:55,320
And I think it's true that, yes, there's a great potential for abuse here.

19
00:02:55,320 --> 00:03:01,880
And certainly there is a lot of abuse that goes on.

20
00:03:01,880 --> 00:03:20,360
Many religious systems, cults, and so on, exploit people's fears and their attachments

21
00:03:20,360 --> 00:03:31,960
in order to become rich and powerful and famous and so on.

22
00:03:31,960 --> 00:03:52,440
And it's essentially the role of a physician, it's the way of a doctor to be able to point

23
00:03:52,440 --> 00:04:04,040
out a problem and offer a solution.

24
00:04:04,040 --> 00:04:06,400
And sometimes the patient isn't even aware there's a problem.

25
00:04:06,400 --> 00:04:20,280
They go to the doctor for a checkup and leave the checkup with some bad news.

26
00:04:20,280 --> 00:04:48,080
Other times they come to the doctor with a problem, not knowing what the cause is, and

27
00:04:48,080 --> 00:04:52,080
the doctor is able to explain the cause and offer a solution.

28
00:04:52,080 --> 00:05:04,320
So both of these examples work in comparison to the Buddhist teaching.

29
00:05:04,320 --> 00:05:09,840
Some people live their lives with no thought that anything's wrong, and they might go to

30
00:05:09,840 --> 00:05:19,760
see the Buddha or go to see a Buddhist teacher.

31
00:05:19,760 --> 00:05:27,320
Just for discussion or to learn something or so on.

32
00:05:27,320 --> 00:05:37,000
And find out through their dialogue and through practicing meditation and so on that actually

33
00:05:37,000 --> 00:05:39,280
there was a big problem that they weren't aware of.

34
00:05:39,280 --> 00:05:49,280
But they were setting themselves up for a great amount of suffering in the future.

35
00:05:49,280 --> 00:05:55,920
But I think for most of us, the other form is the one that we're best able to relate

36
00:05:55,920 --> 00:05:56,920
to.

37
00:05:56,920 --> 00:06:00,920
It's the idea that we have suffering, we have a problem.

38
00:06:00,920 --> 00:06:08,040
We know there's a problem, we know that life's not perfect.

39
00:06:08,040 --> 00:06:16,520
We're able to see suffering in our lives, we don't think, I don't think many people would

40
00:06:16,520 --> 00:06:20,840
deny that it exists in our lives.

41
00:06:20,840 --> 00:06:29,000
So I think this is important because it then dictates the way we approach the Buddhist

42
00:06:29,000 --> 00:06:42,720
teaching and it will shape our practice, our way of relating and engaging in the practice

43
00:06:42,720 --> 00:06:51,080
of the Buddhist teaching.

44
00:06:51,080 --> 00:07:02,640
In the way that we ask ourselves, what's the problem?

45
00:07:02,640 --> 00:07:14,320
And we remind ourselves that what we're doing here is trying to discover the problem

46
00:07:14,320 --> 00:07:21,600
and find a way to remove the problem.

47
00:07:21,600 --> 00:07:25,680
Why I think we have to keep this in mind is because it's easy to lose sight of it in the

48
00:07:25,680 --> 00:07:31,160
practice of Buddhism.

49
00:07:31,160 --> 00:07:36,520
And the Buddha gave several examples of this, that some people come to the Buddhist

50
00:07:36,520 --> 00:07:46,960
teaching and simply study it, engage in intensive study memorization, intellectual discussion

51
00:07:46,960 --> 00:07:51,680
and debate.

52
00:07:51,680 --> 00:07:58,120
Many people nowadays engage in reading, studying.

53
00:07:58,120 --> 00:08:11,240
This is for many people, the way of practicing a religion is to study it.

54
00:08:11,240 --> 00:08:19,160
For many people, the practice is not so important as the study and the intellectual appreciation

55
00:08:19,160 --> 00:08:25,600
of the teachings.

56
00:08:25,600 --> 00:08:37,400
For other people, it can be the memorization of the teachings.

57
00:08:37,400 --> 00:08:44,440
There are, in Buddhist circles, there are people who actually are able to remember vast

58
00:08:44,440 --> 00:08:59,320
tracts of discourses and teachings of the Buddha.

59
00:08:59,320 --> 00:09:08,480
It's a group project to memorize part of the Buddha's teaching, starting from the very

60
00:09:08,480 --> 00:09:18,720
first suddha in the Bhagavitya.

61
00:09:18,720 --> 00:09:21,400
And people do this as a religious practice.

62
00:09:21,400 --> 00:09:23,120
This is common in other religions as well.

63
00:09:23,120 --> 00:09:37,800
I know in Judaism, it's common or it's, you can find people who are actually able to

64
00:09:37,800 --> 00:09:53,240
apparently take a Jewish Bible, the Torah, stick a needle through it or theoretically pretend

65
00:09:53,240 --> 00:10:00,640
that if they stuck a needle through the Torah, they would be able to tell you which

66
00:10:00,640 --> 00:10:05,560
word, based on the first word, that it punctured, they would be able to tell you the word

67
00:10:05,560 --> 00:10:15,600
on each page from beginning to end.

68
00:10:15,600 --> 00:10:20,200
So I think this has been lost in the modern era.

69
00:10:20,200 --> 00:10:27,480
There isn't so much of this reliance on the memorization of texts.

70
00:10:27,480 --> 00:10:32,320
But it certainly has been, and continues to be in religious circles and important religious

71
00:10:32,320 --> 00:10:40,160
practice as though memorizing and chanting, being able to chant, being able to recite

72
00:10:40,160 --> 00:10:46,160
the teachings of the Buddha, being able to parrot it back, I suppose in modern days you

73
00:10:46,160 --> 00:10:55,480
find this in what we call quote dropping, where people are able to quote famous teachers.

74
00:10:55,480 --> 00:11:02,240
And I find this actually quite common in many schools of Buddhism that you'll find

75
00:11:02,240 --> 00:11:09,160
people rather than even quoting the Buddha, they're constantly quoting their teachers.

76
00:11:09,160 --> 00:11:16,880
While my teacher says this, my teacher says that, as though somehow it made it true,

77
00:11:16,880 --> 00:11:25,680
or it had some bearing on the person that they were talking to.

78
00:11:25,680 --> 00:11:34,480
So without perhaps the ability to explain it or even just this reliance on that as a religious

79
00:11:34,480 --> 00:11:46,400
practice, to be able to quote scripture and to recite and to chant and to memorize.

80
00:11:46,400 --> 00:11:57,400
And the Buddha said there are other people who teach and even become great teachers

81
00:11:57,400 --> 00:12:02,400
without or by at the same time neglecting their own practice.

82
00:12:02,400 --> 00:12:08,400
People who take all of their time to help other people or to teach to become famous teachers.

83
00:12:08,400 --> 00:12:14,360
There was a story in the Buddha's time, or maybe it was after the Buddha's time, and

84
00:12:14,360 --> 00:12:21,160
the story in ancient time about this monk who fell into this trap, I think I've told

85
00:12:21,160 --> 00:12:23,640
this story before.

86
00:12:23,640 --> 00:12:30,560
He was a great teacher, and all of his students became enlightened to some degree or

87
00:12:30,560 --> 00:12:44,440
rather, and some of them became Zotapana, some of them became Sakitakami, some of them became Anakami.

88
00:12:44,440 --> 00:12:50,760
And he himself had neglected his own practice, and was actually just a guy who happened

89
00:12:50,760 --> 00:12:53,040
to be able to teach quite well.

90
00:12:53,040 --> 00:12:58,560
He was able to explain the Buddha's teaching in such a way that people were able to practice.

91
00:12:58,560 --> 00:13:05,160
He had an intellectual appreciation of it, but he himself had never practiced or never exerted

92
00:13:05,160 --> 00:13:13,560
himself in intensive meditation practice.

93
00:13:13,560 --> 00:13:20,240
And so all of his students were reaching these higher states of enlightenment, and he himself

94
00:13:20,240 --> 00:13:25,760
was running around teaching and helping other people.

95
00:13:25,760 --> 00:13:30,400
And one day one of his students asked himself, one of his students, who was an Anakami,

96
00:13:30,400 --> 00:13:35,640
which means someone who has attained the third stage of enlightenment and is destined when

97
00:13:35,640 --> 00:13:41,960
they died to never come back to the world again, but instead be reborn in the god realms

98
00:13:41,960 --> 00:13:48,640
or the Brahma realms, and from there become fully enlightened and fully released, and

99
00:13:48,640 --> 00:13:57,320
turned into the purer bones, they're called.

100
00:13:57,320 --> 00:14:01,240
And he asked himself, wonder, you know, here I am, I've realized this stage of enlightenment

101
00:14:01,240 --> 00:14:04,920
and all of these other people have, I wonder what stage of enlightenment, our teacher

102
00:14:04,920 --> 00:14:05,920
has realized.

103
00:14:05,920 --> 00:14:15,800
And through his incredible strength of mind, he was able to discern for himself just by

104
00:14:15,800 --> 00:14:22,200
thinking about it, just by examining the situation and by sending his mind to the

105
00:14:22,200 --> 00:14:27,920
in this direction, he was able to ascertain that his teacher had indeed not attained

106
00:14:27,920 --> 00:14:30,040
any level of enlightenment.

107
00:14:30,040 --> 00:14:37,080
And he thought, oh, well, this won't do, this is not a proper state of affairs.

108
00:14:37,080 --> 00:14:43,680
And he thought to himself out of gratitude for his teacher that he owed it to him to help

109
00:14:43,680 --> 00:14:47,440
him become enlightened as well.

110
00:14:47,440 --> 00:14:52,960
So he went up to his teacher and right when his teacher was very, very busy, waited

111
00:14:52,960 --> 00:15:01,880
for when his teacher was busy, helping people, meeting with students and so on.

112
00:15:01,880 --> 00:15:09,120
And he came up to him and he said, teacher, I think I would like to request that it's

113
00:15:09,120 --> 00:15:12,040
time for a lesson.

114
00:15:12,040 --> 00:15:16,040
When the teacher said, I'm sorry, I don't, how can I possibly have time for a lesson

115
00:15:16,040 --> 00:15:17,040
right now?

116
00:15:17,040 --> 00:15:19,800
I have many duties and people coming to see me and so on.

117
00:15:19,800 --> 00:15:22,960
I'm far too busy for that.

118
00:15:22,960 --> 00:15:34,760
And so this student of his sat down crossed his legs and went into an absorbed state

119
00:15:34,760 --> 00:15:36,760
and started floating off the ground.

120
00:15:36,760 --> 00:15:40,720
And he opened his eyes and said to the teacher, he said, you don't have time even for

121
00:15:40,720 --> 00:15:42,040
yourself.

122
00:15:42,040 --> 00:15:47,320
How could you possibly have time to teach anything to me?

123
00:15:47,320 --> 00:15:56,640
And he floated out the room, out of the room, showing to the teacher that indeed his students

124
00:15:56,640 --> 00:16:00,480
were on a higher level than he was.

125
00:16:00,480 --> 00:16:10,520
And this of course shook the teacher up visibly and he decided that he'd had enough

126
00:16:10,520 --> 00:16:16,920
this teaching business was obviously not the proper way to approach the Buddha's teaching

127
00:16:16,920 --> 00:16:21,880
as the Buddha said, set yourself in what is right.

128
00:16:21,880 --> 00:16:27,760
Set yourself in what is right first, only then should you help others.

129
00:16:27,760 --> 00:16:31,680
And so he decided to go off into the forest.

130
00:16:31,680 --> 00:16:34,320
But he was such a famous teacher that it was very difficult.

131
00:16:34,320 --> 00:16:39,160
He had to find a forest where there was no other people around where he wouldn't be bothered

132
00:16:39,160 --> 00:16:43,080
by people coming to ask him about meditation practice.

133
00:16:43,080 --> 00:16:46,920
And finally, he found this place, no humans in the area.

134
00:16:46,920 --> 00:16:49,480
And so he started this meditation.

135
00:16:49,480 --> 00:16:58,880
And he worked very hard walking back and forth, sitting down.

136
00:16:58,880 --> 00:17:04,600
And little did you know that you can't really go anywhere where you're going to be alone

137
00:17:04,600 --> 00:17:09,080
that whether there are humans or not, there are always beings around whether they be animals

138
00:17:09,080 --> 00:17:15,360
or in this case angels, there were angels in the forest watching him.

139
00:17:15,360 --> 00:17:20,360
And that actually came down and started meditating with him when he walked, they walked

140
00:17:20,360 --> 00:17:28,200
when he said they said, when he sat, they sat.

141
00:17:28,200 --> 00:17:31,160
And so he practiced and practiced and gained nothing from it.

142
00:17:31,160 --> 00:17:34,960
He was pushing very, very hard, and someone said he was probably practicing too hard, pushing

143
00:17:34,960 --> 00:17:38,480
himself too hard, wanting too much to become enlightened.

144
00:17:38,480 --> 00:17:44,640
And of course, the wanting, the needing, the desire was blocking him, it was stopping him

145
00:17:44,640 --> 00:17:49,920
from attaining the very thing he was looking for.

146
00:17:49,920 --> 00:17:54,280
And so he got more and more frustrated and more and more upset until finally he broke

147
00:17:54,280 --> 00:17:55,280
down.

148
00:17:55,280 --> 00:18:01,120
You know, he was under such pressure that I'm this great teacher if I don't become enlightened

149
00:18:01,120 --> 00:18:07,320
or what did I say to my students and until he finally snapped and he sat down and he started

150
00:18:07,320 --> 00:18:16,600
crying and he was crying and suddenly he heard someone else crying and he opened his

151
00:18:16,600 --> 00:18:22,760
eyes and there's this angel sitting down beside him crying and crying and crying and

152
00:18:22,760 --> 00:18:26,120
he stopped crying and he says, what are you doing?

153
00:18:26,120 --> 00:18:31,040
And the angel stopped crying and said, oh, I was crying and he said, why?

154
00:18:31,040 --> 00:18:35,640
And he said, well, you know, you're the great teacher, whatever you do, that's got to

155
00:18:35,640 --> 00:18:36,640
be the way to become enlightened.

156
00:18:36,640 --> 00:18:41,120
So I've just been following you, when you walk, I walked, when you sat, I sat, when you

157
00:18:41,120 --> 00:18:49,040
started crying, I started crying, I figured that's got to be the way to become enlightened.

158
00:18:49,040 --> 00:18:55,840
And this hit him quite hard as well, made him quite ashamed of his behavior.

159
00:18:55,840 --> 00:19:02,560
And as a result of that, he was able to wake up, let go and practice in such a way as

160
00:19:02,560 --> 00:19:04,600
to become enlightened.

161
00:19:04,600 --> 00:19:11,640
And the point of the story being, teaching people is not the most important point and

162
00:19:11,640 --> 00:19:16,640
it's easy to get off track in this way, going out and trying to explain to other people

163
00:19:16,640 --> 00:19:22,760
how great Buddhism is, it's something you can remember, it's always a sign of immaturity

164
00:19:22,760 --> 00:19:31,400
when people are trying to teach when people approach you and start teaching you uninvited,

165
00:19:31,400 --> 00:19:39,800
unasked when people start giving advice without you having asked or even intimated a desire

166
00:19:39,800 --> 00:19:43,120
for any.

167
00:19:43,120 --> 00:19:47,440
But this is very common, especially when people begin to practice.

168
00:19:47,440 --> 00:19:53,960
It's called, it's an upiculate, it's a defilement of insight, it's something that arises

169
00:19:53,960 --> 00:19:56,320
when you start practicing correctly.

170
00:19:56,320 --> 00:19:59,960
You start to think, wow, everyone else should practice this way.

171
00:19:59,960 --> 00:20:14,080
And you lose sight of your own practice and start trying to teach others.

172
00:20:14,080 --> 00:20:20,400
Another thing that people often do, that the Buddha mentioned, that people will often do

173
00:20:20,400 --> 00:20:29,040
in approaching the teachings of the Buddha is to think about it, to ponder on it and to

174
00:20:29,040 --> 00:20:38,600
work it out, work it out in their mind and people who debate or people who like to think

175
00:20:38,600 --> 00:20:49,640
and like to create people who write stories or write books, people who relate to the

176
00:20:49,640 --> 00:20:52,920
Buddha's teaching intellectually.

177
00:20:52,920 --> 00:20:57,960
So these are the sorts of ways that people interact with the Buddha's teaching that are

178
00:20:57,960 --> 00:21:07,040
not getting closer to a solution to the problem.

179
00:21:07,040 --> 00:21:11,880
And so this is why it's important for us to always be asking ourselves, what is the problem?

180
00:21:11,880 --> 00:21:15,240
Now, the problem is not that we don't know enough, that we haven't read enough for the

181
00:21:15,240 --> 00:21:20,360
Buddha's teaching, it isn't that we don't know how to explain to other people.

182
00:21:20,360 --> 00:21:24,680
There are some meditators who are never able to explain to others, who find it very difficult

183
00:21:24,680 --> 00:21:28,800
to put into words the realizations that they have.

184
00:21:28,800 --> 00:21:32,680
And on the other hand, there are people who work very hard to be able to explain things

185
00:21:32,680 --> 00:21:49,920
to others, and yet themselves are gaining nothing and are not progressing on the path.

186
00:21:49,920 --> 00:21:58,000
The real problem, I guess you could say, is twofold, and the problem that we have is that

187
00:21:58,000 --> 00:22:12,480
one our minds are not, our minds are not tranquil enough, and two our minds are lacking

188
00:22:12,480 --> 00:22:16,160
in wisdom and understanding.

189
00:22:16,160 --> 00:22:22,760
So the Buddha's approach to the problem and defining a solution is twofold, is one two

190
00:22:22,760 --> 00:22:37,320
to calm the mind to the practice of meditation, to focus on an object, focus on reality

191
00:22:37,320 --> 00:22:52,080
in a way that stops the mind from flitting around superficially, that allows us to grasp

192
00:22:52,080 --> 00:22:59,000
reality or grasp the object in front of us in a meaningful way to focus our minds

193
00:22:59,000 --> 00:23:00,400
and to calm our minds down.

194
00:23:00,400 --> 00:23:08,560
And you can do this either with a conceptual object or a part of reality.

195
00:23:08,560 --> 00:23:11,680
So a conceptual object would be something that you create in your mind, whether it be

196
00:23:11,680 --> 00:23:26,080
a light or a sound or a concept of being a God, an angel, an idea, and reality would

197
00:23:26,080 --> 00:23:35,120
be something that is arising and ceasing that is coming and going in the experiential

198
00:23:35,120 --> 00:23:38,560
reality already that isn't created by the mind.

199
00:23:38,560 --> 00:23:45,280
So watching the breath, watching the stomach, watching the feet, watching pain or thoughts

200
00:23:45,280 --> 00:23:52,560
or emotions.

201
00:23:52,560 --> 00:23:58,400
And this serves to calm the mind down, the solution here is that our minds no longer

202
00:23:58,400 --> 00:24:06,360
give rise to things like greed, anger, depression, worry, fear, and so on.

203
00:24:06,360 --> 00:24:12,840
Our minds become fixed, focused, happy, calm, peaceful, relaxed.

204
00:24:12,840 --> 00:24:23,360
And for all intents and purposes, become free from suffering for a time.

205
00:24:23,360 --> 00:24:30,520
Now the reason why this is not enough of course is it's still a contrived state that

206
00:24:30,520 --> 00:24:38,880
we've created this state through the work that we've done, through the pressure of

207
00:24:38,880 --> 00:24:42,280
our concentration, which is able to suppress the defilements.

208
00:24:42,280 --> 00:24:48,800
It's a good thing because it makes our mind very clear and very pure.

209
00:24:48,800 --> 00:24:56,360
But it's a good thing primarily because it then allows us to fix things decisively,

210
00:24:56,360 --> 00:25:08,320
completely, and irrevocably or permanently.

211
00:25:08,320 --> 00:25:15,840
Because no matter how much focus and concentration you apply to reality, that focus and

212
00:25:15,840 --> 00:25:20,400
concentration alone is not going to change your mind, it's not going to change the way

213
00:25:20,400 --> 00:25:26,040
you think, it's not going to change your habits.

214
00:25:26,040 --> 00:25:37,320
It's not going to cut off the potential for the arising of defilements.

215
00:25:37,320 --> 00:25:42,720
Wisdom on the other hand, that wisdom is the final solution and the final cure.

216
00:25:42,720 --> 00:25:46,600
And wisdom is something that comes about through this clarity of mind.

217
00:25:46,600 --> 00:25:57,800
Once you have a clear mind, it's imperative that you focus entirely solely on ultimate reality,

218
00:25:57,800 --> 00:26:02,680
whether you've been practicing to calm the mind based on a concept or whether ultimate

219
00:26:02,680 --> 00:26:07,480
reality, when you're practicing to gain wisdom and insight, you have to focus on ultimate

220
00:26:07,480 --> 00:26:10,120
reality.

221
00:26:10,120 --> 00:26:19,760
Because you can't come to understand reality any other way, and you can't come to understand

222
00:26:19,760 --> 00:26:33,160
things as they are when you're creating the object of your attention.

223
00:26:33,160 --> 00:26:38,280
So in essence then we have a twofold problem, we have the problem of the defilements

224
00:26:38,280 --> 00:26:44,320
of the negative mind states, and then we have the problem of the reason why they arise.

225
00:26:44,320 --> 00:26:50,360
To stop them from arising is easy, you focus the mind, but to remove the cause of their

226
00:26:50,360 --> 00:26:56,840
arising, you have to go deeper and you have to come to understand why they arise.

227
00:26:56,840 --> 00:27:02,120
The reason why defilements arises through a misunderstanding of reality for what it is, not

228
00:27:02,120 --> 00:27:11,040
seeing things as they are, a superficial grasp of the experience in front of us at every moment.

229
00:27:11,040 --> 00:27:15,720
As an example, or there's many examples, one example is pain.

230
00:27:15,720 --> 00:27:23,000
When you're sitting and you start to feel pain, it's a very superficial experience where

231
00:27:23,000 --> 00:27:30,120
right away convinced that it's a negative experience, that it's unpleasant, it's negative,

232
00:27:30,120 --> 00:27:40,120
that, and we've right away acting on our habit, our habitual way of approaching problems,

233
00:27:40,120 --> 00:27:49,160
we've already decided and begun to react and switch positions or find a solution, a way

234
00:27:49,160 --> 00:27:52,360
to get rid of the pain.

235
00:27:52,360 --> 00:27:56,400
When we focus on things like pain on a deeper level, when we look at them clearly and

236
00:27:56,400 --> 00:28:00,360
objectively, we come to see that actually it's not so at all.

237
00:28:00,360 --> 00:28:08,160
There's nothing intrinsically negative about things like pain, or people yelling at

238
00:28:08,160 --> 00:28:16,400
us, or smells, or tastes, or sounds, or sights.

239
00:28:16,400 --> 00:28:23,440
The experience of the sense, even when there are people yelling at us, or attacking us,

240
00:28:23,440 --> 00:28:33,000
unpleasant sights, or sounds, or smells, tastes, feelings, thoughts, bad memories, worries

241
00:28:33,000 --> 00:28:40,800
about the future, problems in our life, not having enough money, not having a job, not

242
00:28:40,800 --> 00:28:51,520
having a future, and so on, and so on, hunger, there's hot, cold.

243
00:28:51,520 --> 00:28:57,120
When we focus, when we examine these things on a deeper level, which is exactly what we're

244
00:28:57,120 --> 00:29:02,640
doing through meditation, we come to see that it's not, we come to see things in a quite

245
00:29:02,640 --> 00:29:11,760
a different way, that there actually is nothing negative about any of these things, and

246
00:29:11,760 --> 00:29:19,800
we don't react, we lose this habitual reaction and tendency to compartmentalize reality

247
00:29:19,800 --> 00:29:26,480
into the good and the bad, into what is acceptable, and what is unacceptable.

248
00:29:26,480 --> 00:29:34,040
Some people like it hot, some like it cold, some people like loud music, some people

249
00:29:34,040 --> 00:29:39,080
hate loud music, it's actually a habit that we gain in our mind, and these can change

250
00:29:39,080 --> 00:29:44,280
and develop over time, obviously, you know, people, when you take your first sip of beer,

251
00:29:44,280 --> 00:29:49,680
when you take your first cigarette, it's terrible, but over time, you begin to change your habits

252
00:29:49,680 --> 00:29:53,320
and decide for yourself that it's a positive, a pleasant experience.

253
00:29:53,320 --> 00:30:00,760
People can go to the extent of believing that or of perceiving smoke entering their lungs

254
00:30:00,760 --> 00:30:11,600
as being a pleasant experience.

255
00:30:11,600 --> 00:30:16,400
And so really, this is all we're doing in meditation, we're not trying to change things.

256
00:30:16,400 --> 00:30:20,840
Once we have this level of calm, we simply start to see things for what they are, and

257
00:30:20,840 --> 00:30:26,760
we start to understand things on a far deeper level, to the point that everything that

258
00:30:26,760 --> 00:30:29,200
arises we're able to see it for what it is.

259
00:30:29,200 --> 00:30:35,320
We lose this sense of attachment to it, this habitual reaction that it's good or it's

260
00:30:35,320 --> 00:30:47,520
bad, this identification of it being me or mine, and the mind is able to find peace and

261
00:30:47,520 --> 00:30:55,120
freedom, simply through wisdom, through understanding.

262
00:30:55,120 --> 00:31:01,760
Because there's nobody that, I think we can all agree that nobody wants suffering, everybody

263
00:31:01,760 --> 00:31:07,040
wants to find peace and happiness.

264
00:31:07,040 --> 00:31:12,920
And so the reason that we create suffering for us is not because we, on a deep, down

265
00:31:12,920 --> 00:31:17,520
level, want to suffer, it's because we don't understand that that's what we're doing

266
00:31:17,520 --> 00:31:18,840
to ourselves.

267
00:31:18,840 --> 00:31:23,480
And once we can teach ourselves, once we can convince our minds that that's what we're

268
00:31:23,480 --> 00:31:30,440
doing, that this reaction, this mental process is creating suffering for us, then we'll

269
00:31:30,440 --> 00:31:31,440
stop.

270
00:31:31,440 --> 00:31:34,120
Naturally, there's no question about that.

271
00:31:34,120 --> 00:31:39,640
It's a theory, this is the theory I'm proposing to you, but it's a perfectly, absolutely

272
00:31:39,640 --> 00:31:42,240
testable and verifiable theory.

273
00:31:42,240 --> 00:31:49,280
If you start to meditate, you'll see that all it takes is a deeper, more comprehensive

274
00:31:49,280 --> 00:31:55,040
understanding of the things that you're already experiencing and already reacting to.

275
00:31:55,040 --> 00:31:58,840
Once you see them for what they are and you see the process and you see the way your

276
00:31:58,840 --> 00:32:06,000
mind is reacting, slowly but surely you'll change that.

277
00:32:06,000 --> 00:32:13,240
You'll convince yourself over time with effort that what you're doing is hurting yourself

278
00:32:13,240 --> 00:32:14,240
and you'll change.

279
00:32:14,240 --> 00:32:15,240
It's a natural process.

280
00:32:15,240 --> 00:32:17,600
You don't have to want to change.

281
00:32:17,600 --> 00:32:23,360
You don't have to wish for yourself to become enlightened or even strive in any way.

282
00:32:23,360 --> 00:32:26,600
The striving is just to see things for what they are.

283
00:32:26,600 --> 00:32:34,240
And the closer you become, you come to seeing things as they are, the more free and at ease

284
00:32:34,240 --> 00:32:39,400
and at peace with yourself, you become.

285
00:32:39,400 --> 00:32:52,880
So there you have the problem and I think fairly clear solution, I think that's enough

286
00:32:52,880 --> 00:32:53,880
for today.

287
00:32:53,880 --> 00:32:58,960
I'll give everyone some time to ask questions if you have any and otherwise we can just

288
00:32:58,960 --> 00:33:04,760
sit here and meditate together and you're all welcome to go on with your lives.

289
00:33:04,760 --> 00:33:12,640
I'd like to thank you all for coming and appreciate it to have such an interested group

290
00:33:12,640 --> 00:33:17,200
of people who return again and again to hear these talks and to support the Buddhist

291
00:33:17,200 --> 00:33:23,480
Center in its projects, I'd like to wish for you all to be able to put into practice these

292
00:33:23,480 --> 00:33:30,560
teachings and to be able to practice meditation for the development of your own selves and

293
00:33:30,560 --> 00:33:36,800
for the attainment of real and true peace, happiness and freedom from suffering for you all.

294
00:33:36,800 --> 00:33:37,800
Thank you all for coming.

295
00:33:37,800 --> 00:33:53,920
Have a great day.

