1
00:00:00,000 --> 00:00:18,880
Good evening everyone, broadcasting live December 12th 2015, maybe a short broadcast tonight.

2
00:00:18,880 --> 00:00:24,440
I'm realizing that I have quite a bit of studying what to do for the next two exams.

3
00:00:24,440 --> 00:00:28,840
I haven't taken these classes probably as seriously as I should have.

4
00:00:28,840 --> 00:00:40,240
I have to do a lot of reading, probably not going to do that while on either of these exams.

5
00:00:40,240 --> 00:00:42,960
What subject's Dante?

6
00:00:42,960 --> 00:00:50,120
Linguistics and philosophy.

7
00:00:50,120 --> 00:00:52,120
Monday we're having a study session.

8
00:00:52,120 --> 00:00:54,760
Two of my classmates are coming over.

9
00:00:54,760 --> 00:00:59,720
I'm going to sit down for the linguistics exam.

10
00:00:59,720 --> 00:01:02,640
Not really worried about it because that's the first year class, I don't think it really

11
00:01:02,640 --> 00:01:13,960
matters either way, as long as I pass it, but it's somehow we're talking with another student

12
00:01:13,960 --> 00:01:19,280
about this, with a light and the exam I said it kind of, or I was talking to someone

13
00:01:19,280 --> 00:01:29,000
about how it feels, kind of, wrong to not do your best at something, you know?

14
00:01:29,000 --> 00:01:40,880
There's still an intention to get 100% in everything, and if I don't get 100% of lesson

15
00:01:40,880 --> 00:01:50,600
has to be learned to try to get 100% next time, which is a difficult position to be in.

16
00:01:50,600 --> 00:01:57,680
We'll see if I get 100% in that one, that'd be funny.

17
00:01:57,680 --> 00:02:04,960
Got 100% going into the exam, probably got a 95, at least.

18
00:02:04,960 --> 00:02:07,600
So Latin was last weeks?

19
00:02:07,600 --> 00:02:09,560
Yeah.

20
00:02:09,560 --> 00:02:13,320
There was a bonus question, I asked if there was going to be a bonus section, because

21
00:02:13,320 --> 00:02:17,200
he puts bonus on all the quizzes, and they're actually quite generous.

22
00:02:17,200 --> 00:02:24,200
So I actually, on my quizzes, each of my quizzes, I got over 100% because of the bonus.

23
00:02:24,200 --> 00:02:28,800
I think average was like 103%.

24
00:02:28,800 --> 00:02:33,760
And then, so there was a bonus on the exam, but the bonus was actually quite cheap.

25
00:02:33,760 --> 00:02:39,200
I had 200 marks, it was a three mark bonus question, but why I bring it up?

26
00:02:39,200 --> 00:02:52,960
And it seems related, well, it was an interesting, interesting, probably to many of us,

27
00:02:52,960 --> 00:02:59,880
because he brought up this quote that had just come up recently in the media, a Latin

28
00:02:59,880 --> 00:03:01,840
quote.

29
00:03:01,840 --> 00:03:09,560
And in regards to one of the biggest stories of the past month, if anyone knows what that

30
00:03:09,560 --> 00:03:18,760
quote was, because it's actually quite a famous quote, I hadn't, you know, not being

31
00:03:18,760 --> 00:03:23,760
a news person, I hadn't seen it, but he brought up a picture, and it's now on the

32
00:03:23,760 --> 00:03:33,280
Wikipedia page, Wikipedia page, for that quote, the quote is, flocked to add neck mortgage

33
00:03:33,280 --> 00:03:43,480
to her, doesn't really bounce out, unless you're really keeping up with a news or unless

34
00:03:43,480 --> 00:03:49,720
you happen to be from France, because it's a very famous quote in France, flocked to

35
00:03:49,720 --> 00:04:04,440
add means, tossed about the fluctuate, right, fucked to add to the shaken disturbed, it's

36
00:04:04,440 --> 00:04:12,560
a matter, it's using the imagery of a boat, fucked to add means shaken about, tossed

37
00:04:12,560 --> 00:04:26,480
about, by the way, neck means and not, and more, mergy tour, mergy tour, mergy tour, mergy

38
00:04:26,480 --> 00:04:38,280
tour, merge is not merge, it's from some merge, a mergy tour means sunk, so about successful

39
00:04:38,280 --> 00:04:50,280
ship is one that is tossed about, but not submerged, not sunk, and I just barely remembered

40
00:04:50,280 --> 00:04:54,480
him bringing up and forgot totally what it was about, but I kind of guessed, and actually

41
00:04:54,480 --> 00:05:03,240
I think I got it right, tossed about, and not submerged, I think it's more like tossed

42
00:05:03,240 --> 00:05:14,480
about not sunk, but basically that, anyway it was used, they used it for many occasions

43
00:05:14,480 --> 00:05:20,920
in France to describe, to talk about how they are resilient, it's a quote about resiliency,

44
00:05:20,920 --> 00:05:26,400
but they all used it for the Paris bombings, they show that they weren't going to be,

45
00:05:26,400 --> 00:05:33,480
they weren't going to be defeated, they weren't going to let this defeat them, it's a

46
00:05:33,480 --> 00:05:41,320
quote about resiliency, but I think it's a very good quote for Buddhism, for Buddhist practice,

47
00:05:41,320 --> 00:05:50,360
and it's a very good sort of Buddhist response to things like terrorist attacks, into

48
00:05:50,360 --> 00:06:05,160
the difficulties in my shaking yes, never sunk, there's always another chance, always

49
00:06:05,160 --> 00:06:13,200
get another chance, we have the Buddha as our example of that Buddha who took four uncountable

50
00:06:13,200 --> 00:06:27,920
beyonds, and 100,000 countable beyonds, great beyonds, to become a Buddha, all the time

51
00:06:27,920 --> 00:06:39,760
it took him, the mistakes he had to make to get there, makes you look at our one little

52
00:06:39,760 --> 00:06:54,000
life, it's not really worth all the concern, we give it, like there's that Zen book

53
00:06:54,000 --> 00:07:00,360
that my mother got and showed to me, don't sweat the small stuff, and it's all small

54
00:07:00,360 --> 00:07:24,400
stuff, no questions tonight, I don't think so, yeah, they are a quote about an acrobat, right?

55
00:07:24,400 --> 00:07:31,400
Yes, I like that quote, mm-hmm, just protect, but protect is a poor translation, it is

56
00:07:31,400 --> 00:07:38,120
a literal translation, but it means guard, guard would probably be better, watch out for,

57
00:07:38,120 --> 00:08:05,160
rock habit, rock headed, guard is better, I will, can we just use guard, he watched and guarded

58
00:08:05,160 --> 00:08:14,120
by himself in the first part, but then in the second part he says, no master, with any

59
00:08:14,120 --> 00:08:43,880
switches to protect, which is weird, guard, so how do we guard ourselves?

60
00:08:43,880 --> 00:08:48,760
We practice for foundations of mindfulness and by protecting ourselves we also protect

61
00:08:48,760 --> 00:08:53,840
others, this is another interesting part of this, not only is it about everyone doing

62
00:08:53,840 --> 00:09:03,920
their own duty and being good people, but it addresses the idea that when you do that

63
00:09:03,920 --> 00:09:15,440
you also have others, why because of the leading to patience, forbearance, armlessness,

64
00:09:15,440 --> 00:09:33,560
love and compassion, by practicing mindfulness, by practicing, practicing, practicing

65
00:09:33,560 --> 00:09:46,040
for one's own betterment, one protects others, is in contrast to those people who think

66
00:09:46,040 --> 00:09:53,120
the way to be a good person, the way to do the right thing is to help others, it's not

67
00:09:53,120 --> 00:10:03,200
really because a person who helps themselves is naturally inclined to help others, it's

68
00:10:03,200 --> 00:10:09,960
not what should be the focus, right, your focus is on helping others, you neglect the

69
00:10:09,960 --> 00:10:15,280
source, if your focus is on helping yourself then you radiate goodness in all things

70
00:10:15,280 --> 00:10:42,320
that you do, if you don't have any questions we can just call it a night, say hello, thank

71
00:10:42,320 --> 00:11:02,120
you all for meditating with us, tomorrow we have meetings and Risudimaga may skip some

72
00:11:02,120 --> 00:11:11,400
of the meetings, maybe I'll do my studying while you guys are having a meeting and all

73
00:11:11,400 --> 00:11:20,880
this and I can study and be there at the same time I think, yeah we can just kind of

74
00:11:20,880 --> 00:11:36,920
call out to you if we need your input on something, sure, that sounds good, okay then

75
00:11:36,920 --> 00:11:42,240
good then everyone, thank you Robin for joining me, thank you Bante, good night

