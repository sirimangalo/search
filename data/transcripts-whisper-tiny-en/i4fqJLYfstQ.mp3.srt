1
00:00:00,000 --> 00:00:21,720
Good evening, everyone.

2
00:00:21,720 --> 00:00:50,400
Welcome to our evening talk.

3
00:00:50,400 --> 00:01:01,720
Having a conversation about law, there's a certain law that people were talking about, and

4
00:01:01,720 --> 00:01:15,000
got me thinking about the concept of rules, rules is an interesting concept.

5
00:01:15,000 --> 00:01:26,680
We need rules, how important are rules, should we have lots of rules, or only few rules?

6
00:01:26,680 --> 00:01:28,120
Where do we draw the line?

7
00:01:28,120 --> 00:01:35,640
What does it mean to set a rule?

8
00:01:35,640 --> 00:02:03,520
But as we have this concept of see that, see that which means your, it means a person's

9
00:02:03,520 --> 00:02:16,480
fear, or department, or way, it means normal, yes, that's what it means, it means normal.

10
00:02:16,480 --> 00:02:24,160
But what it means, why we use it to refer to ethics or morality, because see the refers

11
00:02:24,160 --> 00:02:29,760
to what is normal for a certain individual.

12
00:02:29,760 --> 00:02:38,480
For some people, it's normal for them to do bad things, so they're called do seala,

13
00:02:38,480 --> 00:02:52,720
someone with normal behavior that's not very good, someone is soot seala or seala, if they've

14
00:02:52,720 --> 00:03:17,040
got good ethics, if their normal behavior is ethical and good, but when we talk about

15
00:03:17,040 --> 00:03:24,320
see that we often talk about rules, and what the relationship is is that rules are like

16
00:03:24,320 --> 00:03:33,720
fence posts, right, talked about this before, if you see a cow outside of the fence,

17
00:03:33,720 --> 00:03:39,720
how you know it's in the wrong place is because of the fence posts, if it's outside of

18
00:03:39,720 --> 00:03:47,920
the boundaries, you know, the cow got out, or the horse got out.

19
00:03:47,920 --> 00:03:57,440
So if a person acts outside of a certain set of rules, this is how we define easily sort

20
00:03:57,440 --> 00:04:09,520
of in a simplistic sort of way, we define morality based on rules, because we know

21
00:04:09,520 --> 00:04:15,240
while this person has transgressed the boundaries of what is ethical, how we know that is

22
00:04:15,240 --> 00:04:21,880
because there's this rule, and they stepped outside of it, they crossed that line, rules

23
00:04:21,880 --> 00:04:22,880
make that easy.

24
00:04:22,880 --> 00:04:30,800
Now rules, of course, are problematic in terms of describing ethics and morality well,

25
00:04:30,800 --> 00:04:45,800
and are in detail, but it's important that we respect the utility of rules, and we often

26
00:04:45,800 --> 00:04:51,960
complain about Western meditators or even modern meditators of any, it's not even the Western

27
00:04:51,960 --> 00:05:00,320
thing anymore, it's even in Buddhist countries, there are meditators who have sort of abandoned

28
00:05:00,320 --> 00:05:13,440
any concept of morality or ethics in relation to meditation practice, so I have a friend

29
00:05:13,440 --> 00:05:20,000
here who wanted me to teach her how to meditate, and I knew she was a regular user of

30
00:05:20,000 --> 00:05:31,120
cannabis, and I said, well you have to stop taking psychoactive drugs first, of course

31
00:05:31,120 --> 00:05:38,480
doesn't go over well with a great portion of the spiritual world these days, and it's

32
00:05:38,480 --> 00:05:44,880
not that I have anything against marijuana, I think it's a fairly benign form of whatever

33
00:05:44,880 --> 00:05:52,520
mental alteration, it doesn't do much, it never did much for me, but it does something

34
00:05:52,520 --> 00:06:04,280
and it has this effect of not only altering the mind, but becoming a crutch by which you

35
00:06:04,280 --> 00:06:17,760
avoid ordinary reality by which you dampen the harshness of reality, so how can you deal

36
00:06:17,760 --> 00:06:25,960
with reality and it's an adulterated pure form if you're consciously avoiding it anyway,

37
00:06:25,960 --> 00:06:36,360
between being there's not this sense that morals and ethics are important, it's dangerous

38
00:06:36,360 --> 00:06:47,480
because it's perfectly possible to be an ethical and moral person without any rules,

39
00:06:47,480 --> 00:07:02,440
and it's so much easier to transgress the line of between ethics and the immorality,

40
00:07:02,440 --> 00:07:16,920
without defense, without the defense posts, without the rules, what was interesting about

41
00:07:16,920 --> 00:07:25,040
this conversation I was having was about enforcing rules, when you set up rules, you force

42
00:07:25,040 --> 00:07:33,200
people into a certain behavior, or under threat of something, and I think that's where

43
00:07:33,200 --> 00:07:42,440
it gets problematic is when you start to threaten punishment, not that punishment is

44
00:07:42,440 --> 00:07:49,640
necessarily always wrong, I think perhaps it's not in certain forms, at least in terms

45
00:07:49,640 --> 00:08:15,280
of rehabilitation and convincing the community of your sincerity to reform, but if rules

46
00:08:15,280 --> 00:08:20,480
are all about causing pain to someone who does something wrong, then of course they can

47
00:08:20,480 --> 00:08:28,920
become quite unwholesome themselves, and the rules end up being the problem, this is what

48
00:08:28,920 --> 00:08:37,920
happens in fascist societies, fascist dictatorships and so on, and we have lots of rules that

49
00:08:37,920 --> 00:08:48,800
end up being for purposes, for nefarious purposes, and then for the purposes of good, but

50
00:08:48,800 --> 00:08:53,480
I want us to warm up to rules in general, the concept of rules, it's one of these concepts

51
00:08:53,480 --> 00:08:59,580
like as I was talking about religiosity or religion, and I think it'd be good if we

52
00:08:59,580 --> 00:09:07,400
warm up to the concept of religious behavior in terms of taking things seriously, and I think

53
00:09:07,400 --> 00:09:14,920
it's also useful if we warm up to the idea of rules, preset, so I mean, these are things

54
00:09:14,920 --> 00:09:24,400
that about religion, let's say Buddhism, religiosity, rules, these are the things that

55
00:09:24,400 --> 00:09:38,720
work newcomers the most, I think, the idea of being forced to follow rules is uncomfortable

56
00:09:38,720 --> 00:09:45,800
because we've been forced to follow rules that we didn't understand, and we often tend

57
00:09:45,800 --> 00:09:57,600
to cultivate the idea that rules are a means of control, right, a means of power, power

58
00:09:57,600 --> 00:10:10,000
play and egoism and so on, by people who want a grave power, right, and of course rules

59
00:10:10,000 --> 00:10:15,760
are used in that way, but rules are not always, it's not the purpose of rules, purpose

60
00:10:15,760 --> 00:10:23,320
of rules is to control, but it's to control the community in the sense of not exactly

61
00:10:23,320 --> 00:10:30,040
control, I suppose, but it's to have some sort of control over who is considered to be

62
00:10:30,040 --> 00:10:35,800
a part of the community and who is not, so you ostracize those who are acting counter

63
00:10:35,800 --> 00:10:45,720
to the goals and direction of the community, and the meditation community should be

64
00:10:45,720 --> 00:10:51,200
the same, right, if someone started acting in a way that was disturbing other meditators,

65
00:10:51,200 --> 00:11:00,920
we would have to probably just kick them out and say sorry, you broke the rules, rules

66
00:11:00,920 --> 00:11:08,040
are also useful in that way to have something to point to, hey, we have this rule and

67
00:11:08,040 --> 00:11:13,920
you broke it, because otherwise you say you're just behaving poorly, doesn't have the

68
00:11:13,920 --> 00:11:21,760
same power to it, right, not that we're looking to kick people out or not that we have

69
00:11:21,760 --> 00:11:29,680
a really have that I can remember, but it's good to have guidelines and rules, take the

70
00:11:29,680 --> 00:11:34,120
eight precepts, if someone breaks the eight precepts, I wouldn't set a meditator, it was

71
00:11:34,120 --> 00:11:45,640
quite funny, it came to me for reporting and he was kind of sad, feeling guilty and he

72
00:11:45,640 --> 00:11:58,960
said and he did something wrong and Buddhism, he says, I just couldn't handle the lust

73
00:11:58,960 --> 00:12:07,840
and last night I masturbated, this is only happened to me once, it was only time someone's

74
00:12:07,840 --> 00:12:14,440
ever actually done that in a meditation course, but he couldn't handle it and I said,

75
00:12:14,440 --> 00:12:18,200
well, okay, well, you'll have to retake the eight precepts then because that's the tradition,

76
00:12:18,200 --> 00:12:22,600
it just wouldn't be if you break the meat, and he was shocked, he said, it's against

77
00:12:22,600 --> 00:12:31,840
the eight precepts, we hadn't even realized, so clear understanding of the eight precepts

78
00:12:31,840 --> 00:12:38,880
is a good one, and Jantong told me this, told us this story once, well, he just mentioned

79
00:12:38,880 --> 00:12:43,920
really that when he was in Burma, he went to a small meditation center and he asked

80
00:12:43,920 --> 00:12:50,760
them, what are the rules of the center, right, meditation center in Bangkok would have had

81
00:12:50,760 --> 00:12:56,200
lots of rules, what are the rules we were sent to, and the monks looked at him and said,

82
00:12:56,200 --> 00:13:03,880
what, well, we have the eight precepts, I mean, isn't that enough, and that struck him

83
00:13:03,880 --> 00:13:12,120
and it struck me when he said that as somehow important, you're not to, that there's

84
00:13:12,120 --> 00:13:20,040
something elegant at any rate of having few rules, but having a clear set of rules to

85
00:13:20,040 --> 00:13:26,680
be clear that the important rules are the eight precepts, at our center here we don't have

86
00:13:26,680 --> 00:13:32,920
that many extra rules, we have rules about not keeping leftovers in the kitchen, in the

87
00:13:32,920 --> 00:13:39,960
refrigerator because they will go bad and someone might eat poisonous food, we have rules

88
00:13:39,960 --> 00:13:46,200
about recycling, we have rules about, we don't have so many rules, we have a few rules

89
00:13:46,200 --> 00:13:55,560
and guidelines, look after the place because we don't have a cleaning lady or a man, no

90
00:13:55,560 --> 00:14:14,560
janitors here, but it's important to understand that rules aren't all morale, all that

91
00:14:14,560 --> 00:14:25,920
work, we mean by see that, in fact, I think what most of us have some sense of, with some

92
00:14:25,920 --> 00:14:33,120
sense of the fact that true morality is nothing to do with rules, true morality is a part

93
00:14:33,120 --> 00:14:40,200
of the practice, and so there's sometimes this, for newcomers anyway, there might be this

94
00:14:40,200 --> 00:14:45,160
suspicion of the rules for that reason because we understand that it's all about what's

95
00:14:45,160 --> 00:14:51,560
going on in the mind and so we think, well, who needs rules, right? And we're suspicious

96
00:14:51,560 --> 00:14:55,720
when we hear about all these rules, the eight precepts, we think these are people who are

97
00:14:55,720 --> 00:15:07,800
adhering to rights and rituals and clinging to dogmas, right? And so it's important to clear

98
00:15:07,800 --> 00:15:12,760
that up and to clarify that, no, these aren't dogmas, these are signposts and they're

99
00:15:12,760 --> 00:15:19,200
also things we can point to, we can point back at the signposts and say, look, you went

100
00:15:19,200 --> 00:15:28,560
the wrong way, see, there's a signpost, that's what rules are for, morality is much more

101
00:15:28,560 --> 00:15:35,880
than that, it is the rules, it is keeping the rules and keeping within the fence that makes

102
00:15:35,880 --> 00:15:44,640
up is made up by the fence post to stay within the realm of good morality using the

103
00:15:44,640 --> 00:15:54,640
five and the eight precepts as our guide, a guide, but not by no means a exhaustive list

104
00:15:54,640 --> 00:16:04,240
of things you might do wrong, torture isn't on that list, for example. Even in the eight

105
00:16:04,240 --> 00:16:10,320
precepts, there's nothing about harsh speech or divisive speech, not about gossiping,

106
00:16:10,320 --> 00:16:16,280
chatting, none of that's on there, and those would be considered to be problematic for

107
00:16:16,280 --> 00:16:31,440
meditators. And so the Vsudi manga breaks this down into four types of morality,

108
00:16:31,440 --> 00:16:35,360
I'm not sure where it comes from, whether it can't remember if it's somewhere else,

109
00:16:35,360 --> 00:16:39,880
but at least it's in the Vsudi manga does a good job of explaining the four types of

110
00:16:39,880 --> 00:16:56,400
morality, there's the first is the rules, the second is reflecting on our usage, reflecting

111
00:16:56,400 --> 00:17:07,160
on our positions, reflecting on the objects with which we interact, let's put it that way,

112
00:17:07,160 --> 00:17:19,240
reflecting morality through reflecting. And so this refers to our everyday life when we eat,

113
00:17:19,240 --> 00:17:29,200
when we sleep, when we put on our clothes to reflect and to be mindful of our daily lives,

114
00:17:29,200 --> 00:17:34,080
our daily activities. Of course this is accomplished through meditation practice when a person

115
00:17:34,080 --> 00:17:45,360
is mindful, there's not much danger of indulging in the excess, but it's still a good

116
00:17:45,360 --> 00:17:49,000
guideline for meditators, it's certainly a good guideline for people living in the world

117
00:17:49,000 --> 00:18:02,240
for you, who have to deal with houses and possessions and cars and all the equipments of

118
00:18:02,240 --> 00:18:09,200
lay life to consider the use and to be careful in our use of things, so we don't go

119
00:18:09,200 --> 00:18:16,040
to excess, so that we don't eat in excess, so that we eat to live, not live to eat, and

120
00:18:16,040 --> 00:18:23,040
we don't become obsessed with good tastes and even get sick out of our gluttony, that

121
00:18:23,040 --> 00:18:32,720
we don't engage in frivolous activities like wearing lots of makeup or jewelry or fancy clothes,

122
00:18:32,720 --> 00:18:44,920
trying to look good, styling our hair, trimming our beards and so on, trying to look nice,

123
00:18:44,920 --> 00:18:54,920
cleaning about our weight, becoming obsessed with medicines, the things that we use, drugs,

124
00:18:54,920 --> 00:19:03,080
being very careful of what we use, and by extension everything else, our cars, our electronic

125
00:19:03,080 --> 00:19:13,320
devices, but ultimately especially for the meditators here in the center, it means being

126
00:19:13,320 --> 00:19:19,240
mindful, when you eat eating should be a meditation, should be chewing, chewing, swallowing,

127
00:19:19,240 --> 00:19:32,480
even when you raise the food to your mouth, raising, placing, when you shower, showering

128
00:19:32,480 --> 00:19:39,800
should be a meditation, scrubbing, scrubbing, when you brush your teeth brushing, brushing,

129
00:19:39,800 --> 00:19:44,800
when you sleep at night, sleeping should be a lying meditation, you shouldn't just try to

130
00:19:44,800 --> 00:19:53,040
go to sleep, you should lie down and try to stay awake, try to meditate, try to be as

131
00:19:53,040 --> 00:20:00,480
mindful as you can as you fall asleep, aware of the rising and the falling, or even just

132
00:20:00,480 --> 00:20:09,640
that you're lying lying, and of course you accomplish this one without much trouble.

133
00:20:09,640 --> 00:20:17,320
The third is our livelihood, so this is not really applicable to meditators here in the

134
00:20:17,320 --> 00:20:29,360
center, your livelihood is to come up to the kitchen and do defrost some pizza, it's

135
00:20:29,360 --> 00:20:36,520
the extent of your livelihood these days, and those of you in the world, this would mean

136
00:20:36,520 --> 00:20:44,720
ethical livelihood, not cheating other people or not, being greedy or wanting to make

137
00:20:44,720 --> 00:20:51,480
lots of money and be rich, not obsessing over that kind of thing, not engaging in wrong

138
00:20:51,480 --> 00:20:56,760
types of livelihood that involve killing, stealing, lying, cheating, or even just dealing

139
00:20:56,760 --> 00:21:05,040
with things that are problematic, like selling weapons or poisons or human beings or

140
00:21:05,040 --> 00:21:16,840
even animals, and the fourth one, really the most important, and it applies to everyone

141
00:21:16,840 --> 00:21:22,300
here for all of us who are interested in the meditation practice, it's called Injriya

142
00:21:22,300 --> 00:21:38,500
sanga rasidha, regarding the senses, cultivating a state of a normal state, cultivating

143
00:21:38,500 --> 00:21:47,080
restraint of the senses, or guarding of the senses as a normal state of being.

144
00:21:47,080 --> 00:21:52,180
When we see something, let it only be seeing, when we hear, let it only be hearing, when

145
00:21:52,180 --> 00:21:57,180
we smell, let it only be smelly, even in terms of what we look at or what we allow ourselves

146
00:21:57,180 --> 00:22:06,740
to engage in, sometimes you want to avoid delicious food because you know that it's difficult

147
00:22:06,740 --> 00:22:11,540
to be mindful when you have delicious food and you end up just devouring it and you don't

148
00:22:11,540 --> 00:22:16,040
realize where it's gone, so you want to have plain and ordinary food because it makes

149
00:22:16,040 --> 00:22:24,900
you more mindful, certainly sometimes when you're walking around you want to avoid looking

150
00:22:24,900 --> 00:22:30,620
here and there so that you get caught up and lose the track of your mindfulness, lose

151
00:22:30,620 --> 00:22:37,620
the moment, very easy with the eyes to get caught up and to get lost and to lose track

152
00:22:37,620 --> 00:22:45,620
of where you are and what you're doing.

153
00:22:45,620 --> 00:22:52,540
So guarding, even in the sense of avoiding certain sensations, see certain sights or

154
00:22:52,540 --> 00:22:59,140
sounds or smells or something, but an ultimate level this refers to the meditation, it means

155
00:22:59,140 --> 00:23:11,140
when you see, you just remind yourself seeing, when you hear, you remind yourself hearing.

156
00:23:11,140 --> 00:23:19,680
It may seem like something quite simple and superficial even, this isn't deep teaching,

157
00:23:19,680 --> 00:23:20,680
right?

158
00:23:20,680 --> 00:23:28,420
Seeing is just seeing what's so special about that, the truth is seeing when you perceive

159
00:23:28,420 --> 00:23:34,980
seeing as just seeing that can lead you to enlightenment, that what you see is impermanent

160
00:23:34,980 --> 00:23:38,220
to the rises and ceases.

161
00:23:38,220 --> 00:23:43,380
It's suffering means it can't possibly be a source of happiness for you and anything

162
00:23:43,380 --> 00:23:49,260
you see that you cling to will only cause you stress and disappointment because it can't

163
00:23:49,260 --> 00:23:54,100
satisfy it and it's not capable of bringing you true happiness.

164
00:23:54,100 --> 00:24:01,060
It's non-self, you can't control it, you can't hold on to it, you can't keep it the

165
00:24:01,060 --> 00:24:07,100
way it is or keep it from coming.

166
00:24:07,100 --> 00:24:14,540
So when you say to yourself seeing, even just that, the shift that that brings about in

167
00:24:14,540 --> 00:24:23,620
your awareness and that suddenly makes you aware of this moment, that alone can lead

168
00:24:23,620 --> 00:24:34,100
to true and irrevocable enlightenment and the true change of lineage and leads one to become

169
00:24:34,100 --> 00:24:39,100
a Sotapana, Sakadagami, Hanagami, even in our hunt, there are even cases of people who

170
00:24:39,100 --> 00:24:46,420
became particular Buddhist, just by looking at something and by seeing impermanence in

171
00:24:46,420 --> 00:24:48,780
their experience.

172
00:24:48,780 --> 00:24:54,340
One person became a bhajakabuddha by watching a leaf fall from the tree and it just struck

173
00:24:54,340 --> 00:24:59,740
them so profoundly that they suddenly became mindful and were able to see impermanence

174
00:24:59,740 --> 00:25:14,860
suffering in non-self and become a Buddha, or become a bhajakabuddha anyway.

175
00:25:14,860 --> 00:25:18,740
In fact, to some extent, this is an important thing, this simplicity of the practice.

176
00:25:18,740 --> 00:25:26,100
Because a huge part of the problem is something we call bhajakabuddha, which is complicating

177
00:25:26,100 --> 00:25:33,020
things, extrapolating and making more of things than they actually are.

178
00:25:33,020 --> 00:25:40,300
So by design the practice is simple, it's meant to return you again and again to the

179
00:25:40,300 --> 00:25:51,180
simple ordinary nature of reality by design, that's what it's for.

180
00:25:51,180 --> 00:25:55,180
Anyways, so there you go, that's the demo for tonight.

181
00:25:55,180 --> 00:26:04,660
Hope you all are able to cultivate Sila, and may we all cultivate Ariya Sila or Ariya

182
00:26:04,660 --> 00:26:15,620
Sila, and the Sila, the normal behavior that is delightful to the enlightened ones.

183
00:26:15,620 --> 00:26:26,740
Okay, if there are any questions, I'll answer them.

184
00:26:26,740 --> 00:26:34,300
I'm actually going to look on the web here, wondering if we have some questions on the

185
00:26:34,300 --> 00:26:35,300
web.

186
00:26:35,300 --> 00:26:54,460
We have two questions on the web site.

187
00:26:54,460 --> 00:26:59,860
I've been having difficulties in meditation, even when reading or taking part in conversation

188
00:26:59,860 --> 00:27:03,180
certain sounds distract me very easily.

189
00:27:03,180 --> 00:27:05,260
Me so phony I think it's called.

190
00:27:05,260 --> 00:27:09,900
Sadly, I can't find a consistently quiet place to meditate, when meditating in the presence

191
00:27:09,900 --> 00:27:15,220
of noise I must not listening almost constantly, and even when it's quiet there is an unpleasant

192
00:27:15,220 --> 00:27:17,620
sense of anticipation.

193
00:27:17,620 --> 00:27:20,860
Is there some way I can alter the practice to make this more bearable or what do you

194
00:27:20,860 --> 00:27:25,300
recommend?

195
00:27:25,300 --> 00:27:29,900
I'd prefer hearing than listening, because hearing refers to the experience, listening

196
00:27:29,900 --> 00:27:37,660
refers to the action, which is not really an action we want to engage in, per se, semantics,

197
00:27:37,660 --> 00:27:43,340
but the hearing is probably better, but that's not a problem.

198
00:27:43,340 --> 00:27:54,820
This is quite apropos of tonight's talk.

199
00:27:54,820 --> 00:27:58,700
The unpleasant sense of anticipation is also important.

200
00:27:58,700 --> 00:28:05,820
The anticipation comes from the fact that sound is impermanent, it's impermanence

201
00:28:05,820 --> 00:28:10,420
suffering in non-self, and that's what you're seeing, that's an important part of the practice.

202
00:28:10,420 --> 00:28:20,300
I mean this is really an opportunity ripe for you to realize, gain deprealization, at least

203
00:28:20,300 --> 00:28:27,980
of your own neuroses or your own problems.

204
00:28:27,980 --> 00:28:35,380
The deep sense of anticipation or anxiety or fear should be noted, and then the sound

205
00:28:35,380 --> 00:28:36,940
should be noted, it's fine.

206
00:28:36,940 --> 00:28:40,180
This will not last, it's not something that's going to stay with you.

207
00:28:40,180 --> 00:28:47,060
Forever, and even if it did, it would be a perfectly valid object of meditation, so whatever

208
00:28:47,060 --> 00:28:54,580
you feel about the sound, if you're frustrated or anxious or worried or so on, it should

209
00:28:54,580 --> 00:28:58,620
be noted, anxious, anxious, when you get frustrated, frustrated, frustrated, when you're

210
00:28:58,620 --> 00:29:01,740
just hearing the sounds, it would just be hearing hearing.

211
00:29:01,740 --> 00:29:06,620
Then you would try, would prefer you to try to come back to the stomach, but it's fine,

212
00:29:06,620 --> 00:29:12,900
if then you're right away, go back to the sound and note hearing hearing, it's perfectly

213
00:29:12,900 --> 00:29:18,740
valid, and it takes patience, and it builds patience, and that's important.

214
00:29:18,740 --> 00:29:25,020
As you do this, your patience will increase, and your objectivity will increase, and sound

215
00:29:25,020 --> 00:29:29,900
will become less and less of a thing to be feared.

216
00:29:29,900 --> 00:29:33,660
But it's a habit, you know, this is a habit you've obviously developed, it's going

217
00:29:33,660 --> 00:29:38,420
to take time to rework that habit, so just be patient and understand that meditation

218
00:29:38,420 --> 00:29:49,020
is a process, it's not a pill that you can take as a quick fix.

219
00:29:49,020 --> 00:29:52,780
I meditate regularly, and I've learned a lot through your teachings, I was hoping to

220
00:29:52,780 --> 00:29:58,060
go to a meditation, center near my home, and it's connected to the Damakaya Foundation,

221
00:29:58,060 --> 00:30:02,100
if you heard of it, some of the Jewish is similar to what you practice here.

222
00:30:02,100 --> 00:30:07,980
You know, the Damakaya is quite different, it's considered by many to be a cult.

223
00:30:07,980 --> 00:30:13,460
I tend to think I'm one of those people, the Damakaya cult has some really weird ideas

224
00:30:13,460 --> 00:30:18,260
and practices, and they've done a lot of good in a superficial way, but they've done

225
00:30:18,260 --> 00:30:25,660
a lot of bad in terms of perverting the Orthodox teachings of Buddhism, so I would not

226
00:30:25,660 --> 00:30:37,340
recommend that, not in the least.

227
00:30:37,340 --> 00:30:42,260
Here's something I do mean to, I have meant to do a video, and I'll try to do on this

228
00:30:42,260 --> 00:30:55,540
weekend, so I'm going to Florida next month to hopefully do some teaching, the

229
00:30:55,540 --> 00:31:06,660
time I visit my mother, to visit my friends there, and also to take part in an act of

230
00:31:06,660 --> 00:31:09,140
wholesomeness.

231
00:31:09,140 --> 00:31:11,940
So Robin, I think, is Robin here tonight?

232
00:31:11,940 --> 00:31:16,180
That looks like Robin is that Robin sitting in the back row?

233
00:31:16,180 --> 00:31:23,660
Yeah, that's funny how you look like Robin, it's quite impressive actually.

234
00:31:23,660 --> 00:31:29,260
From behind the new way.

235
00:31:29,260 --> 00:31:38,980
So yeah, Robin has graciously set up for the second year in the row, an online campaign

236
00:31:38,980 --> 00:31:45,220
to raise money for the children's home in Tampa Bay, Florida.

237
00:31:45,220 --> 00:32:02,180
Now the whole deal here is we're not suddenly becoming obsessed with charity work, or we're

238
00:32:02,180 --> 00:32:08,140
not going to get too involved with charity work, but the idea is because it's the gift

239
00:32:08,140 --> 00:32:19,060
giving season, and there's a general sort of question, I suppose, of how to or what sorts

240
00:32:19,060 --> 00:32:24,620
of gifts to give that are of greatest benefit, and there's often even a question of ways

241
00:32:24,620 --> 00:32:30,620
of giving more meaningful than simply giving things to people that they really don't

242
00:32:30,620 --> 00:32:34,900
need, especially when there are people in need.

243
00:32:34,900 --> 00:32:43,660
So charity often comes up, and so the idea here was for myself and for my family, and

244
00:32:43,660 --> 00:32:50,700
for all of you and your families, to provide an alternative option of gift giving, and one

245
00:32:50,700 --> 00:32:56,500
that would be sort of impactful because we did it all together.

246
00:32:56,500 --> 00:33:02,980
So this is a home that takes care of kids who have troubles, who maybe run away from home.

247
00:33:02,980 --> 00:33:10,940
There was no specific reason why we chose this one, it just happens to be one that we're

248
00:33:10,940 --> 00:33:18,820
familiar with, or people we know are familiar with, and seems like a wholesome, non-biased

249
00:33:18,820 --> 00:33:28,060
or specific sort of charity, and so we're all, we have this opportunity to get together

250
00:33:28,060 --> 00:33:35,540
and do our gift giving this way, on behalf of people we love, or just in lieu of gift

251
00:33:35,540 --> 00:33:42,060
giving, so my mom has been talking about, they were talking about doing this in the past

252
00:33:42,060 --> 00:33:46,540
with her husband and his family, and so they're hopefully going to get involved if they

253
00:33:46,540 --> 00:33:53,940
haven't already, and instead of giving gifts, we'll take this money and do some goodness

254
00:33:53,940 --> 00:33:59,500
with it, and do some good deeds with him, it's just a thing, so Rob and I think just

255
00:33:59,500 --> 00:34:06,100
post a link in the chat, if anybody wants to take part in that, please feel welcome,

256
00:34:06,100 --> 00:34:11,820
again it's not something I want to pressure you into by any means, I'm not soliciting

257
00:34:11,820 --> 00:34:23,220
money here, but just announcing this collective act of goodness, so there you go.

258
00:34:23,220 --> 00:34:34,660
And with that, there are no other questions I guess, I think, and I'm going to say good

259
00:34:34,660 --> 00:35:01,740
night, and wish you all a good practice.

