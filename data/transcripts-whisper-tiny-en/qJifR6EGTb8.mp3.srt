1
00:00:00,000 --> 00:00:03,840
Hello and welcome back to Ask a Month.

2
00:00:03,840 --> 00:00:11,680
Today I'll be answering the question in regards to physical exercise and whether it is

3
00:00:11,680 --> 00:00:22,880
important to give up or is it proper to give up physical exercise and concern about the

4
00:00:22,880 --> 00:00:28,760
body as well being and health and welfare or is it important for us to look after the

5
00:00:28,760 --> 00:00:33,760
body and therefore important for us to engage in physical exercise.

6
00:00:33,760 --> 00:00:41,720
I think basically this is the question, what role does physical exercise play in the life

7
00:00:41,720 --> 00:00:47,880
of say an enlightened being to put it one way?

8
00:00:47,880 --> 00:00:52,920
I think I have a pretty definite answer on this and that is that it doesn't play any part.

9
00:00:52,920 --> 00:01:02,200
The idea of outright physical exercise seems to me a little bit silly really as I think

10
00:01:02,200 --> 00:01:14,040
it does for anyone who has taken up a lifestyle where we eat food simply enough to live.

11
00:01:14,040 --> 00:01:23,440
And this may seem a little bit odd for most people because most of us think that that's

12
00:01:23,440 --> 00:01:24,440
how we eat.

13
00:01:24,440 --> 00:01:28,120
We all eat simply enough to live but really we don't.

14
00:01:28,120 --> 00:01:35,840
I think in general we eat far more than if necessary to live but the amount that we eat is generally

15
00:01:35,840 --> 00:01:41,360
necessary to keep up our lifestyle which tend to include things like physical exercise,

16
00:01:41,360 --> 00:01:48,880
physical exertion and a lot of mental stress, a lot of emotional stress and so on.

17
00:01:48,880 --> 00:01:54,440
And so as a result of that the food is barely adequate.

18
00:01:54,440 --> 00:02:01,840
How for many people the food is in fact is far too much, there are people who become

19
00:02:01,840 --> 00:02:10,240
obese or who get sicknesses because of the fact that they are eating beyond their needs

20
00:02:10,240 --> 00:02:17,160
and their requirements but even for those people who are eating enough and what seems

21
00:02:17,160 --> 00:02:25,000
to be enough to live by, it means enough to live that lifestyle by a lifestyle of one

22
00:02:25,000 --> 00:02:32,800
who is well built and has a good shape.

23
00:02:32,800 --> 00:02:37,240
Remember thinking how silly it was the first time I thought of this question when one of

24
00:02:37,240 --> 00:02:44,880
the meditators he became a monk and I think after he was a monk he was talking about

25
00:02:44,880 --> 00:02:52,920
how as a monk he felt kind of sad that he couldn't do the same things he did as a

26
00:02:52,920 --> 00:02:58,360
layperson and so when he was meditating as a layperson in our monastery this was in

27
00:02:58,360 --> 00:03:01,400
what voice would happen in Shanghai Thailand.

28
00:03:01,400 --> 00:03:07,200
Every day he would go running I think or jogging down the steps and this is a very large

29
00:03:07,200 --> 00:03:12,640
staircase in a thousand steps or something and then jogging all the way back up every

30
00:03:12,640 --> 00:03:17,360
day and I said why would you do that and said you know I can't do that and I said well why

31
00:03:17,360 --> 00:03:18,360
would you do that?

32
00:03:18,360 --> 00:03:23,760
I wanted to keep in shape and it just struck me and so on I think it's something that

33
00:03:23,760 --> 00:03:27,880
most people wouldn't find funny but I found it incredibly funny and I thought I said to

34
00:03:27,880 --> 00:03:33,720
them I said what shape, what shape do you want to be in a square shape, a round shape,

35
00:03:33,720 --> 00:03:40,000
a triangle shape, is there some specific shape that you want to keep in and you know

36
00:03:40,000 --> 00:03:45,840
obviously we understand this expression and what it means but if you think about it it

37
00:03:45,840 --> 00:03:51,400
really is meaningless I mean what's the difference between this shape and that shape?

38
00:03:51,400 --> 00:03:56,640
The obvious answer being that our attachment to the body leads us to which for it to

39
00:03:56,640 --> 00:04:01,920
be in a certain shape and our attachment to central pleasure and romance and so on leads

40
00:04:01,920 --> 00:04:07,160
us to want to have a body that is attractive to others as well and this is the only reason

41
00:04:07,160 --> 00:04:16,800
for wanting to be in a certain shape but okay so this is one part of the argument but the

42
00:04:16,800 --> 00:04:21,520
claim that people make is that this is necessary for your physical well-being well as

43
00:04:21,520 --> 00:04:27,560
I said it's necessary if you want to eat the amount of food that you're eating if a person

44
00:04:27,560 --> 00:04:32,880
eats a lot of food then yes there will be necessary and it will be you know they go hand

45
00:04:32,880 --> 00:04:38,000
at hand a person wants to exercise and keep in good shape then they have to eat a lot

46
00:04:38,000 --> 00:04:43,680
if a person wants to eat a lot and wants to eat rich foods and wants to eat all day as

47
00:04:43,680 --> 00:04:48,280
you know monks and meditators we only eat once in the morning so it seems really ridiculous

48
00:04:48,280 --> 00:04:55,840
to consider doing any real exercise but if you want to eat a lot and if you have a lifestyle

49
00:04:55,840 --> 00:05:02,000
where you eat three times a day or four times a day or you'll have all sorts of snacks

50
00:05:02,000 --> 00:05:07,920
and so on then yeah you really need to exercise because your body is being polluted by

51
00:05:07,920 --> 00:05:17,640
all of these toxins and excess even excess of good things so it's really only in short

52
00:05:17,640 --> 00:05:23,680
it's really only because of our lifestyle that we think that somehow it's going to exercise

53
00:05:23,680 --> 00:05:27,640
is going to be somehow necessary or a part of our lives.

54
00:05:27,640 --> 00:05:33,400
If you're meditating and especially if you're doing this a walking meditation or just

55
00:05:33,400 --> 00:05:38,120
being mindful in your daily life and doing all your activities with mindfulness then

56
00:05:38,120 --> 00:05:44,640
your body will be in such a state that the blood is able to flow and the digestive system

57
00:05:44,640 --> 00:05:50,840
is able to work properly and you'll find it incredible how healthy you are and how little

58
00:05:50,840 --> 00:05:56,480
sickness you have in the body that your body is fit I know monks that are 80, 90 years old

59
00:05:56,480 --> 00:06:02,800
and still quite fit and able because they're also good meditators and because of their

60
00:06:02,800 --> 00:06:11,120
meditation practice they're able to keep themselves in good shape or in good physical

61
00:06:11,120 --> 00:06:13,840
conditions.

62
00:06:13,840 --> 00:06:20,680
I think there are there's room for physical exercise if you're sick or if your body is

63
00:06:20,680 --> 00:06:26,560
out of its ordinary condition it might be a necessary part of your rehabilitation to

64
00:06:26,560 --> 00:06:31,880
undergo exercise even if a person is obese you know obesity can be a real problem because

65
00:06:31,880 --> 00:06:36,800
it can lead to high blood pressure it can lead to whatever high cholesterol I don't know

66
00:06:36,800 --> 00:06:44,720
what exactly there are a lot of sicknesses diabetes and so on comes from obesity.

67
00:06:44,720 --> 00:06:49,320
So in that case it might be necessary and advisable for you to undertake this sort of

68
00:06:49,320 --> 00:06:53,880
thing temporarily while you're overcoming your condition and at the same time undertaking

69
00:06:53,880 --> 00:06:59,560
a more reasonable lifestyle because ultimately that's what's the most important taking

70
00:06:59,560 --> 00:07:04,520
care of your body means not putting things into it that are going to pollute it that are

71
00:07:04,520 --> 00:07:10,040
going to make it unhealthy because it's like a car you have to be careful the gas and

72
00:07:10,040 --> 00:07:17,640
you have to make sure you have high quality gasoline and you know you have engine oil

73
00:07:17,640 --> 00:07:23,400
all the time and so on and that you take care of it taking care of your body is far more

74
00:07:23,400 --> 00:07:29,440
important than something like exercise and in fact exercise can have the effect of weakening

75
00:07:29,440 --> 00:07:35,960
the body or stressing the body in some sense I mean people say that it's good for cardiovascular

76
00:07:35,960 --> 00:07:44,680
health if you do jogging or so on and I don't know if I would if I would exactly dispute

77
00:07:44,680 --> 00:07:48,800
that but I do know that a person in meditate doesn't have a problem with cardiovascular

78
00:07:48,800 --> 00:07:53,680
health doesn't have a problem with digestive health doesn't have a problem with obesity

79
00:07:53,680 --> 00:07:58,360
and so on if they're undertaking meditation in the meditative lifestyle I mean we can

80
00:07:58,360 --> 00:08:03,600
all eat most of us can eat a lot less I can because I eat very little as it is I eat

81
00:08:03,600 --> 00:08:09,920
just enough to live and I've come to realize that that's really a proper way to live

82
00:08:09,920 --> 00:08:14,920
it is just enough that if I didn't eat as much I would it would be difficult for me to

83
00:08:14,920 --> 00:08:19,560
walk up the stairs every day but if I eat as much then okay I can walk up the stairs

84
00:08:19,560 --> 00:08:25,680
and we can walk around and I can have energy to teach and energy to do this and that

85
00:08:25,680 --> 00:08:33,960
and energy to do walking meditation and that's enough for me if I eat more then I become

86
00:08:33,960 --> 00:08:41,640
actually quite lethargic and you feel unhealthy having gotten used to in this lifestyle

87
00:08:41,640 --> 00:08:50,040
of eating just enough to survive and one final note is that in regards to this idea that

88
00:08:50,040 --> 00:08:57,680
certain exercises might be important to maintain a certain level of physical health is

89
00:08:57,680 --> 00:09:02,800
that I wouldn't really take that as a very good excuse I mean I would say in that case

90
00:09:02,800 --> 00:09:08,040
if people thought that somehow their health was going to be affected if they didn't

91
00:09:08,040 --> 00:09:14,400
exercise their diet was okay but they still felt that cardiovascular health or this

92
00:09:14,400 --> 00:09:24,560
other some kind of system in the body would be benefited by exercise of some sort I would

93
00:09:24,560 --> 00:09:30,400
give the same answers I give the people who ask whether yoga is a good idea stretching

94
00:09:30,400 --> 00:09:35,120
is a good idea some people who say yoga is a spiritual practice and then I've given

95
00:09:35,120 --> 00:09:39,200
an answer that if it's a spiritual practice which it should be considered then you're

96
00:09:39,200 --> 00:09:43,120
following that spiritual path and I don't think it's the same spiritual path as the Buddha

97
00:09:43,120 --> 00:09:49,800
taught but if you're just undertaking yoga for the purpose of stretching then what you're

98
00:09:49,800 --> 00:09:55,080
actually getting involved in is this attachment to the body which I think is part of

99
00:09:55,080 --> 00:10:03,360
this question in regards to exercising is that if it becomes an important part of your

100
00:10:03,360 --> 00:10:08,440
life that your body should be in a healthy condition and so therefore you go out of your

101
00:10:08,440 --> 00:10:14,800
way to do things simply for the purpose of keeping the body in a heightened state of

102
00:10:14,800 --> 00:10:20,240
health or some perfectly healthy state then you're really missing a part of the Buddha's

103
00:10:20,240 --> 00:10:27,320
teaching that is the physical body is not going to last forever and our attachment to

104
00:10:27,320 --> 00:10:30,720
the physical body is only going to cause a suffering because eventually it's going to

105
00:10:30,720 --> 00:10:34,960
break and it's going to fail and break down eventually we're going to have to let it go

106
00:10:34,960 --> 00:10:40,960
so if it comes to just doing the wise things that keep your body healthy like eating

107
00:10:40,960 --> 00:10:47,080
right and doing making movements like doing the walking meditation not simply sitting

108
00:10:47,080 --> 00:10:53,600
and lying down all day but actually walking doing some simple walking to keep the body

109
00:10:53,600 --> 00:11:00,760
moving then there's really no problem when it comes to doing something like intentional

110
00:11:00,760 --> 00:11:09,920
stretching like yoga and stretching yourself out then it seems to me more in the line

111
00:11:09,920 --> 00:11:16,640
of the non-acceptance of the current state of the body where if you have aches and pains

112
00:11:16,640 --> 00:11:24,200
instead of coming to accept and bear with and eventually really overcome these states

113
00:11:24,200 --> 00:11:31,120
you try to work your way out of them you try to get yourself into a state where those

114
00:11:31,120 --> 00:11:35,400
experiences don't arise and same goes with someone who wants to do jogging because they

115
00:11:35,400 --> 00:11:40,640
say I feel great after I'm going to feel great when I build up my muscles and I feel

116
00:11:40,640 --> 00:11:48,520
great and so on and you list this freedom from any kind of sickness another example is

117
00:11:48,520 --> 00:11:54,400
people who instead of just eating ordinary food they have very special diets because

118
00:11:54,400 --> 00:12:00,440
they know this is good for you and I know quite a few people like this who you know they

119
00:12:00,440 --> 00:12:06,240
take have known people who have taken these very special diets and then a couple of them

120
00:12:06,240 --> 00:12:12,920
died of cancer three people I know that all died of cancer so in the end it wasn't really

121
00:12:12,920 --> 00:12:17,360
of any benefit to them at all I mean you could say in some sense it gave them pleasure

122
00:12:17,360 --> 00:12:25,280
and it gave them sense of health and well being but in the end that only became a disadvantage

123
00:12:25,280 --> 00:12:31,760
to them because when they die when they're dying of cancer they have to see that the

124
00:12:31,760 --> 00:12:37,000
opposite effect regardless of what they do their bodies deteriorating and that's really

125
00:12:37,000 --> 00:12:44,560
the nature of reality so we should not be overly concerned with our diet we overly concerned

126
00:12:44,560 --> 00:12:49,880
with our health we should try to minimize the impact that these things have on our lives

127
00:12:49,880 --> 00:12:59,240
not eating too much not getting working too much working out really at all unless it's

128
00:12:59,240 --> 00:13:05,760
necessary for your job and of course a lot of this is is more inclined towards the lifestyle

129
00:13:05,760 --> 00:13:10,120
of monk if you're living in in your life I mean I'm not saying it's unethical to do

130
00:13:10,120 --> 00:13:15,960
act as an exercise I'm just saying my advice as a monk living in the forest in the cave

131
00:13:15,960 --> 00:13:24,480
is that it's not really an important part of my life and if we're looking to go further

132
00:13:24,480 --> 00:13:28,280
on this path we really should work towards giving it up and giving up all the things

133
00:13:28,280 --> 00:13:37,080
that surround exercise and physical health and so on I've often mentioned just as a final

134
00:13:37,080 --> 00:13:43,280
parting note for thought that it would be really interesting I think to together some sort

135
00:13:43,280 --> 00:13:50,280
of sickness I think even now that if I had cancer that would be a really unique experience

136
00:13:50,280 --> 00:13:54,800
to have people who are always trying to avoid these things are really missing the point

137
00:13:54,800 --> 00:14:00,800
and missing out on something quite incredible the experience of to die of cancer would

138
00:14:00,800 --> 00:14:06,840
I think an incredible chance and opportunity for us to come to understand the nature

139
00:14:06,840 --> 00:14:12,160
of reality that it's not what we think is it's not this wonderful body and how we can

140
00:14:12,160 --> 00:14:20,720
make it strong and so on it's actually quite fluid and uncertain and changing and it's

141
00:14:20,720 --> 00:14:28,000
quite dynamic you know you could be dead tomorrow it's not certain so if you're not able

142
00:14:28,000 --> 00:14:32,560
to experience the whole range of reality the whole range of experience then you're really

143
00:14:32,560 --> 00:14:37,680
going to get stuck and fall into suffering we should try to condition ourselves out

144
00:14:37,680 --> 00:14:45,320
of this or take us out of this conditioning so that we are in a condition to accept the

145
00:14:45,320 --> 00:14:49,760
whole range of experience which includes sickness which includes all the age which includes

146
00:14:49,760 --> 00:14:55,240
death and if we can do that then you know where could we be born where could we arise

147
00:14:55,240 --> 00:14:59,240
where could we go what could we meet with that would ever give us problems that would

148
00:14:59,240 --> 00:15:04,320
ever cause us stress and suffering if we can just come to accept things as they are

149
00:15:04,320 --> 00:15:13,400
and to interact with reality rather than react to it and to be then it's really not

150
00:15:13,400 --> 00:15:20,280
much that can ever cause us any sort of difficulty or suffering so hope that helps you know

151
00:15:20,280 --> 00:15:25,680
as usual I know there are going to be people who are disagreeing and get off an arms about

152
00:15:25,680 --> 00:15:31,160
this bit of offense to each their own this is my ask a monk so you've asked me this is

153
00:15:31,160 --> 00:15:48,440
my answer for the best.

