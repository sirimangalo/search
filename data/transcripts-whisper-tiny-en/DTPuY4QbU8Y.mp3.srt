1
00:00:00,000 --> 00:00:05,500
Okay, welcome everyone to our study of the Dhamapada.

2
00:00:05,500 --> 00:00:13,500
Today we will be continuing with verse number 42, which causes follows.

3
00:00:13,500 --> 00:00:20,500
Disou, Disang, yang, tang, gaiirah, wai-bi-wa-pa-nawirinam.

4
00:00:20,500 --> 00:00:28,500
Mih-cha-pa-ni-hi-tang-jitam, papi-o-nam-tum-kari.

5
00:00:28,500 --> 00:00:35,500
Disou, Disou means an enemy.

6
00:00:35,500 --> 00:00:40,500
Disou, Disou, Disou, disang, yang, tang, gaiirah, wai-a-nam-i-kari.

7
00:00:40,500 --> 00:00:43,500
One enemy could do to another.

8
00:00:43,500 --> 00:00:51,500
Wai-ri-wa-pa-nawirinam or one who is vengeful towards another.

9
00:00:51,500 --> 00:01:02,500
Mih-cha-pa-ni-hi-tum, the ill-directed mind or the mind that is set wrongly.

10
00:01:02,500 --> 00:01:09,500
Papi-o-nang-tum-kari is of greater evil.

11
00:01:09,500 --> 00:01:16,500
Does greater evil than that?

12
00:01:16,500 --> 00:01:27,500
It's a pretty powerful words where the meaning is what an enemy could do to an enemy or someone who is seeking vengeance.

13
00:01:27,500 --> 00:01:34,500
Anyone who could ever seek vengeance upon someone else, wreak vengeance upon another person.

14
00:01:34,500 --> 00:01:45,500
The Buddha said, it's a pretty strong word, that it's not possible basically for them to do as much damage as the ill-directed mind.

15
00:01:45,500 --> 00:01:49,500
It's worth listening to know.

16
00:01:49,500 --> 00:01:51,500
But first, we have a story.

17
00:01:51,500 --> 00:01:55,500
It's not much of a story, this story is quite a short one.

18
00:01:55,500 --> 00:02:08,500
The story goes that Anatapindika, this great, latest apple of the Buddha who donated so much and donated to one of this Buddhist monastery for the Buddha.

19
00:02:08,500 --> 00:02:21,500
And a cattle herder, as I know you call it, the person who looks after his cows.

20
00:02:21,500 --> 00:02:33,500
And this cattle herder, he from time to time would go with Anatapindika to listen to the Buddha, teach.

21
00:02:33,500 --> 00:02:50,500
He gained great faith in the Buddha, and so he invited the Buddha to come to his house to receive charity or receive some kind of gift from him.

22
00:02:50,500 --> 00:02:58,500
He thought that, wow, this is someone worth supporting, so he decided he wanted to provide some requisites to the Buddha.

23
00:02:58,500 --> 00:03:09,500
And the Buddha's seeing that there was this guy had some strong good qualities in him and the potential to understand the dhamma.

24
00:03:09,500 --> 00:03:19,500
Except of the invitation and walked all the way to this man's cattle herder's house.

25
00:03:19,500 --> 00:03:42,500
And for seven days, as I understand for seven days, he spent, he went every day to receive the five products of a cow, so he got milk and butter and cheese and all of these good things that come from the cows.

26
00:03:42,500 --> 00:03:50,500
So he had these cows that he looked after, and he was able to provide the Buddha with dairy products.

27
00:03:50,500 --> 00:03:55,500
And the story goes that he offered these to the Buddha seven days in the row.

28
00:03:55,500 --> 00:04:04,500
And on the seventh day, he was taking the Buddha back to, invited the Buddha for seven days.

29
00:04:04,500 --> 00:04:10,500
On the seventh day, he was going to follow the Buddha back to Jetahana, and he carried the Buddha's ball.

30
00:04:10,500 --> 00:04:18,500
Now, they got halfway through the forest, on the way back to Sawati, I guess.

31
00:04:18,500 --> 00:04:26,500
And suddenly, the Buddha turned around and received his ball from Nanda, Nanda was his name.

32
00:04:26,500 --> 00:04:30,500
His Gopalanda, the cattle herder.

33
00:04:30,500 --> 00:04:36,500
And he received his ball from him and said, Nanda, please turn back, don't come any further.

34
00:04:36,500 --> 00:04:39,500
We'll go by ourselves.

35
00:04:39,500 --> 00:04:46,500
And the Buddha turned it and walked away, and Nanda turned around to go home.

36
00:04:46,500 --> 00:04:51,500
And on his way home, some of the monks were coming later.

37
00:04:51,500 --> 00:04:55,500
They had left the house later, I guess, after the Buddha.

38
00:04:55,500 --> 00:05:02,500
And noticed that what happened was Nanda turned around, and as he was walking back to the forest,

39
00:05:02,500 --> 00:05:11,500
he ran into a hunter, there was a hunter who saw him going through the forest, and thought he was a deer or something,

40
00:05:11,500 --> 00:05:16,500
and shot him and killed him, shot him with a bone arrow and killed him.

41
00:05:16,500 --> 00:05:18,500
And some of the monks noticed this.

42
00:05:18,500 --> 00:05:23,500
The hunter realized he was wrong and, I guess, just ran away.

43
00:05:23,500 --> 00:05:31,500
But some of the monks saw this, that he was a guy who had done so much good deeds.

44
00:05:31,500 --> 00:05:35,500
And it's almost as though the Buddha knew what was going to happen.

45
00:05:35,500 --> 00:05:38,500
And it really seemed strange.

46
00:05:38,500 --> 00:05:43,500
The reason the story is so well remembered, and it's actually in the commentary,

47
00:05:43,500 --> 00:05:46,500
is because it's a curious story.

48
00:05:46,500 --> 00:05:51,500
Here we have someone that the Buddha spent seven days with.

49
00:05:51,500 --> 00:05:55,500
And it's almost as though the Buddha knew.

50
00:05:55,500 --> 00:06:00,500
And of course, there was this idea going around that the Buddha was omniscient.

51
00:06:00,500 --> 00:06:05,500
So he wouldn't have sent Nanda back if he didn't know what was going to happen.

52
00:06:05,500 --> 00:06:13,500
And yet he purposefully sent Nanda back, kind of like as though he knew that he was going to get shot by the center.

53
00:06:13,500 --> 00:06:16,500
That's just the curious part of the story.

54
00:06:16,500 --> 00:06:19,500
And what leads to this verse.

55
00:06:19,500 --> 00:06:24,500
And so the monks are kind of like, and they heard that the Buddha had sent them back, and they went,

56
00:06:24,500 --> 00:06:35,500
and they're kind of like, I don't know how to approach this, but you sent Lord Buddha, you sent Nanda back.

57
00:06:35,500 --> 00:06:41,500
Knowing that, did you know that this was going to happen?

58
00:06:41,500 --> 00:06:45,500
No, if you hadn't have sent him back, he wouldn't have gotten shot by the hunter.

59
00:06:45,500 --> 00:06:47,500
So what's up?

60
00:06:47,500 --> 00:06:51,500
And the Buddha said,

61
00:06:51,500 --> 00:06:56,500
if I hadn't, whether I told him to go or didn't tell him to go,

62
00:06:56,500 --> 00:07:02,500
whether he had gone north, east, west, and any of the eight, the four directions or the four minor directions,

63
00:07:02,500 --> 00:07:15,500
wherever he had gone, there's no way he could have escaped from the effects of his bad karma.

64
00:07:15,500 --> 00:07:22,500
And then the Buddha taught this verse.

65
00:07:22,500 --> 00:07:27,500
And as he taught elsewhere, you can't escape this karma.

66
00:07:27,500 --> 00:07:40,500
The point being that, you don't, the blame that we might place on an enemy or on someone seeking vengeance against that

67
00:07:40,500 --> 00:07:52,500
is much better placed or is properly placed on our own mind, which has caused us to arise in such a situation,

68
00:07:52,500 --> 00:07:54,500
causes us to give rise to such a situation.

69
00:07:54,500 --> 00:08:04,500
So it's really an interesting story because it requires,

70
00:08:04,500 --> 00:08:12,500
for understanding it requires a sort of a profound understanding of karma,

71
00:08:12,500 --> 00:08:18,500
not just on a moment to moment level, but you have to somehow see that there's a relationship between this hunter,

72
00:08:18,500 --> 00:08:23,500
killing this cattle hunter,

73
00:08:23,500 --> 00:08:31,500
and something that the cattle hunter has, I guess, done in the past probably to this hunter.

74
00:08:31,500 --> 00:08:35,500
And the even more curious thing about this is we don't know what it was,

75
00:08:35,500 --> 00:08:40,500
because as with most of the stories, then the Buddha says, in the past,

76
00:08:40,500 --> 00:08:45,500
this man did this and this and this, but the curious note that we have is at the end of the story,

77
00:08:45,500 --> 00:08:50,500
the commentary says, but the monks never asked the Buddha what this guy had done,

78
00:08:50,500 --> 00:08:54,500
so we don't know what the karma was.

79
00:08:54,500 --> 00:08:59,500
It's actually a unique story in that way because normally the monks go up to the Buddha and ask him,

80
00:08:59,500 --> 00:09:03,500
what did Nanda do, what did Nanda go Pala do to deserve that?

81
00:09:03,500 --> 00:09:06,500
In this case, they didn't.

82
00:09:06,500 --> 00:09:09,500
So first of all, it talks about karma.

83
00:09:09,500 --> 00:09:17,500
And it's a sort of an example, a strong example of how this verse comes into play,

84
00:09:17,500 --> 00:09:20,500
how this verse comes into play, how this verse,

85
00:09:20,500 --> 00:09:26,500
or the truth of this verse, how this verse has meaning.

86
00:09:26,500 --> 00:09:34,500
Because the point being that not only will your ill-directed mind hurt you

87
00:09:34,500 --> 00:09:37,500
on a momentary level, like if you get angry, you're in pain,

88
00:09:37,500 --> 00:09:41,500
or if you cling to things, you're stressed out about them.

89
00:09:41,500 --> 00:09:47,500
But it has profound reaching effects that can lead over into the next life,

90
00:09:47,500 --> 00:09:51,500
and can chase you throughout your journey and some sorrow,

91
00:09:51,500 --> 00:09:54,500
because of how they affect the mind.

92
00:09:54,500 --> 00:10:07,500
So it actually, you know, you would say maybe requires a little bit of faith,

93
00:10:07,500 --> 00:10:14,500
and people will start to question this idea of karma and say,

94
00:10:14,500 --> 00:10:21,500
here we're entering into a realm that is outside of the core of the Buddha's teaching,

95
00:10:21,500 --> 00:10:32,500
because it's starting to deal with faith and belief in some sort of magical,

96
00:10:32,500 --> 00:10:36,500
karmic potency of acts that when you kill someone,

97
00:10:36,500 --> 00:10:42,500
you magically get punished for it or that the things that we have done

98
00:10:42,500 --> 00:10:48,500
have some connection, unseen connection to things in the past.

99
00:10:48,500 --> 00:10:54,500
All that really shows, if you understand how Buddhism, how karma works,

100
00:10:54,500 --> 00:10:57,500
how rebirth works, all that really shows is they profound

101
00:10:57,500 --> 00:11:04,500
and intrinsic connection between moments of consciousness,

102
00:11:04,500 --> 00:11:11,500
between our acts and the world around us.

103
00:11:11,500 --> 00:11:15,500
So, like they'll say about the weather,

104
00:11:15,500 --> 00:11:19,500
they've come to realize it's about the weather that how small things in one part of the world

105
00:11:19,500 --> 00:11:22,500
can have a profound impact on other parts of the world.

106
00:11:22,500 --> 00:11:25,500
They have this saying which is probably not true

107
00:11:25,500 --> 00:11:28,500
that if a butterfly flaps its wings in China,

108
00:11:28,500 --> 00:11:30,500
there's an earthquake in America or something.

109
00:11:30,500 --> 00:11:33,500
There's a tornado in America.

110
00:11:33,500 --> 00:11:40,500
The point being that it's very easy for things to change,

111
00:11:40,500 --> 00:11:48,500
something very small or very specific can have an effect on the long term.

112
00:11:48,500 --> 00:11:51,500
It's not difficult to understand really at all

113
00:11:51,500 --> 00:11:55,500
when you think about how a act affects your mind

114
00:11:55,500 --> 00:11:59,500
and how your mind affects your future.

115
00:11:59,500 --> 00:12:02,500
So, if you have done bad deeds in the past,

116
00:12:02,500 --> 00:12:07,500
if you've heard other people or if you've clung to,

117
00:12:07,500 --> 00:12:13,500
if you're clinging to things, it's going to change your whole attitude towards life.

118
00:12:13,500 --> 00:12:15,500
It's going to affect your mind.

119
00:12:15,500 --> 00:12:18,500
It's something you'll think about on a daily basis.

120
00:12:18,500 --> 00:12:21,500
And the Buddha said, whatever you think about,

121
00:12:21,500 --> 00:12:23,500
that's what your mind inclines towards.

122
00:12:23,500 --> 00:12:25,500
It's a very obvious sort of teaching.

123
00:12:25,500 --> 00:12:29,500
Something we don't think about because we don't really understand karma.

124
00:12:29,500 --> 00:12:33,500
We don't really get that our thoughts affect our lives.

125
00:12:33,500 --> 00:12:37,500
And yet, they absolutely do.

126
00:12:37,500 --> 00:12:39,500
When we're stressed about something,

127
00:12:39,500 --> 00:12:42,500
it changes all of our interactions with other people

128
00:12:42,500 --> 00:12:45,500
throughout the life with the world around us throughout our life.

129
00:12:45,500 --> 00:12:48,500
It changes even the direction of our lives.

130
00:12:48,500 --> 00:12:52,500
People who have gone through traumatic experiences.

131
00:12:52,500 --> 00:12:54,500
Of course, it changes their whole life.

132
00:12:54,500 --> 00:12:59,500
It changes their outlook. It changes their potential.

133
00:12:59,500 --> 00:13:05,500
Their possibilities in their lives.

134
00:13:05,500 --> 00:13:11,500
The things that we do, the things that we engage in change us.

135
00:13:11,500 --> 00:13:14,500
People who are addicted to things have to spend money

136
00:13:14,500 --> 00:13:17,500
and put resources into the things that they want.

137
00:13:17,500 --> 00:13:19,500
And that they might have put elsewhere.

138
00:13:19,500 --> 00:13:22,500
They have to put time, dedicate time to their addiction.

139
00:13:22,500 --> 00:13:29,500
And energy and brain cells and thoughts and so on.

140
00:13:29,500 --> 00:13:37,500
And so it actually gets, the effect can actually get bigger over the long term.

141
00:13:37,500 --> 00:13:40,500
So that the effect that karma has in the short term

142
00:13:40,500 --> 00:13:46,500
might be minuscule compared to the effect that it has later on in life

143
00:13:46,500 --> 00:13:48,500
or even into the next life.

144
00:13:48,500 --> 00:13:54,500
Because of course, as Buddhists, we're not thinking of the next life as something separate from this life.

145
00:13:54,500 --> 00:13:56,500
It's a continuation.

146
00:13:56,500 --> 00:14:00,500
There's a shift at the moment in death,

147
00:14:00,500 --> 00:14:05,500
but only because we've entered into this state of being born and died.

148
00:14:05,500 --> 00:14:15,500
This human state that is very coarse and requires or associates with a physical body.

149
00:14:15,500 --> 00:14:18,500
Actually, at the moment of death, nothing happens.

150
00:14:18,500 --> 00:14:20,500
Life just continues.

151
00:14:20,500 --> 00:14:26,500
And our mind just continues.

152
00:14:26,500 --> 00:14:29,500
So that's the part that has to do with karma.

153
00:14:29,500 --> 00:14:37,500
When we're talking about the mind and the potency of the mind,

154
00:14:37,500 --> 00:14:42,500
why is the mind so potent in creating karma?

155
00:14:42,500 --> 00:14:47,500
Once we accept the fact that our bad deeds have an effect,

156
00:14:47,500 --> 00:14:58,500
why would it be that the deeds of others are less potent than our own minds?

157
00:14:58,500 --> 00:15:04,500
And further, why is it the mind that the Buddha is focusing on and not the deeds?

158
00:15:04,500 --> 00:15:10,500
So people are still asking me,

159
00:15:10,500 --> 00:15:15,500
why is it that when you do something, it doesn't have karmic potency?

160
00:15:15,500 --> 00:15:18,500
We have this idea that karma is some kind of magic,

161
00:15:18,500 --> 00:15:21,500
because we've been brought up generally in the West,

162
00:15:21,500 --> 00:15:24,500
and we've been brought up to think in terms of God,

163
00:15:24,500 --> 00:15:30,500
to think in terms of nature as having some kind of watchful

164
00:15:30,500 --> 00:15:44,500
or some kind of power to affect and to intervene with our lives.

165
00:15:44,500 --> 00:15:48,500
So we think when you step on an end, bad, you've done a bad deed,

166
00:15:48,500 --> 00:15:52,500
because we're used to sin being something that God decides.

167
00:15:52,500 --> 00:15:54,500
Is it sinful or is it right?

168
00:15:54,500 --> 00:16:00,500
And he's watching you like Santa Claus.

169
00:16:00,500 --> 00:16:03,500
But karma is absolutely not that.

170
00:16:03,500 --> 00:16:07,500
Karma is the effect that something has on your mind,

171
00:16:07,500 --> 00:16:12,500
or the effect that something has on the choices you make,

172
00:16:12,500 --> 00:16:16,500
and on your state of mind, into the future.

173
00:16:16,500 --> 00:16:19,500
And that which has the biggest, the most profound effect,

174
00:16:19,500 --> 00:16:25,500
or really the only effect is your own mind.

175
00:16:25,500 --> 00:16:29,500
Another person, the harm that an enemy can do to you,

176
00:16:29,500 --> 00:16:36,500
or someone who wants to take out their vengeance on you,

177
00:16:36,500 --> 00:16:41,500
is actually absolutely zero.

178
00:16:41,500 --> 00:16:47,500
The harm that they can perform directly upon you is none.

179
00:16:47,500 --> 00:16:50,500
It's not even little. It's not a little.

180
00:16:50,500 --> 00:16:53,500
It's not something you can mitigate. It's zero.

181
00:16:53,500 --> 00:16:55,500
No one can hurt you.

182
00:16:55,500 --> 00:16:59,500
No one can cause harm to another.

183
00:16:59,500 --> 00:17:02,500
So what the Buddha says is actually an understatement of the fact.

184
00:17:02,500 --> 00:17:08,500
There's no comparison between the harm that an enemy can do to you,

185
00:17:08,500 --> 00:17:13,500
and the harm that you can do to yourself.

186
00:17:13,500 --> 00:17:22,500
And this is due to the theory or the Buddhist theory,

187
00:17:22,500 --> 00:17:26,500
which is accepted and not accepted by many of you,

188
00:17:26,500 --> 00:17:33,500
that the mind is apart from the body,

189
00:17:33,500 --> 00:17:36,500
that it's not a physical process.

190
00:17:36,500 --> 00:17:42,500
You're not locked into your environment.

191
00:17:42,500 --> 00:17:46,500
We react to the mind reacts to the environment.

192
00:17:46,500 --> 00:17:52,500
It reacts either in a positive or a negative or a neutral way.

193
00:17:52,500 --> 00:17:55,500
So when we experience something painful,

194
00:17:55,500 --> 00:17:58,500
we generally tend to react in a negative way.

195
00:17:58,500 --> 00:18:01,500
When we experience something pleasant, we react in a positive way.

196
00:18:01,500 --> 00:18:05,500
The body reacts, but the mind also reacts.

197
00:18:05,500 --> 00:18:13,500
And the key to understanding and to practicing Buddhism

198
00:18:13,500 --> 00:18:16,500
is realizing that these two things are separate.

199
00:18:16,500 --> 00:18:21,500
The mind need not react in a negative or a positive way.

200
00:18:21,500 --> 00:18:26,500
The mind can react in an objective way to reality.

201
00:18:26,500 --> 00:18:31,500
If the mind is, if and when the mind is aware of the pain,

202
00:18:31,500 --> 00:18:36,500
that's just pain, then the pain will not harm the mind.

203
00:18:36,500 --> 00:18:40,500
If when someone yells at us or scolds us,

204
00:18:40,500 --> 00:18:43,500
and we see it simply as hearing,

205
00:18:43,500 --> 00:18:47,500
we understand it to be sound arising in the mind,

206
00:18:47,500 --> 00:18:51,500
rising at the ear.

207
00:18:51,500 --> 00:19:01,500
Then we and observation proves this.

208
00:19:01,500 --> 00:19:03,500
If you test this hypothesis,

209
00:19:03,500 --> 00:19:06,500
if you test it out for yourself, you can see this for yourself.

210
00:19:06,500 --> 00:19:09,500
If you listen to the sound of my voice,

211
00:19:09,500 --> 00:19:15,500
and just take it for sound, hearing, hearing.

212
00:19:15,500 --> 00:19:21,500
When you find that any other judgments or thoughts you might have

213
00:19:21,500 --> 00:19:26,500
about what I'm saying disappear, any boredom or aversion

214
00:19:26,500 --> 00:19:31,500
to what I'm saying or any attachment or desire or appreciation

215
00:19:31,500 --> 00:19:34,500
of what I'm saying melts away.

216
00:19:34,500 --> 00:19:36,500
And you simply know it for sound.

217
00:19:36,500 --> 00:19:41,500
And you find that suddenly you're in the realm of peace and of

218
00:19:41,500 --> 00:19:47,500
contentment. You're unshaken by what the person is saying.

219
00:19:47,500 --> 00:19:49,500
If they're saying something nasty or unpleasant,

220
00:19:49,500 --> 00:19:57,500
even if they're hitting you or beating you.

221
00:19:57,500 --> 00:20:05,500
It's actually the only way to be free from suffering is to

222
00:20:05,500 --> 00:20:09,500
free yourself from within, because you can't be free from

223
00:20:09,500 --> 00:20:14,500
enemies, you can't be free from harsh speech.

224
00:20:14,500 --> 00:20:19,500
You can't be free from the cold and heat and hunger and thirst

225
00:20:19,500 --> 00:20:21,500
and sickness and all the age and death.

226
00:20:21,500 --> 00:20:24,500
We can't be free from these things.

227
00:20:24,500 --> 00:20:29,500
So there's only two ways to be free from suffering.

228
00:20:29,500 --> 00:20:33,500
It's not have any of these things which are ubiquitous,

229
00:20:33,500 --> 00:20:37,500
which are intrinsic part of life or overcome them.

230
00:20:37,500 --> 00:20:42,500
Be free from free yourself, free your mind from them.

231
00:20:42,500 --> 00:20:44,500
And this is what the Buddha is referring to.

232
00:20:44,500 --> 00:20:47,500
This is the key theory that we have in Buddhism.

233
00:20:47,500 --> 00:20:52,500
If you need to accept this in order to practice Buddhism

234
00:20:52,500 --> 00:20:59,500
correctly, it's key to understand that you can change the way

235
00:20:59,500 --> 00:21:01,500
you react to reality.

236
00:21:01,500 --> 00:21:03,500
You need not react.

237
00:21:03,500 --> 00:21:06,500
You can interact objectively with reality.

238
00:21:06,500 --> 00:21:11,500
This is why the Buddha said the mind that is set wrongly.

239
00:21:11,500 --> 00:21:18,500
The mind that is set in a bad way is of much greater

240
00:21:18,500 --> 00:21:24,500
or incomparably greater harm to the individual than anyone else

241
00:21:24,500 --> 00:21:27,500
could be than an enemy could be.

242
00:21:27,500 --> 00:21:35,500
And furthermore, what this means is this means actually more

243
00:21:35,500 --> 00:21:36,500
than just karma.

244
00:21:36,500 --> 00:21:38,500
What the Buddha is talking about here is not just someone

245
00:21:38,500 --> 00:21:39,500
doing a bad deed.

246
00:21:39,500 --> 00:21:43,500
What he's talking about is setting the mind in a bad way,

247
00:21:43,500 --> 00:21:47,500
which is actually even worse than doing a bad deed.

248
00:21:47,500 --> 00:21:50,500
So up until now we're just talking about bad karma.

249
00:21:50,500 --> 00:21:52,500
But even worse than bad karma.

250
00:21:52,500 --> 00:21:54,500
There's something even worse than that.

251
00:21:54,500 --> 00:21:57,500
If you kill someone, if you steal, if you do bad things,

252
00:21:57,500 --> 00:22:00,500
is setting your mind in a bad way.

253
00:22:00,500 --> 00:22:06,500
In a bad way.

254
00:22:06,500 --> 00:22:12,500
In a wrong way, it refers to a wrong view.

255
00:22:12,500 --> 00:22:14,500
So there are three kinds of wrong.

256
00:22:14,500 --> 00:22:19,500
One is wrong perception.

257
00:22:19,500 --> 00:22:23,500
And the third is wrong view.

258
00:22:23,500 --> 00:22:25,500
Wrong view is the worst.

259
00:22:25,500 --> 00:22:28,500
Wrong perception is the least serious.

260
00:22:28,500 --> 00:22:32,500
If someone says something to you and you get angry right away,

261
00:22:32,500 --> 00:22:33,500
that's wrong perception.

262
00:22:33,500 --> 00:22:35,500
You perceive it as bad.

263
00:22:35,500 --> 00:22:37,500
But when you start thinking about it,

264
00:22:37,500 --> 00:22:38,500
that starts to get worse.

265
00:22:38,500 --> 00:22:40,500
You think, oh, what did that person do?

266
00:22:40,500 --> 00:22:42,500
That was really mean of them and so on.

267
00:22:42,500 --> 00:22:45,500
Really nasty of them.

268
00:22:45,500 --> 00:22:47,500
That's worse because then you start to think

269
00:22:47,500 --> 00:22:50,500
how you're going to hurt them and hurt them back and so on.

270
00:22:50,500 --> 00:22:54,500
But when you have the view, when you set yourself in the idea,

271
00:22:54,500 --> 00:22:55,500
that person is evil.

272
00:22:55,500 --> 00:22:57,500
I didn't deserve that.

273
00:22:57,500 --> 00:23:02,500
They deserve to be punished when you have the wrong views.

274
00:23:02,500 --> 00:23:05,500
Because we can all, even as Buddhists,

275
00:23:05,500 --> 00:23:07,500
we all have these first two.

276
00:23:07,500 --> 00:23:09,500
But we need not have the third.

277
00:23:09,500 --> 00:23:11,500
And as Buddhists, we generally don't.

278
00:23:11,500 --> 00:23:14,500
Because this is what we're taught.

279
00:23:14,500 --> 00:23:18,500
We're taught that no, it's not good for you to hurt other people.

280
00:23:18,500 --> 00:23:20,500
But if someone say,

281
00:23:20,500 --> 00:23:22,500
treat other people well,

282
00:23:22,500 --> 00:23:24,500
not because they deserve to be happy,

283
00:23:24,500 --> 00:23:27,500
but because you do.

284
00:23:27,500 --> 00:23:28,500
Which I think is very pertinent.

285
00:23:28,500 --> 00:23:30,500
I mean, of course, you can think they deserve to be happy,

286
00:23:30,500 --> 00:23:32,500
but that's not the real reason.

287
00:23:32,500 --> 00:23:34,500
The real reason is saying, oh, look,

288
00:23:34,500 --> 00:23:36,500
if you get angry at them,

289
00:23:36,500 --> 00:23:37,500
it's not good for you.

290
00:23:37,500 --> 00:23:40,500
You don't get anywhere by hurting other people.

291
00:23:40,500 --> 00:23:42,500
You don't help them, obviously.

292
00:23:42,500 --> 00:23:44,500
You don't help yourself.

293
00:23:44,500 --> 00:23:47,500
And it's pertinent because it's how the mind reacts.

294
00:23:47,500 --> 00:23:49,500
The mind is looking for happiness.

295
00:23:49,500 --> 00:23:52,500
It's not really looking for happiness in other people.

296
00:23:52,500 --> 00:23:55,500
We help other people because it makes us happy.

297
00:23:55,500 --> 00:24:03,500
If it didn't make us happy, it wouldn't excite the mind.

298
00:24:03,500 --> 00:24:08,500
The mind wouldn't be pleased to continue it.

299
00:24:08,500 --> 00:24:12,500
So if you want to really encourage your mind

300
00:24:12,500 --> 00:24:16,500
and really get your heart, the attention of your heart,

301
00:24:16,500 --> 00:24:18,500
you have to put it this way and say, look,

302
00:24:18,500 --> 00:24:21,500
let's not get angry because not because they don't deserve it

303
00:24:21,500 --> 00:24:23,500
because we don't deserve it.

304
00:24:23,500 --> 00:24:26,500
We don't deserve to be the software from this

305
00:24:26,500 --> 00:24:29,500
so that I stop our thoughts.

306
00:24:29,500 --> 00:24:32,500
Because we have this idea, because we have this understanding

307
00:24:32,500 --> 00:24:34,500
and this is the understanding we try to give to

308
00:24:34,500 --> 00:24:36,500
Buddhists and the meditators,

309
00:24:36,500 --> 00:24:39,500
we're free from this wrongly directed mind.

310
00:24:39,500 --> 00:24:42,500
Most of the time, still,

311
00:24:42,500 --> 00:24:45,500
we have to always be on guard.

312
00:24:45,500 --> 00:24:48,500
When we're in training, this is something important

313
00:24:48,500 --> 00:24:51,500
to understand. Guard against your mind,

314
00:24:51,500 --> 00:24:57,500
first of all, from having bad perception.

315
00:24:57,500 --> 00:25:00,500
At the very core, you're best off

316
00:25:00,500 --> 00:25:03,500
when you can just perceive things as they are

317
00:25:03,500 --> 00:25:06,500
and not misperceive things as good as bad

318
00:25:06,500 --> 00:25:11,500
as me, as mine, as right, as wrong.

319
00:25:11,500 --> 00:25:13,500
But even when you do, then be careful not to let it come

320
00:25:13,500 --> 00:25:15,500
into your thoughts.

321
00:25:15,500 --> 00:25:18,500
If you're aware of the anger, be aware of the wanting,

322
00:25:18,500 --> 00:25:22,500
be aware of the desire, don't let your mind start thinking

323
00:25:22,500 --> 00:25:24,500
about how you're going to get what you want,

324
00:25:24,500 --> 00:25:27,500
how you're going to chase away what you don't want.

325
00:25:27,500 --> 00:25:31,500
And if it does come to thoughts, try to be aware of the thoughts

326
00:25:31,500 --> 00:25:33,500
and don't let them become views.

327
00:25:33,500 --> 00:25:34,500
I think, yeah, I deserve that.

328
00:25:34,500 --> 00:25:37,500
That's right for me to get that.

329
00:25:37,500 --> 00:25:40,500
Or I don't deserve that.

330
00:25:40,500 --> 00:25:43,500
This is wrong in these, and I'm being unjustly treated

331
00:25:43,500 --> 00:25:45,500
and so on.

332
00:25:45,500 --> 00:25:46,500
Not because they don't deserve it,

333
00:25:46,500 --> 00:25:49,500
but because you don't deserve it.

334
00:25:49,500 --> 00:25:52,500
Because we don't want to suffer.

335
00:25:52,500 --> 00:25:55,500
So let's stop making ourselves separate.

336
00:25:55,500 --> 00:25:58,500
We have to see that these things are causing us suffering.

337
00:25:58,500 --> 00:26:00,500
And the worst is this third one.

338
00:26:00,500 --> 00:26:02,500
When you set your mind in the wrong way.

339
00:26:02,500 --> 00:26:04,500
This is what the Buddha is talking about.

340
00:26:04,500 --> 00:26:06,500
This is the key to this first.

341
00:26:06,500 --> 00:26:08,500
Something for us all to remember.

342
00:26:08,500 --> 00:26:11,500
Whenever you think your problems are other people

343
00:26:11,500 --> 00:26:13,500
in the world around you,

344
00:26:13,500 --> 00:26:15,500
never forget what the Buddha said.

345
00:26:15,500 --> 00:26:18,500
This is the only way to come to the Buddha.

346
00:26:18,500 --> 00:26:21,500
It's the only way to come to the Buddha.

347
00:26:21,500 --> 00:26:24,500
It's the only way to come to the Buddha.

348
00:26:24,500 --> 00:26:27,500
An enemy, as an enemy, would do to an enemy.

349
00:26:27,500 --> 00:26:32,500
Or one seeking vengeance would do to another.

350
00:26:32,500 --> 00:26:38,500
The wrongly directed mind is far, far.

351
00:26:38,500 --> 00:26:43,500
We'll do far, far greater evil than any of those.

352
00:26:43,500 --> 00:26:47,500
So that's the Dhamupanda verse for today.

353
00:26:47,500 --> 00:26:49,500
Thank you all for listening.

354
00:26:49,500 --> 00:27:12,500
Until the next time.

