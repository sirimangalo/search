1
00:00:00,000 --> 00:00:05,400
How do you find balance between loving kindness and compassion towards a

2
00:00:05,400 --> 00:00:13,880
spouse, children, and other close family and non-attachment and other desires

3
00:00:13,880 --> 00:00:18,240
that arise due to these sorts of people?

4
00:00:19,640 --> 00:00:25,240
That's not quite clear this question. What are you trying to find a balance

5
00:00:25,240 --> 00:00:36,240
between? I guess attachment and desires? Or how do you how do you how do you

6
00:00:36,240 --> 00:00:40,360
do in regards to all of these various states? Well compassion and loving

7
00:00:40,360 --> 00:00:44,440
kindness can go together. Compassion and loving kindness complement each other.

8
00:00:44,440 --> 00:00:53,160
Compassion is not wanting beings to suffer. Love, love is wanting them to be

9
00:00:53,160 --> 00:00:57,960
happy. So one is positive and the other is negative. One is desire for them to

10
00:00:57,960 --> 00:01:03,080
be free from suffering. The other is desire for them to be happy. Not exactly

11
00:01:03,080 --> 00:01:10,160
desire but the feeling of friendliness and non-cruelty. So being friendly in

12
00:01:10,160 --> 00:01:16,400
the sense of acting in such a way as to not bring, as to bring them happiness

13
00:01:16,400 --> 00:01:21,840
suffering and non-cruelty in the sense of acting in such a way as to not bring

14
00:01:21,840 --> 00:01:27,160
them suffering. So these two impulses complement each other and they are very

15
00:01:27,160 --> 00:01:33,280
important in family and so on. The less attachment you have to other people and I

16
00:01:33,280 --> 00:01:38,320
know it doesn't sound nice but the less attachment, but it happens to be true,

17
00:01:38,320 --> 00:01:44,120
that the less attachment you have towards people, the more peaceful your life

18
00:01:44,120 --> 00:01:51,240
will be. Your relationships will be. So the better. The less attachment you have to

19
00:01:51,240 --> 00:01:56,640
your family and loved ones, the better. Because attachment is wanting them to

20
00:01:56,640 --> 00:02:03,520
make you happy. Wanting to be in a certain way. Grieving when they are in a

21
00:02:03,520 --> 00:02:07,760
different way, if your family member is hurt or dies, it's really because it

22
00:02:07,760 --> 00:02:17,800
makes you sad. We don't like it that we suffer. It's our own attachment to them.

23
00:02:17,800 --> 00:02:21,720
It doesn't make it any better for them. It makes it better for them when you

24
00:02:21,720 --> 00:02:25,520
have compassion and you try to help them. So trying to help people is always a

25
00:02:25,520 --> 00:02:32,240
good thing. To an extent until it becomes an attachment and becomes

26
00:02:32,240 --> 00:02:42,760
actually wanting them to be in this way or that way. I guess the most important

27
00:02:42,760 --> 00:02:47,080
thing is to know that everyone has to find their own happiness and you can't

28
00:02:47,080 --> 00:02:51,520
bring true happiness to people. You can't truly free people from suffering just

29
00:02:51,520 --> 00:02:56,800
by alleviating their physical suffering or bringing them physical pleasure or

30
00:02:56,800 --> 00:03:02,160
happiness. This can only come from one's own practice and it can only come

31
00:03:02,160 --> 00:03:08,320
with an example of someone else practicing in that way. So the best thing that you

32
00:03:08,320 --> 00:03:22,680
can do for in a family situation and the best thing you can do, the best way for

33
00:03:22,680 --> 00:03:31,680
you to live even in the world is to be mindful and let go.

