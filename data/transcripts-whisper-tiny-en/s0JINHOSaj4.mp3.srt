1
00:00:00,000 --> 00:00:09,540
Okay, so this is part three of my series on how to meditate, and today we're going to

2
00:00:09,540 --> 00:00:13,700
look at walking meditation.

3
00:00:13,700 --> 00:00:18,880
Walking meditation is something that I think many people have never heard of, some people

4
00:00:18,880 --> 00:00:26,680
have heard of it, but discard it as a modern invention or something outside of a standard

5
00:00:26,680 --> 00:00:28,200
meditation practice.

6
00:00:28,200 --> 00:00:33,800
Now, here we're talking about a standard Buddhist practice, meditation practice, so we

7
00:00:33,800 --> 00:00:41,840
go by the example of the Lord Buddha, so it may be that another tradition's walking meditation

8
00:00:41,840 --> 00:00:42,840
is not standard.

9
00:00:42,840 --> 00:00:48,480
I wish you'll look at what is standard in the Buddhist tradition, because what I'm teaching

10
00:00:48,480 --> 00:00:50,040
is a Buddhist meditation.

11
00:00:50,040 --> 00:00:55,760
Now, the truth is that the Lord Buddha, from the time of his enlightenment on, practice

12
00:00:55,760 --> 00:01:04,280
walking meditation every day, as he was able, after he became enlightened, became a perfectly

13
00:01:04,280 --> 00:01:10,160
self enlightened Buddha, he walked, did walking meditation, they say, for seven days, non-self,

14
00:01:10,160 --> 00:01:12,560
true or not true.

15
00:01:12,560 --> 00:01:17,880
According to the records, he practiced walking meditation in alternation with sitting meditation

16
00:01:17,880 --> 00:01:24,320
and according to the records, he taught this to his students as well.

17
00:01:24,320 --> 00:01:29,080
The meditation which he recommended for his students was to do walking and sitting in alternation

18
00:01:29,080 --> 00:01:37,640
every day, and at night to sleep around four hours or so, and do lying meditation during

19
00:01:37,640 --> 00:01:42,160
the time, during those four hours, so it's trying to be mindful until the moment one

20
00:01:42,160 --> 00:01:43,160
one fell asleep.

21
00:01:43,160 --> 00:01:48,200
First, I thought I'd show you the technique for walking meditation, in the tradition

22
00:01:48,200 --> 00:01:52,080
which I follow, and for this we're going to have to stand up.

23
00:01:52,080 --> 00:01:56,720
So the first important thing is that the right hand, during the time we walk, the right

24
00:01:56,720 --> 00:02:05,120
hand is holding the left hand, either in front or in behind.

25
00:02:05,120 --> 00:02:10,840
I should be open and you should be looking at the floor in front of you, about two meters

26
00:02:10,840 --> 00:02:13,040
or six feet out from your feet.

27
00:02:13,040 --> 00:02:20,400
The feet should be close together, almost touching, and we start with the right foot first,

28
00:02:20,400 --> 00:02:29,080
move at an ordinary pace, with an ordinary step, and end up in front of the in line with

29
00:02:29,080 --> 00:02:48,800
the toes of the back foot.

30
00:02:48,800 --> 00:02:55,080
And the meditation is that as we move the foot, we become aware of the movement of the

31
00:02:55,080 --> 00:03:11,360
foot, and we say to ourselves, in our mind, left goes, right goes, left goes, that.

32
00:03:11,360 --> 00:03:15,040
And even though it's one movement, we have three words, so we can break this step up into

33
00:03:15,040 --> 00:03:16,040
three parts.

34
00:03:16,040 --> 00:03:22,500
And when we start to move, we say right, when the foot isn't going, we say goes, and

35
00:03:22,500 --> 00:03:26,920
when it's completely finished and stopped, at that moment we say that.

36
00:03:26,920 --> 00:03:31,840
For those people who are not comfortable with the words right goes, left goes, left goes,

37
00:03:31,840 --> 00:03:33,960
left goes, left goes, left goes, left goes, left goes, left goes, left goes.

38
00:03:33,960 --> 00:03:44,640
You can also say that being left, that being right, that being left and so on.

39
00:03:44,640 --> 00:03:50,120
And when we come to the end of our walking path, we simply bring the foot which is behind

40
00:03:50,120 --> 00:03:55,400
up to come in line with the foot which is in front and say to ourselves, as we move, stop

41
00:03:55,400 --> 00:04:01,080
being, stop being, stop being, until we completely stop.

42
00:04:01,080 --> 00:04:07,480
Once we're standing still, we say to ourselves, standing, standing, standing, like I'm

43
00:04:07,480 --> 00:04:10,480
aware of the fact that we're standing, and then we turn around, start with the right

44
00:04:10,480 --> 00:04:19,480
foot, lift it off the floor, and turn at 90 degrees, say to ourselves, turning, then

45
00:04:19,480 --> 00:04:26,800
we lift the left foot to come to stand beside the right foot, turning, and we do this process

46
00:04:26,800 --> 00:04:35,920
one more time, turning, turning, and then we're standing facing the opposite direction,

47
00:04:35,920 --> 00:04:42,320
at which point we say to ourselves, standing, standing, standing, and continue walking

48
00:04:42,320 --> 00:04:44,200
in the other direction.

49
00:04:44,200 --> 00:04:55,360
Right goes, left goes, left goes, left goes, left goes, left goes, left goes, left goes,

50
00:04:55,360 --> 00:04:58,800
left goes, left goes, left goes, left goes, and so on, until we come to the end of our walk

51
00:04:58,800 --> 00:05:04,160
again, and the same thing applies, we bring the back foot up to come in line with the right

52
00:05:04,160 --> 00:05:09,600
foot and say to ourselves, stop being, stop being, as we move the foot, we say to ourselves,

53
00:05:09,600 --> 00:05:17,880
stop being, stop being, stop being, we're standing still, we say standing, standing, standing,

54
00:05:17,880 --> 00:05:27,680
and then we're going to turn the foot, starting with the right foot, turning, turning,

55
00:05:27,680 --> 00:05:37,800
and then we're facing in the other direction, start again, standing, standing, and start

56
00:05:37,800 --> 00:05:49,840
to walk again, right goes, left goes, left and so on, so that is a brief demonstration

57
00:05:49,840 --> 00:05:55,960
of the practice of walking meditation at a basic level, so altogether now we've learned

58
00:05:55,960 --> 00:06:02,280
both walking and sitting meditation, the technique is to practice in the order of walking

59
00:06:02,280 --> 00:06:10,280
meditation first and followed by sitting meditation, and the amount of time that we practice

60
00:06:10,280 --> 00:06:16,040
should be equal, if we do walking meditation for five minutes, we should do sitting meditation

61
00:06:16,040 --> 00:06:22,120
also for five minutes, if we feel confident with more, we do 10 minutes walking, 10 minutes

62
00:06:22,120 --> 00:06:27,960
sitting, or 15 minutes walking, 15 minutes sitting, all the way up to 30 minutes or one

63
00:06:27,960 --> 00:06:30,280
hour of each.

64
00:06:30,280 --> 00:06:37,080
The reason why we practice walking meditation first is because, according to the Lord Buddha,

65
00:06:37,080 --> 00:06:42,760
there are five benefits of walking meditation first, that we're able to walk long distances

66
00:06:42,760 --> 00:06:46,880
and of course this is important in ancient times, but nowadays it can also be considered

67
00:06:46,880 --> 00:06:53,280
important in keeping fit and healthy and being in touch with the world around us, and

68
00:06:53,280 --> 00:06:59,840
that we do walking, that we take the time to walking from birth to, it gives us endurance

69
00:06:59,840 --> 00:07:07,280
to bear through things which are difficult to endure, a physical labor or things which

70
00:07:07,280 --> 00:07:13,920
are difficult to endure mentally, either way walking meditation has the ability to build

71
00:07:13,920 --> 00:07:20,080
endurance and patience inside of us, and with three, the food which we've eaten during

72
00:07:20,080 --> 00:07:27,240
the time when we're practicing walking meditation can be digested, it aids in our digestion

73
00:07:27,240 --> 00:07:36,480
of our food, so helping it to be used, helping it to support the body.

74
00:07:36,480 --> 00:07:43,680
Number four, sicknesses, which we have people who have sicknesses, people who can serve

75
00:07:43,680 --> 00:07:49,120
people who have any sort of physical sickness, this can be aided through the practice of

76
00:07:49,120 --> 00:07:53,560
walking meditation, and number five, the important one, and the reason why we practice

77
00:07:53,560 --> 00:08:00,440
walking meditation first is that walking meditation, which comes from walking meditation

78
00:08:00,440 --> 00:08:04,640
lasts a long time, this is according to the Lord Buddha, and we can see it play out

79
00:08:04,640 --> 00:08:10,440
in practice, that if we simply sit down to do sitting meditation, we find our minds

80
00:08:10,440 --> 00:08:18,280
either distracted, or we can find our minds fatigued, or lazy, and we find ourselves falling

81
00:08:18,280 --> 00:08:19,280
asleep in our meditation.

82
00:08:19,280 --> 00:08:24,240
When we do walking meditation first, it helps to prepare us for the sitting meditation,

83
00:08:24,240 --> 00:08:29,040
it can give us a well-balanced sitting practice.

84
00:08:29,040 --> 00:08:35,880
So, all together now, we've learned both walking meditation and sitting meditation, and

85
00:08:35,880 --> 00:08:43,040
we have the basic points, which we need to keep in mind, if you need more information

86
00:08:43,040 --> 00:08:48,280
or recap on those things, you can visit the website, the address which I give on YouTube,

87
00:08:48,280 --> 00:08:54,760
and I give on my web blog, and that's about it for now, I have probably one more video

88
00:08:54,760 --> 00:09:01,760
to give, and then I'll finish the series.

