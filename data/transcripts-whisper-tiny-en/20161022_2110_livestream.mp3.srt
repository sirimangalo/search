1
00:00:00,000 --> 00:00:07,600
There's many opportunities for getting off track unfortunately because as with

2
00:00:07,600 --> 00:00:16,120
everything as it grows it it rots not entirely and it's good in it power in it

3
00:00:16,120 --> 00:00:22,160
but there's also a lot of rot unfortunately. So here you don't have a lot of

4
00:00:22,160 --> 00:00:27,600
these problems but still if you want to optimize your practice there are

5
00:00:27,600 --> 00:00:30,920
certain things that you have to find to know about your practice. So what are

6
00:00:30,920 --> 00:00:37,920
in the here in the six finally we get to draw that introduction but it says

7
00:00:37,920 --> 00:00:49,240
chai mai be goi a bharyhani a dhammai di sisahali. Oh because I will teach you the

8
00:00:49,240 --> 00:01:03,240
six dhammas that lead you not to not to fade away not to decline not to get

9
00:01:03,240 --> 00:01:11,440
off track and get lost fall away from the dhamma. Looking to gain something

10
00:01:11,440 --> 00:01:20,560
to cultivate spiritual insight clarity of mind closer to the understanding of

11
00:01:20,560 --> 00:01:29,080
the truth of reality. Things that will keep you from that will make you cause

12
00:01:29,080 --> 00:01:34,960
you to lose your way on that on the path. It's one of these six the six that will

13
00:01:34,960 --> 00:01:46,360
cause you to lose your way and if you if you avoid this you will stay

