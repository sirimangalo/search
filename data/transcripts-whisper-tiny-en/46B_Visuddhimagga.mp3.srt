1
00:00:00,000 --> 00:00:07,920
Those defilements that have swab to grow in past or future or present, and the answer is,

2
00:00:07,920 --> 00:00:12,000
they are simply those described as arisen by having swab to grow in.

3
00:00:12,000 --> 00:00:20,000
But yeah, just that.

4
00:00:20,000 --> 00:00:32,000
Are those defilements that have swab to grow in past or future or present?

5
00:00:32,000 --> 00:00:38,000
And the answer is, they are simply those described as arisen by having swab to grow in.

6
00:00:38,000 --> 00:00:39,000
But yeah, just that.

7
00:00:39,000 --> 00:00:43,000
We cannot say that they are present or they are future or they are first.

8
00:00:43,000 --> 00:00:50,000
But yeah, just those deaths have arisen by having swab to grow in.

9
00:00:50,000 --> 00:00:57,000
Now, there are various meanings of arisen that is to say arisen as accurately operating and so on.

10
00:00:57,000 --> 00:01:04,000
So, I know this is exactly simple.

11
00:01:04,000 --> 00:01:09,000
Is this close to saying that they are empty?

12
00:01:09,000 --> 00:01:11,000
No.

13
00:01:11,000 --> 00:01:16,000
We will expect it out, right?

14
00:01:16,000 --> 00:01:17,000
Okay.

15
00:01:17,000 --> 00:01:18,000
Yeah.

16
00:01:18,000 --> 00:01:28,000
So, then the commented brings in four kinds of, what I call arisen.

17
00:01:28,000 --> 00:01:30,000
The part is open up.

18
00:01:30,000 --> 00:01:35,000
So, on the sheet, I get the part in one also.

19
00:01:35,000 --> 00:01:38,000
Four kinds of open up that features arisen.

20
00:01:38,000 --> 00:01:41,000
Four things that are called open up in family.

21
00:01:41,000 --> 00:01:47,000
And the one open up again means that that has arisen.

22
00:01:47,000 --> 00:01:53,000
Now, the first is arisen as actually occurring.

23
00:01:53,000 --> 00:01:57,000
That means all that is reckoned to possess the three moments of arising,

24
00:01:57,000 --> 00:01:58,000
eating and dissolution.

25
00:01:58,000 --> 00:02:01,000
That means it is the real present.

26
00:02:01,000 --> 00:02:02,000
Things.

27
00:02:02,000 --> 00:02:10,000
Because when we say something is present, we mean it has, it possesses three stages of arising

28
00:02:10,000 --> 00:02:14,000
and staying and disappearing or arising, eating and dissolution.

29
00:02:14,000 --> 00:02:17,000
So, this is the real present.

30
00:02:17,000 --> 00:02:23,000
So, this real present is sometimes called open up that which has arisen.

31
00:02:23,000 --> 00:02:27,000
The next one is arisen as being and gone.

32
00:02:27,000 --> 00:02:31,000
That means they have arisen and now they are no more.

33
00:02:31,000 --> 00:02:33,000
They are also called arisen.

34
00:02:33,000 --> 00:02:35,000
And there are two of them.

35
00:02:35,000 --> 00:02:39,000
Whole salmon and wholesome damas that these potatoes and potatoes exist,

36
00:02:39,000 --> 00:02:42,000
which have experienced the stimulus of an object.

37
00:02:42,000 --> 00:02:46,000
That means which have tasted the object, which has featured,

38
00:02:46,000 --> 00:02:52,000
experienced the object, the taste of the object, and disappeared.

39
00:02:52,000 --> 00:02:57,000
So, they are also called arisen as being and gone.

40
00:02:57,000 --> 00:03:03,000
Now, here being means experienced.

41
00:03:03,000 --> 00:03:06,000
The second is also anything conditioned.

42
00:03:06,000 --> 00:03:11,000
That has reached the three moments beginning with arising and has seized.

43
00:03:11,000 --> 00:03:19,000
So, something which has something conditioned, which has come into being and then disappeared.

44
00:03:19,000 --> 00:03:22,000
So, that also is called arisen.

45
00:03:22,000 --> 00:03:27,000
It is having been and gone.

46
00:03:27,000 --> 00:03:30,000
Now, that there arisen by opportunity made.

47
00:03:30,000 --> 00:03:34,000
Now, the past gamma is called arisen by opportunity made.

48
00:03:34,000 --> 00:03:36,000
It was really past.

49
00:03:36,000 --> 00:03:39,000
So, although it is past, it is called arisen.

50
00:03:39,000 --> 00:03:44,000
That means it is still with us or something like that.

51
00:03:44,000 --> 00:03:46,000
Because it has stood.

52
00:03:46,000 --> 00:03:48,000
That is the direct translation.

53
00:03:48,000 --> 00:03:53,000
It has existed inhibiting the result of other gamma,

54
00:03:53,000 --> 00:03:58,000
and has made the opportunity for its own result to arise in the future.

55
00:03:58,000 --> 00:04:05,000
When there is gamma and then it disappears, then it leaves something like potential

56
00:04:05,000 --> 00:04:09,000
in the continuity of beings to give results in the future.

57
00:04:09,000 --> 00:04:16,000
So, when it made the opportunity for its result to arise in the future,

58
00:04:16,000 --> 00:04:20,000
then it inhibits the result of other cameras.

59
00:04:20,000 --> 00:04:22,000
And the second is the future result.

60
00:04:22,000 --> 00:04:28,000
The future result is also called upina, although it has not yet arisen.

61
00:04:28,000 --> 00:04:31,000
So, although it has not yet arisen, it is called arisen.

62
00:04:31,000 --> 00:04:39,000
Because when opportunity arises made, it is sure to arise in the future.

63
00:04:39,000 --> 00:04:48,000
When gamma is, say, accumulated, I mean, gamma is done, then the fruit is sure to arise.

64
00:04:48,000 --> 00:04:58,000
So, the future result can be called arisen, although it is, in fact, it has not yet arisen.

65
00:04:58,000 --> 00:05:06,000
The fourth arisen, by having soil to grow in this, we are concerned with this, the fourth one.

66
00:05:06,000 --> 00:05:10,000
So, arisen, by having soil to grow in.

67
00:05:10,000 --> 00:05:18,000
That means, unholes some gamma, which has not been eradicated with respect to any given soil,

68
00:05:18,000 --> 00:05:19,000
then what is soil here?

69
00:05:19,000 --> 00:05:26,000
So, soil here means the five aggregates and the three planes of becoming, which are the object of Yipasana.

70
00:05:26,000 --> 00:05:34,000
And what has soil means mental defilements, which are capable of arising with respect to those aggregates.

71
00:05:34,000 --> 00:05:47,000
So, what the first element needs is this kind of defilements, arisen by having soil to grow in.

72
00:05:47,000 --> 00:05:51,000
And here, soil means the five aggregates in these planes of becoming.

73
00:05:51,000 --> 00:05:54,000
They are the object of Yipasana meditation.

74
00:05:54,000 --> 00:06:00,000
And what has soil means mental defilements, which are capable of arising with respect to those aggregates.

75
00:06:00,000 --> 00:06:07,000
That means, suppose I see an object, it is a variable object.

76
00:06:07,000 --> 00:06:14,000
And I do not practice Yipasana towards that object.

77
00:06:14,000 --> 00:06:19,000
So, I take it to be beautiful.

78
00:06:19,000 --> 00:06:27,000
Although at that moment, I may have no attachment, but since I have taken it to be beautiful,

79
00:06:27,000 --> 00:06:32,000
I can have attachment with regard to that thing in the future.

80
00:06:32,000 --> 00:06:40,000
So, that kind of attachment or mental defilement is what is called, which is called, I mean,

81
00:06:40,000 --> 00:06:45,000
which is a soil to grow in.

82
00:06:45,000 --> 00:06:55,000
So, with regard to objects, we have to practice Yipasana meditation so that we see their true nature.

83
00:06:55,000 --> 00:06:58,000
So, that we see that they are implemented and so on.

84
00:06:58,000 --> 00:07:10,000
When we have seen that, when we have practiced Yipasana towards that, then the defilements are not said to be in here and in these objects.

85
00:07:10,000 --> 00:07:24,000
But with regard to objects, which we feel to observe by Yipasana, there is always the possibility that defilements will arise.

86
00:07:24,000 --> 00:07:26,000
With regard to those things.

87
00:07:26,000 --> 00:07:44,000
Those defilements, which can arise through not having observed by Yipasana, are called the having soil to grow in.

88
00:07:44,000 --> 00:07:56,000
Actually, what the eradicates is not the present mental defilements, not the past mental defilements, not the future mental defilements,

89
00:07:56,000 --> 00:07:59,000
but it is something like future.

90
00:07:59,000 --> 00:08:09,000
Because some liability in our continuity, that is, when there are conditions, they can arise.

91
00:08:09,000 --> 00:08:18,000
So, that liability is what is eradicated by the per consciousness.

92
00:08:18,000 --> 00:08:27,000
And such the liability is just called having those having the soil to grow in.

93
00:08:27,000 --> 00:08:45,000
That means, in my continuity, let us see, in my continuity of consciousness, they can arise.

94
00:08:45,000 --> 00:09:08,000
So, they have my continuity as a soil to grow in, because I have taken the object to be beautiful, to be permanent, to be present, to be, what about that, to be substantial or to be self.

95
00:09:08,000 --> 00:09:23,000
So, since I have taken this thing to be permanent and so on at any time, the defilement can arise.

96
00:09:23,000 --> 00:09:37,000
So, that liability, that those defilements which can occur through not being observed with Yipasana, protocol, those that have soil to grow in.

97
00:09:37,000 --> 00:09:59,000
Because it has obtained, it has got my continuity, my mind, to grow in, to arise in.

98
00:09:59,000 --> 00:10:07,000
And that is not, and that is not meant objectively.

99
00:10:07,000 --> 00:10:16,000
That means, having the soil to grow in, having the soil means, having the soil not as an object.

100
00:10:16,000 --> 00:10:22,000
But as a place to grow and as a bridge, that is also important.

101
00:10:22,000 --> 00:10:38,000
Because if we say, it is by way of taking an object, that it has got the soil, then it can mean, say, there is an aha.

102
00:10:38,000 --> 00:10:42,000
He said to be beautiful.

103
00:10:42,000 --> 00:10:52,000
So, he meant so on the arghand, and then he had sexual desires for dead arghand.

104
00:10:52,000 --> 00:10:58,000
I mean, he wanted dead arghand to be something of his wife or something like that.

105
00:10:58,000 --> 00:11:10,000
And so, even with regard to the body of an arghand, mental defilements can arise in other persons.

106
00:11:10,000 --> 00:11:14,000
By taking the body of an arghand as an object.

107
00:11:14,000 --> 00:11:32,000
So, if we mean that having the soil means, having the soil as an object, then it will mean that an arghand can eradicate mental defilements of another person.

108
00:11:32,000 --> 00:11:45,000
Because that person has taken the arghand as an object, and then he has attachment arise in his mind.

109
00:11:45,000 --> 00:11:53,000
So, having the soil does not mean having taken as an object.

110
00:11:53,000 --> 00:12:06,000
But having the soil means having got somewhere at some place to grow in or to grow out of.

111
00:12:06,000 --> 00:12:17,000
So, yeah, like the elder who had obtained, like the defilements that arose in the elder Mahathasa,

112
00:12:17,000 --> 00:12:25,000
for not yet. I think it's 806, right?

113
00:12:25,000 --> 00:12:31,000
Like those that arose in the rich man's story with respect to the aggregates of Mahathata.

114
00:12:31,000 --> 00:12:38,000
So, he had wrong desires for Mahakatana.

115
00:12:38,000 --> 00:12:49,000
And in the Brahman student Nanda, with respect to Upalawana, Upalawana was an arghand in Nanda.

116
00:12:49,000 --> 00:12:57,000
And Nanda was a girl.

117
00:12:57,000 --> 00:13:12,000
Nanda fell in love with her. And so, one day when she came back from the sun, then he raped Nanda.

118
00:13:12,000 --> 00:13:28,000
So, having soil means not having soil by way of taking as an object, but by way of having a place, having a base.

119
00:13:28,000 --> 00:13:45,000
Because if we see the defilement which has the soil to grow in by way of taking objects, then it may mean the defilement in another person also.

120
00:13:45,000 --> 00:13:54,000
And nobody can eradicate, even the Buddha cannot eradicate the defilement in another person.

121
00:13:54,000 --> 00:14:05,000
So, I can eradicate defilements in my mind and not in another person's mind.

122
00:14:05,000 --> 00:14:19,000
So, it is to be understood, having soil means having soil not as an object, not as taking as an object.

123
00:14:19,000 --> 00:14:28,000
Having it as its location or something like that.

124
00:14:28,000 --> 00:14:39,000
So, and if what is called arisen by having soil to grow in, no one could abandon the root of becoming because it would be an abandonable.

125
00:14:39,000 --> 00:14:43,000
That means because it belongs to another person.

126
00:14:43,000 --> 00:14:50,000
But arisen by having soil should be understood subjectively with respect to the basis for them in oneself.

127
00:14:50,000 --> 00:14:53,000
That means they should be understood as having the place.

128
00:14:53,000 --> 00:14:58,000
Having the place to live or to exist something like that.

129
00:14:58,000 --> 00:15:05,000
For the defilements that are the root of the realm, in one's world, actually they not fully understood by inside.

130
00:15:05,000 --> 00:15:18,000
So, if we do not practice Vipasana towards themes, then we do not fully understand these objects.

131
00:15:18,000 --> 00:15:27,000
Fully understanding, having seen, they are arriving and disappearing, and they are characteristics.

132
00:15:27,000 --> 00:15:41,000
And also, being able to get rid of matter of defilements with regard to them.

133
00:15:41,000 --> 00:15:53,000
So, for the defilements that are the root of the realm of interior, that the realm in here and in one's own aggregate are not fully understood by inside.

134
00:15:53,000 --> 00:16:03,000
From the instance, those aggregates arise, and that is what should be understood as arisen by having soil to grow in, in the sense of it being an abandoned.

135
00:16:03,000 --> 00:16:32,000
So, in brief what the path abandons or eliminates is just the mental defilements which would arise when there are conditions, which would arise because one has not seen them.

136
00:16:32,000 --> 00:16:38,000
So, seeing them correctly, one has not fully understood them.

137
00:16:38,000 --> 00:16:50,000
So, that liability is what the path consciousness eradicates, and not the real mental defilements arising at the moment.

138
00:16:50,000 --> 00:17:00,000
Because when there are mental defilements, there can be no path consciousness, and when there is path consciousness, there can be no mental defilements.

139
00:17:00,000 --> 00:17:15,000
And then, if you understand this, the other passages are not difficult.

140
00:17:15,000 --> 00:17:25,000
So, and then the other gives another set of four kinds of opina, paragraph 89.

141
00:17:25,000 --> 00:17:39,000
Besides, these there are four other ways of crossing arisen, namely arisen is happening, arisen with apprehension of an object, arisen through non-suppression, and arisen through non-abolition.

142
00:17:39,000 --> 00:17:55,000
So, arisen as happening is the same as one arisen as accurately occurring. That means, it is rightly in existence, with us in three moments.

143
00:17:55,000 --> 00:18:07,000
Now, when an object has had some previous time come into focus in the eye, etc., and development did not arise then, but arose in full force leader on simply because the object had been apprehended.

144
00:18:07,000 --> 00:18:14,000
That means, taken firmly as dominant and so on.

145
00:18:14,000 --> 00:18:18,000
Then, that defilement is called arisen with apprehension of an object.

146
00:18:18,000 --> 00:18:31,000
Like the defilement that arose in the elder Mahapists after seeing the form of a person of the opposite sex while wandering for arms in the village of Kalea and me, or not, not Kalea and me.

147
00:18:31,000 --> 00:18:50,000
So, before there was no defilement in elder Mahapists, but he saw a person of the opposite sex, and then the defilement arose in his mind, so like that.

148
00:18:50,000 --> 00:19:03,000
As long as the defilement is not suppressed by either serenity or insight, though it may not have actually entered the conscious continuity, it is nevertheless called arisen through non-suppression.

149
00:19:03,000 --> 00:19:15,000
That means, it is not really arisen, but it is called arisen, because it has not been suppressed, because there is no cause to prevent its arising.

150
00:19:15,000 --> 00:19:27,000
Even when they are suppressed by serenity or insight, they are still called arisen through non-abolition, because of the necessity for the arising, has not been transcended unless they have been cut off by the path.

151
00:19:27,000 --> 00:19:38,000
That is, those that are not cut off or abandoned by the path consciousness.

152
00:19:38,000 --> 00:19:42,000
So, they are called arisen through non-abolition.

153
00:19:42,000 --> 00:19:46,000
Now, abortion and suppression are different here.

154
00:19:46,000 --> 00:20:00,000
Suppression means keeping them at bay for some time, and abolition means eradicating altogether.

155
00:20:00,000 --> 00:20:14,000
Like the elder who had obtained the attainment and the defilements that arose in him, while he was going to the air on his ceiling, the sound of a woman singing with the street voice as he was gathering flowers and a group of blossoming trees.

156
00:20:14,000 --> 00:20:25,000
So, he had obtained the eight attainment, so that means he has suppressed the mental development by the eight attainment.

157
00:20:25,000 --> 00:20:30,000
And the defilements that arose in him, how he was going to the air.

158
00:20:30,000 --> 00:20:40,000
But he was going to be air, but he heard the woman singing, the flocking flowers, and so the defilements arose in him.

159
00:20:40,000 --> 00:20:54,000
So, such defilements are called, and those that arisen through non-abolition, because they are not abolished, they are not eradicated, they may arise when they are conditioned.

160
00:20:54,000 --> 00:20:57,000
So, there are these four kinds.

161
00:20:57,000 --> 00:21:11,000
And the three kinds, namely arisen with apprehension of an object, arisen through non-suppression, and arisen through non-abolition should be understood as included by four arisen by having soil to grow in.

162
00:21:11,000 --> 00:21:23,000
So, the fourth of the first list corresponds to three of the second list.

163
00:21:23,000 --> 00:21:41,000
So, as regards to the kinds or reasons stated, the four kinds, namely, as actually occurring, as been and gone by opportunity made, and as happening, cannot be abandoned by any knowledge, because they cannot be eliminated by the thoughts.

164
00:21:41,000 --> 00:21:52,000
But the four kinds of illusions, namely, by having soil to grow in with apprehension of an object through non-suppression, and through non-abolition, can all be abandoned, because if given Monday,

165
00:21:52,000 --> 00:22:00,000
also from Monday, knowledge, which arises when it arises, nullifies a given one of these modes of the arisen.

166
00:22:00,000 --> 00:22:10,000
So, when we see the prep abendance, then we mean this second four.

167
00:22:10,000 --> 00:22:17,000
I mean, second four means here, mentioned one of the first and three of the second list.

168
00:22:17,000 --> 00:22:25,000
So, by having soil and with apprehension of an object, through non-suppression, and through non-abolition.

169
00:22:25,000 --> 00:22:41,000
So, what the first erotic is the inherent tendencies, or latent tendencies, and not the ones that have arisen in the consciousness.

170
00:22:41,000 --> 00:22:51,000
Because when they are in the consciousness, then we cannot dedicate them, simply because they are there.

171
00:22:51,000 --> 00:23:07,000
That is why, in the soldiers, or especially in the commonries, it is said, and no part in the rotor.

172
00:23:07,000 --> 00:23:13,000
That means, non-arising in the future is called cessation.

173
00:23:13,000 --> 00:23:21,000
So, cessation of mental defilements means non-arising of them in the future.

174
00:23:21,000 --> 00:23:27,000
Because mental defilements arise and then they disappear by themselves.

175
00:23:27,000 --> 00:23:30,000
You don't have to do anything about them.

176
00:23:30,000 --> 00:23:32,000
They arise and they disappear.

177
00:23:32,000 --> 00:23:39,000
So, the cessation of mental defilements really means not letting them arise again.

178
00:23:39,000 --> 00:23:45,000
So, non-arising in the future is what is called cessation of mental defilements.

179
00:23:45,000 --> 00:23:58,000
And here also, when there is path, it has the power to render them in active or particle.

180
00:23:58,000 --> 00:24:04,000
Non-arising. Now, the full function, the full function is in the single moment.

181
00:24:04,000 --> 00:24:12,000
It is said that the part does the full functions simultaneously.

182
00:24:12,000 --> 00:24:17,000
And what are before?

183
00:24:17,000 --> 00:24:25,000
Now, at the tenths of penetrating to the truth, each one of the full technology is said to exercise full functions in the single moment.

184
00:24:25,000 --> 00:24:40,000
And these are full understanding of the first noble truth, abandoning the second noble truth, realizing the third noble truth, and developing the full noble truth.

185
00:24:40,000 --> 00:24:51,000
So, at one moment, the part consciousness or part knowledge exercises these full functions.

186
00:24:51,000 --> 00:24:57,000
Not one by one, but simultaneously, these full functions are done.

187
00:24:57,000 --> 00:25:01,000
For this is said by the agent, just as the lamp performs full function and so on.

188
00:25:01,000 --> 00:25:11,000
So, the similarly of the lamp is brought here.

189
00:25:11,000 --> 00:25:14,000
And then another matter, 95.

190
00:25:14,000 --> 00:25:18,000
That's the sun when it arises, performs full function and so on.

191
00:25:18,000 --> 00:25:23,000
And then another matter, it both performs full functions.

192
00:25:23,000 --> 00:25:39,000
So, these similes are given to illustrate the full functions, stand by fat consciousness and simultaneously.

193
00:25:39,000 --> 00:25:44,000
And then in paragraph 97.

194
00:25:44,000 --> 00:25:59,000
So, when his knowledge occurs with the full function in a single moment at the time of penetrating the full truth, then the full truth will be single by the present at the center of fullness in 16 ways, and it is said.

195
00:25:59,000 --> 00:26:09,000
So, there are 16 ways or 16 meanings mentioned here, four for each truth.

196
00:26:09,000 --> 00:26:15,000
So, how is there a single penetration of the full truth and the sense of trueness?

197
00:26:15,000 --> 00:26:19,000
There is single penetration of the full truth and the sense of trueness and 16 aspects.

198
00:26:19,000 --> 00:26:27,000
Suffering has the meaning of oppressing, meaning of being formed, meaning of burning or torment and meaning of change.

199
00:26:27,000 --> 00:26:31,000
These are the four meanings of the first noble truth.

200
00:26:31,000 --> 00:26:41,000
And then the second noble truth, the four meanings of second noble truth, meaning of accumulation, meaning of souls, meaning of bondage and meaning of impediment.

201
00:26:41,000 --> 00:26:54,000
And the four meanings of the third is the meaning of escape, meaning of the crucial, meaning of being not formed, and meaning of gentlessness.

202
00:26:54,000 --> 00:27:04,000
And the meaning of the fourth, noble truth is meaning of cause, meaning of outlet, meaning of cause, meaning of sin, and meaning of dominance.

203
00:27:04,000 --> 00:27:13,000
So, these are called the 16 ways of the four noble truths.

204
00:27:13,000 --> 00:27:17,000
And paragraph 98 raises a question here, it may be asked.

205
00:27:17,000 --> 00:27:24,000
Since there are other meanings of suffering, etc. too, such as indices, human and so on.

206
00:27:24,000 --> 00:27:27,000
Why then are only four mention for each?

207
00:27:27,000 --> 00:27:41,000
Now, if you go back to chapter 20, paragraph 18, you will find that there are 40 ways of looking at things as impermanent and so on.

208
00:27:41,000 --> 00:27:52,000
So, the first noble truth has more than four meanings, meaning of disease and the meaning of a tumor and so on.

209
00:27:52,000 --> 00:27:56,000
So, why are they not taken and only before are taken?

210
00:27:56,000 --> 00:28:04,000
Now, we answered that in this context, it is because of what is evident through seeing the other three truths in each case.

211
00:28:04,000 --> 00:28:13,000
Firstly, in the passage beginning here, in what is knowledge of suffering, it is the understanding, the act of understanding that arises contingent upon suffering.

212
00:28:13,000 --> 00:28:18,000
Knowledge of the truth is presented as having a single truth as its object individual.

213
00:28:18,000 --> 00:28:28,000
But in the passage beginning, because he who sees suffering sees also its origin, it is presented as accomplishing its function with respect to the other three truths simultaneously,

214
00:28:28,000 --> 00:28:31,000
that it is making one of them its object.

215
00:28:31,000 --> 00:28:39,000
And it is described as seeing the one each at a time.

216
00:28:39,000 --> 00:28:50,000
But in other passages, then the seeing is presented as occurring at the same time, I mean seeing of four at the same time.

217
00:28:50,000 --> 00:28:59,000
Now, as regards these two contexts, when firstly knowledge makes each true its object seemingly, then

218
00:28:59,000 --> 00:29:06,000
suffering has the characteristic of progressing and as its individual has done so.

219
00:29:06,000 --> 00:29:19,000
In this paragraph, we should we should strike out those in the square brackets.

220
00:29:19,000 --> 00:29:28,000
It is inserted by the translator and it is not warranted by the sub commentary.

221
00:29:28,000 --> 00:29:38,000
Now, paragraph 99, 99, 100 red, 101, 102, almost for our kids.

222
00:29:38,000 --> 00:29:48,000
Yeah, after the likewise, three, three paragraphs after likewise, and after I didn't just remember you.

223
00:29:48,000 --> 00:29:56,000
But 99 also, after then, when suffering is made the object?

224
00:29:56,000 --> 00:29:59,000
No.

225
00:29:59,000 --> 00:30:09,000
If suffering is made the object then, the only thing it will see is the first meaning of the suffering and not the others.

226
00:30:09,000 --> 00:30:23,000
But what the other is explaining here is that when you see the second noble suit of origin,

227
00:30:23,000 --> 00:30:31,000
then the sense of being formed of the first noble suit becomes evident.

228
00:30:31,000 --> 00:30:38,000
So when you see the second truth, then one meaning of the first two becomes evident,

229
00:30:38,000 --> 00:30:45,000
and when you see the third noble truth, another meaning of the first noble suits becomes evident.

230
00:30:45,000 --> 00:30:50,000
And when you see the fourth noble truth, yet another meaning of the first noble should become evident.

231
00:30:50,000 --> 00:30:52,500
That is why these four are mentioned.

232
00:30:52,500 --> 00:30:54,100
I mentioned here.

233
00:30:54,100 --> 00:30:56,460
So when you see the first noble truth,

234
00:30:56,460 --> 00:31:01,740
then the meaning of oppression

235
00:31:01,740 --> 00:31:03,420
is evident.

236
00:31:03,420 --> 00:31:05,860
When you see the second noble truth,

237
00:31:05,860 --> 00:31:08,840
the meaning of the informed of the first noble truth

238
00:31:08,840 --> 00:31:10,420
becomes evident.

239
00:31:10,420 --> 00:31:12,700
When you see the sad number to the meaning

240
00:31:12,700 --> 00:31:15,380
of burning or torment of the first noble truth

241
00:31:15,380 --> 00:31:20,780
becomes evident, and when it is

242
00:31:20,780 --> 00:31:34,300
change becomes evidence. That is why these four meanings are given.

243
00:31:34,300 --> 00:31:49,500
So by their own seeing them individually and also through seeing other truths, the meanings

244
00:31:49,500 --> 00:31:59,140
of the given truth becomes evidence. So these paragraphs explain at this meaning.

245
00:31:59,140 --> 00:32:05,180
So we do not need when suffering is made the object, when such an origin is made the object,

246
00:32:05,180 --> 00:32:07,340
when such an origin is made the object, when such an origin is made the object, when the

247
00:32:07,340 --> 00:32:23,420
process is made the object. And then in paragraph 99, an example is given the S, the beauties

248
00:32:23,420 --> 00:32:29,380
or soldiery's ugliness did to the venerable nanda through seeing the celestial names.

249
00:32:29,380 --> 00:32:36,820
Now, somebody or beauty was said to be very beautiful. She was very light like a beauty

250
00:32:36,820 --> 00:32:47,140
queen. So she was very beautiful and so nanda was so much in love of her. But Voda wanted

251
00:32:47,140 --> 00:32:57,780
to teach him a lesson. So Voda took him to the celestial world and showed celestial names.

252
00:32:57,780 --> 00:33:05,140
So after seeing the celestial names, who is more beautiful, sundry or celestial names,

253
00:33:05,140 --> 00:33:13,220
then he said, or sundry is like a sheep monkey. We saw on our way here something like that.

254
00:33:13,220 --> 00:33:22,100
After seeing the name, the sundry becomes ugly. But actually, the sundry was a beautiful woman.

255
00:33:22,100 --> 00:33:28,740
So here also, cooling fat removes the burning of the defragments. And so suffering in

256
00:33:28,740 --> 00:33:34,660
sense of burning becomes evident through seeing the fat. When you see the cool one,

257
00:33:34,660 --> 00:33:56,020
then the burning of the another thing becomes evident. Okay, that should be the end of today.

