1
00:00:00,000 --> 00:00:17,600
Hello everyone, welcome to our Saturday broadcast.

2
00:00:17,600 --> 00:00:29,640
Today, as usual, we will be fielding questions about meditation practice and Buddhism.

3
00:00:29,640 --> 00:00:58,640
There's an opportunity for people interested in practicing this tradition to clear up their doubts, learn new things, correct any bad meditative habits they might have developed.

4
00:00:58,640 --> 00:01:02,240
And just overall learn more about the Dhamma.

5
00:01:02,240 --> 00:01:11,840
So to start, as usual, we will have a 15 minute preface meditation.

6
00:01:11,840 --> 00:01:17,240
It gives us time to collect questions, gives people time to ask questions.

7
00:01:17,240 --> 00:01:28,440
If you have questions, you're welcome at any time to start posting them in the chat.

8
00:01:28,440 --> 00:01:54,640
And once you've done that, then while we have 15 minutes to clear our minds, clear our vision, and settle into focus on what's truly important to see clearly to let go and find freedom from suffering.

9
00:01:54,640 --> 00:02:00,640
So, I'll be back at quarter after the hour to start with answering the questions.

10
00:02:24,640 --> 00:02:44,640
Thank you very much.

11
00:02:54,640 --> 00:03:14,640
Thank you very much.

12
00:03:24,640 --> 00:03:44,640
Thank you.

13
00:03:54,640 --> 00:04:14,640
Thank you.

14
00:04:24,640 --> 00:04:44,640
Thank you.

15
00:04:54,640 --> 00:05:14,640
Thank you.

16
00:05:24,640 --> 00:05:44,640
Thank you.

17
00:05:54,640 --> 00:06:14,640
Thank you.

18
00:06:24,640 --> 00:06:44,640
Thank you.

19
00:06:54,640 --> 00:07:14,640
Thank you.

20
00:07:24,640 --> 00:07:44,640
Thank you.

21
00:07:54,640 --> 00:08:14,640
Thank you.

22
00:08:24,640 --> 00:08:44,640
Thank you.

23
00:08:54,640 --> 00:09:19,640
Thank you.

24
00:09:24,640 --> 00:09:49,640
Thank you.

25
00:09:54,640 --> 00:10:19,640
Thank you.

26
00:10:24,640 --> 00:10:49,640
Thank you.

27
00:10:54,640 --> 00:11:19,640
Thank you.

28
00:11:24,640 --> 00:11:49,640
Thank you.

29
00:11:54,640 --> 00:12:19,640
Thank you.

30
00:12:24,640 --> 00:12:49,640
Thank you.

31
00:12:54,640 --> 00:13:19,640
Thank you.

32
00:13:24,640 --> 00:13:49,640
Thank you.

33
00:13:54,640 --> 00:14:19,640
Thank you.

34
00:14:24,640 --> 00:14:49,640
Thank you.

35
00:14:54,640 --> 00:15:19,640
Thank you.

36
00:15:19,640 --> 00:15:44,640
Thank you.

37
00:15:44,640 --> 00:16:09,640
Thank you.

38
00:16:09,640 --> 00:16:34,640
Thank you.

39
00:16:34,640 --> 00:16:59,640
Thank you.

40
00:16:59,640 --> 00:17:24,640
Thank you.

41
00:17:24,640 --> 00:17:49,640
Thank you.

42
00:17:49,640 --> 00:18:14,640
Thank you.

43
00:18:14,640 --> 00:18:34,640
Thank you.

44
00:18:34,640 --> 00:18:59,640
Thank you.

45
00:18:59,640 --> 00:19:25,640
Thank you.

46
00:19:25,640 --> 00:19:51,640
Thank you.

47
00:19:51,640 --> 00:20:17,640
Thank you.

48
00:20:17,640 --> 00:20:43,640
Thank you.

49
00:20:43,640 --> 00:21:09,640
Thank you.

50
00:21:09,640 --> 00:21:35,640
Thank you.

51
00:21:35,640 --> 00:22:01,640
Thank you.

52
00:22:01,640 --> 00:22:27,640
Thank you.

53
00:22:27,640 --> 00:22:53,640
Thank you.

54
00:22:53,640 --> 00:23:19,640
Thank you.

55
00:23:19,640 --> 00:23:45,640
Thank you.

56
00:23:45,640 --> 00:24:11,640
Thank you.

57
00:24:11,640 --> 00:24:37,640
Thank you.

58
00:24:37,640 --> 00:25:03,640
Thank you.

59
00:25:03,640 --> 00:25:29,640
Thank you.

60
00:25:29,640 --> 00:25:55,640
Thank you.

61
00:25:55,640 --> 00:26:21,640
Thank you.

62
00:26:21,640 --> 00:26:47,640
Thank you.

63
00:26:47,640 --> 00:27:13,640
Thank you.

64
00:27:13,640 --> 00:27:39,640
Thank you.

65
00:27:39,640 --> 00:28:05,640
Thank you.

66
00:28:05,640 --> 00:28:31,640
Thank you.

67
00:28:31,640 --> 00:28:57,640
Thank you.

68
00:28:57,640 --> 00:29:23,640
Thank you.

69
00:29:23,640 --> 00:29:49,640
Thank you.

70
00:29:49,640 --> 00:30:15,640
Thank you.

71
00:30:15,640 --> 00:30:41,640
Thank you.

72
00:30:41,640 --> 00:31:07,640
Thank you.

73
00:31:07,640 --> 00:31:33,640
Thank you.

74
00:31:33,640 --> 00:31:59,640
Thank you.

75
00:31:59,640 --> 00:32:25,640
Thank you.

76
00:32:25,640 --> 00:32:51,640
Thank you.

77
00:32:51,640 --> 00:33:17,640
Thank you.

78
00:33:17,640 --> 00:33:43,640
Thank you.

79
00:33:43,640 --> 00:34:09,640
Thank you.

80
00:34:09,640 --> 00:34:35,640
Thank you.

81
00:34:35,640 --> 00:35:01,640
Thank you.

82
00:35:01,640 --> 00:35:27,640
Thank you.

83
00:35:27,640 --> 00:35:53,640
Thank you.

84
00:35:53,640 --> 00:36:19,640
Thank you.

85
00:36:19,640 --> 00:36:45,640
Thank you.

86
00:36:45,640 --> 00:37:11,640
Thank you.

87
00:37:11,640 --> 00:37:37,640
Thank you.

88
00:37:37,640 --> 00:38:03,640
Thank you.

89
00:38:03,640 --> 00:38:29,640
Thank you.

90
00:38:29,640 --> 00:38:55,640
Thank you.

91
00:38:55,640 --> 00:39:21,640
Thank you.

92
00:39:21,640 --> 00:39:47,640
Thank you.

93
00:39:47,640 --> 00:40:13,640
Thank you.

94
00:40:13,640 --> 00:40:39,640
Thank you.

95
00:40:39,640 --> 00:41:05,640
Thank you.

96
00:41:05,640 --> 00:41:31,640
Thank you.

97
00:41:31,640 --> 00:41:57,640
Thank you.

98
00:41:57,640 --> 00:42:23,640
Thank you.

99
00:42:23,640 --> 00:42:49,640
Thank you.

100
00:42:49,640 --> 00:43:15,640
Thank you.

101
00:43:15,640 --> 00:43:41,640
Thank you.

102
00:43:41,640 --> 00:44:07,640
Thank you.

103
00:44:07,640 --> 00:44:33,640
Thank you.

104
00:44:33,640 --> 00:44:59,640
Thank you.

105
00:44:59,640 --> 00:45:25,640
Thank you.

106
00:45:25,640 --> 00:45:51,640
Thank you.

107
00:45:51,640 --> 00:46:17,640
Thank you.

108
00:46:17,640 --> 00:46:43,640
Thank you.

109
00:46:43,640 --> 00:47:09,640
Thank you.

110
00:47:13,640 --> 00:47:39,640
Thank you.

111
00:47:39,640 --> 00:48:05,640
Thank you.

112
00:48:05,640 --> 00:48:31,640
Thank you.

113
00:48:31,640 --> 00:48:57,640
Thank you.

114
00:48:57,640 --> 00:49:23,640
Thank you.

115
00:49:23,640 --> 00:49:49,640
Thank you.

116
00:49:49,640 --> 00:50:15,640
Thank you.

117
00:50:15,640 --> 00:50:41,640
Thank you.

118
00:50:41,640 --> 00:51:07,640
Thank you.

119
00:51:07,640 --> 00:51:33,640
Thank you.

120
00:51:33,640 --> 00:51:59,640
Thank you.

121
00:51:59,640 --> 00:52:27,640
Thank you.

122
00:52:27,640 --> 00:52:53,640
Thank you.

123
00:52:53,640 --> 00:53:00,640
I'm not sure which way you're substituting.

124
00:53:00,640 --> 00:53:06,640
Instead of meditating sleeping or instead of sleeping meditating, I assume you mean instead of sleeping

125
00:53:06,640 --> 00:53:07,640
meditating.

126
00:53:07,640 --> 00:53:32,640
If you're already an advanced meditator who could actually do that sounds like you might be.

127
00:53:32,640 --> 00:53:58,640
Thank you.

128
00:53:58,640 --> 00:54:24,640
Thank you.

129
00:54:24,640 --> 00:54:50,640
Thank you.

130
00:54:50,640 --> 00:55:16,640
Thank you.

131
00:55:16,640 --> 00:55:42,640
Thank you.

132
00:55:42,640 --> 00:56:08,640
Thank you.

133
00:56:08,640 --> 00:56:34,640
Thank you.

134
00:56:34,640 --> 00:57:00,640
Thank you.

135
00:57:00,640 --> 00:57:26,640
Thank you.

136
00:57:26,640 --> 00:57:52,640
Thank you.

137
00:57:52,640 --> 00:58:18,640
Thank you.

138
00:58:18,640 --> 00:58:44,640
Thank you.

139
00:58:44,640 --> 00:59:10,640
Thank you.

140
00:59:10,640 --> 00:59:36,640
Thank you.

141
00:59:36,640 --> 01:00:02,640
Thank you.

142
01:00:02,640 --> 01:00:28,640
Thank you.

