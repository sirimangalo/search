1
00:00:00,000 --> 00:00:10,160
Well, it's interesting thing about this, if we have the wisdom to realize, realize if it's

2
00:00:10,160 --> 00:00:20,760
suffering or something like that, it's, it's, it's, it's, I think people, I mean, start

3
00:00:20,760 --> 00:00:27,000
question reality, start, try to change reality, then you're going to see suffering.

4
00:00:27,000 --> 00:00:33,120
And that's what I would say, just, people, they want to change the reality, they leave

5
00:00:33,120 --> 00:00:44,960
things or they experience them, you become aware that you cannot change reality in this

6
00:00:44,960 --> 00:00:45,960
way.

7
00:00:45,960 --> 00:00:52,760
I don't know, you, anyone agree on this?

8
00:00:52,760 --> 00:00:57,760
It's sort about perception, change your perception.

9
00:00:57,760 --> 00:00:58,760
That's right.

10
00:00:58,760 --> 00:00:59,760
Yeah, that's another step.

11
00:00:59,760 --> 00:01:00,760
That's right.

12
00:01:00,760 --> 00:01:01,760
Yeah.

13
00:01:01,760 --> 00:01:02,760
It's another step.

14
00:01:02,760 --> 00:01:03,760
Yeah.

15
00:01:03,760 --> 00:01:04,760
You can change yourself.

16
00:01:04,760 --> 00:01:10,760
You can't change the surroundings of some, for example, like the water makes you wet.

17
00:01:10,760 --> 00:01:12,760
I can't change this.

18
00:01:12,760 --> 00:01:13,760
Can I?

19
00:01:13,760 --> 00:01:18,760
So, or, you know, the fire is going to hurt you.

20
00:01:18,760 --> 00:01:23,760
You know, and so on.

21
00:01:23,760 --> 00:01:25,760
Every aspect of reality, you can't change it.

22
00:01:25,760 --> 00:01:27,760
You just change yourself.

23
00:01:27,760 --> 00:01:28,760
Reality stays.

24
00:01:28,760 --> 00:01:29,760
It is.

25
00:01:29,760 --> 00:01:34,760
But you just, uh, rely differently to it.

26
00:01:34,760 --> 00:01:40,760
But, uh, you know, it's just, I think a question in reality.

27
00:01:40,760 --> 00:01:50,760
Um, you know, I, that's what I think about this.

28
00:01:50,760 --> 00:01:59,760
So, because you haven't been said yet, I would exactly, it, it, it happened.

29
00:01:59,760 --> 00:02:00,760
Here we go.

30
00:02:00,760 --> 00:02:01,760
I understand.

31
00:02:01,760 --> 00:02:02,760
I'm good.

32
00:02:02,760 --> 00:02:09,760
About the mind made body.

33
00:02:09,760 --> 00:02:10,760
Do you have a question?

34
00:02:10,760 --> 00:02:14,760
Was that, is that an I have a question moment?

35
00:02:14,760 --> 00:02:15,760
No, I know.

36
00:02:15,760 --> 00:02:18,760
I was going to chime in, but we'll go ahead.

37
00:02:18,760 --> 00:02:20,760
We're not recording, but go ahead.

38
00:02:20,760 --> 00:02:27,760
Oh, I don't know if this is a Buddhist thing or not, but.

39
00:02:27,760 --> 00:02:30,760
Um, I think it.

40
00:02:30,760 --> 00:02:38,760
Kind of learning to just, we, I think earlier, just a bit earlier, we were talking about acceptance.

41
00:02:38,760 --> 00:02:52,760
And, uh, it's, you know, this is difficult, but I've learned to just kind of accept the world and accept my surroundings.

42
00:02:52,760 --> 00:03:02,760
And that includes even the, uh, even the frightening things in life, even the, uh, the ugly things that people say in the violence in the world.

43
00:03:02,760 --> 00:03:09,760
It's, it's not easy, but it's a part of the world and it's just a part of the experience.

44
00:03:09,760 --> 00:03:18,760
And, uh, it goes with all of the rest of it, you know, so the beauty and, and the, and the discord.

45
00:03:18,760 --> 00:03:25,760
Uh, I just, it, that's just, uh, you know, the way that, that it is.

46
00:03:25,760 --> 00:03:37,760
And, uh, just, I, you know, not making excuses for any violent type of behavior or anything like that, but just accepting it.

47
00:03:37,760 --> 00:03:40,760
You know, let me kind of away.

48
00:03:40,760 --> 00:03:46,760
Yeah, you see, I accepted the exist, you know, you can accept the exist.

49
00:03:46,760 --> 00:03:54,760
Uh, do you actually, uh, you don't have, well, you can accept and then what?

50
00:03:54,760 --> 00:03:55,760
Okay.

51
00:03:55,760 --> 00:03:56,760
Okay.

52
00:03:56,760 --> 00:03:57,760
Okay.

53
00:03:57,760 --> 00:03:58,760
And then what?

54
00:03:58,760 --> 00:04:02,760
Because like we were talking about earlier, you're saying it's sometimes you do want to change your life.

55
00:04:02,760 --> 00:04:03,760
Is this the deal?

56
00:04:03,760 --> 00:04:10,760
You know, something, what you're getting at is that, well, maybe sometimes you should try to escape certain situations, right?

57
00:04:10,760 --> 00:04:11,760
Yeah.

58
00:04:11,760 --> 00:04:13,760
Is that what you're asking?

59
00:04:13,760 --> 00:04:14,760
Yeah.

60
00:04:14,760 --> 00:04:15,760
I mean,

61
00:04:15,760 --> 00:04:25,760
is it more for you?

62
00:04:25,760 --> 00:04:26,760
Yeah.

63
00:04:26,760 --> 00:04:27,760
Okay.

64
00:04:27,760 --> 00:04:28,760
Okay.

65
00:04:28,760 --> 00:04:29,760
I accept.

66
00:04:29,760 --> 00:04:30,760
Yeah.

67
00:04:30,760 --> 00:04:36,760
I mean, it's better to, to, yeah, it's not, see, Buddhism isn't about, just accepting things.

68
00:04:36,760 --> 00:04:40,760
It's about, um, giving up things and letting go of things.

69
00:04:40,760 --> 00:04:48,760
So if you see that something is useless, is meaningless, uh, then you should completely give it up.

70
00:04:48,760 --> 00:04:56,760
So it's not, it's not, the word, the important word is not quite acceptance because that doesn't really work.

71
00:04:56,760 --> 00:05:01,760
If something, you know, something, acceptance is in terms of not hating it, right?

72
00:05:01,760 --> 00:05:02,760
Yeah, exactly.

73
00:05:02,760 --> 00:05:07,760
There's a part of me that actually psychoticly hates the world.

74
00:05:07,760 --> 00:05:12,760
Well, then, you have to learn to give that up.

75
00:05:12,760 --> 00:05:15,760
And so in, in some sense, there's an acceptance, but it's not really acceptance.

76
00:05:15,760 --> 00:05:18,760
It's seeing the futility of hatred.

77
00:05:18,760 --> 00:05:25,760
What the truth comes on the Buddhist path is the giving up of the hatred.

78
00:05:25,760 --> 00:05:28,760
This, no, the seeing that the hatred is useless is meaningless.

79
00:05:28,760 --> 00:05:30,760
It is unbeneficial.

80
00:05:30,760 --> 00:05:32,760
And therefore actually giving it up.

81
00:05:32,760 --> 00:05:42,760
So you do actually, uh, leave behind things like hatred and, and, and, and, and moreover, you give up situations.

82
00:05:42,760 --> 00:05:45,760
You, you know, so there's discussion about becoming a monk.

83
00:05:45,760 --> 00:05:52,760
I think that's very much a part of the Buddhist path to leave a situation when you see it as being useless.

84
00:05:52,760 --> 00:05:56,760
When you see that the home life is useless, I think that's a very good reason.

85
00:05:56,760 --> 00:06:04,760
If you're just, um, so angry at having to deal with people, for example, and so you become a monk.

86
00:06:04,760 --> 00:06:07,760
It's not a very good reason to become a monk.

87
00:06:07,760 --> 00:06:13,760
But if you see the futility of what, you know, one might objectively argue is,

88
00:06:13,760 --> 00:06:19,760
might argue is objectively futile, then that's, because that's very much in meditation practice,

89
00:06:19,760 --> 00:06:25,760
which, what, what causes a person to realize nibana, the mind doesn't jump into nibana.

90
00:06:25,760 --> 00:06:28,760
It lets go of everything else.

91
00:06:28,760 --> 00:06:31,760
It, it, it rejects some sorrow.

92
00:06:31,760 --> 00:06:36,760
So there is actually a rejection as a part of the attainment of enlightenment.

93
00:06:36,760 --> 00:06:39,760
It's not exactly acceptance.

94
00:06:39,760 --> 00:06:41,760
It's understanding.

95
00:06:41,760 --> 00:06:46,760
I mean, it, it sounds nice about the, and I always tell people to kind of just accept,

96
00:06:46,760 --> 00:06:51,760
but you have to be clear that there's another step and it's the understanding.

97
00:06:51,760 --> 00:06:53,760
Because when you really understand something, you don't accept it.

98
00:06:53,760 --> 00:06:55,760
You give it up.

99
00:06:55,760 --> 00:06:58,760
You just discard it.

100
00:06:58,760 --> 00:07:01,760
I, uh, somewhere in my, in the, in the layers of my mind,

101
00:07:01,760 --> 00:07:05,760
there are some negative things like hatred.

102
00:07:05,760 --> 00:07:11,760
And, uh, actually, what a fight would be reality rather than acceptance.

103
00:07:11,760 --> 00:07:12,760
Yeah.

104
00:07:12,760 --> 00:07:26,760
So, uh, can you get through meditation to that for how you want to, um, like replying your mind,

105
00:07:26,760 --> 00:07:27,760
how to get rid of it.

106
00:07:27,760 --> 00:07:29,760
I mean, it's not getting rid of it.

107
00:07:29,760 --> 00:07:31,760
How to transform it.

108
00:07:31,760 --> 00:07:32,760
Get rid of it.

109
00:07:32,760 --> 00:07:33,760
Get rid of it.

110
00:07:33,760 --> 00:07:34,760
For sure, get rid of it.

111
00:07:34,760 --> 00:07:36,760
You get rid of it through understanding.

112
00:07:36,760 --> 00:07:38,760
Get rid of all the bad stuff.

113
00:07:38,760 --> 00:07:40,760
Through division, right?

114
00:07:40,760 --> 00:07:43,760
The, the, the problem is not trying to get rid of it.

115
00:07:43,760 --> 00:07:46,760
The problem is how you're trying to get rid of it.

116
00:07:46,760 --> 00:07:52,760
Try to get rid of it by, by, by refusing it or by, um,

117
00:07:52,760 --> 00:07:57,760
denying it by rejecting it by, by, um, you know,

118
00:07:57,760 --> 00:07:58,760
opposing it.

119
00:07:58,760 --> 00:07:59,760
No, that's not that you do it.

120
00:07:59,760 --> 00:08:01,760
No, you get rid of it.

121
00:08:01,760 --> 00:08:02,760
No, you get rid of it.

122
00:08:02,760 --> 00:08:04,760
You're just understanding that anger is useless.

123
00:08:04,760 --> 00:08:06,760
That's what you do.

124
00:08:06,760 --> 00:08:11,760
In Buddhism, we don't change, we don't try to change anything.

125
00:08:11,760 --> 00:08:18,760
We're not, you know, the core of Buddhism is not to change anything.

126
00:08:18,760 --> 00:08:22,760
The core is to understand, understand everything.

127
00:08:22,760 --> 00:08:24,760
You know, I, I think,

128
00:08:24,760 --> 00:08:26,760
and so understanding all of all of the reality.

129
00:08:26,760 --> 00:08:28,760
And there is a question.

130
00:08:28,760 --> 00:08:32,760
Why should I, why I have to understand the stupid world

131
00:08:32,760 --> 00:08:36,760
of science to become a great book with me in this situation?

132
00:08:36,760 --> 00:08:38,760
I didn't agree on it.

133
00:08:38,760 --> 00:08:41,760
I didn't, I didn't say I would not be here.

134
00:08:41,760 --> 00:08:43,760
Oh, but you're dead.

135
00:08:43,760 --> 00:08:45,760
You certainly did.

136
00:08:45,760 --> 00:08:46,760
You fired me.

137
00:08:46,760 --> 00:08:47,760
You fired me.

138
00:08:47,760 --> 00:08:48,760
You fired me.

139
00:08:48,760 --> 00:08:49,760
No, I didn't say no.

140
00:08:49,760 --> 00:08:51,760
Yes, but you didn't start it.

141
00:08:51,760 --> 00:08:52,760
So much.

142
00:08:52,760 --> 00:08:55,760
Do you remember, do you remember 100 years ago?

143
00:08:55,760 --> 00:08:56,760
I don't know.

144
00:08:56,760 --> 00:08:58,760
You did 100 years ago.

145
00:08:58,760 --> 00:08:59,760
That's not a very long time.

146
00:08:59,760 --> 00:09:03,760
So you, you don't have very much to go on.

147
00:09:03,760 --> 00:09:05,760
You're not like being a human.

148
00:09:05,760 --> 00:09:07,760
You're not like being a human.

149
00:09:07,760 --> 00:09:13,760
Do you not find food delicious or flowers beautiful?

150
00:09:13,760 --> 00:09:18,760
No, I feel like, I'm 100% sure that I feel like,

151
00:09:18,760 --> 00:09:22,760
I have some, I gave myself some purpose in life.

152
00:09:22,760 --> 00:09:24,760
And I follow my purpose in life.

153
00:09:24,760 --> 00:09:26,760
That's all I know.

154
00:09:26,760 --> 00:09:30,760
But yourself here, if you, if a person does bad deeds,

155
00:09:30,760 --> 00:09:32,760
for example, they put themselves in jail.

156
00:09:32,760 --> 00:09:34,760
So then when they get to the other, I say,

157
00:09:34,760 --> 00:09:35,760
I didn't want to come here.

158
00:09:35,760 --> 00:09:37,760
I didn't, I didn't have anything to do with this.

159
00:09:37,760 --> 00:09:39,760
I didn't say, please put me in jail.

160
00:09:39,760 --> 00:09:40,760
What am I doing here?

161
00:09:40,760 --> 00:09:43,760
Is that reasonable?

162
00:09:43,760 --> 00:09:45,760
No.

163
00:09:45,760 --> 00:09:48,760
I've done nothing wrong.

164
00:09:48,760 --> 00:09:49,760
Okay.

165
00:09:49,760 --> 00:09:54,760
You know, anything wrong, you wouldn't have been reborn.

166
00:09:54,760 --> 00:09:56,760
I couldn't.

167
00:09:56,760 --> 00:09:59,760
You did something wrong because you were reborn.

168
00:09:59,760 --> 00:10:01,760
Sorry.

169
00:10:01,760 --> 00:10:02,760
Just reminds me.

170
00:10:02,760 --> 00:10:03,760
Everybody else.

171
00:10:03,760 --> 00:10:05,760
But what?

172
00:10:05,760 --> 00:10:07,760
This reminds me of an Alan and lots lecture.

173
00:10:07,760 --> 00:10:08,760
Yeah.

174
00:10:08,760 --> 00:10:13,760
Where he talks about being born.

175
00:10:13,760 --> 00:10:17,760
And he touches on that, that thing that's in our core.

176
00:10:17,760 --> 00:10:20,760
Sometimes when we become really angry,

177
00:10:20,760 --> 00:10:21,760
I've even said it at times.

178
00:10:21,760 --> 00:10:24,760
I didn't ask to be born.

179
00:10:24,760 --> 00:10:27,760
And an Alan Watts briefly touches on this.

180
00:10:27,760 --> 00:10:31,760
And he, and he goes, well, of course,

181
00:10:31,760 --> 00:10:33,760
well, of course I did.

182
00:10:33,760 --> 00:10:36,760
I was the, I was the twinkle in my father's eye.

183
00:10:36,760 --> 00:10:37,760
I was a desire.

184
00:10:37,760 --> 00:10:41,760
And I deliberately got involved.

185
00:10:41,760 --> 00:10:44,760
Oh, it's kind of like that.

186
00:10:44,760 --> 00:10:45,760
That's interesting.

187
00:10:45,760 --> 00:10:47,760
I was the twinkle in my father's eye.

188
00:10:47,760 --> 00:10:52,760
Yeah.

189
00:10:52,760 --> 00:10:54,760
That's, that's, that's,

190
00:10:54,760 --> 00:10:55,760
exactly.

191
00:10:55,760 --> 00:10:59,760
I mean, you know,

192
00:10:59,760 --> 00:11:01,760
just, I don't really.

193
00:11:01,760 --> 00:11:03,760
Well, I said everything about it really.

194
00:11:03,760 --> 00:11:05,760
Well, think about where you're going.

195
00:11:05,760 --> 00:11:07,760
Don't, don't think so much about where you're at,

196
00:11:07,760 --> 00:11:11,760
because that's really too subjective for us to be objective about.

197
00:11:11,760 --> 00:11:13,760
But think about where you're headed now.

198
00:11:13,760 --> 00:11:15,760
If you continue the way you are now,

199
00:11:15,760 --> 00:11:18,760
without, and I'm not trying to pick on you,

200
00:11:18,760 --> 00:11:20,760
but if any of us continues the way we are now,

201
00:11:20,760 --> 00:11:23,760
without developing ourselves further,

202
00:11:23,760 --> 00:11:25,760
where do you think you're going to end up?

203
00:11:25,760 --> 00:11:27,760
And then when you look at it like that,

204
00:11:27,760 --> 00:11:29,760
then you should be able to look backwards and say,

205
00:11:29,760 --> 00:11:32,760
oh, well, that's probably what happened in this life.

206
00:11:32,760 --> 00:11:34,760
That's how I got into this life.

207
00:11:34,760 --> 00:11:36,760
Look at what's going to happen.

208
00:11:36,760 --> 00:11:38,760
That's, you know, much easier to see.

209
00:11:38,760 --> 00:11:42,760
Of course you have that rebirth and so on in karma and so on.

210
00:11:42,760 --> 00:11:46,760
But as good as we have a kind of a, you know, working faith

211
00:11:46,760 --> 00:11:49,760
that no, yeah, the Buddha probably knew what he was talking about.

212
00:11:49,760 --> 00:11:50,760
So now, of course,

213
00:11:50,760 --> 00:11:52,760
from your practice meditation and all comes clear

214
00:11:52,760 --> 00:11:54,760
and you understand it for yourself,

215
00:11:54,760 --> 00:11:56,760
but shouldn't be that hard to understand it.

216
00:11:56,760 --> 00:11:59,760
You know, if you do lots and lots of evil things,

217
00:11:59,760 --> 00:12:02,760
well, you're going to have to be reborn again.

218
00:12:02,760 --> 00:12:05,760
Certainly, somebody should become a Buddha.

219
00:12:05,760 --> 00:12:08,760
You know, we're like this to show us

220
00:12:08,760 --> 00:12:13,760
what they're not really like. Yeah, let's find someone to do that.

221
00:12:13,760 --> 00:12:14,760
What?

222
00:12:14,760 --> 00:12:15,760
Yeah.

223
00:12:15,760 --> 00:12:18,760
I pick, I vote on.

224
00:12:18,760 --> 00:12:20,760
Oh, thank you.

225
00:12:20,760 --> 00:12:22,760
Become a Buddha now and show us.

226
00:12:22,760 --> 00:12:23,760
Okay.

227
00:12:23,760 --> 00:12:24,760
Yes.

228
00:12:24,760 --> 00:12:26,760
It's only over so easy.

229
00:12:26,760 --> 00:12:27,760
No.

230
00:12:27,760 --> 00:12:29,760
We need a Buddha definitely.

231
00:12:29,760 --> 00:12:30,760
You hired a Buddha.

232
00:12:30,760 --> 00:12:31,760
This is what they say.

233
00:12:31,760 --> 00:12:34,760
We countless Buddhas have arisen in the past

234
00:12:34,760 --> 00:12:35,760
and we failed.

235
00:12:35,760 --> 00:12:40,760
We failed to take their teachings seriously.

236
00:12:40,760 --> 00:12:43,760
It's not, you can't blame the Buddha for not arising.

237
00:12:43,760 --> 00:12:46,760
The Buddha just arose 2,000,

238
00:12:46,760 --> 00:12:51,760
nearly 2,500 years ago and you blew it.

239
00:12:51,760 --> 00:12:54,760
That's so great.

240
00:12:54,760 --> 00:12:56,760
That's sobering.

241
00:12:56,760 --> 00:12:59,760
What the heck was I doing?

242
00:12:59,760 --> 00:13:02,760
I must have been drunk in a bar somewhere

243
00:13:02,760 --> 00:13:05,760
or living under a rock.

244
00:13:05,760 --> 00:13:07,760
I was probably an earthworm

245
00:13:07,760 --> 00:13:10,760
at the time of the Buddha.

246
00:13:10,760 --> 00:13:15,760
Otherwise, how could you miss that opportunity?

247
00:13:15,760 --> 00:13:19,760
Well, I think we don't believe it possible.

248
00:13:19,760 --> 00:13:22,760
You're actually like, I don't know.

249
00:13:22,760 --> 00:13:27,760
You didn't do anything in the center room picking your nose.

250
00:13:27,760 --> 00:13:29,760
That would be more embarrassing.

251
00:13:29,760 --> 00:13:31,760
At least if you're a worm, you have an excuse.

252
00:13:31,760 --> 00:13:35,760
Well, I was a worm, what do you expect?

253
00:13:35,760 --> 00:13:37,760
There were a lot of monks in the time of the Buddha,

254
00:13:37,760 --> 00:13:41,760
and I think, well, all of them became enlightened.

255
00:13:41,760 --> 00:13:43,760
I think I've got a lot of bad monks in there.

256
00:13:43,760 --> 00:13:48,760
I mean, yeah, talks about it.

257
00:13:48,760 --> 00:13:50,760
Sorry, I just steamrolled over.

258
00:13:50,760 --> 00:13:51,760
What was that about?

259
00:13:51,760 --> 00:13:53,760
What was that question all about?

260
00:13:53,760 --> 00:13:54,760
Forget.

261
00:13:54,760 --> 00:13:58,760
Remember, according to it.

262
00:13:58,760 --> 00:14:06,760
I can't remember what.

263
00:14:06,760 --> 00:14:09,760
The last thing I remember is somebody you mentioned

264
00:14:09,760 --> 00:14:11,760
a question about mind-made body.

265
00:14:11,760 --> 00:14:17,760
Oh, yeah, mind-made body.

266
00:14:17,760 --> 00:14:18,760
I don't know.

267
00:14:18,760 --> 00:14:20,760
Not really worth a video, I suppose.

268
00:14:20,760 --> 00:14:27,760
I always say that, and then it turns out to be an interesting conversation.

269
00:14:27,760 --> 00:14:31,760
Somebody asked a funny question.

270
00:14:31,760 --> 00:14:33,760
We still recording?

271
00:14:33,760 --> 00:14:34,760
No.

272
00:14:34,760 --> 00:14:38,760
But just let me touch on the mind-made body.

273
00:14:38,760 --> 00:14:41,760
The best thing would be to look in the commentaries

274
00:14:41,760 --> 00:14:44,760
and to find someone who really knows what they're talking about.

275
00:14:44,760 --> 00:14:51,760
But it's basically what you find in an outer body experience.

276
00:14:51,760 --> 00:15:00,760
Yeah, it's something akin to having a new body.

277
00:15:00,760 --> 00:15:05,760
Because remember, the body is just a conglomerate of experiences.

278
00:15:05,760 --> 00:15:13,760
So it's understood to be possible for the mind to limit itself

279
00:15:13,760 --> 00:15:19,760
or limit its bodily expression to this state

280
00:15:19,760 --> 00:15:23,760
or to sequester itself, at least in an ethereal body.

281
00:15:23,760 --> 00:15:26,760
It's very difficult in most people aren't able.

282
00:15:26,760 --> 00:15:28,760
But it's fairly common.

283
00:15:28,760 --> 00:15:31,760
You have all this anecdotal talk about outer body experiences

284
00:15:31,760 --> 00:15:36,760
where you feel like you're in a body, but you're floating around.

285
00:15:36,760 --> 00:15:40,760
But I don't have much to talk about.

286
00:15:40,760 --> 00:15:42,760
Astro projection.

287
00:15:42,760 --> 00:15:47,760
Where is Kapila from Japan?

288
00:15:47,760 --> 00:15:53,760
Dude, I don't have a clue what you're talking about.

289
00:15:53,760 --> 00:15:55,760
He was in the couple of the groups.

290
00:15:55,760 --> 00:15:57,760
He was in the couple of the groups before.

291
00:15:57,760 --> 00:15:58,760
Really? Ah, Kapila.

292
00:15:58,760 --> 00:15:59,760
Ah, sorry.

293
00:15:59,760 --> 00:16:01,760
At this thinking about my meditators here from Japan,

294
00:16:01,760 --> 00:16:03,760
I don't have anyone from Japan.

295
00:16:03,760 --> 00:16:06,760
Kapila from Sri Lanka lives in Japan.

296
00:16:06,760 --> 00:16:08,760
Don't know.

297
00:16:08,760 --> 00:16:12,760
He's probably very busy.

298
00:16:12,760 --> 00:16:14,760
Japanese people seem to always be.

299
00:16:14,760 --> 00:16:18,760
No, that's horrible.

300
00:16:18,760 --> 00:16:20,760
I don't know.

301
00:16:20,760 --> 00:16:23,760
I think he must have lost contact because he knew that we'd stopped

302
00:16:23,760 --> 00:16:31,760
and just didn't get back into it.

303
00:16:31,760 --> 00:16:32,760
Comment.

304
00:16:32,760 --> 00:16:33,760
Do we have questions coming up?

305
00:16:33,760 --> 00:16:34,760
How are we doing?

306
00:16:34,760 --> 00:16:39,760
Maybe we should quit while we're at 10 p.m.

307
00:16:39,760 --> 00:16:49,760
Nice to see Lamma still with us in the comments.

308
00:16:49,760 --> 00:16:58,760
Our new people coming to the world.

309
00:16:58,760 --> 00:16:59,760
We've been over this before.

310
00:16:59,760 --> 00:17:02,760
I don't know that I've ever did a real deal on that.

311
00:17:02,760 --> 00:17:04,760
It's not that difficult to understand.

312
00:17:04,760 --> 00:17:11,760
I mean, lots of lots of insects kind of thing.

313
00:17:11,760 --> 00:17:21,760
When we escape something, it's because we are afraid of it.

314
00:17:21,760 --> 00:17:40,760
Is it really that bad to run away from your problems?

315
00:17:40,760 --> 00:17:45,760
I don't think so unless you did something very bad.

316
00:17:45,760 --> 00:17:51,760
That's the point that you cannot escape it like that that's going to everywhere,

317
00:17:51,760 --> 00:17:55,760
stop when you surround you and we cannot escape it.

318
00:17:55,760 --> 00:17:57,760
It wouldn't be here.

319
00:17:57,760 --> 00:18:02,760
I think his premise is not as invalid that we escape something

320
00:18:02,760 --> 00:18:05,760
because we are scared of suffering.

321
00:18:05,760 --> 00:18:10,760
There's something in there.

322
00:18:10,760 --> 00:18:14,760
I don't think we escape things because of fear.

323
00:18:14,760 --> 00:18:22,760
We've already said that you escape things based on knowledge, understanding.

324
00:18:22,760 --> 00:18:26,760
The question you have to ask in response is when you run away from your problems,

325
00:18:26,760 --> 00:18:29,760
do you come to understand them better?

326
00:18:29,760 --> 00:18:32,760
That's, I think, the most pertinent question here.

327
00:18:32,760 --> 00:18:36,760
Because if running away helps you understand reality better,

328
00:18:36,760 --> 00:18:37,760
then run away.

329
00:18:37,760 --> 00:18:38,760
Leave.

330
00:18:38,760 --> 00:18:40,760
That's why I say running away from society.

331
00:18:40,760 --> 00:18:41,760
Wonderful thing.

332
00:18:41,760 --> 00:18:45,760
Why? Because you have much more time and freedom to contemplate

333
00:18:45,760 --> 00:18:49,760
what's really important to understand what really exists

334
00:18:49,760 --> 00:18:54,760
instead of getting caught up in all sorts of concepts and conventions and so on.

335
00:18:54,760 --> 00:19:02,760
But if you think running away is just for the purpose of not having to deal with that,

336
00:19:02,760 --> 00:19:05,760
which you're running away from and that's the only reason for running away,

337
00:19:05,760 --> 00:19:09,760
then you're always going to be running away from everything.

338
00:19:09,760 --> 00:19:11,760
Is it really that bad?

339
00:19:11,760 --> 00:19:13,760
Well, it just makes you someone who runs.

340
00:19:13,760 --> 00:19:15,760
Someone who runs away from things.

341
00:19:15,760 --> 00:19:19,760
I don't know. Do you like being someone who just habitually runs away from things?

342
00:19:19,760 --> 00:19:20,760
I don't know.

343
00:19:20,760 --> 00:19:22,760
Yeah, I'll tell you something.

344
00:19:22,760 --> 00:19:28,760
That question, I'm talking about talking about the state of question.

345
00:19:28,760 --> 00:19:30,760
What is we're running away?

346
00:19:30,760 --> 00:19:42,760
For example, I used to live with households using meeting meets products and animal products,

347
00:19:42,760 --> 00:19:46,760
and I was terrified that I'm going to be cross-contaminated with this.

348
00:19:46,760 --> 00:19:50,760
And I was like renting my own place.

349
00:19:50,760 --> 00:19:54,760
I was changing houses and so on and so on.

350
00:19:54,760 --> 00:20:01,760
Because I couldn't run anyway.

351
00:20:01,760 --> 00:20:08,760
Every third Saturday in my area, there is a farmer's market,

352
00:20:08,760 --> 00:20:11,760
and there's a lot of meat and stuff around me,

353
00:20:11,760 --> 00:20:14,760
and so I just can't run away from it.

354
00:20:14,760 --> 00:20:17,760
I wish to live in a different world like that.

355
00:20:17,760 --> 00:20:24,760
So, you know, you can try to escape your suffering.

356
00:20:24,760 --> 00:20:28,760
Try to escape this.

357
00:20:28,760 --> 00:20:32,760
So geographically, it's impossible.

358
00:20:32,760 --> 00:20:37,760
Even if I live with vegetarians now, so I have like,

359
00:20:37,760 --> 00:20:40,760
okay, that's the life I can live.

360
00:20:40,760 --> 00:20:44,760
But I don't have thousands of other things I don't like.

361
00:20:44,760 --> 00:20:47,760
So I'm not able to run away.

362
00:20:47,760 --> 00:20:48,760
I don't like people.

363
00:20:48,760 --> 00:20:50,760
I don't like most of the people.

364
00:20:50,760 --> 00:20:53,760
I don't like people.

365
00:20:53,760 --> 00:20:59,760
I don't like people.

366
00:20:59,760 --> 00:21:02,760
No, I think the key here, this question is that

367
00:21:02,760 --> 00:21:04,760
you don't escape suffering out of fear.

368
00:21:04,760 --> 00:21:06,760
Look at the four noble truths.

369
00:21:06,760 --> 00:21:09,760
The very, very, very, very, very core Buddhism.

370
00:21:09,760 --> 00:21:14,760
Yeah, you actually have a lot of anger, anger, just kills fear.

371
00:21:14,760 --> 00:21:17,760
You're not afraid.

372
00:21:17,760 --> 00:21:20,760
Fear is a type of anger.

373
00:21:20,760 --> 00:21:25,760
But the point, the point is that the first noble truth is,

374
00:21:25,760 --> 00:21:27,760
is understanding suffering.

375
00:21:27,760 --> 00:21:33,760
We have to get that through our skull, our head.

376
00:21:33,760 --> 00:21:40,760
Think about that. If the first noble truth is to understand suffering,

377
00:21:40,760 --> 00:21:46,760
then it's antithetical to Buddhism to run away from suffering.

378
00:21:46,760 --> 00:21:49,760
It's not just off track.

379
00:21:49,760 --> 00:21:52,760
It's the exact opposite of learning about it

380
00:21:52,760 --> 00:21:54,760
and coming to understand it.

381
00:21:54,760 --> 00:21:57,760
Can you understand something by running away from it?

382
00:21:57,760 --> 00:21:59,760
Can you ever hope to understand it?

383
00:21:59,760 --> 00:22:03,760
But it's not just understand, but understand completely.

384
00:22:03,760 --> 00:22:07,760
Body means, like Bhary Nivana, but Bhary is like a circle.

385
00:22:07,760 --> 00:22:12,760
It means all around, or it just means completely.

386
00:22:12,760 --> 00:22:14,760
Bhary Nyaya.

387
00:22:14,760 --> 00:22:17,760
Nyaya means should be understood.

388
00:22:17,760 --> 00:22:22,760
Bhary Nyayaan, that is to be understood completely.

389
00:22:22,760 --> 00:22:28,760
So that implies something very, very different from running away from suffering.

390
00:22:28,760 --> 00:22:33,760
Think about that, because we always hear Buddhism teaches the escape from suffering

391
00:22:33,760 --> 00:22:36,760
or freedom from suffering.

392
00:22:36,760 --> 00:22:39,760
But that's not the path that leads to the freedom from suffering.

393
00:22:39,760 --> 00:22:41,760
The path that leads to freedom from suffering

394
00:22:41,760 --> 00:22:47,760
is to investigate and to be very much involved in the observation

395
00:22:47,760 --> 00:22:52,760
and investigation of suffering in order to understand it fully.

396
00:22:52,760 --> 00:22:57,760
How can you understand something fully without observing it quite closely?

397
00:22:57,760 --> 00:23:00,760
So it sounds like it's going to be a lot of suffering.

398
00:23:00,760 --> 00:23:04,760
Even like this, if the more suffering, if you more realize

399
00:23:04,760 --> 00:23:09,760
that you cannot escape suffering, to the more you give up.

400
00:23:09,760 --> 00:23:11,760
Does that make sense?

401
00:23:11,760 --> 00:23:14,760
Yeah, very much.

402
00:23:14,760 --> 00:23:18,760
In fact, realizing that you can't escape is a very important part.

403
00:23:18,760 --> 00:23:25,760
So basically you can't escape, but that you can't avoid

404
00:23:25,760 --> 00:23:26,760
it.

405
00:23:26,760 --> 00:23:28,760
Yeah, you can't avoid helps you give up.

406
00:23:28,760 --> 00:23:29,760
And then you really do escape.

407
00:23:29,760 --> 00:23:34,760
You escape by not engaging with it.

408
00:23:34,760 --> 00:23:37,760
It's there, but you don't engage in it.

409
00:23:37,760 --> 00:23:41,760
In the end, this just leads to complete disengagement of Nibbana

410
00:23:41,760 --> 00:23:45,760
or the mind pulls back, comes inside.

411
00:23:45,760 --> 00:23:48,760
And so there's no rising.

412
00:23:48,760 --> 00:23:50,760
Yeah, that's pretty cool.

413
00:23:50,760 --> 00:23:51,760
That's pretty cool.

414
00:23:51,760 --> 00:23:52,760
He just stopped.

415
00:23:52,760 --> 00:23:59,760
Because when you give up something, you actually feel happy.

416
00:23:59,760 --> 00:24:02,760
Because in the beginning, you don't feel happy.

417
00:24:02,760 --> 00:24:05,760
After a while, you feel happy.

418
00:24:05,760 --> 00:24:07,760
So that's cool.

419
00:24:07,760 --> 00:24:10,760
You don't feel happy when you're not really giving it up.

420
00:24:10,760 --> 00:24:12,760
When you're still wanting it.

421
00:24:12,760 --> 00:24:17,760
Once you see that one thing, it's suffering and once you understand

422
00:24:17,760 --> 00:24:21,760
clearly the state of one thing, then you just get bored of it.

423
00:24:21,760 --> 00:24:23,760
Really, Buddhism is not about escaping.

424
00:24:23,760 --> 00:24:28,760
If you're about getting bored, getting disenchanted or uninterested,

425
00:24:28,760 --> 00:24:33,760
losing your desire for it.

426
00:24:33,760 --> 00:24:34,760
Yeah, that's cool.

427
00:24:34,760 --> 00:24:35,760
I can't.

428
00:24:35,760 --> 00:24:37,760
Well, I don't know.

429
00:24:37,760 --> 00:24:40,760
I had like a hard time today.

430
00:24:40,760 --> 00:24:42,760
It's my anxiety.

431
00:24:42,760 --> 00:24:44,760
It happens to me once a month.

432
00:24:44,760 --> 00:24:45,760
I guess.

433
00:24:45,760 --> 00:24:49,760
Something like, I'm totally in pieces.

434
00:24:49,760 --> 00:24:50,760
Totally give up.

435
00:24:50,760 --> 00:24:53,760
Cool, but then you just give up.

436
00:24:53,760 --> 00:24:54,760
See it?

437
00:24:54,760 --> 00:24:55,760
It's just anxiety.

438
00:24:55,760 --> 00:24:57,760
Be with it.

439
00:24:57,760 --> 00:24:58,760
Yeah.

440
00:24:58,760 --> 00:24:59,760
So.

441
00:24:59,760 --> 00:25:00,760
Doesn't.

442
00:25:00,760 --> 00:25:02,760
But yeah, really a problem.

443
00:25:02,760 --> 00:25:05,760
It just is what it is.

444
00:25:05,760 --> 00:25:07,760
Yeah.

445
00:25:07,760 --> 00:25:10,760
And you can.

446
00:25:10,760 --> 00:25:11,760
Yeah, look.

447
00:25:11,760 --> 00:25:12,760
Yeah.

448
00:25:12,760 --> 00:25:16,760
It's something that you have to accept.

449
00:25:16,760 --> 00:25:17,760
You can do it.

450
00:25:17,760 --> 00:25:22,760
I have to accept the.

451
00:25:22,760 --> 00:25:24,760
That's going to happen to me.

452
00:25:24,760 --> 00:25:26,760
And I'm not going to be happy.

453
00:25:26,760 --> 00:25:27,760
I'm not.

454
00:25:27,760 --> 00:25:30,760
I might not be satisfied.

455
00:25:30,760 --> 00:25:32,760
I will be to tell.

456
00:25:32,760 --> 00:25:33,760
I'm sorry.

457
00:25:33,760 --> 00:25:36,760
Stop looking for satisfaction.

458
00:25:36,760 --> 00:25:38,760
Stop looking for a solution.

459
00:25:38,760 --> 00:25:42,760
Stop looking for an answer or a fix.

460
00:25:42,760 --> 00:25:46,760
That's really the hardest thing, but it's the only answer.

461
00:25:46,760 --> 00:25:48,760
Stop looking for a solution.

462
00:25:48,760 --> 00:25:51,760
I had to stop looking for an answer.

463
00:25:51,760 --> 00:25:55,760
Just to see, yes, it is suffering.

464
00:25:55,760 --> 00:25:57,760
I'm to accept that.

465
00:25:57,760 --> 00:25:58,760
You accept, that's interesting.

466
00:25:58,760 --> 00:25:59,760
That's what you accept.

467
00:25:59,760 --> 00:26:01,760
You accept the fact that it is suffering.

468
00:26:01,760 --> 00:26:04,760
Doesn't mean you accept the thing that it is causing you suffering

469
00:26:04,760 --> 00:26:06,760
and say, well, then I'll just suffer forever.

470
00:26:06,760 --> 00:26:07,760
That's fine.

471
00:26:07,760 --> 00:26:09,760
But you accept the fact that it's suffering.

472
00:26:09,760 --> 00:26:12,760
And that in that sense, not going to make you happy and not

473
00:26:12,760 --> 00:26:13,760
worth clinging to.

474
00:26:13,760 --> 00:26:16,760
Not worth even worrying about.

475
00:26:16,760 --> 00:26:19,760
And then you let the object you just discard it.

476
00:26:19,760 --> 00:26:21,760
You say, it is.

477
00:26:21,760 --> 00:26:44,760
It is suffering.

