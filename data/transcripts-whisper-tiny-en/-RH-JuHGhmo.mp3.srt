1
00:00:00,000 --> 00:00:04,320
In the past, please could you define what the mind is?

2
00:00:04,320 --> 00:00:07,360
Is it just an awareness of the present moment?

3
00:00:07,360 --> 00:00:12,080
Also, if we believe that the mind nor nothing else

4
00:00:12,080 --> 00:00:16,240
continues in further rebirths, how do we know the rebirth exists?

5
00:00:20,640 --> 00:00:24,640
Well, okay, lots of questions there.

6
00:00:24,640 --> 00:00:27,280
The first one, the mind is knowing,

7
00:00:27,280 --> 00:00:32,400
mind is that which knows, a rock and a human being are different,

8
00:00:32,400 --> 00:00:36,400
but the only intrinsic difference is the mind

9
00:00:36,400 --> 00:00:39,040
a rock doesn't know anything,

10
00:00:39,040 --> 00:00:43,680
it doesn't have awareness, not that we know of anyway,

11
00:00:43,680 --> 00:00:48,160
but in our own experience, we can see that there are two parts,

12
00:00:48,160 --> 00:00:51,440
there's the physical and there's the mental, the mentalist that

13
00:00:51,440 --> 00:00:57,440
which knows, mind is the experience of knowing.

14
00:00:57,440 --> 00:01:02,000
Maybe you want to take the other part?

15
00:01:02,000 --> 00:01:07,360
What is it that goes from one life to the next?

16
00:01:07,360 --> 00:01:10,000
How do we know that rebirth exists, sorry?

17
00:01:10,000 --> 00:01:16,640
I think we talked about it earlier in earlier videos.

18
00:01:16,640 --> 00:01:27,120
Yes, first you have to have a little bit of faith,

19
00:01:27,120 --> 00:01:29,920
not a belief, but a little bit of faith,

20
00:01:29,920 --> 00:01:32,640
certainly is not necessary.

21
00:01:32,640 --> 00:01:42,720
And then you start to investigate what the Buddha said,

22
00:01:42,720 --> 00:01:52,080
and he is talking about rebirth and the law of action,

23
00:01:52,080 --> 00:02:03,440
and that everything has a cause and a result of the cause.

24
00:02:03,440 --> 00:02:11,440
So when you know these things and you try to meditate

25
00:02:11,440 --> 00:02:17,520
on in the present moment, being the present moment,

26
00:02:17,520 --> 00:02:20,960
and you are mindful in your daily life,

27
00:02:20,960 --> 00:02:25,600
then you will notice that in the short term,

28
00:02:25,600 --> 00:02:32,080
from only this life, you will notice that there is

29
00:02:32,080 --> 00:02:37,040
this kind of comma existing happening

30
00:02:37,040 --> 00:02:42,400
that what you do has a result,

31
00:02:42,400 --> 00:02:46,720
that nothing what you do or say or even think

32
00:02:46,720 --> 00:02:49,680
can be without a result.

33
00:02:49,680 --> 00:02:55,680
And the result comes either from within you or from outside of you.

34
00:02:55,680 --> 00:03:04,720
So you can this, what you understand in daily life,

35
00:03:04,720 --> 00:03:13,600
just transfer if you want to the larger scale

36
00:03:13,600 --> 00:03:20,080
in terms to understand it on the conventional level.

37
00:03:20,080 --> 00:03:34,640
Now there is the other level,

38
00:03:34,640 --> 00:03:39,360
no, I forgot the word, it's not the unconventional level, of course,

39
00:03:39,360 --> 00:03:49,200
ultimate, ultimate, the ultimate level.

40
00:03:49,200 --> 00:04:05,040
There you would say that there is nothing that exists

41
00:04:05,040 --> 00:04:11,600
truly, but that there is still something continuing.

42
00:04:11,600 --> 00:04:19,360
We have the other time the example of an apple

43
00:04:19,360 --> 00:04:24,880
that is eaten and the seed falls on the ground and grows.

44
00:04:24,880 --> 00:04:35,040
And then from the seed, when the conditions are in favor,

45
00:04:35,040 --> 00:04:36,960
another apple will grow.

46
00:04:36,960 --> 00:04:42,080
So you can't say that one apple is the same as the other.

47
00:04:42,080 --> 00:04:44,800
And you can't say that one apple is

48
00:04:44,800 --> 00:04:50,480
mother or father to the other, but still there is something

49
00:04:50,480 --> 00:05:00,400
that is inherent in the first apple,

50
00:05:00,400 --> 00:05:04,400
which makes the second apple to arise

51
00:05:04,400 --> 00:05:12,480
without the first apple, the second apple wouldn't be there.

52
00:05:12,480 --> 00:05:20,400
And so you understand by thinking of this example

53
00:05:20,400 --> 00:05:26,640
that there needs to be a cause, a reason, something,

54
00:05:26,640 --> 00:05:30,800
and then something happens.

55
00:05:30,800 --> 00:05:37,280
In short term or in large term, in short scale or in large scale.

56
00:05:37,280 --> 00:05:46,720
So when you when you think from from one life to the next

57
00:05:46,720 --> 00:05:52,080
or then don't start to consider or to ponder over

58
00:05:52,080 --> 00:05:57,120
or when did it start, the Buddha really

59
00:05:57,120 --> 00:06:01,280
discouraged to ask these questions because they don't

60
00:06:01,280 --> 00:06:03,040
lead to enlightenment.

61
00:06:03,040 --> 00:06:08,080
But if you accept, there must be, must have been

62
00:06:08,080 --> 00:06:11,920
a beginning somewhere, must have started.

63
00:06:11,920 --> 00:06:17,440
And from then on, it evolved and continued.

64
00:06:17,440 --> 00:06:20,320
You just can't imagine yourself.

65
00:06:20,320 --> 00:06:22,880
No, that's wrong, not yourself.

66
00:06:22,880 --> 00:06:33,200
But you can imagine something there, which

67
00:06:33,200 --> 00:06:37,200
is called in poly terms, bhavanga.

68
00:06:37,200 --> 00:06:40,000
There is the Sotabhawanga in the Chitabhawanga.

69
00:06:40,000 --> 00:06:46,880
This is the either the some live stream Sotabhawanga.

70
00:06:46,880 --> 00:06:52,840
We would say more like it's like a live stream or Chitabhawanga.

71
00:06:52,840 --> 00:06:59,160
It's more like the subconsciousness.

72
00:06:59,160 --> 00:07:07,360
So this is there like a stream.

73
00:07:07,360 --> 00:07:13,960
And the moment you notice something

74
00:07:13,960 --> 00:07:16,160
and it comes to consciousness, it would

75
00:07:16,160 --> 00:07:19,800
be like a pearl that you put on the stream,

76
00:07:19,800 --> 00:07:25,600
that you put on the live stream.

77
00:07:25,600 --> 00:07:37,720
And so one bead is there first and then comes the next.

78
00:07:37,720 --> 00:07:39,320
And it's building up.

79
00:07:39,320 --> 00:07:41,760
And the first bead.

80
00:07:41,760 --> 00:07:47,880
And what is maybe stored is not a good word for it,

81
00:07:47,880 --> 00:08:01,520
because there is no storage room, but what there is in,

82
00:08:01,520 --> 00:08:09,800
not existing, that will be a base

83
00:08:09,800 --> 00:08:15,440
for what is going on from one life to the other life.

84
00:08:15,440 --> 00:08:21,240
So you don't need to believe that.

85
00:08:21,240 --> 00:08:32,880
And when you meditate, you may find out that it is like that.

86
00:08:32,880 --> 00:08:38,560
Well, one just one other thing if I can just briefly touch

87
00:08:38,560 --> 00:08:43,200
on this idea of how do we know?

88
00:08:43,200 --> 00:08:48,120
Because it's interesting because you're kind of giving,

89
00:08:48,120 --> 00:08:53,160
well, it's kind of not related the idea that,

90
00:08:53,160 --> 00:08:56,160
or there's something obvious that sticks out to me here,

91
00:08:56,160 --> 00:08:58,280
that the idea that the mind doesn't

92
00:08:58,280 --> 00:09:00,560
continue in further rebirth.

93
00:09:00,560 --> 00:09:03,440
The truth is the mind doesn't continue from one moment

94
00:09:03,440 --> 00:09:05,840
to the next.

95
00:09:05,840 --> 00:09:09,960
It arises and sees its mind is just knowing.

96
00:09:09,960 --> 00:09:15,600
And so in that case, I could turn the tables and ask you,

97
00:09:15,600 --> 00:09:20,440
how do you know that the next moment is going to exist?

98
00:09:20,440 --> 00:09:27,240
Or at least we can say, by the same logic,

99
00:09:27,240 --> 00:09:29,760
that we know that the next moment exists

100
00:09:29,760 --> 00:09:38,920
or by the same premise, the same through the observation

101
00:09:38,920 --> 00:09:42,080
that one mind can create another mind

102
00:09:42,080 --> 00:09:47,920
and can in fact create a habitual string of minds.

103
00:09:47,920 --> 00:09:50,520
So for instance, the cultivation of anger

104
00:09:50,520 --> 00:09:53,000
will lead to the habit of anger.

105
00:09:53,000 --> 00:09:54,720
When people come to practice meditation,

106
00:09:54,720 --> 00:09:58,600
they find intense anger coming out from the past.

107
00:09:58,600 --> 00:10:01,920
And even though they know that it's wrong

108
00:10:01,920 --> 00:10:07,320
and they're not keen to be angry, they can't stop the anger

109
00:10:07,320 --> 00:10:10,000
because of the habit.

110
00:10:10,000 --> 00:10:12,280
So we can see that this is going on.

111
00:10:12,280 --> 00:10:17,520
So the idea that this might occur at the moment of death

112
00:10:17,520 --> 00:10:24,120
and that there might as a result of some power of one mind

113
00:10:24,120 --> 00:10:26,120
cause another mind in a different way.

114
00:10:26,120 --> 00:10:29,800
Or in some other way, it's not really a far lead.

115
00:10:29,800 --> 00:10:32,240
The mind is able to create further minds.

116
00:10:32,240 --> 00:10:36,920
The theory of it is simply that the moment of death

117
00:10:36,920 --> 00:10:40,840
is somewhat special, because if there is no creative force

118
00:10:40,840 --> 00:10:46,840
in the mind, then there will not arise,

119
00:10:46,840 --> 00:10:48,680
there can't arise the next mind.

120
00:10:48,680 --> 00:10:51,520
So if someone is enlightened and as an Arahat,

121
00:10:51,520 --> 00:10:55,120
at that moment, there will be the cessation of this stream

122
00:10:55,120 --> 00:10:59,920
of consciousness, and there will be no further arising.

123
00:10:59,920 --> 00:11:03,240
Throughout our lives, there are many kinds of minds

124
00:11:03,240 --> 00:11:07,080
that can arise, not only mind-created minds.

125
00:11:07,080 --> 00:11:11,000
A body-created minds are also on many, many different causes.

126
00:11:11,000 --> 00:11:14,840
And during our life, as Balanini was explaining about Bhobanga

127
00:11:14,840 --> 00:11:17,200
and so on, this is caused by various forces,

128
00:11:17,200 --> 00:11:18,840
not just the mind.

129
00:11:18,840 --> 00:11:20,760
So it's not just one mind that creates the next,

130
00:11:20,760 --> 00:11:23,320
but the mind has the power to do this.

131
00:11:23,320 --> 00:11:27,880
And at the moment of death, if there is no such impulse,

132
00:11:27,880 --> 00:11:31,080
no such volition in the mind, then all the other forces

133
00:11:31,080 --> 00:11:37,920
stop working, and because this final potential cause

134
00:11:37,920 --> 00:11:39,480
isn't there, then there's no rebirth.

135
00:11:39,480 --> 00:11:42,880
But if there still is craving at the moment of death,

136
00:11:42,880 --> 00:11:46,200
that is a sufficient cause to create a future mind,

137
00:11:46,200 --> 00:11:50,200
even without the physical causes of consciousness and so on.

138
00:11:50,200 --> 00:11:51,840
The body can create consciousness.

139
00:11:51,840 --> 00:11:53,760
If I'm not wrong, there's many.

140
00:11:53,760 --> 00:11:55,240
The body can create consciousness.

141
00:11:55,240 --> 00:11:59,440
Consciousness can create physical reality and so on.

142
00:11:59,440 --> 00:12:01,760
But at any rate, there are many causes of consciousness,

143
00:12:01,760 --> 00:12:05,400
not all of which are previous consciousness.

144
00:12:05,400 --> 00:12:11,080
So it really is just a continuation or another example

145
00:12:11,080 --> 00:12:14,400
of what we already see mind-creating mind.

146
00:12:14,400 --> 00:12:16,520
So it's not really difficult to understand.

147
00:12:16,520 --> 00:12:19,400
I don't think or to, obviously, we don't know.

148
00:12:19,400 --> 00:12:21,560
You have to wait till you die to find out for sure,

149
00:12:21,560 --> 00:12:24,400
but it's not categorically different

150
00:12:24,400 --> 00:12:27,720
from what is happening already.

151
00:12:27,720 --> 00:12:31,960
I was just thinking of computers and how they work.

152
00:12:31,960 --> 00:12:35,760
Like, for me, it's a miracle.

153
00:12:35,760 --> 00:12:38,640
I do not understand how that works.

154
00:12:42,160 --> 00:12:44,840
The information that we put in here,

155
00:12:44,840 --> 00:12:48,880
or what we say here, or what I type sometimes.

156
00:12:50,360 --> 00:12:54,040
Transforms, I don't know, I have heard in ones and zeros,

157
00:12:54,040 --> 00:12:56,560
and endless lines of ones and zeros,

158
00:12:56,560 --> 00:12:58,480
and then it goes out somewhere,

159
00:12:58,480 --> 00:13:03,480
and gets to another point, and is there.

160
00:13:06,400 --> 00:13:13,880
And I want to take this as an example that people,

161
00:13:13,880 --> 00:13:16,120
before computer would have exist,

162
00:13:16,120 --> 00:13:19,600
would not have believed that it is possible.

163
00:13:19,600 --> 00:13:24,240
And I do not believe it's possible, but still,

164
00:13:24,240 --> 00:13:27,320
it is possible.

165
00:13:27,320 --> 00:13:30,560
Just I don't know how that works.

166
00:13:30,560 --> 00:13:33,520
That's a really good explanation for people

167
00:13:33,520 --> 00:13:37,240
wondering about many of life's psychic phenomena,

168
00:13:37,240 --> 00:13:44,120
or phenomena, and things that go on in the mind.

169
00:13:44,120 --> 00:13:45,160
Very good example.

170
00:13:45,160 --> 00:13:48,680
We don't know how it works, but who are we?

171
00:13:48,680 --> 00:13:53,240
Or we don't believe it, or we don't have any proof, but what?

172
00:13:53,240 --> 00:13:56,040
And in the meantime, that is an important point.

173
00:13:56,040 --> 00:14:01,160
It's not stored somewhere, the zeros, ones, and zeros

174
00:14:01,160 --> 00:14:05,720
are just somewhere in space, or wherever.

175
00:14:05,720 --> 00:14:07,960
They are not existing.

176
00:14:07,960 --> 00:14:10,040
They are not really existing.

177
00:14:10,040 --> 00:14:13,360
And so is the mind, not really existing.

178
00:14:13,360 --> 00:14:18,360
And we try to have a mind that is something.

179
00:14:18,360 --> 00:14:22,280
And we really have to get rid of that thought.

180
00:14:22,280 --> 00:14:28,840
We have to let go of the idea that the mind is something.

181
00:14:28,840 --> 00:14:35,840
It is just like the ones and zeros, nothing.

182
00:14:35,840 --> 00:14:53,400
But still, there is something in the end comes out.

