1
00:00:00,000 --> 00:00:06,300
I focus on the nostril, instead of the rising and falling of the valley, since I've started meditation.

2
00:00:06,300 --> 00:00:09,000
Any other any downsides?

3
00:00:09,000 --> 00:00:23,300
I've never used the nostrils, so I'm going, either by hearsay or by, once the word, conjecture.

4
00:00:23,300 --> 00:00:40,800
By hearsay, we see Demaga says that the nostrils or mindfulness of breathing becomes more and more refined as you go.

5
00:00:40,800 --> 00:00:57,800
But the only reason, categorical reason, reason for a decisive reason that was given for using the stomach, is that in Burma,

6
00:00:57,800 --> 00:01:05,300
the in Burma mindfulness of breathing is understood to take place at the nose.

7
00:01:05,300 --> 00:01:10,100
And mindfulness of breathing is considered a summit of meditation.

8
00:01:10,100 --> 00:01:18,300
It's considered a meditation that starts off leading you through the janas, and later on is used to, can be used to bring about Vipassana.

9
00:01:18,300 --> 00:01:31,700
But technically, it's the Jai-do-Vimuthi, the person who practices summit to first, and then Vipassana.

10
00:01:31,700 --> 00:01:39,900
In our technique, we practice starting with Vipassana, so it was simply in order to avoid the accusation.

11
00:01:39,900 --> 00:01:41,400
These are the words of Mahasi Sayadu.

12
00:01:41,400 --> 00:01:49,200
In order to avoid the accusation that he was teaching samata, he switched to, and it may not have been him, it may have been his teacher,

13
00:01:49,200 --> 00:01:54,700
but they switched to, he says, that they switched to the stomach.

14
00:01:54,700 --> 00:02:13,700
The reason for it, the reason for needing to use, or for deciding to use the stomach, is that the Visudimaga says that a person who practices Vipassana should start by focusing on the four elements,

15
00:02:13,700 --> 00:02:19,200
which are the building blocks that make up the physical aspect of experience.

16
00:02:19,200 --> 00:02:28,200
So the earth element is the feeling of hardness and softness. The air element is the feeling of pressure or fluidity.

17
00:02:28,200 --> 00:02:42,200
The fire element is the feeling of heat and cold, and the water element is the cohesion that keeps things together, or that makes things stick together.

18
00:02:42,200 --> 00:02:45,200
In this case, this is the air element.

19
00:02:45,200 --> 00:02:54,200
That's regards to the texts and theory, and as I said here say, so a textual argument.

20
00:02:54,200 --> 00:03:06,200
Conjecture, it seems to me that because this one becomes more and more refined, and based on, I guess also here say what I've heard from meditators,

21
00:03:06,200 --> 00:03:18,200
watching the nostril can be quite soothing, and as a result of it getting more and more refined, there's the tendency, or there appears to be the tendency to lead towards samata.

22
00:03:18,200 --> 00:03:31,200
A person who practices mindfulness at the nostrils, I would conjecture is more likely, and from what I've seen and discussed with meditators who insist on using it, more likely to lead to samata meditation.

23
00:03:31,200 --> 00:03:41,200
It's not wrong, it just tends to be more round about, and then you have to eventually get the meditator to focus it on it as ultimate reality.

24
00:03:41,200 --> 00:03:52,200
But it would generally be after they've already entered into the janas, which takes time.

25
00:03:52,200 --> 00:04:21,200
The stomach, on the other hand, being a course and rather large object is subject to, well, is an obvious, or leads to the clear awareness of ultimate reality.

26
00:04:21,200 --> 00:04:29,200
And there's no opportunity for the arising of calm watching the stomach, and this is a problem that people have with this meditation.

27
00:04:29,200 --> 00:04:35,200
They think it's wrong, they think it's bad, they think it's a problem, because they don't feel calm, because they don't feel peaceful.

28
00:04:35,200 --> 00:04:42,200
But watching the stomach, you're going to see impermanence suffering and non-self quite quickly.

29
00:04:42,200 --> 00:04:55,200
You're going to get quickly into issues of having to let go or suffer. It's a question of leading directly to vipassana.

30
00:04:55,200 --> 00:05:05,200
So, it's not that there's disadvantages, it's just a tendency to lead to samata first, lead to tranquility first.

31
00:05:05,200 --> 00:05:14,200
Whereas, the practice that we do is more like insight first and subsequent tranquility.

