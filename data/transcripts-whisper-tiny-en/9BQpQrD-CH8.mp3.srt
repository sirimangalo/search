1
00:00:00,000 --> 00:00:15,000
Hey everyone, I'm going to try the idea of uploading short videos where I talk to you about something that is happening in my life or something that I think is important that I want to let you know about.

2
00:00:15,000 --> 00:00:22,000
So, kind of like video logging. I've done it before. I'm going to try it again and see how it goes.

3
00:00:22,000 --> 00:00:30,000
So, tonight I wanted to talk to you about two things just for those of you who weren't aware.

4
00:00:30,000 --> 00:00:50,000
The first is that based on the request of one of my followers, I've found out that there is a YouTube feature that now allows you to allow

5
00:00:50,000 --> 00:00:58,000
to submit subtitles for your videos. So, people have asked before about submitting subtitles, translations and other languages.

6
00:00:58,000 --> 00:01:13,000
And as far as I know, you can now do that on my videos. So, somehow there is somewhere a means for you to do that if you're aware of these things or you know how they work, you can now go ahead and do it.

7
00:01:13,000 --> 00:01:19,000
So, just let you know if you wanted to make subtitles, I think it's now possible. Let me know if it's not. We'll try to figure it out.

8
00:01:19,000 --> 00:01:31,000
That's one thing. So, anybody who wants to submit them, please do. It's a great thing to be able to share these things if you think that they're good teachings to share them with other people and who don't speak English.

9
00:01:31,000 --> 00:01:49,000
And the other thing is along the same lines, translating my booklet, How to Meditate, which is located at htm.ceremungalode.org. It's got its own page, its own website there.

10
00:01:49,000 --> 00:02:01,000
And so translating it into languages that it isn't already translated into. We've translated into something like 18 different languages. But we're always looking for more.

11
00:02:01,000 --> 00:02:16,000
My teacher said he wanted us to get 110 languages, I think, or something like that. So, please do. If there's a language that hasn't been translated and you'd like to do that great work, please go ahead and do that.

12
00:02:16,000 --> 00:02:25,000
The final thing is something that I've been meaning to let people know. I've been letting people know, but I haven't done a video about it. So, here's a video that talks specifically about it.

13
00:02:25,000 --> 00:02:38,000
As we've started to do online meditation courses, so I'm meeting with meditators, four different meditators, up to four different meditators a day, actually six on Saturday.

14
00:02:38,000 --> 00:02:59,000
And we meet one on one, and this is for people who are undertaking daily practice at least an hour a day, and we try to get up to two hours a day, and would like to go through a more comprehensive practice than is found in the simple booklet on how to meditate, which is really just about foundation practice.

15
00:02:59,000 --> 00:03:16,000
So, if you're interested, you can go to meditation.ceremungalow.org, sign up, register. You just enter your username and password, then click register, and then there's a meet page, and you click there, and you can choose a slot.

16
00:03:16,000 --> 00:03:27,000
You just click on the slot, and then you're set. You need to not use Hangouts, Google Hangouts, and you need to be familiar with the Hangouts interface at Hangouts.google.com,

17
00:03:27,000 --> 00:03:44,000
but there's a little bit of explanation about that on the page itself, and you need a webcam and a microphone, preferably a headset, but all that's negotiable.

18
00:03:44,000 --> 00:03:53,000
And then we just meet once a week, and you practice at least an hour a day, and every week I give you a new exercise, so anyone who's interested in that, please do.

19
00:03:53,000 --> 00:04:03,000
Take advantage of that. There are limited slots available, but people as people finish the course, new slots become available, so always check back.

20
00:04:03,000 --> 00:04:14,000
And, okay, so that's it. Those are some things that I thought you should know about, and I think I'll do this whenever I think of something that I think you all should know about. I'll just make a video about it.

21
00:04:14,000 --> 00:04:24,000
So I hope that's useful, and helpful, and thank you all for being helpful in that regard. I wish you all good practice. Be well.

