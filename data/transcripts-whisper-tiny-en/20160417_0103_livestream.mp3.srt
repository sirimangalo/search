1
00:00:00,000 --> 00:00:22,000
So, good evening broadcasting live, April 16th.

2
00:00:22,000 --> 00:00:35,000
Oh, did everyone know any leave yet?

3
00:00:35,000 --> 00:00:41,000
I'm Simon.

4
00:00:41,000 --> 00:00:57,000
Yeah, I just got back from our first event.

5
00:00:57,000 --> 00:01:14,000
I thought meditation this afternoon.

6
00:01:14,000 --> 00:01:40,000
I'm actually just getting in the door.

7
00:01:40,000 --> 00:01:46,000
And I've no doubt we're wavering concerning with the fog at that.

8
00:01:46,000 --> 00:01:48,000
Or his teachings.

9
00:01:48,000 --> 00:01:55,400
And it may be expected that such a disciple, though they've resolute in energy, always striving

10
00:01:55,400 --> 00:02:02,400
to abandon bad qualities and develop good ones, and that he will be energetic in exerting

11
00:02:02,400 --> 00:02:12,400
himself and will not drop good.

12
00:02:12,400 --> 00:02:18,400
So this is how we, this is what we work for.

13
00:02:18,400 --> 00:02:22,400
We work for the good.

14
00:02:22,400 --> 00:02:31,400
We're in good with body, good with speech, good with mind, good for ourselves,

15
00:02:31,400 --> 00:02:50,400
good for others, thinking only about welfare and goodness.

16
00:02:50,400 --> 00:03:07,400
Well, the big city is, I don't know, you know, they say wherever you go there you are, I don't

17
00:03:07,400 --> 00:03:11,400
really see that much of a difference.

18
00:03:11,400 --> 00:03:18,400
More seeing, more hearing, more smelling, more tasting, more feeling, more thinking.

19
00:03:18,400 --> 00:03:26,800
I met a nice person today, Joe Vanna, an Italian woman who's sort of running the Buddhist

20
00:03:26,800 --> 00:03:32,400
insights group with Dante Sudasu.

21
00:03:32,400 --> 00:03:50,400
She invited us to lunch, and then we went to her apartment, and I did this evening's

22
00:03:50,400 --> 00:03:59,400
interviews, meditation interviews in her apartment with her Wi-Fi, and then I came home.

23
00:03:59,400 --> 00:04:06,400
Oh, the Danish booklet, well that's nice.

24
00:04:06,400 --> 00:04:10,400
I need a, I can't do anything with it.

25
00:04:10,400 --> 00:04:17,400
Don't send it to me until I get back to Hamilton, back to Canada.

26
00:04:17,400 --> 00:04:28,400
Once I get back to Canada then you can email it to me in either ODT or DOC format.

27
00:04:28,400 --> 00:04:33,400
ODT is actually better if you're using Libra office or something.

28
00:04:33,400 --> 00:04:42,400
No, I can't take a PDF, I need the actual doc.

29
00:04:42,400 --> 00:05:06,400
You need to send me the DOC or DOCX or ODT or something or just, yeah, and format it if you

30
00:05:06,400 --> 00:05:13,400
want.

31
00:05:13,400 --> 00:05:22,400
There's a subway close by, so I can use the Canadian gift cards in America.

32
00:05:22,400 --> 00:05:32,400
So I'm inviting the other monk Sudasu Sudasu here, inviting for breakfast tomorrow.

33
00:05:32,400 --> 00:05:39,400
And then tomorrow evening at seven, there's our next session, and so, because it's at seven

34
00:05:39,400 --> 00:05:45,400
I'm not sure if I'll be back by nine, but we will see.

35
00:05:45,400 --> 00:05:47,400
Anyway, have a good night everyone.

36
00:05:47,400 --> 00:06:10,400
Thanks for tuning in.

