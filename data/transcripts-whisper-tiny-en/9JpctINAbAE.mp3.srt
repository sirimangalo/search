1
00:00:00,000 --> 00:00:12,400
Very good evening, tonight we are continuing our study of the Dhampanda, today we look

2
00:00:12,400 --> 00:00:33,000
at verse 201, which goes as far as, Jaiyang-Weirang-Pasavati, Tukang-Saiti Parajito,

3
00:00:33,000 --> 00:00:52,500
Upasanto-Sukang-Saiti, Ithwa-Jaiyaparajaiyam, which means victory breeds enmity,

4
00:00:52,500 --> 00:01:10,300
or vengeance, Weirak, those who are defeated, those who are not victorious,

5
00:01:10,300 --> 00:01:28,300
to well in suffering, those who are tranquil or who have found peace, to well in happiness,

6
00:01:28,300 --> 00:01:43,100
having abandoned victory and defeat. So this was told in regards where we're told,

7
00:01:43,100 --> 00:01:56,300
that was in relation to Ajata-sattu. Ajata-sattu was a prince in the time in the Buddha,

8
00:01:56,300 --> 00:02:06,300
and he fell in with the wrong crowd. He became friends with Devadatta, who was out to destroy,

9
00:02:06,300 --> 00:02:20,000
or take over the Buddhist congregation, and so he teamed up with Ajata-sattu and Ajata-sattu,

10
00:02:20,000 --> 00:02:31,300
and Devadatta suggested that he kill his father, who was the king, and he would be able to take

11
00:02:31,300 --> 00:02:41,300
over the kingdom, and Devadatta would kill the Buddha, and he would take over the

12
00:02:41,300 --> 00:02:53,300
which are what you call it, the religion, I guess, religious organization.

13
00:02:53,300 --> 00:03:02,300
And they tried, and Devadatta was unsuccessful, of course. Ajata-sattu was eventually successful.

14
00:03:02,300 --> 00:03:08,300
He tried to kill his father. He couldn't bear to actually kill his father,

15
00:03:08,300 --> 00:03:16,300
so he threw him in the dungeon, had him tied to the wall, or chained to the wall,

16
00:03:16,300 --> 00:03:23,300
and had him starve to death, not bringing any food to him.

17
00:03:23,300 --> 00:03:28,300
The story goes that Vimissara was, Vimissara was his father.

18
00:03:28,300 --> 00:03:39,300
He was so happy because his jail cell, he was able to see the monastery, or the Buddha lived,

19
00:03:39,300 --> 00:03:46,300
and so he was able to survive for quite some time.

20
00:03:46,300 --> 00:03:53,300
That's a long story. He would, in jail, he would do walking meditation,

21
00:03:53,300 --> 00:04:00,300
and I can't remember. He was so happy that he wasn't even concerned about being in prison,

22
00:04:00,300 --> 00:04:04,300
eventually he'd chained him to the wall.

23
00:04:04,300 --> 00:04:13,300
And the queen brought food in secretly and eventually he refused to allow the queen to see the king.

24
00:04:13,300 --> 00:04:17,300
Eventually, Vimissara died. It doesn't make a long story short.

25
00:04:17,300 --> 00:04:24,300
But that's not this story. This story is about Ajata-sattu after he killed the king.

26
00:04:24,300 --> 00:04:33,300
He became quite an imperialist or a colonialist trying to take over the rest of the world.

27
00:04:33,300 --> 00:04:44,300
And he fought with the king of Kursala, who I guess was the same day.

28
00:04:44,300 --> 00:04:49,300
And it says he was his uncle, so I'm not really sure where the connection is,

29
00:04:49,300 --> 00:04:54,300
but he fought with his uncle, and he kept beating his uncle.

30
00:04:54,300 --> 00:05:02,300
And the king of Kursala was so upset by being beaten by Ajata-sattu.

31
00:05:02,300 --> 00:05:10,300
Ajata-sattu that he refused to eat, and he became very ill and very weak.

32
00:05:10,300 --> 00:05:14,300
And he was just tormented lying on his bed.

33
00:05:14,300 --> 00:05:17,300
It's actually quite a short story.

34
00:05:17,300 --> 00:05:19,300
But there's much background to it.

35
00:05:19,300 --> 00:05:26,300
The background of this prince killing his father, David Ajata, trying to kill the Buddha.

36
00:05:26,300 --> 00:05:38,300
Vimissara and his greatness at the moment of death, Ajata-sattu tried to actually stop his father from starving to death.

37
00:05:38,300 --> 00:05:42,300
At the last moment, he went to see his father, but he was too late.

38
00:05:42,300 --> 00:05:49,300
His father had died.

39
00:05:49,300 --> 00:06:14,300
But the essence of this is about victory and defeat, one of the results of our perspective on the world,

40
00:06:14,300 --> 00:06:30,300
a person who strives constantly to defeat and to beat others.

41
00:06:30,300 --> 00:06:40,300
So on the surface, it appears to just be a good religious advice telling people not to fight with each other.

42
00:06:40,300 --> 00:06:46,300
So when you fight, it's what we tell kids, when you fight, you end up hurting others.

43
00:06:46,300 --> 00:06:48,300
It's not very nice.

44
00:06:48,300 --> 00:06:59,300
And you end up breeding enmity and vengeance and so on.

45
00:06:59,300 --> 00:07:01,300
But there's a deeper lesson here.

46
00:07:01,300 --> 00:07:10,300
And the lesson for us as meditators is that this perspective is one that leads to suffering.

47
00:07:10,300 --> 00:07:20,300
Tukung-si-diparajito.

48
00:07:20,300 --> 00:07:23,300
Buddhism is very much about our perspective.

49
00:07:23,300 --> 00:07:28,300
We use the word diti a lot when we talk about our views.

50
00:07:28,300 --> 00:07:41,300
It really refers to the way we look at the world, the way we approach, experience, the way we approach our experience.

51
00:07:41,300 --> 00:07:50,300
And it's similar to people who have ambition, like Adjata Satu had ambition, David Adjata had ambition.

52
00:07:50,300 --> 00:07:59,300
Even simply approaching reality as problems that we have to fix.

53
00:07:59,300 --> 00:08:09,300
People who fall into this game of vengeance, of victory and defeat have a sense of what they call a zero sum.

54
00:08:09,300 --> 00:08:15,300
I think it's a zero sum game, sort of it's called.

55
00:08:15,300 --> 00:08:26,300
It means that in order for me to benefit someone else has to suffer.

56
00:08:26,300 --> 00:08:28,300
And so in the end it equals out to zero.

57
00:08:28,300 --> 00:08:32,300
It's like a law of thermodynamics or something.

58
00:08:32,300 --> 00:08:39,300
In order to find happiness, it has to be at the expense of someone else.

59
00:08:39,300 --> 00:08:47,300
So on the face of it it seems practically or conventionally sort of a terrible outlook.

60
00:08:47,300 --> 00:08:58,300
And yet it is a sort of a conventional outlook that people have, thieves and more lords.

61
00:08:58,300 --> 00:09:07,300
But also ordinary people have a sense of trying to find happiness through manipulating others, through harming others,

62
00:09:07,300 --> 00:09:08,300
through defeating others.

63
00:09:08,300 --> 00:09:15,300
It's something we fall into quite easily for making enemies.

64
00:09:15,300 --> 00:09:19,300
We identify people who are in our way.

65
00:09:19,300 --> 00:09:22,300
But it's a deeper psychological issue.

66
00:09:22,300 --> 00:09:23,300
It's our outlook.

67
00:09:23,300 --> 00:09:29,300
And these actions affect our outlook and they spring from our outlook.

68
00:09:29,300 --> 00:09:37,300
Our outlook is a much more basic sense of trying to fix things, trying to force.

69
00:09:37,300 --> 00:09:49,300
Trying to bring about resolution by sheer force of our disliking of the situation.

70
00:09:49,300 --> 00:09:52,300
It doesn't have to be a person.

71
00:09:52,300 --> 00:10:03,300
Though people, other human beings, become galvanizing icons of hatred.

72
00:10:03,300 --> 00:10:07,300
And if you stub your toe, you might get angry at the table.

73
00:10:07,300 --> 00:10:13,300
But if someone hits you, you can develop a grudge for a lifetime.

74
00:10:13,300 --> 00:10:15,300
Someone says bad things.

75
00:10:15,300 --> 00:10:25,300
It becomes a galvanizing, very strengthening focal point for hatred.

76
00:10:25,300 --> 00:10:30,300
But ultimately it's the same habit we don't like something.

77
00:10:30,300 --> 00:10:39,300
And so we fall into this habit of reacting, trying to get rid of what it is that we don't like.

78
00:10:39,300 --> 00:10:41,300
Change what it is that we don't like.

79
00:10:41,300 --> 00:10:51,300
Force the bad circumstances, the bad conditions to become good.

80
00:10:51,300 --> 00:11:05,300
And so it leads directly to our understanding of the practice of mindfulness as an antidote for this sort of behavior.

81
00:11:05,300 --> 00:11:12,300
And sort of as an alternative, it shows quite clearly how mindfulness is different.

82
00:11:12,300 --> 00:11:16,300
Mindfulness is not like fixing problems.

83
00:11:16,300 --> 00:11:21,300
It's certainly not like achieving goals.

84
00:11:21,300 --> 00:11:33,300
And it's absolutely not like forcing or controlling reality or other people, especially to try and be the way we want.

85
00:11:33,300 --> 00:11:37,300
Mindfulness is about experiencing things as they are.

86
00:11:37,300 --> 00:11:43,300
We talk about this and we explain mindfulness as just being aware of things as they are.

87
00:11:43,300 --> 00:11:57,300
This sort of verse, this sort of teaching, this sort of example of the story of a jetticep do or any story of people holding a grudge and fighting and even killing each other

88
00:11:57,300 --> 00:12:04,300
over their desire for some beneficial outcome.

89
00:12:04,300 --> 00:12:13,300
This kind of story shows how different it is to be mindful and it shows the contrast with what we're doing.

90
00:12:13,300 --> 00:12:24,300
The contrast would lead to when someone tries to harm you.

91
00:12:24,300 --> 00:12:37,300
To change your perception of the situation from trying to defend yourself and trying to keep yourself from experiencing suffering.

92
00:12:37,300 --> 00:12:46,300
To being able to see the experience as just an experience and let it happen and let it go.

93
00:12:46,300 --> 00:13:10,300
Which is a very radical sort of approach. It shows how different this approach is from an ordinary way of living.

94
00:13:10,300 --> 00:13:18,300
But the difference is quite apparent, quite profound as well, the difference in result.

95
00:13:18,300 --> 00:13:26,300
A person who engages frequently, regularly in hatred, of course, is going to find suffering.

96
00:13:26,300 --> 00:13:32,300
They're going to cultivate enemies and enemy.

97
00:13:32,300 --> 00:13:47,300
They're going to suffer defeat. They're going to lead to situations where people want to hurt them, want to manipulate and take advantage of them.

98
00:13:47,300 --> 00:14:04,300
We see at a very deep level this sort of approach, this perspective of trying to force, trying to conquer.

99
00:14:04,300 --> 00:14:09,300
This is quite obvious that this is the way things are.

100
00:14:09,300 --> 00:14:21,300
When we try to hurt others, it's unsustainable in terms of providing peace and happiness.

101
00:14:21,300 --> 00:14:27,300
Not only does it create enmity, but it changes who you are.

102
00:14:27,300 --> 00:14:42,300
Really, this, the teaching is that there are two levels to reality. There's the conventional level and there's, I guess, you could call the psychological level.

103
00:14:42,300 --> 00:14:46,300
And they work off of each other.

104
00:14:46,300 --> 00:15:04,300
So when we act and speak, trying to harm others, it affects us psychologically. Our psychological makeup, how we relate to experience, how we perceive reality, is going to inform our decisions.

105
00:15:04,300 --> 00:15:15,300
It's going to affect the way we relate to other people, the way we relate to problems and situations in our lives.

106
00:15:15,300 --> 00:15:26,300
When you take on a philosophy of non-harming that this verse sort of suggests, I'm not trying to conquer others.

107
00:15:26,300 --> 00:15:35,300
I'm not seeing situations as something to be conquered as a zero-sum game.

108
00:15:35,300 --> 00:15:45,300
Because, of course, the reality is not anything like where you defeat others and suddenly you're on top. You've won something.

109
00:15:45,300 --> 00:15:54,300
The reality is that it's a constant changing sort of flux where sometimes you're winning and sometimes you're losing.

110
00:15:54,300 --> 00:16:00,300
And enmity breeds enmity when you hurt others. You make them want to hurt you, so it's not a zero-sum game.

111
00:16:00,300 --> 00:16:07,300
It's when everybody loses sort of game. When you hurt others, you make others want to hurt you.

112
00:16:07,300 --> 00:16:12,300
And all of this affects our psychology, it affects our makeup.

113
00:16:12,300 --> 00:16:29,300
When you cease to do that, when you stop wanting to hurt others, when you allow other people their anger against you and your experience, the results of

114
00:16:29,300 --> 00:16:38,300
bad blood without reacting, this affects your psychology, it affects your mental makeup.

115
00:16:38,300 --> 00:16:53,300
It's a very sort of good practical teaching in terms of the direction we want to be heading of trying to be more peaceful, happier, free from suffering.

116
00:16:53,300 --> 00:17:05,300
But from the other end as well, when we hear about this, when we look at this teaching, abandoning victory and defeat,

117
00:17:05,300 --> 00:17:10,300
it was Jaya Parajayan.

118
00:17:10,300 --> 00:17:14,300
We see the deeper teaching of being mindful.

119
00:17:14,300 --> 00:17:32,300
And when you yourself approach reality in terms of experiences to be understood rather than problems to be fixed or challenges to be overcome or whatever enemies to be conquered, absolutely.

120
00:17:32,300 --> 00:17:42,300
Pain being an enemy, even our defilements as being enemies is really a problematic way of looking at them.

121
00:17:42,300 --> 00:17:52,300
If you look at anger as an enemy or greed as an enemy, it takes on an adversarial sort of state of being.

122
00:17:52,300 --> 00:18:03,300
And there's stress involved and you cultivate this habit of trying to control and force and change reality.

123
00:18:03,300 --> 00:18:17,300
But when you take up mindfulness, it's no longer just about our actions and our speech, but our whole approach to reality that pain is not an enemy.

124
00:18:17,300 --> 00:18:26,300
Speech, harsh speech from other people is not something to be conquered, not something to be fought against.

125
00:18:26,300 --> 00:18:33,300
I don't deserve to hear that. I don't deserve to be spoken to that way.

126
00:18:33,300 --> 00:18:39,300
Instead, experiencing it as sound, experiencing it as sight.

127
00:18:39,300 --> 00:18:42,300
Seeing reality is made up of experiences.

128
00:18:42,300 --> 00:18:51,300
This is what it truly means, I think, to give up victory and defeat because your whole way of looking at reality is no longer...

129
00:18:51,300 --> 00:18:56,300
No longer adversarial.

130
00:18:56,300 --> 00:19:04,300
You rise above it and say, you change the perspective.

131
00:19:04,300 --> 00:19:11,300
There's an old saying, if you're a hammer, everything looks like a nail.

132
00:19:11,300 --> 00:19:24,300
And it points out something, what it means is when your outlook is adversarial, you're constantly going to be trying to react in a certain way.

133
00:19:24,300 --> 00:19:40,300
And it points out a very important reality that the world around us, our environment, our experience of the world, is very much dependent on our outlook.

134
00:19:40,300 --> 00:19:44,300
And so...

135
00:19:44,300 --> 00:19:52,300
I mean, an argument against this sort of passivity whereby you let other people harm you, for example.

136
00:19:52,300 --> 00:19:54,300
Does it may happen?

137
00:19:54,300 --> 00:20:00,300
Where you are not only non-violent, but you're passive, you're not only peaceful, but you're passive.

138
00:20:00,300 --> 00:20:07,300
The criticism might be that you're going to allow people to hurt you.

139
00:20:07,300 --> 00:20:12,300
You're going to be taking advantage of, for example.

140
00:20:12,300 --> 00:20:21,300
And so they would say, if you look at it this way, it's not a very good way to live.

141
00:20:21,300 --> 00:20:24,300
But the point is that there's a deeper reality going on here.

142
00:20:24,300 --> 00:20:28,300
You're changing the way you look at things.

143
00:20:28,300 --> 00:20:43,300
A person who is harmed when someone attacks someone else, or speaks violently, speaks angrily to someone else.

144
00:20:43,300 --> 00:20:46,300
The reality is still just experience.

145
00:20:46,300 --> 00:20:52,300
Another way of looking at this is rather than an adversarial situation.

146
00:20:52,300 --> 00:20:58,300
Looking at it as experience is obscene, hearing, smelling, tasting, feeling, thinking.

147
00:20:58,300 --> 00:21:05,300
If you can see it like that, it doesn't change the situation where suddenly people are nice to you.

148
00:21:05,300 --> 00:21:07,300
It changes the perspective.

149
00:21:07,300 --> 00:21:12,300
And reality is suddenly objective.

150
00:21:12,300 --> 00:21:23,300
It doesn't matter whether you're being hugged or you're being attacked.

151
00:21:23,300 --> 00:21:29,300
It's a very radical, again, sort of outlook.

152
00:21:29,300 --> 00:21:39,300
And I think in this exemplifier, this shows us clearly how radical it is to be mindful,

153
00:21:39,300 --> 00:21:49,300
and the extent to which mindfulness changes our lives doesn't necessarily change our situation.

154
00:21:49,300 --> 00:21:52,300
So it can be quite uncomfortable at first.

155
00:21:52,300 --> 00:22:00,300
We don't have to even talk about other people who might want to hurt us, but our own body wants to hurt us.

156
00:22:00,300 --> 00:22:11,300
Because of our reactions and our emotions, we make the body quite sick, and we can be quite tense.

157
00:22:11,300 --> 00:22:18,300
When we first come to practice meditation, it can be quite painful, your own body can cause you great suffering.

158
00:22:18,300 --> 00:22:29,300
And if you look at that as an adversarial sort of thing, something to be fixed, something to be fought against, something to be controlled or tamed,

159
00:22:29,300 --> 00:22:33,300
you're just going to create more stress and suffering.

160
00:22:33,300 --> 00:22:44,300
You have a never-ending torment. It can be quite painful and quite unpleasant.

161
00:22:44,300 --> 00:22:49,300
So our way of looking at things from the mindfulness is quite simple.

162
00:22:49,300 --> 00:22:54,300
It's not at all easy, but it's quite simple. It's about seeing things just as they are.

163
00:22:54,300 --> 00:22:59,300
This is why we repeat to ourselves as ample pain and pain.

164
00:22:59,300 --> 00:23:02,300
If you're happy, it's a happy, happy.

165
00:23:02,300 --> 00:23:05,300
Just trying to see things as they are without reacting.

166
00:23:05,300 --> 00:23:11,300
Trying to cultivate a new perspective, a new way of looking at things.

167
00:23:11,300 --> 00:23:20,300
That is not adversarial, not about conquering reality or lives or winning or anything.

168
00:23:20,300 --> 00:23:32,300
But it's about cultivating a perspective of objectivity, of peace.

169
00:23:32,300 --> 00:23:39,300
That rises above experiences of victory or defeat.

170
00:23:39,300 --> 00:23:44,300
So someone might look at you and say, wow, that person is a real pushover, they're being defeated.

171
00:23:44,300 --> 00:23:47,300
And you won't even see it that way.

172
00:23:47,300 --> 00:23:51,300
And the end you see it only as experience. It's quite powerful.

173
00:23:51,300 --> 00:23:58,300
Because the practical reality is someone who is mindful is often quite revered, respected.

174
00:23:58,300 --> 00:24:03,300
You'll find that people respect your opinion.

175
00:24:03,300 --> 00:24:08,300
They lose their desire to harm you.

176
00:24:08,300 --> 00:24:13,300
A person who is very mindful often finds that people who they thought were their enemies

177
00:24:13,300 --> 00:24:20,300
are suddenly able to come to terms and work towards a more peaceful resolution.

178
00:24:20,300 --> 00:24:25,300
Because the person has changed the parameters.

179
00:24:25,300 --> 00:24:36,300
A lot of our adversarial conflict, a lot of our conflict as humans,

180
00:24:36,300 --> 00:24:41,300
depends on both sides continuing and perpetuating it.

181
00:24:41,300 --> 00:24:51,300
Our anger perpetuates conflict. Our reactivity perpetuates it.

182
00:24:51,300 --> 00:24:59,300
So it's not something that you should be scared of or perhaps not see it as so radical.

183
00:24:59,300 --> 00:25:05,300
Because it's not as though you have to suddenly let people harm you.

184
00:25:05,300 --> 00:25:10,300
It's about changing the way we look at reality, changing our perspective.

185
00:25:10,300 --> 00:25:13,300
Which also, of course, means changing our lives.

186
00:25:13,300 --> 00:25:19,300
Gradually, gradually, you come to live a more peaceful life where you work out all your problems.

187
00:25:19,300 --> 00:25:25,300
You start to look at your enemies as just people who have problems.

188
00:25:25,300 --> 00:25:36,300
And your interactions with them rather than as fights to be one, their experiences to be understood.

189
00:25:36,300 --> 00:25:41,300
You rise above, you abandon victory and defeat.

190
00:25:41,300 --> 00:25:43,300
That's the verse here.

191
00:25:43,300 --> 00:25:46,300
So that's the Dhammapana, very simple verse.

192
00:25:46,300 --> 00:25:51,300
But I think it ties in well with the understanding of what is mindful of this.

193
00:25:51,300 --> 00:25:54,300
So that's, I think, the lesson we take from it.

194
00:25:54,300 --> 00:25:56,300
Thank you all for listening.

195
00:25:56,300 --> 00:26:11,300
Have a good night.

