1
00:00:00,000 --> 00:00:07,440
Good evening, everyone.

2
00:00:07,440 --> 00:00:25,080
Welcome to our weekly Dhamma talk, Saturdays I try to talk about the Dhamma, something

3
00:00:25,080 --> 00:00:36,280
weekly for the meditators to listen to, to focus their attention, and hopefully for

4
00:00:36,280 --> 00:00:53,440
provide direction and inspiration for their practice.

5
00:00:53,440 --> 00:01:02,600
So I'd like to talk about mindfulness tonight, the topic of course that I talk about quite

6
00:01:02,600 --> 00:01:19,000
often, it's something that we should keep coming back again to thinking about clarifying

7
00:01:19,000 --> 00:01:22,880
or understanding about.

8
00:01:22,880 --> 00:01:29,160
So word that apparently has gained some traction in popular culture, people talk now about

9
00:01:29,160 --> 00:01:36,000
mindfulness in a meditative sense.

10
00:01:36,000 --> 00:01:49,240
The word mindful, word mindful of course means something like being considerate, so it used

11
00:01:49,240 --> 00:01:55,280
to be used before I got into meditation, that's how I thought to use it.

12
00:01:55,280 --> 00:02:04,640
Be mindful of the time, don't lose track of the time, you'll be mindful of other

13
00:02:04,640 --> 00:02:17,040
people's feelings, don't be oblivious, that's a good word, no, it's a good good word

14
00:02:17,040 --> 00:02:32,280
for what we're talking about in Buddhism, don't be oblivious, be mindful.

15
00:02:32,280 --> 00:02:42,440
And so it comes to be a bit of a buzzword, and I think there's some rightful criticism

16
00:02:42,440 --> 00:02:50,520
of the fact that it's perhaps used a little too loosely.

17
00:02:50,520 --> 00:03:03,360
We talk about being mindful, be mindful, be mindful, and it feels like sometimes we talk

18
00:03:03,360 --> 00:03:09,960
about it more than we actually do it, because it's a general sort of buzzword you can

19
00:03:09,960 --> 00:03:19,600
use to say, well be mindful of your thoughts, be mindful of breathing, it's easy to talk

20
00:03:19,600 --> 00:03:21,400
about.

21
00:03:21,400 --> 00:03:29,080
The question is how do you really be mindful and what does it really mean to be mindful?

22
00:03:29,080 --> 00:03:36,800
The word set the, of course, that we translate as mindfulness has similar problems if there

23
00:03:36,800 --> 00:03:43,240
are actually, if we're actually called these problems, and that's problems in the sense

24
00:03:43,240 --> 00:03:53,040
that it's used as well in a more general sense of having sati, it's interesting when

25
00:03:53,040 --> 00:04:02,680
you go to Buddhist countries where a lot of people don't have strong background in

26
00:04:02,680 --> 00:04:09,480
sati pataan of practice, when they use the words, the word sati is, of course, a word

27
00:04:09,480 --> 00:04:17,680
they use in Thai, when I was in Thailand, you hear, I miss it the, chai sati, no, I miss

28
00:04:17,680 --> 00:04:25,720
it the is what my teacher used to say, chai sati is the general expression, mozzati, that

29
00:04:25,720 --> 00:04:42,400
kind of thing, is it in a general sense, you see it used in the sense of being able

30
00:04:42,400 --> 00:04:49,680
to remember things, doesn't really actually mean mindfulness because even the mindfulness

31
00:04:49,680 --> 00:05:02,160
is a good thing, mindfulness means to not lose track, to not be oblivious, doesn't exactly

32
00:05:02,160 --> 00:05:14,840
mean to not forget, but sati means to remember, so if you remember things that happened

33
00:05:14,840 --> 00:05:22,240
a long time ago, that means having sati, it's not quite mindfulness, but mindfulness speaks

34
00:05:22,240 --> 00:05:32,280
to the special usage of the word, when we use sati to mean remembering the present

35
00:05:32,280 --> 00:05:41,400
moment, then it really does relate to mindfulness, the mindful of what you're doing, mind

36
00:05:41,400 --> 00:05:52,280
what you're doing, mind your steps, mind your, mind your speech, mind your manners, mind

37
00:05:52,280 --> 00:06:03,200
your manners, apply your mind to it, if you mind your manners, it means you have some

38
00:06:03,200 --> 00:06:18,040
fortitude of mind that you're not just ignoring your manners, you're not just loosened,

39
00:06:18,040 --> 00:06:27,360
you're not just

40
00:06:27,360 --> 00:06:38,080
afraid of your behavior, and so sati in the special sense, when we get right down to it,

41
00:06:38,080 --> 00:06:40,240
It has a very specific meaning.

42
00:06:40,240 --> 00:06:43,760
We have a definition that they give.

43
00:06:43,760 --> 00:06:45,080
Satyam means...

44
00:06:48,480 --> 00:06:52,520
Satyam has the characteristic of not wavering.

45
00:06:54,320 --> 00:06:57,360
So Satyam means a certain fortitude of mind,

46
00:06:58,960 --> 00:07:00,560
a strength of mind.

47
00:07:03,000 --> 00:07:08,000
Satyam involves the mind state that keeps the object

48
00:07:08,000 --> 00:07:09,000
in mind.

49
00:07:13,280 --> 00:07:15,480
Our ordinary state of mind is wavering.

50
00:07:15,480 --> 00:07:19,920
It's fleeting, flitting here and there.

51
00:07:19,920 --> 00:07:21,840
Flitting from object to object.

52
00:07:25,360 --> 00:07:26,880
We see something and...

53
00:07:32,480 --> 00:07:35,440
We're very briefly actually seeing it.

54
00:07:35,440 --> 00:07:40,440
The majority of our time is spent in reacting to it.

55
00:07:44,720 --> 00:07:45,720
Judging it.

56
00:07:59,200 --> 00:08:01,600
When you watch your mind, perhaps for the first time,

57
00:08:01,600 --> 00:08:03,720
it's gonna be quite shocking

58
00:08:03,720 --> 00:08:07,720
to see how random everything is.

59
00:08:07,720 --> 00:08:11,120
How random mental activity can be jumping here and there.

60
00:08:12,400 --> 00:08:13,720
My element...

61
00:08:15,120 --> 00:08:16,840
If our thoughts were speech,

62
00:08:16,840 --> 00:08:18,200
we would just be...

63
00:08:20,400 --> 00:08:22,720
We would write a book in a matter of minutes.

64
00:08:22,720 --> 00:08:32,720
It wouldn't be the kind of book you'd want to read.

65
00:08:42,720 --> 00:08:48,720
So mindfulness is the ability to stabilize this process.

66
00:08:48,720 --> 00:08:52,720
So that seeing is just seeing, when you're seeing your mind

67
00:08:52,720 --> 00:08:54,720
is with the object.

68
00:08:57,720 --> 00:09:02,720
It's this strong state of mind that grasps the object well.

69
00:09:08,520 --> 00:09:15,520
It has the function, what it's function.

70
00:09:15,520 --> 00:09:19,520
It's function is to not forget.

71
00:09:21,520 --> 00:09:24,520
To not lose track of the object.

72
00:09:24,520 --> 00:09:27,520
We talk about forgetting yourself

73
00:09:31,520 --> 00:09:34,520
in the world when someone forgets themselves.

74
00:09:36,520 --> 00:09:39,520
In the middle of a group of people

75
00:09:39,520 --> 00:09:41,520
and suddenly you say something,

76
00:09:41,520 --> 00:09:45,520
that's only suitable for when you're alone

77
00:09:45,520 --> 00:09:49,520
or are only suitable in your mind.

78
00:09:51,520 --> 00:09:54,520
Like you'll be eating dinner, someone's house and you say,

79
00:09:54,520 --> 00:09:58,520
boy, this meal tastes awful.

80
00:09:59,520 --> 00:10:01,520
Oh, you forgot yourself.

81
00:10:01,520 --> 00:10:04,520
You shouldn't have said that in front of the people who cook the food.

82
00:10:04,520 --> 00:10:16,520
No, even you forget yourself means you fly off the collar

83
00:10:16,520 --> 00:10:19,520
and get angry and start shouting.

84
00:10:19,520 --> 00:10:21,520
You've forgotten yourself.

85
00:10:22,520 --> 00:10:26,520
Or they say it when a person is of a low station,

86
00:10:26,520 --> 00:10:30,520
suppose you're a child and you start mouthing off to your parents

87
00:10:30,520 --> 00:10:34,520
and some cultures they would say you're forgetting yourself.

88
00:10:35,520 --> 00:10:42,520
Western culture we tend to overlook the good that our parents have done for us

89
00:10:42,520 --> 00:10:44,520
and so on and so on.

90
00:10:44,520 --> 00:10:46,520
We're not that kind to them.

91
00:10:46,520 --> 00:10:48,520
I would say not as kind as we should be.

92
00:10:48,520 --> 00:10:50,520
Not as respectful as we should be.

93
00:10:52,520 --> 00:10:54,520
Many of us, anyway,

94
00:10:54,520 --> 00:10:57,520
some are perhaps overly respectful

95
00:10:57,520 --> 00:11:01,520
because of it being drilled into them forcefully,

96
00:11:01,520 --> 00:11:03,520
which is also not great.

97
00:11:04,520 --> 00:11:07,520
Anyway, we talk about forgetting yourself in the sense

98
00:11:07,520 --> 00:11:09,520
that you shouldn't be like this

99
00:11:09,520 --> 00:11:13,520
and you've totally forgot how you should be.

100
00:11:16,520 --> 00:11:20,520
It's not exactly what mindfulness, but it's along the same lines

101
00:11:20,520 --> 00:11:23,520
because the way we should be is objective.

102
00:11:23,520 --> 00:11:27,520
We should be aware of things as they are.

103
00:11:30,520 --> 00:11:38,520
We should be present and merely present,

104
00:11:38,520 --> 00:11:40,520
but we're not.

105
00:11:40,520 --> 00:11:42,520
We're not only are we not present,

106
00:11:42,520 --> 00:11:44,520
we're so much more than that.

107
00:11:44,520 --> 00:11:54,520
We are reacting and judging and forgetting ourselves.

108
00:12:01,520 --> 00:12:03,520
Mindfulness is not forgetting.

109
00:12:03,520 --> 00:12:09,520
Mindfulness is about constantly remembering the experience.

110
00:12:09,520 --> 00:12:15,520
Remembering the essence,

111
00:12:15,520 --> 00:12:22,520
I'm not forgetting that.

112
00:12:22,520 --> 00:12:26,520
This is seeing.

113
00:12:26,520 --> 00:12:30,520
It manifests itself as

114
00:12:30,520 --> 00:12:47,080
It manifests itself as as guarding so the way it manifests this means when a person is mindful

115
00:12:48,200 --> 00:12:50,200
what is the key care what is the key

116
00:12:52,760 --> 00:12:56,680
Quality that you can see in them or they can see in themselves when they're mindful

117
00:12:56,680 --> 00:13:02,120
so how can you tell if you're being mindful how does it manifest itself manifest itself as

118
00:13:02,120 --> 00:13:12,120
guarding meaning guarding the senses meaning when you see things and you're just seeing there's

119
00:13:12,120 --> 00:13:20,360
no reaction when you feel like your senses are perfectly filtered so that you're just seeing

120
00:13:20,360 --> 00:13:30,360
you're just hearing you're just smelling

121
00:13:39,480 --> 00:13:40,600
when you're guarded

122
00:13:42,200 --> 00:13:45,400
when we talk about guarding the senses we talk about guarding your

123
00:13:45,400 --> 00:13:53,320
in general sense we talk about guarding your what's the phrase that they use

124
00:13:54,520 --> 00:14:02,280
guard your thoughts guard your emotions that kind of thing so there's ways of guarding yourself

125
00:14:02,280 --> 00:14:11,080
by suppressing by brute force where you just force yourself not to react externally

126
00:14:11,080 --> 00:14:16,600
you might be with seething inside but I'm not going to say anything that's dear you might be

127
00:14:17,880 --> 00:14:22,520
jumping at the bit to get this or get that but you hold yourself back and say no no

128
00:14:23,640 --> 00:14:28,600
I will not get it

129
00:14:31,480 --> 00:14:36,520
then we do this because we understand the consequences you might repress your emotions

130
00:14:36,520 --> 00:14:44,040
and have a knowledge that the consequences are going to be far more troublesome than their

131
00:14:44,040 --> 00:14:51,640
worth and so you got to stop yourself but mindfulness is mindfulness is more successful

132
00:14:53,000 --> 00:14:57,960
the Buddha said yanny sotani locas mingsatini wara satit is only wara young

133
00:14:59,240 --> 00:15:03,720
whatever streams the literal word is streams but it means

134
00:15:03,720 --> 00:15:08,520
things that will get you caught up in the world will get you in trouble

135
00:15:09,880 --> 00:15:16,680
whatever things there are that will get you caught up in a stream

136
00:15:19,320 --> 00:15:21,800
satit is only wara young sati stops them

137
00:15:23,400 --> 00:15:26,920
sati is what prevents that prevents you from getting caught up

138
00:15:26,920 --> 00:15:33,320
mindfulness is better it's better than repressing it's better than forcing yourself to behave

139
00:15:34,440 --> 00:15:40,680
pretending hiding who you really are taking drugs medication

140
00:15:42,440 --> 00:15:47,000
sati is a better solution it's not easy takes training

141
00:15:49,000 --> 00:15:54,600
but it's far superior because mindfulness is like water mindfulness

142
00:15:54,600 --> 00:15:58,440
dissolves the problems

143
00:16:01,400 --> 00:16:08,840
there's no contrivance there's no artifice there's just objectivity there's just awareness seeing

144
00:16:08,840 --> 00:16:13,880
things as they are

145
00:16:13,880 --> 00:16:26,840
so it guards your senses in a way that guarding your being being wary and being afraid of

146
00:16:27,480 --> 00:16:33,480
your own emotions will never will much better than being afraid having to live in fear and

147
00:16:34,600 --> 00:16:39,000
being a slave to your emotions so far better than that

148
00:16:39,000 --> 00:16:50,120
or it also has the manifestation of confronting another aspect of its manifestation is

149
00:16:51,240 --> 00:17:01,000
confronting we say abimukabawa the state of confronting the the experience

150
00:17:01,000 --> 00:17:14,280
so our ordinary state is not able to confront we react we run away if it's bad we're constantly

151
00:17:14,280 --> 00:17:28,120
avoiding finding ways to destroy or you know to get rid of or to avoid to escape

152
00:17:28,120 --> 00:17:35,480
and if it's positive we also aren't confronting we aren't with the experience

153
00:17:37,400 --> 00:17:43,080
right when when you get something that you want the last thing you do is stay with the experience

154
00:17:43,800 --> 00:17:46,760
you're immediately off in the oh this is wonderful and

155
00:17:48,440 --> 00:17:54,040
how to get it more how to how you know either recursive thought about how you love it

156
00:17:54,040 --> 00:18:01,480
or recursive thought about how you can get it more or keep it or cling to it get closer to it

157
00:18:01,480 --> 00:18:05,800
stronger more satisfying

158
00:18:11,240 --> 00:18:16,600
setty confronts setty is about being with experience it's it's evolving patience

159
00:18:16,600 --> 00:18:26,040
patience is another good word in Buddhism patience is about sticking with good things and bad things

160
00:18:26,040 --> 00:18:29,720
and rather than reacting to them being with them

161
00:18:32,680 --> 00:18:39,800
the ability to be present when something pleasant or unpleasant comes and not be upset by it

162
00:18:39,800 --> 00:18:51,800
not be disturbed that's what setty is the last thing is the the cause of setty what is it that

163
00:18:51,800 --> 00:18:57,720
gives rise to setty and it's another part of the definition how you can know whether you're

164
00:18:57,720 --> 00:19:10,920
being mindful and how to be mindful the cause of the cause of mindfulness is something called

165
00:19:10,920 --> 00:19:22,280
tira sanya sanya is in this context means the perception of a thing so we're in some sense

166
00:19:22,280 --> 00:19:28,920
we're we're immediately mindful of everything when you see there's no question that you

167
00:19:28,920 --> 00:19:35,240
have an awareness of the scene this is why it can be confusing as to what is different about

168
00:19:35,240 --> 00:19:40,360
being mindful you think well of course when I see of course I know I'm seeing

169
00:19:42,440 --> 00:19:48,440
sanya the knowledge that this is seeing and what you're seeing and so on

170
00:19:48,440 --> 00:19:58,920
but tira sanya tira means strength and or strong or firm so there's a sense of strengthening

171
00:20:00,680 --> 00:20:06,200
how do you create setty by strengthening your perception what that means is when you see

172
00:20:06,840 --> 00:20:13,400
you strengthen it you find a way to reinforce the state of that just being seen

173
00:20:13,400 --> 00:20:19,640
it's fine and good to say let's seeing be seeing you and stop there but how do you stop there

174
00:20:19,640 --> 00:20:27,800
how do you reinforce that that's the that's the practice that leads to mindfulness leads to setty

175
00:20:29,720 --> 00:20:33,640
well that's why we use the mantra that's why we have a word to repeat to ourselves

176
00:20:34,520 --> 00:20:39,240
why we say to ourselves seeing seeing why so much meditation is about repeating

177
00:20:39,240 --> 00:20:45,720
mantras because it reaffirms your perception that you're trying to focus on if it's

178
00:20:45,720 --> 00:20:51,400
summit of meditation it would be the perception of a specific thing and we pass in a meditation

179
00:20:51,400 --> 00:20:56,920
it's the perception of your experience when seeing you say to yourself seeing when hearing it's

180
00:20:56,920 --> 00:21:10,680
a hearing and pain it's a pain pain tira sanya this is really the practice that we teach that

181
00:21:10,680 --> 00:21:20,680
we practice it's the practice of strengthening our our experiences you're strengthening our awareness

182
00:21:20,680 --> 00:21:28,440
of the experiences reminding ourselves so that we don't forget we don't lose ourselves

183
00:21:36,360 --> 00:21:40,920
or its proximate causes simply the four foundations of mindfulness if you want to look at it

184
00:21:40,920 --> 00:21:46,760
from a practical perspective it means practicing the four setty batana it's a different way of

185
00:21:46,760 --> 00:21:53,720
looking at it mindfulness of the body when we watch the stomach and say to ourselves

186
00:21:53,720 --> 00:22:01,000
rising falling or when we walk and say stepping right stepping left mindfulness of the body

187
00:22:01,000 --> 00:22:06,680
when you have pain you say to yourself pain pain that's mindfulness of the feelings

188
00:22:08,120 --> 00:22:12,520
when you have thoughts and you say to yourself thinking thinking good thoughts bad thoughts

189
00:22:12,520 --> 00:22:19,720
past future whatever thoughts you say to yourself thinking thinking that's mindfulness of the mind

190
00:22:21,160 --> 00:22:27,800
liking disliking drowsiness distraction doubt when you say to yourself liking liking or wanting

191
00:22:29,640 --> 00:22:38,840
when you say disliking disliking boards sad afraid depressed frustrated

192
00:22:38,840 --> 00:22:46,040
just this reaffirmation the practicing mindfulness

193
00:22:49,880 --> 00:22:54,520
this is the this is what leads to the state of being mindful the state of sati

194
00:23:01,880 --> 00:23:06,440
drowsier tired you say tired tired you're thinking a lot

195
00:23:06,440 --> 00:23:10,200
your mind is not focused you say distracted or unfocused

196
00:23:12,760 --> 00:23:17,640
when you're doubting or confused and say doubting doubting or confused confused

197
00:23:24,760 --> 00:23:27,080
what's the cause that's how you become mindful

198
00:23:27,080 --> 00:23:35,320
tell you develop sati

199
00:23:40,440 --> 00:23:45,160
mindfulness should be regarded as a pillar mindfulness should be seen as

200
00:23:45,160 --> 00:24:01,240
this stake stuck in the ground so pillar pillars being strong pillars being a symbol of strength

201
00:24:01,240 --> 00:24:06,120
mindfulness is like a pillar because a pillar the reason you have a pillar is to strengthen

202
00:24:06,120 --> 00:24:13,400
something that's what pillars are for they are unmoving if you have a pillar that wobbles around

203
00:24:13,400 --> 00:24:20,680
you don't have a pillar if your pillar moves around and jumps here and there it's not a pillar

204
00:24:22,040 --> 00:24:30,760
if it wavers or falls over not a very good pillar now a pillar is designed to be strong and firm

205
00:24:30,760 --> 00:24:43,880
and unmoving and supportive mindfulness should be as thin as a pillar a pillar as opposed to a pumpkin

206
00:24:43,880 --> 00:24:52,360
yeah a gourd a gourd is something very light before they had you know this is an ancient times of

207
00:24:52,360 --> 00:24:58,840
course things that float on water there's not a great number of natural things that float on water

208
00:24:58,840 --> 00:25:09,640
like a gourd nowadays it would be something like a rubber duck or something like that but a gourd

209
00:25:10,920 --> 00:25:19,960
is because it's hollow of course it floats on the water and so when you put a gourd on the water

210
00:25:19,960 --> 00:25:26,760
it's the epitome it's a symbol of everything that a pillar is not it flits here and there it's

211
00:25:26,760 --> 00:25:32,280
buffeted by the winds the slightest wave and it's often moving around here and there

212
00:25:35,080 --> 00:25:40,680
mindfulness is not the ordinary mind is like that the ordinary mind is like the gourd on the water

213
00:25:41,800 --> 00:25:46,440
mindfulness is the exact opposite it's a pillar stuck in the bottom of the ocean

214
00:25:46,440 --> 00:26:04,600
mindfulness is like a gourd a doorkeep in guards the doors of the senses should be seen as a

215
00:26:04,600 --> 00:26:13,400
as the guard our mind has precious treasure our mind should be a place of precious treasure

216
00:26:13,400 --> 00:26:20,840
where we keep all sorts of good qualities like focus and wisdom

217
00:26:23,560 --> 00:26:27,880
as opposed we start with this great treasure house and because we don't have a very good

218
00:26:27,880 --> 00:26:35,640
guard set up a lot of our treasure gets stolen we may be get some treasure but yeah we lose it

219
00:26:35,640 --> 00:26:42,120
and we don't have a lot of treasure because it's not easy to keep treasure without guards

220
00:26:43,720 --> 00:26:50,360
but if you want treasures like concentration and wisdom and love and compassion

221
00:26:51,560 --> 00:27:00,520
patience peace happiness you need a guard mindfulness is that guard it guards the doors

222
00:27:00,520 --> 00:27:07,000
guards our senses to keep the filaments from stealing our good qualities

223
00:27:15,480 --> 00:27:23,480
that's mindfulness again some of that most of this if you're an avid follower some of you I think

224
00:27:23,480 --> 00:27:28,680
are here every week a lot of this is familiar but familiar is often good

225
00:27:28,680 --> 00:27:37,320
what we're trying to do is not complicated it's not complex it's just difficult

226
00:27:38,280 --> 00:27:41,480
it takes training and take patience perseverance

227
00:27:41,480 --> 00:27:57,400
but that's the demo for tonight thank you all for tuning in

