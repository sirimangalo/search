So, tonight we will be going over some fundamentals of meditation practice, giving basic
instruction in meditation practice by request.
So, let's all get in a meditative mode, let's restart, once we start talking about meditation
it's clearly not a theoretical subject, this isn't a talk that you should listen to intellectually
it's a teaching that you should put into practice.
The practice of meditation really follows this sequence of trainings as laid down by the
Buddha, so the Buddha said there are three trainings that we should train ourselves in,
and probably we call these sea lassamadi and panya, in English we might say morality, concentration
and wisdom, but these are misleading or it's difficult to understand these without explaining
them, explaining these terms in detail because misunderstanding arises and based on the
words themselves, morality for example, so with morality we think of certain deeds that
we shouldn't perform and concentration we think of focusing the mind on a single object
or single point and keeping it in one place and wisdom we think of as intellectual thought
or ideas, concepts, theories and so on, these are mundane morality, mundane concentration
and mundane wisdom, in meditation it's on a more ultimate or a more experiential level,
that has to do with our experience, morality plays a part in our existence right here
and now even when we don't have an opportunity to do or say bad things, true morality
is the mind that avoids or refrains from what we might call evil or unwholesumness and
real concentration is, we might let's put it phrase in terms of reality, we'll say morality
is the refraining from or the limiting one's attention to ultimate reality, to experience,
so when the mind wanders off into concepts we bring it back to experience, to reality,
to what's really being experienced right, so when we're thinking it's all just thinking
but instead of seeing it as thinking we see it as this reality or that concept as people
and places and things that we're thinking about, we don't realize that we're thinking.
Concentration is the focusing in the Buddha's teaching, the ultimate or the highest concentration
is the focusing on ultimate reality, to not focusing on one single thing but focusing
on your own experience, having a focused mind, the mind that is in focus, so seeing things
as they are, not how we want them to be or how we don't want them to be, just seeing them
as they are.
And wisdom is rather than any kind of theory about reality or the universe or the past or
the future or our life or life in general, its understanding of experience, or understanding
of reality.
Which is really quite simple actually, it means seeing reality, knowing reality for what
it is, it's something that we already think we have but we don't, we don't really understand
reality, we don't understand our own experiences, if we did why would our experiences
cause trouble for us, why would we do and say and think things that were too our detriment,
it wouldn't if we really understood what we were doing, this is the idea, the idea
behind wisdom, behind the training or training ourselves to go into or to enter into every
experience with wisdom that prevents us from making bad choices.
So this is the base of the teaching, first we have to bring the mind back or keep the
mind from leaving the present moment or leaving the reality in front of us and we have
to focus on the reality to see it as it is and then we have to see and to know it and
to understand it as it is.
This is the progression of our practice.
The actual practice, the technique of the practice is what I was asked to talk about today
is based on the four foundations of mindfulness which is, it's actually a subjective teaching
of the Buddha, there's nothing in reality that you can point to and say these four foundations
of mindfulness, it's just a, it's actually kind of a strange sounding title.
That means the four categories of experience that you can establish mindfulness on, it's
important to have categories or it's important to be able to name out what is ultimate
reality so that you know what is within limits and what is outside of limits for beginning
meditative is important.
So we take these four to be limits, anything outside of these is not an objective entity,
it's not something we should focus on and by separating them into four they, it's easy to
tell what's what, or it's easier to recognize things as they are rather than just lumping
them all together into one category of experience so we break experience up like this.
It's quite useful especially for a beginner meditator you give them four things and they
have something to work with.
So these four are actually the most important thing for a beginner meditator to learn
and so we often have people even memorize them and I'll go through them so it should
be fairly easy to remember, these are the body, the feelings, the mind and the dumbness
or the reality and it's a little bit of a, have to explain the force one a little bit.
But the first three are fairly self-explanatory, the first one is the body and it doesn't
or not exactly the body, it means body or bodily experiences because the body is still
just a concept but in regards to the body or bodily experiences, in regards to body there
are bodily experiences, there is the experience of sitting for example, what is the experience
of sitting while there's pressure in the back and there's a softness on the seat that
we're sitting on, there's attention in the legs, there are various aspects to the sitting
there's even a heat in the body that comes from the contact with the seat and is generated
from the body itself or there's cold from the air around us, there's a physical experience
and we sum that all up by sitting sitting but actually sitting doesn't really exist, it's
just a name for a certain experience and they're standing, they're walking, they're lying
down, these are the basic aspects of physical experience, bending, stretching, turning, reaching,
grasping, eating, chewing, swallowing, brushing your teeth, washing your hair, using the
toilet, all of this is physical experience, you can even meditate on the toilet, the Buddha
himself said it, if you're clearly aware of the physical experience, this is body.
One of the things that we focus on is the stomach, it's a good example because it's a part
of the body that's moving or that's a bodily experience and it's always present or most
of the time is clearly perceptible, so when the stomach rises we just recognize it as
rising and when it falls, as falling this is a basic meditation exercise using the body.
The second one by feelings we don't mean mental judgments or so and we mean simple sensations,
painful sensations, pleasant sensations and neutral sensations, so it could be pain in the body,
this is an obvious one, if you recognize the pain as pain, you remind yourself it's pain,
this is mindfulness of the feelings, if you feel happy and you say you remind yourself
that it is happy and keep your mind focused on it as it's there or pleased or pleasure
or even just feeling, or if you feel calm and you remind yourself calm, this is
mindfulness of the feelings.
When you're thinking about the past or the future and you have thoughts, this is the mind,
when you're thinking dreaming or planning or fantasizing or reminiscing any kind of
discursive thought, any kind of thought at all, the meditation isn't trying to get rid
of the thought, it's not trying to get rid of anything, so this is, as I said before
this kind of like a study, remember what we're trying to gain here, we're trying to gain
first morality, so when thinking arises, well that's still reality, but when you get lost
in it, when you start to perceive it as being an issue or a problem or a story or a fantasy,
then you're outside of reality, so we have to catch the thoughts and see them just as thinking,
when you remind ourselves thinking and trying to train the mind to only see and is thinking,
this is bringing the mind back to reality, this is morality, bringing the mind back to reality,
and then once you see the reality, see the thoughts arising and see, this is your focus,
your focus on them as they arise and see, and the wisdom is seeing that thoughts arise
and see some coming to understand that thoughts arise and see if they're not me, they're
not mind, they're not under my control, they're not worth pursuing, they're not worth
clinging to, they're just reality, they come in the going, and it's this realization
of the pure state of awareness in the present moment, not judgmental and not distracted
or lost or confused or muddled by thoughts and by concepts.
So these three are pretty self-explanatory, you can go back and forth with them, the fourth
one isn't mysterious or anything, it just means it's kind of like et cetera or extras
or sets of the Buddha's teaching you might even say, that progress one further towards
enlightenment.
So the first set is the five hindrances, they're emotive states that will arise and get
in your way and keep you from seeing clearly, so they're kind of extra from the other
three.
So it means in the mind there will be liking and disliking, you'll be focusing on the
body, watching the stomach rise and fall or the feelings or so, and then there will be
a liking or a disliking, maybe you'll feel bored or maybe you'll feel scared or maybe
you'll feel frustrated or even a great anger can come up if it's stuck inside.
On the other hand, you might be liking, you might like the meditation or there might
be wanting, wanting food or wanting pleasure, wanting this or that or liking some experience
that arise, it's maybe some thoughts or fantasies arise and you like them or some pleasant
feelings arise in the body and you like them or you hear something and you like that.
Now this liking and disliking are things that take us out of the present moment, take us
out of reality, you're no longer studying, you're no judging and you're cultivating habits
of judgment.
This liking doesn't lead just to getting, it leads to more liking, it's habitual, disliking
is the same, it leads to anger, it's liking, you have more you give rise and give
and more you cling to the disliking, the more this liking there will be, the more angry
a person you become.
These are hindrances, they stop us from seeing things clearly, they cloud the mind, they
lead the mind to, lead the mind out of reality and into a muddled state and they confuse
the mind, they tire the mind, they tax the mind's strength and they prevent one, the most
important is they prevent you from seeing clearly so again we're not judging them but we're
learning about them, we're studying, we're learning how they come and how they go, what
makes them arise, what makes them cease, we're coming to understand everything about them,
that's all we need.
As I said, the theory is that once you, and the observation, the observation is that
once you understand something, once you understand something to be to your detriment, you
abandon it without thought, without even having to consider, without effort, simply
through understanding the mind, when understanding arises in the mind, understanding
of the problems with negativity or with clinging in any case, then there will arise
no clinging, the mind will change, the mind changes based on the habit.
The liking, disliking in other words, drowsiness or laziness, so this is a hindrance because
it keeps you from seeing things, keeps you from jumping, from going out to the object,
from perceiving the object clearly, distraction when the mind is not focused, so laziness
or this data feeling laziness too much focus or too much concentration, distraction
is when you don't have enough concentration, we have to balance, let's find the balance
where we're not too focused, laziness actually isn't a lack of energy, it's a excess
of concentration, so you can actually pull yourself back and you can cultivate the energy
that's missing by seeing it, by noting to yourself drowsy and drowsy, distraction
or noting to yourself distracted or worried if you're worried and your mind is not able
to stay firm, it's a worried worried, if you have doubt or confusion, say to yourself
doubting, doubting, the underlying technique here is just this noting, this reminding
yourself, the best we don't understand is reminding yourself because we're trying to use
something that's broken to fix itself, our mind is, in a sense, broken, by broken means
we do things that cause our suffering, we give rise to unwholesome thoughts, thoughts that
are not to our benefit or to our detriment, if you're not such a person then we're
considered even lightened and you don't have to practice, but this is the purpose of
meditating to change or to remove or to understand at least what it is that we're doing
to ourselves, but to do this we have to use the mind, the mind which is subjective,
which is judgmental, which is partial, so how do we break out of that?
We need some means of coming outside of our own habits, our own mode of behavior and
this is with the reminding us, we have this objective tool that never changes, you can't
trick yourself into using some other word and when you feel pain saying happy, happy
or something like that, if you understand the technique, you have to say to yourself pain,
you have to remind yourself and you have to confirm in yourself a firm pain, the firm
the reality of it, it's similar to this positive affirmation stuff, it's totally different,
but it's a similar concept and it's kind of counterintuitive, we think that what you should
be saying is happy, happy or no pain, no pain, when you feel angry you should be saying
love, love, to try to counter it, this is if you were trying to create some state, some
opposite state, we're not trying to do that, we're trying to understand or to focus on
the bad stuff, and it seems counterintuitive, you think why would you focus on the bad stuff,
well why would a doctor focus on the sickness, same sort of thing, we have a problem,
we are, our mind is not tweaked correctly, it needs to be adjusted, so we have to look
and see where it needs to be adjusted, but we have to do it objectively and that's what
this word does, so with the body we can just focus on the sitting and say to ourselves
sitting, or we can focus on the stomach, the basic exercise we give to meditators who
come for an intensive course who's watching the stomach, rising and falling and just
remind themselves, rising, falling, of course you don't say these words out loud, you're
just in your mind reminding yourself, so as to cut off or preclude any judgment, any thoughts
of liking or disliking the experience or identifying with the experience or any discursive
or extraneous thought, so when you feel pain and you say pain, this is learning about
not just the pain, but also learning about our reactions to it, and once you see that
your reactions are the problem and it's actually, you're disliking of the pain that's
the problem, you come to see that pain is really just pain and as you say to yourself pain,
pain, you're teaching yourself something, whereas without looking you would be upset about
the pain, once you look at it and understand it as pain you realize for yourself it is
just pain, there's nothing good or bad about it, and the same with happy feelings as
well instead of clinging to them and wishing for them and hoping for them and feeling depressed
and longing for them when they're gone, we see them just as happy feelings and we realize
there's no benefit that comes from clinging to them, they might be good but as soon as
you say that they're good you start clinging and when you cling then you suffer when
they're gone, you have this partiality where your happiness depends on them, so we stop that,
we know them just as happiness and you're thinking and then you say to yourself thinking
thinking and thoughts as well have no power over you, good thoughts, bad thoughts, good
thoughts don't take you away bad thoughts, don't upset you and even with the hindrances
once we say to ourselves liking, liking or disliking, disliking, disliking, this is the means
of understanding these things, sometimes it feels like they get worse when you acknowledge
them and you say to yourself angry, angry, sometimes it feels like you get more angry
and this is how it should be in the beginning because we have anger inside of us and our
reaction to anger is to get angry, we have to see this, we have to see what we're doing
to ourselves, it's actually the best thing for us once we watch the anger and we come
upset by it, once we realize how horrific this anger is and how it can build and so on,
this is exactly what we need to see, it shouldn't build, it should get out of hand so that
we can see how out of hand gets and really want to change, deep down the mind will, this
will affect the mind when it sees how strong this anger is, the problem with anger is
and then it comes up, the problem is that we don't understand it when it arises and so
we follow it, we make use of it, we use it as a premise by which to act, I'm angry
therefore I should hit you or therefore I should yell at you, there's before I should
do something nasty for you towards you, we don't ever understand the anger, let it come,
this is important, it's important to teach your mind lesson, it's going to get angry, well
let it see what anger is, look at the anger, let it eat its own dog food, so they say.
So these are the four foundations, in brief this is all you need to, no depractist meditation,
it's really almost enough, there's only one other thing that I'd add and that's the
next set in the dumbest is the senses, so other things that might come up are seeing,
hearing, smelling, tasting, feeling and thinking, thinking we already have, but the other
five senses we have to note as well, seeing and hearing, all of these senses can give rise
to partiality or conceptual thought, we see something we begin to think about it, when
we hear something we begin to think about it, so we get lost from the meditation, we lose
our morality in a sense, because we lose our morality, we have no concentration, no focus,
we have no focus, we lose our wisdom, we're in the realm of delusion, where it's very
easy to give rise to judgment and partiality, so we bring ourselves back when we see,
we catch it just at the door and say to ourselves seeing, seeing, when we hear something
hearing, when we smell or taste or feel, is it smelling, smelling, tasting, feeling and
when we think, of course we know thinking, and in this way we've kind of covered all of
our basements, there's nothing that could arise as long as we stay, the Buddha was emphatic
about this, in fact he said, if you stay within this boundary of the four foundations
of mindfulness, nothing can get you, Mara can't get you, evil cannot reach you, no trouble
can come to you, it's a very powerful statement and it seems hard to believe, until you understand
what is reality and you realize that all of your problems are outside of this, that there's
no problems in any of these things, there's no issues in any of these things, the problem
is in identifying with them, making more of them than they are projecting, extrapolating
upon them, once they are what they are and that's all they are, there is no potential
for suffering, there's no potential for clinging, there's no potential for expectation.
So this is all the practice really, it's a training and a studying and a learning process.
You want to be learning as much as you can, it's not something you should be, meditation
isn't something you should be using to escape, it should be a laboratory where you study
the things you don't dare study anywhere else, people who have anxiety or phobias or depression
or anger, they don't dare to look at these things, anything to get away from them, how can
I cure them, this is why people start to take pills, take medication, it's apparently
a very common thing, I guess we don't realize how common it is but probably a lot of the people
we know are on some sort of medication for one or another mental illness, we often think
maybe it's only me but I'm not taking them but obviously but it's quite a common thing
and we do this trying to get away from them, so meditation is a way to stop that, it's
a controlled means of approaching these issues, these problems, you see we are sitting on
a mat, we can let them arise without much to fear, if we have anger issues then we can
let anger arise because there's no one around to get angry at, if we have fear or worry
and now we can be afraid and worried because it's really inconsequential, we can look at
the worry, we don't have to act out on it and we don't have to take some kind of medication
to get over it, it's like we want these things to come up that we've never allowed ourselves
to look at, so it's quite an exploration, it's something very unique and not very well
understood, we usually think of meditation as some kind of retreat or an escape that certainly
isn't, it's an advance, we are taking a leap into the unknown where we've never really
gone before and this unknown isn't something mysterious, it's who we are, it's our everyday
life that we never really look at, it's like tuning the machine or our automobile going
under the hood, we drive the car every day and we often don't spend enough time tuning
it or cleaning it, if you go around in your car and you never change the oil eventually
it burns up, you know, I'm an engine trouble, it's exactly how the mind is, we don't
spend the time, we spend all the time using our mind, we use our mind for so many things
but we don't spend enough time servicing the mind, so this is kind of the servicing,
removing all the bad stuff and cultivating all the good stuff.
So that's basically how to meditate, this is sitting meditation, there's also, you can do
it walking, so when you walk, I've given instructions in my booklet, you can read that,
when you walk, you know the right foot is moving, step being right, it's just a technique
but the, the, the, or it's a structure, the technique is the same, the noting is, reminding
yourself is the same, if you use this technique of reminding yourself, you can even use
it in daily life when you're sitting in a car, when you're, you're sitting on the bus,
so when you're in at work and you want to take a break, you can do a five minute meditation
break, you can, when you're eating, you can do eating meditation, chewing, chewing, swallowing,
tasting, tasting, anything can become meditation, there's the formal aspect of it isn't
so important, it's just a sort of, in the sense of retreat from, from the whole spectrum
of experience to, to keep, keeping it in just a couple of objects, so walking back and
forth is quite simple, gives you a much, much more controlled environment in which
to study the experience, otherwise the technique is simply this, reminding yourself
and based on these four foundations of mindfulness that Buddha taught us for, for aspects
of existence, aspects of experience, okay, so let's take that as a basic understanding
of meditation and we can try to, you know, do our, we'll do our daily meditation and
try to put this into practice.
