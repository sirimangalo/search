1
00:00:00,000 --> 00:00:06,020
Okay, I was watching your meditation for kids video. You said that meditation will help you read your

2
00:00:06,020 --> 00:00:11,960
help, you read minds and how to body experiences, how long does it take to have, how long does it take

3
00:00:11,960 --> 00:00:19,000
for these things to occur, one year, two years, never?

4
00:00:19,000 --> 00:00:26,000
Well, some children may have these experiences, not truly. I know this because I had one.

5
00:00:26,000 --> 00:00:30,000
That's the only reference I had and I don't think I'm special in any way.

6
00:00:30,000 --> 00:00:37,000
I may have had something of the idea of being interested in meditation, but I think there's a lot of kids out there who are like that.

7
00:00:37,000 --> 00:00:43,000
And so they may have these experiences and by practicing meditation they may have these experiences.

8
00:00:43,000 --> 00:00:47,000
The other thing in Thailand this happened a lot. Kids would have a lot of strange experiences.

9
00:00:47,000 --> 00:00:54,000
They would remember past lives or have strange out of body experiences. They would come to the monastery in groups.

10
00:00:54,000 --> 00:00:58,000
The problem was, of course, that the monks were encouraging them in this.

11
00:00:58,000 --> 00:01:04,000
And they were really interested in saying, oh, it's a big deal. This person had some experience like this.

12
00:01:04,000 --> 00:01:08,000
So I don't want it to sound like this is really important and a big deal.

13
00:01:08,000 --> 00:01:12,000
I think it's important to talk about these things.

14
00:01:12,000 --> 00:01:22,000
And the best reason why we should talk about these things is to show how serious it is.

15
00:01:22,000 --> 00:01:25,000
This isn't something that's going to bring just stress relief.

16
00:01:25,000 --> 00:01:28,000
This is really going to profoundly change your mind.

17
00:01:28,000 --> 00:01:37,000
Meditation and the mind is something that is something that is real, that is universal.

18
00:01:37,000 --> 00:01:46,000
And so it's not we have a human mind and there are monkey brains and animal minds and so on.

19
00:01:46,000 --> 00:01:49,000
The mind is something that is incredibly profound.

20
00:01:49,000 --> 00:01:53,000
It can go in so many different directions.

21
00:01:53,000 --> 00:01:59,000
So it kind of puts the meditation in perspective because actually what we're trying for is beyond these things.

22
00:01:59,000 --> 00:02:05,000
Some people will gain these powers on their way to become enlightened.

23
00:02:05,000 --> 00:02:10,000
It's like a neat experience they have along the way.

24
00:02:10,000 --> 00:02:14,000
Of course, some people get caught up in them and practice just for this.

25
00:02:14,000 --> 00:02:21,000
I have an interesting anecdote when I was teaching in the Federal Detention Center in Los Angeles.

26
00:02:21,000 --> 00:02:24,000
If you can guess what the guys wanted, I was teaching the men.

27
00:02:24,000 --> 00:02:31,000
One of the guys came up to me and said, so I heard in meditation you can leave your body and fly.

28
00:02:31,000 --> 00:02:37,000
Because these guys are locked up with a bunch of guys.

29
00:02:37,000 --> 00:02:44,000
Mostly drug dealers, I think.

30
00:02:44,000 --> 00:02:46,000
And this one guy he wanted out.

31
00:02:46,000 --> 00:02:48,000
He wanted to get out of there.

32
00:02:48,000 --> 00:02:52,000
So I actually started teaching in my taught him a specific meditation for it.

33
00:02:52,000 --> 00:02:59,000
Why I did it is because I knew this guy was never going to really be interested in letting go.

34
00:02:59,000 --> 00:03:04,000
But that I could show him how the mind works and I could help him because in order to attain these things,

35
00:03:04,000 --> 00:03:07,000
you need a certain clarity of mind.

36
00:03:07,000 --> 00:03:14,000
So I wanted to at least show him how the mind works and give him a feeling of how far the mind can go.

37
00:03:14,000 --> 00:03:17,000
There is some limited appeal there.

38
00:03:17,000 --> 00:03:20,000
So I gave him this meditation to practice.

39
00:03:20,000 --> 00:03:25,000
He didn't ever learn to fly through the air and he was disappointed, I think.

40
00:03:25,000 --> 00:03:27,000
Someone criticized in that way.

41
00:03:27,000 --> 00:03:32,000
They said, you know, telling people about these things, they're just going to get disappointed when these things don't come.

42
00:03:32,000 --> 00:03:39,000
And yeah, most people will be disappointed because to answer your question, for most people these things will never occur.

43
00:03:39,000 --> 00:03:42,000
I mean, I haven't had such an experience since.

44
00:03:42,000 --> 00:03:46,000
Mind you, we don't practice that type of meditation and don't teach it.

45
00:03:46,000 --> 00:03:52,000
So I don't expect that any of my students would have these experiences either.

46
00:03:52,000 --> 00:03:59,000
But, you know, even people trying for them, it's sound from the sounds of it.

47
00:03:59,000 --> 00:04:01,000
Most people don't get it.

48
00:04:01,000 --> 00:04:11,000
If you want that kind of experience, you'd really have to go off into the forest and develop high states of tranquility.

49
00:04:11,000 --> 00:04:14,000
Incredible power of mind.

50
00:04:14,000 --> 00:04:17,000
I can tell you how I did it.

51
00:04:17,000 --> 00:04:18,000
Did I tell this already?

52
00:04:18,000 --> 00:04:20,000
I told this already in the astral body one.

53
00:04:20,000 --> 00:04:22,000
You can watch my video on astral projection.

54
00:04:22,000 --> 00:04:26,000
I'll explain how I had an astral projection when I was young.

55
00:04:26,000 --> 00:04:29,000
It was an interesting experience.

56
00:04:29,000 --> 00:04:39,000
But another reason for talking about these things is to break the taboo and break through this mystery of these things and how it seems.

57
00:04:39,000 --> 00:04:44,000
On the one hand, people are totally skeptical and criticize.

58
00:04:44,000 --> 00:04:50,000
How can you talk about such fantastical, impossible experiences like that?

59
00:04:50,000 --> 00:04:54,000
Which, of course, they're totally reasonable, rational and scientific.

60
00:04:54,000 --> 00:05:02,000
If you look at them in the right way, people have near-death experiences because the mind is so focused.

61
00:05:02,000 --> 00:05:08,000
Near-death, they leave their bodies and they're able to look down and see their body being operated on.

62
00:05:08,000 --> 00:05:17,000
They're documented cases of this, which, of course, materialists don't accept.

63
00:05:17,000 --> 00:05:28,000
So, it's really not something, it's not something difficult to understand or it's not something that we should be truly skeptical about.

64
00:05:28,000 --> 00:05:33,000
We should have an open mind to the fact that there's a lot of people that they're who claim to have had these experiences.

65
00:05:33,000 --> 00:05:37,000
And they really probably aren't all lying about it.

66
00:05:37,000 --> 00:05:39,000
It doesn't make much sense.

67
00:05:39,000 --> 00:05:42,000
Who could be possible?

68
00:05:42,000 --> 00:05:53,000
No, there's a story, there's a documented case of this, I think it was even performed with a group of doctors who had this woman,

69
00:05:53,000 --> 00:06:02,000
lie down on her back and they put a shelf above her with a five-digit number written on a piece of paper.

70
00:06:02,000 --> 00:06:05,000
And they asked her to read the paper.

71
00:06:05,000 --> 00:06:09,000
She's lying down in bed and the shelves above her and the piece of papers on top of the shelf.

72
00:06:09,000 --> 00:06:13,000
And not every time she was able to do it.

73
00:06:13,000 --> 00:06:19,000
They then put the paper, wrote something on the paper in another room and she wasn't able to do it.

74
00:06:19,000 --> 00:06:32,000
So, they had some interesting results that she was able to do it in a limited sense, but she wasn't able to fly through the door for some reason.

75
00:06:32,000 --> 00:06:39,000
That's an example. And the other reason is so that people like this are able to put them into context as well.

76
00:06:39,000 --> 00:06:43,000
Because, yeah, it sounds neat and a lot of people will become obsessed with these.

77
00:06:43,000 --> 00:06:46,000
They think, yeah, I want to meditate so I can gain these magical powers.

78
00:06:46,000 --> 00:06:48,000
But we have to talk about them.

79
00:06:48,000 --> 00:06:50,000
We have to put them in their place.

80
00:06:50,000 --> 00:06:54,000
Yes, it's possible to gain these things, but to what end?

81
00:06:54,000 --> 00:07:01,000
Or to point out that it's really only just a neat experience that shows something important, which is the power of mind.

82
00:07:01,000 --> 00:07:04,000
But, you know, so there's power.

83
00:07:04,000 --> 00:07:06,000
Nuclear bombs have power.

84
00:07:06,000 --> 00:07:08,000
It doesn't really mean anything.

85
00:07:08,000 --> 00:07:11,000
Magical powers have the same, I think.

86
00:07:11,000 --> 00:07:16,000
But to answer your question directly, how long will it take you to gain these things?

87
00:07:16,000 --> 00:07:18,000
It depends really on your life.

88
00:07:18,000 --> 00:07:24,000
Living as you do, as I understand, you live in the world and have a job and so on.

89
00:07:24,000 --> 00:07:33,000
Not, not, not terribly likely to occur in your lifetime, in this life, in a life that you lead now.

90
00:07:33,000 --> 00:07:37,000
If you come to the forest and you're not this forest because I won't teach you them,

91
00:07:37,000 --> 00:07:44,000
if you go to a forest and develop them, you know, develop the genres, develop according to the texts.

92
00:07:44,000 --> 00:07:51,000
Because there are incredibly detailed and systematic practices that you can undertake to gain all of these magical powers.

93
00:07:51,000 --> 00:07:58,000
Then, I would say a year is a good estimate to get some interesting powers.

94
00:07:58,000 --> 00:08:02,000
Read, read minds or remember your past lives.

95
00:08:02,000 --> 00:08:05,000
Leave your body, I think that's probably a simple one.

96
00:08:05,000 --> 00:08:07,000
Looking back, it wasn't that difficult.

97
00:08:07,000 --> 00:08:09,000
It took me a few months, I think.

98
00:08:09,000 --> 00:08:12,000
And I wasn't even trying, it just happened.

99
00:08:12,000 --> 00:08:22,000
Anyway, so, answer.

