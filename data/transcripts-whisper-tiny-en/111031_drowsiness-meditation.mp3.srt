1
00:00:00,000 --> 00:00:08,000
Question, to extend the question about sleeping, I get tired when meditating, is it better to sleep in when I'm not sleepy than meditating?

2
00:00:08,000 --> 00:00:19,000
Well, sleepyness is actually a great object of meditation. It's something that is overlooked, I think, by many meditators.

3
00:00:19,000 --> 00:00:28,000
Yeah, sometimes you have to just fall asleep, but the best thing is for that to come naturally. If you fall asleep, you fall asleep.

4
00:00:28,000 --> 00:00:36,000
When you're sleepy, first before you go to sleep, try to focus on the sleepiness, focus on the tired people.

5
00:00:36,000 --> 00:00:50,000
Because it's difficult in daily life or when you're not in a meditations center, because you really are exhausted a lot of the time from work and just thinking a lot and so much activity.

6
00:00:50,000 --> 00:01:02,000
But you can try it, focus on the tiredness and you'll find that sometimes it gives you energy. Sometimes you're able to burst the bubbles of the speaking. It just disappears. Suddenly you're not tired anymore.

7
00:01:02,000 --> 00:01:17,000
And you'll find you have suddenly a lot of energy and you're able to do really good meditations and drowsiness is a hindrance. It's something that drags you down and stops you from meditating if you're able to pierce it and overcome it.

8
00:01:17,000 --> 00:01:25,000
You'll find that suddenly your mind is clear and your practice goes a lot better and meditation can be quite fruitful after that point.

9
00:01:25,000 --> 00:01:36,000
So we should be careful not to forget about the state of mind that is fatigue or drowsiness.

10
00:01:36,000 --> 00:01:47,000
And forget, we shouldn't forget that it's a part of the meditation when it comes up, it's a valid objective of observation. It's a good one too.

11
00:01:47,000 --> 00:01:55,000
There's two ways. One way is you'll get a lot of energy by focusing on by acknowledging, for example, tired and tired.

12
00:01:55,000 --> 00:02:03,000
And the others, you'll just fall asleep. If you're really good, it'll be one or the other. And neither is a problem.

13
00:02:03,000 --> 00:02:10,000
The problem with sleeping first and then meditating is that sleep drains your awareness and your mindfulness.

14
00:02:10,000 --> 00:02:18,000
We try to have meditators sleep less and less and sometimes they don't even sleep at all, they'll just do meditation.

15
00:02:18,000 --> 00:02:26,000
Because sleep is an uncontrolled state. It's actually just a construct. Sleep doesn't have any ultimate reality.

16
00:02:26,000 --> 00:02:38,000
We think of sleep as being a necessary part of reality, but it's only a conditioned state that we've developed as human beings, or as I suppose most animals have developed.

17
00:02:38,000 --> 00:02:46,000
But it's not a part of ultimate reality. It's nothing intrinsic in the mind.

18
00:02:46,000 --> 00:03:03,000
It's just an aspect of this period of our existence that we have a very coarse body and it falls asleep sometimes.

19
00:03:03,000 --> 00:03:21,000
During the time that we're asleep, the mind is in a specific state that's not very wholesome. It's very free to do what it wants and to make assumptions and conclusions.

20
00:03:21,000 --> 00:03:38,000
You know, people will commit dastardly deeds in their dreams. It can happen. You can do all sorts of bad things. You can do good things, but sometimes you fight and kill and so on, depending on the state of mind.

21
00:03:38,000 --> 00:03:51,000
So it's possible actually to develop. It usually develops in wholesomeness when you sleep. Most people develop on wholesomeness. So when they wake up, the mind is not fresh. The mind is not clear. It's clouded and deluded.

22
00:03:51,000 --> 00:04:01,000
You can feel that. If you're in a meditation center, you can feel that after you sleep. You don't feel better. You feel actually, let's sharpen the less clear.

23
00:04:01,000 --> 00:04:12,000
So, you know, you have to sleep as human beings we do have to sleep. But that's the important point here to try to sleep mindfully.

24
00:04:12,000 --> 00:04:22,000
Before you sleep, do meditation, whether it's lying meditation, saying to yourself, rising, falling, or whether it's some sitting meditation before you lie down.

25
00:04:22,000 --> 00:04:35,000
That'll help your sleep to be a lot more quiet and a lot clearer. You find you don't dream so much your mind is not so distracted when you sleep. And you wake up a lot more refreshed.

26
00:04:35,000 --> 00:04:59,000
And you can actually find that sleep is as much more beneficial. And it's not as much of a hindrance to your meditation.

