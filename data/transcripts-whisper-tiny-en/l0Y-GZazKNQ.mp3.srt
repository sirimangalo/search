1
00:00:00,000 --> 00:00:04,960
There's one while meditating, my mind seems to, at times, multitask.

2
00:00:04,960 --> 00:00:09,120
I can be staying with the breath, thinking about tomorrow,

3
00:00:09,120 --> 00:00:12,640
and being aware that my mind is straying at the same time.

4
00:00:12,640 --> 00:00:14,240
One can be done about this.

5
00:00:17,360 --> 00:00:24,880
Well, it's not at the same time, but you can note it as best it can.

6
00:00:24,880 --> 00:00:29,680
You only have one experience at any given point.

7
00:00:29,680 --> 00:00:38,720
But hopefully, the massy side, I trust that once you become better with the meditation,

8
00:00:38,720 --> 00:00:45,120
you're noting, at some point, you're noting sometimes won't be able to keep up,

9
00:00:45,120 --> 00:00:49,680
so you just do the best you can with keeping up with the noting and your experience.

10
00:00:49,680 --> 00:01:00,800
And then, eventually, it'll be fucking off. The more you do it.

11
00:01:03,680 --> 00:01:09,120
I mean, don't be so hard on yourself, just what you're coming to see is the chaos in the mind.

12
00:01:09,120 --> 00:01:10,960
This is what will help to shrink the ego,

13
00:01:10,960 --> 00:01:17,920
to realize that you're really not as clever as you think you are. Your mind is all chaotic and messed up.

14
00:01:19,600 --> 00:01:24,080
And when you realize that and you realize that that's inevitable, then you don't worry about it.

15
00:01:24,800 --> 00:01:29,760
You don't, on the other hand, hate yourself, because you see that it's really none of your business.

16
00:01:29,760 --> 00:01:39,760
It's not controllable.

