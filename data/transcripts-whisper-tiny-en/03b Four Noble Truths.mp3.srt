1
00:00:00,000 --> 00:00:07,000
So, when you take these steps, you, you purify your moral contact and so these three are there.

2
00:00:07,000 --> 00:00:14,720
So, these three are not actively working at the moment of Wipasana, but since they are

3
00:00:14,720 --> 00:00:20,000
accomplished before Wipasana, they are said to be working maybe in the background.

4
00:00:20,000 --> 00:00:26,000
So, the five factors, right understanding, right thought, right effort, right mindfulness,

5
00:00:26,000 --> 00:00:32,000
right concentration are called the rock factors, working factors.

6
00:00:32,000 --> 00:00:40,000
So, the other ones that should be working harmoniously so that you get good concentration,

7
00:00:40,000 --> 00:00:52,000
good meditation and good understanding and right knowledge of the true nature of mind

8
00:00:52,000 --> 00:01:03,000
and matter. Okay, these are the four noble truths and the eight factors of the, the

9
00:01:03,000 --> 00:01:06,000
fourth noble truth in some detail.

10
00:01:06,000 --> 00:01:13,000
So, you may, you may read other books if you want to know more about and more detail

11
00:01:13,000 --> 00:01:19,000
about and the eightfold path and the four noble truths.

12
00:01:19,000 --> 00:01:28,000
Now, this is the course on fundamentals of Buddhism and so I think it should be enough

13
00:01:28,000 --> 00:01:33,000
with just this much exposition.

14
00:01:33,000 --> 00:01:43,000
So, today we finished our talk on the four noble truths and these are the four noble truths

15
00:01:43,000 --> 00:01:49,000
discovered by the Buddha and revealed to the world.

16
00:01:49,000 --> 00:01:57,000
And among these four noble truths, we should be more concerned with the fourth noble

17
00:01:57,000 --> 00:02:08,000
truth because it is a fourth noble truth that will lead us to the cessation of suffering.

18
00:02:08,000 --> 00:02:20,000
So, may we all practice the fourth noble truths and reach the highest goal in this very life.

19
00:02:20,000 --> 00:02:42,000
Now, you know the eighth factor of path which is right concentration.

20
00:02:42,000 --> 00:03:02,000
So, when Buddha explained that right concentration Buddha mentioned the four Janas, now the commentary

21
00:03:02,000 --> 00:03:13,000
explained that these four Janas are not only mundane Janas, but also we must take to have

22
00:03:13,000 --> 00:03:16,000
these Janas to remain super mundane Janas.

23
00:03:16,000 --> 00:03:29,000
So, the right concentration means not mundane Janas as we might think at the first glance,

24
00:03:29,000 --> 00:03:36,000
but we must understand that it includes although they super mundane Janas.

25
00:03:36,000 --> 00:03:45,000
Now, in order to understand it first, you should understand the Janas.

26
00:03:45,000 --> 00:03:53,000
Now, Janas are the higher states of consciousness.

27
00:03:53,000 --> 00:04:01,000
Now, these are experienced by those who practice samata meditation.

28
00:04:01,000 --> 00:04:12,000
So, Janas can be obtained through the practice of samata meditation.

29
00:04:12,000 --> 00:04:28,000
And, these Janas take as object that is the object of the practice leading to the attainment

30
00:04:28,000 --> 00:04:35,000
of channel or the object of the samata meditation.

31
00:04:35,000 --> 00:04:43,000
For example, he doesn't practice as samata meditation taking the earth disk as an object.

32
00:04:43,000 --> 00:04:50,000
So, he looks at the earth disk and then he memorizes it and let us see, he has memorized

33
00:04:50,000 --> 00:05:02,000
it so completely that he could see that the disk or he could take the image of that disk

34
00:05:02,000 --> 00:05:10,000
in his mind and then he does on it again and again so that it becomes more refined.

35
00:05:10,000 --> 00:05:16,000
And then he does on it again and then he gets Janas.

36
00:05:16,000 --> 00:05:27,000
So, the Janas when it arises takes that object as an object, the refined mental image

37
00:05:27,000 --> 00:05:34,000
of the earth disk. This is just an example that are many, many objects also.

38
00:05:34,000 --> 00:05:51,000
So, when Janas arises, it takes that object, the mental image as an object and it pushes

39
00:05:51,000 --> 00:06:00,000
the mental refinements away for some time. That means, Janas cannot eradicate, cannot

40
00:06:00,000 --> 00:06:08,000
destroy any of the mental defilements, but Janas can push these mental refinements away

41
00:06:08,000 --> 00:06:12,000
for some time so that they do not arise in him for some time.

42
00:06:12,000 --> 00:06:18,000
So, that is the nature of mundane Janas.

43
00:06:18,000 --> 00:06:30,000
So, again, mundane Janas take the respective objects as object and they just push the

44
00:06:30,000 --> 00:06:38,000
mental defilements away for some time. They cannot eradicate, that means, destroy once

45
00:06:38,000 --> 00:06:42,000
and for all any mental defilements.

46
00:06:42,000 --> 00:06:53,000
And when a person is Janas, he experiences happiness, he experiences peacefulness.

47
00:06:53,000 --> 00:07:03,000
The peacefulness he experiences during the time of Janas is superior to the heaviness,

48
00:07:03,000 --> 00:07:21,000
he enjoys experiencing sensual pleasures. So, the heaviness, he experience during the

49
00:07:21,000 --> 00:07:30,000
time of Janas is much better than the heaviness he experience when he enjoys sensual pleasures.

50
00:07:30,000 --> 00:07:40,000
So, this is the nature of Janas, and there are few Janas and in mundane sphere.

51
00:07:40,000 --> 00:07:59,000
Now, the name Janas is given to a combination of some mental states or mental factors.

52
00:07:59,000 --> 00:08:08,000
Now, the first Janas is a name given to a combination of five mental states that are called

53
00:08:08,000 --> 00:08:19,000
Janas factors. And these five mental states are initial application, sustained application

54
00:08:19,000 --> 00:08:29,000
and PT or Rapture and Sukra or heaviness and unification of mind or concentration.

55
00:08:29,000 --> 00:08:37,000
So, when we say first Janas, we mean these five as a group.

56
00:08:37,000 --> 00:08:41,000
And the second Janas consists of the last three.

57
00:08:41,000 --> 00:08:45,000
So, in the second Janas, the first two are missing.

58
00:08:45,000 --> 00:08:53,000
So, the second Janas is a name given to the combination of the last three Janas factors.

59
00:08:53,000 --> 00:09:01,000
That is, PT, Rapture, Sukra, heaviness and concentration.

60
00:09:01,000 --> 00:09:11,000
But that Janas is a name given to the last two factors, heaviness and concentration.

61
00:09:11,000 --> 00:09:18,000
And the fourth Janas is a name given to, again, two factors.

62
00:09:18,000 --> 00:09:26,000
But this time, instead of heaviness or heavy feeling, it is neutral feeling.

63
00:09:26,000 --> 00:09:31,000
So, the fourth Janas consists of neutral feeling and concentration.

64
00:09:31,000 --> 00:09:39,000
So, these five factors are called, five mental states are called, Janas factors.

65
00:09:39,000 --> 00:09:47,000
Now, among these Janas factors, some are also the factor of maga.

66
00:09:47,000 --> 00:10:00,000
The initial application is, right thinking, right thought among the eight factors of path.

67
00:10:00,000 --> 00:10:04,000
You know, there are eight factors of the first one is what?

68
00:10:04,000 --> 00:10:11,000
Right understanding, the second, right thought and then, right speech, right action, right livelihood,

69
00:10:11,000 --> 00:10:14,000
right effort, right mindfulness, right concentration.

70
00:10:14,000 --> 00:10:28,000
So, the first Janas factor, which is Whitaka, which is initial application, is right thought among the eight factors of path.

71
00:10:28,000 --> 00:10:34,000
And then, the second, no corresponding path factor.

72
00:10:34,000 --> 00:10:41,000
And then, the third, no corresponding factor, the fourth, no corresponding factor.

73
00:10:41,000 --> 00:10:44,000
But the fifth, concentration, right?

74
00:10:44,000 --> 00:10:48,000
Unification of mine is actually right concentration.

75
00:10:48,000 --> 00:10:59,000
So, two of the Janas factors can be found in the among the eight path factors.

76
00:10:59,000 --> 00:11:09,000
Now, the third noble truth is a combination of eight factors of path.

77
00:11:09,000 --> 00:11:21,000
Since they are called eight factors of path, they are called factors of path only when they arise with path consciousness.

78
00:11:21,000 --> 00:11:34,000
Now, the problem is how to connect the five mundane Janas with path consciousness.

79
00:11:34,000 --> 00:11:39,000
If they are mundane Janas, they are not path consciousness.

80
00:11:39,000 --> 00:11:43,000
And if it is path consciousness, it is not mundane Janas.

81
00:11:43,000 --> 00:11:46,000
So, how do you connect these two?

82
00:11:46,000 --> 00:11:52,000
Because when Buddha explained the right concentration, he gave these four, four Janas.

83
00:11:52,000 --> 00:12:03,000
So, the commentary said that we must understand here the super mundane Janas also.

84
00:12:03,000 --> 00:12:07,000
Now, what are these super mundane Janas?

85
00:12:07,000 --> 00:12:17,000
Actually, by Janas we normally mean the mundane Janas.

86
00:12:17,000 --> 00:12:25,000
But there can be Janas in super mundane sphere also.

87
00:12:25,000 --> 00:12:28,000
How?

88
00:12:28,000 --> 00:12:34,000
At the moment of enlightenment, as you know, path consciousness arises, right?

89
00:12:34,000 --> 00:12:42,000
So, when path consciousness arises, it arises together with 36 mental factors.

90
00:12:42,000 --> 00:12:54,000
And among the 36, there are initial application, sustained application, PD,

91
00:12:54,000 --> 00:13:00,000
Suka, and concentration, and other factors.

92
00:13:00,000 --> 00:13:12,000
So, when path consciousness arises, all these five Janas factors are with it.

93
00:13:12,000 --> 00:13:25,000
Since, the path consciousness has five Janas, Janas factors with it, the path consciousness is called,

94
00:13:25,000 --> 00:13:29,000
first Janas path consciousness.

95
00:13:29,000 --> 00:13:35,000
That means, path consciousness, which resembles the first Janas.

96
00:13:35,000 --> 00:13:40,000
Because as we know, if it is Janas, it is not Maga, and if it is Maga, it is not Janas.

97
00:13:40,000 --> 00:13:48,000
But here, the Maga consciousness is called, first Janas, because it resembles the first

98
00:13:48,000 --> 00:13:52,000
Janas of mundane sphere.

99
00:13:52,000 --> 00:13:59,000
So, if a person practices Vipasana meditation and gets enlightenment, I mean,

100
00:13:59,000 --> 00:14:07,000
if a person practices pure Vipasana meditation, Vipasana only not combined with Samata.

101
00:14:07,000 --> 00:14:16,000
So, if a person practices pure Vipasana meditation and he gets the path consciousness,

102
00:14:16,000 --> 00:14:20,000
that path consciousness is accompanied by all these five factors.

103
00:14:20,000 --> 00:14:28,000
And so, that path consciousness is said to be, first Janas, path consciousness.

104
00:14:28,000 --> 00:14:34,000
That is, path consciousness, resembling first Janas.

105
00:14:34,000 --> 00:14:46,000
Now, there are persons who practice Samata meditation and who get Janas before the practice Vipasana meditation.

106
00:14:46,000 --> 00:14:59,000
And when the practice Vipasana meditation, they may practice pure Vipasana or they may practice Samata and Vipasana join together.

107
00:14:59,000 --> 00:15:08,000
And he hooked together, that means, pay me, enter into first Janas.

108
00:15:08,000 --> 00:15:10,000
This is mundane Janas.

109
00:15:10,000 --> 00:15:13,000
And then they get out of that Janas.

110
00:15:13,000 --> 00:15:18,000
And practice Vipasana on that first Janas.

111
00:15:18,000 --> 00:15:24,000
And as a result of Vipasana, let us say, path consciousness arises.

112
00:15:24,000 --> 00:15:38,000
So, when path consciousness arises, that path consciousness is accompanied by all five Janas factors with other mental states.

113
00:15:38,000 --> 00:15:45,000
So, his path consciousness is called, first Janas, path consciousness.

114
00:15:45,000 --> 00:15:51,000
That means, path consciousness, which resembles the first Janas.

115
00:15:51,000 --> 00:15:56,000
Again, that person has all four Janas.

116
00:15:56,000 --> 00:15:59,000
So, he may enter into second Janas.

117
00:15:59,000 --> 00:16:04,000
And getting out of second Janas, he may practice Vipasana on second Janas.

118
00:16:04,000 --> 00:16:07,000
And then he gets Maga.

119
00:16:07,000 --> 00:16:16,000
So, his Maga consciousness will be accompanied by only three Janas factors.

120
00:16:16,000 --> 00:16:24,000
Because he made the second Janas, which has only three Janas factors, as a basis for Vipasana.

121
00:16:24,000 --> 00:16:35,000
So, when Maga Jita arises, his Maga Jita is accompanied by three Janas factors and other mental factors.

122
00:16:35,000 --> 00:16:45,000
So, his path consciousness is called, second Janas, path consciousness.

123
00:16:45,000 --> 00:16:51,000
That means, path consciousness, which resembles second Janas.

124
00:16:51,000 --> 00:16:54,000
The same with the third Janas and folk Janas.

125
00:16:54,000 --> 00:17:03,000
So, when we say, first Janas, path consciousness, we really mean it is path consciousness and it is not Janas consciousness.

126
00:17:03,000 --> 00:17:12,000
But it is path consciousness, which resembles, which is like Janas consciousness.

127
00:17:12,000 --> 00:17:18,000
So, they are called super mundane Janas.

128
00:17:18,000 --> 00:17:30,000
Actually, they don't take super mundane Janas, don't take the mental image as object, but they take nibana as object.

129
00:17:30,000 --> 00:17:40,000
So, when these Janas factors arise with path consciousness, they take nibana as object.

130
00:17:40,000 --> 00:17:52,000
But when they arise with Janas consciousness, when they arise as mundane Janas, they take the mental image and so on as object.

131
00:17:52,000 --> 00:17:54,000
So, the object is different.

132
00:17:54,000 --> 00:18:00,000
When they are mundane Janas, they take the mental image and others as object.

133
00:18:00,000 --> 00:18:06,000
But when they arise with path consciousness, they take nibana as object.

134
00:18:06,000 --> 00:18:14,000
And the path consciousness has the ability to destroy all mental defilements,

135
00:18:14,000 --> 00:18:24,000
not just pushing them away for some time, but destroying them altogether so that these mental defilements do not arise again.

136
00:18:24,000 --> 00:18:34,000
And when a person is experiencing the path consciousness and later on fruit consciousness,

137
00:18:34,000 --> 00:18:37,000
he is said to be in bliss.

138
00:18:37,000 --> 00:18:48,000
So, his peacefulness, his heaviness, he experienced at that time is much superior to the experience of Janas,

139
00:18:48,000 --> 00:18:54,000
not to speak of a superior to experience of central pleasures.

140
00:18:54,000 --> 00:19:03,000
So, that is the highest form of heaviness, highest form of peacefulness.

141
00:19:03,000 --> 00:19:10,000
So, mundane Janas and magas differ in this way, right?

142
00:19:10,000 --> 00:19:22,000
mundane Janas take the mental image and others as object and they can push away mental defilements temporarily

143
00:19:22,000 --> 00:19:31,000
and they can give a kind of peacefulness, much superior to ordinary peacefulness.

144
00:19:31,000 --> 00:19:38,000
And maga, we can call them super mundane Janas.

145
00:19:38,000 --> 00:19:48,000
So, super mundane Janas or maga take nibana as object and they are able to eradicate,

146
00:19:48,000 --> 00:19:52,000
destroy once and for all mental defilements.

147
00:19:52,000 --> 00:20:05,000
And the heaviness experience during the time of maga and pala is the best, the highest form of heaviness of peacefulness.

148
00:20:05,000 --> 00:20:08,000
So, they differ in this respect.

149
00:20:08,000 --> 00:20:15,000
So, when you see the explanation given for right concentration as food Janas,

150
00:20:15,000 --> 00:20:22,000
you must understand that these food Janas are both mundane and super mundane.

151
00:20:22,000 --> 00:20:28,000
So, when you take these food Janas to be mundane, they belong to the preliminary stage.

152
00:20:28,000 --> 00:20:35,000
When you take them to be super mundane, they belong to the stage of maga.

153
00:20:35,000 --> 00:20:44,000
Since there are four Janas, both in preliminary stage and at the stage of maga,

154
00:20:44,000 --> 00:20:57,000
the commentary said that there is a variety of Janas, both in the preliminary stage and at the moment of maga,

155
00:20:57,000 --> 00:21:04,000
because there are any one of these four in both cases and both stages.

156
00:21:04,000 --> 00:21:14,000
So, it is not like other factors of fat. For example, right effort.

157
00:21:14,000 --> 00:21:18,000
You remember how many right efforts are there?

158
00:21:18,000 --> 00:21:20,000
Four, right?

159
00:21:20,000 --> 00:21:26,000
So, there are four right efforts to avoid a crucial love,

160
00:21:26,000 --> 00:21:32,000
which is not yet arisen from arriving and then to abandon a crucial love,

161
00:21:32,000 --> 00:21:42,000
which is arisen and then to what we are saying.

162
00:21:42,000 --> 00:21:50,000
The effort for arising of crucial love, which is not yet arisen and the effort for development of crucial love,

163
00:21:50,000 --> 00:21:52,000
which is already arisen, right?

164
00:21:52,000 --> 00:21:54,000
So, there are four kinds of effort.

165
00:21:54,000 --> 00:22:04,000
Now, these four kinds of effort arise during preliminary stage and also at the stage of maga,

166
00:22:04,000 --> 00:22:14,000
but during the preliminary stage, these four come one at a time and not in the order,

167
00:22:14,000 --> 00:22:22,000
but there can be the first effort or second effort or third effort and they take different objects.

168
00:22:22,000 --> 00:22:25,000
The first takes the acoustalized objects, right?

169
00:22:25,000 --> 00:22:28,000
The third takes the acoustalized object and so on.

170
00:22:28,000 --> 00:22:41,000
But when the right effort arises with maga, there is no variety, just right effort.

171
00:22:41,000 --> 00:22:53,000
It arises with maga taking a nibana as object and doing the function of path.

172
00:22:53,000 --> 00:23:05,000
So, in the preliminary stage, there is a variety of efforts, effort number one, number two, number three, number four.

173
00:23:05,000 --> 00:23:14,000
At the stage of maga, there is only one effort in general.

174
00:23:14,000 --> 00:23:22,000
We cannot see that the effort that arises with maga is of the first kind or second kind or third kind of work.

175
00:23:22,000 --> 00:23:25,000
But it is just the effort in general.

176
00:23:25,000 --> 00:23:32,000
So, the comradery said, there is a variety of efforts in preliminary stage,

177
00:23:32,000 --> 00:23:37,000
but at the stage of maga, there is only one effort.

178
00:23:37,000 --> 00:23:41,000
But here, with regard to concentration, it is different.

179
00:23:41,000 --> 00:23:49,000
So, there is a variety of genres, both during the preliminary stage and at the stage of maga.

180
00:23:49,000 --> 00:24:03,000
Let us do some questions. Do genres eradicate mental defalments?

181
00:24:03,000 --> 00:24:04,000
Yes or no?

182
00:24:04,000 --> 00:24:05,000
Yes.

183
00:24:05,000 --> 00:24:13,000
Do genres eradicate describe mental defalments?

184
00:24:13,000 --> 00:24:19,000
It eradicate means, test try altogether. No.

185
00:24:19,000 --> 00:24:27,000
What they do is temporary abandonment.

186
00:24:27,000 --> 00:24:32,000
Push these mental defalments away for some time.

187
00:24:32,000 --> 00:24:39,000
But they cannot eradicate or destroy altogether the mental defilements.

188
00:24:39,000 --> 00:24:42,000
That is one thing.

189
00:24:42,000 --> 00:24:58,000
And the other is, is it essential for a person to get genres before he takes up Vipasana meditation?

190
00:24:58,000 --> 00:25:04,000
Must he get genre before he practices Vipasana meditation?

191
00:25:04,000 --> 00:25:05,000
No.

192
00:25:05,000 --> 00:25:09,000
He may or may not get genres, right?

193
00:25:09,000 --> 00:25:12,000
These are the two things I want to make sure.

194
00:25:12,000 --> 00:25:15,000
I want you to understand clearly, right?

195
00:25:15,000 --> 00:25:20,000
So, genres cannot eradicate or destroy the mental defalments,

196
00:25:20,000 --> 00:25:28,000
but they can push these mental defalments away from the person for some time.

197
00:25:28,000 --> 00:25:35,000
And it is not essential to get genre before you practice Vipasana meditation.

198
00:25:35,000 --> 00:25:38,000
You may get genres before or you may not.

199
00:25:38,000 --> 00:25:41,000
So, these are the two things I want to make clear.

200
00:25:41,000 --> 00:25:47,000
Now, the genres being able to push the mental defalments away from some time, right?

201
00:25:47,000 --> 00:25:52,000
How long?

202
00:25:52,000 --> 00:26:00,000
A few hours, a few days, a few weeks, a month.

203
00:26:00,000 --> 00:26:09,000
No, genres can push longer than that.

204
00:26:09,000 --> 00:26:11,000
You know how long?

205
00:26:11,000 --> 00:26:17,000
60 years.

206
00:26:17,000 --> 00:26:27,000
There was a monk, a very renowned monk, and maybe ancient salon.

207
00:26:27,000 --> 00:26:31,000
And he was a very famous teacher.

208
00:26:31,000 --> 00:26:38,000
And he got all these genres and the mental...

209
00:26:38,000 --> 00:26:43,000
I mean, I have got the magic powers, mental magic powers,

210
00:26:43,000 --> 00:26:47,000
so that he thought that he was an errant.

211
00:26:47,000 --> 00:26:52,000
And he taught many students and many of his students became errant,

212
00:26:52,000 --> 00:26:57,000
but he still remained an ordinary person putojana.

213
00:26:57,000 --> 00:27:02,000
Now, one of his pupils who had become an errant,

214
00:27:02,000 --> 00:27:09,000
read his mind and found out that his teacher was still a putojana.

215
00:27:09,000 --> 00:27:14,000
So, in order to help him, in order to shake him up, he went to the Disha.

216
00:27:14,000 --> 00:27:19,000
And then he, when he met the Disha, the Disha asked,

217
00:27:19,000 --> 00:27:22,000
what do you come here for? And he said,

218
00:27:22,000 --> 00:27:25,000
I want to ask you some questions.

219
00:27:25,000 --> 00:27:28,000
Then the student asked questions,

220
00:27:28,000 --> 00:27:32,000
and the teacher gave the answers right away.

221
00:27:32,000 --> 00:27:37,000
He doesn't have to ask for the answers.

222
00:27:37,000 --> 00:27:45,000
Then the student said, oh, how great, how wonderful.

223
00:27:45,000 --> 00:27:48,000
You can answer my questions right away.

224
00:27:48,000 --> 00:27:52,000
So, when did you reach this stage?

225
00:27:52,000 --> 00:27:58,000
He asked, because the student knew that his teacher thought he was an errant.

226
00:27:58,000 --> 00:28:01,000
So, when did you reach this stage?

227
00:28:01,000 --> 00:28:06,000
Then he said, oh, about 60 years ago.

228
00:28:06,000 --> 00:28:12,000
Because during these 60 years, he did not see any mental confinement arise in his mind.

229
00:28:12,000 --> 00:28:15,000
So, that's why he thought he was an errant.

230
00:28:15,000 --> 00:28:20,000
He was able to push these mandatory filaments away for 60 years.

231
00:28:20,000 --> 00:28:25,000
But then it is not a education by manga.

232
00:28:25,000 --> 00:28:27,000
They can come back.

233
00:28:27,000 --> 00:28:32,000
So, he asked, do you experience,

234
00:28:32,000 --> 00:28:35,000
do you make use of magic powers?

235
00:28:35,000 --> 00:28:38,000
Oh, it's easy for me, I said.

236
00:28:38,000 --> 00:28:40,000
Then the student said,

237
00:28:40,000 --> 00:28:45,000
what is the student's advantage? Please create an elephant.

238
00:28:45,000 --> 00:28:50,000
And it is putting his part about the trunk, right?

239
00:28:50,000 --> 00:28:52,000
The trunk, yeah.

240
00:28:52,000 --> 00:28:56,000
The trunk up and making noise and rushing at you,

241
00:28:56,000 --> 00:28:59,000
say to kill you.

242
00:28:59,000 --> 00:29:05,000
So, the teacher created that image as the student has asked.

243
00:29:05,000 --> 00:29:11,000
And when the image he created went to him,

244
00:29:11,000 --> 00:29:15,000
or came to him to crush him,

245
00:29:15,000 --> 00:29:20,000
he was afraid and so he got up from his seat.

246
00:29:20,000 --> 00:29:23,000
He was afraid of his own creation.

247
00:29:23,000 --> 00:29:29,000
So, the student took hold of the robe and said,

248
00:29:29,000 --> 00:29:36,000
what do you do at a hands of fear?

249
00:29:36,000 --> 00:29:41,000
Only then he realized that he was not in their heart, yeah.

250
00:29:41,000 --> 00:29:44,000
So, he said,

251
00:29:44,000 --> 00:29:46,000
he said,

252
00:29:46,000 --> 00:29:53,000
what are his hands and friend, please be my refuge.

253
00:29:53,000 --> 00:29:55,000
So, the student said,

254
00:29:55,000 --> 00:29:58,000
I came here just for these bubbles

255
00:29:58,000 --> 00:30:02,000
and then he taught him a practice of meditation

256
00:30:02,000 --> 00:30:05,000
and he practiced meditation and he became in a hand.

257
00:30:05,000 --> 00:30:08,000
So, even for 60 years,

258
00:30:08,000 --> 00:30:12,000
they can push the mental department away,

259
00:30:12,000 --> 00:30:14,000
but although for 60 years,

260
00:30:14,000 --> 00:30:17,000
no mental departments are risen in his mind.

261
00:30:17,000 --> 00:30:20,000
When there is the condition,

262
00:30:20,000 --> 00:30:23,000
the elephant coming to the depression,

263
00:30:23,000 --> 00:30:25,000
he was afraid.

264
00:30:25,000 --> 00:30:29,000
So, he got up to run away.

265
00:30:29,000 --> 00:30:30,000
So,

266
00:30:30,000 --> 00:30:30,000
so,

267
00:30:30,000 --> 00:30:33,000
Jana's cannot eradicate,

268
00:30:33,000 --> 00:30:37,000
cannot describe mental departments all together,

269
00:30:37,000 --> 00:30:39,000
but they can push them away,

270
00:30:39,000 --> 00:30:42,000
not just when they were in the Jana state,

271
00:30:42,000 --> 00:30:45,000
maybe for many years.

272
00:30:45,000 --> 00:30:48,000
But, since they are not even educated completely,

273
00:30:48,000 --> 00:30:52,000
they can come back as in this story.

274
00:30:52,000 --> 00:30:56,000
But, those,

275
00:30:56,000 --> 00:30:58,000
are educated by mother,

276
00:30:58,000 --> 00:31:01,000
never arise again.

277
00:31:01,000 --> 00:31:04,000
Maybe an extreme case,

278
00:31:04,000 --> 00:31:10,000
among able to push mental departments away for 60 years,

279
00:31:10,000 --> 00:31:14,000
although he was a puto Jana.

280
00:31:14,000 --> 00:31:20,000
Okay, I think I have explained fairly fully,

281
00:31:20,000 --> 00:31:22,000
not yet fully,

282
00:31:22,000 --> 00:31:24,000
very fully,

283
00:31:24,000 --> 00:31:29,000
the effect of right concentration.

284
00:31:29,000 --> 00:31:32,000
And I think that shows that what I told you about,

285
00:31:32,000 --> 00:31:33,000
the four noble truths,

286
00:31:33,000 --> 00:31:36,000
and over the eightfold part is just

287
00:31:36,000 --> 00:31:39,000
and the tip of an ice box.

288
00:31:39,000 --> 00:31:43,000
There is a lot more to know about these,

289
00:31:43,000 --> 00:31:48,000
those four noble truths and eight factors.

290
00:31:48,000 --> 00:31:51,000
And,

291
00:31:51,000 --> 00:31:55,000
those concluded the lecture

292
00:31:55,000 --> 00:31:57,000
and the four noble truths,

293
00:31:57,000 --> 00:31:58,000
part two,

294
00:31:58,000 --> 00:32:01,000
delivered by the Nurobo Ussilananda,

295
00:32:01,000 --> 00:32:04,000
Atatagata Meditation Center,

296
00:32:04,000 --> 00:32:06,000
September 27,

297
00:32:06,000 --> 00:32:22,000
1996.

