1
00:00:00,000 --> 00:00:04,640
Good evening.

2
00:00:04,640 --> 00:00:20,320
Tonight I thought I would talk about social distancing, buzzword these days, as people

3
00:00:20,320 --> 00:00:31,500
are concerned with the spread of sickness. So there's a call for people to engage in social

4
00:00:31,500 --> 00:00:42,200
distancing. I think it's a unique opportunity because we have so many people in the

5
00:00:42,200 --> 00:00:52,360
world engaging in a similar activity that may be quite different from how they normally

6
00:00:52,360 --> 00:01:03,560
act and they normally behave. And moreover, an activity that is very close to the sorts

7
00:01:03,560 --> 00:01:17,680
of things recommended in Buddhism. Because social distancing is a way of looking inside.

8
00:01:17,680 --> 00:01:29,320
It's a way of focusing your attention on your own being and so it goes quite well with

9
00:01:29,320 --> 00:01:38,160
mental development and introspection and developing understanding of the nature of one's

10
00:01:38,160 --> 00:01:50,520
own mind. If done well, and so of course, it's not to say that despite practicing social

11
00:01:50,520 --> 00:01:58,080
distancing, you somehow become wiser and understand yourself more. And so I thought I'd

12
00:01:58,080 --> 00:02:07,800
talk a little bit about how Buddhists engage in social distancing or how one might engage

13
00:02:07,800 --> 00:02:21,840
properly in social distancing for beneficial results, for it to be a beneficial activity.

14
00:02:21,840 --> 00:02:28,880
So it's not to say that one should also always be socially distant, but it has to be acknowledged

15
00:02:28,880 --> 00:02:37,920
and Buddhism does try to help people to acknowledge that if we're constantly caught up

16
00:02:37,920 --> 00:02:44,920
in society, we'll have very little opportunity to understand ourselves, very little

17
00:02:44,920 --> 00:02:53,080
progress in understanding the nature of our minds in terms of what is causing us

18
00:02:53,080 --> 00:03:02,800
stress and suffering and how we might change that. So as with all things, social distancing

19
00:03:02,800 --> 00:03:09,680
is in the physical realm. We're not talking about becoming mentally distant. There's

20
00:03:09,680 --> 00:03:16,120
no call in the world right now for people to be emotionally or mentally distant from each

21
00:03:16,120 --> 00:03:22,160
other. And we're talking about something physical. And so on the face, it actually doesn't

22
00:03:22,160 --> 00:03:34,400
have anything to do with Buddhism. But as with all things, it all things physical, the

23
00:03:34,400 --> 00:03:39,440
quality of it depends very much on the state of the mind as one performs the act.

24
00:03:39,440 --> 00:03:45,360
And moreover, the performing of certain physical acts can facilitate the evoking of

25
00:03:45,360 --> 00:03:49,520
certain mind states. So when we do walking meditation and sitting meditation, you might

26
00:03:49,520 --> 00:03:55,480
say, well, it's just physical. And what use is that, but being engaging in such a simple

27
00:03:55,480 --> 00:04:05,520
activity, we can give the mind an opportunity to flower, to blossom, to do its work, because

28
00:04:05,520 --> 00:04:20,720
it's not preoccupied with complicated movements. And so one good way to look at any physical

29
00:04:20,720 --> 00:04:27,440
activity is in terms of it being beneficial or harmful in the different ways in which you

30
00:04:27,440 --> 00:04:35,760
could undertake it, that might be harmful, is what we call the four agati. Agati is loosely

31
00:04:35,760 --> 00:04:43,960
translated as partialities or biases, biases better. But literally, it means what you're

32
00:04:43,960 --> 00:04:53,120
led by, I think, what leads you. And so we do certain things where led by this or

33
00:04:53,120 --> 00:05:08,560
were led by that. And there are four reasons why one might do something, what leads one

34
00:05:08,560 --> 00:05:21,760
or what one has gone over to. As a reason for doing something physical, the same goes

35
00:05:21,760 --> 00:05:32,320
of course with speech, anything in the physical realm. And these four are chanda. We do

36
00:05:32,320 --> 00:05:45,800
something based on desire or liking it or liking in general. Dosa, we do something based

37
00:05:45,800 --> 00:06:00,960
on anger. Baya, we do something based on fear. And Moha, we do something based on delusion.

38
00:06:00,960 --> 00:06:07,920
Is it the four agati? They're all bad reasons to do something. So we'll talk about them

39
00:06:07,920 --> 00:06:13,040
and then we'll talk about some of the right reasons to do things or how to make things,

40
00:06:13,040 --> 00:06:20,200
especially things like social distancing really beneficial. And to avoid making them problematic

41
00:06:20,200 --> 00:06:26,800
because something so extreme for many people as social distancing can have debilitating

42
00:06:26,800 --> 00:06:37,240
effects, can have negative effects. So chanda, chanda, chanda, chanda, is partially or the

43
00:06:37,240 --> 00:06:50,160
bias of partiality or of liking. And this encompasses all kinds of desire, all kinds of greed.

44
00:06:50,160 --> 00:06:59,760
So people might social distance because of stinginess, miserliness. Misers become very distant

45
00:06:59,760 --> 00:07:08,160
because of people asking them for donations and gifts and so on, loans often. I met a

46
00:07:08,160 --> 00:07:14,720
woman in Thailand when I was living there. I met her a few times once we went to, I was

47
00:07:14,720 --> 00:07:21,680
with Adjantong quite often, my teacher. And she would, every time she came, she was not.

48
00:07:21,680 --> 00:07:28,160
She was very rich. And she was quite exceptional. I've never met anyone like her. Everywhere

49
00:07:28,160 --> 00:07:35,880
she went when she met a monk especially because she had great confidence and great respect

50
00:07:35,880 --> 00:07:46,320
for the monastic life. She would always give something. I remember going up to sit

51
00:07:46,320 --> 00:07:51,160
in on the reporting of the meditators, the meetings between the teacher and the meditator

52
00:07:51,160 --> 00:07:57,280
and she was there. And she saw me come up and she had just been giving out gifts to the

53
00:07:57,280 --> 00:08:02,960
monks. And she saw me there and immediately she thought and she looked in and she had

54
00:08:02,960 --> 00:08:07,200
a pen in her purse. She looked in her purse and she said, here I would like to give you

55
00:08:07,200 --> 00:08:15,680
a gift of a pen. But for most people that's not like that, we went to her house once

56
00:08:15,680 --> 00:08:22,080
and she was talking about all the many requests for donations that she would get. She

57
00:08:22,080 --> 00:08:27,680
would get letters and letters every day from teachers and firefighters and police and

58
00:08:27,680 --> 00:08:37,320
so on. Everybody asking for a donation. And so for most people this becomes too much rich

59
00:08:37,320 --> 00:08:44,080
people. It becomes too much for ordinary people as well. We become quite distant if we

60
00:08:44,080 --> 00:08:53,000
think people are going to or if people are constantly asking for loans or gifts or so on.

61
00:08:53,000 --> 00:09:00,080
And this is problematic. Does some extent you have to protect yourself if people are greedy

62
00:09:00,080 --> 00:09:07,520
and they're clingy or so on? But it can't be because of your own greed. It has to be

63
00:09:07,520 --> 00:09:13,640
of course because of something more beneficial. For some people they get very greedy

64
00:09:13,640 --> 00:09:20,440
and therefore keep to themselves because they don't want to share. For things like social

65
00:09:20,440 --> 00:09:27,400
distancing for most people that's not going to be like that. But the results of having

66
00:09:27,400 --> 00:09:36,040
to seclude yourself for many people are quite devastating. Lots of livelihood for students,

67
00:09:36,040 --> 00:09:42,920
loss of education. For some people that can lead to great stress having to be in close

68
00:09:42,920 --> 00:09:48,280
quarters with others. So you call it social distancing. But in fact it puts you close to

69
00:09:48,280 --> 00:09:54,080
people who you might have a hard time being close to. Your family members sometimes you might

70
00:09:54,080 --> 00:10:03,040
not get along siblings, even children and with their parents and so on. But in some cases

71
00:10:03,040 --> 00:10:16,120
it can, I think perhaps be an excuse for increased engagement and sensuality. So for many

72
00:10:16,120 --> 00:10:21,920
people, students, a chance to stay at home and not have to go to school might seem like

73
00:10:21,920 --> 00:10:28,840
a chance to just play games all the time or watch television all the time. Some people

74
00:10:28,840 --> 00:10:35,200
that might just be a means of relaxing of being lazy. For some people driven by stress,

75
00:10:35,200 --> 00:10:45,520
the stress of all the other reasons, all the other problems associated with the pandemic

76
00:10:45,520 --> 00:10:53,440
and sickness and so on. It can drive them to increased entertainment and things like binge

77
00:10:53,440 --> 00:11:00,440
eating and so on. And as a result there's a lot of problems that come from this. As a result

78
00:11:00,440 --> 00:11:09,880
there will be increased irritability for someone who is constantly engaged in entertainment

79
00:11:09,880 --> 00:11:18,480
and enjoyment of sensuality. They have the increased capacity of becoming angry, irritated

80
00:11:18,480 --> 00:11:26,120
and annoyed. It leads to fighting among siblings and families and loved ones. So a big

81
00:11:26,120 --> 00:11:36,440
reason perhaps why people engaging in this sort of social distancing or having stress, having

82
00:11:36,440 --> 00:11:44,040
to stay at home as a family is because of their incapacity to be content, to be at peace.

83
00:11:44,040 --> 00:11:50,240
And so constantly fighting over what television program we're going to watch or fighting

84
00:11:50,240 --> 00:11:57,200
over belongings and money, fighting over all sorts of things that should bring, are supposed

85
00:11:57,200 --> 00:12:08,400
to bring pleasure and instead bring great suffering. So when engaging in this sort of activity

86
00:12:08,400 --> 00:12:20,040
you have to be mindful of sorts of things like sensuality and craving and so on. Because

87
00:12:20,040 --> 00:12:27,160
it might be seen as a license or an opportunity to increase in our activity or engagement

88
00:12:27,160 --> 00:12:35,160
in entertainment and so on. Which is problematic.

89
00:12:35,160 --> 00:12:44,680
The second reason based on anger. So social distancing based on anger might easily be carried

90
00:12:44,680 --> 00:12:52,880
out by someone who is racist. You'll see people who don't want to associate it with people

91
00:12:52,880 --> 00:12:59,400
of other races or skin colors or even nationalities and languages and cultures, religions.

92
00:12:59,400 --> 00:13:07,080
From the gins of very common one based on anger or hatred, ethnicity, hatred of other

93
00:13:07,080 --> 00:13:17,880
ethnicities. And you see this even with the current pandemic you hear about people avoiding

94
00:13:17,880 --> 00:13:26,960
others and becoming very angry at people based on their looks or ethnicity. Chinese people

95
00:13:26,960 --> 00:13:34,280
because there's an understanding that their disease originated in China. So there's an

96
00:13:34,280 --> 00:13:46,680
increase in racism as I understand relating to Chinese people. But there are many opportunities

97
00:13:46,680 --> 00:13:52,400
for the arising of anger. We get angry at people who perhaps seem to have done something

98
00:13:52,400 --> 00:14:02,920
negligent. You get angry at people who don't practice social distancing. Or you just

99
00:14:02,920 --> 00:14:11,080
social distancing. You distance yourself from people with a cruel angry attitude or attitude

100
00:14:11,080 --> 00:14:16,120
when we distance our attitude when we do anything is so important. You might feel self

101
00:14:16,120 --> 00:14:22,280
righteous when you do something, thinking it's the right thing to do. But your way of

102
00:14:22,280 --> 00:14:28,360
and it very well might be, but it becomes quite unwholesome if it's done with the wrong attitude.

103
00:14:28,360 --> 00:14:32,880
And so someone tries to shake our hands and we shy away from them angerly and say,

104
00:14:32,880 --> 00:14:42,440
what are you doing? It's on a turn of cold shoulder. We act cold towards others. All of

105
00:14:42,440 --> 00:14:49,080
it boils down to a lack of mindfulness. If a person has mindfulness, they're able to interact

106
00:14:49,080 --> 00:14:55,280
with others in a way that is harmonious in a way that is free from any kind of cruelty,

107
00:14:55,280 --> 00:15:05,160
any kind of harshness. And so it's a very good example of how we really have a responsibility

108
00:15:05,160 --> 00:15:13,760
to ourselves and to others for the purpose of bringing and maintaining peace and happiness

109
00:15:13,760 --> 00:15:22,160
to act in a way that is free from qualities like anger. And absolutely, of course, we

110
00:15:22,160 --> 00:15:35,920
can't be mean to refugees is apparently a thing we have to be conscientious in there

111
00:15:35,920 --> 00:15:47,400
are many ways we can make it quite easy by closing our borders to refugees and so on that

112
00:15:47,400 --> 00:15:55,960
end up being more harmful or, well, whether you can balance what is more harmful,

113
00:15:55,960 --> 00:16:01,960
what is left harmful, but end up being quite cruel and needlessly cruel because of course

114
00:16:01,960 --> 00:16:13,960
the alternative is more difficult. If you let refugees in during this time, it can be challenging

115
00:16:13,960 --> 00:16:18,680
to deal with them and rather than deal with the challenges and example of how we just

116
00:16:18,680 --> 00:16:27,400
act cruelly. So with something like social distancing, there may be times where compassion

117
00:16:27,400 --> 00:16:32,920
overrides, perhaps someone has hurt themselves and you don't know who this person is,

118
00:16:32,920 --> 00:16:38,160
you never met them, you see them on the street and they've hurt falling down or something

119
00:16:38,160 --> 00:16:44,280
you go and you're even allowed to pick people up, you don't know, but anyway, cases where

120
00:16:44,280 --> 00:16:52,560
you might have to break your quarantine, for example, and then just wash your hands, instead

121
00:16:52,560 --> 00:16:58,360
of being, oh, I can't help this person because that would endanger me to, you know, with

122
00:16:58,360 --> 00:17:05,680
this sickness. In fact, in many cases, you might think in a life or death situation, you

123
00:17:05,680 --> 00:17:11,560
might of course throw out any kind of caution, any kind of sense of something like social

124
00:17:11,560 --> 00:17:19,360
distancing and you have to be thoughtful of what is a better thing to do. And you see

125
00:17:19,360 --> 00:17:26,720
this, I think with people, countries closing their borders to refugees. Of course, I'm

126
00:17:26,720 --> 00:17:31,120
not a politician, I don't know the details, but it's something that has to be done quite

127
00:17:31,120 --> 00:17:39,040
thoughtfully, conscientiously, in terms of the negative impact, potential negative impact

128
00:17:39,040 --> 00:17:50,000
and cruelty involved. The third reason why we do things is through fear. And I think this

129
00:17:50,000 --> 00:18:00,320
is probably the most common emotion among the negative reasons or qualities of mind that

130
00:18:00,320 --> 00:18:08,160
we engage in when we distance ourselves in these times. A lot of people afraid doing it

131
00:18:08,160 --> 00:18:21,640
out of fear. And fear is, fear is a great motivator. We are quick to use fear as a means

132
00:18:21,640 --> 00:18:30,880
of motivating ourselves. We allow it, many times, and we allow it to persist because it

133
00:18:30,880 --> 00:18:38,080
appears to motivate us. And it does. I mean, fear is stressful, fear is an exciter. And

134
00:18:38,080 --> 00:18:44,840
as a result, it gets the mind working. But of course, that's just a sign that before

135
00:18:44,840 --> 00:18:51,400
the fear, our mind wasn't working properly. We weren't thoughtful. We become thoughtful

136
00:18:51,400 --> 00:19:00,560
about something when it scares us. How can we fix it? How can we deal with it? But as

137
00:19:00,560 --> 00:19:08,960
you know, fear, it doesn't encourage us to think rationally. It doesn't always or necessarily

138
00:19:08,960 --> 00:19:19,400
or really have the quality of encouraging or promoting or augmenting our wisdom or mindfulness,

139
00:19:19,400 --> 00:19:26,000
or sense of presence when we deal with the problem. And it becomes quite reactionary.

140
00:19:26,000 --> 00:19:39,720
And so we have to sometimes question our level of activity, our level of action in relation

141
00:19:39,720 --> 00:19:50,800
to things like social distancing. A good example, it works for all of these. For many people,

142
00:19:50,800 --> 00:20:02,120
I think social distancing is not that troublesome or burdensome. For many people, it is.

143
00:20:02,120 --> 00:20:10,160
And in some ways, it can seem quite pleasant. So people engaging it or agreeing to engage

144
00:20:10,160 --> 00:20:15,200
in it. You see the world. It's quite quick to engage in the social distancing, because

145
00:20:15,200 --> 00:20:26,480
in some ways it's subconsciously is quite pleasant. Or out of anger, we do things rationally.

146
00:20:26,480 --> 00:20:34,880
We engage in social distancing, we need your reaction out of anger, the aversion towards

147
00:20:34,880 --> 00:20:42,480
sickness. I don't want to get sick. So it can be irrational. We haven't thought about it.

148
00:20:42,480 --> 00:20:47,520
We haven't thought about what is the real benefit. And so our actions are often irrational.

149
00:20:47,520 --> 00:20:53,360
And fear is the same. We do things a lot of the fear going on has caused people to do things

150
00:20:53,360 --> 00:20:59,360
that are in many ways unhelpful. And sometimes it's unhelpful without realizing you do

151
00:20:59,360 --> 00:21:05,080
things thinking they're going to be helpful like wear masks or so on. And apparently, well,

152
00:21:05,080 --> 00:21:10,800
I don't know. It's an example that there was some talk about how the masks aren't helping,

153
00:21:10,800 --> 00:21:17,280
but I don't know if they are. But in cases like this, people buying great amounts of toilet

154
00:21:17,280 --> 00:21:25,920
paper, for example, was a big thing. Just mass hysteria or mass panic. And sometimes panic

155
00:21:25,920 --> 00:21:32,320
can help you. You did something and it physically, maybe you didn't get all the toilet paper

156
00:21:32,320 --> 00:21:35,840
in the store, but it can be quite unwholesome and unpleasant, because of course,

157
00:21:35,840 --> 00:21:47,040
these other people who need an ordinary amount of toilet paper can't get any. And it causes

158
00:21:47,040 --> 00:21:56,160
us to act irrationally and without, it causes us to lose our regard for propriety, rightness.

159
00:21:56,160 --> 00:22:11,280
Fear is a stimulator. And so it can often appear to be helpful. But we can see from the

160
00:22:11,280 --> 00:22:22,080
results of hysteria of fear that it in fact takes us away from a sense of calm, clear,

161
00:22:22,080 --> 00:22:34,720
measured understanding of the situation. The fourth reason why we do things, why we might do something,

162
00:22:35,440 --> 00:22:43,520
is out of delusion. And so something like social distancing, as with anything, we have to consider,

163
00:22:43,520 --> 00:22:53,520
we have to ask ourselves, is it proper to be doing and so on. People who engage out of delusion

164
00:22:53,520 --> 00:22:59,200
might be just be doing it, because other people have told them to do it. People engage in

165
00:22:59,200 --> 00:23:04,640
seclusion out of arrogance and conceit, thinking you're better than someone causes you to

166
00:23:04,640 --> 00:23:14,560
distance yourself. But with something like social distancing in this case, the pandemic,

167
00:23:16,960 --> 00:23:22,720
a really good example is of how we engage in social distancing, because everyone else is

168
00:23:22,720 --> 00:23:29,520
doing it, because we have an idea that it's going to keep us safe from sickness. But we do it

169
00:23:29,520 --> 00:23:38,000
haphazardly. We do it with a disconnect from reality. We often do this, the herd mentality. Everybody's

170
00:23:38,000 --> 00:23:43,520
doing it. Okay, this will keep me safe, the thing about wearing the masks and so on. And then we engage

171
00:23:43,520 --> 00:23:53,120
in other activities, which of course are what happened to be nullifying any benefit that we might get

172
00:23:53,120 --> 00:23:58,800
from social distancing, like we still, we don't shake people's hands, but we touch door knobs in

173
00:23:58,800 --> 00:24:09,040
public places, and then we pick our nose or we scratch our face or so. And so it's an example,

174
00:24:09,760 --> 00:24:16,800
or it's an example that shows the difference between being mindful and being unmindful.

175
00:24:17,840 --> 00:24:25,520
The unmindful state is one that causes us to engage in activities that are potentially harmful to

176
00:24:25,520 --> 00:24:31,520
ourselves, simply because of the darkness, because of the lack of clarity, the lack of awareness.

177
00:24:33,920 --> 00:24:39,680
With the mindful of delusion, we do many things that are contrary to our intention,

178
00:24:40,480 --> 00:24:52,800
so engage in social distancing, but again, we take produce from the grocery store and without

179
00:24:52,800 --> 00:25:01,920
washing it, we eat it to put it in our mouths and so on. And just doing things unmindfully,

180
00:25:01,920 --> 00:25:13,280
like picking your nose after you've, whatever, sucking on your thumb, something that causes us to

181
00:25:13,280 --> 00:25:21,520
negate any benefit in the worldly sense of something like social distancing.

182
00:25:23,600 --> 00:25:33,440
So ultimately the right way or the right way to engage in something like this social distancing

183
00:25:34,160 --> 00:25:38,000
is of course with mindfulness, with wisdom.

184
00:25:38,000 --> 00:25:46,160
And so it's important that we learn about why we're doing everything that we do. When we do

185
00:25:46,160 --> 00:25:51,920
something like social distancing, we learn about it, we study it. It's the kind of wisdom that

186
00:25:51,920 --> 00:26:03,120
it shows us a sort of mindfulness, our deliberate engagement in understanding what it is that we're

187
00:26:03,120 --> 00:26:08,160
doing, so that we do it properly. The internet is a wonderful thing now for information,

188
00:26:08,880 --> 00:26:14,320
our capacity to learn about the proper ways to do things, the benefits of certain things,

189
00:26:14,320 --> 00:26:20,800
and how to make things beneficial in a worldly sense. We have to think about

190
00:26:22,960 --> 00:26:28,560
how we're going to engage in this sort of social distancing, what we have to plan out,

191
00:26:28,560 --> 00:26:34,880
how we engage in the world and so on. But most importantly we have to be mindful

192
00:26:36,880 --> 00:26:41,520
anything that could be quite radical like this where we have to remove ourselves from our

193
00:26:41,520 --> 00:26:49,920
ordinary activity, where we have to leave our social circle and we have to remember not to

194
00:26:49,920 --> 00:26:57,440
shake people's hands, not to kiss people's cheeks, not to touch the door handle, for example,

195
00:26:57,440 --> 00:27:01,760
or if we've touched the door handle to wash our hands before we touch our face and so on.

196
00:27:04,240 --> 00:27:09,680
We have to remember all of that, which means we have to be conscious, we have to be present.

197
00:27:10,720 --> 00:27:15,280
It's very easy to have as a theory, I'm going to do this, I'm going to do that, I'm not going to do

198
00:27:15,280 --> 00:27:19,360
this, I'm not going to do that, I'm going to time comes, it all gets thrown on the window because

199
00:27:19,360 --> 00:27:34,800
you're just not mindful. And so above most things, most especially when you engage in something

200
00:27:34,800 --> 00:27:41,840
like social distancing and Buddhist we have a great experience, wealth of experience relating to

201
00:27:41,840 --> 00:27:49,280
things like social distancing, you absolutely need a proper state of mind, you need to be

202
00:27:49,280 --> 00:27:56,080
present. It's an example of an activity where mindfulness is crucial because of course when you're

203
00:27:56,080 --> 00:28:01,440
alone you'll you'll drive yourself crazy if you're not mindful, if you're not able to be present

204
00:28:01,440 --> 00:28:10,640
or you'll be driven to distraction, you'll just engage in sensual indulgence and cultivate addiction

205
00:28:10,640 --> 00:28:21,280
and so on, or you'll fight with your family, you'll engage in anger, you'll build up fear in

206
00:28:21,280 --> 00:28:37,360
hysteria and panic, you'll develop, you'll cultivate delusion, your ego and conceit and so on,

207
00:28:37,360 --> 00:28:49,920
based on based on your based on your seclusion because there's an inward focus of course when

208
00:28:49,920 --> 00:28:58,960
you distance yourself from others. And so if your mind is bent in the wrong way, the inward focus

209
00:28:58,960 --> 00:29:05,680
is going to magnify that and the effects are going to be magnified because you're focused

210
00:29:05,680 --> 00:29:14,240
inward, any negative, any of these four, the greed, the anger, the fear, the delusion, any of these

211
00:29:14,240 --> 00:29:24,400
when focused inward are going to make you sick and so where if our true goal in this social distancing

212
00:29:24,400 --> 00:29:33,760
is to be free from sickness, well absolutely all that always mental illness is going to trump

213
00:29:33,760 --> 00:29:40,480
any kind of physical illness. And so our concern of over physical illness is important,

214
00:29:41,440 --> 00:29:49,520
yes social distancing is useful simply for the fact that it prevents and reduces

215
00:29:51,200 --> 00:29:57,600
the likelihood of us getting sick or the speed at which the pandemic spreads and so on.

216
00:29:57,600 --> 00:30:10,160
But if it's engaged in such a way that it increases our mental illness then we haven't accomplished

217
00:30:10,160 --> 00:30:21,760
anything at all. So food for thought and a good reason for us to engage in meditation at this time

218
00:30:21,760 --> 00:30:35,200
absolutely now that in this time of social distancing we have a unique opportunity for

219
00:30:40,240 --> 00:30:46,560
so much meditation practice that we should take the opportunity,

220
00:30:46,560 --> 00:30:58,320
we should take it as a real special opportunity for us to try this practice of developing and

221
00:30:59,040 --> 00:31:07,760
working on our minds. Of course our center is shut down so we aren't accepting new meditators

222
00:31:07,760 --> 00:31:14,800
and I assume most meditation centers are as well but there are many ways to practice meditation,

223
00:31:14,800 --> 00:31:20,960
many resources for you to practice meditation on your own you can read books on how to meditate,

224
00:31:20,960 --> 00:31:28,800
we have a booklet on how to meditate. And we also have a stay at home course that we all have had

225
00:31:28,800 --> 00:31:35,440
for a long time where people who practice at home they do at least an hour of meditation a day and

226
00:31:35,440 --> 00:31:45,040
we meet, we talk on a voice call once a week and give a new exercise because of course the

227
00:31:45,040 --> 00:31:50,720
booklet that we have only gives the basic exercise so every week we give you a new exercise adding

228
00:31:50,720 --> 00:31:58,560
to the challenge and pushing you further to train your mind to see clearly, to be present

229
00:31:58,560 --> 00:32:06,560
and to understand yourself. So I encourage absolutely everyone during this time this is a prime

230
00:32:07,440 --> 00:32:12,720
opportunity to practice meditation if you haven't or to increase your practice if you're

231
00:32:12,720 --> 00:32:20,480
already practicing. And absolutely if you're interested in taking a meditation course at home

232
00:32:20,480 --> 00:32:27,200
you can look on our website, SiriMungalow.org under the courses and you can read about it.

233
00:32:28,960 --> 00:32:34,240
So those are my thoughts on social distancing. I just thought I would do a video on it.

234
00:32:35,600 --> 00:32:43,600
I wish you all the best in these trying times and I hope everyone is able to

235
00:32:43,600 --> 00:32:52,960
stay calm and peaceful inside and that internally and externally we all find peace, happiness

236
00:32:52,960 --> 00:33:17,200
and freedom from suffering. Thank you, I wish you all the best.

