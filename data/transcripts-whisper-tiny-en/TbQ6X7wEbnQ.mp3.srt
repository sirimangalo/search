1
00:00:00,000 --> 00:00:06,000
The second and the third I can probably deal with together.

2
00:00:06,000 --> 00:00:13,000
The second corollary to the existence of the mind is the non-existence of the self.

3
00:00:13,000 --> 00:00:15,000
The third is the non-existence of God.

4
00:00:15,000 --> 00:00:19,000
Now, of course, these are not directly inferred from the existence of the mind.

5
00:00:19,000 --> 00:00:24,000
You could postulate the existence of the mind and postulate the existence of the soul.

6
00:00:24,000 --> 00:00:32,000
You could have all sorts of, they're not mutually exclusive.

7
00:00:32,000 --> 00:00:37,000
What we're talking about here is placing the mind in a scientific context

8
00:00:37,000 --> 00:00:40,000
or in the context of reality.

9
00:00:40,000 --> 00:00:47,000
In reality, the mind is very much interconnected, interdependent with the body.

10
00:00:47,000 --> 00:00:48,000
They affect one another.

11
00:00:48,000 --> 00:00:49,000
They work together.

12
00:00:49,000 --> 00:00:52,000
They're very much a part of the same reality.

13
00:00:52,000 --> 00:00:57,000
Two sides to the same coin, you could say.

14
00:00:57,000 --> 00:01:01,000
Once we understand that to be reality,

15
00:01:01,000 --> 00:01:07,000
there's no room to postulate things like a self or things like a God or so on.

16
00:01:07,000 --> 00:01:13,000
In fact, many of the things which we would care to postulate,

17
00:01:13,000 --> 00:01:18,000
many of the things we would care to believe in are totally thrown over the window.

18
00:01:18,000 --> 00:01:23,000
Understanding why there could not possibly be a self, could not possibly be a God.

19
00:01:23,000 --> 00:01:26,000
We have to come back and understand what is reality.

20
00:01:26,000 --> 00:01:32,000
Now, if we talk in terms of the physical, this is something that I think scientists don't,

21
00:01:32,000 --> 00:01:35,000
don't often fully understand.

22
00:01:35,000 --> 00:01:37,000
I'm not sure actually.

23
00:01:37,000 --> 00:01:40,000
I'm going to assume that it's often not understood, not explained it.

24
00:01:40,000 --> 00:01:42,000
If it's already understood, then no problem.

25
00:01:42,000 --> 00:01:46,000
I think one of the things that is not fully understood is the idea of reductionism.

26
00:01:46,000 --> 00:01:51,000
Now, I do think that the most scientists would consider themselves to be reductionists.

27
00:01:51,000 --> 00:01:55,000
But an understanding of what it means to be reductionists is opposed to Hollis.

28
00:01:55,000 --> 00:01:59,000
The Hollis is someone who believes that the hole is more than the sum of its parts.

29
00:01:59,000 --> 00:02:04,000
Reductionism says what is there is all that's real.

30
00:02:04,000 --> 00:02:07,000
Hollism says that once you put something together,

31
00:02:07,000 --> 00:02:09,000
poof something else arises.

32
00:02:09,000 --> 00:02:15,000
And I think that scientists don't understand this because this is what the position they're coming to in terms of the mind.

33
00:02:15,000 --> 00:02:18,000
Mostly, as materialists, they're coming to say that,

34
00:02:18,000 --> 00:02:21,000
put the body together, poof the mind arises.

35
00:02:21,000 --> 00:02:24,000
And a reductionism says that what is there is what is real.

36
00:02:24,000 --> 00:02:30,000
If it's not there already, there's nothing that's going to bring it into existence.

37
00:02:30,000 --> 00:02:34,000
This is why they're looking for this idea of what are the building blocks of reality

38
00:02:34,000 --> 00:02:36,000
because that's what's there.

39
00:02:36,000 --> 00:02:38,000
And it's not going to change.

40
00:02:38,000 --> 00:02:42,000
You're not going to put them together and poof suddenly something else appears out of nothing.

41
00:02:42,000 --> 00:02:47,000
Things don't arise or nothing, of course.

42
00:02:47,000 --> 00:02:58,000
So what this means, suppose we take this table I'm sitting on or this robe I'm wearing or any of the things in this room.

43
00:02:58,000 --> 00:03:02,000
Suppose we take a chair and this is usually the most obvious example.

44
00:03:02,000 --> 00:03:04,000
A chair, we say it exists.

45
00:03:04,000 --> 00:03:06,000
And scientists, I think in general, will say it exists.

46
00:03:06,000 --> 00:03:09,000
But this is where we lose the scientific nature.

47
00:03:09,000 --> 00:03:14,000
The chair doesn't exist. In reality, the chair is made up of things which are real.

48
00:03:14,000 --> 00:03:17,000
And we don't know quite what those are in science yet.

49
00:03:17,000 --> 00:03:21,000
In terms of Buddhism, it's actually quite simple.

50
00:03:21,000 --> 00:03:23,000
It's made up of the four elements.

51
00:03:23,000 --> 00:03:25,000
And people thought these were Greek.

52
00:03:25,000 --> 00:03:27,000
Well, actually it came from earlier from India.

53
00:03:27,000 --> 00:03:31,000
In the times of India, they were already talking about the four elements.

54
00:03:31,000 --> 00:03:33,000
Earth, air, water, fire.

55
00:03:33,000 --> 00:03:37,000
And these are just the four properties of experienced reality.

56
00:03:37,000 --> 00:03:42,000
But, okay, so putting that aside, there is some physical reality.

57
00:03:42,000 --> 00:03:44,000
Now, the mind is exactly the same.

58
00:03:44,000 --> 00:03:46,000
The mind is made up of building blocks.

59
00:03:46,000 --> 00:03:48,000
Mostly we don't quite know what those are.

60
00:03:48,000 --> 00:03:50,000
I could give you my understanding of what they are.

61
00:03:50,000 --> 00:03:51,000
It's not really important.

62
00:03:51,000 --> 00:03:53,000
We understand that they're made up of building blocks.

63
00:03:53,000 --> 00:03:55,000
Some examples are emotions.

64
00:03:55,000 --> 00:03:56,000
We have anger.

65
00:03:56,000 --> 00:03:58,000
Anger is very much a building block of reality.

66
00:03:58,000 --> 00:04:02,000
Greed, greed is very much a building block of the mental reality.

67
00:04:02,000 --> 00:04:07,000
One thing, things, exactly.

68
00:04:07,000 --> 00:04:11,000
Happiness, unhappiness, liking, disliking, and so on.

69
00:04:11,000 --> 00:04:14,000
You can see jealousy.

70
00:04:14,000 --> 00:04:17,000
These are some of the building blocks of the mind.

71
00:04:17,000 --> 00:04:22,000
Thinking, worrying, or so on.

72
00:04:22,000 --> 00:04:29,000
Many of the factors of the mind, you know, experiencing the reality of seeing or hearing or judging something

73
00:04:29,000 --> 00:04:34,000
or being able to measure something, association, being able to remember things.

74
00:04:34,000 --> 00:04:39,000
All of these different functions in the mind, these are the building blocks of the mind.

75
00:04:39,000 --> 00:04:43,000
Nowhere in here, is it possible to say that when you put these things together,

76
00:04:43,000 --> 00:04:44,000
there arises a self.

77
00:04:44,000 --> 00:04:46,000
And so this is what reductionism means.

78
00:04:46,000 --> 00:04:50,000
It says that once you have these building blocks, there's no room for the chair.

79
00:04:50,000 --> 00:04:51,000
There's no room for the self.

80
00:04:51,000 --> 00:04:52,000
And there's no room for God.

81
00:04:52,000 --> 00:04:54,000
Now, God is a special example.

82
00:04:54,000 --> 00:04:56,000
Why is there no room for God?

83
00:04:56,000 --> 00:04:59,000
We've got all these things here. Why isn't God something else?

84
00:04:59,000 --> 00:05:02,000
And this is something that often comes up in meditation.

85
00:05:02,000 --> 00:05:06,000
People who believe in God, they come to meditate, and they say they still want to believe in God.

86
00:05:06,000 --> 00:05:12,000
They say, well, belief, you have to understand that where belief arises, belief is just a building block.

87
00:05:12,000 --> 00:05:19,000
It's a part of the mind that arises when your mind works in a certain way,

88
00:05:19,000 --> 00:05:24,000
or maybe it's even built up with a physical, it surely is.

89
00:05:24,000 --> 00:05:30,000
It's something that arises based on functioning of the brain, the functioning of the body,

90
00:05:30,000 --> 00:05:33,000
the functioning of the mind, and how they function together.

91
00:05:33,000 --> 00:05:35,000
They give rise to belief.

92
00:05:35,000 --> 00:05:39,000
Even if you were to see God, and many people claim to have seen God, what is that?

93
00:05:39,000 --> 00:05:44,000
It's an experience of seeing, which is built up with the physical and the mental and the way the brain works,

94
00:05:44,000 --> 00:05:46,000
the way the mind works.

95
00:05:46,000 --> 00:05:49,000
Suppose you're sitting in prayer and suddenly you feel raptured.

96
00:05:49,000 --> 00:05:57,000
Well, again, that's simply rapture, and Sam Harris of course is an atheist who is very good at explaining this.

97
00:05:57,000 --> 00:06:01,000
He said, you sit there and you say this is Jesus.

98
00:06:01,000 --> 00:06:07,000
Well, the Hindus are saying it's Krishna, and the Buddhists are saying, well, it's the way the mind works,

99
00:06:07,000 --> 00:06:09,000
and the scientists should be saying the same thing as well.

100
00:06:09,000 --> 00:06:12,000
The scientists will be saying it's where the brain works.

101
00:06:12,000 --> 00:06:19,000
So, I think the Buddhism and science in general are on the same wavelength.

102
00:06:19,000 --> 00:06:21,000
It's just, you know, coming from different sides of the coin.

103
00:06:21,000 --> 00:06:29,000
But it's understand the existence of the mind, and I think it's a shame that in general modern science doesn't.

104
00:06:29,000 --> 00:06:40,000
But anyway, once you understand the existence of the mind, you completely blow the idea of the existence of God out of the water.

105
00:06:40,000 --> 00:06:45,000
And even if you were to experience something that you could call God, well, I mean, look at that possibly be.

106
00:06:45,000 --> 00:06:48,000
When I experience another person, I still don't believe that's a person.

107
00:06:48,000 --> 00:06:49,000
It's still not a person.

108
00:06:49,000 --> 00:06:53,000
The reality of it is there's just body and there's mind working together.

109
00:06:53,000 --> 00:06:59,000
If I were to say God, what more could it be than the experience and the object of the experience?

110
00:06:59,000 --> 00:07:05,000
So, these are just some brief ideas, and this is, again, very superficial.

111
00:07:05,000 --> 00:07:10,000
It's something that has to be experienced through meditation, and it's not some kind of hokey-pokey meditation.

112
00:07:10,000 --> 00:07:14,000
It's simply watching and learning and understanding the way the mind works.

113
00:07:14,000 --> 00:07:20,000
It's a very scientific process, and you can't use physical tools to do it, because it's not a physical thing.

114
00:07:20,000 --> 00:07:26,000
When you use mental tools to look at the mind, you just look at your own mind, and there are ways of doing this that are very scientific.

115
00:07:26,000 --> 00:07:32,000
You'll see for yourself, what are the building blocks of the mind and God and the self are not in there.

116
00:07:32,000 --> 00:07:39,000
Again, this is something that takes a lot more explanation and a lot more realization to fully understand.

117
00:07:39,000 --> 00:07:41,000
But anyway, thanks for tuning in.

118
00:07:41,000 --> 00:08:05,000
This is just some more thoughts on the nature of reality.

