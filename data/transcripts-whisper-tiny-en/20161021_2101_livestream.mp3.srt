1
00:00:00,000 --> 00:00:05,440
I mean by being patient, in many ways, to appear patient,

2
00:00:05,440 --> 00:00:11,120
but how do you describe, or how do you determine

3
00:00:11,120 --> 00:00:13,280
whether someone is actually being patient,

4
00:00:13,280 --> 00:00:14,880
and what does it mean?

5
00:00:14,880 --> 00:00:16,280
What is the essence of patients?

6
00:00:19,840 --> 00:00:26,320
Because it's possible to repress your inpatients,

7
00:00:26,320 --> 00:00:29,240
if you're waiting for something,

8
00:00:29,240 --> 00:00:35,200
waiting for your alarm, your meditation timer to ring.

9
00:00:35,200 --> 00:00:39,840
You can suppress that, and force yourself to sit,

10
00:00:39,840 --> 00:00:43,120
but we tend to agree with that's not very patient.

11
00:00:43,120 --> 00:00:46,880
That's a person who's inpatient, but forcing them

12
00:00:46,880 --> 00:00:53,800
to themselves, to bear with their impatience.

13
00:00:53,800 --> 00:00:56,160
Like a kid who's restless in sitting and saying,

14
00:00:56,160 --> 00:00:59,080
are we there yet, are we there yet?

15
00:00:59,080 --> 00:01:02,680
You wouldn't call them patient.

16
00:01:02,680 --> 00:01:05,320
So we're talking about waiting patiently,

17
00:01:05,320 --> 00:01:09,360
and we obviously then have waiting impatiently.

18
00:01:09,360 --> 00:01:11,200
So what it means to wait patiently,

19
00:01:11,200 --> 00:01:18,920
it seems, is to be free from that angst,

20
00:01:18,920 --> 00:01:28,920
that agitation, that desire, first.

