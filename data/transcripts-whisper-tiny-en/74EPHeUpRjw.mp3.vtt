WEBVTT

00:00.000 --> 00:29.840
Okay, good. Good evening, everyone. Welcome to our live broadcast.

00:29.840 --> 00:46.480
Last night, I said something that I think I want to expand upon and it's that, just in passing

00:46.480 --> 00:59.440
you mentioned that religion is as it's classically understood or conventionally understood.

00:59.440 --> 01:11.480
It's a powerful force. Religion will lead. Good people to become bad people, bad people

01:11.480 --> 01:18.480
to become good people. It will lead people to do things they wouldn't otherwise do.

01:18.480 --> 01:35.840
Tremendous force that is caught up in this realm of human activity that we call religion.

01:35.840 --> 01:49.600
And so I thought I'd like to take this opportunity today to get religious, right?

01:49.600 --> 01:56.200
This is something that I think most of you know, I'm not much about. I don't have a lot

01:56.200 --> 02:10.200
of rituals and endurance to cultural, Buddhist practices. But I think there's something to

02:10.200 --> 02:22.080
be said for religiosity, the power of it. So I'd like us all to spend some time reflecting

02:22.080 --> 02:35.280
on this and maybe consider how we can incorporate religion into our lives.

02:35.280 --> 02:42.920
I'm doing some research right now or trying to formulate an argument in favor of a more

02:42.920 --> 02:53.160
inclusive, less specific, and less biased definition of the word religion. The one that

02:53.160 --> 03:02.920
really does it justice as a word. And so it's something along the lines of religion

03:02.920 --> 03:16.640
as whatever you take seriously. I think that definition would be unfavorably looked upon

03:16.640 --> 03:26.720
by many religious people who think there's something more. The argument is that most

03:26.720 --> 03:40.040
often the more is arbitrary and based on biases in regards to what one the individual

03:40.040 --> 03:44.280
takes seriously. So I take this sort of seriously. So I think that's what religion is all

03:44.280 --> 03:53.520
about. It's kind of bizarre actually how otherwise rational people become rational in many

03:53.520 --> 04:03.960
ways, but this is one of them. Thinking that the most important things in life are those

04:03.960 --> 04:09.360
things that I think are most important. And no one else could possibly think other things

04:09.360 --> 04:23.120
are more important. If they did, we should dismiss and disregard and it's not true religion.

04:23.120 --> 04:28.640
And I think it could possibly be argued and it has been argued and actually this is

04:28.640 --> 04:36.000
what the courts in Canada and America and the wrong world that I'm dealing with coming

04:36.000 --> 04:47.160
to terms with this idea of religion and how to allow for religious activity to privilege

04:47.160 --> 04:53.960
religious activity without privileging certain types of religious activity. So you need

04:53.960 --> 05:02.080
a definition that is yet at once restrictive enough to privilege certain types of activity,

05:02.080 --> 05:11.820
religious certain level of religiosity. And yet inclusive enough in general enough to not

05:11.820 --> 05:19.160
privilege only certain types of intents from the curiosity. And so they come up with this

05:19.160 --> 05:32.160
idea of that which relates to profound ideas about existence, about the goal, the goal,

05:32.160 --> 05:41.440
for the purpose of life, etc. For tonight's purpose, I just want us to think of

05:41.440 --> 05:49.860
religion as sort of a rather intense and maybe even extreme, that's probably the wrong

05:49.860 --> 06:02.720
word, but a level of religiosity that takes us out of our comfort zone that forces us to

06:02.720 --> 06:09.680
let go. Right, a lot of religion, not just Buddhism talks about letting go, but what they

06:09.680 --> 06:21.060
mean is give yourself up to God usually. And that that feeling of giving yourself up to

06:21.060 --> 06:27.440
a higher power. Many people have said that's religion, but there is something with a very

06:27.440 --> 06:44.480
religious about it. I don't know, my mind shows it as red, maybe you're just not close

06:44.480 --> 06:53.560
enough. Oh, here we are. Why was that down there? I found the problem. Should be better

06:53.560 --> 07:15.600
now. Let me know if it is. Let me know if it isn't. So I want to actually work with these

07:15.600 --> 07:24.080
kinds of ideas, the idea of letting go, the idea of surrender. But of course, as Buddhists

07:24.080 --> 07:29.520
we want to make sure that we're still objective. I think it's possible to be objectively

07:29.520 --> 07:42.080
religious, meaning we don't become particular. We don't give ourselves up to a specific,

07:42.080 --> 07:54.640
to something that is, we don't give ourselves up to some aspect of some sorrow, really.

07:54.640 --> 08:07.040
We give ourselves up to, we'll talk about what we give ourselves up to. I'm just failing

08:07.040 --> 08:17.680
all over tonight. Okay. The live stream is now up, or is it? Live stream is now, is not

08:17.680 --> 08:38.840
up. What's going on here? In great password. Seriously? It's going on.

08:38.840 --> 09:07.480
Okay. Now the live stream should be up. Technology. Technology and religion. What a combination.

09:07.480 --> 09:23.960
Anyway, the point is I'd like to offer you the opportunity to get religious. You don't

09:23.960 --> 09:29.080
have to. I think that's important. I'm not going to push you, but I want to be clear

09:29.080 --> 09:40.600
that that's that's the topic of tonight to potentially challenge some of our secular, secularist

09:40.600 --> 09:47.800
attachments, our unwillingness to commit ourselves. Unwillingness to let go and fully

09:47.800 --> 09:55.280
submers ourselves in an experience. We're often afraid of religion and rightly so. Religion

09:55.280 --> 10:04.320
can be pretty terrible. That's not the problem of the word religion or the category of religion.

10:04.320 --> 10:10.000
I think that's the point is that we can come up with Buddhist religion and we have, and

10:10.000 --> 10:22.040
this is religion that as Buddhists we can, we find palatable. It shows that the problem

10:22.040 --> 10:29.360
is not with the category of religious activity where you fully immerse yourself and fully

10:29.360 --> 10:36.080
commit yourself to something. The whole point is committing yourself to the right thing,

10:36.080 --> 10:40.920
which is of course the scary thing. How do you know what's right? So I'm going to go through

10:40.920 --> 10:45.840
it. I think for many people this isn't objectionable. For those of you who are Buddhists

10:45.840 --> 11:03.880
or Buddhist practitioners, we have what we call a ceremony to take on or to enter into

11:03.880 --> 11:12.480
the meditation practice. And this is what I went through when I first started meditating

11:12.480 --> 11:19.680
and I've helped countless people. Lots and lots of people through this ceremony have stopped

11:19.680 --> 11:27.200
doing it since I've been in Canada, but not for any particular reason except that it's been

11:27.200 --> 11:33.560
we've been busy setting up the center. It's the kind of thing you really need a staff.

11:33.560 --> 11:39.320
You need people to help you with, to really do a proper ceremony. But I'm going to go through

11:39.320 --> 11:52.040
the concept of the opening ceremony or the ceremony to receive the meditation practice.

11:52.040 --> 11:58.520
It comes actually from the Risudimanga and the tradition, the ancient tradition on the

11:58.520 --> 12:08.280
sorts of things you need to set yourself in or settle yourself in before you practice

12:08.280 --> 12:15.640
meditation. So it's not just arbitrary. It's based, I guess, you could say much, very much

12:15.640 --> 12:26.960
on the, the Arakakamatana, the types of meditations that protect and support in-site meditation.

12:26.960 --> 12:32.760
And you should recognize some of the meds we go through it. The first thing you do,

12:32.760 --> 12:41.520
though, this is a very old tradition, this is from the Risudimanga and probably before,

12:41.520 --> 12:46.880
you give yourself up to the Buddha. You really do. Buddhists actually do this. I'd like

12:46.880 --> 12:53.280
to invite you all to, to think about this. Mahasi Sayyatta gives a good explanation in one

12:53.280 --> 13:01.400
of his books, but how giving yourself up to the Buddha is a psychological support. It's

13:01.400 --> 13:09.480
putting your, putting your fate or your, your well-being in the hands of the Buddha. That's why

13:09.480 --> 13:15.560
we take refuge in the Buddha because we think of the Buddha as someone great, someone powerful.

13:15.560 --> 13:21.800
I mean, it's the same sort of psychological benefit of putting your trust in God. Of course,

13:21.800 --> 13:27.720
there's no God up there and there's certainly no Buddha up there who's going to hear your prayers.

13:27.720 --> 13:39.160
It's not the point. The point is the psychological, well, the, the, the commitment in your mind.

13:40.840 --> 13:48.000
And so we actually, we were saying in Pali, the words are ima, hang bhagava, ata bhavang, tumha kang

13:48.000 --> 14:04.280
bhani chachami. Let me recite that and then translate it. It means, ima hang bhagava, ata

14:04.280 --> 14:12.440
pa vong. I give this, or blessed one, I give this ata bhava, this self, this being that

14:12.440 --> 14:27.600
of, of my being. I entrust it into your care. I hand it over to you. So when we see

14:27.600 --> 14:37.680
the imanga in description of this, this, this determination, it describes the sort of people

14:37.680 --> 14:43.440
who are ideal for practicing meditation, that the kind of people who are ready to die for

14:43.440 --> 14:52.960
the meditation, they're ready to, to sacrifice their own lives. They have such great faith and

14:52.960 --> 15:00.240
such great conviction. That's the best sort of meditator they say it says. I'm going to give

15:00.240 --> 15:05.360
you a good argue that too much faith can be a bad thing if the person can get stuck in the wrong

15:05.360 --> 15:15.280
path and just go with it. But the ability to give yourself up and to detach yourself from your

15:15.280 --> 15:27.680
ordinary predilections, your ordinary hangups, your ordinary personality. Meaning give yourself up,

15:27.680 --> 15:37.200
take your whole being and just plunk it down in the realm of Buddhism. So we make this determination,

15:37.200 --> 15:44.240
we say, I give myself up to you, or blessed one. It's very religious, though. I think it's

15:44.240 --> 15:49.760
interesting, it's interesting not the Buddhism can be very religious, if you let it. And I think

15:49.760 --> 15:58.880
safely so because we're not deviating from the path, we're just intensifying it. Intensification

15:58.880 --> 16:10.000
can be scary, but it's not always wrong. As long as the, as long as the principles are true

16:10.000 --> 16:18.640
are, are stick to the problem, what is right. So we're not deviating from the path by placing our

16:18.640 --> 16:24.000
trust in the Buddha. We're just intensifying it and really committing ourselves and saying, okay,

16:24.880 --> 16:35.680
I really do trust the Buddha. It sounds very religious, it sounds like what Christians would say,

16:35.680 --> 16:40.560
but we, you know, there's the power to it and a good power in this case.

16:40.560 --> 16:50.480
And then secondly, you do the same thing with your teacher. So the teacher will be sitting

16:50.480 --> 16:57.200
at the front and you will say to the teacher, I'ma hang a jarya at the bhavang tummha kang

16:57.200 --> 17:11.120
bhavang bhavang, I give this, I give this being over to you, oh teacher, I entrusted in your

17:11.120 --> 17:26.400
care. I'm a part of this is overcoming the doubts and skepticism and that really are inevitably

17:26.400 --> 17:33.600
in the final form of their hindrance. In the beginning, of course, you want to be circumspect

17:33.600 --> 17:40.240
and investigate the teacher, but to some extent, at least to some extent you want to

17:42.000 --> 17:46.880
give yourself over to the experience. Now you can keep in the back of your mind, don't you

17:46.880 --> 17:56.880
don't give up your rational observation and analysis of whether this is actually good for you,

17:56.880 --> 18:07.120
but you put some trust in both the Buddha and in your teacher. That's great. It's important,

18:07.120 --> 18:13.200
psychologically, because too often we have meditators come and they're full of doubts about the

18:13.200 --> 18:20.640
practice and I think it's useful to give yourself up to things and if you're not,

18:20.640 --> 18:29.680
unless you're terribly blind, I mean most of us are intelligent people and eventually you'll

18:29.680 --> 18:39.040
find out whether your trust was misplaced or not, but if you're constantly doubting what you're

18:39.040 --> 18:44.080
doing, you never have that experience. I remember not saying this is how you have to go about it,

18:44.080 --> 18:53.600
but it can be useful to give yourself up to the religion. So that's the second thing.

18:54.400 --> 19:00.000
Then third thing you do is you ask for the kamatana. You actually make a request. We don't go around

19:00.640 --> 19:06.400
pushing meditation on others, but it was fairly careful about this right and from the very beginning,

19:06.400 --> 19:15.200
he wasn't even going to teach. He was in a sense, he could say stubborn. Sticking to his guns,

19:16.320 --> 19:23.600
he made a determination that he would just pass away without teaching anyone unless someone

19:23.600 --> 19:35.680
came and asked him to teach and so we take this stance of letting go ourselves, not preaching,

19:35.680 --> 19:44.080
not proselytizing, not trying to convert everyone. We leave the door open and this is very much

19:44.080 --> 19:53.520
in line with the principles of renunciation and letting go, not clinging.

19:58.160 --> 20:03.920
So we require the meditators to come and ask for this. Another aspect of this is making it clear

20:03.920 --> 20:11.520
that this is something that is being given to you. It's not something you deserve or we have an

20:11.520 --> 20:18.800
obligation to give you. This isn't the right, so you've got many meditations and they charge you

20:18.800 --> 20:25.600
for it. I've talked several times about how dangerous and problematic that is because

20:26.320 --> 20:32.240
then you feel entitled. The whole relationship is different. I can tell you to

20:33.760 --> 20:39.520
stay up all night or I can tell you my meditation all sorts of crazy things, but no,

20:39.520 --> 20:49.760
but I can tell you to, I can push the meditators because I'm not getting paid for this.

20:49.760 --> 20:54.720
I don't have any responsibility to coddle you. It really is my way or the highway.

20:57.440 --> 21:06.000
Does that make sense? We are quite flexible, but there's an important power and it's a religious

21:06.000 --> 21:15.280
power, this coming and asking, beseeching, beseeching the teacher.

21:15.280 --> 21:29.920
Kneeban is a kneeban, kneeban, kneeban, kneeban, kneeban, kneeban, kneeban, kneeban, kneeban. For the purpose of seeing clearly

21:29.920 --> 21:39.840
for myself, the truth of kneeban, please, venerable sir, give me the kneeban, give me the meditation.

21:39.840 --> 21:50.720
And then my memory is a bit rusty as to the order, but I think the next thing we do,

21:51.920 --> 21:59.600
so we're ready to receive the kneeban. We first have to now establish our minds,

21:59.600 --> 22:05.440
we've already started, but we now get into certain meditations that are going to support us

22:05.440 --> 22:11.360
and calm us enough to be able to accept the meditation properly.

22:13.360 --> 22:19.760
And so the next one is, I think, maita. If I've got the order right, I probably don't.

22:24.320 --> 22:29.920
Now we'll go with it. Anyway, provisionally the order may be wrong, it doesn't really matter.

22:29.920 --> 22:38.480
The next thing would be maita. So you say to yourself, ahang sukito humi, may I be happy?

22:38.480 --> 22:40.240
Well, that's why we're doing this, right?

22:42.960 --> 22:47.440
And then you can expand upon that. May I be free from suffering?

22:47.440 --> 22:59.920
May I be free from enmity, free from sickness, free from grief?

23:01.760 --> 23:06.480
And then you say sabesa tas sukita huntu, may all beings be happy.

23:07.280 --> 23:14.640
This is the important part, this is where you begin to settle your karmic debts, settle your

23:14.640 --> 23:26.960
cycles of revenge, your grudges, your feuds, your conflicts with other people, settle your affairs.

23:29.440 --> 23:35.280
They make a big deal about apologizing to people, especially enlightened beings.

23:35.280 --> 23:39.760
If there's any enlightened being or even spiritual,

23:39.760 --> 23:45.600
spiritually advanced person who you've wronged, who you've maligned,

23:46.720 --> 23:53.120
you should go and apologize to them. And there's great emphasis placed on this because it can

23:53.120 --> 24:00.640
be a hindrance to your path. And this is part of that, wishing happiness, settling your thoughts

24:00.640 --> 24:06.480
towards all other beings, whether you've had grudges or problems, conflicts with them in the past,

24:06.480 --> 24:13.120
you've settled all that here, put it all aside, you leave that baggage outside the door,

24:13.120 --> 24:26.880
and you enter into the temple, the temple of Buddhism, very religious, give up all your grudges.

24:26.880 --> 24:41.200
And then it starts to turn serious, we contemplate death, adhuang maiji vitang. My life is unstable,

24:43.200 --> 24:48.400
dhuang maranang, death is certain, death is for sure.

24:48.400 --> 25:02.480
dhuang mai, adhuang, my life is uncertain, niyatang maranang, or maranang niyatang, death only is certain.

25:07.920 --> 25:12.320
As again, one another, you can see the pattern here, we've sort of dealt with the Buddha,

25:12.320 --> 25:18.960
and often this will be accompanied by recollections of the Buddha as well. And then we've

25:18.960 --> 25:24.960
done Mehta and now death. This is one of the protective kamatanas because this one, death gives

25:24.960 --> 25:31.360
you conviction and gives you encouragement. It reminds you, oh yeah, that's why I'm doing this,

25:31.360 --> 25:37.760
because all of those defilements I have that I'm building up and all the attachments and addictions,

25:37.760 --> 25:44.960
they're just leading me straight to death and the danger of death is that it determines your

25:44.960 --> 25:54.640
birth, your next birth. If you've done bad, you get bad, if you've done good, you'll get good.

26:00.080 --> 26:05.600
So we spend some time reflecting on death, at least reminding ourselves, every time we begin

26:05.600 --> 26:11.600
the meditation, this is something that we do, we should do. And again, we've been negligent here at

26:11.600 --> 26:18.640
our new center, so we'll have to figure out some, eventually some way to incorporate this religious

26:18.640 --> 26:30.240
aspect into it. Maybe. At least I think we can make it optional, so if someone wants to do it,

26:30.240 --> 26:37.440
we could have it available and we could do the ceremony for anyone who wants to do it. There's

26:37.440 --> 26:41.920
also a closing ceremony. They're quite beautiful. The last thing you do is ask forgiveness from each

26:41.920 --> 26:50.400
other formally. Anyway, so yeah, we'll get to that. So after death, then it gets into the actual

26:50.400 --> 27:19.760
this, okay, focusing on insight meditation. You make a recollection, that path by which

27:20.480 --> 27:28.960
all Buddhas and all their enlightened disciples have traveled in order to become enlightened

27:29.760 --> 27:36.000
is the path of the four Satipatana, the aikayana manga of the four Satipatana.

27:40.240 --> 27:44.160
That's a recollection we do. We remind ourselves this path that I'm following,

27:44.160 --> 27:51.200
this path that I'm undertaking now. This is the path taken by all Buddhas in there and like

27:51.200 --> 27:56.720
anyone who wants to become enlightened has to follow the path of Satipatana. This is the path

27:56.720 --> 28:06.400
I'm entering upon. It's an ancient path. It's a hallowed path. It's holy. It is the right way,

28:06.400 --> 28:14.560
the one way, the only way. It is the way to enlightenment.

28:20.400 --> 28:28.320
And so we gain courage and encouragement in this way and conviction and sort of a sense of

28:28.320 --> 28:35.200
severity, right? This is not some homie that we're undertaking. It's not a game that we're playing.

28:36.080 --> 28:44.240
This path, this is the path of the Buddhas.

28:44.240 --> 29:00.720
And then I'm missing something.

29:00.720 --> 29:14.240
Oh, I mean, shoot. I've got it here somewhere. Let's go look at it.

29:14.240 --> 29:38.800
Oh, that's something else. Anyway, if there is something missing, I'll try and find it. But the last

29:38.800 --> 29:48.960
part is Imaya, Dhammanu, Dhamma, Patipati, Ratanatayangpoo, Jamie. And this is a fairly religious thing. But

29:51.040 --> 30:00.720
it relates well to people who have accepted Buddhism as their religion because we pay respect to

30:00.720 --> 30:07.040
the Buddha. We pay respect to the Dhamma, we pay respect to the Sun. We have this relationship

30:07.040 --> 30:14.720
psychologically with the Buddhas, our leader and someone who we revere, bringing

30:14.720 --> 30:23.200
bringing flowers, candles and incense to the Buddha statues and so on, thinking fondly and

30:23.200 --> 30:32.880
reverentially towards the Buddha, bowing down before the Buddha, etc. We do this. So this last one,

30:32.880 --> 30:37.760
it says through this practice of the Dhamma, in order to realize the Dhamma,

30:41.200 --> 30:47.760
I pay homage to the Triple Gem, to the Buddha, the Dhamma, and the Sun, go to three refuges.

30:50.560 --> 30:57.520
And the neat thing about this is it taps into the, or ties into the Buddhist idea that

30:57.520 --> 31:06.160
the best way to revere someone, the best way to pay respect to anyone to the Buddha, especially,

31:06.880 --> 31:12.640
is to practice his teachings. This is the true way you honor a Buddha. I would have said this before

31:12.640 --> 31:19.600
he passed away, he said, flowers and candles and incense, that's not the best way to honor a Buddha.

31:19.600 --> 31:28.080
And so here's our religious practice. We can consider that another great thing we're doing is

31:29.440 --> 31:41.280
we are promoting Buddhism. We are supporting Buddhism. We are engaged in the continuation,

31:41.280 --> 31:49.920
the prolongation of the life of the Buddha's teaching in our practice of Satyipatana.

31:53.280 --> 31:56.720
Anna knows the one. I think he said after the Buddha passed away, he said,

31:58.160 --> 32:03.520
this is it whether or not people practice the force that the Buddha will determine whether the

32:03.520 --> 32:14.880
Buddha says in the last so long time. And so here we pay the greatest and the highest respect to

32:14.880 --> 32:19.120
the Buddha through the practice of the force that he put on it.

32:26.240 --> 32:30.000
There would also be in the ceremony. There would have been before this, the taking of the eight

32:30.000 --> 32:34.560
precepts. Of course, there's actually a formal ceremony for taking the eight precepts. And then

32:34.560 --> 32:42.320
after this, the last thing that we would do is ask forgiveness from each other. All the meditators

32:42.320 --> 33:00.640
would bow down to the teacher and say,

33:00.640 --> 33:16.960
oh, teacher, chariya, pamadhi, na, dua, rata, yina, katang, sabanga, parada, dang, all any wrong doing

33:18.960 --> 33:24.080
that I have done through the three doors. That means by way of body, by way of speech,

33:24.080 --> 33:36.480
by way of thought, out of negligence, out of a lack of rigid religions, or a lack of religiosity.

33:36.480 --> 33:52.000
kamatami Bhante, please forgive me when I will serve. And then the teacher says,

33:52.000 --> 34:02.080
a hang wu, kamami, tumhi, be me, kamitamam. I forgive you. You as well should forgive me for such,

34:02.080 --> 34:11.600
for any such thing. And this is important because even through the throughout the meditation course,

34:11.600 --> 34:18.560
meditators can often get very angry at their teacher and that's okay. They can have bad thoughts

34:18.560 --> 34:26.640
and they can make them teacher's life miserable or more difficult anyway. And so at the beginning

34:26.640 --> 34:32.880
of the course and at the end of the course, you as forgiveness, the teacher also is not perfect.

34:32.880 --> 34:41.440
May say the wrong thing may teach the wrong thing, may even present a poor example of the practice,

34:42.480 --> 34:45.680
and so they ask forgiveness from the students for not being perfect.

34:49.840 --> 34:53.280
It's a great way to clear the air. All in all, it's quite a beautiful ceremony,

34:53.280 --> 35:01.120
very religious. And so the argument I want to make, the whole point of this is to

35:02.400 --> 35:10.080
present the concept of religion to Buddhists or to people practicing Buddhist meditators as

35:10.080 --> 35:19.280
not something that should be discounted or discarded. It could be. You can avoid it or

35:19.280 --> 35:29.760
minimalize the importance. But you can also take it up that it could be something

35:29.760 --> 35:34.720
palatable to a Buddhist. We don't have to say that Buddhism is not a religion and that religious

35:34.720 --> 35:40.800
activity has no place in Buddhism because obviously there are millions of Buddhists around the

35:40.800 --> 35:50.400
globe who would disagree. But to some extent, we consider ourselves a bit elitist as being

35:50.400 --> 35:58.400
people who focus on true Buddhism. There's a lot of Buddhists out there who don't really practice

35:58.400 --> 36:05.360
on the level that we would consider proper Buddhist practice. And I think it's easy to get

36:05.360 --> 36:12.160
arrogant and think of all of that silly stuff is just culture. That's not real Buddhism.

36:15.200 --> 36:21.840
We throw the baby out with a bathwater to some extent. The Buddha was, I think, quite religious

36:23.040 --> 36:30.560
and promoted a sense of religiosity. And there's definitely a long tradition in the meditation

36:30.560 --> 36:37.520
tradition that I follow of taking it quite seriously and making it quite religious in this way,

36:37.520 --> 36:46.160
really devoting yourself to it in a very true religious sort of way. So there you go. I think this

36:46.160 --> 36:51.840
is something that I've never done before is go through in detail the opening ceremony in a video.

36:51.840 --> 37:03.520
So now we have my take on Buddhist religiosity. That's the number for tonight. If anyone has any

37:03.520 --> 37:28.240
questions, I'm happy to answer. What do I think I'm doing past life regressions?

37:28.240 --> 37:38.880
You better go. Sorry, just wait for the meditators to actually go back and meditate before we get

37:38.880 --> 37:49.600
into the speculative stuff. Past life regressions. I don't think about them so much. I think I'm

37:49.600 --> 37:56.560
going to take a pass on that one as being one of those things I don't think about. And therefore,

37:56.560 --> 38:12.160
don't have an answer. Why don't I have my meditators listen to questions? Because you all have

38:12.720 --> 38:15.760
speculative questions that are just going to make them think too much.

38:19.440 --> 38:23.840
The thing is, we're on the internet. Do you have to remember? And so people ask me questions

38:23.840 --> 38:31.280
about crazy stuff. Stuff that I don't want my meditators to worry about. These people are doing

38:31.280 --> 38:39.920
eight, ten, twelve hours of meditation a day. Last thing they need is to be talking about

38:39.920 --> 38:44.160
politics. One of my meditators doesn't even know what their president is.

38:44.160 --> 38:52.880
She's from America and she doesn't yet know.

38:58.560 --> 39:03.600
Okay, you've rewarded it. What is a good way to remove doubts about rebirth? I mean, I could

39:03.600 --> 39:09.360
have entertained that aspect of it, but I'm still not that interested in it. Like if you have

39:09.360 --> 39:18.400
doubts about rebirth, you should say doubting, doubting. I've talked about, I think it's important

39:18.400 --> 39:31.040
to recognize the flaw in our thinking that after death, nothing is the null hypothesis,

39:31.680 --> 39:39.120
meaning that it's the default. And there's no reason to think that except if you're a physical

39:39.120 --> 39:45.680
ist, which is already taking something you have to take on belief, there's no reason to be a

39:45.680 --> 39:52.800
physicalist rather than a biosentrist or a phenomenal for a phenomenologist,

39:56.400 --> 39:58.000
positivist, maybe even.

39:58.000 --> 40:11.040
So if you have doubts about rebirth, I would just say doubting, doubting, and let it go. I mean,

40:11.040 --> 40:17.040
I guess the real point is I wouldn't concern too much about rebirth. You know, you accept,

40:17.040 --> 40:21.760
and I think that's a part of religion as you accept these things provisionally. In the back of

40:21.760 --> 40:26.080
your mind, you say, well, you know, this may all turn out to be wrong, but I'm going to give

40:26.080 --> 40:33.360
it an honest try. And you, you'd be clear with yourself that there's no, there's no, it's not,

40:33.360 --> 40:44.720
there's no reason, there's no better evidence for after death, nothing than there is for after death,

40:44.720 --> 40:47.840
rebirth or birth again.

40:47.840 --> 41:01.920
Because what we see now is a continuation of the mind, to say that this, for some reason, stops

41:01.920 --> 41:09.280
at death is a claim, you know, and you have to provide backing for that. And the backing, of

41:09.280 --> 41:14.960
course, is the idea that the brain is the source of the mind, but that's never been proven.

41:14.960 --> 41:22.560
There's no, there's no, there's no conclusive evidence. And in fact, there's counter evidence

41:22.560 --> 41:28.320
that shows that the mind seemingly can be active when the brain is not.

41:35.920 --> 41:38.480
I know I've talked about all that, lots and lots.

41:38.480 --> 41:44.560
I'd say it's interesting stuff to talk about it certainly within the realm of what I should

41:44.560 --> 41:51.200
be talking about. So I don't mean to dismiss the question of why in the heck should we believe

41:51.200 --> 41:54.960
in rebirth? But I have talked about it quite a bit.

41:56.800 --> 42:01.840
When we're overcome with lust and desire for a person, do we simply become aware of the

42:01.840 --> 42:07.200
lusting and know that until it reduces an intensity? Yeah, not just the last, you'll note the

42:07.200 --> 42:12.240
seeing and the thinking and the feeling, the pleasure of it, because there's usually

42:12.240 --> 42:13.920
pleasure associated with lust.

42:19.440 --> 42:24.400
You'll note all of that. And it's certainly not easy. It's not like that suddenly magically

42:24.400 --> 42:31.040
going to make all your problems go away. But if you get good at it, you can change your life,

42:31.040 --> 42:33.840
and you can certainly weaken and eventually give up these.

42:33.840 --> 42:39.120
There's a really ridiculous attachment to things that are really disgusting.

42:39.120 --> 42:42.960
You know, the human body is certainly not full of sugar and spice and all things nice.

42:52.800 --> 42:56.800
What is the difference between wanting to end samsara and Vibhavatana?

42:56.800 --> 43:00.320
Someone asked me this just, yes, just this morning actually.

43:00.320 --> 43:07.520
Is it right to say this, we're like a couple that brought me food this morning asked me about Vibhavatana?

43:08.720 --> 43:12.720
Is it right to say that the first case is simply coastal, the chanda is opposed to green?

43:18.240 --> 43:23.680
Well, I would say wanting to end samsara is at least dangerously close to Vibhavatana.

43:23.680 --> 43:31.600
I would say wanting to end samsara is not necessarily wholesome.

43:33.040 --> 43:39.360
I didn't be inclined to think of it as unwholesome. Very subtly unwholesome, but at the kind of unwholesome

43:39.360 --> 43:45.440
that gets you born in the Brahma realms, the formless realms. Vibhavatana.

43:48.160 --> 43:53.120
It's not about not wanting to be reborn in samsara. That's just the intellectual outlook and

43:53.120 --> 44:00.320
it's sort of an aversion to scapism. Buddhist practice is about understanding samsara,

44:01.520 --> 44:08.160
seeing it clearly. And the result is not an aversion to it or desire to be free from it.

44:08.160 --> 44:19.200
The result is an ambivalence and disdain but a disregard for it, a complete and utter

44:19.200 --> 44:30.080
weariness or disinterest in samsara. See, the only reason that samsara continues is because

44:30.080 --> 44:39.200
we're interested, because we like it or we dislike it. But the goal is to see it, to understand

44:39.200 --> 44:45.120
it. Because when you understand it, you see this is useless, absolutely and utterly without any

44:45.120 --> 44:54.880
purpose whatsoever. You just start to get more and more disenchanted is the word and peaceful

44:54.880 --> 44:57.760
as a result until finally there's perfect peace.

45:02.880 --> 45:07.920
These emotions are so powerful and they completely hijack your mind. I've tried the meditation

45:07.920 --> 45:13.360
on the disgustiness of the body, but it doesn't help that much. A good one is take the body parts

45:13.360 --> 45:20.480
of the person that you find attractive and visualize cutting them up, visualize slicing them up

45:20.480 --> 45:27.440
into pieces. I mean, that's temporary. That's just like putting a bandaid over it. It still doesn't

45:27.440 --> 45:36.800
stop the festering. Nope. That's why the sati pataan are the only way. The only way to overcome

45:36.800 --> 45:44.720
things like lust is to see the lust clearly as it is and all the aspects of it, the pleasure and

45:44.720 --> 45:52.560
the experience and the thoughts you have about it. Because it really does. Once you see it clearly

45:52.560 --> 46:09.040
become disenchanted, you lose your raga, you lose your raga. Yeah, well, if you want to overcome lust,

46:10.960 --> 46:17.760
stay away from people who you lust after. It's a good sort of, I mean, that's just artificial.

46:17.760 --> 46:23.280
It doesn't solve the problem. It's not the answer, but it's a good sort of stopgap measure.

46:23.280 --> 46:29.280
That's why people go to meditation centers. I remember there was one we were doing courses

46:29.280 --> 46:35.680
before in Stony Creek and there are these paintings on the walls and one of the

46:35.680 --> 46:40.240
members of the daughters of Mara and of course they painted it, where you can actually see their

46:40.240 --> 46:49.600
breasts. Originally, I was there when he was painting it and he originally painted it. It was 17

46:49.600 --> 46:57.280
years ago. He originally painted it with these women bare chested and then they happen to

46:57.280 --> 47:04.480
complain and so he put some sort of gauze. He usually painted a gauze over them like

47:04.480 --> 47:13.200
when he called some kind of semi-transparent cloth. Anyway, there was one of our meditators who

47:13.200 --> 47:18.240
came and we were giving spots and we gave him the spot right under this thing and he came to

47:18.240 --> 47:23.840
me off frustrated and said, of course I get put right under that. I have to do my walking and

47:23.840 --> 47:29.440
sitting meditation right under these sexually provocative pictures of these women.

47:29.440 --> 47:34.880
But yeah, you kind of want to avoid that sort of thing.

47:34.880 --> 48:02.160
We could set up a channel ground in second life. We could have models, pictures. They should.

48:02.160 --> 48:11.040
We should have a channel ground. Place with models of dead bodies with blood coming out and

48:11.040 --> 48:18.320
past coming out and pictures. You could just sit there and look at pictures of cancers,

48:18.320 --> 48:26.240
growths and pictures of cut-up bodies. If you do a Google search for cancer, it's quite interesting.

48:26.240 --> 48:39.200
The Google image search for different kinds of cancer, breast cancer is probably a good one.

48:39.200 --> 49:06.240
That's an interesting point about virtual reality. Obviously, in horrific ways, you can do things

49:06.240 --> 49:11.920
that you would never do in reality. But here's an interesting one. The Buddhist one, we could actually

49:11.920 --> 49:17.040
set up a virtual reality channel ground and we wouldn't have to kill people. We wouldn't have to find

49:17.040 --> 49:30.320
dead bodies. How would that work? I think pictures would be the best. You could have a room full

49:30.320 --> 49:39.360
of pictures to contemplate photos. I don't think trying to make an actual decomposing corpse,

49:39.360 --> 49:44.560
maybe it would work. If we had a corpse that was scripted to decompose when you click on it,

49:45.760 --> 49:48.160
slowly, slowly decompose,

49:48.160 --> 50:04.160
Dar has a channel ground. They see she's forward thinking. Dar has been conspicuously absent.

50:04.160 --> 50:19.120
I understand she's gone away or something. It's funny how every time I'm giving talks,

50:19.120 --> 50:35.120
now the sim turns dark as I give the talk. It's night now. It's quite the visual.

50:35.120 --> 50:41.920
Well, thank you. You've been a wonderful audience. It's always nice to meet up here.

50:41.920 --> 50:47.760
We've got the regulars and for those of you tuning in on YouTube. Thank you for tuning in.

50:49.440 --> 50:53.440
Please share the videos, subscribe to the channel, etc, etc.

50:54.880 --> 50:59.920
And then there's also those of you who might be listening in on audio. Thank you. Thank you for your

51:01.360 --> 51:06.640
patient attendance and your aspiration towards goodness.

51:06.640 --> 51:16.640
Have a good night, everyone.

