1
00:00:00,000 --> 00:00:27,000
Good evening, everyone broadcasting live April 24, 2016, the latest quote is part of a famous

2
00:00:27,000 --> 00:00:49,000
story about a monk who had dysentery, and if you read this, the other monks looking after you,

3
00:00:49,000 --> 00:00:57,000
and he says, because I'm not useful to them, I'm not used to them. And then he goes and asks them,

4
00:00:57,000 --> 00:01:03,000
and then the monks and the monks say, he's a wide, wide angelic capture, and they can refer

5
00:01:03,000 --> 00:01:10,000
because he's not used to them.

6
00:01:10,000 --> 00:01:16,000
But if you read this book, it's not going.

7
00:01:16,000 --> 00:01:21,000
If you read the book, you're not going to hear it.

8
00:01:21,000 --> 00:01:29,000
But if you read the book, you're not going to hear it.

9
00:01:29,000 --> 00:01:37,000
If you read the book, you are going to hear it.

10
00:01:37,000 --> 00:01:48,000
He is so vanti, vanti, vikhu, vikhu, nan, akar, rakho means he does nothing for them.

11
00:01:48,000 --> 00:01:55,760
Not quite sure what that means, vikhu, vikhu, nan, for the vikhu is akar, rakho.

12
00:01:55,760 --> 00:01:58,640
He doesn't do things for the vikhu.

13
00:01:58,640 --> 00:02:07,920
Vikhu, nan, vikhu, nan, for that reason, because of that, they don't look after him.

14
00:02:07,920 --> 00:02:15,840
But this is something, nati wo, vikhu, ema, da.

15
00:02:15,840 --> 00:02:25,920
There is no, you have no mother, monks, nati, pita, you have no father.

16
00:02:25,920 --> 00:02:34,720
Yevo, barta, he, yung, who would look after you.

17
00:02:34,720 --> 00:02:49,200
Tumhe, te, bikhu, ema, nyan, nati, pata, upata, he's seta, atakho, jarahe, upata, he's seti.

18
00:02:49,200 --> 00:03:05,440
Takho, jarahe, jarahe, jarahe, upata, he's seti, kho, jarahe, now, who lives in other form.

19
00:03:05,440 --> 00:03:13,840
If you don't look after each other, monks, nyan, one to the other, each other, atakho,

20
00:03:13,840 --> 00:03:21,600
jarahe, then who now will look after you?

21
00:03:21,600 --> 00:03:28,320
Then he says something that's quite famous, yo, bikhu, ema, upata, heya, so gila, nan, upata,

22
00:03:28,320 --> 00:03:36,360
heya, who would look after me, should look after sick people.

23
00:03:36,360 --> 00:03:42,040
It doesn't actually say monks, but that's the implication here, and people, many would argue

24
00:03:42,040 --> 00:03:47,080
that that's what it means, but it's been used to interpret the importance of looking

25
00:03:47,080 --> 00:03:49,160
after sick people.

26
00:03:49,160 --> 00:03:53,560
I think that stretching is probably more like, because monks aren't really allowed to

27
00:03:53,560 --> 00:04:01,400
look after lay people, or sick, that wouldn't be appropriate, but it's interesting if

28
00:04:01,400 --> 00:04:07,160
you think of it as a lay person, if you're not a monk, I think about the idea of helping

29
00:04:07,160 --> 00:04:16,280
sick people, so it's a very important thing that Buddha actually found this to be important

30
00:04:16,280 --> 00:04:26,040
for the monks at least, if not for general purposes, or in a society looking after sick

31
00:04:26,040 --> 00:04:35,160
people in general.

32
00:04:35,160 --> 00:04:39,520
But what's the most interest to me, as the part where he says, nati will bikhu, ema,

33
00:04:39,520 --> 00:04:45,040
taya, you don't have a mother, or a father, who would look after you.

34
00:04:45,040 --> 00:04:51,560
He's making a very good point here, and I think it goes beyond monasticism, obviously

35
00:04:51,560 --> 00:04:57,920
for monks, it's an important point, is that we don't have family, we don't have caretakers,

36
00:04:57,920 --> 00:05:04,440
if we don't look after each other, who will look after us.

37
00:05:04,440 --> 00:05:17,280
And I think on a broader level, this applies to Buddhists in general.

38
00:05:17,280 --> 00:05:25,120
In Thailand, they talk about nyaati, dhamma, dhamma nyaati, relatives by the dhamma, sisters

39
00:05:25,120 --> 00:05:32,240
and brothers and mothers and fathers in the dhamma.

40
00:05:32,240 --> 00:05:40,640
Because beyond monastics, it's true that, especially when you get on a spiritual path, when

41
00:05:40,640 --> 00:05:47,840
you start to practice Buddhism, you're less and less able to rely upon your families and

42
00:05:47,840 --> 00:05:53,000
the people around you who are not Buddhist or not practicing.

43
00:05:53,000 --> 00:05:58,400
Even in Buddhist countries, if your family is not meditating, in Buddhist countries,

44
00:05:58,400 --> 00:06:07,000
they drink alcohol, they kill, they lie, they cheat on these things, because someone

45
00:06:07,000 --> 00:06:13,520
calls themselves even Buddhist, but much more for those of us who have become Buddhist

46
00:06:13,520 --> 00:06:26,200
or come to practice Buddhism, maybe against the hopes of our family, maybe against their

47
00:06:26,200 --> 00:06:36,800
wishes, but certainly we leave them, we go somewhere that they don't follow, so in a sense

48
00:06:36,800 --> 00:06:44,720
we leave them behind, meaning they're in one place where another, spiritually.

49
00:06:44,720 --> 00:06:50,920
So we stop killing, they still kill, we stop stealing, maybe they don't concern themselves

50
00:06:50,920 --> 00:06:59,680
about morality, they have no concentration, they don't use, they don't cultivate wisdom,

51
00:06:59,680 --> 00:07:05,840
they don't have the same understanding what we would call wisdom.

52
00:07:05,840 --> 00:07:16,200
It's interesting because last night, I just got back from New York, but last night yesterday

53
00:07:16,200 --> 00:07:25,680
was my mother's birthday, and so we had a hangout, we had one of these, basically what

54
00:07:25,680 --> 00:07:32,240
I'm doing now, not live, of course, or not public, but we got together from all around

55
00:07:32,240 --> 00:07:38,440
the world, my brothers in Taiwan, my other two brothers are in, well, they were actually

56
00:07:38,440 --> 00:07:44,720
with my father, so I think they were only, but they were with my father and then my

57
00:07:44,720 --> 00:07:56,520
mother's in Florida and I was in New York, so four different places, yeah, and so we got

58
00:07:56,520 --> 00:08:08,880
together, we joined the hangout, and it was actually somewhat disappointing, if you're

59
00:08:08,880 --> 00:08:14,040
going to let yourself get disappointed, I mean, what is the right word is?

60
00:08:14,040 --> 00:08:29,800
It was awkward, maybe, because of one of the members of the hangout was clearly drunk,

61
00:08:29,800 --> 00:08:33,560
and because it was passed over last night, if you know the Jewish holiday, my father's

62
00:08:33,560 --> 00:08:44,080
family's Jewish, and on pass over they drink a lot of wine, and it can get somewhat drunk,

63
00:08:44,080 --> 00:08:50,640
and so at least one person, if not more, was somewhat inebriated and slurring their

64
00:08:50,640 --> 00:09:01,720
words, and so the conversation quickly turned into things like alcohol and marijuana and

65
00:09:01,720 --> 00:09:12,200
they're talking about how, I mean, I was quiet through most of it, my mother's really

66
00:09:12,200 --> 00:09:18,240
great, she doesn't do any of that, I think she smokes marijuana, she used to anyway, but

67
00:09:18,240 --> 00:09:26,240
she really gave up alcohol and she's interested in a clear mind, I think, I think she

68
00:09:26,240 --> 00:09:38,280
doesn't drink, but then my brother in Taiwan, he started talking about this, something

69
00:09:38,280 --> 00:09:44,720
Harrier, this Harrier group, and they played this game where I wasn't listening really,

70
00:09:44,720 --> 00:09:50,600
but it was this thing to the power where at the end of the game they come together and

71
00:09:50,600 --> 00:09:57,800
they have a champagne breakfast, I think it was called, and he was showing pictures of

72
00:09:57,800 --> 00:10:04,960
cased like crate, no coolers full, large coolers full of champagne, whiskey and beer and

73
00:10:04,960 --> 00:10:15,040
every kind of alcohol, and he said he drank for 20 hours straight, 20 hours, and he said,

74
00:10:15,040 --> 00:10:20,360
you know, I don't want to talk about it because it sounds like a bragging, and the rest

75
00:10:20,360 --> 00:10:23,000
of us are kind of well, you know.

76
00:10:23,000 --> 00:10:26,560
But then he started saying something that's really interesting, I mean, there's a point

77
00:10:26,560 --> 00:10:32,400
why I'm telling this and why I'm talking about things that probably I shouldn't, it's

78
00:10:32,400 --> 00:10:37,840
isn't the kind of thing you should really broadcast, but you know, we take this, I'm not

79
00:10:37,840 --> 00:10:44,080
really criticizing, I'm looking at the state of people, it's interesting because then

80
00:10:44,080 --> 00:11:02,560
he went on about how much better it is, this kind of life, how much healthier it is,

81
00:11:02,560 --> 00:11:07,840
and how he looks at people, the rest of us back, or people back in Canada, all of his

82
00:11:07,840 --> 00:11:15,040
friends, and their hair is turning gray, and they look old, and it's because they're

83
00:11:15,040 --> 00:11:22,080
they're kind of serious, they're too serious, they take things too seriously.

84
00:11:22,080 --> 00:11:30,240
And it was quite clear that drinking for 20 hours was somehow somehow the fountain of youth

85
00:11:30,240 --> 00:11:39,200
or something, living in Taiwan, I'm sorry, I don't mean to bring my brother into this

86
00:11:39,200 --> 00:11:47,600
really, but it's such a good example, because we think like this, we think happiness leads

87
00:11:47,600 --> 00:11:56,440
to happiness, I mean it sounded so much like the angels, how angels think of heaven, they

88
00:11:56,440 --> 00:12:07,520
think that it's a good life, they've won, they think they won, they've beaten some saran,

89
00:12:07,520 --> 00:12:16,440
they found the right way to live, they've earned it, and they've found the right path,

90
00:12:16,440 --> 00:12:20,880
and they're doing the right thing, what in fact they're doing the thing, like we saw

91
00:12:20,880 --> 00:12:31,600
Pakistan, they're eating stale food, means they're eating up, they're eating up their

92
00:12:31,600 --> 00:12:36,240
using up their good karma from the past.

93
00:12:36,240 --> 00:12:41,560
We all have this potential in life, right, we have the potential to use up, to use this

94
00:12:41,560 --> 00:12:48,600
great opportunity we have as human beings, we have such power, we can do so many things

95
00:12:48,600 --> 00:12:53,840
as human, and people say well what's so great about being a human, humans can be horrible

96
00:12:53,840 --> 00:12:54,840
people.

97
00:12:54,840 --> 00:13:02,720
I was just reading about some of the things that have gone on, like in South and Central

98
00:13:02,720 --> 00:13:11,720
America, the dictatorships that were set up by the CIA or whatever, I don't know, but

99
00:13:11,720 --> 00:13:26,920
those dictatorships, there was this, they had this prison in an auditorium, and they sat

100
00:13:26,920 --> 00:13:32,880
all the prisoners in the crowds, and then they just like opened fire on them, shot them

101
00:13:32,880 --> 00:13:38,360
with machine guns, but that was in it, they would dry, every so often they would drag

102
00:13:38,360 --> 00:13:44,480
someone down from the stands and torture them in front of everyone, like theater, and

103
00:13:44,480 --> 00:13:54,240
at the same point human beings can do terrible things, and so we have this power, we have

104
00:13:54,240 --> 00:14:03,680
the power to live a hedonistic lifestyle, to just enjoy as much pleasure as we can to not

105
00:14:03,680 --> 00:14:10,440
take anything seriously, and no matter what we do it appears that we get away with it,

106
00:14:10,440 --> 00:14:16,240
maybe for the most part, not always, but much of the time for many of the people in this

107
00:14:16,240 --> 00:14:23,880
world, it appears that we get away with whatever we do, but it's quite lazy to think

108
00:14:23,880 --> 00:14:29,600
like that, like take alcohol for example, someone who says look at alcohol and drink, drink

109
00:14:29,600 --> 00:14:39,400
and be merry, and it's a good way to live, look at me, I'm doing great, in this anecdotal

110
00:14:39,400 --> 00:14:46,080
evidence, look at how great I am in it, I'm doing this, there was a funny story, a monk

111
00:14:46,080 --> 00:14:57,440
once told me, or I overheard him telling someone else, this man walked in, wrinkled, sort

112
00:14:57,440 --> 00:15:10,600
of, with the pockmarked face, looked to be like, just really aged, you know, this man

113
00:15:10,600 --> 00:15:17,080
sort of walked in, but he was, he had bright eyes, you know, he looked, he looked ancient,

114
00:15:17,080 --> 00:15:26,040
but he had some radiance about him, and he started talking about how he's never been in

115
00:15:26,040 --> 00:15:37,320
the hospital, and he's never been sick, you know, it all has his life, he's never had any

116
00:15:37,320 --> 00:15:45,320
serious health issues, and these young men were asking him, oh, how do you, what do you

117
00:15:45,320 --> 00:15:46,320
do?

118
00:15:46,320 --> 00:15:54,080
And he says, oh, I drink a bottle of whiskey every day, smoke a carton of cigarettes, and

119
00:15:54,080 --> 00:16:00,600
another carton, he'll say a pack of cigarettes, and day, and bottle of whiskey, and, and

120
00:16:00,600 --> 00:16:05,840
on, and on, and I just sit around and do nothing, and then I'm like, wow, and that works,

121
00:16:05,840 --> 00:16:12,320
yeah, it works so far, it works for me, and, you know, and then someone asked him, how

122
00:16:12,320 --> 00:16:21,400
old are you, and he said, 25, and they said, the joke sets him up to seem like he's

123
00:16:21,400 --> 00:16:25,280
very old, it's like this old guy who's bragging about how he's whole life, turns out

124
00:16:25,280 --> 00:16:32,600
he's a young guy who's, you know, looks very old because, anyway, it's just a silly joke,

125
00:16:32,600 --> 00:16:41,720
but, you know, there's studies on alcohol, what it does to the brain, as I understand

126
00:16:41,720 --> 00:16:50,880
it's not good for the brain, cause long-term brain damage, I mean, not severe brain damage,

127
00:16:50,880 --> 00:17:02,840
and it reduces your brain capacity, as does marijuana, according to this study, but more

128
00:17:02,840 --> 00:17:15,200
over happiness doesn't lead to happiness, you know, anybody can be, anybody can be happy

129
00:17:15,200 --> 00:17:24,680
when things are good, as long as they have the ability to enjoy, that's not how you measure

130
00:17:24,680 --> 00:17:30,280
someone's greatness, you don't measure someone's greatness by how good they are to

131
00:17:30,280 --> 00:17:37,200
enjoying pleasure, you measure someone's greatness at how good they are at, bearing with

132
00:17:37,200 --> 00:17:44,160
adversity, right, and then going gets tough, that's when you know the character of a person,

133
00:17:44,160 --> 00:17:47,520
you can't say, well, that guy sure knows how to have fun, well, it's not that hard

134
00:17:47,520 --> 00:17:52,040
and to have, okay, yes, it's true, some people don't know how to have fun and you can

135
00:17:52,040 --> 00:18:02,640
hardly do that, but, it's, it's much easier when you're in your comfort zone, so if someone

136
00:18:02,640 --> 00:18:08,400
is good at having fun, well, yes, I'm drinking 20 hours a day, that is impressive, all

137
00:18:08,400 --> 00:18:16,280
admit, it's a skill that you've developed, but let's see what happens, you know, as a result

138
00:18:16,280 --> 00:18:22,800
of developing that skill and that ability, let's see what happens when you're placed in

139
00:18:22,800 --> 00:18:33,040
the situation that is challenging to you, I mean, does this behavior increase your ability

140
00:18:33,040 --> 00:18:40,600
to deal with challenges, if suddenly the country that you're in becomes a fascist dictatorship,

141
00:18:40,600 --> 00:18:47,680
and they start to torture people and you are being tortured, how do you react?

142
00:18:47,680 --> 00:18:53,960
How do you bear with it?

143
00:18:53,960 --> 00:19:03,760
This, this story of this, this auditorium, there was a guy, one of the leaders, he was

144
00:19:03,760 --> 00:19:13,640
a musician, I think, and he was leading people in chanting the anthem, the national anthem

145
00:19:13,640 --> 00:19:21,360
or something, and so they called them down and they asked him to sing, and so he started

146
00:19:21,360 --> 00:19:30,320
singing and then they scrapped him to this table and they smashed his fingers, he was playing

147
00:19:30,320 --> 00:19:34,080
a guitar, they got him to play the guitar, and then they smashed his fingers or like cut

148
00:19:34,080 --> 00:19:38,240
them off, cut up his fingers or something like that, cut up his fingers and then smashed

149
00:19:38,240 --> 00:19:44,000
his hands to a bloody pulp, they did this, this happened apparently, and then they said

150
00:19:44,000 --> 00:19:53,760
now you now sing, now play your guitar, and so he stood up and he turned to the crowd,

151
00:19:53,760 --> 00:20:00,720
to the other prisoners in the stadium, and he let them, he let them in the anthem or something

152
00:20:00,720 --> 00:20:01,720
like that.

153
00:20:01,720 --> 00:20:06,760
It's an interesting story because of how he dealt with the adversity, I mean, yeah okay

154
00:20:06,760 --> 00:20:15,240
singing is not such a noble thing in our books, but the nobility of being able to deal

155
00:20:15,240 --> 00:20:23,840
with adversity, I mean, it's not saying what is it that leads to the ability to deal

156
00:20:23,840 --> 00:20:28,280
with adversity, but that's the question, what is it that leads to adversity?

157
00:20:28,280 --> 00:20:36,400
If you believe that the skill of being able to drink a lot helps you deal with adversity,

158
00:20:36,400 --> 00:20:45,640
that's one theory, but we can't say what the future is going to bring, and certainly

159
00:20:45,640 --> 00:20:53,680
in Cambodia, for example, before the Polpa massacre, people were living good, there were

160
00:20:53,680 --> 00:21:00,120
probably a lot of people were saying, oh look, life is good, everything's great, eat

161
00:21:00,120 --> 00:21:06,320
drink and be merry, that doesn't help you, I mean, I would argue it probably doesn't

162
00:21:06,320 --> 00:21:15,880
do much to prepare you for being tortured, and it's an extreme example, but examples

163
00:21:15,880 --> 00:21:24,560
abound, suppose you get cancer, and it's the ability to enjoy pleasure going to help

164
00:21:24,560 --> 00:21:35,200
you with that, so if you begin to practice meditation, you start to see the benefits in

165
00:21:35,200 --> 00:21:39,400
this way, you can reflect on this as a sort of benefit, wow, you know, I'm really able

166
00:21:39,400 --> 00:21:43,720
to deal with things that I wouldn't have been able to deal with before, that's really

167
00:21:43,720 --> 00:21:52,160
something quite obvious in the meditation practice, you learn, you see your reactions, and

168
00:21:52,160 --> 00:22:01,920
you learn how to avoid those, how to navigate and experience carefully without reacting,

169
00:22:01,920 --> 00:22:10,720
to be careful, to be conscientious, anyway, that's a bit of a tangent, but my point being

170
00:22:10,720 --> 00:22:21,760
there, in many ways, we are a family, you know, meditators, Buddhists, in many ways

171
00:22:21,760 --> 00:22:29,920
we are the ones that care for each other, we are the ones that, it's each and every

172
00:22:29,920 --> 00:22:39,400
one of us, Anya Manyam, for each other, who is a support to each other, who we would

173
00:22:39,400 --> 00:22:45,600
do well to look to for support, I mean, I can't look to my family so much for support,

174
00:22:45,600 --> 00:22:52,920
I can't even visit with them after time because they're drinking alcohol or, you know,

175
00:22:52,920 --> 00:22:57,040
but I know when I'm with meditators, I, you know, when I was in New York at the monastery,

176
00:22:57,040 --> 00:23:03,760
I knew it was all good, I knew I was around people who I could trust, who I could count

177
00:23:03,760 --> 00:23:23,480
on, who I could relate to, who I could sit with, be with, and not feel the impetus to

178
00:23:23,480 --> 00:23:32,360
leave, feeling awkward, like everyone around me is drunk, and then around me is lost,

179
00:23:32,360 --> 00:23:41,200
and it's on a different path, putting ourselves above anyone, but on different paths.

180
00:23:41,200 --> 00:23:48,920
So, you know, on that note, I really like to thank everyone for all of the support, you know,

181
00:23:48,920 --> 00:23:58,080
we have this house, and I'm able to live because people are supporting me, and that's

182
00:23:58,080 --> 00:24:06,560
many of you, and I'd like to thank you, and also appreciate the meditation, the amount

183
00:24:06,560 --> 00:24:11,600
of meditation, and the number of people involved with this community, or practicing

184
00:24:11,600 --> 00:24:17,840
meditations, it's another wonderful support because it means there's all these people

185
00:24:17,840 --> 00:24:24,560
we can relate to, we have each other, and we can relate to each other, and we can talk

186
00:24:24,560 --> 00:24:32,360
with each other, and encourage each other, and push each other further on the path, push

187
00:24:32,360 --> 00:24:37,720
each other to meditate and to cultivate good things.

188
00:24:37,720 --> 00:24:45,840
So, we care for each other, and true, true, this is true family, we don't depend on our

189
00:24:45,840 --> 00:24:55,920
father and mother, we don't depend on our brothers and sisters, in spiritual matters, right?

190
00:24:55,920 --> 00:25:01,560
In important matters, many ways we can't depend on them because they just end up leading

191
00:25:01,560 --> 00:25:10,560
us down the wrong path because, you know, whether they're understanding is, yes, we would

192
00:25:10,560 --> 00:25:16,760
say wrong, but at the very least, their understanding is different from ours, and so it would

193
00:25:16,760 --> 00:25:23,520
be conflicting for us to depend too much upon our family, right?

194
00:25:23,520 --> 00:25:25,600
We have a problem, what do we do?

195
00:25:25,600 --> 00:25:31,440
We'll buy a big bag of pot and smoke it all, so they were talking about them and saying

196
00:25:31,440 --> 00:25:35,200
it, we've got to find a way, and they're talking about how to find a pot, you know,

197
00:25:35,200 --> 00:25:42,120
or they can get a big bag of, it's my family, you know?

198
00:25:42,120 --> 00:25:55,880
So this quote is somewhat apropos to my life, I mean in that part of it is, is anybody

199
00:25:55,880 --> 00:26:06,480
here, anybody have any questions, we'll take text questions, if you got them, 24 viewers

200
00:26:06,480 --> 00:26:13,280
on YouTube, hello everyone, somebody's watching and we've got a bunch of people on our

201
00:26:13,280 --> 00:26:41,080
meditation page, we've got the usual suspects, so Rob and are we going to do some, are we

202
00:26:41,080 --> 00:26:45,680
going to give people the opportunity, not obviously pushing for it, but give people

203
00:26:45,680 --> 00:27:01,760
the opportunity if they want to offer a robe to that genton, like join us?

204
00:27:01,760 --> 00:27:09,120
Because there was, I think Aurora, I think someone anyway, someone was interested in offering

205
00:27:09,120 --> 00:27:15,600
a robe, set a robe to that genton, and because it's, it does, I think it's quite easy

206
00:27:15,600 --> 00:27:22,760
for us to add sets of robes to our order, but it's just a matter of giving people the

207
00:27:22,760 --> 00:27:28,120
opportunity if they want them, they're not coercion, it's not like trying to push people

208
00:27:28,120 --> 00:27:32,120
to say this is a good thing, it's something really would be keen to do that, I think,

209
00:27:32,120 --> 00:27:41,120
what I understand, I mean, I don't like to put pressure and make people think, like,

210
00:27:41,120 --> 00:27:48,120
oh, that's what we're all about is the sitting donations, we're not, but we are about

211
00:27:48,120 --> 00:27:57,600
giving, to give, so if you want to give with us, you're welcome to give with us, yeah,

212
00:27:57,600 --> 00:28:06,920
we could, you know, who knows, if people want, see the thing is, there was some people

213
00:28:06,920 --> 00:28:11,840
who supported me to get a ticket, but there was lots of support, so I thought, well,

214
00:28:11,840 --> 00:28:18,560
we can get a gift for a genton as well, is that, you know, important, and we've thought

215
00:28:18,560 --> 00:28:25,280
about what kind of gifts and I thought robes, robes are the best, because robes symbolize

216
00:28:25,280 --> 00:28:32,120
monasticism, so if you are interested in the idea, becoming a monk, or if you want to be

217
00:28:32,120 --> 00:28:41,520
close to the monastic life, the aesthetic, or non-scene life, robes are a really good symbolic

218
00:28:41,520 --> 00:28:52,240
gift, and practical gift as well, because you're actually helping to close for just monks.

219
00:28:52,240 --> 00:29:01,480
Awesome, so there's still time, we can set up a campaign, let me make it clear that this,

220
00:29:01,480 --> 00:29:09,120
well, I mean, it is pretty clear, we just have to, we don't want it to sound like we're

221
00:29:09,120 --> 00:29:16,400
looking for money or something, yeah, I think we should make a big deal out of it, I don't

222
00:29:16,400 --> 00:29:27,200
want, we've got, we had a couple of other crowns, funding things, this is just if there

223
00:29:27,200 --> 00:29:32,520
was someone out there, we're doing it already, so we can always have a robe, a set of robes

224
00:29:32,520 --> 00:29:45,080
if someone happened to want to do that, okay, but let me know how, what, if it can work

225
00:29:45,080 --> 00:30:04,120
something out there, awesome, thank you, tomorrow, okay, so let's talk about, you can come

226
00:30:04,120 --> 00:30:11,960
on the hangout tomorrow then, talk about it, maybe, anyway, we'll say good night for tonight

227
00:30:11,960 --> 00:30:17,160
then, no questions, have a good night everyone, thanks for tuning in, wishing you all

228
00:30:17,160 --> 00:30:41,680
the best.

