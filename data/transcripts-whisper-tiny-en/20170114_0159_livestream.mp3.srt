1
00:00:00,000 --> 00:00:24,400
Okay, good evening everyone, welcome to our live broadcast, broadcasting live in second

2
00:00:24,400 --> 00:00:36,400
life, live on our website via audio and recording for upload to YouTube and of course live

3
00:00:36,400 --> 00:00:47,160
here in Hamilton to a live studio audience. We're overfull, we now have seven people staying

4
00:00:47,160 --> 00:00:54,800
in this house. No, do we? We have six people right now and tonight we'll have a seventh

5
00:00:54,800 --> 00:01:07,600
supposed to be coming. We're overfull, just can't turn people away, just can't keep people

6
00:01:07,600 --> 00:01:17,120
they're breaking down the doors to get in. That's how passionate people become. Religion

7
00:01:17,120 --> 00:01:22,480
religion is such a powerful thing. No, the word religion has such a bad reputation

8
00:01:22,480 --> 00:01:29,960
or such a specific reputation that makes a lot of people uneasy when they hear the word.

9
00:01:29,960 --> 00:01:36,720
So I think it's important to reform the word. Religion means taking things seriously.

10
00:01:36,720 --> 00:01:47,600
So tonight's talk is about someone who, not actually so much about him hopefully, but it

11
00:01:47,600 --> 00:01:54,680
involves a discussion with someone who took Buddhism quite seriously, took the Buddha's

12
00:01:54,680 --> 00:02:02,560
teachings quite seriously and he stands out as someone who is remarkable in terms of taking

13
00:02:02,560 --> 00:02:15,000
the Buddha's teaching seriously. He was designated by the Buddha as foremost of the

14
00:02:15,000 --> 00:02:21,840
Buddha's disciples who went forth, left the home life out of faith, out of confidence.

15
00:02:21,840 --> 00:02:33,020
So who had a religious sense of religiosity. His name was Ratapala, you know, the story of

16
00:02:33,020 --> 00:02:46,360
Ratapala. It comes in the minigm minikaya, so it's 82. It's called the Ratapala, so the story

17
00:02:46,360 --> 00:02:52,840
goes, Ratapala heard the Buddha's teaching and realized that it was quite difficult for

18
00:02:52,840 --> 00:02:58,840
him to practice it while living at home. He thought to himself, wow, the sort of teaching

19
00:02:58,840 --> 00:03:04,840
isn't the kind of thing you can do or surrounded by sensuality and caught up by daily affairs

20
00:03:04,840 --> 00:03:12,120
of the quote unquote real world. It's not the real world, but what people would call

21
00:03:12,120 --> 00:03:20,120
the real world. The ordinary mundane, contrived artificial world of society that we've put

22
00:03:20,120 --> 00:03:30,600
together to help us achieve our sensual, our goals of sensual pleasure. Not so useful for

23
00:03:30,600 --> 00:03:40,440
becoming enlightened, too busy, too caught up in defilement. So you decided he wanted to go

24
00:03:40,440 --> 00:03:48,680
forth. He wanted to become a monk, but he asked the Buddha to ordain him and the Buddhists

25
00:03:48,680 --> 00:03:54,840
asked, you know, do you have your parents permission? And he said, no, I don't have my parents

26
00:03:54,840 --> 00:03:59,400
permission. But he said, well, I don't ordain people who don't have their parents permission.

27
00:03:59,400 --> 00:04:06,840
And so he had to go back to get his parents permission to make a long story short. His parents

28
00:04:06,840 --> 00:04:15,080
didn't give permission and he laid down on the floor. He pleaded and begged with them,

29
00:04:15,080 --> 00:04:20,680
but eventually he laid down on the floor and said, I'm not going to eat or drink or do anything.

30
00:04:20,680 --> 00:04:29,880
I'm not going to get up off this floor until you allow me to ordain. And so they waited

31
00:04:29,880 --> 00:04:34,680
him out for a while, but then he made good on his threat and he just lay there and started

32
00:04:34,680 --> 00:04:40,120
starving to death. And they called his friends to try and convince him. And his friends came

33
00:04:40,120 --> 00:04:45,640
over, talked to him, and then went and talked to his parents and said, look, he's pretty serious

34
00:04:45,640 --> 00:04:49,160
about this. How about this? You know, if you let him ordain, at least you'll get to see him alive,

35
00:04:50,600 --> 00:04:55,880
you can go and visit him. But if you don't let him ordain, he's going to die,

36
00:04:55,880 --> 00:05:03,320
then you won't see him. Then he'll be gone. So let him ordain. And the story goes on and on.

37
00:05:03,320 --> 00:05:08,440
But tonight, I didn't want to talk so much about his story as interesting and as inspiring as it is.

38
00:05:11,240 --> 00:05:17,400
Later on in his life, he was living, I can't remember where he was living, but he went somewhere

39
00:05:17,400 --> 00:05:31,240
and met with a king. And King asks him, I can see if I can find a corabi. Yeah, okay, I was going to

40
00:05:31,240 --> 00:05:38,440
say corabi was the name of this king or the name of the place where the king lived. I think

41
00:05:38,440 --> 00:05:50,360
it was actually the name of the place. And the king came to see him and he said to Ratapali,

42
00:05:50,360 --> 00:05:56,280
he said, you know, I know people who leave the home life and go to the forest to do their

43
00:05:56,280 --> 00:06:02,040
religious thing and they do it for one of four reasons. And they do it because they've

44
00:06:05,080 --> 00:06:11,480
they've got an old or they do it because they've gotten sick. They do it because they've

45
00:06:12,360 --> 00:06:16,360
lost wealth or they do it because they've lost their relatives.

46
00:06:16,360 --> 00:06:32,840
So someone who who is old can no longer work and is feeble and can't find any sort of solace

47
00:06:32,840 --> 00:06:42,120
or status or work or activity in the world of young people and so they go off into the forest

48
00:06:42,120 --> 00:06:47,480
and become a religious person. This actually happens even in Buddhism. Old people become monks

49
00:06:47,480 --> 00:06:53,240
and it's somewhat troublesome because they often get sick and that's the second one is people

50
00:06:53,240 --> 00:06:57,240
get sick and then they want to go off and to the forest while there's not much they could do in

51
00:06:57,240 --> 00:07:03,000
India at the time. Nowadays we just put them in old age homes and forget about them, but the same

52
00:07:03,000 --> 00:07:15,080
sort of thing. They didn't have all the age some of these sent them off into the forest to live or to die.

53
00:07:15,080 --> 00:07:20,360
Or someone who loses wealth and become impoverished and have to become a beggar and have to live

54
00:07:20,360 --> 00:07:29,160
under a tree or in a ditch or something. Also come and then also happens now.

55
00:07:29,160 --> 00:07:38,600
Or through lots of relatives, relatives who maybe took care of the person or maybe just out of

56
00:07:38,600 --> 00:07:46,680
grief they would realize the suffering of life and leave the world and he said, but you,

57
00:07:46,680 --> 00:07:50,280
you're young, you're healthy, you come from a good family, they're all still alive.

58
00:07:50,280 --> 00:07:58,520
What was it that caused you to leave home? Why didn't you leave behind the world?

59
00:07:59,240 --> 00:08:03,400
It's a good question, you know, with all the wonderful things that we have in the world,

60
00:08:03,400 --> 00:08:07,560
why do we leave it? Why do we come here? Why do you come here to torture yourself?

61
00:08:10,840 --> 00:08:14,600
Even just to meditate, people will go to the monastery's temples, churches

62
00:08:14,600 --> 00:08:24,520
when something bad happens. So it's remarkable to see someone who has their whole life ahead of them.

63
00:08:24,520 --> 00:08:33,160
Couldn't be capable of so much in the world and this is why Rata Pala's parents wouldn't allow

64
00:08:33,160 --> 00:08:39,880
him. They were quite upset that he wanted to throw away his wonderful future, all the wonderful

65
00:08:39,880 --> 00:08:47,640
good things he can do, right? I wanted to do this useless thing on becoming a reckless,

66
00:08:47,640 --> 00:08:52,520
becoming the Buddhist monk, how useless they thought.

67
00:08:56,200 --> 00:09:03,400
And Rata Pala's Rata Pala's answer is well known in the Buddhist dispensation. He claims that it

68
00:09:03,400 --> 00:09:08,200
comes from the Buddha. Now I'm not sure if we've actually had somewhere where the Buddha said this,

69
00:09:08,200 --> 00:09:13,960
we very well may have, but Rata Pala's the one who's known for giving it. He says it comes from the Buddha.

70
00:09:15,160 --> 00:09:23,960
He said there are these four dhammodesa. And today's a comes from this, this means to to indicate

71
00:09:25,400 --> 00:09:34,360
this, that's the same. So the things the Buddha pointed out, four dhammodesa, four indicators,

72
00:09:34,360 --> 00:09:44,440
dhamma indicators. And when these were pointed out to me, I realized that I had to leave,

73
00:09:44,440 --> 00:09:53,640
I had to do something. I think these are quite useful, not just for encouraging people to

74
00:09:53,640 --> 00:09:58,760
take up meditation or take up Buddhism, but also to encourage us in our practice and to remind us

75
00:09:58,760 --> 00:10:04,840
why we're here when we get discouraged and to sort of focus our attention on what's really

76
00:10:04,840 --> 00:10:15,160
important, clarify and keep us on track. So the first is Upaneetiloko Adhu.

77
00:10:18,920 --> 00:10:22,360
The world is uncertain, the world is unstable.

78
00:10:22,360 --> 00:10:32,360
So all these good things, I mean, just the fact that those losses exist, you don't need to

79
00:10:32,360 --> 00:10:37,640
experience the loss to know that that's a part of life, to know that it's a danger and to start to

80
00:10:37,640 --> 00:10:48,360
question, what am I doing here wasting my time when I certainly won't be prepared for for old age

81
00:10:48,360 --> 00:10:59,160
for sickness, for loss, for death, and the uncertainty of life, all the things that we hold dear

82
00:11:01,480 --> 00:11:07,640
even our own selves, even our own families, our own situation, everything about us

83
00:11:07,640 --> 00:11:17,640
is unpredictable, chaotic.

84
00:11:23,320 --> 00:11:28,840
That's the first reason why one would go off and become a monk or become a long off in practice

85
00:11:28,840 --> 00:11:40,440
meditation. The second one is Adhanoloko Anabhisarul.

86
00:11:43,960 --> 00:11:50,760
This world has no refuge, no master or guardian protector.

87
00:11:53,960 --> 00:11:56,680
And the king questions among all these, he says, what do you mean?

88
00:11:56,680 --> 00:12:03,640
It seems very constant. Here I'm the king and life is very constant for me.

89
00:12:05,080 --> 00:12:11,000
They say, well, do you remember when you were young and you could shoot an arrow or ride a horse?

90
00:12:12,040 --> 00:12:19,000
Can you do that now? In permanent, you see? Well, the age comes to us all seeing this and knowing

91
00:12:19,000 --> 00:12:24,360
that this is the case. Yeah, go forth. And then he says, well, what about this? No protection.

92
00:12:24,360 --> 00:12:28,920
What do you mean? I've got elephants, I've got warriors, I've got lots and lots of refuge

93
00:12:28,920 --> 00:12:33,080
and protectors. He said, well, what if you get sick? Suppose you get really sick?

94
00:12:33,080 --> 00:12:38,840
Are your elephants going to protect you from the sickness? Is your family, are your warriors

95
00:12:38,840 --> 00:12:44,760
they're going to stand around you and keep the sickness away? No, no, there's nobody who can protect you.

96
00:12:46,920 --> 00:12:52,600
Right? And when we're young, this is one of the big shadowings of youth, the realization that

97
00:12:52,600 --> 00:13:01,080
our parents can't protect us from suffering. This shock of growing up, realization that suffering is

98
00:13:09,080 --> 00:13:13,000
that we are vulnerable to suffering and that we're protected from it. You're not sheltered

99
00:13:13,000 --> 00:13:23,320
from it. There's nothing that can shelter us from it. And number three,

100
00:13:26,520 --> 00:13:30,840
Asako Loco, Sabang Bahaya, Gamaniandi.

101
00:13:30,840 --> 00:13:40,680
This world is not an owner. It has no ownership. It's nothing of its own.

102
00:13:43,320 --> 00:13:50,760
It goes having abandoned everything. The kings are, well, what do you mean? I can go with everything.

103
00:13:50,760 --> 00:14:01,480
Full jewels and we have so much that is ours. We've got our cars and our houses and our families

104
00:14:01,480 --> 00:14:09,400
and our friends and our iPhones and our computers and everything. We've got so much it's all ours.

105
00:14:09,400 --> 00:14:16,600
We go with it. No, you don't go with any of it. He says, do you know whether you're going to

106
00:14:16,600 --> 00:14:21,400
be king in the next life? Do you know whether you're going to be human in the next life to be

107
00:14:21,400 --> 00:14:29,880
even enjoy the riches? Maybe he'll be born a hungry ghost guarding your own treasure or maybe he'll

108
00:14:29,880 --> 00:14:46,760
be born an ant or a termite living in the wind wall of the palace. Not just in the next life.

109
00:14:48,120 --> 00:14:52,920
All of our possessions, right? We can't hold on to them. We can't say that. Let this be mine forever.

110
00:14:52,920 --> 00:15:04,680
Let this car, this house, or this iPhone. Let it be mine. Even our friends and family and even

111
00:15:04,680 --> 00:15:11,640
our own bodies don't really belong to us. We're borrowing them and let them borrowing them because

112
00:15:11,640 --> 00:15:19,240
we don't know when they're going to be taken back. It's like we've stolen them and we're just

113
00:15:19,240 --> 00:15:25,480
waiting for the police to come and arrest us. Wait until we get caught. It's like we've stolen,

114
00:15:25,480 --> 00:15:31,320
we're living on stolen time. We've got to get as much pleasure and use out of it. Joy

115
00:15:31,320 --> 00:15:36,600
ride in this body that we have and then death comes and catches us and wham.

116
00:15:39,320 --> 00:15:45,720
Takes it away. We don't know when that's going to be. We're just trying to run away

117
00:15:45,720 --> 00:15:55,960
from the land. We're on the land. We're on the run. Death is on our heels. It's a good analogy.

118
00:15:55,960 --> 00:16:15,240
The number four, Uno, Loco, Atito, Tanha, Dazzo. Una. Una is the same. That's where the word

119
00:16:15,240 --> 00:16:26,840
want, want and own from the same place. Una means wanting. This world is wanting is lacking.

120
00:16:26,840 --> 00:16:42,440
Attita, unsatisfied. Tanha, Tanha, Dazzo. The slave, Dazzo. A slave of Tanha, of craving.

121
00:16:47,720 --> 00:16:55,320
You know what do you mean? I can satisfy myself any time. I get whatever I want. I'm always satisfied.

122
00:16:55,320 --> 00:17:04,680
I eat and then I'm satisfied. But it says, well, what if you heard about another kingdom

123
00:17:04,680 --> 00:17:11,560
that was weak, but had lots of resources and they told you that it was ready for the taking.

124
00:17:11,560 --> 00:17:19,560
What would you do? Oh, I'd conquer it. Another one. Another one. Never have enough.

125
00:17:19,560 --> 00:17:28,920
You know what I said? It could rain gold. It could rain gold or precious jewels

126
00:17:31,000 --> 00:17:39,080
and the shower of them would never be enough for a human, for one being. It's a nature of craving.

127
00:17:39,080 --> 00:17:51,640
The very nature of craving is that it's habitual. It's ever increasing. It's self-perpetuating

128
00:17:51,640 --> 00:17:57,400
or self-enforcing or it feeds itself. It becomes stronger the more you engage in it.

129
00:17:59,560 --> 00:18:03,080
It's a very scary thing. We're always unsatisfied.

130
00:18:03,080 --> 00:18:13,800
Remember that we can never be satisfied. Seeing this many people leave home

131
00:18:15,480 --> 00:18:21,560
when they realize how unsatisfied they are. How they can eat and eat and all their gaining is

132
00:18:21,560 --> 00:18:27,080
more and more craving. It's one of the big things you realize here is how attached we are

133
00:18:27,080 --> 00:18:35,640
to diversion, how unable we are just to be with ourselves, just to have the patience

134
00:18:36,920 --> 00:18:43,720
to be, you know, such a simple thing. I think, well, that's easy. But isn't it ridiculous how we can't

135
00:18:43,720 --> 00:18:58,040
just be, we can't be ourselves. We have to do, make, create, get. It's quite surprising

136
00:18:58,040 --> 00:19:04,440
because I think we generally have this idea that we can just be that we always are, right?

137
00:19:04,440 --> 00:19:14,280
But when we are, we're not good at arming. We're not good at being. We have to do. We have to get

138
00:19:15,240 --> 00:19:22,120
so much craving and aversion. We have so many in-built reactions to everything.

139
00:19:24,520 --> 00:19:31,640
So realizing this, realizing all four of these things, this is what leads one to go forth

140
00:19:31,640 --> 00:19:37,880
to become a monk, to practice meditation. This is the encouragement, the reminder to all of us

141
00:19:42,040 --> 00:19:47,160
that we're here for a reason. We're here to figure this out. We're going to solve this problem.

142
00:19:48,120 --> 00:19:55,960
The problem of impermanence, that the world, any goal we might attain, any object we might

143
00:19:55,960 --> 00:20:05,480
acquire, anything we might become will always be unstable, uncertain, impermanent.

144
00:20:09,720 --> 00:20:13,640
Well, there's nothing that can keep us from the vicissitudes of life. There's no protection,

145
00:20:13,640 --> 00:20:28,520
there's no refuge. There's nowhere we can go, nothing we can do to avoid the vicissitudes of life.

146
00:20:30,600 --> 00:20:39,080
That all of our possessions can't protect us, all of our friends and family. We leave them all

147
00:20:39,080 --> 00:20:45,000
behind, that all the things we hold dear and all the things we crave and love and cling to.

148
00:20:46,120 --> 00:20:50,520
They're not ours. They'll be gone. We'll be gone from them soon enough.

149
00:20:54,760 --> 00:20:59,720
And finally, that craving, we're a slaves of craving, slaves of desire.

150
00:21:01,480 --> 00:21:05,960
We are not masters of our desire. Our desires are not our possession that we can control.

151
00:21:05,960 --> 00:21:12,280
They are the master. We are the slave. Very scary reality. And the scariest, these scary realities

152
00:21:13,880 --> 00:21:19,080
encourage us because there is something that can protect us. There is a refuge, and that's the

153
00:21:19,080 --> 00:21:27,320
dhamma, that's the truth, reality, seeing things as they are. Once we learn how to, just be

154
00:21:27,320 --> 00:21:40,440
and we become content, we become stable, we become invincible. There's no coming or going,

155
00:21:41,800 --> 00:21:43,400
and we fear ourselves from suffering.

156
00:21:47,720 --> 00:21:51,960
So there you go. There's the dhamma for tonight, fine teaching,

157
00:21:51,960 --> 00:21:59,400
and then on by Radhapala, Buddhist monk with the greatest faith and the greatest confidence

158
00:22:00,600 --> 00:22:05,960
in going forth. No doubt in his mind. He would have done anything, he would have died

159
00:22:06,520 --> 00:22:12,280
just to do what we're doing. It's a good example of

160
00:22:12,280 --> 00:22:19,640
I have a religiosity and how wonderful and powerful it can be.

161
00:22:19,640 --> 00:22:43,480
So there you go. Thank you for tuning in. Have a good night.

162
00:22:49,640 --> 00:23:00,600
You have nowhere to go.

163
00:23:05,400 --> 00:23:09,560
I don't know if there are any questions. Does anyone have any questions?

164
00:23:10,600 --> 00:23:15,320
You can hang out. It's not that big of a deal. They're just questions. They're mostly about

165
00:23:15,320 --> 00:23:21,240
meditation. We don't have to, if you want to, go down. I'll just meditate then hearing.

166
00:23:24,760 --> 00:23:33,240
I don't think there's any questions. So overflow, we have one meditator staying in the living

167
00:23:33,240 --> 00:23:53,080
room, which is where we do these talks. Okay, well if there are no questions, then wishing you all a good

168
00:23:53,080 --> 00:24:09,000
night. See you all next time.

