1
00:00:00,000 --> 00:00:14,820
In Buddhism, we recognize three things about the universe, and this is a set of three characteristics

2
00:00:14,820 --> 00:00:30,040
that are true of everything in the universe. Whether you be born a king or you be born an angel

3
00:00:30,040 --> 00:00:49,420
or you be born a god, that everything in the world is impermanent, changing all the time.

4
00:00:49,420 --> 00:01:08,880
It's unstable. It's finite. So the idea of stability is really

5
00:01:08,880 --> 00:01:22,200
contrary to the very nature of our existence. We're not static beings that exist outside

6
00:01:22,200 --> 00:01:32,280
of space and time outside of the universe that we find ourselves in. Our existence is

7
00:01:32,280 --> 00:01:44,700
defined by our relationships with the objects of our experience, which is changing all the time,

8
00:01:44,700 --> 00:02:14,320
so we change with it. The biggest part of what it is that creates this

9
00:02:14,320 --> 00:02:28,840
change is really our own fault. We're looking for stability. We're looking for certainty

10
00:02:28,840 --> 00:02:53,860
and consistency. But the way we act, the way we set about living our lives is contrary

11
00:02:53,860 --> 00:03:08,220
to a life of stability, tranquility and peace.

12
00:03:08,220 --> 00:03:18,100
And quite often, this is because we don't realize our intrinsic relationship with the

13
00:03:18,100 --> 00:03:29,060
world around us, our inherent relationship that we have no existence apart from the universe,

14
00:03:29,060 --> 00:03:46,980
apart from our experience. We're dependent on it. And the universe depends on us.

15
00:03:46,980 --> 00:03:57,300
Our world depends on our actions and changes accordingly. So we tend to do and say and think

16
00:03:57,300 --> 00:04:09,460
and engage in activities, thinking of them to be harmless, failing to see the effect

17
00:04:09,460 --> 00:04:19,980
that they have on our lives, failing to see that they change who we are, they change our

18
00:04:19,980 --> 00:04:34,660
universe. We engage in addictive practices, obsessive practices, we engage in the pursuit

19
00:04:34,660 --> 00:04:44,060
of sensuality, not seeing the danger in it, not seeing the change that it's bringing

20
00:04:44,060 --> 00:05:00,060
about on our minds and on our lives. We don't see the stress, we don't see the tension

21
00:05:00,060 --> 00:05:08,940
that we're creating in our minds. We don't see that our minds are out of balance. We don't

22
00:05:08,940 --> 00:05:22,060
see that we're creating the very impermanence that we're seeking to avoid, the uncertainty,

23
00:05:22,060 --> 00:05:32,460
and stability. We're winding ourselves up with our attachments. We want something more

24
00:05:32,460 --> 00:05:41,820
and more and when we get it, we're not satisfied. We need more. And when we don't get it,

25
00:05:41,820 --> 00:05:58,620
we're far more unsatisfied to the point of desperation when we don't get what we want.

26
00:05:58,620 --> 00:06:10,660
So really there's no one to blame for one's uncertainty beyond oneself. And we've all

27
00:06:10,660 --> 00:06:24,460
brought ourselves to this point. The real key in Buddhism is whether you suffer or not

28
00:06:24,460 --> 00:06:28,860
as a result of it. So the second thing the Buddha said that is inherent in the world around

29
00:06:28,860 --> 00:06:51,060
us is suffering. That. The experiences that we have, the instability that exists in the

30
00:06:51,060 --> 00:07:00,780
universe means that it's not going to make you happy. It doesn't mean that you have

31
00:07:00,780 --> 00:07:07,620
to suffer because you live in the universe. It means that all of the things that you could

32
00:07:07,620 --> 00:07:14,980
cling to are not worth clinging to. All of the things that you could seek to stabilize,

33
00:07:14,980 --> 00:07:22,660
to control, to chase after are only a cause for suffering. All of the things that you can

34
00:07:22,660 --> 00:07:33,580
do, all of the goals that you can have in regards to the attainment of worldly stability

35
00:07:33,580 --> 00:07:48,300
and happiness are only a cause for stress and suffering. So a person can live in great

36
00:07:48,300 --> 00:08:04,460
instability and still be at peace with themselves. A person can live in a world that is chaotic,

37
00:08:04,460 --> 00:08:13,380
one could live with great suffering, physical suffering or suffering in their lives without

38
00:08:13,380 --> 00:08:30,740
actually suffering from it because they don't cling there too. So the important defining

39
00:08:30,740 --> 00:08:36,220
factors whether or not we cling, whether or not we identify with the reality around us.

40
00:08:36,220 --> 00:08:45,060
And this is really the problem is that we've built up so many identities and can't

41
00:08:45,060 --> 00:08:51,580
we play roles? No, role playing. A lot like we do here in Second Life. People come here

42
00:08:51,580 --> 00:08:56,940
and pretend to be this and pretend to be that of me. We really do this in our quote-unquote

43
00:08:56,940 --> 00:09:10,020
real life as well. The roles that we play, the people that we believe to believe ourselves

44
00:09:10,020 --> 00:09:21,100
to be are our roles in the same way that our characters here are roles that we've created

45
00:09:21,100 --> 00:09:38,140
who we are. We've built it up. And so the pursuit of this or that goal or this or that

46
00:09:38,140 --> 00:09:44,660
path is caught up in this role. It's caught up in who we are. I'm going to do this. I'm

47
00:09:44,660 --> 00:09:51,860
going to get that, going to school, getting a job, getting married and so on. We have all

48
00:09:51,860 --> 00:10:04,460
of these fantasies really. All of religion, all of the ideas of God and all of the political

49
00:10:04,460 --> 00:10:15,220
structures and philosophical ideas, all of our laws of physics and chemistry and biology.

50
00:10:15,220 --> 00:10:23,740
It's all just fiction, really. It's all just fantasy. It's where we create all of this.

51
00:10:23,740 --> 00:10:36,220
Our plans for the future, all of our work, it's all constructs we created all. So Buddhism

52
00:10:36,220 --> 00:10:50,460
helps us to go the other direction, helps us to deconstruct, to break apart, and to give

53
00:10:50,460 --> 00:11:03,540
up and let go and rise above this fiction, this contrived state. So I know normally I

54
00:11:03,540 --> 00:11:10,340
give these long talks and people sometimes like them and sometimes don't like them. But

55
00:11:10,340 --> 00:11:17,540
I think more important than that is to actually do some practice. We come here. Let's

56
00:11:17,540 --> 00:11:27,420
not live in fantasy. Let's not engage in this sham. We're here to learn something. We're

57
00:11:27,420 --> 00:11:36,500
here to develop ourselves. Let's take this opportunity. We can do some practice together.

58
00:11:36,500 --> 00:11:44,340
So I'd like to lead you all now. I'm not going to take long, but I'd like to lead you all

59
00:11:44,340 --> 00:11:49,940
in a specific meditation practice and that's the meditation to simply see things as they

60
00:11:49,940 --> 00:12:00,580
are. We have this development of wisdom in Buddhism. Buddhism is for the development

61
00:12:00,580 --> 00:12:07,020
of wisdom, understanding, right view and all this. There's so much reading and studying

62
00:12:07,020 --> 00:12:15,900
you can do in Buddhism. And it often becomes, it comes to seem like such a deep and profound

63
00:12:15,900 --> 00:12:21,420
and intellectual subject that we lose sight of. What we mean by wisdom is really understanding

64
00:12:21,420 --> 00:12:27,940
this. It's not understanding that or anything else. It's understanding this. The this that

65
00:12:27,940 --> 00:12:36,700
is us. Who are you? What are you doing right now? You're in your room. You're somewhere

66
00:12:36,700 --> 00:12:42,460
looking at the computer, but think about you're a little bit broader about your life. The

67
00:12:42,460 --> 00:12:50,780
challenges that are facing you, the problems in your life and so on. Some of the things

68
00:12:50,780 --> 00:12:55,540
that perhaps you're trying to get away from by coming to second life. This is what you're

69
00:12:55,540 --> 00:13:00,780
going to learn. Learn about this is what we're going to study. Rather than run away from

70
00:13:00,780 --> 00:13:07,220
it, rather than avoid it. This is the object of our meditation. When we talk about understanding

71
00:13:07,220 --> 00:13:11,420
reality, we're not talking about understanding a different reality. We're talking about

72
00:13:11,420 --> 00:13:26,060
understanding what it is that we experience our lives. Something very simple and mundane.

73
00:13:26,060 --> 00:13:34,020
The big secret about the mundane is that it's only mundane because we don't like it. It's

74
00:13:34,020 --> 00:13:49,220
only mundane because it's not pleasant for us. It's not acceptable to us. We can't bear

75
00:13:49,220 --> 00:14:07,700
with it. We can't live with it in comfort and peace. So once we learn to live with

76
00:14:07,700 --> 00:14:14,500
ourselves, to live with our own lives in peace and harmony, it no longer becomes

77
00:14:14,500 --> 00:14:38,060
mundane. It no longer is mundane. It becomes incredibly satisfying. Quite interesting,

78
00:14:38,060 --> 00:14:44,300
at the same least, that our lives this mundane life that we have becomes profound, it becomes

79
00:14:44,300 --> 00:14:52,420
spiritual. We become enlightened. It's something that most of us would probably find unfathomable

80
00:14:52,420 --> 00:15:02,580
that somehow we, this ordinary mundane person who has ordinary mundane worries and cares,

81
00:15:02,580 --> 00:15:11,360
could become someone special and wonderful. But this is what we do. We take this mundane

82
00:15:11,360 --> 00:15:22,140
life and return it into a enlightened state. So this is what we're dealing with. It's

83
00:15:22,140 --> 00:15:27,380
something quite profound. On the one hand, quite profound. On the other hand, quite simple.

84
00:15:27,380 --> 00:15:31,900
It's not going to, we're not going to leave our bodies. We're going to study our bodies

85
00:15:31,900 --> 00:15:40,820
and our minds. So you can close your eyes. It's no need to look outside because the eyes

86
00:15:40,820 --> 00:15:52,420
are doors and when we look, we jump. We jump from ourselves to some illusion of where

87
00:15:52,420 --> 00:16:02,260
we are in the world outside of ourselves. Let's look at what's real and that's our experience.

88
00:16:02,260 --> 00:16:09,340
So here we are experiencing. What are we experiencing? Well, the first thing at the very

89
00:16:09,340 --> 00:16:14,020
beginning, it's going to be quite dull and uninteresting though. We're experiencing the

90
00:16:14,020 --> 00:16:27,340
chair. We're experiencing the sounds in the room, the sound of my voice. We're experiencing

91
00:16:27,340 --> 00:16:37,620
the heat or the cold. We're experiencing the feelings in the body. We're experiencing

92
00:16:37,620 --> 00:16:51,860
the thoughts and emotions in the mind. The wandering mind. These sorts of talks and practices

93
00:16:51,860 --> 00:16:59,740
many people have a hard time coping with. And so I see a lot of people when I start

94
00:16:59,740 --> 00:17:06,620
to talk and get up and go. And I'm never really bothered by that because I know it's hard

95
00:17:06,620 --> 00:17:15,740
to listen to what I have to say. I'm not funny. I'm not interesting. I'm not amusing.

96
00:17:15,740 --> 00:17:20,140
But I really honestly think that I have something good to say useful because it's been

97
00:17:20,140 --> 00:17:28,820
useful for me. Buddhism is not something that's easy for people to take. You have to be

98
00:17:28,820 --> 00:17:38,060
strong. I guess the mind is wandering. The mind is it's easy to say these things but our

99
00:17:38,060 --> 00:17:47,260
mind is not stable if we're not well trained. It can be very difficult to look at this

100
00:17:47,260 --> 00:17:52,940
reality which we've built up all sorts of illusions about that it's boring and mundane

101
00:17:52,940 --> 00:18:03,380
and useless. It's much more useful for us to deal with illusion and these mental constructs

102
00:18:03,380 --> 00:18:08,820
of who we are and so on. But now here we're going to look at something mundane. We're

103
00:18:08,820 --> 00:18:17,860
going to look at what most people refuse to look at. We're going to come to understand

104
00:18:17,860 --> 00:18:23,540
it. We're going to come to understand reality. So the method that many of you are aware

105
00:18:23,540 --> 00:18:32,540
of that I recommend because it's worked well for me is this mantra, the technique of

106
00:18:32,540 --> 00:18:41,860
reminding ourselves of the portion of our experience that's real. And that's the very

107
00:18:41,860 --> 00:18:48,540
basic elements of it to seeing the hearing, the smelling, the tasting, the feeling, the thinking,

108
00:18:48,540 --> 00:18:55,540
the physical and mental elements of experience. Or you experience the chair. You experience

109
00:18:55,540 --> 00:19:02,820
it through feeling. You experience my voice. You experience it through hearing. You experience

110
00:19:02,820 --> 00:19:15,340
pain. You experience happiness. You experience thoughts. Emotions. These experiences

111
00:19:15,340 --> 00:19:28,540
are all that's real and the experience arises in seasons. It comes and goes. So we're

112
00:19:28,540 --> 00:19:38,380
going to try to keep our minds with this reality so that we can see it clearer so that

113
00:19:38,380 --> 00:19:45,180
we can come to understand it. And we can get it through this thick skull of ours, this

114
00:19:45,180 --> 00:19:53,860
clouded mind of ours. What is the nature of reality? That it's not worth clinging

115
00:19:53,860 --> 00:20:12,180
to. And we should stop clinging. We should stop obsessing. Running away from. And we

116
00:20:12,180 --> 00:20:22,580
come to just experience to just be. We come to open our minds up to the whole of experience.

117
00:20:22,580 --> 00:20:28,700
So this mantra is simply reminding ourselves of reality so that we can stay with it. For instance,

118
00:20:28,700 --> 00:20:33,980
when we're sitting in the chair and we feel the sitting, we feel our back and our bottoms

119
00:20:33,980 --> 00:20:40,780
and our legs, touching the chair, touching the floor, we say to ourselves, sitting. Just

120
00:20:40,780 --> 00:20:45,260
remind yourself that you're sitting. It's a very basic meditation practice, but it's

121
00:20:45,260 --> 00:20:52,500
quite useful. When you remind yourself in this way that you're sitting, you're mind is

122
00:20:52,500 --> 00:20:57,900
suddenly freed up from all of the illusion and delusion, all of the games and fantasies

123
00:20:57,900 --> 00:21:07,060
that we play and entertain. Suddenly we're here, we're now, we're real. We're just sitting.

124
00:21:07,060 --> 00:21:27,060
So we say to ourselves, sitting, sitting, sitting, right? It's a simple thing. So simple

125
00:21:27,060 --> 00:21:31,220
that we would never think to do it, not only the Buddha ever thought that we should do

126
00:21:31,220 --> 00:21:40,900
such a thing. No one ever wants to learn about Maria about Monday and something we think

127
00:21:40,900 --> 00:21:51,740
that we know all about. It's really the only thing that we know nothing about. It's the only

128
00:21:51,740 --> 00:21:57,140
part of our experience that we know next to nothing about. We're so engaged in learning

129
00:21:57,140 --> 00:22:10,980
about things external to ourselves that we neglect ourselves. Another common one that

130
00:22:10,980 --> 00:22:18,740
we recommend is to watch the breath. This is one that the Buddha recommended quite often.

131
00:22:18,740 --> 00:22:25,060
Watch the breath where it enters the body at the nose or watch it where it finishes in

132
00:22:25,060 --> 00:22:33,180
the body and the stomach. And the stomach rises. You can note the rising motion. It's

133
00:22:33,180 --> 00:22:48,060
clear. Recognizing it for what it is, rise. It falls fall. You say to yourself, rising

134
00:22:48,060 --> 00:22:56,820
falling in the mind. This is a replacement for the distracted thinking. It's a replacement

135
00:22:56,820 --> 00:23:02,420
for the many different kinds of thoughts that arise. Replacing with a single thought,

136
00:23:02,420 --> 00:23:11,020
why is thought, if you thought, is aware of the object for what it is? It doesn't go beyond

137
00:23:11,020 --> 00:23:34,020
the bounds of the true nature of the object. So when we do this, we're getting closer

138
00:23:34,020 --> 00:23:39,460
to the essence of our experience. And in the beginning, we're going to see the chaotic

139
00:23:39,460 --> 00:23:50,220
nature of the experience that the way we live our lives in an unmeditative way. It has

140
00:23:50,220 --> 00:24:06,220
wreaked havoc on our inner selves. It's really messed us up to put it one way. We can

141
00:24:06,220 --> 00:24:12,300
see that our experience, even if the stomach rising and falling, even if this talk, I'm

142
00:24:12,300 --> 00:24:18,020
sitting here giving a talk and you're all listening. Your experience of it is tempered

143
00:24:18,020 --> 00:24:27,020
by your emotions. Maybe I tell you to do rising, falling in it. Suddenly all this judgment

144
00:24:27,020 --> 00:24:32,340
arises. So it's not good. It's not good in this way or I'm not good or so. Or this is

145
00:24:32,340 --> 00:24:41,420
boring or so. Even though you might want to listen, you're unable to focus because of the

146
00:24:41,420 --> 00:24:57,260
way we, the way our minds run here and there. But we're not judging. The important

147
00:24:57,260 --> 00:25:03,900
thing is to see and to understand. We're not trying to get angry or feel bad about the

148
00:25:03,900 --> 00:25:11,620
way we've lived our life. We're just trying to understand what are the results of what

149
00:25:11,620 --> 00:25:20,580
are the consequences of our actions. Naturally, we're trying to understand where scientists

150
00:25:20,580 --> 00:25:30,220
are. We're not religious zealots. We're trying to hold up a doctrine. We're trying

151
00:25:30,220 --> 00:25:42,220
to see things as they are. So even when we feel angry or frustrated or bored or sad, we'll

152
00:25:42,220 --> 00:25:48,940
use the same mantra when we feel angry. We'll just want to learn about the anger. We don't

153
00:25:48,940 --> 00:25:58,420
judge it. If we judge it, then we learn nothing. If we judge it without investigating and

154
00:25:58,420 --> 00:26:04,580
observing and coming to true conclusions, then we've learned nothing. We're failing the

155
00:26:04,580 --> 00:26:10,660
Buddhist teaching. But when we look at it, when we examine it and when we come to understand

156
00:26:10,660 --> 00:26:19,300
it, understand the whole nature of our good and bad emotions, good and bad in the sense

157
00:26:19,300 --> 00:26:30,220
of bringing us good or bad results, when we see the results they're bringing, then there's

158
00:26:30,220 --> 00:26:36,660
no intellectual, exercise necessary. Because obviously when we see that something hurts

159
00:26:36,660 --> 00:26:43,340
us, we won't do it. We won't continue it. We won't have any interest in it. When we see

160
00:26:43,340 --> 00:27:05,340
that something brings us benefit, we will definitely increase it. And eventually we will come

161
00:27:05,340 --> 00:27:12,660
to see that nothing that arises, no experience that we have seeing, hearing, spelling,

162
00:27:12,660 --> 00:27:22,660
decision is inherently good or bad. What's good or bad are our reactions to them and they're

163
00:27:22,660 --> 00:27:27,820
good and bad because those reactions are going to determine whether we meet with happiness

164
00:27:27,820 --> 00:27:37,540
or we meet with suffering. If we cling, we're clinging to something that's impermanent

165
00:27:37,540 --> 00:28:06,460
when it changes, we'll meet with suffering. So we simply watch the objects for what they

166
00:28:06,460 --> 00:28:10,500
are. We come to see them for what they are. We come to see that watching and seeing

167
00:28:10,500 --> 00:28:17,420
and simply knowing the objects of experience where they are is the correct way, is free

168
00:28:17,420 --> 00:28:23,460
from all of the stress and suffering of clinging. When we feel pain in the body, we simply

169
00:28:23,460 --> 00:28:33,900
know that we're feeling pain. We remind ourselves this is pain. Pain. Pain. Pain.

170
00:28:33,900 --> 00:28:45,380
Hearing ourselves that indeed it's neither good nor bad, and indeed the best way, the greatest,

171
00:28:45,380 --> 00:28:52,140
the most appropriate manner of relating to this is with simple knowledge and wisdom.

172
00:28:52,140 --> 00:29:00,940
That when we do see the inherent nature of the pain, we are free from all of the stress

173
00:29:00,940 --> 00:29:07,020
that comes from judging and aversion towards the pain. That in fact the suffering doesn't

174
00:29:07,020 --> 00:29:29,780
come from the pain itself, it comes from our aversion towards you. So let's just do this

175
00:29:29,780 --> 00:29:37,940
here. Let's try and practice this. We'll see the inside of ourselves there are good things

176
00:29:37,940 --> 00:29:44,420
and there are bad things. But mostly there are just neutral things. Those things that are

177
00:29:44,420 --> 00:29:52,780
only bad because we're judging them are only good because we're clinging to them. Try

178
00:29:52,780 --> 00:30:05,380
to watch your body, watch the physical, watch what is most clear. And when your mind begins

179
00:30:05,380 --> 00:30:16,260
to create, begins to wander and chase after experiences and fantasies and so on, catch those

180
00:30:16,260 --> 00:30:22,700
up as well and watch them, know them from what they are when you're thinking, know that

181
00:30:22,700 --> 00:30:32,900
you're thinking. When you have emotions, judging, know that you're judging, liking, liking.

182
00:30:32,900 --> 00:30:40,540
Just remind yourself who this is what it is, angry, disliking, boring, bored. We're not

183
00:30:40,540 --> 00:30:43,780
trying to stop these emotions, we're trying to see them from what they are, learn about

184
00:30:43,780 --> 00:30:53,740
them, understand them. It doesn't take long at all to understand these things. A few moments

185
00:30:53,740 --> 00:31:00,100
of acknowledging is generally all that it takes to see that you're hurting yourself and

186
00:31:00,100 --> 00:31:19,420
give it up.

187
00:31:19,420 --> 00:31:42,460
Let me know what you've heard of.

188
00:31:42,460 --> 00:31:45,460
.

189
00:31:45,460 --> 00:31:48,460
..

190
00:31:48,460 --> 00:31:51,460
..

191
00:31:51,460 --> 00:31:54,460
..

192
00:31:54,460 --> 00:31:56,460
..

193
00:31:56,460 --> 00:31:59,460
...

194
00:31:59,460 --> 00:32:02,460
...

195
00:32:02,460 --> 00:32:05,460
...

196
00:32:05,460 --> 00:32:08,460
...

197
00:32:08,460 --> 00:32:10,460
..

198
00:32:10,460 --> 00:32:12,460
..

199
00:32:12,460 --> 00:32:26,460
...

200
00:32:26,460 --> 00:32:31,460
..

201
00:32:31,460 --> 00:32:38,460
..

202
00:32:38,460 --> 00:32:39,460
..

203
00:32:39,460 --> 00:32:40,460
..

204
00:32:40,460 --> 00:32:42,460
You

205
00:33:10,460 --> 00:33:12,460
You

206
00:33:40,460 --> 00:33:42,460
You

207
00:34:10,460 --> 00:34:12,460
You

208
00:34:40,460 --> 00:34:42,460
You

209
00:35:10,460 --> 00:35:12,460
You

210
00:35:40,460 --> 00:35:42,460
You

211
00:36:10,460 --> 00:36:12,460
You

212
00:36:40,460 --> 00:36:42,460
You

213
00:37:10,460 --> 00:37:12,460
I

214
00:37:12,460 --> 00:37:14,460
At the very least from

215
00:37:42,460 --> 00:37:44,460
You

216
00:38:12,460 --> 00:38:14,460
You

217
00:38:21,460 --> 00:38:23,460
At the very least from

218
00:38:23,460 --> 00:38:25,460
You're short

219
00:38:27,460 --> 00:38:29,460
Look at

220
00:38:29,460 --> 00:38:31,460
Who we are

221
00:38:31,460 --> 00:38:33,460
We should be able to see some

222
00:38:35,460 --> 00:38:37,460
Of how our mind works

223
00:38:37,460 --> 00:38:43,460
No, it's obviously going to take time to

224
00:38:43,460 --> 00:38:49,460
Really say that we've gained these noble qualities of wisdom inside

225
00:38:49,460 --> 00:38:54,460
But in the meantime we've we've done something

226
00:38:54,460 --> 00:39:00,460
Noble and worthwhile that that may not seem like it at first in fact for many people at the beginning

227
00:39:00,460 --> 00:39:04,460
It's the most discouraging

228
00:39:04,460 --> 00:39:10,460
No, you start meditating and it feels like you're just not made for this your mind is just not

229
00:39:11,460 --> 00:39:13,460
Settling down

230
00:39:13,460 --> 00:39:15,460
How do I keep my mind still?

231
00:39:15,460 --> 00:39:17,460
How do I stop my mind from wondering?

232
00:39:18,460 --> 00:39:24,460
But that's really the the revelation that comes from meditation is that you can't that you don't

233
00:39:25,460 --> 00:39:27,460
That you've

234
00:39:27,460 --> 00:39:38,460
Made your bed now you lie in it. We've developed this state of distractedness

235
00:39:38,460 --> 00:39:46,460
We've developed these states of anger of version of creative wanting of attachment and desire

236
00:39:48,460 --> 00:39:50,460
And we're powerless to

237
00:39:50,460 --> 00:39:57,460
To avoid them now we can do is avoid repeating the same mistake in the future

238
00:39:58,460 --> 00:40:00,460
So that eventually

239
00:40:01,460 --> 00:40:07,460
Lessons and lessons and lessons and in the future we we have none of this chaos

240
00:40:08,460 --> 00:40:11,460
That we can eventually be free from the chaos

241
00:40:11,460 --> 00:40:17,460
We can't

242
00:40:18,460 --> 00:40:23,460
Somehow turn the chaos off we can only change the direction we're headed

243
00:40:26,460 --> 00:40:28,460
So that we create no more of this

244
00:40:34,460 --> 00:40:39,460
When we start to meditate the most important skill that we're going to learn is acceptance

245
00:40:39,460 --> 00:40:44,460
Accepting the things that we can't change accepting when we've done things wrong

246
00:40:44,460 --> 00:40:46,460
We have the mistakes that we've made

247
00:40:47,460 --> 00:40:49,460
Accepting our faults

248
00:40:50,460 --> 00:40:52,460
And learning to work them out

249
00:40:53,460 --> 00:40:58,460
Rather than avoiding them and denying them and trying to turn them off

250
00:40:58,460 --> 00:41:00,460
Cut them off and pretend they don't exist

251
00:41:00,460 --> 00:41:09,460
Learning to accept that they're there except that we've developed this

252
00:41:10,460 --> 00:41:11,460
We've created this

253
00:41:12,460 --> 00:41:14,460
By the way we live our lives

254
00:41:16,460 --> 00:41:20,460
So I hope that's been of some use and thank you for coming

255
00:41:21,460 --> 00:41:23,460
Maybe someday I'll have a

256
00:41:23,460 --> 00:41:25,460
A

257
00:41:28,460 --> 00:41:33,460
Steady scheduler and second life and people will be able to know when I'm giving talks in advance

258
00:41:34,460 --> 00:41:41,460
But you can always keep up with me on Facebook that seems to be where I'm going to start posting where I've been posting

259
00:41:42,460 --> 00:41:49,460
My second life talks where my dates when I'll be giving a talk

260
00:41:49,460 --> 00:41:51,460
I

261
00:41:52,460 --> 00:41:54,460
Otherwise, thank you all for coming if you have any questions

262
00:41:54,460 --> 00:41:58,460
I'm happy to take them and I hope that you're all able to put this into practice

263
00:41:59,460 --> 00:42:05,460
And progress on the path to the realization of the truth of life

264
00:42:07,460 --> 00:42:14,460
And I hope that you're all able to find true peace happiness and freedom from suffering

265
00:42:14,460 --> 00:42:18,460
Thank you all for coming. Have a good day.

