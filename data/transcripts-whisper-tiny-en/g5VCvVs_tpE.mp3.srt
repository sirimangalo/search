1
00:00:00,000 --> 00:00:08,680
What should one do if the mind continues to resist being tamed if I feel like no progress

2
00:00:08,680 --> 00:00:17,520
is being made?

3
00:00:17,520 --> 00:00:19,800
Continue meditating.

4
00:00:19,800 --> 00:00:30,360
Just be patient and don't get frustrated, but if you do, then note frustration and

5
00:00:30,360 --> 00:00:34,200
note everything that is going on.

6
00:00:34,200 --> 00:00:39,320
You are living out there in this world with all the temptations and all these distractions

7
00:00:39,320 --> 00:00:40,560
and so on.

8
00:00:40,560 --> 00:00:51,600
So it is when you are not meditating regularly or when you are too much out there in the

9
00:00:51,600 --> 00:01:03,600
world and have not learned yet to seclude your mind, then you might not be able to feel

10
00:01:03,600 --> 00:01:11,480
that the mind starts to be tamed or that it has been tamed already.

11
00:01:11,480 --> 00:01:18,520
Sometimes you don't even notice because it is progressing little by little and you don't

12
00:01:18,520 --> 00:01:25,440
even notice that your mind changed and that your mind has become more silent and more

13
00:01:25,440 --> 00:01:32,200
still and that you can direct your thoughts much better than you could before.

14
00:01:32,200 --> 00:01:36,520
But you are not aware of it.

15
00:01:36,520 --> 00:01:59,960
It can of course be that you can't tame your mind really and then the most efficient solution

16
00:01:59,960 --> 00:02:12,960
were to come on retreat, to get a good foundation and then go home and continue practicing.

17
00:02:12,960 --> 00:02:19,840
And in the meantime, as I said earlier, note everything that is arising and then you

18
00:02:19,840 --> 00:02:24,520
might be able to see the changes in your mind.

19
00:02:24,520 --> 00:02:37,480
But I would like to add one thing, not quite as advocate, but kind of smart me because

20
00:02:37,480 --> 00:02:42,760
okay, what should I do if the mind continues to resist being tamed?

21
00:02:42,760 --> 00:02:45,320
I want to ask you questions in return.

22
00:02:45,320 --> 00:02:48,880
How does it feel to have your mind resist being tamed?

23
00:02:48,880 --> 00:02:51,560
Does that make you want to cling to the mind?

24
00:02:51,560 --> 00:02:55,000
Does that make you feel like the mind is a good thing?

25
00:02:55,000 --> 00:02:57,640
And as Paul and Yanis does that make you frustrated?

26
00:02:57,640 --> 00:03:01,320
And of course that would be one answer to acknowledge the frustration.

27
00:03:01,320 --> 00:03:10,360
But the real question is the real question is what does that mean about the mind?

28
00:03:10,360 --> 00:03:16,000
Does that in that case, is the mind stable or unstable?

29
00:03:16,000 --> 00:03:18,320
Is it satisfying or unsatisfying?

30
00:03:18,320 --> 00:03:22,120
Is it controllable or uncontrollable?

31
00:03:22,120 --> 00:03:27,360
Because we read about the very core of the Buddha's teaching, some people don't do

32
00:03:27,360 --> 00:03:33,960
them, but I think this is a person who has been involved, who's quite, his name got the

33
00:03:33,960 --> 00:03:37,800
Buddhist now, so you know something about Buddhism.

34
00:03:37,800 --> 00:03:45,000
But when we actually experience these things, impermanence, suffering non-self, we still fall

35
00:03:45,000 --> 00:03:50,560
back into the trap of thinking something's wrong.

36
00:03:50,560 --> 00:03:55,360
Because the truth is that that in and of itself is an incredible progress to see that

37
00:03:55,360 --> 00:03:57,280
the mind cannot be tamed.

38
00:03:57,280 --> 00:04:02,560
To see that you can't tie the mind down and expect it to sit still and say, sit down

39
00:04:02,560 --> 00:04:09,560
and expect your mind to, at any point, be under your control.

40
00:04:09,560 --> 00:04:18,720
Because this is the danger involved in being complacent about tranquility meditation.

41
00:04:18,720 --> 00:04:22,000
Because even if you enter into tranquility meditation, which the Buddha recommended, which

42
00:04:22,000 --> 00:04:32,360
is a great thing, if you stop there, if you become complacent about it, you'll still fall

43
00:04:32,360 --> 00:04:37,200
into a great amount of suffering and stress when it changes.

44
00:04:37,200 --> 00:04:42,680
When it disappears, you will still be liable to fall back into a state of impermanence, or

45
00:04:42,680 --> 00:04:44,880
a state of instability of mind.

46
00:04:44,880 --> 00:04:46,880
The mind can still become distracted again.

47
00:04:46,880 --> 00:04:51,760
We have this story that I've told you used to tell all the time about this monk who had

48
00:04:51,760 --> 00:04:57,840
magical powers he could fly through the air, and he was looked after by this king because

49
00:04:57,840 --> 00:05:00,760
the king of greatest steam for him.

50
00:05:00,760 --> 00:05:06,080
And one day the king, one time the king had to go away to fight the war.

51
00:05:06,080 --> 00:05:10,280
And so he gave his queen to be in charge of looking after the monk, and so the monk

52
00:05:10,280 --> 00:05:13,760
would come on on the ground and whereas before the king would give him food, now the

53
00:05:13,760 --> 00:05:18,440
queen would give him food, and he would fly in through the palace window, stand there and

54
00:05:18,440 --> 00:05:19,440
take the food.

55
00:05:19,440 --> 00:05:29,160
And at one time the queen had taken a shower or a bath, and she was lying with her robe,

56
00:05:29,160 --> 00:05:33,160
and she said, I'll lie down and wait for the monk to come, and she fell asleep.

57
00:05:33,160 --> 00:05:38,560
And when the monk came in, he made a noise touching, coming in through the window or touching

58
00:05:38,560 --> 00:05:42,200
the floor or else he cleared his throat or something, and she woke up and stood up and

59
00:05:42,200 --> 00:05:48,320
the robe fell off, and he saw this beautiful queen and totally naked.

60
00:05:48,320 --> 00:05:56,760
And he totally lost his, as they say in Thai, he lost his tranquility and lost his magical

61
00:05:56,760 --> 00:05:57,760
powers as well.

62
00:05:57,760 --> 00:06:01,400
So he turned around when he took the food, he turned around to fly out through the window

63
00:06:01,400 --> 00:06:07,160
and he found that he couldn't fly anywhere, and he's like up, up, and not away.

64
00:06:07,160 --> 00:06:11,720
And so he walked out the door and totally lost everything and the story goes on eventually

65
00:06:11,720 --> 00:06:16,480
the king, the queen explained to the king how to solve the problem.

66
00:06:16,480 --> 00:06:21,080
She said, give me to him, give me to him as his wife.

67
00:06:21,080 --> 00:06:24,560
And so the king called this ascetic and said, okay, you can have my queen and take her

68
00:06:24,560 --> 00:06:25,720
away.

69
00:06:25,720 --> 00:06:32,240
And they went away and the queen really made a fool out of him, said, okay, so we'll

70
00:06:32,240 --> 00:06:33,240
have to go and get a house.

71
00:06:33,240 --> 00:06:36,880
The king has given us this house to live in, and she said, oh, go back to the palace

72
00:06:36,880 --> 00:06:39,880
and fetch me my slippers.

73
00:06:39,880 --> 00:06:42,320
So he went back all the way back to the palace, let me see.

74
00:06:42,320 --> 00:06:46,160
Now go and fetch me my purse or go and fetch me this pillow or go and fetch me that and

75
00:06:46,160 --> 00:06:52,360
go and fetch me this back and forth and back and forth, he totally lost his mind.

76
00:06:52,360 --> 00:06:55,920
And so when he came back finally, she grabbed him by the beard because he was a bearded

77
00:06:55,920 --> 00:07:04,120
ascetic and said, look at you, you're an ascetic and she woke him up and made him realize

78
00:07:04,120 --> 00:07:07,160
what is fooling him, he was being.

79
00:07:07,160 --> 00:07:15,760
But the only point, the reason why I tell that story often is to remind us how there is

80
00:07:15,760 --> 00:07:17,520
no stability of mind.

81
00:07:17,520 --> 00:07:22,720
You can't find even this powerful state that allows you to read people's minds fly

82
00:07:22,720 --> 00:07:24,440
through the air and so on.

83
00:07:24,440 --> 00:07:26,600
It's not permanent.

84
00:07:26,600 --> 00:07:34,680
And so in a roundabout way that I think that is an important part of the answer that your

85
00:07:34,680 --> 00:07:42,960
realization, your insight into the nature of the mind as being difficult to tame is an

86
00:07:42,960 --> 00:07:44,960
important, we pass into insight.

87
00:07:44,960 --> 00:07:51,440
And then the last question we can ask is what then does that do to your mind or how does

88
00:07:51,440 --> 00:07:55,760
that make you react or act in the future?

89
00:07:55,760 --> 00:07:59,760
Will that make you more keen to control the mind in the future?

90
00:07:59,760 --> 00:08:06,760
Are you going to be more keen to, okay, let's try again and tie the mind down.

91
00:08:06,760 --> 00:08:11,040
No, you're more likely to be disinterested.

92
00:08:11,040 --> 00:08:17,960
You'd be does that would have said to be dispassioned about it or even a verse to getting

93
00:08:17,960 --> 00:08:19,600
involved with it at all.

94
00:08:19,600 --> 00:08:22,520
And you'll find that when the mind does go crazy, when the mind starts thinking about

95
00:08:22,520 --> 00:08:29,640
many things, you'll be far less inclined to follow after it or even to become upset about

96
00:08:29,640 --> 00:08:39,920
it, to try to control the mind because the controlling of the mind is out of ignorance.

97
00:08:39,920 --> 00:08:44,400
We try to control our lives, we try to control ourselves, we try to control our minds

98
00:08:44,400 --> 00:08:48,520
out of ignorance of the fact that these things are impermanent unsatisfying and uncontrollable.

99
00:08:48,520 --> 00:08:54,280
So I would say this is a common question that people ask because of a misconception of

100
00:08:54,280 --> 00:09:01,560
or a really an inability to see our own defilements and our own nature of our own mind.

101
00:09:01,560 --> 00:09:04,520
So what you're seeing is actually what you should be seeing.

102
00:09:04,520 --> 00:09:09,360
The problem is at that moment you don't realize it and so there arises frustration and

103
00:09:09,360 --> 00:09:14,520
on top of that there arises worry and doubt about whether you're practicing correctly or

104
00:09:14,520 --> 00:09:19,280
whether this practice is actually any good or so on.

105
00:09:19,280 --> 00:09:26,240
When in fact that's really why the Buddha was so clear about this and so explicit about

106
00:09:26,240 --> 00:09:35,600
the fact about these qualities of reality as being impermanent unsatisfying and controllable.

107
00:09:35,600 --> 00:09:43,040
So despite the constant fight I become more mindful about a few more things than I was.

108
00:09:43,040 --> 00:09:47,720
Even gain outside of meditation by paying more attention to what I'm doing or thinking.

109
00:09:47,720 --> 00:09:49,160
Which is another important point.

110
00:09:49,160 --> 00:09:54,160
Another thing that you'll realize is that you have to be careful because whereas you can't

111
00:09:54,160 --> 00:09:59,440
control the mind you do create the mind through habits and at every moment you have a

112
00:09:59,440 --> 00:10:03,800
choice you can make and if you choose the same thing again and again and again it will

113
00:10:03,800 --> 00:10:09,840
develop a habit in the mind and that's what you've done that's why the mind is out of control.

114
00:10:09,840 --> 00:10:14,240
So instead of trying to force it to be tamed you have to change your habits.

115
00:10:14,240 --> 00:10:16,560
You have to start acting in a different way.

116
00:10:16,560 --> 00:10:23,680
One that doesn't develop that and definitely 100% you have gained from that experience.

117
00:10:23,680 --> 00:10:27,680
There's no question that with someone who can come up with this sort of question has already

118
00:10:27,680 --> 00:10:28,680
gained something.

119
00:10:28,680 --> 00:10:32,240
Has already gained a benefit and it's helped them to let go and it's helped them to say

120
00:10:32,240 --> 00:10:34,000
I don't need to control things.

121
00:10:34,000 --> 00:11:03,840
What I need to do is be careful and not let my mind develop in that way again.

