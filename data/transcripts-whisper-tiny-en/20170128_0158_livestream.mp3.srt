1
00:00:00,000 --> 00:00:21,360
Welcome to our evening session, we all gather together locally or on the internet to

2
00:00:21,360 --> 00:00:36,840
listen to the Dhamma to discuss the Dhamma. Today I got word of a word just yesterday. Last

3
00:00:36,840 --> 00:00:46,400
night I think about a talk that was being held at McMaster University. Something about Buddhism

4
00:00:46,400 --> 00:00:54,120
in Myanmar. When I got there it was a pleasant surprise they were talking about our tradition.

5
00:00:54,120 --> 00:01:04,680
Our meditation tradition that was rather involved but much of it was based around the practice

6
00:01:04,680 --> 00:01:16,400
of mindfulness of insight based on the four foundations of mindfulness. I wrote you in a scholarly

7
00:01:16,400 --> 00:01:23,080
setting. It's quite incredible to people coming from University of Toronto and the speaker

8
00:01:23,080 --> 00:01:36,560
came from University of Virginia I think. It's quite encouraging and the talk was lectured

9
00:01:36,560 --> 00:01:41,480
I don't know what you call the speaker. It's quite encouraging as well. He talked a lot

10
00:01:41,480 --> 00:01:50,680
about how mindfulness has evolved, the practice has evolved as it's moved and over time.

11
00:01:50,680 --> 00:02:01,000
In the use of the word mindfulness it's become such a catch word. There are issues and questions

12
00:02:01,000 --> 00:02:22,640
of authenticity and keeping with the traditional teachings. The idea that the word mindfulness

13
00:02:22,640 --> 00:02:31,880
has come to mean something different over time and so as a result meditation practice has

14
00:02:31,880 --> 00:02:36,280
changed and people's ideas of what it means to practice the Buddha's teaching has changed.

15
00:02:36,280 --> 00:02:46,840
But nonetheless to hear people and to see a picture of Mahasi Sayada up on the board was quite

16
00:02:46,840 --> 00:02:56,760
heartwarming. The image is to hear people talk about him and to talk about the use of insight

17
00:02:56,760 --> 00:03:07,440
meditation, the practice of insight meditation to have a chance to talk about the discussion

18
00:03:07,440 --> 00:03:17,480
afterwards got onto the topic of what is mindfulness and one of the members of the audience was

19
00:03:17,480 --> 00:03:26,040
quite bold in his conception that mindfulness isn't good or bad. Mindfulness just fixes

20
00:03:26,040 --> 00:03:32,640
your attention on an object or something or reinforces. I'm not actually quite clear what

21
00:03:32,640 --> 00:03:38,920
it is. I don't think I quite agree with it but the question came up whether the idea was

22
00:03:38,920 --> 00:03:52,000
that a sniper, a killer, a soldier can be quite mindful in what they're doing and cat can

23
00:03:52,000 --> 00:03:59,360
be quite the cat idea was that it was quite focused and yet if you know anything about

24
00:03:59,360 --> 00:04:08,600
the Abhidhamma these are wholesome mind states. So we have this idea that concentration

25
00:04:08,600 --> 00:04:14,240
is wholesome, mindfulness is wholesome, confidence is wholesome. So what about these people

26
00:04:14,240 --> 00:04:21,240
who are confident, these evil people who are confident about their evil, people who

27
00:04:21,240 --> 00:04:37,080
are confident in scamming, manipulating in the pressing others. And so I had to give

28
00:04:37,080 --> 00:04:45,640
a bit of a correction I thought to explain something that's interesting to us because it's

29
00:04:45,640 --> 00:04:52,520
an interesting question of karma, what is karma? What is good karma and bad karma? How

30
00:04:52,520 --> 00:04:57,200
can you say something is good karma when bad karma, when there's so much, it's quite

31
00:04:57,200 --> 00:05:14,000
complicated. It actually sounds like some sort of magic, something very magical or spiritual.

32
00:05:14,000 --> 00:05:18,080
When you kill, it sounds nice actually when you kill, it's bad karma and bad things are

33
00:05:18,080 --> 00:05:22,680
going to happen to you, you're going to get retribution. And so it takes on this sort

34
00:05:22,680 --> 00:05:34,080
of mystical sort of air, this idea that there's some karmic power or force, but it's not

35
00:05:34,080 --> 00:05:41,200
exactly how it works. I mean karma is with much of the Buddhist teachings is really

36
00:05:41,200 --> 00:05:51,760
to be understood as a meditator, as someone practicing mindfulness. Your karma was this concept

37
00:05:51,760 --> 00:05:59,600
that was around before the Buddha became enlightened, but they had the wrong idea. So he was

38
00:05:59,600 --> 00:06:10,720
just explaining to them what is actually potent. And so what is actually potent is intention.

39
00:06:10,720 --> 00:06:14,480
And so at first you think, well, if you intend to kill, okay, then then killing is bad,

40
00:06:14,480 --> 00:06:27,360
but that's not even that simple. Because every moment, if you think about it, when

41
00:06:27,360 --> 00:06:32,080
you intend to kill someone, that's one intention, but the next moment you intend to pick

42
00:06:32,080 --> 00:06:39,280
up a knife, the next moment you intend to go and find the person you're going to kill.

43
00:06:39,280 --> 00:06:46,920
And every moment there's an intention, it's not even exactly intention, it's your volition

44
00:06:46,920 --> 00:07:00,520
or your bent, your inclination at that moment, your state of mind really. Jitana.

45
00:07:00,520 --> 00:07:05,520
And so I was saying, well, my understanding from the Abhidhamma, I didn't want to sort

46
00:07:05,520 --> 00:07:12,640
of preach as a meditation teacher, but I can preach to all of you. So I mean, as we

47
00:07:12,640 --> 00:07:20,880
understand it for as meditators, karma is every moment. So if you say a killer has mindfulness,

48
00:07:20,880 --> 00:07:24,480
well, they don't have mindfulness at the moment when they want to kill. At that moment,

49
00:07:24,480 --> 00:07:30,360
there's no mindfulness. They are, they are. I'm actually not sure. There's no wholesomeness.

50
00:07:30,360 --> 00:07:36,280
It's funny now, my Abhidhamma is not clear, but they were saying that mindfulness is

51
00:07:36,280 --> 00:07:40,720
also always wholesome according to the Abhidhamma that I'm not so sure. Look at this,

52
00:07:40,720 --> 00:07:48,000
I'm going to get myself in trouble. Doesn't really matter. We're not so interested in

53
00:07:48,000 --> 00:07:59,280
the technicalities not right now. So please forgive me for being imperfect, but the point

54
00:07:59,280 --> 00:08:04,800
being no action is, is, is hard to find an action that's entirely wholesome runnels and

55
00:08:04,800 --> 00:08:11,760
because wholesome really is referring to the individual that which brings you happiness,

56
00:08:11,760 --> 00:08:19,320
peace, goodness or well, bring good things to you. In other words, things that bring success.

57
00:08:19,320 --> 00:08:25,640
So a person's ability to, to successfully shoot someone else, it actually requires moments

58
00:08:25,640 --> 00:08:33,800
of wholesomeness, moments where you are, where you are focused and concentrated, present,

59
00:08:33,800 --> 00:08:40,480
confident. With the problem is there's so much overarching and so many moments of very

60
00:08:40,480 --> 00:08:51,280
strong ignorance, hatred, disregard for people for life, you see. And this is, this is

61
00:08:51,280 --> 00:08:55,680
true because we're not always thinking about killing a person, often we're thinking about

62
00:08:55,680 --> 00:09:04,640
lifting up our gun and so on. It'd be mostly unwholesome, but there will still be moments

63
00:09:04,640 --> 00:09:12,480
of wholesomeness. I mean, an easier example might be when you do something good, when a person,

64
00:09:12,480 --> 00:09:16,920
when you give a gift to someone, you think, well, that's wholesome, right? We say giving

65
00:09:16,920 --> 00:09:26,200
is good. I'm not exactly, not necessarily, not entirely. I remember when we used to give

66
00:09:26,200 --> 00:09:32,640
gifts to Adjendong, everyone is so worried. I have to do it just right. I started thinking

67
00:09:32,640 --> 00:09:37,800
you know, this worry is not wholesome. We're getting all this stuff, sitting there, we have

68
00:09:37,800 --> 00:09:42,720
to sit around and wait for Adjendong to arrive and he's always late. Mm-hmm, kind of grumpy.

69
00:09:42,720 --> 00:09:50,720
I think, well, that's not wholesome. It's much more complicated, you see? Meditation, maybe

70
00:09:50,720 --> 00:09:57,280
people say, yes, I'm going to meditate. Oh, good. There's a lot of unwholesomeness that comes

71
00:09:57,280 --> 00:10:02,000
up for meditation. You see, it's not something to be afraid of. It's not like, oh, well,

72
00:10:02,000 --> 00:10:08,160
I better not go and meditate or I'll get angry, right? It's not magic. It's not a demon

73
00:10:08,160 --> 00:10:17,280
that you have to be afraid of. It's science. It's really psychology. You know, there are

74
00:10:17,280 --> 00:10:22,720
things that tear your mind apart, that remove your mind's ability to function, that

75
00:10:22,720 --> 00:10:38,600
deal, debilitate, that decompass it, that make you incapacitate you, weaken the mind, cripple

76
00:10:38,600 --> 00:10:49,400
the mind, shrink the mind. But those are just moments and in meditation, we're very much

77
00:10:49,400 --> 00:10:59,480
interested in studying this. We're not to be afraid of it. It's like you're studying diseases

78
00:10:59,480 --> 00:11:06,280
or you're studying pain. We want to understand it. So we're willing to allow ourselves,

79
00:11:06,280 --> 00:11:11,400
give ourselves the room to be angry, give ourselves the space, to commit unwholesome

80
00:11:11,400 --> 00:11:16,880
karma even as meditate. So meditation isn't entirely wholesome. There's a lot of unwholesomeness,

81
00:11:16,880 --> 00:11:22,800
an insight meditation, getting angry. It's just why people are skeptical about it sometimes.

82
00:11:22,800 --> 00:11:27,360
And they think you have to do what the Buddha said. You know, the Buddha was inclined

83
00:11:27,360 --> 00:11:34,080
to have its meditators because it's much more wholesome to do some at the first focus

84
00:11:34,080 --> 00:11:39,520
your mind and separate your mind away from unwholesomeness. It takes a lot of time and a lot

85
00:11:39,520 --> 00:11:47,520
of effort and a lot of ideal conditions. So nowadays, it's much more difficult for people

86
00:11:47,520 --> 00:11:52,640
whose minds are consumed. And if so much ignorance that we're not even able to tell that

87
00:11:52,640 --> 00:11:59,280
these are bad, these things are bad. That's much quicker and simpler for us to just

88
00:11:59,280 --> 00:12:17,360
model through it and see directly, oh yeah, causing myself harm. So good and bad are

89
00:12:17,360 --> 00:12:23,200
momentary. It's quite scary actually if you think about it our whole day, every day throughout

90
00:12:23,200 --> 00:12:32,560
our day we're doing countless unwholesome deeds. We're doing lots of wholesome deeds as well,

91
00:12:32,560 --> 00:12:37,760
hopefully. But those deeds are just momentary. It means we have moments of wholesomeness

92
00:12:37,760 --> 00:12:45,920
and unwholesomeness. Karma is only really scary when it becomes a habit, a chin to come,

93
00:12:45,920 --> 00:12:51,600
it's called, or when it's extreme, when we let it get to the point where we do something,

94
00:12:51,600 --> 00:12:57,920
we have a moment that's just so unforgivable, like killing your parents or

95
00:13:02,160 --> 00:13:06,160
hurting a Buddha that kind of thing. Dropping a rock on the Buddha.

96
00:13:10,000 --> 00:13:14,000
But that's when it's only really, you know, karma is not something to be afraid of,

97
00:13:14,000 --> 00:13:20,000
something to understand, something to go beyond really. It's also not something it caught up in

98
00:13:20,000 --> 00:13:27,760
it. I think I'm going to do lots of good karma and that'll make me happy. We have to go beyond

99
00:13:27,760 --> 00:13:36,160
karma by studying it. And this is, so it is everywhere. So in everything we do when we give,

100
00:13:36,160 --> 00:13:40,800
there's good karma, there's bad karma, when we kill, there's mostly bad karma, but there's

101
00:13:40,800 --> 00:13:55,120
still going to be some moments potentially of wholesomeness. It could still be moments of wholesomeness.

102
00:13:55,120 --> 00:14:01,920
I don't want to get in trouble here with these radical ideas, but it seems to me that there would

103
00:14:01,920 --> 00:14:11,040
be moments. But the point is that it only a meditator can truly understand this when you're

104
00:14:11,040 --> 00:14:15,840
meditating, you're able to see that which causes you suffering, that which causes you peace.

105
00:14:17,200 --> 00:14:23,440
I remember when I was hunting when I was a teenager and I was sitting up in a tree

106
00:14:23,440 --> 00:14:32,560
with a crow with a bow, it was the most awful thing to think of now. And I had to sit for a

107
00:14:32,560 --> 00:14:41,680
long time and wait. And I ended up being quite a spiritual experience. This bird came and landed

108
00:14:41,680 --> 00:14:49,440
really right above my head, listening to the birds, floating by watching the sunset,

109
00:14:49,440 --> 00:14:54,080
watching it get dark, just sitting alone in the forest up in the tree,

110
00:14:55,440 --> 00:15:04,240
starts to get dark. Before I got dark, these deer came out into the clearing and I was,

111
00:15:04,240 --> 00:15:10,240
oh, going to get ready now is the time. But they turned out they were female and a female and

112
00:15:10,240 --> 00:15:16,640
young and you're not allowed to kill them, you need a special permit. So okay, I knew it was,

113
00:15:16,640 --> 00:15:22,240
and they came right up and they, they started eating from the tree, right? And I was sitting

114
00:15:22,240 --> 00:15:27,600
and then I was sitting here and I was looking into the eyes of this female deer and she looked at me

115
00:15:27,600 --> 00:15:34,320
and I looked at her and she was chewing on the, it was awful to think back that that was the

116
00:15:34,320 --> 00:15:40,800
sort of thing I was interested in. But just to point out, karma is not so simple.

117
00:15:40,800 --> 00:15:49,120
You think of Angulimala and all of his intent to kill and then he ended up becoming an

118
00:15:49,120 --> 00:15:55,280
Arhan, not because of all the killing, but because during that time, he came to understand suffering.

119
00:15:59,360 --> 00:16:03,200
You could even argue that through doing evil deeds, a person realizes how awful they are.

120
00:16:04,320 --> 00:16:08,720
Sometimes it takes doing an evil deed. Sometimes it takes someone to be addicted to drugs,

121
00:16:08,720 --> 00:16:13,040
in order to know that drugs are wrong. I don't want to give the impression that there's nothing

122
00:16:13,040 --> 00:16:19,360
wrong with evil and this is a good thing to do, but it's complicated. The Buddha said himself,

123
00:16:19,360 --> 00:16:26,240
understanding karma, only a Buddha can really understand it. But even the understanding that we

124
00:16:26,240 --> 00:16:33,520
have is that it's not so easy to understand. So I talk about this, I thought it would be good to

125
00:16:33,520 --> 00:16:38,400
talk about because I want to impress upon you that this is a good way of describing what we're

126
00:16:38,400 --> 00:16:44,240
doing in the practice. We're sorting it out. We're coming to see our habits, the karma that we've

127
00:16:44,240 --> 00:16:50,160
built up as habits, and realizing that some of it's not useful, not beneficial. Some of it's

128
00:16:50,160 --> 00:16:57,360
outright harmful. It's not even about asking whether this is a good deed or that is a good deed.

129
00:16:57,360 --> 00:17:04,080
It's about understanding mind states and seeing for ourselves what's a good mind state, what's a

130
00:17:04,080 --> 00:17:10,240
bad mind state? You can see clearly because this one leads to suffering, this one leads to happiness.

131
00:17:11,760 --> 00:17:17,680
It's undeniable and it doesn't change. You can ever get angry and then say, oh, that was fun.

132
00:17:19,120 --> 00:17:21,520
Angry and look at how much happiness that brought me.

133
00:17:22,960 --> 00:17:31,200
Greed will never make you satisfied. It doesn't have that. It has a very distinct nature

134
00:17:31,200 --> 00:17:37,920
that doesn't change. That's the law of karma. That's what we mean. It's a law. It's, I mean,

135
00:17:38,560 --> 00:17:40,720
might as well be a law because it doesn't ever change.

136
00:17:43,760 --> 00:17:51,360
Anyway, so in meditation, just being mindful, watching, and seeing, we see three things. We see the

137
00:17:51,360 --> 00:18:04,000
the the defilement. Let me see the karma. Let me see the result. This is the wheel of karma.

138
00:18:04,000 --> 00:18:14,960
You've ever heard, never heard of this. There's a kilesa, kama, vibak, or jetana, kilesa, kama, vibak.

139
00:18:14,960 --> 00:18:23,360
When you have the unwholesome mind states, that leads you to do things. It leads you to kill.

140
00:18:23,360 --> 00:18:26,720
It leads you to steal. It leads you even to think bad thoughts.

141
00:18:29,920 --> 00:18:35,120
Then there's the result. We see in meditation how

142
00:18:36,480 --> 00:18:43,040
different mind states bring different results. It's chaotic. It's not like it's going to show it

143
00:18:43,040 --> 00:18:48,240
in some charge or something. You're not going to be able to charge it, but you're going to

144
00:18:48,240 --> 00:18:56,160
send. You'll start to see thinking a lot. Thinking a lot gives me a headache. There's delusion,

145
00:18:58,080 --> 00:19:05,680
frustration, frustration makes me tired, makes me feel hot and sick inside.

146
00:19:05,680 --> 00:19:17,280
Greed makes me feel dirty or greasy or unpleasant, makes me feel agitated, makes me feel hot as well,

147
00:19:19,360 --> 00:19:20,720
burns us up inside.

148
00:19:27,920 --> 00:19:33,520
Then we start to naturally, naturally incline away from bad karma,

149
00:19:33,520 --> 00:19:38,400
but also away from trying to make good karma.

150
00:19:41,360 --> 00:19:45,040
As we start to understand karma more and more, we start to lose our ambitions.

151
00:19:46,480 --> 00:19:54,080
We start to lose the need, the drive to do this or that, but we become more content and more resolved.

152
00:19:54,080 --> 00:20:05,280
So it's not that we lose our effort or we become lazy. We become more intent on

153
00:20:06,480 --> 00:20:14,400
not becoming, not just being, I'm not even being, but in the sense of stopping,

154
00:20:14,400 --> 00:20:30,160
of staying put and become very interested and focused and all of our energy goes into stopping,

155
00:20:31,840 --> 00:20:42,160
into staying, into being. Being isn't the right word, but what it means is not

156
00:20:42,160 --> 00:20:51,280
striving, not reaching. In a sense, striving to stop striving,

157
00:20:54,880 --> 00:20:56,160
striving to stop karma.

158
00:20:59,520 --> 00:21:05,280
Anyway, some scattered thoughts, interesting, very interesting topic.

159
00:21:05,280 --> 00:21:11,840
Again, just want to think about how wonderful it is that

160
00:21:12,720 --> 00:21:17,760
meditation is taking off or has taken off, and it's really a part of the world.

161
00:21:17,760 --> 00:21:34,320
We think of the world as being full of problems, full of dangers, full of unwholesome is really.

162
00:21:35,200 --> 00:21:41,520
There's a lot of, I guess, for lack of a better world, where there's a lot of evil in the world,

163
00:21:41,520 --> 00:21:48,240
if you look at it that way. I don't think it's so useful to dwell upon the evil. There's a lot of

164
00:21:48,240 --> 00:21:57,760
talk about activism and getting involved. I'm not going to say anything. I'm going to criticize it,

165
00:21:57,760 --> 00:22:08,800
but I think there's room for arguably the idea of focusing on the good, like Pollyanna,

166
00:22:08,800 --> 00:22:17,840
sort of, if you read the book, Pollyanna, or was it a book? Sometimes if you focus on the good,

167
00:22:17,840 --> 00:22:25,360
it becomes your universe. Buddhism, I think there's room to talk about this sort of attitude.

168
00:22:28,320 --> 00:22:33,280
If we think about all the meditation that goes on and we gain confidence and encouragement

169
00:22:33,280 --> 00:22:39,840
from that and we work and we focus all of our efforts, not on changing the world or correcting

170
00:22:39,840 --> 00:22:46,720
people's wrong views, but work to support people and to support the practice of meditation,

171
00:22:46,720 --> 00:22:51,840
to encourage others to meditate, to encourage the cultivation of right view.

172
00:22:53,680 --> 00:23:01,520
I think this is what has had this great and lasting impact, bringing goodness to people.

173
00:23:01,520 --> 00:23:08,880
It's never going to fix the world, but it's certainly a good way to live.

174
00:23:11,040 --> 00:23:14,880
I think that's the point. We can't fix the world. We can't change the world,

175
00:23:16,160 --> 00:23:24,880
but we can live well. We can set ourselves in what's right. We can set ourselves on the right path,

176
00:23:24,880 --> 00:23:30,720
and we can have our lives be an outpouring of goodness.

177
00:23:33,600 --> 00:23:37,840
I do this through many things, but really the core of it is we do it through meditation.

178
00:23:39,040 --> 00:23:42,800
So it's awesome to see so many people interested here and

179
00:23:45,600 --> 00:23:51,280
interested around the world. It sounds like from what we hear, we hear reports,

180
00:23:51,280 --> 00:23:55,680
more and more of people interested in meditation and mindfulness.

181
00:23:55,680 --> 00:24:20,640
So there you go. That's the demo for tonight. Thank you all for tuning in.

182
00:24:55,920 --> 00:25:04,800
If there are any questions, I'm happy to take them.

183
00:25:04,800 --> 00:25:25,040
When you say that evil thoughts that result in physical actions are

184
00:25:25,040 --> 00:25:40,480
karmically more severe compared to just thinking you. I think so. It's definitely more complicated

185
00:25:40,480 --> 00:25:47,120
than that because you can think terrible, terrible thoughts and let them consume you without

186
00:25:47,120 --> 00:25:54,880
ever doing anything. They will totally consume you and make you a very evil person. On the other hand,

187
00:25:54,880 --> 00:26:01,760
you can act offhand without much intention. Killing a mosquito doesn't take that much evil

188
00:26:01,760 --> 00:26:09,280
intention. It's awful. It's bad, but it's much worse than plotting someone's murder, for example.

189
00:26:09,280 --> 00:26:22,000
When is my TED Talk scheduled? March, I think, March 4th or 5th or something?

190
00:26:25,120 --> 00:26:28,800
They just got in touch with me today to confirm that they like my idea,

191
00:26:28,800 --> 00:26:33,840
but they're sending it on to their supervisors or something. It's a real

192
00:26:33,840 --> 00:26:46,160
TED is a real monolith juggernaut. It's a very big organization, so they have to talk to their

193
00:26:46,160 --> 00:26:48,320
superiors or their advisors or something.

194
00:26:48,320 --> 00:27:07,680
What do I do if an occurring meditation can't be labeled with one word? If I'm happy I can say

195
00:27:07,680 --> 00:27:14,640
happy, let's say a song is stuck in my head. Do I say song? I don't know. You would never say

196
00:27:14,640 --> 00:27:19,040
song because you're not experiencing song as you're experiencing is hearing.

197
00:27:22,000 --> 00:27:27,440
Or if I think of plans or conversations, do I say conversation or planning? What is the

198
00:27:27,440 --> 00:27:32,880
experience? Experience is really just thinking, but you can say also planning because it's a type of

199
00:27:32,880 --> 00:27:40,880
thinking, planning or remembering or something. If you're confused about all of this, you might say

200
00:27:40,880 --> 00:27:50,560
confused, confused, but it's good to be clear that you would never say song song because that's

201
00:27:50,560 --> 00:27:55,680
not what you're experiencing. You're experiencing hearing sound. You could say sound as

202
00:27:55,680 --> 00:28:01,600
possible. The song is just a concept. It's not entirely bad, but it's not so good.

203
00:28:01,600 --> 00:28:15,840
I probably have to, I still have to, I think you've heard all of what I'm going to talk about.

204
00:28:16,480 --> 00:28:21,600
I think I've already practiced most of it on you. The other day I started, I talked about

205
00:28:21,600 --> 00:28:31,520
some of it, but didn't give the talk. But yeah, I kind of have to sort of, well, they want us to

206
00:28:31,520 --> 00:28:39,280
memorize it, which I'm probably not going to do, but I'll have my points down. It's just trying

207
00:28:39,280 --> 00:28:51,280
to, my talk is going to be about meditation, but trying to explain meditation, especially if

208
00:28:51,280 --> 00:28:56,000
someone's never practiced meditation to sort of give a clear picture of what it means. It's not

209
00:28:56,000 --> 00:29:02,880
something magical. It's not even something hard to understand how it works, why it works. It's

210
00:29:02,880 --> 00:29:09,120
pretty easy if just to try to give a clear explanation of why it works, how it works, what it deals

211
00:29:09,120 --> 00:29:16,400
with, how it deals with habits, how it's a training, the two different kinds of meditation,

212
00:29:16,400 --> 00:29:26,320
and sort of the technique that we use to cultivate a specific type of habit, and to allow us to

213
00:29:26,320 --> 00:29:35,920
see our habits, to understand and to view our habits objectively, so that we get a clear picture

214
00:29:35,920 --> 00:29:46,400
of them in a clear vision of it, clear understanding of it. I wouldn't be, I wouldn't be too

215
00:29:47,280 --> 00:29:53,440
excited about it. I don't know that it's going to be all that impressive. It's certainly not my

216
00:29:53,440 --> 00:30:08,080
forte to give this kind of talk, to a non meditative audience, not that I'm terribly good at that

217
00:30:08,080 --> 00:30:28,880
either, but we do our best. All right, well, there are no other questions. We will stop there.

218
00:30:30,400 --> 00:30:34,960
Thank you all for coming out. Tomorrow, I think we have a special session. I'll be back again,

219
00:30:34,960 --> 00:30:40,800
as usual, but they want me to talk about the Chinese New Year, which is, I don't know,

220
00:30:40,800 --> 00:30:46,400
and Chinese New Year is somewhat meaningless to me, but I'll try and think of something to say,

221
00:30:46,400 --> 00:31:08,560
besides just happy New Year. Anyway, have a good night.

