1
00:00:00,000 --> 00:00:10,800
Hi, so this is a video in response to someone's question about death and funeral.

2
00:00:10,800 --> 00:00:19,680
The question was in regards to one's Catholic family and there's the concern that if

3
00:00:19,680 --> 00:00:25,880
one, this person hasn't let their family know that they're Buddhist and he's afraid

4
00:00:25,880 --> 00:00:32,600
that she is afraid that if they don't let them know then the family is going to give

5
00:00:32,600 --> 00:00:38,320
him a Catholic funeral and they don't know how to let they're wondering how they can

6
00:00:38,320 --> 00:00:44,520
let their family know that they're Buddhist and they want to do it in a way that it's

7
00:00:44,520 --> 00:00:54,120
not seen as boastful or bragging or just trying to be different or so on.

8
00:00:54,120 --> 00:00:59,360
First of all, I wanted to say in what I mentioned initially in my response was that the

9
00:00:59,360 --> 00:01:04,200
most important people at a funeral are not the dead people, they're the living people,

10
00:01:04,200 --> 00:01:07,720
the people who are left behind because they're the ones who are really suffering and

11
00:01:07,720 --> 00:01:10,080
are generally unable to cope with the experience.

12
00:01:10,080 --> 00:01:15,440
The dead person's dead and it's true that sometimes they hang around after a person

13
00:01:15,440 --> 00:01:23,840
dies, they can hang around for several days or it seems even years are possible.

14
00:01:23,840 --> 00:01:28,840
Especially if you're Buddhist and if you're practicing Buddhism, it should be a more

15
00:01:28,840 --> 00:01:34,240
concern or at least some concern as to how your family responds and certainly you don't

16
00:01:34,240 --> 00:01:39,600
want to hang around and watch your family suffering.

17
00:01:39,600 --> 00:01:43,840
If you force them to do a Buddhist funeral for you then you have to consider whether

18
00:01:43,840 --> 00:01:49,480
that's going to cause more suffering than more harm than good.

19
00:01:49,480 --> 00:01:54,080
Because you might want to let them give you a Catholic funeral if it's what makes them

20
00:01:54,080 --> 00:01:55,600
happy.

21
00:01:55,600 --> 00:01:59,640
In that regard, before I get on to actually letting people know that your Buddha is just

22
00:01:59,640 --> 00:02:04,840
in regard to funerals, there is no such thing as a Buddhist funeral, Buddhism was not

23
00:02:04,840 --> 00:02:12,080
a religion for specifically for society, it's obviously a religion to leave behind

24
00:02:12,080 --> 00:02:17,480
society and to go inside yourself.

25
00:02:17,480 --> 00:02:23,600
The idea of a Buddhist funeral is a bit misleading, any sort of culture is fine, if you're

26
00:02:23,600 --> 00:02:27,640
going to do a Brahmin funeral, that's what they would have done in the Buddhist time and

27
00:02:27,640 --> 00:02:33,200
burn the body and put it in the river or so on.

28
00:02:33,200 --> 00:02:37,360
That being said, there are three things, three principles to a Buddhist funeral, funeral

29
00:02:37,360 --> 00:02:44,480
that one could consider, this is a funeral based on Buddhist principles and these three

30
00:02:44,480 --> 00:02:48,640
principles are things that people should keep in mind and I've always taught these as three

31
00:02:48,640 --> 00:02:50,760
things to keep in mind at funeral.

32
00:02:50,760 --> 00:02:55,280
The first one is that you should never grieve for the person who has passed away and this

33
00:02:55,280 --> 00:02:59,440
is something that you can let people know before you die, that you forbid them to grieve

34
00:02:59,440 --> 00:03:05,720
for you or not forbid them, you don't want them to and the reason for this is because

35
00:03:05,720 --> 00:03:07,040
it doesn't help.

36
00:03:07,040 --> 00:03:12,520
There's no benefit that is gained from crying and wailing and thinking fondly back over

37
00:03:12,520 --> 00:03:14,560
the times that you had.

38
00:03:14,560 --> 00:03:21,000
The most important thing you can do while you're still alive is in treat people to understand

39
00:03:21,000 --> 00:03:26,760
the transientcy of life that in the end we all pass away and this is only, we can say

40
00:03:26,760 --> 00:03:32,160
we're family but this is only a temporary span of time that eventually we have to leave

41
00:03:32,160 --> 00:03:38,640
each other behind so emphasizing this is very important and it's something that should

42
00:03:38,640 --> 00:03:43,480
be emphasized at the funeral and there's often, maybe there's a way to let people

43
00:03:43,480 --> 00:03:49,720
know that and to ensure that they keep that in mind at your funeral that it's not supposed

44
00:03:49,720 --> 00:03:53,920
to be a grieving process, it's supposed to be a saying goodbye and moving on to the next

45
00:03:53,920 --> 00:03:55,560
part of life.

46
00:03:55,560 --> 00:04:03,000
The second thing is to take death as an example and to use it as a reason to think of your

47
00:04:03,000 --> 00:04:04,480
own mortality.

48
00:04:04,480 --> 00:04:09,080
So everyone at the funeral should be understanding that they as well will pass away and

49
00:04:09,080 --> 00:04:13,280
if they're going to sit around and cry and moan then everybody who lay leave behind will

50
00:04:13,280 --> 00:04:18,440
also sit around and cry and moan and so this is something that we should keep in mind

51
00:04:18,440 --> 00:04:24,680
it's something that we should use to wake ourselves up rather than to dwell upon.

52
00:04:24,680 --> 00:04:28,280
That is something that should make us think that I also have to pass away.

53
00:04:28,280 --> 00:04:33,240
A better thing to do than to sit around crying is to actually use it as an impetus to get

54
00:04:33,240 --> 00:04:40,560
something done to make your life meaningful and to go ahead and ready yourself for the

55
00:04:40,560 --> 00:04:45,680
eventuality of death and to prepare yourself for the reality of life that we're born and

56
00:04:45,680 --> 00:04:48,280
that we die.

57
00:04:48,280 --> 00:04:53,160
And the third thing is to do good deeds on behalf of the deceased person.

58
00:04:53,160 --> 00:05:00,680
So one thing that you can really emphasize for the people who are leaving behind is that

59
00:05:00,680 --> 00:05:07,120
they should do something good on your behalf when people die Buddhists will often use

60
00:05:07,120 --> 00:05:11,440
it as an opportunity to do good things in their name for people who are not Buddhists.

61
00:05:11,440 --> 00:05:16,440
You can just explain that all of the good that this person has done, all of the things

62
00:05:16,440 --> 00:05:23,320
that you love about them, all of the good qualities that they, all of the good qualities

63
00:05:23,320 --> 00:05:25,160
that they had inside.

64
00:05:25,160 --> 00:05:29,200
You should do whatever you can to ensure that those qualities are passed on or carried

65
00:05:29,200 --> 00:05:31,360
on and persist.

66
00:05:31,360 --> 00:05:35,920
And if you do good things in the name of this person, it's like they're still alive.

67
00:05:35,920 --> 00:05:40,920
It's like their legacy has been passed on.

68
00:05:40,920 --> 00:05:44,680
So this is how you'd run a Buddhist funeral.

69
00:05:44,680 --> 00:05:47,560
The other part of the question, if supposing you want to let people know that you're

70
00:05:47,560 --> 00:05:52,200
Buddhist, I don't think there's anything wrong with that.

71
00:05:52,200 --> 00:05:58,080
I think in fact it would be quite useful to do that before you pass away.

72
00:05:58,080 --> 00:06:04,880
But as far as dealing with people who are not Buddhist, you have to take it carefully

73
00:06:04,880 --> 00:06:10,280
and you have to try to help them understand in a way that doesn't alienate them, from

74
00:06:10,280 --> 00:06:13,480
alienate you from them or make them look at you as an outsider.

75
00:06:13,480 --> 00:06:15,320
You don't have to say you're Buddhist.

76
00:06:15,320 --> 00:06:21,440
You can start by saying that you're meditating, say that you understand, you're starting

77
00:06:21,440 --> 00:06:24,280
to understand that happiness and peace is inside.

78
00:06:24,280 --> 00:06:28,880
You can even quote the Bible, the kingdom of God is within and so on.

79
00:06:28,880 --> 00:06:33,160
And start to explain to them the things that you've started to realize as you practice.

80
00:06:33,160 --> 00:06:35,480
Buddhism is just a word.

81
00:06:35,480 --> 00:06:38,360
You don't have to say that you're Buddhist or tell people that you're Buddhist and

82
00:06:38,360 --> 00:06:45,320
certainly it doesn't matter, it shouldn't matter what the nature of your funeral is for

83
00:06:45,320 --> 00:06:46,320
a Buddhist.

84
00:06:46,320 --> 00:06:50,080
So I'd encourage you to try to stick to the fundamentals.

85
00:06:50,080 --> 00:06:54,240
If you look at my meditation videos, I never talk about Buddhism and you don't have to.

86
00:06:54,240 --> 00:06:59,520
You just practice and teach and study.

87
00:06:59,520 --> 00:07:03,960
And if you do that, you should be able to deal with just about anybody and relate to

88
00:07:03,960 --> 00:07:07,760
just about anybody and you'll find that there's actually a lot less difference between

89
00:07:07,760 --> 00:07:14,520
you and them and you'll be able to keep your relationship with them on positive terms.

90
00:07:14,520 --> 00:07:19,800
So try to explain to them again, what are the fundamental things that you've found helpful

91
00:07:19,800 --> 00:07:22,480
and useful in Buddhism and just talk about those.

92
00:07:22,480 --> 00:07:26,120
So let people know that you're meditating and that when you die, as they said, you don't

93
00:07:26,120 --> 00:07:30,440
want people to grieve, you want them to, you know, that they have to die and this is a

94
00:07:30,440 --> 00:07:31,640
part of life.

95
00:07:31,640 --> 00:07:39,760
And when you pass away, you will expect them to do and to act according to your principles.

96
00:07:39,760 --> 00:07:46,160
And if they love you, if they care for you to take seriously the nature of your life and

97
00:07:46,160 --> 00:07:48,160
the way, the way of your being.

98
00:07:48,160 --> 00:07:55,200
Okay, so I hope that helps, it's an interesting question, it's one that I often had to

99
00:07:55,200 --> 00:07:56,200
talk on.

100
00:07:56,200 --> 00:07:57,640
So I hope that's of use.

101
00:07:57,640 --> 00:08:25,160
If you have any more questions, please send them along.

