1
00:00:00,000 --> 00:00:15,080
Good evening, broadcasting live January 8, 2016.

2
00:00:15,080 --> 00:00:38,080
So I heard back from Ted, Ted X, Ted X people, I'm semi-finalist, I'm the next conference,

3
00:00:38,080 --> 00:01:01,080
there's 16 of us, only select 4, so now I have to write out a talk, now it's real work.

4
00:01:01,080 --> 00:01:12,080
I have to write out a talk and go to an interview and work shop.

5
00:01:12,080 --> 00:01:16,080
I'm sure I've gotten myself into a bit.

6
00:01:16,080 --> 00:01:21,080
You can write out the talk.

7
00:01:21,080 --> 00:01:28,080
20 minutes, not these talks, or 10 minutes, how long are they?

8
00:01:28,080 --> 00:01:31,080
20 minutes, it was long, I don't remember.

9
00:01:31,080 --> 00:01:35,080
I think that 10 talks for 20 minutes, maybe they're 10.

10
00:01:35,080 --> 00:01:42,080
Maybe 10?

11
00:01:42,080 --> 00:01:45,080
Yeah, they didn't let us know that at all.

12
00:01:45,080 --> 00:01:48,080
Am I supposed to know these things?

13
00:01:48,080 --> 00:01:52,080
I think I'm supposed to be like watching 10 talks every day.

14
00:01:52,080 --> 00:02:01,080
I haven't watched the 10 talk for a long time.

15
00:02:01,080 --> 00:02:03,080
Someone else.

16
00:02:03,080 --> 00:02:05,080
They move.

17
00:02:05,080 --> 00:02:14,080
Our Finnish meditator just finished his advanced course.

18
00:02:14,080 --> 00:02:17,080
So congratulations to him.

19
00:02:17,080 --> 00:02:19,080
He's downstairs.

20
00:02:19,080 --> 00:02:24,080
I asked if he wanted to come up and say hello, he said, can we do it another time?

21
00:02:24,080 --> 00:02:27,080
Okay, sure.

22
00:02:27,080 --> 00:02:30,080
He's still going strong.

23
00:02:30,080 --> 00:02:36,080
He's been an intensive meditation for the past 10 days,

24
00:02:36,080 --> 00:02:44,080
and isn't showing any signs of flagging.

25
00:02:44,080 --> 00:03:04,080
There's more Wednesday.

26
00:03:04,080 --> 00:03:06,080
This Wednesday, I think.

27
00:03:06,080 --> 00:03:10,080
I've been invited to give a talk in Toronto.

28
00:03:10,080 --> 00:03:14,080
So I need a ride to Toronto.

29
00:03:14,080 --> 00:03:18,080
Anybody want to give me a ride from Hamilton to Toronto?

30
00:03:18,080 --> 00:03:20,080
Wednesday afternoon.

31
00:03:20,080 --> 00:03:21,080
Why?

32
00:03:21,080 --> 00:03:25,080
Because I have a class and I wouldn't have time to take the bus there.

33
00:03:25,080 --> 00:03:27,080
I wouldn't make it on time.

34
00:03:27,080 --> 00:03:30,080
I have to get a ride that's the only way to make it.

35
00:03:30,080 --> 00:03:38,080
Maybe they can come pick me up.

36
00:03:38,080 --> 00:03:41,080
I'm just looking at TEDx talks here.

37
00:03:41,080 --> 00:03:44,080
It says that they must be under 18 minutes,

38
00:03:44,080 --> 00:03:49,080
but some are as short as five minutes long.

39
00:03:49,080 --> 00:03:51,080
Yeah, probably I could have done.

40
00:03:51,080 --> 00:04:03,080
I'm already a full-page lady.

41
00:04:03,080 --> 00:04:08,080
This is one of my paragraphs.

42
00:04:08,080 --> 00:04:12,080
This is all very, I just wrote it up just now.

43
00:04:12,080 --> 00:04:15,080
I'm not even going to change, but I spent the past 16 years practicing

44
00:04:15,080 --> 00:04:23,080
studying and switching meditation with Buddhist as the end of the list of events in Buddhist monk.

45
00:04:23,080 --> 00:04:34,080
I'm a monastic code that is over 2,500 people.

46
00:04:34,080 --> 00:04:38,080
I've been struck with things, straw heads.

47
00:04:38,080 --> 00:04:42,080
I've been in the caves and the jungles of Thailand and Sri Lanka,

48
00:04:42,080 --> 00:04:46,080
and I haven't worn anywhere in over 14 years.

49
00:04:46,080 --> 00:04:49,080
And at the same time since I've become among,

50
00:04:49,080 --> 00:04:52,080
I've learned how to program in several different computer languages.

51
00:04:52,080 --> 00:04:58,080
I've learned that broadcast is daily as well as an online community.

52
00:04:58,080 --> 00:05:06,080
I'm just writing probably the tone has to change as far as it has to be more lively.

53
00:05:06,080 --> 00:05:10,080
And it is these contrasts that interest me very much.

54
00:05:10,080 --> 00:05:12,080
During my early years as a Buddhist monk,

55
00:05:12,080 --> 00:05:18,080
I've ever seen a few different 80-year-old meditation.

56
00:05:18,080 --> 00:05:25,080
Last year I'd teach her, I'd like to wear master's image.

57
00:05:25,080 --> 00:05:28,080
He shared her listening to him in part,

58
00:05:28,080 --> 00:05:32,080
the ancient wisdom of the Buddha at the Thai meditators and think to myself,

59
00:05:32,080 --> 00:05:35,080
that really these teachings are not that hard to understand,

60
00:05:35,080 --> 00:05:39,080
the real problem with Buddhism and spirituality in general.

61
00:05:39,080 --> 00:05:43,080
If that would get lost in translation and adaptation

62
00:05:43,080 --> 00:05:46,080
and the degradation inherent in word of monk transmission,

63
00:05:46,080 --> 00:05:49,080
that winds up like a game of telephone or the message

64
00:05:49,080 --> 00:05:54,080
that we the modern world get, it's something very different from the rich of Jesus.

65
00:05:58,080 --> 00:06:02,080
Does it sound too dry?

66
00:06:02,080 --> 00:06:05,080
No, I think it sounds good.

67
00:06:05,080 --> 00:06:07,080
Sounds like a good start.

68
00:06:07,080 --> 00:06:08,080
Good.

69
00:06:08,080 --> 00:06:10,080
You hear my chair there?

70
00:06:10,080 --> 00:06:23,080
That was how long did that take to see?

71
00:06:23,080 --> 00:06:25,080
That was, I didn't time it,

72
00:06:25,080 --> 00:06:29,080
but that was probably like two minutes or a minute and a half.

73
00:06:29,080 --> 00:06:30,080
Not very long.

74
00:06:30,080 --> 00:06:32,080
I may want to shorten it a little bit,

75
00:06:32,080 --> 00:06:35,080
because this is all just introductory stuff.

76
00:06:35,080 --> 00:06:40,080
And then I get on to, it is this work, although I've gotten into the,

77
00:06:40,080 --> 00:06:47,080
see the idea is to contrast ancient teachings with modern technology.

78
00:06:47,080 --> 00:06:51,080
And actually, I think maybe ancient teachings in the modern world,

79
00:06:51,080 --> 00:06:52,080
how do you bridge these?

80
00:06:52,080 --> 00:06:59,080
How do you bring modern teachings, ancient teachings to the modern world?

81
00:06:59,080 --> 00:07:02,080
Maybe that's not even the best subject,

82
00:07:02,080 --> 00:07:06,080
but it's gonna be a part of it.

83
00:07:06,080 --> 00:07:10,080
I think the key is to strip it

84
00:07:10,080 --> 00:07:14,080
at which makes it ancient,

85
00:07:14,080 --> 00:07:23,080
which is culture, language.

86
00:07:23,080 --> 00:07:27,080
The sheer access to the information that we have is,

87
00:07:27,080 --> 00:07:31,080
you know, compared to people of the time, I mean,

88
00:07:31,080 --> 00:07:37,080
we have such a broader, so much broader information.

89
00:07:37,080 --> 00:07:47,080
And yet, I mean, the danger is that we're much more,

90
00:07:47,080 --> 00:07:55,080
we have much more immediate gratification pleasure.

91
00:07:55,080 --> 00:07:59,080
The gratification of desire.

92
00:07:59,080 --> 00:08:05,080
I think that your smartphone, and you've got something entertaining right away.

93
00:08:05,080 --> 00:08:07,080
Well, I'm thinking, pick up your smartphone,

94
00:08:07,080 --> 00:08:11,080
and you have, you know, the 40 years of the teachings of the Buddha right away.

95
00:08:11,080 --> 00:08:17,080
As compared to people at the time, you know, maybe they heard a few lines or, you know,

96
00:08:17,080 --> 00:08:21,080
one suta, but we have so much.

97
00:08:21,080 --> 00:08:29,080
Yeah, but the point I was trying to make was in terms of the quality of the mind,

98
00:08:29,080 --> 00:08:33,080
because the teachings can be there, and if there's no one interested in reading them.

99
00:08:33,080 --> 00:08:39,080
So, the person has to have a quality in mind to read the future.

100
00:08:39,080 --> 00:08:48,080
And so, I think a part of modern technology is pulling us away from spirituality.

101
00:08:48,080 --> 00:08:52,080
But there's a whole other part.

102
00:08:52,080 --> 00:08:59,080
I think you can argue that it's a significant part of it that brings us closer.

103
00:08:59,080 --> 00:09:07,080
Our breaking barriers, and expanding our horizons.

104
00:09:07,080 --> 00:09:20,080
It's really the focus on truth because the internet is very much about knowledge, of science,

105
00:09:20,080 --> 00:09:33,080
and much of it is sharing in general things that are making things free.

106
00:09:33,080 --> 00:09:58,080
And then there's this terrible technical aspect.

107
00:09:58,080 --> 00:10:18,080
It's supposed to make things easier.

108
00:10:18,080 --> 00:10:19,080
Is still breaking up?

109
00:10:19,080 --> 00:10:25,080
No, it sounds better now, much more clear.

110
00:10:25,080 --> 00:10:28,080
So, do we have any questions?

111
00:10:28,080 --> 00:10:31,080
We do.

112
00:10:31,080 --> 00:10:38,080
I think.

113
00:10:38,080 --> 00:10:42,080
That's not really a question.

114
00:10:42,080 --> 00:10:45,080
We have to skip that one.

115
00:10:45,080 --> 00:11:07,080
Is a long question that is asking about Mahayana Sutta, the Diamond Sutta, and a concept of arbitrary ideas, which I think is from the Sutta mentioned.

116
00:11:07,080 --> 00:11:09,080
Looks like we have no questions.

117
00:11:09,080 --> 00:11:13,080
What are you all doing here if you don't have any questions?

118
00:11:13,080 --> 00:11:16,080
Does anyone want to come on the Hangout and talk with us?

119
00:11:16,080 --> 00:11:19,080
Is there something different tonight?

120
00:11:19,080 --> 00:11:22,080
Come on, it's a Friday night, live a little.

121
00:11:22,080 --> 00:11:24,080
I put the link out for the Hangout.

122
00:11:24,080 --> 00:11:27,080
You just click on that link, join us in the Hangout.

123
00:11:27,080 --> 00:11:31,080
Have them and say hello.

124
00:11:31,080 --> 00:11:37,080
Bond wants to know if you can meet your teacher through Google Hangouts.

125
00:11:37,080 --> 00:11:43,080
Oh, me, me.

126
00:11:43,080 --> 00:11:45,080
A line would probably be better.

127
00:11:45,080 --> 00:11:48,080
May I could call him on the phone?

128
00:11:48,080 --> 00:11:54,080
That's probably the easiest is to just call him in his room.

129
00:11:54,080 --> 00:11:57,080
Is that John Tang used much technology?

130
00:11:57,080 --> 00:11:59,080
He used telephone.

131
00:11:59,080 --> 00:12:00,080
Not much else.

132
00:12:00,080 --> 00:12:01,080
He has an iPad.

133
00:12:01,080 --> 00:12:06,080
I think they got him an iPad, so they used that to show him photos and so on.

134
00:12:06,080 --> 00:12:08,080
His vision is not that good.

135
00:12:08,080 --> 00:12:12,080
He had surgery and it never really got better at his vision.

136
00:12:12,080 --> 00:12:18,080
It's not really good at all.

137
00:12:18,080 --> 00:12:21,080
He's 92 or something.

138
00:12:21,080 --> 00:12:25,080
Very tired.

139
00:12:25,080 --> 00:12:35,080
Not all the time, but gets tired easier.

140
00:12:35,080 --> 00:12:39,080
Did you hear how he gave up the will to live when he was 80?

141
00:12:39,080 --> 00:12:40,080
No.

142
00:12:40,080 --> 00:12:43,080
You know how the Buddha gave up the will to live in 80s.

143
00:12:43,080 --> 00:12:47,080
On his 80th birthday, he asked permission from everyone to die.

144
00:12:47,080 --> 00:12:52,080
The head monk for the northern region of Thailand came to his birthday.

145
00:12:52,080 --> 00:13:00,080
While he was sitting there, our teacher gets a John gets up and asks him permission to die.

146
00:13:00,080 --> 00:13:05,080
He said, you know, we're working really hard and doing a lot of things, but, you know,

147
00:13:05,080 --> 00:13:12,080
maybe he was so round to put he took a half an hour really to ask permission.

148
00:13:12,080 --> 00:13:16,080
He's like, you know, so it's good to see you here again.

149
00:13:16,080 --> 00:13:22,080
And maybe it's for the last time, maybe the last time.

150
00:13:22,080 --> 00:13:24,080
No, not that.

151
00:13:24,080 --> 00:13:28,080
Well, it's just that I've, you know, been working spent, you know,

152
00:13:28,080 --> 00:13:33,080
since he kept interrupting himself and starting over and trying to just,

153
00:13:33,080 --> 00:13:41,080
and then he said, you know, maybe might be that I just want to ask to die.

154
00:13:41,080 --> 00:13:43,080
So he wasn't sad.

155
00:13:43,080 --> 00:13:47,080
He was asking if he was asking if he could go on vacation or something?

156
00:13:47,080 --> 00:13:49,080
Pretty much yet.

157
00:13:49,080 --> 00:13:50,080
That's awesome.

158
00:13:50,080 --> 00:13:54,080
It's not, and he was really trying to reassure the head monk that he wasn't.

159
00:13:54,080 --> 00:13:59,080
You know, it's not, if I'm here, I'll,

160
00:13:59,080 --> 00:14:02,080
as long as I'm here, I'll help.

161
00:14:02,080 --> 00:14:05,080
And I've been working, you know,

162
00:14:05,080 --> 00:14:08,080
I've been, he said, I've been boxing for many years.

163
00:14:08,080 --> 00:14:12,080
And he said, I think maybe it's just time.

164
00:14:12,080 --> 00:14:16,080
And he used a very northern Thai word that they had monk and understand,

165
00:14:16,080 --> 00:14:18,080
because he's from Bangkok.

166
00:14:18,080 --> 00:14:20,080
That means tired out.

167
00:14:20,080 --> 00:14:25,080
And he said, I think I'm just going to think it's time to shove my gloves,

168
00:14:25,080 --> 00:14:28,080
the boxing gloves.

169
00:14:28,080 --> 00:14:31,080
And he said, I want to focus on meditation, but he said,

170
00:14:31,080 --> 00:14:36,080
and he said, like three times and I'd like to just die.

171
00:14:36,080 --> 00:14:41,080
And the head monk refused to let him and all the one, one laywoman who had just given him a very,

172
00:14:41,080 --> 00:14:44,080
very large donation, a sizeable donation,

173
00:14:44,080 --> 00:14:49,080
and was, you know, giving him lots of donations cried in front of the whole,

174
00:14:49,080 --> 00:14:52,080
the whole congregation.

175
00:14:52,080 --> 00:14:55,080
She was not a very spiritual person.

176
00:14:55,080 --> 00:15:04,080
And then that night there was an earthquake in Chantong.

177
00:15:04,080 --> 00:15:06,080
And there's never an earthquake in Chantong.

178
00:15:06,080 --> 00:15:09,080
It's not an earthquake zone as far as I know.

179
00:15:09,080 --> 00:15:14,080
There's never another time that I've heard of an earthquake or felt an earthquake there.

180
00:15:14,080 --> 00:15:17,080
But that night figures shook.

181
00:15:17,080 --> 00:15:22,080
So in the morning, we were all like, we got up early in the morning to go for Chantong,

182
00:15:22,080 --> 00:15:26,080
and we're like waiting to see whether he shows up or not.

183
00:15:26,080 --> 00:15:32,080
Because doesn't that happen when an enlightened being dies, there's an earthquake or something?

184
00:15:32,080 --> 00:15:35,080
When the Buddha gave up the will to live, there was an earthquake.

185
00:15:35,080 --> 00:15:39,080
I think when he passed away, there isn't earthquake.

186
00:15:39,080 --> 00:15:48,080
So, that's our legend that we're, well, it's true.

187
00:15:48,080 --> 00:15:51,080
That's our magical story of our teacher.

188
00:15:51,080 --> 00:15:54,080
Which was the other side of his still hanging in there.

189
00:15:54,080 --> 00:15:56,080
I don't know what he's hanging in.

190
00:15:56,080 --> 00:16:01,080
I think it's just, he said, you know,

191
00:16:01,080 --> 00:16:07,080
no one knows when they're going to die.

192
00:16:07,080 --> 00:16:12,080
You can't just say you're going to die.

193
00:16:12,080 --> 00:16:14,080
So he's still here.

194
00:16:14,080 --> 00:16:21,080
But he changed and became far less felt like it became a lot less interested

195
00:16:21,080 --> 00:16:25,080
in a lot less coming home about teaching and so on.

196
00:16:25,080 --> 00:16:32,080
He really sort of retreated in more, actually that.

197
00:16:32,080 --> 00:16:37,080
He's still teaches a lot less.

198
00:16:37,080 --> 00:16:40,080
Even today, he's probably still teaching.

199
00:16:40,080 --> 00:16:45,080
Well, I can guarantee he's still teaching.

200
00:16:45,080 --> 00:16:53,080
He's too ill.

201
00:16:53,080 --> 00:16:58,080
Now, I mean, at some point, I probably should go to see him.

202
00:16:58,080 --> 00:17:03,080
I don't think there's any way around it.

203
00:17:03,080 --> 00:17:09,080
If I don't, then yeah, that might be an alternative is to call him.

204
00:17:09,080 --> 00:17:15,080
It's not the same.

205
00:17:15,080 --> 00:17:18,080
And it's not that important.

206
00:17:18,080 --> 00:17:20,080
I don't get to go see him.

207
00:17:20,080 --> 00:17:22,080
I don't get to go see him.

208
00:17:22,080 --> 00:17:29,080
If I have the chance, I probably should.

209
00:17:29,080 --> 00:17:33,080
Have you gotten any more information on the, on the conference or?

210
00:17:33,080 --> 00:17:35,080
He's sending it to me tomorrow.

211
00:17:35,080 --> 00:17:41,080
Good to meet you tonight.

212
00:17:41,080 --> 00:17:49,080
And Sunday, we're having this piece meeting group thing.

213
00:17:49,080 --> 00:17:51,080
So many things going on.

214
00:17:51,080 --> 00:17:59,080
I should probably go because I've got things to think about and plan and we're going to.

215
00:17:59,080 --> 00:18:03,080
Are you all set for the event on Sunday?

216
00:18:03,080 --> 00:18:04,080
I don't know.

217
00:18:04,080 --> 00:18:07,080
Also, maybe no tomorrow.

218
00:18:07,080 --> 00:18:08,080
Okay.

219
00:18:08,080 --> 00:18:15,080
Are you, we're going to go to the volunteer meeting before or no?

220
00:18:15,080 --> 00:18:17,080
Right.

221
00:18:17,080 --> 00:18:22,080
Probably.

222
00:18:22,080 --> 00:18:27,080
Yeah, I'm sure we'll have everything set up for that before that.

223
00:18:27,080 --> 00:18:30,080
I'll probably make both meetings.

224
00:18:30,080 --> 00:18:39,080
That's not a problem.

225
00:18:39,080 --> 00:18:42,080
So yeah, volunteer meeting if anyone wants to join.

226
00:18:42,080 --> 00:18:45,080
We need more volunteers.

227
00:18:45,080 --> 00:18:48,080
People to help us figure stuff out.

228
00:18:48,080 --> 00:18:49,080
Right.

229
00:18:49,080 --> 00:18:50,080
Do we, we don't we?

230
00:18:50,080 --> 00:18:51,080
We do.

231
00:18:51,080 --> 00:18:52,080
Yes.

232
00:18:52,080 --> 00:18:56,080
So it's Sunday at 1 p.m. Eastern time.

233
00:18:56,080 --> 00:18:58,080
And we meet on Google Hangouts.

234
00:18:58,080 --> 00:19:01,080
And if you just let me know who you are.

235
00:19:01,080 --> 00:19:06,080
If you'd like to be in the Hangout, it's broadcasted online.

236
00:19:06,080 --> 00:19:11,080
Not a lot of people watch the volunteer meetings some do, but it's just a good way of us.

237
00:19:11,080 --> 00:19:17,080
You know, to get together, talk up on the and find out what type of things are needed each week for the monastery.

238
00:19:17,080 --> 00:19:25,080
So if anyone would like to join, just let me know.

239
00:19:25,080 --> 00:19:27,080
I think that's it.

240
00:19:27,080 --> 00:19:28,080
No questions.

241
00:19:28,080 --> 00:19:30,080
No questions.

242
00:19:30,080 --> 00:19:31,080
Okay.

243
00:19:31,080 --> 00:19:36,080
We've told everyone everything.

244
00:19:36,080 --> 00:19:37,080
Okay.

245
00:19:37,080 --> 00:19:38,080
Thank you all.

246
00:19:38,080 --> 00:19:39,080
Thanks Robin.

247
00:19:39,080 --> 00:19:41,080
Okay, have a good evening.

