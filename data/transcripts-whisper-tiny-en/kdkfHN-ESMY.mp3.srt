1
00:00:00,000 --> 00:00:05,000
Okay, so good evening, and welcome to another episode of Monk Radio.

2
00:00:07,000 --> 00:00:10,000
Announcements for tonight.

3
00:00:10,000 --> 00:00:18,000
First, we have some sad news, I suppose.

4
00:00:18,000 --> 00:00:22,000
One announcement is that Sumeda left as well.

5
00:00:22,000 --> 00:00:30,000
So we're back down to just me and the Burmese then.

6
00:00:30,000 --> 00:00:35,000
Sumeda left because she had been sick while I was away,

7
00:00:35,000 --> 00:00:40,000
and she tried to go to the hospital and see about getting care there,

8
00:00:40,000 --> 00:00:44,000
but her father in the end asked that she go home.

9
00:00:44,000 --> 00:00:48,000
She had the stomach ulcer, and so she's still in office,

10
00:00:48,000 --> 00:00:52,000
and her plan is to get better there,

11
00:00:52,000 --> 00:00:56,000
and to either come back here to continue as a seminary

12
00:00:56,000 --> 00:01:01,000
or to find a place monastery in the west.

13
00:01:01,000 --> 00:01:04,000
So we'll see how that goes.

14
00:01:04,000 --> 00:01:09,000
Next announcement, we have four new Thai meditators visiting.

15
00:01:09,000 --> 00:01:17,000
These are three of them are people who I have been in contact with before in Thailand,

16
00:01:17,000 --> 00:01:25,000
and the fourth one is a newcomer who I just met last time I was in Thailand for the first time.

17
00:01:25,000 --> 00:01:31,000
So they're here for nine days to do some continue.

18
00:01:31,000 --> 00:01:33,000
One of them is going to continue her meditation course,

19
00:01:33,000 --> 00:01:36,000
and hopefully finish it while she's here,

20
00:01:36,000 --> 00:01:40,000
and the other two are just going to do some review work practicing.

21
00:01:40,000 --> 00:01:46,000
The original intention was to come and go sightseeing around the country.

22
00:01:46,000 --> 00:01:55,000
But out of respect for the woman who is going to finish,

23
00:01:55,000 --> 00:02:03,000
they've decided to stay here and let her to finish the course instead of going sightseeing.

24
00:02:03,000 --> 00:02:06,000
And the other, the final announcement,

25
00:02:06,000 --> 00:02:09,000
I suppose there's other ones, maybe I should get out of the way.

26
00:02:09,000 --> 00:02:10,000
I can't think of them.

27
00:02:10,000 --> 00:02:15,000
The two rooms above are finished, are livable now.

28
00:02:15,000 --> 00:02:18,000
There are two new rooms that I've been talking about.

29
00:02:18,000 --> 00:02:20,000
The electricity is now in.

30
00:02:20,000 --> 00:02:24,000
There's some more mason work that has to be done to cover up the electricity.

31
00:02:24,000 --> 00:02:31,000
And then a railing has to be put in, but they are livable now.

32
00:02:31,000 --> 00:02:35,000
And it's been pretty dry here.

33
00:02:35,000 --> 00:02:38,000
And so they've been cutting out the power every day,

34
00:02:38,000 --> 00:02:43,000
because there's not enough hydroelectricity.

35
00:02:43,000 --> 00:02:45,000
But otherwise everything is good.

36
00:02:45,000 --> 00:02:49,000
But the final announcement is that,

37
00:02:49,000 --> 00:02:55,000
and one that should be, I think, exciting for our viewing audience,

38
00:02:55,000 --> 00:03:00,000
is that this afternoon,

39
00:03:00,000 --> 00:03:06,000
one of, well, how it happened is the story goes,

40
00:03:06,000 --> 00:03:12,000
the new meditator, who I just met recently,

41
00:03:12,000 --> 00:03:20,000
she has, she asked to go on, on, on, on.

42
00:03:20,000 --> 00:03:26,000
To follow after the monks on, on, on, because she's had interest in the monastic life.

43
00:03:26,000 --> 00:03:30,000
And also because she just wanted to see the countryside.

44
00:03:30,000 --> 00:03:36,000
And so I said, well, you know, it's not really good for women to follow after the male monks.

45
00:03:36,000 --> 00:03:41,000
And then I joked and I said, but, you know, if you wanted to ordain,

46
00:03:41,000 --> 00:03:45,000
then you could, you shave your head and ordain,

47
00:03:45,000 --> 00:03:47,000
then you could go and arms around yourself.

48
00:03:47,000 --> 00:03:50,000
And she actually took me up on it.

49
00:03:50,000 --> 00:03:55,000
And so she's going to ordain as a novice monk.

50
00:03:55,000 --> 00:03:59,000
And you're going to get to witness that.

51
00:03:59,000 --> 00:04:01,000
It's kind of crazy, huh?

52
00:04:01,000 --> 00:04:03,000
We'll see how that goes.

53
00:04:03,000 --> 00:04:08,000
So here I have in front of me, a robe set in the bowl,

54
00:04:08,000 --> 00:04:10,000
and she's going to ordain.

55
00:04:10,000 --> 00:04:13,000
Now it's going to be a temporary ordination,

56
00:04:13,000 --> 00:04:15,000
the way they do it in Thailand.

57
00:04:15,000 --> 00:04:20,000
And we talked about it, and normally we wouldn't want to do a temporary ordination.

58
00:04:20,000 --> 00:04:24,000
But under the circumstances, the reason it's going to be temporary

59
00:04:24,000 --> 00:04:29,000
is because she still has to coordinate with her mother.

60
00:04:29,000 --> 00:04:33,000
Her mother is 60, I think.

61
00:04:33,000 --> 00:04:36,000
But it's only her and her mother.

62
00:04:36,000 --> 00:04:40,000
Her mother is only one child, and I guess her father is passed away.

63
00:04:40,000 --> 00:04:44,000
And so for her to ordain in the long term, right now, her mother

64
00:04:44,000 --> 00:04:45,000
wouldn't be happy.

65
00:04:45,000 --> 00:04:50,000
Obviously, if she stayed here, it wouldn't be difficult for her mother.

66
00:04:50,000 --> 00:04:55,000
Her mother doesn't need the support, but sort of the emotional support.

67
00:04:55,000 --> 00:04:59,000
So her mother was, we talked to her mother on the internet,

68
00:04:59,000 --> 00:05:05,000
and she was concerned, you know, just the extremity of having to

69
00:05:05,000 --> 00:05:08,000
save your head and put on robes.

70
00:05:08,000 --> 00:05:11,000
So we said, okay, so what you can do is ordain temporarily now.

71
00:05:11,000 --> 00:05:14,000
And then in the future, come and do some serious practice,

72
00:05:14,000 --> 00:05:17,000
and then consider to ordain in the long term.

73
00:05:17,000 --> 00:05:21,000
I mean, obviously the other thing is we wouldn't want to ordain someone so quickly

74
00:05:21,000 --> 00:05:25,000
who is a newcomer and someone we're not so sure about.

75
00:05:25,000 --> 00:05:29,000
So we're going to do a traditional or a, I guess you could say,

76
00:05:29,000 --> 00:05:32,000
a modern Thai ordination.

77
00:05:32,000 --> 00:05:36,000
Because we talked about it here, and the, in the end, the point is that this

78
00:05:36,000 --> 00:05:39,000
could be her only opportunity in her life.

79
00:05:39,000 --> 00:05:42,000
The future's not certain, not certain.

80
00:05:42,000 --> 00:05:53,000
And so at the, at worst, it's at the least, it's a chance for her to have the opportunity to be in robes.

81
00:05:53,000 --> 00:05:59,000
And make, kind of, make exceptions for Thai people because they are traditionally

82
00:05:59,000 --> 00:06:04,000
Buddhist, and you can be pretty sure that they're not going to cause problems in the monastery.

83
00:06:04,000 --> 00:06:09,000
They have a good knowledge of monasteries and how to live with monks, how to treat monks,

84
00:06:09,000 --> 00:06:12,000
how to deal with a, with a monastic situation.

85
00:06:12,000 --> 00:06:15,000
So why I say all this, because I don't want to get people,

86
00:06:15,000 --> 00:06:18,000
give people the idea that we do temporary ordinations.

87
00:06:18,000 --> 00:06:25,000
Normally we wouldn't do this, but this is going to be based on the Thai way of doing things.

88
00:06:25,000 --> 00:06:30,000
So it's, it's not something out of the ordinary for the Thai people.

89
00:06:30,000 --> 00:06:35,000
And, you know, we, we're reasonably comfortable given that this is, you know,

90
00:06:35,000 --> 00:06:40,000
close friend of the people who are longtime students of mine, and they can vouch for her, of course.

91
00:06:40,000 --> 00:06:44,000
And that it's only going to be a seven day thing.

92
00:06:44,000 --> 00:06:50,000
Then when I go to Thailand and the winter, she'll be able to come and, and do more serious,

93
00:06:50,000 --> 00:06:54,000
and practice an ordination, if possible.

94
00:06:54,000 --> 00:06:58,000
Just to warn you, that's going to interrupt tonight's program.

95
00:06:58,000 --> 00:07:08,000
So, I'm going to have to put up with that, and hopefully it'll be something interesting.

96
00:07:08,000 --> 00:07:32,000
Okay, so that's announcements.

