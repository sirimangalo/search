1
00:00:00,000 --> 00:00:05,000
Okay, so announcements for today.

2
00:00:05,000 --> 00:00:14,000
Well, this is our first live broadcast since I've been back from Thailand.

3
00:00:14,000 --> 00:00:19,000
It was just a way in Thailand for my second trip in as many months.

4
00:00:19,000 --> 00:00:24,000
And there's lots to announce from that trip.

5
00:00:24,000 --> 00:00:32,000
We have one new member of the community.

6
00:00:32,000 --> 00:00:38,000
And we've lost another member of our community, so changing.

7
00:00:38,000 --> 00:00:45,000
But we picked up Michi Jai, whose poly name is Atasiri.

8
00:00:45,000 --> 00:00:52,000
And so she will be staying here for a while, and if she likes it, which I think she does.

9
00:00:52,000 --> 00:01:03,000
She might try to get a long-term visa, and stay over the long-term, and maybe perhaps her date as a salamary.

10
00:01:03,000 --> 00:01:11,000
Michi Jai is a little bit older, and she's someone who has helped me a lot in Thailand.

11
00:01:11,000 --> 00:01:20,000
Some people in our community actually know her from Thailand, where we were working together to help foreign meditators.

12
00:01:20,000 --> 00:01:28,000
Anyway, so she's here now. She's staying in one of the caves.

13
00:01:28,000 --> 00:01:34,000
Yes, I has left, so we're left with one salamary, and myself we're going on on-shound.

14
00:01:34,000 --> 00:01:39,000
We have a new well that has been dug with lots of water, which is great.

15
00:01:39,000 --> 00:01:47,000
Our old well had very, very little water in it, even during the rainy season.

16
00:01:47,000 --> 00:01:52,000
It's always rainy season here, but we found a new well, and I just went and looked at it today.

17
00:01:52,000 --> 00:01:54,000
I'll have some pictures of that up.

18
00:01:54,000 --> 00:01:58,000
I'm trying to put a picture of that up on the internet, because it's quite exciting.

19
00:01:58,000 --> 00:02:02,000
There's about 10 feet of water in it, which is really great.

20
00:02:02,000 --> 00:02:09,000
They just dug a hole in the forest, and they dug it in the right spot.

21
00:02:09,000 --> 00:02:15,000
So we'll have water soon, or we'll have a new source of water soon.

22
00:02:15,000 --> 00:02:20,000
And the rooms are almost finished.

23
00:02:20,000 --> 00:02:27,000
We've got almost two new rooms, and today they were working on the wiring,

24
00:02:27,000 --> 00:02:31,000
and they're going to come back next Saturday to finish up the wiring,

25
00:02:31,000 --> 00:02:35,000
which is just in time for our new meditators.

26
00:02:35,000 --> 00:02:41,000
On the 22nd, I believe, there's a man coming from...

27
00:02:41,000 --> 00:02:45,000
I don't remember where Finland, I think.

28
00:02:45,000 --> 00:02:49,000
And he wants to stay into October, and then go with me to Thailand,

29
00:02:49,000 --> 00:02:56,000
which is my idea that my next announcement that I'll be heading back to Thailand in November.

30
00:02:56,000 --> 00:02:59,000
But let's get through this first.

31
00:02:59,000 --> 00:03:05,000
So this man coming from Finland, he will be here on the 22nd, and then on the 27th,

32
00:03:05,000 --> 00:03:08,000
there are four Thai meditators coming.

33
00:03:08,000 --> 00:03:11,000
So it's really, really picking up actually.

34
00:03:11,000 --> 00:03:17,000
Then in August, there are two men coming, and there are still two or three people

35
00:03:17,000 --> 00:03:21,000
who are interested in becoming monks who are still working out their affairs,

36
00:03:21,000 --> 00:03:25,000
and might be heading over here sometime in the near future.

37
00:03:25,000 --> 00:03:32,000
So it looks like the interest in coming out to Sri Lanka is really picking up.

38
00:03:32,000 --> 00:03:37,000
One thing about coming to Sri Lanka is it doesn't seem to have ever been

39
00:03:37,000 --> 00:03:44,000
an option for people, or something that people consider.

40
00:03:44,000 --> 00:03:49,000
Most people, when they think of going to practice meditation,

41
00:03:49,000 --> 00:03:52,000
think of going to Thailand to practice.

42
00:03:52,000 --> 00:04:00,000
And myself included, you never would hear about much about going to Sri Lanka

43
00:04:00,000 --> 00:04:02,000
to practice meditation.

44
00:04:02,000 --> 00:04:07,000
Now, it turns out that there are a couple of really nice meditation centers here,

45
00:04:07,000 --> 00:04:11,000
but they're not well publicized.

46
00:04:11,000 --> 00:04:13,000
And I think that is changing now.

47
00:04:13,000 --> 00:04:19,000
Nisadana now has a very nice website, if you look up Nisadana.org or something.

48
00:04:19,000 --> 00:04:25,000
And Kanduboda has many foreign students now, and that's the publicity on that,

49
00:04:25,000 --> 00:04:26,000
is getting out.

50
00:04:26,000 --> 00:04:30,000
They published a couple of books of one-day payment series teachings.

51
00:04:30,000 --> 00:04:34,000
So I really think it's worth to say this,

52
00:04:34,000 --> 00:04:40,000
that Sri Lanka is a viable place to come to practice meditation.

53
00:04:40,000 --> 00:04:55,000
And it's an incredibly beneficial place or is a good place for living as a monk.

54
00:04:55,000 --> 00:04:56,000
It's conducive.

55
00:04:56,000 --> 00:05:01,000
It's an incredibly conducive place to live the monk's life,

56
00:05:01,000 --> 00:05:05,000
much more than any place that I've ever lived.

57
00:05:05,000 --> 00:05:08,000
So for those people who are thinking to ordain,

58
00:05:08,000 --> 00:05:12,000
this is a good place to recommend.

59
00:05:12,000 --> 00:05:21,000
Now, that being said, it turns out that I'm probably not going to be here all of the time in the future.

60
00:05:21,000 --> 00:05:26,000
So as for my own future works,

61
00:05:26,000 --> 00:05:33,000
it looks like I'm going to be heading to Thailand as I said in November for three months.

62
00:05:33,000 --> 00:05:36,000
And that might become a pattern in the future,

63
00:05:36,000 --> 00:05:41,000
where I spend something like three months in Sri Lanka and three months in Thailand.

64
00:05:41,000 --> 00:05:50,000
Because the last announcement is that this forest monastery that I was staying at,

65
00:05:50,000 --> 00:05:56,000
remember when one of the many forest monasteries that I tried to desperately

66
00:05:56,000 --> 00:06:01,000
invite to set up a forest meditation center that I could say was mine,

67
00:06:01,000 --> 00:06:06,000
and that I would run and so on.

68
00:06:06,000 --> 00:06:13,000
Then the head monk there is sort of been put in the position by my teacher.

69
00:06:13,000 --> 00:06:17,000
And he's not really keen on it.

70
00:06:17,000 --> 00:06:34,000
So he quite willingly or emphatically insistently insisted that I come and help him there.

71
00:06:34,000 --> 00:06:38,000
So which is really, really fine with me.

72
00:06:38,000 --> 00:06:43,000
I'm quite happy to do it because of the nature of the place.

73
00:06:43,000 --> 00:06:48,000
I put a lot of work into the place before building up the koutis

74
00:06:48,000 --> 00:06:54,000
and repairing the place and turning it into what we thought would be

75
00:06:54,000 --> 00:06:57,000
a really good international meditation center.

76
00:06:57,000 --> 00:07:01,000
And now the exciting thing is it looks like that might come to be.

77
00:07:01,000 --> 00:07:07,000
That he's really keen to have it become a center and he's not at all possessive of it.

78
00:07:07,000 --> 00:07:14,000
So what he wants to do is go home to his province of Radbury near Bangkok

79
00:07:14,000 --> 00:07:20,000
and give it to me for three months or whatever for a month or whatever he decides.

80
00:07:20,000 --> 00:07:23,000
And his hope is that I'll be able to do that in the future.

81
00:07:23,000 --> 00:07:27,000
I think it's a good sort of compromise.

82
00:07:27,000 --> 00:07:32,000
One of the things is that being in Thailand for so long,

83
00:07:32,000 --> 00:07:38,000
I lost a lot of my English skills as I think is probably apparent,

84
00:07:38,000 --> 00:07:42,000
but I gained a fair fluency in the Thai language.

85
00:07:42,000 --> 00:07:49,000
I'm able to teach Buddhism in Thai, which I really shone through in this past month,

86
00:07:49,000 --> 00:07:53,000
the ability to teach even Thai people.

87
00:07:53,000 --> 00:07:57,000
So it would be a shame not to spend some time in Thailand,

88
00:07:57,000 --> 00:08:04,000
even though I'm just as or even even more comfortable just staying in Sri Lanka.

89
00:08:04,000 --> 00:08:07,000
But it looks like that's not going to be one of the big things that I've realized

90
00:08:07,000 --> 00:08:12,000
is that I'm probably never going to be the head of a monastery

91
00:08:12,000 --> 00:08:15,000
or not in the near future anyway.

92
00:08:15,000 --> 00:08:22,000
It's not very easy for a foreign monk to do in a country like Sri Lanka or Thailand.

93
00:08:22,000 --> 00:08:27,000
And so the only option would be to go back to the West or go to some country

94
00:08:27,000 --> 00:08:33,000
to start a monastery in a European or in a non-Buddhist country,

95
00:08:33,000 --> 00:08:38,000
ironically, that in Buddhist countries they're not very keen to have,

96
00:08:38,000 --> 00:08:44,000
they don't seem very keen any way to have foreign monks become the head of a centre.

97
00:08:44,000 --> 00:08:47,000
Anyway, so lots of announcements. There you are.

98
00:08:47,000 --> 00:08:53,000
That's what's happening here and hopefully that catches everyone up a little bit on what's going on.

99
00:08:53,000 --> 00:09:03,000
The whole video-making scene is not really probably going to pick up much.

100
00:09:03,000 --> 00:09:08,000
I have a few other things on my plate right now and I'm probably not going to be doing much

101
00:09:08,000 --> 00:09:11,000
in the way of new videos. There are a few that I want to make.

102
00:09:11,000 --> 00:09:19,000
I'd like to finish the series on Blessings and there is the how to meditate for kids videos

103
00:09:19,000 --> 00:09:23,000
that I can make now that my computer is working properly.

104
00:09:23,000 --> 00:09:29,000
The graphics card is working properly so I can do those cartoon animations again.

105
00:09:29,000 --> 00:09:34,000
But other than that, I wouldn't expect too much besides this monk radio broadcast.

106
00:09:34,000 --> 00:09:41,000
I'm going to be working on the book, this book project that we put together,

107
00:09:41,000 --> 00:09:49,000
editing all those texts that everyone so kindly transcribed for me.

108
00:09:49,000 --> 00:09:57,000
That's about it. Just hanging out and doing as much meditation as I can

109
00:09:57,000 --> 00:10:04,000
and trying to keep dedicated to the practice and the following the Buddha's teaching.

110
00:10:04,000 --> 00:10:07,000
Those are my announcements and I'm doing it alone now.

111
00:10:07,000 --> 00:10:11,000
As you can see, I have no crowd yet.

112
00:10:11,000 --> 00:10:14,000
Maybe in the future we'll have some more people joining us here.

113
00:10:14,000 --> 00:10:18,000
Be nice now if we could have people come on the air but we'll see how that comes.

114
00:10:18,000 --> 00:10:28,000
Anyway, announcements for tonight.

