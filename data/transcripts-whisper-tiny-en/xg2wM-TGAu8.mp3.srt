1
00:00:00,000 --> 00:00:13,440
Okay, good evening, everyone broadcasting live October 31st, 2050.

2
00:00:13,440 --> 00:00:19,240
Today we are not doing the Dhamapanda.

3
00:00:19,240 --> 00:00:24,240
Unless you can convince me otherwise, I got an idea.

4
00:00:24,240 --> 00:00:33,680
Somehow it seems like a little bit much to be doing a Dhamapandaverse every day on many fronts.

5
00:00:33,680 --> 00:00:42,600
So I've decided to suggest doing the Monday, Wednesday, and Friday, and the rest of the

6
00:00:42,600 --> 00:00:49,680
days I'll just come on and for now I'll just answer people's questions.

7
00:00:49,680 --> 00:00:58,760
That way we don't have too many Dhamapanda videos all at once, and I don't have to do

8
00:00:58,760 --> 00:01:05,720
all the research every day because it gets to be a bit much some days.

9
00:01:05,720 --> 00:01:13,600
Today was our Katina in Stony Creek, so we had a fairly long day there.

10
00:01:13,600 --> 00:01:30,240
I gave a talk, I got to set up on the big monkey chair, I gave a talk about Dhamapatina.

11
00:01:30,240 --> 00:01:40,040
So we'll do questions and then just end it whenever there's no question.

12
00:01:40,040 --> 00:01:46,760
We have questions, I know we left off yesterday that was submitted just as we were signing

13
00:01:46,760 --> 00:01:47,760
off.

14
00:01:47,760 --> 00:01:51,240
What is good form for noting while on a bus?

15
00:01:51,240 --> 00:01:57,600
I noted pushing when the bus is celebrated, thinking when pondering this question, et cetera.

16
00:01:57,600 --> 00:02:11,320
But you can just do sitting meditation, I don't know that I'd acknowledge pushing exactness

17
00:02:11,320 --> 00:02:13,280
when we're feeling.

18
00:02:13,280 --> 00:02:18,800
You can acknowledge pressure, I guess pushing is okay, except for the fact that you're not

19
00:02:18,800 --> 00:02:30,240
doing the pushing is so that you feel a sense of pushing feeling if that's possible.

20
00:02:30,240 --> 00:02:35,120
But yeah, the bus is a good place to do sitting meditation, close your eyes and start with

21
00:02:35,120 --> 00:02:43,080
the rising and falling and then note, note the other things that arise.

22
00:02:43,080 --> 00:02:51,960
Hello, Bhante, what is exactly meant by the three fires in the quote of today, are they

23
00:02:51,960 --> 00:03:16,280
agreed hatred and delusion?

24
00:03:16,280 --> 00:03:21,120
Of course the three realms of existence are also seems fires, I don't think that's

25
00:03:21,120 --> 00:03:27,720
what's meant to you, but actually just a second it may be because it's talking about

26
00:03:27,720 --> 00:03:34,840
us, Nirvana, where there are the three fires, it could very well be, I'm not convinced

27
00:03:34,840 --> 00:03:41,040
that it is, but when in contrast to Nirvana, Nirvana is the fourth realm, so the first three

28
00:03:41,040 --> 00:03:51,480
realms are on fire, that's the central realm, which includes humans and animals, hell beings,

29
00:03:51,480 --> 00:04:02,080
I think, ghosts as well, and then there's the, and also angels, all of the heavens, and

30
00:04:02,080 --> 00:04:12,120
then there's the Rupa, Bhuma, Bhumi, which is the gods that have form, and then there's

31
00:04:12,120 --> 00:04:17,640
the Arupa, Bhumi, which is the gods that have no form, those are the three realms, and

32
00:04:17,640 --> 00:04:28,960
they're also considered to be fires, because they rage on, and they're capable of giving

33
00:04:28,960 --> 00:04:42,320
rise to the ground, associated with meditation, how important is investigation, for example

34
00:04:42,320 --> 00:04:48,480
asking ourselves, why am I thinking this now, why am I sad now, or why am I happy now,

35
00:04:48,480 --> 00:04:49,480
and so on?

36
00:04:49,480 --> 00:04:57,520
Well it's not meditation, I mean it might be interesting for you to know why, but it's

37
00:04:57,520 --> 00:05:04,840
really not the ultimate cause, the ultimate cause is in the moment when you meditate and

38
00:05:04,840 --> 00:05:10,240
you see things arising and ceasing, we're not even all that concerned with the cause,

39
00:05:10,240 --> 00:05:15,840
in the end, we're more concerned with the nature, because it's the nature of things that's

40
00:05:15,840 --> 00:05:24,560
going to turn us off of them, that's going to help us change our causal relationship,

41
00:05:24,560 --> 00:05:35,120
because stop clinging to things that we see are stressful.

42
00:05:35,120 --> 00:05:41,000
Just my own question, I've heard the phrase seven links to awakening, and I think the

43
00:05:41,000 --> 00:05:48,920
first one is usually listed as investigation, but obviously it's not that kind of investigation.

44
00:05:48,920 --> 00:05:51,680
Is that a common term?

45
00:05:51,680 --> 00:05:57,640
I think you're, maybe you're thinking of the seven more jungles, where the second one is,

46
00:05:57,640 --> 00:06:02,000
David Namavicia, which is often translated as investigation.

47
00:06:02,000 --> 00:06:16,280
The ones I remember are investigation, tranquility, joy, energy, and ending with equanimity.

48
00:06:16,280 --> 00:06:24,680
You must, yeah, rapture, not joy, I wouldn't translate BTS rapture, I was joy, that's a poor

49
00:06:24,680 --> 00:06:25,680
translation.

50
00:06:25,680 --> 00:06:26,680
We're talking about that.

51
00:06:26,680 --> 00:06:30,640
Like in the Musudimaga, he was calling it happiness, I think.

52
00:06:30,640 --> 00:06:33,960
This is an odd word for BTS.

53
00:06:33,960 --> 00:06:38,960
So in terms with mindfulness, mindfulness is the first one.

54
00:06:38,960 --> 00:06:47,000
So Namavicia is the ability to sort of the discriminating of dumbers, being able to sort

55
00:06:47,000 --> 00:06:50,280
them out, according to their nature.

56
00:06:50,280 --> 00:06:57,280
These are good, these are bad, these are helpful, these are harmful.

57
00:06:57,280 --> 00:07:01,760
So that's what is certainly not investigation, because that would be a different activity

58
00:07:01,760 --> 00:07:05,400
from what we're intending, we're intending to be mindful.

59
00:07:05,400 --> 00:07:12,160
Namavicia is a quality that comes up when you're mindful.

60
00:07:12,160 --> 00:07:16,440
Maybe that's just someone else's translation or something.

61
00:07:16,440 --> 00:07:17,440
Yeah.

62
00:07:17,440 --> 00:07:21,640
I just, I remember investigation being the first one, but it sounds like it's probably

63
00:07:21,640 --> 00:07:24,720
just a strange translation.

64
00:07:24,720 --> 00:07:39,600
And it's probably better to think of them in the poly.

65
00:07:39,600 --> 00:07:43,360
During the moments when the breath is physically not at the abdomen, but elsewhere, for

66
00:07:43,360 --> 00:07:48,280
example, the chest, should we follow the breath or try to note the feeling or lack of

67
00:07:48,280 --> 00:07:50,560
or lack of feeling at the abdomen?

68
00:07:50,560 --> 00:07:56,560
Follow the feeling, if you feel it in your chest and you can say feeling or knowing that

69
00:07:56,560 --> 00:08:01,960
it's in the chest, just acknowledge the experience and then come back to the abdomen.

70
00:08:01,960 --> 00:08:06,840
It's just, that's just a technical aspect, it takes time to become accustomed, but it's

71
00:08:06,840 --> 00:08:12,280
useful to use the abdomen because once you relax, once you progress in the meditation,

72
00:08:12,280 --> 00:08:13,880
your breath will shift down.

73
00:08:13,880 --> 00:08:19,840
The reason it's in its chest is usually because we're very tense, ordinarily, once you

74
00:08:19,840 --> 00:08:25,120
get into meditation, you relax, and you'll find the breath naturally moves to the abdomen.

75
00:08:25,120 --> 00:08:27,720
But then you'll force it, it's just going to go there.

76
00:08:27,720 --> 00:08:33,120
So in the long run, you're better off learning to acknowledge at the abdomen and put your

77
00:08:33,120 --> 00:08:49,000
hand on it if you can't feel it there.

78
00:08:49,000 --> 00:08:51,560
Physical is saying that you can't hear any audio tonight.

79
00:08:51,560 --> 00:08:55,720
Oh, they wouldn't hear you, that's right.

80
00:08:55,720 --> 00:08:57,720
They probably shouldn't hear me.

81
00:08:57,720 --> 00:09:01,520
And Darwin is watching YouTube and she can't hear it either.

82
00:09:01,520 --> 00:09:03,520
Strange.

83
00:09:03,520 --> 00:09:04,520
Hmm.

84
00:09:04,520 --> 00:09:06,760
That's funny.

85
00:09:06,760 --> 00:09:09,800
But some people can't get there, no, maybe no one can.

86
00:09:09,800 --> 00:09:10,800
Test.

87
00:09:10,800 --> 00:09:11,800
Test.

88
00:09:11,800 --> 00:09:13,600
No, they should be able to hear me.

89
00:09:13,600 --> 00:09:15,600
Do you want to say test, Robin?

90
00:09:15,600 --> 00:09:16,600
Test.

91
00:09:16,600 --> 00:09:22,360
We're not for sure, we're broadcasting.

92
00:09:22,360 --> 00:09:25,080
Why can some people not hear us?

93
00:09:25,080 --> 00:09:29,120
And the audio they wouldn't hear you, I forgot, and about that.

94
00:09:29,120 --> 00:09:33,000
So if you're listening to the audio feed, you're only going to get me.

95
00:09:33,000 --> 00:09:37,240
It's just, it's about to know.

96
00:09:37,240 --> 00:09:48,560
I wonder if anyone can hear it all.

97
00:09:48,560 --> 00:09:58,920
I can't hear any audio tonight, it's one thing I've given it to.

98
00:09:58,920 --> 00:10:03,200
Okay, Anthony can hear on YouTube, good, that's a start.

99
00:10:03,200 --> 00:10:06,040
Maybe it's just the audio stream.

100
00:10:06,040 --> 00:10:19,560
Can only hear his from the stream, I really understand.

101
00:10:19,560 --> 00:10:26,680
That happened a week or so ago, now, with the audio stream, it was all static-y.

102
00:10:26,680 --> 00:10:33,800
No, that was, I was trying to get, trying to copy you in programmatically, it should

103
00:10:33,800 --> 00:10:35,960
work, but I don't know why it's not.

104
00:10:35,960 --> 00:10:58,960
Okay, so YouTube is okay, it's just the audio stream, that's a problem.

105
00:10:58,960 --> 00:11:06,960
Oh, I have to check it, then maybe it's taking it from the wrong source, indeed, it's

106
00:11:06,960 --> 00:11:12,960
taking it from the wrong source test, you know, now it's taking it from the right source

107
00:11:12,960 --> 00:11:22,040
after broadcasting silence for a long period, sorry, have to check that before we start

108
00:11:22,040 --> 00:11:35,840
then, start taking it from the right stream, anyway, there I answered a few questions,

109
00:11:35,840 --> 00:11:36,840
so yes.

110
00:11:36,840 --> 00:11:39,960
Thank you, Bhante.

111
00:11:39,960 --> 00:11:42,520
And that they're up to you again tomorrow.

112
00:11:42,520 --> 00:11:46,120
We've got, what do we have tomorrow, Robin, anything people should know?

113
00:11:46,120 --> 00:11:51,120
Yes, we have our volunteer meeting tomorrow.

114
00:11:51,120 --> 00:12:01,240
Well, we have an admin meeting at 12, the regular volunteer meeting at 1 p.m. Eastern,

115
00:12:01,240 --> 00:12:04,640
and the Sudimaga study group at 2 p.m. Eastern.

116
00:12:04,640 --> 00:12:11,600
You know, that's going to have changed for some people, yes, because Eastern Standard

117
00:12:11,600 --> 00:12:15,400
Time changes, moves back.

118
00:12:15,400 --> 00:12:17,600
We change our clocks tonight, yes.

119
00:12:17,600 --> 00:12:23,000
So ask, just go to Google and type in what time is it in New York, and then you'll be

120
00:12:23,000 --> 00:12:26,480
able to figure out whether it's New York time.

121
00:12:26,480 --> 00:12:33,680
Yes, I put in, for the Sudimaga study group, I put it in in UTC Time for tomorrow, so hopefully

122
00:12:33,680 --> 00:12:36,360
everyone will saw that.

123
00:12:36,360 --> 00:12:37,360
Okay.

124
00:12:37,360 --> 00:12:38,360
Okay.

125
00:12:38,360 --> 00:12:39,360
All right.

126
00:12:39,360 --> 00:12:40,360
Thanks, Robin.

127
00:12:40,360 --> 00:12:41,360
Thank you, Bhante.

128
00:12:41,360 --> 00:12:42,360
Good night.

129
00:12:42,360 --> 00:12:55,280
Good night.

