1
00:00:00,000 --> 00:00:05,420
Okay, welcome everyone to our study of the Dhamapada.

2
00:00:05,420 --> 00:00:13,940
Tonight we'll be continuing with verse number 42, which goes as follows.

3
00:00:13,940 --> 00:00:31,440
Dashong is the only part of our boy training on an spiritual mind.

4
00:00:31,440 --> 00:00:36,220
Dissou, Dissou, Dissou means an enemy.

5
00:00:36,220 --> 00:00:38,940
Dissou, Dissou, Dissou, Dissou, Dissou, Dissou, Dissou, Dissou, Dissou, Dissou, Dissou, Dissou,

6
00:00:38,940 --> 00:00:41,560
would an enemy could do to their enemy.

7
00:00:41,560 --> 00:00:44,620
One enemy could do to another.

8
00:00:44,620 --> 00:00:52,720
Wary, Wap, Anawir, Yinong, or one who is vengeful towards another.

9
00:00:52,720 --> 00:00:41,480
Mihchap, when Ihi, Tang, T

10
00:00:41,480 --> 00:00:57,760
Tang, V, Iil directed mind.

11
00:00:57,760 --> 00:01:02,760
Or the mind that is set wrongly.

12
00:01:02,760 --> 00:01:09,760
Bapio, Nang, Toto, Kati, is of greater evil.

13
00:01:09,760 --> 00:01:16,760
Does greater evil than that?

14
00:01:16,760 --> 00:01:21,760
It's a pretty powerful words, where the meaning is

15
00:01:21,760 --> 00:01:26,760
what an enemy could do to an enemy, or someone who is seeking vengeance.

16
00:01:26,760 --> 00:01:30,760
Anyone who could ever seek vengeance upon someone else,

17
00:01:30,760 --> 00:01:33,760
Greek vengeance upon another person.

18
00:01:33,760 --> 00:01:38,760
The Buddha said, it's a pretty strong words, that it's not possible.

19
00:01:38,760 --> 00:01:45,760
Basically, for them to do as much damage as the ill-directed mind.

20
00:01:45,760 --> 00:01:48,760
That's worth listening to, though.

21
00:01:48,760 --> 00:01:50,760
So, but first, we have a story.

22
00:01:50,760 --> 00:01:52,760
It's not much of a story.

23
00:01:52,760 --> 00:01:54,760
This story is quite a short one.

24
00:01:54,760 --> 00:01:59,760
The story goes that Anatapindika, this great

25
00:01:59,760 --> 00:02:03,760
lay disciple of the Buddha, who donated so much,

26
00:02:03,760 --> 00:02:08,760
and donated Jita, one of his Buddhist monastery for the Buddha,

27
00:02:08,760 --> 00:02:16,760
had a cattle herder,

28
00:02:16,760 --> 00:02:21,760
a person who looks after his cows.

29
00:02:21,760 --> 00:02:27,760
This cattle herder, he, from time to time,

30
00:02:27,760 --> 00:02:31,760
would go with Anatapindika to listen to the Buddha teach.

31
00:02:31,760 --> 00:02:38,760
He gained great faith in the Buddha,

32
00:02:38,760 --> 00:02:41,760
and so he invited the Buddha to come to his house,

33
00:02:41,760 --> 00:02:46,760
to receive charity,

34
00:02:46,760 --> 00:02:51,760
or receiving some kind of gift from him.

35
00:02:51,760 --> 00:02:53,760
He thought that, wow, this is someone worth supporting,

36
00:02:53,760 --> 00:02:58,760
so he decided he wanted to provide some requisites to the Buddha.

37
00:02:58,760 --> 00:03:03,760
And the Buddha's seeing that there was this guy had some

38
00:03:03,760 --> 00:03:05,760
strong good qualities in him,

39
00:03:05,760 --> 00:03:09,760
and the potential to understand the Dhamma.

40
00:03:09,760 --> 00:03:14,760
Except at the invitation, and walked all the way to this man's,

41
00:03:14,760 --> 00:03:19,760
his cattle herder's house.

42
00:03:19,760 --> 00:03:23,760
And for seven days, as I understand for seven days,

43
00:03:23,760 --> 00:03:26,760
he spent, he went every day,

44
00:03:26,760 --> 00:03:34,760
to receive the five products of a cow,

45
00:03:34,760 --> 00:03:39,760
so he got milk and butter and cheese and so on.

46
00:03:39,760 --> 00:03:42,760
All of these good things that come from the cow,

47
00:03:42,760 --> 00:03:44,760
he had these cows that he looked after,

48
00:03:44,760 --> 00:03:50,760
and he was able to provide the Buddha with dairy products.

49
00:03:50,760 --> 00:03:53,760
And the story goes that he offered these to the Buddha,

50
00:03:53,760 --> 00:03:55,760
seven days in the row.

51
00:03:55,760 --> 00:03:58,760
And on the seventh day,

52
00:03:58,760 --> 00:04:01,760
he was taking the Buddha back to,

53
00:04:01,760 --> 00:04:04,760
he invited the Buddha for seven days,

54
00:04:04,760 --> 00:04:07,760
and he was going to follow the Buddha back to Jetahana,

55
00:04:07,760 --> 00:04:10,760
and he carried the Buddha's bowl.

56
00:04:10,760 --> 00:04:13,760
Now they got halfway through the forest,

57
00:04:13,760 --> 00:04:17,760
on the way back to Sawati, I guess.

58
00:04:17,760 --> 00:04:21,760
And suddenly, the Buddha turned around

59
00:04:21,760 --> 00:04:23,760
and received his bowl from Nanda,

60
00:04:23,760 --> 00:04:25,760
Nanda was his name.

61
00:04:25,760 --> 00:04:29,760
This is Gopalada, the cattle herder.

62
00:04:29,760 --> 00:04:31,760
And he received his bowl from him and said,

63
00:04:31,760 --> 00:04:34,760
Nanda, please turn back.

64
00:04:34,760 --> 00:04:36,760
Don't come any further.

65
00:04:36,760 --> 00:04:39,760
We'll go by ourselves.

66
00:04:39,760 --> 00:04:42,760
And the Buddha turned it and walked away,

67
00:04:42,760 --> 00:04:45,760
and Nanda turned around to go home.

68
00:04:45,760 --> 00:04:47,760
And on his way home,

69
00:04:47,760 --> 00:04:50,760
some of the monks were coming later.

70
00:04:50,760 --> 00:04:54,760
They had left the house later, I guess, after the Buddha.

71
00:04:54,760 --> 00:04:59,760
And notice that what happened was Nanda turned around,

72
00:04:59,760 --> 00:05:02,760
and as he was walking back to the forest,

73
00:05:02,760 --> 00:05:07,760
he ran into a hunter.

74
00:05:07,760 --> 00:05:09,760
There was a hunter who saw him going through the forest,

75
00:05:09,760 --> 00:05:11,760
and thought he was a deer or something,

76
00:05:11,760 --> 00:05:12,760
and shot him and killed him.

77
00:05:12,760 --> 00:05:16,760
Shot him with a more narrow and killed him.

78
00:05:16,760 --> 00:05:18,760
And some of the monks noticed this.

79
00:05:18,760 --> 00:05:20,760
The hunter realized he was wrong,

80
00:05:20,760 --> 00:05:23,760
and I guess just ran away.

81
00:05:23,760 --> 00:05:26,760
But some of the monks saw this,

82
00:05:26,760 --> 00:05:31,760
that he was a guy who had done so much good deeds.

83
00:05:31,760 --> 00:05:34,760
And it's almost as though the Buddha knew

84
00:05:34,760 --> 00:05:38,760
what was going to happen.

85
00:05:38,760 --> 00:05:41,760
The reason the story is so well remembered,

86
00:05:41,760 --> 00:05:43,760
and it's actually in the commentaries,

87
00:05:43,760 --> 00:05:46,760
is because it's a curious story.

88
00:05:46,760 --> 00:05:51,760
Here we have someone that the Buddha spent seven days with.

89
00:05:51,760 --> 00:05:55,760
And it's almost as though the Buddha knew.

90
00:05:55,760 --> 00:05:57,760
And of course, there was this idea going around

91
00:05:57,760 --> 00:06:00,760
that the Buddha was omniscient.

92
00:06:00,760 --> 00:06:03,760
So he wouldn't have sent Nanda back

93
00:06:03,760 --> 00:06:05,760
if he didn't know what was going to happen.

94
00:06:05,760 --> 00:06:08,760
And yet, he purposefully sent Nanda back

95
00:06:08,760 --> 00:06:10,760
kind of like as though he knew that

96
00:06:10,760 --> 00:06:13,760
he was going to get shot by the sunder.

97
00:06:13,760 --> 00:06:16,760
This is the curious part of the story,

98
00:06:16,760 --> 00:06:19,760
and what leads to this verse.

99
00:06:19,760 --> 00:06:21,760
And so the monks are kind of like,

100
00:06:21,760 --> 00:06:23,760
and they heard that the Buddha had sent him back

101
00:06:23,760 --> 00:06:25,760
and they went and they're kind of like,

102
00:06:25,760 --> 00:06:28,760
I don't know how to approach this,

103
00:06:28,760 --> 00:06:31,760
but you sent Lord Buddha,

104
00:06:31,760 --> 00:06:36,760
you sent Nanda back knowing that

105
00:06:36,760 --> 00:06:41,760
did you know that this was going to happen?

106
00:06:41,760 --> 00:06:43,760
If you hadn't have sent him back,

107
00:06:43,760 --> 00:06:45,760
he wouldn't have gotten shot by the hunter.

108
00:06:45,760 --> 00:06:47,760
So what's up?

109
00:06:47,760 --> 00:06:50,760
And the Buddha said,

110
00:06:50,760 --> 00:06:53,760
if I hadn't, whether I told him to go

111
00:06:53,760 --> 00:06:55,760
or didn't tell him to go,

112
00:06:55,760 --> 00:06:57,760
whether he had gone north,

113
00:06:57,760 --> 00:07:01,760
he's less than any of the eight,

114
00:07:01,760 --> 00:07:02,760
the four directions or the four minor directions,

115
00:07:02,760 --> 00:07:03,760
wherever he had gone,

116
00:07:03,760 --> 00:07:06,760
there's no way he could have escaped

117
00:07:06,760 --> 00:07:11,760
from the effects of his bad karma.

118
00:07:15,760 --> 00:07:18,760
And then the Buddha taught this verse.

119
00:07:18,760 --> 00:07:21,760
And he said,

120
00:07:21,760 --> 00:07:24,760
and as he's taught elsewhere,

121
00:07:24,760 --> 00:07:27,760
you can't escape this karma,

122
00:07:27,760 --> 00:07:31,760
the point being that you don't,

123
00:07:31,760 --> 00:07:33,760
the blame that we might place

124
00:07:33,760 --> 00:07:40,760
on an enemy or on someone seeking vengeance against us

125
00:07:40,760 --> 00:07:44,760
is much better placed or is properly placed

126
00:07:44,760 --> 00:07:46,760
on our own mind,

127
00:07:46,760 --> 00:07:50,760
which has caused us to arise

128
00:07:50,760 --> 00:07:51,760
in such a situation,

129
00:07:51,760 --> 00:07:53,760
causes us to give rise to such a situation.

130
00:07:53,760 --> 00:07:56,760
So it's really an interesting story

131
00:07:56,760 --> 00:08:05,760
because it requires a sort of a profound understanding of karma,

132
00:08:05,760 --> 00:08:13,760
not just on a moment to moment level,

133
00:08:13,760 --> 00:08:15,760
but you have to somehow see

134
00:08:15,760 --> 00:08:18,760
that there's a relationship between this hunter,

135
00:08:18,760 --> 00:08:22,760
killing this cattle hunter.

136
00:08:22,760 --> 00:08:27,760
And something that the cattle hunter has,

137
00:08:27,760 --> 00:08:30,760
I guess, done in the past probably to this hunter.

138
00:08:30,760 --> 00:08:33,760
And the even more curious thing about this is

139
00:08:33,760 --> 00:08:35,760
we don't know what it was,

140
00:08:35,760 --> 00:08:36,760
because as with most of the stories,

141
00:08:36,760 --> 00:08:38,760
then the Buddha says,

142
00:08:38,760 --> 00:08:41,760
in the past this man did this and this and this,

143
00:08:41,760 --> 00:08:43,760
but the curious note that we have

144
00:08:43,760 --> 00:08:45,760
is at the end of the story, the commentary says,

145
00:08:45,760 --> 00:08:47,760
but the monks never asked the Buddha

146
00:08:47,760 --> 00:08:49,760
would he hit with this guy had done,

147
00:08:49,760 --> 00:08:53,760
so we know, so we don't know what the karma was.

148
00:08:53,760 --> 00:08:55,760
It's actually a unique story in that way,

149
00:08:55,760 --> 00:08:57,760
because normally the monks go up to the Buddha

150
00:08:57,760 --> 00:08:59,760
and ask him, what did Nanda do,

151
00:08:59,760 --> 00:09:02,760
what did Nanda go Pala do to deserve that?

152
00:09:02,760 --> 00:09:05,760
And in this case, they didn't.

153
00:09:05,760 --> 00:09:09,760
So first of all, it talks about karma.

154
00:09:09,760 --> 00:09:12,760
And it's sort of an example,

155
00:09:12,760 --> 00:09:16,760
a strong example of how this verse

156
00:09:16,760 --> 00:09:20,760
comes into play, how this verse,

157
00:09:20,760 --> 00:09:22,760
or the truth of this verse,

158
00:09:22,760 --> 00:09:25,760
how this verse has meaning.

159
00:09:25,760 --> 00:09:30,760
Because the point being

160
00:09:30,760 --> 00:09:33,760
that not only will your ill-directed mind

161
00:09:33,760 --> 00:09:35,760
hurt you on a momentary level,

162
00:09:35,760 --> 00:09:37,760
like if you get angry, you're in pain

163
00:09:37,760 --> 00:09:39,760
or if you cling to things,

164
00:09:39,760 --> 00:09:41,760
you're stressed out about them.

165
00:09:41,760 --> 00:09:45,760
But it has profound reaching effects

166
00:09:45,760 --> 00:09:47,760
that can lead over into the next life

167
00:09:47,760 --> 00:09:50,760
and can chase you throughout your journey

168
00:09:50,760 --> 00:09:54,760
in some sara because of how they affect the mind.

169
00:09:58,760 --> 00:10:04,760
So it actually, you know,

170
00:10:04,760 --> 00:10:06,760
you would say maybe requires a little bit of faith,

171
00:10:06,760 --> 00:10:12,760
and people will start to question this idea of karma

172
00:10:12,760 --> 00:10:17,760
and say, well, here we're entering into a realm

173
00:10:17,760 --> 00:10:20,760
that is outside of the core of the Buddha's teaching,

174
00:10:20,760 --> 00:10:23,760
because it's starting to deal with faith

175
00:10:23,760 --> 00:10:31,760
and the leaf in some sort of magical,

176
00:10:31,760 --> 00:10:34,760
karmic potency for the acts

177
00:10:34,760 --> 00:10:36,760
that when you kill someone,

178
00:10:36,760 --> 00:10:38,760
you magically get punished for it,

179
00:10:38,760 --> 00:10:41,760
or that the things that we have done

180
00:10:41,760 --> 00:10:44,760
have some connection,

181
00:10:44,760 --> 00:10:47,760
unseen connection to things in the past,

182
00:10:47,760 --> 00:10:51,760
but all that really shows,

183
00:10:51,760 --> 00:10:53,760
if you understand how Buddhism, how karma works,

184
00:10:53,760 --> 00:10:55,760
how rebirth works, all that really shows

185
00:10:55,760 --> 00:11:00,760
is they profound and intrinsic connection

186
00:11:00,760 --> 00:11:03,760
between moments of consciousness,

187
00:11:03,760 --> 00:11:11,760
of consciousness between our acts and the world around us.

188
00:11:11,760 --> 00:11:14,760
So like they'll say about the weather,

189
00:11:14,760 --> 00:11:16,760
they've come to realize it's about the weather

190
00:11:16,760 --> 00:11:18,760
to have small things in one part of the world

191
00:11:18,760 --> 00:11:21,760
can have a profound impact on other parts of the world.

192
00:11:21,760 --> 00:11:24,760
They have this saying which is probably not true

193
00:11:24,760 --> 00:11:27,760
that if a butterfly flaps its wings in China,

194
00:11:27,760 --> 00:11:29,760
there's an earthquake in America,

195
00:11:29,760 --> 00:11:32,760
or there's a tornado in America.

196
00:11:32,760 --> 00:11:36,760
The point being that it's very easy

197
00:11:36,760 --> 00:11:39,760
for things to change,

198
00:11:39,760 --> 00:11:43,760
and something very small or very specific

199
00:11:43,760 --> 00:11:47,760
can have an effect on the long term.

200
00:11:47,760 --> 00:11:50,760
It's not difficult to understand really at all

201
00:11:50,760 --> 00:11:54,760
when you think about how a act affects your mind

202
00:11:54,760 --> 00:11:58,760
and how your mind affects your future.

203
00:11:58,760 --> 00:12:01,760
So if you have done bad deeds in the past,

204
00:12:01,760 --> 00:12:06,760
if you've heard other people, or if you've clung to,

205
00:12:06,760 --> 00:12:08,760
if you're clinging to things,

206
00:12:08,760 --> 00:12:12,760
it's going to change your whole attitude towards life.

207
00:12:12,760 --> 00:12:14,760
It's going to affect your mind.

208
00:12:14,760 --> 00:12:17,760
It's something you'll think about on a daily basis.

209
00:12:17,760 --> 00:12:20,760
The Buddha said, whatever you think about,

210
00:12:20,760 --> 00:12:22,760
that's what your mind inclines towards.

211
00:12:22,760 --> 00:12:24,760
It's a very obvious sort of teaching.

212
00:12:24,760 --> 00:12:26,760
Something we don't think about,

213
00:12:26,760 --> 00:12:28,760
because we don't really understand karma.

214
00:12:28,760 --> 00:12:33,760
We don't really get that our thoughts affect our lives.

215
00:12:33,760 --> 00:12:37,760
And yet they absolutely do.

216
00:12:37,760 --> 00:12:39,760
When we're stressed about something,

217
00:12:39,760 --> 00:12:42,760
it changes all of our interactions with other people

218
00:12:42,760 --> 00:12:44,760
throughout the life, with the world around us,

219
00:12:44,760 --> 00:12:45,760
throughout our life.

220
00:12:45,760 --> 00:12:48,760
It changes even the direction of our lives.

221
00:12:48,760 --> 00:12:52,760
People who have gone through traumatic experiences.

222
00:12:52,760 --> 00:12:54,760
Of course, it changes their whole life.

223
00:12:54,760 --> 00:12:55,760
It changes their outlook.

224
00:12:55,760 --> 00:13:00,760
It changes their potential,

225
00:13:00,760 --> 00:13:05,760
their possibilities in their lives.

226
00:13:05,760 --> 00:13:10,760
The things that we do, the things that we engage in,

227
00:13:10,760 --> 00:13:12,760
change us.

228
00:13:12,760 --> 00:13:14,760
People who are addicted to things have to spend money

229
00:13:14,760 --> 00:13:17,760
and put resources into the things that they want.

230
00:13:17,760 --> 00:13:19,760
And that they might have put elsewhere.

231
00:13:19,760 --> 00:13:22,760
They have to put time, dedicate time to their addiction.

232
00:13:22,760 --> 00:13:29,760
And energy and brain cells and thoughts and so on.

233
00:13:29,760 --> 00:13:34,760
And so it actually gets,

234
00:13:34,760 --> 00:13:37,760
the effect can actually get bigger over the long term,

235
00:13:37,760 --> 00:13:40,760
so that the effect that karma has in the short term

236
00:13:40,760 --> 00:13:43,760
might be minuscule compared to the effect

237
00:13:43,760 --> 00:13:47,760
that it has later on in life or even into the next life.

238
00:13:47,760 --> 00:13:49,760
Because of course, as Buddhists,

239
00:13:49,760 --> 00:13:53,760
we're not thinking of the next life as something separate

240
00:13:53,760 --> 00:13:54,760
from this life.

241
00:13:54,760 --> 00:13:56,760
It's a continuation.

242
00:13:56,760 --> 00:14:00,760
There's a shift at the moment in death,

243
00:14:00,760 --> 00:14:03,760
but only because we've entered into this state

244
00:14:03,760 --> 00:14:05,760
of being born and died,

245
00:14:05,760 --> 00:14:09,760
this human state that is very coarse

246
00:14:09,760 --> 00:14:15,760
and requires or associates with a physical body.

247
00:14:15,760 --> 00:14:17,760
Actually, at the moment of death,

248
00:14:17,760 --> 00:14:20,760
it happens. Life just continues.

249
00:14:20,760 --> 00:14:26,760
And our mind just continues.

250
00:14:26,760 --> 00:14:29,760
So that's the part that has to do with karma.

251
00:14:29,760 --> 00:14:37,760
When we're talking about the mind and the potency of the mind,

252
00:14:37,760 --> 00:14:42,760
why is the mind so potent in creating karma?

253
00:14:42,760 --> 00:14:45,760
Once we accept the fact that our bad deeds

254
00:14:45,760 --> 00:14:51,760
have an effect, why would it be that the deeds of others

255
00:14:51,760 --> 00:14:58,760
are less potent than our own minds?

256
00:14:58,760 --> 00:15:03,760
And further, why is it the mind that the Buddha is focusing on

257
00:15:03,760 --> 00:15:04,760
and not the deeds?

258
00:15:04,760 --> 00:15:06,760
So people are still asking me,

259
00:15:10,760 --> 00:15:12,760
why is it that when you do something,

260
00:15:12,760 --> 00:15:14,760
it doesn't have karmic potency?

261
00:15:14,760 --> 00:15:17,760
We have this idea that karma is some kind of magic,

262
00:15:17,760 --> 00:15:20,760
because we've been brought up generally in the West,

263
00:15:20,760 --> 00:15:24,760
and we've been brought up to think in terms of God,

264
00:15:24,760 --> 00:15:29,760
to think in terms of nature as having some kind of watchful

265
00:15:29,760 --> 00:15:39,760
or some kind of power to affect and to intervene

266
00:15:39,760 --> 00:15:43,760
with our lives.

267
00:15:43,760 --> 00:15:46,760
So we think when you step on an end,

268
00:15:46,760 --> 00:15:48,760
bad, you've done a bad deed, right?

269
00:15:48,760 --> 00:15:52,760
Because we're used to sin being something that God decides.

270
00:15:52,760 --> 00:15:54,760
Is it sinful or is it right?

271
00:15:54,760 --> 00:15:59,760
And he's watching you like Santa Claus.

272
00:15:59,760 --> 00:16:03,760
But karma is absolutely not that.

273
00:16:03,760 --> 00:16:07,760
Karma is the effect that something has on your mind,

274
00:16:07,760 --> 00:16:12,760
or the effect that something has on the choices you make

275
00:16:12,760 --> 00:16:16,760
and on your state of mind, into the future.

276
00:16:16,760 --> 00:16:19,760
And that which has the biggest, the most profound effect,

277
00:16:19,760 --> 00:16:24,760
or really the only effect is your own mind.

278
00:16:24,760 --> 00:16:28,760
Another person, the harm that an enemy can do to you,

279
00:16:28,760 --> 00:16:36,760
or someone who wants to take out their vengeance on you,

280
00:16:36,760 --> 00:16:42,760
is actually absolutely zero.

281
00:16:42,760 --> 00:16:46,760
The harm that they can perform directly upon you is none.

282
00:16:46,760 --> 00:16:48,760
It's not even little.

283
00:16:48,760 --> 00:16:49,760
It's not a little.

284
00:16:49,760 --> 00:16:51,760
It's not something you can mitigate.

285
00:16:51,760 --> 00:16:52,760
It's zero.

286
00:16:52,760 --> 00:16:53,760
They can't.

287
00:16:53,760 --> 00:16:54,760
No one can hurt you.

288
00:16:54,760 --> 00:16:58,760
No one can cause harm to another.

289
00:16:58,760 --> 00:17:01,760
So what the Buddha says is actually an understatement of the fact.

290
00:17:01,760 --> 00:17:04,760
There's no comparison between the harm

291
00:17:04,760 --> 00:17:07,760
that an enemy can do to you,

292
00:17:07,760 --> 00:17:12,760
and the harm that you can do to yourself.

293
00:17:12,760 --> 00:17:16,760
And this is due to the theory,

294
00:17:16,760 --> 00:17:21,760
or the Buddhist theory,

295
00:17:21,760 --> 00:17:25,760
which is accepted and not accepted by many of you,

296
00:17:25,760 --> 00:17:32,760
that the mind is apart from the body.

297
00:17:32,760 --> 00:17:35,760
That it's not a physical process.

298
00:17:35,760 --> 00:17:41,760
You're not locked into your environment.

299
00:17:41,760 --> 00:17:45,760
We react to the mind reacts to the environment.

300
00:17:45,760 --> 00:17:51,760
It reacts either in a positive or a negative or a neutral way.

301
00:17:51,760 --> 00:17:54,760
So when we experience something painful,

302
00:17:54,760 --> 00:17:57,760
we generally tend to react in a negative way.

303
00:17:57,760 --> 00:17:59,760
When we experience something pleasant,

304
00:17:59,760 --> 00:18:01,760
we react in a positive way.

305
00:18:01,760 --> 00:18:05,760
The body reacts, but the mind also reacts.

306
00:18:05,760 --> 00:18:12,760
And the key to understanding and to practicing Buddhism

307
00:18:12,760 --> 00:18:15,760
is realizing that these two things are separate.

308
00:18:15,760 --> 00:18:20,760
That the mind need not react in a negative or a positive way.

309
00:18:20,760 --> 00:18:25,760
The mind can react in an objective way to reality.

310
00:18:25,760 --> 00:18:31,760
If the mind is, if and when the mind is aware of the pain,

311
00:18:31,760 --> 00:18:36,760
that's just pain, then the pain will not harm the mind.

312
00:18:36,760 --> 00:18:40,760
If when someone yells at us or scolds us,

313
00:18:40,760 --> 00:18:43,760
and we see it simply as hearing,

314
00:18:43,760 --> 00:18:47,760
we understand it to be sound arising in the mind,

315
00:18:47,760 --> 00:18:58,760
in the ear, then we and observation proves this.

316
00:18:58,760 --> 00:19:02,760
Or if you test this hypothesis,

317
00:19:02,760 --> 00:19:04,760
if you test it out for yourself,

318
00:19:04,760 --> 00:19:06,760
you can see this for yourself.

319
00:19:06,760 --> 00:19:09,760
If you listen to the sound of my voice,

320
00:19:09,760 --> 00:19:14,760
and just take it for sound, hearing, hearing.

321
00:19:14,760 --> 00:19:19,760
Then you find that any other judgements

322
00:19:19,760 --> 00:19:22,760
or thoughts you might have about what I'm saying disappear.

323
00:19:22,760 --> 00:19:26,760
Any boredom or aversion to what I'm saying

324
00:19:26,760 --> 00:19:30,760
or any attachment or desire or appreciation

325
00:19:30,760 --> 00:19:33,760
of what I'm saying melts away.

326
00:19:33,760 --> 00:19:35,760
And you simply know it for sound.

327
00:19:35,760 --> 00:19:39,760
And you find that suddenly you're in the realm of peace

328
00:19:39,760 --> 00:19:42,760
and of contentment.

329
00:19:42,760 --> 00:19:46,760
You're unshaken by what the person is saying.

330
00:19:46,760 --> 00:19:49,760
If they're saying something nasty or unpleasant,

331
00:19:49,760 --> 00:19:57,760
even if they're hitting you or beating you.

332
00:19:57,760 --> 00:20:00,760
It's actually the only way to be free from suffering

333
00:20:00,760 --> 00:20:06,760
is to free yourself from within,

334
00:20:06,760 --> 00:20:09,760
because you can't be free from enemies,

335
00:20:09,760 --> 00:20:14,760
you can't be free from harsh speech.

336
00:20:14,760 --> 00:20:18,760
You can't be free from the cold and heat and hunger and thirst

337
00:20:18,760 --> 00:20:21,760
and sickness and all the age and death.

338
00:20:21,760 --> 00:20:23,760
We can't be free from these things.

339
00:20:23,760 --> 00:20:29,760
So there's only two ways to be free from suffering.

340
00:20:29,760 --> 00:20:31,760
It's not have any of these things,

341
00:20:31,760 --> 00:20:35,760
which are ubiquitous, which are intrinsic part of life

342
00:20:35,760 --> 00:20:40,760
or overcome them, be free from free yourself,

343
00:20:40,760 --> 00:20:41,760
free your mind from them.

344
00:20:41,760 --> 00:20:43,760
And this is what the Buddha is referring to.

345
00:20:43,760 --> 00:20:46,760
This is the key theory that we have in Buddhism.

346
00:20:46,760 --> 00:20:50,760
If you need to accept this

347
00:20:50,760 --> 00:20:54,760
in order to practice Buddhism correctly.

348
00:20:54,760 --> 00:20:58,760
It's key to understand that you can change

349
00:20:58,760 --> 00:21:00,760
the way you react to reality.

350
00:21:00,760 --> 00:21:02,760
You need not react.

351
00:21:02,760 --> 00:21:05,760
You can interact objectively with reality.

352
00:21:05,760 --> 00:21:07,760
This is why the Buddha said,

353
00:21:07,760 --> 00:21:11,760
the mind that is set wrongly,

354
00:21:11,760 --> 00:21:15,760
the mind that is set in a bad way

355
00:21:15,760 --> 00:21:18,760
is of much greater,

356
00:21:18,760 --> 00:21:23,760
or a incomparably greater harm to the individual

357
00:21:23,760 --> 00:21:27,760
than anyone else could be than an enemy could be.

358
00:21:27,760 --> 00:21:32,760
And furthermore,

359
00:21:32,760 --> 00:21:34,760
what this means is,

360
00:21:34,760 --> 00:21:36,760
this means actually more than just karma.

361
00:21:36,760 --> 00:21:38,760
What the Buddha is talking about here is not just

362
00:21:38,760 --> 00:21:39,760
someone doing a bad deed.

363
00:21:39,760 --> 00:21:43,760
What he's talking about is setting the mind in a bad way,

364
00:21:43,760 --> 00:21:47,760
which is actually even worse than doing a bad deed.

365
00:21:47,760 --> 00:21:50,760
So up until now we're just talking about bad karma.

366
00:21:50,760 --> 00:21:52,760
But even worse than bad karma.

367
00:21:52,760 --> 00:21:54,760
There's something even worse than that.

368
00:21:54,760 --> 00:21:57,760
If you kill someone, if you steal, if you do bad things,

369
00:21:57,760 --> 00:22:00,760
if setting your mind in a bad way,

370
00:22:00,760 --> 00:22:02,760
meet a Japanese person,

371
00:22:02,760 --> 00:22:08,760
but it means set or sent in a wrong way.

372
00:22:08,760 --> 00:22:10,760
It refers, you can think,

373
00:22:10,760 --> 00:22:12,760
it refers to wrong view.

374
00:22:12,760 --> 00:22:14,760
So there are three kinds of wrong.

375
00:22:14,760 --> 00:22:19,760
One is wrong perception,

376
00:22:19,760 --> 00:22:23,760
and the third is wrong view.

377
00:22:23,760 --> 00:22:25,760
Wrong view is the worst.

378
00:22:25,760 --> 00:22:28,760
Wrong perception is the least serious.

379
00:22:28,760 --> 00:22:32,760
If someone says something to you and you get angry right away,

380
00:22:32,760 --> 00:22:33,760
that's wrong perception.

381
00:22:33,760 --> 00:22:35,760
You perceive it as bad.

382
00:22:35,760 --> 00:22:37,760
But when you start thinking about it,

383
00:22:37,760 --> 00:22:38,760
that starts to get worse.

384
00:22:38,760 --> 00:22:39,760
You think, oh,

385
00:22:39,760 --> 00:22:40,760
what did that person do?

386
00:22:40,760 --> 00:22:42,760
That was really mean of them and so on.

387
00:22:42,760 --> 00:22:45,760
Really nasty of them.

388
00:22:45,760 --> 00:22:47,760
That's worse because then you start to think

389
00:22:47,760 --> 00:22:50,760
how you're going to hurt them and hurt them back and so on.

390
00:22:50,760 --> 00:22:54,760
When you have the view, when you set yourself in the idea,

391
00:22:54,760 --> 00:22:55,760
that person is evil.

392
00:22:55,760 --> 00:22:57,760
I didn't deserve that.

393
00:22:57,760 --> 00:22:59,760
They deserve to be punished.

394
00:22:59,760 --> 00:23:02,760
When you have the wrong views,

395
00:23:02,760 --> 00:23:04,760
because we can all,

396
00:23:04,760 --> 00:23:05,760
even as Buddhists,

397
00:23:05,760 --> 00:23:07,760
we all have these first two.

398
00:23:07,760 --> 00:23:09,760
But we need not have the third.

399
00:23:09,760 --> 00:23:11,760
And as Buddhists, we generally don't.

400
00:23:11,760 --> 00:23:13,760
Because this is what the teaching,

401
00:23:13,760 --> 00:23:14,760
what we're taught,

402
00:23:14,760 --> 00:23:15,760
we're taught that,

403
00:23:15,760 --> 00:23:18,760
no, it's not good for you to hurt other people.

404
00:23:18,760 --> 00:23:20,760
Whether someone say,

405
00:23:20,760 --> 00:23:22,760
treat other people well,

406
00:23:22,760 --> 00:23:24,760
not because they deserve to be happy,

407
00:23:24,760 --> 00:23:26,760
but because you do.

408
00:23:26,760 --> 00:23:28,760
I think it's very pertinent.

409
00:23:28,760 --> 00:23:29,760
I mean, of course,

410
00:23:29,760 --> 00:23:30,760
you can think they deserve to be happy,

411
00:23:30,760 --> 00:23:32,760
but that's not the real reason.

412
00:23:32,760 --> 00:23:33,760
The real reason is,

413
00:23:33,760 --> 00:23:34,760
you know, look,

414
00:23:34,760 --> 00:23:36,760
if you get angry at them,

415
00:23:36,760 --> 00:23:37,760
it's not good for you.

416
00:23:37,760 --> 00:23:40,760
You don't get anywhere by hurting other people.

417
00:23:40,760 --> 00:23:42,760
You don't help them, obviously.

418
00:23:42,760 --> 00:23:44,760
You don't help yourself.

419
00:23:44,760 --> 00:23:47,760
And it's pertinent because it's how the mind reacts.

420
00:23:47,760 --> 00:23:49,760
The mind is looking for happiness.

421
00:23:49,760 --> 00:23:52,760
It's not really looking for happiness in other people.

422
00:23:52,760 --> 00:23:54,760
We help other people because it makes us happy.

423
00:23:54,760 --> 00:23:56,760
If it didn't make us happy,

424
00:23:56,760 --> 00:24:03,760
it wouldn't excite the mind.

425
00:24:03,760 --> 00:24:07,760
The mind wouldn't be pleased to continue it.

426
00:24:07,760 --> 00:24:12,760
So, if you want to really encourage your mind

427
00:24:12,760 --> 00:24:14,760
and really get your heart,

428
00:24:14,760 --> 00:24:15,760
the attention of your heart,

429
00:24:15,760 --> 00:24:17,760
you have to put it this way and say,

430
00:24:17,760 --> 00:24:20,760
look, let's not get angry because not because

431
00:24:20,760 --> 00:24:21,760
they don't deserve it,

432
00:24:21,760 --> 00:24:22,760
but because we don't deserve it.

433
00:24:22,760 --> 00:24:25,760
We don't deserve to be to suffer from this,

434
00:24:25,760 --> 00:24:28,760
so that I stop our thoughts.

435
00:24:28,760 --> 00:24:30,760
Because we have this idea,

436
00:24:30,760 --> 00:24:32,760
because we have this understanding,

437
00:24:32,760 --> 00:24:34,760
and this is the understanding we try to give to

438
00:24:34,760 --> 00:24:36,760
Buddhists and to meditators,

439
00:24:36,760 --> 00:24:39,760
we're free from this wrongly directed mind.

440
00:24:39,760 --> 00:24:41,760
Most of the time,

441
00:24:41,760 --> 00:24:44,760
still we have to always be on guard.

442
00:24:44,760 --> 00:24:46,760
When we're in training,

443
00:24:46,760 --> 00:24:49,760
this is something important to understand.

444
00:24:49,760 --> 00:24:51,760
Guard against your mind,

445
00:24:51,760 --> 00:24:52,760
first of all,

446
00:24:52,760 --> 00:24:55,760
from having bad perception.

447
00:24:55,760 --> 00:24:58,760
At the very core,

448
00:24:58,760 --> 00:25:02,760
you're best off when you can just perceive things as they are,

449
00:25:02,760 --> 00:25:06,760
and not misperceive things as good as bad as me,

450
00:25:06,760 --> 00:25:10,760
as mine, as right, as wrong.

451
00:25:10,760 --> 00:25:12,760
But even when you do,

452
00:25:12,760 --> 00:25:14,760
you're fearful not to let it come into your thoughts.

453
00:25:14,760 --> 00:25:16,760
Be aware of the anger,

454
00:25:16,760 --> 00:25:17,760
be aware of the wanting,

455
00:25:17,760 --> 00:25:20,760
the aware of the desire.

456
00:25:20,760 --> 00:25:24,760
Don't let your mind start thinking about how you're going to get what you want,

457
00:25:24,760 --> 00:25:27,760
how you're going to chase away what you don't want.

458
00:25:27,760 --> 00:25:29,760
And if it does come to thoughts,

459
00:25:29,760 --> 00:25:30,760
try to be aware of the thoughts,

460
00:25:30,760 --> 00:25:32,760
and don't let them become views.

461
00:25:32,760 --> 00:25:34,760
And think, yeah, I deserve that.

462
00:25:34,760 --> 00:25:36,760
That's right for me to get that.

463
00:25:36,760 --> 00:25:39,760
Or I don't deserve that.

464
00:25:39,760 --> 00:25:42,760
And I, this is wrong in these, and I'm being unjustly treated,

465
00:25:42,760 --> 00:25:44,760
and so on.

466
00:25:44,760 --> 00:25:45,760
Not because they don't deserve it,

467
00:25:45,760 --> 00:25:48,760
because you don't deserve it.

468
00:25:48,760 --> 00:25:50,760
Because we don't want to be it.

469
00:25:50,760 --> 00:25:52,760
We don't want to suffer.

470
00:25:52,760 --> 00:25:54,760
So let's stop making our stuff separate.

471
00:25:54,760 --> 00:25:57,760
We have to see that these things are causing us suffering.

472
00:25:57,760 --> 00:25:59,760
And the worst is this third one,

473
00:25:59,760 --> 00:26:01,760
when you set your mind in the wrong way.

474
00:26:01,760 --> 00:26:03,760
This is what the Buddha is talking about.

475
00:26:03,760 --> 00:26:04,760
This is the key to this first.

476
00:26:04,760 --> 00:26:07,760
Something for us all to remember.

477
00:26:07,760 --> 00:26:10,760
Whenever you think your problems are other people,

478
00:26:10,760 --> 00:26:12,760
and the situation in the world around you,

479
00:26:12,760 --> 00:26:14,760
never forget what the Buddha said.

480
00:26:14,760 --> 00:26:16,760
It's hard to think about the Buddha.

481
00:26:16,760 --> 00:26:18,760
It's hard to think about the Buddha.

482
00:26:18,760 --> 00:26:21,760
It's hard to think about the Buddha.

483
00:26:21,760 --> 00:26:24,760
It's hard to think about the Buddha.

484
00:26:24,760 --> 00:26:27,760
And the enemy would do to an enemy,

485
00:26:27,760 --> 00:26:30,760
or one seeking vengeance would do to another.

486
00:26:30,760 --> 00:26:36,760
The wrongly directed mind is far, far.

487
00:26:36,760 --> 00:26:42,760
We'll do far, far greater evil than any of those.

488
00:26:42,760 --> 00:26:46,760
So that's the Dhammapanda verse for today.

489
00:26:46,760 --> 00:27:04,760
Thank you all for listening, and we'll see you next time.

