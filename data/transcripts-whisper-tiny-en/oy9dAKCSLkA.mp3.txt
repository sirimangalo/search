Can someone take the same meditation course several times? Absolutely.
It should be taken as many times as possible.
If it's a meditation course that leads you or helps you to see reality, take it again and again and again.
That's always a good thing.
