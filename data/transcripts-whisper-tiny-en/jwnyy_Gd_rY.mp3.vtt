WEBVTT

00:00.000 --> 00:08.880
Okay, then let's get started. Welcome, everyone, to a fine Sunday afternoon in

00:08.880 --> 00:19.880
second life. Everybody get yourself comfortable, both here in virtual world and at

00:19.880 --> 00:30.360
home. I'd say that it's equally important that your focused at home is more

00:30.360 --> 00:34.840
important that you're focused at home than that you're focused here. If your

00:34.840 --> 00:39.480
avatar here is shifting and shuffling, that's not really a problem. But at home

00:39.480 --> 00:43.800
you should try to, if you can, while you're watching this, try to settle

00:43.800 --> 00:58.820
yourself down, turn Facebook off. Set yourself to busy if it helps. And I'm just

00:58.820 --> 01:03.720
going to talk for a little while and then if anyone has any questions, I'm happy

01:03.720 --> 01:21.240
to answer them. So the topic I'd like to talk about today has to do with the

01:21.240 --> 01:28.400
very core of the Buddha's teaching. No use beating around the bush and no use

01:28.400 --> 01:36.920
giving complicated roundabout or exotic teachings. Let's cut right to

01:36.920 --> 01:44.000
the chase. When we talk about the core of Buddhism, we always think of the

01:44.000 --> 01:49.760
four noble truths. I think because it's a good summary of the Buddha's

01:49.760 --> 01:54.400
teaching. But there's a teaching that goes even more to the core than that,

01:54.400 --> 02:00.360
and that's the Buddha's teaching on dependent origination. And that's what I'd

02:00.360 --> 02:08.080
like to talk about today. I know I've talked about it elsewhere. I can't

02:08.080 --> 02:12.600
remember if I've talked about it here on second life, but it's always good to

02:12.600 --> 02:22.280
go over things again. So we take the Buddha's teaching in its entirety as a

02:22.280 --> 02:27.520
instruction in the practice of meditation, in observing reality and coming

02:27.520 --> 02:32.440
to understand it. So when you listen to me give a talk here, it's important

02:32.440 --> 02:41.280
that you're mimicking or following after the example of the people who

02:41.280 --> 02:46.280
listen to the Buddha himself teach. And that is when they listen to the

02:46.280 --> 02:49.920
Buddha's teaching, they would also practice meditation. They would take it as

02:49.920 --> 03:01.560
an opportunity to look inside themselves and to apply the teachings as a

03:01.560 --> 03:06.800
reminder for how to approach reality. So here we can do the same. You don't

03:06.800 --> 03:13.680
have to look at your computer screen. You can close your eyes if it helps you

03:13.680 --> 03:21.000
to keep on track to look at the screen fine, but try to really appreciate the

03:21.000 --> 03:25.760
teachings on a more practical level. Because in the time of the Buddha, many

03:25.760 --> 03:30.520
people, the example that they said is that they could gain states of

03:30.520 --> 03:34.080
realization during the time that they were listening to the Buddha's

03:34.080 --> 03:41.800
teaching. Simply by applying it and using it to calm the mind, to restrain the

03:41.800 --> 03:48.200
mind, and calm the mind, and to eventually understand the workings of the

03:48.200 --> 03:56.320
mind. They could even enter into a state of enlightenment, go into the

03:56.320 --> 04:09.800
realization of Nibana and so on. So take that as an example here. This teaching

04:09.800 --> 04:14.640
especially is an incredibly useful and practical teaching, the teaching

04:14.640 --> 04:21.040
on dependent origination. It's the most profound, I would say the most profound

04:21.040 --> 04:26.840
statement of reality that exists. I can't think of anything else that I've

04:26.840 --> 04:32.520
heard more profound, even within the Buddha's teaching. And for me the most

04:32.520 --> 04:38.720
profound part is the very beginning of the teaching, and it's these three

04:38.720 --> 04:45.920
words in the Pali that really brought light to the world. Before the uttering

04:45.920 --> 04:54.200
of these three words, a vidja pajayasang kara. Before the Buddha realized this,

04:54.200 --> 05:02.600
that a vidja or ignorance is the cause that is a cause for the arising of

05:02.600 --> 05:14.880
formations or with ignorance as a cause, there are rise formations. Anybody

05:14.880 --> 05:21.360
enlightened yet? Probably not that easy. This is a profound teaching and it's

05:21.360 --> 05:26.360
something that very difficult for us to understand, very difficult to

05:26.360 --> 05:32.360
comprehend this teaching. And it often goes over our heads, we think of it as

05:32.360 --> 05:40.840
some sort of philosophical teaching. Ignorance leads to formations.

05:43.840 --> 05:49.240
Formations here, just to explain the word formations, what we're talking about

05:49.240 --> 05:56.560
here is our mental formations, our ideas about things, our thoughts, what we

05:56.560 --> 06:10.600
think of something, our mental volition. When we want to hurt someone, when we

06:10.600 --> 06:18.440
want to get attained something, when we get angry, when we get greedy, when we

06:18.440 --> 06:31.360
get attached, addicted, when we're afraid or worried and so on. All of these

06:31.360 --> 06:36.400
mental states that are a reaction to something, these arise the Buddha said

06:36.400 --> 06:46.040
based on ignorance. And this is really a profound teaching that deserves the

06:46.040 --> 06:52.440
full of our attention. If we can understand just these three words, if we

06:52.440 --> 06:58.120
can realize this in our meditation practice, this is the state of

06:58.120 --> 07:09.040
enlightenment that we're looking for. And the problem is that for most of us,

07:09.040 --> 07:15.520
we don't think this way. When we get angry or when we become addicted to

07:15.520 --> 07:21.760
something, we say to ourselves or we say to other people, I know it's wrong. I

07:21.760 --> 07:31.480
know it's not good to get angry, but I can't help myself. I know it's not good

07:31.480 --> 07:45.800
to become addicted to sweet foods or so on, but I can't help myself. It's not

07:45.800 --> 07:52.000
that I don't know. It's that I'm unable to change it. I'm unable to avoid

07:52.000 --> 08:02.840
the judging, the emotion. And so the Buddha denied this. He denied that this is

08:02.840 --> 08:08.520
the case. He taught the exact opposite. He said, no, you don't know. You don't know

08:08.520 --> 08:12.680
that it's wrong. You say, yes, I know that it's wrong. What you really mean is

08:12.680 --> 08:16.280
that someone told you that it's wrong. You don't like the results that come

08:16.280 --> 08:22.440
from it, but you don't really understand that it's wrong. This is how we

08:22.440 --> 08:29.840
approach everything in our lives with ignorance, with a incredibly superficial

08:29.840 --> 08:36.120
awareness. Now, even imagine yourself, look at yourself, how you're sitting

08:36.120 --> 08:45.600
right now, listening to my talk. You're seeing things, you're hearing things,

08:45.600 --> 08:51.840
you're smelling, you're tasting, you're feeling and thinking. And all of this

08:51.840 --> 08:57.480
is happening very quickly. And when you see something, you immediately start

08:57.480 --> 09:03.360
to judge it. Immediately start to assess it. This is beautiful. This is nice,

09:03.360 --> 09:10.560
or this is ugly, or this is terrible, horrible, whatever it is. When you hear

09:10.560 --> 09:15.360
something, you immediately start to judge it. Maybe you like the birds in the

09:15.360 --> 09:21.760
background. Maybe they're too loud, too noisy. Maybe that repetitive cricket

09:21.760 --> 09:27.200
noise is driving you crazy. We don't really see, and we don't really hear, we

09:27.200 --> 09:34.600
don't really understand the experience. And we don't really understand our

09:34.600 --> 09:39.560
reaction to the experience. When we see something, we think I see, I'm seeing

09:39.560 --> 09:44.960
this, and we think I like it, and we think it is good. We have all of these

09:44.960 --> 09:49.280
preconceived notions that are totally disconnected from the reality of the

09:49.280 --> 10:04.520
experience. They're generally bound up in our our habits, our accustomed

10:04.520 --> 10:10.600
way of responding to things. We remember that certain things bring us pleasure

10:10.600 --> 10:14.800
and so we respond in that manner. We think something's going to bring

10:14.800 --> 10:23.160
us happy, and it's a habitual response. It's the response of, I'm saying, an

10:23.160 --> 10:31.440
ordinary animal. So all we're trying to do in meditation is to look deeper at

10:31.440 --> 10:37.320
things. And when someone you're sitting here listening to the talk, someone walks

10:37.320 --> 10:41.080
into the room and starts making loud noise right away you get angry at it.

10:41.080 --> 10:46.320
Remember there's a little kid making noises, yelling, pestering you in this

10:46.320 --> 10:51.800
or that, for this or that. And you're ready to get angry, get irritated. You can

10:51.800 --> 10:56.800
even get to the point where you want to yell at them. It's very quick, it's

10:56.800 --> 11:01.000
very easy to do that. And the only reason that you do it, it's not that you're a

11:01.000 --> 11:03.840
bad person, it's that you're ignorant. You don't really understand what

11:03.840 --> 11:08.880
happened. You weren't watching. You weren't clear on the experience. You misunderstood

11:08.880 --> 11:13.680
it. And so you followed after it. You reacted inappropriately. And you caused

11:13.680 --> 11:17.840
suffering for other people and for yourself. You feel guilty, you feel upset,

11:17.840 --> 11:20.720
you feel angry.

11:27.320 --> 11:31.840
So this is the most important point of the Buddha's teaching is that ignorance

11:31.840 --> 11:46.680
is the cause of our reactions to things, our judgments, our improper

11:46.680 --> 12:02.840
modes of behavior, modes of responding to the stimulus that come to us. And so

12:02.840 --> 12:08.920
the Buddha tried to describe to us in his teaching what it was that he realized,

12:08.920 --> 12:17.080
the detailed explanation of what's going on so that when we practice meditation

12:17.080 --> 12:21.640
we can see things clearer. You ask, okay, so I'm ignorant. What is it that I'm

12:21.640 --> 12:29.640
ignorant of? What is it that I don't understand? And the truth is if you

12:29.640 --> 12:32.960
spend some time looking at reality, you'll see there's a lot that you don't

12:32.960 --> 12:36.640
understand. There's a lot that you weren't aware. You'll see that if you just

12:36.640 --> 12:41.920
took the time to really see what's going on when this young child is

12:41.920 --> 12:55.720
pestering you, when this loud noise is bothering you, when it's too hot, when

12:55.720 --> 13:00.160
it's too cold, when you have pain in the body and so on. If you just took the

13:00.160 --> 13:06.760
time to look at it, you'd see there's so much more going on than you thought. And at

13:06.760 --> 13:09.440
the same time, the experience is so much less than you thought. There's

13:09.440 --> 13:18.480
nothing unpleasant about it at all that we've got a totally wrong

13:18.480 --> 13:24.160
understanding of the experience that surprisingly, there's nothing unpleasant

13:24.160 --> 13:29.240
about it at all. You can be an incredible pain. And when you really understand

13:29.240 --> 13:32.000
the pain, when you really see what's going on, it doesn't bother you at all.

13:32.000 --> 13:44.880
You become surprised that you were ever upset by it in the first place. You say

13:44.880 --> 13:49.360
to yourself, you can't believe that you were addicted to this. And it's a

13:49.360 --> 13:54.840
epiphany of sorts. You suddenly realize that there's nothing wrong with

13:54.840 --> 14:01.120
reality. There's nothing wrong with the way things are. It is the way it is. What's

14:01.120 --> 14:06.040
wrong is the way we respond, the way we react to it.

14:13.120 --> 14:20.000
So the Buddha taught us to go into more detail and he explained what's really

14:20.000 --> 14:26.560
going on. And this is in the rest of the exposition on the dependent

14:26.560 --> 14:39.120
origination. So to go through it and brief what's really going on is that

14:39.120 --> 14:50.080
in the world, in the universe, in the ultimate reality, there are two things.

14:50.080 --> 14:58.520
There are two aspects of experience. And they're sort of like two sides of the

14:58.520 --> 15:07.360
same coin. They're distinct, but they're a pair. And these are the physical and

15:07.360 --> 15:17.840
the mental. In the universe, all of our experience can be summed up under the

15:17.840 --> 15:23.800
physical and the mental. When we see something, this is the light touching the

15:23.800 --> 15:28.000
eye. The eye is physical. The light is physical. When we hear something, this is

15:28.000 --> 15:33.280
the sound touching the ear and these are both physical. Smells and the nose,

15:33.280 --> 15:41.800
tastes and the tongue, feelings in the body. These are all physical. And the

15:41.800 --> 15:47.760
mental side is the knowing of the object, the perception of it. When our mind is

15:47.760 --> 15:52.320
at the eye, then we see when our mind is at the ear, then we hear. But sometimes

15:52.320 --> 15:54.800
the ear might be there and the sound might be there, but our mind is

15:54.800 --> 15:59.920
somewhere else. And so we fail to hear the things that people say to us. It's

15:59.920 --> 16:02.880
difficult to see if you're not really focusing. But sometimes when you're using

16:02.880 --> 16:06.800
the computer, you can find that. You're focusing so much on something that you

16:06.800 --> 16:11.480
don't hear someone talking to you. You don't know what it was that they said.

16:19.320 --> 16:24.920
And these things in and of themselves are not a problem obviously. The mind

16:24.920 --> 16:32.800
knows the object, the object arises, the mind knows it. But what happens next is

16:32.800 --> 16:40.920
there arises a feeling. The body and the mind, it comes together at the eye, the

16:40.920 --> 16:49.120
ear, the nose, the tongue, the body or the mind. In the mind, there's only

16:49.120 --> 16:53.680
a thought, there's only the mind. But we have the body and the mind coming

16:53.680 --> 17:00.000
together or else just the mind thinking itself. At the moment of experience,

17:00.000 --> 17:08.200
there arises a feeling. You can verify this. When you see something, if it's a

17:08.200 --> 17:13.080
good thing, you write away, you feel happy about it. And you can see this if

17:13.080 --> 17:17.200
you're really focusing on it. So for instance, when we see something and we say

17:17.200 --> 17:24.480
to ourselves seeing, seeing, seeing, we can catch when we feel happy about it or

17:24.480 --> 17:30.720
when we feel unhappy about it. When we hear something hearing, hearing, we can

17:30.720 --> 17:36.840
catch the feeling that there's a feeling first. There's a pleasant feeling or an

17:36.840 --> 17:44.960
unpleasant feeling or a neutral feeling. And these feelings in and of

17:44.960 --> 17:52.280
themselves aren't a problem either. There's nothing inherently unwholesome

17:52.280 --> 18:03.920
about a happy feeling or an unhappy feeling. A pleasant or an unpleasant feeling,

18:03.920 --> 18:09.920
it's a physical response to a stimulus. And since we feel pain in the body,

18:09.920 --> 18:17.240
there's nothing wrong with that. There's nothing unpleasant about it. And

18:17.240 --> 18:20.560
nothing unwholesome about it. And by the same token, there's nothing unwholesome

18:20.560 --> 18:27.680
about a pleasant feeling. So many people, when they hear that they're instructed

18:27.680 --> 18:33.680
to acknowledge the happy feelings, they get the wrong impression that we're

18:33.680 --> 18:39.440
trying to do away with happiness. That's wrong to feel happiness. And this isn't

18:39.440 --> 18:42.720
at all the case, but we want to understand the happiness. We want to see it

18:42.720 --> 18:52.120
for what it is, because it's the feelings when unacknowledged, when misunderstood.

18:52.120 --> 18:55.440
If there's ignorance about the feeling that this is what's going to give rise

18:55.440 --> 19:01.400
to craving, this is what gives rise to our likes and our dyslinks.

19:01.400 --> 19:14.720
This teaching, if you haven't ever practiced meditation, it might seem quite

19:14.720 --> 19:20.480
foreign. It might seem quite even uninteresting. It's very difficult to understand.

19:20.480 --> 19:24.480
But this is an incredibly useful teaching when you're practicing meditation.

19:24.480 --> 19:30.680
Often meditators will be at a loss as to how to deal with strong emotions

19:30.680 --> 19:35.280
that come up, you know, when they really are attached to something, or when they're

19:35.280 --> 19:41.080
really angry about something, when they're really distracted and unfocused, when

19:41.080 --> 19:52.400
they're worried or stressed, depressed, bored, afraid, whatever. And they don't

19:52.400 --> 19:56.760
know how to deal with it. And what the Buddha is doing here is breaking that

19:56.760 --> 20:02.080
experience up. What happens when you're angry? What happens when you're attached

20:02.080 --> 20:07.840
to something? And when you break it up, you can see that there's nothing really

20:07.840 --> 20:11.600
worth attaching to at all. When you feel happy, it's just a happy feeling.

20:11.600 --> 20:16.880
There's nothing positive or negative about it. It is what it is. You can see that

20:16.880 --> 20:20.640
when you cling to it, when you say, this is good, you're not going to prolong it.

20:20.640 --> 20:26.840
You're just going to create a need for it, an attachment to it. It's not like you

20:26.840 --> 20:30.080
can say, oh, I like this. Therefore, it's going to stay longer. It's going to

20:30.080 --> 20:37.800
stay longer than if I didn't like it. Because it's exactly the case with

20:37.800 --> 20:42.880
negative emotions that you can't make them go away just because you don't want

20:42.880 --> 20:46.680
them to be there. Negative experience doesn't disappear just because you want it

20:46.680 --> 20:54.480
to go. Positive experience doesn't stay just because you want it to stay.

20:54.480 --> 20:59.760
When we come to see this, we come to see the nature of these things is that

20:59.760 --> 21:05.080
they're impermanent. They're unsure, uncertain. They come and go according to

21:05.080 --> 21:11.080
their own nature, according to the causes and effects that created, or the

21:11.080 --> 21:33.080
causes that created them. And so you can pick any one of these parts. The

21:33.080 --> 21:39.440
object of your desire or the object of your aversion. You can pick the

21:39.440 --> 21:45.280
feeling that it gives rise to inside of you. Or you can pick the emotion that

21:45.280 --> 21:51.200
arises. The important thing is that you pick it apart and see it clearly and

21:51.200 --> 21:55.280
you're focusing on something that's real. Because just saying I'm addicted and

21:55.280 --> 22:00.320
that's that and I can't stop myself isn't at all useful, isn't useful in any

22:00.320 --> 22:05.960
way to simply say that I'm an angry person also isn't useful. It's not

22:05.960 --> 22:11.640
really understanding what's happening. It's not seeing clearly what's going on.

22:14.480 --> 22:19.120
Once you can pick it apart, if you can catch yourself at the emotion, at the

22:19.120 --> 22:27.440
feeling, if you feel pain, or so on. Once you see it clearly, then there's no,

22:27.440 --> 22:33.080
you find no reason to get upset about it. You, instead of saying this is bad,

22:33.080 --> 22:38.160
this is painful. You just say this is this. This is what it is. When there's pain,

22:38.160 --> 22:45.720
you know that there's pain. When there's a pleasant feeling, instead of getting

22:45.720 --> 22:54.680
addicted to it, suppose it's good food or or a beautiful sight. You're simply

22:54.680 --> 22:59.080
aware that it is what it is. It's a, it's a sight and it's a happy feeling that

22:59.080 --> 23:06.720
arises. And you don't see any reason to become addicted or attached to it. It

23:06.720 --> 23:10.680
doesn't make it last, as I said. It doesn't do you any good and all it does is

23:10.680 --> 23:14.120
lead to suffering when it's gone.

23:14.120 --> 23:31.720
Because the alternative is, is to live our lives as we do as ordinary people who

23:31.720 --> 23:37.320
are uninterested in, in mental development, live their lives, happy sometimes,

23:37.320 --> 23:41.600
miserable sometimes, even to the point where they try to kill themselves

23:41.600 --> 23:49.440
sometimes, having to go through incredible stress and suffering, because they

23:49.440 --> 23:54.520
don't understand the experience of reality in front of them. It's not

23:54.520 --> 23:59.760
because they're situation. There's anything wrong with it. It's that they don't

23:59.760 --> 24:03.640
understand what's happening. They don't understand the nature of their

24:03.640 --> 24:11.120
experience. And so they attribute it to being me and mine and, and they attribute

24:11.120 --> 24:20.280
the idea that it's somehow should be forced and controlled and changed. And so

24:20.280 --> 24:25.440
we segregate reality into the good and the bad, the acceptable and the

24:25.440 --> 24:33.840
unacceptable. When actually all there is is the physical and the mental and the

24:33.840 --> 24:40.960
feelings that arise. The problem that comes is when we, when we react, the

24:40.960 --> 24:51.280
problem is not in the objects themselves. When we crave for something, when we

24:51.280 --> 24:57.960
need for something, when we require that things be other than what they are, or

24:57.960 --> 25:02.960
when we require that things stay the way they are and not change.

25:02.960 --> 25:11.120
Simply put, when we require things to be other than reality dictates, when

25:11.120 --> 25:16.480
reality dictates that things must change. And we require that it to be

25:16.480 --> 25:24.400
otherwise. This is where suffering comes from. We cling to it. We say it must be.

25:24.400 --> 25:31.920
We require it to be other than this. We're not satisfied the way things are. We

25:31.920 --> 25:38.280
have to go and seek out more. We don't understand and see it for what it is. We

25:38.280 --> 25:46.440
think it's unpleasant, or it's bad, or we think that this is going to make me

25:46.440 --> 25:54.280
happy if I can just attain this, get this or that. Object.

25:54.280 --> 26:07.200
And so we cling to me. We refuse to accept change. We refuse to accept things

26:07.200 --> 26:14.480
the way they are. And this is what gives rise to suffering. This is what sets us on

26:14.480 --> 26:23.360
a cycle of addiction or obsession. This may be a better word, needing it to be

26:23.360 --> 26:31.000
like this, needing it not to be like that. And the suffering that comes when it's

26:31.000 --> 26:38.680
not the way we want it to be. We don't see this in ordinary, everyday life. We

26:38.680 --> 26:47.120
don't see this when we're not observing, when we're not meditating. All we see is

26:47.120 --> 26:51.280
the suffering that comes from things not being the way we want. Even right now,

26:51.280 --> 27:00.280
I'm sure there's many things going on in your experience that are unpleasant.

27:00.280 --> 27:04.640
Anyway, you were trying to change them. You're sitting here maybe it's too hot,

27:04.640 --> 27:08.320
maybe it's too cold, maybe the seat is too hard and you have to shift your

27:08.320 --> 27:14.560
position. Maybe you don't like what I'm saying and it makes you upset and gives

27:14.560 --> 27:35.400
you a headache or so on. And it's our inability to see these things clearly, to see

27:35.400 --> 27:41.400
what's really going on that leads us to obsessions and

27:41.400 --> 27:51.720
to suffering. Once we look at it, we see how amazing reality really is and how

27:51.720 --> 27:57.320
amazing mindfulness really is. Simply seeing things for what they are, understanding

27:57.320 --> 28:06.400
things for what they are. In a moment, you can do away with any suffering that arises.

28:06.400 --> 28:12.680
It feels stressed. When you focus on the stress, just penetrate into it, what's

28:12.680 --> 28:18.960
going on here? What's happening? What does it mean to say, I am stressed, I'm upset?

28:18.960 --> 28:25.320
Where's the eye? Where's the stress? What's really going on?

28:25.320 --> 28:29.840
You just say to yourself, stress, stress, stress, keeping your mind with it and seeing it

28:29.840 --> 28:34.720
simply for what it is. You realize there is no eye involved. There's only a

28:34.720 --> 28:40.040
feeling of stress that arises. When you see that there's nothing intrinsically

28:40.040 --> 28:47.120
wrong with this tense state. It is what it is. It's something that's arisen.

28:47.120 --> 28:59.760
And after some time, we'll disappear. When you want something or when you're

28:59.760 --> 29:05.320
angry about something, whatever the emotion is, whatever is causing you, stress

29:05.320 --> 29:10.560
and suffering, whatever is getting in the way of your clear understanding, your

29:10.560 --> 29:22.720
peace, your peaceful harmony with reality. You penetrate into it, you see it

29:22.720 --> 29:27.480
for what it is. You see that there's many things going on. You have happy

29:27.480 --> 29:33.160
feelings, you have negative unpleasant feelings. You have these states of

29:33.160 --> 29:42.480
greed and anger. And they come and they go. And when you can see and understand

29:42.480 --> 29:47.680
these things, then you can say to yourself, I know it's wrong and that's why I

29:47.680 --> 29:53.160
don't do it. You'll never say to yourself, again, I know it's wrong and but I

29:53.160 --> 29:57.240
still do it. You come to realize that you really don't know what's wrong with it.

29:57.240 --> 30:02.680
You really don't know the true nature of the experience and why it's wrong to

30:02.680 --> 30:06.760
get angry. Because when you really know that it's wrong to get angry or greedy or

30:06.760 --> 30:10.760
so on, you won't do it. When you know that it's wrong to carry out some

30:10.760 --> 30:17.360
behavior, when you truly have Weetja or knowledge, understand the situation,

30:17.360 --> 30:25.960
you'll never cause suffering for yourself again. So that was the teaching that I

30:25.960 --> 30:32.040
thought to discuss today. I hope that was useful for some people. It was a

30:32.040 --> 30:38.440
guide for where you should be going in your meditation. Thanks for

30:38.440 --> 31:04.600
everyone for coming. And if you have any questions, I'm happy to take them now.

