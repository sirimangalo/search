1
00:00:00,000 --> 00:00:24,800
Okay, good evening, everyone.

2
00:00:24,800 --> 00:00:31,800
Welcome to our evening talk.

3
00:00:31,800 --> 00:00:32,800
Let me know if the audio is okay.

4
00:00:32,800 --> 00:00:34,800
I'm using a different interface now.

5
00:00:34,800 --> 00:00:35,800
It's too quiet or too loud.

6
00:00:35,800 --> 00:00:52,800
Please let me know.

7
00:00:52,800 --> 00:01:00,800
So last night we were talking about special experiences.

8
00:01:00,800 --> 00:01:05,800
And then I ended with the idea that the demo might be like into a parasite,

9
00:01:05,800 --> 00:01:19,800
which of course is the wrong word, but the idea was to add a little bit of shock value to the teaching.

10
00:01:19,800 --> 00:01:24,800
The idea being that the teaching is something new.

11
00:01:24,800 --> 00:01:30,800
It often changes something very fundamental about who we are in a good way.

12
00:01:30,800 --> 00:01:39,800
Whatever they maybe didn't get clear or should have been very clear is that

13
00:01:39,800 --> 00:01:45,800
it straightens out a lot of things about us that are crooked,

14
00:01:45,800 --> 00:01:48,800
that are causing us pain and suffering.

15
00:01:48,800 --> 00:01:53,800
We remove a lot of things that we once maybe thought were part of our personality,

16
00:01:53,800 --> 00:01:57,800
part of who we are, and we've removed them.

17
00:01:57,800 --> 00:02:12,800
And then we add certain things that maybe weren't there at all in the past.

18
00:02:12,800 --> 00:02:20,800
Anyway, tonight I wanted to talk about more special things.

19
00:02:20,800 --> 00:02:25,800
Today I wanted to talk about magic.

20
00:02:25,800 --> 00:02:31,800
Magic's an interesting topic.

21
00:02:31,800 --> 00:02:43,800
Often our practice comes across as buying into the materialistic or materialist,

22
00:02:43,800 --> 00:02:46,800
physicalist outlook on life.

23
00:02:46,800 --> 00:02:57,800
We're very secular in our approach.

24
00:02:57,800 --> 00:03:02,800
Sometimes we may even be a little bit over cautious

25
00:03:02,800 --> 00:03:07,800
in terms of even mentioning those aspects of practice.

26
00:03:07,800 --> 00:03:13,800
Sometimes back ourselves into a bit of a corner where our audience

27
00:03:13,800 --> 00:03:18,800
is under the impression that we are physicalists in the sense that we believe

28
00:03:18,800 --> 00:03:22,800
that the reality is based on the lives of physics.

29
00:03:22,800 --> 00:03:27,800
But to some extent that's not really true of Buddhism.

30
00:03:27,800 --> 00:03:36,800
In the Buddhist teaching, the lives of physics appear to be somewhat malleable.

31
00:03:36,800 --> 00:03:41,800
I don't know how much of it's really true, but there is a lot of magic

32
00:03:41,800 --> 00:03:44,800
said to go on in Buddhism.

33
00:03:44,800 --> 00:03:49,800
The idea is that the physical world is fine if you're a frame of reference

34
00:03:49,800 --> 00:03:53,800
as a physical world.

35
00:03:53,800 --> 00:03:58,800
But the mind is this whole other realm.

36
00:03:58,800 --> 00:04:02,800
The power of the mind is in a whole realm of its own,

37
00:04:02,800 --> 00:04:05,800
or a whole dimension, I guess you might say.

38
00:04:05,800 --> 00:04:12,800
But it's not just another dimension of the physical world.

39
00:04:12,800 --> 00:04:27,800
So it can warp and change the physical world in very fundamental ways.

40
00:04:27,800 --> 00:04:44,800
And so we have this teaching that the Buddha gave on the six abhinya.

41
00:04:44,800 --> 00:04:47,800
There's all sorts of interesting stuff.

42
00:04:47,800 --> 00:04:53,800
There's the idea of making yourself invisible or multiplying yourself,

43
00:04:53,800 --> 00:04:55,800
making multiple images of yourself.

44
00:04:55,800 --> 00:05:03,800
We have all these stories that appear fantastical.

45
00:05:03,800 --> 00:05:07,800
When the Buddha didn't claim to have come up with this sort of teaching,

46
00:05:07,800 --> 00:05:12,800
he talked about others and there are talks that would talk about non-Buddists

47
00:05:12,800 --> 00:05:15,800
who to some extent are able to gain these magical powers.

48
00:05:15,800 --> 00:05:20,800
It's not common, but the boast is that it's quite common for Buddhists.

49
00:05:20,800 --> 00:05:25,800
I don't think it's very common for people who come and practice our teaching,

50
00:05:25,800 --> 00:05:34,800
which is again why we have this sort of reputation or this appearance of being very secular,

51
00:05:34,800 --> 00:05:40,800
or very secular, but very mundane,

52
00:05:40,800 --> 00:05:46,800
very much reaffirming that reality is mundane.

53
00:05:46,800 --> 00:05:48,800
There's nothing really mundane about Buddhism.

54
00:05:48,800 --> 00:05:53,800
No, it's all about leaving behind the mundane.

55
00:05:53,800 --> 00:06:02,800
And so the Buddha would paint this broad picture of magic.

56
00:06:02,800 --> 00:06:04,800
One of my first teachers said to me,

57
00:06:04,800 --> 00:06:09,800
and I'm not sure if I think maybe this is where I'll agree with her,

58
00:06:09,800 --> 00:06:13,800
but for the longest time I wasn't really in great agreement.

59
00:06:13,800 --> 00:06:16,800
You said Buddhism puts the magic back into life,

60
00:06:16,800 --> 00:06:20,800
but she was talking about these sort of magical experiences.

61
00:06:20,800 --> 00:06:23,800
I don't know, in my 17 years practicing Buddhism,

62
00:06:23,800 --> 00:06:33,800
I haven't had that many magical experiences of that sort.

63
00:06:33,800 --> 00:06:39,800
More you hear about people claiming.

64
00:06:39,800 --> 00:06:43,800
And you'll often, you'll from time to time meet a Buddhist individual

65
00:06:43,800 --> 00:06:49,800
who can read your mind, or at least know what you're feeling.

66
00:06:49,800 --> 00:06:52,800
I've met people who could read emotions,

67
00:06:52,800 --> 00:06:54,800
and it's not been looking at your face,

68
00:06:54,800 --> 00:07:03,800
because they would suddenly look at you and be startled by your thoughts or your feelings.

69
00:07:03,800 --> 00:07:05,800
People would say this about Ajahn Tang,

70
00:07:05,800 --> 00:07:08,800
no, I'm not sure.

71
00:07:08,800 --> 00:07:11,800
I never noticed such a thing with him,

72
00:07:11,800 --> 00:07:17,800
but then he's sort of a meat and potatoes kind of monk, I suppose.

73
00:07:17,800 --> 00:07:21,800
He's very practical and simple.

74
00:07:21,800 --> 00:07:27,800
Not flashy and certainly not showing off magical powers.

75
00:07:27,800 --> 00:07:30,800
Or there he has them or not.

76
00:07:30,800 --> 00:07:33,800
But there is this ability to levitate.

77
00:07:33,800 --> 00:07:35,800
We talked about yesterday,

78
00:07:35,800 --> 00:07:41,800
walking through walls.

79
00:07:41,800 --> 00:07:44,800
This is the first one, it's called Idi.

80
00:07:44,800 --> 00:07:48,800
Idi means magical powers.

81
00:07:48,800 --> 00:07:56,800
These are all the many types of myriad magical powers that one can gain.

82
00:07:56,800 --> 00:07:59,800
And then there's a clear audience,

83
00:07:59,800 --> 00:08:03,800
and then there would be clear avoidance.

84
00:08:03,800 --> 00:08:05,800
Hearing things far away.

85
00:08:05,800 --> 00:08:09,800
Now, this one actually is relatively common among meditators.

86
00:08:09,800 --> 00:08:12,800
I mean, relative to the rest of these.

87
00:08:12,800 --> 00:08:15,800
You will, from time to time, get a meditator who says they've heard,

88
00:08:15,800 --> 00:08:19,800
they start hearing things, like they'll hear chanting.

89
00:08:19,800 --> 00:08:21,800
Sometimes it even gets to the point where they can hear,

90
00:08:21,800 --> 00:08:26,800
and they can identify it, something far away they can hear.

91
00:08:26,800 --> 00:08:31,800
I guess as far as Idi goes,

92
00:08:31,800 --> 00:08:33,800
another one is people will leave their bodies,

93
00:08:33,800 --> 00:08:36,800
astral travel is a thing.

94
00:08:36,800 --> 00:08:39,800
Believe it or not, there are people who leave their bodies

95
00:08:39,800 --> 00:08:41,800
fly through the air.

96
00:08:41,800 --> 00:08:45,800
The disbeliever might say it's all just imaginary.

97
00:08:45,800 --> 00:08:51,800
But there are those who make claims,

98
00:08:51,800 --> 00:08:53,800
like I have this story,

99
00:08:53,800 --> 00:08:56,800
that I feel comfortable telling because I wasn't a monk at the time.

100
00:08:56,800 --> 00:08:58,800
I was 13 or so,

101
00:08:58,800 --> 00:09:00,800
and I saw something.

102
00:09:00,800 --> 00:09:03,800
I left my body and saw something that hadn't yet happened.

103
00:09:03,800 --> 00:09:05,800
And then when I went back into my body,

104
00:09:05,800 --> 00:09:08,800
I actually saw it happen.

105
00:09:08,800 --> 00:09:09,800
Premonitions.

106
00:09:09,800 --> 00:09:13,800
This is a common one, even outside of Buddhism.

107
00:09:13,800 --> 00:09:25,800
I think with this woman, this teacher of mine,

108
00:09:25,800 --> 00:09:30,800
what she was trying to get at is that we sometimes

109
00:09:30,800 --> 00:09:35,800
have a little bit of a narrow outlook on life.

110
00:09:35,800 --> 00:09:38,800
We see only what is in front of us,

111
00:09:38,800 --> 00:09:43,800
you know, for a few thousand is actually quite useful,

112
00:09:43,800 --> 00:09:45,800
but we fail to be able to see the bigger picture,

113
00:09:45,800 --> 00:09:50,800
to see beyond this conventional reality.

114
00:09:50,800 --> 00:09:57,800
And so magic in some ways helps to broaden our horizons.

115
00:09:57,800 --> 00:09:59,800
And there's reading people's minds,

116
00:09:59,800 --> 00:10:01,800
and then there's remembering past lives.

117
00:10:01,800 --> 00:10:04,800
I've talked to people who have remembered past lives.

118
00:10:04,800 --> 00:10:07,800
Through practice, there's actually a practice you can do.

119
00:10:07,800 --> 00:10:10,800
You can do, you try and remember,

120
00:10:10,800 --> 00:10:12,800
you sit down, you enter into the genres,

121
00:10:12,800 --> 00:10:14,800
and then you try and remember what you just did before

122
00:10:14,800 --> 00:10:17,800
you sat down to meditate.

123
00:10:17,800 --> 00:10:19,800
And then you can remember before that,

124
00:10:19,800 --> 00:10:22,800
and before that, and you go back and back

125
00:10:22,800 --> 00:10:26,800
until you get stuck, and then you start back with the genres.

126
00:10:26,800 --> 00:10:29,800
And eventually you can go further and further back

127
00:10:29,800 --> 00:10:32,800
until you can get to the moment of your conception,

128
00:10:32,800 --> 00:10:37,800
and then you can go beyond that,

129
00:10:37,800 --> 00:10:42,800
break through and remember past lives.

130
00:10:46,800 --> 00:10:51,800
And the fifth one is,

131
00:10:51,800 --> 00:10:54,800
in regards to karma,

132
00:10:54,800 --> 00:10:57,800
there's this magical power of being able to see beings

133
00:10:57,800 --> 00:11:00,800
being reborn, dying and being reborn.

134
00:11:00,800 --> 00:11:04,800
When someone dies, being able to know where they were reborn.

135
00:11:04,800 --> 00:11:07,800
I've heard people claim this Buddhist teacher

136
00:11:07,800 --> 00:11:12,800
who claim this ability, who talk about knowing where people went.

137
00:11:12,800 --> 00:11:17,800
I think sometimes they're just sham artists.

138
00:11:17,800 --> 00:11:20,800
This one, I think, is fairly rare.

139
00:11:20,800 --> 00:11:24,800
Even Ananda, he would go to the Buddha to find this out.

140
00:11:24,800 --> 00:11:33,800
The Buddha had this knowledge, the ability to see where people went when they died.

141
00:11:35,800 --> 00:11:39,800
So where I'm going with this, all of this may sound a little bit wacky.

142
00:11:39,800 --> 00:11:41,800
Like, what good is this to us?

143
00:11:41,800 --> 00:11:44,800
Especially my meditators here are probably wondering where I'm going.

144
00:11:44,800 --> 00:11:47,800
I'm really wasting their time listening.

145
00:11:47,800 --> 00:11:50,800
No, there's a method to my madness.

146
00:11:50,800 --> 00:11:52,800
The point of this is that there is one magic.

147
00:11:52,800 --> 00:11:57,800
There are six abhinyas, and I've just sort of illustrated five of them.

148
00:11:57,800 --> 00:12:01,800
But the sixth one is Asubakayanya.

149
00:12:01,800 --> 00:12:07,800
Asubakayanya is the destruction of the defilements.

150
00:12:07,800 --> 00:12:17,800
When this gets back to this idea of the transformation of one's being.

151
00:12:17,800 --> 00:12:20,800
Now, the Buddha was critical, actually, of all these other types of magic.

152
00:12:20,800 --> 00:12:29,800
He praised them as a sign that the practice of Buddhism really did lead to strong states of concentration.

153
00:12:29,800 --> 00:12:35,800
So he would brag about them that his students had these magical powers.

154
00:12:35,800 --> 00:12:41,800
You know, as a means of showing how powerful is the mind and how powerful is Buddhism?

155
00:12:41,800 --> 00:12:51,800
As these are the sorts of things that people would look towards and strive after.

156
00:12:51,800 --> 00:12:55,800
There was this monk I've talked about him before in California.

157
00:12:55,800 --> 00:12:58,800
I think he's still there.

158
00:12:58,800 --> 00:13:01,800
He had the power to be able to heal people.

159
00:13:01,800 --> 00:13:06,800
He could see inside their bodies and know what was wrong with them.

160
00:13:06,800 --> 00:13:14,800
He would prescribe medicines and even give medicines from time to time.

161
00:13:14,800 --> 00:13:19,800
So I was fairly critical of this whenever people asked me about it.

162
00:13:19,800 --> 00:13:22,800
Because it's really quite problematic.

163
00:13:22,800 --> 00:13:27,800
When I was in California, there was a lot of Thai people who weren't all that interested in coming to learn from me.

164
00:13:27,800 --> 00:13:30,800
Because, well, I couldn't deal their sickness.

165
00:13:30,800 --> 00:13:33,800
But worse than that, people would go to him.

166
00:13:33,800 --> 00:13:37,800
And he couldn't teach them meditation because they weren't interested either.

167
00:13:37,800 --> 00:13:40,800
He would be constantly busy.

168
00:13:40,800 --> 00:13:47,800
He built a very large monastery in the north of the Mojave desert.

169
00:13:47,800 --> 00:13:56,800
But he could never actually teach meditation because no one was interested.

170
00:13:56,800 --> 00:13:58,800
So there's a danger to this.

171
00:13:58,800 --> 00:14:11,800
And further people will always be skeptical and always be obsessed with it and always be going further and further with the investigation of it.

172
00:14:11,800 --> 00:14:16,800
Further, they're not as certain as they may sound.

173
00:14:16,800 --> 00:14:22,800
A person who has mastery over their mind, it takes a very strong and pure mind.

174
00:14:22,800 --> 00:14:28,800
If your mind is not pure, these things can actually become perverted, they can drive you crazy.

175
00:14:28,800 --> 00:14:38,800
I've seen monks who have gone crazy temporarily insane or even over the long term, unable to get back to an ordinary state of reality.

176
00:14:38,800 --> 00:14:54,800
Because their minds have just become so warped with all this conventional messing with the fabric of reality.

177
00:14:54,800 --> 00:15:00,800
But there's a magic that's beyond this, a magic that is perfect and pure.

178
00:15:00,800 --> 00:15:05,800
And I think it's good to think of the results of insight meditation as magic.

179
00:15:05,800 --> 00:15:08,800
Because they're transformative, they truly are.

180
00:15:08,800 --> 00:15:17,800
This isn't about simply curing your mental problems and that's staying as the same person.

181
00:15:17,800 --> 00:15:19,800
It's really transformative.

182
00:15:19,800 --> 00:15:27,800
Because curing all your mental issues, curing the problems and the causes of our suffering really changes who we are.

183
00:15:27,800 --> 00:15:31,800
And it changes our whole reality.

184
00:15:31,800 --> 00:15:35,800
It's quite transformative in a very magical way.

185
00:15:35,800 --> 00:15:39,800
Your life will change, the people around you will change.

186
00:15:39,800 --> 00:15:43,800
Some people won't want to have anything to do with you anymore.

187
00:15:43,800 --> 00:15:46,800
You won't want to have anything to do with certain people.

188
00:15:46,800 --> 00:15:53,800
Suddenly you'll be inclined in different directions and your situation will change.

189
00:15:53,800 --> 00:15:58,800
Magic things will happen.

190
00:15:58,800 --> 00:16:09,800
Simply by aligning yourself in the right way and retuning the mind to a new frequency to talk in such language.

191
00:16:09,800 --> 00:16:16,800
It'll resonate with the universe in a different way.

192
00:16:16,800 --> 00:16:20,800
So I think it's important to be open to this.

193
00:16:20,800 --> 00:16:30,800
To not have some kind of attachment, it all comes down to attachment to conceptions, how we conceive of reality.

194
00:16:30,800 --> 00:16:35,800
Attachments to ourselves, to identity, who I am.

195
00:16:35,800 --> 00:16:39,800
Attachments to the world, what is this world?

196
00:16:39,800 --> 00:16:44,800
Clinging to this as a room, as a building, clinging to what we're seeing, as a park.

197
00:16:44,800 --> 00:16:53,800
Clinging to our bodies as being somehow meaningful, somehow, ultimately real.

198
00:16:53,800 --> 00:17:06,800
And somehow concrete and fundamental when they're not.

199
00:17:06,800 --> 00:17:10,800
The world is quite malley, the universe is quite malleable.

200
00:17:10,800 --> 00:17:20,800
And don't believe in other kinds of magic, that's fine, but this magic, this magic is far more transformative.

201
00:17:20,800 --> 00:17:29,800
It's transformative in powerful ways that we might even not realize.

202
00:17:29,800 --> 00:17:34,800
A person who practices meditation will feel like they were asleep before.

203
00:17:34,800 --> 00:17:39,800
And they'll look back on the person they were before as though they were another person.

204
00:17:39,800 --> 00:17:50,800
In many ways, like a completely different individual.

205
00:17:50,800 --> 00:17:56,800
Ah, so bhakayanya, the destruction of the development of the taints.

206
00:17:56,800 --> 00:18:00,800
So this is what we aim for in the practice. Be open to it.

207
00:18:00,800 --> 00:18:04,800
Be open to your experience.

208
00:18:04,800 --> 00:18:12,800
Don't be afraid of pain, don't be afraid of suffering, don't be afraid of your mind.

209
00:18:12,800 --> 00:18:18,800
Abhinya is going beyond, is rising above the conventional reality.

210
00:18:18,800 --> 00:18:20,800
That's what Abhinya means.

211
00:18:20,800 --> 00:18:22,800
In many ways, you can do this.

212
00:18:22,800 --> 00:18:26,800
In many conventional ways, and then there's through ultimate reality.

213
00:18:26,800 --> 00:18:36,800
By seeing through this space and time, seeing through people, places, things, concepts,

214
00:18:36,800 --> 00:18:40,800
seeing through it until you reach ultimate reality.

215
00:18:40,800 --> 00:18:51,800
And you can experience reality as experiences arising and ceasing.

216
00:18:51,800 --> 00:18:55,800
And when you break it down to your building blocks, then you can rebuild it.

217
00:18:55,800 --> 00:19:09,800
And you'll see it starts to rebuild itself.

218
00:19:09,800 --> 00:19:14,800
So they would have compared these two types of magic, and he said the magic.

219
00:19:14,800 --> 00:19:22,800
The magic of learning the magic and being taught and being instructed.

220
00:19:22,800 --> 00:19:29,800
This is the greatest type of magic, greatest type of magic that there is.

221
00:19:29,800 --> 00:19:34,800
So there you go.

222
00:19:34,800 --> 00:19:37,800
I'm just going to give a little bit of them every day.

223
00:19:37,800 --> 00:19:41,800
I'll give long talks because I'm here every day, so there you go.

224
00:19:41,800 --> 00:19:45,800
There's your development drop for today.

225
00:19:45,800 --> 00:19:46,800
Magic.

226
00:19:46,800 --> 00:19:51,800
Something we don't often talk about, but important nonetheless.

227
00:19:51,800 --> 00:20:00,800
Putting the magic of insight meditation in its place.

228
00:20:00,800 --> 00:20:09,800
Again, if you're interested in asking questions, you're welcome to go on to our meditation site and post them there.

229
00:20:09,800 --> 00:20:13,800
If you can't make it to this live session, or if you can't come into second life.

230
00:20:13,800 --> 00:20:20,800
So I'm just going to read off on these.

231
00:20:20,800 --> 00:20:25,800
Go through them.

232
00:20:25,800 --> 00:20:28,800
I recently learned to let go of my breathful meditating.

233
00:20:28,800 --> 00:20:32,800
After a while in meditation, my body seems to naturally incline towards long breaths,

234
00:20:32,800 --> 00:20:35,800
where I breathe only about four to six times per minute.

235
00:20:35,800 --> 00:20:39,800
This leaves a lot of time between the breaths, where I have nothing to note.

236
00:20:39,800 --> 00:20:44,800
The state between the breath seems empty, but it's pleasant and very calm is this correct.

237
00:20:44,800 --> 00:20:49,800
Or am I somehow unknowingly controlling the breath and slowing it down to get to this pleasant state?

238
00:20:49,800 --> 00:20:52,800
If it henchily, there's always that.

239
00:20:52,800 --> 00:20:56,800
But much more glaringly is the calm, which you should be noting.

240
00:20:56,800 --> 00:21:02,800
If you're very calm and pleased by it, you should note that.

241
00:21:02,800 --> 00:21:10,800
If you're calm, say calm, calm.

242
00:21:10,800 --> 00:21:14,800
And if you like it, say liking, liking, you're not noting that.

243
00:21:14,800 --> 00:21:16,800
And that will always condition things.

244
00:21:16,800 --> 00:21:20,800
That is important example of how the mind conditions things,

245
00:21:20,800 --> 00:21:27,800
because most likely the mind that is pleased by the contentment or the calm is conditioning the breath.

246
00:21:27,800 --> 00:21:35,800
It shouldn't normally be that slow.

247
00:21:35,800 --> 00:21:38,800
How does the mind usually move through the realms of existence?

248
00:21:38,800 --> 00:21:43,800
It is only one step at a time, or only upwards like this,

249
00:21:43,800 --> 00:21:50,800
an animal human god, or is it more chaotic, for instance, animal god human.

250
00:21:50,800 --> 00:21:58,800
There is a commentary that an animal can't go, or is this the tradition?

251
00:21:58,800 --> 00:22:00,800
Is that an animal?

252
00:22:00,800 --> 00:22:04,800
And you have to go through the human realm first, apparently.

253
00:22:04,800 --> 00:22:06,800
No, that's not true.

254
00:22:06,800 --> 00:22:09,800
An animal can become an angel.

255
00:22:09,800 --> 00:22:13,800
And an angel, I think, can become an animal.

256
00:22:13,800 --> 00:22:17,800
So in fact, those ones are all, I think, interchangeable.

257
00:22:17,800 --> 00:22:21,800
As I understand, angels can go directly to hell.

258
00:22:21,800 --> 00:22:23,800
The ghosts can become angels.

259
00:22:23,800 --> 00:22:26,800
Angels, I guess, can become ghosts.

260
00:22:26,800 --> 00:22:32,800
The four sensual realms are the five sensual realms.

261
00:22:32,800 --> 00:22:44,800
Hell, the animals, demons, I guess, and humans, and ghosts.

262
00:22:44,800 --> 00:22:46,800
It can all are interchangeable.

263
00:22:46,800 --> 00:22:49,800
But the Brahma realms, the Brahma realms are different.

264
00:22:49,800 --> 00:22:54,800
So to become a god, the tradition is that you have to be human.

265
00:22:54,800 --> 00:23:00,800
And from the Brahma realms, you can only be reborn as a human.

266
00:23:00,800 --> 00:23:03,800
There has to be an intermediary stage.

267
00:23:03,800 --> 00:23:04,800
This is the tradition.

268
00:23:04,800 --> 00:23:06,800
It may not be true, it may there may be cases.

269
00:23:06,800 --> 00:23:24,800
I don't know if the Buddha himself ever said that, but this is the tradition.

270
00:23:24,800 --> 00:23:28,800
Do you know a meditation technique that releases subconscious baggage?

271
00:23:28,800 --> 00:23:34,800
I know that meditation does that, but I wonder if you know a technique that doesn't even better.

272
00:23:34,800 --> 00:23:40,800
Okay, so here's an example of wanting to speed things along to find a shortcut.

273
00:23:40,800 --> 00:23:45,800
And as I've said many times, trying to find a shortcut in and of itself is a problem.

274
00:23:45,800 --> 00:23:50,800
Because it leads you to want to find shortcuts, and it's tried to attempt to control things.

275
00:23:50,800 --> 00:23:55,800
It's a lack of patience, and it's a lack of lots of other good qualities besides.

276
00:23:55,800 --> 00:23:57,800
So there's no good that can come of it.

277
00:23:57,800 --> 00:23:58,800
It's not useful.

278
00:23:58,800 --> 00:24:00,800
It's just going to add more baggage.

279
00:24:00,800 --> 00:24:01,800
It is baggage.

280
00:24:01,800 --> 00:24:05,800
You're desire to find a better technique than meditation is baggage.

281
00:24:05,800 --> 00:24:08,800
So, not a good idea.

282
00:24:08,800 --> 00:24:10,800
I also wonder about karma.

283
00:24:10,800 --> 00:24:13,800
In your karma video, you only talked about the suffering you will cause yourself if you think

284
00:24:13,800 --> 00:24:17,800
too harmful things, but what about doing positive stuff, but whether you could ego it?

285
00:24:17,800 --> 00:24:20,800
Ego it reasons, for example, raising money for charity.

286
00:24:20,800 --> 00:24:23,800
I usually don't do that to help other people.

287
00:24:23,800 --> 00:24:27,800
They always knew this, or they enhance their self-image.

288
00:24:27,800 --> 00:24:31,800
Just because you do a lot of good things in your life doesn't mean you get good karma.

289
00:24:31,800 --> 00:24:40,800
What karma is, karma is, for the most part, egotistical.

290
00:24:40,800 --> 00:24:45,800
The enlightened being doesn't actually perform karma, per se, because they have no attachment.

291
00:24:45,800 --> 00:24:48,800
It's not only egotistical, it's out of desire.

292
00:24:48,800 --> 00:24:52,800
You do good things out of a desire for good things.

293
00:24:52,800 --> 00:24:54,800
Even if it's just happiness, right?

294
00:24:54,800 --> 00:24:58,800
We help others because we want to be happy.

295
00:24:58,800 --> 00:25:02,800
But in enlightened being does good deeds just as a matter of course.

296
00:25:02,800 --> 00:25:04,800
It's called functional.

297
00:25:04,800 --> 00:25:09,800
They're inclined to good deeds simply by matter of course because they're pure.

298
00:25:09,800 --> 00:25:12,800
But they don't have any desire for happiness.

299
00:25:12,800 --> 00:25:26,800
They're already happy.

300
00:25:26,800 --> 00:25:31,800
If a person does things out of ego, it's increasing your ego.

301
00:25:31,800 --> 00:25:36,800
If you do good deeds because you want good things, it's increasing your desire.

302
00:25:36,800 --> 00:25:37,800
Those are problematic.

303
00:25:37,800 --> 00:25:40,800
Those are going to lead to suffering.

304
00:25:40,800 --> 00:25:45,800
There is something to be said that at the moment when you do a good deed, you don't have

305
00:25:45,800 --> 00:25:46,800
either of those things.

306
00:25:46,800 --> 00:25:50,800
You don't have egotism or you don't have desire or any of that.

307
00:25:50,800 --> 00:25:54,800
You have a wholesome mind state and that's why it leads to happiness.

308
00:25:54,800 --> 00:25:57,800
But there can be certainly a desire and egotism surrounding it.

309
00:25:57,800 --> 00:26:02,800
It's just not at the moment when you actually do a good deed because that's by definition,

310
00:26:02,800 --> 00:26:04,800
the good deed is free from those things.

311
00:26:04,800 --> 00:26:08,800
At the moment when you intend to help someone, there's none of that.

312
00:26:08,800 --> 00:26:12,800
But surrounding it, there's certainly canned beans and this is where we talk about white karma,

313
00:26:12,800 --> 00:26:18,800
black karma and white and black karma.

314
00:26:18,800 --> 00:26:23,800
I had in the morning done half walking and have sitting for 30 minutes and afterwards felt a great pull

315
00:26:23,800 --> 00:26:27,800
to do more feeling that I should be meditating or I need to meditate more.

316
00:26:27,800 --> 00:26:32,800
I do not believe that my issue is from not balancing walking and sitting because in the past I merely felt

317
00:26:32,800 --> 00:26:37,800
blissful, from just sitting and lying meditation or mainly lying meditation.

318
00:26:37,800 --> 00:26:44,800
I believe it's simply that I'm too attached and too greatly desire from the meditation practice.

319
00:26:44,800 --> 00:26:48,800
I'm too greatly clinging when you support this.

320
00:26:48,800 --> 00:26:49,800
I don't know.

321
00:26:49,800 --> 00:26:55,800
I don't really want to contemplate all the things you're saying but it's far too complicated.

322
00:26:55,800 --> 00:26:58,800
I think maybe you're overthinking things.

323
00:26:58,800 --> 00:27:04,800
If you have attachment and you desire something then just say wanting wanting.

324
00:27:04,800 --> 00:27:08,800
Even if it's wanting to meditate more you can still note it.

325
00:27:08,800 --> 00:27:13,800
I don't analyze it too much.

326
00:27:13,800 --> 00:27:18,800
Would you say not faculties but equanimity is the most important and that one should not

327
00:27:18,800 --> 00:27:26,800
fixate on the faculties?

328
00:27:26,800 --> 00:27:34,800
Yeah, I'm not in really too much of your own teaching in there.

329
00:27:34,800 --> 00:27:39,800
Try to make your questions a little shorter and clearer if you have a question for me.

330
00:27:39,800 --> 00:27:42,800
Not just I think this and this and this is that right.

331
00:27:42,800 --> 00:27:47,800
In some cases that works but it's not really what we're here for.

332
00:27:47,800 --> 00:27:52,800
Do you think some of those magical powers may be manifestations of not one's abilities but some other

333
00:27:52,800 --> 00:27:56,800
beings channeling through us.

334
00:27:56,800 --> 00:28:00,800
In my anthropology religion class we're studying the Jin.

335
00:28:00,800 --> 00:28:08,800
I used to think it was but there's a pronounced Jin in Palestine because our professor

336
00:28:08,800 --> 00:28:13,800
actually lived in the West Bank and studied the Jin stories of possession.

337
00:28:13,800 --> 00:28:15,800
Some of it was interesting.

338
00:28:15,800 --> 00:28:21,800
There were these Palestinians in Israeli jails and they would be visited by Jin who would tell

339
00:28:21,800 --> 00:28:26,800
them how their families were doing and what was going on with their families.

340
00:28:26,800 --> 00:28:34,800
And other things besides some like the people who would exercise the spirit would be able to

341
00:28:34,800 --> 00:28:38,800
tell all sorts of things like it all sounds quite magical.

342
00:28:38,800 --> 00:28:42,800
Makes you wonder whether there's some real magic going on there.

343
00:28:42,800 --> 00:28:46,800
In Buddhism there is a...

344
00:28:46,800 --> 00:28:47,800
a stance.

345
00:28:47,800 --> 00:28:53,800
There's a unit story in the windy half.

346
00:28:53,800 --> 00:28:56,800
One monk who goes to get a robe from the channel ground where they just throw the bodies

347
00:28:56,800 --> 00:29:05,800
and he sees this fresh corpse and it pulls the robe off of it and the spirit that's hanging

348
00:29:05,800 --> 00:29:12,800
around gets back into the corpse and chases after him and he has to run back to his

349
00:29:12,800 --> 00:29:17,800
scoot the being chased by the zombie until it falls down.

350
00:29:17,800 --> 00:29:21,800
He just close shuts his door and it falls down dead at his door.

351
00:29:21,800 --> 00:29:23,800
I think the Buddha gave some advice.

352
00:29:23,800 --> 00:29:25,800
I can't remember what the advice was.

353
00:29:25,800 --> 00:29:30,800
Make sure it's dead or wait for a few days before you take the robe or something like that.

354
00:29:30,800 --> 00:29:34,800
Don't go alone to the channel ground maybe it was.

355
00:29:34,800 --> 00:29:37,800
I think Buddhism does.

356
00:29:37,800 --> 00:29:42,800
There's certainly there are certain remedies in the medicines and there's a really important

357
00:29:42,800 --> 00:29:46,800
medicine how to deal with spirit possession.

358
00:29:46,800 --> 00:29:53,800
So it's okay if you eat blood and raw flesh when you're possessed by a demon that you don't break a

359
00:29:53,800 --> 00:29:57,800
robe because you're possessed by a demon.

360
00:29:57,800 --> 00:30:02,800
So yeah there certainly is an admission of that.

361
00:30:02,800 --> 00:30:07,800
I have a belief in that sort of thing.

362
00:30:07,800 --> 00:30:08,800
Did I have a live question?

363
00:30:08,800 --> 00:30:10,800
Did I see a live question here somewhere?

364
00:30:10,800 --> 00:30:14,800
Let me see.

365
00:30:14,800 --> 00:30:18,800
Doesn't the commentary say that there won't be our hunt to have mastered the abinias after the first

366
00:30:18,800 --> 00:30:21,800
1000 years from Buddha, Perinibana?

367
00:30:21,800 --> 00:30:23,800
Perinibana is all six.

368
00:30:23,800 --> 00:30:24,800
Yes.

369
00:30:24,800 --> 00:30:28,800
But I mean it's just a commentary and I think it's I don't know.

370
00:30:28,800 --> 00:30:33,800
Some of those believe them if you like don't believe them if you don't like.

371
00:30:33,800 --> 00:30:38,800
I mean these kind of very set statements.

372
00:30:38,800 --> 00:30:46,800
You have to want to say it's a bit strict you know.

373
00:30:46,800 --> 00:30:51,800
I mean why should it be that exactly after 1000 years suddenly there are no more.

374
00:30:51,800 --> 00:30:56,800
But after you know two and a half thousand years is there likely to be anyone with all six?

375
00:30:56,800 --> 00:30:58,800
Not very likely.

376
00:30:58,800 --> 00:31:00,800
Is it possible that there are none?

377
00:31:00,800 --> 00:31:06,800
What's quite possible that there are none?

378
00:31:06,800 --> 00:31:24,800
I guess it's maybe quite likely that at this point there are none.

379
00:31:24,800 --> 00:31:27,800
Alright well that's it for the questions.

380
00:31:27,800 --> 00:31:28,800
Thank you all for coming.

381
00:31:28,800 --> 00:31:55,800
I wish you a good night.

