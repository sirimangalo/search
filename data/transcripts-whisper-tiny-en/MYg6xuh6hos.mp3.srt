1
00:00:00,000 --> 00:00:13,940
Good evening, broadcasting live from British Columbia and wherever Robman is from

2
00:00:13,940 --> 00:00:30,280
Meghan, I always forget. Not messages, it's a Connecticut. August 15th, 2015, 2015.

3
00:00:30,280 --> 00:00:40,700
Yeah. So today we had the first day of our meditation course. Last night I

4
00:00:40,700 --> 00:00:47,920
gave a talk to a fairly small group and they recorded it for YouTube but that

5
00:00:47,920 --> 00:00:56,940
didn't go very well and the audio was pretty awful so I can probably pull it up

6
00:00:56,940 --> 00:01:14,660
here. Let me see if I can show you it. I don't think you could hear it, but...

7
00:01:14,660 --> 00:01:43,940
I'll show you a picture there do you see me do you see that yes yeah actually it

8
00:01:43,940 --> 00:01:52,100
looks really good. The picture was fine. First problem was he had this

9
00:01:52,100 --> 00:01:59,900
camera set to take 30-minute shots. After 30 minutes it cut out and he had to

10
00:01:59,900 --> 00:02:02,540
start it again.

11
00:02:02,540 --> 00:02:18,140
The funny thing was there was some sort of filter on it that my voice was

12
00:02:18,140 --> 00:02:24,500
fine when I spoke but when I wasn't speaking it was a very loud hissing so if

13
00:02:24,500 --> 00:02:30,420
someone out there would like to take the time to remove the hissing from the

14
00:02:30,420 --> 00:02:38,100
silent parts of the audio that would be about it. If you can do that without

15
00:02:38,100 --> 00:02:47,340
hurting the actual speech then we could post it but I think the really the best

16
00:02:47,340 --> 00:02:52,740
way to do that would be to go through the one hour long talk and manually silence

17
00:02:52,740 --> 00:03:02,540
each his could probably take some time. It's cut up into half hour chunks so

18
00:03:02,540 --> 00:03:06,820
someone could take half and another person could take half to see if we get

19
00:03:06,820 --> 00:03:10,980
any volunteers for that. We have our volunteer group that will be meeting on

20
00:03:10,980 --> 00:03:16,300
Sunday nights starting next Sunday so maybe we'll give volunteers for things

21
00:03:16,300 --> 00:03:22,660
like that. Why it was a pretty good talk. I mean it went over quite well and

22
00:03:22,660 --> 00:03:30,700
I didn't forget myself or get lost or anything covered all the points. It's a

23
00:03:30,700 --> 00:03:34,740
talk I've given before so I'm pretty sure it's already on YouTube a couple of

24
00:03:34,740 --> 00:03:40,060
times but it was a good version of it was a talk about the five types of people

25
00:03:40,060 --> 00:03:51,900
get there being a dummy hurry something to talk about often. So tonight we have a

26
00:03:51,900 --> 00:04:01,300
quote and we have only a few people logged in what's happening there everyone

27
00:04:01,300 --> 00:04:22,060
still meditating. So this quote is about criticism criticizing and praise about

28
00:04:22,060 --> 00:04:32,420
passing judgment. It's about the debate over action and inaction and the

29
00:04:32,420 --> 00:04:42,620
Buddha seems to have overwhelmingly favored action over inaction. Now I'm

30
00:04:42,620 --> 00:04:47,580
not sure why whether that was just because of his own mission which he had

31
00:04:47,580 --> 00:04:53,220
started on because originally he inclined to inaction. So it may be that his

32
00:04:53,220 --> 00:04:58,700
exhortation for his students to incline towards action was just a part of his

33
00:04:58,700 --> 00:05:06,420
own intention once he had made up his mind to teach that therefore it was

34
00:05:06,420 --> 00:05:12,580
proper for his students to incline towards action because this is a question

35
00:05:12,580 --> 00:05:19,660
of whether it's good to stay upon miss and not criticize or praise others not

36
00:05:19,660 --> 00:05:27,580
to get involved and give your opinion whether that is more praised worthy than

37
00:05:27,580 --> 00:05:36,180
then the effective change. I think most of us would agree with the Buddha here

38
00:05:36,180 --> 00:05:44,940
unconditionally but I think we do that out of a sense of a desire to see

39
00:05:44,940 --> 00:05:50,060
things change to see things get better which I think the Buddha pretty clearly

40
00:05:50,060 --> 00:05:57,060
didn't have. Buddha had his mission but it was it was out of invitation and it

41
00:05:57,060 --> 00:06:05,940
was a part of him being the Buddha. So a lot of our hands became guilty of of

42
00:06:05,940 --> 00:06:15,340
inaction and it seems as though that was a viable behavior for an

43
00:06:15,340 --> 00:06:19,820
arahan. I mean in some to some extent an arahan doesn't concern about whether

44
00:06:19,820 --> 00:06:26,340
it's right or wrong. They don't have a sense of feeling guilty or embarrassed

45
00:06:26,340 --> 00:06:30,300
that they're not helping that they're not changing the world. They don't worry

46
00:06:30,300 --> 00:06:37,380
or feel sad because the world is a mess or because people are suffering. They

47
00:06:37,380 --> 00:06:41,500
don't have any of that. If they did and then how can you say that they are

48
00:06:41,500 --> 00:06:46,100
from how can you say that they're unattached that they have given up

49
00:06:46,100 --> 00:06:53,700
craving. I think a lot of people take that the wrong way as well and try to

50
00:06:53,700 --> 00:06:59,820
emulate the arahans in this regard and and become lazy and do nothing. The

51
00:06:59,820 --> 00:07:03,980
reason an arahan is is of that bend is because they've done everything they've

52
00:07:03,980 --> 00:07:11,060
done all that needs to be. For the rest of us it could be considered laziness to

53
00:07:11,060 --> 00:07:18,220
do that because all the good things helping others and so on is work in the

54
00:07:18,220 --> 00:07:23,980
right direction. It's a support for our practice. It surrounds us with good people

55
00:07:23,980 --> 00:07:31,300
and keeps our minds happy and confident and inclined towards happiness and

56
00:07:31,300 --> 00:07:41,480
peace. But for an arahan they have no goal left no destination after

57
00:07:41,480 --> 00:07:53,260
each. And yet the Buddha seems to be pushing for even such pains to act. And in

58
00:07:53,260 --> 00:07:57,780
this is subtle he's not giving an injunction here but he's giving his opinion

59
00:07:57,780 --> 00:08:13,340
that it is more admirable. And it's his name Bodalya. Bodalya uses the word

60
00:08:13,340 --> 00:08:19,020
Upeek. He translates here as indifference, but Upeek means literally or he

61
00:08:19,020 --> 00:08:25,020
usually translated as equanimity. It's used for the fourth Brahmuihaar among other

62
00:08:25,020 --> 00:08:29,900
things. But many types of equanimity and so he's interpreting this one to

63
00:08:29,900 --> 00:08:34,540
mean indifference. But in the really means equanimity and only how Bodalya

64
00:08:34,540 --> 00:08:38,900
meant it. He didn't mean that the guy was indifferent. Someone like that is

65
00:08:38,900 --> 00:08:49,820
equanimous. They're undisturbed by good people or bad people. Another way of

66
00:08:49,820 --> 00:08:54,500
looking at this is the potential idea is what is the path of least

67
00:08:54,500 --> 00:08:59,100
for distance. What would an arahan really do? And I don't think it's clear to

68
00:08:59,100 --> 00:09:04,700
say that an arahan would incline towards inaction in this case. If confronted

69
00:09:04,700 --> 00:09:11,500
at the right time with an opportunity to criticize that which is worthy of

70
00:09:11,500 --> 00:09:16,500
criticism or to praise that which is worthy of praise. It's not clear that they

71
00:09:16,500 --> 00:09:23,940
wouldn't incline towards inaction. It seems to me actually quite reasonable

72
00:09:23,940 --> 00:09:29,060
to suggest that they would speak when they have the opportunity. Sometimes

73
00:09:29,060 --> 00:09:33,740
they didn't. There was the case. If you read the Vinay, there's interesting

74
00:09:33,740 --> 00:09:37,860
cases, one of which is regards to the third council. There were many

75
00:09:37,860 --> 00:09:46,820
monks who were breaking rules. Wait, is it second council? I think second

76
00:09:46,820 --> 00:09:53,220
council. Second council. Many monks were breaking rules and they went to this

77
00:09:53,220 --> 00:09:58,500
very senior monk who was a believer in arahan and he didn't say anything. He

78
00:09:58,500 --> 00:10:05,620
wouldn't give a judgment. I think the story goes that they were waiting for

79
00:10:05,620 --> 00:10:10,300
the judgment of the chief or the most senior monk or our monk who was an

80
00:10:10,300 --> 00:10:15,980
expert in the Vinay, so that's why they didn't answer. But there certainly

81
00:10:15,980 --> 00:10:22,940
seem to be cases where they incline towards inaction. Anyway, certainly as

82
00:10:22,940 --> 00:10:28,460
followers of the Buddha and out of respect for his teaching, respect for the

83
00:10:28,460 --> 00:10:36,420
fact that he took it upon himself to teach and made it a mission to spread

84
00:10:36,420 --> 00:10:43,020
the dhamma to give to share. This is because criticism and praise is an

85
00:10:43,020 --> 00:10:52,700
important part of any religious spiritual teaching. The establishment

86
00:10:52,700 --> 00:10:57,820
establishing where we stand, where do we stand on these issues? Where do we stand

87
00:10:57,820 --> 00:11:05,940
on things like abortion? Where do we stand on homosexuality? Where do we stand on

88
00:11:05,940 --> 00:11:17,620
racism or I don't know any of these things? So praising and criticizing, we

89
00:11:17,620 --> 00:11:24,340
don't criticize homosexuality. We make criticize sexuality in general. We criticize

90
00:11:24,340 --> 00:11:35,860
abortion and that sort of thing. And drugs, alcohol, criticizing these things.

91
00:11:35,860 --> 00:11:41,460
More importantly, most importantly, we criticize unhosenness and we praise

92
00:11:41,460 --> 00:11:47,500
unhosenness. If the mind has a wholesome quality to it, we praise that. A person

93
00:11:47,500 --> 00:11:54,180
who is inclined towards wholesomeness, we praise the wholesomeness. I think an

94
00:11:54,180 --> 00:11:58,100
interesting question is about whether you praise the act or you praise the

95
00:11:58,100 --> 00:12:05,020
person. If this hate the sin or not, hate the sin, not the sinner is a Christian

96
00:12:05,020 --> 00:12:12,340
common Christian praise. So I think that's an interesting thing too. I mean, it's

97
00:12:12,340 --> 00:12:17,340
fairly obvious that Buddhism would be very much of that balance. We don't

98
00:12:17,340 --> 00:12:23,380
exactly criticize people, but that's important, I think, because it can never

99
00:12:23,380 --> 00:12:26,420
be more important than Buddhism especially because you can never say that a

100
00:12:26,420 --> 00:12:32,380
person is good or a person is evil. Because people don't exist. And so we have

101
00:12:32,380 --> 00:12:36,740
to be very careful when we praise that we don't praise people. It would be in

102
00:12:36,740 --> 00:12:42,060
proper to do so. Not only dangerous, it is dangerous to do so, but in proper. And

103
00:12:42,060 --> 00:12:49,220
maybe dangerous because it's improper. If you praise someone, a person, then you

104
00:12:49,220 --> 00:12:54,700
have to own up to their evil deeds. If they then break from their trend of

105
00:12:54,700 --> 00:12:58,820
being a good person, which is completely possible, anytime they do something

106
00:12:58,820 --> 00:13:09,100
wrong, you get stuck because you praise that person. So praising people is

107
00:13:09,100 --> 00:13:13,100
something that can be that it is a great danger. And we should be careful to

108
00:13:13,100 --> 00:13:19,340
be clear what we are praising, which is why one reason probably why this says

109
00:13:19,340 --> 00:13:24,740
praise that, which is praise or it doesn't talk about beings. Praise the acts,

110
00:13:24,740 --> 00:13:32,300
we praise the mind states, we criticize acts in mind state. There's also much

111
00:13:32,300 --> 00:13:36,060
safer in the sense that you don't make enemies and you don't people don't

112
00:13:36,060 --> 00:13:43,380
say, we'll put you on the side. And politics, for example, right now there's an

113
00:13:43,380 --> 00:13:48,820
election here in Canada, there's an election in America. I mean, there's always an

114
00:13:48,820 --> 00:13:52,180
election because it's become a circus and they just like to have it always on

115
00:13:52,180 --> 00:13:56,940
the news. Right after this election, over people will be talking about the

116
00:13:56,940 --> 00:14:03,300
next one. It's how we, it's part of entertainment in the West, I think, but it's

117
00:14:03,300 --> 00:14:07,940
a bit interesting sometimes because it deals with issues of morality and

118
00:14:07,940 --> 00:14:14,420
people are as are forced to ask themselves where they stand on issue, like

119
00:14:14,420 --> 00:14:28,780
abortion, homosexuality. Right now, a big one is is police brutality and racism.

120
00:14:28,780 --> 00:14:34,820
Racism is a funny word just to get off track here, but to talk about things

121
00:14:34,820 --> 00:14:39,140
that are worth criticizing or worth praising because from Buddhist point of

122
00:14:39,140 --> 00:14:44,700
we were all one race, right? It's funny because we all know we are one race and

123
00:14:44,700 --> 00:14:51,620
everyone knows that. And so the idea of racism, I don't know if I want to really

124
00:14:51,620 --> 00:14:54,780
get into it because it's it's much more than just the word race, but it's

125
00:14:54,780 --> 00:15:02,900
been misleading. We're none of us white or black or brown.

126
00:15:02,900 --> 00:15:16,420
I guess we're a little bit pink. I think I'm actually on the number ones

127
00:15:16,420 --> 00:15:23,020
one called the kind of a black jot. And it's a wonderful jot. If you read this

128
00:15:23,020 --> 00:15:29,660
number 440, you can find it on sacred and dash texts dot org. You can find the

129
00:15:29,660 --> 00:15:34,620
whole jot. Because if you read number 440, it's a wonderful jot. But it's about

130
00:15:34,620 --> 00:15:41,140
some guy who was very dark skin. The body said to had black skin or dark skin.

131
00:15:41,140 --> 00:15:49,700
And the king is a king down black. And he said, I'm not black. Black is evil.

132
00:15:49,700 --> 00:15:56,260
Black is a cusada. It's unwholesome. But it's more than a very beautiful

133
00:15:56,260 --> 00:16:02,860
conversation that he has with the king of the gods. But racism isn't about that.

134
00:16:02,860 --> 00:16:10,900
The issue of racism is people are very different. And don't respect

135
00:16:10,900 --> 00:16:18,860
differences. And much more than that are openly cruel and hostile and evil

136
00:16:18,860 --> 00:16:24,260
towards people because of the color of their skin, because of their culture,

137
00:16:24,260 --> 00:16:32,540
et cetera, which of course Buddhism would criticize. So, but my point was

138
00:16:32,540 --> 00:16:36,700
about elections. It's very interesting to talk about the issues and to look at

139
00:16:36,700 --> 00:16:43,140
them. But I don't think you could ever say you should support one person or

140
00:16:43,140 --> 00:16:50,500
another in an election. I mean, obviously, as people we do and voting isn't

141
00:16:50,500 --> 00:16:56,660
wrong as a Buddhist, I don't think, because you're choosing, right? You have to

142
00:16:56,660 --> 00:17:00,620
choose between a sentence or you choose. Doesn't mean that you support

143
00:17:00,620 --> 00:17:07,900
everything that person's status or you believe in it. Absolutely. But from a

144
00:17:07,900 --> 00:17:11,900
monastic point of view, there isn't, there is a point there to be made.

145
00:17:11,900 --> 00:17:19,540
That monastic stand that we take and should take is to never endorse one candidate

146
00:17:19,540 --> 00:17:25,940
to talk about the issues, to praise acts in the speech.

147
00:17:25,940 --> 00:17:34,660
The person says this, this great thing. If you endorse one, then you make

148
00:17:34,660 --> 00:17:39,580
enemies with the other. And monks are not in the position to do that. We're not in

149
00:17:39,580 --> 00:17:47,460
society. We're not able to take the Buddha as, Buddha as our example. In order to

150
00:17:47,460 --> 00:17:51,700
be an argument with a monk about this one, he said that we shouldn't take

151
00:17:51,700 --> 00:17:56,940
signs on issues. He was talking about the Middle East. We should take

152
00:17:56,940 --> 00:18:01,460
signs. That's how else can the number be effective. And that's exactly how it's

153
00:18:01,460 --> 00:18:06,700
effective, because it doesn't take signs. And it teaches what is right, what it

154
00:18:06,700 --> 00:18:14,020
is true. It is an outsider. By not taking sides, by not getting involved, we're

155
00:18:14,020 --> 00:18:22,180
able to teach the truth. We're able to teach objectively. And without getting

156
00:18:22,180 --> 00:18:28,300
caught up, and letting our emotions take over. Anyway, there isn't that much to

157
00:18:28,300 --> 00:18:35,540
this quote, I don't think. It's an interesting one. Interesting for us to learn.

158
00:18:35,540 --> 00:18:42,300
But I've been teaching all day since eight o'clock. And you wouldn't believe I

159
00:18:42,300 --> 00:18:48,260
had 40 people. Today's the course we had 40 people. I think maybe they didn't

160
00:18:48,260 --> 00:18:54,460
know Shavat, but there were 40 people signed up for the course. Probably we had

161
00:18:54,460 --> 00:19:01,580
a little bit less than that. But they were, I had a room off to the side and in

162
00:19:01,580 --> 00:19:06,460
and out all day, people were coming in to see me all day, all day. I met with

163
00:19:06,460 --> 00:19:13,140
at least 40 people. Some people came in twice three times asking questions and

164
00:19:13,140 --> 00:19:19,340
all sorts of questions. And that's the best. That's really the best form of

165
00:19:19,340 --> 00:19:24,860
teaching that I know of, because it's one-on-one. It's personal. You get to

166
00:19:24,860 --> 00:19:30,300
act. You get to take the role of a therapist or a psychologist. But by

167
00:19:30,300 --> 00:19:36,340
teaching Buddhism, you have one and one percent confidence and defectiveness. And you

168
00:19:36,340 --> 00:19:44,180
really can change people's lives. So that's awesome. So today was a good day. We

169
00:19:44,180 --> 00:19:55,220
have one more day tomorrow. Are there any questions tonight? There are some

170
00:19:55,220 --> 00:19:59,860
questions. Well, you see, I'm not updating. This page doesn't always update. So

171
00:19:59,860 --> 00:20:05,740
I see nothing there. How do you interpret differences among countries from

172
00:20:05,740 --> 00:20:28,300
Buddhist point of view? Well, habit, really. It's become cult. In every

173
00:20:28,300 --> 00:20:37,300
country, habit, it's a glance towards a certain habit in general. And due to

174
00:20:37,300 --> 00:20:43,940
isolation, habits only catch on in in one specific culture. Now that's

175
00:20:43,940 --> 00:20:49,380
changing now, right? Habits become international. So culture becomes

176
00:20:49,380 --> 00:20:59,460
international, but the internet culture. And then once they become culture,

177
00:20:59,460 --> 00:21:17,140
then views mix in like we are I am Canadian. And we did this connection.

178
00:21:17,140 --> 00:21:24,300
We did with the live stream. Let me know there started it again. It shows

179
00:21:24,300 --> 00:21:30,820
right there. I should look at that. But culture becomes views. And then it's

180
00:21:30,820 --> 00:21:38,780
like I am Canadian. I am American. I am French. I am British. I am German. And

181
00:21:38,780 --> 00:21:43,460
then it follows you from life to life. If I would assume people are reborn in

182
00:21:43,460 --> 00:21:53,820
the same country. And so it becomes incredibly ingrained at any time. But views

183
00:21:53,820 --> 00:22:04,020
reinforce culture. Then religion. I believe that something is right. There's the

184
00:22:04,020 --> 00:22:08,100
thought and there's the behavior. But then there's the affirmation that this is

185
00:22:08,100 --> 00:22:25,180
right and proper in me and mine. But in the end, it's all just habits. So

186
00:22:25,180 --> 00:22:33,660
it's habits and views. So your chances are, you could also say it's craving.

187
00:22:33,660 --> 00:22:45,540
It's attachment. Like you're sure there is that when you when if you know a

188
00:22:45,540 --> 00:22:52,740
French person eats a with a crepe and gets pleasure from it. And a British

189
00:22:52,740 --> 00:23:04,140
person eats a whatever British people in English muffin. Pudding with the British

190
00:23:04,140 --> 00:23:11,380
biscuits. Let's get tea. Whatever I'd biscuit and gets pleasure about it. Pleasure

191
00:23:11,380 --> 00:23:15,940
from it. Then the one person's brain is wired for craves and the other

192
00:23:15,940 --> 00:23:21,340
person's brain is wired for. I mean, cultural food is wired for a specific type of

193
00:23:21,340 --> 00:23:26,860
food. And then they don't have that connection with the other types of food,

194
00:23:26,860 --> 00:23:40,580
with other cultural types of food, not in the same way. And music. So becomes

195
00:23:40,580 --> 00:23:44,740
very much ingrained. It's really a human. We would say humans are that way,

196
00:23:44,740 --> 00:23:49,860
being the state of being a human is just an ingrained habit. Being a specific

197
00:23:49,860 --> 00:23:55,060
type of animal is just a habit. That's become so terribly ingrained that it

198
00:23:55,060 --> 00:24:01,580
becomes an identification. I am human. I am dog. I am this. So we're born again

199
00:24:01,580 --> 00:24:11,780
and again the same way. See, that's where mutation comes from from deviants.

200
00:24:11,780 --> 00:24:21,740
Mediviants isn't rewarded by the group. So for humans, we stay very much

201
00:24:21,740 --> 00:24:28,660
fairly much the same because we attach to this state. So when someone is

202
00:24:28,660 --> 00:24:34,780
different, different color skin, different height, different weight, you know,

203
00:24:34,780 --> 00:24:43,820
any sort of mutation is immediately look down.

204
00:24:45,540 --> 00:24:52,140
It's not really an invitation question. No, isn't. It's not exactly. It's

205
00:24:52,140 --> 00:24:55,380
been interesting. Sorry, rather than I interrupted you.

206
00:24:55,380 --> 00:25:00,980
Oh, but you actually were answering what I was going to ask about. So yeah, that's

207
00:25:00,980 --> 00:25:06,940
just so interesting that you're more likely to be reborn in in the same

208
00:25:06,940 --> 00:25:12,300
culture that you're in. Not necessarily advantageous for those of us in the

209
00:25:12,300 --> 00:25:17,700
West where there are so few Buddhists here that might be more advantageous to

210
00:25:17,700 --> 00:25:20,980
be born elsewhere, maybe.

211
00:25:20,980 --> 00:25:30,580
There are benefits to be being born in a free country. Many Buddhist countries have had

212
00:25:30,580 --> 00:25:56,180
to have their share. It's a good point. Ready for another question? I'm new to

213
00:25:56,180 --> 00:26:00,220
Buddhism and I don't know where to start learning more about the religion. Is

214
00:26:00,220 --> 00:26:03,100
there a book I can read that will teach me about the core fundamentals of

215
00:26:03,100 --> 00:26:12,780
Buddhism? Well, there's the book with the Buddha Todd, which we always

216
00:26:12,780 --> 00:26:17,220
recommend. I'm not sure how much I recommend it in the end, but it's pretty

217
00:26:17,220 --> 00:26:33,420
good. You can always read my book, let me just start meditating.

218
00:26:33,420 --> 00:26:45,660
Good. Come meditate with us. So I started. I went for a month's meditation course,

219
00:26:45,660 --> 00:26:48,980
not knowing hardly anything about Buddhism, certainly nothing about

220
00:26:48,980 --> 00:26:54,380
teravada Buddhism. And after that month I was hooked and I went and read

221
00:26:54,380 --> 00:26:58,980
everything. Started reading directly the Buddha's teaching. I read the Visudhi

222
00:26:58,980 --> 00:27:05,900
manga, not all of it, but much because I had gone to practice meditation first.

223
00:27:05,900 --> 00:27:25,500
Meditation opened up a whole new world. So what's Jataka? Was it? No.

224
00:27:25,500 --> 00:27:32,300
Let's go see. I mean, it had the word kana in it, so that's a problem.

225
00:27:32,300 --> 00:27:39,300
And after that, I didn't think it was 444. Maybe it wasn't.

226
00:28:02,300 --> 00:28:17,780
No, 440 kana in Jataka. That was right. The black Jataka.

227
00:28:17,780 --> 00:28:26,220
Kanho, what a young person, so black indeed is this man. Kanhang Boonjati Boonjana,

228
00:28:26,220 --> 00:28:32,660
and black is the food that he eats. Kanhang Boonjana, but they

229
00:28:32,660 --> 00:28:43,820
sesame not my Hang Manasopio in a black place, in a black realm, does he

230
00:28:43,820 --> 00:28:58,620
have a black realm and country? Not my Hang Manasopio. Our mind is not, my mind is

231
00:28:58,620 --> 00:29:07,820
not dear. He is not dear, but he is not dear to my mind. This is the King of the God's

232
00:29:07,820 --> 00:29:15,420
talking. He's a terrible racist, but it's a wonderful story because he really

233
00:29:15,420 --> 00:29:38,260
sets him straight.

234
00:29:38,260 --> 00:29:47,060
So you can find them there, but they're at sacred-texts.org.

235
00:29:47,060 --> 00:29:48,060
Thank you.

236
00:29:48,060 --> 00:29:57,500
I got them from all 657 and 547. Behold, the Hang Man, all black of hue that dwells

237
00:29:57,500 --> 00:30:06,660
on this black spot. Black is the meat that he does eat. My spirit likes him not.

238
00:30:06,660 --> 00:30:12,540
And then he replies, the black of hue, a brahmin true at heart, Osaka sea, not by the

239
00:30:12,540 --> 00:30:19,860
skin, but if he sinned, then black of man must be. Fair spoken brahmin, nobody put

240
00:30:19,860 --> 00:30:25,180
the most excellently said, choose what you will as bids your heart, so let your choice

241
00:30:25,180 --> 00:30:26,180
be made.

242
00:30:26,180 --> 00:30:29,580
That's not quite right.

243
00:30:29,580 --> 00:30:35,380
Saka the Lord of all the world, a choice of blessings gained, from malice hatred, covetous,

244
00:30:35,380 --> 00:30:44,820
ignorance, I would hate, and to be free from every lust, these blessings for I crave.

245
00:30:44,820 --> 00:31:05,180
You have to read the whole job together. Very much worth reading. One of my favorites.

246
00:31:05,180 --> 00:31:08,740
When you have two different processes going on during meditation, like feeling pain and

247
00:31:08,740 --> 00:31:14,740
feeling an itch at the same time, how should you note this? Pick whichever one is clearest.

248
00:31:14,740 --> 00:31:20,340
That's all. It's not that important. As long as you pick one, then try to find the one

249
00:31:20,340 --> 00:31:28,140
that's clear.

250
00:31:28,140 --> 00:31:44,060
What do you explain how to relate the four foundations with the meditation practice?

251
00:31:44,060 --> 00:32:08,060
This is the second.

252
00:32:08,060 --> 00:32:11,540
How to relate the four foundations with the meditation practice?

253
00:32:11,540 --> 00:32:19,540
I don't understand. Have you read my booklet?

254
00:32:19,540 --> 00:32:30,940
If you haven't, please read my booklet. Sounds like it might answer your question.

255
00:32:30,940 --> 00:32:36,380
Thank you for the link there for the sacred texts.

256
00:32:36,380 --> 00:32:43,380
What are the benefits of chanting? What chant would you recommend to recite daily?

257
00:32:43,380 --> 00:32:52,940
I wouldn't really. I guess you need a chant, right?

258
00:32:52,940 --> 00:33:07,500
It's a chant. You should do it. You should do it. You should do it. You should do it.

259
00:33:07,500 --> 00:33:11,260
You can knock them up. A recollection of the Buddha, a recollection of the Dhamma, a recollection

260
00:33:11,260 --> 00:33:15,540
of the song. That was every Buddhist, I think. You should know.

261
00:33:15,540 --> 00:33:20,580
What even said you should know them? You should chant them whenever you're afraid because

262
00:33:20,580 --> 00:33:43,580
they would free you from fear to spell the fear.

263
00:33:43,580 --> 00:33:47,700
Or you could come to our poly group and take refuge in poly at the beginning of the class.

264
00:33:47,700 --> 00:34:02,700
We do that every Sunday. It's nice.

265
00:34:02,700 --> 00:34:07,540
One day, is it best to try to relocate to Sri Lanka or Thailand during our toilet years to

266
00:34:07,540 --> 00:34:14,140
pick up on the path again? Or is it a roll of the dice? That's what I was thinking.

267
00:34:14,140 --> 00:34:26,940
Try to shift the odds there to be in a more Buddhist environment.

268
00:34:26,940 --> 00:34:32,700
I guess Sri Lanka is becoming a better place. I definitely recommend Sri Lanka.

269
00:34:32,700 --> 00:34:41,580
And I hear it's a better place now, even more because of the stability of the government.

270
00:34:41,580 --> 00:34:47,860
I mean, the government has problems, but where doesn't it?

271
00:34:47,860 --> 00:34:52,940
But in Sri Lanka, there is peace now. There's no more civil war.

272
00:34:52,940 --> 00:35:02,620
So that always makes life easier. And it's a great culture for practice.

273
00:35:02,620 --> 00:35:05,740
So yeah, I think that could potentially be a good thing.

274
00:35:05,740 --> 00:35:12,060
The thing about Sri Lanka is that from what I see, among even a foreigner, potentially could just get lost.

275
00:35:12,060 --> 00:35:20,340
Even if you're a layperson, if you're a layperson who has like a pension and living in Sri Lanka would be easy.

276
00:35:20,340 --> 00:35:28,660
The problems are dengue, really. I would say the one big problem with Sri Lanka is it's got a lot of dengue, dengue problem.

277
00:35:28,660 --> 00:35:37,700
So you have to be aware that there's a potential to get a fairly problematic sickness.

278
00:35:37,700 --> 00:35:41,300
I don't know, Sri Lankan people would like to hear me say that because they all come out.

279
00:35:41,300 --> 00:35:47,260
But I got in when I was there. Of course, I lived in a living with mosquitoes.

280
00:35:47,260 --> 00:35:52,340
But if you want to live in the forest of Sri Lanka, there's going to be mosquitoes and potentially dengue.

281
00:35:52,340 --> 00:35:58,500
Quite a lot of dengue there.

282
00:35:58,500 --> 00:36:00,500
As for others.

283
00:36:12,660 --> 00:36:15,140
Sorry. Hello. Sorry.

284
00:36:15,140 --> 00:36:17,620
Hi, you're back.

285
00:36:17,620 --> 00:36:19,780
Yeah.

286
00:36:19,780 --> 00:36:32,180
Panty does Sri Lanka welcome immigrants. Yeah. Yeah, and more so especially now.

287
00:36:32,180 --> 00:36:39,300
There's a big, in a big tourism push, I think, or I think, I'm actually not sure.

288
00:36:42,980 --> 00:36:45,780
Immigrants, like people migrating there.

289
00:36:45,780 --> 00:36:54,580
Yes, you know, we were asking about going to live in Sri Lanka, you know, in later years to pick up on the culture.

290
00:36:54,580 --> 00:37:01,780
Yeah, well, you don't have to become a citizen. I think becoming a citizen is pretty difficult.

291
00:37:01,780 --> 00:37:11,780
But going to live there on a non-immigrant, or I don't know, a resident visa, I think that's too long.

292
00:37:11,780 --> 00:37:20,980
Probably a little expensive to get the visa a bit. Once you got it, you live there.

293
00:37:20,980 --> 00:37:28,980
I guess I don't know whether it would be any better than living here as a, as a retiree.

294
00:37:28,980 --> 00:37:34,580
Doing, doing the dhamma, it certainly would be more comfortable here culturally.

295
00:37:34,580 --> 00:37:46,180
You know, the food here would be more suitable. If you can find a place in your own own land,

296
00:37:46,180 --> 00:37:50,180
it's often better just for the car.

297
00:37:50,180 --> 00:37:55,060
And that's important, you know, if you have the wrong type of food, everybody's been used to

298
00:37:55,060 --> 00:37:58,580
a certain type of food, and then you have to adjust to a whole new type of food.

299
00:37:58,580 --> 00:38:04,580
Sri Lankan food is very salty. It's very healthy. It's very good food, but it's very salty.

300
00:38:04,580 --> 00:38:10,180
It's just different, you know, for all the people, I think that's more difficult,

301
00:38:10,180 --> 00:38:14,580
because your body's been used to something for a longer time.

302
00:38:22,580 --> 00:38:28,180
When I meditate for long, like for one hour, I used to get terrible pain in the knee,

303
00:38:28,180 --> 00:38:34,980
which does not go off, even though I become mindful as pain, pain. It was unbearable.

304
00:38:34,980 --> 00:38:39,780
I still tried to become without getting frustrated. Would you please advise on this?

305
00:38:42,660 --> 00:38:46,500
Well, if you get frustrated, then you focus on the frustration. If you don't like it,

306
00:38:46,500 --> 00:38:48,980
then say, dislike it. If it's really unbearable,

307
00:38:48,980 --> 00:39:06,500
then you just find where you're going to win in one sitting. So you back off, retreat,

308
00:39:07,700 --> 00:39:16,740
move your legs out, say moving, moving, or get up and walk, or lie down. It's unbearable,

309
00:39:16,740 --> 00:39:30,020
just change. Don't have to try to become. The calm will come naturally once you understand

310
00:39:30,020 --> 00:39:36,580
the situation and you no longer react to it, but that will come by itself. All you have to do

311
00:39:36,580 --> 00:39:49,060
is see clearly watch and see. Be patient, you'll get there.

312
00:39:55,700 --> 00:40:00,820
Okay, I'm going to cut it off there because it's been a long day. Thank you all for tuning in.

313
00:40:00,820 --> 00:40:10,980
Good to see you all again. Beyond again, once more tomorrow and then Monday, I'm probably not here.

314
00:40:10,980 --> 00:40:32,660
I don't know, wait, it's a second. Let's see, schedule Monday. Anyway, tomorrow I'm here.

315
00:40:32,660 --> 00:40:42,100
I mean, it doesn't tell me. I think I might be back in time.

316
00:40:43,700 --> 00:40:46,980
Yeah, we'll probably be back in time. We'll see Monday, I might actually be able to do it.

317
00:40:48,260 --> 00:40:51,620
Anyway, I'll see you all tomorrow. Good night.

318
00:40:51,620 --> 00:41:03,460
Thank you, Monday. Thank you, Raman. Take care.

