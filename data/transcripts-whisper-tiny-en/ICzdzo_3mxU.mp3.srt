1
00:00:00,000 --> 00:00:05,000
I have such anger towards my mom because the way she treated me when I was growing up.

2
00:00:05,000 --> 00:00:09,000
However, now she is a changed person. She is very loving now.

3
00:00:09,000 --> 00:00:12,000
How can I reconcile this anger with myself?

4
00:00:12,000 --> 00:00:16,000
Every time I think about the past, what she did, I become very angry.

5
00:00:16,000 --> 00:00:20,000
It consumes me when I'm driving to work, working in daily chores.

6
00:00:20,000 --> 00:00:41,000
I kind of can understand what you are going through because I had similar things to fight with many years ago.

7
00:00:41,000 --> 00:00:51,000
And I remember that one time, I was not practicing meditation yet.

8
00:00:51,000 --> 00:00:58,000
I was practicing Zen meditation, but I haven't had a teacher like Bante.

9
00:00:58,000 --> 00:01:11,000
I remembered that what has happened is past and that changed very much.

10
00:01:11,000 --> 00:01:24,000
I suddenly saw that she is now different and she was then already very loving.

11
00:01:24,000 --> 00:01:35,000
So it was not the same person anymore and I was not the same person anymore.

12
00:01:35,000 --> 00:01:45,000
The whole thing that caused anger and that troubled my heart had passed already.

13
00:01:45,000 --> 00:01:58,000
And I was generally quite happy at that time, although anger came up again and again.

14
00:01:58,000 --> 00:02:10,000
So when I realized that it really is past what she did to me and how I reacted and what she did,

15
00:02:10,000 --> 00:02:15,000
that I grew up and that she grew up and we both changed.

16
00:02:15,000 --> 00:02:30,000
I started to think of really forgiving her and now I would say the best thing you can do is to start to forgive her

17
00:02:30,000 --> 00:02:42,000
because that is the only way that you will overcome your anger for giving

18
00:02:42,000 --> 00:02:56,000
has to be done mindful of course because it has to be something that grows out of your heart.

19
00:02:56,000 --> 00:03:03,000
It's one thing to say I forgive her and then still the things come up.

20
00:03:03,000 --> 00:03:15,000
But when you see, when you understand what has happened is past and it will not happen again because she changed and she is now loving.

21
00:03:15,000 --> 00:03:22,000
Then you are in the position she has been in.

22
00:03:22,000 --> 00:03:25,000
You are the bad part now.

23
00:03:25,000 --> 00:03:30,000
And you are creating the Akusala.

24
00:03:30,000 --> 00:03:37,000
You are creating the unholdsum in having the grudge in not forgiving her.

25
00:03:37,000 --> 00:03:48,000
But when you cultivate the paramis, when you cultivate,

26
00:03:48,000 --> 00:03:52,000
it's maybe too long to talk about the 10 paramis.

27
00:03:52,000 --> 00:04:04,000
But I just want to mention that when you practice forgiveness, you are developing all the paramis.

28
00:04:04,000 --> 00:04:21,000
And this is such a wonderful, beautiful thing you can do that you will be able to overcome your anger for your mother.

29
00:04:21,000 --> 00:04:29,000
But you have to be mindful when you go to work, when you are working and when you do your daily chores.

30
00:04:29,000 --> 00:04:36,000
It's an active process for giving or letting go of anger.

31
00:04:36,000 --> 00:04:44,000
For example, when you hold something in your hand that is hurting you, that is hurting your hand.

32
00:04:44,000 --> 00:04:49,000
What you do is you open your hand and you let go of it.

33
00:04:49,000 --> 00:04:57,000
So the same is happening in your heart where you hold all that grudge in that anger.

34
00:04:57,000 --> 00:05:01,000
You have to open the heart and you have to let go of it.

35
00:05:01,000 --> 00:05:07,000
But before you can do so, when you open your heart you have to watch it.

36
00:05:07,000 --> 00:05:11,000
You have to observe mindfully what there is.

37
00:05:11,000 --> 00:05:14,000
You have to observe all the anger that is coming up.

38
00:05:14,000 --> 00:05:23,000
You have to observe all the grudge and all the defilements that are related with it.

39
00:05:23,000 --> 00:05:34,000
Once you have seen all your defilements related to that anger, then you can start to build up the paramis.

40
00:05:34,000 --> 00:05:55,000
Like the love to answer her love with love to write later that she shifted your relationship from cordial to even more closer.

41
00:05:55,000 --> 00:06:18,000
When you see your anger and you understand that it's past, you understand that it's impermanent and gone and it has changed in her, then you might be able to change it within yourself as well.

42
00:06:18,000 --> 00:06:36,000
You can possibly answer her approaches and get closer to her yourself.

43
00:06:36,000 --> 00:06:44,000
Please say something.

44
00:06:44,000 --> 00:07:00,000
Not to say something different, but just to add how it kind of works or plays out in practice because you should never feel guilty about bad deeds.

45
00:07:00,000 --> 00:07:08,000
You should be clear that they're bad, but you can have a wonderful relationship with your mother and still be angry at her.

46
00:07:08,000 --> 00:07:17,000
You can have the anger inside and be boiling up inside and still say nice things to her, how it works because you know the anger is not the answer.

47
00:07:17,000 --> 00:07:21,000
This anger is not how I should be relating to my mother.

48
00:07:21,000 --> 00:07:24,000
This is what I'm working on.

49
00:07:24,000 --> 00:07:36,000
This is important because to pretend that it's not there or to force it out, it's impossible and it's going against the truth of it.

50
00:07:36,000 --> 00:07:40,000
What you're seeing, in fact, is wonderful.

51
00:07:40,000 --> 00:07:44,000
You're seeing that anger is not something you can control.

52
00:07:44,000 --> 00:07:47,000
What you're seeing is a great thing to see.

53
00:07:47,000 --> 00:07:59,000
What you're experiencing now, top notch because most people never come to this or when they come to this, they try to find some way to turn it off or to control it.

54
00:07:59,000 --> 00:08:06,000
I can't control my anger. What am I going to do? They go to find someone who knows how to control their anger or they come to us.

55
00:08:06,000 --> 00:08:10,000
Unfortunately, we say no, no, you can't control your anger. Stop trying to control it.

56
00:08:10,000 --> 00:08:18,000
Then you're forced to turn yourself on your head and accept that what you're experiencing is the truth.

57
00:08:18,000 --> 00:08:23,000
You can't control the anger. You can't stop it. It consumes you.

58
00:08:23,000 --> 00:08:31,000
It takes you away from your work. Why does it do that because you've developed it? Because you've cultivated it in the past.

59
00:08:31,000 --> 00:08:36,000
I can't change. You can't change what you're doing. I can't change what you've done in the past.

60
00:08:36,000 --> 00:08:40,000
What you've done in the past is finished. We can't deal with that.

61
00:08:40,000 --> 00:08:45,000
You've developed this propensity towards getting anger. Angry.

62
00:08:45,000 --> 00:08:47,000
You shouldn't even try to change that.

63
00:08:47,000 --> 00:08:50,000
What you should do is watch the anger as it comes up.

64
00:08:50,000 --> 00:08:53,000
Oh, here I'm angry at my mother. What's that like?

65
00:08:53,000 --> 00:08:58,000
And pretty quickly, you're going to see this is not something I want to develop.

66
00:08:58,000 --> 00:09:06,000
You're going to change your habit. This incredibly strong habit that you've built up.

67
00:09:06,000 --> 00:09:10,000
When you do something once, it changes the way you behave.

68
00:09:10,000 --> 00:09:18,000
When you do something a thousand times, it becomes quite difficult to do anything but that or to react in any way apart from that.

69
00:09:18,000 --> 00:09:27,000
So if you've developed a habit a thousand times, you can think that you probably have to undevelop it or develop a different habit, a different way of reacting, maybe a thousand times.

70
00:09:27,000 --> 00:09:32,000
So it's not going to, the anger is not going to go away in the night time.

71
00:09:32,000 --> 00:09:40,000
But what we'll go away, and this is why it's so clever to talk about a soda band that gets rid of.

72
00:09:40,000 --> 00:09:51,000
And the way the Buddha was able to explain things shows quite clearly why the first thing the soda band gets rid of nothing to do with greed or anger.

73
00:09:51,000 --> 00:10:01,000
The soda band does not get rid of greed or anger, but a person who has entered into the first stage of enlightenment has gotten rid of the wrong view that says anger is good.

74
00:10:01,000 --> 00:10:07,000
So a soda band that might still be angry at their mother. But when the anger comes up, they don't say, you know, this is because my mother did this.

75
00:10:07,000 --> 00:10:17,000
They say, this is because I've developed anger in the past. This is simply a habit that is a construct, an artificial construct or formation that I've developed through habit.

76
00:10:17,000 --> 00:10:27,000
And they patiently work the habit out. They change the habit. So when the anger comes up, they see it and they remind themselves, this is anger and they say, oh, that's not a good thing.

77
00:10:27,000 --> 00:10:40,000
But they see clearly that it's bringing them suffering and causing their minds to burn up, burn up, causing them to have problems at work, daily chores and so on. How does it feel to do your daily chores angry?

78
00:10:40,000 --> 00:10:47,000
I mean, it's actually probably not as bad as you think it is. You're probably just lumping guilt on there and making your life miserable.

79
00:10:47,000 --> 00:10:59,000
So you do daily chores with anger. You can still do your daily chores. So you're angry at work, well, you can still do your work. When you follow it, when the anger comes up and you're not mindful of it, you'll snap it, people.

80
00:10:59,000 --> 00:11:13,000
So the problem, of course the anger is a problem, but the bigger problem is not the anger. It's the following the anger. You can be angry and talk to people nicely because you know intellectually that anger is wrong.

81
00:11:13,000 --> 00:11:22,000
Not exactly intellectually, but the view that you have, which is based on your realization through the practice, is that the anger is your problem, not there.

82
00:11:22,000 --> 00:11:32,000
So you can be really angry at someone and still say, this anger is not something I want to use and understand clearly that this anger is a dangerous thing.

83
00:11:32,000 --> 00:11:53,000
Say, do yourself angry anger and then reply to the person and they think, oh, that was nice. That person is quite nice and quite kind and you're sitting there angry. Do you think with your anger, which you make the determination to get rid of, but that you've built up and is causing your suffering as a result?

84
00:11:53,000 --> 00:12:06,000
If you look at you, as you are now, you might probably come to the conclusion that it's not too bad.

85
00:12:06,000 --> 00:12:20,000
And I think this is some very important thing to see. First, because it's present, it's now the present moment and the second thing is, it's not too bad.

86
00:12:20,000 --> 00:12:48,000
So there is nothing that you can insult her for, that you can be angry for. She did what she did and she probably didn't mean it bad because I don't think that any parents, even if they do the worst things that they don't do it because they're evil, they do so because they don't know better.

87
00:12:48,000 --> 00:12:56,000
And they do it because they have their past and their calm and they do what they do.

88
00:12:56,000 --> 00:13:10,000
And you become what you become because of it and you have in your hands what you become and probably the outcome is not too bad.

89
00:13:10,000 --> 00:13:31,000
So look at yourself and don't point on your mother and when there is anger within you, then it is your anger. It has to do with you and you have to work on it, you have to let go of it.

90
00:13:31,000 --> 00:13:47,000
Yeah, it's not so bad. It's the thing as we start to hate ourselves about this. This was the question, that question about pornography, this guy was addicted to sensual pornography and so on.

91
00:13:47,000 --> 00:14:02,000
I said it's ruining my life and I said well it's probably not ruining your life but it's very easy to think that things are ruining your life because you're still breathing, you still have a computer so you're still able to type this message out so your life is still actually going quite well.

92
00:14:02,000 --> 00:14:18,000
Having a mother like this, you have a great life and you should feel very happy and see that this is just a small problem.

93
00:14:18,000 --> 00:14:34,000
But for ordinary people you have to remember they would have been shouting by now. So we're keeping it bottled up and we have to face it and just a angry angry without blowing up and without acting on it and so on. So it can be a lot of suffering when you're practicing.

94
00:14:34,000 --> 00:14:49,000
But if you sit back and look at it and say well you're not hitting your mom, you're not beating her I hope and if you are you should stop and you're probably not yelling at her and from the sounds of it you're trying your best to keep it bottled up.

95
00:14:49,000 --> 00:15:14,000
Just add on to that mindfulness and you've got a pretty good relationship as a result. The other thing I wanted to say that's probably the negative side is that there's all this talk about people changing has to be tempered because sometimes people don't change or sometimes people change superficially, never be too optimistic about either yourself or other people.

96
00:15:14,000 --> 00:15:21,000
Never think that oh I've changed and now I'm enlightened and never think about other people that they've changed and they're enlightened.

97
00:15:21,000 --> 00:15:29,000
I would never jump into to creating strong bonds with people.

98
00:15:29,000 --> 00:15:44,000
But I suppose there's some there's some exception that has to me but we can talk about that in a second. The point being you don't want to be disappointed because that would be the worst. If you think wow my mother's perfect and the problem is all with me and then you work out your problems to some extent and go back and see your mother.

99
00:15:44,000 --> 00:15:51,000
She starts acting poorly because of course she hasn't practiced or probably she hasn't practiced strong meditation.

100
00:15:51,000 --> 00:15:59,000
And then you get angry again and then you have this horrible relationship because it's all still the seeds are still inside of you.

101
00:15:59,000 --> 00:16:10,000
So when they're fed by her you get back and suddenly your relationship is horrible again. Be mindful and be cautious and be careful when relating to any person.

102
00:16:10,000 --> 00:16:22,000
Forgive but don't forget so I want to add don't forget that she's getting older and she's going to die.

103
00:16:22,000 --> 00:16:39,000
So you want to make sure that you have forgiven her before she does so that's really very important that you should talk to her and find your love for her again.

104
00:16:39,000 --> 00:16:55,000
So I didn't mean to say to not especially with your mother to not make amends because really if a person says that go from cordial to something more.

105
00:16:55,000 --> 00:17:10,000
Well just be clear that something more should be your proper relationship as a son doing your duty and being grateful for the good things that she has done for you and for the love that she does have for her and relating to her as a human being who has her own problems.

106
00:17:10,000 --> 00:17:18,000
But as a very special human being she's the only person in the world who gave birth to you and no one else can do that for you and no one else to do that.

107
00:17:18,000 --> 00:17:28,000
No one else carried you around in her womb for nine months. No one else closed you and bathed you and fed you no matter how poorly a job she did.

108
00:17:28,000 --> 00:17:49,000
You know you have a special relationship with her in a very strong karmic relationship if it was especially if it was a bad relationship. It means that something brought you to this relationship.

109
00:17:49,000 --> 00:18:04,000
You have to put special emphasis on the fact I'm her son she's my mother so regardless of this anger regardless of this you have to you know your default relationship with your parents and this is something every meditator will see when they just start to meditate.

110
00:18:04,000 --> 00:18:20,000
The default relationship should be one of love and caring and responsibility for each other. The parents have responsibilities towards the children and the children have responsibilities for the towards the parent. This is the only way to work out this strong karmic bond that has caused us.

111
00:18:20,000 --> 00:18:35,000
You were you were formed inside of her body that's not an ordinary state. Things don't just happen like that in the universe everywhere. You don't just see things spontaneously arising in other people's bodies.

112
00:18:35,000 --> 00:18:51,000
It's a very special and strong bond and karmic for you to make the decision to be created and formed inside of someone else's body. So something that you have to be very clear about that this is something you can't mess with.

113
00:18:51,000 --> 00:19:06,000
I've messed with it we've all messed with it and you get burnt terribly because it's a strong karmic relationship. I think you really only see that when you begin to practice meditation but you sure do see it.

114
00:19:06,000 --> 00:19:21,000
You should talk to her and try to work that out as quickly as possible. The only question I wanted to give is don't become overconfident and think oh she's perfect now because people are not perfect. You still have to guard yourself.

115
00:19:21,000 --> 00:19:35,000
When she says nasty things because she might sometimes down the line you have to be ready for them. When you get angry at her of course you have to be ready for that. Never think that now everything is perfect and everything is going to be wonderful.

116
00:19:35,000 --> 00:19:59,000
Always be like a soldier in enemy territory at all times but at the same time you don't want her to die feeling bad about it. You don't want her to die with you not having sort of things out because that will cause a great amount of worry and agitation in your mind.

117
00:19:59,000 --> 00:20:06,000
Of course in her mind as well.

