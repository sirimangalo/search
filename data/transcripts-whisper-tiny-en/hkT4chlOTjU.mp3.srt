1
00:00:00,000 --> 00:00:08,000
James asked, recently a meditation teacher told me you don't have to give up anything to practice.

2
00:00:08,000 --> 00:00:12,000
Instead giving up will come naturally as a result of practice.

3
00:00:12,000 --> 00:00:17,000
However, a certain abstinence initially needed for successful practice.

4
00:00:17,000 --> 00:00:36,000
I'll take chance and start to answer, although it's your turn.

5
00:00:36,000 --> 00:00:47,000
Because just today we talked about that a little bit, and I agree with that teacher.

6
00:00:47,000 --> 00:00:59,000
If you ordain or if you're just a yogi and a meditator at home, you don't have to give up from the first day on everything.

7
00:00:59,000 --> 00:01:18,000
Because it can be very difficult and it can happen that you have a huge drawback when you try to get rid of everything and try to really abstain from everything.

8
00:01:18,000 --> 00:01:36,000
And then the day comes you can't bear it anymore and you just indulge or you just go back to your old habits because you can't bear it anymore.

9
00:01:36,000 --> 00:01:49,000
So, when you train your mind, it will really come naturally as a result that your mind is more able to let go.

10
00:01:49,000 --> 00:02:09,000
And then letting go comes easily and things that you could never let go of are suddenly no problem anymore.

11
00:02:09,000 --> 00:02:24,000
There was one monk who, I watched him practice when he was a lay person and later on he became a monk and went to Burma to live with Upandita, I think, Upandita.

12
00:02:24,000 --> 00:02:38,000
And he described it to me after three months of living in this monastery, doing intensive meditation, he said he even made it, he said, I made it all the way to Sankarupi, Kanyanas, I had these ideas about where he was.

13
00:02:38,000 --> 00:02:54,000
And then he said, but once the retreat was over, I took my dhyaka, my helper and we went up to the hotel and I had a salad because for three months it was all boiled, all the food is boiled there, no salad.

14
00:02:54,000 --> 00:03:03,000
So I just had to, and you think, wow, three months of intensive meditation and the guys still can't let go of a salad.

15
00:03:03,000 --> 00:03:20,000
It's certainly the case that you really do have to let, you know, maybe even to go further than this, it's not only do you not have to give up anything, you can't give up anything, you can't actively give up anything.

16
00:03:20,000 --> 00:03:30,000
It's not possible to give up something that you'll still enjoy, think of something that you still don't understand to go even further.

17
00:03:30,000 --> 00:03:36,000
If you, if you still have some idea that this is beneficial to you, you will not let go.

18
00:03:36,000 --> 00:03:40,000
So people have these worry that in Buddhism we have to let go of stuff.

19
00:03:40,000 --> 00:03:46,000
Does that mean I have to let go of my girlfriend? Does that mean I have to let go of my car?

20
00:03:46,000 --> 00:03:50,000
Does that mean I have to let go of my money? What do I have to let go of here?

21
00:03:50,000 --> 00:03:56,000
Not only do you not have to, you can't. It's not possible, so they come to the teacher and say, oh, I'm so worried.

22
00:03:56,000 --> 00:04:01,000
If I continue like this, I'm going to give up everything. It's not possible. You won't give up anything.

23
00:04:01,000 --> 00:04:08,000
You can't give up anything. All you can do is come to understand that these things are not worth clinging to in the first place.

24
00:04:08,000 --> 00:04:14,000
And therefore not give rise to desire for them. It's not even giving up.

25
00:04:14,000 --> 00:04:21,000
It's the non-arising of desire for them that causes the mind to slip off and then turn to Nibana.

26
00:04:21,000 --> 00:04:27,000
It's not a rejecting. It's a letting go.

27
00:04:27,000 --> 00:04:34,000
It's like there's a magnetic attraction and that magnetic attraction is gone.

28
00:04:34,000 --> 00:04:42,000
So once you understand, this is why I say even with in a video on pornography,

29
00:04:42,000 --> 00:04:52,000
that's got so popular once you understand the object.

30
00:04:52,000 --> 00:04:56,000
You will never have attachment to it again. The point is not to give up the desire.

31
00:04:56,000 --> 00:05:02,000
The point is to watch the desire. Watch what happens when you are attracted to something.

32
00:05:02,000 --> 00:05:06,000
Even when you're indulging in something, trying to do it mindfully.

33
00:05:06,000 --> 00:05:12,000
And you'll find yourself just losing any interest in it.

34
00:05:12,000 --> 00:05:17,000
Now, there's one thing that I think may be a part of this question.

35
00:05:17,000 --> 00:05:21,000
And it's an important part is, well, what about the five precepts?

36
00:05:21,000 --> 00:05:27,000
Maybe I can turn that over to you because a beginner meditators ask to give up certain things.

37
00:05:27,000 --> 00:05:30,000
So the five precepts, what about them?

38
00:05:30,000 --> 00:05:36,000
You're saying to practice meditation, you don't have to stop killing, stealing, lying, cheating, and taking drugs and alcohol?

39
00:05:36,000 --> 00:05:44,000
No, this is definitely to be kept on this.

40
00:05:44,000 --> 00:05:53,000
Not only in a meditation retreat, not only the five precepts, but the eight precepts should be kept.

41
00:05:53,000 --> 00:06:09,000
I thought this is understood that to abstain from killing, to abstain from stealing, and to abstain from...

42
00:06:09,000 --> 00:06:20,000
Well, that depends if you have the five or the eight precepts from either any sexual activity or in the five precepts,

43
00:06:20,000 --> 00:06:31,000
to abstain from the wrong sexual behaviour, so hurtful sexual behaviour also,

44
00:06:31,000 --> 00:06:36,000
and abstain from intoxicants.

45
00:06:36,000 --> 00:06:43,000
All these things, when you're a meditator, should be kept.

46
00:06:43,000 --> 00:06:58,000
Although I would say the precepts with intoxicants should be taken, of course, and should be kept,

47
00:06:58,000 --> 00:07:08,000
but I always would recommend to go slow if there is...

48
00:07:08,000 --> 00:07:18,000
Not even an addiction, but if there is not the insight, as you said, the knowledge, the understanding is needed.

49
00:07:18,000 --> 00:07:27,000
So if you don't understand that our drinking of alcohol, for example, is a really bad thing,

50
00:07:27,000 --> 00:07:45,000
then you can't give it upright away, probably, so it might be better to start meditate and break the fifth precept.

51
00:07:45,000 --> 00:07:57,000
And drink some of alcohol, and during meditation one day, you find out, wow, drinking alcohol is really bad, and then just don't do it anymore then.

52
00:07:57,000 --> 00:08:07,000
But to say, oh, I can't stop drinking alcohol, I want to drink alcohol once in a while, and then say, okay, well then, I can't meditate.

53
00:08:07,000 --> 00:08:15,000
That would be the worst solution you can take, I would say.

54
00:08:15,000 --> 00:08:31,000
I mean, the one thing that I've always said about the precepts is the point is that there are just certain things that are too strong of a negative to be allowed.

55
00:08:31,000 --> 00:08:44,000
And it kind of even goes back to this music, for example, or being surrounded by the objects of your senses, of your desire, lust, for example.

56
00:08:44,000 --> 00:09:00,000
Why it's a conclusion from them in the beginning is actually beneficial, but I think even more so the five precepts, because if you do engage in killing, it's very difficult to see the negative side of it.

57
00:09:00,000 --> 00:09:14,000
The negative side of it, the more you kill, the less you're able to see the negative side of it, because the less clearing your mind is, these are weighty things that are not subject to clear discrimination.

58
00:09:14,000 --> 00:09:32,000
And I think I would include alcohol in there, tentatively. I mean, there could be an example where he drinks alcohol and then he's able to still go and learn from the Buddha, but for the most part, when you drink alcohol, you won't have the ability to discern whether that is good or bad for you.

59
00:09:32,000 --> 00:09:44,000
What you do find is that people who have practiced meditation and then do indulge, as you say, they realize the mistake in it, and they realize that they've lost a lot of mindfulness and doing that.

60
00:09:44,000 --> 00:09:59,000
But to start out, you just have to be quite careful that you're not starting out with drinking and meditation, because chances are that you'll just give up the meditation or you'll develop all kinds of weird meditation practices.

61
00:09:59,000 --> 00:10:07,000
So, in general, just that there are things that have an extreme effect on the mind, all five of them.

62
00:10:07,000 --> 00:10:23,000
If anyone has ever killed other living beings and then practiced meditation, they can see how what a strong effect it has and a prolonged effect it has on their minds and on their meditation practice, then they will see the important.

63
00:10:23,000 --> 00:10:30,000
The other thing is our inability to see this danger, to see the weightiness of them.

64
00:10:30,000 --> 00:10:46,000
Because we don't understand, we aren't able to abstain from these things, even having done it several times, or after engaging in the activity several times, it might be difficult to see the danger of it.

65
00:10:46,000 --> 00:10:53,000
So, for this reason, we establish it as a rule to stop this.

66
00:10:53,000 --> 00:11:08,000
These are things that are clearly wrong, and they are so extreme that even with just a little bit of meditation, you should become disinclined towards these things.

67
00:11:08,000 --> 00:11:17,000
So, we say, don't bother with these, don't bother learning about how bad these ones are, we'll give you that much of a head start.

68
00:11:17,000 --> 00:11:27,000
Because they're so weighty, that by the time you realize the problem with them, they've already had a profound effect on your mind, I think.

69
00:11:27,000 --> 00:11:40,000
There's something to mind.

