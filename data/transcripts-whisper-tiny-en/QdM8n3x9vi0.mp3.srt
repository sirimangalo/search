1
00:00:00,000 --> 00:00:11,000
Okay. Is mindfulness on the breath a beginners practice? Isn't the goal to be mindful of everything, not just the breath?

2
00:00:11,000 --> 00:00:21,000
Well, it depends what you mean by mindfulness on the breath, mindfulness of the breath, I guess.

3
00:00:21,000 --> 00:00:32,000
I don't, I'm not sure if I said it, I did another, answer another question, I think last week on, I think something of the sort.

4
00:00:32,000 --> 00:00:39,000
Why we focus on the abdomen, that's what it was right. So I did talk about it, that the breath is a concept.

5
00:00:39,000 --> 00:00:55,000
So, it's not a matter of being a, a beginners practice. It's a matter of being a conceptual practice, a practice that only has the potential ability to calm the mind.

6
00:00:55,000 --> 00:01:07,000
And it doesn't by itself have the ability to clear the mind, to allow one to see clearly, because it's not focused on Nama Rupa, which is relates to the question I just answered.

7
00:01:07,000 --> 00:01:13,000
If you're not focusing on Nama Rupa, you'll never get rid of this idea of self, the ego.

8
00:01:13,000 --> 00:01:22,000
There are many meditative contemplatives out there who believe in a self, who believe in God, who believe in the soul, and so on.

9
00:01:22,000 --> 00:01:32,000
And have this, this view, because they haven't seen Nama Rupa, and they haven't come to have this profound realization that doesn't matter what you talk about.

10
00:01:32,000 --> 00:01:46,000
Talk about God or a soul or a self. What you're really talking about is seeing, hearing, smelling, tasting, feeling, and thinking, which are all nothing more than Nama Rupa, Rupa and Nama.

11
00:01:46,000 --> 00:02:03,000
So, you know, it's a mindfulness specifically or strictly about the breath is a salment of practice or a practice, which only has the potential to calm the mind.

12
00:02:03,000 --> 00:02:15,000
Now, that doesn't mean that what we generally call mindfulness of the breath can't be used to see clearly, but if it's the breath, specifically the breath that you're focusing on,

13
00:02:15,000 --> 00:02:22,000
then it is, and for many people this is the case, they focus on it and their mind becomes calm.

14
00:02:22,000 --> 00:02:37,000
That calm can be useful, it can be used to develop insight, but it doesn't have to be, and many people don't develop subsequently insight, and there's content with the calm and they think they're enlightened or so on and so on.

15
00:02:37,000 --> 00:02:49,000
But, if you focus on the physical experience of the breath, and this is why we focus on the stomach, right, rising, falling, because that's the breath.

16
00:02:49,000 --> 00:02:54,000
Anyone who says that's not mindfulness and the breath is really being dogmatic, right?

17
00:02:54,000 --> 00:02:59,000
Anyone and this is what I was saying about the nose right last week.

18
00:02:59,000 --> 00:03:09,000
The stomach is equally mindfulness of breathing. If the text, say, focus on the nose or focus on the stomach, it's not really clear.

19
00:03:09,000 --> 00:03:22,000
But, what's clear in the text and what I didn't mention, I promised I was going to say last week, was where in the texts they talk about mindfulness of the rising and the falling of the abdomen.

20
00:03:22,000 --> 00:03:32,000
And it's in one suit, at least, but I have to look it up where else it is. It's in the sama-diti-suta, I believe, if I'm not wrong.

21
00:03:32,000 --> 00:03:41,000
We're sorry puta talks about the winds in the body, and it's something that the Buddha has talked about, and the wind in the body, the wind element.

22
00:03:41,000 --> 00:03:52,000
And it's the rising and falling, sorry, the winds in the stomach, this is the part of the wind element.

23
00:03:52,000 --> 00:03:56,000
And so it's a part of our physical experience, and by wind means the pressure.

24
00:03:56,000 --> 00:04:06,000
So the pressure that comes in the stomach, it's specifically mentioned in the sama-diti-suta of the majimani kai, I believe.

25
00:04:06,000 --> 00:04:12,000
Not as scholar by any means.

26
00:04:12,000 --> 00:04:28,000
But okay, so if you're focusing on that aspect of the breath, the physical experience that relates to the breath, then it certainly is vipassana, it is inside meditation, and it's not a beginner's practice.

27
00:04:28,000 --> 00:04:34,000
Because one thing we have to realize in meditation is that we're really not going anywhere.

28
00:04:34,000 --> 00:04:39,000
We're not trying to get somewhere. The path is like this imploding path.

29
00:04:39,000 --> 00:04:49,000
When the eight-fold noble path doesn't take you somewhere, you're not going out there, you're coming back inside until the mind doesn't go anywhere.

30
00:04:49,000 --> 00:04:52,000
Some people call it the inward path.

31
00:04:52,000 --> 00:04:58,000
There's the publishing Buddhist publishing group in Malaysia called the inward path, which is a really good name.

32
00:04:58,000 --> 00:05:01,000
Because that's where this path is leading.

33
00:05:01,000 --> 00:05:08,000
So there's really no such thing as a beginner's practice, anything that is useful in the beginning.

34
00:05:08,000 --> 00:05:12,000
If it's really truly useful, it's going to lead you all the way.

35
00:05:12,000 --> 00:05:18,000
And from the very beginning, the practice that you start with has the potential to take you all the way to the end.

36
00:05:18,000 --> 00:05:28,000
Because as long as it's focusing on nama and rupa, it's going to take you, or it has the potential to take you all the way to the end.

37
00:05:28,000 --> 00:05:45,000
That's one way of answering, but in practical terms, the problem with focusing on the stomach is not that it's not going to bring about insight and realization of the truth.

38
00:05:45,000 --> 00:05:51,000
The problem is that it's not enough.

39
00:05:51,000 --> 00:06:04,000
Well, the idea of avoiding other things is a good point, but it's not really the most important point.

40
00:06:04,000 --> 00:06:10,000
The most important point is that the mind is more active than that.

41
00:06:10,000 --> 00:06:20,000
The mind will fail to, at least in the beginning, will fail to fully grasp the reality of what it's experiencing.

42
00:06:20,000 --> 00:06:38,000
Because the mind is distracted, and because the mind is running all over the place, especially in the beginning, that you have to give it some other objects to pay attention to.

43
00:06:38,000 --> 00:06:49,000
In that sense, really, it's true, and we never recommend for anyone to focus simply on the abdomen.

44
00:06:49,000 --> 00:07:01,000
If you're talking about someone, if you're questioning about someone who practices anapanasati, then generally they're practicing anapanasati for the purpose of gaining calm.

45
00:07:01,000 --> 00:07:08,000
They have different theories on this as well, but mostly they're ideas that they're going to gain calm.

46
00:07:08,000 --> 00:07:11,000
If you're going to gain calm, then, yeah, you do lose the rest.

47
00:07:11,000 --> 00:07:14,000
You're trying to avoid them, and you're trying to ignore them.

48
00:07:14,000 --> 00:07:23,000
Because you're trying to enter into a trance state, or a state of maybe not exact, maybe trance is a pejorative word.

49
00:07:23,000 --> 00:07:35,000
It's sort of is a trance. You're trying to get your mind into a new level of experience, a conditioned level of experience, and that blocks out quite a bit.

50
00:07:35,000 --> 00:07:42,000
It cuts off quite a bit of our spectrum of experience.

51
00:07:42,000 --> 00:07:52,000
On the other hand, if you read the Satipatana Sutta, the discourse on the four foundations of mindfulness, it seems like the idea is to do the opposite.

52
00:07:52,000 --> 00:08:00,000
That if you feel angry or upset, then you know that you're angry or upset, and therefore you do incorporate everything into your practice.

53
00:08:00,000 --> 00:08:02,000
That's the practice that I teach.

54
00:08:02,000 --> 00:08:14,000
Whenever, as I said, we don't tell people to focus simply on the stomach, we use that as, well, yeah, as you said, a beginner's practice.

55
00:08:14,000 --> 00:08:27,000
So what I was saying by not a beginner's practice is that it has the potential to take you all the way, but maybe a beginner's practice is an example practice.

56
00:08:27,000 --> 00:08:35,000
It's just one of an infinite number of practices that you could undertake.

57
00:08:35,000 --> 00:08:44,000
Once you learn how to do that, then we start telling you, well, actually in the beginning, we'll tell you about the feelings, about the mind, about the dumbas.

58
00:08:44,000 --> 00:08:48,000
There are four Satipatanas, no kaya, vidana jita dhamma.

59
00:08:48,000 --> 00:08:58,000
I've read my booklet on how to meditate, you will see that, I think I explained that quite clearly that actually the abdomen is just an example.

60
00:08:58,000 --> 00:09:06,000
But why I stressed in the beginning, because what was on my mind was the power of the rising and the falling, and I think a lot of people miss that.

61
00:09:06,000 --> 00:09:14,000
How incredible it can be to simply say to yourself, rising and falling, and the profound insight that can come from it.

62
00:09:14,000 --> 00:09:31,000
Why I deny that it's a beginner's practice is because I don't want you to think that just because the first thing that we give you and then later we give you other practices, that it was only the first thing, or it was only the first step.

63
00:09:31,000 --> 00:09:44,000
Simply watching the rising and the falling can lead you to enlightenment, and it certainly leads everyone who practices it to change the way they look at reality.

64
00:09:44,000 --> 00:09:53,000
And that's often the problem, because there's one video I did that I was really happy about, because it's one that really needs to be done and needs to be said.

65
00:09:53,000 --> 00:10:02,000
It's in regards to this, it's what was it called experience of reality or something I titled it, some silly name.

66
00:10:02,000 --> 00:10:14,000
But it was basically what is the meditation, and all of those bad experiences that we have that are actually right practice.

67
00:10:14,000 --> 00:10:30,000
And it was basically I hope in that video or I think in that video I talked about how the rising and the falling is a good example, because it becomes stressful and it becomes unpleasant and we can't control it and we try to control it.

68
00:10:30,000 --> 00:10:38,000
And therefore we think this meditation is bad and terrible and wrong for us.

69
00:10:38,000 --> 00:10:44,000
Maybe we're just not good at it, I love how my students from Thailand would always tell me.

70
00:10:44,000 --> 00:10:48,000
I'm just not suited to the rising and falling, or I just don't find it suitable.

71
00:10:48,000 --> 00:11:02,000
My turn at me is where my turn at, which means I'm just not into it basically, which is always funny because it means that they're not having fun with it.

72
00:11:02,000 --> 00:11:16,000
It's much more fun to count in one out, one into, it means much more peaceful, much more dependable because you know that after one comes to and then three and then four it's quite reassuring actually.

73
00:11:16,000 --> 00:11:20,000
But when you go rising, falling, you don't know what's going to come next.

74
00:11:20,000 --> 00:11:22,000
And so it forces you to let go.

75
00:11:22,000 --> 00:11:31,000
That's all I wanted to say, but it is the first thing we give and there's a lot more that you can focus on and in the end you should really be focusing on everything.

76
00:11:31,000 --> 00:11:39,000
But don't discard the rising and falling, always come back to it, it's a wonderful, fairly unique object I suppose.

77
00:11:39,000 --> 00:11:43,000
It's always there and it's always there to teach you something.

78
00:11:43,000 --> 00:12:05,000
And the layers of things that it can teach you is quite incredible.

