Namatu Ratanatayasa.
Welcome everyone.
Today is the session on insight meditation.
I think of it sort of as a time where we can get away.
Get away even from Buddhism where we don't have to talk about Buddhist concepts.
Where we can leave behind our affairs in the world.
But we can also have a chance just to practice and not to worry about even Buddhist concepts or Buddhism.
Not meaning that we're going to throw away.
We've learned or we're going to go outside of the Buddhist teaching.
Meaning that we don't have to study.
We don't have to talk about anything really.
So it should be considered that this is a time where we can have a sort of a guided meditation.
You don't have to think of it even as guided.
It's just sort of a meditation with audio accompaniment.
So I'm going to put my character on busy.
I didn't encourage people to do the same.
We can ignore things for a while.
Certainly isn't a time to be chatting with somebody else while you're sitting here.
Although it's certainly up to you and I won't know if you do.
So you can just close your eyes and just take some time to reflect on ourselves
to look at ourselves, to look within.
To study this elusive creature we call reality.
Reality is kind of a funny word, I think.
If you ask yourself what is real, I think it's pretty hard to pinpoint.
Let me see how far science has gone chasing after reality.
You also see how far the religions of the world have gone in the other direction.
Making all sorts of bold claims and assertions about what is real.
For me I can't figure out what sort of a definition you could give to the word reality.
How you can define whether something is real or not.
Except for that which is experienced.
For me it's from what I understand.
It's more of a mind game than anything if you talk about three-dimensional space.
Or even if you talk about Brahma and Atman and being one with God and so on,
as being the ultimate reality.
It certainly seems look possible that you could become one with God or become God-like or so on.
Could get to a state of oneness.
But I don't see how that's any more real than the experience that we're having right here and right now.
For people to say that our experience right here and right now is illusion.
It just doesn't quite make sense.
Whatever you experience to me that seems like to be the only thing that could possibly be real.
The only way you can make sense of this word real.
Because there is something there, there is an experience, that's what we're having right now.
This is why Buddhism puts so much emphasis on the present moment because it's really all that we have.
It's really all that exists.
There's no future, there's no past, these are mind games we play with ourselves.
There's only just a present moment, that's eternal, that doesn't end.
But in this one moment there's a lot going on as we can see it's changing all the time.
Our experience is not the same.
Basically, it could be broken up into six categories and they come one at a time.
So sometimes we're seeing, sometimes we're hearing, sometimes we're smelling, sometimes tasting,
sometimes feeling and sometimes thinking.
And it happens quite quickly if we're not meditating, it's quite difficult to see.
So here when we sit in meditation with our eyes closed and we're watching reality.
We're taking time to refine our vision, refine our perception.
To see things clearer than we could see them before.
We're not trying to push away, we're not trying to chase after, we're not trying to attain anything.
We're just trying to see clearer than we normally do, normally could.
All of the things that present themselves to the sixth senses.
Why this is useful is because of the laws of nature which govern experience.
When you see it's not just with impunity that you can see what you want and think about it,
whatever you want, to have whatever reaction you choose.
When we see something that we like, we automatically grasp onto it, hold onto it, and chase after it.
There are natural laws going on at work here.
And so depending on which mind states arise, when you see something, depending on what sort of a mind state arises next,
it's going to really define the result of seeing or the result of hearing.
When you see something you don't like, right away there's anger arises.
If you're not careful, if you're not clearly aware of it.
So our normal way of dealing with these things is either to get angry or to get greedy.
And then either choose to repress it, you want something, but you know it's wrong to chase after things that you don't deserve
or that belong to somebody else or so on.
And so you repress it, or maybe it's something that you've been told is wrong.
Sometimes for instance sex or sexuality is so stigmatized or it has been in the past.
I think now people are getting more going to the other side where they just chase after it on the time.
But still things like, things like masturbation, things like pornography or so on.
There's still I think probably pretty stigmatized, at least overtly.
So people do it covertly, they sneak it.
And as a result there's a lot of repression involved pushing it down, pushing it away.
This is something we do also with anger.
We get really angry at people, but we push it down, we repress it.
And we're pretty good at keeping it bottled up for a time, but we can only do it for a certain time.
And the way it works is we'll be working really hard to keep ourselves calm, to keep ourselves from wanting or keep ourselves from hating.
And then suddenly our guards down, one moment will be, maybe watching television or maybe doing something enjoyable.
And we let our guard down.
Or maybe something good comes and suddenly we feel sort of like safe.
And as soon as we start to feel safe, we let our guard down and all just explodes.
So we're something really fun, we like it.
And so we're happy and then someone comes and says or does or something happens.
And unpleasant and right away we flip out, or we chase after or so.
This isn't the right way to deal with things.
This isn't what we should be looking to do.
It works, it works on a temporary basis.
Sometimes even meditators have to do this if you're not.
Strong enough to really just face it.
You have to repress it at first.
But it doesn't solve anything, just takes it away temporarily.
Another way of dealing with it is as I said to chase after it, let it out.
When you're angry at someone, let them know, just get a hangry.
No use bottling it up, that's what they say.
I agree, there's no use bottling it up.
For sure we have to understand this, but doesn't mean that the opposite is correct.
It's correct either.
That we should chase after and cultivate these, at least brain activity.
If you don't want to talk about the mind, just talk about brain activity.
Scientists are perfectly aware how addiction works in terms of the brain, how the chemical reactions in the brain go.
The more you get, the less pleasant it is.
The less the more a stretched or where it worn down become the processes.
So in the beginning it's very pleasurable to get what you want.
But as you get it again and again, it takes more and more of it to really stimulate the chemical reactions.
So it's less and less pleasurable for more and more work.
That's why we always need something new, something more exciting.
Anger, I suppose, works in a different way.
I haven't really studied how the brain deals with anger, but it's just in the same way.
It's changing the way the brain works or the brain reacts.
If we're just talking about the brain and, of course, the mind is very closely interrelated with the brain.
Each just as easily talk about one is talk about the other.
Not that they're the same thing, but that they're very closely related.
The way it works is, of course, you get angry and, of course, it's going to change the way the brain is structured.
It's going to scar or it's going to leave its scratch on the brain.
And on the mind, on the reality, which is the consciousness.
It's going to change the chain reactions of events that are going to arise in the future.
It's building up tendencies, but it's simply.
So the more we act on these impulses, the more they become a part of us,
the more they become a part of who we are.
If you've ever killed killing, it's very difficult in the beginning.
I think a lot of these things are drinking alcohol is very difficult in the beginning.
Sexual intercourse is sometimes quite, and can be in some ways unpleasant in the beginning,
because of how disgusting the body is, and so on.
But it gets to be quite different after a while.
It gets more taste in music.
Sometimes you listen to something the first time, and you don't like it, and as you listen to it,
it becomes more agreeable.
Your mind gets used to it, and it knows how to react.
So it gives rise to either pleasure or pain.
It's used to reacting in that way.
So it's very reactionary as a result of building up tendencies.
So this isn't a good way to deal with anxiety.
In meditation, we're going to try to look and see things simply for what they are.
When you get angry, when you get greedy, even when you haven't gotten to that stage yet,
and you just see something, just to see it for what it is.
Not like see something, and you like it, and really hate yourself for liking it and wanting it or so.
You know, these stories about people coveting their neighbors' wives or husbands,
and then hating themselves for it, or people who are angry and get angry a lot,
and then get angry at themselves because they're angry people,
because they're spiteful people, people who are addicted to shopping,
people who are addicted to eating, and then they hate themselves for it.
They judge themselves for it.
Or else the opposite, you like something, and then you think it's good that you like it.
You're happy that you're so greedy, you're happy that you can get what you want.
I think it's a good thing, so you cultivate it more and more and more.
So we follow in these cycles of repression and release, going back and forth sometimes,
or developing them to quite extremes.
In meditation, we're just trying to look and see things as they are.
We're trying to come to understand things, understand how this works,
and understand reality in a clearer way than we normally would.
As we look, we're going to see that, first of all, the objects of the sense,
there's nothing in them that is intrinsically desirable or undesirable.
There's nothing that you could ever see that would be intrinsically beautiful.
What would it mean that something is intrinsically beautiful?
What does it mean to say that a flower is beautiful, or a body, a human body is beautiful,
or ugly?
It's meaningless.
It means nothing.
It means that we've developed these tendencies from life to life to believe this,
convincing ourselves of it.
You can see this happening, as I said, even in this life.
People who take their first beer and hate it, and then as they get used to drinking beer,
it becomes a wonderful thing.
We train ourselves, we fool ourselves into thinking that this is a good thing.
We have pain, and we convince ourselves that pain is bad,
and doctors tell us that, oh, pain is your body's way of telling you that something's wrong.
But bodies don't have ways of telling anything,
bodies just react, they just have chain reactions.
If anything, it's our reaction to pain that is created, this body,
and made it the way it is.
So instead of being something,
you know, really what you would expect us to be,
you'd expect us to be balls of light, it makes more sense.
Why do we have ten fingers, ten toes,
and all of these funny organs, and so on,
that are all imperfect and all like they were just patched together?
If there is a God, we certainly weren't made in His image or her image.
I had that hurt. It's a pretty lousy God.
And so, evolution has done wonders in clearing this up
and saying how things came physically.
We just sort of adapting that to what we can see is real,
what's really happening.
So that slowly, slowly, we're patching together and piecing together all of it.
There's nothing that's intrinsically good or intrinsically bad.
And so, we're going to see that. We can't see it normally.
When we see something, we write away, make a decision whether it's good or bad.
And that decision is totally meaningless, totally based on nothing,
based on our own idea at the time.
We could like something one day and hate it the next.
Sometimes it's based on function.
Let me see, this is beautiful and this is good because it works.
Like it's a good chair or a good car or so on because it runs well and so on.
But these are still just value judgments.
It's not good for anything really.
In the end, everything is worthless.
There's nothing that is worth anything in the world.
Not you, not I, not any experience that has any intrinsic worth.
It just comes and goes, worth is not something you can speak of.
Just is.
And so, in meditation, we're coming to see that.
We're coming to see that.
That's the way it is.
And the way I always recommend to do that is just remind yourself,
this word set-d, which we translate as mindfulness.
It just means to remind yourself.
It doesn't mean mindfulness.
Set-d means remembrance or that which you use to remember,
that which you use to remind yourself,
or the act of remembering.
When you remind yourself of what you've forgotten,
that this is pain, or this is seeing,
or this is liking, or this is anger,
or all of the myriad of different experiences,
you just remind yourself that's pain.
And your mind gets closer to the reality
and gets to see it clearer than it used to see it.
Then you see how it happens when a sensation arises,
or an experience arises,
then there's a happy feeling or an unhappy feeling.
This is where we go wrong.
When a pleasant feeling comes up, we like it.
We hold on to it.
We want it.
We want more.
And so, we give rise to the cycle of addiction.
When something unpleasant comes up,
then we don't like it.
We give rise to the cycle of hatred.
And we have so many cycles like this.
We have the cycles of depression, cycles of fear,
and anxiety.
People who get anxious and then can worry that they're too anxious
and so get even more anxious.
People who are awake all night trying to get to sleep
because the more stress they are about not being able to sleep,
then they get stressed about being stressed
and how to not be stressed.
And so they stress about trying to not be stressed and so on.
All we're doing here is coming to see things as they are.
We're establishing what we understand to be real
and then we're looking at that.
We're working on that.
We're working with that as our base.
We're not going to get into theories or ideas,
not even Buddhism, not even this or that.
Just it's here.
It's now.
You don't have to decide whether it's real or not.
It's there.
You're experiencing it.
All we're trying to do is come to see it clearer than we normally do.
It's this.
OK.
Let's look and see.
What is it really?
So in one way, this mantra that we use is a way of focusing on reality.
In another way, it's a way of straightening up the mind.
Because the mind sees, but it doesn't say that seeing.
It says that's him or her or that or this.
Good or bad, me or mine, us and them.
Totally forgotten that that's just seeing.
When you hear something, it's wonderful music or evil person yelling at me.
I don't deserve to be yelled at.
We've forgotten already.
We've forgotten totally what's going on.
We're just reminding ourselves, straightening out our mind saying, look, you're all out of shape.
You're all out of whack here.
That's not real.
That's not what's going on here.
And you can do it with anything.
And as you do it more and more, get better at it and better at it.
You come to see so many of the wrong ways we deal with reality.
We get stressed.
We get tense.
We try to force things, even just using this mantra.
It's so often the beginning people will get headaches.
And they say it's just not for them because it's like forcing things.
And this is a part.
This is the reason why we're doing this is so that we can see the way we react to things.
Even meditation, we react totally wrongly to it.
We tell people to breathe and watch the stomach rising and falling.
And so right away they're trying to breathe deep.
They're trying to breathe perfectly smooth.
And they're trying to force their breath.
And the more they force it, the more suffering comes to them,
the more stressful it is.
Then they blame the meditation and say, this sucks.
This is terrible.
It's not pleasant.
It's uncomfortable.
What really sucks is our attitude towards it.
And then we're forcing, controlling,
and stressing, needing it to be in a certain way.
And not able to accept it for what it is.
Even we sit in meditation, we get headaches.
So what?
What's wrong with the headache?
What's wrong is our stress that comes from having a headache.
People sit in meditation, they have a backache.
And they have all these theories about how you have to sit like this or like that.
Then your back will be perfect and they'll be no pain and so on.
And it's really ridiculous.
Your back can't be perfect.
Your back's just going to get worse and worse until you die.
That's not what we're here for.
I've sat in meditation for eight years with my back hunched over.
My back's not dead yet.
My teacher's 86 this year.
And he's been doing it I guess for 50, maybe 60, probably 60 over 60 years.
With his back hunched.
I'm not hunched.
It just means sitting naturally.
I'm not worried about your posture.
I'm not worried about anything.
Just watching it.
Oh, now there's pain.
Oh, interesting.
It'll be fine.
This means this is the practice of insight to see clearly.
Not to judge, not to need, not to worry, not to stress.
Everything just works itself out.
Come back to nature.
You come back to now.
You come back to reality.
There's nothing in the world that we will claim to.
It's kind of like untying a knot.
You have this big knot and you look at it and you say,
well, that's a mess.
There's this big knot there.
But as you untie it just disappears.
And when you've got it totally untied, there's nothing there.
It's just gone.
Really, all of our problems and all of our stresses, all of our worries and all of our cares.
They're all just like the knot.
They're not really even there.
They're just, all they are is a kink.
All the errors are not.
We have all these ideas of who we are, what we are, what we have, what we deal with.
Places, people, things, ideas, concepts, views, everything, the longings.
None of them exist, they're not even real.
It's all just knots clinging, liking, or disliking.
Holding up as good, throwing down as bad.
Pulling in as me, pushing out as you.
In the end it's all just experience, it's all just reality.
It's just one moment of experience.
We've created so many knots.
And then we get worried and stressed about them and say, oh, look at that big knot.
It's not even there, it's just a kink.
It's just a rope.
Got it out of shape.
We'll bunched up, we'll mist up.
I do straighten it out.
Straighten it out and keep going.
So in that sense, Nirvana or Nirvana, the goal of the practice, there's not really anything special.
It's not really anything mysterious.
It's just when all the knots are gone.
I mean, it is something profound because there's nothing left.
At the experience of Nirvana, it's totally unkinked.
And there's nothing, there's no friction.
There's no stress.
There's no seeing.
There's no hearing.
There's no smelling.
There's no tasting.
There's no feeling.
There's no thinking.
Suddenly just ceased.
And it might even be momentary.
Nirvana can be just a fleeting moment, but it's a profound moment.
Something that changes a person to the core, changes the way the person thinks.
It's like a cut.
Finally, the bonds are broken.
Like there's a gap in the string or a gap in the chain.
And that profoundly shakes out the way the person looks at the world because before we looked at the world as being.
And being full of things and concepts and people and selves and souls and egos.
Once you experience the sensations of stress, the cessation of all of this,
it all just clears up and it becomes just experience, just reality.
And then it all bunches up again.
And you're back to liking or disliking, but there's something, there's a gap there.
There's a memory of this.
There's a selected mark as well.
And you can't ever look at things as permanent as stable as me or mine.
It just doesn't make sense anymore.
The experience of total cessation.
And this is even more true, the more you practice and the more it comes that a person is able to experience this state.
To the point where all the kings, there's so many gaps, there's so many interruptions.
And it's just the whole thing just collapses and there's no more, there's no more kinking.
And the wisdom finally gains hold and understanding that this is the way it is finally gains hold.
The mind finally changes from still believing, still seeking, still wishing, still hoping that there's going to be something that's stable, something that's permanent, something that's satisfying, something that we can control.
Finally accepts the fact that there's nothing in the world that's really wonderful and beautiful.
And they say, this is the final graduation.
This is finally becoming free from illusion and we finally understand reality as it is.
It's very simple.
It's just seeing things as they are.
There's no special reality or special experience.
It just is reality, just as experience.
And the only problem, the only single problem is our delusion or misunderstanding of things.
The fact that we can't see things as they are, we can't see things clearly.
So I think that's enough for today.
If you like, you're welcome to just continue meditating here.
If anyone has any questions, you're welcome to ask.
Thank you all for coming.
