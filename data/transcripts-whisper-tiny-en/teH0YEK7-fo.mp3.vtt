WEBVTT

00:00.000 --> 00:05.000
Can you make progress and have insights if you practice one hour a day?

00:05.000 --> 00:09.000
Or do you need a retreat to progress and have insights?

00:09.000 --> 00:15.000
Of course it takes longer, but maybe after one or two years of one hour a day.

00:19.000 --> 00:23.000
Can you make progress and have insights if you practice one hour a day?

00:23.000 --> 00:25.000
Well, I think one person had an insight or enlightenment,

00:25.000 --> 00:28.000
after just hearing the Buddha speak.

00:28.000 --> 00:33.000
So, I guess.

00:33.000 --> 00:39.000
So, you're going to practice who you are because for some of us, that doesn't work.

00:39.000 --> 00:45.000
One hour a day, if you're meditating for one hour a day, I would assume

00:45.000 --> 00:49.000
you'd want to keep your practice going throughout the day.

00:49.000 --> 00:53.000
Not just leave it on the cushions.

00:53.000 --> 00:55.000
For sure.

00:55.000 --> 01:00.000
But you're only meditating for one hour a day.

01:02.000 --> 01:07.000
I think at the beginning, that is a long time.

01:07.000 --> 01:12.000
I know when I first started, I didn't think I could meditate for five minutes.

01:12.000 --> 01:16.000
And meditation is something that gets easier and easier,

01:16.000 --> 01:20.000
and at the beginning I used to time myself.

01:20.000 --> 01:27.000
And then you get to the point where you're worried about meditating too long.

01:27.000 --> 01:31.000
So, that's one thing.

01:31.000 --> 01:34.000
And for me at the beginning, one hour, in a day,

01:34.000 --> 01:36.000
would have been a lot of meditating.

01:36.000 --> 01:43.000
It's just something that just got easier and easier for me.

01:43.000 --> 01:47.000
And so, for a beginner, I think if they take small steps,

01:47.000 --> 01:50.000
they're more likely to stick with it.

01:50.000 --> 01:55.000
And also, if they know ahead of time that, hey, it starts off tough.

01:55.000 --> 02:00.000
And I think it's probably tough for most people at the beginning.

02:03.000 --> 02:07.000
I can only say that hours are not so much important,

02:07.000 --> 02:12.000
like moments when you're meditating.

02:12.000 --> 02:17.000
You can sit for hours and sit and don't make progress.

02:17.000 --> 02:20.000
For sure.

02:20.000 --> 02:25.000
And that's sort of how I wanted to approach this,

02:25.000 --> 02:29.000
for sure what Alfred said is perfect,

02:29.000 --> 02:33.000
perfect, good, perfectly good answer, and your answer as well.

02:33.000 --> 02:40.000
But one thing that can reassure us about such situations

02:40.000 --> 02:46.000
where you're only able to do X number of minutes or hours per day,

02:46.000 --> 02:51.000
is that at every moment that you're mindful,

02:51.000 --> 02:53.000
you've done something good for yourself.

02:53.000 --> 02:57.000
You've created what we call a wholesome karma.

02:57.000 --> 02:59.000
One moment of acknowledging,

02:59.000 --> 03:01.000
one time of saying to yourself,

03:01.000 --> 03:02.000
rising or saying to yourself,

03:02.000 --> 03:06.000
one moment of recognizing something for what it is,

03:06.000 --> 03:08.000
and fixing your mind on it as it is.

03:08.000 --> 03:13.000
You're changing something about yourself and about your future

03:13.000 --> 03:15.000
in a positive way.

03:15.000 --> 03:18.000
So if you do that lots of times during the day,

03:18.000 --> 03:21.000
even an hour's worth,

03:21.000 --> 03:23.000
of course you can't do a full hours worth,

03:23.000 --> 03:27.000
but if within an hour you're able to do that many times,

03:27.000 --> 03:32.000
then that's many instances of wholesome karma.

03:32.000 --> 03:35.000
It's purified some aspect of your existence.

03:35.000 --> 03:40.000
So the question, as Bill said,

03:40.000 --> 03:45.000
some people can become enlightened just listening to a teaching.

03:45.000 --> 03:50.000
So as I replied, it's not very much depends on who you are.

03:50.000 --> 03:54.000
There's no guarantee that if you do intensive meditation

03:54.000 --> 03:58.000
for a year or two years that you'll become enlightened.

03:58.000 --> 04:00.000
It's much more likely,

04:00.000 --> 04:03.000
but it's not a guarantee.

04:03.000 --> 04:06.000
It totally depends on who you are.

04:06.000 --> 04:09.000
Of course who your teacher is

04:09.000 --> 04:12.000
and what center you're staying at and all of these things.

04:12.000 --> 04:14.000
What your past karma is,

04:14.000 --> 04:17.000
sometimes your past karma is such that it gets in your way.

04:17.000 --> 04:22.000
If you get sick or somehow you're not able to practice,

04:22.000 --> 04:26.000
some people even go crazy because of intense bad past karma,

04:26.000 --> 04:28.000
it can happen.

04:28.000 --> 04:30.000
So there's many aspects.

04:30.000 --> 04:36.000
You should never be set on a time limit

04:36.000 --> 04:40.000
that I want to become enlightened in the next number of days.

04:40.000 --> 04:43.000
We should try to go as quickly as we can

04:43.000 --> 04:46.000
or to the best of our ability to train ourselves,

04:46.000 --> 04:50.000
but you have to be realistic that you might take lifetime

04:50.000 --> 04:52.000
to become enlightened.

04:52.000 --> 04:56.000
Some people, even in the Buddhist time,

04:56.000 --> 04:58.000
didn't become enlightened,

04:58.000 --> 05:00.000
made a determination to become enlightened

05:00.000 --> 05:04.000
in the time of Mehtayabuddha.

05:04.000 --> 05:07.000
And so they are angels up in heaven right now

05:07.000 --> 05:09.000
or wherever they are.

05:09.000 --> 05:12.000
And because of their determination and their intentions,

05:12.000 --> 05:15.000
they're going to be born sometime in the far, far future

05:15.000 --> 05:18.000
as disciples of Mehtayabuddha, supposedly,

05:18.000 --> 05:22.000
or possibly.

05:22.000 --> 05:27.000
So moments of mindfulness are in and of themselves beneficial,

05:27.000 --> 05:31.000
let alone a daily practice of one hour or two hours a day,

05:31.000 --> 05:34.000
which is a great thing.

05:34.000 --> 05:39.000
If, as Paulina said, you're actually mindful every moment

05:39.000 --> 05:43.000
of that or even if you're mindful for many moments,

05:43.000 --> 05:45.000
it's a great thing.

05:45.000 --> 05:49.000
But I'll tell you what people say when they come here,

05:49.000 --> 05:52.000
especially now that I'm teaching on the internet,

05:52.000 --> 05:56.000
that they are shocked at the difference

05:56.000 --> 06:02.000
between doing one hour, even one hour a day,

06:02.000 --> 06:10.000
and coming here and dedicating their waking hours to it.

06:10.000 --> 06:12.000
That in and of itself has been an obstacle

06:12.000 --> 06:14.000
for people coming here now,

06:14.000 --> 06:17.000
because of their expectations of how easy it is.

06:17.000 --> 06:20.000
Or, of course, it's difficult to do an hour a day,

06:20.000 --> 06:23.000
but they say, okay, well, that kind of difficulty I can handle.

06:23.000 --> 06:25.000
So they now come here expecting it

06:25.000 --> 06:28.000
to be that difficult or a little more difficult.

06:28.000 --> 06:31.000
It's an order of magnitude more difficult.

06:31.000 --> 06:34.000
And I would say it is an order of magnitude more beneficial.

06:34.000 --> 06:39.000
You can't compare one hour or two hours a day of meditation

06:39.000 --> 06:45.000
to eight, ten, twelve hours,

06:45.000 --> 06:48.000
but moreover, just an utter dedication

06:48.000 --> 06:51.000
to the meditation practice with no interruptions.

06:51.000 --> 06:54.000
And the knowledge that when you wake up in the morning,

06:54.000 --> 06:59.000
that's what you're going to start doing until you fall asleep at night.

06:59.000 --> 07:04.000
That's where very quick changes occur.

07:04.000 --> 07:07.000
So the question of whether after a year or two,

07:07.000 --> 07:11.000
one can expect progress in insights.

07:11.000 --> 07:14.000
I guess the question one might be asking is,

07:14.000 --> 07:16.000
or is it futile?

07:16.000 --> 07:19.000
And I would say no, of course, it's not futile.

07:19.000 --> 07:22.000
If we don't do good deeds,

07:22.000 --> 07:25.000
the alternative is only to do bad deeds.

07:25.000 --> 07:30.000
And that leads us definitely to a bad state.

07:30.000 --> 07:35.000
If we inject into our lives of good and badness,

07:35.000 --> 07:38.000
if we inject more goodness and purity,

07:38.000 --> 07:41.000
it can't help but lead us in a good way.

07:41.000 --> 07:43.000
So it will lead us closer to Buddhism,

07:43.000 --> 07:45.000
closer to meditation.

07:45.000 --> 07:46.000
It will make it easier to meditate.

07:46.000 --> 07:48.000
I mean, without question,

07:48.000 --> 07:51.000
people who have started practicing in this tradition

07:51.000 --> 07:54.000
based on the teachings and the internet I put up.

07:54.000 --> 07:55.000
Without question,

07:55.000 --> 07:57.000
it is actually easier for them when they arrive.

07:57.000 --> 08:00.000
They can already do half hour walking, half hour sitting,

08:00.000 --> 08:02.000
which in my day,

08:02.000 --> 08:05.000
we started at 10 minutes walking, 10 minutes sitting.

08:05.000 --> 08:09.000
So to have already started,

08:09.000 --> 08:11.000
it is a great thing.

08:11.000 --> 08:15.000
It leads, it can only lead you closer to enlightenment.

08:15.000 --> 08:20.000
If the question is whether you can become a Sotupana in that way,

08:20.000 --> 08:24.000
I would say it's quite difficult in a couple of years.

08:24.000 --> 08:28.000
Now, it might happen in a couple of lifetimes,

08:28.000 --> 08:32.000
but if you're looking for something quicker than a couple of lifetimes,

08:32.000 --> 08:37.000
you really are advised to, at some point,

08:37.000 --> 08:39.000
take up intensive practice.

08:39.000 --> 08:43.000
So an example we have is of people doing one or two hours a day,

08:43.000 --> 08:46.000
two hours, I think, minimum a day,

08:46.000 --> 08:52.000
and then spending a weekend of intensive practice,

08:52.000 --> 08:54.000
where they put it all together.

08:54.000 --> 08:58.000
And, you know, I've never done this,

08:58.000 --> 09:02.000
but I've heard of good results.

09:02.000 --> 09:06.000
From people who have led their students through it.

09:06.000 --> 09:08.000
Now, you know, they say it's good results.

09:08.000 --> 09:10.000
You have to trust them and how do they really know

09:10.000 --> 09:13.000
because of course the damage to be realized for oneself.

09:13.000 --> 09:17.000
But I think the best we can say

09:17.000 --> 09:20.000
with certainty is that it's certainly beneficial,

09:20.000 --> 09:23.000
and it's absolutely worth doing,

09:23.000 --> 09:25.000
and it will change you.

09:25.000 --> 09:27.000
It will bring you closer to the demo.

09:27.000 --> 09:29.000
It will change many things about your life,

09:29.000 --> 09:33.000
and it will help you prepare for eventual intensive practice,

09:33.000 --> 09:34.000
if you do decide on it.

09:34.000 --> 09:37.000
And it may even lead you to profound insights,

09:37.000 --> 09:39.000
and even enlightenment,

09:39.000 --> 09:42.000
depending on your qualities and perfection

09:42.000 --> 09:45.000
and your dedication to it.

09:45.000 --> 09:47.000
It depends how you're living your life, as Bill said.

09:47.000 --> 09:49.000
No, if you're mindful throughout the day,

09:49.000 --> 09:52.000
it's better than some people who come to meditation centers

09:52.000 --> 10:13.000
and just sit around and fall asleep on their sitting med.

