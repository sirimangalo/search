1
00:00:00,000 --> 00:00:03,280
Is there a Buddhist approach to get new friends?

2
00:00:03,280 --> 00:00:07,200
I mean, I heard your recent talks on the Sutter study.

3
00:00:07,200 --> 00:00:09,680
As I see it, the one way to get new friends

4
00:00:09,680 --> 00:00:15,120
arises from purity and the eightfold noble path.

5
00:00:15,120 --> 00:00:15,640
Right?

6
00:00:23,680 --> 00:00:27,360
Part two, yeah, to get a new set of friends,

7
00:00:27,360 --> 00:00:33,040
where you've got some friends who you realize are not friends that

8
00:00:33,040 --> 00:00:37,760
at all, but are enemies in sheep and wolf in sheep's clothing.

9
00:00:37,760 --> 00:00:41,600
So what do you do to change that?

10
00:00:42,560 --> 00:00:45,680
Well, this kind of conflict that we're talking about is a big part.

11
00:00:45,680 --> 00:00:50,320
When I started practicing meditation, I lost all my friends

12
00:00:51,120 --> 00:00:55,600
and a lot of that was due to the conflict that I wasn't at all interested in

13
00:00:55,600 --> 00:01:02,800
compromising. I'm just very keen on changing my life.

14
00:01:03,040 --> 00:01:06,960
So at first, I thought I could do it, I could bring my friends along

15
00:01:06,960 --> 00:01:10,320
and so I tried my best to encourage them in it and

16
00:01:10,320 --> 00:01:14,480
absolutely none of them were interested.

17
00:01:14,480 --> 00:01:20,000
There were a couple of old ex-girlfriends

18
00:01:20,000 --> 00:01:26,480
who were managed to convince, but I don't know how purely intentions were there.

19
00:01:26,480 --> 00:01:30,080
They weren't completely impure their intentions.

20
00:01:30,080 --> 00:01:33,360
And I got a couple of couple of people involved,

21
00:01:33,360 --> 00:01:36,080
temporarily. One person actually went on to

22
00:01:36,080 --> 00:01:40,960
do a going for course. But yeah, so the conflict is a big

23
00:01:40,960 --> 00:01:44,720
part of it where you don't compromise.

24
00:01:44,720 --> 00:01:47,600
When you stop compromising your friends, change rig,

25
00:01:47,600 --> 00:01:53,040
your social circle changes very quickly.

26
00:01:56,080 --> 00:01:59,040
I don't think you should seek out new friends, particularly.

27
00:01:59,040 --> 00:02:04,240
You might want to seek out a meditation teacher,

28
00:02:04,240 --> 00:02:06,880
which is considered to be the best friend,

29
00:02:06,880 --> 00:02:09,920
someone who can guide you. So that friend that we talked about yesterday,

30
00:02:09,920 --> 00:02:14,480
that the one who gives advice gives guidance.

31
00:02:14,480 --> 00:02:19,120
I would seek out such a, that sort of friend.

32
00:02:23,760 --> 00:02:28,240
But if you can, if you can find a dumb group, if you can put together a

33
00:02:28,240 --> 00:02:32,320
dumb group even, that would be something interesting to try.

34
00:02:32,320 --> 00:02:36,000
I actually thought we could put together some sort of package

35
00:02:36,000 --> 00:02:41,120
where they're going to tradition, where we have this package and you

36
00:02:41,120 --> 00:02:44,960
pick up the package and it's like I do it yourself,

37
00:02:44,960 --> 00:02:51,680
dumb group. So ways and means of putting together your own meditation group.

38
00:02:54,400 --> 00:02:57,920
And we could do that even, that's something interesting that we could even do on

39
00:02:57,920 --> 00:03:01,920
the Siri Mongolo site. Anybody who wants to start a dumb group,

40
00:03:01,920 --> 00:03:05,200
we could give them a subdomain or a page of their own.

41
00:03:05,200 --> 00:03:08,800
And it would have all sorts of tools on it or something,

42
00:03:08,800 --> 00:03:13,680
setting up a group and I don't know,

43
00:03:13,680 --> 00:03:17,120
whatever someone they would really need as someone who can

44
00:03:17,120 --> 00:03:22,400
figure out how to put that together to allow people to just set up their own

45
00:03:22,400 --> 00:03:29,200
group and attract other like-minded people in their, in their area.

46
00:03:29,200 --> 00:03:33,120
That's maybe the next iteration of Siri Mongolo.org,

47
00:03:33,120 --> 00:03:36,880
but I need some help. I'm not really going to be able to

48
00:03:36,880 --> 00:03:40,880
expand upon the website.

