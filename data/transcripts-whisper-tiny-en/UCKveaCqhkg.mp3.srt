1
00:00:00,000 --> 00:00:07,280
Hi, next question is from nomad1545.

2
00:00:07,280 --> 00:00:12,040
I hope this is a proper question to the series, since it is a subject that has been in

3
00:00:12,040 --> 00:00:14,440
my mind for a very long time.

4
00:00:14,440 --> 00:00:20,760
How does and how can meditation practice help one that had a relationship torn apart with

5
00:00:20,760 --> 00:00:25,600
another person?

6
00:00:25,600 --> 00:00:33,080
I suppose it's important to state off the bat that meditation doesn't particularly help

7
00:00:33,080 --> 00:00:38,200
with relationships, specific relationships with other people.

8
00:00:38,200 --> 00:00:48,160
And it does often, in fact, weaken our bond with the people who we use to find ourselves

9
00:00:48,160 --> 00:00:50,200
very attracted to.

10
00:00:50,200 --> 00:00:54,880
And why it does this is because meditation helps us to relate to humankind in general.

11
00:00:54,880 --> 00:00:57,760
It helps us to relate to humanity.

12
00:00:57,760 --> 00:01:04,880
And so as opposed to being partial to certain people, which we're less inclined to do,

13
00:01:04,880 --> 00:01:11,280
we come to accept all people and to be able to see into the nature of all people.

14
00:01:11,280 --> 00:01:19,440
We often, I would say a meditator often finds themselves less inclined to become friends

15
00:01:19,440 --> 00:01:26,320
with or become close with the majority of humankind, even the people who are also striving

16
00:01:26,320 --> 00:01:33,240
to practice meditation, because what you start to realize is that you have bad things

17
00:01:33,240 --> 00:01:38,560
inside and the people around you who you used to think were perfect also have bad things

18
00:01:38,560 --> 00:01:39,760
inside of them.

19
00:01:39,760 --> 00:01:45,080
And all that happens when you get involved with them is you clash, these defilements,

20
00:01:45,080 --> 00:01:47,920
these bad things inside, they clash together.

21
00:01:47,920 --> 00:02:01,520
And this is why monasticism and solitude are so much encouraged in the Buddha's teaching.

22
00:02:01,520 --> 00:02:11,320
So first off, as far as helping to solidify or to maintain a relationship, I would say not

23
00:02:11,320 --> 00:02:20,120
very likely meditation helps you to deal with the problem of having a relationship torn

24
00:02:20,120 --> 00:02:25,360
apart or the sense of loss that comes from the, you've got to say it, from the addiction

25
00:02:25,360 --> 00:02:30,360
to the other person, from the need to be close to a certain individual, the partiality

26
00:02:30,360 --> 00:02:37,080
towards them by helping you to become an island unto yourself, helping you to become strong

27
00:02:37,080 --> 00:02:43,880
and to see the nature of reality, that actually what's going on when you are engaged

28
00:02:43,880 --> 00:02:54,480
in a relationship is a whole lot of addiction and attachment and the pleasure and the need

29
00:02:54,480 --> 00:02:59,800
for the pleasure, the need for the happiness that comes and the memories that are associated

30
00:02:59,800 --> 00:03:04,600
with that, oh, the good times how they used to be and so on and so on.

31
00:03:04,600 --> 00:03:10,640
So meditation helping us to see these things clearly helps us to realize that on another

32
00:03:10,640 --> 00:03:16,520
level helps us to realize that there is no eye and there is no you, there's no and

33
00:03:16,520 --> 00:03:20,520
them, there's no parts to the relationship.

34
00:03:20,520 --> 00:03:25,560
When there's a relationship going on, there's just phenomena arising and ceasing, there's

35
00:03:25,560 --> 00:03:30,960
the mind and there's mind and there's body, there's seeing, there's hearing, there's smelling,

36
00:03:30,960 --> 00:03:33,840
there's tasting, there's feeling and there's thinking.

37
00:03:33,840 --> 00:03:40,720
And these things come and go and are always in a state of flux and so we come to be less

38
00:03:40,720 --> 00:03:48,400
attached to the concept of an individual and we come to see experience as the essential

39
00:03:48,400 --> 00:03:54,440
nature of reality and so as a result as I said in the beginning we become more universal

40
00:03:54,440 --> 00:03:59,400
in our love and in our appreciation of the good parts of humanity, the good qualities

41
00:03:59,400 --> 00:04:05,920
in our love and our compassion towards other beings so that we only see the evil, the good,

42
00:04:05,920 --> 00:04:13,000
the suffering and the happiness and we don't see individuals and we don't become partial.

43
00:04:13,000 --> 00:04:17,280
Meditation certainly helps you to deal with a torn relationship, it just might not lead

44
00:04:17,280 --> 00:04:23,800
you to go back to that relationship and instead it might bring you to a much more, a much

45
00:04:23,800 --> 00:04:31,640
more comfortable, peaceful state of good relationship with all beings or proper relationship

46
00:04:31,640 --> 00:04:38,920
and in general much less of a relationship outwardly with all beings where you start to

47
00:04:38,920 --> 00:04:46,000
look inwardly when you see the nature of your own existence and you come to understand

48
00:04:46,000 --> 00:04:52,480
the nature of all existence and you don't mess around with external relationships because

49
00:04:52,480 --> 00:04:58,360
you see that that's generally bound up in defilement so I hope that is somehow getting

50
00:04:58,360 --> 00:05:03,160
at the question and giving you a bit of an answer in terms of a Buddhist perspective

51
00:05:03,160 --> 00:05:30,280
okay thanks for the question, keep them coming.

