1
00:00:00,000 --> 00:00:02,000
Another difference of O.P.

2
00:00:04,240 --> 00:00:13,400
That way, something that which has full confirmaries and change of lineage as fifth and first consciousness as sixth has one fusion consciousness

3
00:00:14,920 --> 00:00:19,640
But that is refuted because it is the full or the fifth in function that reaches the part

4
00:00:19,680 --> 00:00:23,720
Not those after that going to their nearness to the life continuum

5
00:00:24,360 --> 00:00:25,560
when

6
00:00:25,560 --> 00:00:27,000
when you are

7
00:00:27,000 --> 00:00:29,000
going towards

8
00:00:29,000 --> 00:00:31,000
a steep cliff

9
00:00:31,000 --> 00:00:35,120
When you are too close to this cliff that you cannot stop there

10
00:00:35,920 --> 00:00:37,920
Because you are going

11
00:00:38,040 --> 00:00:42,640
Going with force so you simply fall into the

12
00:00:43,400 --> 00:00:50,320
Preceptives in the same way and the the thought moments are going very fast and so it cannot stop as

13
00:00:50,320 --> 00:00:59,920
As a mega in the sixth and at the sixth moment and so

14
00:01:02,920 --> 00:01:06,120
It is that is why it is refuted that

15
00:01:07,920 --> 00:01:10,800
There is one one fusion consciousness out here

16
00:01:12,640 --> 00:01:14,640
so

17
00:01:15,280 --> 00:01:17,280
It is in admissible

18
00:01:17,280 --> 00:01:21,440
When you say there is only one

19
00:01:22,200 --> 00:01:27,400
Fluidian consciousness because there cannot be one solution consciousness

20
00:01:30,680 --> 00:01:40,520
Because the thought process becomes close too close to bowanga and when it becomes too close to bowanga

21
00:01:40,520 --> 00:01:41,880
It cannot

22
00:01:41,880 --> 00:01:43,880
stand as the

23
00:01:43,880 --> 00:01:47,680
The mega consciousness on the sixth

24
00:01:48,160 --> 00:01:54,920
So when there is no to no first consciousness at the sixth moment then they are going to be no

25
00:01:55,200 --> 00:02:00,320
Fruising consciousness and only no one fruising consciousness at the seventh moment. So

26
00:02:02,840 --> 00:02:10,760
So that so that cannot be accepted as correct and at this point this film enter a call the second noble bus and now

27
00:02:10,760 --> 00:02:18,360
So he is called the second noble bus and now at the moment of thought consciousness. He's called the first noble bus and

28
00:02:19,560 --> 00:02:26,080
Next moment. He is called the second noble bus and just in one sitting mate and very very brief

29
00:02:27,200 --> 00:02:33,200
Succession of thought moments at the moment of fact consciousness. He is first

30
00:02:33,920 --> 00:02:36,960
noble bus and then at the second

31
00:02:36,960 --> 00:02:44,000
Moment that means at the fruit fruising moment. He is called the second noble person

32
00:02:44,960 --> 00:02:50,160
That is why we have eight there are eight noble bussens actually we can we can

33
00:02:51,920 --> 00:02:57,840
We can meet only four noble bussens who have reached the the fruition stage

34
00:02:59,160 --> 00:03:01,160
so

35
00:03:01,160 --> 00:03:03,160
At the end

36
00:03:03,160 --> 00:03:10,280
It's called second noble bussens. How about navelation? He may be he is bound to make an end of suffering when he has traveled and

37
00:03:10,520 --> 00:03:15,880
Traverse the round of rebut among deities and human beings for the seventh time

38
00:03:16,560 --> 00:03:20,560
Because he has become a sort of an extreme extreme entrant string winner

39
00:03:21,560 --> 00:03:25,680
Now however navelation he may be if he does not

40
00:03:26,760 --> 00:03:28,760
Practice more meditation

41
00:03:29,560 --> 00:03:31,560
It does not mean that

42
00:03:31,560 --> 00:03:33,560
He may be

43
00:03:33,560 --> 00:03:40,140
Breaking the five precepts or whatever, but say if he if he gives up meditation and he doesn't meditate

44
00:03:40,800 --> 00:03:44,240
22 to reach the higher stages then

45
00:03:47,560 --> 00:03:54,080
He is bound to make an end of suffering when he has traveled and traversed a lot of rebut among deities and human beings for the seventh time

46
00:03:54,200 --> 00:03:57,880
so he will be reborn only seven times and

47
00:03:57,880 --> 00:03:59,880
And

48
00:03:59,880 --> 00:04:01,880
at the seventh life

49
00:04:01,880 --> 00:04:06,240
He will he will surely become an arachan and then

50
00:04:07,000 --> 00:04:09,000
Say it

51
00:04:09,000 --> 00:04:11,000
Makes an end of suffering

52
00:04:11,360 --> 00:04:17,000
So the sort of honor has six more I mean seven more rebuts

53
00:04:20,520 --> 00:04:26,040
At the end of the fruition his consciousness enters the life continuum so after the fruition

54
00:04:26,040 --> 00:04:28,040
In a period

55
00:04:28,360 --> 00:04:31,880
There is life continuum. That means that the end of that thought process

56
00:04:35,160 --> 00:04:38,120
After that it arises as mind or

57
00:04:38,440 --> 00:04:44,280
Advading interrupting the life continuum for the bubbles of reviewing the path now immediately after

58
00:04:44,840 --> 00:04:46,840
Becoming a noble person

59
00:04:47,880 --> 00:04:49,880
He reviews the path

60
00:04:51,000 --> 00:04:53,000
the fruition

61
00:04:53,000 --> 00:04:56,840
Lebana and development a better and

62
00:04:57,480 --> 00:05:00,520
Defungments remaining so these five things

63
00:05:01,400 --> 00:05:03,400
It wasn't who has reached enlightenment

64
00:05:04,600 --> 00:05:06,600
Reduce or reflected upon

65
00:05:10,760 --> 00:05:15,240
You know so it won't be the difference from your father and as you talk

66
00:05:16,280 --> 00:05:20,000
Yeah, and light and meant is this

67
00:05:20,000 --> 00:05:24,240
The consciousness here and nivana is

68
00:05:25,120 --> 00:05:27,520
the object of them and i didn't mean moment

69
00:05:28,160 --> 00:05:36,160
So at the moment of enlightenment the path consciousness arises and that first path consciousness takes nivana is object

70
00:05:36,160 --> 00:05:38,160
So nivana is an external object

71
00:05:38,160 --> 00:05:47,920
And it is taken as object bind the path consciousness and also the fruition consciousness

72
00:05:51,200 --> 00:05:53,200
So immediately after

73
00:05:54,400 --> 00:05:56,400
Enlightenment it wasn't

74
00:05:57,040 --> 00:06:00,400
Reflects upon or reviews five things what are the five

75
00:06:00,400 --> 00:06:06,720
Pollution nivana development a better

76
00:06:08,000 --> 00:06:10,000
And development remaining

77
00:06:10,720 --> 00:06:14,000
What fun development I have a better that what if I meant

78
00:06:14,560 --> 00:06:18,720
A main and a better so this also getting flexible these five

79
00:06:19,760 --> 00:06:21,760
Then

80
00:06:22,320 --> 00:06:24,320
Among these five

81
00:06:24,320 --> 00:06:34,000
Train us may or may not have the reviewing of the development's abundance and those still remaining

82
00:06:34,320 --> 00:06:38,320
So the last two they may or may not review

83
00:06:39,360 --> 00:06:45,280
So the first three they definitely review the path fruition and nivana

84
00:06:45,280 --> 00:06:55,200
But the development abandoned and development remaining did me or they may not review

85
00:06:55,840 --> 00:07:02,240
In fact, it was going to the absence of such reviewing that mahar namma asked the lesson one

86
00:07:02,240 --> 00:07:07,280
What state is there still an abandoned by me internally going to which at hand state of

87
00:07:07,280 --> 00:07:12,960
create envy my mind and remain now mahar namma was an enlightened person

88
00:07:12,960 --> 00:07:19,120
But sometimes he he had thoughts of the greed or thoughts of

89
00:07:20,240 --> 00:07:25,200
Fear or thoughts of anger, but he he did not review

90
00:07:26,480 --> 00:07:31,520
The dharma's when he became enlightened so he did not know which

91
00:07:32,480 --> 00:07:38,640
Defiance he has a better and which remain to be abandoned so he asked the Buddha

92
00:07:38,640 --> 00:07:41,440
and

93
00:07:41,440 --> 00:07:43,440
the Buddha

94
00:07:43,440 --> 00:07:45,440
Oh him that because you did not

95
00:07:46,080 --> 00:07:50,720
Reduce the department so you don't know all of which should be called it so

96
00:07:52,960 --> 00:07:54,960
It is a much in many guy

97
00:07:56,080 --> 00:07:59,120
Now knowledge of the second path that's noble person

98
00:08:00,560 --> 00:08:05,840
However after reviewing in this way either while sitting in the same session or on another occasion

99
00:08:05,840 --> 00:08:11,440
The noble disciple who is a supreme emperor makes it his task to reach the second plane

100
00:08:12,000 --> 00:08:16,000
By attenuating good greed for such desires and ill will and so on

101
00:08:16,560 --> 00:08:17,680
now

102
00:08:17,680 --> 00:08:19,680
You may it wasn't made

103
00:08:20,720 --> 00:08:26,640
Proceed to to get the second stage in that sitting in that one city or he may just stop

104
00:08:28,080 --> 00:08:32,640
practicing my dedication after he gains he reaches the first stage and then he may

105
00:08:32,640 --> 00:08:39,440
He may take rest or he may not practice my decision for any length of time

106
00:08:40,080 --> 00:08:42,240
So that that that is up to him

107
00:08:42,880 --> 00:08:45,440
So in the same session or on another occasion

108
00:08:46,160 --> 00:08:51,040
Sometimes he doesn't become the sautabana and then he may remain a sautabana for maybe

109
00:08:52,800 --> 00:08:57,680
Months a year and then he practiced my dedication and he reaches a second stage or

110
00:08:58,960 --> 00:09:00,960
some people

111
00:09:00,960 --> 00:09:05,600
reach the the full stages or three stages or two stages and one sitting

112
00:09:06,880 --> 00:09:08,880
So it depends upon his

113
00:09:11,200 --> 00:09:13,840
Wish to be to reach higher stages

114
00:09:15,040 --> 00:09:16,320
So

115
00:09:16,320 --> 00:09:22,880
He brings to where the faculties the power the enlightenment practice and he worked over a chance of that same field of formation

116
00:09:22,880 --> 00:09:31,680
Plus a stability feeling perception formations and consciousness would be knowledge that they are informative painful not so and he unbox upon the progressive

117
00:09:32,000 --> 00:09:35,840
series of insight so after after a reason first stage

118
00:09:36,960 --> 00:09:41,920
If you want to reach the second stage he must practice men with us on a meditation game

119
00:09:43,600 --> 00:09:45,600
But at that time his wibasana

120
00:09:46,000 --> 00:09:47,600
becomes swift

121
00:09:47,600 --> 00:09:49,600
not like the wibasana or

122
00:09:49,600 --> 00:09:56,720
Ordinary persons, but he must again practice be person to reach the second stage

123
00:09:57,360 --> 00:10:04,400
And then to reach the third stage to be the fourth stage so at every before at before every stage

124
00:10:04,720 --> 00:10:11,440
He must practice with person. That is why we say there can be no no enlightenment without the person

125
00:10:11,680 --> 00:10:13,680
So wibasana must proceed

126
00:10:14,640 --> 00:10:16,640
or

127
00:10:16,640 --> 00:10:18,640
Mm-hmm

128
00:10:18,640 --> 00:10:20,640
enlightenment or all

129
00:10:21,040 --> 00:10:23,040
arising of fat consciousness

130
00:10:26,640 --> 00:10:28,640
So

131
00:10:30,160 --> 00:10:36,320
The second part and the third noble boson then second flushing and the fourth noble boson and so on

132
00:10:39,200 --> 00:10:41,200
Now

133
00:10:41,200 --> 00:10:48,480
The second fruit fourth noble boson the fruising consciousness should be understood to follow immediately upon this knowledge

134
00:10:48,480 --> 00:10:53,920
And the same way as before and at this point this once retainer is called the fourth noble boson

135
00:10:55,280 --> 00:10:58,320
Who has reached the second stage so he has a fourth noble boson

136
00:10:58,720 --> 00:11:03,520
He is bound to make an end of suffering after returning once to this world

137
00:11:04,560 --> 00:11:06,560
Now

138
00:11:06,560 --> 00:11:12,560
What is meant by to this world?

139
00:11:12,800 --> 00:11:14,800
To this human world

140
00:11:16,560 --> 00:11:18,560
So he

141
00:11:18,720 --> 00:11:22,320
After after reaching the second stage, he may be reborn as a

142
00:11:23,280 --> 00:11:30,320
S a celestial being and then from from there from there he will he will be reborn as a human being again

143
00:11:30,320 --> 00:11:39,360
So he is the one who who returns here once once returner so to this world means to this human world

144
00:11:43,120 --> 00:11:46,880
Knowledge of the third but fifth noble person and that is

145
00:11:46,880 --> 00:11:58,960
The third part is a non-returner right non-returner spot

146
00:12:03,520 --> 00:12:11,440
And then the third part is immediately followed by third fruition so after that fusion is called a non-returner

147
00:12:11,440 --> 00:12:17,040
And so he's called the sixth noble person after that he reappears

148
00:12:17,280 --> 00:12:25,280
Apparitionally elsewhere and a change complete extinction there without ever returning without ever coming to this world again through rebut linking

149
00:12:26,000 --> 00:12:28,000
Now

150
00:12:28,800 --> 00:12:33,120
That is paragraph 27 there we find the words to this world

151
00:12:34,880 --> 00:12:37,920
Now here to this world means to this

152
00:12:37,920 --> 00:12:44,960
Mm-hmm centuries world that means to the world of human beings and to the world of Lewis and lesser beings

153
00:12:45,440 --> 00:12:50,640
So there's different beginning and the two there to this world mean to this human world

154
00:12:51,360 --> 00:12:54,400
but here to this world means to this human

155
00:12:56,640 --> 00:12:58,640
and Lewis and lesser world

156
00:12:59,440 --> 00:13:01,440
because after

157
00:13:01,440 --> 00:13:09,280
After reaching the third stage of enlightenment and after becoming a non-returner

158
00:13:09,840 --> 00:13:12,240
He will be reborn in the Brahma world

159
00:13:13,680 --> 00:13:19,200
Not in the human beings or not in the lowest or lesser beings but will be reborn as a Brahma

160
00:13:19,840 --> 00:13:23,280
and in what I call a boat of

161
00:13:23,280 --> 00:13:29,840
Pure persons the five the five brands up in the

162
00:13:33,120 --> 00:13:37,840
Scale of 31 planes of existence. So he will be

163
00:13:38,160 --> 00:13:40,400
Rewind in this one of the pure apples

164
00:13:41,840 --> 00:13:47,920
So to this world here means not only to this human world but to this Kama Vajara war

165
00:13:47,920 --> 00:13:55,920
Now after reviewing it this way either while sitting in the same session or another occasion and so on

166
00:13:56,480 --> 00:13:58,480
and he becomes

167
00:14:00,000 --> 00:14:01,120
a

168
00:14:01,120 --> 00:14:04,800
an arachant so the food fruit the food

169
00:14:06,080 --> 00:14:10,000
Maga and the food fruition so after the food fruition he is

170
00:14:10,720 --> 00:14:12,720
he is called an eighth

171
00:14:12,720 --> 00:14:21,840
noble person or an arachant. So we see that there are eight noble persons

172
00:14:21,840 --> 00:14:28,880
it kinds of noble persons but in actual practice we can meet only four noble persons

173
00:14:28,880 --> 00:14:35,360
because the first the third the fifth and the seventh persons

174
00:14:35,360 --> 00:14:41,760
exist only for one thought moment so how can we catch that one thought moment?

175
00:14:43,920 --> 00:14:47,760
So in practice there are four noble persons

176
00:14:47,760 --> 00:14:50,320
and we say there are eight persons we've got they are different

177
00:14:52,880 --> 00:14:56,960
at the path conscious as moment and the fruition conscious as moment

178
00:14:56,960 --> 00:15:06,560
and then from Bhagavadhi to the states associated with the path etc

179
00:15:07,280 --> 00:15:14,320
Now states associated with the path means states at the moment of

180
00:15:14,320 --> 00:15:20,240
her consciousness are states accompanying the enlightenment and they are said to be they are

181
00:15:20,240 --> 00:15:31,760
associated with that is seven of these states and there are four foundations of mindfulness

182
00:15:32,880 --> 00:15:40,880
the four right and there was the four roads to power the five faculties the five powers

183
00:15:40,880 --> 00:15:48,560
the seven enlightenment figures and the noble eight-fold path so if you add them up all together

184
00:15:48,560 --> 00:15:58,560
you get that is seven but in reality there are not that is seven not that is seven states

185
00:15:58,560 --> 00:16:06,560
there are only harmony you remember a bit of my students

186
00:16:06,560 --> 00:16:20,240
14 yeah only 14 state actually because for example mindfulness city is one one mental factor

187
00:16:20,240 --> 00:16:27,680
but here it is described as four four foundations of mindfulness and the second the four

188
00:16:27,680 --> 00:16:37,840
and there was means and ever means just video one video is described as year four so in reality

189
00:16:37,840 --> 00:16:46,480
there are only 14 I think it is described somewhere now paragraph 24 foundation is because of

190
00:16:46,480 --> 00:16:50,560
establishment by going down into and so on this is description of the body what

191
00:16:50,560 --> 00:16:59,280
but other and so on so mindfulness itself as foundation is foundation of mindfulness so

192
00:16:59,280 --> 00:17:04,240
foundation of mindfulness means firm establishment of mindfulness or something like that

193
00:17:05,280 --> 00:17:10,240
it is a full kind because it occurs with respect to the body feeling consciousness and

194
00:17:10,240 --> 00:17:20,960
dema objects taking them as for fall painful and permanent and not so and because it accomplishes the function of a

195
00:17:20,960 --> 00:17:26,240
presenting perception of beauty pleasure permanent and so that is why full foundation of mindfulness

196
00:17:26,240 --> 00:17:36,160
is said so with the one mindfulness is here described as four because

197
00:17:36,160 --> 00:17:43,280
the it occurs with respect to the body feeling consciousness and vendor objects and also

198
00:17:45,600 --> 00:17:52,400
taking them as fall painful permanent and not so and at the same time apparently the perception of

199
00:17:52,400 --> 00:17:59,440
beauty pleasure prominence and so so there are four aspects that is why there are four mindfulness

200
00:17:59,440 --> 00:18:07,120
and then next one is four endeavors by it the endeavor does it is endeavor and so on these are

201
00:18:07,120 --> 00:18:17,520
the one explanation so one one video one effort is described as year four because it is different

202
00:18:17,520 --> 00:18:27,680
functions it accomplishes the functions of abandoning a risen and profitable things

203
00:18:27,680 --> 00:18:35,120
preventing the arising of those not yet a risen arousing and a risen profitable things

204
00:18:35,120 --> 00:18:40,640
and maintaining those already arisen now with regard to Akusala there are two aspects

205
00:18:40,640 --> 00:18:48,800
and with regard to Kuzala there are two and devouring to get rid of Kuzala

206
00:18:48,800 --> 00:19:03,520
with which I passed and then and devouring not to not to have Akusala for fresh Akusala a risen

207
00:19:04,800 --> 00:19:14,960
and then and devouring to to make Akusala not yet a risen arise and then and they were to develop

208
00:19:14,960 --> 00:19:22,560
what has arisen developed the Kuzala what has arisen so there are four aspects that why there are

209
00:19:22,560 --> 00:19:29,040
four kinds of right right and there were a right effort and then the six but a graph that the six is

210
00:19:29,040 --> 00:19:38,640
about 80 a.d. barda so they are called a barley a.d. barda the the roads to power or the basis of

211
00:19:38,640 --> 00:19:48,800
success and I think they are they are mentioned somewhere else right chapter 12 paragraph only four

212
00:19:49,840 --> 00:20:00,000
and what are before 80 baras it is full full for the middle of the paragraph yes see you desire

213
00:20:00,000 --> 00:20:16,800
and so on what are the other three see you effort consciousness and then right

214
00:20:16,800 --> 00:20:31,280
enquiring I mean enquiring means pioneer so therefore yes enquiring really means enquiring

215
00:20:31,280 --> 00:20:38,320
right understanding correctly not just enquiring not just investigating so there are these are the four

216
00:20:38,320 --> 00:20:50,800
roads to power these are super mundane only but because of the work if you think of things concentration

217
00:20:50,800 --> 00:20:55,360
of things metal unification then making you see with the dominant this is called concentration to

218
00:20:55,360 --> 00:21:02,080
see you etc they are also mundane as states acquire by the dominance of the etc respectively

219
00:21:02,080 --> 00:21:09,360
so at the moment of enlightenment they are super mundane but at the at the moment of practice

220
00:21:09,360 --> 00:21:15,920
of episode of meditation they are mundane and then faculties is in the sense of predominant in

221
00:21:15,920 --> 00:21:22,560
other words of overcoming because in these days as despite all these respective you overcome faithful

222
00:21:22,560 --> 00:21:35,600
faithlessness, idleness, negligence, destruction and confusion so there are five faculties what are

223
00:21:37,680 --> 00:21:47,840
effort mindfulness concentration and wisdom the opposite of faithlessness, idleness, negligence,

224
00:21:47,840 --> 00:21:54,000
destruction and confusion and the power is in the sense of unwavering because these states as

225
00:21:54,000 --> 00:22:00,720
powers are incapable of being overcome respectively by faithlessness and so on so they are strong

226
00:22:00,720 --> 00:22:07,040
strong factors they cannot be overcome by their opposites that is where they are called powers

227
00:22:08,240 --> 00:22:13,840
both are faithful as consisting in faith energy mindfulness concentration and understanding

228
00:22:13,840 --> 00:22:19,920
that is why five faculties and five powers is said the mindfulness investigation of states

229
00:22:19,920 --> 00:22:26,320
energy happiness tranquility concentration and equanimity factors in a being who is becoming

230
00:22:26,320 --> 00:22:31,680
enlightened are the seven enlightenment factor they are called seven factors of enlightenment

231
00:22:32,320 --> 00:22:39,040
and seven our mindfulness investigation of states that means actually Banya energy happiness

232
00:22:39,040 --> 00:22:52,160
PD tranquility poverty concentration samadhi and equanimity upika these are the seven

233
00:22:52,880 --> 00:22:58,080
called seven factors of enlightenment and then right view right thinking right speech right

234
00:22:58,080 --> 00:23:03,680
action right livelihood right effort right mindfulness and right concentration eight factors of noble

235
00:23:03,680 --> 00:23:13,280
eightfold fact so there are these 37 states by taking off enlightenment now in the prior stage

236
00:23:13,280 --> 00:23:19,520
when mundane insight is occurring they are found in a blue blue melody of consciousness as follows

237
00:23:19,520 --> 00:23:28,800
so when you're practicing vipasana meditation then these factors of this state arise in your mind

238
00:23:28,800 --> 00:23:36,080
but with different kinds of consciousness at one time you may be dwelling on the body contemplation

239
00:23:36,080 --> 00:23:43,360
I mean the mindfulness of the body at another time it may be on the feelings and so on so

240
00:23:44,160 --> 00:23:51,600
there is plurality of consciousnesses which go along with them but at the moment of

241
00:23:51,600 --> 00:24:03,440
thought consciousness it is said that they they all arise so they are found in a

242
00:24:03,440 --> 00:24:10,880
plurality of consciousness as follows and so on but at the time arising of anyone of these

243
00:24:10,880 --> 00:24:15,600
full kinds of path knowledge on the next page then all these states are found in their single

244
00:24:15,600 --> 00:24:22,880
consciousness so all these states arise together with that single path consciousness that is

245
00:24:22,880 --> 00:24:28,240
a difference so when they are mundane they they arise with different kinds of consciousness

246
00:24:28,240 --> 00:24:32,960
but when they are supermaning they arise with one type of consciousness which is

247
00:24:32,960 --> 00:24:39,040
a path consciousness in the moment of fruition that the three accepting the full full right

248
00:24:39,040 --> 00:24:46,800
and diverse and found now actually full right and diverse are also they also accompany

249
00:24:46,800 --> 00:24:53,360
fruition consciousness but their function is prominent at the moment of path consciousness

250
00:24:53,360 --> 00:25:00,480
that is why they are they are excluded but in fact path consciousness is also accompanied by

251
00:25:00,480 --> 00:25:08,800
media we really as a matter of fact which a company is many types of consciousness so but

252
00:25:08,800 --> 00:25:14,960
a fruition consciousness is also accompanied by media when these are following the single

253
00:25:14,960 --> 00:25:20,560
consciousness in this way it is the one kind of mindfulness full object is live on that is called

254
00:25:20,560 --> 00:25:31,360
the full violation of mindfulness and so on so when they arise with one consciousness at the

255
00:25:31,360 --> 00:25:37,600
moment of path consciousness then they do not take different objects at the time because

256
00:25:38,960 --> 00:25:44,720
path consciousness takes nibana as objects so they also take nibana as objects that is a difference

257
00:25:44,720 --> 00:25:51,200
and then nine in one way one in two ways then in four ways and in five ways and so on

258
00:25:52,720 --> 00:26:05,440
it is making you more familiar with this with these states so nine in one way that means nine states

259
00:26:05,440 --> 00:26:18,800
has only one function one name now there are 37 right so let's call them W7 names so nine has

260
00:26:18,800 --> 00:26:24,800
only one name something like that these nine are zero consciousness have been a speculative

261
00:26:24,800 --> 00:26:29,680
like whatever the thing in speech text and love you and they are found in one way as road

262
00:26:29,680 --> 00:26:35,120
to power consisting and zero etc since they do not belong to any other group so they are

263
00:26:35,120 --> 00:26:41,360
only one name then one in two ways three is found in two ways as a faculty and as power

264
00:26:42,480 --> 00:26:48,880
then in four ways and in five ways the meaning is that another one is found in four ways

265
00:26:48,880 --> 00:26:54,960
and another in five here in concentration is the one in four ways since it is a faculty

266
00:26:54,960 --> 00:27:02,000
a power and an enlightened effect factor and a path factor understanding is one in five ways

267
00:27:02,000 --> 00:27:09,360
since it is these four and also a road to power so these are just to make you more more

268
00:27:09,360 --> 00:27:16,320
acquainted with these and many states the eight ways and in nine ways too so meaning is that

269
00:27:16,320 --> 00:27:22,960
another one is found in eight ways and another in nine ways mindfulness is one in eight ways

270
00:27:22,960 --> 00:27:30,560
that means mindfulness has eight names among these 37 and that is a mindfulness power

271
00:27:30,560 --> 00:27:36,800
faculty power enlightenment factor path factor energy is one in nine ways since it is four

272
00:27:36,800 --> 00:27:45,200
right and there was taken as four and then road to power faculty power and enlightenment

273
00:27:45,200 --> 00:27:56,160
factor and path factor so and there was the most or the greatest number of names or functions

274
00:27:56,160 --> 00:28:12,960
and then emergence and coupling of powers now the resolution of emergence and coupling of powers

275
00:28:12,960 --> 00:28:24,000
mundane insight induces no emergence either from occurrence of the family internally

276
00:28:24,000 --> 00:28:30,000
because it does not cut off originating which is the act of causing occurrence of the science

277
00:28:30,000 --> 00:28:39,440
because it has a science object now emergence from occurrence and emergence from science

278
00:28:39,440 --> 00:28:45,920
so some emerge from occurrence some from science some from both so change of lineage

279
00:28:45,920 --> 00:28:51,200
knowledge does not induce emergence from occurrence because it does not cut off originating

280
00:28:52,400 --> 00:29:02,800
at that moment of change of lineage no mental development is eradicated so it does not induce

281
00:29:02,800 --> 00:29:10,640
emergence from occurrence if emergence from occurrence of mental development but it does induce

282
00:29:10,640 --> 00:29:18,560
emergence from the science that is because it does not take formations as objects

283
00:29:19,600 --> 00:29:26,720
scientists kind of information it takes nivana as objects so it emerges from the science but not

284
00:29:26,720 --> 00:29:32,720
from the occurrence hence it is that understanding of emergence and turning away from the external

285
00:29:32,720 --> 00:29:39,280
knowledge of change of lineage likewise the whole basic and so on these four kinds of

286
00:29:39,280 --> 00:29:45,120
path knowledge emerge from the science because we have the sinless as the object and also from

287
00:29:45,120 --> 00:29:51,520
occurrence we got the gut of originating so the image from both now that consciousness emerged

288
00:29:51,520 --> 00:29:57,760
from both both from science and from occurrence since it does not they do not take

289
00:29:58,960 --> 00:30:06,000
formations as objects then they emerge from science and since at the moment of

290
00:30:06,000 --> 00:30:14,400
path consciousness in mental development are eradicated they emerge from the occurrence of

291
00:30:14,400 --> 00:30:30,000
mental development so the the path emerge from both the both science and both science or

292
00:30:30,000 --> 00:30:39,840
current so the emerge from both hence it is said and so on so that now I want to point you

293
00:30:39,840 --> 00:30:51,920
one thing of pitch 797 about 1,2,3,4,5,6,7,8,9 lines you see they are right thinking in the sense

294
00:30:51,920 --> 00:31:00,240
of directing images from wrong thinking and so on now when we describe the noble hateful path

295
00:31:01,280 --> 00:31:08,560
during vipassana meditation or we say when you practice vipassana meditation

296
00:31:08,560 --> 00:31:18,320
all these eight factors are working say harmoniously in your mind how right right understanding

297
00:31:18,320 --> 00:31:27,440
right thought now you you try to concentrate on let us say on the breath now you need you need

298
00:31:28,640 --> 00:31:36,960
mental effort to keep your mind on the object so there is right right effort and then when you

299
00:31:36,960 --> 00:31:44,160
make effort your mind as it were hit the the object or hits the target something like that so

300
00:31:44,160 --> 00:31:51,600
that is mindfulness so mindfulness hits the object and then your mind sticks to the object for

301
00:31:51,600 --> 00:31:58,720
some time and that is right concentration so after right concentration you see the true nature of

302
00:31:58,720 --> 00:32:04,480
things you see the information that you see they are writing and disappearing and that is right

303
00:32:04,480 --> 00:32:12,800
understanding so how much do you get four and then the three right speech right action and right

304
00:32:12,800 --> 00:32:21,360
w will are already accomplished when you take recess before the practice of meditation so they are

305
00:32:21,360 --> 00:32:29,520
also said to be accomplished so seven then what about right thought now right or really means

306
00:32:29,520 --> 00:32:38,480
that the matter of fact that we just call it up initial application of mind so right thought

307
00:32:38,480 --> 00:32:46,480
means not thinking of thinking of nib on our thinking of thinking of formations but that

308
00:32:46,480 --> 00:32:51,920
mental factor that directs the mind to the object that takes the mind to the object so it could

309
00:32:51,920 --> 00:33:00,160
be intention not not intention it is something that takes the mind to the object

310
00:33:06,480 --> 00:33:12,320
when you when you shoot an arrow maybe the force of the the bow like that

311
00:33:12,320 --> 00:33:22,400
without without we talk about mind will not not take an object so we need we talk about I mean

312
00:33:22,400 --> 00:33:31,120
this initial application to take our mind to the object so applying the the mind to the object

313
00:33:31,120 --> 00:33:45,760
is affected by right right thought or the initial application so here we see that right thinking

314
00:33:45,760 --> 00:33:52,640
in the sense of directing in the sense of directing means in the sense of something like pushing

315
00:33:52,640 --> 00:34:02,320
the mind to the object taking the mind to the object if if initial application doesn't take the

316
00:34:02,320 --> 00:34:09,920
mind to the object then mind cannot see anything at all but that is why it is grouped with right

317
00:34:09,920 --> 00:34:16,160
understanding right thought and right understanding belong to the group of understanding

318
00:34:16,160 --> 00:34:24,640
and right effort right mentalness right livelihood belongs to the to the group of concentration

319
00:34:25,600 --> 00:34:34,640
and the other three belongs to the group of seala same modern purity so right thinking really

320
00:34:34,640 --> 00:34:46,480
means that mental factor mental state which takes the mind to the object which pushes the mind to

321
00:34:46,480 --> 00:34:56,560
the object so they are described anyway yeah and then coupling of power I think we'll have to

322
00:34:56,560 --> 00:35:10,800
do it next time coupling of power we may as well do it now at the time of developing the

323
00:35:10,800 --> 00:35:17,040
eight mundane attainments the serenity power is in excess so when when you practice

324
00:35:17,040 --> 00:35:26,240
Janus actually eight mundane attainment means eight Janus so when you develop eight Janus 70 power is

325
00:35:26,240 --> 00:35:32,720
in excess quite at the time of developing the contemplations of impermanence etc the inside power is

326
00:35:32,720 --> 00:35:38,160
in excess so when you practice we must allow my division then we must allow is in excess when

327
00:35:38,160 --> 00:35:44,400
you practice somatasmatize and access something else but at the noble path moment the occur couples

328
00:35:44,400 --> 00:35:49,600
together in the sense that neither one exceeds the other so at the moment of path consciousness

329
00:35:51,040 --> 00:36:03,120
both are present the somatasmatize and ribosana so at the moment of path moment the occur couples

330
00:36:03,120 --> 00:36:07,920
together in the sense that neither one exceeds the other so there is coupling of the power

331
00:36:07,920 --> 00:36:14,800
and the case of each one of the full kinds of knowledge according to the design and so on so serenity power and

332
00:36:14,800 --> 00:36:26,960
inside power so during the mundane stage one or the other is in excess but at the moment of

333
00:36:26,960 --> 00:36:38,240
mother they are equal and neither exist the other so no no one exists the other so serenity and

334
00:36:38,240 --> 00:36:42,480
inside of its single nation in the sense of emergence they have coupled together and neither

335
00:36:42,480 --> 00:36:47,760
exist together and it was that he develops serenity and inside couples together in the sense

336
00:36:47,760 --> 00:36:54,560
of emergence so that is emergence and coupling of the powers so they should be understood in

337
00:36:54,560 --> 00:37:01,440
this way and that the kinds of states that ought to be abandoned and so on and they are important

338
00:37:03,840 --> 00:37:13,760
according to this explanation we know what kinds of mental development are eradicated by which

339
00:37:14,400 --> 00:37:21,520
which states if or first states they can state their stateful state so after a person which

340
00:37:21,520 --> 00:37:30,000
reached the first stage they must have abandoned or he must have eradicated and these kinds

341
00:37:30,000 --> 00:37:36,720
of mental development and then after the second stage he must have eradicated another

342
00:37:37,520 --> 00:37:44,000
kinds of mental development and so on so they are described in the following barograph so we will

343
00:37:44,000 --> 00:37:52,720
do them next next week next week was going to be the last class but we might need another one

344
00:37:53,440 --> 00:38:08,320
this is the first place we have talked about it but anything going on on the fifth we might

345
00:38:08,320 --> 00:38:14,160
I think we might have something going on on the fifth which is the following

346
00:38:16,800 --> 00:38:22,240
but I'm not sure I'll know next week whether we have something here that way yeah but

347
00:38:23,440 --> 00:38:28,560
I'm going on the fifth so can we do it on the 12th or you still be in the tree

348
00:38:28,560 --> 00:38:38,560
after the retreat yeah so we might have another class that it must come down next week

349
00:38:38,560 --> 00:39:02,400
we don't want to stop short of the mic we don't want to stop short of the mic

