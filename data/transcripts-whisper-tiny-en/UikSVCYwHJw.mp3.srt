1
00:00:00,000 --> 00:00:05,440
Hey, welcome back to Ask a Monk. The next question comes from Kaws.

2
00:00:05,440 --> 00:00:13,360
Hello, I took those to become a labutist in the NKT-Cadampic tradition, and I sometimes

3
00:00:13,360 --> 00:00:18,480
join my Sangha. I sometimes neglect my meditation and faith practice. How can one keep

4
00:00:18,480 --> 00:00:23,960
themselves focused? Is it bad karma to break vows I have been given?

5
00:00:23,960 --> 00:00:30,960
Okay, first of all, I'm not really familiar with the NKT tradition. I think it's Tibetan.

6
00:00:30,960 --> 00:00:38,080
If I'm not mistaken, it sounds Tibetan. I've heard of it. I think that's, but it's nothing

7
00:00:38,080 --> 00:00:42,200
to do with me. But that's not really your question. Your question is about neglecting

8
00:00:42,200 --> 00:00:46,560
and how you can keep neglecting to practice and how to keep yourself focused.

9
00:00:46,560 --> 00:00:50,160
Well, I think the key is in, or one of the keys is in what you say by joining your

10
00:00:50,160 --> 00:00:55,760
Sangha. It's important to have a Sangha. The Buddha said, the whole of the holy life,

11
00:00:55,760 --> 00:01:01,360
all of the holy life, all of the religious or spiritual life is association with good people.

12
00:01:01,360 --> 00:01:07,960
So if you associate with people who are not meditating and if your friends and family

13
00:01:07,960 --> 00:01:13,560
and the people around you are not meditating, then they can only drag you down, especially

14
00:01:13,560 --> 00:01:20,360
when you're not developed and if you're not strong in your own right, on the other hand,

15
00:01:20,360 --> 00:01:36,360
if you're surrounded by ardent and devout meditators, then you can be expected to develop

16
00:01:36,360 --> 00:01:43,360
and progress in the meditation practice. So finding this community and it can also be

17
00:01:43,360 --> 00:01:51,840
through the internet, but the great thing about a real life Sangha is that it's scheduled

18
00:01:51,840 --> 00:01:59,400
and you can meet together at certain times. One option, obviously the best thing is real

19
00:01:59,400 --> 00:02:05,960
life, but if you're talking about in terms of the internet, then there's this thing called

20
00:02:05,960 --> 00:02:13,240
second life and it's this 3D virtual reality and it's really a silly place, but it can

21
00:02:13,240 --> 00:02:20,840
be used for good and there are a lot of non-governmental organizations using it for profitable

22
00:02:20,840 --> 00:02:24,560
things, for worthy things, and one of the things is the Buddha Center, this where I go

23
00:02:24,560 --> 00:02:28,280
and you can actually go and meditate there, there's also the Zen retreat if you're in

24
00:02:28,280 --> 00:02:33,800
the Zen meditation, there's also a Tibetan retreat center where you can go and what it means

25
00:02:33,800 --> 00:02:41,120
is that at certain times you can set your little character up and take it as an opportunity

26
00:02:41,120 --> 00:02:48,520
to listen to teachings and then do a meditation practice or have some contact with people,

27
00:02:48,520 --> 00:02:54,160
but obviously in real life, you're so much better off, so if you have the time, that's

28
00:02:54,160 --> 00:03:01,840
the best recommendation. When you don't have the option of having close contact with

29
00:03:01,840 --> 00:03:06,400
other meditators, then the best you can do is to set up your own routine. Make sure that

30
00:03:06,400 --> 00:03:11,600
you have some kind of routine so that the practices that you have you're doing on a regular

31
00:03:11,600 --> 00:03:21,400
basis, they're a part of your life. The important thing is, I think, not to make vows

32
00:03:21,400 --> 00:03:30,040
that are likely to be broken. I know that in certain traditions they have you do these

33
00:03:30,040 --> 00:03:39,600
very profound or difficult. Make these vows that are seemingly impossible and that's

34
00:03:39,600 --> 00:03:47,600
I understand it has something to do with the good intentions that come from it and there's

35
00:03:47,600 --> 00:03:50,840
no intention that you're actually able to fulfill the vow or whether you're able to

36
00:03:50,840 --> 00:04:04,280
fulfill it or not. I think if it's a vow that you break, I vow never to tell a lie and

37
00:04:04,280 --> 00:04:11,800
then you tell a lie. In one sense, it's just a stupid vow to take because obviously you're

38
00:04:11,800 --> 00:04:15,720
not at the level where you can keep it, where you can be assured that you're not going

39
00:04:15,720 --> 00:04:21,160
to do that. I will never have a smoker cigarette again or something like that. What you

40
00:04:21,160 --> 00:04:28,040
should be doing is striving for it or I undertake to train myself not to do these things

41
00:04:28,040 --> 00:04:33,840
and that's how they precepts are worded in the original Indian language. They're not

42
00:04:33,840 --> 00:04:42,400
I vow never to there. I undertake this training not to. If you do do it, you're not guilty

43
00:04:42,400 --> 00:04:50,840
of any broken promise, but it's a sign that you have to train yourself harder and it's

44
00:04:50,840 --> 00:04:54,440
a sign of the training that's going on. You're training yourself out of it, weaning yourself

45
00:04:54,440 --> 00:05:02,880
off of it. Vows, I don't know that they have much place unless there are these sort

46
00:05:02,880 --> 00:05:09,360
of vows that are ongoing. I know the big vow in Tibetan Buddhism or in some traditions of

47
00:05:09,360 --> 00:05:15,760
Buddhism, and sorry, not even just naming names, but probably in this tradition of yours,

48
00:05:15,760 --> 00:05:20,560
but in all forms of Buddhism, there are certain people who make a wish to become a fully

49
00:05:20,560 --> 00:05:26,080
enlightened Buddha. There are some people who make vows to do it saying that they won't

50
00:05:26,080 --> 00:05:30,960
become enlightened until they can do it for themselves and be a leader in their time and

51
00:05:30,960 --> 00:05:33,960
somewhere far in the future. There are other people even going to be extreme that they're

52
00:05:33,960 --> 00:05:38,280
not going to become enlightened until they can enlighten all beings or until all beings

53
00:05:38,280 --> 00:05:44,360
are ready to become enlightened. These sort of vows, you're safe in the fact that there's

54
00:05:44,360 --> 00:05:55,080
still time to complete it. There's still time to follow that vow. I don't know, I think

55
00:05:55,080 --> 00:06:01,280
it's probably unrealistic to think that anyone could or should wait that long, but to

56
00:06:01,280 --> 00:06:05,760
each their own, I would say your best bet is to do what you can as an individual because

57
00:06:05,760 --> 00:06:11,440
we're all just cogs in the wheel. We're all interdependent and we don't have any special

58
00:06:11,440 --> 00:06:22,440
power to be able to control the outcome. This idea of making a vow to do something, it's

59
00:06:22,440 --> 00:06:27,320
based on delusion, the delusion that you have some power to change reality when in fact

60
00:06:27,320 --> 00:06:33,000
you're just a conglomerate of states that are arising and ceasing, and you have the potential

61
00:06:33,000 --> 00:06:37,080
to move in a certain direction, but that doesn't mean you have the potential to be the

62
00:06:37,080 --> 00:06:42,240
savior of all beings, for instance. Of course, there are differences of opinion in that,

63
00:06:42,240 --> 00:06:49,400
but I would say your best bet is not to make vows, but to look at the precepts that you

64
00:06:49,400 --> 00:06:54,080
take as being training rules. I'm training to abstain. I'm training to be free from the

65
00:06:54,080 --> 00:07:01,800
desire to do these things. I'm training to free myself from this evil.

66
00:07:01,800 --> 00:07:08,920
When you break vows, I think you've done a bad thing in making the vow, in the sense

67
00:07:08,920 --> 00:07:13,520
that it's a vow that you couldn't keep. Yeah, I think you've broken your promise, but

68
00:07:13,520 --> 00:07:17,800
I wouldn't worry too much about it because there are far worse things you could do.

69
00:07:17,800 --> 00:07:24,280
If you told a lie, you said to someone, I didn't break the vow. When in fact you broke

70
00:07:24,280 --> 00:07:31,040
it, that's far worse than breaking it and admitting that you broke it. It's really not

71
00:07:31,040 --> 00:07:35,560
that big of a deal. It's not a good thing, and it's based on an English in mind, in

72
00:07:35,560 --> 00:07:41,320
both senses, negligent in making a vow that you couldn't keep, and negligent in breaking

73
00:07:41,320 --> 00:07:46,200
it, breaking a vow that you promised yourself to keep. So I wouldn't do that. I wouldn't

74
00:07:46,200 --> 00:07:53,040
make those kind of vows, as I said, but your question is whether it's karma, bad karma.

75
00:07:53,040 --> 00:07:57,480
I mean, honestly, bad karma is made every moment that you're not mindful. We're full

76
00:07:57,480 --> 00:08:03,040
of bad karma. Every moment, karma means action. Everything that you do that's unmindful.

77
00:08:03,040 --> 00:08:07,360
Every time you judge and react to something, that's bad karma. I mean, it's going on

78
00:08:07,360 --> 00:08:10,840
all the time. You don't have to worry too much about it. All that means is that it's

79
00:08:10,840 --> 00:08:15,120
creating stress in your mind. It's creating suffering for you. So you're worrying about

80
00:08:15,120 --> 00:08:23,920
it is actually far worse karma than the actual act itself. But yeah, so two things I would

81
00:08:23,920 --> 00:08:28,280
say, find a community and stick with them, and also set up a schedule for yourself.

82
00:08:28,280 --> 00:08:33,560
Try to make sure that you're doing something. The third thing is not to get too hard on

83
00:08:33,560 --> 00:08:40,040
yourself. When you do fall off the wagon, or you don't live up to your own expectations,

84
00:08:40,040 --> 00:08:46,240
well, sometimes your expectations have to come halfway. You do have to improve, but you also

85
00:08:46,240 --> 00:08:51,280
maybe have to reduce your expectations, not to the level of your ability, but somewhere

86
00:08:51,280 --> 00:08:55,720
halfway so that you're always improving yourself and that you're not content with the

87
00:08:55,720 --> 00:09:01,960
way you are. But you're not setting unreal expectations like these vows. You're saying,

88
00:09:01,960 --> 00:09:09,040
as long as I'm improving, a good rate, and that's fine. If I'm not able to improve

89
00:09:09,040 --> 00:09:20,280
with the rate that I expected, then my expectations are too high. You can adjust your practice

90
00:09:20,280 --> 00:09:27,120
to make you a more able to reach your expectations. But you should also be willing to look

91
00:09:27,120 --> 00:09:34,480
at your expectations and see that probably they're a little bit high. We really have to

92
00:09:34,480 --> 00:09:41,440
go step-by-step and quite slowly. Another thing you could do, of course, which you probably

93
00:09:41,440 --> 00:09:45,320
already aware of is believe your life behind and go and become a monk and live in the

94
00:09:45,320 --> 00:09:51,840
forest. But imagine if that was on your plate, you would have done it already, or maybe

95
00:09:51,840 --> 00:09:58,160
it's in your future, something to think about that. If you really want to be focused,

96
00:09:58,160 --> 00:10:04,920
this is actually important. You can't do it realistically in the world, because living

97
00:10:04,920 --> 00:10:09,160
in the world means doing work and doing things that are unrelated to the path, and they

98
00:10:09,160 --> 00:10:15,680
get you off track inevitably. There's nothing you can do to that. If you want to have perfect

99
00:10:15,680 --> 00:10:22,360
practice or even very good practice, you really need a suitable environment, and that

100
00:10:22,360 --> 00:10:29,480
requires a monastic core or meditative lifestyle, which is very difficult to find in the

101
00:10:29,480 --> 00:10:36,960
world unless you have some financial stability or some resources that allow you to put

102
00:10:36,960 --> 00:10:44,720
aside work in favor of a meditative life. I hope that helps. Good luck. If you're interested

103
00:10:44,720 --> 00:10:50,600
in the practice that I teach, you're welcome to follow it as well. Again, I'm not really

104
00:10:50,600 --> 00:11:20,440
clear on the vowels and traditions of the practice that you follow, but all the best.

