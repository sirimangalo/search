1
00:00:00,000 --> 00:00:04,720
and is a Buddha able to with physical pain.

2
00:00:04,720 --> 00:00:08,440
I see not most of the art of letting go.

3
00:00:08,440 --> 00:00:10,760
What's the origin of the physical pain?

4
00:00:10,760 --> 00:00:13,040
Is thought the origin of physical pain?

5
00:00:17,320 --> 00:00:18,920
What do you mean by withstand?

6
00:00:18,920 --> 00:00:22,080
That's, I think, the important question there.

7
00:00:22,080 --> 00:00:24,800
You mean not experiences of the thing?

8
00:00:24,800 --> 00:00:26,400
No, it doesn't mean that's right.

9
00:00:26,400 --> 00:00:34,640
I'm going to go away with your literally asking her.

10
00:00:34,640 --> 00:00:38,040
And of course, yes, the Buddha is able to master,

11
00:00:38,040 --> 00:00:39,320
to withstand physical pain.

12
00:00:39,320 --> 00:00:44,880
And yes, even because yes, he is master in the art of letting go.

13
00:00:44,880 --> 00:00:48,160
But it kind of sounds like he might be asking

14
00:00:48,160 --> 00:00:50,000
whether he doesn't have physical pain.

15
00:00:50,000 --> 00:00:51,200
And that's not true.

16
00:00:51,200 --> 00:00:54,760
There are, I think, five things that a Buddha can.

17
00:00:54,760 --> 00:01:01,720
The Buddha is unable to prevent death as one.

18
00:01:01,720 --> 00:01:04,360
The activities of the body you're needing

19
00:01:04,360 --> 00:01:06,240
in death for getting this one.

20
00:01:06,240 --> 00:01:09,360
And physical pain, I'm not sure if it's in the list,

21
00:01:09,360 --> 00:01:10,960
but physical pain is certainly something

22
00:01:10,960 --> 00:01:14,880
that, what's part of the physical process?

23
00:01:14,880 --> 00:01:17,680
Buddha cannot stop the physical process.

24
00:01:17,680 --> 00:01:24,360
And now, to some extent, the Buddha can suppress physical

25
00:01:24,360 --> 00:01:26,600
ailments, like there is the case where

26
00:01:26,600 --> 00:01:30,720
one of the Buddha was getting close to passing away.

27
00:01:30,720 --> 00:01:32,840
He got very sick.

28
00:01:32,840 --> 00:01:37,600
But he suppressed the sickness because he

29
00:01:37,600 --> 00:01:39,200
needed his time to work to do not,

30
00:01:39,200 --> 00:01:42,520
because he didn't want to feel pain because the sickness

31
00:01:42,520 --> 00:01:45,360
would get in the way of him finishing his task.

32
00:01:45,360 --> 00:01:51,320
And so he suppressed it in order to finish his teaching.

33
00:01:51,320 --> 00:01:53,160
There's other cases where he didn't do that,

34
00:01:53,160 --> 00:01:56,880
where David Windy went out and dropped a rock on him.

35
00:01:56,880 --> 00:01:59,640
And he laid down and was mindful.

36
00:01:59,640 --> 00:02:03,000
And Givakad has talked to Givakad came in

37
00:02:03,000 --> 00:02:09,360
and put some medicine on his foot and then left him.

38
00:02:09,360 --> 00:02:15,440
And the Buddha, or the Buddha was in great pain.

39
00:02:15,440 --> 00:02:17,760
I think the Buddha was in great pain.

40
00:02:17,760 --> 00:02:19,320
And Givakad was amazed that the Buddha

41
00:02:19,320 --> 00:02:23,760
was able to withstand the pain.

42
00:02:23,760 --> 00:02:32,080
So anything to read, if you can go and find that.

43
00:02:32,080 --> 00:02:34,080
But then he forgot, then he had to leave.

44
00:02:34,080 --> 00:02:36,280
He said, I'll be back in an hour or something

45
00:02:36,280 --> 00:02:42,800
to take the medicine off the leg.

46
00:02:42,800 --> 00:02:45,480
And he went into the city to perform some duties.

47
00:02:45,480 --> 00:02:48,840
And he was a little bit late and so on his way out.

48
00:02:48,840 --> 00:02:51,000
As he was getting to the city gate,

49
00:02:51,000 --> 00:02:53,880
they closed the city gate before he got to it.

50
00:02:53,880 --> 00:02:56,120
And so he wasn't able to leave the city.

51
00:02:56,120 --> 00:02:57,720
And the Buddha was lying there.

52
00:02:57,720 --> 00:03:00,760
And with this medicine, Givakad was really

53
00:03:00,760 --> 00:03:02,480
worried because he didn't.

54
00:03:02,480 --> 00:03:06,080
If the medicine didn't come off, there would be problems.

55
00:03:06,080 --> 00:03:08,200
It was too strong or something like that.

56
00:03:08,200 --> 00:03:09,920
Had to be taken off.

57
00:03:09,920 --> 00:03:11,840
And so in the morning he rushed to the Buddha,

58
00:03:11,840 --> 00:03:15,360
by that time the Buddha had understood the situation

59
00:03:15,360 --> 00:03:17,920
and told Anand that to take the medicine off.

60
00:03:17,920 --> 00:03:20,560
And Givakad wanted to be able to come back today,

61
00:03:20,560 --> 00:03:24,440
take the medicine off to Anand that took it off.

62
00:03:24,440 --> 00:03:26,200
And then Givakad came back.

63
00:03:26,200 --> 00:03:31,560
So the point being is, according to that passage,

64
00:03:31,560 --> 00:03:36,160
we understand that the Buddha didn't suppress the pain

65
00:03:36,160 --> 00:03:39,760
or wasn't interested at all and getting rid of the pain.

66
00:03:39,760 --> 00:03:44,600
But on that, Givakad put this medicine on it

67
00:03:44,600 --> 00:03:51,360
to heal it or do whatever.

68
00:03:51,360 --> 00:03:54,360
And then the last part is, taught the origin of physical pain.

69
00:03:54,360 --> 00:04:00,200
Well, not directly, but indirectly.

70
00:04:00,200 --> 00:04:03,880
Because without thought, you can't be born far.

71
00:04:03,880 --> 00:04:05,600
Maybe it's not exactly where you were.

72
00:04:05,600 --> 00:04:08,400
But without intention, you can't be born

73
00:04:08,400 --> 00:04:10,520
and therefore cannot have pain.

74
00:04:10,520 --> 00:04:16,920
But pain is caused by physical, directly caused

75
00:04:16,920 --> 00:04:17,720
by the physical.

76
00:04:23,080 --> 00:04:27,800
Of course, it only gives a feeling of pain.

77
00:04:33,520 --> 00:04:35,640
It only gives a feeling of pain in mind.

78
00:04:35,640 --> 00:04:40,320
It may deny it's a mental, a physical.

79
00:04:40,320 --> 00:04:42,200
So without the mind, you don't feel pain.

80
00:04:42,200 --> 00:04:45,440
The body, if there's only the body, there's no pain in the body.

81
00:04:45,440 --> 00:04:47,960
There's no feeling of pain.

82
00:04:47,960 --> 00:04:50,760
It might be physical pain.

83
00:04:50,760 --> 00:05:17,840
But without the mind present, there will be no feeling of it.

