1
00:00:00,000 --> 00:00:07,000
Please give an overview on the state of the stream winner.

2
00:00:07,000 --> 00:00:10,000
Stream winner.

3
00:00:10,000 --> 00:00:14,500
The word is Sotapana.

4
00:00:14,500 --> 00:00:17,500
Sotapana.

5
00:00:17,500 --> 00:00:27,000
Sotapana means stream and appana means attained.

6
00:00:27,000 --> 00:00:31,000
So it means one who has attained this stream.

7
00:00:31,000 --> 00:00:35,000
The word Sotapana, generally, as used in the Sutas,

8
00:00:35,000 --> 00:00:38,000
refers to one specific thing.

9
00:00:38,000 --> 00:00:43,000
In the commentaries, there is actually another type of Sotapana.

10
00:00:43,000 --> 00:00:54,000
And then the commentary, like addition, helps us to understand the Sutta definition as well,

11
00:00:54,000 --> 00:00:58,000
because it helps us understand the way that word is used in different contexts.

12
00:00:58,000 --> 00:01:00,000
In the Sutas, it's only used in one way.

13
00:01:00,000 --> 00:01:07,000
It's used to refer to the person who has had a realization of the formable truths,

14
00:01:07,000 --> 00:01:14,000
has come to realize Nibana for the first time.

15
00:01:14,000 --> 00:01:21,000
They have entered into a kind of a cessation, which we call Palasamapati,

16
00:01:21,000 --> 00:01:30,000
where they have realized that all things cease, because they've seen the cessation of all phenomena without remainder.

17
00:01:30,000 --> 00:01:40,000
They've come to regard all phenomena that arise as impermanent, unsatisfying, and uncontrollable.

18
00:01:40,000 --> 00:01:45,000
And that realization is only a temporary realization.

19
00:01:45,000 --> 00:01:48,000
They may still later on cling to things.

20
00:01:48,000 --> 00:01:54,000
But because they've had that realization, at the moment of that realization, the mind goes out, the mind ceases.

21
00:01:54,000 --> 00:01:58,000
And there is this experience of cessation.

22
00:01:58,000 --> 00:02:03,000
It can last for a brief moment, it can last for a minute, it can last for an hour.

23
00:02:03,000 --> 00:02:13,000
It can be cultivated to last, cultivated in some sense of determining to enter into it for up to seven days, according to the texts.

24
00:02:13,000 --> 00:02:19,000
I had one student who claimed to be able to enter into it for 24 hours.

25
00:02:19,000 --> 00:02:23,000
That's what she said, believe it or not.

26
00:02:23,000 --> 00:02:26,000
But she certainly did seem to enter into it quite frequently.

27
00:02:26,000 --> 00:02:36,000
We'd be in the car, and once we were in one of these red trucks that they have in Chiang Mai,

28
00:02:36,000 --> 00:02:41,000
and we're sitting there, and she's not all rocking in the bumps.

29
00:02:41,000 --> 00:02:48,000
And she's hitting her head off the side of the truck and not feeling it, and not being jolted at all.

30
00:02:48,000 --> 00:02:51,000
And as soon as the truck stopped, she opened her eyes.

31
00:02:51,000 --> 00:02:54,000
Something like that, or else it was, I can't remember what it was.

32
00:02:54,000 --> 00:02:59,000
It was like, we're all trying to get out of the car or the truck, and she's still sitting there.

33
00:02:59,000 --> 00:03:06,000
There was another time where we were sitting and chanting, and we'd do chanting in the evening, and we finished chanting.

34
00:03:06,000 --> 00:03:08,000
We all got up, and she was still sitting there.

35
00:03:08,000 --> 00:03:11,000
Because in the middle of chanting, we do sitting meditation.

36
00:03:11,000 --> 00:03:13,000
After sitting meditation, she didn't stop.

37
00:03:13,000 --> 00:03:15,000
She didn't do the rest of the chanting.

38
00:03:15,000 --> 00:03:16,000
We all got up.

39
00:03:16,000 --> 00:03:20,000
And I sat there from my room looking through the hallway at her sitting in the hall for a while.

40
00:03:20,000 --> 00:03:22,000
And after about five minutes, she got up.

41
00:03:22,000 --> 00:03:26,000
She opened her eyes, got up, and walked away.

42
00:03:26,000 --> 00:03:33,000
So this experience, for the first time, where the meditator enters into cessation,

43
00:03:33,000 --> 00:03:40,000
and is able to see the difference between what one's thought was happiness.

44
00:03:40,000 --> 00:03:50,000
And this cessation of suffering, and realizes that all things that arise are unsatisfying,

45
00:03:50,000 --> 00:03:54,000
are not happiness, are not beneficial.

46
00:03:54,000 --> 00:04:00,000
What one loses one's interest in them, loses the attachment to them.

47
00:04:00,000 --> 00:04:06,000
The first realization of Nimbana doesn't free one from attachment to things.

48
00:04:06,000 --> 00:04:09,000
One will become less attached to things through this realization,

49
00:04:09,000 --> 00:04:11,000
because one now has something to compare it to.

50
00:04:11,000 --> 00:04:16,000
Before the only thing one could think of is happy, where a risen experience is.

51
00:04:16,000 --> 00:04:22,000
So maybe ice cream is happiness, or chocolate is happiness, or sex is happiness, or drugs are happiness,

52
00:04:22,000 --> 00:04:25,000
or heaviness is happiness, or so on.

53
00:04:25,000 --> 00:04:27,000
This is what one thought was happiness.

54
00:04:27,000 --> 00:04:31,000
Sometimes after them, finds no satisfaction, because everything is changing,

55
00:04:31,000 --> 00:04:35,000
and everything is impermanent, and finds that all one is gaining through this is more and more greed,

56
00:04:35,000 --> 00:04:39,000
more and more attachment, more and more addiction.

57
00:04:39,000 --> 00:04:44,000
And when one realizes Nimbana, for the first time,

58
00:04:44,000 --> 00:04:48,000
one has realized something that is totally free from any attachment,

59
00:04:48,000 --> 00:04:55,000
has no building up of craving, has no stress, no agitation of mind,

60
00:04:55,000 --> 00:04:57,000
it's pure peace.

61
00:04:57,000 --> 00:05:01,000
The next moment when one arises from this first experience

62
00:05:01,000 --> 00:05:05,000
is the happiest moment of the person's life.

63
00:05:05,000 --> 00:05:09,000
It will, of one's career and some sorrow, really,

64
00:05:09,000 --> 00:05:13,000
because before that one had never, in all of the rounds of some sorrow,

65
00:05:13,000 --> 00:05:15,000
never come in contact with the Buddhist teaching,

66
00:05:15,000 --> 00:05:21,000
never gotten to this point, where one practiced intensively this teaching,

67
00:05:21,000 --> 00:05:26,000
and had never before experienced the state,

68
00:05:26,000 --> 00:05:29,000
where all things disappear, where there's no seeing, no hearing,

69
00:05:29,000 --> 00:05:32,000
no smelling, no tasting, no feeling, no thinking.

70
00:05:32,000 --> 00:05:36,000
So regardless of how, from people who hadn't realized

71
00:05:36,000 --> 00:05:41,000
that this would sound more terrifying, or horrible,

72
00:05:41,000 --> 00:05:46,000
or undistotally undesirable, the cessation of seeing, hearing,

73
00:05:46,000 --> 00:05:49,000
smelling, tasting, feeling, and thinking.

74
00:05:49,000 --> 00:05:52,000
Because it's devoid of all of those things,

75
00:05:52,000 --> 00:05:55,000
which one thought would be a cause of happiness.

76
00:05:55,000 --> 00:06:00,000
When the Sotapana arises from the attainment of Sotapana,

77
00:06:00,000 --> 00:06:05,000
of Sotapatimanga, Sotapatimala,

78
00:06:05,000 --> 00:06:10,000
they have changed this opinion about reality.

79
00:06:10,000 --> 00:06:15,000
And they have changed the idea that anything might be permanent,

80
00:06:15,000 --> 00:06:17,000
or anything might be lasting.

81
00:06:17,000 --> 00:06:20,000
They lose three things as a result of this.

82
00:06:20,000 --> 00:06:23,000
As a result of changing their idea of what is happiness

83
00:06:23,000 --> 00:06:26,000
and realizing the true peace and true happiness,

84
00:06:26,000 --> 00:06:29,000
which leaves them with a clear and pure mind.

85
00:06:29,000 --> 00:06:34,000
A person who has entered into it can be in a state of bliss for days,

86
00:06:34,000 --> 00:06:40,000
just on cloud nine for a great period of time,

87
00:06:40,000 --> 00:06:42,000
because of the change in their existence.

88
00:06:42,000 --> 00:06:46,000
Talking to some people who seem to have gotten this experience,

89
00:06:46,000 --> 00:06:48,000
were in no position to judge,

90
00:06:48,000 --> 00:06:51,000
who their whole character changes.

91
00:06:51,000 --> 00:06:55,000
And they're just in complete bliss and contentment

92
00:06:55,000 --> 00:06:59,000
for a great number of days until the defilements that are left start coming back

93
00:06:59,000 --> 00:07:03,000
and start affecting their mind again.

94
00:07:07,000 --> 00:07:12,000
But at that moment of coming back, they're free of three things.

95
00:07:12,000 --> 00:07:14,000
They may still have defilements and the defilements

96
00:07:14,000 --> 00:07:16,000
and the defilements will slowly come back,

97
00:07:16,000 --> 00:07:22,000
but they know something new that is irreversible.

98
00:07:22,000 --> 00:07:24,000
You could think of it like you have this damn.

99
00:07:24,000 --> 00:07:28,000
And as long as the damn is fully intact,

100
00:07:28,000 --> 00:07:32,000
you know that it could last for a great period of time.

101
00:07:32,000 --> 00:07:35,000
But as soon as you see the first crack in the damn,

102
00:07:35,000 --> 00:07:37,000
this damn can never be fixed,

103
00:07:37,000 --> 00:07:39,000
and eventually the damn is going to break,

104
00:07:39,000 --> 00:07:41,000
and the water is going to be released.

105
00:07:41,000 --> 00:07:44,000
And the same way the Sotupana has entered into some kind of...

106
00:07:44,000 --> 00:07:46,000
has entered into the stream.

107
00:07:46,000 --> 00:07:49,000
This is what the meaning of the word Sotah means.

108
00:07:49,000 --> 00:07:51,000
They've changed something about themselves,

109
00:07:51,000 --> 00:07:55,000
and they have entered into a non-static state.

110
00:07:55,000 --> 00:07:58,000
This state is not something that is going to stay still.

111
00:07:58,000 --> 00:08:02,000
They're headed upstream, you might say,

112
00:08:02,000 --> 00:08:06,000
or they're headed in the direction of Nimana.

113
00:08:06,000 --> 00:08:08,000
It's kind of like the damn is cracked,

114
00:08:08,000 --> 00:08:12,000
and it's only a matter of time before it bursts loose.

115
00:08:12,000 --> 00:08:14,000
So they lose three things.

116
00:08:14,000 --> 00:08:18,000
They lose wrong view, wrong view of self-sakya-diti.

117
00:08:18,000 --> 00:08:21,000
They lose any idea that there is a permanent soul,

118
00:08:21,000 --> 00:08:26,000
because this is the profound thing about the cessation of all experience.

119
00:08:26,000 --> 00:08:29,000
You realize that all that exists is experience.

120
00:08:29,000 --> 00:08:31,000
Without experience, there's nothing.

121
00:08:31,000 --> 00:08:35,000
There is Nimana, there is the cessation,

122
00:08:35,000 --> 00:08:38,000
which we call Nimana.

123
00:08:38,000 --> 00:08:47,000
And so as a result, this idea of a self has no place,

124
00:08:47,000 --> 00:08:51,000
has no meaning for them anymore.

125
00:08:51,000 --> 00:08:54,000
They lose the attachment to rights and rituals,

126
00:08:54,000 --> 00:08:59,000
or practice that is useless.

127
00:08:59,000 --> 00:09:02,000
They lose the idea that practice that is unbeneficial

128
00:09:02,000 --> 00:09:06,000
or of no benefit in leading towards the freedom from suffering,

129
00:09:06,000 --> 00:09:08,000
thinking that it is a benefit.

130
00:09:08,000 --> 00:09:10,000
They now know what is the right path.

131
00:09:10,000 --> 00:09:13,000
So they're able to see that what is useless is useless.

132
00:09:13,000 --> 00:09:15,000
They can see that doing rights and rituals

133
00:09:15,000 --> 00:09:20,000
and performing ceremonies that it's not the way to Nimana.

134
00:09:20,000 --> 00:09:24,000
It doesn't mean that they will stop and refuse to perform these things,

135
00:09:24,000 --> 00:09:27,000
but when they perform them, they'll know that this is meaningless

136
00:09:27,000 --> 00:09:31,000
or it's only meaningful because of the state of mind of the person.

137
00:09:31,000 --> 00:09:34,000
They come to see that it's the state of the mind of one's intention

138
00:09:34,000 --> 00:09:36,000
and one's clarity of mind that's most important.

139
00:09:36,000 --> 00:09:39,000
So when they do chanting, they'll try to use it

140
00:09:39,000 --> 00:09:44,000
as an opportunity to reflect on the wholesome qualities of the Buddha or so on.

141
00:09:44,000 --> 00:09:47,000
Or as they progress, they'll use the chanting

142
00:09:47,000 --> 00:09:50,000
as a time to be aware of their lips moving

143
00:09:50,000 --> 00:09:54,000
or of the feelings in the body and so on.

144
00:09:54,000 --> 00:09:57,000
They won't have the idea that chanting in and of itself

145
00:09:57,000 --> 00:09:59,000
has some power that's going to lead them to enlightenment

146
00:09:59,000 --> 00:10:02,000
or freedom from suffering.

147
00:10:02,000 --> 00:10:05,000
And so on, they don't have the idea that keeping this rule

148
00:10:05,000 --> 00:10:06,000
or that rule is important.

149
00:10:06,000 --> 00:10:09,000
They don't have the idea that, for example,

150
00:10:09,000 --> 00:10:13,000
not touching money, for example, is necessary to become enlightened

151
00:10:13,000 --> 00:10:15,000
so they don't hold on to the precepts as tightly.

152
00:10:15,000 --> 00:10:17,000
If they break a precept, they're not worried

153
00:10:17,000 --> 00:10:19,000
or feeling guilty about it.

154
00:10:19,000 --> 00:10:23,000
They just reprimand themselves and say that was wrong

155
00:10:23,000 --> 00:10:26,000
and they make an effort to not do it again and they confess it.

156
00:10:26,000 --> 00:10:28,000
But they don't feel upset about it.

157
00:10:28,000 --> 00:10:30,000
They don't think that, oh, now I'm going to go to hell

158
00:10:30,000 --> 00:10:32,000
because it's a concept.

159
00:10:32,000 --> 00:10:34,000
It's a convention.

160
00:10:34,000 --> 00:10:40,000
And so they don't cling to rules as well or morality.

161
00:10:40,000 --> 00:10:44,000
They understand it in terms of the mind state.

162
00:10:44,000 --> 00:10:46,000
So they understand that killing is always wrong

163
00:10:46,000 --> 00:10:47,000
or stealing is always wrong.

164
00:10:47,000 --> 00:10:50,000
But they understand that certain things are only concepts

165
00:10:50,000 --> 00:10:53,000
not eating after 12 o'clock if they eat in the afternoon.

166
00:10:53,000 --> 00:10:55,000
As a monk, for example, they won't feel guilty,

167
00:10:55,000 --> 00:10:57,000
but they'll maybe say to themselves,

168
00:10:57,000 --> 00:10:59,000
you've got to be more mindful and so on.

169
00:10:59,000 --> 00:11:02,000
And they will make a note and try to correct their behavior.

170
00:11:02,000 --> 00:11:04,000
That's it.

171
00:11:04,000 --> 00:11:06,000
The third thing is they lose doubt.

172
00:11:06,000 --> 00:11:09,000
So they lose doubt about the Buddha, doubt about the Buddha's teaching

173
00:11:09,000 --> 00:11:15,000
and doubt about what makes a person a enlightened disciple of the Buddha.

174
00:11:15,000 --> 00:11:18,000
So they understand what are the meaning of these three things

175
00:11:18,000 --> 00:11:22,000
and they understand that what the Buddha taught is the truth

176
00:11:22,000 --> 00:11:27,000
because they've realized it for themselves.

177
00:11:32,000 --> 00:11:35,000
So that's what the sutta is called being a sotepan.

178
00:11:35,000 --> 00:11:39,000
It's just one more thing because the Buddha goes and mentions

179
00:11:39,000 --> 00:11:41,000
this other kind of stream enter.

180
00:11:41,000 --> 00:11:44,000
It's called the jula sotepan, sotepan.

181
00:11:44,000 --> 00:11:48,000
The jula sotepan is someone who realizes the second stage of knowledge,

182
00:11:48,000 --> 00:11:50,000
altogether there are 16 stages of knowledge.

183
00:11:50,000 --> 00:11:53,000
When a person realizes the second stage of knowledge,

184
00:11:53,000 --> 00:11:55,000
it means they have an understanding of karma.

185
00:11:55,000 --> 00:11:59,000
How this works is in the beginning they see the body in the mind functioning.

186
00:11:59,000 --> 00:12:04,000
They're able to see the stomach rising and that's a phenomenon of body

187
00:12:04,000 --> 00:12:06,000
and they see the mind that goes out to the stomach

188
00:12:06,000 --> 00:12:08,000
and is aware of the stomach rising.

189
00:12:08,000 --> 00:12:12,000
So only when there is the stomach rising and the mind aware of it

190
00:12:12,000 --> 00:12:14,000
is there the experience of the stomach rising.

191
00:12:14,000 --> 00:12:17,000
If the mind is somewhere else or not thinking about the stomach,

192
00:12:17,000 --> 00:12:21,000
there's also no experience. One sees that there's the body in the mind.

193
00:12:21,000 --> 00:12:23,000
That's the first stage of knowledge.

194
00:12:23,000 --> 00:12:26,000
The second stage of knowledge and this is not intellectual

195
00:12:26,000 --> 00:12:30,000
is one begins to see how the body and the mind work together.

196
00:12:30,000 --> 00:12:32,000
So one wants to stand up and then there's the standing.

197
00:12:32,000 --> 00:12:34,000
One wants to sit down and then there's the sitting.

198
00:12:34,000 --> 00:12:38,000
There arises pain in the body and as a result there arises disliking.

199
00:12:38,000 --> 00:12:42,000
There arises pleasure in the body and as a result there arises liking.

200
00:12:42,000 --> 00:12:46,000
Because of the disliking and because of the liking there arises

201
00:12:46,000 --> 00:12:52,000
clinging in there arises partiality in there arises becoming and acting

202
00:12:52,000 --> 00:12:55,000
and stress and as a result suffering.

203
00:12:55,000 --> 00:12:58,000
And so as a result they start to see the truth of karma.

204
00:12:58,000 --> 00:13:02,000
A person who gets to this stage of knowledge has an understanding

205
00:13:02,000 --> 00:13:04,000
non-intellectual understanding of karma.

206
00:13:04,000 --> 00:13:10,000
They understand how defilements really inevitably lead to stress and suffering.

207
00:13:10,000 --> 00:13:14,000
And so as a result they have entered what we call the little stream

208
00:13:14,000 --> 00:13:19,000
which means whereas a Sotapana, another thing about the Sotapana

209
00:13:19,000 --> 00:13:24,000
is they're not liable to be born in states of whoa

210
00:13:24,000 --> 00:13:26,000
or states of suffering.

211
00:13:26,000 --> 00:13:28,000
They're not liable to be born in hell.

212
00:13:28,000 --> 00:13:30,000
They're not liable to be born as an animal.

213
00:13:30,000 --> 00:13:32,000
They're not liable to be born as a ghost.

214
00:13:32,000 --> 00:13:36,000
They can only be born as a human or an angel or a god.

215
00:13:36,000 --> 00:13:41,000
And eventually some of them will in one lifetime become enlightened.

216
00:13:41,000 --> 00:13:44,000
Some of them in two or three or four or five or six.

217
00:13:44,000 --> 00:13:48,000
But the longest it will take them to become fully enlightened

218
00:13:48,000 --> 00:13:52,000
and free from some saris said to be seven lifetimes.

219
00:13:52,000 --> 00:13:54,000
That's what the texts say.

220
00:13:54,000 --> 00:13:57,000
A Tula Sotapana doesn't have this reassurance.

221
00:13:57,000 --> 00:14:01,000
Obviously they haven't realized the reassurance that they have

222
00:14:01,000 --> 00:14:06,000
is that on realizing the second stage of knowledge in this lifetime

223
00:14:06,000 --> 00:14:10,000
at the end of this lifetime with the breakup of the body

224
00:14:10,000 --> 00:14:13,000
for this one lifetime.

225
00:14:13,000 --> 00:14:17,000
They will not be reborn in any of the states of suffering.

226
00:14:17,000 --> 00:14:19,000
They will not be reborn in hell.

227
00:14:19,000 --> 00:14:21,000
They will not be reborn as an animal.

228
00:14:21,000 --> 00:14:24,000
They will not be reborn as a ghost.

229
00:14:24,000 --> 00:14:29,000
So they are also said to be a Sotapana for that reason.

230
00:14:29,000 --> 00:14:32,000
But it's a Tula Sotapana and it's just a designation to mean

231
00:14:32,000 --> 00:14:36,000
that this is the stage that one needs to reach to be safe for this lifetime.

232
00:14:36,000 --> 00:14:40,000
Next lifetime, they will be born as a human or in a good state.

233
00:14:40,000 --> 00:14:45,000
They might still forget all of that and begin to practice on wholesome deeds

234
00:14:45,000 --> 00:14:52,000
and be born in hell as a result without any difficulty.

235
00:14:52,000 --> 00:14:59,000
As for the follow-up question,

236
00:14:59,000 --> 00:15:04,000
many people who lived in the Buddha's time said to be a streamwinner for sure.

237
00:15:04,000 --> 00:15:08,000
Many, many people, there were...

238
00:15:08,000 --> 00:15:11,000
movie Sakha became a Sotapana when she was seven years old

239
00:15:11,000 --> 00:15:13,000
and at the Pindika became a Sotapana.

240
00:15:13,000 --> 00:15:18,000
King Gimbisara became a Sotapana.

241
00:15:18,000 --> 00:15:20,000
They say in the commentaries,

242
00:15:20,000 --> 00:15:24,000
there's many mentions of Sotapana how the Buddha would give a talk

243
00:15:24,000 --> 00:15:27,000
and the whole crowd would become a Sotapana or so on.

244
00:15:27,000 --> 00:15:32,000
They say in Sawati where the Buddha spent a lot of his lifetime,

245
00:15:32,000 --> 00:15:40,000
only like 26 or 24 years,

246
00:15:40,000 --> 00:15:45,000
that there were seven Goti of people.

247
00:15:45,000 --> 00:15:48,000
And five Goti were...

248
00:15:48,000 --> 00:15:55,000
were Arya Pughalam in Sotapana or Sakha Di Gamir and Gamir Arahan.

249
00:15:55,000 --> 00:16:02,000
And one Goti were Galiana Pughalam, which means they were people

250
00:16:02,000 --> 00:16:05,000
who practiced morality and practiced meditation,

251
00:16:05,000 --> 00:16:07,000
but hadn't become enlightened yet.

252
00:16:07,000 --> 00:16:10,000
And then there were one last Goti who were Pughalalam,

253
00:16:10,000 --> 00:16:12,000
which means they were full of defilements

254
00:16:12,000 --> 00:16:14,000
and had no interest in the Buddha's teaching.

255
00:16:14,000 --> 00:16:16,000
So there's a story of this pig butcher,

256
00:16:16,000 --> 00:16:20,000
Junda, who lived very close to Jata Wana near or in Sawati.

257
00:16:20,000 --> 00:16:23,000
And every day the monks would go past

258
00:16:23,000 --> 00:16:28,000
and they thought it was remarkable that this guy lived so close to Jata Wana

259
00:16:28,000 --> 00:16:32,000
and he never came to listen to the Dhamma once

260
00:16:32,000 --> 00:16:35,000
or do any good deeds of giving charity

261
00:16:35,000 --> 00:16:36,000
or keeping morality or so on.

262
00:16:36,000 --> 00:16:38,000
He just killed pigs for his whole life

263
00:16:38,000 --> 00:16:40,000
and it was horrible how he killed pigs.

264
00:16:40,000 --> 00:16:43,000
I think we did a video about that already.

265
00:16:43,000 --> 00:16:45,000
But just an example.

266
00:16:45,000 --> 00:16:49,000
So I hope that's a fairly comprehensive explanation of Sotapana.

267
00:16:49,000 --> 00:16:52,000
Is there anything else that I can't think of anything?

268
00:16:52,000 --> 00:16:55,000
I think of anything that should be added.

269
00:16:55,000 --> 00:16:59,000
I mean, you know, there's always more that can be said.

270
00:16:59,000 --> 00:17:02,000
Lots of theory involved.

271
00:17:02,000 --> 00:17:08,000
But a Sotapana is a person who has followed the meditation practice

272
00:17:08,000 --> 00:17:10,000
to its consummation.

273
00:17:10,000 --> 00:17:12,000
At that point they're safe.

274
00:17:12,000 --> 00:17:15,000
They have reached a state of safety.

275
00:17:15,000 --> 00:17:18,000
And this is because of the...

276
00:17:18,000 --> 00:17:21,000
It's now become a habit and it begins to snowball

277
00:17:21,000 --> 00:17:24,000
and so it's not that they become content or they can become content.

278
00:17:24,000 --> 00:17:26,000
They can't become content.

279
00:17:26,000 --> 00:17:30,000
They have developed a state of energy, the state of confidence

280
00:17:30,000 --> 00:17:34,000
in the practice that will lead them to always think about practicing.

281
00:17:34,000 --> 00:17:37,000
Even though they might fall into liking or disliking

282
00:17:37,000 --> 00:17:39,000
and even get married and have children,

283
00:17:39,000 --> 00:17:43,000
we saw Kaha had 20 children or something.

284
00:17:43,000 --> 00:17:47,000
And she was a Sotapana.

285
00:17:47,000 --> 00:17:49,000
But they will always be thinking about meditation.

286
00:17:49,000 --> 00:17:51,000
Always be trying to find the time to meditate.

287
00:17:51,000 --> 00:17:54,000
Always be thinking about how they can develop themselves

288
00:17:54,000 --> 00:17:58,000
and always be interested in good things and spiritual things

289
00:17:58,000 --> 00:18:00,000
because it's stuck in their minds.

290
00:18:00,000 --> 00:18:04,000
If there's a crack, then that crack can't be fixed.

291
00:18:04,000 --> 00:18:27,000
It will eventually overflow and make them free.

