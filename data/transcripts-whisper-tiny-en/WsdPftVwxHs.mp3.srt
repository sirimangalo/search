1
00:00:00,000 --> 00:00:23,000
Okay, good evening, tonight is our weekly meditator Dhammatak.

2
00:00:23,000 --> 00:00:30,000
So I wanted to talk tonight about Vipasana.

3
00:00:30,000 --> 00:00:35,000
Vipasana, someone recently asked on a...

4
00:00:35,000 --> 00:00:41,000
on the internet someone was asking,

5
00:00:41,000 --> 00:00:52,000
how do you practice Vipasana?

6
00:00:52,000 --> 00:00:59,000
So I wanted to talk about Vipasana.

7
00:00:59,000 --> 00:01:01,000
How do you practice Vipasana?

8
00:01:01,000 --> 00:01:05,000
There's other questions as well.

9
00:01:05,000 --> 00:01:07,000
Should we practice Vipasana?

10
00:01:07,000 --> 00:01:13,000
That's an interesting question.

11
00:01:13,000 --> 00:01:16,000
Why should we practice Vipasana?

12
00:01:16,000 --> 00:01:21,000
Why should we care how you practice Vipasana?

13
00:01:21,000 --> 00:01:24,000
Should we be practicing it?

14
00:01:24,000 --> 00:01:29,000
It's a question that you should answer right off because

15
00:01:29,000 --> 00:01:35,000
well, it's hard to practice if you have doubts about what you're doing.

16
00:01:35,000 --> 00:01:40,000
But other questions?

17
00:01:40,000 --> 00:01:41,000
What does it mean?

18
00:01:41,000 --> 00:01:45,000
What does Vipasana mean?

19
00:01:45,000 --> 00:01:49,000
So I'd like to answer several questions about it.

20
00:01:49,000 --> 00:01:52,000
First in regards to...

21
00:01:52,000 --> 00:01:54,000
I'm not going to go into detail.

22
00:01:54,000 --> 00:01:57,000
I don't think about why we should practice it.

23
00:01:57,000 --> 00:02:00,000
I'm going to assume that some of that work has already been done,

24
00:02:00,000 --> 00:02:08,000
or will be done if not in some of the other things I've seen.

25
00:02:08,000 --> 00:02:18,000
But I was once told by a monk that Vipasana

26
00:02:18,000 --> 00:02:24,000
was a later invention.

27
00:02:24,000 --> 00:02:27,000
So the question of what is Vipasana?

28
00:02:27,000 --> 00:02:30,000
It is immediately relative.

29
00:02:30,000 --> 00:02:34,000
A relevant, right?

30
00:02:34,000 --> 00:02:36,000
He said that Vipasana...

31
00:02:36,000 --> 00:02:37,000
What is Vipasana?

32
00:02:37,000 --> 00:02:38,000
It's a later invention.

33
00:02:38,000 --> 00:02:41,000
And he said the word Vipasana doesn't exist in a Vipitika.

34
00:02:41,000 --> 00:02:43,000
And it quite shocked me to hear this.

35
00:02:43,000 --> 00:02:49,000
And so I went and looked it up.

36
00:02:49,000 --> 00:02:56,000
The question down to the Buddha actually teach Vipasana.

37
00:02:56,000 --> 00:02:58,000
So we have to understand...

38
00:02:58,000 --> 00:03:02,000
First of all, let's go directly to what it means.

39
00:03:02,000 --> 00:03:05,000
Vipasana means to see it clearly.

40
00:03:05,000 --> 00:03:11,000
Vip means clearly, Pasana means seeing.

41
00:03:11,000 --> 00:03:15,000
In Sanskrit they say Vidarshana, I think.

42
00:03:15,000 --> 00:03:17,000
Or Vipasana.

43
00:03:17,000 --> 00:03:23,000
In Sri Lanka they say Vidarshana, which I assume comes from the Sanskrit.

44
00:03:23,000 --> 00:03:27,000
It's because Pasana and Dasana are the same word.

45
00:03:27,000 --> 00:03:29,000
That bit of trivia might not sound interesting,

46
00:03:29,000 --> 00:03:35,000
but it is important because Vipasana isn't a buzz word.

47
00:03:35,000 --> 00:03:39,000
And we shouldn't ever reduce things to labels, right?

48
00:03:39,000 --> 00:03:41,000
To a buzz word.

49
00:03:41,000 --> 00:03:43,000
The label is useful.

50
00:03:43,000 --> 00:03:45,000
But it's what's behind.

51
00:03:45,000 --> 00:03:47,000
It's the reality behind it.

52
00:03:47,000 --> 00:03:50,000
The reality doesn't have labels.

53
00:03:50,000 --> 00:03:54,000
The label just point us to the reality.

54
00:03:54,000 --> 00:03:56,000
So it means seeing clearly.

55
00:03:56,000 --> 00:03:59,000
And then if we ask generally, did the Buddha teach?

56
00:03:59,000 --> 00:04:01,000
Seeing...

57
00:04:01,000 --> 00:04:03,000
Just to see clearly.

58
00:04:03,000 --> 00:04:07,000
I think anyone who says no doesn't have a very good idea

59
00:04:07,000 --> 00:04:10,000
of the Buddha's teaching at all.

60
00:04:10,000 --> 00:04:16,000
The Buddha used the word Dasana, Pasana.

61
00:04:16,000 --> 00:04:19,000
And then the verbal forms.

62
00:04:19,000 --> 00:04:24,000
Dasana, Pasati is a common one.

63
00:04:24,000 --> 00:04:26,000
But he did use the word Vipasana.

64
00:04:26,000 --> 00:04:31,000
He didn't use it that often, the form.

65
00:04:31,000 --> 00:04:35,000
But he didn't use the actual form of that word Vipasana.

66
00:04:35,000 --> 00:04:41,000
Usually, when paired with samata, they said that these two things

67
00:04:41,000 --> 00:04:45,000
are sort of form the basis of practice.

68
00:04:45,000 --> 00:04:49,000
They're the two most important qualities.

69
00:04:49,000 --> 00:04:51,000
Samata means tranquility.

70
00:04:51,000 --> 00:04:57,000
And Vipasana means seeing clearly.

71
00:04:57,000 --> 00:05:00,000
He said, we once would say, some people practice one first,

72
00:05:00,000 --> 00:05:01,000
samata first.

73
00:05:01,000 --> 00:05:03,000
And then Vipasana, so they calm the mind.

74
00:05:03,000 --> 00:05:05,000
And then they practice insight.

75
00:05:05,000 --> 00:05:07,000
Other people practice insight first.

76
00:05:07,000 --> 00:05:10,000
And then as they go, the mind becomes calm.

77
00:05:10,000 --> 00:05:16,000
Some people practice them together.

78
00:05:16,000 --> 00:05:21,000
But the word Vipasana, the idea of seeing clearly,

79
00:05:21,000 --> 00:05:27,000
is all throughout the Buddha's teaching.

80
00:05:27,000 --> 00:05:30,000
A very common example.

81
00:05:30,000 --> 00:05:33,000
And an example of how you can miss it, if you're not

82
00:05:33,000 --> 00:05:36,000
conversant with the poly, because the English translations

83
00:05:36,000 --> 00:05:40,000
don't often say, they don't make it clear that that's

84
00:05:40,000 --> 00:05:43,000
what's being referred to.

85
00:05:43,000 --> 00:05:49,000
But we have a very influential suit to the Padekarata suit.

86
00:05:49,000 --> 00:05:55,000
Or the Buddha says, a dita nanwakami and a Padekan Kyanagatam.

87
00:05:55,000 --> 00:05:56,000
Don't go back to the past.

88
00:05:56,000 --> 00:05:59,000
Don't worry about the future.

89
00:05:59,000 --> 00:06:00,000
It's in the past.

90
00:06:00,000 --> 00:06:01,000
It's gone.

91
00:06:01,000 --> 00:06:02,000
It's in the future.

92
00:06:02,000 --> 00:06:03,000
It hasn't come.

93
00:06:03,000 --> 00:06:10,000
And then he says, but jupana jayodamang tata tata vipasati,

94
00:06:10,000 --> 00:06:19,000
which means whatever dhammas are present,

95
00:06:19,000 --> 00:06:22,000
are in the present.

96
00:06:22,000 --> 00:06:25,000
Tata tata vipasati see clearly vipasati,

97
00:06:25,000 --> 00:06:30,000
or he sees clearly, or one sees clearly in them,

98
00:06:30,000 --> 00:06:35,000
in regards to them.

99
00:06:45,000 --> 00:06:50,000
And so we have an example here of how the Buddha used this term.

100
00:06:50,000 --> 00:06:59,000
And most important, this is one very important discourse of the Buddha.

101
00:06:59,000 --> 00:07:01,000
He specifically pointed out what we should do.

102
00:07:01,000 --> 00:07:10,000
We should try and see clearly things dhammas in the present moment.

103
00:07:10,000 --> 00:07:17,000
So we can answer at least that far about why we should practice,

104
00:07:17,000 --> 00:07:21,000
or should we practice, or should we not want to learn about it?

105
00:07:21,000 --> 00:07:24,000
Because it was what the Buddha taught us to do.

106
00:07:24,000 --> 00:07:28,000
If you have any kind of idea that the Buddha knew what he was talking about,

107
00:07:28,000 --> 00:07:30,000
but you should, if you know anything about his teachings,

108
00:07:30,000 --> 00:07:31,000
they make.

109
00:07:31,000 --> 00:07:36,000
They're quite profound.

110
00:07:36,000 --> 00:07:40,000
Then you can appreciate the importance of these words

111
00:07:40,000 --> 00:07:44,000
that clearly seeing clearly is obviously something important,

112
00:07:44,000 --> 00:07:50,000
but more generally, just understanding that it means

113
00:07:50,000 --> 00:07:54,000
to see the present moment clearly is such a profound statement.

114
00:07:54,000 --> 00:07:58,000
Whereas we get caught up in the past and we worry about the future

115
00:07:58,000 --> 00:08:02,000
and we stress and suffer so much from that,

116
00:08:02,000 --> 00:08:07,000
if only we could just see reality clearly as being the present moment.

117
00:08:07,000 --> 00:08:13,000
It would be much more peaceful living than the present.

118
00:08:13,000 --> 00:08:16,000
And not only live in the present, but it didn't just say that.

119
00:08:16,000 --> 00:08:18,000
He said, see clearly.

120
00:08:18,000 --> 00:08:20,000
And not just see clearly the present,

121
00:08:20,000 --> 00:08:25,000
but see clearly the dhammas that arise in the present.

122
00:08:25,000 --> 00:08:32,000
Or the dhammas that there are in the present.

123
00:08:32,000 --> 00:08:35,000
So we still haven't answered how we practice we pass in them,

124
00:08:35,000 --> 00:08:44,000
but we've gotten some idea of what is meant by Vipassana.

125
00:08:44,000 --> 00:08:46,000
So we start here looking at this.

126
00:08:46,000 --> 00:08:49,000
This is a good example of where we can look, we say,

127
00:08:49,000 --> 00:08:52,000
okay, you practice Vipassana.

128
00:08:52,000 --> 00:09:02,000
You find some way to see the present dhammas clearly.

129
00:09:02,000 --> 00:09:06,000
And if we go try and trace this back,

130
00:09:06,000 --> 00:09:10,000
if you look at where the Buddha talked about seeing the dhammas

131
00:09:10,000 --> 00:09:14,000
in the present moment, you have another very important verse

132
00:09:14,000 --> 00:09:17,000
that the Buddha taught, where he says,

133
00:09:17,000 --> 00:09:36,000
the dhammas are non-self.

134
00:09:36,000 --> 00:09:43,000
When one sees this, with wisdom.

135
00:09:43,000 --> 00:09:45,000
And if you listen carefully,

136
00:09:45,000 --> 00:09:48,000
then you adopt the dhammas.

137
00:09:48,000 --> 00:09:50,000
So again, he's using the word passate,

138
00:09:50,000 --> 00:09:55,000
which is Vipassana.

139
00:09:55,000 --> 00:09:59,000
When one sees the dhammas with wisdom,

140
00:09:59,000 --> 00:10:01,000
with the dhammas,

141
00:10:01,000 --> 00:10:09,000
the dhammas becomes disenchanted with suffering.

142
00:10:09,000 --> 00:10:16,000
So this is the path of purification.

143
00:10:16,000 --> 00:10:21,000
So why we should practice it is right there that it leads to purity,

144
00:10:21,000 --> 00:10:28,000
which is very valuable quality.

145
00:10:28,000 --> 00:10:35,000
The idea being that when you see things clearly,

146
00:10:35,000 --> 00:10:39,000
there's a purity that's there that's absent

147
00:10:39,000 --> 00:10:43,000
that we're not able to attain when we don't see clearly.

148
00:10:43,000 --> 00:10:46,000
Because our habits, the way we look at things,

149
00:10:46,000 --> 00:10:48,000
the way we're accustomed to looking at things,

150
00:10:48,000 --> 00:10:54,000
is fraught with all sorts of ignorance and misunderstandings.

151
00:10:54,000 --> 00:10:59,000
So wanting to find happiness, we act in ways,

152
00:10:59,000 --> 00:11:06,000
and speaking ways, and thinking ways that cause us suffering.

153
00:11:06,000 --> 00:11:08,000
But once we see clearly, we won't do that.

154
00:11:08,000 --> 00:11:16,000
We won't react to things in ways that give rise to suffering.

155
00:11:16,000 --> 00:11:19,000
But the first part is more important for the practice.

156
00:11:19,000 --> 00:11:23,000
What is that we should see clearly while here we have a good answer.

157
00:11:23,000 --> 00:11:30,000
So we look in the present moment at the dhammas,

158
00:11:30,000 --> 00:11:35,000
whatever those may be.

159
00:11:35,000 --> 00:11:43,000
What we are going to see is that they're not so.

160
00:11:43,000 --> 00:11:51,000
And this opens the door up to really the base of the Buddhist teaching

161
00:11:51,000 --> 00:12:00,000
that really our way of looking at the world

162
00:12:00,000 --> 00:12:05,000
is flawed in at least this very fundamental way.

163
00:12:05,000 --> 00:12:08,000
You know, the Buddha also talks about impermanence and suffering,

164
00:12:08,000 --> 00:12:18,000
but I think the more deeper and more important quality is non-self.

165
00:12:18,000 --> 00:12:21,000
So the way we look at the world, even things in the present moment,

166
00:12:21,000 --> 00:12:24,000
if you told most people live in the present moment

167
00:12:24,000 --> 00:12:28,000
and see clearly the things in the present moment,

168
00:12:28,000 --> 00:12:32,000
they would say, okay, things, that thing there, that thing there,

169
00:12:32,000 --> 00:12:37,000
that person there, that things that have self.

170
00:12:37,000 --> 00:12:41,000
They have self in the sense that they have a name, right?

171
00:12:41,000 --> 00:12:44,000
They have an entity to them.

172
00:12:44,000 --> 00:12:50,000
They are a thing. They are an object.

173
00:12:50,000 --> 00:12:54,000
In fact, if you think about it, they're not actually a thing in the present moment at all.

174
00:12:54,000 --> 00:12:59,000
Their thing in this requires past and future.

175
00:12:59,000 --> 00:13:03,000
Like if you refer to a person by name,

176
00:13:03,000 --> 00:13:05,000
well, they're not at that moment that person.

177
00:13:05,000 --> 00:13:10,000
The person who they are is the thing that lasts for a moment to moment

178
00:13:10,000 --> 00:13:15,000
when you see and hear and smile and taste and hopefully not all of those,

179
00:13:15,000 --> 00:13:19,000
but you see and hear them when you think about them.

180
00:13:19,000 --> 00:13:23,000
But the same with things, everything that you see and hear and smile,

181
00:13:23,000 --> 00:13:26,000
are the things that last.

182
00:13:26,000 --> 00:13:35,000
And in fact, the only thing that is truly in the present moment is experience.

183
00:13:35,000 --> 00:13:39,000
And so, what I say about the base of the Buddha's teaching is, again,

184
00:13:39,000 --> 00:13:43,000
again, the Buddha talked about the six senses, the five aggregates,

185
00:13:43,000 --> 00:13:54,000
body, mind, the building blocks of experience.

186
00:13:54,000 --> 00:13:58,000
They thought that that is the basis of reality.

187
00:13:58,000 --> 00:14:00,000
Those are the dumbers.

188
00:14:00,000 --> 00:14:04,000
That people, places and things, those are not dumbers.

189
00:14:04,000 --> 00:14:06,000
Those are concepts.

190
00:14:06,000 --> 00:14:09,000
Those are selves.

191
00:14:09,000 --> 00:14:14,000
That's looking at the world from a perspective of what we think of it,

192
00:14:14,000 --> 00:14:17,000
of our conception of all the pieces in it.

193
00:14:17,000 --> 00:14:29,000
We put things together and we give them names and so on.

194
00:14:29,000 --> 00:14:35,000
But when we talk about non-self, it means to see the present moment.

195
00:14:35,000 --> 00:14:38,000
So, it encompasses impermanence and suffering as well,

196
00:14:38,000 --> 00:14:43,000
because impermanence is the fact that experiences are momentary.

197
00:14:43,000 --> 00:14:48,000
When we start to see that all the things that people in places,

198
00:14:48,000 --> 00:14:52,000
that we thought were real,

199
00:14:52,000 --> 00:14:55,000
are just made up of experiences.

200
00:14:55,000 --> 00:15:00,000
That our reception of everything in the world,

201
00:15:00,000 --> 00:15:03,000
what we actually receive is just seeing, hearing,

202
00:15:03,000 --> 00:15:11,000
hearing, spelling, tasting, feeling, thinking.

203
00:15:11,000 --> 00:15:17,000
And so seeing clearly has something to do with this change of perspective,

204
00:15:17,000 --> 00:15:24,000
from seeing things as selves, as stable, as satisfying.

205
00:15:24,000 --> 00:15:28,000
Because the essence is satisfaction.

206
00:15:28,000 --> 00:15:31,000
One thing that you start to realize when you practice,

207
00:15:31,000 --> 00:15:34,000
you practice when you see clearly.

208
00:15:34,000 --> 00:15:36,000
This is a part of seeing clearly.

209
00:15:36,000 --> 00:15:39,000
When we talk about suffering,

210
00:15:39,000 --> 00:15:42,000
all that means is that nothing can satisfy.

211
00:15:42,000 --> 00:15:47,000
Because anything that you thought exists actually is not real.

212
00:15:47,000 --> 00:15:50,000
Your experience of it is momentary.

213
00:15:50,000 --> 00:15:54,000
Your contact with it is dependent on experiences.

214
00:15:54,000 --> 00:16:00,000
You can only ever see or hear or smell or taste or feel or think about something.

215
00:16:00,000 --> 00:16:06,000
And so it can actually be anything for you more than just experience.

216
00:16:06,000 --> 00:16:08,000
And when you look at experiences,

217
00:16:08,000 --> 00:16:11,000
experiences are not satisfying.

218
00:16:11,000 --> 00:16:14,000
Why? Because they're impermanent.

219
00:16:14,000 --> 00:16:16,000
So you can't control them.

220
00:16:16,000 --> 00:16:17,000
You can't keep them.

221
00:16:17,000 --> 00:16:19,000
You can't keep them away.

222
00:16:19,000 --> 00:16:25,000
You can't manage them.

223
00:16:25,000 --> 00:16:27,000
And so when our expectations,

224
00:16:27,000 --> 00:16:30,000
when we want for things to be a certain way,

225
00:16:30,000 --> 00:16:33,000
and they're not that certain way we suffer,

226
00:16:33,000 --> 00:16:37,000
we build up attachment to things.

227
00:16:40,000 --> 00:16:45,000
We get out of sync with reality because we don't see clearly.

228
00:16:45,000 --> 00:16:47,000
We start to notice that when we do see clearly,

229
00:16:47,000 --> 00:16:49,000
we suffer less.

230
00:16:49,000 --> 00:16:52,000
When we do interact with things as they are,

231
00:16:52,000 --> 00:16:56,000
rather than how we wish they would be, we suffer less.

232
00:16:56,000 --> 00:17:04,000
So part of our practice of seeing clearly has to be

233
00:17:04,000 --> 00:17:06,000
seeing in terms of experience,

234
00:17:06,000 --> 00:17:08,000
seeing hearing, smelling, tasting, feeling,

235
00:17:08,000 --> 00:17:12,000
thinking in the physical and mental aspects of reality.

236
00:17:19,000 --> 00:17:24,000
And so when we think about this,

237
00:17:24,000 --> 00:17:29,000
if we go the next step and we try to think about how do the Buddha teach us

238
00:17:29,000 --> 00:17:32,000
to approach experience,

239
00:17:32,000 --> 00:17:36,000
then we can start to understand how we can practice to see clearly.

240
00:17:36,000 --> 00:17:39,000
What we should start to see as we look at these teachings

241
00:17:39,000 --> 00:17:42,000
is that you don't really practice seeing clearly.

242
00:17:42,000 --> 00:17:45,000
You practice something to see clearly.

243
00:17:45,000 --> 00:17:49,000
You have to practice too see clearly.

244
00:17:49,000 --> 00:17:54,000
And so to say practicing Vipassana is a bit misleading

245
00:17:54,000 --> 00:17:57,000
because you don't just, okay, I'm going to practice,

246
00:17:57,000 --> 00:17:59,000
I'm just going to see clearly many, many times,

247
00:17:59,000 --> 00:18:01,000
and that'll be my practice.

248
00:18:01,000 --> 00:18:03,000
You practice too see clearly.

249
00:18:03,000 --> 00:18:05,000
And if you practice correctly,

250
00:18:05,000 --> 00:18:07,000
of course you'll start to see clearly.

251
00:18:07,000 --> 00:18:09,000
It has to be that way.

252
00:18:09,000 --> 00:18:13,000
So there's a very, another very famous story

253
00:18:13,000 --> 00:18:17,000
or talk that the Buddha gave to Bahia.

254
00:18:17,000 --> 00:18:23,000
As Bahia asked the Buddha for the most concise teaching he could give.

255
00:18:23,000 --> 00:18:26,000
Bahia had been shipwrecked.

256
00:18:26,000 --> 00:18:31,000
He'd been shipwrecked and then cast ashore naked.

257
00:18:31,000 --> 00:18:37,000
And so he just covered himself up with a piece of pot

258
00:18:37,000 --> 00:18:41,000
or something, pottery or something.

259
00:18:41,000 --> 00:18:45,000
And walked around or begging for food

260
00:18:45,000 --> 00:18:47,000
and people started to think he,

261
00:18:47,000 --> 00:18:50,000
people thought he was maybe some ascetic

262
00:18:50,000 --> 00:18:53,000
because there were a lot of naked ascetics around that time.

263
00:18:53,000 --> 00:18:55,000
And so they gave him food and he thought,

264
00:18:55,000 --> 00:18:59,000
wow, people giving me food thinking I'm going to make it ascetic.

265
00:18:59,000 --> 00:19:04,000
So he started to act the part.

266
00:19:04,000 --> 00:19:06,000
He said, well, they'll not just be a naked ascetic.

267
00:19:06,000 --> 00:19:09,000
And people offered him clothes and he refused the clothes

268
00:19:09,000 --> 00:19:13,000
thinking, well, it won't get any support that way.

269
00:19:13,000 --> 00:19:16,000
And eventually people started to think he was somehow enlightened

270
00:19:16,000 --> 00:19:18,000
and he had started to think he was enlightened.

271
00:19:18,000 --> 00:19:23,000
And then the story goes that a Brahma, a God

272
00:19:23,000 --> 00:19:28,000
who used to be one of his fellow monks in the past life

273
00:19:28,000 --> 00:19:30,000
came down and said, Bahia, you're not an aran.

274
00:19:30,000 --> 00:19:33,000
You're not enlightened and you're not on the path to enlightenment.

275
00:19:33,000 --> 00:19:35,000
He said, go find the Buddha.

276
00:19:35,000 --> 00:19:37,000
The Buddha has arisen, go find him.

277
00:19:37,000 --> 00:19:39,000
So he went to see the Buddha

278
00:19:39,000 --> 00:19:42,000
and based on his harrowing journey

279
00:19:42,000 --> 00:19:47,000
of almost dying in the ocean and his realization that,

280
00:19:47,000 --> 00:19:51,000
well, he wasn't anywhere near enlightened in that,

281
00:19:51,000 --> 00:19:53,000
there was this idea of being enlightened.

282
00:19:53,000 --> 00:20:01,000
He just asked for the Buddha for the most simple teaching possible.

283
00:20:01,000 --> 00:20:05,000
And that's why it's so important that I'd say famous is because

284
00:20:05,000 --> 00:20:10,000
it's this concise and very powerful statement.

285
00:20:10,000 --> 00:20:16,000
It also explains quite well why we practice the way we do.

286
00:20:16,000 --> 00:20:20,000
Because the Buddha said that, then train yourself,

287
00:20:20,000 --> 00:20:25,000
you should train yourself in this way, Bahia.

288
00:20:25,000 --> 00:20:29,000
Deep day, deep tamat dang boi sati.

289
00:20:29,000 --> 00:20:33,000
What is seen should just be seen.

290
00:20:33,000 --> 00:20:38,000
And what is seen, there should just be what is seen.

291
00:20:38,000 --> 00:20:41,000
Soo tai soo tamat dang boi sati.

292
00:20:41,000 --> 00:20:45,000
And what is heard, there should just be what is heard.

293
00:20:45,000 --> 00:20:47,000
Mu tai mu tamat dang boi sati.

294
00:20:47,000 --> 00:20:52,000
What is felt, which means smell, taste, felt on the body.

295
00:20:52,000 --> 00:20:56,000
There should just be what is felt, what is experienced.

296
00:20:56,000 --> 00:21:02,000
Mu nya tai vinya tamat dang boi sati.

297
00:21:02,000 --> 00:21:05,000
What is cognised in the mind,

298
00:21:05,000 --> 00:21:10,000
just be what is cognised in the mind.

299
00:21:10,000 --> 00:21:14,000
And as if you do that, then there will be no,

300
00:21:14,000 --> 00:21:16,000
basically he said, you won't get caught up in it.

301
00:21:16,000 --> 00:21:18,000
He says something very, very cryptic,

302
00:21:18,000 --> 00:21:21,000
and well, terse, I guess.

303
00:21:21,000 --> 00:21:27,000
He says tat bhuang latina.

304
00:21:27,000 --> 00:21:37,000
And that there will be no you by that,

305
00:21:37,000 --> 00:21:42,000
or because of that.

306
00:21:42,000 --> 00:21:47,000
Tat bhuang latina.

307
00:21:47,000 --> 00:21:53,000
Meaning there will be no me, my, no self.

308
00:21:53,000 --> 00:21:58,000
You will not get caught up in those things.

309
00:21:58,000 --> 00:22:04,000
There will be a simple, clear seeing.

310
00:22:04,000 --> 00:22:07,000
So we get a very important part of the puzzle here,

311
00:22:07,000 --> 00:22:10,000
a part of the explanation if you want to clear understanding

312
00:22:10,000 --> 00:22:15,000
of what means to practice vipasana, to see clearly.

313
00:22:15,000 --> 00:22:19,000
Seeing clearly with wisdom,

314
00:22:19,000 --> 00:22:25,000
the word vinya meaning to know fully.

315
00:22:25,000 --> 00:22:30,000
Wisdom can be misleading because of how we use it in the West,

316
00:22:30,000 --> 00:22:35,000
but the word vinya means to know fully,

317
00:22:35,000 --> 00:22:38,000
to really know something.

318
00:22:38,000 --> 00:22:45,000
If you think something, I think,

319
00:22:45,000 --> 00:22:50,000
I think there is life on other planets.

320
00:22:50,000 --> 00:22:56,000
I think, but I don't know if I think that.

321
00:22:56,000 --> 00:22:58,000
But if you go to another planet,

322
00:22:58,000 --> 00:23:00,000
and then you see other life than you own,

323
00:23:00,000 --> 00:23:02,000
or if they contact us or something,

324
00:23:02,000 --> 00:23:05,000
or then you know.

325
00:23:05,000 --> 00:23:06,000
The same way I could say,

326
00:23:06,000 --> 00:23:09,000
well, I think Australia exists,

327
00:23:09,000 --> 00:23:14,000
but until I see it for myself, I don't really know.

328
00:23:14,000 --> 00:23:17,000
So vinya means to really know.

329
00:23:17,000 --> 00:23:20,000
That's just basically what it means.

330
00:23:20,000 --> 00:23:22,000
So if we talk about the dhammas,

331
00:23:22,000 --> 00:23:26,000
it makes a lot of sense to talk about just let seeing be seeing,

332
00:23:26,000 --> 00:23:30,000
because that's really knowing them.

333
00:23:30,000 --> 00:23:37,000
And it may seem quite mundane and simplistic,

334
00:23:37,000 --> 00:23:42,000
but that's actually the point is that we complicate things,

335
00:23:42,000 --> 00:23:46,000
rather than seeing things clearly.

336
00:23:46,000 --> 00:23:52,000
We react and we judge and we complicate

337
00:23:52,000 --> 00:23:58,000
and we transform things from what they are

338
00:23:58,000 --> 00:24:02,000
to what they're not.

339
00:24:02,000 --> 00:24:06,000
So seeing becomes a problem.

340
00:24:06,000 --> 00:24:08,000
You see someone you don't like,

341
00:24:08,000 --> 00:24:10,000
or you see something ugly,

342
00:24:10,000 --> 00:24:14,000
you see something beautiful.

343
00:24:14,000 --> 00:24:16,000
Instead of focusing on the seeing,

344
00:24:16,000 --> 00:24:18,000
it becomes the thing,

345
00:24:18,000 --> 00:24:21,000
and it becomes the whole story behind the thing,

346
00:24:21,000 --> 00:24:24,000
where you see something you like,

347
00:24:24,000 --> 00:24:29,000
or something you dislike.

348
00:24:29,000 --> 00:24:33,000
And then you get off on that,

349
00:24:33,000 --> 00:24:35,000
creating all sorts of karma,

350
00:24:35,000 --> 00:24:38,000
all sorts of future prospects.

351
00:24:38,000 --> 00:24:40,000
Based not at all on the experience,

352
00:24:40,000 --> 00:24:43,000
but based on how you perceive it,

353
00:24:43,000 --> 00:24:48,000
and how you react to it.

354
00:24:48,000 --> 00:24:51,000
So panea, meaning to see something fully,

355
00:24:51,000 --> 00:24:53,000
or to really see something,

356
00:24:53,000 --> 00:24:57,000
to really know something,

357
00:24:57,000 --> 00:25:03,000
to see it with a true knowledge of what it is.

358
00:25:03,000 --> 00:25:07,000
This is very different from how we normally interact with reality.

359
00:25:07,000 --> 00:25:13,000
So we've come close,

360
00:25:13,000 --> 00:25:15,000
I think, to answering the question.

361
00:25:15,000 --> 00:25:18,000
If we talk about the practice of letting seeing,

362
00:25:18,000 --> 00:25:20,000
just be seeing,

363
00:25:20,000 --> 00:25:24,000
there's another very important teaching of the Buddha,

364
00:25:24,000 --> 00:25:26,000
where he says this sort of thing as well.

365
00:25:26,000 --> 00:25:29,000
And this is really exactly how we practice,

366
00:25:29,000 --> 00:25:31,000
whether Buddha in the Satipatana,

367
00:25:31,000 --> 00:25:37,000
consciousness,it says Kachanto wah gachami di pejanati.

368
00:25:37,000 --> 00:25:39,000
Pachanati means, says means,

369
00:25:39,000 --> 00:25:42,000
means, no, it really knows.

370
00:25:42,000 --> 00:25:44,000
That's the same word as panea,

371
00:25:44,000 --> 00:25:46,000
but janati means panea.

372
00:25:52,000 --> 00:25:54,000
Kachanto wah gachami,

373
00:25:54,000 --> 00:25:55,000
when walking, when knowing,

374
00:25:55,000 --> 00:25:58,000
when walking, when going, when knowing,

375
00:25:58,000 --> 00:26:00,000
knowing, and going.

376
00:26:00,000 --> 00:26:11,000
When standing one knows I am standing, when sitting one knows I am sitting, when lying down, when feeling anger, when nose, I am feeling anger and so on and so on.

377
00:26:11,000 --> 00:26:19,000
When one feels pain, when pain arises, one knows that pain is arisen and so on.

378
00:26:19,000 --> 00:26:23,000
And this is the practice of Satipatana.

379
00:26:23,000 --> 00:26:38,000
So what we talk about, as I have said many times, what we practice, how we explain what we practice, we explain it as Satipatana, we passana.

380
00:26:38,000 --> 00:26:42,000
Satipatana is what we practice, we passana is why we practice.

381
00:26:42,000 --> 00:26:48,000
We practice Satipatana to see clearly.

382
00:26:48,000 --> 00:26:59,000
And so when you hear about using a mantra, call it what you like, using a word to remind yourself.

383
00:26:59,000 --> 00:27:08,000
When you say to yourself pain, pain, or when you say hearing, hearing, you see, when you're thinking about something and you say thinking, thinking.

384
00:27:08,000 --> 00:27:17,000
When you focus on the object and remind yourself, it is what it is.

385
00:27:17,000 --> 00:27:33,000
You're trying to cultivate this state of seeing reality just as it is, where seeing is just hearing and so on.

386
00:27:33,000 --> 00:27:47,000
And so how you practice, we passana becomes about how you cultivate this ability to see reality just as it is, to really know what it is.

387
00:27:47,000 --> 00:28:04,000
And it changes, it shifts your perception from people places and things and concepts to experiences.

388
00:28:04,000 --> 00:28:08,000
Some people call it penetrative insight.

389
00:28:08,000 --> 00:28:18,000
That's not how this should be translated, I don't think, but it's an interesting idea, it's a useful idea.

390
00:28:18,000 --> 00:28:30,000
Because it penetrates, we talk about penetrating through the cloud of this nebulous idea of people and places and things.

391
00:28:30,000 --> 00:28:38,000
It's nebulous because it's all in the mind and it's based very much on our own perceptions.

392
00:28:38,000 --> 00:28:44,000
We have ideas of who people are, what things are based on our preferences and so on.

393
00:28:44,000 --> 00:28:48,000
And it's not clearly here and now.

394
00:28:48,000 --> 00:28:52,000
It's not really in tune with what's actually happening.

395
00:28:52,000 --> 00:28:57,000
In order to actually have these concepts arise, you have to have experience.

396
00:28:57,000 --> 00:29:14,000
So it's penetrative in that it really puts you in direct contact with reality with the present moment with the dhamma.

397
00:29:14,000 --> 00:29:35,000
And so in brief, we busenize this parting of the cloud of delusion and darkness and ignorance to see reality just as it is, as it comes to us.

398
00:29:35,000 --> 00:29:51,000
And that is purifying. That changes our habits from one's of reactivity to one's of purity and peace, clarity.

399
00:29:51,000 --> 00:30:11,000
And the way we practice this is by being mindful, by reminding ourselves, by practicing, by cultivating the quality of mind that sees an experience just as it is.

400
00:30:11,000 --> 00:30:16,000
Seeing is just seeing, hearing is just hearing, smelling is just real.

401
00:30:16,000 --> 00:30:24,000
And so by practicing the way we are, even just watching the stomach rise and falling, it's not the stomach we're interested exactly in exactly.

402
00:30:24,000 --> 00:30:29,000
It's more the quality of mind with which we look at the stomach.

403
00:30:29,000 --> 00:30:37,000
It's just a simple object, but when you look at it, you'll see the reaction, the judgment, the forcing of the breath even.

404
00:30:37,000 --> 00:30:44,000
And you'll see all of this baggage that we carry, all of this coloring that we do of the experience.

405
00:30:44,000 --> 00:30:58,000
We don't actually interact with it just as it is, we react to it and we influence it based on our habit.

406
00:30:58,000 --> 00:31:03,000
As you see more and more clearly, this all starts to change.

407
00:31:03,000 --> 00:31:10,000
And so we attempt to just see the experience as it is, we busen.

408
00:31:10,000 --> 00:31:21,000
We say to ourselves, with the body, we watch the stomach and the foot, with the feelings, we say pain, pain, or if you feel happy saying happy or calm.

409
00:31:21,000 --> 00:31:26,000
With the thought, saying thinking, with the emotions, liking and disliking and so on.

410
00:31:26,000 --> 00:31:31,000
Mind states, senses, seeing, hearing, smelling.

411
00:31:31,000 --> 00:31:44,000
When we practice, when we cultivate this, then we start to see more clearly.

412
00:31:44,000 --> 00:31:51,000
So the way we teach, if you concisely want to talk about how you practice with the stomach.

413
00:31:51,000 --> 00:32:00,000
We teach the practice of using a word that reminds you of the experience to bring your mind closer to the experience.

414
00:32:00,000 --> 00:32:05,000
Which will allow you to see clearly, which will purify the mind.

415
00:32:05,000 --> 00:32:10,000
From all the bad habits that come from not seeing clearly.

416
00:32:10,000 --> 00:32:14,000
And that's simply how you practice with the stomach.

417
00:32:14,000 --> 00:32:17,000
So that's the talk for tonight.

418
00:32:17,000 --> 00:32:33,000
Thank you for listening.

