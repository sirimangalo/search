1
00:00:00,000 --> 00:00:07,280
Is it okay to have happiness within but be Christian at the same time or attain enlightenment

2
00:00:07,280 --> 00:00:09,280
and be Christian?

3
00:00:09,280 --> 00:00:16,720
Well, it's okay to have happiness within and be whatever you want, be a Christian.

4
00:00:16,720 --> 00:00:17,720
Possible to do it.

5
00:00:17,720 --> 00:00:19,640
Is it possible to attain enlightenment and be Christian?

6
00:00:19,640 --> 00:00:28,280
It depends what you mean by Christian because many of the things that Christ said are perfectly

7
00:00:28,280 --> 00:00:30,960
fine and actually Buddhist.

8
00:00:30,960 --> 00:00:31,960
You re-put yourself.

9
00:00:31,960 --> 00:00:39,360
That's the Buddha said that, something that was echoed by Christ or echoed in the Bible

10
00:00:39,360 --> 00:00:45,360
by whoever wrote the Bible, the New Testament.

11
00:00:45,360 --> 00:00:54,520
But if you believe that the only way to go to heaven is through Jesus Christ, if you

12
00:00:54,520 --> 00:01:06,360
believe that all you need to do to go to heaven is believe in what the reincarnation

13
00:01:06,360 --> 00:01:19,360
or was it resurrection or so on, or the sanctity, the Godhood of Jesus, or if you believe

14
00:01:19,360 --> 00:01:28,160
that heaven is permanent, hell is permanent, then these are, if you believe in a soul and

15
00:01:28,160 --> 00:01:34,040
everlasting soul or a soul at all, then these are things which will stop you from becoming

16
00:01:34,040 --> 00:01:40,520
in light and do stop Christians from becoming in light and practicing repass in the meditation.

17
00:01:40,520 --> 00:01:44,360
So there's no way around that.

18
00:01:44,360 --> 00:01:49,240
It's not who you are or what you say you are, I'm Christian, I'm Jewish, I'm Buddhist.

19
00:01:49,240 --> 00:01:52,880
It's the views that you hold that will get in the way.

20
00:01:52,880 --> 00:01:54,360
That's a religious thing.

21
00:01:54,360 --> 00:02:00,040
If you hold views that are contrary to reality, then you'll never come to understand reality.

22
00:02:00,040 --> 00:02:07,040
I mean, really, if you hold any views based on based on faith, based on faith, that don't

23
00:02:07,040 --> 00:02:14,080
have any basis in your investigation and aren't verifiable, aren't verified, then you'll

24
00:02:14,080 --> 00:02:20,000
never become enlightened until you're able to verify them or disprove them and give them

25
00:02:20,000 --> 00:02:45,120
up.

