1
00:00:00,000 --> 00:00:19,600
Welcome back to Ask a Monk.

2
00:00:19,600 --> 00:00:29,240
Today I will be answering the question as to what we should do for those people who are

3
00:00:29,240 --> 00:00:35,440
suffering from addiction and as a result might turn away from the meditation practice

4
00:00:35,440 --> 00:00:39,960
or might not be able to immediately step into the meditation practice.

5
00:00:39,960 --> 00:00:45,560
The suggestion is that possibly for some people, meditation isn't the answer or isn't

6
00:00:45,560 --> 00:00:54,280
the immediate answer that rather such people should undertake traditional therapy or rehab

7
00:00:54,280 --> 00:00:58,440
or so on before taking on the meditation.

8
00:00:58,440 --> 00:01:05,840
Now I think that it's important to understand that there are answers besides the meditation

9
00:01:05,840 --> 00:01:06,840
practice.

10
00:01:06,840 --> 00:01:12,440
A meditation practice isn't the only thing that's necessary and this is why the Buddha recommended

11
00:01:12,440 --> 00:01:17,040
morality as well because ideally we do away with these states.

12
00:01:17,040 --> 00:01:23,160
Ideally we give them up when we start to practice meditation and he himself said or made

13
00:01:23,160 --> 00:01:31,840
it quite clear in different ways that the meditation relies on morality and without morality

14
00:01:31,840 --> 00:01:34,760
it's not truly going to succeed.

15
00:01:34,760 --> 00:01:41,640
Now the problem with saying that one should avoid meditation or should not consider meditation

16
00:01:41,640 --> 00:01:49,360
is that it ignores the fact that meditation should indeed be a part or is a part of learning

17
00:01:49,360 --> 00:01:55,240
to keep morality, that even for people who can't keep basic morality who are stuck in addiction

18
00:01:55,240 --> 00:02:07,080
and have physical and mental addictions to substances or to the hormones in the body it

19
00:02:07,080 --> 00:02:12,640
was mentioned about masturbation in the person who asked the question was mentioning

20
00:02:12,640 --> 00:02:24,920
that and so on that there is that side but the meditation, the mental side is very much

21
00:02:24,920 --> 00:02:31,920
a part of the problem and that's very important to address.

22
00:02:31,920 --> 00:02:39,000
Now when we practice meditation we have to keep in mind the physical side and we have

23
00:02:39,000 --> 00:02:45,480
to understand that our state of being is going to affect our meditation is going to really

24
00:02:45,480 --> 00:02:51,520
set the tone of our meditation practice so we need to eat the right food, we need to

25
00:02:51,520 --> 00:02:58,520
look after our bodies and our health in terms of medicine and so on we have to find

26
00:02:58,520 --> 00:03:02,480
a suitable place to live and so on.

27
00:03:02,480 --> 00:03:08,440
Along with the many other aspects of our practice that we have to keep in mind so there's

28
00:03:08,440 --> 00:03:18,200
no reason not to practice meditation but especially for people who are suffering from

29
00:03:18,200 --> 00:03:23,600
severe forms of addiction, extreme forms of addiction, there are going to be many other

30
00:03:23,600 --> 00:03:28,240
things that they'll have to keep in mind, the Buddha gave a talk on all of the many

31
00:03:28,240 --> 00:03:33,680
ways to do away with the problems in the mind for instance we have to guard our senses

32
00:03:33,680 --> 00:03:45,560
so we shouldn't just look around the stare at things and watch and engage in the pleasures

33
00:03:45,560 --> 00:03:51,840
of the sense, we should restrain ourselves in terms of food, in terms of entertainment

34
00:03:51,840 --> 00:04:02,200
and so on, we should guard our faculties so that we are mindful and we are able to keep

35
00:04:02,200 --> 00:04:09,080
track of our state of mind and of the world around us so to not get caught up in entertainment

36
00:04:09,080 --> 00:04:10,080
and pleasure.

37
00:04:10,080 --> 00:04:16,040
I've given talks on this before the various parts of the practice so that certainly is

38
00:04:16,040 --> 00:04:24,920
true but the concern is to whether people who take up the meditation practice and aren't

39
00:04:24,920 --> 00:04:32,120
capable that they might therefore turn away from the practice and give it up is it seems

40
00:04:32,120 --> 00:04:40,400
a little bit specious that the truth of the matter is and what we see is there are reasons

41
00:04:40,400 --> 00:04:50,040
why they give up and to us to an extent we can mitigate these, it's often because there's

42
00:04:50,040 --> 00:04:56,800
improper instruction, it's often because there isn't the comprehensive practice, it's often

43
00:04:56,800 --> 00:05:02,080
because of the surrounding, so every person's position, every person's situation

44
00:05:02,080 --> 00:05:10,080
is different, what's important is how we address this issue of people giving up the practice

45
00:05:10,080 --> 00:05:14,480
and leaving it behind, the first thing we should say is that just because a person gives

46
00:05:14,480 --> 00:05:19,880
up the practice doesn't mean they haven't gained anything, so a person might begin to practice

47
00:05:19,880 --> 00:05:28,240
meditation and get to a certain level or get to a certain point and then give it up or

48
00:05:28,240 --> 00:05:35,680
put it aside, now we shouldn't therefore be discouraged or think that this person is useless

49
00:05:35,680 --> 00:05:42,880
or that they have no potential in the meditation practice, it can be that after some time

50
00:05:42,880 --> 00:05:47,480
they'll come back to it and what we gain the things that we do, especially things that

51
00:05:47,480 --> 00:05:53,640
affect our state of mind, have a profound effect on our psyche and they stay deeply ingrained

52
00:05:53,640 --> 00:05:57,800
in our memory, meditation is something that it's very difficult to forget and people

53
00:05:57,800 --> 00:06:03,800
can always come back to it, just learning the basics, the technique of meditation without

54
00:06:03,800 --> 00:06:09,960
even practicing it can be a great thing because in times of need it often comes back

55
00:06:09,960 --> 00:06:17,560
and people do take up the practice in earnest, so I think that's the first point that

56
00:06:17,560 --> 00:06:27,640
I would make, the second one is that environment plays a great role in addiction recovery

57
00:06:27,640 --> 00:06:35,120
so the physical aspects of addiction are obvious and environment isn't going to get rid

58
00:06:35,120 --> 00:06:38,120
of those no matter where you are, you still have the hormones coming up, you still have

59
00:06:38,120 --> 00:06:46,800
the chemical reactions and so on, the physical craving but at least half of the problem

60
00:06:46,800 --> 00:06:52,200
and actually much more of the problem is the mental side and that you can influence by

61
00:06:52,200 --> 00:06:57,640
your environment, by the people you surround yourself with, by the interactions you have, by

62
00:06:57,640 --> 00:07:02,760
the situations that you get yourself into, and obviously if all of your friends are addicted

63
00:07:02,760 --> 00:07:09,960
as well, all of your friends go out to bars and drinking or do drugs or so on or if there's

64
00:07:09,960 --> 00:07:17,800
a hypersexuality in the world around you, then watching television or going to the mall,

65
00:07:17,800 --> 00:07:23,400
going to the beach and so on and seeing the objects of your desire, then obviously it's

66
00:07:23,400 --> 00:07:28,120
going to be much more difficult for you to overcome the states.

67
00:07:28,120 --> 00:07:34,240
Now this is where meditation can excel because a meditation center is pretty much the ideal

68
00:07:34,240 --> 00:07:35,240
environment.

69
00:07:35,240 --> 00:07:41,120
You're surrounded by people who are interested in meditation, who are trying to purify

70
00:07:41,120 --> 00:07:46,560
their own minds, who are supportive, who are talking about the same things and are encouraging

71
00:07:46,560 --> 00:07:51,480
each other in the same things, you have people talking about the meditation practice and

72
00:07:51,480 --> 00:07:55,920
teaching the meditation practice, you have a really supportive environment and that's really

73
00:07:55,920 --> 00:07:58,440
important, that makes a real difference.

74
00:07:58,440 --> 00:08:05,600
I think the people who turn away most often are those people who have never had that environment

75
00:08:05,600 --> 00:08:10,920
or who have not had it on a long-term basis so people will go to a retreat for 10 days

76
00:08:10,920 --> 00:08:14,800
and all of the people come together for 10 days but no one's living there, no one's staying

77
00:08:14,800 --> 00:08:15,800
there.

78
00:08:15,800 --> 00:08:22,480
There isn't the community feeling, you don't feel like you're really living in this place

79
00:08:22,480 --> 00:08:26,840
in this environment, it's quite different when you have a monastery that you go to and

80
00:08:26,840 --> 00:08:31,760
there are people staying there and living there and you can live for a month or a year

81
00:08:31,760 --> 00:08:35,960
and undertake the practice as a lifestyle.

82
00:08:35,960 --> 00:08:41,680
That's a real great support and you'll see that in addiction therapy as well, they advise

83
00:08:41,680 --> 00:08:47,560
the same thing, that it should be residential, so the meditation in that sense provides

84
00:08:47,560 --> 00:08:56,680
an excellent form of addiction therapy just by the basic environment.

85
00:08:56,680 --> 00:09:02,000
Another thing is in regards to the physical addiction itself and I've said this, I've

86
00:09:02,000 --> 00:09:07,760
talked about this before, that physical addiction is one thing but part of our practice

87
00:09:07,760 --> 00:09:14,880
is to not be free from the physical addiction but to rise above it, to see that it's

88
00:09:14,880 --> 00:09:17,240
only a physical reaction.

89
00:09:17,240 --> 00:09:22,920
The cravings that occur in the mind are actually not cravings, they're just physical processes.

90
00:09:22,920 --> 00:09:27,200
So someone who's addicted to nicotine, for example, or someone who's addicted to the

91
00:09:27,200 --> 00:09:35,240
sexual hormones, this is only the physical side, it's something that arises in ceases,

92
00:09:35,240 --> 00:09:36,920
it's actually neutral.

93
00:09:36,920 --> 00:09:43,680
It's only our deeply ingrained reactions to the physical that causes the problem.

94
00:09:43,680 --> 00:09:51,360
So if we can simply see the feelings, the sensations for what they are, the sensation

95
00:09:51,360 --> 00:10:00,200
of hormones arising, the chemical interactions of the hormones of nicotine and the cravings

96
00:10:00,200 --> 00:10:05,920
in the brain and so on, then they'll cease to have any power over us.

97
00:10:05,920 --> 00:10:15,160
This is why I said actually the physical is not the real problem and so for many people

98
00:10:15,160 --> 00:10:17,680
it's actually the lack of instruction.

99
00:10:17,680 --> 00:10:23,480
I know there are often people who will go to a meditation center and will not get proper

100
00:10:23,480 --> 00:10:28,440
instruction for whatever reason, sometimes it's because they don't listen and this comes

101
00:10:28,440 --> 00:10:33,840
back to the idea that many of people have pointed out already that you can't help everyone.

102
00:10:33,840 --> 00:10:39,640
So in the end it is true and it's very much worth bearing in mind that we should never

103
00:10:39,640 --> 00:10:44,160
be frustrated when the people around us don't want to meditate, we should take it upon ourselves

104
00:10:44,160 --> 00:10:49,840
to meditate and that will have an effect on our friends and family and so on.

105
00:10:49,840 --> 00:10:55,480
But in the end it's up to the individual and there's so many people in the world who

106
00:10:55,480 --> 00:11:01,520
won't ever meditate, not in this life, which is why I said give people what you can

107
00:11:01,520 --> 00:11:06,160
and help people as you can and don't expect too much.

108
00:11:06,160 --> 00:11:18,600
But on the other hand if we do give and if we are clear and if we understand correctly

109
00:11:18,600 --> 00:11:24,840
what it is that what is the meditation and how should one practice meditation?

110
00:11:24,840 --> 00:11:30,160
I've never really had a problem, never found anyone who didn't gain benefit from the practice.

111
00:11:30,160 --> 00:11:34,240
And you have to give up at a certain point and say that's all the person could gain and

112
00:11:34,240 --> 00:11:38,520
that's enough for them and not expect them or get frustrated when they don't get more

113
00:11:38,520 --> 00:11:41,360
or when they don't take it more seriously.

114
00:11:41,360 --> 00:11:47,600
But there has to be someone there to guide them and to instruct them.

115
00:11:47,600 --> 00:11:54,480
So I think it's important that we study and that we get clear in our own minds about

116
00:11:54,480 --> 00:12:01,680
the practice and try our best to give people the information.

117
00:12:01,680 --> 00:12:06,160
What they do with that information is up to them and I would submit that even just giving

118
00:12:06,160 --> 00:12:10,160
them the information as I said is a great thing.

119
00:12:10,160 --> 00:12:13,400
And I would never say to someone that you shouldn't meditate you should do something

120
00:12:13,400 --> 00:12:14,400
else first.

121
00:12:14,400 --> 00:12:18,600
I would say there are many other things that you could do with the meditation, complimenting

122
00:12:18,600 --> 00:12:23,720
the meditation, but meditation should be essential and eventually becomes really the only

123
00:12:23,720 --> 00:12:30,440
thing that you need once you understand and get it and experience the benefits and the results

124
00:12:30,440 --> 00:12:32,080
of the meditation.

125
00:12:32,080 --> 00:12:36,240
Then your mind will incline towards it and you'll find that it more and more becomes

126
00:12:36,240 --> 00:12:39,920
your answer to just about every problem that you have.

127
00:12:39,920 --> 00:12:43,680
So there's an answer to this question.

128
00:12:43,680 --> 00:12:45,760
That's been another episode of Ask a Monk.

129
00:12:45,760 --> 00:12:52,640
Thank you all for tuning in and hope to see you on the forum submitting your own questions

130
00:12:52,640 --> 00:12:53,640
and your own answers.

131
00:12:53,640 --> 00:12:56,000
I'd like to thank everyone for submitting answers.

132
00:12:56,000 --> 00:13:04,280
It certainly makes my job easier to have people who have in this way have studied the meditation.

133
00:13:04,280 --> 00:13:10,720
It's not my teaching, but study from the Buddha's teaching and this tradition, this interpretation

134
00:13:10,720 --> 00:13:12,480
of the Buddha's teaching.

135
00:13:12,480 --> 00:13:18,080
This tradition based on the Buddha's teaching and they're able to use that to help others.

136
00:13:18,080 --> 00:13:24,120
It's great to see and I'd encourage you to do that not only here but also in your own family

137
00:13:24,120 --> 00:13:51,640
and your own town and your own area, so again thanks for tuning in and all the best.

