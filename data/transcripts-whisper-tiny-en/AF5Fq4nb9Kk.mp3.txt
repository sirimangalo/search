Okay, I'm thinking about teaching elementary students meditation especially those who have emotional
disturbances such as strong anger, defiance, hyperactivity.
What is the best technique are they too young to learn?
Okay, I probably should do a separate video just to say this but let me just say it again
and again and all of them because it's something that I believe quite strongly and I've
said it now tonight a few times is that don't go where the real problems are, I would
recommend against trying to deal with people who have serious problems.
It's very altruistic to do so but it's missing one important, very important point.
The amount of effort you put in to helping someone who is in a very bad state is compared
with the benefit of helping them, the potential benefit.
The effort level is incredibly high.
The amount of benefit that you can give to them is very low.
There's no miracles, a person who has a lot of mental trouble is not likely to in most
cases become fully enlightened being in this life.
It just doesn't happen this month that I was talking about as an example of that.
He practiced meditation, he's been practicing, if he still alive, he's been practicing
for 20 years or something and he's still taking baby steps.
He's still blowing up, he's still got the same problems.
It's a very, very slow path.
Everyone who practices meditation is going to go through the same conditions again and
again and again until finally they're worked out, worn down, broken apart and discarded.
But until that point they will come back again and again and again until they become weaker
and weaker until they disappear.
You get a far less return for your effort.
If you take the same amount of effort and you apply it to people who are exceptional
gifted, brilliant, focused, people who have potential and you teach them meditation.
First of all, very little effort, second of all, incredible results that they will
be, they will grasp it much easier, pick it up and in many cases it's like planting
a seed.
You don't have to make it grow, it grows by itself, it goes viral because they pick it
up and they become you.
They become the person who is spreading the teaching.
You help such people who are teachers and those teachers go and help and go and teach.
So at the very least what you'll get theoretically is a bunch of people who are very
gifted who will then go out and pass along what you've passed along to people who are
less gifted and on and on down the line and there will be people who are actually working
with these kind of people who have serious problems and they will have based on the teachings
that you have been giving on a higher level, they will do the grunt work of dealing with
these people and it will create more benefit in the long run, even for those people
who are in a bad way because you are helping people who are in a position to help others
will create a chain, it's like a pyramid scheme, it will multiply and everyone will benefit.
What I'm saying is that people who come to practice meditation with me end up becoming
social workers or teachers and so I don't have to go out and teach those people, I teach
the people who will go out and teach them which is to me far better use of one's time.
The basic point being to try to pick the gifted, pick the ones who are you going to get
the best return for your effort because not only it's not being selfish, it's not being
discriminatory, it's being realistic and understanding that we're really like in a war
here, we don't have the luxury to pick and choose, we have to go for the most benefit
because death waits for no one and nothing waits, the universe will continue rolling on in
its way and if we don't keep up, we're going to get left behind.
This is why people get burnt out when they deal with such people, now this is only half
of the answer, my answer is don't teach children, this is only in relation to the part
of especially those who have emotional disturbances.
You're welcome to do it, I'm not going to try to tell you what to do, just give you
some idea maybe it will help to change your focus, I would think it would be much for
my point of view, much more inspiring to teach the gifted children and the way that you
teach children, the question as to whether they're too young, well the theory, the tradition
goes seven years, everything's seven, so at seven years they're old enough to understand
which is most children already, but anything below seven years is considered to be too
young, but at seven years this is the cutoff and then they can start learning.
The way that these children is to introduce to them the concept of mindfulness, the concept
of recognition, teaching them to recognize and to clearly perceive the experience in
front of them, or anything really, and I tried an experiment once with some kids in a
Thai time monastery in Los Angeles, I had them focus on a cat, I had them think of a cat
and say to themselves in their mind cat, because this was a room of like a hundred kids
and they were unruly, these were kids who come to the temple on Saturdays and Sundays
and get whatever lesson that is possible to give them, but it's mainly, well whatever,
it is what it is, and so they were like anything I would say, they would repeat it and
they would make fun of it and laugh at it, and I wasn't upset by that, but I was prepared
in advance because I was aware of the situation, so you can't lead these people into directly
into meditation, you can't say start watching the stomach say rising, give them something
that's going to appeal to them, so I thought well let's do some simple kid stuff, think
of a cat and say to yourself cat, cat, cat, and then dog, dog, I had them do dog, dog
and it's kind of fun at first, it seems like I'm just playing a game with them, but actually
they start to conceive and they start to focus and their mind starts to quiet down, and
it did work, I think it worked quite well, it's something that you'd have to try long term,
but the point of it is to get them around slowly to the point of being able to watch and
observe their own experience, first a cat and then a dog, and then I had them focus on
their parents, and this is important, and this really got me brownie points with the parents,
the parents are all sitting listening as well, and as I said think of your mother and
say to yourself mother, mother, mother, and then perfect your father and say to yourself
father and father, and then I did, then I came to the, what is actually the beginnings
of Buddhist meditation, it's the focusing on the Buddha, right, you have this famous Buddhist
meditation of saying yourself Buddha, Buddha, so I had them do that, I said picture the
Buddha now, because the Buddha being golden Buddha image, so think of the Buddha and say
Buddha, Buddha, Buddha, Buddha, Buddha, Buddha, Buddha, and so they did this, and then eventually
I got around, I think after the Buddha then it was right to focusing on the body, and
then it was watching the stomach, watching your breath, I brought it to them that way, I think
that it's just a simple point that the idea of starting with something cartoonish that
they can relate to, and of course it depends on the age level, but the important point
is to bring the concept to them, find a way to explain mindfulness to them, and I think
that's clearly explained by using the cat example, the dog exam, because you're just focusing
on something and you're seeing this as a cat, this is a dog, and then you have, they're
building, what you're doing this, they're building up this ability, and all you do is
you bring that ability, that proficiency, or however, back to focus on reality.
