Let's go. I trust you.
Okay, want to help you are well. My biggest fear is to lose my mother.
Lose my mother to death.
I know we have to accept it.
She is not mine, but the fear is still haunting me even in my dreams.
Please help me understand with your words. Thank you so much.
Well, to love your mother is a good thing.
There's good there.
I think you kind of have to separate that out.
And this is what people who have strong sense of family aren't able to do.
And so no matter how they claim that they understand concepts of non-self and detachment,
they really do feel that it's good to be attached to their parents deep down.
It's very much a part of their idea of themselves to be afraid of losing your mother.
So you're not afraid to lose your mother.
Maybe it's a good way to approach this.
You're not afraid of losing your mother.
This fear arises from time to time,
probably with some frequency.
Fear doesn't haunt you, fear arises.
Thinking of it in these terms will help you to align your mind in order to deal with it.
It will also help you to see clearly what's going on.
It's not just the fear. There's also a love of your mother.
Gratitude towards your mother.
Attachment to your mother.
All of those things are also present.
And those things probably fuel the fear.
Well, most definitely fuel the fear.
And so all of those are the constituents.
And they're all being held together by this view of self.
You see how these defilements work.
They play different roles.
The view of self holds it together.
So you say, this is me afraid to lose my mother.
But actually, once you let go of that and you say, okay, fear arose,
you see, oh, that's only this part.
There's also the attachment. There's also the love.
And you're able to separate them out.
And in the end, you are able to free yourself
in the bad ones because you see they're the real problem.
You see clearly, that's not helping me.
Okay, that I know is bad.
And so you remove that from the equation.
And when we're left with is only the good stuff.
So there's love, there's gratitude, there's compassion,
and joy and equanimity.
There's wisdom.
There's insight.
And your relationship with your mother becomes wholesome.
And there's no sense of fear or sadness in regards to loss of your mother.
Okay, so remove the idea of self.
Get just the idea of this as a risen.
And you'll be able to break it up into its part
to see the many parts and as a result of the other.
That's insight meditation in a nutshell.
