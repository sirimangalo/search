1
00:00:00,000 --> 00:00:04,000
I don't want to come back to our study of the Dhamma Bhada.

2
00:00:04,000 --> 00:00:11,000
Today we continue on with verse 142, which reads as follows.

3
00:00:11,000 --> 00:00:19,000
A long katou ji pei samang jari santou dantou nia to brahmatjari.

4
00:00:19,000 --> 00:00:23,000
Subbe subhoutesu nidaya dandang.

5
00:00:23,000 --> 00:00:29,000
So brahmanu sosammanu sabi ku.

6
00:00:29,000 --> 00:00:36,000
Which means a long katou ji pei means even if one is decorated or dressed up in finery,

7
00:00:36,000 --> 00:00:42,000
samang jari shadwan practice or fair peacefully.

8
00:00:42,000 --> 00:00:50,000
Santou or samang means quiet day or in a good way.

9
00:00:50,000 --> 00:01:02,000
Santou peaceful dantou well tamed nia to mean certain brahmatjari when the practice is holiness.

10
00:01:02,000 --> 00:01:14,000
Subbe subhoutesu nidaya dandang having discarded violence towards all beings.

11
00:01:14,000 --> 00:01:17,000
So brahmatjari santou one is called brahmanu.

12
00:01:17,000 --> 00:01:19,000
Santou one is called a brahmanu.

13
00:01:19,000 --> 00:01:25,000
Santou one is called a shaman or a recluse.

14
00:01:25,000 --> 00:01:31,000
Santou one is called a bikhu.

15
00:01:31,000 --> 00:01:38,000
So this verse was taught in regards to the story of santou d.

16
00:01:38,000 --> 00:01:44,000
The santou d was a minister.

17
00:01:44,000 --> 00:01:49,000
King Bessena dikosala.

18
00:01:49,000 --> 00:01:56,000
It was the king of the kosala country in the time of the Buddha.

19
00:01:56,000 --> 00:02:04,000
The story goes that santou d was instrumental in putting down a revolt.

20
00:02:04,000 --> 00:02:09,000
Maybe through violence or maybe just by hopefully by peaceful means.

21
00:02:09,000 --> 00:02:14,000
But somehow there was a there was some problem in the frontier at the borderlands

22
00:02:14,000 --> 00:02:17,000
or too far from the reach of the king.

23
00:02:17,000 --> 00:02:24,000
So the king sent his minister maybe his general or something to see about it.

24
00:02:24,000 --> 00:02:31,000
And he did such a good job at pacifying the situation that the king turned over his kingdom

25
00:02:31,000 --> 00:02:38,000
to santou d for seven days, not to rule but to enjoy.

26
00:02:38,000 --> 00:02:45,000
So he gave him the riches and luxury equal to a king for seven days.

27
00:02:45,000 --> 00:02:49,000
And he gave him a woman or he hired a woman.

28
00:02:49,000 --> 00:02:58,000
Probably wasn't, maybe it was a slave but somehow he got him a woman who danced and sang.

29
00:02:58,000 --> 00:03:01,000
And she entertained him for seven days.

30
00:03:01,000 --> 00:03:08,000
And so for seven days he drank himself into a stupor,

31
00:03:08,000 --> 00:03:16,000
wearing all of his finery and riding on the back of the king's elephant.

32
00:03:16,000 --> 00:03:20,000
And on the seventh day he was riding to the baiting place.

33
00:03:20,000 --> 00:03:23,000
He had to go to where he would bathe for the morning.

34
00:03:23,000 --> 00:03:33,000
All drunk and just enjoying incredible sensual pleasure.

35
00:03:33,000 --> 00:03:38,000
And he saw the Buddha as he was leaving the gate of the city.

36
00:03:38,000 --> 00:03:41,000
Buddha was coming in for arms.

37
00:03:41,000 --> 00:03:51,000
And staying on his seat up on the elephant, he nodded his head to the Buddha by way of respect.

38
00:03:51,000 --> 00:03:54,000
And then passed on.

39
00:03:54,000 --> 00:03:59,000
As he would, after he had passed, the Buddha smiled.

40
00:03:59,000 --> 00:04:06,000
And a Buddha's smile is, of course, a rare and wonderful thing, but it doesn't just smile at anything.

41
00:04:06,000 --> 00:04:13,000
It seems that these statues were the Buddha smiling, giving the implication that the Buddha's always smiling are probably not correct.

42
00:04:13,000 --> 00:04:19,000
It seems the Buddha's smile was a rare and wonderful thing.

43
00:04:19,000 --> 00:04:23,000
Anananda, who was, of course, always with the Buddha, noticed the smile and said to him,

44
00:04:23,000 --> 00:04:26,000
that the Buddha was her wide-view smile.

45
00:04:26,000 --> 00:04:32,000
Ananda, just look at that king's minister's entity.

46
00:04:32,000 --> 00:04:40,000
Here he is enjoying himself, totally steeped and intoxicated by sensual pleasure and by alcohol.

47
00:04:40,000 --> 00:04:50,000
But this very day, this evening, he's going to come into my presence, decked in all of his finery,

48
00:04:50,000 --> 00:04:58,000
completely decked out and looking like a peacock.

49
00:04:58,000 --> 00:05:08,000
And after listening to me teach him four verses, which is, of course, the verse of this Dhamapada,

50
00:05:08,000 --> 00:05:10,000
he will attain our hardship.

51
00:05:10,000 --> 00:05:21,000
He will attain our hardship and not only will he attain our hardship, but he will pass away into Parni Banda this very day.

52
00:05:21,000 --> 00:05:25,000
It seems that wherever the Buddha goes, there are people listening.

53
00:05:25,000 --> 00:05:33,000
So, someone heard him what he said and the words spread quickly.

54
00:05:33,000 --> 00:05:36,000
And the people were divided into two groups.

55
00:05:36,000 --> 00:05:41,000
The people who were sort of against the Buddha said,

56
00:05:41,000 --> 00:05:48,000
look at this fool who just says whatever comes into his head.

57
00:05:48,000 --> 00:05:52,000
Well, of course, this is something that is never going to happen.

58
00:05:52,000 --> 00:05:55,000
And he just says whatever he wants and now we've got him.

59
00:05:55,000 --> 00:06:02,000
Now everyone knows that he actually made this ridiculous claim and tonight we'll see.

60
00:06:02,000 --> 00:06:10,000
The night we'll show what a fool he is.

61
00:06:10,000 --> 00:06:15,000
And the other half of the people thought to themselves, wow, how wonderful is the Buddha?

62
00:06:15,000 --> 00:06:17,000
I can't believe this is actually going to happen.

63
00:06:17,000 --> 00:06:20,000
It's amazing that he's not doubting him for a moment

64
00:06:20,000 --> 00:06:30,000
and kind of waited around in the monastery just to watch this wonderful miracle.

65
00:06:30,000 --> 00:06:37,000
This thing that the Buddha could predict was going to come to be.

66
00:06:37,000 --> 00:06:39,000
So, what happened?

67
00:06:39,000 --> 00:06:46,000
Well, it turns out that this woman who had been dancing and singing for certainty for seven days,

68
00:06:46,000 --> 00:06:53,000
she had been fasting to make herself thin or to improve her grace.

69
00:06:53,000 --> 00:07:01,000
I guess that's a thing with dance, right, ballet artists are always starving themselves.

70
00:07:01,000 --> 00:07:06,000
Anybody who becomes passionate about anything tends to have to suffer

71
00:07:06,000 --> 00:07:12,000
when there would be spiritual or whether it would be worldly.

72
00:07:12,000 --> 00:07:18,000
But she had overdone it and so for seven days she hadn't really eaten much at all

73
00:07:18,000 --> 00:07:21,000
and she had been dancing and who knows what else.

74
00:07:21,000 --> 00:07:26,000
It says she was dancing and singing so maybe that's all she was doing.

75
00:07:26,000 --> 00:07:32,000
But as a result of this on that particular day when after he had gone to the bathing place

76
00:07:32,000 --> 00:07:37,000
and was sitting there watching this woman dance,

77
00:07:37,000 --> 00:07:43,000
suddenly she dropped dead, she felt this pain and her stomach shooting up her spine,

78
00:07:43,000 --> 00:07:45,000
shooting up to her heart.

79
00:07:45,000 --> 00:07:48,000
I don't know what medical condition that is,

80
00:07:48,000 --> 00:07:53,000
but as it were cut the flesh of her heart ascended

81
00:07:53,000 --> 00:08:00,000
and then in there with open mouth and open eyes, she died.

82
00:08:00,000 --> 00:08:09,000
The sanctity was of course shocked

83
00:08:09,000 --> 00:08:13,000
and he said, go take care of her, is she okay?

84
00:08:13,000 --> 00:08:15,000
And I said, she's dead.

85
00:08:15,000 --> 00:08:18,000
And as soon as he heard those words, he's sobered up.

86
00:08:18,000 --> 00:08:21,000
He immediately became sober.

87
00:08:21,000 --> 00:08:24,000
Maybe not immediately, that's what the text says,

88
00:08:24,000 --> 00:08:28,000
but it's perhaps more likely that he's sobered up as far as any of us would

89
00:08:28,000 --> 00:08:31,000
but it still had the effects of the alcohol.

90
00:08:31,000 --> 00:08:35,000
And this is something that we've, this story is of course well known in Buddhist circles

91
00:08:35,000 --> 00:08:38,000
and a lot of people say, use this as an example to say,

92
00:08:38,000 --> 00:08:43,000
well someone who is drunk and can sober up and learn the truth

93
00:08:43,000 --> 00:08:49,000
because next thing is going to do of course is come to realize the truth

94
00:08:49,000 --> 00:08:52,000
and become an hour on according to the prediction.

95
00:08:52,000 --> 00:08:55,000
But it's not actually what happened, you have to read carefully

96
00:08:55,000 --> 00:09:00,000
that is that so he's sobered up and he lost all interest in central pleasures

97
00:09:00,000 --> 00:09:02,000
which of course can happen.

98
00:09:02,000 --> 00:09:07,000
But he waited until the evening, so he went home

99
00:09:07,000 --> 00:09:12,000
and he spent all day recovering and just reeling in the shock of it.

100
00:09:12,000 --> 00:09:16,000
And by that time of course the alcohol would have naturally left his system

101
00:09:16,000 --> 00:09:18,000
to a great extent.

102
00:09:18,000 --> 00:09:22,000
And then he went to see the Buddha because he thought this morning

103
00:09:22,000 --> 00:09:26,000
he'd seen the Buddha come through the gate and he'd of course thought to himself,

104
00:09:26,000 --> 00:09:28,000
wow, what a magnificent person.

105
00:09:28,000 --> 00:09:35,000
And he was so respectful that he actually was not very respectful at all

106
00:09:35,000 --> 00:09:37,000
but was somewhat respectful.

107
00:09:37,000 --> 00:09:42,000
And that brief encounter made him think of something,

108
00:09:42,000 --> 00:09:46,000
doing something spiritually that this guy had bet he knows

109
00:09:46,000 --> 00:09:51,000
and bet he can help me and he said he came and he bowed down to the Buddha

110
00:09:51,000 --> 00:09:55,000
and he said, Reverend sir, I have this great sorrow, I've come to you

111
00:09:55,000 --> 00:10:03,000
because I know you will be able to extinguish my fire that it's in my heart

112
00:10:03,000 --> 00:10:09,000
and be my refuge.

113
00:10:09,000 --> 00:10:13,000
And the Buddha said, indeed,

114
00:10:13,000 --> 00:10:19,000
indeed you have come to find someone who can put out the fire in your heart

115
00:10:19,000 --> 00:10:23,000
who can assuage this mighty sorrow.

116
00:10:23,000 --> 00:10:27,000
And he said, Sunday on countless occasions,

117
00:10:27,000 --> 00:10:32,000
this woman in past lives of yours, innumerable.

118
00:10:32,000 --> 00:10:36,000
She has died and you have wept over her.

119
00:10:36,000 --> 00:10:39,000
And the tears that you have shed are greater than the waters

120
00:10:39,000 --> 00:10:41,000
and the four great oceans.

121
00:10:41,000 --> 00:10:43,000
It's just the way with all of us.

122
00:10:43,000 --> 00:10:49,000
So it's something to remind us of the infinity of some sorrow.

123
00:10:49,000 --> 00:10:55,000
All this is just a play that we go through again and again and again

124
00:10:55,000 --> 00:10:56,000
and again.

125
00:10:56,000 --> 00:10:59,000
All the suffering that we have, we think, oh wow,

126
00:10:59,000 --> 00:11:02,000
I've survived that.

127
00:11:02,000 --> 00:11:06,000
The thing is you've survived it countless times

128
00:11:06,000 --> 00:11:08,000
and you don't really survive it.

129
00:11:08,000 --> 00:11:11,000
You fall right back into it again and again and again

130
00:11:11,000 --> 00:11:17,000
or in your lesson, not in any lasting way.

131
00:11:21,000 --> 00:11:27,000
And then he taught the four verses

132
00:11:27,000 --> 00:11:34,000
and they have the verses actually that enlightened him

133
00:11:34,000 --> 00:11:37,000
or not the dumb upon the verses.

134
00:11:37,000 --> 00:11:39,800
He says, brought him to enlightenment.

135
00:11:39,800 --> 00:11:43,320
Let's chant those for, let's repeat those in Pauli,

136
00:11:43,320 --> 00:11:47,440
because they're actually quite useful meditative verses.

137
00:11:47,440 --> 00:11:51,840
Young will be, dang we so say, what is in the past,

138
00:11:51,840 --> 00:11:55,480
or what is previous, what is in the past?

139
00:11:57,520 --> 00:12:02,520
That, let one destroy, that should be destroyed.

140
00:12:02,520 --> 00:12:05,840
That one destroy, what is in the past,

141
00:12:05,840 --> 00:12:12,320
bhachate, mahokin jhanang, don't speak or, or think about the future.

142
00:12:12,320 --> 00:12:17,120
Don't say anything about the future, about what comes after.

143
00:12:17,120 --> 00:12:22,200
Palipu be means before, bhacham, come in after.

144
00:12:22,200 --> 00:12:27,800
Maje ji, nogahay sasi, if you don't grasp what's in the middle,

145
00:12:27,800 --> 00:12:29,520
which means what's now.

146
00:12:29,520 --> 00:12:35,920
Upasanto jalise sasi, you will dwell in peace.

147
00:12:39,920 --> 00:12:42,320
I think the chase will go with it also.

148
00:12:42,320 --> 00:12:48,720
If what is in the past, you don't, if you destroy what is in the past,

149
00:12:48,720 --> 00:12:52,320
if you destroy what is what came before,

150
00:12:52,320 --> 00:12:55,520
and say nothing of what comes after,

151
00:12:55,520 --> 00:13:02,520
nor grasp, nor grasp what is in the middle,

152
00:13:02,520 --> 00:13:05,520
he will dwell in peace.

153
00:13:08,520 --> 00:13:11,120
It's a very common teaching in the Buddha.

154
00:13:11,120 --> 00:13:14,520
We hear this elsewhere, ditananwakameh, napatikankay, nagatak,

155
00:13:14,520 --> 00:13:16,520
it's a very famous verse.

156
00:13:16,520 --> 00:13:18,520
Don't go back to the past.

157
00:13:18,520 --> 00:13:25,520
Don't worry about the future, but it's in the present, see it clearly.

158
00:13:25,520 --> 00:13:27,920
So there's ever any doubt where our mind should be,

159
00:13:27,920 --> 00:13:29,720
where the Buddha wished for our mind to be.

160
00:13:29,720 --> 00:13:32,920
This is actually a quote from the sutani pata, this one.

161
00:13:32,920 --> 00:13:35,320
It's not just in the dhammapada verse.

162
00:13:35,320 --> 00:13:38,720
Vibnag dhammapada's story.

163
00:13:38,720 --> 00:13:40,720
And so with that, he became an arhant.

164
00:13:40,720 --> 00:13:44,520
Hearing that, which of course brought his mind to the present moment

165
00:13:44,520 --> 00:13:48,120
and helped him to focus and meditate on the actual experience

166
00:13:48,120 --> 00:13:50,120
and see what was going on,

167
00:13:50,120 --> 00:13:54,120
and it was really just a spark to light the incredible fire

168
00:13:54,120 --> 00:13:58,120
that was already inside of him, not the fire of sorrow,

169
00:13:58,120 --> 00:14:01,120
but something else entirely.

170
00:14:01,120 --> 00:14:04,120
And he realized that he was about to pass away

171
00:14:04,120 --> 00:14:07,120
into Parinibhana, and so he asked the Buddha's permission,

172
00:14:07,120 --> 00:14:10,120
the Buddha said, the Buddha thought to himself,

173
00:14:10,120 --> 00:14:14,120
if I let him go into Parinibhana, now people are going to doubt.

174
00:14:14,120 --> 00:14:16,120
They're going to doubt as to whether he was enlightened

175
00:14:16,120 --> 00:14:18,120
or whether he just died.

176
00:14:18,120 --> 00:14:22,120
So he said, well, before you go, you tell us what great merit you did

177
00:14:22,120 --> 00:14:24,120
to become enlightened.

178
00:14:24,120 --> 00:14:27,120
What is this great thing in your heart that caused you

179
00:14:27,120 --> 00:14:30,120
to immediately on hearing this verse

180
00:14:30,120 --> 00:14:35,120
and realigning your mind to become enlightened?

181
00:14:35,120 --> 00:14:38,120
And then you may go.

182
00:14:38,120 --> 00:14:42,120
And so santa di sat down in meditation

183
00:14:42,120 --> 00:14:44,120
and floated up into the air, they say,

184
00:14:44,120 --> 00:14:47,120
you know, I think of seven palm trees,

185
00:14:47,120 --> 00:14:50,120
and then he retold the story.

186
00:14:50,120 --> 00:14:52,120
The story is a simple story,

187
00:14:52,120 --> 00:14:54,120
we won't go into too much detail,

188
00:14:54,120 --> 00:14:58,120
but it goes that he was dwelling in a kingdom

189
00:14:58,120 --> 00:15:02,120
in a past time, some other universe,

190
00:15:02,120 --> 00:15:05,120
or other world cycle,

191
00:15:05,120 --> 00:15:12,120
in a town that has the name of Bandhu, something.

192
00:15:12,120 --> 00:15:16,120
Bandhu Mati, or something.

193
00:15:16,120 --> 00:15:18,120
Bandhu Mati, yes.

194
00:15:18,120 --> 00:15:21,120
In the time of the Buddha weepasi,

195
00:15:21,120 --> 00:15:23,120
and he was reborn in the household,

196
00:15:23,120 --> 00:15:25,120
and he thought to himself,

197
00:15:25,120 --> 00:15:27,120
everybody listened up,

198
00:15:27,120 --> 00:15:28,120
it was a very good thought he thought to himself,

199
00:15:28,120 --> 00:15:33,120
what can I do?

200
00:15:33,120 --> 00:15:35,120
What can I do, what labor can I do

201
00:15:35,120 --> 00:15:39,120
that will do away with the want and suffering of others?

202
00:15:39,120 --> 00:15:41,120
What can I do that is great?

203
00:15:41,120 --> 00:15:44,120
What great thing can I do for the world?

204
00:15:44,120 --> 00:15:47,120
People nowadays, there are people who think like that,

205
00:15:47,120 --> 00:15:49,120
but what did he come up with?

206
00:15:49,120 --> 00:15:52,120
He thought, I will go and proclaim the truth,

207
00:15:52,120 --> 00:15:54,120
the teaching, the teaching of the Buddha.

208
00:15:54,120 --> 00:15:56,120
He had heard the teaching of Vipasi and he thought,

209
00:15:56,120 --> 00:15:57,120
this is what I will do.

210
00:15:57,120 --> 00:15:59,120
We'll go around telling people about this.

211
00:15:59,120 --> 00:16:02,120
It's funny, he never became enlightened himself,

212
00:16:02,120 --> 00:16:05,120
but he went around telling people about it.

213
00:16:05,120 --> 00:16:07,120
It's a great thing to do.

214
00:16:07,120 --> 00:16:09,120
You're doing a great service for others,

215
00:16:09,120 --> 00:16:12,120
telling them about the Buddha, the Buddha has come,

216
00:16:12,120 --> 00:16:18,120
kind of like an advertising service for the Buddha.

217
00:16:18,120 --> 00:16:23,120
And then he would relate the teachings that he had heard.

218
00:16:23,120 --> 00:16:25,120
So he would walk around through the streets,

219
00:16:25,120 --> 00:16:28,120
and the king heard about this and called him to him and said,

220
00:16:28,120 --> 00:16:29,120
what is it?

221
00:16:29,120 --> 00:16:30,120
What are you doing?

222
00:16:30,120 --> 00:16:34,120
And he said, oh, I'm proclaiming the truth.

223
00:16:34,120 --> 00:16:37,120
And he said, well, what do you ride in when you do that?

224
00:16:37,120 --> 00:16:41,120
Well, I just walk around in the kings that, oh, you can't do that.

225
00:16:41,120 --> 00:16:44,120
He said, if you're going to proclaim the dhamma,

226
00:16:44,120 --> 00:16:47,120
you have to ride, and he gave him a horse.

227
00:16:47,120 --> 00:16:51,120
And so he went around in a horse and put on some nice clothes

228
00:16:51,120 --> 00:16:54,120
and went around riding on the horse,

229
00:16:54,120 --> 00:16:56,120
preaching the dhamma throughout the countryside.

230
00:16:56,120 --> 00:17:00,120
And the king heard about, remember about him later on

231
00:17:00,120 --> 00:17:03,120
and talked to him later and said, no, I was still doing that.

232
00:17:03,120 --> 00:17:04,120
He said, yeah, yeah.

233
00:17:04,120 --> 00:17:07,120
And you're still riding that horse, that horse.

234
00:17:07,120 --> 00:17:09,120
He said, you shouldn't ride on that horse.

235
00:17:09,120 --> 00:17:11,120
You should ride something better than that.

236
00:17:11,120 --> 00:17:14,120
So he gave him a chariot.

237
00:17:14,120 --> 00:17:17,120
And the chariot eventually, he did that for a while.

238
00:17:17,120 --> 00:17:20,120
And the king was so impressed by him that he gave him

239
00:17:20,120 --> 00:17:28,120
eventually a royal.

240
00:17:28,120 --> 00:17:31,120
Not only that, he presented him with great wealth

241
00:17:31,120 --> 00:17:34,120
and a splendid set of jewels and gave him an elephant.

242
00:17:34,120 --> 00:17:36,120
And he had him ride on the back of an elephant

243
00:17:36,120 --> 00:17:40,120
with probably one of those, how do you call them pollen?

244
00:17:40,120 --> 00:17:42,120
Whatever they're called.

245
00:17:42,120 --> 00:17:45,120
Those things that you sit in on top of elephants.

246
00:17:45,120 --> 00:17:51,120
Or that you don't put royal people to do.

247
00:17:51,120 --> 00:17:54,120
And so he was completely decked out.

248
00:17:54,120 --> 00:17:56,120
And so this is somehow, I think,

249
00:17:56,120 --> 00:18:00,120
there's something there about the idea that

250
00:18:00,120 --> 00:18:06,120
maybe this last life was sort of an echo of that.

251
00:18:06,120 --> 00:18:09,120
Where in his last life he had still seven,

252
00:18:09,120 --> 00:18:13,120
still some of the goodness, the result of that goodness,

253
00:18:13,120 --> 00:18:16,120
allowing him to be decked out in finery one more time

254
00:18:16,120 --> 00:18:20,120
and riding on an elephant.

255
00:18:20,120 --> 00:18:24,120
And he says that was the mid-varitorious deed

256
00:18:24,120 --> 00:18:28,120
in a previous state of existence.

257
00:18:28,120 --> 00:18:30,120
So that's the story of Santiti.

258
00:18:30,120 --> 00:18:34,120
The Buddha then collected his, they collected his,

259
00:18:34,120 --> 00:18:38,120
is the relics, the bones from his funeral

260
00:18:38,120 --> 00:18:41,120
and they erected a Jaitya stupa somewhere.

261
00:18:41,120 --> 00:18:42,120
In India, I guess.

262
00:18:48,120 --> 00:18:53,120
And he proclaimed the greatness of Santiti.

263
00:18:53,120 --> 00:18:55,120
Now the monks were talking about this later on

264
00:18:55,120 --> 00:18:57,120
and this is where our verse comes from.

265
00:18:57,120 --> 00:18:59,120
And so we're talking about it and they said,

266
00:18:59,120 --> 00:19:00,120
it's amazing.

267
00:19:00,120 --> 00:19:02,120
Santiti, he became an arrow hunt.

268
00:19:02,120 --> 00:19:04,120
All decked out in his finery.

269
00:19:04,120 --> 00:19:07,120
He had been drinking alcohol that morning.

270
00:19:07,120 --> 00:19:09,120
And even though, and you have to remember,

271
00:19:09,120 --> 00:19:12,120
so when he sat across like it and rose up to the air,

272
00:19:12,120 --> 00:19:15,120
he was still decked out in all his finery.

273
00:19:15,120 --> 00:19:18,120
Fine clothes, jewels, perfumes,

274
00:19:18,120 --> 00:19:22,120
you know, the whole nine yards.

275
00:19:22,120 --> 00:19:25,120
And so the Buddha came in and asked him what they're talking about.

276
00:19:25,120 --> 00:19:28,120
And they told him when he was talking about it

277
00:19:28,120 --> 00:19:30,120
and they were talking about the Buddha said,

278
00:19:30,120 --> 00:19:33,120
you see, this is the thing.

279
00:19:33,120 --> 00:19:35,120
You want to call someone a Brahman,

280
00:19:35,120 --> 00:19:37,120
you want to call someone a shaman,

281
00:19:37,120 --> 00:19:41,120
shaman, shaman, shaman, or shaman in Pali.

282
00:19:41,120 --> 00:19:46,120
It means like a recluse or an ascetic or a hermit.

283
00:19:46,120 --> 00:19:50,120
Or a shaman, it's where the word shaman probably came from.

284
00:19:50,120 --> 00:19:59,120
I said, you don't need to be wearing rags.

285
00:19:59,120 --> 00:20:01,120
So there's something else.

286
00:20:01,120 --> 00:20:07,120
There's another reason for calling someone a Brahman or a shaman.

287
00:20:07,120 --> 00:20:10,120
And then he talked this first.

288
00:20:10,120 --> 00:20:12,120
Even though he may be richly adorned,

289
00:20:12,120 --> 00:20:14,120
if he walks in peace,

290
00:20:14,120 --> 00:20:19,120
if he be quiet, subdued, restrained and chased,

291
00:20:19,120 --> 00:20:21,120
yeah, like my version of that.

292
00:20:21,120 --> 00:20:25,120
It's called a Pali and it's translated.

293
00:20:25,120 --> 00:20:27,120
A Lanka Doji, peace among Daddy,

294
00:20:27,120 --> 00:20:29,120
even decked out,

295
00:20:29,120 --> 00:20:33,120
even decked out if one fares in peace,

296
00:20:33,120 --> 00:20:39,120
peaceful, tame, stable,

297
00:20:39,120 --> 00:20:41,120
or constantly holy,

298
00:20:41,120 --> 00:20:44,120
practicing holiness.

299
00:20:44,120 --> 00:20:47,120
Having put aside violence towards all being,

300
00:20:47,120 --> 00:20:49,120
such a one is a Brahman,

301
00:20:49,120 --> 00:20:51,120
such a one is a shaman,

302
00:20:51,120 --> 00:20:53,120
such a one is a beakoo,

303
00:20:53,120 --> 00:20:54,120
which is interesting.

304
00:20:54,120 --> 00:20:56,120
Beakoo is the word we use for monks.

305
00:20:56,120 --> 00:20:58,120
And this is a good example

306
00:20:58,120 --> 00:21:05,120
of how the Buddha used the word primarily in a different sense.

307
00:21:05,120 --> 00:21:08,120
He used the word beakoo to talk about the monks,

308
00:21:08,120 --> 00:21:10,120
and the monks became known as beakoo,

309
00:21:10,120 --> 00:21:12,120
and beakoo needs for the female.

310
00:21:12,120 --> 00:21:16,120
But it looks like the word to the Buddha

311
00:21:16,120 --> 00:21:21,120
actually meant something quite different.

312
00:21:21,120 --> 00:21:24,120
And the commentary gives an interesting,

313
00:21:24,120 --> 00:21:27,120
explains it as it's explained elsewhere.

314
00:21:27,120 --> 00:21:30,120
Bahitapapata, Brahmano.

315
00:21:30,120 --> 00:21:33,120
So one is a Brahmana because of Bahitapata.

316
00:21:33,120 --> 00:21:35,120
Outside Bahitapata is out.

317
00:21:35,120 --> 00:21:36,120
That's expelled.

318
00:21:36,120 --> 00:21:38,120
So one who has expelled evil.

319
00:21:38,120 --> 00:21:40,120
Bahapata.

320
00:21:40,120 --> 00:21:42,120
Bahapata is evil.

321
00:21:42,120 --> 00:21:44,120
That's why we call someone a Brahman,

322
00:21:44,120 --> 00:21:46,120
because they've expelled evil.

323
00:21:46,120 --> 00:21:49,120
Sumitapapata, Brahmano.

324
00:21:49,120 --> 00:21:54,120
One who has quieted or extinguished evil.

325
00:21:54,120 --> 00:21:56,120
One is a summoner.

326
00:21:56,120 --> 00:21:59,120
One is a shaman, because one has a summoner.

327
00:21:59,120 --> 00:22:03,120
Sumitata, extinguished or pieced.

328
00:22:03,120 --> 00:22:07,120
Bina kilesata, beakoo.

329
00:22:07,120 --> 00:22:11,120
One is a beakoo, because one has bina destroyed,

330
00:22:11,120 --> 00:22:18,120
or destroyed the defalments, kilesata.

331
00:22:18,120 --> 00:22:21,120
So that's the real lesson in this.

332
00:22:21,120 --> 00:22:25,120
This is one of those verses that you always recall to mind

333
00:22:25,120 --> 00:22:27,120
when you think of what's really important.

334
00:22:27,120 --> 00:22:30,120
It does nothing to do with the clothes that you wear

335
00:22:30,120 --> 00:22:34,120
or the possessions that you own.

336
00:22:34,120 --> 00:22:37,120
It has completely to do with the mind.

337
00:22:37,120 --> 00:22:42,120
Now, it goes without saying that it's much easier

338
00:22:42,120 --> 00:22:44,120
to calm your mind when you don't have lots of possessions

339
00:22:44,120 --> 00:22:50,120
and finery and dancing women or men surrounding you.

340
00:22:50,120 --> 00:22:54,120
So there's no question that there is a benefit

341
00:22:54,120 --> 00:22:58,120
to leaving all that behind and leaving the distraction

342
00:22:58,120 --> 00:23:00,120
of it all behind.

343
00:23:00,120 --> 00:23:04,120
But it's important to note, and there are a lot of people

344
00:23:04,120 --> 00:23:07,120
who therefore criticize the monastic life

345
00:23:07,120 --> 00:23:08,120
and say, oh, it's not necessary.

346
00:23:08,120 --> 00:23:10,120
It's not necessary for what is.

347
00:23:10,120 --> 00:23:15,120
This is a great, great evidence or authority

348
00:23:15,120 --> 00:23:18,120
to attest to that, but it's not really necessary.

349
00:23:18,120 --> 00:23:20,120
But it's one thing to say it's not necessary.

350
00:23:20,120 --> 00:23:22,120
It's not a thing to say that it's not useful.

351
00:23:22,120 --> 00:23:24,120
Certainly it's useful to give all that up.

352
00:23:24,120 --> 00:23:28,120
And it's also quite common for people

353
00:23:28,120 --> 00:23:30,120
who haven't given these things up to then become

354
00:23:30,120 --> 00:23:34,120
conceited or complacent and think

355
00:23:34,120 --> 00:23:37,120
that having given up external luxury

356
00:23:37,120 --> 00:23:39,120
that they've given up internal defilements,

357
00:23:39,120 --> 00:23:41,120
which is certainly not the case,

358
00:23:41,120 --> 00:23:43,120
because if you're not surrounded with finery

359
00:23:43,120 --> 00:23:50,120
or peeling things, it's very easy to become negligent

360
00:23:50,120 --> 00:23:52,120
and deluded into thinking that you're free

361
00:23:52,120 --> 00:23:54,120
because there's no problem.

362
00:23:54,120 --> 00:23:56,120
It's like if you never get sick

363
00:23:56,120 --> 00:23:58,120
or if you never get hurt,

364
00:23:58,120 --> 00:24:00,120
you might think, well, life is great.

365
00:24:00,120 --> 00:24:02,120
There's no potential for suffering.

366
00:24:02,120 --> 00:24:05,120
My life is good.

367
00:24:05,120 --> 00:24:07,120
Because we're always seeking for that, right?

368
00:24:07,120 --> 00:24:10,120
The good life, which is ridiculous.

369
00:24:10,120 --> 00:24:13,120
Life is very dangerous and it's very precarious.

370
00:24:13,120 --> 00:24:17,120
And if you get intoxicated by the good,

371
00:24:17,120 --> 00:24:22,120
when the bad comes, you'll be devastated.

372
00:24:22,120 --> 00:24:24,120
That's the first lesson of this.

373
00:24:24,120 --> 00:24:27,120
The big lesson is learning how to see

374
00:24:27,120 --> 00:24:28,120
what's really important.

375
00:24:28,120 --> 00:24:30,120
So no matter what you experience,

376
00:24:30,120 --> 00:24:32,120
no matter what you experience,

377
00:24:32,120 --> 00:24:35,120
it's all just experience.

378
00:24:35,120 --> 00:24:37,120
If you're wearing finery,

379
00:24:37,120 --> 00:24:39,120
it's still just seeing, hearing,

380
00:24:39,120 --> 00:24:42,120
smelling, tasting, feeling, thinking.

381
00:24:42,120 --> 00:24:46,120
If it would do it, it's still just tasting.

382
00:24:46,120 --> 00:24:48,120
The music and the sounds you hear,

383
00:24:48,120 --> 00:24:50,120
it's all just hearing.

384
00:24:50,120 --> 00:24:54,120
The sights you see, they're all just seeing.

385
00:24:54,120 --> 00:24:59,120
And the clinging to them, when you cling to beautiful sights or sounds,

386
00:24:59,120 --> 00:25:04,120
it will always be a cause for stress and suffering.

387
00:25:04,120 --> 00:25:09,120
Because none of it is stable.

388
00:25:09,120 --> 00:25:14,120
The more you get, the more you want.

389
00:25:14,120 --> 00:25:18,120
And you can't always get what you want.

390
00:25:18,120 --> 00:25:20,120
It's the big lesson.

391
00:25:20,120 --> 00:25:22,120
The other lesson, which is interesting,

392
00:25:22,120 --> 00:25:24,120
is that the Buddha actually taught the sanctity.

393
00:25:24,120 --> 00:25:26,120
It's actually a little deeper.

394
00:25:26,120 --> 00:25:29,120
It's to stay in the present moment.

395
00:25:29,120 --> 00:25:32,120
Because the sanctity, of course, was dwelling in the past.

396
00:25:32,120 --> 00:25:34,120
The recent past, the morning.

397
00:25:34,120 --> 00:25:37,120
In the evening, he dwelt on the morning.

398
00:25:37,120 --> 00:25:40,120
That's a good example for those of us who are in mourning.

399
00:25:40,120 --> 00:25:41,120
Morning.

400
00:25:41,120 --> 00:25:42,120
Right?

401
00:25:42,120 --> 00:25:43,120
Morning.

402
00:25:43,120 --> 00:25:46,120
When we mourn someone who died, who has died.

403
00:25:46,120 --> 00:25:48,120
When someone dies, you dwell in it.

404
00:25:48,120 --> 00:25:51,120
You're dwelling in the past.

405
00:25:51,120 --> 00:25:53,120
We're taught often that it's good to mourn.

406
00:25:53,120 --> 00:25:55,120
And that if you don't cry, it shows you don't care.

407
00:25:55,120 --> 00:25:57,120
And you should feel sad.

408
00:25:57,120 --> 00:25:59,120
And if you don't feel sad, there's something wrong with you.

409
00:25:59,120 --> 00:26:01,120
Which is quite bizarre.

410
00:26:01,120 --> 00:26:04,120
Because sadness is unpleasant.

411
00:26:04,120 --> 00:26:06,120
What's crying is actually quite pleasant.

412
00:26:06,120 --> 00:26:08,120
And it becomes addictive.

413
00:26:08,120 --> 00:26:10,120
But sadness is unpleasant.

414
00:26:10,120 --> 00:26:13,120
Grief is unpleasant.

415
00:26:13,120 --> 00:26:15,120
It becomes kind of a vicious cycle.

416
00:26:15,120 --> 00:26:18,120
Because the crying is such a relief.

417
00:26:18,120 --> 00:26:22,120
And the sadness is such a pain that you keep crying

418
00:26:22,120 --> 00:26:25,120
to relieve the pain.

419
00:26:25,120 --> 00:26:30,120
And you might even drink alcohol or do other crazy things.

420
00:26:30,120 --> 00:26:32,120
But know if you dwell in the present,

421
00:26:32,120 --> 00:26:36,120
if you're aware of the pleasure and the pain

422
00:26:36,120 --> 00:26:39,120
and the sadness and the happiness.

423
00:26:39,120 --> 00:26:41,120
As it happens.

424
00:26:41,120 --> 00:26:43,120
There's nothing to be sad about.

425
00:26:43,120 --> 00:26:46,120
There's nothing to mourn.

426
00:26:46,120 --> 00:26:48,120
The past has gone.

427
00:26:48,120 --> 00:26:50,120
The future hasn't come yet.

428
00:26:50,120 --> 00:26:52,120
The young pube tung we so say,

429
00:26:52,120 --> 00:26:54,120
he destroyed the past.

430
00:26:54,120 --> 00:27:00,120
But Jade, Maho, ginsunang, don't speak of the future.

431
00:27:00,120 --> 00:27:04,120
Maji, ji, no, gahi, sasi, if you will.

432
00:27:04,120 --> 00:27:06,120
Not grasp in the middle.

433
00:27:06,120 --> 00:27:08,120
Because the middle is where it really is happening.

434
00:27:08,120 --> 00:27:10,120
The middle is what is real.

435
00:27:10,120 --> 00:27:11,120
You can't actually grasp the past.

436
00:27:11,120 --> 00:27:13,120
What you're grasping is the present.

437
00:27:13,120 --> 00:27:15,120
And when you realize that, when you say,

438
00:27:15,120 --> 00:27:17,120
oh, I'm holding on, that's the problem.

439
00:27:17,120 --> 00:27:18,120
It creates the past.

440
00:27:18,120 --> 00:27:19,120
It creates the future.

441
00:27:19,120 --> 00:27:21,120
Holding on to the present.

442
00:27:21,120 --> 00:27:22,120
It creates the past.

443
00:27:22,120 --> 00:27:24,120
The future, when you stop doing that,

444
00:27:24,120 --> 00:27:26,120
Upa Santo, jade, sasi.

445
00:27:26,120 --> 00:27:28,120
Even dwell peaceful.

446
00:27:28,120 --> 00:27:29,120
That's our practice.

447
00:27:29,120 --> 00:27:32,120
That's what we're striving for.

448
00:27:32,120 --> 00:27:34,120
When we remind ourselves,

449
00:27:34,120 --> 00:27:35,120
it is what it is.

450
00:27:35,120 --> 00:27:37,120
Seeing, seeing, hearing, hearing,

451
00:27:37,120 --> 00:27:38,120
thinking, thinking.

452
00:27:38,120 --> 00:27:40,120
Sad, sad.

453
00:27:40,120 --> 00:27:47,120
And then we don't cling.

454
00:27:47,120 --> 00:27:50,120
We just experience.

455
00:27:50,120 --> 00:27:53,120
Comes, and it goes.

456
00:27:53,120 --> 00:27:55,120
And we dwell in peace.

457
00:27:55,120 --> 00:27:58,120
So that's the demo pod of her today.

458
00:27:58,120 --> 00:28:00,120
Thank you all for tuning in.

459
00:28:00,120 --> 00:28:18,120
I'm pushing you all the best.

