1
00:00:00,000 --> 00:00:06,000
Hello and welcome back to our study of the Dhamupanda.

2
00:00:06,000 --> 00:00:12,000
Today we continue on with verse 149, which reads as follows.

3
00:00:12,000 --> 00:00:40,000
This is Kastov, he is that are thrown away.

4
00:00:40,000 --> 00:00:49,000
Like a gourd, like a pumpkin in the fall.

5
00:00:49,000 --> 00:00:54,000
Like a pumpkin that has been harvested.

6
00:00:54,000 --> 00:01:01,000
Kapotakani at Pinini's bones that are graying, that are dull.

7
00:01:01,000 --> 00:01:04,000
They're bleached by the sun.

8
00:01:04,000 --> 00:01:09,000
One in this one, a carnity.

9
00:01:09,000 --> 00:01:14,000
Having seen them thus.

10
00:01:14,000 --> 00:01:29,000
Where, why the lust, why the passion.

11
00:01:29,000 --> 00:01:36,000
This is the old age chapter, so we have a lot of these sorts of verses of this one in regards

12
00:01:36,000 --> 00:01:45,000
to death, but it related to the body and the decay of the body.

13
00:01:45,000 --> 00:01:48,000
So this is referring to the body.

14
00:01:48,000 --> 00:01:52,000
It's a bit, it's poetry, so it's all out of order.

15
00:01:52,000 --> 00:02:03,000
And just as this body that is thrown away, Kastov, like a gourd in the fall,

16
00:02:03,000 --> 00:02:13,000
these bones, this body is just bones that are like bones that are bleached.

17
00:02:13,000 --> 00:02:17,000
It's just decaying bones.

18
00:02:17,000 --> 00:02:22,000
Having seen this, why the passion.

19
00:02:22,000 --> 00:02:27,000
Those are referring to our body where it's referring to other bodies.

20
00:02:27,000 --> 00:02:32,000
We look at the bodies of others.

21
00:02:32,000 --> 00:02:37,000
So this verse is actually in regards to dead bodies.

22
00:02:37,000 --> 00:02:46,000
It's told about a group of monks who were a demonica.

23
00:02:46,000 --> 00:02:52,000
A demonica, mana means one's estimation.

24
00:02:52,000 --> 00:02:55,000
What one thinks?

25
00:02:55,000 --> 00:02:58,000
What one thinks of one's self?

26
00:02:58,000 --> 00:03:06,000
So a demonica means overestimation of one's self.

27
00:03:06,000 --> 00:03:15,000
There's overestimation, underestimation, and then there's even equal estimation.

28
00:03:15,000 --> 00:03:20,000
So one can still esteem one's self appropriately,

29
00:03:20,000 --> 00:03:22,000
but it's still considered mana.

30
00:03:22,000 --> 00:03:25,000
It's still considered conceit.

31
00:03:25,000 --> 00:03:27,000
If you're a very strong person and you think,

32
00:03:27,000 --> 00:03:30,000
oh, I'm very strong and it's still conceit.

33
00:03:30,000 --> 00:03:35,000
If you're a weak person and think that you're very strong, all the worse.

34
00:03:35,000 --> 00:03:41,000
So these monks were a demonica, and they overestimated themselves.

35
00:03:41,000 --> 00:03:48,000
It seems that there were 500 monks who received meditation technique from the Buddha

36
00:03:48,000 --> 00:03:52,000
and went off to practice, and they having lived in the forest for some time.

37
00:03:52,000 --> 00:03:58,000
They entered into the Johnus, which are our meditation technique

38
00:03:58,000 --> 00:04:00,000
that the Buddha would often recommend.

39
00:04:00,000 --> 00:04:03,000
But it's really interesting about this story.

40
00:04:03,000 --> 00:04:11,000
This story gives one of these traditional examples of the common material position,

41
00:04:11,000 --> 00:04:17,000
that it's not enough, and that it's easy to become lost and caught up

42
00:04:17,000 --> 00:04:21,000
by these positive meditation states.

43
00:04:21,000 --> 00:04:25,000
And they thought that there are themselves Kieves and Nangasuracharina,

44
00:04:25,000 --> 00:04:33,000
Pabbatita Kicha, Non-Nipanam, with the non-arising of the defilements,

45
00:04:33,000 --> 00:04:43,000
we have completed and finished the duty of the renunciation life.

46
00:04:43,000 --> 00:04:48,000
So they become a nice idea.

47
00:04:48,000 --> 00:04:55,000
I'm thinking to themselves that because of the states of the Johnus are free from defilements,

48
00:04:55,000 --> 00:05:05,000
when you're in them repeatedly, the calm and the peace means no-arising of lust or aversion,

49
00:05:05,000 --> 00:05:11,000
or they might say there's some delusion, but there's certainly ignorance.

50
00:05:11,000 --> 00:05:18,000
But the ignorance led them to think that they were enlightened.

51
00:05:18,000 --> 00:05:20,000
And they said, let's go and tell the Buddha.

52
00:05:20,000 --> 00:05:24,000
Let's go and see the Buddha and tell him of our attainment.

53
00:05:24,000 --> 00:05:31,000
And so they traveled all the way.

54
00:05:31,000 --> 00:05:37,000
And came to the outer gate, or the outer door of the Buddha's Kukti,

55
00:05:37,000 --> 00:05:42,000
and the outer gate of the monastery.

56
00:05:42,000 --> 00:05:45,000
Right, they arrived outside the gate.

57
00:05:45,000 --> 00:05:52,000
The Buddha knew they were coming with his extraordinary vision and knowledge of things beyond the ordinary

58
00:05:52,000 --> 00:06:01,000
kind of individual, the ordinary people, said it won't do for them to see me.

59
00:06:01,000 --> 00:06:11,000
Because they'll come and then they'll have to argue with them.

60
00:06:11,000 --> 00:06:15,000
Let's show them, let's show them indirectly.

61
00:06:15,000 --> 00:06:21,000
He said to Ananda, tell them to go to such and such a cemetery.

62
00:06:21,000 --> 00:06:22,000
Charno ground.

63
00:06:22,000 --> 00:06:24,000
So in India they didn't have cemeteries.

64
00:06:24,000 --> 00:06:31,000
They wouldn't bury people.

65
00:06:31,000 --> 00:06:32,000
There were reasons for not burying.

66
00:06:32,000 --> 00:06:34,000
I can't remember what they were.

67
00:06:34,000 --> 00:06:36,000
It was something very interesting.

68
00:06:36,000 --> 00:06:41,000
It would have been very wrong to bury the body.

69
00:06:41,000 --> 00:06:48,000
But anyway, so poor people, they would just throw the bodies in the charno ground.

70
00:06:48,000 --> 00:06:51,000
Some of them would have been old and decaying.

71
00:06:51,000 --> 00:06:59,000
Some would have been fresh and you mostly naked men, women alike, young and old.

72
00:06:59,000 --> 00:07:01,000
So the Buddha sent them there.

73
00:07:01,000 --> 00:07:05,000
And they wandered along, they said, that's fine.

74
00:07:05,000 --> 00:07:07,000
Us being enlightened, we can go anywhere.

75
00:07:07,000 --> 00:07:11,000
We are patient.

76
00:07:11,000 --> 00:07:16,000
And so they got to the charno ground and they walked inside and they were standing there for a bit.

77
00:07:16,000 --> 00:07:21,000
They started to notice the corpses around them.

78
00:07:21,000 --> 00:07:29,000
And Charno, the disturbing site, many of them were never seen dead bodies before.

79
00:07:29,000 --> 00:07:36,000
And the disturbing site of rotting corpses cultivated in the moverge into Agatha.

80
00:07:36,000 --> 00:07:46,000
Repartments are dislike, disgust, revulsion.

81
00:07:46,000 --> 00:07:53,000
And the young bodies, the young women, the new bodies, the fresh bodies, the young

82
00:07:53,000 --> 00:08:06,000
beautiful women, cultivated lust and passionate.

83
00:08:06,000 --> 00:08:10,000
And as they stood there waiting for the Buddha, thinking on the Buddha is going to come and find us.

84
00:08:10,000 --> 00:08:22,000
They all realized that all of us still have this lust and the Buddha came to them and said this verse.

85
00:08:22,000 --> 00:08:27,000
He said, is it fitting that upon beholdings such an assemblages of bones,

86
00:08:27,000 --> 00:08:33,000
you should take pleasure in the evil passion and passion?

87
00:08:33,000 --> 00:08:39,000
And the English likes to embellish in the rich, that you should give rise to raga passion.

88
00:08:39,000 --> 00:08:42,000
He chose passion, but it includes the aversion.

89
00:08:42,000 --> 00:08:49,000
The aversion is also not suitable, not wholesome.

90
00:08:49,000 --> 00:08:53,000
And then he gave this verse.

91
00:08:53,000 --> 00:08:58,000
So it's actually relating to dead bodies, but the teaching in general,

92
00:08:58,000 --> 00:09:02,000
this kind of teaching, it's not just about the dead bodies.

93
00:09:02,000 --> 00:09:04,000
We all have this nature.

94
00:09:04,000 --> 00:09:18,000
Our eventual destination, the eventual destination of this physical body is the Charno ground or the cemetery or the crematorium.

95
00:09:18,000 --> 00:09:26,000
We're all just bags of bones, waiting to lie on the earth.

96
00:09:26,000 --> 00:09:31,000
A Chirang Watayangayo, but the Vingatisei sati.

97
00:09:31,000 --> 00:09:37,000
Before long, this body will lie on the ground.

98
00:09:37,000 --> 00:09:56,000
It will lie on the ground, but it will lie on the ground.

99
00:09:56,000 --> 00:10:09,000
It's useless like a Chard-Long, I remember the point.

100
00:10:09,000 --> 00:10:15,000
So, this is just have to do with us, how does this benefit us?

101
00:10:15,000 --> 00:10:19,000
Well, really interesting point about this.

102
00:10:19,000 --> 00:10:25,000
Of course, the simple one is the potential for overestimation.

103
00:10:25,000 --> 00:10:32,000
I mean, this shows the pitfalls of common tranquility.

104
00:10:32,000 --> 00:10:38,000
I mean, not to be hard on tranquility meditation, but to refer to all of us.

105
00:10:38,000 --> 00:10:45,000
I mean, there is that realization that some at the meditation that Charnos will never be enough,

106
00:10:45,000 --> 00:10:54,000
that it is possible to get caught up in them and to mistake them and have them be a cause for overestimation.

107
00:10:54,000 --> 00:11:02,000
But even for all of us practicing insight meditation, so much of our practice is going to be how we estimate, how we esteem ourselves.

108
00:11:02,000 --> 00:11:06,000
So, conceit is something that takes a long time to overcome.

109
00:11:06,000 --> 00:11:12,000
And so, we'll constantly be overestimating underestimating ourselves.

110
00:11:12,000 --> 00:11:14,000
Both of which are problematic.

111
00:11:14,000 --> 00:11:21,000
Underestimation needs to low confidence, needs to weak results.

112
00:11:21,000 --> 00:11:30,000
If you get discouraged, thinking that you're incapable of the practice, overestimation is dangerous because it needs to complacency.

113
00:11:30,000 --> 00:11:37,000
One thing that's quite common of meditators in the center is to overestimate the results of the practice.

114
00:11:37,000 --> 00:11:39,000
I mean, good results come from the practice.

115
00:11:39,000 --> 00:11:41,000
There's no question about that.

116
00:11:41,000 --> 00:11:48,000
But one of the most common observations, once you leave the center,

117
00:11:48,000 --> 00:11:56,000
is that the results were actually far more moderate than how it felt in the meditation center.

118
00:11:56,000 --> 00:12:06,000
Because here there's tranquility, here there's calm, here there is no adversary.

119
00:12:06,000 --> 00:12:12,000
There's no challenge to one's peace of mind.

120
00:12:12,000 --> 00:12:20,000
The defilements are like into a snake in the grass. If you look out on a field of grass, it looks quite calm, right?

121
00:12:20,000 --> 00:12:29,000
It looks peaceful. The mind is like that. The ordinary mind doesn't seem capable of great evil or defilement.

122
00:12:29,000 --> 00:12:37,000
But when you walk through the field and step on the snake, watch out.

123
00:12:37,000 --> 00:12:47,000
If the snake comes out of the grass, especially with the states of tranquility of trance,

124
00:12:47,000 --> 00:12:54,000
they are the calmest of the calm of all the written states.

125
00:12:54,000 --> 00:12:59,000
So it's easy to think that that's all that's left in your mind,

126
00:12:59,000 --> 00:13:05,000
which is why it's important to understand that enlightenment doesn't mean the cessation of defilements.

127
00:13:05,000 --> 00:13:13,000
The cessation of defilements can be temporary. How do you know that they're gone enlightenment means the wisdom that arises?

128
00:13:13,000 --> 00:13:20,000
The wisdom that destroys the potential for the defilements to arise.

129
00:13:20,000 --> 00:13:27,000
So that's an important lesson for us to be aware, not to become complacent.

130
00:13:27,000 --> 00:13:32,000
It's quite important that we're mindful of the calm states.

131
00:13:32,000 --> 00:13:36,000
We see them for what they are without estimating, thinking that they mean something.

132
00:13:36,000 --> 00:13:43,000
Oh, look at how calm I am. That must be a good sign. That must be a sign that I'm halfway there or I'm already there.

133
00:13:43,000 --> 00:13:52,000
Maybe I'm enlightened because of how calm I am. Very easy to fall into that.

134
00:13:52,000 --> 00:13:56,000
And not to overestimate our practice.

135
00:13:56,000 --> 00:14:10,000
And another important lesson of this is what is a proper meditation practice and how challenging ourselves is a proper part of our meditation practice.

136
00:14:10,000 --> 00:14:17,000
So the first about this is a reassurance when we practice difficult practices.

137
00:14:17,000 --> 00:14:25,000
When we practice insight meditation it feels somewhat discouraging by how difficult it is, how challenging it is, how uncomfortable it is.

138
00:14:25,000 --> 00:14:33,000
Here we have a good example of how being uncomfortable, being put in a situation that makes you uncomfortable is important.

139
00:14:33,000 --> 00:14:47,000
Moreover, we have this interesting aspect that are interesting fact or reality that sometimes the arising of the defilements is useful.

140
00:14:47,000 --> 00:14:55,000
We're not talking about purposefully inducing them saying, hey, I'll go do something that makes me angry.

141
00:14:55,000 --> 00:14:59,000
And yet putting yourself in a position where they can arise.

142
00:14:59,000 --> 00:15:03,000
I mean, this is what allows you to see who you are. It allows you to see the reality.

143
00:15:03,000 --> 00:15:11,000
It allows you to see the danger. If you're never confronted with how much makes you angry or greedy,

144
00:15:11,000 --> 00:15:21,000
how can you see the danger of the problem? How can you be spurred into action?

145
00:15:21,000 --> 00:15:27,000
And so to some extent, as I was talking about earlier, our practice has to involve the arising of defilements.

146
00:15:27,000 --> 00:15:31,000
Some people would say, if you don't enter into the John, how can you become enlightened?

147
00:15:31,000 --> 00:15:35,000
One might argue that if you're always in the John, how can you become enlightened?

148
00:15:35,000 --> 00:15:42,000
You never get to see the problems and the misunderstandings that we have.

149
00:15:42,000 --> 00:15:49,000
You can somehow feed them by thinking of things, thinking of reality as stable, as satisfying as control,

150
00:15:49,000 --> 00:15:52,000
because the John is to some extent, are.

151
00:15:52,000 --> 00:15:57,000
You won't ever let go of some sorrow.

152
00:15:57,000 --> 00:15:59,000
We shouldn't be afraid of the defilements.

153
00:15:59,000 --> 00:16:04,000
It's not to say we want them to arise or that they're harmless. They're harmful. They're bad.

154
00:16:04,000 --> 00:16:11,000
But part of our practice is to see how bad they are, how harmful they are.

155
00:16:11,000 --> 00:16:15,000
And so our meditation has to, to some extent, take us out of our comfort zone,

156
00:16:15,000 --> 00:16:18,000
and that includes taking us out of these calm states.

157
00:16:18,000 --> 00:16:21,000
Meditation shouldn't be all calm.

158
00:16:21,000 --> 00:16:25,000
It's a very good lesson for people who have cultivated samata practice,

159
00:16:25,000 --> 00:16:28,000
not to be content or complacent,

160
00:16:28,000 --> 00:16:31,000
and to go the extra step to undertake insight meditation.

161
00:16:31,000 --> 00:16:35,000
It's reassurance for those of us who are undertaking insight meditation,

162
00:16:35,000 --> 00:16:41,000
that even though our meditation might not become or learning a great deal about ourselves.

163
00:16:41,000 --> 00:16:50,000
And we're really changing, as we see the danger of clinging, of striving,

164
00:16:50,000 --> 00:17:06,000
of wishing and wanting and clinging and liking and disliking.

165
00:17:06,000 --> 00:17:09,000
So this is just one example of that.

166
00:17:09,000 --> 00:17:18,000
When you see these facts of life, the nature of our existence,

167
00:17:18,000 --> 00:17:22,000
and really learning to overcome it,

168
00:17:22,000 --> 00:17:27,000
seeing the disturbing aspects of life, seeing unpleasantness,

169
00:17:27,000 --> 00:17:36,000
seeing a non-beautiful, the asumba side of life,

170
00:17:36,000 --> 00:17:39,000
has a challenge for us in learning to overcome it.

171
00:17:39,000 --> 00:17:42,000
This is an important part of our practice.

172
00:17:42,000 --> 00:17:47,000
So important for us to keep in mind to be open to this.

173
00:17:47,000 --> 00:17:51,000
So it's not to say that deep and tranquil meditation is not useful,

174
00:17:51,000 --> 00:17:54,000
or that being in a meditation center is not useful.

175
00:17:54,000 --> 00:17:56,000
There's another reason why we push meditators,

176
00:17:56,000 --> 00:18:01,000
why we can't let you just stay here and be at peace meditating.

177
00:18:01,000 --> 00:18:07,000
We have to make it difficult for you,

178
00:18:07,000 --> 00:18:10,000
because we need to learn to let go.

179
00:18:10,000 --> 00:18:14,000
If it's comfortable, it's very easy to overestimate yourself

180
00:18:14,000 --> 00:18:22,000
to get complacent and reaffirm your attachment.

181
00:18:22,000 --> 00:18:24,000
The best way to let go is to be challenged

182
00:18:24,000 --> 00:18:28,000
and to come to see, through be reminded,

183
00:18:28,000 --> 00:18:32,000
of these facts of life that we get old sick and die,

184
00:18:32,000 --> 00:18:39,000
that we're hurt, that everything changes reality as chaotic.

185
00:18:39,000 --> 00:18:49,000
That there's always the potential for suffering if we have expectations or attachments.

186
00:18:49,000 --> 00:18:54,000
So another good verse, an interesting story.

187
00:18:54,000 --> 00:19:01,000
There's one of these interesting ones where the story itself actually has more than the verse offers.

188
00:19:01,000 --> 00:19:06,000
The verse is, of course, a point in a reminder that we're all going to get old sick and die.

189
00:19:06,000 --> 00:19:10,000
That this body is just a bundle of bones.

190
00:19:10,000 --> 00:19:13,000
That's something to cling to.

191
00:19:13,000 --> 00:19:15,000
There you go.

192
00:19:15,000 --> 00:19:17,000
That's the number part of her today.

193
00:19:17,000 --> 00:19:19,000
Thank you all for tuning in.

194
00:19:19,000 --> 00:19:44,000
Wish you all the best.

