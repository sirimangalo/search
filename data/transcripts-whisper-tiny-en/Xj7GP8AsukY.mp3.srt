1
00:00:00,000 --> 00:00:07,080
With the practice of mental noting, for example, seeing, seeing, seeing,

2
00:00:07,080 --> 00:00:13,520
how should one prioritize what to single out to note?

3
00:00:13,520 --> 00:00:18,160
For example, a person is incessantly breathing,

4
00:00:18,160 --> 00:00:28,000
so theoretically the noting could always reduce back to that lowest common denominator.

5
00:00:28,000 --> 00:00:36,000
On the other hand, one could get very specific and go so far as to note in great detail.

6
00:00:36,000 --> 00:00:43,000
An example, seeing around orange citrus fruit.

7
00:00:43,000 --> 00:00:46,000
Yeah, I could start with it.

8
00:00:46,000 --> 00:00:48,000
Good for you.

9
00:00:48,000 --> 00:00:52,000
Otherwise, I'm going to give it to him.

10
00:00:52,000 --> 00:00:57,000
You should always note what is the most prominent in that present moment.

11
00:00:57,000 --> 00:01:06,000
That might change because our mind is not always the same.

12
00:01:06,000 --> 00:01:13,000
It's not always clear, so it is very impermanent and changing.

13
00:01:13,000 --> 00:01:19,000
So one day you can just say, seeing, seeing, seeing,

14
00:01:19,000 --> 00:01:31,000
because you see something, and one day you can, or one hour later, even you can be more specific.

15
00:01:31,000 --> 00:01:40,000
But as the example that you give, seeing around orange citrus fruit is not the point.

16
00:01:40,000 --> 00:01:51,000
This is giving things a name, giving things a name that it does need,

17
00:01:51,000 --> 00:01:59,000
because seeing is what you do with the rest is concept.

18
00:01:59,000 --> 00:02:13,000
Where the point where you can be more specific would be in noting what comes after the seeing, for example.

19
00:02:13,000 --> 00:02:16,000
If they're seeing, does the feeling arise?

20
00:02:16,000 --> 00:02:20,000
And when a feeling arise, what kind of feeling is it?

21
00:02:20,000 --> 00:02:31,000
So they're seeing, then you note feeling of, because you saw citrus fruit, you are thirsty, for example, or hungry.

22
00:02:31,000 --> 00:02:34,000
And then you note this.

23
00:02:34,000 --> 00:02:48,000
And then you feel the wanting, and you note this, and it might go further that you want to get up to get it.

24
00:02:48,000 --> 00:03:00,000
So all these little details that follow after the seeing should be noticed as well, if you are able to see them.

25
00:03:00,000 --> 00:03:03,000
Yeah, that's it.

26
00:03:03,000 --> 00:03:12,000
I don't have much to add. That was pretty much what I would have said.

27
00:03:12,000 --> 00:03:23,000
Yeah, I was going to kind of say that it may be a wrong framework or the wrong way of looking at this as far as getting specific or seeing general.

28
00:03:23,000 --> 00:03:30,000
As Pa and Yany said, it's a very simple answer that we always give to meditators, whatever's clear, go for that.

29
00:03:30,000 --> 00:03:32,000
And that's what will change.

30
00:03:32,000 --> 00:03:36,000
Sometimes A is clear, sometimes B is clear.

31
00:03:36,000 --> 00:03:43,000
And it's not a trick, it's not a trick question in that sense.

32
00:03:43,000 --> 00:03:47,000
You've got two doors, pick a door, and one of them is right, and one of them is wrong.

33
00:03:47,000 --> 00:03:50,000
If they're both just as clear, well, just pick one.

34
00:03:50,000 --> 00:03:55,000
There was one meditator, a long time ago at Jamthong.

35
00:03:55,000 --> 00:04:01,000
He practiced with me at Deusu Tep, and then I couldn't answer his questions to his satisfaction.

36
00:04:01,000 --> 00:04:08,000
So he went to Jamthong to be with Ad Jamthong, who was of course, could of course answer all of his questions.

37
00:04:08,000 --> 00:04:11,000
Nope. Same question, same answer.

38
00:04:11,000 --> 00:04:12,000
And he still wasn't satisfied.

39
00:04:12,000 --> 00:04:15,000
And I heard him ask the same question.

40
00:04:15,000 --> 00:04:19,000
It was when you're walking, should you stay with the feet?

41
00:04:19,000 --> 00:04:22,000
Or should you note the things that come into your mind?

42
00:04:22,000 --> 00:04:30,000
So when you're with the feet and you think something, should you stop and say to yourself thinking, thinking, thinking,

43
00:04:30,000 --> 00:04:32,000
or should you keep walking?

44
00:04:32,000 --> 00:04:34,000
That was his question.

45
00:04:34,000 --> 00:04:38,000
And so I think maybe my answer wasn't correct.

46
00:04:38,000 --> 00:04:41,000
I can't remember, but I think my answer was the same.

47
00:04:41,000 --> 00:04:43,000
There's two ways.

48
00:04:43,000 --> 00:04:49,000
You can either stop and say to yourself thinking, thinking, thinking, or you can just ignore it and go back to the feet.

49
00:04:49,000 --> 00:04:54,000
That's exactly what Ad Jamthong told me, or had said when I was present.

50
00:04:54,000 --> 00:05:00,000
So I told him that he wasn't satisfied and he asked this question and this question.

51
00:05:00,000 --> 00:05:04,000
You know, God totally got him stuck, this very simple question.

52
00:05:04,000 --> 00:05:10,000
And so he went to see Ad Jamthong and he must have asked Ad Jamthong the same question two or three times.

53
00:05:10,000 --> 00:05:20,000
And Ad Jamthong, you know, tried to shake him up a little bit and always very around, in a very roundabout way.

54
00:05:20,000 --> 00:05:25,000
Because he doesn't want to say, stop asking me dumb questions and Ad Jamthong would never do that.

55
00:05:25,000 --> 00:05:33,000
But he didn't get the point and eventually he was among eventually he disrobed and went home over the silly question.

56
00:05:33,000 --> 00:05:42,000
And because he wanted, you know, we're used to in the West, we're used to having, you know, rules and laws and logic and cut and dry.

57
00:05:42,000 --> 00:05:53,000
But meditation is more like a boxing match. You kind of have to learn, you have to learn a lot of tricks.

58
00:05:53,000 --> 00:05:59,000
Sometimes it's not just about punching and blocking. It's not theory, it's practice.

59
00:05:59,000 --> 00:06:04,000
And when you practice, you know, sometimes sometimes you have to fight dirty.

60
00:06:04,000 --> 00:06:11,000
So if you have lots of lust, you have to stop practicing and go and practice the mindfulness of the parts of the body.

61
00:06:11,000 --> 00:06:18,000
If you have lots of anger, sometimes the same thing, you have to fight dirty and practice loving kindness instead of repossing them, for example.

62
00:06:18,000 --> 00:06:27,000
This kind of thing. When you feel no energy in the practice, feel kind of lazy and like, you know, I don't want to practice.

63
00:06:27,000 --> 00:06:29,000
What is the word?

64
00:06:29,000 --> 00:06:37,000
Not lethargic. It's like lack of ambition, lack of get up and go.

65
00:06:37,000 --> 00:06:43,000
There's a word for it. The theory we were talking of last week.

66
00:06:43,000 --> 00:06:50,000
When you feel like you don't have, have, have chanda, but I don't know what the English word I'm looking for is.

67
00:06:50,000 --> 00:06:57,000
It's not torpor. It's like you don't, you feel like you don't, not ambition.

68
00:06:57,000 --> 00:07:06,000
It's something like ambition. Anyway, like meditate again and you feel kind of better I go to the, better I go watch a movie or something like that.

69
00:07:06,000 --> 00:07:20,000
Anyway, long and short, then you practice mindfulness of death and remind yourself, if I'm like this, I'm going to, I'm going to die, I'm going to, I think pamada is the poly word that I'm looking for.

70
00:07:20,000 --> 00:07:28,000
You feel heedless where you feel like you're getting negligent, negligent.

71
00:07:28,000 --> 00:07:31,000
Lack of determination. That's a good one.

72
00:07:31,000 --> 00:07:40,000
So to develop your determination, you practice mindfulness that these are the kind of dirty tricks for an example.

73
00:07:40,000 --> 00:07:44,000
The point I'm trying to make is that there's lots of little tricks that you have to learn.

74
00:07:44,000 --> 00:07:49,000
Suppose you're doing walking meditation and you feel really, really distracted.

75
00:07:49,000 --> 00:07:54,000
Well, then you have this trick that you instead you go and sit down and if you still feel distracted, you go and lie down.

76
00:07:54,000 --> 00:07:58,000
Whereas lying meditation we would never have people do.

77
00:07:58,000 --> 00:08:07,000
And if you're sitting down and you feel tired and want to go to sleep, get up and do walking meditation, for example, these kind of tricks.

78
00:08:07,000 --> 00:08:16,000
So the, how this relates to your question, the point being that there are no hard and fast rules.

79
00:08:16,000 --> 00:08:19,000
In this regard, it's not a magic trick.

80
00:08:19,000 --> 00:08:27,000
It's not like, if you note so many times in the right succession, like one of those video games where if you push the right buttons, you get a special combo.

81
00:08:27,000 --> 00:08:30,000
Attack or something like that.

82
00:08:30,000 --> 00:08:33,000
It's not like that. It's a learning experience.

83
00:08:33,000 --> 00:08:35,000
You're trying to learn about reality.

84
00:08:35,000 --> 00:08:40,000
So rather than worrying about should I be noting in this way, should I be noting in that way?

85
00:08:40,000 --> 00:08:43,000
Focus on the worry, focus on the doubt.

86
00:08:43,000 --> 00:09:01,000
It's, that's your meditation. This doubting is where your attention should be as we've talked about before.

