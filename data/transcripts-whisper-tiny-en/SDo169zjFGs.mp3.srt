1
00:00:00,000 --> 00:00:05,000
.

2
00:00:05,000 --> 00:00:10,000
.

3
00:00:10,000 --> 00:00:15,000
.

4
00:00:15,000 --> 00:00:20,000
.

5
00:00:20,000 --> 00:00:25,000
.

6
00:00:25,000 --> 00:00:28,000
.

7
00:00:28,000 --> 00:00:33,000
.

8
00:00:33,000 --> 00:00:34,000
.

9
00:00:34,000 --> 00:00:39,000
Good evening, everyone.

10
00:00:39,000 --> 00:00:45,000
Welcome to Evening Dumber.

11
00:00:45,000 --> 00:00:50,000
Welcome back.

12
00:00:50,000 --> 00:00:57,000
Today we're continuing on studying the Satipatana-Sutra.

13
00:00:57,000 --> 00:01:07,000
We're in what is called Sampaganya, Sampaganya Baba, or Sampaganya Baba.

14
00:01:07,000 --> 00:01:11,000
Baba means sections, you know.

15
00:01:11,000 --> 00:01:16,000
What we're looking at is Sampaganya.

16
00:01:16,000 --> 00:01:28,000
This concept of Sampaganya, which a lot is made of, made of this word, but in many ways it's

17
00:01:28,000 --> 00:01:42,000
just another way of describing mindfulness or describing the effects of mindfulness.

18
00:01:42,000 --> 00:01:50,000
When we talk about Sati, Sati is the act of grasping the object, often they're paired together

19
00:01:50,000 --> 00:01:58,000
Sati and Sampaganya, or sometimes it's just Sati and Bhanya, Satipanya.

20
00:01:58,000 --> 00:02:05,000
Sampaganya really is just a fancy way of talking about wisdom.

21
00:02:05,000 --> 00:02:18,760
And not in a good way, because sometimes if you just say wisdom, wisdom, wisdom, it loses

22
00:02:18,760 --> 00:02:27,600
the meaning, it just becomes a catch word, a buzz word, but when you say Bhanya, Bhanya

23
00:02:27,600 --> 00:02:38,480
just means wisdom, but when you say full and complete understanding, it has a little bit

24
00:02:38,480 --> 00:02:45,280
more of a meaning to it.

25
00:02:45,280 --> 00:02:49,280
But it really means, it's what we mean when we say wisdom, but what do we mean by wisdom

26
00:02:49,280 --> 00:03:03,240
means full and complete knowledge, or intensive, the commentary talks about Sampaganya

27
00:03:03,240 --> 00:03:11,680
as being continuous knowledge, always knowing.

28
00:03:11,680 --> 00:03:20,680
So it can be interpreted in different ways.

29
00:03:20,680 --> 00:03:27,280
But it means knowledge, it means this special knowing, but it means it's not intellectual

30
00:03:27,280 --> 00:03:28,280
knowing.

31
00:03:28,280 --> 00:03:36,760
It means really being aware of something, again it's this, well the commentary describes

32
00:03:36,760 --> 00:03:42,280
four ways of understanding, four types of Sampaganya, only one of which we're actually

33
00:03:42,280 --> 00:03:49,680
interested in, but in order to give an idea of what the word means or how it's used.

34
00:03:49,680 --> 00:04:01,400
So the first is Atasampaganya, knowledge of full and complete knowledge of purpose.

35
00:04:01,400 --> 00:04:14,720
So it kind of means having wisdom about purpose, or wisdom because of knowing the purpose.

36
00:04:14,720 --> 00:04:22,760
So you have this clear state of mind because you're sure why you're doing something.

37
00:04:22,760 --> 00:04:29,080
So it means knowing the difference between purpose and purpose useful and not useful

38
00:04:29,080 --> 00:04:42,640
purpose and purposeless, purposeful and purposeless, knowing that doing something has

39
00:04:42,640 --> 00:04:50,120
benefit, has value, knowing that doing something is futile, has no value.

40
00:04:50,120 --> 00:04:54,120
That's one of the big things, I mean this in regards to the dhamma, it's one of the big

41
00:04:54,120 --> 00:05:02,160
things that we learn in Buddhism, learning that killing is not useful, stealing is not

42
00:05:02,160 --> 00:05:12,320
useful, learning that ambition is not useful, learning that addiction is not useful attachment

43
00:05:12,320 --> 00:05:16,800
is not useful.

44
00:05:16,800 --> 00:05:22,960
And people who practice meditation intensively their lives begin to change, they stop

45
00:05:22,960 --> 00:05:31,800
getting caught up in so much complex activity because they start to see it's not useful.

46
00:05:31,800 --> 00:05:35,200
That's not the sampaganya we're talking about in the sutta, but that's a way that it's

47
00:05:35,200 --> 00:05:36,200
used.

48
00:05:36,200 --> 00:05:40,680
It's a type of wisdom.

49
00:05:40,680 --> 00:05:49,200
The second type is Sapaya sampaganya, wisdom about suitability.

50
00:05:49,200 --> 00:05:54,800
So suitability is a bit different than purpose, it means something might be useful, but

51
00:05:54,800 --> 00:05:57,920
is it useful for me?

52
00:05:57,920 --> 00:06:04,280
This is important, there are some things that are just useless, means no one should engage

53
00:06:04,280 --> 00:06:11,200
in them, greed, anger, delusion, to name the basic ones.

54
00:06:11,200 --> 00:06:18,520
But there are some things that are just unsuitable for an individual.

55
00:06:18,520 --> 00:06:24,920
Things are suitable for all individuals, sati is one, practice of mindfulness.

56
00:06:24,920 --> 00:06:28,320
You can never say, well, I don't think this is suitable.

57
00:06:28,320 --> 00:06:34,800
But for some people, different environments are suitable, maybe Sudhi Manga goes into some

58
00:06:34,800 --> 00:06:37,080
fairly practical examples.

59
00:06:37,080 --> 00:06:43,360
For someone who is of greedy temperament, you should put them in a dirty room or a plain

60
00:06:43,360 --> 00:06:52,880
and simple room with broken bed and give them a broken bowl and give them crappy food.

61
00:06:52,880 --> 00:06:56,600
Someone who is greedy should have all sorts of bad stuff.

62
00:06:56,600 --> 00:07:00,920
But if someone is of angry temperament, well, you should give them the nicest room and

63
00:07:00,920 --> 00:07:12,960
good food and a pleasant view, maybe give them all sorts of nice stuff.

64
00:07:12,960 --> 00:07:16,080
The counter, their inherent negativity.

65
00:07:16,080 --> 00:07:23,080
For someone who is deluded, I think you shouldn't give them a big room, because their

66
00:07:23,080 --> 00:07:24,640
mind will be able to expand.

67
00:07:24,640 --> 00:07:26,840
You shouldn't give them a view.

68
00:07:26,840 --> 00:07:30,360
You shouldn't give them a small room or something like that.

69
00:07:30,360 --> 00:07:31,360
There's all detail.

70
00:07:31,360 --> 00:07:39,520
It's quite interesting in the ways if you really want to get into it.

71
00:07:39,520 --> 00:07:52,240
The commentary here gives an example of a person who is a person who is contemplating

72
00:07:52,240 --> 00:08:01,920
a dead corpse.

73
00:08:01,920 --> 00:08:07,200
So if you're a male, you shouldn't contemplate a female corpse, because it's not the

74
00:08:07,200 --> 00:08:14,560
point, it goes against the purpose of seeing loads ofness of the body, arguably.

75
00:08:14,560 --> 00:08:20,200
But arguably, it might help if you see a woman become bloated and disgusting.

76
00:08:20,200 --> 00:08:28,560
The Buddha used this, no, he didn't use it for a woman, but the commentary does say a man

77
00:08:28,560 --> 00:08:37,800
should not reflect upon a female corpse and a woman shouldn't contemplate on a male corpse.

78
00:08:37,800 --> 00:08:39,760
That would be unsuitable.

79
00:08:39,760 --> 00:08:42,800
For loving kindness, it certainly works that way.

80
00:08:42,800 --> 00:08:48,000
Men should do much better if they're cultivating the Janas.

81
00:08:48,000 --> 00:08:56,200
They should focus on men, male individuals, focus their kindness on them, otherwise if they're

82
00:08:56,200 --> 00:09:03,200
heterosexual, they don't focus on something that's going to be the object of your attachment,

83
00:09:03,200 --> 00:09:08,600
depending on what is your proclivity.

84
00:09:08,600 --> 00:09:13,160
The third type of sampa janya is gojara sampa janya.

85
00:09:13,160 --> 00:09:25,440
Gojara means pasture, go means a cow, and jara means walking, or a place of pasture

86
00:09:25,440 --> 00:09:26,440
really.

87
00:09:26,440 --> 00:09:29,360
So it's cow pasture, it's gojara.

88
00:09:29,360 --> 00:09:31,440
Cow is having a great significance in India.

89
00:09:31,440 --> 00:09:41,360
The word took on greater meaning and it means resort, so knowledge of the sort of people

90
00:09:41,360 --> 00:09:49,400
you should hang out with, the sort of places you should go, the sort of wisdom about this.

91
00:09:49,400 --> 00:09:53,520
The fourth type of sampa janya is the one we're looking at here, the commentary acknowledges

92
00:09:53,520 --> 00:09:59,400
that none of these are the type of sampa janya that we're talking about here.

93
00:09:59,400 --> 00:10:06,680
In this section, we're going to talk about sampa janya, it's called asamoha sampa janya.

94
00:10:06,680 --> 00:10:10,880
Samoha means that which partakes in wisdom.

95
00:10:10,880 --> 00:10:20,840
Asamoha means that which does not partake in sorry, asamoha is that which does not

96
00:10:20,840 --> 00:10:32,080
partake of delusion or in delusion, which means wisdom, the full and complete knowledge

97
00:10:32,080 --> 00:10:44,840
that has to do with wisdom, or has to do with non-ignerance, non-delusion, non-confusion.

98
00:10:44,840 --> 00:10:53,600
In this particular, the usage here means specifically, this isn't a conventional sort of wisdom.

99
00:10:53,600 --> 00:10:59,480
The other three are conventional, they come generally from meditation practice, but they

100
00:10:59,480 --> 00:11:09,400
can also come from theory, from rationalization or logic, rational thinking or logic.

101
00:11:09,400 --> 00:11:16,000
But this one is asamoha, this means in a true, full, complete knowledge.

102
00:11:16,000 --> 00:11:25,160
This is where one comes to know unshakably directly without any intermediary.

103
00:11:25,160 --> 00:11:31,440
From one's meditation practice.

104
00:11:31,440 --> 00:11:36,680
So what we're talking about here is this experience of the meditator who knows what's

105
00:11:36,680 --> 00:11:43,800
happening, where suddenly they're transported from the mental intellectual activity to the

106
00:11:43,800 --> 00:11:48,680
awareness, now I'm sitting, now I'm walking, now I'm standing.

107
00:11:48,680 --> 00:11:55,480
Not intellectual thinking it, but it's the best, the best approximation that we can put

108
00:11:55,480 --> 00:11:56,480
into words.

109
00:11:56,480 --> 00:12:02,880
It's this knowing and full and complete knowledge, not just some wavering knowledge that

110
00:12:02,880 --> 00:12:11,600
everyone has, oh yes, I'm going to walk now, no walking, being aware of the movement.

111
00:12:11,600 --> 00:12:15,920
And so it's quite simple, it goes through just about everything, but it's interesting to

112
00:12:15,920 --> 00:12:21,760
remind ourselves that this isn't just a practice for the sitting mat.

113
00:12:21,760 --> 00:12:32,600
Saty is something that for the meditation to progress and succeed has to be cultivated everywhere.

114
00:12:32,600 --> 00:12:45,760
So it says bikhu, a bikhu, a bikhu, a bikhu, a bikante, patikante, sampajana-gari-hoti.

115
00:12:45,760 --> 00:12:56,240
And going forward and coming back and walking back and forth, sampajana-gari-hoti, one is

116
00:12:56,240 --> 00:13:03,960
a person who does or cultivates or makes, makes themselves fully and completely aware.

117
00:13:03,960 --> 00:13:10,200
A commentary describes this, I think is going on arms round and it goes through a long

118
00:13:10,200 --> 00:13:16,000
process of talking about going and coming back, it can also just be walking on a walking

119
00:13:16,000 --> 00:13:23,640
path when you're walking and forward and back.

120
00:13:23,640 --> 00:13:28,520
But the idea here is outside of the meditation practice, so when you leave your room to

121
00:13:28,520 --> 00:13:34,000
come up here to listen to the talk, when you go back to your room, that kind of thing.

122
00:13:34,000 --> 00:13:40,680
When you go out for food, when you go back for food, walking this way and that way is maybe

123
00:13:40,680 --> 00:13:42,880
what it means.

124
00:13:42,880 --> 00:13:48,680
A low-key-day-ri-low-key-day, when looking forward, when turning to look around, when it's

125
00:13:48,680 --> 00:13:50,720
mindful.

126
00:13:50,720 --> 00:13:57,720
Samanji-day-pasa-ri-day, when one extends one's arm, when one flexes one's arm, for example,

127
00:13:57,720 --> 00:14:08,080
you know, reaching, pulling.

128
00:14:08,080 --> 00:14:14,640
Sanga-ti-pata-ji-wara-dharani-sampajana-kari-hoti.

129
00:14:14,640 --> 00:14:23,800
When one is carrying one's coat, some sanga-ti is the monk's coat, but the bull-ji-wara-dhar

130
00:14:23,800 --> 00:14:26,880
robes carrying.

131
00:14:26,880 --> 00:14:33,680
When one is carrying something, carrying one's bull and robes, really, for monks, those

132
00:14:33,680 --> 00:14:34,680
are the belongings.

133
00:14:34,680 --> 00:14:38,800
So when you go to pick up your robes, picking it up mindfully, these things that we have

134
00:14:38,800 --> 00:14:44,760
to do on a daily basis, doing it with mindfulness, with clear awareness.

135
00:14:44,760 --> 00:14:51,960
So your mind isn't somewhere else, your mind is there too, when you're carrying something,

136
00:14:51,960 --> 00:14:54,600
carrying your bull on arms round.

137
00:14:54,600 --> 00:14:59,680
You are there with it, the mind is there too, it's not the body carrying and the mind

138
00:14:59,680 --> 00:15:11,000
is doing something else.

139
00:15:11,000 --> 00:15:27,160
In regards to what is eaten and drunk, swallowed, tasted sampajana-kari-hoti, when eating

140
00:15:27,160 --> 00:15:35,800
and drinking, eating meditation is a wonderful thing, encourage it for all meditators and

141
00:15:35,800 --> 00:15:42,600
people interested in mindfulness meditation, chewing, chewing, swallowing, you've got nothing

142
00:15:42,600 --> 00:15:48,400
better to do, why don't we actually be there when we eat, instead of eating and letting

143
00:15:48,400 --> 00:15:54,440
our minds wander and get preoccupied just with the pleasure of eating, why don't we actually

144
00:15:54,440 --> 00:16:04,640
be there, chewing, chewing, it's quite a enlightening experience.

145
00:16:04,640 --> 00:16:14,480
And here's my favorite one, I think, is Uchara-pa-sawa-kami-sampajana-kari-hoti, favorite, I mean it's

146
00:16:14,480 --> 00:16:24,280
the most interesting one, Uchara is defecating, pa-sawa is urinating, in the act of urinating

147
00:16:24,280 --> 00:16:31,160
and defecating, sampajana-kari-hoti, I mean it shouldn't be any surprise, but that it's

148
00:16:31,160 --> 00:16:44,560
in the text is quite revealing, urinating meditation, defecating meditation, we actually

149
00:16:44,560 --> 00:16:51,920
go there, when we say everywhere, we mean everywhere, being mindful the feelings of urinating

150
00:16:51,920 --> 00:17:08,080
and defecating, mindful the sounds and the smells, gate-te-te-te-ne-sine-ne-su-te-jag-ri-te-basi-te-tuni-bawi,

151
00:17:08,080 --> 00:17:23,200
when walking, when standing, when sitting, when lying, when awake, when waking up, when

152
00:17:23,200 --> 00:17:31,920
speaking, when staying silent, speaking basi-te, are you aware of your lips moving, are

153
00:17:31,920 --> 00:17:37,800
you mindful of the thoughts and emotions going through your mind when you talk, it's

154
00:17:37,800 --> 00:17:44,160
a difficult one, but are you here, are you present?

155
00:17:44,160 --> 00:17:51,160
If not, when you're not, you're not meditating, it's quite revealing to think, this isn't

156
00:17:51,160 --> 00:18:02,440
the one-hour-a-day thing, for a person who is dedicated to the practice, it's a many, it's

157
00:18:02,440 --> 00:18:11,440
an 18-hour thing, at least, if not more, remember I tell this story often of something

158
00:18:11,440 --> 00:18:16,440
in for another teacher, and they had the meditators do 8 hours a day, 10 hours a day, and

159
00:18:16,440 --> 00:18:22,440
the meditators would come back so stressed, some of them, oh, I was able to do, today

160
00:18:22,440 --> 00:18:27,800
I did 10 hours a day, it was hard, but I did it, and this meditator said this to me and

161
00:18:27,800 --> 00:18:34,600
I said, okay, today I want you to do 18 hours a day with meditation, when this eye is

162
00:18:34,600 --> 00:18:41,480
almost, I remember his eyes just, it was unfathomable, it was dreading what I was going

163
00:18:41,480 --> 00:18:50,440
to tell him in 18 hours, who is this new guy, and what's he going to do, and explain

164
00:18:50,440 --> 00:18:55,200
to him, you know, I don't care how much form of meditation you do, but try to be mindful

165
00:18:55,200 --> 00:19:03,000
when you wake up, this isn't an hours thing, this is an always thing, be mindful, jagari

166
00:19:03,000 --> 00:19:09,760
day when waking up, lying, when you want to sit up wanting to sit sitting and I described

167
00:19:09,760 --> 00:19:15,800
to him how to be mindful, standing, walking, sitting, bending, stretching, reaching,

168
00:19:15,800 --> 00:19:23,440
pulling, brushing your teeth, eating your food, showering, urinating, defecating, being

169
00:19:23,440 --> 00:19:39,040
mindful, it's an all the time thing, which is really a, I mean this is a good example

170
00:19:39,040 --> 00:19:49,400
of why the monastic life is so important, not for everyone, but important to have as a means

171
00:19:49,400 --> 00:19:58,040
of dedicating yourself in this way, when you really want to learn how to live, right,

172
00:19:58,040 --> 00:20:03,240
you don't want to go out and live, you want to learn how to live, mostly we do it the other

173
00:20:03,240 --> 00:20:07,960
way, we go out and live and we think we'll learn how to do it along the way, which, well

174
00:20:07,960 --> 00:20:12,880
doesn't work out usually the way we planned because you don't know how you're doing it

175
00:20:12,880 --> 00:20:19,360
all wrong, and you're building bad habits, so going off into the forest or an empty

176
00:20:19,360 --> 00:20:26,640
place, a monastery, a lack of a better word, a place where there are a bunch, a bunch of

177
00:20:26,640 --> 00:20:33,440
other beggars who don't beg but see the danger of getting ambitious and caught up in some

178
00:20:33,440 --> 00:20:42,320
sara and dwelling with them and learning how to live, this is what the monastic life is

179
00:20:42,320 --> 00:20:54,520
for, and that's the section on some Virginia, so going, getting right along, that's the

180
00:20:54,520 --> 00:21:08,000
demo for tonight, let's look at questions, six questions today, how do you be more mindful

181
00:21:08,000 --> 00:21:14,240
during day-to-day activities, I seem to often forget my reasons for things fail to be present,

182
00:21:14,240 --> 00:21:20,360
well the best way is to do an intensive meditation course, in all ways because it, again,

183
00:21:20,360 --> 00:21:24,800
teaches you how to live rather than having to go out and live and try to work it out,

184
00:21:24,800 --> 00:21:32,120
but you're here, you don't have anything better to do, so being mindful is, it's an easy

185
00:21:32,120 --> 00:21:41,720
thing to cultivate, but failing that, failing at doing an intensive course, well, again,

186
00:21:41,720 --> 00:21:48,960
very much as I just said, you learn how to be mindful of just about everything you do,

187
00:21:48,960 --> 00:21:58,040
some dingy day-to-day stretching, flexing, everything you do, does insight lead to tranquil

188
00:21:58,040 --> 00:22:07,000
states, yes, insight leads to the ultimate tranquility of nibana, which, of course, leads

189
00:22:07,000 --> 00:22:13,840
to mundane tranquil states as well, what is the meaning of knowing mind, do we have to mentally

190
00:22:13,840 --> 00:22:18,120
recite rising and falling, or is it just to be aware, well, the problem is if you're

191
00:22:18,120 --> 00:22:24,440
not reciting rising and falling, it's very hard to just be aware, right, without that

192
00:22:24,440 --> 00:22:39,080
practice of grasping, of concretely augmenting the mind, it's very hard to just be aware,

193
00:22:39,080 --> 00:22:43,640
that's the whole point, you know, and people go the other way and say, we're just going

194
00:22:43,640 --> 00:22:47,440
to, so we don't have to use these words, we're just going to be aware, well, if you're

195
00:22:47,440 --> 00:22:57,880
not, if the mantra is in present, that's what the mantra is for, what is a Buddhist point

196
00:22:57,880 --> 00:23:04,080
of view on love, well, there are many kinds of love, it's really just a word and it means

197
00:23:04,080 --> 00:23:09,000
different things to different people in different contexts, what does Buddhism say about

198
00:23:09,000 --> 00:23:12,720
love of another in a relationship, is it wrong because of clinging, well, the love isn't

199
00:23:12,720 --> 00:23:17,600
wrong but the clinging is technically wrong because the clinging is what's going to

200
00:23:17,600 --> 00:23:23,520
lead to suffering, the love won't, don't they love everyone equally, yes, loving everyone

201
00:23:23,520 --> 00:23:30,960
equally means not preferring one person over another unless not being upset when you can't

202
00:23:30,960 --> 00:23:37,800
be with the one you are attached to, so again, love and attachment from a Buddhist point

203
00:23:37,800 --> 00:23:44,560
of view are two very different things. As a byproduct, as we pass in and make you a

204
00:23:44,560 --> 00:23:51,120
more creative person, not a smart person, I mean, in the short term, maybe if you, if

205
00:23:51,120 --> 00:23:57,240
creativity is a part of your ambition, it'll definitely allow you to focus your creativity.

206
00:23:57,240 --> 00:24:02,520
Eventually, I would say, no, it makes you very uncreative because you have no desire to

207
00:24:02,520 --> 00:24:09,160
create anymore, but that's sort of more long term. Eventually, you have no desire for

208
00:24:09,160 --> 00:24:19,440
anything, you're just happy and at peace. But, you know, if you've been created by being

209
00:24:19,440 --> 00:24:25,440
able to see the world completely differently, yeah, absolutely, they're very open minded

210
00:24:25,440 --> 00:24:35,840
to an extent, to some extent, they're not willing to interested in entertaining, speechless

211
00:24:35,840 --> 00:24:42,640
ideas or imaginary situations, but to the extent that they're willing to, they're perfectly

212
00:24:42,640 --> 00:24:50,320
open, perfectly able to imagine things.

213
00:24:50,320 --> 00:24:55,440
Open hours as besides becoming a monk, you can experience bouts of escape from the will to life

214
00:24:55,440 --> 00:25:01,160
through immersing yourself in art and philosophy. What do you think? Again, I don't think

215
00:25:01,160 --> 00:25:08,280
much about Chopin Hour, so I'm going to take the easy way out and just say, yeah, not

216
00:25:08,280 --> 00:25:17,560
my sort of question. Okay, that's all the questions. Thank you all for coming out. Have

217
00:25:17,560 --> 00:25:24,560
a good night.

