1
00:00:00,000 --> 00:00:03,920
Okay, so welcome back to our study of the Dhamapada.

2
00:00:03,920 --> 00:00:07,400
Today we continue on with first number 41,

3
00:00:07,400 --> 00:00:10,400
which reads as follows.

4
00:00:10,400 --> 00:00:16,320
Achirang wata yang gai yu bata vin pate se se tih.

5
00:00:16,320 --> 00:00:26,400
Tuho ape tawin ya no niratang wah kalin karang,

6
00:00:26,400 --> 00:00:29,040
which means

7
00:00:29,040 --> 00:00:33,920
before long indeed achirang wata.

8
00:00:33,920 --> 00:00:42,400
Ayang gai yu this body pate tawin wata se se tih will lie on the earth.

9
00:00:42,400 --> 00:00:52,960
Tuho tuho means discarded ape tawin ya no niratang without consciousness

10
00:00:52,960 --> 00:00:55,880
or with consciousness having left.

11
00:00:55,880 --> 00:01:04,200
Niratang wata yang wata yang wata niratang wata yang wata niratang wata yang wata.

12
00:01:04,200 --> 00:01:07,200
As useless as a log,

13
00:01:07,200 --> 00:01:12,040
as useless as a dead piece of wood or a log.

14
00:01:12,040 --> 00:01:17,840
So, this is seemingly a little bit depressing,

15
00:01:17,840 --> 00:01:26,600
personal referring to death before long, we will be dead,

16
00:01:26,600 --> 00:01:29,600
as I say,

17
00:01:29,600 --> 00:01:32,800
reminder of the negative aspects of life.

18
00:01:32,800 --> 00:01:37,880
So, this was told in regards to

19
00:01:37,880 --> 00:01:39,880
it's a fairly famous story

20
00:01:39,880 --> 00:01:43,200
and it's a fairly famous verse.

21
00:01:43,200 --> 00:01:50,760
This verse is used as a reminder of the inevitability of death.

22
00:01:50,760 --> 00:01:55,560
And so, it's often used at giving discourses and used in ceremonies

23
00:01:55,560 --> 00:02:03,440
in traditional Buddhism.

24
00:02:03,440 --> 00:02:14,240
So, the story goes that there was a man in sovereignty

25
00:02:14,240 --> 00:02:22,240
who, having listened to the Buddha's teaching, became very convinced

26
00:02:22,240 --> 00:02:25,840
that the Buddha's greatness and the greatness of the teaching

27
00:02:25,840 --> 00:02:27,600
to the point that he decided to go forth.

28
00:02:27,600 --> 00:02:35,000
And it's just urang tatua pambatito gave his heart to give himself up

29
00:02:35,000 --> 00:02:41,720
to the Buddha's sassana and went forth.

30
00:02:41,720 --> 00:02:46,960
Now, in a very short time, he became quite sick

31
00:02:46,960 --> 00:02:49,760
for whatever reason he contracted some sort of illness

32
00:02:49,760 --> 00:02:52,960
and had these boils all over his body,

33
00:02:52,960 --> 00:02:56,000
kind of like the chicken box or something.

34
00:02:56,000 --> 00:03:00,400
And they grew bigger and bigger and believe it says,

35
00:03:00,400 --> 00:03:08,040
eventually, they burst and he was covered in this filthy fluid.

36
00:03:08,040 --> 00:03:12,880
And even eventually, his bones broke,

37
00:03:12,880 --> 00:03:17,800
just some horrible sickness that totally destroyed his body

38
00:03:17,800 --> 00:03:20,640
to the point where he was about to die.

39
00:03:20,640 --> 00:03:27,280
But he became so violent and putrid that the monks didn't want to take care of him.

40
00:03:27,280 --> 00:03:34,120
So, they left him alone and he was lying on his bed in his own urine and feces

41
00:03:34,120 --> 00:03:37,000
without anyone to look after him.

42
00:03:37,000 --> 00:03:44,000
Now, the Buddha, the commentary here, says that the Buddha

43
00:03:44,000 --> 00:03:54,480
it's his nature, it's his way to twice a day to look out

44
00:03:54,480 --> 00:04:01,680
into the world around him to see who would most benefit from his teachings.

45
00:04:01,680 --> 00:04:06,760
So, sometimes he would look out at the whole universe,

46
00:04:06,760 --> 00:04:13,160
looking around through the whole of the earth and the heavens and so on.

47
00:04:13,160 --> 00:04:16,520
And then, all the way back to where he was staying,

48
00:04:16,520 --> 00:04:22,200
or sometimes he would start in the monastery and work his way out.

49
00:04:22,200 --> 00:04:31,320
And whoever he, his mind, alighted upon as someone who could benefit from his teachings,

50
00:04:31,320 --> 00:04:38,520
he would make his way to them and give them a teaching that they needed.

51
00:04:38,520 --> 00:04:46,280
So, on this day, or on one day, when near the point where this monk was getting closer to death,

52
00:04:46,280 --> 00:04:54,000
the Buddha saw him in his mind's eye and knew that besides him there was the Buddha himself.

53
00:04:54,000 --> 00:04:59,960
There was no one else who was willing to be his refuge or who could be his refuge.

54
00:04:59,960 --> 00:05:03,040
And so, he headed out into the monastery.

55
00:05:03,040 --> 00:05:04,960
This was in the same monastery as the Buddha was staying,

56
00:05:04,960 --> 00:05:10,960
and he walked out and he first went to the place where they lit fires.

57
00:05:10,960 --> 00:05:18,960
Especially designated place in the monastery for lighting fires and he lit fire and boiled some water.

58
00:05:18,960 --> 00:05:27,440
And he brought the water to this monk's room and proceeded to,

59
00:05:27,440 --> 00:05:31,240
or began to help the monk to wash himself.

60
00:05:31,240 --> 00:05:34,240
And then, when the monks, of course, when they saw the Buddha come and they said,

61
00:05:34,240 --> 00:05:35,400
I don't know, let us do that.

62
00:05:35,400 --> 00:05:39,600
And so, they helped them with the monks' help.

63
00:05:39,600 --> 00:05:45,040
They were able to take his robes off and wash his robes and wash him and wash the bed,

64
00:05:45,040 --> 00:05:49,680
and lie him back down in a clean robe.

65
00:05:49,680 --> 00:05:52,480
And then, the Buddha gave him this teaching.

66
00:05:52,480 --> 00:05:58,520
So, this is a teaching specifically given to a very sick person.

67
00:05:58,520 --> 00:06:07,880
And you might even think that it's a little bit of a specific teaching.

68
00:06:07,880 --> 00:06:16,520
Dedicated specifically for someone who is sick or someone who is dying.

69
00:06:16,520 --> 00:06:26,680
But I think, I'd like to talk a little bit about this verse and how it relates to our practice.

70
00:06:26,680 --> 00:06:32,280
But I think it's an important verse to understand.

71
00:06:32,280 --> 00:06:40,440
It's important to understand that it has a broader reach than simply dealing with sick people.

72
00:06:40,440 --> 00:06:48,480
This very short and brief teaching is a very brief teaching in the Buddha.

73
00:06:48,480 --> 00:07:00,760
It happens to be one of the major reasons why people begin to practice the Buddha's teaching.

74
00:07:00,760 --> 00:07:06,760
So, we ask ourselves, what was the use of this teaching at that time?

75
00:07:06,760 --> 00:07:10,600
How is it that this teaching helped that monk?

76
00:07:10,600 --> 00:07:14,400
Because according to the text here, he was able to become an arrow hunt as a result of

77
00:07:14,400 --> 00:07:16,360
listening to this teaching in Buddha.

78
00:07:16,360 --> 00:07:21,280
It's this very simple teaching was enough to set him on the path to become enlightened.

79
00:07:21,280 --> 00:07:26,080
So, you would understand that this was the beginning for that monk.

80
00:07:26,080 --> 00:07:34,000
And it's just as it's the beginning for all of us on our path to the realization of the truth.

81
00:07:34,000 --> 00:07:39,040
If we think back to the Buddha's story himself, one of the reasons why the Buddha went

82
00:07:39,040 --> 00:07:46,760
forth is because he saw a very sick person, and he realized that sickness was something

83
00:07:46,760 --> 00:07:49,400
that he himself was subject to as well.

84
00:07:49,400 --> 00:07:53,840
He saw a very old person, he saw a very sick person, he saw a dead person.

85
00:07:53,840 --> 00:07:59,000
And these were three things that moved him to the point of wanting to find a way out

86
00:07:59,000 --> 00:08:08,320
of suffering, to move him to realize that there was no point in pursuing sensual

87
00:08:08,320 --> 00:08:16,560
pleasures or things outside of the body or even pursuing the wellness of the body itself.

88
00:08:16,560 --> 00:08:21,840
Even pursuing life itself, there was no point in trying to extend life because eventually

89
00:08:21,840 --> 00:08:29,160
it would be offered not.

90
00:08:29,160 --> 00:08:36,200
And so, the first important way to understand this first is as an impetus for us to begin

91
00:08:36,200 --> 00:08:37,200
the practice.

92
00:08:37,200 --> 00:08:43,960
So, here we have this monk who was feeling useless, feeling like perhaps that maybe feeling

93
00:08:43,960 --> 00:08:49,880
afraid or feeling great suffering and depression because of his sickness, feeling like

94
00:08:49,880 --> 00:08:59,120
he was now unable to practice the Buddha's teaching, until the Buddha reminded him of

95
00:08:59,120 --> 00:09:03,320
the importance of now of putting the teaching into practice.

96
00:09:03,320 --> 00:09:09,520
And this allowed the monk to approach things in a different way, to begin to look at the

97
00:09:09,520 --> 00:09:16,960
body and the whole world as being not self, that's not belonging to us and to begin to

98
00:09:16,960 --> 00:09:18,480
give up, give it up.

99
00:09:18,480 --> 00:09:24,360
This is how the Buddha began his quest to give up attachment.

100
00:09:24,360 --> 00:09:31,600
And it's a very common reason for people to give up the world or to begin meditation

101
00:09:31,600 --> 00:09:38,160
practices when they get sick or when they realize that sickness, all-day sickness and death

102
00:09:38,160 --> 00:09:40,640
are an inevitable part of life.

103
00:09:40,640 --> 00:09:44,320
For some people this can be a great crisis, the realization that one day they are going

104
00:09:44,320 --> 00:09:49,160
to have to die and all the people that they love and all the things that they care for

105
00:09:49,160 --> 00:09:56,640
are going to leave them.

106
00:09:56,640 --> 00:10:04,080
And it also happens to be the beginning of a much longer teaching which gets to, it's

107
00:10:04,080 --> 00:10:09,480
closer and closer and closer to a direct realization of impermanence, suffering and

108
00:10:09,480 --> 00:10:10,480
non-self.

109
00:10:10,480 --> 00:10:15,280
So this is really the beginning of the practice or one of the important beginnings of people

110
00:10:15,280 --> 00:10:20,000
ask, why should I practice meditation, why should I torture myself or why should I

111
00:10:20,000 --> 00:10:24,920
be concerned about the purification of the mind, about understanding the mind, why should

112
00:10:24,920 --> 00:10:32,760
I be concerned about trying to let go of attachment?

113
00:10:32,760 --> 00:10:45,240
Why shouldn't I keep clinging to these things or pursuing worldly pleasures and so on?

114
00:10:45,240 --> 00:10:52,360
And it's this sort of teaching that really answers this question for us and makes us

115
00:10:52,360 --> 00:10:58,640
ask, it turns us away from our pursuit of central pleasures because you begin to realize

116
00:10:58,640 --> 00:11:06,880
that let alone those things outside of us that we might pursue, you know, the things

117
00:11:06,880 --> 00:11:16,480
outside of us, you know, even our own body, our own being, we can't say that it belongs

118
00:11:16,480 --> 00:11:17,480
to us.

119
00:11:17,480 --> 00:11:22,800
In the end, this is the meaning of should go of it, the vignan, the mind will eventually

120
00:11:22,800 --> 00:11:29,520
be gone from this body, this very body, this very being that we hold to be ours, we

121
00:11:29,520 --> 00:11:31,840
hold to be ourselves.

122
00:11:31,840 --> 00:11:37,320
This one thing that we cling to more than anything in the world, even this one thing is

123
00:11:37,320 --> 00:11:43,960
in no way ours, is in no way under our control, it's not something that we can keep.

124
00:11:43,960 --> 00:11:48,000
It's the best you can say we're renting it at a high price, having to feed and to close

125
00:11:48,000 --> 00:11:54,440
and to care for it and having to suffer a lot because of it, so we're renting it, kind

126
00:11:54,440 --> 00:11:58,160
of like renting this thing because we think it's going to bring us pleasure or we're

127
00:11:58,160 --> 00:12:05,240
trying to find pleasure based with it until we realize, we come to realize that actually

128
00:12:05,240 --> 00:12:11,520
it's not worth the cost, that it's actually not something that is going to bring lasting

129
00:12:11,520 --> 00:12:14,280
peace and happiness.

130
00:12:14,280 --> 00:12:20,040
It's not something that we can ever fix, that we can ever hold on to, in fact, the nature

131
00:12:20,040 --> 00:12:30,920
of reality is nothing can ever possibly be so, there's nothing in this universe which

132
00:12:30,920 --> 00:12:38,840
when clung to will lead to happiness, the nature of reality, of objects, of possessions

133
00:12:38,840 --> 00:12:44,640
is that they are temporary, that they come and they go, that all of the work that we

134
00:12:44,640 --> 00:12:53,800
put into, keeping them and maintaining them, it's in the end, leads to no lasting and

135
00:12:53,800 --> 00:13:01,880
sustainable purpose, due to the change and the inevitability of losing everything, so let

136
00:13:01,880 --> 00:13:07,680
alone all of the things in the world, this very body that we have, it's not even ourselves.

137
00:13:07,680 --> 00:13:16,120
The body is really the key possession, so when we talk about the body is being not tough

138
00:13:16,120 --> 00:13:22,760
as the body is not being belonging to us, we also, in extent by extension, understand the

139
00:13:22,760 --> 00:13:28,160
rest of the universe, the rest of the world to be so, when the body is not subject

140
00:13:28,160 --> 00:13:36,200
to our control, how could we possibly find control and stability in the things outside of

141
00:13:36,200 --> 00:13:38,520
the body?

142
00:13:38,520 --> 00:13:44,760
So starting from here, this is where we begin this movement inwards, this is the first

143
00:13:44,760 --> 00:13:53,400
realization among many realizations, where we come to realize that let alone this inevitability

144
00:13:53,400 --> 00:14:05,400
of death, there's actually the inevitability of change from, on increasingly shorter intervals,

145
00:14:05,400 --> 00:14:11,520
so when I was young, I was one way and now I'm older and then I'm getting older, when

146
00:14:11,520 --> 00:14:20,840
our body changes and when we begin to get sick, when we begin to get old, when our physical

147
00:14:20,840 --> 00:14:29,400
appearance or our physical nature changes, when the mind changes, sometimes when we find

148
00:14:29,400 --> 00:14:33,800
ourselves becoming addicted to something or attached to something or we find ourselves

149
00:14:33,800 --> 00:14:39,240
the mind changing, maybe forgetting things more and so on, the change that comes in the

150
00:14:39,240 --> 00:14:45,000
mind will happen in the body and the mind will occur many times or for a course of our lives

151
00:14:45,000 --> 00:14:49,760
and eventually getting shorter and shorter and shorter until we realize that actually

152
00:14:49,760 --> 00:14:56,840
this idea of death is really just the tip of the iceberg and actually death is something

153
00:14:56,840 --> 00:15:03,320
that occurs at every moment, this teaching is something that begins us on this course to

154
00:15:03,320 --> 00:15:09,800
look inward until eventually we see that every moment we're born and dying, every moment

155
00:15:09,800 --> 00:15:20,680
things are changing, there is no reality that exists more than one moment, an ordinary

156
00:15:20,680 --> 00:15:27,040
person when they're sitting for all of us, when we come to practice meditation, at first

157
00:15:27,040 --> 00:15:33,280
we see the body as being an entity, when we think of it as being a stable lasting entity

158
00:15:33,280 --> 00:15:41,640
and an ordinary person who was never thought about these things will even, will generally

159
00:15:41,640 --> 00:15:45,320
give rise to this idea that this is something that is going to last, something that I can

160
00:15:45,320 --> 00:15:52,920
depend upon and they will become negligent in terms of clinging to it, they will expect

161
00:15:52,920 --> 00:15:57,440
it to be there the next moment, the next day, the next month, the next year and they

162
00:15:57,440 --> 00:16:02,000
will plan things out so people will have plans for next, for tomorrow, next month, next

163
00:16:02,000 --> 00:16:08,520
year, even many years off into the future, they have a retirement plan, they have very,

164
00:16:08,520 --> 00:16:19,400
very long, having grandchildren and just plans like this and then this realization comes

165
00:16:19,400 --> 00:16:24,280
that eventually you're going to die and for some people it comes quite quickly suddenly

166
00:16:24,280 --> 00:16:28,400
they get sick and the doctors says they have a month to live and this is an incredible

167
00:16:28,400 --> 00:16:37,040
shock that changes their whole outlook suddenly from years, months or years it becomes

168
00:16:37,040 --> 00:16:45,720
days, what am I going to do tomorrow and realizing that you can't cling to things beyond

169
00:16:45,720 --> 00:16:53,760
maybe the next few days or the next few weeks or even months, that's how we begin to come

170
00:16:53,760 --> 00:16:58,120
and practice meditation with these sorts of realizations, realizing that we can't possibly

171
00:16:58,120 --> 00:17:04,000
find satisfaction and in the end we do have to come to terms with change, with the inevitability

172
00:17:04,000 --> 00:17:07,880
of losing the things that we cling to.

173
00:17:07,880 --> 00:17:16,160
Once we begin to practice meditation, we take this the next step of starting to see

174
00:17:16,160 --> 00:17:20,880
the mind is changing for a moment to moment, the body is changing for a moment, even sitting

175
00:17:20,880 --> 00:17:29,240
here, our experience determines the reality, so the reality is not this body lasting from

176
00:17:29,240 --> 00:17:35,600
one moment to the moment, not even from moment to moment, that alone from day to day

177
00:17:35,600 --> 00:17:40,600
or week to week or month, it's not that we're going to die in days or weeks or months

178
00:17:40,600 --> 00:17:46,880
or years, it's that actually the we, the body is something that dies, it's born and

179
00:17:46,880 --> 00:17:51,600
dies at every moment, as we begin to practice meditation, you begin to see this, you begin

180
00:17:51,600 --> 00:17:56,200
to see that actually there is no body, there is only the experience, so when you look

181
00:17:56,200 --> 00:18:00,720
at, look at another person's body, look at your body, then you see, and at that moment

182
00:18:00,720 --> 00:18:06,800
the body arises as a thought and a mind based on the image that you see, when you experience

183
00:18:06,800 --> 00:18:11,560
the tension in the legs, the tension in the back of sitting here, that's when the body

184
00:18:11,560 --> 00:18:16,840
arises, there's the mind and the body in that moment, once the mind moves somewhere else to

185
00:18:16,840 --> 00:18:23,200
hearing or seeing or so on, that experience disappears and therefore the body disappears,

186
00:18:23,200 --> 00:18:34,680
the body is at that moment gone, the reality of the body is kind of ceased, and this

187
00:18:34,680 --> 00:18:40,320
sort of realization starts with what is explained in this verse, so that's really the importance

188
00:18:40,320 --> 00:18:45,280
of this verse, we should understand it as the beginnings of Buddhist practice, or as an example

189
00:18:45,280 --> 00:18:51,960
of how we begin Buddhist practice, we begin by accepting these very simple truths of life,

190
00:18:51,960 --> 00:18:58,960
that death is inevitable, this body, anyone who clings to this body has to face the question

191
00:18:58,960 --> 00:19:07,880
of what they're going to do when death or what is the purpose of clinging when we're

192
00:19:07,880 --> 00:19:13,160
going to lose this body in the end, why are you worrying about this body, why are you

193
00:19:13,160 --> 00:19:18,520
concerned with it, why are you attached to it, when it's not obviously going to bring you

194
00:19:18,520 --> 00:19:23,920
lasting satisfaction, in the end you're going to lose, in the end this body is going to

195
00:19:23,920 --> 00:19:29,160
lie like a useless log, it's a vivid image for people who cling to the body because

196
00:19:29,160 --> 00:19:35,400
we tend not to like to think of our body as something that we have to throw away, we

197
00:19:35,400 --> 00:19:40,600
try our best to not have to lose the body, to take care for it and concern about it, and

198
00:19:40,600 --> 00:19:44,800
we'll be concerned about all of the limbs and organs, and if we lose a finger, if we lose

199
00:19:44,800 --> 00:19:51,000
a hand, if we lose a leg, this would be a travesty, if we go blind, if we go deaf, if

200
00:19:51,000 --> 00:20:01,400
we lose a tooth, even if we lose a tooth, it's an incredible travesty for us because we

201
00:20:01,400 --> 00:20:06,040
cling to this body as being me, as being minus being stable, as the last person, it's like

202
00:20:06,040 --> 00:20:14,040
a pot and these verses clearly go together, they're along the same sort of theme, that the

203
00:20:14,040 --> 00:20:20,800
body is fragile and in the end it's going to break and become useless like a charred log.

204
00:20:20,800 --> 00:20:25,880
Once we begin on this sort of introspection, and many other introspection see, the idea

205
00:20:25,880 --> 00:20:33,240
that habits can change the whole of your being, for example, in this realization that

206
00:20:33,240 --> 00:20:38,920
clinging leads to, craving leads to clinging leads to suffering and so on.

207
00:20:38,920 --> 00:20:43,200
The realization that the way we're carrying out our life is unsustainable, and in the

208
00:20:43,200 --> 00:20:52,200
end we're going to lose this due to the inevitability of things like death and change,

209
00:20:52,200 --> 00:20:57,320
and this leads us inward, it leads us to approach life from a different point of view,

210
00:20:57,320 --> 00:21:03,760
and it's the beginning of a longer course of practice to see not only that death is inevitable

211
00:21:03,760 --> 00:21:09,240
but that death occurs at every moment, and that not only can we not cling to things next

212
00:21:09,240 --> 00:21:14,680
year or 10 years from now, we can't even cling to things in the next moment, that

213
00:21:14,680 --> 00:21:20,280
they're going to change and they could come at any time, death could come at any moment.

214
00:21:20,280 --> 00:21:22,520
We don't know what the experience is going to be in the next moment.

215
00:21:22,520 --> 00:21:27,400
Once we begin to practice meditation, we begin to see the mind is changing, the body is

216
00:21:27,400 --> 00:21:32,480
changing, and this occurs in the beginning, it occurs in terms of day by day, right?

217
00:21:32,480 --> 00:21:36,440
So you come here to practice meditation and today was a really good day, so you come

218
00:21:36,440 --> 00:21:39,360
and tell me about it and they're very happy and then the next day you come back and

219
00:21:39,360 --> 00:21:40,840
tell me how horrible it was.

220
00:21:40,840 --> 00:21:45,200
You can understand how all of a sudden today was a very, very bad day, or today was a

221
00:21:45,200 --> 00:21:49,880
very bad day and you're thinking of quitting and then tomorrow's a very good day.

222
00:21:49,880 --> 00:21:54,520
And then this happens again and again until finally you realize that this is the nature

223
00:21:54,520 --> 00:21:59,160
of life, that you can't depend on it and then you begin to look closer and closer until

224
00:21:59,160 --> 00:22:04,400
you realize that it's first meditation practice and meditation practice, you can't expect

225
00:22:04,400 --> 00:22:06,040
it to be the same.

226
00:22:06,040 --> 00:22:10,720
You can't cling to even a single meditation practice as being dependable.

227
00:22:10,720 --> 00:22:14,560
The next meditation practice might be totally different and then eventually it gets to

228
00:22:14,560 --> 00:22:18,920
the point of realizing you can't cling from one moment to the next and eventually this

229
00:22:18,920 --> 00:22:22,760
is how you would let go of everything and the end you're able to let go of not only your

230
00:22:22,760 --> 00:22:29,720
body but the mind as well and not be concerned about any experience realizing that the

231
00:22:29,720 --> 00:22:33,600
true cause of suffering is our attachment to things.

232
00:22:33,600 --> 00:22:38,680
So it's this sort of realization that sets us on that it was this was the benefit of

233
00:22:38,680 --> 00:22:44,680
this verse, especially for this monk who was being sick was to remind him and to alert

234
00:22:44,680 --> 00:22:52,480
him to the fact that this was real and was a serious, a fact of life that he was going

235
00:22:52,480 --> 00:22:56,920
to have to face now and to set him on the right path and help him to come to terms with

236
00:22:56,920 --> 00:23:02,040
this and to not be lax and you know because when people are sick they'll often, they

237
00:23:02,040 --> 00:23:06,160
think that we think the correct response to get sad and upset and cry and depressed and

238
00:23:06,160 --> 00:23:14,560
angry and cry and cry out for medicines and or maybe even cling to cling more to the

239
00:23:14,560 --> 00:23:17,960
happy things and say well I've got to get now I've only got so many days to live I've got to

240
00:23:17,960 --> 00:23:22,680
get as much happiness in before I die not realizing that this is going to make you cling

241
00:23:22,680 --> 00:23:30,440
more and more and be less and less satisfied and so you know for the most part people

242
00:23:30,440 --> 00:23:34,680
get depressed and and upset because they're not going to be able to cling to the things

243
00:23:34,680 --> 00:23:41,040
that they're clinging to and so here's the Buddha reminding him that it's too serious for

244
00:23:41,040 --> 00:23:46,480
us to be lax and negligent and so I realized that in the end the body this body will

245
00:23:46,480 --> 00:23:50,680
allow me this is it's something that we can always come back and think about it's something

246
00:23:50,680 --> 00:23:56,240
always useful to come back and and remind yourself that this body is like a log eventually

247
00:23:56,240 --> 00:24:04,520
it's going to fall and lie useless on the earth just as a log it's something that this

248
00:24:04,520 --> 00:24:10,920
is a reason why this verse became quite famous because it's a good meditation to use

249
00:24:10,920 --> 00:24:15,160
to remind yourself from time to time whenever you're clinging to the body you're worried

250
00:24:15,160 --> 00:24:20,320
about the body when you find yourself or you can use it to test yourself to point out

251
00:24:20,320 --> 00:24:25,720
to yourself how ridiculous is your love and your attachment to your body is being beautiful

252
00:24:25,720 --> 00:24:34,480
and wonderful source of stability and happiness and so on so just one more verse going through

253
00:24:34,480 --> 00:24:39,800
this one by one and using each of them to give us a little bit more food for thought

254
00:24:39,800 --> 00:24:44,600
and explore the Buddha's teaching from one more point of view so thanks for tuning in

255
00:24:44,600 --> 00:25:14,440
thanks for listening and until next time back to our practice.

