1
00:00:00,000 --> 00:00:07,000
Why do some people simply possess brilliant minds, or born geniuses, etc.?

2
00:00:07,000 --> 00:00:11,000
Actually, do you think geniuses are born or made?

3
00:00:11,000 --> 00:00:16,000
May one of the reasons be their previous life in some of the higher realms?

4
00:00:16,000 --> 00:00:25,000
Well, not even necessarily in the higher realms, but some aspiration in the past.

5
00:00:25,000 --> 00:00:34,000
Definitely, quality of the mind, even inclination for certain things, so people who are inclined towards music,

6
00:00:34,000 --> 00:00:37,000
people who are inclined towards science.

7
00:00:37,000 --> 00:00:45,000
It's interesting last night I went to one of my supporters' houses and their meditators,

8
00:00:45,000 --> 00:00:51,000
and their two daughters are also meditators, and talking to their two daughters about their university studies

9
00:00:51,000 --> 00:00:56,000
and comparing, just chatting about university and about the way we learn.

10
00:00:56,000 --> 00:01:02,000
The one daughter is very, very much math-oriented, a pure math-oriented,

11
00:01:02,000 --> 00:01:07,000
so she doesn't like practical applications.

12
00:01:07,000 --> 00:01:10,000
There's these two sides of science studies.

13
00:01:10,000 --> 00:01:13,000
One is people who like the pure stuff, where it's just theory,

14
00:01:13,000 --> 00:01:18,000
doesn't have any basis in reality, because reality is muddy reality is organic,

15
00:01:18,000 --> 00:01:22,000
and there's uncertainty in it and so on.

16
00:01:22,000 --> 00:01:33,000
Then the other side hates that stuff, and loves getting the hands-on organic natural stuff that has uncertainty and is practical.

17
00:01:33,000 --> 00:01:37,000
We're talking about it, and it's quite clear that they're two very, very different people.

18
00:01:37,000 --> 00:01:41,000
Science understands that, and they have this left brain, right brain kind of thing.

19
00:01:41,000 --> 00:01:45,000
Genetics and the idea that people are different, but from a Buddhist point of view,

20
00:01:45,000 --> 00:01:51,000
it's also interesting to see how different two daughters can be.

21
00:01:51,000 --> 00:01:57,000
Two siblings can be, and obviously we see that in people,

22
00:01:57,000 --> 00:02:05,000
so we're born differently, and we would attribute this at least partially to past karma.

23
00:02:05,000 --> 00:02:09,000
But the idea of the quality of brilliance, like some people have great memory,

24
00:02:09,000 --> 00:02:17,000
I have great memory. It just happens to be a trait, and this other monk who ordained...

25
00:02:17,000 --> 00:02:22,000
ordained after me, but he's been a meditation teacher for quite some time.

26
00:02:22,000 --> 00:02:27,000
In our tradition, he has a terrible memory, and yet he's a brilliant teacher,

27
00:02:27,000 --> 00:02:31,000
and he's now, I think, world-renowned, he's a great guy.

28
00:02:31,000 --> 00:02:33,000
He's also on YouTube a little bit.

29
00:02:33,000 --> 00:02:39,000
People put his videos on proper. He's an Israel, but terrible memory, and he admits it.

30
00:02:39,000 --> 00:02:48,000
So we're comparing this, and it's just interesting how we have different qualities of mind, different abilities.

31
00:02:48,000 --> 00:02:58,000
And so, definitely we attribute this, and we speculate on the fact that this is probably something to do from the past.

32
00:02:58,000 --> 00:03:03,000
Karma really isn't, you know, that's not really the important questions about karma that we ask.

33
00:03:03,000 --> 00:03:09,000
Most important questions are knowledge that we try to understand,

34
00:03:09,000 --> 00:03:14,000
and we try to cultivate, is about karma right here and now what it's doing to us, how it's changing us.

35
00:03:14,000 --> 00:03:23,000
The idea that you might be reborn in a better place, in a better way, is kind of incentive to help you do that,

36
00:03:23,000 --> 00:03:29,000
to help you understand, and if people knew what their actions were leading them to,

37
00:03:29,000 --> 00:03:34,000
then they'd be a lot more concerned about learning about them, about studying about them.

38
00:03:34,000 --> 00:03:39,000
So the important thing is that you come to the present moment and study your karma.

39
00:03:39,000 --> 00:03:45,000
By telling people about past and future lives, it helps them see the importance of doing that.

40
00:03:45,000 --> 00:03:54,000
So if there is a future life, and if your karma, your state of mind is going to influence that, then G,

41
00:03:54,000 --> 00:04:03,000
you better come and look and see whether what you're doing is the right thing to do, and learn more about how to do the right things.

42
00:04:03,000 --> 00:04:12,000
So it's a lot more basic preliminary stuff to talk about karma in terms of past lives, future lives, and so on.

43
00:04:12,000 --> 00:04:21,000
The ultimate goal is to understand karma as it works here and now how our bad deeds are making us bad people,

44
00:04:21,000 --> 00:04:33,000
our corrupting our minds, and how our good deeds are purifying our minds are making us calmer, more peaceful, more gentle and beneficial beings.

45
00:04:33,000 --> 00:04:43,000
But, yeah, so definitely plays a part, something to...

46
00:04:43,000 --> 00:04:48,000
No, not really something to think about, but there you have it.

47
00:04:48,000 --> 00:04:51,000
Definitely, karma is most likely to play a part.

48
00:04:51,000 --> 00:04:59,000
It seems reasonable to say that, and that's the Buddhist theory, is that past lives do determine a lot,

49
00:04:59,000 --> 00:05:04,000
or do condition our lives in the present and present will condition.

