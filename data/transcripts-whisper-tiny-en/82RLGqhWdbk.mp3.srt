1
00:00:00,000 --> 00:00:05,920
I have made a twenty-four hour retreat and making nightmare since then.

2
00:00:05,920 --> 00:00:06,960
Why is that happening?

3
00:00:06,960 --> 00:00:09,680
It isn't meaning something.

4
00:00:09,680 --> 00:00:12,680
My teacher always said he said, good to have nightmares.

5
00:00:12,680 --> 00:00:18,360
He said, when you have bad dreams, it's a sign that good things are going to come.

6
00:00:18,360 --> 00:00:22,880
I think he was just talking cultural stuff, and Thai stuff.

7
00:00:22,880 --> 00:00:28,120
I'm not sure actually, but you know, it's an interesting conceptual teaching.

8
00:00:28,120 --> 00:00:33,960
I mean, the reality of it, you'd have to get into psychology and so on.

9
00:00:33,960 --> 00:00:40,560
And I don't want to make too much of an assumption, but in general, when you're meditating

10
00:00:40,560 --> 00:00:44,120
a lot of stuff that we've kept repressed comes out.

11
00:00:44,120 --> 00:00:47,640
I don't want it to sound like we're trying to pull stuff out.

12
00:00:47,640 --> 00:00:48,800
We're certainly not.

13
00:00:48,800 --> 00:00:54,280
And I don't want you to expect that you're going to pull up skeletons in the closet.

14
00:00:54,280 --> 00:00:59,280
When I first started, the first teachers I had were always on about this, talking about

15
00:00:59,280 --> 00:01:02,640
are you repressing something, or I have the feeling that you've got something deep down

16
00:01:02,640 --> 00:01:07,240
and it drove me crazy, really, because I don't have anything hidden deep down, and yet

17
00:01:07,240 --> 00:01:08,240
I was looking for it.

18
00:01:08,240 --> 00:01:10,120
And I can tell you that I don't.

19
00:01:10,120 --> 00:01:13,840
If I do man, it's buried.

20
00:01:13,840 --> 00:01:16,520
It was really, you know, I was just messed up.

21
00:01:16,520 --> 00:01:19,760
In general, there was nothing hidden about it.

22
00:01:19,760 --> 00:01:28,000
I had been watching Godfather movies for the past, you know, in Scarface and all that.

23
00:01:28,000 --> 00:01:34,080
This is how I'd been living my spiritual life.

24
00:01:34,080 --> 00:01:46,960
So, but it does happen that the things that you repress just to keep facade of normality

25
00:01:46,960 --> 00:01:57,280
are brought up because you stop that, you quit the fighting, you stop fighting against

26
00:01:57,280 --> 00:02:02,680
them, you stop pushing them away, you stop pretending.

27
00:02:02,680 --> 00:02:10,480
Most people are the difference between people who are able to get by in society and those

28
00:02:10,480 --> 00:02:14,080
that aren't is generally just a potential.

29
00:02:14,080 --> 00:02:16,760
Some people are able to fake it better than others.

30
00:02:16,760 --> 00:02:18,600
For most of us, it's just faking it.

31
00:02:18,600 --> 00:02:25,800
I can't get along well in society, and I feel somehow it has to do with my interest

32
00:02:25,800 --> 00:02:34,080
in general, in reality as opposed to, you know, faking it, because it's tiresome to have

33
00:02:34,080 --> 00:02:36,680
to keep up appearances.

34
00:02:36,680 --> 00:02:41,920
When you meditate, you start to realize that, and so you let your guard down.

35
00:02:41,920 --> 00:02:47,480
And as a result, a lot of crazy stuff comes up.

36
00:02:47,480 --> 00:02:53,280
And so in the beginning, or at certain times, this can lead to nightmares.

37
00:02:53,280 --> 00:02:58,680
I would say that that's probably a good guess as to what's happening in your case.

38
00:02:58,680 --> 00:03:05,280
Sometimes you have very vivid dreams, but I think it's clear that apparently what

39
00:03:05,280 --> 00:03:09,360
in my teacher's say he said, an enlightened being doesn't dream.

40
00:03:09,360 --> 00:03:14,120
And that makes sense, because an enlightened being's mind is very clear, and when they

41
00:03:14,120 --> 00:03:17,000
sleep, they sleep, when they're awake, they're awake.

42
00:03:17,000 --> 00:03:20,280
They don't have any of this mental fermentation.

43
00:03:20,280 --> 00:03:25,520
It's a sign that stuff's coming up, and you should never, ever take your dream seriously.

44
00:03:25,520 --> 00:03:26,520
I can't remember.

45
00:03:26,520 --> 00:03:28,880
I think I did a video about dreams.

46
00:03:28,880 --> 00:03:32,040
I didn't take a video, do a video about dreams when I was in America.

47
00:03:32,040 --> 00:03:35,960
Look that video up, because I think I said everything that I have to say about dreams

48
00:03:35,960 --> 00:03:39,480
in that video, don't take them seriously.

49
00:03:39,480 --> 00:04:07,000
And whatever else I said on that video, endorsing the 100%, hopefully it was all correct.

