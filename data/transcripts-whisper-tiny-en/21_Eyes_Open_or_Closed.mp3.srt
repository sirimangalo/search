1
00:00:00,000 --> 00:00:02,640
eyes open or close.

2
00:00:02,640 --> 00:00:08,280
Should the eyes be open or closed during walking meditation?

3
00:00:08,280 --> 00:00:13,120
The eyes should be open during walking meditation to maintain balance.

4
00:00:13,120 --> 00:00:25,760
The mind should be with the feet, not the eyes.

