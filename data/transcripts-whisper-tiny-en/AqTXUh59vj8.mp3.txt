My understanding of the Buddhist conception of love is that it is a kind of
delusion and attachment with attendant negative connotations. If this is the case
how could we characterize the emotion motivation which impels a bodhisattva to
be reborn? Love is not delusion and again back to the Abidhamma love is a word
and is ambiguous because most often in the West we're referring to Eros,
Eros which Eros which is lustful love or not even lustful but just desire for
something. I love apples or I love my wife or my girlfriend or my boyfriend
or I love my children, adoration and so on, is often unwholesome not
delusion based on delusion but is craving or greed based, desire based and so
that's unwholesome but love that is desire for beings to be happy, wishing for
them to be happy or acting. The impulsion that impulse push moves us to act for
the benefit of others. This is Mehta and this is a positive state, it's wholesome.
It's worldly, it won't get you to Nibana but it's wholesome and if
cultivated can eventually lead to limitless love for the whole universe and
which can lead you to be born as Abramla and can give great states of
concentration which are also beneficial to the practice of insight
meditation. So it depends what you mean by love but the emotion motivation
that impels the bodhisattva to be reborn. The reason a bodhisattva in the
terror of attradition is reborn and I'm assuming you're talking about the
terror of attabodhisattva without the V because with the V in there it gets
kind of weird and Mahayana conception gets pretty radical but what impels a
bodhisattva to be reborn again is delusion and defilement. The only reason
we're reborn is because there's some kind of attachment. Now although the
bodhisattva has done is refused to listen or to accept an enlightenment that
is that is void of omniscience. So given the opportunity to become enlightened
in this life without being omniscient they discard it. There's that. Now
what moves them to do it? Again so now we have to distinguish between
bodhisattas. Some bodhisattas are not going to become Buddhas and they make a
determination to become a Buddha and are unsuccessful and often these
determinations that they make are based on ego or desire or whatever. In the
case of Arbuddha he made a vow out of I guess you could say wisdom and
confidence and faith because he was a highly developed aesthetic who had
magical powers and insight and he knew with certainty with confidence that if
he listened to the Buddha's teaching he would become an Arhan very
quickly right away and so he made the determination to go one step further to
go the next step which was a huge step out of wisdom and understanding. It's
interesting curious to note that as soon as he became a Buddha one of the first
things he thought of was not taking advantage of his omniscience. It was to
pass away into Parinibhana which you know he would have saved himself a lot of
time and he just done that back in the time of Department of Buddha. But the
result but the result of his omniscience was such that he wasn't able to do it
because immediately he was asked to teach Brahma came down and asked him to
teach because he was omniscient no one would come down and ask and Brahma would
not come down and ask me to teach for example because I'm not omniscient but
he came down and asked the Buddha to teach and so it was based on this quality.
What motivated it and so again this is it differs from from person to person but
the real bodhisattas it would seem they do it out of confidence and wisdom on
that occasion where they are recognized and where the another Buddha says see
that guy is lying in the mud and he's going to become a Buddha in the future
life and so.
