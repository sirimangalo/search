1
00:00:00,000 --> 00:00:10,760
Okay, so today I'd like to recite the story of Sumedha.

2
00:00:10,760 --> 00:00:20,760
Sumedha is the name of the Bodhisattva, or in Sanskrit you say Bodhisattva, who later became

3
00:00:20,760 --> 00:00:27,440
known as the Buddha Gautama or Siddhartha Gautama or Sakayamuni or all of the many names

4
00:00:27,440 --> 00:00:32,760
that he was given.

5
00:00:32,760 --> 00:00:38,400
And this is sort of a lead up to what I'll be talking about this afternoon because this

6
00:00:38,400 --> 00:00:42,160
afternoon I'm going to be talking about one of the, I'm going to be giving a story from

7
00:00:42,160 --> 00:00:47,040
one of the lives of the Bodhisattva.

8
00:00:47,040 --> 00:00:55,440
My favorite story, probably my favorite story of all time and the longest and most, most

9
00:00:55,440 --> 00:00:57,760
like an epic of all the stories.

10
00:00:57,760 --> 00:01:04,200
But as sort of a background to that, I thought I'd take us right back to the beginning

11
00:01:04,200 --> 00:01:09,880
and for anybody who's kind of wondering who is the Buddha, who is the Bodhisattva and

12
00:01:09,880 --> 00:01:14,680
what is it that he had to do, what is it that he did, where did Buddhism start?

13
00:01:14,680 --> 00:01:20,720
And this is a poem about where Buddhism started, where our, I guess I'm sorry you can't

14
00:01:20,720 --> 00:01:24,680
say that because Buddhism is something that is always around whenever there's a Buddha

15
00:01:24,680 --> 00:01:26,560
right, a reason.

16
00:01:26,560 --> 00:01:31,840
But I think everyone will admit that in, in the present time, the only reason that we

17
00:01:31,840 --> 00:01:37,400
are aware of Buddhism at all is because of one very special person.

18
00:01:37,400 --> 00:01:45,400
And so as far as where Buddhism starts, I'm going to, let's just, just agree on the fact

19
00:01:45,400 --> 00:01:47,960
that this is one starting point.

20
00:01:47,960 --> 00:01:53,400
One way we can say where Buddhism started and this is a very, very long time ago.

21
00:01:53,400 --> 00:01:59,080
And it starts off by saying how long ago that was, and maybe I can give some commentary

22
00:01:59,080 --> 00:02:01,640
afterwards about exactly how long ago it was.

23
00:02:01,640 --> 00:02:05,920
But due to time constraints, I'm not sure how long it takes to read this, I'll just

24
00:02:05,920 --> 00:02:07,680
get started now.

25
00:02:07,680 --> 00:02:11,880
So please make yourself comfortable and please save all comments and discussion for

26
00:02:11,880 --> 00:02:14,160
after I finished.

27
00:02:14,160 --> 00:02:20,040
And please take it as a chance to meditate as well, so I'll try to be very in tune with

28
00:02:20,040 --> 00:02:25,400
what's being said.

29
00:02:25,400 --> 00:02:30,560
A hundred thousand cycles vast and four amensities ago.

30
00:02:30,560 --> 00:02:35,840
There was a town named Amara, a place of beauty and delights.

31
00:02:35,840 --> 00:02:41,400
It had the noises ten complete and food and drink abundantly.

32
00:02:41,400 --> 00:02:46,360
The noise of elephant and horse of conch shall drum and chariot.

33
00:02:46,360 --> 00:02:53,560
And invitations to partake, eat ye and drink, resound it loud.

34
00:02:53,560 --> 00:02:59,120
A town complete in all its parts where every industry was found and eak the seven precious

35
00:02:59,120 --> 00:03:03,960
gems and foreigners from many lands.

36
00:03:03,960 --> 00:03:10,480
A prosperous city of the gods full of good works and holy men.

37
00:03:10,480 --> 00:03:18,440
In this town of Amara Sumedha lived of Brahmin caste, who many tens of millions had and

38
00:03:18,440 --> 00:03:21,800
grain and treasure in full store.

39
00:03:21,800 --> 00:03:28,840
A student he and wise in spells, a master of the Vedas three, he fortunes told tradition

40
00:03:28,840 --> 00:03:34,720
new and every duty of his caste.

41
00:03:34,720 --> 00:03:39,480
In secret then I sat me down and thus to ponder I began.

42
00:03:39,480 --> 00:03:45,480
Look misery to be born again and have the flesh dissolve at death.

43
00:03:45,480 --> 00:03:51,080
Subject to birth old age disease, extinction will I seek to find where no decay is ever

44
00:03:51,080 --> 00:03:57,360
known nor death but all security.

45
00:03:57,360 --> 00:04:02,720
What if I now should rid me of this body foul, this charnel house and go my way without

46
00:04:02,720 --> 00:04:07,720
a care or least regret for things behind?

47
00:04:07,720 --> 00:04:13,640
There is there must be an escape, impossible there should not be, I will make the search

48
00:04:13,640 --> 00:04:19,840
and find the way which from existence shall release.

49
00:04:19,840 --> 00:04:26,760
Even as although their misery is yet happiness is also found, so though indeed existence

50
00:04:26,760 --> 00:04:32,280
is a non-existence should be sought.

51
00:04:32,280 --> 00:04:38,880
Even as although there may be heat yet grateful cold is also found, so through the three

52
00:04:38,880 --> 00:04:46,400
fold fires exist so though the three fold fires exist likewise Nirvana should be sought.

53
00:04:46,400 --> 00:04:52,640
Even as although their evil is, that which is good is also found, so though it is true

54
00:04:52,640 --> 00:04:59,320
that birth exists, that which is not birth should be sought.

55
00:04:59,320 --> 00:05:05,320
Even as a man be fouled with dung seeing a brimming lake at hand and not the less bathing

56
00:05:05,320 --> 00:05:11,880
not there in where senseless should he chide the lake.

57
00:05:11,880 --> 00:05:18,000
So when Nirvana's lake exists to wash away corruption's stain, should I not seek to bathe

58
00:05:18,000 --> 00:05:24,520
therein I might not then Nirvana chide.

59
00:05:24,520 --> 00:05:30,040
Even as a man hemmed in by foes seeing a certain safe escape and not the less seeking not

60
00:05:30,040 --> 00:05:35,800
to flee, might not the blameless pathway chide.

61
00:05:35,800 --> 00:05:42,840
So when my passions hem me in and yet a way to bliss exists, should I not seek to follow

62
00:05:42,840 --> 00:05:48,480
it, that way of bliss I might not chide.

63
00:05:48,480 --> 00:05:56,360
Even as a man who saw a diseased when a physician may be had should fail to send to have

64
00:05:56,360 --> 00:06:00,680
him come might the physician then not chide.

65
00:06:00,680 --> 00:06:06,760
So when diseased with passion saw oppressed I seek the master not, whose ghostly counsel

66
00:06:06,760 --> 00:06:11,800
might me cure the blame should not on him be laid.

67
00:06:11,800 --> 00:06:17,320
Even as a man might rid him of a horrid corpse bound to his neck and then upon his way

68
00:06:17,320 --> 00:06:22,200
proceed joyous and free and unconstrained.

69
00:06:22,200 --> 00:06:28,360
So must I likewise rid me of this body foul, this channel house and go my way without

70
00:06:28,360 --> 00:06:32,880
a care or least regret for things behind.

71
00:06:32,880 --> 00:06:38,280
As men and women rid them of their dung upon the refuse heap and go their way without

72
00:06:38,280 --> 00:06:43,000
a care or least regret for what they leave.

73
00:06:43,000 --> 00:06:48,480
So will I likewise rid me of this body foul, this channel house and go my way as if

74
00:06:48,480 --> 00:06:53,640
I had cast out my filth into the draw.

75
00:06:53,640 --> 00:06:59,040
Even as the owners leave and quit a worn out shattered leaky ship and go their way without

76
00:06:59,040 --> 00:07:02,720
a care or least regret for what they leave.

77
00:07:02,720 --> 00:07:09,800
So will I likewise rid me of this nine hold ever trickling frame and go my way as owners

78
00:07:09,800 --> 00:07:14,640
do who ship disrupted leave behind.

79
00:07:14,640 --> 00:07:20,000
Even as a man who treasure bears and finds him in a robber gang will quickly flee and

80
00:07:20,000 --> 00:07:25,280
rid him of the robbers lest they steal his gold.

81
00:07:25,280 --> 00:07:29,200
So too a mighty robber might be likened well this body's frame.

82
00:07:29,200 --> 00:07:35,960
I'll cast it off and go my way lest my well lest of my welfare I be robbed.

83
00:07:35,960 --> 00:07:42,960
Just thinking I on rich and poor all that I had in arms bestowed hundreds of millions spent

84
00:07:42,960 --> 00:07:47,520
I then and made to him of un my way.

85
00:07:47,520 --> 00:07:53,120
Not far away from him of un there was a hill named Hamaka and here I made and patterned

86
00:07:53,120 --> 00:07:57,360
well a hermitage and had of leaves.

87
00:07:57,360 --> 00:08:03,240
A walking place I then laid out exempted from the five defects and having all the virtues

88
00:08:03,240 --> 00:08:08,840
ate and there I gained the six high powers.

89
00:08:08,840 --> 00:08:14,640
Then ceased I cloaks of cloth to wear for cloaks possessed the nine defects and girded

90
00:08:14,640 --> 00:08:19,840
on a bark and dress which is with virtues twelve in dude.

91
00:08:19,840 --> 00:08:25,440
My heart of leaves I then forsook and so so crowded with the eight defects and at the

92
00:08:25,440 --> 00:08:32,160
foot of trees I lived for such abodes have virtues ten.

93
00:08:32,160 --> 00:08:37,760
No sewn and cultivated grain allowed I then to be my food but all the many benefits of

94
00:08:37,760 --> 00:08:41,680
wild fruit fair I made my own.

95
00:08:41,680 --> 00:08:47,960
And strenuous effort made I there the while I sat or stood or walked and air seven days

96
00:08:47,960 --> 00:08:52,760
had passed away I had attained the powers high.

97
00:08:52,760 --> 00:08:58,280
When I had thus success attained and made my made me master of the law a conqueror Lord

98
00:08:58,280 --> 00:09:05,720
all Lord of all the world was born by name Deepankara.

99
00:09:05,720 --> 00:09:11,320
What time he was conceived was born what time he boot a ship attained when first he preached

100
00:09:11,320 --> 00:09:18,600
the signs appeared I saw them not deep sunk in trends.

101
00:09:18,600 --> 00:09:24,520
Then in the distant borderland invited they this great this being great and everyone

102
00:09:24,520 --> 00:09:30,520
with joyful heart the pathway for his coming cleared.

103
00:09:30,520 --> 00:09:35,960
Now so I happened at this time that I my hermitage had left and bark and garments rustling

104
00:09:35,960 --> 00:09:39,960
loud was passing or them through the air.

105
00:09:39,960 --> 00:09:47,000
Then saw everyone on alert well please delighted overjoyed and coming downward from the

106
00:09:47,000 --> 00:09:51,160
sky the multitude I straight way asked.

107
00:09:51,160 --> 00:09:56,960
Well please delighted overjoyed and all alert is everyone for whom is being cleared

108
00:09:56,960 --> 00:10:02,040
the way and the path the track to travel on.

109
00:10:02,040 --> 00:10:07,840
When thus I asked response was made a mighty Buddha has appeared a conqueror Lord of all

110
00:10:07,840 --> 00:10:14,920
the world whose name is called Deepankara for him is being cleared the way the path the track

111
00:10:14,920 --> 00:10:24,360
to travel on this word the Buddha when I heard joy sprang up straight way in my heart.

112
00:10:24,360 --> 00:10:33,640
A Buddha cried I then and published my heart's content and standing there I pondered deep

113
00:10:33,640 --> 00:10:40,920
by joyous agitation seized here will I now some good seeds so nor let this fitting season

114
00:10:40,920 --> 00:10:42,480
slip.

115
00:10:42,480 --> 00:10:48,360
For a Buddha do ye clear the road then pray grant also me a place I too will help to clear

116
00:10:48,360 --> 00:10:53,280
the way the path the track to travel on.

117
00:10:53,280 --> 00:10:58,480
And so they granted also me a portion of the path to clear and I again clear while still

118
00:10:58,480 --> 00:11:04,920
my heart said Buddha or and or.

119
00:11:04,920 --> 00:11:09,720
But air my part was yet complete deepankara the mighty sage the conqueror came that way

120
00:11:09,720 --> 00:11:17,360
along thronged by four hundred thousand saints without depravity or spot and having each

121
00:11:17,360 --> 00:11:22,080
the six high powers.

122
00:11:22,080 --> 00:11:26,880
The people then their greetings gave and many kettle drums were beat and men and gods

123
00:11:26,880 --> 00:11:33,240
enjoy its moods loud shouted their applauding cries then men and gods together met and

124
00:11:33,240 --> 00:11:38,800
saw each other face to face and all with joined hands up raised followed the Buddha and

125
00:11:38,800 --> 00:11:45,960
his train the gods with instruments divine the men with those of human make triumphant

126
00:11:45,960 --> 00:11:52,440
music played and well they followed in the Buddha's train celestial beings from on

127
00:11:52,440 --> 00:11:59,480
high through broadcast over all the earth the irin irith rina flower of heaven the

128
00:11:59,480 --> 00:12:05,880
lotus and the coral flower and men abiding on the ground on every side flung up in

129
00:12:05,880 --> 00:12:16,600
the air chumpakas salalas nipas naga spuna gaz gaitakas then loosened I my matted hair

130
00:12:16,600 --> 00:12:21,640
and spreading out upon the mud my dress of bark and cloak of skin I laid me down upon

131
00:12:21,640 --> 00:12:31,840
my face let now on me the Buddha tread with the disciples of his train can I be can I

132
00:12:31,840 --> 00:12:38,480
but keep him from the mire to me a great merit shall accrue well thus I lay upon the ground

133
00:12:38,480 --> 00:12:47,080
arose within me many thoughts today if such were my desire I my corruptions might consume

134
00:12:47,080 --> 00:12:53,400
but why thus in an unknown guys should I the doctrines fruit secure omniscience first

135
00:12:53,400 --> 00:12:59,640
will I achieve and be a Buddha in the world or why should I a valorous man the ocean

136
00:12:59,640 --> 00:13:08,940
seek to cross alone omniscience first will I achieve and men and gods convey it cross

137
00:13:08,940 --> 00:13:14,240
since now I make this earnest wish in presence of this best of men omniscience sometime

138
00:13:14,240 --> 00:13:24,000
I'll achieve and multitudes convey across I'll rebirth circling stream arrest destroy existence

139
00:13:24,000 --> 00:13:32,080
is three modes I'll climb the sides of doctrines ship and men and gods convey across

140
00:13:32,080 --> 00:13:37,680
whose saint ship gains and teacher meets as hermit lives in virtue loves nor lacks resolve

141
00:13:37,680 --> 00:13:47,720
nor fiery zeal can buy these eight conditions joined make his most earnest wish succeed

142
00:13:47,720 --> 00:13:52,600
the bunker who knew our world recipient of offerings came to halt my pillow near and thus

143
00:13:52,600 --> 00:14:00,160
address the multitudes behold ye now this monk austere his matted locks his penance

144
00:14:00,160 --> 00:14:10,640
fierce low he a numbered a numbered cycles hence a Buddha in the world shall be from the fair

145
00:14:10,640 --> 00:14:17,200
town called Kapila his great retirement shall be made then when his struggle fierce is

146
00:14:17,200 --> 00:14:28,120
or his stern austerity is performed he shall in quiet sit him down beneath the ajabala tree

147
00:14:28,120 --> 00:14:34,440
their pottage made of rice receive and seek the stream near Anjara this pottage shall

148
00:14:34,440 --> 00:14:40,640
the conquer it beside the stream near Anjara and ends by road triumphal go to where the

149
00:14:40,640 --> 00:14:46,840
tree of wisdom stands then shall the peerless glorious one walk to the right round

150
00:14:46,840 --> 00:14:54,320
wisdom's throne and there the Buddha ship achieve well sitting at the fig tree's root

151
00:14:54,320 --> 00:15:01,280
the mother that shall bring him forth shall my call be by name Sudhodana his father's

152
00:15:01,280 --> 00:15:10,120
name his own name shall be Gotama Golita Upadhisattu these shall his chief disciples be both

153
00:15:10,120 --> 00:15:19,840
undeprived both passions free both tranquil and serene of mind Anandas shall be his servitor

154
00:15:19,840 --> 00:15:27,400
and on the conqueror attend Kama and Upalawana shall female chief disciples be both

155
00:15:27,400 --> 00:15:33,040
undepraved both passion free and tranquil and serene of mind the bow tree of this blessed

156
00:15:33,040 --> 00:15:41,920
one shall be the tree asata called thus spake the unequalled mighty sage and all will

157
00:15:41,920 --> 00:15:48,040
they had heard his speech both men and gods rejoiced and said behold a Buddha sky on

158
00:15:48,040 --> 00:15:56,040
here now shouts were heard on every side the people clapped their arms and laugh ten thousand

159
00:15:56,040 --> 00:16:04,200
word worlds of men and gods paid me their homage then and said if of our Lord Deepankara

160
00:16:04,200 --> 00:16:10,480
the doctrine doctor now we fail to grasp we yet shall stand in time to come before this

161
00:16:10,480 --> 00:16:17,120
other face to face even as when men are river cross and miss the opposing landing place

162
00:16:17,120 --> 00:16:26,400
a lower landing place they find and there the river bank ascend even so we all if we let

163
00:16:26,400 --> 00:16:34,280
slip the presence of the conqueror that we have yet still shall stand in time to come

164
00:16:34,280 --> 00:16:44,000
before this other face to face Deepankara who all worlds new recipient

165
00:16:44,000 --> 00:16:51,360
of offerings my future having prophesied his right foot raised and went his way and

166
00:16:51,360 --> 00:16:56,080
all who were this conqueror sons walked to the right around me then and serpents men

167
00:16:56,080 --> 00:17:03,520
and demigods saluting me departed hands now when the leader of the world had passed

168
00:17:03,520 --> 00:17:09,120
from sight with all his train my mind with rapturous transport field I raised me up from

169
00:17:09,120 --> 00:17:17,760
where I lay then overjoyed with joy I was delighted with a keen delight and thus with

170
00:17:17,760 --> 00:17:24,440
pleasure saturate I sat me down with legs across and well cross legged there I sat

171
00:17:24,440 --> 00:17:32,160
I thus reflected to myself behold in trance am I adept and all the powers higher mine nowhere

172
00:17:32,160 --> 00:17:39,480
throughout a thousand worlds are any seers equal to me unequaled in the magic arts

173
00:17:39,480 --> 00:17:45,440
have I this height of bliss attained and well I sat with legs across the dwellers of ten

174
00:17:45,440 --> 00:17:53,360
thousand worlds rolled forth a glad and mighty shout surely a Buddha thou shalt be the

175
00:17:53,360 --> 00:17:59,520
pressages that airst were seen when future Buddha sat cross legged these pressages are

176
00:17:59,520 --> 00:18:06,040
seen today surely a Buddha that shall thou be all cold is everywhere dispelled and

177
00:18:06,040 --> 00:18:12,640
mitigated is the heat these pressages are seen today surely a Buddha thou shalt be the

178
00:18:12,640 --> 00:18:17,680
system of ten thousand worlds is hushed to quiet and to peace these pressages are seen

179
00:18:17,680 --> 00:18:25,440
today surely a Buddha thou shalt be the mighty winds then cease to blow nor do the rivers

180
00:18:25,440 --> 00:18:33,720
onward glide these pressages are seen today surely a Buddha shall thou be all plants be

181
00:18:33,720 --> 00:18:38,400
they have landers streamed as straight way put their blossoms forth even so today they

182
00:18:38,400 --> 00:18:46,120
all have bloomed surely a Buddha thou shalt be and every tree and every vine is straight

183
00:18:46,120 --> 00:18:51,400
way laden down with fruit even so today they're laden down surely a Buddha thou shalt

184
00:18:51,400 --> 00:18:59,600
be in sky and earth death straight way then full many a radiant radiant gem appear even

185
00:18:59,600 --> 00:19:07,280
so today they shine afar surely a Buddha thou shalt be then straight way musics heard

186
00:19:07,280 --> 00:19:13,800
to play monks men on earth and gods in heaven so all today in music joined surely a

187
00:19:13,800 --> 00:19:19,320
Buddha thou shalt be their fallen straight way down from heaven a rain of many colored

188
00:19:19,320 --> 00:19:26,320
flowers even so today these flowers are seen surely a Buddha thou shalt be the mighty

189
00:19:26,320 --> 00:19:33,080
ocean heaves and roars and all the worlds ten thousand quake even so is now this tumult

190
00:19:33,080 --> 00:19:40,440
heard surely a Buddha thou shalt be straight way throughout the whole of hell the fires

191
00:19:40,440 --> 00:19:48,320
ten thousand all die out even so today they have expired surely a Buddha thou shalt be

192
00:19:48,320 --> 00:19:54,480
unclouded then the sun shines forth and all the stars appear to view even so today do they

193
00:19:54,480 --> 00:20:03,520
appear surely a Buddha thou shalt be straight way although no rain has fallen burst springs

194
00:20:03,520 --> 00:20:10,320
of water from the earth even so today they gush in streams surely a Buddha thou shalt be

195
00:20:10,320 --> 00:20:15,800
and bright then shine the starry hosts and constellations in the sky the moon in Libra now

196
00:20:15,800 --> 00:20:22,160
that stand surely a Buddha thou shalt be all beasts that lurk in holes and clefts then

197
00:20:22,160 --> 00:20:26,840
get them forth from out their layers even so today they've left their dens surely a Buddha

198
00:20:26,840 --> 00:20:33,320
thou shalt be straight way content is all the world and no unhappiness is known even

199
00:20:33,320 --> 00:20:40,120
so today are all content surely a Buddha thou shalt be then every sickness of anishes

200
00:20:40,120 --> 00:20:45,720
and hunger likewise disappears these pressages are seen today surely a Buddha thou shalt be

201
00:20:46,760 --> 00:20:54,840
then lust death dwindle and glow and grow weak and hate infatuation too even so today they

202
00:20:54,840 --> 00:21:02,760
disappear surely a Buddha thou shalt be then fear and danger are unknown and all all we are freed

203
00:21:02,760 --> 00:21:11,720
from them today and by this token we perceive surely a Buddha thou shalt be no dust up

204
00:21:11,720 --> 00:21:17,000
whirleth towards the sky even so today this thing is seen and by this token we perceive surely

205
00:21:17,000 --> 00:21:22,760
a Buddha thou shalt be all noise some odors drift away and heavenly fragrance fills the air

206
00:21:22,760 --> 00:21:29,400
even so the winds now sweetness waft surely a Buddha thou shalt be then all the gods appear

207
00:21:29,400 --> 00:21:35,880
to view save those that hold the formless realm even so today these all are seen surely a

208
00:21:35,880 --> 00:21:43,000
Buddha thou shalt be then clearly seen are all the hells however many be their tale even so

209
00:21:43,000 --> 00:21:50,280
today may all be seen surely a Buddha thou shalt be through walls and doors and mountain rocks

210
00:21:50,280 --> 00:21:55,800
one finds an easy passage then even so today they yield like air surely a Buddha thou shalt be

211
00:21:55,800 --> 00:22:02,760
existence then forbears its round of death and rebirth for a time even so today this thing is

212
00:22:02,760 --> 00:22:10,840
seen surely a Buddha thou shalt be do thou a strenuous effort make do not turn back go on advance

213
00:22:12,520 --> 00:22:16,680
make certainly we know this thing surely a Buddha thou shalt be

214
00:22:16,680 --> 00:22:25,080
when I had heard the Buddha's speech and what the world's ten thousand said well please

215
00:22:25,080 --> 00:22:33,560
delighted overjoyed I thus reflected to myself the Buddha's never liars are a conquerors word

216
00:22:33,560 --> 00:22:41,560
naryet was vain nothing but truth the Buddha's speak surely a Buddha I shall be as clouds

217
00:22:41,560 --> 00:22:46,440
thrown upward in the air fall surely back upon the earth so what the glorious Buddha's speak is

218
00:22:46,440 --> 00:22:53,000
sure instead fast to the end nothing but truth the Buddha's speak surely a Buddha I shall be

219
00:22:54,600 --> 00:22:59,800
as also for each living thing the approach of death is ever sure show so what the glorious

220
00:22:59,800 --> 00:23:06,840
Buddha's speak is sure instead fast to the end nothing but truth the Buddha's speak surely a

221
00:23:06,840 --> 00:23:14,680
Buddha I shall be as at the waning of the night the rising of the sun is sure so what the

222
00:23:14,680 --> 00:23:19,560
glorious Buddha's speak is sure instead fast to the end nothing but the truth the Buddha's speak

223
00:23:20,200 --> 00:23:27,960
surely a Buddha I shall be as when he issues from his den the roaring of the lions sure so what

224
00:23:27,960 --> 00:23:34,680
the glorious Buddha's speak is sure instead fast to the end nothing but the truth the Buddha's

225
00:23:34,680 --> 00:23:43,480
speak surely a Buddha I shall be as when a female has conceived her bringing forth of young is sure

226
00:23:43,480 --> 00:23:50,040
so what the glorious Buddha's speak is sure instead fast to the end nothing but truth the Buddha's

227
00:23:50,040 --> 00:23:58,200
speak surely a Buddha I shall be come now I'll search that I may find conditions which a Buddha

228
00:23:58,200 --> 00:24:07,400
make above below to all ten points where air the conditions hold their sway and then I searched

229
00:24:07,400 --> 00:24:14,440
and saw the first perfection which consists in alms that high road great where on of old the

230
00:24:14,440 --> 00:24:21,960
former seers had ever walked come now this first one adopt this one has first adopt and practice

231
00:24:21,960 --> 00:24:29,240
it determinantly acquire perfection in thy alms if thou to wisdom would sustain and when a jar

232
00:24:29,240 --> 00:24:34,760
is brimming full and someone overturneth it this the jar its waters all give forth and nothing

233
00:24:34,760 --> 00:24:43,160
for itself keeps back so when a separate death thou does see of mean or high or middling rank give

234
00:24:43,160 --> 00:24:52,760
all in arms in nothing stint in as the or turn it jar but now there must be more than these

235
00:24:52,760 --> 00:24:58,120
conditions which a Buddha make still others will I seek to find that shall in Buddhist

236
00:24:58,120 --> 00:25:05,720
ship mature perfection second then I saw it and lo the precepts came to view which mighty

237
00:25:05,720 --> 00:25:13,080
seers of former times had practiced and had followed come now as second this adopt and practice

238
00:25:13,080 --> 00:25:20,440
it determinately the precepts to perfection keep if thou to wisdom would sustain as when a yak

239
00:25:20,440 --> 00:25:27,080
cows flowing tail is firmly caught by bush or thorn she there upon awaits her death but will

240
00:25:27,080 --> 00:25:35,000
not tear and mar her tail so likewise thou in stages for observe and keep the precepts whole

241
00:25:35,000 --> 00:25:43,000
on all occasions guard them well as every yak cow does her tail but now there must be more

242
00:25:43,000 --> 00:25:48,440
than these conditions which a Buddha make still others will I seek to find that shall in

243
00:25:48,440 --> 00:25:57,160
Buddha ship mature and then perfection third I saw it which is renunciation called which mighty

244
00:25:57,160 --> 00:26:04,040
seers of former times had practiced and had followed come now this one as third adopt and

245
00:26:04,040 --> 00:26:11,080
practice it determinately renounce and imperfection grow if thou to wisdom would sustain even

246
00:26:11,080 --> 00:26:17,800
as a man who long has dwelt in prison suffering miserably no liking for the place conceives

247
00:26:17,800 --> 00:26:25,320
but only longing for longeth for release so likewise thou must every mode of being as a prison

248
00:26:25,320 --> 00:26:34,360
view renunciation be thy aim thus from existence free thyself but now there must be more than

249
00:26:34,360 --> 00:26:40,280
these conditions which a Buddha make still others will I seek to find that shall in Buddha

250
00:26:40,280 --> 00:26:48,520
ship mature and then I sought and found the fourth perfection which is wisdom called which mighty

251
00:26:48,520 --> 00:26:55,560
seers of former times had practiced and had followed come now this one as fourth adopt and

252
00:26:55,560 --> 00:27:01,480
practice it determinately wisdom to its perfection bring and if thou to wisdom would sustain

253
00:27:02,600 --> 00:27:07,960
just as a priest went on his rounds nor lo nor high nor middling folk doth shun but begs of

254
00:27:07,960 --> 00:27:17,000
everyone and so his daily food receives so to the learned I resort and seek thy wisdom to increase

255
00:27:17,000 --> 00:27:23,640
and when this fourth perfection gained a Buddha's wisdom shall be thine but now there must be

256
00:27:23,640 --> 00:27:28,760
more than these conditions would which a Buddha make still others will I seek to find that shall

257
00:27:28,760 --> 00:27:35,480
in Buddha ship mature and then I saw it and found the fifth perfection which is courage called

258
00:27:35,480 --> 00:27:43,560
which mighty seers of former times had practiced and had followed come now this one as fifth

259
00:27:43,560 --> 00:27:50,440
adopt and practice it determinately encourage perfect strive to be if thou to wisdom would

260
00:27:50,440 --> 00:27:59,960
sustain just as the lion king of beast encroaching walking standing still with courage ever is

261
00:27:59,960 --> 00:28:07,160
instinct and watchful always and alert so thou in each repeated birth courageous energy

262
00:28:07,160 --> 00:28:14,840
display and when this fifth perfection gained a Buddha's wisdom shall be thine but now there

263
00:28:14,840 --> 00:28:19,480
must be more than these conditions which a Buddha make still others will I seek to find that shall

264
00:28:19,480 --> 00:28:26,200
in Buddha ship mature and then I sought and found the sixth perfection which is patience called

265
00:28:26,200 --> 00:28:32,520
which mighty seers of former times had practiced and had followed come now this one is sixth

266
00:28:32,520 --> 00:28:38,040
adopt and practice it determinately and if thou keep an even mood a Buddha's wisdom shall be thine

267
00:28:40,200 --> 00:28:46,280
just as the earth whatever is thrown upon her whether sweet or foul all things and jurors

268
00:28:46,280 --> 00:28:53,960
and never shows repugnance nor complacency in so or honor thou or scorn of men with patient

269
00:28:53,960 --> 00:28:59,400
mood must bear and when the six perfections gained a Buddha's wisdom shall be thine

270
00:29:01,080 --> 00:29:05,560
but now there must be more than these conditions which a Buddha makes still others will I seek to

271
00:29:05,560 --> 00:29:12,520
find that shall in Buddha ship mature and then I sought and found the seventh perfection which

272
00:29:12,520 --> 00:29:19,800
is that of truth which mighty seers of former times had practiced and had followed come now this

273
00:29:19,800 --> 00:29:25,400
one is seventh adopt and practice it determinately if thou are narrow of double speech a Buddha's

274
00:29:25,400 --> 00:29:32,200
wisdom shall be thine just as the morning star on high its balance course does ever keep

275
00:29:33,560 --> 00:29:39,240
and through all seasons times and years death never from its pathway swerve so likewise thou

276
00:29:39,240 --> 00:29:45,400
and all thy speech swerve never from the path of truth and when this seventh perfection

277
00:29:45,400 --> 00:29:52,280
gained a Buddha's wisdom shall be thine but now there must be more than these conditions which

278
00:29:52,280 --> 00:29:58,600
a Buddha makes still others will I seek to find that shall in Buddha ship mature and then I

279
00:29:58,600 --> 00:30:04,120
sought and found the eighth perfection resolution called which mighty seers of former times had

280
00:30:04,120 --> 00:30:10,200
practiced and had followed come now this one is eighth adopt and practice it determinately

281
00:30:10,200 --> 00:30:17,560
and when thou art immovable a Buddha's wisdom shall be thine just as a rocky mountain peak

282
00:30:17,560 --> 00:30:25,000
unmoved stands firm stab blushing unshaken by the boisterous scales and always in its place

283
00:30:25,000 --> 00:30:31,560
abides so likewise thou must ever be in resolution firm entrenched and when this eighth perfection

284
00:30:31,560 --> 00:30:38,520
gained a Buddha's wisdom shall be thine but now there must be more than these conditions which

285
00:30:38,520 --> 00:30:45,480
a Buddha makes still others will I seek to find that shall in Buddha ship mature and then I

286
00:30:45,480 --> 00:30:51,720
sought and found the ninth perfection which is called good will which mighty seers of former times

287
00:30:51,720 --> 00:30:59,240
had practiced and had followed come now this one as ninth adopt and practice it determinately

288
00:30:59,240 --> 00:31:09,240
unequaled by be in thy good will if thou to his wisdom what's the tain as water cleans it all

289
00:31:09,240 --> 00:31:15,400
alike the righteous and the wicked to from dust and dirt of every kind and with refreshing

290
00:31:15,400 --> 00:31:23,240
coolness pills so likewise thou both friend and foe alike with thy good will refresh and when

291
00:31:23,240 --> 00:31:30,520
this ninth perfection gained a Buddha's wisdom shall be thine but now there must be more than

292
00:31:30,520 --> 00:31:35,480
these conditions which a Buddha makes still others will I seek to find that shall in Buddha ship

293
00:31:35,480 --> 00:31:43,240
mature and then I sought and found the tenth perfection called equanimity which mighty seers

294
00:31:43,240 --> 00:31:50,120
of former times had practiced and had followed come now this one as tenth adopt and practice

295
00:31:50,120 --> 00:31:55,240
it determinately and when thou art of equal poise a Buddha's wisdom shall be thine

296
00:31:56,520 --> 00:32:01,880
just as the earth whatever is thrown upon her whether sweet or foul in different is to all

297
00:32:01,880 --> 00:32:10,840
alike no hatred shows nor amity so likewise thou in good or ill must ever even balanced ever be

298
00:32:11,480 --> 00:32:18,920
and when this tenth perfection gained a Buddha's wisdom shall be thine but earth no more conditions

299
00:32:18,920 --> 00:32:27,000
has that in the Buddha ship mature beyond these there are none to seek so practice these determinately

300
00:32:28,520 --> 00:32:33,400
now pondering these conditions tend their nature essence characters such fiery vigor had

301
00:32:33,400 --> 00:32:41,000
they all that all the worlds ten thousand quake then shook and creaked the wide wide earth as

302
00:32:41,000 --> 00:32:47,240
dothy sugar millet work then quake the ground as doth the wheel of oil mills when they're made to

303
00:32:47,240 --> 00:32:53,320
turn the entire assemblage that was there and followed in the Buddha's train trembled and shook

304
00:32:53,320 --> 00:33:00,680
and great alarm and fell astonished to the ground and many thousand water parts and pots and many

305
00:33:00,680 --> 00:33:06,280
hundred earth and earth and jars were one upon another dashed and crushed and pounded into dust

306
00:33:07,880 --> 00:33:13,720
excited tremble terrified confused and so repressed in mind the multitudes together came

307
00:33:13,720 --> 00:33:21,320
and two deep conqueror approached oh tell us what these signs portent will good or ill be tied

308
00:33:21,320 --> 00:33:31,160
the world low terror seizes hold on all dispel our fears all seeing one the great sage then

309
00:33:31,160 --> 00:33:38,040
deep conqueror a laden pacified their fears become for it and fear thee not for that the world

310
00:33:38,040 --> 00:33:44,600
death quake and shake of whom today I made proclaim a glorious Buddha shall he be he now conditions

311
00:33:44,600 --> 00:33:52,040
pondered which former conquerors fulfilled tis well on these he is intent as basis for the

312
00:33:52,040 --> 00:33:57,560
Buddha ship the ground in worlds ten thousand shakes in all the realms of gods and men

313
00:34:00,120 --> 00:34:05,160
when thus they'd heard the Buddha speak their anxious minds received relief and all then drawing

314
00:34:05,160 --> 00:34:11,640
near to me again they did me reverence thus on the road to Buddha ship and firm determined

315
00:34:11,640 --> 00:34:18,360
in my mind I raised me up from off my seat in reverence deep conqueror then as I raised me

316
00:34:18,360 --> 00:34:25,320
from my seat both gods and men in unison sweet flowers of heaven and flowers and earth of earth

317
00:34:26,040 --> 00:34:32,440
for profusely sprinkled on my head and gods and men in unison their great delight proclaimed

318
00:34:32,440 --> 00:34:39,400
aloud a mighty prayer thou now has made succeed according to thy wish from all misfortunes

319
00:34:39,400 --> 00:34:44,840
be thou free let every sickness disappear may as thou know hindrance ever know and highest

320
00:34:44,840 --> 00:34:52,680
wisdom soon achieve as when the time of spring has come the trees put forth their blood

321
00:34:52,680 --> 00:34:57,720
buds and flowers likewise thus thou oh hero great with knowledge of a Buddha bloom

322
00:34:57,720 --> 00:35:03,480
as all they who have Buddha's been the ten perfections have fulfilled likewise do thou

323
00:35:03,480 --> 00:35:09,480
oh hero great the ten perfections strive to gain as all they who have Buddha's been on

324
00:35:09,480 --> 00:35:14,760
wisdom's throne their insight gained likewise do thou oh hero great on conquerors throne

325
00:35:14,760 --> 00:35:21,880
thy insight gain as all they who have Buddha's been have made the doctrines wheel to roll likewise

326
00:35:21,880 --> 00:35:28,520
do thou oh hero great make doctrines wheel to roll once more as on the midday of the month the

327
00:35:28,520 --> 00:35:34,120
moon and full perfection shines likewise do thou with perfect mind shine brightly in ten thousand

328
00:35:34,120 --> 00:35:41,640
worlds as when the sun by rahoe freed shines forth exceeding bright and clear so thou and freed

329
00:35:41,640 --> 00:35:50,440
from ties of earth shine forth in brightly in bright magnificence just as the rivers of all lands

330
00:35:50,440 --> 00:35:56,360
into the ocean find their way make gods and men from every world approach and find their way

331
00:35:56,360 --> 00:36:06,120
to thee thus praise they me with glad acclaim and I beginning to fulfill the ten conditions of

332
00:36:06,120 --> 00:36:22,040
my quest re-entered then into the wood thank you that's the story of Sumeda

