1
00:00:00,000 --> 00:00:14,520
Here's the new bathrooms, as you can see the water is already connected, and one of the

2
00:00:14,520 --> 00:00:20,160
bathrooms is most finished.

3
00:00:20,160 --> 00:00:29,160
Here we have the toilet working, the sink working, and you can't really save with the shower

4
00:00:29,160 --> 00:00:30,160
also working.

5
00:00:30,160 --> 00:00:39,880
I'm going to turn on the light here, and this is the light, so I'm going to see the shower

6
00:00:39,880 --> 00:00:51,400
hand, shower tab, tab below, and that's it.

7
00:00:51,400 --> 00:00:56,000
The second bathrooms are still not ready.

8
00:00:56,000 --> 00:01:06,160
I'm going to put the plastic door on, so it seems to be a little bit of difficulty.

9
00:01:06,160 --> 00:01:19,520
So that's our bathroom project, still haven't started on the invitation hall, but it's

10
00:01:19,520 --> 00:01:22,320
going to be up here.

11
00:01:22,320 --> 00:01:37,280
We've got gravel and sand has already been moved, and I think fairly soon, if not this

12
00:01:37,280 --> 00:01:46,360
week, then probably more like next week, we're going to start on two cookies, as I mentioned,

13
00:01:46,360 --> 00:02:16,300
it's over here, so we have sand and gravel, we even have the old bricks, so we're going to

14
00:02:16,300 --> 00:02:25,440
put the old cookie, and we're going to put the two cookies right here in front of me,

15
00:02:25,440 --> 00:02:33,280
and we'll leave them facing the rock actually, so in between the cookie and the rock

16
00:02:33,280 --> 00:02:41,120
there will be a walking space for doing walking meditation, and this cookie up here we

17
00:02:41,120 --> 00:02:50,560
might leave for the time being working in the future, so that's all another update.

