1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Damapada.

2
00:00:05,000 --> 00:00:13,000
Today we continue on with verse 157, which reads as follows.

3
00:00:13,000 --> 00:00:42,000
It means that one knows that the self is dear.

4
00:00:42,000 --> 00:00:53,000
If one knows one's self to be dear to one's self, one holds one's self dear.

5
00:00:53,000 --> 00:00:57,000
One should guard one's self.

6
00:00:57,000 --> 00:00:59,000
Well, guard one's self.

7
00:00:59,000 --> 00:01:07,000
Rakey, a non-sorekita, guard one's self, and guard one's self well.

8
00:01:07,000 --> 00:01:17,000
In one or another of the Yam of the Watches,

9
00:01:17,000 --> 00:01:26,000
Patichagi, a Panditova, a wise person should guard themselves.

10
00:01:26,000 --> 00:01:35,000
Or should be mindful.

11
00:01:35,000 --> 00:01:39,000
If one holds one's self to be dear, one should guard one's self.

12
00:01:39,000 --> 00:01:43,000
At least in one of the three watches.

13
00:01:43,000 --> 00:01:45,000
I'll explain them afterwards.

14
00:01:45,000 --> 00:01:50,000
First, we have the story.

15
00:01:50,000 --> 00:01:51,000
There's two parts to the story.

16
00:01:51,000 --> 00:01:56,000
I'm not really sure what the first part has to do with anything.

17
00:01:56,000 --> 00:02:00,000
I'm suspicious that it was added on later.

18
00:02:00,000 --> 00:02:07,000
Let's just say it has nothing to do with Buddhism, so we're going to skip it.

19
00:02:07,000 --> 00:02:12,000
Except to say that there's this prince who has a questionable moral ethic.

20
00:02:12,000 --> 00:02:19,000
He said to have done some things in the past that were questionable ethics.

21
00:02:19,000 --> 00:02:21,000
That's all you need to know.

22
00:02:21,000 --> 00:02:27,000
This is about a prince, prince Bodhi.

23
00:02:27,000 --> 00:02:36,000
He built a castle and an honor of the building of this palace.

24
00:02:36,000 --> 00:02:41,000
He invited the Buddha and all the monks to come and eat.

25
00:02:41,000 --> 00:02:46,000
The common thing, the idea is it's a blessing.

26
00:02:46,000 --> 00:02:49,000
It's also a great first memory to have.

27
00:02:49,000 --> 00:02:55,000
It's a great way to establish your intentions.

28
00:02:55,000 --> 00:02:58,000
I mean, this guy did seem to have, and he even says himself,

29
00:02:58,000 --> 00:03:02,000
that he's taken refuge in the Buddha.

30
00:03:02,000 --> 00:03:09,000
So he had some good intentions.

31
00:03:09,000 --> 00:03:12,000
But he also had ulterior motives, even for inviting the Buddha.

32
00:03:12,000 --> 00:03:17,000
It turns out Bodhi, prince Bodhi wasn't able to have kids.

33
00:03:17,000 --> 00:03:24,000
He was married, so I wasn't able to conceive.

34
00:03:24,000 --> 00:03:29,000
So he thought, well, we'll have the Buddha come and give me a blessing.

35
00:03:29,000 --> 00:03:36,000
That'll help with this situation.

36
00:03:36,000 --> 00:03:43,000
What he did was apparently a common means of acquiring blessing

37
00:03:43,000 --> 00:03:51,000
was to have a spread out of white cloth

38
00:03:51,000 --> 00:03:59,000
and have a holy man come and step on the cloth

39
00:03:59,000 --> 00:04:03,000
as some kind of ancient tradition, some kind of blessing.

40
00:04:03,000 --> 00:04:06,000
It would be a blessing if they stepped on the cloth.

41
00:04:06,000 --> 00:04:09,000
So he spread these white cloths all throughout the palace

42
00:04:09,000 --> 00:04:12,000
so that the Buddha would have to step on it.

43
00:04:12,000 --> 00:04:15,000
And he thought, if he steps on it, then for sure I will.

44
00:04:15,000 --> 00:04:21,000
I will be blessed with a child.

45
00:04:21,000 --> 00:04:25,000
And so the Buddha, when he got to the palace

46
00:04:25,000 --> 00:04:30,000
with all the monks in Ananda, his attendant, standing beside him

47
00:04:30,000 --> 00:04:32,000
and behind him.

48
00:04:32,000 --> 00:04:37,000
He came to the door of the palace and he refused to enter.

49
00:04:37,000 --> 00:04:40,000
And Bodhi said, please, when it was her entrance,

50
00:04:40,000 --> 00:04:47,000
it stood silently.

51
00:04:47,000 --> 00:04:51,000
When the prince asks him again, please, if I'm real sure,

52
00:04:51,000 --> 00:04:55,000
the food is ready if you just go inside.

53
00:04:55,000 --> 00:04:58,000
He's getting a little anxious in the Buddha again refuses.

54
00:04:58,000 --> 00:05:05,000
When he asks a third time, what it turns to look at Ananda.

55
00:05:05,000 --> 00:05:09,000
When Ananda knows, of course, what the deal is.

56
00:05:09,000 --> 00:05:15,000
And he says to Bodhi, get rid of the carpets.

57
00:05:15,000 --> 00:05:18,000
Get rid of the white cloth.

58
00:05:18,000 --> 00:05:22,000
And Buddha will not step on those cloths.

59
00:05:22,000 --> 00:05:24,000
The story is also in the Vinaya.

60
00:05:24,000 --> 00:05:29,000
And it talks about how the Buddha originally, I think,

61
00:05:29,000 --> 00:05:33,000
forbid the monks as a result of this story from stepping on cloths

62
00:05:33,000 --> 00:05:36,000
because he didn't want to set this sort of precedents

63
00:05:36,000 --> 00:05:39,000
that the monks wouldn't be responsible.

64
00:05:39,000 --> 00:05:41,000
And then if he didn't give birth, then the monks

65
00:05:41,000 --> 00:05:42,000
wouldn't be in trouble.

66
00:05:42,000 --> 00:05:45,000
I mean, just to get caught up in this silly superstition.

67
00:05:45,000 --> 00:05:51,000
But later on, the monks, people, this was a tradition of sorts

68
00:05:51,000 --> 00:05:54,000
that you would put out a white cloth for the monks.

69
00:05:54,000 --> 00:05:57,000
And the Buddha said, go ahead and step on the cloths.

70
00:05:57,000 --> 00:05:59,000
Later, he eventually he amended these and go ahead

71
00:05:59,000 --> 00:06:03,000
and step on the cloths because lay people are superstitious.

72
00:06:03,000 --> 00:06:05,000
They said it like that.

73
00:06:05,000 --> 00:06:10,000
And it's not something we want to take an issue with.

74
00:06:10,000 --> 00:06:12,000
Let them have their superstition.

75
00:06:12,000 --> 00:06:17,000
There's not much you can do about it.

76
00:06:17,000 --> 00:06:20,000
The alternative would be to really upset them.

77
00:06:20,000 --> 00:06:23,000
Anyway, the Buddha didn't step on this cloth.

78
00:06:23,000 --> 00:06:27,000
And when Bodhi remembers, Bodhi removed all the cloths,

79
00:06:27,000 --> 00:06:29,000
the Buddha went into the palace, sat down to eat.

80
00:06:29,000 --> 00:06:34,000
And Bodhi asked him, look, I've been a lay follower of yours.

81
00:06:34,000 --> 00:06:37,000
She followed her viewers for a long time.

82
00:06:37,000 --> 00:06:40,000
Why would you step on the cloths when I put them out?

83
00:06:40,000 --> 00:06:41,000
Be a blessing to me.

84
00:06:41,000 --> 00:06:43,000
And the Buddha said, what were you thinking

85
00:06:43,000 --> 00:06:45,000
when you spread those cloths out?

86
00:06:45,000 --> 00:06:47,000
And he said, well, I was thinking and get it.

87
00:06:47,000 --> 00:06:50,000
I'd be able to have a child if you did.

88
00:06:50,000 --> 00:06:53,000
And I said, that's the reason I didn't step on the cloth.

89
00:06:57,000 --> 00:06:58,000
And he said, what's wrong?

90
00:06:58,000 --> 00:06:59,000
Why can't I have it?

91
00:06:59,000 --> 00:07:03,000
Why won't I be able to have a child?

92
00:07:03,000 --> 00:07:09,000
And was it all not in this life, been in the past life?

93
00:07:09,000 --> 00:07:15,000
You were negligent, up a mod.

94
00:07:15,000 --> 00:07:16,000
And he said, when?

95
00:07:16,000 --> 00:07:19,000
But I told him a story of the past life.

96
00:07:19,000 --> 00:07:27,000
And so the story of this past life story is interesting to us, I think.

97
00:07:27,000 --> 00:07:34,000
So once at one a time, there was a group of travelers

98
00:07:34,000 --> 00:07:37,000
on a boat, on a ship.

99
00:07:37,000 --> 00:07:40,000
And the ship sank.

100
00:07:40,000 --> 00:07:43,000
And everyone died, except for a husband and a wife,

101
00:07:43,000 --> 00:07:46,000
who ended up being shipwrecked on an island.

102
00:07:46,000 --> 00:07:49,000
And on this island, there was nothing to eat except birds.

103
00:07:49,000 --> 00:07:53,000
There were many, there was a large flock of birds.

104
00:07:53,000 --> 00:07:58,000
And so they began by eating the eggs of these birds,

105
00:07:58,000 --> 00:08:01,000
cooking them, I guess they had some way of making fire,

106
00:08:01,000 --> 00:08:03,000
but cooking them and eating the eggs.

107
00:08:03,000 --> 00:08:05,000
And eventually that wasn't enough.

108
00:08:05,000 --> 00:08:07,000
And so they started eating the young birds.

109
00:08:11,000 --> 00:08:13,000
And the Buddha said, they did this throughout their lives.

110
00:08:13,000 --> 00:08:15,000
They lived up their lives on this island.

111
00:08:15,000 --> 00:08:20,000
And at no point did they ever think that this was a bad thing.

112
00:08:20,000 --> 00:08:23,000
So they committed terrible atrocities against these birds,

113
00:08:23,000 --> 00:08:29,000
killing the baby birds, killing the unborn eggs.

114
00:08:29,000 --> 00:08:40,000
As a result, we're unable to have a child,

115
00:08:40,000 --> 00:08:43,000
not in that life in later lives.

116
00:08:43,000 --> 00:08:48,000
They were unable to have children.

117
00:08:48,000 --> 00:08:51,000
And he said, if you had, if at any time in your life,

118
00:08:51,000 --> 00:08:54,000
if when you were, you started off and not,

119
00:08:54,000 --> 00:08:57,000
if you had stopped at any time.

120
00:08:57,000 --> 00:08:59,000
He separates it into three parts.

121
00:08:59,000 --> 00:09:01,000
He said, when you were young,

122
00:09:01,000 --> 00:09:06,000
if you had refrained from murdering the birds,

123
00:09:06,000 --> 00:09:10,000
we would have been able to conceive if you had later on stopped

124
00:09:10,000 --> 00:09:11,000
or when you got older.

125
00:09:11,000 --> 00:09:13,000
And so he splits life up into three parts.

126
00:09:13,000 --> 00:09:16,000
And that's why this relates to the verse.

127
00:09:16,000 --> 00:09:18,000
Because the verse talks about the three watches,

128
00:09:18,000 --> 00:09:22,000
which are actually usually referred to the three watches of the night.

129
00:09:22,000 --> 00:09:24,000
Which is interesting.

130
00:09:24,000 --> 00:09:27,000
I mean, this verse may actually not be referring to this story,

131
00:09:27,000 --> 00:09:29,000
like the commentator says.

132
00:09:29,000 --> 00:09:34,000
I assume some people would be quite adamant that it doesn't.

133
00:09:34,000 --> 00:09:42,000
But either way, there's benefit of seeing it in both ways, I think.

134
00:09:42,000 --> 00:09:47,000
It's a simple story, but the story has to do with our practice.

135
00:09:47,000 --> 00:09:52,000
And in a couple of different ways.

136
00:09:52,000 --> 00:09:58,000
I mean, the main issue here is that it revolves around death and rebirth.

137
00:09:58,000 --> 00:10:00,000
Which I think is important.

138
00:10:00,000 --> 00:10:07,000
I mean, there's a lot to be said around this idea of rebirth.

139
00:10:07,000 --> 00:10:11,000
And Buddhism, we focus very much on the present moment.

140
00:10:11,000 --> 00:10:18,000
It's about what's going on right now in your body and in your mind.

141
00:10:18,000 --> 00:10:27,000
But at the same time, we live in a conceptual world.

142
00:10:27,000 --> 00:10:35,000
We live in a world of people, places, things.

143
00:10:35,000 --> 00:10:43,000
And we were born into a reality that is not permanent.

144
00:10:43,000 --> 00:10:46,000
That's the reality of being a human being.

145
00:10:46,000 --> 00:10:50,000
We were born and we will die.

146
00:10:50,000 --> 00:11:00,000
And so even with our practice of insight meditation and being in the present moment,

147
00:11:00,000 --> 00:11:11,000
a general understanding of the nature of birth and death.

148
00:11:11,000 --> 00:11:15,000
It's important not only to encourage us in our practice,

149
00:11:15,000 --> 00:11:26,000
but to help us understand the nature of reality itself, how it works.

150
00:11:26,000 --> 00:11:32,000
But there are consequences, and as this story says, there is power

151
00:11:32,000 --> 00:11:43,000
in intentional acts of cruelty and or of goodness.

152
00:11:43,000 --> 00:11:49,000
That the things we do here and now in the states of mind, which we cultivate,

153
00:11:49,000 --> 00:11:53,000
affect us, they change our whole reality.

154
00:11:53,000 --> 00:11:56,000
Just thinking how could it be?

155
00:11:56,000 --> 00:12:07,000
What is the mechanism by which a person who destroys the children of another species?

156
00:12:07,000 --> 00:12:13,000
Can thereby be fated to never have offspring of their own among other things?

157
00:12:13,000 --> 00:12:20,000
I mean, the cruelty is certainly to lead to certain to lead to other difficulties,

158
00:12:20,000 --> 00:12:25,000
or the mental and physical.

159
00:12:25,000 --> 00:12:33,000
The reason is the power of the mind,

160
00:12:33,000 --> 00:12:38,000
the power of the present to shape the future,

161
00:12:38,000 --> 00:12:46,000
and that all the power and all the stability,

162
00:12:46,000 --> 00:12:50,000
all of who we are here and now is impermanent.

163
00:12:50,000 --> 00:12:57,000
This body is breaking down.

164
00:12:57,000 --> 00:13:03,000
Death is around the corner.

165
00:13:03,000 --> 00:13:05,000
And after death, there's more.

166
00:13:05,000 --> 00:13:10,000
There's a new voyage, a new journey.

167
00:13:10,000 --> 00:13:16,000
If we haven't freed ourselves from all attachment,

168
00:13:16,000 --> 00:13:21,000
there's going to be a new journey to come.

169
00:13:21,000 --> 00:13:26,000
This makes us think about the things that we are doing to prepare ourselves

170
00:13:26,000 --> 00:13:29,000
for that moment, for that journey, for that future.

171
00:13:29,000 --> 00:13:32,000
It makes us think of how prepared we were in this life.

172
00:13:32,000 --> 00:13:33,000
What did we do?

173
00:13:33,000 --> 00:13:38,000
What have we come into this life with?

174
00:13:38,000 --> 00:13:44,000
To think of all the challenges and all the handicaps we've come,

175
00:13:44,000 --> 00:13:47,000
we've brought into this life,

176
00:13:47,000 --> 00:13:53,000
and what may have led to them in our past lives?

177
00:13:53,000 --> 00:14:11,000
So the thoughts around birth and death are quite important and quite useful for insight meditation.

178
00:14:11,000 --> 00:14:18,000
Sort of as a meta-analysis or a sort of a side reflection

179
00:14:18,000 --> 00:14:25,000
that will encourage us in our practice and direct us in our practice.

180
00:14:25,000 --> 00:14:35,000
It will serve as a good support for our practice.

181
00:14:35,000 --> 00:14:44,000
That we shouldn't end up like many of the figures in Buddhist literature and legend.

182
00:14:44,000 --> 00:14:48,000
Who neglected, who were negligent, who neglected the good,

183
00:14:48,000 --> 00:14:54,000
who lived their lives in the botchery and cruelty and evil.

184
00:14:54,000 --> 00:14:59,000
As a result, we're twisted and perverted in their minds,

185
00:14:59,000 --> 00:15:02,000
who became their worst enemy.

186
00:15:02,000 --> 00:15:15,000
And as a result, when they were born again, they had to suffer all sorts of handicaps and difficulties.

187
00:15:15,000 --> 00:15:23,000
Another thing that's interesting is the idea of guarding oneself in one watch.

188
00:15:23,000 --> 00:15:27,000
Again, I said this could refer to the three watches of the night.

189
00:15:27,000 --> 00:15:36,000
It's actually just a figure of speech referring to the three parts of life.

190
00:15:36,000 --> 00:15:42,000
But either way, it has the same message that it's the understanding that we can't always be mindful.

191
00:15:42,000 --> 00:15:46,000
And this comes up in various places in the Buddha's teaching.

192
00:15:46,000 --> 00:15:50,000
When you're eating, it can be quite difficult to be mindful.

193
00:15:50,000 --> 00:15:54,000
When you have work to do, maybe you have to build something,

194
00:15:54,000 --> 00:15:58,000
or you have to clean something, to do something.

195
00:15:58,000 --> 00:16:02,000
It can be quite difficult to be mindful.

196
00:16:02,000 --> 00:16:05,000
And the Buddha said the inclination as well.

197
00:16:05,000 --> 00:16:08,000
I worked really hard, so now I should take a rest.

198
00:16:08,000 --> 00:16:11,000
And the Buddha said, well, what you should really be thinking is I worked really hard,

199
00:16:11,000 --> 00:16:13,000
and it was difficult to be mindful.

200
00:16:13,000 --> 00:16:22,000
So now that I'm done, I should make twice as much effort to be mindful to make up for it.

201
00:16:22,000 --> 00:16:31,000
And when you're eating as well, I was eating, and that was a lot of work.

202
00:16:31,000 --> 00:16:36,000
Now I should take a rest, but now I should think when I was eating, I couldn't be mindful.

203
00:16:36,000 --> 00:16:38,000
So now I should be twice as mindful.

204
00:16:38,000 --> 00:16:43,000
I think twice as much effort.

205
00:16:43,000 --> 00:16:46,000
But the idea here is that we can expect to be perfect.

206
00:16:46,000 --> 00:16:52,000
And we have to understand that it's not magic, or it's not a mindfulness.

207
00:16:52,000 --> 00:16:56,000
It's not some kind of a trick.

208
00:16:56,000 --> 00:17:00,000
Mindfulness is a training, and you have to figure that out for yourself.

209
00:17:00,000 --> 00:17:03,000
You have to work at it as a training.

210
00:17:03,000 --> 00:17:12,000
When you're walking and sitting, and you're noting the senses and the mind states,

211
00:17:12,000 --> 00:17:21,000
feelings and movements of the body, all of this is changing your habits,

212
00:17:21,000 --> 00:17:36,000
changing the way you behave, and changing the way you look at your body and the world around you.

213
00:17:36,000 --> 00:17:39,000
And so it's definitely something that should be done regularly,

214
00:17:39,000 --> 00:17:47,000
and ideally it should be done constantly.

215
00:17:47,000 --> 00:17:59,000
But there's certainly room for this, or it's not, we shouldn't disregard

216
00:17:59,000 --> 00:18:09,000
a sort of occasional practice or partial practice.

217
00:18:09,000 --> 00:18:17,000
When you're carrying baggage, and when you have to go do some work,

218
00:18:17,000 --> 00:18:20,000
you put the bags down and you go and you do your work,

219
00:18:20,000 --> 00:18:22,000
and then when you're done, you pick the bags up again.

220
00:18:22,000 --> 00:18:24,000
Meditation is the same.

221
00:18:24,000 --> 00:18:27,000
Sometimes you can't be mindful, so you put it down,

222
00:18:27,000 --> 00:18:31,000
but when you're done the work, you've still got this weight, this burden to carry,

223
00:18:31,000 --> 00:18:32,000
which is the meditation.

224
00:18:32,000 --> 00:18:37,000
So you go back and you pick them up where you left off.

225
00:18:37,000 --> 00:18:45,000
There's a power in the practice.

226
00:18:45,000 --> 00:18:56,000
It's not something that you have to continuously do.

227
00:18:56,000 --> 00:18:58,000
It's something that changes.

228
00:18:58,000 --> 00:19:02,000
If you practice an hour a day, two hours a day, it can be in great support.

229
00:19:02,000 --> 00:19:05,000
It can make great changes in your life.

230
00:19:05,000 --> 00:19:08,000
You'll see it spills over into your daily life.

231
00:19:08,000 --> 00:19:11,000
Change is the rest of your day.

232
00:19:11,000 --> 00:19:15,000
Even if you just do a half an hour a day, it's a great start.

233
00:19:15,000 --> 00:19:19,000
Just having the intention to practice,

234
00:19:19,000 --> 00:19:26,000
where you sit down every day and do 10 minutes, 20 minutes, half an hour,

235
00:19:26,000 --> 00:19:33,000
it'll be a great benefit.

236
00:19:33,000 --> 00:19:41,000
The real problem is when we don't spend any time developing ourselves.

237
00:19:41,000 --> 00:19:47,000
I think we should be encouraged by the fact

238
00:19:47,000 --> 00:19:51,000
that we are keen and interested in our development.

239
00:19:51,000 --> 00:19:56,000
It'll be discouraged by sometimes how little we can practice.

240
00:19:56,000 --> 00:19:59,000
If you work all day or if you have a family,

241
00:19:59,000 --> 00:20:01,000
you have to take care of it.

242
00:20:01,000 --> 00:20:03,000
Sometimes it can seem discouraging.

243
00:20:03,000 --> 00:20:06,000
You just want to run away and go and live in the forest.

244
00:20:06,000 --> 00:20:08,000
Of course there's nothing wrong with that.

245
00:20:08,000 --> 00:20:23,000
We shouldn't be discouraged by having just partial time or just some time to practice.

246
00:20:23,000 --> 00:20:29,000
There's a great difference between someone who practices part of their day being mindful

247
00:20:29,000 --> 00:20:33,000
and someone who doesn't practice so anyway.

248
00:20:33,000 --> 00:20:39,000
Simple story, simple verse, meaning of it is guard yourself at times,

249
00:20:39,000 --> 00:20:44,000
spend some time cultivating goodness and awesomeness.

250
00:20:44,000 --> 00:21:11,000
Thank you all for tuning in. Have a good night.

