1
00:00:00,000 --> 00:00:02,000
Changes of insight

2
00:00:02,800 --> 00:00:08,520
Question. Can you explain what an insight is in regards to meditation?

3
00:00:08,520 --> 00:00:18,000
Is it a thought that arises that causes you to let go or does it occur at a deeper level also when a person reaches the

4
00:00:18,000 --> 00:00:22,520
sort of another state of mind? Is that a distinct event in time?

5
00:00:22,520 --> 00:00:36,520
Answer. What is an insight? There are two kinds of insight. The first kind of insight is that the intellectual level the one has an epiphany or intellectual realization.

6
00:00:36,520 --> 00:00:44,040
The second kind of insight is a meditative experience or reality for what it is.

7
00:00:44,040 --> 00:00:57,040
A realization based on meditation practice. It is an experience not an intellectual thought. There is no need for thoughts because one sees things as they are.

8
00:00:57,040 --> 00:01:05,040
There are 16 stages of insight, but one realizes in the practice of mindfulness meditation.

9
00:01:05,040 --> 00:01:14,040
One practices intensively for long enough. It can be expected to go through all 16 of these in order.

10
00:01:14,040 --> 00:01:20,040
One, the first stage is called Nama Rupa Parichet Danyana.

11
00:01:20,040 --> 00:01:24,040
Insight into the separation between body and mind.

12
00:01:24,040 --> 00:01:37,040
At this stage, a meditative sees that they are being separated into two parts. One being physical and the other mental one sees that there is no self or soul.

13
00:01:37,040 --> 00:01:45,040
Only these two realities of the physical and mental phenomena that arise and sees every moment.

14
00:01:45,040 --> 00:01:59,040
Two, the second stage, Parichet Danyana is seeing the nature of the physical and mental phenomena and how they went together based on course and effects.

15
00:01:59,040 --> 00:02:15,040
Three, the third stage is called Sama San Nanyana. This is translated as knowledge of comprehension. It is where one begins to see the three characteristics in parents in every a reason phenomenon.

16
00:02:15,040 --> 00:02:31,040
In permanence, suffering and not soul, why nothing in the world is worth thinking to the things that we thought were permanent that are impermanent, the things that we thought were satisfying and not satisfying us because they are impermanent.

17
00:02:31,040 --> 00:02:46,040
Together, they are not under our control and we cannot force reality to be this way or the way we wish it to be. For the fourth stage is Udaya Para Nanyana, knowledge of arising and ceasing.

18
00:02:46,040 --> 00:03:04,040
At this stage, one is able to clearly break reality into moments of experience that arise and sees rather than seeing things as good or bad, one simply sees them as arising and ceasing incessantly.

19
00:03:04,040 --> 00:03:19,040
The fifth stage is the knowledge of cessation, realizing that everything that arises must see, focusing on cessation and process upon the meditator, the reality of impermanence.

20
00:03:19,040 --> 00:03:33,040
Six, the sixth stage is Banyanyana, knowledge of danger or fiasomeness. At this stage, one begins to realize that there is a problem with clinging to the impermanent.

21
00:03:33,040 --> 00:03:55,040
Seven, the seven stage is Adina Wanyanyana, knowledge of the disadvantages of clinging. This is a direct result of experiencing the problem in the previous stage. Here, one comes to the conclusion that clinging is to blame for our precarious situation.

22
00:03:55,040 --> 00:04:19,040
Eight, the eighth stage is Nibi Danyanyana, knowledge of disenchantment. At this stage, one becomes disenchanted with reality, turning away from clinging to experiences, realizing that there is nothing in the world which is really unique, exceptional or special in any way.

23
00:04:19,040 --> 00:04:40,040
One begins to see that in the end, it is all just seeing, hearing, smelling, tasting, feeling or thinking. One begins to turn away from a reason phenomena, realizing that happiness does not fly in anything that arises.

24
00:04:40,040 --> 00:05:01,040
Nine, the ninth stage is Nibi Danyanyana, knowledge of desire for release or freedom. At this stage, the meditator begins to actively move away from a reason, experience, no longer looking into external objects for peace and happiness.

25
00:05:01,040 --> 00:05:17,040
Ten, the tenth stage is Patissan Kanyanyana, it involves oneness, objective introspection based on the area, realizations, that's free, the meditator from active buyers.

26
00:05:17,040 --> 00:05:31,040
Patissan Kanyan's going over everything again, this time with an unbiased perspective trying to see where exactly you are clinging rather than where you can find happiness.

27
00:05:31,040 --> 00:05:51,040
11, the eleventh stage is Nibi Danyanyana, knowledge of equanimity. As your perception changes instead of trying to find happiness in experiences, your mind begins to feel equanimals in regard to all a reason phenomena.

28
00:05:51,040 --> 00:06:03,040
Nine, in this stage, is finally tuned and alert, interacting with experience, without reacting to it. This is the pinnacle of mundane insights.

29
00:06:03,040 --> 00:06:26,040
12, after Sankar Upekar Nanyanyana, the arises several knowledges in succession. The next one is a Nulomanyanyanyana by the mind reaches its finest points. It is the consummation of insight where the mind is so finely tuned that you are able to see things exactly as they are.

30
00:06:26,040 --> 00:06:36,040
This is the moment of clarity that we are aiming for. A Nulomanyanyanyanyany means the mind that goes with the grain of truth as opposed to going against the grain.

31
00:06:36,040 --> 00:07:03,040
It means being finally in tune with the way things are. This knowledge is just a brief moment in time. 13, the next knowledge is called Bocha Bunyanna where in one changes one's lineage. Changes of lineage means changing one's state of mind from being an ordinary human being to being one who has realized the ultimate truth.

32
00:07:03,040 --> 00:07:29,040
The first stage marks the division between the last moments of a Nulomanyanyanyana, and the moments after 14. The next moment is called Maganyanyanyanya at the end of the part where one's mind goes inwards rather than following apart- outward. Maganyanyanyanyanyany is surpassed inwards the first moments of freedom from suffering.

33
00:07:29,040 --> 00:07:36,360
15th, the moments that follow a cold violin in a realising of the fruits of the part,

34
00:07:36,360 --> 00:07:40,920
they have the same nature as the first moment, but a cold fruit.

35
00:07:40,920 --> 00:07:46,040
Because the first moment has already brought about freedom.

36
00:07:46,040 --> 00:07:51,360
16th, the final stage is called Pachawakana yana,

37
00:07:51,360 --> 00:07:54,920
weighing one reflex upon the previous stages.

38
00:07:54,920 --> 00:08:01,160
After imaging from the realisation of Nibbana, one reflex on the results,

39
00:08:01,160 --> 00:08:07,080
seeing what has changed in the mind as a result of the realising.

40
00:08:07,080 --> 00:08:11,600
Sotapana is attained at the moment of realising,

41
00:08:11,600 --> 00:08:16,600
the 14th stage of insight Magañana for the first time.

42
00:08:16,600 --> 00:08:22,600
Once one attains that stage, one is said to be the first type of enlightened.

43
00:08:22,600 --> 00:08:28,480
The name for this is Sotapi Maga, realisation of the path of Sotapana.

44
00:08:28,480 --> 00:08:32,640
The next moments will be attainment of phthaniñana,

45
00:08:32,640 --> 00:08:38,240
one becomes the second type of holy person called Sotapati Phala,

46
00:08:38,240 --> 00:08:41,000
or fruition of Sotapana.

47
00:08:41,000 --> 00:08:45,600
From that moment on, one is considered to be a Sotapana.

48
00:08:45,600 --> 00:08:50,600
One who has entered the stream, the meaning is that these realisations

49
00:08:50,600 --> 00:08:55,080
have fundamentally changed the person's outlook on reality.

50
00:08:55,080 --> 00:09:00,400
There is no going back to an ordinary state of being for that person.

51
00:09:00,400 --> 00:09:05,360
They are considered to have entered the stream to freedom from suffering,

52
00:09:05,360 --> 00:09:35,200
and will be born a maximum of seven more lifetimes in samsara.

