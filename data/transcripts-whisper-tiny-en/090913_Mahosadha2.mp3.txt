Okay, so now we get into the tests.
What we had first were the seven cases which the Bodhisattva, I mean there are examples
of cases which the Bodhisattva would have tried as a seven year old child.
Now I'm very happy that I'm allowed to do this because I could never get away with this
in an ordinary dhamma talk with most people, they'd be like, where's this guy coming
from this guy, this guy's crazy talking about this stuff.
But here we are in the world of what one might call make beliefs so we can get away with
a lot of things which are difficult to believe.
Not that I don't believe them, I think that they can all possibly be true.
I wasn't there and I don't know.
But for most people it's quite difficult to believe this sort of things I think so I'm
happy to have the chance.
So now we get into the tests, now Seneca comes up with all these things that the Bodhisattva
would be unable to do, unable to solve.
And some of them as for the court cases, some of you are familiar with biblical literature
as my mother pointed out to me when I told her about the story of the mother and the
mother and the goblin.
She said it's the story of Samson and I'd never heard the story of Samson, so I'm not
Samson is that?
I have the right one, Samson, King Solomon, Solomon, sorry, King Solomon.
If anyone knows these two women brought a child to King Solomon, yeah.
And what did King Solomon do, he picked up a sword and said, fine I'll split it in half
for you.
And the mother said, my god, no give it to the other woman then, of course, because she
was the real, your heart was the heart of a mother.
And so I'm not sure whether there was possibly some borrowing going on there, it's
an interesting topic to research.
I read that a little research couldn't find much out, but quite interesting anyway.
The rest of these I've actually seen in other places a couple of them, so anyway I'll
get right into it.
Number eight, the pole.
So one day with a view of testing the sage they fetched in a quesia pole and cutting off
about a span they had it nicely smoothed by a turner and sent it to the east market town
with this message.
The people of the market town have a name for wisdom.
Let them find out then which end is the top and which the root of this stick.
If they cannot, there is a fine of a 10,000 pieces, a thousand pieces of gold.
The people gathered together, but could not find it out.
And they said to their foreman, perhaps Mahosad had a sage with no send and ask him.
The foreman sent for the sage from his playground and told him the matter how they could
not find it out, but perhaps he could.
The sage thought in himself a king could gain nothing from knowing which is the top and
which is the root no doubt it is sent to test me.
He said bring it here my friends I will find out.
Holding in his hand he knew which was the top and which was the root yet to please the
heart of the people he sent for a pot of water and tied a string around the middle of
the stick and holding it by the end of the string.
He let it down to the surface of the water, the root being heavier sank first.
Then he asked the people is the root of a tree heavier or the top, the root wiser.
See then this part sinks first and this is therefore the root.
By this mark he distinguished the root from the top, the people sent it back to the king
distinguishing which was the root and which was the top.
The king was pleased and asked who has found it out.
They said the sage Maho said that son of four men siri wata.
The king turned to Seneca and asked shall we send for him?
Seneca as usual replied, wait my lord, let us try him in another way.
Number nine the head.
One day two heads were brought, one of women's and one of men's.
These were sent to be distinguished with a fine of a thousand pieces in case of failure.
The villagers could not decide and ask the great being.
He recognized them at sight of course, because as they say the sutures in a man s head
are straight and a woman and a woman s head they are crooked.
By this mark he told which was which and they sent it back to the king the rest is as
before.
Number ten the snake.
One day a male and a female snake were brought and sent for the villagers to decide which
was which.
They asked the sage and he knew at once when he saw them for the tail of the male snake
is thick, that of the female is thin.
The male snake s head is thick, the female is long.
The eyes of the male are big of the female small.
The head of the male is rounded, that of the female cut short.
By these signs he distinguished male from female the rest is as before.
Number eleven the cock.
One day a message was sent to the people of the east market town to this effect.
Send us a bull, white all over with horns on his legs and a hump on the head which
utters his voice at three times unfailingly otherwise there is a fine of a thousand pieces.
Not knowing when they asked the sage he said the king means you to send him a cock.
This creature has horns on his feet, the spurs, a hump on his head, the crest and
growing thrice utters his voice at three times unfailingly.
Then send him a cock such as he describes they sent one.
Number twelve the gem.
The gem which sack of the king of the gods gave to kinkuso was octagonal, octagonal.
His thread was broken and no one could remove the old thread which was strung through
it to put in a new.
The hole was far too small.
One day they sent his gem with directions to take out the old thread and to put in a new.
The villagers could do neither, neither the one nor the other, neither take out the old
thread nor put in a new and in their difficulty they told the sage.
He made them fear nothing and asked for a lump of honey.
Despite this he smeared the two holes in the gem.
In twisting a thread of wool he smeared the end of this also with honey.
He pushed it a little way into the hole and put it in a place where ants were passing.
The ants smelling the honey came out of their hole and eating away the old thread, bit
hold of the end of the wool and thread and pulled it out at the other end.
When he saw that it had passed through he bade them presented to the king who was pleased
when he heard how the thread had been put in.
Number 13 the calving.
The royal bull was fed up for some months so that his belly swelled out.
His horns were washed, he was anointed with oil and bathed with turmeric and then they
sent him to the east market town with this message.
You have a name for wisdom, here is the king's royal bull in calf, pregnant.
Deliver him and send him back with the calf or else there is a fine of a thousand pieces.
The villagers perplexed what to do, applied to the sage, who thought fit to meet one question
with another and asked, can you find a bold man able to speak to the king?
That is no hard matter they replied.
So they summoned him and the great being asked, go my good man, said to him, go my good
man, let your hair down loose over your shoulders and go to the palace gate weeping and
lamenting sore.
Answer none but the king only lament.
And if the king sends for you to ask why you lament, say this, seven days my son is in
labor and cannot bring forth, oh help me tell me how I may deliver him.
Then the king will say, what madness this is impossible men do not bear children.
Then you must say if that be true then how can the people of the east market town deliver
your royal bull of a calf?
As he was bitten so he did.
The king asked who thought of that counter-quippin on hearing that it was the sage Mahosada
he was pleased.
Number 14 the boiled rice.
Another day it has the sage this message was sent.
The people of the east market town must send us some boiled rice cooked under eight conditions
and these are, without rice, without water, without a pot, without an oven, without fire,
without firewood, without being sent along a road either by woman or man.
If they cannot do it there is a fine of a thousand people, a thousand pieces.
The people perplexed applied to the sage who said be not troubled, take some broken rice
for that is not rice, snow for that is not water, an earthen bull which is no pot, chop
up some wood blocks which are no oven, kindle fire by rubbing instead of a proper fire,
take leaves instead of firewood, cook your sour rice, put it in a new vessel, press
it well down, put it on the head of a eunuch who is neither man nor woman, leave the
man road and go along a footpath and take it to the king.
They did so and the king was pleased when he heard by whom the question had been solved.
Number fifteen, the tank, the sand.
Another day to test the sage they sent this message to the villagers.
The king wishes to amuse himself in a swing and the old rope is broken.
You are to make a rope of sand or else pay a fine of a thousand pieces.
They knew not what to do and appealed to the sage who saw that this was the place for
a counter question.
He reassured the people in sending for two or three clever speakers.
He made them go to tell the king.
My lord, the villagers do not know whether the sand rope is to be thick or thin.
Send them a bit of the old rope, a span long or four fingers.
This they will look at and twist a rope of the same size.
If the king replied, sand rope there never was in my house.
They were to reply if your majesty cannot make a sand rope, how can the villagers do so?
They did so and the king was pleased on hearing that the sage had thought up this counter
quit.
Number sixteen, the tank.
Another day the message was, the king desires to support him in the water.
You must send me a new tank, covered with water lilies of all five kinds, otherwise there
is a fine of a thousand pieces.
They told the sage who saw that a counter-quip was wanted.
He sent for several men clever at speaking and said to them, go and play in the water till
your eyes are red, then go to the palace door with wet hair and wet garments and your body
is all over with covered all over with mud, holding in your hands ropes, staves and clods.
One word to the king of your coming and when you are admitted say to him, sire, in as
much as your majesty is ordered, the people of the East Market town descend you a tank.
We brought a great tank to suit your taste, but she being used to a life in the forest,
no sooner saw the town with all its walls, moats and watch towers.
Then she took fright and broke the ropes and off into the forest.
We pelted her with clods and beat her with sticks but could not make her come back.
She was then the old water tank which her majesty has said to have brought from the forest
and we will yoke them together and bring the other back.
The king will say, I never had a tank brought in from the forest and never send a tank
there to be yoked and bring in another.
Then you must say of that is, so how can the villagers send you a tank?
They did so and the king was pleased to hear that the sage had thought of this.
Again, on a day the king sent a message, I wish to deport me in the park and my park
is old.
The people of the East Market town must send me a new park filled with trees and flowers.
The sage reassured them as before and sent men to speak in the same manner as above.
Then the king was pleased and said to Seneca, while Seneca shall we send for the sage.
But Seneca, of course, still grudging the other's prosperity, said, that is not all that
makes a sage wait, wait.
On hearing this, the king thought to himself.
The sage Mohosa, that was wise, even as a child, and took my fancy.
In all these mysterious tests and counter-quips, he has given answers like a Buddha.
He has such a wise man as this Seneca will not let me summon him to my side.
What care I for Seneca, I will bring the man here.
So with a great following, he set out for the village mounted upon his royal horse.
But as he went, the horse put his foot into a hole and broke its leg.
So the king turned back from that place to the town.
Then Seneca entered the presence and said, Sire, did you go to the east market town to bring
the sage back?
Yes, said the king.
Sire said Seneca, you make me as one of no account.
I begged you to wait a while, but off you went in a hurry and at the outset of your route,
at the outset, your royal horse broke his leg.
The king had nothing to say to this.
Again on the day he asked Seneca, shall I send for the sage Seneca?
Seneca, not having any further rebuttal, with which to give said, if so, your majesty,
don't go yourself.
But send a messenger saying, O sage, as I was on my way to fetch your you, my horse,
broke his leg, send us a better horse and a more excellent one.
If he takes the first alternative, he will come himself.
If the second he will send his father, then there will be a problem to test him.
The king sent a messenger with this message.
The sage on hearing had recognized that the king wished to see himself and his father.
So he went to his father and said greeting him, Father, the king wishes to see both you and me.
You go first with a thousand merchants in attendance, and when you go, go not empty handed,
but take a sandalwood casket filled with fresh ghee.
The king will speak kindly to you and offer you a householder seat.
Take it and sit down.
Once you are seated, I will come.
The king will speak kindly to me and offer me such another seat.
Then I will look at you.
Take the cue and say, rising from your seat, son,
Moho said otherwise, take this seat.
Then the question will be ripe for solution.
He did so.
On arriving at the palace door,
his father caused his arrival to be known to the king,
and on the king's invitation entered, greeting the king and standing to one side.
The king spoke kindly to Siriwata,
and asked him where was his son, the wise Moho said,
Seeiwata replied, coming after me, my lord.
The king was pleased to hear of his coming and bathed the father sit in a suitable place.
He found a place and sat there.
Meanwhile, the great being dressed himself in all his splendor and attended by a thousand youths,
he came seated in a magnificent chariot.
As he entered the town, he be held and asked by the side of a ditch.
And he directed some stout fellows to fasten up the mouth of the ass,
so that it should make no noise, to put him in a bag and carry him on their shoulders.
They did so.
The Bodhisattva entered the city with his great company.
The people could not praise him enough.
This they cried as the wise Moho said,
The merchant, Siriwata, his son.
This, they say, is he who was born holding our herb of virtue in his hand.
He, it is, who knew the answers to so many problems, set to test him.
On arriving before the palace, he sent in word of his coming.
The king was pleased to hear it and said,
Let my son, the wise Moho said, I make haste to come in.
So with his attendance, he entered the palace and saluted the king and stood on one side.
The king delighted to see him, spoke to him very sweetly,
and bade him to find his seat and sit down.
Moho said, I looked at his father and his father,
at this queue, up rose from his seat and invited him to sit there,
which Moho said I did.
Thereupon the foolish men who were there, Seneca,
Bukusa, Kavinda, Devinda, and others.
Seeing him sit there, clap their hands and laughed loudly and cried.
This is the blind fool they call wise.
He has made his father rise from his seat and sits there himself.
Wise, he should not be called surely.
The king also was crestfallen.
Then the great being said, why my lord are you sad?
Wise, sir, I was glad to hear of you, but to see you, I am not glad.
I was glad to hear of you, but to see you, I am not glad.
Wise, so great king.
Because you have made your father rise from his seat and sit there yourself.
What, my lord, do you think that in all cases the sire is better than the sons?
Yes, wise, sir.
Did you not send word to me to bring you the better horse or the more excellent horse?
So, saying he rose up and looking towards the young fellow said,
bring in the ass you have brought.
Placing this ass before the king, he went on sire, what is the price of this ass?
The king said, if it be serviceable, it is worth eight rupees.
But if he get a mule cold out of a thoroughbred sinned mare, what will the price of it be?
Oh, it will be priceless.
Why do you say that, my lord, have you not just said that in all cases the sire is better than the sons?
By your own saying the ass is worth more than the mule cold.
Now have not your wise men clapped their hands and laughed at me because they did not know that?
What wisdom is this of your wise men? Where did you get them?
And in contempt for all four of them, you address the king in this stanza.
Thinkest thou that the sire is always better than the son, no excellent king?
Then he is young creature better than the mule, the ass is the mule sire.
As after this said he went on, my lord, if the sire is better than the son, take my sire into your service.
If the son is better than the sire, take me.
The king was delighted and all the company cried out of plotting and praising a thousand times.
Well indeed, as the wise men solved the question,
there was cracking of fingers and waving of a thousand scarves, the four ministers were crestfallen.
Now no one knows better than the Bodhisattva, the value of parents.
If one asks then why he did this?
It was not to throw contempt on his father, but when the king sent the message,
send the better horse or the more excellent horse.
He did this in order to solve that problem and to make his wisdom to be recognized
and to take the shine out of the four sages.
The king was pleased in taking a golden vase filled with scented water,
poured the water upon the merchant's hand saying,
enjoy the east market town as a gift from the king.
Let the other merchants he went on be subordinate to this one.
This then he sent to the mother of the Bodhisattva all kinds of ornaments.
Delighted as he was at the Bodhisattva's solution of the asked question.
He wished to make the Bodhisattva as his own son and to the father said,
good sir, give me the great being as my son.
The father replied, sire, very young as he still, even yet his mouth smells of milk.
But when he is old he shall be with you.
The king said, however, good sir, henceforth you must give up your attachment to the boy.
From this day he is my son.
I can support my son so go your ways.
Then he sent him away.
The father did obeisance to the king and embraced his son.
Then throwing his arms about him, kissed him up on the hand and gave him good counsel.
Such were the ways of kings in this time.
The boy also bade his father farewell and begged him not to be anxious and sent him away.
The king then asked the sage whether he would take his meals inside the palace or without it.
Mahosada, seeing that with so large a retinue, were best to have his meals outside the palace,
replied to that effect.
Then the king gave him a suitable house and provided for the maintenance of the thousand youths and all,
gave him all what that was needful.
From that time the sage attended upon the king.
9.
Now the king desired to test the sage.
At that time there was a precious jewel in a crow's nest on a palm tree which stood on the bank of the lake near the southern gate.
In the image of this jewel was to be seen reflected upon the lake.
They told the king that there was a jewel in the lake.
He sent for Seneca saying they tell me there is a jewel in the lake.
How are we to get it?
Seneca said the best ways to drain out the water.
The king instructed him to do so and he collected a number of men and got out the water in mud and dug up the soil at the bottom but no jewel could he see.
But when the lake was full again there was the reflection of the jewel to be seen once more.
Again Seneca did the same thing and found no jewel.
So the king sent for the sage Mahosadai and said a jewel has been seen in a lake and Seneca has taken out the water in the mud and dug up the earth without finding it.
But no sooner is the lake full than it appears again.
Can you get hold of it?
Mahosadai replied that is no hard task sir.
I will get it for you.
The king was pleased at his promise and with a great following event a lake ready to see the might of the sage's knowledge.
The great being stood in the bank and looked he perceived the jewel is not in the lake but must be in the tree and he said aloud.
Sire there is no jewel in the tank.
What is it not visible in the water?
So he sent for a pale of water and said now my large sea.
Is not this jewel visible both in the pale and in the lake?
Then where can the jewel be?
Sire.
It is the reflection which is visible both in the lake and in the pale but the jewel is in a crow's nest in this palm tree.
Send up a man and have it brought down.
The king did so the man brought down the jewel and the sage put it into the king's hand.
All the people applauded at the sage and mocked at Seneca.
Here is a precious jewel in a crow's nest up a tree and Seneca makes strong men dig out the lake.
Surely a wise man should be like Mahosadai.
Thus they praised the great being and the king was delighted with him.
And gave him a necklace of pearls from his own neck and strings of pearls to the thousand boys.
And to him in his retina he granted the right to wait upon him without ceremony.
Again one day the king went into the park.
When a chameleon which lived on the top of the arched gateway saw the king approaching and came down and lay flat upon the ground.
The king seeing this asked what is he doing wiser?
Paying respect to you Sire.
If so not let his service go without reward.
Sire a large s is of no use to him.
All he wants is something to eat.
And what does he eat?
Meet Sire.
How much odd he to have?
A farthing's worth Sire.
A farthing's worth is no gift from the kings of the king.
And he sent a man with orders to bring regularly and give to the chameleon a half and is worth of meat.
This was done forever thereafter.
But on a fast day when there was no killing the man could find no meat.
So he boarded a hole through the half end of the piece and strung it upon a thread and tied it upon the chameleon's neck.
This was the fast day when people would, this would be the holiday when people would do no killing.
And so the man finding no meat took the half end of the piece, which is a coin.
More a hole in it and strung it upon a thread tied it to the chameleon's neck.
This made the creature proud.
That day the king again went into the park, but the chameleon as he saw the king drawn near in pride of wealth made himself equal to the king, thinking within himself.
He may be very rich by the day, but so am I.
So he did not come down, but lay still on the archway stroking his head.
The king seeing this said, why sir, this creature does not come down today as usual.
What is the reason? And he recited the first stanza.
You chameleon used not to climb upon the archway.
Explain Moho said that why the chameleon has become stiff necked.
The sage perceived that the man must have been unable to find meat on this fast day when there was no killing.
And that the creature must have become proud because of the coin hung about his neck.
So he recited this stanza.
The chameleon has got what he never had before, a half-and-a-piece,
hence he despises Videha, Lord of Matila.
The king sent for the man in question him, and he told them all about it truly.
Then he was more than ever pleased with the sage who it seemed.
He knew the idea of the chameleon without asking any questions with a wisdom like the supreme wisdom of a buddha.
So he gave him the revenue taken at the four gates.
Being angry with the chameleon, he thought of discontinuing the gift,
but the sage told him it was unfitting and dissuaded him to do so.
Now Allah and Pangutra lived in Matila, living in Matila, came to Takasila and studied under a famous teacher,
and soon completed his education.
Then after diligent study, he proposed to take leave of his teacher and go.
But in this teacher's family, there was a custom that if there should be a daughter right for marriage.
She should be given to the eldest pupil.
This teacher had a daughter beautiful as a nymph divine, so he said,
My son, I will give you my daughter and you shall take her with you.
Now this lad was unfortunate and unlucky, but the girl was very lucky.
When he saw her, he did not care for her, even though she was the most beautiful woman in the land.
But though he thought so, and said so,
he agreed, not wishing to disregard his master word, master's words,
and the Brahmin married the daughter to him.
Night came when he lay up on the prepared bed,
no sooner had she got into the bed, then he...
Up he got, groaning and lay down upon the floor.
She got out and lay beside him, then he got up and went to the bed again.
When she came into the bed again, he got out, for ill luck cannot meet with good luck.
So the girl stayed in the bed and he stayed on the ground.
Thus they spent seven days.
Then he took leave of his teacher and departed, taking her with him.
On the road there was not so much as an exchange of talk between them.
Both unhappy they came to Matila, not far from the town,
but the girl being good that I saw a fig tree covered with fruit.
And being hungry he climbed up and ate some of the figs.
The girl also being hungry came to the foot of the tree and called out,
threw down some fruit for me too.
What, says he?
Have you no hands or feet climb up and get yourself?
So she climbed up also and ate.
No sooner did he see that she had climbed, then he came down quickly
and piled thorns around the trees and made off saying to himself.
I've gotten rid of that miserable woman at last.
She could not get down but remain sitting where she was.
Now the king who had been amusing himself in the forest was coming back to town
on his elephant in the evening time when he saw her and fell in love.
So he sent to ask if she had a husband or no.
She replied, yes, I have a husband to whom my family gave me.
But he has gone away and left me here alone.
The court here told this tale to the king who said treasure trove belongs to the crown.
She was brought down and placed on the elephant and conveyed to the palace,
where she was sprinkled with the water of consecration as his queen consort.
Dear and darling she was to him, and the name Uddambara was given to her,
which means fig, because he first saw her upon a fig tree.
One day after this they who dwelt by the city gate had to clean the road for the king
to go to sporting in his park.
And Pangutara, who had to earn his living, talked up his clothes
and said to work clearing the road with a hoe.
Before the road was clean, the king with queen and Uddambara came along in a chariot.
And the queen seeing the wretch clearing the road could not restrain her triumph,
but smiled to see the wretch there.
The king was angry to see her smile and asked why she did so.
My lord, she said, that road cleaning fellow is my former husband,
who made me climb up the fig tree and then piled thorns about it and left me.
When I saw him, I could not help feeling triumphant at my good fortune
and smiled to see the wretch there.
The king, jealous, said, you lie, you laughed at someone else and I will kill you.
And he drew his sword.
She was alarmed and said, sigh, or pray, ask your wise men.
The king asked Seneca whether he believed her.
No, my lord, I do not.
Seneca, for who would leave such a woman if he once possessed her?
When she heard this, she was more frightened than ever, but the king thought,
what does Seneca know about it?
I will ask the real sage.
And as Mahosada reciting this stanza,
should a woman be virtuous and fair, a man not desire her?
Do you believe it, Mahosada?
The sage replied, okay, I do believe it.
The man would be an unlucky wretch, good luck and ill luck can never mate together.
These words, delayed the king's anger and his heart was calmed.
And much pleased, he said, oh, wise men, if you had not been here,
I should have trusted the words of that full Seneca and lost this precious woman.
You have saved me, my queen.
He recompensed the sage with a thousand pieces of money.
Then the queen said, did the king rather respectfully and abitimatively?
Sire, it is all through this wise man that my life has been saved.
Grant me the boon that I may treat him as my younger brother.
Yes, my queen, I consent.
The boon is granted.
Then my lord, from this day, I will eat no daintees without my brother from this day in season.
And without my brother, from this day in season and out of season,
my door shall be open to send him sweet food, this boon I crave.
You may have this boon also, my lady, quote, the king.
Here ends the question of good and bad luck.
Okay, I think that's enough for today.
No, I've got to save some for the other days.
Probably it's going to take about, say, seven weeks to get this off finished at this pace.
So thanks for listening to a new next time.
