1
00:00:00,000 --> 00:00:04,880
It is said that the church is far different than what Jesus taught.

2
00:00:04,880 --> 00:00:08,840
Would you say that what is taught now is different than what the Buddha taught?

3
00:00:08,840 --> 00:00:13,320
It's a valid question.

4
00:00:13,320 --> 00:00:14,320
No.

5
00:00:14,320 --> 00:00:17,760
Well, you know the church, no.

6
00:00:17,760 --> 00:00:21,040
What is the church?

7
00:00:21,040 --> 00:00:23,440
And what is what Jesus taught as well?

8
00:00:23,440 --> 00:00:27,040
But we don't know what the Buddha taught, right?

9
00:00:27,040 --> 00:00:30,240
We don't know if what we have is what the Buddha taught.

10
00:00:30,240 --> 00:00:31,760
But I talked about this before.

11
00:00:31,760 --> 00:00:36,480
The most important thing is not whether it was the Buddha who taught these things.

12
00:00:36,480 --> 00:00:42,040
The important is whether it's true.

13
00:00:42,040 --> 00:00:45,440
And that's what's so profound about the Buddha's teaching is it's a bhatsiko.

14
00:00:45,440 --> 00:00:49,920
The Buddha said, you come and see for yourself.

15
00:00:49,920 --> 00:00:52,600
So this is what we have to do in the Buddha's teaching.

16
00:00:52,600 --> 00:00:58,720
And it doesn't quite answer, but it doesn't really answer the question because you can

17
00:00:58,720 --> 00:01:02,760
see that many people have come and seen very, very different things.

18
00:01:02,760 --> 00:01:13,320
Many people interpret Buddhism to be something to be very, very different things.

19
00:01:13,320 --> 00:01:20,360
So really the best you can do if you want to be totally scientific about it is investigate

20
00:01:20,360 --> 00:01:28,480
the various Buddhist traditions and look and see if they're internally consistent.

21
00:01:28,480 --> 00:01:30,880
Or is this tradition internally consistent?

22
00:01:30,880 --> 00:01:39,600
Like the people who claim this as the truth, are they actually practicing it?

23
00:01:39,600 --> 00:01:44,840
Do they actually seem to have realized some of what they're teaching?

24
00:01:44,840 --> 00:01:47,600
Or are there any examples of that?

25
00:01:47,600 --> 00:01:51,840
And then look and see if it's externally consistent.

26
00:01:51,840 --> 00:02:03,280
What they're saying is fine and good, but is there any observable proof that it is true?

27
00:02:03,280 --> 00:02:07,000
So that it doesn't just become a faith thing, like God said this, God said that.

28
00:02:07,000 --> 00:02:11,920
If you do this, you go to God and go to heaven.

29
00:02:11,920 --> 00:02:13,200
That's the best you can do.

30
00:02:13,200 --> 00:02:18,640
I mean, obviously everyone's going to say ours is the, this is what the Buddha taught.

31
00:02:18,640 --> 00:02:23,080
There are some people who say even, oh, there are a few traditions that I've heard this

32
00:02:23,080 --> 00:02:27,600
from where they say the true teachings of the Buddha were hidden for thousands of years

33
00:02:27,600 --> 00:02:33,040
and then this teacher, our teacher, discovered them again.

34
00:02:33,040 --> 00:02:36,520
And this is the lineage of the Buddha.

35
00:02:36,520 --> 00:02:40,320
Just totally ignoring all the other traditions, like all these other traditions that were there

36
00:02:40,320 --> 00:02:41,320
before.

37
00:02:41,320 --> 00:02:47,920
As though they were nothing or as they, they didn't exist.

38
00:02:47,920 --> 00:02:58,200
But there is some other, another aspect of this question is that there are people who

39
00:02:58,200 --> 00:03:03,840
acknowledge that say the tradition that we follow or the teravada in general and say

40
00:03:03,840 --> 00:03:08,200
the polycanon, for example, is most likely what the Buddha taught.

41
00:03:08,200 --> 00:03:10,080
But then they say that's not good enough.

42
00:03:10,080 --> 00:03:16,520
So it gets quite complicated just because it's what the Buddha taught doesn't mean it's

43
00:03:16,520 --> 00:03:17,520
what we should do.

44
00:03:17,520 --> 00:03:22,320
They say the Buddha taught many things just because people couldn't understand the truth.

45
00:03:22,320 --> 00:03:26,640
And now later on people can better understand the truth.

46
00:03:26,640 --> 00:03:29,160
There's that opinion as well.

47
00:03:29,160 --> 00:03:35,120
And so now we have to use different teachings.

48
00:03:35,120 --> 00:03:36,120
Very interesting.

49
00:03:36,120 --> 00:03:40,600
And they even claimed that there are some later teachings of the Buddha that were hidden

50
00:03:40,600 --> 00:03:46,720
and the Buddha hid them because he knew that people couldn't understand them.

51
00:03:46,720 --> 00:03:49,160
And then after he passed away they were discovered.

52
00:03:49,160 --> 00:03:56,560
He gets a little bit suspicious, but sorry, I don't mean to make too much fun.

53
00:03:56,560 --> 00:04:03,680
But just to point out that it's not necessarily enough to ask whether it's the Buddha's

54
00:04:03,680 --> 00:04:04,680
teaching.

55
00:04:04,680 --> 00:04:07,920
And so I would say it's not really the most important thing.

56
00:04:07,920 --> 00:04:17,880
Importance is that it's true, valid, and that you can understand the benefit of it.

57
00:04:17,880 --> 00:04:22,520
And you can understand the goodness of it and the rightness of it.

58
00:04:22,520 --> 00:04:26,240
Because the other thing is that there are different paths.

59
00:04:26,240 --> 00:04:32,320
Following the Buddha's teaching will lead you to become enlightened.

60
00:04:32,320 --> 00:04:36,640
As a follower, you become free from suffering.

61
00:04:36,640 --> 00:04:40,880
It won't necessarily lead you to become a Buddha, where you know everything.

62
00:04:40,880 --> 00:04:45,600
So you might become free from suffering and free from all, from everything.

63
00:04:45,600 --> 00:04:47,480
Let go of everything.

64
00:04:47,480 --> 00:04:52,640
But you may not know everything, most likely won't because the Buddha didn't teach everything

65
00:04:52,640 --> 00:04:54,840
that he knew.

66
00:04:54,840 --> 00:04:57,640
He didn't teach you how to come to know everything because he realized that it wasn't

67
00:04:57,640 --> 00:04:59,600
really necessary.

68
00:04:59,600 --> 00:05:05,920
It wasn't really of any benefit, or it wasn't of the most immediate benefit.

69
00:05:05,920 --> 00:05:08,560
So some people say, well, immediate benefit is all in good.

70
00:05:08,560 --> 00:05:12,240
But what about the next Buddha when there's nobody around to teach?

71
00:05:12,240 --> 00:05:15,160
So then they decide they want to become a Buddha.

72
00:05:15,160 --> 00:05:19,000
And so therefore they don't pay so much attention to the Buddha's teaching.

73
00:05:19,000 --> 00:05:20,680
Anyway, that wasn't what you asked.

74
00:05:20,680 --> 00:05:24,760
What you asked is what I say, what is taught now is different from what the Buddha taught.

75
00:05:24,760 --> 00:05:27,040
I've kind of been ignoring that.

76
00:05:27,040 --> 00:05:32,240
Well, I did say that we don't really know.

77
00:05:32,240 --> 00:05:36,880
But one more thing that could be said is that when you practice, you really do know.

78
00:05:36,880 --> 00:05:38,520
This is the claim anyway.

79
00:05:38,520 --> 00:05:47,440
The claim that we make, that is made, is that a person who practices the Buddha's teaching

80
00:05:47,440 --> 00:05:55,640
will come to realize that the person who taught this was a Buddha, was enlightened, was

81
00:05:55,640 --> 00:05:56,640
knew the truth.

82
00:05:56,640 --> 00:06:03,240
We object the truth, and they'll say, this teaching is the truth.

83
00:06:03,240 --> 00:06:04,240
We object the truth.

84
00:06:04,240 --> 00:06:08,120
They'll come to realize that for themselves.

85
00:06:08,120 --> 00:06:09,120
Because they'll come to see Nibana.

86
00:06:09,120 --> 00:06:10,560
I mean, Nibana is really the key.

87
00:06:10,560 --> 00:06:17,120
You know, when a person realizes Nibana, they realize that there is nothing in some

88
00:06:17,120 --> 00:06:19,920
sorrow of any real benefit.

89
00:06:19,920 --> 00:06:24,640
It's not that they reject it, but they just stop clinging to it.

90
00:06:24,640 --> 00:06:30,440
You stop looking for things, they stop seeking after things.

91
00:06:30,440 --> 00:06:31,440
So it's objective.

92
00:06:31,440 --> 00:06:35,120
You realize it for yourself, and this is the claim, is that it's objective and it's

93
00:06:35,120 --> 00:06:37,920
objective truth.

94
00:06:37,920 --> 00:06:40,280
Whether the Buddha taught it or not, is not the most important.

95
00:06:40,280 --> 00:06:45,880
But if you want an answer to that, once you use the word Buddha, then what you mean by

96
00:06:45,880 --> 00:06:49,760
the word Buddha is someone who has realized the objective truth.

97
00:06:49,760 --> 00:06:54,040
And then what you mean by the teaching, you know, you have this teaching in these books.

98
00:06:54,040 --> 00:06:59,600
If the teaching in the books allows you to realize the objective truth, then yes, you know

99
00:06:59,600 --> 00:07:03,040
that it was taught by the Buddha.

100
00:07:03,040 --> 00:07:31,040
So that's as good of an answer as I think I can do.

