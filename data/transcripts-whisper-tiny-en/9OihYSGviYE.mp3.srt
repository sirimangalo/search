1
00:00:00,000 --> 00:00:05,840
There it is. Wouldn't the Pachaykabuddha be benefiting others because everything is interdependent?

2
00:00:07,920 --> 00:00:14,320
I see. Just by becoming enlightened. Well, first of all, let's dispel the myth. The Pachaykabuddha

3
00:00:14,320 --> 00:00:21,520
is not an island in isolation. A Pachaykabuddha still interacts with their fellow beings

4
00:00:21,520 --> 00:00:28,640
still benefits very, very much benefits. Their fellow beings simply as an example for starters,

5
00:00:28,640 --> 00:00:36,000
and also because of their teachings. Pachaykabuddha is not able to teach the Buddha's teaching.

6
00:00:36,000 --> 00:00:41,200
It's not able to come up with, for example, the four Satipatana, the five Agri, the five Kanda,

7
00:00:41,200 --> 00:00:48,960
the four Noble Truths. But they are able to teach people. They are able to say things, to encourage

8
00:00:48,960 --> 00:00:55,680
other people. They just don't have what it takes to really understand what it is that they've

9
00:00:55,680 --> 00:01:03,600
realized themselves. They can talk about good things and they can encourage people in good

10
00:01:03,600 --> 00:01:09,760
things so they can. I'm not sure how far they can go with that, and I don't want to step out

11
00:01:09,760 --> 00:01:16,160
of line, but there seems no reason for them to not be able to even teach people to give up

12
00:01:16,160 --> 00:01:22,080
desires and so on to practice meditation, to encourage them insofar as they understand themselves.

13
00:01:22,080 --> 00:01:26,880
It doesn't seem to be anything stopping them. So the idea that a bachaykabuddha is someone who keeps

14
00:01:26,880 --> 00:01:35,680
their mouth shut and never whispers a word of what they've come to know for themselves is I think

15
00:01:37,680 --> 00:01:44,480
has no backing. I mean in the commentaries there's a story that probably fairly famous among

16
00:01:44,480 --> 00:01:54,800
Buddhists of Anurudha. Anurudha in the past life was a servant, a serving man, and he gave

17
00:01:56,480 --> 00:02:04,720
arms to a bachaykabuddha. And he made a wish. He gave his only meal for the day because of course

18
00:02:04,720 --> 00:02:12,160
he was so poor that he only had one meal. He gave his one meal of the day he gave it to a bachaykabuddha

19
00:02:12,160 --> 00:02:19,680
and he said, as a result of this gift, may I never hear the words, not the words, not the word,

20
00:02:19,680 --> 00:02:26,560
not the, not the means, there isn't any or there is not. But in this case, his meaning was

21
00:02:26,560 --> 00:02:30,960
there isn't any. He would never, may I never hear the words, there isn't any. So whenever,

22
00:02:30,960 --> 00:02:36,880
meaning, whenever he wanted something, he would always get it. And the bachaykabuddha said something

23
00:02:36,880 --> 00:02:43,200
like, hey, one who took me, it'd be like that, and went away. So Anurudha was very, very happy,

24
00:02:43,200 --> 00:02:48,480
and he went back. I think he also made a vow, may I also realize, may I realize the teaching,

25
00:02:48,480 --> 00:02:52,880
the truth that you have come to realize, and, well, I still haven't realized the truth,

26
00:02:52,880 --> 00:03:00,400
may I never hear the word, not the word. When he goes back, and he tells, he's so happy that

27
00:03:00,400 --> 00:03:07,840
something like, he's so happy that his, his boss, his owner, or whatever, his employer,

28
00:03:08,880 --> 00:03:14,880
asks him what, what happened, you know, why is he so happy? And he tells him, and the owner says,

29
00:03:14,880 --> 00:03:21,520
I'll give you something like a thousand gold coins if you give me the merit, if you give me the

30
00:03:21,520 --> 00:03:28,160
merit, the goodness of what you've done. Give me all, all of your goodness, and I'll give you a

31
00:03:28,160 --> 00:03:33,440
thousand gold coins, all that you've gained from it, because there was a big idea that even in all

32
00:03:33,440 --> 00:03:40,480
times that doing these deeds got you some kind of punya, something that could, you know, the idea

33
00:03:40,480 --> 00:03:45,360
was, the idea that he had was that maybe it could be bought, because of course he was probably a

34
00:03:45,360 --> 00:03:50,640
fairly rich man, and everything of course could be bought, even people. So why couldn't you buy,

35
00:03:50,640 --> 00:04:00,480
buy punya? Anoruda, or the man who was to be Anoruda, he was of course totally horrified by this,

36
00:04:00,480 --> 00:04:05,520
and, and afraid that it might somehow be possible to buy his, his punya, his goodness,

37
00:04:06,320 --> 00:04:11,680
the goodness that he had gained from, from doing it. And so he said, no way, I, I'm, I'm not

38
00:04:11,680 --> 00:04:18,400
getting, I would never give this to anyone, not for any price. And the man, the, the guy was so

39
00:04:18,400 --> 00:04:25,600
upset, and, and, and so, so desires of this, this, this goodness that he said, well, okay,

40
00:04:25,600 --> 00:04:34,320
you give me half of your punya, and, and keep half for yourself, and all by half of it for

41
00:04:34,320 --> 00:04:40,480
whatever 500, 500 gold coins. And here Anoruda was thinking, but what does that mean? Does that

42
00:04:40,480 --> 00:04:46,000
mean I lose half of my goodness? And so he's thinking, and he does, he's not sure, and then he goes

43
00:04:46,000 --> 00:04:49,200
back and he's something like he asks his wife and his wife says, well, why don't you go ask the

44
00:04:49,200 --> 00:04:55,280
pachecaboo there? And so he goes back and he goes to the pachecaboo there, and he says, no, look,

45
00:04:55,280 --> 00:05:02,080
this is the case, I got this great, wonderful merit for doing this good deed of giving you arms.

46
00:05:03,600 --> 00:05:08,560
But now my boss wants to take half of it. Does that mean I'm going to lose half of my punya,

47
00:05:08,560 --> 00:05:15,200
and, but the pachecaboo teaches him, it's the only direct teaching that I can remember of a pachecaboo

48
00:05:15,200 --> 00:05:24,560
that, that, that, that occurs. It's the only one I can remember. He says, well, we'll consider this.

49
00:05:25,280 --> 00:05:31,760
Suppose you have a candle, and you, you light the candle, and then you've got one light. Now,

50
00:05:31,760 --> 00:05:37,120
suppose everyone in the whole village, in your whole village comes and lights, a candle,

51
00:05:37,120 --> 00:05:42,320
lights their candle from your candle. Suppose you were to do that for a thousand people,

52
00:05:42,320 --> 00:05:48,160
would you lose the light from your candle? And he would, and he replied, he said, no, it would be

53
00:05:48,160 --> 00:05:55,360
like one candle became a thousand, one light became a thousand lights. And, and the pachecaboo

54
00:05:55,360 --> 00:06:00,320
says in the very same way, goodness can never be transferred, it can only be multiplied.

55
00:06:01,600 --> 00:06:05,920
When you give, when you dedicate your merit, your goodness to someone else,

56
00:06:05,920 --> 00:06:12,240
it says, though your goodness had become twice. And, of course, he was very happy with this,

57
00:06:12,240 --> 00:06:16,640
because he's going to get money and keep his money. So he goes back and he says, yes, yes,

58
00:06:16,640 --> 00:06:21,680
I dedicate half of my money, or even I think something like all of my money, I dedicate it to you,

59
00:06:21,680 --> 00:06:28,800
I give it all over to you, which actually, I used to use this story as an example of how actually,

60
00:06:28,800 --> 00:06:33,520
it gives you more merit to do that. When you, when you dedicate your merit and wish for your

61
00:06:33,520 --> 00:06:39,760
merit, the power of your goodness may it benefit someone else, you actually get more merit as a

62
00:06:39,760 --> 00:06:47,600
result, of course, because your heart becomes pure. And so that's an example of the teachings

63
00:06:47,600 --> 00:06:51,600
of the pachecaboo that the story goes on and talks about how he actually never does never hear

64
00:06:51,600 --> 00:07:02,800
the word nutty. And as a result, one day his mother runs out of, runs out of cakes. And he says,

65
00:07:02,800 --> 00:07:08,400
well, then bring them, and she says, so nutty, she sends tell them there are no cakes, nutty.

66
00:07:08,400 --> 00:07:13,200
And he says, nutty, what does this mean? And he says, well, then tell her to bring the nutty cakes.

67
00:07:14,640 --> 00:07:16,800
That's fine. I'll take the nutty cakes.

68
00:07:20,720 --> 00:07:22,640
That's funnier when I read it.

69
00:07:24,720 --> 00:07:31,040
So, okay, so I haven't even answered the question yet. So the point being that they do benefit

70
00:07:31,040 --> 00:07:37,600
directly, they are able to teach directly. They're just not able to teach someone the path to

71
00:07:37,600 --> 00:07:43,120
become enlightened. That's what makes them a pachecaboo. And of course, they would be different.

72
00:07:43,120 --> 00:07:48,240
So some of them might be better to teach than others. Some of them might even teach things that

73
00:07:48,240 --> 00:07:52,400
lead people on to practice the Buddhist teaching later on and so on.

74
00:07:52,400 --> 00:08:00,160
There are no, I wouldn't say there are any hard and fast rules in that regard. But whether

75
00:08:00,160 --> 00:08:06,960
someone be the pachecaboo to or even an araha, benefits others by just becoming enlightened,

76
00:08:06,960 --> 00:08:16,160
simply because of interdependence, I'm all for it. I think as we better ourselves,

77
00:08:16,160 --> 00:08:28,160
those people who are connected with us benefit as well. I mean, consider what it would have

78
00:08:28,160 --> 00:08:32,320
been like had you not practiced and become enlightened. If you take just an ordinary

79
00:08:32,320 --> 00:08:36,560
meditator, suppose they practice, suppose they become, they become an araha.

80
00:08:37,600 --> 00:08:43,040
Suppose they hadn't done that. If you compare the state of things, if they hadn't done that,

81
00:08:43,040 --> 00:08:48,480
and if they had, if they become an araha, and then they die or something, or then they go

82
00:08:48,480 --> 00:08:53,200
off in the forest and live the rest of their life. Well, if they hadn't, then they would have,

83
00:08:53,200 --> 00:08:57,440
of course, based on their defilements, based on their attachments, they would have encouraged

84
00:08:58,000 --> 00:09:03,520
all sorts of unpleasant things. If you consider it just from that aspect, they've benefited

85
00:09:03,520 --> 00:09:11,600
others immensely. They've freed others from the karmic cycle. So if you have a karmic cycle of

86
00:09:11,600 --> 00:09:14,960
vengeance towards someone, where you might not have bad feelings towards them, but somehow

87
00:09:15,840 --> 00:09:18,960
you're going to do something to hurt them, because they've heard you in the past,

88
00:09:19,520 --> 00:09:27,200
then by becoming enlightened, you're going off in the forest, for example, you free others from

89
00:09:27,200 --> 00:09:32,880
there. So you might even say that by becoming enlightened, the world around us becomes freer.

90
00:09:33,920 --> 00:09:39,600
You'll also often find that these beings pull others in with them. So as a result of their

91
00:09:39,600 --> 00:09:46,400
becoming enlightened, as an example, or even just because of karmic relationships,

92
00:09:46,400 --> 00:10:16,240
they pull all the people around them in this way.

