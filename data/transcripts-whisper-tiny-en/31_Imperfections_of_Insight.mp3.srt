1
00:00:00,000 --> 00:00:07,600
imperfections of insights. The following is condensed from chapter four imperfections of

2
00:00:07,600 --> 00:00:16,160
insight from how to meditate to, by a venerable, utadamal bitu. When the meditate receives the

3
00:00:16,160 --> 00:00:22,720
causal relationship between the body and the mind, the meditate begins to naturally resist

4
00:00:22,720 --> 00:00:29,520
some habitual tendencies and realizes that certain built-up habits are a cause of great

5
00:00:29,520 --> 00:00:37,200
suffering to oneself and others. A meditator also realizes that it is the reactivity to desires

6
00:00:37,200 --> 00:00:44,800
and aversion that is a real cause for stress and suffering. It is this sort of realization that leads

7
00:00:44,800 --> 00:00:50,720
to the next stage of knowledge which is based on a deepening understanding that the objects of

8
00:00:50,720 --> 00:00:57,280
experience are unworthy of the obsession we normally give to them. Before this understanding comes

9
00:00:57,280 --> 00:01:03,120
through fruition, however, it is common for the minds to rebel, equally from the sudden

10
00:01:03,120 --> 00:01:10,400
onslaught of instability, insipidness, and chaos that arises from no longer chasing of

11
00:01:10,400 --> 00:01:17,280
sub-dozenness, no running away from unpleasantness. In the beginning, the experience of reality

12
00:01:17,280 --> 00:01:23,760
is more likely to lead one to seek out an alternative to that which is impermanent, stressful,

13
00:01:23,760 --> 00:01:30,080
and uncontrollable. As a result, a new meditator will tend to cling to anything that appears

14
00:01:30,080 --> 00:01:37,600
at first glance, even remotely stable, satisfying, uncontrollable. Such objects of clinging

15
00:01:37,600 --> 00:01:43,360
are called the imperfections of insights of which tend are enumerated in the text.

16
00:01:43,360 --> 00:01:52,240
One of us are illumination, experience of visions or bright lines to be noted as seen,

17
00:01:52,240 --> 00:01:58,320
seen until they disappear or no longer in objects of interest for the mind.

18
00:01:59,120 --> 00:02:07,360
Two, nana, knowledge, mental activity directed towards solving mundane problem in one's life

19
00:02:07,360 --> 00:02:14,560
to be noted as thinking, thinking, or knowing, knowing. Three, pity, rapture,

20
00:02:14,560 --> 00:02:21,360
experience of ecstatic states, floating, flowing energy, uncontrollable laughing,

21
00:02:21,360 --> 00:02:30,320
or crying, we should be noted objectively. Four, acidity, tranquility, experience of tranquility,

22
00:02:30,320 --> 00:02:39,280
calmness, or quietity, should to be noted as quiet, quiet, or calm, calm. Five, sukhat,

23
00:02:39,280 --> 00:02:48,400
happiness, experience of pleasant feeling to be noted as happy, happy. Six, adi'moka,

24
00:02:48,400 --> 00:02:56,320
resolve, experience of confidence, self assurance of thinking on self has obtained a superman

25
00:02:56,320 --> 00:03:03,120
jained state of being. These various states should be noted, reminding oneself of their various

26
00:03:03,120 --> 00:03:13,120
natures. Seven, pagaha, exertion, experience of becoming hyper-anigetic or feeling as if one could practice

27
00:03:13,120 --> 00:03:19,120
or day and night without stopping, one should note energy when it becomes apparent.

28
00:03:19,120 --> 00:03:27,600
Eight, bupartana, attention, state of attentiveness brought about by the constant practice

29
00:03:27,600 --> 00:03:34,560
of mindfulness that becomes an obstacle when one takes up an object of observation,

30
00:03:34,560 --> 00:03:40,560
something that is outside the present moment. All of these activities should be seen as

31
00:03:40,560 --> 00:03:50,240
distractions from the path and no change. Nine, bupakha, equanimity, experience of equanimity can also

32
00:03:50,240 --> 00:03:57,280
lead to delusions of enlightenment as one reflects that one is no longer played by likes or

33
00:03:57,280 --> 00:04:04,480
dislikes. Such thoughts should be noted as they arise and one should remind oneself, calm,

34
00:04:04,480 --> 00:04:14,240
calm, tense, content, desire, experience of desire arising for other items on this list of

35
00:04:14,240 --> 00:04:22,000
a sense pleasure or for insight itself to be recognized as harmful obstacles to true understanding

36
00:04:22,000 --> 00:04:30,160
of reality and noted accordingly. Meditators should take notes of the many ways in which one

37
00:04:30,160 --> 00:04:38,400
can become distracted from the path, entice into a false conception of stability, such inspection

38
00:04:38,400 --> 00:04:45,440
and control by the many pleasant and even positive by products of the meditation practice.

39
00:04:45,440 --> 00:04:53,440
Meditators must stand ready to observe such experiences with a clear mind, reminding themselves

40
00:04:53,440 --> 00:05:00,880
of the true essence of these experiences using simple mantras such capture their nature. Once

41
00:05:00,880 --> 00:05:07,520
they are able to do this, they will be ready to enter the realm of true and profound insights

42
00:05:07,520 --> 00:05:37,360
into their nature of reality.

