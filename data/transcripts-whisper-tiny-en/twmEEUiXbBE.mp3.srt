1
00:00:00,000 --> 00:00:06,240
I have a question, does Buddhism have an implicit notion of God?

2
00:00:06,240 --> 00:00:09,440
Implicit notion of God?

3
00:00:09,440 --> 00:00:12,000
It's an interesting way to phrase the question.

4
00:00:12,000 --> 00:00:15,600
I guess we should ask first, does it have an explicit notion of God?

5
00:00:15,600 --> 00:00:22,640
Then we can, because I don't really understand why you would ask implicit.

6
00:00:22,640 --> 00:00:28,240
I mean, I'm not saying it's a wrong question, but it's beyond me at this moment.

7
00:00:28,240 --> 00:00:31,680
Anyone implicit?

8
00:00:31,680 --> 00:00:36,320
Why add that attitude?

9
00:00:36,320 --> 00:00:39,600
Well, go ahead.

10
00:00:39,600 --> 00:00:41,440
No, it was something good stuff.

11
00:00:45,440 --> 00:00:53,040
The Buddha did not talk about God, and I think there was a wise reason for that,

12
00:00:53,040 --> 00:01:01,520
because it would distract from his message, you know, he was coming from Hinduism,

13
00:01:01,520 --> 00:01:08,240
which had Brahma, and so, you know, his message was suffering and how to end it,

14
00:01:08,240 --> 00:01:10,240
not whether or not there's a God.

15
00:01:10,240 --> 00:01:20,240
And so, I think that was a very wise subject to avoid.

16
00:01:20,240 --> 00:01:27,040
Yes, but he didn't quite avoid it.

17
00:01:27,040 --> 00:01:32,000
He avoided, I mean, he didn't exactly avoid it, he didn't certainly focus on it,

18
00:01:32,000 --> 00:01:42,560
but he did sometimes talk about the issues.

19
00:01:42,560 --> 00:01:50,800
I mean, he didn't talk about, I think he did really.

20
00:01:50,800 --> 00:02:00,160
I mean, he talked about Brahma, because in the time, there were many people coming to ask him,

21
00:02:00,160 --> 00:02:06,800
about Brahma, and about the past to Brahma, and he said, I know Brahma,

22
00:02:06,800 --> 00:02:12,080
and I know the past to Brahma.

23
00:02:12,080 --> 00:02:17,680
I guess I was thinking about the Creator God.

24
00:02:17,680 --> 00:02:23,120
Yeah, so I wanted to say that I wanted to agree with you that,

25
00:02:23,120 --> 00:02:26,160
but that isn't referring to the Creator God.

26
00:02:26,160 --> 00:02:33,040
But he did discuss the notion of a Creator God in the Brahma-Jalasut.

27
00:02:33,040 --> 00:02:38,000
So, I think it's fair to say that he didn't focus on it.

28
00:02:38,000 --> 00:02:44,320
But, and Buddhism certainly, Buddhism as a practice certainly doesn't have,

29
00:02:44,320 --> 00:02:50,640
I would say, anything to do with God except in the sense of denying the existence of self or entity

30
00:02:50,640 --> 00:02:56,080
of any sort, which I think does include giving up the belief in God.

31
00:02:56,080 --> 00:03:03,440
But, he did at times, you know, according to the Sutras,

32
00:03:03,440 --> 00:03:10,560
engage in the Brahma-Jalasut is an incredibly profound discussion of God,

33
00:03:10,560 --> 00:03:16,560
especially in the sense of being a Creator.

34
00:03:16,560 --> 00:03:21,760
But, you know, there's a Sutho where he talks about all the leaves on the trees,

35
00:03:21,760 --> 00:03:26,560
and then no leaves that are in this hand and say, you know, what I'm sharing with you is,

36
00:03:26,560 --> 00:03:28,800
you know, comparable to the leaves in my hands.

37
00:03:28,800 --> 00:03:31,360
What I know is like the leaves in the trees.

38
00:03:31,360 --> 00:03:33,040
Because it's helpful or not helpful.

39
00:03:33,040 --> 00:03:42,400
But, I'm not, I'm not completely of the view that it's useless to talk about God,

40
00:03:42,400 --> 00:03:45,840
because many people come to practice meditation,

41
00:03:45,840 --> 00:03:49,360
and not many, some people come to practice meditation,

42
00:03:49,360 --> 00:03:56,160
and aren't able to because of their sense of security,

43
00:03:56,160 --> 00:04:01,920
their inability to let go, you know, you have to be shaken to the corn

44
00:04:01,920 --> 00:04:03,840
in order to really let go.

45
00:04:03,840 --> 00:04:10,000
And, they're not going to be shaken because they have something that's permanent,

46
00:04:10,000 --> 00:04:13,520
something that's solid, something that's absolute, and that's God.

47
00:04:13,520 --> 00:04:24,160
So, whether or not it's useful to confront them on it,

48
00:04:24,160 --> 00:04:33,280
it's certainly useful to find a way to help them to release their grasp of God.

49
00:04:33,280 --> 00:04:36,640
So, you wouldn't find the Buddha often confronting people on this.

50
00:04:36,640 --> 00:04:41,440
So, he talks about Brahma, and he talks, you know, the way he does it is kind of,

51
00:04:41,440 --> 00:04:44,400
you know, in the way only someone who is omniscient can, right?

52
00:04:44,400 --> 00:04:48,320
Or someone who is super, superman, like the Buddha.

53
00:04:48,320 --> 00:04:51,680
I can say it, because we can't say this, we can't talk to the God people and say,

54
00:04:51,680 --> 00:04:56,000
yeah, I know God, I talk to him all the time, and he's actually just some guy up and heaven,

55
00:04:56,000 --> 00:04:57,120
because we don't.

56
00:04:57,120 --> 00:05:00,800
But, the Buddha did, and he went to visit Brahma and so on,

57
00:05:00,800 --> 00:05:05,360
and he said, I used to be Brahma, and I remember that, and so on.

58
00:05:05,360 --> 00:05:14,000
So, what that means to me is that he was able to help people that go and see that it's,

59
00:05:14,000 --> 00:05:20,880
you know, so what, so there's God, so what, you know, it really doesn't have that mean,

60
00:05:20,880 --> 00:05:28,960
much meaning, and that's how I've, you know, how I've developed my approach to this,

61
00:05:28,960 --> 00:05:38,640
and approach to people, is to people who believe in God, is to talk about how it's,

62
00:05:38,640 --> 00:05:50,080
it's a part of the, the cycle, or it's a part of the samsara, God is, is just a part of the mix.

63
00:05:51,200 --> 00:05:56,080
Yeah, so what, if you go to heaven, you know, trying to help people to

64
00:05:56,080 --> 00:06:01,600
bring their beliefs back down to earth, that yes, there might be a heaven.

65
00:06:01,600 --> 00:06:06,400
I mean, we talk, here people talk about it and make sense from the point of view of having

66
00:06:06,400 --> 00:06:09,280
a pure mind where you would go if there wasn't enough to your life.

67
00:06:12,320 --> 00:06:17,600
But it, it ends there, it doesn't go to the next step of, of being permanent,

68
00:06:18,560 --> 00:06:25,120
and okay, so God created the universe, well, who created God, or, or what was there before the

69
00:06:25,120 --> 00:06:31,760
universe or so on, and so we can believe that, okay, maybe God did create the universe,

70
00:06:31,760 --> 00:06:36,800
but so on. You know, there must have been something that before God, and the Mormons have an

71
00:06:36,800 --> 00:06:41,200
interesting approach. I think it's the Mormons, these Mormon kids came to visit me, and of

72
00:06:41,200 --> 00:06:47,040
course I invited them in and asked them all sorts of questions about Mormonism and I grilled them

73
00:06:47,040 --> 00:06:54,080
really, and they were so nice, and really profoundly open-minded in a way that I think only

74
00:06:54,080 --> 00:07:03,600
Mormons can be. And they said that I said, so, how did God become God? And he said, you want to

75
00:07:03,600 --> 00:07:10,960
know my opinion? And I said, yes, and he said, well, I believe that God was just like us,

76
00:07:11,600 --> 00:07:18,640
and he had to work to become God. And I said, that's such a profound thing to say. I'm in agreement

77
00:07:18,640 --> 00:07:24,800
with you. These guys were really cool. So I give them all my alms, all my leftover alms food,

78
00:07:24,800 --> 00:07:30,960
and invited them back, and they didn't come back, because I think they, they realized that I wasn't

79
00:07:31,760 --> 00:07:34,960
going to be converted, or I don't know, or maybe they have to go to everybody or something,

80
00:07:34,960 --> 00:07:39,840
they're only allowed to come once. They did come back to get the food, because they, I gave it to

81
00:07:39,840 --> 00:07:44,480
them, and then we talked some more, and then they forgot it. So they came back the next day,

82
00:07:44,480 --> 00:07:49,120
and I gave them the food that I had the next day, something like that. Nice, guys.

83
00:07:52,800 --> 00:07:59,600
Anyway, certainly what you're saying is, I don't mean to disagree with you, because it's

84
00:07:59,600 --> 00:08:07,040
an important thing to say, that forget about it. Let go of God. It's not the most important thing

85
00:08:07,040 --> 00:08:15,600
in your life. It is for most people, for some, the most fierce, right? But best advice as an atheist,

86
00:08:15,600 --> 00:08:20,640
the atheist Buddhist that we can give you is, you know, let go. It's not really helping. It's not

87
00:08:20,640 --> 00:08:28,240
really going to solve your problems, answer your problems. It's just, you know, it's just nice

88
00:08:28,240 --> 00:08:35,680
to cling to things, and it's nice to cling to things that are permanent, you know? If only such a

89
00:08:35,680 --> 00:08:45,840
thing existed. But we're just put it on a back burner, you know? It's, it's true. It's something

90
00:08:45,840 --> 00:08:52,880
that's, yep. If people can do that, then they'll come to, as you said earlier, they'll come to see

91
00:08:52,880 --> 00:08:56,480
the, you know, whether it's true or not for themselves. That's a really an important thing,

92
00:08:56,480 --> 00:09:01,520
you know, if you are not sure whether there's a God, or if you believe that there's a God,

93
00:09:01,520 --> 00:09:08,240
and we're telling you that there's probably not the permanent and all omnipotent God out there,

94
00:09:09,840 --> 00:09:16,080
then not, maybe not even just on the back burner, but, okay, put it out there, and,

95
00:09:17,360 --> 00:09:23,440
and hold it out there, and then start practicing, and then come back later and ask yourself whether

96
00:09:23,440 --> 00:09:33,360
that hypothesis, the God hypothesis, withstands the test, because if you're, if you're, if you're

97
00:09:33,360 --> 00:09:41,040
belief, requires that you don't investigate. And, and it, it only works when you're afraid to

98
00:09:41,040 --> 00:09:46,080
investigate and afraid to learn about the way things really are, then it's pretty, well, it's

99
00:09:46,080 --> 00:09:52,240
pretty pathetic, and it's, it's not really, you know, it, that's where all the suffering comes from, as I

100
00:09:52,240 --> 00:09:56,560
said about, but my point of view, these people have a lot of suffering from their clinging.

101
00:10:00,560 --> 00:10:08,080
So, if you're, if you're really sure, then invest it, then you don't exactly have to investigate,

102
00:10:08,080 --> 00:10:15,840
but start observing reality and see if it's in line with reality. Everything that's

103
00:10:15,840 --> 00:10:25,680
not in relying on reality should be discarded, should be given up completely. So, yeah,

104
00:10:27,680 --> 00:10:32,000
all of our views and beliefs should be treated in this way. You have enough to give them up,

105
00:10:32,000 --> 00:10:35,840
just say, okay, I believe this, that's fine. Let's see what the truth is.

106
00:10:35,840 --> 00:10:45,840
That's right.

