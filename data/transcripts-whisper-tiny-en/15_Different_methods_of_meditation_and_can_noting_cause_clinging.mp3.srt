1
00:00:00,000 --> 00:00:08,020
Different methods of meditation and can noting cause clinging, what is meditation, is

2
00:00:08,020 --> 00:00:16,580
X, insert method here, a valid meditation, can noting lead to clinging, meditation is

3
00:00:16,580 --> 00:00:23,200
any kind of mental practice, it generally involves habits, cultivating or working with mental

4
00:00:23,200 --> 00:00:27,680
habits in some way, it can be about breaking down habits.

5
00:00:27,680 --> 00:00:31,920
For every moment of experience, there are many ways you can respond.

6
00:00:31,920 --> 00:00:37,920
You can think about the experience, you can react to it, you can try to suppress it, you

7
00:00:37,920 --> 00:00:45,520
can try and find a way to avoid it, you can go with it or engage with it, you can encourage

8
00:00:45,520 --> 00:00:50,240
it to continue, you can enjoy it, you can ignore it.

9
00:00:50,240 --> 00:00:56,800
Some types of practice are only meditation in the western sense of the word, mauling over

10
00:00:56,800 --> 00:01:02,880
or pondering something, these are not valid forms of meditation in a Buddhist sense.

11
00:01:02,880 --> 00:01:08,800
When you feel pain, if you contemplate it intellectually, thinking to yourself, this is

12
00:01:08,800 --> 00:01:14,960
impermanent, this is suffering, this is not self, then you are creating abstract thought

13
00:01:14,960 --> 00:01:21,680
about it, you are not actually cultivating mindfulness, your mind is focused on the thoughts

14
00:01:21,680 --> 00:01:26,280
and you are not actually paying attention to the experience.

15
00:01:26,280 --> 00:01:35,440
The difference between abstraction and saying to yourself, pain, pain, or thinking, thinking,

16
00:01:35,440 --> 00:01:41,200
is that repeating the name of the experience puts your attention on the experience itself,

17
00:01:41,200 --> 00:01:49,360
evoking a consciousness that is free from any kind of judgment, abstraction or diversification,

18
00:01:49,360 --> 00:01:55,120
free from making more out of the experience than it actually is, noting is a unique form

19
00:01:55,120 --> 00:02:01,240
of interaction, designed to create an objective state of mind where you simply experience

20
00:02:01,240 --> 00:02:08,680
the object as it is, qualities like patience, equanimity and even peace and tranquility

21
00:02:08,680 --> 00:02:13,760
are all associated with this non-reactive state of awareness.

22
00:02:13,760 --> 00:02:20,280
Our purpose is to cultivate a state where we do not create, proliferate or make anything

23
00:02:20,280 --> 00:02:26,840
out of the objects of our experience, in Buddhist theory, Bava is the link in the chain

24
00:02:26,840 --> 00:02:33,480
that leads to suffering, Bava means making something out of something, for example, if

25
00:02:33,480 --> 00:02:38,680
someone insults you and you get upset about it, make something of it, then you have

26
00:02:38,680 --> 00:02:47,760
engaged in Bava, Bava means literally being or becoming, anytime you make a big or small

27
00:02:47,760 --> 00:02:50,480
deal of something you are creating Bava.

28
00:02:50,480 --> 00:02:57,160
The use of the mantra or noting is special in that it is taught or logical, it is not

29
00:02:57,160 --> 00:03:03,720
saying anything new about the experience, it is just reminding you to experience the object

30
00:03:03,720 --> 00:03:10,560
simply for what it is, noting is unique in that it results in an absence of results and

31
00:03:10,560 --> 00:03:15,960
it should feel like that, it should feel like you have gained a respite, been given a moment

32
00:03:15,960 --> 00:03:21,720
that is free from any contemplating, reacting or diversifying.

33
00:03:21,720 --> 00:03:28,040
On the other hand, the result of noting is not nothing, when you remind yourself of the

34
00:03:28,040 --> 00:03:35,640
objective nature of any experience, you create a state of objectivity and more importantly,

35
00:03:35,640 --> 00:03:41,760
you foster a habit of being objective, people who never practice meditation or constantly

36
00:03:41,760 --> 00:03:49,400
engaging with the experiences based on specific habits of reactivity, liking, disliking,

37
00:03:49,400 --> 00:03:52,560
identifying, judging, et cetera.

38
00:03:52,560 --> 00:03:58,880
These reactions are so immediate and so habitual that they seem to be a part of the experience

39
00:03:58,880 --> 00:03:59,880
itself.

40
00:03:59,880 --> 00:04:07,200
In a similar manner, meditation develops specific habits, but the habits in meditation,

41
00:04:07,200 --> 00:04:14,920
especially mindfulness meditation, are much simpler by saying to ourselves, pain, pain,

42
00:04:14,920 --> 00:04:21,760
or thinking, thinking, we evoke states of experiencing things just as they are, by practicing

43
00:04:21,760 --> 00:04:28,600
this way regularly, such states become habitual, flowing into our ordinary lives.

44
00:04:28,600 --> 00:04:36,680
At work or with family, in situations we would ordinarily be stressed, reactive or reactionary,

45
00:04:36,680 --> 00:04:41,920
we will find ourselves far less reactionary, because of the habits that we are developing

46
00:04:41,920 --> 00:04:44,440
through mindfulness practice.

47
00:04:44,440 --> 00:04:50,560
Meditation is not magic and it is not a quick fix, because we are dealing with old habits

48
00:04:50,560 --> 00:04:56,320
that are years and perhaps lifetimes old, but the change takes place quite clearly, and

49
00:04:56,320 --> 00:04:59,560
the evidence is empirically observable.

50
00:04:59,560 --> 00:05:05,560
It involves simple principles of cultivating habits and how they affect one's mind.

51
00:05:05,560 --> 00:05:12,200
Furthermore, becoming habitually objective allows you to see your experiences and reactions

52
00:05:12,200 --> 00:05:13,840
more clearly than before.

53
00:05:13,840 --> 00:05:21,800
You will see clearly how bad habits are bad, anger, greed, conceit, arrogance, worry,

54
00:05:21,800 --> 00:05:24,840
restlessness, distraction, and so on.

55
00:05:24,840 --> 00:05:30,160
You will also see the experiences you react to, more clearly themselves.

56
00:05:30,160 --> 00:05:37,880
You will see your thoughts, rationalization, and extrapolation that go on in the mind, facilitating

57
00:05:37,880 --> 00:05:39,440
our reactions.

58
00:05:39,440 --> 00:05:45,080
Most importantly, you will come to see the characteristics of impermanence, suffering, and

59
00:05:45,080 --> 00:05:48,800
non-self in all aspects of your experience.

60
00:05:48,800 --> 00:05:55,640
You will see that the things that you used to cling to or want are not worth wanting,

61
00:05:55,640 --> 00:06:00,960
because the stability you saw in them, the satisfaction you saw in them, and the control

62
00:06:00,960 --> 00:06:07,160
that you thought you had over them was an illusion, something ill conceived out of ignorance.

63
00:06:07,160 --> 00:06:13,320
When you see your experiences as they truly are, you see that they are unpredictable,

64
00:06:13,320 --> 00:06:20,880
incontinent, and impermanent, unsatisfying, and unable to facilitate satisfaction and

65
00:06:20,880 --> 00:06:26,280
uncontrollable, unmanageable, without any substance whatsoever.

66
00:06:26,280 --> 00:06:32,160
It is this clarity of vision that allows us to let go and become truly independent in

67
00:06:32,160 --> 00:06:33,160
the world.

68
00:06:33,160 --> 00:06:37,760
The question of whether noting can lead to clinging is a common one.

69
00:06:37,760 --> 00:06:45,960
When hearing about this practice of saying to yourself, angry, angry, or liking, liking,

70
00:06:45,960 --> 00:06:50,280
people often ask, wouldn't that just make the anger worse?

71
00:06:50,280 --> 00:06:53,960
Wouldn't that just make me like or want that thing more?

72
00:06:53,960 --> 00:06:56,400
It is reasonable to ask this.

73
00:06:56,400 --> 00:07:01,640
If you actually practice noting, however, you will see that it is not so.

74
00:07:01,640 --> 00:07:05,440
When we experience something pleasant, why do we want it?

75
00:07:05,440 --> 00:07:09,880
What is it that makes us angry about experiences we don't like?

76
00:07:09,880 --> 00:07:14,840
Clearly, it is a bias or prejudice that leads to each.

77
00:07:14,840 --> 00:07:21,800
The purpose of reminding yourself of the nature of the object is to prevent the bias perspective

78
00:07:21,800 --> 00:07:26,160
that allows reactions like greed and anger to arise.

79
00:07:26,160 --> 00:07:32,520
When states like greed and anger arise, there is a judgment of the experience as good.

80
00:07:32,520 --> 00:07:35,680
Bad, me, mine, et cetera.

81
00:07:35,680 --> 00:07:42,600
We often reinforce these perceptions with thoughts like, this is good, or this is bad.

82
00:07:42,600 --> 00:07:47,440
If you are angry at someone, you might think to yourself, I hate that person.

83
00:07:47,440 --> 00:07:53,320
Such a thought makes you more angry because your focus is on the person, not the anger.

84
00:07:53,320 --> 00:07:58,640
When you are anxious, you normally perceive the anxiety with more anxiety, perceiving it

85
00:07:58,640 --> 00:07:59,800
as a problem.

86
00:07:59,800 --> 00:08:08,760
You possess rather than merely an experience when you say to yourself, angry, angry, or anxious,

87
00:08:08,760 --> 00:08:09,760
anxious.

88
00:08:09,760 --> 00:08:15,560
It does not escalate because you see it as an experience rather than a problem or a

89
00:08:15,560 --> 00:08:16,560
possession.

90
00:08:16,560 --> 00:08:24,240
Noting is basically saying, this is this rather than saying, this is bad, or this is good,

91
00:08:24,240 --> 00:08:41,480
or this is mine, et cetera.

