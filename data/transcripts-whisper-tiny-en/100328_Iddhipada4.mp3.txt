In the practice of meditation, as with everything else we do, we have to not only practice
not only dedicate our time to the practice, but we also have to practice in such a way
as to be sure that we're going to get results from the practice.
It's not true that simply walking and sitting for long periods at a time or long extended
period of days and weeks and months and years is sure to bring results.
It goes without saying that it should go without saying that our practice needs to have
certain qualities about it in order for us to guarantee some kind of success and some
sort of results from our practice.
Why this is, is because we, in order to succeed in the practice, in order to gain
results, we have to first gain this strength and a certain fortitude of mind.
Our competence has to be very strong, our effort has to be very strong.
Our mindfulness has to be very strong, our concentration has to be very strong and our
wisdom has to be very strong.
Without these five strengths, our mind is too weak, our mind is unable to overcome the evil,
the unhoseness, the darkness and the cloudiveness that's inside of us.
We won't be able to seek clearly as a result of the defilements which we carry around.
And it goes without saying that our ordinary state of mind is not strong enough.
This weak in many ways is lacking in many of these qualities.
And so unless we actually change our minds or build up this strength and the sportitude
of mind, we can practice for years and years and years and never truly become enlightened.
We have to work to gain these qualities of mind, we have to work again and again.
And so sometimes we might ask, well what are the qualities that we need to have, we need
to possess in order to be sure that our practice is going to be a success, is going
to bring result.
And so we have this very famous group of qualities that the Lord Buddha said are a guarantee
of success, given all other things being equal.
If there is the ability to succeed in something, if it's not blocked by fate or circumstance,
or as we say in Buddhism as we would explain it in Buddhism karma, if there is still
a chance to succeed in something, as there surely is with all of us in regards to meditation,
then it is these four dhammas, this set of dhammas of qualities of mind that will make
or break our practice, that will allow us to succeed or cause us to fail.
Their presence will allow us to succeed, their absence will cause us to fail again and again,
until finally we can incorporate these four qualities of mind into our practice.
So these four are called the iti pada, iti means power or strength or success or fortitude
of mind, as I said, the power that we are looking for, to have strong confidence, strong
effort, strong mindfulness, strong concentration and strong wisdom, the strength.
What that means, the path or the practice or the items, or those things that lead to.
So these four dhammas are very important, what are the four?
The first is called chanda, chanda which means a zeal or contentment or interest, you
could say, our interest in practicing, desire to practice, our contentment with practice
or intention, you could say, intention to practice, wholeheartedness, we are chanda, so
I guess simply it just means desire, our desire to practice.
Number two, vitya, which means effort, the effort to practice.
The three is chita, chita which means the mind or keeping something in mind, thinking
about something, you could say it means thought, thinking about the practice, keeping
the practice in mind.
Number four, vimangsa, vimangsa, which means considering consideration or contemplation,
analyzation, and analysis of the practice, analyzing our practice.
And in fact, these four dhammas don't particularly relate to the practice indirectly at
all.
They can be used for anything in the world.
Whatever we apply these four dhammas to, we can be sure to succeed.
For instance, if you wish to do evil deeds, you need these four dhammas as well, you need
to have the interest or the desire to do evil deeds, you have to put out effort to
do evil deeds, you have to think about it and keep it in mind, focus on it, and you have
to analyze the act to figure out how to do it properly, how to make sure it succeeds.
Whatever we want to do in the world, we want to be successful in business, we need
these four, we need to have the desire to be busy, to conduct business, we need to have
the effort, we need to have the concentration, the focus, keeping it in mind, and we need
to consider the causes and effects and make a plan of what we're going to run our business,
analyzing the terms of event and as things as they come.
So these four dhammas are very useful things to remember and to think about, no matter
what we do, but most especially in the meditation practice, they have a great meaning and
a great importance.
Chanda means that we actually want to practice, and this is often a criticism of Buddhist
meditation by people who have studied the books and are perhaps looking for something
to criticize and they say that you're looking for the end of desire and yet you admit
that you yourself have the desire to practice.
And this is really a silly argument, one that baffles many people and makes them think
that it's really true that this is an impossible task.
But really the word desire in this case is not really desire, we're talking about two uses
of the word desire.
When we talk about giving up desire, we need desire for something, for some experience,
for some state, for some phenomenon to arise.
When we talk about the desire to practice, we mean simply an intention to practice, based
on the knowledge that it's a good thing to do, based on the knowledge that desire is a
bad thing and that only through contemplation are we going to be free from desire, from
that desire, it's the difference between an attachment to something and an intention to do
something, to carry out some act.
When we desire for an object, then we carry out all sorts of bad deeds in order to get
it.
But when we desire to do something, it doesn't have to be a desire at all.
In fact, it's very hard to think of anyone actually desiring to practice, we pass in the
meditation.
In fact, if they did, you could say it would be a wrong, it would be an unwholesome sort
of mind state.
But what we mean by the fact when we say we desire, we want to meditate.
It's not that we have this desire or this attachment to meditating, it's the fact that
we realize that if we don't meditate and we don't do it in earnest, we're going to slip back
into all sorts of unwholesome states and conduct ourselves wrongly and give rise to all sorts
of suffering and guilt and unhappiness in the future.
So it's because of this that people want to practice meditation or intend to practice meditation.
In fact, this doesn't come easily in regards to meditation.
Most of us have a hard time meditating.
When we come to meditation in the beginning, it's a very unpleasant practice.
It's something that we look towards with a version.
Because we have to give up, we have to let go, we cannot hold on, we cannot chase after,
we can no longer have any of the wonderful things that we believe are making us happy.
We can no longer chase after them.
We have to put up with many unpleasant situations and pleasant circumstances.
Pain and aching soreness, bad thoughts, boredom and restlessness, frustration.
But as we start to either through the practice or by living our lives and meeting with
such suffering, meeting with this realization that there is such suffering.
So our current state of mind is only capable of a great amount of sorrow.
It's not that we see the suffering outside of ourselves.
We see that our minds are not as strong as they should be.
So they suffer when things don't go our way.
For most people it requires them to see very unpleasant situation before they realize
how an equip their mind is to deal with the situation and then they come to practice meditation.
Sometimes people come to meditate innocently without thinking that it's such a big deal.
And only when they meditate do they realize how astray they've got, how weak their minds
are, how unequipped their minds are for any difficulty, any suffering.
And this is where Chanda rises.
The ways we can give rise to Chanda are in this way that through seeing the suffering in
the world around us or through seeing the suffering that comes from our mind through meditation.
And it's something we should reflect upon often that really indeed we are weak in mind.
And if we allow ourselves to continue like this without training ourselves, without training
our minds, they will only meet with suffering in the future.
Chanda is a very important thing.
Whatever you have the intention or the desire to do, that is where we'll define who you
are.
But as Chanda is the mula, the base of all the basis of all states, all damas.
Number two is media, which means effort.
Effort sort of comes in line with desire.
But on the other hand it's not enough just to want to do something or to know that meditation
is enough.
The Buddha said, few are the people in the world who know what needs to be done, who
know that something needs to be done.
More than just living our lives in the plain old ordinary way of getting a job and making
money and settling down and trying to find stability for a short time and then not passing
away.
People who realize that there's more to life than that, it's hard to find.
And he said, well what is even harder to find is people who actually then do something
about it.
And this is where effort comes into play.
If you don't have the effort to actually get up in practice, to get up and do the walking,
to get up and do the sitting, then your desire has very, it's an impotent desire.
We require both of these that we have to put out effort and put out effort at every moment.
So we have the desire to practice, but we also have this intention to practice.
We also have this effort in our practice, putting our mind out at the object, not just
sitting for long hours at a time, but also knowing what's going on and being mindful
of it.
The effort to get rid of unwholesome states, the effort to build up wholesome states, the
effort to guard against the unoriginal unwholesome states, and the effort to give rise to wholesome
states and to augment wholesome states instead of already arisen.
This is another important, the Buddha said that it is through effort that we overcome
suffering.
When suffering comes to us, in whatever many forms that it comes, it is not simple that
there is no simple way out that we're going to just take a pill and suddenly there's
no more suffering.
It's something that we have to work at.
We have to realize this as well.
If we have this effort, then we can truly become free from suffering.
If we don't have the effort, it will be very difficult for us and be even more painful for
us to practice, because we don't have the effort to really and truly sharpen our minds
and it will be very slow going.
So to me, with success, we have to have this effort.
Number three, we need to tak, which means we need to keep, we need to think about practice.
We need to keep it in our minds at all times.
Even when we're not practicing, when we're not doing the walking or when we're not
doing the sitting, we should always try to keep it in mind.
Always try to think about, keep it at the top of our thoughts, at the top of our list
of things to think about things to do.
Everything else should be subservient to the meditation, we should always be thinking
about coming back and being mindful.
Even when we're doing other things during the day, we should try to be mindful doing
them, standing when we're standing, no that we're standing when we're walking or walking,
becoming aware of our feelings and our emotions, becoming aware of our thoughts, and keeping
our mindfulness throughout the day.
Especially in meditation practice, it's something that requires us to keep it in mind.
Other other works that we do, we might be able to put aside for some time.
We set things in motion and then leave them and wait to see what they bring, but with
meditation, it's not the case.
Meditation is like boiling water, if you turn on the hot water and then later you turn
it off and then turn it on and off, the water will never boil, no matter how many times
you come back to turn the water on.
Even as you turn it off, it loses its momentum and something that we have to work on until
we truly do become free from suffering because our state of mind changes, when we're
not mindful, then we lose all of our strength of mind, of competence, effort, concentration,
wisdom.
We have to use the mindfulness at all times to gain these other faculties.
And so it's a great importance that we keep the meditation in mind.
In fact, the Buddha said that only when we sleep, then we should be allowed to take a
break.
But the way we do this, when we slide down to sleep, is even when we slide down to sleep,
we think about the moment when we're going to wake up, when we make a determination in
our mind as to when we're going to wake up, and then we're mindful until the moment
when we fall asleep, when we wake up, we immediately get up and begin to meditate again.
We should try to live in this way and try to be meditating for all of our waking hours.
That's best we can.
When we work in the world, when we have other things to do, obviously this will be less
than this, it will be not the case.
We'll have to interrupt our practice many times.
But at any rate, we can always come back and remind ourselves.
And the more we're able to keep it in mind, the better it will be for us.
And the quicker we'll be able to gain real benefits from the practice.
The fourth quality we monsa, in terms of meditation, this means figuring out the ways in
which we're meditating incorrectly and being able to assess our own meditation practice.
Being able to catch ourselves when we fall into wrong practice or bad habits.
When being able to play with this imperfect mind that we have, when being able to work
with this imperfect state of affairs, when being learning how to roll with the punches
and how to get back up on our feet and how to work from one moment to one moment from
one day to one day and training ourselves at every moment, trying to figure out what it
is that we have to do next to augment our practice, to support our practice, to look
and see as our confidence waning, then we have doubts, we have to look and see and say
to ourselves doubting and doubting.
Sometimes we have too much confidence and so we become greedy or we become lazy, sometimes
we have too much effort and so we become distracted.
Sometimes we have too much concentration and so we become drowsy or tired, means they're
not balanced.
When we have lots of effort that's good but we have to balance it with concentration
and so we work on our faculties in this way.
When we have lots of confidence that's good but without wisdom it's blind and so
we need to balance our faculties.
We need to, we need to catch ourselves in our practice, we need to see why we're failing,
why we're slacking, why we're falling behind and we have to pick ourselves up and redouble
our efforts and bring ourselves back in line and back on track and we have to work on
this at all times.
We monks up because we can't force our practice, we can't force the meditation to be
perfect, we can't force ourselves to be good at meditating so we have to be clever in terms
of how to train ourselves and how to work with what we have and slowly but surely lead
ourselves, lead our minds to a state of clarity and the state of understanding.
So these four are very important for meditators, they're things that we should constantly
be thinking about and considering, we should at least use them as a guide for our practice
in order to at least tell ourselves to make it clear to ourselves that if we don't have
these we're not going to get anywhere and looking to see do we really have this contentment
or this desire to practice and if we don't then we have to work on that and we have
to see where our laziness lies or this evil in our hearts that's polluting our minds
and making us think maybe that meditation is a bad thing or a useless thing or so on.
That's giving us these bad vibes in regards to meditation and we have to work on this.
Look directly at them and see them.
If it's true that meditation is a bad thing then it should stand up to the test.
You should be able to look at these thoughts and come to some rational conclusion that
indeed meditation is a useless thing or a bad thing but what happens when you do is you'll
see that there are negative emotions of anger, of laziness, of cowardlyness even that
we that cause us to be afraid to or be lazy as well.
So we need this kind of contentment and we need to put out effort to see what we want
to practice but are we putting out the effort to actually practice or are we making excuses
and doing other things as our effort being channeled into other pastimes or other activity.
We need to see is are we keeping it in mind or are we really in some meditation in our minds
at all times and we have to look and use this is using we among status is the fourth one
to consider in this way in and of itself as we months and so we have to remind ourselves
to constantly go over this and to look at our practice and not overly analyzing our practice
in the end we do just have to meditate but as we meditate on we have to keep ourselves in
check and see and look are we really practicing sometimes we have to ask our teacher questions
when we can't figure things out for ourselves it won't do to just walk and sit walk
and sit for years without really having some quality to our practice and so these factors
really help to add that quality to them these are called the for iti pata and they are
important for meditators as they are for success in any undertaking so this is the demo
for today now we'll continue on with practice we're doing mindful frustration and walking
and then sitting.
