1
00:00:00,000 --> 00:00:07,280
noting thinking. Some teachers say to note thinking twice and then go back to the

2
00:00:07,280 --> 00:00:12,320
rising and falling when our minds wander in meditation. Others say to note

3
00:00:12,320 --> 00:00:17,360
thinking until the thought or wandering falls away and then go back to the

4
00:00:17,360 --> 00:00:23,520
abdomen which is more precise. Also some teachers teach to only note thoughts

5
00:00:23,520 --> 00:00:28,320
that take you completely away from the rising and falling and not note the

6
00:00:28,320 --> 00:00:34,560
background thoughts. First of all thinking is not something that generally lasts

7
00:00:34,560 --> 00:00:39,520
more than a couple of notings. What lasts is the attachment to the thought

8
00:00:39,520 --> 00:00:44,320
whether you like it or dislike it or if it is interesting for you

9
00:00:44,320 --> 00:00:48,960
that is what is going to keep it coming back and make it seem like the thought

10
00:00:48,960 --> 00:00:54,160
is persisting. Thoughts are actually quite fleeting so if you are just focusing on

11
00:00:54,160 --> 00:00:59,920
the thought. Noting two or three times is really enough or sometimes even once.

12
00:00:59,920 --> 00:01:04,720
Then go back to the rising and falling. If it does keep coming back then you should

13
00:01:04,720 --> 00:01:10,400
try to note the attachment that is causing it to return. You can note liking,

14
00:01:10,400 --> 00:01:17,360
liking, disliking, disliking or whatever the attachment is. As far as only

15
00:01:17,360 --> 00:01:22,240
noting thoughts that take you completely away from the rising and falling you

16
00:01:22,240 --> 00:01:26,400
could do it either way. When you are walking we tend to give people some

17
00:01:26,400 --> 00:01:29,760
leeway. My teacher explained two different approaches

18
00:01:29,760 --> 00:01:33,520
you could adopt. You could stop walking at every thought

19
00:01:33,520 --> 00:01:37,360
or you could just bring your mind back to the foot

20
00:01:37,360 --> 00:01:42,640
and continue going and when you stop at the end of the walking path then you could

21
00:01:42,640 --> 00:01:47,760
practice being mindful of the thoughts. The point is to be present and mindful

22
00:01:47,760 --> 00:01:52,880
of our experiences in the present moment. When you are sitting it is reasonable to

23
00:01:52,880 --> 00:01:58,560
suggest noting all thoughts. Thinking is a very important part of who we are and

24
00:01:58,560 --> 00:02:03,120
an understanding of the nature of thoughts is very beneficial. Sometimes you

25
00:02:03,120 --> 00:02:06,880
catch your thoughts when they are already finished. You find you have been

26
00:02:06,880 --> 00:02:11,120
thinking for a while and at the very end you realize oh I was

27
00:02:11,120 --> 00:02:16,000
thinking for a long time there and then you remind yourself thinking thinking.

28
00:02:16,000 --> 00:02:19,120
Sometimes you catch yourself in the middle of a thought

29
00:02:19,120 --> 00:02:23,760
so in the middle of the thought you say thinking thinking. With practice you

30
00:02:23,760 --> 00:02:28,480
might catch a thought at the very beginning when it first arises. The practice

31
00:02:28,480 --> 00:02:33,520
of catching thoughts sooner and sooner is an important part of the training.

32
00:02:33,520 --> 00:02:38,880
The ability to catch a thought when it first arises shows a clear and mindful

33
00:02:38,880 --> 00:02:43,120
state of awareness. Of course this is usually something

34
00:02:43,120 --> 00:02:48,880
you can only train in intensive meditation courses. Outside of intensive

35
00:02:48,880 --> 00:02:52,960
practice you might think so much that it is better to note

36
00:02:52,960 --> 00:02:57,840
distracted, distracted, and then bring your attention back to the rising and

37
00:02:57,840 --> 00:03:02,400
falling. Not worrying too much about the individual thoughts. The important

38
00:03:02,400 --> 00:03:07,760
thing is to also note attachments to thoughts just as with everything else.

39
00:03:07,760 --> 00:03:11,040
Often problems we have with noting are not so much

40
00:03:11,040 --> 00:03:14,960
caused by the object itself but with our reactions to it.

41
00:03:14,960 --> 00:03:19,280
Our likes and dislikes etc. Such reactions are called

42
00:03:19,280 --> 00:03:23,440
hindrances. It is worth learning more about the five hindrances.

43
00:03:23,440 --> 00:03:29,840
Liking, disliking, drowsiness, distraction, and doubt as they really do get in the

44
00:03:29,840 --> 00:03:45,760
way of our progress.

