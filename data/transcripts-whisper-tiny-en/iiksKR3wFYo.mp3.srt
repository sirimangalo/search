1
00:00:00,000 --> 00:00:09,440
Good evening and welcome back to our study of the Dhamupada.

2
00:00:09,440 --> 00:00:19,240
Today we continue on with verses 209 to 211, which read as follows.

3
00:00:19,240 --> 00:00:35,720
A yogi yunjamatana, yoga sminja, a yujana, a yujang, a tang hitwa, piyagahi, piyatatana, yujinana.

4
00:00:35,720 --> 00:00:49,480
Mahpiyayi samaganti, a piyagahi kudajana, piyanang dasanang dukang, a piyanang jandasanang.

5
00:00:49,480 --> 00:01:16,080
Tasma bhyang nakaiyirata, piyapayuhi papaku, kantati sang nakrijanti, a sang nakti, piyapyang, which means

6
00:01:16,080 --> 00:01:26,160
one who is involved with things, one shouldn't be involved in, and in those things that

7
00:01:26,160 --> 00:01:31,400
one should be involved in, doesn't get involved.

8
00:01:31,400 --> 00:01:45,760
Having abandoned what is useful at that and clinging to what is dear to them.

9
00:01:45,760 --> 00:02:00,760
They will end up enveying those who were more involved or better involved, those who

10
00:02:00,760 --> 00:02:15,740
were strenuous or got, got work done, don't, don't associate or one shouldn't

11
00:02:15,740 --> 00:02:27,240
associate, one shouldn't hold on to what is dear, or what is dear whatsoever, what is

12
00:02:27,240 --> 00:02:41,600
not dear whatsoever, for not to see what is dear, this is suffering, as well as seeing what

13
00:02:41,600 --> 00:02:48,280
is not dear.

14
00:02:48,280 --> 00:02:57,380
For that reason, make nothing dear, piyapayuhi papaku, for it is, for the loss of what

15
00:02:57,380 --> 00:03:10,920
is dear is evil, and here evil just means bad or unpleasant, kantati sang nakrijanti,

16
00:03:10,920 --> 00:03:22,560
there is no, there are no bonds, binds, there are no fetters, for those who hold nothing

17
00:03:22,560 --> 00:03:31,560
dear or under, not dear, isang nakti piyapayu, for those who have nothing that is dear

18
00:03:31,560 --> 00:03:40,640
or the opposite of dear, which is unpleasant, that dear is maybe not, it's an awkward translation,

19
00:03:40,640 --> 00:03:46,720
it might be something you value, something you cherish, something you hold on to, something

20
00:03:46,720 --> 00:03:55,360
you love, we might say, it's probably the most common usage of the word love, and the love

21
00:03:55,360 --> 00:04:02,400
can mean many different things, we say we love cheesecake, or when you say you love your

22
00:04:02,400 --> 00:04:10,600
partner, even when you say you love your parents, it's often, or love your children, it's

23
00:04:10,600 --> 00:04:21,880
often associated with clinging, rather than appreciation or simply friendlyness, so that's

24
00:04:21,880 --> 00:04:28,000
what piyapayuhi means, and we're in the piyavaga, we've just started, this is the first

25
00:04:28,000 --> 00:04:41,240
story from the piyavaga, which means the chapter on dearness or holding things, on cherishing,

26
00:04:41,240 --> 00:04:46,480
so this was, this verse is supposed to have been said in regards to a story about a

27
00:04:46,480 --> 00:04:55,080
family, a family whose members were very dear to each other, we have a mother and a

28
00:04:55,080 --> 00:05:03,400
father and their son, and they were living together in harmony, but then one day the Buddha

29
00:05:03,400 --> 00:05:18,000
came to their, their area, and the son heard him speak and was so encouraged by the things

30
00:05:18,000 --> 00:05:22,560
the Buddha said that he decided that he would, he wanted to become a monk, so he asked

31
00:05:22,560 --> 00:05:30,720
his parents, his parents refused, they said no, he can't, and they tried to keep watch

32
00:05:30,720 --> 00:05:37,040
on him because they figured he would sneak away and go become a monk if they didn't,

33
00:05:37,040 --> 00:05:42,120
and so whenever the mother would go out the father, she would tell the father to look after

34
00:05:42,120 --> 00:05:49,240
him and watch him and spy on him, or basically hold him prisoner, and the same when the

35
00:05:49,240 --> 00:05:54,240
father went out, he would tell the mother, remind the mother to do the same, one day the

36
00:05:54,240 --> 00:06:00,520
father went out and the mother was weaving or spinning wool or something, and so she would

37
00:06:00,520 --> 00:06:07,080
sit in the doorway of their, their hut or their home, and just to make sure he never

38
00:06:07,080 --> 00:06:12,440
didn't even leave the house, I don't, it doesn't say how old this boy was, but maybe

39
00:06:12,440 --> 00:06:20,640
he was just young enough to become a novice, I don't know, they loved him so much that

40
00:06:20,640 --> 00:06:29,480
they wanted to protect him and, and keep him prisoner even, and, but he's he asked

41
00:06:29,480 --> 00:06:35,760
her if he could go outside and to use the, the facilities, maybe the outhouse or something,

42
00:06:35,760 --> 00:06:44,760
he had to go, maybe water the bushes, I don't know, and he had to make a bowel movement,

43
00:06:44,760 --> 00:06:50,760
some sort, and so he asked her to move out of the way, I could, I could just go outside

44
00:06:50,760 --> 00:06:57,400
for a second, so she let him go outside and went back to her work, and as soon as he got

45
00:06:57,400 --> 00:07:03,520
outside he ran to the monastery, asked the monks to ordain him, they ordained him, which

46
00:07:03,520 --> 00:07:07,920
is not really allowed, but this may have been before it was required to have your parents

47
00:07:07,920 --> 00:07:16,640
permission or, anyway, it's just a story that the details not so important, but the

48
00:07:16,640 --> 00:07:23,720
parents, father came home and he said, went into the house and where's our son? Usually

49
00:07:23,720 --> 00:07:28,520
he would greet the father when he came home, didn't do that, where did our son go?

50
00:07:28,520 --> 00:07:35,520
Well, he went out to the washerman, he's he's not back yet, oh, and the father said,

51
00:07:35,520 --> 00:07:39,720
oh, I know, if he's not here, he looked everywhere for him, he said, if he's not here,

52
00:07:39,720 --> 00:07:46,360
he must have gone to the monastery, so the father went to the monastery, sure enough he

53
00:07:46,360 --> 00:07:54,400
saw his son with a shaven head and wearing robes, and he was so aggrieved, so sad, thinking

54
00:07:54,400 --> 00:08:00,720
to himself, how can I go home and live without my only dear son?

55
00:08:00,720 --> 00:08:05,280
And so then and there he said, I'm not going home and he asked the monks to ordain him

56
00:08:05,280 --> 00:08:12,360
as well, shaved his head, put on the robes, became a monk, the mother sitting at home,

57
00:08:12,360 --> 00:08:19,400
wondering where the husband and son have gone, she said, well, I guess probably the son

58
00:08:19,400 --> 00:08:24,400
moved on to the monastery and so she heads off to the monastery and sees them both and says,

59
00:08:24,400 --> 00:08:28,240
wow, what the heck am I going to do at home if they're both at the monastery, I better ordain

60
00:08:28,240 --> 00:08:33,420
as well and she went to the bikunis and they ordained her, shaved her head, put on the

61
00:08:33,420 --> 00:08:41,320
robes, which sounds like a very heartening story, you know, the son led both his mother

62
00:08:41,320 --> 00:08:51,280
and father to become monks, but it didn't work out the best, they, after they ordained

63
00:08:51,280 --> 00:09:00,440
it says they did nothing but socialize, they were so accustomed and attached to each

64
00:09:00,440 --> 00:09:05,920
other, accustomed to being in each other's present, attached to each other's presence

65
00:09:05,920 --> 00:09:13,800
and company and interactions, but they did nothing but sit around and socialize and chat

66
00:09:13,800 --> 00:09:22,680
and the monks got annoyed, upset or disturbed, at least the ones who weren't yet far advanced

67
00:09:22,680 --> 00:09:30,920
in their meditation and so they went in the Buddha and they said, better both her, this

68
00:09:30,920 --> 00:09:36,520
family kind of regret letting them become monks because now they do a sit around and chat

69
00:09:36,520 --> 00:09:43,320
and socialize, they're not accomplishing anything, accepting, increasing their clinging

70
00:09:43,320 --> 00:09:49,760
and disturbing the monastery and so the Buddha called them up and is this true and you

71
00:09:49,760 --> 00:09:58,560
scolded them and said, this is not the way of, it's not the way to have real happiness

72
00:09:58,560 --> 00:10:04,560
see what did the Buddha exactly say, he said, why do you do this, this isn't the proper

73
00:10:04,560 --> 00:10:08,680
way for you to conduct yourself, but but then or most, sir, it's impossible for us to

74
00:10:08,680 --> 00:10:16,720
live apart and he said once you leave home, having left home, this is very improper and

75
00:10:16,720 --> 00:10:25,280
then you taught, taught the verse basically, verses.

76
00:10:25,280 --> 00:10:31,320
So we have a lesson from the story, we have a lesson from the verses, from the story,

77
00:10:31,320 --> 00:10:39,160
I guess they're two lessons, the first most obvious one is we don't approve chatting

78
00:10:39,160 --> 00:10:46,200
and socializing, so in a monastery and the meditation center, it's why we have these sort

79
00:10:46,200 --> 00:10:57,080
of rules, not just because the Buddha said not to and said it's improper, but he got

80
00:10:57,080 --> 00:11:03,000
admit if the Buddha says that something is improper, it's probably good idea to follow

81
00:11:03,000 --> 00:11:08,840
even blindly because well, we don't know many, many things and taking your teachers

82
00:11:08,840 --> 00:11:17,480
advice is often a good thing, good sort of default until you have reasoned it out.

83
00:11:17,480 --> 00:11:27,040
But we can find reasons easily, of course, socializing and chatting in specific are quite

84
00:11:27,040 --> 00:11:33,400
distracting from our practice and reinforcing of our defilement, the bad things inside that

85
00:11:33,400 --> 00:11:40,160
we're trying to change about ourselves bad habits are quite reinforced simply by engaging

86
00:11:40,160 --> 00:11:46,840
in idle speech because it's very difficult to be mindful, it's very habitual, just the

87
00:11:46,840 --> 00:11:53,160
act of speech is caught up in so much of our habits, our manners of speaking, the topics

88
00:11:53,160 --> 00:11:58,280
of speech, you know, often we sit around gossiping and talking bad about other people

89
00:11:58,280 --> 00:12:04,480
and so on, that sort of thing, teasing, manipulating, joking and all of this, it's just part

90
00:12:04,480 --> 00:12:10,280
of our habits that are caught up with speech which are very difficult to change, it's

91
00:12:10,280 --> 00:12:14,200
not that speaking can't be mindful, you can even be mindful as you're speaking mindful

92
00:12:14,200 --> 00:12:19,600
of your lips, moving even mindful of the sound of your own voice, it's just mostly

93
00:12:19,600 --> 00:12:23,800
we're not doing that, that's the reason why we sit and chatter, not as a meditation

94
00:12:23,800 --> 00:12:29,480
exercise usually, but we shouldn't think that silence is necessary, it isn't, it never

95
00:12:29,480 --> 00:12:34,280
was, it was something that the Buddha criticized, in fact, if you say I'm not going to

96
00:12:34,280 --> 00:12:41,080
talk no matter what, you shouldn't ever go to that extreme, you can, for the most part

97
00:12:41,080 --> 00:12:46,520
not talk and the remarks who did this, they didn't talk hardly at all, but then they didn't

98
00:12:46,520 --> 00:12:51,920
talk at all actually, but then every five days or so they would get together and they

99
00:12:51,920 --> 00:12:58,200
would talk all night, talk about the dhamma, but it was a means of getting feedback and

100
00:12:58,200 --> 00:13:04,200
interacting with your fellows, it's just important, just like we talk every morning, so

101
00:13:04,200 --> 00:13:14,240
you have a chance to interact with someone who has a little bit of background and experience,

102
00:13:14,240 --> 00:13:20,520
but the deeper lesson of course is this idea of holding something dear, so the real lesson

103
00:13:20,520 --> 00:13:29,040
this story gives to us and other stories of the Buddha, or of the Buddha's time is how strongly

104
00:13:29,040 --> 00:13:38,800
people cling to each other, how strongly this habit becomes, how powerful it can be,

105
00:13:38,800 --> 00:13:44,400
the strength of their attachment was preventing them from allowing this young man to do something

106
00:13:44,400 --> 00:13:51,360
great, right, how often you hear about parents not allowing their children to become more

107
00:13:51,360 --> 00:13:58,040
dain, I mean I do in Asian circles anyway, not because they think Buddhism is bad, they

108
00:13:58,040 --> 00:14:02,680
generally Buddhists themselves, there are cases where parents don't want their kids to

109
00:14:02,680 --> 00:14:08,880
ordain, especially in the West because they think Buddhism is evil and wrong and so on,

110
00:14:08,880 --> 00:14:14,800
but that's not the case often in the East, in Buddhist countries, they just don't want

111
00:14:14,800 --> 00:14:19,600
it because they're so attached to their kids, even attached and worried about their kids'

112
00:14:19,600 --> 00:14:27,560
future, they want their kids to be rich and in society and carry on their name and have

113
00:14:27,560 --> 00:14:34,080
kids, they want grandkids and that sort of thing, so it's quite horrific to think that

114
00:14:34,080 --> 00:14:41,840
their kids might become monks and then how strong was the attachment once he had ordained

115
00:14:41,840 --> 00:14:47,080
that they couldn't live without him, they couldn't continue on their lives, they were

116
00:14:47,080 --> 00:14:51,960
so desperate that they decided to ordain even though they didn't seem to be all that

117
00:14:51,960 --> 00:14:57,400
inclined towards Buddhist practice and then once they were ordained their attachment

118
00:14:57,400 --> 00:15:02,200
was so strong that even the sun wasn't able to practice because he was so involved

119
00:15:02,200 --> 00:15:12,560
with socializing with his parents, so it gives us this example of very strong attachment.

120
00:15:12,560 --> 00:15:20,760
When the verses themselves provide sort of the lesson on why it's not a good idea

121
00:15:20,760 --> 00:15:24,880
to attach and there's three lessons, each verse is actually a little bit different and

122
00:15:24,880 --> 00:15:30,680
you can find a distinct lesson from each of them that describes a very important part

123
00:15:30,680 --> 00:15:41,360
of the Buddhist teaching, so the first verse is mostly describing our acts, acts and

124
00:15:41,360 --> 00:15:46,360
omissions, talks about getting involved with things you shouldn't get involved in, I use

125
00:15:46,360 --> 00:15:52,880
the word involved, yoga, yoga means something like bound or attached, but not exactly

126
00:15:52,880 --> 00:16:00,040
attached, tied to yoga is kind of like I've talked about before from yoke, a yoke in English,

127
00:16:00,040 --> 00:16:06,760
where the yoke on the ox is this thing that ties it to the cart, so it can pull it.

128
00:16:06,760 --> 00:16:14,400
So when you're a harnessed to a cart, you do the work of pulling the cart, as I understand

129
00:16:14,400 --> 00:16:21,520
how the word evolved, so it's used often in Buddhism to talk about this person who is dedicated,

130
00:16:21,520 --> 00:16:29,760
a dedicated practitioner, yoga, what Shara is a very common Buddhist word, someone who is

131
00:16:29,760 --> 00:16:38,000
engaged in the work, but there's things that you shouldn't engage in, you shouldn't get involved

132
00:16:38,000 --> 00:16:45,160
and you shouldn't work at, and someone who engages in those things and doesn't engage

133
00:16:45,160 --> 00:16:52,120
in the things that you shouldn't engage in, and then he says it's going to be jealous

134
00:16:52,120 --> 00:16:57,360
or envious, it's going to feel bad when they see everyone else in a monastic setting, especially

135
00:16:57,360 --> 00:17:06,200
or in a meditation center, everyone else is becoming enlightened and there's still useless.

136
00:17:06,200 --> 00:17:16,520
The Buddha gives an example, not here, but it's a famous simile of a cowherd or a shepherd.

137
00:17:16,520 --> 00:17:25,200
You might think of a person who works a servant in a rich household or a person, cows

138
00:17:25,200 --> 00:17:29,120
we don't think of so much because we don't think of cows as highly as they would have

139
00:17:29,120 --> 00:17:35,680
in the time of the Buddha, but it's the equivalent of being surrounded by riches, but

140
00:17:35,680 --> 00:17:39,480
having to take care of them like a banker, a person who can work in a bank, surrounded

141
00:17:39,480 --> 00:17:47,000
by such wealth, but have no access to the bank, a person who shepherds or cowherds, cows

142
00:17:47,000 --> 00:17:53,560
surrounded by very valuable livestock, but they're just working for a minimum wage, they

143
00:17:53,560 --> 00:18:00,080
don't actually get to taste the milk from the cows or the cheese or the meat now as we

144
00:18:00,080 --> 00:18:07,680
kill the cows, you don't get any of the benefit from the animals.

145
00:18:07,680 --> 00:18:12,680
Person who works in a mansion can be surrounded by great luxury, but never have access

146
00:18:12,680 --> 00:18:17,680
to it, they can't sleep in the beds or sit in the chairs or eat the food, they get a

147
00:18:17,680 --> 00:18:26,800
minimum wage and then they go home, or they sleep in the servant's quarters.

148
00:18:26,800 --> 00:18:30,720
So the lesson here is about acts and omissions.

149
00:18:30,720 --> 00:18:36,720
In Buddhism, of course, it's democratic, or it's even democratic, there's equality in the

150
00:18:36,720 --> 00:18:45,520
sense that it's accessible to everyone, it's not a case where you're set in your social

151
00:18:45,520 --> 00:18:52,000
class, so it's different in that you have the option to become rich in Buddhism, you have

152
00:18:52,000 --> 00:18:57,120
the option to be the person who owns the cows, to be the person who owns the money in

153
00:18:57,120 --> 00:19:02,960
the bank, all you have to do is do the work, engage in what you should engage in, don't

154
00:19:02,960 --> 00:19:10,120
engage in what you shouldn't engage in.

155
00:19:10,120 --> 00:19:14,960
So this relates to actions, karma, really.

156
00:19:14,960 --> 00:19:24,200
There's a story of a monk who is very much attached to his food, he lived alone, and this

157
00:19:24,200 --> 00:19:28,920
is a common thing, I've seen this, it's discouraging how common it can be, monks live

158
00:19:28,920 --> 00:19:34,720
alone because they're jealous of their gains, they live in a village and they're surrounded

159
00:19:34,720 --> 00:19:42,040
by people who, for lack of a better monk, a better monastery, go always go and pay respect

160
00:19:42,040 --> 00:19:47,200
and give offerings to this monk, so anytime another monk comes, they're very protective

161
00:19:47,200 --> 00:19:53,400
of their gains, the things they hold dear, so they'll chase the other monk, so why

162
00:19:53,400 --> 00:19:57,400
you've got, you have to be a little bit suspicious when you see a monastery with only one

163
00:19:57,400 --> 00:20:04,000
monk, it's unfortunately common for that to be the case.

164
00:20:04,000 --> 00:20:07,520
So this is a story in the Buddha's time, it was even the case, there was this monk who

165
00:20:07,520 --> 00:20:14,400
was in this sort of situation and another monk came and this monk was so profoundly peaceful

166
00:20:14,400 --> 00:20:20,160
in his demeanor, he would walk slowly when he talked, it was thoughtful and he was in

167
00:20:20,160 --> 00:20:25,680
Arahan, he was enlightened as it turns out, this monk of course didn't know that, he

168
00:20:25,680 --> 00:20:32,800
was just scared because this was clearly a powerful sort of spiritual individual and he

169
00:20:32,800 --> 00:20:39,240
thought to himself, I'm ruined, if the people of the village see this monk, I'll never

170
00:20:39,240 --> 00:20:46,000
get any support and so he arranged it, so in the morning he said he would ring the bell

171
00:20:46,000 --> 00:20:49,600
and in the morning he went and touched the bell with his finger and said, oh I've rung

172
00:20:49,600 --> 00:20:55,520
the bell, this monk hasn't come and he went to the village and they asked, oh where's

173
00:20:55,520 --> 00:20:58,560
the other monk, we heard there's another monk living there and he said, oh I think he's

174
00:20:58,560 --> 00:21:06,840
sleeping in, I rang the bell and he didn't come and there was one laywoman who was

175
00:21:06,840 --> 00:21:16,000
taking care of him, who made very sort of luscious with the word, very delicate, superior

176
00:21:16,000 --> 00:21:26,760
food, good food, delicious food, refined food, fine food and she said, oh well then take

177
00:21:26,760 --> 00:21:33,080
back, you eat some food here and then take back food for him and your bowl and so he ate

178
00:21:33,080 --> 00:21:36,920
the meal and she put food in his bowl for the other monk and he looked at it and he said,

179
00:21:36,920 --> 00:21:40,440
I can't let him taste this or he'll never leave, he'll be so attached to the food that

180
00:21:40,440 --> 00:21:46,920
he'll stick around and then I'm ruined and so on the way back to the monastery he dumped

181
00:21:46,920 --> 00:21:53,000
the food out on the side of the road, the other monk, the arrow hunt had woken up of

182
00:21:53,000 --> 00:21:58,160
course early in the morning he wasn't lazy or wasn't sleeping in and he knew what the other

183
00:21:58,160 --> 00:22:02,680
monk had done I think he had some kind of psychic ability to see that the monk was doing

184
00:22:02,680 --> 00:22:08,440
that and then he knew that the monk had not actually rang the bell and he thought to himself

185
00:22:08,440 --> 00:22:15,080
I can't live with such a person and he went on this way. The monk who was very much attached

186
00:22:15,080 --> 00:22:20,560
to the food and as a result getting engaging in practices he really shouldn't have been

187
00:22:20,560 --> 00:22:30,480
engaging in and ended up being born in hell and ended up becoming a, I think he was, there

188
00:22:30,480 --> 00:22:36,400
was this monk in the time of our Buddha, yes this was a monk, starts with an emendic of

189
00:22:36,400 --> 00:22:45,120
me, maybe I can't remember, one monk who, not mendicah, look here maybe I can't remember,

190
00:22:45,120 --> 00:22:51,280
he never got enough to eat, as a result of his bad karma of throwing out this food that

191
00:22:51,280 --> 00:22:56,560
was dedicated and designated for this other monk, he never got enough food and all the

192
00:22:56,560 --> 00:23:02,800
time he was ordained until finally Sariputta was the one who ordained him and may have been

193
00:23:02,800 --> 00:23:11,200
mendic and not anything and I don't remember his name, someone's going to tell me what

194
00:23:11,200 --> 00:23:19,440
the name was eventually, Sariputta had to feed him with a spoon and finally he was dying

195
00:23:19,440 --> 00:23:28,240
and Sariputta on the last day, Sariputta went for arms, he went, they went for arms together and this

196
00:23:29,440 --> 00:23:34,800
monk didn't get any food, he said and neither of them got any food because of the bad karma

197
00:23:34,800 --> 00:23:41,360
this monk, the story is a very long story, when he was born he was born a beggar and as soon as he was

198
00:23:41,360 --> 00:23:47,760
born, as soon as he was conceived the whole company of beggars didn't get anything to eat,

199
00:23:48,640 --> 00:23:53,040
they kicked his parents out and his parents didn't get anything to eat and then once he was born

200
00:23:54,480 --> 00:23:57,920
none of them got anything to eat so they kicked him out and he never got anything to eat until

201
00:23:57,920 --> 00:24:03,760
finally Sariputta found him and ordained him and he still didn't get anything to eat, Sariputta took

202
00:24:03,760 --> 00:24:09,040
him on arms, neither of them got anything to eat, Sariputta told them to stay at home, Sariputta went

203
00:24:09,040 --> 00:24:14,080
on arms, sent food back with a novice to him, the novice ate the food on the way,

204
00:24:16,160 --> 00:24:22,640
Sariputta went back to the monastery and asked him if he didn't, he said, I'm fine,

205
00:24:23,520 --> 00:24:29,120
he was in Arahan at that point I think and Sariputta took his food, took his bowl and actually with

206
00:24:29,120 --> 00:24:35,200
his own hand, hand fed into his mouth and just before he passed away he got a real meal,

207
00:24:35,920 --> 00:24:40,320
he actually got food to eat and I can't remember his name.

208
00:24:43,040 --> 00:24:50,240
But the story here is about his evil deeds because of his craving and this is of course

209
00:24:50,240 --> 00:24:56,880
just one of many many stories of how clinging or holding something dear,

210
00:24:56,880 --> 00:25:04,560
something silly like food, right, can lead us to do such evil, when we hold people dear, we

211
00:25:04,560 --> 00:25:12,800
when we cherish other people the things we'll do to get what we want, that's why I think you

212
00:25:12,800 --> 00:25:20,480
can extrapolate to why things like rape occur and that sort of things horrible things that happen,

213
00:25:20,480 --> 00:25:26,080
that's why people go to war because they cherish what other people have or they cherish what they

214
00:25:26,080 --> 00:25:35,680
have, we cling to our possessions, we cling to nationalities, we cling to

215
00:25:37,440 --> 00:25:41,760
color and so many things wealth is something rich people clinging to wealth now we see

216
00:25:42,640 --> 00:25:46,880
the rich getting richer, the poor getting poorer and this class warfare,

217
00:25:48,400 --> 00:25:54,640
fighting and in the truth of it is from a Buddhist perspective that the rich are just trading

218
00:25:54,640 --> 00:25:58,960
places with the poor every lifetime so if you're rich and stingy then you get poor and then you

219
00:25:58,960 --> 00:26:03,680
realize how awful it is to be stingy and so you treat you start to be a little more generous and

220
00:26:03,680 --> 00:26:09,120
then maybe you become rich again but we're seeing less and less generosity I think and that's

221
00:26:09,120 --> 00:26:16,240
why the disparity, why people are so few people are rich, so few people actually have

222
00:26:16,240 --> 00:26:28,240
wealth or affluence, luxury because while they've traded places they've done some good things

223
00:26:28,240 --> 00:26:31,920
that give them that and probably when they die they're going to lose it all

224
00:26:36,560 --> 00:26:42,640
so the first first deals with this, this idea that we hold things dear,

225
00:26:42,640 --> 00:26:48,000
leads us to do evil things, it leads us to fail to do good things, it's why this family was

226
00:26:48,000 --> 00:26:53,200
unable to practice meditation and in our meditation this is the important lesson that

227
00:26:54,880 --> 00:27:00,480
it prevents us from being mindful when we start to get soft track and thinking about the past

228
00:27:00,480 --> 00:27:07,200
or the future greedy about or clinging to things that we might get or things that we had or so

229
00:27:07,200 --> 00:27:16,000
it leads us to fail to be mindful, it's why entertainment is something we should put aside during

230
00:27:16,000 --> 00:27:20,720
a meditation course because it gets in the way of our mindfulness.

231
00:27:23,200 --> 00:27:33,840
The second verse deals more directly with the suffering, it says seeing what is not dear,

232
00:27:33,840 --> 00:27:39,280
that's suffering and seeing what you don't like, that's suffering and what you like when you

233
00:27:39,280 --> 00:27:52,560
don't see that, that's also suffering and many stories like this, a story of course of Patachara

234
00:27:53,360 --> 00:28:01,200
who lost her two children and her husband, she was traveling back to see her family and her

235
00:28:01,200 --> 00:28:10,400
husband got bitten by a snake and then one of her babies was taken away by a bird, she had just

236
00:28:10,400 --> 00:28:19,280
given birth and she placed, she left the older son by the side of the river, this river and she

237
00:28:19,280 --> 00:28:25,280
crossed the river with the baby, left the baby on the side of the other side, started across

238
00:28:25,280 --> 00:28:33,120
back the river and then she turned around and saw that this bird had seen her newborn baby and

239
00:28:33,120 --> 00:28:37,600
thought it was a piece of meat and grabbed it and so she waved at the bird in the middle of the

240
00:28:37,600 --> 00:28:44,720
river trying to chase, trying to scare it off and the older child on the other bank saw her waving

241
00:28:44,720 --> 00:28:50,640
and thought, she's calling me and gotten to the river and got swept away by the river, she lost

242
00:28:50,640 --> 00:28:57,920
both of her children and when she got home to her parents now completely alone, she found that

243
00:28:57,920 --> 00:29:05,520
her parents had died in their house and burnt down in the storm and she went crazy, she lost her

244
00:29:05,520 --> 00:29:09,920
mind, she was so distraught that she just was all completely overwhelmed.

245
00:29:09,920 --> 00:29:21,040
Such suffering comes from, I mean the potential for such suffering, it's not that you can't

246
00:29:21,040 --> 00:29:28,000
enjoy the things that you hold dear, it's just that you engage in this kind of happiness

247
00:29:28,800 --> 00:29:36,800
that is fraught with uncertainty and is dependent. In mindfulness we try to become independent,

248
00:29:36,800 --> 00:29:41,920
it's another approach to happiness, it's much more sustainable, it's much more happy really

249
00:29:43,040 --> 00:29:46,880
because we think these things make us happy, they only give us greater and greater clinging,

250
00:29:46,880 --> 00:29:49,600
the happiness disappears and we're left with craving and clinging.

251
00:29:54,720 --> 00:30:04,000
Another story is centity, many stories but centity is another example, he was a minister to a king

252
00:30:04,000 --> 00:30:10,400
and he got rewarded and he ended up living like a king for a while and his girlfriend just

253
00:30:10,400 --> 00:30:16,080
danced her, he was very fond of, she dies, all of a sudden just in front of his eyes while

254
00:30:16,080 --> 00:30:22,160
she's dancing because she was like a starving herself, it was a thing for dancers to do,

255
00:30:22,720 --> 00:30:29,600
still as I suppose and she suddenly got nervous, some kind of nerve damage and died

256
00:30:29,600 --> 00:30:35,280
and he had to go see the Buddha and the Buddha had to explain to him, oh yes,

257
00:30:36,000 --> 00:30:40,480
this is what happens when you hold things dear in permanence.

258
00:30:44,880 --> 00:30:51,040
So the second verse relates to the suffering that comes from clinging, the third verse

259
00:30:51,040 --> 00:31:00,640
relates specifically to the clinging that comes from holding things dear, so the clinging aspect

260
00:31:00,640 --> 00:31:07,920
itself, it says there are no binds, there are no fetters for those who have hold nothing dear

261
00:31:07,920 --> 00:31:14,480
and that's really the point, we don't see how dangerous it is to get attached to things,

262
00:31:15,440 --> 00:31:18,880
we don't see how much suffering is involved, how distraught we can become

263
00:31:18,880 --> 00:31:25,760
when the things we hold dear, people who hold on to relatives and belief family is so important

264
00:31:25,760 --> 00:31:33,440
are the first ones to grieve deeply and really in a way that they would never wish on anyone else

265
00:31:34,640 --> 00:31:40,640
when they lose their relatives and even when their relatives are there, the more clinging we

266
00:31:40,640 --> 00:31:46,240
have towards them we think one's good, hold on to your family but these are the ones that fight

267
00:31:46,240 --> 00:31:51,760
the most with their relatives that argue and when the relatives are not the way they want them to be

268
00:31:52,960 --> 00:31:59,200
if a stranger said something to you you might not react but when or a stranger did something you

269
00:31:59,200 --> 00:32:05,440
didn't expect doesn't bother you but when your relatives change sometimes it's just their children

270
00:32:05,440 --> 00:32:12,080
doing things they don't want like dying their hair or wearing different clothes, not getting good

271
00:32:12,080 --> 00:32:19,120
marks on their tests, someone else's kid doesn't do well in school, maybe you're happy because

272
00:32:19,120 --> 00:32:25,920
it means your child is superior to them, you feel proud of your children but when your children

273
00:32:25,920 --> 00:32:32,000
do not well because of your attachment to them you're scared for their future and not just as

274
00:32:32,000 --> 00:32:40,000
your duty as a parent to teach them and help them but you suffer, children suffer when their parents

275
00:32:40,000 --> 00:32:47,440
say things to them right, if another person were to criticize you you might be able to shrug it

276
00:32:47,440 --> 00:32:54,080
off but when your parents do often you get very angry when your parents school new or so on

277
00:32:55,760 --> 00:33:03,360
because of attachment because we were so caught up in our desire for them to be a certain way

278
00:33:03,360 --> 00:33:11,360
and why it's interesting to especially stress the fact that each verse specifies a different

279
00:33:12,000 --> 00:33:19,760
aspect of the teaching is because these three together the clinging, the action, the karma

280
00:33:19,760 --> 00:33:28,320
and the suffering that comes from it are the cycle of samsara, it's called kilesa kama vibaka,

281
00:33:28,320 --> 00:33:38,160
these three are the circle of craving, circle of life I guess in Buddhism, when you have craving

282
00:33:38,160 --> 00:33:45,360
then there is actions, you act down on it, we do bad things, evil things, things that hurt

283
00:33:45,360 --> 00:33:51,680
ourselves that hurt others, we fail to do good things of course because we're so infatuated with

284
00:33:51,680 --> 00:34:00,000
getting what we want and we suffer because the actions lead to suffering that's the whole point,

285
00:34:00,000 --> 00:34:05,360
they wouldn't be bad, they wouldn't be considered evil if they didn't lead to suffering but they do

286
00:34:06,640 --> 00:34:13,120
clinging to good things and our aversion to bad things leads us to suffer,

287
00:34:13,120 --> 00:34:24,400
these things manipulate others hurt others and as a result we suffer, we suffer when we don't

288
00:34:24,400 --> 00:34:31,520
get what we want, we suffer when we get what we don't want revenge and so on and of course the

289
00:34:31,520 --> 00:34:38,320
suffering leads us to cling more, we cling to when bad things happen to us, when we suffer

290
00:34:38,320 --> 00:34:47,840
we do whatever we can to fix it, to get a pleasant experience and we create more and more clinging

291
00:34:49,120 --> 00:34:54,960
because of the clinging then there's more suffering, defilements lead to action, action leads to

292
00:34:54,960 --> 00:35:01,040
results, results we react to them again and so we create this snowball effect, this is why even

293
00:35:01,040 --> 00:35:05,920
in meditation you can see this happening, you can see how when you get upset about something

294
00:35:05,920 --> 00:35:10,800
then you get upset about that and you build and build the upset, that's when it becomes a real

295
00:35:10,800 --> 00:35:16,480
problem, same with craving, if you want something it's just one thing but when you obsess over it

296
00:35:17,040 --> 00:35:20,800
and you like the fact that you're wanting, you feel the pleasure associated with wanting

297
00:35:21,440 --> 00:35:22,480
then you increase it

298
00:35:27,440 --> 00:35:32,480
and so the final part of the third verse gives of course the opposite the way out

299
00:35:32,480 --> 00:35:39,360
that really a much better way, a much more peaceful and happy way to live is when you don't hold

300
00:35:39,360 --> 00:35:48,160
anything dear or under, you change this whole philosophy, you abandon the idea that happiness

301
00:35:48,160 --> 00:35:57,280
can come from cherishing things or being averse to certain things and you cultivate an open mind

302
00:35:57,280 --> 00:36:05,520
you know ability to experience the whole spectrum of reality because we have two choices,

303
00:36:05,520 --> 00:36:16,320
we can limit our experiences to those things that we like or we can stop liking and we can be

304
00:36:16,320 --> 00:36:24,560
open to all experiences of course the former sounds good on paper but is fraught with dangers

305
00:36:24,560 --> 00:36:30,480
and problems and is a cause is the reason why there's so much suffering in the world because there

306
00:36:30,480 --> 00:36:36,960
is so much suffering in the world it's an undeniable fact as much as we want to ignore it the reality

307
00:36:36,960 --> 00:36:43,200
is there is great suffering in the world and it's all because it's all because of our clinging

308
00:36:43,200 --> 00:36:49,600
because of our attachments because of holding cherishing certain things and hating and despising

309
00:36:49,600 --> 00:36:56,960
other things so that's what we try that's why mindfulness is so central the Buddhism it's

310
00:36:56,960 --> 00:37:03,200
why we're so focused on mindfulness because mindfulness is that middle way it is the avoiding

311
00:37:03,200 --> 00:37:10,080
into two extremes it is a third option where you don't have to chase after things or run away

312
00:37:10,080 --> 00:37:17,680
from things where you simply experience things as they are there's no more good there's no more

313
00:37:17,680 --> 00:37:24,960
bad there's not even any me or mine to cling to it there only is experience it is what it is

314
00:37:26,240 --> 00:37:32,320
and so by simply reminding yourself of that every moment when you can see or hear or have

315
00:37:32,320 --> 00:37:38,640
seeing just be seeing have hearing just be hearing you create a new perspective a new way of

316
00:37:38,640 --> 00:37:44,400
looking at reality instead of things you have to fix and control to being things that you should

317
00:37:44,400 --> 00:37:53,840
experience and understand so they're very good set of verses that's the Democratic

318
00:37:53,840 --> 00:38:17,600
for tonight thank you for listening we'll show all the best

