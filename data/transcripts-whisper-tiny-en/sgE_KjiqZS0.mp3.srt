1
00:00:00,000 --> 00:00:13,840
Okay, good evening, everyone, we are live, I think.

2
00:00:13,840 --> 00:00:20,480
So questions and answers, our website's been having some trouble, so apologies for that.

3
00:00:20,480 --> 00:00:29,720
And much gratitude to the people who, no one has gotten paid as far as I know to make this

4
00:00:29,720 --> 00:00:30,720
website.

5
00:00:30,720 --> 00:00:39,680
And to keep it running, we have volunteers in Germany, Australia, America, and probably

6
00:00:39,680 --> 00:00:49,360
many other places who have gotten this website going and kept it going, so it's kind of amazing

7
00:00:49,360 --> 00:00:51,160
really.

8
00:00:51,160 --> 00:00:59,040
We're not some powerful corporation with money and resources.

9
00:00:59,040 --> 00:01:12,360
Just a bunch of volunteers keen on mindfulness, so thank you to all of them, much blessing.

10
00:01:12,360 --> 00:01:16,200
So now we're going on to questions, I don't think it's working perfectly yet, I still

11
00:01:16,200 --> 00:01:22,320
see a spinning wheel here, but I do see questions, oh, I'm supposed to start this.

12
00:01:22,320 --> 00:01:36,160
I don't think it's going to be on time, better late than never.

13
00:01:36,160 --> 00:01:43,720
Okay, we now have an audio stream as well, which somehow syncs the questions, but I don't

14
00:01:43,720 --> 00:01:48,520
think that's going to work anyway because the website's not, the buttons are working.

15
00:01:48,520 --> 00:01:52,520
Anyway, none of that really matters.

16
00:01:52,520 --> 00:02:04,440
It's a sign of impermanence uncertainty and non-self suffering or inability to control.

17
00:02:04,440 --> 00:02:11,720
I don't probably bring up the three characteristics enough on my talks and on the internet.

18
00:02:11,720 --> 00:02:18,400
Something that we should always keep in mind, I do talk to meditators about them.

19
00:02:18,400 --> 00:02:26,360
It's uncertainty because we're fixated on stability, trying to have things be constant,

20
00:02:26,360 --> 00:02:32,480
trying to find this state that's going to just last satisfaction, we're trying to find

21
00:02:32,480 --> 00:02:36,840
satisfaction, so teaching is on suffering or important.

22
00:02:36,840 --> 00:02:40,320
And by suffering, it's just a word, it's the label we use.

23
00:02:40,320 --> 00:02:47,000
We really just mean it unsatisfying, so suffering is unsatisfying, but there's a lot of things

24
00:02:47,000 --> 00:02:48,760
that are unsatisfying.

25
00:02:48,760 --> 00:02:55,720
Pleasure is also unsatisfying, it doesn't last, it doesn't satisfy it.

26
00:02:55,720 --> 00:03:03,960
And non-self, I guess Buddhism does deny the belief that there is a self in this or

27
00:03:03,960 --> 00:03:09,120
in that, but more to the point, it's because we cling to things as being me and mine

28
00:03:09,120 --> 00:03:18,800
and under my control, and so to see too, to understand, but also to see, and more importantly

29
00:03:18,800 --> 00:03:25,680
really to see through our practice that even our own bodies and minds aren't under our

30
00:03:25,680 --> 00:03:26,680
control.

31
00:03:26,680 --> 00:03:32,640
It's very important to keep these in mind also when you're asking questions, what

32
00:03:32,640 --> 00:03:36,240
do I do about this, what do I do about that?

33
00:03:36,240 --> 00:03:43,040
The answer is that what you're experiencing is what you're supposed to experience, and

34
00:03:43,040 --> 00:03:49,880
trying to do something is denying the fact that it's a problem, it's not something you can

35
00:03:49,880 --> 00:03:56,560
fix, it's not under your control, and simply seeing that rather than trying to fix it

36
00:03:56,560 --> 00:04:03,200
and find a way to get back to a, the way you used to be, is an important part of the change

37
00:04:03,200 --> 00:04:11,160
of enlightenment, stop trying to control the world outside, but also control your own mind.

38
00:04:11,160 --> 00:04:17,480
So that's the first question we have tonight, got a lot of upvotes, good question, but

39
00:04:17,480 --> 00:04:24,200
I think it's fairly easily answered, so how to develop more stable mindfulness?

40
00:04:24,200 --> 00:04:28,800
So without going on to the rest of the question, that is an important aspect of it, and

41
00:04:28,800 --> 00:04:33,840
there's really no answer that I can give that's going to satisfy, the answer is to practice

42
00:04:33,840 --> 00:04:40,440
more, and more intensively as best you can when you have a chance.

43
00:04:40,440 --> 00:04:47,200
But then you talk about remedying, getting lost in thoughts and fantasies, and how it seems

44
00:04:47,200 --> 00:04:53,640
like it's going on without any improvement, there's no progress even the opposite, the tendency

45
00:04:53,640 --> 00:05:00,440
to drift after a couple of moments or minutes seems to be reinforced.

46
00:05:00,440 --> 00:05:07,200
So part of that is, yes, you have to practice more and potentially more intensively,

47
00:05:07,200 --> 00:05:12,440
but a bigger part of what you're seeing, why you say it's not getting better, because

48
00:05:12,440 --> 00:05:17,080
if you pass it in some sense, it's not about seeing it get better, it's about seeing

49
00:05:17,080 --> 00:05:21,360
how bad it is, it's about seeing how broken it is.

50
00:05:21,360 --> 00:05:26,600
The system is broken and you can't fix it, you can't fix your mind.

51
00:05:26,600 --> 00:05:31,280
But when you see, and the more you see, how broken the mind is, and how the whole system

52
00:05:31,280 --> 00:05:38,280
of how the mind works, and how karma works, and everything is, is irreparably broken.

53
00:05:38,280 --> 00:05:41,880
The less concerned you get about it, oh yes, my mind is wandering before that would

54
00:05:41,880 --> 00:05:44,240
make you very angry.

55
00:05:44,240 --> 00:05:49,480
Now you see that it's wandering and you get less angry, why?

56
00:05:49,480 --> 00:05:52,600
Because while you see that you can't control it, you see that getting angry about it

57
00:05:52,600 --> 00:05:56,920
doesn't help, you see that really the real problem, or the much bigger problem is that

58
00:05:56,920 --> 00:06:06,960
I'm getting angry.

59
00:06:06,960 --> 00:06:10,400
Much more roundabout than saying, how do I say, so I don't know what you're seeing

60
00:06:10,400 --> 00:06:15,960
at your end, but this is what I've got.

61
00:06:15,960 --> 00:06:20,400
How should I approach the knowledge that comes out of my practice, or the Buddha said

62
00:06:20,400 --> 00:06:22,880
you should approach it like a raft?

63
00:06:22,880 --> 00:06:30,400
A raft is something that helps you get across a difficulty, a body of water.

64
00:06:30,400 --> 00:06:34,080
But once you've gotten across that body of water, in other words, the knowledge was

65
00:06:34,080 --> 00:06:37,040
useful, the knowledge was a good thing.

66
00:06:37,040 --> 00:06:41,960
You don't carry the raft with you, so don't carry the knowledge with you.

67
00:06:41,960 --> 00:06:48,520
It's good that it's something that improves you, but knowledge comes from practice.

68
00:06:48,520 --> 00:06:52,320
So the real improvement doesn't come from the knowledge, it comes from the practice.

69
00:06:52,320 --> 00:06:54,600
The knowledge is the improvement.

70
00:06:54,600 --> 00:07:01,360
You stop practicing, the knowledge just stops coming, you stop improving, the knowledge

71
00:07:01,360 --> 00:07:05,400
is something to be noted and discarded really.

72
00:07:05,400 --> 00:07:10,280
I mean, if it's real knowledge, it won't, it won't, it will stay with you, it won't

73
00:07:10,280 --> 00:07:13,280
just disappear.

74
00:07:13,280 --> 00:07:26,520
A lot of long questions tonight, so I'm trying to summarize them.

75
00:07:26,520 --> 00:07:44,200
That's one's sexual, I'm not really sure what the question is.

76
00:07:44,200 --> 00:07:47,680
How is one to manage the intoxication of fever?

77
00:07:47,680 --> 00:07:51,960
Right, and the only way I know to deal with it is intense mindfulness, form a meditation

78
00:07:51,960 --> 00:07:57,720
and weakened to fountains, reminding myself of how foreign, strange, and gross the

79
00:07:57,720 --> 00:07:58,720
body.

80
00:07:58,720 --> 00:08:04,320
But my practice is impermanent, imperfect, so it ends up returning.

81
00:08:04,320 --> 00:08:08,760
So, really, and there's another question or another couple of questions about this sort

82
00:08:08,760 --> 00:08:09,760
of topic.

83
00:08:09,760 --> 00:08:16,560
And we have to separate the sort of the stages of the path or the stages of defilements.

84
00:08:16,560 --> 00:08:21,440
The worst defilements are things like, well, are things that lead you to kill and to steal

85
00:08:21,440 --> 00:08:27,760
and to lie and to cheat and to, well, those four really.

86
00:08:27,760 --> 00:08:34,640
If you can stay away from those, if you can overcome the will to do those, you really

87
00:08:34,640 --> 00:08:39,000
cross the first obstacle.

88
00:08:39,000 --> 00:08:44,960
If you want to get deeper and overcome sexual desire entirely, it's a much more drawn-out

89
00:08:44,960 --> 00:08:51,120
process, but it's generally the process you embark upon after you've set yourself straight

90
00:08:51,120 --> 00:08:54,080
in terms of things that are really evil.

91
00:08:54,080 --> 00:09:00,280
So what I've told people in this video that I actually got quite a lot of views was the

92
00:09:00,280 --> 00:09:05,920
first thing I said was that you should stop feeling guilty, so guilty about sexual desire.

93
00:09:05,920 --> 00:09:13,240
I mean, it's not our first, it's not the first obstacle.

94
00:09:13,240 --> 00:09:18,040
Now you read the Buddha telling, talking a lot about it because the Buddha is talking

95
00:09:18,040 --> 00:09:24,040
endgame and he's talking often to monks, monks who have, they've gotten that far and they're

96
00:09:24,040 --> 00:09:29,880
trying to go further, often these are, these would be people who are on the brink of it.

97
00:09:29,880 --> 00:09:33,560
You know, he would be talking to Anangamis and some of these talks.

98
00:09:33,560 --> 00:09:37,440
Some of them do be talking to Arans and it was a means of edifying them and broadening

99
00:09:37,440 --> 00:09:38,440
their knowledge.

100
00:09:38,440 --> 00:09:42,960
It's not to say that part of the path isn't overcoming sexual desire or sexual desire.

101
00:09:42,960 --> 00:09:48,080
I mean, it's one more thing that we see is actually not satisfying, it's not leading to

102
00:09:48,080 --> 00:09:50,080
true happiness.

103
00:09:50,080 --> 00:09:55,880
But it's not where you should start and say, oh, me or my, here I'm starting to meditate

104
00:09:55,880 --> 00:09:57,680
but I have all these lustful thoughts.

105
00:09:57,680 --> 00:10:03,160
Well, just be mindful of them and don't beat yourself up over them.

106
00:10:03,160 --> 00:10:08,280
Learn about them as part of who you are as part of your mental makeup because all of

107
00:10:08,280 --> 00:10:13,600
that is going to get you over the first obstacle, which is wrong view.

108
00:10:13,600 --> 00:10:19,240
The wrong view that things, yes, sexual, central desire is satisfying, that this is the

109
00:10:19,240 --> 00:10:24,240
way to find real happiness or that there are things that are stable and permanent or

110
00:10:24,240 --> 00:10:29,840
that I am in control, there is a self, there is me, there is mine.

111
00:10:29,840 --> 00:10:37,320
Because that really is what disappears and what you can really and truly eradicate relatively

112
00:10:37,320 --> 00:10:43,400
quickly if you're intensive in the practice and once you've gone there, well, then it's

113
00:10:43,400 --> 00:10:49,960
just a matter of working and working and training and refining your insight and the more

114
00:10:49,960 --> 00:10:55,280
you refine your insight, the more clearly you see things, the more these things like sexual

115
00:10:55,280 --> 00:11:05,080
and central desire disappear or what we can stage by stage.

116
00:11:05,080 --> 00:11:09,440
Everything in the universe is everything in the universe connected in one way.

117
00:11:09,440 --> 00:11:17,080
Is it true that we think our minds and positive and negative waves in the universe?

118
00:11:17,080 --> 00:11:23,240
Yes, I'd say to some extent the waves is a bit of a overstatement.

119
00:11:23,240 --> 00:11:26,680
It's not that it was in positive and negative waves.

120
00:11:26,680 --> 00:11:33,800
It's that our minds, our actions have consequences and those consequences are going to have

121
00:11:33,800 --> 00:11:37,040
other consequences and so on and so on.

122
00:11:37,040 --> 00:11:43,440
So there is that connection, there are many connections, everything in the universe, there

123
00:11:43,440 --> 00:11:49,320
is gravity which is a physical connection, but the mind is similar, there is this connection

124
00:11:49,320 --> 00:11:57,040
between things, that's very complicated and I wouldn't talk about negative and positive

125
00:11:57,040 --> 00:12:03,160
waves, or maybe it's okay to talk about it as waves, but all that means is consequences

126
00:12:03,160 --> 00:12:06,480
and consequences and that's kind of like a wave, right?

127
00:12:06,480 --> 00:12:11,200
I just don't want you to think of like vibes, like if I sit and say love to someone, it's

128
00:12:11,200 --> 00:12:16,840
going to go out as vibes and I'm not of that school of thought, but there are consequences

129
00:12:16,840 --> 00:12:29,560
to your love and that's going to have ripple effects, which is a wave, so yeah, okay.

130
00:12:29,560 --> 00:12:33,920
When meditating and lust arises, is it acceptable to quickly go through the 32 parts of

131
00:12:33,920 --> 00:12:38,840
the body to straighten the mind out, or should you just note them, stay within sensation

132
00:12:38,840 --> 00:12:41,800
of lust as your object.

133
00:12:41,800 --> 00:12:47,200
So use the 32 parts of the body as you like, I would recommend, and you don't have to follow

134
00:12:47,200 --> 00:12:53,320
my recommendation, your situation is yours, but I would recommend to use it when it's extreme,

135
00:12:53,320 --> 00:12:57,280
when you feel like you just can't be mindful anymore, then sit down and have yourself

136
00:12:57,280 --> 00:13:01,120
a good talking to them, go through the 32 parts, even read through the Wizuri manga, if

137
00:13:01,120 --> 00:13:06,240
you like, because it goes into great detail and sort of remind yourself, and look, this

138
00:13:06,240 --> 00:13:13,280
is foolishness, get yourself some confidence and then go back and meditate mindfully again.

139
00:13:13,280 --> 00:13:15,840
That would be my recommendation.

140
00:13:15,840 --> 00:13:23,400
And this is another masturbation one.

141
00:13:23,400 --> 00:13:27,240
Mindfully observe yourself when you have an urge, just a sexual urge.

142
00:13:27,240 --> 00:13:31,520
When trying to suppress, instead of understand, so my question is, why is that the Buddha?

143
00:13:31,520 --> 00:13:36,080
So this I was trying to answer this, why is the Buddha insistent that serious practitioners

144
00:13:36,080 --> 00:13:37,080
become monks?

145
00:13:37,080 --> 00:13:42,160
So when you, as a monk, you're not about suppressing your sexual desires.

146
00:13:42,160 --> 00:13:46,760
It is about learning about them, but it's also about secluding yourself.

147
00:13:46,760 --> 00:13:53,000
Because surrounded by temptation, it's not the best place to learn.

148
00:13:53,000 --> 00:13:59,160
It's not the best place to objectively observe these states.

149
00:13:59,160 --> 00:14:05,720
It's like an alcoholic, well, they want to come to terms with their alcoholism and accept

150
00:14:05,720 --> 00:14:12,880
it, but you don't accept it by going to a bar and drinking every night.

151
00:14:12,880 --> 00:14:19,320
It's central desire weakens the mind, it weakens your clarity, it muddles the mind, and

152
00:14:19,320 --> 00:14:23,640
it reinforces the urge, it reinforces the addictions.

153
00:14:23,640 --> 00:14:28,560
So when you come even just to a meditation center, you find how peaceful it is and how

154
00:14:28,560 --> 00:14:36,120
much it's the same idea, a meditator here, who's gone through the course, I think, would

155
00:14:36,120 --> 00:14:41,240
very much likely tell you they didn't repress anything or much at all.

156
00:14:41,240 --> 00:14:45,880
Or there was a lot, maybe that they couldn't handle, but there was much that they brought

157
00:14:45,880 --> 00:14:46,880
up.

158
00:14:46,880 --> 00:14:54,560
Mostly about dealing with and coming to terms with all of these things.

159
00:14:54,560 --> 00:14:58,880
Now, of course, you don't come to a meditation center for one course and cut it all off,

160
00:14:58,880 --> 00:15:04,360
but you do come to terms with some of the most gross desires and so on.

161
00:15:04,360 --> 00:15:12,560
So being among is just extending that and trying to progress over time.

162
00:15:12,560 --> 00:15:18,320
It's really your question, the answer to your question is about the intensity that you

163
00:15:18,320 --> 00:15:27,760
can intensify your practice and there's intensity and there's something can be intensive,

164
00:15:27,760 --> 00:15:33,080
meaning always, doesn't have to be intense, but there's both, there's the intensity and

165
00:15:33,080 --> 00:15:43,200
the intensification, is that a different word?

166
00:15:43,200 --> 00:15:44,200
No.

167
00:15:44,200 --> 00:15:56,200
So just living as a monk is useful because you're constantly reminded of good things.

168
00:15:56,200 --> 00:16:02,200
But the point is there's no better way to come to terms with all these things than to

169
00:16:02,200 --> 00:16:12,880
become a monk and deal with your own mind rather than get caught up in sexuality and so on.

170
00:16:12,880 --> 00:16:16,480
Could you explain the difference between the Buddha and our aunts and the polyken and

171
00:16:16,480 --> 00:16:22,480
is the Bodhisattva path more noble than going straight to Nimalana in the Sutras?

172
00:16:22,480 --> 00:16:29,400
So you want to pin me down, you don't just want my take on things, you want me to appeal

173
00:16:29,400 --> 00:16:38,680
to the Sutras, well I'm not a scholar, so I shirk my duty to be completely knowledgeable

174
00:16:38,680 --> 00:16:44,040
and remember the Sutras says this, that Sutras says that because that would take me some

175
00:16:44,040 --> 00:16:54,760
effort and I'm not going to do that, but I will sit here and think for you, I won't just

176
00:16:54,760 --> 00:17:01,920
answer, I will sit and think. We do have some places where the Buddha calls himself

177
00:17:01,920 --> 00:17:09,760
and our aunts. We have one place where the Buddha says the only difference is that he

178
00:17:09,760 --> 00:17:17,600
became enlightened first. And what that means from a teravada point of view, which often

179
00:17:17,600 --> 00:17:26,440
refers to commentaries, not just the Sutras, is that the most important parts of enlightenment

180
00:17:26,440 --> 00:17:32,320
are shared by a Bodhan and that is freedom from suffering. So freedom from suffering is

181
00:17:32,320 --> 00:17:37,600
the goal. The Buddha said, I teach suffering, the cause of suffering, the cessation of suffering

182
00:17:37,600 --> 00:17:47,520
and the path to the cessation of suffering, that makes it pretty simple. So if you, that being

183
00:17:47,520 --> 00:17:53,200
said, the leeway exists in whether you want to follow the Buddha's path to freedom from

184
00:17:53,200 --> 00:18:00,200
suffering. Some people say, okay, you teach those four things, I'm going to go on and teach

185
00:18:00,200 --> 00:18:05,480
those four things in the future because in the future there will be people who want to

186
00:18:05,480 --> 00:18:14,960
become free from suffering as well. And so I will undertake to be what you are in the future.

187
00:18:14,960 --> 00:18:20,080
But it would be ridiculous to say, hey, everyone should do that. Because if everyone

188
00:18:20,080 --> 00:18:25,480
did that, what you suited me? Everyone said, no, no, I'm not going to practice the teachings

189
00:18:25,480 --> 00:18:29,520
I'm going forward. Well, what good would a Buddha be then if they're the ones who teach

190
00:18:29,520 --> 00:18:34,560
everyone to become free from suffering, if no one became free from suffering? And the other

191
00:18:34,560 --> 00:18:42,720
thing, sorry, I'm getting off track, I'm not really talking from the Sutras at that point.

192
00:18:42,720 --> 00:19:01,520
But the other thing, and so I'll try and keep it to the Sutras, is that, you know,

193
00:19:01,520 --> 00:19:07,320
in the Sutras, there's very, very, very little said about becoming a Buddha. You know,

194
00:19:07,320 --> 00:19:12,280
it's talk, there's talk about how our Buddha became a Buddha. There's talk about other

195
00:19:12,280 --> 00:19:19,640
Buddhas. But there's very little talk, there certainly is never in the Theravada Sutras.

196
00:19:19,640 --> 00:19:27,040
Any advice by the Buddha where he says, hey, you should try and become a Buddha. There's

197
00:19:27,040 --> 00:19:35,760
nothing like that. I think there's one or two places where he points someone out as being

198
00:19:35,760 --> 00:19:39,880
a Bodhisattva. But I think it's usually in the commentaries. I can't even think about

199
00:19:39,880 --> 00:19:46,120
a place in the actual Sutras. But of course, I'm not a scholar. I get mixed up from

200
00:19:46,120 --> 00:19:51,160
me what's in the commentaries and what's in the Sutras, in regards to things that

201
00:19:51,160 --> 00:19:57,040
happened. We have stories, but how many of those stories are actually in the Sutras and

202
00:19:57,040 --> 00:20:06,080
how many of them are in the commentaries? Most are in the commentaries.

203
00:20:06,080 --> 00:20:17,200
But there's, I think, what I was going to say, and it's not explicitly stated in the Sutras,

204
00:20:17,200 --> 00:20:23,280
but there's a sense that you do get certainly from the Sutras that becoming a Buddha.

205
00:20:23,280 --> 00:20:30,840
It's a very, very difficult thing. It's not a thing for most people to aspire to even

206
00:20:30,840 --> 00:20:36,680
in some future existence. And I think for the very reason that one Buddha helps so many

207
00:20:36,680 --> 00:20:46,520
people. So the ratio there of among the ratio among spiritually advanced people on the

208
00:20:46,520 --> 00:20:53,400
path to become enlightened, because the beings who are not even close to the path to become

209
00:20:53,400 --> 00:21:03,040
free from suffering are far, far, far more. But among those small, small group of people,

210
00:21:03,040 --> 00:21:08,600
tiny group of beings who are keen to become enlightened and who have the capacity to

211
00:21:08,600 --> 00:21:19,520
become enlightened. Only one is a Buddha. So it's not recommended for most people. It's

212
00:21:19,520 --> 00:21:24,040
something that only a very, very special being. And you're talking about how only a special

213
00:21:24,040 --> 00:21:30,600
being should ever think to become a Buddha. And then most, well, you get a lot of people

214
00:21:30,600 --> 00:21:35,840
thinking, I'm a special being. And that's cool. That's great. I mean, it's good. I hope

215
00:21:35,840 --> 00:21:41,640
you are, but often skeptical, because I think there are far more people who think they're

216
00:21:41,640 --> 00:21:48,120
a special being than there are people who actually are that special being. There's a in

217
00:21:48,120 --> 00:21:53,000
the commentary, and I brought this up before in the commentary on the commentary on the

218
00:21:53,000 --> 00:22:00,480
commentary, which talks about how our Buddha got the confirmation that he was going to

219
00:22:00,480 --> 00:22:10,120
become a Buddha. Or some commentary says, but wait, weren't there other people that saw

220
00:22:10,120 --> 00:22:16,120
the Buddha who thought, I want to become a Bodhisattva? I want to become a Buddha? The answer

221
00:22:16,120 --> 00:22:21,480
is, yes, there were many. And I think that the commentary actually claims that there wasn't

222
00:22:21,480 --> 00:22:28,360
one person in the crowd who didn't think, maybe I should become a Buddha. But none of

223
00:22:28,360 --> 00:22:36,240
them had the potential. None of them got the confirmation, like this one, Sumeda did. But

224
00:22:36,240 --> 00:22:41,200
things in perspective, I guess, is my advice. Because if you're going to become a Buddha,

225
00:22:41,200 --> 00:22:46,000
that's the most awesome thing you can ever possibly do. Most people will never make it.

226
00:22:46,000 --> 00:22:51,920
That's my opinion. And it's backed by, it's not explicitly stated in the suit, does it

227
00:22:51,920 --> 00:23:01,160
certainly backed by them and backed by the commentary? So I hope that was useful.

228
00:23:01,160 --> 00:23:09,680
At least thought provoking maybe, I hope it wasn't, didn't provoke too much thought. I can

229
00:23:09,680 --> 00:23:16,680
imagine what the comments are on YouTube. I won't even look. Do you have any advice for a

230
00:23:16,680 --> 00:23:21,480
two week long intensive meditation practice with the aim of replacing sleep with meditation

231
00:23:21,480 --> 00:23:35,320
for that period? Oh boy. What do you think, Ricardo? Two weeks without sleep? Two week

232
00:23:35,320 --> 00:23:40,800
long intensive practice with the aim of replacing sleep. Some people do that. There's

233
00:23:40,800 --> 00:23:48,240
stories of people who do it for months. You need to be in a very refined state to be able

234
00:23:48,240 --> 00:23:54,760
to do that. So my advice, get into a very refined state before you even think to attempt

235
00:23:54,760 --> 00:24:00,320
such a thing. Why? Because if you're not in a very, maybe you are, maybe you're a very

236
00:24:00,320 --> 00:24:07,520
refined sort of individual. But if you're not, it's going to be hell and it could be even

237
00:24:07,520 --> 00:24:14,160
deleterious to your mental well-being because you'll develop bad habits. And you need

238
00:24:14,160 --> 00:24:22,720
to be in a fairly stable place to be able to benefit from that, in my opinion. So that's

239
00:24:22,720 --> 00:24:27,760
my advice. I wouldn't give more advice because I don't know whether you are at that state

240
00:24:27,760 --> 00:24:32,960
yet. And I wouldn't want to assume that you are even if you told me you are because you're

241
00:24:32,960 --> 00:24:40,160
not under my tutelage. So that's all I can say about that. Being mindful while using

242
00:24:40,160 --> 00:24:48,040
the computer tablets smartphone. Try not to use it while you're still training. And once

243
00:24:48,040 --> 00:24:51,720
you've already trained, well, then it should just be clear to you how you should have

244
00:24:51,720 --> 00:24:59,160
to do it because you've developed the skills. As with anything that's inherently absorbing

245
00:24:59,160 --> 00:25:07,000
of, you know, pulling the mind, you have to step back from it. You have to engage with

246
00:25:07,000 --> 00:25:13,240
it and not be mindful. But then be mindful when you have a chance, just step back and

247
00:25:13,240 --> 00:25:18,720
see how you're feeling, how is your neck, how is your posture, how are your hands and

248
00:25:18,720 --> 00:25:29,680
fingers and cramping and so on. Here's the question of the week. This one's got to win

249
00:25:29,680 --> 00:25:36,560
some sort of award prize. I'm not mocking it. I really think so. This person has read

250
00:25:36,560 --> 00:25:42,280
something I wrote nine years ago. And so my first thought is, oh my, you're going to hold

251
00:25:42,280 --> 00:25:56,560
me to something I said nine years ago. But I looked at it and it's my short explanation

252
00:25:56,560 --> 00:26:07,240
on my take on things because that's the thing with religion. It develops stories. We tend

253
00:26:07,240 --> 00:26:12,880
to romance things and everyone gets a story. All of our teachers have biographies that

254
00:26:12,880 --> 00:26:19,880
are often romanticized. We have lineages and we like to tell our lineages about our teachers

255
00:26:19,880 --> 00:26:27,960
and their teachers and things they did. We have stories that we tell. But we often lose

256
00:26:27,960 --> 00:26:34,240
or we know often we sometimes lose the truth in doing so. So I tried to set some of the

257
00:26:34,240 --> 00:26:41,080
little things straight and I may have also had my own agenda in writing that. I mean,

258
00:26:41,080 --> 00:26:50,440
at least in so far as I don't approve of everything that goes on at any meditation center.

259
00:26:50,440 --> 00:26:54,560
I mean, I don't even suppose I'd approve of everything that we do. So I don't think we're

260
00:26:54,560 --> 00:27:06,440
perfect by any means. But I try and I guess I've been discouraged from getting involved

261
00:27:06,440 --> 00:27:15,320
in various meditation centers because of the problems that I've seen. And so I mean,

262
00:27:15,320 --> 00:27:19,400
then the question goes, well, aren't I just going to have problems of my own? And that's

263
00:27:19,400 --> 00:27:35,280
quite possible. I think something that I haven't… Well, if you read that article, it

264
00:27:35,280 --> 00:27:42,040
was interesting to me to write it because I really strongly approve of the way Adjantong

265
00:27:42,040 --> 00:27:50,240
teaches. Having seen at least a couple of other systems do a similar meditation technique.

266
00:27:50,240 --> 00:27:54,880
I really approve of the way Adjantong does it. I mean, that's why I've stuck with it

267
00:27:54,880 --> 00:28:03,960
for so long. And so that's really the only the biggest criticism. And it's not a criticism.

268
00:28:03,960 --> 00:28:17,060
It's just a reason why I'm not fond or I'm not keen to encourage going to various

269
00:28:17,060 --> 00:28:24,080
centers, including the ones I mentioned in that list. So my agenda is to point out the

270
00:28:24,080 --> 00:28:33,200
differences. Was it the time? And this is nine years ago. And I think part of that is

271
00:28:33,200 --> 00:28:44,760
because of how important to me, Adjantong's teaching, way of teaching is and how I felt

272
00:28:44,760 --> 00:28:50,920
that some people… There was… There's other ways of teaching similarly and taking from

273
00:28:50,920 --> 00:28:57,000
his teachings but adapting it. And I've seen one sort of adaptation that's become quite

274
00:28:57,000 --> 00:29:03,680
strong and that's one of them I talked about. And so those people are adhering it seems fairly

275
00:29:03,680 --> 00:29:13,120
strongly to that style. And it's similar. It's very similar. It's quite similar. It's

276
00:29:13,120 --> 00:29:19,600
almost the same but I don't like to go that far. I feel there's a distinction. And I

277
00:29:19,600 --> 00:29:23,720
wanted to make that distinction. So people wouldn't say this is Adjantong's teaching.

278
00:29:23,720 --> 00:29:34,640
I've for many for a few years. I practiced with a group of these people and had a challenge

279
00:29:34,640 --> 00:29:41,760
it was challenging trying to understand how it was that they were teaching. And or put the

280
00:29:41,760 --> 00:29:46,960
thing, put the practice together in a system. And when I went to sit with Adjantong and

281
00:29:46,960 --> 00:29:53,840
learned his way, it just fell into place and everything was one, two, three, four. Simple

282
00:29:53,840 --> 00:29:59,280
and perfect. And then he'd bring up the Buddha's teaching this quote that quote. He brought

283
00:29:59,280 --> 00:30:06,000
everything together for me. It was magical and almost. I'm not saying he's unique. I just

284
00:30:06,000 --> 00:30:15,360
think for me it was it was it was eye opening. And so without going into detail about what

285
00:30:15,360 --> 00:30:22,400
you're asking because that's talking about individuals, which is never a great thing to do.

286
00:30:22,400 --> 00:30:28,880
I mean it puts me in a position where I don't think I should talk about places and people

287
00:30:28,880 --> 00:30:38,000
and so on. But I do think I mean I stick I try. So what I was trying to say how I

288
00:30:38,000 --> 00:30:44,640
counter the criticism aren't I corrupting things is that I learned I stuck to an Adjantong

289
00:30:44,640 --> 00:30:50,080
teachers there are other ways that I've learned is that no I'm going to take it as best I can

290
00:30:50,080 --> 00:30:55,280
the way Adjantong teaches now when it's in elsewhere is someone adapted it and everyone else is

291
00:30:55,280 --> 00:31:01,920
following that. Small have that adaptations not not great adaptations but some of the things

292
00:31:01,920 --> 00:31:09,360
I don't agree with. And then there are other people who I do see are teaching some fairly strange

293
00:31:09,360 --> 00:31:16,800
things. That being said every teacher is going to change things a little bit. The way I teach,

294
00:31:16,800 --> 00:31:22,240
the way I speak is nothing like the way Adjantong speaks or teaches in these situations.

295
00:31:23,200 --> 00:31:27,920
I'm teaching to a Western audience. I think Adjantong would take a look at all these questions

296
00:31:27,920 --> 00:31:34,240
and just closes eyes and fall asleep. I mean you just have no interest in this. I mean he's very

297
00:31:34,240 --> 00:31:44,480
tired. He was when I was with him because he worked so hard and he was getting old but he also

298
00:31:44,480 --> 00:31:52,240
didn't have time for speculation. So I've tried my best to follow that and not encourage speculation

299
00:31:52,240 --> 00:31:59,120
but I certainly put my own spin on things but in terms of the details I stick to an Adjantong

300
00:31:59,120 --> 00:32:05,840
teaches. I don't think that's entirely true. I mean there's a couple of details that I disagree

301
00:32:05,840 --> 00:32:11,680
with and there's other groups that just seem to be fairly loose with the way they run things.

302
00:32:12,480 --> 00:32:16,400
And some just bizarre things I've heard but a lot of it's hearsay so.

303
00:32:18,880 --> 00:32:25,360
Mostly that's the thing is I'm not in touch with all these groups anymore so I don't have

304
00:32:25,360 --> 00:32:30,640
great things. I don't have deep information about, I don't have deep information about,

305
00:32:30,640 --> 00:32:34,720
I don't have a lot to say about these places. I just don't know what's going on there.

306
00:32:40,800 --> 00:32:46,080
Can you say when the next six months service period starts and if there is a way to apply,

307
00:32:46,080 --> 00:32:48,960
I'm interested in serving after I come to meditate next month.

308
00:32:48,960 --> 00:32:55,200
Jayvin, when are you leaving? Are you leaving? Are you staying?

309
00:32:58,960 --> 00:33:04,000
You're leaving in September. So we have a steward until September

310
00:33:05,360 --> 00:33:12,880
at least probably unless he or I dies before then which could very well happen.

311
00:33:12,880 --> 00:33:21,120
If you're I dies we don't have a steward and other than that we're probably okay.

312
00:33:26,080 --> 00:33:30,960
One day once you said the position in me passing the meditation was not so important.

313
00:33:30,960 --> 00:33:36,160
Does it mean there's not a problem to put ones back against the wall? Yes that's what it means.

314
00:33:36,160 --> 00:33:45,440
So why you wouldn't if you if you can you're better off not for the simple reason that it'll

315
00:33:45,440 --> 00:33:53,280
keep you more awake but that is that a huge reason? Is that a great concern? I don't think so.

316
00:33:54,880 --> 00:34:00,000
I mean you'll just have to adapt if you're sitting against a cushion you're much more likely to

317
00:34:00,000 --> 00:34:08,880
you're generally more likely to fall asleep or to drift. So you're much more alert. You're somewhat

318
00:34:08,880 --> 00:34:15,120
more alert when you're sitting upright. That's the only distinction. There are many cases where

319
00:34:15,120 --> 00:34:21,760
people feel compelled through back problems or just intense pain to sit with their back against

320
00:34:21,760 --> 00:34:29,280
something. I'd like to I think more people do it than need it from what I've seen so I would

321
00:34:29,280 --> 00:34:37,360
encourage you to try not to not you particularly but in general people try to sit cross-laying in

322
00:34:37,360 --> 00:34:43,440
without leaning against anything but certainly it's not against the rules to lean against anything.

323
00:34:48,320 --> 00:34:54,320
So there we go that's all the questions that I have. The pain is not working at all but I got

324
00:34:54,320 --> 00:35:05,760
through all the questions so we're good. Are we still alive? How's YouTube doing? Oh yes chatting up a

325
00:35:05,760 --> 00:35:15,360
storm. You chat too much YouTube. Look at you should take notes from all my meditators here sitting

326
00:35:15,360 --> 00:35:25,840
quietly, only answering when speaking when spoken to. I don't mind. Do as you like. That's all for

327
00:35:25,840 --> 00:35:55,680
me. I'm out. Have a good night. Thank you all for tuning in.

