1
00:00:00,000 --> 00:00:10,000
Hello everyone. This video is an announcement to let it be known that I have now officially

2
00:00:10,000 --> 00:00:21,000
reached 200 uploaded videos to YouTube, which seems like quite a lot to me. And so to

3
00:00:21,000 --> 00:00:29,000
commemorate this landmark, I thought I would do something special and that is to put a

4
00:00:29,000 --> 00:00:35,000
request out to all of you to show me what you've gained from the the teachings

5
00:00:35,000 --> 00:00:41,000
from the meditation practice. Specifically, what I'd like to ask is that if you don't

6
00:00:41,000 --> 00:00:47,000
have a YouTube account, make one if you have one already, upload a video of yourself

7
00:00:47,000 --> 00:00:53,000
meditating, either practicing mindful frustration, walking meditation, or sitting

8
00:00:53,000 --> 00:01:00,000
meditation. One of these three, according to the technique that I teach, and post

9
00:01:00,000 --> 00:01:06,000
it as a video response to this video that you're watching right now. Once you do

10
00:01:06,000 --> 00:01:10,000
that, and once you get them enough of them, of all three types, you know, you could

11
00:01:10,000 --> 00:01:15,000
upload one video of you doing all three of them, or three videos of you doing each one

12
00:01:15,000 --> 00:01:21,000
of them. It only has to be a short video if you just as an example. You know, you don't

13
00:01:21,000 --> 00:01:26,000
have to actually meditate for 10 minutes or 20 minutes or so, and as a YouTube video,

14
00:01:26,000 --> 00:01:30,000
just give me an example of how you would meditate. It could be showing how you do the

15
00:01:30,000 --> 00:01:38,000
frustration, showing how you do the walking, showing yourself doing the sitting, and post

16
00:01:38,000 --> 00:01:44,000
it or post all three of them if it's three different videos. And I'll download all of

17
00:01:44,000 --> 00:01:55,000
these and make a montage out of them, and then repost it as a look at how people put these

18
00:01:55,000 --> 00:02:01,000
teachings into practice in all parts of the world, from all walks of life, in all different

19
00:02:01,000 --> 00:02:07,000
situations. So find a unique place, the place where you normally meditate, it could be in your

20
00:02:07,000 --> 00:02:16,000
room, it could be in the forest, in a park, in your office where you work. If you go to school,

21
00:02:16,000 --> 00:02:23,000
it could be in the school library, or it could be in an airport, or wherever you have

22
00:02:23,000 --> 00:02:27,000
been able to put the teachings into practice. It doesn't have to be the most serene and calm

23
00:02:27,000 --> 00:02:34,000
environment, just a place where you've been putting the teachings into practice in one

24
00:02:34,000 --> 00:02:44,000
of these three formal modes, the frustrations, the walking, or the sitting. Okay, so looking

25
00:02:44,000 --> 00:02:52,000
forward to it, and hope to see your responses come in. And just to say thanks to everyone

26
00:02:52,000 --> 00:02:58,000
for helping me to get to this, to get this far. I mean, it's only because of the enthusiasm

27
00:02:58,000 --> 00:03:03,000
and encouragement, and the benefits that people say they're getting out of these teachings

28
00:03:03,000 --> 00:03:14,000
that I keep doing what I'm doing. So keep it up, and keep giving your comments and questions

29
00:03:14,000 --> 00:03:23,000
and subscribing to this channel, and most importantly keep meditating. Okay, hope to hear

30
00:03:23,000 --> 00:03:41,000
from you, and all the best, everyone.

