1
00:00:00,000 --> 00:00:07,360
Okay, next question comes from minor players.

2
00:00:07,360 --> 00:00:10,560
Can you help me better understand karma in reincarnation?

3
00:00:10,560 --> 00:00:14,400
What helped you when you first began your studies?

4
00:00:14,400 --> 00:00:20,240
I know you have mentioned meditation as a tool, but so far no insights for me at least.

5
00:00:20,240 --> 00:00:24,040
Perhaps I suggested reading authored by a Westerner.

6
00:00:24,040 --> 00:00:32,440
No, meditate, meditate, meditate, meditate.

7
00:00:32,440 --> 00:00:37,000
Meditation does help you to understand both karma and rebirth.

8
00:00:37,000 --> 00:00:43,000
But I think the problem in most cases is a misunderstanding of karma and a misunderstanding

9
00:00:43,000 --> 00:00:44,200
of rebirth.

10
00:00:44,200 --> 00:00:51,840
We think of it like we think of everything as some entity that clearly says karma, cause,

11
00:00:51,840 --> 00:00:59,120
respect, and we think of rebirth in terms of this life, past life.

12
00:00:59,120 --> 00:01:07,760
How meditation helps you to understand karma is because everything we do is karma.

13
00:01:07,760 --> 00:01:10,600
Every moment of our existence is karma.

14
00:01:10,600 --> 00:01:12,000
It's cause and effect.

15
00:01:12,000 --> 00:01:14,840
We want to walk, therefore we walk.

16
00:01:14,840 --> 00:01:16,920
When you're doing walking meditation, you can see this.

17
00:01:16,920 --> 00:01:20,680
You can see that first there comes the desire to move the foot, then there comes the

18
00:01:20,680 --> 00:01:25,240
moving of the foot.

19
00:01:25,240 --> 00:01:30,080
Even even just a few days of meditating, you should be able to see that when you give

20
00:01:30,080 --> 00:01:35,720
rise to something, karmics say anger or greed, then there's an immediate result.

21
00:01:35,720 --> 00:01:42,200
This is where all of our feelings of guilt and you can say stress and worry and suffering

22
00:01:42,200 --> 00:01:43,800
and addiction come from.

23
00:01:43,800 --> 00:01:46,480
All of these things are caused by karma.

24
00:01:46,480 --> 00:01:53,040
The fact that we don't get what we want is caused by our wanting, that's karma.

25
00:01:53,040 --> 00:01:58,760
The idea of, you know, I kill you, then you come back and kill me is really just an extrapolation

26
00:01:58,760 --> 00:02:06,880
of that, and it's not anything special or mysterious.

27
00:02:06,880 --> 00:02:11,920
It's simply the ripples that we send out into the universe changing the nature of our

28
00:02:11,920 --> 00:02:14,840
mind and the universe around us.

29
00:02:14,840 --> 00:02:24,400
How rebirth works is, as I've said in one of my reality videos, it's not that Buddhists

30
00:02:24,400 --> 00:02:28,720
believe in rebirth, we just don't believe in death, and that's because there's no reason

31
00:02:28,720 --> 00:02:36,040
to believe in death except for someone who has no clinging, but that's another issue.

32
00:02:36,040 --> 00:02:45,160
For most of us, this is, rebirth is a continuation of birth, of existence, of the arising

33
00:02:45,160 --> 00:02:47,560
of phenomena.

34
00:02:47,560 --> 00:02:54,800
Things arise because of the cause when we get angry a lot, we create the world around

35
00:02:54,800 --> 00:03:04,120
us, when we want a lot, we build things, we get involved in projects and work and we open

36
00:03:04,120 --> 00:03:14,120
a store, we start a business, we study things, we develop ourselves, we create existence,

37
00:03:14,120 --> 00:03:19,120
and we change the universe around us, and death is sort of like a wave and a ripple in

38
00:03:19,120 --> 00:03:25,000
that, when there's the building up in this life, then the crashing down, and then the building

39
00:03:25,000 --> 00:03:30,920
up and the crashing down, but it's a continuation, and our mind continues from one moment

40
00:03:30,920 --> 00:03:32,840
to the next.

41
00:03:32,840 --> 00:03:37,800
It's not a belief in anything, neither karma nor rebirth is a belief in something, it's

42
00:03:37,800 --> 00:03:49,680
a giving up of all of our ideas and beliefs of the termination of life and of luck and

43
00:03:49,680 --> 00:03:54,840
of chance and of magic and so on, for a very simple understanding of reality that things

44
00:03:54,840 --> 00:04:00,440
arise in sequence, and when this arises, that arises with the arising of this, there's

45
00:04:00,440 --> 00:04:05,240
a rising of that, when this ceases, that ceases with the non-arising of this, there's

46
00:04:05,240 --> 00:04:12,840
the non-arising of that, you can't have suffering without defilement, if the mind is free

47
00:04:12,840 --> 00:04:22,760
from and defilement and their rise is no suffering, their rise is no result, you can't

48
00:04:22,760 --> 00:04:29,360
not get what you want when you have no wanting, for example, so karma is the giving rise

49
00:04:29,360 --> 00:04:33,680
to the wanting and the result that comes from it.

50
00:04:33,680 --> 00:04:38,320
And I think probably you're already seeing that in meditation if you are meditating, you may

51
00:04:38,320 --> 00:04:44,440
just be looking for some insight based on a concept or an idea of karma and rebirth,

52
00:04:44,440 --> 00:04:51,440
which I would do away with, I would say you can ask yourself, do you think doing bad deeds

53
00:04:51,440 --> 00:04:52,960
has a good result?

54
00:04:52,960 --> 00:04:57,880
If the answer is yes, then okay, you're still lacking in meditation experience, the experience

55
00:04:57,880 --> 00:05:03,600
of reality, you ask yourself, do you think a good deed has a bad result?

56
00:05:03,600 --> 00:05:09,680
If the answer is still yes, then you're still lacking in meditation practice, and I would

57
00:05:09,680 --> 00:05:16,080
say continue in the meditation practice until you can see that actually good deeds meaning

58
00:05:16,080 --> 00:05:23,520
good states of mind lead to good actions, good actions lead to good results, lead to happiness,

59
00:05:23,520 --> 00:05:26,560
peace, harmony, and freedom from suffering.

60
00:05:26,560 --> 00:05:32,560
And deeds on the other hand, bad mind states of greed, of anger, of delusion, conceit, arrogance

61
00:05:32,560 --> 00:05:41,520
and so on, lead to bad deeds which lead in turn to bad results, suffering, conflict,

62
00:05:41,520 --> 00:05:48,720
friction, entanglement, and further and further becoming.

63
00:05:48,720 --> 00:05:55,560
So I hope that kind of answered your question, I would say don't worry so much about theory

64
00:05:55,560 --> 00:06:01,560
and certainly don't read books about the subject, I would practice meditation.

65
00:06:01,560 --> 00:06:05,920
Okay, okay, maybe I'll give you a couple of links in the answer, check out the answer

66
00:06:05,920 --> 00:06:11,400
to your question, maybe the description to this video as well and I'll try to give a couple

67
00:06:11,400 --> 00:06:18,880
of links just to give an overview of karma anyway, rebirth I think is unless you're

68
00:06:18,880 --> 00:06:25,200
going to practice meditations that allow you to remember things that far in the past,

69
00:06:25,200 --> 00:06:31,280
I would say, just understand that we don't believe in death, that's the understanding

70
00:06:31,280 --> 00:06:32,280
of rebirth.

71
00:06:32,280 --> 00:06:37,880
There's no reason to think of death because our experience, especially meditation, is

72
00:06:37,880 --> 00:06:43,160
one of continued arising and that's all that happens at the moment of physical death.

73
00:06:43,160 --> 00:07:02,280
It continues, okay, there you go.

