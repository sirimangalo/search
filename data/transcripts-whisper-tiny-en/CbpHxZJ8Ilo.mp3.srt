1
00:00:00,000 --> 00:00:03,960
Now I will tell you the beginning of the race and thatention of

2
00:00:03,960 --> 00:00:07,280
the nation is in Kainidi and the governor of heaven.

3
00:00:07,620 --> 00:00:10,280
Peace be upon you.

4
00:00:10,280 --> 00:00:09,000
Say,akes, forsaken and

5
00:00:13,160 --> 00:00:14,500
Boko Haram forces

6
00:00:14,500 --> 00:00:17,180
Black Arabs are there even

7
00:00:17,180 --> 00:00:17,960
with their

8
00:00:18,800 --> 00:00:05,280
la libah

9
00:00:19,360 --> 00:00:21,360
Balah

10
00:00:21,360 --> 00:00:22,360
lamuan

11
00:00:22,360 --> 00:00:22,320
drum

12
00:00:22,320 --> 00:00:23,900
May I have Sam Seagat and heKain,

13
00:00:23,900 --> 00:00:24,920
very knowledge and fortune.

14
00:00:24,920 --> 00:00:25,760
Same thing with him.

15
00:00:25,760 --> 00:00:28,300
Let me listen in 1st Amendment with him.

16
00:00:28,300 --> 00:00:28,700
He splpoints overrama,

17
00:00:28,700 --> 00:00:29,600
the

18
00:00:29,600 --> 00:00:39,120
F

19
00:00:36,240 --> 00:00:39,280
in the

20
00:00:39,280 --> 00:00:40,780
war.

21
00:00:40,780 --> 00:00:41,880
We live here

22
00:00:41,880 --> 00:00:41,820
in one

23
00:00:41,820 --> 00:00:44,220
war and

24
00:00:44,220 --> 00:00:45,620
we humans

25
00:00:45,620 --> 00:00:45,920
off time

26
00:00:45,920 --> 00:00:46,320
we

27
00:00:46,320 --> 00:00:43,420
if

28
00:00:43,420 --> 00:00:48,760
to

29
00:00:48,760 --> 00:00:49,480
survive

30
00:01:19,480 --> 00:01:31,960
Thank you, Venerable, Gustadama Teiro, for inviting me to give a talk on meditation today.

31
00:01:31,960 --> 00:01:36,840
And I'd like to appreciate all of those of you who have come here today for the purpose

32
00:01:36,840 --> 00:01:43,920
of paying Boudja to the Tripal Gem, the Boudja to Boudja to Boudja to Boudja to Boudja

33
00:01:43,920 --> 00:01:53,360
and the Boudja and Nang 8th among the Murtamam, paying respect or veneration, paying homage to those people or those beings,

34
00:01:53,360 --> 00:01:58,360
those objects that are worthy of veneration. This was a great blessing.

35
00:01:58,360 --> 00:02:13,320
We had to take advantage of the Boudja & Tripal Armato.

36
00:02:13,320 --> 00:02:17,160
With them were consideredcreen.

37
00:02:47,160 --> 00:03:06,160
So the question then is, here we've come today to Boudja, to pay veneration and homage to the Buddha, to the Dhamma, to the Sangha.

38
00:03:06,160 --> 00:03:18,160
What is the use, or what is the benefit, or what is the importance of meditation in regards to our paying veneration, paying homage to the Buddha, the Dhamma, and the Sangha.

39
00:03:18,160 --> 00:03:36,560
I have been former

40
00:03:36,560 --> 00:03:43,760
the master, the father of his wife, who we have to even watch

41
00:03:43,760 --> 00:03:45,160
the Dhamma, share with us.

42
00:03:45,160 --> 00:03:48,660
The Buddha said,

43
00:03:48,660 --> 00:03:51,860
break before he was about to pass away.

44
00:03:51,860 --> 00:03:59,660
At the time when the angels and the human beings and even the trees themselves were bringing forth flowers,

45
00:03:59,660 --> 00:04:07,660
and there was incense and all kinds of objects of veneration

46
00:04:07,660 --> 00:04:12,660
that people had brought to pay respect to the Buddha before he passed away.

47
00:04:12,660 --> 00:04:16,160
When the Buddha himself said to Ananda, he said that Ananda,

48
00:04:16,160 --> 00:04:21,160
this is not the proper way to pay respect to a perfectly enlightened Buddha.

49
00:04:21,160 --> 00:04:24,160
Even though this is something that is good, something that is beneficial,

50
00:04:24,160 --> 00:04:26,160
it is a true Buddha.

51
00:04:26,160 --> 00:04:33,160
It is not the highest way to pay respect to a fully enlightened Buddha.

52
00:04:33,160 --> 00:04:38,160
The Buddha said,

53
00:04:38,160 --> 00:04:50,160
I will not acknowledge that this means right now that I want to speak.

54
00:05:20,160 --> 00:05:34,100
the

55
00:05:34,100 --> 00:05:45,100
The Buddha said, a person, whether they be a monk, a nun, a layman or a laywoman,

56
00:05:45,100 --> 00:05:50,100
someone who is a follower of the Buddha, when they practice the dhamma, the teaching of the Buddha,

57
00:05:50,100 --> 00:05:54,100
for the realization of the highest of the teaching, the highest truth.

58
00:05:54,100 --> 00:06:01,100
This, the Buddha said, is the greatest sakarote, guru karate, guru karate,

59
00:06:01,100 --> 00:06:09,100
manatee, pujatee, paramaya, pujaya, the highest, the ultimate form of veneration for the Buddha.

60
00:06:09,100 --> 00:06:37,100
After this, dhanma is right for us.

61
00:06:37,100 --> 00:06:45,380
So, actually, meditation, the practice of Bhauana, the practice of mental development,

62
00:06:45,380 --> 00:06:51,140
is the greatest thing we can do on a boya day, on a day that is put aside for veneration

63
00:06:51,140 --> 00:06:56,620
of the Buddha, put aside for the development of holiness.

64
00:06:56,620 --> 00:07:03,820
It's not to venerate simply those objects of holiness, but to develop holiness inside

65
00:07:03,820 --> 00:07:09,580
of ourselves, because everything that we do, whether it's offering flowers or incense

66
00:07:09,580 --> 00:07:16,460
or candles to the Buddha, or whether it's chanting or anything we do in veneration,

67
00:07:16,460 --> 00:07:23,820
if our mind is not pure, if our mind is not full of holiness or with the intention to

68
00:07:23,820 --> 00:07:29,980
do a good deed, then the deed itself cannot be considered to be veneration, cannot be considered

69
00:07:29,980 --> 00:07:36,580
to be a wholesome Akusura, a beneficial act.

70
00:07:36,580 --> 00:07:47,500
Now, I am going to say, Bhauana Akhala, Bhauana Akhala, Bhauana Akhala, Bhauana Akhala,

71
00:07:47,500 --> 00:07:52,260
Bhauana Akhala, Bhagavad-gatma, Bhagavad-gatma, Purjani, Purjani, Herme.

72
00:07:52,260 --> 00:08:08,780
Bhauana Akhala, Prabhaana Akhala, Purjani, Prabhana Akhala, Prabhana Akhala, Purjani, Prabhana Akhala,

73
00:08:38,780 --> 00:09:05,660
They come from the mind, they have the mind as their root, so mental development is essential

74
00:09:05,660 --> 00:09:11,420
in the development of goodness, and for the practice of puja, for the practice of

75
00:09:11,420 --> 00:09:35,720
the importance of the

76
00:09:35,720 --> 00:09:58,760
So, what I'm going to do here now is to help you to see what is happening in your mind,

77
00:09:58,760 --> 00:10:03,640
to help you to understand the nature of your mind at this moment.

78
00:10:03,640 --> 00:10:10,640
I'm going the time when you come here to offer flowers, to offer incense candles, to listen to the Deschana,

79
00:10:10,640 --> 00:10:16,640
to develop, to cultivate goodness for yourselves.

80
00:10:16,640 --> 00:10:21,640
I'm going to help you through the practice of meditation, any practice of meditation,

81
00:10:21,640 --> 00:10:26,640
practice of any type of development or cultivation of good states.

82
00:10:26,640 --> 00:10:37,640
I will allow you to see and to change and to develop your mind in such a way that you actually bring about true goodness,

83
00:10:37,640 --> 00:10:45,640
true peace, happiness and freedom from suffering for yourself, and are able to pay respect to the Buddha in the highest form.

84
00:10:45,640 --> 00:10:55,640
In this case, I will ask you to speak in my mind.

85
00:10:55,640 --> 00:10:58,640
I will ask you to speak in my mind.

86
00:10:58,640 --> 00:11:06,640
I will ask you to speak in my mind.

87
00:11:06,640 --> 00:11:16,640
I will ask you to speak in my mind.

88
00:11:16,640 --> 00:11:25,640
I will ask you to speak in my mind.

89
00:11:25,640 --> 00:11:34,640
I will ask you to speak in my mind.

90
00:11:34,640 --> 00:11:39,640
I will ask you to speak in my mind.

91
00:11:39,640 --> 00:11:44,640
I will ask you to speak in my mind.

92
00:11:44,640 --> 00:11:49,640
I will ask you to speak in my mind.

93
00:11:49,640 --> 00:11:55,640
I will ask you to speak in my mind.

94
00:11:55,640 --> 00:12:00,640
To develop our minds, first of all, we have to know where is our mind.

95
00:12:00,640 --> 00:12:02,640
What's happening in our mind?

96
00:12:02,640 --> 00:12:07,640
Now as you are listening to the Desa-na, where is your mind?

97
00:12:07,640 --> 00:12:11,640
If I ask you this, I think many people can't answer.

98
00:12:11,640 --> 00:12:14,640
If I ask you where your body is, it's very easy to answer.

99
00:12:14,640 --> 00:12:19,640
But if I ask you where your mind is, many people are not aware.

100
00:12:19,640 --> 00:12:22,640
We aren't aware of where our mind is gone.

101
00:12:22,640 --> 00:12:28,640
Even sitting here, your mind can go home, your mind can go ten years into the past, ten years into the future.

102
00:12:28,640 --> 00:12:32,640
The mind is very difficult to catch, very difficult to train.

103
00:12:32,640 --> 00:12:37,640
So in the practice of meditation, we are going to start by focusing on the body.

104
00:12:37,640 --> 00:12:38,640
This is easy to find.

105
00:12:38,640 --> 00:12:42,640
When we focus on the body, we will be able to see the mind clearly.

106
00:12:42,640 --> 00:12:45,640
The practice of meditation will start with focusing on the body.

107
00:12:45,640 --> 00:12:48,640
This is what I will show you how to do.

108
00:12:48,640 --> 00:12:58,640
I am going to show you how to do the body.

109
00:12:58,640 --> 00:13:07,640
I am going to show you the body.

110
00:13:07,640 --> 00:13:10,640
I am going to show you how to do the body.

111
00:14:10,640 --> 00:14:17,640
So I ask everyone now to practice here with me.

112
00:14:17,640 --> 00:14:22,640
Now we have very little time and we should make best use of the time that we have.

113
00:14:22,640 --> 00:14:30,640
So without any further ado, please everyone close your eyes, sit in a meditation position as best you can,

114
00:14:30,640 --> 00:14:33,640
and start with me to practice meditation.

115
00:14:33,640 --> 00:14:53,840
As we did this one, I love to see our

116
00:14:53,840 --> 00:15:14,080
So to start with, we're going to look at the body as it clearly presents itself to us.

117
00:15:14,080 --> 00:15:21,160
When we sit quietly without moving the clearest presentation of the body or the clearest

118
00:15:21,160 --> 00:15:25,720
object that we will find is the stomach.

119
00:15:25,720 --> 00:15:29,880
Because every breath that we take will cause the stomach to rise.

120
00:15:29,880 --> 00:15:33,080
And when we expel the breath, it will cause the stomach to fall.

121
00:15:33,080 --> 00:15:37,080
So what we're going to do is begin to focus on the stomach.

122
00:15:37,080 --> 00:15:42,280
We're going to focus on the clearest appearance of the movement of the body, which is

123
00:15:42,280 --> 00:15:44,920
the rising and falling of the stomach.

124
00:15:44,920 --> 00:15:54,260
That way is the movement of this movement of the fight.

125
00:15:54,260 --> 00:15:57,080
Now comment immediately about time.

126
00:15:57,080 --> 00:16:01,360
Also, we are going to have the body to live on the body.

127
00:16:01,360 --> 00:16:06,620
And we will let it be fully connected with thealk.

128
00:16:06,620 --> 00:16:12,520
So that we can interact with this channel and focus on that.

129
00:16:12,520 --> 00:16:12,780
If I inb

130
00:16:12,780 --> 00:16:14,520
the physical capabilities,

131
00:16:14,520 --> 00:16:38,320
There's nothing special about the stomach, but what's special is when we focus on any

132
00:16:38,320 --> 00:16:42,340
object of the body, we're going to see the mind clearly, we're going to see what our

133
00:16:42,340 --> 00:16:48,760
lives, our dislikes, all of our judgements, all of our key lays, all of the good and bad

134
00:16:48,760 --> 00:17:15,300
things that exist in our mind, we'll be able to see clearly.

135
00:17:15,300 --> 00:17:32,140
So what we want to do is to create clear awareness of the rising for what it is.

136
00:17:32,140 --> 00:17:40,420
To not give rise to any judgment, any misunderstanding, to not get lost in our conceptions,

137
00:17:40,420 --> 00:17:46,300
our ideas, our thoughts, our judgements, not to make more of the object than what it is.

138
00:17:46,300 --> 00:17:52,160
In order to do this, we remind ourselves, this is rising.

139
00:17:52,160 --> 00:17:55,820
As the Buddha said, when you walk, know that you're walking, when you sit, know it as

140
00:17:55,820 --> 00:18:00,540
sitting, when you stand, know it as standing, and so on, whatever happens in the body,

141
00:18:00,540 --> 00:18:02,700
know it as it is.

142
00:18:02,700 --> 00:18:06,340
When you know the rising, know it simply as rising.

143
00:18:06,340 --> 00:18:13,180
When you think, how are you, how does it get it, it becomes a universe, it is a everyday

144
00:18:13,180 --> 00:18:22,860
herence and technique on the conversing planet.

145
00:18:22,860 --> 00:18:24,040
When we have through the

146
00:19:24,040 --> 00:19:32,280
simply say to yourself, in your mind, putting your mind at the stomach, rising, and when

147
00:19:32,280 --> 00:19:37,800
the stomach falls, in your mind, not allowed at the stomach, say to yourself,

148
00:19:37,800 --> 00:19:44,720
we have to trade dinosaurs, and we have to be�, and we share their Ten

149
00:19:44,720 --> 00:19:46,560
locals all throughout life.

150
00:19:46,560 --> 00:19:52,160
If you're cookies, what's good about men are happy to chew?

151
00:19:52,160 --> 00:19:54,260
Maybe it's fun.

152
00:19:54,260 --> 00:19:58,840
And a little bit, all of us don't care now because men always have to eat salmon more

153
00:19:58,840 --> 00:20:00,680
now thanpattern.

154
00:20:00,680 --> 00:20:20,340
This means that the

155
00:20:20,340 --> 00:20:33,700
It's important that we're not trying to force the breath.

156
00:20:33,700 --> 00:20:36,140
We're just trying to see it for what it is.

157
00:20:36,140 --> 00:20:40,620
We're not trying to make it smooth or deep or long or clear.

158
00:20:40,620 --> 00:20:45,420
If you can't feel it, you can put your hand on your stomach.

159
00:20:45,420 --> 00:20:50,420
And this will help you to feel the movement clearly.

160
00:21:15,420 --> 00:21:31,820
Now, as we focus on the stomach, simply rising, falling, we'll see that there are many other things that we didn't notice before.

161
00:21:31,820 --> 00:21:34,660
And this is the purpose of focusing on the body.

162
00:21:34,660 --> 00:21:40,740
The next thing that we're going to see most likely is the sensations, the weight and that.

163
00:21:40,740 --> 00:21:51,380
When we're sitting quietly watching the rising and falling, we'll find that our mind is distracted by pain, aching soreness, by happy feelings, by calm feelings.

164
00:21:51,380 --> 00:22:01,180
And we should acknowledge these as well when we feel pain, simply saying to ourselves, pain, pain, seeing it for what it is.

165
00:22:01,180 --> 00:22:09,620
When we feel happy, happy, happy seeing it for what it is, and calm, calm, simply reminding ourselves, it is what it is.

166
00:22:09,620 --> 00:22:13,980
There's nothing good but me, mine about it.

167
00:22:13,980 --> 00:22:42,460
We must have, now and remember, 0-3, we'd have a really good, 7-7-7-7-7-7-7-7-9-7-7-7-7-7-7-7-6-7-7-7-7-6-7-7-7-7-8-7-8-7-8-7-7-7-7-9-7-7-7-8-7-7-7-7-7-7-7-7-7-7-7-7-8-7-7-7 with one hand.

168
00:22:42,460 --> 00:23:11,460
When the Vedana disappears, we simply come back again to the rising and the falling.

169
00:23:11,460 --> 00:23:24,380
We are going to be a seismic

170
00:23:24,380 --> 00:23:53,520
Happy family you took.

171
00:23:53,520 --> 00:24:09,600
The next thing that you should notice is the thoughts that are arising in the mind.

172
00:24:09,600 --> 00:24:15,400
Even though you're trying to focus on the systemic arising and the following of the abdomen,

173
00:24:15,400 --> 00:24:20,400
you'll find that the mind is constantly being distracted by thoughts about the past, thoughts

174
00:24:20,400 --> 00:24:27,000
about the future, good thoughts, bad thoughts, many different kinds of thought.

175
00:24:27,000 --> 00:24:29,680
This shouldn't be considered to be a bad thing.

176
00:24:29,680 --> 00:24:33,920
We can use these thoughts also as an object of meditation.

177
00:24:33,920 --> 00:24:38,400
It's called jit-dhan-bhasana, mindfulness of the mind.

178
00:24:38,400 --> 00:24:44,920
When we feel thought, when we have thoughts, we simply say to ourselves, thinking, thinking,

179
00:24:44,920 --> 00:24:49,720
thinking, and let the thought go, not repressing it or pushing it away,

180
00:24:49,720 --> 00:24:55,560
but also not following after it and losing sight of the present moment of the reality in front of us.

181
00:25:49,720 --> 00:26:11,000
If we do this for long enough, the final thing that we'll see, and what is really the most important,

182
00:26:11,000 --> 00:26:15,080
is we'll see our judgments, our likes and our dislikes,

183
00:26:15,080 --> 00:26:20,600
we'll see our laziness and our distraction, our worry, our doubts, our confusion,

184
00:26:20,600 --> 00:26:23,120
all of the things that cause trouble for us.

185
00:26:23,120 --> 00:26:27,320
And this is really the goal that we're looking for.

186
00:26:27,320 --> 00:26:30,720
We're looking to understand and to be free from these things.

187
00:26:30,720 --> 00:26:34,440
We're looking to overcome and to change our minds

188
00:26:34,440 --> 00:26:41,400
out of these bad habits of liking and disliking, greed and aversion and hatred and anger,

189
00:26:41,400 --> 00:26:49,080
and out of laziness, out of distraction and worry and doubt and confusion.

190
00:26:49,080 --> 00:26:53,800
So, during the time that we're practicing, when any of these things arise,

191
00:26:53,800 --> 00:26:58,280
we should, in the same way as everything else, see them for what they are,

192
00:26:58,280 --> 00:27:02,200
and come to understand that they're only arising for our detriment.

193
00:27:02,200 --> 00:27:06,360
When we see them for what they are, we'll let go of them.

194
00:27:06,360 --> 00:27:12,360
When we like something, simply saying to ourselves, liking, liking, when we dislike something,

195
00:27:12,360 --> 00:27:21,000
disliking, disliking, disliking, when we feel drowsy or lazy, we say drowsy or tired or lazy,

196
00:27:21,000 --> 00:27:27,800
when we are distracted or confused, we say distracted or distracted or confused or worried,

197
00:27:27,800 --> 00:27:33,400
when we have doubt, we say to ourselves, doubting, doubting, doubting, simply seeing it for what it is,

198
00:27:33,400 --> 00:27:39,080
and not using it to create all sorts of stress and suffering for ourselves.

199
00:29:03,400 --> 00:29:32,520
So, now let's just try this.

200
00:29:32,520 --> 00:29:37,960
For just a few minutes together, without any talking, please keeping your eyes closed

201
00:29:37,960 --> 00:29:44,920
and trying to focus first on the body, and then on the feelings, the mind, and the hindrances,

202
00:29:44,920 --> 00:29:48,760
the dhamma, the liking, and disliking, and so on.

203
00:29:48,760 --> 00:30:04,080
The ads are on the left hand and the

204
00:30:04,080 --> 00:30:11,680
We always have to study.

205
00:30:11,680 --> 00:30:16,600
They always have to study.

206
00:30:16,600 --> 00:30:21,420
Some guys not only write their words,

207
00:30:51,420 --> 00:30:54,420
.

208
00:30:54,420 --> 00:30:57,420
..

209
00:30:57,420 --> 00:31:01,420
..

210
00:31:01,420 --> 00:31:03,420
..

211
00:31:03,420 --> 00:31:05,420
..

212
00:31:05,420 --> 00:31:06,420
..

213
00:31:06,420 --> 00:31:09,420
..

214
00:31:09,420 --> 00:31:12,420
..

215
00:31:12,420 --> 00:31:15,420
...

216
00:31:15,420 --> 00:31:17,420
..

217
00:31:17,420 --> 00:31:19,420
..

218
00:31:19,420 --> 00:31:20,420
..

219
00:31:20,420 --> 00:31:22,420
..

220
00:31:22,420 --> 00:31:24,420
..

221
00:31:24,420 --> 00:31:26,420
..

222
00:31:26,420 --> 00:31:28,420
..

223
00:31:28,420 --> 00:31:30,420
..

224
00:31:30,420 --> 00:31:32,420
..

225
00:31:32,420 --> 00:31:34,420
..

226
00:31:34,420 --> 00:31:36,420
..

227
00:31:36,420 --> 00:31:38,420
..

228
00:31:38,420 --> 00:31:40,420
..

229
00:31:40,420 --> 00:31:44,420
..

230
00:31:44,420 --> 00:31:48,420
..

231
00:31:48,420 --> 00:31:52,420
..

232
00:31:52,420 --> 00:31:56,420
..

233
00:31:56,420 --> 00:31:58,420
..

234
00:31:58,420 --> 00:32:00,420
..

235
00:32:00,420 --> 00:32:02,420
..

236
00:32:02,420 --> 00:32:04,420
..

237
00:32:04,420 --> 00:32:06,420
..

238
00:32:06,420 --> 00:32:08,420
..

239
00:32:08,420 --> 00:32:10,420
..

240
00:32:10,420 --> 00:32:12,420
..

241
00:32:12,420 --> 00:32:14,420
..

242
00:32:14,420 --> 00:32:16,420
..

243
00:32:16,420 --> 00:32:20,420
..

244
00:32:20,420 --> 00:32:24,420
..

245
00:32:24,420 --> 00:32:26,420
..

246
00:32:26,420 --> 00:32:28,420
..

247
00:32:28,420 --> 00:32:30,420
..

248
00:32:30,420 --> 00:32:32,420
..

249
00:32:32,420 --> 00:32:34,420
..

250
00:32:34,420 --> 00:32:36,420
..

251
00:32:36,420 --> 00:32:40,420
..

252
00:32:40,420 --> 00:32:44,420
..

253
00:32:44,420 --> 00:32:48,420
..

254
00:32:48,420 --> 00:32:52,420
..

255
00:32:52,420 --> 00:32:54,420
..

256
00:32:54,420 --> 00:32:56,420
..

257
00:32:56,420 --> 00:32:58,420
..

258
00:32:58,420 --> 00:33:00,420
..

259
00:33:00,420 --> 00:33:04,420
..

260
00:33:04,420 --> 00:33:08,420
..

261
00:33:08,420 --> 00:33:12,420
..

262
00:33:12,420 --> 00:33:16,420
..

263
00:33:16,420 --> 00:33:20,420
..

264
00:33:20,420 --> 00:33:24,420
..

265
00:33:24,420 --> 00:33:28,420
..

266
00:33:28,420 --> 00:33:32,420
..

267
00:33:32,420 --> 00:33:36,420
..

268
00:33:36,420 --> 00:33:38,420
..

269
00:33:38,420 --> 00:33:38,420
..

270
00:33:38,420 --> 00:33:40,420
..

271
00:33:40,420 --> 00:33:40,420
..

272
00:33:40,420 --> 00:33:41,420
..

273
00:33:41,420 --> 00:33:42,420
..

274
00:33:42,420 --> 00:33:42,420
..

275
00:33:42,420 --> 00:33:43,420
..

276
00:33:43,420 --> 00:33:44,420
..

277
00:33:44,420 --> 00:33:45,420
..

278
00:33:45,420 --> 00:33:48,420
..

279
00:33:48,420 --> 00:33:50,920
..

280
00:33:50,920 --> 00:33:51,920
..

281
00:33:51,920 --> 00:33:52,920
..

282
00:33:52,920 --> 00:33:53,920
..

283
00:33:53,920 --> 00:33:54,920
..

284
00:33:54,920 --> 00:33:56,920
..

285
00:33:54,920 --> 00:33:59,920
..

286
00:33:59,920 --> 00:34:01,960
.

287
00:34:01,960 --> 00:34:05,920
..

288
00:34:05,920 --> 00:34:10,900
..

289
00:34:10,900 --> 00:34:16,060
all the beings in this country and this world, for all beings may they be happy,

290
00:34:16,060 --> 00:34:23,780
I find they find peace and may they be truly free from all suffering.

291
00:34:23,780 --> 00:34:50,500
..

292
00:34:50,500 --> 00:34:53,460
and everything comes up.

293
00:34:53,460 --> 00:34:57,180
You must guess already in the library.

294
00:34:58,160 --> 00:35:05,000
Someone called trustees and staff but also

295
00:35:05,000 --> 00:35:07,000
they are interested in them.

296
00:35:07,100 --> 00:35:12,640
When you say that your smart system and understanding

297
00:35:12,640 --> 00:35:08,560
the function means that theou

298
00:35:08,560 --> 00:35:15,560
become the reason that your smart person is universal so our

299
00:35:15,560 --> 00:35:45,440
I would like to thank you all for taking the time to practice meditation with me.

300
00:35:45,440 --> 00:35:50,440
And I'd like to make a sense of your wish that all of you are able to put this meditation into practice

301
00:35:50,440 --> 00:35:54,120
for your own benefit and development in the Buddhist teaching.

302
00:35:54,120 --> 00:36:00,240
That you may all for yourselves find true peace, happiness and freedom from suffering.

303
00:36:00,240 --> 00:36:02,240
Thank you.

304
00:36:02,240 --> 00:36:18,600
Thank you.

305
00:36:18,600 --> 00:36:23,600
It is much better.

306
00:36:23,600 --> 00:36:42,680
After

307
00:36:42,680 --> 00:36:49,680
I'm not gonna be

308
00:36:49,680 --> 00:36:52,680
I'm not gonna be

309
00:36:52,680 --> 00:36:57,680
I'm not gonna be

