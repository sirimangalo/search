1
00:00:00,000 --> 00:00:07,000
It was believed that there are only 101 languages during the time of the Buddha.

2
00:00:07,000 --> 00:00:15,000
So you see, the dialect means scale in the 101 terms, particularly in that of Makata.

3
00:00:15,000 --> 00:00:21,000
But even in one country, there are more than 100 languages or 100 dialects.

4
00:00:21,000 --> 00:00:29,000
But it is the stock phrase used in the commentaries whenever they want to refer to languages.

5
00:00:29,000 --> 00:00:36,000
It may be something like saying a thousand and one thing.

6
00:00:36,000 --> 00:00:45,000
So an expression.

7
00:00:45,000 --> 00:00:47,000
Okay.

8
00:00:47,000 --> 00:00:49,000
And then how?

9
00:00:49,000 --> 00:00:50,000
Borders and particular.

10
00:00:50,000 --> 00:00:57,000
Anti-clad borders mean particular borders reached discrimination through the F prior F order and through achievement

11
00:00:57,000 --> 00:00:58,000
only two.

12
00:00:58,000 --> 00:01:03,000
But disciples go through all these means, all these means mentioned earlier.

13
00:01:03,000 --> 00:01:11,000
There is no way, no special way of developing a meditation subject in order to attain discrimination.

14
00:01:11,000 --> 00:01:19,000
But in trainers, the attaining of the discrimination come about next upon the liberation, consisting in trainers' fusion.

15
00:01:19,000 --> 00:01:22,000
And in non trainers, non trainers mean at a half.

16
00:01:22,000 --> 00:01:26,000
It does so next upon the liberation consisting in non trainers' fusion.

17
00:01:26,000 --> 00:01:30,000
Trainers means those who have reached the first, second and third stages.

18
00:01:30,000 --> 00:01:32,000
They are called trainers.

19
00:01:32,000 --> 00:01:42,000
And those who have reached the fourth stage and become an error hand is called non trainers.

20
00:01:42,000 --> 00:01:49,000
And how is it developed?

21
00:01:49,000 --> 00:01:56,000
Now, the things class as aggregate basis element facility is true, dependent origin, or

22
00:01:56,000 --> 00:02:02,000
imagination as well as the soil of this understanding or the field of this understanding.

23
00:02:02,000 --> 00:02:09,000
And the first two purification namely purification of virtue and purification of consciousness

24
00:02:09,000 --> 00:02:10,000
are its roots.

25
00:02:10,000 --> 00:02:14,000
They are called root purification.

26
00:02:14,000 --> 00:02:22,000
And then the other five are called, what, trunks purification or body purification.

27
00:02:22,000 --> 00:02:29,000
Now, there are seven kinds of purification with regard to the practice of impassana meditation.

28
00:02:29,000 --> 00:02:32,000
So, the first is called purification of virtue.

29
00:02:32,000 --> 00:02:37,000
Actually, it is not yet repassana, but preparing for repassana.

30
00:02:37,000 --> 00:02:41,000
And then the second is purification of consciousness or purification of mind.

31
00:02:41,000 --> 00:02:45,000
That means concentration.

32
00:02:45,000 --> 00:02:46,000
And then what?

33
00:02:46,000 --> 00:02:49,000
Purification of view.

34
00:02:49,000 --> 00:02:52,000
Next purification by overcoming doubt.

35
00:02:52,000 --> 00:02:57,000
Next purification by knowledge and vision of what is the first and what is not the first.

36
00:02:57,000 --> 00:03:01,000
Then purification by knowledge and vision of the way.

37
00:03:01,000 --> 00:03:04,000
Next purification by knowledge and vision.

38
00:03:04,000 --> 00:03:08,000
So, these are the seven stages of purity.

39
00:03:08,000 --> 00:03:12,000
With regard to the practice of impassana meditation.

40
00:03:12,000 --> 00:03:29,000
And this book from, from chapter, let me see, chapter 18 will describe these purifications

41
00:03:29,000 --> 00:03:32,000
one by one.

42
00:03:32,000 --> 00:03:37,000
That means purification of view and so on.

43
00:03:37,000 --> 00:03:44,000
Because purification of virtue and purification of consciousness are already described.

44
00:03:44,000 --> 00:03:46,000
Now, we come to be aggregates.

45
00:03:46,000 --> 00:03:48,000
The five aggregates.

46
00:03:48,000 --> 00:03:53,000
So, you're all familiar with these five aggregates, right?

47
00:03:53,000 --> 00:03:55,000
Yeah, the five standards.

48
00:03:55,000 --> 00:04:00,000
And the first one is ruba.

49
00:04:00,000 --> 00:04:07,000
Aggregate of corporeality of aggregate of matter.

50
00:04:07,000 --> 00:04:09,000
And the second is feeling.

51
00:04:09,000 --> 00:04:10,000
Aggregate of feeling.

52
00:04:10,000 --> 00:04:12,000
The third aggregate of perception.

53
00:04:12,000 --> 00:04:14,000
Fourth aggregate of mental formations.

54
00:04:14,000 --> 00:04:17,000
And the fifth aggregate of consciousness.

55
00:04:17,000 --> 00:04:26,000
But in this chapter, they are described not in this order.

56
00:04:26,000 --> 00:04:30,000
So, the first aggregate of ruba and then aggregate of consciousness.

57
00:04:30,000 --> 00:04:33,000
And then aggregate of feeling and so on.

58
00:04:33,000 --> 00:04:36,000
So, the first one is the aggregate of ruba.

59
00:04:36,000 --> 00:04:41,000
Now, ruba is defined as in paragraph 34.

60
00:04:41,000 --> 00:04:49,000
See, that's of the characteristic of being molested by cold, etc.

61
00:04:49,000 --> 00:04:55,000
But in, in Burma, we understand this word as meaning to change.

62
00:04:55,000 --> 00:05:06,000
So, anything that changes because of cold, heat, hunger, thirst, bite of insects and so on,

63
00:05:06,000 --> 00:05:08,000
is called ruba.

64
00:05:08,000 --> 00:05:14,000
Because, because it is a characteristic of ruba, it is called ruba.

65
00:05:14,000 --> 00:05:19,000
So, ruba means to change.

66
00:05:19,000 --> 00:05:25,000
When it is cold, you have one kind of material properties.

67
00:05:25,000 --> 00:05:30,000
And then when it is hot, there's another kind of material property and so on.

68
00:05:30,000 --> 00:05:37,000
And change here means not that something changes into some other thing.

69
00:05:37,000 --> 00:05:47,000
But the arising of a different continuity of, different continuity of matter is called change.

70
00:05:47,000 --> 00:05:57,000
Because, according to a bit of a ruba, the last for only a few moments, right?

71
00:05:57,000 --> 00:06:00,000
Only 17 thought moments.

72
00:06:00,000 --> 00:06:03,000
And after that, they disappear.

73
00:06:03,000 --> 00:06:13,000
So, change here means arising of a different kind of material property due to heat, cold, etc.

74
00:06:13,000 --> 00:06:19,000
So, that is what we call change.

75
00:06:19,000 --> 00:06:32,000
And broadly, there are two kinds of matter, memory, entity and derived material properties.

76
00:06:32,000 --> 00:06:51,000
And derived, does not mean, what about, descended from, but it means, depending, I would use the word, depend rather than derive.

77
00:06:51,000 --> 00:07:01,000
So, depending on the primary ones and depending on, now, we need a house to live.

78
00:07:01,000 --> 00:07:05,000
So, the house is what we depend upon.

79
00:07:05,000 --> 00:07:14,000
So, in the same way, the 24 material properties, to be mentioned later,

80
00:07:14,000 --> 00:07:21,000
depend upon the four primary elements for the arising and for their existence.

81
00:07:21,000 --> 00:07:29,000
They are not derived from these four, but they depend upon these four for their existence.

82
00:07:29,000 --> 00:07:35,000
So, for our survival, we depend upon the buildings on houses.

83
00:07:35,000 --> 00:07:45,000
So, we are not produced by houses, but we depend upon the houses to live in, say, to get shelter and protection.

84
00:07:45,000 --> 00:07:54,000
So, in the same way, the 24 material properties are not derived from the four, but they depend upon these four for their existence.

85
00:07:54,000 --> 00:08:05,000
So, we are arising. So, that is why we are called in Pali, who part are, that means, depending ones.

86
00:08:05,000 --> 00:08:12,000
And the primary materiality is a four kinds, and they are at element, water, elements, fire, element and air, element.

87
00:08:12,000 --> 00:08:18,000
And we have met with these four elements in chapter six.

88
00:08:18,000 --> 00:08:25,000
And then, depending materiality, or derived materiality, they are 24 kinds.

89
00:08:25,000 --> 00:08:32,000
Now, I, here, knows, town, body, visible datum, sound, odour, flavour.

90
00:08:32,000 --> 00:08:38,000
From an indidificality, masculinity, lifeicality, heart bases.

91
00:08:38,000 --> 00:08:42,000
For the indimations, verbal intimations, face, element, lightness of matter,

92
00:08:42,000 --> 00:08:53,000
malleability of matter, wilderness of matter, growth of matter, continuity of matter, aging of matter, impermanence, or death of matter.

93
00:08:53,000 --> 00:08:58,000
And the last physical neutral matter. So, they are at 24.

94
00:08:58,000 --> 00:09:08,000
24 plus four, we get 28. So, in Abitama, there are said to be 28 material properties.

95
00:09:08,000 --> 00:09:14,000
Four are primary, and 24 are, those depending on them.

96
00:09:14,000 --> 00:09:27,000
Now, if you look at the list closely, say, we have, you will notice, there is eye, there is ear, nose, tongue, body.

97
00:09:27,000 --> 00:09:31,000
And corresponding to them, there should be five, but there are only four here.

98
00:09:31,000 --> 00:09:42,000
Physical datum to the eye, sound to the ear, odour to the nose, flavour to the tongue, but to the body.

99
00:09:42,000 --> 00:09:51,000
Nothing is mentioned here. And that is because, what we call tangible datum,

100
00:09:51,000 --> 00:10:02,000
is just the group of three primary elements, elements, fire, element, and air elements.

101
00:10:02,000 --> 00:10:11,000
So, these three, the combination of these three elements are what we call, tangible, tangible data.

102
00:10:11,000 --> 00:10:15,000
So, that is why it is not mentioned here.

103
00:10:15,000 --> 00:10:20,000
If it is mentioned, then we will get 25 instead of 24, and there will be confusion.

104
00:10:20,000 --> 00:10:26,000
But it does not, we are not to understand that there is no such thing as tangibility,

105
00:10:26,000 --> 00:10:35,000
because corresponding to the body sensitivity, there must be tangible, tangible objects.

106
00:10:35,000 --> 00:10:40,000
But we have body sensitivity here on my skin.

107
00:10:40,000 --> 00:10:44,000
And then when I touch something, then I have the feeling of that touch.

108
00:10:44,000 --> 00:10:52,000
I experience the feeling of that touch, that tangible object.

109
00:10:52,000 --> 00:11:03,000
So, there is what we call a tangible object, but it is not different from the three primary elements mentioned above.

110
00:11:03,000 --> 00:11:08,000
That is why it is not mentioned here.

111
00:11:08,000 --> 00:11:17,000
So, there are 24, and then these 24 are explained in detail.

112
00:11:17,000 --> 00:11:35,000
And the characteristic function, mode of manifestation, and the approximate causes are all mentioned.

113
00:11:35,000 --> 00:11:55,000
And now, paragraph 37, last line, its function is to pick, to pick up an object among visible data.

114
00:11:55,000 --> 00:11:58,000
Actually, it is not picking up.

115
00:11:58,000 --> 00:12:10,000
But the funny words are we in Shana, and that means to prove, so to prove the mind to the object.

116
00:12:10,000 --> 00:12:14,000
So, to let the mind to the object something like that.

117
00:12:14,000 --> 00:12:24,000
Not like picking up an object, but to prove, or maybe to push the mind to the object.

118
00:12:24,000 --> 00:12:28,000
And then, it is manifested as the footing of eye consciousness.

119
00:12:28,000 --> 00:12:34,000
I think we should use basis for eye consciousness is better than moving.

120
00:12:34,000 --> 00:12:36,000
Footing for eye consciousness.

121
00:12:36,000 --> 00:12:44,000
That means eye consciousness cannot arise if there is no eye or no sensitivity in our bodies.

122
00:12:44,000 --> 00:12:47,000
So, it is the basis for eye consciousness.

123
00:12:47,000 --> 00:12:52,000
Its proximate causes primary elements born of gamma, sourcing from desire to sea.

124
00:12:52,000 --> 00:13:03,000
So, we have some desire to see, and that desire makes the gamma and gamma causes the elements.

125
00:13:03,000 --> 00:13:08,000
So, the primary elements caused by gamma is its proximate cause.

126
00:13:08,000 --> 00:13:12,000
And then, ear, nose, and so on.

127
00:13:12,000 --> 00:13:20,000
And it is, yeah.

128
00:13:20,000 --> 00:13:23,000
Ear, nose, tongue.

129
00:13:23,000 --> 00:13:30,000
It could be understood without much difficulty.

130
00:13:30,000 --> 00:13:32,000
And the body.

131
00:13:32,000 --> 00:13:38,000
Now, paragraph 42.

132
00:13:38,000 --> 00:13:41,000
Some, however, see, and so on.

133
00:13:41,000 --> 00:13:47,000
So, there is a difference of opinion among teachers in the past.

134
00:13:47,000 --> 00:13:55,000
And here, some means, they are certain Mahas and Gikas.

135
00:13:55,000 --> 00:14:04,000
Mahas and Gikas are those who divided themselves from the original sangha.

136
00:14:04,000 --> 00:14:08,000
At the second Buddhist Council.

137
00:14:08,000 --> 00:14:13,000
Before the second Buddhist Council, there was a difference of opinion among moms.

138
00:14:13,000 --> 00:14:18,000
About some practice of some linear rules.

139
00:14:18,000 --> 00:14:22,000
And so, they could not come to agreement.

140
00:14:22,000 --> 00:14:27,000
So, they divided themselves away from the original sangha.

141
00:14:27,000 --> 00:14:29,000
And they are called Mahas and Gikas.

142
00:14:29,000 --> 00:14:33,000
So, some of the Mahas and Gikas held this few.

143
00:14:33,000 --> 00:14:36,000
And one of them was through dharma.

144
00:14:36,000 --> 00:14:53,000
I don't know, but in Sanskrit or in, among the teachers of the Mahas and Gikas, there is a person called was Subhantu.

145
00:14:53,000 --> 00:14:59,000
So, it may be the same person.

146
00:14:59,000 --> 00:15:24,000
So, the name was Subhantu was given in the sub commentary that would be Visudhi Maha.

147
00:15:24,000 --> 00:15:29,000
So, this is a difference of opinion.

148
00:15:29,000 --> 00:15:36,000
Now, let us go to paragraph 46.

149
00:15:36,000 --> 00:15:47,000
Even among theta, what a Buddhist, there is a difference of opinion about whether the sensitivity

150
00:15:47,000 --> 00:16:02,000
is taking objects when they come into contact with them or before the objects reached them.

151
00:16:02,000 --> 00:16:07,000
Now, the paragraph 46 explains this.

152
00:16:07,000 --> 00:16:12,000
Now, among the sensitivity, just possessed of difference due to the different gama,

153
00:16:12,000 --> 00:16:19,000
I and the ear, every hand, non-contiguous objective fields, since consciousnesses cause even if the supporting

154
00:16:19,000 --> 00:16:25,000
primaries of the objective fields do not adhere to the figure, these own supporting primaries and so on.

155
00:16:25,000 --> 00:16:29,000
Now, the meaning is this.

156
00:16:29,000 --> 00:16:39,000
I and ear take the object which has not reached them before the objects reached them,

157
00:16:39,000 --> 00:16:46,000
there is what is meant here.

158
00:16:46,000 --> 00:16:55,000
It may or may not be correct, but the opinion of Bhutakosa and later teachers is there.

159
00:16:55,000 --> 00:17:04,000
The eye and ear take the objects which have not reached them.

160
00:17:04,000 --> 00:17:09,000
So, before the objects reached them, they take the object.

161
00:17:09,000 --> 00:17:16,000
Before they come into contact, physical contact with the sensitivities.

162
00:17:16,000 --> 00:17:24,000
The eye take the visible object, visible object means just color.

163
00:17:24,000 --> 00:17:38,000
So, before it really touches the eye, it takes the object because if you say that the eye sensitivity takes the object,

164
00:17:38,000 --> 00:17:49,000
only one, it touches it that you cannot see because the visible object will come and drop your particle passage of light.

165
00:17:49,000 --> 00:18:11,000
So, the opinion of later common leaders including the vulnerable Bhutakosa was that these two centered sensitivities take the object before the object actually reaches them.

166
00:18:11,000 --> 00:18:22,000
But the other three, nose, tongue, and body takes the object only when they come into contact with the vulnerable.

167
00:18:22,000 --> 00:18:27,000
They appear, they become a tear to the sensitivity.

168
00:18:27,000 --> 00:18:33,000
That means, for example, smell.

169
00:18:33,000 --> 00:18:41,000
Unless it appears to be nose, sensitivity or nose, we do not experience smell.

170
00:18:41,000 --> 00:18:44,000
Some distance away from us.

171
00:18:44,000 --> 00:18:53,000
If it does not come into contact with our nose, then we do not feel that we do not experience smell.

172
00:18:53,000 --> 00:19:03,000
What is the difference between the eyes going up to make the object and the object coming to the eyes of the eye?

173
00:19:03,000 --> 00:19:09,000
What, how would you know which was correct?

174
00:19:09,000 --> 00:19:11,000
What would be the test?

175
00:19:11,000 --> 00:19:18,000
What is the effect of understanding of one of the other?

176
00:19:18,000 --> 00:19:25,000
There are arguments on both sides.

177
00:19:25,000 --> 00:19:47,000
Considering some waves, then we may not agree with what Bhutakosa says here because only when some waves strike against the eardrum,

178
00:19:47,000 --> 00:19:51,000
do we hear sound, right?

179
00:19:51,000 --> 00:20:03,000
It must come into physical contact with the ear sensitivity vibrations in order to hear.

180
00:20:03,000 --> 00:20:19,000
So, we may start with later details like Bhutakosa or with former common details.

181
00:20:19,000 --> 00:20:35,000
Those older common rays took eye and ear also to be taking objects which come into contact with them.

182
00:20:35,000 --> 00:20:51,000
If you have the egg positive, I will give you the page numbers of it. It is explained more fully there.

183
00:20:51,000 --> 00:21:07,000
And then the eye sensitivity is described here and it looks like a blue-litre spreader.

184
00:21:07,000 --> 00:21:22,000
During the time of the moment, the black thing in the eye is just a solid thing, not a window.

185
00:21:22,000 --> 00:21:24,000
The black thing is a window, right?

186
00:21:24,000 --> 00:21:30,000
It can go small or big when there is light.

187
00:21:30,000 --> 00:21:39,000
What we gather from the writings of the common details is that they take this to be a real thing.

188
00:21:39,000 --> 00:21:45,000
So eye sensitivity is supposed to reside on that place.

189
00:21:45,000 --> 00:21:49,000
It is surrounded by black eyelashes and braid with dark and circles.

190
00:21:49,000 --> 00:21:55,000
And here is a form in the place in the middle of the black circle, surrounded by the white circle,

191
00:21:55,000 --> 00:22:00,000
and that which are the eye with its accessories and so on.

192
00:22:00,000 --> 00:22:11,000
So eye sensitivity resides on a place in the eye with a blue-litre spreader.

193
00:22:11,000 --> 00:22:18,000
So the black of the eye is compared to a blue-litre spreader.

194
00:22:18,000 --> 00:22:30,000
And it is said that it is the eye sensitivity, many particles of eye sensitivity for which that place.

195
00:22:30,000 --> 00:22:32,000
And then ear sensitivity.

196
00:22:32,000 --> 00:22:48,000
Yes, and the sensitivity is the result in the place with its shape-like finger store.

197
00:22:48,000 --> 00:22:57,000
What is the finger store?

198
00:22:57,000 --> 00:23:01,000
Is it a ring?

199
00:23:01,000 --> 00:23:05,000
So something like a ring in the inner ear.

200
00:23:05,000 --> 00:23:10,000
And then the nose is like a coat.

201
00:23:10,000 --> 00:23:20,000
And then the tongue is like a lotus petal tip of a lotus petal.

202
00:23:20,000 --> 00:23:24,000
And then body is a liquid depth soaks and layer of cotton.

203
00:23:24,000 --> 00:23:28,000
The body sensitivity is found everywhere.

204
00:23:28,000 --> 00:23:44,000
The whole of the body is provided by the body sensitivity, but accepting the nose and then the hair.

205
00:23:44,000 --> 00:24:00,000
In this physical body where there is metal, that is clung too.

206
00:24:00,000 --> 00:24:05,000
It is vaguely renderable by organic or sentient or living metal that is run.

207
00:24:05,000 --> 00:24:12,000
Technically, it is a matter of a full primary that is clung too or derived by karma.

208
00:24:12,000 --> 00:24:17,000
But not necessarily the bone of karma.

209
00:24:17,000 --> 00:24:25,000
But everything that is collected with living beings is called upardin in here, a upardin in barley.

210
00:24:25,000 --> 00:24:32,000
And so something like organic or sentient is better.

211
00:24:32,000 --> 00:24:41,000
And then these are compared to snakes, crocodiles, birds, dogs and jaggers.

212
00:24:41,000 --> 00:24:52,000
The eye is compared to a snake, the ear to crocodiles, the nose to birds, the tongue to dogs and the body to jaggers.

213
00:24:52,000 --> 00:24:56,000
And that gravitate to their own respective resource.

214
00:24:56,000 --> 00:25:03,000
That is to say ant hills, the snakes live in ant hills, crocodiles live in water, birds live in air or space,

215
00:25:03,000 --> 00:25:07,000
dogs live in villages and jaggers live in China or grounds.

216
00:25:07,000 --> 00:25:10,000
So eye is there to be like a snake.

217
00:25:10,000 --> 00:25:14,000
A snake likes to be in a place where there is many things.

218
00:25:14,000 --> 00:25:20,000
And so the eye also wants to see different things.

219
00:25:20,000 --> 00:25:25,000
If the wall has nothing and it is plain, then we don't want to see.

220
00:25:25,000 --> 00:25:27,000
So the eye wants to see different things.

221
00:25:27,000 --> 00:25:32,000
The intricate patterns are painting something like that.

222
00:25:32,000 --> 00:25:35,000
So the eye is compared to a snake.

223
00:25:35,000 --> 00:25:40,000
A snake also does not want to be in a place where there is nothing.

224
00:25:40,000 --> 00:25:45,000
It wants to be in the debris or something.

225
00:25:45,000 --> 00:25:50,000
So they are compared to these animals.

226
00:25:50,000 --> 00:26:02,000
And you can read in more detail in Expositor, which 111.

227
00:26:02,000 --> 00:26:05,000
And then you come to visible datum.

228
00:26:05,000 --> 00:26:11,000
That means visible object or color.

229
00:26:11,000 --> 00:26:17,000
And then sound, odor, flavor, feminine difficulty.

230
00:26:17,000 --> 00:26:20,000
That means the female sex as its characteristic.

231
00:26:20,000 --> 00:26:22,000
It's one kind of issue that this is a female.

232
00:26:22,000 --> 00:26:27,000
It is manifested as a reason for the mark, sign, work and ways of the female.

233
00:26:27,000 --> 00:26:32,000
Now, please expose it to page 119.

234
00:26:32,000 --> 00:26:40,000
So women have different marks, different signs, different work, different ways of doing things than men.

235
00:26:40,000 --> 00:26:48,000
So by looking at them and we know that this is a woman, this is a man.

236
00:26:48,000 --> 00:26:55,000
So that peculiarity in a woman is called the femininity.

237
00:26:55,000 --> 00:26:58,000
And in a man is called masculinity.

238
00:26:58,000 --> 00:27:04,000
Sometimes we may see someone in Expositor.

239
00:27:04,000 --> 00:27:07,000
But we know that it is a man or it is a woman.

240
00:27:07,000 --> 00:27:14,000
Or sometimes we know that it is a man or a woman by looking how they walk.

241
00:27:14,000 --> 00:27:22,000
Sometimes we know how when they are children, how they play and so on.

242
00:27:22,000 --> 00:27:26,000
So boys would play, they are fighting with each other.

243
00:27:26,000 --> 00:27:31,000
And the girls would play cooking or something like that.

244
00:27:31,000 --> 00:27:33,000
So femininity and masculinity.

245
00:27:33,000 --> 00:27:39,000
And these two also are co-extensive with the whole body.

246
00:27:39,000 --> 00:27:47,000
That means masculinity and femininity resides in the whole of the body.

247
00:27:47,000 --> 00:27:52,000
Not in some parts of the body, but the whole of the body.

248
00:27:52,000 --> 00:28:02,000
But it does not follow that they have to be called either located in the space where body sensitivity is located or located in the space where that is not located.

249
00:28:02,000 --> 00:28:07,000
Now, body sensitivity provides the whole body also.

250
00:28:07,000 --> 00:28:14,000
So the femininity or masculinity provides the whole body also.

251
00:28:14,000 --> 00:28:24,000
But it does not follow that they have to be called either located in the space where body sensitivity is located.

252
00:28:24,000 --> 00:28:38,000
Because when there is body sensitivity, then we cannot say that at the same place there is femininity or masculinity.

253
00:28:38,000 --> 00:28:44,000
I heard once at the first thing we noticed about somebody and he is just axed.

254
00:28:44,000 --> 00:28:49,000
But they are manneral movements. That's the first thing you notice.

255
00:28:49,000 --> 00:28:51,000
That's what you're saying.

256
00:28:51,000 --> 00:28:56,000
Sometimes we may not see a button, but we hear him or her talk.

257
00:28:56,000 --> 00:29:01,000
And then we do this as a woman. This is a man.

258
00:29:01,000 --> 00:29:10,000
So we are all marked or signed or something of feminine sex or masculine sex.

259
00:29:10,000 --> 00:29:21,000
So the femininity or masculinity resides in the whole of the body.

260
00:29:21,000 --> 00:29:32,000
Then what makes life equality and this is divider in Bali. And in this paragraph instead of ocar.

261
00:29:32,000 --> 00:29:40,000
So we should say exist.

262
00:29:40,000 --> 00:29:53,000
Because its function is to make them ocar non, not to make them ocar, but to make them continue to exist actually.

263
00:29:53,000 --> 00:29:57,000
They ocar by some other causes.

264
00:29:57,000 --> 00:30:07,000
And after they arrive, then this life equality maintains them or protects them.

265
00:30:07,000 --> 00:30:15,000
So its characteristic is not to make them ocar, but to make them continue to exist.

266
00:30:15,000 --> 00:30:18,000
Although it may be a very short moment.

267
00:30:18,000 --> 00:30:24,000
So instead of ocar, I think we should use exist or present.

268
00:30:24,000 --> 00:30:32,000
And then about the middle of the paragraph, it's sometimes beginning with ends.

269
00:30:32,000 --> 00:30:41,000
And it ocar itself only through its connection with the states that ocar like a pilot.

270
00:30:41,000 --> 00:30:54,000
Now, here also it exists only through its connection with the states that it maintains.

271
00:30:54,000 --> 00:31:03,000
Now, it is said that life equality maintains the cognizant material properties.

272
00:31:03,000 --> 00:31:06,000
So life equality is the one that maintains them.

273
00:31:06,000 --> 00:31:17,000
That keeps them from, say, that keeps them from dissolving or something like that.

274
00:31:17,000 --> 00:31:25,000
Now, if life equality maintains others, then who maintains life equality?

275
00:31:25,000 --> 00:31:28,000
That is the problem.

276
00:31:28,000 --> 00:31:34,000
If your protector of others, who is the protector of you, who protects you?

277
00:31:34,000 --> 00:31:39,000
So the answer is it protects itself.

278
00:31:39,000 --> 00:31:43,000
So in protecting others, it protects itself too.

279
00:31:43,000 --> 00:31:48,000
Like a bold man, say, one important man, thank you to the other show.

280
00:31:48,000 --> 00:31:52,000
It takes sense to do.

281
00:31:52,000 --> 00:32:02,000
That is the point here, but it is not so clear reading this sentence.

282
00:32:02,000 --> 00:32:07,000
So the meaning here is just please.

283
00:32:07,000 --> 00:32:12,000
If life equality protects others, who protect what to text it?

284
00:32:12,000 --> 00:32:15,000
So it protects itself.

285
00:32:15,000 --> 00:32:23,000
So like a pilot means here, like a man who moves to both.

286
00:32:23,000 --> 00:32:26,000
Okay, the bold man.

287
00:32:26,000 --> 00:32:38,000
And it is important to understand this correctly.

288
00:32:38,000 --> 00:32:42,000
And it does not cause occurrence after dissolution.

289
00:32:42,000 --> 00:32:46,000
Because of its own absence and depth of what has to be made to occur.

290
00:32:46,000 --> 00:32:50,000
It does not prolong presence at the moment of dissolution.

291
00:32:50,000 --> 00:32:52,000
There are three moments, right?

292
00:32:52,000 --> 00:32:55,000
Rising, continuation, and dissolution.

293
00:32:55,000 --> 00:32:57,000
So at the moment of dissolution, it cannot protect.

294
00:32:57,000 --> 00:33:00,000
Because it is in the state of dissolving itself.

295
00:33:00,000 --> 00:33:02,000
Dissolution itself.

296
00:33:02,000 --> 00:33:06,000
And like the flame of a lamp on the wig and the oil are getting distant.

297
00:33:06,000 --> 00:33:13,000
But it must not be regarded as destitute of power to maintain or make exist and make

298
00:33:13,000 --> 00:33:16,000
present because it does accomplish each of these functions.

299
00:33:16,000 --> 00:33:20,000
And then we come to the heart base.

300
00:33:20,000 --> 00:33:26,000
I have talked about heart base many times.

301
00:33:26,000 --> 00:33:30,000
So I think you understand about the heart base.

302
00:33:30,000 --> 00:33:43,000
That there is a discussion of heart base in the footnote on page 497.

303
00:33:43,000 --> 00:33:49,000
So I think I told you about heart base.

304
00:33:49,000 --> 00:33:58,000
So in brief, heart base is not mentioned in the first book of a beta.

305
00:33:58,000 --> 00:34:06,000
Even in the last book of a beta-ma in Patana, it is not mentioned by name.

306
00:34:06,000 --> 00:34:14,000
Buddha said just depending on a sudden material property, mind element, and mind consciousness

307
00:34:14,000 --> 00:34:17,000
and consciousness element arise something like that.

308
00:34:17,000 --> 00:34:26,000
But that sudden material property is interpreted to mean the heart base.

309
00:34:26,000 --> 00:34:41,000
And the question of how that is to be known, how do you know that it is a heart base?

310
00:34:41,000 --> 00:34:46,000
And so they give two lines of arguments.

311
00:34:46,000 --> 00:34:51,000
One is from scriptures and the other is from logical reasoning.

312
00:34:51,000 --> 00:34:58,000
Logical reasoning means just by the method of elimination.

313
00:34:58,000 --> 00:35:05,000
So Buddha said there is a sudden material property which is the basis of mind element

314
00:35:05,000 --> 00:35:07,000
and mind consciousness elements.

315
00:35:07,000 --> 00:35:12,000
Now this is cannot be the eye, cannot be the ear and so on.

316
00:35:12,000 --> 00:35:19,000
So by eliminating one after the other you arrive at something remaining and that is the heart base.

317
00:35:19,000 --> 00:35:29,000
So when we commented as a heart base, it is somewhat arbitrary.

318
00:35:29,000 --> 00:35:32,000
So two arguments are given.

319
00:35:32,000 --> 00:35:33,000
Two proves.

320
00:35:33,000 --> 00:35:35,000
One is from the scriptures.

321
00:35:35,000 --> 00:35:48,000
And from the scriptures also, as I said, nowhere in the middle of a beta-glass is a heart base mentioned by name.

322
00:35:48,000 --> 00:35:51,000
By the Buddha.

323
00:35:51,000 --> 00:35:59,000
But they said that Buddha did not mention heart base in the first book of a beta-glass

324
00:35:59,000 --> 00:36:09,000
because Buddha had to conform to a sudden way of preaching.

325
00:36:09,000 --> 00:36:18,000
A sudden way of teaching or unity of his method of teaching.

326
00:36:18,000 --> 00:36:30,000
So in order not to break the unity of teaching, Buddha left the heart base of when he described the material properties.

327
00:36:30,000 --> 00:36:32,000
That is the argument in brief.

328
00:36:32,000 --> 00:36:36,000
And the second one is just by the method of elimination.

329
00:36:36,000 --> 00:36:46,000
So last, all the common leaders arrive at saying that heart base is what is meant by the Buddha.

330
00:36:46,000 --> 00:37:00,000
So please read the, it may be a little difficult, but I think you can understand.

331
00:37:00,000 --> 00:37:07,000
Are you talking about that, put now? Yes. Yeah. Put not on page.

332
00:37:07,000 --> 00:37:14,000
Put not number 26.

333
00:37:14,000 --> 00:37:22,000
Okay.

334
00:37:22,000 --> 00:37:35,000
With about 20 pages, or up to the end of the, what?

335
00:37:35,000 --> 00:37:39,000
Consciousness aggregate.

336
00:37:39,000 --> 00:37:59,000
It's about up to page 118.

337
00:37:59,000 --> 00:38:28,000
And I'll do that.

