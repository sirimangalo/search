1
00:00:00,000 --> 00:00:06,400
Okay, I'm going to line it.

2
00:00:06,400 --> 00:00:14,360
So today we're going to the Jumini Kyan number 27, the jurahatipa no pala sunta.

3
00:00:14,360 --> 00:00:22,360
We've actually given talks on before and studied before, so it's going to be familiar to

4
00:00:22,360 --> 00:00:23,360
some people.

5
00:00:23,360 --> 00:00:29,120
It's going to be familiar to me anyway.

6
00:00:29,120 --> 00:00:37,800
So again, the format is first we will be chanting part of the pali and then reading the

7
00:00:37,800 --> 00:00:43,200
English and discussing it paragraph by paragraph.

8
00:00:43,200 --> 00:00:51,160
If you're here live, you can ask questions via YouTube if you're watching this after

9
00:00:51,160 --> 00:01:12,160
the fact as well, you get to leave comments, we're just going to start it here in a minute.

10
00:01:12,160 --> 00:01:38,960
All right, get into the text here.

11
00:01:38,960 --> 00:01:52,960
Okay, we're ready to start.

12
00:01:52,960 --> 00:02:04,400
Let's begin.

13
00:04:04,400 --> 00:04:11,400
Go jalam moko chasamam nang o tama pasam sisami

14
00:04:11,400 --> 00:04:20,400
Pasatatasatou asu wa wang o tama se to di wa manu sanandhi

15
00:04:20,400 --> 00:04:27,400
kambana bhaum wachayin o ata wa samsam pasamam no

16
00:04:27,400 --> 00:04:33,400
pasamam nigotami wa manu pasam no di

17
00:04:33,400 --> 00:04:38,400
se yatabi bho kusalona wa nikona

18
00:04:38,400 --> 00:04:41,400
gawanam pavi se yah

19
00:04:41,400 --> 00:04:47,400
so pasayana gawanam a hantam a di padam

20
00:04:47,400 --> 00:04:53,400
di gato chaya tantinam yahan jawita tantam

21
00:04:53,400 --> 00:04:59,400
so nitang a jayam a hantam o nang o tini

22
00:04:59,400 --> 00:05:06,400
they were me wakou an bho yatou asu sanam samam nigotami

23
00:05:06,400 --> 00:05:12,400
jatai padani a tantinam nigotam

24
00:05:12,400 --> 00:05:19,400
samam bho baga wa swakatou wakawatam

25
00:05:19,400 --> 00:05:25,400
so pasaytinam a gawatou asu wakasang o ti

26
00:05:25,400 --> 00:05:31,400
kata mani chata rida amo pasami

27
00:05:31,400 --> 00:05:35,400
a kajayati a pandai tindu

28
00:05:35,400 --> 00:05:39,400
di pune a nata para pawa di

29
00:05:39,400 --> 00:05:44,400
wallo a di rupa a di vinantam

30
00:05:44,400 --> 00:05:50,400
mani a jaran di bhan yahakatin a di pakatani

31
00:05:50,400 --> 00:05:55,320
they soon a nitang samarno kahou bho yatam o

32
00:05:55,320 --> 00:06:02,400
a mukanam a gamam wa nigamam wa o sari sati di

33
00:06:02,400 --> 00:06:09,400
they panam a pasang karun di imam ma yam panang samarnam

34
00:06:09,400 --> 00:06:14,400
kata mano pasang kamitwa bho jisamam

35
00:06:14,400 --> 00:06:20,400
a wang jayatou wakawatou a wang yahakatis sati

36
00:06:20,400 --> 00:06:24,400
a wamasamayam wa danarou

37
00:06:24,400 --> 00:06:29,400
pasamam a wang jayatinou bho pho

38
00:06:29,400 --> 00:06:32,400
a wang yahakatis sati

39
00:06:32,400 --> 00:06:36,400
a wang pasamayam wa dan la

40
00:06:36,400 --> 00:06:39,400
pasamati

41
00:06:39,400 --> 00:06:42,400
they soon a nitang samarno kahou bho

42
00:06:42,400 --> 00:06:46,400
kahou bho yatam o a mukanam a gamam wa

43
00:06:46,400 --> 00:06:50,400
nigamam wa o sattori

44
00:06:50,400 --> 00:06:55,400
they a nitang samarno go tamao tane a pasang kamanti

45
00:06:55,400 --> 00:07:00,400
they samarno go tamao dame yahakatis

46
00:07:00,400 --> 00:07:07,400
a nitang satti

47
00:07:07,400 --> 00:07:14,400
a nitang satti

48
00:07:14,400 --> 00:07:21,400
a nitang satti

49
00:07:21,400 --> 00:07:27,400
nitang satti

50
00:07:57,400 --> 00:08:14,160
Bhagavad-ho, sawakasambuddhi, punejapar, ahambo, bhasam, yi-de, kachay brahmana, pani-dhi-dhi-pai, kah-apati-barndi-dhi-pai,

51
00:08:14,160 --> 00:08:22,560
samana-pani-dhi-tai, nil-pani-kata-pa-ra-pa-wa-dhi-wa-la-wa-la-wa-dhi-dhi-ru-pai,

52
00:08:22,560 --> 00:08:29,840
tae-bindantan, yhe-charanti-pa-nya, kate-na-dy-tik-a-tani,

53
00:08:29,840 --> 00:08:37,060
tesunantisam-an-o-kalubo-gautam-o- Kamanga-ma-ma-ma-ma-ma,

54
00:08:37,060 --> 00:08:40,820
nil-yagan-wa-o-sari-sati-dhi,

55
00:08:40,820 --> 00:08:46,800
tae-pa-an-an-am-wisang-haran-ti-im-am-a-ma-a-m-ayam-ban-ham,

56
00:08:46,800 --> 00:08:52,520
saman-ang-gautam-ma-mo-swa-sang-am-mit-wam-woo-chisam-am-mit-woo-chisam-a-m-ma-nah-

57
00:08:52,520 --> 00:09:02,520
a one jay no lorto a one ya kari sati a one must some a young wad and a rope is

58
00:09:02,520 --> 00:09:12,520
hama a one jay p no burpedo a one ya kari sati a one piece some a young wad and

59
00:09:12,520 --> 00:09:22,120
a hero pays I'm party Tastes do none tee some ano color o goo j descending whad nigaman

60
00:09:22,120 --> 00:09:26,560
while s processed to dete eat.

61
00:09:26,560 --> 00:09:31,400
Camin mo go to moteinne pers Second Gandh

62
00:09:31,400 --> 00:09:41,240
t is Yasam- dyno, Bourbon

63
00:09:41,240 --> 00:09:43,720
the Bhang's aiti.

64
00:09:43,720 --> 00:09:48,120
There is a man in a goat, a man in a miyakata,

65
00:09:48,120 --> 00:09:52,280
yasanda, sita, samada, pita, samute,

66
00:09:52,280 --> 00:09:56,300
jita, sampa, nasita, najay,

67
00:09:56,300 --> 00:09:59,520
was samana, and goat, and mampana, and goat,

68
00:09:59,520 --> 00:10:05,280
jandhi, kutosawana, rho, aitanti,

69
00:10:05,280 --> 00:10:08,200
and yada, to samana, we ye,

70
00:10:08,200 --> 00:10:13,960
a goat, and mamu, kasan, yajandhi, rasman,

71
00:10:13,960 --> 00:10:16,920
nagali, and pamba, jaya,

72
00:10:16,920 --> 00:10:20,520
tasamana, a goat, and mampana,

73
00:10:20,520 --> 00:10:27,840
jita, tasamana, vu, bakata,

74
00:10:27,840 --> 00:10:32,000
a pamata, pita, pita, pita,

75
00:10:32,000 --> 00:10:36,160
we are undone, jita, sita,

76
00:10:36,160 --> 00:10:40,600
yasata, yasuna, putasamadeva,

77
00:10:40,600 --> 00:10:44,440
rasma, nagari, yaampa,

78
00:10:44,440 --> 00:10:47,480
jandhi, tasana, rasman,

79
00:10:47,480 --> 00:10:50,440
rasman, jaya, pari, yasana,

80
00:10:50,440 --> 00:10:52,920
and goat, tay, wadami,

81
00:10:52,920 --> 00:10:56,280
saya, mavinya, sajikatma,

82
00:10:56,280 --> 00:10:59,080
pasampa, jaya, ranti,

83
00:10:59,080 --> 00:11:02,320
tay, wasman, suman,

84
00:11:02,320 --> 00:11:05,480
nana, kutosawana, sama,

85
00:11:05,480 --> 00:11:08,640
mana, wadamu, vana, sama,

86
00:11:08,640 --> 00:11:11,440
may, yan, kutu, vay,

87
00:11:11,440 --> 00:11:15,600
sama, nana, sama, nana,

88
00:11:15,600 --> 00:11:17,800
tay, tay, tay, tay, tay, jan,

89
00:11:17,800 --> 00:11:20,640
nima, a bra, mana,

90
00:11:20,640 --> 00:11:22,920
wasama, nana, mana,

91
00:11:22,920 --> 00:11:25,760
tay, bakata, jan, nima,

92
00:11:25,760 --> 00:11:29,400
anarato, sama, nana,

93
00:11:29,400 --> 00:11:32,560
aarantama, tay, tay, tay, jan,

94
00:11:32,560 --> 00:11:36,040
nana, nima, nima, sama,

95
00:11:36,040 --> 00:11:38,480
aarata, nima, mavra,

96
00:11:38,480 --> 00:11:39,680
mana, nana,

97
00:11:39,680 --> 00:11:41,480
nima, nana, nima,

98
00:11:41,480 --> 00:11:43,480
aarantu, tay,

99
00:11:43,480 --> 00:11:45,880
yada, mama, sama, nima,

100
00:11:45,880 --> 00:11:48,840
tay, mama, jatutampa,

101
00:11:48,840 --> 00:11:50,840
nana, nana, sama,

102
00:11:50,840 --> 00:11:52,680
kutasampa, nana, nita,

103
00:11:52,680 --> 00:11:55,240
mama, mama, sama,

104
00:11:55,240 --> 00:11:57,760
sambudu, vakata,

105
00:11:57,760 --> 00:12:01,080
sama, tay, wadasamu,

106
00:12:01,080 --> 00:12:03,520
sama, tay, panno, vakata,

107
00:12:03,520 --> 00:12:04,960
sama, wadasam, wadasam,

108
00:12:04,960 --> 00:12:07,920
gothi, yatoko,

109
00:12:07,920 --> 00:12:10,120
hanbo, sama, nigo,

110
00:12:10,120 --> 00:12:12,560
tame, mani, jatata,

111
00:12:12,560 --> 00:12:16,000
nipata, ni, anasamata,

112
00:12:16,000 --> 00:12:18,760
nita, mama, mama,

113
00:12:18,760 --> 00:12:21,360
sama, sambudu, vakata,

114
00:12:21,360 --> 00:12:24,040
vakata, vakata,

115
00:12:24,040 --> 00:12:26,040
mama, supati,

116
00:12:26,040 --> 00:12:29,520
vakata, sama, wadasamu,

117
00:12:29,520 --> 00:12:31,960
tay, vakata,

118
00:12:31,960 --> 00:12:34,720
jana, sony brahmanu,

119
00:12:34,720 --> 00:12:36,760
sama, sita,

120
00:12:36,760 --> 00:12:38,880
vakata, vakata,

121
00:12:38,880 --> 00:12:40,560
hooro, he twah,

122
00:12:40,560 --> 00:12:42,000
he comes and

123
00:12:42,000 --> 00:12:44,160
tara, sankan,

124
00:12:44,160 --> 00:12:45,640
kari twah, yay,

125
00:12:45,640 --> 00:12:47,040
nabagawati,

126
00:12:47,040 --> 00:12:48,840
nana, nana, nana,

127
00:12:48,840 --> 00:12:50,320
vakata,

128
00:12:50,320 --> 00:12:51,800
tikatutamu,

129
00:12:51,800 --> 00:12:53,560
nana, mudani,

130
00:12:53,560 --> 00:12:55,480
sie, namota,

131
00:12:55,480 --> 00:12:58,640
sama, wadasamma,

132
00:12:58,640 --> 00:13:00,600
sambudasa,

133
00:13:00,600 --> 00:13:02,920
namota, sabbagawatu,

134
00:13:02,920 --> 00:13:04,680
hara, hato, sama,

135
00:13:04,680 --> 00:13:06,680
sambudasa,

136
00:13:06,680 --> 00:13:08,960
namota, sabbagawatu,

137
00:13:08,960 --> 00:13:10,800
hara, hato, sama,

138
00:13:10,800 --> 00:13:12,720
sambudasa,

139
00:13:12,720 --> 00:13:13,560
payu,

140
00:13:13,560 --> 00:13:15,000
nama, maiyam,

141
00:13:15,000 --> 00:13:16,600
vikata, jikara,

142
00:13:16,600 --> 00:13:17,720
hati,

143
00:13:17,720 --> 00:13:19,240
tay, namota,

144
00:13:19,240 --> 00:13:20,240
vakata, mini,

145
00:13:20,240 --> 00:13:21,240
nasa, jim,

146
00:13:21,240 --> 00:13:22,640
samanga, jay,

147
00:13:22,640 --> 00:13:23,920
nama,

148
00:13:23,920 --> 00:13:25,560
payu, nama,

149
00:13:25,560 --> 00:13:27,160
siyakawati,

150
00:13:27,160 --> 00:13:28,160
vakata,

151
00:13:28,160 --> 00:13:30,480
sambagawati,

152
00:13:30,480 --> 00:13:32,080
hato, kojana,

153
00:13:32,080 --> 00:13:33,920
soni, brahmano,

154
00:13:33,920 --> 00:13:35,760
yay, navagawati,

155
00:13:35,760 --> 00:13:37,680
nupasangami,

156
00:13:37,680 --> 00:13:39,080
upa, sangami,

157
00:13:39,080 --> 00:13:40,600
vakata,

158
00:13:40,600 --> 00:13:40,600
vakata,

159
00:13:40,600 --> 00:13:40,840
vakata,

160
00:13:40,840 --> 00:13:40,840
vakata,

161
00:13:40,840 --> 00:13:41,800
vakata,

162
00:13:41,800 --> 00:13:43,280
vakata,

163
00:13:43,280 --> 00:13:44,720
sambudani,

164
00:13:44,720 --> 00:13:45,720
nankata,

165
00:13:45,720 --> 00:13:46,760
sarni,

166
00:13:46,760 --> 00:13:47,280
nyan,

167
00:13:47,280 --> 00:13:48,680
viti, sari,

168
00:13:48,680 --> 00:13:49,680
vakata,

169
00:13:49,680 --> 00:13:51,200
akamantani,

170
00:13:51,200 --> 00:13:52,600
sidi,

171
00:13:52,600 --> 00:13:54,120
akamantani,

172
00:13:54,120 --> 00:13:55,120
sindo,

173
00:13:55,120 --> 00:13:56,120
kojana,

174
00:13:56,120 --> 00:13:56,800
sari,

175
00:13:56,800 --> 00:13:57,800
vakata,

176
00:13:57,800 --> 00:13:58,800
jangami,

177
00:13:58,800 --> 00:13:59,800
hato,

178
00:13:59,800 --> 00:14:00,800
hoo,

179
00:14:00,800 --> 00:14:01,800
sippi,

180
00:14:01,800 --> 00:14:02,800
jikin,

181
00:14:02,800 --> 00:14:03,800
vakata,

182
00:14:03,800 --> 00:14:04,800
kajin,

183
00:14:04,800 --> 00:14:05,800
sari,

184
00:14:05,800 --> 00:14:06,800
kara,

185
00:14:06,800 --> 00:14:08,800
sallapotansamamamara,

186
00:14:08,800 --> 00:14:09,800
vakawati,

187
00:14:09,800 --> 00:14:10,800
sisi,

188
00:14:10,800 --> 00:14:12,800
ayamutay,

189
00:14:12,800 --> 00:14:13,800
vakawati,

190
00:14:13,800 --> 00:14:14,800
janu,

191
00:14:14,800 --> 00:14:15,800
soni,

192
00:14:15,800 --> 00:14:16,800
brahmanu,

193
00:14:16,800 --> 00:14:18,800
yitalahocha,

194
00:14:18,800 --> 00:14:19,800
nakobra,

195
00:14:19,800 --> 00:14:20,800
vanai,

196
00:14:20,800 --> 00:14:21,800
tah,

197
00:14:21,800 --> 00:14:22,800
vatai,

198
00:14:22,800 --> 00:14:23,800
tipadoo,

199
00:14:23,800 --> 00:14:24,800
pamawitari,

200
00:14:24,800 --> 00:14:25,800
nahari,

201
00:14:25,800 --> 00:14:26,800
vakata,

202
00:14:26,800 --> 00:14:27,800
vakata,

203
00:14:27,800 --> 00:14:28,800
hoo,

204
00:14:28,800 --> 00:14:29,800
japith yamana,

205
00:14:29,800 --> 00:14:30,800
katith,

206
00:14:30,800 --> 00:14:31,800
vakata,

207
00:14:31,800 --> 00:14:34,800
pamawitari,

208
00:14:34,800 --> 00:14:36,800
sambaridamo,

209
00:14:36,800 --> 00:14:38,800
practise,

210
00:14:38,800 --> 00:14:39,800
tamsuna,

211
00:14:39,800 --> 00:14:40,800
jcouldho,

212
00:14:40,800 --> 00:14:41,800
vakata,

213
00:14:41,800 --> 00:14:42,800
sthenitin,

214
00:14:42,800 --> 00:14:44,800
ayahangobata,

215
00:14:44,800 --> 00:14:45,800
kajanu,

216
00:14:45,800 --> 00:14:46,800
soni,

217
00:14:46,800 --> 00:14:48,800
vakawataw,

218
00:14:48,800 --> 00:14:49,800
vakata.

219
00:14:49,800 --> 00:14:50,200
soar

220
00:14:50,200 --> 00:14:52,400
af taxable

221
00:14:52,440 --> 00:14:52,800
wakata,

222
00:14:52,800 --> 00:15:01,720
chance. Just going to see how far we've

223
00:16:52,800 --> 00:16:59,800
been here, and then we'll see how far we've been here.

224
00:16:59,800 --> 00:17:11,800
Okay, we're still broadcasting. We can do what that's like, right? So some technical difficulty

225
00:17:11,800 --> 00:17:17,800
are computer just for Thomas, and everything's up for it should be. So I think we're going

226
00:17:17,800 --> 00:17:24,800
to stop the poly there. I think we're going to stop the poly there. We're only about a third

227
00:17:24,800 --> 00:17:27,800
done in the soup death. But I think near the end it gets repetitive. It repeats from

228
00:17:27,800 --> 00:17:32,800
other soup death. So hopefully we've actually done about a half of them, but with the

229
00:17:32,800 --> 00:17:39,800
stuff. Let's see.

230
00:17:39,800 --> 00:17:52,800
Just want to

231
00:17:52,800 --> 00:18:04,800
see if we can find a better place to start.

232
00:18:04,800 --> 00:18:10,800
Now I think we'll do it in three parts then. Okay, so today we'll do the first part. We've

233
00:18:10,800 --> 00:18:17,800
gotten through the, sorry, I'll just pull up the text here again.

234
00:18:17,800 --> 00:18:27,800
Okay, so we got through the

235
00:18:27,800 --> 00:18:33,800
preface of the text, which is actually a whole section itself. So we'll stop there.

236
00:18:33,800 --> 00:18:41,800
And we'll

237
00:18:41,800 --> 00:18:48,800
start with English. So let's have a heard. On one occasion, the blessed one was doing a

238
00:18:48,800 --> 00:18:56,800
sour tea in Jesus Grove, and out to Pentecost Park. Now, a little timbit of information.

239
00:18:56,800 --> 00:19:02,800
This was the first two to preach by Mahindatira following his arrival in Sri Lanka. That's

240
00:19:02,800 --> 00:19:05,900
interesting. I wonder if that's why it was, it's one of the only

241
00:19:05,900 --> 00:19:12,900
souters that I taught in Sri Lanka. There's a video on knowing the Buddha, I think, I

242
00:19:12,900 --> 00:19:18,900
pulled on a rua. I was just looking at it. Trying to think where I'd remember giving a

243
00:19:18,900 --> 00:19:21,500
talk, and it was actually while we were visiting

244
00:19:21,500 --> 00:19:28,900
I pulled in a rua in Sri Lanka. That's why I was giving it.

245
00:19:28,900 --> 00:19:32,000
Now in that occasion, the Brahman, John

246
00:19:32,000 --> 00:19:38,000
Osoni, was living out at Sawati in the middle of the day in an all-white chariot drawn

247
00:19:38,000 --> 00:19:44,000
by white mares. He saw the wonder of Pilotika coming in the distance and asked him.

248
00:19:44,000 --> 00:19:49,000
Now, where's Master Vachayana coming from in the middle of the day?

249
00:19:49,000 --> 00:19:59,000
His name was Pilotika, but they called him by his clan name, which I am also his family name.

250
00:19:59,000 --> 00:20:06,000
So this is a prologue to the actual suit.

251
00:20:06,000 --> 00:20:13,000
What wouldn't cost it to come about? Why did the Buddha teach?

252
00:20:13,000 --> 00:20:19,000
Sir, I am coming from the presence of the recluse Gautama.

253
00:20:19,000 --> 00:20:25,000
What does Master Vachayana think of the recluse Gautama's lucidity of wisdom?

254
00:20:25,000 --> 00:20:32,000
He is wise, is he not? Sir, who am I to know the recluse Gautama's lucidity of wisdom?

255
00:20:32,000 --> 00:20:37,000
One would surely have to be as equal to know the recluse Gautama's lucidity of wisdom.

256
00:20:37,000 --> 00:20:42,000
Master Vachayana praises the recluse Gautama with high praise indeed.

257
00:20:42,000 --> 00:20:45,000
Sir, who am I to praise the recluse Gautama?

258
00:20:45,000 --> 00:20:50,000
The recluse Gautama is praised by the praise to best among gods and humans.

259
00:20:50,000 --> 00:20:56,000
There are some high praise, no, he is really setting him up.

260
00:20:56,000 --> 00:21:03,000
What reasons does Master Vachayana see that he is such firm confidence in the recluse Gautama?

261
00:21:03,000 --> 00:21:09,000
Here we get the, similarly of the elephant-hoof.

262
00:21:09,000 --> 00:21:21,000
The elephant footprint, the elephant footprint, but this is the original part.

263
00:21:21,000 --> 00:21:25,000
The Buddha is going to give his own interpretation.

264
00:21:25,000 --> 00:21:30,000
He talks about four things.

265
00:21:30,000 --> 00:21:36,000
Sir, suppose a wise elephant-whizman would enter an elephant-wood,

266
00:21:36,000 --> 00:21:43,000
and were to see in the elephant-wood a big elephant's footprint, long in extent and broader cross.

267
00:21:43,000 --> 00:21:47,000
He would come to the conclusion, indeed, this is a big bowl elephant.

268
00:21:47,000 --> 00:21:55,000
So too, when I saw four footprints of the recluse Gautama, I came to the conclusion the blessed one is fully enlightened.

269
00:21:55,000 --> 00:21:58,000
The dhamma is well proclaimed in the blessed one.

270
00:21:58,000 --> 00:22:00,000
Sankas practicing the good way.

271
00:22:00,000 --> 00:22:02,000
What are the four?

272
00:22:02,000 --> 00:22:13,000
So, his reckoning is that when a wise elephant-whizman sees a big elephant's footprint,

273
00:22:13,000 --> 00:22:16,000
they'll know right away that this is a big bowl elephant.

274
00:22:16,000 --> 00:22:19,000
So you can tell an elephant by its footprint.

275
00:22:19,000 --> 00:22:25,000
Now, spoiler, Buddha is going to say that you can't actually, he's going to actually extend this, which is interesting.

276
00:22:25,000 --> 00:22:37,000
But it's still an interesting summary, the point that, although it is limited, you see, the problem with this is, of course, that people can be,

277
00:22:37,000 --> 00:22:44,000
he's going to talk about praise, but you can be praised without warrant, right?

278
00:22:44,000 --> 00:22:51,000
It's possible that people who don't don't warrant or don't deserve praise can be praised.

279
00:22:51,000 --> 00:22:58,000
So, he's got this idea that if you hear about how great people are from other people, from others, it's a sign that's surely there are great.

280
00:22:58,000 --> 00:23:12,000
Now, in this case, it's true, but there are, of course, cases where that's not true, where people are highly praised, but underneath, they're actually rogues and devils.

281
00:23:12,000 --> 00:23:21,000
But anyway, he's got four ways of, four elephant footprints that he says, show him that the bunezer, true, will elephant.

282
00:23:21,000 --> 00:23:23,000
So, the first one.

283
00:23:23,000 --> 00:23:31,000
Sir, I have seen here certain learn nobles who are clever, knowledgeable about the doctrines of others, as sharp as hair splitting marksmen.

284
00:23:31,000 --> 00:23:36,000
They wonder about, as it were, demolishing the views of others with the sharp wits.

285
00:23:36,000 --> 00:23:43,000
When they hear the reckless go-to-mount will visit such and such a village or town, they formulate a question thus.

286
00:23:43,000 --> 00:23:46,000
We will go to the reckless go-to-mount and ask him a question.

287
00:23:46,000 --> 00:23:49,000
If he is asked like this, he will answer like this.

288
00:23:49,000 --> 00:23:55,000
And so, his doctrine in this way, and if he is asked like that, he will answer like that.

289
00:23:55,000 --> 00:23:58,000
And so, we will refute his doctrine in that way.

290
00:23:58,000 --> 00:24:12,000
To people who, anyone who, the most clever of the clever, whether they be, and just to skip ahead, the four are actually going to be nobles, brahmins.

291
00:24:12,000 --> 00:24:21,000
Would we have nobles, brahmins, householders, which are like merchants, and then recklessly?

292
00:24:21,000 --> 00:24:33,000
So, the wisest of the wisest of all these four groups, sharp as hair splitting marksmen, the best of the best, they come to go to the Buddha.

293
00:24:33,000 --> 00:24:40,000
Thinking that they are going to house smart him, and that probably he will be shamed in becoming their reckless.

294
00:24:40,000 --> 00:24:48,000
He will be odd by their wit and their wisdom, and will become their disciples.

295
00:24:48,000 --> 00:24:54,000
And then, they hear, the reckless go-to-mount has come to visit such and such a villager town.

296
00:24:54,000 --> 00:25:02,000
They go to the reckless go-to-mount, and the reckless go-to-mount structures, rouses, and glens them with a talk in the dhamma.

297
00:25:02,000 --> 00:25:09,000
After they have been instructed, urged, roused, and glened by the reckless go-to-mount with a talk in the dhamma.

298
00:25:09,000 --> 00:25:14,000
They do not so much ask him the question, so how should they refute his doctrine?

299
00:25:14,000 --> 00:25:19,000
And, actual fact, they become his subjects.

300
00:25:19,000 --> 00:25:20,000
Thank you.

301
00:25:20,000 --> 00:25:21,000
Oops.

302
00:25:21,000 --> 00:25:23,000
Thank you very much.

303
00:25:23,000 --> 00:25:25,000
Wonderful.

304
00:25:25,000 --> 00:25:27,000
Thank you.

305
00:25:27,000 --> 00:25:31,000
Thank you so much.

306
00:25:31,000 --> 00:25:33,000
In fact, they become his disciples.

307
00:25:33,000 --> 00:25:40,000
When I saw this first foot front of the reckless go-to-mount, I came to the conclusion, the blessed one is fully enlightened.

308
00:25:40,000 --> 00:25:44,000
The dhamma is well proclaimed by the blessed one.

309
00:25:44,000 --> 00:25:46,000
The sunka is practicing the good way.

310
00:25:46,000 --> 00:25:49,000
So he kind of jumps, jumps to conclusions here.

311
00:25:49,000 --> 00:25:54,000
It's a good sign, but the good is going to say that it's not the only sign.

312
00:25:54,000 --> 00:25:57,000
Anyway, it's impressive that this occurs.

313
00:25:57,000 --> 00:26:01,000
No matter who comes to see the Buddha, it's sure to impress us just about anyone.

314
00:26:01,000 --> 00:26:09,000
In fact, that no matter who goes to try to debate with the Buddha, they always end up becoming his disciples.

315
00:26:09,000 --> 00:26:16,000
So all of the nobles, this would be the kings and royalty, all of the high-class aristocracy.

316
00:26:16,000 --> 00:26:22,000
They all end up becoming his disciples, even though they think that he is going to be shamed by them.

317
00:26:22,000 --> 00:26:26,000
So this is the first.

318
00:26:26,000 --> 00:26:29,000
The second is the Brahmans do the same.

319
00:26:29,000 --> 00:26:34,000
The third is that householders do the same.

320
00:26:34,000 --> 00:26:41,000
And the fourth is that the fourth is a little bit different.

321
00:26:41,000 --> 00:26:50,000
The fourth is that the preclusers do the same, but not only do they become his students,

322
00:26:50,000 --> 00:26:56,000
but let me start here in an actual effect.

323
00:26:56,000 --> 00:27:02,000
An actual fact, they ask the repless go-to-mounta to allow them to go forth from the home life and the homelessness.

324
00:27:02,000 --> 00:27:05,000
And it gives them the going forth.

325
00:27:05,000 --> 00:27:11,000
Not long after they have gone forth, dwelling alone with drawn, diligent, ardent and resolute.

326
00:27:11,000 --> 00:27:14,000
They are realizing for themselves with direct knowledge.

327
00:27:14,000 --> 00:27:18,000
They hear now interupon and abide in that supreme goal of the holy life,

328
00:27:18,000 --> 00:27:23,000
with a sake of which clans been rightly go forth from the home life and the homelessness.

329
00:27:23,000 --> 00:27:28,000
They say thus, we were very nearly lost, we were very nearly perished.

330
00:27:28,000 --> 00:27:33,000
We, for formerly, we claim that we were requeses, that we were not really requeses.

331
00:27:33,000 --> 00:27:37,000
We claim that we were Brahmins, that we were not really Brahmins.

332
00:27:37,000 --> 00:27:41,000
We claim that we were our hearts, that we were not really our hearts.

333
00:27:41,000 --> 00:27:45,000
And now we are requeses, now we are Brahmins, now we are our hearts.

334
00:27:45,000 --> 00:28:03,000
When I saw this fourth footprint of the reckless go-to-mounta, I came to the conclusion, the blessed one is fully enlightened.

335
00:28:03,000 --> 00:28:14,000
Okay, so he's actually seen, so what he's seen of lay people is that they all become converted and came to establish great faith in the Buddha.

336
00:28:14,000 --> 00:28:24,000
But what he's also seen is even somewhat more impressive, that those people who actually are looking for spiritual enlightenment,

337
00:28:24,000 --> 00:28:27,000
were practicing spiritual path.

338
00:28:27,000 --> 00:28:32,000
When they come to the Buddha, not only did they become converted, but they actually attain their goal.

339
00:28:32,000 --> 00:28:43,000
They actually do become enlightened by practicing Buddha's teaching, and they claim for themselves to be true.

340
00:28:43,000 --> 00:28:49,000
Our hearts, two true requeses, two Brahmins, two our hearts.

341
00:28:49,000 --> 00:28:59,000
And so that the most impressive footprint of all, he points out to Januson.

342
00:28:59,000 --> 00:29:06,000
When I saw these four footprints of the reckless go-to-mounta, I came to the conclusion, the blessed one is fully enlightened.

343
00:29:06,000 --> 00:29:13,000
The Dharma is well proclaimed by the blessed one. The Sangha is practicing the good way.

344
00:29:13,000 --> 00:29:23,000
Certainly reasonable to assume that this is not just any old run of the meal teaching, or run of the meal teacher.

345
00:29:23,000 --> 00:29:28,000
And so Brahmins, Januson, he does what any of us would think to do.

346
00:29:28,000 --> 00:29:31,000
He's not exactly, but he gets quite excited.

347
00:29:31,000 --> 00:29:38,000
When this was said, the Brahmins, Januson, he got down from his all-white chariot, drawn by white mares.

348
00:29:38,000 --> 00:29:45,000
And arranging his upper rope on one shoulder, he extended his hands in reverential salutation toward the blessed one,

349
00:29:45,000 --> 00:29:50,000
and under this exclamation three times.

350
00:29:50,000 --> 00:29:53,000
Honored to the blessed one, accomplished and fully enlightened.

351
00:29:53,000 --> 00:29:56,000
Honored to the blessed one, accomplished and fully enlightened.

352
00:29:56,000 --> 00:30:06,000
Perhaps some time or other, we might meet Master Gautama, and have some conversation with him.

353
00:30:06,000 --> 00:30:17,000
This is actually, if you heard us in the chanting, this is Namontas Abhagavatil, and I had told some of them.

354
00:30:17,000 --> 00:30:24,000
So you can see that this, this, Namontas, actually comes from Natapitika.

355
00:30:24,000 --> 00:30:28,000
We have it here, and we have it elsewhere.

356
00:30:28,000 --> 00:30:38,000
Common means we respect arranging a rover on shoulder, and holding your hands up to your chest in reverential salutation.

357
00:30:38,000 --> 00:30:42,000
Doesn't mean holding like giving a military salute or something.

358
00:30:42,000 --> 00:30:50,000
It's putting, it's pulling your hands up to your chest, and unduly like a prayer kind of thing.

359
00:30:50,000 --> 00:30:56,000
And then he says, well, maybe I can meet them, Master Gautama, but then he does go with it right away,

360
00:30:56,000 --> 00:30:59,000
or maybe later, maybe another day or something.

361
00:30:59,000 --> 00:31:05,000
Then the Brahman, John Isone, went to the blessed one, and exchanged greetings with him.

362
00:31:05,000 --> 00:31:11,000
When this courteous and amiable talk was finished, he sat down on one side and related to the blessed one,

363
00:31:11,000 --> 00:31:14,000
his entire conversation with the wonder of Pilotika.

364
00:31:14,000 --> 00:31:18,000
Thereupon the blessed one told him, at this point, Brahman,

365
00:31:18,000 --> 00:31:23,000
the simile of the elephant's footprint, does not yet been completed in detail.

366
00:31:23,000 --> 00:31:29,000
As to how, as to how it is completed in detail, listen in a tin carefully to what I shall say.

367
00:31:29,000 --> 00:31:34,000
Yes sir, the Brahman, John Isone, he replied, the blessed one, said this.

368
00:31:34,000 --> 00:31:37,000
That's as far as Gautama actually.

369
00:31:37,000 --> 00:31:42,000
That is about a third, and we would have gone further, I think, but then we got cut out.

370
00:31:42,000 --> 00:31:46,000
So we stopped there with the Pilots, let's stop there with the English.

371
00:31:46,000 --> 00:31:48,000
So here we have the preface to the sipta tomorrow.

372
00:31:48,000 --> 00:31:52,000
We'll actually get into what the Buddha says, and of course you can read along.

373
00:31:52,000 --> 00:31:54,000
You can read ahead if you want.

374
00:31:54,000 --> 00:31:59,000
If you have the sipta in front of you, it looks like we'll have three sessions.

375
00:31:59,000 --> 00:32:11,000
Tomorrow we'll discuss the Buddha's idea of how you tell whether someone is a bull elephant.

376
00:32:11,000 --> 00:32:18,000
Let's cut back to here, and we say ourselves again.

377
00:32:18,000 --> 00:32:20,000
And that's all for tonight.

378
00:32:20,000 --> 00:32:21,000
Thank you all for stopping in.

379
00:32:21,000 --> 00:32:24,000
I don't, we don't have quite a question thing up this time.

380
00:32:24,000 --> 00:32:32,000
I don't know whether that actually worked, but hopefully this video has gone up, and we've done the first third of the Julia Heartbeat by the Open Sipta.

381
00:32:32,000 --> 00:32:42,000
Thank you all for tuning in, have a good night.

