1
00:00:00,000 --> 00:00:03,920
Hi, and welcome back to Ask a Monk.

2
00:00:03,920 --> 00:00:11,160
Today's question comes from Jazzy for Sacks.

3
00:00:11,160 --> 00:00:16,800
What is the proper etiquette lay people should bear in mind when entering a Buddhist temple?

4
00:00:16,800 --> 00:00:22,320
What about the basic do's and don'ts when it comes to interacting with monks?

5
00:00:22,320 --> 00:00:28,440
What should or shouldn't lay people do to avoid defending you?

6
00:00:28,440 --> 00:00:34,600
Well, first of all, talking about Buddhist monasteries, I think it's important to understand

7
00:00:34,600 --> 00:00:41,160
that the word Buddhist temple is really a misnomer in Buddhism.

8
00:00:41,160 --> 00:00:47,200
We don't focus on worship and so on.

9
00:00:47,200 --> 00:00:52,040
We focus on monasticism or being alone.

10
00:00:52,040 --> 00:00:54,720
So the leaders of the Buddhist religion are not priests.

11
00:00:54,720 --> 00:00:55,720
They're monks.

12
00:00:55,720 --> 00:01:00,280
And there's an important difference between the two sets of terms.

13
00:01:00,280 --> 00:01:07,120
Temple is a place of worship where you have a priest who intervenes on your behalf or

14
00:01:07,120 --> 00:01:10,360
is your intermediary between you and a god.

15
00:01:10,360 --> 00:01:14,040
And a monastery is a place where one stays alone.

16
00:01:14,040 --> 00:01:20,160
Mono means alone and a monk is someone who stays alone.

17
00:01:20,160 --> 00:01:25,760
So your relationship with a monk should be quite a bit different or somewhat different

18
00:01:25,760 --> 00:01:33,760
between the new relationship with a priest in the sense that the monk can serve as a

19
00:01:33,760 --> 00:01:46,000
guide or at least a good example, but shouldn't be considered as a confidant or a friend.

20
00:01:46,000 --> 00:01:51,280
I think one of the most important things for lay people to realize is that monks have taken

21
00:01:51,280 --> 00:01:57,520
vows to stay away from society or be outside of society.

22
00:01:57,520 --> 00:02:07,760
They aren't really in a position to have close friendships with people who are not monks.

23
00:02:07,760 --> 00:02:14,560
The best way, let's go through these proper etiquette, while treated as a place where people

24
00:02:14,560 --> 00:02:21,920
are staying alone don't get close to the monks, don't sit around chatting, don't expect

25
00:02:21,920 --> 00:02:24,600
it to be a social gathering.

26
00:02:24,600 --> 00:02:29,240
If you come to a monastery, the best thing to do is to think, I'm coming here to meditate

27
00:02:29,240 --> 00:02:36,600
and look for appropriate place or situation or ask about meditation or ask where it's

28
00:02:36,600 --> 00:02:42,560
appropriate to meditate, what are the times that you can come to meditate and so on.

29
00:02:42,560 --> 00:02:50,800
Try to be to address in a modest fashion, because one of the important things of meditation

30
00:02:50,800 --> 00:03:02,120
is to do with what you call abstention from sensuality, so I know a lot of people get

31
00:03:02,120 --> 00:03:07,600
a lot of monastics will get upset or monastic really will get upset when people come

32
00:03:07,600 --> 00:03:13,360
addressing skimpy, skimpy clothing or so on, because it can be a real obvious distraction

33
00:03:13,360 --> 00:03:17,440
and hindrance for people for celibate months.

34
00:03:17,440 --> 00:03:25,560
Addressing modest is important, not flashy, not being loud, not being obnoxious, trying

35
00:03:25,560 --> 00:03:33,280
to fit in with a contemplative environment.

36
00:03:33,280 --> 00:03:37,280
As far as certain things that you shouldn't do, that's pretty particular to the culture

37
00:03:37,280 --> 00:03:41,840
in Thailand, you don't point your feet at anyone, women don't offer things directly to

38
00:03:41,840 --> 00:03:46,880
the monks, but these don't apply to other cultures.

39
00:03:46,880 --> 00:03:51,120
For instance, Sri Lankan monasteries have found quite differently behaviour, so in that

40
00:03:51,120 --> 00:03:57,720
sense, as far as the specifics, they're very culturally based and you're better off talking

41
00:03:57,720 --> 00:04:02,640
to the individual monastics about that.

42
00:04:02,640 --> 00:04:07,720
What about the basic do's and don't interact with monks, as they said, don't expect

43
00:04:07,720 --> 00:04:11,440
to, you don't treat them like a friend, you don't have to treat them like they're higher

44
00:04:11,440 --> 00:04:18,520
than you, but give them the respect of someone who is outside, someone who is sort

45
00:04:18,520 --> 00:04:28,080
of a foreign or an alien or has a separation, try to maintain some sort of separation in

46
00:04:28,080 --> 00:04:33,760
terms of formality, like if you're coming to see a monk, ask questions that are appropriate,

47
00:04:33,760 --> 00:04:40,160
that's the biggest do, is do ask questions, don't expect the monk to teach you.

48
00:04:40,160 --> 00:04:48,080
I'd say one of the biggest don't, so as far as for either monks or for lay people, and

49
00:04:48,080 --> 00:04:55,680
the biggest problems that I find is people with this intense desire to teach others, to

50
00:04:55,680 --> 00:05:01,640
impress their ideas and their interpretations and their views on others, so they will come

51
00:05:01,640 --> 00:05:07,280
up to people and tell them what to do and try to explain Buddhism to them or so on,

52
00:05:07,280 --> 00:05:15,400
and it's really, in my feeling, it's a sign of spiritual immaturity to do that, so don't

53
00:05:15,400 --> 00:05:20,200
obviously don't come in with the intention to teach monks, which of course are surprisingly

54
00:05:20,200 --> 00:05:25,240
happens, where people will come with the intention of explaining to the monks how to be

55
00:05:25,240 --> 00:05:33,560
a good monk or so on, it's kind of funny in some sense, but also don't expect the monks

56
00:05:33,560 --> 00:05:37,320
to do that either, if the monk sits there silently, don't think, well, he doesn't want

57
00:05:37,320 --> 00:05:41,800
to talk to me, maybe I should just leave, ask questions, if you have questions, ask them,

58
00:05:41,800 --> 00:05:45,440
let's what we're here for, but if you don't ask questions, we're just going to sit and

59
00:05:45,440 --> 00:05:52,480
meditate, I think that's an appropriate behavior for a monk, we're not out to teach people

60
00:05:52,480 --> 00:05:59,400
unless they're asking questions, unless they're interested in learning, and what should

61
00:05:59,400 --> 00:06:03,200
or shouldn't lay people do to avoid offending you, I wouldn't worry so much about offending,

62
00:06:03,200 --> 00:06:10,600
I would never worry about offending people in a monastic setting, as long as your behavior,

63
00:06:10,600 --> 00:06:16,760
you should watch your behavior and make sure that it's generally appropriate, and modest

64
00:06:16,760 --> 00:06:25,960
and conservative, not being overly outgoing or so on, thinking of it as a contemplative

65
00:06:25,960 --> 00:06:31,200
environment, but not worrying about the specifics, like if I point my feet there, I'm

66
00:06:31,200 --> 00:06:38,520
going to offend someone or so on, Buddhist are generally pretty laid back as a rule, and

67
00:06:38,520 --> 00:06:46,360
understanding, because obviously nowadays culture is the clash of many different cultures

68
00:06:46,360 --> 00:06:53,000
and so a lot of things, even dress, I find here in the West, people dress a lot less modestly

69
00:06:53,000 --> 00:06:57,720
even when they come to the monastery than they would in Thailand, but I also find that

70
00:06:57,720 --> 00:07:04,800
people are much more understanding about it because of the difference in culture and

71
00:07:04,800 --> 00:07:14,520
the difficulty and the unimportant, it's not entirely essential to follow the strict

72
00:07:14,520 --> 00:07:21,160
code of traditions, though we try to as much as possible, but the biggest thing is that

73
00:07:21,160 --> 00:07:27,400
you'll learn as you go along, and it's the duty of the people in the environment to teach

74
00:07:27,400 --> 00:07:28,400
you.

75
00:07:28,400 --> 00:07:34,720
So, I guess a good set of questions, interesting and probably helpful for those people who

76
00:07:34,720 --> 00:07:39,520
are thinking of attending a meditation session in a monastic setting, so thanks for that

77
00:07:39,520 --> 00:07:46,520
and keep coming.

