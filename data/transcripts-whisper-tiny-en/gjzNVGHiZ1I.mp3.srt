1
00:00:00,000 --> 00:00:20,320
Good evening, everyone, and welcome to our live broadcast in second life.

2
00:00:20,320 --> 00:00:29,840
And locally, there is a reason for doing this every night is that I have meditators here.

3
00:00:29,840 --> 00:00:38,560
People who are dedicating their whole waking hours to the practice of insight meditation.

4
00:00:38,560 --> 00:00:48,400
Doesn't that just sound like something you all would like to do if you haven't done it?

5
00:00:48,400 --> 00:00:57,280
I'm going to introduce you tonight to someone who is just graduated and graduated means

6
00:00:57,280 --> 00:01:03,440
you just finished our foundation course, but he's actually someone who's been practicing

7
00:01:03,440 --> 00:01:16,320
insight meditation for quite a while. I first met him in Winnipeg several years ago, 2013,

8
00:01:16,320 --> 00:01:30,880
finished 12, I can't remember. 11 maybe. He's been keeping in touch and practicing

9
00:01:30,880 --> 00:01:38,400
similar styles and finally he's come over to try out our technique, so I'd like to get

10
00:01:38,400 --> 00:01:52,080
his take and see how this course, so what part it played in his practice. This is Ali.

11
00:01:59,760 --> 00:02:04,560
Hello, everyone. My voice comes across well. I think so.

12
00:02:04,560 --> 00:02:15,760
So my name is Aria Bonta mentioned. I met him both four years ago in Winnipeg and now that I

13
00:02:15,760 --> 00:02:26,000
reflect back in those days, Bonta always talked about the meditation center and now that I came

14
00:02:26,000 --> 00:02:34,720
here and meditated for three weeks, I'm so happy that to see that actually realized that

15
00:02:34,720 --> 00:02:44,640
what a beautiful space and community has been created here is amazing. It's so lovely to see

16
00:02:44,640 --> 00:02:57,200
practitioners, supporters from different countries, Thailand, Sri Lanka, other countries

17
00:02:57,200 --> 00:03:07,040
from Canada who really are very generous and support this center and to have Bonta

18
00:03:07,040 --> 00:03:22,320
here really just present the authentic Buddhist teachings. It's so valuable to have all these

19
00:03:22,320 --> 00:03:37,600
all these support for us to practice and the atmosphere is so conducive. The teachings are so

20
00:03:37,600 --> 00:03:52,080
unpointed and direct and if someone really is fortunate enough to take 20-30 days of their lives and

21
00:03:52,080 --> 00:04:15,200
dedicate practice with these planned methods, then the results are certain, I would say.

22
00:04:15,200 --> 00:04:29,200
So yeah, that's what I just came across now and I always put people on the spot and I know

23
00:04:29,200 --> 00:04:34,480
it's you've just come out of the course so it's hard to really get everything sort of settled

24
00:04:34,480 --> 00:04:40,160
in your mind but how do you feel? Coming into the course you've been a meditator before so it

25
00:04:40,160 --> 00:04:49,440
can't have had a life-changing, it hasn't turned your life from late today but how do you feel

26
00:04:49,440 --> 00:05:02,160
this course has played a part in your practice? I feel very healthy now. I wasn't three weeks ago

27
00:05:02,160 --> 00:05:10,960
and my practice had become a little bit weaker than it used to be like three or four years ago

28
00:05:12,640 --> 00:05:20,160
in the past I would say three months, something six months and as a result I was

29
00:05:20,160 --> 00:05:36,080
more stressed and here I really wanted to come here to kind of I think what in my mind I thought

30
00:05:36,640 --> 00:05:47,040
you know I had realizations and I just need to deepen them but here it was a truly humbling

31
00:05:47,040 --> 00:06:01,040
experience. I learned a lot about the suffering and the mistakes that I you know I have been making

32
00:06:01,040 --> 00:06:14,240
in my life and the places I wasn't mindful and kind of then really I think experienced what

33
00:06:14,240 --> 00:06:26,640
in with much more clarity what I think I just had some glimpse about before but really like this

34
00:06:28,000 --> 00:06:39,680
this course just helped me to really to the point of no return I would say it's just

35
00:06:39,680 --> 00:06:47,200
there's things that are so clear that I don't think I would they could ever forget and

36
00:06:48,320 --> 00:06:59,920
not see them again so yeah and I feel very healthy now and that's essentially what

37
00:06:59,920 --> 00:07:13,280
I was summarized I feel I was sick and now I'm well well thank you great great words

38
00:07:19,920 --> 00:07:27,280
so congratulations to Ali for making it through meditation courses

39
00:07:27,280 --> 00:07:35,280
we had to remember once listening in on reporting before I was teaching before I started

40
00:07:35,280 --> 00:07:42,880
teaching we in Thailand they get a lot of Israeli meditators a lot of Israelis go to Thailand

41
00:07:43,920 --> 00:07:48,400
because they have to serve in the war and I don't know where they have to serve in the army and

42
00:07:50,080 --> 00:07:53,840
after the army they all run away to Thailand it's apparently I think

43
00:07:53,840 --> 00:08:01,200
India as well but they leave the country so anyway I think a lot of Israeli meditators and

44
00:08:02,960 --> 00:08:08,000
one of them he as he was finishing the course on like the second to last day he said

45
00:08:08,880 --> 00:08:15,360
you should teach this to the Israeli army this is harder than anything I had to do in military

46
00:08:15,360 --> 00:08:25,760
training and if you know anything about the Israeli military it's that is saying something from

47
00:08:25,760 --> 00:08:37,440
what I hear it's not easy the great thing about the meditation practice is it's pushing it pushes

48
00:08:37,440 --> 00:08:48,720
you beyond it pushes you far so far out of your comfort zone but it's but it does it gently

49
00:08:50,800 --> 00:09:01,760
it's like it's it's a soft brick it's getting hit in the head with a brick wrapped in a pillow

50
00:09:01,760 --> 00:09:09,040
something it's very we push the meditators very hard but we do it slowly and it's it's

51
00:09:10,800 --> 00:09:18,480
the power of mindfulness so strong that you don't really even

52
00:09:20,480 --> 00:09:25,520
falter most people don't even falter I don't think I've had a single person

53
00:09:25,520 --> 00:09:33,120
who got beyond the the the the first few days or the first

54
00:09:34,720 --> 00:09:41,600
they got over the first sort of adjustment pains since I've been back in Canada I don't think

55
00:09:41,600 --> 00:09:46,560
got a single person run away I mean it says something to their dedication that they're willing

56
00:09:46,560 --> 00:09:56,960
to come all the way to Canada to do this but it's been really super high high graduation rate

57
00:09:57,680 --> 00:10:04,000
very few people I think there's a few people who leave after a few days because it was much

58
00:10:04,000 --> 00:10:12,560
more difficult than they thought than they just were ready for it prepared for it but by and large

59
00:10:12,560 --> 00:10:23,040
it's the kind of difficulty that you that you're happy to do that you're glad to to challenge

60
00:10:23,040 --> 00:10:29,040
yourself with I think that's why people leave in the beginning is because they weren't they weren't

61
00:10:29,040 --> 00:10:35,280
expecting a challenge so that's the one thing I try to get across to people especially when

62
00:10:35,280 --> 00:10:40,400
they're thinking of coming to do a course is that it's not easy it's not meant to be pleasant

63
00:10:40,400 --> 00:10:47,200
all the time or calming and some people are looking for that and I mean most people I think to some

64
00:10:47,200 --> 00:10:54,560
extent are expecting that they're expecting peace even subconsciously and so a big part of the

65
00:10:56,800 --> 00:11:06,160
basic or the beginning stages of the course are helping someone come to terms with the fact that

66
00:11:06,160 --> 00:11:14,880
reality isn't butterflies and rainbows or their that reality is uncomfortable

67
00:11:16,080 --> 00:11:22,960
and so helping meditators to come outside of their comfort zone and to challenge everything that

68
00:11:22,960 --> 00:11:41,040
they hold as real as the year as me as mine and so on but it's a whole other level those people

69
00:11:41,040 --> 00:11:48,400
who have never done if you've never done an intensive course as Ali was saying it's great to hear

70
00:11:48,400 --> 00:11:56,400
the idea that you get this idea that through your practice you're gaining insight that's true

71
00:11:56,400 --> 00:12:02,640
you're certainly are but you tend to overestimate if you just do daily practice you tend to

72
00:12:02,640 --> 00:12:08,320
overestimate your your progress you know you fit meditation into your life without actually

73
00:12:08,320 --> 00:12:15,600
challenging your identity your personality your likes and dislikes you without really challenging

74
00:12:15,600 --> 00:12:23,600
samsara the meditation challenges the whole the meditation course it's a design to challenge all

75
00:12:23,600 --> 00:12:29,280
of samsara the whole of your existence the very framework of what it means to experience

76
00:12:33,360 --> 00:12:41,840
and so you're taking the place as amity and it can get kind of surreal where your experiences

77
00:12:41,840 --> 00:12:53,120
are your minds can sometimes be playing tricks on you and it feels like reality is just a

78
00:12:53,120 --> 00:13:00,320
moments of experience you lose track of the world around you and you see only reality and you're

79
00:13:00,320 --> 00:13:10,960
able to break it apart and and let go and attain to true peace and so you know we can never say what

80
00:13:13,040 --> 00:13:17,200
I can't really talk about what meditators get out of the practice it's wouldn't be fair because

81
00:13:18,320 --> 00:13:26,080
we ourselves are not we're not we're not a Buddha that we can we're not equipped to pronounce

82
00:13:26,080 --> 00:13:35,200
people is having gained this or that attainment on any level but it's encouraging this to hear

83
00:13:35,200 --> 00:13:41,600
these things and this is a tradition my teacher would always make a point of having meditators

84
00:13:42,800 --> 00:13:48,000
talk about their just the feeling that they get how they feel how different they feel

85
00:13:48,000 --> 00:13:56,720
as a means of encouraging others and making clear that this is a positive experience

86
00:13:57,520 --> 00:14:03,760
it's not something to be afraid of the great that's the great thing about how the meditation

87
00:14:03,760 --> 00:14:08,240
challenges you you start to realize that it's not the meditation that's hard

88
00:14:08,240 --> 00:14:18,560
it's that we're not we're not programmed for it it's our own mind and our own

89
00:14:18,560 --> 00:14:25,200
defilements that makes the meditation hard and so you you take refuge in the meditation over

90
00:14:25,200 --> 00:14:31,920
your own personality you come to be your own enemy and and the meditation is a better

91
00:14:31,920 --> 00:14:38,880
friend than you are to yourself and so by using this this growing seed of mindfulness you start

92
00:14:38,880 --> 00:14:46,560
to root out all the stuff inside that's causing you harm I mean that's really it is we are our

93
00:14:46,560 --> 00:14:55,680
worst enemies there's no worse enemy than the untrained mind this is what we this is the paradigm

94
00:14:55,680 --> 00:15:01,120
shift that has to shift it has to take place rather than in the beginning of meditator we'll see

95
00:15:01,120 --> 00:15:06,240
the meditation as a problem the meditation is the enemy it's what's causing me suffering and

96
00:15:07,040 --> 00:15:13,680
when they make that break they start to realize wait a minute I'm the one it's my problem it's

97
00:15:13,680 --> 00:15:22,960
not the meditation and it's it reverses you start to use the the Buddha's teaching to overcome

98
00:15:22,960 --> 00:15:37,200
yourself so this is the best thing that I could encourage on on anyone is to take the time

99
00:15:37,200 --> 00:15:42,800
I mean it's why teaching has always been a part of my life as a Buddhist monk not because I'm

100
00:15:43,360 --> 00:15:51,920
terribly keen to teach but because of how how powerful it is I would a great thing it is to

101
00:15:51,920 --> 00:16:01,920
go through the course how how how transformative it is not just of an individual but of

102
00:16:01,920 --> 00:16:13,760
of society of the world to have people in it that have cultivated such depth of understanding

103
00:16:13,760 --> 00:16:24,160
to the very core of of experience the core of reality it's not some theoretical or it's not a

104
00:16:25,280 --> 00:16:33,440
an equation something you can put down on paper but it's a purity the mind that is free from

105
00:16:33,440 --> 00:16:48,560
greed anger and delusion it's the ability to see the nature of your own mind so through the

106
00:16:48,560 --> 00:16:53,120
foundation course you of course don't become well I don't know that anyone they've ever seen

107
00:16:53,120 --> 00:17:03,760
has become an hour hunt from doing a a meditation a single course not that I would be able to tell

108
00:17:04,880 --> 00:17:06,880
but the first thing it does it's called

109
00:17:10,400 --> 00:17:16,480
but but you're waking a young at the very end of the course when a meditator is starting to settle

110
00:17:16,480 --> 00:17:30,640
back down after going through the whole the whole program they take stock the mind begins to

111
00:17:30,640 --> 00:17:37,680
assess what has changed and so the meditator who has gone through the course is at this unique

112
00:17:37,680 --> 00:17:45,600
position where they can see their own minds where they're clear about their their position on the

113
00:17:45,600 --> 00:17:52,640
path so they see what what they've where they've where they've come what they've accomplished

114
00:17:52,640 --> 00:18:02,160
what they've attained then maybe not you know exactly there's it's not into a completely clear

115
00:18:05,200 --> 00:18:10,000
what they are what they've attained but they see the difference

116
00:18:10,000 --> 00:18:15,840
and then they also see the road ahead they see quite clearly what they have left to do

117
00:18:15,840 --> 00:18:23,280
because they've spent the past two three weeks living with their own mind studying their own

118
00:18:23,280 --> 00:18:29,280
mind so they're quite clear what needs yet to be done quite clear what bad habits they still have

119
00:18:29,280 --> 00:18:41,920
what confusion might still be in the mind what delusion there still is to root out so as I said

120
00:18:41,920 --> 00:18:48,160
before someone someone said it's like seeing the seeing that what you've been studying is only

121
00:18:48,160 --> 00:18:54,560
the tip of the iceberg but you've come to understand that you know that's really a big part of

122
00:18:54,560 --> 00:18:59,440
the finish of the courses in the beginning throughout the course you're working on the iceberg

123
00:18:59,440 --> 00:19:04,720
that you can see above water when you get to the end you can yes you see underwater

124
00:19:05,920 --> 00:19:13,600
and you see which left to be done so I thought that would be a sort of a special

125
00:19:14,320 --> 00:19:21,680
addition of our talk tonight our session tonight I'll give you all a little bit of encouragement

126
00:19:21,680 --> 00:19:31,520
from someone who's been practicing for a while and and is able to give an honest review of the

127
00:19:31,520 --> 00:19:36,480
process for those of you who are here doing the course with us or those of you who are thinking

128
00:19:36,480 --> 00:19:42,080
to those of you who want a better understanding for all of us to get a sort of a better understanding

129
00:19:42,080 --> 00:19:52,080
clarity about what doing an intensive meditation course entails so thank you Ali I'm going to

130
00:19:52,720 --> 00:20:00,240
give that up for tonight's dhamma if anyone has any questions I'm happy to take them otherwise have a good

131
00:20:00,240 --> 00:20:17,360
night

132
00:20:30,240 --> 00:20:34,240
.

133
00:20:34,240 --> 00:20:38,240
..

134
00:20:38,240 --> 00:20:40,240
..

135
00:20:40,240 --> 00:20:44,240
..

136
00:20:44,240 --> 00:20:48,240
..

137
00:20:48,240 --> 00:20:52,240
...

138
00:20:52,240 --> 00:20:54,240
...

139
00:20:54,240 --> 00:20:56,240
..

140
00:20:56,240 --> 00:20:58,240
..

141
00:20:58,240 --> 00:21:08,240
I saw in a video that insinuation is a branch of lying, is this true?

142
00:21:08,240 --> 00:21:19,120
No, it's on a different level because actually outright lying is like you commit, you've

143
00:21:19,120 --> 00:21:20,120
committed to it.

144
00:21:20,120 --> 00:21:29,320
It's like the difference between hurting someone and killing them and killing and you've

145
00:21:29,320 --> 00:21:38,840
committed, it's a bit more extreme, but there's still room to back out when you're just

146
00:21:38,840 --> 00:21:41,880
insinuating something.

147
00:21:41,880 --> 00:21:47,040
It's clearly on immoral, there's the difference if you're trying to trick someone, that's

148
00:21:47,040 --> 00:21:57,080
not a good thing, though you could argue, you're trying to trick them for wholesome purposes.

149
00:21:57,080 --> 00:21:59,520
I think you could potentially argue that.

150
00:21:59,520 --> 00:22:00,520
You could argue that.

151
00:22:00,520 --> 00:22:08,480
There are cases where our hunts have actually tried to trick people, but they don't lie

152
00:22:08,480 --> 00:22:23,480
because lying is on another level, lying is a commission of deception, it's a level of deception

153
00:22:23,480 --> 00:22:34,680
that is a perversion, because truth and wisdom are so important and to say as true, something

154
00:22:34,680 --> 00:22:43,920
that is false, or say as false, something that's true, is strong, there's a weight to

155
00:22:43,920 --> 00:22:54,200
it, karma is never black and white, though it comes down, the only thing black and white

156
00:22:54,200 --> 00:22:58,720
is our state of mind when we do things.

157
00:22:58,720 --> 00:23:03,320
So a person can tell a lie with the best intentions and it won't be that negative.

158
00:23:03,320 --> 00:23:07,920
The consequences won't be that negative, karmically, the person can tell a very small lie

159
00:23:07,920 --> 00:23:13,960
with very bad intentions and it can have very weighty karmic consequences and if they feel

160
00:23:13,960 --> 00:23:21,960
guilty about it afterwards, it will be even worse, because guilt of course is also unwholesome.

161
00:23:21,960 --> 00:23:32,400
But no, we don't consider deception, simple deception as lying or insinuation.

162
00:23:32,400 --> 00:23:39,160
It's much more, I mean, it helps to draw clear distinction if nothing else, that if you're

163
00:23:39,160 --> 00:23:46,480
going to be deceptive, we're all deceptive in various ways, we're not perfect and so through

164
00:23:46,480 --> 00:23:52,120
our defilements, we'll manipulate each other and deceive others, but we take as a vow that

165
00:23:52,120 --> 00:23:58,400
we won't go to the extent of actually outright lying, that's the idea, that it's crossing

166
00:23:58,400 --> 00:23:59,400
a lie.

167
00:23:59,400 --> 00:24:04,720
And you can argue it's just a lie in the sand, it's not real, it's just a concept, but

168
00:24:04,720 --> 00:24:11,560
there is something to it and it helps sort of set a marker for us at the very minimum,

169
00:24:11,560 --> 00:24:16,440
we won't do that, even though we might deceive each other from time to time, we won't

170
00:24:16,440 --> 00:24:17,440
outright lie.

171
00:24:17,440 --> 00:24:23,560
I mean, if you think about it because lies are so terrible when you tell a lie and someone

172
00:24:23,560 --> 00:24:30,720
believes it, it's not just that they insinuated something, it's that they said and you

173
00:24:30,720 --> 00:24:36,760
believe them, and we're so, we don't have telepathy that we can understand, so we believe

174
00:24:36,760 --> 00:24:47,040
each other and we have to, we rely upon words, statements, insinuation is not that and you

175
00:24:47,040 --> 00:24:52,160
actually outright lie at such a terrible thing, the Buddha even said, if a person lies,

176
00:24:52,160 --> 00:24:58,200
there's no evil they won't commit, I think he said in the Jataka or somewhere else, it was

177
00:24:58,200 --> 00:25:03,080
the Bodhisatta I can remember, lying is the worst, if a person lies, there's no evil

178
00:25:03,080 --> 00:25:12,080
that they won't commit, so it's definitely considered to be a bad one.

179
00:25:12,080 --> 00:25:38,760
So, you know, there's no evil, there's no evil, there's no evil, there's no evil

180
00:25:38,760 --> 00:25:52,800
Well, there's the famous case and it's just in the Dhamapada commentary, so it could

181
00:25:52,800 --> 00:26:05,160
always be suspicious, to be a authenticity, we're sorry put that tells this executioner

182
00:26:05,160 --> 00:26:09,920
says, you know, he can't concentrate on, on Sariput is teaching because he's, he's feeling

183
00:26:09,920 --> 00:26:15,560
guilty about all the killing that he's done, executing people, and so Sariput asks him,

184
00:26:15,560 --> 00:26:20,280
you know, well did you do it for, did you do it on your own behest or did you do it

185
00:26:20,280 --> 00:26:24,760
because someone else told you to, and he said, well I did it because someone else told

186
00:26:24,760 --> 00:26:31,880
me to and Sariput says, well then did, then how can you say it's your fault, there

187
00:26:31,880 --> 00:26:38,920
is it really your fault, I guess someone, is it really, are you really to blame?

188
00:26:38,920 --> 00:26:45,280
With the insinuation or the implying that he wasn't to blame, but of course he was, you

189
00:26:45,280 --> 00:26:49,040
know, you don't get out of killing just because someone else tells you to, there's still

190
00:26:49,040 --> 00:26:56,000
a very terrible thing that he did, but it's a deception, Sariput actually, you know, since

191
00:26:56,000 --> 00:27:05,580
to see if now he might argue that an R-100 wouldn't do that, I wouldn't argue that an

192
00:27:05,580 --> 00:27:16,000
R-100 would, would not be afraid, I mean, if it's all about your intentions for the

193
00:27:16,000 --> 00:27:23,800
most part, until you cross those lines, if something decisive about actually saying something

194
00:27:23,800 --> 00:27:33,640
that's false, it's on a different level from the implying, even tricking people when you

195
00:27:33,640 --> 00:27:57,480
outright lie to them, it's evil.

196
00:27:57,480 --> 00:28:27,400
Okay, let's call it a

197
00:28:27,400 --> 00:28:31,800
night then, wishing you all good practice, happy trails.

