1
00:00:00,000 --> 00:00:06,000
Hello Bandai, from some of your videos I have seen that you have said that not

2
00:00:06,000 --> 00:00:11,160
sleeping is healthy. I'm confused really. Isn't sleeping for at least a few

3
00:00:11,160 --> 00:00:16,520
hours a day? For a few hours per day, very useful for both physical and mental

4
00:00:16,520 --> 00:00:24,360
health. Please explain. In most cases, yes, it's really a function of your state

5
00:00:24,360 --> 00:00:34,120
of mind if your mind is in the chaotic state, then you need more sleep to take

6
00:00:34,120 --> 00:00:42,320
time out and to calm down. On the other hand, the sleep state of a mind that's

7
00:00:42,320 --> 00:00:55,160
in chaos is less restful. Even more so because the sleep state of a mind

8
00:00:55,160 --> 00:01:06,000
that is chaotic is the mind that is distracted, the mind that is turbulent, the

9
00:01:06,000 --> 00:01:13,680
sleep state of such a mind is less restful. It's actually exponential in the

10
00:01:13,680 --> 00:01:22,160
sense or it's not just an inverse correlation. It's not one for one. A person

11
00:01:22,160 --> 00:01:28,000
whose mind is calm, first of all, needs less sleep because they need less time

12
00:01:28,000 --> 00:01:33,640
to recuperate from the mental activities and even the physical activities,

13
00:01:33,640 --> 00:01:39,000
which are generally more restful and more calm and more peaceful, but also

14
00:01:39,000 --> 00:01:46,600
their rest time is more restful. They need exponentially to some extent,

15
00:01:46,600 --> 00:01:55,920
exponentially less sleep as a result. There's no sense that, so there's no one

16
00:01:55,920 --> 00:02:01,040
answer. You can't say more sleep is better or it's important to get so many

17
00:02:01,040 --> 00:02:06,360
hours of sleep or more sleep is always better for everyone, less sleep is always

18
00:02:06,360 --> 00:02:12,800
better for everyone. A restful mind is better for everyone and a restful

19
00:02:12,800 --> 00:02:21,040
mind needs less sleep exponentially. If one's mind is perfectly balanced and one

20
00:02:21,040 --> 00:02:28,400
situation is such that one doesn't have as it doesn't engage in

21
00:02:28,400 --> 00:02:36,560
activities that will disturb that sense of calm. There might be no need to

22
00:02:36,560 --> 00:02:39,920
sleep at all, so in the case of Jekyll Bala, this monk who didn't sleep for

23
00:02:39,920 --> 00:02:47,280
three months, and that's not a unique case. It's actually a practice where

24
00:02:47,280 --> 00:02:51,680
people will not sleep for many days, but you have to understand that they're

25
00:02:51,680 --> 00:02:57,440
in a state where their minds are so peaceful, so clear, so calm that they

26
00:02:57,440 --> 00:03:02,600
don't really need to sleep. What would you need to sleep for if your mind and

27
00:03:02,600 --> 00:03:10,760
your body are rested? The body doesn't need to lie down necessarily if it's

28
00:03:10,760 --> 00:03:16,400
calm, if it's in a restful, if you train it to be in a restful state, you'll be

29
00:03:16,400 --> 00:03:23,160
able to sit still and so on. Then certainly the mind doesn't either, the brain

30
00:03:23,160 --> 00:03:31,360
doesn't either, if the mind is calm, so it has all to do with one state of mind.

