1
00:00:00,000 --> 00:00:03,460
Hello and welcome back to our study of the Dhamupada.

2
00:00:03,460 --> 00:00:10,800
Today we continue on with verse 177, which reads as follows.

3
00:00:10,800 --> 00:00:18,300
In a way, Kaddariya, they will look angry with Janti. Bala, Hawaii, Napasang, Santi, Dhanana.

4
00:00:18,300 --> 00:00:22,300
Diro Chaddhanana, Anumodhamana.

5
00:00:22,300 --> 00:00:38,600
Dayne was so hoti, Suki Paratha, which means indeed the stingy do not go to heaven.

6
00:00:38,600 --> 00:00:51,900
Fools indeed do not rejoice in, do not rejoice in gifts.

7
00:00:51,900 --> 00:01:03,100
And the wise or but the wise who rejoice in giving for that.

8
00:01:03,100 --> 00:01:12,400
So hoti, Suki Paratha, they are happy in the hereafter.

9
00:01:12,400 --> 00:01:18,400
So another one of these verses about going to heaven because of giving.

10
00:01:18,400 --> 00:01:21,800
I want to stress that this isn't a deep teaching of the Buddha.

11
00:01:21,800 --> 00:01:28,600
I don't think you could look at this and find some deep psychological or meditative lesson from it.

12
00:01:28,600 --> 00:01:37,100
I'm going to get something of that sort from it, but a little more from the story and we're going to talk about why it is.

13
00:01:37,100 --> 00:01:47,600
I mean what's interesting for us as meditators is why is it that things like giving are, there's a claim that they lead to things like heaven.

14
00:01:47,600 --> 00:01:53,600
There's a lot there and it's quite a source of skepticism.

15
00:01:53,600 --> 00:02:06,600
It sounds like a sort of self-serving teaching where you teach this and what you're really teaching is give.

16
00:02:06,600 --> 00:02:14,600
And not only give, as you'll see with the story, give to me, give to Buddhism.

17
00:02:14,600 --> 00:02:20,600
And the story is talking about how people give to Buddhism and how that's a good thing and it needs you to heaven.

18
00:02:20,600 --> 00:02:26,600
And that rings warning bells I think with many people.

19
00:02:26,600 --> 00:02:33,600
It's just the sort of thing that religious people do is ask for a gift, talk about giving.

20
00:02:33,600 --> 00:02:37,600
I don't think Buddhism is innocent.

21
00:02:37,600 --> 00:02:45,600
Buddhism certainly is, but the Buddhist organization as a whole, there's far too much emphasis on giving.

22
00:02:45,600 --> 00:02:58,600
So the emphasis on these sorts of stories has to be, there has to be tempered with understanding that gas giving is good.

23
00:02:58,600 --> 00:03:08,600
But there's no reason for us to emphasize this teaching to such degree, besides a desire to get things, right?

24
00:03:08,600 --> 00:03:10,600
This is the problem.

25
00:03:10,600 --> 00:03:15,600
It's that if there's desire, then these are the sorts of teachings that are going to be emphasized.

26
00:03:15,600 --> 00:03:20,600
Now that being said, giving is good and giving does lead to things like heaven.

27
00:03:20,600 --> 00:03:24,600
But heaven has never been our goal in Buddhism.

28
00:03:24,600 --> 00:03:33,600
And this teaching, I'll tell the story, I won't drag this out too much, but this story was taught to a king.

29
00:03:33,600 --> 00:03:40,600
And that right there should tell you the sort of the level that this teaching is on.

30
00:03:40,600 --> 00:03:44,600
This king wasn't even, he wasn't Bimbisara, who was a Sotapana and a meditator.

31
00:03:44,600 --> 00:03:47,600
And a very powerful individual.

32
00:03:47,600 --> 00:03:59,600
This is King Poseidon, who was a bit of, well, he wasn't the sharpest knife in the drawer.

33
00:03:59,600 --> 00:04:06,600
He was a great man and a great king in many ways, but I don't think he was entirely good.

34
00:04:06,600 --> 00:04:10,600
I don't think he was particularly wise.

35
00:04:10,600 --> 00:04:20,600
It seems like he often blundered and did things that were often morally questionable.

36
00:04:20,600 --> 00:04:23,600
But he really appreciated the Buddha.

37
00:04:23,600 --> 00:04:32,600
So one day he decided to, the Buddha came back from his travels, and the king decided to give him a great gift.

38
00:04:32,600 --> 00:04:35,600
He gave a gift to all the monks, and so he gave gifts.

39
00:04:35,600 --> 00:04:42,600
And he invited all of his citizens to come and watch, to come and see, oh, look what the king has done.

40
00:04:42,600 --> 00:04:44,600
Look what I have done.

41
00:04:44,600 --> 00:04:51,600
And so they all came, and they saw this, and rather than just appreciating it, which is a good thing.

42
00:04:51,600 --> 00:04:55,600
The appreciation of, it wasn't just a brag, you know.

43
00:04:55,600 --> 00:05:02,600
I mean, inviting all of them there may have been partly to impress the citizenry.

44
00:05:02,600 --> 00:05:07,600
But it also is an approved sort of practice.

45
00:05:07,600 --> 00:05:13,600
Just as giving is approved, and is a good practice, encouraging others to appreciate,

46
00:05:13,600 --> 00:05:18,600
or giving other people the opportunity to appreciate, is also a good thing.

47
00:05:18,600 --> 00:05:25,600
And the answer to why that is, why these things are, why is giving good, why is even appreciation good.

48
00:05:25,600 --> 00:05:32,600
It does have something to do with meditation, and that's, I think, where this story becomes quite interesting to us.

49
00:05:32,600 --> 00:05:35,600
But the bear with me, we've got a story to get through.

50
00:05:35,600 --> 00:05:38,600
It's just all about giving, really.

51
00:05:38,600 --> 00:05:46,600
So the citizens saw this, and they decided, well, the king gave a gift to him, and so they, the next day invited all the monks.

52
00:05:46,600 --> 00:05:49,600
But they gave better than the king.

53
00:05:49,600 --> 00:05:52,600
And they invited the king to see their gift.

54
00:05:52,600 --> 00:05:54,600
Their gift.

55
00:05:54,600 --> 00:06:00,600
So the king came and saw that they had given more and greater than he had given.

56
00:06:00,600 --> 00:06:04,600
And he was disturbed, and he said, well, I can't let them out do me.

57
00:06:04,600 --> 00:06:08,600
And so the next day he invited the Buddha and gave you a better gift.

58
00:06:08,600 --> 00:06:09,600
He invited the citizenry.

59
00:06:09,600 --> 00:06:11,600
The citizen saw this.

60
00:06:11,600 --> 00:06:16,600
The next day they invited the monks and gave even better gifts and better gifts.

61
00:06:16,600 --> 00:06:20,600
And so it went back and forth like a ping-pong mat.

62
00:06:20,600 --> 00:06:22,600
Six times the text sets.

63
00:06:22,600 --> 00:06:24,600
Six times the citizens outdid the king.

64
00:06:24,600 --> 00:06:28,600
And the king finally decided, I don't think I can outdo them.

65
00:06:28,600 --> 00:06:34,600
There's just so many of them, and they have such, they have actually, perhaps, greater resources than I do.

66
00:06:34,600 --> 00:06:37,600
And so he felt very depressed.

67
00:06:37,600 --> 00:06:41,600
And he laid out on his bed, and he started to wonder what he could do.

68
00:06:41,600 --> 00:06:43,600
And the queen came.

69
00:06:43,600 --> 00:06:47,600
Queen Malika, who it seems was the smarter of the two.

70
00:06:47,600 --> 00:06:51,600
She was quite a bit wiser than the king, I think.

71
00:06:51,600 --> 00:06:54,600
And she asked, well, it's wrong.

72
00:06:54,600 --> 00:07:01,600
And he told her, and he said, oh, well, what you have to do is you have to give things that they can't give.

73
00:07:01,600 --> 00:07:13,600
So what she had him do, she said, get these white elephants and have them each hold a parasol over the monks.

74
00:07:13,600 --> 00:07:16,600
And get a bunch of young princesses.

75
00:07:16,600 --> 00:07:18,600
And I'm not really sure why.

76
00:07:18,600 --> 00:07:25,600
I think the point is to find things that the ordinary citizens don't have access to.

77
00:07:25,600 --> 00:07:34,600
I just have this picture of these monks sitting there trying to meditate on all these young women around them crushing perfume, apparently.

78
00:07:34,600 --> 00:07:36,600
Which is the sort of, and fanning them.

79
00:07:36,600 --> 00:07:39,600
Do you imagine monks being a fan by young women?

80
00:07:39,600 --> 00:07:42,600
I mean, that sort of thing, I guess, happened.

81
00:07:42,600 --> 00:07:45,600
And perhaps continues to happen.

82
00:07:45,600 --> 00:07:54,600
I think there's something inappropriate in the sense that a monk certainly wouldn't encourage young women to hang out and fan them and crush perfumes around them.

83
00:07:54,600 --> 00:08:01,600
But again, there's also an allowance for things that kings do.

84
00:08:01,600 --> 00:08:13,600
If kings sometimes want you to do strange things or want to do these sorts of things, well, there's no reason to object.

85
00:08:13,600 --> 00:08:18,600
I mean, it's not against the dhamma to let people to sit where they're when people are doing things.

86
00:08:18,600 --> 00:08:23,600
There's something that says you have to get up and run away or tell them to stop.

87
00:08:23,600 --> 00:08:24,600
It's still.

88
00:08:24,600 --> 00:08:32,600
It's an interesting sort of setup that they had, but very grand-wise, the points.

89
00:08:32,600 --> 00:08:37,600
And certainly the citizens didn't have access to white elephants and that sort of thing.

90
00:08:37,600 --> 00:08:44,600
So they got 500 white elephants and it turns out in the end they only had 499.

91
00:08:44,600 --> 00:08:45,600
They were missing an elephant.

92
00:08:45,600 --> 00:08:49,600
And so the king said to the queen, which should we do, we're missing an elephant.

93
00:08:49,600 --> 00:08:55,600
Our giving is not complete because this grand-wise setup is not perfect.

94
00:08:55,600 --> 00:08:59,600
One of the monks doesn't have an umbrella, isn't going to have an umbrella.

95
00:08:59,600 --> 00:09:02,600
So she said, okay, don't you have other elephants?

96
00:09:02,600 --> 00:09:06,600
And he said, well, we have all these rogue elephants.

97
00:09:06,600 --> 00:09:08,600
These elephants are not trained.

98
00:09:08,600 --> 00:09:11,600
They're unpredictable.

99
00:09:11,600 --> 00:09:18,600
And they're very, very hot-tempered.

100
00:09:18,600 --> 00:09:23,600
So if they came close to the monks, they might stomp on them and gore them to death.

101
00:09:23,600 --> 00:09:25,600
And the queen said, well, I know what to do.

102
00:09:25,600 --> 00:09:34,600
She said, find, bring us one of those monks, one of these elephants, one of these rogue elephants.

103
00:09:34,600 --> 00:09:38,600
And have it stand over Angulimala.

104
00:09:38,600 --> 00:09:47,600
Now Angulimala was this monk who had previously been a murder, a mass murderer.

105
00:09:47,600 --> 00:09:51,600
And the story goes that he killed hundreds of people.

106
00:09:51,600 --> 00:09:53,600
He was feared.

107
00:09:53,600 --> 00:09:55,600
People were terrified.

108
00:09:55,600 --> 00:10:01,600
They wouldn't go near the place where the jungle, where he lived.

109
00:10:01,600 --> 00:10:07,600
And I guess the animals were frightened by him as well because they brought this elephant out with the parasol

110
00:10:07,600 --> 00:10:10,600
and they dragged it over to where Angulimala was standing.

111
00:10:10,600 --> 00:10:14,600
And as soon as it came close to Angulimala, it just stood like this.

112
00:10:14,600 --> 00:10:15,600
I saw the story.

113
00:10:15,600 --> 00:10:19,600
Now, I don't know how much of this is an embellishment, but that's what the story says.

114
00:10:19,600 --> 00:10:22,600
And then there's a verse about that.

115
00:10:22,600 --> 00:10:24,600
No, there's a part.

116
00:10:24,600 --> 00:10:29,600
What's interesting to us is it relates to the verse.

117
00:10:29,600 --> 00:10:33,600
The king had two ministers.

118
00:10:33,600 --> 00:10:40,600
And one of the ministers stood there watching the king giving food and gifts to the monks

119
00:10:40,600 --> 00:10:47,600
and having these young princess crushing perfumes and the elephants and just everything.

120
00:10:47,600 --> 00:10:49,600
Just how expensive it was.

121
00:10:49,600 --> 00:10:53,600
And he thought to himself, what a waste of money.

122
00:10:53,600 --> 00:10:56,600
How much of the king's money is going into this?

123
00:10:56,600 --> 00:11:02,600
First of all, he could have just given food to the man or whatever it is that he gave to the monks.

124
00:11:02,600 --> 00:11:06,600
But to give all that to do all this for no purpose.

125
00:11:06,600 --> 00:11:14,600
It's not like these monks are wealthy landowners who are paying taxes or they're not farmers

126
00:11:14,600 --> 00:11:18,600
who are going to support the kingdom or anything like that.

127
00:11:18,600 --> 00:11:27,600
These are not dignitaries who are going to swear their allegiance and their lands to the king.

128
00:11:27,600 --> 00:11:28,600
These are monks.

129
00:11:28,600 --> 00:11:31,600
They go back to their rooms and they just sit there and do nothing.

130
00:11:31,600 --> 00:11:36,600
I think the text says they just eat and then they go and lie down and sleep,

131
00:11:36,600 --> 00:11:39,600
which I suppose might be true for some monks.

132
00:11:39,600 --> 00:11:44,600
But I don't, I think, generally had the wrong idea about monks.

133
00:11:44,600 --> 00:11:48,600
The other minister, there are two of them.

134
00:11:48,600 --> 00:11:54,600
The other one who was standing beside the king watching, he said, wow, look at all this.

135
00:11:54,600 --> 00:12:01,600
This grand undertaking of the king, what effort he has put into it?

136
00:12:01,600 --> 00:12:09,600
Imagine how he feels inside and the greatness of his heart at this time.

137
00:12:09,600 --> 00:12:14,600
And then it says something, if you're reading the English, if you're following along with this English text,

138
00:12:14,600 --> 00:12:28,600
it's a mistranslation and it's confusing that the mistranslation says, yeah, the mistranslation is just confusing.

139
00:12:28,600 --> 00:12:34,600
But what the poly says is, wow, this king, no one could give like this king.

140
00:12:34,600 --> 00:12:49,600
So that English is prized, but then the next sentence is that there is no king who does not share or give the merit

141
00:12:49,600 --> 00:13:01,600
or the goodness, the benefit of the giving, the greatness of the giving doesn't share it with all beings.

142
00:13:01,600 --> 00:13:13,600
There's no king that doesn't do that.

143
00:13:13,600 --> 00:13:20,600
So what he's saying is that I absolutely, this king must be sharing this with all beings because that's what kings do,

144
00:13:20,600 --> 00:13:22,600
is the implication.

145
00:13:22,600 --> 00:13:33,600
And he says, so, here I am, and I appreciate, I will set my mind in appreciation for the king's good gift.

146
00:13:33,600 --> 00:13:39,600
The green's great gift, an unequalled gift.

147
00:13:39,600 --> 00:13:45,600
And so he stood there appreciating and saying sad, okay, this is good, this is good.

148
00:13:45,600 --> 00:13:47,600
And it's good for me to appreciate, right?

149
00:13:47,600 --> 00:13:51,600
This was the theory, this is the idea, we'll talk about that.

150
00:13:51,600 --> 00:14:01,600
So, and then the monks ask Angalie Malah, he must have been really afraid with that rogue elephant standing over you.

151
00:14:01,600 --> 00:14:04,600
And Angalie Malah said, no, it wasn't afraid.

152
00:14:04,600 --> 00:14:10,600
And the monks said to the Buddha, the Angalie Malah listened to him break, how could he not be afraid?

153
00:14:10,600 --> 00:14:13,600
And the Buddha said, oh no, no, there's no way he's afraid.

154
00:14:13,600 --> 00:14:17,600
And there's a verse there as well, we'll skip over that because it's not really related.

155
00:14:17,600 --> 00:14:22,600
And it's just sort of a side story, that's Angalie Malah thing.

156
00:14:22,600 --> 00:14:25,600
But then the Buddha got up and left.

157
00:14:25,600 --> 00:14:29,600
And the king was literally disappointed.

158
00:14:29,600 --> 00:14:33,600
He thought, I gave so much.

159
00:14:33,600 --> 00:14:40,600
I stood before the Buddha and the Buddha didn't even say thank you, didn't appreciate.

160
00:14:40,600 --> 00:14:44,600
I mean, thank you is not aware they used, but he didn't express appreciation.

161
00:14:44,600 --> 00:14:47,600
He didn't even say anything.

162
00:14:47,600 --> 00:14:50,600
He just got up and left.

163
00:14:50,600 --> 00:14:54,600
And he didn't get angry at the Buddha, but he thought, something must be wrong.

164
00:14:54,600 --> 00:14:55,600
What did I do?

165
00:14:55,600 --> 00:14:57,600
I must have failed in some way.

166
00:14:57,600 --> 00:14:58,600
The Buddha is disappointed in me.

167
00:14:58,600 --> 00:15:03,600
I must have given something improper or not enough, or I don't know.

168
00:15:03,600 --> 00:15:06,600
Something was wrong with what I did.

169
00:15:06,600 --> 00:15:12,600
So he went to the monastery, caught up with the teacher in the Buddha and asked him,

170
00:15:12,600 --> 00:15:14,600
he said, what did I do wrong?

171
00:15:14,600 --> 00:15:17,600
He said, you didn't do anything wrong.

172
00:15:17,600 --> 00:15:21,600
You, everything you gave was perfect.

173
00:15:21,600 --> 00:15:24,600
In fact, that sort of gift is not something,

174
00:15:24,600 --> 00:15:28,600
apparently it's something that a Buddha will only get once in his lifetime.

175
00:15:28,600 --> 00:15:36,600
So once in every Buddha, once in their whole life, they will get this unexpelled gift giving.

176
00:15:36,600 --> 00:15:40,600
That apparently is very impressive.

177
00:15:40,600 --> 00:15:48,600
But he said, the community of the people there was corrupted.

178
00:15:48,600 --> 00:15:51,600
There was something imperfect about it.

179
00:15:51,600 --> 00:15:53,600
There was a problem there.

180
00:15:53,600 --> 00:15:55,600
And the problem was this one minister.

181
00:15:55,600 --> 00:16:00,600
So I think part of this is the Buddha looking to make a point

182
00:16:00,600 --> 00:16:06,600
and to sort of correct something for the king and for this man,

183
00:16:06,600 --> 00:16:08,600
this minister who had the wrong idea.

184
00:16:08,600 --> 00:16:13,600
And I think broader to teach this sort of lesson,

185
00:16:13,600 --> 00:16:18,600
the lesson of how important are our mindsets,

186
00:16:18,600 --> 00:16:27,600
how significant they are, how powerful, how efficacious they are.

187
00:16:27,600 --> 00:16:30,600
And so then he told the king this,

188
00:16:30,600 --> 00:16:32,600
and the king went back and fired the one minister

189
00:16:32,600 --> 00:16:36,600
and promoted the other minister and so on and so on.

190
00:16:36,600 --> 00:16:40,600
And then came back to the Buddha and said, it's amazing.

191
00:16:40,600 --> 00:16:43,600
I had this great arms giving.

192
00:16:43,600 --> 00:16:46,600
This was just the pinnacle of my goodness,

193
00:16:46,600 --> 00:16:51,600
my career in regards to charity.

194
00:16:51,600 --> 00:16:53,600
I'll never do anything like it.

195
00:16:53,600 --> 00:16:56,600
And this silly man ruined it.

196
00:16:56,600 --> 00:17:00,600
He hurt me with that.

197
00:17:00,600 --> 00:17:04,600
He destroyed my opportunity.

198
00:17:04,600 --> 00:17:10,600
Didn't blame the Buddha, which is, I think, charitable of him.

199
00:17:10,600 --> 00:17:15,600
I'm not saying the Buddha, I don't think the Buddha was wrong in this.

200
00:17:15,600 --> 00:17:23,600
But the question is,

201
00:17:23,600 --> 00:17:30,600
and the point is that this was the idea is to teach a lesson.

202
00:17:30,600 --> 00:17:37,600
And so they teach the Buddha says, yes,

203
00:17:37,600 --> 00:17:42,600
this is the way it is.

204
00:17:42,600 --> 00:17:45,600
Foolish people don't appreciate giving.

205
00:17:45,600 --> 00:17:47,600
They don't go, they don't, as a result,

206
00:17:47,600 --> 00:17:49,600
they never go to heaven.

207
00:17:49,600 --> 00:17:54,600
They have a future is not bright.

208
00:17:54,600 --> 00:17:58,600
But wise people rejoice in giving

209
00:17:58,600 --> 00:18:01,600
and appreciate giving,

210
00:18:01,600 --> 00:18:03,600
even giving of others,

211
00:18:03,600 --> 00:18:07,600
and therefore are happy in the hereafter.

212
00:18:07,600 --> 00:18:10,600
That's what the verse says.

213
00:18:10,600 --> 00:18:14,600
So again, the story and the verse are all about giving

214
00:18:14,600 --> 00:18:16,600
and about the goodness of giving.

215
00:18:16,600 --> 00:18:19,600
And that's true, giving is good.

216
00:18:19,600 --> 00:18:25,600
But it behoves us again to go over this again.

217
00:18:25,600 --> 00:18:28,600
And it behoves us to remind ourselves

218
00:18:28,600 --> 00:18:30,600
that that's not the focus of Buddhism.

219
00:18:30,600 --> 00:18:32,600
And there's a danger there as well,

220
00:18:32,600 --> 00:18:38,600
that we as Buddhists, leaders and teachers

221
00:18:38,600 --> 00:18:41,600
get caught up in teaching people to give to us.

222
00:18:41,600 --> 00:18:46,600
I mean, how perverse is that?

223
00:18:46,600 --> 00:18:49,600
It's great and it's true.

224
00:18:49,600 --> 00:18:51,600
That giving is good.

225
00:18:51,600 --> 00:18:54,600
And to teach people to give, that's a good thing.

226
00:18:54,600 --> 00:18:57,600
Even to teach people to give to Buddhist organizations

227
00:18:57,600 --> 00:19:01,600
is technically a good thing.

228
00:19:01,600 --> 00:19:05,600
But let's put it in context.

229
00:19:05,600 --> 00:19:06,600
Giving is good.

230
00:19:06,600 --> 00:19:09,600
It's good for various reasons.

231
00:19:09,600 --> 00:19:11,600
And we're going to put it in context.

232
00:19:11,600 --> 00:19:14,600
Let's talk about why it's good.

233
00:19:14,600 --> 00:19:18,600
On a very base level, it's good for the organization.

234
00:19:18,600 --> 00:19:24,600
It's good for Buddhism. It's a good thing.

235
00:19:24,600 --> 00:19:26,600
If you think Buddhism is a good thing,

236
00:19:26,600 --> 00:19:29,600
then giving to Buddhism and Buddhist organizations

237
00:19:29,600 --> 00:19:31,600
is good.

238
00:19:31,600 --> 00:19:35,600
That's a very base and mundane reasoning.

239
00:19:35,600 --> 00:19:38,600
That doesn't even make it bad

240
00:19:38,600 --> 00:19:40,600
because, hey, I think Buddhism is good,

241
00:19:40,600 --> 00:19:43,600
so I think it's good to support Buddhism.

242
00:19:43,600 --> 00:19:45,600
But that's not why you go to heaven

243
00:19:45,600 --> 00:19:46,600
because you give.

244
00:19:46,600 --> 00:19:51,600
And that's not why giving is really good.

245
00:19:51,600 --> 00:19:57,600
Giving is good for, I can think of three aspects

246
00:19:57,600 --> 00:19:59,600
that are psychologically significant.

247
00:19:59,600 --> 00:20:01,600
And that's really what we're focused on.

248
00:20:01,600 --> 00:20:03,600
And Buddhism focuses on psychology.

249
00:20:03,600 --> 00:20:05,600
That's what meditation is.

250
00:20:05,600 --> 00:20:09,600
It's a sort of dealing with the mind

251
00:20:09,600 --> 00:20:12,600
and working with the mind and the bettering the mind.

252
00:20:12,600 --> 00:20:18,600
The first one is the most basic.

253
00:20:18,600 --> 00:20:24,600
Giving is an act that involves affirming

254
00:20:24,600 --> 00:20:26,600
the goodness of various things.

255
00:20:26,600 --> 00:20:29,600
The goodness, first of all,

256
00:20:29,600 --> 00:20:32,600
of the things that you're giving.

257
00:20:32,600 --> 00:20:35,600
So when you give food, for example,

258
00:20:35,600 --> 00:20:39,600
it's affirming in your mind that food is good.

259
00:20:39,600 --> 00:20:41,600
That might seem simplistic.

260
00:20:41,600 --> 00:20:44,600
I mean, that's certainly not the focus of someone who's giving.

261
00:20:44,600 --> 00:20:46,600
But it's a part of it.

262
00:20:46,600 --> 00:20:50,600
It's an important part of why it's a part.

263
00:20:50,600 --> 00:20:53,600
It's a small part.

264
00:20:53,600 --> 00:20:55,600
Not terribly important, I guess.

265
00:20:55,600 --> 00:21:00,600
But it's a significant part of why giving is good.

266
00:21:00,600 --> 00:21:05,600
Because in your mind, you have an appreciation of these things.

267
00:21:05,600 --> 00:21:09,600
I mean, I don't want to get into a talk about rebirth

268
00:21:09,600 --> 00:21:12,600
and how that works and how karma works from one life to the next.

269
00:21:12,600 --> 00:21:17,600
But there is a psychological impact that affects the mind.

270
00:21:17,600 --> 00:21:20,600
That's going to have far-reaching consequences.

271
00:21:20,600 --> 00:21:27,600
This appreciation and affirmation of the goodness of good things.

272
00:21:27,600 --> 00:21:29,600
And talking about good physical things.

273
00:21:29,600 --> 00:21:33,600
So if you give good food, you're reaffirming

274
00:21:33,600 --> 00:21:37,600
your appreciation of those things.

275
00:21:37,600 --> 00:21:42,600
This affirmation is really a big part of the theory

276
00:21:42,600 --> 00:21:44,600
behind our meditation practice.

277
00:21:44,600 --> 00:21:46,600
Because we do a lot of affirming.

278
00:21:46,600 --> 00:21:51,600
But our affirming is trying to affirm the nature of things.

279
00:21:51,600 --> 00:21:55,600
So we affirm that seeing is seeing.

280
00:21:55,600 --> 00:21:57,600
We affirm that pain is pain.

281
00:21:57,600 --> 00:22:01,600
When we say to ourselves, pain, pain, it's an affirmation.

282
00:22:01,600 --> 00:22:04,600
It's not the same sort of affirmation as giving.

283
00:22:04,600 --> 00:22:08,600
But it's a part of the same process.

284
00:22:08,600 --> 00:22:13,600
And so even just what's most important is for us to learn

285
00:22:13,600 --> 00:22:15,600
how giving works.

286
00:22:15,600 --> 00:22:17,600
Why is giving good?

287
00:22:17,600 --> 00:22:23,600
Not so that we give more, per se, or necessarily.

288
00:22:23,600 --> 00:22:27,600
But so that we understand how the mind works.

289
00:22:27,600 --> 00:22:30,600
It's interesting for us as Buddhists, and it's useful for us

290
00:22:30,600 --> 00:22:34,600
as a meditator to understand why it is.

291
00:22:34,600 --> 00:22:36,600
Or that it is.

292
00:22:36,600 --> 00:22:39,600
That giving is good.

293
00:22:39,600 --> 00:22:43,600
It's good because it affirms first of all a good things.

294
00:22:43,600 --> 00:22:45,600
And that psychologically significant.

295
00:22:45,600 --> 00:22:50,600
A person who is stingy, even in this life,

296
00:22:50,600 --> 00:22:54,600
you'll find that they are less inclined to enjoy

297
00:22:54,600 --> 00:22:57,600
the things that they hold on to.

298
00:22:57,600 --> 00:23:00,600
First of all, because they're always afraid of the loss.

299
00:23:00,600 --> 00:23:05,600
When you enjoy it, you eat good food while you lose money because

300
00:23:05,600 --> 00:23:06,600
of it.

301
00:23:06,600 --> 00:23:10,600
But even more so, just psychologically, they have a warped state of

302
00:23:10,600 --> 00:23:18,600
mind that is afraid or against good things.

303
00:23:18,600 --> 00:23:24,600
And the second thing is it's an appreciation of the recipient.

304
00:23:24,600 --> 00:23:28,600
So when you give to, this is why they say,

305
00:23:28,600 --> 00:23:31,600
giving the Buddhist organizations is a good thing.

306
00:23:31,600 --> 00:23:35,600
It sounds so self-serving, but there's no avoiding it because

307
00:23:35,600 --> 00:23:39,600
it's a good thing because it brings your mind psychologically

308
00:23:39,600 --> 00:23:41,600
closer to the organization.

309
00:23:41,600 --> 00:23:51,600
If you give to, I was going to say, if you give to poor people,

310
00:23:51,600 --> 00:23:55,600
if you give to poor people, there's a different psychology

311
00:23:55,600 --> 00:23:58,600
there, but there could be an argument for bringing you closer

312
00:23:58,600 --> 00:23:59,600
to poor people.

313
00:23:59,600 --> 00:24:00,600
Why?

314
00:24:00,600 --> 00:24:04,600
Because it's not, it's great to give to poor people,

315
00:24:04,600 --> 00:24:11,600
but you're not supporting in all circumstances the best of

316
00:24:11,600 --> 00:24:12,600
society.

317
00:24:12,600 --> 00:24:15,600
So it's a different psychology there.

318
00:24:15,600 --> 00:24:18,600
But the psychology behind giving to a Buddhist organization

319
00:24:18,600 --> 00:24:21,600
is the appreciation of the organization.

320
00:24:21,600 --> 00:24:25,600
It could also mean because, oh, you feel sorry for these Buddhist

321
00:24:25,600 --> 00:24:27,600
monks who live off in the forest.

322
00:24:27,600 --> 00:24:29,600
I have had that happen.

323
00:24:29,600 --> 00:24:31,600
People who felt sorry for me and therefore gave me food,

324
00:24:31,600 --> 00:24:33,600
but that's not usually why we give.

325
00:24:33,600 --> 00:24:35,600
Why we support?

326
00:24:35,600 --> 00:24:38,600
It's not why we would support a university, for example,

327
00:24:38,600 --> 00:24:40,600
or a student.

328
00:24:40,600 --> 00:24:44,600
People don't give scholarships because they feel sorry for the

329
00:24:44,600 --> 00:24:48,600
students, usually. They create scholarship funds because of

330
00:24:48,600 --> 00:24:52,600
their belief and their appreciation of those things.

331
00:24:52,600 --> 00:24:56,600
And we generally have this idea that, yes, this is real.

332
00:24:56,600 --> 00:24:59,600
This is psychologically significant.

333
00:24:59,600 --> 00:25:01,600
But maybe don't take the time.

334
00:25:01,600 --> 00:25:06,600
I don't think in a mundane setting or a secular setting.

335
00:25:06,600 --> 00:25:10,600
We don't generally take the time to appreciate just exactly

336
00:25:10,600 --> 00:25:15,600
how psychologically significant it is to support something.

337
00:25:15,600 --> 00:25:18,600
If you support Buddhism, of course, it brings your mind closer

338
00:25:18,600 --> 00:25:22,600
to it reaffirms in your mind that Buddhism is good.

339
00:25:22,600 --> 00:25:26,600
Why would you give the other reasons feeling sorry for

340
00:25:26,600 --> 00:25:29,600
poor people feeling sorry for monks?

341
00:25:29,600 --> 00:25:34,600
But the reason you give in this case is out of appreciation.

342
00:25:34,600 --> 00:25:39,600
And that appreciation is an affirmation in your mind.

343
00:25:39,600 --> 00:25:46,600
So what that does is keep you close to good things if you give

344
00:25:46,600 --> 00:25:47,600
to good places.

345
00:25:47,600 --> 00:25:55,600
If you give to corrupt and perverse organizations,

346
00:25:55,600 --> 00:25:57,600
then that's an appreciation of those things.

347
00:25:57,600 --> 00:26:01,600
And so it has those consequences.

348
00:26:01,600 --> 00:26:04,600
So giving to Buddhism is good, particularly because Buddhism

349
00:26:04,600 --> 00:26:05,600
is a good thing.

350
00:26:05,600 --> 00:26:09,600
So if you disagree, well, then you probably won't give anything

351
00:26:09,600 --> 00:26:11,600
to Buddhist organizations.

352
00:26:11,600 --> 00:26:19,600
But I would say the problem there is the lack of appreciation

353
00:26:19,600 --> 00:26:22,600
for something that is good, a person who gives.

354
00:26:22,600 --> 00:26:26,600
Doing good for themselves because they appreciate something good.

355
00:26:26,600 --> 00:26:27,600
It's the second one.

356
00:26:27,600 --> 00:26:29,600
The third aspect of giving.

357
00:26:29,600 --> 00:26:32,600
But I think for all of these are significant,

358
00:26:32,600 --> 00:26:37,600
but this one is most significant, I think for meditative purposes.

359
00:26:37,600 --> 00:26:40,600
Is that giving is about also giving up.

360
00:26:40,600 --> 00:26:45,600
It involves an appreciation of contentment or an affirmation

361
00:26:45,600 --> 00:26:48,600
of the rightness of letting go.

362
00:26:48,600 --> 00:26:52,600
Let me see because in order to give something you have to let it go.

363
00:26:52,600 --> 00:26:55,600
If you give something that is worthless to you,

364
00:26:55,600 --> 00:26:58,600
it's of course far less significant.

365
00:26:58,600 --> 00:27:02,600
And it's far less significant for this reason.

366
00:27:02,600 --> 00:27:06,600
That when you give something that has value to you,

367
00:27:06,600 --> 00:27:09,600
you're flipping the table, so to speak,

368
00:27:09,600 --> 00:27:14,600
you're changing from the mind that clings,

369
00:27:14,600 --> 00:27:19,600
that wants, that is happy to have

370
00:27:19,600 --> 00:27:22,600
to the mind that is happy without.

371
00:27:22,600 --> 00:27:26,600
The mind that is free from clinging.

372
00:27:26,600 --> 00:27:30,600
And so it's a mundane act, but it's significant.

373
00:27:30,600 --> 00:27:35,600
Giving is particularly useful for this reason.

374
00:27:35,600 --> 00:27:37,600
Out of all other good deeds.

375
00:27:37,600 --> 00:27:40,600
It's particularly useful because it involves letting go.

376
00:27:40,600 --> 00:27:42,600
So it doesn't have to be physical things,

377
00:27:42,600 --> 00:27:48,600
but even giving physical things is symbolically significant

378
00:27:48,600 --> 00:27:53,600
from a psychological point of view because it involves letting go.

379
00:27:53,600 --> 00:27:57,600
You directly have to let go in order to give,

380
00:27:57,600 --> 00:27:59,600
and that becomes habit forming.

381
00:27:59,600 --> 00:28:03,600
If you give enough, why it's supportive of our meditation is

382
00:28:03,600 --> 00:28:10,600
because it makes it easier and more familiar to us

383
00:28:10,600 --> 00:28:12,600
to let go of the things that we really like.

384
00:28:12,600 --> 00:28:17,600
So when we start to see that they're not maybe as

385
00:28:17,600 --> 00:28:20,600
worth clinging as we thought,

386
00:28:20,600 --> 00:28:23,600
they're more easily, more easily able to let them go.

387
00:28:23,600 --> 00:28:29,600
We've already worked on a conventional level on our attachment.

388
00:28:32,600 --> 00:28:38,600
So why stingy people and people who hate the idea of giving,

389
00:28:38,600 --> 00:28:40,600
why they don't go to heaven?

390
00:28:40,600 --> 00:28:42,600
This is why their minds are corrupt,

391
00:28:42,600 --> 00:28:49,600
their minds are shrunken and miserable really.

392
00:28:49,600 --> 00:28:53,600
And there's no denying that a person who is generous and kind,

393
00:28:53,600 --> 00:29:00,600
their mind is bright, light, happy, peaceful, content,

394
00:29:00,600 --> 00:29:04,600
content because they don't need their attitude,

395
00:29:04,600 --> 00:29:10,600
their behavior, their outlook on life is one of letting go.

396
00:29:10,600 --> 00:29:12,600
One of giving up.

397
00:29:12,600 --> 00:29:14,600
It's much more in line with that.

398
00:29:14,600 --> 00:29:16,600
And so they become freer,

399
00:29:16,600 --> 00:29:19,600
and they go on to be born in better places.

400
00:29:22,600 --> 00:29:24,600
That's all interesting to us.

401
00:29:24,600 --> 00:29:27,600
It's interesting because it encourages us to give,

402
00:29:27,600 --> 00:29:29,600
which is again a support for meditation,

403
00:29:29,600 --> 00:29:31,600
but it's even more interesting because it involves

404
00:29:31,600 --> 00:29:34,600
an understanding of how the mind works.

405
00:29:34,600 --> 00:29:40,600
That our behaviors, our attitudes, our actions,

406
00:29:40,600 --> 00:29:42,600
our outlooks,

407
00:29:42,600 --> 00:29:49,600
these all have a lasting effect and they accumulate.

408
00:29:49,600 --> 00:29:51,600
And they become habitual.

409
00:29:51,600 --> 00:29:55,600
They become really a part of who we are.

410
00:29:55,600 --> 00:29:58,600
And this is what we're seeing in meditation,

411
00:29:58,600 --> 00:30:03,600
as we try and be more objective in a firm objectivity.

412
00:30:03,600 --> 00:30:07,600
We also see all of our partiality,

413
00:30:07,600 --> 00:30:10,600
our lightness and dislikes.

414
00:30:10,600 --> 00:30:12,600
And we see all of these things.

415
00:30:12,600 --> 00:30:15,600
We see how we are stingy, jealous,

416
00:30:15,600 --> 00:30:21,600
conceited, how we are generous, humble.

417
00:30:21,600 --> 00:30:23,600
And we learn all these things.

418
00:30:23,600 --> 00:30:25,600
We learn about how much more peaceful and happy it is

419
00:30:25,600 --> 00:30:27,600
to give and to give up,

420
00:30:27,600 --> 00:30:34,600
and how stressful it is to cling and to hold on.

421
00:30:34,600 --> 00:30:39,600
So again, this verse is a very simple verse about giving,

422
00:30:39,600 --> 00:30:45,600
but it certainly is fodder for deeper conversation

423
00:30:45,600 --> 00:31:10,600
about giving up and letting go.

