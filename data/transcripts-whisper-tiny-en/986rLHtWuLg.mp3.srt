1
00:00:00,000 --> 00:00:05,000
What causes a person to have a pleasant or painful experience?

2
00:00:05,000 --> 00:00:11,000
According to the Buddha, if we hold the view that it is all caused by what we have done in the past,

3
00:00:11,000 --> 00:00:15,000
or it is being done by a god, it has considered wrong view.

4
00:00:15,000 --> 00:00:18,000
What is the right view?

5
00:00:18,000 --> 00:00:24,000
It's a tough one because definitely a lot of it is the past.

6
00:00:24,000 --> 00:00:28,000
But karma is both past and present.

7
00:00:28,000 --> 00:00:41,000
So the point is it's not all karma.

8
00:00:41,000 --> 00:00:45,000
You see, because there is what you have done, and then there is the circumstance.

9
00:00:45,000 --> 00:00:46,000
It is the people around you.

10
00:00:46,000 --> 00:00:50,000
So you have past karma that you have done, and that is caused people to hate you.

11
00:00:50,000 --> 00:00:54,000
So people hate you, and therefore they are causing you to suffer.

12
00:00:54,000 --> 00:01:00,000
But the fact that they are perpetuating it in the present is a part of that.

13
00:01:00,000 --> 00:01:07,000
So they can still choose to follow their hatred or to acknowledge it, be mindful, let go of it.

14
00:01:07,000 --> 00:01:16,000
And so as an example, this is a part of the cause for you to feel pain in that case.

15
00:01:16,000 --> 00:01:19,000
So if people love you, that's your past karma.

16
00:01:19,000 --> 00:01:26,000
That you have the good things that you have done, but they may hold out, or things may happen in their lives.

17
00:01:26,000 --> 00:01:32,000
That change the way they behave, the way they perceive you, and so on.

18
00:01:32,000 --> 00:01:37,000
And that affects whether you experience pleasure or pain from them.

19
00:01:37,000 --> 00:01:39,000
So karma is not everything.

20
00:01:39,000 --> 00:01:40,000
That's what the Buddha said.

21
00:01:40,000 --> 00:01:43,000
And so there is someone who held this view that karma was everything.

22
00:01:43,000 --> 00:01:45,000
And then it's like past karma is everything else.

23
00:01:45,000 --> 00:01:54,000
It's not possible to change. It's not possible to become enlightened, or it's not reasonable to practice.

24
00:01:54,000 --> 00:01:56,000
There's no point in practicing.

25
00:01:56,000 --> 00:01:58,000
It's a bad view to hold.

26
00:01:58,000 --> 00:02:05,000
I mean, one way of looking at it is the idea of what's true and what's not true isn't the most important thing.

27
00:02:05,000 --> 00:02:09,000
The most important thing is what is useful, what is practical.

28
00:02:09,000 --> 00:02:16,000
So even if it's hard to get your mind around, but think of it like this, even if it were true that we had no free will,

29
00:02:16,000 --> 00:02:19,000
and it was all determined by the past, say, which the Buddha denied.

30
00:02:19,000 --> 00:02:24,000
But suppose that were the case, it still would be a bad idea to hold such a view.

31
00:02:24,000 --> 00:02:26,000
It's ironic, you see.

32
00:02:26,000 --> 00:02:32,000
If anyone who held that view would be in trouble, because they would be disinclined to benefit, to better themselves.

33
00:02:32,000 --> 00:02:38,000
And so that such a person would be more likely to suffer than a person who held the view that we can change,

34
00:02:38,000 --> 00:02:40,000
that we can benefit ourselves.

35
00:02:40,000 --> 00:02:45,000
You see, reality according to Buddhism is a bit of a slippery thing, I would say,

36
00:02:45,000 --> 00:02:49,000
because we don't go that step of creating theories.

37
00:02:49,000 --> 00:02:53,000
I've talked about this before the idea of free will determinism.

38
00:02:53,000 --> 00:03:01,000
It takes an assumption of a underlying universe outside of our experience,

39
00:03:01,000 --> 00:03:12,000
which can support the idea of a being who has free will,

40
00:03:12,000 --> 00:03:22,000
or a being who is deterministic, or physical realities, or physical entities, quantum,

41
00:03:22,000 --> 00:03:25,000
that are deterministic.

42
00:03:25,000 --> 00:03:34,000
So, since Buddhism doesn't go for that, we're better able to appreciate and deal with reality.

43
00:03:34,000 --> 00:03:36,000
How to act.

44
00:03:36,000 --> 00:03:39,000
So, because a person who is deterministic can't say, well, I'm going to better myself,

45
00:03:39,000 --> 00:03:43,000
because they're not really going to better themselves.

46
00:03:43,000 --> 00:03:50,000
And they may better themselves, but they'll do so thinking that it's deterministic.

47
00:03:50,000 --> 00:03:58,000
And I think, definitely, there's a danger for these people to become complacent,

48
00:03:58,000 --> 00:04:01,000
and to be less inclined to better themselves.

49
00:04:01,000 --> 00:04:04,000
More inclined to, I mean, there's something good about it,

50
00:04:04,000 --> 00:04:09,000
because more inclined to accept their faults, so less inclined to blame themselves.

51
00:04:09,000 --> 00:04:13,000
So, something there, a person who believes in free will has similar problems.

52
00:04:13,000 --> 00:04:17,000
They tend to blame themselves to hate themselves, to feel frustrated

53
00:04:17,000 --> 00:04:24,000
when they can't change certain things, but this is why Buddhism does away with both of those.

54
00:04:24,000 --> 00:04:28,000
So, what causes, to directly answer your question in brief,

55
00:04:28,000 --> 00:04:31,000
what causes a person to have these experiences?

56
00:04:31,000 --> 00:04:36,000
Well, yes, so karma is a big part of it, but a lot of it is past karma,

57
00:04:36,000 --> 00:04:39,000
and a lot of it is present karma.

58
00:04:39,000 --> 00:04:50,000
But it seems like there's also some vaguely defined processes like natural physical processes.

59
00:04:50,000 --> 00:04:55,000
Maybe even you could say quantum processes, maybe even random elements.

60
00:04:55,000 --> 00:05:02,000
I don't know if that's even valid, but the point is not everything is karma.

61
00:05:02,000 --> 00:05:06,000
What you can say is a lot of it is karma, past karma,

62
00:05:06,000 --> 00:05:09,000
and a lot of it is present karma, so that's where we should watch out,

63
00:05:09,000 --> 00:05:12,000
because obviously the rest of it is out of our control.

64
00:05:12,000 --> 00:05:17,000
We should cultivate those actions that are actually going to benefit us in the present,

65
00:05:17,000 --> 00:05:23,000
and be forgiving of our own past and of the past of others,

66
00:05:23,000 --> 00:05:31,000
so be forgiving of the past and be able to let go and bear with the results of our bad past karma.

67
00:05:31,000 --> 00:05:36,000
So not to become upset when bad things happen to us.

68
00:05:36,000 --> 00:05:40,000
We should understand that there's various factors that we have to take into account.

69
00:05:40,000 --> 00:05:45,000
Those we can affect and those we cannot change.

70
00:05:45,000 --> 00:06:09,000
I think that's...

