1
00:00:00,000 --> 00:00:20,200
Hello, today I'll be talking about one of the most crucial aspects of the Buddha's teaching.

2
00:00:20,200 --> 00:00:31,880
This is the quality of the Buddha's teaching that is one of cause and effect that the

3
00:00:31,880 --> 00:00:41,400
teaching of the Buddha has the quality of being true to the nature of how things arise

4
00:00:41,400 --> 00:00:49,640
and how things cease, that everything that arises comes from a cause and the teachings of

5
00:00:49,640 --> 00:00:57,800
the Buddha are able to describe the actual cause that gives rise to the state of affairs

6
00:00:57,800 --> 00:01:00,520
that we find ourselves in.

7
00:01:00,520 --> 00:01:10,680
And also the effects that come from the way that we respond and interact with the world

8
00:01:10,680 --> 00:01:12,120
around us.

9
00:01:12,120 --> 00:01:21,880
So our ability to understand why it is that we are the way we are and what consequences

10
00:01:21,880 --> 00:01:26,840
will come from our actions and from our state of mind.

11
00:01:26,840 --> 00:01:34,200
This is the Buddha's teaching on dependent origination or Patitya Samupada, which says

12
00:01:34,200 --> 00:01:44,440
that all of our happiness and suffering, all of the difficulties and upsets, stress and

13
00:01:44,440 --> 00:01:50,680
dissatisfaction that we meet with in the world, as well as all of the peace and happiness

14
00:01:50,680 --> 00:01:59,920
and well-being that comes, has a cause and the teaching of the Buddha is able to describe

15
00:01:59,920 --> 00:02:09,200
this cause, the teaching of dependent origination is the perfect method for us to understand

16
00:02:09,200 --> 00:02:17,080
and to thereby adjust and overcome the causes of suffering and thereby overcome all of

17
00:02:17,080 --> 00:02:26,720
suffering, any type of suffering and stress that might come in the future.

18
00:02:26,720 --> 00:02:35,520
The teaching of dependent origination in the Buddha is best seen as a practical teaching.

19
00:02:35,520 --> 00:02:40,880
Often when people approach the teachings of the Buddha, they will, as I've said before,

20
00:02:40,880 --> 00:02:46,600
look at it from an intellectual point of view, trying to understand it logically to think

21
00:02:46,600 --> 00:02:56,640
of examples by which they can understand and accept and agree with the teachings

22
00:02:56,640 --> 00:03:03,080
as they're presented, ways of explaining and ways of assimilating it with their own view

23
00:03:03,080 --> 00:03:05,640
and their own understanding of reality.

24
00:03:05,640 --> 00:03:15,160
But this is not really the most useful beneficial and proper way to approach the Buddha's

25
00:03:15,160 --> 00:03:19,320
teaching, most especially the Buddha's teaching on cause and effect, because this teaching

26
00:03:19,320 --> 00:03:25,680
is something that can be seen, can be understood based on our experience of reality,

27
00:03:25,680 --> 00:03:29,840
most especially our experience in the meditation practice.

28
00:03:29,840 --> 00:03:37,080
So when we undertake to practice meditation or in our daily lives, when we encounter difficulties

29
00:03:37,080 --> 00:03:42,720
in problems, when we encounter situations that give rise to one things and desires or

30
00:03:42,720 --> 00:03:54,640
aversions and dislikes that give rise to our conceit and detachment and delusions, jealousy and

31
00:03:54,640 --> 00:04:01,360
being so on, that we are able to see this process in action.

32
00:04:01,360 --> 00:04:09,920
We should be able to see the nature of reality in terms of cause and effect that when

33
00:04:09,920 --> 00:04:12,840
this arises, that will follow.

34
00:04:12,840 --> 00:04:20,080
When this doesn't arise, if you're able to somehow give up the cause, give up the

35
00:04:20,080 --> 00:04:25,840
behavior that is causing the problem, then the problem has no possible way of arising.

36
00:04:25,840 --> 00:04:32,240
So it says that reality is really functions both physically and mentally on a scientific

37
00:04:32,240 --> 00:04:42,560
basis, that nothing arises by chance or by magic or by any supernatural means, that

38
00:04:42,560 --> 00:04:50,120
there is no possible way for any phenomenon to arise without its cause, without the appropriate

39
00:04:50,120 --> 00:04:51,760
cause.

40
00:04:51,760 --> 00:04:58,560
And so in this way, once we understand the causes, we understand the relationship between

41
00:04:58,560 --> 00:05:05,520
phenomena, between suffering and unwholesome states and between happiness and wholesome

42
00:05:05,520 --> 00:05:10,160
states, between those states that lead to suffering and the suffering and between those

43
00:05:10,160 --> 00:05:17,400
states that lead to happiness and the happiness, then our mind will naturally incline

44
00:05:17,400 --> 00:05:23,480
towards the development of those deans and those mind states that lead to happiness because

45
00:05:23,480 --> 00:05:26,040
there's no one in the world that wants to suffer.

46
00:05:26,040 --> 00:05:32,120
The problem is not that we want to hurt ourselves, intrinsically all beings are always looking

47
00:05:32,120 --> 00:05:39,480
for that which is pleasant or that which is peaceful, that which is true happiness.

48
00:05:39,480 --> 00:05:44,840
The problem is that we don't understand the nature of cause and effect, we do certain things

49
00:05:44,840 --> 00:05:51,240
and we create certain mind states thinking that this is going to somehow lead to our benefit

50
00:05:51,240 --> 00:05:56,480
when in fact, those things that we create, those states of mind and those actions are

51
00:05:56,480 --> 00:06:00,080
only for our detriment, our cause for our suffering.

52
00:06:00,080 --> 00:06:09,200
And so in simply by not understanding, by this ignorance and delusion, we create states

53
00:06:09,200 --> 00:06:16,280
that are contradictory to our purpose, where we want to be happy and we're actually

54
00:06:16,280 --> 00:06:18,800
causing ourselves suffering.

55
00:06:18,800 --> 00:06:22,600
And this is how the Buddha's teaching on dependent origination starts.

56
00:06:22,600 --> 00:06:28,320
The first part of the teaching is that ignorance creates formations.

57
00:06:28,320 --> 00:06:35,680
It is due to ignorance that our mind gives rise to its mental formations or gives rise

58
00:06:35,680 --> 00:06:42,960
to concepts, it conceives things, it gives rise to ideas and all sorts of intentions and

59
00:06:42,960 --> 00:06:49,800
volitions and the very root of karma of all of our ethical action, both ethical and

60
00:06:49,800 --> 00:06:53,680
unethical, simply due to our ignorance.

61
00:06:53,680 --> 00:06:59,600
If we understood that these formations, these conceptions, these intentions, we're going to

62
00:06:59,600 --> 00:07:02,760
cause our suffering, we wouldn't give rise to them.

63
00:07:02,760 --> 00:07:10,400
If we didn't have our ignorance, if we understood that there was no way to find happiness

64
00:07:10,400 --> 00:07:19,400
in this way that even creating stable lifestyles where we have a nice car and a house and

65
00:07:19,400 --> 00:07:23,800
a job and all these things, if we understood that this wasn't able to bring us happiness,

66
00:07:23,800 --> 00:07:26,480
even these good things, then we wouldn't strive for them.

67
00:07:26,480 --> 00:07:31,680
The intention wouldn't arise to find these things, to cling, to attach even to people

68
00:07:31,680 --> 00:07:33,880
or to places or to things.

69
00:07:33,880 --> 00:07:36,800
If we understood reality, we wouldn't cling to these things.

70
00:07:36,800 --> 00:07:41,280
We wouldn't even give rise to those ethical acts that were for the benefit of giving

71
00:07:41,280 --> 00:07:50,040
or creating pleasant circumstances where we had good friends and good food and good society

72
00:07:50,040 --> 00:07:51,040
and so on.

73
00:07:51,040 --> 00:07:54,920
We wouldn't even think to give rise to those intentions.

74
00:07:54,920 --> 00:08:00,400
We would be content and at peace with ourselves, we would have no need for anything.

75
00:08:00,400 --> 00:08:09,960
We wouldn't strive if we understood that striving, this giving rise to intention or the

76
00:08:09,960 --> 00:08:16,840
desire to create, the desire to destroy, the desire to change, to force, to build up some

77
00:08:16,840 --> 00:08:23,840
concept of me, of identity in the world.

78
00:08:23,840 --> 00:08:28,280
If we understood that this was suffering, we wouldn't give rise to it.

79
00:08:28,280 --> 00:08:35,560
So this is incredibly powerful teaching, because normally we would think that without

80
00:08:35,560 --> 00:08:41,000
this intention nothing happens, without this intention no good can come from your life.

81
00:08:41,000 --> 00:08:47,880
But really the ultimate truth of reality is that it is what it is.

82
00:08:47,880 --> 00:08:51,760
It arises and its reality comes and goes.

83
00:08:51,760 --> 00:08:56,040
And there is no one thing in the world that can truly make you happy in that peace.

84
00:08:56,040 --> 00:08:59,360
There is nothing that you can create that won't be destroyed.

85
00:08:59,360 --> 00:09:02,200
There is nothing that you can build up that won't fall apart.

86
00:09:02,200 --> 00:09:05,760
There is nothing that can truly make you happy or satisfied.

87
00:09:05,760 --> 00:09:12,920
If you can't find happiness in peace as you are, as things are in all of reality, then

88
00:09:12,920 --> 00:09:19,400
you'll inevitably fall into suffering and disappointment when things change and inevitably

89
00:09:19,400 --> 00:09:22,000
go against your wishes.

90
00:09:22,000 --> 00:09:29,600
So once we give rise to understanding, if we understand reality, the point is that we will

91
00:09:29,600 --> 00:09:34,840
never have any wishes or any hopes or any desires, because we are truly happy.

92
00:09:34,840 --> 00:09:36,280
We will never want for anything.

93
00:09:36,280 --> 00:09:42,240
We will never hope for anything or wish for anything, because we are content the way things

94
00:09:42,240 --> 00:09:43,240
are.

95
00:09:43,240 --> 00:09:45,200
We are happy with reality as it is.

96
00:09:45,200 --> 00:09:51,560
And this is really the key not to create anything that is inevitably going to fall apart

97
00:09:51,560 --> 00:09:59,080
or disappear, but to be content with whatever comes, whatever should arise.

98
00:09:59,080 --> 00:10:05,480
So this is the key principle in Buddhism that understanding sets you free.

99
00:10:05,480 --> 00:10:11,200
It's not attaining anything, it's not vowing for anything, it's not creating anything.

100
00:10:11,200 --> 00:10:14,760
Simply understanding things as they are will set you free.

101
00:10:14,760 --> 00:10:23,120
And this is why insight meditation or meditation where one contemplates reality is so crucial.

102
00:10:23,120 --> 00:10:27,960
Simply by watching reality, you change your whole way of looking and you change your whole

103
00:10:27,960 --> 00:10:32,480
way of being and you change your whole universe so that nothing can bring you suffering.

104
00:10:32,480 --> 00:10:33,880
This is the key.

105
00:10:33,880 --> 00:10:38,200
Many people begin to practice meditation thinking that they are going to attain or create

106
00:10:38,200 --> 00:10:45,480
or perceive or experience something special that is not going to fall apart, that is not

107
00:10:45,480 --> 00:10:50,880
going to disappear, that is somehow going to make them satisfied.

108
00:10:50,880 --> 00:10:55,440
And as a result, they are dissatisfied with the meditation when they realize that there's

109
00:10:55,440 --> 00:10:58,160
nothing that is going to satisfy them.

110
00:10:58,160 --> 00:11:04,200
And this is an important point for us to understand that the meditation is not for building

111
00:11:04,200 --> 00:11:10,920
up, it's for letting go, for giving up and for accepting and being at peace and at harmony

112
00:11:10,920 --> 00:11:14,800
with things as they are, just simply understanding.

113
00:11:14,800 --> 00:11:19,680
So when we practice meditation, say watching the breath, the stomach when it rises and falls

114
00:11:19,680 --> 00:11:24,320
or watching our feet move when we walk or watching any part of reality, simply being

115
00:11:24,320 --> 00:11:30,400
aware that we're standing or sitting, being aware of pains and aches in the body, as I've

116
00:11:30,400 --> 00:11:38,160
said before, when you feel pain, say, pain, pain, simply seeing it for what it is.

117
00:11:38,160 --> 00:11:42,000
You accomplish the goal of the Buddha's teaching.

118
00:11:42,000 --> 00:11:45,520
You don't have to create anything, you don't have to change anything, you don't have

119
00:11:45,520 --> 00:11:48,600
to get rid of the pains and the aches, you don't have to get rid of the thoughts and

120
00:11:48,600 --> 00:11:49,600
the mind.

121
00:11:49,600 --> 00:11:52,720
But simply saying to yourself, it is what it is.

122
00:11:52,720 --> 00:11:58,640
This is pain, this is thought, this is emotion, this is movements of the body when we're

123
00:11:58,640 --> 00:12:02,240
walking, saying walking, walking, those, or so on.

124
00:12:02,240 --> 00:12:08,360
Simply reminding yourself and creating this clear awareness of the phenomenon as it is is

125
00:12:08,360 --> 00:12:10,640
what it freezes from suffering.

126
00:12:10,640 --> 00:12:16,880
So this is the core of the Buddha's teaching on dependent origination that ignorance leads

127
00:12:16,880 --> 00:12:18,280
to formations.

128
00:12:18,280 --> 00:12:23,200
It's actually a good summary of the whole of the dependent origination because that's

129
00:12:23,200 --> 00:12:24,720
really how it works.

130
00:12:24,720 --> 00:12:29,160
Our ignorance gives rise to formation and those formations in turn, we're ignorant

131
00:12:29,160 --> 00:12:33,080
about them thinking that somehow they're going to satisfy us and so we give rise to

132
00:12:33,080 --> 00:12:36,480
more and more of these formations again and again.

133
00:12:36,480 --> 00:12:44,200
But the next part of the cycle goes into great detail about how this works because those

134
00:12:44,200 --> 00:12:50,200
formations are what give rise to our lives, they give rise to our birth, our becoming.

135
00:12:50,200 --> 00:12:56,960
Just based on this, that when we pass away from one life, we create a new life, we cling

136
00:12:56,960 --> 00:13:03,960
and we develop this existence that we see in front of us, that we have a brain and we

137
00:13:03,960 --> 00:13:09,800
have a body where we have a world around us, we create this again and again and again.

138
00:13:09,800 --> 00:13:11,840
And this gives rise to consciousness.

139
00:13:11,840 --> 00:13:19,320
So the Buddha said, these formations give rise to our conscious awareness in this life.

140
00:13:19,320 --> 00:13:22,440
This is the next link.

141
00:13:22,440 --> 00:13:28,400
So when we're first born there arises a consciousness say in the womb of our mother and

142
00:13:28,400 --> 00:13:37,080
from that moment on, we will see the cause and effect arising.

143
00:13:37,080 --> 00:13:40,160
This is the detailed exposition.

144
00:13:40,160 --> 00:13:45,000
So the first part is just talking about life after life after life where ignorance gives

145
00:13:45,000 --> 00:13:47,800
rise to our intentions to do this or that.

146
00:13:47,800 --> 00:13:53,240
Now when you get into a single life, then we start with the first moment that has been

147
00:13:53,240 --> 00:13:56,120
created, this consciousness.

148
00:13:56,120 --> 00:14:00,960
And through our whole life, it's going to be this consciousness that is most important.

149
00:14:00,960 --> 00:14:09,120
So in the Buddha's teaching, the mind is the most important aspect and I think it's

150
00:14:09,120 --> 00:14:15,720
easy to understand based on talking about how our misunderstandings are what creates suffering.

151
00:14:15,720 --> 00:14:21,360
So when we understand how ignorance creates our intentions, we can understand how important

152
00:14:21,360 --> 00:14:22,360
the mind is.

153
00:14:22,360 --> 00:14:23,760
So this is the beginning.

154
00:14:23,760 --> 00:14:24,760
How does it work?

155
00:14:24,760 --> 00:14:29,760
The mind, the consciousness gives rise to our experience of reality.

156
00:14:29,760 --> 00:14:32,880
We experience the physical and we experience the mental.

157
00:14:32,880 --> 00:14:40,680
So it gives rise to this psychophysical or mental physical reality.

158
00:14:40,680 --> 00:14:45,640
And since the experience of the stomach, when we're breathing, when the stomach rises and

159
00:14:45,640 --> 00:14:48,640
the stomach falls, there's the physical and the mental.

160
00:14:48,640 --> 00:14:53,080
The rising is the physical and the knowing of the rising is the mental.

161
00:14:53,080 --> 00:14:55,680
When we walk, there's the foot moving is the physical.

162
00:14:55,680 --> 00:14:57,400
The mind knowing it is the mental.

163
00:14:57,400 --> 00:15:03,360
When we feel pain, there's the physical experience and there's the mind that knows it and

164
00:15:03,360 --> 00:15:07,280
doesn't like it and so on and says that's painful.

165
00:15:07,280 --> 00:15:16,600
And so on, the whole of our lives thus circle around in these with these two realities,

166
00:15:16,600 --> 00:15:23,440
the mind and the experience of the objects around us.

167
00:15:23,440 --> 00:15:27,960
Now this in and of itself isn't really a problem, obviously we have to experience, we have

168
00:15:27,960 --> 00:15:30,840
to live our lives and there's no suffering that comes from this.

169
00:15:30,840 --> 00:15:34,040
The suffering doesn't come from the things that we experience.

170
00:15:34,040 --> 00:15:37,520
The suffering, as I said, comes from our misunderstanding.

171
00:15:37,520 --> 00:15:45,800
Once we have the psychophysical matrix, the physical and the mental aspects of reality,

172
00:15:45,800 --> 00:15:51,120
the experiences of the body and the mind, then we get the six senses.

173
00:15:51,120 --> 00:15:56,200
So the next link is once you have the physical and the mental, then you're going to have

174
00:15:56,200 --> 00:15:57,200
sensations.

175
00:15:57,200 --> 00:16:01,440
You're going to have the seeing, the hearings, smelling, tasting, feeling, thinking.

176
00:16:01,440 --> 00:16:08,320
So this is where the physical and the mental arise, they arise at the eye, the eye itself

177
00:16:08,320 --> 00:16:15,040
is physical, the light touching the eye is physical, the mind that knows the seeing is

178
00:16:15,040 --> 00:16:22,400
mental, hearing the sound in the ear or physical and the knowing of the sound or the hearing

179
00:16:22,400 --> 00:16:24,520
is mental and so on.

180
00:16:24,520 --> 00:16:28,960
So the six senses are our experience of reality.

181
00:16:28,960 --> 00:16:32,600
So here we have through, we have the consciousness, which is aware of the physical and the

182
00:16:32,600 --> 00:16:35,800
mental realities at the six senses.

183
00:16:35,800 --> 00:16:40,160
So knowing the mental is in this case, knowing the mind.

184
00:16:40,160 --> 00:16:46,680
So the mind is aware of the thoughts, it's aware of the aspects of the mind, the formations

185
00:16:46,680 --> 00:16:50,520
that arise in the mind.

186
00:16:50,520 --> 00:16:54,880
And once you have the six senses, then you have contact.

187
00:16:54,880 --> 00:17:03,280
Now these four together make up the neutral aspects, sorry, these four along with the

188
00:17:03,280 --> 00:17:08,760
next one, which is feeling or sensation.

189
00:17:08,760 --> 00:17:16,640
So once you have consciousness, then you will have the mental and physical experience

190
00:17:16,640 --> 00:17:24,160
at the six senses because of the contact, the contact between the mind and the physical.

191
00:17:24,160 --> 00:17:28,200
And the mind goes out to the body when the mind goes out to the eye, the ear, the nose,

192
00:17:28,200 --> 00:17:33,800
the tongue or when the mind goes out to the thoughts, then there is contact.

193
00:17:33,800 --> 00:17:39,200
Once there is contact, then there arises sensation, there arises pleasant sensation,

194
00:17:39,200 --> 00:17:43,080
an unpleasant sensation or a neutral sensation.

195
00:17:43,080 --> 00:17:48,200
These five aspects of reality are the neutral aspect of reality.

196
00:17:48,200 --> 00:17:56,640
Our whole life would be fine if this is all we had and thus these five are the most important

197
00:17:56,640 --> 00:18:03,560
aspects of reality for us to examine and to analyze.

198
00:18:03,560 --> 00:18:08,080
Once we come to understand these clearly, to see them for what they are and to not go

199
00:18:08,080 --> 00:18:11,480
the next step, which is going to get us into trouble.

200
00:18:11,480 --> 00:18:15,320
Simply knowing when we feel pain, knowing when we feel happy, knowing when we see, knowing

201
00:18:15,320 --> 00:18:18,360
when we hear, when we see something, we know it as seeing.

202
00:18:18,360 --> 00:18:21,840
So we say to ourselves, seeing, seeing, when we hear something, we know it as hearing,

203
00:18:21,840 --> 00:18:24,520
we say to ourselves, hearing, hearing.

204
00:18:24,520 --> 00:18:28,640
When we feel pain, we know it as pain, we say to ourselves, pain, pain.

205
00:18:28,640 --> 00:18:34,280
Just reminding ourselves, it's just simply pain without attaching to it.

206
00:18:34,280 --> 00:18:37,040
No problem would arise for us if we were able to do this.

207
00:18:37,040 --> 00:18:42,520
The problem is that once we, if we're unable to understand reality in this way, we're

208
00:18:42,520 --> 00:18:44,760
going to give rise to craving.

209
00:18:44,760 --> 00:18:49,400
Because of the ignorance that exists in our minds, our inability to see these things as

210
00:18:49,400 --> 00:18:56,760
they are and to see them simply as arising and ceasing reality, then we're going to give

211
00:18:56,760 --> 00:18:58,360
rise to craving.

212
00:18:58,360 --> 00:19:00,600
We will have this intention arise.

213
00:19:00,600 --> 00:19:04,720
When we see something, we'll think, we'll conceive of it as good or we'll conceive of

214
00:19:04,720 --> 00:19:06,760
it as bad.

215
00:19:06,760 --> 00:19:10,720
We will have some kind of desire, some kind of intention arise.

216
00:19:10,720 --> 00:19:18,840
When we hear sounds, we will recognize it as a pleasant sound or an unpleasant sound,

217
00:19:18,840 --> 00:19:23,480
some a person that we like, if it's a music, we will enjoy it and so on.

218
00:19:23,480 --> 00:19:26,840
We'll give rise to some intention in regards to it.

219
00:19:26,840 --> 00:19:29,920
This is how all of addiction works.

220
00:19:29,920 --> 00:19:37,440
For people who suffer from addiction to substances or addiction to any kind of stimulus,

221
00:19:37,440 --> 00:19:44,440
we'll find that the mind goes back and forth between these different parts of reality,

222
00:19:44,440 --> 00:19:47,560
the cause and effect.

223
00:19:47,560 --> 00:19:52,400
Sometimes you'll be aware of the seeing, sometimes you'll be aware of the craving, sometimes

224
00:19:52,400 --> 00:19:57,680
you'll be aware of the mind and knowing of it and the experience of it.

225
00:19:57,680 --> 00:20:01,640
When you practice meditation, you'll be able to break this into pieces.

226
00:20:01,640 --> 00:20:06,360
The mind goes through these again and again and again and it creates more and more

227
00:20:06,360 --> 00:20:07,640
craving.

228
00:20:07,640 --> 00:20:10,200
When you're able to understand them, you're able to break them up.

229
00:20:10,200 --> 00:20:14,120
When you see something, it's simply seeing, it's neither good nor bad because there's

230
00:20:14,120 --> 00:20:18,160
nothing intrinsically good or bad about seeing after all.

231
00:20:18,160 --> 00:20:21,480
It's simply because of our misunderstanding, our ignorance.

232
00:20:21,480 --> 00:20:30,240
And because of our past habits, our habitual ways of experience, experiencing things,

233
00:20:30,240 --> 00:20:35,080
remembering that certain sites are going to be a cause for pleasure, remembering that

234
00:20:35,080 --> 00:20:39,360
certain sounds or a cause for pleasure and so on and remembering that certain sites and

235
00:20:39,360 --> 00:20:43,960
sounds and experiences, our cause for displeasure will give rise to our craving, our

236
00:20:43,960 --> 00:20:50,640
wanting to develop, to cultivate, to increase, to attain, to obtain the phenomenon and

237
00:20:50,640 --> 00:20:59,480
more of the phenomenon and our aversion, our desire to be free, to remove, to destroy,

238
00:20:59,480 --> 00:21:07,320
to banish the phenomenon in the case of those things that habitually we remember as bringing

239
00:21:07,320 --> 00:21:12,160
suffering, bringing displeasure or bringing pain.

240
00:21:12,160 --> 00:21:19,320
Because of our conception, conceiving things as more than simply what they are, for instance,

241
00:21:19,320 --> 00:21:23,320
when we hear someone's voice, right away we're going to put a value judgment on it,

242
00:21:23,320 --> 00:21:27,040
whether they're a good person or a bad person, we like them or we don't like them a friend

243
00:21:27,040 --> 00:21:31,520
or an enemy. When we see someone, we're right away going to get angry and upset because

244
00:21:31,520 --> 00:21:35,760
we don't like them or we're going to become attracted and pleased because we do like them

245
00:21:35,760 --> 00:21:38,000
maybe the beautiful or so on.

246
00:21:38,000 --> 00:21:41,520
We're going to give rise to these cravings.

247
00:21:41,520 --> 00:21:45,280
And this is what's going to get us into trouble because this is how addiction works.

248
00:21:45,280 --> 00:21:50,040
If addiction didn't cause trouble, people wouldn't have to look for a way out of it.

249
00:21:50,040 --> 00:21:57,120
But the truth of reality is that our cravings lead us to a cycle of addiction that is very

250
00:21:57,120 --> 00:22:02,480
difficult and more and more difficult the longer it lasts to break free from.

251
00:22:02,480 --> 00:22:07,720
So this is the next link that craving wouldn't be a problem if it didn't lead to the

252
00:22:07,720 --> 00:22:12,960
next cycle and that is addiction.

253
00:22:12,960 --> 00:22:17,520
Most people who haven't studied the Buddhist teaching, whether they be Buddhists or non-Buddists,

254
00:22:17,520 --> 00:22:22,480
don't see the danger inherent in craving, don't see the inherent danger in liking.

255
00:22:22,480 --> 00:22:27,120
We think that our likes and dislikes are what make us who we are.

256
00:22:27,120 --> 00:22:32,440
And this is exactly the problem because we have some conception of self, of eye, of what

257
00:22:32,440 --> 00:22:35,360
eye like and dislike.

258
00:22:35,360 --> 00:22:44,960
We stumble right over this egregious error of judgment that actually there is no eye

259
00:22:44,960 --> 00:22:47,600
that we're creating this as we go along.

260
00:22:47,600 --> 00:22:48,600
We'll say this.

261
00:22:48,600 --> 00:22:52,080
It's our way of justifying liking and disliking.

262
00:22:52,080 --> 00:22:54,600
We say, I like this, I like that.

263
00:22:54,600 --> 00:22:56,600
And we say this as a justification.

264
00:22:56,600 --> 00:23:03,360
It's the connection, if you will, between our craving and our addiction.

265
00:23:03,360 --> 00:23:11,520
At the time when we crave something, we think, yes, that's something I like.

266
00:23:11,520 --> 00:23:15,160
This is my preference.

267
00:23:15,160 --> 00:23:21,200
Then we'll give rise to this habitual addiction where we cling to the object, we're unable

268
00:23:21,200 --> 00:23:22,200
to let go of it.

269
00:23:22,200 --> 00:23:24,960
We're unable to be without it.

270
00:23:24,960 --> 00:23:29,960
To the extent we'll actually cause great suffering for ourselves if we don't get it.

271
00:23:29,960 --> 00:23:38,280
And this is the danger inherent in simple liking because we are not static creatures.

272
00:23:38,280 --> 00:23:39,960
We are dynamic.

273
00:23:39,960 --> 00:23:45,440
And everything we do and think affects who we are, changes who we are.

274
00:23:45,440 --> 00:23:46,840
Craving gives rise to clinging.

275
00:23:46,840 --> 00:23:50,000
You can't stop it simply by force of will.

276
00:23:50,000 --> 00:23:55,000
You can't simply wish for it to stay simply as liking, where liking will not give rise

277
00:23:55,000 --> 00:23:56,400
to craving.

278
00:23:56,400 --> 00:24:05,440
Liking has, as it's nature, the propensity to give rise to clinging.

279
00:24:05,440 --> 00:24:12,920
Gives rise to creating or becoming, creating really, creating circumstances wherein we can

280
00:24:12,920 --> 00:24:17,480
get what we want, cultivating and developing and building.

281
00:24:17,480 --> 00:24:26,040
Building up, in many cases, a huge ego or identification with reality, where we have who

282
00:24:26,040 --> 00:24:34,480
I am, my status in life, my stable reality, my home, my car, my family, and so on.

283
00:24:34,480 --> 00:24:38,160
We build this up based on our cravings, based on our clings.

284
00:24:38,160 --> 00:24:40,600
We will build up our whole reality.

285
00:24:40,600 --> 00:24:46,880
Why people, why beings are so diversified, why some people are rich, some people are poor,

286
00:24:46,880 --> 00:24:54,520
is totally based on where we have led ourselves from lifetime after lifetime, where we've

287
00:24:54,520 --> 00:25:03,120
directed our minds and what we've clung to, what we've associated and identified with.

288
00:25:03,120 --> 00:25:11,560
And it's because of this becoming that we have all of our suffering, all of our dissatisfaction,

289
00:25:11,560 --> 00:25:19,440
because it's this creating, this conceiving, this seeking after that gives rise to old

290
00:25:19,440 --> 00:25:26,880
age sickness and death, that gives rise to this form-formed existence of being a human,

291
00:25:26,880 --> 00:25:35,360
of being an animal, of being any type of creature that we actually create for ourselves,

292
00:25:35,360 --> 00:25:40,760
that gives rise to our sicknesses, gives rise to our pains, gives rise to our conflicts

293
00:25:40,760 --> 00:25:48,120
and our sufferings, the war and famine and poverty and so on, that exists in the world.

294
00:25:48,120 --> 00:26:01,720
It's all created simply by our erroneous identification and the idea that somehow we

295
00:26:01,720 --> 00:26:04,920
can find true happiness in this way.

296
00:26:04,920 --> 00:26:07,280
This is the core of the Buddhist teaching.

297
00:26:07,280 --> 00:26:13,840
It's really what the Buddha realized on the night that he became enlightened, that reality

298
00:26:13,840 --> 00:26:16,120
does work in terms of cause and effect.

299
00:26:16,120 --> 00:26:19,240
It wasn't a theoretical realization.

300
00:26:19,240 --> 00:26:20,840
He saw reality working.

301
00:26:20,840 --> 00:26:27,800
He saw that it's because of our ignorance, that we give rise to intention, even the intention

302
00:26:27,800 --> 00:26:34,680
to help ourselves, to do good things for ourselves, to create a life that is supposedly

303
00:26:34,680 --> 00:26:40,160
going to make us happy, even giving rise to this intention is due to ignorance.

304
00:26:40,160 --> 00:26:44,480
Because if we understood, as I said, reality for what it was, we wouldn't see any need

305
00:26:44,480 --> 00:26:50,240
to create anything, we would be content and comfortable and happy with things as they are.

306
00:26:50,240 --> 00:26:55,680
We wouldn't give rise to this craving that comes from experience.

307
00:26:55,680 --> 00:27:00,480
Our experience of reality would simply be the conscious experience of the physical and

308
00:27:00,480 --> 00:27:02,960
mental at the six senses.

309
00:27:02,960 --> 00:27:06,320
It would stop at the contact and the sensation.

310
00:27:06,320 --> 00:27:12,920
We would feel happy, we would feel pain, we would feel calm, but we wouldn't attach to

311
00:27:12,920 --> 00:27:15,200
any of these feelings as good or bad.

312
00:27:15,200 --> 00:27:18,600
Anything that we saw would simply be what it is.

313
00:27:18,600 --> 00:27:25,120
We would see it clearly as it is for what it is and not put any value judgment on it

314
00:27:25,120 --> 00:27:36,800
other than what was immediately apparent as being what it was, as being a risen phenomenon

315
00:27:36,800 --> 00:27:39,320
that comes about and ceases.

316
00:27:39,320 --> 00:27:44,000
We would live our lives in the way that many of us truly think that we are living our lives

317
00:27:44,000 --> 00:27:45,000
already.

318
00:27:45,000 --> 00:27:50,680
We think that we are here living our lives in a very ordinary way and we think that we

319
00:27:50,680 --> 00:27:54,280
in general experience, great peace and happiness.

320
00:27:54,280 --> 00:27:59,400
But as a result of our craving and our clinging, we actually create great suffering and

321
00:27:59,400 --> 00:28:03,480
we're not really living in true reality.

322
00:28:03,480 --> 00:28:09,280
Our experience of reality is, as they say, tangential, we experience reality for a moment

323
00:28:09,280 --> 00:28:14,680
and then more often on a conception, the idea of it being a good or a bad thing that

324
00:28:14,680 --> 00:28:16,240
we experience.

325
00:28:16,240 --> 00:28:20,920
When we see someone, something, we hear something, smell, taste immediately, it takes us

326
00:28:20,920 --> 00:28:28,200
away into our craving, craving, leading to clinging, clinging, leading to this, becoming or

327
00:28:28,200 --> 00:28:39,560
creating of some kind of intention to attain or to be free from and so on, which in turn

328
00:28:39,560 --> 00:28:45,140
gives rise to conflict and suffering and not getting what we want and getting what we

329
00:28:45,140 --> 00:28:55,520
don't want and being dissatisfied and disappointed and sorrow, lamentation and despair.

330
00:28:55,520 --> 00:28:59,960
All of these things come from our attachments, our judgments, our ideas that things are

331
00:28:59,960 --> 00:29:01,320
good and bad.

332
00:29:01,320 --> 00:29:07,640
So this is an incredibly important teaching, as I said, and incredibly important for

333
00:29:07,640 --> 00:29:08,640
meditators.

334
00:29:08,640 --> 00:29:13,120
It's really the core of the meditation practice and the core theory that we should understand

335
00:29:13,120 --> 00:29:19,840
when we start to practice meditation because this is how we're going to change our minds

336
00:29:19,840 --> 00:29:22,000
during the meditation practice.

337
00:29:22,000 --> 00:29:27,160
So thank you for tuning in, I hope this has been useful in that those of you who are watching

338
00:29:27,160 --> 00:29:29,040
are able to put this into practice.

339
00:29:29,040 --> 00:29:34,040
Please don't be satisfied simply by intellectual understanding of the Buddha's teaching.

340
00:29:34,040 --> 00:29:38,000
Do take the time and effort to put this into practice.

341
00:29:38,000 --> 00:29:42,080
And I wish for this teaching to be a benefit to all of you and that you are able to

342
00:29:42,080 --> 00:29:46,560
through this teaching and through your practice of the Buddha's teaching to find true

343
00:29:46,560 --> 00:29:49,040
peace, happiness and freedom from suffering.

344
00:29:49,040 --> 00:29:56,040
Thank you and have a good day.

