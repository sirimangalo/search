1
00:00:00,000 --> 00:00:11,160
Leave me everyone, welcome to our live broadcast.

2
00:00:11,160 --> 00:00:23,560
Seems to me that a discussion about not broadcasting every night was understood to actually

3
00:00:23,560 --> 00:00:24,560
be a decision.

4
00:00:24,560 --> 00:00:26,640
It wasn't a decision.

5
00:00:26,640 --> 00:00:32,320
We're still going to do, or I'm still going to do daily broadcasts, at least for the near

6
00:00:32,320 --> 00:00:33,320
future.

7
00:00:33,320 --> 00:00:45,800
If any change comes on that channel, I think nightly broadcasts is fine.

8
00:00:45,800 --> 00:00:51,160
This is long as you're all fine with some of them being short, sometimes I'll just come

9
00:00:51,160 --> 00:00:56,360
on and say good evening and give some brief teaching or some nights maybe just answer

10
00:00:56,360 --> 00:00:57,360
questions.

11
00:00:57,360 --> 00:01:02,800
It's very easy for me to sit here and answer questions to actually come up with something

12
00:01:02,800 --> 00:01:03,800
to talk about.

13
00:01:03,800 --> 00:01:10,880
Well, some nights it might be, I might just have other things on my plate.

14
00:01:10,880 --> 00:01:25,720
Anyway, tonight, we are looking at a good tourniquet, a number of threes, number, 49 or

15
00:01:25,720 --> 00:01:26,720
50.

16
00:01:26,720 --> 00:01:27,720
I don't know.

17
00:01:27,720 --> 00:01:33,400
The English says 49, the poly says 50, so if you're following along with Pekabundi, it's

18
00:01:33,400 --> 00:01:38,080
number 49 in the book of threes.

19
00:01:38,080 --> 00:01:48,480
Our door, or as the Americans say, our door without view, if it looks funny, it's because

20
00:01:48,480 --> 00:01:51,880
I'm not American.

21
00:01:51,880 --> 00:01:56,640
Our door is an interesting word.

22
00:01:56,640 --> 00:02:01,920
It'd be nice to have a different one because our door has different connotations in English,

23
00:02:01,920 --> 00:02:11,160
but it's the best because the word a tapi, or a tapi, or a tapi, or a tapi, a tapi.

24
00:02:11,160 --> 00:02:21,200
A tapi comes from a temperature, and it's the same word as the same root as the English

25
00:02:21,200 --> 00:02:22,200
word temperature.

26
00:02:22,200 --> 00:02:39,160
A tapi comes from a tapi, a tapi comes a tapi, which becomes a tapi.

27
00:02:39,160 --> 00:02:48,200
So the idea is in India, exertion was described as being energetic.

28
00:02:48,200 --> 00:02:53,680
So in English, we have that with ardor, but we don't use it exactly in the same way.

29
00:02:53,680 --> 00:03:00,440
We don't think of work as our effort, as getting hot about something, getting hot about

30
00:03:00,440 --> 00:03:09,040
something, or heated up as either to do a lust or anger, which is not exactly what, not at

31
00:03:09,040 --> 00:03:16,200
all what the wood is referring to, and not how it's used in poly.

32
00:03:16,200 --> 00:03:23,040
But ardor can also mean energy or energetic effort.

33
00:03:23,040 --> 00:03:36,920
So the meaning here is at the bangaraniyam, is the woodwood, is there three things, or three

34
00:03:36,920 --> 00:03:45,920
subjects, three topics on which one should cultivate at the bangaraniyam.

35
00:03:45,920 --> 00:03:56,400
Dangaraniyam means should be done, octopus, ardor, or effort.

36
00:03:56,400 --> 00:03:59,520
And I think this is fairly interesting teaching.

37
00:03:59,520 --> 00:04:12,120
I mean, the Buddha taught different reactions to the various problems that we are challenges

38
00:04:12,120 --> 00:04:17,640
that we are facing with the various aspects of the cause of suffering.

39
00:04:17,640 --> 00:04:23,640
So in certain instances, one should exert one's self, in certain instances, one should

40
00:04:23,640 --> 00:04:38,360
restrain one's self, in certain instances, one should continue, one should not push or

41
00:04:38,360 --> 00:04:46,000
not, not retreat, but one, in certain cases, one should simply continue the way one

42
00:04:46,000 --> 00:04:57,600
is going, and in certain cases, one should cultivate dispassion, so sort of not give, take

43
00:04:57,600 --> 00:05:05,880
out the zeal for something, so not exactly restraint, but abandon, I guess.

44
00:05:05,880 --> 00:05:12,440
Anyway, the different ways of looking at how you deal with defilements, and I think that's

45
00:05:12,440 --> 00:05:16,920
a valid thing to say, because here it just only tells part of the story, and it tells

46
00:05:16,920 --> 00:05:24,760
the part of the story when these three things, where the salient quality or the outstanding

47
00:05:24,760 --> 00:05:32,000
quality is effort, and so whether that means anything or not, but it appears that these

48
00:05:32,000 --> 00:05:46,000
three are especially relating to exertion, because actually there are four great exertions,

49
00:05:46,000 --> 00:05:47,000
right?

50
00:05:47,000 --> 00:05:51,720
But here they don't, here are only two are mentioned, so the Buddha says, in regards to

51
00:05:51,720 --> 00:06:05,880
under reason evil states, one should cultivate effort for their non-arising, if they

52
00:06:05,880 --> 00:06:11,360
haven't arisen, one should work hard to make sure they don't arise.

53
00:06:11,360 --> 00:06:20,560
For under reason wholesome numbers, one should work hard for their arising, and one

54
00:06:20,560 --> 00:06:36,240
should work hard for cultivating anti-wasanaya, anti-wasanaya, anti-wasanaya, which means bearing

55
00:06:36,240 --> 00:06:47,080
with wasanaya means to live, or to dwell, and anti-means on top of, or with while they're

56
00:06:47,080 --> 00:06:50,800
existing.

57
00:06:50,800 --> 00:07:02,840
One should cultivate forbearance or ability to bear with a reason painful feeling was in

58
00:07:02,840 --> 00:07:12,640
the body that are harsh and unpleasant, racking, sharp, piercing, harrowing, disagreeable,

59
00:07:12,640 --> 00:07:19,960
wrapping one's vitality, sapping one's vitality, that's interesting, sapping one's vitality

60
00:07:19,960 --> 00:07:30,280
should probably read, banaharang should probably read life threatening, I think sapping

61
00:07:30,280 --> 00:07:41,040
one's vitality is not how we, I like to translate this, the point is even when the painful

62
00:07:41,040 --> 00:07:48,080
feelings could potentially kill you, even when they seem deadly, I don't know, that's

63
00:07:48,080 --> 00:07:56,600
how it's normally translated, beakabody has chosen banaharang, banaharang, to mean sapping

64
00:07:56,600 --> 00:08:02,400
vitality, which is, I'm not convinced, I think the Buddha is trying to make a point

65
00:08:02,400 --> 00:08:11,600
that, one should cultivate the ultimate equanimity, I'm not quite sure why these three,

66
00:08:11,600 --> 00:08:17,360
the thing about the Ingruturnikai is sometimes the, in the book of twos, threes, even

67
00:08:17,360 --> 00:08:24,640
fours and fives, you have lists that are incomplete, and I think generally the reason

68
00:08:24,640 --> 00:08:31,840
why they're incomplete is given as being that in later books, well later books there, there

69
00:08:31,840 --> 00:08:38,960
are more of them, but that in certain instances, only certain qualities were, were important

70
00:08:38,960 --> 00:08:46,760
for the audience, the Buddha tailored his teaching according to the audience, but sometimes

71
00:08:46,760 --> 00:08:50,760
he would highlight certain teachings, either that or as they said it might be that these

72
00:08:50,760 --> 00:08:58,160
ones are specifically relating to effort, because you've got the under reason unwholesome

73
00:08:58,160 --> 00:09:04,040
states, so there's the, when you have the potential still to get angry about things, when

74
00:09:04,040 --> 00:09:09,360
you have the potential still to get attached to things, you work hard to stay mindful

75
00:09:09,360 --> 00:09:15,600
so that that doesn't happen, right, but then the same goes with a reason unwholesome

76
00:09:15,600 --> 00:09:21,400
states, if evil hazards in your mind, if you are angry, if you are addicted to something

77
00:09:21,400 --> 00:09:27,000
and they do arise, you strive to be mindful to not let them continue to cut them off,

78
00:09:27,000 --> 00:09:35,480
but to take away their nourishment, that would, that which is feeding them, which is

79
00:09:35,480 --> 00:09:49,400
the repeated, the repeated application of the mind to the same state of mind, by changing

80
00:09:49,400 --> 00:09:56,880
it to objectivity, by seeing it from this, and so on, and so you have it both ways,

81
00:09:56,880 --> 00:10:02,720
whether it's an unrisen, don't let them arise, arisen, cut them off, and the same goes

82
00:10:02,720 --> 00:10:10,920
with also, here the Buddha talks about giving rise to wholesomeness, work hard to make

83
00:10:10,920 --> 00:10:16,480
wholesome qualities arise, so through mindfulness, mindfulness is something we work hard

84
00:10:16,480 --> 00:10:21,680
to make arise, but through mindfulness, through the hard work we do of mindfulness, we

85
00:10:21,680 --> 00:10:31,960
work hard to cultivate all sorts of wholesome states, wisdom, concentration, effort,

86
00:10:31,960 --> 00:10:44,920
confidence, you work hard to cultivate wholesome states, but the same goes with wholesome

87
00:10:44,920 --> 00:10:48,720
states of already arisen, you have to work hard to keep them, so there are four right

88
00:10:48,720 --> 00:10:56,720
efforts based on unrisen or arisen wholesome and unwholesome, here the Buddha only talks

89
00:10:56,720 --> 00:11:11,360
about two of them, which is curious, but it's relating to things of unrisen, and the

90
00:11:11,360 --> 00:11:17,440
most important point here is that what is nice about this teaching is that you've got

91
00:11:17,440 --> 00:11:23,160
these effort in regards to wholesomeness, effort in regards to unwholesomeness, but what

92
00:11:23,160 --> 00:11:30,400
about in regards to your circumstance, the third one, what this teaching points out is

93
00:11:30,400 --> 00:11:35,200
that we're not exerting effort to change our circumstance, right, because what's the third

94
00:11:35,200 --> 00:11:40,200
one, the third one is being patient with everything else, basically, what are we working

95
00:11:40,200 --> 00:11:44,680
hard to change? People would say, what do you work for in life? Well, you want it to

96
00:11:44,680 --> 00:11:50,520
be happy to get rid of your suffering, right? Not really. In Buddhism, that's not actually

97
00:11:50,520 --> 00:11:55,360
the focus of our practice, you know, we talk about being free from suffering and being

98
00:11:55,360 --> 00:12:00,760
happy, and that's kind of the goal, you could say, but I mean, it definitely is the goal,

99
00:12:00,760 --> 00:12:08,000
but in a practical sense, it's not the present goal, it's not what we're trying to do,

100
00:12:08,000 --> 00:12:13,800
it's not how we're approaching experience. When they're suffering, you're in no way

101
00:12:13,800 --> 00:12:19,040
shape or form trying to work hard to get rid of it. Absolutely not. In fact, it would

102
00:12:19,040 --> 00:12:25,040
be to your detriment, in two ways. It would be to your detriment because of the quality

103
00:12:25,040 --> 00:12:30,080
of a version that it would cultivate, but also be to your detriment because suffering

104
00:12:30,080 --> 00:12:34,520
is what's going to teach you. If you get rid of suffering, if you try to get rid of

105
00:12:34,520 --> 00:12:37,960
suffering, not only are you going to cultivate a version, but you're also missing on

106
00:12:37,960 --> 00:12:44,120
opportunity, but an opportunity to see a version, to see how you react to suffering and

107
00:12:44,120 --> 00:12:49,320
to learn the difference between suffering and aversion, that suffering is just suffering

108
00:12:49,320 --> 00:12:57,960
or experiences like pain and so on, or just experiencing. So the real point, I think that

109
00:12:57,960 --> 00:13:02,760
we should highlight in this teaching is that all we have to focus on is wholesome and

110
00:13:02,760 --> 00:13:08,760
unwholesome. We're not concerned about pain or happiness. Happiness and pain, happiness

111
00:13:08,760 --> 00:13:15,240
and suffering, these are to be seen as experiencing. They arise and they cease. They're

112
00:13:15,240 --> 00:13:22,040
not to be judged or reacted to. That's another thing that's missing here from the third

113
00:13:22,040 --> 00:13:29,400
one. In the third teaching, the Buddha talks simply about being patient with unpleasant

114
00:13:29,400 --> 00:13:34,600
feelings, but what we don't think of too often is the Buddha's teaching on how to be

115
00:13:34,600 --> 00:13:41,800
on how we have to be patient with pleasant and this is a very important part of the

116
00:13:41,800 --> 00:13:48,200
Buddha's teaching. Not only do we have to be patient with suffering, but we have to be what

117
00:13:48,200 --> 00:13:57,560
one would call patient with happiness and it's the same state and the same quality of mind

118
00:13:57,560 --> 00:14:09,880
because it's a reaction either way. It's the changing of the mind to give up our inclination

119
00:14:09,880 --> 00:14:15,960
to react when we experience unpleasant feelings. Our inclination is to immediately

120
00:14:15,960 --> 00:14:27,640
retreat, recoil from them. When we experience pleasant feelings, our immediate inclinations

121
00:14:27,640 --> 00:14:37,160
to seek them out, to incline towards them, to gravitate towards them and true and patience,

122
00:14:38,120 --> 00:14:45,720
true patience is being able to deal with both. Being able to bear with both. So it's like the

123
00:14:45,720 --> 00:14:53,160
dog you know this trick where you put a bone on the dog's nose and if the dog is really well

124
00:14:53,160 --> 00:14:57,880
trained, it won't snap it and it'll sit there and wait until the owner says okay go for it

125
00:14:57,880 --> 00:15:07,480
and then it'll leave it. Training the dog, training the mind to be like to be well trained

126
00:15:07,480 --> 00:15:17,000
just like you train the dog. Now training the mind to let go really. My impatience is such an

127
00:15:17,000 --> 00:15:23,080
important part of the practice. It's very much the feeling that a meditator should have

128
00:15:23,080 --> 00:15:28,840
throughout the practice that they're being patient and it feels like they're burning off the

129
00:15:28,840 --> 00:15:34,360
filements. It's another good thing about Adapi. Adapi and traditional Buddhist circles is described

130
00:15:34,360 --> 00:15:39,160
why we use the word, we're relating to temperatures because you're burning up the

131
00:15:41,080 --> 00:15:45,960
and patience is the great way. What I said, Kanti, but among the Tapu to Dika,

132
00:15:46,840 --> 00:15:53,880
patience is the best form of tapa, tapa being another form of word Adapi. It's the same root,

133
00:15:53,880 --> 00:16:02,920
same basic form. Tapas is really means temperature or heat. What you're doing is you're burning

134
00:16:02,920 --> 00:16:07,480
up to filements and you bring them up to patience. When you want something and instead you're

135
00:16:07,480 --> 00:16:16,600
patient on it, you can feel even the physical, the chemicals will bubble and boil inside. All of

136
00:16:16,600 --> 00:16:28,680
these chemicals waiting to engage and bring about states of pleasure. And when something unpleasant

137
00:16:28,680 --> 00:16:39,160
comes, there's the tension in the body and you can feel the tension in the habitual reactivity,

138
00:16:39,160 --> 00:16:47,400
waiting to strike, waiting to get upset, waiting to get angry. Instead, you're patient with it.

139
00:16:49,000 --> 00:16:51,800
This is what you should feel in the meditation. You should feel patient.

140
00:16:53,720 --> 00:16:57,960
You can feel like you're burning up to the filements just by sitting with them. Just by sitting

141
00:16:57,960 --> 00:17:09,640
with the things that normally make you react. So, three types. Three types of effort. If you want

142
00:17:09,640 --> 00:17:15,640
to learn about effort, effort, does it relates to wholesomeness? effort as it relates to unwholesomeness,

143
00:17:16,440 --> 00:17:21,960
and effort as it relates to being patient with everything else? There's the only two things that

144
00:17:21,960 --> 00:17:29,080
are really important to our wholesomeness and unwholesomeness. Cultivating wholesomeness, destroying

145
00:17:29,080 --> 00:17:35,400
unwholesomeness, and being patient with everything else, which is really the same thing. It's a part

146
00:17:35,400 --> 00:17:45,400
of the same thing. So, that's our teaching for tonight. I don't know if Robin didn't come tonight.

147
00:17:45,400 --> 00:17:49,240
Maybe she took it seriously that we're not supposed to have. When I said we're not going to have

148
00:17:49,240 --> 00:17:57,960
an overnight, it doesn't matter. I think we don't have any old questions. I did scroll through,

149
00:17:58,840 --> 00:18:07,560
and I think our first one is this evening from Granny's. When I started to think about the

150
00:18:07,560 --> 00:18:12,840
knowledge in your videos and then note that I am thinking, the act of noting stops the thinking.

151
00:18:13,640 --> 00:18:19,080
I can't not think at the same time. But don't I want to be able to think about the knowledge

152
00:18:19,080 --> 00:18:25,720
? No, not really. I would say the knowledge is meant to evoke something in you,

153
00:18:26,680 --> 00:18:33,240
and that to trigger something, and I would say that's enough. The point is to encourage you to

154
00:18:33,240 --> 00:18:37,400
say to yourself thinking, thinking, that's where the point of all the teaching. If you consider

155
00:18:37,400 --> 00:18:42,920
what I was teaching just now, hopefully the understanding you get from it is that, oh, now I have

156
00:18:42,920 --> 00:18:51,960
to cultivate mindfulness. I have to work harder at cultivating mindfulness. I would say if you have to

157
00:18:51,960 --> 00:18:58,280
spend time mulling it over or thinking about it, in a world they sense that's fine, and it's

158
00:18:58,280 --> 00:19:05,080
important for things like teaching, or things like explaining it to others, figuring out what

159
00:19:05,080 --> 00:19:08,360
is the best way to explain. I mean a Buddha, it's not that the Buddha didn't think or that our

160
00:19:08,360 --> 00:19:16,600
hands don't think. And I guess that's really the point is that you're not always,

161
00:19:18,440 --> 00:19:22,120
you know, not necessarily always supposed to be meditating. I mean there are other times where you

162
00:19:22,120 --> 00:19:26,840
want to do other things, sort of if you want to teach someone. Instead of meditating you have to

163
00:19:26,840 --> 00:19:32,200
think, you know, you have to meditate as well, but there will be a portion of time where you have to

164
00:19:32,200 --> 00:19:38,840
think when am I going to teach tonight? Of course, much more important is that you're meditating.

165
00:19:38,840 --> 00:19:46,680
There's no question there, but for things like teaching, explaining to others, and even sometimes

166
00:19:46,680 --> 00:19:52,600
for stepping back and evaluating your practice, sometimes you have to think. So at that time you don't

167
00:19:52,600 --> 00:20:01,640
meditate, but so. But it's not really, none of that's really necessary if you're just able to,

168
00:20:01,640 --> 00:20:06,200
you'll be mindful, you're able to say to yourself thinking, again, it disappears. Well, that's great.

169
00:20:06,920 --> 00:20:10,920
You're starting to see that everything arises and ceases. You're starting to teach yourself,

170
00:20:10,920 --> 00:20:14,840
and even our minds are arising and ceasing all the time.

171
00:20:14,840 --> 00:20:23,320
But no, there will just be times where you just think. So don't worry about that.

172
00:20:24,920 --> 00:20:26,200
At that time you're not meditating.

173
00:20:30,600 --> 00:20:35,800
Wow, one question tonight. I guess scared everyone away at my saying I wasn't going to do it

174
00:20:35,800 --> 00:20:45,000
every night. Well, let's see. I think I'll wait a couple of minutes because there's a delay,

175
00:20:45,000 --> 00:20:51,080
I think, so I have to wait for, wait a minute, wait a minute. More questions come on.

176
00:20:51,080 --> 00:20:59,720
So then look, we've got a big long list of meditators tonight. Let's be like 20 more than 20.

177
00:20:59,720 --> 00:21:05,560
We've got Canadians, we've got Americans, we've got the India,

178
00:21:08,680 --> 00:21:19,000
our Mexico, Sri Lanka, another Mexico, Romania,

179
00:21:19,000 --> 00:21:25,880
New Zealand, Norway,

180
00:21:29,240 --> 00:21:36,440
Moritas, is that for real? We've got someone from, I don't even know where that is, Moritas, Moritas.

181
00:21:36,440 --> 00:21:44,760
Wow, I'm Singapore, all over.

182
00:21:44,760 --> 00:22:04,600
All right, well, and we'll end it there. Thank you all for tuning in. See you all tomorrow, maybe.

