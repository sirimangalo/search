1
00:00:00,000 --> 00:00:08,000
Hello everyone, I'm just an update video here. Apologies that I haven't put any videos up lately.

2
00:00:08,000 --> 00:00:18,000
I've been busy with other things. One of which is a potential monastery.

3
00:00:18,000 --> 00:00:26,000
It looks like maybe in the near future we'll have a forest monastery opened.

4
00:00:26,000 --> 00:00:30,000
And kicking anyway, starting up.

5
00:00:30,000 --> 00:00:35,000
So more on that hopefully in the near future but that's sort of what's been keeping me busy.

6
00:00:35,000 --> 00:00:45,000
In the meantime I've uploaded the how to meditate videos in iPod format

7
00:00:45,000 --> 00:00:52,000
or in smaller format suitable for mobile devices.

8
00:00:52,000 --> 00:00:59,000
And you can download those from our website. The link is down there in the description.

9
00:00:59,000 --> 00:01:05,000
And please let me know. I don't have such a device so I can't say whether they're working or not.

10
00:01:05,000 --> 00:01:13,000
Let me know if they work otherwise. I'll upload them again in a different format.

11
00:01:13,000 --> 00:01:21,000
And yeah, so that's all. Look for another video hopefully in the near future.

12
00:01:21,000 --> 00:01:28,000
I'll try to get back to answering people's questions. I know there's still quite a few that are unanswered.

13
00:01:28,000 --> 00:01:36,000
Thanks for everyone for tuning in and leaving your feedback. What good and bad? Happy to get it.

14
00:01:36,000 --> 00:01:39,000
And keep meditating.

15
00:01:39,000 --> 00:01:46,000
For the best.

