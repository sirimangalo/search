1
00:00:00,000 --> 00:00:05,680
I just wanted to return back to this to try and make a link between that

2
00:00:05,680 --> 00:00:10,760
jotica story because it's actually kind of interesting to look at what happened

3
00:00:10,760 --> 00:00:13,480
and I don't want to say the wrong thing here please don't I'm kind of

4
00:00:13,480 --> 00:00:18,580
ignorant about this all I don't pay too much attention to Tibet but the one

5
00:00:18,580 --> 00:00:24,960
good thing that you have to admit has come of it all is a huge interest in

6
00:00:24,960 --> 00:00:33,480
Buddhism as a result of people's social conscience and sense of social

7
00:00:33,480 --> 00:00:44,600
justice in regards to Tibet I think probably unarguable and not you can't argue

8
00:00:44,600 --> 00:00:55,080
that the fact that Dalai Lama was forced to leave his home country has has it's

9
00:00:55,080 --> 00:01:01,480
really the only reason or the biggest reason why the Dalai Lama is now so so

10
00:01:01,480 --> 00:01:06,920
popular and incredibly popular and so everyone we have a bit of a problem

11
00:01:06,920 --> 00:01:10,320
because everyone when I went to Canada for example people ask me when's the

12
00:01:10,320 --> 00:01:14,120
Dalai Lama coming to town do you know when the Dalai Lama is coming or is the

13
00:01:14,120 --> 00:01:17,920
Dalai Lama or something like that oh no I don't I don't really know much about the

14
00:01:17,920 --> 00:01:25,600
Dalai Lama they're kind of confused but aren't you a Buddhist monk so but in

15
00:01:25,600 --> 00:01:29,800
general that's a wonderful thing that it's meant that people have come to hear

16
00:01:29,800 --> 00:01:34,960
more about the teachings and I think this relates to some point it's not the

17
00:01:34,960 --> 00:01:41,920
only point but some point that we shouldn't miss is that we don't have to hold on

18
00:01:41,920 --> 00:01:48,680
to preserve Buddhism and so there's another example is here in Sri Lanka the

19
00:01:48,680 --> 00:01:55,360
government is now very happy that they were able to do away with the or end the

20
00:01:55,360 --> 00:02:02,600
civil war but it's it'll be interesting to see what happens and always we

21
00:02:02,600 --> 00:02:06,400
should never lose sight of the fact that the most important is that we stick to

22
00:02:06,400 --> 00:02:14,120
Buddhist principles because well if our most important goal is is the

23
00:02:14,120 --> 00:02:18,800
preservation of Buddhism or the preservation really of enlightenment what

24
00:02:18,800 --> 00:02:30,240
are our real goals and our real concerns so what is really important to us is it

25
00:02:30,240 --> 00:02:38,160
our territory our land or is it our minds and our our goodness because really

26
00:02:38,160 --> 00:02:42,840
with with the extreme hardship I guess that is that the Tibetan people have

27
00:02:42,840 --> 00:02:54,240
undergone the it has what you will you have to admit is that it hasn't stopped

28
00:02:54,240 --> 00:02:59,120
the practice of Tibetan Buddhism in the world and it may have weakened it or

29
00:02:59,120 --> 00:03:09,360
or in some cases we can be intensity of it but now the number of people who are

30
00:03:09,360 --> 00:03:14,320
interested in and are sincerely studying these teachings has certainly

31
00:03:14,320 --> 00:03:18,160
actually multiplied but what you can say is that it hasn't decreased so the

32
00:03:18,160 --> 00:03:22,120
point being that that's probably any sad but I think for another reason we

33
00:03:22,120 --> 00:03:25,960
have to be careful not only because of our defilements but because of our

34
00:03:25,960 --> 00:03:31,480
misunderstanding of what's important who are clinging to social justice as

35
00:03:31,480 --> 00:03:38,440
opposed to righteousness it's a very example in in Burma there were these

36
00:03:38,440 --> 00:03:41,760
revolutions and the monks actually got involved and so they called it the

37
00:03:41,760 --> 00:03:47,280
saffron revolution and it just wound up getting a whole bunch of monks killed

38
00:03:47,280 --> 00:03:55,760
but I voice voice opposition to it because regardless people were saying oh yeah you

39
00:03:55,760 --> 00:03:59,480
can you can say it's no good but what if you were in Burma but really the

40
00:03:59,480 --> 00:04:03,960
opposite holds true everyone's encouraging these monks to go out and and get

41
00:04:03,960 --> 00:04:12,040
involved in politics and so on when it it really smacks of a lack of

42
00:04:12,040 --> 00:04:17,680
understanding or even trust in in the laws of karma and the laws of goodness if

43
00:04:17,680 --> 00:04:23,720
you think but but but we we have to get angry we have to fight otherwise we'll

44
00:04:23,720 --> 00:04:29,640
lose this or that or we'll continue to suffer it really is a misunderstanding of

45
00:04:29,640 --> 00:04:33,120
the first thing that the Buddha taught the first teaching that we hear from

46
00:04:33,120 --> 00:04:39,400
the Buddha in the Dhamapada Manopubangama Dhamma the mind comes first if we

47
00:04:39,400 --> 00:04:45,640
act or speak with and and also mind suffering comes and the actions of people in

48
00:04:45,640 --> 00:04:55,400
these various situations seems to be a misunderstanding or a lack of

49
00:04:55,400 --> 00:05:06,760
appreciation for this teaching I would like to just say that if a monk or a

50
00:05:06,760 --> 00:05:12,840
female monk wants to get involved in politics then they should take their

51
00:05:12,840 --> 00:05:18,280
robes of before they do that that's my personal opinion about it because we

52
00:05:18,280 --> 00:05:28,040
have by by definition our job is to try to become enlightened and we should

53
00:05:28,040 --> 00:05:38,880
not even talk the the Buddha called this talk about territories talk about wars

54
00:05:38,880 --> 00:05:45,400
talk about kings and and so on he called it animal talk and it is not

55
00:05:45,400 --> 00:05:57,760
behooving it's not good for for a monk to to talk about that even so and I

56
00:05:57,760 --> 00:06:06,400
think for a layperson it might be different but still it's it's due to the

57
00:06:06,400 --> 00:06:14,760
defalments yeah we we we should never criticize or attack there's a bit that's

58
00:06:14,760 --> 00:06:18,000
really as a monastic I don't think this person is asking is a monastic so I

59
00:06:18,000 --> 00:06:24,680
don't want to get into too much but yeah as you say we should stick to is

60
00:06:24,680 --> 00:06:28,560
which really the answer to this question is wholesome and unwholesome mind

61
00:06:28,560 --> 00:06:35,560
states and if someone brings to us an example then we we analyze it from that

62
00:06:35,560 --> 00:06:39,080
point of view instead of saying oh that person's a communist or that person's

63
00:06:39,080 --> 00:06:43,920
a capitalist or so on and so on we should say well are they doing good deeds or

64
00:06:43,920 --> 00:06:47,200
are they doing bad deeds are they helping themselves and helping others or

65
00:06:47,200 --> 00:06:53,120
are they hurting themselves and hurting others and we go from there don't don't

66
00:06:53,120 --> 00:06:58,160
cling to it you want to be free from suffering I mean the other the other we

67
00:06:58,160 --> 00:07:01,200
were not we're going going all around this question not really answering it

68
00:07:01,200 --> 00:07:07,760
directly but the other thing is the realization that none of this is really

69
00:07:07,760 --> 00:07:12,520
important and in the end the earth is going to go for hurtling into the

70
00:07:12,520 --> 00:07:16,840
sun or I think it's gonna burn up before it gets to the sun I'm not sure or the

71
00:07:16,840 --> 00:07:21,080
sun's going to explode first or something like that so it really none of it

72
00:07:21,080 --> 00:07:26,080
makes any difference we've done this so many times that it's it's enough to

73
00:07:26,080 --> 00:07:31,280
kind of get bored of it and by this so many times I mean the earth has gone

74
00:07:31,280 --> 00:07:36,960
hurtling into the sun so many times in the round you know in the infinity of

75
00:07:36,960 --> 00:07:46,040
existence that it's it's really you know time time to give it up the last

76
00:07:46,040 --> 00:07:54,720
thing that I want to add and you mentioned it a little bit is the closest answer to

77
00:07:54,720 --> 00:08:01,560
the question is we should always do what is beneficial for ourselves and for

78
00:08:01,560 --> 00:08:25,720
others or for both

