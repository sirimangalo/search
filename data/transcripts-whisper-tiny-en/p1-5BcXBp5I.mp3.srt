1
00:00:00,000 --> 00:00:24,000
Normal, is it the normal, or is it part of the path to feel that switch of your mind when it becomes mindful during day to life, and why is it so difficult to go into this mindful state and stay in it, what you keep forgetting?

2
00:00:24,000 --> 00:00:31,000
It's habits. We have lots and lots of conflicting habits. Mindfulness is just another habit.

3
00:00:31,000 --> 00:00:39,000
If you haven't spent the time to develop it, it's not going to come by itself.

4
00:00:39,000 --> 00:00:48,000
If you haven't spent the time to develop it, it's not going to come by itself.

5
00:00:48,000 --> 00:00:55,000
If you haven't spent the time to develop it, it's not going to come by itself.

6
00:00:55,000 --> 00:01:15,000
You only get so much mindfulness because you put in work and intention.

7
00:01:15,000 --> 00:01:25,000
The switch seems to be quite clear that it's just come down and you say, oh, now you should be in control.

8
00:01:25,000 --> 00:01:34,000
Not necessarily. That switch doesn't necessarily mean mindfulness. It can mean worrying. It can mean stressing. It can mean trying to control.

9
00:01:34,000 --> 00:01:43,000
I think the switch itself, in a sense, is mindfulness. What it leads to, it can lead you to get worried and stressed if you've got those bad habits.

10
00:01:43,000 --> 00:01:54,000
It's becoming aware, oh, yes, what am I doing here? I'm not actually flying through the air or living out this fantasy or so.

11
00:01:54,000 --> 00:02:11,000
I'm right here now. I think that's a very good experience because mindfulness is such so different. A clear mindset is so different from an ordinary diluted mind state that it's a very much switch until it becomes, of course, the predominating habit.

12
00:02:11,000 --> 00:02:16,000
In which case, there's less of a switch.

13
00:02:16,000 --> 00:02:43,000
Yeah, it's funny. There's more you can talk about the diluted mind state where you're feeling the chaos and the poles of attraction and avoidance, but the actual peaceful meditative mind state is really not much to talk about because it's like talking about the stillness of a still pond,

14
00:02:43,000 --> 00:02:47,000
as opposed to the raging of an ocean.

15
00:02:47,000 --> 00:03:04,000
It's really not much to say about the calmness of meditation once you're really achieving it, but the constant chaos, this is what we perceive to be the problem in our meditation, but it's really not.

16
00:03:04,000 --> 00:03:14,000
It's the problem of our daily life. It's the problem of our habits is be who just said right there, you know.

17
00:03:14,000 --> 00:03:18,000
Okay, let's skip right along.

18
00:03:18,000 --> 00:03:37,000
I just want to find one thing. Isn't it just you're losing your concentration throughout the day? It's like you got moments of being mindful, you got moments of nothing.

19
00:03:37,000 --> 00:03:50,000
You can't be physically impossible to be. Your mind is in that purpose that you can bring it to be mindful of tasks.

20
00:03:50,000 --> 00:04:03,000
That's what I have. Sometimes you feel a little bit sleepy. Sometimes you can be awake all the time.

21
00:04:03,000 --> 00:04:12,000
But we strive to reach that level, right? Even though it's very far.

22
00:04:12,000 --> 00:04:17,000
How are we going to reach this level if you don't click today?

23
00:04:17,000 --> 00:04:19,000
Well, we can reach this level.

24
00:04:19,000 --> 00:04:25,000
A ball will only hold so much. So if you put something into it, something else has to come out.

25
00:04:25,000 --> 00:04:29,000
And if you put more mindfulness in the bad habits, what come out? They're replaced by mindful habits.

26
00:04:29,000 --> 00:04:34,000
The mind can only hold so much. The mind can hold much more and it's much more complicated.

27
00:04:34,000 --> 00:04:43,000
Okay. Okay. So if you are 24, 24, 7, you're mindful, you're a Buddha, you're what you are right now.

28
00:04:43,000 --> 00:04:46,000
Something like that. I mean, the mind feels sleeping.

29
00:04:46,000 --> 00:04:58,000
Yeah, not exactly. You know, I mean, you could be completely mindful, but your mind is focused on harming beings.

30
00:04:58,000 --> 00:05:05,000
You know, being a Buddha doesn't just mean that you have a conscious dexterity.

31
00:05:05,000 --> 00:05:08,000
You know, the quality of the Buddha.

32
00:05:08,000 --> 00:05:11,000
Yeah, right. Exactly.

33
00:05:11,000 --> 00:05:14,000
Thank you. You can mine for the customer.

34
00:05:14,000 --> 00:05:27,000
What's that? I think in the conventional use, the term mindful, yes, unfortunately.

35
00:05:27,000 --> 00:05:33,000
I think in the terms of mindful in how we want to define it now.

36
00:05:33,000 --> 00:05:41,000
But in the conventional mindfulness, I mean, that that's what they have training of like the Green Berets.

37
00:05:41,000 --> 00:05:46,000
That's what that's about, you know, black ops.

38
00:05:46,000 --> 00:05:54,000
The ninjas are a perfect example where they will, you know, some people will say that they are zen-trained.

39
00:05:54,000 --> 00:06:03,000
And, you know, they're very mindful of their stuff, but, you know, that doesn't mean that they're closer to Buddhahood.

40
00:06:03,000 --> 00:06:09,000
I mean, there is mindfulness in there, right? Any wholesome mind state has mindfulness.

41
00:06:09,000 --> 00:06:15,000
I think the question is, you know, getting rid of the unmindful states of wanting to kill someone.

42
00:06:15,000 --> 00:06:21,000
So if you don't have to be mindful every moment to become a Buddha, the purpose of life is to become a Buddha.

43
00:06:21,000 --> 00:06:26,000
So you don't have to be mindful over time, so don't worry.

44
00:06:26,000 --> 00:06:37,000
Well, I think being mindful all the time is the way to reach the goal, which is probably getting rid of all the bad habits of your mind.

45
00:06:37,000 --> 00:06:39,000
For sure. I mean, being a Buddha, the most.

46
00:06:39,000 --> 00:06:41,000
That's more difficult.

47
00:06:41,000 --> 00:06:50,000
But, yeah, to be careful, you are the better suited you are to get rid of those bad habits.

48
00:06:50,000 --> 00:06:53,000
Okay, you're all wrong. We'll notice them.

49
00:06:53,000 --> 00:06:56,000
Yeah, let's define being mindful.

50
00:06:56,000 --> 00:07:02,000
We can be mindful about that using concentration, or if we can't be aware of the present moment.

51
00:07:02,000 --> 00:07:06,000
There's no you, there's just moments, and they're based on cause and effect.

52
00:07:06,000 --> 00:07:10,000
There's no you saying, oh, look, now I'm losing my mind, but you're losing your mind for this.

53
00:07:10,000 --> 00:07:12,000
How can you be mindful of that?

54
00:07:12,000 --> 00:07:16,000
At that moment, that's the predominant mind state.

55
00:07:16,000 --> 00:07:18,000
Okay.

56
00:07:18,000 --> 00:07:20,000
So I don't know that.

57
00:07:20,000 --> 00:07:24,000
So you're supposed to be all the time mindful, right?

58
00:07:24,000 --> 00:07:28,000
You cannot become Buddha without being mindful all the time.

59
00:07:28,000 --> 00:07:32,000
Well, you're allowed a five minute break every four hours.

60
00:07:32,000 --> 00:07:41,000
So I just wanted to clarify, because one of you said,

61
00:07:41,000 --> 00:07:48,000
well, you'd now become a Buddha if you're 24-7 mindful of the different.

62
00:07:48,000 --> 00:07:52,000
And on the other hand, you have to be mindful of the time because, you know,

63
00:07:52,000 --> 00:07:53,000
because.

64
00:07:53,000 --> 00:07:55,000
A Buddha has to do much more than just be mindful.

65
00:07:55,000 --> 00:08:00,000
A Buddha has to come to know everything, according to the Tervada tradition anyway.

66
00:08:00,000 --> 00:08:09,000
So, as even for an era, it's something more than mindfulness, probably getting rid of all your thirds.

67
00:08:09,000 --> 00:08:11,000
I don't know about that.

68
00:08:11,000 --> 00:08:21,000
If you're truly mindful, and if you cultivate mindfulness, you really don't have to worry about the rest of the Buddha's teaching.

69
00:08:21,000 --> 00:08:23,000
Because it all comes into place.

70
00:08:23,000 --> 00:08:26,000
But you don't become a Samma Sam Buddha that way.

71
00:08:26,000 --> 00:08:30,000
Samma Sam Buddha has to learn everything, has to come to know everything,

72
00:08:30,000 --> 00:08:34,000
has to get to the point where their mind is able to understand.

73
00:08:34,000 --> 00:08:36,000
Able to know everything, not just let go of everything.

74
00:08:36,000 --> 00:08:46,000
That's the difference between an hour hunt and a Buddha from the Tervada tradition.

75
00:08:46,000 --> 00:08:52,000
But the point is not, I think it's more, if you can be clear about this,

76
00:08:52,000 --> 00:08:56,000
you don't have to be mindful every moment in order to become a Buddha.

77
00:08:56,000 --> 00:08:58,000
You have to be mindful of an hour hunt.

78
00:08:58,000 --> 00:09:00,000
You have to be mindful in one moment.

79
00:09:00,000 --> 00:09:07,000
And if in that one moment, you're perfectly and clearly aware of something for what it is as being impermanence,

80
00:09:07,000 --> 00:09:09,000
offering it on top.

81
00:09:09,000 --> 00:09:11,000
The next moment is enlightenment.

82
00:09:11,000 --> 00:09:17,000
The next moment is, well, the next few moments anyway is the process of becoming an hour hunt.

83
00:09:17,000 --> 00:09:21,000
But, of course, that moment has to be cultivated.

84
00:09:21,000 --> 00:09:27,000
It has to be led up to through constant practice of mindfulness.

85
00:09:27,000 --> 00:09:31,000
And that doesn't have to be, you know, every single moment.

86
00:09:31,000 --> 00:09:58,000
It just has to be enough moments to create a level of understanding and allows you to become an hour hunt.

