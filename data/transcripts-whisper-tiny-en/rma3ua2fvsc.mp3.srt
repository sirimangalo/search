1
00:00:00,000 --> 00:00:05,000
So I just graduated high school, quickly all my friends are moving away.

2
00:00:05,000 --> 00:00:15,000
It's been really hard on my soul. How does one accept this change and grow?

3
00:00:15,000 --> 00:00:18,000
Well, this is growing, really.

4
00:00:18,000 --> 00:00:23,000
The suffering that comes from change is teaching you something.

5
00:00:23,000 --> 00:00:27,000
It's teaching you the disadvantages in clinging.

6
00:00:27,000 --> 00:00:36,000
And it's teaching you really from an empirical, from a real, from a reality.

7
00:00:36,000 --> 00:00:39,000
I'm losing my words here. I'm losing my vocabulary.

8
00:00:39,000 --> 00:00:45,000
It's teaching you from a real, sorry, it's been a long night.

9
00:00:45,000 --> 00:00:48,000
It's really teaching you.

10
00:00:48,000 --> 00:00:53,000
As opposed to just thinking about it and saying, yeah, yeah, clinging has causes suffering.

11
00:00:53,000 --> 00:00:56,000
It's not something that's easy to understand.

12
00:00:56,000 --> 00:01:02,000
But when you experience change, when you lose the things that you cling to, then you see leads to suffering.

13
00:01:02,000 --> 00:01:07,000
And you see clearly from an empirical perspective.

14
00:01:07,000 --> 00:01:15,000
So living with it and learning to accept this as reality and learning to see deeper

15
00:01:15,000 --> 00:01:20,000
is really the core of the Buddha's teaching.

16
00:01:20,000 --> 00:01:22,000
Which is the great thing about this.

17
00:01:22,000 --> 00:01:24,000
The problem is people don't think like that.

18
00:01:24,000 --> 00:01:26,000
They think something's wrong.

19
00:01:26,000 --> 00:01:28,000
And they try to give you an answer.

20
00:01:28,000 --> 00:01:31,000
They say, come on, enjoy where you are now.

21
00:01:31,000 --> 00:01:33,000
Start to, you know, don't worry.

22
00:01:33,000 --> 00:01:35,000
You'll find new friends.

23
00:01:35,000 --> 00:01:40,000
Basically setting you up for the exact same suffering later on.

24
00:01:40,000 --> 00:01:45,000
The exact same disappointment.

25
00:01:45,000 --> 00:01:49,000
And until you learn and realize, really, if you cling to something, it's just going to bring you suffering.

26
00:01:49,000 --> 00:01:56,000
You continue to experience the same disappointment.

27
00:01:56,000 --> 00:02:00,000
So the best thing you can do is to accept it.

28
00:02:00,000 --> 00:02:03,000
And to say, you know, the problem is not that I move.

29
00:02:03,000 --> 00:02:05,000
The problem is that I was clinging.

30
00:02:05,000 --> 00:02:10,000
The problem is that I preferred these people.

31
00:02:10,000 --> 00:02:13,000
And these experiences.

32
00:02:13,000 --> 00:02:14,000
I preferred those.

33
00:02:14,000 --> 00:02:16,000
And I really enjoyed them.

34
00:02:16,000 --> 00:02:20,000
As opposed to accepting present moment experience.

35
00:02:20,000 --> 00:02:22,000
And the experience that you have right now, right?

36
00:02:22,000 --> 00:02:26,000
So the best thing for me right now is to accept that now I'm giving a talk.

37
00:02:26,000 --> 00:02:28,000
Or now I'm answering people's questions.

38
00:02:28,000 --> 00:02:31,000
As you can see, tonight I had to give an ordination.

39
00:02:31,000 --> 00:02:37,000
And then I answered some questions, then ordination, and you should have seen the things that have to go through your mind.

40
00:02:37,000 --> 00:02:39,000
Having to print something up for her.

41
00:02:39,000 --> 00:02:42,000
Having to get a razor and, you know, robes.

42
00:02:42,000 --> 00:02:44,000
And all the time considering in my mind, is this proper?

43
00:02:44,000 --> 00:02:46,000
Is this correct?

44
00:02:46,000 --> 00:02:47,000
What is it?

45
00:02:47,000 --> 00:02:53,000
You know, how exciting the present moment is.

46
00:02:53,000 --> 00:03:00,000
If you stay with it, how much is going on right here and right now?

47
00:03:00,000 --> 00:03:02,000
And how it changes so much.

48
00:03:02,000 --> 00:03:06,000
And if you get stuck on one experience, like I like this part, I don't like this part.

49
00:03:06,000 --> 00:03:11,000
You'll immediately set yourself up for suffering.

50
00:03:11,000 --> 00:03:18,000
So the best thing that you can do is to learn to not find a happiness in cling to the present moment,

51
00:03:18,000 --> 00:03:28,000
but to learn to not cling to anything, and to be happy without relying on any experience.

52
00:03:28,000 --> 00:03:31,000
Thank you.

