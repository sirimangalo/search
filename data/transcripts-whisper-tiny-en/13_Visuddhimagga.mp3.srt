1
00:00:00,000 --> 00:00:04,560
And in the teachings of the Buddha, you know, the virtue or sleep, you call it

2
00:00:04,560 --> 00:00:08,400
seala, the keeping of precepts.

3
00:00:08,400 --> 00:00:13,280
The virtue of, I mean, the beauty of what is very important, because it is here

4
00:00:13,280 --> 00:00:18,640
said the thumb ground of what you like, yeah, I feel the balance you

5
00:00:18,640 --> 00:00:27,280
on the ground. Yes. So, seala is compared to ground and energy is compared to

6
00:00:27,280 --> 00:00:33,160
the feet and feet or confidence is compared to a hand. Because if you have a

7
00:00:33,160 --> 00:00:36,760
hand, you can pick up things you want to pick up. If you do not have hands, you

8
00:00:36,760 --> 00:00:42,320
don't, you cannot. In the same way, if you do not have any confidence, then you cannot pick

9
00:00:42,320 --> 00:00:49,720
up a crucial, a good thing. So, and the hand is compared to, I mean, confidence

10
00:00:49,720 --> 00:00:58,480
or free, it is compared to a hand. And the foot, mother is compared to an X, which cuts,

11
00:00:58,480 --> 00:01:04,240
which cuts altogether that it meant a difference. So, if your conduct isn't

12
00:01:04,240 --> 00:01:08,840
fairly virtuous, then there is no way that you're not going to be involved in

13
00:01:08,840 --> 00:01:14,200
a creative moment. Because if you have a disadvantage with the new, whatever you call

14
00:01:14,200 --> 00:01:17,640
yourself, it's totally clear. And that's right, yes.

15
00:01:17,640 --> 00:01:22,000
What is an example of functional?

16
00:01:22,000 --> 00:01:28,720
Functional is something like, I gave this example, but Tim what didn't like that? He said

17
00:01:28,720 --> 00:01:36,560
it was true, maybe, I don't know. It's like a fruit, which does not bear, I mean, a

18
00:01:36,560 --> 00:01:44,160
tree which does not bear fruit. It still is growing, living, it still has branches and

19
00:01:44,160 --> 00:01:51,760
leaves and so on, but it's just not bear fruit, something like that. And at a hand, after

20
00:01:51,760 --> 00:01:58,200
becoming an argham, still do good things. They still teach people, still practice

21
00:01:58,200 --> 00:02:06,600
meditation and still help other people. But all their actions do not constitute

22
00:02:06,600 --> 00:02:14,120
karma, because they have periodically root of karma, I mean, the root of existence, that

23
00:02:14,120 --> 00:02:24,560
is the craving and ignorance. So, they are, their actions are just actions and their actions

24
00:02:24,560 --> 00:02:40,240
do not bring reactions, I mean, results. We, in Burma, at the New Year's ceremony, the

25
00:02:40,240 --> 00:02:48,120
canon for a fire. And so, when something is just, just, just noise and has no substance,

26
00:02:48,120 --> 00:02:55,520
and we say, this is the New Year canon or something like that. What do you call that?

27
00:02:55,520 --> 00:03:05,280
Volume of something? Yeah. So, it is something like that. Fruit, I mean, trees, they are

28
00:03:05,280 --> 00:03:18,680
in no fruit. Yes, it is called, it is called, it is a little bit of, yes. Okay, then, second

29
00:03:18,680 --> 00:03:30,280
one, some, some Buddha. Now, Buddha, we use the word Buddha to refer to him, but his

30
00:03:30,280 --> 00:03:42,160
full habitat is Samma Sam Pudha. Now, Samma is one prefix, Sam is one prefix, there are

31
00:03:42,160 --> 00:03:53,400
two prefixes here and Pudha. Samma means rightly, and Sam means by himself. So, he discovers

32
00:03:53,400 --> 00:04:00,400
the full noble truth. Buddha means to, to, to know. So, he knows the full noble truth. Or,

33
00:04:00,400 --> 00:04:08,400
he discovers the full noble truth, rightly, and without assistance from any person, by himself.

34
00:04:08,400 --> 00:04:14,000
So, we emphasize this when we talk about Buddha. Buddha are those, those persons who do

35
00:04:14,000 --> 00:04:22,280
not need any teachers. So, bodhisattva went to two teachers before, before he became

36
00:04:22,280 --> 00:04:31,320
the Buddha. But, he did not follow their advice, up to the attainment of Buddha. He,

37
00:04:31,320 --> 00:04:38,960
he discarded their, their practice, and then went to a place and practiced by himself.

38
00:04:38,960 --> 00:04:45,860
So, we say, Buddha had no teachers. And this is emphasized by this, Sam Buddha. So, Sam

39
00:04:45,860 --> 00:04:53,960
Buddha means self enlightened. And Samma Buddha means rightly enlightened. So, Samma Sam Buddha

40
00:04:53,960 --> 00:05:02,840
means rightly self enlightened, something like that. And here, to know means to know the

41
00:05:02,840 --> 00:05:16,600
full noble truth. And then, on page 211, paragraph 28, did the other things which Buddha

42
00:05:16,600 --> 00:05:22,680
knew are given. They are six basis, six groups of consciousness, and so on. So, these are

43
00:05:22,680 --> 00:05:30,480
the topics taught in Abidha. Actually, they are found in Suddha, Bhittagatu. But, they

44
00:05:30,480 --> 00:05:38,760
are treatment in full, can be found in Abidha. Sometimes, people take Abidha to be a,

45
00:05:38,760 --> 00:05:46,720
a separate, separate thing, not, not connected with Suddha or something like that. But,

46
00:05:46,720 --> 00:05:55,960
actually, those we found in the Abidha, most of them are also taught in Suddha. That is

47
00:05:55,960 --> 00:06:04,280
why we need a knowledge of Abidhamma to understand the Soodas. We should not separate them,

48
00:06:04,280 --> 00:06:14,320
treat them as a separate. We cannot study Soodas without a knowledge of Abidhamma. That

49
00:06:14,320 --> 00:06:22,840
means, that is, if we want to understand fully and correctly. So, these are topics taught

50
00:06:22,840 --> 00:06:36,440
in Abidhamma and Suddha also. So, here, Buddha's penetration into the full noble truth or

51
00:06:36,440 --> 00:06:54,200
discovery of full noble truth is, is meant by this second attribute, Sama, Sambuddha.

52
00:06:54,200 --> 00:07:10,360
Suddha is one who does not have a, who is enlightened by himself. And that is usually

53
00:07:10,360 --> 00:07:25,840
distinguished from Buddha, who isn't at least totally like by himself, or in the back of him.

54
00:07:25,840 --> 00:07:31,120
The particular Buddhas are not called Sama, Sambuddha. They are called just Sambuddha, or

55
00:07:31,120 --> 00:07:42,200
Sambuddha. But not Sambuddha. Rightly means, in all aspects, they do not understand

56
00:07:42,200 --> 00:07:50,160
in all aspects of the Dharma. They are self enlightened person, but they are knowledge

57
00:07:50,160 --> 00:08:02,120
maybe not as wide as comprehensive as that of the Buddhas. And also, sometimes they are called

58
00:08:02,120 --> 00:08:10,920
silent Buddhas. That comes from the fact that they do not teach much. Although, sometimes

59
00:08:10,920 --> 00:08:16,280
they teach, but they sell them to you. They want to be away from people and live in the

60
00:08:16,280 --> 00:08:22,400
forest. And so, they are called silent Buddhas. Sometimes it is because they were in

61
00:08:22,400 --> 00:08:28,920
talk, it is hard for them to teach. And that is usually kind of the conscious decisions

62
00:08:28,920 --> 00:08:38,560
they do not have. And also, they appear only when they are there are no Buddhas. So, they

63
00:08:38,560 --> 00:08:51,140
appear between the influence of Buddhas. And then, I hope you read the foot notes. It

64
00:08:51,140 --> 00:09:00,160
looks very good. So, we will go next to the third one. He is endowed with clear vision

65
00:09:00,160 --> 00:09:08,560
and virtuous conduct. There are two words here, weak jar and chaturana. Weak jar means understanding

66
00:09:08,560 --> 00:09:18,120
of vision. And chaturana means conduct. So, Buddha is endowed with both clear vision

67
00:09:18,120 --> 00:09:25,760
and conduct. Clear, there are three kinds of clear vision. That means remembering his

68
00:09:25,760 --> 00:09:36,160
past lives and then divine eye and destruction of the filaments. They are all three clear

69
00:09:36,160 --> 00:09:49,320
visions. So, whenever three is mentioned, these are men. Remembering past lives, divine vision

70
00:09:49,320 --> 00:09:56,120
and destruction of the filaments. These are called three clear visions. Sometimes each

71
00:09:56,120 --> 00:10:07,480
kinds of clear visions are mentioned. So, in that case, there are some more. So, the eight

72
00:10:07,480 --> 00:10:13,960
kinds are stated in the unbekshasura. For there, eight kinds of clear vision are stated.

73
00:10:13,960 --> 00:10:20,280
And eight are made up of six kinds of direct knowledge together with insights and the

74
00:10:20,280 --> 00:10:26,160
supernormal power of the mind-bait glory. So, there are eight kinds of clear visions

75
00:10:26,160 --> 00:10:30,880
mentioned in that chaturana. But, if you go to that chaturana, you will not find them

76
00:10:30,880 --> 00:10:38,760
clearly stated. Because, in pari, when they don't want to repeat, they just put the

77
00:10:38,760 --> 00:10:50,960
sign there. Like, you use dots, right? Three or four dots. So, in pari also, they use

78
00:10:50,960 --> 00:10:59,040
the letter P A or sometimes L A. So, when you see this, you understand that there are

79
00:10:59,040 --> 00:11:04,840
something missing here or they don't want to repeat. So, you have to go back to where

80
00:11:04,840 --> 00:11:11,160
they first occur. And, if you don't know where to find them, then, you will be lost.

81
00:11:11,160 --> 00:11:25,840
So, I looked them up in D-100, D-100. And, they were not present in the book. Because,

82
00:11:25,840 --> 00:11:34,360
they were mentioned in the second chaturana. They did pretty much a chaturana. So, this

83
00:11:34,360 --> 00:11:41,360
is one problem with as modern people. These books are meant to be read from the beginning

84
00:11:41,360 --> 00:11:50,760
to the end. Not just because it is soda in the mirror and you read it and you. So, they

85
00:11:50,760 --> 00:11:57,640
are meant to be studied from the beginning. I can guess you the page numbers of the

86
00:11:57,640 --> 00:12:11,440
English translation. So, the A, the six kinds of direct knowledge. That means, the performing

87
00:12:11,440 --> 00:12:21,400
miracles that is one. And, then, divine ear and then, reading other people's mind and

88
00:12:21,400 --> 00:12:31,040
remembering past lives. And, then, divine eye. You get five, right? And, then, destruction

89
00:12:31,040 --> 00:12:39,080
of mental defilements. Six. And, then, two are mentioned here. We pass on our insight.

90
00:12:39,080 --> 00:12:44,680
And, super normal power of the mind-made body. That means, you break this chaturana and

91
00:12:44,680 --> 00:12:53,120
then, you are able to multiply yourself. So, these are the eight kinds of clear vision

92
00:12:53,120 --> 00:13:00,320
stage that in that soul. And, my contact is meant 15 things here. Restained by virtue,

93
00:13:00,320 --> 00:13:05,320
guarding the doors of the sense, like all these knowledge of the right amount in eating,

94
00:13:05,320 --> 00:13:10,100
devotion to wakefulness. The seven good states, they are in the footnote. And, the four

95
00:13:10,100 --> 00:13:16,640
two genres of the fine material sphere. Or, it is precisely by means of these 15 things

96
00:13:16,640 --> 00:13:22,000
that a noble disciple contacts himself, that he goes towards the dead death. That is

97
00:13:22,000 --> 00:13:27,640
why it is called virtue conduct, according to as it is said. Now, you have to understand

98
00:13:27,640 --> 00:13:35,320
the pali word chirana to understand this explanation. The word chirana comes from the root

99
00:13:35,320 --> 00:13:44,680
chirah, C-A-R-A. And, chirah means to walk, to go. So, chirana means some means of going.

100
00:13:44,680 --> 00:13:55,800
So, these 15 are the means of going to the version of lipana. And, this is why it

101
00:13:55,800 --> 00:14:02,960
is said here. It is precisely by means of these 15 things that a noble disciple contacts

102
00:14:02,960 --> 00:14:12,600
himself, that he goes towards the dead death. That is why it is called pali chirana.

103
00:14:12,600 --> 00:14:22,320
And then, we will go to the next one. Subline, paragraph 33. Now, please remember the

104
00:14:22,320 --> 00:14:33,600
pali word chirah, su-gattah, because the English translation subline may not make much

105
00:14:33,600 --> 00:14:47,400
sense here. So, su-gattah, su means, in the first meaning, it is good. So, su means good

106
00:14:47,400 --> 00:14:55,480
and gattah means going. So, su-gattah means good going. That means Buddha has a good going.

107
00:14:55,480 --> 00:15:03,360
That is called Buddha. That is why Buddha is called su-gattah. Because, he is going is purified

108
00:15:03,360 --> 00:15:10,160
and blameless. And, what is that? That is a noble fact. So, noble, if you look at this

109
00:15:10,160 --> 00:15:21,560
of here as a good, good going. So, that is the first meaning, su-gattah. Now, the second

110
00:15:21,560 --> 00:15:33,240
meaning is also gone to an excellent place. Now, via gattah means gone, not like in the

111
00:15:33,240 --> 00:15:42,960
first meaning. In the second meaning, gattah means gone to, su means an excellent place.

112
00:15:42,960 --> 00:15:48,920
So, one who has gone to an excellent place is called su-gattah. Here, excellent place

113
00:15:48,920 --> 00:16:00,520
is nibana. Now, the third meaning of having gone rightly, now, su can mean also rightly.

114
00:16:00,520 --> 00:16:11,520
So, who has gone rightly is called su-gattah. And, rightly, he means not approaching to the

115
00:16:11,520 --> 00:16:21,240
extremes, but following the middle path. And, the last one, the fourth meaning, because

116
00:16:21,240 --> 00:16:31,400
of the intensity rightly. Now, here, the original one is su-gattah with a D, D as in David. And,

117
00:16:31,400 --> 00:16:42,440
that D is changed to T. It is a polygrammatical explanation. So, here, it means su-gattah.

118
00:16:42,440 --> 00:16:52,080
And that means speaking or saying, and su means rightly. So, one who speaks rightly is called su-gattah.

119
00:16:52,080 --> 00:17:00,680
So, there are four meanings to this word. So, with regard to the fourth meaning, the

120
00:17:00,680 --> 00:17:10,160
soda is coated here, paragraph 35. Such speaks as the perfect one knows to be an incorrect,

121
00:17:10,160 --> 00:17:16,000
or this is to harm and displeasing and unwelcome to others, that it does not speak and so on.

122
00:17:16,000 --> 00:17:26,320
Now, according to the soda, there are six kinds of speeches. And, only two of them,

123
00:17:26,320 --> 00:17:36,640
Buddha's use. So, those six are mentioned here in the transition of the soda. So, in

124
00:17:36,640 --> 00:17:45,760
brief, they mean speech which is untrue, harmful and displeasing. Buddha's do not do such speech.

125
00:17:48,400 --> 00:17:55,360
And then, speech which is true, but harmful and displeasing to the heroes. This also

126
00:17:55,360 --> 00:18:04,560
Buddha does do not use. And, the third one is true beneficial. That means not humming. True

127
00:18:04,560 --> 00:18:16,080
beneficial, but displeasing to the listeners. That Buddha's use. But, here in the

128
00:18:16,080 --> 00:18:24,240
translation, it is said, but the one knows the time to expand. That means, if it is time to say

129
00:18:24,240 --> 00:18:29,760
such a speech, then Buddha's use this speech. So, it may not be pleasing to the listeners,

130
00:18:29,760 --> 00:18:37,360
but if it is beneficial and it is true, then Buddha will say, what is the meaning of harmful,

131
00:18:37,360 --> 00:18:51,360
what is harmful and that means, if you follow his words and you will get benefits,

132
00:18:51,360 --> 00:19:01,120
or if you follow his words, you will come to the goal, opposite of that. You will come to

133
00:19:01,120 --> 00:19:12,400
feel you or something. Sometimes, people talk to other people. People give advice to other people.

134
00:19:12,400 --> 00:19:26,160
And, those advice may not be conducive to success. So, which is true beneficial and displeasing,

135
00:19:26,160 --> 00:19:36,320
Buddha's do such speeches. Then, the fourth one, untrue, harmful, but pleasing to other people.

136
00:19:36,320 --> 00:19:47,600
Buddha has never used this. And, then true, harmful, pleasing. No, Buddha never used.

137
00:19:48,880 --> 00:19:55,920
And, the last one, true beneficial and pleasing to the listeners. So, Buddha used only two kinds

138
00:19:55,920 --> 00:20:03,040
of, one of the two kinds of speeches. True beneficial, displeasing or true beneficial,

139
00:20:03,040 --> 00:20:11,520
pleasing, the other Buddha does not use. That is, a Buddha is described as sukata, one who speaks

140
00:20:11,520 --> 00:20:26,160
rightly. Then, no one of the words, walk away going. You may have read this. It is something

141
00:20:26,160 --> 00:20:34,960
like, in the commentary, something like a Buddhist cosmology. And, not all of them

142
00:20:37,760 --> 00:20:43,200
can actually be found in the soldiers. Some of them can be found in the soldiers. And, others are

143
00:20:44,160 --> 00:20:51,280
developed, maybe later after the death of the Buddha. So, you may take them as

144
00:20:51,280 --> 00:21:09,760
just a couple of them. The grave is all. Because, if you look at the world right now,

145
00:21:12,240 --> 00:21:19,120
according to the knowledge of modern science about the world, then they are

146
00:21:19,120 --> 00:21:31,600
very different from them. And, a lot, Indians are maybe, the western people say, Indians are

147
00:21:32,160 --> 00:21:40,960
a form of using numbers, 84,000, something like that. So, here also, lower of the world.

148
00:21:40,960 --> 00:21:49,760
Now, there are three kinds of words mentioned here. The world of formations, the world of beings,

149
00:21:49,760 --> 00:21:58,960
and the world of location. You will find that in paragraph 37, the world of formations,

150
00:21:58,960 --> 00:22:03,680
the world of beings, and the world of location. The world of formations really means,

151
00:22:03,680 --> 00:22:11,760
the world of both animals, I mean, animate and in animating, both beings and in animating.

152
00:22:12,480 --> 00:22:17,280
The world of beings, just beings, and the world of location means outside, outside of war.

153
00:22:18,240 --> 00:22:25,040
And, so, Buddha, this is the other one. I mean, in animate war, like the earth,

154
00:22:25,040 --> 00:22:36,960
and more than rivers and so on. And, if you can draw a diagram of what is mentioned here,

155
00:22:36,960 --> 00:22:46,400
you will get a rough picture of the Buddhist conception of the world and the other things,

156
00:22:46,400 --> 00:22:57,360
I mean, the sun, the moon, the universe. Then, in paragraph 38, the first word, likewise,

157
00:22:57,360 --> 00:23:09,360
I think, should go. Not likewise, but it should be four, or maybe in detail, because paragraph

158
00:23:09,360 --> 00:23:17,840
38 and those following are the detailed description of how Buddha knows the world.

159
00:23:20,480 --> 00:23:25,120
So, one world, all beings, all beings, subsets by human and so on.

160
00:23:29,920 --> 00:23:36,960
Then, paragraph 39, he knows all beings, that is very important. He knows all beings, habits.

161
00:23:36,960 --> 00:23:42,160
He knows they are in here and tendencies, knows they are temperaments, knows they are bench,

162
00:23:42,160 --> 00:23:47,920
knows them, as with little dust in their eyes and with much dust on their eyes.

163
00:23:47,920 --> 00:23:52,320
With key faculties and with difficulties, with good behavior and with bad behavior,

164
00:23:52,320 --> 00:23:58,800
it's to teach and easy to teach and hard to teach, capable and incapable of achievement.

165
00:23:58,800 --> 00:24:05,360
Therefore, this world of beings was known to him in all ways. So, Buddha, only Buddhas

166
00:24:05,360 --> 00:24:13,200
have this ability of knowing exactly the beings, habits in here and tendencies and so on.

167
00:24:15,200 --> 00:24:23,840
Even the other hands do not possess this ability. And then, Buddha's understanding of the physical world

168
00:24:23,840 --> 00:24:37,120
they developed, there are some statements with regard to the physical world and then,

169
00:24:37,120 --> 00:24:45,840
maybe they have developed later on. So, this description of the world according to, say, the Buddhists.

170
00:24:45,840 --> 00:24:54,320
And also, the footnote is very helpful and for me to get back out of this.

171
00:24:55,360 --> 00:25:07,360
You will get a view of the Buddhist universe. Buddhist universe is a round, round thing,

172
00:25:07,360 --> 00:25:16,640
not like a clue, but it is round and it is surrounded by what we call the wild-sided mountains.

173
00:25:16,640 --> 00:25:25,360
So, there are mountains around and then, in this circle, there is water and also, the full

174
00:25:25,360 --> 00:25:32,720
great islands, we call them the full great islands, maybe the continents. And each island

175
00:25:32,720 --> 00:25:40,480
has descended above each, believe it or not, each island has 500 small islands.

176
00:25:42,640 --> 00:25:47,520
And in the middle of this universe, there is the Mount Siniru or Meiru.

177
00:25:49,440 --> 00:25:58,640
And it is not visible to human beings. And surrounding the Meiru, this mountain, great mountain,

178
00:25:58,640 --> 00:26:07,680
there are seven constantly mountains and lower mountains, seven. And in between these seven,

179
00:26:07,680 --> 00:26:16,160
there is water. So, they are called inner oceans or something. There is the outer ocean and then,

180
00:26:17,360 --> 00:26:25,520
they are inner ocean, seven inner oceans. And they are the high, the Siniru is it is a

181
00:26:25,520 --> 00:26:32,960
few thousand weeks. And then, the first, the highest mountain, the surrounding mountain,

182
00:26:32,960 --> 00:26:38,560
is half of it, that height. And then, next one is half that height and so on. But if you,

183
00:26:38,560 --> 00:26:43,280
if you want to draw according to scale, it is very difficult. I have tried it, but

184
00:26:45,120 --> 00:26:52,400
I cannot, because you have to draw a big drawing, not on a small table, because half,

185
00:26:52,400 --> 00:27:02,240
height, half, height, half, height, half, I do so. And, and that is called one, one,

186
00:27:03,680 --> 00:27:10,800
wall cycle. Let's say one universe. And suns and moon are said to go around this,

187
00:27:11,760 --> 00:27:19,840
around the Meiru, the middle, the middle, the middle mountain, at its half height. And so,

188
00:27:19,840 --> 00:27:35,200
the sun and moon are only, then before 42,000 weeks from the earth. And the size of the sun,

189
00:27:35,200 --> 00:27:41,920
it is 50 weeks and the size of the moon is 49 weeks. There is only one, one big difference

190
00:27:41,920 --> 00:27:48,800
between the size of the moon and size of the sun. And then, there will be the stars and planets

191
00:27:48,800 --> 00:27:58,960
who are around them. And it is said that, while the sun goes around the Meiru, then it,

192
00:27:59,920 --> 00:28:06,960
it flows a shell, right? And so, what three continents are like, one continent is dark,

193
00:28:08,080 --> 00:28:10,000
and that is nice, and the others are dead.

194
00:28:10,000 --> 00:28:25,520
So, only one quarter of the universe is, is dark, and the three, three, three, the other three

195
00:28:25,520 --> 00:28:36,720
paths are like, at one time. And the, there are four great islands. And the south island is called

196
00:28:36,720 --> 00:28:48,240
Jambutiba. And it is, it is a place where Buddha's, Buddha's, and all, all of the white men and

197
00:28:48,240 --> 00:29:01,360
fewer people are born. And that land is described as something like white, white, white in one,

198
00:29:01,360 --> 00:29:10,000
one, one side, and then, what about it, you know? So being all, something. It becomes more

199
00:29:10,000 --> 00:29:19,200
than the other side. So, that fits with India. So, India is, India is Jambutiba, the southern island.

200
00:29:20,000 --> 00:29:28,160
So, if India is southern island, then you should be western island and China or Japan should be

201
00:29:28,160 --> 00:29:39,360
eastern island. And I think America should be the northern island, because when it is day in Asia,

202
00:29:39,360 --> 00:29:51,600
when it is night here. So, America should be the north, not great island, which is called

203
00:29:51,600 --> 00:30:06,240
Kukterakuru. So, this is the cosmology of Buddhism. Now, there is one thing

204
00:30:06,240 --> 00:30:25,200
on pitch, 220. You see the paragraph number 43. Those lines, 1, 2, 3, 4, 5. They are misplaced.

205
00:30:26,560 --> 00:30:32,480
The wall sphere mountains line of some exchanges down and you can see just 280,000 lakes in

206
00:30:32,480 --> 00:30:40,800
towers are in light be with. And winning one wall element all around in pitch and tiredly. So,

207
00:30:40,800 --> 00:30:51,840
these lines should be on pitch 221, just above paragraph 44. And not here. I don't know why,

208
00:30:52,400 --> 00:30:58,480
why he put this here, maybe by mistake. So, these lines should be on the other pitch,

209
00:30:58,480 --> 00:31:12,480
based on 121. Now, here you see, you find the moon's distance for the nine

210
00:31:12,480 --> 00:31:21,040
lakes across and the sun's distance is 50 lakes. Therefore, this wall of location was

211
00:31:21,040 --> 00:31:26,080
noted him and always too, so he is north of walls, because he has seen the wall in all ways.

212
00:31:26,080 --> 00:31:36,240
Buddha is called a local, we will know of the wall, know of the wall of beings or physical wall

213
00:31:36,800 --> 00:31:42,400
and wall of formations or wall of conditioned things. Therefore, Buddha is called

214
00:31:42,400 --> 00:31:54,560
know of the walls. And when we, when we, we, the sub commentary is, we find more of these descriptions.

215
00:31:57,040 --> 00:32:04,960
I don't know where, where they come from. Actually, not from the students, but I don't know what

216
00:32:04,960 --> 00:32:14,960
they need, they need, got them from some writings of Hindus, because there are such boots and

217
00:32:14,960 --> 00:32:21,440
Hinduism. And so, they may have got them from them. But they are not important actually.

218
00:32:21,440 --> 00:32:36,560
And one thing is, so one thing is, Himalayas in this book, Himalayas is 500 lakes high. 500

219
00:32:36,560 --> 00:32:52,960
lakes means one lake is above eight miles. So, how many miles? 4,000 miles high? Sometimes

220
00:32:55,360 --> 00:33:05,040
they measure not by height, maybe by path. So, do we, do we, do we, do we, do we, do we do

221
00:33:05,040 --> 00:33:15,360
clients in a number of miles? Say, long term of height is, how many miles high? Maybe one

222
00:33:15,360 --> 00:33:26,160
mile or two miles high? No, no, not from the sea level. I mean, measuring, like with the pool,

223
00:33:26,160 --> 00:33:35,600
measuring by path. We say, as the crow flies direct, this is not as the crow flies. No,

224
00:33:35,600 --> 00:33:44,560
really, that's why we're showing my, say, Himalayas is 500 miles high and so on. And you know,

225
00:33:44,560 --> 00:34:02,080
it will not be, how many miles high? Among the average is 5 miles high, right? But do you think

226
00:34:02,080 --> 00:34:16,720
any referring to this, the world as we know it or the universe as we know it or is it? Some kind of

227
00:34:16,720 --> 00:34:32,320
indicated mirror cannot be seen. So, it may be, you know, some kind of a symbolic thing. But, you know,

228
00:34:32,320 --> 00:34:47,680
one, one statement, conservatives another, because it is said that when three islands are like one

229
00:34:47,680 --> 00:34:54,960
island is dark. So, following this, this, this world is what, what, what, what this meant by

230
00:34:54,960 --> 00:35:03,680
the local in our post? Because, you know, one part of the world is dark when the others are light,

231
00:35:04,960 --> 00:35:14,640
not exactly one, one quarter, but something like that. Maybe they are, they are knowledge of

232
00:35:14,640 --> 00:35:23,600
geography is very limited in those days. And then there may be seizures who just contemplate and

233
00:35:23,600 --> 00:35:32,880
then talked about these things. And they came to be accepted. No, nobody seemed to

234
00:35:32,880 --> 00:35:42,960
bother about measuring the height of Himalayas or Mount Everest. And they had no way, no means of

235
00:35:44,400 --> 00:35:51,200
measuring the height of mountains in those days. So, they may be just measuring by like 5.

236
00:35:51,200 --> 00:36:02,880
But, they are, they are not, not really important things. So, next time,

237
00:36:02,880 --> 00:36:32,720
maybe another 20 pages.

