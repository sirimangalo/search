1
00:00:00,000 --> 00:00:29,980
I'm not going to do a video broadcast this evening.

2
00:00:29,980 --> 00:00:41,980
I have a midterm exam tomorrow on Mahayana Buddhism.

3
00:00:41,980 --> 00:00:44,980
You have to talk about the Lotus Sutra.

4
00:00:44,980 --> 00:00:58,980
We had an interesting class yesterday where I asked the professor,

5
00:00:58,980 --> 00:01:03,980
where the Lotus Sutra situates itself,

6
00:01:03,980 --> 00:01:05,980
where the Lotus Sutra recorded tradition.

7
00:01:05,980 --> 00:01:07,980
When did it happen?

8
00:01:07,980 --> 00:01:10,980
When was it when was it taught?

9
00:01:10,980 --> 00:01:15,980
According to tradition, not according to history or it's called

10
00:01:15,980 --> 00:01:18,980
Scholasticism.

11
00:01:18,980 --> 00:01:21,980
The interesting part of his answer when he started talking about,

12
00:01:21,980 --> 00:01:27,980
what is the Lotus Sutra?

13
00:01:27,980 --> 00:01:30,980
Anyway, I don't know if it's not all that interesting.

14
00:01:30,980 --> 00:01:35,980
I forget about it.

15
00:01:35,980 --> 00:01:42,980
But tonight we have a quote about the five hundredths.

16
00:01:42,980 --> 00:01:47,980
Let's talk about that instead.

17
00:01:47,980 --> 00:01:51,980
We talk about the five hundredths as often in our practice,

18
00:01:51,980 --> 00:01:55,980
in the practice of Buddhist meditation.

19
00:01:55,980 --> 00:02:03,980
And I think they're often a source of suffering for people

20
00:02:03,980 --> 00:02:08,980
who feel bad that they still have these things.

21
00:02:08,980 --> 00:02:11,980
They feel guilty when they arise.

22
00:02:11,980 --> 00:02:15,980
And that's not in and of itself wrong,

23
00:02:15,980 --> 00:02:19,980
but it's not entirely beneficial.

24
00:02:19,980 --> 00:02:24,980
It doesn't solve the problem to feel guilty or to feel bad about these things.

25
00:02:24,980 --> 00:02:28,980
Because they are wrong, and they should be understood as

26
00:02:28,980 --> 00:02:30,980
a problem.

27
00:02:30,980 --> 00:02:40,980
Or for a person who thinks there's something good to be had,

28
00:02:40,980 --> 00:02:43,980
there's some benefit to be had from these things.

29
00:02:43,980 --> 00:02:46,980
That person is deluded.

30
00:02:46,980 --> 00:02:51,980
A person who wants to be happy and yet actively engages

31
00:02:51,980 --> 00:02:59,980
in cultivating the five hundredths is acting

32
00:02:59,980 --> 00:03:03,980
inconsistent with their own desires,

33
00:03:03,980 --> 00:03:08,980
but their own intention, their own goals.

34
00:03:08,980 --> 00:03:12,980
These things are intrinsically injured,

35
00:03:12,980 --> 00:03:14,980
hindrances.

36
00:03:14,980 --> 00:03:19,980
They have intrinsic hindering potential.

37
00:03:19,980 --> 00:03:21,980
There's no inships about it.

38
00:03:21,980 --> 00:03:26,980
They're just not good to eat.

39
00:03:26,980 --> 00:03:31,980
But I think this quote,

40
00:03:31,980 --> 00:03:34,980
it's quotes like this that we should really look at

41
00:03:34,980 --> 00:03:35,980
and take at face value.

42
00:03:35,980 --> 00:03:38,980
We shouldn't go the extra step and say,

43
00:03:38,980 --> 00:03:43,980
oh, when I feel anger, I should be upset at myself for that.

44
00:03:43,980 --> 00:03:45,980
I should feel bad about that.

45
00:03:45,980 --> 00:03:50,980
We should look at quotes like this and try to understand them,

46
00:03:50,980 --> 00:03:59,980
try to have this sort of inspiration arise in us.

47
00:03:59,980 --> 00:04:01,980
To see that they weaken our wisdom,

48
00:04:01,980 --> 00:04:04,980
sense desires and obstruction and hindrance,

49
00:04:04,980 --> 00:04:07,980
which enshrouds the mind and weak and twisted.

50
00:04:07,980 --> 00:04:09,980
Ill will sloth and torpor,

51
00:04:09,980 --> 00:04:12,980
restlessness and worry and doubt.

52
00:04:12,980 --> 00:04:16,980
Obstructions and hindrance is which enshroud the mind

53
00:04:16,980 --> 00:04:18,980
and weak and twisted.

54
00:04:18,980 --> 00:04:21,980
Surely it is possible that after abandoning

55
00:04:21,980 --> 00:04:24,980
these obstructions and hindrances which grow in and up

56
00:04:24,980 --> 00:04:27,980
over the mind and weak and wisdom,

57
00:04:27,980 --> 00:04:29,980
being strong in wisdom,

58
00:04:29,980 --> 00:04:31,980
one should know one's only good.

59
00:04:31,980 --> 00:04:34,980
The good of others, the good of both.

60
00:04:34,980 --> 00:04:36,980
And attain that knowledge and vision

61
00:04:36,980 --> 00:04:40,980
will be fitting noble ones and transcending human states.

62
00:04:40,980 --> 00:04:44,980
And that's it.

63
00:04:44,980 --> 00:04:50,980
I mean, it could easily lead you to

64
00:04:50,980 --> 00:04:54,980
freak out because you realize you're on the wrong path

65
00:04:54,980 --> 00:04:57,980
with all these hindrances inside.

66
00:04:57,980 --> 00:05:01,980
That's not all that productive at all.

67
00:05:01,980 --> 00:05:05,980
It's not the path, it's not the way to overcome them.

68
00:05:05,980 --> 00:05:07,980
Once you understand this,

69
00:05:07,980 --> 00:05:13,980
and once you see the problem in here and

70
00:05:13,980 --> 00:05:16,980
or even just see things as they are,

71
00:05:16,980 --> 00:05:19,980
you don't have to judge ever really.

72
00:05:19,980 --> 00:05:22,980
But that's, I think, key in the practice.

73
00:05:22,980 --> 00:05:28,980
So not only is it useless to judge bad things.

74
00:05:28,980 --> 00:05:33,980
It's also not the way to get rid of them.

75
00:05:33,980 --> 00:05:38,980
No, it's not only useless,

76
00:05:38,980 --> 00:05:48,980
but where I was with that,

77
00:05:48,980 --> 00:05:57,980
the way to be free from the hindrances

78
00:05:57,980 --> 00:06:01,980
is to just look to see clearly.

79
00:06:01,980 --> 00:06:04,980
You don't even have to, in the end,

80
00:06:04,980 --> 00:06:07,980
realize they're bad, as I say.

81
00:06:07,980 --> 00:06:11,980
It's not only, it doesn't lead to any benefit.

82
00:06:11,980 --> 00:06:16,980
It's not the end result.

83
00:06:16,980 --> 00:06:19,980
We are just trying to see things as hard.

84
00:06:19,980 --> 00:06:21,980
When you see things as they are,

85
00:06:21,980 --> 00:06:24,980
the hindrances don't actually arrive.

86
00:06:24,980 --> 00:06:28,980
They arise based on misconception.

87
00:06:28,980 --> 00:06:32,980
Misunderstanding.

88
00:06:32,980 --> 00:06:36,980
Misperception.

89
00:06:36,980 --> 00:06:40,980
Perversion of perception.

90
00:06:40,980 --> 00:06:43,980
If you have, there's a beautiful thing in front of you,

91
00:06:43,980 --> 00:06:48,980
and see it as it is, as seeing.

92
00:06:48,980 --> 00:06:50,980
You see the reality of it.

93
00:06:50,980 --> 00:06:53,980
There's no beauty in reality.

94
00:06:53,980 --> 00:07:00,980
There's no appealing quality to reality.

95
00:07:00,980 --> 00:07:02,980
There's no displacing quality either,

96
00:07:02,980 --> 00:07:05,980
and there's no ugliness in nature either.

97
00:07:05,980 --> 00:07:12,980
Those aren't intrinsic qualities of anything.

98
00:07:12,980 --> 00:07:16,980
So when you see things, when you see clearly,

99
00:07:16,980 --> 00:07:21,980
it's not arbitrary, and it's not specific to Buddhism,

100
00:07:21,980 --> 00:07:23,980
it's not a belief based thing.

101
00:07:23,980 --> 00:07:25,980
When you see things as they are,

102
00:07:25,980 --> 00:07:27,980
the hindrances don't arrive.

103
00:07:27,980 --> 00:07:33,980
And that's what we should focus on.

104
00:07:33,980 --> 00:07:37,980
Now, I guess another thing about this sort of quote is,

105
00:07:37,980 --> 00:07:42,980
it's a reflection, sort of a premeditative reflection,

106
00:07:42,980 --> 00:07:46,980
because the thing is, the quote is,

107
00:07:46,980 --> 00:07:49,980
surely it is possible.

108
00:07:49,980 --> 00:07:53,980
Surely we can find a way.

109
00:07:53,980 --> 00:07:56,980
So when you think like that, then it's an impetus

110
00:07:56,980 --> 00:07:59,980
for you to practice meditation.

111
00:07:59,980 --> 00:08:04,980
It's not actually meditation to be moaned negative aspects

112
00:08:04,980 --> 00:08:06,980
of these hindrances.

113
00:08:06,980 --> 00:08:09,980
It is useful to see the problem with it,

114
00:08:09,980 --> 00:08:12,980
to see how when you engage in anger, on your gauge,

115
00:08:12,980 --> 00:08:15,980
and greed, or when you engage in delusion,

116
00:08:15,980 --> 00:08:20,980
it clouds your mind, makes you unable to see clearly.

117
00:08:20,980 --> 00:08:23,980
But the great thing is, you don't have to feel guilty.

118
00:08:23,980 --> 00:08:25,980
The practice isn't feeling guilty about it.

119
00:08:25,980 --> 00:08:27,980
You just have to, in that moment,

120
00:08:27,980 --> 00:08:31,980
when you realize that you're angry, or greedy, or debutant,

121
00:08:31,980 --> 00:08:33,980
you just have to say to yourself,

122
00:08:33,980 --> 00:08:37,980
liking, liking, just liking, actually remind yourself

123
00:08:37,980 --> 00:08:39,980
of what you're experiencing.

124
00:08:39,980 --> 00:08:41,980
Clear your mind.

125
00:08:41,980 --> 00:08:44,980
Be clear about what you're experiencing.

126
00:08:44,980 --> 00:08:50,980
And if there's no debate,

127
00:08:50,980 --> 00:08:56,980
when you do that, your mind is clear.

128
00:08:56,980 --> 00:08:58,980
And so the practice is quite,

129
00:08:58,980 --> 00:09:00,980
it means right here in front of us.

130
00:09:00,980 --> 00:09:02,980
It's not something you have to look to find.

131
00:09:02,980 --> 00:09:08,980
You have to learn over a course of years,

132
00:09:08,980 --> 00:09:10,980
right here in front of you.

133
00:09:10,980 --> 00:09:12,980
So there should never be discouraged

134
00:09:12,980 --> 00:09:14,980
if we have defilements, or that our defilements

135
00:09:14,980 --> 00:09:18,980
are too many to think,

136
00:09:18,980 --> 00:09:20,980
to entrench.

137
00:09:20,980 --> 00:09:25,980
You just have to be present here and now.

138
00:09:25,980 --> 00:09:31,980
And you have to try to always be present if you can do it.

139
00:09:31,980 --> 00:09:34,980
Often, and if he didn't, it becomes a bit

140
00:09:34,980 --> 00:09:37,980
true, and it changes who you are.

141
00:09:37,980 --> 00:09:39,980
You come to see clearly.

142
00:09:39,980 --> 00:09:42,980
The hindrance is, we can,

143
00:09:42,980 --> 00:09:45,980
and your life improves, your mind improves,

144
00:09:45,980 --> 00:09:47,980
your wisdom improves,

145
00:09:47,980 --> 00:09:50,980
and eventually you come to know,

146
00:09:50,980 --> 00:09:52,980
slowly you come to know.

147
00:09:52,980 --> 00:09:53,980
In stages, the good your own,

148
00:09:53,980 --> 00:09:54,980
what is your own benefit?

149
00:09:54,980 --> 00:09:56,980
What is the benefit of others?

150
00:09:56,980 --> 00:09:59,980
What is it for the benefit of both?

151
00:09:59,980 --> 00:10:02,980
And you attain knowledge and vision of the moment.

152
00:10:02,980 --> 00:10:09,980
Okay, so that's where I'm done with what for today.

153
00:10:09,980 --> 00:10:13,980
I'm going to head off and do a little bit of study,

154
00:10:13,980 --> 00:10:16,980
because I'm not a sitcher.

155
00:10:16,980 --> 00:10:40,980
I'm going to be right.

