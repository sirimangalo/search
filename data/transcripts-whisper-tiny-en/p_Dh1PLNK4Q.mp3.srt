1
00:00:00,000 --> 00:00:23,000
Oh, good evening everyone, and welcome to our evening broadcast.

2
00:00:23,000 --> 00:00:44,000
So last night we talked about the bad things that come of meditation.

3
00:00:44,000 --> 00:01:11,000
The necessary evil, I suppose, are the results of the practice in terms of how it allows you to see the problems that create suffering.

4
00:01:11,000 --> 00:01:23,000
So the negative side of the practice.

5
00:01:23,000 --> 00:01:38,000
But of course it's not all negative, in fact there's great benefit, great goodness involved in seeing the problems in this way.

6
00:01:38,000 --> 00:01:47,000
It allows you to realize the benefits of the practice, which are many.

7
00:01:47,000 --> 00:02:14,000
So I thought I'd go over those tonight.

8
00:02:18,000 --> 00:02:30,000
If someone was voices and working to study, send them a message, I think.

9
00:02:30,000 --> 00:02:46,000
So the first way of understanding benefits, of course we're going by the Buddhist texts themselves.

10
00:02:46,000 --> 00:02:54,000
We have what we call the five aims of the practice of satipatana.

11
00:02:54,000 --> 00:03:06,000
But it gave us a list of five reasons for practicing mindfulness.

12
00:03:06,000 --> 00:03:32,000
Five bold claims, I suppose, that indeed the path of mindfulness that leads to these five benefits.

13
00:03:32,000 --> 00:03:42,000
And so the first one is the purity, purity of mind.

14
00:03:42,000 --> 00:04:01,000
The benefit of seeing the negative aspects of experience is that it cleanses any sort of desire or attachment from the mind.

15
00:04:01,000 --> 00:04:23,000
The mind is no longer left wanting, left hungry, left discontent in its search for satisfaction and pleasure.

16
00:04:23,000 --> 00:04:34,000
Through wisdom, through seeing things as they are, the mind slowly begins to let go.

17
00:04:34,000 --> 00:04:51,000
We can sit, grasp it, we can sit, drive to obtain and attain and become.

18
00:04:51,000 --> 00:05:15,000
Specifically, the mind becomes free from greed, from anger and from delusion, the three roots of all evil, right?

19
00:05:15,000 --> 00:05:28,000
And so the key here is that we don't artificially construct the freedom from defilement.

20
00:05:28,000 --> 00:05:35,000
We're not trying to control our mind to not become angry or greedy or deluded.

21
00:05:35,000 --> 00:05:52,000
The really important and I would say, recently profound point of this teaching is that it's not by controlling the mind or by fixing the problem.

22
00:05:52,000 --> 00:05:58,000
That something so simple and so benign has mindfulness.

23
00:05:58,000 --> 00:06:06,000
It turns out to be the key that unlocks the door to freedom from suffering.

24
00:06:06,000 --> 00:06:15,000
It gets us out of the burning building of defilements.

25
00:06:15,000 --> 00:06:19,000
If you want something, you don't have to tell yourself no.

26
00:06:19,000 --> 00:06:27,000
You just have to study and learn about your desires and the object of your desire until you realize the truth,

27
00:06:27,000 --> 00:06:36,000
which is that that thing is not worth clinging to.

28
00:06:36,000 --> 00:06:41,000
So the freedom is much more like boredom.

29
00:06:41,000 --> 00:06:47,000
Freedom from suffering is like boredom in the sense that you get fed up.

30
00:06:47,000 --> 00:06:52,000
You don't exactly get bored, but you get bored of suffering.

31
00:06:52,000 --> 00:06:59,000
So for again and again and suffer so much that you finally say, phew, enough of that.

32
00:06:59,000 --> 00:07:06,000
That's freedom. Freedom isn't from running away or avoiding or controlling or contriving.

33
00:07:06,000 --> 00:07:14,000
It doesn't come from tricks or artifices.

34
00:07:14,000 --> 00:07:23,000
At the nibindatidu key, he's the mongo, he's the deer.

35
00:07:23,000 --> 00:07:29,000
And then one becomes... I want to say sick, but not sick, but becomes disenchanted with suffering.

36
00:07:29,000 --> 00:07:33,000
I think he becomes sick of it.

37
00:07:33,000 --> 00:07:45,000
This is the path of purification.

38
00:07:45,000 --> 00:07:53,000
It's the first benefit, and that in and of itself is the major benefit of meditation, purity.

39
00:07:53,000 --> 00:07:56,000
So I talk again and again about goodness, right?

40
00:07:56,000 --> 00:08:13,000
Trying to instill in all of us the sense of importance or the focus on the importance of goodness over happiness,

41
00:08:13,000 --> 00:08:29,000
the importance of wholesomeness, the importance of the instigation of positive states.

42
00:08:29,000 --> 00:08:34,000
Suppose they're just waiting for them to come to you, waiting for happiness to drop in your lap.

43
00:08:34,000 --> 00:08:52,000
Like some miracle from God, to actually cultivate the causes of happiness.

44
00:08:52,000 --> 00:09:05,000
And so I've been looking recently at these various...

45
00:09:05,000 --> 00:09:15,000
I would say counter-culture, I suppose, on the internet dedicated to goodness.

46
00:09:15,000 --> 00:09:28,000
It's counter-culture because the internet, I think, exploded mainly on negative aspects of humanity, mostly greed, instant gratification.

47
00:09:28,000 --> 00:09:31,000
But there are some aspects, some parts of the internet.

48
00:09:31,000 --> 00:09:37,000
I found one today, I've subscribed to what is it, uplifting news.

49
00:09:37,000 --> 00:09:47,000
So news that is actually about the good things that people do.

50
00:09:47,000 --> 00:09:51,000
Something called wholesome memes.

51
00:09:51,000 --> 00:09:54,000
And so it's a new take on memes that's just all about good things.

52
00:09:54,000 --> 00:10:00,000
And it's really quite wonderful and endearing.

53
00:10:00,000 --> 00:10:08,000
So I posted one today, a fun drinking game, a fun drinking game.

54
00:10:08,000 --> 00:10:17,000
Take a shot of water every two hours, every hour or so to keep yourself healthy and hydrated.

55
00:10:17,000 --> 00:10:19,000
I haven't even noticed that's not actually wholesomeness.

56
00:10:19,000 --> 00:10:29,000
My only point is transforming our minds from a focus on gratification.

57
00:10:29,000 --> 00:10:44,000
And running away from our problems to keeping moral precepts, like not drinking alcohol, etc., etc.

58
00:10:44,000 --> 00:10:53,000
It's actually doing something about it, practicing meditation.

59
00:10:53,000 --> 00:10:59,000
And so that's the major benefit. The major benefit is not happiness. It's purity.

60
00:10:59,000 --> 00:11:03,000
We don't worry about the happiness part.

61
00:11:03,000 --> 00:11:15,000
We want true purity, not a purity that is controlled or artificial.

62
00:11:15,000 --> 00:11:23,000
True purity is the source of true happiness.

63
00:11:23,000 --> 00:11:26,000
But there are lots more benefits, and they come from purity.

64
00:11:26,000 --> 00:11:32,000
So the second one is overcoming moral limitation and despair.

65
00:11:32,000 --> 00:11:36,000
And included in here would be all sorts of mental illness.

66
00:11:36,000 --> 00:11:56,000
Meditation cures the mind of mental illnesses which end up being no more than simple obsessions over desires and versions and delusions.

67
00:11:56,000 --> 00:12:06,000
And when you see with wisdom that the things worth clinging to problems like depression, anxiety, worry.

68
00:12:06,000 --> 00:12:14,000
When you stop seeing things as me or mine, myself, all of these states are no longer relevant.

69
00:12:14,000 --> 00:12:19,000
They're no longer useful, no longer seen as useful or seen as right.

70
00:12:19,000 --> 00:12:23,000
Once you realize that something doesn't belong to you, why would you worry about it?

71
00:12:23,000 --> 00:12:30,000
You have this man who drives the ox. He wakes up early in the morning and drives his ox up to the past year.

72
00:12:30,000 --> 00:12:33,000
Then it gets light and he realizes it isn't his ox.

73
00:12:33,000 --> 00:12:43,000
And so he drives it back and forgets about it.

74
00:12:43,000 --> 00:12:54,000
And like the woman who picks up the wrong baby and starts to nurse it until she realizes it isn't hers and puts it back down and forgets about it.

75
00:12:54,000 --> 00:13:04,000
And other stories like this, that symbolies like this in the VCD manga, describing this and this release.

76
00:13:04,000 --> 00:13:21,000
Even schizophrenia, I talked several times about this, how it seems to me that schizophrenia though accompanied with incredible paranoia, I think in most cases, crippling paranoia.

77
00:13:21,000 --> 00:13:31,000
I think the paranoia aspect could be ameliorated, if not cured, to the extent that the person is able to deal with their hallucinations mindfully.

78
00:13:31,000 --> 00:13:39,000
Certainly not easy, but potential is there.

79
00:13:39,000 --> 00:13:46,000
The third benefit is suffering, overcoming suffering, mental suffering, physical suffering.

80
00:13:46,000 --> 00:13:50,000
Once your mind is pure, you don't suffer.

81
00:13:50,000 --> 00:13:54,000
Now physical suffering, of course, you can't remove it.

82
00:13:54,000 --> 00:13:59,000
Just because your mind is pure doesn't mean you're not going to feel physical pain,

83
00:13:59,000 --> 00:14:08,000
but there's no mental pain, there's no disliking of the pain.

84
00:14:08,000 --> 00:14:12,000
And so physical pain becomes just another sensation.

85
00:14:12,000 --> 00:14:22,000
Once you're able to see it clearly and remove that kernel of dislike that makes it painful in the first place.

86
00:14:22,000 --> 00:14:32,000
And then it's only just a sensation, like any other sensation, happiness is a sensation, pleasure is a sensation, pain is also a sensation.

87
00:14:32,000 --> 00:14:40,000
There are happiness is always in the mind, but pleasure and pain or Socrates said something like this.

88
00:14:40,000 --> 00:14:46,000
If you read the apology of Socrates, he says something about pleasure and pain, that's interesting.

89
00:14:46,000 --> 00:14:53,000
I mean, probably not in the coming from the same place, but it's so much philosophical about.

90
00:14:53,000 --> 00:15:02,000
I think about there being very little difference.

91
00:15:02,000 --> 00:15:09,000
The fourth benefit is that you get on the right path through the practice of insight meditation.

92
00:15:09,000 --> 00:15:16,000
You enter into the right path.

93
00:15:16,000 --> 00:15:22,000
The key here is that the right path turns out not to be what you do with your life.

94
00:15:22,000 --> 00:15:29,000
Who you are, what you are, but how you live your life.

95
00:15:29,000 --> 00:15:32,000
You don't have to fret or worry about what you do.

96
00:15:32,000 --> 00:15:39,000
If you wind up living on a park bench, live on a park bench mindfully.

97
00:15:39,000 --> 00:15:45,000
If you're living in a mansion, do it mindfully.

98
00:15:45,000 --> 00:15:55,000
If you're a doctor or a lawyer, be a doctor, be a mindful doctor or a mindful lawyer to the extent possible.

99
00:15:55,000 --> 00:16:02,000
I think it's possible, mindful lawyers and mindful doctors, mindful politicians.

100
00:16:02,000 --> 00:16:10,000
Wouldn't it be great if our politicians all just sat down and meditated?

101
00:16:10,000 --> 00:16:16,000
My mom was sending me trumps, pictures of Donald Trump's head on a Buddhist monk's body.

102
00:16:16,000 --> 00:16:18,000
Not sure how I feel about that.

103
00:16:18,000 --> 00:16:26,000
Pretty sure how I should feel about that, but not sure.

104
00:16:26,000 --> 00:16:32,000
Wouldn't it be something?

105
00:16:32,000 --> 00:16:39,000
The right way, the right way, it sounds like such an arrogant thing to say.

106
00:16:39,000 --> 00:16:45,000
If anyone understands, if you understand mindfulness, it doesn't seem unreasonable at all.

107
00:16:45,000 --> 00:17:06,000
Anyone without mindfulness cannot ever be said to be on the right path, no matter what they do, no matter how kind they seem to be without mindfulness they can't perform real kindness.

108
00:17:06,000 --> 00:17:15,000
And so what happens is through the meditation, you develop habits, you break down bad habits and you cultivate good habits.

109
00:17:15,000 --> 00:17:22,000
And your mind changes, your path changes, not just in this life.

110
00:17:22,000 --> 00:17:28,000
So that's the thing about habits, especially deeply.

111
00:17:28,000 --> 00:17:35,000
Spiritual habits are deeply meaningful habits like meditation habits.

112
00:17:35,000 --> 00:17:44,000
And one's based on wisdom or ignorance is that they affect the change of foundation of who you are.

113
00:17:44,000 --> 00:17:52,000
In this life and in the next, assuming locate Paramitra.

114
00:17:52,000 --> 00:17:56,000
In this life and the next and all lives to come.

115
00:17:56,000 --> 00:18:01,000
That's the greatness of meditation.

116
00:18:01,000 --> 00:18:06,000
Karma isn't like a bank where you do all these good deeds and you can cash in later on.

117
00:18:06,000 --> 00:18:09,000
Karma changes you.

118
00:18:09,000 --> 00:18:13,000
True karma isn't the act, it's the mind state.

119
00:18:13,000 --> 00:18:23,000
And so meditation is like karma to the ends degree because every moment you're creating wholesome karma.

120
00:18:23,000 --> 00:18:29,000
You're changing who you are on an accelerated level, accelerated pace.

121
00:18:29,000 --> 00:18:34,000
Instead of every time you do a good deed, there's some wholesomeness involved.

122
00:18:34,000 --> 00:18:39,000
Every moment that you say rising or you say falling or you say sitting or you say pain,

123
00:18:39,000 --> 00:18:44,000
every time you recognize something as it is, you're changing who you are.

124
00:18:44,000 --> 00:18:47,000
You're cultivating good karma.

125
00:18:47,000 --> 00:18:55,000
This is how encouraged we should be by the practice.

126
00:18:55,000 --> 00:19:04,000
With the understanding that we're creating good karma, every moment that we're mindful.

127
00:19:04,000 --> 00:19:06,000
The Buddha said that.

128
00:19:06,000 --> 00:19:10,000
The Buddha said, Chitna hung bekewake among wadami.

129
00:19:10,000 --> 00:19:14,000
It is the mind state amongst that I call karma.

130
00:19:14,000 --> 00:19:20,000
So I quote the Buddha, don't quote me.

131
00:19:20,000 --> 00:19:26,000
And the final benefit, of course, is the realization of freedom.

132
00:19:26,000 --> 00:19:30,000
No, everything up until this one sort of talks about freedom.

133
00:19:30,000 --> 00:19:36,000
But nibana, nirana is true freedom and it's on a different level than the rest of them.

134
00:19:36,000 --> 00:19:38,000
It comes out of the rest.

135
00:19:38,000 --> 00:19:46,000
Once you begin to follow the right path and you've freed yourself from mental illness of all kinds,

136
00:19:46,000 --> 00:19:51,000
major, minor, the mind lets go.

137
00:19:51,000 --> 00:19:57,000
The mind begins to see clearly and more clearly until finally there's an epiphany.

138
00:19:57,000 --> 00:19:59,000
It really isn't epiphany.

139
00:19:59,000 --> 00:20:02,000
Maybe not in the sense that we think about it.

140
00:20:02,000 --> 00:20:08,000
There's no real intellectualizing, but it's just a moment of clarity.

141
00:20:08,000 --> 00:20:17,000
Where the mind says to itself, not so many words, but it just clicks.

142
00:20:17,000 --> 00:20:20,000
Sabhe, dhamma, nalanga, bhini, vaisaya.

143
00:20:20,000 --> 00:20:24,000
All dhammas are not worth clinging to and it lets go.

144
00:20:24,000 --> 00:20:27,000
Poof.

145
00:20:27,000 --> 00:20:31,000
It's like turning off the lights.

146
00:20:31,000 --> 00:20:36,000
It's like unplugging your computer, pressing the power button and it all goes quiet.

147
00:20:36,000 --> 00:20:43,000
It all goes cold, cool.

148
00:20:43,000 --> 00:20:48,000
The heat is dissipated.

149
00:20:48,000 --> 00:20:57,000
Noise and chaos and suffering is all.

150
00:20:57,000 --> 00:21:04,000
It's all destroyed or done away with.

151
00:21:04,000 --> 00:21:06,000
It's the main goal of meditation.

152
00:21:06,000 --> 00:21:07,000
There is this goal.

153
00:21:07,000 --> 00:21:10,000
It's not something to be feared, not something to worry about.

154
00:21:10,000 --> 00:21:14,000
It's a state and you can think of it like a meal.

155
00:21:14,000 --> 00:21:16,000
All you have to do is taste nibana.

156
00:21:16,000 --> 00:21:19,000
If you don't like the taste, you can go back and say,

157
00:21:19,000 --> 00:21:25,000
this enlightenment thing is not for me.

158
00:21:25,000 --> 00:21:33,000
But it's kind of workally impossible to do that because anyone who tastes nibana is

159
00:21:33,000 --> 00:21:51,000
it's so real that it's impossible for you to see other than that nibana nibana nibana is the highest happiness.

160
00:21:51,000 --> 00:21:53,000
That's what they say.

161
00:21:53,000 --> 00:22:00,000
The person who sees nibana is impossible because it's changed fundamentally the nature of their mind.

162
00:22:00,000 --> 00:22:07,000
Through wisdom, not through any mystical, magical power, but through some higher form of wisdom

163
00:22:07,000 --> 00:22:19,000
that's involved, just simply seeing true peace and the realization that all this stuff we called peace or happiness before that was not true peace, not true happiness.

164
00:22:19,000 --> 00:22:25,000
It's a categorical or irrevocable wisdom.

165
00:22:25,000 --> 00:22:37,000
It comes from attaining nibana or realizing nibana for yourself.

166
00:22:37,000 --> 00:22:38,000
So there you go.

167
00:22:38,000 --> 00:22:48,000
These benefits should be familiar to many of you already, but I like talking about them for my own benefit.

168
00:22:48,000 --> 00:22:51,000
It's good for us to remember good things.

169
00:22:51,000 --> 00:22:57,000
It gives us an opportunity to meditate here together and meditate in a sort of special sense of,

170
00:22:57,000 --> 00:23:02,000
maybe a Western sense of the word, to meditate and mold these things over.

171
00:23:02,000 --> 00:23:13,000
To step back from our practice and reflect on what we might be doing wrong and how to adjust our practice and to reaffirm what we're doing right

172
00:23:13,000 --> 00:23:19,000
and to encourage us in it.

173
00:23:19,000 --> 00:23:22,000
So there you go. That's the dhamma for this evening.

174
00:23:22,000 --> 00:23:29,000
Thank you all for coming up.

175
00:23:29,000 --> 00:23:42,000
And I'm going to, I'm going to ask to leave early tonight day, there are some things that I have to do.

176
00:23:42,000 --> 00:23:51,000
So no questions tonight if you have any questions, come on back hopefully tomorrow.

177
00:23:51,000 --> 00:23:57,000
Although this next week or so it might be a little bit if you will have to see how things go.

178
00:23:57,000 --> 00:24:04,000
Anyway, lots of see you all in the future.

179
00:24:04,000 --> 00:24:14,000
Good night.

