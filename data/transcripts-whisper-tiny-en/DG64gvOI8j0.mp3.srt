1
00:00:00,000 --> 00:00:21,860
Okay, good evening, broadcasting live, October 2, 2015, today we have a quote from the

2
00:00:21,860 --> 00:00:35,360
Madi Minikaya. We're breaking in half, Robin, we start reading and we'll stop you and

3
00:00:35,360 --> 00:00:43,580
not become sure. When you are living together in harmony and without contention, a certain person

4
00:00:43,580 --> 00:00:48,420
might do something wrong or transgress. Concerning this, you should not hasten to

5
00:00:48,420 --> 00:00:55,660
re-proof. The person should be examined, incorrecting him you might think, I won't get annoyed

6
00:00:55,660 --> 00:01:03,100
nor will he, for he is without anger and irritation. He is quick to see and easy to convince.

7
00:01:03,100 --> 00:01:08,540
I have the power to raise this person from the unskillful and establish him in the skillful.

8
00:01:08,540 --> 00:01:13,820
If you think this, then it is right to speak. If you think, I won't get annoyed, but he

9
00:01:13,820 --> 00:01:43,660
won't, for he is not prone to irritability or anger. He is quick to see, but difficult.

10
00:01:43,660 --> 00:01:50,540
But I have the power to raise this person from the unskillful and establish him in the

11
00:01:50,540 --> 00:01:56,620
skillful. My annoyance is but a small thing. The great thing is that I will be able to establish

12
00:01:56,620 --> 00:02:03,100
him in the skillful. If you think this, then it is right to speak. If you think, I will get

13
00:02:03,100 --> 00:02:10,020
annoyed and so will he, for he is irritable, angry, slow to see and hard to convince. But still,

14
00:02:10,020 --> 00:02:14,780
I have the power to raise this person from the unskillful and establish him in the skillful.

15
00:02:14,780 --> 00:02:20,220
My annoyance is but a small thing. The great thing is that I will be able to establish him in the

16
00:02:20,220 --> 00:02:26,260
skillful. If you think this, then it is right to speak. However, if you think, I will get annoyed

17
00:02:26,260 --> 00:02:32,660
and so will he, for he is irritable, angry, slow to see and difficult to convince. And I don't think

18
00:02:32,660 --> 00:02:39,260
I have the power to raise him from the unskillful and establish him in the skillful. Then in this case,

19
00:02:39,260 --> 00:02:48,540
I have equanimity towards that person. So we have an example here of laying out in quite some

20
00:02:48,540 --> 00:02:57,500
detail. It's an example of dealing with disputes, dealing with difficulties, dealing with conflict.

21
00:02:59,260 --> 00:03:04,060
These sort of passages are all over the Vinaya and in the Sutras as well. This is from the

22
00:03:04,060 --> 00:03:13,180
Maji Minikaya. The Sutras. And together, they put together, they have a particular real framework

23
00:03:13,180 --> 00:03:19,740
for conflict resolution, which not really being, I mean, conflict resolution in the traditional

24
00:03:19,740 --> 00:03:27,420
senses and exactly the core of Buddhism. But it does tie in nicely with the idea of resolving

25
00:03:27,420 --> 00:03:33,660
suffering, overcoming suffering and overcoming the cause of suffering, because conflict is very much

26
00:03:33,660 --> 00:03:40,140
part of that cause. That's one way of describing the cause. Like when we studied peace studies,

27
00:03:41,180 --> 00:03:46,940
it was really interesting when I was at university last time, took a class on peace studies

28
00:03:49,180 --> 00:03:55,180
and learned about conflict resolution and surprised to see how much of Buddhism

29
00:03:55,980 --> 00:04:00,620
is found in it and how compatible it is with the Buddha's teaching. They talk about

30
00:04:00,620 --> 00:04:12,060
the three kinds of violence, physical violence, structural violence, and cultural violence.

31
00:04:12,060 --> 00:04:18,620
And these tie in quite nicely with the three trainings. So physical violence is actually

32
00:04:18,620 --> 00:04:27,020
hurting each other with sticks and stones. And that's overcome by the first training in Buddhism,

33
00:04:27,020 --> 00:04:34,620
morality. Structural violence is the structures that oppress or that

34
00:04:39,180 --> 00:04:49,580
cause harm just by virtue of the structure, the impetus, the inertia, or the impetus, the inertia

35
00:04:50,620 --> 00:04:56,060
of having the structure. And this is a description of wrong concentration on a personal level. This

36
00:04:56,060 --> 00:05:01,900
means the habits that we've formed when we focused our mind in the wrong way. So it's

37
00:05:01,900 --> 00:05:08,220
contracted by focusing your mind in the right way, having strong focus of mind so that you build

38
00:05:08,220 --> 00:05:14,780
the right mental structure. And cultural violence as views, beliefs, opinions,

39
00:05:18,540 --> 00:05:24,620
and this is contracted by wisdom. So those things like that, and they talk about personal conflict,

40
00:05:24,620 --> 00:05:37,420
interconflict, as being a mirror or a smaller version of global conflict. So here we have

41
00:05:37,420 --> 00:05:44,220
conflict in a monastery or in a community that's quite useful in and of itself. But it also deals

42
00:05:44,220 --> 00:05:50,860
with the same sort of issues that we have to deal with in our meditation in terms of combating

43
00:05:50,860 --> 00:06:00,380
interconflict, dealing with our annoyances, dealing with the states of mind. Now if you have a state

44
00:06:00,380 --> 00:06:09,020
of mind that is problematic and you feel that you can bear with it, learn from it, then you should

45
00:06:09,020 --> 00:06:15,020
just allow it to be and watch it and be an observant. Not exactly allow it to be, but you shouldn't

46
00:06:15,020 --> 00:06:19,420
crush it, you shouldn't run away from it, you shouldn't get up and leave because you're having

47
00:06:19,420 --> 00:06:27,740
this mind state. You shouldn't stick with it, but if you can, then you have to find a way to

48
00:06:30,380 --> 00:06:38,700
back away from it in the meantime. It's the same sort of ideas. Here we have this detailed idea of

49
00:06:40,460 --> 00:06:43,100
whether you'll get angry or that they will get angry, whether they are

50
00:06:43,100 --> 00:06:49,340
quick to see or slow to see, easy to convince, difficult to convince.

51
00:06:51,340 --> 00:07:04,060
It shows the idea that there's no one answer to the issue of conflict, conflict is something

52
00:07:04,780 --> 00:07:11,580
that you have to deal with based on the situation, the individual case-by-case conflict.

53
00:07:11,580 --> 00:07:16,220
You have to understand the conflict, you have to understand all the variables of the conflict.

54
00:07:18,540 --> 00:07:23,740
And so you can ask yourself, you know, how easy is it to know all of these variables?

55
00:07:25,420 --> 00:07:30,300
That's what you need. You need to be familiar and you need to be mindful, you need to be alert

56
00:07:31,020 --> 00:07:35,100
so that you can gain, you need to be objective, so that you're able to see,

57
00:07:35,100 --> 00:07:45,260
so that you are clear of what's going on in the conflict. How is the other person going to react?

58
00:07:45,260 --> 00:07:51,500
If I tell them, hey, don't do that. You have to ask yourself, what is our relationship?

59
00:07:52,220 --> 00:07:59,180
Are they my senior, my superior? Are they my junior or someone who is likely to look up to me or so on?

60
00:07:59,180 --> 00:08:06,460
And this is why mindfulness is so important, rather than rules or guidelines.

61
00:08:07,180 --> 00:08:11,900
Mindfulness allows you to see the variables, allows you to see the situation for what it is,

62
00:08:12,700 --> 00:08:19,020
allows you to stay present. And if you begin to act improperly,

63
00:08:20,060 --> 00:08:24,380
you'll see it quickly, because you'll see the results, you can feel the results

64
00:08:24,380 --> 00:08:33,260
because you're present. So generally, the best way to deal with conflict and deal with problems

65
00:08:33,260 --> 00:08:39,420
in our lives is to start with mindfulness, to have a base of mindfulness. It's the one thing that

66
00:08:39,420 --> 00:08:47,900
you need at all times in all situations. Anyway, Robin, did you have anything to say? You said you

67
00:08:47,900 --> 00:08:57,900
like this quote? I did like that quote, it just, you know, just reminds me of our speech.

68
00:09:07,740 --> 00:09:11,100
Okay, do we have any questions? We do.

69
00:09:11,100 --> 00:09:18,940
When meditating, should I just posture slightly when experiencing pain, pain in the back, for example?

70
00:09:23,500 --> 00:09:27,900
You should try to be mindful of it first, if that doesn't work, then you can adjust.

71
00:09:33,500 --> 00:09:38,140
One day during the day, there are some unoccupied moments. Is it okay to slip in a few

72
00:09:38,140 --> 00:09:44,700
five minute mini meditations each day, say sitting in a chair? This seems to aid mindfulness,

73
00:09:44,700 --> 00:09:51,260
or could it be harmful? No, it wouldn't be harmful. Absolutely not. That's a great idea,

74
00:09:51,260 --> 00:09:56,220
especially in between work. I was just telling someone today that take breaks from work. When you

75
00:09:56,220 --> 00:10:01,100
do study, when you study for an exam or something, take five minutes out to do meditation.

76
00:10:01,100 --> 00:10:12,060
Absolutely, it'll make your work easier, it'll refocus you. No meditation won't hurt. The only

77
00:10:12,060 --> 00:10:17,500
only way it can become a problem, I think I was telling someone this morning is if you push yourself

78
00:10:17,500 --> 00:10:21,580
too hard, if you push yourself to the point that you're really forcing yourself to meditate.

79
00:10:23,020 --> 00:10:26,380
Or if you're too young, how about it in the sense that you're not cautious?

80
00:10:26,380 --> 00:10:30,860
Not in terms of amount of meditation, but cautious in terms of how you approach

81
00:10:31,660 --> 00:10:34,540
experience, like are you still being objective or are you just

82
00:10:36,220 --> 00:10:42,860
following anything? Really two-gun hold, but meditation to the point where you lose your

83
00:10:42,860 --> 00:10:48,860
objectivity. That's where it can become dangerous. As long as you're objective and you're careful,

84
00:10:48,860 --> 00:10:59,020
you can meditate hours and hours a day. What does pressing the prayer hands do?

85
00:11:02,220 --> 00:11:08,860
Nothing. It does give us, actually for the chat, it's more useful because it gives us a sense of

86
00:11:09,500 --> 00:11:17,020
people like this question or that question. And it's a way of expressing appreciation.

87
00:11:17,020 --> 00:11:23,020
It's a wholesome act when you click it. It can be considered, in some cases, a wholesome act,

88
00:11:23,020 --> 00:11:29,180
because I appreciate what you just said. It's also, unless they said, hey, that monk's a jerk

89
00:11:29,180 --> 00:11:34,300
and then you hit go hit. Yeah, I agree. And you're sharing in the unwholesome.

90
00:11:35,100 --> 00:11:37,820
And we need to thump the thumbs down for that comment.

91
00:11:37,820 --> 00:11:47,820
Until you'll be doing a class in Florida soon. I haven't been, um, I shouldn't say that.

92
00:11:47,820 --> 00:11:54,620
I'm probably going to see my mother in December, but I haven't contacted the people at the

93
00:11:54,620 --> 00:12:00,460
university there. So if they're watching this, let's suppose any of them are on here,

94
00:12:00,460 --> 00:12:07,820
but I should get in touch with them. If they get in touch with them, I'll probably see if they're

95
00:12:07,820 --> 00:12:11,180
going, if they'd organize something. I guess it's getting close now, gee, first.

96
00:12:12,220 --> 00:12:22,460
Yeah. Two months. Two months, that's it. Yes. So busy with things here. Next week, next Friday,

97
00:12:22,460 --> 00:12:27,180
we have the peace walk. I'm giving a talk at the university. We're having our first lecture,

98
00:12:27,180 --> 00:12:34,220
and I'm giving it. Did I mention that already? No. The first talk is on, I've, I've decided

99
00:12:34,860 --> 00:12:37,820
the talk is going to be on enlightenment in the digital age.

100
00:12:39,260 --> 00:12:45,180
Then I'm going to talk about all you guys and this hangout thing and our appointment system.

101
00:12:45,180 --> 00:12:49,420
That's just part of it, but I'll be talking about how I got into using the internet for

102
00:12:49,420 --> 00:13:03,660
or spreading the dhamma or teaching for spiritual upliftment. Another question regarding the,

103
00:13:03,660 --> 00:13:09,500
the praying hands. Can you upload as long as you are registered or must you log meditation?

104
00:13:09,500 --> 00:13:19,500
In which to be a lot of things to do with logging meditation.

105
00:13:19,500 --> 00:13:23,020
Dante, I realize you're trying to encourage meditation, but don't you think logging

106
00:13:23,020 --> 00:13:27,740
one's meditation might be detrimental to some because it might give rise to allocating

107
00:13:27,740 --> 00:13:31,820
meditation for a period of the day, rather than continuous mindfulness.

108
00:13:32,540 --> 00:13:39,180
Guess what? Non-meditators only get one question per session. Average joy is a non-meditator.

109
00:13:39,180 --> 00:13:51,500
Everyone. We can all go boo. We got one question. That would be on wholesome. We won't

110
00:13:51,500 --> 00:13:55,180
boo you, but it's an excuse for me not to answer your question.

111
00:14:00,860 --> 00:14:03,820
I'm doing an all day retreat at home tomorrow, any tips?

112
00:14:03,820 --> 00:14:14,540
No, good for you. Tomorrow we have our first day of mindfulness. If anyone shows up,

113
00:14:15,820 --> 00:14:19,500
nine to four, so you can consider that you're joining our day of mindfulness.

114
00:14:22,140 --> 00:14:22,700
Very nice.

115
00:14:22,700 --> 00:14:30,620
No, don't be afraid to do meditation. Don't be afraid to do it and try to stay

116
00:14:37,340 --> 00:14:42,620
try to stay with the practice even if it gets difficult. Basically, no matter what happens,

117
00:14:42,620 --> 00:14:46,540
because your mind will trick you into thinking, oh, I have to stop or I have to do this or

118
00:14:46,540 --> 00:14:51,260
something's getting in my way. Try to be just very mindful of it. Always remember, be objective

119
00:14:51,260 --> 00:14:53,260
in mind.

120
00:14:59,260 --> 00:15:01,500
Will you record the talk? You do let the university?

121
00:15:02,620 --> 00:15:07,580
Probably yes.

122
00:15:15,580 --> 00:15:21,020
So, average joy realizes that you got them there, so hopefully he'll be meditating next time.

123
00:15:21,020 --> 00:15:27,260
And get to ask two or three questions. Hello, Pompey. I am visiting Sri Lanka in December.

124
00:15:27,260 --> 00:15:30,620
Do you have any suggestions on enhancing my practice while I'm there?

125
00:15:33,980 --> 00:15:36,700
I'd recommend going to me, Saturday night.

126
00:15:39,180 --> 00:15:39,820
Good place.

127
00:15:42,700 --> 00:15:49,580
Go talk to them. What is that? Is that a city or a monastery?

128
00:15:49,580 --> 00:15:55,020
Kneeserana is the meditation center. Kneeserana.lk, I think, is their website.

129
00:15:55,740 --> 00:15:55,900
Okay.

130
00:15:59,740 --> 00:16:05,580
Bante damejima is the teacher. Nice guy. Very, very smart.

131
00:16:05,580 --> 00:16:19,580
Singlish isn't perfect, but good enough. Definitely good enough. It's actually quite good.

132
00:16:20,620 --> 00:16:22,780
So, what do you have planned for the day of mindfulness?

133
00:16:22,780 --> 00:16:38,700
I'm just teaching in the morning and letting people practice all day.

134
00:16:52,780 --> 00:17:12,700
Now, the list of logged-in users is sorted.

135
00:17:15,500 --> 00:17:16,860
What's it sorted? Oh, is it?

136
00:17:16,860 --> 00:17:25,660
Yeah, if you're a freshman. I think it worked. I hope it worked.

137
00:17:27,180 --> 00:17:42,140
Oh, sorted alphabetically sort of sorted alphabetically by people whose names are uppercase

138
00:17:42,140 --> 00:17:48,700
followed by people whose names start with lowercase, and also sorted by meditators and non-meditators.

139
00:17:48,700 --> 00:17:54,460
That's just a default sort. It's because there's HTML in there as well. That's why it recognizes

140
00:17:55,260 --> 00:18:00,220
the non-meditators first, just because it's parsing the HTML as well. That's nice.

141
00:18:01,020 --> 00:18:05,100
It is nice. That's fancy, too. No, everyone's not moving around and we can tell where people are.

142
00:18:05,100 --> 00:18:13,020
I like it. They're not moving around. It brings my eye to it, and it's distracting. I like that.

143
00:18:18,140 --> 00:18:23,500
So, it looks like it's sorting alphabetically and if your name is in uppercase,

144
00:18:23,500 --> 00:18:27,740
and if you're maybe on mobile. It's for non-dose in its own category there.

145
00:18:27,740 --> 00:18:38,620
Yeah, because of the HTML, because of the image.

146
00:18:38,620 --> 00:19:02,380
Hmm. Well, two or a few questions. No, it's just nice.

147
00:19:02,380 --> 00:19:08,620
You don't need an average show, as he doesn't need my approval. Absolutely not.

148
00:19:08,620 --> 00:19:14,060
You don't need anyone's approval. Though it helps, because how do you know if you're on the right

149
00:19:14,060 --> 00:19:22,300
path, if you don't listen to a approval of people who you consider to be wise. So, if you don't

150
00:19:22,300 --> 00:19:27,340
need my approval, it could be because you don't consider me to be wise, which is cool.

151
00:19:27,340 --> 00:19:33,580
But sometimes it's good to find some people who you think are wise and then be sure that you have

152
00:19:33,580 --> 00:19:39,420
their approval, because if you don't, then sometimes wrong. If you consider someone to be wise,

153
00:19:39,420 --> 00:19:46,540
but you don't have their approval, then it's a good way of knowing if you're on the right track or not.

154
00:19:48,060 --> 00:19:55,020
And then sometimes you always seek. In some cases, we seek approval all the time. Constantly,

155
00:19:55,020 --> 00:20:00,220
we need to be reaffirmed that we have value. That's a sign of low self-esteem.

156
00:20:01,900 --> 00:20:08,060
But if you don't seek approval for anyone, it's often a sign of ego that you have to be careful of.

157
00:20:10,700 --> 00:20:14,380
So, too much approval or not enough approval. I'm going to watch out for you this side.

158
00:20:16,300 --> 00:20:20,220
One thing, if I can't understand exactly what emotion I'm feeling during meditation,

159
00:20:20,220 --> 00:20:28,220
do I simply not feeling to avoid mislabeling and confusing myself of reality?

160
00:20:28,220 --> 00:20:30,300
Yeah, feeling, feeling is fine.

161
00:20:32,780 --> 00:20:37,020
You can often, if it's a negative one, you can just say disliking because it's generally

162
00:20:37,020 --> 00:20:44,540
some sort of disliking. If it's a positive one, it's liking or it's confused or so on.

163
00:20:44,540 --> 00:20:49,340
In fact, when you're confused about what something is, then you can just acknowledge confused,

164
00:20:49,340 --> 00:20:59,340
confused, and that works really well. It helps to clear things up as well.

165
00:21:10,700 --> 00:21:15,100
There was one video, the video, two nights ago, three nights ago called Annihilationism.

166
00:21:15,100 --> 00:21:21,900
Somehow, it must have gotten posted somewhere because 3,000 people have watched that video.

167
00:21:23,740 --> 00:21:27,900
Hmm. It's extraordinary compared to the others.

168
00:21:29,660 --> 00:21:31,500
You don't tag these with anything, do you?

169
00:21:32,220 --> 00:21:37,980
No. Somehow, people like the word, maybe someone. Sometimes what happens is someone posts it

170
00:21:37,980 --> 00:21:42,380
on a website and it gets taken up.

171
00:21:42,380 --> 00:21:46,620
Maybe they thought it was about something much different than what it was.

172
00:21:46,620 --> 00:21:49,580
Maybe, maybe Annihilationism is amazing for some people.

173
00:21:52,220 --> 00:21:56,300
Why would the Buddha strive to become a Buddha instead of just an Arahan?

174
00:21:56,300 --> 00:21:58,460
Arahan? Same result, no?

175
00:21:59,900 --> 00:22:05,180
Yeah. Well, you know, the thing is, we're not clones. We're not robots.

176
00:22:05,180 --> 00:22:12,380
Everyone has their own paths. Even all Arahan's are different.

177
00:22:12,380 --> 00:22:19,660
So, the Bodhisattva, the time when he became a Bodhisattva, that was his inclination.

178
00:22:19,660 --> 00:22:25,100
That was his path. Some have the inclination to become a particular Buddha.

179
00:22:25,100 --> 00:22:28,940
Some have the inclination to become an Arahan. It's not really a judgment.

180
00:22:28,940 --> 00:22:35,420
Like, oh, bad, you didn't become a Buddha or why the heck did you bother becoming a Buddha?

181
00:22:36,700 --> 00:22:42,860
We take people, people go in their own path and that's an important rule to remember in Buddhism.

182
00:22:42,860 --> 00:22:48,300
Said, we're not, it's not one-size-fits-all. Or maybe, it's one-size-fits-all?

183
00:22:48,300 --> 00:22:52,060
Well, maybe not. Everyone has their own needs.

184
00:22:53,340 --> 00:22:57,500
So, we take people through this, we pass in a meditation course.

185
00:22:57,500 --> 00:23:02,700
It's kind of one-size-fits-all, but we understand that people are different.

186
00:23:02,700 --> 00:23:06,940
And they'll get different results out of it. Some people won't succeed in the course at all.

187
00:23:07,660 --> 00:23:11,980
Just because their path is much longer, maybe because they've determined to become a Buddha.

188
00:23:13,100 --> 00:23:18,220
So, they won't be able to attain Sotupana or anything.

189
00:23:18,220 --> 00:23:26,060
Dante, in the time of the Buddha, would the Bodhisattva have an aware of such a status as an Arahan?

190
00:23:27,420 --> 00:23:29,660
I mean, were those things understood back then?

191
00:23:29,660 --> 00:23:30,060
Sorry.

192
00:23:31,020 --> 00:23:35,420
Back in the time of the Buddha, would the, before his enlightenment,

193
00:23:35,420 --> 00:23:38,780
would the Bodhisattva have even been aware of such a thing as an Arahan?

194
00:23:39,820 --> 00:23:40,620
Well, they had the name.

195
00:23:41,420 --> 00:23:41,980
Did they?

196
00:23:41,980 --> 00:23:44,060
No, you didn't. No, yeah, they used the words.

197
00:23:44,060 --> 00:23:47,980
All right, it just means one who assained kind of.

198
00:23:49,500 --> 00:23:52,860
Okay, so they had that concept back then even before his enlightenment?

199
00:23:52,860 --> 00:23:56,060
The word itself was probably, I don't know if it's Buddhist or pre-Buddhist,

200
00:23:56,060 --> 00:23:58,220
but most of the words the Buddha used are pre-Buddhist.

201
00:23:58,220 --> 00:24:01,340
He just adapted them in, like the word saint.

202
00:24:01,340 --> 00:24:02,860
You know, what is the saint in Buddhism?

203
00:24:02,860 --> 00:24:04,140
What is the saint in Christianity?

204
00:24:04,140 --> 00:24:05,260
We still use the word saint.

205
00:24:07,020 --> 00:24:08,380
Do we have a different meaning?

206
00:24:09,340 --> 00:24:10,220
Okay, thank you.

207
00:24:10,220 --> 00:24:16,860
Okay, sometimes during meditation, my breath becomes so subtle, I can't find it.

208
00:24:16,860 --> 00:24:19,100
Should I stay with it until I find it again?

209
00:24:19,100 --> 00:24:21,020
Or should I switch to a different object?

210
00:24:26,140 --> 00:24:30,140
No, you can just say sitting, sitting, or if you feel calm, you can say calm, calm.

211
00:24:30,140 --> 00:24:32,380
I mean, I don't focus, we don't focus on the breath,

212
00:24:32,380 --> 00:24:33,500
so I don't know exactly.

213
00:24:34,300 --> 00:24:37,260
I'm not, I'm not sure about now you're a non-meditator,

214
00:24:37,260 --> 00:24:39,260
so you may not have even read my booklet.

215
00:24:39,260 --> 00:24:42,460
If you haven't read my booklet, I'd recommend you start there,

216
00:24:43,340 --> 00:24:46,780
and then you'll have an idea of what I teach.

217
00:24:46,780 --> 00:24:49,820
I don't teach exactly mindfulness of the breath.

218
00:24:50,460 --> 00:24:54,460
I teach mindfulness of the body, which is kind of based on the breath,

219
00:24:54,460 --> 00:24:58,940
but still we're focusing on the movement of the body.

220
00:24:58,940 --> 00:25:07,660
If meditating is really hard, sometimes should I do it anyway, or try again later.

221
00:25:12,060 --> 00:25:15,420
It depends on how hard is really hard, if it's just,

222
00:25:17,980 --> 00:25:21,020
and I don't think there's any case where you should not meditate because it's too hard,

223
00:25:21,020 --> 00:25:22,540
but you sometimes have to adapt.

224
00:25:22,540 --> 00:25:24,940
If sitting meditations too hard, you could get up and walk.

225
00:25:24,940 --> 00:25:29,820
If walking meditations too hard, you could sit down or both are too hard, you could lie down.

226
00:25:30,780 --> 00:25:34,300
Now, there are ways of making it a little bit easier on yourself, kind of as a

227
00:25:35,020 --> 00:25:39,180
temporary solution, but you have to just be careful that you're not running away from meditation.

228
00:25:39,980 --> 00:25:41,580
Like there was one meditator,

229
00:25:44,220 --> 00:25:46,220
first he had a, first he was sitting on the floor,

230
00:25:46,220 --> 00:25:49,980
and we said you could use a cushion, so he had this little cushion, and he put underneath him,

231
00:25:50,700 --> 00:25:54,300
and then that worked for a while, and then he decided to put another cushion,

232
00:25:54,300 --> 00:25:56,860
and then another cushion, and eventually he was, he was,

233
00:25:58,140 --> 00:26:00,300
might as well have been sitting on a chair, and I said,

234
00:26:00,300 --> 00:26:02,940
you know, the point is to go the other way, the point is,

235
00:26:02,940 --> 00:26:05,500
you want to start on a chair, you can work your way down, but

236
00:26:06,860 --> 00:26:08,380
you had it in the wrong direction.

237
00:26:11,500 --> 00:26:17,820
So that's the only concern is that you're not getting further and further away from the meditation.

238
00:26:17,820 --> 00:26:19,180
Eventually, you have to make a stand,

239
00:26:19,180 --> 00:26:24,220
and then you have to slowly get to the point where you can meditate through anything.

240
00:26:26,060 --> 00:26:31,340
I liked your video on standing meditation and lying meditation. Those were interesting,

241
00:26:31,340 --> 00:26:36,620
because you don't really hear about standing meditation much, but you have a good video on that.

242
00:26:36,620 --> 00:26:39,420
It's interesting for, you know, kind of a niche,

243
00:26:40,460 --> 00:26:42,940
kind of a meditation. I thought it was interesting.

244
00:26:42,940 --> 00:26:47,340
I don't remember that video.

245
00:26:47,340 --> 00:26:52,620
You do, you have standing and lying, which is just interesting, because normally it's of course

246
00:26:52,620 --> 00:26:58,860
walking and sitting, but you have both.

247
00:26:58,860 --> 00:27:15,900
One day is becoming a private Buddha much easier than a fully enlightened Buddha.

248
00:27:15,900 --> 00:27:31,420
It's easier, depends what you mean by, by much easier. They're both, they're both difficult.

249
00:27:31,420 --> 00:27:46,380
And average Joe mentioned that he is a meditator, but he doesn't need to log his hours to show

250
00:27:46,380 --> 00:27:56,780
anyone. I think now he's just trolling us. No, no, no, he says, I don't think he's trolling us.

251
00:27:56,780 --> 00:28:03,100
He says, this is my path, the path this month sent me on. So he follows you, but he doesn't

252
00:28:03,100 --> 00:28:05,580
want to log his time to show anyone else.

253
00:28:09,260 --> 00:28:12,860
But there I've been told and read that the Buddha was raised Hindu. Is this true?

254
00:28:16,700 --> 00:28:23,100
Next. Fernando Abutta is Abutta, both are fully enlightened.

255
00:28:23,100 --> 00:28:30,060
I'm sorry. I can't, this is, it's getting too late. It's been a really, today was,

256
00:28:31,100 --> 00:28:39,100
we put this poster up and we got this poster made up for the Peace Walk next week and had

257
00:28:39,100 --> 00:28:44,380
someone submitted it and then I was looking over it, just looking over it as it had been posted

258
00:28:44,380 --> 00:28:48,540
on the internet and realized we'd forgotten to put the date on it. I had sent it to everyone.

259
00:28:48,540 --> 00:28:53,980
They said, oh, it looks wonderful. Fabulous, perfect. No one actually read it. And then I realized

260
00:28:53,980 --> 00:29:00,460
it doesn't even have the date on it. So then we saw it all day and I had a quiz today. At least my

261
00:29:00,460 --> 00:29:07,900
Latin quiz, but enough, enough. I can't answer all these questions. I mean, really Wikipedia

262
00:29:07,900 --> 00:29:14,300
is good for a lot of these. If you're interested in, like meditation questions are a lot more

263
00:29:14,300 --> 00:29:22,620
interesting to me. To some extent, I'm happy to answer these, but not after this much of a day.

264
00:29:23,820 --> 00:29:29,820
Enough of a day already. Any more day and I can't take it. So good night, everyone.

265
00:29:29,820 --> 00:29:45,740
Good night. Thank you. Thanks, everyone, for showing up. See you tomorrow. See you tomorrow.

