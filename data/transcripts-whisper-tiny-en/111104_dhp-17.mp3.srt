1
00:00:00,000 --> 00:00:06,400
Okay, so the name of the New Year's the leaf of the dandel panda.

2
00:00:06,400 --> 00:00:19,440
Today we're on to verse 17, verse 17 and 18 are actually very similar to 15 and 16, so there's

3
00:00:19,440 --> 00:00:25,680
not going to be much to say about the verses themselves, but we do have some interesting

4
00:00:25,680 --> 00:00:31,820
stories to tell, so hopefully these will be stories that have meaning for us, I don't

5
00:00:31,820 --> 00:00:34,680
think they are, they do.

6
00:00:34,680 --> 00:00:48,400
So this verse goes in that tappati, pajeta pati, papa kari, ubajeta tappati, papa mikatam ti, tappati,

7
00:00:48,400 --> 00:00:58,840
dio tappati, dukatin kato, so the meaning is very similar.

8
00:00:58,840 --> 00:01:11,080
Here he burns, or the boils inside, and here after he burns, he is burnt.

9
00:01:11,080 --> 00:01:15,240
The evil one is burnt in both places.

10
00:01:15,240 --> 00:01:25,240
Speaking of thinking, papa mikatam, I've done evil, evil has been done by me, he burns.

11
00:01:25,240 --> 00:01:34,000
And here and even more, more so, bio, he burns having gone to suffering, having gone

12
00:01:34,000 --> 00:01:42,120
to bougatil, which in this case is referring to places like hounds, in this case referring

13
00:01:42,120 --> 00:01:47,120
to hell, because this is talking about dio datta, and dio datta was a cousin of the

14
00:01:47,120 --> 00:01:55,960
buddhas, who along with several of the buddhas relatives went forth, and the story goes

15
00:01:55,960 --> 00:02:01,000
that the rest of the people who went forth had, because they had done great merit, and

16
00:02:01,000 --> 00:02:07,640
they were the punyakadi, or the katapunyos, as the verses say, they were ones who have done

17
00:02:07,640 --> 00:02:12,280
good deeds in the past, so their minds were in a good way, and they were actually

18
00:02:12,280 --> 00:02:14,800
ordaining for the right reason.

19
00:02:14,800 --> 00:02:20,600
When they became monks, they put their hearts into realizations, and they were actually

20
00:02:20,600 --> 00:02:21,600
quite different.

21
00:02:21,600 --> 00:02:26,680
For instance, Ananda took a lot of time to become enlightenment, because his intentions

22
00:02:26,680 --> 00:02:27,680
were different.

23
00:02:27,680 --> 00:02:33,640
He was interested in remembering the buddhas teaching, and looking after the buddhas, so he

24
00:02:33,640 --> 00:02:36,840
didn't have as much time for meditation.

25
00:02:36,840 --> 00:02:41,800
Then there was Anarudha, who took time as well, because he was caught up in magical powers,

26
00:02:41,800 --> 00:02:49,600
but dio datta was so caught up in his own baseness of mind that he wasn't able to gain

27
00:02:49,600 --> 00:02:56,880
anything except magical powers, or spiritual power from the practice.

28
00:02:56,880 --> 00:03:01,440
So as the rest of them eventually made their way to become at least so dapunyana, eventually

29
00:03:01,440 --> 00:03:10,480
Anarudha only got this, because he was practicing tranquility only, he wasn't interested

30
00:03:10,480 --> 00:03:15,440
in sight, or he wasn't putting his heart into it, or he wasn't able to, because of the

31
00:03:15,440 --> 00:03:18,240
evil in his heart.

32
00:03:18,240 --> 00:03:29,440
So as a result, he gained only this piece in the common mind that allowed him to fly

33
00:03:29,440 --> 00:03:35,000
through the air and read people's minds, or whatever he had of some magical powers, and

34
00:03:35,000 --> 00:03:36,840
he could fly through the air.

35
00:03:36,840 --> 00:03:44,440
So he used these powers to impress the king's son, because all the other monks were being

36
00:03:44,440 --> 00:03:51,760
praised and supported by the people, but dio datta also, because he was having never done

37
00:03:51,760 --> 00:03:59,880
anything really good, and he didn't have the kind of radiance that people liked to praise,

38
00:03:59,880 --> 00:04:05,080
and so there were no lay people looking after him, and also he had none of the wisdom

39
00:04:05,080 --> 00:04:10,880
or attainment, as far as enlightenment, or so none of the donors looking for him to teach,

40
00:04:10,880 --> 00:04:19,440
and so he felt quite jealous, so he tried to think of who could be that he could bring

41
00:04:19,440 --> 00:04:25,640
under his sway and engage in power or more, and so he thought of the king's son, the king

42
00:04:25,640 --> 00:04:30,480
he couldn't touch, because the king was a sort of partner, and he didn't desire, so he

43
00:04:30,480 --> 00:04:39,760
went after the king's son, and he used his magical powers to make this prince, who really

44
00:04:39,760 --> 00:04:44,200
didn't know the difference between the king and evil, and make the prince think that

45
00:04:44,200 --> 00:04:50,040
he was enlightened, and he said, I am dio datta, and so I'm going to be brought, I made

46
00:04:50,040 --> 00:04:57,880
a picture, or he made an image of himself with snakes, and flying through the air, and

47
00:04:57,880 --> 00:05:04,440
this prince thought, oh, this must be a real holy man, and so dio datta was able to get

48
00:05:04,440 --> 00:05:10,040
him under his sway, and get him to kill his father, and get him to help him to kill with

49
00:05:10,040 --> 00:05:15,680
datta, and so he did many, many evil things, David datta, if you don't know the story,

50
00:05:15,680 --> 00:05:21,400
David datta was a pretty bad guy, and if you read the jot, because you can see that

51
00:05:21,400 --> 00:05:27,240
it's something that he's been doing for a long time, so he tried, he released this wild

52
00:05:27,240 --> 00:05:32,040
elephant to kill the buddha, and that didn't work, the buddha just saw this wild elephant

53
00:05:32,040 --> 00:05:39,080
coming, and he held out his hand and sent out loving kindness to the elephant, it's something

54
00:05:39,080 --> 00:05:44,520
that you can find if you're practicing meditation, that you're able to radiate nothing

55
00:05:44,520 --> 00:05:50,440
kindness to animals, it doesn't always work, and if you're not as powerful as the buddha

56
00:05:50,440 --> 00:05:57,880
wouldn't try it on a rabbit dog, that you can feel the power, and so the buddha was able

57
00:05:57,880 --> 00:06:05,920
to extend great power in this regard, and then he tried to send some of these robbers

58
00:06:05,920 --> 00:06:10,960
after the buddha, he tried to drop a rock on the buddha, and none of this was working, and

59
00:06:10,960 --> 00:06:18,120
finally, when he released this elephant, everyone was so scared, and realized that the

60
00:06:18,120 --> 00:06:27,880
buddha was a really evil guy, suddenly he lost all respect, even from the prince, and

61
00:06:27,880 --> 00:06:32,920
so from that point on he had great trouble, and so he had to think of some other way

62
00:06:32,920 --> 00:06:39,920
to gain power, and what he thought of was that he would break the buddha's sangha, so

63
00:06:39,920 --> 00:06:45,280
he devised this plan that he would ask the buddha to instate these rules that were really

64
00:06:45,280 --> 00:06:52,120
strict, he said to the buddha when everyone was in attendance, he came up to the buddha

65
00:06:52,120 --> 00:06:58,560
and said, I asked that you instate these five rules, and one of them was that monks should

66
00:06:58,560 --> 00:07:03,920
always have to live in the forest, that monks should always go on arms, that monks should

67
00:07:03,920 --> 00:07:09,920
only wear ragrobes and so on, and these were rules that the buddha had already stated

68
00:07:09,920 --> 00:07:17,080
that certain people could undertake them and others might not undertake them, because

69
00:07:17,080 --> 00:07:22,640
they're not intrinsically necessary for the practice, and depending on a person's situation

70
00:07:22,640 --> 00:07:29,840
they might actually inhibit progress, so the buddha refused to lay these down, and David

71
00:07:29,840 --> 00:07:35,760
out there went around saying, oh look who's real strict, I said it, can look who's

72
00:07:35,760 --> 00:07:43,400
the lazy one, and so he started speaking badly of the buddha, at some point he also asked

73
00:07:43,400 --> 00:07:48,560
the buddha to step down, he said, oh you're old now, let me run the sangha, and the

74
00:07:48,560 --> 00:07:53,520
buddha said, I wouldn't even give the sangha to, sorry put our mogalana, let alone

75
00:07:53,520 --> 00:08:02,040
a lick spittle like you, a lick spittle I guess, or someone who licks spittle, and he said

76
00:08:02,040 --> 00:08:08,080
it's really called David that, and it was really for the purpose, because the whole way

77
00:08:08,080 --> 00:08:12,400
along the buddha knew that David that there was not a good person, so the question always

78
00:08:12,400 --> 00:08:17,160
comes up, well why did he let David that there were David, and the answer is that he

79
00:08:17,160 --> 00:08:21,800
saw from the beginning where it would lead to, and he knew that David that had been following

80
00:08:21,800 --> 00:08:25,720
him around, and there was no way he could escape, if he had let him be a lay person, maybe

81
00:08:25,720 --> 00:08:32,160
he would have aroused an army to wipe on all the monks or so on, just the pure evil that

82
00:08:32,160 --> 00:08:37,000
was in his heart, and to the buddha thought that he would be a refuge for David that time,

83
00:08:37,000 --> 00:08:42,080
so he met all of this happen, and the reason why he called him a lick spittle was to make

84
00:08:42,080 --> 00:08:48,240
it very, very clear to David that end, to the whole world, to bring out to the light

85
00:08:48,240 --> 00:08:52,640
finally, he wasn't beating around the bush because this was his last life, he couldn't

86
00:08:52,640 --> 00:08:57,640
play games anymore, so he finally, and he told everyone after David that they got very angry

87
00:08:57,640 --> 00:09:04,120
and that he told everyone, he said, David that is evil and on the wrong path and so on,

88
00:09:04,120 --> 00:09:10,360
so of course this kind of, when you say this about someone, it brings about a conflict

89
00:09:10,360 --> 00:09:15,400
in the heart, so he got very, all of his anger and hatred towards the buddha that he

90
00:09:15,400 --> 00:09:23,720
had built up for following the buddha from life after life, it just came boiling up inside,

91
00:09:23,720 --> 00:09:26,560
and so this is where his burning started.

92
00:09:26,560 --> 00:09:33,440
It was from the point where he decided that he was going to take over the sangha, that

93
00:09:33,440 --> 00:09:38,240
he lost all of his magical powers, it's another thing.

94
00:09:38,240 --> 00:09:47,280
So when he decided to take over the sangha, he went around asking all the monks, he

95
00:09:47,280 --> 00:09:51,680
called all the monks, and then all the monks were assembled, he said, whoever agrees with

96
00:09:51,680 --> 00:09:59,360
these five rules should take a ticket, so they had these tickets and some of the new

97
00:09:59,360 --> 00:10:04,400
monks who didn't know it right for wrong without, hey, well this guy is really serious,

98
00:10:04,400 --> 00:10:10,520
maybe the buddha is not really serious and so they followed after the buddha, they were

99
00:10:10,520 --> 00:10:13,880
that they went up to the diocese and taught them.

100
00:10:13,880 --> 00:10:19,960
The buddha sent Sariputa after him in Mughalana and Sariputa in Mughalana went up, so

101
00:10:19,960 --> 00:10:25,200
they kind of funny how it happened, when Sariputa saw, when they were that they saw Sariputa

102
00:10:25,200 --> 00:10:30,520
in Mughalana coming, if at all here come the buddha is too cheap disciples, see I told

103
00:10:30,520 --> 00:10:35,600
you eventually the whole sangha will come around, so Sariputa Mughalana didn't say anything

104
00:10:35,600 --> 00:10:39,840
but they were that at all, they've come to join me and look, and then they were that

105
00:10:39,840 --> 00:10:44,280
they said himself up, like the buddha and he said, I'm going to lie down now, sorry

106
00:10:44,280 --> 00:10:49,560
for the Mughalana, you teach and might be happy, and so he lay down, because the buddha

107
00:10:49,560 --> 00:10:56,640
would have lied down and listened mindfully, they were that they lay down and fell asleep,

108
00:10:56,640 --> 00:11:00,360
and so Sariputa Mughalana saw him fall asleep and so they turned to the monks and they

109
00:11:00,360 --> 00:11:05,640
started teaching them, Mughalana had also psychic powers that he used to impress them

110
00:11:05,640 --> 00:11:14,480
and Sariputa had these incredible wisdom, and so as a result they were able to, they

111
00:11:14,480 --> 00:11:21,520
were able to teach and to convert these followers and they were that, and as a result

112
00:11:21,520 --> 00:11:25,280
allowed them to see the truth and they actually became enlightened.

113
00:11:25,280 --> 00:11:30,960
As enlightened beings, they all, Sariputa said, okay time, let's go back to where the

114
00:11:30,960 --> 00:11:39,200
real teaching is, and so he brought all the monks back to the buddha, which point they

115
00:11:39,200 --> 00:11:44,600
were that as assistant, I forget his name, won't be without that, kicking him in the chest

116
00:11:44,600 --> 00:11:49,080
and saying, look, I told you that Sariputa Mughalana, no good, what are you doing trusting

117
00:11:49,080 --> 00:11:56,080
them, and he kicked them in the chest, and at that point they were that they got very,

118
00:11:56,080 --> 00:12:02,640
very sick, he was quite injured by that, and as a result of the evil in this heart.

119
00:12:02,640 --> 00:12:08,800
At the point when he got very, very sick, as often happens, he started to realize the evil

120
00:12:08,800 --> 00:12:14,280
that was in his heart, right, this is where it starts to frighten you and you can no longer

121
00:12:14,280 --> 00:12:18,440
overpower it, when you're no longer in charge and you can no longer run away from there,

122
00:12:18,440 --> 00:12:19,440
even deeds.

123
00:12:19,440 --> 00:12:23,840
When a person is sick, this is often when they're good and they're bad deeds become

124
00:12:23,840 --> 00:12:31,440
their refuge, either a good refuge or they're prison, so in this case it became a prison

125
00:12:31,440 --> 00:12:36,320
and this is where he finally caved in and realized all the evil that he had done, he became

126
00:12:36,320 --> 00:12:41,560
quite sick, probably even more so when he realized that the monks had deserted him and

127
00:12:41,560 --> 00:12:51,680
he was now all alone, only with a couple of his cronies, and so he asked, he desired,

128
00:12:51,680 --> 00:12:56,360
he asked his followers that he should like to go and see the Buddha, and he couldn't

129
00:12:56,360 --> 00:13:00,640
even stand up, and so he said, please bring me to see the Buddha, and they said,

130
00:13:00,640 --> 00:13:01,640
what are you talking about?

131
00:13:01,640 --> 00:13:07,600
You only wanted to destroy him when you were well, and he lived at the said, it's true

132
00:13:07,600 --> 00:13:12,040
that I wanted to destroy him, that the Buddha never harbored any evil towards me, and I'm

133
00:13:12,040 --> 00:13:19,000
sure he will see me, and so he requested, please, bring me, and so they picked him up

134
00:13:19,000 --> 00:13:24,440
in his bed, and they brought him to Jaitalana.

135
00:13:24,440 --> 00:13:30,440
Now the story goes that because of how evil he had been, and because of his treachery and

136
00:13:30,440 --> 00:13:34,600
breaking apart the sangha, there was no way that he could possibly see the Buddha that

137
00:13:34,600 --> 00:13:40,520
he would have said, he was told by the monks, they were not the ones to come to see him,

138
00:13:40,520 --> 00:13:46,280
the Buddha said, let him come, he'll never see me again, and they brought him, and as they

139
00:13:46,280 --> 00:13:52,480
were bringing him, people said, oh, now he's only a league away, or he's only a mile away,

140
00:13:52,480 --> 00:13:58,680
and Buddha said, let him come right up to my doorstep, he'll never see me again, and so

141
00:13:58,680 --> 00:14:04,240
they brought him on his chariot, all the way to Jaitalana, and they stopped inside

142
00:14:04,240 --> 00:14:13,080
the gate to take a break and to wash and in the pool, and David at the saw them washing,

143
00:14:13,080 --> 00:14:17,480
and so he sat up on his bed, put his feet on the ground, and when he put his feet on the

144
00:14:17,480 --> 00:14:25,240
ground, I guess thinking that he was going to walk to where the Buddha was, his feet sunk

145
00:14:25,240 --> 00:14:28,960
into the earth, the earth couldn't even hold his weight, and so they say he sunk right

146
00:14:28,960 --> 00:14:36,440
into the earth, and all the way to hell, or whatever, and he died there, and the fires

147
00:14:36,440 --> 00:14:45,240
of hell came up, and he was torched and arose in hell, and this had a story, so something

148
00:14:45,240 --> 00:14:47,040
like that.

149
00:14:47,040 --> 00:14:50,480
When he rose in hell, they say the story of David Datta, where he has to know, because

150
00:14:50,480 --> 00:14:57,120
he threat tried to attack the Buddha and tried to attack the Dhamma, which is impossible

151
00:14:57,120 --> 00:15:04,840
to shake, which is immovable, and then the Dhamma, of course, is eternal, trying to attack

152
00:15:04,840 --> 00:15:11,080
that truth and perverted in the form of the Buddha and the Buddha's teaching.

153
00:15:11,080 --> 00:15:18,160
He's now transfixed himself and immovable, so what that means is he's encased in iron

154
00:15:18,160 --> 00:15:24,000
with a spike going from his head out through his bottom, from the right side out through

155
00:15:24,000 --> 00:15:35,240
the left side, and just constant fire and burning, transfixed and immovable for eons.

156
00:15:35,240 --> 00:15:40,880
What the plus sign is that the Buddha said it would have been worse for him if he hadn't

157
00:15:40,880 --> 00:15:44,560
come in contact with the Buddha, if the Buddha hadn't woke him up in his way, because

158
00:15:44,560 --> 00:15:51,520
at the very end he realized, and he gained wisdom and understanding, so the story goes

159
00:15:51,520 --> 00:15:57,920
that, after all of the time that he spends in hell, he will be reborn as a private

160
00:15:57,920 --> 00:15:59,960
Buddha, particularly.

161
00:15:59,960 --> 00:16:04,160
So it's a long story, it's actually much longer than that, I try to invest to a bridge

162
00:16:04,160 --> 00:16:13,440
it, but it's a story of great importance and one that we always talk about and remember,

163
00:16:13,440 --> 00:16:18,280
a story of what not to do if you become a monk, it's a sort of monk that you don't want

164
00:16:18,280 --> 00:16:19,280
to do.

165
00:16:19,280 --> 00:16:26,680
So the lesson is very much in really in accord with the verses before, that a person

166
00:16:26,680 --> 00:16:31,360
who does bad deeds bad things happen to him, they actually burn, and they work tough

167
00:16:31,360 --> 00:16:41,200
at the team, they are vexed here in vexed in the future, they burn really.

168
00:16:41,200 --> 00:16:46,320
So the idea of burning in hell has to do with all of the anger that's built up and the

169
00:16:46,320 --> 00:16:53,240
result of having so much anger inside, but the anger inside itself, of course, is burning.

170
00:16:53,240 --> 00:17:00,880
At just another story in regards to how evil people burned inside, there was once a teacher,

171
00:17:00,880 --> 00:17:09,280
one of my teacher's teachers in Thailand, I read a book about it, he was very much involved

172
00:17:09,280 --> 00:17:17,120
in bringing the past and a meditation to Thailand, and also the Abhidhamma and many things

173
00:17:17,120 --> 00:17:24,760
that Thailand wasn't well equipped with, so from Burma they brought a lot, because in

174
00:17:24,760 --> 00:17:28,400
Burma, of course, they will study directly, study the diptica, these books come from

175
00:17:28,400 --> 00:17:33,600
Burma that we get here, and they would actually read through and study these, whereas

176
00:17:33,600 --> 00:17:38,800
in Thailand, very little study of the diptica is done, so he brought it all over, and that's

177
00:17:38,800 --> 00:17:43,600
why now in Thailand they would actually teach Abhidhamma, for example, and that's for this

178
00:17:43,600 --> 00:17:49,240
meditation comes from, my teacher was sent as part of a group to go to Burma, to Myanmar

179
00:17:49,240 --> 00:17:57,360
to learn meditation, but of course, there were people who didn't like this, and actually

180
00:17:57,360 --> 00:18:07,400
said things like, isn't the Buddhism in Thailand good enough for you, and really people

181
00:18:07,400 --> 00:18:12,400
who didn't like Burma, because Burma had once attacked Thailand, anyway, actually the real

182
00:18:12,400 --> 00:18:17,000
core of it is this monk was quite popular, and what he was doing was quite popular with

183
00:18:17,000 --> 00:18:22,080
the people, because he would teach meditation to ordinary people, or he was involved with

184
00:18:22,080 --> 00:18:26,040
it, and they were teaching Abhidhamma and they were teaching so many new things, so people

185
00:18:26,040 --> 00:18:31,680
were quite interested, and it was all about jealousy, because he started getting power,

186
00:18:31,680 --> 00:18:37,760
and he had a powerful position, and the other monks who were vying for the same position,

187
00:18:37,760 --> 00:18:42,480
or were trying to get to the top position, and thought that he might be getting for it,

188
00:18:42,480 --> 00:18:51,400
getting it, they began to attack him, and he and eventually, a group of them, apparently

189
00:18:51,400 --> 00:19:01,040
including the Sangaraj and a very supreme patriarch, although his involvement is quite clear,

190
00:19:01,040 --> 00:19:06,840
and they accused him of having sexual intercourse with a woman, and they actually found

191
00:19:06,840 --> 00:19:12,320
a woman who was willing to say that he had sexual intercourse with him, and I was reading

192
00:19:12,320 --> 00:19:17,120
this book, it was a wonderful, very thick book, and trying to read through it in Thai,

193
00:19:17,120 --> 00:19:24,360
and when I came to this part with the woman, and then they sent him a letter and said,

194
00:19:24,360 --> 00:19:29,240
you should disrobe, you've done this terrible thing, and we have a witness, and we've

195
00:19:29,240 --> 00:19:34,560
gone through the motions of investigating and found that you're guilty, you should disrobe

196
00:19:34,560 --> 00:19:39,720
and leave, and he said, if I had actually done it, why should I disrobe, or he said,

197
00:19:39,720 --> 00:19:45,080
you should disrobe, means you should cease to become a monk, and they even called him

198
00:19:45,080 --> 00:19:49,520
nuntar or something like that, as much means a monk, they still called him by a monk,

199
00:19:49,520 --> 00:19:55,320
and he said, well, how could I be a monk if I had done this, why should I stop becoming

200
00:19:55,320 --> 00:20:00,760
a monk, because of the wording said, you should cease to become a monk, and he said,

201
00:20:00,760 --> 00:20:05,880
well, I did it, so obviously it's just a sham that they're making up, but the interesting

202
00:20:05,880 --> 00:20:10,760
point that I wanted to make this in regard to this woman, as you turn the pages and you

203
00:20:10,760 --> 00:20:19,160
come to, he said, eventually, they found the woman, and they talked with her, and after

204
00:20:19,160 --> 00:20:25,760
repeated trying to get her to tell the truth, eventually she came forth, and she wrote

205
00:20:25,760 --> 00:20:33,680
this letter, and she came forth by herself, and she said, I just want to sleep at my,

206
00:20:33,680 --> 00:20:41,280
she said, all this, she said, no, I'm sorry for what I did, I realized now that a person

207
00:20:41,280 --> 00:20:48,680
who does evil deeds can never prosper, and what I've done is a horrible thing, and it's

208
00:20:48,680 --> 00:20:53,760
given me nightmares ever since, I haven't been able to sleep, I just, just be read it,

209
00:20:53,760 --> 00:21:02,960
it was such a heart-rending a letter, and she said, that monk isn't guilty of anything,

210
00:21:02,960 --> 00:21:06,800
and I'm writing this letter, because I just want to sleep at my, I don't want to go

211
00:21:06,800 --> 00:21:15,720
to hell, so this kind of thing in a person who actually has some wits about them, and

212
00:21:15,720 --> 00:21:24,360
it's not totally corrupt as Devadatta was, when you do an evil deed, it can really hit

213
00:21:24,360 --> 00:21:32,760
you very hard, and the realization comes that evil really does exist, for people like

214
00:21:32,760 --> 00:21:38,960
Devadatta, they're able to use their force of will, as I said, to cover it up, most

215
00:21:38,960 --> 00:21:44,040
people are not able to do that, and as a result have some semblance of morality, and

216
00:21:44,040 --> 00:21:51,040
I realized when they do evil deeds, but eventually it comes to you, and then you write,

217
00:21:51,040 --> 00:21:55,720
for Devadatta it came when he was very sick, and his mind was weakened, and by the sickness,

218
00:21:55,720 --> 00:22:02,080
and so as a result he wasn't able to cover up the evil deeds, and he was forced to see

219
00:22:02,080 --> 00:22:07,920
them, and then he realized for himself the badness, unfortunately what's done is done,

220
00:22:07,920 --> 00:22:14,920
and it's very difficult.

