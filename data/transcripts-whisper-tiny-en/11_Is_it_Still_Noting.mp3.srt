1
00:00:00,000 --> 00:00:02,000
Is it still noting?

2
00:00:02,000 --> 00:00:05,760
Question, the practice of noting, as I understand,

3
00:00:05,760 --> 00:00:08,800
it involves court vocalizing in language,

4
00:00:08,800 --> 00:00:10,640
a noted phenomenon.

5
00:00:10,640 --> 00:00:14,960
Is there also a noting without vocalizing the phenomenon in court,

6
00:00:14,960 --> 00:00:19,200
or is the court vocalization intrinsic to the practice?

7
00:00:19,200 --> 00:00:24,400
After another way, if one observes the phenomenon but bypasses the mental

8
00:00:24,400 --> 00:00:30,560
enunciation in language of what is observed, does that qualify as noting?

9
00:00:30,560 --> 00:00:36,160
Answer, I think there is a bit of a misunderstanding about what is going on

10
00:00:36,160 --> 00:00:38,160
in your mind generally.

11
00:00:38,160 --> 00:00:42,080
Thinking is an intrinsic part of our mental activity.

12
00:00:42,080 --> 00:00:44,160
We are always thinking.

13
00:00:44,160 --> 00:00:48,800
The practice of meditation is not the creation of a verbal word,

14
00:00:48,800 --> 00:00:51,440
who are not verbalizing anything.

15
00:00:51,440 --> 00:00:55,280
Rather, you are thinking, but the thought is a clear thought.

16
00:00:55,280 --> 00:00:59,360
The thought has a one-to-one correlation with reality.

17
00:00:59,360 --> 00:01:04,240
For example, when you see something, it is not me, it is not mine.

18
00:01:04,240 --> 00:01:07,600
It is not all of the other things that you might think of,

19
00:01:07,600 --> 00:01:10,400
rather, it is just seeing.

20
00:01:10,400 --> 00:01:15,680
I know that many people even experience meditators who ask this question,

21
00:01:15,680 --> 00:01:19,840
or who argue that you are better off to not use the noting.

22
00:01:19,840 --> 00:01:26,320
And that is fine. Different interpretations and different practices are perfectly acceptable.

23
00:01:26,320 --> 00:01:29,840
You could become enlightened without practicing noting.

24
00:01:29,840 --> 00:01:35,440
And certainly, there are different techniques of practice that leads to enlightenment.

25
00:01:35,440 --> 00:01:41,680
All I can say is that in my experience, the benefits of noting should become obvious

26
00:01:41,680 --> 00:01:43,600
if you give it a chance.

27
00:01:43,600 --> 00:01:49,840
One problem I see is that based on our natural inclination to doubt things combined

28
00:01:49,840 --> 00:01:53,760
with our natural disintegration towards objectivity,

29
00:01:53,760 --> 00:01:58,960
once we start seeing reality as it is, the mind revolves and rejects it

30
00:01:58,960 --> 00:02:01,280
because it feels unnatural.

31
00:02:01,280 --> 00:02:06,560
I often hear its express that noting practice feels unnatural.

32
00:02:06,560 --> 00:02:09,280
But ask yourself, what is natural?

33
00:02:09,280 --> 00:02:15,520
Natural is following your defilements, natural is thinking, this is me, this is mine,

34
00:02:15,520 --> 00:02:18,960
this is what I am, I am walking, I am sitting.

35
00:02:18,960 --> 00:02:25,040
Natural is the belief that there is an entity that things are I, me and mine.

36
00:02:25,040 --> 00:02:30,080
The commentary to the Sati Patana Sutta poses and answers the question of what

37
00:02:30,080 --> 00:02:35,040
makes the awareness of the meditator distinct from ordinary awareness.

38
00:02:35,040 --> 00:02:39,120
It says that even ordinary animals know when they are moving.

39
00:02:39,120 --> 00:02:44,080
But their awareness is clouded by delusion and conception of soul.

40
00:02:44,080 --> 00:02:49,920
The awareness that we are trying to cultivate is a very specific type of awareness.

41
00:02:49,920 --> 00:02:54,480
In the Sati Patana Sutta, it describes the meditator state as possessing

42
00:02:54,480 --> 00:03:01,120
participating mataya, which means with just a state of remembrance of the object as it is.

43
00:03:01,120 --> 00:03:06,640
The awareness that we are trying to evoke is a one-to-one recognition of each experience,

44
00:03:06,640 --> 00:03:08,240
just as it is.

45
00:03:08,240 --> 00:03:12,080
Such recognition is not in natural state of human beings.

46
00:03:12,080 --> 00:03:18,320
Our natural state of observation does not contain this one-to-one remembrance or mindfulness.

47
00:03:18,320 --> 00:03:23,280
Even though you may believe that you know walking as walking or sitting or sitting,

48
00:03:23,280 --> 00:03:27,040
there is usually much more going on in the mind.

49
00:03:27,040 --> 00:03:32,000
Our ordinary inclination is to view our experiences as me,

50
00:03:32,000 --> 00:03:34,400
as mine, as self.

51
00:03:34,400 --> 00:03:41,440
Unless our meditation practice forces us to see reality as the moments of experience that it is,

52
00:03:41,440 --> 00:03:45,920
we will inevitably become attached even to our practice,

53
00:03:45,920 --> 00:03:50,000
conceiving of it as self or belonging to self.

54
00:03:50,000 --> 00:03:55,600
The fact that noting is unnatural, going against our natural inclination

55
00:03:55,600 --> 00:03:58,160
is the actual reason for its use.

56
00:03:58,160 --> 00:04:03,920
Its purpose is to change our natural inclination to interprets and extrapolate

57
00:04:03,920 --> 00:04:08,560
our experiences in favor of seeing things just as they all.

58
00:04:08,560 --> 00:04:14,480
Such a change takes considerable efforts more than newcomers to meditation might realize,

59
00:04:14,480 --> 00:04:17,840
because how sad we are in our natural ways.

60
00:04:18,400 --> 00:04:22,480
Meditation forces us to change our entire outlook,

61
00:04:22,480 --> 00:04:26,480
by beyond what we might expect going in.

