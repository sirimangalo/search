1
00:00:00,000 --> 00:00:13,400
Okay, good evening everyone, broadcasting live, Aldio, we're going to do another dumb

2
00:00:13,400 --> 00:00:32,980
fun video.

3
00:00:32,980 --> 00:00:53,700
Hello and welcome back to our study of the Damapada.

4
00:00:53,700 --> 00:01:01,220
Today we continue on with verse 125, which reads as follows.

5
00:01:01,220 --> 00:01:26,560
Good evening and greetings from

6
00:01:26,560 --> 00:01:42,380
who should attack or offend against the city, a person who is without fault up and with

7
00:01:42,380 --> 00:01:59,760
a person who is soon to have pure and an anger that would fall to their blame.

8
00:01:59,760 --> 00:02:17,380
The Meevabalaam patit bhapam, such a fud, that evil bhapah, patit goes back upon the Meevabalaam,

9
00:02:17,380 --> 00:02:18,380
that false.

10
00:02:18,380 --> 00:02:22,260
The evil goes back upon that false.

11
00:02:22,260 --> 00:02:48,460
So if you throw dust against the wind, it comes back into your face, in the same way evil

12
00:02:48,460 --> 00:02:57,280
done to someone who is blameless, comes back to baiting.

13
00:02:57,280 --> 00:03:12,700
So quite memorable words, one of those things for us to keep in mind that it's a description

14
00:03:12,700 --> 00:03:18,880
of the law of karma, that's what this refers to.

15
00:03:18,880 --> 00:03:27,420
But it's even more because the idea is that evil doesn't harm the one who is the object

16
00:03:27,420 --> 00:03:33,000
of evil, evil harms the one who performs the...

17
00:03:33,000 --> 00:03:38,460
In sometimes in seeming magical ways, which is sort of what this story behind this

18
00:03:38,460 --> 00:03:51,260
verse is about, so this story goes that there was a hunter named Koka, and he was...

19
00:03:51,260 --> 00:03:52,260
He had...

20
00:03:52,260 --> 00:03:59,340
The hunter was accompanied by a pack of hounds, so when he would go hunting, he would take

21
00:03:59,340 --> 00:04:06,320
dogs with him and the dogs would pick up his prey or maybe even help him kill it, drag

22
00:04:06,320 --> 00:04:12,860
it down for him to kill, and so he had trained him to be quite vicious.

23
00:04:12,860 --> 00:04:20,340
One day when he was doing his hunting and the forest, he came across a monk, and somehow

24
00:04:20,340 --> 00:04:27,320
he had this idea that seeing a monk was a bad woman, seeing a monk in the morning was...

25
00:04:27,320 --> 00:04:35,000
This idea had been spread about, maybe by enemies of monks or enemies of Buddhism, and

26
00:04:35,000 --> 00:04:44,220
he thought this was a bad woman to see a monk, and so the monk went on his way to...

27
00:04:44,220 --> 00:04:49,620
For arms in the city, and the hunter went on his way to try and catch prey and churned up

28
00:04:49,620 --> 00:04:51,620
the whole day, he caught nothing.

29
00:04:51,620 --> 00:05:02,020
Meanwhile, the monk came back and he just lunged and was perhaps meditating in the forest,

30
00:05:02,020 --> 00:05:05,980
and the hunter, on his way back to the forest, got nothing, he caught sighted the hunter

31
00:05:05,980 --> 00:05:14,200
and got sighted the monk again, and was furious, and thought this monk, the reason, this

32
00:05:14,200 --> 00:05:21,260
monk is the reason why I got nothing, somehow he had this idea, and so he yelled at the

33
00:05:21,260 --> 00:05:30,700
monk and accused the monk of being the cause of his bad luck.

34
00:05:30,700 --> 00:05:35,580
He brought his hounds out and he was ready to set his hounds on the monk, and the monk

35
00:05:35,580 --> 00:05:39,900
begged him, pleaded for him not to do such a horrible thing, and he was innocent that

36
00:05:39,900 --> 00:05:44,660
he had not done anything against the hunter.

37
00:05:44,660 --> 00:05:51,340
Part of it may have been because Buddhist monks were known to be against killing, they

38
00:05:51,340 --> 00:05:57,700
thought people to give up killing, and so as a result there was this idea that somehow

39
00:05:57,700 --> 00:06:08,500
they were bad luck as a result, because it was something that they believed was wrong.

40
00:06:08,500 --> 00:06:18,580
Anyway, this hunter refused to listen to the monk and set his dogs on the monk and

41
00:06:18,580 --> 00:06:26,020
the monk immediately ran and climbed up a tree, and was standing over to one of the branches,

42
00:06:26,020 --> 00:06:32,460
and so what the hunter did, I was so angry, he was really out to get this monk, and

43
00:06:32,460 --> 00:06:38,400
so he took it one of his arrows, and he poked up at the monk's feet, and first the

44
00:06:38,400 --> 00:06:42,500
monk's right foot, and the monk lifted up his right foot, and put his left foot down

45
00:06:42,500 --> 00:06:47,980
instead, and the hunter poked his left foot, and really gouged his feet, and so then

46
00:06:47,980 --> 00:06:55,980
the monk pulled up both feet, but he was in so much pain, and he started to lose mindful

47
00:06:55,980 --> 00:07:04,140
ness, awareness of what he was doing, and he dropped his outer robe, the big robe that

48
00:07:04,140 --> 00:07:13,740
would act as a blanket, and it fell on the hunter, and covered him, and immediately

49
00:07:13,740 --> 00:07:19,060
the dogs turned and looked at the hunter and thought the monk is falling out of the tree,

50
00:07:19,060 --> 00:07:24,620
and they turned on the hunter and ripped him to shreds and killed him, thinking they

51
00:07:24,620 --> 00:07:36,780
were following the master's orders, because they smelled this robe, and the monk looking

52
00:07:36,780 --> 00:07:39,940
down at what was going, he picked up a stick, and he threw it at the dogs, and the dogs

53
00:07:39,940 --> 00:07:46,060
looked up and saw the hunter, the monk was still in the tree, and the scampered off into

54
00:07:46,060 --> 00:07:55,020
the forest, confused, or maybe embarrassed at how foolish they had been, you know, it's

55
00:07:55,020 --> 00:07:56,020
the story.

56
00:07:56,020 --> 00:08:00,900
And the monk actually felt bad about this, he thought, well, you know, it's because

57
00:08:00,900 --> 00:08:08,540
of me that this hunter died, it was really my fault, it was because I dropped this robe

58
00:08:08,540 --> 00:08:13,980
on him, because it was unmindful, is that, do I have any fault in that, and of course

59
00:08:13,980 --> 00:08:21,020
the Buddha said, no, no, be who you are, blameless, and moreover, this, he said, this isn't

60
00:08:21,020 --> 00:08:26,380
the first time, and he told the story of the past that apparently in the past life, this

61
00:08:26,380 --> 00:08:33,180
guy had the same sort of thing happened, and he was a cruel, terrible person, and the same

62
00:08:33,180 --> 00:08:39,940
sort of thing happened, and he was a physician, and he was trying to sell his cures and

63
00:08:39,940 --> 00:08:44,460
sell his services, and no one was buying it, and no one was sick, and he thought, well,

64
00:08:44,460 --> 00:08:51,580
maybe I'll just make people sick, and get them to, then they'll need me, they'll need

65
00:08:51,580 --> 00:08:57,940
my help, if they're sick, and so he took a poisonous snake, and he stuck it into tree,

66
00:08:57,940 --> 00:09:03,820
and he told these boys to go and fetch him this, that there was a bird in there, in

67
00:09:03,820 --> 00:09:08,700
this hole, and they should go and fetch it for him.

68
00:09:08,700 --> 00:09:14,740
And so one of the boys stuck his hand in the tree, and grabbed the snake, thinking it

69
00:09:14,740 --> 00:09:18,740
was this bird that he was going to catch, and when he realized it was a snake, threw

70
00:09:18,740 --> 00:09:26,380
it, freaked out and was really quickly, and it hit the position in the head, and wrapped

71
00:09:26,380 --> 00:09:34,700
around his head, and bit him, and killed him, kind of silly stories, but there's a

72
00:09:34,700 --> 00:09:42,260
story of the hunter, and the hunter and the monk in the tree is a memorable one, how evil

73
00:09:42,260 --> 00:09:53,420
can go wrong, how our deeds sometimes turn back on us, now there are different reasons

74
00:09:53,420 --> 00:09:58,380
why we could say this happens, I mean the most obvious reason for our evil deeds to bring

75
00:09:58,380 --> 00:10:02,420
evil upon ourselves is the effect that they have on our minds.

76
00:10:02,420 --> 00:10:10,860
So anger, and greed, and delusion, the three developments make you care less, when you're

77
00:10:10,860 --> 00:10:19,300
obsessed with something, or you're angry, it's bad karma first and foremost, because

78
00:10:19,300 --> 00:10:24,220
of how it affects your mind, and because of how it changes you, and changes the actions

79
00:10:24,220 --> 00:10:30,100
and the choices that you make.

80
00:10:30,100 --> 00:10:36,420
Furthermore, it does affect the world around you, the second way it does I think is in

81
00:10:36,420 --> 00:10:44,860
how others look at you, so in this case there were no other people involved, but by

82
00:10:44,860 --> 00:10:53,420
disrupting the monk, he brought upon himself at least you could say some sort of chaos,

83
00:10:53,420 --> 00:10:58,660
in which many things are possible, when you attack someone, sometimes they freak out

84
00:10:58,660 --> 00:11:06,660
and it disturbs the order, but you know see I'm trying to get to the point where you

85
00:11:06,660 --> 00:11:11,460
could actually see what this is, a result of his bad karma, because it doesn't seem

86
00:11:11,460 --> 00:11:18,300
like it is, it seems like it was just a freak chance, and 9 out of 10 times, or 99 of 100

87
00:11:18,300 --> 00:11:26,900
times, or 999 out of 1000 times, the hunter would have killed the monk in the tree.

88
00:11:26,900 --> 00:11:33,740
What we want to say is that there is potentially things protecting people who are good, people

89
00:11:33,740 --> 00:11:34,740
who are blameless.

90
00:11:34,740 --> 00:11:38,860
I don't know that you can actually, I don't know that it's actually proper and true

91
00:11:38,860 --> 00:11:43,420
to say that, we want to, and Buddhists like to try and say that, that there are things

92
00:11:43,420 --> 00:11:44,420
protecting you.

93
00:11:44,420 --> 00:11:51,140
I'm not so sure, I would agree that there are probably beings, like who knows, maybe

94
00:11:51,140 --> 00:11:58,140
there was some angel or spirit involved with this, that pulled the robe off the monk

95
00:11:58,140 --> 00:12:00,580
and dropped it on the hunter.

96
00:12:00,580 --> 00:12:06,420
That makes sense to me, if such beings exist, of course, many, many people think it's ridiculous

97
00:12:06,420 --> 00:12:08,820
that such beings would exist.

98
00:12:08,820 --> 00:12:12,420
I'd agree that it seems kind of fair-fetched that such a being would just be there and

99
00:12:12,420 --> 00:12:16,820
get involved, but hey, who knows, there's that kind of thing, and so that's a direct

100
00:12:16,820 --> 00:12:21,820
result of karma, people don't, other beings don't appreciate when you're cruel to people

101
00:12:21,820 --> 00:12:24,660
who don't deserve it.

102
00:12:24,660 --> 00:12:29,420
And that sort of thing really is a part of karma.

103
00:12:29,420 --> 00:12:37,220
Good people attract, others who would protect them, attract good friends, and so if you

104
00:12:37,220 --> 00:12:44,540
try to harm a harmless being, you're less likely to succeed just because they're surrounded

105
00:12:44,540 --> 00:12:45,540
by good people.

106
00:12:45,540 --> 00:13:02,100
But I think you could also argue that the power of goodness has some sort of reverberations

107
00:13:02,100 --> 00:13:05,460
in the universe, like it sets things up in a certain way.

108
00:13:05,460 --> 00:13:13,140
I think you could argue that, I think you could point to ways in which it's true that

109
00:13:13,140 --> 00:13:21,580
a person who has lots of good deeds, a person who's never done the evil deeds, is somehow

110
00:13:21,580 --> 00:13:29,380
in a different sort of train or groove, you might say, from a person who is intended

111
00:13:29,380 --> 00:13:30,380
on evil.

112
00:13:30,380 --> 00:13:36,460
They're so opposite that they're almost living in different universes, and it's very hard

113
00:13:36,460 --> 00:13:38,900
for them to come together now that's possible.

114
00:13:38,900 --> 00:13:43,580
But I think you could point at that as being a reason why it takes really great amount

115
00:13:43,580 --> 00:13:46,660
of effort to harm someone who is harmless.

116
00:13:46,660 --> 00:13:50,060
I don't want to say it's certainly not magic.

117
00:13:50,060 --> 00:13:58,740
And I think there's much, much more going on, and we often will, it's common for people

118
00:13:58,740 --> 00:14:08,260
to underestimate the power of goodness and the power of evil, evil is something that shrinks

119
00:14:08,260 --> 00:14:16,860
inward or it comes back upon you, good is something that's expensive and it's spreads.

120
00:14:16,860 --> 00:14:23,660
So you can't, spreading evil is not the same as spreading good, it's much more likely

121
00:14:23,660 --> 00:14:28,540
to come back upon you.

122
00:14:28,540 --> 00:14:33,500
I think at any rate, I would argue that there's room for that, I think it's something

123
00:14:33,500 --> 00:14:40,780
that is very hard to find proof or evidence even for, but I would say that there's room

124
00:14:40,780 --> 00:14:47,300
for the idea that good protects goodness, protects good people, and people who try to do

125
00:14:47,300 --> 00:14:54,220
evil towards good people for various reasons, have a harder time of it, and it's much

126
00:14:54,220 --> 00:14:57,020
more likely to come back and bite into the face.

127
00:14:57,020 --> 00:15:02,620
I mean, absolutely even when they get away with it, it's far worse, and that there's

128
00:15:02,620 --> 00:15:13,260
no question that it eventually comes back, far worse on the person who's done an evil

129
00:15:13,260 --> 00:15:21,260
deed towards someone who doesn't deserve it, who hasn't done anything to instigate it,

130
00:15:21,260 --> 00:15:25,020
who is a blameless.

131
00:15:25,020 --> 00:15:29,820
Like if this monk had gone around setting animals free, well then you could say maybe

132
00:15:29,820 --> 00:15:38,820
somewhat metal some, or if he had purposefully wished or if he had done everything he couldn't

133
00:15:38,820 --> 00:15:47,900
to ruin this hunter and to do harm him, he'd done something to warrant even littlest bit

134
00:15:47,900 --> 00:15:56,940
of malice, then it would be different, you could argue, and it would be less of a weighty

135
00:15:56,940 --> 00:16:02,380
crime, but to harm someone is harmless, you know it in your bones, you don't have to be

136
00:16:02,380 --> 00:16:08,580
Buddhist to believe this, it's just far more horrific to harm someone, almost than nothing

137
00:16:08,580 --> 00:16:11,180
to deserve it.

138
00:16:11,180 --> 00:16:15,380
If for that reason, for sure it's going to affect your mind and make you blind to the

139
00:16:15,380 --> 00:16:23,380
dangers that you're facing, and blind to your actions, and blind to the circumstances and

140
00:16:23,380 --> 00:16:32,700
so on, I think if it argue more over that it's such a powerful act that it jars with

141
00:16:32,700 --> 00:16:38,940
the universe, as a result is harder to perform, I'd like to think that there are things

142
00:16:38,940 --> 00:16:46,020
that protect people's lives, just aspects of the universe that are fit together in

143
00:16:46,020 --> 00:16:53,340
such a way that it's not as easy to kill someone as we might think, especially if they

144
00:16:53,340 --> 00:16:54,340
don't deserve it.

145
00:16:54,340 --> 00:17:03,340
The idea that, to some extent, most dealing, most deeds for good or for evil have something

146
00:17:03,340 --> 00:17:09,340
to do with past karma, the idea that there is some sort of connection there, I don't

147
00:17:09,340 --> 00:17:14,140
think all, I think it seems possible to create a new karma with someone, so if someone

148
00:17:14,140 --> 00:17:20,020
is innocent, you can harm them, it's a very weighty karma, but you're starting something

149
00:17:20,020 --> 00:17:26,420
new, and it's quite awesome that they will come back and harm you, even unintentionally,

150
00:17:26,420 --> 00:17:32,980
like in this case, usually it takes lifetimes to do that, but anyway, these are aspects

151
00:17:32,980 --> 00:17:41,300
of karma that are kind of magical and hard to understand or hard to believe, or people

152
00:17:41,300 --> 00:17:47,780
don't think of them as true, but this isn't so important for our practice, what's important

153
00:17:47,780 --> 00:17:55,900
for our practice is as with all of these sorts of stories, the consequences, at no question

154
00:17:55,900 --> 00:18:02,180
there are consequences to our our evil deeds and our good deeds, and I think that's

155
00:18:02,180 --> 00:18:09,380
exemplified here, well, if you're a good person, you don't deserve to be harmed, it's

156
00:18:09,380 --> 00:18:15,820
harder for people to harm you, it's harder for people to wonder, it takes a really base

157
00:18:15,820 --> 00:18:25,620
and evil person to do it, and you've got to wonder whether this monk was pleading with

158
00:18:25,620 --> 00:18:32,300
this hunter to stop, for his own benefit or for the benefit of the hunter, because certainly

159
00:18:32,300 --> 00:18:42,820
even death, even death by dog, by wild, rabid dogs, is preferable, we don't think of it

160
00:18:42,820 --> 00:18:53,340
this way, but it's actually far preferable than to torture an innocent person, because

161
00:18:53,340 --> 00:18:59,740
by being, the Buddha said many things like this, he said you should, you should rather be

162
00:18:59,740 --> 00:19:05,580
killed and or tortured than to do an evil deed, because being killed and tortured doesn't

163
00:19:05,580 --> 00:19:14,220
lead you to a bad future, doesn't hurt your mind, doesn't affect you in the way that

164
00:19:14,220 --> 00:19:24,780
evil does, so I mean, it's a big part of meditation that a beginner has to come to understand

165
00:19:24,780 --> 00:19:31,580
that pain in the meditation, when you're sitting in meditating and you feel pain, pain

166
00:19:31,580 --> 00:19:40,980
is not your enemy, unpleasantness of all kinds, it's itching or heat or loud noise

167
00:19:40,980 --> 00:19:51,580
or thoughts, nightmares, visions, etc. This is not the enemy, this is not the problem,

168
00:19:51,580 --> 00:19:59,580
the problem is evil, what we call unwholesomeness, so greed, anger and delusion, these are

169
00:19:59,580 --> 00:20:07,700
problems, they call, they bring about things like pain and stress and suffering, if

170
00:20:07,700 --> 00:20:16,740
becoming to see that difference is very important, to see that it's the evil that we have

171
00:20:16,740 --> 00:20:27,020
a problem with, so this quote is describing a very extreme sort of karma, the deed, the

172
00:20:27,020 --> 00:20:32,860
evil deed that is performed towards someone who doesn't deserve it, but it brings up all

173
00:20:32,860 --> 00:20:45,420
these issues, who is the person who really receives the effect of a deed, if you try

174
00:20:45,420 --> 00:20:51,620
to harm someone, what they might be, a hurt or even killed, but that's it, it's actually

175
00:20:51,620 --> 00:20:57,900
quite insignificant compared to the evil that comes back upon you, so this is throwing dust

176
00:20:57,900 --> 00:21:02,620
in the wind, if you're a dust in the wind it comes back on you, evil deeds are very

177
00:21:02,620 --> 00:21:08,060
much like that, don't think of it, right? I think if you could get away with it, hurting

178
00:21:08,060 --> 00:21:13,940
someone else really bad for the person who you hurt or killed, it's actually very much

179
00:21:13,940 --> 00:21:19,540
the other way, that's what this verse teaches, this is the claim that's being made, and

180
00:21:19,540 --> 00:21:25,500
I think there's evidence, especially for meditators, that this is very much, I know the

181
00:21:25,500 --> 00:21:32,180
evidence, is very much the case, it's just trying to get the idea that maybe there's even

182
00:21:32,180 --> 00:21:39,300
more than just the obvious, the scars it leaves on your mind, because that's the worst,

183
00:21:39,300 --> 00:21:47,940
but it seems that actually there are cases of being protected when harm someone, it actually

184
00:21:47,940 --> 00:21:52,740
physically hurts you right then there, that's considered to be Dita dhamma reed in the

185
00:21:52,740 --> 00:22:00,060
outcome, a comma that gives results in this life, right here and now, as opposed to the

186
00:22:00,060 --> 00:22:04,380
indirect, where it changes you and those, the result you make different life choices and

187
00:22:04,380 --> 00:22:09,060
other people make different life, different choices in regards to you and how they think

188
00:22:09,060 --> 00:22:17,900
of you, and eventually it harms you in an indirect way, either way, this is all part

189
00:22:17,900 --> 00:22:25,100
of the concept of good and evil, in Buddhism, in our meditation practice, we strive for

190
00:22:25,100 --> 00:22:33,740
good, and we do it rationally, because we're told it's good, or because it's good in

191
00:22:33,740 --> 00:22:39,420
another self, because we really believe that it's the way to peace, happiness, and freedom

192
00:22:39,420 --> 00:22:45,420
from suffering, so that's our practice, and that's the dhamma pattern for tonight, thank

193
00:22:45,420 --> 00:22:50,500
you for tuning in, wishing you all good practice, and peace, happiness, and freedom from

194
00:22:50,500 --> 00:22:55,420
suffering, thank you.

195
00:22:55,420 --> 00:23:24,100
Okay, we can now go live with a video, everyone broadcasting live, January 7, 2016, with

196
00:23:24,100 --> 00:23:31,660
me tonight is Robin, broadcasting live, so if you have questions, you come on over to

197
00:23:31,660 --> 00:23:37,620
meditation.ceremungalow.org and you can ask them there, no, we're not answering questions

198
00:23:37,620 --> 00:23:50,060
on YouTube, today we had a meeting of the McMaster Buddhism Association executive, so we

199
00:23:50,060 --> 00:24:06,780
are, our plan is to, we're going to have meetings weekly, and there's a second I have to

200
00:24:06,780 --> 00:24:36,700
turn this stuff up, I forgot, it's great, we're good, so we're going to have meetings,

201
00:24:36,700 --> 00:24:46,180
to just to get to know people, to bring people together, maybe we'll give some teaching

202
00:24:46,180 --> 00:24:54,500
on Buddhism during the meeting, and we talked about these five-minute meditation lessons

203
00:24:54,500 --> 00:25:04,460
in the table, have to put the banner up, Robin, I'm sorry, but something that order went

204
00:25:04,460 --> 00:25:09,060
all wrong, because I've got rope and I've got a banner and there's no way to connect

205
00:25:09,060 --> 00:25:15,980
the two together, no, like when I did it, when I did a mock order and went through it,

206
00:25:15,980 --> 00:25:22,500
you'd ask me if I wanted to ram it, so our sticky things, you didn't get that, so did

207
00:25:22,500 --> 00:25:37,140
you pick the sticky things up, I mean I didn't order it, I mean I just, I didn't decide

208
00:25:37,140 --> 00:25:53,740
if you wanted grommet, so it just has to, we want your rope, or as it dies, so maybe I missed

209
00:25:53,740 --> 00:26:00,460
something, can you just cut the holes in it, or would you ram it, it doesn't really work

210
00:26:00,460 --> 00:26:10,300
though, and the best thing is to punch holes and make grommet, get grommet, maybe we should

211
00:26:10,300 --> 00:26:17,940
do that, and probably what I was going to do is just use duct tape to pass it to the

212
00:26:17,940 --> 00:26:35,180
holes, let's check on the website and see if this can be sent back, it looks like you have

213
00:26:35,180 --> 00:26:44,440
the grounds for it, you know, it's not any time for next Tuesday, I know how to use

214
00:26:44,440 --> 00:27:12,860
grommet, maybe we can go to the hardware store and get some grommets, it seems possible,

215
00:27:12,860 --> 00:27:24,100
or like the velcro, sticky things on the back, but you don't have the prommets at the University

216
00:27:24,100 --> 00:27:32,680
Summit, you know, that's interesting, but yeah, no, it's going to be in front of a window

217
00:27:32,680 --> 00:27:49,840
actually, probably, if you big dowel, along dowel, what does that do, if you had a dowel,

218
00:27:49,840 --> 00:27:54,080
I don't know how long the banner is, but if you had a dowel you could sew it on to it

219
00:27:54,080 --> 00:28:01,080
and then it would be like a stick, put a string on it, put it in it, sew it, and make

220
00:28:01,080 --> 00:28:24,480
it, you know, I think I'll use duct tape, duct tape sounds good, and we have questions tonight,

221
00:28:24,480 --> 00:28:51,480
I don't know, I don't know, we've done videos on music, yes, let's leave it at that,

222
00:28:51,480 --> 00:28:59,280
is it possible that someone in meditation who has eliminated all desire could be so absolutely

223
00:28:59,280 --> 00:29:04,480
immersed in mindfulness of what things are, but he dies dehydrated for not drinking water,

224
00:29:04,480 --> 00:29:11,480
do you know of any case in which this has happened, is this a person who's meditative with

225
00:29:11,480 --> 00:29:26,280
it? I mean, it sounds like a bit of a misunderstanding of what mindfulness is, but maybe not,

226
00:29:26,280 --> 00:29:33,980
I mean, okay, it's not really mindfulness that you're talking about, it's because mindfulness

227
00:29:33,980 --> 00:29:51,680
can involve all of these awarenesses, it's more like focus, which you know can come from mindfulness,

228
00:29:51,680 --> 00:30:03,960
can come from mindfulness practice, I don't think so, so mindfulness brings you

229
00:30:03,960 --> 00:30:08,540
into the present moment, I mean, just off the cuff, I think technically I get what you're

230
00:30:08,540 --> 00:30:15,540
saying, but really practically speaking, it's not the way it goes, I don't have the technical

231
00:30:15,540 --> 00:30:27,700
aspect of why it is so, but the meditators are more aware of the wants and needs of the body.

232
00:30:27,700 --> 00:30:35,900
Now they might consciously choose to ignore them, but they wouldn't suddenly say, oh, I'm

233
00:30:35,900 --> 00:30:41,500
dehydrated and I didn't know it because I was just experiencing it as sensations or something.

234
00:30:41,500 --> 00:30:53,100
That would require intense concentration that I don't think is really a part of insight meditation.

235
00:30:53,100 --> 00:30:59,260
So mindfulness really helps you, you're super conscious of the fact that there's the dehydration

236
00:30:59,260 --> 00:31:04,220
going on and all the aspects of knowing that it's being, that it's you being dehydrated

237
00:31:04,220 --> 00:31:13,300
are all still likely to be there, but it's just the knowledge of the concepts in the mind

238
00:31:13,300 --> 00:31:18,420
that those concepts could be absent, but I don't think they're more likely to be absent

239
00:31:18,420 --> 00:31:28,020
just because you're mine. I don't think it goes to that extent in most cases, and that

240
00:31:28,020 --> 00:31:32,220
I'm willing to entertain that it's possible. Near the hand, why the heck are you asking

241
00:31:32,220 --> 00:31:41,020
such a question?

242
00:31:41,020 --> 00:31:44,980
I have been wanting to go down the path of the Buddhist monk for years now. I have

243
00:31:44,980 --> 00:31:50,380
even planned to go to Nepal to travel and learn the best I could before becoming a monk.

244
00:31:50,380 --> 00:31:55,540
There is one thing troubling me though, is a relationship with another person having children.

245
00:31:55,540 --> 00:32:01,940
I'm not with anybody, I'm just scared of getting it to a relationship and then later

246
00:32:01,940 --> 00:32:07,220
I'm like hurting that person because I would like to leave and do as I wished before I become

247
00:32:07,220 --> 00:32:12,220
a monk. I don't know if I'm overthinking this or not. This has been on my mind and I'm

248
00:32:12,220 --> 00:32:17,220
having troubles except in meditation.

249
00:32:17,220 --> 00:32:23,540
I'm quite understand, but I think it's like you're afraid of getting into a relationship

250
00:32:23,540 --> 00:32:37,700
because you might become a monk. Is that right? It's not really... I don't know what

251
00:32:37,700 --> 00:32:51,460
I have to say to you. I mean, so don't get into a relationship. What can I do for you?

252
00:32:51,460 --> 00:32:55,700
If you mean like having trouble because you want to get into a relationship but you also

253
00:32:55,700 --> 00:33:01,860
want to become a monk, well, good luck. Probably you want to become a monk. It's easy

254
00:33:01,860 --> 00:33:08,860
to actually do it.

255
00:33:08,860 --> 00:33:24,140
This is a question I actually asked to me from Ryan. I looked on an Expedia today and

256
00:33:24,140 --> 00:33:29,780
round trip tickets to from Toronto to Bangkok started around $1,000. With that being said,

257
00:33:29,780 --> 00:33:34,300
what do we need to do to get the ball rolling to buy Monday to get to see Ajahn Thong? Is

258
00:33:34,300 --> 00:33:38,180
it just as simple as setting up another eukaryon campaign? Let me know whatever I can do

259
00:33:38,180 --> 00:33:47,340
to help. It is and it isn't simple. Only in the timing of it is many of you know we rented

260
00:33:47,340 --> 00:33:53,740
a house in the organization rented a house in September to set up four months during meditation

261
00:33:53,740 --> 00:34:00,940
center and we did a fundraising drive which provided funds for about nine months of operation

262
00:34:00,940 --> 00:34:07,460
at the monastery which was wonderful but it's not enough to keep the monster gun perpetually.

263
00:34:07,460 --> 00:34:13,420
So we actually had planned for February to do another fundraising campaign to judge continued

264
00:34:13,420 --> 00:34:19,660
support of the monster in meditation center to see if it can keep going perpetually and

265
00:34:19,660 --> 00:34:24,220
that's really crucial because you know it was wonderful the initial support but what's

266
00:34:24,220 --> 00:34:29,020
crucial is whether there's enough support to keep it going. So that's kind of what the

267
00:34:29,020 --> 00:34:34,300
organization was gearing up to do to do another fundraising drive or the one stirring

268
00:34:34,300 --> 00:34:41,060
meditation center. So you know what? Well the trip sounds wonderful. It's just the timing

269
00:34:41,060 --> 00:34:46,300
of it is a little bit premature. After we do the second fundraising campaign and see if there

270
00:34:46,300 --> 00:34:51,020
is continued support to keep the monster in meditation center value and that would be

271
00:34:51,020 --> 00:34:57,180
you know a great time to talk about extra you know extra things like a trip to Asia which

272
00:34:57,180 --> 00:35:01,820
it would obviously be a wonderful thing for Monday to go see a teacher and we see the blessing

273
00:35:01,820 --> 00:35:09,020
and everything. It's just a little bit premature at this point I think but you know it's

274
00:35:09,020 --> 00:35:15,500
certainly not up to me. It's a joint decision with the directors of Sermon Global International

275
00:35:15,500 --> 00:35:22,700
which includes Monday so you can certainly chime in on that too. Yeah I don't want to talk about those

276
00:35:22,700 --> 00:35:29,500
things. Leave it to you thank you. All things in good time we just need maybe just a little

277
00:35:29,500 --> 00:35:34,540
bit of patience right now to see you know what what sort of support is available is continuing

278
00:35:34,540 --> 00:35:40,140
support for the monster in meditation center. We'll be rolling that out in a couple of weeks. We had

279
00:35:40,140 --> 00:35:45,500
planned it as a fall campaign and a spring campaign. The spring campaign beginning in February

280
00:35:45,500 --> 00:35:56,620
and a couple of weeks. I guess I'd just say that it's hard to rationalize it you know flying

281
00:35:56,620 --> 00:36:04,140
flinging yourself across the from one end of the earth to the other and the purpose isn't

282
00:36:04,140 --> 00:36:08,380
you know it's not like my teacher doesn't know about this monster he doesn't know what I'm doing

283
00:36:08,380 --> 00:36:17,260
I'm sure he'd like me to come. I would like to go but it doesn't really seem worth the

284
00:36:18,940 --> 00:36:29,260
difficulty worth the trouble especially when we have other troubles here or challenges here that we're

285
00:36:29,260 --> 00:36:34,300
facing. We actually don't know if we have troubles or challenges that are you know if you're

286
00:36:34,300 --> 00:36:40,060
supporters your followers are very generous. We haven't we simply haven't asked what the set up for

287
00:36:40,860 --> 00:36:47,180
fundraising campaign it was a limited online campaign. There's always a support page at the website

288
00:36:47,180 --> 00:36:53,580
surmunglo.org but that doesn't tend to be something that people I think are very often so

289
00:36:53,580 --> 00:36:58,940
well there's always an opportunity to support not many people actually go to the support page

290
00:36:58,940 --> 00:37:05,100
at the surmunglo.org website so based on that we're just old and concerned because there hasn't

291
00:37:05,100 --> 00:37:09,340
been much support in the past couple of months but we didn't have an active fundraising campaign

292
00:37:09,340 --> 00:37:13,580
that was kind of out there and being promoted to ask people some. We don't know that there's a problem

293
00:37:13,580 --> 00:37:19,180
there could be no problem you know a month from now come on straight but we funded for another six

294
00:37:19,180 --> 00:37:24,620
months and all as well. I'm sure the organization would really like to send you to Asia to

295
00:37:24,620 --> 00:37:32,860
see how John Tom is just a little bit you know timing. Yeah I mean I wasn't expecting to go

296
00:37:33,900 --> 00:37:43,660
I said that I wasn't planning to go to Asia in the near future just kind of came up. Also there's

297
00:37:45,100 --> 00:37:50,700
might be half part or partly paid for we'll see that would change things I suppose.

298
00:37:50,700 --> 00:37:56,540
Yeah that would change things quite a bit because that would make it you know a very affordable

299
00:37:56,540 --> 00:38:03,020
way to go over and see your teacher which would be wonderful. Right so there's that that's a

300
00:38:03,020 --> 00:38:06,700
good reason you know if I got this opportunity where half of it's going to be paid for well

301
00:38:07,500 --> 00:38:13,260
yeah it's better than waiting until we have to pay for all of it right? Yeah yeah okay anyway

302
00:38:13,260 --> 00:38:20,540
that's all they're talking about money stuff too much where people think where that's what our

303
00:38:20,540 --> 00:38:28,780
focus is it's not. Anyway it looks like that's it right? Yes. All right because we did

304
00:38:28,780 --> 00:38:37,260
them up at it tonight so that's going to go up. Otherwise I think Saturday I'll try to do

305
00:38:37,260 --> 00:38:45,020
the Buddhism 101 videos so I have to look and see where I left off many years ago

306
00:38:46,140 --> 00:38:50,940
and pick that up or we left off try and get through that. I kind of left off because I was having

307
00:38:50,940 --> 00:38:57,580
trouble figuring out how to address it from a modern perspective. I think it's the next one is

308
00:38:57,580 --> 00:39:03,820
talking about heaven which I have to study it a little bit and try and get a grasp of exactly

309
00:39:03,820 --> 00:39:12,060
what concept the Buddha is trying to trying to pass on and how to translate that from modern

310
00:39:12,060 --> 00:39:17,820
viewers such that no because most people aren't thinking too much about heaven so how far do we

311
00:39:17,820 --> 00:39:22,860
want to take that? Do we want to have people focus on the idea of heaven as a part of Buddhism?

312
00:39:22,860 --> 00:39:30,220
I think so to some extent but I want to bring in the bigger concept, bigger context.

313
00:39:30,220 --> 00:39:35,260
I think karma I think the next one is going to be about karma as a result

314
00:39:36,780 --> 00:39:42,460
that the results of our good deeds. Why should we do good deeds?

315
00:39:45,340 --> 00:39:49,580
Okay anyway good night everyone thank you all for tuning in thanks Robin for your help.

316
00:39:49,580 --> 00:40:05,500
That's what you want. Thank you Bhante. Good night.

