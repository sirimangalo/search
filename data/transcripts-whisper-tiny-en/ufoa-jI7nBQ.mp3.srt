1
00:00:00,000 --> 00:00:09,000
Do you think secular mindfulness programs are valuable in and of themselves or do you think that mindfulness is only valuable within a larger Buddhist framework?

2
00:00:17,000 --> 00:00:19,000
Clemente crushed his computer doing that.

3
00:00:23,000 --> 00:00:27,000
It was such a profound thing that it's just the weight of it.

4
00:00:27,000 --> 00:00:35,000
That was common in the fact that it was Google suppressing the revelation.

5
00:00:36,000 --> 00:00:39,000
It's censorship. No, don't give them the answer.

6
00:00:44,000 --> 00:00:49,000
Okay, I guess I'll answer the start answering this one. No one else is going to.

7
00:00:50,000 --> 00:00:52,000
I think secular mindfulness programs are great.

8
00:00:52,000 --> 00:00:56,000
I don't think I think Buddhism isn't this program, right?

9
00:00:56,000 --> 00:00:58,000
Has anyone with me?

10
00:00:58,000 --> 00:01:00,000
Buddhism is a secular mindfulness program.

11
00:01:00,000 --> 00:01:04,000
It's a secular religious mindfulness program.

12
00:01:04,000 --> 00:01:16,000
It's secular because it has nothing to do with belief or dogma.

13
00:01:16,000 --> 00:01:19,000
It's religious because you have to take it seriously.

14
00:01:19,000 --> 00:01:21,000
It's meant to be taken seriously.

15
00:01:21,000 --> 00:01:24,000
Religion is opposed to negligence.

16
00:01:24,000 --> 00:01:27,000
Religions is the opposite of negligence.

17
00:01:27,000 --> 00:01:35,000
What else do you need? If you're practicing mindfulness, what do you need?

18
00:01:35,000 --> 00:01:42,000
The problem is they often water it down and they're not really pushing mindfulness as hard as monks will.

19
00:01:42,000 --> 00:01:47,000
Because a monk will say, oh, you're bleeding to death? Well, be mindful of it.

20
00:01:47,000 --> 00:01:51,000
The secular programs can get sued for saying such things.

21
00:01:51,000 --> 00:01:56,000
You're dying, you're dying of, oh, you're throwing up in your...

22
00:01:56,000 --> 00:01:58,000
Oh, well, be mindful.

23
00:01:58,000 --> 00:02:00,000
Remember that all things are impermanent.

24
00:02:05,000 --> 00:02:10,000
The more civilized society is...

25
00:02:10,000 --> 00:02:18,000
Yeah, the things we can do to our meditators here, it's like, yeah, yeah, well, you know, you couldn't go see a doctor if you want, but you can also just be mindful of it.

26
00:02:18,000 --> 00:02:20,000
What if I die?

27
00:02:20,000 --> 00:02:24,000
If you die, I promise I'll do the chanting at your funeral, free.

28
00:02:28,000 --> 00:02:31,000
That's what my teacher says, actually, when people say they're...

29
00:02:31,000 --> 00:02:35,000
They start worrying, he says, don't worry if you die, I'll do the chanting for free.

30
00:02:39,000 --> 00:02:40,000
That is it.

31
00:02:40,000 --> 00:02:42,000
I haven't had anyone die yet.

32
00:02:44,000 --> 00:02:49,000
But that's the difference really. It's not so extreme, but a lot of people...

33
00:02:49,000 --> 00:02:51,000
I only know this from second-end experience.

34
00:02:51,000 --> 00:02:53,000
People say it's very watered down.

35
00:02:53,000 --> 00:03:04,000
You've got people sitting up on their zafus and whatever these croissants that they sit on and teachers telling you not to sit through pain, not to be mindful of pain.

36
00:03:04,000 --> 00:03:08,000
Not to that extent, but be gentle with yourself.

37
00:03:08,000 --> 00:03:16,000
You know, there's that one aspect that it tends to be more watered down because monks are like, do or die.

38
00:03:16,000 --> 00:03:20,000
I mean, ban or bust. If I die, that's a great experience.

39
00:03:20,000 --> 00:03:24,000
Sickness is cool and stuff.

40
00:03:24,000 --> 00:03:32,000
I mean, that's generalization, but I think it's a valid one.

41
00:03:32,000 --> 00:03:42,000
Certainly, if a secular mindfulness program has all the potential to be just as valuable as one place in Buddhist cosmology.

42
00:03:42,000 --> 00:03:50,000
I mean, we did talk really about rebirth and I do have personally this belief that ideas like karma and rebirth can help.

43
00:03:50,000 --> 00:03:58,000
If you quote unquote believe in them, they tend to steer you in the right direction because they tend to complement reality.

44
00:03:58,000 --> 00:04:00,000
And you'll see it from...

45
00:04:00,000 --> 00:04:06,000
So if you take it from the other end where you believe in karma and rebirth, you'll start to see it from their experiential end as well.

46
00:04:06,000 --> 00:04:07,000
And they'll meet up together.

47
00:04:07,000 --> 00:04:14,000
If, on the other hand, you believe in death, there's nothing that can actually interfere with your observation of the truth.

48
00:04:14,000 --> 00:04:38,000
But so you can... I don't know. You don't have to take it, but it can be helpful because it happens to be true.

