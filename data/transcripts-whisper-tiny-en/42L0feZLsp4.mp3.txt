Okay, we're live.
there we go.
As we hear,
Atak was a motor with a hoaty bay
That's a jay,
I was so weak to know
We want one down
I knew Saratoy
Wanda Manu Saratou
I want San Ganga
I knew Saratou
Opekaku Saratou
I see the San Tati
So Kana ata vamanu
Ananya, I'm picking cheap, Anja, dhampa, chak, tung, wa, yu, wa, yu, dhampa, linnam.
I am good, chata, also, Aja, tika, wa, yu, dattu, yu, yu, yu, yu, yu, yu, yu, yu, yu,
dattu, wa, yu, yu, dattu, yu, yu, ha.
Tanya, dhampa, ma, niz, O, am aski namis.
O rattati, Ní wa, titled, nieja, bu, tang specifying a y safety,
tang, wa, yuli, damb, mov, tang, zamb, tang,
pa, n' inya, Dam, u divisions, ba, y�owment, that which tung, with
O, tikos, o, o, sos, amayo, yangimana, pachimemas et aluante napi, vilupana, napi,
wa tampari, santi, o, savane, vitina, yinah, yinah, yinah, yinah, yinah, yinah, yinah, yinah,
yinah, yinah, yinah, yudah, to yah, wa mahalika, yah, ni, chattapana, yisati, kayidamatapana,
yisati wa yadamatapana, yisati, vilbari, namatapana, yisati, yinbani masamatatakasakayasatan
hubandin masa, ahaim tiwamamantiwas, nitiwas, aha kwas anote with a hoodie, tanjia,
o sobi kunbari, a kosanti, baribasantiro, santi, vihi, santi, santi, santi,
santi, vihi, vihi, jah, namatapana, kowmi, aya, so tasampasakaya, dukavidana, santi,
tasavidana, santi, gopati, chattapati, jakin, baribasantatapati, chasapipas,
We are going to go and eat Jeddopasi, with an ani Jeddipasati Sanja, ani Jeddipasati Sanja,
Ani Jeddipasati Sanja, Ani Jeddipasati Sanja, Ani Jeddipasati Sanja, Ani Jeddipasati,
Ani Jeddipasati Sanja, Ani Jeddipasati,
Ani Jeddipasati Sanja, Ani Mutjati.
Dangi, I'm a nappy, he's a mudah, jah, dangi.
Pani sambas, a nappy, leer, do sambas, a nappy, dangi, sambas, a nappy.
Pani sambas, a nappy.
So, I want pajana, titatabu, dobu, a yangayu.
Dangi sambas, a player, dangi, dangi, pani sambas, pika manadi.
They do sambas, pika manui.
They learn the sambas, pika manadi, sambas, pika manati.
Samba sambika manati, utan
Blend up, bangu,shine andasaratoo
Yewungs ar kangmanu saratoo
Uteical kus aligni si t't an pasta and great
So d'in a sang Hi Jati sound week
haort Chateel
Alab, bowattam een, mortam een
seven o'clock, Jan
Gla v beddah Guatesu
Yes Same
nay, t water belt
And we'reuskaratoo
Z mania
I don't blame the service, but I don't blame myself.
Niyu Atawa, just so we know about us, about to borrow tea.
And Jatiko Jaiwahu, so mano, hapahimi noo tea.
Ma'irajadama, a patanga, janti.
No Jatadu, so mano, haru, ho tea.
Niyu Atawa, just so we know about us, about to borrow.
Niyu Atawa, Jatiko Jaiwahu, hapahimi noo tea.
Ma'irajadama, a patanga, janti, josam, so mano, haru, ho tea.
Niyu Atawa, just so we know about us, about to borrow.
Niyu Atawa, janti, janti, janti.
Niyu Atawa, bouda, so we know about us, about to borrow tea.
Niyu Atawa, bouda, sasan, janti, sasan, khani, sangang, janti.
So now we get on to what is mostly actually just more of the same.
I'm going to learn a little bit about the other two elements and then there's a little
bit at the end. It's interesting dealing with the four noble truths. They get the rest
of the note. So start us out. What friends is the fire elements? The fire elements
may be either internal or external. What is the internal fire elements? Whatever
internally belonging to us up is fire, fiery and clung to. That is that by which one is
warmed, ages and is consumed and that by which what is eaten, drunk, consumed and
tasted gets completely digested or whatever else internally belonging to oneself is
fire, fiery and clung to. This is called the internal fire elements. Now both the internal
fire elements and the external fire elements are simply fire elements. And that should
be seen as it actually is with proper wisdom thus. This is not mine. This I am not. This is
not myself. When one sees it thus as it actually is with proper wisdom, one becomes disenchanted
with the fire elements and makes the mind dispassionate toward the fire elements. So again
all he's doing is breaking up the practice into its component parts. So you can become
dispassionate with anything. You can become dispassionate with all things based on watching
one thing after another. It could be the earth element, the air element, the water element,
the air element. So he's just giving examples. Mainly here in inside meditation, you'll
be dealing with warmth or cold. I'm dealing with the feeling of warmth or warmth or
cold. Now of course in broader scheme of things, it's dealing with all of the food, the
digestive system, but that's really it's still the heat that comes from digestion. I mean
you digest food, your body waters up into it. And it's external as well, the heat and the
cold from the room around you and you feel it. The acknowledgement of hot, hot, or cold,
might be so clearly seeing it as it is. That's seeing it as not me, not mine, not myself.
Now there comes the time when the external fire element is disturbed. It burns the villages,
towns, cities, districts, and countries. It goes out to lack of fuel only when it comes
to green grass, or to a road, or to a rock, or to water, or to a fair open space. There comes
the time when they seek to make a fire even with a cocks feather or a hide pairing. Even
with the cocks feather or a hide pairing. When even this external fire elements, greatest
it is, the scene to be impermanence, subject to destruction, disappearance, and change. One
of this body, which is clung to by craving in last but a while, there can be no considering
that as I, or mine, or I am. I think in the same as the other two who live in with the
fire element. Given that all the heat in the world in the universe is set to be destroyed
in fact they talk about this heat death eventually, where the whole universe becomes completely
and for all intents and purposes, totally cooled with no energy whatsoever. And when
even that is possible, why should we think of this little bit of heat as being somehow
meaningful? Really, there is a broader question here. Is that a meaning? Given how change
occurs throughout the universe, the entire universe is subject to change and given that
the breadth and the scope of the universe, how pitiful it is that we cling to the small
part of it that is being somehow of some import. Just we cling to a rock, right? We cling
to the earth as something important, we think to ourselves as a little scrap of organic
matter on a step to a rock somewhere in the far reaches of the universe.
So then, if others abuse, revile, scold, and harass a vacuum who has seen this element
as it actually is, he understands us. At that point, too, friends, much has been done
by a vacuum. We understand that this is, let's go to remind ourselves, right? The whole
thing of the saw. Reminds himself, understands us, and it is that the fire is dependent,
not independent, dependent on contact. So the feelings that we have all of the experiences
of heat that are as well dependent. And becoming, letting go of the idea that it's somehow
an entity that somehow part of self, that there is a self, that we have this soul that
the body is ours or so on. We, the mind settles down, becomes uninterested or unaffected
by the changes that assist the tombs of the world around. It says that the world around
is just made up of impermanent things, and so on. And it's neither, it's moved neither
by speech or by actions that are, that are disturbing, even if they, even if they kind of
up with the saw. And so this person strives for equanimity as result. Okay, and then we have
the air element. What friends is the air elements, the air elements may be either internal
or external. What is the internal air elements? Whatever internally, long to oneself, is
air, airy, and clung to. That is, up going winds, down going winds, winds in the belly,
winds in the bowels, winds that coarsely the limbs, in breath and out breath, or whatever else
internally, long to oneself, is air, airy, and clung to. This is called the internal air
element. Now both, both the internal air elements and the external air elements are simply
air elements. And that should be seen as it actually is with proper wisdom thus. This
is not mine, this I am not, this is not myself. When one sees it thus as it actually is with
proper wisdom, one becomes disenchanted with the air element and makes the mind dispassionate
toward the air elements. Okay, one thing that I'd like to point out here is this phrase here,
wind in the belly, which is sort of an answer to the question of where the accusation
that the Buddha never taught this, watching the stomach, winds in the belly, the pressure
in the stomach when you breathe is very much the air element. It's very much taught by the
Buddha, it's a part of this. The point is that when you're looking for something, some physical
experience to note when you're sitting still, this is one of the best ones, the winds in the
belly. And it's not just in our tradition there, when I was doing karate, when I was younger,
we also taught us to watch your abdomen tonight, to feel the deep breath going deeper,
because shallow breath is in the lungs, but a deep breath will be felt in the abdomen.
So when we're doing this, actually breath control techniques, it deals together very, very deep
in the abdomen, very, very ancient technique. There's nothing newer or strange about it.
And also that the in-breath in the out-breath themselves are considered the air element. So it's
really talking about the same thing. And the problem with using the in-breath in the out-breath
is they're conceptual. The idea of something going into the body and something going out of the
body isn't actually experienced, what's experienced is the sensation. So for insight meditation,
you have to use the sensation, whether it be at the nose or it be at the stomach, it doesn't really
matter, at the nose it would be more a little bit, we know them in but also the heat element or the
fire element. Again, this practicing in this way allows us to see oneself. What is about the
air element disturbed? The external element. Now there comes a time when the external air element
is disturbed. It suites away villages, towns, cities, districts, and countries. It comes a time in
the last month of the hot season when they seek wind by means of a fan or bellows and even the
strands of straw and the dip fringe of the fetch. And even the strands of the straw and the dip
fringe of the fetch to not stir. Even this external air element creates great as it is,
it seemed to be impermanence, subject to destruction, disappearance, and change.
What is this body which is clung to by craving in the last bit of while? There could be no
considering that as I or mine or I am. That's another idea that on those in
Fridays that wind which seems so real and you get this concept in your mind of the wind
blowing, where did it go? It suddenly disappeared. Getting used to this idea that things change,
that things don't have any lasting existence. It actually does a number on you and you experience
this, even though intellectually it's child's play. We know these things, of course, the wind comes
in and goes. But when you watch it and you realize, especially more so internally, when you say
things arise and then chasing internally, it really changes the way you look at the world. Start to
focus on this idea or you remember it when things come up, when things that you would normally
cling to or things that you would normally be averse to. So then if others abuse,
revile, scold, and harass beku, who has seen this element as it actually is, he understands thus.
At that point too, friends, much has been done by that beku.
So 26. Friends, just as one of spaces enclosed by timber and creepers,
grass and clay, it comes to be termed just house. So too, when a space is enclosed by bones
and sinus, flesh and skin, it comes to be termed just material form.
Meaning there's no essence to the material form, this idea of our body or so on.
And so this is a meditation technique. It's not really theoretical because we all know these
things theoretically, but it's a reminder to look at, to change the way we look at things.
So these monks are sitting around meditating us, but it says this, and they're able to
see that this material form is just like a house all called them together.
If friends internally, the eye is intact, but no external forms come into its range,
and there is no corresponding conscious engagement, then there is no manifestation of the
corresponding section of consciousness. If internally, the eye is intact,
an external form is coming to its range, but there is no corresponding conscious engagement,
then there is no manifestation of the corresponding section of consciousness.
But when internally, the eye is intact, an external form is coming to its range,
and there is the corresponding conscious engagement, then there is the manifestation of the
of the corresponding section of consciousness.
So engagement refers to the contact.
Let's see what you have here.
I'll not go into it.
Again, just trying to show that it's all conditionally formed,
that seeing isn't some, it isn't I who sees.
It's a phenomenon that arises based on causes and conditions.
The consciousness is required in the eye or the external forms
that they're all required here.
It's just a jogging people's minds
and helping them to come to this realization
of the impermanence of all things.
The material form in what has thus
come to be included in the material form aggregate affected
by clay.
The perception in what has thus come to be is included
and what has now come to be played in the material form.
I agree with the fact that by clay.
The feeling in what has thus come
to be is included in the feeling aggregate affected by clay.
The perception in what has has this come
to be is included in the perception
in the formations aggregate affected by clinging. Consciousness and what has thus come to be
is included in the Consciousness aggregate affected by clinging. He understands thus. This indeed
is how there comes to be the inclusion, gathering, and amassing of things into these five
aggregates affected by clinging. Now this has been said by the Blessed One, one who sees
dependent origination sees the Dhamma, one who sees the Dhamma sees dependent origination, and
these five aggregates affected by clinging are dependently arisen. The desire indulgence,
inclination, and holding based on these five aggregates affected by clinging is the origin of suffering.
The removal of desire and lust, abandonment of desire and lust for these five aggregates affected
by clinging is a cessation of suffering. At that point to friends, much has been done by that
PQ. Okay, so the deal is when you see something, all of that we've dealt with is
dependently arisen and in turn the five aggregates arrives. The material form,
the material form that arises there is part of the five aggregates. The material form, the
feeling, the perceptions, the formation, the formations, and the consciousness, all of those things
arise based on the scene. So when you see there's the material form, there's the feeling,
like the happier the calm, someone there's the perception, the recognition of it,
there's the thoughts about it, what we think about it, and there's the awareness of it.
All of these things arise based on the experience of seeing. So it's all dependently
arisen. So, and he points out that this is exactly what the blessed one would have us see.
One who sees dependent originations sees the damage, this is what it's important for us to see
that everything arises. Watching things arise based on the cause. Watching seeing arrives
where it comes from and seeing that it arises and sees it. Understanding it. When you understand it,
that is the truth of suffering, and that removes the cause, it removes any clinging,
the desire and inclination holding. All of that disappears once you see suffering clearly,
you see that truth clearly, and the room and that removal is the cessation. Now it's interesting,
it doesn't talk about the fourth and all the truths. So only three of the four and all the truths
are explicitly shown in the text, the fourth truth is implied. According to M.A. the commentary,
it is the penetration of these truths by the development of the factors of the path.
Of course. And then he says the same thing about the ear, the nose, the tongue, the body,
and the mind, and this is all the same. Wait, is it? Yes, the last background. That is what the
venerable sorry put just said. The beakers are satisfied and delighted in a venerable sorry put his words.
So is that clear? We have the, what is the six senses? And each of the six senses, the five
aggregates coming and being is basically what he's saying. And all of it is independently arisen
by watching that by practicing meditation based on the six senses and the five aggregate,
which is really what we do in sati pataana meditation. All of clinging, the clinging is
abandoned. So there you have the Maha Maha ati padopas. That's our broadcast for tonight. Thank you for tuning in.
Have a good day.
