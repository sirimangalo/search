1
00:00:00,000 --> 00:00:10,000
Okay welcome everyone continuing our study of the VCD manga Robin you want to start us off?

2
00:00:10,000 --> 00:00:19,120
Sure we're on page 216 number 94. This is chapter 7. Did anyone need the link or everyone's

3
00:00:19,120 --> 00:00:47,120
also like that? Okay one moment.

4
00:00:47,120 --> 00:00:55,120
Okay and David would you be able to begin with 94? Okay how's my sound? Very good thanks.

5
00:00:55,120 --> 00:01:08,120
Okay fit for gifts. As to fit for gifts etc what should be brought and given is a gift.

6
00:01:08,120 --> 00:01:15,120
The meaning is what is to be brought even from far away and donated to the virtuous.

7
00:01:15,120 --> 00:01:24,120
It is a term for the full requisites. The community is fit to receive that gift because it makes it bear great fruit.

8
00:01:24,120 --> 00:01:32,120
Thus it is fit for gifts.

9
00:01:32,120 --> 00:01:41,120
Or alternatively all kinds of property even when the bringer comes from far away can be given here.

10
00:01:41,120 --> 00:01:50,120
Thus the community can be given to. Or it is fit to be given to by Saka and others.

11
00:01:50,120 --> 00:01:59,120
Thus it can be given to. And the brahman's fire is called to be given or sacrificed to.

12
00:01:59,120 --> 00:02:04,120
For they believe that what is sacrificed to it brings great fruit.

13
00:02:04,120 --> 00:02:11,120
But if something is to be sacrificed for the sake of the great fruit brought by what is sacrificed to it.

14
00:02:11,120 --> 00:02:15,120
Then surely the community should be sacrificed to.

15
00:02:15,120 --> 00:02:19,120
For what is sacrificed or given to the community has great fruit.

16
00:02:19,120 --> 00:02:22,120
According as it is said.

17
00:02:22,120 --> 00:02:27,120
For anyone to serve the fire out in the woods a hundred years.

18
00:02:27,120 --> 00:02:37,120
For pay one moment's homage to men of self development. His homage would by far excel is a hundred years of sacrifice.

19
00:02:37,120 --> 00:02:39,120
And the words.

20
00:02:39,120 --> 00:02:45,120
Aha, Veniya, to be sacrificed to which is used in the schools.

21
00:02:45,120 --> 00:02:48,120
It is the same meaning as the word.

22
00:02:48,120 --> 00:02:52,120
Who in Aya fit for gifts used here.

23
00:02:52,120 --> 00:03:06,120
There is only the mere trifling differences of syllables. So it is fit for gifts.

24
00:03:06,120 --> 00:03:19,120
Thank you. Naga, can you read six ninety six.

25
00:03:19,120 --> 00:03:25,120
For hospitality is what a donation to visitors is covered.

26
00:03:25,120 --> 00:03:28,120
Prepared with all owners.

27
00:03:28,120 --> 00:03:34,120
With the sake of dear and beloved relatives and friends who will have come from all quarters.

28
00:03:34,120 --> 00:03:38,120
But even more than to such objects across the body.

29
00:03:38,120 --> 00:03:42,120
It is fitting that it should be given also to the community.

30
00:03:42,120 --> 00:03:50,120
For there is no object of hospitality to, to hospitality self fit to receive hospitality as a community.

31
00:03:50,120 --> 00:03:59,120
Since it is encountered after an interval between Vida and Moses is wholly endearing and vulnerable qualities.

32
00:03:59,120 --> 00:04:01,120
So it is fit for hospitality.

33
00:04:01,120 --> 00:04:06,120
Since the hospitality is fit to be given to it and it is fit to receive it.

34
00:04:06,120 --> 00:04:12,120
But those who take the text to them.

35
00:04:12,120 --> 00:04:17,120
Aha, Veniya, haven't you.

36
00:04:17,120 --> 00:04:21,120
But to be given hospitality.

37
00:04:21,120 --> 00:04:26,120
To profit that the community is worthy to be placed first.

38
00:04:26,120 --> 00:04:29,120
And so what is to be finished first of all.

39
00:04:29,120 --> 00:04:32,120
We got here and given.

40
00:04:32,120 --> 00:04:40,120
Sub Mohammed wa hetha.

41
00:04:40,120 --> 00:04:46,120
And for that reason it is fit to be given hospitality to.

42
00:04:46,120 --> 00:04:53,120
Not going now or since it is worth to be given to all aspect aspects of.

43
00:04:53,120 --> 00:05:05,120
It is thus fit to be given hospitality to the whole one here, and here this is called

44
00:05:05,120 --> 00:05:20,120
looking at the same sense.

45
00:05:20,120 --> 00:05:22,120
Fit for offering.

46
00:05:22,120 --> 00:05:29,120
Offering is what a gift is called that is to be given out of faith in the world to come.

47
00:05:29,120 --> 00:05:37,120
The community is worthy of that offering, or is helpful to that offering because it purifies it by making it

48
00:05:37,120 --> 00:05:39,120
a great fruit.

49
00:05:39,120 --> 00:05:42,120
Thus it is fit for offering.

50
00:05:42,120 --> 00:05:47,120
Fit for salutation.

51
00:05:47,120 --> 00:05:52,120
It is worthy of being accorded by the whole world.

52
00:05:52,120 --> 00:05:59,120
The red rental, salutation consisting in placing both hands.

53
00:05:59,120 --> 00:06:02,120
Almost the other above the head.

54
00:06:02,120 --> 00:06:11,120
Thus it is fit for reverential salutation.

55
00:06:11,120 --> 00:06:21,120
As an incomparable field of merit for the world, as a place without equal in the world for growing merit, just as the place for growing the kings or the ministers,

56
00:06:21,120 --> 00:06:26,120
rice or corn, is the king's rice field or the king's corn field.

57
00:06:26,120 --> 00:06:30,120
So the community is the place for growing the whole world's merit.

58
00:06:30,120 --> 00:06:36,120
For the world's various kinds of merit leading to welfare and happiness grow with the community as their support.

59
00:06:36,120 --> 00:06:44,120
Therefore, the community is an incomparable field of merit for the world.

60
00:06:44,120 --> 00:06:53,120
As long as he collects the special qualities of Sangha in this way, classed as having entered on the good way, etc.

61
00:06:53,120 --> 00:07:01,120
Then on that occasion his mind is not obsessed by greed or obsessed by hate or obsessed by delusion.

62
00:07:01,120 --> 00:07:09,120
His mind has rectitude on that occasion being inspired by the Sangha.

63
00:07:09,120 --> 00:07:20,120
So when he has suppressed the hindrances in the way already described the John factors rise in a single conscious moment,

64
00:07:20,120 --> 00:07:35,120
but owing to the profundity of communities, special qualities, whores, owing to his being occupied in collecting special qualities of many sorts,

65
00:07:35,120 --> 00:07:45,120
the John is only obsessed and does not reach absorption and that excess John itself is known as the collection of Sangha.

66
00:07:45,120 --> 00:07:58,120
Because it rises with the collection of the community's special qualities as the means.

67
00:07:58,120 --> 00:08:21,120
When a vehicle is devoted to this recollection of the community, he is respectful and differential towards the community, he attends forness of faith and so on, he has much happiness and bliss.

68
00:08:21,120 --> 00:08:46,120
He conquers fear and dread. He is able to endure pain. He comes to feel as if he were living in the community's presence and his body when the recollection of the Sangha's special qualities dwells in it.

69
00:08:46,120 --> 00:09:13,120
He becomes as worthy of veneration as an Upa-sata house where the community has met, his mind tends towards the attainment of the community's special qualities.

70
00:09:13,120 --> 00:09:41,120
When he encounters an opportunity for transgressions, he has awareness of cons, cons, consize and shame as vividly as if he were faced to face with the community.

71
00:09:41,120 --> 00:09:53,120
And if he penetrates no higher, he is at least headed for a happy destiny.

72
00:09:53,120 --> 00:10:13,120
Now, when a man is truly wise, he consent tests will surely be this recollection of the Sangha blessed with such mighty potency.

73
00:10:13,120 --> 00:10:28,120
This is the section dealing with the recollection of the community in the detailed explanation.

74
00:10:28,120 --> 00:10:45,120
The recollection of virtue. When he wants to develop the recollection of virtue should go into solitary retreat and recollect his own different kinds of virtue in their special qualities of being unturned, etc., as follows.

75
00:10:45,120 --> 00:10:58,120
Indeed, my various kinds of virtues are unturned, unwatched, unmodeled, liberating, praised by the wise, not adhered to and conducive to concentration.

76
00:10:58,120 --> 00:11:14,120
And the layman should recollect them in the form of layman's virtues, while one gone forth into homelessness should recollect them in the form of the virtue of those gone forth.

77
00:11:14,120 --> 00:11:21,120
I have a quick question about the parley pronunciation in the recollection of the Sangha.

78
00:11:21,120 --> 00:11:32,120
So, when I've heard it chanted by ties, the words sound more like ahunayo parunayo.

79
00:11:32,120 --> 00:11:43,120
So, that last E sounds more like an I, but my understanding of parley pronunciation is that it should be ahunayo parunayo.

80
00:11:43,120 --> 00:11:53,120
It's a good question. You want to know what it is? It's nuya, it's nuya, it's not an E or an I, it's nuya.

81
00:11:53,120 --> 00:12:12,120
And why that is? Because tie has its own special vowels, and one of the vowels in tie is the vowel ui, which you get when you combine the vowel for A and a Y, the consonant for Y, when you put the A and the Y together, the A sound and the Y sound together.

82
00:12:12,120 --> 00:12:23,120
You get nuya, which is just a quirk of the tie language, having many, many vowels that aren't in other languages.

83
00:12:23,120 --> 00:12:31,120
So, it's just a corruption of tie. I've heard people even try to explain it that that's how it really should be pronounced, don't pronounce it as nuya.

84
00:12:31,120 --> 00:12:41,120
It should be pronounced as nuya, which is not, not certainly not correct, because it's not nuya, it's nuya.

85
00:12:41,120 --> 00:12:46,120
Okay, thank you very much.

86
00:12:46,120 --> 00:13:01,120
Whether they are the virtues of layman, or of those gone forth, when no one of them is broken in the beginning or in the end, not being torn like a cloth ragged at the ends, then they are unt torn.

87
00:13:01,120 --> 00:13:10,120
When no one of them is broken in the middle, not being rent like a cloth that is punctured in the middle, then they are unrent.

88
00:13:10,120 --> 00:13:21,120
When they are not broken twice or thrice in succession, not being blotched like a cow whose body is some such colour as black or red, with

89
00:13:21,120 --> 00:13:29,120
discrepent coloured oblong or round patch appearing on her back or belly, then they are unbloched.

90
00:13:29,120 --> 00:13:42,120
When they are not broken or over at intervals, not being mottled like a cow speckled with discrepent coloured spots, then they are unmottled.

91
00:13:42,120 --> 00:14:02,120
Or in general, they are on term, on brands, unbloched, on mottled, when they are undamaged by the seven bones of sexuality, and by anger and enmity and the other evil things.

92
00:14:02,120 --> 00:14:16,120
The same virtues are liberating since they liberate by praying from the slavery of craving. They are praised by the wise because they are praised by such wise men as enlightened ones.

93
00:14:16,120 --> 00:14:31,120
They are not adhered to, since they are not adhered to with craving and false view, or because of the impossibility of misapprehending that there is this flaw in your virtues.

94
00:14:31,120 --> 00:14:50,120
They are conducive to concentration, since they can do to access concentration and absorption concentration, or to the path concentration or fruition concentration.

95
00:14:50,120 --> 00:15:08,120
As long as you collect these own virtues in their special qualities of being undrawn, etc. In this way, then, on this occasion, on that occasion, his mind is not obsessed by green or obsessed by hate or obsessed by dilution.

96
00:15:08,120 --> 00:15:14,120
His mind has rectitude on that occasion being inspired by virtue.

97
00:15:14,120 --> 00:15:41,120
So, when he was surprised with hindrances, in the way of already described, the Jana, Dr. Sarai's in single conscious mood, but owing to the quantity of the virtues, special qualities, are only owned to his being, occupied in the collecting special qualities of many sorts.

98
00:15:41,120 --> 00:16:06,120
The Jana is only accessed in this knowledge of absorption, in that access Jana itself is known as the collection of virtue, to, because it arises with the virtues, special qualities, and some needs.

99
00:16:06,120 --> 00:16:19,120
And one of the cues devoted to this recollection of virtue is respect for the training. He lives in communion with his fellow, with his fellows in the life of purity.

100
00:16:19,120 --> 00:16:38,120
He is said to us and welcoming. He is devoid of the fear of self-approachance and so on. He sees far in the slightest fault. He attains fullness of faith and so on. He is much happiness and gladness.

101
00:16:38,120 --> 00:16:45,120
And if he demonstrates no higher, he is at least headed for a happy destiny.

102
00:16:45,120 --> 00:16:56,120
Now, when a man is truly wise, his constant task will surely be his recollection of his virtue, plus with such mighty potency.

103
00:16:56,120 --> 00:17:07,120
This is the section dealing with recollection of virtue in a detailed explanation.

104
00:17:07,120 --> 00:17:18,120
One who wants to develop the recollection of generosity should be naturally devoted to generosity and the constant practice of giving and sharing.

105
00:17:18,120 --> 00:17:28,120
Or alternatively, if he is one who is starting the development of it, he should make the resolution from now on when there is anyone present to receive it.

106
00:17:28,120 --> 00:17:40,120
I shall not eat even a single mouthful without having given a gift. And that very day he should give a gift by sharing according to his means and his ability, with those who have distinguished qualities.

107
00:17:40,120 --> 00:17:53,120
When he has apprehended the sign in that, he should go into solitary retreat and recollect his own generosity in its special qualities of being free from the stain of avarice, etc., as follows.

108
00:17:53,120 --> 00:18:00,120
It is gained from me. It is great gain from me that in a generation obsessed by the step, stain of avarice.

109
00:18:00,120 --> 00:18:08,120
I abide with my heart free from stain by avarice and am freely generous and open-handed. Did I delight in relinquishing?

110
00:18:08,120 --> 00:18:14,120
Expect to be asked and rejoice in giving and sharing.

111
00:18:14,120 --> 00:18:30,120
I have a question. If everybody is not living in the pedestrian and the area, I have to work in the real world in there so much hatred.

112
00:18:30,120 --> 00:18:48,120
So, when you have to practice, it is just compression and compression you have to practice towards the hatred. Sometimes I find it difficult. And I feel like quitting more.

113
00:18:48,120 --> 00:19:13,120
Or quitting work. Or quitting practicing.

114
00:19:13,120 --> 00:19:20,120
Hello?

115
00:19:20,120 --> 00:19:25,120
Could you maybe type that in? Where are you?

116
00:19:25,120 --> 00:19:31,120
Oh, good.

117
00:19:31,120 --> 00:19:38,120
She deafened herself. I think she didn't hear me. I asked if you meant you want to quit work or quit practicing.

118
00:19:38,120 --> 00:19:45,120
I didn't hear you, so I doubt you did.

119
00:19:45,120 --> 00:19:53,120
Oh, that's okay. Pantay was asking me if your question was about quitting work or quitting work.

120
00:19:53,120 --> 00:20:00,120
I can not quit work because I need to work. So, I keep practicing.

121
00:20:00,120 --> 00:20:15,120
If you go there to be with me or go out to me in reaching these people with compression, not say anything negative to them, but listen what they are saying to me and keep practicing.

122
00:20:15,120 --> 00:20:31,120
But at the end of the day, I find myself in peace that it all is almost. I feel maybe it is for me to practice.

123
00:20:31,120 --> 00:20:56,120
But sometimes I find it difficult to practice.

124
00:20:56,120 --> 00:21:06,120
Please mute yourself when you're finished talking.

125
00:21:06,120 --> 00:21:10,120
You really should set up, push the dog.

126
00:21:10,120 --> 00:21:20,120
Well, my guess my question is what you mean in regards to practicing because it's not correct to suggest that the Buddha was with you.

127
00:21:20,120 --> 00:21:31,120
Certainly not correct to suggest that God is with you. Those aren't Buddhist practices or Buddhist premises on much to practice.

128
00:21:31,120 --> 00:21:37,120
The Buddha said you have to practice for yourself.

129
00:21:37,120 --> 00:21:48,120
So, my question is I guess what exactly are you practicing when you somehow expect God or Buddha to be with you?

130
00:21:48,120 --> 00:21:56,120
I don't want to take Buddha with me because now I'm at a place that I want to be on my own.

131
00:21:56,120 --> 00:22:03,120
So, I keep trying to not to bring them.

132
00:22:03,120 --> 00:22:24,120
It's like I feel alone. I'm doing that, but at the same time I also try not to bring Buddha or Jesus to me. I feel like in a way I recognize that I'm on my own. That's that's where I feel right now.

133
00:22:24,120 --> 00:22:35,120
That's the way things are. We're born alone. We die alone. That's nature. When practice is not difficult, but something being difficult doesn't mean it's not worth doing.

134
00:22:35,120 --> 00:22:58,120
There's no direct connection between something being difficult and something being worth doing. In fact, for most of us, that which is worth doing is often the most difficult.

135
00:22:58,120 --> 00:23:12,120
So, good luck with you. I hope it all works out, but you have to be patient and not expect too much.

136
00:23:12,120 --> 00:23:31,120
Expectations will ruin things.

137
00:23:31,120 --> 00:23:47,120
Hearing this gain for me, it is my gain advantage. The intention is I surely partake of those kinds of gain for a giver that have been commended by the Blessed One as follows.

138
00:23:47,120 --> 00:24:09,120
One who gives life by giving food, shall have life by the divine or human, and a giver is loud and frequent by many. And one who gives is ever loud according to the wise man's law and so on.

139
00:24:09,120 --> 00:24:35,120
It is great again for me. It is grand again for me that this dispensation or the human state has been gained by me. Why? Because of the fact that I abide with my my free from standby.

140
00:24:35,120 --> 00:24:59,120
Ever rest and rejoice the rejoice by rejoice in giving and sharing.

141
00:24:59,120 --> 00:25:05,120
Sorry, which one are we at? 110 on page 220.

142
00:25:05,120 --> 00:25:34,120
Hearing obsessed by the state of avarice is overwhelmed by the state of avarice. Generation means beings so called owing to the fact that they are being generated. So the meaning here is this, among beings who are overwhelmed by the state of avarice, which is one of the dark states that corrupt the natural transparency of consciousness and which has the characteristic of inability to bear sharing one's own good fortune with others.

143
00:25:34,120 --> 00:25:48,120
Free from staying by avarice because of being both free from avarice and from the other stains, greed, hate and the rest.

144
00:25:48,120 --> 00:26:12,120
I abide with my heart. I abide with my consciousness of the kind already stated is the meaning. But in the suta, I lived the home life with my heart free, etc. is said because it was taught there as a mental abiding to depend on constantly to Mahanama the sakyan.

145
00:26:12,120 --> 00:26:24,120
Who was a stream emperor asking about and abiding to depend on. There the meaning is I live overcoming.

146
00:26:24,120 --> 00:26:42,120
Really generous, liberally generous, open-handed, with hands that are purified, where is meant is with hands that are always watched in order to give gifts carefully with ones on hands.

147
00:26:42,120 --> 00:27:03,120
That I delight in ruling pushing. The act of ruling pushing is ruling pushing. The meaning is giving up. To the light in reliquing, relinquing is to delight in constant devotion to that relinquing.

148
00:27:03,120 --> 00:27:14,120
Expect to be asked accustomed to be asked because of giving whatever other acts for is the meaning.

149
00:27:14,120 --> 00:27:35,120
It is a reading. In which case, it is devoted to sacrifice. In other words, to sacrifice in and rejoice in sharing. The meaning is, he recollects through those.

150
00:27:35,120 --> 00:27:48,120
I give gifts and I share out what is to be used by myself and I rejoice in both.

151
00:27:48,120 --> 00:28:09,120
As long as he recollects his own generosity and its special qualities of freedom from stained by avarice, etc. In this way, then on that occasion, his mind is not obsessed by grief or obsessed by hate or obsessed by delusion.

152
00:28:09,120 --> 00:28:25,120
His mind has rectitude on that occasion being inspired by generosity. So, when he has supposed to be hindrances in the way already described, the John effectors arise in a single conscious moment.

153
00:28:25,120 --> 00:28:43,120
But owing to the profundity of the generosity's special qualities, or owing to his being occupied in recollecting the generosity's special qualities of many sorts, the John is only access and does not reach absorption.

154
00:28:43,120 --> 00:29:04,120
And that access John is known as recollection of generosity too, because it arises with the generosity's special qualities as the means.

155
00:29:04,120 --> 00:29:25,120
And then, as he goes to his repulsion of generosity, he becomes ever more intent on generosity, his preferences for non-grade, he acts in conformity with having kindness, his fearless, he has much happiness and sadness.

156
00:29:25,120 --> 00:29:43,120
And he, and he can penetrate no higher, he's at least headed for a happy destiny. Now, when a man is truly wise, his constant task force should be, his recollection of his giving, best to be such mighty potency.

157
00:29:43,120 --> 00:29:57,120
This is the section dealing with the recollection of generosity in the detailed explanation.

158
00:29:57,120 --> 00:30:13,120
All of these potential valid meditation would be to pick one of these or go through each one of the factors that are explained here.

159
00:30:13,120 --> 00:30:30,120
Starting with lava, what the may, lava, what the may, it is indeed a gain for me. And it's a great gain for me. And each of the, each of the qualities.

160
00:30:30,120 --> 00:30:46,120
When we got the Malamat Chai Reena, Chaitasa, we Harami, I don't know. I'm with the Jago, right? Some of these qualities.

161
00:30:46,120 --> 00:31:07,120
It was similar to our quote of the day, the other day too, about giving with one's own hands. The five types, the five qualities of a soup, sappurisa dalen, a gift from a good person.

162
00:31:07,120 --> 00:31:20,120
There's quite a bit about giving charities and an important thing. I mean, it's partly important to mention it because, because of how important it is to keep the religion going.

163
00:31:20,120 --> 00:31:36,120
But it certainly is a great spiritual practice. I mean, for opening the heart for cultivating. I mean, because Buddhism is all about letting go, right? Charity falls hand in hand with that. And if one is not charitable, it's a good sign that they're not capable of letting go.

164
00:31:36,120 --> 00:31:46,120
Quinn, can you read 115?

165
00:31:46,120 --> 00:31:55,120
Yeah, sure. One who wants to develop the recollection of deities should possess the special qualities of faith, etc.

166
00:31:55,120 --> 00:32:08,120
That are evoked by means of the noble path, and he should go into solitary retreat and recollect his own special qualities of faith, etc. with deities standing as witness as follows.

167
00:32:08,120 --> 00:32:28,120
There are deities of the realm of the four kings. There are deities of the realm of the 33. There are deities who are gone to die in bliss, who are contented, who delight in creating, who will power over other creations.

168
00:32:28,120 --> 00:32:45,120
There are deities of the realm of the spirit to knew. There are deities higher than that. And those deities were possessed of faith, such that, on dying here, they were reborn there, and such faith is present in me too.

169
00:32:45,120 --> 00:33:01,120
In those deities were possessed of virtue of learning, of generosity, of understanding, such that when they died here, they were reborn there, and such understanding is present in me too.

170
00:33:01,120 --> 00:33:17,120
In the Suta, however, it is said, on the occasion, Mahanama, on which a noble disciple recollects the faith, the virtue, the learning, the generosity, and the understanding that are both his own, and of those deities.

171
00:33:17,120 --> 00:33:36,120
On that occasion, his mind is not obsessed by greed, although this is said, it should nevertheless be understood as said for the purpose of showing that the special qualities of faith, etc., in oneself are those in the deities, making the deities stand as witness.

172
00:33:36,120 --> 00:33:51,120
For it is said, definitely, in the commentary, he recollects his own special qualities, making the deities stand as witnesses.

173
00:33:51,120 --> 00:34:11,120
As long as in the prior stage, he recollects the deities, special qualities of faith, etc., and in the later stage, he recollects the special qualities of faith, etc., existing in himself, then, on that occasion, his mind is not obsessed by greed,

174
00:34:11,120 --> 00:34:21,120
or obsessed by hate, or obsessed by delusion, his mind is rectitude on that occasion, being inspired by deities.

175
00:34:21,120 --> 00:34:48,120
So, when he has suppressed the hindrances in the way, he already stated, the janitors rise in a single conscious moment, but owing to the profound deity of the special qualities of faith, etc., or owing to his being occupied in recollecting special qualities of many thoughts, the janitors only access and does not reach absorption.

176
00:34:48,120 --> 00:35:02,120
And that access janitor itself is known as recollection of deities too, because it arises with the deities, special qualities, as the means.

177
00:35:02,120 --> 00:35:26,120
But which deities are these that we are recollecting?

178
00:35:26,120 --> 00:35:33,120
Well, although it does include brahma realm, right? So, it is angels in God. I mean, I always translate

179
00:35:33,120 --> 00:35:45,120
data as angels, because it is separate from brahmas, but this includes both means it is not deities, it is higher beings, divine beings, you can say.

180
00:35:45,120 --> 00:36:00,120
So, no one in particular, just the people are for the deities, for particular realm.

181
00:36:00,120 --> 00:36:16,120
And the morality, maybe, that is what you recollect from the name.

182
00:36:16,120 --> 00:36:37,120
Thank you.

183
00:36:37,120 --> 00:36:47,120
And what was the question?

184
00:36:47,120 --> 00:37:16,120
Yeah, I was just confused as to whether these were, you know, particular individual deities or angels or bromos or just kind of in general.

185
00:37:16,120 --> 00:37:20,120
For human, above human.

186
00:37:20,120 --> 00:37:32,120
Thank you, Bhante.

187
00:37:32,120 --> 00:37:54,120
And when a pickle is devoted to this recollection of deities, he becomes dearly loved by deities. He obtains even greater forness of faith. He has much happiness and gladness.

188
00:37:54,120 --> 00:38:03,120
And if he penetrates no higher, he is at least headed for a happy destiny.

189
00:38:03,120 --> 00:38:21,120
Now, when a man is truly wise, his constant text will surely be this recollection of deities, blessed with such mighty potency.

190
00:38:21,120 --> 00:38:48,120
This is the section dealing with the recollection of deities in the detailed explanation.

191
00:38:48,120 --> 00:38:52,120
Bhante, can you read one 19?

192
00:38:52,120 --> 00:38:54,120
Yes, sorry.

193
00:38:54,120 --> 00:39:03,120
Now, in setting forth the detail of these recollections, after the words, his mind has rectitude on the education being inspired by the perfect one.

194
00:39:03,120 --> 00:39:15,120
It has added, when a noble disciples mind has rectitude Mahanama, the meaning inspires him, the law inspires him, and the application of the law makes him glad.

195
00:39:15,120 --> 00:39:18,120
When he is glad, happiness is born in him.

196
00:39:18,120 --> 00:39:26,120
Here in the meaning inspires him should be understood as said of contentment inspired by the meaning beginning.

197
00:39:26,120 --> 00:39:30,120
This blessed one is such, since he is, etc.

198
00:39:30,120 --> 00:39:36,120
The law inspires him, is said to contentment, said of contentment inspired by the text.

199
00:39:36,120 --> 00:39:44,120
The application of the law makes him glad, is said of both.

200
00:39:44,120 --> 00:39:51,120
And when in the case of the recollection of deities inspired by deities is said,

201
00:39:51,120 --> 00:39:59,120
this should be understood as said either of the consciousness that occurs in the price stage inspired by deities,

202
00:39:59,120 --> 00:40:07,120
or of the consciousness that occurs in the later stage inspired by the special qualities that are similar to those of the deities,

203
00:40:07,120 --> 00:40:15,120
and are productive of the deity's state.

204
00:40:15,120 --> 00:40:20,120
This six recollections succeed only in noble disciples.

205
00:40:20,120 --> 00:40:28,120
Disciples, for the special qualities of the enlightened one, the law, and the community are evident to them.

206
00:40:28,120 --> 00:40:36,120
And they possess the beauty with the special qualities of intoneness, etc.

207
00:40:36,120 --> 00:40:44,120
The generosity that is free from a stain by avarice, and the special qualities of faith, etc.

208
00:40:44,120 --> 00:40:49,120
Similar to those of deities.

209
00:40:49,120 --> 00:41:14,120
And in the Mahamnamasuta, they are expounded in detail by the blessed one, in order to show a stream winner and abiding to depend upon when he asked for one.

210
00:41:14,120 --> 00:41:35,120
Also, in the Devasuta, they are expounded in order that a noble disciple should purify his consciousness by means of the political action and so attained further clarification in the ultimate sense that's there because a noble disciple recollect the perfect name in this way.

211
00:41:35,120 --> 00:41:43,120
The blessed one is such things he's accomplished, his mind has rectitude on that occasion.

212
00:41:43,120 --> 00:41:53,120
He has announced, God free from emerge from keepredity, keepredity, because it's a turn for the five chords of sense, desire.

213
00:41:53,120 --> 00:41:57,120
Some beings gain purity here by making this recollection.

214
00:41:57,120 --> 00:42:24,120
They are expounded as the realization of the wide open through the susceptibility of purification that exists in the ultimate sense only in a noble disciple thus.

215
00:42:24,120 --> 00:42:52,120
It is wonderful, friends, it is marvelous how the realization of the wide open in the crowded house life has been discovered by the blessed one who knows and sees accomplished and fully enlightened for the purification of beings, for the surmounting of sorrow and lamentation, for the ending of pain and grief, for the attainment of the true way, for the realization of Nibbana, that is to say the six stations of recollection,

216
00:42:52,120 --> 00:42:59,120
which six, what six, your friends are noble disciple recollect the perfect one, etc.

217
00:42:59,120 --> 00:43:08,120
Some beings are susceptible to purification in this way.

218
00:43:08,120 --> 00:43:27,120
Also in the post that they are expounded in order to show the greatness of the fruit of the impositor, as the mind purifying meditation subject for a noble disciple, who is subsaved in the impositor and what is number one's opposite of Osaka.

219
00:43:27,120 --> 00:43:38,120
It is the gradual cleansing of the mind still solid by imperfections and what is the gradual cleansing of the mind still solid by imperfections.

220
00:43:38,120 --> 00:43:48,120
Here, Osaka, a noble disciple recollect the perfect one, etc.

221
00:43:48,120 --> 00:44:06,120
In the book of 11, when a noble disciple has asked, when a vulnerable sur in what way should we abide, who abide in various ways.

222
00:44:06,120 --> 00:44:19,120
They are expounded to him in order to show the way of abiding in this way, one who has faith is successful.

223
00:44:19,120 --> 00:44:43,120
One who has no faith, one who is energetic, one whose mindfulness is established, one who is concentrated, one who has understanding is successful.

224
00:44:43,120 --> 00:44:56,120
My Hanama, not one who has no understanding, having established yourself in these five things.

225
00:44:56,120 --> 00:45:02,120
My Hanama, you should develop six things here.

226
00:45:02,120 --> 00:45:16,120
My Hanama, you should recollect the perfect one, that the last one is such a thing since.

227
00:45:16,120 --> 00:45:27,120
Still, though this is so, they can be brought to mind by an ordinary man too, if he possesses the special qualities of purified virtue and the rest.

228
00:45:27,120 --> 00:45:33,120
For when he is recollecting the special qualities of the Buddha, etc., even only according to hearsay.

229
00:45:33,120 --> 00:45:38,120
His consciousness settles down by virtue of which the hindrances are suppressed.

230
00:45:38,120 --> 00:45:46,120
In his supreme gladness, he initiates insight, and he even attains to our hunch.

231
00:45:46,120 --> 00:46:01,120
That venerable one, it seems, saw a figure of the enlightened one, created by Mara.

232
00:46:01,120 --> 00:46:07,120
He thought, how could this appear despite its having greed, hate and delusion?

233
00:46:07,120 --> 00:46:11,120
What can the blessed ones goodness have been like?

234
00:46:11,120 --> 00:46:16,120
That he was quite without greed, hate and delusion?

235
00:46:16,120 --> 00:46:25,120
He acquired happiness with the blessed one as object, and by augmenting his insight he reached our hunchhip.

236
00:46:25,120 --> 00:46:33,120
The seventh chapter called the description of six recollections in the treaties on the development of concentration in the path of purification,

237
00:46:33,120 --> 00:46:43,120
composed for the purpose of gladening good people.

238
00:46:43,120 --> 00:47:07,120
That's all for that then, take a break.

239
00:47:07,120 --> 00:47:16,120
We'll stop recording there anyway.

