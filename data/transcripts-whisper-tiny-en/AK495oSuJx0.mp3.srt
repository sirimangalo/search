1
00:00:00,000 --> 00:00:04,000
Hi, and welcome back to Ask a Monk.

2
00:00:04,000 --> 00:00:11,280
Today, I'd like to address several questions that have come in in regards to the issue

3
00:00:11,280 --> 00:00:19,600
specifically of celibacy and of sensuality in general.

4
00:00:19,600 --> 00:00:27,200
Because it seems like a lot of people have a hard time accepting or understanding the reason

5
00:00:27,200 --> 00:00:38,480
for monks in particular or Buddhists in general for giving up things like beauty or sensuality

6
00:00:38,480 --> 00:00:42,040
or even sexuality.

7
00:00:42,040 --> 00:00:52,480
And I think the reason for this comes from people's understanding that what is natural

8
00:00:52,480 --> 00:01:03,880
and our understanding that the human state is natural is equal to what is right.

9
00:01:03,880 --> 00:01:07,440
Something is natural, therefore it is right.

10
00:01:07,440 --> 00:01:15,480
And so we look at the human state and we see all of these emotions like lust and attraction

11
00:01:15,480 --> 00:01:17,880
and appreciation and so on.

12
00:01:17,880 --> 00:01:25,000
And we say that while these are natural and this is a part of nature and therefore it

13
00:01:25,000 --> 00:01:27,280
is right.

14
00:01:27,280 --> 00:01:34,040
And I think right away if we look at this idea of nature equals right, we can see that

15
00:01:34,040 --> 00:01:41,920
it's not really valid considering that much of what occurs in nature is considered by

16
00:01:41,920 --> 00:01:48,720
human standards to be immoral and improper, incorrect, wrong.

17
00:01:48,720 --> 00:01:54,800
If you look in the natural world, we see things like rape, murders, theft and we see

18
00:01:54,800 --> 00:02:04,680
that the natural world has no problem with these things to a certain extent.

19
00:02:04,680 --> 00:02:09,040
And I would further say that there's no reason to believe this, that what is natural

20
00:02:09,040 --> 00:02:15,600
is what is right. In fact, according to Buddhism, our natural state is one that arises

21
00:02:15,600 --> 00:02:17,440
from delusion.

22
00:02:17,440 --> 00:02:21,400
And the difference in understanding here is that for most people we're like a fish in

23
00:02:21,400 --> 00:02:22,560
water.

24
00:02:22,560 --> 00:02:26,920
All we know is the environment that we've grown up with.

25
00:02:26,920 --> 00:02:30,960
And this is how we look at the world, how we see the world is like the eyes of through

26
00:02:30,960 --> 00:02:33,720
the eyes of a fish when it looks in the water.

27
00:02:33,720 --> 00:02:39,800
And if you try to talk to a fish or if a fish were to try to understand something like

28
00:02:39,800 --> 00:02:52,400
dry land, you can imagine that it would be totally foreign to this sort of a life form.

29
00:02:52,400 --> 00:02:54,960
And human beings are in general the same.

30
00:02:54,960 --> 00:02:59,880
We have this set way of being and we think well this is a natural way and this is why,

31
00:02:59,880 --> 00:03:04,480
for instance, many people have a hard time believing in things like angels or gods or

32
00:03:04,480 --> 00:03:09,840
ghosts or beings which are considered to be unnatural or supernatural.

33
00:03:09,840 --> 00:03:14,240
And the only thing that's unnatural, supernatural about them is that they're not human.

34
00:03:14,240 --> 00:03:21,840
And for some reason we seem to believe that this experience is somehow more natural.

35
00:03:21,840 --> 00:03:26,920
It's reasonable because it's clearly observable whereas these other things are not observable

36
00:03:26,920 --> 00:03:29,400
to us.

37
00:03:29,400 --> 00:03:33,640
So Buddhism does or what the Buddha did was go beyond this and the Buddha was able to see

38
00:03:33,640 --> 00:03:39,080
things that most people are able to see just as a physicist is often general too, is

39
00:03:39,080 --> 00:03:44,760
in general often able to understand things that ordinary people are unable to understand

40
00:03:44,760 --> 00:03:51,800
and to therefore see things that ordinary people are not able to see.

41
00:03:51,800 --> 00:03:58,000
And in Buddhism, so we go beyond the human experience and we go beyond this idea of what

42
00:03:58,000 --> 00:04:06,080
is natural to understand that there's a lot more to existence and to reality and this

43
00:04:06,080 --> 00:04:10,480
limited experience.

44
00:04:10,480 --> 00:04:16,080
And what we come to see about this experience, when we look at it objectively and we

45
00:04:16,080 --> 00:04:20,760
stop saying well this is natural, this is therefore this is the right way of being, what

46
00:04:20,760 --> 00:04:35,120
we see is that in fact the human state of existence is one of attraction, addiction, gratification

47
00:04:35,120 --> 00:04:39,920
and future and further addiction or greater addiction.

48
00:04:39,920 --> 00:04:43,560
If we look at the chemistry in the mind, if we look at how addiction works, we can see

49
00:04:43,560 --> 00:04:49,600
that in everything we do, the whole of our existence and our gratification and the human

50
00:04:49,600 --> 00:05:00,280
happiness is one of addiction, is one of continued increased attachment.

51
00:05:00,280 --> 00:05:05,480
If we look at how the receptors in the brain work, these chemicals arise and they're

52
00:05:05,480 --> 00:05:11,280
received by the receptors when we have a pleasant feeling and so that gives us this sense

53
00:05:11,280 --> 00:05:14,920
of calm, a piece of contentment.

54
00:05:14,920 --> 00:05:21,640
But what happens is that reaction or that coupling of the chemical and the receptor weakens

55
00:05:21,640 --> 00:05:22,640
the receptor.

56
00:05:22,640 --> 00:05:29,000
So next time, it takes more of the chemical and hence more of the phenomenon, more of

57
00:05:29,000 --> 00:05:32,520
the experience to make us happy.

58
00:05:32,520 --> 00:05:39,640
This is why for many people marriage is often not satisfying and people will engage in

59
00:05:39,640 --> 00:05:47,480
extramarital affairs, it's why we find ourselves having to seek out new pleasures all the

60
00:05:47,480 --> 00:05:52,880
time, greater and greater and more exotic pleasures, why we can't be content with the

61
00:05:52,880 --> 00:05:55,440
same stimulus over and over again.

62
00:05:55,440 --> 00:05:58,000
It's a well-documented fact.

63
00:05:58,000 --> 00:06:03,360
In meditation this becomes even more clear and from a more objective point of view that

64
00:06:03,360 --> 00:06:05,760
we can actually understand for ourselves.

65
00:06:05,760 --> 00:06:09,120
We can see that when we get what we want, we only want it more.

66
00:06:09,120 --> 00:06:13,240
If we sit in meditation and we feel some happy feeling, we'll find the next time when

67
00:06:13,240 --> 00:06:20,480
we say we are looking for that feeling, we come to attach to it.

68
00:06:20,480 --> 00:06:35,760
And so why do monks maintain celibacy because a monk is someone who has vowed or set themselves

69
00:06:35,760 --> 00:06:42,160
on becoming free from all sensuality, from all addiction, a monk is someone who has decided

70
00:06:42,160 --> 00:06:45,360
that what the Buddha taught was correct, that there's nothing in the world worth clinging

71
00:06:45,360 --> 00:06:47,480
to and therefore they're going to give it up.

72
00:06:47,480 --> 00:06:54,680
Now, obviously someone who's engaging in sexuality or even most forms of sensuality is

73
00:06:54,680 --> 00:06:56,600
going in the opposite direction.

74
00:06:56,600 --> 00:07:03,760
They've affirmed in themselves that this is a good thing, that this sort of thing is beneficial.

75
00:07:03,760 --> 00:07:09,720
And therefore it's totally inappropriate for someone who's on the path, even for meditators.

76
00:07:09,720 --> 00:07:17,440
On a more practical level just in brief you can understand that it makes it very difficult

77
00:07:17,440 --> 00:07:24,240
for you to meditate when you're engaging in sensuality, even listening to music or watching

78
00:07:24,240 --> 00:07:26,200
movies or so on.

79
00:07:26,200 --> 00:07:30,400
These sorts of things you'll find are totally contrary to your meditation practice.

80
00:07:30,400 --> 00:07:35,680
If you engage in both together, you'll find that you have to choose one or the other,

81
00:07:35,680 --> 00:07:40,640
that you won't be able to at the same time meditate and enjoy sensuality because sensuality

82
00:07:40,640 --> 00:07:43,360
is something that intoxicates the mind.

83
00:07:43,360 --> 00:07:48,160
It's something that gives rise to this chemical reaction and then makes you very content

84
00:07:48,160 --> 00:07:49,160
for a short time.

85
00:07:49,160 --> 00:07:52,920
And so you don't have any thought that, oh boy, I have to do something to get rid of

86
00:07:52,920 --> 00:07:56,600
this desire or this addiction or so on.

87
00:07:56,600 --> 00:07:59,840
You feel gratified, you feel like the addiction is a good thing.

88
00:07:59,840 --> 00:08:04,160
And then of course it creates more addiction and makes it harder for you to attain the

89
00:08:04,160 --> 00:08:06,040
gratification in the future.

90
00:08:06,040 --> 00:08:08,080
And it's a cycle.

91
00:08:08,080 --> 00:08:15,560
It also becomes, tend, becomes a habit for me so that we find ourselves more and more requiring

92
00:08:15,560 --> 00:08:17,360
these things.

93
00:08:17,360 --> 00:08:24,520
So for a monk, it's antithetical to our practice and it's considered very bad for them.

94
00:08:24,520 --> 00:08:29,480
In fact, if a monk commits a sexual act, they're no longer a monk.

95
00:08:29,480 --> 00:08:33,640
They have to, they don't, they can't disrobe because they're no longer a monk.

96
00:08:33,640 --> 00:08:39,360
They just take the robes off and go and if they pretend, if they continue to wear the robes,

97
00:08:39,360 --> 00:08:45,120
they'm the, they consider to be, they're considered to be wearing the robes in properly

98
00:08:45,120 --> 00:08:46,960
and they're no longer a monk.

99
00:08:46,960 --> 00:08:49,880
So I hope that clarifies it a little bit.

100
00:08:49,880 --> 00:08:56,960
I think for many people, they have no desire when they start to meditate to give up sensuality

101
00:08:56,960 --> 00:09:03,480
or sexuality and I think that's fine and there's various levels of commitment.

102
00:09:03,480 --> 00:09:10,320
It should be clear that a monk has committed themselves 100% to, to attaining full freedom

103
00:09:10,320 --> 00:09:16,200
from addiction and therefore that's why there are these rules in place to make sure that

104
00:09:16,200 --> 00:09:22,880
only people who are really, really working for full freedom from suffering should be able

105
00:09:22,880 --> 00:09:30,080
to, should, should ordain and should become monks and for those people who, who believe

106
00:09:30,080 --> 00:09:34,320
that this is wrong or incorrect and then you're welcome to go and seek another path.

107
00:09:34,320 --> 00:09:40,000
If you're just looking to find some state of peace and comfort or even states of a basic

108
00:09:40,000 --> 00:09:43,680
wisdom and understanding, you're welcome to meditate and you don't have to become a monk

109
00:09:43,680 --> 00:09:46,720
and you sure don't have to become celibate, okay.

110
00:09:46,720 --> 00:09:53,720
So I hope that helps to clear things up, thanks for the questions, keep them coming.

