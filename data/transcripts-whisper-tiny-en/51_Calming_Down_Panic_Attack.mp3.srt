1
00:00:00,000 --> 00:00:06,320
calming down panic attack I have anxiety panic attacks at least one time per day.

2
00:00:06,320 --> 00:00:10,560
How can I calm down? If I think about what is happening, the panic becomes worse.

3
00:00:13,280 --> 00:00:17,920
When you are mindful of anxiety, you will see that the moments of anxiety are really not a very

4
00:00:17,920 --> 00:00:23,760
big problem at all. Anxiety is a state of mind that causes physical reactions like tensing,

5
00:00:23,760 --> 00:00:29,840
but it really stops there. Unless we react to anxiety or its physical results in a negative way,

6
00:00:29,840 --> 00:00:35,760
it is just an experience. The real problem is not experiences even inherently negative experiences

7
00:00:35,760 --> 00:00:41,760
like anxiety. The problem is our tendency to react to them, to react negatively to a negative experience.

8
00:00:42,720 --> 00:00:48,320
The truth is, if you recognize the anxiety as anxiety, the anxiety itself disappears.

9
00:00:48,960 --> 00:00:54,560
The anxious mind state is replaced by a mindful mind state. What does not disappear,

10
00:00:54,560 --> 00:00:59,920
and what fools you into thinking that you are still anxious, is the physical results of the anxiety,

11
00:00:59,920 --> 00:01:03,040
because they are not dependent on the anxiety to continue.

12
00:01:04,880 --> 00:01:09,840
The problem is we get worried about the physical effects of anxiety and say to ourselves,

13
00:01:09,840 --> 00:01:15,200
it is not going away, and then we get anxious again. The anxiety comes up again,

14
00:01:15,200 --> 00:01:19,520
and we try to be mindful again. We find that yes, the effects of it are still there,

15
00:01:19,520 --> 00:01:24,480
and so we think the anxiety is still there. Often, you need a teacher to point it out to you

16
00:01:24,480 --> 00:01:29,680
that the physical and mental are two different things. Seeing the difference between the physical

17
00:01:29,680 --> 00:01:34,960
and the mental is the first knowledge gained from mindfulness meditation. When you say anxious,

18
00:01:34,960 --> 00:01:40,560
anxious, or worried, worried, the worry is already gone. At that moment, you are not worried,

19
00:01:40,560 --> 00:01:46,720
rather you are focused, mindful, and clearly aware. The physical is something separate from this.

20
00:01:46,720 --> 00:01:52,400
The effects of past anxiety will not immediately go away when you are mindful. What you should do

21
00:01:52,400 --> 00:01:57,840
at that point when you still feel the effects of the anxiety is see the effects as physical and

22
00:01:57,840 --> 00:02:02,640
remind yourself of what they actually are. Note the feeling in the stomach as feeling, feeling,

23
00:02:02,640 --> 00:02:08,320
or tense tense. If you react to it, for example, by disliking it, you should focus on that new

24
00:02:08,320 --> 00:02:14,400
mind state and say to yourself, disliking, disliking. An anxiety attack is not just anxiousness,

25
00:02:14,400 --> 00:02:19,440
it is not an entity that exists continuously. It is many experiences arising in ceasing

26
00:02:19,440 --> 00:02:25,520
moment by moment. First, there is an anxiety, then the physical feeling, then disliking the physical

27
00:02:25,520 --> 00:02:32,880
feeling, then anxiety again, etc. So many different experiences are involved. There can be ego as well,

28
00:02:32,880 --> 00:02:38,320
where one might think, I am anxious. There can be the desire to be confident and to impress other

29
00:02:38,320 --> 00:02:43,200
people. Your meditation practice is helping you to see that these things are impermanent,

30
00:02:43,200 --> 00:02:49,200
suffering, and non-self. They are not under your control. Neither the physical nor the mental is

31
00:02:49,200 --> 00:02:54,320
subject to your control. The more you cling to it, the more you try to fix or avoid it, try to

32
00:02:54,320 --> 00:02:59,040
change it, or try to make it better. The more suffering, stress, and disappointment you will create

33
00:02:59,040 --> 00:03:05,200
for yourself and the bigger the problem will become. When the anxiety comes, it is correct to say

34
00:03:05,200 --> 00:03:10,320
to yourself, anxious, anxious, and it is correct to focus on it, to be mindful of it,

35
00:03:10,320 --> 00:03:16,240
and to let yourself be anxious. The anxiety is not the problem, the problem is your reaction to

36
00:03:16,240 --> 00:03:21,520
physical states that are caused by the anxiety. Anxiety is just a moment. It is something that

37
00:03:21,520 --> 00:03:26,800
occurs in the moment, and then it is gone. At the moment when you are mindful, it is already gone,

38
00:03:26,800 --> 00:03:30,880
and all that is left is the physical experiences, which should be noted as well.

39
00:03:31,680 --> 00:03:36,640
If you can be thorough with this and note what is present in each moment, the physical sensations

40
00:03:36,640 --> 00:03:40,720
that is striking, the thoughts, and feelings, you will begin to realize that nothing has the power

41
00:03:40,720 --> 00:03:47,120
to hurt you until you react to it. Even negative mind states cannot hurt you until you react to them.

42
00:03:49,120 --> 00:03:53,600
If you are patient with the anxiety, noting it, and noting the physical sensations,

43
00:03:53,600 --> 00:03:58,800
as well as the disliking of the anxiety, even though they do not seem to go away, noting every

44
00:03:58,800 --> 00:04:03,440
physical and mental experience that arises from moment to moment, you will be able to dispel

45
00:04:03,440 --> 00:04:08,480
the perception of being anxious, and you will cease to be disturbed by what you can see are simply

46
00:04:08,480 --> 00:04:13,280
individual moments of experience. You will see that they do not have any power over you.

47
00:04:13,920 --> 00:04:19,440
This realization is what truly leads to overcoming anxiety. It may take time, effort,

48
00:04:19,440 --> 00:04:24,720
and patience, but the path to overcoming both anxiety and fear is easy to follow when you realize

49
00:04:24,720 --> 00:04:40,640
that you can be anxious or afraid without having to panic or be consumed by the moments of emotion.

