1
00:00:00,000 --> 00:00:19,360
Good evening everyone broadcasting live April 1st, today's quote, interestingly enough isn't

2
00:00:19,360 --> 00:00:38,480
Buddha watching it. It isn't even Aria Sava Kavatana. I don't think. Can't remember now.

3
00:00:38,480 --> 00:00:57,840
The same idea, it's the words of Poseinade, King Poseinade Kosa.

4
00:00:57,840 --> 00:01:08,320
Zenade was a king, right? He's the anag at the ones, says that he is a bodhisattva, and he will be the force

5
00:01:08,320 --> 00:01:20,400
with future Buddha from now. So, it wasn't even enlightened in these are his words.

6
00:01:20,400 --> 00:01:36,000
But how they can stand is once he said in the Buddha reaffirmed the king's words. Anyway,

7
00:01:36,000 --> 00:01:44,480
they're spot on and they're worth repeating. But we also have the Buddha's response to him,

8
00:01:44,480 --> 00:01:50,000
which is maybe of interest. So, we'll look at the verses that the Buddha used to respond to

9
00:01:50,000 --> 00:01:57,040
Poseinade. So, what Poseinade basically says is he was wondering while he was sitting

10
00:01:57,040 --> 00:02:14,240
Rahul Gattasat when I was in seclusion. Who treat themselves as dear and who treat themselves as a foe?

11
00:02:14,240 --> 00:02:36,480
Of whom for whom are K. So, P.O. At the, I'm quite good at it. Anyway, it's something

12
00:02:36,480 --> 00:02:43,680
like whose self is dear. For whom is the self dear? I don't quite get no case on the case on

13
00:02:44,880 --> 00:02:55,840
for whom is, but it's a question for whom is the self dear? For whom is the self not dear?

14
00:02:57,600 --> 00:03:04,320
I guess it's just plural because for which people are in the self is the self.

15
00:03:04,320 --> 00:03:13,600
It's a singular, it's the self dear for which people is the self not dear, one's self.

16
00:03:13,600 --> 00:03:40,560
Because you can say everyone holds themselves dear, right? Everyone cares about themselves.

17
00:03:40,560 --> 00:03:50,960
But he thought to himself, even if some people say, you know, at our self is dear,

18
00:03:52,800 --> 00:04:00,880
at the quotation up you at the still to them the self is not dear.

19
00:04:00,880 --> 00:04:14,240
What is the cause? What is the cause? What a person who is the opposite of dear,

20
00:04:14,240 --> 00:04:21,680
under, not dear, might do to someone who is another person who is not dear towards.

21
00:04:22,880 --> 00:04:27,920
They're not kindly disposed towards. So, an enemy might do to an enemy.

22
00:04:27,920 --> 00:04:31,440
They are the now, or at the no, or at the no.

23
00:04:39,520 --> 00:04:44,080
For some people, sorry, they do that to themselves.

24
00:04:48,480 --> 00:04:52,160
When an enemy might do to an enemy, some people do that to themselves.

25
00:04:52,160 --> 00:04:58,000
They themselves do it to themselves. What this means is people who do

26
00:04:59,360 --> 00:05:06,320
who have evil thoughts is what he says, who have evil, evil deeds,

27
00:05:07,200 --> 00:05:13,280
who do evil deeds, who save all things, and who have evil thoughts.

28
00:05:13,280 --> 00:05:26,380
You don't nobody want to forget to forget to forget the

29
00:05:26,380 --> 00:05:35,400
other you want.

30
00:05:35,400 --> 00:05:40,120
They do, for they act.

31
00:05:40,120 --> 00:05:42,280
Duchadhi tell them.

32
00:05:42,280 --> 00:05:46,200
They act in a poor way, in an evil way.

33
00:05:46,200 --> 00:05:48,200
Ka Yaena with body.

34
00:05:48,200 --> 00:05:50,960
Wajaya with speech.

35
00:05:50,960 --> 00:05:54,840
Manasa with the mind.

36
00:05:54,840 --> 00:05:58,480
So again, these are the three doors.

37
00:05:58,480 --> 00:06:01,040
Everything good that we do, everything evil that we do,

38
00:06:01,040 --> 00:06:03,880
we do through these doors, everything that we do.

39
00:06:03,880 --> 00:06:07,000
So either there's your body's speech or thought.

40
00:06:07,000 --> 00:06:12,200
But of interest is the ethical and unethical things.

41
00:06:12,200 --> 00:06:19,800
And so the important point here is that someone who does bad deeds,

42
00:06:19,800 --> 00:06:23,560
well, the important point here is how Buddhist ethics works.

43
00:06:26,560 --> 00:06:30,520
That it's not because someone says it's bad.

44
00:06:30,520 --> 00:06:35,480
It's not because it's, it's not because you believe something is bad.

45
00:06:35,480 --> 00:06:39,640
It's because certain things are inherently contradictory.

46
00:06:39,640 --> 00:06:43,720
You, as I've said again and again, you want,

47
00:06:43,720 --> 00:06:46,960
you do something thinking that it'll make you happy.

48
00:06:46,960 --> 00:06:48,160
And it doesn't make you happy.

49
00:06:48,160 --> 00:06:50,560
You do something with a certain intention,

50
00:06:50,560 --> 00:06:53,720
with a certain intended goal, and that intended goal

51
00:06:53,720 --> 00:06:56,680
is not reached by that action.

52
00:06:56,680 --> 00:06:58,240
Which is inherently contradictory.

53
00:06:58,240 --> 00:07:04,200
There's a problem, I think, in a very sort of objective sense.

54
00:07:04,200 --> 00:07:20,600
There's an inconsistency or lack of logic or reason that's inherent in evil deeds.

55
00:07:20,600 --> 00:07:24,600
Point being we do and say and think things,

56
00:07:24,600 --> 00:07:30,720
certain things that go counter to our own intentions,

57
00:07:30,720 --> 00:07:35,000
that lead to results counter to our own intentions.

58
00:07:35,000 --> 00:07:38,200
By their very nature, now you can have good intentions

59
00:07:38,200 --> 00:07:41,240
wanting to do something good for someone and it doesn't work out.

60
00:07:41,240 --> 00:07:42,160
So that's not the point.

61
00:07:42,160 --> 00:07:48,680
But there are certain things that they themselves lead to the opposite

62
00:07:48,680 --> 00:07:52,480
or are conducive to the opposite of one's intention.

63
00:07:52,480 --> 00:08:00,120
So we kill and steal and lie and cheat intrinsically with the thought

64
00:08:00,120 --> 00:08:10,240
that it's going to bring us some benefit or ameliorate some disadvantage.

65
00:08:10,240 --> 00:08:12,720
Something bad is that we're in a bad situation.

66
00:08:12,720 --> 00:08:18,600
We do bad things to get out of a bad situation, for example.

67
00:08:18,600 --> 00:08:19,680
We heard others.

68
00:08:19,680 --> 00:08:25,760
Now they're called bad in Buddhism, not because God or Buddha or monks

69
00:08:25,760 --> 00:08:31,240
or teachers say they're bad, but they're bad because it's a bad choice.

70
00:08:31,240 --> 00:08:40,000
It's Akusala and skillful in it.

71
00:08:40,000 --> 00:08:45,320
It's something that doesn't lead to a solution.

72
00:08:45,320 --> 00:08:50,400
And so you end up unhappy with the results.

73
00:08:50,400 --> 00:08:56,320
And by their very nature, not just by chance or because your good plans are

74
00:08:56,320 --> 00:09:01,680
foiled, but killing and stealing and lying and cheating and taking drugs

75
00:09:01,680 --> 00:09:06,640
in alcohol and even thinking bad thoughts of wanting to hurt others or thoughts

76
00:09:06,640 --> 00:09:19,000
of greed is on thoughts of arrogance and conceit.

77
00:09:19,000 --> 00:09:21,200
These things can do to our suffering.

78
00:09:21,200 --> 00:09:28,400
They can do to disruption of our plans.

79
00:09:28,400 --> 00:09:34,080
Anytime you incorporate these into a plan, it can only go sour.

80
00:09:34,080 --> 00:09:38,160
When you do something, you have a good intention, but then you get greedy about it

81
00:09:38,160 --> 00:09:42,400
or you get angry or frustrated, or you become arrogant or conceited.

82
00:09:42,400 --> 00:09:49,280
Even if you're good, you have good plans to help yourself, to help others.

83
00:09:49,280 --> 00:09:54,720
The greed, anger and delusion, whether by body, you do things to hurt others or

84
00:09:54,720 --> 00:10:02,520
to manipulate others or to oppress others, you say things to hurt others,

85
00:10:02,520 --> 00:10:06,440
you say things out of selfishness and so on.

86
00:10:06,440 --> 00:10:13,880
Or even you think things that are harmful and these things disturb your

87
00:10:13,880 --> 00:10:17,440
plans because of the results that they have with other people because of the

88
00:10:17,440 --> 00:10:26,240
effect they have on your mind, because of the disruption that they cause on so

89
00:10:26,240 --> 00:10:33,160
many levels.

90
00:10:33,160 --> 00:10:42,040
And he says conversely that someone who has good thoughts, someone whose mind is

91
00:10:42,040 --> 00:10:48,400
just good bodily and verbal and mental actions, his body's speech and mind

92
00:10:48,400 --> 00:10:58,920
are purely, even if they were to say, can chop it even when they don't feel no at that.

93
00:10:58,920 --> 00:11:05,400
I don't know, okay, up you know at that, at the courte some feel at that.

94
00:11:05,400 --> 00:11:09,760
Even still they would be dear to themselves, why?

95
00:11:09,760 --> 00:11:17,400
Because young hippie Opsakare would a dear one may do to one who is dear to them,

96
00:11:17,400 --> 00:11:21,640
one day at the now, at the no guarantee.

97
00:11:21,640 --> 00:11:26,640
They do that to themselves or for themselves.

98
00:11:26,640 --> 00:11:31,040
The smart days and view at that reason, they are dear to themselves.

99
00:11:31,040 --> 00:11:36,160
I know here's the key, the Buddha repeats back what he says and the Buddha does this,

100
00:11:36,160 --> 00:11:43,520
I mean it's a critic or a skeptic would say it's probably just a way of,

101
00:11:43,520 --> 00:11:49,800
this was probably just added or manipulated, because you couldn't include the

102
00:11:49,800 --> 00:11:54,480
words of the king without having the Buddha repeat them, otherwise it would not

103
00:11:54,480 --> 00:11:56,680
be Buddha what gender.

104
00:11:56,680 --> 00:12:00,040
So I don't know about that, but the text has it that the Buddha repeated what

105
00:12:00,040 --> 00:12:01,040
he said.

106
00:12:01,040 --> 00:12:10,600
Doesn't really matter, point is it's useful teaching, something proper to say

107
00:12:10,600 --> 00:12:15,400
and it shows that the king, for all his faults, did have some good ideas from

108
00:12:15,400 --> 00:12:20,960
listening to the Buddha, and so we take wisdom wherever it is, even the

109
00:12:20,960 --> 00:12:25,120
Buddha said I agree when someone, when another teacher in another religion or

110
00:12:25,120 --> 00:12:29,640
another tradition says something, that's right, I have a degree with it,

111
00:12:29,640 --> 00:12:32,600
when if they say something that's wrong, I will disagree with that.

112
00:12:32,600 --> 00:12:42,720
It's not, this isn't sectarian, wisdom doesn't know religions, truth doesn't

113
00:12:42,720 --> 00:12:48,520
stay within one religion, it doesn't mean that everything every religion

114
00:12:48,520 --> 00:12:54,800
teaches is right, it just means that the truth is not interested in such

115
00:12:54,800 --> 00:13:05,240
things. Anyway, the Buddha gives some verses sort of as a, as a addition to this

116
00:13:05,240 --> 00:13:06,120
teaching.

117
00:13:06,120 --> 00:13:34,520
It is not easy, it is not easily gained happiness for one who does evil,

118
00:13:34,520 --> 00:13:50,360
it is not easy for indeed it is not easily done, it's not easily gained happiness

119
00:13:50,360 --> 00:14:09,480
for one who does evil deeds, when one is seized, when one is seized by the end

120
00:14:09,480 --> 00:14:15,000
maker, as one describes the human state, what can one truly call one's own,

121
00:14:15,000 --> 00:14:21,000
what does one take when one goes, what follows one along like a shadow that

122
00:14:21,000 --> 00:14:27,360
never departs, so here's a question of Buddha often asks, what do we take with

123
00:14:27,360 --> 00:14:35,920
us, what follows along like a shadow that never reparts, which is reference to

124
00:14:35,920 --> 00:14:41,640
the first dumb upon a verse right, or the second dumb upon a verse actually, but

125
00:14:41,640 --> 00:14:50,880
our evil good, good and evil deeds follow our what follows, actually you know

126
00:14:50,880 --> 00:14:56,920
that's not the first, but they use the word, it's the imagery just in the shadow,

127
00:14:56,920 --> 00:15:03,160
but he uses this, this idea of the shadow in other ways, and in this way it's in

128
00:15:03,160 --> 00:15:15,240
other places, chaya wa anapayimi, it's the same anupayimi, it's the same

129
00:15:15,240 --> 00:15:19,720
wording as the second dumb upon a verse, it's a difference, similarly this is

130
00:15:19,720 --> 00:15:24,120
talking about good deeds following us around, even the same way, good and evil

131
00:15:24,120 --> 00:15:36,600
deeds following us around, kubo, pun, yan, chapa, pan, chaya, yang, macho, kurutade, both

132
00:15:36,600 --> 00:15:49,680
good and evil, these, that a mortal does right here, the mortal does here,

133
00:15:49,680 --> 00:16:00,840
one good and evil that we do here, then he does a second ho, titansha, dai, dai, dai, yakachati.

134
00:16:00,840 --> 00:16:09,880
This is, this is our own right, this indeed is one's own, the good and the evil that

135
00:16:09,880 --> 00:16:19,000
young macho, kurutade, the good and evil that we do here, that indeed is our own, tansha

136
00:16:19,000 --> 00:16:25,400
dai, yakachati, that goes with us, that is what we take with us, that is what we

137
00:16:25,400 --> 00:16:39,920
carry with us, tansha, anugang ho, tit, jaya wa anupayimi, that follows after us, just

138
00:16:39,920 --> 00:16:51,040
like a shadow that never leaves, tasma, kareya, kalya, nang, chayang, samparai, kam, punya, niparaloka,

139
00:16:51,040 --> 00:16:54,360
sping, patiktau, untipan.

140
00:16:54,360 --> 00:17:02,880
This is a very, that part is a verse that we chant actually, as part of a blessing, when

141
00:17:02,880 --> 00:17:12,120
we give blessings, tasma, kareya, kalya, nang, nipchayang, samparai, kam.

142
00:17:12,120 --> 00:17:21,680
For that reason one should do beautiful things, nipchayang, samparai, kam, nipchaya, the

143
00:17:21,680 --> 00:17:33,480
accumulation, and accumulation for the next world, punya, niparaloka, sping, patiktau,

144
00:17:33,480 --> 00:17:44,000
untipan, niparaloka, sping, patiktau, untipan, niparaloka, good, good deeds in the next world

145
00:17:44,000 --> 00:17:53,240
are what beings stand on, are a foundation of beings, as beak of body translates it, merits

146
00:17:53,240 --> 00:18:02,400
are the support for living beings when they arise in the other world.

147
00:18:02,400 --> 00:18:11,800
And that's key for a comprehensive understanding, because it is possible to do evil deeds

148
00:18:11,800 --> 00:18:19,360
and to be an evil person and still benefit in this world, but you're just adding to the

149
00:18:19,360 --> 00:18:24,760
burden that you carry around with you, and it follows you.

150
00:18:24,760 --> 00:18:32,760
So, it's important to understand that death isn't an escape, you don't get the cart

151
00:18:32,760 --> 00:18:41,800
along that death, when you die, it's not a clean slate, suicide isn't the answer, death

152
00:18:41,800 --> 00:18:52,400
isn't the answer, death isn't the end, it's the point, which is, you know, it extends

153
00:18:52,400 --> 00:18:59,360
the potency of karma, because if karma is just in this life, otherwise it was escaping

154
00:18:59,360 --> 00:19:05,480
it temporarily, and you can die without having to really pay back your karma, that's,

155
00:19:05,480 --> 00:19:11,200
I think, true, right, I mean, it's true, but I think that's obvious that the question

156
00:19:11,200 --> 00:19:21,440
is whether it's actually viable, of course, Buddhism says it's not that death isn't

157
00:19:21,440 --> 00:19:27,600
the end, anyway, relating it to our meditation practice, because that's what we always

158
00:19:27,600 --> 00:19:38,320
try to do, there's an appreciation in Buddhism that some people don't maybe appreciate

159
00:19:38,320 --> 00:19:44,360
that you are your own best friend, and you are your own worst enemy, no one can hurt

160
00:19:44,360 --> 00:19:50,600
us the way we do ourselves, and no one can help us the way we do ourselves.

161
00:19:50,600 --> 00:19:57,560
And so there's two corollaries that one is that the importance of looking after your

162
00:19:57,560 --> 00:20:07,360
self, and the supremacy of looking after yourself overlooking after others, and the

163
00:20:07,360 --> 00:20:11,920
answer to the world's problems is not to help each other, it's just for everyone to help

164
00:20:11,920 --> 00:20:16,960
themselves, and you're going to help others, it should be to help them help themselves.

165
00:20:16,960 --> 00:20:22,160
If you help others trying to fix their problems, anyone has ever tried to fix other

166
00:20:22,160 --> 00:20:30,600
people's problems knows that it's a wild goose chase, it's a never-ending battle even

167
00:20:30,600 --> 00:20:38,240
that being their dependency, and you never really fix anything, because you can't fix

168
00:20:38,240 --> 00:20:43,920
other people, we can fix ourselves into much more for ourselves.

169
00:20:43,920 --> 00:20:48,520
So this is where meditation fits, and my meditation is so important, it's not the

170
00:20:48,520 --> 00:20:56,640
train or abandoning others, it's doing the right thing, and that everyone should do,

171
00:20:56,640 --> 00:21:03,240
and they should work to purify our body, and our acts, and our speech, and our thought.

172
00:21:03,240 --> 00:21:12,200
It's the duty of all beings, we can't do that one thing, and become our worst enemy,

173
00:21:12,200 --> 00:21:18,720
we can never truly say that we care for ourselves, in the world they'll never be at peace,

174
00:21:18,720 --> 00:21:23,200
well it's not at peace because we don't do enough for others, it's not at peace because

175
00:21:23,200 --> 00:21:33,440
no one does enough for themselves, very few people do their duty to become stand up, human

176
00:21:33,440 --> 00:21:41,520
beings, so we've got lots of problems inside, that's where the real problems are.

177
00:21:41,520 --> 00:21:45,240
That's one thing that religion understands, you can criticize religion all you want, but

178
00:21:45,240 --> 00:21:50,080
one thing it understands is that problems are inside our minds, and it's in our minds

179
00:21:50,080 --> 00:21:53,240
that we have to fix things, it's not in the world out there, it's not in the physical,

180
00:21:53,240 --> 00:21:58,680
it's not even in the brain, you can't fix things by medicating, you can't fix things

181
00:21:58,680 --> 00:22:05,680
by social work, or psychology, or whatever, you only fix them in your mind, the world

182
00:22:05,680 --> 00:22:15,320
physical world is not the answer, it doesn't work, and the evidence is there, whether

183
00:22:15,320 --> 00:22:21,960
it's ever been collected in a modern scientific context, but of course there have been

184
00:22:21,960 --> 00:22:29,640
some studies, they're not all that impressive, but that's more testament to the limitations

185
00:22:29,640 --> 00:22:36,760
of the modern scientific method, and the limitations of funding and so on, but getting

186
00:22:36,760 --> 00:22:46,080
people to actually meditate in a study, but the evidence is overwhelming that the benefits

187
00:22:46,080 --> 00:22:54,120
say of any kind of physical solution to a simple mental solution, meditation changes

188
00:22:54,120 --> 00:22:59,160
your life, how many people have told me that their life has been changed through the

189
00:22:59,160 --> 00:23:04,160
meditation, that people have never even met, and that's not because of me or anything

190
00:23:04,160 --> 00:23:10,480
special I've done, it's just the meditation practice to see things clearly as they are

191
00:23:10,480 --> 00:23:18,120
of reminding yourself this is this, it is what it is, it's nothing else, constantly working

192
00:23:18,120 --> 00:23:34,200
to better yourself, to do right by yourself, this is the most powerful force, so that's

193
00:23:34,200 --> 00:23:47,920
the number for tonight, and this is why we meditate, working on our one true task, so

194
00:23:47,920 --> 00:23:59,480
push the hangout, we probably just start taking text questions again, no one's coming

195
00:23:59,480 --> 00:24:06,120
on the hangout, so it's a bit much to ask people to come on a live on the air hangout

196
00:24:06,120 --> 00:24:13,480
to ask their questions, so it's just that the questions were piling up and they were

197
00:24:13,480 --> 00:24:21,440
backlogged and a lot of the more questions that have already been answered, if anybody

198
00:24:21,440 --> 00:24:24,880
has any pressing questions and you don't want to come on the hangout, you can text

199
00:24:24,880 --> 00:24:30,200
it there at meditation.surremungalow.org.

200
00:24:54,880 --> 00:25:22,880
All right, well, let's do it, okay, we're tall and, oh, hi, Tom.

201
00:25:22,880 --> 00:25:27,520
You have a question for me?

202
00:25:27,520 --> 00:25:30,560
Yes, I do.

203
00:25:30,560 --> 00:25:41,360
You were talking about it, I should make sure this is working properly, yeah, okay, go ahead.

204
00:25:41,360 --> 00:25:49,160
Yes, last night you were talking about the piece, well I'll call it the piece movement

205
00:25:49,160 --> 00:26:01,640
on it, remember the exact name, and I'm an American, I became a young adult when the Vietnam

206
00:26:01,640 --> 00:26:15,520
War was just really ramping up, I've had a lot of difficulty dealing with the actions

207
00:26:15,520 --> 00:26:23,360
of our government over the decades, and you know, to anybody who's paying attention,

208
00:26:23,360 --> 00:26:32,760
it's obviously getting worse and worse, not better, so I have a lot of personal difficulty

209
00:26:32,760 --> 00:26:43,120
dealing with the activities that are going on in the world, mainly our involvement in military

210
00:26:43,120 --> 00:26:48,280
actions around the globe and what the repercussions have been, and this has a great deal

211
00:26:48,280 --> 00:27:00,840
of, it has a major impact on me personally, so I just wanted your perspective on that

212
00:27:00,840 --> 00:27:01,840
place.

213
00:27:01,840 --> 00:27:08,960
Well, recalling what I said last night about, you're talking about how the trouble maybe

214
00:27:08,960 --> 00:27:14,200
I was having conflict or the way they looked at the piece as opposed to how I looked at

215
00:27:14,200 --> 00:27:18,400
the piece, that kind of thing, is that what I was even referring to?

216
00:27:18,400 --> 00:27:28,880
Well, just that you mentioned, I guess you've, you're active in a group at the university

217
00:27:28,880 --> 00:27:44,600
there, that's trying to bring about a more peaceful world or whatever, and me, it's not

218
00:27:44,600 --> 00:27:54,680
really that in particular, it's just to me right now, I don't want to say it seems hopeless,

219
00:27:54,680 --> 00:28:07,040
but we seem to be on a trajectory that is getting us, the US, and the world into a deeper

220
00:28:07,040 --> 00:28:15,400
and deeper state of conflict, and this has been something that I've looked at since I

221
00:28:15,400 --> 00:28:31,280
was a young man, and I just feel like it's completely out of hand, and I feel that things

222
00:28:31,280 --> 00:28:45,360
could easily get very bad, everybody knows what the capacity is of humans to make

223
00:28:45,360 --> 00:28:54,880
war on one another, so anyway, my point is very specifically, because it's, I'll call it

224
00:28:54,880 --> 00:29:01,640
an obsession of mine, how do I deal with that, or how does anybody deal with that when

225
00:29:01,640 --> 00:29:12,600
you live in a time, in a place where you can easily become overwhelmed with the activities

226
00:29:12,600 --> 00:29:24,120
of our political leaders, and so on? Michael, you can go, you don't have to stay here.

227
00:29:24,120 --> 00:29:33,760
Well, there's lots of sort of important points to keep in mind, the first is that the human

228
00:29:33,760 --> 00:29:40,760
race is doomed, I don't think there's anyone who doubts the fact that eventually it's

229
00:29:40,760 --> 00:29:51,280
going to be wiped out in a blaze of fire or whatnot, or even just more generally, everything

230
00:29:51,280 --> 00:30:02,560
changes. There's no solution besides release from Samsara, which is individual, and the

231
00:30:02,560 --> 00:30:09,760
second thing is the, so that's about the futility of the ultimate futility of making

232
00:30:09,760 --> 00:30:22,240
that a goal, making the peace on earth or so on a goal, but the other part is the actual

233
00:30:22,240 --> 00:30:31,360
again, it comes down to what is leading to your goals and what is actually counterproductive,

234
00:30:31,360 --> 00:30:42,720
so obsession and frustration and depression and despair, and Bernie Sanders said don't

235
00:30:42,720 --> 00:30:50,880
go into that pit of despair, which I think was really, he's been around for a long time

236
00:30:50,880 --> 00:30:56,560
and on the right side of things for a long time and met with interesting people, he's

237
00:30:56,560 --> 00:31:04,160
really got an interesting perspective. It's a lot like the same idea, I think one of these

238
00:31:04,160 --> 00:31:13,360
guys who, well, we wouldn't say he's pure by Buddhist standards, but he's a really good

239
00:31:13,360 --> 00:31:27,680
sort of politician if the wise mind seems anyway. So negative emotions are not only futile

240
00:31:27,680 --> 00:31:32,080
in a sense like why are you worrying about something that's in the end, never going to be

241
00:31:32,080 --> 00:31:42,040
a solution, but also actually detrimental. And it was important on the one hand to think of always

242
00:31:42,040 --> 00:31:52,560
thinking in terms of your spirituality. But on the other hand, to be aware of how your civic

243
00:31:52,560 --> 00:32:00,280
duty and good deeds towards your fellow human beings are actually supportive of spirituality

244
00:32:00,280 --> 00:32:09,640
and so therefore are important, but you understand how do you affect real change and benefit,

245
00:32:09,640 --> 00:32:17,000
significance in the world. It's not by obsession or frustration or depression or getting

246
00:32:17,000 --> 00:32:28,560
angry about things isn't useful. So there's some sort of perspective, I think, obviously

247
00:32:28,560 --> 00:32:36,600
meditation is important, staying mindful, but another important point is which I kind of

248
00:32:36,600 --> 00:32:43,400
said already is I think there is room for civic duty and an importance for late people

249
00:32:43,400 --> 00:32:49,600
to come to undertake their civic duty. Now, I kind of made a remark that may have sounded

250
00:32:49,600 --> 00:32:59,080
like I was condescending or disparaging of worldly peace work, but I'm not. I really think

251
00:32:59,080 --> 00:33:04,600
it's awesome that people are working in a worldly way to bring about peace, of course,

252
00:33:04,600 --> 00:33:11,360
I would argue that it's inferior to meditation, but you could also say it's an important

253
00:33:11,360 --> 00:33:16,600
part of spiritual practice, but it's not for monks, it's really the point. As a monk,

254
00:33:16,600 --> 00:33:26,760
I have some problems getting through involved with worldly peace or world peace. I have

255
00:33:26,760 --> 00:33:32,240
limits personally that you shouldn't take as a model unless you're looking to become

256
00:33:32,240 --> 00:33:38,760
monastic because I think non monastics have certain civic duties that you could argue

257
00:33:38,760 --> 00:33:43,160
they shouldn't take seriously.

258
00:33:43,160 --> 00:34:00,840
Is that how? I'd like to say yes. I can't have played all of these things. I just really shut it out.

259
00:34:00,840 --> 00:34:08,200
But since you brought up, you know, because I look at peace activity and when you look

260
00:34:08,200 --> 00:34:16,360
at a university is to be expected, I was both in the service and at a university in the

261
00:34:16,360 --> 00:34:26,360
late 60s and early 70s and look for it. Got us. But thank you. I appreciate your thoughts

262
00:34:26,360 --> 00:34:39,600
on that and I'll let you go. Nice to talk to you. I guess that's all for tonight then. See

263
00:34:39,600 --> 00:35:00,520
you all next time.

