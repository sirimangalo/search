1
00:00:00,000 --> 00:00:05,680
I have a problem. The moment I lose awareness, the fears and negative emotions are attacking

2
00:00:05,680 --> 00:00:11,920
me. It's impossible to maintain constant awareness all the time. Can you give me some

3
00:00:11,920 --> 00:00:24,040
advice? Thanks. What do you think? I don't think that it's impossible to maintain constant

4
00:00:24,040 --> 00:00:33,120
awareness all the time. You do the best you can, and then the fears and negative emotions

5
00:00:33,120 --> 00:00:40,120
will slowly start going away. But you can't expect to maintain awareness all the time.

6
00:00:40,120 --> 00:00:42,040
You just do the best you can.

7
00:00:42,040 --> 00:00:53,360
You can be also aware what is attacking you. Very good. I miss you on that one. Copy

8
00:00:53,360 --> 00:00:57,040
that.

9
00:00:57,040 --> 00:00:58,840
Just listening.

10
00:00:58,840 --> 00:01:07,800
Well, the biggest thing that I would point out is you have a problem, and that is that

11
00:01:07,800 --> 00:01:15,640
you're trying to maintain awareness. As soon as you say the moment I lose X, something bad

12
00:01:15,640 --> 00:01:22,880
why happens? I understand. I hear you where you're coming from. You're saying why is

13
00:01:22,880 --> 00:01:29,360
the problem? You have X and you have Y. As soon as I lose X, why happens? The problem is that

14
00:01:29,360 --> 00:01:35,920
you're trying to maintain X. That's the real problem. If your stability of mind depends

15
00:01:35,920 --> 00:01:41,760
on something, that is the crutch for you. That thing that you depend on is the problem.

16
00:01:41,760 --> 00:01:47,440
That's how Buddhism works. That's how the meditation practice works. You have to start

17
00:01:47,440 --> 00:01:53,120
by examining this need to be what you call aware all the time. It's not actually being

18
00:01:53,120 --> 00:02:00,160
aware. Awareness, if awareness were the problem, then why can't you be aware of the fears

19
00:02:00,160 --> 00:02:01,880
and negative emotions?

20
00:02:01,880 --> 00:02:05,720
If it were true that you're losing awareness, are you not aware of the fears and negative

21
00:02:05,720 --> 00:02:13,640
emotions? The point is that you're not concentrated, or your mind is not stable, satisfying

22
00:02:13,640 --> 00:02:20,480
and controllable. You're having the problem that you can't control your mind. Now,

23
00:02:20,480 --> 00:02:25,080
guess what the Buddha taught about the mind? It's not subject to control. So guess what

24
00:02:25,080 --> 00:02:31,160
you're learning? We pass in there. Wonderful in there. You're starting to see things as

25
00:02:31,160 --> 00:02:38,760
they are. Isn't it scary? What the only way out of fears and negative emotions is through

26
00:02:38,760 --> 00:02:46,440
them, to see them as they are, your attachment to what you call awareness, which is probably

27
00:02:46,440 --> 00:02:54,200
just concentration or focus or even peace and quiet and so on and so on, is the real problem

28
00:02:54,200 --> 00:03:00,560
because you can see that it's not sustainable. It's not the real problem. It's the first

29
00:03:00,560 --> 00:03:07,920
problem that you have to deal with. Before you can deal with the fears and negative emotions.

30
00:03:07,920 --> 00:03:15,160
And I think that labeling them helps you become the watcher of them and it removes you

31
00:03:15,160 --> 00:03:21,600
from that, like you say, if you have anger instead of getting absorbed into the anger, if

32
00:03:21,600 --> 00:03:27,760
you can label the anger, see it's there and that process will remove you from the anger

33
00:03:27,760 --> 00:03:39,720
or whatever emotion will go away on a song. So that's one thing. But again, we talked about

34
00:03:39,720 --> 00:03:45,920
this before and Alfred gave a great answer. They go away on their own. You don't worry

35
00:03:45,920 --> 00:03:50,120
about it. You don't have a problem. Let's go even deeper. You don't have a problem. You've

36
00:03:50,120 --> 00:03:59,800
got emotions. You've got fears. All of these things you have. Seeing them as a problem

37
00:03:59,800 --> 00:04:05,880
is not helping. Seeing them as a problem is making it worse for you. So start looking

38
00:04:05,880 --> 00:04:10,880
at them and start looking at everything and just look at what comes. Don't think to yourself,

39
00:04:10,880 --> 00:04:15,120
oh, this is no good because I'm going to get afraid again or this is no good. What about

40
00:04:15,120 --> 00:04:19,960
those negative emotions that I had yesterday or what if they come back or so on? What

41
00:04:19,960 --> 00:04:26,120
if my, what about my problem when you could be focusing on what's right here? Now awareness

42
00:04:26,120 --> 00:04:31,240
is right here and now you can create awareness at any moment. You can't lose it because

43
00:04:31,240 --> 00:04:37,800
you didn't have it. You, you created in one moment. That's the moment that you have awareness

44
00:04:37,800 --> 00:04:43,560
or it's not exactly the word I would use recognition or trying for something a little more

45
00:04:43,560 --> 00:04:53,960
specific. We're trying for a specific type of awareness and that's objective fixed exact

46
00:04:53,960 --> 00:05:01,440
and exact awareness or take us objective is another way of explaining it where you simply

47
00:05:01,440 --> 00:05:08,560
know the object as it is. And that you can create at any moment. So when you lay the labeling

48
00:05:08,560 --> 00:05:14,520
is a recognition, it reaffirms the recognition of the object. You recognize something. You

49
00:05:14,520 --> 00:05:19,440
reaffirm that and you keep the mind at bare recognition without getting caught up in it.

50
00:05:19,440 --> 00:05:24,880
All negative emotions disappear in a moment. What's left is the physical manifestations

51
00:05:24,880 --> 00:05:31,440
that are a result. This is the answer, the panic one. Anger lasts for a moment and all

52
00:05:31,440 --> 00:05:35,400
negative emotions, all emotions lasts for a moment, but they affect the body. And so you

53
00:05:35,400 --> 00:05:40,000
think you're still angry because you have a headache or because you are tense in the body

54
00:05:40,000 --> 00:05:45,000
because there's a adrenaline running because your heart is beating. None of those are anger.

55
00:05:45,000 --> 00:05:51,880
All of those are our physical manifestations caused by the anger. So then you can focus

56
00:05:51,880 --> 00:05:58,040
on them and you can deal with them. The anger only lasts for a moment and it returns again

57
00:05:58,040 --> 00:06:05,560
and again and again to affect the body fear as well. Fear, not all of what we call fear

58
00:06:05,560 --> 00:06:11,640
is actually fear. Fear is a momentary mental state that causes changes in the body and

59
00:06:11,640 --> 00:06:17,560
then these changes we associate with fear become upset about them and get more afraid

60
00:06:17,560 --> 00:06:24,520
in its no balls. If you focus on the physical manifestations, focus on the fear and moment

61
00:06:24,520 --> 00:06:29,160
to a moment, focus on whatever comes out, no matter what it may be. And it may be totally

62
00:06:29,160 --> 00:06:36,200
unrelated to your quote unquote problem. Slowly they will work their way out. They will

63
00:06:36,200 --> 00:06:47,560
work themselves out. Be patient, be realistic, don't expect anything and consider that

64
00:06:47,560 --> 00:06:52,160
you're here to learn. That's why they call it practice, not perfect. You're not here

65
00:06:52,160 --> 00:06:59,600
to perfect. You're here to practice. And more importantly, you're here to learn. It's not

66
00:06:59,600 --> 00:07:06,560
a magic trick where you sit and meditate and pulff you become enlightened. It's not like pulling

67
00:07:06,560 --> 00:07:11,680
a rabbit out of your hat. It's learning. Learning more about yourself, more about reality.

68
00:07:11,680 --> 00:07:15,800
And that's what you're doing. You're really learned something. You're learning something

69
00:07:15,800 --> 00:07:20,280
that you don't want to accept, but you're learning the hard truth that you can't control

70
00:07:20,280 --> 00:07:28,040
the mind. And remember that it starts off hard and it gets easier. And so you just have

71
00:07:28,040 --> 00:07:34,360
to keep going at it. And when you get absorbed into one of the emotions, after the fact,

72
00:07:34,360 --> 00:07:38,680
you look at what happened and see the way you got absorbed into it and see the way it affected

73
00:07:38,680 --> 00:07:45,800
your body and learn from it and don't beat yourself up over it. Just continue. And the next

74
00:07:45,800 --> 00:07:51,480
time maybe you can not become absorbed in it and become the watcher of it. You know, it's a

75
00:07:51,480 --> 00:07:58,040
learning process. Try not to feel guilty, not to, not to beat yourself up is a very important

76
00:07:58,040 --> 00:08:03,000
part. In Asian societies, it's not such a problem. They don't beat themselves up so much.

77
00:08:03,960 --> 00:08:10,360
They go the opposite way in terms of maybe indulging themselves a little too much. But Westerners

78
00:08:10,360 --> 00:08:16,120
are very good at beating themselves up and it's a real problem. So the guilt and the anger

79
00:08:16,120 --> 00:08:20,360
at yourself for not being a perfect meditator, just like everything. We've got to be perfect at

80
00:08:20,360 --> 00:08:25,080
everything. And so we spend most of our lives beating ourselves up because we're not perfect.

81
00:08:25,080 --> 00:08:41,560
And it's quite sad. Okay. Is that it? That was our last question.

