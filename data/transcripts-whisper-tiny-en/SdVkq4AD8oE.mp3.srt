1
00:00:00,000 --> 00:00:03,480
Hi, and welcome back to Ask a Monk.

2
00:00:03,480 --> 00:00:09,440
In today's question comes from Nat Jo Rote.

3
00:00:09,440 --> 00:00:11,080
Hello, Yuta Damo.

4
00:00:11,080 --> 00:00:13,480
How can meditation help me overcome an addiction

5
00:00:13,480 --> 00:00:15,240
to drugs in alcohol?

6
00:00:15,240 --> 00:00:16,720
I would really appreciate your answer.

7
00:00:16,720 --> 00:00:20,160
Thanks for your time.

8
00:00:20,160 --> 00:00:22,720
Well, I've gone over addiction before,

9
00:00:22,720 --> 00:00:26,960
but there are some specifics about drugs in alcohol

10
00:00:26,960 --> 00:00:33,720
about physical addictions that bear mentioning.

11
00:00:33,720 --> 00:00:35,120
First of all, with any addiction,

12
00:00:35,120 --> 00:00:41,320
as I've said before, there are certain parts

13
00:00:41,320 --> 00:00:43,760
of the mind that come into play, or certain parts

14
00:00:43,760 --> 00:00:47,440
of reality, physical and mental that come into play.

15
00:00:47,440 --> 00:00:51,880
The first one is the object of one's addiction.

16
00:00:51,880 --> 00:00:57,800
So in this case, we're talking about a physical substance

17
00:00:57,800 --> 00:01:01,160
when you see it, or when you think about it,

18
00:01:01,160 --> 00:01:03,680
when you smell it, when you taste it, or so on.

19
00:01:03,680 --> 00:01:06,000
However, it touches you.

20
00:01:06,000 --> 00:01:07,560
Because that's what's going to set off

21
00:01:07,560 --> 00:01:12,440
the memory of the bliss and the pleasure.

22
00:01:12,440 --> 00:01:14,480
Even though it's like food.

23
00:01:14,480 --> 00:01:18,120
When you see the food, you don't get the taste of it,

24
00:01:18,120 --> 00:01:19,880
but your mouth starts salivating.

25
00:01:19,880 --> 00:01:24,720
So even before you take the drug or take the alcohol,

26
00:01:24,720 --> 00:01:29,400
you already have this craving for it, just by seeing it.

27
00:01:29,400 --> 00:01:30,800
And that's important.

28
00:01:30,800 --> 00:01:33,600
That's going to be one of the first steps,

29
00:01:33,600 --> 00:01:35,480
or there's going to be one of the big steps

30
00:01:35,480 --> 00:01:40,840
in overcoming the addiction, is breaking that chain

31
00:01:40,840 --> 00:01:43,720
between seeing it and wanting it.

32
00:01:43,720 --> 00:01:45,920
If you can break it off there, then you

33
00:01:45,920 --> 00:01:50,360
can be said to really have cut the addiction off.

34
00:01:50,360 --> 00:01:55,760
The next thing is the memory that comes from seeing it.

35
00:01:55,760 --> 00:01:59,920
You remember that this is your drug, this is your alcohol,

36
00:01:59,920 --> 00:02:03,600
and you remember how good it is, and so on.

37
00:02:03,600 --> 00:02:07,280
And then after that comes the happy feeling.

38
00:02:07,280 --> 00:02:09,200
After the happy feeling comes the liking.

39
00:02:09,200 --> 00:02:11,560
After the liking comes the wanting.

40
00:02:11,560 --> 00:02:15,080
After the wanting comes the chasing.

41
00:02:15,080 --> 00:02:19,040
So we can break each of these steps up

42
00:02:19,040 --> 00:02:21,880
and focus on any one of them, whichever one becomes clear.

43
00:02:21,880 --> 00:02:22,880
And they'll become clear.

44
00:02:22,880 --> 00:02:25,200
Different ones will become clear at different times.

45
00:02:25,200 --> 00:02:27,120
At some times it's going to be clear.

46
00:02:27,120 --> 00:02:28,920
The object is going to be clear.

47
00:02:28,920 --> 00:02:31,040
So we see the object.

48
00:02:31,040 --> 00:02:32,960
That's the first way to cut it off.

49
00:02:32,960 --> 00:02:38,040
Say to yourself, seeing, seeing, then there's the memory

50
00:02:38,040 --> 00:02:41,400
or the feeling, the happy feeling that comes from it.

51
00:02:41,400 --> 00:02:44,040
Thinking, oh yes, this is happy.

52
00:02:44,040 --> 00:02:46,160
This is good, this is pleasurable.

53
00:02:46,160 --> 00:02:51,200
So saying to yourself, happy, happy, and liking, liking.

54
00:02:51,200 --> 00:02:52,960
The liking that comes from it, saying to yourself,

55
00:02:52,960 --> 00:02:56,560
liking, wanting, saying wanting, wanting, thinking about ways

56
00:02:56,560 --> 00:02:57,560
to get it, and so on.

57
00:02:57,560 --> 00:03:00,360
And even when you're taking the drug.

58
00:03:00,360 --> 00:03:02,920
And I've heard this helps with people who take cigarettes.

59
00:03:02,920 --> 00:03:04,440
Even when you're smoking the cigarette

60
00:03:04,440 --> 00:03:07,720
to be mindful, smoking, tasting, and so on, inhaling,

61
00:03:07,720 --> 00:03:12,680
or however, can help you to see the true nature

62
00:03:12,680 --> 00:03:14,080
of the addiction.

63
00:03:14,080 --> 00:03:15,920
And when you can break it all up like that,

64
00:03:15,920 --> 00:03:19,480
you actually teach your mind that there's really nothing

65
00:03:19,480 --> 00:03:21,040
really wonderful about this.

66
00:03:21,040 --> 00:03:25,160
There's nothing really attractive about this substance.

67
00:03:25,160 --> 00:03:29,920
And so this is theoretically how you do away with it.

68
00:03:29,920 --> 00:03:32,520
It does work to an extent.

69
00:03:32,520 --> 00:03:33,960
But with things like drugs and alcohol,

70
00:03:33,960 --> 00:03:37,320
I think there's other factors in play.

71
00:03:37,320 --> 00:03:41,320
First of all, the physical, or most of all,

72
00:03:41,320 --> 00:03:44,960
the physical addiction.

73
00:03:44,960 --> 00:03:48,720
Just like things with sensual desire or sexual desire,

74
00:03:48,720 --> 00:03:51,840
it's something that builds up in your brain,

75
00:03:51,840 --> 00:03:53,480
builds up in your body chemistry.

76
00:03:53,480 --> 00:03:55,840
So for instance, with cigarettes, the nicotine

77
00:03:55,840 --> 00:03:57,400
builds up in the brain.

78
00:03:57,400 --> 00:04:00,000
And you have to realize that you're probably

79
00:04:00,000 --> 00:04:04,400
going to be a hard press to do away with that.

80
00:04:04,400 --> 00:04:06,120
The other important thing that we have

81
00:04:06,120 --> 00:04:08,320
to keep in mind besides the meditation practice

82
00:04:08,320 --> 00:04:11,840
as far as breaking it up into pieces and catching each piece

83
00:04:11,840 --> 00:04:19,120
and acknowledging seeing or liking one by one by one

84
00:04:19,120 --> 00:04:23,320
is reminding yourself that these things are not you

85
00:04:23,320 --> 00:04:26,640
and are not yours and have really nothing to do with you.

86
00:04:26,640 --> 00:04:28,840
Because the addiction is going to come up.

87
00:04:28,840 --> 00:04:30,280
You're going to want the thing.

88
00:04:30,280 --> 00:04:34,600
You're going to have this intense desire for it

89
00:04:34,600 --> 00:04:37,720
that really wasn't wanted, wasn't expected.

90
00:04:37,720 --> 00:04:39,920
It comes, you don't know when it's going to come.

91
00:04:39,920 --> 00:04:43,120
And you can't control it.

92
00:04:43,120 --> 00:04:46,360
Realizing that not being able to control it

93
00:04:46,360 --> 00:04:48,320
is the essence of the addiction.

94
00:04:48,320 --> 00:04:50,560
That it's nothing to do with you.

95
00:04:50,560 --> 00:04:54,320
It's going to come up, not expecting it to go away,

96
00:04:54,320 --> 00:04:56,680
not trying to make it go away, not trying to find a trick

97
00:04:56,680 --> 00:04:57,720
to make it go away.

98
00:04:57,720 --> 00:05:01,320
You think of it like someone else's problem.

99
00:05:01,320 --> 00:05:03,640
And this is very much related to the meditation.

100
00:05:03,640 --> 00:05:07,120
This is what theoretically happens when you say to yourself,

101
00:05:07,120 --> 00:05:09,800
liking, liking, wanting, wanting, thinking, thinking,

102
00:05:09,800 --> 00:05:11,160
and so on.

103
00:05:11,160 --> 00:05:15,800
The problem is, it's a fight between the two.

104
00:05:15,800 --> 00:05:17,200
On the one hand, we really wanted.

105
00:05:17,200 --> 00:05:19,360
On the other hand, we really want to do away with it.

106
00:05:19,360 --> 00:05:23,680
So we have to, as well, remind ourselves

107
00:05:23,680 --> 00:05:27,200
that what we're seeing when we do this is the truth.

108
00:05:27,200 --> 00:05:29,880
When we say to ourselves, wanting, we see, oh my god,

109
00:05:29,880 --> 00:05:32,480
oh my god, I can't control it.

110
00:05:32,480 --> 00:05:33,960
It's too much for me.

111
00:05:33,960 --> 00:05:35,520
Realizing that that's the truth of it.

112
00:05:35,520 --> 00:05:37,960
Yeah, it's totally uncontrollable.

113
00:05:37,960 --> 00:05:41,400
It's not wrong that when you say to yourself, liking, liking,

114
00:05:41,400 --> 00:05:45,720
or wanting, wanting, or seeing, that you still end up

115
00:05:45,720 --> 00:05:47,760
wanting and the wanting just gets bigger and bigger

116
00:05:47,760 --> 00:05:49,400
and bigger, that's not a problem.

117
00:05:49,400 --> 00:05:51,520
What you have to do is see that that wanting doesn't mean

118
00:05:51,520 --> 00:05:52,160
anything.

119
00:05:52,160 --> 00:05:54,920
It has no inherent essence.

120
00:05:54,920 --> 00:05:56,240
It is just wanting.

121
00:05:56,240 --> 00:05:58,000
That's all it is.

122
00:05:58,000 --> 00:05:58,840
And it's not you.

123
00:05:58,840 --> 00:06:00,120
It's not yours.

124
00:06:00,120 --> 00:06:02,760
It's not under your control.

125
00:06:02,760 --> 00:06:04,480
One of the most important things in Buddhism

126
00:06:04,480 --> 00:06:07,000
is realizing this non-self, realizing

127
00:06:07,000 --> 00:06:09,320
that there's nothing inside of ourselves

128
00:06:09,320 --> 00:06:12,480
or in the world around us that we can really say is a soul,

129
00:06:12,480 --> 00:06:14,760
is a soul, is eye.

130
00:06:14,760 --> 00:06:16,480
Everything comes and goes.

131
00:06:16,480 --> 00:06:18,920
And when we can see like that, we don't cling to anything.

132
00:06:18,920 --> 00:06:24,160
There's no thought that I, me, mine, and we're

133
00:06:24,160 --> 00:06:25,840
able to float freely.

134
00:06:25,840 --> 00:06:27,760
And we're able to be free.

135
00:06:27,760 --> 00:06:30,160
It's like if someone yells at you,

136
00:06:30,160 --> 00:06:34,400
they're yelling at pieces of experience.

137
00:06:34,400 --> 00:06:35,360
They're yelling at body.

138
00:06:35,360 --> 00:06:38,320
They're yelling at the mind, who are they insulting?

139
00:06:38,320 --> 00:06:40,720
Are they insulting your hair?

140
00:06:40,720 --> 00:06:44,360
Are they insulting your skin or so on?

141
00:06:44,360 --> 00:06:45,840
And the same goes with addiction.

142
00:06:45,840 --> 00:06:48,120
I mean, who is it that wants?

143
00:06:48,120 --> 00:06:49,680
Is it my hair that wants?

144
00:06:49,680 --> 00:06:52,960
Is it my skin that wants or so on?

145
00:06:52,960 --> 00:06:54,240
Is it my brain that wants?

146
00:06:54,240 --> 00:06:56,120
I mean, is there all just pieces?

147
00:06:56,120 --> 00:07:01,680
Are there all just physical and mental phenomena

148
00:07:01,680 --> 00:07:03,320
that arise and cease?

149
00:07:03,320 --> 00:07:05,840
And I mean, to see that that's all this is.

150
00:07:05,840 --> 00:07:07,680
Not giving it power.

151
00:07:07,680 --> 00:07:10,480
I think that's one of the biggest things that comes from this

152
00:07:10,480 --> 00:07:12,840
is that we're not giving power to it.

153
00:07:12,840 --> 00:07:14,880
When you remind yourself it is what it is.

154
00:07:14,880 --> 00:07:15,960
It is what it is.

155
00:07:15,960 --> 00:07:17,120
You say to yourself liking.

156
00:07:17,120 --> 00:07:17,880
It is liking.

157
00:07:17,880 --> 00:07:18,560
It is liking.

158
00:07:18,560 --> 00:07:24,360
That's all it is, liking, liking, wanting, wanting, happy,

159
00:07:24,360 --> 00:07:27,320
and acknowledging the pleasure is that you don't give it

160
00:07:27,320 --> 00:07:29,880
the power that it used to have.

161
00:07:29,880 --> 00:07:32,680
When we feel guilty about it, when we feel angry about it,

162
00:07:32,680 --> 00:07:35,480
when we try to find a way to overcome it,

163
00:07:35,480 --> 00:07:41,520
when we try to forget about it, try to divert our own attention

164
00:07:41,520 --> 00:07:45,920
away from it, and so on, we're giving it power.

165
00:07:45,920 --> 00:07:47,840
We're saying that this is something dangerous.

166
00:07:47,840 --> 00:07:49,480
This is something terrible.

167
00:07:49,480 --> 00:07:50,680
This is something big.

168
00:07:50,680 --> 00:07:53,080
This is a problem.

169
00:07:53,080 --> 00:07:56,320
He's simply what this is a problem.

170
00:07:56,320 --> 00:07:58,800
When we say to ourselves, this, this, it is what it is.

171
00:07:58,800 --> 00:08:00,480
We're letting it be what it is.

172
00:08:00,480 --> 00:08:02,200
When we feel happy because of the addiction,

173
00:08:02,200 --> 00:08:03,320
it brings us pleasure.

174
00:08:03,320 --> 00:08:05,400
We should acknowledge that pleasure and not feel guilty

175
00:08:05,400 --> 00:08:05,720
about it.

176
00:08:05,720 --> 00:08:06,920
Yes, it's pleasure.

177
00:08:06,920 --> 00:08:08,320
It is what it is.

178
00:08:08,320 --> 00:08:10,040
And then we can see that, yeah, it is what it is,

179
00:08:10,040 --> 00:08:12,320
but it's not really anything special.

180
00:08:12,320 --> 00:08:13,640
It just is pleasure.

181
00:08:13,640 --> 00:08:16,520
It comes and it goes just like everything else.

182
00:08:16,520 --> 00:08:17,560
And that should help.

183
00:08:17,560 --> 00:08:21,600
But as I said, alcohol and drugs, you have to realize

184
00:08:21,600 --> 00:08:24,400
it's something that can be very physical.

185
00:08:24,400 --> 00:08:26,320
And it's something that has to take a long time.

186
00:08:26,320 --> 00:08:29,320
Don't expect meditation to get rid of the addiction,

187
00:08:29,320 --> 00:08:32,960
but expect it to allow you to deal with the addiction

188
00:08:32,960 --> 00:08:35,680
to the point where you don't need to use the substance,

189
00:08:35,680 --> 00:08:37,160
even though you're still addicted.

190
00:08:37,160 --> 00:08:40,160
You're able to live with the addiction.

191
00:08:40,160 --> 00:08:41,680
You're able to live with the wanting.

192
00:08:41,680 --> 00:08:45,520
I mean, not having to chase after the wanting.

193
00:08:45,520 --> 00:08:48,600
So I hope that helps and good luck.

194
00:08:48,600 --> 00:08:51,600
I can keep the questions coming.

