1
00:00:00,000 --> 00:00:06,480
We're walking back to our study of the Dhamma Bhagavad today we continue on

2
00:00:06,480 --> 00:00:18,720
with verses number 31 and 32 which goes follows

3
00:00:18,720 --> 00:00:31,200
a pamanda pratou miku pamade by the siva sanyo janang anun tulang dhanang yiva gachati

4
00:00:33,280 --> 00:00:40,720
number 31 and 32 a pamade ratou miku pamade by the siva

5
00:00:40,720 --> 00:00:49,520
a bhambo paliha na yat nibhana siva santiki

6
00:00:52,080 --> 00:00:57,760
so we'll go through this one one by one the first the first verse they're very similar

7
00:00:59,840 --> 00:01:08,800
the first verse is a pamanda ratou miku a monk who or a biku who delights in heathfulness

8
00:01:08,800 --> 00:01:16,560
pamade by a da siva and sees the fierceness or the danger in heedlessness

9
00:01:18,160 --> 00:01:28,560
sanjoyo janang anun tulang the fetters or the bond the binds you know those things that bind

10
00:01:29,440 --> 00:01:37,280
the fetters so that are fetters both large and small dhanang a giva gachati

11
00:01:37,280 --> 00:01:41,360
he goes through them like a burning fire

12
00:01:45,440 --> 00:01:53,520
so this verse was told in regards to a story about a particular monk and the story is quite

13
00:01:53,520 --> 00:02:00,320
short and doesn't have so much meaning but it helps us to understand or visualize the simile here

14
00:02:00,320 --> 00:02:08,080
because it was a simile that came to this monk who was practicing diligently

15
00:02:09,680 --> 00:02:14,320
he was practicing meditation and he was traveling along

16
00:02:16,720 --> 00:02:21,520
when suddenly a fire broke out and he

17
00:02:24,800 --> 00:02:28,800
he was walking through the forest and suddenly there was a fire in the whole forest was being consumed

18
00:02:28,800 --> 00:02:35,920
by fire and so he quickly went to a place of safety up on a large healed area

19
00:02:37,520 --> 00:02:42,160
where there were no trees and he watched the fire consuming the trees and he watched it

20
00:02:42,160 --> 00:02:45,840
no slowly through the forest destroying the entire forest

21
00:02:48,400 --> 00:02:52,880
and he took this as a kind of a metaphor for his practice and it was something of course very

22
00:02:52,880 --> 00:02:59,760
visual people who stare at fire that it becomes an image in their mind you can actually practice it

23
00:02:59,760 --> 00:03:02,960
as a meditation because it's something that sticks quite clearly in the mind

24
00:03:04,560 --> 00:03:09,280
and so as he was meditating this came to him came back to him this image of the fires just

25
00:03:09,280 --> 00:03:16,160
consuming everything and so he actually applied this to his practice and said to himself

26
00:03:16,160 --> 00:03:21,040
just as that fire goes through the forest so sure so too should I go through the the fireman

27
00:03:21,040 --> 00:03:26,880
and the Buddha came to him and confirmed that and said yes indeed

28
00:03:28,240 --> 00:03:33,760
monk who delights in heedfulness and sees the fearsomeness the danger in heedlessness

29
00:03:34,480 --> 00:03:40,800
burns up the fetters large and small just as a fire burns up the tree burns up the fire

30
00:03:43,760 --> 00:03:48,880
the second verse was told based on a completely different story of a different sort

31
00:03:48,880 --> 00:03:56,640
and also very fairly short but the meaning is very similar up the mother it will be koopamandi

32
00:03:56,640 --> 00:04:02,640
by a dusty mother it's the same a monk who delights in heedfulness and sees the danger in heedlessness

33
00:04:03,360 --> 00:04:14,400
a bumble buddy on yet he is unable to fall away or he is un it's not possible for such a person

34
00:04:14,400 --> 00:04:26,400
to fall away or to go to waste or to to waste away nibana say what santike indeed such a person

35
00:04:26,400 --> 00:04:35,360
is very close to nibana or close to freedom and the story here is another simple story with

36
00:04:36,720 --> 00:04:44,240
sort of an interesting idea to it the story is of a monk who was well known for

37
00:04:44,240 --> 00:04:50,960
his name was nigamatisa he was well known for his frugality his his fewness of wishes

38
00:04:50,960 --> 00:05:00,400
and his strict behavior not seeking out luxury or special things and so even though he was

39
00:05:00,400 --> 00:05:03,760
quite near to saw what he where the Buddha was staying and where there were so many

40
00:05:03,760 --> 00:05:14,720
faithful lay disciples who were providing the monks with so much luxury and so much

41
00:05:16,080 --> 00:05:24,800
affluence or opulence you may say he never came to accept the gifts of these people and so

42
00:05:24,800 --> 00:05:29,040
the monks watched him and they they really watched this behavior that he would go for arms

43
00:05:29,040 --> 00:05:35,840
round and every day he would go to his family the village where he grew up which was fairly close

44
00:05:35,840 --> 00:05:41,920
to sawity or a bit away from sawity and he would go and collect food from them and then go back

45
00:05:41,920 --> 00:05:46,960
and live in his forest and he never came down whenever there was an invitation or or a meal

46
00:05:49,040 --> 00:05:53,200
whenever there was there was the offering of requisites he would never take part in the offer

47
00:05:53,200 --> 00:05:57,680
never come to receive these things and so the monks kind of got the feeling that he was clinging to

48
00:05:57,680 --> 00:06:03,280
his family and he was just spending all his time amongst his family not getting involved in

49
00:06:03,280 --> 00:06:09,200
Buddhist culture Buddhist culture that centered around sawity and so they told us to the Buddha

50
00:06:09,200 --> 00:06:12,400
and the Buddha called him up and said is it true that you're acting like this that you're not

51
00:06:12,400 --> 00:06:17,520
getting involved in the society or you're not you're not coming to accept the gifts of these

52
00:06:17,520 --> 00:06:24,320
faithful people and he said well and that you're you're instead clinging to your family and getting

53
00:06:24,320 --> 00:06:32,160
involved in in and getting caught up in your family affairs and he said well it's true that I'm

54
00:06:32,160 --> 00:06:39,040
not coming for these invitations but it's not true at all that I'm caught up in in my family affairs

55
00:06:39,040 --> 00:06:44,560
or caught on attached to my family these I think to myself these people give me enough food to

56
00:06:44,560 --> 00:06:50,000
survive the food that I get from the village is enough to survive and and that's it you know

57
00:06:50,000 --> 00:06:56,160
and the duty for the food is finished why should I go to seek out more I get enough food to

58
00:06:56,160 --> 00:07:01,920
survive and then I go back and and practice meditation on my own and the Buddha actually

59
00:07:03,440 --> 00:07:10,160
praised him for this and as a result said this is this is how a this is how a student of mine

60
00:07:10,160 --> 00:07:16,640
should act and he told the story of the pastor he himself and one of his past lives was

61
00:07:16,640 --> 00:07:24,560
a parrot and was very it was just a story of of contentment where he was content in the same way and

62
00:07:24,560 --> 00:07:31,040
not needing to go anywhere just taking what came to him not seeking out further further

63
00:07:32,160 --> 00:07:38,400
requisites or further material gain and so then he said this verse such a monk is

64
00:07:39,920 --> 00:07:45,440
not not declining it's not he's not a backsliding person it's not a person who is giving up the

65
00:07:45,440 --> 00:07:51,520
training because they do this the person who has no interest in Buddhist culture you know get

66
00:07:51,520 --> 00:07:57,840
taking part in the society that's a person actually is quite close to Nipana for their

67
00:07:57,840 --> 00:08:05,280
frugality and this is a common theme actually that we can talk about the but the verses

68
00:08:05,280 --> 00:08:12,240
themselves are quite similar and as with many of the stories they relate much more to the general

69
00:08:12,240 --> 00:08:18,240
practice than they do to the actual the verses relate much more to our actual practice than they

70
00:08:18,240 --> 00:08:28,160
do to the stories themselves so it's a way of using some very ordinary things to to give a

71
00:08:28,160 --> 00:08:39,120
teaching or to to use in our meditation practice the main theme as the theme of this whole chapter

72
00:08:39,120 --> 00:08:48,240
has been is heedfulness or or vigilance which we've we've explained as having mindfulness always

73
00:08:48,240 --> 00:08:56,800
having mindfulness every moment moment by moment being mindful not getting angry and having a

74
00:08:56,800 --> 00:09:03,760
level head having a balanced mind having your mind clearly aware of things neither partial

75
00:09:03,760 --> 00:09:10,640
towards or against something having your mind stable and and clearly perceiving the objects of

76
00:09:10,640 --> 00:09:18,000
awareness and slowly giving up your your your your greed so basically being mindful and giving up

77
00:09:18,000 --> 00:09:26,080
partial mindfulness is considered to be the essence of this this teaching on heatfulness so the

78
00:09:26,080 --> 00:09:35,040
point of our practice should be twofold to develop this this state of mind that is mindful

79
00:09:35,040 --> 00:09:40,720
that is vigilant and to avoid the state which is negligent this is a common theme in the Buddhist

80
00:09:40,720 --> 00:09:45,440
teaching that you'll see this twofold teaching and the Buddha even explained it in detail he said

81
00:09:45,440 --> 00:09:52,560
this is how you would teach a horse if a if you have a horse that is untrained and you want to

82
00:09:52,560 --> 00:10:00,000
train it then first of all you train it teaching it you train it with a a kind lesson you're kind

83
00:10:00,000 --> 00:10:06,800
to the horse you encourage it on you give it carrots you use a carrot to teach giving it rewards

84
00:10:06,800 --> 00:10:12,960
when it does good things and teaching it in a kind way if it doesn't react to that then you

85
00:10:12,960 --> 00:10:19,280
teach in the harsh way you use the stick and if it doesn't react to either of those then you kill

86
00:10:19,280 --> 00:10:27,440
the horse and so the Buddha said this is how I teach my disciples I teach them kindly at first

87
00:10:27,440 --> 00:10:31,840
and then I teach them harshly if that doesn't work and if even that doesn't work then I kill them

88
00:10:33,760 --> 00:10:38,640
and so he explained what this means first of all the first way to teach is to teach good things

89
00:10:38,640 --> 00:10:43,360
what are the good things what is what are wholesome things what are wholesome types of behavior

90
00:10:43,360 --> 00:10:47,680
when the hopes that they'll catch on by themselves that without having to reprimand them or

91
00:10:47,680 --> 00:10:53,520
scold them that they will be inspired and take up this teaching take up the practice for themselves

92
00:10:53,520 --> 00:10:57,920
if that doesn't work then you teach them one of the bad things and you say this is bad this is

93
00:10:57,920 --> 00:11:02,880
wrong it's as wrong as a means of scolding them as a means of stopping them from backside

94
00:11:02,880 --> 00:11:11,040
stopping them from falling into unwholesome this and if that doesn't work what he means by killing

95
00:11:11,040 --> 00:11:15,760
them is you stop teaching you give up teaching them you you think that this person is useless

96
00:11:15,760 --> 00:11:24,720
and you take them out of your realm of instruction and this is what it means in the Buddha's

97
00:11:24,720 --> 00:11:31,680
teaching to kill someone and so here we have a teaching of this sort with the Buddha on the one

98
00:11:31,680 --> 00:11:37,920
hand is explaining the good thing being being up Amanda is something that we should delight

99
00:11:38,640 --> 00:11:43,360
we should delight and heedfulness and really see the benefit of it see that every moment that

100
00:11:43,360 --> 00:11:50,000
your mindful your mind is clear like pure water like the pure nature around us free from

101
00:11:50,000 --> 00:11:55,440
and free from pollution free from defilement when you go up in the forest and you see a pure

102
00:11:55,440 --> 00:12:01,920
waterfall we should delight in our mindfulness in the same way it's like this forest pool or this

103
00:12:01,920 --> 00:12:09,520
forest waterfall or this clear water that is unpolluted and solid untainted by any sort of

104
00:12:09,520 --> 00:12:19,760
artificial any sort of foreign element when we practice mindfulness we we feel somehow

105
00:12:21,120 --> 00:12:26,400
like this still forest pool and like the waterfall feel quite natural and you can feel the same

106
00:12:26,400 --> 00:12:32,560
sort of delight as an ordinary person would have for such a sight when they go off into the forest

107
00:12:32,560 --> 00:12:37,440
and see the waterfall mostly people that's the closest they can get to this clarity

108
00:12:37,440 --> 00:12:45,200
well this by seeing something so pure so so natural when we practice when we develop our

109
00:12:45,200 --> 00:12:50,000
mindfulness here in the forest you somehow begin to feel very much like the natural forest

110
00:12:50,000 --> 00:12:56,000
your mind becomes very natural and very pure and so this is something that we can delight in

111
00:12:56,000 --> 00:13:00,640
of course in the beginning in our meditation it seems the opposite it seems that what we delight

112
00:13:00,640 --> 00:13:08,720
in is negligence what is most delightful is the following after the defilements following after

113
00:13:08,720 --> 00:13:16,000
our desires what is delightful stories and fantasies and beautiful sights and sounds and so

114
00:13:16,000 --> 00:13:22,960
objects at a sense very difficult to go to set is very difficult to find delight in

115
00:13:22,960 --> 00:13:27,680
mindfulness and so this is something that has to be impressed upon us here we have two verses

116
00:13:27,680 --> 00:13:34,720
that explain the need for it an ordinary person is going in the opposite direction this is why

117
00:13:34,720 --> 00:13:42,960
they are unable to have a balanced mind why they're unable to keep their minds above the

118
00:13:42,960 --> 00:13:47,920
vicissitudes of life when good things come chase after them and bad things come they run away

119
00:13:47,920 --> 00:13:54,000
from them they're always in a state of greater and greater stress and and a sad inspection

120
00:13:54,000 --> 00:13:59,600
so the Buddha's teaching is something that is quite different from the way of the world the

121
00:13:59,600 --> 00:14:05,280
development and not only development but finding delight in the opposite way that ordinary people

122
00:14:05,280 --> 00:14:11,360
would find delight coming to incline our minds just as a tree would incline in one direction or

123
00:14:11,360 --> 00:14:17,440
the other where our minds are ordinarily inclining towards negligence we teach ourselves to incline

124
00:14:17,440 --> 00:14:22,160
towards needfulness something that we should delight in so this is the teaching on good things

125
00:14:22,160 --> 00:14:29,920
the teaching on bad things is seeing the danger in that which we ordinarily delight ordinarily we

126
00:14:29,920 --> 00:14:37,200
can't see the danger we cling to these things and we only see the gratification of the benefit

127
00:14:37,200 --> 00:14:42,320
of that we get some kind of a pleasure in the present moment and we don't see the bait

128
00:14:42,960 --> 00:14:48,320
or we don't see the hook behind the bait we're unable to see the development and the cultivation

129
00:14:48,320 --> 00:14:54,560
of addiction we're unable to see the imbalance in the mind and the the stress and the the

130
00:14:55,360 --> 00:15:02,160
power that is essential or inherent in clinging when you cling to something you're creating a

131
00:15:02,160 --> 00:15:09,840
power in your mind a power of addiction a powerful habit creating a habit in mind that that creates

132
00:15:09,840 --> 00:15:16,320
requirement that creates the the need for such a thing in order to be happy it's like bringing

133
00:15:16,320 --> 00:15:23,360
you out of balance seeing the danger that that that comes from partiality because being

134
00:15:23,360 --> 00:15:28,320
partial towards something takes you out of balance in favor of it and makes you more and more

135
00:15:28,320 --> 00:15:35,440
averse to the opposite more and more averse to that which is not in in your estimation of

136
00:15:36,400 --> 00:15:41,360
or within your estimation of what is what is beneficial what is good and what is pleasant

137
00:15:41,360 --> 00:15:48,080
we can talk about in the many dangers of these things and this is some this is the other part

138
00:15:48,080 --> 00:15:53,360
of the Buddha's teaching is talking about the negative sign the fact that it needs us to fight

139
00:15:53,360 --> 00:15:58,880
with one another why we go to war why there is such a thing as economy or why there is such a

140
00:15:58,880 --> 00:16:04,960
thing as laws while of these things exist is because of our mainly because of our property

141
00:16:04,960 --> 00:16:11,840
our desire for for material gain they say there's enough resources to feed and and clothe all the

142
00:16:11,840 --> 00:16:18,080
people in the world but because of our negligence because of our inability to be content with

143
00:16:18,080 --> 00:16:24,000
what we have we're taking from others and withholding from others stealing from others

144
00:16:24,000 --> 00:16:30,640
cheating others all the time creating great stress and suffering and any time that we can't get

145
00:16:30,640 --> 00:16:37,280
what we want anytime we're denying the objects of our pleasure and it gives rise to it can give rise

146
00:16:37,280 --> 00:16:43,200
to anger and it can destroy friendships it does destroy friendships it destroys families

147
00:16:44,800 --> 00:16:49,680
the whole issue now of divorce how people become married and then can't stay married and now

148
00:16:49,680 --> 00:16:55,360
the common things for people to be married a short time and then decided they have other

149
00:16:55,360 --> 00:17:01,680
interests and they'll pursue a second marriage or third marriage and so on never being satisfied

150
00:17:01,680 --> 00:17:08,560
never being content and as a result experiencing great suffering and and because they can't see

151
00:17:08,560 --> 00:17:16,160
this the danger in it they they're unable to remember unable to keep in their mind or not

152
00:17:16,160 --> 00:17:23,840
willing to keep in their mind because they delight so much in the objects of pleasure they forget

153
00:17:23,840 --> 00:17:28,800
quite easily the suffering that they've been through so people will will claim that they have a

154
00:17:28,800 --> 00:17:33,920
happy life and so on when they've actually gone through an incredible amount of suffering

155
00:17:33,920 --> 00:17:38,880
because they're unable to remember and so as a result they continuously seeking after more and

156
00:17:38,880 --> 00:17:46,720
more pleasure unable to remember or recall or realized the suffering that they've had to bear

157
00:17:46,720 --> 00:17:51,760
as a result of it so often they'll think that this is the best that one can expect this pleasure

158
00:17:51,760 --> 00:18:02,560
pain pleasure pain the dichotomy like the yoyo effect of the pendulum effect swinging back and

159
00:18:02,560 --> 00:18:08,400
forth and creating more and more stress in the mind it's not half half either the more you cling

160
00:18:09,360 --> 00:18:14,480
the more partial you are to things the more stress you have in general so even though you might

161
00:18:14,480 --> 00:18:18,880
get all of the things that you want the stress that's building up in your mind can be quite

162
00:18:18,880 --> 00:18:27,200
intense can create headaches can create tension in the muscles and so on tension in your limbs

163
00:18:27,920 --> 00:18:33,120
they can create sickness in the body because they're so much stress even good stress even the

164
00:18:33,120 --> 00:18:40,800
desire side can create great stress in the body and in the mind and it winds you up and winds

165
00:18:40,800 --> 00:18:47,600
you up into aren't able to do so that's the two-sided teaching that's the first half of both

166
00:18:47,600 --> 00:18:55,200
of these verses now where they differ the first the first verses is quite interesting

167
00:18:56,160 --> 00:19:01,360
for us to think about and remember in our practice about how the practice should progress but

168
00:19:01,360 --> 00:19:06,720
our practice is not going to be something that we're going to complete in one day or in one course

169
00:19:06,720 --> 00:19:13,200
or maybe even in one lifetime we can try our best and the Buddha said if you really put your

170
00:19:13,200 --> 00:19:17,520
heart into it in seven years you can become an hour on or even in seven days you can become an

171
00:19:17,520 --> 00:19:23,360
hour on but it may be that that doesn't happen perhaps maybe that because of our karma because of

172
00:19:23,360 --> 00:19:30,640
our duties because of our future which is uncertain that we're unable to make it the way we

173
00:19:30,640 --> 00:19:39,040
should look at our practice is this steady kind of like a juggernaut that that destroys everything

174
00:19:39,040 --> 00:19:45,600
in its path no matter what comes what comes to block it from its path the juggernaut will continue

175
00:19:45,600 --> 00:19:50,480
in its path and it doesn't have to go quickly it doesn't have to rush because it knows that

176
00:19:50,480 --> 00:19:55,680
nothing can stand in its way this is like like the fire in the forest doesn't immediately burn up

177
00:19:55,680 --> 00:20:03,280
the trees but nothing can stop it when it's taken hold of the forest all there is to do is to

178
00:20:03,280 --> 00:20:12,000
wait for it to destroy all the trees so the problem is that often people come into the practice

179
00:20:12,000 --> 00:20:15,920
with the idea that they're going to suddenly have some profound realization and all of their

180
00:20:15,920 --> 00:20:21,040
troubles are going to disappear that suddenly they're going to become enlightened we hear about

181
00:20:21,040 --> 00:20:27,200
becoming enlightened and we think that it's a momentary thing that didn't require or that rather

182
00:20:27,200 --> 00:20:33,520
than happening gradually can do a work work work as hard as you can and and there's an explosion

183
00:20:33,520 --> 00:20:40,960
of enlightenment suddenly and so we often mistake our our realizations for this experience of enlightenment

184
00:20:40,960 --> 00:20:45,760
we'll have some epiphany and we'll become complacent that to result because we'll think we may

185
00:20:45,760 --> 00:20:50,800
know this is why we consider this one of the uplicky laces one of the uplicky laces is in

186
00:20:50,800 --> 00:20:57,920
Jan as I was talking about before even getting even even having good realization so that's one

187
00:20:57,920 --> 00:21:03,600
of the meditators said today they they coming back and doing the course again they're surprised to

188
00:21:03,600 --> 00:21:08,320
find the old defilements still there when they thought that they had rooted the mouth already

189
00:21:09,120 --> 00:21:18,000
and so the thing that we what we should keep in mind is how slow and inexorable our practice will

190
00:21:18,000 --> 00:21:23,360
be our practice will not be quick and the quicker we want to be the slower the results will become

191
00:21:23,360 --> 00:21:28,640
because then there's more craving and more clinging we have to work through the defilements in

192
00:21:28,640 --> 00:21:35,040
our mind one by one by one piece by piece little by little by little so the more you practice

193
00:21:35,040 --> 00:21:39,920
the slower you realize that it is and the more realistic you become about the results that you

194
00:21:39,920 --> 00:21:45,280
should expect the more patient you become we should be patient like the fire or patient like the

195
00:21:45,280 --> 00:21:53,200
juggernaut that has confident in their progress but not ambitious and not over confident

196
00:21:53,200 --> 00:22:00,160
and not hurrying so it says here how there's nothing stays in the way of the fire that whether

197
00:22:00,160 --> 00:22:06,320
they be big big trees or small trees that the fire destroys obliterates them all in the same way

198
00:22:06,320 --> 00:22:13,280
our practice will allow us to get rid of everything all of our attachments but it happens

199
00:22:13,280 --> 00:22:19,120
as it's going to happen it happens inexorably like a fire burning so we should be very patient

200
00:22:19,120 --> 00:22:26,720
in our practice and methodical as well part of the teaching on heatfulness is being methodical

201
00:22:26,720 --> 00:22:34,720
you know we should not hurry ahead or not skip thing not not skip any of our

202
00:22:34,720 --> 00:22:39,280
defilements thinking that's only a minor defilement we should see the danger in every little thing

203
00:22:39,280 --> 00:22:46,000
and this one is an Anumate who went to Yandasa seeing the fearsomeness the danger and even the

204
00:22:46,000 --> 00:22:55,360
smallest offense or the smallest wrong in our mind and in this way we would be like the fire

205
00:22:55,360 --> 00:23:03,760
that that inexorably destroys all of the defilements in the mind this is the first one the second

206
00:23:03,760 --> 00:23:08,640
one is sort of this reassurance deals much more with the confidence that we should have is

207
00:23:08,640 --> 00:23:13,680
limited to this that if we're practicing in the right way even though it may seem at times

208
00:23:13,680 --> 00:23:18,160
nothing's happening or it may seem at times that we still have so many defilements to deal with

209
00:23:18,720 --> 00:23:25,040
we should understand and we should be clear in our minds that there is no possibility

210
00:23:25,040 --> 00:23:31,760
for us to fall away the Buddha said abaabuabuabuabarihanaya because we understand the nature of

211
00:23:31,760 --> 00:23:38,160
mindfulness because we've experienced this clear state of mind we have to remind ourselves

212
00:23:38,160 --> 00:23:42,640
this is a clear state of mind this mind is a pure state of mind this is what we know when we

213
00:23:42,640 --> 00:23:47,680
when we use it every time we're mindful we can see the purity we can see the clarity of the mind

214
00:23:48,960 --> 00:23:53,600
the problem is that then we we have all these defilements come up and we become discouraged

215
00:23:53,600 --> 00:23:59,360
thinking that it's not doing it but the truth is it can't help but do something whether we

216
00:23:59,360 --> 00:24:05,200
see it or not whether it becomes evident to us in in the practice or not we have to think that

217
00:24:05,200 --> 00:24:11,840
there's no possibility for this not to affect us in a good way because it's a clear state of

218
00:24:11,840 --> 00:24:18,640
mind if you get angry you see that the anger changes you see that how we've developed these habits

219
00:24:19,200 --> 00:24:23,920
if you become greedy or if you like something it will make you want it more and more and more

220
00:24:24,640 --> 00:24:28,880
but it's not something that happens overnight you can't be mindful one day and expect the next

221
00:24:28,880 --> 00:24:33,920
day to be enlightened or even to be to be happy actually you might still be miserable the next

222
00:24:33,920 --> 00:24:37,600
day and then you wonder what was what good was yesterday I practiced and I was still miserable

223
00:24:40,000 --> 00:24:45,680
so the the practice we have to see it as something gradual the Buddha said just as the ocean

224
00:24:46,240 --> 00:24:51,440
slopes gradually and doesn't immediately drop off so to the Buddha's teaching is a gradual

225
00:24:51,440 --> 00:24:58,880
teaching one that goes gradually and requires patience and requires perseverance requires one to be

226
00:24:58,880 --> 00:25:07,760
steadfast like the fire so the Buddha said we should never be discouraged thinking that it's not

227
00:25:07,760 --> 00:25:15,120
coming quickly or thinking that we're gaining nothing just as drops go into the cup eventually it

228
00:25:15,120 --> 00:25:21,680
fills up and overflows the Buddha said gave his reassurance here that it's not possible for

229
00:25:21,680 --> 00:25:27,680
such a person to fall away because they're developing good habits the key here is that every

230
00:25:27,680 --> 00:25:34,560
mind state every moment that we react or interact with an object we're changing who we are

231
00:25:34,560 --> 00:25:42,160
we're changing our habits we're changing the way the mind works every moment and so every moment

232
00:25:42,160 --> 00:25:47,680
of mindfulness we are changing who we are we're not static beings everything changes us every

233
00:25:47,680 --> 00:25:54,960
instant changes who we are and so every moment we have the ability to shape our future and to

234
00:25:54,960 --> 00:26:02,480
change the direction of our lives the Buddha said not only do they not fall away but even further

235
00:26:02,480 --> 00:26:10,720
more such a person nibhana savas and tikinis is quite close to nibhana so as our practice progresses

236
00:26:11,280 --> 00:26:16,480
the sign that we're becoming coming closer to nibhana closer to freedom is that our

237
00:26:17,840 --> 00:26:23,680
is this this keyfulness that the mind begins to delight begins to leap out at the chance to be

238
00:26:23,680 --> 00:26:29,040
mindful in the beginning we have to push ourselves to be mindful and stop ourselves from clinging

239
00:26:29,040 --> 00:26:36,480
to thing once we develop in the practice and and come closer to freedom closer to the realization

240
00:26:36,480 --> 00:26:43,120
in nibhana the mind will be the opposite it will delight in heedfulness and it will cringe

241
00:26:43,120 --> 00:26:53,040
at the idea of defilement it will move away from the for many desire for the for clinging or

242
00:26:53,040 --> 00:27:01,360
for for being for a version towards thing so our practice should be in this regard and we shouldn't

243
00:27:01,360 --> 00:27:06,880
we shouldn't we shouldn't have any doubt about this because we can see clearly what what

244
00:27:06,880 --> 00:27:12,960
mindfulness does what vigilance does what mindfulness does to our minds so the more that we can

245
00:27:12,960 --> 00:27:17,760
create in our minds the more the more we can develop this habit the closer we become to the

246
00:27:17,760 --> 00:27:23,920
the closer we come to nibhana until our mind delights in it and eventually inclines towards it

247
00:27:24,560 --> 00:27:30,960
and our practice becomes a work of its own and there's nothing more for us to do because of the

248
00:27:30,960 --> 00:27:37,040
inclination of the mind at which point the mind enters into nibhana and analysis

249
00:27:37,040 --> 00:27:44,000
freedom from suffering so just another short teaching this is two verses and we've now finished

250
00:27:44,000 --> 00:27:54,000
the upper mind about that so thank you all for listening and now we continue with our practice

