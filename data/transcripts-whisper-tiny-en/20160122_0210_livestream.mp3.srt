1
00:00:00,000 --> 00:00:07,000
Here, it's probably the broadcast just stopped.

2
00:00:07,000 --> 00:00:18,000
Here I am going on and I just talked quite a bit actually.

3
00:00:18,000 --> 00:00:44,000
Let's try to note that in the back middle.

4
00:00:44,000 --> 00:00:48,000
What's the last thing anyone heard?

5
00:00:48,000 --> 00:00:56,000
I was just talking about second life.

6
00:00:56,000 --> 00:01:02,000
Ten minutes of it.

7
00:01:02,000 --> 00:01:13,000
So to sum up what I just said and nobody heard but me and the mice in my room.

8
00:01:13,000 --> 00:01:21,000
Second life is an interesting, interesting platform.

9
00:01:21,000 --> 00:01:28,000
It might be hard for some people.

10
00:01:28,000 --> 00:01:33,000
But it might not work for some computers.

11
00:01:33,000 --> 00:01:37,000
But I'll try to upload it to YouTube afterwards.

12
00:01:37,000 --> 00:01:39,000
That's the TEDx talk.

13
00:01:39,000 --> 00:01:43,000
Well, first of all, I'm not even sure they're going to choose me, but it looks good.

14
00:01:43,000 --> 00:01:48,000
As long as I don't really botch the interviewer.

15
00:01:48,000 --> 00:01:52,000
It doesn't seem too boring to them when I'm talking about.

16
00:01:52,000 --> 00:01:54,000
Probably get in.

17
00:01:54,000 --> 00:01:59,000
I don't.

18
00:01:59,000 --> 00:02:02,000
But I don't know that I'll be given a recording.

19
00:02:02,000 --> 00:02:07,000
I'm assuming that they record them and put them up on their TEDx channel.

20
00:02:07,000 --> 00:02:13,000
Whatever that is.

21
00:02:13,000 --> 00:02:16,000
Anyway, so yeah, Saturday, come on by.

22
00:02:16,000 --> 00:02:27,000
If you can get second life working.

23
00:02:27,000 --> 00:02:40,000
But that's all.

24
00:02:40,000 --> 00:02:43,000
I'm going to sign off now.

25
00:02:43,000 --> 00:02:46,000
It's been a long day, as I said.

26
00:02:46,000 --> 00:02:51,000
I'll be back tomorrow and then Saturday.

27
00:02:51,000 --> 00:02:54,000
I'll do this second life thing.

28
00:02:54,000 --> 00:02:58,000
That's visual.

29
00:02:58,000 --> 00:03:25,000
Okay.

