1
00:00:00,000 --> 00:00:04,000
is the bad result of that bad karma.

2
00:00:04,000 --> 00:00:09,000
Of course, it can become new karma by feeling guilty and getting upset about it.

3
00:00:09,000 --> 00:00:17,000
But having to remember, there's the result of doing these things.

4
00:00:17,000 --> 00:00:24,000
Having to experience unpleasantness.

5
00:00:24,000 --> 00:00:28,000
A meditator really understands the law of karma.

6
00:00:28,000 --> 00:00:32,000
Someone who's come to meditate fresh, never meditated before,

7
00:00:32,000 --> 00:00:33,000
realizes it.

8
00:00:33,000 --> 00:00:36,000
When I first started meditating, it was a shock to me.

9
00:00:36,000 --> 00:00:39,000
I realized what any had I had been.

10
00:00:39,000 --> 00:00:42,000
How foolish, how ignorant.

11
00:00:42,000 --> 00:00:45,000
All the things that I had done.

12
00:00:45,000 --> 00:00:50,000
Just wave after wave of junk.

13
00:00:50,000 --> 00:00:52,000
I was like, oh no.

14
00:00:52,000 --> 00:00:55,000
I had been so blind.

15
00:00:55,000 --> 00:00:57,000
It wasn't like I was being brainwashed.

16
00:00:57,000 --> 00:01:02,000
I was really just looking, opening up my mind for the first time and seeing.

17
00:01:02,000 --> 00:01:04,000
What if I put in here?

18
00:01:04,000 --> 00:01:08,000
What if I've been carrying around with me?

19
00:01:08,000 --> 00:01:13,000
That's what karma is.

20
00:01:13,000 --> 00:01:14,000
Come and do.

21
00:01:14,000 --> 00:01:16,000
We are tied to our karma.

22
00:01:16,000 --> 00:01:17,000
Come and you only.

23
00:01:17,000 --> 00:01:19,000
We are born from our karma.

24
00:01:19,000 --> 00:01:21,000
Come and put this, I don't know.

25
00:01:21,000 --> 00:01:22,000
Put this, I don't know.

26
00:01:22,000 --> 00:01:23,000
Put this, I don't know.

27
00:01:23,000 --> 00:01:47,000
It says resort, but it's our dwelling, I suppose.

