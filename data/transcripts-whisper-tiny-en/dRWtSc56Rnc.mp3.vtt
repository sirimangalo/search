WEBVTT

00:00.000 --> 00:07.000
Good evening, everyone. Welcome to our evening gathering.

00:14.000 --> 00:18.000
Tonight, I promise to talk about the middle way.

00:18.000 --> 00:34.000
Question was asked about what exactly the middle way means.

00:34.000 --> 00:40.000
I think it's important to talk about, first of all, because it's often

00:40.000 --> 00:53.000
this understood easily abused, but also because it does in many ways

00:53.000 --> 01:02.000
capture the essence of what we're trying to do if understood properly.

01:02.000 --> 01:07.000
It's a little bit misleading when we talk about the middle way,

01:07.000 --> 01:16.000
because it seems to sound like there's a happy medium in between two things

01:16.000 --> 01:25.000
where you don't go extreme on anything, but on the other hand you don't give up anything.

01:25.000 --> 01:33.000
And that's not generally speaking if I can speak for the Buddha, which of course is

01:33.000 --> 01:38.000
a frog with some danger.

01:38.000 --> 01:43.000
It's not generally how the middle way is understood.

01:43.000 --> 01:47.000
Take, for example, the first teaching of the Buddha, this is the most famous,

01:47.000 --> 01:58.000
the most well-known usage of this idea of the middle way.

01:58.000 --> 02:10.000
So, first of all, some background we're talking about.

02:10.000 --> 02:21.000
It's a talk given to five ascetics, five

02:21.000 --> 02:31.000
Hindu or Indian ascetics who had left Brahman society because they, like many before them,

02:31.000 --> 02:41.000
thought to themselves and realized that there was something quite wrong about the indulgence

02:41.000 --> 02:49.000
in central pleasures, so religion that was associated with sensuality

02:49.000 --> 02:54.000
where the priests and the

02:54.000 --> 03:04.000
supplicants or the applicants, those who practiced the sacrificial rituals

03:04.000 --> 03:06.000
that they really weren't getting anything out of it.

03:06.000 --> 03:13.000
So there was this movement in the time of the Buddha to leave behind

03:13.000 --> 03:18.000
and turn away from central pleasure with the realization that it was an addiction.

03:18.000 --> 03:23.000
It was something that kept you tied to Samsara.

03:23.000 --> 03:28.000
It was a strong, sort of mystical tradition.

03:28.000 --> 03:36.000
A lot of talk about rising above and getting beyond Samsara.

03:36.000 --> 03:41.000
Different ideas of what it meant, about to see through the illusion of sensuality

03:41.000 --> 03:50.000
and how to break free from the bonds of sensuality.

03:50.000 --> 04:04.000
And so what you had was a strong broad movement to go to the other extreme.

04:04.000 --> 04:06.000
Of course, it wasn't thought of this way.

04:06.000 --> 04:11.000
It was when you reject something and so the other thing is the right thing.

04:11.000 --> 04:21.000
The opposite for lack of a better word is the right thing.

04:21.000 --> 04:27.000
And so you had a lot of people in the time of the Buddha torturing themselves

04:27.000 --> 04:33.000
inflicting pain upon themselves, thinking that if pleasure is the wrong way

04:33.000 --> 04:38.000
then of course the right way must be whatever the opposite is pain.

04:38.000 --> 04:45.000
So if you torture yourself, you could free yourself from any desire for sensuality.

04:45.000 --> 04:52.000
It's kind of odd when you think about it, but there's a non- sort of logic to it.

04:52.000 --> 04:55.000
You beat it out of yourself.

04:55.000 --> 04:58.000
It's a moment we think this when we punish people.

04:58.000 --> 05:04.000
We punish children who are following their cravings by hitting them.

05:04.000 --> 05:08.000
We think if we give them pain somehow it'll make them want pleasure less.

05:08.000 --> 05:15.000
It's kind of an odd sort of reasoning, but we have this sort of reasoning.

05:15.000 --> 05:20.000
Say, if you hurt someone, have some reason be less inclined towards pleasure,

05:20.000 --> 05:24.000
which of course seems to be the opposite.

05:24.000 --> 05:28.000
It makes one want to rethink the whole idea of punishment,

05:28.000 --> 05:30.000
but it's of course our end-to-punishment.

05:30.000 --> 05:33.000
It's a very bad karma.

05:41.000 --> 05:44.000
But so this was the wrong view.

05:44.000 --> 05:46.000
This was the wrong idea.

05:46.000 --> 05:49.000
And when the Buddha came to teach the middle ways,

05:49.000 --> 05:54.000
then Vaimi, Bhikkhuvai, Anda, there are these two Anda.

05:54.000 --> 05:58.000
These two Anda is like the end of something, the extreme.

05:58.000 --> 06:02.000
It doesn't usually mean extreme, but it's used in that way here.

06:02.000 --> 06:10.000
It means the extremity, the very end of some things.

06:10.000 --> 06:16.000
They have two ends of the spectrum, basically.

06:16.000 --> 06:21.000
And so it sounds like he's saying that somewhere in the between there's a happy medium,

06:21.000 --> 06:25.000
where you don't go overboard with sensuality.

06:25.000 --> 06:31.000
But on the other hand, you don't torture yourself too much.

06:31.000 --> 06:38.000
And so we find meditators often taking up this view of a moderate view.

06:38.000 --> 06:43.000
It's okay to engage in some addictions.

06:43.000 --> 06:47.000
If you're addicted to chocolate, it's okay to enjoy the chocolate.

06:47.000 --> 06:49.000
It's just some moderation.

06:49.000 --> 06:52.000
Just don't have too much alcohol.

06:52.000 --> 06:54.000
Some people go to the extent.

06:54.000 --> 06:57.000
Buddhist go to the extent of moderation in alcohol.

06:57.000 --> 07:02.000
Don't have enough to get drunk, just enough to get a little happy.

07:02.000 --> 07:09.000
A little librarians, I suppose.

07:09.000 --> 07:14.000
And don't push yourself too hard, but you have to push yourself.

07:14.000 --> 07:20.000
You have to force yourself to do things, because who wants to really spend weeks and months meditating?

07:20.000 --> 07:22.000
Don't push yourself to do it.

07:22.000 --> 07:24.000
Force yourself a little bit, but not too much.

07:24.000 --> 07:29.000
We maybe don't use the word force, but it was sense that you have to push yourself.

07:29.000 --> 07:32.000
Don't push yourself too hard, moderation.

07:32.000 --> 07:35.000
It's not at all really what the Buddha meant.

07:35.000 --> 07:40.000
We can see that from the texts.

07:40.000 --> 07:48.000
It's sensuality of any of any sorts is useless.

07:48.000 --> 07:50.000
Not just useless, it's problematic.

07:50.000 --> 07:52.000
It's wrong.

07:52.000 --> 07:53.000
Indulgence.

07:53.000 --> 07:55.000
It's not sensuality, exactly.

07:55.000 --> 07:58.000
Of course, experiencing sensuality is not wrong.

07:58.000 --> 08:03.000
Looking at something beautiful is not a problem, but indulging in it in the sense of enjoying it,

08:03.000 --> 08:13.000
appreciating it, liking it, liking it, is the point.

08:13.000 --> 08:25.000
The Buddha where the Buddha said he made a nupagama means nupagama means not approaching these two,

08:25.000 --> 08:32.000
not going to these two extremes.

08:32.000 --> 08:42.000
The Buddha found the middle way.

08:42.000 --> 08:46.000
We think somewhere in the middle is where you have to find.

08:46.000 --> 08:49.000
Maybe it's a very fine line, but it's somewhere in between these two.

08:49.000 --> 08:51.000
When in fact, it really isn't.

08:51.000 --> 08:54.000
If you look at what the Buddha talked about as being the middle way,

08:54.000 --> 08:57.000
it has nothing to do with either of the others.

08:57.000 --> 09:04.000
It's actually exposed as a mere artifice of the Buddha.

09:04.000 --> 09:07.000
If you understand in the context, it makes sense.

09:07.000 --> 09:10.000
He was trying to say, look, you've gone to the other extreme,

09:10.000 --> 09:12.000
and it's just as useless as the other extreme.

09:12.000 --> 09:14.000
They're both useless, as what he was saying.

09:14.000 --> 09:16.000
Forget about those two.

09:16.000 --> 09:23.000
Let's try a third alternative, actually, to one extent.

09:23.000 --> 09:31.000
To some extent, though, it feels very much like being in the middle.

09:31.000 --> 09:36.000
As with others, another will go through a couple of other ways that it can be.

09:36.000 --> 09:42.000
You can understand or it was used in the polycan and in the commentary.

09:42.000 --> 09:44.000
But it's very much in the middle.

09:44.000 --> 09:46.000
It's not just a third alternative.

09:46.000 --> 09:49.000
We found another extreme to go to.

09:49.000 --> 10:04.000
In a sense, it's the extreme of being free from something,

10:04.000 --> 10:13.000
from any sort of extremism, if that makes any sense.

10:13.000 --> 10:16.000
So if you think about what the aidful noble path,

10:16.000 --> 10:21.000
what the middle way is, the aidful noble path, if you look at it.

10:21.000 --> 10:26.000
It's very much about not this nor that or anything, really.

10:26.000 --> 10:33.000
So when sensuality comes up, it's not about any sort of moderation.

10:33.000 --> 10:40.000
But the other hand, it's not about rejection or avoidance or self-toucher.

10:40.000 --> 10:43.000
So in a sense, it is bright in that fine line in the middle.

10:43.000 --> 10:46.000
There's nothing to do with the other two.

10:46.000 --> 10:50.000
When you experience something beautiful, for example.

10:50.000 --> 10:54.000
When there's nothing beautiful about what you're seeing,

10:54.000 --> 10:57.000
the beautiful, ugly, is really meaningless.

10:57.000 --> 11:02.000
It's all very much in our minds, according to our past habits

11:02.000 --> 11:08.000
and our genetic makeup and so on.

11:08.000 --> 11:16.000
So the middle way is really the more simple experiencing objective experience.

11:16.000 --> 11:19.000
When you see something to be aware that you're seeing it,

11:19.000 --> 11:23.000
when you feel pain to be aware that you're feeling pain.

11:23.000 --> 11:30.000
So this idea that somehow you should avoid this other idea of,

11:30.000 --> 11:33.000
this is the opposite thing in fact.

11:33.000 --> 11:39.000
So on the other hand, some people will say, you shouldn't see beautiful things.

11:39.000 --> 11:42.000
Take down all your, anything that could be beautiful.

11:42.000 --> 11:46.000
If there's music, plug your ears.

11:46.000 --> 11:51.000
Food, make sure you have only plain food, nothing delicious.

11:51.000 --> 11:53.000
Nothing that could be considered delicious.

11:53.000 --> 11:58.000
Don't ever eat chocolate, that would be, that would be bad.

11:58.000 --> 12:06.000
You know, the other end, don't ever expose yourself to pain.

12:06.000 --> 12:10.000
So, you have people who would say, you know,

12:10.000 --> 12:13.000
sitting through pain is a torch off torture.

12:13.000 --> 12:15.000
It wasn't very much against with the Buddha taught.

12:15.000 --> 12:17.000
Mahasi Sayada goes on about this guy here.

12:17.000 --> 12:21.000
He goes on and on about it.

12:21.000 --> 12:24.000
Not on and on but he explains it quite well.

12:24.000 --> 12:33.000
He says that these well-meaning meditation teachers will guide their students

12:33.000 --> 12:37.000
to always avoid pain when pain comes up to change position,

12:37.000 --> 12:40.000
always be in a comfortable position, so you never experience pain.

12:40.000 --> 12:43.000
But he says, it's quite misguided.

12:43.000 --> 12:49.000
The Buddha, in fact, is quite clearly one of the texts that we have.

12:49.000 --> 12:57.000
He says that you should be willing to bear with pain, even if it kills you.

12:57.000 --> 13:00.000
Even deathly pain.

13:00.000 --> 13:08.000
A great catalyst for enlightenment.

13:08.000 --> 13:16.000
So, the middle way in this sense, I mean, it's just talking about a way that is free from any kind of reaction.

13:16.000 --> 13:21.000
A way that is free from any kind of ambition or drive or exertion.

13:21.000 --> 13:25.000
It's in fact effortless.

13:25.000 --> 13:29.000
The reason why meditation seems like such a chore is because we're so lazy,

13:29.000 --> 13:34.000
because we're so much to the indulgent side.

13:34.000 --> 13:39.000
Or because we're forcing it, we're practicing self-torture.

13:39.000 --> 13:41.000
We meditate, we force our minds.

13:41.000 --> 13:43.000
No, stay with the stomach.

13:43.000 --> 13:44.000
No, stay with the foot.

13:44.000 --> 13:46.000
No, don't go wandering.

13:46.000 --> 13:49.000
Don't get greedy, don't kidding.

13:49.000 --> 13:53.000
We fall to the extremes.

13:53.000 --> 13:56.000
We fall to the streams because we're not present.

13:56.000 --> 14:00.000
We're not in the center, in the middle.

14:00.000 --> 14:05.000
So it's not exactly a spectrum.

14:05.000 --> 14:09.000
It's not that you take everything to moderation.

14:09.000 --> 14:13.000
It's freedom from them both, which puts you right in the center, which you centered.

14:13.000 --> 14:14.000
Pure.

14:14.000 --> 14:17.000
I mean, if you think purity, what is purity?

14:17.000 --> 14:23.000
It's the freedom from defilement.

14:23.000 --> 14:33.000
Clarity is the freedom from being clouded, being obscure, being blurred.

14:33.000 --> 14:35.000
What is right?

14:35.000 --> 14:37.000
You look at the eight full noble path.

14:37.000 --> 14:44.000
It's all about right, some entity right view.

14:44.000 --> 14:48.000
And right view is really, for the most part,

14:48.000 --> 14:53.000
just not having any wrong view.

14:53.000 --> 14:56.000
It's about having views of reality.

14:56.000 --> 15:01.000
What is nature of reality?

15:01.000 --> 15:05.000
There's nothing, it's not like the middle of the right view

15:05.000 --> 15:08.000
is just knowledge of the form of the truth, which there's nothing.

15:08.000 --> 15:10.000
It's not like that's moderate.

15:10.000 --> 15:12.000
It's in between two things.

15:12.000 --> 15:15.000
It's just freedom from any other kind of view.

15:15.000 --> 15:17.000
And so on.

15:17.000 --> 15:22.000
If you look at the eight full noble path, you can clearly see it's not at all about moderation.

15:22.000 --> 15:29.000
It's about being right and pure in the sense of not having anything that diverts you.

15:29.000 --> 15:35.000
It takes you to an extreme.

15:35.000 --> 15:40.000
The other way that the middle way is used is,

15:40.000 --> 15:42.000
what's used in different ways.

15:42.000 --> 15:50.000
Another way it's abused is to give the idea that the Buddha didn't teach non-self.

15:50.000 --> 15:53.000
Or that non-self doesn't mean there is no self.

15:53.000 --> 15:56.000
And in fact, it doesn't mean there is no self.

15:56.000 --> 16:00.000
It's important to nuance this and be careful.

16:00.000 --> 16:04.000
So people say, well, because it doesn't mean there is no self.

16:04.000 --> 16:07.000
The Buddha never, as far as I know, said that.

16:07.000 --> 16:10.000
He certainly didn't deny it.

16:10.000 --> 16:14.000
But as I've said many times, this is because of the nature of reality.

16:14.000 --> 16:15.000
There is no self.

16:15.000 --> 16:19.000
It's just a statement that doesn't make any sense.

16:19.000 --> 16:22.000
It's like saying there is no cat.

16:22.000 --> 16:27.000
Well, cats are not things that exist and neither are selves.

16:27.000 --> 16:29.000
So you say, well, cats don't exist.

16:29.000 --> 16:30.000
What do you mean?

16:30.000 --> 16:31.000
They have a cat.

16:31.000 --> 16:33.000
So it's confusing.

16:33.000 --> 16:35.000
A cat is an abstraction.

16:35.000 --> 16:38.000
It's on a whole other level of reality.

16:38.000 --> 16:42.000
Non-self is dealing with reality.

16:42.000 --> 16:48.000
So the way the Buddha explained it, people are saying that when you die,

16:48.000 --> 16:52.000
the soul continues on.

16:52.000 --> 16:55.000
When the body dies, the soul enters another body.

16:55.000 --> 16:57.000
The soul is eternal.

16:57.000 --> 17:00.000
And other people would say, no, when you die,

17:00.000 --> 17:02.000
there is a cessation.

17:02.000 --> 17:03.000
There's nothing.

17:03.000 --> 17:05.000
There's no experience, nothing.

17:05.000 --> 17:08.000
And the Buddha said, these are two extreme views.

17:08.000 --> 17:13.000
These two views are wrong view.

17:13.000 --> 17:18.000
So he teaches dependent origination,

17:18.000 --> 17:19.000
which is in the middle.

17:19.000 --> 17:20.000
Why is it in the middle?

17:20.000 --> 17:27.000
Well, because it describes a causal chain of experience.

17:27.000 --> 17:32.000
And then it doesn't rely upon the conceptual abstraction

17:32.000 --> 17:35.000
of there being a self or something.

17:35.000 --> 17:39.000
Something that would be totally outside of the realm

17:39.000 --> 17:44.000
of experiential reality that we could call itself.

17:44.000 --> 17:49.000
So people get all worried or confused about this idea of

17:49.000 --> 17:52.000
non-self or did the Buddha actually say there was a self?

17:52.000 --> 17:56.000
And they really said, let go of all that stuff.

17:56.000 --> 17:58.000
The Buddhism is really an innocuous.

17:58.000 --> 18:01.000
He just didn't say anything, really.

18:01.000 --> 18:07.000
What he pointed out was that we have all these attachments to self.

18:07.000 --> 18:13.000
We have ego, we cling to things as being me as mine and so on.

18:13.000 --> 18:18.000
And so he advocated in giving up of self view,

18:18.000 --> 18:23.000
giving up of any view relating to self.

18:23.000 --> 18:26.000
I hope I don't sound like I'm trying to say that there is any sort of self.

18:26.000 --> 18:28.000
It's not.

18:28.000 --> 18:32.000
There's very much on the side of being non-self.

18:32.000 --> 18:35.000
That any way we could conceive of a self,

18:35.000 --> 18:38.000
any conceiving that might go on,

18:38.000 --> 18:40.000
that might arise in the mind.

18:40.000 --> 18:42.000
It's just an arisen phenomenon.

18:42.000 --> 18:44.000
This belief or this thought,

18:44.000 --> 18:46.000
hey, maybe there's a self.

18:46.000 --> 18:49.000
I feel like there's a self.

18:49.000 --> 18:51.000
All of that is problematic.

18:51.000 --> 18:56.000
It's a cause for stress and suffering because then there's clinging.

18:56.000 --> 18:59.000
And so he taught what he saw, what we can all clearly see

18:59.000 --> 19:04.000
that there is causal relationships between phenomena,

19:04.000 --> 19:06.000
between experiences.

19:06.000 --> 19:08.000
There's very much a middle way.

19:08.000 --> 19:13.000
It's sort of a view that it should not be considered extreme,

19:13.000 --> 19:15.000
but it is.

19:15.000 --> 19:19.000
This idea that any view of self could be problematic.

19:19.000 --> 19:21.000
It sounds very much like there is no self

19:21.000 --> 19:24.000
and then you think, well, what about myself?

19:24.000 --> 19:29.000
I was just reading the Wikipedia page on the middle way.

19:29.000 --> 19:34.000
And it claims that in terabyte of Buddhism,

19:34.000 --> 19:38.000
there's the idea that an arrow hunt upon passing

19:38.000 --> 19:41.000
neither exists nor non-exists.

19:41.000 --> 19:43.000
It says the polycannon says that.

19:43.000 --> 19:46.000
It doesn't give us citations.

19:46.000 --> 19:49.000
I'd like to have someone go in there and put a citation

19:49.000 --> 19:53.000
missing or something notation because

19:53.000 --> 19:55.000
pretty sure that's not what it says.

19:55.000 --> 19:58.000
I remember in the polycannon it says

19:58.000 --> 20:03.000
someone asks, does an arrow hunt exist after they pass away?

20:03.000 --> 20:05.000
No, that's not the case.

20:05.000 --> 20:07.000
Is it that they don't exist when they pass away?

20:07.000 --> 20:08.000
No, that's not the case.

20:08.000 --> 20:11.000
I mean, that's not proper to say.

20:11.000 --> 20:14.000
Well, do they both exist and not exist?

20:14.000 --> 20:15.000
No, that's not proper to say.

20:15.000 --> 20:17.000
Do they neither exist nor not exist?

20:17.000 --> 20:19.000
It says in Wikipedia.

20:19.000 --> 20:22.000
And the answer is no, that's not the case either.

20:22.000 --> 20:25.000
Because arrow hunt is just a concept.

20:25.000 --> 20:28.000
Existence is not like that existence is experience.

20:28.000 --> 20:34.000
All that exists is moments of experience.

20:34.000 --> 20:40.000
So sometimes these things that the Buddha said

20:40.000 --> 20:45.000
and sometimes it was just to avoid it because people get so confused

20:45.000 --> 20:50.000
because they're living in this intellectual or conceptual reality

20:50.000 --> 20:54.000
where I exist and there are people in places and things.

20:54.000 --> 21:01.000
We're very much extreme in this way.

21:01.000 --> 21:04.000
They've gone to an extreme of delusion where we believe

21:04.000 --> 21:11.000
that things exist where we have caught up in our conceptions

21:11.000 --> 21:19.000
or our sunya, we recognize things and it morphs into this belief

21:19.000 --> 21:23.000
for this conception of things as existing

21:23.000 --> 21:27.000
as having entities.

21:27.000 --> 21:38.000
A real middle way is based on the three characteristics

21:38.000 --> 21:42.000
and it's very much about not not not not.

21:42.000 --> 21:45.000
Not permanent, not stable, not lasting.

21:45.000 --> 21:49.000
Not pleasant, not satisfying, not happiness.

21:49.000 --> 21:53.000
Not me, not myself.

21:53.000 --> 21:58.000
What it means is we have all these attachments to things

21:58.000 --> 22:03.000
as being me and mine, as being pleasant, as being a source of happiness

22:03.000 --> 22:08.000
and as being stable, as being a refuge.

22:08.000 --> 22:11.000
We just learned today on the Visudhi manga, we got to this.

22:11.000 --> 22:16.000
One of the best passages I think are one of the most important passages

22:16.000 --> 22:22.000
I think in the Visudhi manga, not for the aspect of the Buddha's teaching

22:22.000 --> 22:26.000
that it describes, but just how well it describes the three characteristics.

22:26.000 --> 22:29.000
I mean, these are so important.

22:29.000 --> 22:32.000
So it gives 40 ways that come from, I think,

22:32.000 --> 22:35.000
the Patisambhida manga or maybe even earlier.

22:35.000 --> 22:40.000
40 ways by what you can understand, the three characteristics.

22:40.000 --> 22:43.000
We read through that. I encourage everyone.

22:43.000 --> 22:47.000
You can look it up. It's in the beginning or the halfway through,

22:47.000 --> 22:53.000
maybe the chapter on manga, manga, nyana, doesn't always seem to be purification

22:53.000 --> 22:58.000
by knowledge and vision of what is the path and what is not the path.

22:58.000 --> 23:01.000
When one first begins to grasp what is the path,

23:01.000 --> 23:03.000
what the path is this.

23:03.000 --> 23:08.000
The path is seeing through our illusions, our ignorance,

23:08.000 --> 23:14.000
seeing through our conception of permanence,

23:14.000 --> 23:21.000
stability and satisfaction and control

23:21.000 --> 23:24.000
to see impermanence suffering in non-self.

23:24.000 --> 23:28.000
As you start to grasp this, you make that shift

23:28.000 --> 23:32.000
when you start upon the noble path.

23:32.000 --> 23:34.000
That's really the middle way.

23:34.000 --> 23:39.000
When a person begins to see impermanence suffering in non-self,

23:39.000 --> 23:42.000
meaning they begin to give up or they begin to get a glimpse

23:42.000 --> 23:47.000
of what it means to let go and how one actually goes about letting go.

23:47.000 --> 23:51.000
It's quite simple as you start to see impermanence,

23:51.000 --> 23:54.000
you give up permanence or you give up those things

23:54.000 --> 23:58.000
that you were clinging to because you thought they were stable.

23:58.000 --> 24:01.000
As you start to see suffering, you give up those things

24:01.000 --> 24:03.000
that you thought were bringing you happiness.

24:03.000 --> 24:06.000
That weren't actually bringing you happiness.

24:06.000 --> 24:08.000
As you have started to see non-self,

24:08.000 --> 24:11.000
you give up those things that you thought were you and yours

24:11.000 --> 24:15.000
and belong to you.

24:15.000 --> 24:20.000
Once that clinging ceases, then suffering ceases,

24:20.000 --> 24:22.000
the suffering and the stress which comes

24:22.000 --> 24:27.000
from the ignorance and the delusion.

24:27.000 --> 24:30.000
So the middle way is very much just a simplification.

24:30.000 --> 24:33.000
I'm giving up those things that one might call extreme,

24:33.000 --> 24:37.000
and often call extreme because they end up being opposite.

24:37.000 --> 24:39.000
This is no good, so let's do this.

24:39.000 --> 24:41.000
This is no good, let's do this.

24:41.000 --> 24:43.000
There's this argument over the two,

24:43.000 --> 24:46.000
and the Buddha's just as neither is really good.

24:46.000 --> 24:49.000
In terms of our meditation, it does feel very much

24:49.000 --> 24:56.000
like being in the middle because you give up your desires

24:56.000 --> 24:59.000
and you give up your versions.

24:59.000 --> 25:02.000
Because you're patient with pleasant things.

25:02.000 --> 25:04.000
You're patient with unpleasant things, obviously.

25:04.000 --> 25:07.000
But you're also patient with pleasant things.

25:07.000 --> 25:10.000
When something pleasant comes, you have to be just as patient

25:10.000 --> 25:14.000
in the sense of not jumping for it.

25:14.000 --> 25:18.000
I think of something you want chocolate

25:18.000 --> 25:24.000
to find the patience and the ability to be with the experience

25:24.000 --> 25:32.000
wanting, wanting, for example, or liking or feeling feeling.

25:32.000 --> 25:35.000
As much as you are for pain, so when you feel unpleasant,

25:35.000 --> 25:38.000
sensation, just be pain, pain, or disliking,

25:38.000 --> 25:41.000
disliking, and you just be with them.

25:41.000 --> 25:43.000
That's really the path.

25:43.000 --> 25:48.000
When you experience disliking, you're not trying to shut it off

25:48.000 --> 25:52.000
or stop it, but you're also not going with it.

25:52.000 --> 25:56.000
When you're angry, when you're bored, when you're frustrated,

25:56.000 --> 25:58.000
and when you want something, you're not trying to shut it off.

25:58.000 --> 26:01.000
Say, no, bad, that's a problem.

26:01.000 --> 26:04.000
But you're also not going with it.

26:04.000 --> 26:05.000
You're objective.

26:05.000 --> 26:10.000
You say like King, like King, or wanting, let it go.

26:10.000 --> 26:14.000
Let it come, let it go.

26:14.000 --> 26:16.000
So there you go.

26:16.000 --> 26:20.000
There's a sort of abbreviated moderate.

26:20.000 --> 26:22.000
There's a moderate talk.

26:22.000 --> 26:28.000
Moderate length talk on the middle lane.

26:28.000 --> 26:30.000
So thank you all for tuning in.

26:30.000 --> 26:54.000
I'll show you all the best.

