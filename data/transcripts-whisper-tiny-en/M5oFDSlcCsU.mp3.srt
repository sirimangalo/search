1
00:00:00,000 --> 00:00:04,900
Hello, Bante, which meditation method do you recommend the most? From which

2
00:00:04,900 --> 00:00:12,900
Suta is this, thanks. Very easy question with a very simple answer. The practice

3
00:00:12,900 --> 00:00:20,480
that I recommend the most, almost exclusively, is the practice of the four

4
00:00:20,480 --> 00:00:29,360
foundations of mindfulness, which is found in three general places, I mean other

5
00:00:29,360 --> 00:00:38,640
places as well, but most explicitly in the Deganikaya number 10, no, Deganikaya

6
00:00:38,640 --> 00:00:44,640
no, no, there's... Magimini kai number 10, Deganikaya number, I want to say 12, but I

7
00:00:44,640 --> 00:00:48,160
can't remember. It's a shameful thing that I can't even remember. No, it's not

8
00:00:48,160 --> 00:00:55,360
12. Where's the Mahasati Patanasita? 16 is Paryni Bhanasuta. No, I'm terrible. Don't even

9
00:00:55,360 --> 00:01:00,840
remember. 22 is that it, I can't remember. My mind is going, and here I was

10
00:01:00,840 --> 00:01:04,600
bragging about having a good memory. Anyway, I remember what the Suta is all

11
00:01:04,600 --> 00:01:12,920
about. Why am I getting those mixed up? No. Anyway, Magimini kai number 10, and the

12
00:01:12,920 --> 00:01:19,800
third place is in the Senyutinikaya, the Satipatanasan Yuta, the group. A whole

13
00:01:19,800 --> 00:01:24,200
group of discourses based on the Satipatanasuta, based on the four Satipatanas,

14
00:01:24,200 --> 00:01:30,120
the four foundations of mindfulness, and these are the body, the feeling, the

15
00:01:30,120 --> 00:01:38,960
feelings, the mind, and dhammas, which are... I asked my teacher about the

16
00:01:38,960 --> 00:01:43,320
dhammas recently, because he was still using the English, he's one of the few

17
00:01:43,320 --> 00:01:47,280
English words that he knows is the translation of the four foundations of mindfulness,

18
00:01:47,280 --> 00:01:52,480
and the translation they've given him is the usual mind objects, which doesn't

19
00:01:52,480 --> 00:01:58,400
at all encompass what is meant by these, by it in this context. The dhamma, as a

20
00:01:58,400 --> 00:02:09,760
mind object, is only dhamma, or the objects of thought. So the sixth sense, which

21
00:02:09,760 --> 00:02:13,360
is thinking. So when you think about something, the object of your thought, that's

22
00:02:13,360 --> 00:02:22,720
a mind object, that's called dhamma in a minute. But the dhammas here are... my

23
00:02:22,720 --> 00:02:33,760
teacher said, things that support you, things that support you, that keep you

24
00:02:33,760 --> 00:02:40,840
from falling into evil, because dhamma comes from dar, which is to to hold

25
00:02:40,840 --> 00:02:47,080
dhammas are those things that hold you up, keep you from falling. So these are

26
00:02:47,080 --> 00:02:51,840
the things, another way to look, a way to translate it, is those things that

27
00:02:51,840 --> 00:02:58,640
exist in reality. So reality is a good translation. Anyway, fourth one has... the

28
00:02:58,640 --> 00:03:02,200
problem is it has many groups in it, and it looks to me very much like it's

29
00:03:02,200 --> 00:03:07,840
simply talking about teachings, you know, the various aspects of the Buddha's

30
00:03:07,840 --> 00:03:13,960
teaching, things that are important, dhammas in the sense of things that will

31
00:03:13,960 --> 00:03:18,200
carry you through to enlightenment, teachings that will carry you through to

32
00:03:18,200 --> 00:03:23,040
enlightenment. Anyway, that's the teaching, the four foundations of mindfulness.

33
00:03:23,040 --> 00:03:27,040
There's an excellent book out there that everyone should read, called The Way of

34
00:03:27,040 --> 00:03:33,880
Mindfulness, and it's simply a translation of the Maasati Patanasuda with

35
00:03:33,880 --> 00:03:40,160
a translation of most, I think, or some at least, of the commentary to the

36
00:03:40,160 --> 00:03:45,040
Sati Patanasuda, and even some of the sub-commentary. So it's an excellent

37
00:03:45,040 --> 00:03:53,120
look at how the ancient teachers, you know, 2,000 years ago, 1,500 to 2,000 years

38
00:03:53,120 --> 00:04:00,760
ago, were teaching and were interpreting the Sati Patanasuda. It's an excellent

39
00:04:00,760 --> 00:04:05,040
book, excellent resource, it's got lots of neat stories in it and insights in it,

40
00:04:05,040 --> 00:04:12,280
but even the Sutta itself is a great place to start to learn about meditation, so

41
00:04:12,280 --> 00:04:19,520
very, very much worth reading. I mean, on the other hand, it may not be perfect

42
00:04:19,520 --> 00:04:24,000
place to start because, of course, the Sutta's are coming from a whole

43
00:04:24,000 --> 00:04:29,840
other era designed for a whole other audience in India, so it's sometimes

44
00:04:29,840 --> 00:04:33,240
difficult, and then they're in a whole other language, completely different from

45
00:04:33,240 --> 00:04:38,920
English. So sometimes difficult to really understand what's being said, so

46
00:04:38,920 --> 00:04:46,240
but if you take what I'm passing along in tandem with the, with that study, I

47
00:04:46,240 --> 00:04:51,960
think it would be indispensable, those two things together. I'm not bragging

48
00:04:51,960 --> 00:04:54,360
about my own teaching, but I'm talking about the teaching that has been

49
00:04:54,360 --> 00:05:00,320
passed on to me. You take that teaching and it's basically an explanation in

50
00:05:00,320 --> 00:05:06,840
modern terms of the Sati Patanasuda. Take those two together. In this

51
00:05:06,840 --> 00:05:11,520
tradition, that is indispensable. Of course, every tradition has its own

52
00:05:11,520 --> 00:05:19,720
explanations, but in this tradition, I would say that's where I get it from, so if

53
00:05:19,720 --> 00:05:26,200
you're looking for something, some sort of source, go there and really take

54
00:05:26,200 --> 00:05:29,480
these two things together, the teachings that are modern and the teachings

55
00:05:29,480 --> 00:05:35,520
that they come from, the Sati Patanasuda. And also read the Sutta, the Sati

56
00:05:35,520 --> 00:05:41,120
Patanas and you did excellent sort of additional resource, something very

57
00:05:41,120 --> 00:05:57,840
much worth reading.

