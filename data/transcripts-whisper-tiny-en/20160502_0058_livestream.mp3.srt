1
00:00:00,000 --> 00:00:26,000
Good evening, everyone, broadcasting live May 1st, 2016.

2
00:00:26,000 --> 00:00:35,000
Today's quote is from the Angutranikaya.

3
00:00:35,000 --> 00:00:40,000
Again, it's time to book up fours.

4
00:00:40,000 --> 00:00:48,000
Again, the quote only shows only singles out part of being sitta.

5
00:00:48,000 --> 00:00:51,000
Again, this isn't the words of the Buddha.

6
00:00:51,000 --> 00:01:01,000
This is the words of Anand.

7
00:01:01,000 --> 00:01:10,000
Anand was the attendant and cousin of the Buddha,

8
00:01:10,000 --> 00:01:16,000
and he spent much of the Buddha's later life caring for him

9
00:01:16,000 --> 00:01:25,000
following him around and listening to him and gathering and remembering the Buddha's teachings.

10
00:01:25,000 --> 00:01:32,000
He had a very good memory, it seems.

11
00:01:32,000 --> 00:01:40,000
So, unsurprising that people would come to him for the Dhamma, perhaps after the Buddha passed away

12
00:01:40,000 --> 00:01:49,000
because Anand is one of the great monks who outlived the Buddha.

13
00:01:49,000 --> 00:01:56,000
As Sariput and Mughalana, the Buddha's two cheap disciples passed away before the Buddha did.

14
00:01:56,000 --> 00:01:58,000
But Anand lived on.

15
00:01:58,000 --> 00:02:05,000
Anand lived to 120, so the legend goes.

16
00:02:05,000 --> 00:02:23,000
And before he died, he knew that both sides of his family were going to fight over his relics.

17
00:02:23,000 --> 00:02:28,000
So if he died, when he was sick and dying, he went to stay with his family,

18
00:02:28,000 --> 00:02:35,000
but both families wanted to stay with him, to stay with them, maybe just making this up.

19
00:02:35,000 --> 00:02:43,000
But at the end result, he flew up into the air and spontaneously combusted,

20
00:02:43,000 --> 00:02:51,000
and made a determination so that half of his relic, his bones,

21
00:02:51,000 --> 00:02:55,000
went on either side of the fence.

22
00:02:55,000 --> 00:02:59,000
And like that, the river, maybe I think it was a river.

23
00:02:59,000 --> 00:03:04,000
There's a lot of stories like this.

24
00:03:04,000 --> 00:03:08,000
Anand is a special monk.

25
00:03:08,000 --> 00:03:19,000
The idea we get about him is he was a very compassionate and interested in helping people

26
00:03:19,000 --> 00:03:28,000
who were, maybe at a hard time, he was moved to compassion in ways that other monks

27
00:03:28,000 --> 00:03:30,000
maybe weren't.

28
00:03:30,000 --> 00:03:38,000
And so he was moved to compassion, moved by compassion to speak up for the beakwinies,

29
00:03:38,000 --> 00:03:42,000
even after the Buddha cautioned against ordaining women.

30
00:03:42,000 --> 00:03:49,000
And Anand kind of pushed the Buddha to ordain women.

31
00:03:49,000 --> 00:03:51,000
Of course, that's a whole issue.

32
00:03:51,000 --> 00:03:58,000
But there is an argument to be had for why the Buddha was hesitant.

33
00:03:58,000 --> 00:04:07,000
I mean, whether you agree or not, the point being that it makes it more difficult.

34
00:04:07,000 --> 00:04:12,000
You can say, well, that's no excuse, and it really isn't in the end.

35
00:04:12,000 --> 00:04:22,000
But certainly when you're trying to be celibate and the majority of those trying to be celibate are men,

36
00:04:22,000 --> 00:04:28,000
our heterosexual men bringing women into the mix makes it more difficult.

37
00:04:28,000 --> 00:04:30,000
So Buddha was hesitant.

38
00:04:30,000 --> 00:04:35,000
And also in India, women were perhaps sheltered, so there's arguments.

39
00:04:35,000 --> 00:04:50,000
But Anand, I was not having any of it and really convinced the Buddha to ask.

40
00:04:50,000 --> 00:04:55,000
And he taught laymen, laywomen, he was big on teaching people.

41
00:04:55,000 --> 00:04:57,000
It was really helpful towards women, especially.

42
00:04:57,000 --> 00:05:04,000
I actually got him in some trouble sometimes because women would take fancy term.

43
00:05:04,000 --> 00:05:07,000
But he was never moved by that.

44
00:05:07,000 --> 00:05:18,000
He was never even claimed that he'd never had any sexual desire for a woman as a monk.

45
00:05:18,000 --> 00:05:25,000
Anyway, all this is beside the point.

46
00:05:25,000 --> 00:05:27,000
There are these four Anand.

47
00:05:27,000 --> 00:05:38,000
So they came to Anand and Anand, and they just taught them these four, what are called the Pari-sundi-pa-dhan-ni-yang-ni.

48
00:05:38,000 --> 00:05:41,000
Pari-sundi means purity.

49
00:05:41,000 --> 00:05:50,000
But dhan-ni-ya-dhan-ni-yam means something you should strive for.

50
00:05:50,000 --> 00:06:03,000
So there are these four things regarding which you should strive for the purity for the purification of.

51
00:06:03,000 --> 00:06:11,000
So four things you should try to purify should strive to purify.

52
00:06:11,000 --> 00:06:35,000
And that's where the quote comes from.

53
00:06:35,000 --> 00:06:45,000
Number two, Jita-pa-ri-sundi-pa-dhan-ni-yam-ni.

54
00:06:45,000 --> 00:06:53,000
Striving for the purification of mind.

55
00:06:53,000 --> 00:07:01,000
Number three, Jita-ti-pa-ri-sundi-pa-dhan-ni-yam-ni.

56
00:07:01,000 --> 00:07:06,000
Striving for the purification of view.

57
00:07:06,000 --> 00:07:11,000
And number four, we mu-ti-pa-ri-sundi-pa-dhan-ni-yam-ni.

58
00:07:11,000 --> 00:07:20,000
Striving for the purification of release freedom.

59
00:07:20,000 --> 00:07:29,000
So these four, we should strive to purify, strive to complete.

60
00:07:29,000 --> 00:07:35,000
So the first one that the quote talks about is purification of moral ups and virtue.

61
00:07:35,000 --> 00:07:41,000
See that.

62
00:07:41,000 --> 00:07:48,000
And this is, we talked a little bit about this last night.

63
00:07:48,000 --> 00:07:53,000
The precepts and rules and keeping rules.

64
00:07:53,000 --> 00:08:00,000
In general, rules, if you have the right rules, they tend to be a good guide for your behavior.

65
00:08:00,000 --> 00:08:02,000
And so we purify them.

66
00:08:02,000 --> 00:08:07,000
We look and see which rules we're breaking, which rules we tend to break.

67
00:08:07,000 --> 00:08:13,000
And then we work to stop breaking those rules.

68
00:08:13,000 --> 00:08:24,000
So what is impure in regards to our unfulfilled, in regards to our paripurangwa, paripurisam.

69
00:08:24,000 --> 00:08:32,000
I will fulfill those virtues or those rules, specifically dealing with rules.

70
00:08:32,000 --> 00:08:38,000
See kapada, see kapada, that are not fulfilled.

71
00:08:38,000 --> 00:08:45,000
I will strive to fulfill them.

72
00:08:45,000 --> 00:08:46,000
So, some people keeps the five precepts, and then they don't keep the eight precepts,

73
00:08:46,000 --> 00:08:49,000
and then they strive to keep the eight precepts, some people keep the eight precepts,

74
00:08:49,000 --> 00:08:55,000
and then they strive to keep the ten precepts, and then some people keep the ten precepts,

75
00:08:55,000 --> 00:09:00,000
and then they strive to become a monk, and keep many more precepts.

76
00:09:00,000 --> 00:09:07,000
Then you have the precepts, and then you break them, and then you strive not to break them.

77
00:09:07,000 --> 00:09:17,320
and work to purify them. And then there's this curious statement, those that are fulfilled,

78
00:09:17,320 --> 00:09:36,360
tatatatapanyaya anungai, anungahi san. I will support with wisdom. Support those that

79
00:09:36,360 --> 00:09:47,960
are fulfilled with wisdom. tatatatata here or there or again and again where appropriate.

80
00:09:50,680 --> 00:09:55,080
So the commentary doesn't give much explanation of what this means, but I guess it means

81
00:09:58,440 --> 00:10:00,680
because it's easy to keep the letter of the rule, right?

82
00:10:00,680 --> 00:10:08,200
And so you can say, I'm keeping all these rules fine. You can simplify precepts and still be

83
00:10:09,400 --> 00:10:14,280
you can still be a bad person giving the five percent. So it's about filling in the gaps

84
00:10:16,040 --> 00:10:22,440
and it's about fulfilling the purpose of the precepts with wisdom. So you might not kill,

85
00:10:22,440 --> 00:10:26,600
but you can still be very angry and you can still want to hurt or even kill.

86
00:10:26,600 --> 00:10:32,760
And you might not steal, but you still covet that of others and are jealous and so on.

87
00:10:35,480 --> 00:10:44,280
You don't cheat, but you still feel desire and you don't have the understanding of hurting others,

88
00:10:44,280 --> 00:10:47,720
of what it means to hurt others and what is the result of hurting others.

89
00:10:47,720 --> 00:10:56,280
So you can use wisdom to augment that because keeping the precepts, there's this idea that even

90
00:10:57,240 --> 00:11:04,200
if it's painful keeping the precepts is a good thing. People might question that. I think

91
00:11:04,200 --> 00:11:08,760
well if it's whatever you're doing is causing you suffering, why would you do it?

92
00:11:08,760 --> 00:11:18,360
But then we can ask the question, suppose there's an alcoholic or a drug addict and if they

93
00:11:18,360 --> 00:11:23,880
stop taking drugs and they go through withdrawal, then you say that's a reason to go back to taking

94
00:11:23,880 --> 00:11:29,160
drugs, of course. And that's really all this is, it's a kind of a withdrawal when you keep

95
00:11:29,160 --> 00:11:35,000
precepts, when you keep these rules that say the eight precepts are so and it's quite unpleasant

96
00:11:35,000 --> 00:11:43,320
for the first time. This is because you're going through withdrawal. It doesn't mean that it's

97
00:11:44,360 --> 00:11:49,720
wrong. In fact, it's a good sign, a sign that you're actually dealing with the problem instead of

98
00:11:49,720 --> 00:11:52,520
placating it with your addiction.

99
00:11:56,120 --> 00:12:00,360
So that's the first one. See that body soon. Teeth and Talanyanga.

100
00:12:00,360 --> 00:12:10,440
The second one is the thing that should be ought to be purified.

101
00:12:15,480 --> 00:12:21,000
Now what should we call the mind? The jithapari sita, the mind. Or not the mind mind.

102
00:12:21,880 --> 00:12:25,800
It's a difficult one to translate because literally it does mean the mind or mind.

103
00:12:25,800 --> 00:12:34,680
But it's not exactly that. It's the purification of mind states because it's not the end of the

104
00:12:34,680 --> 00:12:44,040
mind. Jithapari sita is actually just a concentration. So your mind is pure. That's great.

105
00:12:45,000 --> 00:12:50,360
But it's not your whole mind. It's not the mind and the abstract sense. It's just a mind and

106
00:12:50,360 --> 00:12:57,240
a absolute sense, in the sense of this mind being pure. This moment of mind.

107
00:12:58,360 --> 00:13:04,280
So as you practice meditation, you'll find after some time, after you become proficient,

108
00:13:04,280 --> 00:13:10,280
you'll find these states arising where you're peaceful, where your pure or your mind just feels

109
00:13:10,280 --> 00:13:18,680
completely free from attachment or craving or aversion. Your mind will be in a clear state,

110
00:13:18,680 --> 00:13:27,800
in a pure state. And we talk about the four johns and just explanations about these sorts of

111
00:13:27,800 --> 00:13:35,400
states of mind where you don't have any liking or disliking or drowsiness or distraction or doubt.

112
00:13:37,640 --> 00:13:44,200
No pleasant, no like, you know, judgment, no reaction, just purity of mind.

113
00:13:44,200 --> 00:13:51,000
That's quite useful because it allows you to seek clearly. If you then focus that on reality,

114
00:13:51,640 --> 00:13:56,360
you'll be able to see things in ways that you weren't able to see things when you were judging

115
00:13:56,360 --> 00:14:02,840
them, when you were reacting to them. So that's really what the meditation is all about.

116
00:14:02,840 --> 00:14:15,480
Meditation is getting yourself in the frame of mind so that you can accomplish the third one,

117
00:14:15,480 --> 00:14:24,120
which is purification of you, views and opinions, clearing yourselves up, clearing yourself up,

118
00:14:24,120 --> 00:14:36,360
your mind up in regards to your beliefs, your opinions, your things you hold to be true.

119
00:14:39,880 --> 00:14:44,920
And this is really the heart of the matter because it's not about calming yourself down,

120
00:14:44,920 --> 00:14:55,000
it's about rightening yourself, purifying the source because the source of our mind states,

121
00:14:55,000 --> 00:15:02,040
the source which determines whether we're going to have a wholesome mind or an unwholesome mind,

122
00:15:02,040 --> 00:15:11,960
a pure mind or an impure mind, is our view, our outlook. So if you have the view that certain

123
00:15:11,960 --> 00:15:17,240
things are worth attaining, you'll strive after them because view that certain things are

124
00:15:18,200 --> 00:15:26,360
worth avoiding, you'll avoid them. If the view that it's right to good to be angry and greedy

125
00:15:26,360 --> 00:15:32,360
and deluded, then you'll do things that give rise to those states and you'll give rise to those

126
00:15:32,360 --> 00:15:43,800
states. If you have views that are views that greed, anger and delusion are wrong or you see,

127
00:15:44,360 --> 00:15:48,120
seeing them as being a problem, then you're less likely to give rise to those emotions.

128
00:15:49,640 --> 00:15:55,960
So it's about purifying your view. In fact, it's not about attaining any one view or any outlook.

129
00:15:55,960 --> 00:16:03,800
It's not about acquiring an outlook. It's about acquiring an understanding of things as they are.

130
00:16:03,800 --> 00:16:10,680
So it's not really a view in the end except in the literal sense that you're able to view things

131
00:16:10,680 --> 00:16:16,760
as they are. Your view is in line with reality. That's really all we're aiming for. We're not aiming

132
00:16:16,760 --> 00:16:25,160
to claim anything. The only claim we make is that if you look, you'll see things as they are.

133
00:16:25,160 --> 00:16:31,640
Once you see things as they are, you will agree with us that they are this way, that we can all

134
00:16:31,640 --> 00:16:38,360
come to an agreement on the way things are. A big part of this, as I've mentioned before, is

135
00:16:39,880 --> 00:16:45,800
a paradigm shift and that's what I tried to explain in the second volume on how to meditate

136
00:16:45,800 --> 00:16:48,680
and I think the first chapter, one of the first chapters,

137
00:16:48,680 --> 00:16:56,440
is that we tend to look at reality in terms of the world around us.

138
00:16:57,560 --> 00:17:03,800
Obviously, who doesn't? If you ask what is real of this room, we're sitting in this room,

139
00:17:03,800 --> 00:17:11,640
but in fact, that's really just a projection. All of that is still dependent upon something more

140
00:17:11,640 --> 00:17:19,400
basic and that's where we shift our focus. Instead of looking at the world in terms of the

141
00:17:19,400 --> 00:17:26,040
room around us, we look at in terms of our perception of what then becomes the room around us.

142
00:17:26,040 --> 00:17:33,000
It's an experience with base reality on experience and that's the beginning of right view.

143
00:17:33,000 --> 00:17:43,400
Without that, there's no way we can be sure that we're all going to come to the same conclusion

144
00:17:45,160 --> 00:17:50,840
because the room here is abstract. Some people think it's nice and don't like it.

145
00:17:51,960 --> 00:17:56,200
Our perception of the room can be quite different. Some people looking at it from one direction,

146
00:17:56,200 --> 00:18:07,240
some people from another. This is what leads to, in the world, it leads to so much conflict and

147
00:18:07,240 --> 00:18:14,840
confusion and misunderstanding and so on. But you can't have that when you look at experience.

148
00:18:14,840 --> 00:18:21,080
Experience is more basic and it doesn't change based on your perception. It is what it is.

149
00:18:21,080 --> 00:18:31,880
It might want it to be otherwise, but it might as well as real. So that's what comes and goes and changes.

150
00:18:37,960 --> 00:18:39,880
That's a deep deep body, isn't it?

151
00:18:41,800 --> 00:18:47,880
And number four, we move deep bodies with these. It's purification of freedom or purification through

152
00:18:47,880 --> 00:18:54,520
freedom maybe. It's the fourth. Once you have right view, right view is for what, right view is

153
00:18:54,520 --> 00:19:02,440
for freedom. We want to see things as they are so we can free ourselves from the prison that

154
00:19:02,440 --> 00:19:09,640
we've placed ourselves in. This prison of desire and aversion and delusion. We trap ourselves.

155
00:19:09,640 --> 00:19:18,520
We bind ourselves to certain the way we think things should be and the way we think things

156
00:19:18,520 --> 00:19:27,320
shouldn't be. By the things we think I am and the things I am not, the things I want to be,

157
00:19:27,320 --> 00:19:39,320
the things I don't want to be, that kind of thing. And so there's fourth one,

158
00:19:39,320 --> 00:19:57,160
and this is, with the other three, with purification of Silla, Tita, and Titi, one Raja, Raja,

159
00:19:57,160 --> 00:20:04,840
Nia, Sudhame, Sudhita, we Rajita. One becomes dispassionate about dhammas, about things

160
00:20:04,840 --> 00:20:12,760
that one might be passionate about. We more jani, Sudhame, Sudhita, we more Titi. One frees

161
00:20:12,760 --> 00:20:20,520
one's house from dhammas, from things that one should free oneself from. And having done so,

162
00:20:21,480 --> 00:20:28,440
Sama, we moreting positive one touches or attains right release, right freedom. I am

163
00:20:28,440 --> 00:20:35,320
which ati, we more deep, I mean this is called purification, so release around release.

164
00:20:42,360 --> 00:20:46,600
So these are the four, Pari, Sudhita, Nyanghani.

165
00:20:46,600 --> 00:20:59,960
But we're taught by that blessed one who is nose and seers and enlightened self,

166
00:21:01,560 --> 00:21:09,480
and self enlightened Buddha. They were taught for the purification of beings, for the overcoming

167
00:21:09,480 --> 00:21:17,400
of sorrow, lamentation, and despair, for the eradication, for the overcoming of

168
00:21:24,040 --> 00:21:33,720
somebody, for the destruction of physical pain and mental pain, giving up for freeing oneself,

169
00:21:33,720 --> 00:21:40,520
for attaining the right path, and for seeing it for oneself, nibhana.

170
00:21:43,320 --> 00:21:47,880
That's a standard, that's actually when the Buddha taught, the Buddha, it's a quote from the Buddha,

171
00:21:47,880 --> 00:21:57,720
from Satipatana, Sudhana, we stood here, so kapari dhi waanam sāmati kambaya, dukkadhammanasa, dhammanasa,

172
00:21:57,720 --> 00:22:06,280
nibhana, atangamaya, nyaya sādhi kamaya, nibhana sādhi kiaya.

173
00:22:13,080 --> 00:22:19,560
So four good things, another way of looking at the path. There's a lot of these where the Buddha

174
00:22:19,560 --> 00:22:28,200
would, and then just taking this from the Buddha's teaching, four things that really condense

175
00:22:28,200 --> 00:22:36,520
the path, because when you're teaching, you have to remind people of the roadmap, the direction,

176
00:22:36,520 --> 00:22:44,920
where are we going, and this is a complete roadmap. You start with Sila and then with morality,

177
00:22:44,920 --> 00:22:51,720
you train yourself in right behavior and right speech, to keep yourself ethical, moral virtues,

178
00:22:52,520 --> 00:22:57,640
and then you begin to focus the mind, that's what you practice meditation. As you do that,

179
00:22:58,360 --> 00:23:03,080
you start to see things clearly, you start to see, well, the four noble truths in the end.

180
00:23:04,440 --> 00:23:11,080
You see, nama, rupa, you see, three characteristics, you see the four noble truths,

181
00:23:11,080 --> 00:23:20,840
and then finally, we mutti have become free, quite simple. It's good to have these guides,

182
00:23:20,840 --> 00:23:29,960
because it keeps us, it reminds us of very straight and straightforward nature of the Buddha's

183
00:23:29,960 --> 00:23:35,880
teaching, not getting caught up in details or getting off track. It's so easy for us to get,

184
00:23:35,880 --> 00:23:43,880
let our minds allow us to wonder and get lost from the wrong path, the path that leads to

185
00:23:44,840 --> 00:23:48,440
stress and suffering. It's good for us to remember these things.

186
00:23:51,640 --> 00:23:53,800
Okay, so that's the number for this evening.

187
00:23:53,800 --> 00:24:01,800
Do we have any questions?

188
00:24:01,800 --> 00:24:24,520
Okay, who couldn't quit talking even when another guy was shooting dung into his room?

189
00:24:24,520 --> 00:24:36,600
Huh, that's from the jot, because I know the jot that I know.

190
00:24:39,800 --> 00:24:44,200
Are fully enlightened beings immoral? Good question.

191
00:24:44,200 --> 00:24:52,920
No, I wouldn't, I guess I wouldn't say that.

192
00:24:59,000 --> 00:25:03,240
It really depends how you look at it, and it's just words, right?

193
00:25:03,240 --> 00:25:13,960
And so some people might want to say that.

194
00:25:17,080 --> 00:25:21,320
Some people might want to say that, but they're actually quite immoral.

195
00:25:23,480 --> 00:25:29,160
The lightning being keeps the precepts purely and will not break from them.

196
00:25:29,160 --> 00:25:40,840
But that's, but that's really because there's a sense that morality is intrinsic to reality.

197
00:25:42,040 --> 00:25:50,120
Reality is not immoral. Reality is moral.

198
00:25:50,120 --> 00:25:59,720
It's from a dhamapada, I don't think the story itself, maybe it is. Pretty sure it's from the jot,

199
00:25:59,720 --> 00:26:07,240
because let me see. I'm pretty sure it's a jot, the story.

200
00:26:09,320 --> 00:26:16,520
Maybe in the dhamapada as well, or maybe wrong, but I know the story.

201
00:26:16,520 --> 00:26:27,240
I can't think off the top of my head, but I can't exact the jot.

202
00:26:27,240 --> 00:26:35,720
Well, the jot takers are on the internet, just read them, and read them, and tell us which one it is.

203
00:26:35,720 --> 00:26:47,960
There's only five hundred and forty-seven of them, fifty-seven, five hundred and forty-seven.

204
00:26:47,960 --> 00:27:09,880
I have that.

205
00:27:17,960 --> 00:27:41,800
Here, we're going to drop the ball of dhamapada.

206
00:27:47,960 --> 00:28:04,680
There's too many of them right over there.

207
00:28:04,680 --> 00:28:06,040
Maybe there's a dhamapada.

208
00:28:06,040 --> 00:28:19,960
If anybody finds it, please pass them on.

209
00:28:19,960 --> 00:28:31,480
I'm pretty sure because it starts with trying to get this guy to be quiet, and so every time he talks,

210
00:28:31,480 --> 00:28:36,600
he should have ball of dung into his mouth.

211
00:28:36,600 --> 00:28:42,600
I'm pretty sure it's a cause for telling a story of the past.

212
00:28:42,600 --> 00:28:48,680
Pretty sure it's a cause for a jot taker, for telling a jot taker.

213
00:28:48,680 --> 00:29:08,400
Yes, we should purify our virtue, we have to work at it.

214
00:29:08,400 --> 00:29:24,880
Anyway, then we have a question, so you have a good night, see you all tomorrow.

215
00:29:24,880 --> 00:29:42,360
Good afternoon.

