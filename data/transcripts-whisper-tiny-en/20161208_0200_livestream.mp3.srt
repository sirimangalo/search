1
00:00:00,000 --> 00:00:28,000
Okay, good evening everyone, welcome to our evening broadcast, a daily dhamma.

2
00:00:28,000 --> 00:00:56,000
As promised today we're going to talk about science.

3
00:00:56,000 --> 00:01:00,000
Science is the pursuit of knowledge.

4
00:01:00,000 --> 00:01:20,000
Science involves the idea that there are things that we don't know with the implication that we should know.

5
00:01:20,000 --> 00:01:30,000
It's an aspect of nature, it's a part of nature, the state of knowledge.

6
00:01:30,000 --> 00:01:36,000
Knowledge is a part of the mind's activity.

7
00:01:36,000 --> 00:01:41,000
There are certain things that we know and there are certain things that we don't know.

8
00:01:41,000 --> 00:01:51,000
On the other hand there's a whole lot of things that we think we know.

9
00:01:51,000 --> 00:02:03,000
That maybe we don't actually know.

10
00:02:03,000 --> 00:02:16,000
And in fact if curiously enough it appears that all of science is merely belief in the end.

11
00:02:16,000 --> 00:02:26,000
When we talk about science we are really just talking about belief.

12
00:02:26,000 --> 00:02:36,000
Now some belief is resting on what we call evidence and some belief does not rest on evidence.

13
00:02:36,000 --> 00:02:45,000
And so certain religions have actually interestingly, it's strangely from a Buddhist perspective and from a scientific perspective,

14
00:02:45,000 --> 00:02:52,000
have postulated certain truths without evidence.

15
00:02:52,000 --> 00:02:58,000
And have postulated a way of understanding truths without evidence.

16
00:02:58,000 --> 00:03:06,000
In other words belief in something without adequate evidence.

17
00:03:06,000 --> 00:03:17,000
Having admitted that there is insubstantial evidence, this isn't a boundary for belief.

18
00:03:17,000 --> 00:03:24,000
I think Buddhism would agree with science that this is not a good idea.

19
00:03:24,000 --> 00:03:33,000
This is not a really honest sort of belief that one should hold.

20
00:03:33,000 --> 00:03:38,000
And so science sets itself apart from other beliefs in that.

21
00:03:38,000 --> 00:03:49,000
It sets a standard for what one might call adequate evidence.

22
00:03:49,000 --> 00:03:54,000
Science at its best is quite rigorous.

23
00:03:54,000 --> 00:04:05,000
Now in practice of course as many scientists have told me in practice it's rarely as airtight as one might hope.

24
00:04:05,000 --> 00:04:17,000
And often monetary concerns and concerns ego gets involved.

25
00:04:17,000 --> 00:04:26,000
And this is the first problem that we find with science, even when it is at its most rigorous.

26
00:04:26,000 --> 00:04:44,000
Is that in the quest for knowledge, in our ordinary quest for knowledge we bring along our personalities.

27
00:04:44,000 --> 00:04:50,000
We bring along culture borrowing from what we talked about yesterday.

28
00:04:50,000 --> 00:04:58,000
Most scientists bring along culture unknowingly or knowingly.

29
00:04:58,000 --> 00:05:19,000
And so as science at its worst involves corrupt scientists, bias exhibiting bias because of where the money is coming from or in order to get funding.

30
00:05:19,000 --> 00:05:27,000
Or in order to for prestige and so on.

31
00:05:27,000 --> 00:05:41,000
But science at its best, most science at its best is still coupled with the baggage of culture.

32
00:05:41,000 --> 00:05:48,000
And this is generally the culture of what we call physicalism.

33
00:05:48,000 --> 00:05:56,000
So we're born into this world, not with a blank slate.

34
00:05:56,000 --> 00:06:00,000
And nor are we given the opportunity to examine the world objectively.

35
00:06:00,000 --> 00:06:10,000
We're immediately thrust into a paradigm of people, places and things, concepts.

36
00:06:10,000 --> 00:06:15,000
Our parents don't set out to teach us the Abidhamma when we're first born.

37
00:06:15,000 --> 00:06:25,000
No, they teach us, this is Apple, this is Birdie, this is Mama, this is Dada.

38
00:06:25,000 --> 00:06:39,000
They teach us sort of a relational paradigm where things are spatially.

39
00:06:39,000 --> 00:06:51,000
In terms of their importance and their quality of good and bad, yummy, yummy food.

40
00:06:51,000 --> 00:06:57,000
Good girl, good boy, bad girl, bad boy.

41
00:06:57,000 --> 00:06:59,000
We're taught.

42
00:06:59,000 --> 00:07:09,000
We're taught reality and most especially when we're taught many unfortunate things as we grow up.

43
00:07:09,000 --> 00:07:13,000
But most unfortunate is our reliance.

44
00:07:13,000 --> 00:07:20,000
The human being relies very much on temporal spatial reality.

45
00:07:20,000 --> 00:07:24,000
It means time and space.

46
00:07:24,000 --> 00:07:28,000
We rely on the past and the future just to get by.

47
00:07:28,000 --> 00:07:40,000
How many of us could live our lives without relying on plans for the future or lessons learned in the past.

48
00:07:40,000 --> 00:07:45,000
Even our social relationships would fall apart.

49
00:07:45,000 --> 00:07:51,000
We'd have to shave our heads, put on rags and go and live in the forest or something.

50
00:07:51,000 --> 00:07:58,000
We wouldn't be able to survive and space.

51
00:07:58,000 --> 00:08:18,000
We have to think in terms of things, to think in terms of the physical world, possessions, belongings, relationships with other people.

52
00:08:18,000 --> 00:08:31,000
We're thrust into an impersonal reality, which is actually somewhat surprisingly counterintuitive.

53
00:08:31,000 --> 00:08:35,000
Most people would say it is completely intuitive.

54
00:08:35,000 --> 00:08:45,000
It in fact is from the point of view of the baby who comes out of the womb and sees stuff, lights, shapes, movements.

55
00:08:45,000 --> 00:08:48,000
And here's sound.

56
00:08:48,000 --> 00:08:51,000
What does the baby make of all these things?

57
00:08:51,000 --> 00:08:56,000
Well, they carry over from their past lives.

58
00:08:56,000 --> 00:09:05,000
Potentially beliefs, but to some extent they do have this, there is somewhat of a clean slate you might argue.

59
00:09:05,000 --> 00:09:09,000
They're seeing light.

60
00:09:09,000 --> 00:09:19,000
The mother will coup at the baby and so on, and they feel sensations, the harsh cloth.

61
00:09:19,000 --> 00:09:24,000
They're so different from the womb that they are the warmth of the mother's womb.

62
00:09:24,000 --> 00:09:26,000
The pain for the baby at childbirth.

63
00:09:26,000 --> 00:09:37,000
I don't know if children experience pain coming out of the vaginal cavity, but whatever you call it.

64
00:09:37,000 --> 00:09:50,000
But it must be quite shocking, little bright lights and the sounds.

65
00:09:50,000 --> 00:09:55,000
And then they have to make sense, what does this all mean and what they're taught?

66
00:09:55,000 --> 00:10:04,000
What is impressed upon them and must be helped along by having lived this life, this sort of life again and again.

67
00:10:04,000 --> 00:10:16,000
It's not simply seeing hearings, smelling, tasting, feeling, thinking we're given this artificial paradigm of space and time.

68
00:10:16,000 --> 00:10:23,000
When the very basis is just seeing hearings, it's just experience.

69
00:10:23,000 --> 00:10:37,000
And so the first problem with science is that it tends to be very, very much enmeshed in this paradigm of external reality.

70
00:10:37,000 --> 00:10:49,000
Instead of focusing on personal experience, personal experiences seen as unreliable, prone to subjectivity.

71
00:10:49,000 --> 00:11:14,000
Partuality or bias or errors in judgment.

72
00:11:14,000 --> 00:11:20,000
And the second problem with science.

73
00:11:20,000 --> 00:11:23,000
This is the first way that it differs from Buddhism.

74
00:11:23,000 --> 00:11:26,000
Buddhism, and we'll talk about Buddhist science.

75
00:11:26,000 --> 00:11:38,000
Buddhism claims some extent to sort of a science, but the other problem is that Buddhism isn't actually about knowledge.

76
00:11:38,000 --> 00:11:39,000
Wisdom isn't the goal.

77
00:11:39,000 --> 00:11:46,000
I often talk about and say that wisdom is the goal, but it's just shorthand for describing what the real goal is.

78
00:11:46,000 --> 00:11:52,000
The real goal is happiness, peace, freedom from suffering.

79
00:11:52,000 --> 00:12:01,000
And that's an important difference because science is getting obsessed with the knowledge.

80
00:12:01,000 --> 00:12:08,000
Irrespective of whether it actually makes you happier or brings peace.

81
00:12:08,000 --> 00:12:13,000
If there's a discovery to be had, there's a scientist who will.

82
00:12:13,000 --> 00:12:19,000
There's all scientists who will jump at the opportunity to be the one to discover.

83
00:12:19,000 --> 00:12:26,000
And so to some extent there's a sense that it makes you happy to be the one who discovers something.

84
00:12:26,000 --> 00:12:29,000
But that's not what Buddhism is about.

85
00:12:29,000 --> 00:12:34,000
What is a message about this obsession with knowledge.

86
00:12:34,000 --> 00:12:41,000
And so we talk about the four noble truths, and the important word here that we often gloss over is noble.

87
00:12:41,000 --> 00:12:42,000
Aria.

88
00:12:42,000 --> 00:12:45,000
That's what makes them special, not that they're the truth.

89
00:12:45,000 --> 00:12:52,000
We're not so concerned about the truth, honestly.

90
00:12:52,000 --> 00:12:58,000
We're concerned about becoming free from suffering.

91
00:12:58,000 --> 00:13:00,000
Because you can't really ever know the truth.

92
00:13:00,000 --> 00:13:03,000
We say the truth is that everything nothing is worth clinging to.

93
00:13:03,000 --> 00:13:06,000
Well, you can't know that.

94
00:13:06,000 --> 00:13:09,000
There could be something around the next corner.

95
00:13:09,000 --> 00:13:16,000
If you just meditate it a little bit longer, maybe you'd find something that was worth clinging to.

96
00:13:16,000 --> 00:13:22,000
Meditation even relies what we talk about the four noble truths, even they rely on belief.

97
00:13:22,000 --> 00:13:24,000
It's called anumana.

98
00:13:24,000 --> 00:13:27,000
Anumana means in inference.

99
00:13:27,000 --> 00:13:30,000
You have to infer.

100
00:13:30,000 --> 00:13:37,000
So it's described as that when a person realizes the four noble truths, it's an epiphany.

101
00:13:37,000 --> 00:13:41,000
It's not a realization of actual knowledge.

102
00:13:41,000 --> 00:13:44,000
Because you can't actually know.

103
00:13:44,000 --> 00:13:48,000
But you get to the point where you have sufficient evidence.

104
00:13:48,000 --> 00:13:50,000
It's very much like science.

105
00:13:50,000 --> 00:13:53,000
But it's sufficient for the mind.

106
00:13:53,000 --> 00:13:56,000
Where the mind finally says, I get it.

107
00:13:56,000 --> 00:13:57,000
It's true.

108
00:13:57,000 --> 00:13:59,000
Nothing is worth clinging to.

109
00:13:59,000 --> 00:14:02,000
It doesn't yet know that.

110
00:14:02,000 --> 00:14:09,000
Because it hasn't seen all phenomena.

111
00:14:09,000 --> 00:14:21,000
It has only experienced one phenomenon.

112
00:14:21,000 --> 00:14:23,000
Maybe you could argue.

113
00:14:23,000 --> 00:14:30,000
Maybe you could argue that you're seeing that one phenomenon so clearly

114
00:14:30,000 --> 00:14:34,000
that you understand the nature of phenomena.

115
00:14:34,000 --> 00:14:36,000
Because all phenomena are the same.

116
00:14:36,000 --> 00:14:37,000
All phenomena are the same.

117
00:14:37,000 --> 00:14:41,000
But it's still got to be an inference of sorts.

118
00:14:41,000 --> 00:14:45,000
Because theoretically, you could be something.

119
00:14:45,000 --> 00:14:49,000
And this is why scientists intellectually they say,

120
00:14:49,000 --> 00:14:50,000
you can never prove a theory.

121
00:14:50,000 --> 00:14:52,000
You can only disprove it.

122
00:14:52,000 --> 00:14:54,000
And you're always just waiting for the disprove.

123
00:14:54,000 --> 00:14:56,000
And if there is never a disprove,

124
00:14:56,000 --> 00:15:02,000
then you accept it for provisionally.

125
00:15:02,000 --> 00:15:04,000
Meditation isn't like that.

126
00:15:04,000 --> 00:15:09,000
There is a clarity, a perfect clarity that comes.

127
00:15:09,000 --> 00:15:15,000
But it's only in relation to one thing.

128
00:15:15,000 --> 00:15:18,000
But there's something quite special about this.

129
00:15:18,000 --> 00:15:22,000
I mean, it's a very different state than intellectually saying,

130
00:15:22,000 --> 00:15:28,000
oh yes, the speed of light is whatever equals MC squared,

131
00:15:28,000 --> 00:15:29,000
matter.

132
00:15:29,000 --> 00:15:34,000
And is it this kind of intellectual reasoning?

133
00:15:34,000 --> 00:15:38,000
It doesn't have any profound impact on your state of mind.

134
00:15:38,000 --> 00:15:43,000
It may change the way you look and the way you think about reality.

135
00:15:43,000 --> 00:15:46,000
It doesn't actually change your outlook,

136
00:15:46,000 --> 00:15:49,000
your nature of your mind.

137
00:15:49,000 --> 00:15:54,000
The foreignable truths are quite special in that they do change.

138
00:15:54,000 --> 00:16:00,000
That seeing certain things, certain realities,

139
00:16:00,000 --> 00:16:05,000
have a fundamental impact on the nature of the mind.

140
00:16:05,000 --> 00:16:11,000
So in Buddhism we talk about the power of knowledge,

141
00:16:11,000 --> 00:16:15,000
the power of insight or wisdom.

142
00:16:15,000 --> 00:16:21,000
But really we're talking about knowledge, nyana.

143
00:16:21,000 --> 00:16:24,000
There are certain knowledges that,

144
00:16:24,000 --> 00:16:28,000
well, there's a certain type of knowledge that doesn't have the same impact.

145
00:16:28,000 --> 00:16:31,000
So we call them suttamayapanya.

146
00:16:31,000 --> 00:16:33,000
This is knowledge that you've heard about,

147
00:16:33,000 --> 00:16:35,000
and jintamayapanya.

148
00:16:35,000 --> 00:16:38,000
It's knowledge that you've thought up on your own.

149
00:16:38,000 --> 00:16:41,000
These ones don't have a profound impact on your life.

150
00:16:41,000 --> 00:16:44,000
Even knowledge of Buddhism, there are Buddhists who know very, very much

151
00:16:44,000 --> 00:16:50,000
about people who have read the whole of the Buddha's teaching once, twice over.

152
00:16:50,000 --> 00:16:52,000
But that doesn't have a profound impact on your mind.

153
00:16:52,000 --> 00:16:54,000
It may seem like it does.

154
00:16:54,000 --> 00:16:57,000
You may feel, wow, reading all this Buddha's teachings,

155
00:16:57,000 --> 00:16:59,000
it's really changed my outlook on reality.

156
00:16:59,000 --> 00:17:03,000
And okay, so does some extent it might.

157
00:17:03,000 --> 00:17:10,000
But that's not the effect of insight meditation practice.

158
00:17:10,000 --> 00:17:13,000
It's not the same thing.

159
00:17:13,000 --> 00:17:19,000
It's not the same as observing reality moment by moment,

160
00:17:19,000 --> 00:17:24,000
watching experiences arise and cease.

161
00:17:24,000 --> 00:17:29,000
And really viscerally feeling the change come about

162
00:17:29,000 --> 00:17:33,000
in the way you look at reality, seeing your ego,

163
00:17:33,000 --> 00:17:40,000
seeing your attachment, seeing your dissatisfaction

164
00:17:40,000 --> 00:17:45,000
and your irritability and inability to bear with reality

165
00:17:45,000 --> 00:17:49,000
and overcoming it.

166
00:17:49,000 --> 00:17:54,000
Changing, watching your mind change as you see your flaws,

167
00:17:54,000 --> 00:18:00,000
as you see your mistakes.

168
00:18:00,000 --> 00:18:03,000
As I said last night, I think I said that

169
00:18:03,000 --> 00:18:10,000
once the scientist in the lab rat,

170
00:18:10,000 --> 00:18:14,000
that's a crucial distinction is we're experimenting on ourselves

171
00:18:14,000 --> 00:18:19,000
and not experimenting but we're studying ourselves.

172
00:18:19,000 --> 00:18:24,000
And so this is arguably something that Buddhism has over science,

173
00:18:24,000 --> 00:18:28,000
that science doesn't actually address one's culture,

174
00:18:28,000 --> 00:18:31,000
doesn't actually address the paradigm.

175
00:18:31,000 --> 00:18:35,000
The way one looks at reality, in general,

176
00:18:35,000 --> 00:18:38,000
I mean it generally doesn't.

177
00:18:38,000 --> 00:18:41,000
It makes certain assumptions.

178
00:18:41,000 --> 00:18:46,000
It looks at reality in a certain way.

179
00:18:46,000 --> 00:18:53,000
Buddhism is simply the science of the individual knowledge

180
00:18:53,000 --> 00:18:58,000
about the individual, about one's own mind.

181
00:18:58,000 --> 00:19:04,000
The science of experience maybe.

182
00:19:04,000 --> 00:19:10,000
And so what we're concerned with is merely an understanding

183
00:19:10,000 --> 00:19:14,000
of our own habits, our own behaviors,

184
00:19:14,000 --> 00:19:18,000
and nature of our own minds.

185
00:19:18,000 --> 00:19:21,000
How we like and dislike certain things,

186
00:19:21,000 --> 00:19:24,000
how we build up habits based on likes and dislikes,

187
00:19:24,000 --> 00:19:29,000
how we cultivate ego and identification.

188
00:19:29,000 --> 00:19:38,000
All of this is the object of insight meditation.

189
00:19:38,000 --> 00:19:58,000
This is important as meditators that we don't get obsessed with knowledge,

190
00:19:58,000 --> 00:20:00,000
even with insight knowledge.

191
00:20:00,000 --> 00:20:03,000
It's not about those aha moments where you get it, where you realize,

192
00:20:03,000 --> 00:20:08,000
oh, I get, I understand, I see impermanence,

193
00:20:08,000 --> 00:20:13,000
I see suffering, I see non-self, meditators will often describe these experiences,

194
00:20:13,000 --> 00:20:18,000
kind of excited and happy.

195
00:20:18,000 --> 00:20:23,000
But it's not that experience that it's not the knowledge that's important,

196
00:20:23,000 --> 00:20:26,000
it's the state of one's mind.

197
00:20:26,000 --> 00:20:30,000
So even insight knowledge is not something you should cling to

198
00:20:30,000 --> 00:20:34,000
or see as the goal.

199
00:20:34,000 --> 00:20:44,000
It's encouraging, of course, to see these things, but

200
00:20:44,000 --> 00:20:50,000
even when we see reality,

201
00:20:50,000 --> 00:20:57,000
even that can become an object of clinging if we like or if we're excited about it.

202
00:20:57,000 --> 00:21:02,000
We have to remind ourselves that it's about us, it's not about that knowledge,

203
00:21:02,000 --> 00:21:13,000
it's about how we react to it, how we react to reality, how we interact with our experience.

204
00:21:13,000 --> 00:21:22,000
Don't get caught up in the intellectualizing or thinking about meditation practice.

205
00:21:22,000 --> 00:21:26,000
Even when you experience knowledge, the Buddha said it's like a raft.

206
00:21:26,000 --> 00:21:30,000
A raft has a purpose, and once its purpose is over,

207
00:21:30,000 --> 00:21:34,000
you don't pick the raft up and walk away with it.

208
00:21:34,000 --> 00:21:38,000
You throw it away when you use a raft across a river.

209
00:21:38,000 --> 00:21:41,000
Well, good for you, good for the raft.

210
00:21:41,000 --> 00:21:43,000
That was a good raft.

211
00:21:43,000 --> 00:21:46,000
A good raft doesn't mean you pick it up and carry it with you.

212
00:21:46,000 --> 00:21:48,000
Knowledge is the same.

213
00:21:48,000 --> 00:21:56,000
Throw it away once you cross the river, keep going.

214
00:21:56,000 --> 00:21:59,000
So there you go, some brief thoughts about science,

215
00:21:59,000 --> 00:22:03,000
Buddhism could in some ways be considered a science.

216
00:22:03,000 --> 00:22:09,000
In other ways it's not to talk about the science, it's all about

217
00:22:09,000 --> 00:22:16,000
the results, so we only worry about a certain type of science.

218
00:22:16,000 --> 00:22:20,000
What of course is the tool, without that science,

219
00:22:20,000 --> 00:22:25,000
without that knowledge, without the realizations, you can never become enlightened.

220
00:22:25,000 --> 00:22:29,000
That's the other side of this coin, is that anyone who thinks they're just going to sit

221
00:22:29,000 --> 00:22:35,000
and through calming the mind somehow become enlightened as a engine for a rude awakening.

222
00:22:35,000 --> 00:22:36,000
Well, no.

223
00:22:36,000 --> 00:22:39,000
He's in for years and years of pointless exercise,

224
00:22:39,000 --> 00:22:44,000
where they just sit in our calm and peaceful and become attached,

225
00:22:44,000 --> 00:22:49,000
and you go statistical about their peace, meditation,

226
00:22:49,000 --> 00:22:52,000
meditation of that sort can't for you from suffering,

227
00:22:52,000 --> 00:22:57,000
without science, without knowledge,

228
00:22:57,000 --> 00:23:01,000
without realizing the truth and permanence of freeing non-self,

229
00:23:01,000 --> 00:23:07,000
that nothing is worth clinging to, even calm and peace and tranquility,

230
00:23:07,000 --> 00:23:13,000
without realizing that you can't become enlightened.

231
00:23:13,000 --> 00:23:15,000
So there you go.

232
00:23:15,000 --> 00:23:19,000
There's our bit of dhamma for tonight.

233
00:23:27,000 --> 00:23:30,000
Are you leaving tomorrow?

234
00:23:30,000 --> 00:23:32,000
Or soon?

235
00:23:32,000 --> 00:23:33,000
Yeah.

236
00:23:33,000 --> 00:23:34,000
Okay.

237
00:23:34,000 --> 00:23:37,000
Do you want to say hello to everyone?

238
00:23:37,000 --> 00:23:39,000
Hello, everyone.

239
00:23:39,000 --> 00:23:43,000
Tell us about what happened, tell us about a little bit about just what happened,

240
00:23:43,000 --> 00:23:45,000
why am I having you say hello?

241
00:23:45,000 --> 00:23:48,000
Well, I'm finishing, come close, come this side.

242
00:23:48,000 --> 00:23:56,000
I'm finishing my stay here and introduce yourself.

243
00:23:56,000 --> 00:23:58,000
My name is Will Will Stevenson.

244
00:23:58,000 --> 00:24:04,000
I'm from New York and I just completed my program.

245
00:24:04,000 --> 00:24:10,000
How is it? Very revealing for me.

246
00:24:10,000 --> 00:24:15,000
Now when I hear the talks, they mean different things to me,

247
00:24:15,000 --> 00:24:19,000
because I've directly experienced some things I had

248
00:24:19,000 --> 00:24:23,000
only an intellectual appreciation for before.

249
00:24:23,000 --> 00:24:30,000
I'm probably, I've learned about myself that I am one of the types of people

250
00:24:30,000 --> 00:24:35,000
that cling to the intellectual appreciation.

251
00:24:35,000 --> 00:24:43,000
I thought through knowledge that a greater appreciation for peace would come.

252
00:24:43,000 --> 00:24:47,000
But that is certainly not the case.

253
00:24:47,000 --> 00:24:55,000
And really only through direct observation of the simple things of life

254
00:24:55,000 --> 00:25:00,000
and a very kind of, I guess, myopic controlled situation,

255
00:25:00,000 --> 00:25:06,000
as it, you know, again, it's not what you'd said at tonight.

256
00:25:06,000 --> 00:25:09,000
It's not like I had aha moments.

257
00:25:09,000 --> 00:25:17,000
It's really just exposed to me that it's the tip of the iceberg, you know?

258
00:25:17,000 --> 00:25:22,000
So now I'm, we've moved from the unknown unknowns

259
00:25:22,000 --> 00:25:25,000
into the known unknowns.

260
00:25:25,000 --> 00:25:26,000
Yeah.

261
00:25:26,000 --> 00:25:27,000
Right.

262
00:25:27,000 --> 00:25:28,000
Tip of the iceberg.

263
00:25:28,000 --> 00:25:30,000
Well, that said that is really what the foundation course

264
00:25:30,000 --> 00:25:32,000
should just show you the iceberg and say,

265
00:25:32,000 --> 00:25:34,000
this is what you've got to work on.

266
00:25:34,000 --> 00:25:37,000
It's a little, oh, but it'll scare you.

267
00:25:37,000 --> 00:25:38,000
Yeah, yeah.

268
00:25:38,000 --> 00:25:39,000
Got the first step.

269
00:25:39,000 --> 00:25:41,000
It's a good way, you know?

270
00:25:41,000 --> 00:25:42,000
Well, thank you, Will.

271
00:25:42,000 --> 00:25:45,000
Thank you so much.

272
00:25:45,000 --> 00:25:47,000
There you go.

273
00:25:47,000 --> 00:25:48,000
You still alive?

274
00:25:48,000 --> 00:25:49,000
Yes.

275
00:25:49,000 --> 00:25:51,000
It's not.

276
00:25:51,000 --> 00:25:56,000
It's not such a scary thing to do the course.

277
00:25:56,000 --> 00:25:59,000
People do survive it.

278
00:25:59,000 --> 00:26:01,000
I've never had to bury anyone.

279
00:26:01,000 --> 00:26:05,000
So my teacher said, he'd always say, if you die,

280
00:26:05,000 --> 00:26:07,000
I'll do your funeral for free.

281
00:26:07,000 --> 00:26:10,000
I mean, the joke is that monks go to funerals.

282
00:26:10,000 --> 00:26:13,000
They do ceremonies for when people die.

283
00:26:13,000 --> 00:26:15,000
So, and it costs a lot of money.

284
00:26:15,000 --> 00:26:17,000
You pay the monks, you pay the monks,

285
00:26:17,000 --> 00:26:20,000
usually donations a little bit, but it's not just that.

286
00:26:20,000 --> 00:26:23,000
There's, of course, lots of costs that go in.

287
00:26:23,000 --> 00:26:27,000
I'll do the chanting free.

288
00:26:35,000 --> 00:26:36,000
Or I think it's more than that,

289
00:26:36,000 --> 00:26:42,000
he would probably arrange the funeral.

290
00:26:42,000 --> 00:26:43,000
We'd arrange a funeral.

291
00:26:43,000 --> 00:26:45,000
If anyone died meditating, we'd for sure.

292
00:26:45,000 --> 00:26:48,000
It's the funeral.

293
00:26:48,000 --> 00:26:50,000
Right, right, Robin?

294
00:26:50,000 --> 00:26:54,000
It's the least we could do.

295
00:26:54,000 --> 00:26:56,000
It's on our waiver.

296
00:26:56,000 --> 00:26:59,000
Our waiver says you have to be aware that meditation

297
00:26:59,000 --> 00:27:02,000
can lead to death among other things.

298
00:27:06,000 --> 00:27:08,000
We were discussing this.

299
00:27:08,000 --> 00:27:11,000
The question of how do we work on,

300
00:27:11,000 --> 00:27:16,000
how do we make sure to not get into legal battles and so on?

301
00:27:16,000 --> 00:27:19,000
How do we avoid having problems with men?

302
00:27:19,000 --> 00:27:21,000
Well, let's just put a waiver and say nice,

303
00:27:21,000 --> 00:27:25,000
complete to death and dismemberment and so on.

304
00:27:25,000 --> 00:27:28,000
And you're aware and you're not going to hold less responsible

305
00:27:28,000 --> 00:27:30,000
even if it kills you.

306
00:27:30,000 --> 00:27:33,000
So far, no one's died.

307
00:27:33,000 --> 00:27:36,000
Okay.

308
00:27:44,000 --> 00:27:47,000
Did I see a question there?

309
00:27:52,000 --> 00:27:54,000
I'm going to be glad.

310
00:27:54,000 --> 00:27:56,000
Can we take scientific knowledge as wisdom?

311
00:27:56,000 --> 00:27:58,000
Isn't wisdom all about good judgment

312
00:27:58,000 --> 00:28:00,000
and knowing what is beneficial?

313
00:28:00,000 --> 00:28:03,000
I mean, this is semantics.

314
00:28:03,000 --> 00:28:05,000
It depends what you mean by wisdom.

315
00:28:05,000 --> 00:28:09,000
I think the Buddha would, let me see.

316
00:28:09,000 --> 00:28:12,000
No, I think in orthodox Buddhism,

317
00:28:12,000 --> 00:28:15,000
yes, it would have to be something to do with Buddhism, actually.

318
00:28:15,000 --> 00:28:17,000
You'd have to be in line with the Buddha's teaching.

319
00:28:17,000 --> 00:28:23,000
So knowledge of the Adam Baum isn't actually wisdom.

320
00:28:23,000 --> 00:28:28,000
And so that's really, I may kind of mention

321
00:28:28,000 --> 00:28:31,000
that I said we're not obsessed with knowledge

322
00:28:31,000 --> 00:28:39,000
or obsessed with what is beneficial or focused on the results of good knowledge.

323
00:28:39,000 --> 00:28:55,000
Yeah, so they're all, I mean, they're also anonymous.

324
00:28:55,000 --> 00:28:57,000
They're not actually synonymous.

325
00:28:57,000 --> 00:28:59,000
I mean, all that nyanam means knowledge.

326
00:28:59,000 --> 00:29:01,000
It's very clear.

327
00:29:01,000 --> 00:29:05,000
Bunya, interestingly enough, is just nyan with a bat on the front.

328
00:29:05,000 --> 00:29:09,000
Bunya nyanah, the difference is the bat.

329
00:29:09,000 --> 00:29:11,000
Because the root is nyanah.

330
00:29:11,000 --> 00:29:16,000
Nyanah is turning the root to no into knowledge.

331
00:29:16,000 --> 00:29:21,000
And Bunya is adding an A on the end,

332
00:29:21,000 --> 00:29:23,000
discerning nyanah into nyanah.

333
00:29:23,000 --> 00:29:26,000
And then bat becomes separate.

334
00:29:26,000 --> 00:29:29,000
Bunya is actually, in the dictionary,

335
00:29:29,000 --> 00:29:30,000
it would be different.

336
00:29:30,000 --> 00:29:36,000
Nyanah will just be any type of knowledge, but Bunya is like wisdom.

337
00:29:36,000 --> 00:29:41,000
True knowledge, that kind of thing.

338
00:29:41,000 --> 00:29:45,000
Virya, Virya is a little bit different flavor.

339
00:29:45,000 --> 00:29:47,000
So Virya comes from Vid,

340
00:29:47,000 --> 00:29:51,000
which is where you get the word, Vedana.

341
00:29:51,000 --> 00:29:56,000
So Vedana means something that you experience.

342
00:29:56,000 --> 00:30:00,000
So Vidja is like experience.

343
00:30:00,000 --> 00:30:02,000
It's translated often, that's knowledge,

344
00:30:02,000 --> 00:30:04,000
and it's used as knowledge.

345
00:30:04,000 --> 00:30:09,000
But it's a knowledge that you get from experience.

346
00:30:09,000 --> 00:30:13,000
So it's a different, a whole different root.

347
00:30:13,000 --> 00:30:16,000
But culturally, even in the Buddhist time,

348
00:30:16,000 --> 00:30:18,000
it had come out to mean the same thing.

349
00:30:18,000 --> 00:30:21,000
Or be used in the same way, although there was something

350
00:30:21,000 --> 00:30:26,000
Vidja is a little bit more experiential, I guess,

351
00:30:26,000 --> 00:30:31,000
but special in a way.

352
00:30:31,000 --> 00:30:32,000
Or is it weight?

353
00:30:32,000 --> 00:30:36,000
Does it second Vidja?

354
00:30:36,000 --> 00:30:47,000
Vidja, yeah, yeah, Vidja.

355
00:30:47,000 --> 00:30:49,000
OK.

356
00:30:49,000 --> 00:30:51,000
We had a few more questions here on the website.

357
00:30:51,000 --> 00:30:55,000
Let's go through them quickly.

358
00:30:55,000 --> 00:30:59,000
How important is chanting to the meditation practice?

359
00:30:59,000 --> 00:31:02,000
Are there any signs when one reaches a state of true awareness?

360
00:31:02,000 --> 00:31:05,000
Those are two different questions.

361
00:31:05,000 --> 00:31:08,000
Janting is not really important, except the mantra

362
00:31:08,000 --> 00:31:10,000
that we use is kind of important.

363
00:31:10,000 --> 00:31:13,000
So meditation is kind of a chant.

364
00:31:13,000 --> 00:31:15,000
Say it's a pain, pain when you feel pain,

365
00:31:15,000 --> 00:31:17,000
but it's a different kind of chant because the object

366
00:31:17,000 --> 00:31:19,000
is what you're experiencing.

367
00:31:19,000 --> 00:31:22,000
So it keeps you focused on what you're experiencing.

368
00:31:22,000 --> 00:31:24,000
Any other kind of chanting is just

369
00:31:24,000 --> 00:31:26,000
going to be a type of meditation,

370
00:31:26,000 --> 00:31:29,000
sort of that is conceptual.

371
00:31:29,000 --> 00:31:32,000
Like, it'd be so Bhagavas, mindfulness of the Buddha.

372
00:31:32,000 --> 00:31:34,000
That's good, it's useful.

373
00:31:34,000 --> 00:31:39,000
But it's only conventionally useful.

374
00:31:39,000 --> 00:31:42,000
Are there any signs when one reaches a state of true awareness?

375
00:31:42,000 --> 00:31:44,000
Yes, when one reaches a state of true awareness,

376
00:31:44,000 --> 00:31:48,000
having realized Nibana, one will have a sense that something

377
00:31:48,000 --> 00:31:52,000
has changed, one will have a sense that things are not

378
00:31:52,000 --> 00:31:54,000
as they were before.

379
00:31:54,000 --> 00:31:56,000
One will have a clear understanding that something

380
00:31:56,000 --> 00:31:59,000
is missing, something has gone, something has changed,

381
00:31:59,000 --> 00:32:02,000
and also have a clear sense of what is left to do.

382
00:32:02,000 --> 00:32:08,000
So one will have generally a good moral ethic

383
00:32:08,000 --> 00:32:12,000
and will be able to differentiate between what is the path

384
00:32:12,000 --> 00:32:15,000
and what is not the path.

385
00:32:18,000 --> 00:32:22,000
And one will have perfect confidence in the right path

386
00:32:22,000 --> 00:32:25,000
and in Buddhist meditation because one will come

387
00:32:25,000 --> 00:32:30,000
to see that it actually does lead to the goal.

388
00:32:30,000 --> 00:32:33,000
Is there a proper way to use or incorporate prayer

389
00:32:33,000 --> 00:32:35,000
beads in our meditation practice?

390
00:32:35,000 --> 00:32:37,000
I'm unsure of the purpose of the beads

391
00:32:37,000 --> 00:32:38,000
think in advance.

392
00:32:38,000 --> 00:32:40,000
Well, they don't really have a purpose in our meditation.

393
00:32:40,000 --> 00:32:43,000
They were used sort of for counting, right?

394
00:32:43,000 --> 00:32:46,000
So certain meditations involve counting,

395
00:32:46,000 --> 00:32:50,000
like you're chanting the Buddha's name 108 times.

396
00:32:50,000 --> 00:32:54,000
So you do that one bead for every time.

397
00:32:54,000 --> 00:32:57,000
There's different conventional ways they can be used.

398
00:32:57,000 --> 00:32:58,000
It's just an art tradition.

399
00:32:58,000 --> 00:33:00,000
We don't use them.

400
00:33:03,000 --> 00:33:06,000
Are there other means, aside from walking meditation

401
00:33:06,000 --> 00:33:08,000
or cultivate effort with a hand motion?

402
00:33:08,000 --> 00:33:12,000
Didn't we talk about this? Didn't I answer this question?

403
00:33:12,000 --> 00:33:14,000
So there'd be some effort that you'd get from that,

404
00:33:14,000 --> 00:33:16,000
but it's not the same as walking.

405
00:33:20,000 --> 00:33:23,000
What are your thoughts on the formless Janis?

406
00:33:27,000 --> 00:33:29,000
I don't really think about them too much.

407
00:33:29,000 --> 00:33:32,000
Do you see any value in practicing these kinds of meditations?

408
00:33:32,000 --> 00:33:35,000
You seem to see it trying this kind of a long way around,

409
00:33:35,000 --> 00:33:38,000
achieving the more important goal inside.

410
00:33:38,000 --> 00:33:40,000
Yes, that's still my perspective.

411
00:33:47,000 --> 00:33:50,000
And quite about the stillness, silence of the mind and body

412
00:33:50,000 --> 00:33:51,000
while sitting.

413
00:33:59,000 --> 00:34:01,000
I don't see a question.

414
00:34:01,000 --> 00:34:04,000
Okay.

415
00:34:04,000 --> 00:34:07,000
Well, sitting there is space of observation.

416
00:34:07,000 --> 00:34:12,000
Sometimes those magical experiences occur after prolonged

417
00:34:12,000 --> 00:34:14,000
periods.

418
00:34:14,000 --> 00:34:17,000
I see them arise and fall.

419
00:34:22,000 --> 00:34:25,000
Yeah, well, I mean, I've talked about it in the imperfections of

420
00:34:25,000 --> 00:34:26,000
insight.

421
00:34:26,000 --> 00:34:28,000
Stillness, quiet is silence.

422
00:34:28,000 --> 00:34:31,000
Stillness is a, it's called passity.

423
00:34:31,000 --> 00:34:34,000
Still a very low level of insight.

424
00:34:34,000 --> 00:34:37,000
So if you feel calm or, you know, if you're quiet or still,

425
00:34:37,000 --> 00:34:41,000
you should say quiet, quiet or still, still.

426
00:34:41,000 --> 00:34:43,000
Just stay noting it.

427
00:34:43,000 --> 00:34:45,000
You haven't know what anything else that comes.

428
00:34:45,000 --> 00:34:48,000
So if you see something seeing, seeing a few years,

429
00:34:48,000 --> 00:34:51,000
if you feel bliss or rapture,

430
00:34:51,000 --> 00:34:53,000
and say feeling, feeling.

431
00:34:53,000 --> 00:34:59,000
Like everything else, it's impermanent suffering and

432
00:34:59,000 --> 00:35:00,000
no one's self.

433
00:35:00,000 --> 00:35:02,000
It's not going to satisfy you.

434
00:35:02,000 --> 00:35:05,000
It's not something you can cling to or something you should

435
00:35:05,000 --> 00:35:09,000
identify with as this is me, this I am.

436
00:35:09,000 --> 00:35:12,000
You should see it as impermanent and satisfying and

437
00:35:12,000 --> 00:35:13,000
uncontrollable.

438
00:35:13,000 --> 00:35:14,000
Then let it go.

439
00:35:14,000 --> 00:35:24,000
Okay, so there are other questions.

440
00:35:30,000 --> 00:35:34,000
Thank you, everyone, for coming.

441
00:35:34,000 --> 00:35:52,000
I wish you all a good night.

