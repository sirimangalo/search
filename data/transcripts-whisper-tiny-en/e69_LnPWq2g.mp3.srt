1
00:00:00,000 --> 00:00:13,000
Jesse asks, you've mentioned that depression comes from anger, but how about fear?

2
00:00:13,000 --> 00:00:24,000
Fear also is rooted in anger.

3
00:00:24,000 --> 00:00:36,000
I remember a discussion with a woman sometime back, and she was in fear and in sadness.

4
00:00:36,000 --> 00:00:49,000
We were talking about it and I said, you know, sadness and fear are actually rooted in anger.

5
00:00:49,000 --> 00:01:09,000
I said, well, look at your reaction now.

6
00:01:09,000 --> 00:01:15,000
It is rooted in anger.

7
00:01:15,000 --> 00:01:27,000
If you wanted to tell me that anger, well, anyway, it doesn't matter so much what she wanted to explain to me,

8
00:01:27,000 --> 00:01:39,000
the fact is that when there is within a person's mind, the disposition for anger,

9
00:01:39,000 --> 00:01:49,000
it is likely that feelings like fear, depression, sadness and all this can arise.

10
00:01:49,000 --> 00:01:57,000
In a mind where no disposition for anger is found anymore,

11
00:01:57,000 --> 00:02:11,000
there such things like experiences like fear, anger and distress and depression, sadness and so on,

12
00:02:11,000 --> 00:02:34,000
will not arise anymore.

