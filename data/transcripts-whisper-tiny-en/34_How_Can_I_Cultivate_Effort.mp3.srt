1
00:00:00,000 --> 00:00:06,000
How can I cultivate efforts? Which way can I cultivate efforts?

2
00:00:06,000 --> 00:00:11,000
It is hard for me to meditate. How do I make it easier?

3
00:00:11,000 --> 00:00:13,000
Meditation is hard.

4
00:00:13,000 --> 00:00:20,000
The satisfaction with quality or amount of practice is the recognition of imperfection.

5
00:00:20,000 --> 00:00:23,000
You just have to work on becoming more perfect.

6
00:00:23,000 --> 00:00:27,000
It may take years or lifetimes to become a perfect meditator.

7
00:00:27,000 --> 00:00:31,000
There is no quick fix and no appeal that one can take,

8
00:00:31,000 --> 00:00:36,000
but suddenly gives one the quality is necessary to make it feel effortless.

9
00:00:36,000 --> 00:00:39,000
Effort is something one has to work at.

10
00:00:39,000 --> 00:00:42,000
Asking for the ways to make it easier,

11
00:00:42,000 --> 00:00:46,000
it's probably not the best way to cultivate efforts.

12
00:00:46,000 --> 00:00:53,000
Look at it as a chance to find ways to become stronger than the experience easier.

13
00:00:53,000 --> 00:01:00,000
Association with good people can help the Buddha recommended us to associate with good people

14
00:01:00,000 --> 00:01:07,000
as well as when one associates with good people, good qualities increase.

