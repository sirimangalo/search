1
00:00:00,000 --> 00:00:06,060
You want to do mochstick to their clique in most monasteries?

2
00:00:06,060 --> 00:00:10,160
And is there politics among mochts, too?

3
00:00:10,160 --> 00:00:17,120
What are the things that not expected when you were dying?

4
00:00:17,120 --> 00:00:20,080
Oh, and use this question.

5
00:00:20,080 --> 00:00:25,920
Is it really good for me to answer this?

6
00:00:25,920 --> 00:00:27,480
What do you think?

7
00:00:27,480 --> 00:00:34,480
Is this going to help people's meditation if they hear the answer?

8
00:00:34,480 --> 00:00:40,760
Perhaps if it shows us what guided you position at that time,

9
00:00:40,760 --> 00:00:49,920
I think everyone has their own personal story as to why and being certain things happen in their lives.

10
00:00:49,920 --> 00:00:52,720
How would I answer the last question?

11
00:00:52,720 --> 00:00:57,520
What are the things you had not expected when you were dying?

12
00:00:57,520 --> 00:01:04,040
Because honestly, I didn't have very high expectations when I were dying.

13
00:01:04,040 --> 00:01:10,520
Partly because I had no knowledge of Buddhism, which I'm sorry.

14
00:01:10,520 --> 00:01:12,240
Sorry, ordained was much later.

15
00:01:12,240 --> 00:01:19,560
When I first started practicing Buddhism, I had no expectations or no knowledge of Buddhism.

16
00:01:19,560 --> 00:01:27,560
And so, I don't know that ordination is a different issue.

17
00:01:27,560 --> 00:01:34,160
It's maybe the more interesting one, but if I could just get the other part out of the way is that...

18
00:01:34,160 --> 00:01:37,480
First of all, I had no expectations of how monks should act.

19
00:01:37,480 --> 00:01:46,680
I was offering monks food in the afternoon and it was confused as to why they weren't accepting it.

20
00:01:46,680 --> 00:01:56,640
And also because I saw a lot of things that, before I ordained, that totally made it totally,

21
00:01:56,640 --> 00:02:01,280
perfectly clear that, at least in the area of the world where I was at the time,

22
00:02:01,280 --> 00:02:08,280
and the Buddhist world that I was revolving around, didn't have very high standards.

23
00:02:08,280 --> 00:02:18,280
And so, I had a lot of problems and so on. My first teachers were laypeople and that added to it

24
00:02:18,280 --> 00:02:26,280
because they're understanding and not understanding, but they're one of the things that they...

25
00:02:26,280 --> 00:02:42,280
They adhere to or hold strongly as the idea that you don't have to ordain that there's not any intrinsic benefit in ordaining,

26
00:02:42,280 --> 00:02:49,280
which is technically true. A layperson can be as pure as a monk, but they emphasized it.

27
00:02:49,280 --> 00:03:00,280
And so, that sort of emphasis leads you to have some sort of disregard for the monastic life in the monkhood.

28
00:03:00,280 --> 00:03:07,280
And so, I didn't have expectations. I saw, you know, yeah, a lot of monks are just corrupt and so on.

29
00:03:07,280 --> 00:03:14,280
And so on. But after I ordained, you see, this is two years later.

30
00:03:14,280 --> 00:03:23,280
So in my situation, it's not that profound of an experience.

31
00:03:23,280 --> 00:03:37,280
After I ordained, one of the things I didn't expect was the fact that I had to be different or as different,

32
00:03:37,280 --> 00:03:42,280
as I had to be, or in the ways that I had to be different from laypeople.

33
00:03:42,280 --> 00:03:49,280
Of course, everyone has the idea and really feels that they're becoming something different and knows that you have to be different as a monk.

34
00:03:49,280 --> 00:04:00,280
But I caught myself acting a lot like a layperson in many ways and working very closely with laypeople because my teachers were still laypeople.

35
00:04:00,280 --> 00:04:07,280
And that kind of took me by surprise, the fact that I really shouldn't be doing that.

36
00:04:07,280 --> 00:04:18,280
Because I shouldn't have been in such close relations with laypeople and I certainly shouldn't have been acting so much like a layperson.

37
00:04:18,280 --> 00:04:27,280
It took me by surprise, I don't know that it took me by surprise, but one thing that slowly dawned on me.

38
00:04:27,280 --> 00:04:35,280
And yeah, it did take me by surprise when I found myself unable to break rules.

39
00:04:35,280 --> 00:04:40,280
When I found myself unable to justify breaking the rules anymore.

40
00:04:40,280 --> 00:04:48,280
Which I know good monks who do justify breaking the rules, minor rules, their whole lives.

41
00:04:48,280 --> 00:04:57,280
But because of my Western upbringing and because of the stubborn torus that I am,

42
00:04:57,280 --> 00:05:07,280
just the kind of person that I am, I've got this most Western monks get this kind of irreverence for authority.

43
00:05:07,280 --> 00:05:20,280
But ambition or I don't know what the right word is, the zeal to do something a thousand percent.

44
00:05:20,280 --> 00:05:32,280
And I guess those two qualities exist in Western monks to different degrees or in monks in general to different degrees.

45
00:05:32,280 --> 00:05:38,280
But with Westerners, some Westerners have just this total disregard for authority, so don't care about the rules.

46
00:05:38,280 --> 00:05:41,280
But don't have the ambition to do things perfectly.

47
00:05:41,280 --> 00:05:43,280
So they say, yeah, meditation is enough.

48
00:05:43,280 --> 00:05:46,280
I mean, what do you need all these rules for?

49
00:05:46,280 --> 00:05:51,280
But people like me with both of them which are kind of conflicting.

50
00:05:51,280 --> 00:05:55,280
You don't care about the fact that everyone else is breaking the rules.

51
00:05:55,280 --> 00:06:00,280
You want to do it fully in a hundred or a thousand percent.

52
00:06:00,280 --> 00:06:09,280
And often the fact that everyone else is breaking the rules will obnoxiously lead you to obnoxiously decide to keep the rules.

53
00:06:09,280 --> 00:06:16,280
We're flying fully in the face of your monastic community, which is really not a fun situation.

54
00:06:16,280 --> 00:06:25,280
That's kind of fun. It's fun to be hated by everyone in your monastery in that sense, which I certainly have been in my time.

55
00:06:25,280 --> 00:06:43,280
Do you feel that some of the archaic practices, rules and regulations count Buddhism, do you feel since it's 25, 26 hundred years?

56
00:06:43,280 --> 00:06:50,280
Do you think some of those ideas are detrimental to the girls? More than that Buddhism?

57
00:06:50,280 --> 00:06:53,280
No, I don't think so.

58
00:06:53,280 --> 00:06:58,280
No, I think what's detrimental is people's belief that it's detrimental.

59
00:06:58,280 --> 00:07:00,280
People's belief that it has to change.

60
00:07:00,280 --> 00:07:13,280
Because if we decided that no, they're good and they don't have to change, then the Buddhist culture would very quickly and very easily work to accommodate them.

61
00:07:13,280 --> 00:07:17,280
They're not, the Buddhist monastic rules are not difficult to accommodate.

62
00:07:17,280 --> 00:07:25,280
Sometimes you might have to scratch or even break knowing that you're breaking them in certain situations.

63
00:07:25,280 --> 00:07:28,280
You don't have to, but sometimes you will.

64
00:07:28,280 --> 00:07:37,280
For example, I might stay in an apartment building, and some people might say that's breaking the rule.

65
00:07:37,280 --> 00:07:40,280
I might even break the rule by staying in, I don't know.

66
00:07:40,280 --> 00:07:47,280
I mean, there are ways around some of the rules that are kind of silly. You're not allowed to stay in the same residence as a layperson.

67
00:07:47,280 --> 00:07:51,280
But what that means is you're not allowed to be lying down at sunrise.

68
00:07:51,280 --> 00:07:56,280
So if you get up and sit up before sunrise, you've not spent the night with a layperson.

69
00:07:56,280 --> 00:08:01,280
For example, a layman, a laywoman, you can't even lie down together.

70
00:08:01,280 --> 00:08:04,280
But that's not really the point here.

71
00:08:04,280 --> 00:08:13,280
The actual rules themselves, I can't think of even one where I would really say, definitely, that has to go.

72
00:08:13,280 --> 00:08:16,280
Most especially, they're not touching money one.

73
00:08:16,280 --> 00:08:24,280
I mean, that's the big one that people harp on, but that's such an intrinsic, even today, at such an intrinsic part of the monastic life.

74
00:08:24,280 --> 00:08:32,280
And it's really not difficult if the problem is finding a community that is supportive.

75
00:08:32,280 --> 00:08:58,280
One thing I will admit is a lot of the modes and behaviors that are expected of Buddhist monks are under the assumption that you're living in a community in a society that provides arms to holy people.

76
00:08:58,280 --> 00:09:10,280
So, in India, not just Buddhists, but holy people are not even holy, but people who are trying to become holy or living in a holy way like shamans, shrahmanas,

77
00:09:10,280 --> 00:09:13,280
were able to get by.

78
00:09:13,280 --> 00:09:15,280
And even today, are able to get by in India.

79
00:09:15,280 --> 00:09:25,280
So these guys go around begging for money or begging for food in many different sects in India, not just Buddhists.

80
00:09:25,280 --> 00:09:35,280
Now Buddhists don't, but that's what I'm facing now going to Canada, because I can't go to Canada and just go arms around on the street.

81
00:09:35,280 --> 00:09:39,280
I can't go to Canada in the same way as I would go to Thailand.

82
00:09:39,280 --> 00:09:47,280
If I want to go to Thailand, I just go to Thailand, find some place and decide that I'm going to live there, and nowadays it's much more difficult.

83
00:09:47,280 --> 00:09:49,280
You can't really do it, but in all the days, you could just do that.

84
00:09:49,280 --> 00:09:57,280
Set up your tent, set up your, you know, put your robe over you at night, and you get arms food, you get enough food.

85
00:09:57,280 --> 00:10:00,280
In Canada, I won't get that.

86
00:10:00,280 --> 00:10:08,280
And so, it's a challenge, but you look at the two different sides of the coin.

87
00:10:08,280 --> 00:10:10,280
Trying to keep the rules is a challenge.

88
00:10:10,280 --> 00:10:14,280
Breaking the rules is a disappointment, is a corruption.

89
00:10:14,280 --> 00:10:19,280
It means your monk life is going to suffer.

90
00:10:19,280 --> 00:10:27,280
In the sense of how you look, how you present, what a reckless is, what a monk is,

91
00:10:27,280 --> 00:10:36,280
how people relate to you, how they respect the life of a dedicated Buddhist meditator,

92
00:10:36,280 --> 00:10:40,280
how they respect the teachings, how they look at the teachings and so on,

93
00:10:40,280 --> 00:10:48,280
and how you yourself live in terms of your peace of mind and your ability to grow.

94
00:10:48,280 --> 00:10:55,280
So, the challenges are far more appealing, right?

95
00:10:55,280 --> 00:10:58,280
And so, it's not two sets of challenges.

96
00:10:58,280 --> 00:11:07,280
There's not much challenge in breaking the rules, but suddenly you have corruption sneaking in in terms of how people look at you,

97
00:11:07,280 --> 00:11:14,280
how they look at the dhamma, how the community is set up, and how people feel about what they're doing.

98
00:11:14,280 --> 00:11:23,280
Most people in the West don't get this because they've never really had the experience of either being a monk

99
00:11:23,280 --> 00:11:26,280
or even living with a monastic community.

100
00:11:26,280 --> 00:11:31,280
I don't know that I'll ever provide that because look at me, I use computers and stuff.

101
00:11:31,280 --> 00:11:40,280
But what I do provide is people who are interested in it because some people really do chastise me for not touching money,

102
00:11:40,280 --> 00:11:53,280
but for those people who get into it, especially Asian Buddhists who have never really been accustomed to monks who don't keep the rules,

103
00:11:53,280 --> 00:12:06,280
find it refreshing to know that the monk that they're dealing with, this teacher that they have has no money and doesn't take money and doesn't have any interest in money

104
00:12:06,280 --> 00:12:16,280
and relies entirely, you know, subsists entirely upon what he is provided with, what is donated to him for that day.

105
00:12:16,280 --> 00:12:33,280
So, he's not able to choose his meals, he's not able to differentiate between food or keep or hold on to these sorts of things.

106
00:12:33,280 --> 00:12:48,280
And so, I found it to be an incredible support for my practice and a incredible support for my work as well because people really want to support me.

107
00:12:48,280 --> 00:12:58,280
For two reasons, I'm thinking of Asian Buddhists who really want to support me for the fact that I teach meditation,

108
00:12:58,280 --> 00:13:04,280
but also very much for the fact that I keep the rules strictly because they know that they can trust me,

109
00:13:04,280 --> 00:13:11,280
or they're more likely to believe that they can trust me, they feel it's more likely that they can be trusted.

110
00:13:11,280 --> 00:13:25,280
So, so much of that concept in the Western world is so foreign to us outside of the Catholic faith, which still has nuns and monks, monsters and so forth.

111
00:13:25,280 --> 00:13:31,280
I think they have become so modernized in most of their dealings with the public.

112
00:13:31,280 --> 00:13:41,280
There's a few there, I guess they're close to it, but most of them are out in public. They're quite different from Buddhist monks.

113
00:13:41,280 --> 00:13:52,280
They're active, particularly in other countries, but outside of that, so many of the people in the United States are of the opinion that any teacher that they may have,

114
00:13:52,280 --> 00:14:05,280
that's a preacher around by and so forth. First of all, they're usually married. You have to read my normal people, hold down the job on the people, they have a normal home, a normal lifestyle sort of thing.

115
00:14:05,280 --> 00:14:14,280
And that's what the populace of the Western world looks towards, it looks to just inspiration for their teaching.

116
00:14:14,280 --> 00:14:22,280
And that's what they have outside of that norm becomes, it's totally foreign to us.

117
00:14:22,280 --> 00:14:33,280
That's a good reason I think why they would be more comfortable with a lay Buddhist teacher, even in which you find in some circles.

118
00:14:33,280 --> 00:14:51,280
But one I think quick defense to that or quick response to that is what we're teaching is quite different. What we're teaching under cuts away or tears away the concept of marriage.

119
00:14:51,280 --> 00:15:07,280
Sure, as a Buddhist, you can be married and have sex and so on, but it's quite clear even from the outset that that's a part of what we're trying to give up where it's not in Christianity, it's not in Judaism, it's not in any of these religions.

120
00:15:07,280 --> 00:15:28,280
How clear Buddhism is about giving up attachment. You can have a teacher who plays tennis and has a wife or a husband and a car and money and television and so on.

121
00:15:28,280 --> 00:15:46,280
But unlike religions that don't specify the need to give up attachments, here we have a conflict, right? Here we have a teacher who's teaching giving up attachments and clearly isn't interested in giving up attachments or is only going halfway.

122
00:15:46,280 --> 00:16:05,280
So yeah, they can teach and they could be great people, great Buddhists, but they can't be considered to be the ideal or to be striving for the ideal because they're not striving or could be but not obviously striving.

123
00:16:05,280 --> 00:16:15,280
And it's not clear because lay people can be good teachers and can be perfect Buddhists, but you see what I'm getting at it. There's a lot more required in Buddhism.

124
00:16:15,280 --> 00:16:35,280
I have understanding and agree with that, but I'm the same. So many people I think in the Western world are the opinion right or wrong, but how can you tell me and live my life and relate and you're not away from me.

125
00:16:35,280 --> 00:16:48,280
You have an experience, they look at that a lot of times, I think it's a great priest. You're not a father, you're not a parent, you know, you're supported by the church, you don't work for your own.

126
00:16:48,280 --> 00:17:04,280
How can you come back and try to teach and tell me you're not experiencing that. You don't have the experience to tell me as a married parent working parent, you know, there's almost like a disconnect.

127
00:17:04,280 --> 00:17:20,280
Well, for sure, and I totally am willing to, on the one hand admit that, and I could even entertain an answer by saying, yeah, sure, because that's not what I'm telling you to do. I'm saying, give up your married life and become a monk.

128
00:17:20,280 --> 00:17:41,280
So you can take that tag. Most Buddhist monks and Buddhists go the other way and claim to have some knowledge to provide for married life, which is kind of disappointing because they go far too much to that degree and they say, yes, yes, I'll teach you how to be a good lay person and you've got a good point there.

129
00:17:41,280 --> 00:17:55,280
But I think more important than that is, how can you do that when that's, you know, going in the wrong direction? You know, yes, you can accept the fact that they're lay people with wives and children and husbands and children and so on.

130
00:17:55,280 --> 00:18:20,280
But it's clearly not what you should be preaching. Now, or it should not be all of what you're preaching. And if someone has a really, really bad married life, you should really include, at least include in your teaching, the option that they might give it up and become a monk because that really is what the Buddha prescribed as the ideal.

131
00:18:20,280 --> 00:18:43,280
There are other arguments you can give to that. I think the fact that a person is not, you know, there was, I read this thing where this man who was a bachelor was very able to describe married life because he was outside of it because he had never been married.

132
00:18:43,280 --> 00:19:00,280
And so he was able to show people what they couldn't see because they were looking at it from the inside. And there's that argument. I don't, I'm not going to push that sort of argument. I'm fully willing to accept the fact that I don't have much knowledge of how to raise children.

133
00:19:00,280 --> 00:19:14,280
But it doesn't seem to get that much in the way and I don't feel feel, I do feel quite confident giving general Buddhist advice to parents and children.

134
00:19:14,280 --> 00:19:25,280
But, you know, as I've said before, in the end, it just gets too complicated for me and I just say, you know, look, if you really want the answer, it's to give this all up and don't get involved with it.

135
00:19:25,280 --> 00:19:31,280
You know, how can, how will I raise my children, don't have children?

136
00:19:31,280 --> 00:19:33,280
Yeah.

137
00:19:33,280 --> 00:20:02,280
Okay, wait, let me stop this one.

