1
00:00:00,000 --> 00:00:20,440
Hello?

2
00:00:20,440 --> 00:00:23,080
I'm Justin.

3
00:00:53,080 --> 00:00:58,080
Hi, Robyn.

4
00:00:58,080 --> 00:01:01,080
Hey, Bunton.

5
00:01:01,080 --> 00:01:05,080
You're not doing yet.

6
00:01:05,080 --> 00:01:08,080
Like that caught in a storm.

7
00:01:08,080 --> 00:01:12,080
Lost the trees down on the road.

8
00:01:12,080 --> 00:01:17,080
Are you coming alone or is your head spinning?

9
00:01:17,080 --> 00:01:24,080
I'm all up on this time from work this week.

10
00:01:24,080 --> 00:01:50,080
That's a bit on that storm.

11
00:01:50,080 --> 00:01:57,080
Here's a wrong microphone.

12
00:01:57,080 --> 00:02:08,080
So tonight's quote is about

13
00:02:08,080 --> 00:02:13,080
eating.

14
00:02:13,080 --> 00:02:20,080
I know it's about livelihood to read at the end.

15
00:02:20,080 --> 00:02:26,080
That one makes one's living specifically

16
00:02:26,080 --> 00:02:28,080
how religious people make their living,

17
00:02:28,080 --> 00:02:33,080
what religious people focus on.

18
00:02:33,080 --> 00:02:40,080
There are religious practices that are low.

19
00:02:40,080 --> 00:02:51,080
They're just practices that are not worthy of one who is

20
00:02:51,080 --> 00:02:55,080
one who is seeking to become free from suffering.

21
00:02:55,080 --> 00:03:05,080
I'm not worthy of a Buddhist monk at any rate.

22
00:03:05,080 --> 00:03:10,080
So there's looking down and there's looking up, looking down,

23
00:03:10,080 --> 00:03:15,080
looking at the signs and that mundane signs,

24
00:03:15,080 --> 00:03:26,080
looking up, looking at divine signs, also not good.

25
00:03:26,080 --> 00:03:36,080
Those who act as messengers and errands boys.

26
00:03:36,080 --> 00:03:45,080
This is one thing that monks aren't allowed to do or not allowed to run errands for people.

27
00:03:45,080 --> 00:03:52,080
Because we are living up the charity of others.

28
00:03:52,080 --> 00:04:02,080
We have given up financial security and

29
00:04:02,080 --> 00:04:15,080
some solubility in favor of the homeless life.

30
00:04:15,080 --> 00:04:22,080
So we rely upon charity.

31
00:04:22,080 --> 00:04:28,080
If we start working for people,

32
00:04:28,080 --> 00:04:37,080
first of all, it's going against our determination to be free from all that.

33
00:04:37,080 --> 00:04:46,080
But it's also setting one up for obligation.

34
00:04:46,080 --> 00:04:50,080
So you ingratiate yourself with people by doing the errands.

35
00:04:50,080 --> 00:04:55,080
And then they give you food and then they come to

36
00:04:55,080 --> 00:05:00,080
think of it in a different way rather than being charity.

37
00:05:00,080 --> 00:05:07,080
It's wages, the dynamic breaks down.

38
00:05:07,080 --> 00:05:14,080
It's no longer a religious thing, it's our employment.

39
00:05:14,080 --> 00:05:24,080
The sparse palmistry, I don't know how that works,

40
00:05:24,080 --> 00:05:27,080
there's another category.

41
00:05:27,080 --> 00:05:30,080
But that's looking at all.

42
00:05:30,080 --> 00:05:33,080
So the going on errands is looking at the four directions,

43
00:05:33,080 --> 00:05:36,080
even looking at the four directions.

44
00:05:36,080 --> 00:05:43,080
It says a symbol, a symbolism.

45
00:05:43,080 --> 00:05:53,080
Eating and looking in four directions is a metaphor for living religious life as a

46
00:05:53,080 --> 00:05:58,080
go between or an errand boy.

47
00:05:58,080 --> 00:06:03,080
And looking at the points between the metaphor for palmistry,

48
00:06:03,080 --> 00:06:06,080
it's okay.

49
00:06:06,080 --> 00:06:31,080
I guess that has to do with the body maybe.

50
00:06:31,080 --> 00:06:46,080
I seek my food rightly and rightly do I eat it after I have sought it.

51
00:06:46,080 --> 00:06:59,080
So rightfully means without expectation, without insinuation or

52
00:06:59,080 --> 00:07:10,080
it's simply because people want to give and in return,

53
00:07:10,080 --> 00:07:18,080
leading life that is pure and setting an example and offering teaching,

54
00:07:18,080 --> 00:07:23,080
religious teaching, such as the one thing that monks and religious people

55
00:07:23,080 --> 00:07:33,080
should do for the people.

56
00:07:33,080 --> 00:07:43,080
Offering them apart in the spiritual life.

57
00:07:43,080 --> 00:08:03,080
Sort of an interesting quote from an asterisk, but not much else.

58
00:08:03,080 --> 00:08:14,080
If you have questions today, let's look at some questions.

59
00:08:14,080 --> 00:08:34,080
Let's see, for a back do I have to go some way back here.

60
00:08:34,080 --> 00:08:37,080
Too much chatting we've lost on the questions.

61
00:08:37,080 --> 00:08:39,080
Too much chatting.

62
00:08:39,080 --> 00:08:47,080
This is a chat box.

63
00:08:47,080 --> 00:08:52,080
Some monks sleep in the floor on a bed made or a bed made of rock.

64
00:08:52,080 --> 00:08:57,080
Do you do the same?

65
00:08:57,080 --> 00:09:16,080
I don't know how healthy that would be.

66
00:09:16,080 --> 00:09:30,080
Living on the floor is good on a carpet or so on.

67
00:09:30,080 --> 00:09:36,080
You don't sleep too much.

68
00:09:36,080 --> 00:09:54,080
We should guard our mind.

69
00:09:54,080 --> 00:10:14,080
It's better to meditate after waking up or after breakfast.

70
00:10:14,080 --> 00:10:24,080
Especially in the morning because it's hard to focus and the stomach is not settled until you've

71
00:10:24,080 --> 00:10:29,080
had some porridge or oatmeal for breakfast.

72
00:10:29,080 --> 00:10:48,080
It's called a simple plain breakfast to calm the stomach.

73
00:10:48,080 --> 00:11:02,080
It might be in the tungus, I'm not sure.

74
00:11:02,080 --> 00:11:07,080
Eating first is useful.

75
00:11:07,080 --> 00:11:14,080
Are the hindrance is viewed as hindering equanimity or hindering concentration or hindering any peaceful

76
00:11:14,080 --> 00:11:16,080
positive qualities of mind state?

77
00:11:16,080 --> 00:11:19,080
I think specifically it's about hindering concentration.

78
00:11:19,080 --> 00:11:22,080
It's a good question.

79
00:11:22,080 --> 00:11:25,080
In general they are hindrances.

80
00:11:25,080 --> 00:11:27,080
They hinder everything.

81
00:11:27,080 --> 00:11:30,080
They're in general bad.

82
00:11:30,080 --> 00:11:42,080
The orthodox answer would be the word hindrance is used to indicate the hindering of concentration.

83
00:11:42,080 --> 00:11:47,080
That's the hindering of wisdom as well because concentration means to wisdom.

84
00:11:47,080 --> 00:11:54,080
Or focus you might say rather than concentration.

85
00:11:54,080 --> 00:11:59,080
But the hindr positive qualities of mind states in general.

86
00:11:59,080 --> 00:12:03,080
They'll even hinder success in life in a mundane sense.

87
00:12:03,080 --> 00:12:13,080
You can't succeed if you have used five or it's harder to succeed with them.

88
00:12:13,080 --> 00:12:19,080
Knead does not mean nibana, some kind of gut it.

89
00:12:19,080 --> 00:12:21,080
No, no, no.

90
00:12:21,080 --> 00:12:24,080
Maybe that's a common to have answer.

91
00:12:24,080 --> 00:12:27,080
But nnead just means out or away.

92
00:12:27,080 --> 00:12:33,080
What are nnead means to word?

93
00:12:33,080 --> 00:12:35,080
That's not the real entomology.

94
00:12:35,080 --> 00:12:38,080
Maybe the commentary talks about that.

95
00:12:38,080 --> 00:12:43,080
Give you the real entomology.

96
00:12:43,080 --> 00:13:12,080
It comes from the verb, nnead, it's a verb, it's nothing to do with nnead bana.

97
00:13:12,080 --> 00:13:26,080
Let's look at the commentary.

98
00:13:26,080 --> 00:13:35,080
Nnead, what are nana?

99
00:13:35,080 --> 00:14:00,080
It's a good example, the nnead at the beginning is a prefix meaning out or away or out from

100
00:14:00,080 --> 00:14:12,080
near means without or out of or away.

101
00:14:12,080 --> 00:14:14,080
Water means to like to word.

102
00:14:14,080 --> 00:14:19,080
It's probably the same as the English word to word away something.

103
00:14:19,080 --> 00:14:23,080
So the word means to word away.

104
00:14:23,080 --> 00:14:33,080
The hindrance is something that keeps you away, keeps you from nnead water nana.

105
00:14:33,080 --> 00:14:49,080
Nirvana, nnead bana is away from or out of bondage, free from bondage, kind of thing.

106
00:14:49,080 --> 00:14:53,080
The move isn't happening yet.

107
00:14:53,080 --> 00:15:01,080
It's happening Saturday, I guess we're doing some packing before then.

108
00:15:01,080 --> 00:15:16,080
Robin was going to be here today, but she got caught up in her still.

109
00:15:16,080 --> 00:15:19,080
Except boxes of food and tea from different countries.

110
00:15:19,080 --> 00:15:22,080
I can't accept food.

111
00:15:22,080 --> 00:15:29,080
I also can't accept money.

112
00:15:29,080 --> 00:15:32,080
So the best way is through our organization.

113
00:15:32,080 --> 00:15:37,080
You can go to our support page and look at ways to support there.

114
00:15:37,080 --> 00:15:45,080
As far as food for me, I've been accepting gift cards to subway or pita pit.

115
00:15:45,080 --> 00:15:49,080
To our Starbucks or Tim Hortons.

116
00:15:49,080 --> 00:15:54,080
And these gift cards can all be purchased online.

117
00:15:54,080 --> 00:16:02,080
If you purchase them online, the company sends me an e-gift or sends a card in the mail or something.

118
00:16:02,080 --> 00:16:04,080
There's different ways to do that.

119
00:16:04,080 --> 00:16:08,080
But I'm not sure how much I'll be using that in the future because where we're living,

120
00:16:08,080 --> 00:16:11,080
there are not any convenient stores like that.

121
00:16:11,080 --> 00:16:20,080
So we'll either hopefully try and get a steward, someone to come and stay and prepare food.

122
00:16:20,080 --> 00:16:22,080
Or I'll go to the local.

123
00:16:22,080 --> 00:16:29,080
We'll try and set something up at the local restaurant so that I have food there.

124
00:16:29,080 --> 00:16:34,080
Hopefully we'll be able to stay alive a little longer.

125
00:16:34,080 --> 00:16:50,080
Okay, please use the green or orange button to preface your questions.

126
00:16:50,080 --> 00:17:01,080
Otherwise, I will be able to find it.

127
00:17:01,080 --> 00:17:12,080
Could you please speak to how the layperson can make a living with guidelines to follow for those that do touch money?

128
00:17:12,080 --> 00:17:25,080
Yeah, I mean, on the one hand, you have to understand that there aren't going to be too many concrete guidelines.

129
00:17:25,080 --> 00:17:31,080
Because again, it's not so much about what you do, but about how you do it.

130
00:17:31,080 --> 00:17:40,080
That being said, there are some basic concrete guidelines, but they're very much common sense from a Buddhist point of view.

131
00:17:40,080 --> 00:17:45,080
So any livelihood that breaks the five precepts is wrong.

132
00:17:45,080 --> 00:17:55,080
Any livelihood that directly involves substances that harm individuals as well.

133
00:17:55,080 --> 00:18:10,080
Selling poison, selling weapons, selling animals, selling poisons, weapons, living beings,

134
00:18:10,080 --> 00:18:17,080
and toxicants, I guess, is the fifth, right?

135
00:18:17,080 --> 00:18:26,080
This is all wrong.

136
00:18:26,080 --> 00:18:51,080
Anything that's involved, so that if it comes down to some extent common sense, what you're doing is involved in harming,

137
00:18:51,080 --> 00:18:58,080
or if you have to do harmful things to make a living, then it's problematic.

138
00:18:58,080 --> 00:19:06,080
I guess I would say many types of livelihood are indirectly involved in unwholesome things,

139
00:19:06,080 --> 00:19:11,080
or involved in unwholesome things that are mild.

140
00:19:11,080 --> 00:19:15,080
Even entertainment isn't considered the greatest livelihood.

141
00:19:15,080 --> 00:19:29,080
Because it's involved in unwholesome desire, there's desire involved, playing with people's emotions, that kind of thing.

142
00:19:29,080 --> 00:19:34,080
You use quantum physics to support Buddhism, something that you find compelling.

143
00:19:34,080 --> 00:19:41,080
As Buddhism believe in a single universe or a multiverse or other.

144
00:19:41,080 --> 00:19:45,080
Buddhism doesn't have too many beliefs, and it isn't focused on beliefs.

145
00:19:45,080 --> 00:19:51,080
Buddhism is much more about the truth of reality, which is very much in the present moment here and now.

146
00:19:51,080 --> 00:19:55,080
Something to do with far away things.

147
00:19:55,080 --> 00:19:58,080
That's to do with one's own experience.

148
00:19:58,080 --> 00:20:02,080
Within this six-foot frame, our universe has no size.

149
00:20:02,080 --> 00:20:09,080
Our universe is not singular, multiple, our universe is an experience.

150
00:20:09,080 --> 00:20:16,080
It is a series of experiences.

151
00:20:16,080 --> 00:20:18,080
That's what Buddhism is.

152
00:20:18,080 --> 00:20:23,080
That's what our tradition of Buddhism focuses on.

153
00:20:23,080 --> 00:20:29,080
What do you think of Titignet Han and his monasteries and the plum village monasteries?

154
00:20:29,080 --> 00:20:31,080
And blue-ciff monasteries?

155
00:20:31,080 --> 00:20:33,080
Sorry, I don't think too much about them.

156
00:20:33,080 --> 00:20:38,080
I mean, I don't think about them too much.

157
00:20:38,080 --> 00:20:42,080
It's okay to directly try to notice the elements.

158
00:20:42,080 --> 00:20:47,080
When I know it has a rising and falling, I get a mental picture to belly rising and falling.

159
00:20:47,080 --> 00:20:50,080
If it's not happening, I focus on the attention.

160
00:20:50,080 --> 00:20:52,080
You can just say tense if you want.

161
00:20:52,080 --> 00:20:58,080
But if you get a mental picture, you just say seeing, seeing.

162
00:20:58,080 --> 00:21:10,080
It's fine that it for it not to be, for it not to be amenable to your will when you see the pictures you see.

163
00:21:10,080 --> 00:21:16,080
I mean, the Buddha didn't require you to say things like earth or earth or air or so on.

164
00:21:16,080 --> 00:21:20,080
He said, you can't go walk a tummy deep a tummy deep a tummy deep.

165
00:21:20,080 --> 00:21:24,080
When going, when knows I go, I'm walking.

166
00:21:24,080 --> 00:21:28,080
When you say walking, you get a picture of your feet, no.

167
00:21:28,080 --> 00:21:36,080
Walking is meant to be a description of the feeling of the elements.

168
00:21:36,080 --> 00:21:43,080
But you still say walking or you stay sitting or so on.

169
00:21:43,080 --> 00:21:49,080
I wouldn't worry too much about that.

170
00:21:49,080 --> 00:21:55,080
But if you do see the image, you would see seeing.

171
00:21:55,080 --> 00:22:02,080
When mind stops in climbing, forming words and starts to black out rhythmically.

172
00:22:02,080 --> 00:22:05,080
So we force it to form a noting word.

173
00:22:05,080 --> 00:22:06,080
We don't force it.

174
00:22:06,080 --> 00:22:09,080
If the mind starts to black out, whatever that means,

175
00:22:09,080 --> 00:22:13,080
you would say knowing, knowing, or feeling, or feeling, or calm,

176
00:22:13,080 --> 00:22:20,080
calm, quiet, quiet.

177
00:22:20,080 --> 00:22:24,080
We should use a word based on what you're experiencing.

178
00:22:24,080 --> 00:22:26,080
The word is a reminder.

179
00:22:26,080 --> 00:22:28,080
It's that deep.

180
00:22:28,080 --> 00:22:30,080
It's cultivation of mindfulness.

181
00:22:30,080 --> 00:22:38,080
You're reminding yourself it's just this so that you don't react to it.

182
00:22:38,080 --> 00:22:41,080
When a kama is being committed, can there be situations

183
00:22:41,080 --> 00:22:44,080
when the role of free will becomes insignificant?

184
00:22:44,080 --> 00:22:47,080
For example, a person has a view that killing animals is OK

185
00:22:47,080 --> 00:22:51,080
and if a mosquito bites him causing anger to arise.

186
00:22:51,080 --> 00:22:54,080
But he's still able to refrain from killing him,

187
00:22:54,080 --> 00:22:57,080
even that no other factors intervene.

188
00:22:57,080 --> 00:23:00,080
And that's very...

189
00:23:00,080 --> 00:23:06,080
And I don't think you're making things over the complicated.

190
00:23:06,080 --> 00:23:15,080
If the mosquito bites causes anger to arise,

191
00:23:15,080 --> 00:23:20,080
when it's failed being mindful of the feeling.

192
00:23:20,080 --> 00:23:24,080
If the feeling comes and knows it as a feeling,

193
00:23:24,080 --> 00:23:26,080
there will be no anger.

194
00:23:26,080 --> 00:23:29,080
If anger arises, it's already too late.

195
00:23:29,080 --> 00:23:32,080
Depending on one state of mind, that anger

196
00:23:32,080 --> 00:23:40,080
might result in killing me the animal.

197
00:23:40,080 --> 00:23:43,080
But there's required more than anger.

198
00:23:43,080 --> 00:23:46,080
There's required wrong view, I think.

199
00:23:46,080 --> 00:23:47,080
I should think.

200
00:23:47,080 --> 00:23:50,080
Because it's sort of on a wooden...

201
00:23:50,080 --> 00:23:55,080
I couldn't give rise to the thoughts of that killing

202
00:23:55,080 --> 00:24:01,080
is the right answer.

203
00:24:01,080 --> 00:24:03,080
Here you go, it's about what I have to intervene.

204
00:24:03,080 --> 00:24:10,080
Without wholesome, wholesome mind state one...

205
00:24:10,080 --> 00:24:11,080
It's not...

206
00:24:11,080 --> 00:24:13,080
It didn't intervene in time for the anger,

207
00:24:13,080 --> 00:24:15,080
but the anger is only one mind state.

208
00:24:15,080 --> 00:24:18,080
After the anger, there has to be the thoughts.

209
00:24:18,080 --> 00:24:22,080
I should kill this, the thought, kill.

210
00:24:22,080 --> 00:24:25,080
And that thought has to have...

211
00:24:25,080 --> 00:24:28,080
Once that thought arises, there has to be a response

212
00:24:28,080 --> 00:24:31,080
to an anger and a delusion response.

213
00:24:31,080 --> 00:24:34,080
That thought.

214
00:24:34,080 --> 00:24:36,080
So without...

215
00:24:36,080 --> 00:24:41,080
It's about whether he or he ought to intervene at that point.

216
00:24:41,080 --> 00:24:44,080
They've already missed the chance for the...

217
00:24:44,080 --> 00:24:47,080
to stop the anger from arising.

218
00:24:47,080 --> 00:24:49,080
But anger isn't enough.

219
00:24:49,080 --> 00:24:55,080
It requires the thought to kill

220
00:24:55,080 --> 00:24:58,080
and the reaction to that.

221
00:24:58,080 --> 00:25:27,080
All right, I'm going to give it a rest there for tonight.

222
00:25:27,080 --> 00:25:29,080
I'm going to do my meditation.

223
00:25:29,080 --> 00:25:31,080
I skip my evening meditation.

224
00:25:31,080 --> 00:25:35,080
I'm going to do it now.

225
00:25:35,080 --> 00:25:37,080
One more question.

226
00:25:37,080 --> 00:25:40,080
Remember hearing that you said we practiced at the baton,

227
00:25:40,080 --> 00:25:43,080
but I also heard you say that we practiced on upon the sati.

228
00:25:43,080 --> 00:25:51,080
What do you mean is it a combination?

229
00:25:51,080 --> 00:25:57,080
On upon the sati, sati means mindfulness, sati is sati batana.

230
00:25:57,080 --> 00:25:59,080
On upon the means the breath.

231
00:25:59,080 --> 00:26:03,080
So if you're watching the breath that's on upon the sati.

232
00:26:03,080 --> 00:26:07,080
But sati, if you're using the...

233
00:26:07,080 --> 00:26:11,080
If you're using the elements, then it's sati batana.

234
00:26:11,080 --> 00:26:13,080
You're focusing on the elements.

235
00:26:13,080 --> 00:26:15,080
So when you watch the stomach,

236
00:26:15,080 --> 00:26:16,080
and you say to yourself,

237
00:26:16,080 --> 00:26:18,080
rising, falling, that's both watching the breath

238
00:26:18,080 --> 00:26:21,080
and watching the element.

239
00:26:21,080 --> 00:26:26,080
So it's both on upon the sati and sati batana.

240
00:26:26,080 --> 00:26:29,080
In the future, do you plan on going back to Thailand?

241
00:26:29,080 --> 00:26:31,080
Oh, I don't think too much about the future.

242
00:26:31,080 --> 00:26:34,080
In the future, I plan on moving to Stony,

243
00:26:34,080 --> 00:26:36,080
to Hamilton.

244
00:26:36,080 --> 00:26:42,080
A couple, about ten minutes down the road.

245
00:26:42,080 --> 00:26:46,080
Is it fair to say that an Arahan has the perfect free will

246
00:26:46,080 --> 00:26:52,080
since they are not swayed by the defined ones?

247
00:26:52,080 --> 00:26:54,080
I don't know.

248
00:26:54,080 --> 00:27:05,080
I don't like using terms like freewell or determinism.

249
00:27:05,080 --> 00:27:08,080
I wouldn't say such a thing.

250
00:27:08,080 --> 00:27:12,080
Or it's an Arahan has perfect freedom.

251
00:27:12,080 --> 00:27:15,080
Besides the fact they still have to live.

252
00:27:15,080 --> 00:27:19,080
They still have to eat, still have to suffer physically.

253
00:27:19,080 --> 00:27:24,080
Besides that, they're pretty much free.

254
00:27:32,080 --> 00:27:34,080
It's exercising another presumed health habit.

255
00:27:34,080 --> 00:27:38,080
It's just another attachment.

256
00:27:38,080 --> 00:27:43,080
Well, there's some extent you could argue that it's functional

257
00:27:43,080 --> 00:27:50,080
but for the most part, exercise is because we eat too much.

258
00:27:50,080 --> 00:27:52,080
We didn't eat too much.

259
00:27:52,080 --> 00:27:56,080
We wouldn't need to exercise nearly as much as most people do.

260
00:27:56,080 --> 00:27:59,080
Place sports and that kind of thing.

261
00:27:59,080 --> 00:28:01,080
It's all because of too much food.

262
00:28:01,080 --> 00:28:06,080
If you ate just enough to survive, you wouldn't need to do much exercise.

263
00:28:06,080 --> 00:28:09,080
Keep yourself healthy.

264
00:28:09,080 --> 00:28:16,080
That being said, for some exercise is useful.

265
00:28:16,080 --> 00:28:21,080
Some amount of healthiness is reasonable.

266
00:28:21,080 --> 00:28:25,080
It's a good practice.

267
00:28:25,080 --> 00:28:30,080
Often we become very much obsessed with health.

268
00:28:30,080 --> 00:28:32,080
That's a problem.

269
00:28:32,080 --> 00:28:37,080
You try to be too healthy, too obsessed with always being healthy.

270
00:28:37,080 --> 00:28:41,080
Then you won't be able to deal with sickness.

271
00:28:41,080 --> 00:28:45,080
You'll be very much averse and concerned and afraid of sickness.

272
00:28:45,080 --> 00:28:49,080
You shouldn't be afraid of getting sick.

273
00:28:49,080 --> 00:28:56,080
You should be ready and able to deal with sickness when it comes.

274
00:28:56,080 --> 00:29:02,080
I'm so obsessing about health is somewhat counterproductive.

275
00:29:02,080 --> 00:29:07,080
Okay, I'm really going this time. Good night everyone.

276
00:29:07,080 --> 00:29:32,080
Good night.

