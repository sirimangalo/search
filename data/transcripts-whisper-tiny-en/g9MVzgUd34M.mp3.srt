1
00:00:00,000 --> 00:00:10,000
Good evening, everyone. Welcome to our evening dumber.

2
00:00:10,000 --> 00:00:25,000
Tonight we're looking at right-lively hood. Path factor number five.

3
00:00:25,000 --> 00:00:30,000
I'm just completes the morality of the ethics section.

4
00:00:30,000 --> 00:00:39,000
So we have right speech, right action, and now the right livelihood.

5
00:00:39,000 --> 00:00:47,000
And if you look at right livelihood, it's interesting that it basically means right speech and right action.

6
00:00:47,000 --> 00:00:53,000
If you look at the definition, which is sometimes misleading.

7
00:00:53,000 --> 00:01:04,000
And if you look at it, in this case, I think it is because if you look at it and you think, why didn't the Buddha have to add right livelihood?

8
00:01:04,000 --> 00:01:14,000
When right livelihood, really the definition is refraining from livelihood that involves wrong action and wrong speech.

9
00:01:14,000 --> 00:01:28,000
So all the kinds of wrong action, or at least the lying, all kinds of wrong action, or lying wrong speech.

10
00:01:28,000 --> 00:01:35,000
If you do that to make a living, that's wrong livelihood. But then you think, well,

11
00:01:35,000 --> 00:01:44,000
if you're keeping right speech and right livelihood, why do we need to mention a right speech and right action?

12
00:01:44,000 --> 00:01:51,000
Why do you need to mention right livelihood?

13
00:01:51,000 --> 00:02:01,000
But I think it is quite important. I think this belies the fact that livelihood is an important part of who we are.

14
00:02:01,000 --> 00:02:10,000
And it's very essence, livelihood means staying alive. What do you do to stay alive?

15
00:02:10,000 --> 00:02:15,000
And it was a big part of the Buddha's conception of the monastic life.

16
00:02:15,000 --> 00:02:19,000
And I don't think this would have been something even unfamiliar to him.

17
00:02:19,000 --> 00:02:28,000
It was something he had to deal with for six years. Once he left the palace, it was a question, how do you stay alive?

18
00:02:28,000 --> 00:02:35,000
Because suddenly he didn't have a livelihood.

19
00:02:35,000 --> 00:02:41,000
And so a big part of spiritual practice at the time was to go without food,

20
00:02:41,000 --> 00:02:47,000
or to go with a bare minimum of food. Under the realization that livelihood is a big deal.

21
00:02:47,000 --> 00:02:57,000
If you want to stay alive, you want food. You've got to do quite a bit of work to get it, even just food.

22
00:02:57,000 --> 00:03:03,000
And then there's clothing, shelter, medicine.

23
00:03:03,000 --> 00:03:09,000
So from the very beginning, this was something that would have weighed on the Bodhisattva's mind.

24
00:03:09,000 --> 00:03:17,000
And of course, was something the Buddha had to consider when organizing his community.

25
00:03:17,000 --> 00:03:27,000
Or even just living himself.

26
00:03:27,000 --> 00:03:34,000
So it actually weighs in quite significantly how we live our lives.

27
00:03:34,000 --> 00:03:39,000
Beyond simply wrong action and wrong speech.

28
00:03:39,000 --> 00:03:46,000
I mean, on the very outset, there's the question of what is right livelihood.

29
00:03:46,000 --> 00:03:50,000
If you don't define it the way the Buddha did, there's a question about livelihood.

30
00:03:50,000 --> 00:03:59,000
And people feel righteous or even guilty about their livelihood, potentially for the wrong reasons.

31
00:03:59,000 --> 00:04:07,000
Maybe a person has a dull job that is meaningless, pointless.

32
00:04:07,000 --> 00:04:13,000
And they think they don't label it this way, but they think it's wrong livelihood.

33
00:04:13,000 --> 00:04:19,000
I think it's wrong because when you should enjoy your job, right, you should do something you love.

34
00:04:19,000 --> 00:04:28,000
And other people do something they love and they think that's right livelihood because it makes them happy.

35
00:04:28,000 --> 00:04:36,000
And so even just defining livelihood, right livelihood is having nothing to do with whether you like it or dislike it.

36
00:04:36,000 --> 00:04:44,000
But having more to do with is it wholesome or unwholesome? Is it involved with wholesomeness or unwholesomeness?

37
00:04:44,000 --> 00:04:51,000
Is it a significant step? And it clarifies some of the guilt we might feel at being insignificant.

38
00:04:51,000 --> 00:05:01,000
You know, I'm just a menial labor or maybe I work in McDonald's or something.

39
00:05:01,000 --> 00:05:08,000
Maybe I have a job that is very insignificant or maybe someone is famous, you know.

40
00:05:08,000 --> 00:05:18,000
People think the greatest livelihood wouldn't, you know, to be a famous doctor or scientist or someone who changes the world.

41
00:05:18,000 --> 00:05:21,000
That would be the best sort of livelihood.

42
00:05:21,000 --> 00:05:29,000
For a monk, it would be to be the Dalai Lama. That would be the, you know, you sow such a public figure

43
00:05:29,000 --> 00:05:36,000
who ostensibly does such great good in the world.

44
00:05:36,000 --> 00:05:41,000
But that's not the measure of right livelihood. None of that is really.

45
00:05:41,000 --> 00:05:48,000
A monk living off in the forest who nobody knows can have perfect livelihood.

46
00:05:48,000 --> 00:05:51,000
They can also have terrible livelihood.

47
00:05:51,000 --> 00:05:58,000
They can be killing wild animals for food, which would be very bad.

48
00:05:58,000 --> 00:06:11,000
But so basically, again, this part of this path that orient us, the part of this factor that orient us, is around the right action and right speech.

49
00:06:11,000 --> 00:06:14,000
So your livelihood is okay.

50
00:06:14,000 --> 00:06:21,000
It's within the boundaries of what will lead you to the noble path.

51
00:06:21,000 --> 00:06:28,000
Again, talking about the preliminary paths, the cultivation.

52
00:06:28,000 --> 00:06:33,000
If you keep basically the five precepts, if you don't kill,

53
00:06:33,000 --> 00:06:44,000
and if your livelihood is not to kill, obviously, then steal or steal or cheat or lie.

54
00:06:44,000 --> 00:06:49,000
That's a good basis for right livelihood.

55
00:06:49,000 --> 00:07:00,000
Of course, it's not yet right livelihood, and then any livelihood involves torturing others, manipulating others.

56
00:07:00,000 --> 00:07:07,000
Even any livelihood that involves greed or ambition from a meditative point of view is wrong livelihood.

57
00:07:07,000 --> 00:07:17,000
Even at a meditation center, if you start requesting special food or, you know, if you start,

58
00:07:17,000 --> 00:07:22,000
if you go out as a meditator, if you go out to the store and buy food,

59
00:07:22,000 --> 00:07:26,000
if you worry about food, is it delicious? Does it taste good? That kind of thing.

60
00:07:26,000 --> 00:07:31,000
This you could call wrong livelihood or in line with wrong livelihood.

61
00:07:31,000 --> 00:07:37,000
Of course, on a much lower level, but what it means is we have this necessity to go and eat,

62
00:07:37,000 --> 00:07:39,000
but we're making it complicated.

63
00:07:39,000 --> 00:07:41,000
It's becoming a hindrance.

64
00:07:41,000 --> 00:07:46,000
Your need to stay alive is getting in the way.

65
00:07:46,000 --> 00:07:51,000
So the question for the Buddha is always, how do you keep this from getting in the way?

66
00:07:51,000 --> 00:07:56,000
The need to wear clothing really gets in the way because, of course, you have to go out,

67
00:07:56,000 --> 00:08:02,000
and first you have to make all the money, and then you have to go and buy the clothes, choose the clothes,

68
00:08:02,000 --> 00:08:09,000
or maybe you've got the very least you have to make clothes, which is a lot of work itself.

69
00:08:09,000 --> 00:08:12,000
And then even worse, you start to get attached to the clothes.

70
00:08:12,000 --> 00:08:16,000
You wear beautiful clothes, and you become, you get this image.

71
00:08:16,000 --> 00:08:23,000
This is the kind of clothes that I wear, and this makes me look like this sort of person.

72
00:08:23,000 --> 00:08:28,000
Easy to get complicated.

73
00:08:28,000 --> 00:08:37,000
What you have to do to stay alive or to get the things that you want.

74
00:08:37,000 --> 00:08:42,000
So the greatest bright livelihood, of course, is the monastic life from a Buddhist point of view.

75
00:08:42,000 --> 00:08:44,000
This is why the Buddha created it.

76
00:08:44,000 --> 00:08:50,000
The envisioned monastic life is the best way to stay alive.

77
00:08:50,000 --> 00:08:58,000
Without engaging in all the complicated livelihood-related issues.

78
00:08:58,000 --> 00:09:03,000
So he said, for food, you take whatever people give you.

79
00:09:03,000 --> 00:09:11,000
Alms, I'm just a spiritual person. It's not easy to find support, to find a community,

80
00:09:11,000 --> 00:09:14,000
and people who are willing to keep you alive.

81
00:09:14,000 --> 00:09:23,000
For robes, for clothing, you just take rags, scraps of cloths that are discarded, and put them together.

82
00:09:23,000 --> 00:09:32,000
The kind of equivalent nowadays would be at the least for a lay person to go to the second hand clothing store.

83
00:09:32,000 --> 00:09:35,000
For a monk, it would be to go to the re-jects.

84
00:09:35,000 --> 00:10:04,000
The second hand clothing store, the stuff they throw away. I give away for free.

85
00:10:04,000 --> 00:10:29,000
Sorry, just reading something off to talk about that later.

86
00:10:29,000 --> 00:10:34,000
It's not a matter of having to become a monk to gain right livelihood,

87
00:10:34,000 --> 00:10:39,000
but it's important to understand this, to see this as a linear progression.

88
00:10:39,000 --> 00:10:45,000
The worst kind of livelihood is where you're doing the most awful things to stay alive.

89
00:10:45,000 --> 00:10:56,000
Or to make money, to live a lifestyle that is grotesque, and slowly slowly simplifying and simplifying our life.

90
00:10:56,000 --> 00:11:02,000
Again, thinking about wholesomeness, what keeps us and what allows us to continue our life,

91
00:11:02,000 --> 00:11:09,000
all the way to living in a meditation center or a monastery, where you're in your room and you say,

92
00:11:09,000 --> 00:11:13,000
hmm, well, if I don't go for food, I'm going to have to.

93
00:11:13,000 --> 00:11:16,000
I'm going to starve, and I'm not going to be able to meditate.

94
00:11:16,000 --> 00:11:21,000
And considering what is the best way to get that food the simplest way,

95
00:11:21,000 --> 00:11:26,000
you have to consider because if you don't eat the right food or enough food,

96
00:11:26,000 --> 00:11:28,000
it'll be hard to meditate as well.

97
00:11:28,000 --> 00:11:31,000
So it doesn't mean that you don't ask for food.

98
00:11:31,000 --> 00:11:34,000
If there's something missing in our diet here, you have to let us know.

99
00:11:34,000 --> 00:11:41,000
But if you need special food and you're allergic to this type of food, don't just eat it and suffer.

100
00:11:41,000 --> 00:11:43,000
But again, these are all issues.

101
00:11:43,000 --> 00:11:48,000
It points out that livelihood is for all of us an important part of our path.

102
00:11:48,000 --> 00:11:52,000
So it's not just about right action and right speech.

103
00:11:52,000 --> 00:11:57,000
It's about understanding our livelihood and making it, staying alive,

104
00:11:57,000 --> 00:12:03,000
and not allowing the necessities of life to get in our way,

105
00:12:03,000 --> 00:12:11,000
and making them the least complicated and least of a hindrance as possible.

106
00:12:11,000 --> 00:12:15,000
I think that's it.

107
00:12:15,000 --> 00:12:21,000
That's all I have to say about that.

108
00:12:21,000 --> 00:12:24,000
You all can go. You don't have to sit here for the rest of the questions.

109
00:12:24,000 --> 00:12:30,000
They're, in fact, I'd probably encourage you not to because they're often not about meditation,

110
00:12:30,000 --> 00:12:36,000
and they're mostly things that you've heard before.

111
00:12:36,000 --> 00:12:40,000
Alexander, you're here. How many days?

112
00:12:40,000 --> 00:12:46,000
Two or three weeks. Three weeks. And you've remind me, have you read my booklet?

113
00:12:46,000 --> 00:12:49,000
We haven't done the online course, have we?

114
00:12:49,000 --> 00:12:54,000
Okay. So you've been doing meditation according to the booklet.

115
00:12:54,000 --> 00:12:59,000
Okay, so maybe we'll have a refresher tomorrow morning.

116
00:12:59,000 --> 00:13:02,000
So you call him after you finish.

117
00:13:02,000 --> 00:13:08,000
She will call you after she finishes and you'll meet me here tomorrow around 8-15.

118
00:13:08,000 --> 00:13:10,000
Okay, everything okay?

119
00:13:10,000 --> 00:13:11,000
Yeah.

120
00:13:11,000 --> 00:13:12,000
Room is okay.

121
00:13:12,000 --> 00:13:17,000
I think we'd rather you go in those rooms over there because that room doesn't have a fire exit.

122
00:13:17,000 --> 00:13:19,000
The window is blocked.

123
00:13:19,000 --> 00:13:21,000
Yeah, by the porch.

124
00:13:21,000 --> 00:13:24,000
Yeah. So Robin was having some concerns about that.

125
00:13:24,000 --> 00:13:28,000
So we'll leave it open to last and the last person who comes has to stay in it.

126
00:13:28,000 --> 00:13:31,000
I guess I should move to one of the backwards.

127
00:13:31,000 --> 00:13:34,000
Probably better.

128
00:13:34,000 --> 00:13:43,000
It's not like it's dangerous. In fact, the owner told me that he pointed out specifically that the window across the hall is the fire exit.

129
00:13:45,000 --> 00:13:48,000
Just don't light fires.

130
00:13:48,000 --> 00:13:50,000
Well, there's many ways fires can start, right?

131
00:13:50,000 --> 00:13:55,000
Find out where the fire extinguishers are. Everyone should know where the fire extinguisher downstairs is.

132
00:13:55,000 --> 00:13:58,000
Do you know where the fire extinguisher is downstairs?

133
00:13:58,000 --> 00:14:00,000
Yeah, so everyone should know where it is.

134
00:14:00,000 --> 00:14:05,000
I should tell people when they come in just in case.

135
00:14:05,000 --> 00:14:07,000
It happens. It happens.

136
00:14:07,000 --> 00:14:09,000
We have karma.

137
00:14:09,000 --> 00:14:11,000
Sometimes karma catches up with you.

138
00:14:11,000 --> 00:14:13,000
Very strange and meditation centers.

139
00:14:13,000 --> 00:14:16,000
Strange things happen because we're working out our karma.

140
00:14:16,000 --> 00:14:20,000
So sometimes it can come at you quite quickly.

141
00:14:20,000 --> 00:14:23,000
Not to scare you or anything.

142
00:14:23,000 --> 00:14:30,000
Okay, have a good night.

143
00:14:30,000 --> 00:14:39,000
The first number of truths still true with regards to some kara, even for someone who has eliminated craving.

144
00:14:39,000 --> 00:14:46,000
Well, I'm not going to go into stacking exchange, not right now, but.

145
00:14:46,000 --> 00:14:51,000
So again, the first number of truths doesn't mean they are suffering.

146
00:14:51,000 --> 00:14:53,000
It means they are dukka.

147
00:14:53,000 --> 00:14:59,000
And dukka means that which is not able to cause happiness.

148
00:14:59,000 --> 00:15:01,000
That which is bad.

149
00:15:01,000 --> 00:15:03,000
They are useless, meaningless.

150
00:15:03,000 --> 00:15:05,000
Just no good.

151
00:15:05,000 --> 00:15:08,000
And for enlightened being that's true as well.

152
00:15:08,000 --> 00:15:09,000
They're still no good.

153
00:15:09,000 --> 00:15:13,000
They're still not suka.

154
00:15:13,000 --> 00:15:16,000
They're not something that causes happiness.

155
00:15:16,000 --> 00:15:20,000
It's not that they are suffering in that sense.

156
00:15:20,000 --> 00:15:23,000
There's important to understand the first number of truths.

157
00:15:23,000 --> 00:15:27,000
There's important to understand suffering, the concept of suffering or dukka.

158
00:15:27,000 --> 00:15:35,000
And it doesn't in every way mean painful, stressful.

159
00:15:35,000 --> 00:15:43,000
But in a sense, they are inferior.

160
00:15:43,000 --> 00:15:50,000
In a sense, they are stressful, even though they don't cause stress because they are incessant.

161
00:15:50,000 --> 00:15:52,000
They are rising and ceasing.

162
00:15:52,000 --> 00:15:53,000
They are relentless.

163
00:15:53,000 --> 00:15:56,000
So they are they are no good.

164
00:15:56,000 --> 00:15:59,000
We think of a rising and ceasing.

165
00:15:59,000 --> 00:16:01,000
It's good, you know.

166
00:16:01,000 --> 00:16:04,000
This is a wonderful world to live in, but it's not.

167
00:16:04,000 --> 00:16:10,000
And enlightened being realized is that, but the nature of them is this incessant arising.

168
00:16:10,000 --> 00:16:17,000
And that's why they're called dukka.

169
00:16:17,000 --> 00:16:23,000
Maybe later if you give me that link later, I can go to Stack Exchange and look at it.

170
00:16:23,000 --> 00:16:29,000
Is it important to be mindful of involuntary actions such as coughing or twitching?

171
00:16:29,000 --> 00:16:32,000
Well, that's no, that's not action.

172
00:16:32,000 --> 00:16:36,000
Right action has all to do with your intention.

173
00:16:36,000 --> 00:16:43,000
I mean, that's what I said yesterday, so in reference to yesterday's talk, no, that is nothing to do with it.

174
00:16:43,000 --> 00:16:48,000
Like suppose you're sitting with a knife in your hand and you twitch and you kill someone.

175
00:16:48,000 --> 00:16:54,000
Well, you shouldn't have had the knife in your hand probably, but you're not guilty of murder.

176
00:16:54,000 --> 00:16:57,000
So right action is nothing to do with the action.

177
00:16:57,000 --> 00:17:04,000
I mean, I thought I made that fairly clear last night, but maybe not.

178
00:17:04,000 --> 00:17:10,000
Is there a difference between being wise and having wisdom?

179
00:17:10,000 --> 00:17:12,000
Well, there's no such thing as a being.

180
00:17:12,000 --> 00:17:19,000
The being, anything is not real, but there's no such thing as possessing either.

181
00:17:19,000 --> 00:17:23,000
So it's not being or having it, it's about wisdom arising.

182
00:17:23,000 --> 00:17:25,000
But conceptually, we say both.

183
00:17:25,000 --> 00:17:35,000
We say one has wisdom, bhanyoah, or one is wise.

184
00:17:35,000 --> 00:17:36,000
I don't know how you can say that.

185
00:17:36,000 --> 00:17:42,000
Nipako is a way of saying it.

186
00:17:42,000 --> 00:17:46,000
How can I see this text and suit of the poly that you are using during broadcast?

187
00:17:46,000 --> 00:17:53,000
I wouldn't look to look this up for the study, but I can't always make up the exact words you're using.

188
00:17:53,000 --> 00:17:55,000
Well, I don't post them.

189
00:17:55,000 --> 00:17:59,000
I mean, I'm taking a lot of this from memory, and I'm reading from various sources.

190
00:17:59,000 --> 00:18:07,000
Now, there's not one source that I read from.

191
00:18:07,000 --> 00:18:12,000
I mean, maybe eventually once we get back to a good platform, I can have screencasting.

192
00:18:12,000 --> 00:18:17,000
There's this app studio thing that allows you to do that to show your screen.

193
00:18:17,000 --> 00:18:25,000
So I may be able to do that, but it probably wouldn't be great anyway, because YouTube quality is not great.

194
00:18:25,000 --> 00:18:30,000
You can, you know, if you want to email me with some,

195
00:18:30,000 --> 00:18:33,000
hey, you said this, where can I find that?

196
00:18:33,000 --> 00:18:37,000
Those kind of emails I'm happy to answer, although I'm very bad at answering emails.

197
00:18:37,000 --> 00:18:43,000
Hopefully, hopefully I'll get back to you on it.

198
00:18:43,000 --> 00:18:45,000
Too many emails.

199
00:18:45,000 --> 00:18:50,000
If in my room as a spider insect, I should try to get them through the window to freedom,

200
00:18:50,000 --> 00:18:56,000
or just leave them alone and ignore them when a mosquito settles on my screen, for example.

201
00:18:56,000 --> 00:18:59,000
Hmm.

202
00:18:59,000 --> 00:19:03,000
Yeah, I mean, you're not responsible for the acts of others.

203
00:19:03,000 --> 00:19:10,000
This is all about your intentions, but I would say it's compassionate to think of their well-being,

204
00:19:10,000 --> 00:19:16,000
and so letting them out, but that's totally optional.

205
00:19:16,000 --> 00:19:22,000
I mean, I think if a bird is fluttering around in your house, you probably do the compassionate thing to let it out,

206
00:19:22,000 --> 00:19:25,000
because that's a significant thing you can do.

207
00:19:25,000 --> 00:19:32,000
But insects, you know, it's really, they're just going to come back in any way more than we're going to come in.

208
00:19:32,000 --> 00:19:35,000
So I generally let them out.

209
00:19:35,000 --> 00:19:39,000
I generally like if there's flies in here, I'll try and find a way to let it outside.

210
00:19:39,000 --> 00:19:40,000
It's a compassionate thing to do.

211
00:19:40,000 --> 00:19:42,000
It's a good thing to do.

212
00:19:42,000 --> 00:19:44,000
That's not something you should obsess over.

213
00:19:44,000 --> 00:19:49,000
And certainly something, not something you should worry about.

214
00:19:49,000 --> 00:19:56,000
It's sort of an optional, you know, do what you can, but getting a fly out of the house is not an easy task.

215
00:19:56,000 --> 00:20:03,000
I remember once when I was doing tree planting, this was after I had started meditating,

216
00:20:03,000 --> 00:20:05,000
and I went up north to do tree planting.

217
00:20:05,000 --> 00:20:10,000
And if you know anything about north, north, Canada, in the summer, which most people don't,

218
00:20:10,000 --> 00:20:15,000
but I'll tell you, the mosquitoes up there, they said, I don't know if it was a joke or not,

219
00:20:15,000 --> 00:20:22,000
but I believe it, that the mosquitoes were chasing away the bears.

220
00:20:22,000 --> 00:20:29,000
And like it really is clouds of mosquitoes when you get up in the north.

221
00:20:29,000 --> 00:20:32,000
And it's quite remarkable.

222
00:20:32,000 --> 00:20:35,000
So I had to, I was thinking, you know, how am I, I was reading about this,

223
00:20:35,000 --> 00:20:38,000
I was thinking, how am I going to do this without killing them, right?

224
00:20:38,000 --> 00:20:42,000
I mean, I wouldn't kill them, but how am I going to do this without getting eaten alive, I guess.

225
00:20:42,000 --> 00:20:50,000
And I read about the various, the only repellent that really works is deep,

226
00:20:50,000 --> 00:20:52,000
which is really bad for you.

227
00:20:52,000 --> 00:20:55,000
So I thought, well, I'm going to use deep every day and enough to keep these,

228
00:20:55,000 --> 00:20:59,000
apparently it leads to sleep loss and hallucinations and all sorts of bad things.

229
00:20:59,000 --> 00:21:02,000
So instead, I had this full body mosquito net.

230
00:21:02,000 --> 00:21:04,000
Anyway, that's not the point.

231
00:21:04,000 --> 00:21:10,000
One night, I got out of my tent to go to use the tree,

232
00:21:10,000 --> 00:21:14,000
and I'd forgot to close the door when I got back.

233
00:21:14,000 --> 00:21:20,000
The, and the tent was full of mosquitoes.

234
00:21:20,000 --> 00:21:24,000
There were like, at least 20 mosquitoes, probably more in my tent.

235
00:21:24,000 --> 00:21:28,000
And so I spent the next hour with this little cup.

236
00:21:28,000 --> 00:21:33,000
And getting the mosquitoes out one by one by one and using a flashlight to find them

237
00:21:33,000 --> 00:21:36,000
and getting them out without killing them.

238
00:21:36,000 --> 00:21:40,000
I mean, that was probably more for my sake than theirs.

239
00:21:40,000 --> 00:21:47,000
But, you know, I think at point it's optional.

240
00:21:47,000 --> 00:21:49,000
It's something that's good to do.

241
00:21:49,000 --> 00:21:59,000
But it's certainly not killing them if you don't do it.

242
00:21:59,000 --> 00:22:02,000
My email address is utadamoatgmail.com,

243
00:22:02,000 --> 00:22:04,000
but don't broadcast it on the internet,

244
00:22:04,000 --> 00:22:11,000
because lots of people are going to email me.

245
00:22:11,000 --> 00:22:14,000
Okay, that's all the questions tonight.

246
00:22:14,000 --> 00:22:18,000
Please don't email me with questions like tonight.

247
00:22:18,000 --> 00:22:23,000
And I tend to respond better to technical questions

248
00:22:23,000 --> 00:22:24,000
because they're easy to answer,

249
00:22:24,000 --> 00:22:27,000
but I'm using my phone a lot for email.

250
00:22:27,000 --> 00:22:30,000
So I'm not going to answer questions like tonight's questions.

251
00:22:30,000 --> 00:22:33,000
Many people email me with questions about their practice and so on,

252
00:22:33,000 --> 00:22:35,000
and I'm sorry, but I just don't answer them.

253
00:22:35,000 --> 00:22:38,000
It's too many, and I don't really, you know,

254
00:22:38,000 --> 00:22:44,000
I have to be on the computer, and it's just not the way I want to live my life.

255
00:22:44,000 --> 00:22:46,000
I'm sorry to say.

256
00:22:46,000 --> 00:22:49,000
So thank you all for coming tonight.

