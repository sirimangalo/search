1
00:00:00,000 --> 00:00:08,100
Good evening everyone, welcome to our live broadcast.

2
00:00:08,100 --> 00:00:13,800
Funny thing is when I say that there's not actually anyone watching, it does let you

3
00:00:13,800 --> 00:00:16,800
to know that this was a live broadcast.

4
00:00:16,800 --> 00:00:24,600
Or maybe, I don't know how it works, maybe people are watching as soon as they start broadcasting.

5
00:00:24,600 --> 00:00:33,800
I've never actually watched one of these live, obviously.

6
00:00:33,800 --> 00:00:48,600
So today we are on to chapter 8 and we are going to number 76.

7
00:00:48,600 --> 00:00:54,200
These are really interesting, I like these ones.

8
00:00:54,200 --> 00:01:01,200
Pamatika, Apamatika, these are insignificant.

9
00:01:01,200 --> 00:01:07,200
Apamatika is a big way perihani.

10
00:01:07,200 --> 00:01:15,200
I know this, this, perihani, you know this.

11
00:01:15,200 --> 00:01:27,400
Perihani means loss, Apamatika means a little, a little importance, a little measure, literally

12
00:01:27,400 --> 00:01:40,100
means a little measure of little significance, a small matter insignificant.

13
00:01:40,100 --> 00:01:54,100
This loss is insignificant, monks, and is to say the loss of relatives.

14
00:01:54,100 --> 00:02:17,100
Pamatika, Patikitang, Patikitang, Patikitang, Patikitang, Patikaka.

15
00:02:17,100 --> 00:02:25,100
I don't know this one.

16
00:02:25,100 --> 00:02:43,100
I don't know what this word means.

17
00:02:43,100 --> 00:02:58,500
Anyway, I'm assuming it means the opposite of Apamatika, he says the worst, Patikitang.

18
00:02:58,500 --> 00:03:11,300
The worst, that is the worst, the worst of losses is the loss of wisdom.

19
00:03:11,300 --> 00:03:21,700
Into the Buddha it is to just mess with our sense of priority, you know?

20
00:03:21,700 --> 00:03:30,180
To tell us, leave it to the Buddha to be the one to tell all those grieving widows and

21
00:03:30,180 --> 00:03:40,300
orphans, or the bereaved, to tell them their loss is insignificant.

22
00:03:40,300 --> 00:03:54,580
Buddhism works on really on a whole other level, apart from mundane concerns, it's the

23
00:03:54,580 --> 00:04:06,500
most hard core of religion, the shame that it's been watered down and a lot of people,

24
00:04:06,500 --> 00:04:18,500
a lot of Buddhists, focus on a sort of soft core version of Buddhism, bad, focused on

25
00:04:18,500 --> 00:04:31,300
worldly things and social welfare, therapy, stress relief, Buddhism deals on a whole other

26
00:04:31,300 --> 00:04:36,660
level beyond worldly concerns.

27
00:04:36,660 --> 00:04:42,140
When you think about the Buddha's story himself, what he went through, the losses that

28
00:04:42,140 --> 00:04:48,260
he went through, I mean that's really the point is this is so much bigger, so much more

29
00:04:48,260 --> 00:04:51,660
profound and just our ordinary concern.

30
00:04:51,660 --> 00:04:58,420
We worry about the most ridiculous of things, and many we recognize it's ridiculous.

31
00:04:58,420 --> 00:05:10,340
We worry about a pimple, we worry about our weight, we worry about what other people think

32
00:05:10,340 --> 00:05:17,660
of us, we worry about our grains, we worry about our employment, we worry about money,

33
00:05:17,660 --> 00:05:27,660
so we worry about things and we put an increasing level of value on these things, so

34
00:05:27,660 --> 00:05:33,180
pimple might be most ridiculous and they go people that's not really worth worrying about.

35
00:05:33,180 --> 00:05:36,980
What other people think about me, well that's sort of a bit more important, and then

36
00:05:36,980 --> 00:05:45,260
my study and then my work and money and these kind of, and way up here is our relatives,

37
00:05:45,260 --> 00:05:54,020
our own health, our own life, our life of other people, our relationships, things that

38
00:05:54,020 --> 00:06:01,100
we believe have some sort of meaning, importance, worth worrying about, and that the loss

39
00:06:01,100 --> 00:06:05,500
is somehow significant, it's really not.

40
00:06:05,500 --> 00:06:12,380
This is the Buddha's teaching here, these things are still a very insignificant, but completely

41
00:06:12,380 --> 00:06:23,900
insignificant, there's no meaning, there's no value to that loss, or if there is a

42
00:06:23,900 --> 00:06:29,100
value, it's far, it's not that there isn't a value, it's just compared to things that

43
00:06:29,100 --> 00:06:34,540
are actually important, it's insignificant.

44
00:06:34,540 --> 00:06:43,140
I mentioned this before, I think, but when I was taking Buddhism in university, one of

45
00:06:43,140 --> 00:06:47,900
my friends was a Catholic, came up to me after a class once and just said, you know,

46
00:06:47,900 --> 00:06:56,580
Buddhism is just, it's such a relief, the idea that you could keep coming back again and

47
00:06:56,580 --> 00:07:05,420
again, you know, you don't just have one chance, that's Buddhism, and it's true, it really

48
00:07:05,420 --> 00:07:12,380
is a mind opener, it's relief from all the worries and stresses, all the things we have,

49
00:07:12,380 --> 00:07:15,980
I mean imagine being Catholic and having to worry about going to hell for eternity, you

50
00:07:15,980 --> 00:07:27,900
know, the things we worry about, Buddhism really blows the roof off of all that.

51
00:07:27,900 --> 00:07:31,860
Because in the face of eternity, what does it all mean?

52
00:07:31,860 --> 00:07:38,300
Even hell isn't terribly significant, because it comes and it goes.

53
00:07:38,300 --> 00:07:48,580
You think of eternity when you think of time erasing all things, you know, it's not just

54
00:07:48,580 --> 00:07:54,700
that in a hundred years we're all going to be dead, that's for sure.

55
00:07:54,700 --> 00:07:59,700
But in the thousand years no one will remember us, in a hundred thousand years there'll

56
00:07:59,700 --> 00:08:10,700
be no trace of us, in a million years, there will be no one, the universe will be nothing

57
00:08:10,700 --> 00:08:17,540
like it is now, and all of that is coming, all of that is real, all of that is in the

58
00:08:17,540 --> 00:08:28,980
near future, a million years goes by in the flash, and then it comes again and again.

59
00:08:28,980 --> 00:08:40,060
And it's not just a long time, eternity is not just a long time, what it means is that

60
00:08:40,060 --> 00:08:50,980
there can be no meaning to these things, to some event, there's things that we cry over

61
00:08:50,980 --> 00:09:04,140
that we mourn, that we worry and fret about, considered to be important, there's only

62
00:09:04,140 --> 00:09:10,540
one category of things that can be important, so those are the things that we carry with

63
00:09:10,540 --> 00:09:24,500
some qualities, wisdom, it's the difference between worrying about the ocean and how you

64
00:09:24,500 --> 00:09:29,020
can across the ocean and worrying about the boat.

65
00:09:29,020 --> 00:09:35,980
Our lives, like an ocean, the world, the universe, our experience of some sars like an

66
00:09:35,980 --> 00:09:48,820
ocean, and we worry about the weather, we worry about the direction, but much more important

67
00:09:48,820 --> 00:09:57,420
is the boat, the ship, the vehicle, wisdom is the vehicle, we worry too much about the

68
00:09:57,420 --> 00:10:02,460
circumstances in our life, oh am I going to get this, am I going to get that, if you get

69
00:10:02,460 --> 00:10:10,380
the things you want and you're not ready to accept and to own them, you know, to deal

70
00:10:10,380 --> 00:10:16,580
with them, when people wish for promotion or the questions are you ready for a promotion,

71
00:10:16,580 --> 00:10:24,460
they wish for a relationship, are you able to, you know, how are you going to deal with

72
00:10:24,460 --> 00:10:31,460
the things that you get, how are you going to live with them, are you always getting

73
00:10:31,460 --> 00:10:37,700
what you want, is not the answer, if you're spoiled and the things that you get spoil

74
00:10:37,700 --> 00:10:46,140
you rotten, may end up being a curse, the only thing that's really important is how you

75
00:10:46,140 --> 00:10:53,900
respond, how you react, how you interact, wisdom is the most important, because it determines

76
00:10:53,900 --> 00:11:02,820
how you're going to interact with the things that you get, loss of relatives, meaning

77
00:11:02,820 --> 00:11:07,500
loss of wisdom, the person who has no wisdom, I'm not quite sure what is meant by loss

78
00:11:07,500 --> 00:11:13,740
of wisdom, the wisdom is not an easy thing to lose once you get it, it is possible,

79
00:11:13,740 --> 00:11:22,860
mundane wisdom, but still it's one of those things that's so valuable because you can't

80
00:11:22,860 --> 00:11:31,540
lose it, rather it's hard to lose anyway, it's not like concentration, if you fix and focus

81
00:11:31,540 --> 00:11:36,620
your mind so that you never get angry and never get greedy, you can do it, but it's not

82
00:11:36,620 --> 00:11:43,740
as powerful as wisdom because it comes and it goes, but wisdom is very deep, and super

83
00:11:43,740 --> 00:11:52,780
mundane wisdom, wisdom of a soda bonding, there's no going back from that.

84
00:11:52,780 --> 00:12:00,020
But I think the point is the difference between wisdom and relatives, don't worry so much

85
00:12:00,020 --> 00:12:06,620
about people, worry so much about even people, it's a shock because there are many more

86
00:12:06,620 --> 00:12:11,820
things, the other things the Buddha could have compared it to, they say even relatives,

87
00:12:11,820 --> 00:12:17,380
which is one thing that we mourn the most, just the one thing in this life that we mourn

88
00:12:17,380 --> 00:12:24,140
the most, if your father and your mother, your child, your sister and your brother, someone

89
00:12:24,140 --> 00:12:31,740
you love, one of your relatives dies, that's one of the worst, right?

90
00:12:31,740 --> 00:12:42,140
And Buddha says insignificant, and then he says the same about wealth and the same about

91
00:12:42,140 --> 00:12:49,140
fame, insignificant is the loss of wealth, insignificant is the loss of fame, the worst

92
00:12:49,140 --> 00:12:56,180
thing to lose is wisdom, there's a quote, here's a Buddha quote, and I want to look

93
00:12:56,180 --> 00:13:05,780
at this concept of losing wisdom, what exactly does he mean by, come let's look at

94
00:13:05,780 --> 00:13:31,980
that, I mean you can fall away from, it's the point that you fall away from wisdom or

95
00:13:31,980 --> 00:13:38,060
you become, you become lost, I bet the commentary does say, but I don't really have time

96
00:13:38,060 --> 00:13:54,500
to go through it, anyway, the absence of wisdom is worst, wisdom is most important is

97
00:13:54,500 --> 00:14:04,100
the point here, relatives wealth, even fame, and then he says, therefore you should train

98
00:14:04,100 --> 00:14:11,140
yourselves, thus we will increase in wisdom, it is in such a way that you should train

99
00:14:11,140 --> 00:14:18,620
yourself, you don't worry about your circumstances, even people, people come and go, there's

100
00:14:18,620 --> 00:14:23,420
no question that all the people we know are going to be separated from, we're born alone,

101
00:14:23,420 --> 00:14:33,900
we die alone, we're born alone, die alone again, and the people we're with, they come

102
00:14:33,900 --> 00:14:44,420
and they go, we're like, what is this, I think it's a poem or something, we're like trains

103
00:14:44,420 --> 00:14:53,260
passing in the night or something like that, the very brief time we have together, we

104
00:14:53,260 --> 00:14:58,860
make the mistake of getting, it sounds cruel I suppose, but we do make the mistake of getting

105
00:14:58,860 --> 00:15:05,660
attached and as a result we suffer, and that sounds like a terrible thing to say, but if

106
00:15:05,660 --> 00:15:09,660
you look at it from the point of view of a Buddha, who sees people doing this again and

107
00:15:09,660 --> 00:15:14,540
again and again and again, and again, and remembers doing this again and again, and comes

108
00:15:14,540 --> 00:15:21,980
to think, you know, what is the point, is this really, you know, it seems worth it, seems

109
00:15:21,980 --> 00:15:29,020
to us who can't remember anything, that this one life is somehow rich, valuable, but if

110
00:15:29,020 --> 00:15:38,380
you look at it, objectively, there's a lot of suffering in life, and then to do it again

111
00:15:38,380 --> 00:15:45,660
and again and again, you start to wonder what's the real, what's really the point, and

112
00:15:45,660 --> 00:15:51,340
with the precarious nature of human life and the potential to be reborn as an animal or

113
00:15:51,340 --> 00:15:58,140
in hell, it's actually quite scary, because we don't know what we're going to be in our next

114
00:15:58,140 --> 00:16:03,660
life, which direction we're going to go, again, because our vehicle is uncertain, for fixed

115
00:16:03,660 --> 00:16:17,020
and focused on our experiences, our minds can easily become corrupted, so we focus on wisdom,

116
00:16:17,020 --> 00:16:25,100
how should we increase in wisdom, the basic, basic fundamental wisdom that we're looking

117
00:16:25,100 --> 00:16:29,980
for is simply to see things as they are, this is important to understand, wisdom isn't

118
00:16:29,980 --> 00:16:36,980
theory, that's not real wisdom, we learned in the Visudimanga on Sunday that there's three

119
00:16:36,980 --> 00:16:44,620
types of wisdom, sutamaya panya from hearing, that's what you hear me say, or you hear

120
00:16:44,620 --> 00:16:52,500
the Buddha say, or you read, dintamaya panya means from thinking, when you think and you use

121
00:16:52,500 --> 00:17:00,860
logic and reason, but the third one, powanamaya panya comes just from seeing, from watching,

122
00:17:00,860 --> 00:17:06,660
what you learn about yourself and you meditate, oh boy, I've got anger and issues, oh boy,

123
00:17:06,660 --> 00:17:14,300
look at me, hurting myself, why am I doing that, seeing the nature of your mind, seeing

124
00:17:14,300 --> 00:17:26,140
now I'm hurting myself, no one else is hurting me, and the wisdom, just see that the things

125
00:17:26,140 --> 00:17:33,620
that we cling to are not worth clinging to, very simple wisdom, in fact just knowing

126
00:17:33,620 --> 00:17:42,860
this is rising, this is falling, that's wisdom, it's the difference between seeing things

127
00:17:42,860 --> 00:17:48,940
as they are and judging them, when you don't know this is rising, you know it is good

128
00:17:48,940 --> 00:17:53,900
and as bad as me as mine and all the bad stuff comes, all the problems come, attachment

129
00:17:53,900 --> 00:17:59,580
to version like hell, when you just know this is rising, this is falling, your mind is

130
00:17:59,580 --> 00:18:07,620
pure, there's wisdom there, you do it enough, it starts to sink in, oh this is just rising,

131
00:18:07,620 --> 00:18:15,620
oh I see, it's not really good or bad or me or mine, I don't have to control it, I don't

132
00:18:15,620 --> 00:18:26,220
have to force it, I don't have to be in charge, pain, thoughts, emotions, these things

133
00:18:26,220 --> 00:18:32,940
are me or mine, then I'm a good or bad, they start to see that, start to see that, oh this

134
00:18:32,940 --> 00:18:39,740
is just pain, this is just thoughts, this is just, this is, let me start to let it go,

135
00:18:39,740 --> 00:18:48,700
anyway, so that's a little bit of dumb for tonight, stop there and let's see if we have

136
00:18:48,700 --> 00:18:53,580
some questions for Robin, Robin you don't have to come here every night, I know, you

137
00:18:53,580 --> 00:18:58,620
know I don't have a day job, so this is fine for me to come every night, but please don't

138
00:18:58,620 --> 00:19:06,660
feel obligated to be here, if some days you aren't, that's fine, so I'll show up one again

139
00:19:06,660 --> 00:19:15,700
Monday, great, so someone sent me a question to ask you, and it is, I have to know in day

140
00:19:15,700 --> 00:19:22,660
to day to practice where we have just to conventions and can't go with Paramata, like

141
00:19:22,660 --> 00:19:26,780
while reading, studying and talking with someone, and I have to admit I don't know

142
00:19:26,780 --> 00:19:34,220
what Paramata is, Paramata, it's missing a T there, Aata with two T's, Aata means meaning

143
00:19:34,220 --> 00:19:43,420
or nature, meaning it really means, Paramata means ultimate or highest, so Paramata, Dama

144
00:19:43,420 --> 00:19:49,620
is a Dama in the highest sense or the highest meaning, I mean this ultimate reality, that's

145
00:19:49,620 --> 00:19:54,780
the translation of the word Paramata Dama, so the Abidama deals in Paramata Dama, real

146
00:19:54,780 --> 00:20:00,860
Dama, the suit does deal in concept, they deal with people, places, things, but the Abidama

147
00:20:00,860 --> 00:20:07,140
just deals with qualities, if you read the Abidama, it's quite breathtaking for a Buddhist,

148
00:20:07,140 --> 00:20:12,820
for anyone else, it just seems dense and unreadable and boring, but for a Buddhist, it's

149
00:20:12,820 --> 00:20:20,700
breathtaking, learning about these Damas and lists and lists of different aspects of reality.

150
00:20:20,700 --> 00:20:29,580
Right, so you don't have to stick to conventions, the reality is still there, when you're

151
00:20:29,580 --> 00:20:33,740
talking to someone, you're lips are moving, there's a feeling of that, there are thoughts

152
00:20:33,740 --> 00:20:38,300
going through your mind, there are emotions, you can be mindful of all of those, even

153
00:20:38,300 --> 00:20:43,740
while you're talking, in between, you know, the point is when you're reading, you're

154
00:20:43,740 --> 00:20:48,740
not trying to practice meditation, you're trying to imbibe some knowledge, so you can't

155
00:20:48,740 --> 00:20:56,420
meditate, at the moment when you're trying to understand something, but in between moments,

156
00:20:56,420 --> 00:21:01,140
you can be mindful, and especially in between sessions, so you do five minutes of studying,

157
00:21:01,140 --> 00:21:06,740
five minutes of meditation, half an hour of meditating, five minutes of, half an hour of studying,

158
00:21:06,740 --> 00:21:12,740
five minutes of meditating, that kind of thing, if you do breaks for meditation or even

159
00:21:12,740 --> 00:21:17,820
just every moment that you remember to be mindful, just be mindful, just be learned yesterday

160
00:21:17,820 --> 00:21:40,220
or the day before, a moment of goodness, makes your life not a waste, and we don't have

161
00:21:40,220 --> 00:21:48,100
any other questions, just one, and one more, and down the line, okay, if there are cumulative,

162
00:21:48,100 --> 00:21:51,020
effective wisdom of our multiple birds coming across the dhamma in this life, completely

163
00:21:51,020 --> 00:21:58,100
by chance, seriously by chance, it's probably not by chance, but it could be, things can

164
00:21:58,100 --> 00:22:04,340
just conspire to come together, but usually not, you know, now, and I think an orthodox Buddhist

165
00:22:04,340 --> 00:22:10,380
would scold me for even suggesting it, because there's so many things that have to come together,

166
00:22:10,380 --> 00:22:14,540
you have to be born a human, you have to be born a human in the time of the Buddha,

167
00:22:14,540 --> 00:22:19,020
you have to have all your good faculties, you have to be able to understand, and you

168
00:22:19,020 --> 00:22:28,460
have to not be sickly and weak, or worse, be corrupt of mind, and then you have to have

169
00:22:28,460 --> 00:22:37,220
the opportunity, you know, to just, it seems like just a chance, right?

170
00:22:37,220 --> 00:22:42,100
Like I have four brothers, I have three brothers, none of them are really interested in

171
00:22:42,100 --> 00:22:48,100
Buddhism or spirituality, even the way I am, and they're all seem like nice people, and

172
00:22:48,100 --> 00:22:53,420
I suppose I can understand the different ways in which we've connected and probably

173
00:22:53,420 --> 00:22:59,220
were born together for those reasons, but none of them have, sort of, the inclination

174
00:22:59,220 --> 00:23:05,460
to, or ever had the inclination towards the sort of things that I have had an inclination

175
00:23:05,460 --> 00:23:07,660
for.

176
00:23:07,660 --> 00:23:14,500
So I wouldn't say it's, I wouldn't say it's a chance, and I mean consider becoming

177
00:23:14,500 --> 00:23:18,060
a monk, if you look at all the people who try to become a monk, and this isn't a

178
00:23:18,060 --> 00:23:23,540
brag, because I'm, I'm nothing, you know, not some special person, but there's a group

179
00:23:23,540 --> 00:23:27,540
of us who really had an easy time becoming monks, and those, that's those of us who are

180
00:23:27,540 --> 00:23:32,500
still monks, and it seems like we must have done something different in the past lives,

181
00:23:32,500 --> 00:23:39,940
because I never wanted this role, but I've never had, and it's just been, it's just,

182
00:23:39,940 --> 00:23:44,700
I've never really been an issue in my mind, it's like, as soon as I learned about becoming

183
00:23:44,700 --> 00:23:50,060
a monk, it was like, that's it, you know, but other people who have even, even students

184
00:23:50,060 --> 00:23:53,380
of mine who have ordained, you can see there's something very different going on in their

185
00:23:53,380 --> 00:24:00,060
minds, they can't do it, they can't take it, they just roll very quickly, or more after

186
00:24:00,060 --> 00:24:02,860
some time, you know, they just can't take it.

187
00:24:02,860 --> 00:24:10,740
I's not to say them, you know, but I would say very little of what will become across

188
00:24:10,740 --> 00:24:16,140
in our lives as most likely coincidents, sure it's, you know, it's hard to predict in

189
00:24:16,140 --> 00:24:23,340
its complex, but the big themes of our lives are probably very much associated with past

190
00:24:23,340 --> 00:24:25,380
lives.

191
00:24:25,380 --> 00:24:30,420
So, but simply an answer question, wisdom of course has an effect over multiple births,

192
00:24:30,420 --> 00:24:37,460
and we wish them has an effect on the future, the future includes next lives, so everything

193
00:24:37,460 --> 00:24:43,780
we do, the facts are our future, in some way, are another big or small, and it's more

194
00:24:43,780 --> 00:24:50,060
complicated, you know, they are the effects affect each other, the effects affect each

195
00:24:50,060 --> 00:24:53,060
other.

196
00:24:53,060 --> 00:25:03,060
So, that's it, two questions must have answered them already.

197
00:25:03,060 --> 00:25:08,060
I think so, you have a lot of questions for the last few next up.

198
00:25:08,060 --> 00:25:12,420
I'm not really concerned about getting questions, it's nice when they come, but I was

199
00:25:12,420 --> 00:25:17,420
thinking, you know, this is really, really this is for my one meditator who's here and

200
00:25:17,420 --> 00:25:22,180
when meditators come, more meditators come, this is a chance for me to give them a little

201
00:25:22,180 --> 00:25:28,980
bit of dumb my everyday, so the fact that I'm broadcasting it is kind of, actually secondary,

202
00:25:28,980 --> 00:25:34,740
I think that's fair to say, even though you compare one person to hundreds and thousands

203
00:25:34,740 --> 00:25:41,540
of people who watch YouTube, my YouTube videos, maybe it's not fair to say then, but it's

204
00:25:41,540 --> 00:25:46,580
a whole different issue because as you know, doing the meditation courses on a whole

205
00:25:46,580 --> 00:25:51,700
level, right, this is much more the important work that we're doing.

206
00:25:51,700 --> 00:25:59,940
It's very nice to listen to your Dhamma talk slide, but it's nice to listen to them on

207
00:25:59,940 --> 00:26:00,940
YouTube as well.

208
00:26:00,940 --> 00:26:03,940
Here we have a new question.

209
00:26:03,940 --> 00:26:04,940
We do.

210
00:26:04,940 --> 00:26:11,460
Is the experience of attachment then suffering the loss required in order to rise above

211
00:26:11,460 --> 00:26:12,460
ignorance?

212
00:26:12,460 --> 00:26:13,460
No.

213
00:26:13,460 --> 00:26:18,060
The link that's required to rise above ignorance is seeing things as they are, seeing

214
00:26:18,060 --> 00:26:28,860
that the things that you, well, not exactly, but the point is you have to have ignorance

215
00:26:28,860 --> 00:26:39,100
in order to let go of ignorance and it's actually a good question, but maybe not for

216
00:26:39,100 --> 00:26:46,380
the reason that you think it's because you can have ignorance technically without craving,

217
00:26:46,380 --> 00:26:51,820
but I think that's just academic because ignorance leads to craving.

218
00:26:51,820 --> 00:26:57,540
We like in dislike things, we have partialities, because we don't understand that things

219
00:26:57,540 --> 00:27:03,340
are impermanent unsatisfying and controllable, so all you have to do is see that things

220
00:27:03,340 --> 00:27:08,860
are impermanent unsatisfying and uncontrollable and if you see that, that's considered

221
00:27:08,860 --> 00:27:15,220
the dispelling of ignorance and as a result of dispelling that ignorance about things

222
00:27:15,220 --> 00:27:22,460
about the experiences, you won't give rise to attachment, so it's not about suffering

223
00:27:22,460 --> 00:27:23,460
the loss.

224
00:27:23,460 --> 00:27:28,900
That's all sutta and dinted, that's all thinking, you think, yeah, it's not worth clinging

225
00:27:28,900 --> 00:27:33,820
to because when I clung to it before I suffered, that's not really what we're talking

226
00:27:33,820 --> 00:27:34,820
about here.

227
00:27:34,820 --> 00:27:36,580
You don't have to see it like that.

228
00:27:36,580 --> 00:27:41,140
You just have to see that the things that you cling to are not worth clinging to, you

229
00:27:41,140 --> 00:27:45,740
might mean, or sorry, you have to see that the objects of experience are impermanent because

230
00:27:45,740 --> 00:27:48,060
they're impermanent, they're not worth clinging to you.

231
00:27:48,060 --> 00:27:52,660
You don't have to cling to them to know that they're not worth clinging to.

232
00:27:52,660 --> 00:27:56,780
Once you see them clearly, you'd think it would be absurd to cling to such a thing.

233
00:27:56,780 --> 00:27:57,780
Why?

234
00:27:57,780 --> 00:28:01,860
Because it comes and it goes, it's gone in a moment, it's really not the sort of thing

235
00:28:01,860 --> 00:28:04,220
that you could possibly cling to.

236
00:28:04,220 --> 00:28:13,660
One day I think we missed one last night or maybe it came in after we sat off, but is

237
00:28:13,660 --> 00:28:17,540
it bad if you're a meditator and you don't get out that often and you don't socialize

238
00:28:17,540 --> 00:28:18,540
much?

239
00:28:18,540 --> 00:28:22,700
I've been told that it's unhealthy for me to do that.

240
00:28:22,700 --> 00:28:26,700
But if you don't, if you don't socialize, no, I think.

241
00:28:26,700 --> 00:28:50,620
When you're alone, you're like Brahma, you're like Brahma, you're like the angels are

242
00:28:50,620 --> 00:28:51,860
too.

243
00:28:51,860 --> 00:28:56,500
So when you're together with another person, that's like being an angel.

244
00:28:56,500 --> 00:29:09,780
Three people is like a village, four people are more, it's an uproar, like a commotion.

245
00:29:09,780 --> 00:29:11,180
Being alone is great.

246
00:29:11,180 --> 00:29:16,500
Being on socialize, don't worry about it.

247
00:29:16,500 --> 00:29:21,260
I don't think it's fair to say that you become a more...

248
00:29:21,260 --> 00:29:26,340
I mean, you have to find, I suppose that's not quite fair to say, because the question

249
00:29:26,340 --> 00:29:29,020
is if you're alone, what are you doing?

250
00:29:29,020 --> 00:29:34,380
So just being alone in and of itself is not good or it's not good, it may not be bad,

251
00:29:34,380 --> 00:29:37,020
but there's nothing good about it.

252
00:29:37,020 --> 00:29:39,180
It depends what you're doing alone.

253
00:29:39,180 --> 00:29:45,900
The point is that being alone allows you to do much more soul searching, so to speak,

254
00:29:45,900 --> 00:29:52,140
so learning about yourself, and that's what's important about being alone.

255
00:29:52,140 --> 00:29:58,060
You can't easily do that when you're with that, but being with good people on the other

256
00:29:58,060 --> 00:30:05,100
hand can encourage you to spend time alone when you have good friends, but not socializing.

257
00:30:05,100 --> 00:30:10,420
I think it still stands because if you're talking about getting out and socializing, it's

258
00:30:10,420 --> 00:30:14,980
a secret that most people don't know, it's not really worth it.

259
00:30:14,980 --> 00:30:18,900
This idea that's a most socializing is going to make you happy, it's really not.

260
00:30:18,900 --> 00:30:24,860
It's just going to make you all neurotic, worried about what other people think of you,

261
00:30:24,860 --> 00:30:31,580
and addicted to company and enjoyment and pleasure and even drugs and alcohol and all sorts

262
00:30:31,580 --> 00:30:33,580
of things.

263
00:30:33,580 --> 00:30:42,380
I think the person that asked that question is, is one of our younger meditators, and that's

264
00:30:42,380 --> 00:30:48,140
something that happens, your family's worried about you, that you're not going out and doing.

265
00:30:48,140 --> 00:30:52,140
Tell them, tell them it's okay, you're a Buddhist.

266
00:30:52,140 --> 00:31:00,980
Okay, why in that case, I don't know how well that would go over in most of my life.

267
00:31:00,980 --> 00:31:11,580
All right, we have more questions, you know where I'll be tomorrow.

268
00:31:11,580 --> 00:31:12,580
Have a good night, everyone.

269
00:31:12,580 --> 00:31:13,580
Thank you, God.

270
00:31:13,580 --> 00:31:14,580
Thank you, God.

271
00:31:14,580 --> 00:31:42,580
Thank you, God.

