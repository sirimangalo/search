1
00:00:00,000 --> 00:00:07,740
no welcome today we will be giving a tour of our monastery so here we are at the

2
00:00:07,740 --> 00:00:13,300
end things for the monastery if you want to just follow me I'll take you around and see

3
00:00:13,300 --> 00:00:28,500
them the little place

4
00:00:43,300 --> 00:00:56,620
This is the main hall, and it doesn't look like much now, but mostly because we're in

5
00:00:56,620 --> 00:01:01,900
the middle of construction, and we're just wide in this out, so, you know, one day

6
00:01:01,900 --> 00:01:16,620
it'll look nice, and now it's much more functional than it would be the same.

7
00:01:16,620 --> 00:01:20,460
So, let's take your attention to the tour of the hall.

8
00:01:20,460 --> 00:01:29,780
In here is the hall, this is a place where we do meditation together, and we have an office

9
00:01:29,780 --> 00:01:52,020
in there, with the computer, here is the kitchen, that's where we sit and eat, that's where

10
00:01:52,020 --> 00:01:57,300
we bring the food, where we have a fruit refrigerator, and water filter, and the sink

11
00:01:57,300 --> 00:02:25,900
that's where it comes from, okay, let's go back up, and we're going to go up to the top

12
00:02:25,900 --> 00:02:41,180
of the city, and then the new one, and the sink the top, and it's not fly away, it's

13
00:02:41,180 --> 00:03:05,820
a place where we have our token good image, but more importantly, it's a place where

14
00:03:05,820 --> 00:03:13,580
we do all sorts of official business of the sun we have, so when the monks have to meet

15
00:03:13,580 --> 00:03:23,580
together every two weeks, we do it in here, and so on, also got a good zoom on it, so you

16
00:03:23,580 --> 00:03:34,580
can see with that, you found zoom control, zoom controls are at your finger, move it back

17
00:03:34,580 --> 00:04:00,200
in front of you, the

18
00:04:00,200 --> 00:04:18,600
Okay, now we're heading into the meditation center, we can, we should probably first start

19
00:04:18,600 --> 00:04:27,800
with what we already have, put through this thing, over there, and we're still going to put

20
00:04:27,800 --> 00:04:33,320
the footy that I'm staying in right now, and we're building two rooms up above it, so you see

21
00:04:33,320 --> 00:04:38,600
they still have the roof already, pretty soon both sides of the roof, and then actually people can stay in there,

22
00:04:38,600 --> 00:04:56,600
and there's the well, if you didn't know where the well was, down there we're doing there,

23
00:04:56,600 --> 00:05:07,500
it's going to dry up, it looks like it's going to dry up, it's going to dry up, it's going to dry up.

24
00:05:07,500 --> 00:05:36,860
Okay, so here this building here

25
00:05:36,860 --> 00:05:42,500
here, you see these two rooms up on top here, actually we can go right up

26
00:05:42,500 --> 00:05:52,180
and see them, just be careful because they're still railing, and this building is

27
00:05:52,180 --> 00:05:58,100
this room is actually extra special because the door is like facing against the

28
00:05:58,100 --> 00:06:04,420
cave, so it probably can't end, you can view but you see the door is going to open

29
00:06:04,420 --> 00:06:12,420
onto the rock, and it's just how it was built, the rocks are too close, I didn't have enough room,

30
00:06:12,420 --> 00:06:20,220
and it can go, I don't have to go around, the rooms up here, and then downstairs there's

31
00:06:20,220 --> 00:06:27,700
two more rooms, I can see them on the way back, and now we're going up to where the caves

32
00:06:27,700 --> 00:06:54,020
are, and then I'm going to go to the bridge, and then I'm going to go to the bridge,

33
00:06:54,020 --> 00:07:07,500
so this is the first cave, this is the big one upstairs, and there's a resident here,

34
00:07:07,500 --> 00:07:34,520
but you can go on up and take a look in, this used to be the

35
00:07:34,520 --> 00:08:03,500
car, and it's going to be where they had to get there, the long stuff, and this is the

36
00:08:03,500 --> 00:08:11,380
boaty tree, and it doesn't look much now because all the leaves are falling, and it's

37
00:08:11,380 --> 00:08:16,960
a boaty tree from the boaty tree at Anoralepura, which in turn is a descendant from the

38
00:08:16,960 --> 00:08:24,840
boaty tree in India, and in fact, this is, we're going to try to extend this out, so there

39
00:08:24,840 --> 00:08:35,540
will actually be a lot of room to collect this meditation under the boaty tree, and that's

40
00:08:35,540 --> 00:08:42,140
on top of the other two rooms, so on top of them there will be a platform to do meditation

41
00:08:42,140 --> 00:09:05,380
so these old stairs were ruined by the boaty tree because it didn't have, it's on the

42
00:09:05,380 --> 00:09:13,200
rock basically, it didn't have enough room to grow, so it started growing down where the water

43
00:09:13,200 --> 00:09:17,200
was, and there's a boaty roof, and there's a roof that goes off to the set and it just broke

44
00:09:17,200 --> 00:09:43,400
up, so we've gotten this there, got this there, got this there, got this there, got this

45
00:09:43,400 --> 00:10:13,280
there, and this here is a new meditation hall over there, so it is that eventually

46
00:10:13,280 --> 00:10:20,280
we'll have a roof coming down, probably not so long, once they finished the kapistata,

47
00:10:20,280 --> 00:10:28,160
we have a little kapistata roof on here, and then we were already using it, but in rainy

48
00:10:28,160 --> 00:10:35,680
season this gets quite wet, so try to keep this area dry, and then we have the second

49
00:10:35,680 --> 00:10:46,400
here, so you want to open the door and take a look, it's too dark, too dark, it's a

50
00:10:46,400 --> 00:11:02,280
small cave anyway, this is not much of it, and that's here, the newer land that I was

51
00:11:02,280 --> 00:11:08,020
talking about is up here, so you can actually, we should be able to look down on this

52
00:11:08,020 --> 00:11:14,280
here, sort of, and to move it back, I don't know if we'll come so close to the edge, but

53
00:11:14,280 --> 00:11:28,280
you should sort of be able to see, and in fact I saw the boaty tree, pretty much there.

54
00:11:28,280 --> 00:11:43,480
There's a spot where you can build something against, you could probably build some kind

55
00:11:43,480 --> 00:11:48,800
of cookie there, against the rock, the problem with building against the rock is the rock

56
00:11:48,800 --> 00:12:06,800
and some metal, still going on here, still going on here, and then I have our two bathrooms,

57
00:12:06,800 --> 00:12:30,800
still going on here, still going on here, still going on here, still going on there.

58
00:12:30,800 --> 00:12:32,800
Can you help her?

59
00:12:32,800 --> 00:12:34,800
Can you help her?

60
00:12:34,800 --> 00:12:36,800
Can you help her?

61
00:13:04,800 --> 00:13:10,800
Can you help her?

62
00:13:34,800 --> 00:13:49,800
And the purposefully kind of dark, because they used to make them kind of cable, like we didn't build against the cable.

63
00:13:49,800 --> 00:14:02,800
But that is the word that we did in the field of security.

64
00:14:02,800 --> 00:14:05,800
It's some of the small rows.

65
00:14:05,800 --> 00:14:07,800
I think that's all for walking.

66
00:14:07,800 --> 00:14:12,800
It's a little bathroom.

67
00:14:12,800 --> 00:14:16,800
I think it's the biggest making fit here, because it's building on a range sort of.

68
00:14:16,800 --> 00:14:25,800
As it is, we had to really build up some foundation walls.

69
00:14:25,800 --> 00:14:33,800
But the thing about these two rooms that I'm using, I'm going to call this the coolest rooms in the modern state.

70
00:14:33,800 --> 00:14:43,800
And the design of the cable is in the middle.

71
00:14:43,800 --> 00:14:52,800
And there's the word for this one.

72
00:14:52,800 --> 00:15:15,800
And there's the stairs that we were just at.

73
00:15:15,800 --> 00:15:28,800
That's the bridge we were just on.

74
00:15:28,800 --> 00:15:53,800
That's it.

75
00:15:53,800 --> 00:16:06,300
This will be a old well in this and we can dig that out because there's always water in it.

76
00:16:06,300 --> 00:16:15,300
If we dug that down we can make another well. We'll have to see if we do run out of water.

77
00:17:06,300 --> 00:17:21,300
So here we come closer, we're now back at the kitchen.

78
00:17:21,300 --> 00:17:41,300
I'm going to wait to start it.

79
00:17:41,300 --> 00:18:04,300
You don't follow me, you don't go around here with this.

80
00:18:04,300 --> 00:18:33,300
I'm going to wait for it.

81
00:18:33,300 --> 00:18:48,300
If we run down that we want to go around.

82
00:18:48,300 --> 00:19:12,300
I'm not going to take it into the distance.

83
00:21:42,300 --> 00:21:56,300
So this is all not our land that we've been walking through and here's where it starts.

84
00:21:56,300 --> 00:22:00,300
And we don't have all the clips above.

85
00:22:00,300 --> 00:22:02,300
This is some of the clips above.

86
00:22:02,300 --> 00:22:08,300
But from here where I'm standing on belongs to the monastery, and it goes a little

87
00:22:08,300 --> 00:22:11,300
ways I've ever gone to the end up there.

88
00:22:11,300 --> 00:22:13,300
It's a half an acre.

89
00:22:13,300 --> 00:22:17,300
It's a big hot sauce.

90
00:22:17,300 --> 00:22:21,300
This is a piece this will stick.

91
00:22:21,300 --> 00:22:28,300
We still have the ancient peak.

92
00:22:28,300 --> 00:22:50,300
So if you clear out some of this, you can do put these on.

93
00:22:50,300 --> 00:22:53,300
You can find the room for cookies.

94
00:22:53,300 --> 00:23:00,300
It's not flat, but...

95
00:23:00,300 --> 00:23:22,300
And now I think we're about the upside.

96
00:23:22,300 --> 00:23:29,300
I think we're about the upstairs cave.

97
00:23:29,300 --> 00:23:36,300
So now we're right above the cave yet.

98
00:23:36,300 --> 00:23:40,300
And we'll actually go down a little spot above the cave where you can go to the tent

99
00:23:40,300 --> 00:23:41,300
before.

100
00:23:41,300 --> 00:23:47,300
You can build a small cookie and I don't know if you can build a little piece.

101
00:23:47,300 --> 00:23:52,300
So it gets a little steep here.

102
00:23:52,300 --> 00:23:57,300
I don't know how far our land goes, but in some of that we can build some more.

103
00:23:57,300 --> 00:24:12,300
It's difficult going to have a little bit into the edge.

104
00:24:12,300 --> 00:24:18,300
It's not much to see, but all it has to be cleared out.

105
00:24:18,300 --> 00:24:21,300
It's probably there's a way to go over...

106
00:24:21,300 --> 00:24:28,300
You can try going a little further, see if there's a way to get unboxed downstairs too.

107
00:24:28,300 --> 00:24:44,300
They have thorns backwards, so they don't just stick here, they hook you.

108
00:24:44,300 --> 00:25:06,300
We're going to plan.

109
00:25:06,300 --> 00:25:16,300
It's not much of a...

110
00:25:16,300 --> 00:25:19,300
Okay, probably this is it.

111
00:25:19,300 --> 00:25:23,300
It's not Sunday we'll go further and the downstairs over here.

112
00:25:23,300 --> 00:25:34,300
It's just never been only the monkeys go here.

113
00:25:34,300 --> 00:25:36,300
Well that's it really, there's not much else to see.

114
00:25:36,300 --> 00:26:01,300
We may as well start the video here.

