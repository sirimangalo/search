1
00:00:00,000 --> 00:00:09,000
The Buddhist continually improve their behavior through life, or there's one reach plateau of enlightenment,

2
00:00:09,000 --> 00:00:16,000
which you have to sustain as you live through life's difficulties.

3
00:00:16,000 --> 00:00:29,000
Well, it's improvement and change every day. Everything is impermanent, and it's changing hopefully for the better,

4
00:00:29,000 --> 00:00:32,000
and that's what we work for.

5
00:00:32,000 --> 00:00:43,000
And if one really gets enlightened becomes enlightened, I don't think even this is to be understood as a plateau

6
00:00:43,000 --> 00:00:47,000
from which it doesn't move anymore.

7
00:00:47,000 --> 00:01:00,000
Even in that stage, you have to maintain, you have to meditate, and you can't get better,

8
00:01:00,000 --> 00:01:07,000
you can't improve anymore, but you have the difficulties of life.

9
00:01:07,000 --> 00:01:16,000
You have to deal with sicknesses, with unpleasant things, with aging, and so on.

10
00:01:16,000 --> 00:01:20,000
So you can't really speak or from plateau.

11
00:01:20,000 --> 00:01:31,000
It's always in the progress of and always in the change.

12
00:01:31,000 --> 00:01:42,000
The plateau, I guess, is that you have no more greed, no more anger, no more delusion.

13
00:01:42,000 --> 00:01:54,000
There's a plateau there, but it's actually very, it's much more like a slide.

14
00:01:54,000 --> 00:02:03,000
Like you've climbed up to the top of the slide, and now you're sliding down the slide, because the person who is enlightened,

15
00:02:03,000 --> 00:02:07,000
they're heading towards total freedom.

16
00:02:07,000 --> 00:02:16,000
So the longer they live, the more closer to that state they'll become.

17
00:02:16,000 --> 00:02:28,000
They'll find themselves entering into the attainment of nibana, more and more throughout their lives,

18
00:02:28,000 --> 00:02:45,000
generally find themselves less involved in the world, and eventually at the end of their life they become totally free.

19
00:02:45,000 --> 00:02:49,000
I don't think you have to sustain.

20
00:02:49,000 --> 00:02:54,000
I think it's important that what Paul and Yany said that you do meditate.

21
00:02:54,000 --> 00:03:03,000
A person who reaches enlightenment continues to meditate and continues to work hard,

22
00:03:03,000 --> 00:03:13,000
but no matter what they do, there will be no more arising of greed, anger, or delusion.

23
00:03:13,000 --> 00:03:19,000
That's really the end.

24
00:03:19,000 --> 00:03:26,000
So it's not like a plateau, and you don't have to sustain that, but it doesn't mean you stop meditating,

25
00:03:26,000 --> 00:03:42,000
it doesn't mean you stop working or learning, studying, even practicing.

26
00:03:42,000 --> 00:03:52,000
Well, like you said, it's pretty much the end of how far you need to go,

27
00:03:52,000 --> 00:03:58,000
but then once someone reaches enlightenment they could still improve in other ways,

28
00:03:58,000 --> 00:04:02,000
like they may become enlightened, but not be very good at teaching others how to do it.

29
00:04:02,000 --> 00:04:04,000
They could become a better teacher.

30
00:04:04,000 --> 00:04:10,000
They could develop psychic powers and such.

31
00:04:10,000 --> 00:04:15,000
There's other things that they could develop and improve,

32
00:04:15,000 --> 00:04:44,000
but they're unneeded things, really.

