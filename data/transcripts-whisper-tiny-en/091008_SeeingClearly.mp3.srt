1
00:00:00,000 --> 00:00:07,000
Namatu Ratanatayasa.

2
00:00:07,000 --> 00:00:10,000
Welcome everyone.

3
00:00:10,000 --> 00:00:22,000
Today is the session on insight meditation.

4
00:00:22,000 --> 00:00:31,000
I think of it sort of as a time where we can get away.

5
00:00:31,000 --> 00:00:39,000
Get away even from Buddhism where we don't have to talk about Buddhist concepts.

6
00:00:39,000 --> 00:00:46,000
Where we can leave behind our affairs in the world.

7
00:00:46,000 --> 00:01:01,000
But we can also have a chance just to practice and not to worry about even Buddhist concepts or Buddhism.

8
00:01:01,000 --> 00:01:03,000
Not meaning that we're going to throw away.

9
00:01:03,000 --> 00:01:09,000
We've learned or we're going to go outside of the Buddhist teaching.

10
00:01:09,000 --> 00:01:11,000
Meaning that we don't have to study.

11
00:01:11,000 --> 00:01:15,000
We don't have to talk about anything really.

12
00:01:15,000 --> 00:01:21,000
So it should be considered that this is a time where we can have a sort of a guided meditation.

13
00:01:21,000 --> 00:01:24,000
You don't have to think of it even as guided.

14
00:01:24,000 --> 00:01:30,000
It's just sort of a meditation with audio accompaniment.

15
00:01:30,000 --> 00:01:36,000
So I'm going to put my character on busy.

16
00:01:36,000 --> 00:01:40,000
I didn't encourage people to do the same.

17
00:01:40,000 --> 00:01:45,000
We can ignore things for a while.

18
00:01:45,000 --> 00:01:50,000
Certainly isn't a time to be chatting with somebody else while you're sitting here.

19
00:01:50,000 --> 00:01:58,000
Although it's certainly up to you and I won't know if you do.

20
00:01:58,000 --> 00:02:09,000
So you can just close your eyes and just take some time to reflect on ourselves

21
00:02:09,000 --> 00:02:14,000
to look at ourselves, to look within.

22
00:02:14,000 --> 00:02:25,000
To study this elusive creature we call reality.

23
00:02:25,000 --> 00:02:31,000
Reality is kind of a funny word, I think.

24
00:02:31,000 --> 00:02:38,000
If you ask yourself what is real, I think it's pretty hard to pinpoint.

25
00:02:38,000 --> 00:02:43,000
Let me see how far science has gone chasing after reality.

26
00:02:43,000 --> 00:02:49,000
You also see how far the religions of the world have gone in the other direction.

27
00:02:49,000 --> 00:02:57,000
Making all sorts of bold claims and assertions about what is real.

28
00:02:57,000 --> 00:03:07,000
For me I can't figure out what sort of a definition you could give to the word reality.

29
00:03:07,000 --> 00:03:12,000
How you can define whether something is real or not.

30
00:03:12,000 --> 00:03:15,000
Except for that which is experienced.

31
00:03:15,000 --> 00:03:18,000
For me it's from what I understand.

32
00:03:18,000 --> 00:03:24,000
It's more of a mind game than anything if you talk about three-dimensional space.

33
00:03:24,000 --> 00:03:31,000
Or even if you talk about Brahma and Atman and being one with God and so on,

34
00:03:31,000 --> 00:03:40,000
as being the ultimate reality.

35
00:03:40,000 --> 00:03:47,000
It certainly seems look possible that you could become one with God or become God-like or so on.

36
00:03:47,000 --> 00:03:50,000
Could get to a state of oneness.

37
00:03:50,000 --> 00:03:57,000
But I don't see how that's any more real than the experience that we're having right here and right now.

38
00:03:57,000 --> 00:04:02,000
For people to say that our experience right here and right now is illusion.

39
00:04:02,000 --> 00:04:08,000
It just doesn't quite make sense.

40
00:04:08,000 --> 00:04:15,000
Whatever you experience to me that seems like to be the only thing that could possibly be real.

41
00:04:15,000 --> 00:04:20,000
The only way you can make sense of this word real.

42
00:04:20,000 --> 00:04:28,000
Because there is something there, there is an experience, that's what we're having right now.

43
00:04:28,000 --> 00:04:34,000
This is why Buddhism puts so much emphasis on the present moment because it's really all that we have.

44
00:04:34,000 --> 00:04:36,000
It's really all that exists.

45
00:04:36,000 --> 00:04:42,000
There's no future, there's no past, these are mind games we play with ourselves.

46
00:04:42,000 --> 00:04:54,000
There's only just a present moment, that's eternal, that doesn't end.

47
00:04:54,000 --> 00:04:59,000
But in this one moment there's a lot going on as we can see it's changing all the time.

48
00:04:59,000 --> 00:05:03,000
Our experience is not the same.

49
00:05:03,000 --> 00:05:09,000
Basically, it could be broken up into six categories and they come one at a time.

50
00:05:09,000 --> 00:05:14,000
So sometimes we're seeing, sometimes we're hearing, sometimes we're smelling, sometimes tasting,

51
00:05:14,000 --> 00:05:16,000
sometimes feeling and sometimes thinking.

52
00:05:16,000 --> 00:05:23,000
And it happens quite quickly if we're not meditating, it's quite difficult to see.

53
00:05:23,000 --> 00:05:32,000
So here when we sit in meditation with our eyes closed and we're watching reality.

54
00:05:32,000 --> 00:05:40,000
We're taking time to refine our vision, refine our perception.

55
00:05:40,000 --> 00:05:44,000
To see things clearer than we could see them before.

56
00:05:44,000 --> 00:05:51,000
We're not trying to push away, we're not trying to chase after, we're not trying to attain anything.

57
00:05:51,000 --> 00:05:56,000
We're just trying to see clearer than we normally do, normally could.

58
00:05:56,000 --> 00:06:06,000
All of the things that present themselves to the sixth senses.

59
00:06:06,000 --> 00:06:15,000
Why this is useful is because of the laws of nature which govern experience.

60
00:06:15,000 --> 00:06:20,000
When you see it's not just with impunity that you can see what you want and think about it,

61
00:06:20,000 --> 00:06:25,000
whatever you want, to have whatever reaction you choose.

62
00:06:25,000 --> 00:06:34,000
When we see something that we like, we automatically grasp onto it, hold onto it, and chase after it.

63
00:06:34,000 --> 00:06:38,000
There are natural laws going on at work here.

64
00:06:38,000 --> 00:06:46,000
And so depending on which mind states arise, when you see something, depending on what sort of a mind state arises next,

65
00:06:46,000 --> 00:06:52,000
it's going to really define the result of seeing or the result of hearing.

66
00:06:52,000 --> 00:07:01,000
When you see something you don't like, right away there's anger arises.

67
00:07:01,000 --> 00:07:07,000
If you're not careful, if you're not clearly aware of it.

68
00:07:07,000 --> 00:07:14,000
So our normal way of dealing with these things is either to get angry or to get greedy.

69
00:07:14,000 --> 00:07:24,000
And then either choose to repress it, you want something, but you know it's wrong to chase after things that you don't deserve

70
00:07:24,000 --> 00:07:27,000
or that belong to somebody else or so on.

71
00:07:27,000 --> 00:07:34,000
And so you repress it, or maybe it's something that you've been told is wrong.

72
00:07:34,000 --> 00:07:41,000
Sometimes for instance sex or sexuality is so stigmatized or it has been in the past.

73
00:07:41,000 --> 00:07:48,000
I think now people are getting more going to the other side where they just chase after it on the time.

74
00:07:48,000 --> 00:07:54,000
But still things like, things like masturbation, things like pornography or so on.

75
00:07:54,000 --> 00:07:59,000
There's still I think probably pretty stigmatized, at least overtly.

76
00:07:59,000 --> 00:08:03,000
So people do it covertly, they sneak it.

77
00:08:03,000 --> 00:08:10,000
And as a result there's a lot of repression involved pushing it down, pushing it away.

78
00:08:10,000 --> 00:08:13,000
This is something we do also with anger.

79
00:08:13,000 --> 00:08:19,000
We get really angry at people, but we push it down, we repress it.

80
00:08:19,000 --> 00:08:25,000
And we're pretty good at keeping it bottled up for a time, but we can only do it for a certain time.

81
00:08:25,000 --> 00:08:38,000
And the way it works is we'll be working really hard to keep ourselves calm, to keep ourselves from wanting or keep ourselves from hating.

82
00:08:38,000 --> 00:08:46,000
And then suddenly our guards down, one moment will be, maybe watching television or maybe doing something enjoyable.

83
00:08:46,000 --> 00:08:48,000
And we let our guard down.

84
00:08:48,000 --> 00:08:54,000
Or maybe something good comes and suddenly we feel sort of like safe.

85
00:08:54,000 --> 00:08:58,000
And as soon as we start to feel safe, we let our guard down and all just explodes.

86
00:08:58,000 --> 00:09:01,000
So we're something really fun, we like it.

87
00:09:01,000 --> 00:09:04,000
And so we're happy and then someone comes and says or does or something happens.

88
00:09:04,000 --> 00:09:12,000
And unpleasant and right away we flip out, or we chase after or so.

89
00:09:12,000 --> 00:09:14,000
This isn't the right way to deal with things.

90
00:09:14,000 --> 00:09:18,000
This isn't what we should be looking to do.

91
00:09:18,000 --> 00:09:21,000
It works, it works on a temporary basis.

92
00:09:21,000 --> 00:09:24,000
Sometimes even meditators have to do this if you're not.

93
00:09:24,000 --> 00:09:27,000
Strong enough to really just face it.

94
00:09:27,000 --> 00:09:29,000
You have to repress it at first.

95
00:09:29,000 --> 00:09:38,000
But it doesn't solve anything, just takes it away temporarily.

96
00:09:38,000 --> 00:09:43,000
Another way of dealing with it is as I said to chase after it, let it out.

97
00:09:43,000 --> 00:09:47,000
When you're angry at someone, let them know, just get a hangry.

98
00:09:47,000 --> 00:09:50,000
No use bottling it up, that's what they say.

99
00:09:50,000 --> 00:09:53,000
I agree, there's no use bottling it up.

100
00:09:53,000 --> 00:09:58,000
For sure we have to understand this, but doesn't mean that the opposite is correct.

101
00:09:58,000 --> 00:10:00,000
It's correct either.

102
00:10:00,000 --> 00:10:06,000
That we should chase after and cultivate these, at least brain activity.

103
00:10:06,000 --> 00:10:12,000
If you don't want to talk about the mind, just talk about brain activity.

104
00:10:12,000 --> 00:10:19,000
Scientists are perfectly aware how addiction works in terms of the brain, how the chemical reactions in the brain go.

105
00:10:19,000 --> 00:10:23,000
The more you get, the less pleasant it is.

106
00:10:23,000 --> 00:10:34,000
The less the more a stretched or where it worn down become the processes.

107
00:10:34,000 --> 00:10:37,000
So in the beginning it's very pleasurable to get what you want.

108
00:10:37,000 --> 00:10:43,000
But as you get it again and again, it takes more and more of it to really stimulate the chemical reactions.

109
00:10:43,000 --> 00:10:48,000
So it's less and less pleasurable for more and more work.

110
00:10:48,000 --> 00:10:57,000
That's why we always need something new, something more exciting.

111
00:10:57,000 --> 00:10:59,000
Anger, I suppose, works in a different way.

112
00:10:59,000 --> 00:11:11,000
I haven't really studied how the brain deals with anger, but it's just in the same way.

113
00:11:11,000 --> 00:11:15,000
It's changing the way the brain works or the brain reacts.

114
00:11:15,000 --> 00:11:20,000
If we're just talking about the brain and, of course, the mind is very closely interrelated with the brain.

115
00:11:20,000 --> 00:11:23,000
Each just as easily talk about one is talk about the other.

116
00:11:23,000 --> 00:11:28,000
Not that they're the same thing, but that they're very closely related.

117
00:11:28,000 --> 00:11:35,000
The way it works is, of course, you get angry and, of course, it's going to change the way the brain is structured.

118
00:11:35,000 --> 00:11:44,000
It's going to scar or it's going to leave its scratch on the brain.

119
00:11:44,000 --> 00:11:52,000
And on the mind, on the reality, which is the consciousness.

120
00:11:52,000 --> 00:11:58,000
It's going to change the chain reactions of events that are going to arise in the future.

121
00:11:58,000 --> 00:12:02,000
It's building up tendencies, but it's simply.

122
00:12:02,000 --> 00:12:08,000
So the more we act on these impulses, the more they become a part of us,

123
00:12:08,000 --> 00:12:10,000
the more they become a part of who we are.

124
00:12:10,000 --> 00:12:15,000
If you've ever killed killing, it's very difficult in the beginning.

125
00:12:15,000 --> 00:12:21,000
I think a lot of these things are drinking alcohol is very difficult in the beginning.

126
00:12:21,000 --> 00:12:29,000
Sexual intercourse is sometimes quite, and can be in some ways unpleasant in the beginning,

127
00:12:29,000 --> 00:12:33,000
because of how disgusting the body is, and so on.

128
00:12:33,000 --> 00:12:38,000
But it gets to be quite different after a while.

129
00:12:38,000 --> 00:12:41,000
It gets more taste in music.

130
00:12:41,000 --> 00:12:45,000
Sometimes you listen to something the first time, and you don't like it, and as you listen to it,

131
00:12:45,000 --> 00:12:48,000
it becomes more agreeable.

132
00:12:48,000 --> 00:12:55,000
Your mind gets used to it, and it knows how to react.

133
00:12:55,000 --> 00:12:59,000
So it gives rise to either pleasure or pain.

134
00:12:59,000 --> 00:13:01,000
It's used to reacting in that way.

135
00:13:01,000 --> 00:13:05,000
So it's very reactionary as a result of building up tendencies.

136
00:13:05,000 --> 00:13:12,000
So this isn't a good way to deal with anxiety.

137
00:13:12,000 --> 00:13:17,000
In meditation, we're going to try to look and see things simply for what they are.

138
00:13:17,000 --> 00:13:21,000
When you get angry, when you get greedy, even when you haven't gotten to that stage yet,

139
00:13:21,000 --> 00:13:26,000
and you just see something, just to see it for what it is.

140
00:13:26,000 --> 00:13:31,000
Not like see something, and you like it, and really hate yourself for liking it and wanting it or so.

141
00:13:31,000 --> 00:13:37,000
You know, these stories about people coveting their neighbors' wives or husbands,

142
00:13:37,000 --> 00:13:45,000
and then hating themselves for it, or people who are angry and get angry a lot,

143
00:13:45,000 --> 00:13:49,000
and then get angry at themselves because they're angry people,

144
00:13:49,000 --> 00:13:52,000
because they're spiteful people, people who are addicted to shopping,

145
00:13:52,000 --> 00:13:57,000
people who are addicted to eating, and then they hate themselves for it.

146
00:13:57,000 --> 00:13:59,000
They judge themselves for it.

147
00:13:59,000 --> 00:14:05,000
Or else the opposite, you like something, and then you think it's good that you like it.

148
00:14:05,000 --> 00:14:09,000
You're happy that you're so greedy, you're happy that you can get what you want.

149
00:14:09,000 --> 00:14:17,000
I think it's a good thing, so you cultivate it more and more and more.

150
00:14:17,000 --> 00:14:30,000
So we follow in these cycles of repression and release, going back and forth sometimes,

151
00:14:30,000 --> 00:14:33,000
or developing them to quite extremes.

152
00:14:33,000 --> 00:14:37,000
In meditation, we're just trying to look and see things as they are.

153
00:14:37,000 --> 00:14:41,000
We're trying to come to understand things, understand how this works,

154
00:14:41,000 --> 00:14:46,000
and understand reality in a clearer way than we normally would.

155
00:14:46,000 --> 00:14:54,000
As we look, we're going to see that, first of all, the objects of the sense,

156
00:14:54,000 --> 00:14:59,000
there's nothing in them that is intrinsically desirable or undesirable.

157
00:14:59,000 --> 00:15:04,000
There's nothing that you could ever see that would be intrinsically beautiful.

158
00:15:04,000 --> 00:15:08,000
What would it mean that something is intrinsically beautiful?

159
00:15:08,000 --> 00:15:14,000
What does it mean to say that a flower is beautiful, or a body, a human body is beautiful,

160
00:15:14,000 --> 00:15:17,000
or ugly?

161
00:15:17,000 --> 00:15:18,000
It's meaningless.

162
00:15:18,000 --> 00:15:20,000
It means nothing.

163
00:15:20,000 --> 00:15:26,000
It means that we've developed these tendencies from life to life to believe this,

164
00:15:26,000 --> 00:15:28,000
convincing ourselves of it.

165
00:15:28,000 --> 00:15:31,000
You can see this happening, as I said, even in this life.

166
00:15:31,000 --> 00:15:35,000
People who take their first beer and hate it, and then as they get used to drinking beer,

167
00:15:35,000 --> 00:15:38,000
it becomes a wonderful thing.

168
00:15:38,000 --> 00:15:47,000
We train ourselves, we fool ourselves into thinking that this is a good thing.

169
00:15:47,000 --> 00:15:51,000
We have pain, and we convince ourselves that pain is bad,

170
00:15:51,000 --> 00:15:56,000
and doctors tell us that, oh, pain is your body's way of telling you that something's wrong.

171
00:15:56,000 --> 00:15:59,000
But bodies don't have ways of telling anything,

172
00:15:59,000 --> 00:16:03,000
bodies just react, they just have chain reactions.

173
00:16:03,000 --> 00:16:06,000
If anything, it's our reaction to pain that is created, this body,

174
00:16:06,000 --> 00:16:08,000
and made it the way it is.

175
00:16:08,000 --> 00:16:11,000
So instead of being something,

176
00:16:11,000 --> 00:16:13,000
you know, really what you would expect us to be,

177
00:16:13,000 --> 00:16:17,000
you'd expect us to be balls of light, it makes more sense.

178
00:16:17,000 --> 00:16:20,000
Why do we have ten fingers, ten toes,

179
00:16:20,000 --> 00:16:23,000
and all of these funny organs, and so on,

180
00:16:23,000 --> 00:16:28,000
that are all imperfect and all like they were just patched together?

181
00:16:28,000 --> 00:16:35,000
If there is a God, we certainly weren't made in His image or her image.

182
00:16:35,000 --> 00:16:39,000
I had that hurt. It's a pretty lousy God.

183
00:16:39,000 --> 00:16:45,000
And so, evolution has done wonders in clearing this up

184
00:16:45,000 --> 00:16:48,000
and saying how things came physically.

185
00:16:48,000 --> 00:16:53,000
We just sort of adapting that to what we can see is real,

186
00:16:53,000 --> 00:16:55,000
what's really happening.

187
00:16:55,000 --> 00:17:00,000
So that slowly, slowly, we're patching together and piecing together all of it.

188
00:17:00,000 --> 00:17:04,000
There's nothing that's intrinsically good or intrinsically bad.

189
00:17:04,000 --> 00:17:08,000
And so, we're going to see that. We can't see it normally.

190
00:17:08,000 --> 00:17:13,000
When we see something, we write away, make a decision whether it's good or bad.

191
00:17:13,000 --> 00:17:17,000
And that decision is totally meaningless, totally based on nothing,

192
00:17:17,000 --> 00:17:23,000
based on our own idea at the time.

193
00:17:23,000 --> 00:17:28,000
We could like something one day and hate it the next.

194
00:17:28,000 --> 00:17:31,000
Sometimes it's based on function.

195
00:17:31,000 --> 00:17:34,000
Let me see, this is beautiful and this is good because it works.

196
00:17:34,000 --> 00:17:41,000
Like it's a good chair or a good car or so on because it runs well and so on.

197
00:17:41,000 --> 00:17:43,000
But these are still just value judgments.

198
00:17:43,000 --> 00:17:45,000
It's not good for anything really.

199
00:17:45,000 --> 00:17:46,000
In the end, everything is worthless.

200
00:17:46,000 --> 00:17:48,000
There's nothing that is worth anything in the world.

201
00:17:48,000 --> 00:17:53,000
Not you, not I, not any experience that has any intrinsic worth.

202
00:17:53,000 --> 00:17:57,000
It just comes and goes, worth is not something you can speak of.

203
00:17:57,000 --> 00:18:00,000
Just is.

204
00:18:00,000 --> 00:18:02,000
And so, in meditation, we're coming to see that.

205
00:18:02,000 --> 00:18:03,000
We're coming to see that.

206
00:18:03,000 --> 00:18:07,000
That's the way it is.

207
00:18:07,000 --> 00:18:10,000
And the way I always recommend to do that is just remind yourself,

208
00:18:10,000 --> 00:18:13,000
this word set-d, which we translate as mindfulness.

209
00:18:13,000 --> 00:18:14,000
It just means to remind yourself.

210
00:18:14,000 --> 00:18:16,000
It doesn't mean mindfulness.

211
00:18:16,000 --> 00:18:22,000
Set-d means remembrance or that which you use to remember,

212
00:18:22,000 --> 00:18:24,000
that which you use to remind yourself,

213
00:18:24,000 --> 00:18:28,000
or the act of remembering.

214
00:18:28,000 --> 00:18:33,000
When you remind yourself of what you've forgotten,

215
00:18:33,000 --> 00:18:37,000
that this is pain, or this is seeing,

216
00:18:37,000 --> 00:18:39,000
or this is liking, or this is anger,

217
00:18:39,000 --> 00:18:44,000
or all of the myriad of different experiences,

218
00:18:44,000 --> 00:18:48,000
you just remind yourself that's pain.

219
00:18:48,000 --> 00:18:50,000
And your mind gets closer to the reality

220
00:18:50,000 --> 00:18:55,000
and gets to see it clearer than it used to see it.

221
00:18:55,000 --> 00:18:59,000
Then you see how it happens when a sensation arises,

222
00:18:59,000 --> 00:19:01,000
or an experience arises,

223
00:19:01,000 --> 00:19:03,000
then there's a happy feeling or an unhappy feeling.

224
00:19:03,000 --> 00:19:05,000
This is where we go wrong.

225
00:19:05,000 --> 00:19:07,000
When a pleasant feeling comes up, we like it.

226
00:19:07,000 --> 00:19:10,000
We hold on to it.

227
00:19:10,000 --> 00:19:11,000
We want it.

228
00:19:11,000 --> 00:19:13,000
We want more.

229
00:19:13,000 --> 00:19:17,000
And so, we give rise to the cycle of addiction.

230
00:19:17,000 --> 00:19:19,000
When something unpleasant comes up,

231
00:19:19,000 --> 00:19:23,000
then we don't like it.

232
00:19:23,000 --> 00:19:29,000
We give rise to the cycle of hatred.

233
00:19:29,000 --> 00:19:31,000
And we have so many cycles like this.

234
00:19:31,000 --> 00:19:33,000
We have the cycles of depression, cycles of fear,

235
00:19:33,000 --> 00:19:34,000
and anxiety.

236
00:19:34,000 --> 00:19:37,000
People who get anxious and then can worry that they're too anxious

237
00:19:37,000 --> 00:19:39,000
and so get even more anxious.

238
00:19:39,000 --> 00:19:43,000
People who are awake all night trying to get to sleep

239
00:19:43,000 --> 00:19:47,000
because the more stress they are about not being able to sleep,

240
00:19:47,000 --> 00:19:49,000
then they get stressed about being stressed

241
00:19:49,000 --> 00:19:51,000
and how to not be stressed.

242
00:19:51,000 --> 00:19:57,000
And so they stress about trying to not be stressed and so on.

243
00:19:57,000 --> 00:20:03,000
All we're doing here is coming to see things as they are.

244
00:20:03,000 --> 00:20:07,000
We're establishing what we understand to be real

245
00:20:07,000 --> 00:20:09,000
and then we're looking at that.

246
00:20:09,000 --> 00:20:11,000
We're working on that.

247
00:20:11,000 --> 00:20:13,000
We're working with that as our base.

248
00:20:13,000 --> 00:20:15,000
We're not going to get into theories or ideas,

249
00:20:15,000 --> 00:20:19,000
not even Buddhism, not even this or that.

250
00:20:19,000 --> 00:20:21,000
Just it's here.

251
00:20:21,000 --> 00:20:22,000
It's now.

252
00:20:22,000 --> 00:20:24,000
You don't have to decide whether it's real or not.

253
00:20:24,000 --> 00:20:25,000
It's there.

254
00:20:25,000 --> 00:20:27,000
You're experiencing it.

255
00:20:27,000 --> 00:20:31,000
All we're trying to do is come to see it clearer than we normally do.

256
00:20:31,000 --> 00:20:32,000
It's this.

257
00:20:32,000 --> 00:20:33,000
OK.

258
00:20:33,000 --> 00:20:34,000
Let's look and see.

259
00:20:34,000 --> 00:20:35,000
What is it really?

260
00:20:35,000 --> 00:20:50,000
So in one way, this mantra that we use is a way of focusing on reality.

261
00:20:50,000 --> 00:20:56,000
In another way, it's a way of straightening up the mind.

262
00:20:56,000 --> 00:21:00,000
Because the mind sees, but it doesn't say that seeing.

263
00:21:00,000 --> 00:21:05,000
It says that's him or her or that or this.

264
00:21:05,000 --> 00:21:10,000
Good or bad, me or mine, us and them.

265
00:21:10,000 --> 00:21:13,000
Totally forgotten that that's just seeing.

266
00:21:13,000 --> 00:21:20,000
When you hear something, it's wonderful music or evil person yelling at me.

267
00:21:20,000 --> 00:21:22,000
I don't deserve to be yelled at.

268
00:21:22,000 --> 00:21:25,000
We've forgotten already.

269
00:21:25,000 --> 00:21:28,000
We've forgotten totally what's going on.

270
00:21:28,000 --> 00:21:33,000
We're just reminding ourselves, straightening out our mind saying, look, you're all out of shape.

271
00:21:33,000 --> 00:21:35,000
You're all out of whack here.

272
00:21:35,000 --> 00:21:36,000
That's not real.

273
00:21:36,000 --> 00:21:42,000
That's not what's going on here.

274
00:21:42,000 --> 00:21:43,000
And you can do it with anything.

275
00:21:43,000 --> 00:21:47,000
And as you do it more and more, get better at it and better at it.

276
00:21:47,000 --> 00:21:51,000
You come to see so many of the wrong ways we deal with reality.

277
00:21:51,000 --> 00:21:52,000
We get stressed.

278
00:21:52,000 --> 00:21:54,000
We get tense.

279
00:21:54,000 --> 00:21:58,000
We try to force things, even just using this mantra.

280
00:21:58,000 --> 00:22:01,000
It's so often the beginning people will get headaches.

281
00:22:01,000 --> 00:22:06,000
And they say it's just not for them because it's like forcing things.

282
00:22:06,000 --> 00:22:10,000
And this is a part.

283
00:22:10,000 --> 00:22:14,000
This is the reason why we're doing this is so that we can see the way we react to things.

284
00:22:14,000 --> 00:22:18,000
Even meditation, we react totally wrongly to it.

285
00:22:18,000 --> 00:22:22,000
We tell people to breathe and watch the stomach rising and falling.

286
00:22:22,000 --> 00:22:24,000
And so right away they're trying to breathe deep.

287
00:22:24,000 --> 00:22:27,000
They're trying to breathe perfectly smooth.

288
00:22:27,000 --> 00:22:28,000
And they're trying to force their breath.

289
00:22:28,000 --> 00:22:32,000
And the more they force it, the more suffering comes to them,

290
00:22:32,000 --> 00:22:34,000
the more stressful it is.

291
00:22:34,000 --> 00:22:37,000
Then they blame the meditation and say, this sucks.

292
00:22:37,000 --> 00:22:38,000
This is terrible.

293
00:22:38,000 --> 00:22:39,000
It's not pleasant.

294
00:22:39,000 --> 00:22:41,000
It's uncomfortable.

295
00:22:41,000 --> 00:22:44,000
What really sucks is our attitude towards it.

296
00:22:44,000 --> 00:22:48,000
And then we're forcing, controlling,

297
00:22:48,000 --> 00:22:52,000
and stressing, needing it to be in a certain way.

298
00:22:52,000 --> 00:22:56,000
And not able to accept it for what it is.

299
00:22:56,000 --> 00:22:58,000
Even we sit in meditation, we get headaches.

300
00:22:58,000 --> 00:22:59,000
So what?

301
00:22:59,000 --> 00:23:01,000
What's wrong with the headache?

302
00:23:01,000 --> 00:23:06,000
What's wrong is our stress that comes from having a headache.

303
00:23:06,000 --> 00:23:08,000
People sit in meditation, they have a backache.

304
00:23:08,000 --> 00:23:16,000
And they have all these theories about how you have to sit like this or like that.

305
00:23:16,000 --> 00:23:19,000
Then your back will be perfect and they'll be no pain and so on.

306
00:23:19,000 --> 00:23:20,000
And it's really ridiculous.

307
00:23:20,000 --> 00:23:22,000
Your back can't be perfect.

308
00:23:22,000 --> 00:23:27,000
Your back's just going to get worse and worse until you die.

309
00:23:27,000 --> 00:23:30,000
That's not what we're here for.

310
00:23:30,000 --> 00:23:34,000
I've sat in meditation for eight years with my back hunched over.

311
00:23:34,000 --> 00:23:37,000
My back's not dead yet.

312
00:23:37,000 --> 00:23:39,000
My teacher's 86 this year.

313
00:23:39,000 --> 00:23:45,000
And he's been doing it I guess for 50, maybe 60, probably 60 over 60 years.

314
00:23:45,000 --> 00:23:48,000
With his back hunched.

315
00:23:48,000 --> 00:23:49,000
I'm not hunched.

316
00:23:49,000 --> 00:23:51,000
It just means sitting naturally.

317
00:23:51,000 --> 00:23:52,000
I'm not worried about your posture.

318
00:23:52,000 --> 00:23:54,000
I'm not worried about anything.

319
00:23:54,000 --> 00:23:55,000
Just watching it.

320
00:23:55,000 --> 00:23:56,000
Oh, now there's pain.

321
00:23:56,000 --> 00:24:00,000
Oh, interesting.

322
00:24:00,000 --> 00:24:05,000
It'll be fine.

323
00:24:05,000 --> 00:24:10,000
This means this is the practice of insight to see clearly.

324
00:24:10,000 --> 00:24:14,000
Not to judge, not to need, not to worry, not to stress.

325
00:24:14,000 --> 00:24:18,000
Everything just works itself out.

326
00:24:18,000 --> 00:24:20,000
Come back to nature.

327
00:24:20,000 --> 00:24:21,000
You come back to now.

328
00:24:21,000 --> 00:24:24,000
You come back to reality.

329
00:24:24,000 --> 00:24:51,000
There's nothing in the world that we will claim to.

330
00:24:51,000 --> 00:25:16,000
It's kind of like untying a knot.

331
00:25:16,000 --> 00:25:22,000
You have this big knot and you look at it and you say,

332
00:25:22,000 --> 00:25:24,000
well, that's a mess.

333
00:25:24,000 --> 00:25:27,000
There's this big knot there.

334
00:25:27,000 --> 00:25:30,000
But as you untie it just disappears.

335
00:25:30,000 --> 00:25:34,000
And when you've got it totally untied, there's nothing there.

336
00:25:34,000 --> 00:25:37,000
It's just gone.

337
00:25:37,000 --> 00:25:43,000
Really, all of our problems and all of our stresses, all of our worries and all of our cares.

338
00:25:43,000 --> 00:25:45,000
They're all just like the knot.

339
00:25:45,000 --> 00:25:50,000
They're not really even there.

340
00:25:50,000 --> 00:25:53,000
They're just, all they are is a kink.

341
00:25:53,000 --> 00:25:58,000
All the errors are not.

342
00:25:58,000 --> 00:26:05,000
We have all these ideas of who we are, what we are, what we have, what we deal with.

343
00:26:05,000 --> 00:26:13,000
Places, people, things, ideas, concepts, views, everything, the longings.

344
00:26:13,000 --> 00:26:16,000
None of them exist, they're not even real.

345
00:26:16,000 --> 00:26:23,000
It's all just knots clinging, liking, or disliking.

346
00:26:23,000 --> 00:26:30,000
Holding up as good, throwing down as bad.

347
00:26:30,000 --> 00:26:38,000
Pulling in as me, pushing out as you.

348
00:26:38,000 --> 00:26:43,000
In the end it's all just experience, it's all just reality.

349
00:26:43,000 --> 00:26:47,000
It's just one moment of experience.

350
00:26:47,000 --> 00:26:53,000
We've created so many knots.

351
00:26:53,000 --> 00:26:57,000
And then we get worried and stressed about them and say, oh, look at that big knot.

352
00:26:57,000 --> 00:26:59,000
It's not even there, it's just a kink.

353
00:26:59,000 --> 00:27:02,000
It's just a rope.

354
00:27:02,000 --> 00:27:05,000
Got it out of shape.

355
00:27:05,000 --> 00:27:08,000
We'll bunched up, we'll mist up.

356
00:27:08,000 --> 00:27:11,000
I do straighten it out.

357
00:27:11,000 --> 00:27:34,000
Straighten it out and keep going.

358
00:27:34,000 --> 00:27:39,000
So in that sense, Nirvana or Nirvana, the goal of the practice, there's not really anything special.

359
00:27:39,000 --> 00:27:42,000
It's not really anything mysterious.

360
00:27:42,000 --> 00:27:44,000
It's just when all the knots are gone.

361
00:27:44,000 --> 00:27:51,000
I mean, it is something profound because there's nothing left.

362
00:27:51,000 --> 00:27:57,000
At the experience of Nirvana, it's totally unkinked.

363
00:27:57,000 --> 00:28:04,000
And there's nothing, there's no friction.

364
00:28:04,000 --> 00:28:05,000
There's no stress.

365
00:28:05,000 --> 00:28:06,000
There's no seeing.

366
00:28:06,000 --> 00:28:07,000
There's no hearing.

367
00:28:07,000 --> 00:28:08,000
There's no smelling.

368
00:28:08,000 --> 00:28:09,000
There's no tasting.

369
00:28:09,000 --> 00:28:10,000
There's no feeling.

370
00:28:10,000 --> 00:28:11,000
There's no thinking.

371
00:28:11,000 --> 00:28:16,000
Suddenly just ceased.

372
00:28:16,000 --> 00:28:25,000
And it might even be momentary.

373
00:28:25,000 --> 00:28:35,000
Nirvana can be just a fleeting moment, but it's a profound moment.

374
00:28:35,000 --> 00:28:43,000
Something that changes a person to the core, changes the way the person thinks.

375
00:28:43,000 --> 00:28:48,000
It's like a cut.

376
00:28:48,000 --> 00:28:57,000
Finally, the bonds are broken.

377
00:28:57,000 --> 00:29:07,000
Like there's a gap in the string or a gap in the chain.

378
00:29:07,000 --> 00:29:17,000
And that profoundly shakes out the way the person looks at the world because before we looked at the world as being.

379
00:29:17,000 --> 00:29:29,000
And being full of things and concepts and people and selves and souls and egos.

380
00:29:29,000 --> 00:29:40,000
Once you experience the sensations of stress, the cessation of all of this,

381
00:29:40,000 --> 00:29:49,000
it all just clears up and it becomes just experience, just reality.

382
00:29:49,000 --> 00:29:52,000
And then it all bunches up again.

383
00:29:52,000 --> 00:29:57,000
And you're back to liking or disliking, but there's something, there's a gap there.

384
00:29:57,000 --> 00:29:59,000
There's a memory of this.

385
00:29:59,000 --> 00:30:04,000
There's a selected mark as well.

386
00:30:04,000 --> 00:30:16,000
And you can't ever look at things as permanent as stable as me or mine.

387
00:30:16,000 --> 00:30:21,000
It just doesn't make sense anymore.

388
00:30:21,000 --> 00:30:32,000
The experience of total cessation.

389
00:30:32,000 --> 00:30:47,000
And this is even more true, the more you practice and the more it comes that a person is able to experience this state.

390
00:30:47,000 --> 00:31:03,000
To the point where all the kings, there's so many gaps, there's so many interruptions.

391
00:31:03,000 --> 00:31:09,000
And it's just the whole thing just collapses and there's no more, there's no more kinking.

392
00:31:09,000 --> 00:31:17,000
And the wisdom finally gains hold and understanding that this is the way it is finally gains hold.

393
00:31:17,000 --> 00:31:30,000
The mind finally changes from still believing, still seeking, still wishing, still hoping that there's going to be something that's stable, something that's permanent, something that's satisfying, something that we can control.

394
00:31:30,000 --> 00:31:38,000
Finally accepts the fact that there's nothing in the world that's really wonderful and beautiful.

395
00:31:38,000 --> 00:31:50,000
And they say, this is the final graduation.

396
00:31:50,000 --> 00:31:58,000
This is finally becoming free from illusion and we finally understand reality as it is.

397
00:31:58,000 --> 00:31:59,000
It's very simple.

398
00:31:59,000 --> 00:32:01,000
It's just seeing things as they are.

399
00:32:01,000 --> 00:32:08,000
There's no special reality or special experience.

400
00:32:08,000 --> 00:32:12,000
It just is reality, just as experience.

401
00:32:12,000 --> 00:32:19,000
And the only problem, the only single problem is our delusion or misunderstanding of things.

402
00:32:19,000 --> 00:32:35,000
The fact that we can't see things as they are, we can't see things clearly.

403
00:32:49,000 --> 00:32:58,000
So I think that's enough for today.

404
00:32:58,000 --> 00:33:02,000
If you like, you're welcome to just continue meditating here.

405
00:33:02,000 --> 00:33:06,000
If anyone has any questions, you're welcome to ask.

406
00:33:06,000 --> 00:33:26,000
Thank you all for coming.

