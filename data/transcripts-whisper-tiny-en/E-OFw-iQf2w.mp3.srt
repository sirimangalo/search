1
00:00:00,000 --> 00:00:17,320
Okay, we are live, let's make sure we're live collecting test, okay, well, we can hear

2
00:00:17,320 --> 00:00:18,320
me anyway.

3
00:00:18,320 --> 00:00:29,720
So today we're going to do another demo pattern, let's get this all set up properly.

4
00:00:29,720 --> 00:00:35,720
One day, did you want to have the earbuds in during the demo pattern?

5
00:00:35,720 --> 00:00:36,720
No.

6
00:00:36,720 --> 00:00:59,400
See, that's why I need a team, okay, I think we're ready to go.

7
00:00:59,400 --> 00:01:02,400
And welcome back to our study of the demo pattern.

8
00:01:02,400 --> 00:01:08,840
Today we continue on with first number 107, which reads as follows.

9
00:01:08,840 --> 00:01:37,920
And one might and a being, you know, whatever being might,

10
00:01:37,920 --> 00:01:45,120
a gig, but he can't jedi when he live in the forest,

11
00:01:45,120 --> 00:01:47,720
carrying tending to a fire.

12
00:01:47,720 --> 00:01:53,360
So in the event of forest for a hundred years,

13
00:01:53,360 --> 00:01:58,520
a kanja bawitatana misses the same as yesterday's first.

14
00:01:58,520 --> 00:02:05,560
Muhudam people, jedi, one should pay homage to one of the develop, one who has developed themselves,

15
00:02:05,560 --> 00:02:08,880
just for one moment.

16
00:02:08,880 --> 00:02:18,520
Sayya yuapu jedi say you, this puja is greater, greater than the one who for a hundred

17
00:02:18,520 --> 00:02:23,600
years practices, this sacrifice, fire sacrifice.

18
00:02:23,600 --> 00:02:33,880
Now, not a, not a terribly meaningful verse for those of us who aren't familiar with living

19
00:02:33,880 --> 00:02:38,680
in the forest tending to a fire, but we can generalize and talk about it.

20
00:02:38,680 --> 00:02:43,240
The story as well is quite similar to yesterday, it's almost identical.

21
00:02:43,240 --> 00:02:46,880
Difference here is the, sorry, put that instead of going to talk to his uncle,

22
00:02:46,880 --> 00:02:52,120
he doesn't talk to his nephew.

23
00:02:52,120 --> 00:02:57,240
And he asks his nephew so nephew, his nephew is also a Brahman, his whole family, of course,

24
00:02:57,240 --> 00:02:58,800
was Brahman.

25
00:02:58,800 --> 00:03:07,280
He says, what sort of King Brahman Akusulankarosi, what sort of wholesomeness or good deeds

26
00:03:07,280 --> 00:03:08,280
do you do?

27
00:03:08,280 --> 00:03:17,520
And so he says, oh, every month, mase mase, a kang yi kang bhasun, every month, I have, having

28
00:03:17,520 --> 00:03:27,080
killed a passu livestock, like a pet of some sort, some kind of domesticated animal or

29
00:03:27,080 --> 00:03:37,880
some animal, the sheep may be a goat, probably, a king, bhari, bhari, jharami, I use it to

30
00:03:37,880 --> 00:03:44,720
tend to a fire or burn it in a fire, maybe make a burnt offering, it sounds like he just

31
00:03:44,720 --> 00:03:55,000
burns it in a fire, kind of an abramic sort of practice, it's with the Jewish people

32
00:03:55,000 --> 00:03:56,000
with you.

33
00:03:56,000 --> 00:03:58,240
It's very similar.

34
00:03:58,240 --> 00:04:07,560
But fire sacrifice seems to have originally been burning goats and so on, all over the world.

35
00:04:07,560 --> 00:04:13,000
Somehow this was a big thing to do.

36
00:04:13,000 --> 00:04:23,040
And then he says, for what purpose, kimatang avang karosi, for what purpose do you do that?

37
00:04:23,040 --> 00:04:31,560
And he says, oh, for the same as before, oh, for the bramalokamago giri, so I have heard

38
00:04:31,560 --> 00:04:38,120
or it is said that this is the path to the bramaloka.

39
00:04:38,120 --> 00:04:40,400
And sorry, but this is, who told you that?

40
00:04:40,400 --> 00:04:43,520
Same as yesterday, same as last time.

41
00:04:43,520 --> 00:04:45,960
And again, it was my teachers.

42
00:04:45,960 --> 00:04:51,760
So for the first one, it was for last one, it was easy to kind of get a sense that his

43
00:04:51,760 --> 00:04:56,600
teachers were these naked ascetics, last time Randy was giving every month, giving a thousand

44
00:04:56,600 --> 00:05:02,800
pieces of money worth of food or whatever, maybe even just money, directly to these

45
00:05:02,800 --> 00:05:05,400
naked ascetics.

46
00:05:05,400 --> 00:05:09,000
And the one we think that they were probably his teachers who were teaching in this.

47
00:05:09,000 --> 00:05:14,600
Now, in this case, the question is, why are his teachers teaching such things?

48
00:05:14,600 --> 00:05:15,920
There's various reasons.

49
00:05:15,920 --> 00:05:20,920
And we can extrapolate this sort of generalized in terms of religion.

50
00:05:20,920 --> 00:05:23,400
Why do people teach such ridiculous things?

51
00:05:23,400 --> 00:05:28,520
I mean, it's understandable why people believe them.

52
00:05:28,520 --> 00:05:33,480
We just tend to believe things based on tradition or based on the authority of our teachers,

53
00:05:33,480 --> 00:05:38,720
even though we ourselves have no sense of the causal relationship.

54
00:05:38,720 --> 00:05:44,920
So this is why religions do such wide array of practices, because we're just told to

55
00:05:44,920 --> 00:05:53,680
buy our priests or buy our leaders, the question is, why do the leaders teach such things?

56
00:05:53,680 --> 00:05:55,720
Sometimes it's just for gain.

57
00:05:55,720 --> 00:05:57,880
I mean, I've seen it even in Buddhism.

58
00:05:57,880 --> 00:06:06,720
People come to the monasteries or the temples, and they ask the teacher, what should

59
00:06:06,720 --> 00:06:07,720
I do?

60
00:06:07,720 --> 00:06:09,240
Someone passed away, what should I do?

61
00:06:09,240 --> 00:06:12,720
And I've seen teachers, you can just, I've seen monks, you can just see they're the

62
00:06:12,720 --> 00:06:19,360
wheels turning in their head, kind of in a bit of panic, like, oh, scrambling to find

63
00:06:19,360 --> 00:06:23,440
something that that looks or sounds kind of mystical.

64
00:06:23,440 --> 00:06:28,640
I mean, it really comes down to that, just making something up on the spur of the moment

65
00:06:28,640 --> 00:06:38,080
to appease people's desire for some ritual, something to solve their problems.

66
00:06:38,080 --> 00:06:47,360
Sri Dhammika, a very wonderful sort of populist Buddhist monk and teacher from Sri

67
00:06:47,360 --> 00:06:53,480
Lanka, he said, someone asked him about these ambulance that monks gave out, and he said,

68
00:06:53,480 --> 00:06:59,520
well, they have absolutely no meaning, but he said sometimes, sometimes for people who are

69
00:06:59,520 --> 00:07:03,960
new or who don't really have a good understanding of cause and effect, you have to give

70
00:07:03,960 --> 00:07:04,960
them something.

71
00:07:04,960 --> 00:07:10,240
He took a bed, he said, you see this, I want you to keep it, hold onto it, and it will

72
00:07:10,240 --> 00:07:11,240
keep you safe.

73
00:07:11,240 --> 00:07:15,840
They said, sometimes you have to do that, that's a sort of like a like dumbo in his

74
00:07:15,840 --> 00:07:22,360
feather, if you ever saw the Disney movie, sometimes people need their magical feather.

75
00:07:22,360 --> 00:07:30,880
So there is a defense of it, but I don't think it's a very strong defense, especially

76
00:07:30,880 --> 00:07:37,720
when it becomes one's main, absolutely no defense, when it becomes one's main religious

77
00:07:37,720 --> 00:07:47,200
practice, and not only is there no defense, but it's a blame worthy when it involves

78
00:07:47,200 --> 00:07:54,320
unwholesome, clearly unwholesome acts, like it's one thing to tell people that offering

79
00:07:54,320 --> 00:08:01,520
food to a statue is going to be to their benefit, or pouring water on the root of a tree

80
00:08:01,520 --> 00:08:07,720
is going to do this and do that, I mean that's kind of innocent, and who knows, maybe the

81
00:08:07,720 --> 00:08:14,400
angels will get involved and help out with it, if they know there's this interesting

82
00:08:14,400 --> 00:08:24,360
thing, Buddhists in Sri Lanka are very keen to pour water at the roots of the Bodhi tree,

83
00:08:24,360 --> 00:08:30,200
so that the pour water, the water, the Bodhi tree that go around is very important ceremony,

84
00:08:30,200 --> 00:08:35,960
and they do this with the understanding that it leads to pregnancy.

85
00:08:35,960 --> 00:08:41,000
And it apparently has some measure of success, I mean, it's anecdotal, and scientists

86
00:08:41,000 --> 00:08:48,600
may be able to study it and find that in fact, there is no correlation, but it's an interesting

87
00:08:48,600 --> 00:08:52,880
idea that there might be some correlation, because how would that relate to the Buddhist

88
00:08:52,880 --> 00:08:53,880
teaching?

89
00:08:53,880 --> 00:08:57,440
It's certainly not a Buddhist teaching, that such a thing as possible, and yet we have

90
00:08:57,440 --> 00:09:01,800
in the Dhammapada stories, remember the first story that we did, I don't know if I

91
00:09:01,800 --> 00:09:06,320
actually brought that and brought up the whole backstory, but this guy does basically that,

92
00:09:06,320 --> 00:09:14,840
he goes to a tree and he cleans up around this great tree in the forest and puts up banners

93
00:09:14,840 --> 00:09:20,640
and flags and a wall around it to protect it, and then makes a promise to the tree that

94
00:09:20,640 --> 00:09:25,840
if he gets a son or a daughter, they'll come back and do a great homage and respect

95
00:09:25,840 --> 00:09:32,440
to the tree, and sure enough, his wife gives birth right quite quickly after that.

96
00:09:32,440 --> 00:09:35,920
So people from Sri Lanka have told me that this actually works, and I was trying to

97
00:09:35,920 --> 00:09:41,560
figure out exactly how it might work in reality, because there has to be a causal relationship,

98
00:09:41,560 --> 00:09:49,080
it can't just be magic, there's no such thing, and it's not really, it's not accepted

99
00:09:49,080 --> 00:09:50,080
in Buddhism.

100
00:09:50,080 --> 00:09:54,120
So the idea that somehow you could do some ritual and that kind of could work, the only

101
00:09:54,120 --> 00:10:00,200
way it could work, and I'm thinking it actually could, is if the angels got involved,

102
00:10:00,200 --> 00:10:08,800
because there's got to be something to do with angels hanging out at the Bodhi tree, and

103
00:10:08,800 --> 00:10:15,000
if the lay people come to the Bodhi tree and they do this and they make a sincere wish

104
00:10:15,000 --> 00:10:19,080
and they have a sincerely good heart, it's kind of like the angels can look down and say,

105
00:10:19,080 --> 00:10:24,480
oh, those are nice people, well, my lifespan is almost that, I think, maybe they even

106
00:10:24,480 --> 00:10:29,520
hang out looking for parents, and when the angels know that their lifespan is almost

107
00:10:29,520 --> 00:10:34,680
that, they try to find suitable parents to go and be reborn, I mean, something like that,

108
00:10:34,680 --> 00:10:41,200
somehow the angels, the day was get involved, but I give that only as an example of how

109
00:10:41,200 --> 00:10:45,840
there might be some causal relationship, but there has to be something like that, or

110
00:10:45,840 --> 00:10:51,640
else it's just ridiculous, and so for the most part it is, especially, it's beyond ridiculous

111
00:10:51,640 --> 00:10:55,680
when it comes down to killing animals, killing your fellow living beings and saying that

112
00:10:55,680 --> 00:10:58,720
somehow the past, the Brahma world.

113
00:10:58,720 --> 00:11:03,360
So, Sarayabhuna rightly says to him, he says, who taught you your teachers and he says

114
00:11:03,360 --> 00:11:08,320
your teachers have to look through, and so the reason why the teachers might have been

115
00:11:08,320 --> 00:11:14,320
teaching it may have just been, because you see, if you sound like you know what you're

116
00:11:14,320 --> 00:11:20,440
doing, then if you set up all these complex rituals, people think, oh, well, this person

117
00:11:20,440 --> 00:11:25,200
we need him because he leads us in these very important rituals, and only he knows the

118
00:11:25,200 --> 00:11:30,520
right rituals when, in fact, they just make them up, and so I think that's what you get,

119
00:11:30,520 --> 00:11:37,200
I'm pretty sure that's what you get in a lot of the rituals at the time of the Buddha,

120
00:11:37,200 --> 00:11:42,960
and even after the Buddha, but definitely very much before the Buddha came around, like

121
00:11:42,960 --> 00:11:48,920
we've studied these in university when I was taking Indian religion many years ago, and

122
00:11:48,920 --> 00:11:55,120
some of the rituals are just silly, like they take, it seems like, originally they were

123
00:11:55,120 --> 00:12:01,640
horrific, these horrific sacrifices, a lot of killing, and so they would take a goat up to

124
00:12:01,640 --> 00:12:06,720
the altar and cut its head off, and there was something about burying a live turtle under

125
00:12:06,720 --> 00:12:11,920
the altar, and just burying it alive and killing it, suffocating it, under the altar.

126
00:12:11,920 --> 00:12:18,040
I mean, for no reason, I mean, why would you bury a live turtle under a stone altar?

127
00:12:18,040 --> 00:12:22,920
But it was an important part of building the altar, and then there was butter, had to be

128
00:12:22,920 --> 00:12:29,800
poured into the fire and that kind of thing, and it evolved, so that eventually, probably

129
00:12:29,800 --> 00:12:35,960
with the advent of Buddhism, Jainism, and a lot of the movements that were anti-torture,

130
00:12:35,960 --> 00:12:38,000
anti-crullity.

131
00:12:38,000 --> 00:12:43,080
Eventually, by the time we came to the form that we studied today, what they do is they

132
00:12:43,080 --> 00:12:48,160
bring a goat to the altar, and they have to show the altar to the goat, like the goat has

133
00:12:48,160 --> 00:12:53,600
to see the altar, and then they take the goat away, there's a sacrificial pole, there's

134
00:12:53,600 --> 00:12:58,360
a special wooden pole that has to be there as well, and they have to bring the goat and

135
00:12:58,360 --> 00:13:00,680
show the goat that pole.

136
00:13:00,680 --> 00:13:05,360
So as long as the goat sees the pole, they've done enough, and they take the goat away,

137
00:13:05,360 --> 00:13:10,320
and you can imagine some guys saying, oh, well, we can't kill them anymore, let's just tell

138
00:13:10,320 --> 00:13:13,360
everyone, oh, it's enough that the goat sees the pole, I mean, it's something that they

139
00:13:13,360 --> 00:13:18,760
just come up with, and say, oh, no, no, people say, well, we don't really want to kill

140
00:13:18,760 --> 00:13:23,680
the goat, oh, well, it's okay, as long as he sees the post, somehow, that's meaning.

141
00:13:23,680 --> 00:13:29,360
There's no longer about killing, so I mean, good on them that they're ridiculous sacrifices

142
00:13:29,360 --> 00:13:34,560
have become innocent, that's one step in their interaction.

143
00:13:34,560 --> 00:13:38,600
The other thing is they, instead of burying a turtle under the altar, they take a lump

144
00:13:38,600 --> 00:13:44,680
of butter, and they bury it under the altar, it's a substitute for a live turtle, it's

145
00:13:44,680 --> 00:13:52,760
a good thing for all the poor little turtles, but definitely, innocent doesn't mean beneficial

146
00:13:52,760 --> 00:13:59,840
worthwhile, and in this guy's case, he's got real problems with his killing, every month

147
00:13:59,840 --> 00:14:04,360
killing a living being and feeding it to the fire like it was just butter, he was feeding

148
00:14:04,360 --> 00:14:09,080
the fire, then he could say, well, that's kind of innocent, but even still, it's not the

149
00:14:09,080 --> 00:14:13,800
way to the Brahma world, it doesn't have any connection with being reborn as a brahma,

150
00:14:13,800 --> 00:14:22,960
as a god, so take some to see the Buddha, and the Buddha, he tells the Buddha what he

151
00:14:22,960 --> 00:14:29,720
does, and the Buddha says, you can do that for a hundred years, every month, and it wouldn't

152
00:14:29,720 --> 00:14:37,920
be worth this thousandth part, here to your word, I just pay a respect to an enlightened

153
00:14:37,920 --> 00:14:49,200
being for one moment, so again talking about homage, and the Buddha specifies my students,

154
00:14:49,200 --> 00:14:55,840
so it is a bold claim, and as I said last time, it seems like a bit of a bias claim

155
00:14:55,840 --> 00:14:59,880
in first blush, because anyone could say that, well, of course, everyone's going to say

156
00:14:59,880 --> 00:15:05,440
their own students, but it has to be true at some point, you know, if your students are

157
00:15:05,440 --> 00:15:11,240
enlightened, and if the, if your people really are, if you're talking about a group of people

158
00:15:11,240 --> 00:15:16,360
who really are enlightened, and the Buddha wasn't afraid of making these bold and sort

159
00:15:16,360 --> 00:15:23,840
of bragging claims, most of all claims, because if you look at it another way, of course,

160
00:15:23,840 --> 00:15:29,880
it's leaving you wide open to attack, and the Buddha fully welcomed what he's saying, he's

161
00:15:29,880 --> 00:15:35,160
really, it's called the lion's roar, he was really putting it out there, he's making

162
00:15:35,160 --> 00:15:41,520
a claim, a boast, and challenge, it's a challenge, you know, prove me wrong, that's basically

163
00:15:41,520 --> 00:15:50,160
saying, it's claiming something very, very boldly, and that's really what the purpose

164
00:15:50,160 --> 00:15:55,880
in the meaning there is, that's not the Buddha bragging or forget gain of any sort, there's

165
00:15:55,880 --> 00:16:00,760
no sense that the Buddha even had any need, even if you don't follow Buddhism, there was

166
00:16:00,760 --> 00:16:13,040
no real evidence to support that, and he was well taken care of and well supported.

167
00:16:13,040 --> 00:16:19,640
But here is what he's doing is instead of making just this bold claim that could then

168
00:16:19,640 --> 00:16:24,320
be challenged, and of course, the Brahman would have to be impressed by that, because he

169
00:16:24,320 --> 00:16:27,840
couldn't challenge it, and looking at the Buddha's monks, he would have to agree that,

170
00:16:27,840 --> 00:16:34,160
well yes indeed, there's something special about many of these monks who have developed

171
00:16:34,160 --> 00:16:38,800
themselves, and so that's what the verse actually says, Bobby that time, to one, if you

172
00:16:38,800 --> 00:16:45,520
pay homage to one, or to those who are of developed self.

173
00:16:45,520 --> 00:16:49,720
So again, not to get too much into it, I think the more important aspect of this that

174
00:16:49,720 --> 00:16:54,920
differentiates it from the last one is talking about sacrifice, which I've done, but

175
00:16:54,920 --> 00:17:02,200
I think we can extrapolate that to talk about religious practices in general, it's not

176
00:17:02,200 --> 00:17:06,840
enough that we have a religious practice, and I think there's a lot of this in the world,

177
00:17:06,840 --> 00:17:10,200
just because something is a spiritual practice or a religious practice doesn't make

178
00:17:10,200 --> 00:17:17,480
it really all that valuable, and certainly an understanding that different spiritual practices

179
00:17:17,480 --> 00:17:23,400
have different values, so it's not to say that either you're practicing to become enlightened

180
00:17:23,400 --> 00:17:28,800
or you're doing useless things, there are certain religious practices that are valuable,

181
00:17:28,800 --> 00:17:35,480
but just not as valuable, so feeding of sacrificial fires pretty useless, but practicing

182
00:17:35,480 --> 00:17:40,560
loving kindness, for example, is quite valuable, practicing charity, giving money to the

183
00:17:40,560 --> 00:17:48,600
poor is valuable, it's less valuable than, well, for example, paying homage to one who deserves

184
00:17:48,600 --> 00:17:56,160
it, but even paying homage to one who is worthy of homage, is far less valuable, and

185
00:17:56,160 --> 00:18:02,520
then actually becoming worthy of respect yourself, my teacher said that he said, going

186
00:18:02,520 --> 00:18:09,400
to see an enlightened being, or paying homage to an enlightened being is not worth the

187
00:18:09,400 --> 00:18:14,320
smallest part of becoming, practicing becoming enlightened being yourself, it's one of,

188
00:18:14,320 --> 00:18:24,680
it's in a very famous talk that he gave on the force of Deepatana, but yeah, he made that

189
00:18:24,680 --> 00:18:31,360
very important statement, but yeah, it's good, but it's not worth the smallest part, it's

190
00:18:31,360 --> 00:18:40,880
far less good than actually practicing to become one yourself, so it's a matter of degrees,

191
00:18:40,880 --> 00:18:45,840
and that's something for us to keep in mind in our spiritual practice that some people

192
00:18:45,840 --> 00:18:50,320
will put a lot of emphasis on chanting, some people put a lot of emphasis on charity

193
00:18:50,320 --> 00:18:56,080
or social work, and it's all about our priorities, some people put a lot of emphasis

194
00:18:56,080 --> 00:19:01,840
on study, and in the end we have to ask ourselves, what is our goal, and we have to do

195
00:19:01,840 --> 00:19:08,240
those things to engage, put primer in, this is on those things that lead us to that goal,

196
00:19:08,240 --> 00:19:12,760
and for just giving and giving and giving, what is the purpose of that, if we're just

197
00:19:12,760 --> 00:19:19,520
chanting and chanting or this or that, what is the purpose of all these things, and

198
00:19:19,520 --> 00:19:27,560
on the other hand, if we do have a set purpose, and we do things for that purpose, we

199
00:19:27,560 --> 00:19:33,680
have to differentiate between those things that actually lead to the purpose, but then there

200
00:19:33,680 --> 00:19:38,720
are wide array of things that can help us, that can actually be a benefit to help us realize

201
00:19:38,720 --> 00:19:45,960
our goals, like charity can be useful for meditation, morality of course is essential for meditation,

202
00:19:45,960 --> 00:19:52,920
these kind of things study is also quite important, but putting them in context, so many

203
00:19:52,920 --> 00:20:00,320
religious practices have to be taken as a support rather than the main goal, or main practice,

204
00:20:00,320 --> 00:20:08,040
main focus, anyway, things to consider, it's just some of the ideas that arise based on

205
00:20:08,040 --> 00:20:14,720
this verse, and the importance of quality rather than quantity, you can do a lot, a lot

206
00:20:14,720 --> 00:20:21,160
of deeds that doesn't make any of them good or worthwhile, it can work for 100 years, work

207
00:20:21,160 --> 00:20:28,000
very, very hard, and have nothing to show for, or meaningless things to show for, as you

208
00:20:28,000 --> 00:20:34,680
can just for one moment do the right thing, and have it worse, far more than those hundred

209
00:20:34,680 --> 00:20:39,760
years, that's basically what's been said in a very useful teaching, so that's the

210
00:20:39,760 --> 00:20:51,800
demo factor for tonight, thank you for tuning in, we'll show you all the best, so that's

211
00:20:51,800 --> 00:21:05,720
that, just give me a second, I can have it, because it has to convert, so, just a second,

212
00:21:05,720 --> 00:21:29,480
here, I've lost it, that's my production crew, so, so,

213
00:21:29,480 --> 00:21:44,520
on to the demo portion, the general demo portion of the night, does anyone have any questions?

214
00:21:44,520 --> 00:21:59,480
There are questions, dear Bande, there is only one second, sorry, just give me some things

215
00:21:59,480 --> 00:22:29,320
that doesn't, and now, one more thing, there's that one, oh seven, right?

216
00:22:29,320 --> 00:22:43,960
Okay, there's only one demo group near where I live, I've meditated with the group maybe

217
00:22:43,960 --> 00:22:48,240
five times now, I like the people there, but I'm not sure going there has been good

218
00:22:48,240 --> 00:22:53,400
for my practice, my original hope was that there would be a few people on the similar

219
00:22:53,400 --> 00:22:58,680
path as I've been set on by your videos and Mahasi books, I don't want to complain or

220
00:22:58,680 --> 00:23:03,200
anything, but unfortunately, it's become obvious that all the people in the group either

221
00:23:03,200 --> 00:23:08,440
have only mindfulness-based stress reduction goal or just the simple goal of cultivating

222
00:23:08,440 --> 00:23:13,040
calmness, obviously I'm not saying there's anything wrong with all those things, but

223
00:23:13,040 --> 00:23:17,760
I think the practice can go so much deeper than that as you often talk about.

224
00:23:17,760 --> 00:23:21,480
I feel like the few times I've opened up a little bit about my understanding of in-site

225
00:23:21,480 --> 00:23:27,160
meditation, which is the understanding that I've cultivated from your videos, everybody

226
00:23:27,160 --> 00:23:33,040
has found the things I say really intimidating and contrary to what they would like to think

227
00:23:33,040 --> 00:23:37,760
the practice is about, but as I said, I really like the people and kind of get intoxicated

228
00:23:37,760 --> 00:23:42,760
by their company, which in a way pulls me away from the practice, should I keep going

229
00:23:42,760 --> 00:23:43,760
there?

230
00:23:43,760 --> 00:23:48,280
And the next time I go, what would be the best things to say, talk about to the group,

231
00:23:48,280 --> 00:23:50,440
or should I just stay quiet?

232
00:23:50,440 --> 00:23:55,160
Sorry Robin for making you read such a long question, best wishes.

233
00:23:55,160 --> 00:23:59,560
It's a good question though.

234
00:23:59,560 --> 00:24:06,680
I mean, this from the sounds of it, I think you may be a little bit overreacting, it's

235
00:24:06,680 --> 00:24:08,400
just impersonal.

236
00:24:08,400 --> 00:24:13,280
I don't actually know, because at least on what you're saying, it could be overreacting,

237
00:24:13,280 --> 00:24:17,880
it could be underreacting, you know, they could actually be lynching you every time you

238
00:24:17,880 --> 00:24:24,400
say it, but you're just playing it down, but most likely, I mean, in most cases we get

239
00:24:24,400 --> 00:24:32,280
too gung-ho and maybe push a little too far and talk a little bit, too much stress on

240
00:24:32,280 --> 00:24:39,480
things that are scary like Nirvana and non-attachment and that kind of thing.

241
00:24:39,480 --> 00:24:44,720
But you don't really have to do, because really that it's all about reducing stress.

242
00:24:44,720 --> 00:24:50,000
And it's just a corollary that if you reduce it enough, it eventually all goes away.

243
00:24:50,000 --> 00:25:02,920
You see, I mean, it's all about how you approach it and how you present it.

244
00:25:02,920 --> 00:25:13,280
Now it doesn't seem to be a valid problem if people are just interested in reducing stress.

245
00:25:13,280 --> 00:25:21,760
All you have to do is be clear about how deep that goes and help people to see that, well,

246
00:25:21,760 --> 00:25:26,240
you know, if you're goal is to do stress, well, look at that stress that you still have.

247
00:25:26,240 --> 00:25:29,440
It's a sign that you're really not going quite deep enough.

248
00:25:29,440 --> 00:25:35,840
So, hey, I want to maybe this practice will help you go deeper, but even still, it's,

249
00:25:35,840 --> 00:25:36,840
you don't really do that.

250
00:25:36,840 --> 00:25:41,120
I mean, I never really would point out to someone, hey, look, your practice is inadequate.

251
00:25:41,120 --> 00:25:46,320
It would more be like if they come to you and say, you know, I'm not sure why, but my practice isn't,

252
00:25:46,320 --> 00:25:50,960
you know, I'm not able to get deeper in my practice, well, then I would say, well, maybe

253
00:25:50,960 --> 00:25:54,000
you could try this practice, maybe it works better for you.

254
00:25:54,000 --> 00:25:56,480
It would only be if they approach you.

255
00:25:56,480 --> 00:26:00,560
You don't really try to point out other people's faults and flaws.

256
00:26:00,560 --> 00:26:05,840
Now, what might become a problem is if people are only superficially interested in the dhamma,

257
00:26:05,840 --> 00:26:08,800
that will show itself in other ways.

258
00:26:08,800 --> 00:26:15,200
Like, I've heard at some of these meditation groups, not MBSR, but other groups that are similar.

259
00:26:15,200 --> 00:26:21,200
They'll do a meditation day and then afterwards they'll have alcohol, they'll sit around and drink.

260
00:26:22,560 --> 00:26:26,320
That's a real note, no, that's absolutely stay away from that group.

261
00:26:28,640 --> 00:26:33,680
I mean, maybe not stay away, but I've put some serious caution and yes, probably stay away.

262
00:26:33,680 --> 00:26:39,680
That will certainly not go out with them after the meditation, but at the very least,

263
00:26:40,400 --> 00:26:48,320
make your, make it known that you disapprove and that's not, not in line with meditative goals.

264
00:26:50,560 --> 00:26:55,440
But they may be frivolous, they may sit around chatting about frivolous things and that is a problem.

265
00:26:56,400 --> 00:27:01,280
So that's what I would focus on more and if you can focus on those things and just say,

266
00:27:01,280 --> 00:27:11,440
you know, there's, I think it's important that, or even not even go so far, but just bring up

267
00:27:11,440 --> 00:27:18,080
these teachings. Hey, could we, do you guys want to study the Buddha's teaching on morality and on ethics

268
00:27:18,080 --> 00:27:24,880
and that kind of thing? And that, I think it's enough to push people to be more serious about

269
00:27:24,880 --> 00:27:28,320
their practice once they see the importance of morality, the importance of

270
00:27:28,320 --> 00:27:37,200
non-provolity or how do you say it, frivolousness. Once they're forced to do that, they'll say,

271
00:27:37,200 --> 00:27:41,680
oh, well, then I guess I have to step up my game if the Buddha required more than just us sitting

272
00:27:41,680 --> 00:27:48,880
around all day because it requires you to be mindful, mindful of your desires and your

273
00:27:48,880 --> 00:27:55,120
aversion and your delusions. So I don't know, I mean, that's a little bit, that's some of my

274
00:27:55,120 --> 00:27:59,840
thoughts on it, just to give you maybe some insight, maybe that help.

275
00:28:05,680 --> 00:28:08,880
But the other thing is, don't be too critical. We often are very, very critical of,

276
00:28:09,600 --> 00:28:17,360
we're very easy to get critical of people who are not as into what we're doing or maybe even more

277
00:28:17,360 --> 00:28:25,120
not as under, who don't have as much understanding as us. It's easy at least in the beginning

278
00:28:25,120 --> 00:28:34,880
to look down upon people, not look down exactly, but become overly parental.

279
00:28:36,320 --> 00:28:39,120
You know, what the right word is, can't think of it, but something like that,

280
00:28:40,320 --> 00:28:45,840
where we want, where we feel like we're in a position, condescending it becomes, even though we don't

281
00:28:45,840 --> 00:28:47,840
realize we become quite condescending.

282
00:28:59,120 --> 00:29:03,440
It was just a little bit of a follow-up to that. I don't want to sound like I'm better than

283
00:29:03,440 --> 00:29:07,920
the other people in the group. I just think my approach to the practice is better due to the effort

284
00:29:07,920 --> 00:29:12,880
I've been putting in to try to understand your books and videos and a few cyanobooks and trying

285
00:29:12,880 --> 00:29:18,400
to practice according to those teachings. I think you've covered that. Thank you, Monte.

286
00:29:21,520 --> 00:29:26,320
Monte, for the last few months, I've been attending a teravada meditation center, but because of my

287
00:29:26,320 --> 00:29:32,160
job, I have moved to an area where the only Buddhist temple meditation center is a Vajrayana

288
00:29:32,160 --> 00:29:38,160
center, but they have full-time teachers there. Would it be better to stick to self-study

289
00:29:38,160 --> 00:29:44,480
with teravada, which I prefer to Tibetan Buddhism, or would it be useful to utilize the meditation

290
00:29:44,480 --> 00:30:14,320
teachers available in my area, despite being from a different school of Buddhism. Thank you.

291
00:30:14,480 --> 00:30:31,040
We're having technical difficulties.

292
00:32:14,480 --> 00:32:35,360
I can join using my phone, my tablet. Welcome back, Hunter. The computer is doing funny things.

293
00:32:35,360 --> 00:32:46,880
Oh, it says a start, John, is running for a file system. I thought you were sitting there really,

294
00:32:46,880 --> 00:32:56,000
really quietly. You're reading that extra thing. I was reading away and reading away.

295
00:32:56,000 --> 00:33:06,000
I thought, oh, I'm just sitting there really still. Yes, but the last question, it was just a

296
00:33:06,000 --> 00:33:12,640
follow-up comment, but I think it had been covered in your answer there. So here's the new question.

297
00:33:13,360 --> 00:33:17,120
Monte, for the last few months, I've been attending a teravada meditation center,

298
00:33:17,840 --> 00:33:22,480
but because of my job, I have moved to an area where the only Buddhist temple meditation center

299
00:33:22,480 --> 00:33:28,400
is a Vajrayana center, but they have full-time teachers there. Would it be better to self-study

300
00:33:28,400 --> 00:33:34,480
with teravada, which I prefer to prefer to Tibetan Buddhism anyway, or would it be useful to utilize

301
00:33:34,480 --> 00:33:40,320
the meditation teachers available in my area, despite being from a different school of Buddhism. Thanks.

302
00:33:41,920 --> 00:33:45,840
That's not going to be very useful if you're practicing a different technique. If you go to a

303
00:33:45,840 --> 00:33:53,360
certain teacher, though, it does depend on the teachers. If they're not well-versed in meditation themselves,

304
00:33:54,880 --> 00:34:00,400
they might be open to you practicing your own way. On the other hand, they might have

305
00:34:00,400 --> 00:34:06,000
or knows they might have some insight into this type of meditation. Depends on the teachers,

306
00:34:06,880 --> 00:34:12,160
but if they are teaching a different technique, it's not really going to be all that useful to

307
00:34:12,160 --> 00:34:22,560
except as a place to go. I mean, Tibetan Buddhism can be, is varied, depends on the group,

308
00:34:22,560 --> 00:34:30,800
but it can be really specific and esoteric. They say esoteric. It just means they have really

309
00:34:30,800 --> 00:34:40,320
specific rituals and practices that are not really online with the sort of things where the

310
00:34:40,320 --> 00:34:52,400
direction we're going. It seems to be a common theme tonight. A lot of Tibetan centers and centers,

311
00:34:52,400 --> 00:35:00,800
I guess, Tibetan is more prominent. There's a lot of them out there. What you should do is

312
00:35:00,800 --> 00:35:08,000
start your own teravada group. You can use meetup.com, start your own group, and you'd be surprised

313
00:35:08,000 --> 00:35:17,840
at how many people you could get to join. Is it considered a good deed to care for the environment?

314
00:35:17,840 --> 00:35:47,680
I mean, it's a bit of a round about deed. It's not really wrong.

