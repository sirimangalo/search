1
00:00:00,000 --> 00:00:05,000
Hi, next question comes from Jillian Nep.

2
00:00:05,000 --> 00:00:12,000
I'm a beginner to Buddhism and I am in fact still doing my initial research on it.

3
00:00:12,000 --> 00:00:16,000
I would like to get a balanced view of the negative aspects of Buddhism.

4
00:00:16,000 --> 00:00:24,000
What are some of the things that have derailed both laypersons and monastics?

5
00:00:24,000 --> 00:00:30,000
Yeah, it's not often I get asked what are the negative aspects of my religion.

6
00:00:30,000 --> 00:00:33,000
I don't think there are any honestly.

7
00:00:33,000 --> 00:00:44,000
Although I suppose I've gone over some of the apparent negative aspects of Buddhism in the sense of not being much fun at parties,

8
00:00:44,000 --> 00:00:53,000
not being able to get along in society, having to segregate yourself, having to be careful.

9
00:00:53,000 --> 00:00:59,000
There's a lot of difficulty that comes from trying to be pure.

10
00:00:59,000 --> 00:01:11,000
It's not easy to purify your mind and to be solely a good person and to do away with all evil in the mind, which is really what we're trying to do.

11
00:01:11,000 --> 00:01:19,000
There are many people who would say that's impossible, mostly because they've never practiced meditation.

12
00:01:19,000 --> 00:01:26,000
So they would consider that to be a negative aspect of Buddhism.

13
00:01:26,000 --> 00:01:33,000
But I can't think of any, obviously I don't think there are any negative aspects to Buddhism.

14
00:01:33,000 --> 00:01:41,000
You're kind of asking two questions. The other one is, or I don't know what you're aiming at,

15
00:01:41,000 --> 00:01:48,000
but as far as what stops people from practicing Buddhism correctly, I guess that's the answer.

16
00:01:48,000 --> 00:01:55,000
There's nothing wrong with Buddhism, but people just don't understand that they don't practice it correctly.

17
00:01:55,000 --> 00:02:05,000
What stops people from practicing correctly, obviously our preconceived notions of what is right and what is wrong,

18
00:02:05,000 --> 00:02:09,000
which are not really based on truth and reality.

19
00:02:09,000 --> 00:02:28,000
What stops people from making progress, according to the Buddha, there are certain things that get in the way and prevent a person's progress as a lay person and as a monk.

20
00:02:28,000 --> 00:02:34,000
So maybe I'm not sure if this is exactly what you're asking, but I'll take the opportunity to talk about some of these things.

21
00:02:34,000 --> 00:02:45,000
For lay people, the first one is there's a requirement for faith if people don't have a faith and confidence in what they're doing.

22
00:02:45,000 --> 00:02:55,000
It makes it very difficult for them to continue because obviously for most or for many lay people there's not the opportunity to realize all of the Buddha's teaching,

23
00:02:55,000 --> 00:02:57,000
even to study all of the Buddha's teachings.

24
00:02:57,000 --> 00:03:05,000
So there has to be some level of confidence in your leaders, in the religion.

25
00:03:05,000 --> 00:03:13,000
If you don't believe in the principles of Buddhism, it can be very difficult for you to progress.

26
00:03:13,000 --> 00:03:24,000
And I guess what that means really is if you have ideas that are contrary, then you won't even open your mind to the idea that the Buddha's teaching might be right.

27
00:03:24,000 --> 00:03:40,000
And I recognize this in many so-called Buddhists, they still believe in a delicious meal and preference for things in beauty, in sexuality and so on.

28
00:03:40,000 --> 00:03:45,000
Not only are they taking it, but they also believe that it's right to be attached to these things.

29
00:03:45,000 --> 00:03:54,000
And these are people who consider themselves to be Buddhists even from birth, and that can be a real hindrance in the practice.

30
00:03:54,000 --> 00:03:56,000
The second one is morality.

31
00:03:56,000 --> 00:04:02,000
Obviously if you're an immoral person, it's very difficult to practice the Buddha's teaching.

32
00:04:02,000 --> 00:04:14,000
The third one is a belief in superstition, and this is common throughout the Buddhist world and the world in its entirety.

33
00:04:14,000 --> 00:04:26,000
Because the Buddha taught cause and effect, when people have different ideas of what is cause and what is effect in terms of magical causes, if you wear this around your neck, somehow it's going to have a benefit.

34
00:04:26,000 --> 00:04:36,000
If you say this or say that, if it's somehow there's going to be a negative effect and so on, the belief in superstition.

35
00:04:36,000 --> 00:04:48,000
Because it's contrary to the idea of karma and cause and effect is another hindrance to, especially to lay people because they're not so close to the teaching.

36
00:04:48,000 --> 00:05:00,000
So they hear about this, which doctor or a fortune teller or so on, and they can get mixed up in terms of what is the Buddha's teaching.

37
00:05:00,000 --> 00:05:09,000
The fourth one is getting involved in other religions, and it's related to the last one.

38
00:05:09,000 --> 00:05:24,000
When people go and support other religions, other religious doctrines in terms of paying respect to these teachers, listening to what they have to say, and trying to get their opinion.

39
00:05:24,000 --> 00:05:39,000
They have a whole other outlook on life, and it confuses people and people say things like, all religions teach the same thing or so on, all religions are the same.

40
00:05:39,000 --> 00:05:45,000
It doesn't matter what religion you are, and so they will respect the teachings of all religions.

41
00:05:45,000 --> 00:05:53,000
It's not that we don't, in the sense, respect these teachings in terms of, you want to practice that way, that's fine, you believe this, that's fine.

42
00:05:53,000 --> 00:05:59,000
But to accept that that belief is proper, that it actually is beneficial.

43
00:05:59,000 --> 00:06:09,000
When those beliefs go against, the understanding of the Buddha is to what is right and what is wrong can be hindrance to one's practice.

44
00:06:09,000 --> 00:06:18,000
Because if there is a conflict, and the final one is to support the Buddha's teaching.

45
00:06:18,000 --> 00:06:41,000
So to not spend time supporting teachings which are contrary to the Buddha's teaching, and to actually engage in supporting the Buddhist religion in terms of material support, in terms of spiritual support, and spreading the teachings, and so on.

46
00:06:41,000 --> 00:06:53,000
When we fail to do that, and when we get involved in other religions, these are two things the Buddha said, cause you to move away from the Buddha's teaching.

47
00:06:53,000 --> 00:07:09,000
Those things which are hindrance or cause monks to get to become derailed, as you say, are the Buddha had four things that he said, especially for new monks, are going to be a real hindrance.

48
00:07:09,000 --> 00:07:14,000
This is sort of an addition to what I was saying about how to become a monk earlier.

49
00:07:14,000 --> 00:07:18,000
I think it's useful to know about these as well.

50
00:07:18,000 --> 00:07:32,000
The first one is not being able to stand the teachings or instruction, not being able to bear or being instructed, being told what to do.

51
00:07:32,000 --> 00:07:42,000
This is very common for new monks. When they're told they have to do this and have to do that, it's very easy for them to become angry and frustrated.

52
00:07:42,000 --> 00:07:55,000
When they don't understand and don't agree, they aren't able to follow along because of their own ideas, their own views of what is right and what is wrong.

53
00:07:55,000 --> 00:08:03,000
It makes it very difficult for them to do things like walking back and forth or sitting for long periods of time.

54
00:08:03,000 --> 00:08:08,000
When we tell meditators they have to sit through the pain instead of trying to move around all the time.

55
00:08:08,000 --> 00:08:17,000
This can be very difficult for some people. They don't agree with it. When we tell people they have to let go, they can't cling and they can't chase after.

56
00:08:17,000 --> 00:08:35,000
When we tell them not to make eye contact or look around or wonder or move quickly or so on, it can be very difficult for people, especially for monks and for meditators who have come to the meditation center to stay.

57
00:08:35,000 --> 00:08:53,000
The second one is being addicted to, addicted to simple, being lazy, I guess, is a good explanation of it.

58
00:08:53,000 --> 00:09:20,000
Meditation in the Buddha's teaching does take a lot of effort. It's something you have to work hard at because we're trying to change the core of how we look at the world, of how we see things.

59
00:09:20,000 --> 00:09:25,000
We're trying to change the way we look entirely, change the way we look at things.

60
00:09:25,000 --> 00:09:31,000
This takes a lot of effort because we're generally so off track.

61
00:09:31,000 --> 00:09:45,000
If we are lazy, if we think we can just sit around and eat and sleep and socialize and think that being a monk or being a meditator is somehow enough.

62
00:09:45,000 --> 00:09:51,000
If you come to stay at the monastery somehow, you're gaining something without even practicing.

63
00:09:51,000 --> 00:10:04,000
This can make it very difficult and it does in the end, lead one to feel like what being a monk or being a meditator is pointless, going home and not gaining any benefit from it.

64
00:10:04,000 --> 00:10:24,000
The third one is being attached to happiness, being attached to central pleasures, so needing good food, needing nice clothing, needing a soft bed and so on, needing to listen to music and watch television.

65
00:10:24,000 --> 00:10:44,000
All of these things that monks and meditators are not allowed to do. When these addictions come up, if they're very strong, they can very easily lead one to become derailed and lose interest in the practice because of your one's addictions, one's attachments.

66
00:10:44,000 --> 00:11:02,000
When these come up, it leads to boredom, it leads to disinterest in reality and meditation, wanting something else, wanting more, and eventually leads one to leave the monks' life or to lead the meditation center.

67
00:11:02,000 --> 00:11:21,000
The fourth one is specifically for monks, they say, is love for a woman, but I think it can easily, obviously, be flipped to talking about a female or even a meditator in general.

68
00:11:21,000 --> 00:11:48,000
The sexuality, obviously, is a very strong attachment, and it's the primary reason why monks disrobe when the woman comes to the monastery and they fall in love and the monk decides that he's had enough and he's found a more desirable or more pleasurable path to follow.

69
00:11:48,000 --> 00:11:57,000
This is the fourth danger. These dangers are something that one has to watch out for, especially this last one, have to be very careful about.

70
00:11:57,000 --> 00:12:10,000
I think it all depends on your appreciation of the Buddhist teaching and your appreciation of the life of the Buddha laid out for us.

71
00:12:10,000 --> 00:12:26,000
If you really believe in what the Buddha taught and if you really see the benefit of what the Buddha taught and if it really does bring you benefit, then I think these dangers are easily overcome.

72
00:12:26,000 --> 00:12:43,000
The way you overcome them is by by seeing the benefit, by understanding that what we're doing is something great, something powerful, something pure and something higher than all of these other desires, these other attachments, these other needs and wants.

73
00:12:43,000 --> 00:12:59,000
A higher than our views and our opinions, that what the Buddha taught is very much real.

74
00:12:59,000 --> 00:13:24,000
There will become derailed, either as lay people or as monks as lay people will feel more and more attracted to helping and supporting, learning, studying and appreciating and following the Buddha's teaching and with monks as well, will be able to overcome all adversity and all of our prior addictions and attachments and wants and needs and so on.

75
00:13:24,000 --> 00:13:36,000
So I hope that comes at least close to answering your question, thanks for giving me a chance to talk about these subjects which I think are very important for Buddhists.

