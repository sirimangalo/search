1
00:00:00,000 --> 00:00:08,000
Are there any differences in the strength of good and bad deeds done at different ages?

2
00:00:08,000 --> 00:00:11,000
I mean, Western people do a lot of bad thinking.

3
00:00:11,000 --> 00:00:22,000
It is good, competition, ego, etc., based on ignorance, slash misguided.

4
00:00:22,000 --> 00:00:30,000
First, later in life, will we, really, the strength of good and bad deeds done at different ages?

5
00:00:30,000 --> 00:00:35,000
Yeah, but he's got something right above that, if he's following on it, let me see.

6
00:00:35,000 --> 00:00:43,000
First, later in life, will we really get a chance to see for ourselves what such technical things, with such technical things?

7
00:00:43,000 --> 00:00:48,000
Because evil should be known, but not necessarily the technicals.

8
00:00:48,000 --> 00:00:51,000
I don't quite get it.

9
00:00:51,000 --> 00:00:57,000
But okay, let's talk about good and bad deeds done at different ages.

10
00:00:57,000 --> 00:01:08,000
I mean, I think the classical question here would be, do children, the bad deeds done by children have the same effect as bad deeds done by adults?

11
00:01:08,000 --> 00:01:10,000
That's a good question.

12
00:01:10,000 --> 00:01:20,000
I would be tempted to say, and it's just me answers, not really from the suit as answer, that children don't.

13
00:01:20,000 --> 00:01:22,000
In any way, it makes sense.

14
00:01:22,000 --> 00:01:25,000
It's the answer that most people would naturally want to give.

15
00:01:25,000 --> 00:01:33,000
That children don't have the same, karmic potential in their acts, because their brains are not fully formed,

16
00:01:33,000 --> 00:01:49,000
so for them to come up with a fully conscious volition to hurt or to steal or so on, is not possible that their intentions are.

17
00:01:49,000 --> 00:01:58,000
The whimsical, the strength of an act is often based on the pre-meditation of it.

18
00:01:58,000 --> 00:02:04,000
If you really, really, really strongly intend to do something, then it has stronger karmic potential,

19
00:02:04,000 --> 00:02:06,000
and it's harder for children to do that.

20
00:02:06,000 --> 00:02:08,000
They just come up with things on whim.

21
00:02:08,000 --> 00:02:14,000
It's harder for a child to plan a murder of seven days in advance, or even a day in advance,

22
00:02:14,000 --> 00:02:19,000
but it's possible for a child to kill another child that could happen.

23
00:02:19,000 --> 00:02:25,000
But they would just do it spontaneously, so I would say in general, there's a good likelihood.

24
00:02:25,000 --> 00:02:33,000
And, you know, society tends to subscribe to this, and history tends to attest to it.

25
00:02:33,000 --> 00:02:43,000
It tends to be a common sense sort of thing where we accept the fact that children are not as not to be held as accountable for their deeds.

26
00:02:43,000 --> 00:02:50,000
And I think the universe will attest to that karma, will attest to that.

27
00:02:50,000 --> 00:03:03,000
Then with the coming of age or the advancing of age, in 10th, intention becomes more prominent in our thinking with the younger age group.

28
00:03:03,000 --> 00:03:09,000
Yeah, that's the point of pre-meditation, holding on to grudges and so on.

29
00:03:09,000 --> 00:03:16,000
So it can become very, very entrenched in the mind.

30
00:03:16,000 --> 00:03:25,000
But kids can do horrible things and can be affected horribly by things.

31
00:03:25,000 --> 00:03:30,000
I guess the problem with kids is, the problem for kids is that they're quite impressionable.

32
00:03:30,000 --> 00:03:33,000
So if a child is, I wonder what you'd think of that.

33
00:03:33,000 --> 00:03:37,000
If a child is raped versus an adult is raped,

34
00:03:37,000 --> 00:03:41,000
which one would it affect stronger?

35
00:03:41,000 --> 00:03:46,000
Because children are so, I don't have means of processing certain things,

36
00:03:46,000 --> 00:03:54,000
so they can be corrupted by acts one might think easier than adults.

37
00:03:54,000 --> 00:03:57,000
Anyway, I don't know, kind of speculate.

38
00:03:57,000 --> 00:04:07,000
And here's an interesting question.

