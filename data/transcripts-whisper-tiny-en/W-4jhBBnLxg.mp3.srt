1
00:00:00,000 --> 00:00:04,840
Hi, today's question comes from XZ mass X

2
00:00:05,920 --> 00:00:09,600
Hello you to the moment my question is based on Sati Bhattanami Bhassana

3
00:00:10,080 --> 00:00:19,240
Do you have any advice on how to observe thoughts correctly particularly on holding the attention on the thought being clearly aware of it without being too weak or too strong?

4
00:00:19,440 --> 00:00:21,440
Thanks

5
00:00:22,920 --> 00:00:26,920
In meditation

6
00:00:26,920 --> 00:00:32,480
Our practice is going to go up and down

7
00:00:33,320 --> 00:00:40,920
Sometimes when we acknowledge something it's going to be clear sometimes when we acknowledge something it's not going to be clear sometimes our

8
00:00:42,040 --> 00:00:44,040
attention is going to be

9
00:00:45,800 --> 00:00:48,440
Soft sometimes our attention is going to be strong

10
00:00:49,800 --> 00:00:51,800
Sometimes weak sometimes strong

11
00:00:51,800 --> 00:00:57,600
The important thing is not to worry about the quality of the

12
00:00:58,320 --> 00:01:02,920
The acknowledgement the important thing is to see how the mind reacts see how the mind works

13
00:01:04,040 --> 00:01:09,440
When it when it makes the acknowledgement so for instance when you're watching the rising and the falling

14
00:01:10,440 --> 00:01:13,600
Or in your in your example when you're watching the thoughts

15
00:01:14,240 --> 00:01:18,920
Sometimes you'll find that you're forcing yourself trying not to think or trying to

16
00:01:18,920 --> 00:01:26,480
Cut off the thought or train the thoughts trying to stop them from going in a certain direction and

17
00:01:28,640 --> 00:01:36,560
It's it's not your job to stop that to stop yourself from trying to stop or stop yourself from trying to force things

18
00:01:36,560 --> 00:01:42,760
It's your job to learn what the nature of that is so probably your question is coming from

19
00:01:42,760 --> 00:01:51,720
Experience in the meditation practice where you experience these states where you find yourself forcing things where you find your

20
00:01:52,520 --> 00:01:54,520
your mind

21
00:01:54,720 --> 00:02:02,720
Fixing heavily or fixing very strongly on the object or other experiences where the mind is weak where the mind is wandering away

22
00:02:03,080 --> 00:02:08,800
The the most important thing is to watch and see what those states are like what happens when the mind is weak

23
00:02:08,800 --> 00:02:10,800
What happens when the mind is strong?

24
00:02:10,800 --> 00:02:14,360
Meditation is a practice of contemplating of observing of

25
00:02:15,120 --> 00:02:24,000
Understanding not of changing or fixing or perfecting anything and the fixing and the perfecting comes through wisdom through

26
00:02:24,600 --> 00:02:32,280
Understanding once you understand once you get it once your mind gets it. Okay. This is how we should approach a

27
00:02:32,280 --> 00:02:42,600
situation. This is how we should approach it and experience your mind will naturally react to things and

28
00:02:45,480 --> 00:02:52,200
Respond to experience in a way that is natural in a way that is harmonious and in a way that is perfectly balanced

29
00:02:52,200 --> 00:02:53,520
so

30
00:02:53,520 --> 00:03:01,400
The balance is not our work our work is to see what happens when we're not when we're not balanced or or what is the result of

31
00:03:01,400 --> 00:03:05,560
The various ways of experiencing and you don't have to experiment experiment

32
00:03:05,560 --> 00:03:10,600
You just watch and and see what's going on in your mind when when my mind acts like this

33
00:03:10,680 --> 00:03:14,520
This is the way I normally respond to things. What's the result and

34
00:03:15,200 --> 00:03:21,840
What you'll see is that most of the way we respond to things is causing a suffering and that's why we suffer in our lives

35
00:03:21,840 --> 00:03:23,840
That's why we

36
00:03:24,480 --> 00:03:27,880
Why we're unsatisfied why we're not content why we're not peaceful

37
00:03:27,880 --> 00:03:33,720
In our lives because we respond to things in a way that brings about stress once we see that

38
00:03:34,400 --> 00:03:39,080
The mind will naturally balance. There's no need to adjust your practice in any way

39
00:03:39,080 --> 00:03:58,280
All you need to do is watch learn and understand. Okay, so hope that helps

