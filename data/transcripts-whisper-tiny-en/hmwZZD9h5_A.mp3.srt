1
00:00:00,000 --> 00:00:28,000
Good evening, everyone.

2
00:00:28,000 --> 00:00:33,000
We'll make sure they're a little early

3
00:00:33,000 --> 00:00:39,000
and to stop my meditation to come online.

4
00:00:39,000 --> 00:00:56,000
No matter.

5
00:00:56,000 --> 00:01:08,000
Good evening, everyone.

6
00:01:08,000 --> 00:01:15,000
No clock.

7
00:01:15,000 --> 00:01:20,000
So tonight's quote is

8
00:01:20,000 --> 00:01:29,000
Again, from the end, good Pernicaia.

9
00:01:29,000 --> 00:01:31,000
You know, the Pernicaia is a good source of

10
00:01:31,000 --> 00:01:35,000
dogma or doctrine.

11
00:01:35,000 --> 00:01:41,000
That's a place where it's the book that you go to if you want to get

12
00:01:41,000 --> 00:01:45,000
doctrine on a topic.

13
00:01:45,000 --> 00:01:52,000
It teaches to refer to when they're giving talks.

14
00:01:52,000 --> 00:01:57,000
Because the end good Pernicaia is mainly lists.

15
00:01:57,000 --> 00:02:03,000
So you've got separated into books.

16
00:02:03,000 --> 00:02:10,000
And this is the...

17
00:02:10,000 --> 00:02:15,000
Why is this the book of eleven, so I don't know.

18
00:02:15,000 --> 00:02:22,000
Some reason I thought it would be the book of sixes.

19
00:02:22,000 --> 00:02:26,000
Let's see, the commodity often has some good insight on to why.

20
00:02:26,000 --> 00:02:39,000
How they do the numbering.

21
00:02:39,000 --> 00:03:06,000
This is actually a part of a bigger set of

22
00:03:06,000 --> 00:03:11,000
souptas, so that's not worrying about the numbering because it seems a little bit

23
00:03:11,000 --> 00:03:16,000
complicated.

24
00:03:16,000 --> 00:03:23,000
So we start by mentioning five.

25
00:03:23,000 --> 00:03:25,000
This is actually the Mahanama.

26
00:03:25,000 --> 00:03:40,000
And so Mahanama comes to see the Buddha and...

27
00:03:40,000 --> 00:03:43,000
The Buddha was going to set out wandering.

28
00:03:43,000 --> 00:03:48,000
Mahanama was the brother of Anurudha.

29
00:03:48,000 --> 00:03:59,000
And if you remember the story of Anurudha, Anurudha, because when the Buddha went back to his home,

30
00:03:59,000 --> 00:04:09,000
many of the young men and even young women went forth and became monks under the Buddha.

31
00:04:09,000 --> 00:04:12,000
And so from every family it seemed that they were going.

32
00:04:12,000 --> 00:04:19,000
And some people complained and said, oh, I said, I said, I said, I said, I said,

33
00:04:19,000 --> 00:04:21,000
what have they called them?

34
00:04:21,000 --> 00:04:25,000
It's taking away our sons, stealing away our sons.

35
00:04:25,000 --> 00:04:29,000
Mostly it was the men in the beginning.

36
00:04:29,000 --> 00:04:34,000
And they told the Buddha this and the Buddha said, oh, tell them that they're...

37
00:04:34,000 --> 00:04:41,000
They're ordaining according to the dharma or into the dharma would have been.

38
00:04:41,000 --> 00:04:45,000
And the monks told the people, they're ordaining according to the dharma.

39
00:04:45,000 --> 00:04:50,000
And people were, oh, because they respected the dharma the way.

40
00:04:50,000 --> 00:04:52,000
Dharma is a...

41
00:04:52,000 --> 00:04:54,000
It took on a lot of meaning this word.

42
00:04:54,000 --> 00:04:57,000
Has laden with much meaning originally.

43
00:04:57,000 --> 00:05:00,000
It just meant what people held.

44
00:05:00,000 --> 00:05:06,000
It became that which is held to be true kind of thing.

45
00:05:06,000 --> 00:05:10,000
So it came to be the right.

46
00:05:10,000 --> 00:05:15,000
Anyway, from Mahanama and Anaruda, no one from their family became monks.

47
00:05:15,000 --> 00:05:17,000
And so they were kind of embarrassed.

48
00:05:17,000 --> 00:05:20,000
Mahanama said, one of us has to become a monk.

49
00:05:20,000 --> 00:05:24,000
And Anaruda said, oh, well, becoming a monk sounds difficult.

50
00:05:24,000 --> 00:05:28,000
I'll stay as a lay person, you will become a monk.

51
00:05:28,000 --> 00:05:35,000
And so Mahanama had to explain to Anaruda how to be a lay person.

52
00:05:35,000 --> 00:05:38,000
We have estates that you have to care for.

53
00:05:38,000 --> 00:05:42,000
So you have to go and oversee the planting of the grain,

54
00:05:42,000 --> 00:05:46,000
the caring for the field, the tilling of the fields, the planting of the grain,

55
00:05:46,000 --> 00:05:50,000
the caring of the fields, the gardening of the grain from the animals.

56
00:05:50,000 --> 00:05:57,000
You have to oversee taking it to the mill, grinding the grain and all this.

57
00:05:57,000 --> 00:06:02,000
All the affairs having to do with the farm.

58
00:06:02,000 --> 00:06:06,000
And then you have to give wages to all the workers.

59
00:06:06,000 --> 00:06:11,000
And you have to oversee and make sure that they're doing their jobs.

60
00:06:11,000 --> 00:06:14,000
And then you have to look after the money.

61
00:06:14,000 --> 00:06:16,000
And he went on and on and on.

62
00:06:16,000 --> 00:06:21,000
And Anaruda's eye has gotten wider and wider until finally he's stopped.

63
00:06:21,000 --> 00:06:22,000
I'll become a monk.

64
00:06:22,000 --> 00:06:24,000
He has to be as a lay person.

65
00:06:24,000 --> 00:06:28,000
And so Anaruda became a monk.

66
00:06:28,000 --> 00:06:30,000
Mahanama stayed as a layman.

67
00:06:30,000 --> 00:06:33,000
And he's a subject.

68
00:06:33,000 --> 00:06:37,000
He's actually one of the great laypeople in the Buddha's dispensation.

69
00:06:37,000 --> 00:06:40,000
He often came to the Buddha and asked him questions.

70
00:06:40,000 --> 00:06:43,000
So he found out that the Buddha was going away.

71
00:06:43,000 --> 00:06:50,000
The monks were making a robe for him after the rains.

72
00:06:50,000 --> 00:06:56,000
Because he was going to go wandering to teach.

73
00:06:56,000 --> 00:07:04,000
And some Mahanama heard this and he wanted to get some instruction before the Buddha left.

74
00:07:04,000 --> 00:07:06,000
So he went to the Buddha and he said, is this true?

75
00:07:06,000 --> 00:07:09,000
So I've heard that you're leaving.

76
00:07:09,000 --> 00:07:14,000
With all our various engagements, how should we dwell?

77
00:07:14,000 --> 00:07:17,000
Oh, this is people, Bodhi's translate.

78
00:07:17,000 --> 00:07:23,000
Among the various ways in which we dwell, how should we dwell?

79
00:07:23,000 --> 00:07:35,000
Kainasa, we had Reina, we had the Bhong.

80
00:07:35,000 --> 00:07:40,000
By which means should all of the many means,

81
00:07:40,000 --> 00:07:44,000
the many ways we can dwell, this world we had.

82
00:07:44,000 --> 00:07:46,000
This is a good quote.

83
00:07:46,000 --> 00:07:54,000
We had Reina, we had Reina, we had the Bhong.

84
00:07:54,000 --> 00:07:56,000
This is the word we have.

85
00:07:56,000 --> 00:07:58,000
Harder.

86
00:07:58,000 --> 00:08:01,000
Harder means to carry, we have.

87
00:08:01,000 --> 00:08:05,000
The harm means to take, I think, to take.

88
00:08:05,000 --> 00:08:06,000
We have.

89
00:08:06,000 --> 00:08:10,000
We have means to dwell.

90
00:08:10,000 --> 00:08:17,000
All the many ways we can dwell, how should we dwell?

91
00:08:17,000 --> 00:08:22,000
And the Buddha says sad, who is sad, Mahanama.

92
00:08:22,000 --> 00:08:28,000
It is proper that you should ask the Buddha this question.

93
00:08:28,000 --> 00:08:32,000
And so first, before he tells him how you should dwell,

94
00:08:32,000 --> 00:08:38,000
he offers these five qualities that he's going to require

95
00:08:38,000 --> 00:08:42,000
in order to dwell properly, in order to accomplish the dwelling

96
00:08:42,000 --> 00:08:45,000
that the Buddha is going to give to him.

97
00:08:45,000 --> 00:08:48,000
He needs five qualities first.

98
00:08:48,000 --> 00:08:50,000
Maybe this is where the 11 comes from.

99
00:08:50,000 --> 00:08:53,000
There's the five qualities and the six dwellings,

100
00:08:53,000 --> 00:08:54,000
all together it makes 11.

101
00:08:54,000 --> 00:09:00,000
It's not how we could body numbers it, but we have the five things.

102
00:09:00,000 --> 00:09:02,000
You should make, right?

103
00:09:02,000 --> 00:09:05,000
When you have established these five things in yourself,

104
00:09:05,000 --> 00:09:10,000
you should also make six other things grow within you.

105
00:09:10,000 --> 00:09:15,000
So, one who is serious.

106
00:09:15,000 --> 00:09:21,000
I'm just going to tell the big Buddha who is here.

107
00:09:21,000 --> 00:09:30,000
Sadhgoma, Mahanama, Arada, Kohoti, nah, a sadhgoh.

108
00:09:30,000 --> 00:09:44,000
So, someone who is energetic, someone who is successful, eager,

109
00:09:44,000 --> 00:09:46,000
one is successful.

110
00:09:46,000 --> 00:09:48,000
How is one successful?

111
00:09:48,000 --> 00:09:52,000
First, Sadhgoh, nah, no a sadhgoh.

112
00:09:52,000 --> 00:09:55,000
One needs to have confidence.

113
00:09:55,000 --> 00:09:58,000
They can't be faithless.

114
00:09:58,000 --> 00:10:06,000
The first thing you need is you have to have confidence.

115
00:10:06,000 --> 00:10:09,000
You should have confidence in the Buddha, the Dhamma, the Sangha.

116
00:10:09,000 --> 00:10:14,000
You should have confidence in the practice, confidence in your teacher,

117
00:10:14,000 --> 00:10:16,000
confidence in yourself.

118
00:10:16,000 --> 00:10:21,000
You don't have confidence in all these things you have to remedy it.

119
00:10:21,000 --> 00:10:24,000
Then you need a new teacher.

120
00:10:24,000 --> 00:10:28,000
Maybe you need to study about the Buddha, study the Dhamma, study.

121
00:10:28,000 --> 00:10:34,000
You have to get to know the Sangha, so you have faith in them.

122
00:10:34,000 --> 00:10:39,000
You have to study the practice and find the right practice.

123
00:10:39,000 --> 00:10:43,000
You have to look at yourself and you have to see the good qualities in yourself

124
00:10:43,000 --> 00:10:51,000
and work on the good qualities and focus on good qualities so that you have confidence.

125
00:10:51,000 --> 00:10:56,000
Aranda will you know kusito.

126
00:10:56,000 --> 00:11:00,000
They have, they are accomplished in effort.

127
00:11:00,000 --> 00:11:04,000
Aranda means firm effort.

128
00:11:04,000 --> 00:11:07,000
No kusito, not lazy.

129
00:11:07,000 --> 00:11:10,000
You have to work.

130
00:11:10,000 --> 00:11:12,000
You have to commit yourself.

131
00:11:12,000 --> 00:11:16,000
Doesn't mean you have to run around or you have to push yourself very hard.

132
00:11:16,000 --> 00:11:28,000
It just means you have to have firm effort, strong effort, consistent and sustained effort.

133
00:11:28,000 --> 00:11:37,000
Because you can't have effort at any given moment, but it's easy to fall into laziness,

134
00:11:37,000 --> 00:11:41,000
out of greed or out of anger, out of delusion.

135
00:11:41,000 --> 00:11:46,000
But as soon as you are mindful, then you can have effort again.

136
00:11:46,000 --> 00:11:49,000
So you can rebuild effort every moment you can cultivate it.

137
00:11:49,000 --> 00:11:52,000
This is how people are able to practice day and night.

138
00:11:52,000 --> 00:11:58,000
They are able to find effort in the present moment again and again and again.

139
00:11:58,000 --> 00:12:03,000
They just continue to stay with that.

140
00:12:03,000 --> 00:12:06,000
Number three, up dita sati.

141
00:12:06,000 --> 00:12:13,000
No mutasati, they have established mindfulness or remembrance.

142
00:12:13,000 --> 00:12:16,000
They are not forgetful.

143
00:12:16,000 --> 00:12:23,000
No mutasati, they are done a muddled sati.

144
00:12:23,000 --> 00:12:27,000
Means they are able to see things arising and see things.

145
00:12:27,000 --> 00:12:33,000
And they are able to recognize this, this, this, this, this, this.

146
00:12:33,000 --> 00:12:41,000
They are able to practice body, kaya, vedana, jita, dama.

147
00:12:41,000 --> 00:12:46,000
I'm able to see what is kaya, what is vedana, what is jita, what is dama?

148
00:12:46,000 --> 00:12:50,000
Before sati patana.

149
00:12:50,000 --> 00:12:55,000
Samahito, no as samahito.

150
00:12:55,000 --> 00:13:00,000
They are concentrated or focused, not unfocused.

151
00:13:00,000 --> 00:13:03,000
So again, this is not quite concentrated.

152
00:13:03,000 --> 00:13:06,000
It's more focused like a land.

153
00:13:06,000 --> 00:13:10,000
Seeing things sharpening your vision.

154
00:13:10,000 --> 00:13:14,000
Seeing things as they are.

155
00:13:14,000 --> 00:13:19,000
And number five, panyo, no dupanyo.

156
00:13:19,000 --> 00:13:33,000
They are possessing a wisdom, not of low or base wisdom, base understanding.

157
00:13:33,000 --> 00:13:38,000
So you have to have wisdom.

158
00:13:38,000 --> 00:13:50,000
Wisdom about, wisdom about right view, wisdom about body and mind, three characteristics.

159
00:13:50,000 --> 00:13:52,000
Wisdom about karma.

160
00:13:52,000 --> 00:13:57,000
You have to have wisdom about right and wrong.

161
00:13:57,000 --> 00:14:00,000
You have wisdom about cause and effect.

162
00:14:00,000 --> 00:14:06,000
You have to know these things intellectually first, but then you have to see them through

163
00:14:06,000 --> 00:14:09,000
practice.

164
00:14:09,000 --> 00:14:16,000
So true, punya, true wisdom is the first you hear, then you think, and then you study

165
00:14:16,000 --> 00:14:20,000
and you experience real wisdom.

166
00:14:20,000 --> 00:14:24,000
As you see nama, you see the physical and mental things arising.

167
00:14:24,000 --> 00:14:29,000
You see, it's quite obvious actually, but you start to realize that that's the nature of reality.

168
00:14:29,000 --> 00:14:33,000
Not this room that we are in or this world that we live in.

169
00:14:33,000 --> 00:14:35,000
That's not real.

170
00:14:35,000 --> 00:14:36,000
That's real.

171
00:14:36,000 --> 00:14:39,000
There's experiences of physical and mental phenomena.

172
00:14:39,000 --> 00:14:41,000
You see like that.

173
00:14:41,000 --> 00:14:45,000
And then you see cause and effect.

174
00:14:45,000 --> 00:14:49,000
This causes this, that causes that.

175
00:14:49,000 --> 00:14:53,000
And see when you don't give rise to this, that doesn't arise.

176
00:14:53,000 --> 00:14:58,000
When you give rise to this, that arise.

177
00:14:58,000 --> 00:15:04,000
You see the three characteristics that everything you think was sad as it was stable, satisfying

178
00:15:04,000 --> 00:15:07,000
and controllable, it's not actually.

179
00:15:07,000 --> 00:15:12,000
You start to lose your passion, your desire.

180
00:15:12,000 --> 00:15:14,000
And then you see the path.

181
00:15:14,000 --> 00:15:21,000
You start to see how you can live, how you can exist without falling into suffering,

182
00:15:21,000 --> 00:15:27,000
without cultivating unholesomeness and falling into suffering.

183
00:15:27,000 --> 00:15:29,000
And then you attain the fruit.

184
00:15:29,000 --> 00:15:34,000
And finally you pick the fruit and you taste the fruit of freedom.

185
00:15:34,000 --> 00:15:37,000
That's wisdom.

186
00:15:37,000 --> 00:15:39,000
So those five.

187
00:15:39,000 --> 00:15:47,000
And then he talks about the six recollections that you should keep in mind.

188
00:15:47,000 --> 00:15:49,000
And these are the six Anosati.

189
00:15:49,000 --> 00:15:55,000
If those of you who have studied the Visudimanga with us, we went through these.

190
00:15:55,000 --> 00:15:59,000
There's 10 recollections. The first six are Chinosati.

191
00:15:59,000 --> 00:16:03,000
They're called the six recollections.

192
00:16:03,000 --> 00:16:07,000
So we have the Buddha, the Dhamma, the Sangha.

193
00:16:07,000 --> 00:16:12,000
We have Sila, Dana, Najaga, right?

194
00:16:12,000 --> 00:16:15,000
Sila, Jaga, and Daywah.

195
00:16:15,000 --> 00:16:18,000
Daywada.

196
00:16:18,000 --> 00:16:23,000
So the Buddha, the Dhamma, and the Sangha, we recollect on Buddha.

197
00:16:23,000 --> 00:16:26,000
We think about the Buddha, the T.P. Zobago.

198
00:16:26,000 --> 00:16:29,000
Indeed, he is the blessing one and so on.

199
00:16:29,000 --> 00:16:33,000
So Bhakta, we think of the Dhamma well thought.

200
00:16:33,000 --> 00:16:36,000
There's actually a mantra for that.

201
00:16:36,000 --> 00:16:41,000
So Bhakta, no, we think about the Sangha being well-practiced.

202
00:16:41,000 --> 00:16:44,000
And there's a mantra for all three of those.

203
00:16:44,000 --> 00:16:48,000
The other three are reflecting on our Sila, our morality.

204
00:16:48,000 --> 00:16:50,000
We think of how pure our morality is.

205
00:16:50,000 --> 00:16:54,000
How we're not killing, we're not stealing, we're not cheating, we're not lying.

206
00:16:54,000 --> 00:16:58,000
We're not taking drugs or alcohol.

207
00:16:58,000 --> 00:17:02,000
When we're practicing meditation, our morality is quite pure.

208
00:17:02,000 --> 00:17:04,000
Our behavior is quite pure.

209
00:17:04,000 --> 00:17:06,000
That's why meditation is so wonderful.

210
00:17:06,000 --> 00:17:10,000
So by sitting on a mat and actually doing formal meditations,

211
00:17:10,000 --> 00:17:16,000
very important because it's that moment or that period of purity

212
00:17:16,000 --> 00:17:23,000
where you're not engaged in any impure activity.

213
00:17:23,000 --> 00:17:29,000
And then Jagga, so you reflect on that and you feel happy about that.

214
00:17:29,000 --> 00:17:32,000
Jagga means reflecting on your generosity.

215
00:17:32,000 --> 00:17:37,000
So those of us who, those of you who have joined with us

216
00:17:37,000 --> 00:17:41,000
to give robes to Adjunctana, that's an awesome thing.

217
00:17:41,000 --> 00:17:42,000
That's great.

218
00:17:42,000 --> 00:17:46,000
That kind of thing, it's just an example.

219
00:17:46,000 --> 00:17:48,000
And in good deeds that you do any gift that you've given,

220
00:17:48,000 --> 00:17:51,000
any help that you've given to someone, any.

221
00:17:51,000 --> 00:17:54,000
If you've given the dhamma, maybe you've taught someone how to meditate,

222
00:17:54,000 --> 00:17:58,000
all these things, reflecting on those, remembering them.

223
00:18:06,000 --> 00:18:08,000
And then finally, Daywata.

224
00:18:08,000 --> 00:18:12,000
We were recollect on the angels on heaven.

225
00:18:12,000 --> 00:18:14,000
Again, this is advice to a layman.

226
00:18:14,000 --> 00:18:16,000
So you might wonder why are we thinking about angels?

227
00:18:16,000 --> 00:18:20,000
It's actually thinking about the qualities that exist in us.

228
00:18:20,000 --> 00:18:23,000
And we're joycing about our good qualities that lead us

229
00:18:23,000 --> 00:18:26,000
to good things in the future.

230
00:18:26,000 --> 00:18:30,000
Now, obviously heaven isn't where we're aiming for,

231
00:18:30,000 --> 00:18:35,000
but for a layman, for someone who is sort of, again,

232
00:18:35,000 --> 00:18:42,000
practicing not necessarily to attain very nibana in this life,

233
00:18:42,000 --> 00:18:48,000
but to purify their minds and to see the truth,

234
00:18:48,000 --> 00:18:53,000
there was this person who came to visit a couple of days ago.

235
00:18:53,000 --> 00:19:01,000
And she said to me,

236
00:19:01,000 --> 00:19:03,000
she's married.

237
00:19:03,000 --> 00:19:11,000
She said, oh, I saw your video on how you became a monk.

238
00:19:11,000 --> 00:19:14,000
And I said, oh, yeah, you're interested.

239
00:19:14,000 --> 00:19:17,000
And she said, no.

240
00:19:17,000 --> 00:19:23,000
Which is so good, honest of her.

241
00:19:23,000 --> 00:19:24,000
But that kind of thing.

242
00:19:24,000 --> 00:19:28,000
Not everyone is going to go out of the way.

243
00:19:28,000 --> 00:19:31,000
Another, I've gotten another answer before

244
00:19:31,000 --> 00:19:33,000
when I asked people if they want to become a monk.

245
00:19:33,000 --> 00:19:35,000
No, absolutely.

246
00:19:35,000 --> 00:19:39,000
Someone even said, oh, I hope not.

247
00:19:39,000 --> 00:19:41,000
So maybe one day you'll become a monk.

248
00:19:41,000 --> 00:19:43,000
I said, oh, I hope not.

249
00:19:43,000 --> 00:19:47,000
So not everyone's headed in that direction.

250
00:19:47,000 --> 00:19:49,000
But you know, we're going to heaven is a great thing

251
00:19:49,000 --> 00:19:52,000
because there's lots of Buddhists up in heaven.

252
00:19:52,000 --> 00:19:54,000
And so think about that.

253
00:19:54,000 --> 00:19:58,000
And so the Buddha says when you think about these things,

254
00:19:58,000 --> 00:20:02,000
it, the mind becomes, I'd say,

255
00:20:02,000 --> 00:20:07,000
against inspiration,

256
00:20:07,000 --> 00:20:09,000
against inspiration,

257
00:20:09,000 --> 00:20:13,000
against joy, when his joyful rapture arises

258
00:20:13,000 --> 00:20:15,000
for one with a rapture is the mind.

259
00:20:15,000 --> 00:20:18,000
The body becomes tranquil.

260
00:20:18,000 --> 00:20:22,000
One tranquil and body feels pleasure for one feeling pleasure.

261
00:20:22,000 --> 00:20:24,000
The mind becomes concentrated.

262
00:20:24,000 --> 00:20:27,000
So there's a kind of pleasure that's based on wholesomeness.

263
00:20:27,000 --> 00:20:31,000
Not all pleasure is based on unwholesome.

264
00:20:31,000 --> 00:20:34,000
It's a pleasure that comes from thinking of good things,

265
00:20:34,000 --> 00:20:37,000
rejoicing and good things.

266
00:20:37,000 --> 00:20:39,000
The mind becomes concentrated.

267
00:20:39,000 --> 00:20:42,000
This is called a noble disciple who dwells in balance

268
00:20:42,000 --> 00:20:46,000
amid an unbalanced populace and was unaffected

269
00:20:46,000 --> 00:20:50,000
amid an afflicted populace.

270
00:20:50,000 --> 00:20:55,000
There's one who has entered the stream of the dharma he develops.

271
00:20:55,000 --> 00:21:01,000
A recollection of these things.

272
00:21:01,000 --> 00:21:06,000
So we have dwelling in the world without suffering.

273
00:21:06,000 --> 00:21:09,000
So these are protection.

274
00:21:09,000 --> 00:21:12,000
These six are useful as protection.

275
00:21:12,000 --> 00:21:15,000
The Buddha is not saying that these are what leads you to enlightenment.

276
00:21:15,000 --> 00:21:18,000
He's saying these are things that allow you to have a balanced,

277
00:21:18,000 --> 00:21:23,000
like a state focused in balance and not get lost in some sorrow.

278
00:21:23,000 --> 00:21:27,000
Think about these six things from time to time.

279
00:21:27,000 --> 00:21:33,000
So first, as I preface it with actual core, hard core teachings,

280
00:21:33,000 --> 00:21:46,000
the five faculties, confidence, effort, mindfulness, concentration, and wisdom.

281
00:21:46,000 --> 00:21:51,000
But then before talking about these other things,

282
00:21:51,000 --> 00:21:54,000
if you are already practicing well,

283
00:21:54,000 --> 00:21:55,000
how do you protect it?

284
00:21:55,000 --> 00:21:59,000
You protect it by in these ways by thinking about things like this group of six.

285
00:21:59,000 --> 00:22:03,000
It's the Anu-Santi part.

286
00:22:03,000 --> 00:22:06,000
They're useful meditations.

287
00:22:06,000 --> 00:22:09,000
So we still practice mindfulness as our main meditation,

288
00:22:09,000 --> 00:22:12,000
but from time to time we think about the Buddha, the dharma,

289
00:22:12,000 --> 00:22:14,000
the Sangha, especially living in the world.

290
00:22:14,000 --> 00:22:20,000
It's easy to get lost when you have to go to work and so on.

291
00:22:20,000 --> 00:22:27,000
So these keeps you focused and remind you of what's important.

292
00:22:27,000 --> 00:22:34,000
So that's the dharma for tonight.

293
00:22:34,000 --> 00:22:43,000
Do you have any questions to late?

294
00:22:43,000 --> 00:22:57,000
We have a question from last night, huh?

295
00:22:57,000 --> 00:23:06,000
Why are human beings forced to live when suffering and fertility are the only things we know of?

296
00:23:06,000 --> 00:23:15,000
Well, just as you see, this is the funny thing about humans like we have this idea that there should be a purpose,

297
00:23:15,000 --> 00:23:18,000
or you know, everything happens for a purpose, et cetera, et cetera.

298
00:23:18,000 --> 00:23:24,000
But we also have the idea that there's some rhyme or reason to things.

299
00:23:24,000 --> 00:23:30,000
So your question is kind of weird because you acknowledge that there's no purpose.

300
00:23:30,000 --> 00:23:37,000
But then you assume that there's something forcing us to live like there's something like a God or something,

301
00:23:37,000 --> 00:23:41,000
which Buddhism doesn't ignore.

302
00:23:41,000 --> 00:23:44,000
So what we know is that we live.

303
00:23:44,000 --> 00:23:49,000
There's no answer to why we live ignorance.

304
00:23:49,000 --> 00:24:04,000
It's the root cause.

305
00:24:04,000 --> 00:24:06,000
No questions.

306
00:24:06,000 --> 00:24:29,000
There are no questions I'm going to go.

307
00:24:29,000 --> 00:24:45,000
All right.

308
00:24:45,000 --> 00:24:48,000
Have a good night, everyone.

309
00:24:48,000 --> 00:24:56,000
We'll try to be back tomorrow.

310
00:24:56,000 --> 00:25:19,000
Thank you.

