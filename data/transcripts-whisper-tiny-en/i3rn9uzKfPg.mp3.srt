1
00:00:00,000 --> 00:00:16,000
Can you please talk about equanimity? How can one develop and cultivate equanimity?

2
00:00:16,000 --> 00:00:28,000
We have talked about this before. I think the key to the question is to understand that there are two types of equanimity.

3
00:00:28,000 --> 00:00:38,000
I mentioned, I talked about this one in the video. I was totally unrelated a video about something I can't remember.

4
00:00:38,000 --> 00:00:50,000
But the key to equanimity, as I mentioned there, is to understand that there are two types of equanimity.

5
00:00:50,000 --> 00:00:59,000
There's equanimity as a feeling that arises, and there's equanimity on an intellectual level.

6
00:00:59,000 --> 00:01:06,000
I'm starting to use this word intellectual to explain it because I can't think of a better word,

7
00:01:06,000 --> 00:01:15,000
but the point is not theoretical or based on books, but it's the mental activity that occurs.

8
00:01:15,000 --> 00:01:24,000
On the level of judgments or on the level of mental projections that arise.

9
00:01:24,000 --> 00:01:31,000
So on the level of likes and dislikes, the mind state that avoids liking and disliking,

10
00:01:31,000 --> 00:01:38,000
that can be considered a sort of equanimity. Even though in Buddhism, it's not recognized as equanimity.

11
00:01:38,000 --> 00:01:56,000
It's not recognized as a mind state. It's not given its own designation.

12
00:01:56,000 --> 00:01:59,000
Equanimity is just considered a feeling in the equanimity.

13
00:01:59,000 --> 00:02:11,000
So what really exists is this equanimity. The problem with the feeling is that equanimity can be associated with delusion.

14
00:02:11,000 --> 00:02:18,000
It can even be associated with greed.

15
00:02:18,000 --> 00:02:26,000
And it can be associated with rootless minds.

16
00:02:26,000 --> 00:02:31,000
So you don't actually have to cultivate this sort of equanimity.

17
00:02:31,000 --> 00:02:35,000
The mind has it inherently when you see something.

18
00:02:35,000 --> 00:02:39,000
At the moment of seeing, there's equanimity in that mind.

19
00:02:39,000 --> 00:02:43,000
When the moment of hearing, when you hear something, there's equanimity in that mind.

20
00:02:43,000 --> 00:02:49,000
So this equanimity as a feeling is not something necessarily to be developed.

21
00:02:49,000 --> 00:02:55,000
If you're keen to develop it, then you develop it as a genre.

22
00:02:55,000 --> 00:03:05,000
Equanimity as a pure feeling, where you're simply equanimous.

23
00:03:05,000 --> 00:03:14,000
And you have this feeling of calm and this intense feeling of peace.

24
00:03:14,000 --> 00:03:21,000
But the kind of equanimity that should be developed is a equanimity based on wisdom.

25
00:03:21,000 --> 00:03:39,000
The impartiality that we call Sankarupaka, which is equanimity in regards to formations, meaning anything that arises is treated with the same response.

26
00:03:39,000 --> 00:03:52,000
That is, it is something that is arisen and is something that we'll see seeing it as an object of experience, simply for what it is, without any projection, without liking or disliking.

27
00:03:52,000 --> 00:04:00,000
So if you're asking how to develop this one, then the answer is quite clearly that you have to develop wisdom.

28
00:04:00,000 --> 00:04:07,000
You have to develop understanding for the object.

29
00:04:07,000 --> 00:04:21,000
The progress in the meditation is such that as a person develops their mind, and the mind becomes clearer, as one comes to understand reality clearer.

30
00:04:21,000 --> 00:04:32,000
One comes to regard things with greater and greater equanimity, until finally the mind is simply aware of things as they are with no partiality.

31
00:04:32,000 --> 00:04:40,000
At that point, the medictator could experience great pain and be impartial towards it, could experience great pleasure and be impartial towards it.

32
00:04:40,000 --> 00:04:55,000
And they're in a state of peace that is really considered to be the highest state in Sankarup.

33
00:04:55,000 --> 00:05:04,000
You can't get any higher than this state, because at this point nothing can arise to bring them suffering.

34
00:05:04,000 --> 00:05:15,000
Nothing can cease to make them feel without their completely content in a state of peace that isn't based on an emotion.

35
00:05:15,000 --> 00:05:26,000
Or isn't based on a feeling, so even with great pain, they'll still feel the equanimity. They won't be moved by it. It has nothing to do with entering into some trance state.

36
00:05:26,000 --> 00:05:39,000
It doesn't have an object where you're feeling some kind of equanimity, or you're focused on an object, and therefore blotting everything out, no matter what arises, the mind is equanimity.

37
00:05:39,000 --> 00:05:53,000
It's called Sankarupik, and when a meditator goes through the course, this is the last stage they enter into before the experience of cessation and nibana.

38
00:05:53,000 --> 00:06:14,000
So, there's really no secret as to how to develop that, learn about what it is that you're an equanimist towards. Learn about what it is that you're partial towards. All of the things that we're addicted to that we love or like, learn about them.

39
00:06:14,000 --> 00:06:31,000
Because if you're simply learning about them in an objective manner, not judging them, or not even trying to be equanimist towards them, you'll naturally become less and less inclined towards partiality towards those objects.

40
00:06:31,000 --> 00:06:48,000
So, the answer is, if you practice to see things as they are, to see clearly observing the object and reminding yourself and training yourself to simply see it for what it is, then eventually the mind will lose all interest.

41
00:06:48,000 --> 00:07:00,000
It will see that yes, that's all it is, and it will lose any interest in partiality. It will see that likes and dislikes are actually a stress, and it will give them up.

42
00:07:00,000 --> 00:07:06,000
I would like to give an example for what Bante Uto Damu just said.

43
00:07:06,000 --> 00:07:35,000
If meditating or not meditating, if you, for example, have a mosquito around you, then usually the thought of or the feeling of being bothered comes arises and anger arises and the fear that the mosquito bites and then the wanting to scare it away and or even the wanting to slam.

44
00:07:35,000 --> 00:07:55,000
This is a normal reaction that we automatically have because we have developed our mind to react like this.

45
00:07:55,000 --> 00:08:24,000
When you are meditating or, as I said, when you are not meditating but you want to be mindful of equanimity, then you would just see the mosquito coming, you would hear the sound, you would know it is there, you would think eventually a note, it might bite me, you would see your fear and you would see your worry here and the forest.

46
00:08:24,000 --> 00:08:29,000
Your fear and the forest behold, it might have some disease.

47
00:08:29,000 --> 00:08:37,000
And you note it as it is, but you don't react on it.

48
00:08:37,000 --> 00:08:48,000
And then it eventually bites you, and then it is itching for half an hour, and then that's it.

49
00:08:48,000 --> 00:09:07,000
And you haven't reacted on it. You haven't developed any anger or if you did and it noted early enough, then you haven't developed or cultivated equanimity.

