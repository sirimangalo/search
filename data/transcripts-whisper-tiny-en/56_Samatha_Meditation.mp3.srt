1
00:00:00,000 --> 00:00:03,000
Samata meditation

2
00:00:03,000 --> 00:00:05,000
Question

3
00:00:05,000 --> 00:00:12,000
Auntie, based on many of your videos, I think you do not like Samata meditation at all.

4
00:00:12,000 --> 00:00:17,000
From what I know, there are two clear ways for enlightenment.

5
00:00:17,000 --> 00:00:20,000
Samata first and then Repassana

6
00:00:20,000 --> 00:00:24,000
or Repassana first and then Samata.

7
00:00:24,000 --> 00:00:29,000
What do you think about this?

8
00:00:29,000 --> 00:00:31,000
Answer?

9
00:00:31,000 --> 00:00:37,000
Actually, according to the Buddha, there are four ways to enlightenment.

10
00:00:37,000 --> 00:00:41,000
Samata first and then Repassana

11
00:00:41,000 --> 00:00:48,000
Repassana first and then Samata and Repassana together

12
00:00:48,000 --> 00:00:53,000
or the settling of the mind in regards to the demons.

13
00:00:53,000 --> 00:01:00,000
The Buddha said those are the four ways people can become an arhant.

14
00:01:00,000 --> 00:01:07,000
Regarding your comment of you thinking I do not like Samata meditation at all,

15
00:01:07,000 --> 00:01:14,000
I will say that if that is true, then it is wrong of me, of course.

16
00:01:14,000 --> 00:01:18,000
Because disliking itself is a bad thing.

17
00:01:18,000 --> 00:01:24,000
I admit that I tend to discourage people from focusing their attention

18
00:01:24,000 --> 00:01:28,000
on cultivating Samata meditation.

19
00:01:28,000 --> 00:01:32,000
I do not ever say to not practice it.

20
00:01:32,000 --> 00:01:39,000
Rather, I try to focus on the benefits of Repassana meditation.

21
00:01:39,000 --> 00:01:42,000
There are two reasons for that.

22
00:01:42,000 --> 00:01:47,000
First, Repassana meditation is what I know.

23
00:01:47,000 --> 00:01:53,000
So, it is what I am best suited to help them with.

24
00:01:53,000 --> 00:01:59,000
If instead, I start talking about the benefits of Samata meditation,

25
00:01:59,000 --> 00:02:04,000
I am not going to be able to help anyone realize those benefits.

26
00:02:04,000 --> 00:02:07,000
I will end up saying to them,

27
00:02:07,000 --> 00:02:11,000
I am sorry, I do not teach Samata meditation.

28
00:02:11,000 --> 00:02:14,000
You will have to go somewhere else.

29
00:02:14,000 --> 00:02:19,000
So, it is better for me to promote Repassana meditation,

30
00:02:19,000 --> 00:02:22,000
because I can help with that directly.

31
00:02:22,000 --> 00:02:26,000
The other reason I do not recommend Samata first

32
00:02:26,000 --> 00:02:33,000
is because that path seems to me to have three disadvantages.

33
00:02:33,000 --> 00:02:36,000
It seems to take longer.

34
00:02:36,000 --> 00:02:39,000
It requires more effort.

35
00:02:39,000 --> 00:02:45,000
And it has a greater potential for falling into wrong practice.

36
00:02:45,000 --> 00:02:50,000
The benefits of practicing Samata meditation first

37
00:02:50,000 --> 00:02:55,000
are that it is more complete and more powerful.

38
00:02:55,000 --> 00:03:03,000
More complete means, if provides the potential to enter into profound states of calm and tranquility,

39
00:03:03,000 --> 00:03:10,000
where one can sit stiff as a board in deep bliss or calm.

40
00:03:10,000 --> 00:03:18,000
It is also more complete in the sense that it allows the cultivation of magical powers,

41
00:03:18,000 --> 00:03:25,000
reading people's minds, remembering past lives, all sorts of fun stuff.

42
00:03:25,000 --> 00:03:28,000
It is more powerful in that,

43
00:03:28,000 --> 00:03:35,000
once ability to enter and remain in cessation becomes more powerful.

44
00:03:35,000 --> 00:03:39,000
Having cultivated Samata meditation,

45
00:03:39,000 --> 00:03:46,000
you can more easily enter into cessation for hours or days on end.

46
00:03:46,000 --> 00:03:50,000
Those are the two benefits.

47
00:03:50,000 --> 00:03:53,000
Regarding the disadvantages,

48
00:03:53,000 --> 00:04:02,000
practicing Samata first takes longer because you need to first cultivate meditation based on a concept

49
00:04:02,000 --> 00:04:05,000
that has nothing to do with reality.

50
00:04:05,000 --> 00:04:11,000
True freedom from suffering comes only from understanding reality,

51
00:04:11,000 --> 00:04:17,000
so you need to eventually change the object of your meditation.

52
00:04:17,000 --> 00:04:23,000
Focusing on concepts only suppresses mental defilements.

53
00:04:23,000 --> 00:04:31,000
Practicing Samata first requires more because in order to practice Samata,

54
00:04:31,000 --> 00:04:33,000
you need to seclude yourself.

55
00:04:33,000 --> 00:04:38,000
You need to find a cloud-eye at place away from distraction,

56
00:04:38,000 --> 00:04:43,000
and this is not always easy or even possible.

57
00:04:43,000 --> 00:04:48,000
Samata meditation is ideally practiced in the wilderness,

58
00:04:48,000 --> 00:04:55,000
away from any kind of human contact or worldly disruption.

59
00:04:55,000 --> 00:05:04,000
The third disadvantage of practicing Samata first is the potential for getting lost.

60
00:05:04,000 --> 00:05:08,000
As Samata deals with the realm of concepts,

61
00:05:08,000 --> 00:05:15,000
it is possible to become fully accomplished in profound states of peace and bliss,

62
00:05:15,000 --> 00:05:20,000
without every attaining enlightenment.

63
00:05:20,000 --> 00:05:28,000
It is clear from the texts that such achievements are not sufficient for the attainment of enlightenment.

64
00:05:28,000 --> 00:05:32,000
They lead at most to rebirth in the Brahma world,

65
00:05:32,000 --> 00:05:38,000
as in the example of the bodhisattas to teachers.

66
00:05:38,000 --> 00:05:43,000
Now, Samata is great. It is a wonderful thing.

67
00:05:43,000 --> 00:05:46,000
It quiets and calms your mind.

68
00:05:46,000 --> 00:05:50,000
It is not a matter of disliking it.

69
00:05:50,000 --> 00:05:58,000
We do not call to it Samata meditation because we do not have the time or resources.

70
00:05:58,000 --> 00:06:05,000
We streamline the path because our time and resources are of the essence,

71
00:06:05,000 --> 00:06:11,000
so we teach the most direct path to help the most people.

72
00:06:11,000 --> 00:06:15,000
I do not think that I do not like Samata.

73
00:06:15,000 --> 00:06:22,000
When I was young, I did practice it, sort of not knowing what it was,

74
00:06:22,000 --> 00:06:26,000
but it was very different from what I practiced now.

75
00:06:26,000 --> 00:06:31,000
I do take issue with those who do not see the difference,

76
00:06:31,000 --> 00:06:35,000
those who think they are practicing repassana,

77
00:06:35,000 --> 00:06:39,000
but are actually practicing Samata.

78
00:06:39,000 --> 00:06:47,000
It is tough because you cannot just tell someone that their practice is inferior to your own practice.

79
00:06:47,000 --> 00:06:52,000
Everyone is going to say that their practice is better,

80
00:06:52,000 --> 00:07:01,000
but it is an unavoidable reality that some meditation cannot lead to enlightenment,

81
00:07:01,000 --> 00:07:06,000
because it is not focused on reality.

82
00:07:06,000 --> 00:07:12,000
It is like the story of the person who lost their contact lens in a dark room

83
00:07:12,000 --> 00:07:18,000
and went outside to look for it because the light was better.

84
00:07:18,000 --> 00:07:23,000
Even though Samata meditation might be easier and more pleasant,

85
00:07:23,000 --> 00:07:52,000
that is primarily because it is conceptually based.

