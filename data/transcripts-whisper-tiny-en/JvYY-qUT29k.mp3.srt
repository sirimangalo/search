1
00:00:00,000 --> 00:00:02,760
Okay, starting.

2
00:00:02,760 --> 00:00:03,920
I feel confused.

3
00:00:03,920 --> 00:00:07,160
Is it okay not to feel the body and meditation?

4
00:00:07,160 --> 00:00:10,640
For example, often I can't tell where my eyes are looking.

5
00:00:10,640 --> 00:00:13,360
Later bodily feelings seem to fade.

6
00:00:13,360 --> 00:00:15,400
I only register the breath.

7
00:00:15,400 --> 00:00:19,400
Is that okay?

8
00:00:19,400 --> 00:00:22,800
Yeah, in fact, I'm probably just going to answer this.

9
00:00:22,800 --> 00:00:25,240
I don't know if anyone else has anything to add.

10
00:00:25,240 --> 00:00:32,560
But it's actually considered to be one of the stages, potentially one of the stages of knowledge.

11
00:00:32,560 --> 00:00:35,000
The point being that the body doesn't exist.

12
00:00:35,000 --> 00:00:37,640
The body was never there in the first place.

13
00:00:37,640 --> 00:00:40,560
And there comes a point in your practice where you realize that.

14
00:00:40,560 --> 00:00:45,240
And as a result you say, oh, my body disappeared, well, the body wasn't there in the

15
00:00:45,240 --> 00:00:46,240
first place.

16
00:00:46,240 --> 00:00:47,320
I mean, think about it.

17
00:00:47,320 --> 00:00:51,840
Without experiencing it, where does the body, where does the idea of the body exist at

18
00:00:51,840 --> 00:00:52,840
all?

19
00:00:52,840 --> 00:00:54,200
It exists in the mind.

20
00:00:54,200 --> 00:00:59,520
So our continued awareness of the body is totally mental.

21
00:00:59,520 --> 00:01:04,600
It comes from an experience here and experience there that we put together in your mind

22
00:01:04,600 --> 00:01:06,520
and say, oh, my body.

23
00:01:06,520 --> 00:01:12,080
But really, when you close your eyes, why should your body be there at all?

24
00:01:12,080 --> 00:01:14,040
You don't see it anymore.

25
00:01:14,040 --> 00:01:17,240
And if you're sitting quite still, you don't feel it anymore.

26
00:01:17,240 --> 00:01:20,080
It only exists as a concept in the mind.

27
00:01:20,080 --> 00:01:23,680
And there comes a point in your meditation where you break through that and you start

28
00:01:23,680 --> 00:01:34,760
to observe only ultimate realities at which point body has no place in place no part.

29
00:01:34,760 --> 00:01:38,640
So potentially, what you're describing is actually a very good thing.

30
00:01:38,640 --> 00:01:47,120
It's a realization of the nature of reality giving up the observation of concepts like

31
00:01:47,120 --> 00:01:48,120
body.

32
00:01:48,120 --> 00:01:57,600
Yeah, I think it would be kind of silent after that, anyone?

33
00:01:57,600 --> 00:02:26,560
Okay, okay, okay, okay, okay, okay, okay.

