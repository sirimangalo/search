1
00:00:00,000 --> 00:00:10,480
Okay, good evening everyone. We're broadcasting live October 20th, 2015.

2
00:00:10,480 --> 00:00:17,120
Tonight, we're not going to do a demo part. I'm going to ask to do a short session tonight

3
00:00:17,120 --> 00:00:26,680
because I have an exam tomorrow and various things going on this week. So, it wasn't able

4
00:00:26,680 --> 00:00:31,960
to get around to it. Today was a big day as well and wasn't able to get around to study

5
00:00:31,960 --> 00:00:40,600
for the Dhammapanda. It takes at least a little bit of preparation. So hopefully we'll do that

6
00:00:40,600 --> 00:00:45,640
tomorrow instead. Tonight, if there are any questions, I'll answer questions otherwise.

7
00:00:47,160 --> 00:00:49,000
Let's start with that. Are there any questions?

8
00:00:49,000 --> 00:00:55,480
Yes, I'm sure there are. Just have to see where they start.

9
00:01:03,640 --> 00:01:08,200
Hello, Bhandi. Sometimes in meditation, I don't know if I'm forcing myself or if I'm just trying to

10
00:01:08,200 --> 00:01:13,400
be sincere in the practice. And when I let go of many thoughts, when I let go, many thoughts

11
00:01:13,400 --> 00:01:18,920
come to my mind, although I try to be mindful about them. But I feel a bit lazy and feeling

12
00:01:18,920 --> 00:01:24,440
that I am not sincere enough in my practice. Can you help me with this complex? Thanks.

13
00:01:31,240 --> 00:01:43,080
Well, the entry point there is feeling lazy or judging yourself is lazy. However, it actually

14
00:01:43,080 --> 00:01:48,920
appears to you. You really do have a feeling of lethargy and you should acknowledge that. If you

15
00:01:48,920 --> 00:01:54,920
have just a guilty feeling like, oh, I'm being lazy and that's a judgment and you should acknowledge

16
00:01:54,920 --> 00:02:06,840
that, like judging or thinking or disliking or however it appears to you. But trying to be sincere

17
00:02:06,840 --> 00:02:14,120
is usually problematic. I mean, you either are or you aren't. It's not about trying.

18
00:02:15,000 --> 00:02:22,360
Yoda said, do or do not. There is no trying. I mean, on some level, you are trying and trying

19
00:02:22,360 --> 00:02:28,920
is important. It's important to keep trying and never give up. But as you practice, you'll start

20
00:02:28,920 --> 00:02:35,880
to realize that it's not exactly a trying. It's just that, okay, now I'm mindful. Again, I'm mindful.

21
00:02:35,880 --> 00:02:41,720
Again, I'm mindful. It's not a sense of being sincere or pushing. You know, in a sense of

22
00:02:43,960 --> 00:02:51,720
cultivating something because that becomes a sankara, a sense, formation, you start to have an

23
00:02:51,720 --> 00:02:56,360
artificial formation arrives. We don't want. We want to be natural.

24
00:02:59,160 --> 00:03:03,800
So the effort that you put out is just that one moment to be mindful in one moment and then again,

25
00:03:03,800 --> 00:03:06,520
and then another moment.

26
00:03:09,400 --> 00:03:16,680
So focus more on your judgment of thinking or lazy because it sounds like that's okay. If you let

27
00:03:16,680 --> 00:03:22,280
the mind, let the thoughts come to the mind, that's fine. They're coming not because you want them to.

28
00:03:23,400 --> 00:03:28,040
You just have to catch them when they do come. Once you realize that you're thinking,

29
00:03:28,040 --> 00:03:34,280
they're thinking eventually you'll catch it short quicker and quicker.

30
00:03:36,280 --> 00:03:40,600
Question for anyone who can answer it. It seems as though the number of minutes in the

31
00:03:40,600 --> 00:03:45,880
meditation log decreases gradually. Is there a time limit on how long your minutes of meditation

32
00:03:45,880 --> 00:03:56,120
stay in the log? I think a week. I think your personal meditation log is for the past week.

33
00:03:56,120 --> 00:04:01,960
Don't quote me on that, but there is a time limit. It's not giving you your lifetime meditation.

34
00:04:03,080 --> 00:04:05,080
It's just giving you the past week, I think.

35
00:04:10,440 --> 00:04:12,920
Yeah, that's whatever it is. It's just a simple log.

36
00:04:15,400 --> 00:04:18,680
We need a professional to make this site look all beautiful and

37
00:04:18,680 --> 00:04:27,400
land. Jake weary it up a bit or he jacks it up. I don't know what you'd use.

38
00:04:28,680 --> 00:04:30,600
Stylish it up.

39
00:04:33,720 --> 00:04:35,640
We're supposed to be boring. We're meditators.

40
00:04:36,760 --> 00:04:40,360
That's true. Good that it's kind of boring and uninteresting.

41
00:04:41,320 --> 00:04:43,800
Yeah, so we don't want to be distracted from meditation.

42
00:04:43,800 --> 00:04:49,000
It's a bit of a mess really. It looks like a patchwork robe.

43
00:04:49,800 --> 00:04:53,240
It looks like a monk picked up pieces off the floor and sewed them together.

44
00:04:54,760 --> 00:05:02,200
As I recall, you put this together pretty quickly, so considering considering it's very functional.

45
00:05:03,400 --> 00:05:08,440
Dante, I was wondering if I have started a habit and meditation that is incorrect.

46
00:05:08,440 --> 00:05:13,240
When I turn my awareness specifically to thoughts, they disappear as soon as I know them.

47
00:05:13,240 --> 00:05:17,160
I feel like this is sort of like cultivating a version, but I'm not sure.

48
00:05:17,880 --> 00:05:21,320
I've always known to simply observe them, not get them to go away.

49
00:05:25,080 --> 00:05:29,240
No, it's not, it's not getting them to go away, but that's clear. They are, they do go away.

50
00:05:31,080 --> 00:05:35,640
Your intention is not to make them go away. Your intention is to remind yourself that's a thought.

51
00:05:36,360 --> 00:05:40,520
Unfortunately, you know, of course, when you remind yourself that's a thought,

52
00:05:40,520 --> 00:05:43,960
you're thinking something new and so you've broken the chain, that's normal.

53
00:05:45,000 --> 00:05:46,760
A version is something entirely different.

54
00:05:52,520 --> 00:05:58,040
Can you explain today's quote? The quote is, of the tree in whose shade

55
00:05:58,040 --> 00:06:03,720
once it's our lies, not a branch of it should he break. For if he did, he would be a betrayer

56
00:06:03,720 --> 00:06:08,280
of a friend and evil doer. The recognized this one is from the Jataka.

57
00:06:08,280 --> 00:06:14,920
Let's find the good, there's a really good English translation, if I remember correctly.

58
00:06:19,000 --> 00:06:24,760
No, except it's got the wrong numbers.

59
00:06:24,760 --> 00:06:37,240
For 93.

60
00:06:49,640 --> 00:06:52,280
Which one is it? The name is called the Maha.

61
00:06:52,280 --> 00:06:59,160
You are ninja.

62
00:06:59,160 --> 00:07:27,160
The tree that gives you pleasant shade to sit or lie at need, you should not tear its branches

63
00:07:27,160 --> 00:07:34,160
down a cruel want indeed.

64
00:07:34,160 --> 00:07:42,160
It's funny, really.

65
00:07:42,160 --> 00:07:56,160
But it's a weird verse, really.

66
00:07:56,160 --> 00:08:00,160
I assumed it was a metaphoric, right?

67
00:08:00,160 --> 00:08:12,160
It evades me, there was something about how this relates to a good friend.

68
00:08:12,160 --> 00:08:25,160
There's some story of this man who betrays a friend and then it's compared to cutting the branch off a tree that shades you.

69
00:08:25,160 --> 00:08:37,160
Yeah, I think it's a metaphoric.

70
00:08:37,160 --> 00:08:54,160
It doesn't look like it's actually metaphorical, but the idea is you don't betray a friend or someone who has been helpful to you.

71
00:08:54,160 --> 00:08:59,160
Are the three marks conceptual?

72
00:08:59,160 --> 00:09:02,160
No.

73
00:09:02,160 --> 00:09:14,160
They are characteristics of reality.

74
00:09:14,160 --> 00:09:22,160
We're caught up on questions for the moment, so could I talk about offering you a robe?

75
00:09:22,160 --> 00:09:35,160
This is the time of year when the rains retreat ends, and typically people will offer a robe to a monk at their local monastery.

76
00:09:35,160 --> 00:09:45,160
And not long ago, not long enough ago, just about a week ago, it occurred to me that maybe our internet community could find a way to offer a venerable you to demo a robe.

77
00:09:45,160 --> 00:09:55,160
And it's a little complicated since we're kind of spread out around the world, and a little complicated, but we got a way to figure it out.

78
00:09:55,160 --> 00:10:09,160
I've been in touch with a person in Thailand who is placing an order for a robe, and we have an opportunity, I have an opportunity for people to share in the merit of this.

79
00:10:09,160 --> 00:10:15,160
I just posted it into the link there, and if anyone would like to share in the merit of this, please do.

80
00:10:15,160 --> 00:10:19,160
So what actually happened was, well, a couple things.

81
00:10:19,160 --> 00:10:29,160
I wasn't actually sure of the cost of the robe because it is in bot, and I'm not very knowledgeable about that, but I have a rough estimate.

82
00:10:29,160 --> 00:10:36,160
But we have, I have a plan for everything that is donated over and above the actual cost of the robe.

83
00:10:36,160 --> 00:10:44,160
And that is, before a venerable you to demo, began his own monastery, he stayed at Stony Creek.

84
00:10:44,160 --> 00:10:50,160
I'm on a stair called Wat Kmerkram, and at one point said he'd really like to do something nice for them.

85
00:10:50,160 --> 00:10:55,160
They had supported him for a long time, and he'd like to do something nice for him for them.

86
00:10:55,160 --> 00:11:00,160
And this is kind of our opportunity to share in that merit as well.

87
00:11:00,160 --> 00:11:10,160
So anyone who would like to share in the merit of the robe, please do so.

88
00:11:10,160 --> 00:11:13,160
Everything that we receive over and above the cost of the robe will be re-donated to Wat Kmerkram because they also have an online campaign.

89
00:11:13,160 --> 00:11:16,160
They're doing a building project of some sort.

90
00:11:16,160 --> 00:11:27,160
And if you look at the campaign that I created in the text of it, it shows the second campaign where any additional funds will be re-gifted.

91
00:11:27,160 --> 00:11:31,160
So there's a lot of good merit potential if anybody's interested.

92
00:11:31,160 --> 00:11:36,160
So what's going to happen with the robe is it will be mailed from Thailand to the monastery.

93
00:11:36,160 --> 00:11:43,160
And to people who are local, Aruna and Stefano, who's going to be going to the monastery to meditate for a couple of months,

94
00:11:43,160 --> 00:11:53,160
will offer to a venerable you to demo or possibly even put together a little ceremony and possibly invite others if others are available.

95
00:11:53,160 --> 00:12:00,160
Certainly anybody who's in the area let us know we can figure out something and make it nice.

96
00:12:00,160 --> 00:12:10,160
It will probably be a little bit after the normal deadline, but hopefully not too long after the normal deadline.

97
00:12:10,160 --> 00:12:17,160
So that's about it. Thank you, Bhante.

98
00:12:17,160 --> 00:12:23,160
And still caught up on questions.

99
00:12:23,160 --> 00:12:26,160
All right, let's go.

100
00:12:26,160 --> 00:12:29,160
Say goodnight. I can't.

101
00:12:29,160 --> 00:12:36,160
In regards to money, I can't talk and I can't even say.

102
00:12:36,160 --> 00:12:44,160
I can't even express thanks in regards to money, but.

103
00:12:44,160 --> 00:12:51,160
Well, appreciate the thought of trying to provide me with robes and that's kind of awesome.

104
00:12:51,160 --> 00:12:53,160
You can have a new color, I think.

105
00:12:53,160 --> 00:13:05,160
So it's much appreciated everyone's continuing interest and support to keep me alive and closed and housed and that kind of thing.

106
00:13:05,160 --> 00:13:07,160
It's much appreciated.

107
00:13:07,160 --> 00:13:18,160
It's quite a nice opportunity on our end to be close, you know, for a lot of us we didn't grow up in Buddhist culture and this is this is new and you know, it's really cool.

108
00:13:18,160 --> 00:13:20,160
So thank you.

109
00:13:20,160 --> 00:13:22,160
That's awesome that we're able to do this.

110
00:13:22,160 --> 00:13:24,160
They said it couldn't be done.

111
00:13:24,160 --> 00:13:29,160
We're starting a center without charging money to anyone.

112
00:13:29,160 --> 00:13:32,160
Yes, which is pretty cool.

113
00:13:32,160 --> 00:13:34,160
Good night.

114
00:13:34,160 --> 00:13:38,160
Good night, Dante. Thank you. Thanks for your help.

