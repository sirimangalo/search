1
00:00:00,000 --> 00:00:01,000
Go ahead.

2
00:00:01,000 --> 00:00:07,000
I have a friend who suffers from a severe disease and only has one month and a half

3
00:00:07,000 --> 00:00:08,840
according to the doctors.

4
00:00:08,840 --> 00:00:13,840
I'm able to deal with this much better than my friend's problem is.

5
00:00:13,840 --> 00:00:35,840
I don't know how to help them through this better.

6
00:00:35,840 --> 00:00:47,480
So, the question is not about your friend who has only one month and a half, but about

7
00:00:47,480 --> 00:00:50,080
the other friends around both of you.

8
00:00:50,080 --> 00:00:52,080
Do I understand that correctly?

9
00:00:52,080 --> 00:00:57,840
How to help other people deal with the friend who's dying or when a friend dies?

10
00:00:57,840 --> 00:01:12,040
I think the first thing to do is to make them see, maybe you find a careful way to make them

11
00:01:12,040 --> 00:01:21,680
see that death is very much part of life and that everybody has to face it sooner or

12
00:01:21,680 --> 00:01:31,800
later and that from the beginning we are born, we are dying, we are of course some years

13
00:01:31,800 --> 00:01:42,520
are building up, strength and power and so on, but I think from age 18 or so we start

14
00:01:42,520 --> 00:01:49,520
actually to age and to get old.

15
00:01:49,520 --> 00:02:03,200
This is a fact and it is very important to accept this, although most people run away from

16
00:02:03,200 --> 00:02:10,240
seeing this fact and try to pretend this will never happen to me, so I think the most

17
00:02:10,240 --> 00:02:18,120
important is to stop them from running away and make them understand that it is going

18
00:02:18,120 --> 00:02:32,160
to happen and that it is something very natural that it is nothing that we have to fear,

19
00:02:32,160 --> 00:02:39,880
although most people fear it.

20
00:02:39,880 --> 00:02:57,680
We should try to see it as natural happening and I don't know how I have something

21
00:02:57,680 --> 00:03:10,880
about it, but I am not sure how to say that, please go ahead you and maybe I come with

22
00:03:10,880 --> 00:03:11,880
my soul later.

23
00:03:11,880 --> 00:03:17,560
Well, you do have to mention what we are talking about in the last question as well,

24
00:03:17,560 --> 00:03:23,920
that autonomy will put among but it will pay any reason.

25
00:03:23,920 --> 00:03:33,360
You have to set yourself in what is right first before you can help others, so looking

26
00:03:33,360 --> 00:03:41,120
out for other people and trying to give them the answers to life, it is not really

27
00:03:41,120 --> 00:03:49,280
in the cards so much, even the Buddha was only able to show the way and he said it is

28
00:03:49,280 --> 00:03:58,880
up to you to do the work, stating the obvious and letting them know that that is a part

29
00:03:58,880 --> 00:04:04,920
of life is important, it takes a lot of work and a lot of understanding and you have

30
00:04:04,920 --> 00:04:11,080
to develop yourself, you really have to be a meditation teacher to answer your question

31
00:04:11,080 --> 00:04:15,160
because if you are a meditation teacher then you do have something to give to them, if

32
00:04:15,160 --> 00:04:20,080
you have done the training and know how to teach people meditation, that is all I could

33
00:04:20,080 --> 00:04:25,200
answer is saying, find them a meditation teacher or train yourself as a meditation teacher

34
00:04:25,200 --> 00:04:29,600
because we will all meet with these people, that is why I have everyone who comes here

35
00:04:29,600 --> 00:04:34,960
to try to encourage them to train as a meditation teacher because that is what you can

36
00:04:34,960 --> 00:04:45,120
actually give to people, but of course you can only ever point them the way you

37
00:04:45,120 --> 00:04:52,480
can't make them drink.

38
00:04:52,480 --> 00:05:04,080
Yeah, I think my thought was going in that direction that I wanted to find a shortcut

39
00:05:04,080 --> 00:05:10,360
in how you could tell your friends that are actually in that situation of losing a friend

40
00:05:10,360 --> 00:05:20,840
in one and a half months and that might be very scary for them so it is probably very

41
00:05:20,840 --> 00:05:29,160
important for them to say everything that they want to say to that person and to kind

42
00:05:29,160 --> 00:05:38,120
of bit farewell already so that in the end is nothing left to say so that they spend

43
00:05:38,120 --> 00:05:44,360
all the time that they need to spend with that person, that they ask for forgiveness

44
00:05:44,360 --> 00:05:53,000
for things that they have done wrong or ask that person to ask for forgiveness from

45
00:05:53,000 --> 00:06:00,400
them if there is anything unclear among them, this should be cleared out before the person

46
00:06:00,400 --> 00:06:01,400
dies.

47
00:06:01,400 --> 00:06:14,600
No, no, no, totally to add to that, normally we don't know when we are going to die and

48
00:06:14,600 --> 00:06:21,600
I had a friend in Los Angeles, he is a Buddhist and his brother died, like fell down

49
00:06:21,600 --> 00:06:26,600
the stairs or got hit by something and went into a coma and died within a few days so

50
00:06:26,600 --> 00:06:28,400
they didn't have the chance.

51
00:06:28,400 --> 00:06:32,760
You've got a month and a half with this guy, right, that's a great point that you made

52
00:06:32,760 --> 00:06:39,360
is that you now have the time to find closure with this person and that's really how

53
00:06:39,360 --> 00:06:43,440
we should look at all of us, we should look around us at everyone, this is how the Buddha

54
00:06:43,440 --> 00:06:51,040
would have us do with everyone, don't spend your time fighting, don't spend your time

55
00:06:51,040 --> 00:06:58,800
prancing around as tomorrow wasn't going to come, you don't know when death is going

56
00:06:58,800 --> 00:07:03,600
to come to yourself or to the people around you if you waste your time arguing and fighting

57
00:07:03,600 --> 00:07:10,360
and don't get the chance to find closure with the person, this is what leads people to

58
00:07:10,360 --> 00:07:17,240
suffer horribly, to be unprepared for their own death and for the death of other people.

59
00:07:17,240 --> 00:07:22,240
It's a great point to take the time, what other thing I wanted to say, if you have more

60
00:07:22,240 --> 00:07:31,840
please, you know, but is that in line with R not being able to help other people, we always

61
00:07:31,840 --> 00:07:37,520
do this, we go to funerals and I've seen it several times already, you go to the funeral

62
00:07:37,520 --> 00:07:42,520
and you explain all of this about how we should understand death and death is a part of life

63
00:07:42,520 --> 00:07:47,680
and that we shouldn't cry and we shouldn't feel bad, it doesn't help the person, it doesn't

64
00:07:47,680 --> 00:07:51,080
help us.

65
00:07:51,080 --> 00:07:56,880
What we should be doing is doing good deeds on behalf of this person, let their name live

66
00:07:56,880 --> 00:08:05,720
on, do something to carry on our good relationship with them, to find the closure, to say

67
00:08:05,720 --> 00:08:09,720
well I have this attachment to this person, I'm going to, every time I think about them

68
00:08:09,720 --> 00:08:13,640
I'm going to use that as an impetus to do more good deeds.

69
00:08:13,640 --> 00:08:22,280
So I just remember a girl here from the village, very going on arms round, she speaks

70
00:08:22,280 --> 00:08:28,720
good English and she came the other day to give a donation, to put it in the donation

71
00:08:28,720 --> 00:08:37,840
box when we were present and Bante asked if there is a special reason for it and she said

72
00:08:37,840 --> 00:08:46,480
yes for this and that person died and we said, oh, condolences and so on and she said

73
00:08:46,480 --> 00:08:54,520
oh, that's 18 years ago, so still they do it, they do every month, they do something for

74
00:08:54,520 --> 00:09:01,960
that person, but the conclusion that I wanted to bring up is just something for some sort

75
00:09:01,960 --> 00:09:08,040
of food for thought, is that we always give this kind of teaching, especially emphasizing

76
00:09:08,040 --> 00:09:13,640
the don't cry part and then our turn is over and the whole family comes up and says

77
00:09:13,640 --> 00:09:20,040
there are peace and they all break down cry, invariably, you might give this profound

78
00:09:20,040 --> 00:09:28,640
dhamma talk about how death is just a natural part of life and it's not something but

79
00:09:28,640 --> 00:09:34,760
the attachment, this is the habits that have been built up, you can't break that, you

80
00:09:34,760 --> 00:09:42,000
can't control it, so on the one hand you might, you might for that reason say they're

81
00:09:42,000 --> 00:09:48,920
crying or they're their way, their sadness is the natural outcome of the years and years

82
00:09:48,920 --> 00:09:57,880
of clinging and their inability to understand the feelings that arise when sadness arises

83
00:09:57,880 --> 00:10:03,200
the inability to acknowledge, the inability to see it for what it is and so it leads

84
00:10:03,200 --> 00:10:08,200
to what am I going to do, oh and then sad again, what am I going to do is sad again, feeding

85
00:10:08,200 --> 00:10:17,640
it, it creates a feedback loop, so you may not be able to help people to not cry and not

86
00:10:17,640 --> 00:10:30,200
be sad, but on the other hand you can see that as a guideline for how to actually help

87
00:10:30,200 --> 00:10:35,400
these people is that you can't make them not feel sad, but you can help them to deal

88
00:10:35,400 --> 00:10:39,400
with the sadness and say it's natural to be sad, not say we shouldn't feel sad, but

89
00:10:39,400 --> 00:10:46,120
say when we feel sad we should be mindful of it and we should understand where the sadness

90
00:10:46,120 --> 00:10:51,440
comes, you know, teaching people why they are feeling sad, because often you see people

91
00:10:51,440 --> 00:10:55,840
they will, you can talk to them after and say you know wow it was, it was, that was

92
00:10:55,840 --> 00:11:00,080
really moving or so on and say they'll say yeah I didn't think that was going to cry,

93
00:11:00,080 --> 00:11:03,680
they didn't feel like they were going to cry because they're hiding it all inside, but

94
00:11:03,680 --> 00:11:08,040
as soon as they get up there and start to talk about the person, everyone breaks down

95
00:11:08,040 --> 00:11:17,760
it's quite interesting, it's a funeral phenomenon and so you know help them when the sadness

96
00:11:17,760 --> 00:11:23,320
comes up to, you know understand why it's there, why it's coming out of nowhere and how

97
00:11:23,320 --> 00:11:31,400
to deal with it, how to be mindful of it and so on.

98
00:11:31,400 --> 00:11:41,920
One thing I wanted to add that is only far related to your question, but it is a great

99
00:11:41,920 --> 00:11:47,160
lesson maybe for yourself and for your friends when a friend is dying and you can be with

100
00:11:47,160 --> 00:11:57,680
the friend and you can learn a lot from that and be grateful for that friend that this person

101
00:11:57,680 --> 00:12:06,120
shares this experience of dying with you and learn as much as you can from that and take

102
00:12:06,120 --> 00:12:16,200
the lesson very well, it is, the Pali word is Maruna Musati, the reflection on death that

103
00:12:16,200 --> 00:12:32,120
helps maybe to to understand some of life, so yeah, I'm good.

