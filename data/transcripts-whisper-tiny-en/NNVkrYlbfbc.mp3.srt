1
00:00:00,000 --> 00:00:09,000
Hello YouTube. I'm back now for Ask a Monk. It was a way for a brief time.

2
00:00:09,000 --> 00:00:13,000
We'll be busy over the weekend. So I've got some questions here.

3
00:00:13,000 --> 00:00:19,000
I'm going to answer them not according to the order they were received, but according to popularity.

4
00:00:19,000 --> 00:00:22,000
So please remember to place your votes.

5
00:00:22,000 --> 00:00:27,000
So the lead question is, how should my meditation progress

6
00:00:27,000 --> 00:00:32,000
after the initial stages of concentration of the breath and being witness to a clear thought?

7
00:00:32,000 --> 00:00:37,000
And when will I know I have arrived?

8
00:00:37,000 --> 00:00:43,000
I don't know exactly about when will I know I have arrived because it's a long path and

9
00:00:43,000 --> 00:00:49,000
it's really a lot more gradual. What happens when you create the clear thought is you start to let go of things.

10
00:00:49,000 --> 00:00:53,000
You don't cling and you don't worry and you don't fret over things.

11
00:00:53,000 --> 00:00:59,000
You don't hold on to it because you see that there's nothing you can do to keep it the way the way you want it.

12
00:00:59,000 --> 00:01:02,000
It's not going to bring you peace and happiness.

13
00:01:02,000 --> 00:01:08,000
These things that we hold on to our modes of reacting to things,

14
00:01:08,000 --> 00:01:15,000
either clinging to good things and chasing after them or running away from bad things and preventing them,

15
00:01:15,000 --> 00:01:22,000
always denying them this compartmentalization of reality as these things are good and these things are

16
00:01:22,000 --> 00:01:26,000
bad and only allowing certain experiences in.

17
00:01:26,000 --> 00:01:30,000
Seeing that we see that that's a cause for suffering,

18
00:01:30,000 --> 00:01:34,000
that there's nothing that we could hold on to, nothing that we could cling to,

19
00:01:34,000 --> 00:01:40,000
nothing that we could try to shape and mold into the perfect reality that would make us happy.

20
00:01:40,000 --> 00:01:43,000
And so we let go. The clear thought just helps you to let go.

21
00:01:43,000 --> 00:01:51,000
It helps you to be here and now without having to be somewhere else or something else than what you are.

22
00:01:51,000 --> 00:01:56,000
So there's really not very far that you have to go.

23
00:01:56,000 --> 00:01:59,000
If you're creating the clear thought, you should be seeing.

24
00:01:59,000 --> 00:02:06,000
If you're practicing correctly, you should be seeing learning things about yourselves that you didn't know before,

25
00:02:06,000 --> 00:02:10,000
coming to see things that you didn't see, you couldn't see about yourself before.

26
00:02:10,000 --> 00:02:16,000
It'll help you to deal with all of the difficulties in life,

27
00:02:16,000 --> 00:02:25,000
to deal with things in a more rational and more wise and a more productive way.

28
00:02:25,000 --> 00:02:30,000
There's a lot of technical detail that I could go into as to the stages of practice,

29
00:02:30,000 --> 00:02:36,000
but that's not really helpful and it's not really advised in the beginning stages anyway.

30
00:02:36,000 --> 00:02:43,000
The best thing I could advise for you is to find a teacher and practice as you are continuously,

31
00:02:43,000 --> 00:02:49,000
because it's the continuous practice that leads to the goal that leads you to arrive.

32
00:02:49,000 --> 00:02:53,000
One thing I would say is that you're going to run into a lot of hindrances.

33
00:02:53,000 --> 00:02:59,000
You're going to come up against a lot of things that try to trick you into thinking that you've hit a roadblock.

34
00:02:59,000 --> 00:03:07,000
There will be states of liking, states of disliking, states of boredom, states of worries,

35
00:03:07,000 --> 00:03:14,000
states of depression. Your mind is going to change as you practice, and it's not necessarily going to be in a positive way.

36
00:03:14,000 --> 00:03:16,000
Your mind is always changing.

37
00:03:16,000 --> 00:03:20,000
So you might be thinking that when you practice, you're just going to get happier and happier and happier,

38
00:03:20,000 --> 00:03:26,000
but what you're doing in the meditation is digging up all of the positive and negative stuff inside of you.

39
00:03:26,000 --> 00:03:31,000
So there's going to be a lot of negative stuff coming up, and you should be very quick to catch that

40
00:03:31,000 --> 00:03:37,000
and to apply the meditation to everything that arises.

41
00:03:37,000 --> 00:03:40,000
So when something comes up and you think, oh, now what do I do, or you think, oh, that's it,

42
00:03:40,000 --> 00:03:43,000
and I don't have to practice anymore, practice on that.

43
00:03:43,000 --> 00:03:49,000
Look at what's going on in your mind, and as long as you're doing that, continuously being very honest with yourself,

44
00:03:49,000 --> 00:03:51,000
the clear thought is all you need.

45
00:03:51,000 --> 00:03:56,000
As long as you have this clear understanding of things as they are, there's nothing else you need,

46
00:03:56,000 --> 00:04:01,000
and there's no other stage or method of practice.

47
00:04:01,000 --> 00:04:10,000
Once you practice like this, the arrival is the freedom from attachment that no longer you need things to be in a certain way.

48
00:04:10,000 --> 00:04:15,000
So if you're asking when you arrive, the best thing I could say is when you have no more greed,

49
00:04:15,000 --> 00:04:19,000
when you have no more anger, when you have no more delusion, when you just see things as they are,

50
00:04:19,000 --> 00:04:24,000
and when you're content with things as they are, and when nothing can bring you stress or suffering,

51
00:04:24,000 --> 00:04:26,000
that's when you've reached the goal.

52
00:04:26,000 --> 00:04:55,000
So I hope that helps, this is my first answer, and thanks, keep them coming.

