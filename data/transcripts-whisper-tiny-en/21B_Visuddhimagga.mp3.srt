1
00:00:00,000 --> 00:00:07,920
So this anger will not bring you full enlightenment as a Buddha or to enlightenment as a

2
00:00:07,920 --> 00:00:12,000
particular Buddha or to enlightenment as a disciple and so on.

3
00:00:12,000 --> 00:00:17,280
So you talk to yourself this way.

4
00:00:17,280 --> 00:00:29,840
And also you reflect on yourself and also you reflect on the other person in the same way

5
00:00:29,840 --> 00:00:30,840
in.

6
00:00:30,840 --> 00:00:36,840
So he is now angry with me and he is angry with me and he is angry with me and his anger

7
00:00:36,840 --> 00:00:41,160
will not beat him to the enlightenment as a Buddha and so on.

8
00:00:41,160 --> 00:00:48,720
And so the anger will give him thankful results.

9
00:00:48,720 --> 00:00:56,160
So in this way you may be able to get rid of resentment and then the next, the sixth one

10
00:00:56,160 --> 00:01:01,080
is review the special qualities of the master's form of conduct.

11
00:01:01,080 --> 00:01:12,440
That means think of the bodhisattva in his, in this life as bodhisattva he had done many

12
00:01:12,440 --> 00:01:25,040
noble things and so by reflecting on his form of conduct you may be able to get rid

13
00:01:25,040 --> 00:01:37,680
of resentment and a number of jadakas are referred to here, the first one is once.

14
00:01:37,680 --> 00:01:48,960
For example in the Sila Wanda Bhatt story, do you have jadakakabuk here or blabadi?

15
00:01:48,960 --> 00:01:52,240
So you can find these jadakas in that book.

16
00:01:52,240 --> 00:01:58,240
When his friends rose to provide his kingdom of 300 leagues being seized by an enemy king

17
00:01:58,240 --> 00:02:03,440
who had been incited by wicked minister and whose mind is all queen had so hate for him.

18
00:02:03,440 --> 00:02:06,320
He did not allow them to live over upon and so on.

19
00:02:06,320 --> 00:02:15,920
So the other king seized him and then buried him to do the neck and then he was queen.

20
00:02:15,920 --> 00:02:23,280
He was left at the cemetery with other office ministers.

21
00:02:23,280 --> 00:02:33,920
Even he was treated that way he did not get angry with that king and then with his effort

22
00:02:33,920 --> 00:02:44,320
he got rid of that stage and he went back to his palace and then the other he made peace

23
00:02:44,320 --> 00:02:47,040
with the other king.

24
00:02:47,040 --> 00:03:00,320
So the king whose name was Sila Wanda here practiced patience of forbearance with the other

25
00:03:00,320 --> 00:03:05,840
king who tried to kill him and so in the same way you should not have anger towards such

26
00:03:05,840 --> 00:03:21,760
presence and so on.

27
00:03:21,760 --> 00:03:30,600
And he practiced patience but the king was very cruel but at the time when he met when

28
00:03:30,600 --> 00:03:39,240
the king met him and asked him what do you practice he said I am a preacher of patience.

29
00:03:39,240 --> 00:03:46,760
So the king had him flocked with couches of pounds and had his hands and feet cut off.

30
00:03:46,760 --> 00:03:54,080
He felt not the slightest anger although his hands and feet were cut he did not feel any

31
00:03:54,080 --> 00:03:58,280
anger towards that king.

32
00:03:58,280 --> 00:04:11,200
And then the general king left the helmet the general king and asked him to get angry

33
00:04:11,200 --> 00:04:13,160
with the king.

34
00:04:13,160 --> 00:04:18,080
Please hate the king, please get angry with the king so that he would not suffer so much

35
00:04:18,080 --> 00:04:27,160
painful consequences and the helmet said people like me do not hate or people like me do

36
00:04:27,160 --> 00:04:33,600
not get angry that is what he said.

37
00:04:33,600 --> 00:04:39,400
And the next is Sujulat Damabala.

38
00:04:39,400 --> 00:04:45,920
He was a child good boy said there was a child in this story and he was killed by his

39
00:04:45,920 --> 00:04:48,080
his own father.

40
00:04:48,080 --> 00:05:00,040
And then the next one on page 329 was the Jardhaka the story of an elephant where Boris

41
00:05:00,040 --> 00:05:04,560
had always born as an elephant at that time.

42
00:05:04,560 --> 00:05:09,600
The elephant once struck by the stout chef addressed the hunter with no hate in mind.

43
00:05:09,600 --> 00:05:10,600
What is your aim?

44
00:05:10,600 --> 00:05:13,520
What is the reason why you killed me thus?

45
00:05:13,520 --> 00:05:17,080
What can your purpose be?

46
00:05:17,080 --> 00:05:26,800
What the what the elephant said was whose order was this whose order is this not what

47
00:05:26,800 --> 00:05:35,720
can your Baba's be but whose order is this who ordered you to kill me because the

48
00:05:35,720 --> 00:05:41,840
hunter was ordered by the queen to kill the elephant because the elephant has a task and

49
00:05:41,840 --> 00:05:48,240
it is said that the task has six kinds of ways so six colored tasks.

50
00:05:48,240 --> 00:05:55,480
That's why the elephant was called Chad Damda, charming six and Damda missed task but

51
00:05:55,480 --> 00:06:01,880
not six tasks but six colored tasks.

52
00:06:01,880 --> 00:06:08,080
And then he did not kill me although he could kill the the the the hunter easily he did

53
00:06:08,080 --> 00:06:15,600
not he packed his patience of forbearance and the next was the the great monkey but Buddha

54
00:06:15,600 --> 00:06:26,920
was reborn as a great monkey at that time so he he saves a man from a rocky pattern

55
00:06:26,920 --> 00:06:32,960
say from a rocky pit the man could not get out of it himself so the monkey saved him and

56
00:06:32,960 --> 00:06:39,560
then monkey after saving him the monkey was tired and so he said let me rest a little

57
00:06:39,560 --> 00:06:45,000
and let me put your but put my head on your lair and plus a little so when he was resting

58
00:06:45,000 --> 00:06:55,920
the man thought now this is food for human kind like other forest animals so why then

59
00:06:55,920 --> 00:07:04,120
surely hungry may not kill the ape to eat I'll travel independently actually I will

60
00:07:04,120 --> 00:07:10,680
travel satiated taking his meat as a provision thus I shall cross the waste and that will

61
00:07:10,680 --> 00:07:27,080
finish my fire tickum I don't know what this what so he struck the monkey with a big rock

62
00:07:27,080 --> 00:07:32,840
but the monkey was not killed he sees his head was broken and so he went up he climbed

63
00:07:32,840 --> 00:07:41,560
up the tree and said you are very ungrateful person but as I am as I'm a bodhisatta say

64
00:07:41,560 --> 00:07:47,600
I will not hurt you or I will not kill you I will even show you the way out of this wilderness

65
00:07:47,600 --> 00:07:58,880
so he jumped from from from three to three and his drop of blood from his body drop on the

66
00:07:58,880 --> 00:08:07,560
on the earth and following those drops of blood the man was able to get out of the waste

67
00:08:07,560 --> 00:08:17,600
or wilderness and then the next was he's serpentine so what he said I was reborn as a

68
00:08:17,600 --> 00:08:28,920
servant in this soda and he had power but he did not use his power to kill his tormentors

69
00:08:28,920 --> 00:08:39,240
is a buridatta and then the next is another servant in another life Buddha was reborn

70
00:08:39,240 --> 00:08:48,320
as a servant naga the same thing and then the next one for a draughtatifu is Sankapala

71
00:08:48,320 --> 00:09:00,280
Ryork naga again so Buddha was reborn as a snake for many many lives now there are some

72
00:09:00,280 --> 00:09:07,880
people who find it difficult to to accept that a person could die here and then reborn

73
00:09:07,880 --> 00:09:14,560
as a snake and if a human being could be reborn as a snake or as an animal because they

74
00:09:14,560 --> 00:09:23,280
were thinking of evolution but it is not evolution because it is just a person dies here

75
00:09:23,280 --> 00:09:33,400
and is reborn there as a result of his past his camera so it is possible now the other

76
00:09:33,400 --> 00:09:42,280
day I was reading a book about the first life and in that book the other said he requested

77
00:09:42,280 --> 00:09:51,720
vision to go back to former lives and then he was asking what did you see then and so on

78
00:09:51,720 --> 00:10:03,040
so in one life the division one bed and in one life he was said I am a dog naga so he

79
00:10:03,040 --> 00:10:10,080
remembered his life his past life as a dog or as a wolf there and it was reported in that

80
00:10:10,080 --> 00:10:23,760
book and in in Burma to there was a monk who said he had been a snake so as a snake he

81
00:10:23,760 --> 00:10:34,840
got a ball he ate eggs so one day when he was looking for a foreign ape the men saw him

82
00:10:34,840 --> 00:10:43,880
and so killed him with his fear but he he didn't die he didn't die quickly so he was

83
00:10:43,880 --> 00:10:53,520
put on a heap of rubbish and left to die and he died and he was reborn as the son of that man

84
00:10:53,520 --> 00:11:05,360
and when he when he when he grew up he he could remember his past life as a snake and and he

85
00:11:05,360 --> 00:11:15,240
he remembered how he suffered before he died after he was struck by a spear so he was so

86
00:11:15,240 --> 00:11:24,880
apprehensive so he was so afraid of suffering that he became a monk so that he may not be

87
00:11:24,880 --> 00:11:35,200
reborn as a snake again so such things can have an you can you can find people say who had

88
00:11:35,200 --> 00:11:42,680
been animals in their past lives some may not want to tell you or some just don't don't remember

89
00:11:42,680 --> 00:11:57,720
but there is people who had been reborn as as animals and remember those lives okay then so

90
00:11:57,720 --> 00:12:05,920
many stories and then the next one is Martu Posagar here is an elephant so now it is

91
00:12:05,920 --> 00:12:10,480
in the highest degree improper and I'm becoming to you to around thoughts of recent

92
00:12:10,480 --> 00:12:19,280
man since you are emulating as your master that blessed one now that the poly work means you

93
00:12:19,280 --> 00:12:26,000
are pointing to him as your master then you are referring to him as your master not

94
00:12:26,000 --> 00:12:41,120
emulating you you are claiming that Buddha was your master so we should review the special

95
00:12:41,120 --> 00:12:48,600
qualities of the masters from a conduct next next review the so does that deal with the

96
00:12:48,600 --> 00:13:01,200
beginninglessness of the round of rebirds now buildings have lives in the past and they

97
00:13:01,200 --> 00:13:08,960
would have life in the future and this series of lives is called a round of rebirds and

98
00:13:08,960 --> 00:13:18,640
nobody knows or nobody can tell when this round of rebirds begin so that is the round of

99
00:13:18,640 --> 00:13:25,680
rebirds beginning less so there is no beginning although we know that there is a mirror

100
00:13:25,680 --> 00:13:31,920
because we we are in the middle of this round of rebirds so there will be many more rebirds

101
00:13:31,920 --> 00:13:40,480
and there have been millions of rebirds in the first but we do not know or the beginning

102
00:13:40,480 --> 00:13:50,960
of it or the beginning of it cannot be known so beginning less in this beginning less round

103
00:13:50,960 --> 00:13:58,160
of rebirds the person who you are angry with having your father your mother your brother

104
00:13:58,160 --> 00:14:04,480
your sister your son and so on so Buddha said it is very difficult to find a person in

105
00:14:04,480 --> 00:14:09,040
this samsara who had not been your father your mother and so on so if he might be your

106
00:14:09,040 --> 00:14:14,400
father in your past life or your mother in your past life and so it is not proper for you

107
00:14:14,400 --> 00:14:27,240
to be angry with him and so on now the reference given there is S218990 so that is

108
00:14:27,240 --> 00:14:37,160
sainu tanikaya now sainu tanikaya is translated as kintrad sayings so kintrad sayings

109
00:14:37,160 --> 00:14:51,400
volume 2 page 128 these references are to paritaxes so if you want to read if English translation

110
00:14:51,400 --> 00:15:03,720
then kintrad sayings volume 2 page 128 the next is review the advantages of loving kindness

111
00:15:04,280 --> 00:15:12,040
so there are 11 advantages to be to be gained from the practice of loving kindness and they will

112
00:15:12,040 --> 00:15:21,560
be explained in detail later towards the end of the chapter towards the end of loving kindness meditation

113
00:15:21,560 --> 00:15:27,800
so if you are angry and if you do not love practice loving kindness meditation you will not get these advantages

114
00:15:27,800 --> 00:15:44,680
and then next one that is resolution into elements so resolve that person into elements

115
00:15:47,880 --> 00:15:54,120
now you have gone forth into homelessness when you are angry with him what is it you are

116
00:15:54,120 --> 00:16:00,600
angry with is it had hair you are angry with or body hairs or nails or teeth skin and so on so you as

117
00:16:00,600 --> 00:16:07,640
yourself are you angry with your hair and something like that so resolution into elements

118
00:16:10,760 --> 00:16:20,120
then the next yeah and here not only elements say I always say according to this

119
00:16:20,120 --> 00:16:27,800
cutting into pieces so if you are familiar with the teachings of aggregate

120
00:16:29,000 --> 00:16:36,040
then you get you may say are you familiar with the I mean I am angry with the material aggregate

121
00:16:36,040 --> 00:16:46,120
or formation aggregate consciousness aggregated so on the last one is try the giving of a gift

122
00:16:46,120 --> 00:16:55,800
so if you are angry with the button give him a gift it can either be given by himself to the

123
00:16:55,800 --> 00:17:03,240
other or accepted by himself from the other so you may accept the given given you by him or you

124
00:17:03,240 --> 00:17:11,000
may give him a gift you may give him a present because when when somebody accepted present

125
00:17:11,000 --> 00:17:20,520
then his man becomes soft so you may be able to to get rid of resentment and a story is given here

126
00:17:22,040 --> 00:17:26,760
as happened you see near Elder who received a bowl given to him at the titular papa

127
00:17:26,760 --> 00:17:32,120
morna straight by an arms foot either Elder who had been three times made to move from his

128
00:17:32,120 --> 00:17:37,400
lodging by him and who presented it with these words one of us are this bull what it

129
00:17:37,400 --> 00:17:43,880
you get was given me by my mother who is a lead devotee and it is rightly obtained that the good

130
00:17:43,880 --> 00:17:59,000
lead devotee acquire merit yeah the other man was maybe resentful monk so he he made he made the

131
00:17:59,000 --> 00:18:06,840
other monk give up his give up his lodging three times you know when you are living in a

132
00:18:06,840 --> 00:18:16,120
monastery which is a property of the sangha that you are given in place to live say maybe a small

133
00:18:16,120 --> 00:18:24,440
room but if if a monk comes who is senior to you and if there are no other places that you

134
00:18:24,440 --> 00:18:31,720
give to him then you have to give your place to that monk because he is senior to you he is older

135
00:18:31,720 --> 00:18:41,000
than you so that monk when they are and get his place for three times but out I think out of

136
00:18:41,000 --> 00:18:50,280
malice so the other monk said there are the monk donated he he pulled to him and say and this

137
00:18:50,280 --> 00:18:55,880
bowl what eight you get I don't know how much they was was given me by my mother who is a lead

138
00:18:55,880 --> 00:19:04,360
devotee and so on so he gives for training the untamed he gives for every kind of good or they

139
00:19:04,360 --> 00:19:11,560
give at giving accomplishes everything that is what you said yeah by giving you can do anything

140
00:19:11,560 --> 00:19:26,840
something like that but we should not we should not misapply this because especially in our countries

141
00:19:26,840 --> 00:19:36,760
people say giving accomplishes everything so giving a pride so once a car was stopped by

142
00:19:36,760 --> 00:19:48,440
policemen in this country the the grandchild was driving and there was her grandmother so the

143
00:19:48,440 --> 00:19:55,080
grandmother told said to her give him some some some pocket money give him some a bride

144
00:19:55,080 --> 00:20:15,800
and the at the concert crema it is not power you cannot do that here but it's it's good to

145
00:20:16,440 --> 00:20:22,680
to give something to the person to get rid of resentment now the breaking down of the barriers

146
00:20:22,680 --> 00:20:28,520
when his reason and towards that host of person has been thus elite then he can turn his mind

147
00:20:28,520 --> 00:20:34,680
with loving kindness towards that person too just as towards the one with the and so on so

148
00:20:37,480 --> 00:20:45,000
after reflecting in different ways and you'll be able to get rid of resentment then practice towards

149
00:20:45,000 --> 00:20:54,520
him too until the the barriers are broken and the characteristic of it is this I think

150
00:20:56,920 --> 00:21:06,360
can say indication of this that means you may just from this when the barriers are broken and when

151
00:21:06,360 --> 00:21:15,480
they are not so the party what is la cana la cana is usually translated as characteristic but sometimes

152
00:21:16,600 --> 00:21:24,120
I think it may be translated differently so indication may be better here the indication of it is this

153
00:21:25,080 --> 00:21:31,000
and then there are four persons and so on and the robbers come and us to give one person for

154
00:21:31,000 --> 00:21:40,440
sacrifice and if you can give yourself or any other person you have not broken down the barriers

155
00:21:40,440 --> 00:21:51,000
only one you cannot give anyone are the barriers broken because if you say take me to be killed

156
00:21:51,000 --> 00:22:01,240
do not take them then you are not impartial you you want your own or the call destruction and that

157
00:22:01,240 --> 00:22:09,000
is impartiality that is partiality that towards others so you have not broken the barriers if you

158
00:22:09,000 --> 00:22:21,400
see that the barriers are broken only one you say do not take any persons you know you you see

159
00:22:22,680 --> 00:22:29,720
other three persons as yourself and so you do not do you not find anybody to be given

160
00:22:29,720 --> 00:22:37,480
for the sacrifice only then the barrier is broken and the barriers are broken that means you see them

161
00:22:37,480 --> 00:22:46,360
equally impartiality these four kinds of persons after the barriers are broken or when the barriers

162
00:22:46,360 --> 00:22:54,520
are broken then you are said to gain the signing and access here there is no no special

163
00:22:54,520 --> 00:23:01,000
gaining of excess concentration but when these barriers are broken you are said to gain excess

164
00:23:01,000 --> 00:23:11,720
concentration and from excess concentration you go on until you get Jana at this point he has a

165
00:23:11,720 --> 00:23:17,320
chain the first Jana which abandoned five factors and so on and then text and commentary

166
00:23:17,320 --> 00:23:23,080
now comes the text given in the suit as

167
00:23:30,040 --> 00:23:41,720
and in the explanation of the words and how to practice meditation there is a method of

168
00:23:41,720 --> 00:23:55,240
practice of meditation given according to what is taught in patism beta and that is five hundred

169
00:23:55,240 --> 00:24:05,560
and twenty two ways of practicing beta so please look at the end of today and this is how we

170
00:24:05,560 --> 00:24:12,440
practice beta and five hundred and twenty eight ways first if you look at the first column there

171
00:24:12,440 --> 00:24:20,440
are twelve kinds of persons to whom we send thoughts of loving kindness and the column two are

172
00:24:20,440 --> 00:24:29,080
the ten directions four cardinal directions and four corners and down and up so ten ten

173
00:24:29,080 --> 00:24:39,800
directions and column three are the four kinds of the four modes of loving kindness say baby free

174
00:24:39,800 --> 00:24:46,040
from n-m-t may be free from displeasure may be free from affection and may be this happy or

175
00:24:46,040 --> 00:24:52,520
may they keep themselves happy so in this in these four ways you practice what person and

176
00:24:52,520 --> 00:24:57,400
and we can practice

177
00:25:01,640 --> 00:25:09,720
meta following this chart and five hundred and twenty eight ways now if you look at me

178
00:25:09,720 --> 00:25:24,760
oh if you look at the column one say one two three four and five the space should be

179
00:25:24,760 --> 00:25:30,040
after five and below five and not between four and five so one two three four and five

180
00:25:30,040 --> 00:25:42,920
are called and nonspecific perversion and from six to 12 a specific perversion

181
00:25:45,960 --> 00:25:51,080
because when you say may all beings we mean all beings and may all breathing beings also

182
00:25:51,080 --> 00:25:56,120
mean all beings may all creatures mean all beings may all persons all beings and may all

183
00:25:56,120 --> 00:26:04,200
those who every personality also mean all beings so one two three four and five mean just all beings

184
00:26:05,080 --> 00:26:12,200
without specification and then from six onwards we say may all women in that case no man

185
00:26:12,200 --> 00:26:20,840
may all men no women and so on so they are specific so five five unspecific and seven specific

186
00:26:20,840 --> 00:26:28,680
and then when we practice loving kind of meditation we combine these three in different ways so

187
00:26:29,960 --> 00:26:37,800
first we say may all beings be free from entity free from displeasure free from affection and live

188
00:26:37,800 --> 00:26:45,400
happy and then may all breathing things be free from entity free from displeasure free from

189
00:26:45,400 --> 00:26:52,920
affliction free and live happy and so on and so on we come to 12 may all in states of laws

190
00:26:52,920 --> 00:26:58,520
be free from entity free from displeasure free from separation and free from and live happily

191
00:26:58,520 --> 00:27:09,720
and then next we combine it with directions so here in Pali the word for direction come first

192
00:27:09,720 --> 00:27:20,120
but in English it comes later so it's a little a little strange here so you say may all beings

193
00:27:20,120 --> 00:27:28,040
in the Eastern direction be where be free from entity and so on and may all breathing things

194
00:27:28,040 --> 00:27:33,560
in the Eastern direction may all creatures in the Eastern direction may all persons in the Eastern

195
00:27:33,560 --> 00:27:46,200
direction and so on so following this we practice 48 nonspecific and 480 specific so all together

196
00:27:46,200 --> 00:27:54,280
we have five hundred and twenty eight ways of practicing media and we have practiced this

197
00:27:54,280 --> 00:28:07,400
a number of times here in this country and it takes about 45 minutes or maybe 50 minutes

198
00:28:07,400 --> 00:28:19,080
now it's nine o'clock

199
00:28:19,080 --> 00:28:36,600
on page three hundred and thirty six on top of the page there's a explanation of the word sata

200
00:28:36,600 --> 00:28:48,440
being is called sata because any craving for it has helped it has gripped it that is why

201
00:28:48,440 --> 00:28:59,000
it being is said so sata of being is a being is called sata because he is attached to his

202
00:28:59,000 --> 00:29:07,400
life or something like that now but in ordinary speech this term of common usage is applied

203
00:29:07,400 --> 00:29:13,400
also to those who are without greed just as the term of common usage pound fan

204
00:29:14,440 --> 00:29:20,440
along that is used for different sorts of fans in general even if made of split bamboo now

205
00:29:21,560 --> 00:29:27,400
a being is called sata and why is he called sata because he has attachment to things

206
00:29:27,400 --> 00:29:35,000
then what are called calling an arachan is sata arachan is also called being arachan is

207
00:29:35,000 --> 00:29:42,360
the person is saying who has eradicated called mender defalment so arachan and arachan is free from

208
00:29:42,360 --> 00:29:49,080
all mender with mental defalment he has no attachment at all but still he is called sata

209
00:29:50,120 --> 00:29:55,480
so the the commentary explains here in ordinary speech this term of common usage is applied

210
00:29:55,480 --> 00:30:05,560
also to those who are without greed although literally sata means it was in who is who has

211
00:30:05,560 --> 00:30:14,600
attachment it is applied to both to being who has attachment who has who have attachment who has

212
00:30:14,600 --> 00:30:26,520
no and the other one one example the pali was dala vanta palm fan so dala vanta means

213
00:30:26,520 --> 00:30:36,920
a fan made of palm leaves but this this this what is used for other fans as well whether they

214
00:30:36,920 --> 00:30:45,480
are made of palm leaves or not so I mean in these days they are they are made by the people or some

215
00:30:48,040 --> 00:30:56,040
split of bamboo still they are called dala vanta and pali so in the same way if it wasn't

216
00:30:56,040 --> 00:31:03,160
is called sata it was in who has attachment is called sata and it wasn't who has not attachment

217
00:31:03,160 --> 00:31:09,160
is also called sata can you can you give me an example of some example in English

218
00:31:11,400 --> 00:31:13,000
you know about what is meant yeah

219
00:31:18,200 --> 00:31:28,120
sometimes they use well used to choose toward men to being men and women you know they could be

220
00:31:28,120 --> 00:31:37,560
I think that may be different what would I know this there's a there's a town called 29 pounds

221
00:31:39,640 --> 00:31:48,840
via palm spring there may be more than 29 pounds now or less than 29 pounds now but the city is

222
00:31:48,840 --> 00:31:58,040
still called 29 pounds oh I look I look at the pen the what pen in the dictionary what is the

223
00:31:58,040 --> 00:32:10,600
meaning of pen original meaning it comes from friends and from legend it is the meaning is feather

224
00:32:11,880 --> 00:32:24,200
so at first it is hands or middle of feather yeah quill but now pens are not made of feathers

225
00:32:24,200 --> 00:32:29,880
or quill but you still call it pen so it is a usage

226
00:32:33,480 --> 00:32:42,200
and next in paragraph 54 that's an interesting word pugula in the middle of the paragraph

227
00:32:43,080 --> 00:32:50,280
boom is what hair is called actually it is put in Sanskrit in UT food

228
00:32:50,280 --> 00:33:01,880
and they fall into that is a meaning now put color the poly work and Sanskrit word is P-U-D T-A-L-A

229
00:33:01,880 --> 00:33:14,600
put color this word is interesting because it reflects a sudden belief in Hinduism

230
00:33:14,600 --> 00:33:25,160
according to Hinduism you will go to hell after death if you do not have offsprings if you do

231
00:33:25,160 --> 00:33:30,600
not have sons and daughters if you die without having a abandoned order you will go to hell

232
00:33:32,280 --> 00:33:37,000
that is why Brahmins all of this point of the month you have you have those who are going to

233
00:33:37,000 --> 00:33:44,760
have because you have no family you have no offsprings so this is the brahmins belief that is why

234
00:33:44,760 --> 00:33:50,440
they have to be householder for sons years even though they want to become months they have to

235
00:33:50,440 --> 00:33:57,480
be householder for sons years and have a family and then leave the family and become a month so those

236
00:33:57,480 --> 00:34:08,760
who have no sons or daughters will go to hell now pugula means those who are going to have

237
00:34:10,680 --> 00:34:17,800
so such presence but later on the word pugula is applied to every every being and then the word

238
00:34:17,800 --> 00:34:29,880
for for a son is putra in Sanskrit putra tramins to to protect so putra means a person who protects

239
00:34:29,880 --> 00:34:37,320
you from hell because when you have a son you will you won't go to hell so your protectors from

240
00:34:37,320 --> 00:34:50,600
hell so the son is a person who protects you from going to hell so he is called putra

241
00:34:50,600 --> 00:34:59,880
that's right yes but later on it just means a son because it is a Hindu belief but the word

242
00:34:59,880 --> 00:35:15,400
itself is used by Hindus as well as Buddhists and then I think the others you can even breathe by

243
00:35:15,400 --> 00:35:30,440
yourself do the end of the mitta that is three hundred forty I mean they are not that in nine that is a yes

244
00:35:30,440 --> 00:35:47,560
it makes me another thirty pages also we should read it up to where let me say three hundred sixty

245
00:35:49,400 --> 00:35:57,480
three fifty four that's one that's the next chapter yeah but we were to go to the chapter two

246
00:35:57,480 --> 00:36:08,440
okay so about three hundred sixty hour yes and end of the first in material jam

247
00:36:08,440 --> 00:36:25,480
yeah so that that that chapter has must be good with the vidam

