1
00:00:00,000 --> 00:00:24,600
Now I'll give a short talk explaining what today means and a little bit about the purpose

2
00:00:24,600 --> 00:00:34,800
of this sort of ceremony.

3
00:00:34,800 --> 00:00:47,400
So our understanding is that 2600 years ago, according to Sri Lankan reckoning on this

4
00:00:47,400 --> 00:01:03,120
very day, 2600 years ago, exactly, the Buddha first gave his first teaching.

5
00:01:03,120 --> 00:01:06,760
And for those of you who are not familiar with the story of the Buddha, I'll just give

6
00:01:06,760 --> 00:01:11,120
a little bit of background here.

7
00:01:11,120 --> 00:01:20,240
So the man who was to be the Buddha, many people know this much about the story, he was

8
00:01:20,240 --> 00:01:22,640
a prince.

9
00:01:22,640 --> 00:01:35,120
And at the age of 29, he got fed up with the meaninglessness of luxury and the life as

10
00:01:35,120 --> 00:01:42,280
a prince in a royal household.

11
00:01:42,280 --> 00:01:50,520
And so he decided to leave home in order to seek out some answers to these questions,

12
00:01:50,520 --> 00:01:58,600
why we have to be born, why are we born, why do we get old, why do we get sick, why

13
00:01:58,600 --> 00:02:00,560
do we die?

14
00:02:00,560 --> 00:02:09,080
What's the point of it all? And how to overcome this? What do we have, what is it that

15
00:02:09,080 --> 00:02:19,880
we have to do? What is the way to find freedom from suffering?

16
00:02:19,880 --> 00:02:32,080
And so he spent six years trying in vain to find an answer, a way out of suffering, to

17
00:02:32,080 --> 00:02:36,880
find a real answer to the problems in life.

18
00:02:36,880 --> 00:02:41,880
For his whole prince life, it was the understanding that the way out of suffering is to

19
00:02:41,880 --> 00:02:51,120
find more and more pleasure to engage in pleasant experiences and find ways to avoid unpleasant

20
00:02:51,120 --> 00:02:52,120
experiences.

21
00:02:52,120 --> 00:03:01,400
And after 29 years of living this way, he realized that this wasn't sufficient, that as

22
00:03:01,400 --> 00:03:09,640
I've talked about before, you can only escape unpleasant situations for so long.

23
00:03:09,640 --> 00:03:22,920
And you realize that there's no way to escape old age, sickness, and death, then these

24
00:03:22,920 --> 00:03:31,840
answers, the answer that is seeking out central pleasure is not sufficient.

25
00:03:31,840 --> 00:03:38,840
The medicine that isn't strong enough for the sickness, there's no amount of pleasure

26
00:03:38,840 --> 00:03:45,640
which can take away the reality of old age sickness and death, and in fact, in some ways

27
00:03:45,640 --> 00:03:54,440
it makes these things worse because it creates partiality and addiction and aversion.

28
00:03:54,440 --> 00:03:59,800
And so he realized that there was no amount of money or luxury, no amount of power or influence

29
00:03:59,800 --> 00:04:05,520
that could overcome these things, that could stop these things, you can't buy your way out

30
00:04:05,520 --> 00:04:12,960
of death, and you can't survive, none of the luxury or power or influence that you have

31
00:04:12,960 --> 00:04:26,320
in this life survives with you after your death, you have to give it all up, 100%.

32
00:04:26,320 --> 00:04:32,160
So he spent six years trying to find a better answer.

33
00:04:32,160 --> 00:04:39,440
And it was fruitless because he didn't have a clear idea of where to look.

34
00:04:39,440 --> 00:04:49,800
The story goes, or the commentary to the story, is that he had in a past life, because

35
00:04:49,800 --> 00:04:55,200
it took many, many lifetimes for him to develop his mind to the state where he was

36
00:04:55,200 --> 00:05:03,200
able to even come to these realizations and leave home without a teacher, without anyone's

37
00:05:03,200 --> 00:05:10,760
guidance, to do it on his own, especially from such a wonderful, pleasurable lifestyle,

38
00:05:10,760 --> 00:05:12,800
something very difficult.

39
00:05:12,800 --> 00:05:23,720
But in one of his past lifetimes, some long, long time ago, he saw some, or he saw

40
00:05:23,720 --> 00:05:30,840
a enlightened being, someone who had done the same thing and gone and become, come to

41
00:05:30,840 --> 00:05:36,360
realize the truth and come to find a way out of suffering.

42
00:05:36,360 --> 00:05:38,520
And he was an ignorant person at the time.

43
00:05:38,520 --> 00:05:44,280
And so he said about that person, he said, oh, they were torturing themselves.

44
00:05:44,280 --> 00:05:47,120
This person was torturing himself.

45
00:05:47,120 --> 00:05:54,920
And he said, so he said something nasty to this enlightened being, useless, no good, good

46
00:05:54,920 --> 00:06:00,040
for nothing, doing something that's torturing yourself.

47
00:06:00,040 --> 00:06:04,480
And they say, that's the reason, because he had set himself on this idea that a person

48
00:06:04,480 --> 00:06:07,720
who leaves home is torturing himself.

49
00:06:07,720 --> 00:06:14,200
When it finally came time for him to leave home, he had this wrong idea that torturing

50
00:06:14,200 --> 00:06:17,000
himself was the right way out of suffering.

51
00:06:17,000 --> 00:06:20,600
And in fact, this was a common belief at the time of the Buddha.

52
00:06:20,600 --> 00:06:29,000
This was the belief of people in India at the time that the way out of suffering was

53
00:06:29,000 --> 00:06:33,000
to burn up your attachments.

54
00:06:33,000 --> 00:06:43,920
So if you're attached to things that are pleasant, then you should treat yourself

55
00:06:43,920 --> 00:06:50,520
to be content with unpleasantness, by creating unpleasant situations, making it difficult

56
00:06:50,520 --> 00:06:58,200
for yourself, creating suffering, because this would offset the pleasure that you have,

57
00:06:58,200 --> 00:07:06,640
the attachment that you have to pleasure.

58
00:07:06,640 --> 00:07:11,440
And this is actually quite reasonable, and it's actually something that we do often.

59
00:07:11,440 --> 00:07:15,600
And if we haven't left the home life, we feel guilty about our pleasures.

60
00:07:15,600 --> 00:07:24,000
So we torture ourselves, people who work very hard will often have this idea that they

61
00:07:24,000 --> 00:07:27,080
want this and want that, and they feel guilty about it.

62
00:07:27,080 --> 00:07:33,640
And so they think the right thing to do is to work very, very hard and make trouble for

63
00:07:33,640 --> 00:07:35,640
yourself, really.

64
00:07:35,640 --> 00:07:41,800
And they'll say how people who just indulge in pleasure are lazy and so on, and they'll

65
00:07:41,800 --> 00:07:46,160
develop this idea that you have to, if you want to be a good person, you have to work,

66
00:07:46,160 --> 00:07:49,960
you need a good work ethic, which is really pretty silly.

67
00:07:49,960 --> 00:07:57,920
It's something that is driving our world, and there are two driving forces in the world.

68
00:07:57,920 --> 00:08:03,920
One is our incessant quest for pleasure.

69
00:08:03,920 --> 00:08:10,920
And the other is this idea that somehow we have to work, somehow we have to develop and

70
00:08:10,920 --> 00:08:13,400
advance.

71
00:08:13,400 --> 00:08:19,720
And so they really go hand in hand, the idea that the best thing we can do is indulge

72
00:08:19,720 --> 00:08:26,240
and the idea that the best thing we can do is develop or create, the best thing we can do

73
00:08:26,240 --> 00:08:27,240
is work.

74
00:08:27,240 --> 00:08:31,920
And so we engage in all sorts of silly works to create things and thinking that somehow

75
00:08:31,920 --> 00:08:34,880
this is a morally right thing.

76
00:08:34,880 --> 00:08:40,920
There was a famous philosopher in America, Henry David Thoreau, who was very critical

77
00:08:40,920 --> 00:08:42,600
of this idea.

78
00:08:42,600 --> 00:08:52,120
These people who work, he said, he told the story as something about this man who had

79
00:08:52,120 --> 00:08:58,040
an ox pull a stone across the field and then later on pull the same stone back to the

80
00:08:58,040 --> 00:08:59,920
other side of the field.

81
00:08:59,920 --> 00:09:08,280
This is basically what we do is we engage in useless works and thinking that somehow it

82
00:09:08,280 --> 00:09:11,920
builds character to torture yourself.

83
00:09:11,920 --> 00:09:14,040
And so the Buddha did this intensively.

84
00:09:14,040 --> 00:09:17,640
This was the idea at the time that somehow it had spiritual benefit.

85
00:09:17,640 --> 00:09:20,400
It actually may have some limited spiritual benefit.

86
00:09:20,400 --> 00:09:27,920
I know the Buddha said it was useless or not connected with what is right, but it actually

87
00:09:27,920 --> 00:09:37,440
may have the preliminary benefit of opening your mind up to more experiences.

88
00:09:37,440 --> 00:09:46,400
So it could very well bring the patience and the forbearance and the ability to tolerance.

89
00:09:46,400 --> 00:09:50,400
So it could be useful for meditation in a preliminary way.

90
00:09:50,400 --> 00:09:55,440
And in this sense we all have to go through this phase because when we first come to meditate

91
00:09:55,440 --> 00:10:01,360
it can be torture, having to sit still, having to sit cross-legged, having to eat only

92
00:10:01,360 --> 00:10:07,560
in the morning and so on, can be quite difficult because our tolerance is quite low in

93
00:10:07,560 --> 00:10:17,560
the beginning and our state of being is quite out of balance.

94
00:10:17,560 --> 00:10:21,240
And so just to create balance we often have to go through some suffering, just the tension

95
00:10:21,240 --> 00:10:26,800
in the body for example, many people come to meditate and just sitting still creates tension

96
00:10:26,800 --> 00:10:35,440
in the shoulders, tension in the back because our minds are tense, our minds are stress

97
00:10:35,440 --> 00:10:38,760
in the state of stress.

98
00:10:38,760 --> 00:10:43,400
So in some sense we do have to go through a little bit of torture in the beginning, but

99
00:10:43,400 --> 00:10:52,360
we should never have the idea that somehow this is the way out of suffering when some people

100
00:10:52,360 --> 00:10:58,960
come to meditate and they think the idea is to experience suffering and they will score

101
00:10:58,960 --> 00:11:03,400
in those people who meditate and find peace and happiness and bliss.

102
00:11:03,400 --> 00:11:08,960
On the other hand some people come to meditate and think that it has to be a state of bliss

103
00:11:08,960 --> 00:11:13,840
and peace and happiness and if you are not feeling calm and peaceful then something is

104
00:11:13,840 --> 00:11:15,080
wrong with your meditation.

105
00:11:15,080 --> 00:11:21,000
Both of these are wrong understandings and it leads back to the two extremes.

106
00:11:21,000 --> 00:11:27,240
So this is the context that we come into for the Buddha's first teaching.

107
00:11:27,240 --> 00:11:33,240
Two months prior he had become enlightened and he spent two months walking, that's not

108
00:11:33,240 --> 00:11:38,400
true, he spent wait a second, let's get this right, May, June, July.

109
00:11:38,400 --> 00:11:50,840
So two months and he spent 49 days reflecting on his enlightenment and living just in the

110
00:11:50,840 --> 00:11:54,280
bliss and peace and happiness of enlightenment.

111
00:11:54,280 --> 00:12:05,280
And then the last day is what is at that, actually not many days, we have 49 days which

112
00:12:05,280 --> 00:12:14,040
is 30, 19, so only 11 days or something, I hope I have got this right.

113
00:12:14,040 --> 00:12:23,240
That he spent walking to find these five recklessness because what happened was he had

114
00:12:23,240 --> 00:12:27,760
been torturing himself for six years and then he realized it was useless, he realized

115
00:12:27,760 --> 00:12:33,800
that if he tortured himself any further he would die, he couldn't go any further, he

116
00:12:33,800 --> 00:12:39,160
had done everything, he had stopped eating food, he had even stopped breathing, he had

117
00:12:39,160 --> 00:12:43,480
tortured himself in every way possible and he said there is no other, I can't go any farther

118
00:12:43,480 --> 00:12:49,880
with this and yet still I have no wisdom in understanding, this is the key because many

119
00:12:49,880 --> 00:12:56,960
people will practice meditation and gain this or gain that and never gain any wisdom

120
00:12:56,960 --> 00:13:02,120
or understanding about themselves or about reality and this is a wake up cause, should

121
00:13:02,120 --> 00:13:05,840
be a wake up call for all of us, for those of us who sit in meditation and find great

122
00:13:05,840 --> 00:13:12,240
peace or happiness and also for those of us who sit in meditation and torture ourselves.

123
00:13:12,240 --> 00:13:17,240
That if we are not really gaining wisdom and understanding then there is something wrong

124
00:13:17,240 --> 00:13:23,560
and so the Buddha said maybe there is another way and so he tried to allow these peaceful

125
00:13:23,560 --> 00:13:31,800
and happy feelings to arise, he went in eight food and brought his body back to a

126
00:13:31,800 --> 00:13:39,320
state of normality where it wasn't always groaning and complaining to him and then he

127
00:13:39,320 --> 00:13:48,320
started practicing and allowed peaceful and happy feelings to come up and allowed the natural

128
00:13:48,320 --> 00:13:55,480
state of the mind to arise and it was quite peaceful and happy and so he would, he watched

129
00:13:55,480 --> 00:13:59,840
this and he watched everything that came up and he came to understand how attachment

130
00:13:59,840 --> 00:14:04,960
works and this is what I have been saying many times is that if you want to understand

131
00:14:04,960 --> 00:14:10,800
attachment you have to let the pleasure come up, you have to let the desire come up.

132
00:14:10,800 --> 00:14:16,200
If you just repress them all the time then you are not going to be able to see the truth.

133
00:14:16,200 --> 00:14:21,200
What we want to understand is our addictions and how to overcome them and if we don't

134
00:14:21,200 --> 00:14:26,800
let them arise then we will never be able to understand and overcome them, we will just

135
00:14:26,800 --> 00:14:32,320
always be repressing them. Once we understand the nature of the object of our addiction

136
00:14:32,320 --> 00:14:37,480
and the nature of the addiction itself then we will be able to overcome it, we will

137
00:14:37,480 --> 00:14:42,040
be able to give it up. This is the path to enlightenment. So the last thing the Buddha

138
00:14:42,040 --> 00:14:50,360
realized was that everything that arises arises based on the cause and he saw the cause

139
00:14:50,360 --> 00:14:55,240
of suffering and he saw the way out of suffering, he saw that suffering is only caused

140
00:14:55,240 --> 00:15:04,840
by craving and clinging and he saw how it works, that there was no good reason to cling

141
00:15:04,840 --> 00:15:12,040
to anything, that it was the clinging itself that was causing the suffering when ordinarily

142
00:15:12,040 --> 00:15:15,440
we think that there are certain things that when we cling to them they are going to bring

143
00:15:15,440 --> 00:15:20,680
this happiness and so we think the way out of suffering is to just arrange things so that

144
00:15:20,680 --> 00:15:24,720
we only get those things that are worth clinging to. The Buddha realized that it is the

145
00:15:24,720 --> 00:15:36,240
clinging itself that leads to suffering, nothing to do with the object and but the point

146
00:15:36,240 --> 00:15:42,120
is that everyone else already thought that or still thought that the way out of suffering

147
00:15:42,120 --> 00:15:49,560
was to torture yourself and so these five followers of the Bodhisattva, the Buddha to

148
00:15:49,560 --> 00:15:57,960
be became quite disenchanted with him and lost all their faith in him when he started

149
00:15:57,960 --> 00:16:02,360
taking food again and stopped torturing himself so they thought he had given up the

150
00:16:02,360 --> 00:16:15,360
path, given up the practice and so they left and so now the Buddha went back to find

151
00:16:15,360 --> 00:16:23,400
them and this is where he gave the first teaching. And the first teaching started on

152
00:16:23,400 --> 00:16:32,960
this day, this is what this day represents today, starts with these two extremes, some

153
00:16:32,960 --> 00:16:36,680
people who read this don't quite understand with the context of this teaching but this

154
00:16:36,680 --> 00:16:42,240
is the real context, the point that the Buddha had come to, the realization that these

155
00:16:42,240 --> 00:16:49,480
two extremes were useless and the fact that these five recklessness still thought that

156
00:16:49,480 --> 00:16:53,320
it was in terms of these two extremes that one extreme was the right way, one extreme was

157
00:16:53,320 --> 00:16:57,600
the wrong, Buddha said both extremes of the wrong way, they thought he had gone back to the

158
00:16:57,600 --> 00:17:05,840
other extreme of indulgence and he said no, actually both of these, it's not that

159
00:17:05,840 --> 00:17:12,000
I think that pleasure is the right extreme and pain is the wrong extreme, that both extremes

160
00:17:12,000 --> 00:17:19,080
are useless, engaging in central pleasure leads only to addiction and attachment and

161
00:17:19,080 --> 00:17:26,640
partiality but engaging in torturing yourself is also just repressing and avoiding the nature

162
00:17:26,640 --> 00:17:33,280
of clinging. We don't have a problem with unpleasant things, we don't want them, we're

163
00:17:33,280 --> 00:17:38,920
not going to cling to unpleasant things, we have to look at the pleasant things that

164
00:17:38,920 --> 00:17:43,960
we cling to and as once we don't cling to anything then we'll have no partiality and

165
00:17:43,960 --> 00:17:53,920
the unpleasant things will no longer be unpleasant, we make no comparison and so this

166
00:17:53,920 --> 00:18:02,800
is what we are undertaking here, we are taking the first step and we take this step again

167
00:18:02,800 --> 00:18:08,400
and again when we take the precepts, it's a reaffirmation that we have started on the path

168
00:18:08,400 --> 00:18:16,880
and that we too want to come to see the truth of reality and want to overcome our attachment

169
00:18:16,880 --> 00:18:28,080
and the first step is to focus our energies and to focus our activities in terms of

170
00:18:28,080 --> 00:18:35,360
those activities which will bring clarity of mind and wisdom and understanding while avoiding

171
00:18:35,360 --> 00:18:40,360
those activities that are going to cloud the mind and so this is where we start with these

172
00:18:40,360 --> 00:18:49,880
five or eight precepts with the morality, teaching of morality. The three refuges is simply

173
00:18:49,880 --> 00:18:58,040
an observation, a determination that we are going to practice, this is like saying I am

174
00:18:58,040 --> 00:19:03,240
committing myself to this path, the Buddha is my teacher, the Dhamma is the teaching and

175
00:19:03,240 --> 00:19:11,160
following and the Sangha are my support group, the other Buddhist prag meditators and

176
00:19:11,160 --> 00:19:17,240
practitioners, I'm going to take these three things and there's a note here that we really

177
00:19:17,240 --> 00:19:23,560
should take this seriously, that we should think of the Buddha often, the Buddha recommended

178
00:19:23,560 --> 00:19:27,640
to think of him and we have Buddha images and we do frustrations in front of the Buddha

179
00:19:27,640 --> 00:19:32,760
image and it's not because of worship or so and it's just a relationship we have with

180
00:19:32,760 --> 00:19:38,080
the Buddha, someone who reminds us of good things, it's not God, it's not someone who's

181
00:19:38,080 --> 00:19:42,360
going to answer our prayers but it really does have a strong effect on your mind, this

182
00:19:42,360 --> 00:19:49,600
is why theistic religions really have a powerful effect on people's minds because it

183
00:19:49,600 --> 00:19:55,040
creates faith and confidence. So if we channel those good, essentially good qualities of

184
00:19:55,040 --> 00:20:00,680
mind in a way that is actually meaningful, there's the practice to gain wisdom and

185
00:20:00,680 --> 00:20:06,240
understanding, then this faith will have a profound effect on our practice, this confidence

186
00:20:06,240 --> 00:20:10,960
that we have thinking yes, we have this great teacher who taught these wonderful things

187
00:20:10,960 --> 00:20:19,480
and he was right and you're feeling good and confident about that and when we have doubts,

188
00:20:19,480 --> 00:20:24,360
we just think of how hard the Buddha had to work and we think of the sacrifice he made

189
00:20:24,360 --> 00:20:31,160
and we think of his example that it is possible and his reassurance that someone can

190
00:20:31,160 --> 00:20:36,000
practice in this way. So we should think about the Buddha, the Dhamma is something that

191
00:20:36,000 --> 00:20:39,840
we should think about, we should study the Dhamma, the Dhamma is the teaching, so we should

192
00:20:39,840 --> 00:20:44,000
take this seriously and we should actually spend time reading the Buddha's teaching, listening

193
00:20:44,000 --> 00:20:50,600
to talks and so on and you can pick up books and you can look on the internet, there are

194
00:20:50,600 --> 00:20:56,800
textual teachings and so on. And the Sangha, we really should take this seriously and here's

195
00:20:56,800 --> 00:21:05,440
a chance for me to plug our new website which is my.serumungalow.org which is a chance

196
00:21:05,440 --> 00:21:11,600
for everyone to get involved in our group. If you go and log on, I guess everyone here

197
00:21:11,600 --> 00:21:16,840
has already logged on but please do make the most of it. If there are problems with it,

198
00:21:16,840 --> 00:21:25,320
let me know if you have suggestions, let me know and please do use this website at the very

199
00:21:25,320 --> 00:21:29,520
least as a means of communicating with each other. It doesn't mean that we have to come

200
00:21:29,520 --> 00:21:33,960
on and post this and post that and be really active. This means come on every so often,

201
00:21:33,960 --> 00:21:40,960
let us know what you're doing, meditating and if you have any questions, if you have questions,

202
00:21:40,960 --> 00:21:48,400
you can post them on the Ask Datsurumungalow.org but organizing things. Someone started

203
00:21:48,400 --> 00:21:54,880
the Dhamma group in England, Owen started a Dhamma group in England based on the Mahasi

204
00:21:54,880 --> 00:22:07,280
doctor edition and so hopefully that group will come together and can use my Datsurumungalow.org

205
00:22:07,280 --> 00:22:12,000
we didn't know how. Different ways that we can use this at the very least to see that

206
00:22:12,000 --> 00:22:16,480
there are other people in our group and we have some how an international group, those

207
00:22:16,480 --> 00:22:21,760
people who have come here to practice. Using the Sangha as a support group because it's

208
00:22:21,760 --> 00:22:27,280
great encouragement and you really do need this kind of encouragement especially when you're

209
00:22:27,280 --> 00:22:33,400
living on your own surrounded often by people who aren't meditating so here is an opportunity

210
00:22:33,400 --> 00:22:37,880
for us to come together and just post something every so often and read things you can

211
00:22:37,880 --> 00:22:43,240
read. At the very least you can read what I'm doing, what's that's interesting, what

212
00:22:43,240 --> 00:22:48,520
we're doing here and the projects that we have in the world that's going on. So this

213
00:22:48,520 --> 00:22:54,800
is the three refuges and the precepts are the beginning of the practice. The three refuges

214
00:22:54,800 --> 00:22:59,680
are just the determination of the practice that we're going to take. The precepts are

215
00:22:59,680 --> 00:23:04,560
the beginning of the practice so we have this determination. My first step is I'm going

216
00:23:04,560 --> 00:23:10,240
to focus my activities and this is going to create concentration because I'm not going

217
00:23:10,240 --> 00:23:14,680
to have this guilt all the time, I'm not going to have clouding in my mind, I'm not going

218
00:23:14,680 --> 00:23:19,240
to have anger and greed, I'm going to cut these things off, I'm going to abstain from

219
00:23:19,240 --> 00:23:23,440
them. When I want something I'm going to force myself to look at the wanting, when I don't

220
00:23:23,440 --> 00:23:29,600
like something I'm going to force myself to look at the disliking, when I have views and

221
00:23:29,600 --> 00:23:34,840
opinions I'm going to force myself to challenge them and to give up views and opinions in

222
00:23:34,840 --> 00:23:42,560
favor of realizations and understanding. And so this morality is really for the purpose

223
00:23:42,560 --> 00:23:49,680
of the development of concentration or focus and wisdom. Again, I don't like using the

224
00:23:49,680 --> 00:23:54,400
word concentration because I think it's misleading and it's probably not even appropriate.

225
00:23:54,400 --> 00:24:03,280
The word is samadhi. Samadhi means composure, really. Sama means same. It doesn't mean

226
00:24:03,280 --> 00:24:10,640
same but it comes from the same root as the word same in English. Sama means level.

227
00:24:10,640 --> 00:24:17,280
So when your mind is balanced, having a balanced mind, it doesn't mean you concentrate your

228
00:24:17,280 --> 00:24:22,480
mind to the point where you don't experience anything else or so on. It's where you see

229
00:24:22,480 --> 00:24:27,160
something perfectly clearly. It's like focusing a camera as I've said before. You have

230
00:24:27,160 --> 00:24:33,920
to focus the camera exactly correctly if you focus it too much. This would be like concentrating.

231
00:24:33,920 --> 00:24:38,600
Then it goes out of focus again. If you don't focus enough, then it's not, it doesn't

232
00:24:38,600 --> 00:24:43,800
work. You have to find exactly the right focus. And this, this is simply seeing things

233
00:24:43,800 --> 00:24:49,240
as they are. So as we use the technique of being mindful reminding ourselves, this is seeing,

234
00:24:49,240 --> 00:24:54,080
this is hearing, this is smelling, this is tasting. As the Buddha said, when you see something,

235
00:24:54,080 --> 00:24:58,920
let it only be seeing. When you hear something, let it only be hearing. So remind yourself,

236
00:24:58,920 --> 00:25:03,280
this is seeing, this is hearing. When it's pain, remind yourself, this is pain. When

237
00:25:03,280 --> 00:25:07,560
you feel happy, remind yourself, this is happiness. It's not good. It's not bad. It's not

238
00:25:07,560 --> 00:25:14,800
me. It's not mine. By doing this, your mind becomes focused. It comes into focus and you

239
00:25:14,800 --> 00:25:19,960
start to see things clearly. Your mind might not be concentrated in the sense it might

240
00:25:19,960 --> 00:25:24,560
still jump here and there. But when it jumps, you clearly see it jumps. And you see the

241
00:25:24,560 --> 00:25:33,520
next object in line as it is, not as you'd like it to be or with any projections, but

242
00:25:33,520 --> 00:25:41,760
simply and clearly as the reality for what it is, it's actually quite simple. So concentration,

243
00:25:41,760 --> 00:25:47,360
or sorry, focus is therefore for the purpose of gaining wisdom. Because once you're in focus,

244
00:25:47,360 --> 00:25:51,640
you will see things as they are. Once you see things as they are, you'll become free

245
00:25:51,640 --> 00:25:57,960
from suffering. Your mind will let go. And this is really the key that the way to become

246
00:25:57,960 --> 00:26:05,840
free from suffering is not by any force of mind. It's simply by understanding things

247
00:26:05,840 --> 00:26:13,600
as they are. The only reason one would ever create suffering for oneself is when one

248
00:26:13,600 --> 00:26:18,480
doesn't realize that one is creating suffering for oneself. Let me think about it. No one

249
00:26:18,480 --> 00:26:25,600
would ever do something if they knew that that thing was going to cause them harm. If

250
00:26:25,600 --> 00:26:30,000
they really understood, the only reason we do things is because we think it has some benefit

251
00:26:30,000 --> 00:26:34,600
that there's some purpose to it, that this is a good thing to do. Once you see clearly

252
00:26:34,600 --> 00:26:38,160
that something is not a good thing to do, that there's no benefit that comes from it,

253
00:26:38,160 --> 00:26:42,680
then you give it up. If you examine it and you see that there is benefit and it does create

254
00:26:42,680 --> 00:26:48,600
goodness and peace and happiness, then you develop it. So the only thing we need to do in

255
00:26:48,600 --> 00:26:52,320
order to become free from suffering, it's quite simple. It's just to see things as they

256
00:26:52,320 --> 00:26:58,600
are because you'll never do something that is useless when you know that it's useless.

257
00:26:58,600 --> 00:27:03,440
The problem is we don't know that many of the things that we do and can say and think

258
00:27:03,440 --> 00:27:14,600
are useless and detrimental to ourselves and to other beings. So this is a brief exposition

259
00:27:14,600 --> 00:27:22,200
of what this ceremony means and what this day means or should mean to all of us and to

260
00:27:22,200 --> 00:27:27,440
thank everyone for joining. Maybe before we quit, for those of you who still have time

261
00:27:27,440 --> 00:27:32,840
and it's not too late or too early in the morning, we can do a short meditation, group

262
00:27:32,840 --> 00:27:38,760
meditation and then I'll take questions if there are still people left there and we

263
00:27:38,760 --> 00:27:43,960
can go from there. So I'm going to start with a meditation, you're welcome to leave

264
00:27:43,960 --> 00:27:48,840
whenever you want. I'm not going to keep the audio on now. I'm going to turn the audio

265
00:27:48,840 --> 00:27:56,240
off so you can just set your own timer or just peek every so often and see when the text

266
00:27:56,240 --> 00:28:00,760
chat. I think if you turn the sound on in the text chat, there's a little speaker in

267
00:28:00,760 --> 00:28:07,720
the bottom right corner. Click that, then every time someone says something, it gives

268
00:28:07,720 --> 00:28:12,760
you a beep. So after 15 minutes, I'm going to say something and you'll hear that beep.

269
00:28:12,760 --> 00:28:18,360
Otherwise, this has been another episode of Among Radio, thanks for tuning in.

