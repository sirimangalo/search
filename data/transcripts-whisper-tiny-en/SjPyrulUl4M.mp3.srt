1
00:00:00,000 --> 00:00:07,000
I have anxiety, panic attacks, at least one time per day.

2
00:00:07,000 --> 00:00:09,000
How can I calm down?

3
00:00:09,000 --> 00:00:14,000
I mean, if I think on what's happening, the panic becomes worse.

4
00:00:14,000 --> 00:00:15,000
Help.

5
00:00:15,000 --> 00:00:25,000
This is one that I answer a lot in various forms, but it's especially good to answer these questions

6
00:00:25,000 --> 00:00:29,000
because this is a real-life important question.

7
00:00:29,000 --> 00:00:36,000
It's not like what would happen if everyone became Buddhist and so on and so on.

8
00:00:36,000 --> 00:00:38,000
Because it's actually pertinent to that.

9
00:00:38,000 --> 00:00:40,000
It's actually an important question.

10
00:00:40,000 --> 00:00:44,000
It's not such an important question to know what would happen if,

11
00:00:44,000 --> 00:00:52,000
not to really not to poke fun too much.

12
00:00:52,000 --> 00:01:04,000
This is an important question because you have anxiety and that's causing real problems in your life.

13
00:01:04,000 --> 00:01:09,000
So you have to ask yourself, is the anxiety really a problem?

14
00:01:09,000 --> 00:01:14,000
Is the problem really the anxiety?

15
00:01:14,000 --> 00:01:19,000
Because if it is, then clearly looking at it is not the answer, right?

16
00:01:19,000 --> 00:01:23,000
Because when you look at it, it gets stronger, it becomes clearer.

17
00:01:23,000 --> 00:01:28,000
You get even more focused on the anxiety.

18
00:01:28,000 --> 00:01:33,000
But let's step back a second and really ask ourselves, what happens when you're anxious?

19
00:01:33,000 --> 00:01:36,000
What does it do?

20
00:01:36,000 --> 00:01:42,000
Probably it tenses up the stomach, tenses up the head,

21
00:01:42,000 --> 00:01:53,000
tenses up the shoulders, it makes you feel really bad.

22
00:01:53,000 --> 00:02:00,000
It prevents you from interacting with other people, from carrying out your life.

23
00:02:00,000 --> 00:02:04,000
It might even lead you to do something desperate.

24
00:02:04,000 --> 00:02:15,000
It hurts yourself, hurt other people, it will cause you to prevent you from living an ordinary life.

25
00:02:15,000 --> 00:02:18,000
But what is actually going on there?

26
00:02:18,000 --> 00:02:33,000
When you have the anxiety, what is it that is preventing you from...

27
00:02:33,000 --> 00:02:39,000
What is it that is actually preventing you from carrying out your life in an ordinary manner?

28
00:02:39,000 --> 00:02:48,000
When you look, what I'm trying to say here in a bit of an awkward way is that when you look at the anxiety,

29
00:02:48,000 --> 00:02:57,000
you'll see that actually it really isn't objectively a very big problem at all.

30
00:02:57,000 --> 00:03:06,000
Anxiety is a state of mind that causes physical reactions, you know, tensing and so on.

31
00:03:06,000 --> 00:03:08,000
But it really stops there.

32
00:03:08,000 --> 00:03:12,000
A simple anxiety, panic attack, stops there.

33
00:03:12,000 --> 00:03:15,000
It has effects in the mind, it has effects in the body.

34
00:03:15,000 --> 00:03:26,000
But unless we react to it in a negative way, it doesn't have...

35
00:03:26,000 --> 00:03:33,000
a massive impact or a significant impact on our lives.

36
00:03:33,000 --> 00:03:38,000
It doesn't have to have a significant impact on our lives in any way.

37
00:03:38,000 --> 00:03:50,000
I used to have very strong panic attacks when I had to give a talk, when I had to get up on stage or anything like that.

38
00:03:50,000 --> 00:03:59,000
This became an issue as a Buddhist monk because people think you're in robes.

39
00:03:59,000 --> 00:04:03,000
They must know something, so they want you to give a talk.

40
00:04:03,000 --> 00:04:12,000
So you get up on stage and sometimes in front of hundreds or hundreds of people anyway.

41
00:04:12,000 --> 00:04:25,000
And it can create a real panic attack. That is a clear example of how people who don't have a strong tendency to panic can really panic.

42
00:04:25,000 --> 00:04:28,000
And what I found through the meditation was...

43
00:04:28,000 --> 00:04:32,000
The hardest part of it was that noting didn't help.

44
00:04:32,000 --> 00:04:36,000
Meditation didn't help because I would acknowledge it.

45
00:04:36,000 --> 00:04:43,000
I would say to myself, you know, afraid, afraid or feeling, feeling and attention and stomach and so on.

46
00:04:43,000 --> 00:04:49,000
And it wouldn't go away. Sometimes it would get even worse.

47
00:04:49,000 --> 00:04:59,000
But what I found was that by doing it anyway, by noting anyway, regardless of whether it was going to wait,

48
00:04:59,000 --> 00:05:08,000
by trying and trying and trying and really putting my heart into the noting and every moment to be aware of what's going on,

49
00:05:08,000 --> 00:05:20,000
that the panic attack in general didn't get in my way, surprisingly enough, didn't get in my way in giving the talk.

50
00:05:20,000 --> 00:05:24,000
And sometimes I actually gave a better talk than when I was relaxed.

51
00:05:24,000 --> 00:05:33,000
I've seen motivational speakers, because when I was young, I would go to all these leadership conferences and they'd have motivational speakers.

52
00:05:33,000 --> 00:05:39,000
I saw one motivational speaker twice. It's something I'll always remember.

53
00:05:39,000 --> 00:05:49,000
The first time you gave the speech, I didn't know him or I wasn't in close contact with him.

54
00:05:49,000 --> 00:05:55,000
But it was a big deal. He'd never been to this leadership conference before.

55
00:05:55,000 --> 00:06:00,000
This was his first time being invited. And so it seemed like he took it very seriously and it was a brilliant talk.

56
00:06:00,000 --> 00:06:04,000
It was really something that inspired us all.

57
00:06:04,000 --> 00:06:11,000
The second time, he came back to give a talk again and I was at that point, it was my second time or third time or whatever.

58
00:06:11,000 --> 00:06:17,000
And I was one of the organizers and so I was talking with him and I was really impressed by this guy,

59
00:06:17,000 --> 00:06:27,000
sitting with him. And he was so, seemed so high on his last appearance that he got overconfident.

60
00:06:27,000 --> 00:06:31,000
He was sitting there telling me about how you just get up there and say, you just get up there and talk.

61
00:06:31,000 --> 00:06:36,000
You know, it doesn't matter what you say. It always comes out, right? If you just let yourself talk and so on.

62
00:06:36,000 --> 00:06:48,000
And he got up and he bombed, totally. He really fell flat because he was expecting people to just, you know, he was expecting a positive reaction.

63
00:06:48,000 --> 00:06:53,000
And so he wasn't trying. He wasn't putting any effort into it. He wasn't being mindful, you might say.

64
00:06:53,000 --> 00:07:04,000
Of course, he didn't have a great knowledge of mindfulness. When you're worried, when you have stress, it's often easier to be mindful than when you're happy.

65
00:07:04,000 --> 00:07:12,000
This is why people come to Buddhism when they're suffering because they really feel like they need an answer.

66
00:07:12,000 --> 00:07:36,000
And so, this is the surprising thing. Let me to or help me to understand what I've come to understand in general through the meditation practices that the biggest problem is not the experiences, even be they inherently negative experiences.

67
00:07:36,000 --> 00:07:43,000
It's our tendency to react to them, react negatively to a negative experience.

68
00:07:43,000 --> 00:07:56,000
The truth of it is when you focus on the anxiety or not just focus on it, when you recognize the anxiety as anxiety, the anxiety itself disappears.

69
00:07:56,000 --> 00:08:00,000
The anxious mind state is replaced by a mindful mind state.

70
00:08:00,000 --> 00:08:07,000
What doesn't disappear and what fools you into thinking that you're still anxious is the physical results of it.

71
00:08:07,000 --> 00:08:21,000
The physical manifestations of it because they are not directly connected to one state of mind.

72
00:08:21,000 --> 00:08:34,000
It's like you've already set the ball rolling and now you're feeling the effects of it, now you're feeling the effects of the anxiety in the stomach, in the head, in the shoulders, feeling all tense.

73
00:08:34,000 --> 00:08:38,000
And that's what doesn't go away.

74
00:08:38,000 --> 00:08:45,000
The problem is we don't see that. We lose our confidence in the meditation practice and we give it up and we feel anxious again.

75
00:08:45,000 --> 00:08:52,000
We get worried about it. We say, it's not going away and then we get anxious again. This isn't the, I don't have an answer.

76
00:08:52,000 --> 00:08:58,000
I don't know what to do. I still don't have a solution and so it makes us anxious again.

77
00:08:58,000 --> 00:09:13,000
The anxiety comes up again and we try to be mindful of it again and we find that yes, the effects of it are still there and so we think the anxiety is still there and so on and so on.

78
00:09:13,000 --> 00:09:29,000
If we are observant, we will be able to notice the difference and you should try to notice the difference.

79
00:09:29,000 --> 00:09:35,000
Often you need this, you need a teacher to point it out to you that the physical and mental are two different things.

80
00:09:35,000 --> 00:09:38,000
It's the first stage of knowledge that a person comes to.

81
00:09:38,000 --> 00:09:43,000
The difference between the physical and the mental and the separation between them two and in fact that's what you're realizing.

82
00:09:43,000 --> 00:09:53,000
That's what this is showing to you. It's showing you the difference between the physical and mental because when you say anxious or worried, worried, the worry is already gone.

83
00:09:53,000 --> 00:09:58,000
At that moment you're not worried, you're focused and mindful and clearly aware.

84
00:09:58,000 --> 00:10:09,000
The physical is not affected by that. The physical will continue to try. The physical is something separate from this.

85
00:10:09,000 --> 00:10:14,000
It is something that has already been affected by the past anxiety.

86
00:10:14,000 --> 00:10:22,000
What you should do at that point when you still feel the anxious, still feel the effects of the anxiousness is you should see them as physical and note them as well.

87
00:10:22,000 --> 00:10:31,000
The feeling in the stomach, feeling, feeling, tense, tense. If you react to it, disliking it, you should focus on this new mind state that has arisen, disliking, disliking.

88
00:10:31,000 --> 00:10:40,000
But this is what's going on. An anxiety attack is not just anxiousness. There's entity that is there continuously.

89
00:10:40,000 --> 00:10:49,000
It's a whole bunch of things going on for a moment to moment, anxious physical feeling, disliking the physical feeling, anxious again and so on and so on.

90
00:10:49,000 --> 00:10:56,000
And so many different things involved in there. There can be ego, the attachment to it. I'm anxious.

91
00:10:56,000 --> 00:11:06,000
There can be desire, the desire to be confident and impress other people and so on.

92
00:11:06,000 --> 00:11:14,000
But certainly the meditation is working. The meditation is helping you to see that these things are not under your control.

93
00:11:14,000 --> 00:11:31,000
Neither the physical nor the mental is subject to your control. The more you cling to it, the more you try to fix it or try to avoid it, try to change it, try to make things better.

94
00:11:31,000 --> 00:11:39,000
The more suffering, stress and disappointment you'll create for yourself. The bigger the problem will become.

95
00:11:39,000 --> 00:11:51,000
So, when the anxiety comes, it is correct to say to yourself, anxious anxious to focus on it, to be mindful of it, to let yourself quote unquote be anxious.

96
00:11:51,000 --> 00:12:02,000
Because the anxiety is not what's the problem. The problem is your reaction to generally physical states that are caused by the anxiety.

97
00:12:02,000 --> 00:12:12,000
The anxiety is just a moment. It's something that occurs in a moment and then is gone. When your mindful is already gone, all that's left is the physical experiences, which should be noted as well.

98
00:12:12,000 --> 00:12:26,000
And if you can be thorough with this and note everything, note the anxiety when it's there, note the physical sensations, note the disliking, going back and forth and be thorough and complete in your noting.

99
00:12:26,000 --> 00:12:37,000
You'll begin to get the hang of it and you will realize that it doesn't matter what arises. Nothing has the power to hurt you until you react to it.

100
00:12:37,000 --> 00:12:51,000
Not even negative mind states can hurt you until you react to it. So, just be a little more patient with it and you'll see for yourself all the things that I'm explaining to you.

101
00:12:51,000 --> 00:13:05,000
If you're a little more patient with the anxiety, noting it even though the anxiety doesn't seem to go away and noting more over the physical sensations, the disliking of the anxiety and so on and so on.

102
00:13:05,000 --> 00:13:23,000
Every physical and mental experience that arises for a moment to moment, you will see and you'll be able to break these things apart and you'll cease to be disturbed by them because you'll see that they don't have any power over you.

103
00:13:23,000 --> 00:13:29,000
And then it doesn't really matter what occurs. It doesn't matter whether you get afraid, it doesn't matter whether you're anxious. It doesn't even matter whether it inhibits your ability to perform in life.

104
00:13:29,000 --> 00:13:41,000
It doesn't matter if people think you're a fool and useless and incompetent. It doesn't really matter. It doesn't matter if you're thrown out on the street. It doesn't matter if you die.

105
00:13:41,000 --> 00:13:47,000
When you come to this realization, this is what really overcomes anxiety.

106
00:13:47,000 --> 00:14:03,000
I mean, I guess I had real anxiety problems even as a monk and it's something that through practice is really easy to solve actually. I think anxiety and fear are really easy ones to solve.

107
00:14:03,000 --> 00:14:15,000
It may not be eradicated quickly, but the solution comes fairly quickly when you realize that it doesn't really matter. Yeah, you can be anxious. Yeah, you can be afraid.

108
00:14:15,000 --> 00:14:21,000
And these are the, of course, acceptance, which is the opposite of anxiety.

109
00:14:21,000 --> 00:14:26,000
And that destroys them, I think.

110
00:14:26,000 --> 00:14:31,000
Anybody want to chime in there?

111
00:14:31,000 --> 00:14:57,000
Marianne, have something to add? I was just going to say, I thought you answered this problem. And with the emphasis on patience and realizing that all these petty little feelings that we have are just not that important.

112
00:14:57,000 --> 00:15:03,000
I was thinking of thinking of you when I said getting thrown out on the street.

