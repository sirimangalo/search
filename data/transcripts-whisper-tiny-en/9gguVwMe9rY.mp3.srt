1
00:00:00,000 --> 00:00:22,000
Worshiping the Buddha statue. I'm doing meditation since 21, since age 21. After taking meditation, I am doing less worship, and I feel there is no point in worship, since Buddha teach me, taught me only we can help ourselves. I just want to know if it's okay, I don't worship.

2
00:00:22,000 --> 00:00:36,000
Well, the Buddha said the best sort of worship is practice. He didn't use the word worship, although he might translate it that way.

3
00:00:36,000 --> 00:00:51,000
Yeah, I mean, you could, if you look at his wording, because when the Buddha was getting ready to pass into final liberation, all of the angels from all around the universe came.

4
00:00:51,000 --> 00:01:01,000
And all of the people came and offered flowers and candles and incense to the body, to worship the Buddha's dying body.

5
00:01:01,000 --> 00:01:12,000
And the Buddha, he wasn't dead yet. He brought on and over, and he said to Ananda, you see there worshiping in this way.

6
00:01:12,000 --> 00:01:26,000
But he said, this isn't how you truly worship, or pay respect to someone who is fully enlightened. He said the proper way to pay respect to someone is through the practice.

7
00:01:26,000 --> 00:01:50,000
He said, this is a good way to practice. He said, this is a good way to practice. He said, this is a good way to practice.

8
00:01:50,000 --> 00:02:16,000
So in that light, I would say it's not really proper to say we shouldn't worship, or to say that the Buddha would have us not worship.

9
00:02:16,000 --> 00:02:35,000
The Buddha said, this is a great blessing, worshiping those worthy of worship, or paying homage, because we don't want to try to tone it down, people don't like the word worship.

10
00:02:35,000 --> 00:02:53,000
Worship is a wonderful word, actually. It's just become corrupted by or it's been taken hostage by theistic religions. Worship means worthship, holding someone to be worthy of homage or have to have some worth to put it simply.

11
00:02:53,000 --> 00:03:11,000
It's actually not such a God-based, theistic-based word, worthship that has to do with worth. So it's expressing your feeling of veneration, of something having great worth and great value and great merit.

12
00:03:11,000 --> 00:03:40,000
So because we do consider the Buddha to be something that has great worth, and we do hold the Buddha up very high. So we develop, when we do practice worship, we develop states of humility and states of gratitude towards which our wholesome mind states, which create peace in the mind, create confidence in the mind.

13
00:03:40,000 --> 00:04:01,000
When we give ourselves up to the Buddha, there's one monkey said, that would damage you. He said, when you give yourself up to the Buddha, then when pain comes, you don't have to worry about it. You can say, it's not my body. I give it to the Buddha already.

14
00:04:01,000 --> 00:04:26,000
But the act of paying homage is an act of gratitude and veneration. It's not an unwholesome thing. What we see as unwholesome, we see other people doing it and thinking that that's practice, that that's of some ultimate benefit, some end in and of itself.

15
00:04:26,000 --> 00:04:36,000
Having worshipped something high without really knowing much about it, this has some benefit.

16
00:04:36,000 --> 00:05:04,000
And in a conventional sense, it does. It makes one happy and makes one confident and can theoretically lead a person to be born in heaven, for example. So this is, we might even say that theistic religions have some merit to them in the sense that they do teach people theoretically, how to go to heaven through this practice of prayer and worship and developing mind states of rapture and so on.

17
00:05:04,000 --> 00:05:11,000
Of course, the negative side is they have these wrong views and the ideas that heaven is permanent, but even beings in heaven have wrong views.

18
00:05:11,000 --> 00:05:23,000
I talked about this with Bantayanoma once, wondering he was saying, no, no, Christian people can't go to heaven because they have wrong view and they say, well, you know, if you think about it, Davis, many devers have wrong view.

19
00:05:23,000 --> 00:05:33,000
Angels have wrong view, so going to heaven is not, it's difficult, but it's not difficult in the sense that Buddhist meditation is difficult or inside is difficult.

20
00:05:33,000 --> 00:05:38,000
It's difficult in the sense you have to work hard and you have to develop all some states.

21
00:05:38,000 --> 00:05:51,000
So in a conventional sense, paying, you know, worshiping something has benefits. It brings power to the mind. It's objectively beneficial mind state.

22
00:05:51,000 --> 00:06:02,000
The negative side is the blind faith and the belief that by doing this, you're going to somehow go to heaven if you worship a God or you worship an idol or so, and then people worship trees and in India they worship.

23
00:06:02,000 --> 00:06:13,000
In India they worship phallic symbols and so on. The idea that somehow it's going to bring benefit. This is wrong view and they can lead you to be born as an angel, but you still have wrong view.

24
00:06:13,000 --> 00:06:19,000
For example, so from Buddhist point of view, we don't look at that. We don't pay much attention to it.

25
00:06:19,000 --> 00:06:29,000
We do customarily, you pay homage to the Buddha, you know, even Mahakasapah and the Buddha had passed away, Mahakasapah came and paid homage to his feet.

26
00:06:29,000 --> 00:06:41,000
We do do this. He came and you could say worship the Buddha, expressed his feeling that the Buddha had some worth and the idea that the Buddha was his teacher, but he didn't do it thinking that it was going to bring some benefit.

27
00:06:41,000 --> 00:06:53,000
He just did it out of respect and a feeling that it was the proper thing to do, out of respect and gratitude for his teacher and for the Buddha who was a very special being.

28
00:06:53,000 --> 00:07:01,000
But we don't consider it to be our practice and this is why the Buddha was very careful before he passed away to say this is not the highest form of homage.

29
00:07:01,000 --> 00:07:08,000
The highest form of homage is the practice of the Buddha's teaching and so you shouldn't hold such a hard line.

30
00:07:08,000 --> 00:07:13,000
I wouldn't recommend holding such a hard line stance as worship is useless and I shouldn't engage in it.

31
00:07:13,000 --> 00:07:23,000
I would harmonize the two views by saying that this is the highest form of homage, the highest form of worship that I'm doing already because the Buddha himself said that.

32
00:07:23,000 --> 00:07:34,000
And so it keeps the mind from getting overly critical and skeptical and antagonistic because people who criticize things and say this is no good and that's no good as many people do.

33
00:07:34,000 --> 00:07:45,000
Buddhists will often do saying all these people worshiping is they just have all this blind faith and people who are anti-theists talking about how silly, theistic religions are.

34
00:07:45,000 --> 00:07:52,000
And so this is an antagonistic, a negative mind state, a person who engages in this will actually have a lot of unwholesomeness.

35
00:07:52,000 --> 00:08:11,000
So if you look at many atheists, prominent atheists, they actually have these negative mind states so you can see some of them are actually angry people and unwholesomeness in their mind because they're just all about denying things.

36
00:08:11,000 --> 00:08:24,000
And of course the fact that they deny the existence of the mind but that's a whole other issue.

37
00:08:24,000 --> 00:08:42,000
In some senses we have a lot more in common with them and they can actually be more inclined towards meditation practice.

38
00:08:42,000 --> 00:08:57,000
Only Muhammad or only Allah or so on. These kind of wrong views are what generally stopped them from coming to practice meditation. Not the worshiping, the worshiping and the prayer and so on.

39
00:08:57,000 --> 00:09:21,000
So it actually creates benefit, creates a wholesome mind state since the views associated with the title of the problem.

