1
00:00:00,000 --> 00:00:11,000
Okay, welcome everyone to our morning session and second life.

2
00:00:11,000 --> 00:00:12,000
Good morning.

3
00:00:12,000 --> 00:00:25,000
For those of you in Europe, good evening.

4
00:00:25,000 --> 00:00:41,000
Today I thought I would talk about some of the criteria

5
00:00:41,000 --> 00:00:52,000
by which we used to decide what is the Buddha's teaching and what is not the Buddha's teaching.

6
00:00:52,000 --> 00:01:02,000
I will give a talk I think last week or not so far in the past.

7
00:01:02,000 --> 00:01:16,000
But all of the different schools of Buddhism and in my mind it's really

8
00:01:16,000 --> 00:01:24,000
going against the purpose of Buddhism to split up into different groups and that really when you look at all of the different types of Buddhism.

9
00:01:24,000 --> 00:01:53,000
What they claim to be unique about their school is actually generally not unique to anyone's school.

10
00:01:53,000 --> 00:02:10,000
And in fact what it seems is that each of the schools, the one that I belong to included, tends to go to extremes in regards to what they, in regards to their specialty.

11
00:02:10,000 --> 00:02:21,000
And so rather than having a balanced approach, a Buddhist approach, they have a very specialized approach towards Buddhism,

12
00:02:21,000 --> 00:02:29,000
which tends to really miss the point and miss the whole elephant, as I said.

13
00:02:29,000 --> 00:02:39,000
So following up on that, I thought it would be of interest to people to know what exactly is a good criterion.

14
00:02:39,000 --> 00:02:47,000
Once we, if we accept the idea that we should all just be Buddhist, then how do we know what is true Buddhism,

15
00:02:47,000 --> 00:02:57,000
when what is not true Buddhism, and it's really not that difficult, we actually have some guidelines as to what is the Buddhist teaching and what is not.

16
00:02:57,000 --> 00:03:15,000
And so today I thought I'd bring up one such list in the tradition that I follow, but I think there's nobody in any tradition who would be likely to go against the general feeling of

17
00:03:15,000 --> 00:03:18,000
encompassed in these teachings.

18
00:03:18,000 --> 00:03:32,000
So it's in Pali, I thought I'd give everyone a treat and bring up some Pali so that we can have some original words of the Buddha,

19
00:03:32,000 --> 00:03:38,000
and sort of get a feel for the language that the Buddha spoke.

20
00:03:38,000 --> 00:03:45,000
And so probably most of you are not going to be able to understand this, but let's just go through it.

21
00:03:45,000 --> 00:04:04,000
Atakho, Mahapachapati got me in Abakawati, Nupasankami. Atakho means at that time Mahapachapati got me the Bikuni, who was a female monk named Gautami, Mahapachapati got me.

22
00:04:04,000 --> 00:04:23,000
The sort of like mother, it's an ancient Indian word, and so it could either be used as her name or as an epithet to mean that she was the one who looked after the Buddha as he was growing up.

23
00:04:23,000 --> 00:04:25,000
Maha just means great.

24
00:04:25,000 --> 00:04:36,000
Gautami was her clan name or her given name. The Buddha's clan name was also Gautama, so we also always hear about Buddha Gautama.

25
00:04:36,000 --> 00:04:54,000
In Abakawati, Nupasankami, where the blessed one was there, she went up to. It means she went to see the Buddha. Bhakawantang is just means it's an epithet for a leader of a religion. And so in Buddhism, it means the Buddha.

26
00:04:54,000 --> 00:05:11,000
Abakawantang, Abhiva, did to Aikamantang Atasi, having approached the Buddha, and Abhiva, did to a paid respect to the Buddha. It means most likely bowed down to the Buddha.

27
00:05:11,000 --> 00:05:29,000
Aikamantang Atasi stood to one side or stood in an appropriate place. Because when you go to see the Buddha, there's always going to be people attending upon him and coming to ask him questions, coming to listen to what he says.

28
00:05:29,000 --> 00:05:53,000
He generally would be surrounded just as all famous teachers. Generally, R would be surrounded by people, so she's to do one side. And while standing on one side, Aikamantang Atasi, standing on one side, Maha Pajapati Kottami, Bhakawantang Atasi, she said this.

29
00:05:53,000 --> 00:06:11,000
Sadhu me Bhante, Bhakawantang Atasi, he said sadhu. Sadhu is a word that means good. Many people recognize this. We use it or it's used in India in the present day to talk about a holy man.

30
00:06:11,000 --> 00:06:32,000
And so it has a double meaning here. It means someone who is a good person, a sadhu. And it just means good. So sadhu here means good. She said it would be good for me, venerable sir. If the blessed one could teach the dhamma in brief, sankita in brief, dhamma is the dhamma.

31
00:06:32,000 --> 00:07:01,000
So she wants a teaching by which having heard the dhamma from the Buddha, she would be able to go off to the dhamma.

32
00:07:01,000 --> 00:07:29,000
She would go off alone into seclusion. Aikam means one. Wupakata means to go out into seclusion. Aapamata means to be diligent. She would be diligent and put out effort and dwell giving up abandoning the defilements.

33
00:07:29,000 --> 00:07:40,000
So basically speaking, she wants a brief teaching. And just going through this, we can understand, okay, what is she asking?

34
00:07:40,000 --> 00:07:49,000
She's asking for the most concise teaching that the Buddha can give her. And the Buddha gives her something very interesting.

35
00:07:49,000 --> 00:08:12,000
And this is the subject of the talk today.

36
00:08:12,000 --> 00:08:41,000
The Buddha says, when you know of those teachings that number one,

37
00:08:41,000 --> 00:08:58,000
Saragayya, Samwatantinoiragayya, they are for the purpose of developing lust, not for the purpose of giving up lust.

38
00:08:58,000 --> 00:09:15,000
Saragayya, Samwatantinoiragayya, they are for the purpose of becoming attached, not for the purpose of giving up attachment or for the purpose of bondage, not for the purpose of freedom from bondage.

39
00:09:15,000 --> 00:09:32,000
Saragayayya, Samwatantinoiragayayya, they are the purpose of building up, not for the purpose of giving up.

40
00:09:32,000 --> 00:09:58,000
Mahitsatayya, Samwatantinoiragayayya, they are for the purpose of great wanting, having of great desires, having many desires, not for the purpose of being of little desire, or having few desires, fewness of wishes.

41
00:09:58,000 --> 00:10:08,000
Asan-tutya, Samwatantinoiragayya, they are for the purpose of discontent, not for the purpose of contentment.

42
00:10:08,000 --> 00:10:24,000
Sankanikayya, Samwatantinoiragayayya, they are for the purpose of congregation or society, socializing, not for the purpose of seclusion.

43
00:10:24,000 --> 00:10:36,000
Kosa Jaya, Samwatantinoiragayya, they are for the purpose of laziness, not for the purpose of putting out effort.

44
00:10:36,000 --> 00:10:52,000
Dubarata, Samwatantino Subarata, they are for the purpose of being difficult to support, not for the purpose of being easy to support.

45
00:10:52,000 --> 00:10:59,000
What do you think? Are these things likely to be the dumb of the Buddha? No, the Buddha says, a kung-se-nat?

46
00:10:59,000 --> 00:11:14,000
You can be sure that, when they say, totally on the side, you can be sure completely.

47
00:11:14,000 --> 00:11:26,000
You can hold perfectly to be true that naiso-dhammo, this is not the dhamma, naiso-vinyo, this is not the vinya.

48
00:11:26,000 --> 00:11:38,000
Naitang-sattu-sasananti, this is not the teaching of the teacher, of our teacher.

49
00:11:38,000 --> 00:11:58,000
Then he goes on to say, the exact opposite, you said, those teachings that are for the purpose of giving up lust, and so on, and not for the purpose of having lust, of gaining lust, and so on.

50
00:11:58,000 --> 00:12:03,000
Then these are the teaching of the Buddha.

51
00:12:03,000 --> 00:12:13,000
This is, I think, a really good list, something that we can always use to verify for ourselves, and to not get caught up in particular.

52
00:12:13,000 --> 00:12:22,000
That really, the Buddha's teaching is not something high in mindy or high in, what do you say?

53
00:12:22,000 --> 00:12:30,000
It's not something esoteric or hard to understand, it's not something that is philosophical or theoretical.

54
00:12:30,000 --> 00:12:39,000
It's something quite simple, it's just generally, totally against the way we look at the world.

55
00:12:39,000 --> 00:12:52,000
It's totally against our ordinary way of life, diametrically opposed to our approach to existence.

56
00:12:52,000 --> 00:13:03,000
That's not something, it shouldn't be thought of as something that you have to read or have to study or have to spend years and years, just to understand it.

57
00:13:03,000 --> 00:13:10,000
The years and years should be spent in practice in trying to perfect it.

58
00:13:10,000 --> 00:13:12,000
So what do we have here?

59
00:13:12,000 --> 00:13:19,000
We have altogether eight criteria by which we can tell whether something is the Buddha's teaching.

60
00:13:19,000 --> 00:13:22,000
Sara Gaiya, someone and Dino Vira Gaiya.

61
00:13:22,000 --> 00:13:29,000
Buddha's teaching is for the purpose of giving up less, for the purpose of giving up all desire.

62
00:13:29,000 --> 00:13:38,000
A simple explanation of this is that anytime you desire something, it means that you're not happy with things as they are.

63
00:13:38,000 --> 00:13:58,000
It's a very simple fact that desire is basically saying to yourself that given two realities, the reality in front of us, and the reality that's not yet in front of us, the latter is more preferable.

64
00:13:58,000 --> 00:14:14,000
So it's a basic state of discontent. At its root, it's basically a state of suffering, a state of discontent, of dissatisfaction with things as they are.

65
00:14:14,000 --> 00:14:42,000
This is totally contrary to the Buddha's teaching because the Buddha teaches us to find contentment in the here and now, to accept this reality, or to find the way by the means by which reality becomes the preferred state.

66
00:14:42,000 --> 00:15:05,000
Or it becomes a totally peaceful, totally blissful state where there is no need, no hopes or dreams or wishes.

67
00:15:05,000 --> 00:15:13,000
And especially no lust.

68
00:15:13,000 --> 00:15:20,000
Lust is, I suppose, one of those things that is on the extreme end. It's the most basic teaching.

69
00:15:20,000 --> 00:15:29,000
In Indian religion, this was widely accepted, not only in Buddhism, not even something that the Buddha came up with himself.

70
00:15:29,000 --> 00:15:45,000
Even before the Buddha was born even, there were teachings on giving up lust. It was agreed that the passions of the sense, attachment to sights and sounds and smells and tastes and feelings were not a religious path.

71
00:15:45,000 --> 00:16:02,000
There were not something that could lead one to spiritual contentment. In fact, it was agreed upon through the various practices that were in place that there was a much greater happiness and peace that could be found by giving these things out.

72
00:16:02,000 --> 00:16:25,000
That this was the root of addiction. It's the same as a drug addict to be addicted to sensual pleasures. In fact, it is a drug addiction because sensual, according to science, it's clearly understood in this day and age that,

73
00:16:25,000 --> 00:16:35,000
what occurs when we receive a pleasurable stimulus at one of the senses is a chemical reaction in the brain.

74
00:16:35,000 --> 00:16:54,000
There's a release of certain chemicals, dopamine and whatever else is involved with the pleasure receptor system.

75
00:16:54,000 --> 00:17:15,000
That leads to a pleasurable feeling. It's a drug that gives us happiness. It does that for a short time and then goes away and leaves the receptors in a less than optimal state.

76
00:17:15,000 --> 00:17:31,000
At the next time, we receive the stimulus. There's a release of less of the chemical and the receptors are not able to not able to process as much and so on.

77
00:17:31,000 --> 00:17:48,000
The system is not able to bring as much pleasure as much happiness as the first time and so on and so on. It leads to more work for less pleasure. It's a drug addiction.

78
00:17:48,000 --> 00:18:03,000
Even just ordinary sensual pleasures. At the very base, it's a drug addiction.

79
00:18:03,000 --> 00:18:24,000
I guess you could say the most course of our addictions that we have to get rid of. It's one of the most widely accepted in religious traditions. I'd say the problem is not an agreement on giving them up. The problem is once you've given them up, how to then overcome the drug addiction.

80
00:18:24,000 --> 00:18:38,000
As we can see, many people who, for instance, become celibate wind up repressing the emotions and as a result, there are quite dire consequences to repression.

81
00:18:38,000 --> 00:18:48,000
The eventual explosion of desire, the release in other ways that can often be quite perverse.

82
00:18:48,000 --> 00:19:02,000
But at any rate, the Buddhist teaching is for the purpose of giving this up. It's for the purpose of seeing the negative side of this and seeing that in actual fact less and the happiness, the pleasure that comes from it. It's not sustainable.

83
00:19:02,000 --> 00:19:11,000
It's not really intrinsically positive in any way. It's a feeling that arises and ceases. There's nothing bad about it.

84
00:19:11,000 --> 00:19:36,000
And I think really the problem that comes from religious people is they start to feel guilty when they have these feelings. They hear that people like the Buddha gave these things up and so they feel guilty when they have these feelings arise rather than actually acknowledging them and coming to understand the feelings.

85
00:19:36,000 --> 00:19:42,000
But at any rate, we can understand that any teaching that is for the purpose of giving rise to less.

86
00:19:42,000 --> 00:19:54,000
There are even some religious teachings that encourage people to give rise to passion, to give rise to states of central intoxication.

87
00:19:54,000 --> 00:20:03,000
In fact, there are quite a many religions. From what I've seen visiting and taking part in other religious traditions,

88
00:20:03,000 --> 00:20:12,000
you can see that a lot of the ordinary service is geared towards central stimulus. I would say even in Buddhism there's a lot of this.

89
00:20:12,000 --> 00:20:21,000
In sort of cultural Buddhism, where the means of attracting people's attention is central stimulus.

90
00:20:21,000 --> 00:20:37,000
The encouragement of this sort of addiction and the stimulation of the addiction system in the brain.

91
00:20:37,000 --> 00:20:50,000
This clearly is not the Buddha's teaching. It's something that we should get straight for ourselves that the practice of the Buddha's teaching has to be very clearly for the purpose of giving this out.

92
00:20:50,000 --> 00:21:13,000
The second one is that Buddhism is for the purpose of freedom from bondage, not for the purpose of bondage, becoming bound.

93
00:21:13,000 --> 00:21:25,000
So obviously in our life, in a worldly sense, this is easy to see the difference between being bound and being unbound.

94
00:21:25,000 --> 00:21:35,000
In our lives, we become bound by so many things. We become bound by our work, bound by our debts, bound by other people.

95
00:21:35,000 --> 00:21:47,000
We get caught up in so many things, so much, frivolity. We get caught up in the world very easily.

96
00:21:47,000 --> 00:22:00,000
But in spirituality, we also get caught up in this as well. We become bound to rights and rituals. We become bound to our duties and to our station.

97
00:22:00,000 --> 00:22:11,000
We can even become bound to certain practices. We get caught up in the idea of being this or being that.

98
00:22:11,000 --> 00:22:18,000
San yoga, which means to be bound.

99
00:22:18,000 --> 00:22:39,000
This is why I stress last time about not identifying as this type of Buddhist or that type of Buddhist, that we follow the Buddhist teaching should really be enough, and we understand that the Buddhist teaching is not to become anything, not to be bound to anything, to say that I am this.

100
00:22:39,000 --> 00:22:52,000
Often, when talking with Buddhist meditators, there still is this re-attachment to a tradition or attachment to a teacher.

101
00:22:52,000 --> 00:23:01,000
And there's an immediate judgment when we hear someone else practicing in a certain way.

102
00:23:01,000 --> 00:23:14,000
And rather than using the Buddhist principles to say why we believe this is better than that, we cling instead to our tradition.

103
00:23:14,000 --> 00:23:20,000
And everything is seen through the lens of that tradition.

104
00:23:20,000 --> 00:23:40,000
When I'm talking to Buddhist meditators, they immediately begin to repeat the things that their teacher has said rather than actually looking at things from the point of view of the Buddha and the point of view of the Buddhist teaching.

105
00:23:40,000 --> 00:24:07,000
It's very difficult to speak with such people, with these sorts of people, they tend to just regurgitate whatever their teacher has said, and are unwilling to accept the fact that other traditions might actually be of great benefit or actually have a viable practice for the purpose of realizing the truth.

106
00:24:07,000 --> 00:24:11,000
When we practice Buddhism, we're simply trying to see reality for what it is.

107
00:24:11,000 --> 00:24:18,000
We have right here in front of us certain phenomena that are rising and ceasing in our body and our mind.

108
00:24:18,000 --> 00:24:22,000
We're not interested in being anything or being attached to anything.

109
00:24:22,000 --> 00:24:25,000
There's no need to call yourself Buddhist.

110
00:24:25,000 --> 00:24:30,000
There's certainly no need to call you this or yourself this or that type of Buddhist.

111
00:24:30,000 --> 00:24:40,000
Even in the tradition that I follow, there are different nikayas or groups, and these groups now don't talk with each other.

112
00:24:40,000 --> 00:24:47,000
I put a picture up of that one of my old students sent me of her ordination.

113
00:24:47,000 --> 00:24:50,000
She's ordained as a female monk.

114
00:24:50,000 --> 00:24:57,000
I put it up on my website and a monk from Europe sent me an email saying don't.

115
00:24:57,000 --> 00:25:04,000
He said, but they don't, please don't get involved in this schism.

116
00:25:04,000 --> 00:25:10,000
I wrote back and said, well, these people look quite harmonious to me.

117
00:25:10,000 --> 00:25:12,000
There doesn't seem to be any schism going on at all.

118
00:25:12,000 --> 00:25:15,000
They were happy and smiling and there were monks and nuns.

119
00:25:15,000 --> 00:25:18,000
There's about 20 of them in the picture.

120
00:25:18,000 --> 00:25:23,000
I said, honestly, you sound more like a schismatic to me.

121
00:25:23,000 --> 00:25:30,000
We cling to this idea that this is not correct and that is not correct for really no reason,

122
00:25:30,000 --> 00:25:33,000
no purpose and without really even thinking.

123
00:25:33,000 --> 00:25:37,000
Without thinking, we create barriers.

124
00:25:37,000 --> 00:25:48,000
We cling to things and we compartmentalize reality into our way, your way, right way, wrong way.

125
00:25:48,000 --> 00:25:57,000
When all the Buddha was really trying to teach us is to understand reality.

126
00:25:57,000 --> 00:26:06,000
Something that we should think about rather than clinging to principles and ideas.

127
00:26:06,000 --> 00:26:07,000
It sure is best we can.

128
00:26:07,000 --> 00:26:11,000
We should try to keep rules and the regulations of the community.

129
00:26:11,000 --> 00:26:16,000
There's no question about that, but it goes beyond that.

130
00:26:16,000 --> 00:26:26,000
We cling to my practice, your practice, mine is right and this idea that our teachers are perfect.

131
00:26:26,000 --> 00:26:31,000
Therefore, they must be right all the time.

132
00:26:31,000 --> 00:26:33,000
The Buddha said, we're not to cling.

133
00:26:33,000 --> 00:26:36,000
When you see something that leads to clinging, that's not the Buddha's teaching.

134
00:26:36,000 --> 00:26:41,000
The third one is for building up and not for taking down.

135
00:26:41,000 --> 00:26:49,000
Buddhism is not for building anything up, not for creating anything, not for becoming anything.

136
00:26:49,000 --> 00:26:52,000
We don't want to become this or become that.

137
00:26:52,000 --> 00:26:59,000
We don't want to become a Buddhist or a Buddhist teacher or even become a monk.

138
00:26:59,000 --> 00:27:07,000
I always looked at becoming a monk, for example, is really about more about what you aren't than what you are.

139
00:27:07,000 --> 00:27:18,000
The Buddha called it, giving up the house life, going forth, leaving behind.

140
00:27:18,000 --> 00:27:23,000
This is a good example because in the Buddha's teaching, we don't want to become anything.

141
00:27:23,000 --> 00:27:26,000
We don't have any desire to become anything.

142
00:27:26,000 --> 00:27:38,000
We don't want to build up in our minds identification with anything.

143
00:27:38,000 --> 00:27:46,000
We're not trying to build a Buddhist empire or a Buddhist.

144
00:27:46,000 --> 00:27:55,000
We're not trying to spread the Buddhist teaching to everyone or turn Buddhism into this.

145
00:27:55,000 --> 00:28:05,000
The missionary religion or so on.

146
00:28:05,000 --> 00:28:09,000
In fact, we're trying to take down, we're trying to take down much of who we are.

147
00:28:09,000 --> 00:28:17,000
We're trying to give up our identifications with this and with who we are and with our status in life.

148
00:28:17,000 --> 00:28:25,000
With ideas like class, social class and gender and so on.

149
00:28:25,000 --> 00:28:30,000
We're trying to in our minds give up the attachment to self.

150
00:28:30,000 --> 00:28:33,000
We're not trying to build up anything.

151
00:28:33,000 --> 00:28:37,000
We don't have the idea that we want to become reborn again as this or that.

152
00:28:37,000 --> 00:28:45,000
We don't have the idea that in this life we want to become famous or rich.

153
00:28:45,000 --> 00:28:54,000
Even becoming enlightened in meditation practice, it's very easily for people to get attached to this idea that we're going to become enlightened.

154
00:28:54,000 --> 00:29:01,000
As though enlightenment was this attainment, this badge that you get to wear.

155
00:29:01,000 --> 00:29:07,000
Oh, you've achieved something and become enlightened.

156
00:29:07,000 --> 00:29:17,000
Actually, enlightenment is all about what you aren't.

157
00:29:17,000 --> 00:29:21,000
The enlightened being is not greedy, it's not angry, it's not deluded.

158
00:29:21,000 --> 00:29:24,000
The enlightened being is given up, hasn't taken anything on.

159
00:29:24,000 --> 00:29:33,000
In our meditation practice, it's very important, extremely important that we give up this notion of trying to attain anything.

160
00:29:33,000 --> 00:29:40,000
We're not trying to reach some state where the mind is perfectly calm all the time.

161
00:29:40,000 --> 00:29:52,000
Never thinking anything, never feeling any pain.

162
00:29:52,000 --> 00:30:01,000
We're trying to give up all expectations, all wants and needs, all attachments whatsoever.

163
00:30:01,000 --> 00:30:03,000
We don't need to become anything.

164
00:30:03,000 --> 00:30:11,000
It's more of a giving up of the path than the taking on of any path.

165
00:30:11,000 --> 00:30:13,000
Giving up of all paths.

166
00:30:13,000 --> 00:30:19,000
The path of the Buddha is to be here and now, to stop going, to stop coming.

167
00:30:19,000 --> 00:30:25,000
Buddha said, in the Bhanta there is no going and no coming.

168
00:30:25,000 --> 00:30:32,000
The Bhanta isn't somewhere else, enlightenment isn't a place that we go to.

169
00:30:32,000 --> 00:30:40,000
It's when we stop going and stop becoming.

170
00:30:40,000 --> 00:30:48,000
The fourth one, Mahi Chaitaya, Samwatant, you know, Api Chaitaya.

171
00:30:48,000 --> 00:30:56,000
But I said, those things that are for the purpose of wanting many different things, not for the purpose of wanting little.

172
00:30:56,000 --> 00:31:06,000
I think this goes along with much of what I've already said. I'm not going to go in much detail here.

173
00:31:06,000 --> 00:31:13,000
But it's just another reminder that our wants are really the problem.

174
00:31:13,000 --> 00:31:19,000
We have these in our minds at every moment we want things in our daily life.

175
00:31:19,000 --> 00:31:22,000
Every situation in our lives becomes a want.

176
00:31:22,000 --> 00:31:24,000
Wanting this, wanting that.

177
00:31:24,000 --> 00:31:27,000
Chasing up to this, chasing up to that.

178
00:31:27,000 --> 00:31:28,000
We can't even sit still.

179
00:31:28,000 --> 00:31:32,000
We can't be happy with things as they are because of our many wants.

180
00:31:32,000 --> 00:31:37,000
Buddhism is to be a fewness of wishes to have few wants.

181
00:31:37,000 --> 00:31:43,000
We do not want a lot to be able to live without.

182
00:31:43,000 --> 00:31:53,000
And this is a very practical teaching in terms of our necessities.

183
00:31:53,000 --> 00:32:01,000
When we're not able to acquire the things that we would normally feel necessary.

184
00:32:01,000 --> 00:32:09,000
We need this and we need that to be able to live with little, to be able to live a simple life, a content life.

185
00:32:09,000 --> 00:32:12,000
And this is number five, so these go together.

186
00:32:12,000 --> 00:32:28,000
Number five is, you know, if the teaching is for discontent and not for contentment, then it's not the Buddha's teaching.

187
00:32:28,000 --> 00:32:37,000
Number six, number six is rather important that I think is often missed in Buddhist practice or in spiritual practice.

188
00:32:37,000 --> 00:32:41,000
It's the need for solitude.

189
00:32:41,000 --> 00:32:44,000
The need to take time off.

190
00:32:44,000 --> 00:32:51,000
You hear a lot about how important it is to meditate in a group.

191
00:32:51,000 --> 00:33:01,000
And how important it is to sit together in a group when you meditate.

192
00:33:01,000 --> 00:33:10,000
And I think it's really a sign of the importance to not be in a group.

193
00:33:10,000 --> 00:33:20,000
When we practice in a group it's obviously much easier because your mind is constantly being reminded.

194
00:33:20,000 --> 00:33:31,000
By the people around you are reminded to focus, to not become distracted.

195
00:33:31,000 --> 00:33:34,000
And so it's quite easy in that sense.

196
00:33:34,000 --> 00:33:48,000
But the problem with it is, and you can see this if you ever take the time to go off on your own, is that when you're in a group, your mind is constantly

197
00:33:48,000 --> 00:33:51,000
going out to the people in the room around you.

198
00:33:51,000 --> 00:33:54,000
You're not actually as focused as you think you are.

199
00:33:54,000 --> 00:34:02,000
We think it's so easy to sit in a group because you constantly think of the people around you and you feel kind of embarrassed

200
00:34:02,000 --> 00:34:13,000
if you slouch or if you mind wanders or if you think bad thoughts and so on.

201
00:34:13,000 --> 00:34:18,000
But there's actually, I would say, far less focus when you're together than when you're alone.

202
00:34:18,000 --> 00:34:24,000
And when you're alone you can actually see how your mind works much better because there's nothing for your mind to cling to.

203
00:34:24,000 --> 00:34:32,000
There's no external phenomenon, no external stimulus.

204
00:34:32,000 --> 00:34:38,000
I think it goes without saying that in the Buddha's teaching it's not for the purpose of socializing.

205
00:34:38,000 --> 00:34:50,000
I'm sitting around talking and meeting up with friends and this is a very important teaching in and of itself that when we practice the Buddha's teaching we're not.

206
00:34:50,000 --> 00:34:57,000
It's very important that we can give up our need to socialize.

207
00:34:57,000 --> 00:35:12,000
That actually it's quite a detriment to our practice to be addicted to friendships and association and going out to parties and chatting and socializing.

208
00:35:12,000 --> 00:35:18,000
A Buddhist meditator should be content with solitude.

209
00:35:18,000 --> 00:35:25,000
There is no replacement for being alone spending time with your own mind.

210
00:35:25,000 --> 00:35:34,000
Because it's really the only time that we can really be true to ourselves, where we don't have to put up a front and pretend to be something.

211
00:35:34,000 --> 00:35:46,000
Even when we're meditating, when we meditating in a group, so often we pull on a front.

212
00:35:46,000 --> 00:35:59,000
We don't want other people to think that we're a bad meditator, so we sit up straight.

213
00:35:59,000 --> 00:36:03,000
We put on a front, even when we're meditating.

214
00:36:03,000 --> 00:36:18,000
But when we're alone we give this all up and we're able to be true and completely with ourselves not focusing on anything else.

215
00:36:18,000 --> 00:36:41,000
Number seven is that the Buddha's teaching is for putting out effort, not for being lazy.

216
00:36:41,000 --> 00:37:02,000
It's important when we meditate where we're not just meditating to feel peaceful and calm to sit down and to get something.

217
00:37:02,000 --> 00:37:16,000
Meditation in a Buddhist sense is a word. The word we use is kamatana. It means to focus your mind, put your mind to work.

218
00:37:16,000 --> 00:37:22,000
When we practice meditation, we're not going to simply just sit there and fall asleep.

219
00:37:22,000 --> 00:37:29,000
It's not just a relaxation time. It's not our vacation time, our time away from life.

220
00:37:29,000 --> 00:37:37,000
Meditation is for the purpose of clearing the mind for developing the mind.

221
00:37:37,000 --> 00:37:47,000
I think it's very easy to become lazy in our practice where we practice maybe once a day, twice a day.

222
00:37:47,000 --> 00:37:54,000
We're not really developing our minds, but simply falling back into a state of peace and calm.

223
00:37:54,000 --> 00:38:11,000
It's important when we meditate that we actually examine our state of mind that we're looking at it and trying to develop our minds to become a better person, to clear our minds, to go deeper and to understand our minds

224
00:38:11,000 --> 00:38:17,000
clear and clear and to understand reality.

225
00:38:17,000 --> 00:38:24,000
We have to examine ourselves when we practice. Are we just practicing to feel states of peace and calm and be content with that?

226
00:38:24,000 --> 00:38:31,000
Or are we actually learning something? I would say if you're practicing correctly, every time you sit down you should learn something new about yourself.

227
00:38:31,000 --> 00:38:35,000
This is really a test as to whether you actually meditate it.

228
00:38:35,000 --> 00:38:43,000
Meditation is the contemplation, the examination of oneself.

229
00:38:43,000 --> 00:38:51,000
Number eight, to be easy to support, not difficult to support.

230
00:38:51,000 --> 00:39:05,000
This one, taking it to fold first in terms of our daily lives.

231
00:39:05,000 --> 00:39:09,000
I would say it's very easy to become very difficult to support.

232
00:39:09,000 --> 00:39:20,000
We are quite difficult to support. We need so many things. Our minds are constantly searching out, seeking out.

233
00:39:20,000 --> 00:39:23,000
It's like looking after a little child.

234
00:39:23,000 --> 00:39:26,000
Looking after ourselves is like looking after a little child.

235
00:39:26,000 --> 00:39:32,000
Our wants and our needs and our nagging and complaining.

236
00:39:32,000 --> 00:39:37,000
It's very difficult for us to, for instance, find a good position for meditation.

237
00:39:37,000 --> 00:39:46,000
We often need the perfect position and so rather than simply sitting on the floor we need a cushion and the cushion's not enough.

238
00:39:46,000 --> 00:40:05,000
We need a backrest and then the backrest not enough. We need cushions under our legs and a pillow behind our neck and so on.

239
00:40:05,000 --> 00:40:11,000
We live our lives in a state of general discontent, not being able to stick with the here and the now.

240
00:40:11,000 --> 00:40:15,000
Even right now you can examine yourself.

241
00:40:15,000 --> 00:40:20,000
When things arise in the mind, when things arise in the world around you.

242
00:40:20,000 --> 00:40:23,000
You're just sitting here listening.

243
00:40:23,000 --> 00:40:32,000
Maybe suddenly you start to feel pain in the legs or it's too hot or there's an itching sensation in the body.

244
00:40:32,000 --> 00:40:38,000
Maybe it's been half an hour already and it's starting to get boring.

245
00:40:38,000 --> 00:40:46,000
The mind starts to wander, starts to think of other things.

246
00:40:46,000 --> 00:40:50,000
You can see the mind is quite difficult to take care of. It's not easily amused.

247
00:40:50,000 --> 00:40:57,000
It's not easily entertained. It always needs something new and exotic, something fun and interesting.

248
00:40:57,000 --> 00:41:12,000
It can't be content just being here and just being now. It can't be content with reality.

249
00:41:12,000 --> 00:41:18,000
So the Buddha said the Buddha's teaching is for the purpose of being easy to take care of.

250
00:41:18,000 --> 00:41:22,000
We don't need other people to take care of us. We don't need so many things.

251
00:41:22,000 --> 00:41:34,000
We don't need constant attention, constant gratification that we can accept reality for what it is.

252
00:41:34,000 --> 00:41:42,000
We only need a certain amount of food to eat, simple clothes to wear.

253
00:41:42,000 --> 00:41:50,000
We're able to put up with anything that arises.

254
00:41:50,000 --> 00:42:00,000
So I thought I'd point this list out to everybody and let you know that there are general principles by which we can say what is the Buddha's teaching.

255
00:42:00,000 --> 00:42:07,000
So that was a list of those things that aren't just for completeness. Here's the rest of it.

256
00:42:07,000 --> 00:42:14,000
And just to recapitulate, to recapitulate, to go over it again.

257
00:42:14,000 --> 00:42:22,000
Those things that are for the purpose of giving up lust. Those things that are for the purpose of giving up bondage.

258
00:42:22,000 --> 00:42:31,000
Those things that are for the purpose of not collecting or not building up oneself.

259
00:42:31,000 --> 00:42:38,000
Those things that are for the purpose of being a few-ness of having few-wants or few wishes.

260
00:42:38,000 --> 00:42:54,000
Those things that are for the purpose of contentment, for the purpose of seclusion, for the purpose of putting out effort and for the purpose of being easy to support.

261
00:42:54,000 --> 00:43:10,000
You can be sure, or go to me, that this is the dhamma, this is the bhinnaya, this is the teaching of our teacher.

262
00:43:10,000 --> 00:43:30,000
Everyone has any questions, I'm happy to take them. Otherwise, thanks for coming.

