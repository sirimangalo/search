1
00:00:00,000 --> 00:00:08,800
Good evening, everyone.

2
00:00:08,800 --> 00:00:26,480
Welcome to our daily Dhamma broadcast, testing out our new broadcast method and to our local

3
00:00:26,480 --> 00:00:44,800
meditators who, again, have spent the day dedicating themselves to Buddha's teaching.

4
00:00:44,800 --> 00:00:56,400
When we study the Buddha's teaching, it's easy to get lost in the Buddha's teaching.

5
00:00:56,400 --> 00:01:03,840
It's not easy to sum up the Buddha's teaching in a few words.

6
00:01:03,840 --> 00:01:14,160
It seems like everywhere you pull out the Buddha's teaching, it leads to something else.

7
00:01:14,160 --> 00:01:33,320
But to really understand it takes, we're often quite a bit of, quite a bit of explanation,

8
00:01:33,320 --> 00:01:45,880
picking here and there, but we do have various basic starting points.

9
00:01:45,880 --> 00:01:53,720
We have the Buddha's starting point, where he started when he began to teach, when the

10
00:01:53,720 --> 00:02:04,360
Buddha began to teach, first he wasn't going to teach, he became enlightened, he had no inclination

11
00:02:04,360 --> 00:02:05,360
to teach.

12
00:02:05,360 --> 00:02:09,320
It's sort of the dilemma of becoming enlightened.

13
00:02:09,320 --> 00:02:23,000
On the one hand, you have great benefit and you're a wonderful person to learn from,

14
00:02:23,000 --> 00:02:30,800
on the other hand, you have no desire left, no intention to teach unless invited, fortunately

15
00:02:30,800 --> 00:02:39,880
for us, the Buddha was invited to teach, and so he taught the Dhamma-chakapawatana-sutra.

16
00:02:39,880 --> 00:02:51,000
He turned the wheel of Dhamma, he said in motion, a living tradition.

17
00:02:51,000 --> 00:03:03,560
A living just to be handed from a teacher to student, or I just continued rolling up until

18
00:03:03,560 --> 00:03:16,040
the present day, and it all started with five ascetics reluctant to hear the Buddha's teaching

19
00:03:16,040 --> 00:03:25,360
because it went against their views and their idea of having to torture oneself in order

20
00:03:25,360 --> 00:03:33,560
to become enlightened, and so the Buddha explained to them the middle way, the first thing

21
00:03:33,560 --> 00:03:44,640
he taught was that spiritual practice need not be extreme, shouldn't be extreme, religious

22
00:03:44,640 --> 00:03:57,920
practice shouldn't be a reaction, shouldn't be a reaction to the problem of worldliness,

23
00:03:57,920 --> 00:04:06,720
shouldn't be a reaction to the problem of desire and ambition and clinging and so on.

24
00:04:06,720 --> 00:04:15,640
It really just practiced should be an understanding, something very special about the Buddha's

25
00:04:15,640 --> 00:04:25,440
teaching as a religion, it's how important it is for one to cultivate understanding.

26
00:04:25,440 --> 00:04:33,600
Buddhism isn't, of course, about faith, it isn't about escaping the mundane, it's

27
00:04:33,600 --> 00:04:44,800
understanding, understanding the mundane, so Buddhism is at once a religion and a science,

28
00:04:44,800 --> 00:04:50,640
it's a religion because we take it seriously, it's not just about knowledge and wisdom,

29
00:04:50,640 --> 00:04:57,280
it's about us, it's about our lives, it's about the very core of our being, our identity,

30
00:04:57,280 --> 00:05:08,360
who we are, it's about our future, it's about ourselves, it's about happiness and peace,

31
00:05:08,360 --> 00:05:24,240
it's a science because it involves investigation, observations and conclusions based on solid observations.

32
00:05:24,240 --> 00:05:29,760
So the Buddha taught this as a middle way, a middle way of understanding, really, right

33
00:05:29,760 --> 00:05:44,800
view, right thought, right speech, right action, right livelihood, right effort, right mindfulness,

34
00:05:44,800 --> 00:05:55,000
right concentration, you see, so it becomes a whole bunch of things, I start with something

35
00:05:55,000 --> 00:06:08,720
simple, but it's not really simple at all, right view is the knowledge of the four

36
00:06:08,720 --> 00:06:21,600
normal truths, so it's reflexive, reflective and it's recursive, it's an infinite loop,

37
00:06:21,600 --> 00:06:31,120
yeah, you talk about the eight full noble path and you end up going to the four noble truths,

38
00:06:31,120 --> 00:06:34,720
the fourth of which is the eight full noble path, the first of which involves the four

39
00:06:34,720 --> 00:06:47,840
noble truths, so you see Buddhism is explaining the Buddha's teaching, it requires us to

40
00:06:47,840 --> 00:06:56,560
be content and to touch upon pieces of it to really explain all of the Buddha's teaching

41
00:06:56,560 --> 00:07:06,800
gets more complicated. So when we talk about the eight full noble path, we have eight

42
00:07:06,800 --> 00:07:15,120
parts, so we can actually, if we just want to get a sense of it, it's very useful to go into

43
00:07:15,120 --> 00:07:24,240
detail, but if we just want to get a sense of it, we talk about the three trainings, right view

44
00:07:24,240 --> 00:07:29,920
involves four noble truths, understanding the four noble truths and it involves having right

45
00:07:29,920 --> 00:07:38,320
thoughts, thoughts that are free from greed, anger, delusion, but altogether it just means wisdom,

46
00:07:39,440 --> 00:07:43,840
this is what we mean by wisdom, right view and right thought,

47
00:07:47,120 --> 00:07:52,000
and then we have right speech, right action, right livelihood, so

48
00:07:52,000 --> 00:07:57,440
we don't kill, we don't steal, we don't lie and cheat,

49
00:08:04,480 --> 00:08:11,840
but altogether it just means ethics, morality, see that,

50
00:08:11,840 --> 00:08:22,080
do I have a proper ethic, means our actions and our speech should be mindful, ultimately

51
00:08:22,080 --> 00:08:29,200
morality is a part of a meditation practice, ethics isn't putting yourself in a situation where

52
00:08:29,200 --> 00:08:36,000
you might kill and not kill, right, do you think living in a meditation center, how can you

53
00:08:36,000 --> 00:08:46,560
cultivate morality, it's a part of your meditation practice, when you walk, you can walk

54
00:08:47,280 --> 00:08:55,600
unestically, and when you speak, when you eat, when you shower, your shower is ethically charged,

55
00:08:56,720 --> 00:09:01,600
that you didn't realize that when you're on the toilet, it can be an unethical act,

56
00:09:01,600 --> 00:09:11,280
the ethics of the bathroom, it's very true, no joke,

57
00:09:14,800 --> 00:09:18,160
the things that you do is your mind pures, your mind clear,

58
00:09:19,360 --> 00:09:26,800
and clearly aware of what you're doing that's proper morality, it's associated with mindfulness,

59
00:09:26,800 --> 00:09:36,320
the mindful, the mind that has mindfulness also has morality, also is ethical, you can never kill

60
00:09:36,320 --> 00:09:46,080
or steal or lie or cheat mindfully, there has to be a moment where you disregard the clarity of

61
00:09:46,080 --> 00:09:58,720
mind that comes with mindfulness and you force yourself, push yourself to do something unethical,

62
00:09:58,720 --> 00:10:03,120
much stronger in a meditation center, of course you're not killing or stealing or lying or cheating

63
00:10:03,120 --> 00:10:10,560
great, that's wonderful being in a meditation center, ethics is quite easy, but it becomes much more

64
00:10:10,560 --> 00:10:17,920
refined and powerful because you're eating ethically, you're not greedy when you eat, you're not

65
00:10:18,720 --> 00:10:27,280
eating out of greed, your actions are pure, when you're in the shower, you're showering ethically,

66
00:10:27,280 --> 00:10:51,840
mindfully there's no delusion, there's no greed, there's no anger and the last three are

67
00:10:51,840 --> 00:11:01,120
right effort, right mindfulness, right concentration, altogether it's the samadhi which we translate

68
00:11:01,120 --> 00:11:10,320
as concentration or focus, that's the concentration section, to have proper concentration, you need

69
00:11:10,320 --> 00:11:20,320
effort, you need to be, it takes work, you need mindfulness, right concentration isn't just about

70
00:11:20,320 --> 00:11:30,720
focusing the mind, it's about being clearly aware as well, being focused on the object and

71
00:11:30,720 --> 00:11:38,240
having the object in focus, right it's easy to be focused on something, it's not really, it's

72
00:11:38,240 --> 00:11:44,400
easy to feel focused in the sense of the mind that is stiff and unbending but it's much harder to

73
00:11:44,400 --> 00:11:51,440
be in focus where you're actually aware of the object and clearly grasping it in your awareness.

74
00:11:55,520 --> 00:12:00,240
So see la samadhi panya, we can do this sort of thing, we abbreviate the Buddhist teaching

75
00:12:01,120 --> 00:12:05,680
to get a sense of it, it helps us so we don't have to get in because each one of the eight full

76
00:12:05,680 --> 00:12:11,600
normal path parts can be further subdivide and we can talk about each one of them and

77
00:12:11,600 --> 00:12:19,120
sends us down the rabbit hole. But that's the first thing the Buddha taught and then he put it

78
00:12:19,120 --> 00:12:25,120
in the larger context, that was in many ways the first teaching, the first thing he taught was

79
00:12:25,120 --> 00:12:30,160
much more directed to the specific audience because they were at an extreme, they were torturing

80
00:12:30,160 --> 00:12:36,640
themselves and they were really reluctant to listen to him because obviously he was no longer

81
00:12:36,640 --> 00:12:43,200
practicing in an extreme way. So if you want to say the first teaching proper it was what came

82
00:12:43,200 --> 00:12:47,680
next and that's the actual delineation of the four noble truths.

83
00:12:52,720 --> 00:13:00,560
So in remembering the Buddha, rememberance of the Buddha's teaching and getting back to our own

84
00:13:00,560 --> 00:13:09,840
basics, starting at the beginning, we go over the four noble truths, remind ourselves what are these?

85
00:13:09,840 --> 00:13:14,800
What is this that we're seeking? The Buddha said about these

86
00:13:21,680 --> 00:13:27,440
not seeing these four noble truths, I wandered on from life to life.

87
00:13:27,440 --> 00:13:52,400
So the first number of truths is one we're all familiar with suffering. We're familiar with it

88
00:13:52,400 --> 00:13:58,000
in two ways. We're familiar with it as the first or as the essence of the Buddha's teaching.

89
00:13:58,000 --> 00:14:05,520
Yes, what is Buddhism all about? Yes, it's all about suffering. And also second we are familiar

90
00:14:05,520 --> 00:14:10,320
with it because we're familiar with suffering. I think it's hard to find someone who doesn't have

91
00:14:10,320 --> 00:14:18,240
at least a mild sense of what it means to suffer. You might criticize certain people and say

92
00:14:18,240 --> 00:14:24,320
they've lived such pampered lives, they don't know suffering, but I don't think it really matters

93
00:14:24,320 --> 00:14:32,640
how pampered you are, you still suffer as a human being. A rich person suffers when they have

94
00:14:32,640 --> 00:14:38,080
to wait for it, when they don't get what they want immediately, they still suffer.

95
00:14:40,000 --> 00:14:45,520
Even when they get what they want, they still might suffer when it's not exactly what they expected.

96
00:14:45,520 --> 00:14:53,760
In fact, you might say the more spoiled the person is, the more suffering they have,

97
00:14:56,320 --> 00:15:02,960
constantly wanting. And whenever you're wanting, there's a stress involved, there's a suffering involved.

98
00:15:02,960 --> 00:15:16,080
You're not happy, you're anxious, you're displeased, you're uncomfortable. Most people don't

99
00:15:16,080 --> 00:15:23,040
understand happiness. They think they do, they think they know what happiness is. So it's such a

100
00:15:23,040 --> 00:15:28,720
surprise once they begin to meditate to realize how much they're suffering. When the beginning,

101
00:15:28,720 --> 00:15:34,720
it often feels like the meditation is causing you suffering. It's meditation, wow.

102
00:15:36,560 --> 00:15:42,640
It's really unpleasant to meditate, right? But all it's doing is showing you you. It's saying,

103
00:15:42,640 --> 00:15:48,160
hey, stop running away from it and let's see what we've got inside, because believe it or not,

104
00:15:48,160 --> 00:15:51,200
some people practice meditation and don't suffer terribly.

105
00:15:51,200 --> 00:15:59,360
And so you come to realize that it's really our own minds that cause us suffering.

106
00:16:00,320 --> 00:16:06,400
I think to do with the meditation, meditation is just showing you this first noble truth.

107
00:16:09,040 --> 00:16:12,720
They're noble because they're important because they're useful,

108
00:16:13,360 --> 00:16:18,480
because these are the truths that make you free. These are the truths that set you free.

109
00:16:18,480 --> 00:16:25,120
There are many kinds of truths that don't, and if you know and understand them, they're not

110
00:16:25,120 --> 00:16:33,680
really that useful. But understanding these truths, they're very fundamental to not an impersonal

111
00:16:33,680 --> 00:16:38,000
understanding of the universe, but they're very fundamental to an understanding of our universe,

112
00:16:38,000 --> 00:16:44,640
of us, of who we are. They're very religious in that sense. They're truths that are important,

113
00:16:44,640 --> 00:16:52,240
that are useful, that are about all about us, all about our experience.

114
00:16:58,640 --> 00:17:04,560
So suffering. It's there. And really it's the one you have to understand. If you understand

115
00:17:04,560 --> 00:17:09,600
suffering and you see what is causing you suffering and you really get it, it's all you need.

116
00:17:09,600 --> 00:17:17,920
So this is an important point about Buddhism is that suffering is not to be escaped. It's not

117
00:17:17,920 --> 00:17:24,160
something we run from, something we come to understand. The goal in Buddhism is not to escape

118
00:17:24,160 --> 00:17:31,200
suffering, not directly, exactly. The goal of Buddhism is to understand suffering.

119
00:17:31,200 --> 00:17:39,760
And then, and only then, can you escape it? Because we cause ourselves suffering. Why?

120
00:17:40,880 --> 00:17:45,920
Because of the second noble truths. What is the cause of suffering? The cause of suffering really?

121
00:17:46,960 --> 00:17:51,440
It's not exactly what the Buddha said, because you have to, again, go into, you have to go deeper.

122
00:17:52,480 --> 00:17:56,240
The real cause of suffering is not understanding suffering.

123
00:17:56,240 --> 00:18:06,480
Seeing things that are unsatisfying or unable to satisfy, you're not thinking they're going

124
00:18:06,480 --> 00:18:14,320
to satisfy. Because it's that which leads to the cause of suffering, which is a desire or

125
00:18:14,320 --> 00:18:28,960
thirst or craving. This ambition or this attraction to various experiences and concepts and objects.

126
00:18:36,240 --> 00:18:40,560
And so through understanding, through understanding and suffering, through the first noble truth,

127
00:18:40,560 --> 00:18:44,480
you really understand it. What happens is you abandon craving. It's quite simple.

128
00:18:48,000 --> 00:18:56,160
We're often concerned with giving up our addictions, giving up our desires and being more content,

129
00:18:56,160 --> 00:19:03,840
living a simpler life. And so we strive to free ourselves and to free ourselves from addiction.

130
00:19:05,360 --> 00:19:08,560
I mean, we even go about it all wrong. We don't have to worry about the addiction. Look at it.

131
00:19:08,560 --> 00:19:13,440
Look at the thing you're addicted to and we're afraid to because we think that's attractive,

132
00:19:13,440 --> 00:19:22,960
that's desirable. So in order to be free from it, you have to avoid it. You have to not think about

133
00:19:22,960 --> 00:19:29,440
that thing because that thing is worth that thing makes you happy. We know that ultimately it makes

134
00:19:29,440 --> 00:19:39,040
us unhappy, but we also know that we really are pleased by that somehow deep down in our hard

135
00:19:39,040 --> 00:19:46,160
of hearts who are attracted to it. Once we can't figure out because intellectually we know it's

136
00:19:46,160 --> 00:19:56,960
horrible for us. Alcohol is awful. I like it. So the wonderful thing about Buddhism is this great

137
00:19:56,960 --> 00:20:06,880
claim that it makes is that it's not attractive. It's that if you, the only reason we're attracted

138
00:20:06,880 --> 00:20:12,080
to things, anything is because we don't understand it because we haven't seen it clearly,

139
00:20:12,880 --> 00:20:22,640
that if you observe something, anything for what it is, as it is clearly, there's no way it's

140
00:20:22,640 --> 00:20:33,280
impossible for you to be attracted to it. It's not a powerful claim. It's not possible to like

141
00:20:33,280 --> 00:20:37,040
something if you understand it. It's not possible to be attached to something.

142
00:20:40,720 --> 00:20:45,600
It's the second normal truth. The third number truth is the cessation of suffering. This

143
00:20:45,600 --> 00:20:58,160
speaks to the real goal, right? The final goal, which is to escape. Don't like to talk about it

144
00:20:58,160 --> 00:21:03,040
exactly like that because it's easy to get the wrong idea that that should be our focus.

145
00:21:03,040 --> 00:21:08,720
That's runaway. Let's go to Canada and stay in the room in a basement and

146
00:21:08,720 --> 00:21:16,560
I won't have to deal with all the suffering of life. I'll be able to find the door. Get me out

147
00:21:16,560 --> 00:21:24,960
of here. There's no secret door in our basement that I found. There's no way out of suffering like

148
00:21:24,960 --> 00:21:33,840
that. The cessation of suffering, it's suffering of course comes when you stop craving things,

149
00:21:33,840 --> 00:21:41,200
which comes from when you understand them. Nonetheless, it is real and it's important to talk

150
00:21:41,200 --> 00:21:47,360
about cessation from suffering is real. The cessation of suffering is real. Freedom from suffering.

151
00:21:48,480 --> 00:21:56,560
Nibana is real. When you realize it, boy, it's it real. It's the most real thing, the most

152
00:21:56,560 --> 00:22:12,400
complete and the whole other category of existence. It's ineffable. It's indescribable.

153
00:22:14,240 --> 00:22:20,240
It's not all that hard to understand intellectually. It's just freedom, peace, cessation,

154
00:22:20,240 --> 00:22:26,480
but it doesn't do it. Come anywhere close to doing it justice.

155
00:22:32,800 --> 00:22:39,920
We have one goal and one goal only. There's no complicated goal in Buddhism. There's no esoteric

156
00:22:39,920 --> 00:22:53,760
or biased. There's nothing specifically Buddhist as a goal. The goal is very simple. It's the

157
00:22:53,760 --> 00:22:57,760
one goal that is of any use of any value to any of us. It's freedom from suffering.

158
00:23:01,040 --> 00:23:05,280
The fourth number of truths again is the path which I've already talked about.

159
00:23:05,280 --> 00:23:11,120
That was it. That was the first teaching in the Buddha. He goes on to say, there's one more

160
00:23:11,120 --> 00:23:19,920
thing as Daddy talks about how each of these truths is important to understand. There's something

161
00:23:19,920 --> 00:23:25,760
you have to do. There's a task to be done. As I mentioned, the first number of truths,

162
00:23:26,720 --> 00:23:32,560
suffering is to be understood, fully understood. Second, noble truths, the cause of suffering is

163
00:23:32,560 --> 00:23:39,520
to be abandoned, abandoned through understanding. Third, noble truths, the cessation of suffering

164
00:23:39,520 --> 00:23:44,480
is to be realized for yourself. Fourth, noble truths, the path is to be cultivated.

165
00:23:47,600 --> 00:23:50,960
And once suffering has been understood, fully understood, once

166
00:23:50,960 --> 00:24:00,320
it's, craving, desire has been fully abandoned. Once the cessation of suffering has been realized,

167
00:24:03,840 --> 00:24:10,400
and once the path has been followed in order to realize it, that's when one can say one is in light.

168
00:24:11,280 --> 00:24:18,240
Not before that. That's the Buddha's first teaching. That's the basics of Buddhism. That's really

169
00:24:18,240 --> 00:24:24,240
where Buddhism starts. If you want someone to understand Buddhism, this sort of thing is really,

170
00:24:24,960 --> 00:24:39,440
I don't know how, how, what's the word? I don't know how easily easy to understand this is for

171
00:24:39,440 --> 00:24:51,200
someone who's new to Buddhism, but there's nothing simple about Buddhism in the sense of

172
00:24:52,480 --> 00:24:59,760
easy for ordinary people to understand. If you haven't done some meditation, if you haven't begun

173
00:24:59,760 --> 00:25:05,440
to see reality, if you don't have a clear mind, it can be very difficult to understand Buddhism.

174
00:25:05,440 --> 00:25:11,120
Of course, once you practice, it's all quite very simple. For those of you who have been

175
00:25:11,120 --> 00:25:16,640
meditating, I don't think anything I've said tonight is, I'm sure it's hard to understand

176
00:25:16,640 --> 00:25:28,640
or esoteric in any way. So there you go. There's our dhamma for tonight. I hope that was useful.

177
00:25:28,640 --> 00:25:34,080
I hope the broadcast went okay. It looks like it's going fine. Figured everything out after

178
00:25:34,080 --> 00:25:45,280
last night. And I can't still get, I can't now get the audio working. Our audio broadcast somehow

179
00:25:45,280 --> 00:26:06,800
is broken. It won't let me, I don't know what happened there. So

180
00:26:15,280 --> 00:26:36,320
I don't know if there are any questions I'm happy to answer them.

181
00:26:45,280 --> 00:26:50,640
What advice can you give for meditation discipline to practice much more?

182
00:26:53,040 --> 00:26:56,800
I suppose I could. I get this question a lot, but my inclination is to say

183
00:26:58,160 --> 00:27:04,400
motivation is your job. It's not my job to motivate you. Yeah, that's not fair, is it? Teachers are

184
00:27:04,400 --> 00:27:10,080
supposed to motivate. But what I want to say is that I'm not going to hold your hand, then a teacher

185
00:27:10,080 --> 00:27:16,240
shouldn't hold your hand. What a teacher should do is give you this kick and a pants. I think it's

186
00:27:16,240 --> 00:27:23,760
quite motivating to tell you that you have to motivate yourself. I think that's the best advice I

187
00:27:23,760 --> 00:27:30,800
can give you for motivation. Stop asking me to motivate you, because it's lazy, right? Sorry,

188
00:27:30,800 --> 00:27:36,160
they don't mean to be critical, but I get this question a lot. I think this is how I'm inclined

189
00:27:36,160 --> 00:27:45,520
to answer it. See, it's more deep than that at the point being, it's not going to work out.

190
00:27:46,320 --> 00:27:52,080
There's no God. We come in the Western world very much entrenched in this idea, this is not

191
00:27:52,080 --> 00:27:59,680
even this idea, but this sense that there's a purpose to the universe. God has a plan for you.

192
00:27:59,680 --> 00:28:06,720
I mean, even if we're not Christian or Jewish, we still, we grew up in this sort of idea,

193
00:28:07,280 --> 00:28:16,080
this environment, and it's not true. Everything might go to garbage for you. You might

194
00:28:16,080 --> 00:28:20,000
wind up in hell and who knows where you're going in the future. It's not all going to work out.

195
00:28:20,000 --> 00:28:28,400
There is no happy ending unless you make it completely up to you. So that's really the one thing

196
00:28:28,400 --> 00:28:36,880
that a person has to do for themselves is motivate themselves. Think of that as your one job,

197
00:28:38,240 --> 00:28:47,760
to get motivated, to do it, to actually do it. And if you think like that, if you're focused on that,

198
00:28:48,560 --> 00:28:54,960
think everything else comes, right? Chanda is the first step when you want to do something,

199
00:28:54,960 --> 00:28:59,200
when you're interested in doing something, when you have the inclination to do something.

200
00:29:04,960 --> 00:29:14,880
Okay, we have lots of questions here. Okay, I guess I should go over a second life as well and see

201
00:29:14,880 --> 00:29:31,040
what's going on there. Are we doing over here a second life? Right. So there's sort of a question,

202
00:29:31,040 --> 00:29:35,440
we shouldn't run away from suffering, but the purpose of Buddhism is to escape suffering.

203
00:29:36,400 --> 00:29:43,520
Yes. The purpose of Buddhism is to be free from suffering. Right?

204
00:29:43,520 --> 00:29:48,160
It's much simpler than you're making out to be, I think. It's not really a contradiction.

205
00:29:50,640 --> 00:29:57,760
There's something wrong with us, and that's clear, or maybe not clear. I mean,

206
00:29:57,760 --> 00:30:05,200
Buddhism makes the claim that suffering is because of us. There's something wrong with us,

207
00:30:05,200 --> 00:30:10,160
and that's why we suffer. That's sort of a Buddhist claim. It's not because someone else is

208
00:30:10,160 --> 00:30:15,840
hurting us. It's not because of our situation. It's not because we're missing something. That's us.

209
00:30:15,840 --> 00:30:24,240
It's for something's wrong with us. So what we do is we try to figure that out. We try to understand

210
00:30:24,240 --> 00:30:31,120
that, to really figure out what's wrong with that. And the idea is once you understand it,

211
00:30:31,120 --> 00:30:33,360
you solve the problem. That's really all it is.

212
00:30:33,360 --> 00:30:41,280
So when we talk about being free from suffering and being free from the problem, well,

213
00:30:41,840 --> 00:30:45,440
if you want to be free from a problem, you really should try and figure out what the problem is,

214
00:30:45,440 --> 00:31:04,320
figure out what you're doing wrong, and then start doing it right, and so. I really need an assistant

215
00:31:04,320 --> 00:31:10,240
here to sort out all these questions. Maybe we shouldn't do the live chat questions. There's just

216
00:31:10,240 --> 00:31:17,440
too many of them. Make it more difficult. We should just do questions on our site. That's what we

217
00:31:17,440 --> 00:31:28,400
should do. Too many questions. And their questions I ask again and again. I answer again and again.

218
00:31:29,920 --> 00:31:34,480
Okay, so I'm going to be selective tonight. It's laying down okay to meditate. Yes, absolutely.

219
00:31:34,480 --> 00:31:45,200
Okay, we're living a life in this society. Yes, qualified, yes.

220
00:31:46,480 --> 00:31:50,080
If you become really enlightened, you start to leave society and get less interested in it.

221
00:31:50,080 --> 00:32:04,800
You're completely enlightened. You really can't engage in society in the way other people do.

222
00:32:04,800 --> 00:32:24,640
Yeah, you can certainly lie down. That's no problem.

223
00:32:35,680 --> 00:32:49,840
Pros and cons of being a monk in the West versus East. I don't know. That's not something I think

224
00:32:49,840 --> 00:32:58,320
about. Those aren't the sorts of questions I answer. I'm not a very good teacher. I'm very

225
00:32:58,320 --> 00:33:06,480
crabby and I'm very picky. I'm going to answer all your questions. How does meditation involve

226
00:33:06,480 --> 00:33:14,560
itself with that? Being able to control. I don't get it. It's me a simpler question.

227
00:33:20,560 --> 00:33:24,800
Whoa, and the sunko is good questions. Okay, let's delve into the sunko's question.

228
00:33:24,800 --> 00:33:30,800
Why are the sahayatana omitted from the Mahadim Nidhanasu? What do I look like? The Buddha,

229
00:33:30,800 --> 00:33:31,520
I can answer this.

230
00:33:37,280 --> 00:33:43,440
Yeah, and then I can answer what he taught what he did. I'd say Mahadim Nidhanasu. We're looking at

231
00:33:43,440 --> 00:33:58,240
the, yeah, this is pretty just a mupada. That's a good question.

232
00:33:59,840 --> 00:34:05,920
Well, the six senses are in contact. Contact is the six senses. That's what I would say quite

233
00:34:05,920 --> 00:34:21,840
simply. I'm surprised as you are that they're missing. It's a good point. Good question.

234
00:34:29,840 --> 00:34:32,800
This is a very terrible translation as usual.

235
00:34:32,800 --> 00:34:36,800
Tennis are because a really good monk, but it's not a very good translator. I'm sorry to say.

236
00:34:36,800 --> 00:34:42,960
I'm not the only one who thinks that. Let's see if I can find this. Which one is it? It's 15, right?

237
00:34:42,960 --> 00:34:43,440
Okay.

238
00:34:49,360 --> 00:34:51,040
D-N-F-T.

239
00:34:51,040 --> 00:34:59,200
Good Maurice Walsh, just to see if it's any better.

240
00:35:03,440 --> 00:35:09,200
The six senses are omitted for some reason in this sutta. That's what Maurice Walsh says.

241
00:35:09,200 --> 00:35:17,760
It's what the common dares, the common dares, the common dares, the common dares says.

242
00:35:19,280 --> 00:35:29,440
Indang, bhanayasma, salayatana, bajyati, bajyati, bajyati, jakusampasidana. Yeah, the six senses are

243
00:35:29,440 --> 00:35:34,720
contact or contact is the six senses. I mean, the brief scan of the common dary, it doesn't

244
00:35:34,720 --> 00:35:40,160
really say why he omitted them, it just says they're in there.

245
00:35:49,200 --> 00:35:53,280
Pasa-satsa, jasala, yatana-tou, a jiritang,

246
00:35:53,280 --> 00:36:01,200
and yam bv-satsapajyam dasti-tou, kamu.

247
00:36:03,280 --> 00:36:07,280
Yeah, I mean, I have to spend a little time translating that, but it's basically that they're

248
00:36:07,280 --> 00:36:14,560
together. Right? Because contact means contact with, of the sense, a contact at the moment of

249
00:36:14,560 --> 00:36:21,760
seeing at the moment of hearing. So someone might say, I'm not the be the one to say it, but I'll

250
00:36:21,760 --> 00:36:26,880
say someone might say that it was just a copyist error, that it actually should be in there,

251
00:36:26,880 --> 00:36:35,040
but in fact was, you know, an error of copying or transcribing or passing on the text.

252
00:36:38,160 --> 00:36:42,560
I mean, I have to accept that that is a possibility, even if it's Buddhist, we don't like that

253
00:36:42,560 --> 00:37:06,400
idea. Good variable, I have to do with the audience, that's a good point. Okay, there's too many questions

254
00:37:06,400 --> 00:37:13,840
I can't get through them all. Okay, someone here is trying to ask me a question, Dick Carly,

255
00:37:13,840 --> 00:37:18,960
I'll ask, but it's not really a question, so you have to give me something simpler. What do you mean?

256
00:37:19,840 --> 00:37:25,600
Do you believe in God? No. Do you believe in God? No. Buddhism with Hindu Yogi,

257
00:37:25,600 --> 00:37:30,080
Christianity, that's okay. Power to you, do as you like. I'm not here to tell you what to do.

258
00:37:30,080 --> 00:37:39,520
What do you think about rhythmic breathing? I don't, I don't teach rhythmic breathing.

259
00:37:39,520 --> 00:37:41,920
I don't believe in God, no.

260
00:37:41,920 --> 00:38:00,320
Do Westerners really understand meditation? Yeah, I don't know my question.

261
00:38:00,320 --> 00:38:15,200
Can a tourist go with Kaasaya robe in Thailand? Are you asking whether a non-month can wear

262
00:38:15,200 --> 00:38:30,880
the robe if they do? No, they'll get a big trouble for that. I think it arrested for that.

263
00:38:31,520 --> 00:38:34,320
I mean, you can, you just might get arrested if they find out.

264
00:38:34,320 --> 00:38:45,280
When do you think about kaholics anonymous? I, in fact, do sometimes think about

265
00:38:45,280 --> 00:38:51,680
alcoholics anonymous. I'm not convinced. I mean, I think it's impressive and I appreciate

266
00:38:54,560 --> 00:38:59,520
the good results that people get from it and how thoughtful it is and so on,

267
00:38:59,520 --> 00:39:08,400
but I think you can see how it's not exactly Buddhists. It might be effective to help people

268
00:39:08,400 --> 00:39:14,000
free themselves from alcohol and we'd want to say that some of the best parts of it are in line

269
00:39:14,000 --> 00:39:19,040
with Buddhism, but I think the whole reliance on the higher power is sketchy at best.

270
00:39:19,040 --> 00:39:29,280
What is the significance of the robe?

271
00:39:31,680 --> 00:39:36,800
Well, the robe, it's the lack of significance really. I mean, the significance is the simplicity

272
00:39:36,800 --> 00:39:41,840
of it. The robe that we wear is just a rectangle, the idea being we shouldn't wear anything

273
00:39:41,840 --> 00:39:54,240
more spancy than that, so I wear two rectangles of cloth. There's no sleeves, there's no buttons

274
00:39:56,800 --> 00:40:05,040
and it's sewn in a minute of pieces so that it's less valuable. It's not one big

275
00:40:05,040 --> 00:40:12,800
beautiful piece of cloth, it's pieces that have been stitched together.

276
00:40:17,200 --> 00:40:20,960
Yeah, there's lots of questions. This is quite surprising actually. I didn't realize

277
00:40:21,520 --> 00:40:27,840
opening up live chat would subject me to question after question after question.

278
00:40:27,840 --> 00:40:37,440
Why do many monks and I can answer that? Why do many monks think it's okay to use tobacco?

279
00:40:40,160 --> 00:40:44,960
I think many monks don't think it's okay to use tobacco, but they're addicted to it and it's

280
00:40:44,960 --> 00:40:57,600
sort of become a culture in Asia. I guess there is sort of a tradition where Buddhism was not

281
00:40:57,600 --> 00:41:02,160
very strong. I think is the answer and it just became a tradition for monks to smoke.

282
00:41:05,200 --> 00:41:10,480
But it's hard as a Western to understand it and I think most of them realize it's not good

283
00:41:11,280 --> 00:41:15,680
but they're addicted. I'm going to not really meditating is the point. I mean, I think we have

284
00:41:15,680 --> 00:41:28,880
this illusion about monks is that they all meditate all the time and it's not really true.

285
00:41:32,080 --> 00:41:37,440
Well, it's significant and that wearing a robe means you're a monk. So if you pretend to be a

286
00:41:37,440 --> 00:41:47,440
monk for Thai people that's significant, what is your view on solipsism? I had this funny experience

287
00:41:47,440 --> 00:42:01,520
in New York. I said as Buddhists, I don't think Buddhism is against solipsism. I don't think

288
00:42:01,520 --> 00:42:09,360
we can know whether we're in the solipsistic universe. I don't think it matters, but theoretically

289
00:42:11,200 --> 00:42:17,280
and this monk I was sitting beside was horrified. A Western monk was quite surprised at how

290
00:42:17,280 --> 00:42:24,880
vehemently against solipsism he was. I mean, to me solipsism is just a mind game.

291
00:42:24,880 --> 00:42:32,480
It doesn't really mean anything. Whether I am the only real being and you are all just

292
00:42:33,040 --> 00:42:38,400
emanations of my reality, it doesn't really matter to a Buddhist because the experience of it

293
00:42:38,400 --> 00:42:42,800
is what it is and it's only the experience that we're interested in.

294
00:42:42,800 --> 00:42:52,160
So it's just something some mind candy for us to ponder on and get caught up in the lost in.

295
00:43:01,280 --> 00:43:07,120
Did the Buddha have dreams? Yes, the Buddha had dreams and some very special dreams.

296
00:43:07,120 --> 00:43:12,640
No, they're just getting silly.

297
00:43:21,440 --> 00:43:27,040
Okay, too many questions. Do many questions. Ask them I'm not going to answer have enough.

298
00:43:28,320 --> 00:43:34,720
You really want to ask a question. We'll prioritize those questions on our website.

299
00:43:34,720 --> 00:43:39,920
So you can go to meditation.ceremungalow.org. Let me see how I'll post a link.

300
00:43:41,440 --> 00:43:50,640
You'll sign up for our site. Join our meditation community.

301
00:43:55,200 --> 00:43:56,480
There's our site.

302
00:43:56,480 --> 00:44:05,360
Did the Buddha have dreams after his enlightenment? Maybe not. I'm not quite sure about that.

303
00:44:06,400 --> 00:44:13,120
It's a good question. You may not have. I think my teacher once said that our hands don't dream.

304
00:44:13,120 --> 00:44:23,200
I think it is a specific idea. I mean, it would be kind of silly if they did, right?

305
00:44:23,200 --> 00:44:39,120
So it would have to be a sense of distraction and a mind that is not completely at peace.

306
00:44:39,120 --> 00:44:54,320
You're okay. Well, thank you all for tuning in. Do you have any more here?

307
00:44:54,320 --> 00:45:16,480
No. Okay. Well, thank you all for coming out. Have a good night. See you all next time.

