WEBVTT

00:00.000 --> 00:14.000
Okay, good evening, everyone. Welcome to our daily Dhamma broadcast.

00:17.000 --> 00:28.400
Today I thought I'd go back to the basics last night. I was mentioned a little

00:28.400 --> 00:40.900
bit about the experience of being mindful, how it changes your perception of the

00:40.900 --> 00:58.200
experience or ordinary perception of experiences are often clouded, it's wrong to put

00:58.200 --> 01:06.500
it bluntly. And so I wanted to talk a little bit about the experience of

01:06.500 --> 01:14.840
mindfulness, the very basics of our practice of mindfulness. Of course to do

01:14.840 --> 01:29.000
that we go back to the Satipatana Sutta in the Digani Kaya, number 22, and then

01:29.000 --> 01:54.160
in the Magimani Kaya, number 10. And so at the very beginning of this sutta, of

01:54.160 --> 02:00.360
course we have the aikai anoyang bhikkhuimang, who we have the buddhas expression of why

02:00.360 --> 02:07.200
he's teaching mindfulness to purify beings and for the purification, right,

02:07.200 --> 02:21.320
and the mind for overcoming mental illness, sorrow, limitation, despair,

02:21.320 --> 02:30.800
overcoming mental and physical suffering, for finding the right path and for

02:30.800 --> 02:37.440
seeing the Bhana. With any dives right in to what it's like, what does it mean

02:37.440 --> 02:45.520
to be mindful? And he lays out the four Satipatana. So the four Satipatana, it's not

02:45.520 --> 02:51.640
important that you're able to categorize. The point is not to be able to

02:51.640 --> 02:56.440
recognize this is this Satipatana. Now I'm practicing this one, now I'm practicing

02:56.440 --> 03:05.920
that one. It's more of a map laying out the framework by describing these four

03:05.920 --> 03:12.760
different aspects of experience or four different types of experience or ways

03:12.760 --> 03:20.440
of looking at experience. It really flashes out the whole sphere of what it

03:20.440 --> 03:26.200
means to experience. So you can look at experience physically. There's a

03:26.200 --> 03:33.400
physical aspect arising and falling in the stomach that's physical. The lifting

03:33.400 --> 03:37.440
of the foot, the feeling of the tension and the leg, the feeling of the foot

03:37.440 --> 03:43.360
pressing against the floor, that's physical. Even just the feeling of sitting

03:43.360 --> 03:53.560
still, the tension in the back and the pressure against the floor. Heat and cold,

03:53.560 --> 04:00.040
that's all physical. That's Ka Ya Ka Ya Nupasiri, Haratya Wonderwells, seeing the body as

04:00.040 --> 04:06.960
body. You are in the body when it sees it as body in regards to the body when

04:06.960 --> 04:16.800
it sees body. We denies the second one. So if we focus on the feeling aspect of

04:16.800 --> 04:24.360
experience, if you'll pain, to be mindful of it as pain, if you'll happy to

04:24.360 --> 04:32.200
know to be mindful of that, if you'll calm, so the three main feelings. We

04:32.200 --> 04:36.960
need to know how you experience something to experience with us pleasant, as

04:36.960 --> 04:46.040
painful, or as neutral. If you focus on the mental aspect of experience, ji-ti-ti-ta-na-pa-si-vi-am-a-ti,

04:46.040 --> 04:52.280
thinking about the past, or future of good thoughts, bad thoughts. It doesn't

04:52.280 --> 05:01.640
matter what kind of thought. It's all thinking. And then dhamma, which is a little bit unique. It

05:01.640 --> 05:07.280
deals with, it's different in quality than the other. Some of it deals very much

05:07.280 --> 05:12.560
with present experience. For example, the hindrances. It's a very much a part of our

05:12.560 --> 05:18.120
experience, liking, disliking, drowsiness, distraction, doubt. There's much more in

05:18.120 --> 05:22.600
there. There's the senses. Again, that's part of it. But then it gets on to the,

05:22.600 --> 05:30.440
there's the aggregates. There's the bowjungas. There's the truth's noble truth.

05:30.440 --> 05:36.640
So it's much more about, it's much more doctrinal, which is why I think it's

05:36.640 --> 05:41.720
called dhamma. Anyway, it doesn't really matter. The essence of our practice is

05:41.720 --> 05:48.440
our experience, based on the force that you put on. But it's the quality that's

05:48.440 --> 05:58.040
in most interesting, because quantities fairly straightforward, right? You can

05:58.040 --> 06:01.840
tell me how many hours of meditation the meditators will often tell me how

06:01.840 --> 06:06.360
many hours of meditation they've done. You often tell the story of how I

06:06.360 --> 06:10.840
subbed in at a center that was quite adamant about the number of hours

06:10.840 --> 06:16.720
meditators should do a day. So they would assign a number of hours. And I have to

06:16.720 --> 06:20.040
say I don't find this to be incredibly helpful, because it wasn't really

06:20.040 --> 06:25.000
working, at least for the foreign meditators who would spend all the time

06:25.000 --> 06:32.840
outside of their hours doing nothing. So I taught them how to be mindful from

06:32.840 --> 06:37.680
morning to night. I told them, today you have to do 18 hours of meditation, which

06:37.680 --> 06:41.200
means I don't care how much form of meditation you do, as much as you can come

06:41.200 --> 06:48.240
to me. But outside of that time, I want you to be mindful to have the quality

06:48.240 --> 06:55.320
of mindfulness. Hours are nothing. Come walk and sit for hours. In the end, it's

06:55.320 --> 07:01.040
both quality and quantity. On the other end, because on the other hand, as we'll

07:01.040 --> 07:05.840
see as I go through these, what we don't want is to become complacent. We don't

07:05.840 --> 07:10.360
want to think that I'm mindful, and that's good enough. Mindfulness doesn't work

07:10.360 --> 07:19.160
that way. So we have three qualities. What it means to be mindful. A-t-p-samp-a-jah-n-o-sap-ti-ma.

07:19.160 --> 07:29.760
I'm going to go through these briefly. A-t-p is a word that relates to temperature,

07:29.760 --> 07:40.640
heat. We use the word, or that's where the etymology of it is. So it's used in

07:40.640 --> 07:47.600
a sense of exertion. In the commentaries, I think, refer to it as that which

07:47.600 --> 07:55.520
burns up the defilements. That's apt because we become complacent even in

07:55.520 --> 08:01.160
meditation. Even if at some moment we're very mindful, this is how mindfulness

08:01.160 --> 08:05.560
becomes a hindrance, because you're mindful, and then you cling to the fact that

08:05.560 --> 08:11.960
you're mindful. Meaning you become content. Oh, yes, I'm very mindful now, and

08:11.960 --> 08:15.920
you're not anymore at that moment once you think about it, once you get

08:15.920 --> 08:22.680
attached to it, you're immediately no longer mindful. So A-t-p is the effort that

08:22.680 --> 08:31.760
we put out to build up quantity and quality. When both quality and quantity,

08:31.760 --> 08:37.600
which means you can't just practice for hours without having the mental

08:37.600 --> 08:43.360
energy to see the rising and the falling. Put the mind with the object. When you

08:43.360 --> 08:47.920
walk, is your mind with the foot? Are you aware of the foot moving? Or are you

08:47.920 --> 08:53.320
letting your mind wander? And then quantity, are you doing it again, the next

08:53.320 --> 08:59.320
moment? Are you moving from, are you mindful of the next experience in the

08:59.320 --> 09:05.120
next experience? It takes an incredible amount of effort, an incredible amount of

09:05.120 --> 09:14.440
mental rectitude. So a brightness of mind, straightness of mind, to keep

09:14.440 --> 09:20.400
yourself from sliding back into, oh, it's okay, I'll just take it easy for

09:20.400 --> 09:26.040
a bit, right? Something you have to train it. Something that requires quantity to

09:26.040 --> 09:33.240
get good at. You have to do it again and again and to really gain concentration,

09:33.240 --> 09:36.680
which is not mentioned here, but to get to the point where you're focused,

09:36.680 --> 09:44.560
does what everyone understands is important in meditation. You have to practice,

09:44.560 --> 09:48.960
you have to cultivate it, repeatedly, moment after moment. You can't like, you can't

09:48.960 --> 09:51.440
slack off.

09:54.440 --> 10:02.680
Sampajana means to know something, but it's sampah, these are prefixes to the

10:02.680 --> 10:08.680
word chanyam, chanyam just means to know sampajana means to know something. But

10:08.680 --> 10:18.360
sampap, I mean, they're like, regarding wisdom, really. But in the context, in a

10:18.360 --> 10:22.960
way, they're used in the sati patanasu, it seems to refer to a good way of

10:22.960 --> 10:33.360
understanding of being aware, or self-awareness, it's as word that's often used. Meaning,

10:33.360 --> 10:37.640
the knowledge that we're looking for is not like the knowledge that I am a monk

10:37.640 --> 10:44.680
or that I am a man or that I am human or that I'm living in Hamilton. I mean,

10:44.680 --> 10:52.000
these are all knowledges, but they're not sampajana. Things we know about the past

10:52.000 --> 10:55.760
or the future are also not sampajana. Sampajana is a really good word for

10:55.760 --> 11:01.360
reminding us of the present moment, reminding us that mindfulness has to be

11:01.360 --> 11:07.600
done now, has to refer to the present. What's happening now? If you are aware

11:07.600 --> 11:17.920
of what's happening now, meaning you see things, you see what is as it is. You're

11:17.920 --> 11:24.080
not lost in your head, judging it or reacting to it, you're clearly aware of

11:24.080 --> 11:31.840
the experience. That's sampajana. It's a way of using the word knowledge, but it

11:31.840 --> 11:37.600
means awareness. Do you really know what's going on? Do you really know what's

11:37.600 --> 11:45.880
happening? Know in the sense of being aware of it. And, finally, satimah,

11:45.880 --> 11:55.240
satimah is really the key. It's what cements your awareness. It's what keeps you

11:55.240 --> 12:02.600
straight. It's what keeps your mind from floating away, from drifting away. It

12:02.600 --> 12:07.640
keeps your mind fixed on the object. Mindfulness is what sati is what

12:07.640 --> 12:13.000
grasps the object, but it's grasps in the way we use it as word in English when

12:13.000 --> 12:17.800
you grasp a concept. Someone tells you something and they try to explain

12:17.800 --> 12:23.320
something and when you grasp, they ask, have a good grasp of what I explained to you.

12:23.320 --> 12:28.760
We use the same thing to apply to experience. We all experience in more aware of

12:28.760 --> 12:34.200
our experiences without mindfulness, without sati. Sati isn't just awareness.

12:34.200 --> 12:39.240
Those dogs are aware of you. Does a dog know whether it's walking? Yes.

12:39.240 --> 12:45.880
The dog knows that it's walking. We all know what's happening. If someone

12:45.880 --> 12:51.320
rings the doorbell, we all are aware of the sound. The awareness is there.

12:51.320 --> 12:57.640
It's not yet mindfulness. It's not yet sati patana. Sati patana is where you

12:57.640 --> 13:06.840
fix your sati. Fix your mind on the object. Remembering it.

13:06.840 --> 13:15.960
Sati really means remembering. Remembering. The approximate cause of sati is

13:15.960 --> 13:23.960
something we call tira sunya. Sunya is a recognition of something. When you see

13:23.960 --> 13:28.760
this is this, that is that. So when you know the doorbell is ringing and you hear

13:28.760 --> 13:38.680
the sound, there's a sunya. Ah, yes, that's a sound. You recognize it.

13:38.680 --> 13:46.120
Tira sunya is where you tira means you augment or you reaffirm that perception.

13:46.120 --> 13:51.000
So it can refer to a concept. Like if you say there's a Buddha, Buddha thinking of

13:51.000 --> 13:54.280
the Buddha, or if there's a doorbell and you think of yourself,

13:54.280 --> 14:01.480
doorbell, doorbell, doorbell. That's not, that's mindfulness, but it's not,

14:01.480 --> 14:07.000
it's not we passana. So it is still mindfulness. It's not sati patana in the way

14:07.000 --> 14:11.080
the Buddha is explaining it in the sutta. Although parts of the sutta are

14:11.080 --> 14:15.400
samata, like the cemetery contemplation. It's the real core of it is we passana,

14:15.400 --> 14:21.560
which means the object has to be experienced. So you can't say doorbell,

14:21.560 --> 14:26.280
doorbell. You have to say hearing hearing is that the experience. But when you

14:26.280 --> 14:29.960
remind yourself hearing, you're augmenting your strengthening that

14:29.960 --> 14:34.600
perception, you're fixing your mind and you're grasping that perception

14:34.600 --> 14:40.200
fully and completely. And that's what allows this change in perception. That's

14:40.200 --> 14:46.040
what allows us to see clearly where before we were

14:46.040 --> 14:49.880
confused or mistaken, the things that we thought brought us happiness

14:49.880 --> 14:54.840
when we apply mindfulness. And it's the only way

14:54.840 --> 14:58.520
we can break through the veil of ignorance,

14:58.520 --> 15:07.000
to see things in a whole new way. It's quite, quite eye opening.

15:08.200 --> 15:11.720
So that's the what the Buddha says in the first section. Then he starts talking

15:11.720 --> 15:15.240
about the various, he goes through each of the four sutta,

15:15.240 --> 15:19.720
in various ways, various ways, by which one can practice.

15:19.720 --> 15:28.520
Sutta, at the end of most of the sections he says, I think all of the sections he says.

15:28.520 --> 15:31.160
For example, for the body he says,

15:31.160 --> 15:36.520
one sees ityajatangwa kaya kaya nupasimyaji. So one dwells

15:36.520 --> 15:40.280
in regards to the body, seeing it as body, seeing

15:40.280 --> 15:46.680
it as body. And then he once sees the

15:46.680 --> 15:51.080
arising of body and the ceasing of body. One

15:51.080 --> 15:54.520
watches, meaning one is really aware of the experience

15:54.520 --> 15:57.640
from beginning to end. I mean this is really what makes this an

15:57.640 --> 16:01.320
experiential practice. It's not about knowing, okay, now I'm walking, it's about

16:01.320 --> 16:02.280
really

16:02.280 --> 16:06.120
experiencing the beginning of a movement in the end of a movement.

16:06.120 --> 16:17.080
And then at the end he says something that I

16:17.080 --> 16:19.800
really am quite interested in,

16:19.800 --> 16:24.680
is atikayotiwapanasatipatupatitahoti.

16:24.680 --> 16:27.800
One establishes mindfulness just to the extent

16:27.800 --> 16:31.480
atikayo, there is the body, or this is body.

16:31.480 --> 16:35.480
I mean really it is what it is, it's really the point.

16:35.480 --> 16:40.120
Not it is body, but that thing is what it is.

16:40.120 --> 16:44.680
Body is body, walking is walking,

16:44.680 --> 16:47.720
and so on. In the sampaganya Baba, he says,

16:47.720 --> 16:51.800
kachan'to kachami di vajamati, when going,

16:51.800 --> 16:56.440
one knows I am walking, kachami.

16:56.440 --> 17:00.280
Polygrammar is different from English, so one word means I am walking. In English,

17:00.280 --> 17:04.440
we just say walking, walking. But in Poly you say kachami,

17:04.440 --> 17:06.920
which also has the first person,

17:06.920 --> 17:13.880
the first person, subject, embedded in it.

17:17.800 --> 17:20.680
And then yavadeva nyanamataya,

17:20.680 --> 17:25.000
pati sati mataya, just for knowledge,

17:25.000 --> 17:31.080
just enough for knowledge, and pati sati, which means

17:31.080 --> 17:34.600
not quite sure, but he used to like this word,

17:34.600 --> 17:39.960
pati sati means just mindfulness, just being mindful.

17:39.960 --> 17:44.920
This is the whole seeing things as they are, it's not adding anything.

17:44.920 --> 17:50.120
So instead of saying this is good, this is bad, this is me, this is mine.

17:50.120 --> 17:55.720
We say this is this, and if you're aware of that, that moment is mindfulness.

17:55.720 --> 18:02.520
If you're aware of things as they are.

18:02.520 --> 18:05.400
Oh, I missed something from the last section, it's another important thing

18:05.400 --> 18:08.760
that anyway fits with what I'm going to say next, but the Buddha says,

18:08.760 --> 18:11.880
we nayya logay, a bhija dhammanasana,

18:11.880 --> 18:16.040
it's just important to mention.

18:16.040 --> 18:23.720
By practicing sati, we are overcoming the ideas to overcome judgment, really,

18:23.720 --> 18:27.560
liking and disliking, because when you don't have agentangi,

18:27.560 --> 18:29.240
he picks this apart.

18:29.240 --> 18:39.080
So this section, for that passage, it says one puts aside,

18:39.080 --> 18:43.720
liking and disliking, greed and anger, really, is what it's saying.

18:43.720 --> 18:48.520
And then the question is, what about delusion?

18:48.520 --> 18:51.960
Because there are three bases for unholsiveness, and they're

18:51.960 --> 18:54.280
greed, anger, and delusion.

18:54.280 --> 18:56.680
Why isn't delusion here?

18:56.680 --> 19:01.560
And as I mentioned last night, this is because of this concept of mindfulness

19:01.560 --> 19:03.320
being like a light.

19:03.320 --> 19:09.480
So greed and anger rely upon delusion, but delusion can't exist with mindfulness.

19:09.480 --> 19:12.600
So when your mindful delusion is gone,

19:12.600 --> 19:16.200
you can still be mindful of greed and anger,

19:16.200 --> 19:23.560
at least the way they present themselves in your experience.

19:23.560 --> 19:29.080
But at the moment, when your mindful delusion is already gone,

19:29.080 --> 19:31.800
it's the opposite mindfulness, it's the opposite of delusion.

19:35.640 --> 19:38.840
And then he says, Anisito, Joviyara, they related to this.

19:38.840 --> 19:46.600
Anisito, one dwells independent.

19:46.600 --> 19:51.720
I remember earlier a few days ago I was talking about the duality,

19:51.720 --> 19:58.280
and one of the dualities is how dependence, he is vulnerability, the Buddha said.

19:58.280 --> 20:01.240
Those who are dependent are vulnerable.

20:01.240 --> 20:05.720
Those who are independent are invincible, invulnerable.

20:05.720 --> 20:09.720
And you see this in various places, and it's a very

20:09.720 --> 20:17.720
good description of the power of mindfulness

20:17.720 --> 20:22.520
that we have here a practice that makes you completely invincible,

20:22.520 --> 20:33.400
completely impervious to suffering, impervious to the trials and tribulations

20:33.400 --> 20:37.640
of Samsara.

20:37.640 --> 20:43.880
Not Jikinji low-gay Upadi at the one clings to nothing in this world.

20:46.280 --> 20:48.280
This is how you do it.

20:48.280 --> 20:52.040
You want to be free from suffering. If you want to be invincible,

20:52.040 --> 20:55.400
don't cling to anything. It's really a note.

20:55.400 --> 20:59.400
The Buddha said, this is the only thing you really need to know about Buddhism.

20:59.400 --> 21:01.560
If you want to practice, the only thing you really need to know

21:01.560 --> 21:03.880
before you start learning how to meditate,

21:03.880 --> 21:06.680
Sambhi Dhamma, na-la-la-la-la-la-la-la-la-la-la-la.

21:06.680 --> 21:12.920
No Dhamma is nothing, no thing is worth clinging to.

21:15.880 --> 21:21.640
Ayyuan go bekou-ve-pikou-kai-ga-ya-na-pa-seviyat.

21:21.640 --> 21:24.440
This is how one practice is mindfulness.

21:24.440 --> 21:30.520
This is how one practice is to be mindful of the four foundations of mindfulness.

21:30.520 --> 21:34.520
So it's quite simple. There's not a lot to it.

21:34.520 --> 21:44.520
It's also quite concrete, the difference between mindfulness and ordinary experience.

21:44.520 --> 21:54.520
Mindfulness is a deliberate, intentional, active moment-by-moment, active activity.

21:54.520 --> 22:00.520
It's like pounding away at metal.

22:00.520 --> 22:08.520
Buddha used this simile of the analogy of pounding a blacksmith

22:08.520 --> 22:12.520
when they heat it up and they pound it.

22:12.520 --> 22:16.520
Every time you're mindful, it's like you pick up the hammer and you hit the metal.

22:18.520 --> 22:21.520
You can't hit it once and then sit there and watch it and say,

22:21.520 --> 22:26.520
boy would a good hit that was or think that you did something,

22:26.520 --> 22:31.520
you have to keep every time it has to be the same, every moment,

22:31.520 --> 22:35.520
every time that you hit it it becomes straighter.

22:35.520 --> 22:42.520
Mindfulness is this tool that we use to straighten our minds.

22:42.520 --> 22:44.520
So we apply a moment-by-moment.

22:44.520 --> 22:48.520
Obviously it's in the beginning for sure it's going to be

22:48.520 --> 22:53.520
hit and miss some moments mindful, some moments not mindful.

22:53.520 --> 22:57.520
It's very important to see the clear distinction there.

22:57.520 --> 22:59.520
You can only measure moments.

22:59.520 --> 23:03.520
You can't measure hours, days, months.

23:03.520 --> 23:06.520
We talk about how many years they've been meditating.

23:06.520 --> 23:08.520
It's not really that useful.

23:08.520 --> 23:11.520
It's useful to talk about is the moments.

23:11.520 --> 23:15.520
How good are you being mindful moment to moment?

23:15.520 --> 23:20.520
How many moments a day are you mindful?

23:20.520 --> 23:25.520
So there you go a little bit about mindfulness tonight, again basics,

23:25.520 --> 23:29.520
but the most important part of our teaching really.

23:29.520 --> 23:32.520
So thank you all for tuning in.

23:32.520 --> 23:47.520
We'll show you all the best.

