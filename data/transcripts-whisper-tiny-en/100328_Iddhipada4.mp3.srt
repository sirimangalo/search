1
00:00:00,000 --> 00:00:28,400
In the practice of meditation, as with everything else we do, we have to not only practice

2
00:00:28,400 --> 00:00:43,000
not only dedicate our time to the practice, but we also have to practice in such a way

3
00:00:43,000 --> 00:00:49,900
as to be sure that we're going to get results from the practice.

4
00:00:49,900 --> 00:01:01,200
It's not true that simply walking and sitting for long periods at a time or long extended

5
00:01:01,200 --> 00:01:14,600
period of days and weeks and months and years is sure to bring results.

6
00:01:14,600 --> 00:01:19,000
It goes without saying that it should go without saying that our practice needs to have

7
00:01:19,000 --> 00:01:31,200
certain qualities about it in order for us to guarantee some kind of success and some

8
00:01:31,200 --> 00:01:37,800
sort of results from our practice.

9
00:01:37,800 --> 00:01:50,760
Why this is, is because we, in order to succeed in the practice, in order to gain

10
00:01:50,760 --> 00:02:01,400
results, we have to first gain this strength and a certain fortitude of mind.

11
00:02:01,400 --> 00:02:06,600
Our competence has to be very strong, our effort has to be very strong.

12
00:02:06,600 --> 00:02:11,640
Our mindfulness has to be very strong, our concentration has to be very strong and our

13
00:02:11,640 --> 00:02:15,640
wisdom has to be very strong.

14
00:02:15,640 --> 00:02:29,400
Without these five strengths, our mind is too weak, our mind is unable to overcome the evil,

15
00:02:29,400 --> 00:02:35,200
the unhoseness, the darkness and the cloudiveness that's inside of us.

16
00:02:35,200 --> 00:02:44,480
We won't be able to seek clearly as a result of the defilements which we carry around.

17
00:02:44,480 --> 00:02:49,800
And it goes without saying that our ordinary state of mind is not strong enough.

18
00:02:49,800 --> 00:02:59,400
This weak in many ways is lacking in many of these qualities.

19
00:02:59,400 --> 00:03:04,320
And so unless we actually change our minds or build up this strength and the sportitude

20
00:03:04,320 --> 00:03:12,480
of mind, we can practice for years and years and years and never truly become enlightened.

21
00:03:12,480 --> 00:03:31,720
We have to work to gain these qualities of mind, we have to work again and again.

22
00:03:31,720 --> 00:03:48,840
And so sometimes we might ask, well what are the qualities that we need to have, we need

23
00:03:48,840 --> 00:03:54,560
to possess in order to be sure that our practice is going to be a success, is going

24
00:03:54,560 --> 00:03:56,240
to bring result.

25
00:03:56,240 --> 00:04:03,880
And so we have this very famous group of qualities that the Lord Buddha said are a guarantee

26
00:04:03,880 --> 00:04:14,520
of success, given all other things being equal.

27
00:04:14,520 --> 00:04:22,160
If there is the ability to succeed in something, if it's not blocked by fate or circumstance,

28
00:04:22,160 --> 00:04:28,240
or as we say in Buddhism as we would explain it in Buddhism karma, if there is still

29
00:04:28,240 --> 00:04:38,000
a chance to succeed in something, as there surely is with all of us in regards to meditation,

30
00:04:38,000 --> 00:04:45,960
then it is these four dhammas, this set of dhammas of qualities of mind that will make

31
00:04:45,960 --> 00:04:52,520
or break our practice, that will allow us to succeed or cause us to fail.

32
00:04:52,520 --> 00:04:57,800
Their presence will allow us to succeed, their absence will cause us to fail again and again,

33
00:04:57,800 --> 00:05:06,920
until finally we can incorporate these four qualities of mind into our practice.

34
00:05:06,920 --> 00:05:16,000
So these four are called the iti pada, iti means power or strength or success or fortitude

35
00:05:16,000 --> 00:05:22,800
of mind, as I said, the power that we are looking for, to have strong confidence, strong

36
00:05:22,800 --> 00:05:29,320
effort, strong mindfulness, strong concentration and strong wisdom, the strength.

37
00:05:29,320 --> 00:05:47,760
What that means, the path or the practice or the items, or those things that lead to.

38
00:05:47,760 --> 00:05:53,640
So these four dhammas are very important, what are the four?

39
00:05:53,640 --> 00:06:07,360
The first is called chanda, chanda which means a zeal or contentment or interest, you

40
00:06:07,360 --> 00:06:16,800
could say, our interest in practicing, desire to practice, our contentment with practice

41
00:06:16,800 --> 00:06:28,840
or intention, you could say, intention to practice, wholeheartedness, we are chanda, so

42
00:06:28,840 --> 00:06:35,200
I guess simply it just means desire, our desire to practice.

43
00:06:35,200 --> 00:06:45,600
Number two, vitya, which means effort, the effort to practice.

44
00:06:45,600 --> 00:06:55,520
The three is chita, chita which means the mind or keeping something in mind, thinking

45
00:06:55,520 --> 00:07:03,040
about something, you could say it means thought, thinking about the practice, keeping

46
00:07:03,040 --> 00:07:06,640
the practice in mind.

47
00:07:06,640 --> 00:07:16,800
Number four, vimangsa, vimangsa, which means considering consideration or contemplation,

48
00:07:16,800 --> 00:07:32,080
analyzation, and analysis of the practice, analyzing our practice.

49
00:07:32,080 --> 00:07:38,320
And in fact, these four dhammas don't particularly relate to the practice indirectly at

50
00:07:38,320 --> 00:07:39,320
all.

51
00:07:39,320 --> 00:07:41,560
They can be used for anything in the world.

52
00:07:41,560 --> 00:07:49,360
Whatever we apply these four dhammas to, we can be sure to succeed.

53
00:07:49,360 --> 00:07:55,520
For instance, if you wish to do evil deeds, you need these four dhammas as well, you need

54
00:07:55,520 --> 00:08:00,760
to have the interest or the desire to do evil deeds, you have to put out effort to

55
00:08:00,760 --> 00:08:09,920
do evil deeds, you have to think about it and keep it in mind, focus on it, and you have

56
00:08:09,920 --> 00:08:22,160
to analyze the act to figure out how to do it properly, how to make sure it succeeds.

57
00:08:22,160 --> 00:08:26,560
Whatever we want to do in the world, we want to be successful in business, we need

58
00:08:26,560 --> 00:08:33,800
these four, we need to have the desire to be busy, to conduct business, we need to have

59
00:08:33,800 --> 00:08:40,240
the effort, we need to have the concentration, the focus, keeping it in mind, and we need

60
00:08:40,240 --> 00:08:50,120
to consider the causes and effects and make a plan of what we're going to run our business,

61
00:08:50,120 --> 00:08:58,360
analyzing the terms of event and as things as they come.

62
00:08:58,360 --> 00:09:02,440
So these four dhammas are very useful things to remember and to think about, no matter

63
00:09:02,440 --> 00:09:06,800
what we do, but most especially in the meditation practice, they have a great meaning and

64
00:09:06,800 --> 00:09:10,040
a great importance.

65
00:09:10,040 --> 00:09:16,040
Chanda means that we actually want to practice, and this is often a criticism of Buddhist

66
00:09:16,040 --> 00:09:22,000
meditation by people who have studied the books and are perhaps looking for something

67
00:09:22,000 --> 00:09:30,240
to criticize and they say that you're looking for the end of desire and yet you admit

68
00:09:30,240 --> 00:09:35,000
that you yourself have the desire to practice.

69
00:09:35,000 --> 00:09:39,360
And this is really a silly argument, one that baffles many people and makes them think

70
00:09:39,360 --> 00:09:44,600
that it's really true that this is an impossible task.

71
00:09:44,600 --> 00:09:50,920
But really the word desire in this case is not really desire, we're talking about two uses

72
00:09:50,920 --> 00:09:51,920
of the word desire.

73
00:09:51,920 --> 00:10:00,120
When we talk about giving up desire, we need desire for something, for some experience,

74
00:10:00,120 --> 00:10:03,280
for some state, for some phenomenon to arise.

75
00:10:03,280 --> 00:10:10,720
When we talk about the desire to practice, we mean simply an intention to practice, based

76
00:10:10,720 --> 00:10:16,000
on the knowledge that it's a good thing to do, based on the knowledge that desire is a

77
00:10:16,000 --> 00:10:21,080
bad thing and that only through contemplation are we going to be free from desire, from

78
00:10:21,080 --> 00:10:27,640
that desire, it's the difference between an attachment to something and an intention to do

79
00:10:27,640 --> 00:10:31,760
something, to carry out some act.

80
00:10:31,760 --> 00:10:38,720
When we desire for an object, then we carry out all sorts of bad deeds in order to get

81
00:10:38,720 --> 00:10:41,800
it.

82
00:10:41,800 --> 00:10:46,120
But when we desire to do something, it doesn't have to be a desire at all.

83
00:10:46,120 --> 00:10:51,680
In fact, it's very hard to think of anyone actually desiring to practice, we pass in the

84
00:10:51,680 --> 00:10:52,680
meditation.

85
00:10:52,680 --> 00:10:59,240
In fact, if they did, you could say it would be a wrong, it would be an unwholesome sort

86
00:10:59,240 --> 00:11:01,280
of mind state.

87
00:11:01,280 --> 00:11:05,600
But what we mean by the fact when we say we desire, we want to meditate.

88
00:11:05,600 --> 00:11:11,120
It's not that we have this desire or this attachment to meditating, it's the fact that

89
00:11:11,120 --> 00:11:16,640
we realize that if we don't meditate and we don't do it in earnest, we're going to slip back

90
00:11:16,640 --> 00:11:23,240
into all sorts of unwholesome states and conduct ourselves wrongly and give rise to all sorts

91
00:11:23,240 --> 00:11:29,400
of suffering and guilt and unhappiness in the future.

92
00:11:29,400 --> 00:11:35,840
So it's because of this that people want to practice meditation or intend to practice meditation.

93
00:11:35,840 --> 00:11:39,000
In fact, this doesn't come easily in regards to meditation.

94
00:11:39,000 --> 00:11:41,360
Most of us have a hard time meditating.

95
00:11:41,360 --> 00:11:52,040
When we come to meditation in the beginning, it's a very unpleasant practice.

96
00:11:52,040 --> 00:11:58,120
It's something that we look towards with a version.

97
00:11:58,120 --> 00:12:04,280
Because we have to give up, we have to let go, we cannot hold on, we cannot chase after,

98
00:12:04,280 --> 00:12:10,320
we can no longer have any of the wonderful things that we believe are making us happy.

99
00:12:10,320 --> 00:12:13,320
We can no longer chase after them.

100
00:12:13,320 --> 00:12:19,120
We have to put up with many unpleasant situations and pleasant circumstances.

101
00:12:19,120 --> 00:12:35,440
Pain and aching soreness, bad thoughts, boredom and restlessness, frustration.

102
00:12:35,440 --> 00:12:40,840
But as we start to either through the practice or by living our lives and meeting with

103
00:12:40,840 --> 00:12:49,080
such suffering, meeting with this realization that there is such suffering.

104
00:12:49,080 --> 00:12:55,240
So our current state of mind is only capable of a great amount of sorrow.

105
00:12:55,240 --> 00:12:58,280
It's not that we see the suffering outside of ourselves.

106
00:12:58,280 --> 00:13:02,200
We see that our minds are not as strong as they should be.

107
00:13:02,200 --> 00:13:04,880
So they suffer when things don't go our way.

108
00:13:04,880 --> 00:13:10,600
For most people it requires them to see very unpleasant situation before they realize

109
00:13:10,600 --> 00:13:18,000
how an equip their mind is to deal with the situation and then they come to practice meditation.

110
00:13:18,000 --> 00:13:25,000
Sometimes people come to meditate innocently without thinking that it's such a big deal.

111
00:13:25,000 --> 00:13:31,600
And only when they meditate do they realize how astray they've got, how weak their minds

112
00:13:31,600 --> 00:13:38,960
are, how unequipped their minds are for any difficulty, any suffering.

113
00:13:38,960 --> 00:13:42,920
And this is where Chanda rises.

114
00:13:42,920 --> 00:13:48,120
The ways we can give rise to Chanda are in this way that through seeing the suffering in

115
00:13:48,120 --> 00:13:54,960
the world around us or through seeing the suffering that comes from our mind through meditation.

116
00:13:54,960 --> 00:14:02,080
And it's something we should reflect upon often that really indeed we are weak in mind.

117
00:14:02,080 --> 00:14:08,680
And if we allow ourselves to continue like this without training ourselves, without training

118
00:14:08,680 --> 00:14:13,560
our minds, they will only meet with suffering in the future.

119
00:14:13,560 --> 00:14:15,640
Chanda is a very important thing.

120
00:14:15,640 --> 00:14:22,160
Whatever you have the intention or the desire to do, that is where we'll define who you

121
00:14:22,160 --> 00:14:23,160
are.

122
00:14:23,160 --> 00:14:36,080
But as Chanda is the mula, the base of all the basis of all states, all damas.

123
00:14:36,080 --> 00:14:40,240
Number two is media, which means effort.

124
00:14:40,240 --> 00:14:47,760
Effort sort of comes in line with desire.

125
00:14:47,760 --> 00:14:52,040
But on the other hand it's not enough just to want to do something or to know that meditation

126
00:14:52,040 --> 00:14:53,040
is enough.

127
00:14:53,040 --> 00:14:57,120
The Buddha said, few are the people in the world who know what needs to be done, who

128
00:14:57,120 --> 00:15:03,040
know that something needs to be done.

129
00:15:03,040 --> 00:15:09,320
More than just living our lives in the plain old ordinary way of getting a job and making

130
00:15:09,320 --> 00:15:17,680
money and settling down and trying to find stability for a short time and then not passing

131
00:15:17,680 --> 00:15:18,680
away.

132
00:15:18,680 --> 00:15:23,280
People who realize that there's more to life than that, it's hard to find.

133
00:15:23,280 --> 00:15:26,680
And he said, well what is even harder to find is people who actually then do something

134
00:15:26,680 --> 00:15:27,680
about it.

135
00:15:27,680 --> 00:15:30,080
And this is where effort comes into play.

136
00:15:30,080 --> 00:15:34,520
If you don't have the effort to actually get up in practice, to get up and do the walking,

137
00:15:34,520 --> 00:15:44,080
to get up and do the sitting, then your desire has very, it's an impotent desire.

138
00:15:44,080 --> 00:15:51,520
We require both of these that we have to put out effort and put out effort at every moment.

139
00:15:51,520 --> 00:15:56,160
So we have the desire to practice, but we also have this intention to practice.

140
00:15:56,160 --> 00:16:00,240
We also have this effort in our practice, putting our mind out at the object, not just

141
00:16:00,240 --> 00:16:05,240
sitting for long hours at a time, but also knowing what's going on and being mindful

142
00:16:05,240 --> 00:16:09,640
of it.

143
00:16:09,640 --> 00:16:15,600
The effort to get rid of unwholesome states, the effort to build up wholesome states, the

144
00:16:15,600 --> 00:16:22,760
effort to guard against the unoriginal unwholesome states, and the effort to give rise to wholesome

145
00:16:22,760 --> 00:16:29,880
states and to augment wholesome states instead of already arisen.

146
00:16:29,880 --> 00:16:35,040
This is another important, the Buddha said that it is through effort that we overcome

147
00:16:35,040 --> 00:16:36,840
suffering.

148
00:16:36,840 --> 00:16:44,240
When suffering comes to us, in whatever many forms that it comes, it is not simple that

149
00:16:44,240 --> 00:16:48,560
there is no simple way out that we're going to just take a pill and suddenly there's

150
00:16:48,560 --> 00:16:49,560
no more suffering.

151
00:16:49,560 --> 00:16:51,720
It's something that we have to work at.

152
00:16:51,720 --> 00:16:54,040
We have to realize this as well.

153
00:16:54,040 --> 00:16:57,360
If we have this effort, then we can truly become free from suffering.

154
00:16:57,360 --> 00:17:02,680
If we don't have the effort, it will be very difficult for us and be even more painful for

155
00:17:02,680 --> 00:17:08,400
us to practice, because we don't have the effort to really and truly sharpen our minds

156
00:17:08,400 --> 00:17:11,400
and it will be very slow going.

157
00:17:11,400 --> 00:17:15,320
So to me, with success, we have to have this effort.

158
00:17:15,320 --> 00:17:20,320
Number three, we need to tak, which means we need to keep, we need to think about practice.

159
00:17:20,320 --> 00:17:25,120
We need to keep it in our minds at all times.

160
00:17:25,120 --> 00:17:28,800
Even when we're not practicing, when we're not doing the walking or when we're not

161
00:17:28,800 --> 00:17:32,600
doing the sitting, we should always try to keep it in mind.

162
00:17:32,600 --> 00:17:40,800
Always try to think about, keep it at the top of our thoughts, at the top of our list

163
00:17:40,800 --> 00:17:43,720
of things to think about things to do.

164
00:17:43,720 --> 00:17:46,720
Everything else should be subservient to the meditation, we should always be thinking

165
00:17:46,720 --> 00:17:50,600
about coming back and being mindful.

166
00:17:50,600 --> 00:17:54,200
Even when we're doing other things during the day, we should try to be mindful doing

167
00:17:54,200 --> 00:17:59,760
them, standing when we're standing, no that we're standing when we're walking or walking,

168
00:17:59,760 --> 00:18:06,000
becoming aware of our feelings and our emotions, becoming aware of our thoughts, and keeping

169
00:18:06,000 --> 00:18:10,600
our mindfulness throughout the day.

170
00:18:10,600 --> 00:18:15,360
Especially in meditation practice, it's something that requires us to keep it in mind.

171
00:18:15,360 --> 00:18:20,120
Other other works that we do, we might be able to put aside for some time.

172
00:18:20,120 --> 00:18:23,760
We set things in motion and then leave them and wait to see what they bring, but with

173
00:18:23,760 --> 00:18:26,320
meditation, it's not the case.

174
00:18:26,320 --> 00:18:31,360
Meditation is like boiling water, if you turn on the hot water and then later you turn

175
00:18:31,360 --> 00:18:37,040
it off and then turn it on and off, the water will never boil, no matter how many times

176
00:18:37,040 --> 00:18:40,000
you come back to turn the water on.

177
00:18:40,000 --> 00:18:47,360
Even as you turn it off, it loses its momentum and something that we have to work on until

178
00:18:47,360 --> 00:18:52,080
we truly do become free from suffering because our state of mind changes, when we're

179
00:18:52,080 --> 00:18:58,120
not mindful, then we lose all of our strength of mind, of competence, effort, concentration,

180
00:18:58,120 --> 00:18:59,120
wisdom.

181
00:18:59,120 --> 00:19:08,120
We have to use the mindfulness at all times to gain these other faculties.

182
00:19:08,120 --> 00:19:11,640
And so it's a great importance that we keep the meditation in mind.

183
00:19:11,640 --> 00:19:16,800
In fact, the Buddha said that only when we sleep, then we should be allowed to take a

184
00:19:16,800 --> 00:19:17,800
break.

185
00:19:17,800 --> 00:19:23,360
But the way we do this, when we slide down to sleep, is even when we slide down to sleep,

186
00:19:23,360 --> 00:19:27,240
we think about the moment when we're going to wake up, when we make a determination in

187
00:19:27,240 --> 00:19:31,320
our mind as to when we're going to wake up, and then we're mindful until the moment

188
00:19:31,320 --> 00:19:38,000
when we fall asleep, when we wake up, we immediately get up and begin to meditate again.

189
00:19:38,000 --> 00:19:47,800
We should try to live in this way and try to be meditating for all of our waking hours.

190
00:19:47,800 --> 00:19:48,800
That's best we can.

191
00:19:48,800 --> 00:19:53,520
When we work in the world, when we have other things to do, obviously this will be less

192
00:19:53,520 --> 00:19:56,000
than this, it will be not the case.

193
00:19:56,000 --> 00:19:58,840
We'll have to interrupt our practice many times.

194
00:19:58,840 --> 00:20:02,600
But at any rate, we can always come back and remind ourselves.

195
00:20:02,600 --> 00:20:06,000
And the more we're able to keep it in mind, the better it will be for us.

196
00:20:06,000 --> 00:20:14,960
And the quicker we'll be able to gain real benefits from the practice.

197
00:20:14,960 --> 00:20:21,400
The fourth quality we monsa, in terms of meditation, this means figuring out the ways in

198
00:20:21,400 --> 00:20:27,000
which we're meditating incorrectly and being able to assess our own meditation practice.

199
00:20:27,000 --> 00:20:32,000
Being able to catch ourselves when we fall into wrong practice or bad habits.

200
00:20:32,000 --> 00:20:40,920
When being able to play with this imperfect mind that we have, when being able to work

201
00:20:40,920 --> 00:20:45,800
with this imperfect state of affairs, when being learning how to roll with the punches

202
00:20:45,800 --> 00:20:52,240
and how to get back up on our feet and how to work from one moment to one moment from

203
00:20:52,240 --> 00:20:57,320
one day to one day and training ourselves at every moment, trying to figure out what it

204
00:20:57,320 --> 00:21:03,600
is that we have to do next to augment our practice, to support our practice, to look

205
00:21:03,600 --> 00:21:08,560
and see as our confidence waning, then we have doubts, we have to look and see and say

206
00:21:08,560 --> 00:21:13,400
to ourselves doubting and doubting.

207
00:21:13,400 --> 00:21:21,920
Sometimes we have too much confidence and so we become greedy or we become lazy, sometimes

208
00:21:21,920 --> 00:21:24,360
we have too much effort and so we become distracted.

209
00:21:24,360 --> 00:21:31,880
Sometimes we have too much concentration and so we become drowsy or tired, means they're

210
00:21:31,880 --> 00:21:32,880
not balanced.

211
00:21:32,880 --> 00:21:36,560
When we have lots of effort that's good but we have to balance it with concentration

212
00:21:36,560 --> 00:21:41,080
and so we work on our faculties in this way.

213
00:21:41,080 --> 00:21:48,120
When we have lots of confidence that's good but without wisdom it's blind and so

214
00:21:48,120 --> 00:21:52,520
we need to balance our faculties.

215
00:21:52,520 --> 00:21:57,560
We need to, we need to catch ourselves in our practice, we need to see why we're failing,

216
00:21:57,560 --> 00:22:04,720
why we're slacking, why we're falling behind and we have to pick ourselves up and redouble

217
00:22:04,720 --> 00:22:10,960
our efforts and bring ourselves back in line and back on track and we have to work on

218
00:22:10,960 --> 00:22:14,360
this at all times.

219
00:22:14,360 --> 00:22:18,000
We monks up because we can't force our practice, we can't force the meditation to be

220
00:22:18,000 --> 00:22:25,320
perfect, we can't force ourselves to be good at meditating so we have to be clever in terms

221
00:22:25,320 --> 00:22:30,160
of how to train ourselves and how to work with what we have and slowly but surely lead

222
00:22:30,160 --> 00:22:38,840
ourselves, lead our minds to a state of clarity and the state of understanding.

223
00:22:38,840 --> 00:22:43,120
So these four are very important for meditators, they're things that we should constantly

224
00:22:43,120 --> 00:22:51,200
be thinking about and considering, we should at least use them as a guide for our practice

225
00:22:51,200 --> 00:22:56,800
in order to at least tell ourselves to make it clear to ourselves that if we don't have

226
00:22:56,800 --> 00:23:02,440
these we're not going to get anywhere and looking to see do we really have this contentment

227
00:23:02,440 --> 00:23:07,600
or this desire to practice and if we don't then we have to work on that and we have

228
00:23:07,600 --> 00:23:13,960
to see where our laziness lies or this evil in our hearts that's polluting our minds

229
00:23:13,960 --> 00:23:20,960
and making us think maybe that meditation is a bad thing or a useless thing or so on.

230
00:23:20,960 --> 00:23:26,640
That's giving us these bad vibes in regards to meditation and we have to work on this.

231
00:23:26,640 --> 00:23:28,840
Look directly at them and see them.

232
00:23:28,840 --> 00:23:32,760
If it's true that meditation is a bad thing then it should stand up to the test.

233
00:23:32,760 --> 00:23:37,320
You should be able to look at these thoughts and come to some rational conclusion that

234
00:23:37,320 --> 00:23:42,040
indeed meditation is a useless thing or a bad thing but what happens when you do is you'll

235
00:23:42,040 --> 00:23:51,520
see that there are negative emotions of anger, of laziness, of cowardlyness even that

236
00:23:51,520 --> 00:24:01,040
we that cause us to be afraid to or be lazy as well.

237
00:24:01,040 --> 00:24:07,280
So we need this kind of contentment and we need to put out effort to see what we want

238
00:24:07,280 --> 00:24:12,200
to practice but are we putting out the effort to actually practice or are we making excuses

239
00:24:12,200 --> 00:24:21,680
and doing other things as our effort being channeled into other pastimes or other activity.

240
00:24:21,680 --> 00:24:28,080
We need to see is are we keeping it in mind or are we really in some meditation in our minds

241
00:24:28,080 --> 00:24:35,280
at all times and we have to look and use this is using we among status is the fourth one

242
00:24:35,280 --> 00:24:40,640
to consider in this way in and of itself as we months and so we have to remind ourselves

243
00:24:40,640 --> 00:24:47,760
to constantly go over this and to look at our practice and not overly analyzing our practice

244
00:24:47,760 --> 00:24:52,400
in the end we do just have to meditate but as we meditate on we have to keep ourselves in

245
00:24:52,400 --> 00:24:58,560
check and see and look are we really practicing sometimes we have to ask our teacher questions

246
00:24:58,560 --> 00:25:03,040
when we can't figure things out for ourselves it won't do to just walk and sit walk

247
00:25:03,040 --> 00:25:10,160
and sit for years without really having some quality to our practice and so these factors

248
00:25:10,160 --> 00:25:14,960
really help to add that quality to them these are called the for iti pata and they are

249
00:25:14,960 --> 00:25:23,480
important for meditators as they are for success in any undertaking so this is the demo

250
00:25:23,480 --> 00:25:29,680
for today now we'll continue on with practice we're doing mindful frustration and walking

251
00:25:29,680 --> 00:25:36,680
and then sitting.

