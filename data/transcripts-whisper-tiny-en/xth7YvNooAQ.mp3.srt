1
00:00:00,000 --> 00:00:04,000
It could have come from me as well, but it didn't.

2
00:00:04,000 --> 00:00:05,000
Okay, go ahead.

3
00:00:05,000 --> 00:00:07,000
Dante, I have questions.

4
00:00:07,000 --> 00:00:13,000
What is the recommended method of writing oneself of an annoying song playing in one's head during meditation?

5
00:00:13,000 --> 00:00:16,000
I suffered from an old Bowie song for almost two weeks.

6
00:00:16,000 --> 00:00:21,000
Should I refrain from music and film in the future?

7
00:00:21,000 --> 00:00:25,000
It is actually two different questions.

8
00:00:25,000 --> 00:00:29,000
How to get rid of the song and whether you should refrain.

9
00:00:29,000 --> 00:00:32,000
Because there's actually nothing wrong with the song.

10
00:00:32,000 --> 00:00:35,000
You're not plagued by music playing.

11
00:00:35,000 --> 00:00:36,000
That's suffering.

12
00:00:36,000 --> 00:00:38,000
You're not suffering from the song.

13
00:00:38,000 --> 00:00:40,000
Not any more than you suffer.

14
00:00:40,000 --> 00:00:42,000
Not having the song in your head.

15
00:00:42,000 --> 00:00:47,000
The song itself is no more suffering than any other experience.

16
00:00:47,000 --> 00:00:52,000
The suffering comes from your desire to be free from it.

17
00:00:52,000 --> 00:00:57,000
Your desire to not have the song playing in your head constantly, repeatedly.

18
00:00:57,000 --> 00:00:58,000
And so on.

19
00:00:58,000 --> 00:01:03,000
And that comes from a misunderstanding or a delusion of self.

20
00:01:03,000 --> 00:01:06,000
The idea that you can somehow control that.

21
00:01:06,000 --> 00:01:13,000
The idea that somehow it is you that is experiencing that song.

22
00:01:13,000 --> 00:01:18,000
It comes from the frustration of thinking you can stop it and so on.

23
00:01:18,000 --> 00:01:26,000
Once you see impermanence suffering in non-self, it won't bother you that there are songs playing in your head if there are songs playing in your head.

24
00:01:26,000 --> 00:01:38,000
No, the reason why you should probably refrain from music and film is because of the dulling quality it has to cultivate delusion.

25
00:01:38,000 --> 00:01:42,000
The addictive quality that it has to create greed.

26
00:01:42,000 --> 00:01:47,000
And therefore to create anger when you don't get what you want.

27
00:01:47,000 --> 00:01:52,000
Or in this case when it arises in your head.

28
00:01:52,000 --> 00:02:01,000
So it's not the fact that it's arising in your head that is a sign that you should stop watching movies or listening to music.

29
00:02:01,000 --> 00:02:07,000
It's the fact that you become irritable when it isn't according to your desire.

30
00:02:07,000 --> 00:02:18,000
And that irritation and that delusion as well thinking that you can somehow control it is related to your addiction to film and music and entertainment.

31
00:02:18,000 --> 00:02:22,000
And anything that's addictive in general.

