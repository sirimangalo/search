1
00:00:00,000 --> 00:00:05,640
So, some teachers say to note thinking twice and then go back to the rising and falling

2
00:00:05,640 --> 00:00:10,520
when our minds wander in meditation, other say to note thinking until the thought or wandering

3
00:00:10,520 --> 00:00:15,800
falls away, and then go back to the abdomen, which is more precise.

4
00:00:15,800 --> 00:00:21,960
Also some teachers teach to only note thoughts that take you completely away from the rising

5
00:00:21,960 --> 00:00:26,320
and falling and not the background thoughts that one has.

6
00:00:26,320 --> 00:00:36,080
Okay, well, first of all, as I said that thinking is not something that generally lasts

7
00:00:36,080 --> 00:00:38,200
more than a couple of noteings.

8
00:00:38,200 --> 00:00:43,720
What lasts is the attachment to the noting, whether you like it or dislike it or it's

9
00:00:43,720 --> 00:00:45,720
interesting for you.

10
00:00:45,720 --> 00:00:49,400
That's what's going to keep it coming back and make it seem like the thought is persisting,

11
00:00:49,400 --> 00:00:52,160
but thoughts are actually quite fleeting.

12
00:00:52,160 --> 00:00:56,680
So if you're just focusing on the thought twice is really enough, whatever three times

13
00:00:56,680 --> 00:01:07,920
as you like, sometimes even once.

14
00:01:07,920 --> 00:01:17,280
And then go back to the rising and falling, but if it is persistent, then you should

15
00:01:17,280 --> 00:01:21,280
note what is persistent, which is generally the feeling or the attachment, so then you

16
00:01:21,280 --> 00:01:29,760
say feeling, feeling happy, happy or pain, disliking or so on, whatever the feeling or emotion

17
00:01:29,760 --> 00:01:30,760
is.

18
00:01:30,760 --> 00:01:36,440
If it gives you a headache, guess it would be pain.

19
00:01:36,440 --> 00:01:40,200
And then as far as only noting thoughts that take you completely away from the rising and

20
00:01:40,200 --> 00:01:42,240
falling, well, it is debatable.

21
00:01:42,240 --> 00:01:49,280
You could do it either way, but when you're walking, we tend to have people give people

22
00:01:49,280 --> 00:01:50,280
some leeway.

23
00:01:50,280 --> 00:01:53,480
My teacher would always say, you know, there's different ways to do it.

24
00:01:53,480 --> 00:01:59,520
He said, you can stop at every thought or you can just bring your mind back to the foot

25
00:01:59,520 --> 00:02:00,520
and continue going.

26
00:02:00,520 --> 00:02:05,200
And he said, when you stop at the end of the walking, then you can focus on the thoughts

27
00:02:05,200 --> 00:02:06,200
or so.

28
00:02:06,200 --> 00:02:08,440
And there's different ways.

29
00:02:08,440 --> 00:02:12,360
The point is to be mindful, no, the point is to be here and now I would say that when

30
00:02:12,360 --> 00:02:17,520
you're sitting, there's more of a case for going out to all the thoughts.

31
00:02:17,520 --> 00:02:18,520
Why?

32
00:02:18,520 --> 00:02:25,120
Because thoughts that are very important, thinking is a very important part of who we are.

33
00:02:25,120 --> 00:02:34,120
This is going to be a very beneficial investigation.

34
00:02:34,120 --> 00:02:38,640
Thought sometimes you catch the thoughts when they're already finished, when you've been

35
00:02:38,640 --> 00:02:42,960
thinking for a while and at the very end, you realize, oh, I was thinking for a long time

36
00:02:42,960 --> 00:02:44,200
there.

37
00:02:44,200 --> 00:02:47,440
And then you say thinking, thinking, sometimes you catch it in the middle of the

38
00:02:47,440 --> 00:02:48,440
thought.

39
00:02:48,440 --> 00:02:51,480
So in the middle of the thought, you say thinking, thinking, sometimes you catch it at the

40
00:02:51,480 --> 00:02:54,280
very beginning when you're first starting to think.

41
00:02:54,280 --> 00:03:01,080
And this experimentation or this practice, this training to catch the thoughts, actually

42
00:03:01,080 --> 00:03:05,600
to catch them sooner and sooner and sooner, is really an important part of the training,

43
00:03:05,600 --> 00:03:11,560
the ability to catch a thought when it first arises, is really important in sharpening

44
00:03:11,560 --> 00:03:20,080
the mind and as your practice progresses, as you improve, it's an important ability.

45
00:03:20,080 --> 00:03:26,280
And I suppose another thing is in daily life, if you're practicing like once a day or something,

46
00:03:26,280 --> 00:03:31,360
then you might want to limit it because you're thinking so much.

47
00:03:31,360 --> 00:03:37,200
A good way to do it is to just stay distracted and distracted and then come back and focus

48
00:03:37,200 --> 00:03:40,760
on the rising and falling and not worry about the individual thought so much.

49
00:03:40,760 --> 00:03:49,880
Because in an ordinary life, where your practice is not intensive, it really is much more

50
00:03:49,880 --> 00:03:52,920
on the level of distraction than it is on thoughts.

51
00:03:52,920 --> 00:03:57,120
So if you're thinking a lot, you just say to yourself, when it comes up, say distracted,

52
00:03:57,120 --> 00:04:01,960
distracted and forget about it, go back to the rising and falling.

53
00:04:01,960 --> 00:04:07,240
But again, important is to note the attachment to the thoughts, as with everything, often

54
00:04:07,240 --> 00:04:12,320
the problem that we have noting and being mindful is not so much with the object itself

55
00:04:12,320 --> 00:04:16,720
as with our reactions to it, our likes and dislikes.

56
00:04:16,720 --> 00:04:18,680
That's what they're called hindrances.

57
00:04:18,680 --> 00:04:23,400
The five hindrances are named that for a reason and it's worth learning more about the

58
00:04:23,400 --> 00:04:24,400
five hindrances.

59
00:04:24,400 --> 00:04:29,680
It's liking, disliking, drowsiness, distraction and doubt because they really do get in

60
00:04:29,680 --> 00:04:34,320
the way of our practice.

61
00:04:34,320 --> 00:04:37,680
So thanks for the question.

