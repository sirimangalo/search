1
00:00:00,000 --> 00:00:05,000
Welcome back to our study of the Dhamupanda.

2
00:00:05,000 --> 00:00:11,000
Today we continue on with verse number 65, which reads as follows.

3
00:00:11,000 --> 00:00:17,000
Muhu Tampi J. Winu Panditang by Yuru Bhazati.

4
00:00:17,000 --> 00:00:27,000
Kippang dhammang wijana di jyuha su parasang dha pya pya pya pya pya.

5
00:00:27,000 --> 00:00:34,000
That means very similar to the last one, but here we're talking about the opposite.

6
00:00:34,000 --> 00:00:39,000
Winu, a wise person, Muhu dhampiti.

7
00:00:39,000 --> 00:00:44,000
If a wise person, even for a moment Panditang by Yuru Bhazati,

8
00:00:44,000 --> 00:00:51,000
should attend upon or hang out with a wise person,

9
00:00:51,000 --> 00:00:59,000
Kippang dhammang wijana di jyya quick to know the, to understand the dhammah.

10
00:00:59,000 --> 00:01:06,000
Jyuha su parasang dha jyuha, just as the tongue is quick to,

11
00:01:06,000 --> 00:01:11,000
you know the taste of the soup.

12
00:01:11,000 --> 00:01:16,000
Just as the tongue to the soup of a wise person,

13
00:01:16,000 --> 00:01:20,000
is able to quickly understand the dhammah of a wise person.

14
00:01:20,000 --> 00:01:26,000
By hanging out with a wise person, even for a short time.

15
00:01:26,000 --> 00:01:29,000
So this, again, a rather short story,

16
00:01:29,000 --> 00:01:31,000
but we can provide a little bit of background.

17
00:01:31,000 --> 00:01:38,000
This is in relation to these, the group of 30 friends,

18
00:01:38,000 --> 00:01:41,000
who the story goes were out,

19
00:01:41,000 --> 00:01:46,000
cavorting in the forest with their wives.

20
00:01:46,000 --> 00:01:51,000
And one of them didn't have a wife, so they hired a courtesan for him,

21
00:01:51,000 --> 00:01:57,000
an escort, I guess.

22
00:01:57,000 --> 00:02:04,000
Contact and looked in the yellow pages and found an escort service.

23
00:02:04,000 --> 00:02:07,000
Something like the equivalent of the yellow pages,

24
00:02:07,000 --> 00:02:10,000
where you find an escort in those times, a courtesan,

25
00:02:10,000 --> 00:02:16,000
invited her along to be his wife for the day.

26
00:02:16,000 --> 00:02:20,000
So they went into the forest and they're playing in the water,

27
00:02:20,000 --> 00:02:24,000
and they took off all their clothes,

28
00:02:24,000 --> 00:02:28,000
jumped in the water and they're sporting around.

29
00:02:28,000 --> 00:02:31,000
And whether in the water,

30
00:02:31,000 --> 00:02:33,000
I'm not making this up, whether they were in the water,

31
00:02:33,000 --> 00:02:35,000
or they were asleep, I can't remember what she was.

32
00:02:35,000 --> 00:02:37,000
Okay, but while they were negligent,

33
00:02:37,000 --> 00:02:40,000
this courtesan runs off with all their jewels,

34
00:02:40,000 --> 00:02:44,000
goes picking through their clothes and gathers up all their valuables

35
00:02:44,000 --> 00:02:46,000
and runs off into the forest.

36
00:02:46,000 --> 00:02:49,000
When they get back and find their clothes,

37
00:02:49,000 --> 00:02:53,000
missing all the jewels, they realize right away what happened

38
00:02:53,000 --> 00:02:56,000
and they start.

39
00:02:56,000 --> 00:02:59,000
They send their wives home, I guess it doesn't say about that,

40
00:02:59,000 --> 00:03:01,000
but they go off into the forest,

41
00:03:01,000 --> 00:03:05,000
these 30 men trying to find this courtesan,

42
00:03:05,000 --> 00:03:08,000
and get back their jewels.

43
00:03:08,000 --> 00:03:14,000
Just so it just so happens that the Buddha having taught the

44
00:03:14,000 --> 00:03:18,000
five, the group of five ascetics in Isipatana,

45
00:03:18,000 --> 00:03:21,000
having turned the wheel in Dhamma,

46
00:03:21,000 --> 00:03:28,000
and then having enlightened Yasa and all of his 54 friends,

47
00:03:28,000 --> 00:03:31,000
and having sent the total of 60,

48
00:03:31,000 --> 00:03:34,000
Arahan's off into the world to spread the teaching,

49
00:03:34,000 --> 00:03:36,000
no two of them going in one direction.

50
00:03:36,000 --> 00:03:38,000
So he sent them all through India.

51
00:03:38,000 --> 00:03:44,000
He went himself off to Rajika to spread the Dhamma there.

52
00:03:44,000 --> 00:03:47,000
Uruayla, there he would find Isipat.

53
00:03:47,000 --> 00:03:50,000
Uruayla Kasaband, the other two Kasipa brothers

54
00:03:50,000 --> 00:03:53,000
and eventually go off and teach Pimbe Saran,

55
00:03:53,000 --> 00:03:54,000
spread his teaching in Rajika.

56
00:03:54,000 --> 00:04:02,000
But first, he did something that appears in the texts.

57
00:04:02,000 --> 00:04:06,000
He stopped off where these 30 men were running around the forest,

58
00:04:06,000 --> 00:04:10,000
trying to find this courtesan.

59
00:04:10,000 --> 00:04:12,000
And he stopped under a tree,

60
00:04:12,000 --> 00:04:14,000
I'm just sat there,

61
00:04:14,000 --> 00:04:18,000
and meditation, and these 30 men

62
00:04:18,000 --> 00:04:20,000
wandered upon him,

63
00:04:20,000 --> 00:04:22,000
and they looked and they said,

64
00:04:22,000 --> 00:04:23,000
oh, he looks like a wise person,

65
00:04:23,000 --> 00:04:26,000
and that he can give us some good advice.

66
00:04:26,000 --> 00:04:28,000
And so they wandered up to him,

67
00:04:28,000 --> 00:04:31,000
and paid respect, gave him respect,

68
00:04:31,000 --> 00:04:33,000
and then asked him,

69
00:04:33,000 --> 00:04:35,000
a vendor will say,

70
00:04:35,000 --> 00:04:38,000
you see he looked like he'd been sitting here in the forest for a while,

71
00:04:38,000 --> 00:04:42,000
and we wonder if you happen to have seen this woman?

72
00:04:42,000 --> 00:04:45,000
We're looking for a woman who stole our jewels.

73
00:04:49,000 --> 00:04:54,000
Not exactly the sort of advice that most people would ask the Buddha for.

74
00:04:54,000 --> 00:04:57,000
But the Buddha gives him something much more valuable.

75
00:04:57,000 --> 00:05:00,000
He looks at them.

76
00:05:00,000 --> 00:05:05,000
And he knows, the reason he's there is because he knows the

77
00:05:05,000 --> 00:05:10,000
ability or the qualities of these 30 men

78
00:05:10,000 --> 00:05:14,000
that they're highly cultivated individuals,

79
00:05:14,000 --> 00:05:17,000
even though they don't realize it.

80
00:05:17,000 --> 00:05:21,000
And so he asks them,

81
00:05:21,000 --> 00:05:24,000
he says, what do you think is better

82
00:05:24,000 --> 00:05:27,000
that you should search after a woman?

83
00:05:27,000 --> 00:05:31,000
You know, think you should search after yourself.

84
00:05:31,000 --> 00:05:33,000
Search after yourselves.

85
00:05:39,000 --> 00:05:42,000
Not exactly the advice they were looking for.

86
00:05:42,000 --> 00:05:44,000
And of course, this is the Buddha,

87
00:05:44,000 --> 00:05:48,000
and so they take in what he's saying,

88
00:05:48,000 --> 00:05:50,000
and they say, of course,

89
00:05:50,000 --> 00:05:53,000
it's much more important,

90
00:05:53,000 --> 00:05:56,000
even all the jewels that we lost are not really all that valuable

91
00:05:56,000 --> 00:06:00,000
in comparison to finding oneself.

92
00:06:00,000 --> 00:06:03,000
Which actually sounds kind of non-Buddhist if you think about it,

93
00:06:03,000 --> 00:06:07,000
but it's all about giving up yourself, right?

94
00:06:07,000 --> 00:06:09,000
Giving up the idea of self.

95
00:06:09,000 --> 00:06:12,000
But it's a lot of this passage,

96
00:06:12,000 --> 00:06:15,000
actually, those people who think that the Buddha actually taught

97
00:06:15,000 --> 00:06:19,000
that there is a self or had some idea of there being self.

98
00:06:19,000 --> 00:06:23,000
But it's quite clear from the Buddha's teaching that's not the case.

99
00:06:23,000 --> 00:06:28,000
And this is simply a teaching instrument.

100
00:06:28,000 --> 00:06:29,000
How to get the people,

101
00:06:29,000 --> 00:06:32,000
how to get them to look for something that's truly valuable.

102
00:06:32,000 --> 00:06:34,000
And they're probably working in dues,

103
00:06:34,000 --> 00:06:36,000
or whatever would have been,

104
00:06:36,000 --> 00:06:39,000
and they were Brahmin religion.

105
00:06:39,000 --> 00:06:41,000
So they would have believed in a self.

106
00:06:41,000 --> 00:06:43,000
And so it's a good way to get people's attention.

107
00:06:43,000 --> 00:06:47,000
They say, well, why don't you seek out what is truly important?

108
00:06:47,000 --> 00:06:49,000
Seek out yourselves.

109
00:06:49,000 --> 00:06:52,000
Come to know yourselves and learn about yourselves.

110
00:06:52,000 --> 00:06:59,000
And they say, well, they'll sit down and I'll help you find yourself.

111
00:06:59,000 --> 00:07:01,000
And they'll have them sit down,

112
00:07:01,000 --> 00:07:02,000
and he teaches them a dhamma,

113
00:07:02,000 --> 00:07:05,000
and they all became enlightened right there.

114
00:07:05,000 --> 00:07:08,000
I don't know how many people have ever become enlightened.

115
00:07:08,000 --> 00:07:12,000
They might not have been the same categories,

116
00:07:12,000 --> 00:07:15,000
but it can't happen.

117
00:07:15,000 --> 00:07:17,000
Sometimes listening to a talk,

118
00:07:17,000 --> 00:07:23,000
if you're clearly aware of the experience,

119
00:07:23,000 --> 00:07:25,000
during the time that you're listening to the talk,

120
00:07:25,000 --> 00:07:30,000
it's useful because one, you can't go anywhere.

121
00:07:30,000 --> 00:07:34,000
And two, the teaching constantly reminds you of good things.

122
00:07:34,000 --> 00:07:41,000
Three, the company of the fellow audience members are all

123
00:07:41,000 --> 00:07:47,000
ideally on the same path.

124
00:07:47,000 --> 00:07:55,000
And so there are good qualities to the experience of listening to the dhamma

125
00:07:55,000 --> 00:07:57,000
that you don't get elsewhere.

126
00:07:57,000 --> 00:08:04,000
I mean, that timing is actually quite easy to realize the teachings,

127
00:08:04,000 --> 00:08:13,000
realize the truth, and to find the truth, to understand reality.

128
00:08:13,000 --> 00:08:19,000
And so they did these 30 young men,

129
00:08:19,000 --> 00:08:22,000
became our hunts, and decided to become,

130
00:08:22,000 --> 00:08:24,000
and obviously decided to become monks,

131
00:08:24,000 --> 00:08:28,000
and proceeded along with the Buddha.

132
00:08:28,000 --> 00:08:29,000
I'm not sure where they went.

133
00:08:29,000 --> 00:08:33,000
Actually, they maybe went off on their own somewhere.

134
00:08:33,000 --> 00:08:37,000
But the other monks were talking about this later.

135
00:08:37,000 --> 00:08:39,000
And this is how this verse came up.

136
00:08:39,000 --> 00:08:41,000
They said, well, this is kind of crazy.

137
00:08:41,000 --> 00:08:43,000
All of us would take so much work,

138
00:08:43,000 --> 00:08:45,000
and maybe these monks weren't yet our hunts,

139
00:08:45,000 --> 00:08:47,000
but maybe they were.

140
00:08:47,000 --> 00:08:51,000
But how long it took them to practice over the days and weeks and months

141
00:08:51,000 --> 00:08:54,000
and even years to become our hunts.

142
00:08:54,000 --> 00:09:00,000
But these 30 young men who were earlier in the same day

143
00:09:00,000 --> 00:09:07,000
are frolicking in negligence, in indolence.

144
00:09:11,000 --> 00:09:14,000
And then suddenly they can become our hunts.

145
00:09:14,000 --> 00:09:16,000
How is that?

146
00:09:16,000 --> 00:09:18,000
And the Buddha came in and asked them,

147
00:09:18,000 --> 00:09:19,000
what are you talking about?

148
00:09:19,000 --> 00:09:21,000
And they said, I'm talking about this,

149
00:09:21,000 --> 00:09:23,000
and then the Buddha said, well,

150
00:09:23,000 --> 00:09:26,000
and then he actually told one of the Jatakas based on this.

151
00:09:26,000 --> 00:09:33,000
And he said that he reminded them of a Jatakas of once

152
00:09:33,000 --> 00:09:34,000
in a past life.

153
00:09:34,000 --> 00:09:37,000
He had taught these 30 men were coming to kill him,

154
00:09:37,000 --> 00:09:44,000
and he was able to help them realize the ways

155
00:09:44,000 --> 00:09:50,000
and as a result they gave up their evil

156
00:09:50,000 --> 00:09:53,000
and became disciples of it.

157
00:09:53,000 --> 00:10:01,000
But the meaning behind it is that they had a lot coming into this.

158
00:10:01,000 --> 00:10:02,000
They were called Upanishaya.

159
00:10:02,000 --> 00:10:04,000
They brought from their past lives.

160
00:10:04,000 --> 00:10:05,000
And they all bring this from our past lives.

161
00:10:05,000 --> 00:10:07,000
We come into this life with qualities,

162
00:10:07,000 --> 00:10:09,000
both positive and negative.

163
00:10:09,000 --> 00:10:15,000
The fact that we're born human means we definitely still are born

164
00:10:15,000 --> 00:10:17,000
with unwholesome qualities.

165
00:10:17,000 --> 00:10:20,000
Everyone, even the Bodhisattva was born as a human being.

166
00:10:20,000 --> 00:10:24,000
I still have greed, still have anger, still have delusion.

167
00:10:24,000 --> 00:10:28,000
Without those we wouldn't be born again as a human.

168
00:10:28,000 --> 00:10:32,000
We have negative qualities, but we also carry all the positive qualities,

169
00:10:32,000 --> 00:10:34,000
all that we've cultivated in our past lives,

170
00:10:34,000 --> 00:10:40,000
all the good work that we've done in Buddhism and outside of Buddhism,

171
00:10:40,000 --> 00:10:46,000
helping other people, being moral and ethical,

172
00:10:46,000 --> 00:10:50,000
and a high-minded and spiritually minded,

173
00:10:50,000 --> 00:10:54,000
and being inclined toward the spiritual life inside Buddhism,

174
00:10:54,000 --> 00:10:57,000
within Buddhism and outside of it as well.

175
00:10:57,000 --> 00:10:59,000
All the wisdom that we've gained,

176
00:10:59,000 --> 00:11:01,000
all the learning that we've gained from lifetime to lifetime,

177
00:11:01,000 --> 00:11:05,000
it doesn't leave us except insofar as our memories go.

178
00:11:05,000 --> 00:11:09,000
Memories, of course, fade away as they always do over time.

179
00:11:09,000 --> 00:11:16,000
But the quality of the mind changes.

180
00:11:16,000 --> 00:11:20,000
Yes, we're all aware of it, but it's.

181
00:11:20,000 --> 00:11:22,000
And so the Buddha said, it's because of we knew,

182
00:11:22,000 --> 00:11:23,000
because these men are we knew,

183
00:11:23,000 --> 00:11:25,000
they're people who understand,

184
00:11:25,000 --> 00:11:28,000
people who have this resonance in them.

185
00:11:28,000 --> 00:11:32,000
They're the potential to resonate with the dhamma.

186
00:11:32,000 --> 00:11:34,000
Just as the tongue has the potential,

187
00:11:34,000 --> 00:11:37,000
a person without their tongue damaged,

188
00:11:37,000 --> 00:11:39,000
their sense of taste damaged,

189
00:11:39,000 --> 00:11:42,000
unless their tense sense of taste is damaged,

190
00:11:42,000 --> 00:11:49,000
they have the potential to receive whatever the taste of the soup is.

191
00:11:49,000 --> 00:11:52,000
But quickly, without any effort,

192
00:11:52,000 --> 00:11:55,000
unlike the spoon which we talked about last week,

193
00:11:55,000 --> 00:11:57,000
the spoon no matter how long it touches the soup,

194
00:11:57,000 --> 00:11:58,000
it never tastes anything.

195
00:11:58,000 --> 00:12:00,000
It's all about one's potential.

196
00:12:00,000 --> 00:12:06,000
So the point is that the dhamma is good in and of itself.

197
00:12:06,000 --> 00:12:12,000
There's nothing wrong with the dhamma just because we can't become enlightened off of it.

198
00:12:12,000 --> 00:12:16,000
It's up to our qualities as we talked about last week.

199
00:12:16,000 --> 00:12:18,000
If we don't have those qualities,

200
00:12:18,000 --> 00:12:22,000
we spend all the time we want with enlightened beings.

201
00:12:22,000 --> 00:12:29,000
And if our needs and our speech and our thoughts are unwholesome

202
00:12:29,000 --> 00:12:32,000
and are not inclined towards enlightenment,

203
00:12:32,000 --> 00:12:38,000
then we'll never get there. It's useless.

204
00:12:38,000 --> 00:12:40,000
It's like being the spoon with the soup,

205
00:12:40,000 --> 00:12:48,000
but the quality of a wise person is

206
00:12:48,000 --> 00:12:51,000
that they quickly understand the truth.

207
00:12:51,000 --> 00:12:55,000
Even when they just taught the Buddhist teachings from the books,

208
00:12:55,000 --> 00:12:58,000
they immediately get the meaning of it.

209
00:12:58,000 --> 00:12:59,000
They very quickly get the meaning of it.

210
00:12:59,000 --> 00:13:03,000
Some people when they hear the Buddhist teaching are confused

211
00:13:03,000 --> 00:13:07,000
and disinterested and turned off even repelled by the Buddhist teaching

212
00:13:07,000 --> 00:13:11,000
when they hear about it being for the giving up,

213
00:13:11,000 --> 00:13:14,000
the discarding of central desire

214
00:13:14,000 --> 00:13:17,000
and for the giving up of the whole world.

215
00:13:17,000 --> 00:13:21,000
I think of this as a ridiculous or horrible thing, a bad teaching.

216
00:13:21,000 --> 00:13:26,000
Teaching that is built on repression and so on and so on.

217
00:13:26,000 --> 00:13:34,000
The misunderstandings that arise come from a different sort of person.

218
00:13:34,000 --> 00:13:36,000
Divas are a personality.

219
00:13:36,000 --> 00:13:39,000
Those people who are inclined towards the world

220
00:13:39,000 --> 00:13:42,000
and inclined towards craving and attachment

221
00:13:42,000 --> 00:13:48,000
they aren't able to understand the teaching.

222
00:13:48,000 --> 00:13:51,000
And so that's a common question.

223
00:13:51,000 --> 00:13:55,000
How do these monks in the time of the morning hear the Buddha giving a talk?

224
00:13:55,000 --> 00:13:57,000
People just immediately became around.

225
00:13:57,000 --> 00:13:59,000
Very quickly, like, sorry, put that.

226
00:13:59,000 --> 00:14:01,000
So he put that didn't even have to hear from the Buddha.

227
00:14:01,000 --> 00:14:06,000
He heard from Asaji this one of the first five months

228
00:14:06,000 --> 00:14:09,000
within the five ascetics.

229
00:14:09,000 --> 00:14:14,000
And he just heard half of a stanza and he became a sotapana

230
00:14:14,000 --> 00:14:17,000
just from hearing half of the stanza.

231
00:14:17,000 --> 00:14:26,000
He was ready for it.

232
00:14:26,000 --> 00:14:29,000
He was ready for it.

233
00:14:29,000 --> 00:14:33,000
It was very sensitive tongue.

234
00:14:33,000 --> 00:14:49,000
He was able to realize the teaching.

235
00:14:49,000 --> 00:14:54,000
So what does this mean for all of us?

236
00:14:54,000 --> 00:14:56,000
It means that realistically,

237
00:14:56,000 --> 00:14:59,000
first of all, we should never fault the teaching

238
00:14:59,000 --> 00:15:02,000
just because it doesn't bring us anything.

239
00:15:02,000 --> 00:15:07,000
We find fault with the teaching in an intellectual sense.

240
00:15:07,000 --> 00:15:09,000
This is what we should give up the teaching.

241
00:15:09,000 --> 00:15:11,000
But when we find the teaching doesn't work,

242
00:15:11,000 --> 00:15:13,000
we have to ask ourselves one of two things.

243
00:15:13,000 --> 00:15:15,000
Is the teaching ineffective?

244
00:15:15,000 --> 00:15:18,000
Or are we unable to understand the teaching?

245
00:15:18,000 --> 00:15:20,000
Are we unable to understand the practice?

246
00:15:20,000 --> 00:15:22,000
There's these two ways.

247
00:15:22,000 --> 00:15:25,000
When you see enlightened beings,

248
00:15:25,000 --> 00:15:30,000
and this is the key, if your teacher is full of greed

249
00:15:30,000 --> 00:15:32,000
and full of anger and full of delusion,

250
00:15:32,000 --> 00:15:36,000
maybe it's my teacher's fault or the teaching is impure.

251
00:15:36,000 --> 00:15:38,000
But when you know that your teacher,

252
00:15:38,000 --> 00:15:41,000
like the Buddha, we know that we have the Buddha

253
00:15:41,000 --> 00:15:45,000
and when we see that people who practice meditation

254
00:15:45,000 --> 00:15:50,000
are able to free themselves from these things,

255
00:15:50,000 --> 00:15:54,000
then we have to understand that it's most likely our own fault.

256
00:15:54,000 --> 00:15:57,000
We ourselves are just lacking in the requisites

257
00:15:57,000 --> 00:15:59,000
to quickly understand the teaching.

258
00:15:59,000 --> 00:16:02,000
The second part is not to be discouraged,

259
00:16:02,000 --> 00:16:03,000
not to be quickly discouraged

260
00:16:03,000 --> 00:16:05,000
because after a week of practicing,

261
00:16:05,000 --> 00:16:06,000
I'm still not enlightened.

262
00:16:06,000 --> 00:16:08,000
Here I have been sitting for ten minutes

263
00:16:08,000 --> 00:16:10,000
and I don't feel anything yet.

264
00:16:10,000 --> 00:16:14,000
I don't understand that part of this is the cultivation.

265
00:16:14,000 --> 00:16:17,000
Tolkien, Tolkien, the Buddha said,

266
00:16:17,000 --> 00:16:20,000
dropped by drop or little by little.

267
00:16:20,000 --> 00:16:22,000
The goodness doesn't come to you all at once.

268
00:16:22,000 --> 00:16:25,000
It's something that you cultivate little by little every moment.

269
00:16:25,000 --> 00:16:27,000
Every moment that we're mindful,

270
00:16:27,000 --> 00:16:29,000
that we're aware of something,

271
00:16:29,000 --> 00:16:31,000
that we're clearly aware of experience,

272
00:16:31,000 --> 00:16:34,000
just as it is without liking and disliking or judgment

273
00:16:34,000 --> 00:16:37,000
or attachment or conceit or views.

274
00:16:37,000 --> 00:16:40,000
That moment is blessed, that moment is great.

275
00:16:40,000 --> 00:16:43,000
If we can cultivate those moments again and again,

276
00:16:43,000 --> 00:16:46,000
continuously, eventually becomes habitual

277
00:16:46,000 --> 00:16:49,000
and that's when we're able to understand the number.

278
00:16:49,000 --> 00:16:51,000
If you're example,

279
00:16:51,000 --> 00:16:53,000
when you learn about Buddhism,

280
00:16:53,000 --> 00:16:55,000
before you practice meditation,

281
00:16:55,000 --> 00:16:58,000
a lot less clear than after you actually sit down and practice

282
00:16:58,000 --> 00:17:00,000
the teaching and then approach the same text.

283
00:17:00,000 --> 00:17:03,000
You find that you understand them so much better and so much clearer

284
00:17:03,000 --> 00:17:06,000
and so much more fruitfully.

285
00:17:06,000 --> 00:17:13,000
Your studies are far greater fruit because of your practice.

286
00:17:13,000 --> 00:17:15,000
So it's not to be discouraged.

287
00:17:15,000 --> 00:17:21,000
We shouldn't think that we're useless.

288
00:17:21,000 --> 00:17:24,000
We shouldn't think we're the spoon.

289
00:17:24,000 --> 00:17:28,000
We should always look at our own attitude as the point

290
00:17:28,000 --> 00:17:32,000
that we shouldn't be like Udayi, this guy who

291
00:17:32,000 --> 00:17:35,000
just hung out with the Buddha and pretended to be wise,

292
00:17:35,000 --> 00:17:37,000
but never really listened to what the Buddha had to say

293
00:17:37,000 --> 00:17:41,000
or put it into practice or tried to understand it even.

294
00:17:41,000 --> 00:17:43,000
We should always try to put the teachings into practice,

295
00:17:43,000 --> 00:17:46,000
try to understand the more and try to apply them to our own life,

296
00:17:46,000 --> 00:17:48,000
not to just keep them up in our head

297
00:17:48,000 --> 00:17:51,000
or chant them or teach them to others

298
00:17:51,000 --> 00:17:55,000
or think about them, but we should actually try every day

299
00:17:55,000 --> 00:17:58,000
and in every aspect of our lives to incorporate them into our lives.

300
00:17:58,000 --> 00:18:01,000
How can we be more generous? How can we be more moral?

301
00:18:01,000 --> 00:18:05,000
How can we be more focused and concentrated and clear in our minds?

302
00:18:05,000 --> 00:18:09,000
We're cultivating goodness, the three types of goodness

303
00:18:09,000 --> 00:18:13,000
and cultivating morality and concentration in wisdom.

304
00:18:13,000 --> 00:18:16,000
Most importantly, using mindfulness to cultivate it all

305
00:18:16,000 --> 00:18:18,000
because mindfulness is what clears the mind

306
00:18:18,000 --> 00:18:20,000
and allows you to see the difference between wholesome

307
00:18:20,000 --> 00:18:25,000
and unwholesome and allows you to keep the mind

308
00:18:25,000 --> 00:18:29,000
from wandering in unwholesome ways, focus the mind

309
00:18:29,000 --> 00:18:31,000
and see things clearly.

310
00:18:31,000 --> 00:18:35,000
It's what leads us to new bandit, freedom from suffering.

311
00:18:35,000 --> 00:18:38,000
It's just the meaning is it's all on us.

312
00:18:38,000 --> 00:18:41,000
Up to us whether we're going to be the spoon or the tongue.

313
00:18:41,000 --> 00:18:42,000
So the analogy ends there.

314
00:18:42,000 --> 00:18:44,000
It doesn't mean that if you're a spoon, you're always going to be a spoon.

315
00:18:44,000 --> 00:18:47,000
It just means don't be a spoon.

316
00:18:47,000 --> 00:18:50,000
Be a tongue and try to receive the teachings.

317
00:18:50,000 --> 00:18:54,000
Cultivate knowledge and understanding so you can understand the teachings.

318
00:18:54,000 --> 00:18:58,000
And don't think that just because you've memorized all the Buddha's teachings

319
00:18:58,000 --> 00:19:00,000
or you know how to practice meditation,

320
00:19:00,000 --> 00:19:04,000
that you're a Buddhist or that you're practicing the Buddha's teaching.

321
00:19:04,000 --> 00:19:06,000
You should ever take that for granted.

322
00:19:06,000 --> 00:19:10,000
You find them the more you practice and the longer that you.

323
00:19:10,000 --> 00:19:18,000
And associate with Buddhism with the Buddha's teaching,

324
00:19:18,000 --> 00:19:21,000
the deeper your understanding of the same teachings gets.

325
00:19:21,000 --> 00:19:25,000
And you realize that there are still things that you're missing.

326
00:19:25,000 --> 00:19:30,000
It gets more and more refined and your sense of taste.

327
00:19:30,000 --> 00:19:33,000
It becomes more and more refined until you can taste the dhamma,

328
00:19:33,000 --> 00:19:36,000
which has the taste of freedom.

329
00:19:36,000 --> 00:19:40,000
So simple verse, simple story, simple meaning.

330
00:19:40,000 --> 00:19:45,000
And you have to cultivate knowledge and understanding.

331
00:19:45,000 --> 00:19:48,000
Understanding of the Buddha's teaching requires

332
00:19:48,000 --> 00:19:53,000
us to be ready for it or us to be in a condition to understand the truth.

333
00:19:53,000 --> 00:19:56,000
So that's the story for tonight.

334
00:19:56,000 --> 00:20:01,000
Teaching on the dhamma pada, thank you for tuning in and for coming out.

335
00:20:01,000 --> 00:20:03,000
I'll go see right here locally.

336
00:20:03,000 --> 00:20:07,000
I wish you all to find true peace, happiness and freedom from suffering.

