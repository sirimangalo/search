1
00:00:00,000 --> 00:00:08,720
Yeah, I think one thing we should always keep in mind is that we're here to try to help.

2
00:00:08,720 --> 00:00:16,800
And so rather than just getting too much of track, we should always think that there

3
00:00:16,800 --> 00:00:21,040
are people behind these questions.

4
00:00:21,040 --> 00:00:25,440
We've had a lot of discussions evening about not wanting to help people and not going

5
00:00:25,440 --> 00:00:30,400
out of your way and that we're not here to help people.

6
00:00:30,400 --> 00:00:36,600
But insofar as this is part of our practice, the wholesomeness of it does come from helping

7
00:00:36,600 --> 00:00:37,600
other people.

8
00:00:37,600 --> 00:00:45,880
And the way it helps you is by being compassionate and by being kind and generous in

9
00:00:45,880 --> 00:00:47,640
terms of getting the done.

10
00:00:47,640 --> 00:00:55,400
So insofar as you're willing to do that, I just think we should keep that in mind.

11
00:00:55,400 --> 00:01:01,440
And from time to time, I remind myself that that's what's really important in these

12
00:01:01,440 --> 00:01:05,840
sessions is that people do actually benefit from it.

13
00:01:05,840 --> 00:01:12,800
And that's just don't have open up like a, remember peanuts, you know, a Charlie Brown

14
00:01:12,800 --> 00:01:13,800
and Snoopy.

15
00:01:13,800 --> 00:01:20,280
Just don't be like Lucy and open up a stand that says a Buddhist therapy, you know, five

16
00:01:20,280 --> 00:01:29,960
or five, no, no, wind up with lots of Charlie Brown's, oh, yeah, I spent a year reading

17
00:01:29,960 --> 00:01:34,800
peanuts comics only like it was the only thing I read when I was about eight years old.

18
00:01:34,800 --> 00:01:38,360
My parents just taught me a book shelf full of peanuts comics and it was the only thing

19
00:01:38,360 --> 00:01:44,720
I would read and it was a leading cause of depression for me.

20
00:01:44,720 --> 00:01:54,240
I think Charles should show us to read the service to humanity by creating peanuts because

21
00:01:54,240 --> 00:01:59,040
Charlie Brown is the worst role model.

22
00:01:59,040 --> 00:02:04,440
Anyway, I still shudder to think of, you know, in pity of this guy.

23
00:02:04,440 --> 00:02:06,920
Why doesn't he ever win a baseball game?

24
00:02:06,920 --> 00:02:07,920
Yeah.

25
00:02:07,920 --> 00:02:14,480
It's like you, the only guy that really had to stuck together was like Schroeder and maybe

26
00:02:14,480 --> 00:02:22,040
in a line of the way peppermint patty, you don't give a line of sending credit line.

27
00:02:22,040 --> 00:02:24,280
Is, was he the one with the blanket?

28
00:02:24,280 --> 00:02:25,280
Yeah.

29
00:02:25,280 --> 00:02:27,440
Why was the philosopher?

30
00:02:27,440 --> 00:02:28,440
He was the philosopher.

31
00:02:28,440 --> 00:02:29,440
He was the Buddhist.

32
00:02:29,440 --> 00:02:31,480
Oh, I was like that.

33
00:02:31,480 --> 00:02:32,480
Okay.

34
00:02:32,480 --> 00:02:34,640
I could give it to him.

35
00:02:34,640 --> 00:02:37,480
He was the one that waited for the great pumpkin, right?

36
00:02:37,480 --> 00:02:39,520
See the philosopher.

37
00:02:39,520 --> 00:02:40,520
Yeah.

38
00:02:40,520 --> 00:02:44,400
Well, you know, if you're talking about depression, let's throw a little bit of

39
00:02:44,400 --> 00:02:51,280
what Winnie the Pooh into that Winnie the Pooh is really when you take a step back, it's

40
00:02:51,280 --> 00:02:54,280
really a bummer.

41
00:02:54,280 --> 00:02:58,400
It's all about suffering, it's a very Buddhist too.

42
00:02:58,400 --> 00:03:03,800
Did you ever read A, Milns books, yeah, actually Winnie the Pooh books?

43
00:03:03,800 --> 00:03:04,800
Yeah.

44
00:03:04,800 --> 00:03:12,960
Only, you know, when I was like, what, 10 and it was just overwhelming for me.

45
00:03:12,960 --> 00:03:17,840
Just a few more robins and the little woods of what a hundred acres.

46
00:03:17,840 --> 00:03:20,320
Do you ever read the Dao's Pooh?

47
00:03:20,320 --> 00:03:22,160
Yeah, I love that.

48
00:03:22,160 --> 00:03:28,760
I love especially Eeyore and he's in the river and everybody's trying to get him out and

49
00:03:28,760 --> 00:03:31,840
Pooh just comes along and what drops a rock on him.

50
00:03:31,840 --> 00:03:32,840
Yeah.

51
00:03:32,840 --> 00:03:36,320
He goes to the bottom and then walks out of the river.

52
00:03:36,320 --> 00:03:37,320
Right.

53
00:03:37,320 --> 00:03:38,320
Yeah.

54
00:03:38,320 --> 00:03:45,320
He goes to the bottom and he goes to the bottom and he goes to the bottom and then he goes

