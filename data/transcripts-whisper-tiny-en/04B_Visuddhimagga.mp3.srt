1
00:00:00,000 --> 00:00:05,000
So, we come to the end of questions and answers.

2
00:00:05,000 --> 00:00:10,000
What is what you, in what sense, is it what you and so on?

3
00:00:10,000 --> 00:00:18,000
Now, what is the defining of it, and what is the cleansing of it?

4
00:00:18,000 --> 00:00:26,000
Now, monks are exalted to keep rules, very intact,

5
00:00:26,000 --> 00:00:29,000
and to keep the sealer very pure.

6
00:00:29,000 --> 00:00:37,000
So, the ideal state is that the precepts are not broken anywhere.

7
00:00:37,000 --> 00:00:40,000
And so, there are explain here,

8
00:00:40,000 --> 00:00:43,000
when a man has broken the training course at the beginning,

9
00:00:43,000 --> 00:00:47,000
or at the end, in any instance of the seventh class of offenses,

10
00:00:47,000 --> 00:00:52,000
his virtue is called tawn, like a clock that is cut at the edge.

11
00:00:52,000 --> 00:00:56,000
So, his virtue should be tawn.

12
00:00:56,000 --> 00:01:01,000
If it is broken, at the beginning, or at the end,

13
00:01:01,000 --> 00:01:04,000
then it is called tawn sealer.

14
00:01:04,000 --> 00:01:08,000
But when he has broken, in the middle, it is called rent,

15
00:01:08,000 --> 00:01:12,000
like a cloth that is rent in the middle.

16
00:01:12,000 --> 00:01:16,000
When he has broken, it twice or twice in succession.

17
00:01:16,000 --> 00:01:20,000
It is called blotched, like a cow whose body is some,

18
00:01:20,000 --> 00:01:23,000
some such color as black or red with it,

19
00:01:23,000 --> 00:01:27,000
this discrepancy of color appearing on the back of the belly.

20
00:01:27,000 --> 00:01:30,000
When he has broken it all over, at intervals,

21
00:01:30,000 --> 00:01:34,000
it is called mottled, like a cow.

22
00:01:34,000 --> 00:01:37,000
Broken it all over.

23
00:01:37,000 --> 00:01:43,000
It's like a cow, speckled all over with discrepancy of color,

24
00:01:43,000 --> 00:01:45,000
sports at intervals.

25
00:01:45,000 --> 00:01:47,000
This is the first place.

26
00:01:47,000 --> 00:01:52,000
This in the first place is how there comes to be tawnness with the bridge

27
00:01:52,000 --> 00:01:58,000
that has gained, et cetera, as its cause, and so on.

28
00:01:58,000 --> 00:02:05,000
Now, likewise with the seven bones of sexuality,

29
00:02:05,000 --> 00:02:12,000
it means seven kinds of engagement in sexuality.

30
00:02:12,000 --> 00:02:27,000
A here, Brahman, some ascetic or Brahman claims to lead the life of purity rightly.

31
00:02:27,000 --> 00:02:29,000
For he does not.

32
00:02:29,000 --> 00:02:33,000
It is a little inaccurate.

33
00:02:33,000 --> 00:02:40,000
It should say some ascetics or Brahman claiming to lead the life of purity rightly,

34
00:02:40,000 --> 00:02:42,000
but that's not.

35
00:02:42,000 --> 00:02:44,000
It should go like that.

36
00:02:44,000 --> 00:02:48,000
So you claim to be leading a life of purity,

37
00:02:48,000 --> 00:02:50,000
but you do this, something like that.

38
00:02:50,000 --> 00:02:55,000
So some ascetic or Brahman claiming to lead the life of purity rightly,

39
00:02:55,000 --> 00:03:00,000
but that's not in the end of your sexual intercourse with women.

40
00:03:00,000 --> 00:03:06,000
Yet he agrees to massage manipulation, bathing, and rubbing down by women.

41
00:03:06,000 --> 00:03:14,000
In our country, these are called minor sexuality.

42
00:03:14,000 --> 00:03:21,000
It is not sexual intercourse, but the opportunity to sexuality,

43
00:03:21,000 --> 00:03:26,000
so they are called minor sexuality, these seven things.

44
00:03:26,000 --> 00:03:36,000
Agreeing to massage and so on.

45
00:03:36,000 --> 00:03:44,000
When, although a person may not break the root,

46
00:03:44,000 --> 00:03:48,000
which forbid sexual intercourse, if he is thinking of this,

47
00:03:48,000 --> 00:03:53,000
then his sealer is said to be immature, not broken,

48
00:03:53,000 --> 00:04:01,000
but it becomes immature because he is thinking of these things,

49
00:04:01,000 --> 00:04:10,000
or he is taking delight in being massage done to him and so on.

50
00:04:10,000 --> 00:04:13,000
So this is a kind of sexuality.

51
00:04:13,000 --> 00:04:18,000
So these are called minor sexualities in our country.

52
00:04:18,000 --> 00:04:41,000
So if we want to keep sealer pure, then we have to avoid doing these things too.

53
00:04:41,000 --> 00:04:51,000
And on page 53, paragraph 154, about the middle,

54
00:04:51,000 --> 00:04:58,000
owing to that unvirtuousness, he is as ugly as him cloth.

55
00:04:58,000 --> 00:05:04,000
Contact with him is painful because those who fall in with his views.

56
00:05:04,000 --> 00:05:06,000
What is that?

57
00:05:06,000 --> 00:05:08,000
Do fall in with his views.

58
00:05:08,000 --> 00:05:12,000
What does that mean?

59
00:05:12,000 --> 00:05:14,000
Agree with him?

60
00:05:14,000 --> 00:05:16,000
The greenstone.

61
00:05:16,000 --> 00:05:18,000
The greenstone?

62
00:05:18,000 --> 00:05:20,000
The greenstone is better.

63
00:05:20,000 --> 00:05:22,000
Right.

64
00:05:22,000 --> 00:05:31,000
The Parley Ward is to imitate him, to follow his conduct.

65
00:05:31,000 --> 00:05:52,560
Now, sometimes the Pali words can be misleading if you do not know the exact formation

66
00:05:52,560 --> 00:05:54,200
of these words.

67
00:05:54,200 --> 00:06:06,080
The Pali word here is Ditha nugati and it could be Dithi a nugati or Ditha a nugati and

68
00:06:06,080 --> 00:06:11,480
it really is Ditha a nugati and not Dithi a nugati.

69
00:06:11,480 --> 00:06:19,240
But Nyanamali took it to be Dithi a nugati that is why you see the word view here, Dithi

70
00:06:19,240 --> 00:06:25,520
means wrong view, right? So, to fall in with this views, the Pali word is to be separated

71
00:06:25,520 --> 00:06:33,560
as Ditha a nugati. Ditha means what is seen and a nugati is following. So, following what

72
00:06:33,560 --> 00:06:45,880
is seen that means following his conduct, is there a good word for this?

73
00:06:45,880 --> 00:06:55,600
Emity. Emity dating him, brought to long lasting suffering in the states of loss and

74
00:06:55,600 --> 00:07:06,400
he is like a log from a pyre, a schooner of fire and the reference is not ninety-nine, but

75
00:07:06,400 --> 00:07:15,680
90, although you may not be using this reference.

76
00:07:15,680 --> 00:07:31,840
Now on page 154, the paragraph 155, do you understand that message?

77
00:07:31,840 --> 00:07:37,440
Now the blessed one has shown that when the unvirtuous have their minds captured by pleasure

78
00:07:37,440 --> 00:07:42,640
and satisfaction in the indulgence of the five quads of sense desires.

79
00:07:42,640 --> 00:07:47,960
In receiving solicitation, in being honored, etc., the result of that comma, directly visible

80
00:07:47,960 --> 00:07:53,160
in all ways, is very violent pain with that comma as its condition, capable of producing

81
00:07:53,160 --> 00:08:07,360
a gush of hot blood by causing agony of heart with a mere recollection of it.

82
00:08:07,360 --> 00:08:22,760
So that message must mean this.

83
00:08:22,760 --> 00:08:30,520
Now the blessed one who has directly seen the result of comma in all ways, so Buddha

84
00:08:30,520 --> 00:08:37,520
has seen the comma and its results in all ways.

85
00:08:37,520 --> 00:08:54,120
And wanting to show the very bitter pain, to be experienced by, we should go there.

86
00:08:54,120 --> 00:08:58,620
So who has directly seen the result of comma in all ways and wanting to show the very

87
00:08:58,620 --> 00:09:05,020
bitter pain, to be experienced by unvirtuous persons whose minds I captured by pleasure

88
00:09:05,020 --> 00:09:12,820
and satisfaction mentioned above said, has said, and then what kind of pain, which is

89
00:09:12,820 --> 00:09:18,320
caused by pleasure and satisfaction in the indulgence of the five quads of sensual desires.

90
00:09:18,320 --> 00:09:28,620
So when the person indulges in these sensual desires, then they will come pain.

91
00:09:28,620 --> 00:09:34,940
And caused by pleasure and satisfaction in receiving solicitation, that means, suppose I am

92
00:09:34,940 --> 00:09:41,420
a monk and I have broken the most important rule, then I am not a monk at all, but I

93
00:09:41,420 --> 00:09:48,320
crank myself to be monk and then I receive a salutation of bind on from lay people.

94
00:09:48,320 --> 00:09:55,420
And so that is, as a result of that, then I am being suffered in hair or in the woeful

95
00:09:55,420 --> 00:09:56,520
states.

96
00:09:56,520 --> 00:10:04,000
So pleasure and satisfaction caused by pleasure and satisfaction in receiving salutations

97
00:10:04,000 --> 00:10:06,880
or being honored, etc.

98
00:10:06,880 --> 00:10:13,640
And that pain which is capable of producing a gush of hot blood by causing agony of heart

99
00:10:13,640 --> 00:10:17,680
with near recollection of unvirtuousness.

100
00:10:17,680 --> 00:10:28,640
Now, suppose I am unvirtuous and when I think of my unvirtuousness, I will have remorse

101
00:10:28,640 --> 00:10:35,280
and depression and this can cause me to vomit blood.

102
00:10:35,280 --> 00:10:40,440
So it is capable of producing a gush of hot blood by causing agony of heart just by

103
00:10:40,440 --> 00:10:45,440
mere recollection of unvirtuousness.

104
00:10:45,440 --> 00:10:54,000
So this kind of pain is very bitter pain is to be experienced by those who are unvirtuous

105
00:10:54,000 --> 00:10:59,120
and whose minds I kept at by pleasure and satisfaction mentioned above.

106
00:10:59,120 --> 00:11:02,680
So it is to be translated that way.

107
00:11:02,680 --> 00:11:06,040
Although it is also a little odd.

108
00:11:06,040 --> 00:11:14,720
It still doesn't make sense to me the second paragraph because it says, or, what do you

109
00:11:14,720 --> 00:11:20,640
think, whose, which is better that one should sit down or lie down and bracing that mess

110
00:11:20,640 --> 00:11:25,600
of fire burning, glazing and glowing, or that he should sit down or lie down and bracing

111
00:11:25,600 --> 00:11:29,200
a warrior noble maiden or a problem maiden.

112
00:11:29,200 --> 00:11:36,160
It's like either or, which way it's asking you to understand it.

113
00:11:36,160 --> 00:11:44,600
That one should sit down or lie down and bracing that of a mess of burning fire and

114
00:11:44,600 --> 00:11:51,880
glazing and glowing, all that he should sit down or lie down and bracing a warrior noble

115
00:11:51,880 --> 00:11:56,840
maiden and so on.

116
00:11:56,840 --> 00:12:02,000
It is better.

117
00:12:02,000 --> 00:12:10,960
Once it's down and bracing a mess of fire burning or once it's down and bracing a warrior

118
00:12:10,960 --> 00:12:18,560
maiden and so on, I think all is good here, which is better.

119
00:12:18,560 --> 00:12:32,520
I'm bracing, sitting and bracing a fire or sitting and bracing a maiden.

120
00:12:32,520 --> 00:12:41,840
And in paragraph 156, about four lines down, you will need that of unclean and suspect

121
00:12:41,840 --> 00:12:52,360
habits, secretive of his eggs, who is not an ascetic and claims to be one, so that's

122
00:12:52,360 --> 00:13:09,360
all right, I think, who is not an ascetic but claims to be one.

123
00:13:09,360 --> 00:13:29,160
Then on page 56, one word is missing, just above the verses, paragraph 158.

124
00:13:29,160 --> 00:13:39,160
So what therefore should stand at the head of the beginning of the paragraph 158, therefore

125
00:13:39,160 --> 00:13:43,560
what pleasure has a man of broken virtue for sagging not sense pleasures, which perfect

126
00:13:43,560 --> 00:13:45,920
and so on.

127
00:13:45,920 --> 00:13:54,200
So the advantage of being virtuous and that disadvantage of being unvirtuous, these two are

128
00:13:54,200 --> 00:13:59,480
given here.

129
00:13:59,480 --> 00:14:07,960
And then on page 57, the first line, he is not free from any sort of terror or it could

130
00:14:07,960 --> 00:14:12,800
be translated as danger, any sort of danger.

131
00:14:12,800 --> 00:14:24,600
And on food line, he is well set upon the road to hell, if we mean all woeful states

132
00:14:24,600 --> 00:14:31,160
by hell, then it will be all right.

133
00:14:31,160 --> 00:14:38,200
But here, what is meant is not not hell only, but the other woeful states also, there are

134
00:14:38,200 --> 00:14:45,040
full woeful states.

135
00:14:45,040 --> 00:14:55,920
And then down, further down the page, second verse from the bottom, there are no kankers

136
00:14:55,920 --> 00:14:59,160
here and now, now.

137
00:14:59,160 --> 00:15:08,480
There are no dangers here and now, not kankers.

138
00:15:08,480 --> 00:15:14,760
There are no dangers here and now, to plague the virtuous man at all.

139
00:15:14,760 --> 00:15:23,480
Okay, okay, I think that's it, in the first chapter.

140
00:15:23,480 --> 00:15:35,920
So you have seen that mostly the monks virtue or the precept or the monks is treated in

141
00:15:35,920 --> 00:15:39,360
this chapter.

142
00:15:39,360 --> 00:15:49,640
But some are some advices applicable to virtue of lay people and also of the nuns and

143
00:15:49,640 --> 00:15:50,640
so on.

144
00:15:50,640 --> 00:15:55,160
But many of them are only for monks.

145
00:15:55,160 --> 00:16:09,200
For lay person, the keeping five precepts is just enough because the Buddha said that

146
00:16:09,200 --> 00:16:17,000
a person who claims himself to be a disciple of Buddha should keep his moral conduct

147
00:16:17,000 --> 00:16:22,320
pure and that means he should keep five precepts.

148
00:16:22,320 --> 00:16:29,560
So if you take five precepts before going to practice meditation, that's all right.

149
00:16:29,560 --> 00:16:38,440
So for lay people, it is easier to have purification of morals, purity of morals than monks.

150
00:16:38,440 --> 00:16:45,240
So monks have disadvantage here because monks have so many rules to keep and then if they

151
00:16:45,240 --> 00:16:53,080
have broken some rules and if they do not get rid of them, get rid of the offense, then

152
00:16:53,080 --> 00:17:01,040
that state can be a block to the concentration and progress.

153
00:17:01,040 --> 00:17:09,560
So they must do confession or if the offenders are graver, then they must do something

154
00:17:09,560 --> 00:17:14,640
like living under probation for some period of time.

155
00:17:14,640 --> 00:17:25,240
And so that is not so easy for monks to have complete purity of morals than lay people.

156
00:17:25,240 --> 00:17:34,720
So for lay people, although they may have done broken rules in the past, before practice

157
00:17:34,720 --> 00:17:41,360
of meditation, if they take precepts and means sincerely and really try to keep them, then

158
00:17:41,360 --> 00:17:43,800
that's enough for them.

159
00:17:43,800 --> 00:17:57,440
So purity of morals for the practice of meditation is easier for lay people than for monks.

160
00:17:57,440 --> 00:18:05,560
Any questions?

161
00:18:05,560 --> 00:18:19,800
Some monks have more time to devote to this study and practice.

162
00:18:19,800 --> 00:18:31,080
That is why I said in one of the talks that those who study Visodemaga, those monks

163
00:18:31,080 --> 00:18:36,760
who study Visodemaga, either give up their rules or go into the forest and practice

164
00:18:36,760 --> 00:18:38,080
meditation.

165
00:18:38,080 --> 00:19:04,240
I don't need to give you a table of content because it's plain, second chapter.

166
00:19:04,240 --> 00:19:11,240
I wish I read that chapter 25 pages.

