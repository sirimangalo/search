1
00:00:00,000 --> 00:00:03,200
Noting, knowing, question.

2
00:00:03,200 --> 00:00:08,720
What should one do if one cannot find a name for an arisen phenomenon?

3
00:00:08,720 --> 00:00:10,160
Answer.

4
00:00:10,160 --> 00:00:17,360
In most cases, noting, either knowing, knowing, or feeling, feeling

5
00:00:17,360 --> 00:00:22,000
will be accurate enough to create objective awareness of any phenomenon

6
00:00:22,000 --> 00:00:28,640
that is not as easily categorizable as seeing or hearing, etc.

7
00:00:28,640 --> 00:00:30,000
Question.

8
00:00:30,000 --> 00:00:33,280
What should one do when a phenomena is so brief?

9
00:00:33,280 --> 00:00:37,520
One is only aware of it after it has already disappeared.

10
00:00:37,520 --> 00:00:38,880
Answer.

11
00:00:38,880 --> 00:00:43,600
At that moment, there is a knowledge of the disappearance of the phenomena.

12
00:00:43,600 --> 00:00:49,200
Noting, knowing, knowing is enough to prevent any uncertainty or judgment

13
00:00:49,200 --> 00:01:05,120
about the object that has already ceased.

