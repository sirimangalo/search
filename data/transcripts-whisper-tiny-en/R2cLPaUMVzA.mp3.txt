Bhante, so in short all I have to do is mindfully know everything that arises in my mind
and even if I don't know anything else about the dhamma, I'd be able to achieve navanda.
So maybe I should not bother with anything else but mindfulness.
Sounds good, no?
It could theoretically work.
The Buddha said, A. K. Anoyang bhui mango mindfulness is the one-way path or even the only
path depending on how you interpret that.
But a simple answer is it only really works if you've got a very strong will to do that.
And I would say therefore a very strong wholesomeness already in your mind.
I would say if you're probably, you're already enlightened, let's say, or you have a teacher
who can guide you through all the mistakes that you're going to make, all the times when
you're actually not doing that.
But I would say for all intents and purposes, that's a fairly good outlook.
I don't see, there's not anything really wrong with the idea.
If you fix that in your mind that mindfulness is really the key here.
You won't go that far wrong, it's just that while you're not at a state of good mindfulness
you're going to make lots of mistakes, right?
Because you're not yet there, you're potentially going to kill and steal and lie and cheat
and even more likely, engage in things that are going to lead you down the wrong path.
You're going to misinterpret mindfulness, you're going to not realize that you're not
actually being mindful, think that you're being mindful and you're actually forcing,
you're actually being partial and training the mind in a certain way and cultivating
some kara's in the mind, artificial constructs, artificial habits, doing all sorts of unwholesome
things because you're not really mindful, you don't really know how to be mindful.
You can only truly be mindful once you've cultivated, once you understand, once you've
practiced and become proficient in it.
So until you get there, the question is how are you going to get there on your own?
If you don't have a teacher, really the only way to do it is to read a lot and to make
sure that you're not falling into any of the many pitfalls that are discussed in the
pitika. But if you have a teacher on the other hand, you just do your mindfulness and they
will remind you, they'll tell you, okay, you have to keep these rules, so you'll keep
those rules.
They'll tell you, no, you have to stop this, you have to stop that or you're being mindful
of this.
That's not really being mindful and so on and so on and then they'll be able to catch
you and bring you back to being mindful, but absolutely mindfulness is the key.
Mindfulness is kind of like the thread through the through necklace.
So if the necklace is, the brief one is just morality, concentration, and wisdom, mindfulness
is the thread that goes through the mall.
It starts in the morality, so true morality comes through mindfulness.
Once you have morality that it cultivates concentration, your mind starts to get focused,
but that comes through even more mindfulness and through being mindful in a concentrated
state, leads to wisdom, but mindfulness is what you use from beginning to end.
Mostly the most important one.
All the rest is just guidance to keep you on track when you fall off, when you stop really
being mindful.
