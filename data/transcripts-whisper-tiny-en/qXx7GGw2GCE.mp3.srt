1
00:00:00,000 --> 00:00:10,760
Okay, hello everyone, back with more videos, more questions.

2
00:00:10,760 --> 00:00:27,720
So today's question is on how to not react to praise and blame in life people, praise

3
00:00:27,720 --> 00:00:35,960
as some people will praise us, sometimes we will be blamed, people speak well of us, we will

4
00:00:35,960 --> 00:00:51,680
be spoken well of, we will be spoken poorly of, if you've ever been on the internet, this

5
00:00:51,680 --> 00:00:59,160
should be familiar to you, when people say good things about you, post something somewhere

6
00:00:59,160 --> 00:01:07,320
and someone likes it, makes you feel good, someone just like this video, I feel good when

7
00:01:07,320 --> 00:01:15,560
someone likes your things, they don't have dislikes on many a site, on the famous sites, but

8
00:01:15,560 --> 00:01:24,280
someone posts a nasty comment, says something bad about you criticizes you, get a lot of

9
00:01:24,280 --> 00:01:33,960
that on YouTube, you can feel pretty bad, but this is part of society, this is something

10
00:01:33,960 --> 00:01:42,200
we encounter throughout our lives, when you're at work, when your boss praises you or

11
00:01:42,200 --> 00:01:51,600
blames you, your co-workers say bad things about you or good things about you, we want

12
00:01:51,600 --> 00:01:59,480
people to say we're beautiful, we're handsome, we're smart, we're wise, and yet people

13
00:01:59,480 --> 00:02:08,720
will say the opposite, they will say things like you're ugly, you're dumb, you're not

14
00:02:08,720 --> 00:02:25,960
your unwise, you become quite sensitive to these things, so it's part of life that deserves

15
00:02:25,960 --> 00:02:42,360
a question, the question is well formed, an ordinary question might be, how do we make

16
00:02:42,360 --> 00:02:47,880
it so that people only praise us, I don't think people, don't think you hear people ask

17
00:02:47,880 --> 00:02:53,360
that so much, but it's an unconscious sort of question that an ordinary person would

18
00:02:53,360 --> 00:03:00,360
ask them, an ordinary person who doesn't meditate, a person who's not interested in spirituality,

19
00:03:00,360 --> 00:03:09,800
might spend much of their time trying to find ways to only be praised and never be blamed,

20
00:03:09,800 --> 00:03:15,840
like everything, this praise and blame are in the context of what we call the local

21
00:03:15,840 --> 00:03:25,880
dumb, the worldly dumbness, the vicissitudes of life, praise and blame, there's gain and

22
00:03:25,880 --> 00:03:42,440
loss, there's fame and whatever the opposite of fame is, I don't think it's infamy,

23
00:03:42,440 --> 00:03:52,680
I think the opposite is, and it's not fame, it's high social status, and low social

24
00:03:52,680 --> 00:03:58,360
status, so it's not just being famous having a lot of people know about you, it's being

25
00:03:58,360 --> 00:04:08,160
high, high society, and low society, like in high school, are you one of the popular

26
00:04:08,160 --> 00:04:21,400
kids, are you one of the reject, and the fourth one is happiness and suffering, to these

27
00:04:21,400 --> 00:04:30,240
four sets of realities, vicissitudes, they change, you can't really have one without the

28
00:04:30,240 --> 00:04:40,160
other, you can't have life without all eight of these, they take turns, nobody said,

29
00:04:40,160 --> 00:04:48,320
put us a local dummy, jitanya sana kampati, whoever's mind when touched by these vicissitudes

30
00:04:48,320 --> 00:05:04,960
of life is unshaken, a soul kang, wirajang kamang, not sorrow, not sorrowful, not disturbed

31
00:05:04,960 --> 00:05:18,840
or corrupted by them, corrupted by them, safe kamang, eight among alamut among, this is the

32
00:05:18,840 --> 00:05:28,400
greatest blessing, it's a description of enlightenment in a way, the person does not move

33
00:05:28,400 --> 00:05:32,120
by the vicissitudes of life.

34
00:05:32,120 --> 00:05:42,680
So the first thing to note when you're dealing with these qualities is the gaining of

35
00:05:42,680 --> 00:05:53,680
a wisdom about them, that you can't have one without the other, that starting with gain

36
00:05:53,680 --> 00:06:01,120
and loss, you can't always gain, you will not always be getting what you want, you can't

37
00:06:01,120 --> 00:06:17,120
always get what you want, but gain and loss are a part of life, so we can't guarantee that

38
00:06:17,120 --> 00:06:24,360
we're always going to have the things that we want, that we're always going to gain and

39
00:06:24,360 --> 00:06:36,600
that the things that we own will never leave us, death and manipulation and disaster

40
00:06:36,600 --> 00:06:45,880
can come, natural disasters can wipe out people's livelihood in a day, sickness can

41
00:06:45,880 --> 00:06:54,200
often lead to great loss, we can of course lose our loved ones when they pass away or

42
00:06:54,200 --> 00:07:04,600
when they leave us or when they change and as with the others, high society, low society,

43
00:07:04,600 --> 00:07:09,040
we've ever been in high school, you know how that goes, you'll be part of the high

44
00:07:09,040 --> 00:07:16,360
society, you'll be a popular kid one day and suddenly you're unpopular ostracized, very

45
00:07:16,360 --> 00:07:23,760
easy for that to happen and that goes with the rest of life as you go through university

46
00:07:23,760 --> 00:07:38,160
and get a job out into the world, being high society, being low society, very easy to become

47
00:07:38,160 --> 00:07:53,840
and looking down upon reputations, it's a very fickle sort of thing and praise and blame,

48
00:07:53,840 --> 00:08:03,760
they love to be praised, but as you said in life you have both happiness and suffering,

49
00:08:03,760 --> 00:08:19,720
the, we have to see and this is sort of the basis of Buddhism is to see that chasing

50
00:08:19,720 --> 00:08:23,960
the four good ones and running from the four bad ones is never going to be sustainable,

51
00:08:23,960 --> 00:08:36,840
but that way of living that's been taught to us by society, by culture even often by religion

52
00:08:36,840 --> 00:08:47,640
even is not sustainable, it's not, it's not skillful, it's not possible, it's not a good

53
00:08:47,640 --> 00:08:54,080
thing and you'll notice that you gain this sort of wisdom throughout your life, you'll

54
00:08:54,080 --> 00:09:01,920
encounter the four negative ones and in ways that you're unable to or unprepared to

55
00:09:01,920 --> 00:09:09,840
to overcome or to free yourself from and you begin to realize that suffering is not just

56
00:09:09,840 --> 00:09:16,800
something you can escape, it's a part of life, this is something that you can many people

57
00:09:16,800 --> 00:09:22,840
gain as they go through life, those people who have dealt with suffering often become wise

58
00:09:22,840 --> 00:09:29,000
in a sense, you can't always get what you want, you can't always be in possession of

59
00:09:29,000 --> 00:09:37,880
the good ones, so really the first answer is to gain some understanding and even some

60
00:09:37,880 --> 00:09:47,960
experience of this and so listening to teachings is the first way, pondering it, considering

61
00:09:47,960 --> 00:10:00,120
it yourself and then gaining experience about that in your life, this is the sort of the

62
00:10:00,120 --> 00:10:07,760
way that we all deal with these things, we begin to ask these questions about not how

63
00:10:07,760 --> 00:10:12,640
can I get the ones with that I want, but how can I free myself from the need for these

64
00:10:12,640 --> 00:10:26,800
things, my greed for gains and possessions, my lust after position and power and influence,

65
00:10:26,800 --> 00:10:39,960
my craving for praise, my craving for happiness, for pleasure and so this is a very good

66
00:10:39,960 --> 00:10:46,480
sort of reason for people to come to practice meditation is when they start to see these

67
00:10:46,480 --> 00:10:53,120
things when they start to be a part of their outlook, their worldview instead of trying

68
00:10:53,120 --> 00:10:58,480
to get what you want to try and stop one thing and then you begin to undertake to practice

69
00:10:58,480 --> 00:11:01,720
meditation.

70
00:11:01,720 --> 00:11:11,800
So two things that I want to say today, the first one was this idea that there are

71
00:11:11,800 --> 00:11:16,760
both sides, talk about the local demos that there are eight of them and you can't always

72
00:11:16,760 --> 00:11:27,040
have the four good ones, so an answer to how to not be caught up in praise and blame,

73
00:11:27,040 --> 00:11:34,040
not react to them is to understand these eight, understand and have them as a frame of

74
00:11:34,040 --> 00:11:38,280
reference, sort of as a worldview, right, an understanding of other world works.

75
00:11:38,280 --> 00:11:45,840
The second thing was the three different kinds of understanding, so I mentioned them but

76
00:11:45,840 --> 00:11:55,840
to explicitly state the three ways that we learn this, that we learn about the vicissitudes

77
00:11:55,840 --> 00:12:03,000
of life and about everything really, is one through hearing, so when you gain this intellectual

78
00:12:03,000 --> 00:12:08,200
knowledge about this idea of changing the way we look at things from trying to always

79
00:12:08,200 --> 00:12:15,480
get the good things to giving up our need for the good things and our partiality for

80
00:12:15,480 --> 00:12:18,200
certain things.

81
00:12:18,200 --> 00:12:22,720
The second way is through thinking about it, when you consider the things you have heard

82
00:12:22,720 --> 00:12:28,600
and you ponder them over and the third is through experience.

83
00:12:28,600 --> 00:12:36,360
Because this third one, I think even without referencing meditation you can see how it's

84
00:12:36,360 --> 00:12:42,480
the most powerful, when you, as I said, experience this reality that suffering is a part

85
00:12:42,480 --> 00:12:48,760
of life, something you cannot avoid, the white powerful, but that's really the essence

86
00:12:48,760 --> 00:12:56,000
of what meditation is, so when you ask, how do you stop reacting to praise and blame?

87
00:12:56,000 --> 00:13:03,120
When people praise me, I feel really good, when people blame me, I feel really bad, meditation

88
00:13:03,120 --> 00:13:17,000
and mindfulness is about gaining experience and an understanding of the reality behind

89
00:13:17,000 --> 00:13:21,880
praise and blame and behind our attachment to praise and blame, because the only reason why

90
00:13:21,880 --> 00:13:28,360
you wouldn't want to be attached to praise is because you sometimes don't get it, it's

91
00:13:28,360 --> 00:13:36,200
because it's because being attached to praise leads to suffering, to put it specifically

92
00:13:36,200 --> 00:13:44,200
or precisely, and that being the case when you look, when you're observant, when you're

93
00:13:44,200 --> 00:13:50,440
mindful, when you're present during the experience, you'll start to see that, you'll

94
00:13:50,440 --> 00:13:54,400
start to see how attachment to praise leads to suffering.

95
00:13:54,400 --> 00:14:05,320
Or clearly, you'll see that praise itself is meaningless, it's an experience, it's an

96
00:14:05,320 --> 00:14:12,480
experience of hearing, or however you receive it, and processing in the mind, and then

97
00:14:12,480 --> 00:14:18,520
the arising of probably some chemical reaction in the brain that leads to some pleasure,

98
00:14:18,520 --> 00:14:23,600
because that's how you, your brain associates it, there's some kind of association with

99
00:14:23,600 --> 00:14:35,240
the positive state, and so when you break it down like that, you see that the praise

100
00:14:35,240 --> 00:14:43,000
isn't worth getting, the pleasure isn't of any value either, there's no benefit that

101
00:14:43,000 --> 00:14:53,560
comes from being happy about something, and more importantly, that's all there is.

102
00:14:53,560 --> 00:15:01,840
And this is the last thing I wanted to talk about, was that all of all aid of the locomotives,

103
00:15:01,840 --> 00:15:11,640
gain loss, high society, low society, praise, blame, happiness, suffering, all depend

104
00:15:11,640 --> 00:15:23,440
for their power on the existence of consents, and most especially self, who's gain

105
00:15:23,440 --> 00:15:32,160
is at mine, whose loss is at mine, who's got high society, me, who's low society, me,

106
00:15:32,160 --> 00:15:41,120
who are they praising me, who are they blaming me, who's happy me, who suffers me, me,

107
00:15:41,120 --> 00:15:51,440
me, me, mine, mine, mine, and that doesn't exist in reality, but it's not observed during

108
00:15:51,440 --> 00:15:56,080
meditation, it's the most important thing, is when you're during mindfulness, so when

109
00:15:56,080 --> 00:16:02,880
you say to yourself, say happy, happy, when you say pain, pain, and when you like any of

110
00:16:02,880 --> 00:16:07,800
these things and you say liking, liking, as you start to dwell, even when you're just sitting

111
00:16:07,800 --> 00:16:13,360
and you say sitting, sitting, or watching the rising, falling in the stomach, or any of

112
00:16:13,360 --> 00:16:19,280
these things, you're dwelling in a reality that is void of concepts, void of self, there's

113
00:16:19,280 --> 00:16:29,200
no reference by which you would say, that's me, that's mine, that's I, and so all eight

114
00:16:29,200 --> 00:16:33,560
of these disappear, praise and blame, the only way to be free from them is to really not

115
00:16:33,560 --> 00:16:45,640
have a concept of self, because the concept of self is that which leads to conceit, to

116
00:16:45,640 --> 00:16:56,360
believing you deserve praise, or you deserve blame, or being reacting to praise and blame.

117
00:16:56,360 --> 00:17:03,200
If there's no self, you can't react to praise or blame, or you won't have a sense of,

118
00:17:03,200 --> 00:17:07,520
if someone says you're fat, or suppose you're fat, and someone says you're fat, you would

119
00:17:07,520 --> 00:17:11,400
look down and say, that's a correct observation, right?

120
00:17:11,400 --> 00:17:15,760
Why do you say, why do you get upset when someone calls you that, there's a, you have an

121
00:17:15,760 --> 00:17:19,160
attachment to yourself, to a self, right?

122
00:17:19,160 --> 00:17:25,040
This person is calling me this, this person, if you're, if you're, you're like someone

123
00:17:25,040 --> 00:17:28,840
calls you a buffalo, and you're not a buffalo, and you would say, that is an incorrect

124
00:17:28,840 --> 00:17:38,520
statement, this person called me a buffalo, and I'm not a buffalo.

125
00:17:38,520 --> 00:17:44,720
And so I think that is, it's not a solution, but it's an important point to keep in

126
00:17:44,720 --> 00:17:46,240
mind when you're looking for a solution.

127
00:17:46,240 --> 00:17:51,600
You want to be free from your attachment and the suffering that comes from the attached

128
00:17:51,600 --> 00:17:57,360
from to these things, you have to let go of self, let go of the concept of I, which really

129
00:17:57,360 --> 00:18:03,640
only comes from gaining a new perspective on reality, that's free from that, that reality

130
00:18:03,640 --> 00:18:10,640
which is based on real things, like seeing, hearing, smelling, tasting, feeling, thinking,

131
00:18:10,640 --> 00:18:21,520
experience.

132
00:18:21,520 --> 00:18:32,880
And so I guess one more thing is that when you say, how can I not react, we have to, and

133
00:18:32,880 --> 00:18:38,240
this goes with all sorts of questions like this, how can I free myself from this or that?

134
00:18:38,240 --> 00:18:42,120
It was the same with the anxiety, depression one is you can't turn these things off.

135
00:18:42,120 --> 00:18:44,400
It's an important part of this.

136
00:18:44,400 --> 00:18:51,080
So when we talk about freeing yourself from, from the idea of self, letting go of any

137
00:18:51,080 --> 00:18:55,600
conception of self, it's something that comes through training.

138
00:18:55,600 --> 00:19:00,040
When we talk about seeing that the things you cling to are not worth clinging to, we

139
00:19:00,040 --> 00:19:03,960
talk about seeing that clinging to the means to suffering and so on.

140
00:19:03,960 --> 00:19:13,400
All of this has to come through practice because reacting to praise and blame or anything

141
00:19:13,400 --> 00:19:14,400
is habitual.

142
00:19:14,400 --> 00:19:17,160
It's a habit that you develop.

143
00:19:17,160 --> 00:19:22,920
And it's caught up in many kinds of habits, many, many habits, including the habitual

144
00:19:22,920 --> 00:19:30,400
conception of self, reaffirming the self, reifying the self.

145
00:19:30,400 --> 00:19:35,560
And so the only way to be free from it is through cultivating new habits and that's what

146
00:19:35,560 --> 00:19:37,320
meditation is all about.

147
00:19:37,320 --> 00:19:42,520
It's about changing the direction of the mind, the inclination of the mind, instead of

148
00:19:42,520 --> 00:19:51,800
inclining the mind to react, inclining the mind to observe, to experience, to be aware.

149
00:19:51,800 --> 00:20:03,000
Over time, that practice leads to new habits of objectivity, of dispassion.

150
00:20:03,000 --> 00:20:07,480
When you no longer get excited, when someone calls you fat or uglier, stupid, or this

151
00:20:07,480 --> 00:20:11,920
or that, or when someone praises you and said, good job, way to go.

152
00:20:11,920 --> 00:20:16,560
You're so smart, you're so beautiful, you're so wise.

153
00:20:16,560 --> 00:20:29,800
You let go of these things, you become dispassionate about them, content, content with

154
00:20:29,800 --> 00:20:37,720
reality, not depending on externalities, such a horrible thing, right, to depend on

155
00:20:37,720 --> 00:20:39,120
other people's opinions of you.

156
00:20:39,120 --> 00:20:40,600
But this is what we do.

157
00:20:40,600 --> 00:20:42,000
And you can't just turn it off.

158
00:20:42,000 --> 00:20:43,000
We hate it.

159
00:20:43,000 --> 00:20:48,560
We hate that this person can say it is a bad thing about me and make me feel bad, but

160
00:20:48,560 --> 00:20:51,320
we can't just turn it off.

161
00:20:51,320 --> 00:20:59,120
You can train yourself out of it, and you train in mindfulness, it changes the nature

162
00:20:59,120 --> 00:21:07,280
of the mind to be less reactionary and more aware, more present.

163
00:21:07,280 --> 00:21:09,640
It's quite simple, it's not magic.

164
00:21:09,640 --> 00:21:15,480
It's not magic pill, but you change your habits, you change the way you look at things,

165
00:21:15,480 --> 00:21:23,480
change your even your whole framework from one of selves and me and mine, to one of experiences.

166
00:21:23,480 --> 00:21:29,160
And as soon as you start, as soon as you're able, once you're able to look at the world

167
00:21:29,160 --> 00:21:35,280
from perspective of experience, there's no self, there's no concept of me and mine and

168
00:21:35,280 --> 00:21:36,280
so on.

169
00:21:36,280 --> 00:21:40,000
So there you go, that's the video for tonight.

