1
00:00:00,000 --> 00:00:02,000
You

2
00:00:30,000 --> 00:00:32,000
You

3
00:00:48,400 --> 00:00:50,400
Good evening everyone

4
00:00:50,400 --> 00:00:54,800
Welcome to our evening dhamma

5
00:01:09,120 --> 00:01:14,160
We call our practice we passana bhavana or we passana meditation

6
00:01:14,160 --> 00:01:22,880
Our goal is to attain or obtain

7
00:01:24,560 --> 00:01:26,560
Vipasana

8
00:01:30,960 --> 00:01:38,960
Passana means seeing we we implies that it's in a special way or a specific way

9
00:01:38,960 --> 00:01:44,240
The very vipasana implies that

10
00:01:46,400 --> 00:01:48,640
First of all that there are two ways of seeing

11
00:01:50,800 --> 00:01:55,920
And second of all that one of these ways is better than the other

12
00:01:57,760 --> 00:02:00,960
Or at the very least is is

13
00:02:02,000 --> 00:02:05,040
more correct more clear more

14
00:02:05,040 --> 00:02:08,240
Penetrative

15
00:02:12,480 --> 00:02:14,960
At the very least at first of all

16
00:02:16,800 --> 00:02:22,720
It points out that there are two ways of seeing so what are the two ways of seeing that it talks about

17
00:02:26,240 --> 00:02:31,840
When we passana means seeing seeing the three characteristics impermanence

18
00:02:31,840 --> 00:02:33,840
Suffering non-self

19
00:02:35,920 --> 00:02:37,920
Anitya

20
00:02:45,360 --> 00:02:49,920
So the other way of seeing is is the opposite

21
00:02:52,320 --> 00:02:54,320
It's the

22
00:02:54,480 --> 00:02:56,480
way of seeing things or seeing

23
00:02:57,360 --> 00:02:59,360
generally

24
00:02:59,360 --> 00:03:01,600
seeing

25
00:03:01,600 --> 00:03:02,800
stability

26
00:03:02,800 --> 00:03:03,760
Nitya

27
00:03:03,760 --> 00:03:05,760
permanency

28
00:03:06,720 --> 00:03:10,400
And Soka happiness or satisfaction

29
00:03:12,480 --> 00:03:14,480
And at the south

30
00:03:14,480 --> 00:03:25,840
Our ordinary way of seeing things without Vipasana

31
00:03:35,840 --> 00:03:37,840
Is first of all to

32
00:03:37,840 --> 00:03:44,240
Give too much reality or too much primacy

33
00:03:45,920 --> 00:03:47,440
Two

34
00:03:47,440 --> 00:03:49,440
things

35
00:03:50,480 --> 00:03:54,400
We'd say concepts but more familiar with them as things

36
00:03:56,720 --> 00:03:58,960
Because we don't realize that they're just concepts

37
00:04:01,600 --> 00:04:03,600
People places things

38
00:04:03,600 --> 00:04:07,600
Possessions

39
00:04:09,200 --> 00:04:11,200
Entities

40
00:04:12,000 --> 00:04:15,360
The way we look at reality is to give

41
00:04:17,280 --> 00:04:19,280
Primacy primacy

42
00:04:23,040 --> 00:04:25,280
Meaning to give them the base

43
00:04:27,440 --> 00:04:29,440
To make them the base

44
00:04:29,440 --> 00:04:33,440
These impersonal

45
00:04:34,400 --> 00:04:36,400
Unshakeable unchanging things

46
00:04:41,680 --> 00:04:43,680
And at the same time to

47
00:04:45,440 --> 00:04:47,840
Delegitimize to take away from

48
00:04:49,120 --> 00:04:53,280
The legitimacy the truth the reality of experience

49
00:04:55,440 --> 00:04:57,440
Don't trust your senses

50
00:04:57,440 --> 00:04:59,440
Why?

51
00:04:59,440 --> 00:05:01,440
Why? Because

52
00:05:02,960 --> 00:05:04,960
The

53
00:05:07,120 --> 00:05:09,120
Impression you get from them

54
00:05:10,080 --> 00:05:12,080
Might be wrong

55
00:05:12,080 --> 00:05:15,360
But really all that says is don't trust your impressions

56
00:05:15,360 --> 00:05:17,360
Don't stress your

57
00:05:19,840 --> 00:05:23,120
Conclusions about the things you sense

58
00:05:23,920 --> 00:05:25,200
And that's an important distinction

59
00:05:25,200 --> 00:05:28,000
But we go further than that. We say don't trust your senses

60
00:05:29,040 --> 00:05:32,080
Let's science tell you what reality is

61
00:05:34,320 --> 00:05:40,320
And so science has wonderful ways of explaining to us what what the entities are made up of

62
00:05:41,840 --> 00:05:43,840
First it was atoms

63
00:05:44,240 --> 00:05:47,760
I guess first it was molecules, but there was an idea of the atom

64
00:05:48,720 --> 00:05:52,400
Eventually we we understood the atoms. We got down that far

65
00:05:52,400 --> 00:05:55,280
And the word atom means indivisible

66
00:05:55,280 --> 00:05:57,280
So we thought that was those were the things

67
00:05:58,400 --> 00:06:02,080
And then we found out that those weren't even the things and there were things smaller than that

68
00:06:03,280 --> 00:06:08,560
And now it sounds like we're even realizing that even though small things doesn't it's not about getting smaller

69
00:06:10,240 --> 00:06:12,720
We don't really know what the heck they are in the first place

70
00:06:14,880 --> 00:06:19,920
We know there's well we we were quite convinced generally that there's something that there are things

71
00:06:19,920 --> 00:06:26,960
And more generally more practically it's things that we can see people places and and

72
00:06:28,080 --> 00:06:30,320
objects possessions and so on

73
00:06:34,400 --> 00:06:40,720
And the mind our experience our experience is relegated to some kind of secondary

74
00:06:44,000 --> 00:06:46,880
Sort of as an epiphenomenon or something

75
00:06:46,880 --> 00:06:52,000
It's that's like a result or a byproduct of of things

76
00:06:55,600 --> 00:06:58,640
And so it's no wonder I mean that's a that's a big part of why we

77
00:06:59,760 --> 00:07:04,160
Have this false sense and it is false sense of stability

78
00:07:06,480 --> 00:07:08,480
Satisfaction of control

79
00:07:08,480 --> 00:07:15,120
And and not just control but of of existence of

80
00:07:16,560 --> 00:07:23,920
They're being things they're being self me and mine and I right and you and him and her

81
00:07:26,400 --> 00:07:28,400
Things

82
00:07:28,400 --> 00:07:34,960
And that's that's one way of looking at things and it's a problem

83
00:07:38,640 --> 00:07:41,920
Because it doesn't matter whether you trust or don't trust your senses

84
00:07:44,080 --> 00:07:49,600
The important thing is that no matter what atoms or subatomic particles or whatever

85
00:07:53,280 --> 00:07:55,280
It has no bearing on

86
00:07:55,280 --> 00:07:58,480
Your stress level your stress your happiness

87
00:07:59,040 --> 00:08:03,600
No, it has no bearing on what's truly important to us to the individual

88
00:08:05,360 --> 00:08:07,360
Outside of experience

89
00:08:11,680 --> 00:08:14,960
And so it's not about the truth of that

90
00:08:16,240 --> 00:08:18,320
You know that way of looking or that

91
00:08:18,320 --> 00:08:24,720
Paradigm that way of looking and things

92
00:08:25,520 --> 00:08:30,640
It's about it not being meaningful not being consequential and relegating our experience

93
00:08:31,360 --> 00:08:33,360
to some kind of

94
00:08:33,360 --> 00:08:35,360
inferior position where we don't even

95
00:08:35,840 --> 00:08:37,840
worry about it or we we

96
00:08:39,360 --> 00:08:44,080
Disregard it because we know how valuable our our impressions of it can be

97
00:08:44,080 --> 00:08:51,920
By doing that we we ignore what's prime what's prime what's

98
00:08:55,680 --> 00:08:57,680
What's most basic

99
00:08:59,280 --> 00:09:04,800
So the other way of looking at realities you could say it's flipping things around. It's delegitimizing

100
00:09:06,000 --> 00:09:08,000
the realm of things

101
00:09:08,000 --> 00:09:13,440
Reminding us that this box doesn't exist. You don't exist

102
00:09:14,480 --> 00:09:16,480
What are you well, you're made up of

103
00:09:18,160 --> 00:09:20,800
Depends who you asked, but you certainly don't exist

104
00:09:21,760 --> 00:09:24,960
You know to take the body for example does the body exist? I don't know

105
00:09:26,160 --> 00:09:28,720
We know from science that it's made up of cells, but

106
00:09:30,240 --> 00:09:32,240
But much more importantly

107
00:09:32,240 --> 00:09:39,520
Delegitimizing the whole realm of things doesn't it's not about how big or how small or breaking it up into parts

108
00:09:40,960 --> 00:09:48,840
It's about reality being a different sort of thing and that different sort of thing is experiences

109
00:09:50,800 --> 00:09:52,800
So everything that that

110
00:09:54,440 --> 00:09:56,440
we

111
00:09:56,920 --> 00:09:58,920
know or we

112
00:09:58,920 --> 00:10:02,200
Experience everything everything relating to us

113
00:10:03,240 --> 00:10:05,240
About all these things is

114
00:10:06,120 --> 00:10:12,680
Experience without experience. It's nothing doesn't matter that we might as well be talking about Santa Claus's workshop

115
00:10:14,520 --> 00:10:16,520
It

116
00:10:16,520 --> 00:10:21,520
Only matters in regards to experience and so experience is always going on

117
00:10:24,520 --> 00:10:26,520
And and it's always

118
00:10:26,520 --> 00:10:32,040
Primal prime and a primary that's sort of it's always a primary

119
00:10:34,280 --> 00:10:38,520
Are the things are all secondary they all depend on our experience and

120
00:10:39,720 --> 00:10:46,840
That's why there's subjectivity is because two people can look at the same thing and thing and have very different ideas about it

121
00:10:46,840 --> 00:10:49,880
One person says it's good bad beautiful ugly

122
00:10:49,880 --> 00:10:51,880
So

123
00:10:55,320 --> 00:10:57,320
The other way of looking at things

124
00:10:57,640 --> 00:10:59,640
puts experience first

125
00:11:00,200 --> 00:11:06,440
Delegitimizes those things, but it more importantly raises up experience to be primary

126
00:11:11,160 --> 00:11:17,560
Experience comes first and it's not it's not an argument about which one's right and what's wrong

127
00:11:17,560 --> 00:11:19,800
It's a much more an argument of which one's better and

128
00:11:21,320 --> 00:11:23,960
When you ask the question of something being better

129
00:11:25,160 --> 00:11:27,160
It's always in regards to

130
00:11:27,160 --> 00:11:30,840
Well better for whom or better for what better in relation to what?

131
00:11:32,680 --> 00:11:34,680
So because

132
00:11:34,680 --> 00:11:39,440
Obviously for us the most important thing is ask you ask which one is better for us and

133
00:11:41,080 --> 00:11:44,760
So of course, it's us. It's the one that relates to our experience

134
00:11:44,760 --> 00:11:48,440
Depression anxiety

135
00:11:50,200 --> 00:11:52,200
Addiction

136
00:11:52,760 --> 00:11:56,120
All the problems in life the problems in life are not about

137
00:11:58,120 --> 00:12:01,880
External entities the problems in life are about experience

138
00:12:01,880 --> 00:12:15,880
And so our whole way of looking at things changes with what we've best in the means it means to change

139
00:12:16,920 --> 00:12:18,920
what we put as primary

140
00:12:19,880 --> 00:12:24,520
Things people places and so on. Yes, we acknowledge them and that's why practical to

141
00:12:25,320 --> 00:12:27,320
acknowledge their reality

142
00:12:28,040 --> 00:12:30,040
But it's far less practical

143
00:12:30,040 --> 00:12:33,720
when compare it's a part of that's practical than

144
00:12:36,440 --> 00:12:39,080
Experience the understanding of experience

145
00:12:40,680 --> 00:12:42,680
Because experience is not

146
00:12:43,480 --> 00:12:45,480
stable

147
00:12:46,440 --> 00:12:51,320
This question the question about the three characteristics. It's it's quite

148
00:12:52,040 --> 00:12:54,040
puzzling

149
00:12:54,040 --> 00:12:59,320
Because we're thinking in terms of things right saying so what does it mean to say something is unstable?

150
00:12:59,320 --> 00:13:03,720
That's not what it means it means experiences and so it's actually quite obvious

151
00:13:05,880 --> 00:13:11,240
Is an experienced stable or unstable? Well, no, it arises and it ceases quite quickly

152
00:13:13,000 --> 00:13:15,800
That's quite obvious. It's not something mystical or magical

153
00:13:17,640 --> 00:13:22,120
What we pass in a means it means shifting from this one way of looking things to another

154
00:13:23,240 --> 00:13:25,240
And what that does

155
00:13:25,240 --> 00:13:29,880
Is it changes the way you look at reality reality is no longer something that is fixed

156
00:13:31,480 --> 00:13:37,560
As a result, it's no longer full of anything that can satisfy because the things are now experiences which

157
00:13:38,440 --> 00:13:40,440
Arise and cease

158
00:13:40,440 --> 00:13:42,440
We stop being addicted because we stop

159
00:13:45,400 --> 00:13:47,080
Seeing

160
00:13:47,080 --> 00:13:50,840
Things that might satisfy we stop seeing in terms of

161
00:13:50,840 --> 00:14:00,920
Certain in terms of things which would have the potential in our minds to satisfy us if that was if it weren't for this pesky

162
00:14:02,440 --> 00:14:04,440
Need to experience them, right

163
00:14:05,800 --> 00:14:07,800
Because experience is required

164
00:14:08,280 --> 00:14:11,000
For enjoying all those things and experience

165
00:14:12,440 --> 00:14:16,360
Therefore being primary and experience being unstable

166
00:14:16,360 --> 00:14:20,360
They can satisfy us

167
00:14:20,840 --> 00:14:22,840
They aren't what would we

168
00:14:23,400 --> 00:14:29,720
Experience we experience experience we experience seeing hearing smelling tasting feeling thinking

169
00:14:33,160 --> 00:14:38,040
They're not under our control. They don't belong to us experiences don't have that quality

170
00:14:38,120 --> 00:14:44,200
It's not something hard to understand all these questions about self and non-self does the self exist

171
00:14:44,200 --> 00:14:46,200
It's totally

172
00:14:46,920 --> 00:14:51,880
Off-based questions. They're questions that are based on thinking about this old way of looking at things

173
00:14:53,800 --> 00:14:55,800
Non-self

174
00:14:55,800 --> 00:15:02,280
And simply means experiences aren't don't belong to you. They aren't you. They aren't you. They aren't a you they aren't a thing an entity

175
00:15:03,960 --> 00:15:05,960
They're an experience

176
00:15:05,960 --> 00:15:07,880
Rises and ceases

177
00:15:07,880 --> 00:15:09,480
So it's not about

178
00:15:09,480 --> 00:15:16,200
Somehow realizing that the things you thought were permanent are impermanent. I mean that's one way of in a very

179
00:15:20,440 --> 00:15:26,520
Introductory a way of explaining it, but it's much deeper than that or it's it's different than that

180
00:15:27,560 --> 00:15:33,320
These three characteristics shouldn't be hard to understand what it means is you're going to change the way you look at reality

181
00:15:33,320 --> 00:15:38,200
From from things things that are

182
00:15:38,920 --> 00:15:43,480
Stable satisfying and controllable to which are people in places and things

183
00:15:44,760 --> 00:15:49,720
To seeing in terms of experiences which are impermanent unsatisfying and uncontrollable

184
00:15:51,720 --> 00:15:55,640
I mean it sounds like a bum rat a bad deal, right? It's like wait a minute

185
00:15:55,640 --> 00:16:01,880
Why would I want to do that when shouldn't I stick to this way of looking at things that sees thing?

186
00:16:03,800 --> 00:16:06,120
But it's the that's the problem right is that

187
00:16:08,200 --> 00:16:10,680
Those things are dependent on experience

188
00:16:11,960 --> 00:16:13,720
It's good for

189
00:16:13,720 --> 00:16:18,520
Society if you do that it means we can build nuclear reactors and

190
00:16:19,480 --> 00:16:21,480
Space stations and computers

191
00:16:21,480 --> 00:16:25,320
And now they're getting into quantum computers and so on

192
00:16:27,720 --> 00:16:31,800
It doesn't have any bearing on our happiness or our unhappiness

193
00:16:33,800 --> 00:16:35,640
Because in terms of what's good for us

194
00:16:37,160 --> 00:16:41,560
Our experience is primary and when you put experience as primary

195
00:16:43,640 --> 00:16:45,640
You fall into the trouble

196
00:16:45,640 --> 00:16:52,920
Of the fact that an experience doesn't lend itself to clinging

197
00:16:54,040 --> 00:16:56,040
It's kind of a trouble

198
00:16:56,760 --> 00:17:00,920
But because it's primary it doesn't matter whether you do or you don't it is that way

199
00:17:01,960 --> 00:17:06,760
Either way if you live in the world of concepts things people places and things

200
00:17:07,560 --> 00:17:10,440
You're going to get burned because you're depending on

201
00:17:10,440 --> 00:17:17,000
This reality that you're ignoring of impermanence suffering and on so

202
00:17:18,120 --> 00:17:22,440
If instead you see things through the lens of of experience

203
00:17:24,760 --> 00:17:27,160
You have no disappointment you have no upset

204
00:17:28,360 --> 00:17:32,840
You're able it's it's like really living with life keeping up with life

205
00:17:34,360 --> 00:17:36,760
Instead of fighting it instead of

206
00:17:36,760 --> 00:17:43,560
Living in fear of it. What's going to come next? Well, I know it's going to come next more experience

207
00:17:44,920 --> 00:17:50,520
You suddenly this this cliche phrase of the whole world is suddenly your oyster

208
00:17:50,520 --> 00:17:54,600
It's like suddenly you fit with with the world. They came with with reality

209
00:17:55,800 --> 00:17:58,600
It become natural you become in tune

210
00:17:59,960 --> 00:18:01,960
With reality

211
00:18:01,960 --> 00:18:09,080
It's a very apt way of putting it. I think in tune with reality. There's no dissonance. There's no

212
00:18:10,040 --> 00:18:12,040
worry about the future or

213
00:18:12,600 --> 00:18:14,600
sadness about the past

214
00:18:15,320 --> 00:18:18,360
Which is all caught up in concepts things and you know

215
00:18:19,960 --> 00:18:21,960
me my

216
00:18:22,840 --> 00:18:24,840
Experience is always only present

217
00:18:26,520 --> 00:18:28,520
Here and now

218
00:18:28,520 --> 00:18:30,520
born and dies

219
00:18:30,520 --> 00:18:35,080
So our practice of mindfulness when we practice the four foundations of mindfulness

220
00:18:36,440 --> 00:18:40,600
Ment to help us see this to help us look at things look at reality

221
00:18:41,640 --> 00:18:43,640
From a point of view of experience

222
00:18:45,560 --> 00:18:48,280
Physical experience when you walk stepping right

223
00:18:49,000 --> 00:18:51,880
There's an experience there where there's many experiences, but

224
00:18:52,520 --> 00:18:58,040
Simply put there's it's experience of walking with the right foot and then the left foot is another experience

225
00:18:58,040 --> 00:19:04,760
When you sit and you say rising falling this is the physical experiences when you feel pain

226
00:19:06,040 --> 00:19:09,960
And you focus on the pain and say pain you start to see it as experiences

227
00:19:11,960 --> 00:19:13,960
Many moments of experience

228
00:19:15,800 --> 00:19:18,840
Thinking about the past or future you start to see them not as

229
00:19:19,800 --> 00:19:23,960
My past my future you see them as experiences of thinking

230
00:19:25,480 --> 00:19:27,480
Thinking about the past

231
00:19:27,480 --> 00:19:30,120
Thinking about the future it's just a experience of thinking

232
00:19:34,840 --> 00:19:39,160
Emotions when you like something dislike something or other mind states like

233
00:19:39,880 --> 00:19:42,360
drowsiness or distraction or worry or

234
00:19:43,240 --> 00:19:45,240
Anxiety or doubt

235
00:19:47,480 --> 00:19:53,240
Instead of being tortured by these things or ruled by them and have to go take drugs and medication

236
00:19:53,240 --> 00:20:00,680
It's really kind of a silly thing to take. I mean I understand that when it's very extreme and people are not able to be mindful

237
00:20:00,680 --> 00:20:02,680
they take medication for it, but

238
00:20:06,680 --> 00:20:08,280
I mean

239
00:20:08,280 --> 00:20:10,280
It's sad that it comes to that because

240
00:20:11,160 --> 00:20:19,560
They're just emotions and when you if you can and eventually can see them just as experience as it doesn't matter how intense or

241
00:20:20,520 --> 00:20:22,520
Overwhelming they are

242
00:20:22,520 --> 00:20:24,520
They just are their experiences

243
00:20:26,120 --> 00:20:30,280
Of course, it's it's I don't mean to trivialize mental illness, but

244
00:20:32,200 --> 00:20:36,840
It's it is trivial. It's just that we are so

245
00:20:38,840 --> 00:20:40,840
We are so accustomed

246
00:20:41,880 --> 00:20:43,880
to reacting

247
00:20:44,440 --> 00:20:49,240
We're accustomed to thinking of emotions as my emotions as me

248
00:20:49,240 --> 00:20:54,120
Experiencing emotions and even as the emotions as being a thing like

249
00:20:54,920 --> 00:20:56,920
People say I have depression

250
00:20:57,480 --> 00:20:59,960
I am depressed. I am clinically depressed, but

251
00:21:00,920 --> 00:21:05,640
Depression is an emotion that comes it comes for a moment and then it's gone and then it comes again

252
00:21:07,320 --> 00:21:12,680
It's only when we cling to it and when we see it as a thing as an entity that we suffer

253
00:21:12,680 --> 00:21:16,680
I

254
00:21:17,000 --> 00:21:23,000
Same with anxiety and so on and the sense is seeing hearing smelling tasting feeling thinking

255
00:21:23,960 --> 00:21:28,520
How they can torture us which is kind of silly because seeing is really just seeing

256
00:21:29,320 --> 00:21:35,800
How is it that when you see a spider you freak out or you see something ugly you get nauseous or so on

257
00:21:37,000 --> 00:21:39,000
It's just seeing

258
00:21:39,000 --> 00:21:48,240
We're conditioned we condition ourselves and we're encouraged by others too by external things to become conditioned

259
00:21:53,880 --> 00:21:56,120
Buddhism is about becoming unconditioned

260
00:22:00,840 --> 00:22:02,840
Objective

261
00:22:03,160 --> 00:22:04,840
Seeing

262
00:22:04,840 --> 00:22:06,840
Becoming in tune with reality

263
00:22:06,840 --> 00:22:10,600
So those are the two ways of seeing

264
00:22:12,040 --> 00:22:18,680
A little bit about that concept and more importantly about insight meditation and mindfulness, which are

265
00:22:20,040 --> 00:22:25,800
Insight and mindfulness. These are the two most important concepts in our practice

266
00:22:26,760 --> 00:22:28,760
We're practicing mindfulness to gain

267
00:22:29,320 --> 00:22:31,320
Repassana which means insight or

268
00:22:31,320 --> 00:22:35,720
Clear sight or seeing things in a special way

269
00:22:35,720 --> 00:23:02,680
So that's the dumb of her tonight. Thank you all for coming out

