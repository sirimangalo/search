1
00:00:00,000 --> 00:00:07,000
Nothing to note, what should one do, and there is nothing to note?

2
00:00:07,000 --> 00:00:13,280
It is common to encounter states of quiet, where there is no awareness of the abdomen

3
00:00:13,280 --> 00:00:19,320
on any other experience besides the sense of calm, quiet, or emptiness.

4
00:00:19,320 --> 00:00:24,920
Although these states may seem to be outside of ordinary meditation practice, they are

5
00:00:24,920 --> 00:00:31,800
no different from any other phenomenon, and should therefore be noted in the same way,

6
00:00:31,800 --> 00:00:40,120
noting quiet, quiet, calm, or empty, empty until they disappear.

7
00:00:40,120 --> 00:00:46,240
If there is any liking or wanting in regard to such states, one should note that is

8
00:00:46,240 --> 00:00:53,240
liking, liking, or wanting, wanting.

