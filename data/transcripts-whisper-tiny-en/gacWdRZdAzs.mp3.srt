1
00:00:00,000 --> 00:00:09,000
I have been practicing mindfulness without mental notice and focusing on the sensations, thoughts, etc.

2
00:00:09,000 --> 00:00:12,000
For me, it's easier to be mindful this way.

3
00:00:12,000 --> 00:00:15,000
What's your opinion on this?

4
00:00:17,000 --> 00:00:20,000
How many times do I have to answer this one though?

5
00:00:20,000 --> 00:00:29,000
Nobody wants to put words in your head, right?

6
00:00:29,000 --> 00:00:31,000
Why?

7
00:00:31,000 --> 00:00:32,000
Why?

8
00:00:32,000 --> 00:00:41,000
Because it's difficult.

9
00:00:41,000 --> 00:00:47,000
All I can say is you have to ask yourself, what is the purpose of meditation?

10
00:00:47,000 --> 00:00:52,000
Is the purpose of meditation to make things easy?

11
00:00:52,000 --> 00:00:58,000
Or is the purpose in meditation to overcome what is difficult?

12
00:00:58,000 --> 00:01:07,000
The reason why, from my point of view, people ask this, or people make this observation that it's

13
00:01:07,000 --> 00:01:24,000
more difficult to what I call, and my teachers call recognizing the object for what it is in terms of using what we might understand to be as being a mantra.

14
00:01:24,000 --> 00:01:28,000
Is because it forces you to see impermanence, suffering in non-cell.

15
00:01:28,000 --> 00:01:32,000
It forces you to see that things are not as you would like them.

16
00:01:32,000 --> 00:01:39,000
It forces you to see your mind's inclination to control things without being able to control them.

17
00:01:39,000 --> 00:01:53,000
Because of the regimented nature of the practice, you're not able to have this continuous, what you would call mindfulness.

18
00:01:53,000 --> 00:01:57,000
And what I would just call concentration.

19
00:01:57,000 --> 00:02:03,000
The word mindfulness is a bit meaningless because it's not even a really good translation of the word sati.

20
00:02:03,000 --> 00:02:15,000
I just finished redoing this talk I gave on the comprehensive practice.

21
00:02:15,000 --> 00:02:26,000
I totally redo it because the video talk was not very good, but I had one part, particularly where I finally put down and writing some of my thoughts on this that

22
00:02:26,000 --> 00:02:29,000
mindfulness is a poor translation for the word sati.

23
00:02:29,000 --> 00:02:35,000
The word sati means specifically the recognition of something as it is.

24
00:02:35,000 --> 00:02:41,000
It's caused by something called Tirasanya, which is sunya, the recognition of something.

25
00:02:41,000 --> 00:02:49,000
This is that is Tirah, which means firm, strong, fortified.

26
00:02:49,000 --> 00:02:56,000
So the word is fortifying the recognition of the object for what it is.

27
00:02:56,000 --> 00:03:00,000
That's the meaning of the word sati and that's why we use the noting.

28
00:03:00,000 --> 00:03:03,000
The problem is it's not how we would like it.

29
00:03:03,000 --> 00:03:17,000
This breaks up the continuity and breaks up the inclinations of the mind, the stream of habitual clinging to the ideas of self

30
00:03:17,000 --> 00:03:22,000
to concede, to craving.

31
00:03:22,000 --> 00:03:39,000
The whole idea that something is more easy for me not surely implies the preference and possibly even a identification with the experience that it's me meditating.

32
00:03:39,000 --> 00:03:42,000
It's me being mindful.

33
00:03:42,000 --> 00:03:52,000
This is all broken up through the practice of noting and as a result one feels like one is forcing the experience.

34
00:03:52,000 --> 00:04:07,000
One feels like the experience is jarring and chaotic, jarring, chaotic and uncontrollable, which it is, which is the actual reality of it.

35
00:04:07,000 --> 00:04:12,000
And this sort of practice is forcing you to see it.

36
00:04:12,000 --> 00:04:17,000
It's not the noting itself that is uncomfortable.

37
00:04:17,000 --> 00:04:25,000
It's the discord between the way we would like things to be and the way things really are.

38
00:04:25,000 --> 00:04:35,000
Once you can come to see things as they are, which is moment to moment experiences that don't carry on from one moment to the next,

39
00:04:35,000 --> 00:04:38,000
then noting becomes a very easy thing to do.

40
00:04:38,000 --> 00:04:50,000
Catching things up in your attention one by one by one becomes natural because you're finally able to see and accept that that's the way things are that there are no continuous entities.

41
00:04:50,000 --> 00:04:54,000
There are only moment to moment realities until that time.

42
00:04:54,000 --> 00:05:01,000
What you're expecting to find is something that continues for a moment to moment to moment and because the mind doesn't get that,

43
00:05:01,000 --> 00:05:14,000
it becomes quite distraught and disturbed by what is actually a simple task of just reminding yourself of the nature of the object.

44
00:05:14,000 --> 00:05:32,000
And instead of doing that, it controls, it forces, and it feels like this sort of practice is causing you to force, which actually it's not.

45
00:05:32,000 --> 00:05:44,000
It's the word mindful actually means to remind yourself, the words not mindful that we use or mindfulness is not what's important in Satyipatana practice.

46
00:05:44,000 --> 00:05:51,000
It's recognition and remembrance and reminding yourself of the object for what it is as the Buddha said,

47
00:05:51,000 --> 00:06:08,000
Chantalwagachamiti bhajamiti, when walking one nose, I am walking kachamit, which is just a poly word that means walking in the sense that it's me walking, but without emphasizing the eye or the me.

48
00:06:08,000 --> 00:06:21,000
So it really is the simplest way to focus one's attention on the object. It's also a very traditional way. The whole idea of a mantra, the reason I use the word mantras, because actually it's a word that we already know.

49
00:06:21,000 --> 00:06:24,000
It's a word that we should be very familiar with.

50
00:06:24,000 --> 00:06:38,000
And the fact that using the mantra on things that are impermanence suffering a non-self makes forces you to experience impermanence suffering a non-self is a sign that it's working.

51
00:06:38,000 --> 00:06:46,000
The fact that it's uncomfortable is a sign that it's forcing you out of your comfort zone, enforcing you to look at things in a new way.

52
00:06:46,000 --> 00:06:59,000
And from our point of view, forcing you to look at things objectively. Now, of course, people disagree with this. And there's many people who had listened to me and just doesn't know what he's talking about.

53
00:06:59,000 --> 00:07:05,000
But to each their own and you've asked me, so that's my answer. Hopefully it helps.

54
00:07:05,000 --> 00:07:28,000
So if you're interested in this practice, I can only recommend that you go back to the actual noting of the objects. I think prefer something more peaceful, calming and soothing than you have to look elsewhere.

55
00:07:28,000 --> 00:07:42,000
Thank you.

