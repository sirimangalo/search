1
00:00:00,000 --> 00:00:07,000
Welcome everyone to a special edition of Monk Radio here in Thailand.

2
00:00:07,000 --> 00:00:14,000
It doesn't look like much, but we're in my uncle's office.

3
00:00:14,000 --> 00:00:23,000
And it's actually moving right now so everything's all a bit disorganized.

4
00:00:23,000 --> 00:00:30,000
I managed to sit here and see lighting.

5
00:00:30,000 --> 00:00:38,000
I'm going to get here and wet canvas on and put together a special session here.

6
00:00:38,000 --> 00:00:44,000
So, what we're going to do tonight, we'll probably not take so much time.

7
00:00:44,000 --> 00:00:47,000
Appreciate that some people have come up.

8
00:00:47,000 --> 00:00:54,000
How much is going to take this opportunity to talk a little bit about what we've been through.

9
00:00:54,000 --> 00:00:58,000
What we've done in the past couple of weeks.

10
00:00:58,000 --> 00:01:04,000
And what we're doing and where we're going the next little while.

11
00:01:04,000 --> 00:01:10,000
Maybe answer a few questions if people have any pertinent questions.

12
00:01:10,000 --> 00:01:15,000
Maybe do a little bit of group meditation.

13
00:01:15,000 --> 00:01:19,000
So, first things first, let's go through announcements.

14
00:01:19,000 --> 00:01:28,000
The first announcement is that we're in Thailand now.

15
00:01:28,000 --> 00:01:40,000
And we've been spending the past ten days with my teacher.

16
00:01:40,000 --> 00:01:45,000
With our teacher, I'm head teacher at Chantong.

17
00:01:45,000 --> 00:01:49,000
We're doing a meditation course with him.

18
00:01:49,000 --> 00:01:53,000
So, far less work with the through.

19
00:01:53,000 --> 00:01:56,000
A ten-day advanced course.

20
00:01:56,000 --> 00:02:04,000
A review course with Chantong.

21
00:02:04,000 --> 00:02:11,000
And it was really quite special actually.

22
00:02:11,000 --> 00:02:15,000
I contacted him earlier to make sure that they had room for us.

23
00:02:15,000 --> 00:02:18,000
And it was my birthday on May 9th.

24
00:02:18,000 --> 00:02:23,000
So, we arrived on the 8th in the evening in Chantong.

25
00:02:23,000 --> 00:02:27,000
And in terms of they didn't have room for us.

26
00:02:27,000 --> 00:02:32,000
And Adagentong wasn't doing any reporting.

27
00:02:32,000 --> 00:02:36,000
So, we went to see Adagentong.

28
00:02:36,000 --> 00:02:37,000
Oh, no.

29
00:02:37,000 --> 00:02:43,000
We managed to get almost enough rooms.

30
00:02:43,000 --> 00:02:45,000
And then we went to see Adagentong.

31
00:02:45,000 --> 00:02:53,000
And in terms of you, he had stopped reporting because he had been sick and recovering from me.

32
00:02:53,000 --> 00:02:58,000
From a condition called bladder stones.

33
00:02:58,000 --> 00:03:03,000
I don't know if you're following.

34
00:03:03,000 --> 00:03:09,000
As he wasn't doing a reporting, but he agreed to do a special, to give us a special exception.

35
00:03:09,000 --> 00:03:12,000
And to meet with us once a day.

36
00:03:12,000 --> 00:03:19,000
And so, we actually got to go to one-on-one practice with Adagentong.

37
00:03:19,000 --> 00:03:22,000
And finally, we got enough rooms.

38
00:03:22,000 --> 00:03:29,000
So, it was great.

39
00:03:29,000 --> 00:03:32,000
From my point of view, it was a great experience with us.

40
00:03:32,000 --> 00:03:38,000
Then there's not just for myself, but for all of us, to have this opportunity to spend time to hear his words.

41
00:03:38,000 --> 00:03:43,000
And it was through the translation for most people.

42
00:03:43,000 --> 00:03:50,000
Things are real opportunity to learn some new things.

43
00:03:50,000 --> 00:03:53,000
We get ourselves focused back on the practice for myself.

44
00:03:53,000 --> 00:03:56,000
The biggest problem of the question was at the past month.

45
00:03:56,000 --> 00:03:59,000
It's been quite hectic with our donations.

46
00:03:59,000 --> 00:04:05,000
The next announcement should have been here for the first and last year.

47
00:04:05,000 --> 00:04:13,000
Our donations with visas, with building with everything that's been going on in the past month.

48
00:04:13,000 --> 00:04:16,000
It was quite...

49
00:04:16,000 --> 00:04:32,000
On the one-on-one, it was difficult to get back into it, but on the other end, it was comforting to come back to something that makes more sense and is much more pleasant or more...

50
00:04:32,000 --> 00:04:36,000
Much more conducive of leading towards happiness.

51
00:04:36,000 --> 00:04:37,000
And peace.

52
00:04:37,000 --> 00:04:40,000
And there's some other things that we can talk to.

53
00:04:40,000 --> 00:05:00,000
So it was quite a reminder and something that really for myself to stabilize things, refocus back on the practice.

54
00:05:00,000 --> 00:05:07,000
The next announcement is that we have welcomed to our two new monastics.

55
00:05:07,000 --> 00:05:13,000
Many of you have been following in our awareness that they were planning to ordain while they haven't been.

56
00:05:13,000 --> 00:05:16,000
Some of you even see the origination online.

57
00:05:16,000 --> 00:05:21,000
And so now our milk radio has multiplied.

58
00:05:21,000 --> 00:05:30,000
It's doubled for four monastics.

59
00:05:30,000 --> 00:05:40,000
So yes, as ordained as a salmonella rep, and suvella suvella suvella suvella suvella, the same word it just means, novice.

60
00:05:40,000 --> 00:05:46,000
Maybe it's one who is dependent on the beak of sin and the beak of sin.

61
00:05:46,000 --> 00:05:54,000
Is as undertaken to cultivate the practice of the beak of sin.

62
00:05:54,000 --> 00:06:08,000
So there are novices to eventually we'll get monks together to do higher donations if they survive that one.

63
00:06:08,000 --> 00:06:13,000
That's it.

64
00:06:13,000 --> 00:06:21,000
Oh, and the final announcement for me is that today, so after this 10-day course,

65
00:06:21,000 --> 00:06:27,000
we came into Chiang Mai to do some parents or to meet with people.

66
00:06:27,000 --> 00:06:33,000
And I kind of got the idea to go back to this place I had been in north of Chiang Mai,

67
00:06:33,000 --> 00:06:34,000
this forest monastery.

68
00:06:34,000 --> 00:06:37,000
Let me try to start a forest invitation sometime tomorrow.

69
00:06:37,000 --> 00:06:40,000
Yes, the first came to practice.

70
00:06:40,000 --> 00:06:43,000
And we were involved in our day.

71
00:06:43,000 --> 00:06:44,000
That's an H.E.

72
00:06:44,000 --> 00:06:45,000
Almost.

73
00:06:45,000 --> 00:06:46,000
Right.

74
00:06:46,000 --> 00:06:49,000
That's the place where we were dealing with the papers.

75
00:06:49,000 --> 00:06:51,000
And we got back there.

76
00:06:51,000 --> 00:06:54,000
And of course, we had problems with the head monk as in general.

77
00:06:54,000 --> 00:06:55,000
We had monks.

78
00:06:55,000 --> 00:06:59,000
And it turns out that there's a new monk there.

79
00:06:59,000 --> 00:07:02,000
There's a really great monk and really wants help.

80
00:07:02,000 --> 00:07:09,000
So the short story is that in the future we might be doing courses in Thailand again.

81
00:07:09,000 --> 00:07:14,000
So there might be an opportunity to do some practice here again.

82
00:07:14,000 --> 00:07:20,000
So I might be back to Thailand sometime in the future.

83
00:07:20,000 --> 00:07:22,000
So that's all my notes.

84
00:07:22,000 --> 00:07:27,000
Is there anything else that you would think of as to be said?

85
00:07:27,000 --> 00:07:29,000
Well, we're leaving tomorrow.

86
00:07:29,000 --> 00:07:31,000
Tomorrow we're going to Bangkok.

87
00:07:31,000 --> 00:07:36,000
Today, tomorrow, today we went to this forest monastery.

88
00:07:36,000 --> 00:07:39,000
Tomorrow we're going to July 2, 10.

89
00:07:39,000 --> 00:07:40,000
Which is the mountain of H.E.

90
00:07:40,000 --> 00:07:44,000
And then the evening we're leaving from Bangkok and we're going to do some.

91
00:07:44,000 --> 00:07:46,000
We're going to be getting visas.

92
00:07:46,000 --> 00:07:50,000
And we're going to do a little bit of teaching in Bangkok.

93
00:07:50,000 --> 00:07:52,000
And then back to the shore.

94
00:07:52,000 --> 00:08:01,000
Well, I will stay here for a week more so almost two weeks.

95
00:08:01,000 --> 00:08:12,000
Until we finally go back to Sri Lanka on 31st May.

96
00:08:12,000 --> 00:08:15,000
Okay, so that's our notes.

97
00:08:15,000 --> 00:08:16,000
That's our trip.

98
00:08:16,000 --> 00:08:18,000
The trip has been great so far.

99
00:08:18,000 --> 00:08:39,000
For the next video, I'd like to have everyone say something about practice.

