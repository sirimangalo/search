1
00:00:00,000 --> 00:00:15,000
The Dhamatok, delivered by a valuable Uxilananda at the Tagata Media Tisan Center, San Jose,

2
00:00:15,000 --> 00:00:27,000
California, Special Retreat of November, 1998.

3
00:00:27,000 --> 00:00:39,000
Suppose you have to pick up a package about 70 pounds in weight,

4
00:00:39,000 --> 00:00:48,000
and you are made to carry that package on your back and go a long journey.

5
00:00:48,000 --> 00:00:58,000
So with every step, the package seems to be heavier.

6
00:00:58,000 --> 00:01:09,000
And at the end of the journey, you are able to throw down that package.

7
00:01:09,000 --> 00:01:15,000
Now, when you have to carry the package on the back, you are suffering.

8
00:01:15,000 --> 00:01:27,000
You are in Dukha, and the Dukha is caused by picking up that package.

9
00:01:27,000 --> 00:01:36,000
And when you throw down, you feel relieved, and you are glad.

10
00:01:36,000 --> 00:01:51,000
So throwing down is actually the cause of your relief or your gladness.

11
00:01:51,000 --> 00:02:05,000
Now, this similarly, we should understand in connection with a discourse given by the Buddha.

12
00:02:05,000 --> 00:02:12,000
And that discourse is found in the San Yuta Nikaya.

13
00:02:12,000 --> 00:02:19,000
It is a very short soda. It's about about a page.

14
00:02:19,000 --> 00:02:27,000
Although it is a short soda, it contains many things to understand.

15
00:02:27,000 --> 00:02:44,000
And so we can expand it into many pages, giving explanations to those that are taught in this discourse.

16
00:02:44,000 --> 00:02:57,000
Expect this discourse to begin with the usual opening passage, a one-way-suit on, and thus have a heart.

17
00:02:57,000 --> 00:03:03,000
But we don't find those words at the beginning of this discourse.

18
00:03:03,000 --> 00:03:16,000
We just find the words near sawati, and then just that.

19
00:03:16,000 --> 00:03:28,000
I think the elders at the first Buddhist council who recorded these sodas were very patient.

20
00:03:28,000 --> 00:03:38,000
Now, they had to put this passage at the beginning of every discourse.

21
00:03:38,000 --> 00:03:50,000
But I think even there is a limit to the patience of these arachts.

22
00:03:50,000 --> 00:04:05,000
Or, more probably, it was not the elders at the first Buddhist council, but those who wrote them down on the palm leaves at that lost patience.

23
00:04:05,000 --> 00:04:19,000
So, in this collection, the collection of the kindred sayings, at the beginning of many sodas, they just put the words in sawati, in sawati, and so on.

24
00:04:19,000 --> 00:04:42,000
But whenever we see the word in sawati and so on, we must understand that this means that this discourse was given by the Buddha when he was staying in the data on a monastery and they are not happy because Park near the city of sawati, and there he addressed the monks and so on.

25
00:04:42,000 --> 00:04:58,000
So, there is like the one we studied the Dhamma heritage soda, because there was nobody who requested the Buddha to give this talk.

26
00:04:58,000 --> 00:05:10,000
So, he just went to the assembly hall or Dhamma hall, and he called the monks, and then he just delivered this soda.

27
00:05:10,000 --> 00:05:17,000
The name of this discourse is Bha-ra-suta.

28
00:05:17,000 --> 00:05:35,000
I will tell you the burden, the carrier of the burden, the taking up of the burden and laying down of the burden.

29
00:05:35,000 --> 00:05:40,000
Listen to it, fake or detention.

30
00:05:40,000 --> 00:05:46,000
And then Buddha said, what is the burden?

31
00:05:46,000 --> 00:05:51,000
The five aggregates of clinging should be the answer.

32
00:05:51,000 --> 00:06:06,000
So, the Buddha said, the five aggregates of clinging were burden.

33
00:06:06,000 --> 00:06:11,000
Now, in this term, five aggregates of clinging, we have to understand three things here.

34
00:06:11,000 --> 00:06:18,000
The five, the aggregates and clinging.

35
00:06:18,000 --> 00:06:25,000
So, first we would understand the aggregate.

36
00:06:25,000 --> 00:06:40,000
Now, when Buddha analyzed the whole world, the whole world, including the beings, he analyzed them into five aggregates, five groups.

37
00:06:40,000 --> 00:07:00,000
And these five groups are these five aggregates aggregate of matter, aggregate of feeling, aggregate of perception, aggregate of mental formations and aggregate of consciousness.

38
00:07:00,000 --> 00:07:16,000
Aggregate of matter means the group of material properties, beginning with the four essential elements, the element of the water, fire and air and so on.

39
00:07:16,000 --> 00:07:31,000
This group of actually 28 properties of matter is called matter aggregate.

40
00:07:31,000 --> 00:07:46,000
So, the house is a matter aggregate, the rug is a matter aggregate, anything that is inanimate is composed of matter aggregate.

41
00:07:46,000 --> 00:07:50,000
The one is feeling aggregate, that means just feeling.

42
00:07:50,000 --> 00:08:01,000
The feeling is of three kinds or five kinds, three kinds are pleasant feeling, unpleasant feeling and neutral feeling.

43
00:08:01,000 --> 00:08:11,000
Pleasant feeling is again two fold, pleasant feeling connected with the body and pleasant feeling in the mind.

44
00:08:11,000 --> 00:08:19,000
So, two pleasant feelings and two unpleasant feelings, one connected with the body and the other in the mind.

45
00:08:19,000 --> 00:08:25,000
But neutral feeling is said to be only one, mental.

46
00:08:25,000 --> 00:08:36,000
So, there are first three kinds of feelings and they can be divided into five kinds of feelings.

47
00:08:36,000 --> 00:08:42,000
And feeling is the experience of the object.

48
00:08:42,000 --> 00:08:53,000
Now, there is pain and we feel that pain with this feeling, aggregate or this mental state.

49
00:08:53,000 --> 00:09:04,000
So, feeling is mental, not material, but aggregate is the aggregate of perception.

50
00:09:04,000 --> 00:09:13,000
And perception means a mental state that makes marks of the objects.

51
00:09:13,000 --> 00:09:24,000
So, whenever we experience an object, that mental state arises and makes a mark of that object.

52
00:09:24,000 --> 00:09:37,000
And that is why we recognize it when we bring to our mind later or when we experience it later.

53
00:09:37,000 --> 00:09:45,000
So, that making mark is what is translated as perception here.

54
00:09:45,000 --> 00:09:51,000
So, aggregate is the aggregate of mental formations.

55
00:09:51,000 --> 00:10:02,000
Now, there are mental states that are called J. J. the Seagulls in Bali.

56
00:10:02,000 --> 00:10:13,000
And it is the what is translated as mental concomitant or mental factors are in Bali.

57
00:10:13,000 --> 00:10:27,000
Because they are headed by a mental state, the volition, which is called gamma and which is also called Sankara.

58
00:10:27,000 --> 00:10:42,000
So, Sankara aggregate means the aggregate of mental states headed by fallition or headed by gamma or headed by Sankara.

59
00:10:42,000 --> 00:10:49,000
Aggregate is aggregate of consciousness.

60
00:10:49,000 --> 00:10:56,000
Here also, consciousness means the awareness of the object.

61
00:10:56,000 --> 00:11:03,000
And here awareness does not mean awareness we use in meditation.

62
00:11:03,000 --> 00:11:08,000
Because awareness we use in meditation is actually mindfulness.

63
00:11:08,000 --> 00:11:17,000
Now, here regarding consciousness awareness means just a mere awareness of the object.

64
00:11:17,000 --> 00:11:32,000
So, that mere awareness of the object is something like the connection with the object is what is called Vinyana or Chita and Pali.

65
00:11:32,000 --> 00:11:43,000
So, these are five aggregates in brief and among them the first one belongs to Meta or it is Meta and the other four are mental.

66
00:11:43,000 --> 00:11:52,000
So, one material aggregate and four mental aggregates all together we have five aggregates.

67
00:11:52,000 --> 00:11:59,000
And beings are made up of these five aggregates.

68
00:11:59,000 --> 00:12:05,000
Now, human beings are made made of five aggregates, animals are made of five aggregates.

69
00:12:05,000 --> 00:12:12,000
The celestial beings are also made of five aggregates.

70
00:12:12,000 --> 00:12:26,000
But there are some Brahamas who have only one aggregate the first aggregate or who have only four aggregates.

71
00:12:26,000 --> 00:12:30,000
The second, third, fourth and fifth.

72
00:12:30,000 --> 00:12:38,000
But mostly beings are made of five aggregates.

73
00:12:38,000 --> 00:12:46,000
Among them, the Meta aggregate is said to be always mundane.

74
00:12:46,000 --> 00:12:50,000
So, it belongs to mundane level, the Meta aggregate.

75
00:12:50,000 --> 00:13:05,000
But the other four aggregates, the mental aggregates belong to both mundane and super mundane levels.

76
00:13:05,000 --> 00:13:27,000
So, among these five aggregates, we must understand that one is mundane and therefore the other four are super mundane.

77
00:13:27,000 --> 00:13:32,000
And among these, the other four are both mundane and super mundane.

78
00:13:32,000 --> 00:13:39,000
So, among these four, the last one is said to be the chief.

79
00:13:39,000 --> 00:13:45,000
The consciousness is said to be the chief of other mental states.

80
00:13:45,000 --> 00:14:01,000
So, consciousness is a chief and a feeling perception and mental formations aggregate are the followers of the fifth aggregate.

81
00:14:01,000 --> 00:14:17,000
That means the three aggregates, feeling perception and mental formations always arise simultaneously with the fifth aggregate.

82
00:14:17,000 --> 00:14:24,000
That arise at the moment of enlightenment are called super mundane.

83
00:14:24,000 --> 00:14:29,000
And those that arise ordinarily are called mundane.

84
00:14:29,000 --> 00:14:43,000
So, the four aggregates, feeling perception, mental formations and consciousness are both mundane and super mundane.

85
00:14:43,000 --> 00:14:51,000
Thinking to this world and super mundane means transcending out of this world.

86
00:14:51,000 --> 00:14:59,000
So, now we will try to understand the cleaning.

87
00:14:59,000 --> 00:15:05,000
Now, the aggregates are called aggregates of cleaning.

88
00:15:05,000 --> 00:15:09,000
There are aggregates and there are aggregates of cleaning.

89
00:15:09,000 --> 00:15:12,000
So, there are two kinds of aggregates.

90
00:15:12,000 --> 00:15:26,000
In general, an aggregates of cleaning means aggregates that are the object, objects of cleaning.

91
00:15:26,000 --> 00:15:31,000
We will try to understand the cleaning.

92
00:15:31,000 --> 00:15:52,000
Now, there are four cleanings, clinging to sense objects, clinging to wrong views, clinging to wrong practices,

93
00:15:52,000 --> 00:16:05,000
and clinging to the doctrine of self, clinging to the doctrine that there is a self.

94
00:16:05,000 --> 00:16:21,000
So, there are four kinds of clinging and the first clinging is actually the tanha, the craving.

95
00:16:21,000 --> 00:16:25,000
But craving, which has become strong.

96
00:16:25,000 --> 00:16:36,000
So, the craving which is not so strong is called craving and the craving which has become strong is called clinging.

97
00:16:36,000 --> 00:16:46,000
So, when it reaches the level of cleaning, then that means it will not let go of the object.

98
00:16:46,000 --> 00:16:57,000
It is compared to a snake, a hollowing, a frog.

99
00:16:57,000 --> 00:17:02,000
Cleaning to wrong views means just wrong views.

100
00:17:02,000 --> 00:17:15,000
Now, when a person believes that there is no comma, there is no results of comma, there is no rebirth,

101
00:17:15,000 --> 00:17:20,000
no other world and so on, then he is said to have wrong views.

102
00:17:20,000 --> 00:17:27,000
So, the wrong views themselves are called clinging to wrong views.

103
00:17:27,000 --> 00:17:44,000
The third is the clinging to wrong practices, wrong rights and wrong rituals, taking those rights and rituals to be the way to eradication of mental departments.

104
00:17:44,000 --> 00:18:01,000
So, this is also a kind of wrong view to doctrine of self clinging to the belief that there is a self.

105
00:18:01,000 --> 00:18:07,000
This self is the one that sees that hears and so on.

106
00:18:07,000 --> 00:18:15,000
So, when we hear, it is not asked that hear, but the the the Arman hears and so on.

107
00:18:15,000 --> 00:18:29,000
So, believe in a permanent self is the fourth clinging, so clinging to the doctrine of self.

108
00:18:29,000 --> 00:18:39,000
Among these four, the first is Dantna, I mean craving and the other three are wrong views.

109
00:18:39,000 --> 00:18:50,000
So, in reality, there are only two clinging, craving and wrong view.

110
00:18:50,000 --> 00:19:00,000
Now, we cling to objects with any one of these clinging.

111
00:19:00,000 --> 00:19:07,000
Whenever we experience an object, we always cling to that object.

112
00:19:07,000 --> 00:19:18,000
Sometimes by the first one, clinging sense object, or sometimes maybe with the second or the third or the fourth.

113
00:19:18,000 --> 00:19:35,000
So, it is said that if we do not if we are not mindful, if we do not practice mindfulness, we always cling to the objects with any one of these clinging.

114
00:19:35,000 --> 00:19:45,000
And the aggregates that are the objects of clinging are called aggregates of clinging.

115
00:19:45,000 --> 00:19:52,000
Aggregates of clinging means again, aggregates that are the objects or that can be the object of clinging.

116
00:19:52,000 --> 00:19:58,000
Now, if you look around, you will see that everything is the object of clinging.

117
00:19:58,000 --> 00:20:05,000
We can cling to everything by craving or by wrong view.

118
00:20:05,000 --> 00:20:11,000
So, actually, everything in the world is the object of clinging.

119
00:20:11,000 --> 00:20:22,000
Now, I told you, there are two kinds of aggregates mundane and super mundane.

120
00:20:22,000 --> 00:20:32,000
Now, super mundane aggregates are never the object of clinging.

121
00:20:32,000 --> 00:20:39,000
So, we cannot cling to that consciousness.

122
00:20:39,000 --> 00:20:53,000
We cannot cling to fruit consciousness. We cannot cling to Nabana with clingy.

123
00:20:53,000 --> 00:21:01,000
Aggregates are outside the range of the clingy.

124
00:21:01,000 --> 00:21:10,000
So, here, what Buddha said was, the burden is the five aggregates of clinging.

125
00:21:10,000 --> 00:21:15,000
So, that means five mundane aggregates.

126
00:21:15,000 --> 00:21:23,000
When we want to attain nibhana, don't we cling to nibhana?

127
00:21:23,000 --> 00:21:30,000
Yes or no? Yes?

128
00:21:30,000 --> 00:21:41,000
So, when we say may I attain nibhana, the nibhana we take as an object is not the real nibhana.

129
00:21:41,000 --> 00:21:47,000
It is the nibhana just in name or just a concept.

130
00:21:47,000 --> 00:21:59,000
The real nibhana we cannot take as object unless we get the path consciousness unless we become enlightened.

131
00:21:59,000 --> 00:22:08,000
So, nibhana cannot be clung to by these four clingings.

132
00:22:08,000 --> 00:22:20,000
The analogy given in the books is just as the flies cannot rest on the hot, I mean,

133
00:22:20,000 --> 00:22:29,000
how about that core, burning core. So, the mental defilements cannot take nibhana or the

134
00:22:29,000 --> 00:22:35,000
the super mundane states as object.

135
00:22:35,000 --> 00:22:41,000
You know the five aggregates. Five aggregates in general and five aggregates of clinging.

136
00:22:41,000 --> 00:22:52,000
And five aggregates in general are both mundane and super mundane and five aggregates of clinging are mundane only,

137
00:22:52,000 --> 00:23:04,000
not super mundane. And these five aggregates of clinging that are mundane are said to be burden in this discourse.

138
00:23:04,000 --> 00:23:20,000
Now, the commentary explains that they are called burden because they are difficult to look after difficult to keep.

139
00:23:20,000 --> 00:23:37,000
So, they are called a burden. Now, when explaining the burden, the commentary dwells only on the physical body.

140
00:23:37,000 --> 00:23:51,000
So, we have got this physical body. So, we have to keep it, we have to attend to the body, we have to make it stand,

141
00:23:51,000 --> 00:24:01,000
go sit, lie down, we have to clean this body, we have to make it up, we have to feed this body.

142
00:24:01,000 --> 00:24:10,000
And so, these are all burdens. These are all burdens, some things that we have to do with regard to the body.

143
00:24:10,000 --> 00:24:22,000
And so, the body is said to be a burden. See the body is a burden.

144
00:24:22,000 --> 00:24:38,000
Younger people may not think that it is a burden. But as you get older and older, you begin to understand that it is a real burden.

145
00:24:38,000 --> 00:25:03,000
And not as strong as you are young, you are not as healthy, your activities are not like before. And so, you come to realize that it is a real burden due to have this material body.

146
00:25:03,000 --> 00:25:14,000
It is a care of this body in many ways. Even just to survive, you have to be doing something.

147
00:25:14,000 --> 00:25:22,000
Now, when you go out, you may see people running, jogging and so on.

148
00:25:22,000 --> 00:25:29,000
So, whenever I saw people jogging, I had that feeling, oh, this is a burden.

149
00:25:29,000 --> 00:25:38,000
Just to be alive, you have to be running, right?

150
00:25:38,000 --> 00:25:49,000
There are two kinds of burdens. Those that are unavoidable and those that are avoidable.

151
00:25:49,000 --> 00:26:02,000
Now, you wake up in the morning and you have to brush your teeth, wash your face and so on, and you have to take care of your body, your health, you have to take medicine and so on.

152
00:26:02,000 --> 00:26:09,000
So, they are unavoidable. You have to do them to keep yourself healthy and alive.

153
00:26:09,000 --> 00:26:21,000
But there are some things that are avoidable, but that people willingly take upon themselves.

154
00:26:21,000 --> 00:26:29,000
Now, I talk about jogging, right? Jogging is okay because it is for one's health.

155
00:26:29,000 --> 00:26:50,000
But there are people who go through their lungs or who would do anything to get their body, and could shape a beautiful body, to be thin, to be beautiful.

156
00:26:50,000 --> 00:27:06,000
So, these are avoidable burdens of the body. People are taking upon themselves and also there are people who want to get big muscles, both men and women.

157
00:27:06,000 --> 00:27:27,000
And what they have to do is, I have confession for them. They have to wait lifting, right? They have to pick up and then they practice with these big machines.

158
00:27:27,000 --> 00:27:41,000
And so, these are the things they accept willingly, right? And also some people want to remove the wrinkles, face-live and so on.

159
00:27:41,000 --> 00:27:56,000
So, they have to suffer to get just face-lived. So, there are a lot of dukas, which people willingly accept, just to be beautiful,

160
00:27:56,000 --> 00:28:13,000
or just to have big muscles and say to have a good name or to get money. But the sad thing about it is, those people do not necessarily live longer.

161
00:28:13,000 --> 00:28:26,000
They may get fame, they may get money, which is short-lived, but they don't live longer, necessarily.

162
00:28:26,000 --> 00:28:36,000
You talk about the physical body. I think we include the mental aggregates also because this cannot be isolated.

163
00:28:36,000 --> 00:28:46,000
And also, we want to have good feelings. We want to have pleasant feelings and not unpleasant feelings.

164
00:28:46,000 --> 00:28:53,000
So, we have to make effort. We have to do something to feel good, to have the good feeling.

165
00:28:53,000 --> 00:28:57,000
So, that is also the burden of the feeling aggregate.

166
00:28:57,000 --> 00:29:08,000
And then we want to remember pleasant things. We don't want to remember unpleasant incidents or unpleasant objects.

167
00:29:08,000 --> 00:29:23,000
So, but although we don't want to remember unpleasant objects as you know during meditation, they can come up out of nowhere.

168
00:29:23,000 --> 00:29:28,000
The long, long forgotten memories and so on.

169
00:29:28,000 --> 00:29:42,000
So, we have to try hard to get free from them. And that is also the burden of the perception aggregate.

170
00:29:42,000 --> 00:29:48,000
And with regard to the mental formation aggregates.

171
00:29:48,000 --> 00:29:56,000
So, mental formation means these status acres. So, these status acres arise and disappear arise and disappear.

172
00:29:56,000 --> 00:30:02,000
And so, we want to have good karma. So, we avoid bad karma and good karma.

173
00:30:02,000 --> 00:30:05,000
So, that is also a kind of burden.

174
00:30:05,000 --> 00:30:11,000
So, the five aggregates, that is why Buddha said these five aggregates are burden.

175
00:30:11,000 --> 00:30:22,000
So, the moment you get these five aggregates as a burden, you have to carry it all along until the end of the life.

176
00:30:22,000 --> 00:30:28,000
There is not a single moment when you can put it down.

177
00:30:28,000 --> 00:30:30,000
So, you always have to carry this.

178
00:30:30,000 --> 00:30:36,000
So, imagine carrying that heavy load on your back and there is no respite.

179
00:30:36,000 --> 00:30:47,000
No interweil between the burden. So, how heavy it is at the burden of the five aggregates is.

180
00:30:47,000 --> 00:30:54,000
The burden over finished when one dies.

181
00:30:54,000 --> 00:31:03,000
This moment you get rid of this at the burden.

182
00:31:03,000 --> 00:31:11,000
And next moment you get another burden, maybe even heavier.

183
00:31:11,000 --> 00:31:18,000
So, this burden goes on and on and on until the end of the samsara.

184
00:31:18,000 --> 00:31:32,000
So, there is no rest period when you are carrying this burden of the five aggregates.

185
00:31:32,000 --> 00:31:35,000
So, now we understand the burden.

186
00:31:35,000 --> 00:31:37,000
Five aggregates of cleaning.

187
00:31:37,000 --> 00:31:41,000
Now, the second is the carrier of the burden.

188
00:31:41,000 --> 00:31:46,000
So, Buddha said who is the carrier of the burden or what is the carrier of the burden?

189
00:31:46,000 --> 00:31:49,000
And Buddha said it is the person.

190
00:31:49,000 --> 00:31:58,000
It is the individual, maybe someone by this name or by that name.

191
00:31:58,000 --> 00:32:06,000
So, the person or the individual is the carrier of that burden.

192
00:32:06,000 --> 00:32:12,000
Now, here comes the problem.

193
00:32:12,000 --> 00:32:17,000
Now, please note the word.

194
00:32:17,000 --> 00:32:25,000
The burden is bhara and the burden carrier is bhara.

195
00:32:25,000 --> 00:32:37,000
Now, there are people, mostly non-Buddhist people or Western people.

196
00:32:37,000 --> 00:32:42,000
Please excuse me.

197
00:32:42,000 --> 00:32:49,000
Who want to put Arman into Buddhism?

198
00:32:49,000 --> 00:32:58,000
So, who tried to find every indication that Buddha believed in Arman or something like that?

199
00:32:58,000 --> 00:33:07,000
And so, here they said that now here, Buddha is saying the person is the carrier of the

200
00:33:07,000 --> 00:33:23,000
burden.

201
00:33:23,000 --> 00:33:36,000
Now, the English translation of this discourse is very different.

202
00:33:36,000 --> 00:33:45,000
It is translated here as the taking hold of the burden, which I translated as burden carrier.

203
00:33:45,000 --> 00:33:56,000
So, the word bhara, the word hara can mean carrying as well as a person who carries.

204
00:33:56,000 --> 00:34:03,000
So, he took the hara, the word hara to mean carrying.

205
00:34:03,000 --> 00:34:08,000
And so, he translated as the taking hold of the burden.

206
00:34:08,000 --> 00:34:14,000
But the next word is lifting of it up.

207
00:34:14,000 --> 00:34:21,000
So, I want to ask him, what is the difference between taking hold of the burden and lifting it up?

208
00:34:21,000 --> 00:34:26,000
Sometimes people are very, very stubborn.

209
00:34:26,000 --> 00:34:38,000
Because here they want to make the person exist or they want to make Buddha accept the

210
00:34:38,000 --> 00:34:40,000
person in reality.

211
00:34:40,000 --> 00:34:50,000
So, here they said the bhara hara is to be translated as taking hold of the burden and not

212
00:34:50,000 --> 00:34:55,000
the carrier of the burden.

213
00:34:55,000 --> 00:35:08,000
So, the other things that in Buddhism, the burden carrier or a person or individual in reality

214
00:35:08,000 --> 00:35:09,000
is not accepted.

215
00:35:09,000 --> 00:35:16,000
So, in order to avoid that, he translated the word as not the burden carrier but the taking

216
00:35:16,000 --> 00:35:17,000
hold of the burden.

217
00:35:17,000 --> 00:35:22,000
So, where which is the same as saying lifting up of the burden.

218
00:35:22,000 --> 00:35:36,000
So, there are some people who said according to this course, there is a person accepted

219
00:35:36,000 --> 00:35:37,000
by the Buddha.

220
00:35:37,000 --> 00:35:54,000
And this translated quotes one book where the other of dead books are something different

221
00:35:54,000 --> 00:36:01,000
from what we understand.

222
00:36:01,000 --> 00:36:10,000
The book was quoted in this translation and it says, the other of this sutra, this

223
00:36:10,000 --> 00:36:19,000
discourse, did not entertain the view that the person is nothing except the five aggregates.

224
00:36:19,000 --> 00:36:22,000
That is the correct view, right?

225
00:36:22,000 --> 00:36:31,000
So, he says, the other of this sutra, he takes the Buddha to be the other or somebody who

226
00:36:31,000 --> 00:36:33,000
wrote this discourse.

227
00:36:33,000 --> 00:36:40,000
So, the other of this sutra did not entertain the view that the person is nothing except

228
00:36:40,000 --> 00:36:43,000
the five aggregates.

229
00:36:43,000 --> 00:36:56,000
So, he says, these authorities, Pudugosa, Vasubandru, Chandra Kirti and Yasomitra insist.

230
00:36:56,000 --> 00:37:06,000
And so, that does not believe that a person is nothing but the five aggregates.

231
00:37:06,000 --> 00:37:09,000
A person is in reality exist.

232
00:37:09,000 --> 00:37:16,000
So, the other of this sutra believes that way.

233
00:37:16,000 --> 00:37:27,000
Although, the authorities like the Venerable Buddha Gosa and other Buddhist scholars, Buddhist

234
00:37:27,000 --> 00:37:29,000
teachers insist.

235
00:37:29,000 --> 00:37:36,000
So, that means, Venerable Buddha Gosa and Vasubandru, Chandra Kirti and Yasomitra, or the

236
00:37:36,000 --> 00:37:41,000
Venerable Buddha Gosa belong to Tiravada Buddhism and the others belong to Mahayana Buddhism.

237
00:37:41,000 --> 00:37:50,000
So, both Mahayana and Tiravada agree here that there is no person, just the five aggregates.

238
00:37:50,000 --> 00:38:00,000
But he said, this sutra believes in the person against what these Buddhist teachers insist.

239
00:38:00,000 --> 00:38:13,000
So, he is going against these authorities on Buddhism.

240
00:38:13,000 --> 00:38:22,000
So, I think the other of that book meant to say that I do not care what these ancient authorities

241
00:38:22,000 --> 00:38:31,000
and Buddhism say said because here is a sutra and so, I will take that Buddha believes

242
00:38:31,000 --> 00:38:36,000
in or Buddha accepted the person as a reality.

243
00:38:36,000 --> 00:38:50,000
So, he is actually, I recall, contradicting the tradition which has been handed down from

244
00:38:50,000 --> 00:38:55,000
generation to generation for more than 2,500 years.

245
00:38:55,000 --> 00:39:02,000
But this other just born in maybe 19th century.

246
00:39:02,000 --> 00:39:12,000
He is far away from the Buddha with regard to time or place or tradition or culture.

247
00:39:12,000 --> 00:39:22,000
But sometimes these people think that they know more than those who are closest to the Buddha knew.

248
00:39:22,000 --> 00:39:31,000
They discovered something that was hidden from these disciples, the immediate disciples.

249
00:39:31,000 --> 00:39:44,000
So, it is very hard to go.

250
00:39:44,000 --> 00:39:52,000
So, such people seem to try to break the tradition.

251
00:39:52,000 --> 00:39:58,000
But I think tradition is too strong for them.

252
00:39:58,000 --> 00:40:11,000
And he also said all those who maintain that the sutra accepts a person are justified.

253
00:40:11,000 --> 00:40:19,000
Now, here Buddha said the career of the burden is the person.

254
00:40:19,000 --> 00:40:26,000
So, I think what they think was the burden must be different from the career.

255
00:40:26,000 --> 00:40:30,000
The burden is one thing and the career is another.

256
00:40:30,000 --> 00:40:36,000
So, here Buddha said five aggregates are burden and the person is the career.

257
00:40:36,000 --> 00:40:40,000
So, the person must be a different thing from the five aggregates.

258
00:40:40,000 --> 00:40:49,000
He must be a separate entity, a permanent entity or an art man that bears the burden.

259
00:40:49,000 --> 00:40:55,000
So, I think they argued that way.

260
00:40:55,000 --> 00:41:04,000
But there are hundreds of instances where Buddha used the word person or individual in his talks.

261
00:41:04,000 --> 00:41:14,000
If we were to take that because Buddha used the word person in disorder, the Buddha accept the person as reality,

262
00:41:14,000 --> 00:41:22,000
then Buddha was the strongest promoter of the existence of a person.

263
00:41:22,000 --> 00:41:35,000
Because every, you read every soda and you find the word individual or person hundreds of times.

264
00:41:35,000 --> 00:41:43,000
Buddha did not believe in the existence of a person.

265
00:41:43,000 --> 00:41:49,000
Then why did he use these words in his teachings?

266
00:41:49,000 --> 00:41:57,000
So, as I said, you find hundreds of these words in his discourses.

267
00:41:57,000 --> 00:42:05,000
So, Buddha had to use these terms because he could not avoid using them.

268
00:42:05,000 --> 00:42:23,000
He could not say, if a bundle of five aggregates goes to the monastery and a five bundle of five aggregates gave a dumb at all.

269
00:42:23,000 --> 00:42:29,000
A bundle of five aggregates make donations to the monasteries.

270
00:42:29,000 --> 00:42:31,000
He could not say that.

271
00:42:31,000 --> 00:42:38,000
So, he had to use the conventional terms so that people understand.

272
00:42:38,000 --> 00:42:51,000
So, Buddha did not go against the conventional language because it is through this conventional language that he reached people.

273
00:42:51,000 --> 00:43:01,000
If he were to use just the aggregates and bases or elements and so on, nobody would understand him.

274
00:43:01,000 --> 00:43:19,000
So, he had to use the conventional language to make people understand so that they can practice and they can get the benefits of his teaching.

275
00:43:19,000 --> 00:43:30,000
So, Buddha used both conventional terms and absolute terms when he preached.

276
00:43:30,000 --> 00:43:35,000
He was like a very clever teacher.

277
00:43:35,000 --> 00:43:44,000
A teacher teaching to a class made up of people belonging to different nationalities.

278
00:43:44,000 --> 00:43:55,000
Would speak to a certain group in their own language and to another group in their language and so on so that they can understand him.

279
00:43:55,000 --> 00:44:11,000
So, in the same way, when Buddha talked, he used the conventional terms to those people who would understand the conventional terms and then practice and get results.

280
00:44:11,000 --> 00:44:17,000
And he would use absolute terms to those who would understand those absolute terms.

281
00:44:17,000 --> 00:44:27,000
Like when he went up to tower, things are heaven and preached a bit of a to the gods.

282
00:44:27,000 --> 00:44:34,000
And also in vigna, Buddha used conventional terms only.

283
00:44:34,000 --> 00:44:50,000
Like when you say, if a monk cuts a tree, say he comes to an offense, but he did not say if a five aggregate with the ropes,

284
00:44:50,000 --> 00:44:56,000
cuts the material aggregate, then he comes to some offense.

285
00:44:56,000 --> 00:45:11,000
Nobody would understand if he would use the absolute terms, so if a monk, not that Buddha believed that a monk in reality exists.

286
00:45:11,000 --> 00:45:19,000
But he had to use these terms to make his disciples understand.

287
00:45:19,000 --> 00:45:28,000
So, here also, to make his audience understand the monks understand Buddha used the conventional terms.

288
00:45:28,000 --> 00:45:37,000
So, he said, the burden career is the person.

289
00:45:37,000 --> 00:45:50,000
So, the burden career is the person, but we have one problem, one still to be solved.

290
00:45:50,000 --> 00:45:54,000
Now, the person is also the five aggregates.

291
00:45:54,000 --> 00:46:04,000
And so, the five aggregates are the burden, and the five aggregates are those that carry the burden.

292
00:46:04,000 --> 00:46:08,000
So, these two are the same.

293
00:46:08,000 --> 00:46:11,000
Now, this is the usage.

294
00:46:11,000 --> 00:46:21,000
When we use language, sometimes we use words to describe one thing.

295
00:46:21,000 --> 00:46:36,000
But sometimes we use words as though they were different.

296
00:46:36,000 --> 00:46:39,000
Now, what is it tree?

297
00:46:39,000 --> 00:46:43,000
It is a combination of the trunk and branch.

298
00:46:43,000 --> 00:46:46,000
The branch is included in the tree.

299
00:46:46,000 --> 00:46:58,000
So, we say a branch of a tree means protocol, saying the same thing again.

300
00:46:58,000 --> 00:47:02,000
A branch and a tree are not two different things, right?

301
00:47:02,000 --> 00:47:09,000
But we use this usage, a branch of a tree, or maybe a member of the community.

302
00:47:09,000 --> 00:47:18,000
So, the member is included in the community, but we use the word so as though community is one thing and the member is another.

303
00:47:18,000 --> 00:47:24,000
They separate things, but the member is included in the community.

304
00:47:24,000 --> 00:47:33,000
So, this is the usage that we cannot violate.

305
00:47:33,000 --> 00:47:48,000
So, what I use here, the word person to drive the point that the five aggregates are burden.

306
00:47:48,000 --> 00:48:00,000
And so, a person according to the conventional sense is the one who has to carry that burden.

307
00:48:00,000 --> 00:48:04,000
So, this explanation actually comes from the commentary.

308
00:48:04,000 --> 00:48:15,000
So, the commentary says, thus a person who is established as mere convention is shown as the burden carrier.

309
00:48:15,000 --> 00:48:29,000
So, a person who is established as mere convention, it is established as a usage that the bundle of five aggregates is called a person.

310
00:48:29,000 --> 00:48:39,000
So, this term has come to be established to represent these five aggregates.

311
00:48:39,000 --> 00:48:44,000
And only when this term is used could people understand.

312
00:48:44,000 --> 00:48:58,000
So, the Buddha used the word person or Buddha describes that the person who is established as a mere convention is the burden carrier.

313
00:48:58,000 --> 00:49:06,000
So, who is the burden carrier, the person, the individual.

314
00:49:06,000 --> 00:49:23,000
So, the individual is the one who has to carry this heavy burden from the moment of conception all through his life 2030, 40, 50, 100 years.

315
00:49:23,000 --> 00:49:37,000
And then that comes and he throws down this old burden and he has to pick up the new burden the next moment.

316
00:49:37,000 --> 00:49:44,000
And then he has to go on carrying that burden to the end of the life and then to the next life and so on.

317
00:49:44,000 --> 00:49:54,000
And so, the person is said to be the carrier of the burden of the five aggregates of cleaning.

318
00:49:54,000 --> 00:50:05,000
Now, taking off the burden is craving and laying down the burden is the total cessation of suffering.

319
00:50:05,000 --> 00:50:16,000
So, I will explain this to you tomorrow.

