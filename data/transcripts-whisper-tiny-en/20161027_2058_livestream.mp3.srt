1
00:00:00,000 --> 00:00:16,000
Hello, and welcome to our broadcast, what have I forgotten?

2
00:00:16,000 --> 00:00:35,000
Oh, it started automatically, and that's funny.

3
00:00:35,000 --> 00:00:39,000
Alright, I no longer have to start it.

4
00:00:39,000 --> 00:00:42,000
I can't just mess it up.

5
00:00:42,000 --> 00:00:43,000
Good evening, everyone.

6
00:00:43,000 --> 00:00:46,000
Welcome to our broadcast.

7
00:00:46,000 --> 00:00:53,000
Tonight we're looking at a good journey, a book of sixes,

8
00:00:53,000 --> 00:01:02,000
soot of fifty-three.

9
00:01:02,000 --> 00:01:09,000
The epamada soot.

10
00:01:09,000 --> 00:01:15,000
And so it starts at a cool and yet a little brahmano.

11
00:01:15,000 --> 00:01:20,000
At that time, a certain brahmana, yinabagavat,

12
00:01:20,000 --> 00:01:29,000
some kami approach, the bhagavat, the blessed one.

13
00:01:29,000 --> 00:01:35,000
Having approached the blessed one, sending samodhi.

14
00:01:35,000 --> 00:01:37,000
He shattered with him.

15
00:01:37,000 --> 00:01:44,000
Samodhi is pleasantries.

16
00:01:44,000 --> 00:01:48,000
Samodhi, niyang, kattang, sireh, niyang.

17
00:01:48,000 --> 00:01:50,000
We decided by a kamantam.

18
00:01:50,000 --> 00:01:55,000
After having exchanged pleasantries, pleasant talk,

19
00:01:55,000 --> 00:02:03,000
that was kind and...

20
00:02:03,000 --> 00:02:04,000
And so on.

21
00:02:04,000 --> 00:02:10,000
He sat at one side and then he asked.

22
00:02:10,000 --> 00:02:19,000
Ati no kobo, go to moo, go to moo.

23
00:02:19,000 --> 00:02:25,000
It should be a familiar question, I think.

24
00:02:25,000 --> 00:02:28,000
Well, at least this part of it.

25
00:02:28,000 --> 00:02:30,000
Tell me go to moo.

26
00:02:30,000 --> 00:02:34,000
Brethren, go to moo.

27
00:02:34,000 --> 00:02:37,000
Is there one dhamma?

28
00:02:37,000 --> 00:02:40,000
One thing.

29
00:02:40,000 --> 00:02:42,000
It's a common question, no?

30
00:02:42,000 --> 00:02:44,000
We want the short version.

31
00:02:44,000 --> 00:02:47,000
We want one dhamma.

32
00:02:47,000 --> 00:02:49,000
Give me one teaching.

33
00:02:49,000 --> 00:02:53,000
Don't give me all this lists of things, right?

34
00:02:53,000 --> 00:02:54,000
You'll be happy tonight.

35
00:02:54,000 --> 00:02:56,000
We don't have a list of six, even though it's the book of six,

36
00:02:56,000 --> 00:02:58,000
because we just got one thing.

37
00:02:58,000 --> 00:03:09,000
When cultivated, one made much of yo-o-o-bo,

38
00:03:09,000 --> 00:03:19,000
ati is samadhi, kai-hati.

39
00:03:19,000 --> 00:03:24,000
That's...

40
00:03:24,000 --> 00:03:31,000
We don't quite understand that.

41
00:03:38,000 --> 00:03:41,000
It's able to accomplish, which is how that works.

42
00:03:41,000 --> 00:03:48,000
It's able to accomplish

43
00:03:48,000 --> 00:03:49,000
both benefits.

44
00:03:49,000 --> 00:03:54,000
Don't just read the English, the benefits in this life,

45
00:03:54,000 --> 00:03:59,000
or benefit in this life, and benefit in the life here after.

46
00:03:59,000 --> 00:04:06,000
Samadhi, samadhi, kong.

47
00:04:06,000 --> 00:04:12,000
You say this is familiar, because it sounds like he's looking for a shortcut, right?

48
00:04:12,000 --> 00:04:15,000
He's looking for a quick fix.

49
00:04:15,000 --> 00:04:26,000
This should sound familiar to many of us are looking for a quick fix.

50
00:04:26,000 --> 00:04:28,000
It's kind of lazy in a sense.

51
00:04:28,000 --> 00:04:33,000
We don't want to have to work, or anything complicated, or involved,

52
00:04:33,000 --> 00:04:39,000
something you have to get caught up in.

53
00:04:39,000 --> 00:04:40,000
It's a good question.

54
00:04:40,000 --> 00:04:41,000
It's not a bad question.

55
00:04:41,000 --> 00:04:46,000
The question is, what is that one thing?

56
00:04:46,000 --> 00:04:49,000
Here, the Buddha's been teaching for 45 years.

57
00:04:49,000 --> 00:04:52,000
So many different dhammas, so many complex teachings in here.

58
00:04:52,000 --> 00:04:58,000
This guy wants them to summon up in one dhamma.

59
00:04:58,000 --> 00:05:00,000
Can he do that?

60
00:05:00,000 --> 00:05:01,000
And indeed, the Buddha does that.

61
00:05:01,000 --> 00:05:09,000
And it's given away in the title of this sutta.

62
00:05:09,000 --> 00:05:11,000
The Buddha says, Ati, kobra manu.

63
00:05:11,000 --> 00:05:13,000
There is indeed one dhamma.

64
00:05:13,000 --> 00:05:16,000
There is a wall.

65
00:05:16,000 --> 00:05:24,000
What is it?

66
00:05:24,000 --> 00:05:31,000
When the Buddha says, a pamado, kobra manu, a kobra manu.

67
00:05:31,000 --> 00:05:33,000
A pamada is that one dhamma.

68
00:05:33,000 --> 00:05:51,000
A pamada is the, many, many people will call it, the key of the Buddha's teaching,

69
00:05:51,000 --> 00:05:54,000
and it's the summary of the Buddha's teaching.

70
00:05:54,000 --> 00:06:00,000
The commentary says, all of the tepitika can be, can be boiled down to this one thing,

71
00:06:00,000 --> 00:06:03,000
the path of a pamada.

72
00:06:03,000 --> 00:06:05,000
It's the last words of the Buddha.

73
00:06:05,000 --> 00:06:08,000
A pamadi in the sampadita.

74
00:06:08,000 --> 00:06:15,000
Become accomplished, or fulfilled, or fulfilled yourself.

75
00:06:15,000 --> 00:06:34,000
Become fulfilled with, filled up with, accomplished in a pamada.

76
00:06:34,000 --> 00:06:40,000
One of the most useful aspects of this teaching is cutting away all the,

77
00:06:40,000 --> 00:06:45,000
in fact, trimming away all the fat.

78
00:06:45,000 --> 00:06:53,000
You might have doubt that it was actually possible to boil it all down to one dhamma.

79
00:06:53,000 --> 00:07:00,000
It seemed like an impossible task, but the immediate benefit of being able to do so is,

80
00:07:00,000 --> 00:07:05,000
makes things a lot more simple.

81
00:07:05,000 --> 00:07:18,000
Unfortunately, at the same time, it removes any excuse or escape from actually doing the work.

82
00:07:18,000 --> 00:07:25,000
Because it's basically saying, how do you succeed in your work?

83
00:07:25,000 --> 00:07:28,000
So for anyone who thought they could find a quick fix, or a shortcut,

84
00:07:28,000 --> 00:07:34,000
a way around the practice, because at pamada,

85
00:07:34,000 --> 00:07:37,000
a pamada is a difficult word to translate.

86
00:07:37,000 --> 00:07:44,000
We have translations, and we can make approximations, but it's also useful to go down to the root of the word,

87
00:07:44,000 --> 00:07:47,000
which come from mud.

88
00:07:47,000 --> 00:07:53,000
Mud is a root, root means it's the kernel in the middle of the word.

89
00:07:53,000 --> 00:07:57,000
That spawns all sorts of other words.

90
00:07:57,000 --> 00:08:04,000
And it relates to, it has a kernel of meaning relating to confusion,

91
00:08:04,000 --> 00:08:11,000
darkness, cloudiness, that kind of thing.

92
00:08:11,000 --> 00:08:16,000
Muddy, maybe where the word mud comes from in English.

93
00:08:16,000 --> 00:08:22,000
It's clear as mud.

94
00:08:22,000 --> 00:08:31,000
But when it becomes pamada, pamada is a little more serious because of strength.

95
00:08:31,000 --> 00:08:38,000
But pamada is generally used to refer to intoxication, a drunken state.

96
00:08:38,000 --> 00:08:48,000
So not just confused in an ordinary sense, but in a really bad way,

97
00:08:48,000 --> 00:08:53,000
it's confused in the bad sense, because a good person can be confused.

98
00:08:53,000 --> 00:09:00,000
If you ask someone something, if you're not clear, then they might be unsure.

99
00:09:00,000 --> 00:09:07,000
But only a person who is as unwholesomeness in their mind can be intoxicated.

100
00:09:07,000 --> 00:09:10,000
It's also used to talk about physical intoxication.

101
00:09:10,000 --> 00:09:17,000
That's where the word, that's where the word, that's also used there.

102
00:09:17,000 --> 00:09:21,000
If the precept, alcohol is something that leads to pamada.

103
00:09:21,000 --> 00:09:24,000
And pamada is just a negation.

104
00:09:24,000 --> 00:09:28,000
So it's the opposite of being intoxicated.

105
00:09:28,000 --> 00:09:35,000
The opposite of being knee-breated.

106
00:09:35,000 --> 00:09:43,000
And so you can think of it as analogous, analogous, analogous, analogous.

107
00:09:43,000 --> 00:09:51,000
To physical intoxication, just like physical intoxication with alcohol,

108
00:09:51,000 --> 00:09:53,000
it's a big problem for you.

109
00:09:53,000 --> 00:09:55,000
There's also mental intoxication.

110
00:09:55,000 --> 00:10:04,000
There's intoxication through greed, through anger, through delusion.

111
00:10:04,000 --> 00:10:06,000
Greed is something that intoxicates us.

112
00:10:06,000 --> 00:10:10,000
A person full of greed can't think clearly.

113
00:10:10,000 --> 00:10:15,000
A person full of anger certainly isn't thinking clearly.

114
00:10:15,000 --> 00:10:19,000
And a person full of delusion, arrogance, conceit.

115
00:10:19,000 --> 00:10:21,000
Don't think clearly.

116
00:10:21,000 --> 00:10:24,000
Egotism.

117
00:10:24,000 --> 00:10:27,000
High self-esteem, low self-esteem.

118
00:10:27,000 --> 00:10:36,000
And all muddies your thought, muddies your mind.

119
00:10:36,000 --> 00:10:44,000
So pamada is this clarity of mind, really, this unsulliedness.

120
00:10:44,000 --> 00:10:46,000
It might be a good translation.

121
00:10:46,000 --> 00:10:50,000
We often try to find a positive, or we often translate in the positive sense,

122
00:10:50,000 --> 00:10:52,000
which is problematic.

123
00:10:52,000 --> 00:10:56,000
Like if you say vigilant, which I've used tonight,

124
00:10:56,000 --> 00:11:01,000
put a name of this talk, and heedfulness.

125
00:11:01,000 --> 00:11:03,000
These are positive versions.

126
00:11:03,000 --> 00:11:10,000
pamada is actually a negation of something, it's like not something.

127
00:11:10,000 --> 00:11:15,000
But you can see how it relates to vigilant seedfulness.

128
00:11:15,000 --> 00:11:19,000
Non-negligence.

129
00:11:19,000 --> 00:11:24,000
It's a little bit awkward.

130
00:11:24,000 --> 00:11:30,000
The unconfusedness, the unconfused nature of the mind,

131
00:11:30,000 --> 00:11:47,000
and it's not confused, diluted, distracted from the reality.

132
00:11:47,000 --> 00:11:51,000
The mind that sees things as they are, that's pamada.

133
00:11:51,000 --> 00:11:59,000
If you have that clarity of mind, that's a perfect sobriety.

134
00:11:59,000 --> 00:12:06,000
That's pamada.

135
00:12:06,000 --> 00:12:09,000
And so if you have a distant answer, a lot of questions.

136
00:12:09,000 --> 00:12:13,000
You hear the Buddha saying it relates to benefits even in this life,

137
00:12:13,000 --> 00:12:19,000
or benefits in this life, but even benefits in terms of your afterlife.

138
00:12:19,000 --> 00:12:23,000
In terms of your soul, for lack of a better word,

139
00:12:23,000 --> 00:12:29,000
your spiritual journey through Samsara,

140
00:12:29,000 --> 00:12:35,000
and unspoken, of course, is the benefit in an ultimate sense,

141
00:12:35,000 --> 00:12:38,000
in terms of freeing oneself from Samsara.

142
00:12:38,000 --> 00:12:41,000
But there's no shortcut.

143
00:12:41,000 --> 00:12:43,000
There's no way around it.

144
00:12:43,000 --> 00:12:45,000
A lot of the questions, how do I deal with this?

145
00:12:45,000 --> 00:12:48,000
How do I deal with that?

146
00:12:48,000 --> 00:12:51,000
Or even how do I know my practice is working?

147
00:12:51,000 --> 00:12:54,000
How do I know my practice incorrectly?

148
00:12:54,000 --> 00:12:59,000
There's only one correct practice in Buddhism, really.

149
00:12:59,000 --> 00:13:07,000
All other practices have to be relegated to a supportive role.

150
00:13:07,000 --> 00:13:16,000
And the only dhamma that is truly the path that is intrinsic to the path is pamada.

151
00:13:16,000 --> 00:13:21,000
That's maybe not quite true. There are many dhammas that come along with it.

152
00:13:21,000 --> 00:13:28,000
But the leader, the dhamma that is the chief.

153
00:13:28,000 --> 00:13:30,000
And so that's what the Buddha says in this Buddha.

154
00:13:30,000 --> 00:13:32,000
So that's why we have Book of Sixes.

155
00:13:32,000 --> 00:13:34,000
There are six similes.

156
00:13:34,000 --> 00:13:42,000
They say it's just like the elephant's footprint.

157
00:13:42,000 --> 00:13:48,000
It's the biggest footprint and all other animal footprints fit into the elephant's footprint.

158
00:13:48,000 --> 00:13:51,000
So the elephant's footprint is foremost.

159
00:13:51,000 --> 00:13:55,000
A pamada is like that.

160
00:13:55,000 --> 00:13:57,000
Or the rafters of a house.

161
00:13:57,000 --> 00:14:02,000
If you have a piqued house, one of the houses that's like a round roof or a teepee kind of,

162
00:14:02,000 --> 00:14:06,000
they all lean towards the piqued.

163
00:14:06,000 --> 00:14:22,000
And the same way all dhammas lean towards a pamada.

164
00:14:22,000 --> 00:14:31,000
And he has a simile of a reed cutter and he doesn't understand that.

165
00:14:31,000 --> 00:14:36,000
I guess he grabs the kind of bunch of reeds you grab them by the top.

166
00:14:36,000 --> 00:14:42,000
I think that you have to grab them reeds by the top for some reason.

167
00:14:42,000 --> 00:14:45,000
So the top is pamada, I guess.

168
00:14:45,000 --> 00:14:48,000
When you kind of bunch of mangoes from the tree,

169
00:14:48,000 --> 00:14:53,000
they all cling to the stalk and follow the stalk.

170
00:14:53,000 --> 00:14:58,000
So the stalk is pamada.

171
00:14:58,000 --> 00:15:05,000
And just like all princes and royals are vassals of a wheel turning monarch.

172
00:15:05,000 --> 00:15:10,000
The king of the world.

173
00:15:10,000 --> 00:15:14,000
The king is in charge.

174
00:15:14,000 --> 00:15:20,000
So pamada is like the king.

175
00:15:20,000 --> 00:15:24,000
Or just as in the sky, when you look up at the light from the stars,

176
00:15:24,000 --> 00:15:29,000
you have all the light from all these stars, bright twinkling bright.

177
00:15:29,000 --> 00:15:33,000
They don't amount to a 16th part of the radiance of the moon.

178
00:15:33,000 --> 00:15:37,000
So the radiance of the moon is foremost.

179
00:15:37,000 --> 00:15:49,000
The light of the moon is far brighter than the light from that we see from all the star.

180
00:15:49,000 --> 00:15:54,000
The same way, pamada is like the moon.

181
00:15:54,000 --> 00:15:57,000
Compare that to the light from all the other stars,

182
00:15:57,000 --> 00:16:03,000
means all other demos are insignificant, really.

183
00:16:03,000 --> 00:16:06,000
So this is what we have to cultivate.

184
00:16:06,000 --> 00:16:13,000
I mean, pamada is like this continual state of clarity,

185
00:16:13,000 --> 00:16:19,000
the cultivation of moments of clarity,

186
00:16:19,000 --> 00:16:24,000
the changing or building up of proper habits,

187
00:16:24,000 --> 00:16:27,000
and the cutting down of improper habits,

188
00:16:27,000 --> 00:16:38,000
just like cutting down a jungle to make arable farmland.

189
00:16:38,000 --> 00:16:45,000
You cut down all the thickets and you organize,

190
00:16:45,000 --> 00:16:47,000
make the land livable.

191
00:16:47,000 --> 00:16:52,000
You do the same with our minds cutting down all the habits,

192
00:16:52,000 --> 00:16:57,000
and all the cobwebs in our minds,

193
00:16:57,000 --> 00:17:04,000
all those things in our minds that cultivate

194
00:17:04,000 --> 00:17:10,000
our breeding ground for greed, anger, and delusion.

195
00:17:10,000 --> 00:17:12,000
It's quite a simple practice.

196
00:17:12,000 --> 00:17:14,000
This is what mindfulness is right.

197
00:17:14,000 --> 00:17:17,000
But as said, Satya, a weep of us,

198
00:17:17,000 --> 00:17:22,000
pamada, a deep, a deep, a weep of us.

199
00:17:22,000 --> 00:17:25,000
I'm not sure the ending of that.

200
00:17:25,000 --> 00:17:28,000
To never be without mindfulness, as the Buddha said,

201
00:17:28,000 --> 00:17:31,000
this is what pamada is.

202
00:17:31,000 --> 00:17:35,000
The mindfulness is really what it means here, or Satya.

203
00:17:35,000 --> 00:17:38,000
And when you remember yourself,

204
00:17:38,000 --> 00:17:41,000
so this act of reminding ourselves,

205
00:17:41,000 --> 00:17:43,000
the clarity of mind that you feel,

206
00:17:43,000 --> 00:17:48,000
I mean, it's a shame that there's so many questions about

207
00:17:48,000 --> 00:17:50,000
about the benefit of the practice.

208
00:17:50,000 --> 00:17:54,000
Everyone's focused on some measure,

209
00:17:54,000 --> 00:17:56,000
which is very difficult, especially in the beginning.

210
00:17:56,000 --> 00:17:58,000
How can you measure how far you've gotten?

211
00:17:58,000 --> 00:18:00,000
How good this is for you?

212
00:18:00,000 --> 00:18:03,000
And we ignore the greatness of the moment.

213
00:18:03,000 --> 00:18:05,000
At that moment, when you're mindful,

214
00:18:05,000 --> 00:18:08,000
how can you doubt that state of pamada,

215
00:18:08,000 --> 00:18:13,000
or you're perfectly so see things perfectly clear?

216
00:18:13,000 --> 00:18:16,000
A million of sense who cares what comes from it.

217
00:18:16,000 --> 00:18:20,000
I mean, who's why would you focus on what comes from it?

218
00:18:20,000 --> 00:18:23,000
When you know that your mind is pure as clear,

219
00:18:23,000 --> 00:18:29,000
that in and of itself is the first benefit of mindfulness,

220
00:18:29,000 --> 00:18:32,000
and satanangwisudhiya,

221
00:18:32,000 --> 00:18:35,000
for the purification of beings,

222
00:18:35,000 --> 00:18:38,000
just the purity of mind,

223
00:18:38,000 --> 00:18:41,000
and the building up of that habit,

224
00:18:41,000 --> 00:18:44,000
changing the way we look at things.

225
00:18:44,000 --> 00:18:48,000
So rather than reacting and being all muddled

226
00:18:48,000 --> 00:18:52,000
and intoxicated by experiencing,

227
00:18:52,000 --> 00:18:55,000
to live our lives free from intoxication,

228
00:18:55,000 --> 00:18:57,000
to live our lives objective,

229
00:18:57,000 --> 00:19:01,000
to pure and clear.

230
00:19:01,000 --> 00:19:03,000
So we're aiming for the meditation.

231
00:19:03,000 --> 00:19:06,000
That's what we get every moment.

232
00:19:06,000 --> 00:19:08,000
And as you build up moments and moments,

233
00:19:08,000 --> 00:19:10,000
it's like rain pouring down.

234
00:19:10,000 --> 00:19:12,000
Little drops of rain.

235
00:19:12,000 --> 00:19:13,000
This is why it's discouraging,

236
00:19:13,000 --> 00:19:15,000
because each moment is just like a drop of rain.

237
00:19:15,000 --> 00:19:16,000
So people say, well,

238
00:19:16,000 --> 00:19:18,000
it's really doing anything.

239
00:19:18,000 --> 00:19:21,000
Just like the people who see the rain fall and say,

240
00:19:21,000 --> 00:19:24,000
it's not going to fly.

241
00:19:24,000 --> 00:19:30,000
And they get swept away by the flood.

242
00:19:30,000 --> 00:19:32,000
This is a good flood.

243
00:19:32,000 --> 00:19:33,000
It's a bit of a bad analogy,

244
00:19:33,000 --> 00:19:36,000
because this flood is actually,

245
00:19:36,000 --> 00:19:37,000
don't worry,

246
00:19:37,000 --> 00:19:40,000
the flood will come and sweep you away

247
00:19:40,000 --> 00:19:42,000
for you better,

248
00:19:42,000 --> 00:19:45,000
for your betterment.

249
00:19:45,000 --> 00:19:46,000
All right,

250
00:19:46,000 --> 00:19:48,000
so that's the number for tonight.

251
00:19:48,000 --> 00:19:54,000
That's a simple talk on a pamanda.

252
00:19:54,000 --> 00:19:56,000
But I'm going to remember,

253
00:19:56,000 --> 00:19:58,000
keep to the principles,

254
00:19:58,000 --> 00:19:59,000
where it's not,

255
00:19:59,000 --> 00:20:03,000
and it's good to see there's not that much we have to learn.

256
00:20:03,000 --> 00:20:05,000
You learn the force at the baton,

257
00:20:05,000 --> 00:20:07,000
and then you've got up the mud,

258
00:20:07,000 --> 00:20:10,000
and we're good to go.

259
00:20:10,000 --> 00:20:14,000
Okay, do we have any questions?

260
00:20:14,000 --> 00:20:17,000
Yes, we do.

261
00:20:17,000 --> 00:20:19,000
One person asking many, many questions,

262
00:20:19,000 --> 00:20:20,000
and I'm going to single you out,

263
00:20:20,000 --> 00:20:22,000
because as I said,

264
00:20:22,000 --> 00:20:24,000
the person who asks a lot of questions

265
00:20:24,000 --> 00:20:26,000
is often the sign that they're not meditating,

266
00:20:26,000 --> 00:20:30,000
so we're going to be,

267
00:20:30,000 --> 00:20:33,000
I'm going to keep close eye on those people

268
00:20:33,000 --> 00:20:34,000
who ask lots of questions,

269
00:20:34,000 --> 00:20:36,000
and we'll be especially hard on them.

270
00:20:43,000 --> 00:20:45,000
Sorry about my screen is loading.

271
00:20:45,000 --> 00:20:46,000
Should we just another moment?

272
00:20:46,000 --> 00:20:48,000
Let's see if I can...

273
00:20:48,000 --> 00:20:50,000
Yeah, I got the first one.

274
00:20:50,000 --> 00:20:51,000
These are actually good questions.

275
00:20:51,000 --> 00:20:52,000
I was being hard,

276
00:20:52,000 --> 00:20:54,000
and I was trying to go through and delete some,

277
00:20:54,000 --> 00:20:57,000
but I couldn't come up with any worth deleting,

278
00:20:57,000 --> 00:20:59,000
so that's a good sign.

279
00:21:01,000 --> 00:21:03,000
If the Buddha had created

280
00:21:03,000 --> 00:21:05,000
comprehensive teachings on rebirth,

281
00:21:05,000 --> 00:21:07,000
yet he had previously

282
00:21:07,000 --> 00:21:10,000
relinquished religious dogmatism and pursuit,

283
00:21:10,000 --> 00:21:15,000
could it be assumed he had an actual source for his information?

284
00:21:15,000 --> 00:21:20,000
Is it safe to just trust someone's unwieldy and lofty claims

285
00:21:20,000 --> 00:21:22,000
at how rebirth works?

286
00:21:22,000 --> 00:21:26,000
How would he have known upon his awakening?

287
00:21:26,000 --> 00:21:29,000
The Buddha saw it happening.

288
00:21:29,000 --> 00:21:32,000
He was able to use his mental powers

289
00:21:32,000 --> 00:21:35,000
to watch people being reborn and dying,

290
00:21:35,000 --> 00:21:38,000
and further was able to remember his own past lives.

291
00:21:38,000 --> 00:21:41,000
So that's the best way to be able to remember your own past lives

292
00:21:41,000 --> 00:21:44,000
and to be able to see with your mind

293
00:21:44,000 --> 00:21:47,000
and to watch how they'd be able to watch the spiritual plane

294
00:21:47,000 --> 00:21:49,000
of beings being born and dying

295
00:21:49,000 --> 00:21:51,000
and going according to their karma,

296
00:21:51,000 --> 00:21:53,000
which is incredibly instructive

297
00:21:53,000 --> 00:21:56,000
in terms of seeing the ways in which it works

298
00:21:56,000 --> 00:21:58,000
and how complicated it was, right?

299
00:21:58,000 --> 00:22:00,000
So he was able to see the complexity

300
00:22:00,000 --> 00:22:03,000
and the intricacies of how the mind works

301
00:22:03,000 --> 00:22:06,000
at the moment of death and how it leads to rebirth.

302
00:22:06,000 --> 00:22:09,000
That's the realm of magical powers.

303
00:22:09,000 --> 00:22:11,000
So most of us,

304
00:22:11,000 --> 00:22:13,000
most people will doubt that that's even possible

305
00:22:13,000 --> 00:22:17,000
and they'll never have a hope of realizing that power for themselves,

306
00:22:17,000 --> 00:22:22,000
but I mean, rebirth isn't something that takes faith.

307
00:22:22,000 --> 00:22:28,000
You know, if you take on the paradigm of experiential reality,

308
00:22:28,000 --> 00:22:31,000
where you see things in terms of the very,

309
00:22:31,000 --> 00:22:47,000
which turns out to be the very basis of the experience.

