1
00:00:00,000 --> 00:00:15,000
Next question from Charlie Lee 24, sometimes in my meditation I notice a feeling of itch or something and if I try to investigate it saying to myself itchy itchy it immediately goes away.

2
00:00:15,000 --> 00:00:21,000
When I return to the main object it comes back that bothers me a little, what should I do?

3
00:00:21,000 --> 00:00:33,000
Yeah, welcome to the world of meditation. Life has a rhythm to it and our problem is we're out of rhythm with life.

4
00:00:33,000 --> 00:00:44,000
Our inability to accept reality, that's what it means. It means we're not in tune, we're, we don't jive, we don't harmonize with the way things are.

5
00:00:44,000 --> 00:00:54,000
We can't accept reality. We generally think that we have some kind of control over reality and this is why we don't accept.

6
00:00:54,000 --> 00:01:08,000
We live our lives thinking that we're in control and that the objects of the sense belong to us and so we go about our lives trying to influence things.

7
00:01:08,000 --> 00:01:27,000
Now there is a process of cause and effect by which the mind is able to, on a microscopic scale, change things and so we get this idea that we're able to do that on a macroscopic level that we're able to say no more itching.

8
00:01:27,000 --> 00:01:38,000
We're able to say no more pain or so on. We're able to get the things that we want. But the reality of it is we're only able to move in a certain direction.

9
00:01:38,000 --> 00:01:47,000
We can build up to the point where there is no itching. You can develop yourself in a way that you're able to put off itching for some time.

10
00:01:47,000 --> 00:02:00,000
On the microscopic level you can see that when you say itching it goes away for a second. But that's a microscopic, they call it a process one intervention and quantum physics.

11
00:02:00,000 --> 00:02:10,000
So one of the things I'm reading about. But it's so miniscule that all you've done is put a decide for a second.

12
00:02:10,000 --> 00:02:24,000
Once you come back to the main object, it's come back. Now the point is not to build up some kind of state where in you're no longer itching.

13
00:02:24,000 --> 00:02:32,000
Because that in and of itself is still temporary. It has a certain power that you've built up in the brain to shut these things off.

14
00:02:32,000 --> 00:02:41,000
And it doesn't last because there's so many other factors, physical factors involved that it's unsustainable.

15
00:02:41,000 --> 00:02:54,000
The point is to stop living our lives in this way and stop trying to control things and to get ourselves to the point where we're simply observing.

16
00:02:54,000 --> 00:03:05,000
The problem is not the itching, the problem is the bothering. What you're seeing is the way things work. Reality is not under your control.

17
00:03:05,000 --> 00:03:15,000
What you're doing there is you're turning off the itching for a moment. You're altering the function of the brain so it doesn't experience the itch.

18
00:03:15,000 --> 00:03:28,000
And then turning that functioning off by going back to the rising and the following, the itch comes back. You go back, the itch goes, you come back, the itch comes back.

19
00:03:28,000 --> 00:03:38,000
And that's reality. That's the way things work. That's how the mind works. All you could do is force the itch away for a long time.

20
00:03:38,000 --> 00:03:47,000
Force the unpleasant situation away by building up these mind-states and these interventions.

21
00:03:47,000 --> 00:03:57,000
That's not an ultimate solution. The ultimate solution is to accept and to say, okay, now I'm itching.

22
00:03:57,000 --> 00:04:05,000
And if it goes, it comes. Practicing pretty much exactly the way you're doing it.

23
00:04:05,000 --> 00:04:16,000
And changing the way you react to it. Because you're practicing correctly. When you practice this way, you start to see your anger.

24
00:04:16,000 --> 00:04:24,000
You start to see your frustration, your inability to accept the way reality works or the way things are.

25
00:04:24,000 --> 00:04:38,000
As you see it again and again and again, and as you straighten out your mind, acknowledging as well frustrated, frustrated, angry, angry, upset, upset, you start to see that you're doing something that's creating suffering for yourself.

26
00:04:38,000 --> 00:04:53,000
You're reacting in a way that's unpleasant. Now no one wants to create suffering for themselves. And this is very true, even on an ultimate level of the brain that our minds don't want to create suffering for ourselves.

27
00:04:53,000 --> 00:05:06,000
Because we don't see things clearly. Once you show yourself again and again and again and prove to yourself that what you're doing is directly creating suffering for yourself by getting angry, by getting upset, you won't get upset anymore.

28
00:05:06,000 --> 00:05:19,000
Until eventually it's itching. It goes away. Come back rising. It comes back. You go back and say it's itching. And there's no frustration. There's no upset. You become in tune with reality and you're in tune with the rhythm.

29
00:05:19,000 --> 00:05:29,000
So it's like itching, itching, rising, falling. And it's not rhythmic. It's not like that. But as a course example, there is...

30
00:05:29,000 --> 00:05:37,000
Well, you have to think of it like a rhythm. That's the way it is. That's natural. In the same way that a rhythm is natural.

31
00:05:37,000 --> 00:05:50,000
And though it's not rhythmic, it is what it is. And that's reality. And you have two choices. You can always be trying to change it to be in some specific set way. This is the way it shouldn't be in no other way.

32
00:05:50,000 --> 00:06:01,000
Or you can accept everything for what it is. The path of Buddhism is to accept things as they are. So keep going. Your practice is fine. The problem is the frustration.

33
00:06:01,000 --> 00:06:09,000
And that will go as you acknowledge the object and as you acknowledge the frustration, saying to yourself, frustrated, frustrated, and itching.

34
00:06:09,000 --> 00:06:33,000
Your mind will ease up and you'll be able to see it just for one again. Thanks for the question.

