1
00:00:00,000 --> 00:00:06,400
Hi, the next question comes from minor players.

2
00:00:06,400 --> 00:00:11,480
You recommend a method of observing the breath, then labeling thoughts, pain, etc as they

3
00:00:11,480 --> 00:00:16,000
arise, and returning to the breath once those conditions subside.

4
00:00:16,000 --> 00:00:20,280
As one develops his practice, one also learns to solve his or her problems using this

5
00:00:20,280 --> 00:00:25,160
method.

6
00:00:25,160 --> 00:00:35,800
On the issue of problems, first of all, we have to understand that the word problem

7
00:00:35,800 --> 00:00:40,760
in and of itself is a judgment that we're saying that this is a problem, and that's going

8
00:00:40,760 --> 00:00:52,280
to be crucial in our solving of our problems or overcoming the state of having a problem.

9
00:00:52,280 --> 00:00:59,640
There are many problems which have to be solved, and so the short answer is yes, I think

10
00:00:59,640 --> 00:01:05,440
meditation can help you to solve many of life's problems, and basically how it does that

11
00:01:05,440 --> 00:01:12,600
is by giving you this clarity and being able to look at things objectively, seeing things

12
00:01:12,600 --> 00:01:21,800
as they are not becoming confused or distracted from the task at hand, but being able to

13
00:01:21,800 --> 00:01:28,440
put your full attention and have clear attention and clear understanding of the problem.

14
00:01:28,440 --> 00:01:39,280
But I think what you'll see clearer and more immediate is that you'll find yourself

15
00:01:39,280 --> 00:01:51,480
reevaluating a situation or various situations to the point where they no longer problems

16
00:01:51,480 --> 00:02:00,320
to you, where you can live with certain experiences, certain states that used to be a problem.

17
00:02:00,320 --> 00:02:07,840
For instance, maybe you have a physical ailment or maybe you have your work or your family

18
00:02:07,840 --> 00:02:17,480
or so on, something which is stressing you or causing you some kind of state of suffering.

19
00:02:17,480 --> 00:02:20,400
Through the meditation practice, you find that you're able to deal with it to the point

20
00:02:20,400 --> 00:02:21,920
that it doesn't bother you.

21
00:02:21,920 --> 00:02:28,320
I would say that that's much more direct, that yes, there are many puzzles that you

22
00:02:28,320 --> 00:02:33,680
have to figure out, many questions that you have to answer, and you'll find that the

23
00:02:33,680 --> 00:02:38,440
answer does come naturally through the meditation that you are able to answer many of

24
00:02:38,440 --> 00:02:45,560
life's questions, but in many more of the cases, you'll find that actually what meditation

25
00:02:45,560 --> 00:02:49,000
does is help you to see that it wasn't a problem in the first place.

26
00:02:49,000 --> 00:02:54,280
The word problem is something that arose in your mind, and once you got rid of that, you're

27
00:02:54,280 --> 00:02:57,760
able to see it for what it is, and you're able to accept it for what it is, and you're

28
00:02:57,760 --> 00:03:01,480
able to accept a lot more things than before.

29
00:03:01,480 --> 00:03:15,160
You're able to accept your life as it is and not need to solve it.

30
00:03:15,160 --> 00:03:19,440
The other thing that meditation does is help you avoid problems.

31
00:03:19,440 --> 00:03:26,600
It helps you give up many of your problems as opposed to finding an answer for them, so

32
00:03:26,600 --> 00:03:32,760
much of the way we live our lives is inherently problematic, and I would say that another

33
00:03:32,760 --> 00:03:37,880
thing that meditation does is help you to simplify your life, to give up activities,

34
00:03:37,880 --> 00:03:47,760
to give up behaviors which are inconclusive towards peace, happiness, and freedom from

35
00:03:47,760 --> 00:03:54,400
suffering, so there are problems that meditation might solve, there are problems that

36
00:03:54,400 --> 00:03:58,000
meditation helps you to realize there are not problems, and then there are problems

37
00:03:58,000 --> 00:04:06,240
which you'll just give up, which you'll solve by not engaging in the activity which brings

38
00:04:06,240 --> 00:04:11,720
about the problem, so I hope that's a useful answer, thanks for the question.

