1
00:00:00,000 --> 00:00:22,000
Good afternoon, everyone, to a afternoon session in second life on this fine Sunday afternoon.

2
00:00:22,000 --> 00:00:36,000
Today's topic, I'm not exactly sure what to call it, although the impetus for talking

3
00:00:36,000 --> 00:00:50,000
about the subject of today's talk comes from a number of people, a small number of people,

4
00:00:50,000 --> 00:01:02,000
a few people who have contacted me on YouTube.

5
00:01:02,000 --> 00:01:11,000
I assume it because of my white skin and my shaved head.

6
00:01:11,000 --> 00:01:20,000
And ask me if I'm a skin head or a white supremacist.

7
00:01:20,000 --> 00:01:28,000
And making all sorts of rude comments besides the normal response, of course,

8
00:01:28,000 --> 00:01:35,000
is to block such people since I don't have time for the silliness.

9
00:01:35,000 --> 00:01:45,000
But I think having thought about it seems to be an interesting entry point to giving a talk.

10
00:01:45,000 --> 00:01:53,000
So I guess you could say that today's talk is about white supremacy, although it's probably going to diverge from that.

11
00:01:53,000 --> 00:02:07,000
And the thing about such identification questions, are you this, are you that, is they have a place in the Buddhist teaching.

12
00:02:07,000 --> 00:02:14,000
The Buddha often got this sort of a question from people.

13
00:02:14,000 --> 00:02:28,000
More accusations from people saying that the Buddha was a nihilist, that the Buddha taught inaction,

14
00:02:28,000 --> 00:02:40,000
that, and the people asking the Buddha whether he was a Brahmin or a seminar or so on.

15
00:02:40,000 --> 00:03:00,000
The Buddha would more often than not redefine the term.

16
00:03:00,000 --> 00:03:12,000
A incredibly good example that's quite pertinent to this topic is the term area.

17
00:03:12,000 --> 00:03:18,000
Many people should be familiar with the Aryans.

18
00:03:18,000 --> 00:03:38,000
Mostly we come in contact with this word in relation to Nazi Germany and their idea of creating the pure, perfect, Aryan race.

19
00:03:38,000 --> 00:03:51,000
But the origin of the term, the oldest known usage that I'm aware of, comes from India, or comes from a group of people who invaded India.

20
00:03:51,000 --> 00:04:05,000
It was a group of horse riding barbarians who called themselves the Aryans and they had war gods and war hymns,

21
00:04:05,000 --> 00:04:08,000
much like the Vikings.

22
00:04:08,000 --> 00:04:22,000
You might think of it as horse riding Vikings or thinking of the Genghis Khan or Harbran, I'm so familiar with all of that.

23
00:04:22,000 --> 00:04:33,000
But anyway, this group of warriors of people who believed in conquest and domination.

24
00:04:33,000 --> 00:04:39,000
They came into India and they called themselves the Aryans.

25
00:04:39,000 --> 00:04:51,000
They were rather white skinned, as soon as they saw these dark skinned inhabitants of the Indian subcontinent,

26
00:04:51,000 --> 00:04:55,000
and even got to the point of calling them demons.

27
00:04:55,000 --> 00:05:00,000
These are dark beings.

28
00:05:00,000 --> 00:05:09,000
The word Aryan does, in fact, have a fairly negative connotation.

29
00:05:09,000 --> 00:05:29,000
It's a term used for a group of people who believe that people with light skin are superior or more than light skin.

30
00:05:29,000 --> 00:05:39,000
Extrapolate that blue eyes, blonde hair, and so on.

31
00:05:39,000 --> 00:05:46,000
Tall.

32
00:05:46,000 --> 00:05:52,000
And so the Buddha redefined the word Arya, where it probably comes from, it probably means something like victor,

33
00:05:52,000 --> 00:06:06,000
because Arya means enemy, and Yea means one who conquers or overcomes.

34
00:06:06,000 --> 00:06:12,000
So the Buddha redefined the word to mean one who overcomes the real enemy,

35
00:06:12,000 --> 00:06:23,000
which is the defound, or the defoundments inside.

36
00:06:23,000 --> 00:06:34,000
They're skin color, or their birth, or their eye color, or something ridiculous like that.

37
00:06:34,000 --> 00:06:43,000
You don't call a person Aryan simply because you don't call them a victor or a noble one,

38
00:06:43,000 --> 00:06:51,000
because of their birth, you call them that because they have destroyed their defoundments.

39
00:06:51,000 --> 00:06:54,000
They have killed the enemy.

40
00:06:54,000 --> 00:07:02,000
They have destroyed greed, they have destroyed anger, they have destroyed delusion.

41
00:07:02,000 --> 00:07:20,000
And the Pali is Gileus Aryo, one who kills the enemies, which are the Gileus of the defoundments.

42
00:07:20,000 --> 00:07:28,000
This is known as Arya.

43
00:07:28,000 --> 00:07:43,000
And you might say that, oh, this is just fanciful entomology, creating new meanings to words that are totally unrelated to the meaning.

44
00:07:43,000 --> 00:07:52,000
But from a Buddhist point of view, it seems absurd to think of anyone as noble simply because of their birth.

45
00:07:52,000 --> 00:08:09,000
And the Buddha was constantly denouncing this idea that you could be somehow high-born because of your class or because of your skin color.

46
00:08:09,000 --> 00:08:26,000
He said, if a high class of Brahmin or a Cassatia, a royal royalty, commits a bad deed, kills someone or so on.

47
00:08:26,000 --> 00:08:30,000
Do they not put that person in jail?

48
00:08:30,000 --> 00:08:41,000
And is it not true that that person will have to suffer because of their bad deeds?

49
00:08:41,000 --> 00:08:50,000
And this might seem a little bit silly in talking about the class system because obviously in our society we don't have this class system.

50
00:08:50,000 --> 00:09:04,000
But here we have, even today, these people who claim that they are white supremacists, they believe in the supremacy of whiteness.

51
00:09:04,000 --> 00:09:13,000
And their definition or their way of telling whether someone is white or not is based on the color of their skin.

52
00:09:13,000 --> 00:09:23,000
And I was fooling around with the color of my skin here in second life.

53
00:09:23,000 --> 00:09:29,000
And I'll just see if I can get in here.

54
00:09:29,000 --> 00:09:33,000
That's kind of a silly thing, but here's a black person.

55
00:09:33,000 --> 00:09:41,000
I was looking at it, and I can't see that I've ever seen anyone with black skin.

56
00:09:41,000 --> 00:09:48,000
And I even looked it up on Wikipedia and apparently the closest you can get is close to black.

57
00:09:48,000 --> 00:09:51,000
This first skin color.

58
00:09:51,000 --> 00:10:03,000
Now I have seen people with almost white skin, but there are of course few and far between.

59
00:10:03,000 --> 00:10:19,000
But most of us have some shade of brown and maybe some pink thrown in there.

60
00:10:19,000 --> 00:10:42,000
So I think this is an important topic to discuss because it goes into the whole idea of identifying with things and giving labels to, especially to people, to yourself and others.

61
00:10:42,000 --> 00:10:52,000
So you have people who give themselves the label white or they give themselves the label black.

62
00:10:52,000 --> 00:10:55,000
And then they identify with it.

63
00:10:55,000 --> 00:11:07,000
They talk about people as being not black enough, not too white or so on.

64
00:11:07,000 --> 00:11:16,000
They talk about white culture, white trash, black culture.

65
00:11:16,000 --> 00:11:20,000
And like everything else, this separates us into groups.

66
00:11:20,000 --> 00:11:28,000
It divides people on ridiculous absurd grounds.

67
00:11:28,000 --> 00:11:35,000
And it comes because we see what is not essential as being essential.

68
00:11:35,000 --> 00:11:48,000
So like with Hitler, he believed that somehow there was something good about being blonde, blue-eyed, white skinned, tall, muscular, and whatever.

69
00:11:48,000 --> 00:11:49,000
German again.

70
00:11:49,000 --> 00:11:59,000
I'm already in this group of European people who wrote horses and conquered.

71
00:11:59,000 --> 00:12:05,000
And it actually turned out to be probably one of the most evil groups of people on the earth.

72
00:12:05,000 --> 00:12:12,000
That would be the same people who were the same idea.

73
00:12:12,000 --> 00:12:26,000
The same culture that ended up turning into British imperialism, the conquering of the quote-unquote new world, which of course already had advanced or well-established civilizations.

74
00:12:26,000 --> 00:12:41,000
And most of the rest of the known world, Africa, India, and Asia, and bringing an incredible amount of suffering to the rest of the world.

75
00:12:41,000 --> 00:12:46,000
This is what that group managed to accomplish.

76
00:12:46,000 --> 00:13:06,000
The taking over of India as an example and killing off, in fact, a well-established civilization that we now know very little about because they were annihilated or pushed out into the woods, into the countryside, out of the cities.

77
00:13:06,000 --> 00:13:14,000
It's a little known fact that there were cities in India before the Aryans came.

78
00:13:14,000 --> 00:13:19,000
And so it's mind-boggling that people can say that these people are somehow supreme.

79
00:13:19,000 --> 00:13:27,000
But people do that, and this is because we have some bizarre ideas of what is right and what is wrong.

80
00:13:27,000 --> 00:13:34,000
Often, having to do with the concept of might makes right.

81
00:13:34,000 --> 00:13:46,000
And what helps you is good for oneself.

82
00:13:46,000 --> 00:13:59,000
On a material level, what brings one more power, more affluence is a essential, beneficial, positive thing.

83
00:13:59,000 --> 00:14:04,000
And so we create all sorts of in-groups and out-groups. White and black is just one of them.

84
00:14:04,000 --> 00:14:10,000
I think it's silly, really. It's incredibly silly because as I said, none of us are white and none of us are black.

85
00:14:10,000 --> 00:14:17,000
There's only these labels that we give and these separations.

86
00:14:17,000 --> 00:14:20,000
In fact, skin color is quite diverse.

87
00:14:20,000 --> 00:14:30,000
Then we call people red, the red Indians, as they used to be called, the yellow people from Asia.

88
00:14:30,000 --> 00:14:38,000
I think eclipse are because there's no red, there were no red people here in America.

89
00:14:38,000 --> 00:14:42,000
And I've never seen a yellow person in China.

90
00:14:42,000 --> 00:14:49,000
But we immediately want to label. It's like when we see someone, we immediately want to decide whether they're male or female.

91
00:14:49,000 --> 00:14:57,000
This is the first thing that we do. And if they happen to be a person who doesn't have very strong male or female characteristics,

92
00:14:57,000 --> 00:15:02,000
we find it quite troubling in the mind because we write a way we want to characterize them.

93
00:15:02,000 --> 00:15:14,000
This is what our mind was to do. Then we segregate people, we segregate reality, we separate things and immediately make judgments about them.

94
00:15:14,000 --> 00:15:19,000
It has a devastating effect on the world, really.

95
00:15:19,000 --> 00:15:34,000
Of course, the white black issue is probably not so big anymore, except with these strange people who call themselves white supremacists.

96
00:15:34,000 --> 00:15:44,000
But it points to a bigger problem of separation and categorization in general, how we categorize people.

97
00:15:44,000 --> 00:15:58,000
A bigger one is religion. Right now, religion is probably one of the most devastating, divisive influences that there is.

98
00:15:58,000 --> 00:16:07,000
I can't have a dialogue with a civil conversation with a Christian because they see me as a Buddhist missionary.

99
00:16:07,000 --> 00:16:27,000
When we talk, we can be civil enough, but there's an undercurrent of pressure and normally a thought process in their mind of how they can best work to convert me to Christianity because I'm on the wrong path.

100
00:16:27,000 --> 00:16:35,000
That's an example. And the same goes with other religions when I talk to Jewish people.

101
00:16:35,000 --> 00:16:42,000
There's a sense that I'm outside, I'm foreign.

102
00:16:42,000 --> 00:16:56,000
It's even in a sense because I used to subscribe to some of the tendencies of Judaism that I've committed apostasy and so on.

103
00:16:56,000 --> 00:17:06,000
This isn't exactly what I wanted to talk about, but it leads into what I wanted to talk about and that is this interesting teaching by the Buddha about white and black.

104
00:17:06,000 --> 00:17:21,000
I wanted to preface with this because when we talk about white and black and Buddhism, I was confronted on this by a black friend with dark skin who came to me and he said,

105
00:17:21,000 --> 00:17:27,000
why do we wear, why do the Buddhist meditators wear white?

106
00:17:27,000 --> 00:17:40,000
And it was clear that he understood the pejorative attachment that we give to black and the positive characteristics we give to white.

107
00:17:40,000 --> 00:17:50,000
That's the idea being that white is somehow pure, it was considered so in India.

108
00:17:50,000 --> 00:18:01,000
So I think it's important to say that it's important to express the belief, the understanding that there are no black people,

109
00:18:01,000 --> 00:18:04,000
no people with black skin or white skin.

110
00:18:04,000 --> 00:18:13,000
And I think if we can get over that then we can get on to talking about what is really white and what is really black.

111
00:18:13,000 --> 00:18:21,000
And we can overcome these barriers, this divisiveness that is always separating us.

112
00:18:21,000 --> 00:18:29,000
When we come to see the color white, there's nothing pure about it at all, it's a color.

113
00:18:29,000 --> 00:18:33,000
The color black, there's nothing dirty about it as well.

114
00:18:33,000 --> 00:18:41,000
But in India they separate the two, they say that white is pure and black being dirty.

115
00:18:41,000 --> 00:18:47,000
And that's because in terms of cloth it actually is the case that pure cloth is white.

116
00:18:47,000 --> 00:18:54,000
Cotton cloth in India would be white and when it touches with the soil of the earth it gets darker.

117
00:18:54,000 --> 00:19:05,000
So the extrapolation from that is that white is pure and dark is somehow bad.

118
00:19:05,000 --> 00:19:11,000
But of course the opposite doesn't, or the same doesn't hold with skin color.

119
00:19:11,000 --> 00:19:22,000
And in fact the funny thing about skin color is that, well anyway, we all come from the same ancestor.

120
00:19:22,000 --> 00:19:30,000
The understanding is that we're all from the African continent and have branched off and due to geographic isolation.

121
00:19:30,000 --> 00:19:40,000
Our skin color is changed due to the amount of sunlight that we get, the altitude and so on.

122
00:19:40,000 --> 00:19:44,000
So what is white and what is black and Buddhism?

123
00:19:44,000 --> 00:19:47,000
I think if you know anything about Buddhism it's pretty easy to answer.

124
00:19:47,000 --> 00:19:53,000
Obviously what is white and black is our actions.

125
00:19:53,000 --> 00:20:12,000
And the Buddha used these words living in India with this understanding that people had that white is pure and black is impure.

126
00:20:12,000 --> 00:20:21,000
White is positive, black is negative.

127
00:20:21,000 --> 00:20:29,000
And so the Buddha said that there are four types of karma.

128
00:20:29,000 --> 00:20:31,000
There's white karma.

129
00:20:31,000 --> 00:20:35,000
And white karma with white results.

130
00:20:35,000 --> 00:20:38,000
Black karma with black results.

131
00:20:38,000 --> 00:20:42,000
White and black karma with white and black results.

132
00:20:42,000 --> 00:20:52,000
And neither white nor black karma with neither white nor black results.

133
00:20:52,000 --> 00:20:57,000
And again he's just using these words in a standard Indian fashion.

134
00:20:57,000 --> 00:21:05,000
And it probably is based somewhat on these prejudices that people had for light colored skin.

135
00:21:05,000 --> 00:21:24,000
And versus dark colored skin because the predominant culture at the time was the Brahmin culture based on, based on the Aryan invasion.

136
00:21:24,000 --> 00:21:26,000
So what is white karma?

137
00:21:26,000 --> 00:21:27,000
What is black karma?

138
00:21:27,000 --> 00:21:29,000
What is white and black karma?

139
00:21:29,000 --> 00:21:30,000
And neither white nor black.

140
00:21:30,000 --> 00:21:38,000
Because the interesting thing here in the beginning I was going to try to formulate this talk to say, yes, I am a white supremacist.

141
00:21:38,000 --> 00:21:47,000
And then explain it from there that because I believe in the performance of white karma.

142
00:21:47,000 --> 00:21:49,000
White deeds, pure deeds.

143
00:21:49,000 --> 00:21:57,000
When we perform a deed with pure heart, just like a white cloth.

144
00:21:57,000 --> 00:22:00,000
So a mind that is unsullied by defilements.

145
00:22:00,000 --> 00:22:05,000
Is it because the mind in its ordinary state is undefiled?

146
00:22:05,000 --> 00:22:15,000
The Buddha said that the mind is luminescent, is brilliant, is bright, is white.

147
00:22:15,000 --> 00:22:28,000
It becomes defiled by defilements which wander in or sort of like guests.

148
00:22:28,000 --> 00:22:31,000
Aren't intrinsic to the mind?

149
00:22:31,000 --> 00:22:41,000
Their extrinsic states, just like dirt is not intrinsic to the cloth.

150
00:22:41,000 --> 00:22:48,000
But then when I started looking at it, I realized, oh, that's right, you can't say that.

151
00:22:48,000 --> 00:22:51,000
And I'll explain from Buddhist point of view, we aren't white.

152
00:22:51,000 --> 00:22:56,000
So we don't believe in white karma as the supreme.

153
00:22:56,000 --> 00:23:01,000
White karma is just any deed that we do that leads to happiness.

154
00:23:01,000 --> 00:23:05,000
You notice that I said white karma with white results, black karma with black results.

155
00:23:05,000 --> 00:23:15,000
When you think in the standard Buddhist way of working for completeness in categorization,

156
00:23:15,000 --> 00:23:27,000
that there would also be black karma with white results and white karma with black results.

157
00:23:27,000 --> 00:23:30,000
But there isn't.

158
00:23:30,000 --> 00:23:35,000
There's only good results that come from good deeds.

159
00:23:35,000 --> 00:23:43,000
No bad result can come from a good deed and no bad result can come from a good deed.

160
00:23:43,000 --> 00:23:52,000
No good result can come from a bad deed.

161
00:23:52,000 --> 00:24:01,000
When someone kills or steals or lies or cheats, it does something that has bad results.

162
00:24:01,000 --> 00:24:07,000
Then it's considered a bad deed, considered a black karma.

163
00:24:07,000 --> 00:24:12,000
A karma that is black.

164
00:24:12,000 --> 00:24:17,000
If someone refrains from these things or performs a deed that is for someone's benefit,

165
00:24:17,000 --> 00:24:21,000
either oneself or others, then this is a white karma.

166
00:24:21,000 --> 00:24:29,000
This is a deed that is considered to be beneficial or positive, white pure.

167
00:24:29,000 --> 00:24:31,000
Because the mind is pure.

168
00:24:31,000 --> 00:24:40,000
When we help other people, when we are generous and kind, when we support others and encourage them

169
00:24:40,000 --> 00:24:50,000
and teach them and help them to overcome their suffering, when we give people support and help.

170
00:24:50,000 --> 00:24:56,000
When we do good things for ourselves, when we purify our minds, when we practice meditation,

171
00:24:56,000 --> 00:25:03,000
when we see things as they are, when we understand reality, this is white.

172
00:25:03,000 --> 00:25:17,000
In a sense, white karma, when we are making our minds more pure, our minds are becoming more unsullied.

173
00:25:17,000 --> 00:25:20,000
So black karma has black results.

174
00:25:20,000 --> 00:25:22,000
White karma has white results.

175
00:25:22,000 --> 00:25:32,000
White karma that is both black and white is there to show us that,

176
00:25:32,000 --> 00:25:37,000
you know, whereas you can't do a purely good deed and get bad results from it.

177
00:25:37,000 --> 00:25:40,000
Sometimes we do a deed that has both good and bad results.

178
00:25:40,000 --> 00:25:49,000
For instance, you might have good intentions, but the nature of the deed forces the arising of bad thoughts,

179
00:25:49,000 --> 00:25:51,000
of harmful intentions.

180
00:25:51,000 --> 00:26:01,000
For instance, when in the case of euthanasia, people say they have good intention,

181
00:26:01,000 --> 00:26:11,000
they don't want the person to suffer, but the nature of the deed is such that it can't

182
00:26:11,000 --> 00:26:17,000
help but give rise to both the incredible states of delusion that are required to believe

183
00:26:17,000 --> 00:26:20,000
that you are not hurting the person.

184
00:26:20,000 --> 00:26:32,000
And the anger-based state required to end someone's life.

185
00:26:32,000 --> 00:26:38,000
You can't possibly kill someone without giving rise to at least the state of delusion,

186
00:26:38,000 --> 00:26:40,000
and most likely the state of anger as well.

187
00:26:40,000 --> 00:26:48,000
Because you have to crush the natural compassion that exists in the mind,

188
00:26:48,000 --> 00:26:52,000
the empathy where you yourself don't want to suffer,

189
00:26:52,000 --> 00:26:54,000
and you have to cause this suffering for others.

190
00:26:54,000 --> 00:27:01,000
Mostly unknowing and realizing that with the cloud of delusion

191
00:27:01,000 --> 00:27:11,000
that stops you from realizing the suffering that you're causing for the person.

192
00:27:11,000 --> 00:27:15,000
But there is, on the other hand, there are some good intentions.

193
00:27:15,000 --> 00:27:21,000
There is a sort of ignorant belief that somehow you're helping the person.

194
00:27:21,000 --> 00:27:25,000
The same is true when we do good deeds, when you give something,

195
00:27:25,000 --> 00:27:30,000
when you suppose you give charity, but you do it in such a way.

196
00:27:30,000 --> 00:27:35,000
Many people in religious circles will give, but then they want something back,

197
00:27:35,000 --> 00:27:39,000
or they still cling to it.

198
00:27:39,000 --> 00:27:49,000
When people give gifts to church, or to a religious organization,

199
00:27:49,000 --> 00:27:52,000
and then they're always watching to see how their money is spent,

200
00:27:52,000 --> 00:27:57,000
or how the items are used.

201
00:27:57,000 --> 00:28:02,000
And so there's still this attachment that comes with it.

202
00:28:02,000 --> 00:28:07,000
There's also often the case where when you give everyone else to see you giving,

203
00:28:07,000 --> 00:28:10,000
and to know that you gave, and to know what a great person you are,

204
00:28:10,000 --> 00:28:14,000
this is really the case with all of our good deeds.

205
00:28:14,000 --> 00:28:17,000
That there's always some level of ego involved.

206
00:28:17,000 --> 00:28:25,000
As long as we have ego in our minds, an attachment to self,

207
00:28:25,000 --> 00:28:33,000
that will always give rise to some sense of self-worth,

208
00:28:33,000 --> 00:28:42,000
feeling like, feeling proud of ourselves, have you done that?

209
00:28:42,000 --> 00:28:44,000
So these are three types of karma.

210
00:28:44,000 --> 00:28:53,000
The fourth type of karma is actually karma for the purpose of ending karma.

211
00:28:53,000 --> 00:29:04,000
And so Buddhism is often referred to as overcoming good and evil,

212
00:29:04,000 --> 00:29:10,000
because contrary to popular belief, I think,

213
00:29:10,000 --> 00:29:19,000
the Buddha didn't teach the supremacy of good karma, of doing good deeds,

214
00:29:19,000 --> 00:29:22,000
but without the supremacy of giving up.

215
00:29:22,000 --> 00:29:25,000
And you need to do either good or bad deeds.

216
00:29:25,000 --> 00:29:30,000
And this is karma that does neither white nor black,

217
00:29:30,000 --> 00:29:33,000
and has neither white nor black nor souls.

218
00:29:33,000 --> 00:29:36,000
In other words, it's action that doesn't give rise to results,

219
00:29:36,000 --> 00:29:45,000
because it's free from the intention of giving rise to any results.

220
00:29:45,000 --> 00:29:51,000
There's no intention when one performs such a karma for any results to arise.

221
00:29:51,000 --> 00:29:56,000
And the point being that if you perform a deed

222
00:29:56,000 --> 00:29:59,000
intending that it has a good result,

223
00:29:59,000 --> 00:30:05,000
then you'll always be conditioned for the creation of that result.

224
00:30:05,000 --> 00:30:10,000
There'll always be a future when you give,

225
00:30:10,000 --> 00:30:13,000
thinking that it's going to be good in this way or that way,

226
00:30:13,000 --> 00:30:17,000
then maybe it's something that will make you happy in this life,

227
00:30:17,000 --> 00:30:20,000
maybe it's something that leads you to heaven in the next life.

228
00:30:20,000 --> 00:30:28,000
Then it's something that still is bound up in clinging,

229
00:30:28,000 --> 00:30:34,000
and still will give rise to future results.

230
00:30:34,000 --> 00:30:37,000
The practice of the eightfold noble path,

231
00:30:37,000 --> 00:30:41,000
the Buddha's path of morality, of concentration and wisdom,

232
00:30:41,000 --> 00:30:46,000
is considered to be neither white nor black karma,

233
00:30:46,000 --> 00:30:49,000
because it's something that frees the mind from the need to do good or bad.

234
00:30:49,000 --> 00:30:57,000
They need to do good or bad, and they need to do good or bad deeds.

235
00:30:57,000 --> 00:31:04,000
And this is because we're able to give up our categorization and our judgment,

236
00:31:04,000 --> 00:31:10,000
our diversification of phenomena, things that arise.

237
00:31:10,000 --> 00:31:16,000
We see them simply for what they are, and we don't give any label to them.

238
00:31:16,000 --> 00:31:21,000
We don't take them to require any action on our parts.

239
00:31:21,000 --> 00:31:30,000
We don't take them to mean anything in terms of an impetus

240
00:31:30,000 --> 00:31:42,000
for performing future karma.

241
00:31:42,000 --> 00:31:52,000
So in essence, it's changing our nature from one of cause and effect,

242
00:31:52,000 --> 00:31:57,000
past, present and future,

243
00:31:57,000 --> 00:32:05,000
to one that is more stationary in terms of being with reality as it is.

244
00:32:05,000 --> 00:32:13,000
Not thinking this phenomenon, that phenomenon

245
00:32:13,000 --> 00:32:20,000
is a cause for me to work for something in the future,

246
00:32:20,000 --> 00:32:25,000
to try to get away from bad things or try to develop good things.

247
00:32:25,000 --> 00:32:30,000
But instead, changing our whole worldview

248
00:32:30,000 --> 00:32:35,000
to being one of acceptance and contentment in the present moment,

249
00:32:35,000 --> 00:32:37,000
not needing anything.

250
00:32:37,000 --> 00:32:42,000
No longer trying to find happiness in things external to ourselves.

251
00:32:42,000 --> 00:32:55,000
No longer trying to change things to suit our idea of how they should be.

252
00:32:55,000 --> 00:32:59,000
No longer saying to ourselves, this is good and this is acceptable.

253
00:32:59,000 --> 00:33:06,000
This is supreme, this is noble, this is high, this is low,

254
00:33:06,000 --> 00:33:18,000
this is ignoble, this is unacceptable.

255
00:33:18,000 --> 00:33:24,000
So I would have to say that the conclusion is that no,

256
00:33:24,000 --> 00:33:28,000
I'm not a white supremacist.

257
00:33:28,000 --> 00:33:33,000
Because I do understand at least intellectually what the Buddha was talking about,

258
00:33:33,000 --> 00:33:36,000
and I practice according to this,

259
00:33:36,000 --> 00:33:44,000
that it's actually not the practice of building up white karma, building up good karma.

260
00:33:44,000 --> 00:33:52,000
It's the practice of tearing down and letting go as I talked about last time I think last Wednesday.

261
00:33:52,000 --> 00:34:00,000
The Buddha's teaching is for the purpose of tearing down, not building up.

262
00:34:00,000 --> 00:34:05,000
That's for the purpose of letting go and not holding on.

263
00:34:05,000 --> 00:34:17,000
And so even good deeds, this is the classic Buddhist paradox or sorts,

264
00:34:17,000 --> 00:34:30,000
that even good deeds we have to give up, we perform good deeds for the purpose of giving up good deeds.

265
00:34:30,000 --> 00:34:43,000
And it's a dilemma of sorts that often leads people to Buddhists to claim that there's no reason

266
00:34:43,000 --> 00:34:49,000
there's no good and there's no use in good doing good deeds,

267
00:34:49,000 --> 00:34:58,000
in giving charity or in teaching others or in setting up meditation centers or so on,

268
00:34:58,000 --> 00:35:12,000
distributing teachings, working in hospitals, working with homeless shelters or so on.

269
00:35:12,000 --> 00:35:20,000
But these things are just the accumulation of good deeds.

270
00:35:20,000 --> 00:35:28,000
And this is a mistaken conclusion that people come to in regards to this teaching.

271
00:35:28,000 --> 00:35:33,000
And the belief that, okay, that means that we have to stop all of these things.

272
00:35:33,000 --> 00:35:40,000
And the truth is that in light and being is much more inclined to perform such deeds.

273
00:35:40,000 --> 00:35:43,000
The difference is that they do it unconditionally.

274
00:35:43,000 --> 00:35:48,000
They would do it without any interest for future results.

275
00:35:48,000 --> 00:35:51,000
They do it because it's the most harmonious thing.

276
00:35:51,000 --> 00:35:56,000
They perform good deeds, quote unquote, good deeds,

277
00:35:56,000 --> 00:36:04,000
because they have gained the wisdom and understanding that there is an intrinsic difference between good and bad deeds.

278
00:36:04,000 --> 00:36:10,000
That a good deed is much easier and more harmonious.

279
00:36:10,000 --> 00:36:15,000
It leads to greater harmony when performed than a quote unquote, bad deed.

280
00:36:15,000 --> 00:36:20,000
And that's why they're given the label of good and bad deeds.

281
00:36:20,000 --> 00:36:29,000
And this goes back to the labels that are given to them in terms of white and black.

282
00:36:29,000 --> 00:36:43,000
The white being akin to the pure cloth and black or dark being solid.

283
00:36:43,000 --> 00:36:49,000
The reason that good deeds are intrinsic, the difference is because they're simply a deed that is free from defilement.

284
00:36:49,000 --> 00:37:01,000
And done without any thought of personal gain or attachment or clinging or identification of self.

285
00:37:01,000 --> 00:37:07,000
That a person who hasn't under this clinging naturally performs good deeds.

286
00:37:07,000 --> 00:37:15,000
So for all of us who are still in the, at the point where we have defilements in our minds,

287
00:37:15,000 --> 00:37:22,000
we should try our best to conform with this sort of behavior of helping people when asked,

288
00:37:22,000 --> 00:37:28,000
and supporting others, spreading the Buddhist teachings, setting up meditation courses and so on.

289
00:37:28,000 --> 00:37:36,000
Because these are the things that are going to lead us closer and closer to our harmonious, peaceful state of existence.

290
00:37:36,000 --> 00:37:43,000
We are not doing good deeds for the sake of the good deeds or for some result.

291
00:37:43,000 --> 00:37:46,000
We're doing them to help us to let go.

292
00:37:46,000 --> 00:37:51,000
When you give something, it's not because you think, oh, I hope this person gets benefit from it.

293
00:37:51,000 --> 00:37:59,000
If you do that, then you're still clinging and you'll always be upset when they maybe don't get benefit from it.

294
00:37:59,000 --> 00:38:07,000
If you give something, you give money to a beggar on the street and then you see them take that money and go and buy alcohol.

295
00:38:07,000 --> 00:38:13,000
And right away you're upset because your intention was that it should benefit them.

296
00:38:13,000 --> 00:38:20,000
Your intention was that it should have some specific results.

297
00:38:20,000 --> 00:38:23,000
And when it doesn't, you become upset.

298
00:38:23,000 --> 00:38:32,000
And Buddhism, when we give, we give to give up and we give to support a state of existence that allows us to practice.

299
00:38:32,000 --> 00:38:38,000
When we support meditation centers or when we support places like the Buddha Center.

300
00:38:38,000 --> 00:38:47,000
We're doing so because we believe that this is something that is bringing us and other people closer to reality.

301
00:38:47,000 --> 00:39:01,000
When we support such places, such organizations that were creating more harmony than the alternative.

302
00:39:01,000 --> 00:39:16,000
The alternative of not supporting and not assisting in the development and expansion of such places.

303
00:39:16,000 --> 00:39:27,000
So just that important point that while we're not focused on good deeds, we certainly have to align ourselves in that direction.

304
00:39:27,000 --> 00:39:34,000
Because when someone comes and asks you for something, ask you for support and you don't help them at all.

305
00:39:34,000 --> 00:39:38,000
It creates incredible states of defilement in the mind.

306
00:39:38,000 --> 00:39:46,000
Feelings of guilt, feelings of stinginess, feelings of anger or however.

307
00:39:46,000 --> 00:39:56,000
And so it behooves us to give unconditionally.

308
00:39:56,000 --> 00:40:02,000
When someone comes and asks us for a hundred dollars, maybe give them five dollars.

309
00:40:02,000 --> 00:40:10,000
Someone comes and asks us to teach them about the Buddha's teaching.

310
00:40:10,000 --> 00:40:12,000
Well, say something.

311
00:40:12,000 --> 00:40:15,000
Give them the basics.

312
00:40:15,000 --> 00:40:22,000
If you don't know how to teach or if you don't have time or so on, always be giving when people ask.

313
00:40:22,000 --> 00:40:32,000
When you're confronted with an opportunity, use it as a chance to create this harmonious state of existence.

314
00:40:32,000 --> 00:40:34,000
It's something that supports our practice.

315
00:40:34,000 --> 00:40:43,000
And we should always be finding this happy medium where we're not looking and going out of our way to help people,

316
00:40:43,000 --> 00:40:56,000
which can become incredibly obnoxious and is often undesired by the people, by the recipients.

317
00:40:56,000 --> 00:41:02,000
But rather simply doing it as a matter of course means of not clinging.

318
00:41:02,000 --> 00:41:12,000
The simplest way out of the situation to support, to give, to bring yourself closer to a state of peace, happiness and tranquility.

319
00:41:12,000 --> 00:41:16,000
So that's some, I think that's some dharma for today.

320
00:41:16,000 --> 00:41:18,000
Food for thought.

321
00:41:18,000 --> 00:41:29,000
Hopefully something that can help us to come closer to a meditation practice and help us to focus our minds in the meditation practice,

322
00:41:29,000 --> 00:41:38,000
in terms of seeing that we're not trying to gain any state even in the meditation practice of peace or happiness.

323
00:41:38,000 --> 00:41:44,000
Even meditation, we're not practicing to gain anything. We're practicing to let go.

324
00:41:44,000 --> 00:41:47,000
To give up.

325
00:41:47,000 --> 00:41:49,000
So that's the talk for today.

326
00:41:49,000 --> 00:41:52,000
I'd like to thank you all for coming if you've got any questions.

327
00:41:52,000 --> 00:42:16,000
I'm happy to ask them otherwise. Have a good day.

