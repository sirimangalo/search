1
00:00:00,000 --> 00:00:11,760
Hello everyone, I'm broadcasting live November 29th 2015.

2
00:00:11,760 --> 00:00:24,600
It's me as always, who's Robin Esposito.

3
00:00:24,600 --> 00:00:31,600
Notice anything different?

4
00:00:31,600 --> 00:00:35,600
No, it's not time yet, you have a few more weeks, right?

5
00:00:35,600 --> 00:00:38,600
I'm a phone one.

6
00:00:38,600 --> 00:00:41,600
I shape my face more.

7
00:00:41,600 --> 00:00:48,600
Yeah, apparently people get a bad image of you if you have your face shape.

8
00:00:48,600 --> 00:00:55,600
Hey, oh, we got a cat person.

9
00:00:55,600 --> 00:00:58,600
I clicked on a link.

10
00:00:58,600 --> 00:01:01,600
Okay, well welcome.

11
00:01:01,600 --> 00:01:02,600
No, that's great.

12
00:01:02,600 --> 00:01:03,600
Hello.

13
00:01:03,600 --> 00:01:06,600
Hi.

14
00:01:06,600 --> 00:01:07,600
I don't know who you are.

15
00:01:07,600 --> 00:01:08,600
What's your name?

16
00:01:08,600 --> 00:01:10,600
My name is Anne.

17
00:01:10,600 --> 00:01:11,600
Hi, Anne.

18
00:01:11,600 --> 00:01:12,600
Hi, Anne.

19
00:01:12,600 --> 00:01:16,600
Your live on me.

20
00:01:16,600 --> 00:01:17,600
Hi.

21
00:01:17,600 --> 00:01:18,600
I was expecting that.

22
00:01:18,600 --> 00:01:19,600
I just clicked the link.

23
00:01:19,600 --> 00:01:20,600
Okay.

24
00:01:20,600 --> 00:01:28,600
You're not part of our broadcast.

25
00:01:28,600 --> 00:01:31,600
Can we pull her up and find out like this?

26
00:01:31,600 --> 00:01:34,600
Does everyone see her?

27
00:01:34,600 --> 00:01:40,600
I think that everyone sees our profiles as smaller pictures.

28
00:01:40,600 --> 00:01:43,600
There you go.

29
00:01:43,600 --> 00:01:58,600
I can't force everyone to see her.

30
00:01:58,600 --> 00:02:00,600
Present to everyone.

31
00:02:00,600 --> 00:02:01,600
Okay.

32
00:02:01,600 --> 00:02:02,600
So is our guest?

33
00:02:02,600 --> 00:02:03,600
Hello.

34
00:02:03,600 --> 00:02:06,600
You're now the face too high.

35
00:02:06,600 --> 00:02:08,600
You can move your camera down a little bit.

36
00:02:08,600 --> 00:02:11,600
No, you can see my dirty room.

37
00:02:11,600 --> 00:02:13,600
Well done.

38
00:02:13,600 --> 00:02:14,600
Well done.

39
00:02:14,600 --> 00:02:15,600
I understand.

40
00:02:15,600 --> 00:02:16,600
Okay.

41
00:02:16,600 --> 00:02:17,600
I'm going to stop presenting.

42
00:02:17,600 --> 00:02:18,600
Okay.

43
00:02:18,600 --> 00:02:26,600
So Anne, welcome Anne.

44
00:02:26,600 --> 00:02:28,600
Did you have a question for us tonight?

45
00:02:28,600 --> 00:02:30,600
You want to tell us about it?

46
00:02:30,600 --> 00:02:31,600
Okay.

47
00:02:31,600 --> 00:02:33,600
Tell us about yourself first.

48
00:02:33,600 --> 00:02:35,600
Do you practice meditation?

49
00:02:35,600 --> 00:02:36,600
Yeah.

50
00:02:36,600 --> 00:02:39,600
How long have you been practicing meditation for?

51
00:02:39,600 --> 00:02:45,600
I'm on and off for about 26 or seven years.

52
00:02:45,600 --> 00:02:51,600
And for the last six months I've been practicing meditation regularly.

53
00:02:51,600 --> 00:02:53,600
Okay.

54
00:02:53,600 --> 00:02:56,600
Have you tried practicing the way we practice?

55
00:02:56,600 --> 00:02:57,600
Yes.

56
00:02:57,600 --> 00:02:58,600
Have you read my book?

57
00:02:58,600 --> 00:03:01,600
My question tonight is about learning.

58
00:03:01,600 --> 00:03:05,600
Did you read my booklet on how to meditate?

59
00:03:05,600 --> 00:03:10,600
I watched the video on studying and watching meditation.

60
00:03:10,600 --> 00:03:11,600
Okay.

61
00:03:11,600 --> 00:03:12,600
I read my book.

62
00:03:12,600 --> 00:03:14,600
There's a booklet.

63
00:03:14,600 --> 00:03:18,600
There's a booklet that explains it probably a little bit clearer.

64
00:03:18,600 --> 00:03:22,600
And if you have a chance, I'd recommend you read.

65
00:03:22,600 --> 00:03:24,600
It'll remind me about that.

66
00:03:24,600 --> 00:03:26,600
I have to talk about the booklet afterwards.

67
00:03:26,600 --> 00:03:27,600
But okay.

68
00:03:27,600 --> 00:03:28,600
So you have a question.

69
00:03:28,600 --> 00:03:29,600
Go ahead.

70
00:03:29,600 --> 00:03:32,600
You get priority because you were bold enough to join us.

71
00:03:32,600 --> 00:03:40,600
My question is, and I did type it.

72
00:03:40,600 --> 00:03:50,600
Is it appropriate and is it useful to notes throughout the day outside of meditation?

73
00:03:50,600 --> 00:03:52,600
Like, washing my dishes.

74
00:03:52,600 --> 00:03:55,600
Like, I noted while I washed my dishes.

75
00:03:55,600 --> 00:04:01,600
And when I'm sitting in my desk at work, I'll try to note, is that useful?

76
00:04:01,600 --> 00:04:02,600
Absolutely.

77
00:04:02,600 --> 00:04:09,600
In fact, it's, you couldn't say in many ways it's where it's the purpose of training and

78
00:04:09,600 --> 00:04:10,600
meditation.

79
00:04:10,600 --> 00:04:16,600
Like, why we sit walking and sitting meditation is to train ourselves to be able to be that

80
00:04:16,600 --> 00:04:20,600
way in our lives, you know, to be mindful during our lives.

81
00:04:20,600 --> 00:04:24,600
So when you get upset, during the day you can say upset, upset.

82
00:04:24,600 --> 00:04:29,600
Training all the time is incredibly useful because then it becomes a habit.

83
00:04:29,600 --> 00:04:31,600
Which is what we're trying for.

84
00:04:31,600 --> 00:04:33,600
We're trying to change our habits.

85
00:04:33,600 --> 00:04:36,600
Normally our habit is when we experience things, we judge them.

86
00:04:36,600 --> 00:04:40,600
We're very reactionary, reacting to just about everything, whether it's a thought we have,

87
00:04:40,600 --> 00:04:42,600
or whether it's an experience outside.

88
00:04:42,600 --> 00:04:49,600
We react, liking, disliking, identifying, pushing away.

89
00:04:49,600 --> 00:04:52,600
So changing that to be objective is incredibly useful.

90
00:04:52,600 --> 00:04:54,600
It will change your whole life.

91
00:04:54,600 --> 00:05:01,600
So it will free you up from so much of the stress that comes from being judgmental and

92
00:05:01,600 --> 00:05:08,600
having opinions and worrying about things because you think they're yours and so on.

93
00:05:08,600 --> 00:05:09,600
Absolutely.

94
00:05:09,600 --> 00:05:11,600
If you're washing dishes, then you're just washing dishes.

95
00:05:11,600 --> 00:05:14,600
And when it's done, it's done and you feel so much better.

96
00:05:14,600 --> 00:05:20,600
Then if you're washing dishes and you are worried about your work, you might as somewhere else.

97
00:05:20,600 --> 00:05:29,600
The Buddha said, person who is not in the present moment is like grass that is cut off from the source.

98
00:05:29,600 --> 00:05:32,600
When you cut grass, it shrivels up and dry.

99
00:05:32,600 --> 00:05:37,600
And he's a person who's cut off from reality is the same.

100
00:05:37,600 --> 00:05:47,600
So what you're doing is connecting yourself with reality and it's nourishes the mind, keeps the mind fresh and alive.

101
00:05:47,600 --> 00:05:54,600
You should read the booklet and this chapter 6 talks a little bit about that.

102
00:05:54,600 --> 00:06:00,600
Or anything if you read anything in this tradition, like anything by Mahasi Sayadha talks all about that kind of thing.

103
00:06:00,600 --> 00:06:05,600
There's lots of good advice.

104
00:06:05,600 --> 00:06:12,600
I just, I like the booklet in the chat there.

105
00:06:12,600 --> 00:06:14,600
Thank you.

106
00:06:14,600 --> 00:06:15,600
How is it?

107
00:06:15,600 --> 00:06:16,600
Where do you live?

108
00:06:16,600 --> 00:06:17,600
I live in Florida.

109
00:06:17,600 --> 00:06:19,600
I live in East of Florida.

110
00:06:19,600 --> 00:06:21,600
What part of Florida?

111
00:06:21,600 --> 00:06:22,600
Central Florida.

112
00:06:22,600 --> 00:06:25,600
How far from Tampa?

113
00:06:25,600 --> 00:06:29,600
Well, I'm actually planning on coming and seeing you on the 26th.

114
00:06:29,600 --> 00:06:30,600
Oh, awesome.

115
00:06:30,600 --> 00:06:31,600
Yeah.

116
00:06:31,600 --> 00:06:34,600
A couple of hours from Tampa.

117
00:06:34,600 --> 00:06:35,600
Not too bad.

118
00:06:35,600 --> 00:06:36,600
Okay.

119
00:06:36,600 --> 00:06:40,600
I'll be in the need, which is on the coast or near the coast.

120
00:06:40,600 --> 00:06:41,600
I love the need in.

121
00:06:41,600 --> 00:06:42,600
Beautiful.

122
00:06:42,600 --> 00:06:48,600
Well, my mother lives.

123
00:06:48,600 --> 00:06:57,600
My father came to visit today and he thinks I should stay in the school.

124
00:06:57,600 --> 00:07:07,600
So that is something, you know, your parents have to think about it.

125
00:07:07,600 --> 00:07:12,600
I submitted an essay on Locke today.

126
00:07:12,600 --> 00:07:20,600
I think I'll send him my essay for him to read.

127
00:07:20,600 --> 00:07:25,600
Got a new robe today from all of you actually.

128
00:07:25,600 --> 00:07:28,600
Many of the people here were involved with that.

129
00:07:28,600 --> 00:07:32,600
So thank you and blessings to all of you.

130
00:07:32,600 --> 00:07:37,600
I'm bringing peace and happiness to know that you gave someone

131
00:07:37,600 --> 00:07:40,600
cloth to cover their body.

132
00:07:40,600 --> 00:07:48,600
Word off cold and heat and mosquitoes and ticks and

133
00:07:48,600 --> 00:07:57,600
to cover up the private parts of the body.

134
00:07:57,600 --> 00:08:00,600
Not to mention the snow in Canada.

135
00:08:00,600 --> 00:08:03,600
Yeah, still went here.

136
00:08:03,600 --> 00:08:07,600
I'm told there will be snow.

137
00:08:07,600 --> 00:08:09,600
Haven't seen any yet.

138
00:08:09,600 --> 00:08:11,600
We did see a little.

139
00:08:11,600 --> 00:08:20,600
Yeah, it snowed a couple of times.

140
00:08:20,600 --> 00:08:24,600
Yeah, the big problem with switching colors is everybody's going to comment on it.

141
00:08:24,600 --> 00:08:31,600
I wonder if you changed somehow.

142
00:08:31,600 --> 00:08:35,600
That's what they think in Thailand.

143
00:08:35,600 --> 00:08:38,600
I switched to purple for a while.

144
00:08:38,600 --> 00:08:42,600
The burgundy that they use quite a bit in Sri Lanka.

145
00:08:42,600 --> 00:08:44,600
Same thing.

146
00:08:44,600 --> 00:08:49,600
Oh, yeah, so you're in my hand among them.

147
00:08:49,600 --> 00:08:52,600
That was interesting what you were explaining before someone

148
00:08:52,600 --> 00:08:54,600
had asked about the darker colors.

149
00:08:54,600 --> 00:08:58,600
And you were explaining that they're a little more affiliated with certain groups.

150
00:08:58,600 --> 00:09:04,600
So, but it's most of us just wherever we get.

151
00:09:04,600 --> 00:09:08,600
If you talk to the average monk, they switch and they wear this color and then that color.

152
00:09:08,600 --> 00:09:10,600
Nobody really cares.

153
00:09:10,600 --> 00:09:14,600
It's only at the big monasteries and the really strict and formal schools.

154
00:09:14,600 --> 00:09:17,600
Where for some reason they get caught up in colors.

155
00:09:17,600 --> 00:09:20,600
I mean, it's more for an image than anything.

156
00:09:20,600 --> 00:09:23,600
It becomes like a uniform.

157
00:09:23,600 --> 00:09:28,600
Just some extent it was meant to be a uniform, but I don't think the color was meant to be uniform.

158
00:09:28,600 --> 00:09:31,600
There certainly was never a color that's a bit depict.

159
00:09:31,600 --> 00:09:44,600
He just gave a range because it depends on what dye you have available.

160
00:09:44,600 --> 00:09:52,600
I'm going to get lots of questions about it.

161
00:09:52,600 --> 00:09:55,600
What else?

162
00:09:55,600 --> 00:09:58,600
Something else.

163
00:09:58,600 --> 00:10:07,600
Oh, boy.

164
00:10:07,600 --> 00:10:09,600
We have a nice quote today.

165
00:10:09,600 --> 00:10:13,600
It's a little long, but it's a really good one.

166
00:10:13,600 --> 00:10:15,600
We have some questions now.

167
00:10:15,600 --> 00:10:16,600
We have questions.

168
00:10:16,600 --> 00:10:17,600
Yeah.

169
00:10:17,600 --> 00:10:19,600
Let's go for the questions then.

170
00:10:19,600 --> 00:10:20,600
Sure.

171
00:10:20,600 --> 00:10:33,600
But I am traveling abroad with someone I do not get along with and I feel terribly homesick.

172
00:10:33,600 --> 00:10:37,600
How should I deal with these emotions when I feel like this time will never end?

173
00:10:37,600 --> 00:10:40,600
Thank you.

174
00:10:40,600 --> 00:10:49,600
Have you read my booklet on how to meditate?

175
00:10:49,600 --> 00:10:53,600
Because it sounds like a fairly simple problem.

176
00:10:53,600 --> 00:10:57,600
I mean, you have to be mindful.

177
00:10:57,600 --> 00:11:01,600
Not getting along with someone.

178
00:11:01,600 --> 00:11:08,600
It's partly them, but it's partly you that you get upset and so on.

179
00:11:08,600 --> 00:11:10,600
I think that you feel terribly homesick.

180
00:11:10,600 --> 00:11:11,600
Well, it's just a feeling.

181
00:11:11,600 --> 00:11:18,600
And if you can remind yourself of that, then that would solve the problem there.

182
00:11:18,600 --> 00:11:20,600
Feeling like the time will never end.

183
00:11:20,600 --> 00:11:22,600
Well, that's also just a feeling.

184
00:11:22,600 --> 00:11:24,600
I'm not going to kill you.

185
00:11:24,600 --> 00:11:29,600
So if you can learn to see it just as a feeling, then it'll solve the problem.

186
00:11:29,600 --> 00:11:33,600
I'd recommend reading my booklet and starting the practice accordingly.

187
00:11:33,600 --> 00:11:38,600
Thank you.

188
00:11:38,600 --> 00:11:46,600
You said when there are more objects, the choice of the noted objects should be based on clarity.

189
00:11:46,600 --> 00:11:51,600
But other times you say that one should note mental states, such as wanting or disliking or distracted.

190
00:11:51,600 --> 00:11:59,600
Does that mean we should give priority to and or actively search for certain unwholesome states?

191
00:11:59,600 --> 00:12:00,600
No.

192
00:12:00,600 --> 00:12:02,600
I mean, they're present.

193
00:12:02,600 --> 00:12:08,600
If you have pain, for example, and you don't like it, then you should probably not disliking first.

194
00:12:08,600 --> 00:12:13,600
Usually we ignore the disliking, because we cling to it.

195
00:12:13,600 --> 00:12:20,600
We hold onto the disliking and we look at the pain and say, there's the problem, there's the problem.

196
00:12:20,600 --> 00:12:22,600
And in fact, the problem is here.

197
00:12:22,600 --> 00:12:23,600
The problem is in the disliking.

198
00:12:23,600 --> 00:12:25,600
So it's not that it's less clear.

199
00:12:25,600 --> 00:12:27,600
It's that we ignore it completely.

200
00:12:27,600 --> 00:12:34,600
We feel happy and you like it, and so you're not the happiness, but you never know what the liking, even though the liking is actually clear.

201
00:12:34,600 --> 00:12:39,600
It's not that it's not clear.

202
00:12:39,600 --> 00:12:44,600
But there will be times where the feeling will be clearer than focus on the feeling.

203
00:12:44,600 --> 00:12:48,600
In fact, you can go back and forth, and there are different aspects.

204
00:12:48,600 --> 00:12:54,600
In a desire state, there's the desire, there's the feeling, there's the thoughts about it, there's the physical activities,

205
00:12:54,600 --> 00:12:57,600
there's the object of your desire.

206
00:12:57,600 --> 00:13:04,600
All of these things at different times can become meditation objects.

207
00:13:04,600 --> 00:13:18,600
You should not leave anyone out, but whatever, whichever one is clearest in the mind, that's the one you should focus on.

208
00:13:18,600 --> 00:13:24,600
And the next question is from Anne, so thank you for poppin' on and asking that. That was awesome.

209
00:13:24,600 --> 00:13:25,600
And names yourself.

210
00:13:25,600 --> 00:13:28,600
Am I subtle? Is that Pauli?

211
00:13:28,600 --> 00:13:31,600
No, it's my first initial in my last name.

212
00:13:31,600 --> 00:13:38,600
Must I recognize the sorrow, so I looked it up in the Pauli dictionary.

213
00:13:38,600 --> 00:13:44,600
And I saw that the ASARO means, like, illusion or unreality.

214
00:13:44,600 --> 00:13:46,600
What is your name?

215
00:13:46,600 --> 00:13:48,600
The sorrow or ASARO?

216
00:13:48,600 --> 00:13:50,600
The last name is Masero.

217
00:13:50,600 --> 00:13:51,600
Masero.

218
00:13:51,600 --> 00:13:53,600
It looks very Pauli.

219
00:13:53,600 --> 00:13:55,600
I was trying to figure out a Masero.

220
00:13:55,600 --> 00:13:58,600
I must say I could probably get a Masero.

221
00:13:58,600 --> 00:13:59,600
Masero.

222
00:13:59,600 --> 00:14:01,600
Masero.

223
00:14:01,600 --> 00:14:03,600
Masero is like, what is Masero?

224
00:14:03,600 --> 00:14:06,600
I want to say Masero, but I don't think so.

225
00:14:06,600 --> 00:14:20,600
Yeah, it's an interesting name, but it could almost be Pauli.

226
00:14:20,600 --> 00:14:22,600
Enter first question on the robe.

227
00:14:22,600 --> 00:14:24,600
Did you level up, Montet?

228
00:14:24,600 --> 00:14:28,600
Brown is good.

229
00:14:28,600 --> 00:14:31,600
How is an Arahant different from Anagami?

230
00:14:31,600 --> 00:14:37,600
Is it a big shift from Anagami to Arahant or is it more gradual?

231
00:14:41,600 --> 00:14:46,600
It's gradual, but you don't become half an Arahant.

232
00:14:46,600 --> 00:14:52,600
You stay an Anagami until the moment of attaining our hardship and then it's complete.

233
00:14:52,600 --> 00:14:54,600
It is quite a difference.

234
00:14:54,600 --> 00:15:00,600
Anagami might seem somewhat like an Arahant, but they still have ambition.

235
00:15:00,600 --> 00:15:01,600
They can still have ambition.

236
00:15:01,600 --> 00:15:03,600
They can still be confused.

237
00:15:03,600 --> 00:15:05,600
An Arahant doesn't have any of that.

238
00:15:05,600 --> 00:15:07,600
So I can see it.

239
00:15:07,600 --> 00:15:10,600
An Anagami can have consent.

240
00:15:10,600 --> 00:15:29,600
And that is it for questions so far.

241
00:15:29,600 --> 00:15:36,600
Yeah, see, there is a word, Amassa.

242
00:15:36,600 --> 00:15:41,600
Amassa.

243
00:15:41,600 --> 00:15:46,600
What does that mean?

244
00:15:46,600 --> 00:15:49,600
Amassa means to touch.

245
00:15:49,600 --> 00:15:51,600
Amassa means to touch.

246
00:15:51,600 --> 00:16:00,600
Amassa would be probably one who has touched or one who has been touched.

247
00:16:00,600 --> 00:16:04,600
Maybe the touch or maybe.

248
00:16:04,600 --> 00:16:09,600
It means one who has touched the Bama you could say.

249
00:16:09,600 --> 00:16:14,600
That's a pretty awesome day.

250
00:16:14,600 --> 00:16:18,600
You need to find you a poly name if you want to ordain this among someday.

251
00:16:18,600 --> 00:16:25,600
I shouldn't say I do, but I do.

252
00:16:25,600 --> 00:16:30,600
I have children.

253
00:16:30,600 --> 00:16:39,600
Amassa is a beard, right?

254
00:16:39,600 --> 00:16:42,600
It's a beard.

255
00:16:42,600 --> 00:16:49,600
Amassa is one who has a beard.

256
00:16:49,600 --> 00:17:00,600
Arah, Arah is odd, but I think you can put Arah at the end.

257
00:17:00,600 --> 00:17:13,600
Amassa is one who has a beard.

258
00:17:13,600 --> 00:17:19,600
Amassa is one who has a beard.

259
00:17:19,600 --> 00:17:32,600
That's right, Amassuka is one without a beard, a woman.

260
00:17:32,600 --> 00:17:41,600
A woman is Amassuka.

261
00:17:41,600 --> 00:17:51,600
It's more physical than metaphorical.

262
00:17:51,600 --> 00:17:57,600
It's supposed to be or pass.

263
00:17:57,600 --> 00:18:05,600
When you touch Bama, it's more of a different word than you use.

264
00:18:05,600 --> 00:18:16,600
Amassa is one who has a beard.

265
00:18:16,600 --> 00:18:45,600
What do you think about the Theravadin claim that an Anagami goes to some kind of heaven after death?

266
00:18:45,600 --> 00:18:46,600
This is the same person.

267
00:18:46,600 --> 00:18:47,600
He's coming back and asking again.

268
00:18:47,600 --> 00:18:48,600
I told you no more questions.

269
00:18:48,600 --> 00:18:49,600
You have to go and meditate.

270
00:18:49,600 --> 00:18:52,600
It's not all these Anagami questions.

271
00:18:52,600 --> 00:18:54,600
Not I'm not going to answer them.

272
00:18:54,600 --> 00:18:55,600
You want to come here?

273
00:18:55,600 --> 00:19:04,600
You meditate with us and ask questions about your meditation.

274
00:19:04,600 --> 00:19:07,600
No more Anagami questions.

275
00:19:07,600 --> 00:19:10,600
I'm not sure you like the next question either, but we'll give it a try.

276
00:19:10,600 --> 00:19:18,600
Our concentration and effort always balanced in our hunt.

277
00:19:18,600 --> 00:19:21,600
Maybe not.

278
00:19:21,600 --> 00:19:27,600
I think you might want to say yes, but I think there can be an excess.

279
00:19:27,600 --> 00:19:29,600
I don't think they have to always be balanced.

280
00:19:29,600 --> 00:19:31,600
It's interesting.

281
00:19:31,600 --> 00:19:32,600
Interesting question.

282
00:19:32,600 --> 00:19:41,600
Again, why?

283
00:19:41,600 --> 00:19:43,600
Patrick, are you isn't this the Patrick who meditates with this?

284
00:19:43,600 --> 00:19:44,600
It is.

285
00:19:44,600 --> 00:19:45,600
Yes.

286
00:19:45,600 --> 00:19:48,600
Maybe Patrick is meditating after.

287
00:19:48,600 --> 00:19:50,600
Oh, Patrick meditates all the time.

288
00:19:50,600 --> 00:19:53,600
He must be meditating after.

289
00:19:53,600 --> 00:19:55,600
Hang out tonight.

290
00:19:55,600 --> 00:19:56,600
Go meditate.

291
00:19:56,600 --> 00:20:12,600
It's not asking these questions.

292
00:20:12,600 --> 00:20:15,600
We have a long quote.

293
00:20:15,600 --> 00:20:16,600
I don't want to get into it.

294
00:20:16,600 --> 00:20:18,600
It's been a long day.

295
00:20:18,600 --> 00:20:19,600
I've got meditators here.

296
00:20:19,600 --> 00:20:21,600
I just finished my essay.

297
00:20:21,600 --> 00:20:23,600
My father came to visit.

298
00:20:23,600 --> 00:20:27,600
We had recently migrated this afternoon.

299
00:20:27,600 --> 00:20:29,600
I think we've done enough today.

300
00:20:29,600 --> 00:20:31,600
No, we're all offering.

301
00:20:31,600 --> 00:20:35,600
We had a long volunteer meeting.

302
00:20:35,600 --> 00:20:37,600
Tell us about that.

303
00:20:37,600 --> 00:20:40,600
You actually came in at the end of it.

304
00:20:40,600 --> 00:20:42,600
It was a great volunteer meeting.

305
00:20:42,600 --> 00:20:44,600
We started out saying this will be short because

306
00:20:44,600 --> 00:20:46,600
they can't make it to the volunteer meeting.

307
00:20:46,600 --> 00:20:48,600
We won't have much to talk about.

308
00:20:48,600 --> 00:20:51,600
We ended up having quite a few things to talk about anyway.

309
00:20:51,600 --> 00:20:54,600
It's the end.

310
00:20:54,600 --> 00:20:58,600
We were trying to plan for a small ceremony to offer the

311
00:20:58,600 --> 00:21:01,600
robe to Bante and trying to figure out how everyone could get

312
00:21:01,600 --> 00:21:02,600
there at the same time.

313
00:21:02,600 --> 00:21:06,600
It just wasn't working out because it's tough.

314
00:21:06,600 --> 00:21:09,600
It's tough to get people in the same place at one time.

315
00:21:09,600 --> 00:21:13,600
Then we just realized, well, the present moment is perfect.

316
00:21:13,600 --> 00:21:15,600
There were some meditators there.

317
00:21:15,600 --> 00:21:17,600
They presented the robe to Bante.

318
00:21:17,600 --> 00:21:19,600
While we were still online with them.

319
00:21:19,600 --> 00:21:22,600
It's all recorded as part of our volunteer meeting.

320
00:21:22,600 --> 00:21:25,600
We can post that for anybody who would like to see it.

321
00:21:25,600 --> 00:21:26,600
It was nice.

322
00:21:26,600 --> 00:21:28,600
It worked out nicely.

323
00:21:28,600 --> 00:21:33,600
Nothing like the present moment.

324
00:21:33,600 --> 00:21:41,600
Anything of interest else happened in the meeting?

325
00:21:41,600 --> 00:21:46,600
We were talking about the fact that your electric bill is quite high.

326
00:21:46,600 --> 00:21:50,600
It was about $300 for two months.

327
00:21:50,600 --> 00:21:55,600
That seems high, given the fact that your heat is natural gas.

328
00:21:55,600 --> 00:22:01,600
The electricity should be just lights and appliances.

329
00:22:01,600 --> 00:22:07,600
I'm going to, I don't know, use an electric heater in this room.

330
00:22:07,600 --> 00:22:11,600
No, he said he wasn't using it.

331
00:22:11,600 --> 00:22:17,600
I was going to contact the electric company and see if they would be willing to do an energy

332
00:22:17,600 --> 00:22:18,600
on it.

333
00:22:18,600 --> 00:22:20,600
Generally, the utilities will do those things.

334
00:22:20,600 --> 00:22:25,600
If it turns out there's something wrong with the meter, if they're charging more

335
00:22:25,600 --> 00:22:30,600
wattage than what they should, it does seem a little high.

336
00:22:30,600 --> 00:22:36,600
Jeff said by comparison, his is about $50 a month, although he is in a different province.

337
00:22:36,600 --> 00:22:38,600
He might have lower rates.

338
00:22:38,600 --> 00:22:44,600
It does seem strange in trying to think, well, my electric heater may be, but it's not a big one.

339
00:22:44,600 --> 00:22:50,600
If they do an audit, they can just see if there's something radically wrong or something wrong with the meter,

340
00:22:50,600 --> 00:22:57,600
or maybe one of the appliances, if like a fridge or a freezer is very old and not energy efficient,

341
00:22:57,600 --> 00:22:59,600
it can be a big draw.

342
00:22:59,600 --> 00:23:02,600
Sometimes it's actually worth it to invest in a newer, more energy.

343
00:23:02,600 --> 00:23:07,600
We need to freeze downstairs maybe, but still $150 a month.

344
00:23:07,600 --> 00:23:08,600
It seems like a lot.

345
00:23:08,600 --> 00:23:18,600
Yeah, so we're going to look into that and see.

346
00:23:18,600 --> 00:23:30,600
I'm still wondering about your winter project, what we can do to help with your winter project that you wanted to do.

347
00:23:30,600 --> 00:23:33,600
Is there any more information on that, Monte?

348
00:23:33,600 --> 00:23:39,600
No, it's fundamentally said Monday or Tuesday, someone's going to go to the place and talk.

349
00:23:39,600 --> 00:23:41,600
How wonderful.

350
00:23:41,600 --> 00:23:42,600
But there was something else.

351
00:23:42,600 --> 00:23:44,600
What was it, other booklet?

352
00:23:44,600 --> 00:23:50,600
Someone is going to Sri Lanka on the 5th.

353
00:23:50,600 --> 00:23:57,600
Srihan is going to Sri Lanka on the 5th, and I was going to get them to bring back some booklets with them.

354
00:23:57,600 --> 00:24:04,600
So, the organization could arrange that, or we could have everyone.

355
00:24:04,600 --> 00:24:13,600
If anyone wants to support that, be a part of the printing, we could do an online thing.

356
00:24:13,600 --> 00:24:14,600
What do you think?

357
00:24:14,600 --> 00:24:15,600
Sure.

358
00:24:15,600 --> 00:24:16,600
Now, what do you mean, booklet?

359
00:24:16,600 --> 00:24:17,600
Do you mean your book?

360
00:24:17,600 --> 00:24:18,600
Yeah.

361
00:24:18,600 --> 00:24:20,600
Oh, so they'll actually print it up for you.

362
00:24:20,600 --> 00:24:21,600
A thousand of them.

363
00:24:21,600 --> 00:24:24,600
Oh, awesome.

364
00:24:24,600 --> 00:24:25,600
Yeah.

365
00:24:25,600 --> 00:24:35,600
It's 55 rupees each, which I think is how much is 55 rupees?

366
00:24:35,600 --> 00:24:39,600
25 cents?

367
00:24:39,600 --> 00:24:56,600
0.823 US dollars, so 82 cents?

368
00:24:56,600 --> 00:24:57,600
No.

369
00:24:57,600 --> 00:24:58,600
Patient?

370
00:24:58,600 --> 00:24:59,600
Well, it's a 55 Indian rupee.

371
00:24:59,600 --> 00:25:01,600
Is the Sri Lanka rupee different?

372
00:25:01,600 --> 00:25:02,600
Yes, it's different.

373
00:25:02,600 --> 00:25:03,600
Okay.

374
00:25:03,600 --> 00:25:04,600
About 50 cents.

375
00:25:04,600 --> 00:25:09,600
50 cents each?

376
00:25:09,600 --> 00:25:11,600
50 cents each?

377
00:25:11,600 --> 00:25:16,600
4,000 is 500 dollars?

378
00:25:16,600 --> 00:25:17,600
500 dollars.

379
00:25:17,600 --> 00:25:18,600
That's well worth it.

380
00:25:18,600 --> 00:25:22,600
That's a great way to share that.

381
00:25:22,600 --> 00:25:27,600
So, yeah, if we can grab them.

382
00:25:27,600 --> 00:25:28,600
Yeah.

383
00:25:28,600 --> 00:25:31,600
That would definitely be something we'd want to support.

384
00:25:31,600 --> 00:25:33,600
Do you have time to work on that?

385
00:25:33,600 --> 00:25:34,600
Yeah.

386
00:25:34,600 --> 00:25:35,600
Definitely.

387
00:25:35,600 --> 00:25:36,600
Okay.

388
00:25:36,600 --> 00:25:37,600
Okay.

389
00:25:37,600 --> 00:25:40,600
So, we will do that and also the winter project.

390
00:25:40,600 --> 00:25:43,600
So, we'll have two projects going.

391
00:25:43,600 --> 00:25:48,600
That'll be nice.

392
00:25:48,600 --> 00:25:51,600
Awesome.

393
00:25:51,600 --> 00:25:54,600
Just made a little more information on the other project.

394
00:25:54,600 --> 00:25:58,600
And the other thing just going on within the volunteer group.

395
00:25:58,600 --> 00:26:03,600
As Sri Mongolian International is in the process of redoing bylaws.

396
00:26:03,600 --> 00:26:08,600
So, people that are interested in reviewing the bylaws and coming to a meeting to vote on them.

397
00:26:08,600 --> 00:26:11,600
That would be our next volunteer meeting.

398
00:26:11,600 --> 00:26:14,600
We're just going to ask people to take a look at it.

399
00:26:14,600 --> 00:26:20,600
So, if anyone hasn't been involved but would like to become involved.

400
00:26:20,600 --> 00:26:26,600
If you can just send me an email or our Facebook group also.

401
00:26:26,600 --> 00:26:29,600
Go and go and go and go and go to international volunteers on Facebook.

402
00:26:29,600 --> 00:26:31,600
We're in a bunch of different places.

403
00:26:31,600 --> 00:26:34,600
But I'll post my email in the meditation chat.

404
00:26:34,600 --> 00:26:38,600
And we're definitely looking for more people to become involved.

405
00:26:38,600 --> 00:26:42,600
And to take a look at the new bylaws which are.

406
00:26:42,600 --> 00:26:46,600
They're actually, it's a template provided by the Province of Ontario.

407
00:26:46,600 --> 00:26:49,600
So, it's something that they actually provide.

408
00:26:49,600 --> 00:26:54,600
And that lets you know the rules that a nonprofit organization is required to abide by.

409
00:26:54,600 --> 00:26:58,600
So, it seems pretty solid but always good to have different people take a look at it and say,

410
00:26:58,600 --> 00:27:02,600
hey, this is maybe something that is a concern.

411
00:27:02,600 --> 00:27:05,600
So, just send me an email if you'd like to become involved.

412
00:27:05,600 --> 00:27:07,600
We would be happy to have you.

413
00:27:07,600 --> 00:27:12,600
And we have a question from Brenna.

414
00:27:12,600 --> 00:27:15,600
Who states that even though she's yellow, she did meditate today.

415
00:27:15,600 --> 00:27:18,600
Renunciation is considered a wholesome deed.

416
00:27:18,600 --> 00:27:23,600
But can fixating, clinging to the idea of renunciation be considered.

417
00:27:23,600 --> 00:27:26,600
And wholesome.

418
00:27:26,600 --> 00:27:29,600
Brenna, you get a pass.

419
00:27:29,600 --> 00:27:32,600
You don't have to be clear.

420
00:27:32,600 --> 00:27:34,600
You get a lifetime pass.

421
00:27:34,600 --> 00:27:37,600
Don't worry about that.

422
00:27:37,600 --> 00:27:39,600
Absolutely.

423
00:27:39,600 --> 00:27:43,600
Yeah, if you're fixated on anything, the Buddha said,

424
00:27:43,600 --> 00:28:00,600
the Buddha said,

425
00:28:00,600 --> 00:28:07,600
So, it's not really renunciation that we're talking about.

426
00:28:07,600 --> 00:28:15,600
And often it's running away.

427
00:28:15,600 --> 00:28:23,600
Not wanting to have to deal with the stress of possessions.

428
00:28:23,600 --> 00:28:27,600
I mean, at face value, that can be wholesome.

429
00:28:27,600 --> 00:28:32,600
If you just can't be bothered, if you've given up any desire for possessions,

430
00:28:32,600 --> 00:28:33,600
that's good.

431
00:28:33,600 --> 00:28:40,600
But if you're upset about having to deal with possession, that's not good.

432
00:28:40,600 --> 00:28:44,600
So, it's kind of weird because true renunciation you could never be attached to.

433
00:28:44,600 --> 00:28:49,600
But true renunciation is renouncing even desire to renounce.

434
00:28:49,600 --> 00:28:55,600
You know, desire to be free, desire to escape.

435
00:28:55,600 --> 00:28:56,600
In a sense.

436
00:28:56,600 --> 00:29:01,600
I mean, it's really hard to talk because there are some sense for desire to be free

437
00:29:01,600 --> 00:29:03,600
and desire to escape is good.

438
00:29:03,600 --> 00:29:04,600
It's wholesome.

439
00:29:04,600 --> 00:29:06,600
But it's not really desire.

440
00:29:06,600 --> 00:29:13,600
It's just the knowledge-based impetus that leads you there.

441
00:29:13,600 --> 00:29:17,600
But there's no upset if it doesn't go the way you planned or anything, right?

442
00:29:17,600 --> 00:29:21,600
There's no attachment involved.

443
00:29:21,600 --> 00:29:26,600
It's just more of an inclination.

444
00:29:26,600 --> 00:29:31,600
So, what we want to develop is the inclination to renounce.

445
00:29:31,600 --> 00:29:37,600
It's just becoming inclined to return away, as the Buddha said.

446
00:29:37,600 --> 00:29:45,600
Become nibita, nibita, which means you become disenchanted.

447
00:29:45,600 --> 00:29:49,600
Nibindatidukay is a mongolus utya.

448
00:29:49,600 --> 00:30:00,600
This is the path of purity.

449
00:30:00,600 --> 00:30:19,600
Okay, good night.

450
00:30:19,600 --> 00:30:20,600
Thank you, Bhante.

451
00:30:20,600 --> 00:30:22,600
Thank you for being brave and joining us.

452
00:30:22,600 --> 00:30:24,600
You're welcome anytime.

453
00:30:24,600 --> 00:30:26,600
Thank you, Bhante.

454
00:30:26,600 --> 00:30:28,600
Thank you, Robin.

455
00:30:28,600 --> 00:30:29,600
Thank you.

456
00:30:29,600 --> 00:30:30,600
And thank you, Bhante.

457
00:30:30,600 --> 00:30:31,600
Have a good evening, everyone.

458
00:30:31,600 --> 00:30:32,600
Good evening, everyone.

459
00:30:32,600 --> 00:30:33,600
Good evening, everyone.

460
00:30:33,600 --> 00:31:00,600
Thanks for joining us.

