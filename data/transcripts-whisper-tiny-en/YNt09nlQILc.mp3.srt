1
00:00:00,000 --> 00:00:09,600
Hey welcome back to Ask a Monk. Next question comes from Jay who asks media addiction TV shows fiction books popular music

2
00:00:09,880 --> 00:00:12,800
These things can be a distraction when meditating for sure

3
00:00:13,280 --> 00:00:18,720
But do you think they are okay to enjoy in moderation? What are your thoughts on media in general?

4
00:00:22,200 --> 00:00:26,240
Yeah, okay, well the the agreement is that

5
00:00:26,240 --> 00:00:31,720
They are distraction from meditation. This is I think easy to understand

6
00:00:32,080 --> 00:00:34,080
So easy for people to

7
00:00:34,200 --> 00:00:42,280
Keep this sort of a rule during a meditation course most people. I do know people who have brought their guitars to meditation centers or

8
00:00:42,320 --> 00:00:46,120
You know brought marijuana and tried to get high during their meditation course

9
00:00:46,120 --> 00:00:51,120
so on but mostly we can accept that we're trying to do something very specific here and

10
00:00:51,120 --> 00:00:58,680
And that these things shouldn't shouldn't play a part in that and that's really why when you read about the formation of

11
00:00:59,360 --> 00:01:04,160
moral moral precepts in in the Buddha's teaching

12
00:01:06,320 --> 00:01:10,100
You you don't have you don't see these in in terms of a lay

13
00:01:11,160 --> 00:01:16,740
morality the rules against entertainment and so on for monks were not allowed to listen to music or

14
00:01:16,740 --> 00:01:21,980
Or what shows or so on that this is something that is not

15
00:01:22,660 --> 00:01:23,980
acceptable for a

16
00:01:23,980 --> 00:01:26,460
Monastic but for ordinary people

17
00:01:26,740 --> 00:01:33,020
There are only the five rules not to kill not to steal not to cheat not to lie and not to take drugs in alcohol

18
00:01:33,180 --> 00:01:40,820
These are the five moral precepts because these five things are are obviously immoral and

19
00:01:42,580 --> 00:01:45,700
Intrinsically immoral they can't be separated from an

20
00:01:45,700 --> 00:01:47,700
Immoral mind

21
00:01:48,420 --> 00:01:57,700
That being said my thoughts on the media. I mean the more important point than then is it allowed or is it not allowed is?

22
00:02:00,300 --> 00:02:02,300
Is whether it's it's beneficial

23
00:02:03,260 --> 00:02:08,180
Whether it actually brings happiness and the truth is these things don't bring peace and happiness

24
00:02:08,180 --> 00:02:18,580
They don't create a state of contentment in the mind. I mean Buddhism or the or meditation is for the purpose of of learning to just just be learning to

25
00:02:21,100 --> 00:02:27,020
To be here and now to exist in the present moment without having to be something or get something or

26
00:02:27,620 --> 00:02:30,120
Without having to make our experience a specific

27
00:02:30,120 --> 00:02:42,020
Pleasing experience something that is going to stimulate us to be able to be here and now in any situation and the truth about these things is they actually make us irritable

28
00:02:43,520 --> 00:02:45,120
discontent

29
00:02:45,120 --> 00:02:47,120
board

30
00:02:47,120 --> 00:02:53,760
You even depressed and so on we we are finding ourselves in this world more and more

31
00:02:53,760 --> 00:02:59,800
Unsatisfied more and more depressed and unhappy and frustrated and angry and

32
00:03:01,440 --> 00:03:07,280
Feeling a sense of less self-worth the more we engage in this greater of this

33
00:03:08,800 --> 00:03:10,800
Entertainment with greater and greater intensity

34
00:03:11,800 --> 00:03:14,640
now it's it's it's become you know the

35
00:03:15,640 --> 00:03:21,920
So instantaneous that we can get whatever we want and yet we're finding you know people are more and more

36
00:03:21,920 --> 00:03:28,480
turning to drugs and and alcohol and even prescription medication to to

37
00:03:29,240 --> 00:03:32,920
relieve the the suffering that this is causing you can

38
00:03:34,720 --> 00:03:41,640
If you have any sort of awareness self awareness you can verify this for yourself that these things don't make you happy

39
00:03:41,840 --> 00:03:45,360
They don't make you a more content and peaceful person

40
00:03:45,360 --> 00:03:57,600
They create states of discontent the inability to sit still the inability to be happy without the stimulation that you require more and more

41
00:03:58,400 --> 00:04:01,760
stimulation just to attain the same state of

42
00:04:04,360 --> 00:04:06,360
of

43
00:04:06,360 --> 00:04:08,360
contentment or of

44
00:04:08,560 --> 00:04:10,560
pleasure

45
00:04:10,560 --> 00:04:11,960
and

46
00:04:11,960 --> 00:04:17,360
So I would say that that's in a Buddhist sense what makes things immoral is what do they do to you?

47
00:04:17,360 --> 00:04:19,360
I mean killing is not evil

48
00:04:19,760 --> 00:04:25,280
It's it's evil only in the sense that it creates suffering both for you and for the other person

49
00:04:25,400 --> 00:04:33,080
Most especially for yourself because a person who is killed can theoretically be free from suffering if you know

50
00:04:33,080 --> 00:04:37,120
If you're murdered, but your mind is pure you can theoretically

51
00:04:37,120 --> 00:04:44,440
Be at peace with that, but a person who kills cannot the person who kills is is affecting their mind is

52
00:04:45,640 --> 00:04:48,800
Perverting their mind and and is causing suffering for themselves

53
00:04:51,040 --> 00:04:57,800
So that's what makes it evil that the in that sense you can say that these things are her in a sense evil

54
00:04:57,800 --> 00:05:02,960
I mean they they are an addiction and really everything is like that anything that you cling to

55
00:05:04,200 --> 00:05:06,200
becomes an addiction

56
00:05:06,200 --> 00:05:07,040
The

57
00:05:07,040 --> 00:05:14,280
There is a tendency to want to deny this and I'm sure probably I'm gonna get comments to this saying that

58
00:05:14,280 --> 00:05:19,600
Oh, no, I don't agree music is something that you know brings you into this trance state and so on

59
00:05:20,400 --> 00:05:22,200
and

60
00:05:22,760 --> 00:05:25,160
I suppose to some extent that's that's

61
00:05:26,360 --> 00:05:29,080
Perfectly true and I would agree with that that there are certain

62
00:05:29,680 --> 00:05:31,040
sounds like

63
00:05:31,040 --> 00:05:38,240
Chanting or even especially music as well that sends you into a trance sort of state and people say well, that's meditative

64
00:05:39,360 --> 00:05:44,320
Obviously, that's not the meditation that I teach and it's kind of beside the point the point is the

65
00:05:44,880 --> 00:05:51,640
Attachment to these things your addiction to them the fact that you like them even the fact that you like that trance state that state of

66
00:05:52,160 --> 00:05:57,760
meditative bliss and absorption that is a hindrance to you either Buddha said even

67
00:05:57,760 --> 00:06:05,600
Even trance states are our dangerous in the sense that one might become attached to them

68
00:06:06,040 --> 00:06:11,040
There's nothing wrong with these states in and of themselves and they can be actually useful for meditation

69
00:06:11,120 --> 00:06:18,720
The point is that that things like music have an appeal to them have create this stimulation in the mind in the brain

70
00:06:20,080 --> 00:06:22,560
that gives rise to a pleasure stimulus and

71
00:06:22,560 --> 00:06:28,600
And you know and as a result in addiction because you you get you get what you want and

72
00:06:29,160 --> 00:06:30,840
There's the chemicals arise

73
00:06:30,840 --> 00:06:36,640
But the next time when you want to get it you need more and it has to be more intense and so on as I've talked about in other videos and

74
00:06:37,200 --> 00:06:42,280
As can be verified by modern biology and chemistry

75
00:06:43,960 --> 00:06:45,960
so

76
00:06:46,560 --> 00:06:51,320
There are distraction from meditation, but they're also a path that is is

77
00:06:51,320 --> 00:06:56,400
Not really conducive to to peace happiness and freedom from suffering. So

78
00:06:56,960 --> 00:06:58,120
the

79
00:06:58,120 --> 00:07:01,640
use of these things in moderation is not a problem

80
00:07:01,640 --> 00:07:05,320
It's not going to send you to to a bad place when you die

81
00:07:05,600 --> 00:07:10,240
But it will keep you bound to to the wheel of some. Sorry. I will keep you

82
00:07:10,800 --> 00:07:16,360
You know going on and on because your mind is still clinging your mind is still creating

83
00:07:16,360 --> 00:07:22,840
It's still fixed on on coming back. So I mean, I think for most people in this world

84
00:07:22,840 --> 00:07:25,320
That's not a bad thing in fact for many

85
00:07:25,960 --> 00:07:27,320
Theistic

86
00:07:27,320 --> 00:07:30,280
Religious people who have under who have come to understand Buddhism

87
00:07:30,320 --> 00:07:33,960
It's quite a relief to know that you have another chance and that you can come back

88
00:07:34,720 --> 00:07:37,560
But this is obviously a fairly immature sort of

89
00:07:39,600 --> 00:07:45,000
Position or understanding when once you take the next step and start to realize the ramifications you think oh

90
00:07:45,000 --> 00:07:51,080
Well, yeah, well, then I have to come back and do this again and maybe again and again and again and again and again

91
00:07:52,160 --> 00:08:00,280
Because you don't likely remember everything and eventually you're going to forget it and fall back into a place that it is very coarse and

92
00:08:03,200 --> 00:08:10,280
Deterministic like the human world and it'll be very difficult and you'll fall into the same difficulties that you've had in this life

93
00:08:10,280 --> 00:08:11,880
maybe even worse

94
00:08:11,880 --> 00:08:16,880
So yeah, that's a little bit off track, but that's what these things lead to. This is the

95
00:08:18,480 --> 00:08:22,440
Opiate it's something that keeps us asleep keeps us

96
00:08:23,760 --> 00:08:27,440
You know, it's like that it's exactly the opiate of the masses

97
00:08:27,440 --> 00:08:32,920
It's something that keeps us from asking the questions that we should ask keeps us from having any

98
00:08:34,320 --> 00:08:36,320
Get up and go keeps us from

99
00:08:36,320 --> 00:08:41,400
Developing and improving ourselves music is a very obvious one. It's something that

100
00:08:42,240 --> 00:08:48,640
You know, they have all these conspiracy theories that the entertainment industry is controlled by the government who's trying to just keep us all happy

101
00:08:48,640 --> 00:08:52,120
Well, you don't have to go so far. I mean, we're doing it to ourselves where

102
00:08:54,440 --> 00:09:03,360
We're it or you can go even further and say the whole universe is is based on this way. It's keeping us attached to rebirth so

103
00:09:03,360 --> 00:09:05,360
our

104
00:09:06,640 --> 00:09:15,040
Addiction to these things is is helping us to pass the time and allowing us to forget about the important questions like

105
00:09:17,360 --> 00:09:23,240
All the age sickness and death and suffering and why these things exist and how to become free from them

106
00:09:23,360 --> 00:09:25,760
Then where do we go when we die and so on?

107
00:09:26,480 --> 00:09:27,920
so

108
00:09:27,920 --> 00:09:35,280
From a Buddhist point of view, it doesn't really make sense to give these things and more than a basic

109
00:09:39,280 --> 00:09:46,320
Appreciation if you want to listen to music. It's not a problem. It's not going to make you not a Buddhist

110
00:09:46,640 --> 00:09:47,760
but

111
00:09:47,760 --> 00:09:52,280
Buddhists do have an understanding that these things are addictive and

112
00:09:54,000 --> 00:09:55,080
that

113
00:09:55,080 --> 00:10:00,080
they can they generally lead us to waste time and

114
00:10:02,120 --> 00:10:05,840
And lead to more and more suffering so unless unless that is faction

115
00:10:06,400 --> 00:10:11,280
When you practice meditation you start to realize this and you start to give these things up naturally

116
00:10:11,360 --> 00:10:15,080
You start to become bored of them and say that you know

117
00:10:15,080 --> 00:10:24,200
There's really no happiness or peace to be gained from this and and as a result you you find yourself just being able to just

118
00:10:24,960 --> 00:10:28,520
live life as it were and to be present

119
00:10:29,080 --> 00:10:33,160
Without needing anything without needing any kind of stimulus or special experience

120
00:10:33,720 --> 00:10:35,720
Okay, so long answer, but

121
00:10:36,600 --> 00:10:40,920
Someone did ask that I talk about music. So there's my take on it and

122
00:10:40,920 --> 00:10:46,840
I'm just a little bit more personal information. It's not that I'm a person who never

123
00:10:48,040 --> 00:10:50,920
Enjoyed music. I was a guitarist. I had a

124
00:10:52,440 --> 00:10:54,440
Less Paul deluxe

125
00:10:54,600 --> 00:10:56,600
$1,200 electric guitar

126
00:10:57,320 --> 00:11:01,640
I had a fairly extensive music collection and was in

127
00:11:02,680 --> 00:11:04,920
Two or three different by bands

128
00:11:04,920 --> 00:11:12,440
So you know music is something that was very difficult for me to let go of but

129
00:11:13,160 --> 00:11:17,320
In the beginning. I was very decisive about these things. I understood

130
00:11:18,360 --> 00:11:24,440
Intellectually what the teaching meant and I appreciated that and I thought that has real meaning to me

131
00:11:24,680 --> 00:11:25,800
So regardless

132
00:11:25,800 --> 00:11:27,080
It's kind of like this

133
00:11:27,080 --> 00:11:29,880
I knew that these things were attractive to me

134
00:11:29,880 --> 00:11:36,440
I knew that I liked music, but I also knew that that liking was the cause of my suffering because I was suffering

135
00:11:36,520 --> 00:11:38,520
I was you know deeply

136
00:11:38,600 --> 00:11:40,600
Mired in these things

137
00:11:40,680 --> 00:11:43,880
They were my life and I was suffering terribly

138
00:11:43,880 --> 00:11:47,720
I was I was incredibly depressed and had no meaning in my life

139
00:11:47,720 --> 00:11:49,160
I felt like it was

140
00:11:49,160 --> 00:11:55,160
No, it was just these feelings that come up and they're based on addiction. It's a withdrawal thing or it's it's the addictive cycle

141
00:11:55,720 --> 00:11:57,080
so

142
00:11:57,080 --> 00:12:01,640
That's basically how I took all of these things so many different things. I said to myself

143
00:12:01,880 --> 00:12:07,000
Yeah, I like it, but I couldn't I couldn't I didn't need to convince myself to let them go

144
00:12:07,000 --> 00:12:14,520
I knew that this liking was the problem the problem was not not getting the thing the problem was the liking of it in the first place

145
00:12:14,520 --> 00:12:16,520
Which was causing me suffering

146
00:12:16,680 --> 00:12:17,720
so

147
00:12:17,720 --> 00:12:22,360
I'm not a stranger to these things and I understand where many people are coming from but

148
00:12:22,360 --> 00:12:29,800
With serious meditation practice you do change the way you look at these things and for the better because you are

149
00:12:30,120 --> 00:12:37,960
Obviously, we are obviously suffering when we're addicted to these things. There's no way around that and I don't think you can convince me otherwise

150
00:12:39,400 --> 00:12:46,520
With the exception of those people who use it to gain a sort of a trans state and sort of look at that as being the goal is this

151
00:12:47,000 --> 00:12:48,440
state of

152
00:12:48,440 --> 00:12:54,600
Intense calm but even that is not permanent. It's something that is conditioned and

153
00:12:55,560 --> 00:13:00,160
You know is a sort of an addiction and we'll create a dissatisfaction in your lives

154
00:13:00,160 --> 00:13:07,160
It's has to do with ego and and you you can see when you practice the type of meditation that I teach that this

155
00:13:07,400 --> 00:13:09,720
You know this state is inferior and there's still

156
00:13:11,000 --> 00:13:13,160
The mind is still not clear and

157
00:13:14,200 --> 00:13:16,200
not pure

158
00:13:16,200 --> 00:13:17,880
so

159
00:13:17,880 --> 00:13:24,680
No, that's how I look at it, and I'm sure there are people with other opinions, but

160
00:13:24,680 --> 00:13:45,720
You know to each of their own you ask the monk. This is the monks reply. Thanks for the question. Keep practicing

