1
00:00:00,000 --> 00:00:07,200
Hello, welcome back to Ask a Monk. This next question is a pretty good example, I think,

2
00:00:07,200 --> 00:00:17,600
of how not to ask questions on this forum. And that's because it's specifically, well,

3
00:00:17,600 --> 00:00:26,000
it seems quite clearly to be a presentation of one's own philosophy in the form of a question,

4
00:00:26,000 --> 00:00:31,200
because here we have a premise and a question I'll read it to you. I want to ask about

5
00:00:31,200 --> 00:00:37,600
non-duality, when one doesn't distinguish the two realms of Samsara and Nirvana, then there is no

6
00:00:37,600 --> 00:00:44,400
goal. So my question is, is wanting to transcend reverb selfish? But what you have here is a complete

7
00:00:44,400 --> 00:00:50,000
philosophy that exists in Buddhism. Buddhism, there are many philosophies and they're not all

8
00:00:50,000 --> 00:00:59,440
compatible. This one I happen to not agree with. And this is a complete presentation of this

9
00:00:59,440 --> 00:01:11,600
philosophy. And it's clearly so because the premise and the conclusion that is presented in the

10
00:01:11,600 --> 00:01:18,560
premise, which the implicit conclusion being that striving for the end of reverb is selfish,

11
00:01:18,560 --> 00:01:28,880
it has nothing to do with each other unless accepting the greater framework of this philosophy.

12
00:01:29,600 --> 00:01:38,320
So it seems clear that the purpose of this post is not to ask a question, it's to present a philosophy,

13
00:01:38,320 --> 00:01:44,000
which is not really appropriate. Now, I'm not sure this is the case. It may be that the person

14
00:01:44,000 --> 00:01:49,520
asking is really interested in really things that I believe that these two things are

15
00:01:49,520 --> 00:01:58,960
indistinguishable and somehow thinks that that effects has some effect on the question as to

16
00:01:58,960 --> 00:02:06,240
whether strengthening reverb with the selfish. I don't see it. So briefly I'll go over this,

17
00:02:06,240 --> 00:02:12,000
but this isn't the kind of question that I'm interested in asking. I would like to be answering

18
00:02:12,000 --> 00:02:21,120
questions about meditation and that really help you understand the Buddhist teaching and

19
00:02:22,880 --> 00:02:27,600
understand the meditation practice and improve your meditation practice and help you along the

20
00:02:27,600 --> 00:02:37,520
path. So, but anyway, the premise, why I disagree with this philosophy, the idea that some

21
00:02:37,520 --> 00:02:45,440
sara and nirvana are indistinguishable is just a solpism. It's a theory. It's a view. It has

22
00:02:45,440 --> 00:02:50,400
something to do with reality. If you experience some sara, some sara is one thing. This is some sara

23
00:02:50,400 --> 00:02:55,120
seeing the hearing, smelling the tasting and feeling and thinking. It's theorizing of suffering

24
00:02:55,120 --> 00:03:01,760
and the pursuit of suffering. nirvana is the opposite. It's the non-arising of suffering,

25
00:03:01,760 --> 00:03:12,080
the non pursuit of suffering. The reason why people are unable to distinguish it is because they

26
00:03:12,080 --> 00:03:17,760
haven't experienced it. Everything they've experienced is the same. They practice meditation and

27
00:03:17,760 --> 00:03:23,120
their experiences come and go and so they relate that back to some sara and it also comes and goes

28
00:03:23,120 --> 00:03:27,120
and everything comes and goes and they say nothing is different. So they're unable to distinguish

29
00:03:27,120 --> 00:03:33,040
one from the other, which is correct really because everything is indistinguishable except nirvana.

30
00:03:33,040 --> 00:03:38,960
nirvana. Everything else arises in sieces and this is why the Buddha said nothing is worth

31
00:03:38,960 --> 00:03:49,360
clinging to because it all arises in sieces. So, it's basically equating two opposites or

32
00:03:49,360 --> 00:03:57,680
confusing two opposites. It's like saying that night and day are indistinguishable, light and

33
00:03:57,680 --> 00:04:09,920
darkness are indistinguishable. Theoretically, you can come up with a theory that says that is the

34
00:04:09,920 --> 00:04:15,600
case but doesn't affect the reality that light is quite different from darkness and that's

35
00:04:15,600 --> 00:04:23,440
objectively so nirvana and samsara are objectively so as well. No matter what theory or philosophy

36
00:04:23,440 --> 00:04:31,200
you come up with because you can come up with any philosophy you like and it doesn't affect,

37
00:04:31,200 --> 00:04:38,720
it doesn't affect on reality. As to the question, I'm assuming that the implication here is that

38
00:04:38,720 --> 00:04:51,520
somehow that premise implies that or leads to the conclusion, supports the conclusion that

39
00:04:52,080 --> 00:04:59,840
freedom or the desire wanting to transcend rebirth is selfish. I'm assuming that's what's implied here.

40
00:04:59,840 --> 00:05:08,720
So, I can talk about that. Selfishness only really comes into play and Buddhism as I understand it

41
00:05:10,400 --> 00:05:14,960
in terms of clinging. So, if you cling to something and you don't want someone else to have it,

42
00:05:16,080 --> 00:05:19,600
if you ask me for something and I don't want to give it to you, it's only because I'm clinging

43
00:05:19,600 --> 00:05:28,000
to the object. There's no true selfishness that arises. It's a clinging. It's a function of the

44
00:05:28,000 --> 00:05:33,520
mind that wants an object. You don't think about the other person. What we call selfishness is

45
00:05:33,520 --> 00:05:42,400
this absorption in one's attachment and only by overcoming that can you possibly give up something

46
00:05:42,400 --> 00:05:47,360
that is a benefit to you and we're able to do that as we grow up. It's not because we become, well,

47
00:05:47,360 --> 00:05:57,920
it could be because we become more altruistic but that is not the way of the Buddha, that's not

48
00:05:57,920 --> 00:06:03,760
the answer because this idea of altruism is just another theory. It's another mind game and there

49
00:06:03,760 --> 00:06:08,080
are people who even refute this theory and say, you know, how can you possibly work for the

50
00:06:08,080 --> 00:06:15,520
benefit of other people? But the Buddha's teaching doesn't go either way. There's no idea that

51
00:06:16,400 --> 00:06:23,600
working for one's benefit is better than working for one's own benefit. You act in such a way

52
00:06:23,600 --> 00:06:28,800
that brings harmony. You act in such a way that leads to the least conflict. You act in such

53
00:06:28,800 --> 00:06:36,640
a way that is going to be most appropriate at any given time and it's not a intellectual exercise.

54
00:06:36,640 --> 00:06:41,280
You don't have to study the books and learn what is most appropriate though. That can help

55
00:06:42,400 --> 00:06:49,680
for people who don't have the experience. You understand the nature of reality through your

56
00:06:49,680 --> 00:06:54,960
practice and therefore are able to clearly see what is of the greatest benefit? What is the most

57
00:06:54,960 --> 00:06:59,360
appropriate at any given time? And it might be acting for your own benefit. It might be acting

58
00:06:59,360 --> 00:07:08,000
for someone else's benefit but the point is not either or. The point is the appropriateness of the

59
00:07:08,000 --> 00:07:15,760
act. So if someone comes and asks you for something many things might come into play. It depends on

60
00:07:15,760 --> 00:07:21,200
the situation. If you ask me for something, I might give it to you even though it might cause me

61
00:07:21,200 --> 00:07:25,760
suffering. There may be many reasons for that. It could be because you've helped me before. It could

62
00:07:25,760 --> 00:07:32,640
be because I am able to deal with suffering. I know that you're not. I have practiced meditation and

63
00:07:32,640 --> 00:07:37,520
I am able to be patient and so on. I can see that you're not. I can see that giving to you

64
00:07:37,520 --> 00:07:42,960
will lead to less suffering overall because for me it's only physical suffering for you. It's mental

65
00:07:42,960 --> 00:07:47,680
and so on. I might not give you any of it because I might see that you are not going to use it

66
00:07:47,680 --> 00:07:51,680
or I might see that if you're without it, you're going to learn something. You're going to learn

67
00:07:51,680 --> 00:07:58,800
patients and so on. There are many responses. This is talking about enlightened person. If a

68
00:07:58,800 --> 00:08:04,080
person is enlightened, they will act in this way. They will never rise to them. My benefit,

69
00:08:04,080 --> 00:08:10,240
you're a benefit because there's no clinging and that's the only time that selfishness arises.

70
00:08:10,240 --> 00:08:21,760
So the idea that wanting to transcend rebirth is selfish doesn't come into play. It has nothing to do

71
00:08:21,760 --> 00:08:26,560
with anyone else. The only way you could call it selfish is if you start playing these mind games

72
00:08:26,560 --> 00:08:34,240
and logic and it's an intellectual exercise where you say, well, if you did stay around you could

73
00:08:34,240 --> 00:08:40,560
help so many people and therefore it is selfish to, you know, but it's just a theory, right? It

74
00:08:40,560 --> 00:08:45,200
has nothing to do with the state of mind. The person could be totally unselfish and if anyone asked

75
00:08:45,200 --> 00:08:49,760
them or something, they would help them. But they don't ever think, hmm, boy, I should stick around

76
00:08:49,760 --> 00:08:55,440
otherwise it's going to be selfish because there's no clinging. There's no attachment to this

77
00:08:55,440 --> 00:09:01,440
state or that state. The person simply acts as is appropriate in that instance. So

78
00:09:01,440 --> 00:09:10,240
the realization of freedom from rebirth or the attainment of freedom from rebirth is of the

79
00:09:10,240 --> 00:09:16,080
greatest benefit to a person. If a person wants to strive for that, then that's a good thing. It

80
00:09:16,080 --> 00:09:26,640
creates benefit. If a person decides to extend their or not work towards the freedom from

81
00:09:26,640 --> 00:09:30,800
rebirth and they want to be reborn again and again, then that's their prerogative. The Buddha never

82
00:09:30,800 --> 00:09:38,320
laid down any laws of nature in this regard because there are none. There is simply the cause

83
00:09:38,320 --> 00:09:46,400
and effect. If a person decides that they want to extend their existence and come back again and

84
00:09:46,400 --> 00:09:51,280
again and be reborn again and again things are going to help people then that's fine or that's

85
00:09:51,280 --> 00:09:59,360
their choice. That's their path. There's no need for any sort of judgment at all. If a person

86
00:09:59,360 --> 00:10:04,800
decides that they want to become free from suffering in this life and not come back,

87
00:10:04,800 --> 00:10:11,440
be free from rebirth, then that's their path as well. There's only the cause and effect. So a

88
00:10:11,440 --> 00:10:15,520
person who is reborn again and again and again, will have to come back again and again and

89
00:10:15,520 --> 00:10:24,720
again. It's questionable as to how much help they can possibly be given that they are not yet

90
00:10:24,720 --> 00:10:37,760
free from clinging because a person who is free from clinging would not be reborn. The person,

91
00:10:37,760 --> 00:10:45,760
actually the person who does become free from rebirth, who does transcend rebirth, so to speak,

92
00:10:46,960 --> 00:10:51,760
is able to help people because their mind is free from clinging and therefore they are able to

93
00:10:53,760 --> 00:10:57,680
provide great support and help and great insight. People, the Buddha is a good example.

94
00:10:57,680 --> 00:11:09,120
Now there is the question of whether the Buddha and enlightened beings do come back because there

95
00:11:09,120 --> 00:11:14,400
are even philosophies that believe a person who is free from clinging still is reborn, but that's

96
00:11:14,400 --> 00:11:22,240
not really the question here. So it's getting a little bit too much into other people's philosophies

97
00:11:22,240 --> 00:11:28,320
and I don't want to create more confusion than is necessary. So the point here is that we work for

98
00:11:28,320 --> 00:11:37,200
benefit, we work for welfare, we do it's appropriate, or we try to, this is our practice,

99
00:11:37,200 --> 00:11:43,840
and then enlightened being is someone who is perfect at it, who doesn't have any thought of self

100
00:11:43,840 --> 00:11:54,480
or other, and is not subject to suffering when they pass away from this life, they are free

101
00:11:54,480 --> 00:12:04,800
from all suffering and there's no more coming back, no more rising. So basically I just wanted to

102
00:12:04,800 --> 00:12:15,440
say that this is not the sort of question that I'm hoping to entertain and I think probably in

103
00:12:15,440 --> 00:12:19,760
the future I'm just going to skip over because I got a lot of questions and I'm going to try to

104
00:12:19,760 --> 00:12:25,280
be selective, I've been quite random actually in choosing which questions to answer because it's

105
00:12:25,280 --> 00:12:36,320
difficult to sort them, I've got a lot of them and Google doesn't make it easy by any means, so I'm

106
00:12:36,320 --> 00:12:44,880
going to try to skip and go directly to those ones that I think are going to be useful for people.

107
00:12:44,880 --> 00:12:52,320
Okay so anyway thanks for tuning in so it's been another episode of Ask a Monk and wishing you

108
00:12:52,320 --> 00:12:55,840
all peace happiness and freedom from suffering.

