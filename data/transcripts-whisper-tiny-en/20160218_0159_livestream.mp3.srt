1
00:00:00,000 --> 00:00:29,000
Sorry, I'm a little bit late, just to reinstall my viewer.

2
00:00:29,000 --> 00:00:36,000
Okay, I'm going to get the audio set up.

3
00:00:36,000 --> 00:00:41,000
This one should come from now.

4
00:00:41,000 --> 00:00:46,000
This one should come.

5
00:00:46,000 --> 00:00:58,000
Okay, so now we should have voice on both channels.

6
00:00:58,000 --> 00:01:03,000
Okay.

7
00:01:03,000 --> 00:01:08,000
Okay.

8
00:01:08,000 --> 00:01:23,000
Just going to set up my seat.

9
00:01:23,000 --> 00:01:33,000
Okay.

10
00:01:33,000 --> 00:01:43,000
Okay.

11
00:01:43,000 --> 00:02:09,000
Okay, so good evening, everyone.

12
00:02:09,000 --> 00:02:16,000
I'm broadcasting live.

13
00:02:16,000 --> 00:02:19,000
This is actually the time when I normally broadcast on the internet anyway.

14
00:02:19,000 --> 00:02:36,000
So there should be some people listening in at meditation.ceremango.org.

15
00:02:36,000 --> 00:02:55,000
So today I understand is really Chinese New Year celebration.

16
00:02:55,000 --> 00:03:03,000
Has anyone been taking part in the second life?

17
00:03:03,000 --> 00:03:13,000
It's been happening around today, it's going on today.

18
00:03:13,000 --> 00:03:28,000
Everybody's going on all day.

19
00:03:28,000 --> 00:03:33,000
Is my audio okay?

20
00:03:33,000 --> 00:03:35,000
Last time I saw that it was turning red.

21
00:03:35,000 --> 00:03:40,000
I don't want to look too loud, I don't know if it's too loud.

22
00:03:40,000 --> 00:03:53,000
I shouldn't ever see the red.

23
00:03:53,000 --> 00:04:00,000
Okay.

24
00:04:00,000 --> 00:04:07,000
Well, I don't celebrate Chinese New Year.

25
00:04:07,000 --> 00:04:12,000
Put it down for something.

26
00:04:12,000 --> 00:04:18,000
Honestly, I'm kind of confused by all the New Year's.

27
00:04:18,000 --> 00:04:25,000
There's the one I grew up with.

28
00:04:25,000 --> 00:04:32,000
It's the one we have on January 1st.

29
00:04:32,000 --> 00:04:40,000
It's going to say, I grew up with a Christian calendar.

30
00:04:40,000 --> 00:04:49,000
And so we have another Korean New Year.

31
00:04:49,000 --> 00:04:54,000
Then there's some South Asia and Southeast Asia.

32
00:04:54,000 --> 00:05:01,000
They have a New Year and people.

33
00:05:01,000 --> 00:05:16,000
And now we have a third New Year that I have to think about.

34
00:05:16,000 --> 00:05:35,000
So we, but Buddhist holidays are kind of, kind of like that.

35
00:05:35,000 --> 00:05:49,000
We're just excuses for birthdays and excuses for birthdays and excuses for birthdays.

36
00:05:49,000 --> 00:06:08,000
So our mortality, there are standard Buddhist holidays that actually have one million as a result.

37
00:06:08,000 --> 00:06:27,000
But it's a nightmare that they have his first discourse.

38
00:06:27,000 --> 00:06:41,000
So both monks and my peoples in Buddhist society has been when you've been exiting the room.

39
00:06:41,000 --> 00:06:52,000
But New Year, well, not so much.

40
00:06:52,000 --> 00:06:59,000
The idea of cycles is important.

41
00:06:59,000 --> 00:07:08,000
It's important that we count in some way to be able to measure, to be able to compare.

42
00:07:08,000 --> 00:07:24,000
So New Year is just going to measure, boy, a whole year is going to be.

43
00:07:24,000 --> 00:07:43,000
And so it's traditionally become a tiny.

44
00:07:43,000 --> 00:07:46,000
And Chinese tradition, it's a time for a member of your ancestors, which of course doesn't really have anyone to do.

45
00:07:46,000 --> 00:07:54,000
Buddhism, it's much more interesting than just general folk religion.

46
00:07:54,000 --> 00:07:55,000
But they didn't do it in Thailand.

47
00:07:55,000 --> 00:07:57,000
People have Chinese background.

48
00:07:57,000 --> 00:07:58,000
They didn't have Chinese background.

49
00:07:58,000 --> 00:07:59,000
They didn't have Chinese background.

50
00:07:59,000 --> 00:08:01,000
They burned stuff.

51
00:08:01,000 --> 00:08:06,000
They weren't able to listen or burn.

52
00:08:06,000 --> 00:08:15,000
They burned money, they burned houses, they burned food, they burned clothes.

53
00:08:15,000 --> 00:08:22,000
They burned just about everything.

54
00:08:22,000 --> 00:08:29,000
They burned effigy.

55
00:08:29,000 --> 00:08:40,000
It's a big thing in India, at the time of the Buddha, it's a lot of instagram stuff.

56
00:08:40,000 --> 00:08:47,000
It's a burning thing, burning things has always been symbolic for us.

57
00:08:47,000 --> 00:08:52,000
It's a flame.

58
00:08:52,000 --> 00:08:56,000
It's a little bit of talk about flames.

59
00:08:56,000 --> 00:08:59,000
They said, you want to know the truth about fire?

60
00:08:59,000 --> 00:09:05,000
I thought the cleansing of fire, we talked about the God of fire.

61
00:09:05,000 --> 00:09:12,000
The Chinese tradition is somehow that if you burn it, it gets to the person who died because you've burnt the person who died.

62
00:09:12,000 --> 00:09:17,000
So when you burn stuff, it gets to them.

63
00:09:17,000 --> 00:09:20,000
It doesn't make any sense from the point of Buddhism.

64
00:09:20,000 --> 00:09:25,000
Because you're not burning real stuff, you're not sacrificing anything, you're not giving anything.

65
00:09:25,000 --> 00:09:28,000
The way Buddhism looks at things is much different.

66
00:09:28,000 --> 00:09:38,000
So in that tradition, we should comment on this Chinese tradition the way Buddha would comment on the Indian tradition.

67
00:09:38,000 --> 00:09:45,000
So the Buddha went after he became enlightened.

68
00:09:45,000 --> 00:09:53,000
He walked from water in a seat to basically to Rajika.

69
00:09:53,000 --> 00:10:00,000
But before he went to Rajika, 120 kilometers.

70
00:10:00,000 --> 00:10:05,000
It's a long distance.

71
00:10:05,000 --> 00:10:12,000
It was a long way to walk.

72
00:10:12,000 --> 00:10:24,000
But before he went to Rajika, he had to find someone to give him legitimacy,

73
00:10:24,000 --> 00:10:26,000
providing with the Judaism.

74
00:10:26,000 --> 00:10:30,000
He would need to make his case for ordinary peoples.

75
00:10:30,000 --> 00:10:38,000
And so at the time there was a group of ascetics, three groups of ascetics, three brothers,

76
00:10:38,000 --> 00:10:44,000
which were following, living along the river.

77
00:10:44,000 --> 00:10:50,000
He used to be a river going to Rajika in a surface seat.

78
00:10:50,000 --> 00:10:59,000
2005, the years ago found a river.

79
00:10:59,000 --> 00:11:10,000
And they all worshiped by us.

80
00:11:10,000 --> 00:11:14,000
On the topic of fire, we have the spiral worshipers.

81
00:11:14,000 --> 00:11:18,000
And so the Buddha sent to them all the same fire.

82
00:11:18,000 --> 00:11:23,000
He spent some time convincing in the first of his magical powers,

83
00:11:23,000 --> 00:11:29,000
and eventually he led them to a spot Gaiya Sisa,

84
00:11:29,000 --> 00:11:32,000
which is, I guess, the top of the mountain.

85
00:11:32,000 --> 00:11:37,000
You'll move on to that.

86
00:11:37,000 --> 00:11:42,000
Oh, God, look at this.

87
00:11:42,000 --> 00:11:53,000
And it was kind of close to Rajika in some moment.

88
00:11:53,000 --> 00:11:58,000
And he taught them the adiitya periya sita.

89
00:11:58,000 --> 00:12:22,000
And so he started by saying sabbang deep degree as it felt.

90
00:12:22,000 --> 00:12:29,000
All sabbang deep degree, all sabbang deep degree,

91
00:12:29,000 --> 00:12:38,000
all sabbang deep degree.

92
00:12:38,000 --> 00:13:02,000
What do I mean by everything else?

93
00:13:02,000 --> 00:13:24,000
So basically, the six senses, the internal base, the external base and consciousness,

94
00:13:24,000 --> 00:13:36,000
giving the minds on fire.

95
00:13:36,000 --> 00:13:47,000
The thought, um, mind, consciousness, a thought.

96
00:13:47,000 --> 00:13:52,000
So this left is a common method of the Buddha. He asks a question in the answer again.

97
00:13:54,000 --> 00:13:57,000
It's a very clear method of discourse.

98
00:14:00,000 --> 00:14:03,000
In Aditam, it's what I do on the phone.

99
00:14:05,000 --> 00:14:07,000
I agree, I agree.

100
00:14:08,000 --> 00:14:12,000
I'll let you know something I'm not thinking.

101
00:14:12,000 --> 00:14:15,000
Yes?

102
00:14:15,000 --> 00:14:19,000
Well, I've been used while I was on Saturdays.

103
00:14:19,000 --> 00:14:17,040
I shouldn't.

104
00:14:19,040 --> 00:14:26,000
Last, those of you that have found an algorithm that showed you

105
00:14:26,000 --> 00:14:29,000
may have had me now before the motion.

106
00:14:34,000 --> 00:14:37,980
And you tell me job awards

107
00:14:37,980 --> 00:14:58,980
So they're entire with two problems, but they're also in primary suffering.

108
00:14:58,980 --> 00:15:03,980
So, Jazzy, I don't know if these are all the types of suffering.

109
00:15:03,980 --> 00:15:08,980
So this is what they're on fire with.

110
00:15:08,980 --> 00:15:12,980
Because they're caught up in some sense.

111
00:15:12,980 --> 00:15:16,980
They're caught up in this perpetual cycle.

112
00:15:16,980 --> 00:15:19,980
Decide the satisfaction.

113
00:15:19,980 --> 00:15:41,980
So, this is the way they would have approached fire with two of them.

114
00:15:41,980 --> 00:15:50,980
Probably an important or a good way.

115
00:15:50,980 --> 00:16:02,980
And just responding or commenting on the rituals and things.

116
00:16:02,980 --> 00:16:09,980
And it's not actually that related.

117
00:16:09,980 --> 00:16:15,980
One other thing that we've seen is that these sorts of things.

118
00:16:15,980 --> 00:16:20,980
Because often our rituals are considered good.

119
00:16:20,980 --> 00:16:23,980
Rituals are considered to be positive.

120
00:16:23,980 --> 00:16:25,980
And if it's a wholesome.

121
00:16:25,980 --> 00:16:31,980
So you burn stuff for your ancestors and that's supposed to be wholesome.

122
00:16:31,980 --> 00:16:32,980
Good.

123
00:16:32,980 --> 00:16:41,980
And the idea here is that some of these things get to benefit.

124
00:16:41,980 --> 00:16:46,980
And the people who've been living.

125
00:16:46,980 --> 00:16:48,980
Those are going to be good.

126
00:16:48,980 --> 00:16:55,980
So, your ancestors wherever they are will benefit from burning stuff.

127
00:16:55,980 --> 00:16:56,980
It's curious.

128
00:16:56,980 --> 00:17:01,980
I mean, this kind of thing happens all over the world.

129
00:17:01,980 --> 00:17:03,980
You burn a sacrifice for God.

130
00:17:03,980 --> 00:17:04,980
These were big things.

131
00:17:04,980 --> 00:17:07,980
The angels wanted us to burn sacrifices, ancient times.

132
00:17:07,980 --> 00:17:11,980
We read the Torah, the holy book of the Jewish people.

133
00:17:11,980 --> 00:17:14,980
Apparently God likes them to burn stuff.

134
00:17:14,980 --> 00:17:16,980
Something to burn stuff.

135
00:17:16,980 --> 00:17:19,980
Somehow it got to God.

136
00:17:19,980 --> 00:17:42,980
But it's the idea that somehow you could, somehow you could perform a sacrifice.

137
00:17:42,980 --> 00:17:45,980
And that would benefit us.

138
00:17:45,980 --> 00:17:50,980
We do it in Buddhism as well.

139
00:17:50,980 --> 00:17:53,980
We do it towards the Buddha a lot.

140
00:17:53,980 --> 00:17:56,980
Like we burn candles and flowers.

141
00:17:56,980 --> 00:18:05,980
We burn incense light candles and put flowers in front of the Buddha.

142
00:18:05,980 --> 00:18:14,980
I don't know that I can remember the point where the Buddha said that's a good thing.

143
00:18:14,980 --> 00:18:21,980
I imagine you did it to the close that you could find where he said, oh yes, it's mandatory.

144
00:18:21,980 --> 00:18:24,980
It's probably more impassive.

145
00:18:24,980 --> 00:18:28,980
The most famous point is where he actually addressed this.

146
00:18:28,980 --> 00:18:31,980
He said, this isn't how he is.

147
00:18:31,980 --> 00:18:36,980
Rightly worshiped the Buddha.

148
00:18:36,980 --> 00:18:38,980
But we do it anyway.

149
00:18:38,980 --> 00:18:45,980
And we've got in many instances gone kind of crazy about it.

150
00:18:45,980 --> 00:18:47,980
We're just talking about this today.

151
00:18:47,980 --> 00:18:50,980
Funny enough, on Chinese New Year, that's what it is.

152
00:18:50,980 --> 00:18:53,980
I have had this one for three.

153
00:18:53,980 --> 00:18:54,980
The Chinese won't.

154
00:18:54,980 --> 00:18:58,980
It's just something I do every day, but he just happened to come this one.

155
00:18:58,980 --> 00:19:01,980
He went up to lunch.

156
00:19:01,980 --> 00:19:04,980
And he was showing me pictures of how they...

157
00:19:04,980 --> 00:19:11,980
They asked for 24 sets of food.

158
00:19:11,980 --> 00:19:13,980
They asked him if one was good.

159
00:19:13,980 --> 00:19:23,980
But in Sri Lankan tradition, they are after 28 sets of 28 liters.

160
00:19:23,980 --> 00:19:28,980
So there's this list of 28 liters that's in one case.

161
00:19:28,980 --> 00:19:35,980
That's over time.

162
00:19:35,980 --> 00:19:36,980
It could be just made up.

163
00:19:36,980 --> 00:19:41,980
But it's a list of 28 weren't come from them, really.

164
00:19:41,980 --> 00:19:45,980
But they give these extra within all things.

165
00:19:45,980 --> 00:19:48,980
And every time they know...

166
00:19:48,980 --> 00:19:52,980
Well, the...

167
00:19:52,980 --> 00:19:59,980
One of the most troubling aspects of it is that they throw out everything they give after me.

168
00:19:59,980 --> 00:20:05,980
So I'm just watching them put together these sets of drinks.

169
00:20:05,980 --> 00:20:10,980
So it's three different being glasses of just 28 sets.

170
00:20:10,980 --> 00:20:13,980
And then at the end, they throw the juice away.

171
00:20:13,980 --> 00:20:16,980
They say we can't drink it once it's given food.

172
00:20:16,980 --> 00:20:23,980
And they do this with food as well as with food.

173
00:20:23,980 --> 00:20:27,980
And they start looking to offer food to the good day.

174
00:20:27,980 --> 00:20:32,980
Shall I show it up?

175
00:20:32,980 --> 00:20:34,980
I'm not convinced that this is...

176
00:20:34,980 --> 00:20:38,980
I mean, I've talked about it, but I don't want to go into too much detail.

177
00:20:38,980 --> 00:20:45,980
But my overarching point is that this isn't really how goodness works in business work.

178
00:20:45,980 --> 00:20:49,980
And goodness is doing a good deed and dedicate.

179
00:20:49,980 --> 00:20:51,980
Whatever you like.

180
00:20:51,980 --> 00:20:55,980
Using the power of the goodness.

181
00:20:55,980 --> 00:20:59,980
There's power in goodness, but it has to be goodness.

182
00:20:59,980 --> 00:21:02,980
Putting food in front of a statue.

183
00:21:02,980 --> 00:21:03,980
I get it.

184
00:21:03,980 --> 00:21:04,980
I get it.

185
00:21:04,980 --> 00:21:09,980
There's faith in this confidence.

186
00:21:09,980 --> 00:21:11,980
It's just...

187
00:21:11,980 --> 00:21:15,980
You're not really benefiting a statue.

188
00:21:15,980 --> 00:21:19,980
It's not goodness in the same way as giving food to a beggar.

189
00:21:19,980 --> 00:21:21,980
Or giving food to a monk.

190
00:21:21,980 --> 00:21:24,980
Or religious teacher.

191
00:21:24,980 --> 00:21:29,980
If you feed someone who's teaching good things.

192
00:21:29,980 --> 00:21:32,980
Sorry, I shouldn't.

193
00:21:32,980 --> 00:21:35,980
I'm not hinting at anything.

194
00:21:35,980 --> 00:21:38,980
But this is the thing, is people would give food

195
00:21:38,980 --> 00:21:43,980
in the old days and even now in Buddhist countries.

196
00:21:43,980 --> 00:21:46,980
It would be food to be food to be religious leaders.

197
00:21:46,980 --> 00:21:49,980
And that's clearly good.

198
00:21:49,980 --> 00:21:54,980
Because you're providing life to that person.

199
00:21:54,980 --> 00:21:58,980
You know that you are directly supporting good things.

200
00:21:58,980 --> 00:22:02,980
You're directly influenced in the future.

201
00:22:02,980 --> 00:22:06,980
Giving to a statue.

202
00:22:06,980 --> 00:22:10,980
You have to argue it's a different type of goodness.

203
00:22:10,980 --> 00:22:15,980
But it borders on blind ritual.

204
00:22:15,980 --> 00:22:18,980
Because there's some idea that by doing this,

205
00:22:18,980 --> 00:22:19,980
and goodness comes in it.

206
00:22:19,980 --> 00:22:21,980
It's not really how good it is in water.

207
00:22:21,980 --> 00:22:24,980
Goodness comes from goodness.

208
00:22:24,980 --> 00:22:26,980
There's actually a good thing that you're doing.

209
00:22:26,980 --> 00:22:28,980
Putting juice in front of the statue.

210
00:22:28,980 --> 00:22:30,980
Putting food in front of the statue.

211
00:22:30,980 --> 00:22:32,980
I get that this is in goodness.

212
00:22:32,980 --> 00:22:37,980
Anywhere.

213
00:22:37,980 --> 00:22:40,980
That's how Buddhists do it.

214
00:22:40,980 --> 00:22:43,980
And some people argue very seriously

215
00:22:43,980 --> 00:22:45,980
that it is a good thing to do.

216
00:22:45,980 --> 00:22:48,980
To each their own.

217
00:22:48,980 --> 00:22:51,980
But it points to this.

218
00:22:51,980 --> 00:22:54,980
And then this idea of it.

219
00:22:54,980 --> 00:22:58,980
That we have no one things to do.

220
00:22:58,980 --> 00:23:00,980
And we burn some.

221
00:23:00,980 --> 00:23:02,980
And so a Buddhist.

222
00:23:02,980 --> 00:23:04,980
An actual Buddhist ceremony.

223
00:23:04,980 --> 00:23:05,980
Wouldn't be the burn stuff.

224
00:23:05,980 --> 00:23:10,980
Hopefully that it gets to the people you love.

225
00:23:10,980 --> 00:23:13,980
Though you could argue that you're good thoughts.

226
00:23:13,980 --> 00:23:16,980
The fact that you are wishing them well.

227
00:23:16,980 --> 00:23:18,980
Yeah, there's something for sure.

228
00:23:18,980 --> 00:23:21,980
There's a way of clearing or having thoughts.

229
00:23:21,980 --> 00:23:24,980
And thinking good thoughts about that person.

230
00:23:24,980 --> 00:23:26,980
Respecting and reviewing.

231
00:23:26,980 --> 00:23:28,980
It's a symbolic gesture.

232
00:23:28,980 --> 00:23:31,980
That's probably how they're explained.

233
00:23:31,980 --> 00:23:38,980
But there is a way that your goodness can reach the disease.

234
00:23:38,980 --> 00:23:41,980
And that is by dedicating.

235
00:23:41,980 --> 00:23:43,980
Actual good deans.

236
00:23:43,980 --> 00:23:47,980
They can be compared burning fake money.

237
00:23:47,980 --> 00:23:49,980
They're gold.

238
00:23:49,980 --> 00:23:53,980
Too actually taking money and doing good things with it.

239
00:23:53,980 --> 00:23:58,980
And then saying, this is our respect for that person.

240
00:23:58,980 --> 00:24:01,980
And I do this on behalf of that person.

241
00:24:01,980 --> 00:24:03,980
Those people.

242
00:24:03,980 --> 00:24:06,980
That's something that actually has fruits.

243
00:24:06,980 --> 00:24:09,980
There's fruit in many ways.

244
00:24:09,980 --> 00:24:13,980
But most importantly, it says that the power of the goodness

245
00:24:13,980 --> 00:24:17,980
gives you the power to determine in your mind.

246
00:24:17,980 --> 00:24:23,980
There's all sort of the direction that you like that can take.

247
00:24:23,980 --> 00:24:27,980
So it doesn't just work for disease value.

248
00:24:27,980 --> 00:24:32,980
If you give charity and then you make a vow through this,

249
00:24:32,980 --> 00:24:38,980
through this give me an eye, progress on the spirit of the pattern.

250
00:24:38,980 --> 00:24:40,980
And that kind of thing.

251
00:24:40,980 --> 00:24:43,980
Just because goodness has a powerful quality.

252
00:24:43,980 --> 00:24:48,980
It takes away the guilt.

253
00:24:48,980 --> 00:24:53,980
It takes away the stingy that you breathe.

254
00:24:53,980 --> 00:24:59,980
I think the deans improved for venting capacity.

255
00:24:59,980 --> 00:25:00,980
That's the idea.

256
00:25:00,980 --> 00:25:07,980
So I mean, this hopefully is a no interesting quality.

257
00:25:07,980 --> 00:25:11,980
It's not entirely what I had in mind to talk about today.

258
00:25:11,980 --> 00:25:18,980
And I did want to talk about the idea.

259
00:25:18,980 --> 00:25:23,980
It was just briefly to point out that with all these rituals

260
00:25:23,980 --> 00:25:28,980
and all these sort of branches.

261
00:25:28,980 --> 00:25:31,980
I shouldn't just make it a button.

262
00:25:31,980 --> 00:25:36,980
We shouldn't just make the new be about the mirror.

263
00:25:36,980 --> 00:25:38,980
So as I said, this is a way of counting.

264
00:25:38,980 --> 00:25:42,980
And counting years.

265
00:25:42,980 --> 00:25:45,980
But what is actually new?

266
00:25:45,980 --> 00:25:48,980
This is the point.

267
00:25:48,980 --> 00:25:51,980
What is that?

268
00:25:51,980 --> 00:25:56,980
Are you the same old person with all the same old problems?

269
00:25:56,980 --> 00:25:58,980
Because we're counting for a reason.

270
00:25:58,980 --> 00:26:01,980
So we're just counting one year of our life gone.

271
00:26:01,980 --> 00:26:04,980
That will never get back.

272
00:26:04,980 --> 00:26:09,980
We're counting when we're closer to death.

273
00:26:09,980 --> 00:26:13,980
And then we're just slaves to the stars and the sun.

274
00:26:13,980 --> 00:26:15,980
We're shipping the sun.

275
00:26:15,980 --> 00:26:17,980
We've gone around the sun again more time.

276
00:26:17,980 --> 00:26:19,980
It is just for fun.

277
00:26:19,980 --> 00:26:22,980
I think often times it's just to celebrate.

278
00:26:22,980 --> 00:26:26,980
If you go to China, if you go to countries that celebrate these things.

279
00:26:26,980 --> 00:26:29,980
For many people it's just an excuse to get done.

280
00:26:29,980 --> 00:26:33,980
That's what we do here on New Year's Eve, right?

281
00:26:33,980 --> 00:26:37,980
No, no, you should leave us anything.

282
00:26:37,980 --> 00:26:42,980
You want to have a happy year.

283
00:26:42,980 --> 00:26:46,980
And you yourself should...

284
00:26:46,980 --> 00:26:50,980
And bother to leave us.

285
00:26:50,980 --> 00:26:54,980
That's what we're supposed to do.

286
00:26:54,980 --> 00:26:57,980
That's something we should do on our birthday.

287
00:26:57,980 --> 00:27:00,980
Also something we should do on New Year's Eve.

288
00:27:00,980 --> 00:27:05,980
That's what we're supposed to do on New Year's Eve.

289
00:27:05,980 --> 00:27:08,980
That's why we make these resolutions.

290
00:27:08,980 --> 00:27:11,980
But when you wish everyone a happy New Year's,

291
00:27:11,980 --> 00:27:13,980
many of you have probably heard me.

292
00:27:13,980 --> 00:27:16,980
Some of you have probably heard me with this talk so far.

293
00:27:16,980 --> 00:27:23,980
There are four things that you should keep in mind that we should wish each other.

294
00:27:23,980 --> 00:27:30,980
There's four things that we're supposed to bring as well to New Year's Eve.

295
00:27:30,980 --> 00:27:32,980
I'm going to talk to you about New Year's Eve,

296
00:27:32,980 --> 00:27:34,980
because this is what we wish for New Year's Eve.

297
00:27:34,980 --> 00:27:37,980
This is what we want to change.

298
00:27:37,980 --> 00:27:39,980
To find a point where we can see,

299
00:27:39,980 --> 00:27:42,980
I'm going to be different this year.

300
00:27:42,980 --> 00:27:44,980
I'm going to do something this year.

301
00:27:44,980 --> 00:27:51,980
So it's just to actually find a new year.

302
00:27:51,980 --> 00:27:59,980
So these four things, a wisdom, effort,

303
00:27:59,980 --> 00:28:10,980
a self-perstraint, a composure, a kind of an internunciation.

304
00:28:10,980 --> 00:28:22,980
So I've actually, for the actual new year,

305
00:28:22,980 --> 00:28:27,980
I did the following year.

306
00:28:27,980 --> 00:28:29,980
Just a few days before the New Year's Eve.

307
00:28:29,980 --> 00:28:35,980
I did a guided meditation piece.

308
00:28:35,980 --> 00:28:42,980
So I'd like to guide you guys through this one.

309
00:28:42,980 --> 00:28:47,980
But if you want to look at the full one,

310
00:28:47,980 --> 00:28:50,980
you can look at New Year's meditation to something.

311
00:28:50,980 --> 00:28:51,980
It's a really good talk.

312
00:28:51,980 --> 00:28:52,980
I've got it working well.

313
00:28:52,980 --> 00:28:54,980
I like to do it very well.

314
00:28:54,980 --> 00:28:57,980
But some talks I think they do kind of bland.

315
00:28:57,980 --> 00:29:00,980
This one I think was pretty good.

316
00:29:00,980 --> 00:29:02,980
It was much more guided meditation.

317
00:29:02,980 --> 00:29:04,980
So that's what's really good.

318
00:29:04,980 --> 00:29:07,980
The first is to close your eyes.

319
00:29:07,980 --> 00:29:08,980
Start meditating.

320
00:29:08,980 --> 00:29:09,980
Turn off Facebook.

321
00:29:09,980 --> 00:29:11,980
Stop taking pictures.

322
00:29:11,980 --> 00:29:13,980
Not that the pictures are unwanted.

323
00:29:13,980 --> 00:29:16,980
At this point, let's switch to meditation.

324
00:29:16,980 --> 00:29:21,980
Close your eyes.

325
00:29:21,980 --> 00:29:26,980
And we start with wisdom.

326
00:29:26,980 --> 00:29:31,980
Wisdom starts by seeing what you need.

327
00:29:31,980 --> 00:29:35,980
So you close your eyes and say, what am I looking at?

328
00:29:35,980 --> 00:29:37,980
What am I looking at?

329
00:29:37,980 --> 00:29:43,980
When you close your eyes, you're really waiting to look at them.

330
00:29:43,980 --> 00:29:47,980
And you can focus on love.

331
00:29:47,980 --> 00:29:49,980
You can focus on death.

332
00:29:49,980 --> 00:29:55,980
You can focus on the good there.

333
00:29:55,980 --> 00:30:00,980
But for wisdom, we're going to focus on reality.

334
00:30:00,980 --> 00:30:03,980
We're going to focus on the experiences.

335
00:30:03,980 --> 00:30:11,980
Our experiences from science.

336
00:30:11,980 --> 00:30:23,980
So we have to start by breaking reality up into its components.

337
00:30:23,980 --> 00:30:27,980
If you start recognizing the things that the experience of your eyes,

338
00:30:27,980 --> 00:30:30,980
you can't do that immediately.

339
00:30:30,980 --> 00:30:31,980
Close your eyes.

340
00:30:31,980 --> 00:30:32,980
It's not clear what am I looking at?

341
00:30:32,980 --> 00:30:33,980
What is it?

342
00:30:33,980 --> 00:30:35,980
I need direction.

343
00:30:35,980 --> 00:30:37,980
The Buddha provided this direction.

344
00:30:37,980 --> 00:30:40,980
I don't think everyone knows this practice of Buddhism.

345
00:30:40,980 --> 00:30:43,980
He talked about mindfulness and people know that.

346
00:30:43,980 --> 00:30:47,980
But he also talked about something called the four foundations of mindfulness.

347
00:30:47,980 --> 00:30:49,980
These are important.

348
00:30:49,980 --> 00:30:52,980
They're from the Buddha himself.

349
00:30:52,980 --> 00:30:57,980
If you take them as a very useful tool,

350
00:30:57,980 --> 00:31:03,980
you'll be making sense in your own experience.

351
00:31:03,980 --> 00:31:09,980
The four foundations of mindfulness set the time of the body.

352
00:31:09,980 --> 00:31:17,980
All of our movements and sensations in the body are feelings,

353
00:31:17,980 --> 00:31:22,980
the pain, the pleasure, the calm, the thoughts,

354
00:31:22,980 --> 00:31:27,980
the good thoughts, the bad thoughts, the good things.

355
00:31:27,980 --> 00:31:32,980
And the dhammas.

356
00:31:32,980 --> 00:31:37,980
So the dhammas are a whole bunch of stuff that really encapsulates the path

357
00:31:37,980 --> 00:31:42,980
that we have to follow to become the Buddha.

358
00:31:42,980 --> 00:31:46,980
But these four are a good guide.

359
00:31:46,980 --> 00:31:49,980
The fourth one is sort of open-ended.

360
00:31:49,980 --> 00:31:53,980
It's a lead to the goal.

361
00:31:53,980 --> 00:31:55,980
So the bhavana.

362
00:31:55,980 --> 00:31:57,980
So the bhavana.

363
00:31:57,980 --> 00:32:00,980
It's just very similar.

364
00:32:00,980 --> 00:32:02,980
So we start with the body.

365
00:32:02,980 --> 00:32:06,980
Normally, I teach people to focus on the stomach because it's part of the body

366
00:32:06,980 --> 00:32:10,980
that we're always going to experience because it's effective.

367
00:32:10,980 --> 00:32:20,980
When it rises, we're going to look forward to seeing the body.

368
00:32:20,980 --> 00:32:26,980
Not at large, it's just a new mind.

369
00:32:26,980 --> 00:32:30,980
It gets you to start to recognize the body.

370
00:32:30,980 --> 00:32:35,980
And you can just say to yourself, it's a good system.

371
00:32:35,980 --> 00:32:38,980
So lots of different meditations as mentors, the mentor here,

372
00:32:38,980 --> 00:32:41,980
who you're using is just based on it.

373
00:32:41,980 --> 00:32:47,980
So we have a feeling such a good thing,

374
00:32:47,980 --> 00:32:52,980
that we're going to have to look forward to.

375
00:32:52,980 --> 00:32:56,980
We thought it's really safe thinking.

376
00:32:56,980 --> 00:33:24,980
So we're kind of getting ahead of myself, but with wisdom.

377
00:33:24,980 --> 00:33:28,980
Let's start with this way.

378
00:33:28,980 --> 00:33:33,980
So wisdom is just learning how to see things.

379
00:33:33,980 --> 00:33:38,980
Learning how to see what's going to start with the stomach.

380
00:33:38,980 --> 00:33:41,980
The other ones we can use to talk about the other ones.

381
00:33:41,980 --> 00:33:43,980
So for effort, the second one.

382
00:33:43,980 --> 00:33:49,980
This is when you start to expand your awareness and become aware of everything else.

383
00:33:49,980 --> 00:33:55,980
So wisdom would just understand that to be the basic idea,

384
00:33:55,980 --> 00:33:59,980
seeing things instead of this is when we start to see the awareness.

385
00:33:59,980 --> 00:34:03,980
But we need effort, which means we have to apply it to everything.

386
00:34:03,980 --> 00:34:06,980
It's not enough to sit and meditate on a single object,

387
00:34:06,980 --> 00:34:09,980
because so many other things are going to be moving away.

388
00:34:09,980 --> 00:34:13,980
So this is when you feel pain, and you feel happiness.

389
00:34:13,980 --> 00:34:16,980
Incorporating them into your practice means effort.

390
00:34:16,980 --> 00:34:19,980
This is how we accomplish it.

391
00:34:19,980 --> 00:34:22,980
We cannot get stuck on one thing.

392
00:34:22,980 --> 00:34:31,980
We cannot get stuck on anything.

393
00:34:31,980 --> 00:34:36,980
So with effort, we look at everything inside of it.

394
00:34:36,980 --> 00:34:48,980
The third one, the composure, and it means then expanding it even beyond ourselves

395
00:34:48,980 --> 00:34:52,980
to our experiences of the world around us.

396
00:34:52,980 --> 00:34:57,980
And we feel the heat or the cold in the room,

397
00:34:57,980 --> 00:35:01,980
and hear the noises in the room, and hear my speech.

398
00:35:01,980 --> 00:35:09,980
And then the birds are the crickets, and the deer turn.

399
00:35:09,980 --> 00:35:12,980
To meditate on them as well.

400
00:35:12,980 --> 00:35:16,980
And here, see here.

401
00:35:16,980 --> 00:35:23,980
Can you feel this feeling right there?

402
00:35:23,980 --> 00:35:34,980
And this is called composure, because it prevents reaction to your experiences.

403
00:35:34,980 --> 00:35:37,980
Whether it be attachment, or it will be a version,

404
00:35:37,980 --> 00:35:40,980
it keeps you level head, it keeps you a piece.

405
00:35:40,980 --> 00:35:43,980
It keeps you three years old.

406
00:35:43,980 --> 00:35:53,980
It keeps you three years old.

407
00:35:53,980 --> 00:36:02,980
It keeps you three years old.

408
00:36:02,980 --> 00:36:21,980
And so we don't want to get caught up in them and become controlled by them.

409
00:36:21,980 --> 00:36:23,980
And the final one is renunciation.

410
00:36:23,980 --> 00:36:33,980
So renunciation has to do with our reaction.

411
00:36:33,980 --> 00:36:38,980
So this is part of the dumber.

412
00:36:38,980 --> 00:36:40,980
Beyond just our inner experience and the outer experience,

413
00:36:40,980 --> 00:36:44,980
we have our reactions to all this.

414
00:36:44,980 --> 00:36:52,980
And as I said, the idea is to be injected and not react.

415
00:36:52,980 --> 00:37:00,980
But it's important to be able to understand our reaction,

416
00:37:00,980 --> 00:37:06,980
to understand why we refer what it's like to react.

417
00:37:06,980 --> 00:37:10,980
Because if you don't react to anything,

418
00:37:10,980 --> 00:37:13,980
you're not saying right here.

419
00:37:13,980 --> 00:37:21,980
But when we do react, this is where our practice begins in terms of renunciation.

420
00:37:21,980 --> 00:37:28,980
Our practice is about giving up our attachments,

421
00:37:28,980 --> 00:37:36,980
giving up our addictions, giving up our versions, giving up our partial addictions.

422
00:37:36,980 --> 00:37:45,980
So we incorporate these things into our practice.

423
00:37:45,980 --> 00:37:52,980
We're not trying to judge any of this.

424
00:37:52,980 --> 00:37:55,980
We're not trying to judge any of this.

425
00:37:55,980 --> 00:38:00,980
We're just trying to study it.

426
00:38:00,980 --> 00:38:03,980
We're not not thinking to think.

427
00:38:03,980 --> 00:38:05,980
We don't get upset about our upset.

428
00:38:05,980 --> 00:38:08,980
We don't get angry about our anger.

429
00:38:08,980 --> 00:38:13,980
We don't have to be afraid about fear.

430
00:38:13,980 --> 00:38:16,980
We don't have to be afraid about fear.

431
00:38:16,980 --> 00:38:19,980
We don't have to be afraid about fear.

432
00:38:19,980 --> 00:38:24,980
We don't have to be afraid about fear.

433
00:38:24,980 --> 00:38:29,980
We don't have to be afraid about fear.

434
00:38:29,980 --> 00:38:33,980
But at least for things,

435
00:38:33,980 --> 00:38:40,980
we can see how they very much fit into our insight meditation practice.

436
00:38:40,980 --> 00:38:43,980
We're a good way of understanding.

437
00:38:43,980 --> 00:38:48,980
We're a good way of looking at the framework of the first that people found there.

438
00:38:48,980 --> 00:38:53,980
In order to get a sense of the quality of the practice,

439
00:38:53,980 --> 00:38:58,980
and the fourth step of the time of the third time of the third time.

440
00:38:58,980 --> 00:39:02,980
Okay, I think I've talked just about long enough.

441
00:39:02,980 --> 00:39:05,980
Maybe we want to chat now.

442
00:39:05,980 --> 00:39:09,980
Part of teaching the Dhamma and studying the Dhamma is conversation.

443
00:39:09,980 --> 00:39:11,980
You never just be one star.

444
00:39:11,980 --> 00:39:17,980
You never have any questions or comments or concerns about what I'm saying.

445
00:39:17,980 --> 00:39:23,980
You just want to tell me I really can't teach birth beings face to face.

446
00:39:23,980 --> 00:39:28,980
Always happy to talk to people about their experiences.

447
00:39:28,980 --> 00:39:30,980
Thank you all for coming.

448
00:39:30,980 --> 00:39:32,980
Happy meeting you.

449
00:39:32,980 --> 00:39:49,980
I wish you could have brought that up earlier.

450
00:39:49,980 --> 00:39:58,980
It's lower now.

451
00:39:58,980 --> 00:40:13,980
Except that also turns down.

452
00:40:13,980 --> 00:40:14,980
You know what I should do not?

453
00:40:14,980 --> 00:40:18,980
What I did, I should do this way.

454
00:40:18,980 --> 00:40:23,980
And the end.

455
00:40:23,980 --> 00:40:30,980
Because I want voice to be high.

456
00:40:30,980 --> 00:40:38,980
And so I think I can turn this back into a window.

457
00:40:38,980 --> 00:40:52,980
Let me know if it's still to them, but I think it should be quieter now.

458
00:40:52,980 --> 00:40:59,980
For me, on my fewer, I turn off all the sounds except for voice.

459
00:40:59,980 --> 00:41:02,980
And so I don't hear anything but your voice.

460
00:41:02,980 --> 00:41:06,980
So it's good on my end.

461
00:41:06,980 --> 00:41:08,980
I like the ambient.

462
00:41:08,980 --> 00:41:10,980
I thought the crickets were kind of a nice touch.

463
00:41:10,980 --> 00:41:12,980
It's just their way to loud.

464
00:41:12,980 --> 00:41:16,980
If I keep my standards set up, they're too loud on the audio stream.

465
00:41:16,980 --> 00:41:23,980
So I just ruined that recording.

466
00:41:23,980 --> 00:41:26,980
I forgot about the audience audio stream.

467
00:41:26,980 --> 00:41:33,980
I did link it to everybody who couldn't come in today.

468
00:41:33,980 --> 00:41:36,980
But your audio is not very good.

469
00:41:36,980 --> 00:41:38,980
It's not like my audio.

470
00:41:38,980 --> 00:41:41,980
My audio is direct from the source.

471
00:41:41,980 --> 00:41:48,980
The audio you're getting is through the vbox or something else.

472
00:41:48,980 --> 00:41:53,980
Not as good.

473
00:41:53,980 --> 00:41:54,980
Because I upload it.

474
00:41:54,980 --> 00:42:02,980
I watched that video that you made in the recording that you made in Simon and it was clipping as well.

475
00:42:02,980 --> 00:42:05,980
Distorted.

476
00:42:05,980 --> 00:42:10,980
If it ever goes to red, if you see the thing above my head ever turned red, that's bad.

477
00:42:10,980 --> 00:42:11,980
That means it's clipping.

478
00:42:11,980 --> 00:42:18,980
It means it's reached its limit and it just gets distorted.

479
00:42:18,980 --> 00:42:20,980
More red is good.

480
00:42:20,980 --> 00:42:21,980
It wasn't that bad.

481
00:42:21,980 --> 00:42:30,980
It was more of a red than anything.

482
00:42:30,980 --> 00:42:48,980
It's got a sound gate or something.

483
00:42:48,980 --> 00:43:08,980
This is the way that is.

484
00:43:08,980 --> 00:43:27,980
Oh, I'm too quiet.

485
00:43:27,980 --> 00:43:30,980
That's interesting.

486
00:43:30,980 --> 00:43:36,980
Because I wanted to be careful.

487
00:43:36,980 --> 00:43:43,980
Simon was getting red bars last time.

488
00:43:43,980 --> 00:43:52,980
Maybe I have to turn voice up in the volume.

489
00:43:52,980 --> 00:44:14,980
There are so many variables.

490
00:44:14,980 --> 00:44:30,980
So that's good.

491
00:44:30,980 --> 00:44:56,980
Thank you.

492
00:44:56,980 --> 00:45:10,980
I'm going to say good night.

493
00:45:10,980 --> 00:45:36,980
I'm going to say good night.

