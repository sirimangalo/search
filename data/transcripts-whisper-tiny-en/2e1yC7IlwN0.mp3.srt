1
00:00:00,000 --> 00:00:09,000
Should we ever explore things intentionally that arise naturally during the meditation, contrary to simply observing?

2
00:00:16,000 --> 00:00:21,000
I think it can be at times useful.

3
00:00:21,000 --> 00:00:28,000
I think the times I can think of where it's useful is when you're just banging your head against the wall.

4
00:00:28,000 --> 00:00:31,000
And it feels like you're not getting anywhere.

5
00:00:31,000 --> 00:00:35,000
And the meditation is not progressing.

6
00:00:35,000 --> 00:00:49,000
I think it can be useful conventionally useful to spend some time,

7
00:00:49,000 --> 00:00:53,000
a little bit of time sort of figuring out what you're doing wrong.

8
00:00:53,000 --> 00:01:00,000
This is called We Mungsat, which is in one sense anyway.

9
00:01:00,000 --> 00:01:05,000
It refers to this sort of stepping back and taking a look at things.

10
00:01:05,000 --> 00:01:12,000
But I think that's the only point where I would recommend this.

11
00:01:12,000 --> 00:01:14,000
You know, explore things intentionally.

12
00:01:14,000 --> 00:01:19,000
You're kind of misleading with that terminology because we are exploring things.

13
00:01:19,000 --> 00:01:23,000
The usage of that word is that the verbiage there is kind of sneaky.

14
00:01:23,000 --> 00:01:25,000
You know, I'm not blaming, not criticizing exactly.

15
00:01:25,000 --> 00:01:30,000
But you have to be careful that you're not tricking yourself by using that words

16
00:01:30,000 --> 00:01:35,000
into backing up your sort of argument.

17
00:01:35,000 --> 00:01:38,000
Because you're not talking about exploring.

18
00:01:38,000 --> 00:01:43,000
You're talking about...

19
00:01:43,000 --> 00:01:45,000
I mean, I don't know.

20
00:01:45,000 --> 00:01:46,000
Exploring could go either way.

21
00:01:46,000 --> 00:02:01,000
It's a fairly neutral word, but your implication is that you want to sort of prod and delve,

22
00:02:01,000 --> 00:02:09,000
which sort of from our point of view implies leaving the middle way.

23
00:02:09,000 --> 00:02:16,000
Because the middle way would be something a little bit less than what you're suggesting.

24
00:02:16,000 --> 00:02:18,000
But it still could be called exploration.

25
00:02:18,000 --> 00:02:23,000
So what we're doing is an exploration, and it's intentional.

26
00:02:23,000 --> 00:02:25,000
It's just not...

27
00:02:25,000 --> 00:02:35,000
You know, a prodding in the sense of going beyond a simple awareness of things as they are.

28
00:02:35,000 --> 00:02:39,000
What we're doing, as I said in the last question, is observing.

29
00:02:39,000 --> 00:02:44,000
We're conducting a science experiment, and we're going to record our observations.

30
00:02:44,000 --> 00:02:49,000
And in this science experiment, as with a perfect science experiment,

31
00:02:49,000 --> 00:02:54,000
the conclusions will be obvious from the observations.

32
00:02:54,000 --> 00:03:00,000
You don't interpret the observations beyond what is obvious.

33
00:03:00,000 --> 00:03:05,000
So that's a perfect science experiment where you don't have to interpret,

34
00:03:05,000 --> 00:03:11,000
where anyone who looks at the data can come to the same conclusions.

35
00:03:11,000 --> 00:03:15,000
So that is what happens in meditation.

36
00:03:15,000 --> 00:03:17,000
The conclusions come naturally.

37
00:03:17,000 --> 00:03:21,000
So all that's important is the proper observation.

38
00:03:21,000 --> 00:03:26,000
I think this question comes up because people feel that it's not enough.

39
00:03:26,000 --> 00:03:31,000
They're not satisfied by simply observing.

40
00:03:31,000 --> 00:03:36,000
And so they're looking for something more.

41
00:03:36,000 --> 00:03:39,000
That is...

42
00:03:39,000 --> 00:03:42,000
I mean, that is caused by something completely different.

43
00:03:42,000 --> 00:03:53,000
That's caused by sort of ambition and impatience, frustration, boredom,

44
00:03:53,000 --> 00:03:55,000
all of these things.

45
00:03:55,000 --> 00:04:08,000
And this sort of preconception that meditation has to be more than simply observing.

46
00:04:08,000 --> 00:04:16,000
Lack of confidence or trust and the inability to conceive of the fact

47
00:04:16,000 --> 00:04:19,000
that simply observing things is going to get either results.

48
00:04:19,000 --> 00:04:25,000
And it's hard to believe that by doing something so silly as just reminding yourself,

49
00:04:25,000 --> 00:04:30,000
this, this, this, this, this, that that's actually going to do anything.

50
00:04:30,000 --> 00:04:35,000
Most people are like, wow, you know everything about myself already.

51
00:04:35,000 --> 00:04:38,000
I mean, Wes, I've been... how many years I've been watching myself?

52
00:04:38,000 --> 00:04:41,000
What could I possibly learn?

53
00:04:41,000 --> 00:04:43,000
So there's this one teacher in Bangkok.

54
00:04:43,000 --> 00:04:45,000
Some of you may have heard me say this.

55
00:04:45,000 --> 00:04:50,000
He says, it told a 95-year-old woman.

56
00:04:50,000 --> 00:04:53,000
They said, so, you know yourself pretty well, right?

57
00:04:53,000 --> 00:04:55,000
Yeah.

58
00:04:55,000 --> 00:04:58,000
You know, how well you know your hand.

59
00:04:58,000 --> 00:05:00,000
And she said, my hand, you know, quite well.

60
00:05:00,000 --> 00:05:02,000
I've used my hand all my life.

61
00:05:02,000 --> 00:05:06,000
And he said, how many knuckles do you have in one hand?

62
00:05:06,000 --> 00:05:07,000
Don't count.

63
00:05:07,000 --> 00:05:10,000
Tell me, how many knuckles do you have?

64
00:05:10,000 --> 00:05:12,000
Couldn't do it.

65
00:05:12,000 --> 00:05:15,000
She couldn't do it. No idea.

66
00:05:15,000 --> 00:05:16,000
I don't even remember.

67
00:05:16,000 --> 00:05:22,000
It's 14, 15, something like that.

68
00:05:22,000 --> 00:05:26,000
Just kind of as a funny joke, but our minds are very much like that.

69
00:05:26,000 --> 00:05:29,000
You know, of course we've been spending so much time with our minds,

70
00:05:29,000 --> 00:05:31,000
but we're not really investigating.

71
00:05:31,000 --> 00:05:34,000
And it actually is something quite simple.

72
00:05:34,000 --> 00:05:37,000
And the answers are quite obvious.

73
00:05:37,000 --> 00:05:40,000
We've just, it's amazing that we never look.

74
00:05:40,000 --> 00:05:44,000
It's incredible that really incredible that we spend all this time

75
00:05:44,000 --> 00:05:46,000
and didn't really see any of this.

76
00:05:46,000 --> 00:05:47,000
We were never looking.

77
00:05:47,000 --> 00:05:50,000
We were never trying to understand.

78
00:05:50,000 --> 00:05:52,000
Or maybe we gave up too soon.

79
00:05:52,000 --> 00:05:54,000
We didn't have the tools to understand.

80
00:05:54,000 --> 00:05:57,000
I suppose there always was a time where we wanted to understand.

81
00:05:57,000 --> 00:06:03,000
We just never really had the tools or clarity necessary to think of it

82
00:06:03,000 --> 00:06:07,000
as a science experiment where you need objective observation.

83
00:06:07,000 --> 00:06:10,000
You can't just think, you know, question.

84
00:06:10,000 --> 00:06:12,000
What is the answer?

85
00:06:12,000 --> 00:06:17,000
You skip the most important part and that's observing.

86
00:06:17,000 --> 00:06:20,000
But here that's exactly what's being asked here.

87
00:06:20,000 --> 00:06:22,000
Should you do more than observe?

88
00:06:22,000 --> 00:06:23,000
No.

89
00:06:23,000 --> 00:06:25,000
Science doesn't work that way.

90
00:06:25,000 --> 00:06:27,000
And there's a reason why it doesn't.

91
00:06:27,000 --> 00:06:34,000
Anything else that you do, you know, is manipulating the experiment.

92
00:06:34,000 --> 00:06:38,000
And that's why you double blinds and everything is to avoid all that.

93
00:06:38,000 --> 00:07:05,000
Anyway, it's all good.

