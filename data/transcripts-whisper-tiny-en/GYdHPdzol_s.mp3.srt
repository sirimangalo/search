1
00:00:00,000 --> 00:00:09,520
Hi, today I'll be talking about the number four reason why everyone should practice meditation.

2
00:00:09,520 --> 00:00:14,040
The number four reason why everyone should practice meditation is because meditation allows

3
00:00:14,040 --> 00:00:18,240
us to overcome all sorts of mental sickness.

4
00:00:18,240 --> 00:00:20,560
Now we have to understand what we mean here by mental sickness.

5
00:00:20,560 --> 00:00:25,240
We're not talking about those things which lead people to enter psychiatry words or

6
00:00:25,240 --> 00:00:31,400
insane as silums. We're talking about states of mind which are understood by people to

7
00:00:31,400 --> 00:00:36,000
be some sort of mental distress or disease.

8
00:00:36,000 --> 00:00:39,960
So when we understand disease in this sense, we understand it as disease, as not being

9
00:00:39,960 --> 00:00:41,160
at ease.

10
00:00:41,160 --> 00:00:52,000
So this could be from all sorts of states like depression or stress, anxiety.

11
00:00:52,000 --> 00:00:56,920
All of these things which actually nowadays people are trying to find medication as a solution.

12
00:00:56,920 --> 00:01:02,400
They're trying to use pills, they're trying to use sometimes therapy or trying to find all

13
00:01:02,400 --> 00:01:07,760
sorts of external ways of removing these mental conditions.

14
00:01:07,760 --> 00:01:10,800
And they never go out the root which is in the mind.

15
00:01:10,800 --> 00:01:15,400
So in the meditation practice we understand that the great thing about meditation

16
00:01:15,400 --> 00:01:20,080
is that we don't ever have to take medication for something which is purely mental.

17
00:01:20,080 --> 00:01:24,160
It's a mind like depression, stress, anxiety, insomnia.

18
00:01:24,160 --> 00:01:29,880
All of these states which we tend to go to see a clinical therapist or a clinical

19
00:01:29,880 --> 00:01:32,800
doctor for, we can actually remove on our own.

20
00:01:32,800 --> 00:01:36,880
And this is one thing that people maybe don't ever realize that they have this incredible

21
00:01:36,880 --> 00:01:43,200
tool which allows you to get rid of things like insomnia, depression, stress, anxiety,

22
00:01:43,200 --> 00:01:52,880
worry, fear, phobias in really a very short time and on a very real and profound level.

23
00:01:52,880 --> 00:01:59,480
But actually when we practice meditation creating this clear thought, it creates a sort

24
00:01:59,480 --> 00:02:04,120
of series of mind states which come to be a new way of looking at the world.

25
00:02:04,120 --> 00:02:11,280
So instead of looking at things as stressful, instead of looking at our situation as depressing,

26
00:02:11,280 --> 00:02:12,840
instead of looking at something as fearful.

27
00:02:12,840 --> 00:02:13,840
We simply see it for what it is.

28
00:02:13,840 --> 00:02:18,960
If it's something we're afraid of, when we say to ourselves seeing, seeing, hearing,

29
00:02:18,960 --> 00:02:24,120
hearing, or thinking, thinking, or even liking, liking, or afraid, afraid, that mind state

30
00:02:24,120 --> 00:02:30,320
disappears and it doesn't then continue, it doesn't become a tendency or become a habit.

31
00:02:30,320 --> 00:02:33,640
We get this new habit in our mind of seeing things simply as they are.

32
00:02:33,640 --> 00:02:37,120
When we are depressed, we have a situation which is depressing.

33
00:02:37,120 --> 00:02:41,720
We simply say to ourselves thinking, thinking, disliking or stressed or so on, we remind

34
00:02:41,720 --> 00:02:46,680
ourselves of the true nature of it and we don't give it the power to create this mental

35
00:02:46,680 --> 00:02:52,680
sickness, mental disease or mental upset, however you want to call it, this state of depression

36
00:02:52,680 --> 00:02:55,080
which can actually lead people to kill themselves.

37
00:02:55,080 --> 00:03:00,360
Actually if people were to practice meditation, there would be no need to ever consider

38
00:03:00,360 --> 00:03:01,680
to try to end your life.

39
00:03:01,680 --> 00:03:09,040
There would be no idea that something was unpleasant or something was bad or depressing.

40
00:03:09,040 --> 00:03:13,520
So like insomnia, people who suffer from insomnia, I've had many people who've cured

41
00:03:13,520 --> 00:03:18,000
themselves in insomnia, lifelong insomnia in a week of meditation.

42
00:03:18,000 --> 00:03:22,360
People who have been alcoholics are able to cure themselves, give up alcoholism, give

43
00:03:22,360 --> 00:03:27,600
up drugs, give up all sorts of addictions, all sorts of states which we would normally consider

44
00:03:27,600 --> 00:03:32,880
to be a sort of a mental sickness or a mental disease which we go to see a doctor for.

45
00:03:32,880 --> 00:03:36,680
We can actually cure by ourselves or with a help, if not by ourselves, at least with the

46
00:03:36,680 --> 00:03:39,520
help of a qualified meditation teacher.

47
00:03:39,520 --> 00:03:43,480
So this is the number four reason why everyone should practice meditation, not just those

48
00:03:43,480 --> 00:03:47,520
people who are suffering from clinical depression, but those people who suffer from these

49
00:03:47,520 --> 00:03:56,200
general sense of disease or discomfort which arises really for everybody.

50
00:03:56,200 --> 00:04:00,280
So thank you for tuning in this is number four and please look forward to the rest of

51
00:04:00,280 --> 00:04:01,280
my videos in the future.

52
00:04:01,280 --> 00:04:14,280
Thanks.

