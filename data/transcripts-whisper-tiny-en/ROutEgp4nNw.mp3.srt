1
00:00:00,000 --> 00:00:28,080
Good morning.

2
00:00:28,080 --> 00:00:36,080
The car of the Buddha's teaching is the Four Noble Truths.

3
00:00:36,080 --> 00:00:45,760
Four truths about reality.

4
00:00:45,760 --> 00:00:47,800
They are noble.

5
00:00:47,800 --> 00:00:57,600
They are the truths of the noble ones, they are the truths that make those people who understand

6
00:00:57,600 --> 00:01:09,840
them noble because of their understanding.

7
00:01:09,840 --> 00:01:21,800
All four of the truths relate to the concept of dukkha or suffering.

8
00:01:21,800 --> 00:01:24,160
That's why they're noble.

9
00:01:24,160 --> 00:01:37,000
Because they accomplish a noble goal, the goal of freeing one from suffering.

10
00:01:37,000 --> 00:01:49,040
They relate to it as truly an important, valuable, precious, and that is peace and happiness

11
00:01:49,040 --> 00:01:58,880
and freedom from suffering.

12
00:01:58,880 --> 00:02:08,160
Suffering is a word that you can use to describe all the problems that might ever arise.

13
00:02:08,160 --> 00:02:10,400
Some things are problem because it relates to suffering.

14
00:02:10,400 --> 00:02:23,680
If it didn't relate to suffering, it wouldn't really be a problem.

15
00:02:23,680 --> 00:02:24,840
It's an important word.

16
00:02:24,840 --> 00:02:36,680
It's a v-word that we need to pay attention to, keep our attention on.

17
00:02:36,680 --> 00:02:58,440
It's a suffering is to be fully understood, barinaya.

18
00:02:58,440 --> 00:03:06,520
And that's just it, that suffering isn't fully understood.

19
00:03:06,520 --> 00:03:11,400
It's not that we can't escape suffering, it's that we can't understand it.

20
00:03:11,400 --> 00:03:14,040
We don't understand it.

21
00:03:14,040 --> 00:03:27,720
In fact, our understanding is often very much focused on escape, avoidance, elimination.

22
00:03:27,720 --> 00:03:38,400
How can we get rid of suffering?

23
00:03:38,400 --> 00:03:45,440
Because our understanding of suffering is on a very rudimentary level generally.

24
00:03:45,440 --> 00:03:59,520
There are four ways of understanding suffering.

25
00:03:59,520 --> 00:04:16,080
So we have understanding is understanding suffering is as we call dukkawedana, dukka, painful or suffering.

26
00:04:16,080 --> 00:04:26,400
Vedana means feeling, as a painful feeling, that's suffering.

27
00:04:26,400 --> 00:04:35,160
Maybe mental pain, maybe physical pain, that's suffering, how do we get rid of it?

28
00:04:35,160 --> 00:04:40,040
It sounds right, it sounds like how we should understand suffering, if we didn't have

29
00:04:40,040 --> 00:04:45,720
any painful or mental or physical feelings, that would be great.

30
00:04:45,720 --> 00:04:53,000
Problem isn't that it's wrong, the problem is that it's simplistic, it belies a lack of

31
00:04:53,000 --> 00:05:09,400
understanding, a positive understanding, a limited understanding, because if all you see

32
00:05:09,400 --> 00:05:17,760
is dukkawedana, then all you'll do is try to run away in the escape suffering, find ways

33
00:05:17,760 --> 00:05:38,640
to avoid it, of course the answer is simple, fix it, remove it, avoid it, escape it.

34
00:05:38,640 --> 00:05:44,560
And so we spend all our time trying to avoid suffering, we build up habits of avoidance,

35
00:05:44,560 --> 00:05:52,880
we build up defense mechanisms and structures that keep us protected from suffering, spend

36
00:05:52,880 --> 00:06:07,240
a lot of our lives, a lot of our time afraid of suffering, and if we're lucky or good

37
00:06:07,240 --> 00:06:21,120
karma, we can for some time avoid suffering, and appears that that's a solution, find

38
00:06:21,120 --> 00:06:29,520
a way to live, that allows you to avoid the problem, but as Buddhist meditators we know

39
00:06:29,520 --> 00:06:32,760
this isn't sustainable.

40
00:06:32,760 --> 00:06:43,720
The second way to understand suffering is called dukkawsa bhava, sambhava means something

41
00:06:43,720 --> 00:06:53,160
that has existence, existential phenomenon, a thing that really exists, but here the

42
00:06:53,160 --> 00:07:03,680
meaning is that suffering is inescapable reality, it's there, it exists, it is no matter

43
00:07:03,680 --> 00:07:20,240
how you try to run from it, no matter how much you try to make it, so it isn't, it is.

44
00:07:20,240 --> 00:07:36,040
We try to avoid pain and suffering, we still get sick, we still get old, we still die,

45
00:07:36,040 --> 00:07:51,160
we still get hungry and thirsty, we still get afraid and disappointed, avoiding suffering

46
00:07:51,160 --> 00:08:09,360
is not only unsustainable and predictable, it's also incomplete and unsuccessful, we don't

47
00:08:09,360 --> 00:08:23,720
really realize how much we suffer, how much we limit our state of peace and happiness,

48
00:08:23,720 --> 00:08:41,080
how limited is our peace of mind, we don't realize until we take up the practice of observing

49
00:08:41,080 --> 00:08:57,920
our minds, observing reality and developing wisdom, and when we do that we become to

50
00:08:57,920 --> 00:09:11,280
see suffering more clearly, we come to see that this is a part of a life, it's unavoidable.

51
00:09:11,280 --> 00:09:24,000
This is actually the reason dukkawsa bhava is the main reason why people come to practice

52
00:09:24,000 --> 00:09:33,160
religion, spirituality, they can't figure out a way to free themselves from suffering,

53
00:09:33,160 --> 00:09:39,760
so they turn to more spiritual practices, often spiritual practices designed to facilitate

54
00:09:39,760 --> 00:09:47,440
avoidance, many meditation practices are just to get away from the suffering, so still

55
00:09:47,440 --> 00:09:54,320
in the frame of mind that it's just dukkawed and that's something you can avoid if you

56
00:09:54,320 --> 00:10:06,960
figure out how, but when people become desperate suffering is so great they realize they

57
00:10:06,960 --> 00:10:15,480
can't avoid it, or else they have a keen insight and understanding if people have

58
00:10:15,480 --> 00:10:26,520
someone has some kind of high-mindedness to understand, this isn't the right way, avoiding

59
00:10:26,520 --> 00:10:31,720
isn't the right way, running away from suffering isn't the right way, it's not sustainable

60
00:10:31,720 --> 00:10:40,200
they get this sense, either from experience or from introspection, it's a major reason

61
00:10:40,200 --> 00:10:46,440
why someone might look for a better way, rather than avoid suffering, how can I understand

62
00:10:46,440 --> 00:10:56,680
it, how can I free myself from its power, a big part of the Buddhist path, the path of

63
00:10:56,680 --> 00:11:07,040
purification is this shift from trying to run away from suffering to letting go of our concern

64
00:11:07,040 --> 00:11:15,040
about suffering, letting go of our concern about whether we feel pain or don't feel pain,

65
00:11:15,040 --> 00:11:20,240
whether things are the way we want or not the way we want, rather than trying to always

66
00:11:20,240 --> 00:11:30,360
get what we want, the shift to letting go of wanting, placing the blame back on our desire

67
00:11:30,360 --> 00:11:42,480
and a big part of the fruit of the practice is the realization of true happiness not

68
00:11:42,480 --> 00:11:51,680
through escaping suffering, but by changing the way we look at it and realizing true peace

69
00:11:51,680 --> 00:12:02,760
beyond what we ever thought possible, irrespective of our experiences, regardless of whether

70
00:12:02,760 --> 00:12:21,240
we feel pain or have unpleasant thoughts or memories, this is the third type of understanding,

71
00:12:21,240 --> 00:12:32,400
let's do kalakana once we begin to practice spirituality, we've come to see that there

72
00:12:32,400 --> 00:12:39,880
must be a better way when we come to see that there's nothing to do with what we experience,

73
00:12:39,880 --> 00:12:45,600
having realized that suffering is an unavoidable part of experience, we come to realize

74
00:12:45,600 --> 00:12:56,320
that it really isn't about whether I experience this or that at all, suffering is simply

75
00:12:56,320 --> 00:13:07,520
a part of our tendency to cling to things, our tendency to misunderstand and how happiness

76
00:13:07,520 --> 00:13:19,320
is found, peace is found, so we realize we're just biased towards this or that experience

77
00:13:19,320 --> 00:13:28,920
when in fact that bias is the problem, not that we don't get what we want, but that we

78
00:13:28,920 --> 00:13:39,560
want and like in this, like in the first place, look kalakana is the understanding that

79
00:13:39,560 --> 00:13:51,120
comes only through spirituality, that suffering is a characteristic of all things, in the

80
00:13:51,120 --> 00:13:57,520
sense that it doesn't matter what you're referring to, all phenomenon can cause us pain

81
00:13:57,520 --> 00:14:06,600
and suffering, not because of their nature exactly, but because of our misunderstanding

82
00:14:06,600 --> 00:14:12,400
of their nature, that it's worth clinging to, that some benefit comes from clinging,

83
00:14:12,400 --> 00:14:21,240
from craving, from bias, right, there's nothing wrong with reality, what's wrong is

84
00:14:21,240 --> 00:14:31,920
how we understand it, misunderstanding, the idea that we can gain something, the idea that

85
00:14:31,920 --> 00:14:44,440
somehow it's going to satisfy us, to kalakana, when the meditator realizes that it's

86
00:14:44,440 --> 00:14:52,880
a characteristic of all things, the perception of things shifts, and they begin to let

87
00:14:52,880 --> 00:15:04,200
go, begin to fly, to free themselves from the clinging to this or that experience, constant

88
00:15:04,200 --> 00:15:24,920
bouncing around, pushing, pulling, that's the third type of understanding that we get

89
00:15:24,920 --> 00:15:34,640
through meditation that causes us to let go, and the fourth is, when the mind lets go,

90
00:15:34,640 --> 00:15:41,400
the fourth understanding is called dukasateh, the truth of suffering.

91
00:15:41,400 --> 00:15:50,920
The truth of suffering refers to the realization that nothing is worth clinging to.

92
00:15:50,920 --> 00:16:03,240
dukalakana is that all of experience, it's not one experience or another, it's our relationship

93
00:16:03,240 --> 00:16:09,600
to experience that causes suffering, and when you see that clearly enough, there is one

94
00:16:09,600 --> 00:16:17,960
moment dukasateh, the truth of suffering when it hits you, it's the culmination of

95
00:16:17,960 --> 00:16:25,400
this practice of creating understanding and clarity of mind, when one realizes just

96
00:16:25,400 --> 00:16:31,040
in a moment that nothing is worth clinging to, it's the natural evolution of the natural

97
00:16:31,040 --> 00:16:39,840
consequence of observing how clinging causes suffering, that there comes a moment where

98
00:16:39,840 --> 00:16:51,960
the mind is free of any kind of craving or clinging to anything, realizing that, craving

99
00:16:51,960 --> 00:16:58,520
is the cause of suffering, nothing is worth clinging to, clinging is the problem, and the

100
00:16:58,520 --> 00:17:09,080
mind lets go, all it takes is that one moment and that is the truth of suffering, that's

101
00:17:09,080 --> 00:17:22,920
the path that leads to freedom from suffering, and it's the moment when it becomes an

102
00:17:22,920 --> 00:17:40,600
enlightened being, a noble being, that is the essence of the Buddha's teaching, the basis

103
00:17:40,600 --> 00:17:52,440
of the formable truths, so, four types of suffering, something for us to, any meditators

104
00:17:52,440 --> 00:18:01,800
should be aware of this distinction, we're getting a sense of what we mean when we talk

105
00:18:01,800 --> 00:18:12,760
about suffering, and where we're headed, what is the goal, what is the path, also useful

106
00:18:12,760 --> 00:18:20,400
teaching for non meditators to understand that, the way we look at suffering is often

107
00:18:20,400 --> 00:18:29,560
simplistic, ineffective, and a big part of the problem, not that we can't free ourselves

108
00:18:29,560 --> 00:18:37,120
from suffering, but that in trying to free ourselves from suffering, we only cause more problems

109
00:18:37,120 --> 00:18:56,840
for ourselves, so that's the demo for this morning, thank you.

