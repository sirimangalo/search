1
00:00:00,000 --> 00:00:06,080
Welcome, everyone, to find Wednesday morning in the Deer Park.

2
00:00:09,920 --> 00:00:15,760
Today's talk will be about becoming Buddhist.

3
00:00:25,600 --> 00:00:27,600
And it's an interesting topic.

4
00:00:27,600 --> 00:00:35,040
There are actually many Buddhist teachers out there who recommend not to become Buddhist.

5
00:00:36,240 --> 00:00:40,800
And maybe not many, but there are some teachers out there who

6
00:00:46,960 --> 00:00:53,360
who believe that the word Buddhism is in itself a problem

7
00:00:53,360 --> 00:00:59,440
and is taking away from the purity of the teaching.

8
00:01:02,000 --> 00:01:08,160
So I'd like to qualify and I'm not going to deny that there's a danger in labels.

9
00:01:10,320 --> 00:01:16,160
But I'm going to qualify what I mean and try to explain why it might actually be a benefit to us

10
00:01:16,160 --> 00:01:23,920
to take on the label or the role or the identification of a Buddhist.

11
00:01:26,720 --> 00:01:30,960
First of all, what is meant by a Buddhist? There are three ways of approaching the Buddhist teaching.

12
00:01:33,520 --> 00:01:40,160
Three sorts of paths you can take when you approach what we understand to be Buddhism.

13
00:01:40,160 --> 00:01:47,120
And the first is to become a fully enlightened Buddha.

14
00:01:49,920 --> 00:01:58,960
This path is obviously the most difficult and it's also the most noble and exceptional.

15
00:01:58,960 --> 00:02:14,080
To become a Buddha takes an incredibly long time and it depends on one's skill and imperfections

16
00:02:14,080 --> 00:02:16,400
as to whether one will even come close to attaining it.

17
00:02:19,280 --> 00:02:21,920
They say that to become a perfectly enlightened Buddha

18
00:02:21,920 --> 00:02:35,360
and you have to be willing to swim through an ocean of red-hot coals.

19
00:02:36,880 --> 00:02:41,520
If suppose there were an ocean the size of the Pacific Ocean

20
00:02:42,480 --> 00:02:47,920
and instead of water it were full of red-hot coals and you knew that you could swim through it without

21
00:02:47,920 --> 00:02:57,280
dying but you'd still feel all the pain and suffering. If you had what it took to become a Buddha

22
00:02:57,280 --> 00:03:03,760
you wouldn't think twice to cross that ocean in order to become a Buddha.

23
00:03:06,800 --> 00:03:13,040
It takes an incredible determination and you might even say it takes an incredible amount of luck

24
00:03:13,040 --> 00:03:19,680
because it's something that takes such a long time that the chances of you maintaining your dedication

25
00:03:20,800 --> 00:03:26,320
for such an uncountable length of time is quite difficult. This isn't to discourage people from

26
00:03:26,320 --> 00:03:34,080
that path. It's maybe to instill some measure of realism because it seems like there's a lot

27
00:03:34,080 --> 00:03:40,320
of people who take on the vow to become a fully enlightened Buddha without really understanding what

28
00:03:40,320 --> 00:03:49,040
it entails and without really taking on those practices and that sort of dedication which is going

29
00:03:49,040 --> 00:03:59,040
to give one even a slight chance of getting there. The second path is to become enlightened for one

30
00:03:59,040 --> 00:04:11,040
itself but not teach and so what makes a Buddha a fully enlightened Buddha special is that first of

31
00:04:11,040 --> 00:04:20,000
all he's able to realize here she is able to realize the truth for themselves without anyone

32
00:04:20,000 --> 00:04:26,640
having to explain it to them. This is the extent of their perfection. They don't need a teacher.

33
00:04:26,640 --> 00:04:34,960
They're so highly developed that they're able to look at reality and to see it for what it is

34
00:04:34,960 --> 00:04:42,400
without any instruction. But the second thing is that they're also then able to teach it to

35
00:04:42,400 --> 00:04:50,880
other people. Not only do they free themselves from all attachment, all clinging, all suffering,

36
00:04:50,880 --> 00:05:00,480
but they understand clearly how they did it and how anyone else can do it and they're able to

37
00:05:00,480 --> 00:05:07,520
see the path of any other person as to what it's going to take to get them to that stage

38
00:05:09,680 --> 00:05:16,000
and so they can teach others and they can teach others perfect with perfect accuracy

39
00:05:16,000 --> 00:05:22,560
such that they know exactly what it's going to take to that person to develop.

40
00:05:25,760 --> 00:05:30,480
Now the second type here is called the Pachaykabuddha private Buddha or one who becomes

41
00:05:30,480 --> 00:05:36,720
enlightened for themselves and this sort of person is special as well because they're still

42
00:05:36,720 --> 00:05:46,240
able to realize the truth by themselves. These are many of the great seers that have been in the past.

43
00:05:47,280 --> 00:05:56,880
A good example I think could be Lao Tzu who was the founder of Daoism but he wasn't the founder

44
00:05:56,880 --> 00:06:04,560
in the sense of actually going on teaching anyone. He just left behind a text or a set of teachings

45
00:06:04,560 --> 00:06:10,720
which in the end aren't really teachings, they're just principles and then he left the world

46
00:06:13,040 --> 00:06:20,160
and so these beings can arise at any time and they go into the forest and they're able to

47
00:06:20,160 --> 00:06:28,000
realize the truth but it's a different sort of realization. They let go, they see things clearly

48
00:06:28,000 --> 00:06:41,840
but they don't have this design in terms of understanding the method to bring people to this

49
00:06:41,840 --> 00:06:46,640
realization just like all of us when we practice, often we practice according to a teacher

50
00:06:47,520 --> 00:06:53,360
and we're even less able than the Pachaykabuddha to teach anyone else.

51
00:06:53,360 --> 00:06:58,880
We were able to practice by ourselves and we're able to follow the instruction of the teacher

52
00:06:58,880 --> 00:07:03,760
but we're not able to lead other people to the same realization because we don't have the

53
00:07:03,760 --> 00:07:11,840
knowledge of the teacher. So Pachaykabuddha becomes enlightened but then doesn't teach others.

54
00:07:13,520 --> 00:07:19,840
The third type what I would identify with as a Buddhist or it's the type of Buddhists that I'm

55
00:07:19,840 --> 00:07:26,960
going to be talking about here is a person who is not able in their present condition to realize

56
00:07:26,960 --> 00:07:36,320
the truth for themselves but they take on either the Buddha or one of his enlightened disciples

57
00:07:36,320 --> 00:07:44,480
or someone who is knowledgeable about the teachings of the Buddha as a teacher and as a result

58
00:07:44,480 --> 00:07:58,320
they're able to realize the truth having been instructed. This is the most common. The difference

59
00:07:59,280 --> 00:08:07,120
between the enlightened Buddha and the fully enlightened Buddha and one of his disciples

60
00:08:08,320 --> 00:08:13,680
it really boils down to this. A fully enlightened Buddha to become a fully enlightened Buddha,

61
00:08:13,680 --> 00:08:19,920
you not only have to give up everything, you also have to understand everything, know everything.

62
00:08:21,280 --> 00:08:25,120
You have to know everything, you have to know nothing that no stone left unturned,

63
00:08:25,120 --> 00:08:31,440
you have to have come to understand the entire universe. To be a disciple of the Buddha,

64
00:08:31,440 --> 00:08:37,840
you don't have to know everything but you have to let go of everything. You have to understand

65
00:08:37,840 --> 00:08:45,520
enough about the universe to realize that there's nothing worth clinging to. When you do that,

66
00:08:45,520 --> 00:08:52,160
you become free from suffering. Once it's the realization is the same on a practical level

67
00:08:53,440 --> 00:08:58,000
because you no longer subject to attachment and addiction and suffering.

68
00:09:00,080 --> 00:09:04,800
The fully enlightened Buddha has the advantage in terms of being able to teach others and

69
00:09:04,800 --> 00:09:15,280
to the breadth of the knowledge is incomparable. Today I'm just going to be talking about this

70
00:09:15,280 --> 00:09:23,680
third type, how to become a follower of the Buddha. The benefits, as I see it, of taking on this

71
00:09:23,680 --> 00:09:35,120
label, given that there are potential disadvantages of self identification and creating some sort of

72
00:09:35,120 --> 00:09:46,880
sense of self or so on, are that by determining in your mind by making a determination that

73
00:09:46,880 --> 00:09:54,400
you're going to follow the Buddha's teaching, that the Buddha and Buddhism is your path.

74
00:10:01,680 --> 00:10:13,200
It's something that protects you. You gain both internal and external protection.

75
00:10:13,200 --> 00:10:25,040
When you become protected from evil spirits, you become protected from misfortune.

76
00:10:27,840 --> 00:10:35,040
You become protected by Buddhists, by the Buddhist religion. You become protected internally

77
00:10:35,040 --> 00:10:45,120
and externally. Evil spirits, there are malevolent forces in the world. There are

78
00:10:46,480 --> 00:10:56,160
both humans and non-humans who, through mischief, through genuine malevolence,

79
00:10:56,160 --> 00:11:07,440
will try to harm us. Having taken the Buddha and his teachings and the Sangha as our refuge,

80
00:11:08,800 --> 00:11:17,440
having become a Buddhist or one who follows the Buddha's teaching, we have this strength of

81
00:11:17,440 --> 00:11:26,320
mind and we have this, it's like entering the mafia, you have protection.

82
00:11:29,600 --> 00:11:39,440
It's to make a crude comparison because there are also benevolent forces in the universe,

83
00:11:39,440 --> 00:11:49,280
both humans and non-humans. The most benevolent are the Buddhists, those who follow the Buddha's

84
00:11:49,280 --> 00:11:57,760
teaching because of the purity of the teaching, the fact that the Buddha focused and stressed

85
00:11:58,800 --> 00:12:04,560
the purification of the mind. There are many forces out there that protect the Buddha's teaching,

86
00:12:04,560 --> 00:12:11,200
seeing that as a beneficial force in the world, and not just talking about spirits, but also

87
00:12:11,200 --> 00:12:22,320
human beings who see the benefit of the Buddha's teaching. In fact, and many times non- Buddhists

88
00:12:22,320 --> 00:12:28,960
will protect Buddhists and I've seen this. There are both sides, there are human beings

89
00:12:28,960 --> 00:12:36,160
who obviously through their ignorance or through their bigotry, they're actually malevolent

90
00:12:36,160 --> 00:12:41,360
towards the Buddha's teaching, towards Buddhism, and they see monks, they feel hostility.

91
00:12:43,760 --> 00:12:51,600
I was arrested last year and put in jail by someone who thought or was either trying to have

92
00:12:51,600 --> 00:12:59,760
me, trying to put me in trouble or else genuinely thought I was something I wasn't thought I

93
00:12:59,760 --> 00:13:07,280
was a streaker or so on. You get this, but you also get other people who are not Buddhist,

94
00:13:07,280 --> 00:13:17,760
but see the genuine sincerity, the purity, the love and compassion that is expressed by Buddhists

95
00:13:17,760 --> 00:13:21,120
and wish to protect them.

96
00:13:28,880 --> 00:13:35,120
And this is one of the benefits I would say of identifying yourself as a Buddhist.

97
00:13:35,120 --> 00:13:40,880
It's one of those, it's maybe similar to identifying yourself as a Canadian. We always have this

98
00:13:40,880 --> 00:13:49,200
joke when we go traveling. As Canadians, we always put the Canadian flag on our backpack.

99
00:13:51,200 --> 00:13:58,080
And this is because Canadians were, I don't know anymore, but they were for a long time respected

100
00:13:58,880 --> 00:14:07,360
rightfully or not rightfully as nice people, good people. And so we'd generally be given

101
00:14:07,360 --> 00:14:18,240
an easier time if they were accosted by police or so on. So what happened then is Americans when

102
00:14:18,240 --> 00:14:24,720
they would go abroad, they would also put a Canadian flag on their backpacks. And if anyone

103
00:14:24,720 --> 00:14:32,000
asked they would tell them they were Canadian. It's maybe something like that. This is sort of

104
00:14:32,000 --> 00:14:39,600
one of the lower more base benefits of becoming a Buddhist, but it certainly is there.

105
00:14:40,400 --> 00:14:48,560
And protection is quite useful being protected from danger. Obviously when we're trying to develop

106
00:14:48,560 --> 00:15:00,480
ourselves, they say that when you take the Buddha as your refuge, evil spirits will not dare

107
00:15:00,480 --> 00:15:06,480
to come near you because they know the danger of attacking someone who is a follower of the Buddha.

108
00:15:08,880 --> 00:15:15,680
There is an inherent danger for spirits because it's very easy for them to

109
00:15:17,520 --> 00:15:23,360
switch from one state to another. A human being doesn't evil deed and it's very slow to bring

110
00:15:23,360 --> 00:15:31,120
results, but a spirit does evil deeds or good deeds, it's very quick to bring results.

111
00:15:34,800 --> 00:15:38,080
The next benefit is that it brings happiness in this life.

112
00:15:38,080 --> 00:15:52,560
When we become Buddhist, it brings a great state of peace and surety confidence to us.

113
00:15:54,640 --> 00:16:02,160
It eases our minds. We don't have to think about what is right and what is wrong. We accept

114
00:16:02,160 --> 00:16:07,840
the teachings of the Buddha. We've studied them and we're sure in our minds and we have this

115
00:16:07,840 --> 00:16:16,560
great confidence. We know where we stand. So it's like when the Christians, they always tell you

116
00:16:16,560 --> 00:16:23,280
and when you have a problem, you just ask yourself what would Jesus do? This is the Christian

117
00:16:23,280 --> 00:16:29,040
answer. For Buddhist, we say, well, what would the Buddha do? What would a Buddhist do? What would

118
00:16:29,040 --> 00:16:37,840
a follower of the Buddha do? This is generally useful as well. As I said, it brings us peace and

119
00:16:37,840 --> 00:16:45,840
happiness. It's something that calms and tranquilizes our mind. When we practice meditation,

120
00:16:47,920 --> 00:16:53,120
we feel somehow like we are a part of the group. We're walking with the Buddha.

121
00:16:53,120 --> 00:17:05,520
We're following after the Buddha. And then the next benefit related to that is it brings happiness

122
00:17:05,520 --> 00:17:13,520
in our next life. Because of the surety of mind, the confidence that we have and the peace

123
00:17:13,520 --> 00:17:24,000
and tranquility that exists in our minds from accepting the Buddha as our teacher. When we pass away,

124
00:17:27,200 --> 00:17:30,320
we can be sure to be born in a good place, in a good way.

125
00:17:36,480 --> 00:17:41,120
But the real reason I would say, and probably the best reason, the only sort of reason that is

126
00:17:41,120 --> 00:17:50,960
of immediate benefit or immediate purpose for us is that considering that the Buddha's teaching

127
00:17:50,960 --> 00:18:02,480
is so pure and that there's no part of it that is detrimental to us, that taking on the Buddha

128
00:18:02,480 --> 00:18:12,080
as our refuge or taking on Buddhism as our path leads to purity of mind, leads to purity of

129
00:18:12,080 --> 00:18:19,680
mind and the dedication to the practice. And this is obviously the most important because our

130
00:18:20,480 --> 00:18:27,520
intention, our path as Buddhists is to practice, is to develop our minds. And so taking the

131
00:18:27,520 --> 00:18:36,320
Buddha as our refuge and reminding ourselves of the qualities of the Buddha and determining

132
00:18:36,320 --> 00:18:41,440
that this is our path gives us the strength of mind to continue in the practice.

133
00:18:42,400 --> 00:18:49,600
When the going gets tough, we remember the example that the Buddha said and we reaffirm in our

134
00:18:49,600 --> 00:18:55,760
minds that we are followers of the Buddha. It gives us confidence. It gives us reassurance that

135
00:18:55,760 --> 00:19:05,840
we're a good person, that we're someone who has done a good thing in terms of

136
00:19:07,360 --> 00:19:12,320
you know, put placing our trust and our confidence in the Buddha. And by following this path,

137
00:19:12,320 --> 00:19:14,800
which leads us to freedom from suffering.

138
00:19:18,800 --> 00:19:22,400
So I would say it's generally a good thing. It's obviously not the best thing and it's not a

139
00:19:22,400 --> 00:19:28,000
replacement for meditation. Let's say there's a lot of people out there who are Buddhist

140
00:19:29,120 --> 00:19:39,120
quote unquote and don't practice and tend to get this overconfidence that you know it's

141
00:19:39,120 --> 00:19:46,400
you know I'm Buddhist and I've been Buddhist from birth and that's somehow that means something,

142
00:19:46,400 --> 00:19:54,480
somehow that is going to protect one. And I would say you know having said that it is a protection,

143
00:19:54,480 --> 00:20:03,760
it is a support. It's a fairly weak protection in the face of the defilements that exist in our minds.

144
00:20:03,760 --> 00:20:09,600
If that's our only protection is this identification as a Buddhist, then we're not likely to be

145
00:20:09,600 --> 00:20:18,080
protected in any way from the evils in our mind. It's simply a support. It's a basic practice

146
00:20:18,080 --> 00:20:23,520
that encourages us, that tranquilizes our minds. When I was practicing meditation, when I first

147
00:20:23,520 --> 00:20:28,960
went to practice meditation, I didn't wasn't even interested in Buddhism. I had no intention to

148
00:20:28,960 --> 00:20:34,160
become a Buddhist. But they made us do this ceremony where we you know we take the Buddha as

149
00:20:34,160 --> 00:20:40,240
our refuge and so I followed after and I was reading the words reading the script again while I

150
00:20:40,240 --> 00:20:46,240
was meditating. They wouldn't let us read. So all we had is this little booklet and I just kept

151
00:20:46,240 --> 00:20:54,880
reading it over and over again in the chanting. And so when the practice got really difficult

152
00:20:54,880 --> 00:21:08,160
and suddenly I was faced with this overwhelming pain and suffering from just the realization

153
00:21:08,160 --> 00:21:17,840
of the insanity that existed in my mind and what I had done to myself and how I was so on the

154
00:21:17,840 --> 00:21:29,600
wrong path with drugs and alcohol and women and music and so much that was really destroying my

155
00:21:29,600 --> 00:21:36,480
peace of mind and had been doing so for many years. I remember waking up at three in the morning

156
00:21:36,480 --> 00:21:41,680
and just saying okay I get ready to start and as soon as I sat down to start meditating everything

157
00:21:41,680 --> 00:21:47,520
came back again. Everything from the last day was right there again waiting for me. It hadn't gone

158
00:21:47,520 --> 00:21:53,760
anywhere it hadn't disappeared with my sleep and just freaking out and realizing you know I

159
00:21:53,760 --> 00:22:05,440
I can't do this anymore and I had no way nowhere to go I had no no refuge no nothing to hold on to

160
00:22:05,440 --> 00:22:15,040
nothing to pull myself up with. And so I went walking out of my out of my hut looking you know I

161
00:22:15,040 --> 00:22:20,000
not looking actually just wandering aimlessly and then I saw off in this bamboo salad they had

162
00:22:20,000 --> 00:22:26,880
this Buddha image and the light was on somebody left the light on on night and as soon as I saw

163
00:22:26,880 --> 00:22:33,840
it I just just drawn to it and I walked over and and and I felt down in front of the Buddha

164
00:22:33,840 --> 00:22:39,280
and I prostrated down and I started chanting according to this booklet and taking refuge in the

165
00:22:39,280 --> 00:22:53,280
Buddha. I suppose that sounds a lot like a Christian tale of what do you call newborn again being

166
00:22:53,280 --> 00:22:59,360
born again and you know I don't think it's that dissimilar and it's certainly not a state of

167
00:22:59,360 --> 00:23:05,280
enlightenment but it was something that really helped me in my practice it helped me to continue

168
00:23:05,280 --> 00:23:10,400
meditation so if you want to meditate if that was my intention is to meditate then I would say that

169
00:23:10,400 --> 00:23:17,840
was a real benefit for me because it gave me this you know basic reassurance that I wasn't alone

170
00:23:17,840 --> 00:23:25,280
I had someone you know to hold on to well I was like a child just learning to walk

171
00:23:25,280 --> 00:23:35,200
and it certainly it's certainly not the the final solution but for for newcomers it can be a very

172
00:23:35,200 --> 00:23:42,160
great thing to do a great benefit and and helps to calm and reassure your mind in the beginning.

173
00:23:45,040 --> 00:23:49,600
Okay so the next thing is what does it mean to be a Buddhist and how do you become a Buddhist

174
00:23:49,600 --> 00:23:59,200
and it's quite simple. As a Buddhist we're not really interested in identifying ourselves as

175
00:23:59,200 --> 00:24:12,800
anything but there are certain qualifications that one has to obtain one has to take the three

176
00:24:12,800 --> 00:24:19,360
refuges to be considered a Buddhist you have to take the Buddha as your refuge the idea is as a

177
00:24:19,360 --> 00:24:26,480
Buddhist we say that the Buddha is our leader though he's passed away long long ago we still

178
00:24:26,480 --> 00:24:33,040
you know understand that this this person who taught all of these wonderful things is our leader

179
00:24:33,040 --> 00:24:42,480
and we we take him and and singularly him as our leader. The second is is the dhamma his teachings

180
00:24:42,480 --> 00:24:47,840
we take his teachings as our teaching as the teachings we are going to follow we take we say that

181
00:24:47,840 --> 00:24:55,680
these are the teachings that we're going to follow this is our path and third we we take the

182
00:24:55,680 --> 00:25:02,640
teachers or them the enlightened disciples of the Buddha as as our refuge the people who have passed

183
00:25:02,640 --> 00:25:21,280
on the Buddha's teaching up until this time this is this is what I meant by you know having the

184
00:25:21,280 --> 00:25:27,520
protection having a support because these things are something that we can always reflect on

185
00:25:27,520 --> 00:25:33,600
something that we can always remember something that we can use to support ourselves when they're

186
00:25:33,600 --> 00:25:40,480
going to get stuff when when we're in difficulty it's not a central practice again I don't want

187
00:25:40,480 --> 00:25:45,840
to be able to get this idea that we're like a faith-based religion where we we focus on worshiping

188
00:25:45,840 --> 00:25:51,760
these things or something but the support is is undeniable the support that these things give to our

189
00:25:51,760 --> 00:26:08,080
mind and so my my teacher he said when you're when you're in danger when you when you're

190
00:26:09,360 --> 00:26:16,000
traveling anywhere when you're going on a trip or something and you're worried and you're not sure

191
00:26:16,000 --> 00:26:26,080
you know whether there'll be danger or misfortune or so on you can just remind yourself of the

192
00:26:26,080 --> 00:26:29,680
Buddha and say that the Buddha is your refuge the dumb is your refuge the song is your refuge

193
00:26:29,680 --> 00:26:36,960
and it it strengthens your mind and it can actually change the course of events because it creates

194
00:26:36,960 --> 00:26:49,280
some sort of some sort of strength and power in your mind and perhaps even is a means of seeking

195
00:26:49,280 --> 00:26:56,800
protection from from the angels and the guardian spirits who who may also be Buddhist or

196
00:26:56,800 --> 00:27:03,520
appreciate the Buddhist teaching so we have this mantra actually that will say to ourselves that

197
00:27:03,520 --> 00:27:08,560
just as the Buddha is my refuge the dumb is my refuge the song is my refuge Buddha may not

198
00:27:08,560 --> 00:27:19,200
or Tamil may not those uncle may not all and we repeat that to ourselves the second thing that

199
00:27:19,200 --> 00:27:30,480
that's required as a Buddhist is that we keep the five precepts and this is where people tend to

200
00:27:30,480 --> 00:27:36,640
have a lot of trouble I would say most people are at least superficially able to

201
00:27:38,000 --> 00:27:44,400
accept the Buddha the dumb and the sangha as good things and as their refuge even though they

202
00:27:44,400 --> 00:27:49,440
might not quite know what that means most for many Buddhists don't really know what the dummy is

203
00:27:49,440 --> 00:27:56,800
don't don't have a strong understanding of of what is the teaching of the Buddha and and also

204
00:27:56,800 --> 00:28:02,880
don't have a way of distinguishing between those those teachers or those Buddhists who could be

205
00:28:02,880 --> 00:28:08,000
considered sangha who actually practice according to the Buddha's teaching so there are a lot of

206
00:28:08,000 --> 00:28:16,080
Buddhists out there who follow after teachers who maybe are fortune telling or you know offering

207
00:28:16,080 --> 00:28:24,080
ambulance or you know teaching public school or whatever many things which are not in line with

208
00:28:24,080 --> 00:28:29,520
the Buddha's teaching and would make one thing that this is not really a part of the sangha

209
00:28:29,520 --> 00:28:35,600
these people are not really followers or passing on the Buddha's teaching

210
00:28:40,480 --> 00:28:44,320
but so people are able at least superficially to accept this it's very difficult for people to

211
00:28:44,320 --> 00:28:55,040
accept the five precepts and this makes it a little bit difficult to become a Buddhist and obviously

212
00:28:55,040 --> 00:29:03,040
it that makes sense because Buddhism is not an easy path it's not an easy thing to become enlightened

213
00:29:03,040 --> 00:29:08,960
to become free from suffering we're not talking about something you know there's no okay become a

214
00:29:08,960 --> 00:29:16,800
Buddhist sign up in your your your your set for life this isn't one of those religions it's a

215
00:29:16,800 --> 00:29:22,720
very difficult thing and and this is perhaps why why seeking out a support might be maybe beneficial

216
00:29:22,720 --> 00:29:30,880
because we have no illusions about the difficulty that of the task at hand it's not something where

217
00:29:30,880 --> 00:29:40,720
everyone can be said to succeed you might fail you might end this life not having gained any

218
00:29:41,680 --> 00:29:49,600
lasting benefit or or not having gained not having become enlightened to any degree

219
00:29:51,040 --> 00:29:54,320
it's possible that you could practice for some time and follow away from it

220
00:29:54,320 --> 00:30:04,960
and so you know really we we need all the help we can get the benefit of becoming Buddhist

221
00:30:10,080 --> 00:30:17,600
but we also have to take on very some some fairly difficult precepts people always ask about the

222
00:30:17,600 --> 00:30:22,560
five precepts you know they're really necessary especially you know they're not drinking alcohol

223
00:30:22,560 --> 00:30:27,600
and taking drugs and really I think this is a terrible terrible question to ask and I think it's

224
00:30:27,600 --> 00:30:32,400
ridiculous that people always say oh well if you want to have one glass of wine it's not really that

225
00:30:32,400 --> 00:30:38,800
but it's really I mean come on people if you if you can't give up such a simple thing like

226
00:30:38,800 --> 00:30:47,360
like in toxicants drinking poison drinking rotten grapes you know you know it is it really

227
00:30:47,360 --> 00:30:54,880
you know is it really fair for us to to to to to wine and complain about such a small thing

228
00:30:54,880 --> 00:31:03,360
when what we're looking at here is such a profound and incredibly difficult path that if you

229
00:31:03,360 --> 00:31:18,000
can't even give up these basic acts these these basic immoral immoral undertaking it's not it

230
00:31:18,000 --> 00:31:22,400
doesn't speak well for your ability to do away with the very subtle to found

231
00:31:27,280 --> 00:31:31,520
and I think this this is easy to see for people who really practice meditation that if you're

232
00:31:31,520 --> 00:31:36,000
serious about meditation it's it's not really that difficult for you to give these things up

233
00:31:36,000 --> 00:31:42,480
this is what is required this is what the Buddha recommended not not even really recommended

234
00:31:42,480 --> 00:31:47,360
but he was he was fairly categorical in terms of you know if you don't keep these you're not

235
00:31:47,360 --> 00:31:53,840
really going to progress in your practice these are the these are the the basic

236
00:31:53,840 --> 00:32:02,240
moral principles of a Buddhist meditator or Buddhist practitioner if you can't even keep these

237
00:32:02,240 --> 00:32:10,560
it's it's very difficult to see it anyway for you to to progress and so it's it's often a scene

238
00:32:10,560 --> 00:32:25,760
as a making a concession or you know giving something up in in favor for our practice

239
00:32:26,800 --> 00:32:31,520
we we want to go out drinking we want to drink wine we want to be a social drinker or so on

240
00:32:32,160 --> 00:32:37,520
maybe we want to kill we want to steal we want to cheat we want to lie and in some ways we can

241
00:32:37,520 --> 00:32:46,880
we can verify we can justify very small bad deeds but this is a sacrifice that we make

242
00:32:48,720 --> 00:32:54,480
and and in the end it isn't a sacrifice at all we see that we were wrong to once we practice we

243
00:32:54,480 --> 00:32:58,720
did it we were wrong to think that there was any good that could come from any of these things

244
00:33:00,080 --> 00:33:05,120
even just in terms of drinking people who who would drink a little bit and say oh I'm just doing

245
00:33:05,120 --> 00:33:11,200
it to be social and to fit in and so on actually what you're saying there is that you're doing

246
00:33:11,200 --> 00:33:16,080
it so that you don't have to challenge other people's beliefs you know suppose your friends are

247
00:33:16,080 --> 00:33:20,320
alcoholics or they really do get drunk while you just drink just so that they don't feel bad

248
00:33:21,040 --> 00:33:27,840
basically what you're saying you drink so that they they're able to to retain their sense that

249
00:33:27,840 --> 00:33:34,240
drinking is okay which of course it isn't and as a Buddhist it certainly isn't as a Buddhist

250
00:33:34,240 --> 00:33:40,800
we have you know we make this determination that this is the opposite of a clear and sober

251
00:33:40,800 --> 00:33:50,720
and and pure state of mind and so you're doing an incredible disservice to the Buddhist

252
00:33:50,720 --> 00:33:56,720
teach to Buddhism by encouraging these people and you're wasting a very precious

253
00:33:56,720 --> 00:34:08,160
chance to to change the world to say to these people I'm sorry I don't drink I think I believe

254
00:34:08,160 --> 00:34:20,400
that drinking is a cost for for a muddled state of mind this is an enemy to clear it to

255
00:34:20,400 --> 00:34:28,480
clarity of mind my practice is to purify my mind and so I don't do they say no if that makes them

256
00:34:28,480 --> 00:34:33,520
angry then then then here you go this is something they have to look at in themselves and something

257
00:34:33,520 --> 00:34:37,200
you have to look at why are you hanging out with people who get angry when you talk about clarity

258
00:34:37,200 --> 00:34:42,880
of mind and purity of mind when you talk about meditation and all your friends start to you know

259
00:34:42,880 --> 00:34:50,480
get upset and and and get bored and disinterested looks on their faces why are you with these

260
00:34:50,480 --> 00:34:59,680
people if you can somehow explain to them to the extent that they're able to accept and eventually

261
00:34:59,680 --> 00:35:07,520
come to practice good but otherwise it's not much of an excuse to say well my friends like to drink

262
00:35:07,520 --> 00:35:13,040
and therefore I should drink with them you know it probably means that you're hanging out with the wrong people

263
00:35:17,040 --> 00:35:25,600
so the five precepts we don't kill we don't steal we don't cheat meaning to commit adultery

264
00:35:25,600 --> 00:35:35,280
or or break up other relationships for in romantic affairs we don't lie and we don't take drugs

265
00:35:35,280 --> 00:35:43,520
and alcohol and those things that intoxicate the mind and this is basically what it means to

266
00:35:43,520 --> 00:35:48,960
become a Buddhist when you when you take these two things the three refuges you accept that the Buddha

267
00:35:49,680 --> 00:35:56,640
his teachings and the Sangha the teachers who have passed on his teachings are your refuge

268
00:35:56,640 --> 00:36:08,880
are your your your point of reference and when you take the five precepts as rules for your life

269
00:36:10,480 --> 00:36:13,920
this is really what it means to become a Buddhist

270
00:36:13,920 --> 00:36:31,360
and just as a side note I made a video on YouTube recently that is a ceremony for taking

271
00:36:31,360 --> 00:36:44,640
the three refuge and the five precepts so if in case anyone is interested in becoming a Buddhist

272
00:36:44,640 --> 00:37:05,040
you're welcome to look it up and here's the link

273
00:37:14,640 --> 00:37:29,920
so I thought that was something useful to talk about this ceremony on YouTube is is a way of

274
00:37:29,920 --> 00:37:38,400
reaffirming these principles that I've talked about it's a ceremony it's a ritual and what it does

275
00:37:38,400 --> 00:37:44,240
is it reaffirms in your mind it's just like our meditation practice when we watch the rising we reaffirm

276
00:37:44,240 --> 00:37:49,440
for ourselves this is the rising this is the falling of the abdomen we feel pain we reaffirm it

277
00:37:49,440 --> 00:37:54,960
for what it is and it has a power when we do this ceremony it has a power in our minds it reaffirms

278
00:37:54,960 --> 00:38:01,840
in our minds it strengthens our our vow our intention and it has a purpose it has an effect

279
00:38:01,840 --> 00:38:11,360
I would say the effect is beneficial because it strengthens our ability to to carry out our practice

280
00:38:12,720 --> 00:38:17,120
so that's what I wanted to talk about today I think that should be something generally useful for

281
00:38:17,120 --> 00:38:22,560
people and there's a support for our meditation practice I haven't really gotten into meditation

282
00:38:22,560 --> 00:38:32,400
in these past couple of weeks and I promised that I would but maybe I'll say that I'd like to

283
00:38:32,400 --> 00:38:40,880
teach a session record a session on on basic meditation practice just a 10-15 minute session

284
00:38:40,880 --> 00:38:46,240
where all we it's a guided meditation that I can then use for people to use at home when they

285
00:38:46,240 --> 00:38:52,400
want to learn to meditate in a very short time and get a basic understanding of meditation it's

286
00:38:52,400 --> 00:39:18,640
maybe I'll do that anyway thank you all for coming if you have any questions I'm happy to answer them

