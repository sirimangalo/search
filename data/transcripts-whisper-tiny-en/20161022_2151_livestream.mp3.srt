1
00:00:00,000 --> 00:00:05,720
and gold to strive for, to the point that the Buddha left his home, right?

2
00:00:05,720 --> 00:00:08,000
This is an important point.

3
00:00:08,000 --> 00:00:13,600
He left home, he dropped everything, and Buddha said you should,

4
00:00:13,600 --> 00:00:17,500
a person who doesn't, isn't a soda punishment, think of themselves as a person with

5
00:00:17,500 --> 00:00:20,200
their head on fire.

6
00:00:20,200 --> 00:00:22,920
What are you going to do with your head on fire and say, well, I'll take care of that

7
00:00:22,920 --> 00:00:26,200
later. I got work to do.

8
00:00:26,200 --> 00:00:28,900
No, your head is on fire.

9
00:00:28,900 --> 00:00:37,400
I know you're not, I know who this person is and he's doing a course to me and you're not

10
00:00:37,400 --> 00:00:45,400
negligent and you're not unaware of these issues, but it's important to realize that

11
00:00:45,400 --> 00:00:57,360
you have to, you know, there's no guarantees, and so at some point, hopefully sooner

12
00:00:57,360 --> 00:01:04,040
or rather than later, you have to put in the effort and this person actually is putting

13
00:01:04,040 --> 00:01:12,320
an effort and practicing every day, so that is a making progress, but you know, doing

14
00:01:12,320 --> 00:01:15,680
limited practice, you're just going to have to be patient.

15
00:01:15,680 --> 00:01:21,640
There's no better way to balance the faculties, there's no, you know, I mean all those

16
00:01:21,640 --> 00:01:26,440
things I just talked about can be support, don't talk too much, don't eat too much, eat

17
00:01:26,440 --> 00:01:30,840
I think is in the book of sevens, which we'll maybe get to, your dad's don't eat too

18
00:01:30,840 --> 00:01:35,920
much, don't talk too much, don't eat too much, don't sleep too much, don't work too

19
00:01:35,920 --> 00:01:43,720
much, all those things don't do too much, and then practice a lot, be mindful as

20
00:01:43,720 --> 00:01:44,720
much as you can.

21
00:01:44,720 --> 00:01:47,440
I mean, guess the other thing I'd say is...

