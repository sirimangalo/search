1
00:00:00,000 --> 00:00:07,000
Okay, so the final video what's in my pocket, let's see.

2
00:00:07,000 --> 00:00:15,000
Being among, actually I have a few pockets here.

3
00:00:15,000 --> 00:00:26,000
The first pocket is, let me see if you can get that.

4
00:00:26,000 --> 00:00:30,000
This pocket is this.

5
00:00:30,000 --> 00:00:33,000
This is something I made myself.

6
00:00:33,000 --> 00:00:38,000
I've got a couple of pockets there.

7
00:00:38,000 --> 00:00:44,000
And in it are a piece of cloth.

8
00:00:44,000 --> 00:00:50,000
This is a handker ship.

9
00:00:50,000 --> 00:01:09,000
This is my card wallet for carrying business cards and carrying this.

10
00:01:09,000 --> 00:01:13,000
This is a multi-tool.

11
00:01:13,000 --> 00:01:20,000
One thing I do is fix things myself.

12
00:01:20,000 --> 00:01:24,000
I fix and take things apart.

13
00:01:24,000 --> 00:01:28,000
Put them back together less, but take them apart more.

14
00:01:28,000 --> 00:01:31,000
This has all the, let's see, it has a pen.

15
00:01:31,000 --> 00:01:37,000
This is what you see me in drawing with in the earlier video.

16
00:01:37,000 --> 00:01:48,000
This is just something that I use forever.

17
00:01:48,000 --> 00:01:55,000
The business cards I keep, I only keep two business cards, so here I have another.

18
00:01:55,000 --> 00:02:03,000
Here we have the sergeant of the more part of the lease.

19
00:02:03,000 --> 00:02:11,000
This is the guy we went to see recently to let him know that I'm going to be staying here.

20
00:02:11,000 --> 00:02:23,000
The hope is that somehow he's going to help to mitigate some of the backlash during

21
00:02:23,000 --> 00:02:34,000
the lock-on ohms around.

22
00:02:34,000 --> 00:02:45,000
This is a Thai police officer who lives in Los Angeles and offered me his card and his telephone.

23
00:02:45,000 --> 00:02:55,000
In case I have further problems with the police.

24
00:02:55,000 --> 00:03:00,000
This is just an address of a man who actually does broadcasting at a monastery in

25
00:03:00,000 --> 00:03:01,000
personal.

26
00:03:01,000 --> 00:03:07,000
He does a show that they broadcast over the internet.

27
00:03:07,000 --> 00:03:12,000
My ID card.

28
00:03:12,000 --> 00:03:18,000
I don't live here, I'm actually from Canada, but I got an ID card.

29
00:03:18,000 --> 00:03:24,000
This little thing is actually the reason why I was put in jail in the first place.

30
00:03:24,000 --> 00:03:28,000
It wasn't because they claimed that I was a streaker.

31
00:03:28,000 --> 00:03:36,000
It was because once they claimed it, they couldn't just give me whatever I guess.

32
00:03:36,000 --> 00:03:43,000
Because I didn't have my ID on me because I didn't have my ID on me.

33
00:03:43,000 --> 00:03:44,000
They put me in jail.

34
00:03:44,000 --> 00:03:49,000
That's how it went.

35
00:03:49,000 --> 00:03:52,000
Here's my cards.

36
00:03:52,000 --> 00:04:01,000
These are the cards that I give up to people.

37
00:04:01,000 --> 00:04:08,000
This cards have my name on it, my website.

38
00:04:08,000 --> 00:04:14,000
These are the four foundations of mindfulness.

39
00:04:14,000 --> 00:04:17,000
There are four things that we have to keep in mind when we meditate.

40
00:04:17,000 --> 00:04:19,000
We have to go to the English and Thai.

41
00:04:19,000 --> 00:04:23,000
If you look at my videos, you can see what they mean.

42
00:04:23,000 --> 00:04:30,000
Then I sort of close up pictures of me on the back with something I've been fond of thinking

43
00:04:30,000 --> 00:04:34,000
and fond of telling my students.

44
00:04:34,000 --> 00:04:41,000
Then I got some cards for some reason for the people who own these properties.

45
00:04:41,000 --> 00:04:46,000
This is where I'm staying.

46
00:04:46,000 --> 00:04:48,000
Let's see.

47
00:04:48,000 --> 00:04:52,000
That's interesting.

48
00:04:52,000 --> 00:04:56,000
That's that.

49
00:04:56,000 --> 00:04:59,000
These are my keys to an apartment.

50
00:04:59,000 --> 00:05:02,000
I also had some keys to another apartment.

51
00:05:02,000 --> 00:05:08,000
Which is kind of more exciting because we have another apartment though.

52
00:05:08,000 --> 00:05:13,000
We have no three apartments here.

53
00:05:13,000 --> 00:05:18,000
Which means that more people can come to meditate with us.

54
00:05:18,000 --> 00:05:19,000
These are just my keys.

55
00:05:19,000 --> 00:05:21,000
This one is the key downstairs.

56
00:05:21,000 --> 00:05:24,000
There's someone staying there right now.

57
00:05:24,000 --> 00:05:28,000
It's a double bedroom so there's room for someone else to come in and stay.

58
00:05:28,000 --> 00:05:29,000
This is 17.

59
00:05:29,000 --> 00:05:31,000
This is the room I'm staying in.

60
00:05:31,000 --> 00:05:36,000
This is my mailbox key.

61
00:05:36,000 --> 00:05:41,000
In case I get a new mail.

62
00:05:41,000 --> 00:05:44,000
I can make a address.

63
00:05:44,000 --> 00:05:47,000
This is in my pocket.

64
00:05:47,000 --> 00:05:50,000
This is the receiver.

65
00:05:50,000 --> 00:05:57,000
This is the receiver for the wireless microphone.

66
00:05:57,000 --> 00:06:00,000
I really shouldn't be in the video.

67
00:06:00,000 --> 00:06:01,000
Okay.

68
00:06:01,000 --> 00:06:02,000
What else?

69
00:06:02,000 --> 00:06:04,000
See, this is the pockets of my shirt.

70
00:06:04,000 --> 00:06:06,000
Here I have some pockets.

71
00:06:06,000 --> 00:06:07,000
Okay.

72
00:06:07,000 --> 00:06:09,000
This is a paper clip that I got from the DMV.

73
00:06:09,000 --> 00:06:18,000
We went to register a trailer.

74
00:06:18,000 --> 00:06:25,000
One of those RV trailers that is a room with room on wheels.

75
00:06:25,000 --> 00:06:28,000
Apparently they consider that vehicle.

76
00:06:28,000 --> 00:06:30,000
That's not a driver's license that I got.

77
00:06:30,000 --> 00:06:31,000
It's just an ID.

78
00:06:31,000 --> 00:06:34,000
I don't have a driver's license and yet I have to register a vehicle.

79
00:06:34,000 --> 00:06:37,000
Which I don't quite understand.

80
00:06:37,000 --> 00:06:38,000
It's a trailer.

81
00:06:38,000 --> 00:06:41,000
The trailer's apparently need registration.

82
00:06:41,000 --> 00:06:53,000
We got there and they charged us like $465 for registration and fines and so on.

83
00:06:53,000 --> 00:06:55,000
But I got a paper clip.

84
00:06:55,000 --> 00:06:57,000
There's the deal.

85
00:06:57,000 --> 00:07:00,000
They got somebody's money and I got their paper clip.

86
00:07:00,000 --> 00:07:02,000
I didn't steal it.

87
00:07:02,000 --> 00:07:04,000
They gave it to me with the papers.

88
00:07:04,000 --> 00:07:07,000
These are Medicaid medicine.

89
00:07:07,000 --> 00:07:11,000
No, no, no, no, no, no.

90
00:07:11,000 --> 00:07:15,000
This is pie medicine.

91
00:07:15,000 --> 00:07:20,000
Why I have these in my pocket.

92
00:07:20,000 --> 00:07:29,000
Well, before I speak, I often put these in my mouth and use them like laws and just

93
00:07:29,000 --> 00:07:37,000
make their useful for getting rid of flam and clearing it.

94
00:07:37,000 --> 00:07:46,000
But one thing that's interesting about these is I really don't take Western medicine.

95
00:07:46,000 --> 00:07:51,000
That's sort of a choice of my own, it's certainly not against the rules to take medicine.

96
00:07:51,000 --> 00:07:54,000
But I don't know what that is.

97
00:07:54,000 --> 00:08:05,000
I guess it has to do with my idea that sickness is something not to be afraid of.

98
00:08:05,000 --> 00:08:17,000
So all I've got a medicine, I've got a big chest full of medicine that people have given me.

99
00:08:17,000 --> 00:08:25,000
When I went to India and got very, very sick twice, both times people gave me just handfuls of medicine.

100
00:08:25,000 --> 00:08:29,000
And every time I get sick people did this, they gave me handfuls of medicine.

101
00:08:29,000 --> 00:08:34,000
I just put it in my chest.

102
00:08:34,000 --> 00:08:39,000
I take some because they are thinking me herbal medicine.

103
00:08:39,000 --> 00:08:48,000
And so I take some of that and 90% of it goes into the trunk.

104
00:08:48,000 --> 00:08:51,000
Just another odd fact about my life.

105
00:08:51,000 --> 00:08:54,000
Finally, in my last pocket, this is my chest pocket.

106
00:08:54,000 --> 00:08:57,000
This is a Buddha image.

107
00:08:57,000 --> 00:09:01,000
Let me see that now.

108
00:09:01,000 --> 00:09:18,000
This Buddha was given to me when I first did my first recitation of the Batimulka, which is the monk's rules.

109
00:09:18,000 --> 00:09:23,000
Now monks have 227 rules in my tradition that they have to follow.

110
00:09:23,000 --> 00:09:26,000
And every two weeks we will chant them together.

111
00:09:26,000 --> 00:09:31,000
So there's a monk who gave this to me.

112
00:09:31,000 --> 00:09:35,000
And it has great meaning to me when I chant the rules.

113
00:09:35,000 --> 00:09:37,000
I could carry it with me.

114
00:09:37,000 --> 00:09:44,000
So tomorrow, the day after tomorrow would be a day when we chant the rules and I should be practicing.

115
00:09:44,000 --> 00:09:47,000
So I'll start now.

116
00:09:47,000 --> 00:09:50,000
It's been a great, it's been a fun day.

117
00:09:50,000 --> 00:09:53,000
I'm glad that I had a chance to get involved with this.

118
00:09:53,000 --> 00:10:01,000
I really hope that some of this is included just to show that we're a part of the world.

119
00:10:01,000 --> 00:10:30,000
So I'm an important part of reality.

