1
00:00:00,000 --> 00:00:05,280
Okay, good evening.

2
00:00:05,280 --> 00:00:10,920
I guess when I say good evening, there's actually no one here yet, but you'll be seeing

3
00:00:10,920 --> 00:00:17,120
this on YouTube, so it's being recorded.

4
00:00:17,120 --> 00:00:25,840
So tonight's question is about the need to believe in rebirth.

5
00:00:25,840 --> 00:00:39,160
The question is, can you be a Buddhist? Can you become enlightened without a belief in rebirth?

6
00:00:39,160 --> 00:00:47,280
And so first of all, with I guess, as with a lot of questions, we have to start by clarifying

7
00:00:47,280 --> 00:00:55,400
that when I talk about Buddhism, then when I answer this or most questions, I'm talking

8
00:00:55,400 --> 00:01:01,160
about my Buddhism, my form of Buddhism, and that's important to say, because I think

9
00:01:01,160 --> 00:01:05,720
many people are not aware that there are many types of Buddhism, some of which actually

10
00:01:05,720 --> 00:01:10,400
conflict with each other, who have used that conflict with each other.

11
00:01:10,400 --> 00:01:19,560
Some forms of Buddhism were created as a counter to another type of Buddhism and so on.

12
00:01:19,560 --> 00:01:23,560
And there are also different practices and even different beliefs, and many of the beliefs

13
00:01:23,560 --> 00:01:29,520
are the same, many of the core principles are the same, but I'm only speaking from my

14
00:01:29,520 --> 00:01:30,520
perspective.

15
00:01:30,520 --> 00:01:35,280
So I'll say things like Buddhism in Buddhism, I just mean my form.

16
00:01:35,280 --> 00:01:46,080
So here I'm going to start by saying that in Buddhism, we make the claim of being scientific.

17
00:01:46,080 --> 00:01:53,680
So that I'm not suggesting that I think everyone agrees with that, or that it's easy

18
00:01:53,680 --> 00:01:54,680
to see.

19
00:01:54,680 --> 00:01:59,680
I think there are a lot of people, if they looked into Buddhism superficially, at least,

20
00:01:59,680 --> 00:02:01,680
they would feel it's not very scientific.

21
00:02:01,680 --> 00:02:10,840
I mean, there are some things, I think, about Buddhism as it's formed that are unscientific

22
00:02:10,840 --> 00:02:25,600
and perhaps even exaggerated or misrepresented, maybe even wrong, like accounts of things

23
00:02:25,600 --> 00:02:29,200
and details and descriptions and so on.

24
00:02:29,200 --> 00:02:34,080
But in its basics, in its basics, in its essence, we would say there are scientific

25
00:02:34,080 --> 00:02:36,800
principles that work.

26
00:02:36,800 --> 00:02:42,960
And so the analogy I'm going to use, the question of whether you need to understand rebirth,

27
00:02:42,960 --> 00:02:50,720
you need to believe in rebirth in order to become enlightened, say it like that.

28
00:02:50,720 --> 00:02:57,960
It's similar to whether the question of whether you can be a scientist, a good scientist,

29
00:02:57,960 --> 00:03:08,920
like a great scientist, and not believe in the nuclear bomb, the thermonuclear bomb,

30
00:03:08,920 --> 00:03:11,560
let's say.

31
00:03:11,560 --> 00:03:13,720
And why I put it like that?

32
00:03:13,720 --> 00:03:19,200
It's not a perfect analogy, but it's a useful one, because it points to two very different

33
00:03:19,200 --> 00:03:20,200
things.

34
00:03:20,200 --> 00:03:21,400
It puts things in perspective, right?

35
00:03:21,400 --> 00:03:24,840
Because when people look at Buddhism, they say, well, what does Buddhism believe?

36
00:03:24,840 --> 00:03:28,160
And Buddhism believes in rebirth, and you say, what?

37
00:03:28,160 --> 00:03:32,440
That's just a religious belief, and where's their proof, where's their evidence?

38
00:03:32,440 --> 00:03:41,280
And it's kind of like saying, well, scientists claim to have a nuclear bomb, and they can

39
00:03:41,280 --> 00:03:50,240
blow things up, but just a small, relatively small bit of whatever, plutonium, I guess,

40
00:03:50,240 --> 00:03:54,640
uranium.

41
00:03:54,640 --> 00:03:59,720
But there are scientific principles behind it, and how you explain the atomic bomb is not

42
00:03:59,720 --> 00:04:08,760
be magic, or not because you found it in the forest somewhere, or because you just cobbled

43
00:04:08,760 --> 00:04:09,760
it together.

44
00:04:09,760 --> 00:04:19,640
No, there are very detailed principles at work in order to create a thermonuclear bomb.

45
00:04:19,640 --> 00:04:24,920
Now, rebirth is not like that, because, of course, it's the ideas that happens to everyone,

46
00:04:24,920 --> 00:04:26,440
but it doesn't happen all the time.

47
00:04:26,440 --> 00:04:33,520
It's not a common part of life, and as a result, it's not really a basic, important part

48
00:04:33,520 --> 00:04:35,000
of Buddhism.

49
00:04:35,000 --> 00:04:41,240
So can you become enlightened without believing in rebirth?

50
00:04:41,240 --> 00:04:45,040
I would say the answer is yes.

51
00:04:45,040 --> 00:04:51,480
But just as a really good scientist, let's say a scientist who's just there and everything

52
00:04:51,480 --> 00:04:56,360
there is to know about material science, if you ask them, do you believe in the nuclear

53
00:04:56,360 --> 00:05:04,800
bomb, the thermonuclear bomb gets, of course, I mean, I can explain it to you.

54
00:05:04,800 --> 00:05:12,680
And so the problem, of course, or the dilemma or the challenge as Buddhists is to try and

55
00:05:12,680 --> 00:05:20,040
explain rebirth, but explain it in terms of scientific principles.

56
00:05:20,040 --> 00:05:25,800
And that's not often done night, and I find, I mean, I think the texts are not always

57
00:05:25,800 --> 00:05:31,400
accessible, and there's some basic sort of the idea that craving, if you have craving,

58
00:05:31,400 --> 00:05:35,560
you're going to be reborn, I mean, that's often talked about.

59
00:05:35,560 --> 00:05:40,320
But it is often presented in such a way that, or it's read about or heard about in such

60
00:05:40,320 --> 00:05:45,680
a way as to just be, I believe, something we have faith in because the Buddha taught us.

61
00:05:45,680 --> 00:05:50,480
Which isn't really the case, it doesn't have to be the case, it's not, it is often the

62
00:05:50,480 --> 00:05:56,280
case, and many Buddhists do just believe in it, that's not wrong, it's not bad, it's like

63
00:05:56,280 --> 00:06:01,880
I believe in the nuclear bomb, I have no idea how it works, or no detailed knowledge about

64
00:06:01,880 --> 00:06:04,080
works.

65
00:06:04,080 --> 00:06:08,880
But a scientist knows, and the important thing here is you don't have to be able to explain

66
00:06:08,880 --> 00:06:09,880
it.

67
00:06:09,880 --> 00:06:14,000
It's not really my job to explain to people about rebirth, I do often because it's, of

68
00:06:14,000 --> 00:06:20,240
course, such an object of contention, there's a lot of people who ask, do I have to,

69
00:06:20,240 --> 00:06:25,680
I want to be a Buddhist, but I don't believe in rebirth, and I just don't believe in it.

70
00:06:25,680 --> 00:06:30,000
So can I be, can I practice Buddhism, can I become a knight?

71
00:06:30,000 --> 00:06:37,240
Now, if you didn't believe in a nuclear bomb, and this is different, right, there's, sorry,

72
00:06:37,240 --> 00:06:40,160
we have to be clarified what we mean by belief.

73
00:06:40,160 --> 00:06:48,240
If you don't have a conviction about the nuclear bomb, that's fine, right, you can still

74
00:06:48,240 --> 00:06:52,720
be a scientist, and you can learn, and the idea is that eventually you're going to learn,

75
00:06:52,720 --> 00:06:55,800
and you're going to understand, you say, oh yeah, yes, it's great.

76
00:06:55,800 --> 00:07:01,080
But if on the other hand, you believe that there's no such things in a nuclear bomb,

77
00:07:01,080 --> 00:07:05,640
I don't know how much that shuts you out of being a scientist, but you can see how it

78
00:07:05,640 --> 00:07:07,000
would get in your way.

79
00:07:07,000 --> 00:07:13,000
It certainly can get in the way of Buddhism, and the most obvious way it gets in the way.

80
00:07:13,000 --> 00:07:17,560
When you say, I believe it's wrong, I believe rebirth is wrong, is that it takes something

81
00:07:17,560 --> 00:07:19,760
away from the practice.

82
00:07:19,760 --> 00:07:22,080
What does the pre-app practice become then?

83
00:07:22,080 --> 00:07:31,360
If you don't have an understanding, if you don't have an understanding of the extent to

84
00:07:31,360 --> 00:07:37,880
which it's important to free yourself from defilements and purify your mind and so on, then

85
00:07:37,880 --> 00:07:46,080
it cuts you off to some extent to the sense of importance and urgency and the far-reaching

86
00:07:46,080 --> 00:07:49,760
effects or significance of what we're doing.

87
00:07:49,760 --> 00:07:53,880
It becomes hard to really put effort into meditation if it's just going to be about what

88
00:07:53,880 --> 00:07:57,800
feeling good in this life, being a good person in this life and so on.

89
00:07:57,800 --> 00:08:04,160
I don't know who cares, why not just eat, drink, and marry because you're going to die anyway.

90
00:08:04,160 --> 00:08:06,200
So it is an important thing.

91
00:08:06,200 --> 00:08:09,040
If it were true that when we died, there's nothing.

92
00:08:09,040 --> 00:08:15,360
It kind of reduces the importance of meditation.

93
00:08:15,360 --> 00:08:20,720
So it becomes an important doctrine in that sense, and thus you could say there's an importance

94
00:08:20,720 --> 00:08:26,320
for us to talk about it, to explain it.

95
00:08:26,320 --> 00:08:30,840
So let's talk about why Buddhism sees itself as scientific or why I see it in my Buddhism

96
00:08:30,840 --> 00:08:33,200
as scientific.

97
00:08:33,200 --> 00:08:37,480
So the difference, the real difference and the reason why a lot of scientists probably

98
00:08:37,480 --> 00:08:49,880
say we're not scientific is because you can't, I think the term is it's unfalsifiable

99
00:08:49,880 --> 00:08:55,480
in the sense that you couldn't prove whether someone was telling a truth or not.

100
00:08:55,480 --> 00:09:05,040
Like if I say, if I say I sat down and closed my eyes and I left my body, how can I

101
00:09:05,040 --> 00:09:06,960
prove that to someone?

102
00:09:06,960 --> 00:09:08,880
How can I provide evidence?

103
00:09:08,880 --> 00:09:14,440
How could someone show whether I'm telling the truth or not?

104
00:09:14,440 --> 00:09:22,480
So superficially it seems like you're dealing with something that's subjective and problematic,

105
00:09:22,480 --> 00:09:25,240
you know, subject to so much bias, right?

106
00:09:25,240 --> 00:09:26,800
And it is, of course.

107
00:09:26,800 --> 00:09:32,040
If I sit here and I close my eyes and I see the Buddha, right?

108
00:09:32,040 --> 00:09:36,000
I will say to you, the Buddha came to me, I wouldn't say that, but that's what many people

109
00:09:36,000 --> 00:09:37,000
do.

110
00:09:37,000 --> 00:09:41,200
They would say things like this, Christians say I saw Jesus and people go even further,

111
00:09:41,200 --> 00:09:45,240
they'll hear something and they'll say, it was God who came to me or it was the Buddha

112
00:09:45,240 --> 00:09:46,880
who came to me or so on, right?

113
00:09:46,880 --> 00:09:52,360
So they didn't even see it, they didn't even have real interesting evidence, but they

114
00:09:52,360 --> 00:09:53,360
extrapolate it.

115
00:09:53,360 --> 00:09:56,120
I mean, that's just as common.

116
00:09:56,120 --> 00:10:01,360
But even if I did see the Buddha in front of me, how do I know it's really the Buddha?

117
00:10:01,360 --> 00:10:05,720
How do I know it's not Mara pretending to be the Buddha?

118
00:10:05,720 --> 00:10:13,120
If I saw Jesus or so on, how do I know it's really Jesus, right?

119
00:10:13,120 --> 00:10:21,360
Most people are content, religious people are content with affirming their experience as

120
00:10:21,360 --> 00:10:24,480
true and this is what science, this is the argument science has, right?

121
00:10:24,480 --> 00:10:29,080
No matter what you, suppose I remember Pat my past lives, what science would say, how do

122
00:10:29,080 --> 00:10:30,400
you know it's a memory?

123
00:10:30,400 --> 00:10:34,040
How do you know it's not just a delusion, a illusion, right?

124
00:10:34,040 --> 00:10:38,160
They talk about leaving your body as just some hallucination and so on.

125
00:10:38,160 --> 00:10:39,160
How could you tell the difference?

126
00:10:39,160 --> 00:10:40,160
It's a quandary.

127
00:10:40,160 --> 00:10:46,640
I mean, this is a question that is valid.

128
00:10:46,640 --> 00:10:51,240
But it's also valid for, I mean turning the tables, I think it's also valid for the

129
00:10:51,240 --> 00:10:53,920
material world around us.

130
00:10:53,920 --> 00:10:57,160
How do we know that the laws of physics mean anything?

131
00:10:57,160 --> 00:11:04,040
How do we know it's not just God, you know, playing, playing around, you know, in his

132
00:11:04,040 --> 00:11:05,040
workshop?

133
00:11:05,040 --> 00:11:07,200
Well, how do we know tomorrow?

134
00:11:07,200 --> 00:11:11,440
God isn't going to just say, yeah, gravity who needs it and suddenly we all float

135
00:11:11,440 --> 00:11:15,720
off into space, but we don't.

136
00:11:15,720 --> 00:11:17,520
We're pretty sure that's not going to happen.

137
00:11:17,520 --> 00:11:21,120
There's no evidence or reason for us to believe that it's going to happen.

138
00:11:21,120 --> 00:11:26,440
What we don't know and no, that's probably not going to happen, but what we don't know

139
00:11:26,440 --> 00:11:31,080
is that like they take the laws of thermodynamics, we can't know whether those are true.

140
00:11:31,080 --> 00:11:35,640
We just say, well, that's, you know, what we understand and you can even say believe

141
00:11:35,640 --> 00:11:40,880
though it's a reasonable belief, it's rational and with so many things, the speed of light,

142
00:11:40,880 --> 00:11:41,880
they say it's constant.

143
00:11:41,880 --> 00:11:45,480
Well, how do we know it's constant, right?

144
00:11:45,480 --> 00:11:47,720
And so on and so on.

145
00:11:47,720 --> 00:11:49,360
It's not really to dispute any of these things.

146
00:11:49,360 --> 00:11:53,200
It's just to say, in fact, they're not in the realm of anything we can know.

147
00:11:53,200 --> 00:11:57,560
And I keep going back to Descartes and Apologize for everyone who's heard me say this

148
00:11:57,560 --> 00:12:02,880
many, many times, but he works for in a Western context to help frame this for a Western

149
00:12:02,880 --> 00:12:05,040
audience because he saw this.

150
00:12:05,040 --> 00:12:08,280
I mean, all the whatever you say about him, I don't know how great he was.

151
00:12:08,280 --> 00:12:13,520
I think many of the things I've sketched you from my perspective, but he said, you

152
00:12:13,520 --> 00:12:15,520
can't know any of this.

153
00:12:15,520 --> 00:12:21,480
But sort of this, it became, I think, this, this idea of the brain and the vat for anyone

154
00:12:21,480 --> 00:12:23,760
who's seen this very famous movie, The Matrix.

155
00:12:23,760 --> 00:12:26,480
Well, the Matrix didn't come up with that concept.

156
00:12:26,480 --> 00:12:33,960
They, they took it from this, this philosophical thought experiment that would, how do

157
00:12:33,960 --> 00:12:39,680
we know we're not just a brain and a vat and a vat is a thing that has fluid in it that

158
00:12:39,680 --> 00:12:44,560
keeps the brain alive and it's called these electrodes plugged into the brain.

159
00:12:44,560 --> 00:12:49,520
And it stimulates the brain and says to the brain, it's like virtual reality, basically.

160
00:12:49,520 --> 00:12:51,400
How do we know this isn't virtual reality?

161
00:12:51,400 --> 00:12:55,520
I mean, we're pretty sure it isn't and we have no reason to believe it is and it's,

162
00:12:55,520 --> 00:13:01,000
anyone who's suggested it is, is usually kind of laughed at because it's not a very reason.

163
00:13:01,000 --> 00:13:05,880
I mean, there's no reason to believe that, Occam's razor and all that.

164
00:13:05,880 --> 00:13:09,480
But, but it's not in the realm of things that we can know.

165
00:13:09,480 --> 00:13:13,200
Now Descartes and he said, well, what can I know?

166
00:13:13,200 --> 00:13:19,040
And this is where you came up with this cogito ergo sum, which means I caught, I'm cognizant,

167
00:13:19,040 --> 00:13:23,920
I'm cognizant, I'm conscious basically.

168
00:13:23,920 --> 00:13:28,320
Consciousness is something that, that exists because you can't trick someone into thinking

169
00:13:28,320 --> 00:13:37,640
they're thinking, I think we, you can't create consciousness without consciousness.

170
00:13:37,640 --> 00:13:38,720
It can't be something else.

171
00:13:38,720 --> 00:13:42,840
I mean, I mean, you can, the ideas it can be created for sure.

172
00:13:42,840 --> 00:13:43,840
And it's consciousness.

173
00:13:43,840 --> 00:13:44,840
It's not something else.

174
00:13:44,840 --> 00:13:48,840
You're not being tricked into thinking that you're conscious because you have to be conscious

175
00:13:48,840 --> 00:13:52,640
in order to be aware of it, right?

176
00:13:52,640 --> 00:13:57,240
Now that's all finding good for Descartes and I think it's a really great thing that

177
00:13:57,240 --> 00:13:58,840
he did there.

178
00:13:58,840 --> 00:14:03,080
But in Buddhism it goes even further because Buddhism, of course, is not so much interested

179
00:14:03,080 --> 00:14:09,280
in theory, we're interested in practice and on a practical level, not being able to

180
00:14:09,280 --> 00:14:13,800
know something for sure is significant.

181
00:14:13,800 --> 00:14:18,600
So when we sit down and close our eyes, we're not interested in whether we remember

182
00:14:18,600 --> 00:14:24,480
our past lives or whether we leave our body or so on.

183
00:14:24,480 --> 00:14:31,640
All of those are objects of experience, just as experiences in your body or memories

184
00:14:31,640 --> 00:14:34,920
of yesterday, what you had for breakfast.

185
00:14:34,920 --> 00:14:44,320
All of these are experiences and experiences are, well, the Buddhist important Buddhist

186
00:14:44,320 --> 00:14:50,080
theories that they're impermanent, they're unsatisfying and they're uncontrollable and that

187
00:14:50,080 --> 00:14:58,880
our grasping these things as stable, as satisfying, as controllable as me as mine and so

188
00:14:58,880 --> 00:15:02,320
on, possessiveness and identification.

189
00:15:02,320 --> 00:15:05,520
All of that is creative.

190
00:15:05,520 --> 00:15:13,760
I don't know, creative is the right word, it's creative for lack of a better word.

191
00:15:13,760 --> 00:15:17,840
It's creative in the sense that it's productive, that's the word I'm looking for.

192
00:15:17,840 --> 00:15:20,440
It produces a result.

193
00:15:20,440 --> 00:15:30,400
So if I want money, then suddenly I'm an employee or I'm a business person, suddenly I need

194
00:15:30,400 --> 00:15:33,400
a livelihood.

195
00:15:33,400 --> 00:15:40,720
If I want, suppose I want to get married, then oh boy, I have to, I probably need some

196
00:15:40,720 --> 00:15:47,520
new clothes, I have to shave every day, I have to shower every day, I have to pluck my nose

197
00:15:47,520 --> 00:15:53,160
hairs and so on, put on makeup or whatever and then I mean that's nothing, then I have

198
00:15:53,160 --> 00:16:06,600
to go out and I have to, wow, if I want to car, if I want, sorry, those are actually

199
00:16:06,600 --> 00:16:11,760
all too abstract, it's every moment of liking and this is what you see in meditation and

200
00:16:11,760 --> 00:16:15,800
this is what's important for this question, is that every moment of liking something is

201
00:16:15,800 --> 00:16:27,440
productive, it reinforces a habit or creates a new habit and it identifies things with

202
00:16:27,440 --> 00:16:28,440
results.

203
00:16:28,440 --> 00:16:32,520
So if I get this again, I'll be happy, we think of it and we think yes, I want that and

204
00:16:32,520 --> 00:16:36,560
when we want that, it's productive.

205
00:16:36,560 --> 00:16:42,880
Let's take a simple example, I'll be sitting in meditation and suddenly I think, ah,

206
00:16:42,880 --> 00:16:48,000
Facebook, I want to check Facebook and suddenly I'm no longer sitting in meditation, I'm

207
00:16:48,000 --> 00:16:50,000
checking Facebook, right?

208
00:16:50,000 --> 00:16:55,840
Oh, I want to listen to this monk, answer this question, oh, but what about Facebook?

209
00:16:55,840 --> 00:17:00,760
Or sorry, I'm listening to the monk, answer this question and say, oh, I wonder, I wonder

210
00:17:00,760 --> 00:17:07,120
what's going on in Facebook or, oh, someone just pinged me and you have this, this intention,

211
00:17:07,120 --> 00:17:11,800
this desire and suddenly you're no longer listening or maybe you're listening to me and suddenly

212
00:17:11,800 --> 00:17:16,600
you think of something and suddenly you're distracted, you're no longer listening to me or

213
00:17:16,600 --> 00:17:24,720
now thinking about something you have to do somewhere or some pleasure or something, right?

214
00:17:24,720 --> 00:17:31,160
So it's productive, craving is productive or any kind of, we use the word craving,

215
00:17:31,160 --> 00:17:41,720
the idea is any kind of impulse, any kind of wanting, let's say, wanting of anything,

216
00:17:41,720 --> 00:17:47,320
want to do this, want to do that, want to be this, want to do that, want this, want that,

217
00:17:47,320 --> 00:17:53,560
don't want this, don't want that, like, want this not to be, it's also a thing, ah, when

218
00:17:53,560 --> 00:17:57,240
when suppose I'm sitting in a mosquito's about buzzing around me, right?

219
00:17:57,240 --> 00:18:06,160
I don't want that mosquito and so black, I hit the, I hit the mosquito, I do, it's productive.

220
00:18:06,160 --> 00:18:10,720
That continues throughout our lives, that's the principle of Buddhism, that's why we

221
00:18:10,720 --> 00:18:16,400
practice, we practice to try and change that, so that when we hear the mosquito, it's just

222
00:18:16,400 --> 00:18:21,680
hearing, we say to ourselves, hearing, hearing and the idea is that it just becomes hearing,

223
00:18:21,680 --> 00:18:28,080
it's not bad, it's not a problem, it's not scary or annoying or anything and so it's not

224
00:18:28,080 --> 00:18:35,360
productive and we cultivate this habit and we change and we start to see these things that I was

225
00:18:35,360 --> 00:18:39,360
looking for pleasure and I was looking for happiness in, they're actually just creating

226
00:18:39,360 --> 00:18:44,240
more and more stress and I'm never going to get what I want because they're chaotic,

227
00:18:44,240 --> 00:18:50,640
they're impermanent, they're unsatisfying and controllable and so you see this throughout

228
00:18:50,640 --> 00:18:57,040
the practice, so that's the basic Buddhist premise and that's why rebirth becomes really

229
00:18:57,040 --> 00:19:07,360
important is because, okay, this is great but it only really has significance if it's, if it's

230
00:19:07,360 --> 00:19:13,840
uninterrupted because it's quite difficult to change your habits, it would be much easier if you just

231
00:19:14,560 --> 00:19:19,520
enjoyed the things you enjoyed, hated the things you hate, kind of get by for many of us,

232
00:19:19,520 --> 00:19:25,840
not for all of us. I think those of us who are in a position of privilege, anyone really

233
00:19:25,840 --> 00:19:30,880
was able to watch this YouTube, well most are many of the people who are able to watch this

234
00:19:30,880 --> 00:19:35,760
YouTube video and places of privilege and never had it really had to deal with, the horrors of

235
00:19:35,760 --> 00:19:42,400
war, famine, you know, I mean even probably there are people here that watching this

236
00:19:42,400 --> 00:19:49,600
who have suffered from sexual abuse or domestic violence, people who have had drug abuse issues,

237
00:19:49,600 --> 00:19:59,120
people who are on medications for depression and so on. All of these, all of these things,

238
00:19:59,120 --> 00:20:17,760
this is what we're working on. So the idea is that eventually, it becomes a question of whether

239
00:20:19,200 --> 00:20:24,160
whether this is going to catch up with us, whether we're going to have bad things happen to us

240
00:20:24,160 --> 00:20:33,280
and so on, whether we're going to have to deal with our reactions, deal with this result,

241
00:20:33,280 --> 00:20:40,480
the productivity of our attachment. So in regards to rebirth,

242
00:20:45,040 --> 00:20:53,280
the question is, how do we justify that it doesn't just come to an end, that this is actually

243
00:20:53,280 --> 00:21:00,240
important because it's interesting, it's interesting if craving leads to problems in your life,

244
00:21:00,880 --> 00:21:05,760
but the idea that it's going to lead to problems in a future life, where do you get that from?

245
00:21:07,600 --> 00:21:17,360
So the first thing I've commented before, talked about before, is that it's the null hypothesis,

246
00:21:17,360 --> 00:21:23,600
this is a scientific term, I learned it, that's sort of throwing out these big terms that

247
00:21:23,600 --> 00:21:28,160
you shouldn't really have to know, but it's a useful one and I picked it up for that reason,

248
00:21:28,160 --> 00:21:33,840
I don't just like to throw out terms, but the null hypothesis was a scientific term.

249
00:21:36,480 --> 00:21:43,920
It refers to the hypothesis that nothing is going to happen. In other words, you ask yourself,

250
00:21:43,920 --> 00:21:49,280
okay, if I'm going to hypothesize something, how is it different from nothing happening?

251
00:21:49,280 --> 00:21:53,440
And then nothing happening is the null hypothesis, right? Something like that, I'm actually,

252
00:21:53,440 --> 00:22:03,440
I'm maybe mangling it. But basically, if you say to yourself, what I experience now,

253
00:22:04,400 --> 00:22:11,600
this cause and effect relationship when I get caught up with something it affects me as a result,

254
00:22:11,600 --> 00:22:20,880
if you say that, and then you say, I believe that this exact same thing is going to happen

255
00:22:22,000 --> 00:22:29,360
forever, then you, that's the null hypothesis, you're basically said nothing is going to happen,

256
00:22:30,160 --> 00:22:33,120
nothing new anyway, nothing is going to be introduced into the system,

257
00:22:34,400 --> 00:22:38,160
so to that extent it's a null hypothesis, nothing's going to change.

258
00:22:38,160 --> 00:22:42,320
So then someone says, well, yes, but we've seen this event,

259
00:22:43,520 --> 00:22:50,480
we've seen this event where a person dies, the being dies, and all these ways by which

260
00:22:52,000 --> 00:22:58,640
experience occurs seem to cease with it. And so they introduce a hypothesis,

261
00:22:59,360 --> 00:23:04,160
it's no longer the null hypothesis, they say something changes when a person dies,

262
00:23:04,160 --> 00:23:10,720
the experience stops. Of course, you can't see because there's no eyes, you can't hear because

263
00:23:10,720 --> 00:23:13,760
the ears are not functioning, the brain is not functioning, basically.

264
00:23:18,480 --> 00:23:26,880
And so at the very least, this has to be where the claim is, the claim is not

265
00:23:26,880 --> 00:23:36,080
that we're reborn, the claim is that we don't die. The claim is that death is only

266
00:23:37,360 --> 00:23:43,760
a moment in this ever changing, ever continuing moments of experience.

267
00:23:45,520 --> 00:23:53,680
Now, it's not a bad hypothesis. In fact, Buddhism agrees. I would say Buddhism agrees that yes,

268
00:23:53,680 --> 00:23:59,120
at the moment in death, that's when there's no more experience.

269
00:24:00,800 --> 00:24:08,000
But there's a problem that only works if there's nothing that's going to be productive

270
00:24:08,000 --> 00:24:14,960
of more experience. Because it gets a little complicated in the sense that

271
00:24:14,960 --> 00:24:26,400
craving is not the only thing that's productive, the body is productive, the mind itself is

272
00:24:26,400 --> 00:24:34,880
productive, beyond just craving. And so that's why an enlightened being doesn't just stop living.

273
00:24:36,160 --> 00:24:40,960
I mean, it would be silly to think they would, but it's important to point out that

274
00:24:40,960 --> 00:24:45,040
just because you become free from craving doesn't mean there's nothing productive.

275
00:24:45,760 --> 00:24:51,600
There's your life, there's your body, there's your mind. And so you live, a person who

276
00:24:51,600 --> 00:24:56,480
becomes enlightened doesn't just stop living, of course. It's pointing out that, oh yes,

277
00:24:56,480 --> 00:25:01,040
right, of course, it's not just craving that's productive. It's only one of the things that's

278
00:25:01,040 --> 00:25:06,080
productive or clinging or whatever you want to go. So the person lives that they're live.

279
00:25:06,080 --> 00:25:12,560
But enlightened being, when they die, all of those other productive things are gone. Craving

280
00:25:12,560 --> 00:25:19,360
itself is also gone. And so there's nothing productive. When a enlightened being passes away,

281
00:25:19,360 --> 00:25:25,200
that's why they enter what we call barringy ban or there's the cessation and there's no more

282
00:25:25,200 --> 00:25:39,120
rebirth. So the claim that we make is that this this productive craving is real. It's independent

283
00:25:39,120 --> 00:25:49,120
of ordinary bodily functions that keep us alive. And so at the moment of death, if there is still

284
00:25:49,120 --> 00:25:55,840
craving, it's going to give a result. And so that's that's how you approach this idea of rebirth.

285
00:25:56,880 --> 00:26:00,640
That's that's the science behind it. I think this is going to be a longer answer. I hope

286
00:26:00,640 --> 00:26:05,120
you're bearing with me. I think it's interesting. But I haven't really, there's one last thing to

287
00:26:05,120 --> 00:26:14,160
talk about. And that's how it is or to what extent a person can become enlightened without

288
00:26:14,160 --> 00:26:20,160
believing in rebirth. Because I've kind of suggested that you don't need to believe in rebirth.

289
00:26:20,160 --> 00:26:26,160
If you believe against it, it's going to be a problem potentially, may not be. But either way,

290
00:26:26,800 --> 00:26:31,040
as you practice, I mean, I guess I've already mentioned this, but just to be clear,

291
00:26:32,080 --> 00:26:40,320
as you practice, you become more clear in the truth of rebirth. Just because it's an extrapolation

292
00:26:40,320 --> 00:26:45,680
of this knowledge that shouldn't be clear. I mean, me telling you that craving is productive

293
00:26:45,680 --> 00:26:49,280
shouldn't be clear unless you've done meditation, unless you've actually looked and seen.

294
00:26:50,480 --> 00:26:55,200
It's something that you learn as you go and you really see, wow, this is how it works. There's

295
00:26:55,200 --> 00:27:01,440
nothing mysterious about why I cling to things or why I suffer. It's my, it's habits. It's the

296
00:27:01,440 --> 00:27:08,480
building up of habits of of productive karma, what we call karma. Right karma is just this,

297
00:27:08,480 --> 00:27:18,640
these moments of productivity. And so you start to see that. And so for a beginner minute here,

298
00:27:19,360 --> 00:27:24,640
it starts to become clear and you start to open up to the idea of rebirth and say, oh, yeah,

299
00:27:24,640 --> 00:27:29,440
yeah, I can see how that could happen. I mean, this is really productive. It's really powerful

300
00:27:29,840 --> 00:27:35,600
in ways that we didn't realize. Right, we don't realize because we don't realize so,

301
00:27:35,600 --> 00:27:42,320
which, which is why or how we go out and get things. Right. If we knew the danger of clinging to

302
00:27:42,320 --> 00:27:48,000
things, we wouldn't cling. And they say, whoa, that, that's just cause for stress. And we don't

303
00:27:48,000 --> 00:27:52,320
realize it. And you start to see this. You don't see boy, this is really just causing me stress

304
00:27:52,320 --> 00:28:03,280
and suffering. For an advanced meditator, I guess I would just say that it becomes increasingly

305
00:28:03,280 --> 00:28:09,280
clear to the point where the idea that when you become enlightened, the idea that craving

306
00:28:09,280 --> 00:28:17,920
wouldn't lead to rebirth. It's just ridiculous. The idea that craving is not productive,

307
00:28:17,920 --> 00:28:23,600
that craving is just chemicals in the brain or something like that. It just doesn't make any sense

308
00:28:23,600 --> 00:28:27,680
anymore. I mean, your mind doesn't work in that way. You just don't see things in that way.

309
00:28:27,680 --> 00:28:42,320
And, but it, it, it is never the most important belief. I think belief, even out of faith,

310
00:28:42,320 --> 00:28:49,360
is useful to sort of give you an idea, like faith in the, in the, in the thermonuclear bomb is

311
00:28:49,360 --> 00:28:53,360
a good thing to have faith in. It's good to believe that the nuclear bombs do exist,

312
00:28:53,360 --> 00:29:00,240
because they could very, there's a very real potential for them to obliterate humankind

313
00:29:01,360 --> 00:29:11,600
still today. I mean, it's kind of that magnitude, rebirth is important in order to remind us that

314
00:29:11,600 --> 00:29:16,240
we're not going to get off scot-free. You know, all of our habits, all of these things that we're

315
00:29:16,240 --> 00:29:22,720
building up. And it should make sense, I think, if you're not steeped in some materialist tradition,

316
00:29:22,720 --> 00:29:28,640
it should make sense that what we have here is what's real. I mean, what we're experiencing

317
00:29:28,640 --> 00:29:35,360
moment to moment is real. It doesn't just suddenly change. I mean, it's kind of like a carryover

318
00:29:35,360 --> 00:29:41,760
from, if I dare say, from, from theistic religions that believe that God is going to judge us when

319
00:29:41,760 --> 00:29:46,000
we die and it's heaven or hell or something. You know, there's sort of one life and that's all it is.

320
00:29:46,000 --> 00:29:52,160
Now Buddhism says, this is all that there is, this present moment, this reality.

321
00:29:54,560 --> 00:30:01,760
So that's sort of my take on, on rebirths. I mean, there's more that you can say there are always

322
00:30:01,760 --> 00:30:09,120
hints that you can get and a part of meditation is about understanding the difference between

323
00:30:09,120 --> 00:30:15,040
the body and mind and some of the realizations you come to and things like leaving your body

324
00:30:15,040 --> 00:30:22,000
or remembering past lives are, are quite interesting in that sense. But they don't ever get to this

325
00:30:22,000 --> 00:30:29,040
point of understanding karma of understanding this moment after moment of experience and the

326
00:30:29,040 --> 00:30:36,320
productive nature of, of craving, which really underpins it all. Once you see that as you realize

327
00:30:36,320 --> 00:30:46,880
that through your practice, the idea of of rebirth is just an extension of it. It's just a part of it

328
00:30:46,880 --> 00:30:51,920
really. It's not even an extension. It's just basically saying, oh, yes, at that moment the same thing

329
00:30:51,920 --> 00:30:59,200
happens as has always been happening. So there you go. I mean, I don't think there's anything I

330
00:30:59,200 --> 00:31:06,000
could say that would really convince people who aren't who are are set against it. And so the

331
00:31:06,000 --> 00:31:11,680
last thing I would say and I've kind of said it already is that clinging to views, if you cling

332
00:31:11,680 --> 00:31:17,280
to a view of belief, either in rebirth, let's say or against rebirth, it's going to be problematic.

333
00:31:17,280 --> 00:31:21,040
I mean, clinging against it will be more problematic from my perspective because it's wrong.

334
00:31:22,320 --> 00:31:28,400
But clinging to it can also be problematic as well because it, it, it becomes a belief and,

335
00:31:28,400 --> 00:31:36,880
and it's, it's never going to be exactly the reality. You know, you say, yes, yes, I believe in

336
00:31:36,880 --> 00:31:40,720
rebirth. I understand that I've thought about it. I've heard, listen to this talk about it.

337
00:31:41,440 --> 00:31:45,440
It's never going to be the same as you actually experiencing it for yourself. The truths of karma,

338
00:31:45,440 --> 00:31:52,640
the truths of a momentary karma. Karma is just every moment that we are productive.

339
00:31:52,640 --> 00:32:01,920
We produce some desires, some yearnings of habit and so on. So don't cling to views. If a person is

340
00:32:01,920 --> 00:32:07,760
stubbornly, I don't believe in rebirth. I believe it's wrong. Well, drop that. It's heavy.

341
00:32:08,560 --> 00:32:13,840
It's not going to help you in any way to, you know, great good for you that you have this view,

342
00:32:13,840 --> 00:32:18,640
but not good for you. It's just going to get in your way. You'd be much better off to investigate

343
00:32:18,640 --> 00:32:24,800
scientifically, try and understand the sorts of things that they cart under,

344
00:32:24,800 --> 00:32:30,880
to go get to Argosum, that there is experience, that experience arises in cases and it's really

345
00:32:30,880 --> 00:32:37,200
all that we can be sure of. And everything else that we think we're sure of turns out to be

346
00:32:37,200 --> 00:32:43,760
illusion, turns out to be abstraction and kinds of concepts. And once you'd let go of that and let

347
00:32:43,760 --> 00:32:52,160
go of your conceptions, all of our cravings sort of slowly vanish. And we come to see things objectively,

348
00:32:52,160 --> 00:32:58,800
as experiences that arise and see something. I guess it's kind of ironic that when you finally

349
00:32:58,800 --> 00:33:05,280
know the truth about rebirth, you're not reborn again. But it does say something about the nature

350
00:33:05,280 --> 00:33:13,600
rebirth that it's not a great thing. It's not a very good thing at all. It's not a good

351
00:33:13,600 --> 00:33:18,160
sense. I mean, many people when they hear about rebirth, they're excited. They're like, wow,

352
00:33:18,160 --> 00:33:23,680
really? I get another chance. I can come back and do this all again. Yeah, if you want to, I suppose.

353
00:33:26,320 --> 00:33:31,680
So you learn slowly, not only that it's the truth, but that it's not a very good one. It's

354
00:33:31,680 --> 00:33:47,600
problematic. Okay. So that's all I have to say about that. Thank you all for tuning in. Have a great night.

