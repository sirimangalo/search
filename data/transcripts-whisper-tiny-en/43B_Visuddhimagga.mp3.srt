1
00:00:00,000 --> 00:00:07,000
Developed and cultivates in this way, the contemplation of dissolution, the object of which

2
00:00:07,000 --> 00:00:12,440
is positioned, consisting and destruction for and break up of false formations, then from

3
00:00:12,440 --> 00:00:18,080
engines class, according to all kinds of becoming generations, just an institution or able

4
00:00:18,080 --> 00:00:21,720
to be a peer to him in the form of great terror.

5
00:00:21,720 --> 00:00:28,720
That means when he has developed this knowledge of seeing the dissolution of things, then

6
00:00:28,720 --> 00:00:39,040
he comes to see all life, all kinds of becoming generations, just an institution or able

7
00:00:39,040 --> 00:00:51,160
to be, they all mean just life, both are described in different names and so all these

8
00:00:51,160 --> 00:01:07,680
are taken here, so you can just say life or existence, so you see existence or life as

9
00:01:07,680 --> 00:01:19,200
a great terror, full of danger, as lions, tigers, leopards, bears, iron, hyenas, spirits,

10
00:01:19,200 --> 00:01:24,600
tigers, fears, bulls, service dogs, rat medians, white elephants, hideous animals,

11
00:01:24,600 --> 00:01:30,200
sedans, thunderbolts, channel grounds, battlefields, flaming cool kids, etcetera, a peer

12
00:01:30,200 --> 00:01:33,200
to a timid man who wants to live in peace.

13
00:01:33,200 --> 00:01:42,080
The yogi will not see all these things, but we must understand that in the books it is

14
00:01:42,080 --> 00:01:54,560
given in all its diversity, but a particular yogi will not see all of them, he may see one

15
00:01:54,560 --> 00:01:55,720
or two of them.

16
00:01:55,720 --> 00:02:02,040
When he sees how far from his sense of sees, present ones are seizing and those who

17
00:02:02,040 --> 00:02:06,880
be generated in the future will see in just the same way then what is called knowledge

18
00:02:06,880 --> 00:02:15,800
of appearance as terror arises in him at that stage, so he sees, he sees all phenomena,

19
00:02:15,800 --> 00:02:24,240
all existence as with terrible, and then it's simply is given, so I will not read the

20
00:02:24,240 --> 00:02:32,040
same way, there are two similes, the second simile on page 754, a woman with an infected

21
00:02:32,040 --> 00:02:45,920
woman, the part he will use is puritya, purity means rotten, and puritya means offspring,

22
00:02:45,920 --> 00:02:57,120
so literally translated means the woman with rotten offspring, maybe the same thing as

23
00:02:57,120 --> 00:03:05,640
with an infected woman, who's offspring do not live, so whenever his son or daughter

24
00:03:05,640 --> 00:03:12,920
is born, he or she dies in a little later, so that's what is meant, so he has the attention

25
00:03:12,920 --> 00:03:18,520
and so on, but I don't know that it is important, but that's the knowledge of appearance

26
00:03:18,520 --> 00:03:25,960
as terror, fear, or that's not fear, that means when a yogi has reached this stage, is

27
00:03:25,960 --> 00:03:42,000
he afraid, does he fear, or is he afraid, no, if he is afraid or if he fears, then he

28
00:03:42,000 --> 00:03:50,800
has fallen away from meditation, because fear is an also mental state, and when fear is

29
00:03:50,800 --> 00:03:56,720
in your mind, in the mind of the yogi, then he is not doing meditation at that moment,

30
00:03:56,720 --> 00:04:03,320
so he does not fear, for it is simply the mere judgments that thus formations of seeds present

31
00:04:03,320 --> 00:04:07,960
once of seeding and future ones will cease, just as a man with eyes looking at three

32
00:04:07,960 --> 00:04:13,640
charcoal pits at the city page is not into the fray, since he only forms the mere judgment

33
00:04:13,640 --> 00:04:20,400
that all two form and if them will suffer no little pain and so on, so although he see

34
00:04:20,400 --> 00:04:30,760
the existence or formations of mental and physical phenomena as terrible, as fearful, he

35
00:04:30,760 --> 00:04:48,720
himself does not fear them, he is not afraid of them, and then the next paragraph,

36
00:04:48,720 --> 00:04:55,440
at least three, but it is called appearance as terror only, because formations in all

37
00:04:55,440 --> 00:05:01,800
kinds of becoming generations that they need to station or about a fearful envy, bound

38
00:05:01,800 --> 00:05:07,600
for destruction, and so they appear only as terror, here is the text about its appearance

39
00:05:07,600 --> 00:05:17,320
to him as terror and so on, so he sees both the sign and the occurrence as empty, vain,

40
00:05:17,320 --> 00:05:23,720
void without power or guilt, like an empty village, a mirror, a goblin city, etc., and

41
00:05:23,720 --> 00:05:28,400
it brings them to mind as not self, and so the sign and occurrence appear to him as

42
00:05:28,400 --> 00:05:35,360
a terror, the sign means shape and form of mother, physical phenomena, and occurrence,

43
00:05:35,360 --> 00:05:42,440
occurrence, do you understand what is meant by occurrence, he uses occurrence many times

44
00:05:42,440 --> 00:05:51,800
here, in this chapter, occurrence really means not just arising, arising and then continues

45
00:05:51,800 --> 00:05:58,880
to stay for some time, that the Hollywood is for water, the border, and it is meant not

46
00:05:58,880 --> 00:06:08,520
just arising, arising and then continuing for some time, this is the knowledge of appearance

47
00:06:08,520 --> 00:06:16,440
of terror, as terror, the next is contemplation of danger, so as he repeats, develops and

48
00:06:16,440 --> 00:06:20,520
cultivates the knowledge of appearance as terror, he finds no asylum, no shelter, no

49
00:06:20,520 --> 00:06:26,640
police, to go to no refuge in any kind of becoming generation, destiny, station and about,

50
00:06:26,640 --> 00:06:36,280
so when he sees all these as terrible, then he finds no shelter, no police, no solids, because

51
00:06:36,280 --> 00:06:44,120
whatever phenomena, a phenomena, he can don't place on, he sees them as all disintegrating,

52
00:06:44,120 --> 00:06:49,960
so there is no shelter, no refuge, in all kinds of becoming generation, destiny, station

53
00:06:49,960 --> 00:06:56,040
and about there is not a single formation, that he can place his hopes in a hole or two,

54
00:06:56,040 --> 00:07:01,640
the three kinds of becoming appear like chocolates full of glowing holes, the full primary

55
00:07:01,640 --> 00:07:07,880
elements like hideous Panama slaves, the five aggregates like murderers and with raised

56
00:07:07,880 --> 00:07:15,720
weapons, the 16 internal bases like an anti-velished, the six external bases like village

57
00:07:15,720 --> 00:07:20,840
raiding robots, the second stations of consciousness and the nine apples of being as

58
00:07:20,840 --> 00:07:27,400
dull, burning, blazing and glowing with the 11 fires and all formations appear as a huge

59
00:07:27,400 --> 00:07:33,720
mess of dangers, just the truth of satisfaction or substance, like a tumor, a disease in

60
00:07:33,720 --> 00:07:41,880
diagonally and affliction and then out and so on, so during this stage, he's everything,

61
00:07:42,440 --> 00:07:53,640
he sees everything faulty, he cannot see anything to hold on to anything to place his hopes on

62
00:07:53,640 --> 00:08:00,280
and so on and so on and so he feels very helpless during this stage of meditation,

63
00:08:04,120 --> 00:08:14,840
it is like a beer as a forest thick, a forest thicket of seemingly pleasant aspect but

64
00:08:14,840 --> 00:08:21,080
infested with wild peas and so on, the blazes seemingly good, but there are wild beasts

65
00:08:21,080 --> 00:08:26,280
he came full of tigers, water-hundred by monsters and ogres and enemy with raised root,

66
00:08:26,280 --> 00:08:31,720
poison food, a root beset by robbers, a burning cool, a battlefield between containing armies,

67
00:08:31,720 --> 00:08:39,480
appears to be a timid man who wants to live and peace, so during this stage of knowledge,

68
00:08:39,480 --> 00:08:49,880
whatever he sees, he sees them as he sees it as faulty, as dangerous and there is no refuge or no

69
00:08:49,880 --> 00:08:59,400
continuation for him at that time, sometimes yogis, when yogis have reached his stage,

70
00:09:01,320 --> 00:09:07,880
they don't know what to do, because they feel like they are depressed or something like that,

71
00:09:08,440 --> 00:09:14,840
because whatever the officer seems to be going away and so there is no,

72
00:09:14,840 --> 00:09:28,360
you know, not at all, not solely for him, some event, some event, left meditation and go back

73
00:09:28,360 --> 00:09:46,040
to the office, because they feel so helpless at this stage of practice, and then there is a long

74
00:09:46,040 --> 00:09:59,480
quotation and the explanation, so we will skip those, and then we come to where seven and one hundred and fifty

75
00:09:59,480 --> 00:10:15,880
eight, ten knowledge he understands, for the paragraph 42, one who understands knowledge of danger

76
00:10:15,880 --> 00:10:21,480
understands when it takes fear as his ten kinds of knowledge, that is the fact based on arising

77
00:10:21,480 --> 00:10:28,440
etcetera and the fact on non-arising and so on, and you can imagine earlier, when skipping

78
00:10:28,440 --> 00:10:32,920
these two kinds of knowledge with skill in the institute that is knowledge of danger and knowledge

79
00:10:32,920 --> 00:10:39,160
of the state of peace, the various views will shake him not, he does not resulate about views that

80
00:10:39,160 --> 00:10:47,000
focus such as the ultimate dependence here and now and the rest is clear, so this is the contemplation of

81
00:10:47,000 --> 00:10:55,640
danger, so at this stage he sees danger and also he has a knowledge of the state of peace, I mean

82
00:10:55,640 --> 00:11:03,240
nebulize, nebulize, peaceful nebulize good and the world is full of danger, something like that he sees,

83
00:11:03,240 --> 00:11:11,560
so there are two kinds of seeing here, one is let us say positive and the other is negative,

84
00:11:15,400 --> 00:11:30,360
then after seeing, after seeing phenomena as dangerous, he goes on and he comes to another stage,

85
00:11:30,360 --> 00:11:34,680
when he sees all formations in this way as danger, he becomes dispassionate towards,

86
00:11:36,040 --> 00:11:43,720
some others translated as turning away from, so he turns away from, or he becomes dispassionate towards,

87
00:11:44,280 --> 00:11:50,520
this is described with, takes no delight in the manifold field of formations belonging to any kind

88
00:11:50,520 --> 00:11:58,920
of becoming destiny and so on, so when he sees danger in them, then he is dispassionate towards

89
00:11:58,920 --> 00:12:10,120
that, he turns, he turns away from them, just as a golden form that loves the foothills of

90
00:12:10,120 --> 00:12:16,680
take-up peace, finds delight, not in a filthy puddle, a decade of a village of outcasts, but only in

91
00:12:16,680 --> 00:12:22,600
the seven great lakes, so to this very day, this one finds delight not in the manifold formations

92
00:12:22,600 --> 00:12:29,320
in clear as danger, but only in the seven contemplations, because he delights in development, that means

93
00:12:29,320 --> 00:12:43,160
he delights in the practice, just as the lion, he won't be, he finds delight, not when put into a gold

94
00:12:43,160 --> 00:12:49,000
cage, but only in him earlier with his three thousand leaves extent, so to be motivated, the lion finds

95
00:12:49,000 --> 00:12:59,400
delight, not in the triple becoming of a healthy destiny, but only in the three contemplation, and just

96
00:12:59,400 --> 00:13:09,080
as chag dandar, king of elephants, all quite with seven full stans, possessed of supernormal power,

97
00:13:09,080 --> 00:13:14,520
who travels through the air, finds place and not in the midst of a town, but only in the chag dandar

98
00:13:14,520 --> 00:13:20,360
lake and works in the Himalaya, so to do this muddy day, the air, it finds delight not in any

99
00:13:20,360 --> 00:13:25,880
formation, but only in the state of peace, seen in the way beginning no arising a safety and

100
00:13:25,880 --> 00:13:37,000
his mind, tans, inclines and limbs towards it. Now, there's a footnote regarding chag dandar, it's not

101
00:13:37,000 --> 00:13:48,680
important, but in one thing, we should understand this chag dandar. Now, tandar means the tasks of an

102
00:13:48,680 --> 00:14:01,560
elephant, cham means six, so chag dandar means and could mean an elephant with six tasks, but in

103
00:14:01,560 --> 00:14:14,520
the Java, it is explained that not that the elephant had six tasks, but the elephant has a head

104
00:14:14,520 --> 00:14:28,440
task which emits six colors or six colors raised, so when people paint chag dandar elephant,

105
00:14:28,440 --> 00:14:35,320
they paint with only two tasks, but in one of the paintings, I remember seeing in a book,

106
00:14:35,320 --> 00:14:44,520
like the paintings from, I think, a chag dandar or some caves in India, and they drew the elephant

107
00:14:44,520 --> 00:14:57,400
with six tasks, but in the Java, it just stayed there, the elephant has tasks emitting six

108
00:14:57,400 --> 00:15:05,320
colors raised, that is why he is called chag dandar. Chag dandar was one incarnation of

109
00:15:05,320 --> 00:15:11,640
bodhisattva, say before he became the bodhisattva he was, he was born as an elephant, a quite elephant,

110
00:15:12,600 --> 00:15:19,160
so this referred to it again. All quite with seven full stands, possessed of super normal power,

111
00:15:19,160 --> 00:15:23,400
who travels through the air, French place and not in the midst of a town, but only in

112
00:15:23,400 --> 00:15:27,000
the chag dandar, they can put them in the Himalayas, so to this, maybe they did the elephant,

113
00:15:27,000 --> 00:15:31,960
France, they like, not in any formation, but only in the state of peace, even the way

114
00:15:31,960 --> 00:15:37,400
beginning, not arising in safety, and this might change in France and this was that. Now,

115
00:15:38,680 --> 00:15:47,160
I don't want to talk about this, but in the foot now, all the expression with seven full stands,

116
00:15:47,160 --> 00:16:00,920
the MCs, he didn't translate it. Seven stands, seven full stands,

117
00:16:00,920 --> 00:16:16,200
believe it or not. Now, you see the parid there, parid the sentence, hachat, parada, voila, right? Yeah.

118
00:16:17,160 --> 00:16:24,440
What he calls he, bumit, prusen, he sata, he put it, he told it, sata put it on.

119
00:16:24,440 --> 00:16:35,560
He touches the ground with seven of his limbs, hachat, in the trunk, one, parada, four feet,

120
00:16:36,920 --> 00:16:41,640
voila means to tail, and what he calls, I mean, may remember,

121
00:16:46,200 --> 00:16:49,560
so it is explained this way in another commentary too,

122
00:16:49,560 --> 00:16:56,840
that is not different, and I'm not important here. So, these are knowledge of dispassion,

123
00:16:56,840 --> 00:17:01,800
so this knowledge of dispassion or knowledge of turning, turning will come after the knowledge

124
00:17:01,800 --> 00:17:09,560
of danger. So, it is logical, you first see the arising and disappearing, and then after

125
00:17:09,560 --> 00:17:14,120
that you see only the disappearing, when you see things disappearing or disintegrating,

126
00:17:14,120 --> 00:17:22,360
you see that they are fear, they are with fear, or they are dangerous, they are terrible or something,

127
00:17:22,360 --> 00:17:27,960
and then you find fault with them, because they are dangerous, they are with danger,

128
00:17:31,000 --> 00:17:36,760
you see danger in them, or you find fault with them, and after finding fault with them,

129
00:17:36,760 --> 00:17:46,280
you are dispassion to us there, we don't have anything to do with them, so that is knowledge

130
00:17:46,280 --> 00:17:54,600
of contemplation of dispassion. Now, here, knowledge of contemplation of danger is the same

131
00:17:54,600 --> 00:18:00,840
as the last two kinds of knowledge and meaning, and meaning means in essence, in reality they are

132
00:18:00,840 --> 00:18:06,520
the same. Hence, the ancient said, knowledge of appearance is terror while only one has three names,

133
00:18:07,320 --> 00:18:15,320
it's all formations has terror, that's the name, appearance is terror, arose. It allows the

134
00:18:15,320 --> 00:18:21,240
appearance of danger in those same formations, that's the name, contemplation of danger arose.

135
00:18:21,240 --> 00:18:26,120
If the rules becoming dispassionate towards those same formations, that's the name,

136
00:18:26,120 --> 00:18:31,480
contemplation of dispassion arose with three names. Also, we just said in the text, understanding

137
00:18:31,480 --> 00:18:37,160
of appearance as terror, knowledge of danger and dispassion, these things are one in meaning,

138
00:18:37,160 --> 00:18:42,360
only the letter is different, so only the words are different, but the meaning is one,

139
00:18:43,400 --> 00:18:47,480
so they mean the same, same, the same Vipasana knowledge.

140
00:18:47,480 --> 00:18:59,080
Now, when one sees danger and then, once to turn away from it, the next thing is desire for

141
00:18:59,080 --> 00:19:07,240
deliver, to get out of it. First, you see danger and then you become dispassionate towards it,

142
00:19:07,240 --> 00:19:12,520
now you want to get rid of it, so this is for desire for deliverance. When going to this knowledge

143
00:19:12,520 --> 00:19:18,360
of dispassionate, this glance when becomes dispassionate towards it, it's a desire to take

144
00:19:18,360 --> 00:19:23,480
no delight in any single one of all the many formations and any kind of becoming generation,

145
00:19:23,480 --> 00:19:29,880
definitely a special consciousness or a world of being, it's my no-long mistakes fast, please,

146
00:19:29,880 --> 00:19:39,640
first hand is all to them, so whatever object he observes, his mind does not want to take it actually,

147
00:19:39,640 --> 00:19:49,160
his mind is not stuck to them, and he becomes deseras of being delivered from the whole field of

148
00:19:49,160 --> 00:19:55,960
formations and escaping from it, so when we really see the danger of something, we want to get out

149
00:19:55,960 --> 00:20:04,600
of it, we want to get rid of it, it is natural, and so here the similarly is given, just as a fish

150
00:20:04,600 --> 00:20:12,440
and a net, he frog in his next jaw, he jungle falls shut in the cage and so on, now that's

151
00:20:12,440 --> 00:20:20,600
the mention of Rahu here, the moon inside Rahu's mouth, and then in the photo he said Rahu is a name

152
00:20:20,600 --> 00:20:28,200
for the eclipse of sun or moon, but analyzed as a demon who takes them in his mouth, so whenever

153
00:20:28,200 --> 00:20:35,000
there's an eclipse we said, oh Rahu has swallowed the moon, Rahu has swallowed the sun,

154
00:20:36,760 --> 00:20:48,680
and when at the end of the eclipse we said, oh Rahu has thrown out the moon,

155
00:20:48,680 --> 00:21:01,800
since Rahu is a demon, it brings it as a gaze, the name Rahu learned to his son,

156
00:21:02,440 --> 00:21:10,440
because a demon is a hindrance or an obstacle or something, so when his son was born he thought

157
00:21:10,440 --> 00:21:16,840
there is another attachment from me, another object of attachment from me, so he is like a Rahu

158
00:21:16,840 --> 00:21:27,720
to me, so his son was named Rahu laugh, now this is a desire for deliverance, or this I have to

159
00:21:27,720 --> 00:21:35,240
keep it of formations, to get rid of existence altogether, after that knowledge of contemplation

160
00:21:35,240 --> 00:21:45,080
of reflection, doing it again, yeah, in this desire of deliverance from all the many full

161
00:21:45,080 --> 00:21:50,040
formations in any kind of becoming generation just in his state or about, in order to be delivered

162
00:21:50,040 --> 00:21:55,960
from the whole field of formations, he again descends those same formations, attributing to them

163
00:21:55,960 --> 00:22:10,040
the three characteristics by knowledge of contemplation of reflection, so he wants to get rid of

164
00:22:10,040 --> 00:22:18,120
the existence and knowledge of these things, but he cannot just leave them alone, he has to

165
00:22:18,120 --> 00:22:27,320
take them as object of his meditation again, so he had to see the impermanent suffering and

166
00:22:28,040 --> 00:22:37,880
no soul nature of these formations again, so in Pari the what is Padizankar, but it means again

167
00:22:37,880 --> 00:22:46,840
and Sankami is knowing, so knowing again, so contemplation of observing again observing the

168
00:22:46,840 --> 00:22:52,760
three characteristics again, so he sees all formations as impermanent for the following reasons

169
00:22:52,760 --> 00:23:06,680
and so on, you can compare these passages with those in the section on comprehending by groups,

170
00:23:08,120 --> 00:23:17,800
so he again sees a metaphysical phenomena as impermanent, as painful and as not so,

171
00:23:17,800 --> 00:23:25,080
and in different ways as they are non-continuous and temporarily limited by rise and fall and so on,

172
00:23:31,240 --> 00:23:37,400
no, and here Isemin is given and this is a very good semi eye,

173
00:23:39,000 --> 00:23:46,280
I often tell this semi to people on paragraph 49,

174
00:23:46,280 --> 00:23:53,240
but why does he design them in this way, that is in order to confront the means to deliverance,

175
00:23:53,240 --> 00:24:02,680
why has he to observe these major and physical phenomena again, because he must have some means

176
00:24:02,680 --> 00:24:09,640
or some means of deliverance, here is a semi, he may have thought to catch a fish, it seems,

177
00:24:09,640 --> 00:24:14,920
so he took a fishing net and cast it in the water, now here fishing net really means,

178
00:24:14,920 --> 00:24:23,480
what do you call there, like a basket, you put it in the water and then the fish is caught in

179
00:24:23,480 --> 00:24:29,320
the basket and then you lower your hand down and then take the cold of the fish,

180
00:24:31,560 --> 00:24:41,000
do you call that a net, fishing net, I don't know, because I thought net means

181
00:24:41,000 --> 00:24:47,480
to the other thing with you, to throw into the water. I don't think there may be a

182
00:24:47,480 --> 00:24:52,600
fish, I need the only fish in my nose, but I don't think I've ever heard of a basket used for

183
00:24:52,600 --> 00:24:58,120
fishing, usually for crabs they use like pots they're called, for baskets or some kind of lobsters

184
00:24:58,120 --> 00:25:06,200
benefit, actually not really a basket, it is a final shape, I've seen that, there's a net like

185
00:25:06,200 --> 00:25:16,360
that comes to a cone, you should have a name for it, something like a basket, so you put it in

186
00:25:16,360 --> 00:25:25,000
the water and the fish are caught in it, and there is a hole for you to take them out,

187
00:25:26,760 --> 00:25:33,400
yes, so he took a fishing net and cast it in the water, he put it in the mouth of the net

188
00:25:33,400 --> 00:25:40,120
under the water and sees the snake by the net, thinking it was a fish, he wasn't glad, thinking

189
00:25:40,120 --> 00:25:44,120
I have caught a fish, and I believe that he had caught a big fish, he lifted it up to fish,

190
00:25:44,120 --> 00:25:54,200
when he saw three marks, so he took three lines and the neck of the snake, he perceived that

191
00:25:54,200 --> 00:25:59,400
it was a snake, he knew that it was a snake and he was terrified, he saw danger, felt dispersion,

192
00:25:59,400 --> 00:26:06,040
reversion for what he had seen, and decided to deliver, to be delivered from it, so he wanted to throw

193
00:26:06,040 --> 00:26:16,360
it away, but he was afraid that it might bite him, throw it away as it is, so confronting a means to

194
00:26:16,360 --> 00:26:22,600
deliver and wrap the quail from his hand, starting from the tip of its tail, then he raised his arm,

195
00:26:22,600 --> 00:26:28,040
and when he had weekend snake by swinging it two or three times on his head, he flanged it away,

196
00:26:28,040 --> 00:26:36,840
crying, go fall snake, so his feeling, the snake two or three times is like his,

197
00:26:38,440 --> 00:26:44,600
he's contemplating on the information, so on, on the formations again,

198
00:26:46,600 --> 00:26:52,760
then quickly scrambling up on to dry land, he stood, looking backwards, he had come, thinking

199
00:26:52,760 --> 00:26:58,200
goodness, I had been delivered from the jaws of his huge snake, and then applying of the simile

200
00:26:58,200 --> 00:27:05,160
to do the actual experience as an expert across, so at this point, knowledge of reflection has

201
00:27:05,160 --> 00:27:10,200
arisen in him with reference to which he said and so on, so this is called knowledge of reflection,

202
00:27:10,200 --> 00:27:26,120
I think, in that book, the what uses not reflection but some other thing.

203
00:27:41,160 --> 00:27:51,400
Knowledge of misery, re-observation, very good, the knowledge of re-observation,

204
00:27:56,360 --> 00:28:01,560
having thus descend by knowledge of contemplation of reflection that all formations avoid,

205
00:28:01,560 --> 00:28:08,600
he again descends whiteness in the double logical religion and so on, they are not not so easy to

206
00:28:08,600 --> 00:28:13,320
understand, the double is easy to understand, but quadruple logical not revision,

207
00:28:15,000 --> 00:28:21,800
the text itself is very, very strange, now the double, let's talk about double, so the

208
00:28:21,800 --> 00:28:30,360
double logical religion means this is white of self, or of what belongs to self, so this is white

209
00:28:30,360 --> 00:28:36,440
of self is one, and this is white of what belongs to self is that, so there are two ways of

210
00:28:36,440 --> 00:28:44,600
descending the whiteness, it is white, white of self, it is white of what belongs to self,

211
00:28:45,960 --> 00:28:52,200
now the quadruple logical religion is very difficult to understand, do you see that?

212
00:28:52,200 --> 00:29:04,680
No, I mean, in the photo he says, the message is a difficult one, it is indeed,

213
00:29:06,600 --> 00:29:11,880
because the party text, you have to party there, not quite the name,

214
00:29:11,880 --> 00:29:22,280
because it is not the name, it is not the name, it is not the name, it is not the name, it is not the name,

215
00:29:22,280 --> 00:29:36,920
this message actually suggested by the translator was used by those of other fate during the time of the

216
00:29:36,920 --> 00:29:47,320
Buddha, and it may not be true or party actually, like, you know, you know, practice languages,

217
00:29:48,360 --> 00:29:55,960
practice languages are similar to party, but they are different from a party, it is something like

218
00:29:55,960 --> 00:30:05,560
corrupt form of Sanskrit or party, so that is why this is very difficult to understand,

219
00:30:08,680 --> 00:30:20,600
but let me see, the photo is very good, the last paragraph on that thing, the commentorial,

220
00:30:20,600 --> 00:30:29,480
commentorial interpretation, given here is summed up by bm as follows, now seeing in four ways,

221
00:30:29,480 --> 00:30:38,360
right, the quadruple logical relation, so four ways, the first is what he sees the non-existence

222
00:30:38,360 --> 00:30:47,560
of his self, that means there is no self of me, that is one scene, and the second he sees of his own

223
00:30:47,560 --> 00:30:56,200
self itself, itself not to, itself, that it is not the property of another's self, now,

224
00:30:58,760 --> 00:31:07,880
there is no self of me, and I am no property of another's, another person's self,

225
00:31:07,880 --> 00:31:21,240
that is the second one, that that one is, there is no self of another person,

226
00:31:23,640 --> 00:31:32,760
and I am not the property of that person, so that they are full, see, there is no self in me,

227
00:31:32,760 --> 00:31:41,320
and somebody is not my, not the property of myself, and then there is no self of others,

228
00:31:43,880 --> 00:31:50,120
and I am not the property of the self of others, so in these four ways,

229
00:31:51,720 --> 00:32:01,160
he has to contemplate, now he sees the non-existence of another's self, that is one thing,

230
00:32:01,160 --> 00:32:07,000
he sees of another, that that other is not the property of his own self,

231
00:32:13,160 --> 00:32:20,760
so in four ways, he contemplates, and then 55, in six modes, again, he contemplates,

232
00:32:22,120 --> 00:32:28,680
that I is quite yourself, or of the property of self, and so on, and then next paragraph,

233
00:32:28,680 --> 00:32:35,720
in ten modes, and then next paragraph, in first mode, and then next paragraph, in 42 modes,

234
00:32:38,920 --> 00:32:43,400
one here, this is information for attributing the three characteristics to them, and seeing them,

235
00:32:43,400 --> 00:32:49,160
that is, why didn't this create, he abatows both terror and delight, and he becomes indifferent to them,

236
00:32:49,160 --> 00:32:54,600
and neutral, he never takes time, as I know, as mine, he is like a man who has divorced,

237
00:32:54,600 --> 00:33:03,800
divorced, his wife, and so on, so he will go to bed next time, so he, during this stage of knowledge,

238
00:33:04,520 --> 00:33:16,840
he re-observe the mental and physical phenomena, in many different ways, say, to see the

239
00:33:16,840 --> 00:33:25,400
impermanent, soulless, suffering, and soulless needs out of that, and Mahasizyaros said, again,

240
00:33:25,400 --> 00:33:34,760
here that he doesn't need not go through all the modes given here, it is impossible,

241
00:33:34,760 --> 00:33:46,840
but as a book, all the methods of observing is given, but he doesn't should not do all these,

242
00:33:46,840 --> 00:33:55,080
in order to get to the next higher stage, so maybe one or two, he maybe one or two, and then

243
00:33:55,080 --> 00:34:11,160
he can go onto the higher stages, or one thing, please, in part of 57, say, about five or six

244
00:34:11,160 --> 00:34:19,720
lines, has been capable of being had as one which has been susceptible to the exercise of mastery,

245
00:34:19,720 --> 00:34:27,480
as alien, as secluded from past and future, in fact, as secluded from course and result,

246
00:34:34,040 --> 00:34:43,000
and then in the footnote, 25, the meaning such as what is common usage in the world is called,

247
00:34:43,000 --> 00:34:49,480
the being is not materiality, it's not in density here, because it is not implied by what it says,

248
00:34:50,680 --> 00:35:01,080
the actual meaning is a little different, because it is established without saying anything,

249
00:35:01,960 --> 00:35:08,360
what do you call that? It's so, so, so evident that it doesn't need any explanation.

250
00:35:08,360 --> 00:35:18,760
Sir, sir, sir, evident, right, so we can say that because it is self evident,

251
00:35:21,000 --> 00:35:28,440
not it is not being right by what it says, it is so plain that it doesn't need any explanation

252
00:35:29,080 --> 00:35:34,280
that is what it's meant, for the common usage of the world does not speak of mere materiality

253
00:35:34,280 --> 00:35:44,120
as a being, so when common people say, this is a being, it doesn't mean that the matter is being,

254
00:35:45,160 --> 00:35:49,960
so when it says something is somewhat of a being, it means mind and matter of thought,

255
00:35:50,920 --> 00:35:59,400
so the word, the word, it's not in Pali here, should not be understood as meaning a being,

256
00:35:59,400 --> 00:36:07,160
but meaning and armour and armour, so that is what is, what the common data is driving at,

257
00:36:09,080 --> 00:36:15,240
so what is intended as a being is the self that is conjectured by outsiders, that means

258
00:36:15,240 --> 00:36:31,960
not Buddhist, and then let us see, okay, so we are going to

259
00:36:31,960 --> 00:36:45,000
equanimity about formations next time, and as I said before, this book would be very, very

260
00:36:46,120 --> 00:36:55,720
good, because it gives what what a yogi really experiences during practice of meditation,

261
00:36:55,720 --> 00:37:04,200
and with our coefficients and all, so it's simpler and better reading the day than the

262
00:37:04,200 --> 00:37:08,040
miso demaga, but miso demaga is a basis for that book.

263
00:37:08,040 --> 00:37:27,640
Okay, so we buy this book. Thank you, Bob. Thank you, Bob. Thank you for your call.

264
00:37:27,640 --> 00:37:38,600
Okay, next time we will check with this chapter, we will check with this chapter.

265
00:37:47,480 --> 00:37:56,920
No, we cannot skip the stages, but sometimes what happens is it doesn't go through two or three stages

266
00:37:56,920 --> 00:38:05,880
in one day, that can happen, but we cannot skip the stage, because we are logic logical,

267
00:38:06,440 --> 00:38:11,720
say one after the hour, but we will go through three stages and only in one day,

268
00:38:11,720 --> 00:38:27,640
sometimes maybe even more than three stages in one, one day of meditation.

