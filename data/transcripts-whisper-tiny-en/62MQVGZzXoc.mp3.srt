1
00:00:00,000 --> 00:00:03,000
Hello, welcome back to Ask a Monk.

2
00:00:03,000 --> 00:00:09,000
Today I thought I would try to answer at least one of the questions that are waiting for me.

3
00:00:09,000 --> 00:00:14,000
And the question I'm thinking of now is on euthanasia.

4
00:00:14,000 --> 00:00:23,000
It was asked, what is the Buddhist stance on killing someone who wants to die

5
00:00:23,000 --> 00:00:33,000
or who would perhaps be freed from suffering through their own death?

6
00:00:33,000 --> 00:00:41,000
Now, I hope it's quite clear that I don't think it should be clear from what I've said before that I don't think they would be freed from suffering through their own death.

7
00:00:41,000 --> 00:00:46,000
I think that's something that I don't have to get into.

8
00:00:46,000 --> 00:00:54,000
Because my views on that are pretty clear, I mean, only through giving up, clinging, are you free from suffering?

9
00:00:54,000 --> 00:01:09,000
And that's really a key here that the view and the belief that killing someone could somehow be better for them is delusional

10
00:01:09,000 --> 00:01:22,000
because it's something that they are clinging to and it's an aversion that they have towards the suffering and to indulge that is by no means of any benefit to them.

11
00:01:22,000 --> 00:01:33,000
Anytime we look at a being who's suffering greatly and we think that by putting them out of their misery we're somehow doing them a favor is quite mistaken.

12
00:01:33,000 --> 00:01:54,000
When in fact, some of the greatest insights and wisdom and realizations of the wake-up enlightenment that comes from suffering, that comes from bearing with your suffering and learning to overcome the clinging and the aversion that we have towards it.

13
00:01:54,000 --> 00:01:56,000
That's where the real benefit is.

14
00:01:56,000 --> 00:02:16,000
So countless stories, if you talk to people who work with dying patients in hospice care or so on, they will tell you about the moment of clarity that comes to a person who's suffering greatly, that right before they die, not everyone, but many people will have this moment where suddenly they're free.

15
00:02:16,000 --> 00:02:22,000
And it's so clear that they've gone through something and they've worked through something and they've worked it out.

16
00:02:22,000 --> 00:02:27,000
Finally, at peace with themselves and there's clarity of mind and they're able to move on.

17
00:02:27,000 --> 00:02:37,000
When you kill, when you cut that off, first of all, especially if it's their wish that they should die, then you're helping them escape from the lesson.

18
00:02:37,000 --> 00:02:48,000
You're helping them to run away from this temporarily, from this lesson that they have to help them to overcome the clinging.

19
00:02:48,000 --> 00:02:56,000
Helping them to encourage their clinging to their aversion to pain and suffering.

20
00:02:56,000 --> 00:03:07,000
That's the one side, but I think more important is to talk about the effect of killing on one's own mind, because that's what makes, as I've said before, an action immoral or moral.

21
00:03:07,000 --> 00:03:09,000
It's the effect that it has on your own mind.

22
00:03:09,000 --> 00:03:12,000
It's not really the suffering that it brings to the other person.

23
00:03:12,000 --> 00:03:18,000
The most horrible thing about killing someone else is the disruptive nature of killing.

24
00:03:18,000 --> 00:03:25,000
When you kill something that wants to live, it's of course a lot more disruptive than when you kill someone that wants to die.

25
00:03:25,000 --> 00:03:28,000
But nonetheless, it's an incredibly disruptive act.

26
00:03:28,000 --> 00:03:31,000
It's a very powerful and important act.

27
00:03:31,000 --> 00:03:38,000
For people who have never killed before, for you to never kill before, it's very hard to understand this.

28
00:03:38,000 --> 00:03:44,000
I think they will generally have a natural aversion to it because of the weight of the act.

29
00:03:44,000 --> 00:03:47,000
So many people have never killed even insects and so on.

30
00:03:47,000 --> 00:03:52,000
And really feel it abhorrent to do such a thing just by nature.

31
00:03:52,000 --> 00:03:55,000
And that's because of the strength of the act.

32
00:03:55,000 --> 00:04:01,000
But it may be difficult for them to understand because they've never experienced.

33
00:04:01,000 --> 00:04:11,000
On the other hand, for someone who kills often a person who murders animals or slaughters animals or insects and so on,

34
00:04:11,000 --> 00:04:18,000
it's equally difficult for them to see because they've become desensitized to it.

35
00:04:18,000 --> 00:04:23,000
I've told the story before, but when I was younger, I would do hunting.

36
00:04:23,000 --> 00:04:28,000
I thought to do hunting with my father, to hunt with my father and hunt deer.

37
00:04:28,000 --> 00:04:34,000
And I killed one deer and it was really a very difficult thing to do.

38
00:04:34,000 --> 00:04:37,000
It surprised me because I had no qualms about it.

39
00:04:37,000 --> 00:04:40,000
I didn't think there was anything wrong with killing at the time.

40
00:04:40,000 --> 00:04:43,000
I thought it's a good way to get food.

41
00:04:43,000 --> 00:04:48,000
But when it came time to actually kill the deer, my whole body was shaking.

42
00:04:48,000 --> 00:04:54,000
And it surprised me that I didn't understand what was going on, what was happening.

43
00:04:54,000 --> 00:05:06,000
But of course, later on when I practiced meditation and really woke up and had this great wake-up cause to what I was doing to my mind.

44
00:05:06,000 --> 00:05:16,000
And so many things that I had to go through and have to work through and all of these emotions that came up that I had to sort out and realize and give up.

45
00:05:16,000 --> 00:05:26,000
And just changing my whole outlook until finally there was this great transformation.

46
00:05:26,000 --> 00:05:35,000
When you go through meditation and I was in a pretty bad state, there's a lot of cleaning that goes on, even just in the first few days.

47
00:05:35,000 --> 00:05:45,000
And so, made me realize that, how many of you realize that, oh, that's what was going on now.

48
00:05:45,000 --> 00:05:48,000
There really is a great weight to killing.

49
00:05:48,000 --> 00:05:55,000
And I think that's quite clear when you find tune your mind.

50
00:05:55,000 --> 00:06:02,000
When you start to practice meditation, your mind becomes very quiet, relatively speaking.

51
00:06:02,000 --> 00:06:07,000
You're able to see that any little thing that comes up, you're able to see it much better than before.

52
00:06:07,000 --> 00:06:16,000
Whereas when your mind is very active, how in your full of defilement and greed and anger and so on, you can't really make head or tail of anything.

53
00:06:16,000 --> 00:06:25,000
So it's really difficult for most people to come up with any solid theory of reality or solid understanding of reality when you talk about these things.

54
00:06:25,000 --> 00:06:31,000
It kind of sounds interesting, but it's really hard for them to understand because they've never taken the time to quiet their mind

55
00:06:31,000 --> 00:06:38,000
to receive things, clearly, one by one by one, and to be able to break things apart.

56
00:06:38,000 --> 00:06:44,000
But it's like you have a very fine tuned instrument and you're able to see these things that people aren't able to see.

57
00:06:44,000 --> 00:06:55,000
And so, killing becomes very abhorrent and you're able to see how even the slightest anger has a great power to let alone the destructive power.

58
00:06:55,000 --> 00:07:03,000
When you say something to someone and it changes their life and it disrupts their course, there's great power in it.

59
00:07:03,000 --> 00:07:10,000
But to kill has a far greater power. It's on a whole other level.

60
00:07:10,000 --> 00:07:15,000
Even when you kill someone who wants to die, you're changing their karma.

61
00:07:15,000 --> 00:07:21,000
It's like trying to help someone who's running from the law.

62
00:07:21,000 --> 00:07:25,000
For instance, I'm going to rob the bank, you have to hide them. It's a lot of work.

63
00:07:25,000 --> 00:07:30,000
And that's really what's going on here when you kill someone who wants to die.

64
00:07:30,000 --> 00:07:35,000
It's no less, but it's still on the level of killing someone who doesn't want to die.

65
00:07:35,000 --> 00:07:40,000
Because it's a great and weighty act to kill and it's something that weighs on your mind.

66
00:07:40,000 --> 00:07:50,000
It's a very, there's repercussions because of the power and the intensity of the act of the disruption.

67
00:07:50,000 --> 00:07:57,000
If you think of the world, the universe is being waves, energy waves, and even physical, of course, this energy.

68
00:07:57,000 --> 00:08:08,000
If you think of it as all energy, the explosion of energy that you've created, mental energy is tremendous by killing.

69
00:08:08,000 --> 00:08:11,000
If there's someone who's never killed before, it might be difficult to see.

70
00:08:11,000 --> 00:08:14,000
This is why people think, well, euthanasia could be a good thing.

71
00:08:14,000 --> 00:08:23,000
Now, there's one exception that I've talked about before, and I think it is an exception. I'm not 100% sure, but to me, you could at least argue for it.

72
00:08:23,000 --> 00:08:26,000
And that is letting someone die.

73
00:08:26,000 --> 00:08:33,000
I think clearly you can, you can and should let people die rather than giving them medication.

74
00:08:33,000 --> 00:08:37,000
That's only going to prolong their misery or so on.

75
00:08:37,000 --> 00:08:44,000
I think that's reasonable, but I think also you could stop life support.

76
00:08:44,000 --> 00:08:54,000
If it was considered that they had no chance or maybe even at the mind had left or so on, there was the case.

77
00:08:54,000 --> 00:08:58,000
Actually, apparently that's what happened with Mahasi Sayad, that they said, well, the mind is no longer working.

78
00:08:58,000 --> 00:09:05,000
The brain is no longer functioning, so they could take them off life support because he was no longer there.

79
00:09:05,000 --> 00:09:09,000
But on the other hand, this is where you could be excused.

80
00:09:09,000 --> 00:09:17,000
But on the other hand, my teacher in Thailand gave an interesting perspective on this that you can take as you like.

81
00:09:17,000 --> 00:09:21,000
But he said, well, you never really know.

82
00:09:21,000 --> 00:09:26,000
And we can say that the person is not going to come back, but it's never really clear.

83
00:09:26,000 --> 00:09:33,000
There was the case, and he told this story about someone who they thought was going to die, but they kept them on life support for some time.

84
00:09:33,000 --> 00:09:37,000
And eventually, they were covered and came back and lived another 10 years or something.

85
00:09:37,000 --> 00:09:39,000
And we're able to do all sorts of goodies.

86
00:09:39,000 --> 00:09:43,000
Now, his point was the person has the opportunity and it's life to do good deeds.

87
00:09:43,000 --> 00:09:48,000
You shouldn't be so quick to cut them off because you don't know where they're going.

88
00:09:48,000 --> 00:09:53,000
This is why we say always that this human life is very precious.

89
00:09:53,000 --> 00:10:01,000
Now, the second story I gave about Mathakundaly, the boy who was on his deathbed and had a chance to see the Buddha.

90
00:10:01,000 --> 00:10:07,000
It's just an example of how we can possibly create wholesome mind states before the moment of death.

91
00:10:07,000 --> 00:10:19,000
And you never really know if the person has a chance now as a human being, if their mind is impure, then letting them die or helping them to die.

92
00:10:19,000 --> 00:10:29,000
He's only going to lead them or destined to an unpleasant result, an unpleasant life in the future.

93
00:10:29,000 --> 00:10:37,000
So, if you can help them to stay and to at least help them to practice meditation or listen to the dhamma or so on,

94
00:10:37,000 --> 00:10:44,000
or at least be with their family and learn about compassion and love and so on, and cultivate good thoughts.

95
00:10:44,000 --> 00:10:51,000
And even cultivating patience with the pain, which is an excellent learning tool, learning experience.

96
00:10:51,000 --> 00:10:56,000
It's something that we should not underestimate and undervalue.

97
00:10:56,000 --> 00:11:01,000
There's going to be a great thing, and it's something that we should actually encourage.

98
00:11:01,000 --> 00:11:09,000
Something that is far better than to dismiss the person and send them on their way, not knowing where they're going to go, and not just send them on their way,

99
00:11:09,000 --> 00:11:12,000
but distract their life.

100
00:11:12,000 --> 00:11:23,000
So, even in the case of not killing, but letting a person die, it can be that helping them to stay on is a good thing.

101
00:11:23,000 --> 00:11:26,000
If you have to judge for yourself, it really depends on the individual.

102
00:11:26,000 --> 00:11:32,000
I think I wouldn't want to stay on, and if it came down to taking medication or so on, I would just let myself go.

103
00:11:32,000 --> 00:11:41,000
But you really have to judge for yourself, and for a person who's not meditating for a person who hasn't really begun to practice,

104
00:11:41,000 --> 00:11:47,000
it's always dangerous to let them go because you don't know where they're going.

105
00:11:47,000 --> 00:11:53,000
Once you can see, I guess the answer would be if once you can see that clearly they're on the right path and they're ready to go,

106
00:11:53,000 --> 00:11:58,000
then you can help them to give up medication or to get off life so far and so on.

107
00:11:58,000 --> 00:12:05,000
And say, go on your way and be reassured that they're going in the right direction.

108
00:12:05,000 --> 00:12:13,000
Just be careful that they wouldn't be better off sticking around to do more good deeds and practice more meditation,

109
00:12:13,000 --> 00:12:16,000
because you don't really know where they're going in the future.

110
00:12:16,000 --> 00:12:22,000
So, I hope that that gave some insight into the Buddhist take on euthanasia.

111
00:12:22,000 --> 00:12:47,000
Thanks for tuning in all the best.

