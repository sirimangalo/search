1
00:00:00,000 --> 00:00:08,920
Hi, now I'll give the number one reason why everyone should practice meditation.

2
00:00:08,920 --> 00:00:12,960
So the number one reason why everyone should practice meditation, not just monks, not just

3
00:00:12,960 --> 00:00:17,400
Buddhists, not just people who live up in the forest, but why everyone should practice

4
00:00:17,400 --> 00:00:21,360
meditation, because meditation makes you free.

5
00:00:21,360 --> 00:00:25,240
Now really everything I've been talking about up to now should have sort of given this

6
00:00:25,240 --> 00:00:29,440
away already, but if it's not clear already this is the number one reason why we practice

7
00:00:29,440 --> 00:00:30,440
meditation.

8
00:00:30,440 --> 00:00:33,560
And this is what we call nirvana, if you've ever heard of this word nirvana, but never

9
00:00:33,560 --> 00:00:37,440
really understand what it meant, it simply means freedom.

10
00:00:37,440 --> 00:00:45,440
Freedom not to suffer, freedom not to have to have to go through unpleasant situations.

11
00:00:45,440 --> 00:00:50,720
Freedom from defilement, freedom from mental impurity, freedom from addiction, freedom from

12
00:00:50,720 --> 00:00:55,960
hatred, freedom from bigotry, freedom from wrong views, freedom from conceit, freedom from

13
00:00:55,960 --> 00:01:05,280
all sorts of unpleasant, unwholesome, unskillful and useless states of mind, total freedom

14
00:01:05,280 --> 00:01:10,240
and it's not freedom to do anything, of course you're always free to do what you want.

15
00:01:10,240 --> 00:01:15,040
In fact it can really, severely limit the things that you decide to do, because a lot

16
00:01:15,040 --> 00:01:20,760
of the things we decide to do are actually based on suffering and leading to suffering.

17
00:01:20,760 --> 00:01:27,640
It's not freedom to, it's freedom from real and true freedom is like flying like a bird

18
00:01:27,640 --> 00:01:32,760
where you don't cling to anything, wherever you go you take just your two wings with

19
00:01:32,760 --> 00:01:33,760
you.

20
00:01:33,760 --> 00:01:40,080
A bird is an animal which is able to fly free and not have to cling to anything.

21
00:01:40,080 --> 00:01:44,280
So wherever we go in whatever situation that we are, we're not attached to anything.

22
00:01:44,280 --> 00:01:48,880
We aren't attached to people, we aren't attached to places, we aren't attached to things.

23
00:01:48,880 --> 00:01:53,120
We're able to live our lives in whatever situation and not be caught up with should be,

24
00:01:53,120 --> 00:02:00,040
should not be, wish it were, wish it weren't, or any kind of judgment whatsoever.

25
00:02:00,040 --> 00:02:08,000
We live like a bird and wherever we go we eat the fruit from the tree wherever we land.

26
00:02:08,000 --> 00:02:13,760
The practice of meditation is something which allows us to live our lives like a bird,

27
00:02:13,760 --> 00:02:19,200
free like a bird, it's something which allows us to stop grasping and stop clinging and

28
00:02:19,200 --> 00:02:23,960
that really everywhere and anywhere we can be truly happy and at peace with ourselves, at

29
00:02:23,960 --> 00:02:26,280
peace with the world around us.

30
00:02:26,280 --> 00:02:30,800
So this one I think you really have to see for yourself, it's not something that you can

31
00:02:30,800 --> 00:02:32,200
just take my word for.

32
00:02:32,200 --> 00:02:35,840
So the practice of meditation is something that you have to practice by yourself, these

33
00:02:35,840 --> 00:02:39,440
videos are not just for your entertainment pleasure, they're something that you should

34
00:02:39,440 --> 00:02:44,680
really put into practice and since the meditation is something that allows us to be free,

35
00:02:44,680 --> 00:02:48,360
I think it's something that I would encourage everyone to please take the time to at

36
00:02:48,360 --> 00:02:49,360
least try.

37
00:02:49,360 --> 00:02:53,040
If you try it and you find that it doesn't work for you, then fine.

38
00:02:53,040 --> 00:02:58,080
Or please do contact me and I'll try to convince you otherwise or explain to you why

39
00:02:58,080 --> 00:03:01,800
it might be that you're not able to get results in the practice.

40
00:03:01,800 --> 00:03:05,800
Take a little bit of time and a little bit of patience and you yourselves can understand

41
00:03:05,800 --> 00:03:07,200
the benefits of meditation.

42
00:03:07,200 --> 00:03:11,240
Thanks for tuning in and I sincerely hope that this leads everyone watching it and

43
00:03:11,240 --> 00:03:37,840
at least a significant number of people to peace, happiness and freedom from suffering.

