This morning, I continued talking about the things which lead to success and prosperity,
and often the sense, we call it the highest blessings.
Today, we continue on with Qarau, this translated as respect, or image, or something like that.
Today, we talk about respect for the practice of a pamanda.
A pamanda is a difficult word to translate to English, although the meaning is clear.
The pamanda means negligence.
The pamanda means the opposite of negligence or non-negligence.
You can translate it as vigilance or diligence or heatfulness,
meaning of pamanda is that we should be mindful at all times.
Not only be mindful at all times, but altogether for things.
We should be free from anger or ill will towards other beings.
We should be always mindful.
We should be equanimists or level-headed.
Our minds should be unmoved by good things or things which arise.
We should stay equanimists in the face of whatever might arise.
Composed, our minds be composed of whatever arises.
When good things arise, our minds instead get greedy or attached to them.
Instead, we set ourselves on being mindful.
We don't let green come and take over our minds.
When anger might arise, when we see your hair, smell,
how bad things arise.
Instead of letting anger arise, we need to be internally composed or level-headed,
balanced in the face of the thing of this.
And to train ourselves to become free from craving.
This is what we are going to call the kapamanda for vigilance or heatfulness.
The respect for this means that we should practice respecting the Lord with teaching on kapamanda.
In fact, the last words of the Lord Buddha before He passed away,
we had a masam kara, a pamatina, pamatina, pamatina.
Everything which arises is of the nature to pass away.
For this reason, we should strive on work hard to fulfill the teaching of kapamanda,
fulfill the teaching of vigilance.
When we practice meditation, we should not think that there is also some occasion
for taking a vacation from our practice.
Maybe in the morning we practice, but in the afternoon we let our minds wander
and fall into greed or fall into anger.
If it is work hard to cut off the wandering mind, cut off the mind which attaches
to external objects or internal objects.
It is like a simile yesterday of the man carrying a pot of oil.
I have already talked about kapamanda.
Today I just talked briefly about kapamanda.
I have to give another story to talk about kapamanda.
I am going to go to kapamanda.
The most negligent person, no person that existed in the time of kapamanda.
He killed altogether 999 people.
His plan was to kill 1000 people.
His plan was to kill his mother who would be the 1000th victim.
The Lord Buddha knew that this was going to happen.
Of course, if he killed his mother, he would never dance to become free from suffering in this life.
The Lord Buddha went to visit Angulimala and so many people protested.
In the end, Angulimala became a student of the Lord Buddha.
He became a monk.
Cut off his hair and beard.
Put on the orange robes.
In the end, he became an Arab.
The Lord Buddha said, you hope to be from a chitmapatasu and from a chitm.
So, among those kambapasi, he became a pamut algat and you are.
Though in the past, someone was negligent.
Later on, they are no longer negligent.
When later on, they become someone who is not negligent, they light up the whole world,
just like the moon, which lights up the world when it comes up behind it, from behind a cloud.
This is the teaching of a pamanda.
As we should always be mindful, we should not be negligent in our practice or negligent in our training of our minds.
We should work very hard to fight against our natural tendencies, to greed or to anger,
that we can develop the tendency to not be greedy, to not be angry.
The last one in the series on respect is respect for hospitality.
That we should receive guests.
We should be able to receive other people.
Now, receiving other people is in two ways.
One, we should be ready to receive other people in terms of giving them objects that they might need.
Food and shelter and clothing and medicine.
We should also receive hospitality in terms of giving guests or visitors, giving them the dhamma.
We should always remember that whenever people come to visit us.
What we can give to them is two things.
We can give them items that they might need and we can give them the dhamma.
In the dhamma, it doesn't mean we should sit around and talk with them.
Try to teach them this or teach them that.
It means we have to encourage them to go and practice, encourage them to carry out the practice of bypassing the meditation.
In fact, this is an important point in receiving visitors.
We have to be careful not to think that we have to find out all sorts of information about them or sit around and talk with them for a long time.
Visit with them.
When visitors come, we have to encourage them quickly to go and practice because they don't have so much time and we also don't have so much time.
We don't know when is going to be the end of our time here.
No one knows whether death will come tomorrow.
We waste our time talking or visiting.
This is not the way to receive guests, receive people who come to us or wherever we go when we go home or when we stay here.
We have to receive guests by putting them to practice right away.
Giving them the opportunity to practice, not wasting their time visiting the chatting or so on.
Giving them the things that they need to practice and then give them them to teach them how to practice.
Once they know how to practice and send them off right away to practice.
For more time we have for them to practice.
We can really be said to have been hospitable in the highest sense.
This is the end of respect thing, having respect.
All in all we have to have a respectful attitude towards these six things.
The training and not being negligent and didn't receiving guests.
Next we go on, the next blessing is new at Oja.
I think highly of oneself and put other people down.
It is not to hold on to oneself.
Not to hold on to the idea of self that I am this, I am that.
This is me, that is me, this is mine, this is under my control.
New at Oja means to be humble.
Like Sariputah, Sariputah is an very good example of humility.
Like in himself to an outcast in the time of the Buddha there were people who were outcast from Hindu society.
They had to hit a stick against the ground, they had to carry a walking stick and beat it against the ground to make noise wherever they went.
So that the higher class people, the Brahmins and the Katya's and other classes would know that they were coming and wouldn't be able to avoid their touch.
Sariputah likened himself to one of these people, he said, as far as how I look at myself, it is the same.
Because it happened that when he was walking by a young monk, his robe touched the monk as he was walking.
And the monk sprinted around that Sariputah had hit him, Sariputah had hit this monk.
And Sariputah said for someone who was attached to themselves, then maybe they could commit such a deed, but for me he said, I am like an outcast.
He said, I am like the earth.
People may do what they want to the earth, but the earth never complain.
If we want to be really humble, we have to be like the earth.
We want to be like the water, we want to be like the air.
Like the air is because anyone can come and write on the earth or attack the air.
You can come and attack the air and write on the earth.
Nothing stays just in the same way someone who is humble,
whether someone says bad things to them or does bad things to them, it doesn't stay with the person who is humble.
They start to attach to self.
They see that these things are only body and mind, sound or sight or smell or taste or feelings or thoughts.
And they come and they go, and one doesn't hold on to them.
One should be like the water in the same way.
When you write on the water or when you people can urinate in the water or defecate in the water or throw things in the water and the water never complain.
Water accepts it all.
People write on the water or cut the water or hit the water and never complain.
On the same with the earth, they will do what they want with the earth.
They dig the earth and cut the earth.
They spoil the earth and the earth never complain.
I'm going to say, this is how we should be.
This is the teaching of Neemat on the child.
Very important for meditators.
We have to let go of ourselves.
Not always to think about the bad things that people did to us.
No matter what they did to us.
They didn't do it to anyone, no one did anything.
These things come and go there and I see.
They have to let go.
Let go in the mind.
These are only things that happened in the past.
They happened because of causes and conditions.
All that's left now is our own thought.
We just let go of our own thought.
We don't go back to it at all.
Next, we have Sun2T.
Sun2T means contentment.
This is a great blessing.
But in Buddhism, we need to have more Sun2T and Sun2T.
Both of them have been praised by the Buddha.
Contentment and discontent are both been praised by the Buddha.
Contentment means we have to be content with what are
requisites we have.
Whatever clothing we have, we have to be content with it.
This is why monks and nuns, they wear ragrobes.
They wear robes made of pieces of cloth, stitched together.
Then with whatever clothing they get.
Content with food, we have to be content with whatever
alms would we get.
Content with shelter, whatever lodging we get, we have to be
content, not worrying that it's too hard or too cold or too
noisy.
This is not too small.
We should be content with whatever lodging we have.
With what medicines we have, not to worry about finding this
medicine or that medicine that we should be content to
take whatever medicine we get.
We need not to be worried about sicknesses which might arise
and the body always trying to fix the body with this or
that medicine.
But us and to team is we have to be discontent.
Discontent means discontent with whatever
of attainment which we have gained in the practice.
We have to always think to ourselves that there is a higher
state which we have to get to gain.
Though we have heard or we know about the
attainment of Arahan, the attainment of freedom from
suffering, we still haven't reached the attainment of freedom from
suffering.
We still have greed, we still have anger, we still have
delusion in these things can still arise in the future.
If we are not careful, they will come back again and again to
haunt us.
So we should not be content with the level of practice which we
have attained, we should never rest content.
This is one thing we should never be content with.
Just the word Buddha is teaching.
If we rest content with those spiritual attainment we have, it's
most likely that more bad and evil states will come into our
hearts.
This is being negligent neglecting our duties.
And so greed, more greed and anger and delusion will arise and
cover over our minds again.
If we content with a small attainment, not even be
content with the scriptures they say, not even the
Buddha said, not even be content with an academy.
We can know we've cut off greed and anger, we should not be
content with that until we have cut off delusion.
This is the teaching on some community.
Next, we have Kattan.
Kattan, you can call in and have some of them.
I think I'll see those for tomorrow.
Time for morning chanting.
Please continue to practice this morning.
I guess we have an ordination.
Congratulations.
You can go ahead and probably start chanting.
