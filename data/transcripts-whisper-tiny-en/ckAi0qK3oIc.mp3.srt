1
00:00:00,000 --> 00:00:05,700
I've been thinking about asking for a girl's number who's been staring at me, who have

2
00:00:05,700 --> 00:00:10,880
been terrified of even with meditation since I fear her, this is a question, should I

3
00:00:10,880 --> 00:00:16,600
confront her or would that pursue backfire?

4
00:00:16,600 --> 00:00:28,080
Giving dating advice, I can get in trouble here, if I give you advice as to how to

5
00:00:28,080 --> 00:00:35,080
hook up with someone, I can, it's a serious offense of the Vinay.

6
00:00:35,080 --> 00:00:44,480
Guys, they can't tell you how to get the girl.

7
00:00:44,480 --> 00:00:47,920
Someone's already given an answer, be brave and talk to her, girls love guys who are

8
00:00:47,920 --> 00:00:48,920
confident.

9
00:00:48,920 --> 00:00:54,240
Yeah, if I said something like that, I can be serious, in serious trouble for the Vinay.

10
00:00:54,240 --> 00:01:05,840
Just to let you know, that's a difficult question to answer.

11
00:01:05,840 --> 00:01:13,840
They shouldn't fear people at all, and you could create a harmonious relationship outside

12
00:01:13,840 --> 00:01:21,360
of romance, if you were to talk to her, that's where I would go with it.

13
00:01:21,360 --> 00:01:27,520
I think the best advice you can give to someone is to create friendships, whether romance

14
00:01:27,520 --> 00:01:34,040
occurs or not, it's not really going to make you happy, in fact it often makes you miserable,

15
00:01:34,040 --> 00:01:39,440
but friendship will never, true friendship will never make you miserable, love for people

16
00:01:39,440 --> 00:01:45,120
will never make you miserable, but you have to be able to separate love from attachment.

17
00:01:45,120 --> 00:01:54,360
So what we think of as love is really love attachment, or it's too mixed together.

18
00:01:54,360 --> 00:01:58,320
So yeah, I think that should help you with your fear as well.

19
00:01:58,320 --> 00:02:10,040
If you looked at it as a chance to make a friend, or a chance to cultivate loving kindness,

20
00:02:10,040 --> 00:02:26,320
if you aren't afraid of losing something, you see, because why are you afraid?

21
00:02:26,320 --> 00:02:32,960
Because of how you look, because of how she'll think of you, because of you don't know

22
00:02:32,960 --> 00:02:37,280
how to act or so on, that's the big deal, that's the big deal with everything, why

23
00:02:37,280 --> 00:02:41,000
fear comes up, because you're afraid of failure, you're afraid of looking bad, you're

24
00:02:41,000 --> 00:02:43,120
afraid of people laughing at you.

25
00:02:43,120 --> 00:02:48,160
The biggest lesson I learned as a teacher was to stop being afraid of people laughing

26
00:02:48,160 --> 00:02:55,720
at me and criticizing me, it's such a wonderful thing to have people saying bad things

27
00:02:55,720 --> 00:03:06,920
about you, it's just so liberating, because there's only one answer to say, who cares?

28
00:03:06,920 --> 00:03:11,720
Someone calls you a buffalo, just turn around and see if you've got a tail, if you've

29
00:03:11,720 --> 00:03:17,320
got a tail, okay, you're a buffalo, if you don't, why are you excited?

30
00:03:17,320 --> 00:03:21,720
And that's really the deal, is many times the criticism is valid, people criticize you,

31
00:03:21,720 --> 00:03:26,360
and it's valid, it's like, wow, well, yes, that's the truth, and if it's not, it's

32
00:03:26,360 --> 00:03:30,840
not, but that's all it is, if you say something really stupid and you act like an idiot

33
00:03:30,840 --> 00:03:38,000
and the girl realizes, oh God, this guy is socially inept, okay, so you're socially inept,

34
00:03:38,000 --> 00:03:39,000
who cares?

35
00:03:39,000 --> 00:03:48,080
Really, that's the question, what is the problem with being incompetent, useless, meaningless?

36
00:03:48,080 --> 00:03:54,880
I think that's the most liberating thing is to realize that you have used this you are,

37
00:03:54,880 --> 00:04:01,960
to realize that you're, you have no value whatsoever, I didn't think of a talk on

38
00:04:01,960 --> 00:04:04,680
that one, I've certainly talked about it before, I think that's a wonderful

39
00:04:04,680 --> 00:04:09,760
thing, to realize that you have no value, it's go so contrary to what we're

40
00:04:09,760 --> 00:04:14,600
taught, modern society is you are valuable, you are special,

41
00:04:14,600 --> 00:04:24,760
but wasn't there a talk, someone posted a talk recently this guy gave an

42
00:04:24,760 --> 00:04:30,720
involuntary an address, where I don't remember what it was about or if it was even Buddhist,

43
00:04:30,720 --> 00:04:35,560
but there was one part that was kind of intriguing, he says, you're all taught that you're

44
00:04:35,560 --> 00:04:41,520
special, and I want to tell you you're not, and so the whole talk was about how you're

45
00:04:41,520 --> 00:04:50,120
not special, because if you're special then you've got a reputation to make to you, you've

46
00:04:50,120 --> 00:05:00,320
got something to live up to, something to cling to, you're not special, you're just a lump

47
00:05:00,320 --> 00:05:15,640
of flesh on a rock, big rock, you're a motor corner of the galaxy, mostly harmless, that's

48
00:05:15,640 --> 00:05:27,440
all we are, so when you can think like that, then you should have no trouble, when you

49
00:05:27,440 --> 00:05:32,280
have no expectations and no desires, that's where the best friendships come, when you have

50
00:05:32,280 --> 00:05:37,880
no desires and no expectations, even for friendship, where you give people the opportunity

51
00:05:37,880 --> 00:05:48,640
to benefit from your presence for you, give people your service and your help as they ask

52
00:05:48,640 --> 00:05:55,640
for it, and as their need becomes apparent, but don't look for anything more, and don't

53
00:05:55,640 --> 00:06:00,360
cling to them and don't pull them and don't try to keep them or hold them or anything

54
00:06:00,360 --> 00:06:13,360
that this is so much suffering and stress, Brahmua, I'm Brahmua, so he said something quite

55
00:06:13,360 --> 00:06:20,640
clever about this, he said, he asked a question, who is the most important person?

56
00:06:20,640 --> 00:06:27,080
And his answer was, you know, I think I disagree that actually the most important person

57
00:06:27,080 --> 00:06:33,920
is yourself, and if all of us, because the Buddha clearly said that you're to look after

58
00:06:33,920 --> 00:06:40,040
your own behavior, for example, do your own work, and that's what has been actually

59
00:06:40,040 --> 00:06:45,360
benefits others, but he certainly has a point when he says the most important person

60
00:06:45,360 --> 00:06:53,760
is whoever you're with, and it's an important point in the sense of not clinging to beings

61
00:06:53,760 --> 00:06:58,800
or people and saying, this is who I have to help, this is who I love, this is who I want

62
00:06:58,800 --> 00:07:04,520
to be with and so on, and be with whoever you're with, when you're with someone, be with

63
00:07:04,520 --> 00:07:12,720
them and you're with someone else, be with someone else, rather than cultivating friendships,

64
00:07:12,720 --> 00:07:18,560
cultivate friendship, there you go, there's a quote for you, rather than cultivating

65
00:07:18,560 --> 00:07:27,880
friendships, cultivate friendship, friendliness, be the kind of friend that you'd like to

66
00:07:27,880 --> 00:07:35,440
have, be the friend to everyone, and then you can't go wrong, I mean, and then this girl's

67
00:07:35,440 --> 00:07:40,080
staring is meaningless, like, well, if she wants to stare at me good for her, maybe that

68
00:07:40,080 --> 00:07:44,320
makes her happy, I don't know, or then you write us none of my business, but if she needs

69
00:07:44,320 --> 00:07:49,520
my help, I'm there to give it, if she doesn't, well, there certainly will be someone who

70
00:07:49,520 --> 00:08:02,320
does, but when you focus on this girl and lay the woman female being, it limits you, and

71
00:08:02,320 --> 00:08:07,280
it takes away your energies and it creates all sorts of stress and fear, which is not

72
00:08:07,280 --> 00:08:15,800
good for either of you, I would say go up and talk to her and and create a more, more

73
00:08:15,800 --> 00:08:25,280
wholesome behavior, there's someone staring at you, I mean, well, it's kind of a reason

74
00:08:25,280 --> 00:08:41,060
to start a conversation, but as a Buddhist monk, I'm bound to answer in terms of platonic

75
00:08:41,060 --> 00:08:54,140
relationship, it's sort of, they don't have anything to recommend in terms of romance, and

76
00:08:54,140 --> 00:09:03,540
much too much to, to sort of try to dissuade you from, much to speak against it, from experience

77
00:09:03,540 --> 00:09:10,540
and from practice and from theory, romance is really not where it's at.

