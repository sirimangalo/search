1
00:00:00,000 --> 00:00:05,000
I'm going to get our meditators to come and sit and listen.

2
00:00:05,000 --> 00:00:13,000
Hello, we're now live on YouTube, December 4th.

3
00:00:13,000 --> 00:00:19,000
We've just finished the demo part of video, so now the rest of this is going to be about

4
00:00:19,000 --> 00:00:22,000
lots of different things potentially.

5
00:00:22,000 --> 00:00:29,000
Maybe some questions, maybe some news, maybe some announcements of our own.

6
00:00:29,000 --> 00:00:33,000
But I was thinking, as I say, I have two meditators now.

7
00:00:33,000 --> 00:00:34,000
This is my big news.

8
00:00:34,000 --> 00:00:39,000
I have two international, we're now an international, officially international meditation center

9
00:00:39,000 --> 00:00:43,000
because we have two meditators from two different countries that are not Canada.

10
00:00:43,000 --> 00:00:48,000
We have one meditator from Austria and one meditator from Finland.

11
00:00:48,000 --> 00:00:50,000
Now that's impressive.

12
00:00:50,000 --> 00:00:53,000
That's great.

13
00:00:53,000 --> 00:00:56,000
So we need to go everyone to make this a reality.

14
00:00:56,000 --> 00:01:00,000
We're not using someone else's meditation center, someone else's monastery.

15
00:01:00,000 --> 00:01:02,000
We're not relying on them.

16
00:01:02,000 --> 00:01:05,000
We put this together as a community.

17
00:01:05,000 --> 00:01:08,000
And we now have two meditators.

18
00:01:08,000 --> 00:01:10,000
We have three acts, if you count.

19
00:01:10,000 --> 00:01:12,000
I don't know which we should.

20
00:01:12,000 --> 00:01:14,000
It's from Sri Lanka.

21
00:01:14,000 --> 00:01:16,000
So we had three different countries.

22
00:01:16,000 --> 00:01:22,000
On top of that, we had two visitors tonight, one from Sri Lanka, one from Canada.

23
00:01:22,000 --> 00:01:27,000
I think Sri Lanka, I think.

24
00:01:27,000 --> 00:01:30,000
Padamali and Sarah.

25
00:01:30,000 --> 00:01:33,000
Two friends.

26
00:01:33,000 --> 00:01:36,000
And they meditated with us.

27
00:01:36,000 --> 00:01:42,000
They listened to our chanting and then meditated in their life.

28
00:01:42,000 --> 00:01:46,000
But I'm going to make a point that Monday, Wednesday, Friday,

29
00:01:46,000 --> 00:01:49,000
they can come up and listen to them about it.

30
00:01:49,000 --> 00:01:50,000
It's useful.

31
00:01:50,000 --> 00:01:54,000
We had a dhamma talk, something light,

32
00:01:54,000 --> 00:01:58,000
to kind of relieve the burden of the heavy meditation.

33
00:01:58,000 --> 00:02:02,000
It's nice to have that light counterpoint.

34
00:02:02,000 --> 00:02:03,000
Some stories.

35
00:02:03,000 --> 00:02:07,000
I think for them to think about.

36
00:02:07,000 --> 00:02:08,000
I have to remember.

37
00:02:08,000 --> 00:02:11,000
Remind me, Robin, if I have to ask me next Monday,

38
00:02:11,000 --> 00:02:13,000
are the meditators there?

39
00:02:13,000 --> 00:02:21,000
The only one that one woman from Austria is leaving Monday.

40
00:02:21,000 --> 00:02:23,000
And he should come listen.

41
00:02:23,000 --> 00:02:27,000
The other one.

42
00:02:27,000 --> 00:02:33,000
So, do you have any announcements?

43
00:02:33,000 --> 00:02:37,000
You were talking about books.

44
00:02:37,000 --> 00:02:39,000
Booking is moving along?

45
00:02:39,000 --> 00:02:43,000
It is. Yes.

46
00:02:43,000 --> 00:02:47,000
The book project is moving along very well.

47
00:02:47,000 --> 00:02:51,000
We are up to $877, which is a lesson.

48
00:02:51,000 --> 00:02:54,000
That will make a nice gift for the children's home,

49
00:02:54,000 --> 00:02:57,000
because it far exceeds the cost of printing of the booklets.

50
00:02:57,000 --> 00:03:06,000
So, it's wonderful.

51
00:03:06,000 --> 00:03:23,000
Do you have a couple of questions?

52
00:03:23,000 --> 00:03:25,000
Okay, I'm ready.

53
00:03:25,000 --> 00:03:26,000
Okay.

54
00:03:26,000 --> 00:03:28,000
Sorry, just give me one minute.

55
00:03:28,000 --> 00:03:30,000
Sure.

56
00:03:30,000 --> 00:03:37,000
Hmm.

57
00:03:37,000 --> 00:03:55,000
It sometimes started, and it's better I don't.

58
00:03:55,000 --> 00:04:02,000
I don't do it now.

59
00:04:02,000 --> 00:04:11,000
Okay.

60
00:04:11,000 --> 00:04:13,000
That's going to take some time.

61
00:04:13,000 --> 00:04:14,000
I'll wait and do it later.

62
00:04:14,000 --> 00:04:16,000
Go for questions first.

63
00:04:16,000 --> 00:04:17,000
Okay.

64
00:04:17,000 --> 00:04:18,000
Hello, Bande.

65
00:04:18,000 --> 00:04:21,000
I have a question unrelated to meditation.

66
00:04:21,000 --> 00:04:24,000
You learned this practice in a part of the world that is tropical.

67
00:04:24,000 --> 00:04:27,000
Why would you move to a much colder environment?

68
00:04:27,000 --> 00:04:32,000
Would you be constrained to where the flimsy robe that are plenty in Sri Lanka?

69
00:04:32,000 --> 00:04:35,000
It seems to put an unnecessary hardship on the monk,

70
00:04:35,000 --> 00:04:41,000
and could in fact be lethal.

71
00:04:41,000 --> 00:04:44,000
Yeah, I mean, if you were to wear Sri Lankan robes,

72
00:04:44,000 --> 00:04:50,000
could potentially be lethal, those robes are like tissue paper.

73
00:04:50,000 --> 00:04:54,000
This robe, on the other hand, is quite thick, could be even thicker,

74
00:04:54,000 --> 00:04:56,000
could be made of wool.

75
00:04:56,000 --> 00:05:00,000
And in fact, I have another robe that is thicker and is made of wool,

76
00:05:00,000 --> 00:05:06,000
and keeps me perfectly warm, even in Canada winter.

77
00:05:06,000 --> 00:05:08,000
No, it's funny.

78
00:05:08,000 --> 00:05:14,000
I had this funny, funny sort of conversation with,

79
00:05:14,000 --> 00:05:17,000
remember I was telling you about this Chinese monk who came to visit

80
00:05:17,000 --> 00:05:20,000
the habit of this monastery in Toronto, good friend.

81
00:05:20,000 --> 00:05:21,000
Yes.

82
00:05:21,000 --> 00:05:26,000
It's funny how we get so stuck on our,

83
00:05:26,000 --> 00:05:30,000
it's kind of proud, you know?

84
00:05:30,000 --> 00:05:33,000
Thinking that, in the sense of thinking, we're doing it the right way,

85
00:05:33,000 --> 00:05:36,000
because he does things differently as a Mahayana monk.

86
00:05:36,000 --> 00:05:39,000
And it was kind of a bit of a turn off.

87
00:05:39,000 --> 00:05:44,000
Like, I kind of accept it because I've seen it so many times.

88
00:05:44,000 --> 00:05:49,000
But it started because he wears special sandals

89
00:05:49,000 --> 00:05:57,000
that actually have holes.

90
00:05:57,000 --> 00:06:01,000
So they don't go all around, but they cover the soles of the feet,

91
00:06:01,000 --> 00:06:03,000
and they have a heel.

92
00:06:03,000 --> 00:06:08,000
And, but they have slots in them.

93
00:06:08,000 --> 00:06:11,000
And I was looking at and I said, well, how do you avoid the snow?

94
00:06:11,000 --> 00:06:14,000
I'll get in through those. And he said, we can be very careful.

95
00:06:14,000 --> 00:06:19,000
And he said, but no, I said, so all of monks in your tradition

96
00:06:19,000 --> 00:06:20,000
have to wear those.

97
00:06:20,000 --> 00:06:22,000
And he said, yes, all you're around, that's all you can wear.

98
00:06:22,000 --> 00:06:24,000
Yeah, that's all we wear.

99
00:06:24,000 --> 00:06:35,000
And he said, he said the same thing about the robes.

100
00:06:35,000 --> 00:06:39,000
But his own idea of the robes was,

101
00:06:39,000 --> 00:06:41,000
like, it was the same kind of thing.

102
00:06:41,000 --> 00:06:43,000
Why do you have to wear that specific type of robe?

103
00:06:43,000 --> 00:06:46,000
Why don't you wear robes that are more like late people?

104
00:06:46,000 --> 00:06:49,000
And he didn't question like that, but he was implying.

105
00:06:49,000 --> 00:06:52,000
He said, well, we wear robes that are similar to the,

106
00:06:52,000 --> 00:06:56,000
to people of modern times in him, basically.

107
00:06:56,000 --> 00:06:59,000
But then I thought, well, what are you dumb sandals?

108
00:06:59,000 --> 00:07:03,000
You know, those sandals are the most impractical things I've ever seen.

109
00:07:03,000 --> 00:07:06,000
He may be watching this, but it doesn't really matter.

110
00:07:06,000 --> 00:07:09,000
We're good friends. I couldn't bring this up.

111
00:07:09,000 --> 00:07:11,000
But apologies if it's insulting.

112
00:07:11,000 --> 00:07:13,000
But still, he has to answer the hat.

113
00:07:13,000 --> 00:07:17,000
And I'm not afraid to say this to his face.

114
00:07:17,000 --> 00:07:20,000
It's just, you know, it can be a little bit

115
00:07:20,000 --> 00:07:22,000
tiresome to talk about this.

116
00:07:22,000 --> 00:07:25,000
But I think it's a fair criticism.

117
00:07:25,000 --> 00:07:32,000
Probably what my point is that there was in relation to your question.

118
00:07:32,000 --> 00:07:37,000
But the point is really this.

119
00:07:37,000 --> 00:07:44,000
This isn't a specific type of thing that,

120
00:07:44,000 --> 00:07:47,000
that monks in Sri Lanka wear because it's in Sri Lanka,

121
00:07:47,000 --> 00:07:49,000
because it's in the tropic.

122
00:07:49,000 --> 00:07:52,000
There is a very important non-biased,

123
00:07:52,000 --> 00:07:57,000
non-specific, non-culturally based reason for wearing robes.

124
00:07:57,000 --> 00:08:04,000
It's the most simple piece of cloth that you can wear.

125
00:08:04,000 --> 00:08:06,000
That's defensible, I think.

126
00:08:06,000 --> 00:08:08,000
You want to disagree with the importance of that.

127
00:08:08,000 --> 00:08:10,000
Well, fine. There's nothing to do with culture.

128
00:08:10,000 --> 00:08:13,000
It's because we're wearing two rectangles,

129
00:08:13,000 --> 00:08:15,000
three potentially, but they're perfect rectangles.

130
00:08:15,000 --> 00:08:17,000
There's nothing special.

131
00:08:17,000 --> 00:08:20,000
There's not allowed sleeves or not allowed buttons.

132
00:08:20,000 --> 00:08:24,000
The closest thing we have to button is something like a button to keep it.

133
00:08:24,000 --> 00:08:29,000
There is something like a button, but it's just pieces of thick thread,

134
00:08:29,000 --> 00:08:33,000
the string that make a button and a loop.

135
00:08:33,000 --> 00:08:35,000
But it's still just strength.

136
00:08:35,000 --> 00:08:40,000
And that's allowed because it helps keep it closed and so on.

137
00:08:40,000 --> 00:08:42,000
But that's it.

138
00:08:42,000 --> 00:08:46,000
And so that really keeps you humble.

139
00:08:46,000 --> 00:08:50,000
It keeps you focused.

140
00:08:50,000 --> 00:08:55,000
You know, that all you have is a rectangle, two rectangles.

141
00:08:55,000 --> 00:09:01,000
So it's much more about the shape than it is about the thickness or the practicality or so.

142
00:09:01,000 --> 00:09:04,000
Because this robe can be just as practical.

143
00:09:04,000 --> 00:09:09,000
I've been through the winter and Winnipeg wearing just these kind of robes.

144
00:09:09,000 --> 00:09:12,000
And people say, oh no, many monks have given up the robes.

145
00:09:12,000 --> 00:09:17,000
Even in the teravada tradition, they've started to even make up new ideas,

146
00:09:17,000 --> 00:09:21,000
new special uniforms and the Sri Lankans have their new special uniforms.

147
00:09:21,000 --> 00:09:23,000
They don't wear robes.

148
00:09:23,000 --> 00:09:25,000
They started to wear shirts.

149
00:09:25,000 --> 00:09:33,000
And even monks in Los Angeles have started wearing shirts in the summer.

150
00:09:33,000 --> 00:09:35,000
You know, what's up with that?

151
00:09:35,000 --> 00:09:38,000
It's become easier.

152
00:09:38,000 --> 00:09:42,000
And part of it, I think, is this desire to look like everyone else,

153
00:09:42,000 --> 00:09:47,000
which is not proper. We're not, we're different.

154
00:09:47,000 --> 00:09:51,000
We've taken on a vow to be externally different.

155
00:09:51,000 --> 00:09:52,000
And it's external.

156
00:09:52,000 --> 00:09:53,000
It's not internal.

157
00:09:53,000 --> 00:09:56,000
A monk isn't internally different from a lay person.

158
00:09:56,000 --> 00:09:57,000
They're still a person.

159
00:09:57,000 --> 00:09:58,000
They still have defilements.

160
00:09:58,000 --> 00:10:00,000
They still have issues that they have to do with.

161
00:10:00,000 --> 00:10:03,000
But they've taken on an external restraint, constrained.

162
00:10:03,000 --> 00:10:08,000
And so to give that up, you no longer a monk, you know.

163
00:10:08,000 --> 00:10:11,000
You no longer following this tradition.

164
00:10:11,000 --> 00:10:12,000
And it's external.

165
00:10:12,000 --> 00:10:14,000
It's not the most important thing.

166
00:10:14,000 --> 00:10:16,000
I've said many times, even monk is an important.

167
00:10:16,000 --> 00:10:19,000
But if you're going to do it, it's useful.

168
00:10:19,000 --> 00:10:24,000
The external life is an important system that the Buddha put in place.

169
00:10:24,000 --> 00:10:25,000
People say it's just concepts.

170
00:10:25,000 --> 00:10:30,000
Well, yeah, but it's the Buddha's concept of what a religious person should be like.

171
00:10:30,000 --> 00:10:32,000
They should wear this kind of cloth and so on.

172
00:10:32,000 --> 00:10:35,000
It should be limited to cloth.

173
00:10:35,000 --> 00:10:38,000
Someone asked me recently, why can't you wear black cloth?

174
00:10:38,000 --> 00:10:41,000
Because it would color cloths and say, well, we can't wear black, for example.

175
00:10:41,000 --> 00:10:42,000
Or she said, can you wear black?

176
00:10:42,000 --> 00:10:44,000
I said, no, why not?

177
00:10:44,000 --> 00:10:50,000
The answer is because black is a, it could be something of value.

178
00:10:50,000 --> 00:10:54,000
This kind of color, the color that we're supposed to be wearing is kind of pukey color.

179
00:10:54,000 --> 00:10:57,000
It's the color that no one would want to wear on purpose.

180
00:10:57,000 --> 00:11:03,000
Because you want something that's not going to make you think, oh, I have this wonderful, beautiful robe.

181
00:11:03,000 --> 00:11:07,000
We have to, when you first get the robe, you have to, you have to mark it.

182
00:11:07,000 --> 00:11:11,000
I had to mark this one, before I put it on, I had to mark it with a pen.

183
00:11:11,000 --> 00:11:15,000
Just as a symbol, and it's no longer pure.

184
00:11:15,000 --> 00:11:23,000
Keep in my mind the fact that, or to make it clear in my mind that this is a soiled piece of cloth.

185
00:11:23,000 --> 00:11:30,000
And so, there's many reasons to stick to the way that the Buddha had it.

186
00:11:30,000 --> 00:11:36,000
There's nothing to do with this country, that country, hot or cold, because it can be thick.

187
00:11:36,000 --> 00:11:43,000
And so, this argument that you can't wear these robes, because they're too thin, you have to switch to wearing coats, is ridiculous.

188
00:11:43,000 --> 00:11:50,000
Because I can put on my thick robe, and walk through a snowstorm, 20 minus 20.

189
00:11:50,000 --> 00:11:52,000
That minus 30, I was in, in Winnipeg.

190
00:11:52,000 --> 00:11:57,000
Walking down the street, it was cold, but so was everyone else.

191
00:11:57,000 --> 00:12:02,000
And, you know, perfectly livable.

192
00:12:02,000 --> 00:12:06,000
Not, not terribly comfortable, but that's normal.

193
00:12:06,000 --> 00:12:10,000
So, I own it, I mean, I don't know.

194
00:12:10,000 --> 00:12:14,000
It's not a meditation question, I apologize for going on about it, but.

195
00:12:14,000 --> 00:12:21,000
It's, it's important in that sense, from a, from, from a monastic point of view, looking at it as a monastic.

196
00:12:21,000 --> 00:12:23,000
My teachers commented on this.

197
00:12:23,000 --> 00:12:30,000
I remember there was a Mahayana monk came to visit us, who wore pants, and under his robe, he wore pants.

198
00:12:30,000 --> 00:12:38,000
And Ajahn Tang kind of looked at him and said, this is the, the end of the robe.

199
00:12:38,000 --> 00:12:46,000
The end of the robes means that we have a prophecy that eventually Buddhism is going, of course, to disappear from the world.

200
00:12:46,000 --> 00:12:49,000
And the last sign is when the robes disappear.

201
00:12:49,000 --> 00:12:53,000
When there's nobody, you know, living even the external life anymore.

202
00:12:53,000 --> 00:12:56,000
The internal life will disappear first.

203
00:12:56,000 --> 00:13:01,000
But the very final sign that Buddhism has disappeared is where people stop wearing robes.

204
00:13:01,000 --> 00:13:11,000
So it's a, it's a bad sign when, when the monks have already, even in the terra vada tradition have already sort of put aside the importance of the robes.

205
00:13:11,000 --> 00:13:13,000
They put aside the importance of the ball.

206
00:13:13,000 --> 00:13:16,000
There's much less going on arms round.

207
00:13:16,000 --> 00:13:21,000
In Sri Lanka, very few monks go on arms round, which is a shame.

208
00:13:21,000 --> 00:13:25,000
Especially when Sri Lankan people are so happy to give off.

209
00:13:25,000 --> 00:13:35,000
And we would get such, I mean, before people would give what they had, that people would give a piece of, you know, portion of their food.

210
00:13:35,000 --> 00:13:37,000
It's just quite impressive.

211
00:13:37,000 --> 00:13:45,000
It's a great spiritual practice to go through the village.

212
00:13:45,000 --> 00:13:52,000
Anyway, hope that answers your question.

213
00:13:52,000 --> 00:13:56,000
I would like to understand why it is improper to eat afternoon.

214
00:13:56,000 --> 00:13:58,000
Thank you so much.

215
00:13:58,000 --> 00:14:01,000
Well, it's not about afternoon per se.

216
00:14:01,000 --> 00:14:04,000
It's about eating enough just to get this to survive.

217
00:14:04,000 --> 00:14:07,000
So you could argue, well, why not?

218
00:14:07,000 --> 00:14:09,000
What's wrong with eating at 1pm?

219
00:14:09,000 --> 00:14:13,000
But I think the deal is, what do you need to survive?

220
00:14:13,000 --> 00:14:15,000
And what's the most important?

221
00:14:15,000 --> 00:14:17,000
What's essential and what's unessential?

222
00:14:17,000 --> 00:14:20,000
And so eating in the afternoon isn't really all that essential

223
00:14:20,000 --> 00:14:22,000
because you're not going to put it to much use.

224
00:14:22,000 --> 00:14:25,000
Eating in the morning, however, is essential

225
00:14:25,000 --> 00:14:29,000
because you need it to get you through the day through all the work that you have to do.

226
00:14:29,000 --> 00:14:32,000
I mean, even if that work is just meditating.

227
00:14:32,000 --> 00:14:35,000
It works actually quite perfectly.

228
00:14:35,000 --> 00:14:41,000
Especially when you have the Buddha's allowance to have juice in the afternoon.

229
00:14:41,000 --> 00:14:46,000
Pure, strange juice without any pulp or fruit in it.

230
00:14:46,000 --> 00:14:49,000
It's just enough.

231
00:14:49,000 --> 00:14:52,000
You have a good meal in the morning.

232
00:14:52,000 --> 00:14:55,000
And then in the afternoon, when you start to get a little tired,

233
00:14:55,000 --> 00:14:56,000
you have some juice.

234
00:14:56,000 --> 00:15:00,000
And the sugar in the juice perks you up just enough

235
00:15:00,000 --> 00:15:03,000
to continue through the afternoon and into the evening.

236
00:15:03,000 --> 00:15:06,000
And then tied you over until the morning.

237
00:15:10,000 --> 00:15:15,000
So it's just more about getting parsing it down to the bare minimum,

238
00:15:15,000 --> 00:15:18,000
so you don't have to worry about food, so you don't have to get involved

239
00:15:18,000 --> 00:15:21,000
with storing food and so on.

240
00:15:21,000 --> 00:15:23,000
Imagine if we ate in the evening,

241
00:15:23,000 --> 00:15:27,000
it wouldn't be about storing food or going out for food again.

242
00:15:27,000 --> 00:15:30,000
Really cut into your meditation practice.

243
00:15:36,000 --> 00:15:37,000
Hello, Bunte.

244
00:15:37,000 --> 00:15:40,000
In my home, my mom does not believe in Buddhism.

245
00:15:40,000 --> 00:15:45,000
I cannot practice sitting and walking meditation at all.

246
00:15:45,000 --> 00:15:54,000
Perhaps should I put it in my daily routine?

247
00:15:54,000 --> 00:15:56,000
I think they're asking what their daily routine should be

248
00:15:56,000 --> 00:16:00,000
if they're not allowed to practice sitting and walking meditation.

249
00:16:00,000 --> 00:16:05,000
You can't tell me you're not allowed to sit down.

250
00:16:05,000 --> 00:16:09,000
You could sit down and close your eyes, sit in a chair,

251
00:16:09,000 --> 00:16:12,000
in order to make it look like you're meditating.

252
00:16:12,000 --> 00:16:14,000
Maybe you can't do walking meditation.

253
00:16:14,000 --> 00:16:18,000
I'll just go for a walk outside or something.

254
00:16:18,000 --> 00:16:20,000
It's really quite a sad state,

255
00:16:20,000 --> 00:16:24,000
and it's something that you really probably want to work at.

256
00:16:24,000 --> 00:16:29,000
Send your mom loving kindness, send your mom good thoughts.

257
00:16:29,000 --> 00:16:32,000
May she be happy, may she be well, may she be free from suffering.

258
00:16:32,000 --> 00:16:34,000
Do that after your meditation.

259
00:16:34,000 --> 00:16:38,000
And absolutely just close your eyes and meditate.

260
00:16:38,000 --> 00:16:42,000
At the same time, yes, incorporate it into your daily life.

261
00:16:42,000 --> 00:16:45,000
But I also would recommend sending love to your mother,

262
00:16:45,000 --> 00:16:47,000
wishing her to be happy and free,

263
00:16:47,000 --> 00:16:52,000
because that'll break up that horrible, horrible mind state

264
00:16:52,000 --> 00:16:56,000
that wants to prevent you from meditating that sounds like.

265
00:16:56,000 --> 00:16:58,000
This is not important that she believes in it.

266
00:16:58,000 --> 00:17:02,000
The question is whether she allows you to do walking and sitting,

267
00:17:02,000 --> 00:17:17,000
which seems pretty innocuous to me.

268
00:17:17,000 --> 00:17:20,000
And with that, we are caught up on questions.

269
00:17:20,000 --> 00:17:24,000
And with that, it's 10pm, so we've done an hour.

270
00:17:24,000 --> 00:17:27,000
It's a good time to stop.

271
00:17:27,000 --> 00:17:29,000
Thanks, everyone.

272
00:17:29,000 --> 00:17:33,000
Thank you, Robin, for helping.

273
00:17:33,000 --> 00:17:34,000
Thank you, Bante.

274
00:17:34,000 --> 00:17:35,000
Have a good night.

275
00:17:35,000 --> 00:17:36,000
See you tomorrow.

276
00:17:36,000 --> 00:18:00,000
Good night.

