1
00:00:00,000 --> 00:00:04,340
Hi, and welcome back to our study of the Dhamapada.

2
00:00:04,340 --> 00:00:31,800
Today we continue on with verse number 83, which reads as follows.

3
00:00:31,800 --> 00:00:43,780
The Dhamapada way, Sapurisa, Jajanti, Sapurisa, good people, are always renowned, are everywhere.

4
00:00:43,780 --> 00:00:54,100
In every instance, on every occasion, renouncing, giving up, giving up.

5
00:00:54,100 --> 00:01:07,060
Nacama kama la playantisanto, they don't mutter or blather or chatter, as though they were

6
00:01:07,060 --> 00:01:15,260
nacama kamasanto, as though they were enamored by sensuality, in love with love or lusting after lust,

7
00:01:15,260 --> 00:01:22,380
lusting after lustful things, desires of the desire and diasirables.

8
00:01:22,380 --> 00:01:29,380
Sukayana puta at dawadukayna, when touched by either happiness or suffering, no jawajana

9
00:01:29,380 --> 00:01:38,660
bandita dasayanti, they don't exhibit the wise, don't exhibit highs or lows.

10
00:01:38,660 --> 00:01:44,540
So ordinary people when touched by Sukha or dukha, happiness or suffering, they are affected,

11
00:01:44,540 --> 00:01:54,620
they are elated, they rejoice about their good fortune, they become somehow dependent

12
00:01:54,620 --> 00:02:05,940
on it, they step into it and they come to expect it, they went touched by suffering,

13
00:02:05,940 --> 00:02:18,220
and then they lament and bemoaned their state. But asapurisaya's ever chaganti is ever

14
00:02:18,220 --> 00:02:29,380
renouncing, letting go. So that's the verse, in regards to a story about, it says 500

15
00:02:29,380 --> 00:02:43,820
banks and 500 beggars. So in the time of the Buddha, right after the Buddha was recently

16
00:02:43,820 --> 00:02:49,180
enlightened, he was staying in Wairanja, he lived there for the rains, he had been invited

17
00:02:49,180 --> 00:03:01,740
to stay in Wairanja, if you read the beginning of the vinaya, day in Akopanai, Pandasama yi

18
00:03:01,740 --> 00:03:12,420
in the Buddha, Bhakava, Wairanja, and we had a tea, the Buddha was dwelling in Wairanja.

19
00:03:12,420 --> 00:03:18,260
And this Brahman invited him to stay there and then forgot about him. And so the Buddha

20
00:03:18,260 --> 00:03:26,820
and his 500 monks dwelt in great hardship and they only were able to survive because

21
00:03:26,820 --> 00:03:33,460
of some horse traders who gave them horse food, gave them oats, basically, or something

22
00:03:33,460 --> 00:03:41,900
like oats, some kind of grain and they were able to live off this grain. But it was great

23
00:03:41,900 --> 00:03:46,780
hardship, so they went on arms but I guess didn't get much. And this Brahman who invited

24
00:03:46,780 --> 00:03:51,100
them to this area where they weren't able to get arms, totally forgot about them until

25
00:03:51,100 --> 00:03:59,740
the end of the rains. But it says here that there were a bunch of beggars living with them.

26
00:03:59,740 --> 00:04:29,580
By the kindness of the monks, 500 eaters of refuse lived within the monastery enclosure.

27
00:04:29,580 --> 00:04:38,940
Or it doesn't even say that, but what it does say is that the point is these beggars

28
00:04:38,940 --> 00:04:44,180
were before, so they weren't living. They weren't living with the Buddha and Wairanja.

29
00:04:44,180 --> 00:04:49,020
But in Wairanja the monks, these 500 monks were all at least so dependent, maybe all

30
00:04:49,020 --> 00:04:57,540
are hands. And they were all living with the Buddha and they were content with their

31
00:04:57,540 --> 00:05:06,300
poor fare. They were at peace, even in great hardship. Later on they came to great wealth

32
00:05:06,300 --> 00:05:11,340
and prosperity, not wealth in terms of money, but they were very well cared for. They

33
00:05:11,340 --> 00:05:17,820
moved to Sawati and they lived there. But they were still the same, they were still

34
00:05:17,820 --> 00:05:24,740
at peace. So whether they lived in hardship or

35
00:05:24,740 --> 00:05:30,420
whether they lived in great comfort, they were the same. And then you had these 500 beggars

36
00:05:30,420 --> 00:05:37,140
who appeared after there was great wealth and because of their own hardship decided

37
00:05:37,140 --> 00:05:42,780
that they would hang out at the monastery and so on, through the kindness of the monks,

38
00:05:42,780 --> 00:05:51,820
these 500 beggars sort of lived off the leftover food because everyone was always bringing

39
00:05:51,820 --> 00:05:56,780
food to the monks and coming to hear the monks at teach and supporting the monks in

40
00:05:56,780 --> 00:06:02,380
different ways. And so there was lots of leftovers and so these beggars would come.

41
00:06:02,380 --> 00:06:06,940
And it says they would eat the choice food left over by the monks and then lie down

42
00:06:06,940 --> 00:06:11,700
and sleep. And then when they got up from their sleep, they would be full of energy and

43
00:06:11,700 --> 00:06:17,500
they would go and wrestle. They would shout and jump and wrestle and play, misbehaved both

44
00:06:17,500 --> 00:06:24,060
within and without the monastery. They did nothing but misbehaved. And so the monks

45
00:06:24,060 --> 00:06:34,940
were discussing this and they noted the difference because ostensibly a part of the

46
00:06:34,940 --> 00:06:44,500
problem for these beggars was the opulence. Once they got great food, they got strength

47
00:06:44,500 --> 00:06:52,260
and energy and they just started acting, rumunctious. Whereas the monks, whether they were

48
00:06:52,260 --> 00:07:05,780
in hardship or in opulence or affluence, were unchanged. And so for that reason, he

49
00:07:05,780 --> 00:07:10,860
told a jotic historian about horses, which is actually quite interesting because it points

50
00:07:10,860 --> 00:07:18,940
out that even, it's a story about some donkeys and horses. And these thoroughbred horses

51
00:07:18,940 --> 00:07:26,740
were fed, it was an accident or something, they were fed wine of some sort or some kind

52
00:07:26,740 --> 00:07:34,860
of alcohol and were unaffected by it. But these donkeys who were hanging out with the

53
00:07:34,860 --> 00:07:43,820
thoroughbred horses drank the same wine and started praying and carrying on the point

54
00:07:43,820 --> 00:07:48,420
being that the alcohol, even alcohol, even though we rail against it as being a terrible

55
00:07:48,420 --> 00:07:56,380
terrible thing, all it has the power to do is remove your inhibitions. So if you don't

56
00:07:56,380 --> 00:08:03,420
have any harmful intentions in your mind, it doesn't actually hurt you. Unfortunately,

57
00:08:03,420 --> 00:08:08,740
that for most of us, that includes delusion, it includes ego and so on. So if you have

58
00:08:08,740 --> 00:08:19,860
none of that and you're fine, but anyway, he makes the point that this is a difference

59
00:08:19,860 --> 00:08:26,380
between people, therefore an ordinary person. It's actually an interesting point because

60
00:08:26,380 --> 00:08:31,540
for ordinary people, it's sometimes the other way around. Sometimes when things are fine,

61
00:08:31,540 --> 00:08:36,060
people can be quite moral and ethical, right? When you don't need to steal, when you don't

62
00:08:36,060 --> 00:08:43,300
need to kill, when there's no adversity, when there's no danger. But when the going gets

63
00:08:43,300 --> 00:08:53,820
tough, many people will abandon their ethics, abandon their goodness, their generosity,

64
00:08:53,820 --> 00:09:07,340
or rather people that stay the way around in hardship, they are forced to behave themselves.

65
00:09:07,340 --> 00:09:12,860
They have to act in such a way that other people respect them and so on. But when things

66
00:09:12,860 --> 00:09:21,540
are good, they get lazy, right? Or for monastics, this is very much the case. When things

67
00:09:21,540 --> 00:09:27,460
are rough, you know, you're forced to be mindful because there's lots of physical pain

68
00:09:27,460 --> 00:09:33,300
and physical discomfort and hunger and thirst and heat and cold that you can escape. But

69
00:09:33,300 --> 00:09:38,860
when you have the pleasure and the luxury that it's very easy to get lazy, it's very easy

70
00:09:38,860 --> 00:09:44,380
to become indulgent and so on. It's actually quite an impressive thing, impressive feat

71
00:09:44,380 --> 00:09:50,380
that these monks having been in such hardship, going to such opulence and not being affected

72
00:09:50,380 --> 00:09:57,540
by it. But the point still stands that it works both ways and it seems more to me about

73
00:09:57,540 --> 00:10:05,180
change than anything. That after a while you become sort of stable in your situation and

74
00:10:05,180 --> 00:10:11,460
if things don't change, you become, you may become complacent thinking that everything

75
00:10:11,460 --> 00:10:20,340
is okay, only when things change. You see what you couldn't see because of the stability

76
00:10:20,340 --> 00:10:28,220
and the part of it points out the nature of impermanence or the importance of impermanence,

77
00:10:28,220 --> 00:10:38,980
the harm and the danger of change to one psyche. And also the usefulness of impermanence

78
00:10:38,980 --> 00:10:46,340
for that very reason when things change, you're able to better see your defilement when

79
00:10:46,340 --> 00:10:50,660
they have the rug pulled out from under you. When everything's going stable, whether

80
00:10:50,660 --> 00:10:56,140
it's difficult, if it's difficult, you plot along, if it's good, then you feel calm

81
00:10:56,140 --> 00:11:02,540
and at peace. But when things change, you can be quite upset. The person who is living

82
00:11:02,540 --> 00:11:10,580
in opulence, if they fall into hardship, would have a very hard time coping mentally

83
00:11:10,580 --> 00:11:20,020
with it. So it goes both ways and this is continuing this theme in this verse of the, we've

84
00:11:20,020 --> 00:11:27,140
seen this recently quite a bit. We talk about how wise people touched by their happiness

85
00:11:27,140 --> 00:11:31,940
or unhappiness, happiness or suffering show no change.

86
00:11:31,940 --> 00:11:52,020
So the experience, no highs or lows. Which is interesting, people often think of that

87
00:11:52,020 --> 00:11:58,980
as being a terribly dull and an interesting state. Certainly not something to be striped

88
00:11:58,980 --> 00:12:08,020
for, the highs and the lows or the spice of life they would say. It gives life meaning,

89
00:12:08,020 --> 00:12:16,900
I guess, it gives meaning to your happiness. But it's actually quite the opposite. We find

90
00:12:16,900 --> 00:12:21,260
people who are stuck unhappiness and suffering that these are the ones who are like zombies.

91
00:12:21,260 --> 00:12:33,180
They can be very depressed and colorless in their constant aversion to suffering or

92
00:12:33,180 --> 00:12:41,660
in their constant obsession with happiness. A zombie is someone who is constantly seeking

93
00:12:41,660 --> 00:12:46,900
out pleasure. It's by seeking out pleasure that you become this zombie that consumes

94
00:12:46,900 --> 00:12:56,620
and consumes. Or you're a zombie working, working just to survive in great hardship. Just

95
00:12:56,620 --> 00:13:02,420
to cope with suffering. The person who has had great loss becomes a zombie. A person who

96
00:13:02,420 --> 00:13:08,020
exhibits neither highs or lows is actually quite a piece, has quite a calm and uplifted

97
00:13:08,020 --> 00:13:16,340
mind, a light mind, a mind that is flexible, a mind that is kind and compassionate because

98
00:13:16,340 --> 00:13:24,420
they don't have, they aren't caught up in their own emotions. So they're able to approach

99
00:13:24,420 --> 00:13:33,540
life with great zest and in finger and clarity and goodness as well. So their intentions

100
00:13:33,540 --> 00:13:40,500
to help others and that kind of thing are pronounced. Their happiness and their peace are

101
00:13:40,500 --> 00:13:47,780
much pronounced. So they're anything but a zombie. The zombie is the one who has caught

102
00:13:47,780 --> 00:13:58,820
up in legs and dislikes. They become tired and they get into this trance-like state, zombie-like

103
00:13:58,820 --> 00:14:23,780
state of consuming pleasure and coping with suffering. It's a warning for all of us not

104
00:14:23,780 --> 00:14:29,300
to become complacent. It's easy to think that you're moral and ethical. If you don't look

105
00:14:29,300 --> 00:14:34,100
closely, it's easy to think, well I'm not a bad person, I'm living fine. But so much of

106
00:14:34,100 --> 00:14:38,980
that is often dependent on our pleasure. When you start to meditate, you start to see,

107
00:14:38,980 --> 00:14:45,740
actually I'm quite intoxicated by this pleasure. And many of the things that I thought were

108
00:14:45,740 --> 00:14:53,060
harmless are actually building me up to take a fall because when I'm not able to indulge

109
00:14:53,060 --> 00:14:58,020
in central pleasures anymore, I'm going to get angry and upset and frustrated. It's what

110
00:14:58,020 --> 00:15:05,780
leads us to fight and quarrel and manipulate others are indulgence and sensuality.

111
00:15:13,540 --> 00:15:22,820
It's really the crux of it. Our reactions to pleasant experiences and our reactions to

112
00:15:22,820 --> 00:15:30,500
unpleasant experiences is about sums up all of the problems that we have or the problems of the

113
00:15:30,500 --> 00:15:36,900
mind that we're working to overcome. We have these two. There's a third one that's not really

114
00:15:36,900 --> 00:15:43,460
explicitly mentioned here and that's a delusion. We consider these to be the three evils

115
00:15:43,460 --> 00:15:51,220
in the mind, the three things that lead us to suffer. But they're not equal. Happiness and

116
00:15:51,220 --> 00:15:57,300
unhappiness lead to greed and anger, liking and disliking. Our reactions to these things,

117
00:15:58,020 --> 00:16:03,220
these are, you could say, the surface problem, but the underlying problem. The third one is the

118
00:16:03,220 --> 00:16:10,660
underlying problem. Our delusion is how we approach these things. The reason we get angry about

119
00:16:10,660 --> 00:16:16,100
unpleasant things, the reason we become attached to pleasant things, is because of our delusion,

120
00:16:16,100 --> 00:16:22,660
because of our ignorance, our perception, that this one's going to make me happy. This one's

121
00:16:23,220 --> 00:16:29,380
something that I, the way to be happy is to avoid it. If I chase after this one, I'll be happy,

122
00:16:29,380 --> 00:16:35,460
if I push this one away, I'll also be happy. Not realizing that more we push this one away,

123
00:16:35,460 --> 00:16:40,980
more unhappy will become. The more we cling to this one, the more needy we will become and so

124
00:16:40,980 --> 00:16:54,660
the more unhappy we will become. And shameful we will become. If you ask yourself, who would you

125
00:16:54,660 --> 00:17:03,140
rather have as a friend, who would you rather associate with? Someone who wants a lot, or someone

126
00:17:03,140 --> 00:17:10,900
who doesn't, who has great wanting, or someone who has little wanting, who has great desires,

127
00:17:10,900 --> 00:17:20,180
strong desires, or someone who has simple desires, or is free from desire. I don't know, some

128
00:17:20,180 --> 00:17:25,860
people might say they like people who have great desires, but they think about it when people

129
00:17:25,860 --> 00:17:31,460
have great desires, then they end up wanting a lot from you. Friends who have great desires can

130
00:17:31,460 --> 00:17:39,380
be great burdens. Friends who are hard to please, or friends who have great people who have great

131
00:17:39,380 --> 00:17:50,500
aversion to suffering. So this is the sort of situation we have here where part of the implication

132
00:17:50,500 --> 00:17:58,980
here is that these guys are acting improper or acting in an unsuitable way of having

133
00:17:58,980 --> 00:18:06,340
eaten as this food that was meant for the monks and living in close quarters with these spiritual

134
00:18:06,340 --> 00:18:12,740
people. They're acting like well like ordinary individuals. You get that a lot in monasteries when

135
00:18:12,740 --> 00:18:19,780
they get big in Thailand. You can see this a lot where the workers and the monasteries

136
00:18:19,780 --> 00:18:30,500
are not so very moral. Someone, I don't know, I was talking, talking to one of the workers about

137
00:18:30,500 --> 00:18:37,060
cockroaches and she, I think she pulled out a can of cockroach spray and was trying to hand it

138
00:18:37,060 --> 00:18:49,380
to me. And yeah, I remember going, we went on this parade once. The monks, we were up in this boat,

139
00:18:49,380 --> 00:18:53,860
they had us up on this float with a boat. It was like a boat and people were putting food in the

140
00:18:53,860 --> 00:19:00,180
boat. It was the means of supporting, I don't know actually what it was, what the food was going

141
00:19:00,180 --> 00:19:07,540
to be for, but it was a parade with monks, which was kind of nice. But then the board of directors

142
00:19:07,540 --> 00:19:12,660
for the monastery was surrounding it and going on with us and then they started pulling, they

143
00:19:12,660 --> 00:19:19,460
pulled out alcohol and started drinking whiskey. Well, you know, all around us, they were holding

144
00:19:19,460 --> 00:19:31,940
onto the edges of the boat and riding the float. It's a shame really, that that sort of thing

145
00:19:31,940 --> 00:19:41,940
that living so close to such goodness and such spiritual teachings that they don't bother to put

146
00:19:41,940 --> 00:19:47,700
them into practice. And part of it is this problem of having things too good when things are good,

147
00:19:48,340 --> 00:19:55,060
people may tend to misbehave. It's part of the reason why monks are encouraged to go off

148
00:19:55,060 --> 00:20:05,140
into the forest and live in a simplest, simple accommodations to live more or less in hardship

149
00:20:05,140 --> 00:20:10,900
because it does, it can keep you honest. It forces you to be mindful.

150
00:20:13,540 --> 00:20:22,420
In terms of the monastic life, hardship is quite often important for spiritual development

151
00:20:22,420 --> 00:20:29,060
because it's very easy to get lazy as we can see here. Anyway, so a simple teaching,

152
00:20:29,060 --> 00:20:38,580
the verse itself has some interesting points. The idea that what makes a good person is their

153
00:20:38,580 --> 00:20:45,540
practice of renunciation everywhere, whether they're happy or unhappy. And they never blather on

154
00:20:45,540 --> 00:20:52,420
lupianity. It's an interesting word. They've chatter. The implication is that people who are

155
00:20:52,420 --> 00:20:59,940
intoxicated with sensuality will blather on about it. Something we should catch ourselves

156
00:20:59,940 --> 00:21:05,860
in talking about food or clothing or music or art or this kind of thing.

157
00:21:10,900 --> 00:21:18,980
But one who is wise, when touched by other happiness or by sorrow, is that peace,

158
00:21:18,980 --> 00:21:24,100
is content. Their happiness, their peace, their contentment is not dependent.

159
00:21:26,340 --> 00:21:30,020
Anamisasoka is a happiness that is not based on an object.

160
00:21:31,780 --> 00:21:37,940
Doesn't require this or that. It isn't affected. It isn't disturbed.

161
00:21:40,420 --> 00:21:44,500
It isn't disturbed and it isn't dependent on external sense and external

162
00:21:44,500 --> 00:21:53,220
experiences or phenomena. This is why meditation is so important. It's important to understand

163
00:21:53,220 --> 00:22:00,740
change and to become familiar with change and to see that change is a part of life so that

164
00:22:01,700 --> 00:22:09,460
changes don't affect us, so that our happiness can be our peace of mind. It can be undisturbed by

165
00:22:09,460 --> 00:22:17,300
change. Anyway, that's the teaching of this demo by the verse. That's all for now. Thank you for

166
00:22:17,300 --> 00:22:47,140
tuning in. Keep practicing and be...

