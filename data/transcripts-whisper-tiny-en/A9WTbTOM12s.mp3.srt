1
00:00:00,000 --> 00:00:09,000
Hi, so today I'll be explaining a third technique in the meditation practice and this is called Mindful Prostration.

2
00:00:09,000 --> 00:00:17,000
Now, Prostration is something which is well familiar to many Buddhists around the world and many religious people around the world.

3
00:00:17,000 --> 00:00:25,000
Some people will use it, for instance, in Thailand, to pay respect to one's parents or pay respect to one's teachers.

4
00:00:25,000 --> 00:00:34,000
In some religions, they will use it as a means of paying respect or worshiping a God or an angel or some higher divinity.

5
00:00:34,000 --> 00:00:40,000
In the meditation practice, it is a means of paying respect to the meditation practice.

6
00:00:40,000 --> 00:00:51,000
So it's done before the walking and before the sitting, and it's done simply to create a state of respect and humility

7
00:00:51,000 --> 00:00:57,000
and gratitude towards the meditation practice as something which is a value to us.

8
00:00:57,000 --> 00:01:07,000
It's also a means of starting the mind on the right track or creating mindfulness in the mind before we actually go ahead to do the walking and then do the sitting.

9
00:01:07,000 --> 00:01:12,000
So I'll now demonstrate how to do what is called a Mindful Prostration.

10
00:01:12,000 --> 00:01:22,000
First of all, I'll show how to do the Thai method of Prostration which is used to pay respect to one's parents and one's teachers.

11
00:01:22,000 --> 00:01:37,000
And I'll show this because it will be a framework, a basis, a model for the Mindful Prostration so you can see what it should look like.

12
00:01:37,000 --> 00:01:42,000
So in Thailand, Prostration is done with one sitting on one's knees.

13
00:01:42,000 --> 00:01:54,000
The traditional way is to set up on one's toes, but if this is uncomfortable, you can sit down on the tops of your feet.

14
00:01:54,000 --> 00:02:03,000
And an ordinary Prostration is performed with the hands coming up to the chest, then up to the forehead between the eyebrows.

15
00:02:03,000 --> 00:02:12,000
And then the hands go down and they will come out in a W pattern with the thumbs touching and they will touch the floor.

16
00:02:12,000 --> 00:02:21,000
And they have to be far out enough so that the elbows can come to rest in front of the knees, not on top of the knees or to the side.

17
00:02:21,000 --> 00:02:32,000
When the elbows are touching in front of the knees and the hands are on the floor, the forehead goes down and touches the thumbs between the eyebrows.

18
00:02:32,000 --> 00:02:37,000
And back up again to the chest.

19
00:02:37,000 --> 00:02:42,000
And this is done three times as follows.

20
00:02:42,000 --> 00:03:04,000
Now the Mindful Prostration is similar and is based on the Thai Prostration.

21
00:03:04,000 --> 00:03:09,000
But in this case, it's a meditation practice as well, so it's done slowly.

22
00:03:09,000 --> 00:03:13,000
And it's with the Mind focused on this time on the hand.

23
00:03:13,000 --> 00:03:18,000
So when the hand turns, one creates a clear thought, turning.

24
00:03:18,000 --> 00:03:24,000
And one says it to oneself three times, turning, turning, turning, turning.

25
00:03:24,000 --> 00:03:30,000
When the hand starts to raise, one says raising, raising, raising three times.

26
00:03:30,000 --> 00:03:33,000
And it's in time with the motion.

27
00:03:33,000 --> 00:03:39,000
So again, since we're practicing based on what's really happening, we have to do it as the movement occurs.

28
00:03:39,000 --> 00:03:44,000
We can't say to ourselves first raising, raising, raising, and then raise the hand.

29
00:03:44,000 --> 00:03:47,000
Or raise first and then say, raising, raising, raising.

30
00:03:47,000 --> 00:03:50,000
It has to be done at the same time as the movement.

31
00:03:50,000 --> 00:04:01,000
So starting again, turning, turning, turning, raising, raising, raising, touching, touching, touching,

32
00:04:01,000 --> 00:04:06,000
turning, turning, turning, raising, raising, raising.

33
00:04:06,000 --> 00:04:15,000
Touching, touching, touching, touching, raising, raising, raising, touching, touching, touching,

34
00:04:15,000 --> 00:04:21,800
loring, loring, loring, touching, touching, bending, vending,

35
00:04:51,800 --> 00:04:54,020
So on, in this point on it repeats itself.

36
00:04:54,020 --> 00:04:57,620
So turning, turning, turning, raising, raising,

37
00:04:57,620 --> 00:04:59,800
raising, touching, touching, touching,

38
00:04:59,800 --> 00:05:05,340
turning, turning, turning, raising, raising, raising,

39
00:05:05,340 --> 00:05:08,980
touching, touching, touching, and up again to the forehead,

40
00:05:08,980 --> 00:05:10,900
down, and so on.

41
00:05:10,900 --> 00:05:12,780
It repeats this process three times.

42
00:05:15,580 --> 00:05:19,500
Slowly, slowly each time, taking the time to say turning,

43
00:05:19,500 --> 00:05:22,500
turning, turning, raising, raising, raising, and so on.

44
00:05:27,780 --> 00:05:29,100
Okay?

45
00:05:29,100 --> 00:05:30,660
Once one has done this three times,

46
00:05:30,660 --> 00:05:34,060
and one finishes turning, turning, turning, as usual.

47
00:05:34,060 --> 00:05:37,940
Raising, raising, raising, touching, touching, and so on.

48
00:05:37,940 --> 00:05:40,580
After the third time, one has done frustration.

49
00:05:40,580 --> 00:05:43,340
One comes back up to this point and finishes off,

50
00:05:43,340 --> 00:05:48,460
raising, raising, raising, raising, touching, touching, touching,

51
00:05:48,460 --> 00:05:52,960
lowering, lowering, lowering, touching, touching, touching,

52
00:05:52,960 --> 00:05:55,640
and instead of bending, bending, bending again,

53
00:05:55,640 --> 00:05:59,460
one brings one's hand back to one's thighs, lowering,

54
00:05:59,460 --> 00:06:04,300
lowering, lowering, touching, touching, touching, covering,

55
00:06:04,300 --> 00:06:07,620
covering, covering, lowering, lowering,

56
00:06:07,620 --> 00:06:10,780
lowering, touching, touching, touching,

57
00:06:10,780 --> 00:06:13,760
covering, covering, covering.

58
00:06:13,760 --> 00:06:15,180
And one one has finished this,

59
00:06:15,180 --> 00:06:17,840
one then continues on with the walking meditation,

60
00:06:17,840 --> 00:06:21,340
walking right goes, that's left goes, that's right goes, that's to the end,

61
00:06:21,340 --> 00:06:25,640
and back and forth, and when we'll generally do walking for 10 minutes or 15

62
00:06:25,640 --> 00:06:30,840
minutes, depending on the one's energy and one's willpower, after doing the

63
00:06:30,840 --> 00:06:34,340
walking meditation, when we'll do the sitting meditation, and it's important

64
00:06:34,340 --> 00:06:38,940
that these three meditation techniques are done in sequence without any

65
00:06:38,940 --> 00:06:43,140
break in between. I'll talk more about that in my next video, but for now,

66
00:06:43,140 --> 00:06:46,940
this is a brief demonstration on how to do what is called mindful

67
00:06:46,940 --> 00:06:50,940
frustration, or a way of paying respect to the meditation practice, and setting

68
00:06:50,940 --> 00:06:54,340
up one's mindfulness for the walking and the sitting meditation, which

69
00:06:54,340 --> 00:07:01,340
follow. Thanks for tuning in, and that's all for today.

