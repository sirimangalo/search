1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Dhamapanda.

2
00:00:05,000 --> 00:00:16,000
Today we continue with verses 246 to 248, which read as follows.

3
00:00:16,000 --> 00:00:26,000
This is the Dhamapanda.

4
00:00:26,000 --> 00:00:36,000
This is the Dhamapanda.

5
00:00:36,000 --> 00:00:46,000
This is the Dhamapanda.

6
00:00:46,000 --> 00:00:57,000
This is the Dhamapanda.

7
00:00:57,000 --> 00:01:22,000
This is the Dhamapanda.

8
00:01:22,000 --> 00:01:43,000
Whoever engages in killing, engages in taking life, engages in false speech.

9
00:01:43,000 --> 00:01:50,000
Whoever engages in this world and taking what isn't given,

10
00:01:50,000 --> 00:01:56,000
to someone else's spouse.

11
00:01:56,000 --> 00:02:09,000
Whoever engages in drinking alcohol and taking intoxicants.

12
00:02:09,000 --> 00:02:19,000
In this very world, such a person pulls themselves up by the root.

13
00:02:19,000 --> 00:02:23,000
Know this.

14
00:02:23,000 --> 00:02:29,000
Know this, oh man.

15
00:02:29,000 --> 00:02:44,000
One who is unrestrained, will become an evil nature.

16
00:02:44,000 --> 00:03:06,000
Don't let greed and unrighteousness evil lead you to suffering for a long time.

17
00:03:06,000 --> 00:03:15,000
These verses were taught in response to a group of lay disciples of the Buddha,

18
00:03:15,000 --> 00:03:21,000
or people who called themselves lay disciples, meaning not monks.

19
00:03:21,000 --> 00:03:28,000
They were just people living their lives, trying to follow the Buddha's teaching as best they could,

20
00:03:28,000 --> 00:03:38,000
but they took up a curious practice of keeping one or another of the precepts.

21
00:03:38,000 --> 00:03:41,000
Maybe some of them kept two or three, I don't know.

22
00:03:41,000 --> 00:03:46,000
Out of the five basic precepts that I just mentioned in the verse,

23
00:03:46,000 --> 00:03:50,000
they would generally keep one of them.

24
00:03:50,000 --> 00:03:58,000
And they argued one day about whose precept was harder to keep, telling each other,

25
00:03:58,000 --> 00:04:03,000
oh I keep, I don't ever take life, but that's a really hard precept,

26
00:04:03,000 --> 00:04:07,000
and I'm proud of that because that one's a hard one to keep.

27
00:04:07,000 --> 00:04:10,000
Another one would say, oh, it's nothing.

28
00:04:10,000 --> 00:04:15,000
No matter what, we'll never steal anything from anyone else.

29
00:04:15,000 --> 00:04:22,000
That's the hardest one to keep. You're wrong.

30
00:04:22,000 --> 00:04:28,000
Sexual misconduct, not committing adultery, not breaking up other people's relationships.

31
00:04:28,000 --> 00:04:33,000
That one's the hardest because when you want something, you just take it.

32
00:04:33,000 --> 00:04:37,000
That one's the hardest, I'm very proud of that.

33
00:04:37,000 --> 00:04:44,000
I assume they were all proud, that's why they argued.

34
00:04:44,000 --> 00:04:49,000
And then another one, of course, would say,

35
00:04:49,000 --> 00:04:54,000
lying, not telling false speech, that one's the hardest for sure.

36
00:04:54,000 --> 00:04:59,000
But for many people, of course, even the fifth precept is the hardest.

37
00:04:59,000 --> 00:05:03,000
Not taking any alcohol, not taking any intoxicants.

38
00:05:03,000 --> 00:05:10,000
It's difficult, both in terms of having to then deal with

39
00:05:10,000 --> 00:05:17,000
social pressure and anxiety and depression and so on.

40
00:05:17,000 --> 00:05:22,000
And also just because of the pressure of society,

41
00:05:22,000 --> 00:05:28,000
the pressure to, like everyone else, engage in such things,

42
00:05:28,000 --> 00:05:36,000
in social situations and just as a general part of life.

43
00:05:36,000 --> 00:05:41,000
So they went to the Buddha and they brought this question to the Buddha.

44
00:05:41,000 --> 00:05:43,000
I said, look, we can't decide.

45
00:05:43,000 --> 00:05:51,000
We're wondering, venerable sir, which precept is hardest to keep?

46
00:05:51,000 --> 00:05:54,000
And the Buddha, that wasn't having any of it.

47
00:05:54,000 --> 00:05:57,000
He refused to say this one is more difficult,

48
00:05:57,000 --> 00:05:58,000
or that one is more difficult.

49
00:05:58,000 --> 00:06:03,000
He said, instead, all the precepts are hard to keep.

50
00:06:03,000 --> 00:06:12,000
And then he taught this verse, these verses.

51
00:06:12,000 --> 00:06:18,000
So the basic lesson, I think,

52
00:06:18,000 --> 00:06:26,000
is that not just that all precepts are hard to keep.

53
00:06:26,000 --> 00:06:29,000
But more importantly, that keeping one or another of the precepts

54
00:06:29,000 --> 00:06:34,000
is never going to cut it, it's never going to suffice.

55
00:06:34,000 --> 00:06:38,000
They're a group of five precepts for a reason.

56
00:06:38,000 --> 00:06:43,000
And it's not some magical group whereby just keeping them

57
00:06:43,000 --> 00:06:48,000
is somehow a magical thing that is going to solve all your problems

58
00:06:48,000 --> 00:06:58,000
or lead you to enlightenment or be enough to lead to spiritual development.

59
00:06:58,000 --> 00:07:02,000
But I'll say two things.

60
00:07:02,000 --> 00:07:10,000
First of all, that indeed keeping one or another of the precepts

61
00:07:10,000 --> 00:07:16,000
is absolutely a positive act.

62
00:07:16,000 --> 00:07:19,000
And first of all, let's take another step back

63
00:07:19,000 --> 00:07:23,000
and talk about what it means to keep a precept.

64
00:07:23,000 --> 00:07:29,000
Now, keeping a precept is not simply not doing something

65
00:07:29,000 --> 00:07:32,000
because technically a person can go their whole life

66
00:07:32,000 --> 00:07:37,000
without being confronted with the opportunity to do something.

67
00:07:37,000 --> 00:07:43,000
And that's a good thing, but it's not keeping the precepts.

68
00:07:43,000 --> 00:07:48,000
Keeping a precept, keeping a rule means when the opportunity arises,

69
00:07:48,000 --> 00:07:50,000
not engaging it in it.

70
00:07:50,000 --> 00:07:53,000
And it's a good thing that you never in the position

71
00:07:53,000 --> 00:07:56,000
because then you never have to deal with that psychological mind state

72
00:07:56,000 --> 00:07:58,000
of having to deal with it.

73
00:07:58,000 --> 00:08:03,000
But it isn't technically anything

74
00:08:03,000 --> 00:08:06,000
because it doesn't say anything about your state of mind.

75
00:08:06,000 --> 00:08:11,000
An evil person can keep a precept just as well as a pure person

76
00:08:11,000 --> 00:08:14,000
provided they're not given the opportunity.

77
00:08:14,000 --> 00:08:18,000
But a person who keeps a precept

78
00:08:18,000 --> 00:08:21,000
when the opportunity to kill doesn't come up,

79
00:08:21,000 --> 00:08:24,000
they stay in from killing.

80
00:08:24,000 --> 00:08:28,000
There's a goodness in that for whatever reason they do it.

81
00:08:28,000 --> 00:08:33,000
Of course, meaning it's better than the alternative of actually engaging

82
00:08:33,000 --> 00:08:35,000
and breaking the precept.

83
00:08:35,000 --> 00:08:38,000
So these people who kept one or another

84
00:08:38,000 --> 00:08:42,000
even just the intention to keep that one precept

85
00:08:42,000 --> 00:08:44,000
is a wholesome thing.

86
00:08:44,000 --> 00:08:50,000
The problem, and the problem especially with the five precepts,

87
00:08:50,000 --> 00:08:56,000
is that any one of them is enough to corrupt the mind

88
00:08:56,000 --> 00:09:02,000
to a sufficient degree that you really have a hard time

89
00:09:02,000 --> 00:09:05,000
maintaining a basic state of humanity.

90
00:09:05,000 --> 00:09:12,000
There are all five of them real and true obstacles

91
00:09:12,000 --> 00:09:19,000
to spiritual development, to purity of mind, to clarity of mind.

92
00:09:19,000 --> 00:09:23,000
They're the five precepts for a reason.

93
00:09:23,000 --> 00:09:26,000
And they don't encompass everything.

94
00:09:26,000 --> 00:09:30,000
Not every evil deed is included in them.

95
00:09:30,000 --> 00:09:31,000
They're not commandments.

96
00:09:31,000 --> 00:09:34,000
They're not the things that would have required people to do.

97
00:09:34,000 --> 00:09:39,000
But they're the five things that can be singled out

98
00:09:39,000 --> 00:09:48,000
as being sufficiently harmful to have serious repercussions

99
00:09:48,000 --> 00:09:53,000
on one's spiritual state of mind.

100
00:09:53,000 --> 00:09:56,000
So someone who keeps all five of the precepts

101
00:09:56,000 --> 00:10:00,000
is going to be an order of magnitude better off

102
00:10:00,000 --> 00:10:03,000
than someone who keeps one, two, three, or four.

103
00:10:03,000 --> 00:10:07,000
It's just hard to really praise someone who doesn't keep

104
00:10:07,000 --> 00:10:10,000
some of them and not all of them.

105
00:10:10,000 --> 00:10:13,000
It's really not praise worthy at all.

106
00:10:13,000 --> 00:10:15,000
You can't say, oh, well, four out of five,

107
00:10:15,000 --> 00:10:16,000
that's not bad.

108
00:10:16,000 --> 00:10:17,000
80% right.

109
00:10:17,000 --> 00:10:19,000
It doesn't work like that.

110
00:10:19,000 --> 00:10:24,000
It really is all or nothing.

111
00:10:24,000 --> 00:10:26,000
90% it is.

112
00:10:26,000 --> 00:10:29,000
You can say maybe if you keep four of them,

113
00:10:29,000 --> 00:10:31,000
you get 10% credit or something.

114
00:10:31,000 --> 00:10:34,000
A very little credit, but some credit.

115
00:10:34,000 --> 00:10:39,000
Because they're that important.

116
00:10:39,000 --> 00:10:44,000
This story might seem a little bit odd

117
00:10:44,000 --> 00:10:49,000
or maybe even far-fetched, strange anyway.

118
00:10:49,000 --> 00:10:52,000
But it actually is the sort of thing that comes up

119
00:10:52,000 --> 00:10:53,000
in Buddhist circles.

120
00:10:53,000 --> 00:10:58,000
I've seen people talk about being able to keep four precepts

121
00:10:58,000 --> 00:11:02,000
and there's in fact this movement to change

122
00:11:02,000 --> 00:11:05,000
the way people request the precepts.

123
00:11:05,000 --> 00:11:08,000
So in some Buddhist circles now,

124
00:11:08,000 --> 00:11:11,000
for a long time actually,

125
00:11:11,000 --> 00:11:15,000
the requesting the five precepts from a monk,

126
00:11:15,000 --> 00:11:20,000
which is a formal ceremony of asking to

127
00:11:20,000 --> 00:11:23,000
recite them for you,

128
00:11:23,000 --> 00:11:26,000
or do this repeat after me sort of thing

129
00:11:26,000 --> 00:11:30,000
where you affirm them in the presence of the monk.

130
00:11:30,000 --> 00:11:33,000
But instead of asking for all five of them as a group,

131
00:11:33,000 --> 00:11:35,000
you ask for them,

132
00:11:35,000 --> 00:11:44,000
we assume we sooner kind of die that I may keep them individually.

133
00:11:44,000 --> 00:11:48,000
You specify that because you know you're not going to keep all five of them.

134
00:11:48,000 --> 00:11:50,000
So the idea is if I break one,

135
00:11:50,000 --> 00:11:52,000
well, I'm still keeping the others.

136
00:11:52,000 --> 00:11:55,000
And then the idea is it leaves you free to choose,

137
00:11:55,000 --> 00:11:57,000
which ones you want to keep.

138
00:11:57,000 --> 00:12:01,000
Of course, anyone is free to choose.

139
00:12:01,000 --> 00:12:03,000
In Buddhism we don't have commandments,

140
00:12:03,000 --> 00:12:05,000
not for lay people.

141
00:12:09,000 --> 00:12:11,000
But there's really very little benefit

142
00:12:11,000 --> 00:12:13,000
in keeping some of the five precepts

143
00:12:13,000 --> 00:12:16,000
that really doesn't work that way.

144
00:12:16,000 --> 00:12:18,000
Furthermore,

145
00:12:18,000 --> 00:12:21,000
the simple psychological benefit

146
00:12:21,000 --> 00:12:24,000
of keeping one of the precepts is valuable.

147
00:12:24,000 --> 00:12:28,000
You know, someone who holds that up not killing.

148
00:12:28,000 --> 00:12:30,000
I hold this up as a valuable thing.

149
00:12:30,000 --> 00:12:33,000
That's pure, that's noble.

150
00:12:36,000 --> 00:12:40,000
But more noble than that, of course,

151
00:12:40,000 --> 00:12:44,000
if someone who holds the five precepts up as a noble thing,

152
00:12:44,000 --> 00:12:47,000
someone who sees not just these five things,

153
00:12:47,000 --> 00:12:50,000
but the principle behind them,

154
00:12:50,000 --> 00:12:52,000
the principle of basic humanity,

155
00:12:52,000 --> 00:12:55,000
these five things and anything else like them,

156
00:12:55,000 --> 00:12:58,000
I take as a basis of ethics and morality.

157
00:12:58,000 --> 00:13:01,000
That's a very pure intention.

158
00:13:01,000 --> 00:13:06,000
So even when you don't have the opportunity to break them,

159
00:13:06,000 --> 00:13:08,000
I think it is valid to say

160
00:13:08,000 --> 00:13:11,000
that someone who undertakes to keep them

161
00:13:11,000 --> 00:13:13,000
has done a great thing.

162
00:13:13,000 --> 00:13:15,000
They have made a great start,

163
00:13:15,000 --> 00:13:17,000
cultivated a great foundation.

164
00:13:17,000 --> 00:13:20,000
That's how the Buddha talks about morality.

165
00:13:20,000 --> 00:13:23,000
But they tie standing upon ethics.

166
00:13:23,000 --> 00:13:26,000
And here in this verse he talks about the opposite.

167
00:13:26,000 --> 00:13:29,000
If you don't keep ethics, what is the result?

168
00:13:29,000 --> 00:13:31,000
You uproot yourself.

169
00:13:31,000 --> 00:13:33,000
So ethics are the root of the tree.

170
00:13:33,000 --> 00:13:36,000
There what everything is founded on.

171
00:13:36,000 --> 00:13:42,000
Because think about it when you're engaged

172
00:13:42,000 --> 00:13:44,000
in any unethical behavior,

173
00:13:44,000 --> 00:13:47,000
everything in your life becomes shaky.

174
00:13:47,000 --> 00:13:50,000
There's an instability in your mind,

175
00:13:50,000 --> 00:13:53,000
in your livelihood,

176
00:13:53,000 --> 00:13:55,000
in your social relationships.

177
00:13:55,000 --> 00:13:58,000
There's an element of corruption,

178
00:13:58,000 --> 00:14:00,000
of degradation.

179
00:14:00,000 --> 00:14:02,000
And conversely,

180
00:14:02,000 --> 00:14:05,000
for someone who does have a solid ethical framework,

181
00:14:05,000 --> 00:14:08,000
everything in your life becomes solid.

182
00:14:08,000 --> 00:14:10,000
No matter what attacks might come

183
00:14:10,000 --> 00:14:12,000
or problems or conflicts might come,

184
00:14:12,000 --> 00:14:14,000
you have a solid,

185
00:14:14,000 --> 00:14:16,000
what they say moral compass,

186
00:14:16,000 --> 00:14:18,000
more than that you have a solid foundation.

187
00:14:18,000 --> 00:14:21,000
You know where you stand, right?

188
00:14:21,000 --> 00:14:24,000
You have integrity.

189
00:14:24,000 --> 00:14:27,000
And that integrity runs deep,

190
00:14:27,000 --> 00:14:29,000
depending how deep you take the precepts,

191
00:14:29,000 --> 00:14:33,000
meaning how deep you take them in your mind.

192
00:14:33,000 --> 00:14:38,000
It becomes an integrity on a meditative level,

193
00:14:38,000 --> 00:14:42,000
which is where this verse really is important for us.

194
00:14:42,000 --> 00:14:44,000
That's meditators.

195
00:14:44,000 --> 00:14:47,000
There's an integrity of our state of mind,

196
00:14:47,000 --> 00:14:49,000
meaning our mind is not wavering,

197
00:14:49,000 --> 00:14:51,000
it's not feeling guilty,

198
00:14:51,000 --> 00:14:56,000
it's not feeling manipulative

199
00:14:56,000 --> 00:14:59,000
or crooked in any way.

200
00:14:59,000 --> 00:15:01,000
There's an integrity or rectitude,

201
00:15:01,000 --> 00:15:06,000
a righteousness in the mind.

202
00:15:06,000 --> 00:15:09,000
So how we should understand this verse

203
00:15:09,000 --> 00:15:13,000
or think of this verse in terms of a meditative practice.

204
00:15:13,000 --> 00:15:17,000
We have to think of the precepts as going deeper

205
00:15:17,000 --> 00:15:19,000
or of ethics as going deeper

206
00:15:19,000 --> 00:15:21,000
than just the precepts.

207
00:15:21,000 --> 00:15:24,000
When the Buddha talks about a foundation

208
00:15:24,000 --> 00:15:27,000
on a worldly level,

209
00:15:27,000 --> 00:15:28,000
it's easy to understand.

210
00:15:28,000 --> 00:15:30,000
It's useful to understand

211
00:15:30,000 --> 00:15:33,000
the precepts as leading to

212
00:15:33,000 --> 00:15:37,000
an integrity of mind,

213
00:15:37,000 --> 00:15:39,000
a peace of mind,

214
00:15:39,000 --> 00:15:41,000
and the Buddha said joy,

215
00:15:41,000 --> 00:15:46,000
happiness that comes from keeping them.

216
00:15:46,000 --> 00:15:50,000
Normally in the world,

217
00:15:50,000 --> 00:15:54,000
there's this idea that breaking the precepts,

218
00:15:54,000 --> 00:15:57,000
breaking rules, breaking ethics

219
00:15:57,000 --> 00:16:01,000
is quite often a source of happiness.

220
00:16:01,000 --> 00:16:04,000
We don't steal because we know the problems,

221
00:16:04,000 --> 00:16:06,000
but if you did steal it,

222
00:16:06,000 --> 00:16:07,000
you'd get what you want,

223
00:16:07,000 --> 00:16:09,000
and that would make you happy.

224
00:16:09,000 --> 00:16:11,000
We often think of ethics as being something

225
00:16:11,000 --> 00:16:12,000
that makes you unhappy,

226
00:16:12,000 --> 00:16:14,000
and we think, well,

227
00:16:14,000 --> 00:16:16,000
maybe it's better to be unhappy and ethical,

228
00:16:16,000 --> 00:16:18,000
but it's actually not true.

229
00:16:18,000 --> 00:16:20,000
True ethics,

230
00:16:20,000 --> 00:16:26,000
someone who has a profound and strong practice

231
00:16:26,000 --> 00:16:28,000
in ethics, will find great joy,

232
00:16:28,000 --> 00:16:31,000
and great peace of mind.

233
00:16:31,000 --> 00:16:33,000
And that joy,

234
00:16:33,000 --> 00:16:35,000
that peace of mind leads to stability,

235
00:16:35,000 --> 00:16:37,000
leads to concentration,

236
00:16:37,000 --> 00:16:40,000
and that concentration leads to understanding,

237
00:16:40,000 --> 00:16:42,000
so you understand yourself better,

238
00:16:42,000 --> 00:16:43,000
you understand people better.

239
00:16:43,000 --> 00:16:45,000
You're able to see things not in terms of us,

240
00:16:45,000 --> 00:16:46,000
this is them,

241
00:16:46,000 --> 00:16:48,000
but I have these issues,

242
00:16:48,000 --> 00:16:49,000
and they have those issues,

243
00:16:49,000 --> 00:16:52,000
and you see things much more as they are,

244
00:16:52,000 --> 00:16:54,000
as opposed to what can I get out of this?

245
00:16:54,000 --> 00:16:56,000
What am I losing from this?

246
00:16:56,000 --> 00:16:59,000
A lot of immorality,

247
00:16:59,000 --> 00:17:04,000
cultivates when you're engaging in theft,

248
00:17:04,000 --> 00:17:05,000
and manipulation, and killing,

249
00:17:05,000 --> 00:17:06,000
and harming others,

250
00:17:06,000 --> 00:17:08,000
and so on,

251
00:17:08,000 --> 00:17:11,000
unethical behavior changes your perspective.

252
00:17:11,000 --> 00:17:13,000
Once you're more ethical,

253
00:17:13,000 --> 00:17:15,000
you don't have a horse in the race,

254
00:17:15,000 --> 00:17:16,000
as they say.

255
00:17:16,000 --> 00:17:20,000
Nothing is really personal for you.

256
00:17:20,000 --> 00:17:21,000
You're able to see things on.

257
00:17:21,000 --> 00:17:24,000
This person is doing this because of this and that.

258
00:17:24,000 --> 00:17:26,000
It's a very good way to cultivate,

259
00:17:26,000 --> 00:17:29,000
focus, concentration, and wisdom,

260
00:17:29,000 --> 00:17:32,000
but that's really only on a worldly level.

261
00:17:32,000 --> 00:17:35,000
So understanding ethics from a Buddhist perspective

262
00:17:35,000 --> 00:17:37,000
has to go deeper.

263
00:17:37,000 --> 00:17:38,000
The Buddha said,

264
00:17:38,000 --> 00:17:42,000
bhapadhamma asanyatta in the third verse,

265
00:17:42,000 --> 00:17:45,000
and that means one who is unrestrained

266
00:17:45,000 --> 00:17:47,000
is of an evil nature.

267
00:17:47,000 --> 00:17:49,000
The ethics, ethics,

268
00:17:49,000 --> 00:17:51,000
the ethical precepts and ethics in general

269
00:17:51,000 --> 00:17:53,000
is about restraint,

270
00:17:53,000 --> 00:17:55,000
and so as I said about restraint

271
00:17:55,000 --> 00:17:57,000
when you want to do something,

272
00:17:57,000 --> 00:17:59,000
and you decide not to do it,

273
00:17:59,000 --> 00:18:00,000
or want to say something,

274
00:18:00,000 --> 00:18:01,000
and you decide not to say it,

275
00:18:01,000 --> 00:18:03,000
but more importantly,

276
00:18:03,000 --> 00:18:10,000
it's a restraint of action as a experience,

277
00:18:10,000 --> 00:18:13,000
meaning every moment

278
00:18:13,000 --> 00:18:17,000
where we move the body,

279
00:18:17,000 --> 00:18:21,000
every moment where we engage in speech,

280
00:18:21,000 --> 00:18:24,000
every moment of our experience

281
00:18:24,000 --> 00:18:27,000
has the potential to have an ethical quality to it.

282
00:18:27,000 --> 00:18:29,000
Everything we see

283
00:18:29,000 --> 00:18:32,000
has the potential to have an ethical component.

284
00:18:32,000 --> 00:18:35,000
Where we cultivate something unethical

285
00:18:35,000 --> 00:18:36,000
or something ethical,

286
00:18:36,000 --> 00:18:39,000
where we can go either way,

287
00:18:39,000 --> 00:18:40,000
depending on our habit,

288
00:18:40,000 --> 00:18:43,000
depending on our interaction with it.

289
00:18:43,000 --> 00:18:48,000
And so when we do walking meditation,

290
00:18:48,000 --> 00:18:51,000
each step has an ethical quality to it,

291
00:18:51,000 --> 00:18:54,000
and the practice of walking meditation is a training,

292
00:18:54,000 --> 00:18:56,000
a cultivation,

293
00:18:56,000 --> 00:19:01,000
an attempt to evoke ethical states

294
00:19:01,000 --> 00:19:05,000
or ethical action in each step,

295
00:19:05,000 --> 00:19:09,000
where each step becomes an ethical action,

296
00:19:09,000 --> 00:19:11,000
and of course as a result,

297
00:19:11,000 --> 00:19:15,000
our life and everything we do begins to take on

298
00:19:15,000 --> 00:19:18,000
a more ethical quality to it.

299
00:19:18,000 --> 00:19:20,000
It goes on that deep of a level,

300
00:19:20,000 --> 00:19:23,000
so that when it comes to things that are so coarse,

301
00:19:23,000 --> 00:19:26,000
like the five preset like killing,

302
00:19:26,000 --> 00:19:29,000
the idea of the act of killing

303
00:19:29,000 --> 00:19:31,000
could never arise because you're so much deeper

304
00:19:31,000 --> 00:19:34,000
and so much more granular than that.

305
00:19:34,000 --> 00:19:38,000
Your mind is so pure at every moment

306
00:19:38,000 --> 00:19:41,000
that there could never arise the conclusion

307
00:19:41,000 --> 00:19:42,000
I should harm someone.

308
00:19:42,000 --> 00:19:44,000
I should steal something.

309
00:19:44,000 --> 00:19:49,000
And that kind of ethics

310
00:19:49,000 --> 00:19:54,000
leads to a deeper state of focus or concentration,

311
00:19:54,000 --> 00:19:57,000
then simply a peaceful or a calm state of mind.

312
00:19:57,000 --> 00:20:01,000
It leads to a clear focus.

313
00:20:01,000 --> 00:20:03,000
I like to use the word focus more than concentration

314
00:20:03,000 --> 00:20:06,000
because I think it gets the point across better.

315
00:20:06,000 --> 00:20:08,000
Concentration, you can see as a state

316
00:20:08,000 --> 00:20:10,000
where the mind is just quiet

317
00:20:10,000 --> 00:20:12,000
or the mind is undisturbed.

318
00:20:12,000 --> 00:20:15,000
It can be like this helmet that you wear or this arm

319
00:20:15,000 --> 00:20:18,000
or that you put on.

320
00:20:18,000 --> 00:20:21,000
But focus is something actually very different, right?

321
00:20:21,000 --> 00:20:23,000
If you're out of focus,

322
00:20:23,000 --> 00:20:25,000
you don't see things clearly.

323
00:20:25,000 --> 00:20:27,000
You make mistakes.

324
00:20:27,000 --> 00:20:29,000
It's like being in the dark

325
00:20:29,000 --> 00:20:31,000
when you turn off the lights and you can dimly see

326
00:20:31,000 --> 00:20:33,000
or not even see

327
00:20:33,000 --> 00:20:36,000
you can cause a lot of problems that way.

328
00:20:36,000 --> 00:20:38,000
But when you focus,

329
00:20:38,000 --> 00:20:40,000
the blurriness fades

330
00:20:40,000 --> 00:20:43,000
and the things that were always there.

331
00:20:43,000 --> 00:20:44,000
You don't see anything new,

332
00:20:44,000 --> 00:20:46,000
but the things that were already there

333
00:20:46,000 --> 00:20:49,000
that you are coming contact with every day

334
00:20:49,000 --> 00:20:53,000
suddenly become clear to you.

335
00:20:53,000 --> 00:20:56,000
Like when I was 13,

336
00:20:56,000 --> 00:20:58,000
we suddenly realized

337
00:20:58,000 --> 00:21:01,000
that everyone was seeing things that I couldn't see.

338
00:21:01,000 --> 00:21:04,000
And then I've got glasses and oh,

339
00:21:04,000 --> 00:21:07,000
the world doesn't look like I thought it did.

340
00:21:07,000 --> 00:21:12,000
When you cultivate meditation,

341
00:21:12,000 --> 00:21:16,000
mindfulness, this is what comes from it.

342
00:21:16,000 --> 00:21:19,000
This is the concentration of every moment

343
00:21:19,000 --> 00:21:21,000
walking the movement of the foot

344
00:21:21,000 --> 00:21:23,000
which seemed like a very ordinary thing.

345
00:21:23,000 --> 00:21:25,000
The rising and falling of the stomach

346
00:21:25,000 --> 00:21:27,000
which seemed so banal that,

347
00:21:27,000 --> 00:21:28,000
of course, I know what rising is.

348
00:21:28,000 --> 00:21:32,000
Why am I looking at this thing that's so familiar to me?

349
00:21:32,000 --> 00:21:36,000
You realize how unfamiliar your own body is to you.

350
00:21:36,000 --> 00:21:39,000
Not to mention the mind.

351
00:21:39,000 --> 00:21:41,000
And you start to see things about the mind

352
00:21:41,000 --> 00:21:44,000
that were always there.

353
00:21:44,000 --> 00:21:45,000
Always there.

354
00:21:45,000 --> 00:21:47,000
They weren't hiding from you.

355
00:21:47,000 --> 00:21:49,000
You just didn't have the vision

356
00:21:49,000 --> 00:21:51,000
to focus to see them.

357
00:21:51,000 --> 00:21:53,000
And that's the real wisdom that comes.

358
00:21:53,000 --> 00:21:55,000
It's not the wisdom about yourself

359
00:21:55,000 --> 00:21:56,000
or someone else.

360
00:21:56,000 --> 00:21:58,000
It's a wisdom about reality

361
00:21:58,000 --> 00:22:01,000
but moments of experience.

362
00:22:02,000 --> 00:22:06,000
So ethics isn't really about keeping the precepts.

363
00:22:06,000 --> 00:22:09,000
When someone talks about keeping only four of the precepts

364
00:22:09,000 --> 00:22:12,000
for a meditator that's just absurd.

365
00:22:12,000 --> 00:22:14,000
Because these five things are so course

366
00:22:14,000 --> 00:22:16,000
that anyone who breaks any one of them

367
00:22:16,000 --> 00:22:18,000
you can immediately cut them out

368
00:22:18,000 --> 00:22:22,000
of the people who have any potential to go deeper.

369
00:22:22,000 --> 00:22:24,000
You're drinking alcohol.

370
00:22:24,000 --> 00:22:26,000
It really doesn't matter what else you do.

371
00:22:26,000 --> 00:22:27,000
You're not going anywhere.

372
00:22:27,000 --> 00:22:29,000
You're killing, you're stealing.

373
00:22:29,000 --> 00:22:31,000
Meditation isn't really possible for you

374
00:22:31,000 --> 00:22:33,000
until you stop doing that.

375
00:22:33,000 --> 00:22:37,000
They're just so course that it's absurd to think

376
00:22:37,000 --> 00:22:41,000
that you can decide to keep some of them.

377
00:22:41,000 --> 00:22:43,000
If you're not keeping the five precepts,

378
00:22:43,000 --> 00:22:45,000
there's so much more you have to do that

379
00:22:45,000 --> 00:22:47,000
you don't even have a hope.

380
00:22:47,000 --> 00:22:48,000
You do have a hope.

381
00:22:48,000 --> 00:22:50,000
But your hope is in keeping them.

382
00:22:50,000 --> 00:22:52,000
They're not arbitrary.

383
00:22:52,000 --> 00:22:56,000
They're not in some religions

384
00:22:56,000 --> 00:22:58,000
think of the precepts that you have to keep.

385
00:22:58,000 --> 00:23:00,000
You can't eat certain things.

386
00:23:00,000 --> 00:23:02,000
You have to wear certain things.

387
00:23:02,000 --> 00:23:04,000
You can't wear certain things.

388
00:23:04,000 --> 00:23:11,000
You can't do certain things on certain days, for example.

389
00:23:11,000 --> 00:23:14,000
There's none of that really in Buddhism.

390
00:23:14,000 --> 00:23:17,000
I mean that's certainly nothing like that in the five precepts

391
00:23:17,000 --> 00:23:19,000
or even the eight precepts.

392
00:23:19,000 --> 00:23:24,000
They're specifically designed or related to the

393
00:23:24,000 --> 00:23:27,000
practice of meditation, the practice of the cultivation

394
00:23:27,000 --> 00:23:29,000
of mental development.

395
00:23:29,000 --> 00:23:34,000
For the purpose, ultimately, of wisdom on a

396
00:23:34,000 --> 00:23:38,000
momentary level to see impermanent suffering

397
00:23:38,000 --> 00:23:44,000
and non-salve to see about our realities, those things

398
00:23:44,000 --> 00:23:48,000
that we cling to, that none of them and nothing in the world

399
00:23:48,000 --> 00:23:50,000
is worth clinging to.

400
00:23:50,000 --> 00:23:54,000
The things that we cling to are actually not worth it,

401
00:23:54,000 --> 00:23:57,000
which has the power of course then to lead us to freedom from

402
00:23:57,000 --> 00:24:03,000
suffering, which of course is the goal of everything.

403
00:24:03,000 --> 00:24:05,000
So that's the Dhammapada for tonight.

404
00:24:05,000 --> 00:24:06,000
Thank you all for listening.

405
00:24:06,000 --> 00:24:09,000
I wish you all the best.

