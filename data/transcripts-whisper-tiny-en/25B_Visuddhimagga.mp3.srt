1
00:00:00,000 --> 00:00:16,120
Now, all duration means great change, and here the sentence should read whether they are

2
00:00:16,120 --> 00:00:24,600
unclung to or clung to, they are the great alterations.

3
00:00:24,600 --> 00:00:46,360
That means whether they belong to things or whether they belong to beings, living beings,

4
00:00:46,360 --> 00:00:53,680
all the same, they are great alterations, they are great changes.

5
00:00:53,680 --> 00:00:58,920
They are in the great alteration of the unclung to, that means outside things, evidence

6
00:00:58,920 --> 00:01:02,960
as itself in the emergence of an Ian.

7
00:01:02,960 --> 00:01:15,200
Now, this word appears in another context also, and he made the same translation, but

8
00:01:15,200 --> 00:01:40,080
on page 329, just close to the bottom, three lines from the bottom, what did you see there?

9
00:01:40,080 --> 00:01:50,800
Page 329, some medical terms resembling the fire that ushers in the end of an Ian.

10
00:01:50,800 --> 00:01:55,880
So here also, it should be the end of an Ian, not emergence.

11
00:01:55,880 --> 00:02:02,400
The body word here is deceptive.

12
00:02:02,400 --> 00:02:09,040
So when the world at the time of the destruction of the world, you see this change

13
00:02:09,040 --> 00:02:13,840
very, very clearly, everything is destroyed.

14
00:02:13,840 --> 00:02:25,960
So the great alteration of the unclung to in an organic, called lifeless things, evidence

15
00:02:25,960 --> 00:02:33,640
as itself in the end of an Ian, and that of the clung to in the disturbance of the elements

16
00:02:33,640 --> 00:02:41,960
in the body, that means we believe that these four elements should be well-balanced, if

17
00:02:41,960 --> 00:02:53,400
there is too much of the element or what element, and you get the seeds and you get sick.

18
00:02:53,400 --> 00:03:03,520
So there is a native medical practice, which make the elements their pieces.

19
00:03:03,520 --> 00:03:07,200
And they always explain with reference to these elements.

20
00:03:07,200 --> 00:03:14,200
So if you eat food with your sticky, and it will cause constipation and so on, and this food

21
00:03:14,200 --> 00:03:30,120
has an element from an property, and if you eat something which is bitter, you will get

22
00:03:30,120 --> 00:03:34,880
a lot of wind elements, so it promotes wind elements.

23
00:03:34,880 --> 00:03:42,080
And so they base on these four primary elements, and they divide food into, say, those

24
00:03:42,080 --> 00:03:49,640
which have earth elements as prominent property or water element as prominent property,

25
00:03:49,640 --> 00:03:59,360
and so they treat their patients by giving them food appropriate for the disease.

26
00:03:59,360 --> 00:04:05,720
They always say, food is medicine and medicine is food.

27
00:04:05,720 --> 00:04:10,840
Now for accordingly, the conflagration is playing first, and so on, this is the explanation

28
00:04:10,840 --> 00:04:18,560
of the disfucking of the wall, and we'll come to that leader too, in the next chapter.

29
00:04:18,560 --> 00:04:31,720
And then there are four verses that describe the alteration in the article, living beings.

30
00:04:31,720 --> 00:04:38,920
The bite of wooden moths can make the body stiff, to all intents when raw is its element.

31
00:04:38,920 --> 00:04:41,120
It might be gripped by such a snake.

32
00:04:41,120 --> 00:04:45,880
So they believe that there are snakes.

33
00:04:45,880 --> 00:04:53,800
The bites of which can cause your body to become stiffens, and also there are some kind

34
00:04:53,800 --> 00:05:04,200
of snakes which can make your body like water elements and so on.

35
00:05:04,200 --> 00:05:09,120
So when bitten by such a snake, there is disturbance of elements in your body, and so your

36
00:05:09,120 --> 00:05:11,640
body alters.

37
00:05:11,640 --> 00:05:18,720
So they are great primaries because they have become the basis of great alterations.

38
00:05:18,720 --> 00:05:25,240
And then the next one is because they are great and because they are entities, great.

39
00:05:25,240 --> 00:05:30,680
Because they need great effort to descend them and entities are put up because they are

40
00:05:30,680 --> 00:05:31,680
existent.

41
00:05:31,680 --> 00:05:37,200
That's they are great primaries because they are great and because they are entities.

42
00:05:37,200 --> 00:05:43,120
And then again they are elements because of bearing their own characteristics, because

43
00:05:43,120 --> 00:05:49,640
of cresting suffering and because of sorting out or because of producing suffering.

44
00:05:49,640 --> 00:05:54,080
And because none of them are exempt from the characteristic of being elements.

45
00:05:54,080 --> 00:05:59,000
And they are states, they are called states, Tamas, going to bear in their own characteristics.

46
00:05:59,000 --> 00:06:07,800
And going to the so bearing for the length of the moment appropriate to them.

47
00:06:07,800 --> 00:06:16,720
The common sub-comradery explains here that the word darana is not bearing, but just as existing.

48
00:06:16,720 --> 00:06:27,560
Existing with their own characteristics, I mean existing for the length of the moment

49
00:06:27,560 --> 00:06:29,520
appropriate to them.

50
00:06:29,520 --> 00:06:37,200
That means data is called in element and Rubai is also called in element.

51
00:06:37,200 --> 00:06:44,680
Data has only one moment exist at one moment's life, but Rubai has 17 moments life.

52
00:06:44,680 --> 00:06:51,480
So that is why they said according to their own characteristic, I mean for the length

53
00:06:51,480 --> 00:06:55,240
of the moment appropriate to them.

54
00:06:55,240 --> 00:07:02,720
So if it is, if it is a ticker or G to the zigga, then it has only one moment's life, one

55
00:07:02,720 --> 00:07:03,720
moment's existence.

56
00:07:03,720 --> 00:07:10,920
If it is Rubai, it has 17 moments life, they are impermanent in the sense of liability

57
00:07:10,920 --> 00:07:14,920
due to destruction and so on, so they are not difficult.

58
00:07:14,920 --> 00:07:23,280
And then as to the resolution, separability and non-resolution inseparability, they are

59
00:07:23,280 --> 00:07:29,080
positionally unresolvable, inseparable, since they always arise together in every single

60
00:07:29,080 --> 00:07:34,560
minimal material group consisting of the bear octet and the others.

61
00:07:34,560 --> 00:07:47,880
Bear octet means those mentioned on page 397, paragraph 88, color hooded, taste, nutritive

62
00:07:47,880 --> 00:07:56,960
essence and the full elements, they are called bear octets.

63
00:07:56,960 --> 00:08:01,760
But they are resolvable, separable by characteristic, so they are different according

64
00:08:01,760 --> 00:08:03,760
to characteristic.

65
00:08:03,760 --> 00:08:09,240
This is how they should be given attention to as to resolution and non-resolution.

66
00:08:09,240 --> 00:08:14,160
And then to the similar and dissimilar, although they are unresolved, inseparable in

67
00:08:14,160 --> 00:08:19,840
this way, yet the first two are similar in heaviness and so are the last two in lightness.

68
00:08:19,840 --> 00:08:28,480
Up and water are heavy and fire and air are light.

69
00:08:28,480 --> 00:08:34,000
And the first two are dissimilar to the last two and the last two to the first two, so

70
00:08:34,000 --> 00:08:35,800
they are dissimilar.

71
00:08:35,800 --> 00:08:39,320
This is how they should be given attention to as similar and dissimilar.

72
00:08:39,320 --> 00:08:43,960
As to distinction between internal and external, the internal elements are the material

73
00:08:43,960 --> 00:08:47,920
support for the physical basis of consciousness.

74
00:08:47,920 --> 00:08:54,320
That means eyes, ears and so on, for the kinds of intimation and for the material faculties.

75
00:08:54,320 --> 00:08:59,920
They are associated with posture and they are of full originations.

76
00:08:59,920 --> 00:09:01,800
That means caused by full things.

77
00:09:01,800 --> 00:09:06,640
The external elements are of the opposite kind.

78
00:09:06,640 --> 00:09:16,960
The external elements are not caused by all full causes, but just by one, one temperature.

79
00:09:16,960 --> 00:09:21,920
And they are not associated with push-ups and they are not basis for consciousness and

80
00:09:21,920 --> 00:09:24,960
so on.

81
00:09:24,960 --> 00:09:30,440
As to the inclusion, gamma-originated at element is included together with the other

82
00:09:30,440 --> 00:09:35,320
gamma-originated elements, because there is no difference in their origination.

83
00:09:35,320 --> 00:09:39,480
So they are similar in being gamma-originated.

84
00:09:39,480 --> 00:09:44,040
Like was the consciousness-originated is included together with other consciousness-originated

85
00:09:44,040 --> 00:09:45,040
elements.

86
00:09:45,040 --> 00:09:47,320
They are grouped together.

87
00:09:47,320 --> 00:09:51,560
And then as to condition, the other element which is held together by water, maintained

88
00:09:51,560 --> 00:09:57,080
by fire and descended by air is a condition for other three great families by acting as

89
00:09:57,080 --> 00:09:58,800
their foundation and so on.

90
00:09:58,800 --> 00:10:05,160
So they help each other and they do their own functions.

91
00:10:05,160 --> 00:10:07,280
So they are conditioned for each other.

92
00:10:07,280 --> 00:10:14,200
And then as to lack of conscious reaction, that means they have no consciousness, they

93
00:10:14,200 --> 00:10:17,640
have no understanding of their own.

94
00:10:17,640 --> 00:10:20,240
So here to the other element does not know.

95
00:10:20,240 --> 00:10:26,120
I am the other element or I am a condition by acting as foundation for three great

96
00:10:26,120 --> 00:10:27,120
families.

97
00:10:27,120 --> 00:10:28,480
And the other three do not know.

98
00:10:28,480 --> 00:10:33,400
The other element is a condition for us by acting as our foundation and similarly in

99
00:10:33,400 --> 00:10:39,160
each instance, this is how they should be given attention to us, lack of conscious reaction.

100
00:10:39,160 --> 00:10:43,600
So they are non-cognizing things.

101
00:10:43,600 --> 00:10:48,800
They do not have cognition, they do not have consciousness, they do not have thinking.

102
00:10:48,800 --> 00:10:52,520
As to the analysis of conditions, there are four conditions now.

103
00:10:52,520 --> 00:10:53,520
This is important.

104
00:10:53,520 --> 00:11:01,120
There are four conditions or four causes for the elements or for the material properties.

105
00:11:01,120 --> 00:11:07,000
And they are kamma, consciousness, nutrients and temperature.

106
00:11:07,000 --> 00:11:12,960
And kamma is a condition for what is kamma originated, not consciousness and the rest.

107
00:11:12,960 --> 00:11:17,660
And consciousness, etc. alone are these respective conditions for what is consciousness

108
00:11:17,660 --> 00:11:20,920
originated and not others.

109
00:11:20,920 --> 00:11:29,120
And kamma is the producing condition for what is kamma originated, so producing condition.

110
00:11:29,120 --> 00:11:42,360
For what is kamma originated, it is indirectly decisive support condition for the rest.

111
00:11:42,360 --> 00:11:50,400
You have to use knowledge of Patana to understand this.

112
00:11:50,400 --> 00:12:02,120
In Patana, there are 24 conditions mentioned here, the producing condition means that

113
00:12:02,120 --> 00:12:12,920
which produces, but there is no producing condition, I mean no producing condition among

114
00:12:12,920 --> 00:12:21,800
the 28 among 24 mentioned in Patana.

115
00:12:21,800 --> 00:12:28,440
So in the footnote, you see, the term producing condition refers to causing origination,

116
00:12:28,440 --> 00:12:31,440
though as a condition, it is actually kamma condition.

117
00:12:31,440 --> 00:12:39,400
So among the 24 conditions mentioned in Patana, it is kamma condition.

118
00:12:39,400 --> 00:12:44,200
But here, it is certainly producing condition, so they are the same.

119
00:12:44,200 --> 00:12:50,360
For this, it is a profitable and profitable, volition is a condition as kamma condition

120
00:12:50,360 --> 00:12:57,240
for result in aggregates and for materiality due to kamma forms.

121
00:12:57,240 --> 00:13:02,200
And it is indirectly decisive support condition for the rest.

122
00:13:02,200 --> 00:13:10,920
Now decisive support condition has the widest application among the 24 conditions.

123
00:13:10,920 --> 00:13:20,320
If you cannot explain certain things by the other conditions, you may point to the decisive

124
00:13:20,320 --> 00:13:26,160
support condition because it is very wide and so you can include everything in that.

125
00:13:26,160 --> 00:13:36,840
Now strictly speaking, decisive support, pertains to only those that are mental, take

126
00:13:36,840 --> 00:13:40,040
us and take us and not rub us.

127
00:13:40,040 --> 00:13:47,840
So here, we cannot have the direct decisive support, that is why they said indirectly decisive

128
00:13:47,840 --> 00:13:55,840
support, that means decisive support condition not mentioned in the Patana, but mentioned

129
00:13:55,840 --> 00:13:58,480
in some of the sodas.

130
00:13:58,480 --> 00:14:03,640
And so the sub-commentary gives some of the sodas, passages from some of the sodas, but

131
00:14:03,640 --> 00:14:09,600
those sodas cannot be traced, that is why there are blanks between the parenthesis.

132
00:14:09,600 --> 00:14:21,080
So until this time, nobody can trace those quotations in the sub-commentary.

133
00:14:21,080 --> 00:14:28,280
And with a person as decisive support and with a growth as decisive support in the sodas,

134
00:14:28,280 --> 00:14:32,440
the decisive support condition can be indirectly understood according to the sodas in the

135
00:14:32,440 --> 00:14:35,600
sense of absence without.

136
00:14:35,600 --> 00:14:40,720
That means not arising without, without it is something that does not arise.

137
00:14:40,720 --> 00:14:44,200
So that is the condition for that.

138
00:14:44,200 --> 00:14:51,200
So if I cannot exist without you, then you are my decisive support condition, something like

139
00:14:51,200 --> 00:14:52,200
that.

140
00:14:52,200 --> 00:14:59,120
So not arising without.

141
00:14:59,120 --> 00:15:05,720
And the others are also explained that way, so that needs an effort, but I mean it

142
00:15:05,720 --> 00:15:25,080
is not less of Patana, so I think we will skip this.

143
00:15:25,080 --> 00:15:29,480
Then the same method applies in the case of the consciousness originated, the new

144
00:15:29,480 --> 00:15:36,360
treatment originated and the temperature originated at element at the rest, barakra of

145
00:15:36,360 --> 00:15:37,360
130.

146
00:15:37,360 --> 00:15:42,320
And when these elements have been made to occur through the influence of cognizance, et cetera,

147
00:15:42,320 --> 00:15:51,000
conditions with three in four ways to one due and likewise with one due to three, with

148
00:15:51,000 --> 00:15:56,680
two in six ways due to two, thus the occurrence comes to it.

149
00:15:56,680 --> 00:16:03,720
Now, please have in mind, say the four elements, let's say one, two, three, four.

150
00:16:03,720 --> 00:16:10,800
So what is explaining here is one is dependent on the other three, so let us say one is dependent

151
00:16:10,800 --> 00:16:17,240
on two, three, four, two is dependent on one, three, four, three is dependent on one, two,

152
00:16:17,240 --> 00:16:21,280
four, and four is dependent on one, two, three, something like that.

153
00:16:21,280 --> 00:16:33,800
And then two, three, four is dependent on one, and one, three, four is dependent on two, and

154
00:16:33,800 --> 00:16:41,760
then one, two, four is dependent on three, and then one, two, three is dependent on four.

155
00:16:41,760 --> 00:16:52,920
So what about that, mutual dependency, so that is what it explained here.

156
00:16:52,920 --> 00:16:57,200
And then you take two, two, two, two at a time, one, two is dependent on three, four, three,

157
00:16:57,200 --> 00:17:02,840
four, three, four is dependent on one, two, so you get all these things.

158
00:17:02,840 --> 00:17:08,880
Now, next barakra, at the time of moving forward and moving backward, the other element

159
00:17:08,880 --> 00:17:14,600
among this is a condition for pressing pushing, that seconded by the water element is

160
00:17:14,600 --> 00:17:19,440
a condition for establishing only foundation, but the water element, seconded by element

161
00:17:19,440 --> 00:17:21,400
is a condition for lowering down.

162
00:17:21,400 --> 00:17:27,080
So when you lowering down, there is the prominence of water element, the fire element

163
00:17:27,080 --> 00:17:29,920
seconded by air element is conditioned for lifting up.

164
00:17:29,920 --> 00:17:35,560
When you lift your foot up, the fire element and water element are prominent at that time.

165
00:17:35,560 --> 00:17:41,880
So when going down, the heaviness or something like that is prominent, and when you

166
00:17:41,880 --> 00:17:46,320
lift your body or your foot up, there is lightness.

167
00:17:46,320 --> 00:17:58,920
So you see the prominence of a given element, when you raise your foot or when you put

168
00:17:58,920 --> 00:18:04,160
it down, as it gives his attention to them as what too many, etc.

169
00:18:04,160 --> 00:18:08,080
And this way, the elements become evident to him under each heading.

170
00:18:08,080 --> 00:18:14,840
And he again and again, adverse and gives attention to them, excess concentrations, as

171
00:18:14,840 --> 00:18:20,000
he again and again, adverse and gives attention to them, excess concentration arises in

172
00:18:20,000 --> 00:18:24,960
the way already described, so there is only excess concentration, no absorption.

173
00:18:24,960 --> 00:18:29,720
And this concentration, too, is called definition of full elements, because it arises in

174
00:18:29,720 --> 00:18:34,320
one who defines the full element, owing to the influence of his knowledge.

175
00:18:34,320 --> 00:18:42,760
And this big who is devoted to the defining, these are the benefits of this meditation.

176
00:18:42,760 --> 00:18:45,800
And then development of concentration, conclusion.

177
00:18:45,800 --> 00:18:53,800
So what benefit you will get from the development of concentration, and that the benefits

178
00:18:53,800 --> 00:19:00,400
are given in detail from paragraph 120 onwards.

179
00:19:00,400 --> 00:19:08,240
So the first benefit is what?

180
00:19:08,240 --> 00:19:14,280
It is blissful abiding here and now, that is a first.

181
00:19:14,280 --> 00:19:19,200
Now, for the development of solving concentration provides the benefit of a blissful abiding

182
00:19:19,200 --> 00:19:24,080
here and now, for the other hands with kangas described, who develop concentration, thinking

183
00:19:24,080 --> 00:19:29,080
we shall retain and dwell with unified mind for a whole day.

184
00:19:29,080 --> 00:19:36,760
So the other hands and noble ones enter into Jana to enjoy happiness.

185
00:19:36,760 --> 00:19:41,480
And that happiness is called here and now, happiness here and now.

186
00:19:41,480 --> 00:19:46,800
So for the absorption concentration has this benefit.

187
00:19:46,800 --> 00:19:51,840
And the next one is what?

188
00:19:51,840 --> 00:19:56,320
When ordinary people and trainers develop it, thinking after emerging visual exercise

189
00:19:56,320 --> 00:20:01,680
inside with concentrated consciousness, the development of absorption concentration provides

190
00:20:01,680 --> 00:20:04,160
them with the benefit of insight.

191
00:20:04,160 --> 00:20:10,280
So they make the Jana the basis for insight.

192
00:20:10,280 --> 00:20:19,120
So it gives the benefit of insight or insight as benefit by solving as the proximate cause

193
00:20:19,120 --> 00:20:25,480
for insight and so does excess concentration as a method of arriving at wide open in crowded

194
00:20:25,480 --> 00:20:26,480
circumstances.

195
00:20:26,480 --> 00:20:35,840
That means in this samsara samsara is explained to be a very crowded place.

196
00:20:35,840 --> 00:20:44,240
And so you get some white place when you get the excess concentration.

197
00:20:44,240 --> 00:20:53,280
That means you get a little happiness or peacefulness when you get concentration and so you

198
00:20:53,280 --> 00:20:58,480
get a good chance there.

199
00:20:58,480 --> 00:21:07,280
So at wide open conditions in crowded circumstances and the process of existence is the

200
00:21:07,280 --> 00:21:13,560
round of rebirth which is a very cramped place is crowded by the defilements of craving

201
00:21:13,560 --> 00:21:14,720
and so on.

202
00:21:14,720 --> 00:21:17,200
So this is what it meant here.

203
00:21:17,200 --> 00:21:26,360
So you get some moments with a free from the crowdedness of mental defilements of craving

204
00:21:26,360 --> 00:21:29,400
and so on.

205
00:21:29,400 --> 00:21:38,520
And then the next paragraph is a little complicated, 122, so I wrote it, maybe I'll explain

206
00:21:38,520 --> 00:21:40,920
this.

207
00:21:40,920 --> 00:21:51,920
Now the development of concentration has direct knowledge as benefits or they can give the

208
00:21:51,920 --> 00:22:00,160
benefits of direct knowledge, the development of excess immune absorption concentration.

209
00:22:00,160 --> 00:22:10,080
Now when a person has attained all eight attainments that means eight genres and then we

210
00:22:10,080 --> 00:22:12,800
want to get the direct knowledge.

211
00:22:12,800 --> 00:22:19,720
So if we want to get the direct knowledge, what must he do?

212
00:22:19,720 --> 00:22:34,160
So he entered into these eight attainments and then make one of them actually the fourth

213
00:22:34,160 --> 00:22:38,760
genre, make one of them as a basis for direct knowledge.

214
00:22:38,760 --> 00:22:44,440
So he enters into that genre and then emerges from that genre.

215
00:22:44,440 --> 00:22:54,160
And after emerging from that genre, he wishes that I get some kind of miracle power.

216
00:22:54,160 --> 00:23:05,200
Here he always say, having been one, he becomes many and that means I'm one but I want

217
00:23:05,200 --> 00:23:06,200
to be many.

218
00:23:06,200 --> 00:23:15,360
So he can create many, many resemblances of him like Tulabandaga did.

219
00:23:15,360 --> 00:23:23,600
So he wishes for that kind of power and then he practices meditation and then he gets

220
00:23:23,600 --> 00:23:25,320
the direct knowledge.

221
00:23:25,320 --> 00:23:36,000
So that is what is explained in this paragraph but some, it's difficult here to understand.

222
00:23:36,000 --> 00:23:38,080
But the meaning is that.

223
00:23:38,080 --> 00:23:42,640
So after getting the old eight genres, eight genres are called attainments here.

224
00:23:42,640 --> 00:23:55,000
If he wants to get the direct knowledge, then he enters into one genre as a basis for direct

225
00:23:55,000 --> 00:23:58,960
knowledge and that genre is the fourth genre.

226
00:23:58,960 --> 00:24:08,120
And then emerging from that genre, he again makes a wish for something that having been

227
00:24:08,120 --> 00:24:15,480
one, I may become many and then as a result of his meditation, he will get the direct

228
00:24:15,480 --> 00:24:16,480
knowledge.

229
00:24:16,480 --> 00:24:23,960
And the right knowledge really means the ability to create himself into many persons or

230
00:24:23,960 --> 00:24:28,960
their authority and the other opinion as also, so when ordinary people have not lost their

231
00:24:28,960 --> 00:24:33,360
genre and they aspire to rip out in the promo world as, let us be reborn in the promo

232
00:24:33,360 --> 00:24:39,120
world or even do they do not make the aspiration, then the development of absorption concentration

233
00:24:39,120 --> 00:24:43,520
provides them with the benefits of an improved form of existence since it ensures that

234
00:24:43,520 --> 00:24:44,520
for there.

235
00:24:44,520 --> 00:24:50,840
That means the development of concentration can lead you to get a better existence, to

236
00:24:50,840 --> 00:25:01,600
be reborn in a better, better existence as Brahma and so on, so it has this benefit.

237
00:25:01,600 --> 00:25:09,880
And the last one, paragraph 124 is the development of concentration has attainment of

238
00:25:09,880 --> 00:25:12,920
cessation as benefit.

239
00:25:12,920 --> 00:25:18,960
That means those who have reached the third and third stage of enlightenment and also

240
00:25:18,960 --> 00:25:23,440
Arathams can enter into the attainment of cessation.

241
00:25:23,440 --> 00:25:31,040
So during this attainment of cessation, all mental activities are suspended.

242
00:25:31,040 --> 00:25:41,040
For as long as the person wishes, but the maximum duration for human beings is seven days.

243
00:25:41,040 --> 00:25:51,200
So for seven days or for one day or two days, he is like a statue, he has no mental activity.

244
00:25:51,200 --> 00:26:00,600
And then that state of being without mental activities is certainly peaceful and very,

245
00:26:00,600 --> 00:26:06,000
it's a highest form of happiness in this world.

246
00:26:06,000 --> 00:26:15,120
So it can give this benefit to the development of concentration.

247
00:26:15,120 --> 00:26:22,280
And so we come to the end of this chapter.

248
00:26:22,280 --> 00:26:27,640
Now the next chapter deals with supernormal powers, direct knowledge.

249
00:26:27,640 --> 00:26:33,160
So we have a glimpse of direct knowledge in the previous chapter and in this chapter,

250
00:26:33,160 --> 00:26:40,480
these direct kinds of direct knowledge are explained and in this chapter we have a lot

251
00:26:40,480 --> 00:26:42,560
of stories.

252
00:26:42,560 --> 00:26:54,440
So what we should note here is there are five kinds of direct knowledge, the B.R.W.

253
00:26:54,440 --> 00:27:08,760
In the second paragraph, when his concentrated mind is that purified, oh, one is performing

254
00:27:08,760 --> 00:27:14,440
some kind of miracles and the two is the knowledge of penetration of minds that mean

255
00:27:14,440 --> 00:27:17,440
reading other people's minds.

256
00:27:17,440 --> 00:27:26,360
What is third of divine ear, the knowledge of the divine ear?

257
00:27:26,360 --> 00:27:40,640
That means hearing sounds that cannot be heard normally.

258
00:27:40,640 --> 00:27:44,880
And then the knowledge of the penetration of minds.

259
00:27:44,880 --> 00:27:47,800
And then the knowledge of regulating of past lives.

260
00:27:47,800 --> 00:27:51,760
And then the knowledge of passing away and reappearance of being.

261
00:27:51,760 --> 00:27:59,800
These are called direct knowledge.

262
00:27:59,800 --> 00:28:06,880
And the kinds of supernormal power, if a metadata wants to begin performing and so on.

263
00:28:06,880 --> 00:28:16,240
Here, he must achieve the attainment that mean eight journals, full, Rupa, and full Rupa.

264
00:28:16,240 --> 00:28:20,320
In each of the eight casinos ending with white casino.

265
00:28:20,320 --> 00:28:27,040
So the first eight casinos and the last two are left out.

266
00:28:27,040 --> 00:28:29,520
What are the last two?

267
00:28:29,520 --> 00:28:30,520
Right.

268
00:28:30,520 --> 00:28:31,520
Space.

269
00:28:31,520 --> 00:28:33,800
Space and light.

270
00:28:33,800 --> 00:28:46,840
So space is left out because that cannot be the basis for attaining immaterial genre.

271
00:28:46,840 --> 00:28:56,680
And the subcommittee explains that the light casino is to be taken to be taken as the same

272
00:28:56,680 --> 00:29:06,240
as what?

273
00:29:06,240 --> 00:29:09,200
White casino.

274
00:29:09,200 --> 00:29:18,080
So in the Uizurima or eight casinos are given, I mean mentioned here, but the common

275
00:29:18,080 --> 00:29:36,240
really says that we can take also the light casino, but it is included in the white casino.

276
00:29:36,240 --> 00:29:41,880
So that's it, eight casinos.

277
00:29:41,880 --> 00:29:49,360
And then you have to be very, you have to make yourself very familiar with these casinos

278
00:29:49,360 --> 00:29:50,680
and the genres.

279
00:29:50,680 --> 00:29:58,880
So there are 14 ways of making yourself familiar with these genres.

280
00:29:58,880 --> 00:30:03,840
That is in order of the casinos, in the reverse order of the casinos and so on.

281
00:30:03,840 --> 00:30:10,280
So you end up in the genre on this casino and the next genre on this casino.

282
00:30:10,280 --> 00:30:14,800
And then again, you enter into first genre on this casino.

283
00:30:14,800 --> 00:30:21,320
And then second, I mean first genre on this casino and then first genre on the other

284
00:30:21,320 --> 00:30:23,280
casino and so on.

285
00:30:23,280 --> 00:30:31,280
So that is to make you very familiar with the genres and casinos, you have to go in many

286
00:30:31,280 --> 00:30:40,200
different ways.

287
00:30:40,200 --> 00:30:58,160
And then there are many stories in this chapter.

288
00:30:58,160 --> 00:31:07,600
And some stories have been given, and some chapters we have read, and many, many are new,

289
00:31:07,600 --> 00:31:09,600
maybe new to you.

290
00:31:09,600 --> 00:31:18,920
Now on page four hundred and twelve, the givey burned up, what I'm not offering at the

291
00:31:18,920 --> 00:31:21,880
end of a tent paragraph.

292
00:31:21,880 --> 00:31:25,760
And then there is a footnote now.

293
00:31:25,760 --> 00:31:35,680
In Paris, the word for island and a lamp, it's the same, the word diva.

294
00:31:35,680 --> 00:31:43,440
So he took a diva busa to mean island offering, but it is not island offering.

295
00:31:43,440 --> 00:31:47,400
It's ridiculous, you say island offering.

296
00:31:47,400 --> 00:31:52,240
So it is a light offering.

297
00:31:52,240 --> 00:31:59,280
So during that festival, say lights are offered to at different places, like starting

298
00:31:59,280 --> 00:32:04,360
with the GT, here immediately and extending over the whole island and up to a leak into

299
00:32:04,360 --> 00:32:05,600
the sea.

300
00:32:05,600 --> 00:32:13,800
So that is light offering festival, not island offering.

301
00:32:13,800 --> 00:32:21,160
And then the others, I think, I hope you have read that.

302
00:32:21,160 --> 00:32:32,080
Now, I want to give you these numbers of some stories, if you want to go a read, read,

303
00:32:32,080 --> 00:32:37,640
a more detail.

304
00:32:37,640 --> 00:32:51,520
And on page, let's see, on page four hundred and sixteen, at the end of paragraph twenty-eight,

305
00:32:51,520 --> 00:32:54,960
the reference given there is T-H-A-2-240.

306
00:32:54,960 --> 00:33:06,800
So if you have the book Buddhist Legends, then part two, page 238.

307
00:33:06,800 --> 00:33:14,400
And then at the end of paragraph twenty-nine, no reference is given, but actually there

308
00:33:14,400 --> 00:33:15,640
is reference.

309
00:33:15,640 --> 00:33:25,720
So please read Buddhist Legends, part three, page 179 and so on.

310
00:33:25,720 --> 00:33:38,040
And then on page four hundred and seventeen, end of first paragraph that line, M-1-3-3-3.

311
00:33:38,040 --> 00:33:49,680
That is, middle-length scenes, first volume, page three hundred and ninety-six.

312
00:33:49,680 --> 00:34:00,040
And then end of paragraph thirty-three, D-H-A-2-254, that is, Buddhist Legends, part two,

313
00:34:00,040 --> 00:34:04,040
page 246.

314
00:34:04,040 --> 00:34:12,980
And next paragraph, D-H-A-3-300-10 and so on, that is, Buddhist Legends, part three, page

315
00:34:12,980 --> 00:34:32,760
103 and next paragraph, D-H-A-1-2-1-6 and so on, Buddhist Legends, part one, 286.

316
00:34:32,760 --> 00:34:48,720
And then on page 419, paragraph 41-3rd line, D-H-A-4-207, that is, Buddhist Legends, part

317
00:34:48,720 --> 00:34:54,000
three, page 319.

318
00:34:54,000 --> 00:35:07,920
And then beginning of line five, D-H-A-4-216, that is, Buddhist Legends, part three, 327.

319
00:35:07,920 --> 00:35:17,680
And then two lines down, D-H-A-1-1-74, that is, Buddhist Legends, part one, 256.

320
00:35:17,680 --> 00:35:25,000
And then end of the paragraph, D-H-A-3-3-64, that is, Buddhist Legends, part three, page

321
00:35:25,000 --> 00:35:38,640
one hundred and thirty, there, two lines from, from, bottom of that paragraph, made

322
00:35:38,640 --> 00:35:46,360
of the seven gems in a place, actually, not seven gems, seven precious things.

323
00:35:46,360 --> 00:35:54,840
Seven precious things means cold, silver, and other two else.

324
00:35:54,840 --> 00:36:12,680
And then end of paragraph 42, D-H-A-1-3-4, that is, Buddhist Legends, part two, page 59.

325
00:36:12,680 --> 00:36:18,480
So, those are the references.

326
00:36:18,480 --> 00:36:28,680
And in the, in the part we have covered the ten kinds of, we are called AD, Success Power,

327
00:36:28,680 --> 00:36:31,480
ten kinds of Success Power are given.

328
00:36:31,480 --> 00:36:35,320
And we are interested, all of them are interesting.

329
00:36:35,320 --> 00:36:43,240
So if you have time, it is study-edicate, and there is one thing which is very interesting,

330
00:36:43,240 --> 00:36:56,400
and that is, let me see, paragraph 26, a distinction brought about by the influence

331
00:36:56,400 --> 00:37:02,800
of knowledge, either before the arising of the knowledge or after it or at that moment

332
00:37:02,800 --> 00:37:07,160
is called success by intervention of knowledge.

333
00:37:07,160 --> 00:37:17,080
Now, if it wasn't, it's going to, to become an error hunt in that life, he will not die,

334
00:37:17,080 --> 00:37:21,880
however much attempt, attempt is made for, to his life.

335
00:37:21,880 --> 00:37:26,960
He will not die without becoming an error hunt.

336
00:37:26,960 --> 00:37:35,240
So, the, the knowledge of a handshake, which he is going to, going to acquire in that

337
00:37:35,240 --> 00:37:44,200
life, prevents, protects him from being killed.

338
00:37:44,200 --> 00:37:46,200
That is very strange thing.

339
00:37:46,200 --> 00:37:50,800
See, if you are going to become an error hunt, then you will not die without becoming

340
00:37:50,800 --> 00:37:51,800
an error hunt.

341
00:37:51,800 --> 00:38:01,800
And nothing can, nothing can cause you to be killed.

342
00:38:01,800 --> 00:38:09,000
So, the, the stories are given, the, the first one is the story of Venerable Bhakkula.

343
00:38:09,000 --> 00:38:16,160
So, in, in this story, he was followed by a fish, he, his mother was beating him in the

344
00:38:16,160 --> 00:38:21,480
river, and he was swallowed by the fish, and the fish was caught by a, a fisherman, and

345
00:38:21,480 --> 00:38:29,440
so to, to, a lady, and the lady who wanted to cook the fish, cut it open and, and saw

346
00:38:29,440 --> 00:38:32,120
the, to the child there, her life.

347
00:38:32,120 --> 00:38:40,000
So, he did not die in the, in the, in the stomach of the fish, because he was destined

348
00:38:40,000 --> 00:38:43,960
to, to reach enlightenment in that life.

349
00:38:43,960 --> 00:38:46,280
That is why he was not killed.

350
00:38:46,280 --> 00:38:54,320
Normally, he would not survive, so being swallowed by a, a fish, so something like that.

351
00:38:54,320 --> 00:39:00,280
And the next story was Sankeja, so while he was in the war, half his mother, his mother

352
00:39:00,280 --> 00:39:02,280
died.

353
00:39:02,280 --> 00:39:08,600
And so, as, as was the customer and those people tried to commit the body.

354
00:39:08,600 --> 00:39:13,560
So, when they commit the body, they, they use, and they stay.

355
00:39:13,560 --> 00:39:21,640
And so, one sticks, hit the child, and it's, it plays like this at the edge of the eye.

356
00:39:21,640 --> 00:39:23,880
And so, he made, he makes sound noise.

357
00:39:23,880 --> 00:39:28,560
So, one, one, they had the noise, it all there is something there, and then so it cut

358
00:39:28,560 --> 00:39:33,360
open the, point the belly of the body, and then got the child.

359
00:39:33,360 --> 00:39:40,320
So, he was, he was named Sankeja here, so the, his name is Sankeja, because he was

360
00:39:40,320 --> 00:39:46,960
injured by the, by the state, which is in Bali called Sankeu, that's why he was called

361
00:39:46,960 --> 00:39:48,680
Sankeja and so on.

362
00:39:48,680 --> 00:40:00,600
So, there are many stories given, and then Buddha Bala, and also, those who, who cannot

363
00:40:00,600 --> 00:40:09,960
be harmed while they are in attainment, so that, we came with paragraph 30, say, Sahib

364
00:40:09,960 --> 00:40:19,960
Buddha was in attainment, and so, no, no hum could come to him, and then Sankewah, and

365
00:40:19,960 --> 00:40:21,600
so on.

366
00:40:21,600 --> 00:40:32,920
And I think we, we have, reference of the devotee utra, say, some weeks back, and also

367
00:40:32,920 --> 00:40:44,400
about Samavadhi, so they are, they are, to ask miracles, but these miracles are caused

368
00:40:44,400 --> 00:40:50,440
by the intervention of concentration, or intervention of knowledge and so on.

369
00:40:50,440 --> 00:41:01,720
So, these are interesting stories, some are not, among them some are not caused by any,

370
00:41:01,720 --> 00:41:08,160
any meditation or concentration, because, the fact that the birds can fly and so on,

371
00:41:08,160 --> 00:41:13,920
they are also called success of miracles, and they, they, they, they, they contact the

372
00:41:13,920 --> 00:41:18,960
ability just by birth, by being born as, as a, as a bird, so they can fly through the

373
00:41:18,960 --> 00:41:21,200
air, but we cannot.

374
00:41:21,200 --> 00:41:31,000
So, these 10, 10, 10 kinds of success are mentioned in, in the first part of this chapter.

375
00:41:31,000 --> 00:41:47,840
Okay, so we are not meeting next week, so we follow, we should refer through the next

376
00:41:47,840 --> 00:41:56,600
chapter for third part, and let's see, yes, about 20 pages, we are going to have 30, 9,

377
00:41:56,600 --> 00:42:17,440
or 40, or 40, yeah, 440, or 139, oh, yeah, damn it knowledge.

378
00:42:17,440 --> 00:42:24,440
Oh, yeah.

379
00:42:47,440 --> 00:42:51,440
.

380
00:42:51,440 --> 00:42:55,440
..

381
00:42:55,440 --> 00:42:57,440
..

382
00:42:57,440 --> 00:42:59,440
..

383
00:42:59,440 --> 00:43:01,440
..

384
00:43:01,440 --> 00:43:03,440
..

385
00:43:03,440 --> 00:43:07,440
..

386
00:43:07,440 --> 00:43:11,440
..

387
00:43:11,440 --> 00:43:15,440
..

388
00:43:15,440 --> 00:43:15,440
..

389
00:43:15,440 --> 00:43:16,440
..

