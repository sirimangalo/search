1
00:00:00,000 --> 00:00:17,360
Good evening everyone from broadcasting live May 21st, 2016.

2
00:00:17,360 --> 00:00:26,000
So I think there was a bit of confusion or maybe even disagreement about when is we

3
00:00:26,000 --> 00:00:33,920
sack at the food jail, the waste act, the Buddha's birthday.

4
00:00:33,920 --> 00:00:42,000
We counted according to the full moon, so the celebration is on the full moon and we

5
00:00:42,000 --> 00:00:49,240
sack, we sack is the month, we're wasted, I guess, why sack I mean, we're wasted

6
00:00:49,240 --> 00:00:56,240
the kind of Sanskrit, that's the name of the month, so on the full moon, which is today

7
00:00:56,240 --> 00:01:14,480
it was about three hours ago, four hours ago, yeah, so that's, this is, this is

8
00:01:14,480 --> 00:01:21,880
we sack a puja, puja is the puja means the celebration or the holiday, this is the

9
00:01:21,880 --> 00:01:29,600
holiday or the holy day of we sack.

10
00:01:29,600 --> 00:01:36,640
And it said that that was the day that the Bodhisattva was born, it's the day the Bodhisattva

11
00:01:36,640 --> 00:01:42,640
became a Buddha, and it's the day the Buddha passed into Tharni Banda, that's what they

12
00:01:42,640 --> 00:01:48,680
say.

13
00:01:48,680 --> 00:01:55,920
So it's a good opportunity to talk today, I think we'll ignore the quote on our website

14
00:01:55,920 --> 00:02:06,760
whenever that is, but actually it's a really good quote, but maybe we can delve into it

15
00:02:06,760 --> 00:02:13,760
later, we'll talk a little bit about the Buddha, why a Buddha, what's important about the

16
00:02:13,760 --> 00:02:25,600
Buddha, why Buddhism, why do we have to be so concerned about our minds, why do we have

17
00:02:25,600 --> 00:02:34,440
to work so hard to train our minds, there's so much good in the world, so much happiness

18
00:02:34,440 --> 00:02:42,080
in the world, so much pleasure in the world, why can't we just be content the way things

19
00:02:42,080 --> 00:03:03,520
are, isn't it, isn't that enough, isn't that good enough, the answer is it's not all happiness

20
00:03:03,520 --> 00:03:17,160
and pleasure in this world, we're not perfect, we have problems, we get angry, we get

21
00:03:17,160 --> 00:03:27,680
obsessed, we get anxious and worried and afraid, arrogant and conceited and we make mistakes,

22
00:03:27,680 --> 00:03:38,000
we hurt ourselves, we hurt others, we are confused and afraid, we get lost, we make mistakes,

23
00:03:38,000 --> 00:03:48,320
we make bad decisions, we suffer, we feel pain, we get sick, we get old, we die, is there

24
00:03:48,320 --> 00:04:01,920
all parts of life, are there, they exist, they are, and anyone who thinks that these are

25
00:04:01,920 --> 00:04:10,000
just an inevitable part of life that we should just accept and take the good with the

26
00:04:10,000 --> 00:04:23,480
bad, as though there were no need to delve deeper or to respond, no need to train, no need

27
00:04:23,480 --> 00:04:35,360
to better oneself, as such a person is misleading themselves, because really that's our

28
00:04:35,360 --> 00:04:46,360
whole life, our lives from the time that were born or spent learning just this, how to better

29
00:04:46,360 --> 00:04:53,640
ourselves, not just means we become better people but to be happier people, how can we

30
00:04:53,640 --> 00:05:00,120
find happiness, this is what our life is about, it's about learning and learning how to

31
00:05:00,120 --> 00:05:08,800
cope and deal with suffering, we're bombarded by so many kinds of challenge and difficulty

32
00:05:08,800 --> 00:05:16,720
and stress and suffering from childhood, our whole lives have been this way, this isn't

33
00:05:16,720 --> 00:05:22,920
start when you come to practice meditation, we've been asking these questions and finding

34
00:05:22,920 --> 00:05:30,960
answers to them our whole lives, that's what life is, life isn't ever just about a vacation

35
00:05:30,960 --> 00:05:38,760
or enjoyment, if you want to enjoy you have to figure out how to enjoy, how to be happy,

36
00:05:38,760 --> 00:05:47,960
how to acquire the things that you want, how to escape the things that you don't want,

37
00:05:47,960 --> 00:05:57,040
to spend our lives bettering ourselves, problem is mostly we don't find the answers, mostly

38
00:05:57,040 --> 00:06:07,960
we stumble, we're blind, we're not equipped, we're not capable of finding the answers

39
00:06:07,960 --> 00:06:20,280
ourselves, so every so often being a rise is in the world, who is capable, we are all

40
00:06:20,280 --> 00:06:30,400
of lesser or greater to faculties, there's nothing to be ashamed of or it's not to

41
00:06:30,400 --> 00:06:40,440
despair but to be realistic, we're none of us fully cognizant of our situation, fully

42
00:06:40,440 --> 00:06:49,360
wise to the ways of our minds and the ways of the world, we stumble, we make mistakes,

43
00:06:49,360 --> 00:06:58,160
we're confused, we're ignorant to so many things, but a person arises who is nodding

44
00:06:58,160 --> 00:07:08,160
who is able, through their own striving, through their own effort, to find the answers,

45
00:07:08,160 --> 00:07:20,360
and then they share it, they spread this teaching, Buddhism is essential, Buddhism by its

46
00:07:20,360 --> 00:07:25,200
very definition, and I think the word Buddhism it sounds once, just one of the many religions

47
00:07:25,200 --> 00:07:33,280
that's not, Buddhism is not, the word Buddha, it isn't a name like Charlie or Frank,

48
00:07:33,280 --> 00:07:40,840
Buddha means one who is awake or one who knows, one who understands reality, that's what

49
00:07:40,840 --> 00:07:52,320
Buddha means, so it shouldn't be a specialized religion where some people will become

50
00:07:52,320 --> 00:07:57,640
Buddhist and some people won't, some people become Christians or become Buddhist and it's

51
00:07:57,640 --> 00:08:02,680
up to you to choose, it's not really like Buddhism is for everyone, of any religion,

52
00:08:02,680 --> 00:08:12,760
of any persuasion, of anyone living any kind of life, their success and their failure,

53
00:08:12,760 --> 00:08:19,160
their happiness and their suffering, it's going to be directly related to how close they

54
00:08:19,160 --> 00:08:29,160
are to Buddha, how close they are to awakening, how close they are toness and science,

55
00:08:29,160 --> 00:08:44,000
to knowledge, to understanding, to the answers, to living their life in a way that makes

56
00:08:44,000 --> 00:08:56,080
them happy. We think we're happy, we have happiness, we think so much of what happiness,

57
00:08:56,080 --> 00:09:02,080
we might say we're obsessed, we don't see it until you come to meditate, but when you

58
00:09:02,080 --> 00:09:07,880
come to meditate you see how obsessed we are in, and it seems like well you could just

59
00:09:07,880 --> 00:09:16,360
get what you want, why do I have to come to meditate, just get what I want, but it's not

60
00:09:16,360 --> 00:09:26,480
that simple, you can't always get what you want, it's not just trade saying, it's beyond

61
00:09:26,480 --> 00:09:38,160
that, you often get what you don't want, to such a degree that you, you moan and you whale

62
00:09:38,160 --> 00:09:45,840
and you lament and you cry and you scream and you shake your fists and you beat your

63
00:09:45,840 --> 00:09:57,720
breast and all these things, people go crazy, kill themselves because they can't take

64
00:09:57,720 --> 00:10:05,080
the suffering, it's part of our daily life, we make mistakes because suffering for ourselves

65
00:10:05,080 --> 00:10:16,920
and others, Buddhism is not optional, Buddhism is the right way to live our lives, we're

66
00:10:16,920 --> 00:10:23,520
trying, we're trying always to live our lives in a better way, happiness for us, happiness

67
00:10:23,520 --> 00:10:33,360
for our lives, we can't succeed, we can't do it, we fail, mostly, maybe we get by so

68
00:10:33,360 --> 00:10:43,520
we can be happy, but that happiness is qualified, it's balanced with a lot of stress and

69
00:10:43,520 --> 00:10:54,360
suffering and angst and anxiety and anguish, and that's not something you should settle

70
00:10:54,360 --> 00:10:58,400
for, it's not something we ever settle for, we don't, you can say oh well you take, it's

71
00:10:58,400 --> 00:11:08,040
not how the world works, we're always learning, we're always striving, we're always aiming

72
00:11:08,040 --> 00:11:13,120
for the top, we all want to be happy all the time, there's no question, think it's impossible

73
00:11:13,120 --> 00:11:22,440
but that's how we live our lives, we're always trying to be happy, the claim we make

74
00:11:22,440 --> 00:11:30,920
is that, it's not quite as easy as it sounds or as it seems, but that there is a way,

75
00:11:30,920 --> 00:11:38,320
if you work, if you work to straighten your mind, evolve its crookedness and all of

76
00:11:38,320 --> 00:11:53,320
its cleanliness, you work to straighten and purify your mind, train yourself to see things

77
00:11:53,320 --> 00:12:05,280
as they are, not how you wish they were, train yourself to accept, not accept, but tolerate

78
00:12:05,280 --> 00:12:14,800
to stand strong in the face of anything, to be undisturbed by the vicissitudes of life, you

79
00:12:14,800 --> 00:12:22,200
can train yourself in that, you're invincible, you can be happy all the time, you can be

80
00:12:22,200 --> 00:12:29,120
at peace, no matter what it comes, you can conquer all of life's problems and challenges

81
00:12:29,120 --> 00:12:35,000
and difficulties.

82
00:12:35,000 --> 00:12:45,680
It was a very special person, today we honor his memory and someone who taught something

83
00:12:45,680 --> 00:12:56,080
that, for those who have practiced it clearly does lead to peace, happiness and freedom

84
00:12:56,080 --> 00:13:03,000
from suffering, it's an honest path, it's a straight path where you're no longer crooked,

85
00:13:03,000 --> 00:13:08,360
you're no longer fooling yourself, you're no longer deluding yourself, you're no longer making

86
00:13:08,360 --> 00:13:19,840
mistakes, you're no longer choosing the wrong response to problems, to challenges, where

87
00:13:19,840 --> 00:13:31,760
you respond properly, mindfully, purely peacefully.

88
00:13:31,760 --> 00:13:39,840
So the three qualities of the Buddha that we remember on this day are, first of all, his

89
00:13:39,840 --> 00:13:47,640
wisdom, this is what we were talking about, the Buddha understood the truth and he understood

90
00:13:47,640 --> 00:13:58,480
the way to find the truth, he was able to teach the truth, a great wisdom, to understand

91
00:13:58,480 --> 00:14:05,880
and to see, it's very essence, what does it mean to be wise, it's a very specific thing

92
00:14:05,880 --> 00:14:13,640
in Buddhism, it means to see everything that arises and ceases, everything that exists

93
00:14:13,640 --> 00:14:27,800
in this world, everything that arises is not permanent, it's not stable, it's not predictable,

94
00:14:27,800 --> 00:14:34,760
everything that arises is unsatisfying, it's not a source of happiness, happiness can't

95
00:14:34,760 --> 00:14:41,560
come from a thing, no thing can make you happy because it's impermanent, it won't satisfy

96
00:14:41,560 --> 00:14:53,960
you, if you try to find happiness in it, you will suffer disappointment when it's gone,

97
00:14:53,960 --> 00:15:00,400
and that everything in the world, inside of ourselves and the world around us, it doesn't

98
00:15:00,400 --> 00:15:09,720
belong to us, doesn't have an end, doesn't have its own entity or existence, isn't

99
00:15:09,720 --> 00:15:17,200
the thing that you can possess, it doesn't belong to a thing, doesn't belong to anyone,

100
00:15:17,200 --> 00:15:24,280
which means you can't control, there's no control over things, nothing can control and

101
00:15:24,280 --> 00:15:32,800
change so that you keep it, or something you keep it away, everything is in a constant

102
00:15:32,800 --> 00:15:38,840
flux, seeing, hearing, smelling, tasting, feeling, thinking that's really all there

103
00:15:38,840 --> 00:15:45,880
is, when you see a person, the person isn't real, the seeing is real, when you hear the

104
00:15:45,880 --> 00:15:51,640
person in the hearing is real, when you taste food, the food isn't real, it's the taste

105
00:15:51,640 --> 00:16:00,840
of the feeling, it's quite simple, reality is actually quite innocuous, there's nothing

106
00:16:00,840 --> 00:16:10,760
dangerous or scary, if you're being confronted by a scary situation in the end, it only

107
00:16:10,760 --> 00:16:18,680
comes down to the senses, if you're in an argument with someone and you get frightened

108
00:16:18,680 --> 00:16:25,400
because of how loud they are and how angry, because of how vicious they are, we remember

109
00:16:25,400 --> 00:16:32,080
it's only seeing, hearing, smelling, tasting, feeling, thinking, nothing good comes from

110
00:16:32,080 --> 00:16:39,920
clinging to it, it's impermanent unsatisfying and controllable, you getting caught up and

111
00:16:39,920 --> 00:16:45,120
it doesn't do anything good.

112
00:16:45,120 --> 00:16:50,680
So the first is the wisdom of the Buddha taught, all of the teachings on the four foundations

113
00:16:50,680 --> 00:16:55,840
of mindfulness, how to see things clearly.

114
00:16:55,840 --> 00:17:05,760
The second is the purity of the Buddha, so through his wisdom, he found purity and that's

115
00:17:05,760 --> 00:17:10,600
what we all strive for, we strive through our wisdom and understanding to have the

116
00:17:10,600 --> 00:17:16,840
pure mind, same pure mind of the Buddha, we don't get greedy or we don't get angry or

117
00:17:16,840 --> 00:17:28,880
we don't get afraid or anxious or depressed or worried, confused, arrogant, conceited,

118
00:17:28,880 --> 00:17:40,120
and frustrated, none of these things come to us, and we have a mind that is well-trained,

119
00:17:40,120 --> 00:17:51,760
that is well-tamed, that is, and the third quality of the Buddha, that's not universal

120
00:17:51,760 --> 00:17:56,920
and it's not something we all strive for, it's his compassion, we don't all strive for

121
00:17:56,920 --> 00:18:02,240
the same level of compassion of the Buddha, some do.

122
00:18:02,240 --> 00:18:08,480
But we praise the Buddha for it and we thank him for it and we revere him, we feel gratitude,

123
00:18:08,480 --> 00:18:17,960
because he didn't have to teach, he didn't even have to spend all the time becoming capable

124
00:18:17,960 --> 00:18:24,960
of teaching, he had opportunities to follow the teachings of another Buddha, but he didn't

125
00:18:24,960 --> 00:18:30,920
instead, he went on to cultivate his own perfection so he could find it by himself in

126
00:18:30,920 --> 00:18:37,640
a time when there was no Buddha, and so that's what we're left with, that's what allows

127
00:18:37,640 --> 00:18:47,920
us now, 2,500 years later, to still practice, because he took the time to cultivate his

128
00:18:47,920 --> 00:18:54,200
own perfection so that he could teach on his own, but many people who try to do that,

129
00:18:54,200 --> 00:19:01,880
who try to emulate his perfection, and many people who don't, who are content with following

130
00:19:01,880 --> 00:19:10,600
his teachings, there are those who are discontent and who actually take the time to stay

131
00:19:10,600 --> 00:19:16,000
around in some sorrow for lifetime after lifetime, cultivating their own perfections and

132
00:19:16,000 --> 00:19:22,800
working to better themselves to the point where they too can be a Buddha, and that's

133
00:19:22,800 --> 00:19:32,720
what our Buddha did, so we're respecting for that, the day is the day to think about the

134
00:19:32,720 --> 00:19:37,560
Buddha and to practice the Buddha's teaching, the Buddha said, if we care for him, we

135
00:19:37,560 --> 00:19:44,800
will practice his teaching, so that's all take this as an opportunity to better ourselves

136
00:19:44,800 --> 00:20:00,720
and that respect for the Buddha to practice his teachings, so that's the dhamma for tonight,

137
00:20:00,720 --> 00:20:13,120
and we got here some questions, we're going to restrict the site hopefully in the next

138
00:20:13,120 --> 00:20:19,360
week we're going to recreate this site in a new format, it'll look a little nicer and

139
00:20:19,360 --> 00:20:30,880
it'll be a little more professional, I guess, is taking refuge in the triple gem required

140
00:20:30,880 --> 00:20:35,960
for stream imagery, if not, then what is the minimum requirement for stream imagery?

141
00:20:35,960 --> 00:20:42,120
Someone who is a Sotupana has perfect faith in the Buddha, the dhamma, the sangha, so it's

142
00:20:42,120 --> 00:20:49,920
not about actually a ceremony like taking refuge, but they do naturally, but all that's

143
00:20:49,920 --> 00:20:57,920
required for stream imagery is seeing nibana, that's what happens, seeing the three characteristics,

144
00:20:57,920 --> 00:21:04,040
so clearly that the mind lets go enters into nibana for the first time, that's what makes

145
00:21:04,040 --> 00:21:27,040
someone a stream encounter, a Sotupana, how do you stay in a peaceful state without being

146
00:21:27,040 --> 00:21:34,640
distracted, especially in society? Well, come and do a meditation course, we'll teach you

147
00:21:34,640 --> 00:21:40,040
how, it's not something I can just say to you, you should read my booklet if you haven't,

148
00:21:40,040 --> 00:21:45,080
if you come and do a meditation course with us, stay with us, we'll feed you, how

149
00:21:45,080 --> 00:21:55,960
does you teach you all for free, and then you'll have your answer? Does banga mean vanishing

150
00:21:55,960 --> 00:22:03,160
without remainder, so does it go against the view that energy cannot be created or destroyed?

151
00:22:03,160 --> 00:22:19,040
Why do you torture me so, banga means vanishing without remainder, and yes, I guess it goes

152
00:22:19,040 --> 00:22:26,240
against, I mean, I think it does go against the view that energy can either be created

153
00:22:26,240 --> 00:22:33,720
or destroyed, but it's a different world view, we don't talk in terms of energy, energy

154
00:22:33,720 --> 00:22:39,360
isn't a thing according to Buddhism, it doesn't exist, all that exists or experiences,

155
00:22:39,360 --> 00:22:49,880
and experiences and horizons. What's the official name of the tradition in Mahasi Sayyada,

156
00:22:49,880 --> 00:22:59,520
we call it the Mahasi Sayyada tradition, some people call it Satipatana, we pass in that,

157
00:22:59,520 --> 00:23:14,720
some people call it Teravada, Burmese, we pass in there, Burmese, Satipatana, I think

158
00:23:14,720 --> 00:23:23,880
Haravada Lupe has a autocorrect problem, to the bushes have all the 32 marks of heat

159
00:23:23,880 --> 00:23:34,320
man, you have to be careful before you submit, I think you mean to the Buddha's have

160
00:23:34,320 --> 00:23:59,320
all the 32 marks of your great man, the Buddha apparently has 32 marks, 32 characteristics.

161
00:23:59,320 --> 00:24:04,920
You guys don't have to stick around for this, you can if you want, but I'm just answering

162
00:24:04,920 --> 00:24:26,800
question, okay, just so many of them, got two meditators here, my call and one meditator.

163
00:24:26,800 --> 00:24:33,800
So this week we'll try to work on the website, Saturday and next Saturday is the way

164
00:24:33,800 --> 00:24:46,520
sex celebration, this is saga, and then a few days after that I'm off to Thailand, we've

165
00:24:46,520 --> 00:24:55,760
got someone coming to stay in the house while I'm away, Jason who's here in Hamilton, he's

166
00:24:55,760 --> 00:25:00,400
on our Buddhism association, he said he'll come and stay in the house while I'm away,

167
00:25:00,400 --> 00:25:06,640
which is good, good to hear, but we still need someone who can come long term to stay with

168
00:25:06,640 --> 00:25:14,040
us, someone who can help look after the meditators, who wants to spend some time months

169
00:25:14,040 --> 00:25:21,680
in the monastery, someone who doesn't have a dog that they have to go in the captain.

170
00:25:21,680 --> 00:25:26,160
Does a point of view mean it can't be changed or discarded, or does there always have

171
00:25:26,160 --> 00:25:40,560
to be a point of view in all situations, you're making my brain hurt, not about that question.

172
00:25:40,560 --> 00:25:48,880
A view is a belief, so you can have a view that is in line with reality, you can have

173
00:25:48,880 --> 00:25:55,400
a view that is out of line with reality, you don't always have a view, sometimes you do

174
00:25:55,400 --> 00:26:03,000
something out of habit without a view that it's right or wrong or good or bad, et cetera.

175
00:26:03,000 --> 00:26:07,840
But views do arise, and they're occasional, not every mind has a view in it, so not

176
00:26:07,840 --> 00:26:23,000
every experience as a view. We had a, we have an uptick today, 40 people watching today,

177
00:26:23,000 --> 00:26:31,480
that's I think, that's a high, and I was in Sri Lanka used to get 40-50 I think, but

178
00:26:31,480 --> 00:26:42,800
since I've been back in Canada, it's rare to get 40 people, so hello everyone, welcome.

179
00:26:42,800 --> 00:26:46,080
If you're wondering where the questions are, if you're watching this on YouTube, maybe

180
00:26:46,080 --> 00:26:53,960
watching it later even, most of what we do is send it around our own website at meditation.ceerymongolow.org,

181
00:26:53,960 --> 00:26:59,520
and that's where people are chatting in it, asking questions, and meditating together.

182
00:26:59,520 --> 00:27:09,320
We also have online meditation courses, so people sign up for those, and meditate together

183
00:27:09,320 --> 00:27:13,240
online as well.

184
00:27:13,240 --> 00:27:25,640
Okay, so I'm going to say good night to everyone, I'm sure you all are happy we saw

185
00:27:25,640 --> 00:27:31,800
a couple of chat, happy full moon, and see you all later.

