Good evening, everyone, broadcasting live from Stony Creek, Ontario, August 7, 2015.
Here we have another verse on Mehta, on a verse passage.
This one is actually one of the more popular passages.
It may not be very well known in the West, but in Thailand and in Asia we actually, some places they'll chant it.
When I stayed at the Thai monastery in Los Angeles, they would chant this every day.
Subscribe to Mehta!
It sounds nice in the poly.
So here we have 11, 8th, 8th, 8th, 8th, 9th, 9th, 8th, 7th, 11th.
So I saw people are asking, what are they?
11.
Because in English it's not clear, but if you click on the link to the poly, if you're using
Firefox and you have the digital poly reader installed and on top of that you
click on the link it will take you to the poly and if you read that you can see in
the second paragraph it's clear of course you have to also know poly. A lot of
apps. So kang supati one dreams or sleeps in happiness because it's not one is
not bothered by guilt or remorse, hatred or fear.
So kang party bhudshati one wakes up with happily so does the wake up feeling
guilty or bad about the things that they've done either the papa kang supinang
bhudshati one doesn't see any evil dreams because one's mind is clear. Love is
like this pure vibe that keeps your mind on a on a wavelength on a positive
wavelength. So the bad brain waves can't arise in the bad mental activity is
quite it's quite just because the mental activity is unimpositable. I mean
there's probably a more organic way to explain it but basically it's it's
like that it's like a good vibes and good vibrations and have the effect of
focusing one's one's state of mind. So one doesn't see bad dreams either.
Manusan and Biyohoti one is dear to humans.
Up Manusan and Biyohoti one is dear to non-humans. Non-humans is usually in
modern times the day they use the word to mean like ghosts or evil spirits but
I'm gonna say could mean any non-human could mean animals. People who have love are
very good with animals. People who have made that. You can often tell someone
someone's love by how good they are with animals. How good they are with children.
How good they are with animals. Usually you can't tell how good they are with
ghosts or angels or so. It looks so easy.
But the angels guard the person. They're guarded by angels. Angels see fit to
guard because they appreciate the goodness of the person. They try to make
sure that no harm comes to this being. Apparently even though we can't
normally see them this apparently happens. Nasa-agiywa-wisan-wa-sak-tang-wa-kamati.
Fire poison and swords do not affect one. Do not affect one. They can't penetrate
it. It's the meaning. Not for that person to unge and not into that person to
fire and poison and weapons enter. So a person would love if they have this.
Remember we're talking here about Jitou-i-mouti which means a state of Jhana of
intense super kind of super mundane concentration. It's still mundane but it's
beyond the central realm. So there's a state of vibration that is so intense
that it actually takes one out of this sphere. And so the idea is that weapons
don't even penetrate. You can get hit with a sword or shot with a gun. They talk
about this in Thailand. It's possible for there's been cases where people were
monks or meditators were shot with guns and it didn't hit them somehow.
I mean the truth of it is something else but this is the idea. This is something
apparently the Buddha said. Poison, if you're full of love there's a strength of
mind to it. So how many do we have? One, so kang-su-pati, dreams, sleeps in happiness,
two wakes in happiness, three no evil dreams, four dear to humans, five dear to
non-humans, six angels, a garden, two person, seven these things don't hurt
a person. Tu-wa-tang-ji-tang samadhi, number eight, one gets a well-established
concentration. Mine concentrates a Tu-wa-tang, it's quickly. Mine becomes
concentrated quickly because one doesn't have to deal with negativity. The
mind is full of positivity wishing only good things.
positivity supports the mind. 9. Mukovano, we've received a tea when there's a radiant complexion.
Number 10. One dies with it. Asamu-ho-galang-karate, one makes one's time, means
dies, makes an end to one's time. Asamu-l-ho, without confusion and confused
because the mind is so sharp and strong and focused, clearly aware of what is
good. And if one doesn't further develop insight meditation,
but the wheat genet, wheat genet, one doesn't develop further. Brahmalok-pago-ho-tang,
one is set to be a right to arise in the Brahbalok.
So, yeah, love is a great thing. Love is awesome, it has these benefits. But
there's clearly a limit to those benefits. There's no how great they are.
Finally, there's a provision that it can only lead so far.
It leads to big born as a god or to arise in the Brahmalok, which is still
finite and still a part of samsara. Nonetheless, it's great and it's quite
helpful in the practice of insight meditation. Today it's fortuitous or
serendipitous or it is serendipitous. That this verse comes today. Today we
were invited to for lunch at a restaurant in Aaron Mills, I think, or some
more near, some more Mississauga, Toronto area. And I wasn't really keen to
go, I said, I got a call and said, hey, you want to go, I said, no, I'm not really.
Oh, I think I know what it was. I knew it was there was a monk who'd visited
here and he's such a kind monk. Someone who's so full of love, you could just get
us feeling for how kind he is. Well, if I don't go, it's going to be
disappointing to him. And you see how great his love is able to manipulate
people without trying, he manipulates me and the one thing to go because I
wouldn't want to disappoint someone so full of love. So I went and it turned out
it was a meeting of all the many of the monks who had helped organize or been
involved in the celebration of Weisak in that's in celebration square in
Mississauga in May. So I don't have any pictures I don't think, but there's lots of
pictures I'm sure up on Facebook by now. If you check out Bhandi Serna Paula's
page, I bet he's got some up. But it was awesome. There was so much just in the
short time that we were together for lunch, maybe two hours. There was such
love and just kindness and you get a sense of how great it is to be in good
company. How great it is to be with people who have this love and this caring.
And then you can also feel where there's not so much love. Like there are times
where the famines arise and so you get a sense of that person who's not
really in the spirit, you know, that kind of thing. And you can taste you can tell
that you can taste the difference, kind of different flavor to it. So there's no
one, no, never underestimate the power, the greatness of love. It's a wonderful
thing and brings lots of benefits. So that's the dumb of her today. Now we can
get into some questions. Does anybody have any questions? I'm going to stipulate
that they should probably be about meditation because I don't want to get
overwhelmed here. So let's try to keep them to meditation question.
What do you do if you have a bad dream but cannot wake up?
The problem of dreaming is there's no mindfulness, the variability that's
required to grasp to grasp what's happening and to experience it clearly and
to realize this is a dream is generally lacking. As soon as you realize it's a
dream you wake up, I think there are people I guess who have lucid dreams. But in
that case, it's much less a dream. I don't know so much about lucid dreaming.
Anyway, I don't know. I'd probably wake up. It's not that big of a deal. It
happens. It's not much you can do about it. The more you meditate the less you
dream, certainly the less bad dreams you have. Prevention, prevent yourself from
having the bad dreams in the first place. They're generally a sign of guilt or
other negative mind stays.
So if you're listening, if you're watching on YouTube and you want to know what's
going on here, we're actually asking and we're doing the question thing in
another place. We're doing questions at meditation.ceremungalow.org and so I'm
not answering questions on YouTube right now. I'm reading questions.
People are posting in our chat, chat box, our shout box over at
meditation.ceremungalow.org. But I'm also broadcasting simultaneously on
YouTube. Just because there's lots of people on YouTube.
We recommend practice of the ability to lucid dreaming. Well, no.
No, I teach based on a specific meditation practice. I think there's a link.
Yes, there's a link to the booklet up near the top of the page. So if you click on
that link, you'll see what I recommend.
Any advice for countering the attachment to the idea of progress in the
meditation practice or such, desire acceptable. It's acceptable, I'd say, in the
beginning. It's in the end, you do away with it.
Beginning, you're doing everything wrong. In the end, you're just mindful.
There's no countering. We're not in charge here that we can counter.
We're just trying to dodge the bullets. Duck and weave, duck and weave.
Not exactly duck and weave, like avoid, but be in the right place in the right
situation. Experience every experience with the right frame of mind, with a clear
frame of mind. It's like dodging bullets, in a sense,
dodge all the problems. It takes a lot of maneuvering flexibility.
Are there any landmarks besides Nimbana that one will continue
on the right path in the next life? No, no, no. Yeah, there's one and it's called
julesso dapana. If one retains at least the second stage of knowledge,
the tradition goes that such a person will not be born in a bad state in
the next life. Just the reaching the second, so understanding about it,
seeing cause and effect, practicing meditation to the point where you
experience cause and effect, and you're able to see
how the body and the mind work together and how good deeds lead to.
Good results, bad deeds or bad mind states lead to bad results.
How much control does a novice have over how much formal practice they can do?
Or how much can a novice expect to be able to formally practice?
I guess it sounds like you have the idea that Buddhism is somehow
monolithic, homogenous. Every monastery you go to is different.
You can't ask me that unless you're talking about coming here toward you.
So, ordination is a vehicle. It depends very much what you do with it and where you go with it.
And so yeah, it depends also on your teachers. Maybe they make you sweep out the
latrines every day. So we sweep the forest. In Thailand, they like to sweep the
forest. It's just one of the most absurd things to
we have a very different view from over here and they'll sweep out the
entire forest. So there's no more leaves on the ground and then they'll burn out the leaves.
Keeps the snakes away and the scorpions I guess.
Well, if you came to ordain here, you would have lots and lots of time to meditate conceivably.
Because there's not much else to do. I mean, you could learn poly. You could have to do
polycourses and study you can do. But you certainly would have time to do courses. You can be
expected to do meditation courses regularly.
When you recommend to meditate right before going to bed for preventing bad dreams, yeah,
it's a good recommendation. Can you talk about tensions in the body, especially in the jaw?
Is it okay to let the jaw relax and have the mouth hang open when meditating?
Yeah, sure. Your mouth dries. It tends to dry out if you do that.
You can let it relax, but maybe relaxed and closed. It's better.
There's nothing wrong with tension. As long as you're able to maintain it.
Yeah, insects are a tough one. When I was in Sri Lanka,
I had termites in my cave, and they got into my books and my tent. They actually ate a
hole through the bottom of the tent. There's a mosquito tent that I was sleeping in
to get at the books that were inside the tent. They weren't interested in the plastic,
but it got in their way, so they just ate a hole through the bottom of the tent.
And so I had to find a way to get them out of the cave. So what I did is I funneled them all
into a jar and then I took a funnel, another funnel. I went outside and found a place where the
termites were, and I opened a little hole in it, and I funneled them all into the different termites.
Sorry, what's the question about the repass in a retreat? Don't see the question exactly.
Hopefully I'd have a pro tip. I don't really know what a pro tip is. I mean, I don't recommend,
I can't recommend meditation courses in other traditions, so unless it's in our tradition,
it's not really much I can say. You have to talk to people in that tradition.
Except to say that it's probably a good thing when I can't say too much about other traditions.
Okay, well that's it.
Thank you all for tuning in then, and keep up the practice. We'll see you again tomorrow.
All right, good night.
