1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Dhamupada.

2
00:00:05,000 --> 00:00:15,000
Today we continue with verse 225, which reads as follows.

3
00:00:15,000 --> 00:00:37,000
A Hingsaka yimunayo, Nijanga yin sangutta, dayyanti ajutantana, yatagantwa nasotre, which means non-violent

4
00:00:37,000 --> 00:00:48,000
those sages ever restrained in body.

5
00:00:48,000 --> 00:00:55,000
They go to a place they never fall away from.

6
00:00:55,000 --> 00:01:07,000
We're having gone, one suffers not, one sorrows not.

7
00:01:07,000 --> 00:01:19,000
This verse was apparently taught in response to a question that relates to a story.

8
00:01:19,000 --> 00:01:34,000
But the Buddha, when he was living near Saketa, story goes that there was a brahmana living in the area.

9
00:01:34,000 --> 00:01:44,000
When the Buddha went on arms around this brahmana saw the Buddha walking and rushed up to him and said,

10
00:01:44,000 --> 00:01:50,000
My son, my son, what are you doing?

11
00:01:50,000 --> 00:01:53,000
Where have you been?

12
00:01:53,000 --> 00:01:54,000
Where have you been?

13
00:01:54,000 --> 00:01:57,000
Why have you not come home?

14
00:01:57,000 --> 00:02:03,000
Why have you not come to see us?

15
00:02:03,000 --> 00:02:10,000
And he invited the Buddha to his house and to his wife and his wife said,

16
00:02:10,000 --> 00:02:16,000
the brahmana woman said, my son, where have you been?

17
00:02:16,000 --> 00:02:19,000
Why have you not come to visit?

18
00:02:19,000 --> 00:02:22,000
And they called out all of their children and said,

19
00:02:22,000 --> 00:02:29,000
come come, pay respect, your brother is a Rishi, is an ascetic.

20
00:02:29,000 --> 00:02:34,000
Come and pay respect to your brother.

21
00:02:34,000 --> 00:02:43,000
And the Buddha was silent and he waited through it patiently and they asked the Buddha,

22
00:02:43,000 --> 00:02:49,000
they said, our son, please, you must come to eat at our house every day.

23
00:02:49,000 --> 00:02:58,000
Why would you go anywhere else when you're in your home, when you're near us, your parents?

24
00:02:58,000 --> 00:03:07,000
And the Buddha said, oh, it's not the way, it's not our way to go to the same place regularly.

25
00:03:07,000 --> 00:03:21,000
Buddha's way is to support and commune with all sorts of different people by moving, by traveling.

26
00:03:21,000 --> 00:03:30,000
And they said, well, then please send us your followers, send us other followers of your teaching, other monks.

27
00:03:30,000 --> 00:03:33,000
We'll take care of them.

28
00:03:33,000 --> 00:03:40,000
And they did this and whenever the Buddha was free, he would go to their house and again and again,

29
00:03:40,000 --> 00:03:50,000
they listened to his teachings and they came to the monastery and always calling him our son.

30
00:03:50,000 --> 00:04:01,000
And they became Anagami throughout this time, they practiced the Buddha's teaching and listened to his teaching and became devout followers.

31
00:04:01,000 --> 00:04:10,000
And we're successful in becoming Anagami, the non-returner, meaning when they passed away, they would never be reborn again.

32
00:04:10,000 --> 00:04:20,000
They had given up all greed, all anger, all central greed anyway, and all anger.

33
00:04:20,000 --> 00:04:22,000
They were very pure.

34
00:04:22,000 --> 00:04:30,000
And so the monks took this up as a topic of conversation one day asking, what's going on here?

35
00:04:30,000 --> 00:04:39,000
Not only do these Brahmin people erroneously,

36
00:04:39,000 --> 00:04:44,000
you know, label the Buddha as their son, the Buddha doesn't correct them.

37
00:04:44,000 --> 00:04:54,000
They know that the Buddha has a father and a mother, Sudhodana and Mahapatyya.

38
00:04:54,000 --> 00:04:58,000
They were his parents.

39
00:04:58,000 --> 00:05:02,000
And the Buddha doesn't bother to correct them.

40
00:05:02,000 --> 00:05:06,000
So it's quite exceptional.

41
00:05:06,000 --> 00:05:11,000
And the Buddha heard them and he said, oh, you want to know the reason why they call me?

42
00:05:11,000 --> 00:05:21,000
Their son and he told a story of the past in Jataka and he said, in 500 lifetimes, in 500 births,

43
00:05:21,000 --> 00:05:27,000
both of these two lovely people were my parents.

44
00:05:27,000 --> 00:05:37,000
In 500 births, they were my aunt and uncle, and in 500 births, they were my grandparents.

45
00:05:37,000 --> 00:05:42,000
So again and again and again, we were related.

46
00:05:42,000 --> 00:05:52,000
And as a result, this perception, this recognition has stuck with them.

47
00:05:52,000 --> 00:05:58,000
From that time on, the story goes on to say that they spent the rains with the Buddha,

48
00:05:58,000 --> 00:06:02,000
that the Buddha relied upon them during the three months of the rainy season.

49
00:06:02,000 --> 00:06:10,000
He went ever and again to their house and through his guidance, they became our aunt.

50
00:06:10,000 --> 00:06:11,000
And passed away.

51
00:06:11,000 --> 00:06:17,000
And when they passed away, the monks asked the Buddha, where did they go?

52
00:06:17,000 --> 00:06:23,000
Most are not realizing that they had become our aunt, never to be born again.

53
00:06:23,000 --> 00:06:30,000
The Buddha said, oh, people, the sages like them, special individuals, you know,

54
00:06:30,000 --> 00:06:32,000
quite exceptional.

55
00:06:32,000 --> 00:06:34,000
They never born again.

56
00:06:34,000 --> 00:06:36,000
They go to a place where did they go?

57
00:06:36,000 --> 00:06:40,000
They go to a place that you'll never fall away from.

58
00:06:40,000 --> 00:06:46,000
And then he taught this verse.

59
00:06:46,000 --> 00:06:51,000
So the lesson of the story, if there is one, I think, especially relating to meditation,

60
00:06:51,000 --> 00:06:58,000
is in regards to perception and in regards to concepts.

61
00:06:58,000 --> 00:07:04,000
Specifically, concepts of relationship.

62
00:07:04,000 --> 00:07:16,000
The story reminds us that our relationships are conceptual based on our perception.

63
00:07:16,000 --> 00:07:23,000
Many of us, I think, can relate not necessarily to the specific perception of these Brahman couple,

64
00:07:23,000 --> 00:07:31,000
but to this general type of perception where you recognize someone who you've never met before.

65
00:07:31,000 --> 00:07:35,000
They're in sharper focus than everyone else.

66
00:07:35,000 --> 00:07:41,000
And your connection to them is quick and strong and lasting.

67
00:07:41,000 --> 00:07:45,000
When we see this with people, we just met for the first time.

68
00:07:45,000 --> 00:07:52,000
And it might be good, it might be bad, it might be negative in the sense that we become enemies, rivals.

69
00:07:52,000 --> 00:07:57,000
It can be just as sharp as with friends.

70
00:07:57,000 --> 00:08:02,000
But we generally don't look at family the same way.

71
00:08:02,000 --> 00:08:10,000
And this story gives us a reminder that even our family members,

72
00:08:10,000 --> 00:08:16,000
there are no less conceptual than our friends or the relationships with our friends and so on.

73
00:08:16,000 --> 00:08:23,000
Our loved ones, our lovers, our romantic and loving partners.

74
00:08:23,000 --> 00:08:40,000
Who is to say that this couple is any less the Buddhist parents than Sudhodhana or Mahapaty or Mahamaya?

75
00:08:40,000 --> 00:08:48,000
Just because their relationship with the Buddha is older, this past is changed.

76
00:08:48,000 --> 00:08:51,000
And the end it all comes down to our perceptions.

77
00:08:51,000 --> 00:08:55,000
So if there's any lesson to be had for a meditator,

78
00:08:55,000 --> 00:09:04,000
it's to remind us that our reification of things is just conceptual

79
00:09:04,000 --> 00:09:12,000
and helping us to see that our perception of everything, of people and places and things,

80
00:09:12,000 --> 00:09:19,000
and our attachment to things is based on this cultivation of a perception

81
00:09:19,000 --> 00:09:23,000
that builds up over time and with attention.

82
00:09:23,000 --> 00:09:26,000
The more attention and the more focus we put on something,

83
00:09:26,000 --> 00:09:30,000
but if it's a person, then on our relationship,

84
00:09:30,000 --> 00:09:34,000
the stronger that perception becomes,

85
00:09:34,000 --> 00:09:38,000
you know, lifetime after lifetime after lifetime it builds up.

86
00:09:38,000 --> 00:09:42,000
And that's the only reason why we are the way we are,

87
00:09:42,000 --> 00:09:47,000
why we have the personality we have, not because we were born with it.

88
00:09:47,000 --> 00:09:50,000
It's much deeper than that, it's because we cultivate it,

89
00:09:50,000 --> 00:09:55,000
and we've cultivated it and continue cultivating it.

90
00:09:55,000 --> 00:10:01,000
And so meditation is very much a practice of refining and adjusting those habits,

91
00:10:01,000 --> 00:10:04,000
straightening out our personalities.

92
00:10:04,000 --> 00:10:09,000
We don't have to lose our personalities, just straighten them out.

93
00:10:09,000 --> 00:10:12,000
Then what we lose, what we remove are the ones that we seek

94
00:10:12,000 --> 00:10:19,000
clearly for ourselves without any reliance on others at all,

95
00:10:19,000 --> 00:10:27,000
and that as being harmful and stressful and a cause for suffering.

96
00:10:27,000 --> 00:10:32,000
But the deeper lesson, of course, comes from the verse as usual.

97
00:10:32,000 --> 00:10:37,000
And the lesson, I think, the main lesson from the verse,

98
00:10:37,000 --> 00:10:45,000
relates again to this difference between worldly pursuits and spiritual

99
00:10:45,000 --> 00:10:51,000
or holy religious pursuits.

100
00:10:51,000 --> 00:10:59,000
Because it relates to violence, it relates to activity,

101
00:10:59,000 --> 00:11:03,000
unrestrained activity with the body.

102
00:11:03,000 --> 00:11:10,000
You know, acting on impulse, acting on ambition, acting on desire.

103
00:11:10,000 --> 00:11:12,000
It's the way of the world.

104
00:11:12,000 --> 00:11:14,000
It's a way that gets you what you want.

105
00:11:14,000 --> 00:11:21,000
Very often, anger, violence.

106
00:11:21,000 --> 00:11:26,000
And by violence, not just physical violence, verbal violence, verbal conflict.

107
00:11:26,000 --> 00:11:28,000
Very often gets you what you want.

108
00:11:28,000 --> 00:11:36,000
A verbal manipulation, physical acts of manipulation, of coercion, of threat,

109
00:11:36,000 --> 00:11:38,000
of intimidation.

110
00:11:38,000 --> 00:11:41,000
Very often gets people what they want.

111
00:11:41,000 --> 00:11:44,000
And so the way of the world is one way.

112
00:11:44,000 --> 00:11:49,000
The way of the number is very different.

113
00:11:49,000 --> 00:11:52,000
And it's in stark contrast.

114
00:11:52,000 --> 00:11:56,000
Because the way of the world always, whether it relates to anger and violence

115
00:11:56,000 --> 00:12:04,000
or whether it relates to greed, which also has to do with violence,

116
00:12:04,000 --> 00:12:13,000
is ultimately about the reliance on certain situations to make you happy.

117
00:12:13,000 --> 00:12:15,000
We get what we want.

118
00:12:15,000 --> 00:12:17,000
We accomplish what we desire.

119
00:12:17,000 --> 00:12:23,000
We become proud and detached and identify with things.

120
00:12:23,000 --> 00:12:27,000
More and more, we become dependent and reliant on them.

121
00:12:27,000 --> 00:12:30,000
We suffer from the anger of the violence.

122
00:12:30,000 --> 00:12:38,000
We suffer from the greed, the desire, which stresses our taxes, our system.

123
00:12:38,000 --> 00:12:44,000
We suffer from the worry and the fear of other people's violence.

124
00:12:44,000 --> 00:12:52,000
And they're greed, the concern for losing what we have, from having it threatened.

125
00:12:52,000 --> 00:12:56,000
The more violent you are, whether it be through greed or anger,

126
00:12:56,000 --> 00:13:01,000
the more concerned you become, the more susceptible you become.

127
00:13:01,000 --> 00:13:06,000
Because you become familiar with loss, the concept of loss.

128
00:13:06,000 --> 00:13:14,000
Only someone who has something to lose suffers from loss.

129
00:13:14,000 --> 00:13:18,000
Only someone who is attached to the things that they have,

130
00:13:18,000 --> 00:13:23,000
or the things they might get, can suffer from losing or not getting what they want,

131
00:13:23,000 --> 00:13:28,000
what they like, what they want, what they need.

132
00:13:28,000 --> 00:13:31,000
And we suffer when we are, when it is taken away.

133
00:13:31,000 --> 00:13:41,000
When we are conquered, the conqueror who is conquered suffers more than the one who conquers not.

134
00:13:41,000 --> 00:13:54,000
We suffer when we are taken advantage of when we lose the things we want to others.

135
00:13:54,000 --> 00:13:57,000
We, the manipulator, are manipulated.

136
00:13:57,000 --> 00:14:01,000
The greedy one has things taken from them.

137
00:14:01,000 --> 00:14:03,000
This is how we suffer.

138
00:14:03,000 --> 00:14:11,000
So it's constantly in a state of uncertainty.

139
00:14:11,000 --> 00:14:18,000
And this is the essence of the Buddha's teaching on uncertainty, on a Nietzsche, on the impermanence.

140
00:14:18,000 --> 00:14:27,000
Or someone who relies upon this king of the hill, king of the castle, mentality,

141
00:14:27,000 --> 00:14:30,000
where you can rise to the top.

142
00:14:30,000 --> 00:14:32,000
Someone who rises to the top.

143
00:14:32,000 --> 00:14:35,000
Has that much further to fall?

144
00:14:35,000 --> 00:14:44,000
Is that much more susceptible to loss?

145
00:14:44,000 --> 00:14:55,000
And so it seems that the person who wins, who is victorious, that they have got it made.

146
00:14:55,000 --> 00:14:57,000
There is no way.

147
00:14:57,000 --> 00:15:01,000
There is no instance.

148
00:15:01,000 --> 00:15:16,000
There is no possibility for that to be lasting, permanent, stable, to be any sort of everlasting goal.

149
00:15:16,000 --> 00:15:29,000
Even putting aside death, you know, the ultimate, and to any accomplishment, any worldly accomplishment.

150
00:15:29,000 --> 00:15:36,000
The simple reliance on victory, you know, the simple frame of mind,

151
00:15:36,000 --> 00:15:45,000
is one that is unbalanced, requires this, is constantly stressed,

152
00:15:45,000 --> 00:15:48,000
constantly in a state of suffering.

153
00:15:48,000 --> 00:15:56,000
The person who gets what they want feels so happy, and thinks they are so happy.

154
00:15:56,000 --> 00:16:03,000
They are so full of greed and anger and delusion that they cannot see.

155
00:16:03,000 --> 00:16:06,000
They can't see while they are suffering.

156
00:16:06,000 --> 00:16:12,000
They can't see how tense and stressed and perverted their mind is how perverse the mind.

157
00:16:12,000 --> 00:16:20,000
A person who is perverse misses the fact that that perversion is a cause for suffering.

158
00:16:20,000 --> 00:16:26,000
So we have this example of worldly example of police brutality,

159
00:16:26,000 --> 00:16:33,000
and we have on the other side the example of protesters being angry and violent.

160
00:16:33,000 --> 00:16:43,000
And not to really get involved in that.

161
00:16:43,000 --> 00:16:50,000
Not to, sorry, not to equivalently, equivalently, not to create an equivalence.

162
00:16:50,000 --> 00:16:57,000
Because clearly there is something to be, to stand up and say something about.

163
00:16:57,000 --> 00:16:59,000
Clearly there is something wrong with the system.

164
00:16:59,000 --> 00:17:07,000
The system, the society that we have built for ourselves is based on greed.

165
00:17:07,000 --> 00:17:14,000
Because there is a problem with what might be called police brutality, and there is no question.

166
00:17:14,000 --> 00:17:21,000
That is an issue that is worth standing up for and speaking out about.

167
00:17:21,000 --> 00:17:34,000
That being said, stepping back and looking from the point of view of all who might wish for good things.

168
00:17:34,000 --> 00:17:39,000
Good things come from anger, from violence.

169
00:17:39,000 --> 00:17:46,000
Whether it be on the side of the police who think they are so good getting their way, they have the power.

170
00:17:46,000 --> 00:17:54,000
Not realizing how torturous it must be to be such a violent and cruel individual.

171
00:17:54,000 --> 00:17:57,000
Fearful, even.

172
00:17:57,000 --> 00:18:08,000
How much, there is apparently a great amount of trauma because they are under pressure, peer pressure from other police officers from their superiors and so on.

173
00:18:08,000 --> 00:18:24,000
And on the other side of the protesters who have such a noble aspiration to end racism, to end discrimination, to end cruelty, brutality, to end evil states of mine.

174
00:18:24,000 --> 00:18:32,000
Ultimately, they may not phrase it, they may not realize it, ultimately it comes down to evil states of mine.

175
00:18:32,000 --> 00:18:38,000
Without the evil states of mine, the unwholesome, the states of mine that cause stress and suffering.

176
00:18:38,000 --> 00:18:46,000
Without those mine states, there would be no tension or there would be no conflict.

177
00:18:46,000 --> 00:18:54,000
And so, as a result, it is self-defeating to increase and to cultivate anger.

178
00:18:54,000 --> 00:18:59,000
It may get us what we want, it may even be the right solution.

179
00:18:59,000 --> 00:19:06,000
It may even be the solution in the sense that in a worldly level it has changed things for the better.

180
00:19:06,000 --> 00:19:13,000
But as a rule, and for our own spiritual health, for our own mental health and wellbeing,

181
00:19:13,000 --> 00:19:17,000
which ultimately is what it's all about.

182
00:19:17,000 --> 00:19:26,000
For our own spiritual mental health, mental wellbeing, we can't succumb to anger and violence.

183
00:19:26,000 --> 00:19:36,000
Ultimately, we're self-defeating because if the goal is to be achieved with anger and violence,

184
00:19:36,000 --> 00:19:42,000
then what kind of a goal is it? What is the result? If that's the outcome, then we haven't really fixed anything.

185
00:19:42,000 --> 00:19:54,000
It's not a world worth living in, even if it does change.

186
00:19:54,000 --> 00:19:59,000
So, that's on the side of...

187
00:19:59,000 --> 00:20:07,000
Well, that's the disadvantages to that, and in opposition, we have the way of the Dhamma,

188
00:20:07,000 --> 00:20:12,000
one who is non-violent, one who is controlled.

189
00:20:12,000 --> 00:20:21,000
Kainasamutta, which means controlled in body, but the commentary says it doesn't just relate to body.

190
00:20:21,000 --> 00:20:24,000
It's actually body, speech and mind, of course.

191
00:20:24,000 --> 00:20:31,000
The Buddha wasn't just praising physical, restrained.

192
00:20:31,000 --> 00:20:40,000
Kaya, Kaya often just means us in the being, so it refers to all three.

193
00:20:40,000 --> 00:20:43,000
This way is very different.

194
00:20:43,000 --> 00:20:45,000
You know, we might put up an equivalence and say,

195
00:20:45,000 --> 00:20:53,000
well, what about you? Don't you? Aren't you attached to your way as well or something?

196
00:20:53,000 --> 00:21:00,000
We don't want to think that there might be one thing better than another one way of life better than another.

197
00:21:00,000 --> 00:21:08,000
Meaning, we are quite often put up these false equivalences between different things.

198
00:21:08,000 --> 00:21:15,000
So, we have to point out the stark contrast between these two ways.

199
00:21:15,000 --> 00:21:23,000
One is always inclining, always reaching, and the other is never inclining, and never reaching.

200
00:21:23,000 --> 00:21:28,000
Never pushing and pulling the Buddha said, never coming or going.

201
00:21:28,000 --> 00:21:35,000
Where there is no coming and no going.

202
00:21:35,000 --> 00:21:43,000
And so, the issue of impermanence, of instability, is solved.

203
00:21:43,000 --> 00:21:57,000
Why, the second part of the verse talks about permanence, a chutat, chutabhins falling away, a chutabhin, never falling away.

204
00:21:57,000 --> 00:22:01,000
You can never fall away from it because it's at the bottom already.

205
00:22:01,000 --> 00:22:04,000
It's at the base already.

206
00:22:04,000 --> 00:22:14,000
If something desirable comes, no matter how desirable, something desirable goes.

207
00:22:14,000 --> 00:22:19,000
If something undesirable comes, it makes no difference.

208
00:22:19,000 --> 00:22:28,000
It is a way of being, a way of relating to reality, that is inherently, by its very nature,

209
00:22:28,000 --> 00:22:44,000
without any cause for doubt or uncertainty, stable, consistent, unshakable, invincible.

210
00:22:44,000 --> 00:22:51,000
The question as to whether it's such a state as possible or really exists.

211
00:22:51,000 --> 00:22:57,000
Well, that's the question that Buddhism seeks to answer and to demonstrate.

212
00:22:57,000 --> 00:22:59,000
And that's where meditation comes in.

213
00:22:59,000 --> 00:23:08,000
Our practice of mindfulness, the question that often arises in people, why are we repeating to ourselves?

214
00:23:08,000 --> 00:23:17,000
Something like pain, pain, or for angry, say angry, angry, if we want something wanting, why do we say to ourselves?

215
00:23:17,000 --> 00:23:18,000
Why are we doing this?

216
00:23:18,000 --> 00:23:26,000
Why are we reminding ourselves about these mundane realities?

217
00:23:26,000 --> 00:23:27,000
And this is the answer.

218
00:23:27,000 --> 00:23:35,000
We're trying to cultivate a state of objectivity where we relate to things just as they are.

219
00:23:35,000 --> 00:23:48,000
Because our actions, just like the actions of anyone who gets angry and violent or greedy and covetous and ambitious and so on,

220
00:23:48,000 --> 00:23:55,000
our habits have power and influence, they change us.

221
00:23:55,000 --> 00:24:11,000
And if we cultivate this state of objectivity where we remember things, where we remember, oh yeah, I'm not going to forget that that's all, that's just pain.

222
00:24:11,000 --> 00:24:16,000
Because if you forget that that's just pain, you will get upset about it.

223
00:24:16,000 --> 00:24:21,000
When you see delicious food, for example, I'm not going to forget that that's seeing.

224
00:24:21,000 --> 00:24:30,000
Because if I forget, I will get caught up in all these concepts, oh, I know what that tastes like and that's something I like.

225
00:24:30,000 --> 00:24:34,000
When you're angry, I'm not going to forget that that's anger.

226
00:24:34,000 --> 00:24:47,000
Because if I forget, if I lose track of the experience, I will get caught up by the anger, by the greed, I will become violent.

227
00:24:47,000 --> 00:25:00,000
I will become covetous, I will be manipulative, I will do all sorts of things to get what I want and to remove what I don't want and so on.

228
00:25:00,000 --> 00:25:06,000
This is what it means to be restrained, some Buddha.

229
00:25:06,000 --> 00:25:10,000
It doesn't mean you have to force yourself in any way, really.

230
00:25:10,000 --> 00:25:20,000
It means you have to keep the mind with the experience, keep the mind in the reality of the experience.

231
00:25:20,000 --> 00:25:31,000
Not getting caught up in our reactions and our perceptions of things.

232
00:25:31,000 --> 00:25:44,000
So when the Buddha talks about this, a jutatana, a jutantana, yet a gantuan, a sultur, a sulturae, where one sorrows not.

233
00:25:44,000 --> 00:25:59,000
He's talking about at its base, this state, this way of life, way of being, that one comes to through training and through practice,

234
00:25:59,000 --> 00:26:10,000
of non-reactivity, of non-judgment, of simple, true and pure experience of reality as it is.

235
00:26:10,000 --> 00:26:23,000
Experience of this, of what we are all experiencing, but experiencing it purely and truly, and without judgment or reactivity.

236
00:26:23,000 --> 00:26:40,000
Because that is the way of life, the way of practice, that is the way that carries on to eternity, that has no falling back.

237
00:26:40,000 --> 00:26:56,000
There is no tension in it, there is no opposition. When you strive and strive and strive to get something, you build up and build up this attachment to it.

238
00:26:56,000 --> 00:27:07,000
When you live your life in objectivity, in clear awareness of things like that, there is no build up like that.

239
00:27:07,000 --> 00:27:28,000
There is a shedding off, there is a freeing, and so the result, rather than a building up and a building up of some formation that is going to topple down, there is the deconstruction, and there is a reduction in needs and attachment.

240
00:27:28,000 --> 00:27:38,000
And the ultimate destination, the ultimate result, is freedom from suffering.

241
00:27:38,000 --> 00:27:54,000
In a way, very different from any conception of freedom from suffering that we might have in a worldly sense, because it is independent, but there is nothing to do with some sorrow or our experiences of this or that.

242
00:27:54,000 --> 00:28:09,000
And so it is free from any of the uncertainty and the impermanence, the unpredictability of some sorrow of the world.

243
00:28:09,000 --> 00:28:21,000
And so there is no fear of sorrow from one who has gone to this place.

244
00:28:21,000 --> 00:28:26,000
That's the Dhampanda for today. Thank you all for listening.

