1
00:00:00,000 --> 00:00:12,960
Okay, so this area is the place where the Buddha, the Bodhisattva was born.

2
00:00:12,960 --> 00:00:16,840
I was trying to figure out if you could say that the Buddha was born here, and of course

3
00:00:16,840 --> 00:00:22,920
it's all semantics, but it is a point of technicality that he wasn't the Buddha when

4
00:00:22,920 --> 00:00:29,920
he was born here, but the commentaries talk about two births of the Buddha.

5
00:00:29,920 --> 00:00:37,640
And one is the birth of Rupakaya, and the other is the birth of Damakaya, and this is

6
00:00:37,640 --> 00:00:45,880
the place of the birth of the Rupakaya, the Buddha's physical manifestation or physical

7
00:00:45,880 --> 00:00:48,640
form.

8
00:00:48,640 --> 00:00:54,560
The second birth is at Bodhyaya where we'll be heading, and it's the birth of the Damak, the

9
00:00:54,560 --> 00:01:00,840
actual Buddha-ness of the Buddha.

10
00:01:00,840 --> 00:01:06,360
So because his Rupakaya was born before the Damakaya, you could say this is the birth

11
00:01:06,360 --> 00:01:14,600
place of the Buddha, in the Rupakaya.

12
00:01:14,600 --> 00:01:23,920
In some ways it's the least inspiring of the four places for some, okay, in the sense

13
00:01:23,920 --> 00:01:31,280
it might be why are we here, or what should we feel when we're here?

14
00:01:31,280 --> 00:01:35,280
Because again it was just the Rupakaya, and you can say, well the Buddha was born, but

15
00:01:35,280 --> 00:01:38,320
he wasn't a Buddha yet.

16
00:01:38,320 --> 00:01:46,960
And in some sense, it's not even that remarkable because it's simply the very last bit

17
00:01:46,960 --> 00:01:56,840
of a very long and arduous and difficult and incredible journey.

18
00:01:56,840 --> 00:02:09,680
So the last life is just the very last bit of it when the whole journey is quite incredible.

19
00:02:09,680 --> 00:02:16,280
In terms of birth, there is something quite important about the fact that this was the

20
00:02:16,280 --> 00:02:22,600
Buddha's last birth, this was it, this was the culmination of births.

21
00:02:22,600 --> 00:02:33,480
So then I talk a little bit about birth, make it a little deeper for us, but also highlight

22
00:02:33,480 --> 00:02:43,280
how incredible and special the Buddha's last birth was, this last life.

23
00:02:43,280 --> 00:02:53,040
So there are, besides being born as a Buddha, there are six, birth, six types of birth.

24
00:02:53,040 --> 00:02:59,200
Birth as in hell.

25
00:02:59,200 --> 00:03:04,840
Birth as a ghost.

26
00:03:04,840 --> 00:03:09,400
Birth in the animal realm.

27
00:03:09,400 --> 00:03:12,600
Birth as a human.

28
00:03:12,600 --> 00:03:18,520
Birth as a daywa and birth as a brahma.

29
00:03:18,520 --> 00:03:25,200
Those are the six types of birth.

30
00:03:25,200 --> 00:03:36,360
If we're talking in terms of what birth means, all six of those types of birth are under

31
00:03:36,360 --> 00:03:41,320
the category of conventional births or conceptual birth.

32
00:03:41,320 --> 00:03:44,680
Because of course the mind is born and dies every moment.

33
00:03:44,680 --> 00:03:49,960
So you can say there are two types of birth, and all of those six are one type.

34
00:03:49,960 --> 00:03:58,720
Because we're talking about them, it's worth reminding ourselves about the nature of reality

35
00:03:58,720 --> 00:04:06,000
that our existence as human beings is contrived, it's nothing special.

36
00:04:06,000 --> 00:04:15,520
This isn't the de facto birth, where we have ten fingers and ten toes and two arms, two

37
00:04:15,520 --> 00:04:20,880
legs, and many other types of birth.

38
00:04:20,880 --> 00:04:35,840
To be born in hell, if you're full of anger, then your birth becomes a birth in a realm

39
00:04:35,840 --> 00:04:44,280
of great suffering, because your inclination of mind is to cause suffering to yourself

40
00:04:44,280 --> 00:04:45,280
and to others.

41
00:04:45,280 --> 00:04:49,800
Of course it's very painful to be angry, but it's also the cause of greatest cause of

42
00:04:49,800 --> 00:04:53,600
harm to others.

43
00:04:53,600 --> 00:05:02,680
And so the birth that comes from anger is birth in hell.

44
00:05:02,680 --> 00:05:12,840
Just as a ghost comes from greed, the person has a great amount of greed, intense greed,

45
00:05:12,840 --> 00:05:26,120
stingy, miserly clingy, then the birth that comes from that is birth as a ghost.

46
00:05:26,120 --> 00:05:31,960
The person has a great amount of delusion, then the birth that comes from that is birth

47
00:05:31,960 --> 00:05:36,680
as an animal.

48
00:05:36,680 --> 00:05:43,840
These are important conceptually to give us a sort of a broader picture of the nature

49
00:05:43,840 --> 00:05:48,120
of reality, the nature of defilements.

50
00:05:48,120 --> 00:05:56,640
Give us a sense of an important distinction among mind states and among beings.

51
00:05:56,640 --> 00:06:06,280
As you see humans, they have words in Pali, like Manusapeto, and a human who is like a ghost,

52
00:06:06,280 --> 00:06:09,440
hungry, never satisfied.

53
00:06:09,440 --> 00:06:14,880
The stories that we hear about ghosts are beings who are stuck on a place, some place

54
00:06:14,880 --> 00:06:16,120
that was very important to them.

55
00:06:16,120 --> 00:06:24,400
So the intense clinging kept them there when they passed away, but they're always wanting,

56
00:06:24,400 --> 00:06:28,320
they're always yearning for something.

57
00:06:28,320 --> 00:06:36,160
And we have many ghost stories, stories of ghosts, descriptions of ghosts, and Buddhism.

58
00:06:36,160 --> 00:06:42,840
But you can see humans who are like that, never satisfied, drug addicts who get example.

59
00:06:42,840 --> 00:06:52,120
You see humans, womanusan, nirayiko, I think, something like that, someone who is a human

60
00:06:52,120 --> 00:07:00,040
but a hellish, like a hell-being, and you've ever gotten really angry, you know what that's

61
00:07:00,040 --> 00:07:01,040
like.

62
00:07:01,040 --> 00:07:03,680
It's hell to be so angry.

63
00:07:03,680 --> 00:07:11,240
You've never seen people who are so angry, they look like demons, so we all have that

64
00:07:11,240 --> 00:07:20,680
in us when a person who is intent upon that, it's most likely to be very born in hell.

65
00:07:20,680 --> 00:07:26,480
If they, if it's their life, you know, angry about everything, mostly mean and cruel, you

66
00:07:26,480 --> 00:07:27,480
know.

67
00:07:27,480 --> 00:07:30,960
Honestly, it doesn't mean just because we're angry or greedy that we're likely to go to

68
00:07:30,960 --> 00:07:31,960
these places.

69
00:07:31,960 --> 00:07:38,840
You really need to be breaking the five percent, killing and stealing and so on.

70
00:07:38,840 --> 00:07:42,080
And the same with the animal realm.

71
00:07:42,080 --> 00:07:50,120
You see humans who are so deluded, intent upon remaining ignorance with no interest in higher

72
00:07:50,120 --> 00:07:52,320
qualities.

73
00:07:52,320 --> 00:07:58,280
Humans who are just like the cows that we see, content to eat and ruminate and ruminate

74
00:07:58,280 --> 00:08:03,400
is that the way?

75
00:08:03,400 --> 00:08:11,120
Content to chew their cud, as it were.

76
00:08:11,120 --> 00:08:15,040
Being a cow isn't the worst, I don't think, but if real delusion, you get born in some

77
00:08:15,040 --> 00:08:20,640
of the bad realms, bad animal realms, the cow can be terrible if you get slaughtered,

78
00:08:20,640 --> 00:08:28,520
but the life isn't so bad, it's quite peaceful compared to say being born as a rabbit,

79
00:08:28,520 --> 00:08:39,000
being hunted by wolves, a mouse being hunted by cats, a squirrel, or animals that fight

80
00:08:39,000 --> 00:08:51,720
each other, cats that fight each other, dogs, wolves that fight and kill each other.

81
00:08:51,720 --> 00:08:52,720
That's three.

82
00:08:52,720 --> 00:09:00,000
Being born as a human, even if you have greed, anger, delusion, being born as a human,

83
00:09:00,000 --> 00:09:03,560
requires something along the lines of keeping the five precepts.

84
00:09:03,560 --> 00:09:06,720
That's why the five precepts are enumerated like that.

85
00:09:06,720 --> 00:09:16,080
These are the five sort of general concepts that delineates birth in a good realm and

86
00:09:16,080 --> 00:09:21,240
birth in a, not so good, not good realm.

87
00:09:21,240 --> 00:09:29,240
The born as a human, if you keep the five precepts and are generally committed as an individual,

88
00:09:29,240 --> 00:09:33,360
then the birth that comes from that is a human.

89
00:09:33,360 --> 00:09:39,040
You know, it's more granular than that, you can say, because your experience is, if you

90
00:09:39,040 --> 00:09:44,400
get angry at someone, you're going to get into hell on earth, you're mean and cruel to

91
00:09:44,400 --> 00:09:46,000
people and so on.

92
00:09:46,000 --> 00:09:50,800
If on the other hand, you're keeping the precepts, you'll find you live a fairly civilized

93
00:09:50,800 --> 00:09:51,800
life.

94
00:09:51,800 --> 00:09:55,720
You don't get caught up with drunk, drunkards and murderers and thieves and liars and

95
00:09:55,720 --> 00:09:56,720
so on.

96
00:09:56,720 --> 00:09:59,120
It feels fairly human, you mean, right?

97
00:09:59,120 --> 00:10:09,280
It would be a good word.

98
00:10:09,280 --> 00:10:17,720
The born as an angel, born as an angel requires not just keeping the five precepts, but

99
00:10:17,720 --> 00:10:23,520
it requires something we call Mahakusa, goodness.

100
00:10:23,520 --> 00:10:28,000
So being humane is one thing, treating other people as you like to be treated, that sort

101
00:10:28,000 --> 00:10:30,880
of thing is good.

102
00:10:30,880 --> 00:10:36,240
But people who go above and beyond are the ones who are born as angels, people who are

103
00:10:36,240 --> 00:10:37,640
intent on purification.

104
00:10:37,640 --> 00:10:44,640
I think a lot of Buddhist meditators, it's a general concept, a conception that Buddhist

105
00:10:44,640 --> 00:10:47,760
meditators tend to go to heaven because they're intent.

106
00:10:47,760 --> 00:10:49,480
I mean, that's the whole thrust of Buddhism.

107
00:10:49,480 --> 00:10:53,320
It's not to believe in this or believe in that, believe in the Buddha.

108
00:10:53,320 --> 00:10:54,320
What is the core of Buddhism?

109
00:10:54,320 --> 00:10:57,960
It's really about purifying your mind.

110
00:10:57,960 --> 00:11:03,800
And so I don't think it's bragging or kind of, you know, conceit to think that Buddhism

111
00:11:03,800 --> 00:11:08,240
is Buddhist or more likely to go to heaven because we're intent upon it.

112
00:11:08,240 --> 00:11:12,080
We don't have these views like you do this and you go to heaven, you do that and you

113
00:11:12,080 --> 00:11:14,120
go to heaven.

114
00:11:14,120 --> 00:11:18,440
You act heavenly and you go to heaven.

115
00:11:18,440 --> 00:11:24,440
And a lot, it's very much, in many ways, in line with the Buddhist teaching, some of

116
00:11:24,440 --> 00:11:33,120
the heavenly stuff that some of the things that would lead to heaven, you know, things

117
00:11:33,120 --> 00:11:45,320
like we're announcing entertainment and sensuality, eating only in the morning, it's all

118
00:11:45,320 --> 00:11:50,960
of you are doing that sort of thing, sort of dedicated, you're not doing it to torture

119
00:11:50,960 --> 00:11:55,360
yourself or because the Buddha said or because you want to feel good about yourself, you're

120
00:11:55,360 --> 00:11:59,520
doing it because you understand that it's a good training, it's really useful and it helps

121
00:11:59,520 --> 00:12:03,480
you purify your mind, that sort of thing leads to heaven.

122
00:12:03,480 --> 00:12:06,000
But many non-Buddhist, of course, also go to heaven.

123
00:12:06,000 --> 00:12:11,200
People who are very kind and charitable, they don't have to, they're not just doing their

124
00:12:11,200 --> 00:12:17,160
ordinary duty as a human being, but they intend to go out of their way to help people who

125
00:12:17,160 --> 00:12:24,160
are not well-off, to be kind, to think good thoughts, to people who practice Mehta, Babana,

126
00:12:24,160 --> 00:12:33,000
that sort of thing, Mahakusana, Dana, Sila, Babana, even just keeping Sila, someone who is

127
00:12:33,000 --> 00:12:35,960
said or not lying, I'm never going to lie.

128
00:12:35,960 --> 00:12:42,360
I think there was a story of someone who went to heaven because of that, maybe a man, keeping

129
00:12:42,360 --> 00:12:48,680
the eight precepts is a good sort of example.

130
00:12:48,680 --> 00:12:52,880
To be born as a Brahma, which is the Buddhist equivalent of God or the closest thing we

131
00:12:52,880 --> 00:12:59,800
have to God's, only because they're different than angels, they don't engage in sensuality,

132
00:12:59,800 --> 00:13:02,280
they're more high-minded, more lofty.

133
00:13:02,280 --> 00:13:12,120
To get there you have to practice the Samatha Jana, so a person who is intent upon entering

134
00:13:12,120 --> 00:13:18,480
into states of absorption that are one-pointed, that are out of any sensuality, so they don't

135
00:13:18,480 --> 00:13:23,880
have any experience of the world, they're just, the whole world is one thing, one concept,

136
00:13:23,880 --> 00:13:36,320
maybe it's Mehta, could be, maybe it's a candle flame, different things like that.

137
00:13:36,320 --> 00:13:42,920
And again, all three of these things keep going to heaven or becoming a God, you can think

138
00:13:42,920 --> 00:13:49,120
of the more granular as well, and it helps you to understand and get some kind of confidence

139
00:13:49,120 --> 00:13:53,320
that, yeah, it makes sense that that would happen, that's how birth would occur, because

140
00:13:53,320 --> 00:13:59,920
you can see it in this life, unless you have the view that when you die consciousness

141
00:13:59,920 --> 00:14:05,320
ceases, you can see in this life, a person who is kind and generous, maybe bad things

142
00:14:05,320 --> 00:14:10,800
happen to them, we'd say from bad karma, we'd try to excuse it off by that, but so many

143
00:14:10,800 --> 00:14:17,200
good things happen to them, and so many people love them, and appreciate them, and a person

144
00:14:17,200 --> 00:14:21,160
who is intent upon Samatha meditation, they're so fixed and focused, it doesn't matter

145
00:14:21,160 --> 00:14:28,440
if they get harassed by others, because they're high-minded, Brahma, so very high-minded

146
00:14:28,440 --> 00:14:32,520
state.

147
00:14:32,520 --> 00:14:40,760
But none of these six forms of birth comes close to birth as a Buddha, of course.

148
00:14:40,760 --> 00:14:44,520
So the birth of the Buddha, the Buddha, when after he became enlightened, he was asked,

149
00:14:44,520 --> 00:14:53,640
he walked from where he said he walked to Sarnat to Isipatana, Deer Park, and on his way

150
00:14:53,640 --> 00:14:58,960
he met a man who asked him, what are you, he saw, oh, this guy is kind of special, and

151
00:14:58,960 --> 00:15:04,440
he said, what are you, are you a human, are you a god, are you an angel, are you a Mara,

152
00:15:04,440 --> 00:15:09,280
I don't know what exactly he has, all these different things, what are you?

153
00:15:09,280 --> 00:15:15,000
In the Buddha roundabout way, he said, I've learned what needs to be learned, I've conquered

154
00:15:15,000 --> 00:15:20,200
all the universe, I mean you're very lofty sort of lion's roar kind of thing, and then

155
00:15:20,200 --> 00:15:25,120
he said, I have no teacher, there's no one like me, there's no one parallel to me, no

156
00:15:25,120 --> 00:15:31,920
one above me, I am a samma sam Buddha, so he differentiated, he didn't answer, I'm a human

157
00:15:31,920 --> 00:15:37,280
or a god, and some people say, you know, he didn't, he isn't, wasn't really a human,

158
00:15:37,280 --> 00:15:43,600
he was a Buddha, I think you can say he was a human, it's pretty clear that to some extent

159
00:15:43,600 --> 00:15:49,760
he was a human, but it's also proper to say he was simply a Buddha, or a samma

160
00:15:49,760 --> 00:15:56,480
sam Buddha.

161
00:15:56,480 --> 00:16:09,880
So we take this as an important event, the birth of the Buddha, it was the story, there's

162
00:16:09,880 --> 00:16:18,640
an encapsulated sense of, when he was born here in Lumini, something important happened.

163
00:16:18,640 --> 00:16:28,840
This was the beginning of the Buddha's story, you often hear it told this way, or as

164
00:16:28,840 --> 00:16:37,600
Buddhist meditators, when we think of birth, to some extent we have to look at all of these

165
00:16:37,600 --> 00:16:41,240
places in our whole pilgrimage in a different way, and that's I think what we're all kind

166
00:16:41,240 --> 00:16:49,760
of struggling with is maybe too strong of a word, but grappling with trying to align ourselves

167
00:16:49,760 --> 00:16:57,520
in the right way, because we see that simply being excited to go to see, oh, this was

168
00:16:57,520 --> 00:17:03,720
the Buddha, it's not really where it's at, it's not the way to be free from suffering,

169
00:17:03,720 --> 00:17:09,360
or to follow the Buddha's teachings, either, to put it more strongly, that the Buddha

170
00:17:09,360 --> 00:17:16,360
did, and then tend for us to go tour a sting, and then tend for us to come here and think,

171
00:17:16,360 --> 00:17:22,280
oh, I'm excited to be there.

172
00:17:22,280 --> 00:17:29,480
So we have to think about birth, ultimately birth comes down to momentary birth, every time

173
00:17:29,480 --> 00:17:39,240
an emotion arises, that's the birth of that emotion, and suffering arises, when the

174
00:17:39,240 --> 00:17:45,760
pyramins arise, it was most important about the Buddha is that he gave birth to what we

175
00:17:45,760 --> 00:17:52,800
call the pyramitans, the ten perfection, he gave birth to Donna, charity, he gave away his

176
00:17:52,800 --> 00:18:00,640
own eyes once, what it really means is he gave up any sense of possessiveness, any sense

177
00:18:00,640 --> 00:18:06,160
of self, and this was a big part of why he was able to finally see non-self and have a sense

178
00:18:06,160 --> 00:18:18,720
that reality doesn't emit a possession and ego and so on, because he sacrificed, he

179
00:18:18,720 --> 00:18:26,720
made a determination to not take possession of things, to not claim ownership of things.

180
00:18:26,720 --> 00:18:35,840
Seela, he was intent on morality, ethics, he wasn't always a monk, or an ascetic

181
00:18:35,840 --> 00:18:42,840
even, had children, he broke even, maybe the five precepts sometimes, but for the most

182
00:18:42,840 --> 00:18:49,840
part, actually I think you'd be hard pressed to find even an example, and some people

183
00:18:49,840 --> 00:18:55,800
might say, maybe the truth, maybe the orthodox idea is that he kept the five precepts

184
00:18:55,800 --> 00:19:05,200
until, until I can't think of an instance where he broke them, there was something about

185
00:19:05,200 --> 00:19:12,480
him being a thief, but I'm not sure about that, anyway, and what he developed from the

186
00:19:12,480 --> 00:19:21,280
time of Deepankaran, the time of his last birth, and it profound sense of ethics, whereby

187
00:19:21,280 --> 00:19:30,040
even to save his own life, he wouldn't break, he wouldn't do something immoral, even

188
00:19:30,040 --> 00:19:35,400
to save his own life to save someone else's life. A lot of it being beyond our ordinary

189
00:19:35,400 --> 00:19:42,000
sense of ethics or even charity or any of the perfections, because he knew that he had

190
00:19:42,000 --> 00:19:48,880
a broader sense of trying to keep people from suffering, maybe if I kill someone, it'll

191
00:19:48,880 --> 00:19:54,080
help someone else or a lot of other people. He had a deeper understanding of it all being

192
00:19:54,080 --> 00:20:02,960
some sara, and you can't really free people from their own suffering, because they pass away

193
00:20:02,960 --> 00:20:08,440
and they're born again. The only way would be to purify his mind, to find the way out,

194
00:20:08,440 --> 00:20:13,320
and then he could help people, because there was no way out, there's no way to fix people's

195
00:20:13,320 --> 00:20:16,720
problems or even your own problem.

196
00:20:16,720 --> 00:20:21,320
They come up out of me, they were enunciation, many times he gave up, kingship, he gave

197
00:20:21,320 --> 00:20:29,240
up wealth, renunciation, panya para me, there's so many stories you hear, there's a very

198
00:20:29,240 --> 00:20:35,040
good story, the Mahamamaga Jataka, where he was even at seven years old, he was solving

199
00:20:35,040 --> 00:20:40,720
people's problems for them, and the wisdom that he displayed, but his appreciation and

200
00:20:40,720 --> 00:20:48,400
his dedication to understanding, to leaving home and finding, understanding his own mind

201
00:20:48,400 --> 00:20:58,040
and understanding the world. We lay up, borrow me, effort, one time the Bodhisatta was

202
00:20:58,040 --> 00:21:04,000
shipwrecked, and he said, he just swam across the ocean, and the angel and the ocean said,

203
00:21:04,000 --> 00:21:06,680
what are you doing? You're never going to make it to the engine, I did the ocean, you're

204
00:21:06,680 --> 00:21:12,440
in the middle of the ocean, and he said, tell me what can't be, I don't know, he said,

205
00:21:12,440 --> 00:21:17,480
one time the Buddha said, tell me what can't be achieved by striving. He said, if I don't

206
00:21:17,480 --> 00:21:25,720
try it, then I'm lost. That story, that's Mahajana. It's a very famous Jataka, the angel

207
00:21:25,720 --> 00:21:30,680
picked them up and took him to the edge of the ocean. It was quite impressed by his dedication

208
00:21:30,680 --> 00:21:37,480
with the Buddha had never gave up, that's a good way of summing it up. His effort

209
00:21:37,480 --> 00:21:41,280
is effort mean, he never gave up, and that's an example, when we talk about effort

210
00:21:41,280 --> 00:21:46,120
and meditation courses, something you have to work really hard is that when you fail,

211
00:21:46,120 --> 00:21:51,560
when you feel like you just can't do it, try again. And if you have that capacity to try

212
00:21:51,560 --> 00:21:58,880
again, when you feel like you just can't do it anymore, that's effort.

213
00:21:58,880 --> 00:22:02,720
Kanti patients, well, we learn all about that in meditation, but the Buddha's patient,

214
00:22:02,720 --> 00:22:09,000
he had his ears cut off, his nose cut off, one life, his hands cut off. This king was

215
00:22:09,000 --> 00:22:16,000
very angry and jealous and thought he was stealing his women cut off all his slivers. And

216
00:22:16,000 --> 00:22:22,080
he said, what do you think? Do you think my patients is in my ears? I said, my patients

217
00:22:22,080 --> 00:22:30,240
is deeper than that. And he died patient in that life.

218
00:22:30,240 --> 00:22:35,840
Such a part of me in the Buddha was such a great, such a, such a, you could see it at the

219
00:22:35,840 --> 00:22:44,560
end, such a means being true to yourself, having true to a vision, being true to an intention.

220
00:22:44,560 --> 00:22:47,600
So when Mara came in the Buddha was seated under the Bodhi tree and he said, you don't

221
00:22:47,600 --> 00:22:51,440
deserve to be there. The Buddha said, I do deserve to be there. And the earth is my witness.

222
00:22:51,440 --> 00:23:00,440
And he touched the earth. And Mara just got thrown away, got blown away by the water.

223
00:23:00,440 --> 00:23:05,560
It was such a, it was the power of, you can say, you know, I don't deserve it all you want.

224
00:23:05,560 --> 00:23:12,280
And the Buddha's, the, the, the power of, I do deserve to sit here. That was true. It was

225
00:23:12,280 --> 00:23:17,920
very powerful. There's many examples of monks and, and the stories and lay people using

226
00:23:17,920 --> 00:23:25,880
truth. Angulimala is one example. Aditana, when the Buddha made a determination to become

227
00:23:25,880 --> 00:23:30,480
a Buddha and then never wavered, when he made a determination for his bowl to float upstream,

228
00:23:30,480 --> 00:23:35,320
which can't happen, you know, right, bowls don't float upstream. He made a determination

229
00:23:35,320 --> 00:23:45,680
that it should, and it did, made that power of me. The Buddha's friendliness is the Bodhisattas,

230
00:23:45,680 --> 00:23:52,160
cultivation of friendliness, even when people were angry at him. And equanimity, how he didn't

231
00:23:52,160 --> 00:24:00,680
look at one person or another as greater or lesser. He never, he'd cultivated the capacity

232
00:24:00,680 --> 00:24:07,760
to not be attached and partial. These are the ten parameters. These are the things that

233
00:24:07,760 --> 00:24:16,480
the Buddha gave birth to. Of course, he also gave birth to suffering, as we all do. Birth

234
00:24:16,480 --> 00:24:23,960
as a human is birth of suffering, and birth of things that can cause you suffering. So,

235
00:24:23,960 --> 00:24:30,200
a part of our practice is to be aware of birth when it occurs, meaning experience. When

236
00:24:30,200 --> 00:24:35,200
an experience arises, doesn't need to cause you suffering, even though you could argue,

237
00:24:35,200 --> 00:24:41,080
whatever. It doesn't need to cause you suffering unless you cling to it, unless you get caught

238
00:24:41,080 --> 00:24:47,040
up in it. And so, we're very much concerned about birth. We're also concerned about death,

239
00:24:47,040 --> 00:24:54,320
which is something we can talk about at the next place, maybe. But we're concerned about

240
00:24:54,320 --> 00:25:00,680
the birth of experiences. When something comes up, the birth of problems, the birth of anything,

241
00:25:00,680 --> 00:25:04,400
because we're concerned about how we are going to relate to it. It's something that's

242
00:25:04,400 --> 00:25:10,360
very important to us. You feel a pain, that something was born, and that's very important

243
00:25:10,360 --> 00:25:15,280
because how you deal with that. That thing could cause you a lot of suffering if you let

244
00:25:15,280 --> 00:25:23,880
it. Someone says something to you. Birth of sound. That sound was born. That's a very

245
00:25:23,880 --> 00:25:28,920
important thing, because that sound could cause you to do all sorts of things. We taste

246
00:25:28,920 --> 00:25:39,000
food, it can cause you. Birth of experience is a very important thing. So, we look at the

247
00:25:39,000 --> 00:25:44,040
world like this experiences, and that's where birth, that's the second type of birth. Birth

248
00:25:44,040 --> 00:25:55,120
is in God or in Angel or Human, in Hell or so on. That's conventional. We look at these

249
00:25:55,120 --> 00:26:04,120
two types of birth. We come here, it's a good reason to talk about them both. But even

250
00:26:04,120 --> 00:26:11,520
on a conventional level, putting them both together, I guess, we come here to celebrate

251
00:26:11,520 --> 00:26:17,760
the Buddha's birth, in the conventional sense, as a means of giving birth ourselves to wholesome

252
00:26:17,760 --> 00:26:27,360
qualities of reverence and respect and appreciation. We come here to revere this very monumentous

253
00:26:27,360 --> 00:26:33,560
event where the Buddha was finally born in his last life, where he would become the Buddha.

254
00:26:33,560 --> 00:26:38,600
He would give rise to the Dhamma, give birth to the Dhamma, and the Dhamma would be born,

255
00:26:38,600 --> 00:26:44,960
and then the Sangha would be born. And we would all be able to be here, to have this

256
00:26:44,960 --> 00:26:57,480
precious Dhamma. So, some thoughts on Lumbini. We are here now. That's the pond where

257
00:26:57,480 --> 00:27:03,560
she bathed and behind that is the pillar, and that big building has apparently the exact spot

258
00:27:03,560 --> 00:27:28,600
where the Buddha was born.

