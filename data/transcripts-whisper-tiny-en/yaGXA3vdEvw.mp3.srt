1
00:00:00,000 --> 00:00:03,800
Hello and welcome back to our study of the Namapada.

2
00:00:03,800 --> 00:00:10,720
Today we continue on with 1st number 67, which reads as follows.

3
00:00:10,720 --> 00:00:23,520
Nattankamang kattang sadhu, yankatua anutapati, yasa asu mukoro dang vipakang patisi vatini,

4
00:00:23,520 --> 00:00:25,320
which means.

5
00:00:25,320 --> 00:00:33,760
Nattankamang kattang sadhu, that kamma which is performed or that kamma when performed is not good,

6
00:00:33,760 --> 00:00:46,120
which, yankatua anutapati, which, having performed, one is burnt or gets incensed in the

7
00:00:46,120 --> 00:00:49,040
mind, suffers in the mind.

8
00:00:49,040 --> 00:01:02,200
Yasa asu mukoro dang crying, one who has an asu mukoro, one who is faces full of tears,

9
00:01:02,200 --> 00:01:11,680
yasa, because of that kamma, we back on patisi vatini.

10
00:01:11,680 --> 00:01:22,800
So one who, by which, or for which, one who with tears in their eyes cries, crying with

11
00:01:22,800 --> 00:01:29,000
tears in their eyes, but he say what he receives the result of the kamma.

12
00:01:29,000 --> 00:01:37,240
So that kamma, busy, so, poly way of saying in verse that kamma is not good, an action

13
00:01:37,240 --> 00:01:38,640
is not good.

14
00:01:38,640 --> 00:01:45,560
If once you perform it, it causes you to, it burns you up inside, or it causes you to

15
00:01:45,560 --> 00:01:57,040
be to suffer, whereby you receive, because you receive the results, crying with a face

16
00:01:57,040 --> 00:02:01,200
full of tears, or tears in your eyes.

17
00:02:01,200 --> 00:02:07,560
Another one of these great verses, another all great, but this one, again, as usually

18
00:02:07,560 --> 00:02:13,520
it strips the story, which is a nice story, a short and sweet, and the story goes that

19
00:02:13,520 --> 00:02:21,200
there was, there were, starts off there were some robbers, some thieves in Sawati, and

20
00:02:21,200 --> 00:02:28,280
they dug a tunnel and went into a rich person's house and stole all their goods, and they

21
00:02:28,280 --> 00:02:29,280
divided up.

22
00:02:29,280 --> 00:02:40,920
One of the robbers found a, especially, a stash of, especially, valuable items, coins

23
00:02:40,920 --> 00:02:48,320
or gems, and he stuck it in his, in his coat or in his pants or whatever, and hid it

24
00:02:48,320 --> 00:02:57,040
from his fellow thieves, and so they escaped from the house and they went off into the fields

25
00:02:57,040 --> 00:03:01,120
and they went to this one farmer's field and they, they sat there and they were, they

26
00:03:01,120 --> 00:03:07,040
distributed up all the, the loot, and they were sitting, gloating and, and then they,

27
00:03:07,040 --> 00:03:14,920
they got up and were ready to leave, and this one who had kept a, a package, a purse

28
00:03:14,920 --> 00:03:21,520
aside, as he was getting him to leave, he dropped the purse in the farmer's field and didn't

29
00:03:21,520 --> 00:03:27,760
realize it, and they all got up and went on their way, went back to their thieves'

30
00:03:27,760 --> 00:03:32,640
lair, wherever it is that thieves were back to their homes.

31
00:03:32,640 --> 00:03:40,840
In the morning, the Buddha, as he was want to do in the early mornings, he sent out

32
00:03:40,840 --> 00:03:47,520
his mind to the whole of the universe, as he's done in, as we've seen him do another,

33
00:03:47,520 --> 00:03:55,920
another times in this book, and he thinks about who, in the nearby area, or in, in one

34
00:03:55,920 --> 00:04:02,240
of the heavens maybe, or in some faraway area, I mean faraway location was ready to hear

35
00:04:02,240 --> 00:04:07,960
the dhamma and who would best profit, and on this day, the owner of the farmer's field

36
00:04:07,960 --> 00:04:14,880
came into his mind and he realized that today was the day that this farmer would be

37
00:04:14,880 --> 00:04:20,400
just ready to, to realize the truth and to be able to let go of his worldly ways as

38
00:04:20,400 --> 00:04:29,120
a farmer and, or I'm not sure what happens in the end, but he wouldn't be able to become

39
00:04:29,120 --> 00:04:40,080
enlightened, you know, Kasekko, a pass, a go, a pass, a go, a go, a go, a go, so the,

40
00:04:40,080 --> 00:04:45,600
but people don't, okay, so he's, he realized that he's going to become a Sotapana with this

41
00:04:45,600 --> 00:04:52,800
teaching, so I said, well this is a good reason to go, and I'll also make a good, good example,

42
00:04:52,800 --> 00:04:58,400
and we can make a verse out of it, and eventually, 2,500 years later, someone would be reading

43
00:04:58,400 --> 00:05:05,280
this sort of book. So this was, perhaps not all going through the Buddha's mind, but to some extent,

44
00:05:05,280 --> 00:05:11,600
it was the idea that this was a worthwhile thing to do. So in the morning, he set it with Ananda

45
00:05:11,600 --> 00:05:17,520
on Amstround, and passed right by this farmer's field and the farmer was out in his field

46
00:05:17,520 --> 00:05:26,560
tilling this oil or doing whatever just the farmer's do, and the farmer saw him and

47
00:05:27,760 --> 00:05:32,080
it was quite impressed, of course, by the greatness of the Buddha wandering to the fields,

48
00:05:32,080 --> 00:05:38,080
carrying his robe and bowl with his eyes downcast, walking majestically like the king of the

49
00:05:38,080 --> 00:05:46,720
dhamma, the king of righteousness, and he went to the Buddha, and he paid respect to him,

50
00:05:47,840 --> 00:05:53,360
and then went back to plowing this field, and the Buddha didn't say anything. Normally,

51
00:05:53,360 --> 00:05:58,320
often when the people would come to pay respect to the Buddha, he would reply by giving them

52
00:05:58,320 --> 00:06:05,840
some teaching or at least acknowledging them. In this occasion, he didn't acknowledge the farmer,

53
00:06:07,360 --> 00:06:14,240
he stood and let the farmer pay respect to him and worship him and so on, and then just went

54
00:06:14,240 --> 00:06:18,720
on his way, and the farmer was, I guess, a little bit confused, and so he was kind of watching the

55
00:06:18,720 --> 00:06:26,240
Buddha go, and as the Buddha walked away, he looked over into the, he looked over into the field,

56
00:06:26,240 --> 00:06:34,160
and he said to Ananda, oh, Ananda, do you see that poisonous snake? Ananda looked, and he said,

57
00:06:34,960 --> 00:06:43,120
oh, indeed, that's a terrible, awful snake, and the farmer stood up, and when he heard this,

58
00:06:44,000 --> 00:06:47,840
when they walked away, he walked over to where they were looking, creeping over with his stick,

59
00:06:47,840 --> 00:06:54,880
getting ready to take on his fearsome snake, and he sees the little, the purse lying there where

60
00:06:54,880 --> 00:07:00,960
the Buddha and Ananda had looked, and he looks at the purse and he doesn't get it, he doesn't quite

61
00:07:00,960 --> 00:07:07,360
understand what the meaning is here, but he opens the purse and he sees all these valuable things inside,

62
00:07:08,160 --> 00:07:15,760
and he realizes that, well, this is something going on here, so he digs a hole, and he puts

63
00:07:15,760 --> 00:07:20,160
the purse into the hole and covers it up, and then says, well, I'll figure out what to do with

64
00:07:20,160 --> 00:07:27,120
his later, I don't really know what to do with that right now, but finding valuables on your property,

65
00:07:27,840 --> 00:07:35,360
if he figured he had to do something, and he went back to his work. Meanwhile, the authorities

66
00:07:35,360 --> 00:07:39,760
were busy doing their inspection of this rich person's property, and they actually managed to

67
00:07:39,760 --> 00:07:45,040
track the thieves to the farmer's field, then they saw the tracks coming and then dispersing,

68
00:07:45,040 --> 00:07:55,040
but then they also saw the farmer's tracks going to where the, and coming from where the farmer

69
00:07:55,040 --> 00:08:05,600
had been digging, and going right to where the thieves had been, had been, had been meeting,

70
00:08:06,480 --> 00:08:11,040
and then he saw it veering off a little bit, they went there, and they found the stash,

71
00:08:11,040 --> 00:08:16,160
and they dug it up, and they found the purse, and then they followed the tracks back to the farmer's

72
00:08:16,160 --> 00:08:21,840
house, and so, of course, they suspected that the farmer was, in fact, a thief, and they caught him

73
00:08:21,840 --> 00:08:27,760
up, and they took him out, and they were going to take him to jail, or maybe beat him, or I don't

74
00:08:27,760 --> 00:08:34,480
remember what they were going to do, maybe even kill him in those times. Justice was swift,

75
00:08:34,480 --> 00:08:42,160
but not very just, and so they took him, they were taking him through the streets, and as they

76
00:08:42,160 --> 00:08:50,240
were walking, he started shaking his head and muttering to himself, he says, on and you see that

77
00:08:51,040 --> 00:08:59,280
poisonous snake? Yes, venerable sir, what a terrible snake, and he's shaking his head and just

78
00:08:59,280 --> 00:09:03,680
muttering to himself, and the guards are like looking at him like he's crazy, and they say,

79
00:09:03,680 --> 00:09:11,440
what are you muttering to yourself? And he says, if you tell me, if you take him to the king,

80
00:09:11,440 --> 00:09:19,120
I'll tell you, and they're here, they think something's going on here, so they take him to the king,

81
00:09:21,040 --> 00:09:26,640
and he says, and they tell the king, they say that this man, we caught him red-handed,

82
00:09:26,640 --> 00:09:34,000
and buried this stolen goods, so obviously he was the one who he was a thief, and now he's saying

83
00:09:34,000 --> 00:09:41,840
something, he's evoking the name of the Buddha, and Ananda, and we thought this was worthy of some

84
00:09:43,120 --> 00:09:48,560
investigation in my destiny, and the king turns to the farmer and says, what's the meaning of this?

85
00:09:48,560 --> 00:09:55,120
Why are you saying to yourself, oh, oh, and Ananda, you see that poisonous snake,

86
00:09:55,120 --> 00:10:01,360
yes, venerable sir, what a terrible snake, and so he tells him the story, he says, I'm not a thief,

87
00:10:01,360 --> 00:10:06,080
and he tells it to him, what happened, and what the Buddha had said, and the king is

88
00:10:06,800 --> 00:10:13,280
as intrigued by this, something very interesting has happened. Why did the Buddha take the time

89
00:10:13,280 --> 00:10:19,920
to go to this farmer, and what is the lesson that's being taught here, so he takes the farmer,

90
00:10:19,920 --> 00:10:25,760
and he goes to the Buddha, and he asks, is this true, did you walk by this farmer's field,

91
00:10:25,760 --> 00:10:34,400
and happen to make mention of the poisonous snake, and the Buddha said yes indeed, I did,

92
00:10:35,600 --> 00:10:41,920
and he said, and then he spoke this verse, he says a person should be very careful about their

93
00:10:41,920 --> 00:10:49,360
deeds, but they don't come back to bite them like a poisonous snake, and so this is the story,

94
00:10:49,360 --> 00:10:58,240
but as usual, the verse says so much more, the verse points to any act, and it brings up the

95
00:10:58,240 --> 00:11:07,200
idea that an act is only as good as the result, and this is really the problem, because it's hard

96
00:11:07,200 --> 00:11:13,840
for us to tell what is going to be the result of our actions, and we often don't relate the actions

97
00:11:13,840 --> 00:11:20,640
with the result, we think that something is going to bring us happiness, and in fact it often does

98
00:11:20,640 --> 00:11:29,120
bring us temporary pleasure or relief from suffering, so here he has this probably inside has this

99
00:11:29,120 --> 00:11:36,880
desire for the treasure, and so he buries at thinking excited, you know, thinking that this is

100
00:11:36,880 --> 00:11:42,240
something, somehow this situation is going to bring me some sort of happiness, whereas with the Buddha

101
00:11:42,240 --> 00:11:47,760
and Ananda, having spent a lot of time looking at the nature of the world and the nature of

102
00:11:47,760 --> 00:11:52,480
desires, we're quite clear in their minds that this is indeed a poisonous snake, I often,

103
00:11:53,600 --> 00:12:00,080
in a narrow context I often use this verse to talk about the evils of money, specifically for monks,

104
00:12:01,680 --> 00:12:08,320
but on a larger scale you can see why it was that the Buddha enjoying the monks not to use money,

105
00:12:08,320 --> 00:12:13,920
because it can be a real place in this snake, people tend to get a little bit emotional or a lot

106
00:12:13,920 --> 00:12:22,080
emotional when money matters are involved, so if there had been a bunch of manure that he had hidden

107
00:12:22,080 --> 00:12:27,360
that someone had stolen and he had hidden it, it may not have evoked such an emotive response

108
00:12:27,360 --> 00:12:31,840
and caused such trouble for him, caused him to be beaten and dragged through the streets,

109
00:12:33,200 --> 00:12:36,960
but specifically for money there's something very poisonous about it,

110
00:12:36,960 --> 00:12:42,800
but with the Buddha saying here in his verse, in his teaching, he's saying to be where the

111
00:12:42,800 --> 00:12:51,200
results and as I said on a worldly level this is often difficult and in many cases we can't be sure

112
00:12:51,200 --> 00:12:57,360
what the results of our actions are going to be, but we never really focus on that in Buddhism

113
00:12:58,080 --> 00:13:06,560
because we consider that the world is mind made anyway or reality is a product of our past

114
00:13:06,560 --> 00:13:14,000
anyway, so product of our actions and of our intentions, so if it happens that we are unjustly

115
00:13:14,000 --> 00:13:19,840
accused of something, we often figure that there is some sort of justice to it because we've

116
00:13:19,840 --> 00:13:25,760
somehow managed to get ourselves in this situation, we've somehow managed to get here and now

117
00:13:25,760 --> 00:13:34,400
and there's no innocence there, so it's like when you enter into a contract and you're bound by

118
00:13:34,400 --> 00:13:41,920
the conditions of the contract, no matter what comes, in the same way we've entered into this

119
00:13:41,920 --> 00:13:47,200
contract willingly, we've willingly been born as human beings, which is why the Buddha said actually

120
00:13:47,200 --> 00:13:53,440
birth is also suffering because when you're born you're, it's like entering into a contract,

121
00:13:53,440 --> 00:13:58,640
if you didn't enter into the contract you wouldn't be forced to abide by the conditions

122
00:13:58,640 --> 00:14:04,800
and it's these conditions that often cause suffering, but I think in the Buddhist sense we

123
00:14:07,760 --> 00:14:15,280
have to focus on the karmagaspects of this verse, like the intentional aspect of it,

124
00:14:16,000 --> 00:14:22,400
that this farmer couldn't have known specifically what was going to happen when he took that,

125
00:14:22,400 --> 00:14:28,560
he may have even, it doesn't say in the verse, but we could give him the benefit of the doubt and

126
00:14:29,840 --> 00:14:34,880
imagine that to the best of his wisdom he thought this was a good idea that somehow he was going

127
00:14:34,880 --> 00:14:40,880
to return this one even, or he had no intention of taking it, no desire for it, probably wasn't

128
00:14:40,880 --> 00:14:48,320
the case, and so we could say that it actually wasn't the bad deed that he did,

129
00:14:48,320 --> 00:14:54,160
potentially, we don't know the intentions behind it, although as I said with money there's always

130
00:14:54,160 --> 00:15:02,880
the problem, people get emotional and there was really no need for him to bury it unless he was

131
00:15:02,880 --> 00:15:08,960
somehow concerned about it, but this is the point of, this is the reason why Ananda and the Buddha,

132
00:15:10,000 --> 00:15:15,840
especially the Buddha, were quite clear on the dangers of having anything to do with something

133
00:15:15,840 --> 00:15:22,720
like that because of the amount of desire that's involved, and so we focus on the intentions here,

134
00:15:22,720 --> 00:15:32,160
then we can ask ourselves how to cultivate an understanding of what is going to make us suffer in

135
00:15:32,160 --> 00:15:37,520
the future, what deeds are we going to perform, what deeds having performed are we going to

136
00:15:37,520 --> 00:15:46,640
weep and cry and beat our breasts, and Anotapati which really means to burn because of it or to

137
00:15:46,640 --> 00:15:57,280
get heated in the mind to get stressed in the mind, and even still for most people it's difficult

138
00:15:57,280 --> 00:16:05,040
to know, it's difficult for us to be clear what deeds having performed are going to lead us to

139
00:16:05,040 --> 00:16:12,800
suffering, and the problem is the act and the results are so far removed from each other in many cases,

140
00:16:13,440 --> 00:16:22,640
so you might indulge in something, eat lots of ice cream, or have a romantic engagement,

141
00:16:22,640 --> 00:16:29,200
a romantic engagement is a good example because it often ends in tears, in fact the more

142
00:16:29,200 --> 00:16:35,360
passionate it is, the more likely you could say it's going to end in tears if two people are relaxed

143
00:16:35,360 --> 00:16:41,440
in their relationship and comfortable in their own individuality than any breakup is not likely

144
00:16:41,440 --> 00:16:48,000
to be as devastating, but if it's passionate and if there's a great amount of, in fact, pleasure

145
00:16:48,000 --> 00:16:55,840
involved, then there's much more likely to be a great amount of pain and suffering, and so this

146
00:16:55,840 --> 00:17:03,200
is why when relationships break up or people pass away or move on or whatever, there's a great

147
00:17:03,200 --> 00:17:14,320
amount of asumukha, peaceful of tears, but we can't see it because there's so much pleasure,

148
00:17:15,360 --> 00:17:25,440
and in fact it's even worse than that because we started out with pleasure because we have

149
00:17:25,440 --> 00:17:29,600
suffering, generally the reason why people want pleasure is because there's something lacking,

150
00:17:29,600 --> 00:17:34,880
there's a feeling of agitation or upset in their mind, so they want something to ease that,

151
00:17:34,880 --> 00:17:41,280
to appease that, to make them happy again, and so they undertake something, in this case,

152
00:17:41,280 --> 00:17:49,120
an romantic engagement, and it brings them lots and lots of pleasure, and so they become accustomed

153
00:17:49,120 --> 00:17:54,240
to the pleasure, and that pleasure directly leads to their suffering because of the addiction, so

154
00:17:54,240 --> 00:18:01,040
then the person changes or their expectations are not met and leaves or daily or they fight or

155
00:18:01,040 --> 00:18:06,160
something, and there's a great amount of suffering. Now that suffering is exactly what's going to lead

156
00:18:06,160 --> 00:18:13,840
them back into a relationship or seeking out the same sort of relationship because it was the same

157
00:18:13,840 --> 00:18:22,880
reason that got them into it, and so it's a deadly cycle, it's the craving for food is another one

158
00:18:22,880 --> 00:18:28,720
when you crave for the food, this is a kind of a suffering, so you go and get the food and you feel

159
00:18:28,720 --> 00:18:40,400
happy, enjoy the taste or you enjoy the sensation of being full, and it creates this attachment to

160
00:18:40,400 --> 00:18:45,840
it, and then when it's gone, you suffer again, and you go out and get it again, and we do this

161
00:18:45,840 --> 00:18:52,640
with all sorts of pleasurable things, so in fact it's even worse than just being disconnected,

162
00:18:52,640 --> 00:18:59,280
and they're actually responsible for each other, the pleasure and the pain, the pleasure

163
00:18:59,920 --> 00:19:05,920
is responsible to bring us disappointment, dissatisfaction, the dissatisfaction, leads us to want to get

164
00:19:05,920 --> 00:19:15,520
more, and have to quickly go and get more, and so the actions are directly responsible

165
00:19:17,360 --> 00:19:22,480
for the suffering, but the suffering is also directly responsible for our desire for

166
00:19:22,480 --> 00:19:29,760
more, and so it's a terrible vicious cycle, and so this is why, or this is where in Buddhism

167
00:19:29,760 --> 00:19:34,960
meditation comes in, this is why meditation is so important, if you think in a worldly sense,

168
00:19:34,960 --> 00:19:42,480
how would you know a certain action like bringing a bunch of coins is going to have bad results,

169
00:19:43,200 --> 00:19:51,840
you'll know from experience, so there are so many different situations that you can't sit down

170
00:19:51,840 --> 00:19:59,520
and consider logically what's going to be the outcome, but with experience, you can see how

171
00:19:59,520 --> 00:20:05,600
people react and you can see what are probable outcomes, and with a lot of experience in a worldly

172
00:20:05,600 --> 00:20:13,040
sense, you can live your life fairly stably, so you know not to bury, if you've been beaten and

173
00:20:13,040 --> 00:20:17,920
dragged to the streets, for example, you know not to bury a bunch of coins because probably people

174
00:20:17,920 --> 00:20:21,760
aren't going to get upset and you're going to be accused of something you didn't do and so on,

175
00:20:21,760 --> 00:20:26,160
and so on, it should be very careful, this is why of course, if this happened today,

176
00:20:27,040 --> 00:20:35,200
people would often go directly to the authorities, if some bank, when you're sacked,

177
00:20:35,920 --> 00:20:39,520
from a bank or something was found in a farmer's field, the farmer would right away

178
00:20:40,320 --> 00:20:43,520
hopefully go to the authorities, because he wouldn't want to be

179
00:20:43,520 --> 00:20:55,360
unduly accused. So the same principle applies for meditation, applies for the

180
00:20:57,920 --> 00:21:06,400
intentional activities of the mind, meditation is for the direct purpose,

181
00:21:06,400 --> 00:21:15,360
and for the soul purpose, the main purpose of meditation is experience. We're trying to

182
00:21:15,360 --> 00:21:20,320
understand, we're not trying to change, we're not trying to stop ourselves from

183
00:21:21,680 --> 00:21:30,320
doing any number of things, we're trying to understand and gain experience about how the

184
00:21:30,320 --> 00:21:38,400
mind works, how reality works, how cause and effects works, what leads to what, when you want

185
00:21:38,400 --> 00:21:44,800
something, what does that lead to, leads to clinging, when you cling to something, what does that

186
00:21:44,800 --> 00:21:52,080
lead to, leads to chasing, when you chase something, what does that lead to, it leads to suffering,

187
00:21:52,960 --> 00:21:58,720
leads to not getting eventually or leads to the stress of having to work or so on,

188
00:21:58,720 --> 00:22:04,000
and as a result, leads to suffering, what does suffering lead to a suffering, leads to one thing,

189
00:22:04,000 --> 00:22:10,640
again, and so you see this, and eventually you get tired of it, you're able to change your habits,

190
00:22:10,640 --> 00:22:15,600
through experience, you're able to see what the Buddha said, that indeed it's not good,

191
00:22:16,320 --> 00:22:23,360
nutang kamang katang sad, that kamma is indeed not good, if you do it and it causes you suffering,

192
00:22:23,360 --> 00:22:27,520
it's indeed not good, he's not, you know, it's actually, if you read the verses, actually,

193
00:22:27,520 --> 00:22:31,600
something, you say, well of course I know that, if something causes me suffering, I shouldn't do it,

194
00:22:31,600 --> 00:22:37,200
but this is the realization that the Buddha is talking about, he's talking about what we need to do,

195
00:22:37,200 --> 00:22:45,360
we need to realize that which the Buddha has realized, has realized that that deed is not worth

196
00:22:45,360 --> 00:22:51,680
doing, that deed brings me suffering, he's pointing out, for most of us we don't know this,

197
00:22:51,680 --> 00:22:58,640
most of us, or we know the principle, but we don't know that these things are causing a suffering,

198
00:22:59,680 --> 00:23:05,760
you're saying, you're doing this because you don't understand that it's causing you suffering,

199
00:23:06,480 --> 00:23:10,240
because it's not worth doing, if you knew that it was suffering, why would you do it?

200
00:23:11,040 --> 00:23:17,680
And they're saying, when we do things that cause a suffering, we have to be clear that

201
00:23:17,680 --> 00:23:23,040
we would have been better off to not perform those actions, to not get involved, to not get caught

202
00:23:23,040 --> 00:23:30,080
up in those things that cause a suffering. And so it's a lesson, it's a lesson really for us

203
00:23:30,080 --> 00:23:36,960
to continue a meditation, to dedicate ourselves to meditation, to focus on this idea of acquiring

204
00:23:36,960 --> 00:23:41,040
experience, acquiring an understanding of how the mind and the body works, when you sit,

205
00:23:41,040 --> 00:23:45,360
then you watch your mind, you see one thing leading to another, when you see your desires,

206
00:23:45,360 --> 00:23:50,640
you see your versions, you see your sufferings, and you see what causes what,

207
00:23:51,280 --> 00:23:57,600
and you learn to relax your mind and ease up this tension that comes from wanting and kneading,

208
00:23:57,600 --> 00:24:08,640
and that comes from aversion and frustration and greed and anger, and all these pushes and pulls

209
00:24:08,640 --> 00:24:14,800
that exist in the mind. And we see that we are causing ourselves suffering and we realize that

210
00:24:14,800 --> 00:24:19,680
indeed this is the point, we should stop doing those things that cause our suffering,

211
00:24:21,360 --> 00:24:25,520
and the point and the problem is that often we don't know that they're causing us suffering,

212
00:24:25,520 --> 00:24:30,960
we don't realize that they're going to cause our suffering. So as the Buddha said, before you do

213
00:24:30,960 --> 00:24:36,000
something you should be clear, that it's a good thing to do, when you're doing something you should

214
00:24:36,000 --> 00:24:41,840
be clear, that it's a good thing to do, and after you finished it you should be clear that it was

215
00:24:41,840 --> 00:24:47,600
a good thing to have done, and if not you should quickly change, you should have the mindfulness

216
00:24:47,600 --> 00:24:54,320
to be clearly aware of those things that you do. Without mindfulness we are easily caught up in

217
00:24:54,320 --> 00:25:01,760
performing bad deeds of act, speech, and mind and caught up, and all the way through

218
00:25:01,760 --> 00:25:09,040
until it leads to great, can lead to great suffering and stress for us. So that's the lesson,

219
00:25:09,040 --> 00:25:17,280
it's a simple lesson, but then one of those things that reminds us of the inevitability of cause and

220
00:25:17,280 --> 00:25:24,000
effect, that you can't escape your karma, and better to not do these deeds at all, not to just

221
00:25:24,000 --> 00:25:30,960
hope that it doesn't come to us, but better is it not to do, indeed it's not well done if it

222
00:25:30,960 --> 00:25:39,840
needs you to suffering, everything is judged by the result that has, so thank you for tuning in,

223
00:25:39,840 --> 00:25:45,360
this is another verse of the Dhamma Bhadha, wishing you all true peace, happiness, and freedom

224
00:25:45,360 --> 00:26:15,200
from suffering, have a good one.

