1
00:00:00,000 --> 00:00:10,000
Hi, good morning and I'm here today to talk about the official release of our one-hour DVD on how to meditate.

2
00:00:10,000 --> 00:00:24,000
The DVD contains the six video clips from YouTube and a menu that allows you to select each individual clip or to watch the entire DVD.

3
00:00:24,000 --> 00:00:32,000
Now, we are as of now officially distributing this through the website www.ceremungalode.org

4
00:00:32,000 --> 00:00:44,000
and the DVDs are available for free. However, due to the costs and making the DVD, we are also looking for sponsors.

5
00:00:44,000 --> 00:00:51,000
So anyone who would like to sponsor the distribution of the DVDs, please check out the information on the website.

6
00:00:51,000 --> 00:00:59,000
The way we're going to do it is as sponsorship comes in, we're going to distribute the DVDs based on the sponsorship that we've already got.

7
00:00:59,000 --> 00:01:06,000
Now, we'll try to use sponsorship from other areas and bring that in as well.

8
00:01:06,000 --> 00:01:18,000
But if you would like a DVD, please consider to make a donation to the cost and for every $5 that is donated, which is the estimated cost of the production.

9
00:01:18,000 --> 00:01:22,000
Now, we're not getting any money for this, so we're not trying to make money off of it.

10
00:01:22,000 --> 00:01:28,000
So for every $5 that is donated, we will make available one DVD.

11
00:01:28,000 --> 00:01:36,000
So if you would like to help with this, or if you'd like a free DVD, please send us a note at www.ceremungalode.org

12
00:01:36,000 --> 00:01:48,000
front slash contact. If you'd like to support us, it's front slash support. If you'd like more information, it's www.ceremungalode.org front slash videos with an S on the end.

13
00:01:48,000 --> 00:02:14,000
Okay, that's all for now. Have a good day.

