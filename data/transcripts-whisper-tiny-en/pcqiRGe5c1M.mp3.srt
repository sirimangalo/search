1
00:00:00,000 --> 00:00:12,400
Hi, this is going to be the last video I make before I go to Thailand and so this is goodbye

2
00:00:12,400 --> 00:00:19,520
and thanks again for everyone for tuning in and keeping up and supporting the work that

3
00:00:19,520 --> 00:00:31,240
I do. I'll be gone for maybe two months maybe more who knows maybe I'll just settle down

4
00:00:31,240 --> 00:00:38,400
in Sri Lanka for a year or two in which case you'll be getting asked a monk from broadcast

5
00:00:38,400 --> 00:00:44,840
from Sri Lanka for the next year or two but who knows maybe I'll be back in January to

6
00:00:44,840 --> 00:00:56,080
America. One thing I wanted to just talk about briefly before I left was the idea of finding

7
00:00:56,080 --> 00:01:02,740
the truth inside yourself. It's important to understand that no matter what books you

8
00:01:02,740 --> 00:01:08,480
read or teachers you listen to as long as you're looking for answers outside of yourself

9
00:01:08,480 --> 00:01:14,120
you're never going to find the truth. The truth is right inside of yourself. It's about finding

10
00:01:14,120 --> 00:01:20,280
peace and happiness right here and right now in the present moment in your experience of

11
00:01:20,280 --> 00:01:25,400
every moment. If you're not happy right here right now you're never going to find peace and

12
00:01:25,400 --> 00:01:34,000
happiness. So looking for happiness outside of yourself or looking for instruction and guidance

13
00:01:34,000 --> 00:01:41,000
from teachers is never really going to bring you to the truth. The best it can do is give

14
00:01:41,000 --> 00:01:47,000
you information and point you in the right way. If I tell you to walk in a certain direction

15
00:01:47,000 --> 00:01:53,800
I'm down a certain path it's up to you to walk the path. So once you have this information

16
00:01:53,800 --> 00:01:59,080
you shouldn't just be content with the fact that you know that you understand and accept

17
00:01:59,080 --> 00:02:04,440
the concept you should actually try to put it into practice in your daily life and

18
00:02:04,440 --> 00:02:13,000
inform a meditation practice try to find the answers inside of yourself. So when you sit

19
00:02:13,000 --> 00:02:17,440
down to meditate the ideas that all the answers should come to you you shouldn't need to

20
00:02:17,440 --> 00:02:23,080
ask questions of anyone else. Asking questions is really a preliminary practice if something

21
00:02:23,080 --> 00:02:29,760
that is used to pull you away from the wrong path and get you on a path which will allow

22
00:02:29,760 --> 00:02:34,400
you to understand reality for yourself so that you don't have to depend on anyone. You

23
00:02:34,400 --> 00:02:38,480
don't even have to depend on your own beliefs or views because you see things as they

24
00:02:38,480 --> 00:02:45,520
are you're able to see very deeply the truth of reality and to find for yourself the answers

25
00:02:45,520 --> 00:02:53,760
that you seek. As I've always said the truth is within. So thanks for again for tuning

26
00:02:53,760 --> 00:02:58,800
in and wishing everyone all the best and those of you who have taken up the meditation

27
00:02:58,800 --> 00:03:08,680
based on these videos. I'd like to appreciate that and wish you all the best and give

28
00:03:08,680 --> 00:03:15,440
my encouragement to you. I hope these videos have been a benefit to some number of people

29
00:03:15,440 --> 00:03:23,200
to some people and that you're able to use them for the development of your mind and

30
00:03:23,200 --> 00:03:28,880
the development of peace and happiness and freedom from suffering and that you're able to find

31
00:03:28,880 --> 00:03:37,840
true freedom and release through these teachings. So that's all. I hope that everyone

32
00:03:37,840 --> 00:03:47,320
has a great peace and happiness in their lives and that you're able to live your lives

33
00:03:47,320 --> 00:03:54,280
free from the dangers and the sorrows and the sufferings that so many of us have to deal

34
00:03:54,280 --> 00:04:02,040
with and when they do come that you're able to bear them with strength and with equanimity

35
00:04:02,040 --> 00:04:07,240
and to see through them with wisdom and to find peace and happiness even in the face of

36
00:04:07,240 --> 00:04:23,160
great danger and misfortune. Okay so take care all the best and remember keep meditating.

