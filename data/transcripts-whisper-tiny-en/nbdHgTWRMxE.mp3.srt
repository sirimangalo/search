1
00:00:00,000 --> 00:00:08,120
Hey everyone, this is Adder with another update about our meditation center fundraiser.

2
00:00:08,120 --> 00:00:13,920
This week we surpassed a hundred donors and it's been really wonderful to see how wide

3
00:00:13,920 --> 00:00:18,760
reaching and engaged the Sierra Mangolo community is.

4
00:00:18,760 --> 00:00:24,080
Today I want to take a little bit of time to tell you all a little bit about how the

5
00:00:24,080 --> 00:00:28,920
finances of Sierra Mangolo international work.

6
00:00:28,920 --> 00:00:35,520
Finally we don't talk in much detail about our finances because we want to make sure that

7
00:00:35,520 --> 00:00:41,360
everyone understands that the teachings are offered freely and all of the services the

8
00:00:41,360 --> 00:00:54,160
same and that giving should be based on one's own expression of generosity and not to

9
00:00:54,160 --> 00:01:02,120
re-compensate a specific amount for any course taken or any services from our community

10
00:01:02,120 --> 00:01:04,520
that you've taken advantage of.

11
00:01:04,520 --> 00:01:10,520
So we want to remind everyone that all of Dante's teachings, all of the retreats that

12
00:01:10,520 --> 00:01:17,640
we run, these really are available to the world for free and through the generosity of

13
00:01:17,640 --> 00:01:19,760
our community.

14
00:01:19,760 --> 00:01:27,000
But that said, in doing a fundraiser like this in hopes to build a new center or establish

15
00:01:27,000 --> 00:01:33,600
a new home for our meditation, it is helpful to people to hear a little bit about how

16
00:01:33,600 --> 00:01:42,800
our finances work and why we have the goals that we do and what giving money is actually

17
00:01:42,800 --> 00:01:44,920
going to go towards.

18
00:01:44,920 --> 00:01:50,960
We do publish our quarterly financial reports on the website at seriamangolo.org slash

19
00:01:50,960 --> 00:01:54,080
info down on the bottom of the page there.

20
00:01:54,080 --> 00:02:02,640
So if you ever want to look at a summary of our finances or if you want to learn more,

21
00:02:02,640 --> 00:02:08,840
you can always hop on our Discord server and reach out to me, Adder or any of the board

22
00:02:08,840 --> 00:02:12,640
members and we can let you know more.

23
00:02:12,640 --> 00:02:19,160
So seriamangolo International is a registered charity in Canada and we operate completely

24
00:02:19,160 --> 00:02:23,480
off of donations off of the Donna of our community.

25
00:02:23,480 --> 00:02:33,040
And we bring in about $50 to $60,000 Canadian dollars each year and spend most all of

26
00:02:33,040 --> 00:02:44,000
that in a given year, at least $50,000.

27
00:02:44,000 --> 00:02:51,280
Solid half of that goes to paying rent for the house that we run the center out of.

28
00:02:51,280 --> 00:02:55,480
So we're just renting a regular sized family home.

29
00:02:55,480 --> 00:03:01,680
We can get about five meditators in at a time, though of course less right now as we've

30
00:03:01,680 --> 00:03:08,200
taken precautions to deal with coronavirus.

31
00:03:08,200 --> 00:03:16,480
And so it's a pretty big financial burden to be putting half of the donations towards

32
00:03:16,480 --> 00:03:20,000
paying the rent in a relatively small center.

33
00:03:20,000 --> 00:03:28,040
So our thinking for establishing a new center is really having ownership of some property

34
00:03:28,040 --> 00:03:33,240
by the organization itself rather than having to rent.

35
00:03:33,240 --> 00:03:36,800
So we have this fundraising goal of $300,000.

36
00:03:36,800 --> 00:03:40,360
Now it's a pretty rough number.

37
00:03:40,360 --> 00:03:47,200
We surpass it or if we find ourselves a little bit short, we will still likely be able

38
00:03:47,200 --> 00:03:50,800
to use those funds to establish a new center.

39
00:03:50,800 --> 00:03:59,280
But $300,000 works out nicely to match with some of the properties that we had looked

40
00:03:59,280 --> 00:04:00,280
at.

41
00:04:00,280 --> 00:04:08,400
We looked at a number of places in the $600 to $800,000 range that are commercial properties

42
00:04:08,400 --> 00:04:17,800
like a bed and breakfast or a church that we could convert into a more of a center that

43
00:04:17,800 --> 00:04:23,640
can handle more people and we can build more community out of.

44
00:04:23,640 --> 00:04:32,160
So based on rough calculations, if we had $300,000, we could put a down payment on an

45
00:04:32,160 --> 00:04:37,560
$800,000 property and have our monthly mortgage payment be about the same as the rent

46
00:04:37,560 --> 00:04:39,200
we're paying now.

47
00:04:39,200 --> 00:04:45,440
Alternatively, we could get a smaller property that's closer to what we have and actually

48
00:04:45,440 --> 00:04:54,880
reduce our monthly payments because we would be able to put more down and more of a proportion

49
00:04:54,880 --> 00:05:03,200
of the total cost as a down payment and not be having to rent.

50
00:05:03,200 --> 00:05:09,880
So again, as we continue this fundraiser, it's really up to the generosity of our community

51
00:05:09,880 --> 00:05:18,440
and we will take whatever funds are raised and figure out a way to use them wisely and

52
00:05:18,440 --> 00:05:24,600
efficiently and in service of the Dhamma.

53
00:05:24,600 --> 00:05:33,000
Yeah, I think that's all I have to share about that.

54
00:05:33,000 --> 00:05:39,600
I just wanted to give you all a feel for why it is that we're reaching out and trying

55
00:05:39,600 --> 00:05:46,120
to raise some money and how we'll get to use that and how that will benefit all the

56
00:05:46,120 --> 00:05:51,600
meditators who come to the center in the future for the first time and all of those of

57
00:05:51,600 --> 00:06:00,120
us who plan on returning to the center as soon as we're able or when the time is right.

58
00:06:00,120 --> 00:06:07,080
I want to remind you all that we're still gathering stories for our testimonial series looking

59
00:06:07,080 --> 00:06:14,040
for people to share their personal story of meditation and their path and their involvement

60
00:06:14,040 --> 00:06:21,000
in Siri Mangala or just the ways that the practice of mindfulness has changed their life.

61
00:06:21,000 --> 00:06:26,920
So please feel free to record your story and reach out and we can tell you more about that

62
00:06:26,920 --> 00:06:30,560
if you need any help.

63
00:06:30,560 --> 00:06:58,000
Until next time, I wish you all wellness and happiness.

