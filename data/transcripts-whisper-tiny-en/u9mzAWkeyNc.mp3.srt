1
00:00:00,000 --> 00:00:10,160
Good evening everyone, welcome to our live broadcast.

2
00:00:10,160 --> 00:00:14,760
Tonight we are going to go straight into questions and answers.

3
00:00:14,760 --> 00:00:24,120
We just had our volunteer meetings, so I didn't actually research something to talk about

4
00:00:24,120 --> 00:00:25,120
tonight.

5
00:00:25,120 --> 00:00:32,000
I could come up with something off the top of my head, but I think we'll just keep it short

6
00:00:32,000 --> 00:00:33,000
tonight.

7
00:00:33,000 --> 00:00:39,440
Answer people's questions, and early, look at this every night, so it's not like you're

8
00:00:39,440 --> 00:00:47,760
missing out, but back again tomorrow.

9
00:00:47,760 --> 00:00:58,520
So Robin's having sound issues, I think, unfortunately, so if and when you're able to

10
00:00:58,520 --> 00:01:12,680
talk, go ahead, but until I hear you, I will start off the questions.

11
00:01:12,680 --> 00:01:25,320
Does acceptance arise from dispassion?

12
00:01:25,320 --> 00:01:29,840
Acceptance isn't exactly what we want, you see.

13
00:01:29,840 --> 00:01:37,360
We don't use the word acceptance that much, because it's not exactly, it's not at all

14
00:01:37,360 --> 00:01:45,240
really what we are, it's improper, it's incorrect, it's an improper description of the

15
00:01:45,240 --> 00:01:46,240
goal.

16
00:01:46,240 --> 00:01:54,080
We're not accepting anything, and you might, I mean, some two-team is probably the closest

17
00:01:54,080 --> 00:02:02,600
song means with and two-team means happiness, so it means being happy with what you have.

18
00:02:02,600 --> 00:02:10,680
But that doesn't refer to, you might argue that that's acceptance, but that's just

19
00:02:10,680 --> 00:02:18,600
one quality that we have describing, the Buddha's teaching acceptance is not the core value.

20
00:02:18,600 --> 00:02:24,840
Dispassion leads to turning away, actually, discarding.

21
00:02:24,840 --> 00:02:31,240
We don't accept anything about the universe, we discard everything about the universe.

22
00:02:31,240 --> 00:02:42,880
We dismiss it as meaningless, pointless, of no consequence, of no value.

23
00:02:42,880 --> 00:02:58,600
The only thing we accept, we accept the truth about reality, and maybe it's just nitpicking,

24
00:02:58,600 --> 00:03:06,800
because it is to some extent an acceptance in terms of not getting upset, but it's not

25
00:03:06,800 --> 00:03:14,040
an acceptance exactly because you're still clear in your mind that there's no benefit

26
00:03:14,040 --> 00:03:21,200
to the universe, to any aspect of some sorrow, so it's really a rejection of some

27
00:03:21,200 --> 00:03:28,960
sorry, and it's that rejection, which brings about or creates the conditions for the realization

28
00:03:28,960 --> 00:03:35,680
of nibana, it only comes about not by acceptance, but by rejection.

29
00:03:35,680 --> 00:03:42,880
The moment before realizing nibana, nirvana is a moment of realizing that reality is

30
00:03:42,880 --> 00:03:51,800
supermanent unsatisfying and controllable and rejecting it, so I guess it just means

31
00:03:51,800 --> 00:03:57,160
to be a little bit careful with the word acceptance, but some form of acceptance could

32
00:03:57,160 --> 00:04:08,560
be seen as coming about from that process in the sense of giving up your ambitions, accepting

33
00:04:08,560 --> 00:04:18,320
that you're not ever going to get stability, satisfaction, or control from some sorrow,

34
00:04:18,320 --> 00:04:25,760
and thereby giving up, but it's more about giving up than accepting.

35
00:04:25,760 --> 00:04:35,200
Acceptance, the preceding cause of loving kindness, kind of accepting people as they are.

36
00:04:35,200 --> 00:04:44,480
Equanimity is caused by acceptance, in the sense of accepting that people are the way

37
00:04:44,480 --> 00:04:48,200
they are, and that you're not going to change them.

38
00:04:48,200 --> 00:04:57,120
Equanimity is the big one, meta, no meta, friendliness or loving kindness, cause of that

39
00:04:57,120 --> 00:05:08,880
is seeing a potential for people to be happy, and thinking about a thought of someone obtaining

40
00:05:08,880 --> 00:05:19,960
happiness, and as a result you feel happy, or you feel a desire for them to be happy in a sense.

41
00:05:19,960 --> 00:05:26,520
Acceptance and inclination for their happiness, for them to obtain the happiness, but acceptance

42
00:05:26,520 --> 00:05:28,040
is what leads to unanimity.

43
00:05:28,040 --> 00:05:36,160
If someone's just a terrible, horrible person, you might try to help them or alleviate

44
00:05:36,160 --> 00:05:41,760
the problem, but if you can't, then you accept in a sense and have equanimity, except

45
00:05:41,760 --> 00:05:50,880
that you can't change them.

46
00:05:50,880 --> 00:05:51,880
We had this one.

47
00:05:51,880 --> 00:05:56,320
Could I replace formal practice with just sitting down, doing nothing, we had that last

48
00:05:56,320 --> 00:05:57,320
night?

49
00:05:57,320 --> 00:05:59,600
Are we going to use this site anymore?

50
00:05:59,600 --> 00:06:04,280
No, this site is closing down, unfortunately, I'm actually kind of fond of the layout

51
00:06:04,280 --> 00:06:09,720
of this site, having it all on one page as opposed to the tabs.

52
00:06:09,720 --> 00:06:13,560
The new site is going to supersede this.

53
00:06:13,560 --> 00:06:17,720
So if you have complaints about the UI, if you'd rather see it, look some other way,

54
00:06:17,720 --> 00:06:20,240
you'll talk to us on GitHub.

55
00:06:20,240 --> 00:06:27,600
I don't think there's even a link, but I'll post the link here, and then you guys can

56
00:06:27,600 --> 00:06:30,600
hang on to this link, on posting issues.

57
00:06:30,600 --> 00:06:38,160
You have to learn a little bit about GitHub, there's the issues, if you go there, if you

58
00:06:38,160 --> 00:06:46,880
have an issue with the new site, not this site, the new site that we're working on.

59
00:06:46,880 --> 00:07:16,240
I've been here still mute, yeah, I don't hear you, I see a lip smoothie, more sound coming

60
00:07:16,240 --> 00:07:24,000
out.

61
00:07:24,000 --> 00:07:29,600
Monday posted a really long joke, fortunately, I don't have time to read it.

62
00:07:29,600 --> 00:07:33,680
Regarding your talk yesterday, I was wondering what makes a person greater equal less to

63
00:07:33,680 --> 00:07:35,280
someone, right?

64
00:07:35,280 --> 00:07:44,920
Well, you could say, even worldly matters, worldly affairs would count.

65
00:07:44,920 --> 00:07:52,040
Like if someone is richer, no, that wouldn't work with it.

66
00:07:52,040 --> 00:07:58,840
Well, yes, suppose someone has a lot of money, and thereby thinks of themselves as greater

67
00:07:58,840 --> 00:07:59,840
than another person.

68
00:07:59,840 --> 00:08:00,840
That's conceit, right?

69
00:08:00,840 --> 00:08:06,120
I've got a lot of money, and I look at a poor person and say, I'm greater than them.

70
00:08:06,120 --> 00:08:12,240
That conceiving of being greater, even though they're not greater, and we could argue that

71
00:08:12,240 --> 00:08:18,400
money from a Buddhist point of view doesn't make someone greater, and therefore here's

72
00:08:18,400 --> 00:08:22,880
a case of someone thinking they were greater when they're not actually greater.

73
00:08:22,880 --> 00:08:30,080
On the other hand, you could have someone who is pure of mind, right?

74
00:08:30,080 --> 00:08:40,400
They're kind and generous and compassionate and they're wise, but they might have low self-esteem,

75
00:08:40,400 --> 00:08:45,040
and they might be always worried about, they might have the fault of being worried about

76
00:08:45,040 --> 00:08:50,600
bad things they do, and so they look at someone else who's very confident, saying, and

77
00:08:50,600 --> 00:09:04,440
who appears to be, who does bold acts of goodness or something, and they think, wow, that

78
00:09:04,440 --> 00:09:17,080
person is better than me, that person is better than me because of how confident they are,

79
00:09:17,080 --> 00:09:18,080
or something like that.

80
00:09:18,080 --> 00:09:22,320
But in fact, you might argue that while the confidence is actually potentially a detriment

81
00:09:22,320 --> 00:09:27,760
to that person, and maybe it's associated with ego, so they might actually not be better.

82
00:09:27,760 --> 00:09:34,880
On the other hand, a person can be a very good person and know it and can become conceited

83
00:09:34,880 --> 00:09:41,240
about it, and they look at people who are evil and mean and nasty, and think, huh,

84
00:09:41,240 --> 00:09:47,320
I'm better than that person, they are better than that, it's still conceited.

85
00:09:47,320 --> 00:09:51,760
So what really makes a person greater, lesser and equal is in regards to their goodness,

86
00:09:51,760 --> 00:09:57,920
their purity, their good qualities of Buddhism, to someone regular, they give rise to greater

87
00:09:57,920 --> 00:09:58,920
ones.

88
00:09:58,920 --> 00:10:02,920
Someone assault upon that sack of the gami on the gun, even assault upon it can be conceited

89
00:10:02,920 --> 00:10:03,920
about it.

90
00:10:03,920 --> 00:10:10,360
Hi, Bhante, can you hear me now?

91
00:10:10,360 --> 00:10:11,760
Yes, I can.

92
00:10:11,760 --> 00:10:12,760
Okay.

93
00:10:12,760 --> 00:10:13,760
Is it breaking up or anything?

94
00:10:13,760 --> 00:10:14,760
No, it's great.

95
00:10:14,760 --> 00:10:15,760
Okay.

96
00:10:15,760 --> 00:10:16,760
Thank you.

97
00:10:16,760 --> 00:10:21,840
Nine plus years ago, when I started meditating, my first teacher taught, standing, standing,

98
00:10:21,840 --> 00:10:28,400
but also, Rupa stands, Rupa sits, and so on, I never saw any fault in that.

99
00:10:28,400 --> 00:10:32,240
Now when the mind falls back into this old way of noting, it feels or seems like the mind

100
00:10:32,240 --> 00:10:36,320
is creating a separate idea or entity for the label Rupa.

101
00:10:36,320 --> 00:10:40,680
What are your thoughts on noting, standing, standing versus Rupa stands?

102
00:10:40,680 --> 00:10:41,680
Yes, of course.

103
00:10:41,680 --> 00:10:47,360
I've read your book, I'm puzzled because you're both Mahasi teachers.

104
00:10:47,360 --> 00:10:55,360
Mahasi Sayada seemed somewhat critical of that sort of, not exactly critical, but dismissed

105
00:10:55,360 --> 00:10:57,920
the idea that that was necessary.

106
00:10:57,920 --> 00:11:04,480
One of his books, one of the sort of well-known, you can read through it.

107
00:11:04,480 --> 00:11:08,720
I can find it for you if you like, discusses this and says, you know, the Buddha said,

108
00:11:08,720 --> 00:11:15,240
I go in the back of Kachami, I stand, deep-dome here.

109
00:11:15,240 --> 00:11:23,000
So and he talks about whether you need to actually talk about Rupa or say, you know,

110
00:11:23,000 --> 00:11:27,400
air element or that or Rupa or that kind of thing.

111
00:11:27,400 --> 00:11:32,040
And I would agree, you know, even if he wasn't the Mahasi Sayada, I would still agree

112
00:11:32,040 --> 00:11:40,360
because it's somehow intellectualizing, you know, I mean, it's not something you have

113
00:11:40,360 --> 00:11:42,360
to remind yourself about.

114
00:11:42,360 --> 00:11:48,240
The Rupa is the experience, the experiences of Rupa, you know, when you say, standing,

115
00:11:48,240 --> 00:11:54,280
you're aware of the Rupa standing, but as soon as you say Rupa, you're introducing some sort

116
00:11:54,280 --> 00:12:02,360
of theory in the sense of what is Rupa or this being Rupa or that kind of thing.

117
00:12:02,360 --> 00:12:09,200
It's not necessarily wrong, but I would bet that anyone who does that could have the

118
00:12:09,200 --> 00:12:14,080
potential for, especially if they know what Rupa is, could have the potential for over

119
00:12:14,080 --> 00:12:16,080
intellectualizing.

120
00:12:16,080 --> 00:12:22,400
Now, if you say you've done it and it works and it worked for you, I mean, yeah, it probably

121
00:12:22,400 --> 00:12:27,880
does work fine, it's not what I would recommend for that reason because you're adding,

122
00:12:27,880 --> 00:12:35,840
as you see, as you say, you're adding an extra concept and there's a potential for distraction

123
00:12:35,840 --> 00:12:39,600
over analyzing and intellectualizing that kind.

124
00:12:39,600 --> 00:12:46,160
And you want to keep it as simple as possible, standing as fine, standing as I would

125
00:12:46,160 --> 00:12:52,160
argue better, it seems that the Buddha is, you know, if you read this, it's deep home here,

126
00:12:52,160 --> 00:12:56,640
I'm standing, which, you know, people would say, oh, you can't say, I'm standing,

127
00:12:56,640 --> 00:13:05,960
but the Buddha didn't, so it's just the simplest way to say, standing, standing.

128
00:13:05,960 --> 00:13:11,240
Is it dangerous to practice other types of meditation while also practicing mindful meditation

129
00:13:11,240 --> 00:13:17,360
not at the same time?

130
00:13:17,360 --> 00:13:22,440
Well, you can look up my video on other meditation types, I think that's what it's called

131
00:13:22,440 --> 00:13:25,120
other meditation techniques or something.

132
00:13:25,120 --> 00:13:29,640
There are four protective meditations, I hopefully there's actually one up there that's

133
00:13:29,640 --> 00:13:34,080
labeled protective meditations, there's four types of meditation that actually support

134
00:13:34,080 --> 00:13:40,960
your insight practice, other types of meditation, probably not so much, so I would recommend

135
00:13:40,960 --> 00:13:46,840
sticking to those four, if you wanted something that actually was going to be useful to augment

136
00:13:46,840 --> 00:13:53,760
your meditation practice, practice from time to time these other four meditations.

137
00:13:53,760 --> 00:13:58,200
They are, you know, look up the video, but they're quickly mindfulness of the Buddha,

138
00:13:58,200 --> 00:14:06,240
mindfulness of not in any particular mindfulness of load, someness of the body or the aspects

139
00:14:06,240 --> 00:14:11,600
of the body, loving kindness and mindfulness of death.

140
00:14:11,600 --> 00:14:19,840
I mean, I could argue that compassion goes along with loving kindness and moody time and

141
00:14:19,840 --> 00:14:31,520
community as well, but those four are the big ones.

142
00:14:31,520 --> 00:14:35,480
They have read chapters one and two of your booklet and I'd like to understand what you think is the

143
00:14:35,480 --> 00:14:40,080
larger goal of why we practice the four foundations of mindfulness.

144
00:14:40,080 --> 00:14:45,120
Is it to develop a clearer awareness continuously with what we can use to investigate on

145
00:14:45,120 --> 00:14:49,120
phenomena and see the characteristics and meditation?

146
00:14:49,120 --> 00:14:56,120
You know, that sounds good and it's probably okay, but I wouldn't overthink it.

147
00:14:56,120 --> 00:15:05,920
The goal is just to, well, the goal state, you know, that's the practice state is to just

148
00:15:05,920 --> 00:15:12,800
see things as they are, plain and simple, and thereby see impermanence, unsatisfactoriness

149
00:15:12,800 --> 00:15:23,080
and uncontrollability, see that things are unsaid, unstable, unsatisfying and uncontrollable,

150
00:15:23,080 --> 00:15:26,720
what you're going to see, you're going to be challenged by the practice.

151
00:15:26,720 --> 00:15:31,760
If you're doing it properly, it will be challenging and it will force you to reevaluate

152
00:15:31,760 --> 00:15:40,360
your attachments and it will force you to become more flexible as you see that your expectations

153
00:15:40,360 --> 00:15:47,920
and your rigidity of mind, your clinging to things is causing you suffering.

154
00:15:47,920 --> 00:15:53,240
I wouldn't over-intellectualize the goal because the practically you're just going, you're

155
00:15:53,240 --> 00:15:58,960
going to experience so much that what you should be focusing on is dealing with and learning

156
00:15:58,960 --> 00:16:02,000
about what you experience.

157
00:16:02,000 --> 00:16:14,160
That's basically what you're saying, so you're fine, I guess I'm always wary of putting

158
00:16:14,160 --> 00:16:15,680
it out in words like that.

159
00:16:15,680 --> 00:16:19,840
It sounds like actually you've got a good grasp on it.

160
00:16:19,840 --> 00:16:25,720
Sorry, this was, I think, another part of the original question, I find it's hard to keep

161
00:16:25,720 --> 00:16:29,760
up with the effort to pick up the four foundations of mindfulness throughout the day.

162
00:16:29,760 --> 00:16:33,360
I was hoping keeping the larger purpose, motivation in mind would help me.

163
00:16:33,360 --> 00:16:35,600
Let's see, that's what I was afraid of.

164
00:16:35,600 --> 00:16:38,720
You're trying to find a trick to make it easier.

165
00:16:38,720 --> 00:16:42,720
Anytime anyone says I find it hard, you've got to say, good, it's supposed to be hard.

166
00:16:42,720 --> 00:16:43,720
It's a challenge.

167
00:16:43,720 --> 00:16:45,560
It's meant to challenge you.

168
00:16:45,560 --> 00:16:49,560
You don't look for a trick, fight with it, work on it.

169
00:16:49,560 --> 00:16:55,640
When it's hard, that's when the work is happening because you're seeing, it's unpredictable,

170
00:16:55,640 --> 00:16:58,360
it's unsatisfying, and it's uncontrollable.

171
00:16:58,360 --> 00:17:02,840
The practice is that way, that's what you're supposed to see.

172
00:17:02,840 --> 00:17:06,800
So yeah, you have to work on it, there's no shortcut.

173
00:17:06,800 --> 00:17:12,120
Trying to find a trick that makes it, that helps you, it's not going to help you.

174
00:17:12,120 --> 00:17:14,440
Don't look for something to help you.

175
00:17:14,440 --> 00:17:20,280
Don't look on it, be more patient, be more methodical.

176
00:17:20,280 --> 00:17:26,640
There's no trick, it's quite clear what you have to do, it's just, we don't like the

177
00:17:26,640 --> 00:17:32,120
fact that it's so difficult, so we try to find a way around it, the shortcut.

178
00:17:32,120 --> 00:17:33,120
Don't look for shortcuts.

179
00:17:33,120 --> 00:17:37,640
That in and of itself is, or don't look for crutches or supports, because that in and

180
00:17:37,640 --> 00:17:42,440
of itself is an unwholesome mindset, it's laziness, it's common, I mean, it's not a criticism

181
00:17:42,440 --> 00:17:48,400
but something you have to be wary of, there's no shortcut.

182
00:17:48,400 --> 00:17:55,040
One day I have a question, and hopefully this isn't asking about a shortcut, but maybe

183
00:17:55,040 --> 00:18:00,880
it is phones with all their notifications, so distracting.

184
00:18:00,880 --> 00:18:10,280
When practicing daily meditation, if I hear notifications, it's not simple hearing, hearing

185
00:18:10,280 --> 00:18:16,280
for other ordinary sounds, I start to get worried, anxious, somebody need me, do I need

186
00:18:16,280 --> 00:18:19,120
to answer that.

187
00:18:19,120 --> 00:18:25,920
So would it be a better practice too, know all of the myriad of curiosity, worried, anxious,

188
00:18:25,920 --> 00:18:36,520
or just turn off the phone before daily meditation?

189
00:18:36,520 --> 00:18:42,520
You'd want to more look at your level and practice for a beginner, much better to turn

190
00:18:42,520 --> 00:18:44,600
off the phone.

191
00:18:44,600 --> 00:18:49,000
When you're doing an intensive course, you don't need that distraction, you're going to

192
00:18:49,000 --> 00:18:54,640
find all the deep stuff anyway, but during daily life for a continuing practitioner,

193
00:18:54,640 --> 00:18:59,360
I would say, you're going to have to learn to deal with these things, and you're strong

194
00:18:59,360 --> 00:19:04,080
enough through your practice too, recognize them and deal with them and do not get lost

195
00:19:04,080 --> 00:19:05,600
in them.

196
00:19:05,600 --> 00:19:13,120
So yeah, I would argue, to some extent, you would want to open yourself up to those.

197
00:19:13,120 --> 00:19:19,440
Now when you're really trying to be more intensive about your practice, I would just

198
00:19:19,440 --> 00:19:24,480
turn off the notification, because the other thing is even without the worry and so on,

199
00:19:24,480 --> 00:19:30,800
there's an obligation, there's a duty, and if you make a determination to focus on the

200
00:19:30,800 --> 00:19:37,400
meditation, you're basically saying, I'm not going to be in contact with people, because

201
00:19:37,400 --> 00:19:40,880
it does diffuse my meditation practice.

202
00:19:40,880 --> 00:19:48,040
I guess I would say, when it's when you're doing intensive practice, better to turn

203
00:19:48,040 --> 00:19:55,200
it off, but unless you're a beginner, for someone who's new, then I would argue probably

204
00:19:55,200 --> 00:20:01,080
turning it off, turning them off as the best, but if it's not intensive practice, you might

205
00:20:01,080 --> 00:20:05,200
want to turn them on and learn to deal with the worry.

206
00:20:05,200 --> 00:20:15,200
That's pretty much what I do now, but it is challenging, which is good, it's important.

207
00:20:15,200 --> 00:20:17,200
I recently had a major setback.

208
00:20:17,200 --> 00:20:22,080
I just recently got back from New Mexico, and on the way back, I lost my temper, and

209
00:20:22,080 --> 00:20:27,440
quite verbally, even yelling a few times, I felt all my progress and mindfulness was in

210
00:20:27,440 --> 00:20:28,440
vain.

211
00:20:28,440 --> 00:20:34,360
Is it normal to have to start over, or a feeling of starting over, I feel lousy about

212
00:20:34,360 --> 00:20:35,360
it?

213
00:20:35,360 --> 00:20:42,200
Yeah, I mean, you can't lose mindfulness, mindfulness is not really an aggregate quality,

214
00:20:42,200 --> 00:20:47,880
so that's something that gets bigger over time, that's concentration.

215
00:20:47,880 --> 00:20:51,680
That's more what meditators build up, and they build up concentration, to the extent that

216
00:20:51,680 --> 00:20:59,800
they think they gain something, and then to see the mindfulness pop like a bubble, it's

217
00:20:59,800 --> 00:21:03,640
somewhat disappointing, because we have expectations, and we think we've gained something

218
00:21:03,640 --> 00:21:06,240
real when we actually have it.

219
00:21:06,240 --> 00:21:13,360
But mindfulness will allow you to know that you're angry, and the fact that you're aware

220
00:21:13,360 --> 00:21:18,720
that you're angry, and that you're aware now that you feel lousy about it, all of that

221
00:21:18,720 --> 00:21:28,560
is it's a start, being aware of these things is a sign of a mindful awareness, or a mindful

222
00:21:28,560 --> 00:21:33,040
quality to your mind.

223
00:21:33,040 --> 00:21:42,120
Progress will be getting less angry about things, but in the beginning it's just more

224
00:21:42,120 --> 00:21:49,880
accepting that they're there, and learning to identify them, rather than, and this is maybe

225
00:21:49,880 --> 00:21:56,000
where acceptance could come in, is that rather than pretending they're not there, or trying

226
00:21:56,000 --> 00:22:00,360
to wish them away, accepting that you've got a problem and that you've got to deal with it,

227
00:22:00,360 --> 00:22:04,360
it's sort of a temporary acceptance and the sense of accepting that you're going to have

228
00:22:04,360 --> 00:22:10,600
to deal with these things, rather than just run away from them, that they're still there,

229
00:22:10,600 --> 00:22:18,320
accepting the fact that you're not enlightened yet, when you've got work to do.

230
00:22:18,320 --> 00:22:21,680
So I wouldn't feel discouraged about that at all.

231
00:22:21,680 --> 00:22:26,280
I wouldn't take it as an opportunity to deal with these emotions, when you feel bad about

232
00:22:26,280 --> 00:22:37,840
it, say, upset, upset, or disliking, disliking, sad, sad, angry, frustrated, don't think of

233
00:22:37,840 --> 00:22:38,840
it as a setback.

234
00:22:38,840 --> 00:22:44,400
It was a mistake, it was a part of the problem, it was a product of the problem that you

235
00:22:44,400 --> 00:22:45,400
have inside.

236
00:22:45,400 --> 00:22:53,880
Okay, well, we know what the problem is, it's like when you, when a plumber tries to find

237
00:22:53,880 --> 00:22:57,480
a drain, a clog in the drain, it's great when you find it, nah, now you've found the

238
00:22:57,480 --> 00:23:03,680
clog, or when a computer programmer finds a bug, they found the bug in the program, now

239
00:23:03,680 --> 00:23:06,080
they just have to fix it.

240
00:23:06,080 --> 00:23:21,680
So you've found the bug, and you just have to fix it, and you're all caught up on questions

241
00:23:21,680 --> 00:23:39,740
and you're all right.

242
00:23:39,740 --> 00:23:46,120
He said, he took me aside before the talk, and he's got all serious and said, I want

243
00:23:46,120 --> 00:23:48,160
to tell you, these are the most like Christians.

244
00:23:48,160 --> 00:23:53,240
I thought he was going to say, and I know you've been bashing Christians lately, but he

245
00:23:53,240 --> 00:23:54,240
doesn't know.

246
00:23:54,240 --> 00:23:58,120
I thought that's what he was going to tell, he's going to, you know, I would don't

247
00:23:58,120 --> 00:23:59,120
eat a bash.

248
00:23:59,120 --> 00:24:05,160
Chris, because I'm not very friendly towards Christian, you know, I can, it was, it was

249
00:24:05,160 --> 00:24:06,160
good.

250
00:24:06,160 --> 00:24:07,160
I think they were happy with it.

251
00:24:07,160 --> 00:24:13,120
I mean, it was interesting because they're Christian, but the woman who sort of organized

252
00:24:13,120 --> 00:24:18,480
it, her husband and her daughter died in a car crash.

253
00:24:18,480 --> 00:24:23,040
And she was just, of course, devastated.

254
00:24:23,040 --> 00:24:28,920
And she came to the monastery there and it was amongst there who helped her deal with

255
00:24:28,920 --> 00:24:34,240
her sadness or loss three years ago.

256
00:24:34,240 --> 00:24:38,160
And so she actually is very much impressed by Buddhism.

257
00:24:38,160 --> 00:24:45,120
And I think she was actually somewhat critical of, she sort of left her Christian faith

258
00:24:45,120 --> 00:24:55,400
because it didn't really help her and it certainly couldn't explain why they had to die.

259
00:24:55,400 --> 00:25:06,360
It's all part of some mythical beings planned for us to be ashamed to have to be stuck

260
00:25:06,360 --> 00:25:11,800
on that kind of meaningless belief.

261
00:25:11,800 --> 00:25:20,160
All right, anyway, and on that note, have a good night, everyone.

262
00:25:20,160 --> 00:25:23,120
Thank you, Robin, for joining me on the social.

263
00:25:23,120 --> 00:25:24,120
Thank you, Bante.

264
00:25:24,120 --> 00:25:25,120
Good night.

265
00:25:25,120 --> 00:25:26,120
Thank you.

