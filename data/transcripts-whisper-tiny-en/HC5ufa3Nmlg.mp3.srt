1
00:00:00,000 --> 00:00:08,360
and welcome back to S.K. Monk. Today's question comes from T. Master Yoda T.

2
00:00:08,360 --> 00:00:12,960
Now that I'm sitting here perceiving the world and accepting what is, I was wondering

3
00:00:12,960 --> 00:00:18,520
how one can be really proactive as a Buddhist. Meditating and trying to unravel oneself

4
00:00:18,520 --> 00:00:29,840
thoughts does seem to refrain from taking action needed to change or not or not.

5
00:00:29,840 --> 00:00:41,440
Two things, I suppose. The first one, three things. The first one is, I'm not going to pass

6
00:00:41,440 --> 00:00:49,520
judgment, but it's hard to believe that you could just sit down and suddenly start accepting

7
00:00:49,520 --> 00:00:57,480
the world, accepting what is. You have to understand that it's not just, it's not something

8
00:00:57,480 --> 00:01:02,360
that's easy to do or it's not something that you just do. You sit down, okay. Now I'm

9
00:01:02,360 --> 00:01:11,160
accepting the world what next. The ability to accept things for what they are is enlightenment,

10
00:01:11,160 --> 00:01:17,720
and I suppose it's easy to think that suddenly you're accepting things as they are, but the

11
00:01:17,720 --> 00:01:24,920
fact that you're asking this question shows that there is quite likely non-acceptance

12
00:01:24,920 --> 00:01:28,680
in the need for something more. The fact that you're sitting there and saying what next

13
00:01:28,680 --> 00:01:35,800
shows that there's some need in the mind for something greater. Actually accepting what

14
00:01:35,800 --> 00:01:47,040
is, is a state that affects a profound change on who you are. It's not just, you sit there,

15
00:01:47,040 --> 00:01:50,560
everything else is the same except now you accept everything for what it is. It changes

16
00:01:50,560 --> 00:01:58,400
your whole life. You have to go through great upheaval in your life and in the way you look

17
00:01:58,400 --> 00:02:06,320
at the world. It's something, it's an incredibly difficult and generally lengthy process

18
00:02:06,320 --> 00:02:16,840
that requires you to analyze and to reevaluate the whole of your life in terms of how you

19
00:02:16,840 --> 00:02:25,320
look at the world, how you interact with other people and so on. So I wouldn't misunderstand

20
00:02:25,320 --> 00:02:32,520
that as soon as you sit down and say to yourself, rising, falling or seeing, seeing, hearing,

21
00:02:32,520 --> 00:02:37,000
hearing or so on, suddenly you accept things. Know what this does is allows you to see the

22
00:02:37,000 --> 00:02:40,520
things that you don't accept, the friction that you have, the tension that you have in your

23
00:02:40,520 --> 00:02:47,360
mind that's causing you suffering. That's the first thing. The second thing is the whole

24
00:02:47,360 --> 00:02:59,840
idea of being proactive is in many ways, I would say a misunderstanding of what's good

25
00:02:59,840 --> 00:03:05,800
and what's right or misreading of the world in general. The idea that the problems in the

26
00:03:05,800 --> 00:03:14,440
world can somehow be fixed if we all just work harder or do more. I would say that in fact

27
00:03:14,440 --> 00:03:18,320
most of the problems in the world come from the fact that we're doing too much, that

28
00:03:18,320 --> 00:03:24,320
we're seeking after, that we're trying to fix and to force and to make things according

29
00:03:24,320 --> 00:03:33,560
to our wishes. If you look at the rise of civilization, of human civilization, it has

30
00:03:33,560 --> 00:03:39,480
mostly been because of our inability to accept things as they are. Things are not comfortable

31
00:03:39,480 --> 00:03:46,320
enough, they're not convenient enough, they're not satisfying enough. And so we create greater

32
00:03:46,320 --> 00:03:53,720
and more immediate pleasures again and again adding to the burden that we have to carry

33
00:03:53,720 --> 00:03:57,880
around and the burden that we're putting on the world and on the natural resources around

34
00:03:57,880 --> 00:04:05,400
us. Instead of simply accepting things for what they are, which would leave the world

35
00:04:05,400 --> 00:04:12,280
in the state of harmony and peace if we were all content and were able to follow this

36
00:04:12,280 --> 00:04:19,080
sort of idea of simply accepting things or at least understanding things for what they

37
00:04:19,080 --> 00:04:30,480
are, then we wouldn't have the problems that we see in the world today. Most of them.

38
00:04:30,480 --> 00:04:37,760
The third thing is that even in those cases where it is necessary to be proactive, where

39
00:04:37,760 --> 00:04:46,160
we have to help other people or where we should encourage and support people in good

40
00:04:46,160 --> 00:04:54,960
things and in developing themselves and in helping the world, it's impossible to think

41
00:04:54,960 --> 00:05:00,680
that it's absurd to think that you can just go out and help and you can just go out

42
00:05:00,680 --> 00:05:08,760
and work in the world without any sort of training in terms of your own purification.

43
00:05:08,760 --> 00:05:14,080
The idea that we can just go out and help other people and that's what we should be doing

44
00:05:14,080 --> 00:05:23,760
is fallacious for the reason that the only way you can help people is if you are a helpful

45
00:05:23,760 --> 00:05:32,360
person, if you're a person who has negative states of mind, what you're going to pass

46
00:05:32,360 --> 00:05:35,880
on to other people are those negative states of mind. If you're a person who is stressed

47
00:05:35,880 --> 00:05:42,160
out, if you're a person who gets angry easily, if you're a person who is addicted and attached

48
00:05:42,160 --> 00:05:46,040
to central pleasures, you're not going to go out and help other people. You're going to

49
00:05:46,040 --> 00:05:53,760
go out and twist and turn them for your own benefit and harm them by your own mental

50
00:05:53,760 --> 00:05:59,360
defilements. I think this is what we see. There are many organizations that claim to be

51
00:05:59,360 --> 00:06:06,880
doing good or wish for good things to rise but end up suffering a lot and having great difficulty

52
00:06:06,880 --> 00:06:13,800
doing what they're doing because of the conflict and the tension, the anger, the frustration,

53
00:06:13,800 --> 00:06:26,080
the stress and so on, that assails them in their own minds. This is like, as I've said,

54
00:06:26,080 --> 00:06:36,800
before to a river, if any river or stream a water source is impure at the source,

55
00:06:36,800 --> 00:06:43,560
then all down the stream, no matter what comes in contact with it, they can't get any

56
00:06:43,560 --> 00:06:49,400
benefit from that water, no matter anywhere along the stream, anywhere down the river.

57
00:06:49,400 --> 00:06:57,280
If the source is only if the source is pure, can the rest of the stream be of any use

58
00:06:57,280 --> 00:07:02,480
and the same is with our mind. If our mind is impure, if we act or speak with an impure

59
00:07:02,480 --> 00:07:11,400
mind, then we don't bring happiness, we only bring suffering. So meditation in that sense,

60
00:07:11,400 --> 00:07:15,960
it starts with a training, it starts with a personal training where you close the door,

61
00:07:15,960 --> 00:07:21,120
close your eyes, block out the world and start from square one, you know, look at the

62
00:07:21,120 --> 00:07:31,400
very kernel, the source of all of your actions, all of your manifestations in the world

63
00:07:31,400 --> 00:07:35,960
and start to clean that, start to purify that. As you get better at that, then you move

64
00:07:35,960 --> 00:07:42,160
outward and your meditation slowly enters into your daily life. As you practice on and

65
00:07:42,160 --> 00:07:46,880
on, eventually your daily life is meditation. When you meet with people and you get angry,

66
00:07:46,880 --> 00:07:51,760
you're watching the anger, you're looking at it and you're trying to clean that as well,

67
00:07:51,760 --> 00:07:58,560
you're trying to purify that. So on the one hand, the idea that meditation is only for

68
00:07:58,560 --> 00:08:01,760
the sitting mat with the closed door and therefore there's nothing to do with the world

69
00:08:01,760 --> 00:08:08,680
is false. But on the other hand, it has to start from within, you can't just go out in

70
00:08:08,680 --> 00:08:12,240
the world and meditate, you can't just walk down the road and say, okay, I'm going to meditate,

71
00:08:12,240 --> 00:08:18,360
I'm going to be mindful of everything I'm going to consider things carefully. You're

72
00:08:18,360 --> 00:08:27,120
dealing with so much, so many layers of dirt and defilement in the mind and misunderstandings

73
00:08:27,120 --> 00:08:32,440
that exist in the mind, that it's highly unlikely that you'd be able to do such a thing

74
00:08:32,440 --> 00:08:39,960
and conversely, you're more likely to harm others, to harm yourself and to harm others

75
00:08:39,960 --> 00:08:46,560
and to feel bad about it later on. So retreating is coming back to the basics and starting

76
00:08:46,560 --> 00:08:51,400
off from the very kernel of our being, which is our mind. When we close our eyes, we start

77
00:08:51,400 --> 00:08:55,760
to see how our mind reacts to very simple things, reacts to the stomach rising and falling

78
00:08:55,760 --> 00:09:01,360
and reacts to having to sit still, reacts to pain, reacts to the building blocks that

79
00:09:01,360 --> 00:09:06,120
make up our everyday life. Once we get better at that, then we move out into our daily

80
00:09:06,120 --> 00:09:11,120
life and actually incorporate the meditation practice into our lives. Meditation should

81
00:09:11,120 --> 00:09:18,120
not be something that is external to your daily life, an addition or a segregated part,

82
00:09:18,120 --> 00:09:23,400
your life should become meditative. If your life is not becoming meditative, you'll never

83
00:09:23,400 --> 00:09:29,120
succeed in the meditation practice in becoming enlightened. Your whole life has to be meditation.

84
00:09:29,120 --> 00:09:35,480
Meditation is a way of life, not an addition to life. So in that sense, it will be a great

85
00:09:35,480 --> 00:09:40,560
value to you in anything you do, in any time you wish to help others or do good things

86
00:09:40,560 --> 00:09:46,680
for the world. Meditation will keep you balanced and reasonable and able to see things

87
00:09:46,680 --> 00:09:53,680
as they are and react properly, appropriately to every situation. Okay, so I hope that

88
00:09:53,680 --> 00:10:18,680
helps. Good question, it's one that's asked often, so yeah, there's my answer.

