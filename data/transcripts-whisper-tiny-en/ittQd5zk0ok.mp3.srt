1
00:00:00,000 --> 00:00:03,000
Hi, welcome back to Ask a Monk.

2
00:00:03,000 --> 00:00:08,000
Today I will be answering a question about how to become a monk.

3
00:00:08,000 --> 00:00:18,000
I get this question a lot and I believe someone recently asked it on the Google Moderator page.

4
00:00:18,000 --> 00:00:29,000
Being a non-ethnic Buddhist, finding it difficult to find a place or a way to become or how to get started on the path to become a monk.

5
00:00:29,000 --> 00:00:43,000
So I'm just going to go through some of the things that some of the steps that one has to take or I would say are useful to take or a part of becoming a monastic.

6
00:00:43,000 --> 00:00:47,000
The first one is to prepare yourself obviously.

7
00:00:47,000 --> 00:00:53,000
A lot of details that you have to take care of making sure that you're not in debt.

8
00:00:53,000 --> 00:01:05,000
Making sure that you don't have any unfinished business or bad relationships that are going to come back and haunt you, say with your parents or with your loved ones.

9
00:01:05,000 --> 00:01:20,000
That once you go on practice and stay at a monastery are going to haunt you and are going to be a cause for you to change your mind and realize that you don't have unfinished business you have to take care of.

10
00:01:20,000 --> 00:01:33,000
There are many cases of people who go to a monastery with the intention of staying for months or years or even their whole life and after a week or two, change their mind and leave.

11
00:01:33,000 --> 00:01:37,000
Not because of the monastery but because they have unfinished business.

12
00:01:37,000 --> 00:01:53,000
So taking care of your business, all of these externalities and it's important to maybe do a little bit of research into some of the things that are required for you to become a monk.

13
00:01:53,000 --> 00:02:05,000
For instance, being free from debt and there are many technicalities that are going to be required if you're going to be accepted as a monk.

14
00:02:05,000 --> 00:02:08,000
The other thing is to prepare yourself mentally.

15
00:02:08,000 --> 00:02:21,000
So I would never even think about becoming a monk if you haven't started practicing meditation and it may even be a good idea to go to a meditation course before you think about becoming a monk.

16
00:02:21,000 --> 00:02:38,000
On the other hand, it might be a part of you becoming a monk. Go to the monastery, start to meditate, spend some time meditating and then become a monk.

17
00:02:38,000 --> 00:02:52,000
As long as you're prepared that when you go to the center you're going to, your intention is to become ordained.

18
00:02:52,000 --> 00:03:17,000
So finding a meditation center that is also a monastery where there's the option of ordaining. The whole deal with ethnicity to me is a red herring. I wouldn't worry about your ethnicity and not being this or that ethnicity because I would say one should avoid monasteries that are culturally based.

19
00:03:17,000 --> 00:03:24,000
Unless of course you happen to belong to that culture.

20
00:03:24,000 --> 00:03:35,000
But even then, even if you do belong to the culture, I have some sort of bias against these sort of centers because they don't tend to teach very pure or deep Buddhism.

21
00:03:35,000 --> 00:03:59,000
They tend to have minimal meditation, practice and sort of a kind of a mixed understanding of the Buddha's teaching where they mix it with the culture of their whatever country and it becomes more of a social gathering or community center.

22
00:03:59,000 --> 00:04:15,000
Which is good for that culture but is certainly not good for Buddhist practice because it tends to lean to ceremonies, celebrations and a lot of things that are actually antithetical to the practice of the Buddha's teaching.

23
00:04:15,000 --> 00:04:36,000
So avoid centers like that. Try to find a meditation center which also happens to be a monastery rather than the other way around. A monastery which has some mention of meditation. It should be a monastery where one of the first things you read on the home page or one of the links on the home pages to meditation.

24
00:04:36,000 --> 00:04:49,000
In terms of saying, oh, we do five, ten minutes of meditation here or there or we can teach you this. But they actually have meditation courses or residential stays for meditators.

25
00:04:49,000 --> 00:04:56,000
Because that is an indication that their emphasis is on meditation rather than ordination.

26
00:04:56,000 --> 00:05:04,000
And ordination is seen as an extension of the meditation practice for the purpose of practicing meditation on the long term.

27
00:05:04,000 --> 00:05:14,000
And once you find a place, then it's not much that can be said because you're going to have to just follow their rules and regulations.

28
00:05:14,000 --> 00:05:27,000
Ask them what it's going to take for you to ordain. Make sure that there's the opportunity. Try to be careful of places that say, oh, well, yes. Try to avoid the question.

29
00:05:27,000 --> 00:05:39,000
If you ask, can you become a monk or monastic? And they avoid the questions and come and meditate. It's often a sign that no, they won't let you ordain, but they want you to come and meditate.

30
00:05:39,000 --> 00:05:49,000
And they want you to put aside the idea of ordaining. Try to get a positive answer there from the get goes so that you're not disappointed later on.

31
00:05:49,000 --> 00:06:05,000
Once you get to the center, don't have any expectations once you've got an assurance that if you practice for such and such a time, they're going to let you ordain or once they see that you practice is proper.

32
00:06:05,000 --> 00:06:15,000
They're going to let you ordain and just go with it. Try to forget about the future. That's going to be the most important thing in becoming a monk is to forget about time.

33
00:06:15,000 --> 00:06:26,000
But the idea of one month, one year, how long you've been here, how long you have to stay and so on. Try to be present and go step by step.

34
00:06:26,000 --> 00:06:39,000
And don't be in a hurry to ordain. Try to get your meditation practice solidified first. Once you're set in the meditation practice, then it shouldn't be a problem for you to ordain and you'll be much better prepared.

35
00:06:39,000 --> 00:06:48,000
Once you've become a monk, the quest doesn't end there, of course. It only starts there.

36
00:06:48,000 --> 00:07:06,000
And the biggest problem for monks is the difficulty in adapting to the lifestyle and the difficulty in being patient with the monastic system.

37
00:07:06,000 --> 00:07:17,000
Especially nowadays when many things are different, of course, from in the Buddha's time and monks don't always just live up in the forest in peace and quiet.

38
00:07:17,000 --> 00:07:30,000
There's a lot of problems. People are less patient and have shorter attention spans and so on.

39
00:07:30,000 --> 00:07:47,000
You're going to be dealing with all sorts of different kinds of people. The most important thing once you become a monk to make note of is your own assessment of your practice, assessment of your progress as a monk.

40
00:07:47,000 --> 00:08:11,000
And from my own practice, my own history and watching other people who become a monk, it seems like in about the first month of being a monk, you've already got this idea that you're the best monk out there or you're a perfect monk or you're an excellent monk.

41
00:08:11,000 --> 00:08:23,000
And it goes like this, after a monk you think you're perfect, after about a year, you start to realize that you weren't perfect, but now you're good, now you're after a year, you're a really good monk.

42
00:08:23,000 --> 00:08:28,000
And then after about five years, you realize that you weren't a very good monk, but now you're a passable monk.

43
00:08:28,000 --> 00:08:36,000
And then after I would say about ten years, you start to let go of the idea and finally say to yourself, I'm just a monk.

44
00:08:36,000 --> 00:08:41,000
And so it sort of goes downhill and this is a losing of your ego.

45
00:08:41,000 --> 00:08:46,000
So the reason why people fail often is not because they feel that they are unable to do it.

46
00:08:46,000 --> 00:08:50,000
It's that they feel that they're too good for everybody else.

47
00:08:50,000 --> 00:08:55,000
And everyone else is practicing incorrectly, only I'm practicing correctly.

48
00:08:55,000 --> 00:09:00,000
And they find themselves unable to deal with other people's defilements.

49
00:09:00,000 --> 00:09:06,000
And the people around them are causing problems and they don't see their own problems.

50
00:09:06,000 --> 00:09:13,000
And so they feel like Buddhism is just a sham or there's no one practicing correctly.

51
00:09:13,000 --> 00:09:22,000
Buddhism is very important to be patient and to understand that yes, the people around us, the other monastics may not be perfect monks.

52
00:09:22,000 --> 00:09:24,000
But are they trying?

53
00:09:24,000 --> 00:09:28,000
And I've seen monks with a lot of problems.

54
00:09:28,000 --> 00:09:35,000
And I think what helped me through it and helped me to let go of it was answering the question, are they really trying?

55
00:09:35,000 --> 00:09:41,000
And to see that actually they are trying and they're trying to help themselves.

56
00:09:41,000 --> 00:09:43,000
They're trying to be better people.

57
00:09:43,000 --> 00:09:51,000
You should never discount someone's efforts just because they do and say bad things sometimes.

58
00:09:51,000 --> 00:09:57,000
We all get angry, we all get upset, we all have problems and that's why we're here to practice.

59
00:09:57,000 --> 00:10:06,000
And the other thing about being a monk is keeping to yourself, not getting busy with groups, not getting busy with society.

60
00:10:06,000 --> 00:10:18,000
And I think that's one of the big problems that has entered into the monastic life is people like to sit around joking and chatting and talking rather than staying to yourself.

61
00:10:18,000 --> 00:10:31,000
A monk ultimately is someone who stays by themselves and that's what you should keep in mind that you shouldn't be seeking out groups or seeking out company.

62
00:10:31,000 --> 00:10:37,000
You should be seeking out solitude, seeking out quiet with a practice of meditation.

63
00:10:37,000 --> 00:10:50,000
And well that's about it, I hope that's useful for anyone interested in becoming a monk and if you're like more detailed information, you're welcome to contact me.

64
00:10:50,000 --> 00:10:58,000
I could probably point out a couple of places that I think are still open to the idea of ordaining westerners.

65
00:10:58,000 --> 00:11:08,000
And of course in the not so distant future, I'll be starting the process here to start up a monastery in the California area.

66
00:11:08,000 --> 00:11:32,000
Okay, so thanks for tuning in and all the best.

