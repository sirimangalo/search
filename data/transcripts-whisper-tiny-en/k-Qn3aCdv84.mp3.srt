1
00:00:00,000 --> 00:00:20,000
He said to me, so it wasn't any of you, and some of the things he would talk to you for a little bit, because he said,

2
00:00:20,000 --> 00:00:25,000
oh, I can talk to you in time, because he knows it's hard to talk to you and he can't give you too much.

3
00:00:25,000 --> 00:00:34,000
But at one point, he said to me, he said, no, I know, I don't know.

4
00:00:34,000 --> 00:00:46,000
I think of the Buddha, all the perfections for four uncountable eons and 100,000 great eons that he developed.

5
00:00:46,000 --> 00:00:53,000
He could have had anything he wanted, and he didn't want anything.

6
00:00:53,000 --> 00:00:56,000
And it just hit me, this was Nivita, I think.

7
00:00:56,000 --> 00:01:00,000
Nivita, you know, wherever they were right, he said, he said,

8
00:01:00,000 --> 00:01:03,000
think of the Buddha, whatever he wanted.

9
00:01:03,000 --> 00:01:10,000
If he wanted to be ahead of the ruler of the universe, he could have been.

10
00:01:10,000 --> 00:01:13,000
Whatever he wanted, he didn't want anything.

11
00:01:13,000 --> 00:01:15,000
Not one thing.

12
00:01:15,000 --> 00:01:20,000
Some may and then he used this for some way, dhamma, dhamma, dhamma, he knew himself.

13
00:01:20,000 --> 00:01:24,000
Nothing is worth thinking. No dhamma is worth thinking.

14
00:01:24,000 --> 00:01:28,000
And that really didn't mean it was like, I mean, it's an obvious thing to say,

15
00:01:28,000 --> 00:01:31,000
but it really, you go back to your community.

16
00:01:31,000 --> 00:01:37,000
We're really, you know, what is, how could I cling to something with the Buddha?

17
00:01:37,000 --> 00:01:43,000
He could have had anything, and he didn't cling to the things that I wish for that I want for them.

18
00:01:43,000 --> 00:01:48,000
Nothing compared to the wonder, the power, the pleasure that he could have had.

19
00:01:48,000 --> 00:01:54,000
And he threw it all away and gave it on.

20
00:01:54,000 --> 00:02:01,000
So that was something I was really quite wonderful with putting it.

21
00:02:01,000 --> 00:02:07,000
The feeling you get from my family says that is that he's actually,

22
00:02:07,000 --> 00:02:10,000
this is something he understands and experiences himself.

23
00:02:10,000 --> 00:02:11,000
We don't know.

24
00:02:11,000 --> 00:02:17,000
We can't say what someone else's attainment is, but this is the guy who really needs what he says.

25
00:02:17,000 --> 00:02:26,000
It's like, when you hear it said from someone who is there,

26
00:02:26,000 --> 00:02:31,000
and who really experiences these things from himself,

27
00:02:31,000 --> 00:02:34,000
because the teaching you get is quite simple to me.

28
00:02:34,000 --> 00:02:37,000
It's the same thing again and again and again, it's exactly

29
00:02:37,000 --> 00:02:41,000
that people need to know and need to hear and you hear it from him.

30
00:02:41,000 --> 00:02:44,000
Someone who actually means it eventually knows it.

31
00:02:44,000 --> 00:02:48,000
It's quite different from someone who's repeating it, which is fair.

32
00:02:48,000 --> 00:02:50,000
So, I was really happy.

33
00:02:50,000 --> 00:02:54,000
And then I didn't really have any great happiness with the fact that all of you got a chance.

34
00:02:54,000 --> 00:02:58,000
All of you know, people, I was able to bring people to a adjunct on the day,

35
00:02:58,000 --> 00:03:01,000
and to have them do a course.

36
00:03:01,000 --> 00:03:05,000
You know, it's part of passing this legacy on because he's not going to be around for him.

37
00:03:05,000 --> 00:03:06,000
He's gone.

38
00:03:06,000 --> 00:03:08,000
All we're going to have is stories that go.

39
00:03:08,000 --> 00:03:14,000
You've never had a chance to practice a adjunct on.

40
00:03:14,000 --> 00:03:26,000
I'm so happy that I kind of pushed that you go and then I had to come.

41
00:03:26,000 --> 00:03:29,000
Oh, yeah, you pushed me to go.

42
00:03:29,000 --> 00:03:39,000
It worked out really well.

43
00:03:39,000 --> 00:03:43,000
Everything worked out really well for this trip.

44
00:03:43,000 --> 00:03:46,000
Everything has worked out so incredibly well.

45
00:03:46,000 --> 00:03:48,000
I mean, you guys may not realize it.

46
00:03:48,000 --> 00:03:50,000
You haven't followed me around.

47
00:03:50,000 --> 00:03:55,000
You probably realize that you have how horrible things go most of them.

48
00:03:55,000 --> 00:03:58,000
And so how wonderful it's been.

49
00:03:58,000 --> 00:04:04,000
No, no reporting with that chance, even though we said we wanted to come to the presentation.

50
00:04:04,000 --> 00:04:06,000
I was like, and I just had me honest.

51
00:04:06,000 --> 00:04:07,000
And I said, we're joking about it.

52
00:04:07,000 --> 00:04:09,000
I said, yeah, this is a great birthday present.

53
00:04:09,000 --> 00:04:09,000
Thanks.

54
00:04:09,000 --> 00:04:11,000
Happy birthday to us.

55
00:04:11,000 --> 00:04:12,000
Thank you a lot of guys.

56
00:04:12,000 --> 00:04:17,000
It's not really proper to mind complaining, but it's kind of fun to hear.

57
00:04:17,000 --> 00:04:19,000
So let's just sit and complain.

58
00:04:19,000 --> 00:04:20,000
Gee, thanks.

59
00:04:20,000 --> 00:04:21,000
Happy birthday to me.

60
00:04:21,000 --> 00:04:23,000
And so we sat around for a while.

61
00:04:23,000 --> 00:04:25,000
And then we finally got to come to it.

62
00:04:25,000 --> 00:04:28,000
And it ended everything down the place.

63
00:04:28,000 --> 00:04:31,000
And except for the temperature.

64
00:04:31,000 --> 00:04:35,000
The forecast was supposed to rain the whole time we were there.

65
00:04:35,000 --> 00:04:38,000
And it kept supposing to rain and pretending to rain.

66
00:04:38,000 --> 00:04:39,000
And there was lots of thunder.

67
00:04:39,000 --> 00:04:40,000
That was the funny part.

68
00:04:40,000 --> 00:04:43,000
John Tom's great for lots of thunder and no rain.

69
00:04:43,000 --> 00:04:45,000
We could have talked about this.

70
00:04:45,000 --> 00:04:47,000
There are people who thunder and don't rain.

71
00:04:47,000 --> 00:04:49,000
This is people who talk and practice.

72
00:04:49,000 --> 00:04:52,000
There are people who rain and don't thunder.

73
00:04:52,000 --> 00:04:55,000
This is people who practice but don't teach.

74
00:04:55,000 --> 00:04:58,000
There are people who don't have rain or thunder.

75
00:04:58,000 --> 00:05:00,000
There's a lot of practice or teach.

76
00:05:00,000 --> 00:05:05,000
And there are people who rain and don't have a practice and a teach.

77
00:05:05,000 --> 00:05:08,000
So the weather and John Tom is like that.

78
00:05:08,000 --> 00:05:11,000
It actually does tell you a lot.

79
00:05:11,000 --> 00:05:13,000
And everything is work well.

80
00:05:13,000 --> 00:05:15,000
Then we got to Chiang Mai.

81
00:05:15,000 --> 00:05:17,000
And then we were going to Chiang Mai.

82
00:05:17,000 --> 00:05:18,000
And everything fell in the place.

83
00:05:18,000 --> 00:05:21,000
And we helped with my supporters and Chiang Mai.

84
00:05:21,000 --> 00:05:24,000
And we had lunch with them.

85
00:05:24,000 --> 00:05:26,000
They offered to bring me a lot of time.

86
00:05:26,000 --> 00:05:27,000
And I was kind of concerned.

87
00:05:27,000 --> 00:05:29,000
I was going to be OK going there.

88
00:05:29,000 --> 00:05:32,000
And it was the month going to be like the new month that's there.

89
00:05:32,000 --> 00:05:36,000
And he was like, oh, I don't even want to be here.

90
00:05:36,000 --> 00:05:38,000
Please come and help.

91
00:05:38,000 --> 00:05:40,000
He wants to go back to his own town.

92
00:05:40,000 --> 00:05:41,000
So he wants me to help.

93
00:05:41,000 --> 00:05:45,000
And he was so happy.

94
00:05:45,000 --> 00:05:46,000
We came.

95
00:05:46,000 --> 00:05:50,000
And that maybe I'll be able to help him to bring you to business.

96
00:05:50,000 --> 00:05:53,000
I know if Pranola comes, he's got lots of students

97
00:05:53,000 --> 00:05:54,000
and he can help.

98
00:05:54,000 --> 00:05:59,000
And he'll be able to take care to help to build this place up.

99
00:05:59,000 --> 00:06:01,000
I don't believe it.

100
00:06:01,000 --> 00:06:05,000
My near there, you can use the opportunity to spend time

101
00:06:05,000 --> 00:06:06,000
in this village.

102
00:06:06,000 --> 00:06:07,000
That's just great.

103
00:06:07,000 --> 00:06:12,000
The idea is that we can do a switch for a month or something.

104
00:06:12,000 --> 00:06:14,000
Anyway, that's a lot.

105
00:06:14,000 --> 00:06:17,000
There are a couple of insights that I wanted to share.

106
00:06:17,000 --> 00:06:20,000
I don't know how much to get into.

107
00:06:20,000 --> 00:06:22,000
But two things that really hit me during the course.

108
00:06:22,000 --> 00:06:25,000
One is this idea of happiness.

109
00:06:25,000 --> 00:06:27,000
And I don't know if I can put a coach.

110
00:06:27,000 --> 00:06:29,000
They were a little bit tired because we've been really

111
00:06:29,000 --> 00:06:35,000
worn out this past moving around and traveling and so on.

112
00:06:35,000 --> 00:06:39,000
First of all, that happiness, when we talk about happiness,

113
00:06:39,000 --> 00:06:42,000
because happiness is really the key.

114
00:06:42,000 --> 00:06:44,000
And it's what brings you to the meditation practice.

115
00:06:44,000 --> 00:06:49,000
Happiness is what we expect to get under the practice.

116
00:06:49,000 --> 00:06:54,000
But the curious thing, when you talk about happiness as being a thing,

117
00:06:54,000 --> 00:06:58,000
then suddenly you're talking about an experience.

118
00:06:58,000 --> 00:07:01,000
You're talking about ultimate reality.

119
00:07:01,000 --> 00:07:05,000
When we say this is happiness, like this water is happiness,

120
00:07:05,000 --> 00:07:10,000
or a place is happiness, or a person makes us happy.

121
00:07:10,000 --> 00:07:16,000
Or like food makes us happy, or our addictions make us happy.

122
00:07:16,000 --> 00:07:20,000
What we're actually talking about is ultimate reality.

123
00:07:20,000 --> 00:07:25,000
We think that there is an experience and that is happiness.

124
00:07:25,000 --> 00:07:28,000
We have this view and this idea of an entity

125
00:07:28,000 --> 00:07:33,000
for a situation that makes us happy.

126
00:07:33,000 --> 00:07:37,000
But the truth is, we're talking about individual experiences

127
00:07:37,000 --> 00:07:43,000
that have to do with some happiness and some desire

128
00:07:43,000 --> 00:07:45,000
and some experiences and so on.

129
00:07:45,000 --> 00:07:48,000
And when you actually look at those things,

130
00:07:48,000 --> 00:07:51,000
none of those ultimate realities are truly happiness.

131
00:07:51,000 --> 00:07:54,000
So the curious thing is, in the end, you don't find happiness.

132
00:07:54,000 --> 00:07:57,000
And happiness is not what you get on the practice.

133
00:07:57,000 --> 00:08:02,000
What you get is the realization that this thing that we call happiness

134
00:08:02,000 --> 00:08:08,000
is not actually real.

135
00:08:08,000 --> 00:08:11,000
It's made up of things that are not happening,

136
00:08:11,000 --> 00:08:13,000
that are just experiences.

137
00:08:13,000 --> 00:08:16,000
I'm not going to turn out that.

138
00:08:16,000 --> 00:08:19,000
Whatever little pleasure you get out of what you call happiness

139
00:08:19,000 --> 00:08:20,000
is just a moment.

140
00:08:20,000 --> 00:08:24,000
It's a brief moment in that experience and that situation.

141
00:08:24,000 --> 00:08:27,000
And in the end, you see that it's quite worthless.

142
00:08:27,000 --> 00:08:31,000
It's something that once you cling to, it causes you suffering.

143
00:08:31,000 --> 00:08:37,000
If you're looking for it, once you start to create these desires

144
00:08:37,000 --> 00:08:40,000
and addictions, it causes you suffering.

145
00:08:40,000 --> 00:08:42,000
And this is why the Buddha said, not this entity,

146
00:08:42,000 --> 00:08:44,000
not this entity, but it's not happening.

147
00:08:44,000 --> 00:08:47,000
There's no happiness besides peace.

148
00:08:47,000 --> 00:08:53,000
But around means, if somebody will translate

149
00:08:53,000 --> 00:08:55,000
as there is no happiness without peace,

150
00:08:55,000 --> 00:08:56,000
but that's not the trends.

151
00:08:56,000 --> 00:08:57,000
That's not the meaning.

152
00:08:57,000 --> 00:09:00,000
But around means, besides peace, nothing can be called happy.

153
00:09:00,000 --> 00:09:04,000
Because there is nothing that could be called the happiness.

154
00:09:04,000 --> 00:09:05,000
You look at this.

155
00:09:05,000 --> 00:09:06,000
Could this be happiness?

156
00:09:06,000 --> 00:09:08,000
No, it's there and it's gone.

157
00:09:08,000 --> 00:09:11,000
What does that mean to you, to be called happiness?

158
00:09:11,000 --> 00:09:14,000
What you think of as happiness is not that moment

159
00:09:14,000 --> 00:09:16,000
because that's the mean of it.

160
00:09:16,000 --> 00:09:18,000
When you think of happiness, it's all watermelon,

161
00:09:18,000 --> 00:09:24,000
or state, or sex, or family, or friends,

162
00:09:24,000 --> 00:09:25,000
or something.

163
00:09:25,000 --> 00:09:27,000
You think of these as happiness.

164
00:09:27,000 --> 00:09:30,000
And the reason why it's not satisfying

165
00:09:30,000 --> 00:09:35,000
and why it leads us to addiction and dissatisfaction

166
00:09:35,000 --> 00:09:37,000
is because it's not real.

167
00:09:37,000 --> 00:09:41,000
The things that we're talking about are a momentary experience.

168
00:09:41,000 --> 00:09:44,000
It was something that was very visceral

169
00:09:44,000 --> 00:09:46,000
and I've never done what I wanted to share,

170
00:09:46,000 --> 00:09:48,000
and it was one of the things.

171
00:09:48,000 --> 00:09:50,000
It's certainly that the Buddha thing was something

172
00:09:50,000 --> 00:09:53,000
I wanted to share, so now you've all had a teaching

173
00:09:53,000 --> 00:09:58,000
for my hometown, and a pair of bracelets.

174
00:09:58,000 --> 00:10:01,000
Okay, so that's what I'm talking from us.

175
00:10:01,000 --> 00:10:04,000
On our practice, the past 10 days,

176
00:10:04,000 --> 00:10:07,000
so thank you all for sharing with me.

177
00:10:07,000 --> 00:10:27,000
Thank you very much.

178
00:10:37,000 --> 00:10:39,000
Thank you.

179
00:11:07,000 --> 00:11:09,000
Thank you.

180
00:11:37,000 --> 00:11:39,000
Thank you.

181
00:12:07,000 --> 00:12:11,000
Thank you.

182
00:12:37,000 --> 00:12:41,000
Thank you.

183
00:13:07,000 --> 00:13:09,000
You

184
00:13:37,000 --> 00:13:39,000
You

185
00:14:07,000 --> 00:14:09,000
You

186
00:14:37,000 --> 00:14:39,000
You

187
00:15:07,000 --> 00:15:09,000
You

188
00:15:37,000 --> 00:15:39,000
You

189
00:16:07,000 --> 00:16:09,000
You

190
00:16:37,000 --> 00:16:39,000
You

191
00:17:07,000 --> 00:17:09,000
You

192
00:17:37,000 --> 00:17:39,000
You

193
00:18:07,000 --> 00:18:09,000
You

194
00:18:37,000 --> 00:18:39,000
You

195
00:19:07,000 --> 00:19:09,000
You

196
00:19:37,000 --> 00:19:39,000
You

197
00:20:07,000 --> 00:20:09,000
You

198
00:20:37,000 --> 00:20:39,000
You

199
00:21:07,000 --> 00:21:09,000
You

200
00:21:37,000 --> 00:21:39,000
You

201
00:22:07,000 --> 00:22:09,000
You

202
00:22:37,000 --> 00:22:39,000
You

203
00:23:07,000 --> 00:23:09,000
You

204
00:23:37,000 --> 00:23:39,000
You

205
00:24:07,000 --> 00:24:09,000
You

206
00:24:37,000 --> 00:24:39,000
You

207
00:25:07,000 --> 00:25:09,000
You

208
00:25:37,000 --> 00:25:39,000
You

209
00:26:07,000 --> 00:26:09,000
You

210
00:26:37,000 --> 00:26:39,000
You

