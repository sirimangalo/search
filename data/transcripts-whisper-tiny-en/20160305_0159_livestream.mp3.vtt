WEBVTT

00:00.000 --> 00:18.240
Okay everyone.

00:18.240 --> 00:48.160
It's a short quote, you can read it off and forth, I don't know if the polling

00:48.160 --> 00:57.520
in front of him, but he who puts aside his wants in order to do the right thing, even

00:57.520 --> 00:58.520
though it'd be difficult.

00:58.520 --> 01:12.520
It was like a patient drinking medicine later, he will rejoice.

01:12.520 --> 01:24.840
So yeah, sometimes doing the right thing hurts, sometimes you want something, but you know

01:24.840 --> 01:34.280
the right thing is to not take it, and do something, you know that I'm not doing it's

01:34.280 --> 01:42.800
probably the better thing.

01:42.800 --> 01:57.120
This is a really important teaching, I think, because it's hard to go against your desires,

01:57.120 --> 02:13.160
we're not equipped to, our brains, our brains are designed to accommodate addiction,

02:13.160 --> 02:20.440
accommodate our desires, and the knowing balls gets weaker and weaker has become more

02:20.440 --> 02:27.760
addicted, so the know system, whatever that is, the system that says no is quite different

02:27.760 --> 02:36.840
from the system and says yes, and so you can only say no for some long minutes, it takes

02:36.840 --> 02:50.280
effort and it's taxing to say no, to try and prevent yourself from following your desires.

02:50.280 --> 02:57.080
That's not the important part of this first, the important part of the teaching is not

02:57.080 --> 03:06.680
to deny your desires, it's about the happiness that comes from, what's right, the knowing

03:06.680 --> 03:13.720
what's right, just like the happiness that comes from taking medicine, the only reason

03:13.720 --> 03:20.680
we take medicine is not because of how it tastes, because it makes us feel that it's because

03:20.680 --> 03:37.720
it cures us of our illness, and that knowledge that reassurance confidence in our mind

03:37.720 --> 03:48.520
that is strong enough, that is strong enough to overcome your desire, you want something

03:48.520 --> 03:59.400
and well that's a hard argument to fight against, I want it, I want that, that thing makes

03:59.400 --> 04:12.880
me happy, it's a hard argument to fight, you can't say no, no, bad for you, but it's

04:12.880 --> 04:17.960
a the only way, it's not, it's understandable if you don't find something better than

04:17.960 --> 04:45.720
something more powerful, this can be enough to then, to cause you to let go, let go of your

04:45.720 --> 04:54.840
desire, this is why giving is better than receiving, it really is, it really feels better

04:54.840 --> 05:03.840
to give, and to receive, and this is how one conquers miserliness within oneself,

05:03.840 --> 05:18.920
within stinginess, practice giving, because it'll feel good, it'll feel better than getting,

05:18.920 --> 05:24.720
if you do it enough, it's actually a way to overcome stinginess, give, practice giving, it

05:24.720 --> 05:33.480
actually works because it feels better, breaking preset, this is always nice, we like

05:33.480 --> 05:43.520
to, we like to kill and steal and line, cheat when we get away with it, the only way

05:43.520 --> 05:49.800
we do away with it, we do, we prevent ourselves from breaking preset, and from being immoral

05:49.800 --> 05:57.200
it's, it's by telling us how bad, ourselves are bad, it is bad being immoral, but again

05:57.200 --> 06:05.720
doesn't really, it's not uplifting, it's not a substitute, okay, if you're life just saying

06:05.720 --> 06:13.960
everything's bad, this is bad, that's bad, but when you do keep the preset, when you

06:13.960 --> 06:23.720
keep the rules, when you look at the does uplifting, when you feel the power of being a good

06:23.720 --> 06:39.320
person, it's this power that actually, the power of goodness, the power of being moral,

06:39.320 --> 06:45.280
of knowing that you're a ethical person, you're not stealing enough, that power is at this

06:45.280 --> 06:53.640
game, you sit and you think about how you've given freedom from fear to all beings,

06:53.640 --> 07:01.240
and by and, and by and, and the, the gift of fearlessness, no one needs to be afraid of

07:01.240 --> 07:16.240
you, this is what the precepts do, and it's a gift and it feels good.

07:16.240 --> 07:34.160
And if the same goes with some good green, with anger, even with delusion, with ignorance,

07:34.160 --> 07:40.960
they say ignorance is bliss, so it's nice to just live your life and not care, and not

07:40.960 --> 07:45.440
carry the world on your shoulders, or think about death, or think about getting sick or

07:45.440 --> 07:51.960
old age, or think about suffering, think about the world's problems, blissful to not think

07:51.960 --> 07:58.120
about these things, it's really a horrible philosophy, because your problems don't go away

07:58.120 --> 08:03.720
just because you're ignorant of them, it's really bliss, it's not sustainable, it's not rational,

08:03.720 --> 08:09.400
it's not reasonable, but the opposite seems even worse to have to live your life and worry

08:09.400 --> 08:15.240
about these things, what good does that do, what good does it do to concern yourself with

08:15.240 --> 08:25.240
the problems of life, the problems of the world, just makes you stressed and sick and upset,

08:25.240 --> 08:34.480
so ignorance is better, but the only thing better than ignorance is knowledge, and not the

08:34.480 --> 08:42.040
kind of knowledge, not knowledge of your problems, but understanding of your problems, maybe

08:42.040 --> 08:50.240
knowledge is a bad way, but understanding, so ignorance is bliss, but because knowledge

08:50.240 --> 09:00.680
is suffering, but understanding that's peace, once you understand your problems, they

09:00.680 --> 09:10.040
cease to be problems, and then thinking about them is not stressful, then they have no power

09:10.040 --> 09:19.040
of you, when they do come to pass, when they do rear their heads, they have no power

09:19.040 --> 09:31.080
of you, because you understand them, you know their true nature, you're not shocked

09:31.080 --> 09:45.040
or sad if I can, but surprised by them, and so these wisdom that comes through meditation

09:45.040 --> 09:57.880
to understand their experience, this is more powerful than ignorance, it's better than

09:57.880 --> 10:08.320
it, the similarly with the medicine, a person who steeps themselves in greed, anger and

10:08.320 --> 10:16.520
delusion follows their desires, even when it means not doing the right thing, it's like

10:16.520 --> 10:21.520
a person who doesn't take their medicine, a sick person who doesn't take the medicine

10:21.520 --> 10:28.680
and dies, because it's going to die, because you do the bad and do the wrong thing, it

10:28.680 --> 10:41.080
feels good, it's like why I take my medicine, it's just bitter and unpleasant, and then

10:41.080 --> 10:46.480
they die because of that, because they didn't take their medicine, they didn't do something

10:46.480 --> 10:53.920
that was unpleasant, sometimes you have to do it as unpleasant, you have to go against your

10:53.920 --> 10:59.440
desires, you have to go against your anger, be patient with other people, make you upset

10:59.440 --> 11:08.400
when things make you upset with the patient, bear with unpleasantness, sometimes you have

11:08.400 --> 11:18.480
to make yourself up, slap yourself in the face, face your problems, learn about your problems,

11:18.480 --> 11:23.360
understand your problems, rather than run away from them or ignore them or pretend they don't

11:23.360 --> 11:29.120
exist or drown yourself in drugs and alcohol, so they don't have to think about them,

11:29.120 --> 11:51.600
if you can actually understand them, cultivate wisdom about them, then you rejoice and

11:51.600 --> 12:07.160
truly, truly find truth, peace and happiness, then it's lasting substantial, meaningful,

12:07.160 --> 12:21.440
so interesting little quote, I'm quite busy this weekend, still not over, but I don't

12:21.440 --> 12:31.000
know if it's over the second, so tomorrow I'm canceling second time, I'll see you

12:31.000 --> 12:44.120
about more nine o'clock broadcasts, I don't know, I'm behind in my school, I've got a

12:44.120 --> 12:53.560
good catch stop, catch stop in the next week, thank you all for tuning in, good bye.

