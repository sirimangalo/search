1
00:00:00,000 --> 00:00:01,000
Go ahead.

2
00:00:01,000 --> 00:00:08,720
I have not given up normal life just yet, but I fluctuate between the peace of practice

3
00:00:08,720 --> 00:00:11,400
and dealing with real life.

4
00:00:11,400 --> 00:00:17,240
How can I deal with the fluctuations of normal life?

5
00:00:17,240 --> 00:00:18,800
Don't give it up.

6
00:00:18,800 --> 00:00:22,200
I don't deal with it.

7
00:00:22,200 --> 00:00:23,440
I don't know.

8
00:00:23,440 --> 00:00:24,880
I ran away from it.

9
00:00:24,880 --> 00:00:25,880
I gave it up.

10
00:00:25,880 --> 00:00:28,680
I said it's not for me.

11
00:00:28,680 --> 00:00:31,800
I don't have advice.

12
00:00:31,800 --> 00:00:39,840
Well, I suppose I do, but no, it's very pertinent, no, it's what we're just talking about.

13
00:00:39,840 --> 00:00:46,280
I think an answer I can give along the lines that I just did that's a little more explanatory

14
00:00:46,280 --> 00:00:51,440
is that that's what you have to deal with.

15
00:00:51,440 --> 00:00:53,440
What you're describing is reality.

16
00:00:53,440 --> 00:01:00,440
You are standing with two feet, I don't know what the saying is, you're on sitting on

17
00:01:00,440 --> 00:01:01,440
the fence.

18
00:01:01,440 --> 00:01:02,440
I don't know.

19
00:01:02,440 --> 00:01:08,800
You're both your feet are in different rooms, and so until you decide one or the other,

20
00:01:08,800 --> 00:01:13,960
they're until you are able to give up normal life, you're not going to find a perfect

21
00:01:13,960 --> 00:01:19,440
peace of practice, though you're not going to be able to find harmony.

22
00:01:19,440 --> 00:01:28,600
When you say give up normal life, so you're probably, I don't know, but you're probably

23
00:01:28,600 --> 00:01:35,040
not describing what the normal everyday person would think of in terms of normal life.

24
00:01:35,040 --> 00:01:42,160
You're thinking in terms of normal life, you're thinking about life with less desires,

25
00:01:42,160 --> 00:01:50,720
material desires, less stress in your life, less anxiety, lack of future, less guilt.

26
00:01:50,720 --> 00:01:57,720
No, I mean, I'm referring to his idea of normal life in terms of the other side, the

27
00:01:57,720 --> 00:02:04,560
stress part, because he's saying fluctuating between peace of practice and real life, sorry,

28
00:02:04,560 --> 00:02:09,960
but he says he haven't given up normal life, so it means that he hasn't given up whatever

29
00:02:09,960 --> 00:02:15,120
that is the normal life in terms of a normal lay person.

30
00:02:15,120 --> 00:02:22,040
I'm thinking of that as the other side of the coin, the bad side, normal life being a normal,

31
00:02:22,040 --> 00:02:26,360
non-monastic, non-buddhist, crazy life.

32
00:02:26,360 --> 00:02:34,760
But what I was pointing out is a life that most of us is a culmination of all those

33
00:02:34,760 --> 00:02:46,600
various parts of our lives, anxiety, the frustration, anger, desire, you don't get rid

34
00:02:46,600 --> 00:02:52,960
of that by becoming a monk or any such thing.

35
00:02:52,960 --> 00:03:00,320
But if you don't put yourself in a position to deal with those things directly on a sustained

36
00:03:00,320 --> 00:03:09,240
and long-term basis, then yeah, there's no end, it shouldn't be a question.

37
00:03:09,240 --> 00:03:13,200
You can just bemoan the fact that you have to do it.

38
00:03:13,200 --> 00:03:19,760
It's an observation or what your observation is quite astute, that that's the way it is.

39
00:03:19,760 --> 00:03:24,000
And many people come to this observation, but the problem is they tend to come to it as a question

40
00:03:24,000 --> 00:03:30,120
asking me, how do I make it perfect so I can have my cake and eat it too, which is basically

41
00:03:30,120 --> 00:03:37,880
I think unfortunately what it is that you're describing, it seems to me.

42
00:03:37,880 --> 00:03:45,600
And so rather than trying to find a static way to deal with it, slowly move yourself towards

43
00:03:45,600 --> 00:03:47,480
what do you think is the better option?

44
00:03:47,480 --> 00:03:52,840
I mean, obviously you accept the fact that meditation brings peace because you say it.

45
00:03:52,840 --> 00:03:56,240
I don't suppose you think that normal life brings a similar peace.

46
00:03:56,240 --> 00:04:01,480
So you know, except the fact that one is better than the other and start moving towards

47
00:04:01,480 --> 00:04:06,000
the one that is better, as best you can't.

48
00:04:06,000 --> 00:04:08,800
So that calls for more meditation?

49
00:04:08,800 --> 00:04:16,880
Well no, no actually, it calls for, well yes, but it also calls for making steps to

50
00:04:16,880 --> 00:04:23,720
provide yourself with a situation where you can undertake more meditation, changing things

51
00:04:23,720 --> 00:04:26,640
about your life.

52
00:04:26,640 --> 00:04:31,600
But changing one of the best ways to do that is yes, I agree to do more meditation,

53
00:04:31,600 --> 00:04:37,000
mini meds as you say, and being mindful more in daily life, that kind of thing will change

54
00:04:37,000 --> 00:04:40,480
your life.

55
00:04:40,480 --> 00:04:46,440
Now what I was going to say is if you can't do that, if you're at your limit, you maxed

56
00:04:46,440 --> 00:04:52,960
out your democratic card and you really have to, you know, you have children for example

57
00:04:52,960 --> 00:05:02,080
or you have debts or obligations of other sorts to your parents, for example, then you

58
00:05:02,080 --> 00:05:10,880
just have to accept and learn, learn an important lesson is that actually meditation is

59
00:05:10,880 --> 00:05:20,040
not necessarily going, not necessarily need to be peaceful and Arahad can have a lot of difficult

60
00:05:20,040 --> 00:05:25,440
situations and can appear to be faced with a lot of suffering.

61
00:05:25,440 --> 00:05:29,800
The difference is they don't suffer based on it, Arahad, enlightened person's life may

62
00:05:29,800 --> 00:05:37,400
not be and I would say oftentimes is not, roses and clover is not just fun and game or

63
00:05:37,400 --> 00:05:46,040
not just peace and happiness, well it's not calm waters and Arahad will be surrounded

64
00:05:46,040 --> 00:05:55,320
by their karmic buddies who are coming to square up their karmic differences, so a lot

65
00:05:55,320 --> 00:06:01,280
of crazy things can happen, bad people can surround Arahads because of their bad, bad karma,

66
00:06:01,280 --> 00:06:11,520
crazy things and so we should be willing to follow that example and accept the fact that

67
00:06:11,520 --> 00:06:20,400
our lives, even as monks, very much, so even as monks may not be peaceful, calm, tranquil,

68
00:06:20,400 --> 00:06:49,960
we may have to deal with a lot of crazy stuff, you do, do have a lot of questions.

