1
00:00:00,000 --> 00:00:03,000
.

2
00:00:03,000 --> 00:00:08,000
..

3
00:00:08,000 --> 00:00:11,000
..

4
00:00:11,000 --> 00:00:14,000
..

5
00:00:14,000 --> 00:00:17,000
..

6
00:00:17,000 --> 00:00:19,000
...

7
00:00:19,000 --> 00:00:22,000
...

8
00:00:22,000 --> 00:00:25,000
...

9
00:00:25,000 --> 00:00:28,000
..

10
00:00:28,000 --> 00:00:29,000
..

11
00:00:29,000 --> 00:00:32,000
.

12
00:00:32,000 --> 00:00:35,000
.

13
00:00:35,000 --> 00:00:40,000
.

14
00:00:40,000 --> 00:00:44,000
.

15
00:00:44,000 --> 00:00:47,000
..

16
00:00:47,000 --> 00:00:50,000
..

17
00:00:50,000 --> 00:00:55,000
..

18
00:00:55,000 --> 00:00:58,000
..

19
00:00:58,000 --> 00:01:24,000
I am back just testing it out, see how it goes, and I'll see it.

20
00:01:24,000 --> 00:01:32,000
I'll see how it goes, see how it goes, see how it goes.

21
00:01:32,000 --> 00:01:40,000
I am back just testing it out, see how it goes.

22
00:01:40,000 --> 00:01:47,000
See how it goes, see how it goes.

23
00:01:47,000 --> 00:02:09,000
Hm.

24
00:02:09,000 --> 00:02:11,000
How is the video?

25
00:02:11,000 --> 00:02:25,000
I can't see a video, I don't know why it's not showing for me.

26
00:02:25,000 --> 00:02:34,000
Yeah, it's much quicker, much higher bandwidth.

27
00:02:34,000 --> 00:02:40,000
How is the video?

28
00:02:40,000 --> 00:02:45,000
Better, I guess.

29
00:02:45,000 --> 00:02:46,000
Good.

30
00:02:46,000 --> 00:02:50,000
Okay, I figured it out, I think.

31
00:02:50,000 --> 00:02:57,000
We'll see, I mean, it may start to buffer again, but

32
00:02:57,000 --> 00:03:06,000
just look at what YouTube advises and what FMPing advises.

33
00:03:06,000 --> 00:03:09,000
Okay, all right, we'll stick with this then.

34
00:03:09,000 --> 00:03:11,000
Oh no, no.

35
00:03:11,000 --> 00:03:14,000
YouTube is not receiving enough video to maintain

36
00:03:14,000 --> 00:03:28,000
this intense, smooth streaming, back to the drawing board.

37
00:03:28,000 --> 00:03:37,000
Why would that be?

38
00:03:37,000 --> 00:04:00,000
Okay, I'll see how it goes.

39
00:04:00,000 --> 00:04:22,000
Oh, streaming selfie again.

40
00:04:22,000 --> 00:04:39,000
I still can't see it.

41
00:04:39,000 --> 00:04:49,000
Oh yeah, quality is much better, huh?

42
00:04:49,000 --> 00:05:09,000
You're welcome, it's good to hear someone actually appreciate

43
00:05:09,000 --> 00:05:10,000
it that video.

44
00:05:10,000 --> 00:05:14,000
That's one of the more contentious I've received a lot of attention

45
00:05:14,000 --> 00:05:39,000
lately from people who wouldn't normally give me that kind of attention, I guess.

46
00:05:39,000 --> 00:06:00,000
So it says the stream health is good for now, where's hoping it stays that way?

47
00:06:00,000 --> 00:06:05,000
Okay.

48
00:06:05,000 --> 00:06:10,000
I'm glad you've appreciated it, it's good to hear, good to have feedback.

49
00:06:10,000 --> 00:06:19,000
I'm always interested to know how people affected by such issues,

50
00:06:19,000 --> 00:06:38,000
what do you think of the things that I say?

51
00:06:38,000 --> 00:06:41,000
Okay, well the stream health is still excellent.

52
00:06:41,000 --> 00:07:00,000
Assuming that may not last, but I didn't tweak something, so I don't know if that helps.

53
00:07:00,000 --> 00:07:17,000
It might not have been an internet thing, wait, I've just fixed it.

54
00:07:17,000 --> 00:07:24,000
Oh, there's another one, ultra fast.

55
00:07:24,000 --> 00:07:35,000
Right, that's what it says, okay.

56
00:07:35,000 --> 00:07:38,000
Maybe we're good.

57
00:07:38,000 --> 00:07:53,000
I switched to super fast, and there's still an even faster one, which is ultra fast, if this one doesn't work.

58
00:07:53,000 --> 00:08:00,000
The subject of the talk today is how to live stream on YouTube.

59
00:08:00,000 --> 00:08:02,000
This is just a test.

60
00:08:02,000 --> 00:08:09,000
But there's a video that I just did, and it shouldn't be in the video in my video section.

61
00:08:09,000 --> 00:08:12,000
Let me see.

62
00:08:12,000 --> 00:08:19,000
Yes, if you look, I'll delete this short one.

63
00:08:19,000 --> 00:08:24,000
So if you look, there is one called live stream in my videos list.

64
00:08:24,000 --> 00:08:31,000
And the topic of that video was preparation for meditation course,

65
00:08:31,000 --> 00:08:43,000
along with a lot of technical difficulties.

66
00:08:43,000 --> 00:08:50,000
Alright, if you got questions, I'm here to answer.

67
00:08:50,000 --> 00:08:57,000
What are your questions, go for it?

68
00:08:57,000 --> 00:09:00,000
Special test question session.

69
00:09:00,000 --> 00:09:02,000
Do we really die?

70
00:09:02,000 --> 00:09:12,000
We die every moment, we're born and die every moment.

71
00:09:12,000 --> 00:09:14,000
Do you have your favorite chant?

72
00:09:14,000 --> 00:09:15,000
If so, why?

73
00:09:15,000 --> 00:09:18,000
Mine is ATP, so.

74
00:09:18,000 --> 00:09:22,000
I mean, favorites imply attachment, right?

75
00:09:22,000 --> 00:09:27,000
So we have to be careful about that.

76
00:09:27,000 --> 00:09:31,000
But I don't really have a favorite chant.

77
00:09:31,000 --> 00:09:34,000
But so, oh, there's one, there's one chant.

78
00:09:34,000 --> 00:09:36,000
I've filled this story before.

79
00:09:36,000 --> 00:09:42,000
And I was up on Doyseetep, and long story short, these monks,

80
00:09:42,000 --> 00:09:45,000
they were going around the pagoda.

81
00:09:45,000 --> 00:09:48,000
And I heard them doing this chanting, and immediately it struck me.

82
00:09:48,000 --> 00:09:52,000
It was just like something resonated with me with that chant.

83
00:09:52,000 --> 00:09:54,000
I think I was probably Burmese.

84
00:09:54,000 --> 00:09:55,000
These were Burmese monks.

85
00:09:55,000 --> 00:09:58,000
I was probably Burmese in the past.

86
00:09:58,000 --> 00:10:05,000
And I had to find out what that chant was.

87
00:10:05,000 --> 00:10:12,000
I got stuck in my mind, and I kept it in my mind for quite a while.

88
00:10:12,000 --> 00:10:16,000
And then when I was in Bangkok one time, I was sitting beside Burmese monk,

89
00:10:16,000 --> 00:10:17,000
and I asked them about it.

90
00:10:17,000 --> 00:10:19,000
I kind of mumbled it to him.

91
00:10:19,000 --> 00:10:21,000
And he knew right away what it was.

92
00:10:21,000 --> 00:10:27,000
It was the Mahapatana.

93
00:10:27,000 --> 00:10:34,000
Mahapatana, I would say, is my favorite chant as far as that goes.

94
00:10:34,000 --> 00:10:41,000
Let's see if I can get you a...

95
00:10:41,000 --> 00:10:44,000
Here, I'll show you a link.

96
00:10:44,000 --> 00:10:48,000
Maybe I can send a link.

97
00:10:48,000 --> 00:10:50,000
Does that work?

98
00:10:50,000 --> 00:10:55,000
There's a link to the chant.

99
00:10:55,000 --> 00:10:57,000
I think it's the best version I have.

100
00:10:57,000 --> 00:11:03,000
It's not very good quality, but audio, but that's the chant.

101
00:11:03,000 --> 00:11:08,000
How frequently do you stream live on YouTube?

102
00:11:08,000 --> 00:11:14,000
Well, this is the first time in a while, but should be doing it most days.

103
00:11:14,000 --> 00:11:19,000
I might reduce the frequency in the future, maybe three times a week,

104
00:11:19,000 --> 00:11:23,000
something like that, we'll see.

105
00:11:23,000 --> 00:11:29,000
What are your thoughts on negative and positive energy meditating with cats?

106
00:11:29,000 --> 00:11:33,000
There's not... I don't believe in such things.

107
00:11:33,000 --> 00:11:37,000
I mean, negative and positive is how we react to them.

108
00:11:37,000 --> 00:11:43,000
How we react to experiences.

109
00:11:43,000 --> 00:11:48,000
So, I'm much more about our reactions to things.

110
00:11:48,000 --> 00:11:52,000
Something becomes negative because we have a negative conception of it.

111
00:11:52,000 --> 00:11:58,000
Something is positive because we have a positive reaction to it.

112
00:11:58,000 --> 00:12:02,000
So, we try to free ourselves from that, and then energy is just energy.

113
00:12:02,000 --> 00:12:07,000
It's not positive or negative.

114
00:12:07,000 --> 00:12:08,000
What can help with insomnia?

115
00:12:08,000 --> 00:12:11,000
Are you getting very hungry in the evening when it's time to sleep?

116
00:12:11,000 --> 00:12:14,000
It was a very fairly different note.

117
00:12:14,000 --> 00:12:19,000
Well, if you haven't, I'd suggest you read my booklet on how to meditate.

118
00:12:19,000 --> 00:12:25,000
That should help with things I can insomnia or hunger and that kind of thing.

119
00:12:25,000 --> 00:12:31,000
You want to link to that, for those of you who don't know?

120
00:12:31,000 --> 00:12:36,000
Let's say you'll link to my booklet on how to meditate.

121
00:12:36,000 --> 00:12:43,000
There's another link.

122
00:12:43,000 --> 00:12:45,000
What's your opinion on monks?

123
00:12:45,000 --> 00:12:48,000
On monk with sake and I have no idea.

124
00:12:48,000 --> 00:12:49,000
Sakean, are you Thai?

125
00:12:49,000 --> 00:12:51,000
You must be a Thai person because you're using Thai word.

126
00:12:51,000 --> 00:12:53,000
Sakean is Thai too, right?

127
00:12:53,000 --> 00:12:58,000
Special religious tattoos.

128
00:12:58,000 --> 00:13:01,000
Superstition, really.

129
00:13:01,000 --> 00:13:05,000
Or, I mean, you could argue that there's some, it affects you psychologically.

130
00:13:05,000 --> 00:13:08,000
So, there's some good to it.

131
00:13:08,000 --> 00:13:13,000
I don't know much opinion on it.

132
00:13:13,000 --> 00:13:18,000
I was doing qigong and abilities when meditating in my cat freaked out and ran.

133
00:13:18,000 --> 00:13:23,000
Well, your sounds like your cat is reactionary.

134
00:13:23,000 --> 00:13:31,000
Your thoughts on dogma, dogmatic beliefs, etc.

135
00:13:31,000 --> 00:13:34,000
Yeah, I mean, I wouldn't hold on to anything dogmatically.

136
00:13:34,000 --> 00:13:38,000
Beliefs should come from experience.

137
00:13:38,000 --> 00:13:44,000
Actually come from experience, but it can also come from an experience that gives you confidence in authority.

138
00:13:44,000 --> 00:13:49,000
So, I have confidence in many things that Buddha said, even though I've never experienced them,

139
00:13:49,000 --> 00:13:55,000
because the things I have practiced that Buddha has given me confidence in him, that kind of thing.

140
00:13:55,000 --> 00:14:08,000
So, sometimes it's not dogmatic, it's based on a educated or a informed confidence in authority.

141
00:14:08,000 --> 00:14:16,000
Well, Asian, but you're using Thai word sakyan, sakyan.

142
00:14:16,000 --> 00:14:18,000
Why do I care so much?

143
00:14:18,000 --> 00:14:20,000
I want to stop, it hurts me.

144
00:14:20,000 --> 00:14:22,000
Well, it's habits.

145
00:14:22,000 --> 00:14:25,000
We build up habits by of caring.

146
00:14:25,000 --> 00:14:29,000
But the great thing is that there's no mean being hurt.

147
00:14:29,000 --> 00:14:31,000
Hurt is just a state of mind.

148
00:14:31,000 --> 00:14:35,000
And if you come to see that, you free yourself.

149
00:14:35,000 --> 00:14:38,000
You no longer have this for yourself.

150
00:14:38,000 --> 00:14:39,000
It's a funny thing to say.

151
00:14:39,000 --> 00:14:44,000
There's freedom for yourself by not having self.

152
00:14:44,000 --> 00:14:49,000
But not clinging to the experience is me, I, me.

153
00:14:49,000 --> 00:14:51,000
But read my booklet.

154
00:14:51,000 --> 00:14:55,000
If you practice the meditation that's in that booklet I guarantee it will help.

155
00:14:55,000 --> 00:15:00,000
Be less hurting, less suffering.

156
00:15:00,000 --> 00:15:02,000
Have you seen a ghost?

157
00:15:02,000 --> 00:15:04,000
No, I've never seen a ghost.

158
00:15:04,000 --> 00:15:07,000
I mean, I don't know.

159
00:15:07,000 --> 00:15:11,000
I've never seen anything that I could identify as a ghost.

160
00:15:11,000 --> 00:15:14,000
What's the best way to become motivated?

161
00:15:14,000 --> 00:15:17,000
I mean, read the Buddha's teaching.

162
00:15:17,000 --> 00:15:24,000
Be surrounded by people who are energetic who are motivated.

163
00:15:24,000 --> 00:15:28,000
There's different ways.

164
00:15:28,000 --> 00:15:34,000
There's meditating with eyes closed different than with eyes open.

165
00:15:34,000 --> 00:15:37,000
Can the two methods cause different states of mindfulness?

166
00:15:37,000 --> 00:15:38,000
I don't know.

167
00:15:38,000 --> 00:15:39,000
It's not that important.

168
00:15:39,000 --> 00:15:41,000
It's easier with your eyes closed.

169
00:15:41,000 --> 00:15:43,000
I'm easy to get me focused.

170
00:15:43,000 --> 00:15:46,000
Better for beginners anyway.

171
00:15:46,000 --> 00:15:47,000
Better in general.

172
00:15:47,000 --> 00:15:49,000
I mean, you're just very more focused.

173
00:15:49,000 --> 00:15:53,000
One less thing to concern yourself with.

174
00:15:53,000 --> 00:15:56,000
How come one begin to see their own false judgments

175
00:15:56,000 --> 00:15:58,000
when the hinder ability even to know their existence?

176
00:15:58,000 --> 00:16:00,000
Well, having a teacher helps.

177
00:16:00,000 --> 00:16:03,000
Having a technique that you force yourself to do helps

178
00:16:03,000 --> 00:16:08,000
because it forces you to, forces you out of your comfort zone.

179
00:16:08,000 --> 00:16:13,000
But we have this unique ability or this special or this remarkable ability

180
00:16:13,000 --> 00:16:15,000
to change.

181
00:16:15,000 --> 00:16:19,000
We aren't automatons that are stuck in loops.

182
00:16:19,000 --> 00:16:24,000
We have the ability to reflect and to change.

183
00:16:24,000 --> 00:16:27,000
Some of us more than others, of course.

184
00:16:27,000 --> 00:16:33,000
Some people don't have a very strong ability.

185
00:16:33,000 --> 00:16:36,000
You communicate with beings of other realms?

186
00:16:36,000 --> 00:16:37,000
No.

187
00:16:37,000 --> 00:16:40,000
How to go beyond mind?

188
00:16:40,000 --> 00:16:42,000
Well, let's go of it.

189
00:16:42,000 --> 00:16:46,000
Stop clinging to mind.

190
00:16:46,000 --> 00:16:49,000
Audio is a little ahead of the video by about half a second.

191
00:16:49,000 --> 00:16:52,000
Well, that's interesting.

192
00:16:52,000 --> 00:16:57,000
I don't know why that would be.

193
00:16:57,000 --> 00:17:04,000
It's a shame, really.

194
00:17:04,000 --> 00:17:06,000
Are we living in some kind of hologram?

195
00:17:06,000 --> 00:17:11,000
Not that I know of.

196
00:17:11,000 --> 00:17:12,000
Okay.

197
00:17:12,000 --> 00:17:13,000
Enough questions.

198
00:17:13,000 --> 00:17:15,000
That's all for tonight.

199
00:17:15,000 --> 00:17:16,000
Thank you all for tuning in.

200
00:17:16,000 --> 00:17:18,000
Thanks for being a part of our little test.

201
00:17:18,000 --> 00:17:21,000
Hopefully it continues to work.

202
00:17:21,000 --> 00:17:24,000
It can be tweaked and improved in the future.

