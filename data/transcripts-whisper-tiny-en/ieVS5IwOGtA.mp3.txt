What exactly is noting in meditation?
Well, if you're asking because you've heard the word and don't know what it means,
I'd recommend you to read my booklet on how to meditate.
That's the first thing.
But if you're meaning more philosophically or more technically,
what is going on there?
It is the fixing of the mind and the straightening of the mind in regards to the object,
reminding yourself of the essence of the experience,
the reality of the experience, as opposed to seeing it as an entity
or something, a relation in terms of liking or disliking it or identifying with it,
just to experience it as it is to remind yourself of what it is.
Because that's the word set the mindfulness, which translates mindfulness.
But the word set the means remembrance.
The reminding, remindedness, the recollectedness, the ability to recognize something for what it is.
And so that's what the noting does. The noting is the creation in the mind of a recognition,
which is why the word is not, which word you choose is not important,
but the word itself is an act of recognizing the object as close as possible to what it actually is.
