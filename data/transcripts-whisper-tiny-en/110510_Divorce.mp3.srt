1
00:00:00,000 --> 00:00:03,000
Okay, welcome back to Ask a Monk.

2
00:00:03,000 --> 00:00:07,000
Next question is about divorce.

3
00:00:07,000 --> 00:00:17,000
I was asked the question about how to deal with a divorce when you are a third party,

4
00:00:17,000 --> 00:00:25,000
or in this case, your parents are divorced or divorcing in the process of it.

5
00:00:25,000 --> 00:00:29,000
How do you deal with it?

6
00:00:29,000 --> 00:00:35,000
And I guess, as always, we should put the question in context.

7
00:00:35,000 --> 00:00:38,000
And because here we are talking about several different things.

8
00:00:38,000 --> 00:00:43,000
We are talking about the idea of family and one's parents.

9
00:00:43,000 --> 00:00:48,000
We are talking about the concept of marriage.

10
00:00:48,000 --> 00:00:51,000
And we are talking about suffering.

11
00:00:51,000 --> 00:00:58,000
So these are all three of the issues that come up in relation to divorce.

12
00:00:58,000 --> 00:01:09,000
The concept of a family has to do with all of those concepts that most of us seem,

13
00:01:09,000 --> 00:01:17,000
most of us find to seem or that seem to most of us to be ultimately real.

14
00:01:17,000 --> 00:01:19,000
For instance, being human.

15
00:01:19,000 --> 00:01:24,000
We think that humanity is something real.

16
00:01:24,000 --> 00:01:30,000
And we think of our gender as being something real.

17
00:01:30,000 --> 00:01:42,000
And as a result, these concepts form our opinion, our ideas about reality,

18
00:01:42,000 --> 00:01:48,000
our ideas of family, our idea, the concept of having parents.

19
00:01:48,000 --> 00:01:52,000
And I see these are concepts, and I think they should be broken apart.

20
00:01:52,000 --> 00:01:57,000
They should be broken down and understood to be simply concept.

21
00:01:57,000 --> 00:02:02,000
Reality doesn't admit of any of these concepts.

22
00:02:02,000 --> 00:02:09,000
But in an ultimate sense, there is no human, there is no gender, there is no family,

23
00:02:09,000 --> 00:02:12,000
there is no mother and father and so on.

24
00:02:12,000 --> 00:02:15,000
And I'm going to get in trouble for that.

25
00:02:15,000 --> 00:02:20,000
Because on a conventional level, people understand that there is mother and father.

26
00:02:20,000 --> 00:02:23,000
If I say there is no mother and father, then people are going to say,

27
00:02:23,000 --> 00:02:30,000
what that means we shouldn't respect our parents, or we shouldn't be thankful for the things they've done.

28
00:02:30,000 --> 00:02:33,000
And obviously that's not the case.

29
00:02:33,000 --> 00:02:37,000
But on an ultimate, ultimate reality doesn't admit of these things.

30
00:02:37,000 --> 00:02:42,000
And when we can break away from these concepts,

31
00:02:42,000 --> 00:02:48,000
then we can break away from a lot of the suffering that comes from them.

32
00:02:48,000 --> 00:02:53,000
So, for instance, in this case, our attachment to our parents,

33
00:02:53,000 --> 00:02:57,000
we understand them to be a specific entity.

34
00:02:57,000 --> 00:03:02,000
Our father is thus, our mother is thus, our family is this unit.

35
00:03:02,000 --> 00:03:04,000
There is an entity that we call our family.

36
00:03:04,000 --> 00:03:10,000
There is a relationship that we call family, that we call blood relationship and so on.

37
00:03:10,000 --> 00:03:17,000
But simply looking at, if we examine this from a scientific point of view,

38
00:03:17,000 --> 00:03:21,000
it's easy to see the falsehood of them.

39
00:03:21,000 --> 00:03:27,000
I mean, as human beings, our understanding is that by blood we're related to each and every one,

40
00:03:27,000 --> 00:03:34,000
each and every human on the planet, by blood ties, so we're all family.

41
00:03:34,000 --> 00:03:40,000
Obviously this is a beneficial realization as well,

42
00:03:40,000 --> 00:03:50,000
because it breaks down all of the cultural barriers, all of the racism and prejudice and so on.

43
00:03:50,000 --> 00:03:56,000
The us and them, mentalities that pervade the world.

44
00:03:56,000 --> 00:03:59,000
But whether it's useful or not, it's the truth.

45
00:03:59,000 --> 00:04:06,000
The reality is that there is no barrier or boundary where we can see this is family and this is not.

46
00:04:06,000 --> 00:04:09,000
It's just a matter of degree.

47
00:04:09,000 --> 00:04:15,000
And that degree is important in one sense, in terms of our understanding of each other,

48
00:04:15,000 --> 00:04:21,000
our ability to relate to each other, our knowledge of each other's past and so on.

49
00:04:21,000 --> 00:04:24,000
And this is the relationship we have with their parents.

50
00:04:24,000 --> 00:04:40,000
There is a, there are many emotions and many memories and many ultimate realities that are tied up with this concept of family.

51
00:04:40,000 --> 00:04:48,000
Once we have parents, then we have many, many attachments that go along with that.

52
00:04:48,000 --> 00:04:54,000
We have many comforts and supports.

53
00:04:54,000 --> 00:04:59,000
There's many beneficial things that come obviously from having a stable family unit.

54
00:04:59,000 --> 00:05:12,000
But something that will help us to deal with divorce is to understand that there is really no change occurring,

55
00:05:12,000 --> 00:05:19,000
apart from the actual suffering that comes from a divorce, where you lose the stability,

56
00:05:19,000 --> 00:05:30,000
where you lose the cohesion, where you lose the support of having two parents and a family unit.

57
00:05:30,000 --> 00:05:32,000
There's nothing wrong.

58
00:05:32,000 --> 00:05:36,000
There's nothing wrong.

59
00:05:36,000 --> 00:05:43,000
There's no problem, except in your own mind, where you've had your life disrupted.

60
00:05:43,000 --> 00:05:46,000
You've had your way of looking at reality change.

61
00:05:46,000 --> 00:05:53,000
And in fact, it's in a way that seems like something that was very real and whole has broken into,

62
00:05:53,000 --> 00:05:55,000
has broken into pieces.

63
00:05:55,000 --> 00:06:02,000
When in fact that's not the case, because the idea of a family unit is in our mind, it's a concept.

64
00:06:02,000 --> 00:06:12,000
If our parents split up, it just means that this entity, this mass of physical and mental realities has gone here,

65
00:06:12,000 --> 00:06:18,000
and this one has gone here, and they're relating to each other in a different way.

66
00:06:18,000 --> 00:06:26,000
The problem comes with our attachment, and so this gets into understanding of suffering.

67
00:06:26,000 --> 00:06:36,000
The other concept I'd like to talk about is the one of marriage, because it relates to the first one.

68
00:06:36,000 --> 00:06:39,000
It relates to the idea of having parents.

69
00:06:39,000 --> 00:06:45,000
So our reliance on our parents is one attachment that we have.

70
00:06:45,000 --> 00:06:51,000
It's something that we've misled ourselves from our very birth.

71
00:06:51,000 --> 00:06:57,000
We've been misleading ourselves into thinking that there was something stable about the family unit.

72
00:06:57,000 --> 00:07:04,000
And throughout our lives, that stability will be eroded or shattered or taken away from us.

73
00:07:04,000 --> 00:07:08,000
And it will generally lead to a great amount of suffering.

74
00:07:08,000 --> 00:07:14,000
And as a result, learning, understanding that what we thought was stable is not in fact stable.

75
00:07:14,000 --> 00:07:24,000
And it will allow us to open our minds up to the idea that life is transient, that we shouldn't cling to things.

76
00:07:24,000 --> 00:07:26,000
And as a result, we'll be able to accept change better.

77
00:07:26,000 --> 00:07:28,000
Most people come to this.

78
00:07:28,000 --> 00:07:36,000
So they come to a realization and they come to wisdom through the suffering and realizing that they were setting themselves up for this,

79
00:07:36,000 --> 00:07:41,000
through their ignorance, through their misunderstanding that there was stability.

80
00:07:41,000 --> 00:07:52,000
Marriage is another concept, and it's also tied up with a lot of clinging, obviously.

81
00:07:52,000 --> 00:08:05,000
So the idea of a divorce is one that we think of as breaking something up.

82
00:08:05,000 --> 00:08:11,000
That there is something that has been shattered with the marriage.

83
00:08:11,000 --> 00:08:15,000
And we feel bad about that even if we're not involved in it.

84
00:08:15,000 --> 00:08:20,000
We also feel like there is something incomplete.

85
00:08:20,000 --> 00:08:25,000
There's some knot that has been untied and you have to tie it back together and so on.

86
00:08:25,000 --> 00:08:28,000
But marriage is simply a contract.

87
00:08:28,000 --> 00:08:31,000
It's a conventional agreement between two people.

88
00:08:31,000 --> 00:08:35,000
And that agreement has to do with morality.

89
00:08:35,000 --> 00:08:37,000
It has to do with not cheating on each other.

90
00:08:37,000 --> 00:08:40,000
It has to do with supporting each other and so on.

91
00:08:40,000 --> 00:08:44,000
So there isn't that mutual support anymore.

92
00:08:44,000 --> 00:08:54,000
These people have come to an agreement that they're not going to act in those same ways anymore.

93
00:08:54,000 --> 00:09:01,000
It's really divorce is nothing. It's just a change of agreement, a change of our relationships.

94
00:09:01,000 --> 00:09:06,000
It's like leaving one job and going to another job.

95
00:09:06,000 --> 00:09:08,000
It's simply a change.

96
00:09:08,000 --> 00:09:15,000
So again, a lot of the suffering that comes from divorce just comes from our attachment to concepts,

97
00:09:15,000 --> 00:09:17,000
the idea of the solid family.

98
00:09:17,000 --> 00:09:27,000
And the idea of stability, which is a false one, in the end we have to accept the fact that reality is impermanent.

99
00:09:27,000 --> 00:09:29,000
Everything is changing.

100
00:09:29,000 --> 00:09:36,000
And if we, whatever we cling to, it's only going to lead us to suffering because it's not stable.

101
00:09:36,000 --> 00:09:39,000
So this is one side of things.

102
00:09:39,000 --> 00:09:45,000
And if this is causing you suffering, then you should immediately wipe that out of your mind and move on.

103
00:09:45,000 --> 00:09:52,000
The real suffering that comes from divorce, and this is one that is much more difficult to overcome,

104
00:09:52,000 --> 00:09:56,000
is the negative emotions that are caused by divorce.

105
00:09:56,000 --> 00:10:09,000
The anger, when the greed, when there's jealousy involved with a divorce.

106
00:10:09,000 --> 00:10:15,000
So anger comes when you are blaming each other for the divorce and so on.

107
00:10:15,000 --> 00:10:22,000
There's guilt involved with it, the guilt in relation to one's children and so on.

108
00:10:22,000 --> 00:10:31,000
There is a lot of stress involved with the work that's required to split up.

109
00:10:31,000 --> 00:10:40,000
And there's a lot of greed involved in terms of splitting up the belongings of the family, who gets what and how much.

110
00:10:40,000 --> 00:10:45,000
And then lawyers get involved and there's more stress.

111
00:10:45,000 --> 00:10:59,000
There is jealousy in terms of competing for the love of the children, trying to not be seen as the bad guy.

112
00:10:59,000 --> 00:11:09,000
And so you'll have both parents vying for the love of their children saying how great they are and how trying to show off.

113
00:11:09,000 --> 00:11:19,000
And because they're afraid that the other parent is doing the same thing and it's a competition because they don't want to look like the bad guy.

114
00:11:19,000 --> 00:11:26,000
And they care what their children think and they're afraid their children are going to judge them.

115
00:11:26,000 --> 00:11:33,000
And it's like this when two people are vying for the love of one person.

116
00:11:33,000 --> 00:11:40,000
So the children get caught in the middle and the parents fight using the children.

117
00:11:40,000 --> 00:11:47,000
And that can be a great amount of suffering for both the parents and the children obviously.

118
00:11:47,000 --> 00:11:55,000
The children feel terrible having to be put in the middle and obviously they don't want to choose between their parents.

119
00:11:55,000 --> 00:11:58,000
So how do you deal with these sufferings?

120
00:11:58,000 --> 00:12:10,000
Once you can come to terms with the idea of the divorce and overcoming the concept of the life that you had that has now been shattered.

121
00:12:10,000 --> 00:12:15,000
This is where obviously meditation is of great benefit.

122
00:12:15,000 --> 00:12:30,000
So if you begin to practice meditation, if you're able to look at the experience as it is, I think it'll be essential and the incredibly beneficial during that time.

123
00:12:30,000 --> 00:12:33,000
You have to understand that it's going to be a specific length of time.

124
00:12:33,000 --> 00:12:35,000
Eventually your parents are going to move on.

125
00:12:35,000 --> 00:12:36,000
They may already have moved on.

126
00:12:36,000 --> 00:12:39,000
It's been a while since this question was posted.

127
00:12:39,000 --> 00:12:58,000
But whatever suffering comes from the divorce or even long-term suffering in terms of not having the same stability that you had before has to be ameliorated through understanding, through the ability to accept the reality in front of you.

128
00:12:58,000 --> 00:13:19,000
Just because it's different doesn't mean it's bad, just because one is stressed and has headaches and is having to deal with difficult, more difficult situation than before, even having to work more or work to support one's family, work to support one's self and so on.

129
00:13:19,000 --> 00:13:39,000
That doesn't mean that one has to suffer if you can accept and live with the reality and not cling to your idea of how it should be or be upset by disappointment, by not getting things the way you think they should be.

130
00:13:39,000 --> 00:13:55,000
When you're able to simply understand things as they are and to see that this is what's happening, when your relationship with reality is not, I wish it could be some other way or I hope it stays this way, your relationship with reality is it is this way.

131
00:13:55,000 --> 00:14:24,000
And every moment knowing that now it is this way, now it is this way, when you're able to think in that way that this is reality, this is reality, and you see things and you know that you're seeing, you hear things and you know that you're hearing and your relationship with reality is an understanding and an acceptance and a clear awareness of it as it is with no thoughts of the past or the future in terms of regret.

132
00:14:24,000 --> 00:14:29,000
Or worry or feelings of loss and so on.

133
00:14:29,000 --> 00:14:33,000
Then you will be, obviously you will be far better off.

134
00:14:33,000 --> 00:14:42,000
This is clearly the preferable path and so this is really the way out of suffering.

135
00:14:42,000 --> 00:14:44,000
This is the path that the Buddha taught.

136
00:14:44,000 --> 00:14:48,000
So you can take a look at some of the videos I posted on how to practice meditation.

137
00:14:48,000 --> 00:14:55,000
I think it will be of great help to anyone suffering from this sort of stress or any other stress.

138
00:14:55,000 --> 00:14:57,000
And I hope those are views quickly.

139
00:14:57,000 --> 00:15:08,000
If you haven't looked at those videos, then it's a very simple practice to see things as they are instead of wishing they were some other way or holding on to these things and hoping that they'd stay this way.

140
00:15:08,000 --> 00:15:11,000
You simply remind yourself that it is what it is.

141
00:15:11,000 --> 00:15:14,000
You don't put any value judgment on it.

142
00:15:14,000 --> 00:15:18,000
When you see something, you say to yourself, seeing, knowing that this is seeing.

143
00:15:18,000 --> 00:15:22,000
When you hear hearing, when you smell, you say to yourself, smelling.

144
00:15:22,000 --> 00:15:24,000
When you feel angry, you say to yourself, angry.

145
00:15:24,000 --> 00:15:26,000
When you feel sad, you say to yourself, sad.

146
00:15:26,000 --> 00:15:33,000
When you feel pain or headache, there's one of them pain or headache, aching, aching is right.

147
00:15:33,000 --> 00:15:43,000
Simply knowing it for what it is, this teaches you how to accept and to understand reality for what it is instead of wishing it were some other way or holding on.

148
00:15:43,000 --> 00:15:48,000
Or holding on to and clinging to it and setting yourself up for greater suffering.

149
00:15:48,000 --> 00:15:59,000
So, but I'd encourage you to look at the videos that I've posted and if you have time to read the book, I have a book on my web blog that's better than the videos.

150
00:15:59,000 --> 00:16:08,000
At least in terms of the information, it contains and it should give a basic understanding of how to practice meditation in the tradition that I follow.

151
00:16:08,000 --> 00:16:13,000
So, thanks for the question, hope that helped are the best.

