1
00:00:00,000 --> 00:00:07,920
Hello and welcome back to our study of the Dhamapada.

2
00:00:07,920 --> 00:00:15,960
Today we continue on with verse 258, which reads as follows.

3
00:00:15,960 --> 00:00:45,920
When it's not a pundit, simply because

4
00:00:45,920 --> 00:01:02,160
one talks a lot, one who is safe,

5
00:01:02,160 --> 00:01:20,520
Vengeless, fearless, is rightly called a pundit.

6
00:01:20,520 --> 00:01:28,920
So this verse was told in response to an issue that arose regarding an infamous group

7
00:01:28,920 --> 00:01:37,680
of monks called the group of six, I guess because there were six of them.

8
00:01:37,680 --> 00:01:43,160
But they became infamous, infamous, the group of six monks became an infamous group of

9
00:01:43,160 --> 00:01:51,120
troublemaker monks who often tried to find ways to skirt around the rules and yet

10
00:01:51,120 --> 00:01:55,880
maintain their status as monks, reading their stories, you might wonder why they weren't

11
00:01:55,880 --> 00:02:01,400
just kicked out.

12
00:02:01,400 --> 00:02:08,560
But it's a testament to, or it's a lesson, show it, show something about Buddhism that

13
00:02:08,560 --> 00:02:22,880
we're tolerant and patient and non-authoritarian, so allowing people to learn on their own.

14
00:02:22,880 --> 00:02:32,920
And only taking action in cases where a lion has been crossed and taking appropriate

15
00:02:32,920 --> 00:02:43,000
action, not simply deciding that we don't want someone in kicking them out.

16
00:02:43,000 --> 00:02:49,360
The story is quite short, so there's not much to tell and I'll just make a note that

17
00:02:49,360 --> 00:02:54,600
this is already been the case, I think I mentioned it as well, but one thing about the

18
00:02:54,600 --> 00:03:02,080
Dhammapada is the commentary is that it starts off with long, elaborate, generational

19
00:03:02,080 --> 00:03:11,160
sometimes stories, epic in some cases, but starts to lose steam as it goes along and I'm

20
00:03:11,160 --> 00:03:17,520
not sure why exactly, but by the end or by the middle even the stories, many of the stories

21
00:03:17,520 --> 00:03:21,000
become quite short.

22
00:03:21,000 --> 00:03:29,080
So the story is that these group of six were setting themselves up to be something like

23
00:03:29,080 --> 00:03:35,520
pundits, so they would append it in the west as someone who is an expert on things.

24
00:03:35,520 --> 00:03:40,640
In the time the word pundit comes from directly from the Sanskrit, we're in English,

25
00:03:40,640 --> 00:03:46,440
we see on TV these political pundits or so on, means an expert on some topic, someone who

26
00:03:46,440 --> 00:03:56,240
goes and shows or goes in public and speaks on a topic, and it takes those ideas from

27
00:03:56,240 --> 00:04:01,640
the original meaning of the word, someone who appenditas, and it's originally from the

28
00:04:01,640 --> 00:04:08,280
Sanskrit word pundita, the Sanskrit poly word, which means someone who has learned it,

29
00:04:08,280 --> 00:04:17,720
who is wise, who is an expert, it really took on a meaning of its own in many cultures,

30
00:04:17,720 --> 00:04:24,720
so in the Buddhist time I'm sure it had the meaning of that sort, someone who was not

31
00:04:24,720 --> 00:04:31,600
just wise living in the forest, but wise in an expressive sort of way, someone who was

32
00:04:31,600 --> 00:04:40,040
a teacher, who was a lecturer, it's kind of not exactly, but it was involved with lecturing,

33
00:04:40,040 --> 00:04:46,240
involved with talking on subjects, hence the word or the phrase not simply because they

34
00:04:46,240 --> 00:04:52,680
speak a lot, it's not, so we often translate punditas wise one, but it means something,

35
00:04:52,680 --> 00:04:57,840
I think a little bit more specific, it certainly does in most cultures, in Thai culture

36
00:04:57,840 --> 00:05:03,080
they have a word pundit, and I'm sure in the other Buddhist cultures, they also use the

37
00:05:03,080 --> 00:05:18,600
word pundit in Thai, so these six monks were, I guess, pushing their opinions on people

38
00:05:18,600 --> 00:05:26,280
and challenging anyone else who would debate them, challenging not in a fair way, not

39
00:05:26,280 --> 00:05:36,000
in a debate sense, but insulting them, and apparently even abusing them, so they would

40
00:05:36,000 --> 00:05:42,200
strut around in the dhamma hall or in the main hall where the monks would gather, and

41
00:05:42,200 --> 00:05:48,880
they would go from monastery to monastery doing this, strutting around, spouting off and abusing

42
00:05:48,880 --> 00:05:57,360
anyone else who tried to criticize them or argue with them or even tried to teach anything,

43
00:05:57,360 --> 00:06:03,640
so if lay people might come, they would ask questions, and the monks would try to answer

44
00:06:03,640 --> 00:06:10,800
in these six monks would just insult them and ridicule them, and even apparently the

45
00:06:10,800 --> 00:06:21,760
text says they would grab them and dump dustbin, you know, the dustpan all over them

46
00:06:21,760 --> 00:06:27,560
with the sweepings, that's what it is, the stuff they sweep on, they dump it over their heads,

47
00:06:27,560 --> 00:06:38,800
quite terrible behavior, and so one day an novice was coming from the hall and the monks

48
00:06:38,800 --> 00:06:42,320
were asking, how is it going in there? They said, don't ask me how it's going, this

49
00:06:42,320 --> 00:06:48,680
group of six monks, and they just dumped all the sweepings on my head, that's how terrible

50
00:06:48,680 --> 00:06:58,760
they are, sort of taking control of the place. I think maybe something's lost in the

51
00:06:58,760 --> 00:07:02,920
translation as well, I just can't quite picture how they were acting, but they were

52
00:07:02,920 --> 00:07:19,960
acting quite representable, and so the monks were quite surprised by this and concerned,

53
00:07:19,960 --> 00:07:26,920
so they brought their concerns to the Buddha and the Buddha taught this verse, he said,

54
00:07:26,920 --> 00:07:42,600
yeah, these guys are not pundits, so it's first of all a lesson if we, before we talk

55
00:07:42,600 --> 00:07:51,120
about how it relates to our practice, it's a lesson in teaching because that often becomes

56
00:07:51,120 --> 00:07:58,040
a part of everyone's practice. As a meditation practitioner, you are often confronted with

57
00:07:58,040 --> 00:08:06,120
the opportunity to talk about what you have become increasingly an expert on, the more

58
00:08:06,120 --> 00:08:14,000
you practice, the more effort you put into it, the more you spend cultivating mindfulness,

59
00:08:14,000 --> 00:08:23,720
the more proficient you are at it, and we always have to remember it's much more important

60
00:08:23,720 --> 00:08:31,840
to maintain positive qualities of mind than a positive attitude. We have to be teaching

61
00:08:31,840 --> 00:08:39,800
for the right reasons, it's quite common in the beginning for people to teach with an

62
00:08:39,800 --> 00:08:50,000
agenda or a pushy attitude, a strong conviction that they feel everyone else should share,

63
00:08:50,000 --> 00:08:58,120
and a conviction can often run far ahead of wisdom such that we don't accept that people

64
00:08:58,120 --> 00:09:10,080
might reject our meditation, and so we can become upset and unmindful when dealing with

65
00:09:10,080 --> 00:09:19,680
people who are not interested in what we are so interested in. Absolutely, there is no

66
00:09:19,680 --> 00:09:25,480
need to talk a lot, and that's something we should always keep in mind when we relate

67
00:09:25,480 --> 00:09:30,280
to others, when we teach the Dhamma, it's not about how much you talk, it's not about

68
00:09:30,280 --> 00:09:35,720
how little you talk either, talking very little as the benefit of not saying the wrong

69
00:09:35,720 --> 00:09:42,720
thing, but it's not necessarily the right thing either. Sometimes there are words that

70
00:09:42,720 --> 00:09:49,600
need to be spoken, that's not the problem, it's just not simply because one talks a lot,

71
00:09:49,600 --> 00:09:55,760
that one is wise. So how this relates to our practice, because our practice of course isn't

72
00:09:55,760 --> 00:10:01,160
about teaching, and I hope you're not teaching each other during your course or teaching

73
00:10:01,160 --> 00:10:09,720
anyone during your course, during your practice you are trying to teach yourself, and

74
00:10:09,720 --> 00:10:15,360
so the verse itself, the word bandita can be just seen as one who is wise, and in that

75
00:10:15,360 --> 00:10:21,880
way it very much does relate to our practice. Wisdom is very much the goal that we're

76
00:10:21,880 --> 00:10:29,040
seeking. It's just important to understand what is meant in Buddhism by wisdom, because

77
00:10:29,040 --> 00:10:36,240
it's not really how we think of it in the West or in any in modern times. As in modern

78
00:10:36,240 --> 00:10:42,240
times what is esteemed and prized is often intellect, cleverness, skill and argument,

79
00:10:42,240 --> 00:10:48,180
skill in logic, and all of those things are generally positive qualities, but they aren't

80
00:10:48,180 --> 00:10:56,800
wisdom. Wisdom has much more to do with your outlook and your familiarity with reality.

81
00:10:56,800 --> 00:11:02,400
Your connection with reality, like someone who is out of touch we say in English, is

82
00:11:02,400 --> 00:11:07,120
the idea of someone who isn't wise. If they're out of touch with reality, we say that

83
00:11:07,120 --> 00:11:15,280
in many ways with worldly topics, but someone who is literally out of touch with reality

84
00:11:15,280 --> 00:11:19,440
in the sense they're living in the past, they're living in the future, or they're caught

85
00:11:19,440 --> 00:11:27,200
up in judgment, someone who is very much a victim of their own partialities, biases, views,

86
00:11:27,200 --> 00:11:34,680
opinions, beliefs, those sorts of things. This is someone who we might say is out of touch.

87
00:11:34,680 --> 00:11:40,200
And another way of putting it, it's actually interesting that the Buddha would choose

88
00:11:40,200 --> 00:11:45,720
these three words, because they aren't everything that is meant by someone who is wise,

89
00:11:45,720 --> 00:11:51,640
and they do relate very much to the story. These guys, it's not that their wisdom is

90
00:11:51,640 --> 00:11:56,920
synonymous, or someone's wisdom is synonymous with these three qualities. It's that a person

91
00:11:56,920 --> 00:12:01,240
who is in touch with reality would never act in the way that these monks were, so there's

92
00:12:01,240 --> 00:12:14,440
no way that they could be considered wise. So while these three qualities are not everything

93
00:12:14,440 --> 00:12:21,560
it means to be wise, they do give us a hint at the connection between ethics and wisdom,

94
00:12:21,560 --> 00:12:27,720
which is another thing that is often not clearly understood in modern times. I think often

95
00:12:27,720 --> 00:12:34,360
people who come to Buddhism, Buddhist practice, don't realize the important connection. In fact,

96
00:12:34,360 --> 00:12:40,840
the intrinsic connection between ethics and wisdom. It's not that ethics and wisdom are connected,

97
00:12:40,840 --> 00:12:47,080
really, it's that ethics and wisdom are inseparable. In many ways, one and the same,

98
00:12:48,440 --> 00:12:56,120
they're very much a part of the same thing. Meaning, wisdom is all about

99
00:12:56,120 --> 00:13:06,280
your ethics, how you treat others, how you react, how you interact with reality. If you get angry,

100
00:13:06,280 --> 00:13:15,400
that's not simply a manifestation of lack of wisdom. It is directly unwise. It is an unwise state

101
00:13:15,400 --> 00:13:33,240
of mind. So in some ways, these three qualities of being safe, of being vengeless, which is a word

102
00:13:33,240 --> 00:13:37,400
we don't use much in English, but it's the opposite of vengeful. Someone who doesn't have

103
00:13:37,400 --> 00:13:50,680
vengeance and free from fear or fearless. The curious thing about these words

104
00:13:52,600 --> 00:13:59,400
is that from the sounds of it, they sound like something positive for oneself that I don't have

105
00:13:59,400 --> 00:14:09,640
any fear. I am safe from harm, from others, and I am, well, the second one, when you hear it,

106
00:14:09,640 --> 00:14:15,640
usually means the opposite, it means I'm not vengeful towards others. So it relates very much

107
00:14:15,640 --> 00:14:23,560
to the story, but the thing about these words is they can go both ways. In English, they often

108
00:14:23,560 --> 00:14:31,080
have a very definite meaning one way or the other, but in Pauli, came me, one who is safe.

109
00:14:34,360 --> 00:14:38,840
Well, it can go both ways, and I think it's useful for us to talk about it both ways.

110
00:14:38,840 --> 00:14:46,280
And what I mean is, safe can mean I am safe from the harm that others bring, but it can also

111
00:14:46,280 --> 00:14:53,640
mean I am safe in the sense I am safe to talk to, I am safe to be around, I am safe for other people. It

112
00:14:53,640 --> 00:14:59,000
is safe to be around me, and that's very much related to the story, so I think it's very

113
00:14:59,000 --> 00:15:05,800
important, but it's also very much related to our practice. These three words are, I think,

114
00:15:05,800 --> 00:15:12,200
really instructive, and I'll try to explain why, instructive in understanding how our practice

115
00:15:12,200 --> 00:15:17,880
works, what the benefits are, how being mindful, what the benefits are of wisdom, and what the goal

116
00:15:17,880 --> 00:15:28,040
is in our practice. So being safe in terms of your own safety from the harm that others might bring,

117
00:15:30,840 --> 00:15:37,160
and the harm that might come to you from any source, it's important to understand that this sort

118
00:15:37,160 --> 00:15:45,000
of safety, the safety that meditation or mindfulness brings in this regard is very different

119
00:15:45,720 --> 00:15:56,040
from the safety you bring to others by being mindful. By being mindful, you absolutely do bring

120
00:15:56,040 --> 00:16:02,280
safety to others. How are you safe? How are others safe from you? I mean it should be quite obvious.

121
00:16:02,280 --> 00:16:11,080
All of the harm that we might cause intentionally to others comes from bias, comes from partiality.

122
00:16:11,080 --> 00:16:19,720
We can't hurt others intentionally or bring intentionally, bring harm to others with wisdom or

123
00:16:19,720 --> 00:16:27,320
with objectivity. You harm others out of cruelty, you harm others out of fear. All of these

124
00:16:27,320 --> 00:16:33,640
things that you come to understand in meditation as being harmful. An interesting thing about

125
00:16:33,640 --> 00:16:45,560
the self-ether dichotomy is that it's not, it's not, it's dissonant to wish for yourself to be free

126
00:16:45,560 --> 00:16:54,120
from suffering and to harm others, to bring suffering to others. If I myself learn as a part of being

127
00:16:54,120 --> 00:17:00,440
mindful, the sorts of qualities of mind that harm myself, I can't ever want them for myself.

128
00:17:00,440 --> 00:17:07,640
It just isn't possible, the familiarity, the being in touch with them that comes from being mindful

129
00:17:07,640 --> 00:17:13,640
creates this dissonance. You just can't abide by them hurting yourself. You start to see that

130
00:17:13,640 --> 00:17:20,600
that's what we do in life. We hurt ourselves and you start to see that, well you see it as just

131
00:17:20,600 --> 00:17:25,720
that hurting yourself and there's an incapacity to continue. You're unable to hurt yourself.

132
00:17:26,520 --> 00:17:32,760
Mindfulness doesn't just give you the opportunity to decide better. It makes you incapable of hurting

133
00:17:32,760 --> 00:17:39,640
yourself. At the same time, it makes you incapable of hurting others because they're one and the same.

134
00:17:39,640 --> 00:17:49,400
They involve the same mind states. You can't absolutely know and be clearly in touch with what is

135
00:17:49,400 --> 00:17:54,520
to your benefit and not act for the benefit of others. It just can't happen. There's a dissonance

136
00:17:54,520 --> 00:18:00,200
there. There would be a dissonance in the mind that mindfulness doesn't allow. It of course comes

137
00:18:00,200 --> 00:18:06,360
up when you're not mindful but cannot exist with someone who is in touch with reality. It's this

138
00:18:06,360 --> 00:18:13,720
wonderful quality of reality that we can't possibly harm ourselves or others if we're in touch

139
00:18:13,720 --> 00:18:19,800
with reality. It's just a very profound truth. One of the core truths of Buddhism that

140
00:18:19,800 --> 00:18:24,600
wisdom is simply being in touch with reality. When you're in touch with reality, you don't have

141
00:18:24,600 --> 00:18:32,040
any problem with ignorance or delusion or making a mistake. Any mistakes, you may get our purely

142
00:18:33,880 --> 00:18:43,480
accidental accidents. That really sums up what it means to bring safety to yourself and to

143
00:18:43,480 --> 00:18:48,440
others at the same time. That safety will bring to others is very visceral. You no longer hurt

144
00:18:48,440 --> 00:18:53,640
others physically. You no longer hurt others verbally. You no longer act in such a way as to

145
00:18:53,640 --> 00:18:59,240
manipulate others. All of these safeties are very real and physical. They're the kind of thing that

146
00:18:59,240 --> 00:19:07,560
we wish for ourselves. Unfortunately, mindfulness doesn't bring so much of that sort of safety to

147
00:19:07,560 --> 00:19:15,640
yourself. Your mindfulness is not going to control what other people do. It's certainly not going

148
00:19:15,640 --> 00:19:22,040
to control the weather or natural disasters or the economy or these sorts of things.

149
00:19:23,160 --> 00:19:29,880
Mindfulness doesn't make you safe from the sorts of harm that you bring to others. It's an

150
00:19:29,880 --> 00:19:37,160
incredible benefit that we bring to the world. We can feel so good about ourselves. The more mindful

151
00:19:37,160 --> 00:19:44,520
we are, the more in touch with reality we are. Because of how free people become from our

152
00:19:47,400 --> 00:19:55,320
from our harm. But the safety that it brings to us is important to understand as well.

153
00:19:55,320 --> 00:20:02,360
And I already touched upon it. It's the freedom from bias, from partiality.

154
00:20:02,360 --> 00:20:15,240
Because the truth of suffering involves quite explicitly reaction, judgment.

155
00:20:16,920 --> 00:20:26,760
A person who is perfectly in touch with reality can and will still suffer physically.

156
00:20:26,760 --> 00:20:35,560
They can still be hurt physically by others. They can be, of course, accosted with verbal abuse.

157
00:20:36,520 --> 00:20:43,480
They can be met with circumstances that create loss. But what is missing is the

158
00:20:45,320 --> 00:20:54,120
reaction to these experiences, the judgment, the upset and the stress that comes to people who

159
00:20:54,120 --> 00:21:01,800
don't cultivate mindfulness, who are out of touch with reality. Which is, in fact, a much more

160
00:21:01,800 --> 00:21:10,680
important sort of safety. That in fact, one of the things that we stress in mindfulness practice

161
00:21:11,320 --> 00:21:20,200
is that it's not the pain or the experiences in general that are unpleasant or suffering.

162
00:21:20,200 --> 00:21:29,880
It's how we react to them. It's our inability to face them. Our inability to maintain

163
00:21:30,680 --> 00:21:37,240
our peace of mind in the face of these experiences. And that's distinct from the experience itself.

164
00:21:37,240 --> 00:21:43,480
It isn't a foregone conclusion that we're going to dislike pain or be upset by pain.

165
00:21:43,480 --> 00:21:51,160
And in fact, what that means is that pain is in fact not stressful or painful or while it's not

166
00:21:51,160 --> 00:22:00,360
unpleasant. It's possible to be completely at peace and still experience any number of

167
00:22:01,240 --> 00:22:09,240
undesirable experiences. And that's, of course, an incredible safety. It's really the only safety

168
00:22:09,240 --> 00:22:17,560
because we can bring our safety to others. But we also cannot ensure they're complete safety just

169
00:22:17,560 --> 00:22:23,160
by our actions. We can't protect anyone else from all the age sickness and death.

170
00:22:25,640 --> 00:22:32,520
And not to mention all the many other possible consequences of their actions and of the actions of

171
00:22:32,520 --> 00:22:40,600
those around them. So that's in regards to safety. The most important safety is always going

172
00:22:40,600 --> 00:22:52,200
to be our own safety of wisdom. The second one, Vengeless, also goes both ways.

173
00:22:54,040 --> 00:22:59,800
The word weirah, I use the word vengeance. I'm not sure that that's what weirah means.

174
00:22:59,800 --> 00:23:05,160
It's what it means in Thailand. And I was told quite explicitly that that's how I had to translate it.

175
00:23:06,760 --> 00:23:09,320
But I think it actually is something a bit simpler.

176
00:23:13,800 --> 00:23:19,640
The idea of vengeance is very common in Buddhist culture and Buddhist texts. There's a

177
00:23:21,560 --> 00:23:27,720
I think it's a Dhamapado or a Jataka story. A very famous story of this, it might be somewhere else

178
00:23:27,720 --> 00:23:37,480
actually. There's a story of these two women who are married to the same man. It was in India at

179
00:23:37,480 --> 00:23:47,560
the time of the Buddha or in those times. It was a male dominant in society and men would marry

180
00:23:47,560 --> 00:23:55,160
as many women, rich men would marry as many women as they wanted. So these two women had the same

181
00:23:55,160 --> 00:24:06,760
husband and one of them was very jealous of the other. So she put some nettles in this woman's

182
00:24:06,760 --> 00:24:10,440
bad, I think, is this terrible thing. She did, she ended up killing the woman.

183
00:24:13,320 --> 00:24:19,320
Killing the woman and her baby or something just horribly. That's right, the woman got pregnant.

184
00:24:19,320 --> 00:24:27,960
And this other woman was very jealous of her. And so she killed the woman. She

185
00:24:27,960 --> 00:24:32,360
gave them, she caused the woman to have an abortion, I think, drink, give her some drink.

186
00:24:34,680 --> 00:24:43,480
And the woman died along with the baby as a result of it. The story, it leads me exactly.

187
00:24:43,480 --> 00:24:48,360
But the woman died. And when she died, this woman realized that the other woman had poisoned her

188
00:24:49,560 --> 00:24:53,960
and killed her baby and killed her. And as she was dying, she vowed, she said,

189
00:24:54,600 --> 00:25:01,000
I'm going to, in some future life one day, I'm going to kill you and your babies.

190
00:25:05,240 --> 00:25:10,120
And she died and was born a cat in that very house, apparently.

191
00:25:10,120 --> 00:25:20,440
And when the husband found out he was very angry and he was not a very nice man as well and

192
00:25:20,440 --> 00:25:27,400
he killed his wife, he beat her to death, the other wife. She died and she was born as a chicken

193
00:25:29,000 --> 00:25:38,680
in the same house. And when she grew up as a chicken, she had little chicks, she gave

194
00:25:38,680 --> 00:25:44,040
birth to little chicks and this cat, one day saw that the chicken with the chicks and

195
00:25:45,560 --> 00:25:52,040
immediately remembered. It didn't maybe know why, but knew right away what to do.

196
00:25:52,840 --> 00:25:56,360
And killed the hand, killed the hand under chicks.

197
00:26:02,360 --> 00:26:05,400
And the hand and the chicks were born as a cougar,

198
00:26:05,400 --> 00:26:10,440
or a lion or something, a tiger maybe, some wild cat.

199
00:26:13,560 --> 00:26:20,360
And the cat, when the cat died, was born as a deer in the same forest as the wild cat.

200
00:26:22,120 --> 00:26:29,400
And when they met again in the wild cat, the mother deer had a child in the

201
00:26:29,400 --> 00:26:37,720
cougar killed the deer and back and forth and back and forth every lifetime this happened until

202
00:26:37,720 --> 00:26:46,200
finally in the time of the Buddha. This woman was, one of them was born a woman, and the other

203
00:26:46,200 --> 00:26:55,080
was born a demon, a Yakini, this race of beings like an ogre.

204
00:26:55,080 --> 00:27:04,040
And she was bathing in the pool near Jita Wanda, where the Buddha was living.

205
00:27:06,040 --> 00:27:10,920
A pool, there was a pool apparently just outside of Sabhati, so if you can walk, you could walk

206
00:27:10,920 --> 00:27:16,120
from Sabhati to Jita Wanda. So just outside of Sabhati, there was the bathing pools and she was bathing

207
00:27:16,120 --> 00:27:26,760
at one of these ponds sort of like small lake maybe. And she had put her baby down and there

208
00:27:26,760 --> 00:27:39,320
she was bathing in the pond and the Yakini, the demon woman came up and saw her and they looked at

209
00:27:39,320 --> 00:27:46,600
each other and they met their eyes met and they recognized right away from lifetime after

210
00:27:46,600 --> 00:27:52,200
lifetime these two had been at each other's throats and the woman became incredibly completely

211
00:27:53,080 --> 00:28:02,760
froze and then jumped up, grabbed her baby and ran and ran straight into Jita Wanda,

212
00:28:02,760 --> 00:28:05,320
straight into the Buddhist monastery near Sabhati.

213
00:28:05,320 --> 00:28:18,200
Now the thing about Yakas and Yakinis is Jita Wanda being a monastery belonging to the Buddha was of

214
00:28:18,200 --> 00:28:28,920
course surrounded and was a home to various angels where his day was. And so there were

215
00:28:28,920 --> 00:28:36,680
day was at the gate who stopped, prevented the Yakini from entering and this woman ran into Jita Wanda

216
00:28:36,680 --> 00:28:44,200
and the Yakini ran after and stopped at the gate and was stopped by the day was and this woman ran

217
00:28:44,200 --> 00:28:49,080
into Jita Wanda with her baby and ran up to where the Buddha was sitting with all the monks teaching

218
00:28:49,960 --> 00:28:55,800
and took her baby and deposited her baby at the feet of the Buddha and said please

219
00:28:55,800 --> 00:29:02,040
venerable, save me and my child. This is a story, I mean believe it or not, this is

220
00:29:03,000 --> 00:29:09,320
a story that we often tell. The Buddha asked her what had happened, she explained that

221
00:29:09,880 --> 00:29:15,880
what she knew that she knew right away that this beast was going to eat her child and the Buddha

222
00:29:17,000 --> 00:29:25,320
had the monks call the Yakini in and this Yakini came walking in and the Buddha lectured them

223
00:29:25,320 --> 00:29:30,440
and the point, it's related to this story just because it talks about this word weirah. So

224
00:29:31,240 --> 00:29:36,200
if you've never heard about this importance of this concept in Buddhism you can understand how

225
00:29:37,800 --> 00:29:45,000
important it is and how we often think about the nature of this, the nature of Samsara

226
00:29:45,000 --> 00:29:53,160
as containing this element of unrequited vengeance. And so the Buddha said to them,

227
00:29:53,160 --> 00:29:57,400
you know countless lives you have been after each other's throats, he says when is it going to end

228
00:29:59,000 --> 00:30:04,840
and he explained he taught them and he related to them their past lives that they couldn't remember

229
00:30:06,440 --> 00:30:13,560
help them to see that there was no value in any of this and helped them to become friends

230
00:30:14,680 --> 00:30:21,640
and their vengeance. So that's the common story about this word weirah. But I think weirah can just

231
00:30:21,640 --> 00:30:35,720
mean an imity of any sort like these monks were hostile but it does often take on the meaning of

232
00:30:35,720 --> 00:30:44,600
relating to holding a grudge. And so of course someone who is wise should not hold a grudge

233
00:30:44,600 --> 00:30:55,400
and should not react at all obviously but it's a good sign of a lack of wisdom when you do hold on

234
00:30:55,400 --> 00:31:03,080
to some enmity. It's a common thing to come up in meditation, you will think of people who you

235
00:31:03,080 --> 00:31:09,960
have a problem with and who make you upset could be family members, could be fellow employees or

236
00:31:09,960 --> 00:31:18,360
employers or that sort of thing. And there's also, we'll come up of course those people who have

237
00:31:18,360 --> 00:31:23,960
enmity towards you, those people who hold a grudge against you, those who you have wronged perhaps

238
00:31:23,960 --> 00:31:34,920
or those who have bad intentions towards you. And upon the wise person of course doesn't have this

239
00:31:34,920 --> 00:31:43,240
as well. You become free from vengeance from others because you're not entirely always but

240
00:31:47,160 --> 00:31:55,160
you gain this capacity to appease, to placate others through kindness, through compassion

241
00:31:56,280 --> 00:32:03,880
and you find yourself not of course always you can't control others but much more you find

242
00:32:03,880 --> 00:32:11,640
yourself surrounded by those who have positive qualities of mind. You cultivate that in others,

243
00:32:11,640 --> 00:32:15,800
you cultivate respect in others because of your kindness, because of your thoughtfulness,

244
00:32:16,440 --> 00:32:25,160
because of your patience and your wisdom. So this is the second quality. The third quality

245
00:32:25,160 --> 00:32:33,640
abaya. Abaya relates somewhat to the others and especially the first one.

246
00:32:35,480 --> 00:32:44,920
Baya means fear or danger, sort of literally fear. So someone who is fearless

247
00:32:44,920 --> 00:33:00,680
is a sign of wisdom. But also someone who doesn't inspire fear in others or someone who

248
00:33:00,680 --> 00:33:09,320
others need not fear. And so how this is different from safe if someone is safe,

249
00:33:09,320 --> 00:33:23,400
it's interesting because safe can describe any person who has positive circumstances.

250
00:33:24,200 --> 00:33:33,800
All of us are probably quite safe from most dangers living here. Irrespective of our meditation

251
00:33:33,800 --> 00:33:45,640
practice. Most people in this city are reasonably safe, safe from war. Many people are in this

252
00:33:45,640 --> 00:33:54,520
society or safe from poverty, safe from hunger, safe from disease. Not completely but we can live

253
00:33:54,520 --> 00:34:04,440
most of our lives, much of our lives quite safe. The difference in this regard is many people

254
00:34:04,440 --> 00:34:15,000
who are safe still have fear. A person can be completely safe and still terrorized by unreasonable

255
00:34:15,000 --> 00:34:24,200
fear. Even reasonable fear, reasonable in the sense that it absolutely is possible for any one

256
00:34:24,200 --> 00:34:31,240
of us to suddenly become unsafe. To suddenly be confronted with danger, this house could collapse.

257
00:34:31,800 --> 00:34:38,440
Anyone of us could get sick at any time. We could be robbed, we could be attacked,

258
00:34:38,440 --> 00:34:44,840
we could be run over by a car or we could be get in a car accident, anything can happen. So many

259
00:34:44,840 --> 00:34:53,960
things. And for some people this is quite fearsome. But a sign of someone who is wise is someone

260
00:34:53,960 --> 00:35:03,960
who is fearless. Sometimes we think of fear as being useful but it's one of these cases where we

261
00:35:03,960 --> 00:35:16,440
mix up what is useful from what is ancillary. So understanding the dangers does not require a person

262
00:35:16,440 --> 00:35:21,640
to be afraid. This is important in our meditation practice. Through the meditation you're going to

263
00:35:21,640 --> 00:35:30,840
appreciate very much the danger in clinging, the danger in reacting, the danger in judging,

264
00:35:30,840 --> 00:35:38,280
the danger in losing touch with reality. But at the same time you're going to lose your fear.

265
00:35:39,640 --> 00:35:49,960
So you end up in a state, a quality of mind that is at once very conscious of that which is

266
00:35:49,960 --> 00:35:58,120
dangerous in terms of mind states but also in terms of life in general. You're capable very,

267
00:35:58,120 --> 00:36:07,400
very much more than most people of appreciating and assessing danger. But you are completely or

268
00:36:07,400 --> 00:36:16,200
you are more and more through your practice free from any sort of fear. So a fearless person

269
00:36:16,200 --> 00:36:22,120
is not someone who ignores danger. These are two very different things, two distinct things. But a

270
00:36:22,120 --> 00:36:29,640
person who has fear cannot be seen as wise. A person should never have fear. Fear is not a wholesome

271
00:36:29,640 --> 00:36:35,560
state and it's a mistake that we make to think that it is the ability to assess danger.

272
00:36:36,760 --> 00:36:40,920
There's actually talked about in the texts because it's an important part of the practice to see

273
00:36:40,920 --> 00:36:46,360
the danger. To see the bayah, it's called bayah, like to see the fear, but it doesn't mean you

274
00:36:46,360 --> 00:36:53,160
become afraid. It's explicitly stated, you don't. Practice doesn't make you afraid. Practice helps

275
00:36:53,160 --> 00:37:01,720
you see the fearfulness of clinging. But of course it encourages you not to cling. It doesn't make

276
00:37:01,720 --> 00:37:11,240
you afraid. And on the other side, of course meditation means others have nothing to fear from you.

277
00:37:11,240 --> 00:37:17,400
Of course doesn't mean that others will not fear you. Hopefully it makes them fear you less,

278
00:37:18,440 --> 00:37:24,120
but very much more that people have nothing to fear from you. And this is a very powerful

279
00:37:25,160 --> 00:37:29,560
result of the practice that you might not think about. You might not have thought about. But

280
00:37:29,560 --> 00:37:36,840
for just a simple example of keeping the five precepts, it's often described as being what we

281
00:37:36,840 --> 00:37:45,640
call a bayah dana. Dana meaning a gift. It's a gift of fearlessness. And what that means is nobody

282
00:37:45,640 --> 00:37:52,040
has anything to fear from you. If you don't kill, you have suddenly given a gift to all beings

283
00:37:52,040 --> 00:38:07,640
when you absolutely unequivocally, without any ifs or ifs and or buts, undertake not to kill a living

284
00:38:07,640 --> 00:38:13,240
being. You have given freedom to the universe and you have given freedom from fear. This is called

285
00:38:13,240 --> 00:38:22,520
a bayah dana. When you don't steal, when you don't cheat or lie or in any way hard, when you undertake

286
00:38:22,520 --> 00:38:36,840
not to harm others. And again, this relates back to safety. But you'll give people no reason to fear

287
00:38:36,840 --> 00:38:45,880
you. You become someone that others can trust and generally do trust and generally do rely upon

288
00:38:45,880 --> 00:38:59,400
and feel safe around. That's a great thing about goodness. It doesn't really bear mentioning

289
00:38:59,400 --> 00:39:04,920
because it's so obvious, but it's funny how it should be so obvious and yet it's not that goodness

290
00:39:04,920 --> 00:39:20,680
is good. That which leads others to see you as good. We often take shortcuts in this regard

291
00:39:20,680 --> 00:39:29,960
with other people trying to impress them, trying to boast and push on them. Our good qualities

292
00:39:29,960 --> 00:39:38,280
trying to manipulate them or scare them into respecting us, none of which is goodness. And yet

293
00:39:38,280 --> 00:39:47,560
it's goodness that benefits us and benefits others. So this is why wisdom has so much to do with

294
00:39:47,560 --> 00:39:58,280
ethics. It's not independent. Wisdom is not scientific discovery. Or it is scientific discovery,

295
00:39:58,280 --> 00:40:05,480
but the science that modern scientists haven't discovered is that there is something intrinsically

296
00:40:08,040 --> 00:40:14,920
real about ethics and goodness that they are an intrinsic part of reality because reality is

297
00:40:14,920 --> 00:40:26,360
based on experience. Okay, so that's quite a lot of talk about a verse that was about not talking

298
00:40:26,360 --> 00:40:33,400
a lot or about wisdom not being about talking a lot, but I hope that the topics are pertinent

299
00:40:34,360 --> 00:40:46,120
and helpful for your practice. And I wish you all success and progress in your practice.

300
00:40:46,120 --> 00:41:02,040
Thank you for listening.

