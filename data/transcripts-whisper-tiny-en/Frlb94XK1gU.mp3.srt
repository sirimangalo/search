1
00:00:00,000 --> 00:00:18,000
Okay, so welcome everyone to our live broadcast and real life meditation, teachings and practice.

2
00:00:18,000 --> 00:00:20,000
Everybody get comfortable.

3
00:00:20,000 --> 00:00:26,000
Remember we're taking this as an opportunity to learn about ourselves.

4
00:00:26,000 --> 00:00:32,000
The teaching of the Buddha is to learn something about yourself.

5
00:00:32,000 --> 00:00:43,000
It's not philosophy or theoretical science or so on.

6
00:00:43,000 --> 00:00:47,000
It's practical, it's meant to help us.

7
00:00:47,000 --> 00:00:55,000
So sit down, sit in meditation, close your eyes, don't look at me.

8
00:00:55,000 --> 00:01:01,000
I'm not going to help you, you have to help yourself.

9
00:01:01,000 --> 00:01:14,000
Tonight I'd like to talk about the question, what is Buddhism?

10
00:01:14,000 --> 00:01:19,000
It's a question I don't get asked that often.

11
00:01:19,000 --> 00:01:36,000
I think it's a rather useful question and it's one that we're often confused about in the nature of Buddhism.

12
00:01:36,000 --> 00:01:40,000
And obviously there are going to be many answers to this question.

13
00:01:40,000 --> 00:01:45,000
I think that's probably why you don't get asked it and why people don't think about it too much.

14
00:01:45,000 --> 00:01:55,000
Because it's such a broad topic, it's very hard to get your head around Buddhism.

15
00:01:55,000 --> 00:02:02,000
So the answer that I give is I guarantee it's only one of many answers.

16
00:02:02,000 --> 00:02:06,000
It's probably not the most all-inclusive answer out there.

17
00:02:06,000 --> 00:02:14,000
It might not even be the most popular answer out there.

18
00:02:14,000 --> 00:02:23,000
But Buddhism for me is something very particular and so I'm calling it as I see it.

19
00:02:23,000 --> 00:02:27,000
Calling it as Buddhism means for me, it means to me.

20
00:02:27,000 --> 00:02:31,000
I think Buddhism is something else and so be it.

21
00:02:31,000 --> 00:02:34,000
I have no problem with that.

22
00:02:34,000 --> 00:02:40,000
But Buddhism as I see it, if someone asks me, what is Buddhism?

23
00:02:40,000 --> 00:02:46,000
I'd say Buddhism is the teachings of the Buddha or the teachings of a Buddha.

24
00:02:46,000 --> 00:02:49,000
I think that's fair.

25
00:02:49,000 --> 00:02:57,000
I don't think that's completely, I don't think that's entirely agreed upon.

26
00:02:57,000 --> 00:03:08,000
But I think it's a simple definition that's one that many outsiders would easily understand.

27
00:03:08,000 --> 00:03:14,000
The teachings of a Buddha and who is a Buddha, Buddha is someone who knows.

28
00:03:14,000 --> 00:03:24,000
And as I understand it, a Buddha is someone who knows and understands the four noble truths.

29
00:03:24,000 --> 00:03:32,000
But as someone who understands those things that when understood, lead one to,

30
00:03:32,000 --> 00:03:37,000
or bring one transport one directly to complete freedom from suffering.

31
00:03:37,000 --> 00:03:47,000
Not simply by knowing these things, not simply, but the entire set of things that are required to free one from suffering.

32
00:03:47,000 --> 00:03:52,000
Upon the realization of these things, one no longer suffers.

33
00:03:52,000 --> 00:04:01,000
Basically the understanding of suffering and how it works.

34
00:04:01,000 --> 00:04:07,000
And the question that we often get asked is, is Buddhism a religion?

35
00:04:07,000 --> 00:04:10,000
And there are many answers to this question.

36
00:04:10,000 --> 00:04:12,000
And these answers are all out there.

37
00:04:12,000 --> 00:04:15,000
If you Google it, you'd probably find right away.

38
00:04:15,000 --> 00:04:18,000
Buddhism religion is Buddhism a religion.

39
00:04:18,000 --> 00:04:23,000
You have people asking and answering.

40
00:04:23,000 --> 00:04:29,000
My answer is that yes, Buddhism is a religion.

41
00:04:29,000 --> 00:04:33,000
And I agree with some people who say Buddhism is not a religion.

42
00:04:33,000 --> 00:04:38,000
Because it totally depends on what you mean by the word religion.

43
00:04:38,000 --> 00:04:47,000
But the word religion, it comes generally from, it's generally understood to come from being bound to something.

44
00:04:47,000 --> 00:04:51,000
And I'm willing to bet that that's where the word actually came from.

45
00:04:51,000 --> 00:05:08,000
That it was being bound to God or being bound to a set of teachings, a set of principles, a set of theories, a set of beliefs.

46
00:05:08,000 --> 00:05:28,000
And while Buddhism isn't fixated on the belief in God or the idea of a supreme being, Buddhism is, as I say, it's an attachment to certain beliefs and ideas and practices and so on.

47
00:05:28,000 --> 00:05:35,000
To me, religious Buddhism is taking Buddhism, taking the teaching of the Buddha's religion religiously.

48
00:05:35,000 --> 00:05:47,000
The things that the Buddha taught, we hold to those, we are restrained by those.

49
00:05:47,000 --> 00:05:51,000
We are bound to the teachings of the Buddha.

50
00:05:51,000 --> 00:06:03,000
If you consider yourself Buddhist, a part of the Buddhist religion, then the minimum requirement, and I'd say the only requirement is that you stick to the teachings of the Buddha.

51
00:06:03,000 --> 00:06:07,000
And to me, that makes it religion.

52
00:06:07,000 --> 00:06:11,000
That's qualification enough for it to be a religion.

53
00:06:11,000 --> 00:06:15,000
It doesn't have to be faith, it doesn't have to be belief.

54
00:06:15,000 --> 00:06:19,000
If you practice the Buddha's teachings out of faith, then you're doing it religiously.

55
00:06:19,000 --> 00:06:28,000
If you practice the teachings of the Buddha out of wisdom, you're still doing it religiously.

56
00:06:28,000 --> 00:06:38,000
As long as you stick to the teachings of the Buddha, or able to.

57
00:06:38,000 --> 00:06:46,000
So, the teaching that I often do get asked by Thai people, and it's an indirect question.

58
00:06:46,000 --> 00:06:57,000
It's one of these funny things about cultural Buddhists, people who grew up Buddhist, but never really studied or practiced Buddhist teachings in any great depth.

59
00:06:57,000 --> 00:07:10,000
They often ask me, how would they explain Buddhism in a nutshell to someone who doesn't know anything about Buddhism?

60
00:07:10,000 --> 00:07:21,000
How would you explain Buddhism that what is the core of Buddhism to someone who doesn't know anything about Buddhism?

61
00:07:21,000 --> 00:07:29,000
And this question is answered quite well, I think, and across the board.

62
00:07:29,000 --> 00:07:34,000
And I think the best answer is given quite often.

63
00:07:34,000 --> 00:07:41,000
That the teachings of the Buddha are summed up by what is called the Owa Dapati Mocha.

64
00:07:41,000 --> 00:07:54,000
The Owa Dapati Mocha means the code that's translated as code.

65
00:07:54,000 --> 00:07:57,000
In this case, monastic code you could say.

66
00:07:57,000 --> 00:08:07,000
The code of the Buddha, the real core essence of the Dharma, the teachings of the Buddha.

67
00:08:07,000 --> 00:08:12,000
And Owa Dap means the speech.

68
00:08:12,000 --> 00:08:17,000
So, this is the speech given by the Buddha that is the core or the code.

69
00:08:17,000 --> 00:08:23,000
It was to be the binding code or the 10 commandments of Buddhism.

70
00:08:23,000 --> 00:08:31,000
Except it's not commandments, it's edicts or it's injunctions or so on.

71
00:08:31,000 --> 00:08:42,000
It's laws, you can say, that the Buddha said, this is this, this is this and so on.

72
00:08:42,000 --> 00:08:47,000
And how it goes is the Buddha first sums up his teachings.

73
00:08:47,000 --> 00:09:03,000
That the teaching of all the Buddhas, the Buddha and the sassana.

74
00:09:03,000 --> 00:09:09,000
Whether they be in the past or in the present Buddha that is just recently passed away,

75
00:09:09,000 --> 00:09:16,000
or whether they be future Buddhas, their teaching is all comes down to three things.

76
00:09:16,000 --> 00:09:31,000
Not doing any evil deeds, number one, number two, being full of good deeds, and number three purifying the mind.

77
00:09:31,000 --> 00:09:35,000
These three things the Buddha said, this is the teaching of all the Buddhas.

78
00:09:35,000 --> 00:09:38,000
Anyone who claims to be a Buddha has to teach these three things.

79
00:09:38,000 --> 00:09:43,000
Anyone who is a perfectly enlightened Buddha teaches these three things.

80
00:09:43,000 --> 00:09:49,000
It's really not a lot.

81
00:09:49,000 --> 00:09:56,000
In fact, one of the wonderful things about the Buddha's teaching is what it's not.

82
00:09:56,000 --> 00:10:03,000
From my point of view, one of the great things about the Buddha's teaching is what it leaves out, what it leaves behind.

83
00:10:03,000 --> 00:10:08,000
There's no excess baggage in Buddhism, in the Buddha's teaching.

84
00:10:08,000 --> 00:10:16,000
We Buddhists have put a lot of extra baggage on it. We have these ceremonies and practices.

85
00:10:16,000 --> 00:10:20,000
But the core teachings of the Buddha are very simple.

86
00:10:20,000 --> 00:10:24,000
If you can follow these three things, you don't need anything else.

87
00:10:24,000 --> 00:10:31,000
If you can stop doing all evil deeds, fill yourself up with goodness and purify your mind,

88
00:10:31,000 --> 00:10:39,000
then you become enlightened. According to the Buddha, you're, according to the Buddha's teaching, you're enlightened.

89
00:10:39,000 --> 00:10:42,000
There's nothing else you need to do.

90
00:10:42,000 --> 00:10:48,000
When someone asks, what are the teachings of the Buddha?

91
00:10:48,000 --> 00:10:54,000
These three are the nutshell. Buddhism in the nutshell.

92
00:10:54,000 --> 00:11:00,000
You can go into the four noble truths and so on, but if you want people to understand exactly what is Buddhism.

93
00:11:00,000 --> 00:11:18,000
And they should understand that that's all it is, that there's no requirements in terms of believing this or believing that worship or rights and rituals.

94
00:11:18,000 --> 00:11:23,000
You don't even have to call yourself Buddhist.

95
00:11:23,000 --> 00:11:47,000
If you do these three things, you're following the teachings of the Buddha.

96
00:11:47,000 --> 00:11:57,000
The key of the three, the one that really sets the Buddha's teaching apart from other religious teachings,

97
00:11:57,000 --> 00:12:07,000
or according to us, it does. I think it's not 100% cut and dry, but the one thing where the Buddha really shines,

98
00:12:07,000 --> 00:12:10,000
where the Buddha's teaching really shines, is in the purification of the mind.

99
00:12:10,000 --> 00:12:14,000
Because it's not something you hear talked about a lot.

100
00:12:14,000 --> 00:12:19,000
You might find it mentioned in other religious traditions,

101
00:12:19,000 --> 00:12:25,000
but the purification of the mind becomes the central theme of Buddhism,

102
00:12:25,000 --> 00:12:29,000
and actually not doing bad deeds and doing good deeds,

103
00:12:29,000 --> 00:12:36,000
actually becomes secondary or becomes less central to the teachings.

104
00:12:36,000 --> 00:12:41,000
Yes, it's important to refrain from bad deeds, and yes, it's important to do good deeds,

105
00:12:41,000 --> 00:12:47,000
but the most important, by far most important,

106
00:12:47,000 --> 00:12:52,000
and in fact, the only important thing for you to do is to purify your mind.

107
00:12:52,000 --> 00:12:57,000
Because once you've done that, you can never do bad deeds.

108
00:12:57,000 --> 00:13:02,000
Everything you do is either a neutral deed or a good deed.

109
00:13:02,000 --> 00:13:11,000
You can never perform an evil deed if you mind this pure.

110
00:13:11,000 --> 00:13:18,000
And so, well, it would never do for us to ignore the first two.

111
00:13:18,000 --> 00:13:21,000
You shouldn't say, oh, well, you do bad deeds.

112
00:13:21,000 --> 00:13:26,000
If you don't think about doing good deeds, don't worry about that.

113
00:13:26,000 --> 00:13:30,000
Just try to purify your mind. You can't say that,

114
00:13:30,000 --> 00:13:34,000
because every time you do a bad deed, your mind becomes more defiled.

115
00:13:34,000 --> 00:13:38,000
And without performing good deeds, your mind will never have the power,

116
00:13:38,000 --> 00:13:46,000
and the strength to become free to be pure,

117
00:13:46,000 --> 00:13:54,000
without undertaking the first two, your mind will never become pure.

118
00:13:54,000 --> 00:13:57,000
But when we want to understand what is the core,

119
00:13:57,000 --> 00:13:59,000
and what is the most important, it's the purification of the mind.

120
00:13:59,000 --> 00:14:05,000
We should never be complacent, content just to be a good person,

121
00:14:05,000 --> 00:14:09,000
and not do bad deeds, thinking that that's enough.

122
00:14:09,000 --> 00:14:15,000
Because inevitably, when we are aroused, bad thoughts will come into our minds,

123
00:14:15,000 --> 00:14:18,000
whether it be thoughts of anger, hate theories,

124
00:14:18,000 --> 00:14:27,000
and extraneous superfluous ideas about what the Buddha taught on the other side.

125
00:14:27,000 --> 00:14:32,000
You get all sorts of things, people say that the Buddha was a social activist.

126
00:14:32,000 --> 00:14:38,000
The outcast in India said he was their champion.

127
00:14:38,000 --> 00:14:51,000
In Buddhist countries, we see rituals and even worship and prayer come up.

128
00:14:51,000 --> 00:14:59,000
And Buddhism becomes a much fuller entity.

129
00:14:59,000 --> 00:15:03,000
To the point, not that these things are wrong,

130
00:15:03,000 --> 00:15:07,000
and the Buddha certainly was good for the lower class in India.

131
00:15:07,000 --> 00:15:11,000
And prayer in a certain form,

132
00:15:11,000 --> 00:15:15,000
even worship and ritual in a certain form, can be wholesome.

133
00:15:15,000 --> 00:15:19,000
But it gets to the point where we lose sight of the core.

134
00:15:19,000 --> 00:15:21,000
The set of what is the Buddha's teaching,

135
00:15:21,000 --> 00:15:28,000
because the Buddha didn't teach us to rise up and raise our status in society.

136
00:15:28,000 --> 00:15:35,000
The Buddha didn't teach us to worship him and perform rights in rituals,

137
00:15:35,000 --> 00:15:40,000
and to pray the Buddha taught us these three things.

138
00:15:40,000 --> 00:15:43,000
And if we do these other associated things,

139
00:15:43,000 --> 00:15:47,000
then as long as they have to do with the purification of the mind,

140
00:15:47,000 --> 00:15:52,000
then they can fit in with the Buddha's teaching.

141
00:15:52,000 --> 00:15:55,000
I often talked about culture.

142
00:15:55,000 --> 00:15:57,000
Culture when culture mixes with Buddhism,

143
00:15:57,000 --> 00:16:01,000
it's fine if culture can adapt itself to the Buddha's teaching,

144
00:16:01,000 --> 00:16:06,000
but you should never adapt the Buddha's teaching to fit the culture.

145
00:16:06,000 --> 00:16:09,000
Adapt the Buddha's teaching to fit the culture suddenly.

146
00:16:09,000 --> 00:16:16,000
You've got people drinking alcohol and dancing and beauty pageants and so on.

147
00:16:16,000 --> 00:16:21,000
In the monasteries,

148
00:16:21,000 --> 00:16:27,000
it can only degrade the purity of the Buddha's teaching.

149
00:16:27,000 --> 00:16:32,000
This is if we wish to be Buddhist.

150
00:16:32,000 --> 00:16:35,000
We wish to follow the Buddha's teaching.

151
00:16:35,000 --> 00:16:37,000
Then we have to understand what is the core,

152
00:16:37,000 --> 00:16:41,000
what are the things that the Buddha actually taught?

153
00:16:41,000 --> 00:16:45,000
So what else did the Buddha teach?

154
00:16:45,000 --> 00:16:48,000
He said, this is the core, or he didn't say this is the core.

155
00:16:48,000 --> 00:16:50,000
He said, this is the teachings of all the Buddha.

156
00:16:50,000 --> 00:16:53,000
We understand this to be the core.

157
00:16:53,000 --> 00:16:55,000
What else did he say?

158
00:16:55,000 --> 00:17:03,000
He said, patience is the highest form of austerity,

159
00:17:03,000 --> 00:17:09,000
and the Buddha among the Buddha.

160
00:17:09,000 --> 00:17:14,000
I think no one understands this as well as meditators.

161
00:17:14,000 --> 00:17:18,000
And in that sense, it's spot on.

162
00:17:18,000 --> 00:17:21,000
It's the first thing you need to tell a meditator.

163
00:17:21,000 --> 00:17:25,000
Patience is the highest form of austerity.

164
00:17:25,000 --> 00:17:31,000
Patience is the greatest form of torture.

165
00:17:31,000 --> 00:17:36,000
One way of to translate this is the form of torture.

166
00:17:36,000 --> 00:17:40,000
Because in India, they would torture themselves.

167
00:17:40,000 --> 00:17:43,000
Torturing themselves saying, okay, I'm going to inflict this pain on that pain

168
00:17:43,000 --> 00:17:45,000
on myself.

169
00:17:45,000 --> 00:17:50,000
And actually, I would say there's nothing as painful as patience.

170
00:17:50,000 --> 00:17:53,000
I would say that because when you inflict pain on yourself,

171
00:17:53,000 --> 00:18:02,000
there's some kind of grim sort of pleasure that you gain from it.

172
00:18:02,000 --> 00:18:07,000
This kind of greeting your teeth and so on,

173
00:18:07,000 --> 00:18:09,000
inflicting something.

174
00:18:09,000 --> 00:18:14,000
You're in charge, but patience is the worst.

175
00:18:14,000 --> 00:18:16,000
Patience is an incredible torture.

176
00:18:16,000 --> 00:18:27,000
Having to sit through my talks for minutes and minutes on end.

177
00:18:27,000 --> 00:18:32,000
When I was in Thailand, we had to sit through talks an hour,

178
00:18:32,000 --> 00:18:35,000
sometimes an hour and a half.

179
00:18:35,000 --> 00:18:40,000
I think that was bad.

180
00:18:40,000 --> 00:18:46,000
They were all in Thai, and we didn't understand the word of it.

181
00:18:46,000 --> 00:18:50,000
That's the worst.

182
00:18:50,000 --> 00:18:52,000
And so here we are, these new meditators.

183
00:18:52,000 --> 00:18:54,000
Our teacher tells us to sit for 10 minutes,

184
00:18:54,000 --> 00:18:59,000
walk for 10 minutes, sit for 10 minutes, and we can do that.

185
00:18:59,000 --> 00:19:02,000
Maybe we're up to 15 minutes walking, 15 minutes a day,

186
00:19:02,000 --> 00:19:04,000
20 minutes walking, 20 minutes.

187
00:19:04,000 --> 00:19:06,000
And suddenly we have to sit still for an hour and a half,

188
00:19:06,000 --> 00:19:11,000
two hours sometimes, with all the ceremonies and so on.

189
00:19:15,000 --> 00:19:16,000
Patience is the worst.

190
00:19:16,000 --> 00:19:18,000
Simply having to bear with things.

191
00:19:18,000 --> 00:19:23,000
And to the point where people often accuse us of torturing ourselves.

192
00:19:23,000 --> 00:19:27,000
You know, sitting through pain when you sit still and then pain arises.

193
00:19:27,000 --> 00:19:29,000
The normal thought is to shift.

194
00:19:29,000 --> 00:19:31,000
Get yourself some more pillows.

195
00:19:31,000 --> 00:19:34,000
You want to do this leg, one under that leg, one behind your back.

196
00:19:34,000 --> 00:19:39,000
And the point where your medist will be lying down.

197
00:19:39,000 --> 00:19:42,000
When we tell them not to, we say, you know,

198
00:19:42,000 --> 00:19:44,000
try your best to sit still.

199
00:19:44,000 --> 00:19:49,000
If you have to prop yourself your legs up in the beginning.

200
00:19:49,000 --> 00:19:52,000
I had one student who started on the floor.

201
00:19:52,000 --> 00:19:53,000
He was doing really good.

202
00:19:53,000 --> 00:19:55,000
And then the second day he put a pillow.

203
00:19:55,000 --> 00:19:57,000
And then the third day, two pillows under his legs.

204
00:19:57,000 --> 00:20:00,000
And then the fourth day and another pillow.

205
00:20:00,000 --> 00:20:03,000
He said to him, you know, look, the object really is to go down.

206
00:20:03,000 --> 00:20:05,000
Not to go up.

207
00:20:05,000 --> 00:20:08,000
And object is to become more tolerant.

208
00:20:08,000 --> 00:20:10,000
Not less tolerant.

209
00:20:13,000 --> 00:20:16,000
It's like you're just trying to find a perfect position.

210
00:20:16,000 --> 00:20:18,000
And then when some pain comes up, you think, OK,

211
00:20:18,000 --> 00:20:19,000
I got to add another pillow.

212
00:20:19,000 --> 00:20:20,000
And then more pain.

213
00:20:20,000 --> 00:20:21,000
And I got to add more.

214
00:20:21,000 --> 00:20:31,000
And people say, oh, you're bearing with the pain that's torture.

215
00:20:31,000 --> 00:20:33,000
That's wrong practice.

216
00:20:33,000 --> 00:20:35,000
That's against the Buddhist teaching.

217
00:20:35,000 --> 00:20:38,000
And we have to vehemently deny this.

218
00:20:38,000 --> 00:20:44,000
This is not someone who teaches this way is not looking up for your best interest.

219
00:20:44,000 --> 00:20:47,000
And it's not going according to the Buddhist teaching.

220
00:20:47,000 --> 00:20:49,000
When they tell you you have to adjust, you have to move,

221
00:20:49,000 --> 00:20:53,000
you have to avoid all pain.

222
00:20:53,000 --> 00:20:56,000
It's very clear from the Buddhist teaching that you have to put up with pain,

223
00:20:56,000 --> 00:20:59,000
even to the point that it might kill you.

224
00:20:59,000 --> 00:21:02,000
If you really want to become free from suffering,

225
00:21:02,000 --> 00:21:06,000
you have to be willing to put up with even deadly pain.

226
00:21:06,000 --> 00:21:08,000
Because that's what we're faced with.

227
00:21:08,000 --> 00:21:12,000
And the end, deadly pain is on our horizon.

228
00:21:12,000 --> 00:21:16,000
That may be how we die in deadly pain.

229
00:21:16,000 --> 00:21:21,000
And if we're not able to bear with it now,

230
00:21:21,000 --> 00:21:23,000
it's really the pain when we die.

231
00:21:23,000 --> 00:21:24,000
We'll be much worse.

232
00:21:24,000 --> 00:21:26,000
And if we can't bear with it, we'll have to take drugs.

233
00:21:26,000 --> 00:21:28,000
And we die in a muddled state.

234
00:21:28,000 --> 00:21:30,000
And who knows where we end up?

235
00:21:30,000 --> 00:21:33,000
But for sure we end up attached and clinging

236
00:21:33,000 --> 00:21:35,000
because we haven't let go.

237
00:21:38,000 --> 00:21:41,000
Patients is so important because it helps you to let go.

238
00:21:41,000 --> 00:21:44,000
Patients is the ultimate letting go

239
00:21:44,000 --> 00:21:47,000
and saying to yourself, I can bear with this.

240
00:21:47,000 --> 00:21:51,000
I can live with this.

241
00:21:51,000 --> 00:21:55,000
I can accept this reality.

242
00:21:55,000 --> 00:21:58,000
It's accepting reality for what it is.

243
00:21:58,000 --> 00:22:05,000
I don't need some other other state of reality to make me happy.

244
00:22:05,000 --> 00:22:09,000
It's contentment.

245
00:22:09,000 --> 00:22:13,000
I always think of contentment as this wonderful,

246
00:22:13,000 --> 00:22:18,000
like the crown jewel of existence

247
00:22:18,000 --> 00:22:23,000
if you could only be content with whatever comes.

248
00:22:23,000 --> 00:22:30,000
The Buddha said it's the ultimate treasure.

249
00:22:30,000 --> 00:22:31,000
The ultimate wealth.

250
00:22:31,000 --> 00:22:35,000
The greatest wealth is contentment.

251
00:22:35,000 --> 00:22:38,000
And I think that's so true.

252
00:22:38,000 --> 00:22:40,000
And there's nothing we can get that's ever going to send.

253
00:22:40,000 --> 00:22:42,000
There's no object that we can get

254
00:22:42,000 --> 00:22:46,000
that's ever going to satisfy us except contentment

255
00:22:46,000 --> 00:22:50,000
when we can be content with things as they are.

256
00:22:50,000 --> 00:22:52,000
Because reality is singular.

257
00:22:52,000 --> 00:22:54,000
You only have one reality.

258
00:22:54,000 --> 00:22:56,000
It's this. It's where you are now.

259
00:22:56,000 --> 00:22:58,000
And every moment you have two choices.

260
00:22:58,000 --> 00:23:00,000
Accepting it.

261
00:23:00,000 --> 00:23:02,000
Or not accepting it.

262
00:23:02,000 --> 00:23:06,000
Accepting it or needing something else.

263
00:23:06,000 --> 00:23:12,000
Cleaning.

264
00:23:12,000 --> 00:23:15,000
When you're finally content with things as they are.

265
00:23:15,000 --> 00:23:19,000
And you don't require any specific reality.

266
00:23:19,000 --> 00:23:25,000
This is the ultimate wealth.

267
00:23:25,000 --> 00:23:31,000
And so patience is the highest form of torture.

268
00:23:31,000 --> 00:23:33,000
And this is because it leads to contentment.

269
00:23:33,000 --> 00:23:37,000
It leads you to accept.

270
00:23:37,000 --> 00:23:39,000
The word actually the Buddha uses tapa.

271
00:23:39,000 --> 00:23:44,000
And tapa means to burn.

272
00:23:44,000 --> 00:23:46,000
Or it means heat.

273
00:23:46,000 --> 00:23:49,000
But it was used in India to mean austerity

274
00:23:49,000 --> 00:23:52,000
and torturing yourself and so on.

275
00:23:52,000 --> 00:23:54,000
But if you think about it, what is it doing?

276
00:23:54,000 --> 00:23:56,000
Is it's burning up?

277
00:23:56,000 --> 00:23:58,000
It's burning up the evil inside.

278
00:23:58,000 --> 00:24:04,000
It's burning up the unhostiveness, the things that are causing you suffering.

279
00:24:04,000 --> 00:24:07,000
It's another thing when we talk about evil and Buddhism.

280
00:24:07,000 --> 00:24:12,000
We don't mean God says it's evil or I say it's evil or Buddha says evil.

281
00:24:12,000 --> 00:24:14,000
It means it's something that hurts you.

282
00:24:14,000 --> 00:24:16,000
Something that causes suffering for you.

283
00:24:16,000 --> 00:24:19,000
Something that only leads to your detriment.

284
00:24:19,000 --> 00:24:22,000
It's a pretty selfish sort of thing.

285
00:24:22,000 --> 00:24:26,000
There's nothing that's evil except leads to your detriment.

286
00:24:26,000 --> 00:24:40,000
And how we get avoid the selfishness is because anything that hurts anyone else also leads to your detriment.

287
00:24:40,000 --> 00:24:43,000
It can't help you to hurt someone else.

288
00:24:43,000 --> 00:24:46,000
So there's no need to worry about being selfish in Buddhism.

289
00:24:46,000 --> 00:24:48,000
People say, I'm sitting in meditation.

290
00:24:48,000 --> 00:24:52,000
What are you doing for other people?

291
00:24:52,000 --> 00:24:57,000
What are you doing to help the world?

292
00:24:57,000 --> 00:25:00,000
That's what you're not doing, really.

293
00:25:00,000 --> 00:25:02,000
In one sense it's what you're not doing.

294
00:25:02,000 --> 00:25:04,000
That is the most benefit.

295
00:25:04,000 --> 00:25:12,000
You're not going out there and yelling at people and fighting with people and grabbing for power.

296
00:25:12,000 --> 00:25:16,000
And think of what other people do when they get together.

297
00:25:16,000 --> 00:25:18,000
Think of what we do when we get together.

298
00:25:18,000 --> 00:25:22,000
The time it's unwholesome, more than half the time.

299
00:25:22,000 --> 00:25:34,000
You know, clinging and you know, posturing.

300
00:25:34,000 --> 00:25:36,000
When we sit in meditation what are we doing?

301
00:25:36,000 --> 00:25:38,000
We're purifying our mind.

302
00:25:38,000 --> 00:25:42,000
So on the one hand we're not hurting anyone and that's a good thing.

303
00:25:42,000 --> 00:25:49,000
But on the other hand we're doing something equally important.

304
00:25:49,000 --> 00:25:52,000
We're preparing ourselves.

305
00:25:52,000 --> 00:25:57,000
We're changing the makeup of our minds.

306
00:25:57,000 --> 00:26:01,000
We're changing the way we look at things.

307
00:26:01,000 --> 00:26:09,000
We're changing the way we react to things.

308
00:26:09,000 --> 00:26:14,000
It's amazing to me that anyone could say that meditating is being lazy.

309
00:26:14,000 --> 00:26:18,000
And I think I understand why it's because they don't understand what is meditation.

310
00:26:18,000 --> 00:26:24,000
But if you understood what meditation was,

311
00:26:24,000 --> 00:26:28,000
it would be incredible to think that somehow it was not, it was unbeneficial.

312
00:26:28,000 --> 00:26:29,000
That it was selfish.

313
00:26:29,000 --> 00:26:34,000
That it wasn't going to help anybody else for you to sit on a pillow and meditate.

314
00:26:34,000 --> 00:26:40,000
Because it's an incredible benefit to everyone you come in contact with.

315
00:26:40,000 --> 00:26:42,000
In contact with.

316
00:26:42,000 --> 00:26:46,000
Excuse me.

317
00:26:46,000 --> 00:26:48,000
You meditate.

318
00:26:48,000 --> 00:26:58,000
You practice, you learn, you understand your patient.

319
00:26:58,000 --> 00:27:03,000
And all of those skills that you gain from the meditation you bring out into the world around you.

320
00:27:03,000 --> 00:27:07,000
Meditation is the beginning of your life.

321
00:27:07,000 --> 00:27:09,000
It's not the end of it.

322
00:27:09,000 --> 00:27:16,000
Meditation is what leads to, it should be the source of all of your actions.

323
00:27:16,000 --> 00:27:21,000
It should be the meditative mind.

324
00:27:21,000 --> 00:27:24,000
Just like the source of water in the spring,

325
00:27:24,000 --> 00:27:32,000
flowing down into the stream and the stream flowing down into the lake.

326
00:27:32,000 --> 00:27:38,000
And if the source is pure, the stream is pure and the lake is pure.

327
00:27:38,000 --> 00:27:46,000
But if the source is defiled, it's impure.

328
00:27:46,000 --> 00:27:52,000
Then the stream is impure and the lake can't help but be impure.

329
00:27:52,000 --> 00:27:59,000
It means everything we do and say and how we interact with the world around us is based on the mind.

330
00:27:59,000 --> 00:28:07,000
This is the source. When our mind is impure, everything we do and say is going to be based on that impurity.

331
00:28:07,000 --> 00:28:12,000
We're going to hurt other people, we're going to hurt ourselves.

332
00:28:12,000 --> 00:28:18,000
So when people say meditation is only for yourself or so.

333
00:28:18,000 --> 00:28:36,000
When meditation is benefit to everyone, even our own meditation.

334
00:28:36,000 --> 00:28:39,000
When we go in two party, I think I've gone on long enough.

335
00:28:39,000 --> 00:28:41,000
So I'm just going to wrap it up there.

336
00:28:41,000 --> 00:28:50,000
I didn't want to go to too much detail about the Buddha's teaching, but just try to get idea of what is Buddhism.

337
00:28:50,000 --> 00:28:57,000
It's a religion where we bind ourselves to these practices.

338
00:28:57,000 --> 00:29:07,000
We make a resolution in ourselves that we're going to work and we're going to base our lives on these principles.

339
00:29:07,000 --> 00:29:21,000
If not hurting other people, not doing evil deeds, trying our best to do good deeds to help other people and to help ourselves and to purification of our minds.

340
00:29:21,000 --> 00:29:25,000
So this is why we're here tonight. We're here to practice these teachings.

341
00:29:25,000 --> 00:29:33,000
It becomes a religion for us when we say this is how we want to live our lives.

342
00:29:33,000 --> 00:29:40,000
And there's no need to make it a religion if you want to just try it and people want to do it as a hobby or so.

343
00:29:40,000 --> 00:29:43,000
And there's no need to call yourself Buddhists.

344
00:29:43,000 --> 00:29:50,000
But there's no need to be afraid of calling yourself Buddhists, at least in your own mind, at least internally.

345
00:29:50,000 --> 00:29:58,000
You can say to yourself that you're Buddhist and you don't have to accept this or that tradition or this or that culture.

346
00:29:58,000 --> 00:30:04,000
We accept these three principles in your Buddhist.

347
00:30:04,000 --> 00:30:09,000
So especially this one, the third one, this is what we're focusing on here.

348
00:30:09,000 --> 00:30:19,000
This is what we're here together to do. We're here to purify our minds to purify our understanding of ourselves in the world around us.

349
00:30:19,000 --> 00:30:26,000
And we start with our formal meditation practice which we can then take out into our daily lives.

350
00:30:26,000 --> 00:30:35,000
In our daily lives we can apply these principles of being mindful of knowing what we're doing, of seeing things for what they are.

351
00:30:35,000 --> 00:30:42,000
No more, no less. This is what we're training ourselves here.

352
00:30:42,000 --> 00:30:50,000
Thank everyone for coming and now we'll go right into the practical section, segment.

353
00:30:50,000 --> 00:30:57,000
First we'll do mindful frustration and then walk in and that's it.

