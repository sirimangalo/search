1
00:00:00,000 --> 00:00:06,640
He lacks with foreigners, but it's an intricate language.

2
00:00:06,640 --> 00:00:07,840
OK, good evening.

3
00:00:07,840 --> 00:00:12,080
We're broadcasting live a couple minutes late.

4
00:00:12,080 --> 00:00:16,880
And just got off the phone with a man who

5
00:00:16,880 --> 00:00:19,640
is going to help us this Sunday.

6
00:00:19,640 --> 00:00:23,680
So he owns a Thai restaurant in Dundas called Bangkok

7
00:00:23,680 --> 00:00:28,120
Spoon, a free advertising reel.

8
00:00:28,120 --> 00:00:30,360
But he's a really nice guy, and he's

9
00:00:30,360 --> 00:00:33,880
been a supporter of the monastery in Stony Creek.

10
00:00:33,880 --> 00:00:37,200
And so I told him we're opening a new monastery in your McMaster

11
00:00:37,200 --> 00:00:43,480
asked if he would come and maybe bring some food for the monks.

12
00:00:43,480 --> 00:00:47,240
And he said he's going to Toronto on Sunday.

13
00:00:47,240 --> 00:00:48,960
So he won't be able to come.

14
00:00:48,960 --> 00:00:52,880
But he said he will send his wife and he's still.

15
00:00:52,880 --> 00:00:57,320
And so I was talking Thai to him and Robin.

16
00:00:57,320 --> 00:01:02,120
And I were just Robin recognized some of the words.

17
00:01:02,120 --> 00:01:05,000
But he can't take me as an example.

18
00:01:05,000 --> 00:01:07,480
We were talking because the words I use,

19
00:01:07,480 --> 00:01:10,000
because monks are on a higher level than laypeople.

20
00:01:10,000 --> 00:01:12,760
So I use words that you would actually normally

21
00:01:12,760 --> 00:01:15,440
reserve for talking to very young people.

22
00:01:15,440 --> 00:01:17,960
I could never use those words for a monk.

23
00:01:17,960 --> 00:01:20,480
So I say kap Thai to him.

24
00:01:20,480 --> 00:01:22,240
Most monks don't even say kap Thai.

25
00:01:22,240 --> 00:01:24,480
Though it's the way my teacher talks.

26
00:01:24,480 --> 00:01:26,800
So it's a fairly familiar.

27
00:01:26,800 --> 00:01:30,200
If you want to, for a monk, you would have to say,

28
00:01:30,200 --> 00:01:33,720
for to be formal, like a monk, you would have to say,

29
00:01:33,720 --> 00:01:37,880
just animal tonight, which means animal tonight, right?

30
00:01:37,880 --> 00:01:42,880
Or you would say jaloenpawn, which it's quite formal.

31
00:01:42,880 --> 00:01:44,200
I do that sometimes.

32
00:01:44,200 --> 00:01:47,040
Jaloenpawn, jaloenpawn is like, yes,

33
00:01:47,040 --> 00:01:51,160
but it's very, very formal way of saying yes.

34
00:01:51,160 --> 00:01:55,240
Anyway, so that's good news.

35
00:01:55,240 --> 00:02:00,880
This Sunday looks like it's going to work out well.

36
00:02:00,880 --> 00:02:06,320
Today, we had our first meditation group on campus.

37
00:02:06,320 --> 00:02:11,360
And maybe I can show you a picture of those of you

38
00:02:11,360 --> 00:02:16,680
who don't have a leaf, follow my every move on Facebook.

39
00:02:16,680 --> 00:02:18,600
Here's the picture.

40
00:02:18,600 --> 00:02:22,880
Let's see if we can get this up here.

41
00:02:22,880 --> 00:02:23,880
Screen share.

42
00:02:29,720 --> 00:02:30,360
There.

43
00:02:30,360 --> 00:02:31,360
Can you see that?

44
00:02:31,360 --> 00:02:32,720
Oh, very nice, yes.

45
00:02:39,640 --> 00:02:40,640
Point that didn't work.

46
00:02:40,640 --> 00:02:57,920
Yeah, it's a nice campus.

47
00:02:57,920 --> 00:03:00,680
I mean, people come here to have weddings and stuff,

48
00:03:00,680 --> 00:03:02,320
because it's fairly green.

49
00:03:02,320 --> 00:03:06,520
But behind the university is a big nature preserve.

50
00:03:06,520 --> 00:03:10,160
Because of how awful Hamilton is, they

51
00:03:10,160 --> 00:03:14,880
tried to offset it by making lots of natural preserves.

52
00:03:14,880 --> 00:03:16,640
No, the chemicals in the water and stuff

53
00:03:16,640 --> 00:03:22,840
probably limited efficacy of that activity.

54
00:03:22,840 --> 00:03:27,240
But nonetheless, we'll go on then.

55
00:03:27,240 --> 00:03:30,760
So yeah, we had, let's see, come to me.

56
00:03:30,760 --> 00:03:37,360
One, two, three, four, five, six, seven, eight, nine.

57
00:03:37,360 --> 00:03:39,640
Looks like nine there.

58
00:03:39,640 --> 00:03:42,880
Let's go back here.

59
00:03:42,880 --> 00:03:47,200
Oops, there.

60
00:03:47,200 --> 00:03:50,880
One, two, three, four, five, six, seven, eight, nine, ten,

61
00:03:50,880 --> 00:03:55,840
eleven plus me is twelve.

62
00:03:55,840 --> 00:03:58,320
This is just, this is when I was just demonstrating

63
00:03:58,320 --> 00:04:01,360
how to practice.

64
00:04:01,360 --> 00:04:05,280
Just explaining it had them close their eyes and sitting.

65
00:04:05,280 --> 00:04:07,400
So it took about an hour and it was,

66
00:04:07,400 --> 00:04:11,560
everyone was quite happy with it.

67
00:04:11,560 --> 00:04:18,200
This girl on the left, Michelle, she's an old, old time

68
00:04:18,200 --> 00:04:19,800
meditator.

69
00:04:19,800 --> 00:04:24,840
She's been to Thailand and her parents are Thai.

70
00:04:24,840 --> 00:04:26,680
I think maybe her father's love,

71
00:04:26,680 --> 00:04:29,200
emotion and that's her.

72
00:04:29,200 --> 00:04:35,000
And this guy, he said he would join our executive.

73
00:04:35,000 --> 00:04:40,000
These two showed up late, but we're, you know, very, very keen.

74
00:04:40,000 --> 00:04:41,920
Well, we're awesome.

75
00:04:41,920 --> 00:04:45,560
This is our president of our organization,

76
00:04:45,560 --> 00:04:49,960
sitting full notice and not Buddhist,

77
00:04:49,960 --> 00:04:52,640
but very interested in Buddhism.

78
00:04:52,640 --> 00:04:54,520
I think that's how it is.

79
00:04:54,520 --> 00:04:58,840
Jordan, don't remember his name.

80
00:04:58,840 --> 00:05:01,440
Jennifer, Jennifer is, I think,

81
00:05:01,440 --> 00:05:05,080
with you in the knees background, but she's, I met her in,

82
00:05:05,080 --> 00:05:07,280
and I said, wow, I can't remember.

83
00:05:07,280 --> 00:05:09,840
But she is, the reason I say is because she,

84
00:05:09,840 --> 00:05:11,640
she comes to Stony Creek,

85
00:05:11,640 --> 00:05:13,840
or she's in, I met her at Stony Creek.

86
00:05:16,600 --> 00:05:18,880
And don't remember his name, shoot,

87
00:05:18,880 --> 00:05:22,760
by the names too many names, too many names, too many people.

88
00:05:22,760 --> 00:05:25,640
I should remember some of my names.

89
00:05:25,640 --> 00:05:34,400
Anyway, so that's that, everyone, I share it with you.

90
00:05:34,400 --> 00:05:35,760
Why?

91
00:05:35,760 --> 00:05:37,520
Because you share good deeds with each other,

92
00:05:37,520 --> 00:05:39,440
so everyone can appreciate them.

93
00:05:39,440 --> 00:05:41,440
Because when you appreciate the good deeds of others,

94
00:05:41,440 --> 00:05:43,320
even though you didn't, you weren't involved

95
00:05:43,320 --> 00:05:46,240
with the good deeds, then you get goodness.

96
00:05:46,240 --> 00:05:47,200
Goodness comes to you.

97
00:05:47,200 --> 00:05:50,760
It's called Patanumodana, Mahyabunya.

98
00:05:50,760 --> 00:05:55,320
Punya that comes from rejoicing in the charity

99
00:05:55,320 --> 00:06:00,320
of others, Patanumodana, something like that.

100
00:06:01,800 --> 00:06:03,400
But I'm not quite sure about that.

101
00:06:03,400 --> 00:06:07,320
Patanumodana, the Punya namodana,

102
00:06:07,320 --> 00:06:10,160
and when you're anamodana, when you rejoice

103
00:06:10,160 --> 00:06:14,000
in the goodness of others, this is, this is goodness.

104
00:06:14,000 --> 00:06:17,160
It's goodness just to rejoice in the goodness of others.

105
00:06:17,160 --> 00:06:18,880
Though you're talking about how great it is

106
00:06:18,880 --> 00:06:21,320
to be charitable and kind to others,

107
00:06:21,320 --> 00:06:22,960
but when you appreciate the kindness

108
00:06:22,960 --> 00:06:27,840
in the goodness of others, that in and of itself is goodness.

109
00:06:27,840 --> 00:06:30,240
So if you appreciate the appreciation of others,

110
00:06:30,240 --> 00:06:33,320
you see, okay, it's just an endless stream of goodness.

111
00:06:35,400 --> 00:06:38,040
Goodness is not that difficult, you know?

112
00:06:38,040 --> 00:06:39,680
Compnovation of goodness.

113
00:06:39,680 --> 00:06:41,080
We should talk more about that.

114
00:06:42,360 --> 00:06:43,920
Goodness is very important.

115
00:06:45,520 --> 00:06:49,800
So I did say earlier that charity is,

116
00:06:50,800 --> 00:06:52,520
it's not a part of the path.

117
00:06:52,520 --> 00:06:54,840
I think it has to be said, we have to be clear.

118
00:06:54,840 --> 00:06:59,840
We don't want a mistake, charity for the Buddha's teaching,

119
00:06:59,840 --> 00:07:02,960
like for the path, it's not the path

120
00:07:02,960 --> 00:07:04,880
at least in cessation of suffering.

121
00:07:04,880 --> 00:07:08,280
But it's important because of the benefits

122
00:07:08,280 --> 00:07:11,240
that it gives you, the encouragement that it gives you.

123
00:07:12,520 --> 00:07:14,720
But you know, Christians can be generous

124
00:07:14,720 --> 00:07:17,160
and non-Buddhist, people not seeking

125
00:07:17,160 --> 00:07:22,160
Bana, generosity is, can lead you in other directions,

126
00:07:23,960 --> 00:07:24,800
like to heaven.

127
00:07:40,560 --> 00:07:42,960
Well, the weirded out people probably didn't show up.

128
00:07:44,160 --> 00:07:46,800
But it was nice because you can see it's right

129
00:07:46,800 --> 00:07:47,760
in the middle of campus.

130
00:07:47,760 --> 00:07:50,120
So when people get off class, they walk right by us

131
00:07:50,120 --> 00:07:52,160
and it was quite active.

132
00:07:52,160 --> 00:07:53,560
That was the one thing I was concerned

133
00:07:53,560 --> 00:07:55,480
that it might not have been the greatest venue.

134
00:07:55,480 --> 00:07:57,720
But on the other hand, we talked about it

135
00:07:57,720 --> 00:08:02,720
and we could also argue that it's good to,

136
00:08:05,960 --> 00:08:08,360
for good for people to see meditation,

137
00:08:08,360 --> 00:08:10,320
to be reminded of meditation.

138
00:08:11,320 --> 00:08:14,560
And another part of that is that because it's new,

139
00:08:14,560 --> 00:08:17,400
we wanted to let people know that it's happening

140
00:08:17,400 --> 00:08:19,760
in case they were interested in joining us.

141
00:08:19,760 --> 00:08:23,080
And indeed, two or three people, I think, in that group

142
00:08:24,360 --> 00:08:27,960
just joined just because they saw us.

143
00:08:30,160 --> 00:08:33,920
And on top of that, it was like a nice day out.

144
00:08:33,920 --> 00:08:38,560
So we needed, we thought, great to meditate outside.

145
00:08:38,560 --> 00:08:42,280
And it's useful to practice in a place that's not so quiet

146
00:08:42,280 --> 00:08:47,280
because it helps you to incorporate the meditation into your life,

147
00:08:51,960 --> 00:08:54,400
to become better able to deal with distraction.

148
00:08:57,000 --> 00:08:59,840
So anyway, tonight, we have a quote.

149
00:09:01,040 --> 00:09:04,480
Robin, would you address the honor of reading that?

150
00:09:05,480 --> 00:09:06,320
Yes.

151
00:09:06,320 --> 00:09:09,240
Every time you read those quotes, goodness comes to you.

152
00:09:09,240 --> 00:09:10,600
I know more than that.

153
00:09:10,600 --> 00:09:11,880
I know more than that.

154
00:09:11,880 --> 00:09:13,600
There are these four powers.

155
00:09:13,600 --> 00:09:16,560
What for the power of mindfulness,

156
00:09:16,560 --> 00:09:20,200
the power of concentration, the power of innocence,

157
00:09:20,200 --> 00:09:21,640
and the power of unity?

158
00:09:23,040 --> 00:09:25,880
The power of innocence, that's my favorite.

159
00:09:25,880 --> 00:09:28,520
I had to look at the polyguess, I'm not gonna say that.

160
00:09:28,520 --> 00:09:29,800
It does.

161
00:09:29,800 --> 00:09:33,560
I know what jambala, and what jambala is power.

162
00:09:33,560 --> 00:09:37,240
And that is means no, no,

163
00:09:37,240 --> 00:09:40,560
un-meant, no, a wadja, or a wadja,

164
00:09:40,560 --> 00:09:44,360
I know wadja, why is it?

165
00:09:44,360 --> 00:09:46,360
Anna is negative, right?

166
00:09:48,160 --> 00:09:50,240
Instead of a wadja, it's un-wadja.

167
00:09:52,320 --> 00:09:53,920
In a sense, doesn't seem to be a word

168
00:09:53,920 --> 00:09:55,680
that we come across all that often

169
00:09:55,680 --> 00:09:57,480
in the scripture, doesn't it?

170
00:09:57,480 --> 00:10:01,600
No, it's usually translated as

171
00:10:01,600 --> 00:10:06,480
a blamelessness, or freedom from fault,

172
00:10:06,480 --> 00:10:10,320
because wadja means fault, it's a faultlessness.

173
00:10:12,120 --> 00:10:14,160
So we do come across, but not as innocent.

174
00:10:14,160 --> 00:10:16,040
See, he's doing well as a translator

175
00:10:16,040 --> 00:10:20,480
in terms of making it a palatable, approachable.

176
00:10:21,400 --> 00:10:23,040
Understand, easily understandable,

177
00:10:23,040 --> 00:10:25,080
by English speakers, which is a great survey.

178
00:10:26,200 --> 00:10:28,520
So innocence really gets the point across much better

179
00:10:28,520 --> 00:10:33,520
than freedom from fault or blamelessness, even innocence.

180
00:10:33,800 --> 00:10:35,720
I mean, it's actually not a literal translation

181
00:10:35,720 --> 00:10:39,640
because un-wadja is negative, it's a negative of something,

182
00:10:39,640 --> 00:10:42,480
like not being something, so like blamelessness,

183
00:10:42,480 --> 00:10:44,680
or freedom from fault or faultlessness.

184
00:10:46,040 --> 00:10:48,760
But innocence does sound nice.

185
00:10:51,280 --> 00:10:53,640
So the power of mindfulness, let's go through these.

186
00:10:53,640 --> 00:10:55,400
Why is mindfulness powerful?

187
00:10:55,400 --> 00:10:58,200
Mindfulness is the ultimate solvent.

188
00:10:58,200 --> 00:11:02,440
All the stickiness, all of our attachments fade away.

189
00:11:02,440 --> 00:11:04,880
It's like the ultimate power.

190
00:11:04,880 --> 00:11:07,000
It's the most powerful thing

191
00:11:08,760 --> 00:11:11,800
because it's invincible.

192
00:11:13,640 --> 00:11:16,520
And there's nothing that can hurt you if you're mindful.

193
00:11:19,120 --> 00:11:21,360
Because you don't react, or mindfulness means not reacting.

194
00:11:24,720 --> 00:11:27,400
Power of concentration is different.

195
00:11:27,400 --> 00:11:29,680
Concentration, not as powerful, I would say.

196
00:11:29,680 --> 00:11:31,560
Although it depends because

197
00:11:31,560 --> 00:11:34,800
John concentrate the highest concentration is John.

198
00:11:34,800 --> 00:11:36,600
And it's very powerful.

199
00:11:36,600 --> 00:11:40,720
Worldly John is like summit to John is very powerful.

200
00:11:40,720 --> 00:11:43,880
They say even fire and weapons.

201
00:11:43,880 --> 00:11:46,960
If you enter into the John, it's a legend.

202
00:11:46,960 --> 00:11:49,560
I don't know if it's really true, but apparently you can't.

203
00:11:52,600 --> 00:11:55,480
A weapon won't, it can be shot or something.

204
00:11:55,480 --> 00:11:58,240
You won't get hurt.

205
00:11:58,240 --> 00:12:00,360
It would be an interesting thing to test them.

206
00:12:00,360 --> 00:12:03,240
Have people go into John and then shoot them with,

207
00:12:03,240 --> 00:12:06,040
shoot them to see if they can get hurt.

208
00:12:06,040 --> 00:12:09,320
Then we shoot them in rubber bullets or something.

209
00:12:09,320 --> 00:12:14,480
It's probably not Bounya, unless they were a willing participant.

210
00:12:19,360 --> 00:12:22,080
But that's about, there's a power to it nonetheless.

211
00:12:22,080 --> 00:12:26,800
Regardless of that, you can say emotions don't arise.

212
00:12:26,800 --> 00:12:29,640
So mindfulness, we say, it makes you invincible.

213
00:12:29,640 --> 00:12:33,000
Well, concentration also makes you invincible.

214
00:12:33,000 --> 00:12:40,000
But it's more of avoiding because you don't hear.

215
00:12:40,000 --> 00:12:44,000
So mindfulness, you hear, if someone calls you nasty names,

216
00:12:44,000 --> 00:12:47,600
you hear it, but it doesn't, you don't react to it.

217
00:12:47,600 --> 00:12:52,480
Concentration is not non-reactivity, it's the transcendence.

218
00:12:52,480 --> 00:12:54,640
So you're not there to hear it.

219
00:12:54,640 --> 00:12:58,360
Your mind isn't at the ear, your mind is focusing on the Buddha

220
00:12:58,360 --> 00:13:03,680
or it's focusing on mid-downs, focusing on something else.

221
00:13:03,680 --> 00:13:05,480
That's worldly, John.

222
00:13:05,480 --> 00:13:12,120
Now, local drajana is another John concentration,

223
00:13:12,120 --> 00:13:13,600
but it's super mundane.

224
00:13:13,600 --> 00:13:18,280
It's the focus is nibana, the object of the John is nibana.

225
00:13:18,280 --> 00:13:23,760
So at that time, it's also not non-reactivity.

226
00:13:23,760 --> 00:13:28,480
It's you don't hear it because there's no hearing in nibana.

227
00:13:28,480 --> 00:13:30,840
So that's the power of concentration.

228
00:13:30,840 --> 00:13:34,080
It's total avoidance, total escape.

229
00:13:34,080 --> 00:13:45,200
So worldly and worldly, temporary, super mundane is stable, solid.

230
00:13:45,200 --> 00:13:49,920
So an arahant, even after they come back from nibana,

231
00:13:49,920 --> 00:13:57,080
they will live their lives, but they, what would you say there?

232
00:13:57,080 --> 00:14:00,800
I think you'd still say that was mindfulness, not concentration

233
00:14:00,800 --> 00:14:03,920
because there they actually will hear again.

234
00:14:03,920 --> 00:14:07,600
But they're perfectly mindful, so they don't react.

235
00:14:07,600 --> 00:14:12,080
That's not concentration, but the John is our concentration.

236
00:14:12,080 --> 00:14:21,440
The power of innocence lies in one's ability to know

237
00:14:21,440 --> 00:14:27,480
that when accused of something, you have done nothing wrong.

238
00:14:27,480 --> 00:14:35,520
People who get upset when others accuse them in propriety

239
00:14:35,520 --> 00:14:40,440
or call them nasty names will tend to be those people

240
00:14:40,440 --> 00:14:42,280
who actually have done something wrong.

241
00:14:42,280 --> 00:14:45,240
When you get outraged, someone who is outraged

242
00:14:45,240 --> 00:14:47,080
because people call them nasty.

243
00:14:47,080 --> 00:14:49,880
It's a sign that they're not sure of their own innocence.

244
00:14:49,880 --> 00:14:54,120
So maybe they know they're innocent in that specific instance

245
00:14:54,120 --> 00:14:56,200
so they get outraged about it.

246
00:14:56,200 --> 00:14:58,880
But if they were truly innocent, it wouldn't bother.

247
00:14:58,880 --> 00:15:01,440
There's a power to innocence.

248
00:15:01,440 --> 00:15:04,880
When you know that you are a good person,

249
00:15:04,880 --> 00:15:08,680
not just innocence in this one particular case.

250
00:15:08,680 --> 00:15:14,840
Innocence really means, in a true sense, being a good person.

251
00:15:14,840 --> 00:15:18,400
Good people don't have to, don't get upset when others

252
00:15:18,400 --> 00:15:23,440
accuse them in the power of simply not feeling guilty.

253
00:15:23,440 --> 00:15:25,360
This is a great power.

254
00:15:25,360 --> 00:15:28,960
When you sit in meditation, when you're alone,

255
00:15:28,960 --> 00:15:32,800
when you speak in public, you speak in public.

256
00:15:32,800 --> 00:15:35,840
If you're sure of yourself, there's two ways of being sure of yourself.

257
00:15:35,840 --> 00:15:39,600
You can pretend to be sure you can exude this confidence

258
00:15:39,600 --> 00:15:42,520
or you can truly have done nothing wrong in which case.

259
00:15:42,520 --> 00:15:45,240
You're perfectly happy to go in front of people

260
00:15:45,240 --> 00:15:48,320
and talk about anything.

261
00:15:48,320 --> 00:15:52,880
When people debate you, attack you, argue with you.

262
00:15:52,880 --> 00:15:57,720
If you're innocent, if you know you're right, because you are right,

263
00:15:57,720 --> 00:16:00,240
and there's two ways again.

264
00:16:00,240 --> 00:16:02,040
You can think you're right and be wrong.

265
00:16:02,040 --> 00:16:05,840
So you have no problem arguing because I'm right,

266
00:16:05,840 --> 00:16:08,200
or you can really be right.

267
00:16:08,200 --> 00:16:10,280
The problem is if you're not right and you think you are,

268
00:16:10,280 --> 00:16:13,120
then someone can debate you, argue with you

269
00:16:13,120 --> 00:16:15,360
and show you that you're wrong, and then

270
00:16:15,360 --> 00:16:19,400
your whole confidence crumples, the power, you lose the power.

271
00:16:19,400 --> 00:16:22,640
But you can't lose the power if you're right and you know you're right.

272
00:16:22,640 --> 00:16:27,160
The other hand, if you're if you're right, you know it's confident.

273
00:16:27,160 --> 00:16:36,600
There's you don't have the power, but that is often because you have been wrong before.

274
00:16:36,600 --> 00:16:42,760
You're not, you know you have faults or flawed.

275
00:16:42,760 --> 00:16:45,880
You feel guilty or you feel unsure of yourself.

276
00:16:45,880 --> 00:16:55,520
And so it's easy to doubt even when you are right, innocent springs confidence.

277
00:16:55,520 --> 00:17:01,520
And it brings stability of mind, no guilt, no remorse.

278
00:17:01,520 --> 00:17:04,880
In the power of unity, I didn't have enough time to really look this up,

279
00:17:04,880 --> 00:17:18,920
but I don't know if you're wrong, but I don't know if I'm wrong, but I don't know if I'm wrong.

280
00:17:18,920 --> 00:17:24,560
If so, it means showing kindness, helping.

281
00:17:24,560 --> 00:17:40,240
So, because it's so good, I don't know.

282
00:17:40,240 --> 00:17:51,480
So, I'm going to have some gun, some gun, some gun, some gun, some showing kindness.

283
00:17:51,480 --> 00:18:01,480
The power of showing kindness, I guess the sense here how it relates to unity is of harmony.

284
00:18:01,480 --> 00:18:10,200
And so the power of sangha, sangha means being connected.

285
00:18:10,200 --> 00:18:13,600
Collection, inclusion.

286
00:18:13,600 --> 00:18:17,360
What can also mean kind disposition?

287
00:18:17,360 --> 00:18:22,400
So I think we have to go with that instead, it's not sangha in terms of being connected.

288
00:18:22,400 --> 00:18:27,120
We're going to say that he's got a wrong translation here.

289
00:18:27,120 --> 00:18:31,000
Well according to the commentary, you see some people disagree with the commentaries,

290
00:18:31,000 --> 00:18:35,880
think the commentaries are just making stuff up, which you know sometimes you've got to wonder

291
00:18:35,880 --> 00:18:46,200
whether they're interpreting things or you know whether they're actually being strictly

292
00:18:46,200 --> 00:18:54,720
etymologically or strictly, how do you say it, or whether they're interpreting, or

293
00:18:54,720 --> 00:18:59,400
they're interpreting and not adding meaning, but in this case we go by the commentary.

294
00:18:59,400 --> 00:19:09,600
The commentary says sangha, which it can mean means sun, gun, which means kindness.

295
00:19:09,600 --> 00:19:16,920
And there's a great power to kindness, kindness is powerful.

296
00:19:16,920 --> 00:19:28,960
It makes you friends, it is blameless, you know anyone who blames you for it, it returns

297
00:19:28,960 --> 00:19:34,100
back to them, you know for a good person when you attack a good person, it's like a

298
00:19:34,100 --> 00:19:43,760
boomerang, the negative effects come back and attack you when you attack a good person

299
00:19:43,760 --> 00:19:48,680
when you say bad things about them, when you try to destroy them or hurt them, you only

300
00:19:48,680 --> 00:19:54,200
make them more powerful, people only love them more because they see you.

301
00:19:54,200 --> 00:19:57,560
This is a big thing that people don't realize when they criticize.

302
00:19:57,560 --> 00:20:01,520
There's one teacher or it's a common thing in Thailand to say when you point your finger

303
00:20:01,520 --> 00:20:13,640
at someone, three fingers are pointing back at you, you see these three, so you do more

304
00:20:13,640 --> 00:20:22,680
damage to yourself when you point your finger at someone.

305
00:20:22,680 --> 00:20:27,400
So kindness is powerful in that way, if you're a kind person, a good person, all some things

306
00:20:27,400 --> 00:20:36,440
happen, your life becomes better, happier, it will become more content, more at peace with

307
00:20:36,440 --> 00:20:41,600
yourself and you have great power.

308
00:20:41,600 --> 00:20:47,440
It gets to the point where Buddhists sometimes use it to gain power and influence, where

309
00:20:47,440 --> 00:20:53,040
they'll do good deeds just because of how it looks to others, there are people who do this

310
00:20:53,040 --> 00:21:00,160
because it looks good and there's, I think, a grandwa, donations, and somehow bring in

311
00:21:00,160 --> 00:21:06,080
it does bring them some power in a worldly sense, it's highly unsustainable, it's not

312
00:21:06,080 --> 00:21:10,600
the thing that's going to bring them to heaven for sure, leave them closer to Nubana,

313
00:21:10,600 --> 00:21:17,920
but they use this and it's like a currency, currency of gifts, it's really kind of weird.

314
00:21:17,920 --> 00:21:24,800
All that birdies do that too, it seems like, but it's, it gets to strange proportions

315
00:21:24,800 --> 00:21:33,040
in Buddhism, because it's hard to let yourself let it go and rely upon the power of

316
00:21:33,040 --> 00:21:41,160
goodness, let go of this, this need for to get something back, you know, so you think,

317
00:21:41,160 --> 00:21:47,280
what do I get out of it, and you can't just let go and realize that the goodness will

318
00:21:47,280 --> 00:22:01,960
take care of itself, goodness is good and just for it's done, okay, uh oh, I don't think

319
00:22:01,960 --> 00:22:04,800
we have 50% green Robin.

320
00:22:04,800 --> 00:22:11,320
No, either that or the people with orange names have a specially long name, so it proportionally

321
00:22:11,320 --> 00:22:18,160
looks more, like there's someone named the emperor of ice cream, so maybe it's just longer

322
00:22:18,160 --> 00:22:23,640
names that are orange, but I think you're right, I think everyone should do 10 minutes

323
00:22:23,640 --> 00:22:29,160
of meditation right now, let's say that, we're going to take a 10 minute break for everyone

324
00:22:29,160 --> 00:22:33,520
to do 10 minutes of meditation, okay, all right, and then we'll come back and answer

325
00:22:33,520 --> 00:22:38,320
some question, okay, 10 minutes everyone, and that means you have to actually click, you

326
00:22:38,320 --> 00:22:44,120
know, you who are logged in, you have to scroll down and either do five minutes walking,

327
00:22:44,120 --> 00:22:49,120
five minutes sitting or 10 minutes walking or 10 minutes sitting, you can do more if you

328
00:22:49,120 --> 00:22:54,640
want, but we're going to take a 10 minute break to allow those people who are orange

329
00:22:54,640 --> 00:23:17,640
to get their green on, okay, I have to turn something on right back, okay,

330
00:23:17,640 --> 00:23:25,700
all right.

331
00:23:25,700 --> 00:23:29,700
.

332
00:23:29,700 --> 00:23:33,700
..

333
00:23:33,700 --> 00:23:37,700
..

334
00:23:37,700 --> 00:23:41,700
..

335
00:23:41,700 --> 00:23:45,700
...

336
00:23:45,700 --> 00:23:49,700
...

337
00:23:49,700 --> 00:23:53,700
..

338
00:23:53,700 --> 00:23:57,700
..

339
00:23:57,700 --> 00:24:01,700
..

340
00:24:01,700 --> 00:24:05,700
..

341
00:24:05,700 --> 00:24:09,700
..

342
00:24:09,700 --> 00:24:13,700
..

343
00:24:13,700 --> 00:24:17,700
..

344
00:24:17,700 --> 00:24:21,700
..

345
00:24:21,700 --> 00:24:25,700
..

346
00:24:25,700 --> 00:24:29,700
..

347
00:24:29,700 --> 00:24:31,700
..

348
00:24:31,700 --> 00:24:33,700
..

349
00:24:33,700 --> 00:24:37,700
..

350
00:24:37,700 --> 00:24:41,700
..

351
00:24:41,700 --> 00:24:45,700
..

352
00:24:45,700 --> 00:24:49,700
..

353
00:24:49,700 --> 00:24:53,700
..

354
00:24:53,700 --> 00:24:57,700
..

355
00:24:57,700 --> 00:24:59,700
..

356
00:24:59,700 --> 00:25:01,700
..

357
00:25:01,700 --> 00:25:05,700
..

358
00:25:05,700 --> 00:25:09,700
..

359
00:25:09,700 --> 00:25:13,700
..

360
00:25:13,700 --> 00:25:17,700
..

361
00:25:17,700 --> 00:25:21,700
..

362
00:25:21,700 --> 00:25:25,700
..

363
00:25:25,700 --> 00:25:29,700
..

364
00:25:29,700 --> 00:25:33,700
..

365
00:25:33,700 --> 00:25:37,700
..

366
00:25:37,700 --> 00:25:41,700
..

367
00:25:41,700 --> 00:25:45,700
..

368
00:25:45,700 --> 00:25:49,700
..

369
00:25:49,700 --> 00:25:53,700
..

370
00:25:53,700 --> 00:25:57,700
..

371
00:25:57,700 --> 00:26:01,700
..

372
00:26:01,700 --> 00:26:05,700
..

373
00:26:05,700 --> 00:26:09,700
..

374
00:26:09,700 --> 00:26:13,700
..

375
00:26:13,700 --> 00:26:17,700
..

376
00:26:17,700 --> 00:26:21,700
..

377
00:26:21,700 --> 00:26:25,700
..

378
00:26:25,700 --> 00:26:29,700
..

379
00:26:29,700 --> 00:26:37,700
..

380
00:26:37,700 --> 00:26:41,700
..

381
00:26:41,700 --> 00:26:45,700
..

382
00:26:45,700 --> 00:26:49,700
..

383
00:26:49,700 --> 00:26:51,700
..

384
00:26:51,700 --> 00:26:53,700
..

385
00:26:53,700 --> 00:26:57,700
..

386
00:26:57,700 --> 00:27:01,700
..

387
00:27:01,700 --> 00:27:05,700
..

388
00:27:05,700 --> 00:27:09,700
..

389
00:27:09,700 --> 00:27:13,700
..

390
00:27:13,700 --> 00:27:17,700
..

391
00:27:17,700 --> 00:27:21,700
..

392
00:27:21,700 --> 00:27:25,700
..

393
00:27:25,700 --> 00:27:29,700
..

394
00:27:29,700 --> 00:27:33,700
..

395
00:27:33,700 --> 00:27:37,700
..

396
00:27:37,700 --> 00:27:41,700
..

397
00:27:41,700 --> 00:27:45,700
..

398
00:27:45,700 --> 00:27:47,700
..

399
00:27:47,700 --> 00:27:49,700
..

400
00:27:49,700 --> 00:27:53,700
..

401
00:27:53,700 --> 00:27:57,700
..

402
00:27:57,700 --> 00:28:01,700
..

403
00:28:01,700 --> 00:28:05,700
..

404
00:28:05,700 --> 00:28:09,700
..

405
00:28:09,700 --> 00:28:13,700
..

406
00:28:13,700 --> 00:28:17,700
..

407
00:28:17,700 --> 00:28:21,700
..

408
00:28:21,700 --> 00:28:25,700
..

409
00:28:25,700 --> 00:28:29,700
..

410
00:28:29,700 --> 00:28:33,700
..

411
00:28:33,700 --> 00:28:37,700
..

412
00:28:37,700 --> 00:28:41,700
..

413
00:28:41,700 --> 00:28:45,700
..

414
00:28:45,700 --> 00:28:49,700
..

415
00:28:49,700 --> 00:28:53,700
..

416
00:28:53,700 --> 00:28:57,700
..

417
00:28:57,700 --> 00:29:01,700
..

418
00:29:01,700 --> 00:29:05,700
..

419
00:29:05,700 --> 00:29:09,700
..

420
00:29:09,700 --> 00:29:13,700
..

421
00:29:13,700 --> 00:29:17,700
..

422
00:29:17,700 --> 00:29:21,700
..

423
00:29:21,700 --> 00:29:25,700
..

424
00:29:25,700 --> 00:29:29,700
..

425
00:29:29,700 --> 00:29:33,700
..

426
00:29:33,700 --> 00:29:37,700
..

427
00:29:37,700 --> 00:29:41,700
..

428
00:29:41,700 --> 00:29:45,700
..

429
00:29:45,700 --> 00:29:49,700
..

430
00:29:49,700 --> 00:29:53,700
..

431
00:29:53,700 --> 00:29:57,700
..

432
00:29:57,700 --> 00:30:01,700
..

433
00:30:01,700 --> 00:30:05,700
..

434
00:30:05,700 --> 00:30:09,700
..

435
00:30:09,700 --> 00:30:13,700
..

436
00:30:13,700 --> 00:30:17,700
..

437
00:30:17,700 --> 00:30:21,700
..

438
00:30:21,700 --> 00:30:25,700
..

439
00:30:25,700 --> 00:30:29,700
..

440
00:30:29,700 --> 00:30:33,700
..

441
00:30:33,700 --> 00:30:37,700
..

442
00:30:37,700 --> 00:30:41,700
..

443
00:30:41,700 --> 00:30:45,700
..

444
00:30:45,700 --> 00:30:49,700
..

445
00:30:49,700 --> 00:30:53,700
..

446
00:30:53,700 --> 00:30:57,700
..

447
00:30:57,700 --> 00:31:01,700
..

448
00:31:01,700 --> 00:31:05,700
..

449
00:31:05,700 --> 00:31:09,700
..

450
00:31:09,700 --> 00:31:13,700
..

451
00:31:13,700 --> 00:31:17,700
..

452
00:31:17,700 --> 00:31:21,700
..

453
00:31:21,700 --> 00:31:25,700
..

454
00:31:25,700 --> 00:31:29,700
..

455
00:31:29,700 --> 00:31:33,700
..

456
00:31:33,700 --> 00:31:37,700
..

457
00:31:37,700 --> 00:31:41,700
..

458
00:31:41,700 --> 00:31:45,700
..

459
00:31:45,700 --> 00:31:49,700
..

460
00:31:49,700 --> 00:31:53,700
..

461
00:31:53,700 --> 00:31:57,700
..

462
00:31:57,700 --> 00:32:01,700
..

463
00:32:01,700 --> 00:32:05,700
..

464
00:32:05,700 --> 00:32:09,700
..

465
00:32:09,700 --> 00:32:13,700
..

466
00:32:13,700 --> 00:32:17,700
..

467
00:32:17,700 --> 00:32:21,700
..

468
00:32:21,700 --> 00:32:25,700
..

469
00:32:25,700 --> 00:32:29,700
..

470
00:32:29,700 --> 00:32:33,700
..

471
00:32:33,700 --> 00:32:41,700
..

472
00:32:41,700 --> 00:32:45,700
..

473
00:32:45,700 --> 00:32:49,700
..

474
00:32:49,700 --> 00:32:53,700
..

475
00:32:53,700 --> 00:32:55,700
..

476
00:32:55,700 --> 00:32:57,700
..

477
00:32:57,700 --> 00:33:01,700
..

478
00:33:01,700 --> 00:33:05,700
..

479
00:33:05,700 --> 00:33:09,700
..

480
00:33:09,700 --> 00:33:13,700
..

481
00:33:13,700 --> 00:33:17,700
..

482
00:33:17,700 --> 00:33:21,700
..

483
00:33:21,700 --> 00:33:25,700
..

484
00:33:25,700 --> 00:33:29,700
..

485
00:33:29,700 --> 00:33:33,700
..

486
00:33:33,700 --> 00:33:37,700
..

487
00:33:37,700 --> 00:33:41,700
..

488
00:33:41,700 --> 00:33:45,700
..

489
00:33:45,700 --> 00:33:47,700
..

490
00:33:47,700 --> 00:33:49,700
..

491
00:33:49,700 --> 00:33:51,700
...

492
00:33:51,700 --> 00:33:53,700
..

493
00:33:53,700 --> 00:33:55,700
..

494
00:33:55,700 --> 00:33:59,700
..

495
00:33:59,700 --> 00:34:01,700
...

496
00:34:01,700 --> 00:34:03,700
..

497
00:34:03,700 --> 00:34:05,700
..

498
00:34:05,700 --> 00:34:07,700
..

499
00:34:07,700 --> 00:34:09,700
..

500
00:34:09,700 --> 00:34:13,700
..

501
00:34:13,700 --> 00:34:15,700
..

502
00:34:15,700 --> 00:34:17,700
..

503
00:34:17,700 --> 00:34:19,700
..

504
00:34:19,700 --> 00:34:28,180
...

505
00:34:28,180 --> 00:34:28,700
..

506
00:34:28,700 --> 00:34:29,700
..

507
00:34:29,700 --> 00:34:37,700
..

508
00:34:37,700 --> 00:34:44,020
All right, so questions.

509
00:34:44,020 --> 00:34:51,460
Venerable Yutadamo, when I meditate, somewhere halfway I get the urge to do something, move

510
00:34:51,460 --> 00:34:56,460
my hands to somewhere, shift my posture, anything just to move.

511
00:34:56,460 --> 00:34:59,340
Where does this almost irresistible urge arise from?

512
00:34:59,340 --> 00:35:00,340
Thank you.

513
00:35:00,340 --> 00:35:10,980
So, restlessness, I think we'd call that Yudadcha, and it's associated with energy.

514
00:35:10,980 --> 00:35:22,220
People who are active, who have the habit of activity or sports, physical exertion,

515
00:35:22,220 --> 00:35:28,660
all of these things, people who are into that, it cultivates this inability to sit

516
00:35:28,660 --> 00:35:31,980
still.

517
00:35:31,980 --> 00:35:41,600
One said, the woman who had this so bad that she just got up, I think it was also her own

518
00:35:41,600 --> 00:35:50,780
meditation condition, it was probably had to do with concentration in her state of mind,

519
00:35:50,780 --> 00:35:54,540
but she just had to get up and run around, like, walk very, very quickly, not run with

520
00:35:54,540 --> 00:35:59,780
walk very, very quickly around the monastery.

521
00:35:59,780 --> 00:36:03,420
We've had sport, people who have practiced sports in the bad, played sports, from big

522
00:36:03,420 --> 00:36:11,700
into sports, they have a hard time sitting still.

523
00:36:11,700 --> 00:36:19,820
In addition to mindfulness, what other factors help in realizing reality?

524
00:36:19,820 --> 00:36:30,140
Well, there's three factors, Adapi in San Pagano and Satima, Adapi is the effort.

525
00:36:30,140 --> 00:36:37,060
San Paganya is the clarity of mind or the awareness of the object, and Sati is reminding

526
00:36:37,060 --> 00:36:41,500
yourself, this is this, that is that.

527
00:36:41,500 --> 00:37:01,700
Being and seeing hearing is hearing, when you say seeing, seeing, we're hearing.

528
00:37:01,700 --> 00:37:06,700
Bante would be appropriate for you to volunteer perhaps on behalf of the Buddhism Society

529
00:37:06,700 --> 00:37:13,060
to give a seminar on meditation to the university faculty.

530
00:37:13,060 --> 00:37:18,060
I feel like that's something academics would dig and it could help spread the word.

531
00:37:18,060 --> 00:37:21,420
I'm not really into volunteering to teach.

532
00:37:21,420 --> 00:37:25,460
If they want me to teach, they'll invite me, no?

533
00:37:25,460 --> 00:37:32,060
I've got a really cool article from Brown University in Rhode Island.

534
00:37:32,060 --> 00:37:38,060
They have meditation labs and they work meditation into the curriculum for, I think it

535
00:37:38,060 --> 00:37:42,980
was for students that are in the medical pre-med and so forth.

536
00:37:42,980 --> 00:37:46,980
I should dig that article out, it was really interesting, but they have the meditation

537
00:37:46,980 --> 00:37:49,900
labs in between classes and all the students can go.

538
00:37:49,900 --> 00:38:05,820
So it's really just part of their day there.

539
00:38:05,820 --> 00:38:12,340
Realization is occurred naturally, we just have to mindfully observe phenomena that

540
00:38:12,340 --> 00:38:24,740
occurs at the moment, that's it.

541
00:38:24,740 --> 00:38:30,020
Bante, what is your opinion on having extra senses, being extra sensory, seeing aura,

542
00:38:30,020 --> 00:38:44,900
extra, is this healthy wisdom or dangerous emotion?

543
00:38:44,900 --> 00:38:51,020
I don't think it's either, it's not wisdom, it's just, it's Abinya, it's in a sense

544
00:38:51,020 --> 00:39:03,980
it's higher knowledge, but it's not wisdom, it's based on concentration.

545
00:39:03,980 --> 00:39:14,700
Yeah, emperor of ice cream, could you stay on after the session and we'll try to figure

546
00:39:14,700 --> 00:39:24,820
out what's going wrong, no, you don't need to pick it, you don't need to pick a sound,

547
00:39:24,820 --> 00:39:29,460
that shouldn't make a difference.

548
00:39:29,460 --> 00:39:32,820
We can figure out what's wrong, it may be your username, it may for some reason not

549
00:39:32,820 --> 00:39:43,980
like your username, too long or something, I don't know, not sure what's wrong.

550
00:39:43,980 --> 00:39:58,980
We can figure it out, I have no more questions, okay, no more questions, that's because

551
00:39:58,980 --> 00:40:06,460
everyone did meditation, you see, meditation answers your question, it's true, more meditation

552
00:40:06,460 --> 00:40:35,100
than the list questions, okay, so let's quit, I'm gonna figure out this, figure this

553
00:40:35,100 --> 00:40:46,860
out with this person who has named themselves emperor of ice cream, thank you Bantay,

554
00:40:46,860 --> 00:41:12,940
thank you Robin, good night, good night, thank you everyone for tuning in, good night.

