1
00:00:00,000 --> 00:00:07,000
Right, and they would know what makes the Buddha special.

2
00:00:07,000 --> 00:00:11,000
It's special about the Buddha.

3
00:00:11,000 --> 00:00:14,000
There are three things that are special about the Buddha.

4
00:00:14,000 --> 00:00:16,000
Three incense represents the Buddha.

5
00:00:16,000 --> 00:00:18,000
I always remember them.

6
00:00:18,000 --> 00:00:21,000
But doesn't represent the Buddha number seven.

7
00:00:21,000 --> 00:00:23,000
It represents the Buddha.

8
00:00:23,000 --> 00:00:25,000
And it depends on you as this is the tradition.

9
00:00:25,000 --> 00:00:27,000
Maybe there are other questions.

10
00:00:27,000 --> 00:00:30,000
I think this is the best one we should have.

11
00:00:30,000 --> 00:00:32,000
The Buddha had three special qualities.

12
00:00:32,000 --> 00:00:36,000
The first one was he was too hurt.

13
00:00:36,000 --> 00:00:39,000
And the Buddha had no defile hope.

14
00:00:39,000 --> 00:00:41,000
He was actually the wrong one.

15
00:00:41,000 --> 00:00:43,000
He would have had no defile hope.

16
00:00:43,000 --> 00:00:45,000
So what's special about his life?

17
00:00:45,000 --> 00:00:47,000
Is that the Buddha had it?

18
00:00:47,000 --> 00:00:48,000
Is that when you become enlightened?

19
00:00:48,000 --> 00:00:51,000
You have no greed, no hate.

20
00:00:51,000 --> 00:00:55,000
You have no thoughts in your life.

21
00:00:55,000 --> 00:00:57,000
You don't want to hurt other people.

22
00:00:57,000 --> 00:00:59,000
You don't want anything.

23
00:00:59,000 --> 00:01:01,000
You become like the bird.

24
00:01:01,000 --> 00:01:04,000
This is the first special thing about the Buddha.

25
00:01:04,000 --> 00:01:10,000
The second thing is the wisdom.

26
00:01:10,000 --> 00:01:15,000
The Buddha's wisdom is something that you can't find any limit.

27
00:01:15,000 --> 00:01:20,000
Would I have so much wisdom you can understand anything?

28
00:01:20,000 --> 00:01:23,000
If you remember all of his past lives,

29
00:01:23,000 --> 00:01:26,000
then he has life that he wanted.

30
00:01:26,000 --> 00:01:28,000
And he understood everything.

31
00:01:28,000 --> 00:01:30,000
He would exactly have the future.

32
00:01:30,000 --> 00:01:32,000
He would have the special power.

33
00:01:32,000 --> 00:01:35,000
He knew what practices he could look for.

34
00:01:35,000 --> 00:01:37,000
He practiced his wisdom.

35
00:01:37,000 --> 00:01:41,000
He knew the ways to teach different people.

36
00:01:41,000 --> 00:01:45,000
He knew what could be and what could not be.

37
00:01:45,000 --> 00:01:47,000
He could always tell you this is possible.

38
00:01:47,000 --> 00:01:49,000
This is not possible.

39
00:01:49,000 --> 00:01:53,000
So I had a many special business.

40
00:01:53,000 --> 00:01:55,000
The wisdom of the Buddha.

41
00:01:55,000 --> 00:02:00,000
I'm so happy that we have a whole shelf full of his teaching.

42
00:02:00,000 --> 00:02:06,000
It's 45 years for the greatest wisdom you can find.

43
00:02:06,000 --> 00:02:09,000
This is number three.

44
00:02:09,000 --> 00:02:16,000
The third one is the compassion.

45
00:02:16,000 --> 00:02:21,000
Because for all of us we can practice and we don't have to teach other people.

46
00:02:21,000 --> 00:02:30,000
The third one is the compassion.

47
00:02:30,000 --> 00:02:36,000
Because for all of us we can practice and we don't have to teach other people.

48
00:02:36,000 --> 00:02:41,000
You can come and practice with me and then you go home.

49
00:02:41,000 --> 00:02:42,000
I can go home.

50
00:02:42,000 --> 00:02:45,000
I can run up in the forest.

51
00:02:45,000 --> 00:02:49,000
Because the Buddha had a special quality that we think about.

52
00:02:49,000 --> 00:02:52,000
The Buddha could have done the same thing.

53
00:02:52,000 --> 00:02:59,000
When he became a light in that Buddha gang, he didn't have to walk 120 miles.

54
00:02:59,000 --> 00:03:02,000
He could have just stayed there and been happy.

55
00:03:02,000 --> 00:03:09,000
But he walked all that way just to find someone who would understand what he thought he was.

56
00:03:09,000 --> 00:03:13,000
And then on 45 years, there was not a tool.

57
00:03:13,000 --> 00:03:17,000
Then it was always his teaching.

58
00:03:17,000 --> 00:03:19,000
The service.

59
00:03:19,000 --> 00:03:23,000
And this is the greatest.

60
00:03:23,000 --> 00:03:33,000
Not only that he had to be talked about how long he had to spend, but just to become a Buddha.

61
00:03:33,000 --> 00:03:38,000
So this is something very special.

62
00:03:38,000 --> 00:03:42,000
The second is the middle of the candle.

63
00:03:42,000 --> 00:03:56,000
We already told you about this.

64
00:03:56,000 --> 00:03:59,000
The middle of the candle represents the dhamma and the mean they have.

65
00:03:59,000 --> 00:04:02,000
So it represents the dhamma and the teaching.

66
00:04:02,000 --> 00:04:05,000
The teaching as a Buddha separated into two parts.

67
00:04:05,000 --> 00:04:08,000
One is the things that the Buddha said he should do.

68
00:04:08,000 --> 00:04:11,000
And one is the things that the Buddha should do.

69
00:04:11,000 --> 00:04:21,000
The things that you should do are to kill, to steal, to lie, to cheat, to dig for a dhamma home.

70
00:04:21,000 --> 00:04:30,000
The things that you should do are develop morality, concentration, which is the quality of the past.

71
00:04:30,000 --> 00:04:34,000
But these are the two parts.

72
00:04:34,000 --> 00:04:43,000
So what is left there?

