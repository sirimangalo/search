Hello, and welcome back to our study of the Dampanda.
Today we begin the Balawaga, the chapter on fools.
So all the vous vous vous vous vous in here, in this chapter we'll have something to do
with fools. We're foolishness. It's verse 61 today, which reads as follows.
Now, we're going to see the Dampanda.
We're going to see the Dampanda.
We're going to see the Dampanda.
Wondering or faring through some sorrow, one doesn't find someone who is one's equal or better,
or at least equal to one's self.
We're going to see the Dampanda.
We're going to see the Dampanda.
We're going to see the Dampanda.
The story behind this verse goes that when the Buddha was dwelling in Sawati,
Mahakasapa was staying in his cave above Rajagahat,
which you can still see today if you go there.
Apparently they have a place that they pinpointed as his cave.
Anyway, you can still go into Rajagahat.
Up on the mountain above Rajagahat, he would go every day down the mountain into the city for arms.
At the time, he had two students, two novices, staying with him.
They would attend upon him.
One of them was a good student, and one of them was a rather recalcitrant student.
He was lazy in performing his duties and in practicing the Dhamma.
And so how it would go is the one student who was quite diligent and faithful
and obedient and so on.
He would prepare the elder's water in the morning and his tooth sticks,
and he would wait upon his elder and perform all these duties.
He would heat up water for the elder's bath and leave the water out in the morning.
And the other disciple would watch and would come or would sleep in
and then would come out when everything was done.
He would see if everything was done and go to tell the elder,
but in both the tooth sticks have been put out and water has been put out for washing
and for bathing, please come and it's time to do your morning routine.
And so the elder, as a result, got to thinking that this student
who kept coming to tell him and invite him to use the things that had been set out for him
and had performed duties himself.
And the other student who had actually done all the work sat back and watched
and after some time realized what this, the other student was doing
that he was taking credit for the work that he hadn't done.
He said, well, I know what to do then.
And so he, in the morning, when it was time for the elder's bath,
he went to the room where the water,
and he put just a little bit of water in the bottom of the big pot,
the big cauldron for boiling bath water.
And he did that.
And so we could see the steam coming up, but there was very little water in it.
And then the other novice, when he came over and saw the steam coming out,
he said, oh, I must have put out the water.
And he went to tell the elder,
Mr. Bocher, your bath water is ready.
Come and take your bath.
And the elder went into the bathroom and said, okay, bring out the water.
And the novice went into the boiling room
and looked in the cauldron and realized that there was just a very little bit of water
and got very angry and shouted.
And he said, that's scoundrel.
He only put a very little bit of water in.
And so he rushed off to the river to get more water.
Where the river was, rushed off to get water from wherever they get the water.
Maybe he must have had to gone down into Rajika had to go.
And the elder watched him go.
And when he came back, the elder said,
what did you mean by that?
Are you saying that you didn't do any of this?
And you've been taking credit for this all the time.
And he said, that's not, and the elder scolded him.
And so that's not how a monk should behave.
One should not take credit for something that one hasn't done.
And the novice got very angry and went off enough.
And began to develop a grudge against the elder.
The next day, or on subsequent days,
they went off.
When they went off for alms,
I guess the next day, when they went for alms,
he has a result.
He decided not to go on alms with the elder.
And so when the elder and the other novice went off for alms,
they went alone.
And the disobedient novice, he stayed back
and was just sulked at the kuti at the cave.
And the elder went with his other novice.
And then when they were gone,
he went off to one of the elder's supporters' houses.
He went down into Rajika had his own
and went to one of the elder supporters
and said to them that the elder was sick.
He said, oh, where's the elder?
Why are you coming along?
He said, oh, he's sick.
But he needs some special food.
And please give him very delicate
and good tasting food and so on,
so he can recover from his sickness.
And so they gave him all this special food
and prepared all this special food for him.
And he took it back and aided himself.
And the elder went on alms around with the other novice
and happened to get a full set of robes,
new nice robes from one family.
And so he gave them to the novice
that went with him.
And the novice changed out of his own set
and put on this new set of robes.
And they went back to the monastery.
On yet another day,
the elder went to see, happened to go to see this supporter
who had been tricked.
And they said to the elder, oh, are you venerable,
sir, how are you doing?
We heard that you were sick.
But we gave food to the novice
when he came, hope the food helped you to get better.
And the elder just stood there,
didn't say anything.
We received food from him and went off.
And they went back to the monastery
and he confronted this novice.
And he said to him, is it true that you've been going around?
Is it true that you ended these people and told them
that I was sick and convinced them
to give you all this special food?
And he said, what do you mean?
It's just, and he scolded him.
And my husband scolded him and said,
that's not something that a monk should do.
And the novice was, he was totally fed up at this point.
He said, over a little bit of water,
the elder gets upset.
And then he gets upset about a little bit of food.
And on top of that, he's been giving,
he gives a robe a full set of robes
to the other novice.
So this novice got very, very angry
and then he said, I know what I'm going to do.
And the next morning when the elder went off on Armstrong,
he went around the cave and broke up all the pots
and everything, just tore up everything
and ripped up everything, all the bedding and everything.
And then set the whole place on fire.
And then he ran off.
And I think ended up in hell as a result.
But the elder came back and saw this
and fixed it all up as best he could.
And of course, went on with his life.
So this is a story of these two novices.
One who was a good companion.
And there's a bad companion.
And eventually, the story got back to the Buddha,
one of the monks from Rajagat wandered all the way to Sawati
and Buddha asked him, how is Mahakasupa doing?
And he said, oh, he's doing fine.
He happened to have these two students.
And one of them was very, very good.
And followed the elder's instruction and advice
and practiced the elder's teachings.
But the other one was an evil scoundrel.
And not only did he not practice,
but he also ended up destroying the elder's residence.
And the Buddha said, oh, yes, yes.
The elder is better off without this novice.
And he told a story of the past.
This is the introduction to one of the Jatakas stories,
where he was a monkey.
And he tore up this bird's nest.
I'm not going to tell this story.
It's not much to it.
But he said, basically, this novice, this is the way he was.
And he said, better off without him.
He said, there's no association with fools.
If the only people you can hang out with
and rely upon are fools while you're better off without them.
And then he told this verse.
So a simple story.
A story that I'm sure we can all relate to.
That's all living in caves,
having novices attend upon us.
No, all of us having good friends and bad friends,
people who we can rely upon
and people who we shouldn't rely upon,
people who help us up and people who drag us down.
And so as meditators,
this is one of the most important aspects
of our practice is the association with good people.
I'm going to send it's all of the holy life,
all of the spiritual life.
The spiritual life is all about associating with the right people,
having a good friend, having someone to guide you
and teach you and raise you up.
In many places, he said that friends are good friends
and most valuable and do not associate with fools.
Of course, this is the famous, the mongolisture,
the first thing that the Buddha says is not associating with fools.
This is the greatest blessing.
So how does this go?
The verse is broken down quite well by the commentary.
So I'll go through that first and then talk a little bit about
how it relates to meditation, particularly.
First of all, the word jarang, jarang,
which means to fare, to wander, to travel,
to move about, to live your life, basically.
The commentary says it doesn't refer to moving about with your body.
It refers to the movements of the mind or the way of the mind.
So there's nothing to do with being close to someone.
It's not like if you shouldn't sit on the subway next to someone
unless you're sure they're a wise person.
It also doesn't necessarily mean that you can't work or live
or go habitat with such people.
So it says manasach, which means the fairing of the mind.
Don't let them into your heart, basically.
Don't take their counsel or take them up as your intimate associates.
And then what is meant by Sayyang, Sadhisahmata,
and what is meant by one who is greater than you,
or one who is equal to you.
The commentary says it's specifically in regards to
Sila Samadipanya, morality, concentration, and wisdom.
So if someone has greater morality than you,
if there's someone who is practicing higher morality,
for instance, monks who are practicing celibacy and poverty
and who are taking themselves out of all of the distracting activities
that lay people who engage in,
then it's something that can help us to cultivate concentration ourselves.
If we associate with such people,
if we are around people who are around people who are equal to us,
then it will not, our morality will not fade away.
If we have, for around people who are greater concentration,
then their concentration will be a support for us.
It will be an example for us.
It will be something that we can rely upon or we can emulate
to cultivate concentration ourselves.
And if we hang around people who have wisdom,
greater wisdom than us, then it will be something that inspires us
because the reason for hanging out with someone
who associate with people who are greater than you
because you will develop what the good qualities
of morality, concentration, wisdom will increase
if we are around people who are a greater morality, concentration,
and wisdom than us.
And if we are around people who are equal,
an equal level to us,
then we can be assured that we at least won't follow us.
At least won't follow away from our morality, concentration,
wisdom, and as a result through our own practice,
we'll be able to develop.
The point is that the people around us won't drag us down.
If you're around people who are inferior to you,
and if you rely upon them, and if you emulate them
and cultivate their qualities, then you will follow away from your own morality
and concentration, because it comes under attack,
if you let them into clothes.
Now, if you can't find someone who is greater,
someone who is equal to the Buddha said,
actually, it's preferable to hang out on your own.
Now, the key word here is Dalhang,
because Dalhang means steadfast to resolute
and the point is that you actually need resolution
if you're on your own.
Many people are always contacting us constantly,
even get these emails from people saying that they're all alone
and they need some guidance and the moaning,
the fact that they're not able to find a sangha community.
And the Buddhists give reassurance,
and we should reassure such people that you can practice on your own.
You don't really need so much guidance.
It just takes more resolution,
because if you're not surrounded by examples
or people who are reminding you to
remind you of what you're doing,
reminding you of the cultivation of greater morality, concentration,
wisdom, it's easier for yourself to fall away.
It's like a plant or a young tree that requires a cage
or some kind of support to keep it from falling over
to the people from breaking in the wind and so on.
That support is helpful,
unless the tree itself is able to maintain its own strength
and grow on its own.
So the benefit of having a good companion is that they support you
through your growth.
Not Tim Bali, Sahayata.
Why is it that we should stay on our own
if we can't find such a person?
Because the association with fools.
There is no association with fools.
The meaning being that none of the good things that come
from associating with good people, morality, concentration,
wisdom, all of the good katawat through the ten types
of profitable speech.
All of the dutanga practices, all of the
we passed on the inside.
And even the attainment of Nibana is difficult.
It becomes difficult to become threatened by fools.
Maybe surrounded by people who are distracting you
or are talking about useless things and who are indulging
in harmful pursuits, killing and stealing and lying
and cheating and so on.
All of these things will drag you down.
We'll make it more difficult for you to practice
and we'll even cause you to follow away from the practice.
So this is quite a simple meaning,
but specifically in regards to the meditation practice.
The point is that people will distract us.
People will be a cause for us to study your mind.
The question that always comes up is whether or not
this means that we should ignore people who are in trouble
morally or have poor concentration.
People who are on the wrong path shouldn't we help such people.
And of course this is the exception.
The Buddha said Anyatra Anundaya Anyatra Anukampa,
which means that you shouldn't hang out
Aetang or whatever.
Aetang Purisa.
Aetang Purisa.
Aetang Purisa.
So, Purisa, the Savitable.
Such a person should not be,
should not be associated with.
Except in the case of pity or compassion.
So, the commentary explains,
which is exactly how it should be explained,
that if you have compassion,
if out of compassion for someone,
it's possible or not,
we should have the compassion.
So if you think that a person is capable
and you think you're able to help them,
then you should hang out with them
and you should try to support their practice
and you should help them to cultivate.
If you're not able to,
if you try and you fail
and it seems like this person is not going to,
I'm not going to develop themselves,
then of course,
then you should stay away from them actually
and you should go your own way
because they will drag you down.
So, in meditation,
this is especially important
and this is why,
because meditation is the direct cultivation
of wholesome mental states.
So, if you're constantly,
if you're distracted
or led astray by other people,
then your mind becomes impossible
to cultivate wholesome states of mind.
This is why the Buddha often talks about staying alone,
even away from good friends,
because even wholesome talk,
if engaged in over much,
can lead to distraction as well.
Because meditation is the cultivation of the mind
in our environment
and our engagement with the environment
and with the world around us
is at most important
so we have to be careful how we engage with the world around us.
So, for a meditator,
even good people,
we should only engage in moderation
and much less much more so.
We should be careful when we associate
or they interact with foolish people.
They will distract us,
they take away our focus of mind,
they take away our clarity of mind
and they stop us from seeing things as they are.
We're not able to stay in the present
because such people are constantly in the past
or in the future.
We're not able to stay in reality
because such people are always talking about concepts
and we're not able to stay with what is important
because such people are always engaged
and concerned with and intend upon
what is unimportant, what is unessential.
So,
unless we can help such people
and unless we can help such people easily
without interrupting our own practice,
we should be very careful to stay on our own.
So, for many people,
this is, I think, an important reminder.
If you can't find someone who can lead you,
who can help you,
can support your practice,
stay on your own,
you're better off on your own
because just having friends isn't
just the fact that you have friends alone
isn't necessarily useful
or a good thing.
Something that leads to your happiness.
You need good friends.
You need friends who lead you to good things.
Friends who help you support you
who encourage you
in your own spiritual development.
So, simple teaching,
but one that we should all keep in mind
and a very important aspect of our life
is who we associate with.
So, that's the teaching for tonight.
Thank you all for tuning in
and wish you all peace, happiness,
and freedom from suffering.
Thank you.
