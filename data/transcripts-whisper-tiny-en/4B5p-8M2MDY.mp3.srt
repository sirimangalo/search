1
00:00:00,000 --> 00:00:07,120
Okay, good evening, so we're looking at the Dhamapanda again tonight, continuing with

2
00:00:07,120 --> 00:00:12,680
first 193, which reads as follows.

3
00:00:12,680 --> 00:00:26,000
Vul-la-bho-pri-sa-janyu-nasu-sa-bata-jai-ati-yata-so-jai-ati-di-ri-ra-tankulang-so-kame-dati,

4
00:00:26,000 --> 00:00:41,640
which means a thoroughbred man is hard to find, hard to get, hard to find.

5
00:00:41,640 --> 00:00:46,200
They are not born everywhere.

6
00:00:46,200 --> 00:00:58,160
Had-dir-o-yata-so-jai-ati-di-ri-ra-o, in a place where such a person, such a wise person, is born,

7
00:00:58,160 --> 00:01:10,280
that family finds great happiness, so there's not much of a story.

8
00:01:10,280 --> 00:01:14,680
This is another one of Anandas questions.

9
00:01:14,680 --> 00:01:20,200
Anandas didn't become enlightened until after the Buddha passed away, so you sometimes wonder

10
00:01:20,200 --> 00:01:23,720
whether these questions were a part of that.

11
00:01:23,720 --> 00:01:29,520
I think that's perhaps a little unfair, but he seems to be very much interested in gaining

12
00:01:29,520 --> 00:01:35,480
all the theoretical knowledge he can while the Buddha is still around.

13
00:01:35,480 --> 00:01:40,560
I don't think, you can necessarily chastise him for it, I don't think the Buddha ever did,

14
00:01:40,560 --> 00:01:48,240
but it seemed to understand that Anandas wouldn't become enlightened, but that is his goal,

15
00:01:48,240 --> 00:01:57,160
and his intent, his path, because not all Buddhist practitioners have the same path.

16
00:01:57,160 --> 00:02:02,320
His path wouldn't be one of theoretical study, and so he had many questions just to make

17
00:02:02,320 --> 00:02:08,680
sure he understood and that he knew, not just understood, but knew, and had in his mind

18
00:02:08,680 --> 00:02:09,680
all of these teachings.

19
00:02:09,680 --> 00:02:11,600
Apparently he had a very good memory.

20
00:02:11,600 --> 00:02:16,520
There are people in the world you hear about them, they have these photographic memories

21
00:02:16,520 --> 00:02:23,280
or something, apparently Anandas had a very good memory, so he could remember anything,

22
00:02:23,280 --> 00:02:27,760
but he hadn't to hear it, so he asked the Buddha many questions, and his question here

23
00:02:27,760 --> 00:02:33,240
was, where do you find a thoroughbred human?

24
00:02:33,240 --> 00:02:39,840
He said, you know, we know about thoroughbred horses and elephants, and they're actually

25
00:02:39,840 --> 00:02:45,680
pretty easy to find, you can find them anywhere, but where can you find a thoroughbred

26
00:02:45,680 --> 00:02:47,480
human?

27
00:02:47,480 --> 00:02:57,480
So the story says the Buddha took it to mean that Anandas was talking about a Buddha.

28
00:02:57,480 --> 00:03:02,840
So there's two ways we can look at this verse, from the story perspective, we're talking

29
00:03:02,840 --> 00:03:07,600
about the Buddha, where do you find a Buddha, and it's not easy to find a Buddha, takes

30
00:03:07,600 --> 00:03:13,320
a long, long time, it's not just two or three days, then you get a Buddha, and Buddha

31
00:03:13,320 --> 00:03:21,800
is something that requires uncountable lengths of time, many multiple uncountable lengths

32
00:03:21,800 --> 00:03:29,520
of time for a person to become a Buddha. They're very rare, and they don't just arise

33
00:03:29,520 --> 00:03:35,480
up anywhere, so he talked a little bit about the conditions that are required for the

34
00:03:35,480 --> 00:03:42,960
arising of a Buddha, what sort of family they'll be born into and that sort of thing.

35
00:03:42,960 --> 00:03:49,680
But from the perspective of the verse, there's another aspect, yes, the verse says that

36
00:03:49,680 --> 00:03:54,040
it's hard to find a Buddha, but it also says how great, or how hard it is to find the

37
00:03:54,040 --> 00:03:58,880
thoroughbred person, but also talks about how great it is for their family, and just how

38
00:03:58,880 --> 00:04:05,760
great it is and how much happiness comes from a person who is thoroughbred.

39
00:04:05,760 --> 00:04:11,280
And it makes us think of the Buddha's teaching on thoroughbred humans, and it seems pretty

40
00:04:11,280 --> 00:04:15,840
clear from that teaching that the Buddha didn't reserve this teaching only for Buddhas,

41
00:04:15,840 --> 00:04:22,080
this designation, so just because someone isn't a Buddha doesn't mean they can't be thoroughbred.

42
00:04:22,080 --> 00:04:24,560
So there's two things I want to talk about here.

43
00:04:24,560 --> 00:04:34,320
The first one coming off the story perspective is a little bit of humility, because it's

44
00:04:34,320 --> 00:04:41,840
easy to think that you're special, and in Buddhas on this manifest itself as people thinking

45
00:04:41,840 --> 00:04:48,600
that they're qualified to become a Buddha, and I think I'm certainly not a person to put

46
00:04:48,600 --> 00:04:55,480
someone in their place and say, you can't become a Buddha, but I think the general consensus

47
00:04:55,480 --> 00:05:00,520
in our tradition is that a lot more people want to become Buddhas than will ever actually

48
00:05:00,520 --> 00:05:03,360
attain Buddhahood.

49
00:05:03,360 --> 00:05:13,520
So Kundanya was, I know, Kundanya was his name, Kachayana, I think.

50
00:05:13,520 --> 00:05:20,400
I think Kachayana, one of the Buddhas chief disciples, had made a vow to become a Buddha.

51
00:05:20,400 --> 00:05:23,600
It wasn't him, it was someone.

52
00:05:23,600 --> 00:05:28,600
And when he came to this life and saw the Buddha, he thought to himself, oh boy, that's

53
00:05:28,600 --> 00:05:33,880
just too pure and too perfect, I'm not capable of that, and he gave it up and became

54
00:05:33,880 --> 00:05:35,760
a monk.

55
00:05:35,760 --> 00:05:45,160
But a lot of people just have the idea to become a Buddha, and I think it can be based

56
00:05:45,160 --> 00:05:51,680
on this sort of conceit of thinking that you're special, thinking that you have some

57
00:05:51,680 --> 00:05:56,920
special quality that makes you better than everyone else.

58
00:05:56,920 --> 00:06:02,040
And so when I read this first, one of the things I think of is a reminder that I'm not

59
00:06:02,040 --> 00:06:08,600
a Buddha, I'm not this kind of a thoroughbred, I'm not anything special.

60
00:06:08,600 --> 00:06:13,720
I think that's potentially useful, of course you have to be careful not to be hard on

61
00:06:13,720 --> 00:06:22,240
yourself or think of yourself as bad or evil or corrupt or just useless or pointless.

62
00:06:22,240 --> 00:06:30,000
But on the other hand, we have to find freedom, we have to, to some extent, see how worthless

63
00:06:30,000 --> 00:06:36,520
and useless we are, our bodies and our minds.

64
00:06:36,520 --> 00:06:40,640
Because part of letting go is seeing how useless it all is.

65
00:06:40,640 --> 00:06:47,200
Part of becoming a Buddha even is to realize how useless the person who is the Buddha actually

66
00:06:47,200 --> 00:06:56,080
is, meaning in an ultimate sense, the person, the being, the identity and even the physical

67
00:06:56,080 --> 00:07:00,840
and mental manifestations are in a little themselves of no value.

68
00:07:00,840 --> 00:07:07,560
They have nothing that you should cherish or hold on to.

69
00:07:07,560 --> 00:07:18,360
So reminding ourselves that we are just ordinary and that our minds are chaotic and unwieldy.

70
00:07:18,360 --> 00:07:23,560
Reminding ourselves that we are not something special can be quite useful.

71
00:07:23,560 --> 00:07:30,560
It's useful to come down to earth and to not hold yourself up over others or to think of

72
00:07:30,560 --> 00:07:32,800
yourself as something special.

73
00:07:32,800 --> 00:07:39,400
If you want to become a Buddha, you have to let go of that.

74
00:07:39,400 --> 00:07:44,480
You want to become enlightened, you also have to let go of that.

75
00:07:44,480 --> 00:07:55,760
But on the other side, it's important to be encouraged and it's important to remember the

76
00:07:55,760 --> 00:08:02,360
greatness of the Buddha's teaching, of the practice, the greatness of mindfulness, the greatness

77
00:08:02,360 --> 00:08:09,800
of purity of mind, the greatness of greatness, the greatness of goodness.

78
00:08:09,800 --> 00:08:14,640
And so we talk about what is the difference between a thoroughbred and just an ordinary

79
00:08:14,640 --> 00:08:23,200
human being, an untrainable sort of, if you relate it to animals, you have these animals

80
00:08:23,200 --> 00:08:31,120
that are thoroughbred, like a horse or a dog, we don't have much familiarity with elephants,

81
00:08:31,120 --> 00:08:36,080
they're thoroughbred elephants, but purebred elephants, but purebred dogs are somewhat

82
00:08:36,080 --> 00:08:37,080
exceptional.

83
00:08:37,080 --> 00:08:42,360
They have an intelligence that's, I guess they say, they're bred into them, but you could

84
00:08:42,360 --> 00:08:48,680
also think of it as something to do with the human connection and how humans are born as

85
00:08:48,680 --> 00:08:50,800
animals and animals and born as humans.

86
00:08:50,800 --> 00:08:57,360
They have a strong connection to human, to being human or to just intelligence or high-minded

87
00:08:57,360 --> 00:09:06,960
us, and so they're very easy to train, and so that we make the simile, the analogy with

88
00:09:06,960 --> 00:09:11,480
human beings, comparison to human beings.

89
00:09:11,480 --> 00:09:13,640
What sort of human being is easy to train?

90
00:09:13,640 --> 00:09:21,240
What sort of human being, when they put their mind to it, can achieve greatness?

91
00:09:21,240 --> 00:09:29,800
When the Buddha talked about, in particular, in relation to the path, he talked about a horse,

92
00:09:29,800 --> 00:09:39,960
a horse that knows the way to go, a horse that is responsive.

93
00:09:39,960 --> 00:09:44,960
So there are different kinds of horse and different levels of capacity of horses.

94
00:09:44,960 --> 00:09:52,440
Some horses, a rider pulls up the goat, the stick, and they just have to show it in the

95
00:09:52,440 --> 00:10:01,440
right direction, and the horse knows the horse knows to go.

96
00:10:01,440 --> 00:10:04,360
The second type of horse, you actually have to touch them.

97
00:10:04,360 --> 00:10:06,680
That would be a very special horse, or you didn't even have to touch them, they knew

98
00:10:06,680 --> 00:10:11,240
what right away, go left, go right, go faster, and go slower.

99
00:10:11,240 --> 00:10:14,680
But the second one you have to touch them, and when they feel the touch, no, no, no,

100
00:10:14,680 --> 00:10:19,560
they have painful touch, which is the touch, and they know which way to go, to speed up,

101
00:10:19,560 --> 00:10:21,120
just slow down.

102
00:10:21,120 --> 00:10:26,240
The third type of horse you actually have to make it hurt, and it doesn't hurt the horse

103
00:10:26,240 --> 00:10:27,240
won't go.

104
00:10:27,240 --> 00:10:32,080
I don't know, it sounds kind of cruel, I'm not giving directions on how to deal with horses.

105
00:10:32,080 --> 00:10:37,360
I think probably horse riding has some bad karma associated with it, but with similes

106
00:10:37,360 --> 00:10:46,560
you can talk about these sort of things like hunters and that sort of thing, but you hit

107
00:10:46,560 --> 00:10:51,240
it till it hurts, when it hurts, then the horse knows to go.

108
00:10:51,240 --> 00:10:59,200
Fourth type of horse, you really have to get to the bone, you really have to really make

109
00:10:59,200 --> 00:11:06,800
it injured, and it's a bruised even, you bruise it if you make it really hurt, and then

110
00:11:06,800 --> 00:11:10,920
the horse won't go, go this way, go that way, and so on.

111
00:11:10,920 --> 00:11:14,640
I think I hope I'm not getting this wrong, it's been a long time, but I think that's

112
00:11:14,640 --> 00:11:20,440
the fourth one you really have to hit to the bone, I think, yeah, that's what it is.

113
00:11:20,440 --> 00:11:25,560
Of course, the fifth type of horse, you can hit them until they die, and you hit them

114
00:11:25,560 --> 00:11:32,720
until they go to breaks, or the stick breaks, or the horse falls down, and it'll never

115
00:11:32,720 --> 00:11:36,360
go the right way.

116
00:11:36,360 --> 00:11:44,320
So that rather violent list of horses, how does that relate to humans?

117
00:11:44,320 --> 00:11:47,960
How does that relate to us, to meditators?

118
00:11:47,960 --> 00:11:51,000
It's nothing like the, I'm not going to hit, I'm not going to hit anyone with a stick,

119
00:11:51,000 --> 00:11:54,480
this isn't that sort of teaching.

120
00:11:54,480 --> 00:12:00,440
No, the stick here is death and suffering.

121
00:12:00,440 --> 00:12:03,120
Some people just want to hear about it.

122
00:12:03,120 --> 00:12:09,400
If you remember the Buddha, the Bodhisattva, when he heard about, well, actually that's

123
00:12:09,400 --> 00:12:14,720
not true, but he had never heard of, the legend goes that he had never heard of all

124
00:12:14,720 --> 00:12:17,960
the age sickness and death.

125
00:12:17,960 --> 00:12:21,720
But the first type of person anyway, just when they hear about it, they actually tried

126
00:12:21,720 --> 00:12:25,320
to not let him know about it, that's why the legend goes, I don't know if it's actually

127
00:12:25,320 --> 00:12:33,080
true, but they say, but when we hear about it, hear about people dying, like an ensue

128
00:12:33,080 --> 00:12:38,800
not me or an earthquake, or you hear about murders, these mass murders now that we hear

129
00:12:38,800 --> 00:12:44,960
about people just going in and shooting people because they're different color skin or

130
00:12:44,960 --> 00:12:54,960
because they're gay or because they're different religion or whatever, and you hear about

131
00:12:54,960 --> 00:12:59,520
that, you hear about, not about the evil of the killing, but about the death and the suffering

132
00:12:59,520 --> 00:13:08,360
and you think, wow, that really is the nature of life, but that's in the realm of possibility.

133
00:13:08,360 --> 00:13:14,360
I am not at all prepared to deal with such reality, which means I am out of touch with

134
00:13:14,360 --> 00:13:24,080
reality, I am not safe, I am not where I should be, I have not attained what needs to

135
00:13:24,080 --> 00:13:25,080
be attained.

136
00:13:25,080 --> 00:13:32,080
Some people think like this and they really get a sense of urgency and work and become enlightened

137
00:13:32,080 --> 00:13:44,320
and learn the way to free themselves from the danger of old age sickness and death, I'm

138
00:13:44,320 --> 00:13:49,000
suffering, just from hearing about it, that's like the horse that you don't even have

139
00:13:49,000 --> 00:13:54,760
to hit them with, it hasn't hit them at all.

140
00:13:54,760 --> 00:14:00,120
Second type of person they have to see someone or they have to actually come in contact

141
00:14:00,120 --> 00:14:07,920
with someone who is suffering and sick and old or dying and dead, they have to actually

142
00:14:07,920 --> 00:14:15,880
see it or come in contact with it in some way, and when they do, they realize this

143
00:14:15,880 --> 00:14:21,000
could happen to me and this is the nature of reality.

144
00:14:21,000 --> 00:14:27,720
Suffering is a part of life, here I have been negligent, engaging in mindless, meaningless

145
00:14:27,720 --> 00:14:31,480
pleasures and so on.

146
00:14:31,480 --> 00:14:37,360
The third type of person it has to be someone close to them when their parents die or

147
00:14:37,360 --> 00:14:45,200
relatives or friends or loved ones, when they get old sick and died and they see, like

148
00:14:45,200 --> 00:14:51,480
I said, before it's like this, the armies of Mara have attacked your fortress and the

149
00:14:51,480 --> 00:14:56,320
walls are crumbling, it's like you see the people dying around you, it's like an army

150
00:14:56,320 --> 00:15:02,520
has Mahasthani in the Mathuna, they have been attacked by the great armies of Mara,

151
00:15:02,520 --> 00:15:08,000
again, armies of death.

152
00:15:08,000 --> 00:15:15,040
And the fourth type of person, well, they don't get it, even when people around

153
00:15:15,040 --> 00:15:21,960
them are dying, they have to themselves be old, sick or dying, have to be in great pain

154
00:15:21,960 --> 00:15:28,920
or suffering and then they realize it, then they realize something needs to be done.

155
00:15:28,920 --> 00:15:36,240
So it's quite desperate at that point, but they are still considered to be trainable because

156
00:15:36,240 --> 00:15:41,360
they realize that something needs to be done, then they go and do it, they take up the

157
00:15:41,360 --> 00:15:47,000
practice of training their mind, purring their purifying their mind, cleansing their mind

158
00:15:47,000 --> 00:15:52,080
of all the clinging and attachment that might cause them suffering and they're able to

159
00:15:52,080 --> 00:15:55,080
free themselves.

160
00:15:55,080 --> 00:15:59,120
Fifth type of human being, what's that one well, they get old sick and die and even on

161
00:15:59,120 --> 00:16:06,720
their deathbed, they're still drinking and smoking or cursing people or clinging to things,

162
00:16:06,720 --> 00:16:14,200
they'll up into their last moment, their last breath, they die in a bad way.

163
00:16:14,200 --> 00:16:16,760
Even when they're dying, they don't realize.

164
00:16:16,760 --> 00:16:22,160
And part of it is just our culture, you know, we're taught ways to be, if you had only

165
00:16:22,160 --> 00:16:23,600
one day to live, what would you do?

166
00:16:23,600 --> 00:16:30,920
Go on party, you know, do all these sky dive, go traveling, see things, see all the things

167
00:16:30,920 --> 00:16:40,080
outside of yourself, none of which will prepare you and make you better equipped to deal

168
00:16:40,080 --> 00:16:50,640
with this part of life, that is death, none of it will lead to greater things.

169
00:16:50,640 --> 00:16:56,040
So someone who does, someone who takes up the practice, so one of these reasons is considered

170
00:16:56,040 --> 00:17:01,520
to be a thoroughbred, purebred, it's a sign of greatness.

171
00:17:01,520 --> 00:17:08,920
So it's something to be proud of and encouraged by just not to let it go to your head.

172
00:17:08,920 --> 00:17:14,880
I think the other reminder that we're not Buddha, that we're just, we're actually rejects

173
00:17:14,880 --> 00:17:18,120
at this point, it's quite humbling.

174
00:17:18,120 --> 00:17:23,160
If we had been great people, we would have probably become enlightened when the Buddha

175
00:17:23,160 --> 00:17:30,000
was here, but we were around back then, probably humans, maybe animals, who knows we could

176
00:17:30,000 --> 00:17:36,000
have been anywhere, but we missed it, we missed our opportunity and here we are, we're

177
00:17:36,000 --> 00:17:49,400
like the beggars outside of the palace walls taking scraps, 2500 years later.

178
00:17:49,400 --> 00:17:53,080
And the other part of that is how great it is, right?

179
00:17:53,080 --> 00:18:00,000
So the greatness and the benefit and the happiness and the sukm, sukm, sukmaidati needs

180
00:18:00,000 --> 00:18:05,360
to great happiness when there is such a person, it's happiness for their family, happiness

181
00:18:05,360 --> 00:18:07,320
for their society.

182
00:18:07,320 --> 00:18:16,520
I think this is something that is a good reminder and would be a great thing for people

183
00:18:16,520 --> 00:18:22,720
to realize for families to realize when they are hard on their children or pushing their

184
00:18:22,720 --> 00:18:30,280
children into making money or finding worldly success or stability, getting good grades

185
00:18:30,280 --> 00:18:35,720
and how they are disappointed in their kids if they don't do well in school or if they

186
00:18:35,720 --> 00:18:43,520
change their religion or if they happen to be homosexual or if they, any number of things

187
00:18:43,520 --> 00:18:49,760
that transgender is a big thing now, I have many much hate there, how disappointed people

188
00:18:49,760 --> 00:18:57,680
are because of things like that, anyway, not getting into all that.

189
00:18:57,680 --> 00:19:03,680
Many types of disappointment that parents have and all of the expectations for worldly

190
00:19:03,680 --> 00:19:13,440
success and stability and rectitude, moral, rectitude in various ways, religion and

191
00:19:13,440 --> 00:19:23,080
sexuality and all of these things that lead parents to denounce their children and throw

192
00:19:23,080 --> 00:19:30,600
them out and all of the things that parents look up, look for in their children and force

193
00:19:30,600 --> 00:19:39,800
their children, push their children into getting a good job and making money and getting

194
00:19:39,800 --> 00:19:45,520
married for many cultures, marriage is a very important thing, you must get married, that's

195
00:19:45,520 --> 00:19:51,920
a measure of success and if only people realize what was a true greatness and what wouldn't

196
00:19:51,920 --> 00:19:57,720
really be a truly great thing for your children to do and be for them to practice and

197
00:19:57,720 --> 00:20:03,800
free themselves from suffering, to purify their minds and to train themselves to realize

198
00:20:03,800 --> 00:20:15,920
that all of that useless meaningless stuff is useless and meaningless, is no consequence

199
00:20:15,920 --> 00:20:26,840
and all the energy we put into pushing their children and forcing their children to find

200
00:20:26,840 --> 00:20:33,760
success is always still in misguided and there's so much greatness that comes not from

201
00:20:33,760 --> 00:20:46,560
money or worldly success but from a child or a person who follows the path and finds their

202
00:20:46,560 --> 00:20:54,720
own way to free themselves from all clinging and craving and anger and conceit and ego

203
00:20:54,720 --> 00:21:02,120
and so on, how much greatness comes not just to them but to their family and to their society,

204
00:21:02,120 --> 00:21:08,360
they become morally upstanding, citizen, they become someone who is able to give advice,

205
00:21:08,360 --> 00:21:15,600
someone who guides others and supports others in their own spiritual practice, someone who

206
00:21:15,600 --> 00:21:26,800
is harmless, who is not cruel or unkind, someone who is generous and wise, it's such a great

207
00:21:26,800 --> 00:21:35,760
thing to have in society then, this is just a reminder of what are the important things

208
00:21:35,760 --> 00:21:43,480
in life, what are the important qualities that we should aspire for in ourselves and aspire

209
00:21:43,480 --> 00:21:53,000
for in the people around us, hard to find wherever you do find such a person, such a wise

210
00:21:53,000 --> 00:22:00,840
person, great happiness comes to everyone, so that's the verse, that's the number part

211
00:22:00,840 --> 00:22:30,680
of her tonight, thank you all for listening.

