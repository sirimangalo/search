1
00:00:00,000 --> 00:00:29,000
Okay, good evening, everyone.

2
00:00:29,000 --> 00:00:36,000
Welcome to our dumb session.

3
00:00:36,000 --> 00:00:40,000
Tonight we're looking at,

4
00:00:40,000 --> 00:00:44,000
we're looking at a suit that

5
00:00:44,000 --> 00:00:49,000
that's actually a suit that I've taught

6
00:00:49,000 --> 00:00:52,000
before on the internet.

7
00:00:52,000 --> 00:00:57,000
It should be familiar to at least some of you.

8
00:00:57,000 --> 00:01:00,000
But we're going through, in order,

9
00:01:00,000 --> 00:01:04,000
in the Midgey minikai and now.

10
00:01:04,000 --> 00:01:07,000
I figured we shouldn't skip it

11
00:01:07,000 --> 00:01:10,000
because it's one of those who does that.

12
00:01:10,000 --> 00:01:14,000
I think of what you call perennial value,

13
00:01:14,000 --> 00:01:17,000
it's something that you can repeat.

14
00:01:17,000 --> 00:01:20,000
That should be repeated.

15
00:01:20,000 --> 00:01:24,000
It's a fairly simple teaching.

16
00:01:24,000 --> 00:01:34,000
There's not one you might call a great amount of depth to it.

17
00:01:34,000 --> 00:01:39,000
But it does lay out something very fundamental about Buddhism

18
00:01:39,000 --> 00:01:43,000
that I think is important.

19
00:01:43,000 --> 00:01:51,000
It's also a suit that my teacher talked about often.

20
00:01:51,000 --> 00:02:00,000
Which is, I think, is a good recommendation as any.

21
00:02:00,000 --> 00:02:03,000
So we're looking at Midgey minikai in 29,

22
00:02:03,000 --> 00:02:08,000
the Mahasaro pamasuta.

23
00:02:08,000 --> 00:02:12,000
Sahuro pamma, Maha Sahuro pamma.

24
00:02:12,000 --> 00:02:15,000
Maha means it's a big suit to her.

25
00:02:15,000 --> 00:02:18,000
It's the bigger one of two.

26
00:02:18,000 --> 00:02:23,000
There's another one that's the Judasaro pamasuta, small one.

27
00:02:23,000 --> 00:02:36,000
Sahara means something that is essential core.

28
00:02:36,000 --> 00:02:43,000
It also means the core of a tree, the heart of a tree.

29
00:02:43,000 --> 00:02:48,000
I think that's how it's used in this suit anyway.

30
00:02:48,000 --> 00:02:50,000
That's what it means.

31
00:02:50,000 --> 00:02:55,000
Because upamma means a simile.

32
00:02:55,000 --> 00:02:59,000
So the Buddha used similes.

33
00:02:59,000 --> 00:03:02,000
So useful teaching technique.

34
00:03:02,000 --> 00:03:06,000
It helps someone understand a new concept

35
00:03:06,000 --> 00:03:12,000
by appealing to their intimate understanding of an old concept.

36
00:03:12,000 --> 00:03:17,000
A simple concept allows us to categorize things.

37
00:03:17,000 --> 00:03:19,000
This is like that.

38
00:03:19,000 --> 00:03:25,000
It's a function of our mind to categorize things.

39
00:03:25,000 --> 00:03:31,000
If this is like that, then I know what sort of thing it is

40
00:03:31,000 --> 00:03:35,000
and how to deal with it.

41
00:03:35,000 --> 00:03:43,000
Sanya, sanya, sanya means making it, knowing it like something else,

42
00:03:43,000 --> 00:03:47,000
recognizing it as like something else.

43
00:03:47,000 --> 00:03:52,000
It's a useful tool.

44
00:03:52,000 --> 00:03:54,000
The Buddha starts out.

45
00:03:54,000 --> 00:03:58,000
The introduction just to give a brief mention is on Vulture's peak,

46
00:03:58,000 --> 00:04:00,000
which you can actually go and see.

47
00:04:00,000 --> 00:04:02,000
Even today it does look.

48
00:04:02,000 --> 00:04:04,000
You can understand why they might have thought it was like a vulture.

49
00:04:04,000 --> 00:04:08,000
It's a bunch of rocks piled up.

50
00:04:08,000 --> 00:04:13,000
Kind of like a vulture like.

51
00:04:13,000 --> 00:04:20,000
And it was a place where the Buddha spent some time.

52
00:04:20,000 --> 00:04:23,000
I was a place where the Buddha probably meditated quite a bit

53
00:04:23,000 --> 00:04:26,000
and even spent nights there.

54
00:04:26,000 --> 00:04:33,000
Before the first monastery was created, this is where the Buddha stayed.

55
00:04:33,000 --> 00:04:36,000
We soon after Devodata had left.

56
00:04:36,000 --> 00:04:39,000
Devodata was the Buddha's cousin.

57
00:04:39,000 --> 00:04:41,000
He caused a lot of problems.

58
00:04:41,000 --> 00:04:51,000
He was full of himself and wanted to be the big teacher.

59
00:04:51,000 --> 00:04:57,000
And so he caused a schism in the sangha.

60
00:04:57,000 --> 00:05:02,000
He left with a bunch of new monks who didn't know better.

61
00:05:02,000 --> 00:05:07,000
And while he ended up making a fool of himself and losing all of his followers.

62
00:05:07,000 --> 00:05:14,000
But at this point he had just left.

63
00:05:14,000 --> 00:05:20,000
And so it was in reference to Devodata, the Buddha talked about.

64
00:05:20,000 --> 00:05:24,000
How some people just don't find what is essential.

65
00:05:24,000 --> 00:05:28,000
Rather than really talking bad about Devodata,

66
00:05:28,000 --> 00:05:35,000
he just described Devodata as someone who hadn't found the source.

67
00:05:35,000 --> 00:05:40,000
Hadn't found the heart.

68
00:05:40,000 --> 00:05:43,000
The heart of religion one might say.

69
00:05:43,000 --> 00:05:48,000
I don't really say the heart of Buddhism because Buddhist don't believe in things like Buddhism.

70
00:05:48,000 --> 00:05:53,000
We believe in reality and truth and so on.

71
00:05:53,000 --> 00:05:54,000
Religion.

72
00:05:54,000 --> 00:05:58,000
I like to think of Buddhism as sort of like religion.

73
00:05:58,000 --> 00:06:02,000
Buddhism is pure religion.

74
00:06:02,000 --> 00:06:08,000
Religion in the sense of something profound.

75
00:06:08,000 --> 00:06:17,000
Something meaningful.

76
00:06:17,000 --> 00:06:36,000
And so in regards to what we might call religion, there are five aspects of how we might look at the world religiously.

77
00:06:36,000 --> 00:06:45,000
And from the first one we can see that most of what we call most of what we aim for in the world is.

78
00:06:45,000 --> 00:06:47,000
Surprisingly not very religious.

79
00:06:47,000 --> 00:06:51,000
It's not very advanced from a religious point of view.

80
00:06:51,000 --> 00:06:54,000
So the Buddha says he's referring to monks.

81
00:06:54,000 --> 00:07:03,000
He says someone leaves home, becomes a religious person.

82
00:07:03,000 --> 00:07:14,000
And as a result of that they become quite perhaps famous or they fall under a famous teacher.

83
00:07:14,000 --> 00:07:27,000
People look up to them, respect them, give them all sorts of support and esteem.

84
00:07:27,000 --> 00:07:33,000
And they become content with that.

85
00:07:33,000 --> 00:07:45,000
If you think from a religious point of view, in terms of religious individuals, this is the charlatan.

86
00:07:45,000 --> 00:07:53,000
Someone is going to claim to be a religious individual, but they're not doing anything religious.

87
00:07:53,000 --> 00:08:05,000
Not even try to do anything greater.

88
00:08:05,000 --> 00:08:08,000
Pretend to be something they're not.

89
00:08:08,000 --> 00:08:10,000
This is the worst, right?

90
00:08:10,000 --> 00:08:11,000
This is the lowest.

91
00:08:11,000 --> 00:08:18,000
But I'd like to look at this suit and not exclusively in terms of how it relates to monks.

92
00:08:18,000 --> 00:08:23,000
Because my audience is mostly not monks.

93
00:08:23,000 --> 00:08:27,000
And it's I think a lesser point.

94
00:08:27,000 --> 00:08:34,000
The greater point is that most of the world is caught up in things like gain and praise and fame.

95
00:08:34,000 --> 00:08:40,000
And really what we're talking about is sensuality.

96
00:08:40,000 --> 00:08:46,000
And it's poetic to think of it like a monk is like a person going off into the forest

97
00:08:46,000 --> 00:08:53,000
and looking for heart wood, looking for really the core, the heart of a tree.

98
00:08:53,000 --> 00:09:06,000
This hard wood that is very, like if you take teak, it's that core, the heart of the teak tree that is the strongest.

99
00:09:06,000 --> 00:09:15,000
But if you expand this and think of all of us, think of the whole world, most people are seeking.

100
00:09:15,000 --> 00:09:23,000
They're seeking something good, something beneficial, something meaningful.

101
00:09:23,000 --> 00:09:29,000
But they find meaning in things that are meaningless.

102
00:09:29,000 --> 00:09:42,000
So our search is for sensuality.

103
00:09:42,000 --> 00:09:49,000
We strive for beautiful sights and sounds and smells and tastes and feelings.

104
00:09:49,000 --> 00:10:04,000
We strive for stimulus, stimulation of the senses.

105
00:10:04,000 --> 00:10:13,000
And I think this is a really important entry point for religion.

106
00:10:13,000 --> 00:10:15,000
Someone tried it.

107
00:10:15,000 --> 00:10:32,000
I think my mother actually believed she tried to pin fear as the catalyst for religious mindset.

108
00:10:32,000 --> 00:10:35,000
She's not religious, she doesn't look highly upon religion.

109
00:10:35,000 --> 00:10:38,000
She doesn't think very highly of my path, I don't think.

110
00:10:38,000 --> 00:10:40,000
Well, I'm able to convince her.

111
00:10:40,000 --> 00:10:48,000
I think no, that's not true because she sees how meditation is helpful to other people.

112
00:10:48,000 --> 00:10:58,000
But so she said fear and I think it's true that many people turn to religion out of fear.

113
00:10:58,000 --> 00:11:04,000
I think it's a bit deeper and there's something more meaningful.

114
00:11:04,000 --> 00:11:11,000
And it's sort of the realization that something's wrong.

115
00:11:11,000 --> 00:11:17,000
Something's wrong with this picture.

116
00:11:17,000 --> 00:11:24,000
So it could be a fear of death, it could be a real catalyst.

117
00:11:24,000 --> 00:11:38,000
I mean, it's much more broad than that many people come to religion just because they can't make sense of the world.

118
00:11:38,000 --> 00:11:53,000
They've gone through loss or perhaps they've seen loss in others or they've thought deeply about the world and just doesn't add up.

119
00:11:53,000 --> 00:12:07,000
If you look one real common theme among philosophers and religious people alike is that desire is not universal.

120
00:12:07,000 --> 00:12:16,000
But it's something you see again and again, Socrates, Aristotle, even Epicurus.

121
00:12:16,000 --> 00:12:27,000
You look at the Hindus, all in India, everyone before the Buddha, they were all the different philosophers in India.

122
00:12:27,000 --> 00:12:29,000
They were all talking about this.

123
00:12:29,000 --> 00:12:40,000
Just around the time that Socrates was talking about it, how desire for sensuality is just messes you up.

124
00:12:40,000 --> 00:12:46,000
It doesn't lead to happiness or satisfaction.

125
00:12:46,000 --> 00:12:50,000
It leads to dissatisfaction and addiction.

126
00:12:50,000 --> 00:12:59,000
This was the kind of thing people were talking about.

127
00:12:59,000 --> 00:13:01,000
And so this is where religion starts.

128
00:13:01,000 --> 00:13:07,000
I think it's a big reason why religion clings so strongly to morality.

129
00:13:07,000 --> 00:13:15,000
And that's really the entry point for religion where religion says no, don't do this.

130
00:13:15,000 --> 00:13:20,000
Don't just follow, don't just follow your wins.

131
00:13:20,000 --> 00:13:25,000
There's so many reasons following after our desires leads to war.

132
00:13:25,000 --> 00:13:28,000
It's why we go to war for the most part.

133
00:13:28,000 --> 00:13:30,000
Why there's so much war in the world.

134
00:13:30,000 --> 00:13:39,000
It's mostly greed, it's mostly because we all want more in each other reasons.

135
00:13:39,000 --> 00:13:44,000
But I think it's the biggest reason why we fight with each other.

136
00:13:44,000 --> 00:13:46,000
It's why families fight taking it along.

137
00:13:46,000 --> 00:13:52,000
I think it's a big reason why divorce is so common.

138
00:13:52,000 --> 00:13:54,000
There's other reasons.

139
00:13:54,000 --> 00:14:10,000
But marriage was, I think, meant to be kind of this limiting of your addiction to sensuality, to sexuality.

140
00:14:10,000 --> 00:14:12,000
Among other things.

141
00:14:12,000 --> 00:14:15,000
But some sort of order, because it's chaos.

142
00:14:15,000 --> 00:14:22,000
If everyone just takes what they want,

143
00:14:22,000 --> 00:14:28,000
most of our laws are meant to sort of stop us.

144
00:14:28,000 --> 00:14:33,000
Stop us from doing what we want.

145
00:14:33,000 --> 00:14:43,000
Without any religious training, we want to do things that are very, very wrong.

146
00:14:43,000 --> 00:14:47,000
You have something I want it.

147
00:14:47,000 --> 00:14:52,000
I want to take it from you.

148
00:14:52,000 --> 00:14:57,000
It leads to terrible things, theft, murder, rape.

149
00:14:57,000 --> 00:15:02,000
Not to mention war, genocide, all sorts of terrible atrocity.

150
00:15:02,000 --> 00:15:10,000
Things that we do is humans with our intense greed and other things, not just greed.

151
00:15:10,000 --> 00:15:14,000
It's the big one.

152
00:15:14,000 --> 00:15:22,000
So the second, someone who seeks for desire, if you're familiar with this,

153
00:15:22,000 --> 00:15:27,000
but compared them to people who go into the forest looking for wood.

154
00:15:27,000 --> 00:15:33,000
And they come out with twigs and leaves.

155
00:15:33,000 --> 00:15:36,000
And they say, hey, look at the wood I cut.

156
00:15:36,000 --> 00:15:38,000
I'm going to build my house out of this.

157
00:15:38,000 --> 00:15:45,000
Like the first little pig, or another second little pig.

158
00:15:45,000 --> 00:15:46,000
Twigs and leaves.

159
00:15:46,000 --> 00:15:48,000
That's all they get.

160
00:15:48,000 --> 00:15:49,000
Handful leaves.

161
00:15:49,000 --> 00:15:53,000
Hey, look what I got.

162
00:15:53,000 --> 00:15:56,000
This is how we live our lives running around.

163
00:15:56,000 --> 00:16:05,000
Finding meaning, the philosophy of indulgence.

164
00:16:05,000 --> 00:16:08,000
People talk about what they're drunk.

165
00:16:08,000 --> 00:16:12,000
They sit around and philosophize about the good life.

166
00:16:12,000 --> 00:16:15,000
Hey, if it's good for you, go for it.

167
00:16:15,000 --> 00:16:21,000
You know what was as long as you don't treat others badly.

168
00:16:21,000 --> 00:16:25,000
No, not to look down on people, but we're all.

169
00:16:25,000 --> 00:16:33,000
We have this in us, this sort of animal, lust, desire.

170
00:16:33,000 --> 00:16:38,000
Just take what we want in it.

171
00:16:38,000 --> 00:16:40,000
We rationalize it.

172
00:16:40,000 --> 00:16:43,000
And so this is what I mean by philosophy is we try to rationalize it.

173
00:16:43,000 --> 00:16:52,000
And we come up with all these quasi philosophical arguments for

174
00:16:52,000 --> 00:16:54,000
you only live once, right?

175
00:16:54,000 --> 00:17:00,000
These kind of things.

176
00:17:00,000 --> 00:17:05,000
But the biggest thing is that engaging in sensuality

177
00:17:05,000 --> 00:17:12,000
it messes up your mind, prevents you from seeing clearly.

178
00:17:12,000 --> 00:17:15,000
So morality is a big thing.

179
00:17:15,000 --> 00:17:20,000
But I said, well, someone who engages in morality

180
00:17:20,000 --> 00:17:29,000
and sticks to that.

181
00:17:29,000 --> 00:17:34,000
He says, that person has taken the outer bark.

182
00:17:34,000 --> 00:17:39,000
Yeah.

183
00:17:39,000 --> 00:17:49,000
The person takes bark a bunch of bark.

184
00:17:49,000 --> 00:17:50,000
And says, oh, look at me.

185
00:17:50,000 --> 00:17:51,000
I've got wood.

186
00:17:51,000 --> 00:17:55,000
Maybe they can make a bark dress out of it or something.

187
00:17:55,000 --> 00:17:57,000
Can't make a house out of it.

188
00:17:57,000 --> 00:18:04,000
There's nothing very significant.

189
00:18:04,000 --> 00:18:08,000
Morality is interesting.

190
00:18:08,000 --> 00:18:12,000
I think it's superior to sensuality.

191
00:18:12,000 --> 00:18:21,000
If you can, because it involves contentment,

192
00:18:21,000 --> 00:18:24,000
you can say it's a sort of a forced contentment.

193
00:18:24,000 --> 00:18:27,000
But a person who is moral and there's people like,

194
00:18:27,000 --> 00:18:31,000
I mean, most of us are like that most of us aren't

195
00:18:31,000 --> 00:18:36,000
raping and pillaging and killing each other for our belongings.

196
00:18:36,000 --> 00:18:38,000
Most of us are fairly ethical.

197
00:18:38,000 --> 00:18:41,000
Human beings to be born in human, you have to be fairly ethical

198
00:18:41,000 --> 00:18:46,000
in the first place.

199
00:18:46,000 --> 00:18:52,000
So it's superior because we're less crazed and addicted.

200
00:18:52,000 --> 00:18:56,000
I have greater contentment because we stop ourselves.

201
00:18:56,000 --> 00:18:59,000
And I say, OK, I want that while I'm not going to go for it.

202
00:18:59,000 --> 00:19:04,000
And because we don't go for it, we don't encourage the addiction.

203
00:19:04,000 --> 00:19:08,000
And so in general, our minds are more content just from ethics,

204
00:19:08,000 --> 00:19:13,000
from morality, more stable, more clear.

205
00:19:13,000 --> 00:19:19,000
The mind is more clear because we don't kill and steal and cheat

206
00:19:19,000 --> 00:19:26,000
and lies on.

207
00:19:26,000 --> 00:19:29,000
But it's not nearly enough.

208
00:19:29,000 --> 00:19:35,000
You look at these priests and Buddhist monks who abuse

209
00:19:35,000 --> 00:19:42,000
little boys.

210
00:19:42,000 --> 00:19:52,000
If so many religious people who are so repressed and just angry

211
00:19:52,000 --> 00:20:02,000
and bitter and often become perverse as a result

212
00:20:02,000 --> 00:20:07,000
and take even greater pleasure, guilty pleasure.

213
00:20:07,000 --> 00:20:10,000
For most of us, it becomes guilty pleasure.

214
00:20:10,000 --> 00:20:11,000
We know it's wrong.

215
00:20:11,000 --> 00:20:16,000
We know it's kind of like worse than just saying,

216
00:20:16,000 --> 00:20:20,000
I like it, so I'm going to go for it.

217
00:20:20,000 --> 00:20:25,000
When you say it's wicked and it's sinful.

218
00:20:25,000 --> 00:20:30,000
Morality can become quite perverse when we say something is sinful

219
00:20:30,000 --> 00:20:34,000
and we feel guilty about it, but we do it anyway.

220
00:20:34,000 --> 00:20:36,000
And it's even more attractive.

221
00:20:36,000 --> 00:20:41,000
The taboo, that which is wrong.

222
00:20:41,000 --> 00:20:45,000
It's the only way I can make sense of this.

223
00:20:45,000 --> 00:20:48,000
It's actually apparently not.

224
00:20:48,000 --> 00:20:51,000
Religion doesn't.

225
00:20:51,000 --> 00:20:56,000
Religious people don't abuse young children any more

226
00:20:56,000 --> 00:21:01,000
than any other position of power, apparently they didn't study on it.

227
00:21:01,000 --> 00:21:06,000
I mean, I think there is something there that

228
00:21:06,000 --> 00:21:13,000
morality can be quite dangerous if you rely upon it.

229
00:21:13,000 --> 00:21:16,000
At the very least it's just repressing.

230
00:21:16,000 --> 00:21:21,000
It's just sort of stemming the tide with your bare hands

231
00:21:21,000 --> 00:21:30,000
just trying to hold back the power of the lust and the desire.

232
00:21:30,000 --> 00:21:33,000
It's a good attempt and it's a good start.

233
00:21:33,000 --> 00:21:34,000
It's certainly necessary.

234
00:21:34,000 --> 00:21:40,000
I mean, you can't really hope to understand

235
00:21:40,000 --> 00:21:47,000
to make sense of the world and rise above the tide

236
00:21:47,000 --> 00:21:53,000
if you're drowning in lust and desire and addiction.

237
00:21:53,000 --> 00:21:56,000
I have to sober up before you can see clearly.

238
00:21:56,000 --> 00:22:00,000
So this temporary solution is quite useful.

239
00:22:00,000 --> 00:22:04,000
Coming here to the meditation center is a temporary solution

240
00:22:04,000 --> 00:22:07,000
you're on in detox.

241
00:22:07,000 --> 00:22:11,000
There's like a detox center you come here.

242
00:22:11,000 --> 00:22:13,000
And it helps you get your life back together.

243
00:22:13,000 --> 00:22:16,000
That's the start.

244
00:22:16,000 --> 00:22:20,000
Keeping all these rules and precepts and really just living

245
00:22:20,000 --> 00:22:27,000
in a steer life, you guys are living more austere than many monks.

246
00:22:27,000 --> 00:22:36,000
You should be congratulated.

247
00:22:36,000 --> 00:22:39,000
So it's the second, the third.

248
00:22:39,000 --> 00:22:41,000
What do you need to do next?

249
00:22:41,000 --> 00:22:46,000
Really, what morality of ethics

250
00:22:46,000 --> 00:22:50,000
if performed properly, what it leads to is calm.

251
00:22:50,000 --> 00:22:54,000
So by not doing all these things your mind starts to calm down

252
00:22:54,000 --> 00:23:01,000
and clear up.

253
00:23:01,000 --> 00:23:06,000
Ethics really take into its ultimate conclusion

254
00:23:06,000 --> 00:23:09,000
or its consummation.

255
00:23:09,000 --> 00:23:11,000
It's not about not killing or stealing.

256
00:23:11,000 --> 00:23:19,000
It's about purifying the mind really.

257
00:23:19,000 --> 00:23:25,000
It's about when you see something about guarding that.

258
00:23:25,000 --> 00:23:33,000
So that your mind doesn't do something unethical.

259
00:23:33,000 --> 00:23:39,000
The shift from ethics to meditation

260
00:23:39,000 --> 00:23:46,000
comes about when you prevent your mind from doing bad things,

261
00:23:46,000 --> 00:23:48,000
not just your body of killing and stealing,

262
00:23:48,000 --> 00:23:53,000
but where something comes that would anger you

263
00:23:53,000 --> 00:23:57,000
and you stop your mind.

264
00:23:57,000 --> 00:24:05,000
Again, it's this sort of forcing your mind not to do what it wants to do.

265
00:24:05,000 --> 00:24:07,000
Not to do what it's used to doing.

266
00:24:07,000 --> 00:24:09,000
You're getting angry.

267
00:24:09,000 --> 00:24:12,000
Instead, when we say to ourselves seeing,

268
00:24:12,000 --> 00:24:21,000
I'm spilling concentration.

269
00:24:21,000 --> 00:24:24,000
Now, with that kind of practice,

270
00:24:24,000 --> 00:24:27,000
it's not really easy to get stuck on concentration

271
00:24:27,000 --> 00:24:32,000
because it's actually considered not to be the practice of concentration.

272
00:24:32,000 --> 00:24:37,000
It's insight, but that's where it gets a little complicated.

273
00:24:37,000 --> 00:24:40,000
There are many types of meditation,

274
00:24:40,000 --> 00:24:44,000
and this is where religion really becomes quite religious.

275
00:24:44,000 --> 00:24:45,000
They may call it meditation.

276
00:24:45,000 --> 00:24:47,000
They may not call it meditation,

277
00:24:47,000 --> 00:24:49,000
but where it calms the mind down

278
00:24:49,000 --> 00:24:51,000
and clears the mind up,

279
00:24:51,000 --> 00:24:55,000
where you force the mind into a state,

280
00:24:55,000 --> 00:24:57,000
or you train the mind.

281
00:24:57,000 --> 00:25:02,000
It should be generous, charitable.

282
00:25:02,000 --> 00:25:07,000
You train the mind in such a way that it calms down.

283
00:25:07,000 --> 00:25:11,000
It doesn't ever, like you circle the wagons

284
00:25:11,000 --> 00:25:14,000
and you keep the mind in the fence,

285
00:25:14,000 --> 00:25:16,000
or you build a fence,

286
00:25:16,000 --> 00:25:21,000
and you keep the mind tied to a rope,

287
00:25:21,000 --> 00:25:28,000
so it can't go out.

288
00:25:28,000 --> 00:25:33,000
So this is the practice of summit to meditation,

289
00:25:33,000 --> 00:25:36,000
mainly, or let's say the calming of the mind.

290
00:25:36,000 --> 00:25:38,000
It's a common Buddhist practice,

291
00:25:38,000 --> 00:25:40,000
but it's definitely a common religious practice

292
00:25:40,000 --> 00:25:43,000
to find a way to calm the mind.

293
00:25:43,000 --> 00:25:51,000
Theistic religions pray, read, study, sing,

294
00:25:51,000 --> 00:25:55,000
any religious traditions, sing, and even dance.

295
00:25:55,000 --> 00:25:57,000
It's all meditation.

296
00:25:57,000 --> 00:26:01,000
It's always putting the mind in a trance state

297
00:26:01,000 --> 00:26:03,000
so that it doesn't wander,

298
00:26:03,000 --> 00:26:05,000
so that it doesn't get distracted

299
00:26:05,000 --> 00:26:11,000
or attracted by the evils of the world.

300
00:26:11,000 --> 00:26:13,000
And it's better than ethics.

301
00:26:13,000 --> 00:26:15,000
It's dealing with the mind.

302
00:26:15,000 --> 00:26:18,000
It's a powerful state.

303
00:26:18,000 --> 00:26:21,000
It's such a powerful state that if you really get into it,

304
00:26:21,000 --> 00:26:24,000
you can become God.

305
00:26:24,000 --> 00:26:26,000
The Hindus talked to all about this,

306
00:26:26,000 --> 00:26:27,000
the Brahmins.

307
00:26:27,000 --> 00:26:29,000
You become one with God.

308
00:26:29,000 --> 00:26:31,000
In the Bhani shan,

309
00:26:31,000 --> 00:26:34,000
there's a big thing in Hindu meditation nowadays.

310
00:26:34,000 --> 00:26:37,000
It's all about becoming God.

311
00:26:37,000 --> 00:26:41,000
What just, you know, the Buddha actually agreed with them.

312
00:26:41,000 --> 00:26:44,000
Yeah, that's the way to become one with God.

313
00:26:52,000 --> 00:26:53,000
So is that it then?

314
00:26:53,000 --> 00:26:54,000
You become God?

315
00:26:54,000 --> 00:26:59,000
No, no, no.

316
00:26:59,000 --> 00:27:06,000
Not because remember, it's still just some kind of repression.

317
00:27:06,000 --> 00:27:10,000
Some kind of avoidance.

318
00:27:10,000 --> 00:27:13,000
It's like saying there's

319
00:27:13,000 --> 00:27:15,000
problems with our country,

320
00:27:15,000 --> 00:27:18,000
so let's go and take a vacation.

321
00:27:18,000 --> 00:27:20,000
Here we live in this country,

322
00:27:20,000 --> 00:27:23,000
and it's all sorts of social problems.

323
00:27:23,000 --> 00:27:28,000
And, well, let's go to some tropical island.

324
00:27:28,000 --> 00:27:31,000
Just fine.

325
00:27:31,000 --> 00:27:33,000
You know, you can go live on the tropical island,

326
00:27:33,000 --> 00:27:38,000
but it doesn't fix the problems in your homeland.

327
00:27:38,000 --> 00:27:42,000
And if you want to take the analogy even further,

328
00:27:42,000 --> 00:27:45,000
well, you don't have a passport for this island,

329
00:27:45,000 --> 00:27:52,000
so eventually you're going to go back.

330
00:27:52,000 --> 00:27:54,000
Yeah, even a God, right?

331
00:27:54,000 --> 00:27:56,000
And this is something that is,

332
00:27:56,000 --> 00:27:58,000
let's not talk about being a God,

333
00:27:58,000 --> 00:28:01,000
because it's not something I suppose many of us think about here,

334
00:28:01,000 --> 00:28:06,000
but just in terms of tranquility meditation,

335
00:28:06,000 --> 00:28:08,000
when you stop practicing,

336
00:28:08,000 --> 00:28:11,000
all your problems come back,

337
00:28:11,000 --> 00:28:14,000
even with respect to meditation.

338
00:28:14,000 --> 00:28:17,000
When you come here,

339
00:28:17,000 --> 00:28:19,000
a lot of what you're doing is just calming down.

340
00:28:19,000 --> 00:28:22,000
Really, a lot of it is forced.

341
00:28:22,000 --> 00:28:25,000
And so when you leave here, you're going to feel very calm

342
00:28:25,000 --> 00:28:29,000
and wow, it really improved my life.

343
00:28:29,000 --> 00:28:34,000
But then you get out there and all the problems come back.

344
00:28:34,000 --> 00:28:36,000
We overestimate our progress,

345
00:28:36,000 --> 00:28:42,000
because a great part of our progress is just this tranquility aspect.

346
00:28:42,000 --> 00:28:56,000
The calming of the mind and this strong training of the mind and calm.

347
00:28:56,000 --> 00:29:01,000
And then it all comes back, and some doubt comes to meditators often.

348
00:29:01,000 --> 00:29:05,000
Did I really was it really as good as I thought,

349
00:29:05,000 --> 00:29:08,000
and why am I still have all the same problem?

350
00:29:08,000 --> 00:29:10,000
That's usually not that bad.

351
00:29:10,000 --> 00:29:12,000
I mean, I've never really heard a meditator feel like,

352
00:29:12,000 --> 00:29:15,000
oh, that was a waste of my time.

353
00:29:15,000 --> 00:29:17,000
But you have to grudgingly accept,

354
00:29:17,000 --> 00:29:22,000
I know it wasn't as...

355
00:29:22,000 --> 00:29:24,000
I didn't get rid of everything.

356
00:29:24,000 --> 00:29:30,000
All the things I thought I got rid of.

357
00:29:30,000 --> 00:29:35,000
And this wake up call was going to take longer than I thought.

358
00:29:35,000 --> 00:29:41,000
So the Buddha said a person who practices some at the meditation,

359
00:29:41,000 --> 00:29:43,000
who calms the mind down,

360
00:29:43,000 --> 00:29:54,000
does like a person who picks up the inner bark, right?

361
00:29:54,000 --> 00:29:58,000
Yeah.

362
00:29:58,000 --> 00:30:00,000
Takes the inner bark and says,

363
00:30:00,000 --> 00:30:01,000
hey, look what I got.

364
00:30:01,000 --> 00:30:07,000
Still not very useful.

365
00:30:07,000 --> 00:30:10,000
So here's where Buddhism comes in, really.

366
00:30:10,000 --> 00:30:14,000
And Buddhism, in the sense of the philosophy of the Buddha.

367
00:30:14,000 --> 00:30:16,000
Because at the time of the Buddha,

368
00:30:16,000 --> 00:30:19,000
there were clearly people practicing to calm the mind down

369
00:30:19,000 --> 00:30:21,000
and another religions.

370
00:30:21,000 --> 00:30:32,000
And we say religions that don't really have any part

371
00:30:32,000 --> 00:30:37,000
in what we would call wisdom in terms of understanding religious.

372
00:30:37,000 --> 00:30:41,000
And I don't mean religions like Christianity as a religion,

373
00:30:41,000 --> 00:30:48,000
but people practicing religious ways that are mostly just faith-based

374
00:30:48,000 --> 00:30:50,000
where they believe something.

375
00:30:50,000 --> 00:30:52,000
But a lot of these people, they calm down.

376
00:30:52,000 --> 00:30:54,000
They have ways of calming themselves.

377
00:30:54,000 --> 00:30:59,000
Hindu is somewhere it's all about becoming one with God and so on.

378
00:30:59,000 --> 00:31:03,000
They calm themselves down.

379
00:31:03,000 --> 00:31:15,000
But in Buddhism, we have this conceit that we have wisdom.

380
00:31:15,000 --> 00:31:20,000
We have some sort of wisdom higher than these other paths.

381
00:31:20,000 --> 00:31:26,000
And so this is what we get into in the practice of insight meditation.

382
00:31:26,000 --> 00:31:29,000
You learn so much about yourself, right?

383
00:31:29,000 --> 00:31:33,000
I really do think it's a different type.

384
00:31:33,000 --> 00:31:40,000
It's impossible to suggest that it's the same type as all these other types of meditation.

385
00:31:40,000 --> 00:31:42,000
Because you're not trying to calm the mind.

386
00:31:42,000 --> 00:31:44,000
You're not really trying to...

387
00:31:44,000 --> 00:31:53,000
I mean, you're not achieving higher states of equanimity or tranquility.

388
00:31:53,000 --> 00:32:01,000
Instead of running away, you're actually sorting things out.

389
00:32:01,000 --> 00:32:12,000
You're learning to dance or learning to fight or learning to exist in a war zone, really.

390
00:32:12,000 --> 00:32:19,000
The benefit of that is the training, the familiarity and the ability,

391
00:32:19,000 --> 00:32:28,000
because it's the understanding of war, the understanding of the war inside.

392
00:32:28,000 --> 00:32:31,000
Actually suffering through your problems.

393
00:32:31,000 --> 00:32:40,000
Your likes and dislikes, your ego, your anxiety, your fear, your doubt, all of this.

394
00:32:40,000 --> 00:32:43,000
Actually suffering through it, how is this?

395
00:32:43,000 --> 00:32:50,000
This is how change, the real, deep change comes about.

396
00:32:50,000 --> 00:32:54,000
Because we're doing it to ourselves.

397
00:32:54,000 --> 00:33:02,000
Understanding desire, understanding addiction, understanding the guilt for wanting things that we know are wrong,

398
00:33:02,000 --> 00:33:08,000
and understanding our anxieties and fears, our worries, our doubts.

399
00:33:08,000 --> 00:33:13,000
Concede, understanding all these things.

400
00:33:13,000 --> 00:33:22,000
Is understanding that which causes us suffering, how true that which hurts us.

401
00:33:22,000 --> 00:33:26,000
We think, oh, well, then if we found the heartwood and the answer is no, we haven't.

402
00:33:26,000 --> 00:33:32,000
No, even that's not good enough for the Buddha.

403
00:33:32,000 --> 00:33:39,000
Even the understanding we gain that you're gaining now through meditation is not enough.

404
00:33:39,000 --> 00:33:47,000
You can still forget it.

405
00:33:47,000 --> 00:33:57,000
Often you know what something is wrong or you learn something is wrong, but you still go after it.

406
00:33:57,000 --> 00:34:06,000
And you can lose your way and learn something. You can forget it.

407
00:34:06,000 --> 00:34:16,000
So the goal isn't the heart of religion or whatever you want to call existence.

408
00:34:16,000 --> 00:34:21,000
The real essence of existence isn't even wisdom.

409
00:34:21,000 --> 00:34:29,000
Not in that, not that sort, not what we normally think of as wisdom.

410
00:34:29,000 --> 00:34:35,000
But wisdom is the tool.

411
00:34:35,000 --> 00:34:46,000
If you gain enough wisdom, if you gain enough wisdom, there's this kind of epiphany really.

412
00:34:46,000 --> 00:34:56,000
Or it's more like a landslide, or the mind begins to create this feedback loop really.

413
00:34:56,000 --> 00:35:01,000
Yes, that is not worth clinging to.

414
00:35:01,000 --> 00:35:04,000
That's not worth clinging to. That's not worth clinging to.

415
00:35:04,000 --> 00:35:13,000
And again, until nothing is worth clinging to, until there comes this moment or this landslide,

416
00:35:13,000 --> 00:35:36,000
sort of like a explosion of light, where the mind hasn't a feeling that's such a inadequate word,

417
00:35:36,000 --> 00:35:47,000
a deep and profound, complete perception of nothing being worth clinging to.

418
00:35:47,000 --> 00:35:50,000
That's the point.

419
00:35:50,000 --> 00:35:54,000
So this wisdom of saying, oh, look at me clinging to these things.

420
00:35:54,000 --> 00:35:58,000
They're not worth clinging to. Look at that. Here I am. That's why I'm suffering.

421
00:35:58,000 --> 00:36:01,000
Because I'm clinging to these things that are not worth clinging to.

422
00:36:01,000 --> 00:36:06,000
I'm trying to fix things. I'm trying to control things. I'm reacting to things.

423
00:36:06,000 --> 00:36:08,000
That's where the problem is.

424
00:36:08,000 --> 00:36:13,000
So then when you see that again and again and again and it becomes this feedback loop

425
00:36:13,000 --> 00:36:19,000
that gets stronger and stronger and stronger until it just overwhelms you.

426
00:36:19,000 --> 00:36:25,000
And you have this complete perception of nothing being worth clinging to.

427
00:36:25,000 --> 00:36:28,000
That's the moment that's what leads to freedom.

428
00:36:28,000 --> 00:36:36,000
That's what leads to the experience of Nimana. Nimana is just this unbinding where you have let go.

429
00:36:36,000 --> 00:36:39,000
And that's what causes the experience of cessation.

430
00:36:39,000 --> 00:36:48,000
And it's the experience of cessation that is categorically different.

431
00:36:48,000 --> 00:36:57,000
I think the best way of explaining why that is so is because it is peaceful.

432
00:36:57,000 --> 00:37:05,000
It is happiness.

433
00:37:05,000 --> 00:37:28,000
And it is something, and the Buddha says, un-sailable, unshakable.

434
00:37:28,000 --> 00:37:36,000
It is going to cause you going to bring your happiness.

435
00:37:36,000 --> 00:37:45,000
Then all that has to happen for you not to be happy is you don't get X, Y, or Z.

436
00:37:45,000 --> 00:37:56,000
By definition, the state of not clinging to anything, saying that of feeling that of not even intellectually,

437
00:37:56,000 --> 00:38:10,000
but just having the state of mind of not wanting to cling to anything, that is where the power is.

438
00:38:10,000 --> 00:38:23,000
And once you mean the experience and the verification that that is true, that of the perfect piece that comes from that,

439
00:38:23,000 --> 00:38:27,000
that is life-changing.

440
00:38:27,000 --> 00:38:32,000
That is the hardwood.

441
00:38:32,000 --> 00:38:41,000
Whereas in that person who has gone all that way, how they found something really essential.

442
00:38:41,000 --> 00:38:47,000
So you see, I think this is a very important description.

443
00:38:47,000 --> 00:38:54,000
I mean, a lot of it is simple and stuff that we know of morality leading the concentration of wisdom,

444
00:38:54,000 --> 00:38:57,000
but you see it is a little different.

445
00:38:57,000 --> 00:39:12,000
Instead of morality, concentration, wisdom, we have a good explanation of what is enough and what is not enough and how far you have to go.

446
00:39:12,000 --> 00:39:19,000
What we are going for and why we are going there, why we can't just go home and enjoy life.

447
00:39:19,000 --> 00:39:27,000
Thinking about these things, I think it is quite useful in being able to map out a path.

448
00:39:27,000 --> 00:39:31,000
I think it is quite useful.

449
00:39:31,000 --> 00:39:33,000
So I think this is a suit in it.

450
00:39:33,000 --> 00:39:45,000
It is fairly simple, but at the same time, quite profound teaching.

451
00:39:45,000 --> 00:39:52,000
That is the demo for tonight. Thank you all for tuning in. Have a good night.

