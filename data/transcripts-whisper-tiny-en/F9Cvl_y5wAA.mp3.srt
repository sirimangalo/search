1
00:00:00,000 --> 00:00:07,000
How would someone go about getting you to formally be their teacher? What would that entail?

2
00:00:07,000 --> 00:00:19,000
Can I get this from time to time? How to take you on as my teacher even though I live thousands of kilometers away?

3
00:00:19,000 --> 00:00:24,000
I tried doing some online courses for a while.

4
00:00:24,000 --> 00:00:32,000
We're practically speaking, there's not that much I can do for people who aren't in my immediate vicinity.

5
00:00:32,000 --> 00:00:37,000
So the obvious answer is, well, if you want someone to be their teacher, you should go to them.

6
00:00:37,000 --> 00:00:45,000
And I always think back to this movie, King of American Shaolin.

7
00:00:45,000 --> 00:00:53,000
And about this guy who goes, when I was younger, I thought it was an 80s movie, I think.

8
00:00:53,000 --> 00:00:57,000
And he goes to the Shaolin Monastery in China, I think.

9
00:00:57,000 --> 00:01:06,000
And they make him sit out in the courtyard, kind of like Fight Club, which was a 90s movie, I think.

10
00:01:06,000 --> 00:01:12,000
They make him sit out in the courtyard, they don't let him in and so he sits out in the courtyard and he waits.

11
00:01:12,000 --> 00:01:18,000
And he spends like days out there in the rain until they finally let him in.

12
00:01:18,000 --> 00:01:28,000
And I always think back to, well, maybe that's sometimes how hard it can be to get into a meditation center and find a teacher.

13
00:01:28,000 --> 00:01:46,000
That it may not be easy and sometimes expecting it to be easy or it just may not be possible to find a teacher in your immediate vicinity or be able to stay at home and find a teacher.

14
00:01:46,000 --> 00:02:01,000
And when I was looking for a teacher, I got on a plane and flew to Thailand and just started wandering and finally found the place where I began to practice.

15
00:02:01,000 --> 00:02:09,000
So, practically speaking, there's not much that can be done.

16
00:02:09,000 --> 00:02:18,000
But this is a different question. People often want to feel somehow that they formally have a teacher and that's...

17
00:02:18,000 --> 00:02:25,000
I mean, it's very traditional and ritualistic and I kind of shy away from the labels that it involves and entails.

18
00:02:25,000 --> 00:02:34,000
But in our tradition, it is common to take a teacher and they have a whole ceremony for asking someone to...

19
00:02:34,000 --> 00:02:52,000
It's just to be your teacher, but asking for the meditation training. It's called Kinkamathan, which means entering into the meditation, the state of being a meditator entering the meditation practice.

20
00:02:52,000 --> 00:02:58,000
So, we can do that. But again, not something you do over the internet, for example.

21
00:02:58,000 --> 00:03:05,000
So, I guess I'm going to go out on a limb or go out and go ahead and say that you'd have to come here.

22
00:03:05,000 --> 00:03:13,000
If you want me to take me or anyone else your teacher, the thing to do is to find that person in person.

23
00:03:13,000 --> 00:03:19,000
Visit them at the very least, visit them and have a ceremony. I mean, that would maybe be something to do.

24
00:03:19,000 --> 00:03:28,000
Do a ceremony and then give you some basic tips, pointers, instruction on meditation, and then you can go home and continue practice.

25
00:03:28,000 --> 00:03:33,000
And then, like I know you. And then, I can have an excuse to say, well, this person I've met.

26
00:03:33,000 --> 00:03:37,000
So, when they send me emails, I will respond to them.

27
00:03:37,000 --> 00:03:47,000
Because otherwise, I don't respond to all the email requests and inquiries into practice because it's just too much.

28
00:03:47,000 --> 00:03:51,000
So, that would be something that would be a step, come, meet.

29
00:03:51,000 --> 00:04:19,000
And we can even do an opening ceremony to give you the meditation practice formally.

