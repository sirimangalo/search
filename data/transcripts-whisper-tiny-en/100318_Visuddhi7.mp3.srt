1
00:00:00,000 --> 00:00:29,960
The practice of Vipasana meditation is a path, it's best thought of as a voyage or a

2
00:00:29,960 --> 00:00:47,960
journey or a means by which to attain something.

3
00:00:47,960 --> 00:00:55,200
This is important sometimes we think of meditation as a hobby or a pastime and this is

4
00:00:55,200 --> 00:01:07,520
not correct. Meditation is a means of training the mind, parallel to a similar sort of

5
00:01:07,520 --> 00:01:14,560
training that might be undertaken with the body in the hopes of attaining

6
00:01:14,560 --> 00:01:25,840
fortitude of our physical form. So in meditation practice we're practicing to

7
00:01:25,840 --> 00:01:36,760
we're training ourselves to attain fortitude of mind, strength of mind. And not only

8
00:01:36,760 --> 00:01:42,800
that but unlike the body we consider that the mind is something that actually

9
00:01:42,800 --> 00:01:52,920
undergoes changes when it's trained. Unlike the body where no matter how hard

10
00:01:52,920 --> 00:02:02,120
we train it in the end it has to return to its ordinary state and eventually

11
00:02:02,120 --> 00:02:10,920
falls apart and dies. With the mind we consider it much more like say a precious

12
00:02:10,920 --> 00:02:24,240
metal where as you work with it as you fire it up as you purify it as you work

13
00:02:24,240 --> 00:02:31,120
out all of the impurities it becomes pure. It comes to a state of perfection

14
00:02:31,120 --> 00:02:41,200
from which it doesn't recede or fall away from. So we say not only does the

15
00:02:41,200 --> 00:02:47,160
mind become well trained it becomes pure and the main reason why it becomes

16
00:02:47,160 --> 00:02:52,640
pure as opposed to just becoming strong is there is the difference between

17
00:02:52,640 --> 00:02:57,200
repass and a meditation or insight meditation and say a tranquility

18
00:02:57,200 --> 00:03:03,560
meditation which might make the mind very strong. But making the mind strong is a

19
00:03:03,560 --> 00:03:09,200
lot like making the body strong. It only lasts for some time and eventually it

20
00:03:09,200 --> 00:03:18,600
it recedes based on our whether or not we continue to put our effort.

21
00:03:19,600 --> 00:03:27,000
But we present a meditation as something quite different. It's because it

22
00:03:27,000 --> 00:03:33,160
mainly deals with insight or wisdom. Knowledge and understanding is opposed to

23
00:03:33,160 --> 00:03:41,360
simple strength and brute force and that one can attain through other types

24
00:03:41,360 --> 00:03:46,120
of meditation. We're looking for some understanding which then becomes a

25
00:03:46,120 --> 00:03:55,120
barrier to ignorance and to defilement because of course the only reason why

26
00:03:55,120 --> 00:04:05,120
one might enter into a state of mind that was unwholesome and skillful and

27
00:04:05,120 --> 00:04:11,840
helpful would be because one has misunderstanding or ignorance as to the

28
00:04:11,840 --> 00:04:19,120
unskillful unwholesome nature of that mind state. Clearly one would never

29
00:04:19,120 --> 00:04:24,320
intentionally do something to hurt oneself if one knew fully well that it was

30
00:04:24,320 --> 00:04:31,120
going to bring suffering. We always do things with the intention that they

31
00:04:31,120 --> 00:04:36,240
should bring us happiness. This maybe isn't so clear if we have a practice

32
00:04:36,240 --> 00:04:40,280
meditation this might not be as clear as once we've started to practice and

33
00:04:40,280 --> 00:04:43,440
started to watch and to understand how the mind works but actually it's true.

34
00:04:43,440 --> 00:04:48,640
Our intention is to bring us ourselves happiness even though we can do so

35
00:04:48,640 --> 00:04:56,120
many things that bring us suffering. Once we have wisdom and understanding

36
00:04:56,120 --> 00:04:58,760
we're able to see the difference and we're able to see those things that

37
00:04:58,760 --> 00:05:03,560
really and truly are useless. We're bringing about no positive benefit and

38
00:05:03,560 --> 00:05:11,080
no positive result and do away with them so that when the opportunity to

39
00:05:11,080 --> 00:05:23,960
perform those acts arises we see no reason we're unable to give rise to the

40
00:05:23,960 --> 00:05:30,760
delusion that is required to enter into such a state because of the

41
00:05:30,760 --> 00:05:35,000
understanding that is arisen. So it's not a strength exactly it comes from

42
00:05:35,000 --> 00:05:41,680
the training of the mind but once it arises it's of a whole different

43
00:05:41,680 --> 00:05:46,880
quality from single fortitude of mind. So what we're really talking about

44
00:05:46,880 --> 00:05:56,760
here is a path to realization and understanding and it's summed up by the

45
00:05:56,760 --> 00:06:03,360
expression of a path to purity because the ultimate goal of realization and

46
00:06:03,360 --> 00:06:07,880
understanding is that our minds be pure. Once we have understanding and

47
00:06:07,880 --> 00:06:11,760
wisdom then our minds will never fall back into misunderstanding and

48
00:06:11,760 --> 00:06:18,200
delusion and greed and anger and so on. So it's important as meditators that

49
00:06:18,200 --> 00:06:22,160
we understand this and we understand sort of the path and we have an outline

50
00:06:22,160 --> 00:06:28,120
of the path that we're following. This can be quite useful when we're looking at

51
00:06:28,120 --> 00:06:33,440
the path they're wondering the sort of the direction which we should be

52
00:06:33,440 --> 00:06:40,360
heading, whether we're just starting out or whether we've gone some way down

53
00:06:40,360 --> 00:06:47,240
the path already. It's very important, it's very useful for us if we have some

54
00:06:47,240 --> 00:06:52,960
kind of idea of the path, some outline of the path. So we have this outline

55
00:06:52,960 --> 00:07:01,960
and it appears in the Topitika as a dialogue between Sariputa and

56
00:07:01,960 --> 00:07:07,960
Punamantani Buddha. Punamantani Buddha. Punamantani Buddha.

57
00:07:07,960 --> 00:07:21,560
Forget exactly the name of the other man. Punamantala Buddha. Anyway, the point is

58
00:07:21,560 --> 00:07:26,560
they were talking about these seven stages of purification and this became

59
00:07:26,560 --> 00:07:33,360
the basis of the outline of this book, the Visudimaka which became a very

60
00:07:33,360 --> 00:07:41,440
influential text and sort of the core treatise on understanding the Buddha's

61
00:07:41,440 --> 00:07:54,680
teaching and putting it out together in an understandable cohesive path.

62
00:07:54,680 --> 00:07:59,760
So here I'd like to just talk a little bit about these seven purifications

63
00:07:59,760 --> 00:08:05,040
in the order that they go and this is sort of for informational purposes but

64
00:08:05,040 --> 00:08:09,840
also the sort of gauge where we are and where we should be going. So the first

65
00:08:09,840 --> 00:08:13,920
purification of the purification of morality and this is of course the

66
00:08:13,920 --> 00:08:21,240
base of the practice, the base of the path, the start of the path. The first thing

67
00:08:21,240 --> 00:08:28,480
that one should do is to purify one's morality, at least making an

68
00:08:28,480 --> 00:08:33,520
determination that as we practice this path we're going to stay away from

69
00:08:33,520 --> 00:08:39,880
various extremely unwholesome states. We're going to refrain from these

70
00:08:39,880 --> 00:08:45,160
states. These are things which are extremely, extremely unwholesome

71
00:08:45,160 --> 00:08:52,040
and sort of counter to the entire path. They're the opposite of the whole

72
00:08:52,040 --> 00:09:01,400
path. The performance of these acts is going in the opposite direction of

73
00:09:01,400 --> 00:09:06,000
where we should be heading and so this is why it's imperative that we give

74
00:09:06,000 --> 00:09:12,320
these things up. This is killing, stealing, cheating, lying, taking drugs and

75
00:09:12,320 --> 00:09:20,840
alcohol. If we can't keep at least these five rules then there's no real chance

76
00:09:20,840 --> 00:09:25,760
that we're going to be able to develop ourselves effectively since these

77
00:09:25,760 --> 00:09:32,680
things pervert and destroy and corrupt the mind. They create states of

78
00:09:32,680 --> 00:09:41,360
greed and anger and delusion which are total obstruction to the

79
00:09:41,360 --> 00:09:45,640
development of the mind. So for this reason we have to give these up and as

80
00:09:45,640 --> 00:09:48,920
meditators we generally don't have a big problem with these ones. We

81
00:09:48,920 --> 00:09:54,280
understand them and to start practicing. We also take rules like not eating in

82
00:09:54,280 --> 00:09:59,680
the afternoon, on the eating once a day, on the eating enough to survive,

83
00:09:59,680 --> 00:10:07,320
not indulging in entertainment or cosmetics or decoration and not

84
00:10:07,320 --> 00:10:13,200
sleeping on luxurious bedding or sleeping long hours. But as a base we

85
00:10:13,200 --> 00:10:17,800
understand that immoral acts are something that we have to do away with

86
00:10:17,800 --> 00:10:21,360
before we start to practice. This is the first clarification. This is sort of

87
00:10:21,360 --> 00:10:27,480
preliminary. Once we start to do that then we were able to approach the path

88
00:10:27,480 --> 00:10:34,400
which is concentration, the purification of mind it's called, purification of

89
00:10:34,400 --> 00:10:39,440
mind states because the mind is what we're going to be using to look at the

90
00:10:39,440 --> 00:10:44,320
mind, to look at the body and to look at the mind. So this is like cleaning the

91
00:10:44,320 --> 00:10:52,120
tool or sharpening the tool that we're going to be using. This is the very

92
00:10:52,120 --> 00:10:56,360
beginning of the practice before any inside arises. The next step is for us to

93
00:10:56,360 --> 00:11:01,920
start to do walking meditation, start to do sitting meditation and start to

94
00:11:01,920 --> 00:11:07,480
really approach the phenomenon that arise in the present moment and focus on

95
00:11:07,480 --> 00:11:13,800
them. So we use this mantra, this tool that we have and so the second purification

96
00:11:13,800 --> 00:11:22,080
is simply training ourselves to be proficient in using this mantra, using

97
00:11:22,080 --> 00:11:27,320
this meditation practice, so watching the stomach rise as we're able to focus

98
00:11:27,320 --> 00:11:35,000
on reality, focus on the objects as they arise. Our mind becomes sharper and

99
00:11:35,000 --> 00:11:41,400
clear and this is called the purification of mind. What it means is that we don't

100
00:11:41,400 --> 00:11:47,560
have many contrary states that might obstruct our practice. So all of the

101
00:11:47,560 --> 00:11:54,280
states associated with evil acts or bad acts come into play here and we're

102
00:11:54,280 --> 00:11:57,720
trying to do away with them. When we first sit down sometimes we have

103
00:11:57,720 --> 00:12:02,480
greed so we want this or want that or we like this or like that and it distracts

104
00:12:02,480 --> 00:12:09,960
us from the purpose. Or we have anger or sadness or depression or frustration

105
00:12:09,960 --> 00:12:17,040
or boredom, any of the many negative emotions and this is also a distraction

106
00:12:17,040 --> 00:12:24,480
or an obstruction to the path. Sometimes we feel a drowsy or lazy when our

107
00:12:24,480 --> 00:12:30,960
minds are not unwieldy and this becomes an obstruction or we have a

108
00:12:30,960 --> 00:12:36,280
distraction, our minds are not focused and we're thinking about so many

109
00:12:36,280 --> 00:12:41,760
different things thinking about the past, the future thinking. I'm about far in a

110
00:12:41,760 --> 00:12:47,960
way useless things our mind is not yet focused and the last one is we have

111
00:12:47,960 --> 00:12:54,400
doubts. We have doubts about the practice doubts about ourselves and

112
00:12:54,400 --> 00:13:01,440
unsureities, uncertainties in the mind. As we start to practice our practice in

113
00:13:01,440 --> 00:13:05,560
the beginning is simply doing away with these things. So when we acknowledge

114
00:13:05,560 --> 00:13:09,880
even directly acknowledging them, we're saying to ourselves like King Mikey

115
00:13:09,880 --> 00:13:16,600
disliking, disliking, disliking, angry, drowsy, distracted, we're doubting, worried

116
00:13:16,600 --> 00:13:22,680
us on. When we do this we're focusing the mind, we're teaching the mind how to

117
00:13:22,680 --> 00:13:30,480
focus on objects as opposed to how to make more of things and they are

118
00:13:30,480 --> 00:13:39,480
or proliferate, turn small things into big things or get into these loops where

119
00:13:39,480 --> 00:13:44,720
we interpret. We're getting away from interpreting things to actually

120
00:13:44,720 --> 00:13:49,320
understanding them and all of these things are of course interpretations of

121
00:13:49,320 --> 00:13:53,720
liking or disliking and so on. So we're getting away from that and we're

122
00:13:53,720 --> 00:13:59,160
building up a focus that will allow us to see things simply as they are, see

123
00:13:59,160 --> 00:14:05,040
things clearly as they are, see how our mind works. I had an argument some time

124
00:14:05,040 --> 00:14:11,840
ago with some materialists and my argument basically started off and ended

125
00:14:11,840 --> 00:14:17,040
with the idea that meditation could be a scientific inquiry which of course

126
00:14:17,040 --> 00:14:21,800
was totally unacceptable to them and sort of ended the conversation right then

127
00:14:21,800 --> 00:14:27,480
and there. But that's actually what meditation is and it's scientific in

128
00:14:27,480 --> 00:14:31,640
that we can really understand how the mind works and no matter what anyone

129
00:14:31,640 --> 00:14:35,880
else tells you once you've practiced meditation you know for yourself that

130
00:14:35,880 --> 00:14:39,560
that's how the mind works and the great thing is if anyone else comes in

131
00:14:39,560 --> 00:14:43,440
as the same practice they will say the same thing. If they're honest and

132
00:14:43,440 --> 00:14:48,560
sincere about their practice they too will obtain the same results. They'll be

133
00:14:48,560 --> 00:14:51,880
able to say that this is how the mind works and it's very clear it's not

134
00:14:51,880 --> 00:14:56,040
something where you could say well maybe you're deluding yourself or maybe

135
00:14:56,040 --> 00:15:00,800
you're it's an illusion or so on. It really is how the mind works and it's

136
00:15:00,800 --> 00:15:05,280
how reality works and it's much more real than anything that we can ever test

137
00:15:05,280 --> 00:15:10,560
externally from external to ourselves. So it is really a scientific inquiry

138
00:15:10,560 --> 00:15:17,920
it allows us to give rise to knowledge which is of course what it means what

139
00:15:17,920 --> 00:15:27,560
science means we're able to learn to know things verifiably know things and so

140
00:15:27,560 --> 00:15:32,800
this is what why we're doing this practice this is what we're hoping to gain

141
00:15:32,800 --> 00:15:37,760
from our focus and our concentration on say the rising and the falling of the

142
00:15:37,760 --> 00:15:43,080
abdomen we're going to slowly look at when what we are. So this leads to a

143
00:15:43,080 --> 00:15:49,680
whole slew of knowledge that come in succession there's altogether 16

144
00:15:49,680 --> 00:15:52,320
knowledge and they're broken up into the purifications. I won't go into the

145
00:15:52,320 --> 00:15:55,800
knowledge it's just basically going over the purifications here but the rest

146
00:15:55,800 --> 00:15:59,920
of them are involved with wisdom and understanding. First of all we gain right

147
00:15:59,920 --> 00:16:06,160
view when meditators come to do reporting sessions so apart from our group

148
00:16:06,160 --> 00:16:11,600
session then we have the daily one-on-one group session. This is a time where

149
00:16:11,600 --> 00:16:17,440
we'll have a chance to to assess a person's view of person's understanding the

150
00:16:17,440 --> 00:16:22,920
way they look at the world their outlook online on reality and we'll be able

151
00:16:22,920 --> 00:16:28,040
to see whether that outlook is in line with reality is in line with their

152
00:16:28,040 --> 00:16:32,120
experience are they actually experiencing anything or are they just going by

153
00:16:32,120 --> 00:16:39,600
view. So this is the sort of the gateway to wisdom is this movement away from

154
00:16:39,600 --> 00:16:46,080
views that we've been told or we've accepted based on faith or based on here

155
00:16:46,080 --> 00:16:53,360
say based on our teachers or so on to an experiential view of reality where

156
00:16:53,360 --> 00:17:00,680
we understand reality based on our experience based on the way it actually

157
00:17:00,680 --> 00:17:08,680
appears the way it actually works and so our view will not be counter to what

158
00:17:08,680 --> 00:17:15,560
is actually being experienced they won't conflict. This is a very basic one

159
00:17:15,560 --> 00:17:19,120
and it's one that has to be tested we're actually testing people because if

160
00:17:19,120 --> 00:17:24,280
they can't get this one then there's no chance that they can continue on with

161
00:17:24,280 --> 00:17:29,360
the other ones. Once we get the view our view straight and we actually are

162
00:17:29,360 --> 00:17:34,200
getting into this experiential view of reality then we can start to come to

163
00:17:34,200 --> 00:17:38,440
understand experience we can understand how our mind works and come to work

164
00:17:38,440 --> 00:17:44,760
a lot of unwholesome things out. So the next one is our understanding of

165
00:17:44,760 --> 00:17:50,040
karma the next one is the purification by overcoming doubt and here it's

166
00:17:50,040 --> 00:17:56,880
doubt in regards to cause and effect what is what is a cause for what and what

167
00:17:56,880 --> 00:18:01,720
is the effect of what what is the cause of happiness and what is the cause of

168
00:18:01,720 --> 00:18:05,600
suffering. This is the first and most important wisdom that we start to gain

169
00:18:05,600 --> 00:18:10,680
after our view is after we start to see things correctly then we start to look

170
00:18:10,680 --> 00:18:18,240
and and when they come to understand the phenomena that arise and see in the

171
00:18:18,240 --> 00:18:22,760
present moment and the first thing we realize is that when you have a unwholesome

172
00:18:22,760 --> 00:18:26,720
mind state it leads to suffering when you have a wholesome mind state it leads

173
00:18:26,720 --> 00:18:31,440
to happiness. So we start to see how the not only that but we see how the whole

174
00:18:31,440 --> 00:18:35,960
body and mind system works based on cause and effect everything we do is cause

175
00:18:35,960 --> 00:18:40,600
and effect. We want to stand up then we stand up the one thing comes first and we

176
00:18:40,600 --> 00:18:45,000
can see that that's totally separate from the actual standing up. Yeah separate

177
00:18:45,000 --> 00:18:50,240
in the sense that it is an entity in an of its own which then gives rise to

178
00:18:50,240 --> 00:18:56,560
the effect of standing up. Sometimes when we walk then once we've already walked

179
00:18:56,560 --> 00:19:01,280
or once we've already done something then we see the we we realize it and so we

180
00:19:01,280 --> 00:19:05,920
see that the realizing is the effect and the movement is the cause. It works

181
00:19:05,920 --> 00:19:10,560
in both ways sometimes the body is the cause sometimes the mind is the cause but

182
00:19:10,560 --> 00:19:16,040
we see this and we see more importantly what are good and bad causes for good

183
00:19:16,040 --> 00:19:28,800
and bad results for beneficial useful results. Once we start again once we start

184
00:19:28,800 --> 00:19:32,120
to understand in this way this is when the mind starts to change and this is

185
00:19:32,120 --> 00:19:37,480
when we start to realize that we really had a great grave misunderstanding and

186
00:19:37,480 --> 00:19:42,480
that our minds are really as a result in a great turmoil and so we start to

187
00:19:42,480 --> 00:19:47,640
see that this is this is why we're doing what we're doing we start to gain

188
00:19:47,640 --> 00:19:54,360
this great faith in what we're doing and this is to really make a profound

189
00:19:54,360 --> 00:19:58,440
change on our minds up until this point we may have thought that meditation was

190
00:19:58,440 --> 00:20:01,080
just going to show us things that we already knew and at this point we start

191
00:20:01,080 --> 00:20:06,040
to see things that we didn't already know and so the next one is to see what

192
00:20:06,040 --> 00:20:09,240
is the path and what is not the path because at this point we start to see

193
00:20:09,240 --> 00:20:12,800
that there there is actually something that needs to be done and so we start

194
00:20:12,800 --> 00:20:20,800
to move towards an enlightenment move towards a state which is totally

195
00:20:20,800 --> 00:20:23,520
unlike the state that we're in because we see that the state that we're in

196
00:20:23,520 --> 00:20:27,520
is really a mess it's all jumbled up and there's all sorts of defilements

197
00:20:27,520 --> 00:20:34,800
that are or unholes of states that are sort of running the show so at this

198
00:20:34,800 --> 00:20:43,720
point as we start with this point we start to make a real determination to

199
00:20:43,720 --> 00:20:48,640
set out on the path and this is where we start to separate what is the right

200
00:20:48,640 --> 00:20:55,120
path from what is the wrong path as we start to practice in earnest we're going

201
00:20:55,120 --> 00:21:02,040
to start to give rise to all sorts of all sorts of potential paths because

202
00:21:02,040 --> 00:21:05,720
there will be all sorts of altered experiences arising which we might say

203
00:21:05,720 --> 00:21:09,000
okay well that's the altered state that I'm looking for that's the new that's

204
00:21:09,000 --> 00:21:13,040
enlightenment there's some people will see bright light some people will

205
00:21:13,040 --> 00:21:18,040
hear sounds some people experience great peace and tranquility that they've

206
00:21:18,040 --> 00:21:21,920
never felt before some people have great states of bliss some people have

207
00:21:21,920 --> 00:21:30,480
have states bodily states of rapture elation or or static charge in their

208
00:21:30,480 --> 00:21:39,080
bodies some people have profound insights into worldly things or into

209
00:21:39,080 --> 00:21:44,160
Buddhism or into meditation or so on some people have great energy arise

210
00:21:44,160 --> 00:21:49,600
there are many different types of states like this which are all taken by the

211
00:21:49,600 --> 00:21:55,920
meditators at this point to be the path and without a teacher to explain what

212
00:21:55,920 --> 00:22:02,800
is the right path or to remind the meditators of what is the right path it's

213
00:22:02,800 --> 00:22:09,200
incredibly common it's it's the greater likelihood is that a meditator at

214
00:22:09,200 --> 00:22:13,440
this point will become stuck because they will go down the wrong path a path

215
00:22:13,440 --> 00:22:17,000
that is just a dead end and needs nowhere following after light through

216
00:22:17,000 --> 00:22:20,480
colors or pictures that they see following after special experiences of any

217
00:22:20,480 --> 00:22:26,520
kind and not being objective not seeing things as they are so the path of

218
00:22:26,520 --> 00:22:29,600
course is simply seeing things as they are and this is where the with the

219
00:22:29,600 --> 00:22:34,360
meditator I've learned in as they progress on but they come to this

220
00:22:34,360 --> 00:22:38,960
realization is understanding eventually from through a long time or or even a

221
00:22:38,960 --> 00:22:42,560
quick time if the teacher is able to catch them and to explain to them clearly

222
00:22:42,560 --> 00:22:45,880
they're able to understand what is the right path and they're able to

223
00:22:45,880 --> 00:22:50,320
continue on so this is the next purification that's that comes next in line and

224
00:22:50,320 --> 00:22:55,360
this is a very important part of the path after one it understands what is the

225
00:22:55,360 --> 00:23:03,240
right path this is the fifth purification or say the fourth the fifth

226
00:23:03,240 --> 00:23:10,120
purification that's right fifth purification after that then it's it's pretty

227
00:23:10,120 --> 00:23:14,840
much smooth going you could say that once the meditator gets this

228
00:23:14,840 --> 00:23:18,080
understanding of what is the path and what is not the path then it's pretty

229
00:23:18,080 --> 00:23:21,040
clear that they're just going to sail through the rest of the of the course

230
00:23:21,040 --> 00:23:26,160
and rest of the path and from that point on they're on the right path just like

231
00:23:26,160 --> 00:23:32,840
a like that these curling stones when they throw the curling stone out on the

232
00:23:32,840 --> 00:23:39,760
ice and from Canada you have curling this is sport when our bowling you can

233
00:23:39,760 --> 00:23:44,280
say we're from the beginning you have to aim the ball once you let it go

234
00:23:44,280 --> 00:23:48,480
and it just goes had their straighter or crooked but once you get the get

235
00:23:48,480 --> 00:23:53,200
rolling in the right direction then it's in a groove and most of people are

236
00:23:53,200 --> 00:23:58,520
able to continue on and realize that the freedom from from the

237
00:23:58,520 --> 00:24:06,200
fountains out of it or realize the purity and understanding so the next

238
00:24:06,200 --> 00:24:10,000
purification is simply the purification of practice once one is on the right

239
00:24:10,000 --> 00:24:16,520
path and is is as is understanding not to go down these wrong paths then it's

240
00:24:16,520 --> 00:24:21,680
just a matter of continuing to practice and not giving up not going home not

241
00:24:21,680 --> 00:24:27,960
slacking off because at this point you know what we know what the practice is

242
00:24:27,960 --> 00:24:31,880
we know where we're going and all we have to do is just continue on so at this

243
00:24:31,880 --> 00:24:35,080
point many knowledge is will arise and they come in succession slowly

244
00:24:35,080 --> 00:24:40,560
basically slowly slowly coming to see that whatever it was that we were

245
00:24:40,560 --> 00:24:43,960
holding on to and for everyone this is different all of the many things that

246
00:24:43,960 --> 00:24:49,560
we grasp when we cling to and we wish for when we seek out slowly coming to

247
00:24:49,560 --> 00:24:58,520
realize that these things are actually void of any lasting satisfaction

248
00:24:58,520 --> 00:25:03,320
lasting peace or happiness that they're actually real cause for suffering

249
00:25:03,320 --> 00:25:10,760
and stress for us as we seek them out and can be a cause for true and lasting

250
00:25:10,760 --> 00:25:20,880
displeasure and sadness and sorrow and and suffering and once we start to

251
00:25:20,880 --> 00:25:26,440
realize this then the mind really starts to become lifted out of this

252
00:25:26,440 --> 00:25:31,800
and the situation so this is this is just a description of the everyday

253
00:25:31,800 --> 00:25:34,720
practice and this is what we're going through through this practice we start

254
00:25:34,720 --> 00:25:39,320
with the walking continue with the sitting walking and sitting walking and

255
00:25:39,320 --> 00:25:44,600
sitting and we start to see that even our own bodies all of the thoughts that

256
00:25:44,600 --> 00:25:49,440
we have all of the things that make us happy all the things that we cling to

257
00:25:49,440 --> 00:25:53,760
just coming to see them for what they are and see that there's really nothing

258
00:25:53,760 --> 00:25:57,960
special about them there's nothing special about the good things there's

259
00:25:57,960 --> 00:26:02,640
nothing wrong with the bad things and we start to see that everything that arises

260
00:26:02,640 --> 00:26:07,560
is just a thing these are just things just phenomena that arise and sees

261
00:26:07,560 --> 00:26:12,600
we generally explain this as being the understanding of three things impermanence

262
00:26:12,600 --> 00:26:17,960
that the things that we thought were permanent and lasting or the things that

263
00:26:17,960 --> 00:26:21,960
we didn't realize we're going to just disappear the realization that they do

264
00:26:21,960 --> 00:26:26,760
disappear so whether it be good things or bad things so when bad things arise

265
00:26:26,760 --> 00:26:30,160
we get angry not realizing that it's just something that's come for a short

266
00:26:30,160 --> 00:26:35,720
while and it's going to go away and with good things not realizing that we can't

267
00:26:35,720 --> 00:26:42,320
hold on to them we can't cling to them number two realizing suffering that

268
00:26:42,320 --> 00:26:46,240
the things that we thought were happiness are actually in fact a cause for

269
00:26:46,240 --> 00:26:51,840
stress and suffering when they disappear when we don't get them and so on and the

270
00:26:51,840 --> 00:26:54,600
bad things are suffering only when we cling to them realizing that the

271
00:26:54,600 --> 00:26:59,920
suffering is because we need for it to be other than it is and when it can't be

272
00:26:59,920 --> 00:27:06,320
that way then we suffer and number three realizing non-self or

273
00:27:06,320 --> 00:27:11,640
uncontrollability of phenomenon that all things that arise they come and go of

274
00:27:11,640 --> 00:27:19,800
their own nature and there's no master there's no person or no ego sitting up

275
00:27:19,800 --> 00:27:24,240
they're saying let it be this let it not be that let it not be that that's

276
00:27:24,240 --> 00:27:29,200
sure the mind can be a cause for for whatever effect we might wish for

277
00:27:29,200 --> 00:27:33,560
we wish for something and we put up the right causes and yes surely it can

278
00:27:33,560 --> 00:27:38,280
come but there is no master in control it's not we're not that in control of

279
00:27:38,280 --> 00:27:42,800
what arises saying that we can keep it forever and it's going to suddenly be

280
00:27:42,800 --> 00:27:48,800
ours so not to say that there is no self it's to say that there is a there's

281
00:27:48,800 --> 00:27:52,720
no master we are not in control of the things that arise there is a self in

282
00:27:52,720 --> 00:27:59,360
a sense of being a cause the mind being a cause for the next sequence of events

283
00:27:59,360 --> 00:28:04,000
and going in step step by step to the realization of whatever whatever goes

284
00:28:04,000 --> 00:28:09,840
one might have but there is no self in the sense of being a master of the

285
00:28:09,840 --> 00:28:15,160
phenomenon that phenomenon arise and this goes on and on and on in great

286
00:28:15,160 --> 00:28:22,520
detail as our mind becomes more and more sensitive and more more tuned into

287
00:28:22,520 --> 00:28:28,200
reality we find ourselves giving up and letting go and freeing ourselves from

288
00:28:28,200 --> 00:28:32,080
these addictions and attachments and the versions that we've clung to

289
00:28:32,080 --> 00:28:37,040
for so long and we've kept inside for so long until finally we reached the

290
00:28:37,040 --> 00:28:42,480
seventh purification which is really the goal what we're looking for is this

291
00:28:42,480 --> 00:28:46,400
realization it's not exactly the goal but it's the final purification it's

292
00:28:46,400 --> 00:28:53,440
the one that leads to the goal the purification of knowledge and vision it's

293
00:28:53,440 --> 00:28:57,920
what it's called I mean specifically knowledge and vision of the truth of the

294
00:28:57,920 --> 00:29:03,120
four noble truths you could say or simply of reality finally getting it and

295
00:29:03,120 --> 00:29:08,040
realizing what things for what they are just giving up this old way of looking

296
00:29:08,040 --> 00:29:12,840
at things and suddenly it's kind of like an epiphany just realizing that there's

297
00:29:12,840 --> 00:29:20,160
nothing worth clinging to this idea of clinging of seeking out is not not the

298
00:29:20,160 --> 00:29:24,720
proper way to do one's life it's not the proper way to approach reality and

299
00:29:24,720 --> 00:29:30,120
giving up and so this is the entering into freedom from suffering when the

300
00:29:30,120 --> 00:29:36,600
mind enters into Nirvana or into this state of peaceful cessation where

301
00:29:36,600 --> 00:29:41,600
there is no arising of suffering and the subsequent

302
00:29:41,600 --> 00:29:46,880
subsequent understanding of what is the right what is the wrong what is

303
00:29:46,880 --> 00:29:51,800
useful and what is useless of what is real happiness and what is fake

304
00:29:51,800 --> 00:29:57,680
happiness so we're really aiming for this high state of understanding is once

305
00:29:57,680 --> 00:30:05,200
this understanding comes about and we can we can say that we will we will

306
00:30:05,200 --> 00:30:09,760
have no suffering either now or in the future whatever unpleasant phenomena

307
00:30:09,760 --> 00:30:14,800
arise and they will not be able to bring about states of suffering in our

308
00:30:14,800 --> 00:30:20,920
mind or distress in our mind and this comes about as you can see it comes

309
00:30:20,920 --> 00:30:25,200
about through a path or training and the training it's not necessary to

310
00:30:25,200 --> 00:30:30,680
memorize these these stages but a clear understanding of them that slowly

311
00:30:30,680 --> 00:30:34,920
slowly we're going to come to change our way of looking at things we're going

312
00:30:34,920 --> 00:30:41,960
to open up our minds and we're not going to be so rigid or dogmatic or clingy

313
00:30:41,960 --> 00:30:51,600
to views or to ourselves or to objects of pleasure and displeasure and be

314
00:30:51,600 --> 00:30:57,480
able to see things objectively thrown on us a lens of understanding that wasn't

315
00:30:57,480 --> 00:31:01,680
there before and we'll be able to see things simply for what they are which

316
00:31:01,680 --> 00:31:06,800
really most of us can't do in our everyday life that we see things through

317
00:31:06,800 --> 00:31:12,360
colored lenses either as good or as bad we judge things based on our views and

318
00:31:12,360 --> 00:31:17,160
our beliefs and so on we don't judge things based on what they are we don't

319
00:31:17,160 --> 00:31:21,920
see things from up there so this is something that is I think quite useful

320
00:31:21,920 --> 00:31:25,760
for meditators to understand and kind of gives us a picture of where we're

321
00:31:25,760 --> 00:31:31,120
going and hopefully gives us encouragement to continue on to this in this

322
00:31:31,120 --> 00:31:37,120
most useful tasks this most beneficial result that we're all aiming for

323
00:31:37,120 --> 00:31:42,160
and striving for so that's the demo that I wish to give tonight and now we'll

324
00:31:42,160 --> 00:31:45,840
continue on with the meditation portion starting with mindful

325
00:31:45,840 --> 00:32:04,240
frustration and then walking and then sitting

