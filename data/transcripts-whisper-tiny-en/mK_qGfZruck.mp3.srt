1
00:00:00,000 --> 00:00:12,100
Consciousness is not considered non-local, as I understand non-local in quantum physics.

2
00:00:12,100 --> 00:00:27,180
Consciousness is considered undescribed by quantum physics, which is outside of physics, outside of what is

3
00:00:27,180 --> 00:00:41,580
indescribable by physics, except as being what, what is the guy with the processes anyway?

4
00:00:41,580 --> 00:00:48,380
Process 1, it is considered to be the process 1, the decision making, this whole thing that got everyone up in

5
00:00:48,380 --> 00:00:54,780
arms about how it all depends on which decision you make. If you decide to do this experiment or if you decide to do

6
00:00:54,780 --> 00:01:04,780
that experiment, actually determines the framework of what you are talking about.

7
00:01:04,780 --> 00:01:14,780
So, actually to me it sounds a lot like dualism, with Henry Stapp and with Orthodox quantum physics

8
00:01:14,780 --> 00:01:21,480
discusses there is process 1, process 2, process 2 is that which can be calculated in the

9
00:01:21,480 --> 00:01:34,480
described by quantum physics, process 1 is what initiates, process 2, and so that is quite dual, actually.

10
00:01:34,480 --> 00:01:45,480
But Stapp actually, if you look at his articles on, he has an article on non-dualism, for example,

11
00:01:45,480 --> 00:01:52,480
and he says, there is no reason to think of it as being dualistic in a sense of two different entities.

12
00:01:52,480 --> 00:02:00,480
It could be just separate poles, I think he says, you have an experience and you have the two aspects of it.

13
00:02:00,480 --> 00:02:12,480
There could be just aspects of the experiment experience, which, I mean, what I don't get is why people are so fixed on finding

14
00:02:12,480 --> 00:02:19,480
something non-dualist, I guess it just kind of makes you feel good to think that it can all be reduced to one thing.

15
00:02:19,480 --> 00:02:30,480
And I don't really see that, I understand how it is kind of nice to think of, but I don't see how it is any more logical or rational.

16
00:02:30,480 --> 00:02:40,480
It is just kind of this reductionist, or I don't know, this attempt by science, they are trying to find the grand unifying theory of everything.

17
00:02:40,480 --> 00:02:48,480
They want to get it down to one, which, I don't really see the point, it is what it is, right?

18
00:02:48,480 --> 00:02:56,480
Whether it's one or two, it is what it is, and that's much more important than counting, or it's actually many.

19
00:02:56,480 --> 00:02:59,480
There was an argument between these two monks.

20
00:02:59,480 --> 00:03:02,480
One said there was three monks or two people anyway.

21
00:03:02,480 --> 00:03:06,480
One said there are three kinds of feeling, and the other one said there are five kinds of feeling,

22
00:03:06,480 --> 00:03:12,480
and then the Buddha came to the Buddha and asked him who was right, and the Buddha said there are 108 types of feeling or something.

23
00:03:12,480 --> 00:03:36,480
Just to show them they were being stupid.

