You
You
You
You
You
You
You
You
Good evening, everyone welcome to our live podcast
Okay, tonight we're looking at a word
I
Should be
Gradably familiar. It's probably one of the few
Sanskrit poly words that's more familiar to people than karma
What could be more familiar to people than the word karma, right?
I don't know if there's even another word. Maybe Buddha is Buddha more familiar to people than karma
But this one's definitely in in running yoga
You'll get well-known word right
I don't think it's what most people don't know don't have a clue with the word yoga means
Yoga is an interesting word
It appears to be one of those few words that
He's actually the same in
In Paul in English or not the same but has a very close cognate
Yoga what does yoga sound like
What English word does yoga sound like
Yoke
Yoga sounds like yoke
Not the egg variety, but the other kind of yoke the yoke that we don't need to do not actually
We don't use that much in English some people I assume are not very familiar with this word a yoke a yoke is the thing that
You put on the shoulders of an ox
I'm not familiar with it actually all that much. I know it more from Buddhism than anywhere to yoke
Someone in the yoke and ox is yoke to a cart and yoke is something that you put on the shoulders in it
It's what
Because the stocks would have big shoulders
That was enough to get them to pull the cart put this on their shoulders
So whether that's the original meaning or not, that's one of the meanings
Then a meaning is bondage a bond
Right it's the same idea right? Yeah, yoke is something that ties you but
It ends up being used for bondage like when you're attached to something
And so a Buddhism there are four yoke's
Then I'm good things
You know, it's not necessarily a good thing now. I don't know much about the traditional modern yoga
But where it comes from but I guess my understanding is that a yoga in a good sense means being bound to something
Being kind of like dharma actually dharma means to keeping a code
That's how it was used in the time of the Buddha everyone had their dharma the warriors had their dharma
The brahmins had their dharma was code that they live in by so yoga is something that you
It's like a red gym in or a schedule
right a prescription
where someone has a
Training schedule weightlifting or exercising
Or anything that they were tied to so yoga was one exercise one type of
Regimen of exercise
But I guess became spiritual or whatever
Kind of like Tai Chi or something
Something that you do people whose practice karate they stick by it
So they're bound to it
But the four yoga's in Buddhism are something quite different
They refer actually to things that bind us the ties that bind
But there are things that keep us hold us back
keep us
And snared I have a pull on us
Keep us from being free, you know, you want to be free at peace does not be subject to
Forces that can mean due to suffer I guess the real problem with bondage is that you're
You're subject to the
External force that
You're tied down
You're not free
And when you get tied to something and you're stuck with it, you're stuck with whatever comes along with it
And so the four yoga's are kama yoga
So you know, of course, what's the biggest bondage that we can think of in Buddhism is kama which means sensuality
sensual bondage is a
It's an incredible force
Number two bhava yoga bhava means
existant
Number three dikti yoga
The bondage views and number four a wimjaya yoga
The bondage of ignorance
And this is an important teaching especially for meditators
So listen up
The suit goes on in a way that I'm not I'm gonna rather try to explain these things
No way the suit it
They can
Talk a little bit about what the suit says or we'll start with there the suit to
explains the problem
And and the reason why we're bound to these things is is based on a formula that we've already heard recently
the three aspects of
any kind of attachment which I've talked about quite recently the gratification
The danger and the escape
So there's a gratification that comes with each of these there's a danger to each of them and there's an escape from each of them
And
Understanding these three is importantly
It's important to understand the gratification of each of these
But then it's also important to see the danger some people and see only the gratification
And seeing the danger of course isn't enough you have to also once you see the danger you have to realize it's worth escaping and find the escape
That's a true understanding
Let's explain what they are so the bond of sensuality is of course fairly self evident
Maybe the strength of it really isn't we don't realize how tied we are to the things that we love the thing that we like
Whenever when I loved one dies or leaves
Or betrays us
We've been acts in a way that's undesirable
And then we see some of the problem. We see how strong is our attachment
They say there's a fine line between love and hate and it's really because when you love someone
Well the word the word love we use this word to describe our attachments to people and our attachments are to
Just that beings or people
And a person isn't something that exists a person is a concept that we have in our minds based on the experiences that we have
Surrounding the other individual
But we build up that individual is just made of
momentary experiences
momentary events
Seeing hearing smelling just thinking we cling instead to the idea of that person when we have this idea any time that
It's not actually in line with the truth of that individual
We suffer
They do something we don't like they say something we don't like or they just leave or they die
It's not
That's not what we love about that person
I think things we love about people mostly have to do with them being alive
mostly
So when they die that's or when they disappear most of the things we love about them have to do with them being present
That's a good example of how
How we strongly reclaiming
But we cling to so many things
Site sounds, smells, tastes, feelings, thought
You're cling to all of our experiences and they're habitual
You can't cling and say well, I'm going to like it but not cling to it
The more you like something the more it builds up in your mind
Until you need it you can you feel uncomfortable without it
And you're bound to it
Find yourself crazy about these things
You need it
Let the calm of yoga
Bava yoga is
Bava yoga is a bit different
But we're bound to it nonetheless
It's existence
So existence means being bound to something being or to being something
And bound to the idea is to ideas mostly
If you're famous you get caught up in the idea of being famous
It's pleasant to you
If you have a rank and they're caught up in the rank if you have a job you're caught up in the job
If you want a promotion you get caught up in the idea of a promotion
You get caught up in ideas of things
The existence being something
One thing to be this, one thing to be that
One thing to be born in heaven
It's a big one for Buddhists
Many Buddhists become caught up in the idea of being reborn in heaven
Of course that also has to do its sensuality
And they get caught up in the idea of being a man, being a woman, being a monk
Being a teacher, being a doctor, being a lawyer, being a president
The head of something else
They also get caught up in ideas of existence that we don't like
Being something that we don't like being too short, being too tall
Being poor, being
Being something, being a man, being a woman
If some men want to be women, some women want to be men
You cut up in being
Not liking or being, liking or being attached to it
Very attached to who we are
It's number two, number three, Ditti yoga
Ditti is any type of view
They are caught up in our beliefs, beliefs are incredible
Bond, they tie you down
See this in meditation, someone who comes into meditation with strong beliefs that are counter to meditation
Practice like there's a soul, there is a God who brings us deliverance
They can't really progress in meditation
And I think that there's an all-powerful being out there who's going to save them
They can potentially seem to have incredible difficulty because they're not really concerned about their problems
It's okay because when we die, God saves us
The idea of a soul makes it very difficult
To break your soul up into what really exists
Belief in belief in things like being
The inefficacy of karma, the idea that there is no results to our deeds
You can do lots of evil things and nothing bad happens to you, that's incredible
We've had slavery, we've been enslaved and it's a terrifying prospect
We have views that there's no one we need to be grateful to those who've helped you like your father or your mother
If your father or mother have actually raised you and cared for you
They get you caught up in great difficulty
Your mind becomes quite coarse
Through your views, the belief that we only live once, that when we die there's nothing
It's quite an enslaving view because
It gets you, it removes so much potential
If that were the view, then what would you do?
Actually if you there's more to that view usually than just that because the truth is it's still more
More pleasant to be a good person, but most people don't take it that way
If they take it as an opportunity to do all sorts of evil in the botch and undertake great debauchery because
There's no consequences
Of course there are one of the consequences even in this life
Then the less our views, our beliefs, enslave us
Our views and beliefs and our views and beliefs that are cultural as well
The views about marriage
views about
gender roles
views about races, the various different types of human beings
views about your country and
Lots of different views, views about your role as a father or mother
son and daughter and you get you really tied up
Unless you're able to see the truth
The views can be quite dangerous because when they're not in line with reality
They get you in quite a bit of trouble and reality rears its ugly head
Number three and number four, a wije yoga the bondage that is ignorant. This is a clever one. I don't think
I suppose it is talked about outside of Buddhism
But it's an important one the real bondage is just ignorant
This is really the key in Buddhism
If we but knew
All of these things but knew the truth
There'd be no bondage there'd be no clinging
There'd be no greed no anger
There'd be no war no conflict
There would be no attachment or addiction
There'd be no hate surrender
There'd be no suffering
There'd be no suffering
It's a key in Buddhism all that it's a quite a claim really and it doesn't it's not self-evident because we're so ignorant
But suffering only comes because of our reactions
You can't suffer unless you get upset about something
If you can completely be free from that
So that you see things just as they are and do you see
Through your reaction you see the endangering all these
reactions to things
Then you stop you stop reacting
You see that nothing's worth clinging to nothing is worth getting upset about
If and when you can finally see things as they are is just coming and going and not worth clinging to
There's no suffering
That's what we tried to do in meditation right try to see things as they are
So we don't react it's basically it
Teach ourselves a new way a way of being
that isn't reactionary
It's just a piece not sure
Seeing things as they are
So those are the four I won't go into any more detail that's enough before
Yoga is not good. You see the four problems our practice is about overcoming all four of these
So that's the number for tonight
Two new meditators here which means we have three all together because the cool is still here
But Mark is here
From the UK
Right
You can go ahead not for the questions. I don't have your meditators thing because
It's usually pretty broad based
Okay, let's get back to our broadcast here. We're broadcasting. Hopefully everything's going well
It's turn on Robin's audio
Check out our live dashboard at a bunch of chatting
Ready for questions. I'm never sure if I'm going to regret looking at the YouTube comments
All right, go for it. Hi. How do I log in my meditation sessions?
I think that was probably just meant for the YouTube or the meditation community, but hopefully it wasn't already answered
Let's see what it's okay. I mean there's pretty easy. You have to log in and then there's
It is yeah, I'm just look at the bottom of the screen and there's a place to type in your minutes of walking and
Sitting on the meditation tab
Get out of the chat
Yes
When focusing on an object is it necessary to observe all the details or is just noting the existence experience of it enough
Good question. I mean I do answer this quite. I've answered this recently, but
You're actually not supposed to observe the details. I mean if you're aware of the details
Don't don't pay attention to them
None you meet a gahi nana bian julagahi anu bian julamis details. Don't grasp at them
Just see things as they are the details are what gets you in the problem into difficulty
Seeing is just seeing, hearing is just hearing, keep it simple.
Just remind yourself that it's seeing, so you're self-seeing.
One day, some of the imperfections of insight seem identical to some of the seven awakening factors.
Is this because the awakening factors are signs we are practicing correctly
and should not be purposefully developed, accepting mindfulness persistence
and possibly analysis of qualities?
No, it's that these things can lead you
when they get strong, right?
Like mindfulness, the word is...
Patan, I think.
But the idea there is that it's strong mindfulness.
Your mindfulness gets good, and it leads you to negligence very easily.
You say, oh, I'm doing very well, and you get kind of confident.
Because you get confident, you start being mindful,
and you can easily get caught up in that.
I do more, so confidence.
I think, I know, I do more.
I say, I do more kind.
I give them many confidence as one of them, so you have great confidence, which is a good thing.
But it becomes obsessive, so not obsessive, but you get caught up in it.
The point is, even good things, these are all good things.
Actually, all 10 of the...
Even obese, all of these are positive.
Assign that your mind is focusing.
But you get caught up in them.
And you start practicing.
You start practicing based on any good thing that comes up.
Like with mindfulness, when it gets very strong,
you maybe start noting quicker and quicker.
You had one meditator recently who did that,
and you didn't know where to go next.
You said, I'm getting so quick, how do I?
Yeah, but he wasn't really being mindful anymore.
He was caught up in the fact that he was so good at it.
What's the subtle thing?
Eventually, you realize that you have to be balanced, moderate.
Not sure about it.
Don't get caught up in any even good thing.
My brother is a psychoanalyst,
and sometimes he questions me why meditation would work.
Would studying Abidhamma give me some theoretical background
to justify the mechanics of meditation?
Yeah, I don't know if you have to go that far.
You might just get lost if you study Abidhamma.
There may be, I mean, I guess I guess psychoanalyst.
I mean, it's quite simple.
We react to things.
Our natural progression is to experience something,
and then judge it.
And it's our judgments that cause problems.
When you get angry, it creates stress.
When you agree, you want things to create stress.
Addiction.
And we're confused about them.
So when you start to remind yourself,
this is this, and you start to see things just as they are,
you develop this habit of seeing things objectively.
When you see things objectively, your mind is much clearer.
And you're able to learn things about reality
that are otherwise hidden by the cloud of greed
and anger and delusion.
It's quite simple.
Whether he agrees with Adder believes that is totally up to him,
but it works.
Which you can say to him something simple like that,
and then say, if he says, yeah, how could that work?
I'd say, you know?
I mean, we can argue back and forth, but it works.
You try it on 100% guarantee.
There's absolutely no chance that it wouldn't work.
I mean, how far it works is another thing.
Whether someone can actually take it to enlightenment,
it's completely up to them, but it's no question
that it makes your mind clear.
Hello, Bhante.
When being mindful in my daily life,
I often find myself struggling to find a mantra
for the experience that arises.
Should we try to find a mantra that describes the experience
the most accurate, or should we notice it on a more basic level?
For instance, when I wash my hands and dry them with a towel afterwards,
should I notice that it's feeling, feeling, or washing,
and drying, drying?
Thank you.
It's fine to say washing, washing, drying,
drying, scrubbing might be more.
Washing is fine, I suppose.
Brushing may be with washing and moving, moving.
Yes, drying, drying, it's fine.
It's just something objective, because there's nothing subjective.
It's not a judgment to say you're drying, right?
Oh, that's very drying of you.
And it's what drying is objective.
It's just a thing.
So that's fine.
You can call this movement.
And the Buddha did this.
When walking, you said walking, or walking is
it really what's going on.
It's just a feeling, right?
It's a physical sensation.
But that physical sensation we call walking.
So that washing, washing, drying, drying is fine.
There are certain cases where you might want to be
a little more, oops, erased one.
Yeah, hopefully.
I lost one.
They're rubbing it, clicked it, clicked it straight to them.
How do I answer your question?
I'm not sure.
I can't.
Once we get through the ones that are here,
I'll go back and look for answered once they don't look
familiar.
Did you see if he did?
I still see he did.
It's going for me to answer it.
How do I stop comparing ourselves to others?
We live so differently, and everyone has their own experience.
We have no real reason to compare,
but yet we are still doing it.
Why?
Well, you can't stop your habits.
This question, the answer is the same to any question like this.
You come to see that it's not useful to do that.
Your judgment that you don't want to compare people,
prepares them to others isn't all that helpful,
except that it might spur you to meditate more.
But the reason how we stop is through changing the habits
by seeing that it's a cause for stress and suffering
to compare ourselves to others.
Let's concede them.
It's a very deep-rooted element.
Why we do it?
It's a very deep-seated habit,
because we like certain things,
and we dislike certain things,
and we get caught up in identifying things.
We feel good about the idea of being better,
or worried that we're not as good.
We have it drilled into us,
that we have to be something,
our parents tell us we have to be something society tells us
that when we see someone else,
we feel like maybe we're a failure.
There's lots of forces that work together
to create low self-esteem, high self-esteem,
and conceit,
delusion, and the end it's all ignorance and delusion.
I tend to resist calling myself a Buddhist,
because I'm afraid I might do something I'd deed,
and then make people think Buddhists are hypocrites,
so that meditation doesn't work.
Any thoughts on that?
It's maybe overthinking things a little bit.
Oh, I think that's an excellent question.
I don't know, I mean, calling yourself Buddhists is right.
You want to be a good example, no?
I mean, from the perspective of someone who's in an area
where there aren't a lot of Buddhists,
maybe a Buddhist is the only Buddhist
that anyone who knows them.
I don't think I said they're right, but I think you know what I mean.
If you're the only Buddhist that your community knows,
it feels like such a responsibility.
I mean, I guess my initial reaction is don't take on that responsibility.
Call yourself Buddhists and you'll be happy about it,
but then I think, I guess what you're saying,
I can see what you're saying as you're talking about
how you relate to others, so it is all about that.
So the question is why, what would be the reasons for calling yourself Buddhists?
And one of them, of course, is to promote Buddhism, right?
And then if you're not a good Buddhist,
then you're not really promoting Buddhism.
Do I have a good question?
I get it.
I mean, I guess it's just my initial reactions.
And what are you going to do by or pretend that you're not?
So if anything, the opposite would be true
is that calling yourself Buddhists pushes you to be a better person, right?
Because you don't want to let the Buddha down.
If you didn't call yourself Buddhists, it would just be an excuse to do whatever you want.
That sense, you're going by your argument.
I wouldn't worry too much about that.
Calling yourself Buddhists is a good thing,
because it reminds you of what you're doing.
It lets people at the very least hear about the existence of such people
who take on the teachings of a fully enlightened being,
and work to, in their own ways, work to free themselves from suffering.
I guess just to add in my own question,
because I really like that question,
but it's not even just calling yourself Buddhist,
but knowing that you may be the only example to someone,
just worrying that what if I lose my temper,
or what if I do something that seems a very unbiased?
I mean, it just feels like such a great responsibility.
What can be a good thing?
It keeps you on the straight and narrow,
because you're concerned.
It's not necessarily a bad thing.
Sure. Hello, Bunthe. Hello, Robin.
I'm having a hard time with the ego.
I now see it rising, so I'm able to stop the wanting to control a lot,
and think that I get angry at myself,
as if I'm not believing in oneself.
Is there a practice to clearly see that there is nothing intrinsically good with ego?
Like it is in the case for sensual compulsions,
where one is able to identify the object, or what it is through meditation.
Thank you.
No, I mean, insight meditation is the one that comes to illusion.
Ego is a part of delusion.
Greed and anger can be tempered with things like love,
and loads ofness of the body, that kind of thing.
But delusion can only be countered through insight meditation.
I mean, you could probably argue that study would help.
Some people who study the Abi Dhamma, they claim,
and I tend to think it's true that they tend to be less egotistical.
I found that people who study Abi Dhamma tend to be really nice people.
So if you study the Buddha's teaching a lot,
you can get warped as a result,
and you know, caught up in thinking too much.
But it does tend to help your delusion, I would say.
But it's, yeah, that's just it.
It's a very weak practice.
So if you're looking for something, studying is probably helpful.
And then someone comes back and argues,
a counter argument that's studying, you can get very conceited about study.
I tend not to think so. I tend to think that someone who studies.
I don't know.
It's not a surefire. It's not, it's not enough.
The only real solution is insight meditation.
I tend to think that studying the Buddha's teaching would help.
Maybe wrong.
When I've been thinking,
we catch it after several places or minutes.
Join note thinking, thinking, or is it too late?
Thank you, Bombay.
Knowing might be better.
Just knowing that because at that moment,
there's a knowing that you were thinking.
There's an awareness that you were thinking of realization.
So it's either realizing or knowing,
you're just being aware of the actual realization
that you were thinking is probably better.
You could still say thinking, because again,
you're just reaffirming that that's all it was and not reacting to it,
but knowing is usually better than that instant.
What makes someone's personality?
Habits, habits, and also physical traits,
which don't all have to do with habits.
Genes, that kind of thing, you know, your genetic makeup.
Well, I would say have an effect on certain aspects of your personality.
Or how, you know, because personality is very much how others see you,
rather than actually how you are.
So your manner of speech, your facial expressions,
your physical features are how people know you.
Sometimes your body weight, your height, color of your hair,
color of your eyes.
So there's many things because personality is not real.
It's how other people see you.
Or how you see yourself.
But the real person or the deep stuff is all habits.
Your habits of impartiality and aversion and your defilements
and your good things and your bad things.
Your wisdom, your ignorance, all of that.
Hi, you.
Thank you to Dhamaviku.
Are you really this peaceful or is this just an act for your channel?
If so, what gives you this peace and tranquility?
I'm not allowed to answer these questions, but it's not appropriate.
But it's a serious accusation.
Something to be take to do.
Do I put on an act for these things?
I mean, I try to be very mindful when teaching.
I think that's important because it's easy to get lost.
And it's an interesting revelation that when you're mindful,
when you're actually meditating, even when you're talking,
when you're teaching, you apply the meditation technique.
It calms you down, you're no longer worried or anxious.
You know, they're getting up in front of a hundred people or
their teachers who teach in front of thousand people.
They're getting up on a stage in front of a hundred people.
It's quite an experience.
And I was doing that when I was like 25 maybe.
Even before that.
But really started when I was 25.
I'll never forget this one talk I had to give was pretty awful.
But I gave a talk in front of the whole city of Chiang Mai, basically.
Not the whole city, but a great number of Buddhist.
There were hundreds.
I don't know how many hundreds.
I don't really know, but it was huge.
And I gave a pretty awful talk.
Because it was entire.
I had to give this talk entire.
But the point is mindfulness helps.
And so that's something that the other thing is you're kind of
hyper aware.
And people are watching it.
So you want to be on your best behavior.
When you're in your room or alone or out in the forest,
there's not that reminder.
It's like why meditation in groups works so much better because
people are aware of this.
There's other people watching them potentially.
And so it keeps you on your best behavior.
So I wouldn't say I'm always like exactly like this.
But then again, we're all just conglomerate.
So aggregate.
So have it an experience.
So we're not the same from one day to the next.
I've had people criticize the way I am on the air.
Boy, you seem depressed.
You seem sad.
You see this, that, and the other thing.
Arrogant.
I had one guy call me a squad of brat race.
And we're not all the same.
And people's perception of us is another thing.
It's easy to perceive someone as something.
Anyway, I'm not going to go into much more detail about that.
What to do when the breath disappears after meditating on the stomach for a while.
Eventually it vanishes and a bright, warm, bright light comes in behind my eyelids.
I'm not sure what to do after that.
Yeah, I mean, it has, the disappearing has a lot to do with the warm, bright light.
And that's something that will distract you.
Take your mind into another plane.
At which point you should say to yourself,
see, it's quite simple.
You would just say seeing, seeing until it goes away.
And once it goes away, you'll find that ordinary experiences like watching the stomach come back.
If you like the bright light, you should say liking.
Like if you feel calm, which is a good one that people usually are often failed to note.
And you should not calm, calm, quiet, quiet.
Since I started meditating, the world started to look like a dream.
Is that what they call Maya, the world of illusion?
Maya is an idea that the whole world is an illusion, that this isn't really happening.
That's different from Buddhism, which says this is happening,
but the underlying happening is much simpler than it appears.
So it's not that you look at me.
It's not that I don't exist.
It's that the eyes, quite simpler.
It's not really a monk or a being.
It's just experience.
It's actually, it's just light touching your eye right now,
and sound waves touching your ear.
That's what's real.
So this is real.
It's not Maya.
But what you're probably experiencing,
or what a meditator in this tradition would experience,
is the breakdown of all those concepts.
Like people, places, things, entities would start to disappear.
Even their own body, they would sit in their body,
it would be like it wasn't even there.
Because it isn't.
The body is just a concept.
But it's not exactly like Maya, which is a Hindu concept.
Although we have the word Maya, but Maya in Buddhism,
means deception, where you deceive someone.
When you're a deceptive sort of person,
so it's actually a defilement, a bad mind state.
Good evening, Banthay.
When I'm meditating in the woods or park at my university,
Anson spiders crawl on my legs and feet.
How would I avoid being distracted by them?
Thank you.
Well, it's not a problem to be quote-unquote distracted by something.
It's just our your mindful of it.
But at that point, you could do spider crawling meditation,
feeling, feeling, if they bite you, you could do biting meditation,
pain, pain.
If they're fire ants, well, then you've got a real good meditation to focus on.
Fire ants are probably a fairly potent meditation object.
I don't know how to sit somewhere,
so that they have some spiders don't crawl on your feet.
I mean, meditating on an ant tells probably a bad idea.
But the odd ant or spider, well, it's just a meditation.
It's just an experience, a sensation,
it's a feeling, feeling, if you like it or dislike it,
you say liking or distance.
I remember meditating my first meditation course.
It was a really powerful experience,
because of course I used to kill mosquitoes.
Most people do.
And I was told this to meditate outside,
and stupid me, I meditated right beside my hut,
where there was this sort of ditch with water in it,
and tons of mosquitoes.
I didn't think they'd go and find a place
to where there were no mosquitoes,
but I knew they were going to come,
and I thought, well, I guess this is what I'm supposed to do.
I was really kind of dumb about the whole thing.
I teach your thoughts, so anyway,
why don't you go find a place where there were no mosquitoes?
But I sat there, and I sure enough,
I knew they were coming,
and my body was so tense,
and one mosquito landed on my shoulder.
Another one.
I counted about five or six,
and by the time the fifth one came,
I was noting, feeling, feeling, and disliking,
tens, tens, and then suddenly my body just let go,
and it was just this release,
and I felt so peaceful and so happy and such bliss.
It was the power of finally doing the right thing
and not being violent,
not reacting with violence to violence.
We can't stand these sorts of things.
It was probably a bad idea in long term,
and most people would say,
you just can't let mosquitoes bite you,
especially in a place where there's a danger of malaria,
for example.
But spiritually, it was an incredible experience.
That's the kind of experience that is very important,
where you find a new way,
because it was very much about no longer killing,
no longer being violent.
I knew I couldn't kill this mosquito.
And I said, I was just shifting into a new way.
It was a very powerful experience for me
because killing was second nature before now.
I've been trying meditation for a couple of years.
My mind was wild.
Now it's just annoying asking so many questions.
I'm left wondering if I will have to reincarnate.
I hope not.
I used your technique.
I burned my finger and I looked at it
and said pain, pain, and the pain went away.
And that just temporarily, thank you.
I'm sorry, I guess that wasn't actually a question.
But I guess there is a question,
wondering if they will have to reincarnate.
I don't really have an answer for that.
If you have no defilements,
you won't reincarnate if you have defilements,
you will.
That's not reincarnate.
It'll be reborn on your continuum.
My phone end at death.
Do you see these two questions?
Did we miss some going through?
I see too.
Oh, okay, these are those, sorry.
Can an enlightened being commit bad deeds?
Does this affect fair escape from rebirth?
And our hunt cannot intentionally do something that's bad.
I quote unquote bad, which means they have no defilements.
So as a result, they can't do something that would be based on the defilements.
But a soda pan they could.
A soda pan is like someone who's seen you by them,
but isn't yet free.
So they could potentially be reborn.
But maybe an enlightened being might do something that might seem like it's bad.
We've talked in a study group about how a lot of this is about our hunts smile at odd things.
Well, in an hour hunt could kill, but they wouldn't do it consciously.
And our hunt can step on an ant without seeing the ant.
Takupala is the best example of that.
He was doing walking meditation and all these insects died.
But he was blind.
He didn't, he had lost his eyesight.
So he had no clue what he was doing, what was happening.
Mano pabungamandamam.
It's the mind that is the base.
Can an enlightened being go back down to the level of normal human consciousness?
No.
Why are we getting a question, how say I'm seeing it?
If the question is not for me, you don't have to put a cue call in front of it.
Please don't actually, because it just confuses things.
The cue call in is for us to be able to filter out.
So we only see questions.
Yeah, just checking for someone else's.
I don't think we missed any before.
I think you're all caught up on that.
Well, there's a system where it automatically makes it a question.
If you ask a question, it does it like it changes it.
Let's look at this.
Is this a question?
Well, the second time I didn't do it is this.
Can an enlightened being choose to reincarnate into a body?
Choose to reincarnate into a body again if he chooses to.
Thank you, Bhante.
Thank you, Bhante.
