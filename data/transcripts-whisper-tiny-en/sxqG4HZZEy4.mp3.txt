Good evening, everyone broadcasting live September 17, 2015, and we have a large list of
meditators here, a lot of green, heartwarming to see.
Now we've got to get some local people meditating for people here in the area.
So I got word back from Thailand, my teacher.
Someone got in touch with him to let him know that we're opening a new monastery and meditation
centre on his birthday.
And he was overjoyed.
We did a reindeer with him, told he said, one knee today by grab in Lopu now, I went
to see Lopu, which is our venerable grandfather.
He said, he said, do I think he's truly very happy for us?
Very happy that we have a monastery to spend Buddhism.
And he asked, he said, call, I tell him, tell him, tell him, tell him, tell him, he wishes
for us to prosper.
Lita Mag, he was very happy, and her spelling's not very good.
So then she went back and said, sorry, the spelling's not good.
When she told them that we're going to have this ceremony on the 20th for his birthday,
he spoke louder than normal.
He's usually very quiet, quieter than me even.
And he spoke very loudly, D-D-D, which means good, good, good.
And then he asked for our address, so I had to pass along the address, who knows what
that's for.
And don't imagine he'll be making a visit, paying a visit, but he may very well be sending
us something.
So good news there.
In time, glad that we got through to him before we actually have the opening.
On the opening, we'll have to get a picture taken, and everyone who comes, and maybe several
pictures, and send them along.
Best would probably be if we can like mail a picture, or find someone in Thailand who could
frame a picture and bring him a heart, coffee, or something.
Anyway, good news there, all to do with goodness.
We're planning on cultivating goodness, and I've been talking lately about how goodness
and happiness are synonymous.
And here we have proof that I'm not lying, that I'm not making this up.
We have the Buddha's words.
This is a different suited than the one I'm familiar with, but it says the same thing,
which is the word order, it's a little different.
Ma bikhui punya na nan bhita, don't be afraid of punya means goodness, it doesn't say
good deeds.
It's a little bit misleading, it's fine, but it's better to just say goodness, because
goodness is in the mind.
And it doesn't really have to do with deeds, usually it's expressed as the deed associated
with the mind state, but it doesn't have to be, it's goodness, just to straighten your
view, just to cultivate understanding and wisdom, that's goodness, doesn't require actions.
You could say activity, the word bunya doesn't mean any of it, it just means cleansing
or any activity that makes your mind pure.
So the Buddha gave lots of different kinds of punya, charity is punya.
When you see someone on the side of the road and they're asking for some change, and sometimes
you wonder, should I give, shouldn't I give, you're afraid, but it's a don't be afraid.
He said elsewhere that giving to someone, even if they have no morality, still the charity,
the power of the charity, if you do it with a pure heart, wishing happiness for that
person, and still of great value, of awesome power, morality, just keeping the precepts,
determining not to lie, not to cheat, not to steal, not to take drugs and alcohol, not
to kill, it's very powerful, it makes you fearless, it makes you confident, it brings
happiness.
What I said, it's a sukhasetang bhikavaya di wajanang yalidang bunya'abi, happiness is synonymous
with goodness, goodness is synonymous with happiness, it's synonymous with happiness, with
pleasure, with that which is dear, goodness is that which brings happiness and pleasure
and all sorts of worldly good things, and this is why I said it's not exactly, it's
not exactly a part of the path, but it makes life easier, it means less stress, less
worry, it means a life that's easier and more conducive towards meditation, but he goes
on to talk about how he knows, bhijana mikopana hang bhikave di garatang katana punyana
yitang katana piyama napang vipakana, so I know how it brings and you can read the English,
so it brings what is dear, brings happiness, brings punya that has performed any kind
of goodness that I have done, has brought, vipaka results that are dear and pleasing and
pleasurable, it talks about how for five years he cultivated meta, just sitting and cultivating
a mind of meta jita, the mind of kindness, friendliness, love, and for seven, seven of what
we might consider to be the big bang and the big crunch, the universe evolution, seven
of them he didn't return to, or he didn't return as a human being, so means he was in
heaven for, so meta mani sudhan hikave kapi, ambhashta rupango abhasara, he was in the abhasara, the
radiant gods for, for the sunwatamana and for the vwatamana kapi, just two kinds of kapatas,
the evolving and then devolving, the getting better and then the getting worse, it's a
Hindu concept really, but seems that the Buddha kind of agreed with that cosmology,
they dwelt in the abhasara, the radiant gods and then as brahma, the emptiness, the
brahma's of emptiness, no, an empty brahma, we manda, an empty palace of brahma, and then
from there, also for one evolution, that's what it was, those two, and then, I'm going
to get it, he was dwelling as Mahabramma for certain amount of time, and then for six times,
he dwelt us as the king of the angels, this is lower than the brahma's, the devas,
the sakai, for six times, and then another seven times, he was Anika sat that for several thousand
births, after that, he was born as a king, king of the world, emperor of the world,
anyway, so it goes on and on, and that's not really important, but the point is, he wants
to express how awesome was the fruit of his goodness, the good needs that he performed
lasted life after life, just because of seven years of cultivating manta, he didn't
lead to enlightenment, not directly, but he's clearly saying that even as monks, even as
meditators, should cultivate goodness, that's for our benefit, and another thing he says
in this which is interesting, he talks about three aspects that were involved in his
cultivation of manta, I think, or there are three things involved with this, I think
we can go to a translator, because, I don't know, it's not the right way, okay, yeah,
right, da da da da, king, then the thought occurred to me of what action of mine is
this, this is the fruit, right, so there are three things, the first one is da na, so charity,
giving being generous and kind, the second one was da na na sada masa sanya masa, da masa
means training, or taming one's mind, and sanya ma, sanya ma, dama really, sanya ma means,
It has to do with restraint, with also with taming, jibtang sanyam isantimokantimar
bandana, the one who is able to tame their mind. So dhamma is taming the mind,
sanyamma is quieting or restraining, I guess, like holding, holding still, keeping still.
Just I think that's a bit interesting because it's a list of three that we can think of,
that are generally useful in life to be charitable, to be controlled, but it's kind of funny
that dhamma and sanyamma are actually quite similar. Let's see if we can find there.
Dhamma means taming the senses, so not letting the mind wander when you see, not reacting,
and judging and clinging and hating and so on. Sanyamma is body and speech, so being restrained
and body and speech. Dhamma, so would be the mind then, so dhamma means be restrained
in the mind, sanyamma means, and the Buddha does talk about this other places, restrained
of the hands, you know, so not fiddling your hands or not just taking things, not just
acting, being aware of the movements of the hands, bhadasanyamma, which is the feet,
being restrained with the feet, with the body, with the movements, with your head, with your eyes,
and dhamma is with the mind, when you do see, when you hear, to not let the
defilements arise, to not give rise to judgment.
Anyway, another simple and really general teaching from goodness, don't be afraid of it.
Apart from that, do we have any questions tonight?
We just had a question on what the earlier we had a question on what the heart means,
that means the heart on the meditation website. Not much, it's just like
showing that you care, I guess. The idea was actually that after you meditate, you could
and you could sit in some loving kindness, so if you want, we can do that now. Let's sit and wish
each other to be happy.
That was goodness. What we just did there? Goodness. See how easy it was? Don't be afraid of it.
Don't be discouraged. Goodness is easy to perform. Take a moment. You could have taken longer,
you can take a long time to do that. Today something a story came to me that's not a very good story.
Won't give the details, but it seems that there's some great good part of it is,
interesting for us. Yesterday when we did the on-campus meditation, one of the people who came was
the daughter of an old meditator, a long time meditator with us who think was one of the meditators
when I was, I invited a teacher to come to Stony Creek, and this would have been 2000,
three or two thousand, four thousand and two thousand and one both years I think,
for maybe two thousand and one thousand long time ago. So I asked her to give my phone number
or to get me in touch with her mother. And so today we talked to her mother and she told me a
fairly sad story about money and how there's a meditation group in our tradition here that
is charging money for courses. And they've gone ahead and purchased a house for like a million
dollars with their jobs and become meditation teachers, something like this.
And then are charging money. So it's kind of, so I said, well, you know, this I explained
sort of my philosophy and it's not wrong. It's not like it's immoral exactly, but there's some
kind of, it makes you think that there's a desire, you know, the one for it to arise, it's not the
need, it's the want. You want to have it. So you go to, you go outside of what is natural,
you know, it's much different from what, what our philosophy is that if people wanted to happen,
they will support it. And if they don't support it, we also will stop doing it.
So we'll do as much as we can with the support that we have based on the fact that people wanted,
you know, they're saying, I mean, again, it's wanting, but there is a need for it, right? We need
the building because people are want to come and the demand is enough to warrant it.
And there's, it's like it, the point is that it's paid for, it's supported.
It's not something that we have to go looking for support.
Anyway, it's just a different philosophy, but she said, no, this isn't Buddhism, this isn't the
Buddhism that she knows. Anyway, I probably shouldn't, this is a bit of a gossip.
The neat thing about it was in the end, she wants to help us. So I mean, she's always,
we've always been good friends, but it was neat to reconnect with her and talk about these things
and these issues about what is the proper way. But I'm set on that, you know, I mean, if there's
not enough support, it means there's not enough demand. And it has to be clear that it should be
clear that the only reason we want support, we ask for support or plenty of an ask for support,
is the only reason that we take support is because there's a desire for it to keep going.
The only reason we keep going is because there's enough support to keep going with the point.
And if there's not enough support, well, then we just stop. The only reason we do it is because
we have the support. That way you avoid the need to pay, you need to charge money for it.
Otherwise, it becomes a burden, really. That's why we're renting. I think maybe for us
renting will be good for the long term because it's no commitment, you know. It's a year
commitment and then at the end of the year.
Sorry, it sounds like the audio stopped. Oh, it crashed.
There we go.
Sorry, interrupted the audio.
But no, but the neat thing is, so she suggested that we could use the freezer here
and have pre-cooked meals and have the meditators heat up pre-cooked meals because it's difficult
to have someone cooking fresh food but having frozen food would be a lot easier and she said she
would keep, she would make sure that we have enough just herself. But we don't have to even rely on
that because there are other people who would do that sort of thing in the area. We do have
several people who are interested in that kind of thing. So that's an option if people come and
meditate. I mean, they asked something and I said to her, you know, I talked about this one woman
who came to me at clubs fest and said, how much is it? I said, it's free. I've been looking for this
everywhere. Now you tell me that it's free. I said, yeah, hey, I do, well, I do these courses
and you come and stay with me and we give you food and show and room and teach your meditation 21 days.
She said, okay, how much is that? What does that cost? Free. And she was so happy to
have this conversation and we both agree that this is what we know. This is what we're familiar
with. It's the idea that it's free. I mean, it's just awesome that we're able to do this.
And everyone, we all agree it's awesome. So people are happy to help and happy to just give
it because this is an awesome thing that we are. It's a very proud. This is goodness. You know,
talk about goodness, the ability to give someone something so priceless for free.
They said it couldn't be done. Many people say it can be done in the West. You can't do it in the
West. You can't do this in outside of Asia. There may be a little something a little bit.
It would have been a lot harder if we didn't have the internet community. But here you go,
the internet is not all bad. It connects us. Connects like-minded people. You don't have to connect
just with your neighbors. The old lady who complained about her flags. No, all around the world,
we have people who are into the same thing and we have people flying from all over the world.
Well, not all over the world. We've had people flying here to practice or traveling long distances.
One guy took a road of motorcycle from Florida to Ontario to do, of course.
Anyway, if nothing else, that's an example of goodness.
Description of how important and how powerful goodness can be.
Because, you know, anyone can get into the business model of things.
I don't mean to be really critical. And I know these people are still alive and
could potentially see this. But I'm not afraid, I think. We shouldn't be afraid to give
our opinions, right? To say, we'd rather do it this way and maybe people will take it. But
you have to be careful. You know, if you commit yourself to an expensive project,
then you have to make it happen. And sometimes it's a choice between morality. I'm not saying
in this case, it is. But you see, if you're committed and you have to ask yourself, it would cause.
Does it mean like pushing people, being pushy and pushing them to donate? They're dangerous.
I've heard this. I've heard stories of this where people are actually pushed to donate.
You have to. You need to. For a story started with this award,
there's this award that you can get in Thailand for spreading the dhamma, especially internationally.
They like that. And all of this, there's this group at Jhamtong and they've all gotten these awards.
There's people, you know, some of the people who are my students, I think now have this award.
They all for spreading the dhamma and doing.
They're very organized. And, you know, they've done a lot more in many ways,
they've done a lot more than I have as far as having courses, having a center, having a place,
and organizing courses, which is really the best. You know, this online teaching is great,
but it's not, it's not on that level. You've got to admire that.
But the reason they get these awards really is because they apply for them.
You apply for it. You submit yourself and you talk about how great you are. You don't do it.
You know, someone's doing it for them, but it's in the earth. So no one's ever done that for me,
and I don't get, I don't have that award. Not that I'm saying I deserve it, you know, but
there is goodness going on here. We're doing good things.
But we'll never, I never apply for such an award. Not my, you know,
that Jen said, man, anyway, don't want to go into it. Not being critical, but
anyway, no, her thing was, she was being pushed again. And she said this, she said this,
that I don't know. I shouldn't, this was told in confidence. I'm probably breaking
content. She didn't go into detail, but these are interesting things. You know, the idea of,
you know, you do goodness for the sake of goodness. And it's all related to this quote.
Goodness is for the sake of goodness. I apologize if anyone's watching this and is
offended or feels like I've spoken out of line. Those of you who this concerns,
let them, I'm talking about goodness. So I'm trying to be, I was, I was assuming I was being vague
and not trying to talk about people, but ideas. So if you disagree with my ideas or you think
the specific circumstances of some case don't warrant the ideas or the claims or the opinions
that I'm giving that's fine. It's not about specific instances. It's about ideas. That's,
that's the point here. And goodness is for the sake of, what, sake of happiness, not for the sake
of a golden statue that you can put on your, not that I'm critical of that. I mean, it's
a great that they're doing that. It's a great way to promote and encourage people to do the
ideas. See, she felt it anyway. I don't want to get into it. Not a good idea.
Any more questions? There are questions, but I just want to say that's a very good idea that she had
to fill the freezer with frozen food. That was one of those. That's the idea. It's also a commitment.
She said she will do it. That's fantastic. Because that, that was really kind of a unclear how
that was going to happen for food for meditators. And we have the deep freeze in the
basement. We have to look and make sure it works. That's fantastic. Awesome news.
Well, that said, we could get, we could get a new one, not that expensive and far more energy efficient.
Yes. Usually after a certain point, they, you do save money to buy a new one for the
energy savings. So you'll, you'll check that out and maybe let us know what the next volunteer
meeting. Awesome. How is restraining your hands and body helping you? I don't really see the
connection unless it's in a meditation context. That means being mindful, you know,
bodily expressions are course. They are, they go beyond physical. So moving my hands like this
is an expression of some mind state. Now it could be a mindful mind state, moving, knowing that
I'm moving the hand, but it could also be an angry mind state. It could be a nervous mind state,
you know, if I'm whatever, a fiddling, twiddling my thumbs like this,
could be a headache of something or scratching. And it's an expression of the emotion. So it's,
it's more powerful. Physical acts are considered more powerful. The, the karma required or the
intention required to create the physical act is more powerful than just having the mental
act. Like if you sit around plotting to kill someone, it's not as powerful or imagining yourself
killing someone. It's not as powerful as if you actually go out and kill them. Of course,
there's other reasons for that. But we, we sometimes commit, uh, unwholesome needs for lack of
mindfulness. You know, like maybe you might hit someone in the movies, how they hit someone
because they're just not thinking, you know, they're so angry that they just do it. So that's what
it means. Your feet, it would be, I don't know, it's a good example, like going somewhere,
traveling somewhere, just on a whim or based on emotion, running away from a, from a conflict or
someone running away from, from something that scares you, not dealing with it. So does it,
does it relate to like if you're tapping your feet and, you know, tapping your hands and everything
relates to your mind status a little more chaotic or? Yeah, and it's an expression. So it's reaffirming
it. It's, it's more powerful than just the mental, the physical act of, of shaking nervously
like this or tapping your knee or so. It, um, it reinforces the behavior. Like I would imagine you
could say the same thing, you could say the same thing about someone who has OCD, the act of performing
it is going to reinforce it more than just the obsessive mindset. That's why it's a hard habit to change.
Can meditation help with OCD? Absolutely. It's a habit. I don't know. Some people, I guess,
are born with it, but some people acquire it or have certain mind states that snowball and,
and become OCD. So if you can, if you can gain a habit, you can lose it. You can untrain,
you can train yourself out of it. Neuroplasticity, you can change your mind. Not, not radically, but
you can change many things about your mind and about your brain, about your habits.
And that's the point. I mean, it's not the physical. It is the mind that is important. So
all of the activities that someone who has OCD performs are not a problem. They're not OCD.
It's the obsessive compulsive obsession, the obsessive mind state that compels one to do that.
So if you can do those actions mindfully, you know, if you can learn to cultivate mindfulness,
you'll find it just falls apart to maybe over years or even lifetimes, but it will
reverse steadily decrease in intensity.
Thank you, Monday.
Is there a scripture you recommend that is about preparing for death?
There's the, the two, two, what the consultant, you see.
Yeah, do what the consultant, no, no, no, not the two, what the consultant, sorry, put her
a bed to sit there. Yes, okay, so here I'll give you a link. It's not going to come through those
it.
Oh, yeah, okay. Here's a link.
That's, that's a commentary on it by the Mahasi Sayada, which makes it awesome.
But the photo bed is, it is, I think, certainly patterned maybe. It's a short
filter. What is this photo bed? It's the dumbens that one should cultivate the four, one dies.
It's like SN 4.10 maybe? Could be. So that's a good one.
Specifically on preparing for death, but you know mindfulness is really the best way to prepare for
death. If you practice this meditation, it's the best preparation for death you could ever have.
So just a follow up comment preparing for death. Sorry, yeah.
Oh, just a follow up comment on the question. So you can pay attention to what your body is doing
as a cue for what the mind is doing, for what the mind is like. Absolutely.
No, it's not exactly that. It's not even the most important, but when you're mindful of the body,
your mind is pure. So if your mind flows, as soon as you become mindful of this,
as soon as you have an angry mind, as soon as you become mindful that you're shaking your fist,
the anger disappears. So that's a purifying, changing your mind.
Is that also related to the body language that we always think we know something about someone
because their body language, if they look away, it means this, if they look down, it means that
is that kind of related or is that not really? Sure, yeah. No, as a meditation teacher,
you do learn to read people's body language. It's useful for knowing the person's state.
If I'm practicing being mindful throughout life and I'm multitasking, what actions should I be
describing? Whatever is clearest, whatever is most prominent, it doesn't really matter.
Obviously, better is to not multitask, but when you have to, it's not really a problem.
Just be mindful of whatever is clearest.
Okay, so awesome to see so many green people. Again, if you're joining us on YouTube,
you have to go to meditation.ceremungalow.org. That's where we meet and ask questions and do
meditation together. So tomorrow I will be back in Stony Creek and then Saturday I'm coming back
here with a whole bunch of stuff, probably some plates and cutlery and the table to put all the
food on. Maybe two tables, I don't know. But I'm borrowing tables from the other monastery.
Think as we don't have one table.
Can you borrow chairs? I don't think any chairs in there. I don't think we'll do the chairs.
People can eat on the floor, sit on their laps, so you don't sign. Sunday's going to be cold,
I think, but hopefully it won't rain. Feed in the basement, eat anywhere, sit around and eat.
Maybe we have to go driving to find more questions. That was fun. Did we say, did we talk about that
here? I don't think so. Maybe we did. Anyway, we went driving around the area. Robin's husband
drove us around and we're looking for trash and we found some couches. People were throwing out
and we took all the cushions and we found two beanbag chairs and we took them and
brought them back and we washed the covers and now they fill our library. The library has cushions.
That's right. In Stony Creek, they've got cushions of various kinds. I can just borrow a bunch.
That sounds good. It looked pretty good after they were cleaned too. They didn't look like they
came out of the trash. I think one of them has the stain on it. Flip it upside down.
Yeah. Okay, so let's stop there. Thank you, Dante.
Thank you, Robin. We didn't even have you read the quote. Did I? No.
Should we do that just to close off? Let's close with the quote. Do not be afraid of doing
the deeds. They are another name for happiness or the desirable, the wished for, the deer,
and the agreeable. I know well that for a long time I have experienced desirable, wished for,
deer, and agreeable results because of doing good deeds.
So there you have it from the Buddha. What is the source? I'm sorry. What is the source?
IT 15. What is that? If you would took it. Okay. I'm not familiar with that.
If you would take that means that's what it's just, it's just interesting book.
Everything ends well. Everything starts with this was said by the blessed one.
Nice.
It's one of the, in the kudakani kai, it's one of the miscellaneous sections.
Ayyam pi ato, wouldo, bagavata, iti misutante. It doesn't start with able misutante.
So I think it was Uvali, it's supposed to be Uvali, I'm not sure. Where the iti would
take a story behind it. Very nice.
It's just one more question. Are you in need of new cushions? Are you in need of cushions
for the monastery? More cushions. No, no, especially. We've got maybe 15 already.
8, 9, 10, 11, 12, 14.
So we don't have 14 meditators every day. So it's just for this one, one thing that we could
maybe use some more.
We will have several people, many people here.
But you do have a wish list on amazon for things for the center. Just in case anyone was asking
because they wanted to to send something right. And if anyone is inclined to look up the wish list,
I think if you go to amazon.ca for amazon canada and just put in one day's email,
which is utadamal at gmail.com under look for a wish list. It'll pop right up.
And I think there's a few things for the meditation center on there.
And one last question is meditation impermanent.
I think you can just type in utadamal in the new thing anyway.
Is what impermanent? Is meditation impermanent.
Meditation doesn't exist. Meditation is not a thing.
Meditation is a name for certain experiences of a certain type. But experiences are all
that exist in there impermanent. Sorry, besides nibana, which is not impermanent, but does exist.
Although exist is maybe not the proper word.
Anyway, that's not really a meditation question, is it?
What do you mean is meditation impermanent?
I think that's definitely a meditation question.
Is it? I think it makes more clarification if it's going to be about meditation.
What are you doing asking questions? Aren't you doing sitting meditation?
Texas tutorial.
Maybe a listening meditation.
Enough. Good night.
Good night, Dante. Thank you.
