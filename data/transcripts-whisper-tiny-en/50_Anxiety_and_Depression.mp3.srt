1
00:00:00,000 --> 00:00:07,680
restlessness, question, and answer. Anxiety and depression. How can I free myself from anxiety and

2
00:00:07,680 --> 00:00:14,560
depression? Mindfulness meditation helps us deal with and overcome anxiety, depression,

3
00:00:14,560 --> 00:00:20,240
and many other mental illnesses. So the answer to your question is quite simple. It is one of

4
00:00:20,240 --> 00:00:25,760
the purposes of what we teach and practice. There are, however, two issues that potentially

5
00:00:25,760 --> 00:00:31,360
obstruct the proper practice of mindfulness for one who suffers from chronic anxiety or depression.

6
00:00:33,680 --> 00:00:39,440
The first issue is the reification of depression and anxiety. The term reification has great

7
00:00:39,440 --> 00:00:46,160
importance in Buddhism. To reify means to give something existence. When we say, I have an

8
00:00:46,160 --> 00:00:51,920
anxiety problem. We have reified that concept. We have conceived of an entity that is monolithic

9
00:00:51,920 --> 00:00:57,520
and atomic in the sense of being indivisible. It has become a thing that you have to get rid of.

10
00:00:58,800 --> 00:01:03,360
There is a story about the time after the Buddha passed away, where in the monks were gathered

11
00:01:03,360 --> 00:01:08,560
trying to decide what the Buddha's actual teaching was. Buddhism had split into a number of different

12
00:01:08,560 --> 00:01:12,880
schools, and many of the schools were teaching things that certainly did not bear any resemblance to

13
00:01:12,880 --> 00:01:18,960
the original teachings. A king asked one well-respected monk, what did the Buddha teach, what sort of

14
00:01:18,960 --> 00:01:25,440
teacher was the Buddha, and the monk correctly answered. The Buddha was one who split things apart.

15
00:01:26,160 --> 00:01:31,200
The Buddha taught the splitting of atoms. You had this indivisible entity you call anxiety or

16
00:01:31,200 --> 00:01:36,880
depression. The Buddha's teaching aims to split it up into its constituent parts. It is about

17
00:01:36,880 --> 00:01:42,800
separating the condition, problem, illness, or whatever into actual realities, because depression

18
00:01:42,800 --> 00:01:49,760
and anxiety do not exist. What exists are moments of experience at an ultimate level that is what

19
00:01:49,760 --> 00:01:57,280
is truly real. For anxiety, there will be moments of experiencing that we lump together as anxiety.

20
00:01:58,000 --> 00:02:02,960
Suppose that you have to get up and go onto a stage in front of 100 people or 1000 people

21
00:02:02,960 --> 00:02:08,000
and give a talk. You are sitting backstage or in the audience waiting for your turn,

22
00:02:08,000 --> 00:02:15,200
and you think about going up on stage. That thought is an experience. Then immediately after that,

23
00:02:15,200 --> 00:02:21,600
anxiety might arise, which is another experience. After that initial experience of anxiety,

24
00:02:21,600 --> 00:02:26,960
there may be experiences of butterflies in the stomach, the heart beating fast or tension in the

25
00:02:26,960 --> 00:02:33,600
shoulders, and based on those experiences, other experiences will arise. This chain of experiences

26
00:02:33,600 --> 00:02:39,200
can create a feedback loop which is how panic attacks arise. You enter this feedback loop based

27
00:02:39,200 --> 00:02:45,200
on moments of experience that spiral so far out of control, and the habit or the causal chain is

28
00:02:45,200 --> 00:02:49,920
so strong, quick, and ingrained in you that you are unable to get yourself out of it.

29
00:02:50,960 --> 00:02:56,560
Experiences of depression can be similar. There are experiences. Maybe something bad happens to you,

30
00:02:56,560 --> 00:03:01,440
and maybe you become sad, and then reacting with sadness becomes a habit, and you become more and

31
00:03:01,440 --> 00:03:05,920
more sad when you think about that experience. And then you try to get rid of the thought,

32
00:03:05,920 --> 00:03:12,000
so the thought becomes a source of aversion. You feed it. There is an energy involved with avoiding

33
00:03:12,000 --> 00:03:18,560
things or disliking things that creates more negative thoughts and habits. All of this is just to

34
00:03:18,560 --> 00:03:24,240
say that the solution from a Buddhist perspective is to break things into their constituent parts.

35
00:03:24,240 --> 00:03:30,080
That is the most important step. The second issue that gets in the way of dealing with mental

36
00:03:30,080 --> 00:03:35,760
illness is the negative relationship we have towards the chain of experiences. To call it a mental

37
00:03:35,760 --> 00:03:41,040
illness, even to call it bad is not inherently wrong, but we have to understand the difference

38
00:03:41,040 --> 00:03:46,960
between understanding something is bad for us and feeling bad about it. We feel bad about

39
00:03:46,960 --> 00:03:52,720
a depression, we feel averse to it, we feel bad about our anxiety, and we want to get rid of it.

40
00:03:52,720 --> 00:03:57,920
Taking our patterns of experience as a monolithic condition and feeling bad about the condition

41
00:03:57,920 --> 00:04:03,840
are hindrances to actually dealing with the condition. They lead us to seek out ways to not have to

42
00:04:03,840 --> 00:04:07,920
deal with the condition directly, taking medication or other drastic measures.

43
00:04:10,560 --> 00:04:15,680
People take medication to cover up anxiety and depression by flooding their systems with serotonin

44
00:04:15,680 --> 00:04:19,440
and other brain chemicals that prevent them from following the habits and cycles that they

45
00:04:19,440 --> 00:04:26,080
normally would get into. They see anxiety and depression as problems and react negatively to them.

46
00:04:26,080 --> 00:04:30,880
In order to begin to face our problems, we must first begin by seeing that our reactions to

47
00:04:30,880 --> 00:04:37,760
our problems simply causes more suffering. Some of what we perceive as depression and anxiety is not

48
00:04:37,760 --> 00:04:43,680
even mental at all. For example, butterflies in the stomach, heart beating fast tension and headaches

49
00:04:43,680 --> 00:04:51,280
etc are distinct from actual anxiety. They are not anxiety, they are merely physical byproducts of

50
00:04:51,280 --> 00:04:56,720
anxiety, they are not a problem, and if we can see them as just experiences, then we will not react

51
00:04:56,720 --> 00:05:02,560
to them, feeding our negative mental habits. There is no question that depression, anxiety,

52
00:05:02,560 --> 00:05:07,280
and any mental illness are bad, but what is bad about them is that they are reactions,

53
00:05:07,280 --> 00:05:12,720
they are bad reactions, and if you react to them by disliking, hating, or feeling guilty about them,

54
00:05:12,720 --> 00:05:17,040
you are feeding your negativity, leading only to more anxiety and depression.

55
00:05:17,040 --> 00:05:23,680
You can see this in regards to schizophrenia. My understanding of schizophrenia is that it involves

56
00:05:23,680 --> 00:05:28,480
what our ordinarily referred to as hallucinations. In Buddhist meditation practice, however,

57
00:05:28,480 --> 00:05:33,120
we would simply call them experiences. Maybe you hear voices, maybe you see things,

58
00:05:33,120 --> 00:05:36,320
or maybe thoughts arise that you think come from an external source.

59
00:05:37,200 --> 00:05:42,320
It is not to ask whether they are real or hallucinations, it is important to understand

60
00:05:42,320 --> 00:05:46,640
they are experiences, to see them for what they are, and to not react to them at all.

61
00:05:48,320 --> 00:05:53,680
I met a monk once who told me he heard voices telling him to kill himself. He had difficulty

62
00:05:53,680 --> 00:05:58,880
understanding that they were just voices. Even if a real person tells you to kill yourself,

63
00:05:58,880 --> 00:06:04,160
it does not mean you should. Without mindfulness, we are carried away by our experiences,

64
00:06:04,160 --> 00:06:09,680
reacting before we can appreciate them for what they really are. Being able to see our experiences

65
00:06:09,680 --> 00:06:16,320
as experiences is essential to mindfulness meditation. The first part of the solution is to

66
00:06:16,320 --> 00:06:21,520
break our conditions up into individual experiences and to understand, first conceptually,

67
00:06:21,520 --> 00:06:26,480
and eventually through direct experience, the distinction between entities like depression

68
00:06:26,480 --> 00:06:30,880
where one might think, I have depression and the basic building blocks of experience.

69
00:06:32,240 --> 00:06:35,920
The second part is to take those building blocks, including the problem,

70
00:06:35,920 --> 00:06:40,640
and see them objectively and without reaction. Let them be just what they are,

71
00:06:40,640 --> 00:06:47,760
try to create this ability to see experiences objectively. This is what allows wisdom to arise,

72
00:06:47,760 --> 00:06:53,600
what allows us to see clearly, and what allows us to let go. Our attachments involve

73
00:06:53,600 --> 00:06:59,120
delusion, ignorance, and lack of understanding of what real happiness and peace are. Happiness

74
00:06:59,120 --> 00:07:04,560
and peace cannot come from trying to fix our problems, and they cannot come from experiences

75
00:07:04,560 --> 00:07:14,560
themselves. Happiness and peace have to come from the way we perceive and relate to experiences.

