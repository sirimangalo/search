All right, so welcome everyone.
We have quite a crowd tonight.
Couldn't see people interested in the Dhamma.
The Buddha said, Dhamma kam over Bhagavan Hauti.
One who is attracted to the Dhamma,
who is kam, you see, Dhamma kam over kam
is generally we use it as a bad word,
but to mean attachment or desire.
The Buddha said, one who is desirous of practicing the Dhamma
or hearing the Dhamma likes the Dhamma that's put that way.
Bhagavan Hauti is someone who is distinguished.
So thanks everyone for coming tonight.
The Dhamma that we chanted tonight is perfect for a Dhamma talk.
We've given the Dhamma talk on this before.
We've even written something on this Buddha.
This is, does anyone know what Suta this comes from?
Where are scholars at them?
Oh, we have a scholar.
We have a winner.
The Giri Mananda Suta.
So what's the story of Giri Mananda?
Giri Mananda, I think.
He was sick.
He was sick.
And Ananda said, please Lord Buddha,
out of compassion, would you go and see Giri Mananda?
Because maybe if you go, he will get better.
And the Buddha said, well, maybe if you go, he will get better.
No, he didn't quite say it like that.
The Buddha said, well, maybe Ananda,
if you go and teach him the Dasa-sanya, the ten perceptions,
maybe he'll get better.
And he said, so what are the ten perceptions?
And then he taught these ten perceptions, which are really a comprehensive,
a reminder, a remembrance of a Buddhist approach,
or a Buddhist perspective of the world, right?
From Buddha's point of view, what's the world like?
What's, how do we view reality?
So these ten perceptions are something that,
see, the word sanya means remembrance or memory or a perception,
how we recognize, recognition, you might say.
But the meaning here is something that we should recollect.
And the Buddha could see that, I guess,
Giri Mananda had forgotten these things.
They needed a reminder until he instructed Ananda to encourage him
with this teaching and give him some peace of mind,
help him to deal with the sickness.
Ananda went and indeed Giri Mananda got better,
just by hearing these things through the power of the Dhamma.
So they often will chant this when people are sick as a result,
but I think it's kind of wishful thinking
because normally the sick people don't have a clue it's being said.
The point is that through practicing these things,
through recollecting these things, it heals the mind,
it makes the mind strong and powerful, keeps the mind from clinging.
So these are recollections that we do as a matter of course,
sharing our practice, starting with the first one.
Anitya Sanyaya, this is a simple one, seeing impermanence.
The first two are actually related,
so we should deal with them together.
And it actually relates to what we were studying the past three days.
No, the three Sutras, the Dhamma Jakapawata, the Anattalakanasut and the Aditya Parayayasut.
The second to anyway, Anattalakanasut is
deals with the five aggregates.
The Aditya Parayayasut deals with the six senses.
So here again, we have the five aggregates and the six senses.
Curious thing is, here the Buddha says that the five aggregates are impermanent,
whereas in the Anattalakanasut, the emphasis was on the fact
that the five aggregates are non-self, but there is a connection there
because if you look at how the Buddha starts the Anattalakanasut,
he starts by pointing out the fact that they're impermanent.
And this is important because one of the best ways to understand non-self
is through the other two characteristics.
People always have a difficult time understanding non-self
and what does it mean to say that something is Anattalakanasut?
It's easy to understand Anitya and Dukkah, but Anattal is difficult sometimes
for people to understand.
So we understand it through Anitya.
The point being, if something is impermanent, you can't say that it is yours
and it belongs to you and it's controllable, or the same with Dukkah.
If something is going to, this status is going to leave you unsatisfied,
and this status value, you can't say that it's self.
And so one of the, an easier way to understand the five aggregates is through impermanence,
but at any rate, the truth about these first is that you could just as easily
switch them, I think, I wouldn't, don't quote me on that, but quite clearly they're
both Anitya and Dukkah, and there are two ways of understanding reality.
The Buddha, they say that the Buddha taught different, sometimes you would teach
the six senses and sometimes you would teach the five aggregates and sometimes
you would teach the, the, the dad does sometimes the 18 dad does or the 20 some
Andrea who would teach different things for different people, because if you,
some people have attachment to the mind, some people have attachment to the body,
and some people have attachment to both.
So for those people who have attachment to the mind, the Buddha would teach the
five aggregates. The first aggregate is body, but the rest are mine, most of it,
is mostly to do with the mind. For those people who were attached to the body,
he would teach the, not the six senses, but it's called the Ayatana, and they
believe that's what we've got here, right? Yeah, this isn't the six senses, this is
the 12 Ayatana. We got the eye and, and form the ear and sound, the nose and
the, the nose and smell, the tongue in taste, the body and feeling and the
thought and the mind and thought. And this, this he would teach, they say, for
people who are attached to the body, because it's mostly physical. It's all
physical, except for the mind and sometimes dhamma, I believe dhamma can also,
thoughts can also be, mind objects can also be physical, I'm not sure, dhamma
can be either mental or physical, I think. And sometimes he would teach the Ayatana
because they're, for people, for people who are attached to both, because,
or not the Ayatana, the dhatu, the dhatu are the six senses, seeing, hearing
smelling taste. And the six senses, the eye, the ear, the nose, the tongue, the
body and the heart and the objects of sense, sight, sound, smell, taste, feelings
and thoughts. And then the consciousness, eye, consciousness, ear, consciousness,
nose, consciousness, tongue, consciousness, body, consciousness and mind
consciousness. And that's because if you count them up, it's almost, it's
almost even. So we have the six consciousnesses are, are my mental, then you've
got the mind is mental, thoughts can be mental. So that's a seven, seven and a
half are mental and 10 and a half are physical, something like that. But the,
the point I want to make here is that they're really all the same thing. And
you can, that's referring to the same thing in different forms. It's
referring to experience. Reality is not something difficult to understand.
It's, it's, it's experience. You sometimes see things, you sometimes hear
things, you sometimes smell, you sometimes taste, you sometimes feel you
sometimes think. But whatever arises, all of that fits under the heading of
experience. And that's what we focus on in our meditation. We come to see
that all of experience, all, every bit of reality is impermanent, suffering
and non-self is incapable of satisfying, incapable of bringing peace, happiness,
or freedom from suffering. And in fact, delivers stress, stress, pain and
suffering, when we cling to them, when we cling to it. Everything, in
everything that arises does this because it's impermanent. So when we begin to
look at, at experience and look at reality and realize for ourselves that
everything is impermanent and all things that arise are impermanent, they're
unsatisfying, they're uncontrollable. Then we don't cling to them. So when you
see something that you don't like or when you hear something that you don't
like, you don't become upset about it. The disliking doesn't arise. You simply
experience it for what it is. You see it for what it is, it's arising, it's
ceasing. The idea of clinging to it can't arise because you know that it's not
going to satisfy you. It's not going to do any good to cling to it. It's not
going to make you happy. It's not going to bring you peace. It's not going to
bring you happiness. And so these are, these are the essence of Vipassana, seeing
impermanence and suffering and on South the three characteristics. Because it's
what arrest, it's what removes this idea, this whole idea of clinging to
things, of chasing after things from the mind, of reacting to things. Like a
car. Would you get in and drive a car if you knew you couldn't control it? If
there was, if you, or would you get in, suppose you got in a big power forklift
or one of those back holes or something? And if you knew you couldn't drive it,
would you turn it on and try to drive it anyway? You wouldn't do such a
thing. We wouldn't drive a vehicle if we knew we weren't capable of
operating it. And this is what happens when you start to look at reality and
you realize how out of control it is. You realize that your own mind is out of
your control. We realize this in our daily life, but our reaction is to
the opposite. Our reaction is to try to make these things controllable. It's to
try to make things permanent. It's to try to make things satisfy. We see
impermanence, suffering and on South. We see it daily. And we're constantly
fighting against it. We're constantly rejecting the truth that you can't
control things that they can't satisfy. So this is the reality that we live
with. And this is the conflict that you see in the world around and in the
world at large, in the world in general. People trying to change reality, trying
to reject the truth, deny in denial you might say. So our lives, we try to set up
our lives as as stable as possible, right? We want to get a good education. We
want to make a lot of money. I think if you have a big fat bank account, we'll
be our lives will be stable. Sure. We try to eat well. We try to exercise to make
our bodies healthy. So we will be safe from disease and and stress and
suffering. We try to surround ourselves with pleasant things constantly fighting
against the reality that suffering is waiting just around the corner. We don't
know when it's coming. And well, and in fact, it comes every it comes all the
time to rich people to poor people suffering comes. It comes right here sitting
here, right? You would think that this at least would allow us freedom from
suffering sitting here, but it doesn't. Even if your body is is free from pain or
aches or soreness for a moment, still the mind will create so much suffering
for a moment to moment because of the things that we want and can't get to the
things that we don't like, things that upset us. How the mind becomes bored and
disinterested, unhappy, dissatisfied. And we try to control everything. We try to
force ourselves to be this way or that. We try to force ourselves even our
own selves. We try to force and other people we try to force us. It might say
even more more ridiculous. We try to force ourselves to be this way or that
and that doesn't work. And so we're dissatisfied with ourselves. We tend to
dislike ourselves. We're dissatisfied with ourselves. We feel guilty because
of how inadequate we are. And so we go around trying to change everyone else
instead. We try to force other people to be like this, to be the way we want.
It's much easier to teach other people than to teach yourself, but you don't
get nearly the same results teaching other people. So we learn that this
isn't the case. We learn that this isn't useful in any way trying to force
things. You're trying to build a castle out of sand.
Or as a Buddha, I think, in the Buddha said a castle in the sky. You're trying to
build a castle in the sky. You can't possibly succeed. You build something up and
then it falls apart.
And you never get to the point where everything is stable because it's called
Sankara. Sankara is like the sand castle. You build it up and build it up. Think
about, would you ever want to live in a castle made of sand? Would you ever think
it's safe and secure? If someone said, do you look, I've got this wonderful
castle here and it's going to be a safe and comfortable place to live and you see
that it's just a sand castle. You think the person of course was crazy. But
that's really how we are trying to live our lives. As though death wasn't around
the corner or trying to live our lives as though everything was stable and
secure and permanent. And if you look at the lessons people learn, if you look at
the reality around us, you see that it's not the case. Those are the first
two. They're the important ones. The third one, Subasanya, has to do with seeing
that the body is not beautiful. And this is a particularly useful one for monks
because monks are sworn to give up the attraction to the body. And so it's
something that is important to remind yourself. Remember,
Subasanya means a reminder. So you have to remind yourself from time to time.
And this isn't, there's nothing in here that says, there's nothing in the
meditation that says it's disgusting. The Buddha says, Asuchino means it is,
what's the word, this impure, full of impurities. But he says, you see this, not
by saying this is bad, this is ugly, this is disgusting. You say, what's in the
body? In this body, there's head, there's hair, head, body hair, head, head,
hair, English much, head, hair, body hair, nails, teeth, skin, flesh, tendons,
bones, bone marrow, kidneys, heart, liver, pluris, pain, lungs, large
intestines, more intestine, undigested food in the stomach, feces, brain,
bio, phlem, plus blood, sweat, fat, tears, grief, saliva mucus, sinovial fluid,
and urine. There's no diamonds on this list, you see. There's not, one of these
things is made of gold. There's no roses, there's no sugar and spice, I don't
know. You don't sugar and spice. What are boys made of? What's the other part of
this, that poem? I don't think you see any of that either. I forget to think of
the boys part. But girls are supposed to be made of sugar and spice and all
things nice. So we have to write it in there. No, we have to add. No, I don't see
it. So you go over this and you start to see that, well, they say, they say,
beauty is only skin deep. Well, it's not even skin deep. It's maybe makeup deep if
you have that far. Because you can put, you can put, you know, where golden rings
and golden necklaces, you can wear diamonds and flowers and your hair and all that, but
it stops there and the rest of it is actually. It's just because we have, we have a different
kind of sun, yeah, we have a different kind of habitual idea of what the body is like.
And that it's habitual and that it's artificial and that it's not based on reality. You
can see if you look at dogs, for example. Now, it's, I suppose it's not impossible. It's
probably fairly difficult to find a human being who is incredibly attracted to dogs of
the opposite gender. I mean, you could go further and find animals that are, that human
beings are certainly not attracted to. But I think in general, we can agree that dogs
are not very attractive and yet dogs are attracted to each other just in the way that humans
are attracted to each other as well. So the fact that we see beauty, it's a sun, yeah,
it's a perception. There's nothing intrinsically beautiful about the body. So in order
to see this, see through this, we don't have to be negative about the body. We simply
have to remind ourselves what's really there. And we look at it piece by piece. In fact,
in the Risudhi manga, it's really one of the best parts of the Risudhi manga. The whole
book is incredible. But one of the great parts is reading about these pieces of the
body, because you would have never thought, I would have never thought on my own in such
detail what these things are actually like. Like hair on the head, it's in this Risudhi
manga, he says it's like grass or something, like rice growing in oil and blood and dead skin.
That's what the hair of the head is. And it's greasy and it's smelly and so on. And so
you objectively look what color is it, what smell, what does it smell like? And so what
does it taste like? What does it look like? And it goes through all of these, the 32 parts
of the body and it's quite interesting. So it's a useful one, something to wake us up and
help us to see. It helps to let go of the body and then you feel so relieved. You don't
have to worry about your body. You have chipped teeth and stained teeth and you have,
you know, I don't know, bad skin or skin or you have chipped nails, I don't know. Your hair
is the wrong color, your hair is the wrong length or you don't have to worry about it
anymore. Because you see that it's, you know, you can maybe make it, you can maybe make
it smell good and look good for hours at a time. But then you have to do it all over again.
You have to hide the impurities of the body. We're constantly, just in order to interact
with people in this world, we're constantly having to hide the impurities of the body
from, from, from society. Otherwise, we'd be, we'd be chased out of society because of
the smell and the way we look. But the reality of it is that it's not something certainly
that we should be concerned about. It's just physical, physical. It's just matter.
But most important is that it removes the idea of beauty, the attachment to the body
is being beautiful, which is an incredibly strong perception. The Buddha said, there's
no greater attachment. There's no greater sight that men attached to, I had a
sexual man cling to or, or, or become attached to, than the, the form of a woman. And
vice versa, there's no greater attachment, greater form that women, heterosexual women
attached to than the form of men. So important to, to understand and to overcome and
to see it simply for what it is. It's not real. It's not, it's not even natural. You see,
this is the argument people always have. Well, it's natural to be attached to, to be
attracted. But isn't really, what does it mean to say that it's natural? It means that
it's been evolved, just, you know, if you look at from a physical science point of
you, it's just kind of evolved by mistake. And the Buddhism would say it's evolved completely
by mistake. There are three, three theories of evolution, or three theories of the origin
of, of species. One they call intelligent design. This is the Chris, what the Christians
believe in, theists believe that, that there was an intelligent creator. The second one
is evolution, or I think they have another name for it. I can't remember what it's called.
And the third is unintelligent design. This is the Buddhist theory. So it was an unintelligent
creator. We, we created it ourselves out of, not out of intelligence. We created ourselves
out of our own stupidity. This is the Buddhist theory of, of creation, of origin of the species.
So if, if, if you, if you look at it, there's no reason why it should be this way and not
be another way. It's just a, for the purpose of perpetuating itself, the species perpetuates
itself by sexual attraction. That's all. If you think, it's a good thing to have lots and
lots of humans. Or if you think the human form is somehow the ideal form of existence,
then by all means, this is something good to be attached, to get people attracted to
each other. This is what you do if you want more chill, more, more, more birth, right? But
if you look objectively at it, being a human is not perfect. Human beings are certainly
not perfect. If they were made in God's image, then God's in a pretty poor self image,
I think. So we could certainly do better than this. I mean, why aren't we full of sugar
and spice and all things nice? Wouldn't that be more fun? Angels and so on. You know,
stardust. Why aren't we just stardust? Why do we have to suffer? Why do we have pain?
Why do we have old age sickness and death? These are questions we don't ask because we're
so steeped in this idea of humanity of being a human. This is how we got here. The only
way you can be born is a human is to be incredibly steeped in the idea of being a human.
But it doesn't have to be this way. You can change. You can see through it. You can objectively
see that the body is just physical. You can overcome your attachment to the physical body,
which is an incredibly empowering thing. It's like breaking free of the mold. Like you've
got this prison of your perceptions that is forcing you to see things in a certain way.
And you can be free from that. You can free yourself from being a human being. You can
make yourself universal. You can free yourself from the subjectivity of being a human being
and become an objective being in the universe. This is how you become like an angel, for
example, you become free from the contrived artificial construct of being a human being.
You give up your attachment to suffering, to these ugly things. Angels can smell human beings
from, I think, a thousand leagues. That's how bad we smell. An angel can, that's why we
don't see angels free often, apparently, because they're not so keen on visiting. We
smell bad. And we think we're wonderful and beautiful and incredible. We convince ourselves
that we're not the way we are. We try to escape reality, not realizing that there's a
better way, not realizing that we can be free from that true happiness comes from just
the opposite, from letting go and straightening out the mind, removing these imperception
or these wrong perceptions from the mind. So that's useful. Asubha Sanyos useful. Adino
Sanyo is quite useful as well. Maybe more on a very real level. Sickness. Adino means
the dangers of disadvantage. But sickness is an incredible stimulator for meditation. Stimulant
helps. Encourage you in meditation quite well. And it encourages you to let go quite well,
especially of the body. So there's this wonderful saying that I always remember. Bahu dukho
kho ayeong ka yu. Bahu ayeenobu. You can see it here. We chanted it tonight. It's so totally
Buddhist because no one else would want to remember such a thing. Bahu dukho of great suffering
indeed is this body of great disadvantage. Not very positive now is it? Bahu dukho kho ayeen
ka yu. Why would you want to say such a thing? Why would you purposefully focus on the problems
with the body? Because the body is not ours. It's ours if you believe that this is all we've
got. That all we've got is one life. Or more importantly, if you believe in the material universe,
if you believe in the material body as existing as an entity. Or if you believe that we are the
body, that we are this body. And that's all we are. And it's this is not something that can
simply be intellectualized away because we do believe to some extent that we are the body.
We do believe that this is our life and we were born and we identify very much with the body.
It's so pervasive that we have to constantly be questioning ourselves on this.
And pulling ourselves out of our conceiving, it's kind of like how when you watch the sun,
when you watch the sun, it looks very much like the sun. Well don't look straight at the sun,
but you watch the sun's tracks. And it looks very much like the sun is going around the earth.
Just like how the moon goes around the earth. It looks like the sun is doing the same thing.
So it's easy to think that here the sun is going around the earth.
Like the sun and the moon are chasing each other or something like that. But in fact it's
an order to pull yourself out of the solution. You have to do some brain work. You have to
actually pull yourself out of your perception. So it's kind of similar. It's not exactly,
but the idea is the same is that we have these perceptions that take a lot of work. I mean the
god perception, many people have grown up with and maybe lifetime after lifetime have been
and ingrained in them this idea of the Creator God. So it feels so comfortable to worship and to
pray to God that people of Christians or atheists will often say that because they felt it
themselves that you you are inclined naturally to believe these things inclined naturally
to follow a God. And part of it might be our attachment to our parents. You know the idea of
following our parents. But we have we have many misperceptions about reality that are more
comfortable and more natural to us than reality. This is because of the habits that we've created,
the artificial constructs we've set up for ourselves. So we have to break these down often going
against our perception. This is important and it's very very very it's important for
Buddhist practice to question yourself. Someone wrote on the internet that's this saying that's
going around it's so pernicious because it sounds so good. It's one of those fake Buddha quotes.
There's actually a sign up now called fake Buddha quotes.com. I don't think I've ever been
to it but I'm sure it's full of things that I see constantly on my feed. And this one says like
don't believe don't believe what anyone tells you don't believe even what I say unless it agrees
with your own reason and your own common sense the Buddha. But but if and then they say it
comes from this the Kalamazoo. But if you go and look at the Kalamazoo to the Buddha's actually
says don't believe your own reason don't believe your own common sense. He basically says
don't even believe your own intellect don't even believe logic. He says when you know for yourself
that when you know, well here's just the word no when you know for yourself as though you see it
when you see for yourself that something is good for you take that up when you know for yourself
that something is bad for you and this means no not just think not just intuition you feel like
it's good right but when you know that it's bad for you or when you know that it's good for you
when you know it's bad for you give it up even though you might want it even though you might
scream and cry and your mind might scream to be given the things that you want even then give it up.
So that's what I had the advice helping us to see how the body is
is what it is helping us to let go of the body. Why would you want to think that the body is
suffering so that the I'm because you're dealing with a little child here this thing that want
this mind that doesn't have a clue what's good for it which what's of its what's to its benefit.
So you have to show it something that you already know intellectually the brain is already aware of it
but the heart is still chasing after these things facing after the body so you teach yourself
you change your habits and when you see that the body is suffering then you don't
chase after the body you don't worry about the body you don't concern you break your leg
my legs broken you get some some lethal some life when it went fatal disease you've got a fatal
disease we've all got one it's called life so here the Buddha talks about in this one the Buddha
talks about all sorts of sicknesses and just going over this is quite useful it helps you wake
up and say yeah it really not not the fun and games that we thought it was this body thing
which which doesn't make you depressed it in fact enlightens you it in it makes you
refreshed and invigorated you don't have to worry about the body anymore you realize that the
most important thing is to train yourself not to worry not to be concerned not to be upset when
the body causes suffering okay so let's go try to go a little quicker through the rest of this
next one Bahanas and yeah Bahana means abandoning
abandoning is is is the next stage of the practice when you after you see impermanence
suffering a non-soft Bahana is the giving up that comes from it
well and and here he's also talking about avoiding kind of like refusing to chase after
thoughts of sensuality thoughts of
ill will thoughts of oppression these have to do generally with other people
Kama is last when you have lust for or desire for generally another person but it could be anything
Vyapada means when you're upset about when you're angry at someone but it can also be angry about
but anything we Hings that refers to wanting to oppress other people so it has to do with arrogance
and self righteousness and conceit it can also be repressing yourself oppressing yourself you have
conceit against yourself meaning thinking little of yourself feeling yourself inferior
all of these thoughts should be done away with the Buddha said abandon them dispel them get rid
of them cause them to attain non-existence anambha one gameti causes them to go to extinction
and beyond that upa nupani papa kya kusaleda mean they do the same for any arisen
unholesome states get rid of them abandon them dispel them get rid of them
so you get rid of them you can get rid of them in different ways you can get rid of them by
their opposites so when you have anger you can think loving thoughts when you have lust you can think
of asumba i think of the repulsiveness of of the body gase anomanda kadanda ju
but ultimately the the best way to be free from these is
through the abandoning delusion the only way to give up delusion like conceit and arrogance
and so on another the delusion side which is really the root of of greed and anger in the first
place there's through insight through seeing clearly how do you dispel an angry thought
look at what it is that's making your angry when someone's yelling at you and it makes you very
angry say to yourself hearing hearing try it see what happened once you see hearing for hearing
and it's just sound it won't make you angry anymore it doesn't have the power to make you
angry anymore because you don't see it as a bad thing once you see things as they are you get rid
of these things so it's a part of the practice it's kind of like a signal for us to begin to
practice when you feel angry you realize oh you're not mindful that's why you're angry
then we have we raga and you know new raga new rada are actually referring to sort of the same thing
we raga is the abandoning of raga and new raga is the cessation of raga you can say of
tana you could say but they both refer to nibana if you look at this this is the this is calm
eightang sentang eightang beneath and this is calm this is excellent namely the calming of all
mental formations sabasanga and asamato meaning the non-arising of sanga coming to see that that's
true peace that there's a real difference between these two types of happiness the happiness
that comes from chasing from getting what you want and the happiness that comes from giving
a bonding they're really the two happiness we want to summarize happiness there's these two
getting what you want and giving a bonding and they're very different getting what you want is
fraught with stress and and excitement in the mind it's something that's actually painful if you
if you examine it if you watch it carefully if you watch it carefully you lose it right away
because you see it's there's no there's no substance to it it's born of delusion at the moment
when we want something we're deluded we're confused as soon as you unconfused yourself oh wait you
said you you'll say right away oh wait a minute that's not right it's it's it's almost magical
when you wake up and you're wanting something as you want the one thing or you look at it as
he's seeing seeing suddenly it's gone because you you're awake now remember there's that desire
craving wanting relies on delusion you could it can only arise when you're deluded hence the
importance of insight so and on the other hand we raga neuroda the happiness of nibana is
absolutely contrary to that it's it's complete
complete peace you have no no suffering it's it's something that you can't even imagine
you can't possibly imagine unless you've experienced it you see because if you if you think of
how long we've been in some sorrow without experiencing nibana we have no we have nothing to
compare it to we have no point of reference but at the moment that you experience nibana your
whole existence changes because now you have something to compare it to and you will never
believe that some sara can bring you happiness you lose all concept that some sara is
is a source for for happiness and this is why a person who's realized nibana is set to
enter into parynibana there's no way they can they can fail to attain parynibana because there's
no way that they can possibly go back to one now that they have in their experience the
cessation of suffering and so thinking of it is a useful tool to remind us because we still have
habits even even even one even after a person has attained nibana they will still have the habits
to cling to some sara so you remind yourself oh yes yes here I am getting off track again
when you think of what is true peace true happiness the next two are are quite interesting
nabiratas nya sambaloke nambiratas nya to not be
do not delight perception of non-delight in the whole world that's funny it's even this next
one is a mistake we have to correct that anyway the perception of non-delight so the Buddha
and the bikhu gives up all delight in the world the Buddha said sabe nama nala nabini rasaian
all damas are not worth clinging to there's no he said this is the basis of the Buddha's teaching
if you know for yourself if someone if someone comes to practice meditation and they want to know
what's the theory that I need before I practice the Buddha said if you know if you're clear on the
fact that no damas are worth clinging to nothing is worth clinging to then you've got enough if
you understand that this concept whether you agree with it or not then you have enough theory to
begin to put into practice the Buddha's teaching meaning this is the basis on which our practice
should should rest understanding this idea of clinging to things and testing out this hypothesis
that nothing is worth clinging to that's the framework within we should base our practice so that when
we approach moment to moment experience we see it in terms of the clinging and we examine
the state of clinging and reacting and so on and come to see why it is that nothing is worth
clinging to so we we look at what it is that we cling to what it is what is it in the world
that we cling to that we delight in and we give it up so these two actually go together and
I'll be right to the next one is not a need just I'm I'm quite sure it's Anitasanya Anita
Ita means that which is delightful Anita means that which is not delightful so Anabirata means
non-delight in the world and Anita means not worth delighting in so the the first one is
about the the fact that we don't delight in it and the second one is that Sankara is
or not worth delighting in when you come to see that Sankara is our when you become to be
ashamed and disgusted or disgusted by them just repulsed by them when you and it's it's not an
anger based but you become uninterested as we talked about yesterday anyway the final one is Anabandasati
Anabandasati is the Buddhist you might say the Buddhist favorite meditation technique the one that he
you might say recommended the most and it's because well it's it's also what was often practiced
even at the time probably before the Buddha but it's because the breath is always there it's the
most natural object when you're sitting still you want to contemplate reality what's the most
natural object well there's there's really very little besides the breath that you could be mindful of
you could be aware of the feeling of sitting on the ground you could be aware of the stress
in the back but the most obvious is the breath so the the benefit of Anabandasati
depends on how you approach it if you approach it as a concept breath going in breath going out it
can calm you but if you approach it as a reality the sensation caused by the breath going
into the breath going out which is the true experience the reality of the experience then it
leads to insight so this is why we have people focus on the rising and falling in the stomach it's
actually it's quite easy to understand this as Anabandasati mindfulness of breathing but it's a
form of mindfulness of breathing that is based on ultimate reality it's the actual experience of the
stomach getting larger in the stomach getting smaller you could also focus on the the heat and the
cold but if you're focusing on the going in and the going out it's a concept and it will only make
the mind calm to understand reality you have to use Anabandasati as a means of observing
ultimate reality the sensations the four elements Anabandasati is an incredible tool just
watching the stomach for example watching the stomach rising and falling just doing it a few
times this is an incredible benefit in the rising and the falling you can find all of the 37
bodhi pakya dhamma you can find all the whole truth of reality nibana can be attained just by
watching the stomach rise and fall it's that incredible people ask about how you you know the
practice meditation but they have a hard time seeing impermanent suffering and non-self except
intellectually and the funny thing is they often have a heart they say I have a hard time because
the stomach is you know it's not constant it's always changing you know it's it's stressful it's
not comfortable and you can't force it you can't control it you can't keep the breath it is so
so how are you going to see the truth of life if you have to deal with these right
so they totally totally missing the object of the practice the object is to see these things
first thing focusing on the stomach is one of the best ways to see impermanent suffering and
nonself to teach the mind that this is the way things work this is the way the stomach works
and everything else it's impermanent and suffering and it's nonself it's not worth clinging to
so just watching the stomach rise and fall will give you an incredible insight into impermanent
suffering and nonself into the nature of reality and lead you rapidly towards the goal of freedom
from suffering and peace and happiness so those are the tens on you it's quite a lot to take in but
whatever it's not that you have to memorize all tenonies and remember them but they're good
things to think of and so we've had some time now to reflect on them now hopefully we can put
at least some of them into practice and use them as a as at least an encouragement in our practice
reason to practice why should we be practicing because these are reality you can't deny any of
the things in those tens and you can't deny that they are the nature of reality and that many
of our understandings many of our perceptions of reality are false so Buddhism as I've said
before is a study your studying and why are you studying because you we have a very strong misunderstanding
about reality that's how we got here in the first place that's how we give that's why we have
our mental problems our sufferings our stresses in life because we have a profound misunderstanding
about reality how can you overcome a profound misunderstanding about reality you have to study
reality and as you study it you will overcome and give up and be free from this misunderstanding
that's the goal of meditation so without further ado let us meditate and study ourselves together
