1
00:00:00,000 --> 00:00:12,440
Hi and welcome back to Buddhism 101. Today we will be talking about the next aspect of

2
00:00:12,440 --> 00:00:20,480
the Buddha's teaching and that is the consequences of our actions. So we've talked

3
00:00:20,480 --> 00:00:31,880
about charity, we've talked about morality. The next step is to look at how sort of the

4
00:00:31,880 --> 00:00:42,640
why we should cultivate these good deeds, why we should abstain from unholesome deeds.

5
00:00:42,640 --> 00:00:52,600
This is because our actions have consequences, not only in the immediate sense, but over

6
00:00:52,600 --> 00:01:12,200
the long term as well. And Buddhist theory goes that our journey in this universe continues,

7
00:01:12,200 --> 00:01:22,000
meaning we have these experiences in this life. And no one disputes that. But from

8
00:01:22,000 --> 00:01:31,360
a Buddhist point of view, the experiences are the foundation of reality. So the reality

9
00:01:31,360 --> 00:01:41,920
around us, the time and space and so on. It's actually all conceptual. If you think of time,

10
00:01:41,920 --> 00:01:48,200
is something that you think of. You think of the past, you think of the future. When you

11
00:01:48,200 --> 00:01:55,920
think of space, you look at the room, this comes up in the mind, the mind is aware of

12
00:01:55,920 --> 00:02:04,640
the space. But the basis of all of that is experience. You're seeing, hearing, smelling,

13
00:02:04,640 --> 00:02:20,520
tasting, feeling, thinking. And so those experiences that we have continue moment after

14
00:02:20,520 --> 00:02:31,320
moment after moment and continue even after the breakup of the physical body. So if we

15
00:02:31,320 --> 00:02:40,680
look at another person and we see that their body has ceased to live, it's very hard

16
00:02:40,680 --> 00:02:46,520
to believe that there's anything going on there, that that mind is continuing because

17
00:02:46,520 --> 00:02:52,920
you can see all activity has ceased. Now this is if you look at things from an external

18
00:02:52,920 --> 00:02:57,080
point of view. But if you look at them from the point of view of your own experience,

19
00:02:57,080 --> 00:03:07,040
you can see your experiences creating new results. So if you're charitable and kind,

20
00:03:07,040 --> 00:03:12,920
it creates positive results. If you're moral and ethical, this helps you avoid negative

21
00:03:12,920 --> 00:03:27,360
results. If you're on the other hand, greedy, stingy, cruel and immoral, then likewise, this

22
00:03:27,360 --> 00:03:34,000
will bring other results. It changes and it creates a new aspect of your existence. Every

23
00:03:34,000 --> 00:03:41,440
moment we're creating and we're cultivating habits that not only change the world around us

24
00:03:41,440 --> 00:03:53,560
and change our environment, but also turn us and put us on a new pathway, because of

25
00:03:53,560 --> 00:04:01,120
the habits, they change our whole perception and our whole state of being. So this is

26
00:04:01,120 --> 00:04:09,240
the reason, this is the significance of our emphasis on wholesome deeds and the avoidance

27
00:04:09,240 --> 00:04:17,320
of unwholesome things, so practicing charity and cultivating morality. So how it works is

28
00:04:17,320 --> 00:04:24,680
when you die your whole life, they say flashes before your eyes. What this is, is the things

29
00:04:24,680 --> 00:04:31,440
in your mind that are most clear to you, the things in your mind that you hold on most strongly

30
00:04:31,440 --> 00:04:36,080
because the rest of the things becoming insignificant. The rest of your experiences, your

31
00:04:36,080 --> 00:04:41,120
day-to-day life, goes to the window. You're not thinking about your shopping, you're not

32
00:04:41,120 --> 00:04:47,000
thinking about your laundry. When you're dying, all of these things become insignificant and

33
00:04:47,000 --> 00:04:51,920
so they fade away. What's left is all the really important stuff and that's why it flashes

34
00:04:51,920 --> 00:04:56,640
up. You start to think of all the things you've done to hurt others or the good things

35
00:04:56,640 --> 00:05:01,120
you've done to help others, all the bad things people have done to you or the good things

36
00:05:01,120 --> 00:05:08,760
people have done for you, all of the important and emotional, emotionally charged aspects

37
00:05:08,760 --> 00:05:22,960
of your life come up. At that moment, the inclination is to cling to something. Absolutely,

38
00:05:22,960 --> 00:05:28,120
these are the things that we cling to. So something is going to grab your attention and you

39
00:05:28,120 --> 00:05:34,280
start to react to it. We do this in our life anyway. When we have a thought, we'll react

40
00:05:34,280 --> 00:05:38,720
to it and we'll say, oh yeah, I should do that. Right away, we start up a new ambition,

41
00:05:38,720 --> 00:05:44,760
a new train and start in a new journey. The moment of death, you see, all of the physical

42
00:05:44,760 --> 00:05:57,520
aspects of our being are shutting down. They also move out of the picture. So memories

43
00:05:57,520 --> 00:06:03,040
of this life and the brain, you know, stops working, all of the thoughts that would come

44
00:06:03,040 --> 00:06:10,520
from the brain. And all that's left is this. So in our life, well, we create new ambitions

45
00:06:10,520 --> 00:06:15,520
based on our desires and so on. That's part of it. But much of it is just our brain

46
00:06:15,520 --> 00:06:21,880
regurgitating memories and sensory perceptions. All of that is gone. All that we're

47
00:06:21,880 --> 00:06:32,960
left with is the mind. So the needs that we have done become very powerful. And the clinging

48
00:06:32,960 --> 00:06:40,040
that goes on becomes the catalyst. It's the only thing left. And so this is why when you

49
00:06:40,040 --> 00:06:48,320
start out, I mean, if you believe or if you follow Buddhist logic, reason and thought,

50
00:06:48,320 --> 00:06:54,320
you start off with something very simple. As a human being, you'll start off with a simple

51
00:06:54,320 --> 00:07:00,840
egg and a sperm, very small, because that's all that you've got. It's just the seed that

52
00:07:00,840 --> 00:07:07,920
you want to be a human again or the idea of being a human of the desire for human life.

53
00:07:07,920 --> 00:07:12,240
If you don't have that, there's different things that happen. Example, if you're full

54
00:07:12,240 --> 00:07:24,120
of anger, then when you die and you grasp onto an angry, unpleasant, displeased thought,

55
00:07:24,120 --> 00:07:30,840
then you're unborn as a human. You're born in a state of anger, a state of pain, a

56
00:07:30,840 --> 00:07:39,680
state of suffering. We would call the equivalent of the Judeo-Christian hell. Now, it's

57
00:07:39,680 --> 00:07:48,520
not permanent. I mean, these are states that come about based on your bent, your intentions,

58
00:07:48,520 --> 00:07:55,400
your mind-state when you die. So they last based on that, the power of that desire, just

59
00:07:55,400 --> 00:08:05,240
as any mind-state that comes from the mind. It has a power and it arises and it prevails

60
00:08:05,240 --> 00:08:11,320
and eventually it ceases. Now, it can last a long, long time, depending on how powerful

61
00:08:11,320 --> 00:08:20,400
is the evil intention, the angry, unwholesome, unpleasant, undispleased intention. But then it's

62
00:08:20,400 --> 00:08:30,960
over. But this is what would happen. Anger, anger, the result of anger, is to go to one

63
00:08:30,960 --> 00:08:39,480
of the many health. So any, you create a state, you enter into a state of pain and suffering.

64
00:08:39,480 --> 00:08:45,960
The person who's killed a lot of other beings is cruel and unpleasant, harsh speech, that

65
00:08:45,960 --> 00:08:56,920
kind of thing. If your mind is full of greed on the other hand, greed is what we normally

66
00:08:56,920 --> 00:09:02,760
associate with ghosts. That's in Buddhism, that's where it leads you. If you have a mindful

67
00:09:02,760 --> 00:09:08,600
of greed, desire, attachment, clinging to something. So if you hear these ghost stories about

68
00:09:08,600 --> 00:09:18,160
ghosts that are sent to haunt places that have meaning for them. Maybe they appear in some

69
00:09:18,160 --> 00:09:22,640
clothes that are associated with something they cling to and that kind of thing. This

70
00:09:22,640 --> 00:09:31,480
is the general nature of a ghost is that they are hungry, they want something. They are

71
00:09:31,480 --> 00:09:37,600
attached to something. They can let go. So if you live your life full of greed and when

72
00:09:37,600 --> 00:09:44,000
you die, that's the overwhelming emotion. And that's what you cling to is some greedy,

73
00:09:44,000 --> 00:09:53,120
lustful desire is thought. Being born as a ghost is the likely destination. If you have

74
00:09:53,120 --> 00:10:04,040
delusion in your mind, delusion being kind of confusion and arrogance, maybe you can see it.

75
00:10:04,040 --> 00:10:08,960
Wrong view or if you're a person who takes a lot of mind numbing drugs so that you just

76
00:10:08,960 --> 00:10:15,280
have a confused state of mind or if you are keen on stupidity and ignorance and that kind

77
00:10:15,280 --> 00:10:19,840
of thing. If that interests you and that's your bent, then when you die, if you die in

78
00:10:19,840 --> 00:10:26,720
a confused mind or if you die in a deluded mind, you're born as an animal. This is the state

79
00:10:26,720 --> 00:10:32,320
of animals. They're always sort of cloudy in the mind, confused and they can be very arrogant

80
00:10:32,320 --> 00:10:37,600
and conceited. But they don't have much wisdom or understanding. They're not able to comprehend

81
00:10:37,600 --> 00:10:50,000
deep concepts like meditation or spirituality, that kind of thing. Not really easy for them

82
00:10:50,000 --> 00:10:54,400
because their minds are clouded and that's what happens. That's where you can go in that

83
00:10:54,400 --> 00:10:59,840
case. Now an ordinary person who has ordinary thoughts will be born as a human being. That's

84
00:10:59,840 --> 00:11:04,320
sort of in the middle of somewhere. If you're not too evil of a person but not too good of a person,

85
00:11:04,320 --> 00:11:09,120
you'd probably be born as a human again and have to go through all the same sorts of things,

86
00:11:09,760 --> 00:11:15,760
give or take depending on your karma. There's very many variables. So if you

87
00:11:17,120 --> 00:11:21,840
you might be a sick person, you might be a healthy person, you might be a strong person,

88
00:11:21,840 --> 00:11:25,440
you might be an intelligent person, you know, all these very different variables.

89
00:11:26,560 --> 00:11:32,000
You may be rich, you may be poor or this kind of thing, but you'll be born a human.

90
00:11:32,000 --> 00:11:39,360
Now to be born in heaven, Buddhism has this concept of heaven, but it's just very much like

91
00:11:39,360 --> 00:11:46,240
being a human except nicer, more comfortable. So there are states, there are many, many different

92
00:11:46,240 --> 00:11:57,200
states and it's all a great, so it's degrees of happiness and suffering. If you're a very good

93
00:11:57,200 --> 00:12:04,960
person, a person who is by nature, kind, caring and helpful and engages in wholesomeness and

94
00:12:06,720 --> 00:12:12,480
refrains from unhulsomeness, abstains from unhulsomeness, then you can be pretty sure,

95
00:12:12,480 --> 00:12:19,200
it can be proud of yourself, happy about, you can be confident in yourself, reassured that you're

96
00:12:19,200 --> 00:12:26,960
going to a good place. People who are by nature good, who has their default, constantly thinking of

97
00:12:26,960 --> 00:12:32,720
ways to do good deeds and careful to avoid unhulsomeness like killing and stealing and all the

98
00:12:32,720 --> 00:12:41,200
things that I talked about with regards to morality and then they go to happen.

99
00:12:43,360 --> 00:12:47,520
So these are some of the results of mundane good deeds.

100
00:12:49,840 --> 00:12:55,280
Now, important out of all of this and what we'll talk about next time is

101
00:12:55,280 --> 00:13:01,120
not so much the idea of going to heaven and avoiding going to hell, but really an understanding

102
00:13:01,120 --> 00:13:06,880
of how the system works because it's an important framework on which to base our meditation

103
00:13:06,880 --> 00:13:14,560
practice. If we base our meditation practice on external, you know, reality, then we start to

104
00:13:14,560 --> 00:13:21,440
think of our mental illnesses, our mental problems as being a part of nature or a part of biology.

105
00:13:21,440 --> 00:13:33,840
And so not subject to our ability to change. So we lose the sense of self-responsibility

106
00:13:34,800 --> 00:13:39,120
and that's important, it's important to understand that that's not really how it works.

107
00:13:40,000 --> 00:13:47,920
That death is at the end, that we really have a reason to cultivate good and evil beyond just

108
00:13:47,920 --> 00:13:55,200
the simple, well, it's, you know, what good people do or it's to live a good life in this life.

109
00:13:55,200 --> 00:13:59,520
And because at the end, if death was it, then it would all be pointless. There would be no need to

110
00:13:59,520 --> 00:14:05,520
strive or worry or concern yourself about goodness or evil at all. And so that's how many of us live

111
00:14:05,520 --> 00:14:11,040
our lives. And as a result, we have a lot of problems in this world because people are all

112
00:14:11,040 --> 00:14:17,280
live for the moment. You only live once these kind of things. And as a result, regardless of what

113
00:14:17,280 --> 00:14:22,960
happens to them, we can see the degradation that it causes in the world that we live in. And that

114
00:14:22,960 --> 00:14:29,680
should be a sign for us, that we have perhaps more to the world than we think more to the universe

115
00:14:29,680 --> 00:14:36,640
than we think. And so this is an important for you to examine. You don't have to believe it right

116
00:14:36,640 --> 00:14:42,640
off but I would encourage you to consider that this might be the case because it's an important

117
00:14:42,640 --> 00:14:50,000
part of why we want to, you know, of cultivating the desire to develop meditation practice,

118
00:14:50,000 --> 00:14:58,000
which we'll be talking about in upcoming segments of Buddhism 101. But for now, this is just an

119
00:14:58,000 --> 00:15:05,120
outline of what happens to us in the future, where we go as a result of our deeds. What are the

120
00:15:05,120 --> 00:15:16,640
consequences of good and evil? So thank you for tuning in. We'll see you all in the best.

