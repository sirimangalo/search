1
00:00:00,000 --> 00:00:09,300
What do you think of modern education? Not much. I wanted to leave

2
00:00:09,300 --> 00:00:20,720
school when I was in grade 11, when I was 16 years old. My parents, my parents

3
00:00:20,720 --> 00:00:27,640
did something really neat and that took me out of school. When I was in

4
00:00:27,640 --> 00:00:35,040
kindergarten, when I finished kindergarten, I was going to grade one. How old

5
00:00:35,040 --> 00:00:41,080
that is six, seven, five, six, six. They decided that they were going to take me and

6
00:00:41,080 --> 00:00:46,720
my older brother out of school. Then we spent grades one till I spent grade one to

7
00:00:46,720 --> 00:00:54,960
eight at home. And it was pure torture. I mean it was really good actually. We

8
00:00:54,960 --> 00:00:59,760
learned a lot and we were free. Like we didn't do anything. Every day we had to

9
00:00:59,760 --> 00:01:05,640
do some math and some some reading and our parents would buy us all sorts of

10
00:01:05,640 --> 00:01:10,200
used books. They go to the used bookstores and buy boxes of used books. So we

11
00:01:10,200 --> 00:01:15,920
got to read a lot. But as a result, we learned so much and our minds became sharp

12
00:01:15,920 --> 00:01:22,480
and all four of us have gone on to do good things. And I think that's an

13
00:01:22,480 --> 00:01:27,880
important answer to your question. It shows part of the uselessness that I'm

14
00:01:27,880 --> 00:01:33,760
going to come to because that's really my conclusion is that and it goes back

15
00:01:33,760 --> 00:01:38,600
to your thorough quote is that modern education is really not nearly as

16
00:01:38,600 --> 00:01:43,840
useful as people think. The most useful thing that modern education brings is

17
00:01:43,840 --> 00:01:49,840
socialization. And as a result, I'm a social reject. You see, I wear rags for

18
00:01:49,840 --> 00:01:57,280
crying. I live in the forest and can't get along in society. And this isn't

19
00:01:57,280 --> 00:02:03,520
really what happens because my brothers are finding society. But there is a

20
00:02:03,520 --> 00:02:07,000
certain element of socialization, which you could call brainwashing you and

21
00:02:07,000 --> 00:02:13,280
it's kind of sad how it affects certain people. And it leads us to buy into so

22
00:02:13,280 --> 00:02:18,720
much that we would otherwise be free from. But it doesn't really teach you

23
00:02:18,720 --> 00:02:25,360
anything. I can verify that because I spent the first eight grades learning on

24
00:02:25,360 --> 00:02:29,480
my own basically. Our parents bought us a computer. I had a computer. We had a

25
00:02:29,480 --> 00:02:37,320
computer when I was six and they got all this neat software and so we learned a

26
00:02:37,320 --> 00:02:43,960
lot of stuff from the computer. And just by reading, we would read more than

27
00:02:43,960 --> 00:02:48,680
anyone. Kids don't like to read. Nowadays, no one likes to read.

28
00:02:48,680 --> 00:02:51,840
Because they weren't brought up like that. We were brought up reading. So we

29
00:02:51,840 --> 00:02:57,720
read, read, read, read so much. And that's an incredible way of becoming

30
00:02:57,720 --> 00:03:06,600
educated. It's just taking the example of great writers. So, but the great

31
00:03:06,600 --> 00:03:12,720
point is that you don't get a lot from school. I certainly didn't. And it became

32
00:03:12,720 --> 00:03:21,320
more and more pronounced. In grade 11, I just suddenly, without even any kind of

33
00:03:21,320 --> 00:03:28,520
view, it just suddenly hit me that I don't want to be in school. And it

34
00:03:28,520 --> 00:03:35,120
probably tied into the whole idea that I had that many people have that, well,

35
00:03:35,120 --> 00:03:40,160
what is school for? And it maybe I read it somewhere because it just seemed

36
00:03:40,160 --> 00:03:45,000
like it was a trap. You go to school to get it to get educated and the

37
00:03:45,000 --> 00:03:48,560
education is only for the purposes of getting a job. And when are you getting a

38
00:03:48,560 --> 00:03:52,440
job for it to make money? And you spend all this time making money and then you

39
00:03:52,440 --> 00:03:57,320
have to get a house and a car and whatever a family. And by the time you

40
00:03:57,320 --> 00:04:01,000
you have money and you have status and so on, you're too old to enjoy it. And

41
00:04:01,000 --> 00:04:06,600
then it's time to get old. No, and the point being that the happiness only is

42
00:04:06,600 --> 00:04:09,600
supposed to come when you retire. And by the time you retire, you're too old to

43
00:04:09,600 --> 00:04:13,360
enjoy it anyway. And you're just sharing a worry about sickness all the age and

44
00:04:13,360 --> 00:04:20,520
death. And that was really what was going through my mind at the time. That

45
00:04:20,520 --> 00:04:24,400
and I think I read the doubt, it changed around that time. And it became a

46
00:04:24,400 --> 00:04:32,960
Taoist. I wanted to go to meet the Dalai Lama and become a monk. So I sort of

47
00:04:32,960 --> 00:04:37,080
an answer as to what I think of education is basically that it's it's it's

48
00:04:37,080 --> 00:04:44,880
useless and it's just for the purposes of perpetuating uselessness. And that's a

49
00:04:44,880 --> 00:04:49,720
hard thing for people to swallow because it's it's so much a part of become

50
00:04:49,720 --> 00:04:55,400
so much a part of a reality for us, right? School. School is what kids do. Everyone

51
00:04:55,400 --> 00:05:02,800
goes to school. You not go to school. School is useless. Well, the monkeys go to

52
00:05:02,800 --> 00:05:07,720
school. Do, you know, what other animal goes to school? And what do we get out of

53
00:05:07,720 --> 00:05:20,120
school? We get society. We get economics. We get iPads. iPods, iPhones. I we

54
00:05:20,120 --> 00:05:28,200
get I know. I and look at look at what our education has brought us. The sky

55
00:05:28,200 --> 00:05:36,200
is polluted. The water is polluted. The trees are gone. The world's heating up hasn't

56
00:05:36,200 --> 00:05:41,000
brought us a lot of good. I'm not saying things like learning and wisdom and

57
00:05:41,000 --> 00:05:45,480
are not good, but they can be gained in such better ways. I mean, being a monk,

58
00:05:45,480 --> 00:05:49,000
that's one of the greatest things about being a monk. See what happened is I

59
00:05:49,000 --> 00:05:53,040
went to university thinking well, maybe university will be different.

60
00:05:53,040 --> 00:05:58,040
I spent half a year and did a lot of stupid things got involved with love. It's did

61
00:05:58,040 --> 00:06:04,280
some drugs and I'm like four different women I was interested in and got on all

62
00:06:04,280 --> 00:06:08,840
these committees and became involved so many doing just when everywhere. And

63
00:06:08,840 --> 00:06:12,400
realized that it wasn't any different. No, there was no matter what I did in

64
00:06:12,400 --> 00:06:18,040
which way I went, it was all meaningless. And so I vowed to myself that I'm

65
00:06:18,040 --> 00:06:22,680
going to learn, I'm going to find a way to learn without university. Because

66
00:06:22,680 --> 00:06:26,080
university wasn't learning for me and I actually made a big fuss about it.

67
00:06:26,080 --> 00:06:30,800
Before I left, I got all our fellow students in our program together. It was a

68
00:06:30,800 --> 00:06:35,400
special program for people who want to learn. You needed like a 92 average to get

69
00:06:35,400 --> 00:06:42,000
into the program and all these extra curriculars and so on. So I thought, wow,

70
00:06:42,000 --> 00:06:47,480
this is, wow, to get into this program was a good one. And so I made a big

71
00:06:47,480 --> 00:06:51,840
fuss when I left. I got everyone together and we started talking about what was

72
00:06:51,840 --> 00:06:55,680
wrong with the program and how there wasn't this was real learning. It was just

73
00:06:55,680 --> 00:07:01,320
rehashing old stuff. Whatever. And so anyway, I dropped out with the, the

74
00:07:01,320 --> 00:07:07,160
point is I dropped out with the idea of doing some real learning. And so the

75
00:07:07,160 --> 00:07:14,040
point I want to make is that I found that now. Being a monk is the ideal in

76
00:07:14,040 --> 00:07:19,560
every sense of the word, the ideal opportunity to learn. Manirata is going to

77
00:07:19,560 --> 00:07:26,400
university and learning is able to learn everything about what is, what is

78
00:07:26,400 --> 00:07:32,680
proper to learn. I mean, the Buddha's teaching itself is a profound body, a vast

79
00:07:32,680 --> 00:07:39,000
body of knowledge. So simply learning the truth and learning good things and

80
00:07:39,000 --> 00:07:43,240
learning about what is right and what is wrong and learning profound

81
00:07:43,240 --> 00:07:51,360
philosophy. I have the ideal opportunity to do that here. But of course there's

82
00:07:51,360 --> 00:07:54,680
a deeper meaning to that is that now I have the opportunity to do the real

83
00:07:54,680 --> 00:07:59,440
learning. I've learned what it is that is necessary to learn and that of

84
00:07:59,440 --> 00:08:04,160
course is learning about oneself. You could extrapolate on that on that and say

85
00:08:04,160 --> 00:08:09,280
there's so many incredible things that you can learn. I mean, what other school

86
00:08:09,280 --> 00:08:13,960
gives you the opportunity to learn how to fly through the air. Read people's minds,

87
00:08:13,960 --> 00:08:23,160
see heaven, see hell. Remember your past lives, what university has a curriculum,

88
00:08:23,160 --> 00:08:31,040
textbooks designed to teach people how to remember their past lives. We have that.

89
00:08:31,040 --> 00:08:35,560
If you want to remember your past lives, we've got a course for you and I don't

90
00:08:35,560 --> 00:08:42,360
teach it, but I teach a different subject because if finally if you want to be

91
00:08:42,360 --> 00:08:47,080
free from greed, anger and delusion and have none of these left to have no greed,

92
00:08:47,080 --> 00:08:52,600
no anger and delusion in your mind ever and no possibility of it ever to come

93
00:08:52,600 --> 00:08:56,200
back. We've got a course for that. We've got curriculum and we've got textbooks

94
00:08:56,200 --> 00:09:05,920
and we've got teachers. School of Xavier teaches that. I don't know what that is, but

95
00:09:05,920 --> 00:09:29,080
I'm sorry. No, I don't know what you're doing.

