1
00:00:00,000 --> 00:00:07,000
Hello, and welcome back to our study of the Dampanda.

2
00:00:07,000 --> 00:00:14,000
Today we begin the Balawaga, the chapter on fools.

3
00:00:14,000 --> 00:00:20,000
So all the vous vous vous vous vous in here, in this chapter we'll have something to do

4
00:00:20,000 --> 00:00:34,000
with fools. We're foolishness. It's verse 61 today, which reads as follows.

5
00:00:34,000 --> 00:00:43,000
Now, we're going to see the Dampanda.

6
00:00:43,000 --> 00:00:49,000
We're going to see the Dampanda.

7
00:00:49,000 --> 00:00:57,000
We're going to see the Dampanda.

8
00:00:57,000 --> 00:01:06,000
Wondering or faring through some sorrow, one doesn't find someone who is one's equal or better,

9
00:01:06,000 --> 00:01:11,000
or at least equal to one's self.

10
00:01:11,000 --> 00:01:21,000
We're going to see the Dampanda.

11
00:01:21,000 --> 00:01:30,000
We're going to see the Dampanda.

12
00:01:30,000 --> 00:01:39,000
We're going to see the Dampanda.

13
00:01:39,000 --> 00:01:46,000
The story behind this verse goes that when the Buddha was dwelling in Sawati,

14
00:01:46,000 --> 00:01:50,000
Mahakasapa was staying in his cave above Rajagahat,

15
00:01:50,000 --> 00:01:53,000
which you can still see today if you go there.

16
00:01:53,000 --> 00:01:59,000
Apparently they have a place that they pinpointed as his cave.

17
00:01:59,000 --> 00:02:03,000
Anyway, you can still go into Rajagahat.

18
00:02:03,000 --> 00:02:12,000
Up on the mountain above Rajagahat, he would go every day down the mountain into the city for arms.

19
00:02:12,000 --> 00:02:19,000
At the time, he had two students, two novices, staying with him.

20
00:02:19,000 --> 00:02:21,000
They would attend upon him.

21
00:02:21,000 --> 00:02:32,000
One of them was a good student, and one of them was a rather recalcitrant student.

22
00:02:32,000 --> 00:02:41,000
He was lazy in performing his duties and in practicing the Dhamma.

23
00:02:41,000 --> 00:02:48,000
And so how it would go is the one student who was quite diligent and faithful

24
00:02:48,000 --> 00:02:55,000
and obedient and so on.

25
00:02:55,000 --> 00:03:03,000
He would prepare the elder's water in the morning and his tooth sticks,

26
00:03:03,000 --> 00:03:06,000
and he would wait upon his elder and perform all these duties.

27
00:03:06,000 --> 00:03:13,000
He would heat up water for the elder's bath and leave the water out in the morning.

28
00:03:13,000 --> 00:03:18,000
And the other disciple would watch and would come or would sleep in

29
00:03:18,000 --> 00:03:21,000
and then would come out when everything was done.

30
00:03:21,000 --> 00:03:26,000
He would see if everything was done and go to tell the elder,

31
00:03:26,000 --> 00:03:31,000
but in both the tooth sticks have been put out and water has been put out for washing

32
00:03:31,000 --> 00:03:39,000
and for bathing, please come and it's time to do your morning routine.

33
00:03:39,000 --> 00:03:43,000
And so the elder, as a result, got to thinking that this student

34
00:03:43,000 --> 00:03:50,000
who kept coming to tell him and invite him to use the things that had been set out for him

35
00:03:50,000 --> 00:03:53,000
and had performed duties himself.

36
00:03:53,000 --> 00:03:57,000
And the other student who had actually done all the work sat back and watched

37
00:03:57,000 --> 00:04:01,000
and after some time realized what this, the other student was doing

38
00:04:01,000 --> 00:04:04,000
that he was taking credit for the work that he hadn't done.

39
00:04:04,000 --> 00:04:07,000
He said, well, I know what to do then.

40
00:04:07,000 --> 00:04:11,000
And so he, in the morning, when it was time for the elder's bath,

41
00:04:11,000 --> 00:04:17,000
he went to the room where the water,

42
00:04:17,000 --> 00:04:21,000
and he put just a little bit of water in the bottom of the big pot,

43
00:04:21,000 --> 00:04:23,000
the big cauldron for boiling bath water.

44
00:04:23,000 --> 00:04:25,000
And he did that.

45
00:04:25,000 --> 00:04:29,000
And so we could see the steam coming up, but there was very little water in it.

46
00:04:29,000 --> 00:04:33,000
And then the other novice, when he came over and saw the steam coming out,

47
00:04:33,000 --> 00:04:35,000
he said, oh, I must have put out the water.

48
00:04:35,000 --> 00:04:37,000
And he went to tell the elder,

49
00:04:37,000 --> 00:04:39,000
Mr. Bocher, your bath water is ready.

50
00:04:39,000 --> 00:04:40,000
Come and take your bath.

51
00:04:40,000 --> 00:04:44,000
And the elder went into the bathroom and said, okay, bring out the water.

52
00:04:44,000 --> 00:04:50,000
And the novice went into the boiling room

53
00:04:50,000 --> 00:04:53,000
and looked in the cauldron and realized that there was just a very little bit of water

54
00:04:53,000 --> 00:04:55,000
and got very angry and shouted.

55
00:04:55,000 --> 00:04:57,000
And he said, that's scoundrel.

56
00:04:57,000 --> 00:05:00,000
He only put a very little bit of water in.

57
00:05:00,000 --> 00:05:03,000
And so he rushed off to the river to get more water.

58
00:05:03,000 --> 00:05:09,000
Where the river was, rushed off to get water from wherever they get the water.

59
00:05:09,000 --> 00:05:14,000
Maybe he must have had to gone down into Rajika had to go.

60
00:05:14,000 --> 00:05:18,000
And the elder watched him go.

61
00:05:18,000 --> 00:05:21,000
And when he came back, the elder said,

62
00:05:21,000 --> 00:05:24,000
what did you mean by that?

63
00:05:24,000 --> 00:05:27,000
Are you saying that you didn't do any of this?

64
00:05:27,000 --> 00:05:29,000
And you've been taking credit for this all the time.

65
00:05:29,000 --> 00:05:32,000
And he said, that's not, and the elder scolded him.

66
00:05:32,000 --> 00:05:35,000
And so that's not how a monk should behave.

67
00:05:35,000 --> 00:05:38,000
One should not take credit for something that one hasn't done.

68
00:05:38,000 --> 00:05:45,000
And the novice got very angry and went off enough.

69
00:05:45,000 --> 00:05:49,000
And began to develop a grudge against the elder.

70
00:05:49,000 --> 00:05:52,000
The next day, or on subsequent days,

71
00:05:52,000 --> 00:05:54,000
they went off.

72
00:05:54,000 --> 00:05:56,000
When they went off for alms,

73
00:05:56,000 --> 00:05:59,000
I guess the next day, when they went for alms,

74
00:05:59,000 --> 00:06:00,000
he has a result.

75
00:06:00,000 --> 00:06:05,000
He decided not to go on alms with the elder.

76
00:06:05,000 --> 00:06:08,000
And so when the elder and the other novice went off for alms,

77
00:06:08,000 --> 00:06:09,000
they went alone.

78
00:06:09,000 --> 00:06:13,000
And the disobedient novice, he stayed back

79
00:06:13,000 --> 00:06:17,000
and was just sulked at the kuti at the cave.

80
00:06:17,000 --> 00:06:20,000
And the elder went with his other novice.

81
00:06:20,000 --> 00:06:22,000
And then when they were gone,

82
00:06:22,000 --> 00:06:27,000
he went off to one of the elder's supporters' houses.

83
00:06:27,000 --> 00:06:29,000
He went down into Rajika had his own

84
00:06:29,000 --> 00:06:31,000
and went to one of the elder supporters

85
00:06:31,000 --> 00:06:34,000
and said to them that the elder was sick.

86
00:06:34,000 --> 00:06:36,000
He said, oh, where's the elder?

87
00:06:36,000 --> 00:06:37,000
Why are you coming along?

88
00:06:37,000 --> 00:06:38,000
He said, oh, he's sick.

89
00:06:38,000 --> 00:06:40,000
But he needs some special food.

90
00:06:40,000 --> 00:06:44,000
And please give him very delicate

91
00:06:44,000 --> 00:06:47,000
and good tasting food and so on,

92
00:06:47,000 --> 00:06:50,000
so he can recover from his sickness.

93
00:06:50,000 --> 00:06:52,000
And so they gave him all this special food

94
00:06:52,000 --> 00:06:54,000
and prepared all this special food for him.

95
00:06:54,000 --> 00:06:58,000
And he took it back and aided himself.

96
00:06:58,000 --> 00:07:03,000
And the elder went on alms around with the other novice

97
00:07:03,000 --> 00:07:06,000
and happened to get a full set of robes,

98
00:07:06,000 --> 00:07:09,000
new nice robes from one family.

99
00:07:09,000 --> 00:07:11,000
And so he gave them to the novice

100
00:07:11,000 --> 00:07:13,000
that went with him.

101
00:07:13,000 --> 00:07:15,000
And the novice changed out of his own set

102
00:07:15,000 --> 00:07:18,000
and put on this new set of robes.

103
00:07:18,000 --> 00:07:21,000
And they went back to the monastery.

104
00:07:21,000 --> 00:07:23,000
On yet another day,

105
00:07:23,000 --> 00:07:27,000
the elder went to see, happened to go to see this supporter

106
00:07:27,000 --> 00:07:29,000
who had been tricked.

107
00:07:29,000 --> 00:07:31,000
And they said to the elder, oh, are you venerable,

108
00:07:31,000 --> 00:07:32,000
sir, how are you doing?

109
00:07:32,000 --> 00:07:34,000
We heard that you were sick.

110
00:07:34,000 --> 00:07:37,000
But we gave food to the novice

111
00:07:37,000 --> 00:07:44,000
when he came, hope the food helped you to get better.

112
00:07:44,000 --> 00:07:46,000
And the elder just stood there,

113
00:07:46,000 --> 00:07:47,000
didn't say anything.

114
00:07:47,000 --> 00:07:49,000
We received food from him and went off.

115
00:07:49,000 --> 00:07:51,000
And they went back to the monastery

116
00:07:51,000 --> 00:07:53,000
and he confronted this novice.

117
00:07:53,000 --> 00:07:58,000
And he said to him, is it true that you've been going around?

118
00:07:58,000 --> 00:08:00,000
Is it true that you ended these people and told them

119
00:08:00,000 --> 00:08:02,000
that I was sick and convinced them

120
00:08:02,000 --> 00:08:03,000
to give you all this special food?

121
00:08:03,000 --> 00:08:05,000
And he said, what do you mean?

122
00:08:05,000 --> 00:08:09,000
It's just, and he scolded him.

123
00:08:09,000 --> 00:08:11,000
And my husband scolded him and said,

124
00:08:11,000 --> 00:08:13,000
that's not something that a monk should do.

125
00:08:13,000 --> 00:08:17,000
And the novice was, he was totally fed up at this point.

126
00:08:17,000 --> 00:08:18,000
He said, over a little bit of water,

127
00:08:18,000 --> 00:08:19,000
the elder gets upset.

128
00:08:19,000 --> 00:08:21,000
And then he gets upset about a little bit of food.

129
00:08:21,000 --> 00:08:23,000
And on top of that, he's been giving,

130
00:08:23,000 --> 00:08:25,000
he gives a robe a full set of robes

131
00:08:25,000 --> 00:08:26,000
to the other novice.

132
00:08:26,000 --> 00:08:29,000
So this novice got very, very angry

133
00:08:29,000 --> 00:08:32,000
and then he said, I know what I'm going to do.

134
00:08:32,000 --> 00:08:35,000
And the next morning when the elder went off on Armstrong,

135
00:08:35,000 --> 00:08:39,000
he went around the cave and broke up all the pots

136
00:08:39,000 --> 00:08:42,000
and everything, just tore up everything

137
00:08:42,000 --> 00:08:44,000
and ripped up everything, all the bedding and everything.

138
00:08:44,000 --> 00:08:46,000
And then set the whole place on fire.

139
00:08:46,000 --> 00:08:48,000
And then he ran off.

140
00:08:48,000 --> 00:08:52,000
And I think ended up in hell as a result.

141
00:08:52,000 --> 00:08:55,000
But the elder came back and saw this

142
00:08:55,000 --> 00:08:59,000
and fixed it all up as best he could.

143
00:08:59,000 --> 00:09:02,000
And of course, went on with his life.

144
00:09:02,000 --> 00:09:05,000
So this is a story of these two novices.

145
00:09:05,000 --> 00:09:07,000
One who was a good companion.

146
00:09:07,000 --> 00:09:09,000
And there's a bad companion.

147
00:09:09,000 --> 00:09:11,000
And eventually, the story got back to the Buddha,

148
00:09:11,000 --> 00:09:16,000
one of the monks from Rajagat wandered all the way to Sawati

149
00:09:16,000 --> 00:09:20,000
and Buddha asked him, how is Mahakasupa doing?

150
00:09:20,000 --> 00:09:22,000
And he said, oh, he's doing fine.

151
00:09:22,000 --> 00:09:25,000
He happened to have these two students.

152
00:09:25,000 --> 00:09:27,000
And one of them was very, very good.

153
00:09:27,000 --> 00:09:32,000
And followed the elder's instruction and advice

154
00:09:32,000 --> 00:09:34,000
and practiced the elder's teachings.

155
00:09:34,000 --> 00:09:38,000
But the other one was an evil scoundrel.

156
00:09:38,000 --> 00:09:41,000
And not only did he not practice,

157
00:09:41,000 --> 00:09:45,000
but he also ended up destroying the elder's residence.

158
00:09:48,000 --> 00:09:50,000
And the Buddha said, oh, yes, yes.

159
00:09:50,000 --> 00:09:53,000
The elder is better off without this novice.

160
00:09:53,000 --> 00:09:55,000
And he told a story of the past.

161
00:09:55,000 --> 00:09:57,000
This is the introduction to one of the Jatakas stories,

162
00:09:57,000 --> 00:09:58,000
where he was a monkey.

163
00:09:58,000 --> 00:10:00,000
And he tore up this bird's nest.

164
00:10:00,000 --> 00:10:01,000
I'm not going to tell this story.

165
00:10:01,000 --> 00:10:02,000
It's not much to it.

166
00:10:02,000 --> 00:10:07,000
But he said, basically, this novice, this is the way he was.

167
00:10:07,000 --> 00:10:10,000
And he said, better off without him.

168
00:10:10,000 --> 00:10:12,000
He said, there's no association with fools.

169
00:10:12,000 --> 00:10:15,000
If the only people you can hang out with

170
00:10:15,000 --> 00:10:19,000
and rely upon are fools while you're better off without them.

171
00:10:19,000 --> 00:10:22,000
And then he told this verse.

172
00:10:22,000 --> 00:10:23,000
So a simple story.

173
00:10:23,000 --> 00:10:25,000
A story that I'm sure we can all relate to.

174
00:10:25,000 --> 00:10:27,000
That's all living in caves,

175
00:10:27,000 --> 00:10:30,000
having novices attend upon us.

176
00:10:30,000 --> 00:10:32,000
No, all of us having good friends and bad friends,

177
00:10:32,000 --> 00:10:34,000
people who we can rely upon

178
00:10:34,000 --> 00:10:36,000
and people who we shouldn't rely upon,

179
00:10:36,000 --> 00:10:41,000
people who help us up and people who drag us down.

180
00:10:41,000 --> 00:10:45,000
And so as meditators,

181
00:10:45,000 --> 00:10:50,000
this is one of the most important aspects

182
00:10:50,000 --> 00:10:53,000
of our practice is the association with good people.

183
00:10:53,000 --> 00:10:55,000
I'm going to send it's all of the holy life,

184
00:10:55,000 --> 00:10:57,000
all of the spiritual life.

185
00:10:57,000 --> 00:11:00,000
The spiritual life is all about associating with the right people,

186
00:11:00,000 --> 00:11:03,000
having a good friend, having someone to guide you

187
00:11:03,000 --> 00:11:06,000
and teach you and raise you up.

188
00:11:06,000 --> 00:11:11,000
In many places, he said that friends are good friends

189
00:11:11,000 --> 00:11:15,000
and most valuable and do not associate with fools.

190
00:11:15,000 --> 00:11:17,000
Of course, this is the famous, the mongolisture,

191
00:11:17,000 --> 00:11:21,000
the first thing that the Buddha says is not associating with fools.

192
00:11:21,000 --> 00:11:24,000
This is the greatest blessing.

193
00:11:24,000 --> 00:11:27,000
So how does this go?

194
00:11:27,000 --> 00:11:30,000
The verse is broken down quite well by the commentary.

195
00:11:30,000 --> 00:11:34,000
So I'll go through that first and then talk a little bit about

196
00:11:34,000 --> 00:11:37,000
how it relates to meditation, particularly.

197
00:11:37,000 --> 00:11:41,000
First of all, the word jarang, jarang,

198
00:11:41,000 --> 00:11:45,000
which means to fare, to wander, to travel,

199
00:11:45,000 --> 00:11:51,000
to move about, to live your life, basically.

200
00:11:51,000 --> 00:11:56,000
The commentary says it doesn't refer to moving about with your body.

201
00:11:56,000 --> 00:12:02,000
It refers to the movements of the mind or the way of the mind.

202
00:12:02,000 --> 00:12:05,000
So there's nothing to do with being close to someone.

203
00:12:05,000 --> 00:12:08,000
It's not like if you shouldn't sit on the subway next to someone

204
00:12:08,000 --> 00:12:10,000
unless you're sure they're a wise person.

205
00:12:10,000 --> 00:12:14,000
It also doesn't necessarily mean that you can't work or live

206
00:12:14,000 --> 00:12:17,000
or go habitat with such people.

207
00:12:17,000 --> 00:12:23,000
So it says manasach, which means the fairing of the mind.

208
00:12:23,000 --> 00:12:26,000
Don't let them into your heart, basically.

209
00:12:26,000 --> 00:12:34,000
Don't take their counsel or take them up as your intimate associates.

210
00:12:34,000 --> 00:12:41,000
And then what is meant by Sayyang, Sadhisahmata,

211
00:12:41,000 --> 00:12:43,000
and what is meant by one who is greater than you,

212
00:12:43,000 --> 00:12:45,000
or one who is equal to you.

213
00:12:45,000 --> 00:12:48,000
The commentary says it's specifically in regards to

214
00:12:48,000 --> 00:12:52,000
Sila Samadipanya, morality, concentration, and wisdom.

215
00:12:52,000 --> 00:12:55,000
So if someone has greater morality than you,

216
00:12:55,000 --> 00:12:58,000
if there's someone who is practicing higher morality,

217
00:12:58,000 --> 00:13:02,000
for instance, monks who are practicing celibacy and poverty

218
00:13:02,000 --> 00:13:15,000
and who are taking themselves out of all of the distracting activities

219
00:13:15,000 --> 00:13:17,000
that lay people who engage in,

220
00:13:17,000 --> 00:13:21,000
then it's something that can help us to cultivate concentration ourselves.

221
00:13:21,000 --> 00:13:23,000
If we associate with such people,

222
00:13:23,000 --> 00:13:30,000
if we are around people who are around people who are equal to us,

223
00:13:30,000 --> 00:13:35,000
then it will not, our morality will not fade away.

224
00:13:35,000 --> 00:13:38,000
If we have, for around people who are greater concentration,

225
00:13:38,000 --> 00:13:40,000
then their concentration will be a support for us.

226
00:13:40,000 --> 00:13:42,000
It will be an example for us.

227
00:13:42,000 --> 00:13:48,000
It will be something that we can rely upon or we can emulate

228
00:13:48,000 --> 00:13:50,000
to cultivate concentration ourselves.

229
00:13:50,000 --> 00:13:52,000
And if we hang around people who have wisdom,

230
00:13:52,000 --> 00:13:59,000
greater wisdom than us, then it will be something that inspires us

231
00:13:59,000 --> 00:14:04,000
because the reason for hanging out with someone

232
00:14:04,000 --> 00:14:07,000
who associate with people who are greater than you

233
00:14:07,000 --> 00:14:11,000
because you will develop what the good qualities

234
00:14:11,000 --> 00:14:14,000
of morality, concentration, wisdom will increase

235
00:14:14,000 --> 00:14:19,000
if we are around people who are a greater morality, concentration,

236
00:14:19,000 --> 00:14:20,000
and wisdom than us.

237
00:14:20,000 --> 00:14:23,000
And if we are around people who are equal,

238
00:14:23,000 --> 00:14:25,000
an equal level to us,

239
00:14:25,000 --> 00:14:28,000
then we can be assured that we at least won't follow us.

240
00:14:28,000 --> 00:14:31,000
At least won't follow away from our morality, concentration,

241
00:14:31,000 --> 00:14:33,000
wisdom, and as a result through our own practice,

242
00:14:33,000 --> 00:14:38,000
we'll be able to develop.

243
00:14:38,000 --> 00:14:41,000
The point is that the people around us won't drag us down.

244
00:14:41,000 --> 00:14:44,000
If you're around people who are inferior to you,

245
00:14:44,000 --> 00:14:47,000
and if you rely upon them, and if you emulate them

246
00:14:47,000 --> 00:14:55,000
and cultivate their qualities, then you will follow away from your own morality

247
00:14:55,000 --> 00:14:58,000
and concentration, because it comes under attack,

248
00:14:58,000 --> 00:15:02,000
if you let them into clothes.

249
00:15:02,000 --> 00:15:05,000
Now, if you can't find someone who is greater,

250
00:15:05,000 --> 00:15:07,000
someone who is equal to the Buddha said,

251
00:15:07,000 --> 00:15:11,000
actually, it's preferable to hang out on your own.

252
00:15:11,000 --> 00:15:17,000
Now, the key word here is Dalhang,

253
00:15:17,000 --> 00:15:20,000
because Dalhang means steadfast to resolute

254
00:15:20,000 --> 00:15:23,000
and the point is that you actually need resolution

255
00:15:23,000 --> 00:15:25,000
if you're on your own.

256
00:15:25,000 --> 00:15:29,000
Many people are always contacting us constantly,

257
00:15:29,000 --> 00:15:32,000
even get these emails from people saying that they're all alone

258
00:15:32,000 --> 00:15:35,000
and they need some guidance and the moaning,

259
00:15:35,000 --> 00:15:38,000
the fact that they're not able to find a sangha community.

260
00:15:38,000 --> 00:15:40,000
And the Buddhists give reassurance,

261
00:15:40,000 --> 00:15:43,000
and we should reassure such people that you can practice on your own.

262
00:15:43,000 --> 00:15:46,000
You don't really need so much guidance.

263
00:15:46,000 --> 00:15:49,000
It just takes more resolution,

264
00:15:49,000 --> 00:15:51,000
because if you're not surrounded by examples

265
00:15:51,000 --> 00:15:54,000
or people who are reminding you to

266
00:15:54,000 --> 00:15:56,000
remind you of what you're doing,

267
00:15:56,000 --> 00:15:59,000
reminding you of the cultivation of greater morality, concentration,

268
00:15:59,000 --> 00:16:02,000
wisdom, it's easier for yourself to fall away.

269
00:16:02,000 --> 00:16:07,000
It's like a plant or a young tree that requires a cage

270
00:16:07,000 --> 00:16:10,000
or some kind of support to keep it from falling over

271
00:16:10,000 --> 00:16:13,000
to the people from breaking in the wind and so on.

272
00:16:13,000 --> 00:16:15,000
That support is helpful,

273
00:16:15,000 --> 00:16:20,000
unless the tree itself is able to maintain its own strength

274
00:16:20,000 --> 00:16:22,000
and grow on its own.

275
00:16:22,000 --> 00:16:29,000
So the benefit of having a good companion is that they support you

276
00:16:29,000 --> 00:16:31,000
through your growth.

277
00:16:31,000 --> 00:16:33,000
Not Tim Bali, Sahayata.

278
00:16:33,000 --> 00:16:37,000
Why is it that we should stay on our own

279
00:16:37,000 --> 00:16:39,000
if we can't find such a person?

280
00:16:39,000 --> 00:16:41,000
Because the association with fools.

281
00:16:41,000 --> 00:16:43,000
There is no association with fools.

282
00:16:43,000 --> 00:16:46,000
The meaning being that none of the good things that come

283
00:16:46,000 --> 00:16:51,000
from associating with good people, morality, concentration,

284
00:16:51,000 --> 00:16:55,000
wisdom, all of the good katawat through the ten types

285
00:16:55,000 --> 00:16:58,000
of profitable speech.

286
00:16:58,000 --> 00:17:02,000
All of the dutanga practices, all of the

287
00:17:02,000 --> 00:17:04,000
we passed on the inside.

288
00:17:04,000 --> 00:17:07,000
And even the attainment of Nibana is difficult.

289
00:17:07,000 --> 00:17:11,000
It becomes difficult to become threatened by fools.

290
00:17:11,000 --> 00:17:13,000
Maybe surrounded by people who are distracting you

291
00:17:13,000 --> 00:17:17,000
or are talking about useless things and who are indulging

292
00:17:17,000 --> 00:17:21,000
in harmful pursuits, killing and stealing and lying

293
00:17:21,000 --> 00:17:23,000
and cheating and so on.

294
00:17:23,000 --> 00:17:25,000
All of these things will drag you down.

295
00:17:25,000 --> 00:17:27,000
We'll make it more difficult for you to practice

296
00:17:27,000 --> 00:17:32,000
and we'll even cause you to follow away from the practice.

297
00:17:32,000 --> 00:17:35,000
So this is quite a simple meaning,

298
00:17:35,000 --> 00:17:42,000
but specifically in regards to the meditation practice.

299
00:17:42,000 --> 00:17:46,000
The point is that people will distract us.

300
00:17:46,000 --> 00:17:50,000
People will be a cause for us to study your mind.

301
00:17:50,000 --> 00:17:54,000
The question that always comes up is whether or not

302
00:17:54,000 --> 00:17:59,000
this means that we should ignore people who are in trouble

303
00:17:59,000 --> 00:18:02,000
morally or have poor concentration.

304
00:18:02,000 --> 00:18:05,000
People who are on the wrong path shouldn't we help such people.

305
00:18:05,000 --> 00:18:07,000
And of course this is the exception.

306
00:18:07,000 --> 00:18:12,000
The Buddha said Anyatra Anundaya Anyatra Anukampa,

307
00:18:12,000 --> 00:18:16,000
which means that you shouldn't hang out

308
00:18:16,000 --> 00:18:20,000
Aetang or whatever.

309
00:18:20,000 --> 00:18:22,000
Aetang Purisa.

310
00:18:22,000 --> 00:18:23,000
Aetang Purisa.

311
00:18:23,000 --> 00:18:24,000
Aetang Purisa.

312
00:18:24,000 --> 00:18:27,000
So, Purisa, the Savitable.

313
00:18:27,000 --> 00:18:29,000
Such a person should not be,

314
00:18:29,000 --> 00:18:32,000
should not be associated with.

315
00:18:32,000 --> 00:18:36,000
Except in the case of pity or compassion.

316
00:18:36,000 --> 00:18:38,000
So, the commentary explains,

317
00:18:38,000 --> 00:18:41,000
which is exactly how it should be explained,

318
00:18:41,000 --> 00:18:43,000
that if you have compassion,

319
00:18:43,000 --> 00:18:45,000
if out of compassion for someone,

320
00:18:45,000 --> 00:18:47,000
it's possible or not,

321
00:18:47,000 --> 00:18:48,000
we should have the compassion.

322
00:18:48,000 --> 00:18:51,000
So if you think that a person is capable

323
00:18:51,000 --> 00:18:54,000
and you think you're able to help them,

324
00:18:54,000 --> 00:18:56,000
then you should hang out with them

325
00:18:56,000 --> 00:18:58,000
and you should try to support their practice

326
00:18:58,000 --> 00:19:00,000
and you should help them to cultivate.

327
00:19:00,000 --> 00:19:02,000
If you're not able to,

328
00:19:02,000 --> 00:19:04,000
if you try and you fail

329
00:19:04,000 --> 00:19:07,000
and it seems like this person is not going to,

330
00:19:07,000 --> 00:19:10,000
I'm not going to develop themselves,

331
00:19:10,000 --> 00:19:12,000
then of course,

332
00:19:12,000 --> 00:19:15,000
then you should stay away from them actually

333
00:19:15,000 --> 00:19:17,000
and you should go your own way

334
00:19:17,000 --> 00:19:19,000
because they will drag you down.

335
00:19:19,000 --> 00:19:20,000
So, in meditation,

336
00:19:20,000 --> 00:19:23,000
this is especially important

337
00:19:23,000 --> 00:19:25,000
and this is why,

338
00:19:25,000 --> 00:19:29,000
because meditation is the direct cultivation

339
00:19:29,000 --> 00:19:32,000
of wholesome mental states.

340
00:19:32,000 --> 00:19:34,000
So, if you're constantly,

341
00:19:34,000 --> 00:19:37,000
if you're distracted

342
00:19:37,000 --> 00:19:40,000
or led astray by other people,

343
00:19:40,000 --> 00:19:44,000
then your mind becomes impossible

344
00:19:44,000 --> 00:19:47,000
to cultivate wholesome states of mind.

345
00:19:47,000 --> 00:19:56,000
This is why the Buddha often talks about staying alone,

346
00:19:56,000 --> 00:19:58,000
even away from good friends,

347
00:19:58,000 --> 00:20:00,000
because even wholesome talk,

348
00:20:00,000 --> 00:20:08,000
if engaged in over much,

349
00:20:08,000 --> 00:20:13,000
can lead to distraction as well.

350
00:20:13,000 --> 00:20:17,000
Because meditation is the cultivation of the mind

351
00:20:17,000 --> 00:20:22,000
in our environment

352
00:20:22,000 --> 00:20:26,000
and our engagement with the environment

353
00:20:26,000 --> 00:20:31,000
and with the world around us

354
00:20:31,000 --> 00:20:35,000
is at most important

355
00:20:35,000 --> 00:20:39,000
so we have to be careful how we engage with the world around us.

356
00:20:39,000 --> 00:20:42,000
So, for a meditator,

357
00:20:42,000 --> 00:20:44,000
even good people,

358
00:20:44,000 --> 00:20:46,000
we should only engage in moderation

359
00:20:46,000 --> 00:20:50,000
and much less much more so.

360
00:20:50,000 --> 00:20:55,000
We should be careful when we associate

361
00:20:55,000 --> 00:21:01,000
or they interact with foolish people.

362
00:21:01,000 --> 00:21:03,000
They will distract us,

363
00:21:03,000 --> 00:21:06,000
they take away our focus of mind,

364
00:21:06,000 --> 00:21:08,000
they take away our clarity of mind

365
00:21:08,000 --> 00:21:11,000
and they stop us from seeing things as they are.

366
00:21:11,000 --> 00:21:13,000
We're not able to stay in the present

367
00:21:13,000 --> 00:21:15,000
because such people are constantly in the past

368
00:21:15,000 --> 00:21:16,000
or in the future.

369
00:21:16,000 --> 00:21:18,000
We're not able to stay in reality

370
00:21:18,000 --> 00:21:21,000
because such people are always talking about concepts

371
00:21:21,000 --> 00:21:24,000
and we're not able to stay with what is important

372
00:21:24,000 --> 00:21:26,000
because such people are always engaged

373
00:21:26,000 --> 00:21:30,000
and concerned with and intend upon

374
00:21:30,000 --> 00:21:33,000
what is unimportant, what is unessential.

375
00:21:33,000 --> 00:21:35,000
So,

376
00:21:35,000 --> 00:21:38,000
unless we can help such people

377
00:21:38,000 --> 00:21:40,000
and unless we can help such people easily

378
00:21:40,000 --> 00:21:42,000
without interrupting our own practice,

379
00:21:42,000 --> 00:21:44,000
we should be very careful to stay on our own.

380
00:21:44,000 --> 00:21:45,000
So, for many people,

381
00:21:45,000 --> 00:21:47,000
this is, I think, an important reminder.

382
00:21:47,000 --> 00:21:50,000
If you can't find someone who can lead you,

383
00:21:50,000 --> 00:21:51,000
who can help you,

384
00:21:51,000 --> 00:21:52,000
can support your practice,

385
00:21:52,000 --> 00:21:54,000
stay on your own,

386
00:21:54,000 --> 00:21:55,000
you're better off on your own

387
00:21:55,000 --> 00:21:58,000
because just having friends isn't

388
00:21:58,000 --> 00:22:01,000
just the fact that you have friends alone

389
00:22:01,000 --> 00:22:03,000
isn't necessarily useful

390
00:22:03,000 --> 00:22:04,000
or a good thing.

391
00:22:04,000 --> 00:22:06,000
Something that leads to your happiness.

392
00:22:06,000 --> 00:22:07,000
You need good friends.

393
00:22:07,000 --> 00:22:09,000
You need friends who lead you to good things.

394
00:22:09,000 --> 00:22:11,000
Friends who help you support you

395
00:22:11,000 --> 00:22:14,000
who encourage you

396
00:22:14,000 --> 00:22:16,000
in your own spiritual development.

397
00:22:16,000 --> 00:22:17,000
So, simple teaching,

398
00:22:17,000 --> 00:22:19,000
but one that we should all keep in mind

399
00:22:19,000 --> 00:22:21,000
and a very important aspect of our life

400
00:22:21,000 --> 00:22:23,000
is who we associate with.

401
00:22:23,000 --> 00:22:25,000
So, that's the teaching for tonight.

402
00:22:25,000 --> 00:22:26,000
Thank you all for tuning in

403
00:22:26,000 --> 00:22:28,000
and wish you all peace, happiness,

404
00:22:28,000 --> 00:22:29,000
and freedom from suffering.

405
00:22:29,000 --> 00:22:52,000
Thank you.

