So, we are on page 535, classification of the five aggregates under 11 heads.
Now, I hope we will be help of the handouts.
We can locate the papers and see the figures in the thought of purification.
The order given in the thought of purification is different from that in the menu.
And we are primarily acquainted with the order in the menu.
So, we always go from that, that reference.
So, today is the classification of the five aggregates.
We have finished the five aggregates in detail.
The following section firstly is that of the detailed explanation of the aggregates according
to the beta, bhajaniya, that is one way of treatment of the five aggregates in the second book of a beta.
So, we bhanga is the second book of a beta.
But, the aggregates are being in detail, given in detail by the blessed one in the sultanta
bhajaniya in this way.
Now, in the bhajaniya, when Buddha treated the aggregates and also the other subjects,
it treated first in the sultanta exposition.
And then, a beta-ma exposition.
And then, question and answer exposition.
So, in these three expressions, he treated the aggregates.
So, here, the order of the front of purification to the first method of exposition,
that is, a sultanta, bhajaniya.
And in materiality, whatever, but the first feature of present, internal, external,
close and subtle, inferior or superior, far or near, all that together in the mess,
all that together in the groves, is called the materiality aggregates, and feeling whatever
and so on.
And this can be found in other parts of the canon, that is, in here, in magic, manica and
other sultans also.
And then, the common data explains the words, whatever, and materiality.
So, here, in the word, whatever includes without exception.
In pali, the word is yang king chi.
So, yang king chi means whatever or it can mean all.
So, the word, whatever or yang king chi includes everything.
So, when we say, let us say, all materiality, then if we just say all, that means we have
to take all materiality as well as the mentality.
So, all, the word or whatever includes without exception, everything, but to limit its
goal, the word materiality is thought.
So, materiality prevents over generalization.
That means over extension.
When you say all, then you cover both mind and matter.
But when you say all matter, then you take only meta and not mind.
So, the word materiality prevents over extension of the word, whatever or all.
So, with these two words, we mean just materiality.
All kinds of materiality, that is, first, future, present, and so on.
The pali word, the youth here, is a thick personga for over generalization.
So, personga means actually to stick.
So, when you say something, it extends to this thing that thing and so on.
So, in order to restrict the extension, then the word materiality is thought here.
So, here, all materiality, that means only materiality and not mentality.
Thus, materiality is comprised without exception by the two expressions.
So, all materiality is included or taken by these two words.
Then, under this, it's exposition as past which are present and so on.
And here, the commentator explains past, future, and present in a hopeful way.
According to extents, according to continuity, according to periods, and according to movement.
So, extents first, continuity first, period first, movement first, like that.
First, according to extents, in the case of a single becoming, that is, single life of one living being.
Previous to reburred linking is first, subsequent to death is future, between these two is present.
So, one life, that the present life is present and the life before it is first and life after it is future.
This is according to extents.
And according to continuity, that means a series of meta or series of minds.
So, that materiality which has like a single origination by temperature.
And single origination by nutriment.
So, it occurs successfully is present.
Here, it is not a single origination, but origination by single temperature.
So, you can see the materiality which has origination by single temperature.
And single nutriment, like that.
Here, single temperature means just one kind of temperature.
Say, you are in the sun, and so there is a hot temperature, heat.
And then you go into the shade, and then the temperature changes, say, to cool.
So, that one series of hot temperature is said to be present.
And then before that, there may be some dissimilar temperature, and that is first.
And another dissimilar temperature after that is future.
So, here, origination by single temperature, origination by single nutriment.
That which previous to that was of unlike origination by temperature and nutriment.
That is, origination by unlike temperature and unlike nutriment.
Different temperature, different nutriment, that which is subsequent is future.
That which is born of consciousness and has its origination in one cognitive series.
In one impression, in one attainment is present.
Now, this is with regard to materiality, born of consciousness.
It has origination in one cognitive series that means thought processes of five-door.
And in one impression, really means mind-door thought process.
Because in mind-door thought process, there is only adverting and impression on Javanas.
So, it is called just Javanas.
So, in one impression, origination in one impression means in one thought process belonging to mind-door.
In one attainment, there is a statement of Javanas.
Previous to that, it's subsequent to that is future.
There is no special classification into first continuity, etc.
of that which has its origination in karma, that which is caused by karma.
So, with regard to materiality caused by karma, there is no classification into first continuity, present continuity, or future continuity.
But its fastness, etc. should be understood according as it supports those which have their origination through temperature and nutriment and consciousness.
But although karma does not produce, nevertheless it supports the materiality caused by other causes, temperature and nutriment and consciousness.
So, in reality, there is no classification into first, present and future continuity with regard to meta-born of karma.
But it can be said to be first present and future because they support the materiality caused by temperature caused by nutriment and caused by consciousness.
And the footnote down there, photo number central 4, is in fact for this passage.
Even in the supplementary, it is a little confusing.
But the footnote is for this passage.
It looks like explaining the words caused and conditioned in paragraph 191.
But the subsequent paragraph in the footnote, I mean beginning with because there is similarity and so on, refers to this sentence.
There is no special classification into first continuity, etc. of step and so on.
And in that footnote, my ball, three or four lines.
Just as the siege function is to around the sprout, and that of the earth, etc. is to consolidate it and just show.
Just as, karma's function is to arouse result, as meta, that is due to karma performed.
And there are some words missing.
And resulting consciousness, so we have to put this word after performed because the result of karma is not only meta but also consciousness.
At the moment of relinking, there is the relinging consciousness, which is the result of karma.
Along with this relinking consciousness, material properties caused by karma arise.
So, karma produces not only karma born meta but also result in consciousness.
And here we should say to arouse result as meta, that is due to karma performed and result in consciousness.
And that of the new treatment, etc. etc. etc. is missing there.
That of new treatment etc. is to completely date it.
So the function of and so on.
So there are two kinds of causes or whatever we call that.
One, that produces and the other that supports or that consolidate it.
So two kinds of conditions.
With regard to, is he becoming a tree of plants.
So the seed is the producing cause of the tree and water and temperature are the supporting conditions of the tree.
And here, those that are born of karma.
With regard to those that are born of karma, we cannot say that and this belongs to past, this belongs to present, this belongs to future.
Because there is no similarity or dissimilarity in temperature, etc. in the way stated.
But there is no such similarity in the karma that gives birth to a single becoming.
So instead of stating according to continuity, the person has etc. of material instances.
Or it is stated according to what consolidate is. So with regard to what it consolidates.
The matter born of karma can be said to be a belonging to present, first present or future, first present or future.
And then the subcontinent, as it is his own observation.
With the worst beginning, however, when there comes to be reversal of sex and so on.
It may not please ladies.
Especially in the video, there are instances of people having their sex has changed.
I mean, changed by karma, not by surgery.
There are instances of change of sex in the video.
Then, you know, men are supposed to be super women.
So milsex disappears owing to powerful and profitable karma.
So as a result of adverse alarm with sex disappears and the female sex appears owing to weak profitable karma.
It is still like karma, but it is weak. It is stronger than it will be a male sex and not female sex and so on.
So it is the addition made by the subcontinent data.
According to the visual demaga, then there can be no classification into first continuity with regard to etc with regard to karma bond matter.
But here it is said.
There can be what we call the first present and future, even with regard to karma bond matter because sex changed.
So before the change, it was the first and the change is the present and then later maybe some change into change back into the previous sex and that maybe the future.
So there can be a classification into first continuity etc even with regard to karma bond matter.
So this was addition made by the subcontinent data.
Then according to period, that is any period among those such as one minute, morning, evening day and etc that occurs.
So this made it is present and the first minute is first and the minute coming is future and so on.
And according to moment, what is included in the trial of moments that is the rising presence and dissolution, three, three sub moments beginning with a rising is called present.
So this is a real present.
At the time previous that it is future, at the time subsequent that it is first.
Furthermore, that whose functions of cost and conditions have elapsed is first.
Now here cost means producing and conditioning means supporting.
That whose function of cost is finished and whose function of conditioning is unfinished is present.
That means something is caused by some other thing and so something which is caused is in existence now.
So its origination is over but its continuation or its being supported is still going on.
So that is why whose function of cost is finished and whose function of conditioning is unfinished is present.
Then which is not a thing to either function is future or alternatively the moment of the function is present.
That means when a certain matter of mine or whatever is doing its own function, then that is the present moment.
At the time previous that it is future and subsequent to that is the first.
And there are some we will have the numbers identified but that is misplaced or it is superfluous.
So we can just rub it out because there is a 75 down in paragraph 192.
So that is division into according to moment and next division in the internal and external.
And it is already stated.
Besides it is internal in the sense of one's own that should be understood here as internal and that of another person as external.
Now in the footnote the four kinds of internal are given.
In the sense of one's own four kinds of agenda, internal literally belonging to one's own mentions in the commentaries and sub commentaries.
And the first one is co-chair agenda internally as range or resort.
Then agenda agenda internally as such.
Then need your character internal in the sense of one's own.
And then we say agenda internally as objective field.
Do you understand it?
Now the first one co-chair agenda is simply any internal object.
So any internal object is called co-chair agenda.
The next one, adjust agenda.
That means very internal because the two words are repeated here, adjust agenda.
So adjust agenda, so internal internal.
So by that is meant to be the five senses.
I hear no standing body.
I mean, I sensitivity and so on.
So they are called object agenda or in the section on matter.
They are called internal and partly adjust speaker.
And then the third one here, need your character.
That means anything which arises in one's own continuity.
That is called adjust agenda.
And the last one, we say agenda is really the attainment of fruition.
Palazzama party.
Not internally as objective field.
We can see that internally as scope.
Because when the border is in the palad attainment, then he has mastery over it.
And so it is called with the adjuster.
So there are four kinds of internal agenda.
And sometimes we do not, we are not sure which agenda is meant.
And so we have to find out.
So here internal and external, the first is just as it is already stated in paragraph 73.
And another way of explaining it is that internal in the sense of one's own.
That means everything in everything in me is internal.
And everything in you is external.
For you everything in you is internal and everything in other is external.
Gross and subtle are also already stated.
In Syria and Syria are too full namely figurative, relative and absolutely literal.
Here the materiality of the suit as in deities is inferior to the materiality of the
alternator deities.
You remember the 35, 31 flames of existence.
So it is referring to that.
So alternator is the highest of the foam grammars.
So that is the most superior.
And so the thing is 1, 1, 1 stage after the alternator.
So it goes that way.
That's the materiality of the suit as in deities is superior to the materiality of the suit as a deity.
That's what we showed the inferior deity and superior deity.
We understood figuratively down as far as the denizens of hell.
So 1, 1 is superior to the next below it and so on and so on.
And then you reach the hell.
But absolutely, literally in Syria where it arises as unprofitable result.
And it is superior where it arises as profitable result.
So absolutely for an ultimate sense, in Syria is where it arises as the result of unprofitable
and profitable calmer, up to salakama.
And that which arises as a result of whose salakama, profitable calmer is superior.
And then far and near is also already described.
Which is difficult to see is far.
And which is not difficult to see or easy to see is near and so on.
Besides very defined as a near and it should be understood here according to locations.
So according to luggage and magazine, this is near and that is far.
All that together in the must and in the grows.
That is a message mentioned in paragraph 185.
By making all that materiality separately described by the worst, first, etc., into a collection by understanding
its oneness, in other words, its characteristic of being molested.
It comes to be called the materiality every case.
Now this shows, this shows why they are called aggregates.
Now here, materiality separately described by the worst, first and so on.
So it is described as past, present, future, internal, external, grows, subtle, far, near and so on.
So that materiality described in that way is made into a collection by understanding.
In its oneness, I think the word in is missing there.
In its oneness, in other words, its characteristic of being molested or of change.
Now the characteristic of rupa is change, right?
Or say being molested.
So with regard to this characteristic, all the meta described as past, present and future are
collected by understanding.
And that is why it is called an aggregate.
So by understanding, should go with making a collection.
So making a collection means you collect them up in your mind with understanding.
Because say past, present and future cannot exist at the same time.
We can have only the present meta here.
So the past is already present.
We cannot collect it.
We cannot take it and put it here.
And we cannot take the future and put it with the present physically.
But in our minds, we can just group them together.
So by understanding means in our minds, we group them together and call it an aggregate.
So aggregate does not mean a group of, and does not necessarily mean a group of different material properties.
Now, for example, there are 28 kinds of material properties.
These 28 are also called aggregate of meta.
Now, aggregate is one of the material properties can be called aggregate of meta.
Not only when they are taken together.
So let us say the eye sensitivity.
Only one eye sensitivity is called an aggregate.
Because there is eye sensitivity, which is first, which is present, which is future, which is gross, which is subtle, which is inferior, which is superior, which is far, which is near.
And so we group these different kinds of eye sensitivity together and call it a group or call it an aggregate.
So aggregate or group does not necessarily mean that there must be a real group of meta.
So even one aggregate, I mean one material particle, one feeling, one perception is called aggregate.
So this explained why they are called aggregates.
So by understanding, you should go with making into a collection.
So making making into a collection in the mind.
In its oneness, that is its characteristic here with regard to meta being molested or changed.
With regard to feeling, it is what?
With the characteristic of feeling, with regard to perception, with its characteristic of perception.
And with regard to consciousness being its characteristic of knowing the object and so on.
So this is the reason why even one particle of meta or one mental factor is called an aggregate.
But this too, it is shown that the material, the aggregate is all materiality, which all comes into the collection with the characteristic of being molested.
For there is no materiality, aggregate, apart from materiality.
And so in feeling, and also to say.
So it is the classification of first aggregate, ruba aggregate into 11 heads.
Now the feeling of leakage in the classification into first, etc.
But the first future and present state of feeling should be understood according to continuity and according to movement and so on.
The same as in material aggregate.
Here, according to continuity, that includes in a single cognitive series.
Again, that means five-door thought process is single important.
In addition, it means mind or thought process and a single attainment and that occurring in association with objective field of one can one object is present.
Before that is present, the sequence is future.
Then according to movement, the feeling included in the trial of moments, three moments, which is in between the first time and the future time.
And which is performing its own function, which is doing its own function, so it is really present one.
It is present.
Before that is first and subsequent, it is future.
And then internal and external.
Then grows and subtle.
Now, grows and subtle should be understood according to kind.
According to individual essence, according to person and according to mundane and super mundane.
Now, according to kind, the unprofitable feeling is the state of desire because it is the cause of reprehensible actions and because it produces burning of defilement.
That means defilement that are burning, not burning away the defilements.
So, I don't want you to understand in that sense.
So, burning of defilement really means defilements that are called burning.
So, it is grows.
And because it is accompanied by interestedness, that means, activeness and drive and result.
And because of the burning of the defilements, the same thing.
And because it is reprehensible, it is grows compared with resultant and determinate and so on.
So, here one is described as, one is described as a growth or subtle relatively with other kind of feeling.
So, in Kusalar feeling, Akusalar feeling, indeterminate, Abjakata feeling and so on.
And then, according to individual essence, the painful feeling is grows compared with the others because it is without enjoyment.
It involves intervention.
So, intervention really means shaking.
Non-peacefulness.
And it causes disturbance.
It creates anxiety and it is over power.
And the other two are subtle compared with the painful because they are satisfying peaceful and superior and respectively agreeable and neutral.
And so, this is the reason for there being subtle and grows.
Also, they are relative states of subtleness and grossness.
According to person, feeling in one who has no attainment.
I would say, who is not in attainment?
It doesn't may have attainment, but if it is not in that attainment at the moment, then his feeling could be, say, gross.
So, feeling in one who is not in attainment is grows compared with that in one who is in attainment.
Because it is distracted by multiple objects.
In the opposite sense, the other is subtle.
This is how grossness and subtlety should be understood according to person.
So, when in person is in attainment, in attainment of Jana or in attainment of Hala, the fruition, his feeling is said to be subtle.
And if it is not in attainment, it is said to be gross.
According to the mundane and super mundane.
So, feeling subject to kankas is mundane, subject to kankas means object of kankas.
Which can be the object of kankas, asa was, or just mental defilements.
And that is gross compared with that free from kankas.
Because it is a cause for the arising of kankas.
It is liable to the floods, liable to the bonds, liable to the ties, liable to the hindrances,
liable to the cleanings, defiable, and shared by ordinary men.
Now, these floods, bonds, ties, hindrances, and cleanings, defiable and shared.
So, to understand them, I would like to refer you to the manual of a bhirama, chapter 7.
In fact, the mental defilements are given different names.
So, the same mental defilements.
So, for example, robot is a mental defilement.
The robot is given the name of kankas, floods, bond, hindrances, cleaning, and so on.
So, both are described in different ways to suit the susceptibility of listeners to understanding.
And they are taught in a bhirama as well as in some sodas.
So, to understand them, the best place to go is to the manual of a bhirama.
So, in chapter 7 of the manual of bhirama, they are all described.
And towards the end, or always to the end of the book, we will find them again.
When, how these defilements are eradicated by different type consciousness that you've
mentioned again, so the first part consciousness eradicated two of the defilements or three of the
defilements and so on.
So, we will find them later at the end of, almost at the end of the book.
Shared by ordinary men means those that arise in puto-geners.
The letter in the opposite sense is subtle compared with their subjective kankas.
So, they are not subjective kankas.
Those that are not the object of kankas is subtle.
This is how grossness and subtlety should be understood according to the mundane and
should from one day.
And then, there is the warning.
One should be aware of mixing up, because if you mix up, if you mix up, you will mix
up.
You'll be confused.
So, beware of, the party word actually means a void.
So, one should avoid mixing up, the classifications according to kankas and so on.
So, just do according to one.
So, according to kankas or according to some other thing, but not mix them together.
So, if you mix them together, then there will be contradiction.
So, according to one, it may be subtle, but according to another, it may be gross.
So, that is what the commentary gives here.
So, according to individual, and it is said that, in determining its feeling is subtle.
The painful feeling is gross.
The feeling in one with an attainment is subtle, or this is the passage from Vibhana.
Now, like painful feeling, so also pleasant, etc., is gross, according to the kind and subtle,
according to the individual essence.
So, let's say, Doka, right?
Pay.
Paying is gross, according to its individual essence, because it causes, it inflicts us.
But, since it is indeterminate, in another sense, it is abyajada.
It is neither kusala nor Akusala, right?
It is abyajada.
So, it is subtle, because abyajada are described to be subtle.
So, if we mix these two, then we will get nowhere.
According to one, it is subtle, according to another, it is gross.
So, do not mix them up.
For instance, when it is said, the indeterminate, according to kind, is subtle, compared with the profitable and the unprofitable,
the individual essence, class, etc., must not be insisted upon like this.
So, which kind of indeterminate you don't ask these questions?
Is it painful? Is it the person?
Is it the one in attainment?
Is it the one not in attainment?
Is it that subjective kangas?
Is it that free from kangas and so on in each instance?
So, you cannot ask these questions.
Furthermore, because of the work of feeling should be regarded as gross or subtle in comparison with this, but that feeling.
Among the unprofitable, etc., feelings accompanied by hate to is gross compared with that accompanied by greed, because it burns up its own support like a fire.
And that accompanied by greed is subtle.
Also, that accompanied by hate is gross when the hate is constant and subtle when it is inconsistent.
I wonder whether you understand that.
In order to correctly understand it, you have to understand Abirama.
You have to go to the first book of Abirama, because the work is constant and in constant here.
Do not mean the same as we had before.
Formerly, the work constant and in constant are used to refer to cheezy zigas, right, which go along with a given chee there all the time.
Every time that chee there arises, it also arises.
That is called constant.
But there are some cheezy zigas, which do not arise every time a given chee there arises.
So, they are called constant and in constant.
But here, although the poly word is the same, the meaning is very different.
And you have to have a knowledge of the first book of Abirama to understand this.
So, constant here means actually a fixed as to giving results in next life.
They are sure to give results in next life.
So, they are such hate or anger is called constant.
In constant means not fixed as to giving results in the next life.
Now, for example, killing one's own mother, killing one's own father and so on.
So, when you kill your own father, your mother there is Dosa, right?
So, that Dosa is called fixed because it will invariably give results in the next life.
That means you will go to hell.
There is no way of avoiding that.
Now, you may kill an animal.
They are also you have Dosa.
That Dosa will give you a result in awful stage, but not necessarily in the next life.
You may get the result of the killing of an animal in other life.
Or you may in the next life.
So, that is not fixed.
But killing one's own mother and one's own father, killing an other hand, wounding the Buddha
and causing cases in the sound of these five are called the very previous sins and Buddhism.
And so, if you do one of these, then you will surely go to hell in the next life.
There is no way of avoiding that.
So, such a core, I mean a constant, yeah.
And subtle when it is in constant.
And the constant is gross when giving results that last was the year while the other is subtle.
So, among the fixed as to the giving of results, there are two subdivisions.
Whose result last for the year?
And whose results last not for the year?
That means, I will give you an example.
So, you know Adjada Satu and Devatata.
Adjada Satu was the son of King Benvisara.
And Devatata was Buddha's cousin and brother-in-law.
Adjada Satu killed his own father at the instigation of Devatata.
So, Devatata asked, Devatata wanted to become the Buddha himself.
So, he thought, now these people live long lives.
So, we cannot wait until they die.
So, you kill your own father and be a king and I will kill my brother and be a Buddha.
So, he instigated Adjada Satu to kill his own father.
So, Adjada Satu killed his own father.
So, there is...
