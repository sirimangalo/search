1
00:00:00,000 --> 00:00:05,840
And now we will have the third speaker for this session and he is a Biku in the

2
00:00:05,840 --> 00:00:10,840
Theravadan Buddhist tradition and he's been practicing intensive and daily

3
00:00:10,840 --> 00:00:16,920
meditation following the Mahasi Saira tradition over the last 17 years. He has

4
00:00:16,920 --> 00:00:21,480
been teaching intensive meditation in Thailand Sri Lanka, the US and the

5
00:00:21,480 --> 00:00:27,120
Canada. The topic would be wrong mindfulness and its consequences according

6
00:00:27,120 --> 00:00:34,120
to the Buddha. Please join me to welcome Yutadamu Biku.

7
00:00:34,120 --> 00:00:41,120
So today I was asked to talk about wrong mindfulness.

8
00:01:04,120 --> 00:01:08,240
Which is interesting because the Buddha was quite clear that mindfulness is

9
00:01:08,240 --> 00:01:14,040
always good. In the Abhidhamma it's called a sobhana Jaitasika. So it's a

10
00:01:14,040 --> 00:01:19,560
beautiful mind state. And so what I'd like to do is explore some of the ways

11
00:01:19,560 --> 00:01:24,280
that mindfulness can go wrong. The Buddha himself did use the term wrong mindfulness.

12
00:01:24,280 --> 00:01:30,720
It's not something that we came up with. But mindfulness itself is a positive

13
00:01:30,720 --> 00:01:33,880
state. So I'd like to first look at and the other two speakers have talked a

14
00:01:33,880 --> 00:01:38,080
little bit about it already but about mindfulness. So I'm just going to give a

15
00:01:38,080 --> 00:01:44,200
technical description if I can figure how this thing works. So with any

16
00:01:44,200 --> 00:01:48,680
dhamma in the traditional Theravada texts we have four qualities or four

17
00:01:48,680 --> 00:01:54,120
aspects that we use to define it. The characteristic, the function, the

18
00:01:54,120 --> 00:02:01,360
manifestation and the approximate cause. This is with all dhammas.

19
00:02:01,360 --> 00:02:08,360
Does this thing work? I've got the wrong one.

20
00:02:08,360 --> 00:02:33,360
Oh, my other talk is on technology so something to say about that tomorrow.

21
00:02:33,360 --> 00:02:43,360
Yeah, it works for you. I did. It's repeatedly.

22
00:02:43,360 --> 00:02:47,840
Okay, okay. So the characteristic, the characteristic of mindfulness is not

23
00:02:47,840 --> 00:02:53,320
wobbling. The ordinary mind wobbles and waivers. It isn't stable. It isn't fixed

24
00:02:53,320 --> 00:03:00,080
on an object. Mindfulness is able to grasp the object.

25
00:03:00,080 --> 00:03:04,720
It's function is to not forget. So ordinary mindfulness can actually refer to

26
00:03:04,720 --> 00:03:08,720
remembering things that happened a long time ago in the past. The ability to

27
00:03:08,720 --> 00:03:14,320
recollect something. But in Satipatana, in mindfulness meditation practice, it

28
00:03:14,320 --> 00:03:17,680
means to not forget the object in the present moment. So ordinarily we

29
00:03:17,680 --> 00:03:21,920
experience something and then right away we're often the mind judging it,

30
00:03:21,920 --> 00:03:25,680
reacting to it and we forget about the object. You see something but you're not

31
00:03:25,680 --> 00:03:30,000
interested in the seeing. You're interested in what it means. Is it good? Is it bad?

32
00:03:30,000 --> 00:03:35,160
Is it me? Is it mine and so on. Mindfulness doesn't do that. Mindfulness sticks

33
00:03:35,160 --> 00:03:40,680
with the actual object. It is manifested as guarding. So mindfulness guards the

34
00:03:40,680 --> 00:03:45,600
mind. It guards the mind from defilements specifically. An ordinary mind is

35
00:03:45,600 --> 00:03:51,720
unguarded and so lots of defilements are able to enter. Or it is manifested

36
00:03:51,720 --> 00:03:56,160
simply as confronting the objective field. So what this means is an ordinary

37
00:03:56,160 --> 00:04:00,320
mind is unable to confront. It's like when an experience comes and if it's an

38
00:04:00,320 --> 00:04:04,120
unpleasant experience, the mind shies away from it. If it's a pleasant

39
00:04:04,120 --> 00:04:08,280
experience, the mind immediately chases after it. But to just sit and confront

40
00:04:08,280 --> 00:04:13,800
the object, this is mindfulness. To be able to be with the positive or negative

41
00:04:13,800 --> 00:04:22,800
experience without reacting. This is fun. It's proximate cause is strong

42
00:04:22,800 --> 00:04:27,640
perception. So the proximate cause means would gives rise to mindfulness.

43
00:04:27,640 --> 00:04:32,440
Tira sunya. So this is the poly sunya is a perception of something. When you

44
00:04:32,440 --> 00:04:37,240
perceive that you're seeing the perception of seeing, well that sunya. When you

45
00:04:37,240 --> 00:04:41,600
perceive that you're hearing a cat meowing, the perception of the cat that

46
00:04:41,600 --> 00:04:47,160
sunya. But Tira sunya is when you reaffirm it, it becomes strengthened. So this

47
00:04:47,160 --> 00:04:51,200
is what meditation does. When you practice meditation, for example, the text

48
00:04:51,200 --> 00:04:55,720
give the object of a disc of earth. If you're looking at earth, then you just

49
00:04:55,720 --> 00:05:01,200
repeat to yourself. Earth, earth, earth, earth, or bhatui, bhatui. And by doing that, your

50
00:05:01,200 --> 00:05:09,160
mind stays focused on the object. You strengthen the perception. This is going to

51
00:05:09,160 --> 00:05:14,800
take longer than 15 minutes, if I can't get through this. Okay. Or simply put, the

52
00:05:14,800 --> 00:05:18,280
proximate cause is the practice of the four foundations of mindfulness, which

53
00:05:18,280 --> 00:05:25,520
the other monks have gone through. It should be, okay, and then we've got a

54
00:05:25,520 --> 00:05:31,960
couple of similes. So it's regarded as a pillar. Like I said, the function or

55
00:05:31,960 --> 00:05:35,360
the characteristic is to not wobble. So the text talk about this idea of a

56
00:05:35,360 --> 00:05:40,160
gourd floating on the on the top of the water. It flits about here and there. As

57
00:05:40,160 --> 00:05:44,320
compared to a pillar sunk in the bottom of the the the lake, no matter what

58
00:05:44,320 --> 00:05:48,040
the how the waves or the wind buffet, it doesn't shake mindfulness is like the

59
00:05:48,040 --> 00:05:54,280
pillar. Or it is like a doorkeeper because it guards the eye and the ear and so

60
00:05:54,280 --> 00:05:59,360
on. So the sense doors, the sense doors are where all of our experience comes

61
00:05:59,360 --> 00:06:03,120
and therefore all of the problems come. And if the mindfulness is there, it

62
00:06:03,120 --> 00:06:08,080
lets the experience in without letting in the defilements. So that's mindfulness.

63
00:06:08,080 --> 00:06:11,840
When we talk about wrong mindfulness, then the question is, well, what, what

64
00:06:11,840 --> 00:06:16,480
then does that mean? And I've come up with four categories that I think

65
00:06:16,480 --> 00:06:19,920
according to the Buddha's teaching relate to this idea of wrong mindfulness. I'll

66
00:06:19,920 --> 00:06:24,320
just go through them. The first is unmindfulness. So unmindfulness is just the

67
00:06:24,320 --> 00:06:27,560
opposite of mindfulness. If you never go to a meditation center or never take

68
00:06:27,560 --> 00:06:31,320
up the practice of meditation, you're generally unmindful. So your mind

69
00:06:31,320 --> 00:06:35,760
wobbles. This is the opposite, right? Your mind is forgetful. So you can't

70
00:06:35,760 --> 00:06:39,440
remember things that happened yesterday. And and and you can never remember

71
00:06:39,440 --> 00:06:43,400
the present moment. You experience something and immediately you react. You

72
00:06:43,400 --> 00:06:47,720
don't confront the object. And it's caused by weak perception. You perceive

73
00:06:47,720 --> 00:06:52,520
something, but your mind is not trained, right? Your mind is not well-developed

74
00:06:52,520 --> 00:06:58,000
in order to stick with the perception. So you get lost in your reactions. The

75
00:06:58,000 --> 00:07:03,960
second is something called misdirected mindfulness. So this type of mindfulness is

76
00:07:03,960 --> 00:07:09,800
not actually wrong in a general sense. Just like if you want to go to Niagara

77
00:07:09,800 --> 00:07:14,040
Falls, you've got to get on the QEW. If you get on the 400 year on your way to

78
00:07:14,040 --> 00:07:17,360
Sudbury, your number, it's not, there's nothing wrong with the 400. It's just

79
00:07:17,360 --> 00:07:21,840
it doesn't link to Niagara Falls. This by the same token, if you practice

80
00:07:21,840 --> 00:07:26,160
mindfulness of the past, remembering even your past lives, for example, it's

81
00:07:26,160 --> 00:07:30,960
never going to allow you to attain enlightenment because it's focused on the

82
00:07:30,960 --> 00:07:35,240
wrong object. Nothing wrong with it. It's just a mundane object or it's a

83
00:07:35,240 --> 00:07:39,120
conceptual object. Mindfulness of the future is the same if you're able to

84
00:07:39,120 --> 00:07:44,640
plan ahead or if you're even able to have an experience of precognition where

85
00:07:44,640 --> 00:07:48,440
you see something before it happens. Could be considered a form of mindfulness,

86
00:07:48,440 --> 00:07:52,400
but it doesn't help you because it's not focused on actual reality as you

87
00:07:52,400 --> 00:07:56,880
experience it. And of course, mindfulness of concepts. Now mindfulness of

88
00:07:56,880 --> 00:08:01,480
concepts is a valid meditation technique. So if you focus on a candle flame,

89
00:08:01,480 --> 00:08:05,640
for example, or as I said, the disc of earth, eventually you're able to see this

90
00:08:05,640 --> 00:08:11,520
object in your mind conceptually. Now that object is stable, it's satisfying and

91
00:08:11,520 --> 00:08:16,040
it's even controllable. You can expand it, you can contract it, you can enter

92
00:08:16,040 --> 00:08:20,640
into very high states of calm with it, but it's not going to lead you to enlightenment

93
00:08:20,640 --> 00:08:25,880
because it's a concept. It's not impermanent. It's not an Ichadukana. You're not

94
00:08:25,880 --> 00:08:30,880
going to see the three characteristics. The third is something I'd call

95
00:08:30,880 --> 00:08:35,920
lapsed mindfulness. And so this is like unmindfulness, except the key here is

96
00:08:35,920 --> 00:08:41,120
that this arises in someone who is practicing correctly because for every

97
00:08:41,120 --> 00:08:46,440
meditator, we're not robots. I'm not machines. We don't, this is an assembly line

98
00:08:46,440 --> 00:08:50,400
where we pass people through and they all come out enlightened. Everyone has

99
00:08:50,400 --> 00:08:53,680
conditions that they come to the meditation with and so everyone has a

100
00:08:53,680 --> 00:08:59,120
different experience. Often this shows itself as positive states. It can also

101
00:08:59,120 --> 00:09:05,200
show itself as negative states, deep states that we keep hidden inside. So the

102
00:09:05,200 --> 00:09:08,440
text deal with some of the good ones. These are positive states that arise

103
00:09:08,440 --> 00:09:12,640
because you're practicing correctly, but they themselves are not the path.

104
00:09:12,640 --> 00:09:15,880
They themselves will not lead you to enlightenment and unfortunately sometimes

105
00:09:15,880 --> 00:09:20,040
meditators start to think that they will. For example, you see bright lights or

106
00:09:20,040 --> 00:09:23,480
colors or pictures and you say, oh, that must be the path to enlightenment or

107
00:09:23,480 --> 00:09:28,560
knowledge that you gain about your problems or knowledge about past lives or

108
00:09:28,560 --> 00:09:31,920
being able to read people's minds, many different kinds of knowledge that you

109
00:09:31,920 --> 00:09:38,320
get caught up in. Rapture, you can feel energy or lightness or goosebumps. All of

110
00:09:38,320 --> 00:09:42,520
these things can distract you from being mindful. Tranquility sometimes you

111
00:09:42,520 --> 00:09:44,960
feel very calm and then you don't know what to do because there's nothing to

112
00:09:44,960 --> 00:09:50,600
be mindful of except you can be mindful of the calm and we forget this. Happiness

113
00:09:50,600 --> 00:09:54,360
meditators often feel great happiness and again all of these are good things.

114
00:09:54,360 --> 00:10:00,000
Problem is when you get attached to it and stop being mindful of it. Confidence

115
00:10:00,000 --> 00:10:03,800
of course being mindful suddenly focuses your mind and makes you very

116
00:10:03,800 --> 00:10:07,080
confident suddenly you're very sure of yourself, sure of your teacher, sure of

117
00:10:07,080 --> 00:10:10,720
your practice, sure of the Buddha and you get caught up in that and think, wow,

118
00:10:10,720 --> 00:10:16,200
this is great, this is the best and you're no longer mindful. Exertion you gain a

119
00:10:16,200 --> 00:10:20,400
great amount of energy from the practice and this can distract you. Sometimes

120
00:10:20,400 --> 00:10:24,400
you have to get up and run around because you have so much energy. Mindfulness

121
00:10:24,400 --> 00:10:28,440
even mindfulness because you gain such clear mindfulness you start to become

122
00:10:28,440 --> 00:10:35,360
complacent and you think, boy, am I mindful? It happens. It happens more often

123
00:10:35,360 --> 00:10:39,360
than one might think. It's kind of funny that mindfulness can be a distraction.

124
00:10:39,360 --> 00:10:44,200
Equanimity you become very calm and you think, wow, I'm enlightened, this is

125
00:10:44,200 --> 00:10:49,360
great. Equanimity comes and goes and if it still goes well it's not enlightenment.

126
00:10:49,360 --> 00:10:53,400
And finally attachment, attachment is really the problem if you're attached to

127
00:10:53,400 --> 00:11:00,960
any of these or if you're attached to anything really attached to sites or sounds

128
00:11:00,960 --> 00:11:05,280
or anything this will lead you to get distracted. So your mindfulness will

129
00:11:05,280 --> 00:11:09,960
collapse. The final thing that I would call wrong mindfulness is I think what

130
00:11:09,960 --> 00:11:15,560
Brahman Langso was talking about and I think this is probably what the Buddha

131
00:11:15,560 --> 00:11:20,320
really meant by wrong mindfulness when he talked about the eightfold

132
00:11:20,320 --> 00:11:25,200
noble path and these are the other seven factors. So we have wrong view and so

133
00:11:25,200 --> 00:11:30,240
on and so on. And the point is that mindfulness is impotent if it's not

134
00:11:30,240 --> 00:11:34,440
associated with the other seven path factors. So if you have wrong view as the

135
00:11:34,440 --> 00:11:38,720
venerable said, it's going to no matter how mindful you are if you still

136
00:11:38,720 --> 00:11:42,600
believe in things like the soul or the self or if you don't believe in karma

137
00:11:42,600 --> 00:11:46,320
if you have, you know, think that God created us and so on. All of this is

138
00:11:46,320 --> 00:11:50,560
going to prevent you from seeing clearly the ordinary experience. Wrong thought,

139
00:11:50,560 --> 00:11:56,360
wrong speech if you have bad intentions or ambitions, cruelty in the mind and

140
00:11:56,360 --> 00:12:01,560
that kind of thing. If you have, if you're a liar, if you're a gossip, if you

141
00:12:01,560 --> 00:12:05,880
just talk a lot, it's going to get in the way of mindfulness. If you're a

142
00:12:05,880 --> 00:12:10,560
murderer, if you're a thief, if you take drugs or alcohol, all of these of

143
00:12:10,560 --> 00:12:14,480
course are going to get in the way. You can be try your best at being mindful

144
00:12:14,480 --> 00:12:18,160
but if you aren't keeping, for example, keeping the five precepts, I would

145
00:12:18,160 --> 00:12:21,200
never teach someone a meditation course. If they weren't able to keep at

146
00:12:21,200 --> 00:12:28,040
least the five precepts, it's just futile. Wrong livelihood so if you make a

147
00:12:28,040 --> 00:12:31,520
living out of those bad things, wrong effort, if you're lazy or if your effort

148
00:12:31,520 --> 00:12:35,280
is directed towards the cultivated cultivation of unwholesomeness, all of that

149
00:12:35,280 --> 00:12:38,680
will prevent mindfulness from being effective. And if wrong, wrong, wrong

150
00:12:38,680 --> 00:12:43,360
concentration, if you're unfocused or if you're focused on the wrong things. So

151
00:12:43,360 --> 00:12:47,280
idea being the eight full noble path has to work together. Mindfulness is not

152
00:12:47,280 --> 00:12:50,600
enough. Mindfulness is like the key that starts the engine. It gets everything

153
00:12:50,600 --> 00:12:56,240
going. But if your engine's broken, the key doesn't do much. So we have some

154
00:12:56,240 --> 00:13:00,520
consequences. This is the last thing I'll talk about in a few minutes left.

155
00:13:00,520 --> 00:13:05,720
The first consequence of wrong mindfulness potentially is regression. And so

156
00:13:05,720 --> 00:13:10,480
this isn't that it's unfortunate but isn't that dangerous. Regression just

157
00:13:10,480 --> 00:13:15,000
means you get discouraged. You instead of gaining wholesome states, you

158
00:13:15,000 --> 00:13:19,720
regress. You become unmindful and you get afraid of the practice or upset

159
00:13:19,720 --> 00:13:22,840
with the practice. You think the practice is useless because while you're not

160
00:13:22,840 --> 00:13:28,360
doing it properly. And so one leaves the practice, even leaves Buddhism, even

161
00:13:28,360 --> 00:13:32,480
begins to disparage Buddhism as useless and so on. Which is unfortunate.

162
00:13:32,480 --> 00:13:38,200
Stagnation. Stagnation is also common. It's common for meditators to practice

163
00:13:38,200 --> 00:13:42,040
for years without becoming enlightened. They practice and they're very calm and

164
00:13:42,040 --> 00:13:44,960
peaceful but they never gain insight because they never actually look at the

165
00:13:44,960 --> 00:13:49,040
forest at the batana. Instead, they're focused say on concepts. If someone

166
00:13:49,040 --> 00:13:53,240
practices loving kindness, it's a great thing. Fun practice is focusing on a

167
00:13:53,240 --> 00:13:56,360
candle flame. Eventually, apparently you can learn to shoot fireballs out of

168
00:13:56,360 --> 00:14:02,240
your hands. It's great fun. But not going to lead you to enlightenment. This

169
00:14:02,240 --> 00:14:05,120
isn't me. The ancient texts are full of these things. This apparently is a

170
00:14:05,120 --> 00:14:10,400
thing. Complication. The third consequence is, of course, the most dangerous.

171
00:14:10,400 --> 00:14:14,120
It's medit mindfulness is of course designed to simplify things as the Buddha

172
00:14:14,120 --> 00:14:18,600
said, when you see, let it just be seeing. This is how you should train

173
00:14:18,600 --> 00:14:22,280
yourself. When you hear, let it just be hearing. When you experience something,

174
00:14:22,280 --> 00:14:28,400
let it just be the experience. When you're unmindful or if you have a sort of

175
00:14:28,400 --> 00:14:32,880
technically speaking perverted state of mindfulness in the sense of having

176
00:14:32,880 --> 00:14:38,400
one of the other factors, path factors, missing, it complicates things. It can

177
00:14:38,400 --> 00:14:44,280
actually create mind states that are more tense, more stressed, more poisonous,

178
00:14:44,280 --> 00:14:48,800
and through repeated and intensive practice, it's actually possible to

179
00:14:48,800 --> 00:14:53,440
wind yourself up so that you lose your mindfulness entirely and go what we

180
00:14:53,440 --> 00:14:57,640
might call temporarily insane. You're unable to control yourself. I've seen

181
00:14:57,640 --> 00:15:02,840
this happen. Never happened with any of my meditators, but I've seen it. A meditator

182
00:15:02,840 --> 00:15:06,160
will lose their mindfulness. That's, of course, the most dangerous. I don't have a

183
00:15:06,160 --> 00:15:09,800
lot of time. I'm out of time, actually, but I hope we can talk about this

184
00:15:09,800 --> 00:15:13,760
a little more. I'm really interested too. I have much more to say on this topic.

185
00:15:13,760 --> 00:15:17,200
But, of course, wrong mindfulness is a thing and it's something we'll have to be

186
00:15:17,200 --> 00:15:22,520
concerned with. So, thank you very much. I appreciate it. I'll do it for sure.

