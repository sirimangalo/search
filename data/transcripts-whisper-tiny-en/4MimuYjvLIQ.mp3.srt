1
00:00:00,000 --> 00:00:18,400
Good evening, everyone broadcasting live October 13th 2015, I'm going to go with another

2
00:00:18,400 --> 00:00:46,400
demo part of the video tonight. Testing, test, test, testing, okay.

3
00:00:46,400 --> 00:00:54,080
Welcome back to our study of the demo pande. Today we continue with verses 85 and 86,

4
00:00:54,080 --> 00:01:16,080
which read as follows.

5
00:01:24,080 --> 00:01:32,880
These verses are a common chant in Thailand. So, quite familiar. These are the next ones in the

6
00:01:34,240 --> 00:01:43,360
chapter. So, these two mean apakati minuses. So, few are they among humans.

7
00:01:43,360 --> 00:01:50,560
Yajanaparagarmino, who go to the farther shore.

8
00:01:50,560 --> 00:01:59,760
The tayangitra paja. These other here, these other humans, other people.

9
00:02:01,440 --> 00:02:05,280
Thiramaywa alunwwt, simply run up and down the shore.

10
00:02:05,280 --> 00:02:14,560
Yajakho samadakati. But, those who

11
00:02:18,560 --> 00:02:28,400
samadakati Nami, in regards to the well taught dhamma, dhamma, what are those who

12
00:02:28,400 --> 00:02:43,840
go according to the dhamma, tayjanaparmino, these people go to the farther shore.

13
00:02:43,840 --> 00:02:46,160
But, Yajakho dayang

14
00:02:53,680 --> 00:03:00,320
these cross, or these cross to the other side. But, Yajakho dayang, the king of the kingdom of death,

15
00:03:01,440 --> 00:03:04,880
which is hard to cross, which is very hard to cross.

16
00:03:04,880 --> 00:03:12,640
So, we have some imagery here, an imagery of crossing an ocean,

17
00:03:14,000 --> 00:03:16,480
that most people just run up and down the shore.

18
00:03:18,240 --> 00:03:23,920
Thiram is this shore. Anodawati is running up and down.

19
00:03:27,920 --> 00:03:32,000
So, this was given in regards to another very short story.

20
00:03:32,000 --> 00:03:36,720
It's rather more of a remark that the Buddha gave in regards to

21
00:03:38,560 --> 00:03:42,320
the habits of those who would come and listen to the dhamma.

22
00:03:43,040 --> 00:03:50,000
It seems that there was a specific group of people who came to listen to the Buddha's teaching.

23
00:03:50,800 --> 00:03:57,600
They had done good deeds, or they had supported the monastery, and so they came and gave

24
00:03:57,600 --> 00:04:02,720
us to the monks, and then stuck around, intending to listen to the dhamma all night.

25
00:04:04,080 --> 00:04:07,200
There would be teachings of the dhamma where the monks would take turns,

26
00:04:07,920 --> 00:04:12,160
giving talks all night, or maybe even one monk would talk all night, probably the former,

27
00:04:13,040 --> 00:04:15,120
probably would be several monks taking turns.

28
00:04:17,440 --> 00:04:22,880
We once did this in Thailand, something like this. There was

29
00:04:22,880 --> 00:04:28,000
there were some ancient Buddha statues that were stolen.

30
00:04:28,560 --> 00:04:31,760
They were actually kept in, kept behind bars. They were quite valuable.

31
00:04:32,320 --> 00:04:35,200
They were kept behind bars and someone actually cut through the bars,

32
00:04:36,080 --> 00:04:39,520
or bent the bars or something to steal these Buddha images, cut through the bars.

33
00:04:41,120 --> 00:04:42,400
To steal the Buddha images.

34
00:04:42,400 --> 00:04:52,800
Someone miraculously, one of them was found at the bottom of one of the

35
00:04:54,880 --> 00:04:59,680
rivers, I guess, like a small river, when it dried up or something in the dry season.

36
00:05:00,640 --> 00:05:08,880
It was found, and so they pulled it up and brought it back and had an all night ceremony.

37
00:05:08,880 --> 00:05:14,080
It was something like this. Again, when the details are in precise,

38
00:05:14,080 --> 00:05:18,320
I may be never even paid too much attention to the details, but it was something like that.

39
00:05:19,040 --> 00:05:27,040
We had an all night ceremony to honor the Buddha, or maybe to reestablish its sanctity or

40
00:05:27,040 --> 00:05:32,080
something, its holiness. It was a very cultural thing that I didn't pay too much attention to.

41
00:05:32,080 --> 00:05:36,080
It may have been broken or something, and there was a fixing that had to go on, and at the end

42
00:05:36,080 --> 00:05:43,280
of fixing it, you have to re-bless it or something, but it was kind of considered a miracle that

43
00:05:43,280 --> 00:05:49,840
they got it back. So it was a big deal, and all of the monks, I was a part of it, we would

44
00:05:49,840 --> 00:05:55,680
retake turns chanting or giving talks or that kind of thing of all night until the sun rose.

45
00:05:56,800 --> 00:06:00,320
So the spirit of this still goes on, even today.

46
00:06:00,320 --> 00:06:07,200
This idea of an all night dhamma thing, that it was sort of, I think it was Adjantang

47
00:06:07,200 --> 00:06:11,600
that put it together. I don't know how often it happens. I know in Burma they do all night

48
00:06:11,600 --> 00:06:14,800
chantings and all night teachings from what I hear.

49
00:06:16,320 --> 00:06:23,440
Anyway, the situation here was all of them were trying to listen to the dhamma, but none of them

50
00:06:23,440 --> 00:06:31,280
lasted, or maybe not none of them, but many of them were unable to last.

51
00:06:31,840 --> 00:06:37,440
Right, none of them, they were unable to listen to the dhamma all night. Some of them were

52
00:06:37,440 --> 00:06:50,480
overcome by lust, sexual passion, and went home. Some of them, some of them were overcome by

53
00:06:50,480 --> 00:06:58,320
anger, boredom, maybe. The anger is a very different kind. Many different kinds can be boredom,

54
00:06:59,600 --> 00:07:06,800
can be dislike of the dhamma teachings, the self-hatred idea that every time you hear

55
00:07:09,440 --> 00:07:15,360
something that hits too close to home, maybe they're talking about things that are

56
00:07:15,360 --> 00:07:19,040
unwholesome and you realize that you engage in some of those and so you get angry

57
00:07:19,040 --> 00:07:24,400
at yourself and feel guilty and all these things. So being overcome with that, they were unable to

58
00:07:24,400 --> 00:07:37,280
stay and they went home. Some of them, some of them were overcome by mana conceived, so that would

59
00:07:37,280 --> 00:07:49,040
be the feeling self-righteous and displeased by the dhamma, but not an angry thing to feeling like

60
00:07:49,040 --> 00:07:53,760
they knew better than the monks, or these monks don't know what they're talking about, being attached

61
00:07:53,760 --> 00:08:00,720
to views and that kind of thing. Some of them got caught up with Tina Mida, so they got tired,

62
00:08:00,720 --> 00:08:05,760
that's a common one. We're nodding off and said, I realized that they had to go home

63
00:08:05,760 --> 00:08:19,200
as better until they went home in the middle of the teaching and so they went off.

64
00:08:19,200 --> 00:08:36,640
The next day, the monks gathered and commented on this and just were sort of remarking on how

65
00:08:36,640 --> 00:08:42,640
difficult it is for people to listen to the dhamma and how people aren't all that keen

66
00:08:42,640 --> 00:08:51,440
to take the opportunity to listen to the dhamma. And the Buddha said, monks for the most,

67
00:08:51,440 --> 00:08:56,480
the Buddha came in and asked them what they were talking about when they told him. He remarked

68
00:08:56,480 --> 00:09:06,320
that this is common for people. So for the most part, Yebu Yena, Baba Nisita,

69
00:09:06,320 --> 00:09:15,280
the most part, they are dependent upon or they are attached to existence. Baba, becoming

70
00:09:17,840 --> 00:09:30,320
Baba's way of a langa, we hadn't evaded well, stuck in Baba, stuck in becoming.

71
00:09:30,320 --> 00:09:37,520
The meaning here, Baba is a sort of neutral term that sort of blanket statement for anything

72
00:09:37,520 --> 00:09:45,040
that leads to further becoming. So when you want something, on a very basic level, it leads to

73
00:09:45,040 --> 00:09:52,400
becoming here and now you give a rise to a new something new, which is the plan to get what you want.

74
00:09:52,400 --> 00:10:01,200
And you want to eat and so suddenly the plan to make food arises. Maybe you want to watch a movie,

75
00:10:01,200 --> 00:10:06,480
so you plan to go out and do the, I guess people don't go to the video store anymore.

76
00:10:10,080 --> 00:10:17,680
You, whatever you want, you want to go to Thailand and so you give a rise to something.

77
00:10:17,680 --> 00:10:28,880
You give rise to further becoming something that's not just functional, but it's actually a desire

78
00:10:28,880 --> 00:10:39,360
base, a whole new set of variables. Or if you're angry, you give rise to something new as well,

79
00:10:39,360 --> 00:10:47,840
the very least, a headache, but you can also have anger, give rise to argument, friction, conflict,

80
00:10:48,560 --> 00:10:54,240
war, even, of course, working because by greed, and often it's constantly by greed,

81
00:10:55,600 --> 00:11:00,320
concede, you give rise to this competition or this sense of

82
00:11:00,320 --> 00:11:11,040
this conflict based on power, struggle, and desire for dominance, oppression,

83
00:11:12,000 --> 00:11:17,040
this kind of thing, oppressing others, belittling others, that kind of thing. So you create

84
00:11:17,040 --> 00:11:25,680
this something new, or you'd simply create a new plan for yourself. I deserve to be

85
00:11:25,680 --> 00:11:35,760
kings, so I'm gonna fight my way to become king. I deserve this or that, or I don't deserve this

86
00:11:35,760 --> 00:11:46,400
or that, and so you strive to accordingly. Sometimes it's just out of laziness, this gives rise to

87
00:11:46,400 --> 00:11:55,200
sleep, but it also gives rise to sickness, it gives rise to conflict and problems when you don't

88
00:11:56,720 --> 00:12:03,280
act according to your duties, where you just consume, but don't produce, and as a result,

89
00:12:03,280 --> 00:12:09,040
people become upset, because you're simply a consumer, a mooch, lazy, etc.

90
00:12:09,040 --> 00:12:18,560
And so on. So this is the idea of becoming, it's really anything that leads to, so there was

91
00:12:18,560 --> 00:12:24,160
a question recently about what's wrong with, and there's always questions about what's wrong with

92
00:12:25,760 --> 00:12:32,400
seeking out pleasure, ordinary pleasure, because you do get some, and it's really this

93
00:12:32,400 --> 00:12:43,360
is to avert these two verses point to the, the mindset or the outlook of the Buddha and

94
00:12:43,360 --> 00:12:49,680
and of Buddhists in general, is that it's not enough, and it's not sustainable, because the more you

95
00:12:49,680 --> 00:12:57,440
want, as I said, the more you suffer, the less you get, the less satisfied you are, and in fact,

96
00:12:57,440 --> 00:13:03,040
it's the wrong way, it's unsustainable, you find yourself bouncing back and forth, or for a time,

97
00:13:03,760 --> 00:13:08,880
you're able to balance things before you fall one way or the other, then it's back and forth again,

98
00:13:09,920 --> 00:13:17,200
and it's building up, and disappointment, building up attachment, and stress, or it's

99
00:13:17,200 --> 00:13:28,400
working for a time to, to, well, to either repress, often it's simply to repress our desires,

100
00:13:29,520 --> 00:13:34,880
and we fluctuate back and forth, and go through life, or we go through lives again, and again,

101
00:13:34,880 --> 00:13:37,920
and again, or born, old sick, died, born, old sick, died.

102
00:13:37,920 --> 00:13:51,040
So this is, sort of, the outlook that the Buddha's basing this on, these two verses are actually

103
00:13:52,400 --> 00:13:58,960
one thing that stands out for me, and besides the obvious symbolism or imagery, which is

104
00:13:58,960 --> 00:14:09,600
quite powerful, is sort of congratulatory, it seemed like a good opportunity to congratulate

105
00:14:11,600 --> 00:14:18,240
all of the people involved with this community, all of the listeners, all of you who are

106
00:14:18,800 --> 00:14:22,720
listening to the Dharma, right, taking the time, and some of you every day too,

107
00:14:22,720 --> 00:14:28,720
it's not something I never thought would happen, I thought, okay, once a week, maybe, but some

108
00:14:28,720 --> 00:14:37,440
people actually come on every day to listen to the Dharma. You take the time to come online and sit

109
00:14:37,440 --> 00:14:45,360
still for an hour, half an hour an hour, even to ask questions, so to get involved, to perform

110
00:14:45,360 --> 00:14:53,680
this act of good karma of asking questions, and many of you are very respectful, and so

111
00:14:53,680 --> 00:14:56,160
that being respectful, and all sorts of good karma going on.

112
00:14:59,600 --> 00:15:04,240
And, you know, that's something to be proud of, because even in the context of the story,

113
00:15:07,760 --> 00:15:12,000
many people, I mean, in the context of the story, these people are still to be

114
00:15:12,000 --> 00:15:17,960
congratulated, because they tried, and it's not like they didn't hear some of the

115
00:15:17,960 --> 00:15:21,840
Dharma, it's just they tried to do something beyond their capabilities, but

116
00:15:25,360 --> 00:15:30,720
all of the people here should be congratulated as well for having the good intentions. Many people

117
00:15:32,000 --> 00:15:40,240
don't even think about crossing, or if they think about crossing, they never do anything to try

118
00:15:40,240 --> 00:15:45,760
to cross. They would never come and meditate. If you look at the list of meditators that we have,

119
00:15:45,760 --> 00:15:53,360
we've got, see how many we have to name. We've got, actually, a fairly small list today.

120
00:15:54,720 --> 00:15:59,520
Oh, and a bunch of orange people, maybe I spoke to you soon. What happened today?

121
00:15:59,520 --> 00:16:08,160
Well, still, we have many, many people and we have people meditating on this side around the clock.

122
00:16:10,720 --> 00:16:14,000
People coming to listen to the Dharma every day.

123
00:16:17,680 --> 00:16:22,640
So this is a sign of at least wanting to cross. Maybe some people just listen to the talks

124
00:16:22,640 --> 00:16:30,320
or watch my YouTube videos, and maybe dip their foot in the water, but don't ever cross. But at the

125
00:16:30,320 --> 00:16:36,080
very least, there's some thought, and this is the first step. The first step is thinking about it,

126
00:16:37,200 --> 00:16:45,920
having the giving rise to the intention or the desire to better one. So

127
00:16:45,920 --> 00:16:51,680
as opposed to just running up and down the shore, because that's what it's like,

128
00:16:52,320 --> 00:16:56,800
that's what the Buddha likens most of our activity to, just running up and down the shore,

129
00:16:58,640 --> 00:17:04,640
sometimes running up and down out of desire, sometimes running up and down out of anger,

130
00:17:04,640 --> 00:17:18,480
fear, worry, conceit, with many, many ways of running around in circles.

131
00:17:19,680 --> 00:17:24,240
Running around in circles in the kingdom of death, and this is where we find ourselves being born

132
00:17:24,240 --> 00:17:36,880
again and again, and again, and again, and again, and again, and again, and again, and again,

133
00:17:40,080 --> 00:17:44,160
not really learning from our mistakes, so our intention here, our practice here,

134
00:17:44,880 --> 00:17:52,960
is to break that, to cross over, to go beyond, to go beyond death, to figure the system out,

135
00:17:52,960 --> 00:18:00,480
to come to understand the system, and to understand becoming, and as a result, not be dependent

136
00:18:00,480 --> 00:18:05,680
upon it, one way of explaining how why we're dependent upon it is, because we don't understand

137
00:18:05,680 --> 00:18:15,120
it, we're dependent upon it, because it runs us, we don't run it, we don't lord over death,

138
00:18:15,120 --> 00:18:24,000
deathlords over us, becoming as well, we act in such a way, usually that,

139
00:18:26,720 --> 00:18:34,480
and we don't fully understand the system, we don't fully grasp what we're dealing with,

140
00:18:34,480 --> 00:18:40,720
or what's in store for us when we chase after things,

141
00:18:40,720 --> 00:18:47,760
and we're not clear in our minds as to the nature of our addictions, and our versions,

142
00:18:47,760 --> 00:18:54,160
and our conceits, and our views, and so we hold on to them often without a clear understanding,

143
00:18:54,160 --> 00:18:59,040
usually without a clear understanding, once we understand them clearly, this is the crossing over,

144
00:18:59,040 --> 00:19:13,200
this is the rising above, so it's rather a poetic verse, simple story, simple verse really,

145
00:19:13,200 --> 00:19:27,120
simple meaning, it's kind of hard teaching, it lays bare the fact that most of what we do is

146
00:19:27,120 --> 00:19:30,240
just running up and down the shore, doesn't really accomplish anything in the end,

147
00:19:32,640 --> 00:19:37,600
but we should, on the other hand, it should be an encouraging teaching, because whatever the good

148
00:19:37,600 --> 00:19:44,000
things that we do, this is our coming closer to be one of those few people who is actually able

149
00:19:44,000 --> 00:19:50,880
to cross over, to actually actually able to rise above and free themselves from the wheel of

150
00:19:50,880 --> 00:19:58,720
suffering, from the kingdom of death, so good work everyone, and thank you all for tuning in,

151
00:19:58,720 --> 00:20:05,760
and for supporting these teachings with your practice, for practicing and continuing on this practice,

152
00:20:07,840 --> 00:20:13,120
so that's the Dhammapada teaching for tonight, thank you all for tuning in, keep practicing and

153
00:20:13,120 --> 00:20:40,720
be well, it's the fan was with me again tonight, hello, let's take a moment's place,

154
00:20:44,080 --> 00:20:52,160
do you have any questions? Yes, first question is, the cat charity I do some volunteer work for

155
00:20:52,160 --> 00:20:58,480
running a raffle to raise cash, you say that gambling is not in line with the teachings,

156
00:20:58,480 --> 00:21:04,880
but if the money raised goes towards helping running something good and makes everyone feel good,

157
00:21:05,840 --> 00:21:11,840
would that make a difference? I think so, I mean, no one's going to come out saying

158
00:21:11,840 --> 00:21:20,880
feeling bad about having donated, no one's going to come out saying, if only I had, it's not,

159
00:21:21,440 --> 00:21:25,520
it's not exactly the same as gambling, it's not perfect, and I wouldn't say you're

160
00:21:25,520 --> 00:21:30,800
spot free from all that in wholesomeness of gambling, because you're encouraging greed and people

161
00:21:30,800 --> 00:21:35,520
still, it's like a white and black karma, that's definitely a good example of a white and

162
00:21:35,520 --> 00:21:43,360
black karma, because people are doing it, out of desire to help, but also out of desire to win.

163
00:21:45,680 --> 00:21:48,320
For most people, I don't think it's such a big deal that they win, they're just,

164
00:21:49,120 --> 00:21:54,000
it's a way of encouraging people, encouraging the people's intention, but you're encouraging

165
00:21:54,000 --> 00:21:59,600
with a little bit of greed or excitement for, well, what if I did when, wouldn't that be kind of neat?

166
00:21:59,600 --> 00:22:05,440
I don't think most people buy raffle tickets thinking, yeah, I really want to win that, but

167
00:22:05,440 --> 00:22:10,720
but I don't know really, even if people just do it to support the charity, which is a good part.

168
00:22:12,880 --> 00:22:17,360
Still, it's not, not how I would do things, not how I would support a charity, but

169
00:22:18,080 --> 00:22:19,920
or try to get this charity supported.

170
00:22:19,920 --> 00:22:31,360
Hello, Bante, when I am talking to someone in my thoughts while meditating, should I acknowledge

171
00:22:31,360 --> 00:22:34,000
this as talking or thinking? Thanks.

172
00:22:37,120 --> 00:22:42,000
When you were talking to someone, I think when they're talking to someone in their head,

173
00:22:42,000 --> 00:22:50,800
I think, like, going over a conversation or something. Yeah, I would still say thinking, right,

174
00:22:50,800 --> 00:22:54,880
going over a conversation is more thinking. If you want, you can say talking. Again, the word

175
00:22:54,880 --> 00:23:01,040
is not the most important thing, which word you pick, it's just something that keeps you objective.

176
00:23:01,040 --> 00:23:07,360
So it's not damn that person who I talked with or why I really love that person, really

177
00:23:07,360 --> 00:23:13,280
attracted to that person, et cetera, et cetera. It keeps you, whatever keeps you objective.

178
00:23:13,280 --> 00:23:19,600
So if it's talking, just say talking, talking, and you'll see now it's just a thought,

179
00:23:19,600 --> 00:23:22,640
you're thinking is better, because you'll see that it's just a thought.

180
00:23:24,320 --> 00:23:29,680
Or if they mean when they're actually talking to someone, what should they do in their thoughts?

181
00:23:29,680 --> 00:23:32,800
Like, which, how should they be mindful? They might mean that.

182
00:23:32,800 --> 00:23:40,320
Mindful of your lips moving, feeling, feeling. You might feel the feelings in the body,

183
00:23:40,320 --> 00:23:48,640
in the emotions, the thoughts, the intentions to speak. Your mind is pretty quick, so you can do

184
00:23:48,640 --> 00:23:50,320
this all and still continue to talk.

185
00:23:50,320 --> 00:24:04,160
Okay. When I have emotions, for example, anger, I know anger, and then I just know feeling

186
00:24:04,160 --> 00:24:07,920
feeling, because it seems that there isn't anger, but just sensations.

187
00:24:08,720 --> 00:24:12,400
Is this correct noting or should I keep noting anger because it was anger?

188
00:24:12,400 --> 00:24:17,440
That's awesome. Yeah, no, that's very clear. All of the emotions are like that. They have physical

189
00:24:17,440 --> 00:24:23,600
counterparts, physical manifestations, and physical effects, and they are distinct from the anger

190
00:24:23,600 --> 00:24:29,920
and the greed and the worry and the fear and et cetera, et cetera. So acknowledge them, separate,

191
00:24:29,920 --> 00:24:30,560
absolutely.

192
00:24:34,720 --> 00:24:40,240
Sometimes during sitting meditation, I get very distracted because I am tired.

193
00:24:41,040 --> 00:24:46,560
I start to fall asleep. I find that I can focus better if my eyes are open.

194
00:24:46,560 --> 00:24:49,920
Does this take away from the point of meditation or is this okay?

195
00:24:52,080 --> 00:24:52,800
Yeah, it's okay.

196
00:24:54,880 --> 00:25:00,880
It's not wrong. It's better to have your eyes closed when you can, but if you find it keeps you away,

197
00:25:00,880 --> 00:25:08,160
it gets really not wrong to open your eyes. It's better, of course, to overcome the drowsiness

198
00:25:08,160 --> 00:25:14,160
and keep meditating, but you can do it either way. I mean, your mind shouldn't be with your eyes

199
00:25:14,160 --> 00:25:20,960
anyway. It should be with whatever your object is. So with the stomach. In fact, in the end,

200
00:25:20,960 --> 00:25:28,240
it's probably just a trick and it's not really the cause of your drowsiness. Once you get

201
00:25:28,240 --> 00:25:33,120
equally focused with your eyes open and you're still going to get drowsy.

202
00:25:33,120 --> 00:25:42,880
But at our solutions, probably to do walking meditation, I would think.

203
00:25:50,240 --> 00:25:55,600
How long do you recommend a student practice your teachings on meditation, according to your

204
00:25:55,600 --> 00:26:00,720
booklet, before undertaking the formal study with you over the scheduled one-on-one

205
00:26:00,720 --> 00:26:04,240
meetings via the internet? Thank you.

206
00:26:07,200 --> 00:26:11,280
Take a week. If you've done a week, at least an hour a day,

207
00:26:12,480 --> 00:26:19,520
if you can, at least an hour, one hour a day, then I'm happy to help.

208
00:26:19,520 --> 00:26:31,680
What's matter supposed to feel like? It kind of bothers me that I can't feel much matter,

209
00:26:31,680 --> 00:26:36,800
but I'm thinking I might have the wrong idea about what it is. Is it supposed to be sentimental?

210
00:26:38,560 --> 00:26:41,840
Kind of, yeah. I wouldn't worry too much about it.

211
00:26:41,840 --> 00:26:48,720
Mehta is something that comes naturally as you practice in sight meditation. We concern more about

212
00:26:48,720 --> 00:27:00,160
insight. Mehta is secondary. But, you know, if you really want to experience it, focus on

213
00:27:00,160 --> 00:27:06,160
people who you know you have that love for, like family members may be of good friends.

214
00:27:06,160 --> 00:27:11,200
It's a sense of friendliness wishing for them to be happy.

215
00:27:12,960 --> 00:27:14,400
Good intentions toward them.

216
00:27:22,960 --> 00:27:29,680
Does the process of samsara death and rebirth occur on other planets or in other

217
00:27:29,680 --> 00:27:46,960
solar systems? No idea. And that's it. Okay. I think we'll stop then.

218
00:27:46,960 --> 00:27:58,480
Oh, wait, maybe there is one right here. Can you tell us the best way to help

219
00:27:59,920 --> 00:28:03,440
feed you currently? Visit it through you caring site.

220
00:28:03,440 --> 00:28:11,680
No, the you caring site is a project set up by Robin to help support the monthly expenses of just

221
00:28:11,680 --> 00:28:21,200
having a house, having a place. So that's for things like rent and utilities.

222
00:28:22,880 --> 00:28:30,800
That's something fairly specific. Although it goes, it's that's the nonprofit organization that

223
00:28:31,680 --> 00:28:37,600
is run by a group of volunteers. So it's general. I mean, it can be used for various things,

224
00:28:37,600 --> 00:28:45,040
but it isn't used to support me actually, except in so far as I live here and depend very much

225
00:28:45,040 --> 00:28:53,120
on this place and the bills and so on. I mean, it's really a big thing. But yeah, Fernando has

226
00:28:53,120 --> 00:28:57,920
probably the right language, probably your best bet. I will say that I don't, you know, food is

227
00:28:57,920 --> 00:29:04,400
really not an issue at this point. So it's often the one thing that people would like to help with.

228
00:29:04,400 --> 00:29:11,600
It's not an easy thing. I mean, ideally, if you have a house somewhere nearby,

229
00:29:12,800 --> 00:29:22,880
they can come for arms one morning or even not so close. I could find time to travel throughout

230
00:29:22,880 --> 00:29:28,960
Ontario. Suppose it's going to America for arms is a bit much.

231
00:29:28,960 --> 00:29:37,760
Although, you know, outside of the rains, I can take time to go and stay in someone's backyard

232
00:29:38,480 --> 00:29:48,240
during the hot season. But then it would be more like a trip than an arms giving.

233
00:29:50,320 --> 00:29:54,080
But barring that, you know, there's ways of, well, they've got to go to the food support. That's

234
00:29:54,080 --> 00:30:00,240
what we figured out so far. Then that's Tina is doing most of the organizing there. I think

235
00:30:01,280 --> 00:30:03,040
that there's several people involved with that.

236
00:30:10,160 --> 00:30:13,360
Okay, so that's all for today. Thank you all for tuning in.

237
00:30:14,240 --> 00:30:17,120
Thank you. Thanks, Stefano, for taking part.

238
00:30:17,120 --> 00:30:27,120
Open it. Have a good night, everyone.

