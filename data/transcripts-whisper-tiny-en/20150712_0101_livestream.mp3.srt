1
00:00:00,000 --> 00:00:28,680
Okay, good evening, broadcasting life, delight, loving it, so today's quote is about teaching

2
00:00:28,680 --> 00:00:37,080
it's interesting, as I said, we've got the, there's a monk who looked up all the

3
00:00:37,080 --> 00:00:45,760
sources for all these quotes, and so when I read this one, immediately thought there's

4
00:00:45,760 --> 00:00:54,480
something fishy about it, and it's why it could happen indeed, it's not a direct quote.

5
00:00:54,480 --> 00:01:05,880
It's actually an example of how dangerous it is to travel, to trust, or how easy it is,

6
00:01:05,880 --> 00:01:15,640
to misquote the Buddha, you know, it's actually almost correct, and that's the

7
00:01:15,640 --> 00:01:20,640
problem, that's the most dangerous part, but if you read this, I mean basically it says

8
00:01:20,640 --> 00:01:30,560
that only when one is a teacher can one be a complete, dumber practitioner.

9
00:01:30,560 --> 00:01:37,240
Well this quote actually comes from is the book of 10s, and the Buddha and the Buddha

10
00:01:37,240 --> 00:01:48,760
gives 10 qualities, and so he says, the person can have faith, but not be moral, and

11
00:01:48,760 --> 00:02:01,440
in that case one should practice morality, and then they are complete in regards to that.

12
00:02:01,440 --> 00:02:17,120
Therefor they should become full, or that factor should be fulfilled, that factor

13
00:02:17,120 --> 00:02:22,320
should be fulfilled by them.

14
00:02:22,320 --> 00:02:33,880
Once it is fulfilled by them, then once they are faithful, confident, and moral, then

15
00:02:33,880 --> 00:02:37,440
sotin and vina, parepulohanti.

16
00:02:37,440 --> 00:02:43,800
So they are complete, but they are complete, tain and vina, in regards to that, unga, that

17
00:02:43,800 --> 00:02:44,800
factor.

18
00:02:44,800 --> 00:02:50,680
So that's the crucial part that's left out of this quote, because this quote in Buddha

19
00:02:50,680 --> 00:02:57,960
Watchana, which is the book we're doing by that quote, simply states that one one is

20
00:02:57,960 --> 00:03:07,280
a teacher, one is complete, but what the text actually says is one one is also a teacher

21
00:03:07,280 --> 00:03:11,960
of dhamma, then one is complete in regards to that factor, which means they've got an

22
00:03:11,960 --> 00:03:15,880
extra factor that was lacking when they were in complete.

23
00:03:15,880 --> 00:03:21,280
And teaching the dhamma is only the, it's only in a list of them, we have to believe

24
00:03:21,280 --> 00:03:25,280
her, the virtuous learned, it's only the fourth in a list of ten.

25
00:03:25,280 --> 00:03:38,160
So the actual text goes on and says one can be a teacher, the dhamma, but it's not one

26
00:03:38,160 --> 00:03:47,880
who frequents assemblies, parisawach at all, and it doesn't, not, not frequents, but there's

27
00:03:47,880 --> 00:03:58,480
one who doesn't get involved with the community, you know, like helping out and keeping

28
00:03:58,480 --> 00:04:00,880
up with work in the community and so on.

29
00:04:00,880 --> 00:04:07,000
Or maybe no parisawach or it could be goes, goes about with the community, so it doesn't,

30
00:04:07,000 --> 00:04:15,720
so has a retinue, has a companion, has a companion, a little bit of practice with it.

31
00:04:15,720 --> 00:04:21,800
So this list is actually quite interesting, but it's not saying that one has to be a teacher,

32
00:04:21,800 --> 00:04:30,480
and it's talking about a beaker in fact, talking about a monk, so that's another thing

33
00:04:30,480 --> 00:04:31,840
that's missing here.

34
00:04:31,840 --> 00:04:40,400
So there's something about the duty of a monk to teach, and I think that is reasonable

35
00:04:40,400 --> 00:04:57,680
to assume that to suggest that monks should try to teach, to be fully perfected as a monk,

36
00:04:57,680 --> 00:05:04,680
and should also teach, speak the dhamma, but it's, it's, the word is dhamma, katik,

37
00:05:04,680 --> 00:05:13,680
called one who speaks on a dhamma, it's not one who, yeah, I just want to teach the

38
00:05:13,680 --> 00:05:20,360
dhamma, who gives dhamma talks, basically, gives the dhamma day, so there.

39
00:05:20,360 --> 00:05:26,400
So one should, so to be in perfect beaker, one should be endowed with faith, virtuous,

40
00:05:26,400 --> 00:05:33,640
learn, a speaker of the dhamma, one who frequents assemblies, one who confidently teaches

41
00:05:33,640 --> 00:05:39,760
the dhamma to an assembly, an expert on the discipline, forest dweller who resorts

42
00:05:39,760 --> 00:05:44,880
to remote lodgings, can be able to gain and will without trouble or difficulty, the

43
00:05:44,880 --> 00:05:51,240
four dhammas that come through the higher mind of our pleasant times in this very life.

44
00:05:51,240 --> 00:05:56,800
And finally, should, with the destruction of the taints, realized for themselves with

45
00:05:56,800 --> 00:06:02,600
direct knowledge in this very life, the taints' numberation of mind, some be free from

46
00:06:02,600 --> 00:06:03,600
dhamma.

47
00:06:03,600 --> 00:06:08,080
So number nine is one should be perfected in summit of practice, and number ten is

48
00:06:08,080 --> 00:06:15,160
one should be protected, and we pass a no or more directly in enlightenment in the destruction

49
00:06:15,160 --> 00:06:16,160
of dhammas.

50
00:06:16,160 --> 00:06:26,240
So ten things here that it's a really useful list, shows what a perfect monk is.

51
00:06:26,240 --> 00:06:30,360
And I don't think you could argue that this is a list that has meant for all people

52
00:06:30,360 --> 00:06:35,200
because it's talking about the patimoka, right, talking about the discipline, so I'm living

53
00:06:35,200 --> 00:06:41,240
in the forest, which is not expected of even of all monks.

54
00:06:41,240 --> 00:06:48,520
But the Buddha says, Pika who possesses these ten qualities is one who inspires confidence

55
00:06:48,520 --> 00:06:52,640
in all respects, and who is completely in all aspects.

56
00:06:52,640 --> 00:07:00,000
So there's lots of monks, even who don't fulfill all ten of these, there are aunts who wouldn't

57
00:07:00,000 --> 00:07:05,080
necessarily have ten of these, who wouldn't have all ten of these, so frequentig assemblies

58
00:07:05,080 --> 00:07:11,360
or having assemblies, there are monks who don't belong, but they would not be considered,

59
00:07:11,360 --> 00:07:16,640
but those are aunts who would not be considered perfect, and they would not be considered

60
00:07:16,640 --> 00:07:18,920
perfect in all aspects.

61
00:07:18,920 --> 00:07:26,680
So think blame worthy about it, it's just less inspiring.

62
00:07:26,680 --> 00:07:33,080
But anyway, let's go through them and we can talk about these ten qualities.

63
00:07:33,080 --> 00:07:39,480
Remember, we should just focus on the idea of teaching.

64
00:07:39,480 --> 00:07:49,680
I think the thought behind, or the intention behind this misquote on the Buddha is fine,

65
00:07:49,680 --> 00:07:57,440
there's no ball, there seems to be the intention is to suggest that teaching makes one

66
00:07:57,440 --> 00:08:07,240
complete, and without teaching, without sharing, the one is incomplete, so it's useful

67
00:08:07,240 --> 00:08:16,360
quote, if it were a quote, to show that you're not selfish, you know, don't practice

68
00:08:16,360 --> 00:08:24,600
meditation just to help ourselves, helping others as a means of becoming completed to

69
00:08:24,600 --> 00:08:33,640
enjoy, and in some sense that you can expand it more generally to say that helping others

70
00:08:33,640 --> 00:08:38,840
is a part of our practice, so doing good for others is something that helps yourself

71
00:08:38,840 --> 00:08:44,000
as a part of our spiritual development, so anyone who says spiritual development, people

72
00:08:44,000 --> 00:08:50,240
focus on the spiritual development, or only focus on themselves, it's not really true,

73
00:08:50,240 --> 00:08:59,240
but it's truly spiritually developed as someone who has at least willing to help others

74
00:08:59,240 --> 00:09:05,040
in any chance they may not have the opportunity, they may find themselves living off

75
00:09:05,040 --> 00:09:16,440
in the forest, but they have a willingness and for the most part, in most cases do help

76
00:09:16,440 --> 00:09:24,360
others greatly and spend much of their time helping others.

77
00:09:24,360 --> 00:09:34,840
Teaching is, I think a lot of people are afraid to teach all of you this, thinking that

78
00:09:34,840 --> 00:09:41,360
you have to be someone great, it was an interesting question posted on the internet,

79
00:09:41,360 --> 00:09:57,720
I don't know if it was a meditation teacher, but the very brief question is, my students

80
00:09:57,720 --> 00:10:04,280
formal feedback about teaching makes me feel defeated, didn't reply to it immediately,

81
00:10:04,280 --> 00:10:10,600
I can relate to that, teaching is an interesting thing, your students teach you a lot,

82
00:10:10,600 --> 00:10:18,520
so people are afraid and have this belief that you need some kind of formal training

83
00:10:18,520 --> 00:10:25,120
to be a teacher, and the other people may be taken too lightly to call themselves teachers

84
00:10:25,120 --> 00:10:35,440
without having proper training, I think there's no, the problem is often here about this

85
00:10:35,440 --> 00:10:42,320
idea of being a qualified teacher, as though there was a category, you could put yourself

86
00:10:42,320 --> 00:10:48,600
in a category, or you qualified, and I think that's ridiculous, teaching is about teaching

87
00:10:48,600 --> 00:10:56,080
meditation anyways, about experience, so there's better teachers and there's worse teachers,

88
00:10:56,080 --> 00:11:03,360
it's not a categorical leap to become a teacher, and I was teaching, I remember when

89
00:11:03,360 --> 00:11:11,600
I did my first course in meditation, the layman who was my teacher, a couple of days

90
00:11:11,600 --> 00:11:17,920
after I started, or day after I started maybe, he had me teach the next student how

91
00:11:17,920 --> 00:11:22,400
to meditate, show him how to do the mindful frustration in the walking and the sitting, so

92
00:11:22,400 --> 00:11:31,200
from my very first meditation course I was already a teacher, and that never stopped and

93
00:11:31,200 --> 00:11:40,720
it increased, so he was quite good in getting his students to teach, to give him that,

94
00:11:40,720 --> 00:11:47,600
I ended up leaving this teacher, got kind of turned off by the way that he taught, but

95
00:11:47,600 --> 00:11:59,360
he was great in organizing people, and he still has his good organization, and so he got me

96
00:11:59,360 --> 00:12:04,800
taught me how to teach, showed me, and told me stuff about the 16th stages of knowledge,

97
00:12:04,800 --> 00:12:11,520
and how to recognize them, it was quite helpful in the beginning, but I say now he's not my

98
00:12:11,520 --> 00:12:17,360
teacher anymore, people got really upset about that, because you're not supposed to say that

99
00:12:17,360 --> 00:12:24,400
I guess, but well, it's not my team, it's no longer my teacher, but even when I went to practice,

100
00:12:24,400 --> 00:12:30,000
when I first started, even before I switched to having to take a gentong as my teacher,

101
00:12:33,520 --> 00:12:39,440
how it started is exactly that, I was invited to sit in on his teaching sessions,

102
00:12:42,000 --> 00:12:49,760
and that's when I realized what I was missing, what a great teacher that gentong really is

103
00:12:49,760 --> 00:13:03,520
wonderful, amazing, hard to put into words, sort of person that, and it only needs once,

104
00:13:04,880 --> 00:13:09,360
less than once in a lifetime, not someone you meet every lifetime, for sure,

105
00:13:12,480 --> 00:13:19,280
and people would not meet such a great person, anyway, not to lay on hard or something,

106
00:13:19,280 --> 00:13:31,040
but was lucky to spend time with him, but the point being that, from what I've seen,

107
00:13:31,040 --> 00:13:35,920
it's just always been about learning how to teach more and better, and so, like when I,

108
00:13:37,680 --> 00:13:42,400
when I first went back to university, I learned a little bit of the knowledge, I was in the

109
00:13:42,400 --> 00:13:49,600
monk yet, but when some of my fellow students, from when I was at university, the first time,

110
00:13:51,040 --> 00:13:58,880
one of them came up to me and asked me, if I could tell him something about meditation,

111
00:13:58,880 --> 00:14:04,800
and I actually let him almost grow an entire course, doing it over a period of time,

112
00:14:04,800 --> 00:14:11,200
and he didn't actually come and stay in a center or something, but he was practicing at home,

113
00:14:11,200 --> 00:14:17,120
and I let him through the stages, and it changed his life. He was a biologist, and he was cutting

114
00:14:20,560 --> 00:14:25,840
live mice and performing experiments on them, and it really affected his meditation, and he

115
00:14:25,840 --> 00:14:30,320
came to me, and he said, you know, look, I don't know what to do, I just feel awful about this,

116
00:14:30,320 --> 00:14:41,520
I feel like I'm stuck, I don't want to be killing animals anymore, and I know it's affecting

117
00:14:41,520 --> 00:14:46,560
my meditation, I say, well, you have to choose, I told him straight up, and he has to choose

118
00:14:46,560 --> 00:14:53,360
do you want to, because they're two different paths, and so he had to drop out of biology,

119
00:14:53,360 --> 00:14:59,360
switch his major over to political science or something, he now works for the Canadian government,

120
00:14:59,360 --> 00:15:11,920
I think, last I heard, I'm not sure actually, but that from my own basic, basic experience,

121
00:15:11,920 --> 00:15:18,560
knowledge and politics, so from then on, and there was another man as well who did they really

122
00:15:18,560 --> 00:15:24,800
do the same thing with these two guys, and this other guy, he'd gone all over Ontario,

123
00:15:24,800 --> 00:15:35,440
which is admittedly not a hotbed of Buddhism or wasn't 15 years ago, but he went to all his

124
00:15:35,440 --> 00:15:45,200
different centers, maybe even outside of Ontario, but he said, you know, I haven't found anything

125
00:15:45,200 --> 00:15:52,960
that is so clear and simple as what you're teaching, and I'm not trying to brag, it's not

126
00:15:52,960 --> 00:15:57,760
nothing like that, and that's the opposite of the intention here, the intention is to show that

127
00:15:59,120 --> 00:16:08,560
if a teaching is good, it teaches itself to some extent, you know, the important point is to know

128
00:16:08,560 --> 00:16:14,000
your limits, not to reject the concept of teaching, that's my point, it's not categorical,

129
00:16:14,000 --> 00:16:19,920
or you say, I'm not a teacher, so I will not teach anything. There were two women who came

130
00:16:19,920 --> 00:16:25,920
to practice with Ajahn Tong and I'm translating for them, I think, and they stayed maybe five days,

131
00:16:25,920 --> 00:16:32,560
maybe five to seven days, I think, and I'll never forget them because when they left, Ajahn

132
00:16:32,560 --> 00:16:40,800
turned to them and said, I want you to, or I translate it, so he said to them, I want you to teach

133
00:16:40,800 --> 00:16:46,400
exactly as you've been taught, so take what you've learned here and go back and teach it to people

134
00:16:46,400 --> 00:16:54,960
in your country just as it's been taught to you, and that's sort of the, the attitude,

135
00:16:54,960 --> 00:17:00,880
as far as I've seen, it's not a problem to let someone teach or to encourage people to teach,

136
00:17:02,240 --> 00:17:08,560
the point is to have people know their limits, they're easy to go overboard, I've seen teachers

137
00:17:08,560 --> 00:17:14,720
so people put themselves up as teachers and claim to know many, many things and I'm really unqualified

138
00:17:14,720 --> 00:17:26,320
to do what they do and end up misleading their students. Another thing is to be able to

139
00:17:27,520 --> 00:17:38,000
be willing to learn from your students and do not get upset and feel defeated when your students

140
00:17:38,000 --> 00:17:56,560
and your ego on a platter. I had one student throw candy at me, I had a whole group of students,

141
00:17:57,760 --> 00:18:06,000
I was very intent upon having my students not taught and they were so awful and all these

142
00:18:06,000 --> 00:18:14,400
tourist kids in, in chain way and they were just talking all the time and I was very strict trying

143
00:18:14,400 --> 00:18:20,880
to stop them from talking and they had a whole group of them leave, 10 of them I think, it's just

144
00:18:20,880 --> 00:18:24,640
all decided that they'd had enough of me and they didn't even know it, these people didn't even

145
00:18:24,640 --> 00:18:31,920
know each other, but they all came together and decided that they were leaving and it was devastating

146
00:18:31,920 --> 00:18:44,320
to grab this happen, I've seen been there done that, had a dog, but the point is it helps people,

147
00:18:44,880 --> 00:18:50,000
I'm not the number one teacher out there, certainly nowhere in the top 10 though,

148
00:18:52,560 --> 00:18:58,960
but I can see that teaching helps people, that's what's always kept me going and I can also see

149
00:18:58,960 --> 00:19:03,200
that it helps me, teaching is something that helps the individual as well,

150
00:19:04,240 --> 00:19:09,440
helps them in their own practice, keeps them on their toes, keeps them training, keeps them

151
00:19:09,440 --> 00:19:15,200
accountable, it can, doesn't have to, it's all in you, it's all in how you look at it as with everything,

152
00:19:19,440 --> 00:19:25,680
it's all in your sincerity, why are you teaching, are you teaching because you want to be a teacher,

153
00:19:25,680 --> 00:19:32,000
I mean admittedly I guess some part of me teaching is as a duty because if I don't teach,

154
00:19:32,000 --> 00:19:36,400
I probably won't get fanned and I probably won't have a place to stay and that kind of thing,

155
00:19:37,440 --> 00:19:44,800
it's not necessarily true, but teaching allows me some support, so there is that, but

156
00:19:48,720 --> 00:19:53,200
if you're teaching to just become a big teacher or if you're teaching to become rich in

157
00:19:53,200 --> 00:20:02,720
a famous or so, it's a problem, it's a good reason to start, but there's no good reason to say,

158
00:20:03,760 --> 00:20:09,520
I mean anyone can teach, anyone who's practiced can teach, they should teach as they've learned,

159
00:20:09,520 --> 00:20:13,920
to the extent that you've learned it, to the extent that you understand it, to that extent you

160
00:20:13,920 --> 00:20:22,480
teach a teacher, the story of Asaji, one of the first five hour hunts, disciples of the Buddha,

161
00:20:22,480 --> 00:20:30,000
Buddha, and asari put the Upa Tisa who was to become, sorry, put that came up to him in the streets

162
00:20:30,000 --> 00:20:39,200
when he was on arms, and asked him to teach, and Asaji said, no, I can't teach, I don't know anything,

163
00:20:39,200 --> 00:20:47,440
I'm just, I'm new to this rich, and so I've always said to you, I won't be able to teach in

164
00:20:47,440 --> 00:20:52,320
detail, I said, no, just teach me something, breathe, that's all I need, and so he taught him,

165
00:20:52,320 --> 00:21:04,400
it's very famous for a C.A. Dhamma, he had to blah, blah, blah, blah, blah, blah, blah, blah,

166
00:21:04,400 --> 00:21:06,640
blah, blah, blah, blah, blah, blah, blah, blah, blah, blah, blah, blah, blah, blah,

167
00:21:06,640 --> 00:21:12,960
quote, exactly, but basically, cause and effect, the Buddha taught cause and effect,

168
00:21:12,960 --> 00:21:18,960
and so he said, so he taught based on, as he understood it, though he was in our hunt,

169
00:21:18,960 --> 00:21:24,320
so he's fully qualified to speak about the Dhamma, as he's understood it,

170
00:21:26,560 --> 00:21:30,320
but there's a pattern there, the idea of teaching what you know,

171
00:21:30,320 --> 00:21:37,120
teaching what you understand, and even in the rest of the demogas, it says, you don't have to be

172
00:21:37,120 --> 00:21:41,440
enlightened to teach, someone who knows a lot of the Dhamma, who's enlightened,

173
00:21:42,320 --> 00:21:48,080
can often help others to become enlightened, doesn't help themselves, there's a story of the teacher,

174
00:21:49,200 --> 00:21:53,840
all the students became enlightened, and he was still an ordinary person, so he ran off into the forest,

175
00:21:53,840 --> 00:21:59,360
and you know, such a great teacher and such, so famous, and well known that the angels started

176
00:21:59,360 --> 00:22:04,160
practicing with him while he was in the forest, and when he broke down and cried, when the angels

177
00:22:04,160 --> 00:22:08,720
started crying, and he said, why are you crying? And he said, well, I'm doing it because

178
00:22:08,720 --> 00:22:13,440
I know you're such a great teacher, I thought that must be the past enlightenment, so I'm crying.

179
00:22:17,760 --> 00:22:21,600
So it's about perspective, and that no one should be afraid of teaching.

180
00:22:21,600 --> 00:22:28,480
The problem only comes when we teach beyond our capabilities, or when we

181
00:22:32,560 --> 00:22:36,720
teach for the wrong reasons, at least two things.

182
00:22:38,880 --> 00:22:42,000
So when we are only teaching and not practicing a very simple,

183
00:22:43,360 --> 00:22:47,440
and I think I'm just going to talk there, that's a little bit about teaching, the idea of

184
00:22:47,440 --> 00:22:55,840
teaching as an encouragement for people, something to think about. So that's the number for the

185
00:22:55,840 --> 00:23:25,680
same thing, thanks for tuning in.

