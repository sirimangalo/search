Hello, today I'll be talking about one of the most crucial aspects of the Buddha's teaching.
This is the quality of the Buddha's teaching that is one of cause and effect that the
teaching of the Buddha has the quality of being true to the nature of how things arise
and how things cease, that everything that arises comes from a cause and the teachings of
the Buddha are able to describe the actual cause that gives rise to the state of affairs
that we find ourselves in.
And also the effects that come from the way that we respond and interact with the world
around us.
So our ability to understand why it is that we are the way we are and what consequences
will come from our actions and from our state of mind.
This is the Buddha's teaching on dependent origination or Patitya Samupada, which says
that all of our happiness and suffering, all of the difficulties and upsets, stress and
dissatisfaction that we meet with in the world, as well as all of the peace and happiness
and well-being that comes, has a cause and the teaching of the Buddha is able to describe
this cause, the teaching of dependent origination is the perfect method for us to understand
and to thereby adjust and overcome the causes of suffering and thereby overcome all of
suffering, any type of suffering and stress that might come in the future.
The teaching of dependent origination in the Buddha is best seen as a practical teaching.
Often when people approach the teachings of the Buddha, they will, as I've said before,
look at it from an intellectual point of view, trying to understand it logically to think
of examples by which they can understand and accept and agree with the teachings
as they're presented, ways of explaining and ways of assimilating it with their own view
and their own understanding of reality.
But this is not really the most useful beneficial and proper way to approach the Buddha's
teaching, most especially the Buddha's teaching on cause and effect, because this teaching
is something that can be seen, can be understood based on our experience of reality,
most especially our experience in the meditation practice.
So when we undertake to practice meditation or in our daily lives, when we encounter difficulties
in problems, when we encounter situations that give rise to one things and desires or
aversions and dislikes that give rise to our conceit and detachment and delusions, jealousy and
being so on, that we are able to see this process in action.
We should be able to see the nature of reality in terms of cause and effect that when
this arises, that will follow.
When this doesn't arise, if you're able to somehow give up the cause, give up the
behavior that is causing the problem, then the problem has no possible way of arising.
So it says that reality is really functions both physically and mentally on a scientific
basis, that nothing arises by chance or by magic or by any supernatural means, that
there is no possible way for any phenomenon to arise without its cause, without the appropriate
cause.
And so in this way, once we understand the causes, we understand the relationship between
phenomena, between suffering and unwholesome states and between happiness and wholesome
states, between those states that lead to suffering and the suffering and between those
states that lead to happiness and the happiness, then our mind will naturally incline
towards the development of those deans and those mind states that lead to happiness because
there's no one in the world that wants to suffer.
The problem is not that we want to hurt ourselves, intrinsically all beings are always looking
for that which is pleasant or that which is peaceful, that which is true happiness.
The problem is that we don't understand the nature of cause and effect, we do certain things
and we create certain mind states thinking that this is going to somehow lead to our benefit
when in fact, those things that we create, those states of mind and those actions are
only for our detriment, our cause for our suffering.
And so in simply by not understanding, by this ignorance and delusion, we create states
that are contradictory to our purpose, where we want to be happy and we're actually
causing ourselves suffering.
And this is how the Buddha's teaching on dependent origination starts.
The first part of the teaching is that ignorance creates formations.
It is due to ignorance that our mind gives rise to its mental formations or gives rise
to concepts, it conceives things, it gives rise to ideas and all sorts of intentions and
volitions and the very root of karma of all of our ethical action, both ethical and
unethical, simply due to our ignorance.
If we understood that these formations, these conceptions, these intentions, we're going to
cause our suffering, we wouldn't give rise to them.
If we didn't have our ignorance, if we understood that there was no way to find happiness
in this way that even creating stable lifestyles where we have a nice car and a house and
a job and all these things, if we understood that this wasn't able to bring us happiness,
even these good things, then we wouldn't strive for them.
The intention wouldn't arise to find these things, to cling, to attach even to people
or to places or to things.
If we understood reality, we wouldn't cling to these things.
We wouldn't even give rise to those ethical acts that were for the benefit of giving
or creating pleasant circumstances where we had good friends and good food and good society
and so on.
We wouldn't even think to give rise to those intentions.
We would be content and at peace with ourselves, we would have no need for anything.
We wouldn't strive if we understood that striving, this giving rise to intention or the
desire to create, the desire to destroy, the desire to change, to force, to build up some
concept of me, of identity in the world.
If we understood that this was suffering, we wouldn't give rise to it.
So this is incredibly powerful teaching, because normally we would think that without
this intention nothing happens, without this intention no good can come from your life.
But really the ultimate truth of reality is that it is what it is.
It arises and its reality comes and goes.
And there is no one thing in the world that can truly make you happy in that peace.
There is nothing that you can create that won't be destroyed.
There is nothing that you can build up that won't fall apart.
There is nothing that can truly make you happy or satisfied.
If you can't find happiness in peace as you are, as things are in all of reality, then
you'll inevitably fall into suffering and disappointment when things change and inevitably
go against your wishes.
So once we give rise to understanding, if we understand reality, the point is that we will
never have any wishes or any hopes or any desires, because we are truly happy.
We will never want for anything.
We will never hope for anything or wish for anything, because we are content the way things
are.
We are happy with reality as it is.
And this is really the key not to create anything that is inevitably going to fall apart
or disappear, but to be content with whatever comes, whatever should arise.
So this is the key principle in Buddhism that understanding sets you free.
It's not attaining anything, it's not vowing for anything, it's not creating anything.
Simply understanding things as they are will set you free.
And this is why insight meditation or meditation where one contemplates reality is so crucial.
Simply by watching reality, you change your whole way of looking and you change your whole
way of being and you change your whole universe so that nothing can bring you suffering.
This is the key.
Many people begin to practice meditation thinking that they are going to attain or create
or perceive or experience something special that is not going to fall apart, that is not
going to disappear, that is somehow going to make them satisfied.
And as a result, they are dissatisfied with the meditation when they realize that there's
nothing that is going to satisfy them.
And this is an important point for us to understand that the meditation is not for building
up, it's for letting go, for giving up and for accepting and being at peace and at harmony
with things as they are, just simply understanding.
So when we practice meditation, say watching the breath, the stomach when it rises and falls
or watching our feet move when we walk or watching any part of reality, simply being
aware that we're standing or sitting, being aware of pains and aches in the body, as I've
said before, when you feel pain, say, pain, pain, simply seeing it for what it is.
You accomplish the goal of the Buddha's teaching.
You don't have to create anything, you don't have to change anything, you don't have
to get rid of the pains and the aches, you don't have to get rid of the thoughts and
the mind.
But simply saying to yourself, it is what it is.
This is pain, this is thought, this is emotion, this is movements of the body when we're
walking, saying walking, walking, those, or so on.
Simply reminding yourself and creating this clear awareness of the phenomenon as it is is
what it freezes from suffering.
So this is the core of the Buddha's teaching on dependent origination that ignorance leads
to formations.
It's actually a good summary of the whole of the dependent origination because that's
really how it works.
Our ignorance gives rise to formation and those formations in turn, we're ignorant
about them thinking that somehow they're going to satisfy us and so we give rise to
more and more of these formations again and again.
But the next part of the cycle goes into great detail about how this works because those
formations are what give rise to our lives, they give rise to our birth, our becoming.
Just based on this, that when we pass away from one life, we create a new life, we cling
and we develop this existence that we see in front of us, that we have a brain and we
have a body where we have a world around us, we create this again and again and again.
And this gives rise to consciousness.
So the Buddha said, these formations give rise to our conscious awareness in this life.
This is the next link.
So when we're first born there arises a consciousness say in the womb of our mother and
from that moment on, we will see the cause and effect arising.
This is the detailed exposition.
So the first part is just talking about life after life after life where ignorance gives
rise to our intentions to do this or that.
Now when you get into a single life, then we start with the first moment that has been
created, this consciousness.
And through our whole life, it's going to be this consciousness that is most important.
So in the Buddha's teaching, the mind is the most important aspect and I think it's
easy to understand based on talking about how our misunderstandings are what creates suffering.
So when we understand how ignorance creates our intentions, we can understand how important
the mind is.
So this is the beginning.
How does it work?
The mind, the consciousness gives rise to our experience of reality.
We experience the physical and we experience the mental.
So it gives rise to this psychophysical or mental physical reality.
And since the experience of the stomach, when we're breathing, when the stomach rises and
the stomach falls, there's the physical and the mental.
The rising is the physical and the knowing of the rising is the mental.
When we walk, there's the foot moving is the physical.
The mind knowing it is the mental.
When we feel pain, there's the physical experience and there's the mind that knows it and
doesn't like it and so on and says that's painful.
And so on, the whole of our lives thus circle around in these with these two realities,
the mind and the experience of the objects around us.
Now this in and of itself isn't really a problem, obviously we have to experience, we have
to live our lives and there's no suffering that comes from this.
The suffering doesn't come from the things that we experience.
The suffering, as I said, comes from our misunderstanding.
Once we have the psychophysical matrix, the physical and the mental aspects of reality,
the experiences of the body and the mind, then we get the six senses.
So the next link is once you have the physical and the mental, then you're going to have
sensations.
You're going to have the seeing, the hearings, smelling, tasting, feeling, thinking.
So this is where the physical and the mental arise, they arise at the eye, the eye itself
is physical, the light touching the eye is physical, the mind that knows the seeing is
mental, hearing the sound in the ear or physical and the knowing of the sound or the hearing
is mental and so on.
So the six senses are our experience of reality.
So here we have through, we have the consciousness, which is aware of the physical and the
mental realities at the six senses.
So knowing the mental is in this case, knowing the mind.
So the mind is aware of the thoughts, it's aware of the aspects of the mind, the formations
that arise in the mind.
And once you have the six senses, then you have contact.
Now these four together make up the neutral aspects, sorry, these four along with the
next one, which is feeling or sensation.
So once you have consciousness, then you will have the mental and physical experience
at the six senses because of the contact, the contact between the mind and the physical.
And the mind goes out to the body when the mind goes out to the eye, the ear, the nose,
the tongue or when the mind goes out to the thoughts, then there is contact.
Once there is contact, then there arises sensation, there arises pleasant sensation,
an unpleasant sensation or a neutral sensation.
These five aspects of reality are the neutral aspect of reality.
Our whole life would be fine if this is all we had and thus these five are the most important
aspects of reality for us to examine and to analyze.
Once we come to understand these clearly, to see them for what they are and to not go
the next step, which is going to get us into trouble.
Simply knowing when we feel pain, knowing when we feel happy, knowing when we see, knowing
when we hear, when we see something, we know it as seeing.
So we say to ourselves, seeing, seeing, when we hear something, we know it as hearing,
we say to ourselves, hearing, hearing.
When we feel pain, we know it as pain, we say to ourselves, pain, pain.
Just reminding ourselves, it's just simply pain without attaching to it.
No problem would arise for us if we were able to do this.
The problem is that once we, if we're unable to understand reality in this way, we're
going to give rise to craving.
Because of the ignorance that exists in our minds, our inability to see these things as
they are and to see them simply as arising and ceasing reality, then we're going to give
rise to craving.
We will have this intention arise.
When we see something, we'll think, we'll conceive of it as good or we'll conceive of
it as bad.
We will have some kind of desire, some kind of intention arise.
When we hear sounds, we will recognize it as a pleasant sound or an unpleasant sound,
some a person that we like, if it's a music, we will enjoy it and so on.
We'll give rise to some intention in regards to it.
This is how all of addiction works.
For people who suffer from addiction to substances or addiction to any kind of stimulus,
we'll find that the mind goes back and forth between these different parts of reality,
the cause and effect.
Sometimes you'll be aware of the seeing, sometimes you'll be aware of the craving, sometimes
you'll be aware of the mind and knowing of it and the experience of it.
When you practice meditation, you'll be able to break this into pieces.
The mind goes through these again and again and again and it creates more and more
craving.
When you're able to understand them, you're able to break them up.
When you see something, it's simply seeing, it's neither good nor bad because there's
nothing intrinsically good or bad about seeing after all.
It's simply because of our misunderstanding, our ignorance.
And because of our past habits, our habitual ways of experience, experiencing things,
remembering that certain sites are going to be a cause for pleasure, remembering that
certain sounds or a cause for pleasure and so on and remembering that certain sites and
sounds and experiences, our cause for displeasure will give rise to our craving, our
wanting to develop, to cultivate, to increase, to attain, to obtain the phenomenon and
more of the phenomenon and our aversion, our desire to be free, to remove, to destroy,
to banish the phenomenon in the case of those things that habitually we remember as bringing
suffering, bringing displeasure or bringing pain.
Because of our conception, conceiving things as more than simply what they are, for instance,
when we hear someone's voice, right away we're going to put a value judgment on it,
whether they're a good person or a bad person, we like them or we don't like them a friend
or an enemy. When we see someone, we're right away going to get angry and upset because
we don't like them or we're going to become attracted and pleased because we do like them
maybe the beautiful or so on.
We're going to give rise to these cravings.
And this is what's going to get us into trouble because this is how addiction works.
If addiction didn't cause trouble, people wouldn't have to look for a way out of it.
But the truth of reality is that our cravings lead us to a cycle of addiction that is very
difficult and more and more difficult the longer it lasts to break free from.
So this is the next link that craving wouldn't be a problem if it didn't lead to the
next cycle and that is addiction.
Most people who haven't studied the Buddhist teaching, whether they be Buddhists or non-Buddists,
don't see the danger inherent in craving, don't see the inherent danger in liking.
We think that our likes and dislikes are what make us who we are.
And this is exactly the problem because we have some conception of self, of eye, of what
eye like and dislike.
We stumble right over this egregious error of judgment that actually there is no eye
that we're creating this as we go along.
We'll say this.
It's our way of justifying liking and disliking.
We say, I like this, I like that.
And we say this as a justification.
It's the connection, if you will, between our craving and our addiction.
At the time when we crave something, we think, yes, that's something I like.
This is my preference.
Then we'll give rise to this habitual addiction where we cling to the object, we're unable
to let go of it.
We're unable to be without it.
To the extent we'll actually cause great suffering for ourselves if we don't get it.
And this is the danger inherent in simple liking because we are not static creatures.
We are dynamic.
And everything we do and think affects who we are, changes who we are.
Craving gives rise to clinging.
You can't stop it simply by force of will.
You can't simply wish for it to stay simply as liking, where liking will not give rise
to craving.
Liking has, as it's nature, the propensity to give rise to clinging.
Gives rise to creating or becoming, creating really, creating circumstances wherein we can
get what we want, cultivating and developing and building.
Building up, in many cases, a huge ego or identification with reality, where we have who
I am, my status in life, my stable reality, my home, my car, my family, and so on.
We build this up based on our cravings, based on our clings.
We will build up our whole reality.
Why people, why beings are so diversified, why some people are rich, some people are poor,
is totally based on where we have led ourselves from lifetime after lifetime, where we've
directed our minds and what we've clung to, what we've associated and identified with.
And it's because of this becoming that we have all of our suffering, all of our dissatisfaction,
because it's this creating, this conceiving, this seeking after that gives rise to old
age sickness and death, that gives rise to this form-formed existence of being a human,
of being an animal, of being any type of creature that we actually create for ourselves,
that gives rise to our sicknesses, gives rise to our pains, gives rise to our conflicts
and our sufferings, the war and famine and poverty and so on, that exists in the world.
It's all created simply by our erroneous identification and the idea that somehow we
can find true happiness in this way.
This is the core of the Buddhist teaching.
It's really what the Buddha realized on the night that he became enlightened, that reality
does work in terms of cause and effect.
It wasn't a theoretical realization.
He saw reality working.
He saw that it's because of our ignorance, that we give rise to intention, even the intention
to help ourselves, to do good things for ourselves, to create a life that is supposedly
going to make us happy, even giving rise to this intention is due to ignorance.
Because if we understood, as I said, reality for what it was, we wouldn't see any need
to create anything, we would be content and comfortable and happy with things as they are.
We wouldn't give rise to this craving that comes from experience.
Our experience of reality would simply be the conscious experience of the physical and
mental at the six senses.
It would stop at the contact and the sensation.
We would feel happy, we would feel pain, we would feel calm, but we wouldn't attach to
any of these feelings as good or bad.
Anything that we saw would simply be what it is.
We would see it clearly as it is for what it is and not put any value judgment on it
other than what was immediately apparent as being what it was, as being a risen phenomenon
that comes about and ceases.
We would live our lives in the way that many of us truly think that we are living our lives
already.
We think that we are here living our lives in a very ordinary way and we think that we
in general experience, great peace and happiness.
But as a result of our craving and our clinging, we actually create great suffering and
we're not really living in true reality.
Our experience of reality is, as they say, tangential, we experience reality for a moment
and then more often on a conception, the idea of it being a good or a bad thing that
we experience.
When we see someone, something, we hear something, smell, taste immediately, it takes us
away into our craving, craving, leading to clinging, clinging, leading to this, becoming or
creating of some kind of intention to attain or to be free from and so on, which in turn
gives rise to conflict and suffering and not getting what we want and getting what we
don't want and being dissatisfied and disappointed and sorrow, lamentation and despair.
All of these things come from our attachments, our judgments, our ideas that things are
good and bad.
So this is an incredibly important teaching, as I said, and incredibly important for
meditators.
It's really the core of the meditation practice and the core theory that we should understand
when we start to practice meditation because this is how we're going to change our minds
during the meditation practice.
So thank you for tuning in, I hope this has been useful in that those of you who are watching
are able to put this into practice.
Please don't be satisfied simply by intellectual understanding of the Buddha's teaching.
Do take the time and effort to put this into practice.
And I wish for this teaching to be a benefit to all of you and that you are able to
through this teaching and through your practice of the Buddha's teaching to find true
peace, happiness and freedom from suffering.
Thank you and have a good day.
