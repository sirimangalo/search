1
00:00:00,000 --> 00:00:04,000
Hello and welcome back to our study of the Dhamapada.

2
00:00:04,000 --> 00:00:11,000
Today we continue on with verse number 121, which reads as follows.

3
00:00:34,000 --> 00:00:56,000
Which means one should not look down and should not think little of evil.

4
00:00:56,000 --> 00:01:11,000
This will not come to me.

5
00:01:11,000 --> 00:01:22,000
Not look down upon not think little of evil deeds thinking evil results won't come of them.

6
00:01:22,000 --> 00:01:28,000
I am so in the case of just a small evil deed at this point.

7
00:01:28,000 --> 00:01:37,000
Because uda bin dunipatina uda kumbo people are dead.

8
00:01:37,000 --> 00:01:45,000
Raindrops or water falling one drop at a time.

9
00:01:45,000 --> 00:01:55,000
Even one drop even drops of water are able to fill a water pot I think.

10
00:01:55,000 --> 00:02:04,000
Are able to make a flood of water is actually.

11
00:02:04,000 --> 00:02:10,000
Uda kumbo flood of water or a water jug depending on you ask.

12
00:02:10,000 --> 00:02:17,000
Puerati fills the water jug probably.

13
00:02:17,000 --> 00:02:27,000
In the same way balo Puerati papasa the fool becomes full of evil.

14
00:02:27,000 --> 00:02:38,000
Tokang tokang be a tinang even though be it is gathered a tinang tokang tokang.

15
00:02:38,000 --> 00:02:45,000
I always remember this tokang tokang tokang tokang means a bit or a small amount.

16
00:02:45,000 --> 00:02:54,000
So tokang tokang means little by little.

17
00:02:54,000 --> 00:02:59,000
Little by little one becomes full of evil.

18
00:02:59,000 --> 00:03:10,000
So this was taught in regards to a certain equal monk whose name isn't given.

19
00:03:10,000 --> 00:03:17,000
But it was a monk who failed to keep who did something actually quite a minor.

20
00:03:17,000 --> 00:03:25,000
And the act itself wasn't necessarily, well wasn't terribly evil.

21
00:03:25,000 --> 00:03:32,000
You might even say that the act itself was negligible.

22
00:03:32,000 --> 00:03:34,000
Not something certainly to make a story about.

23
00:03:34,000 --> 00:03:38,000
But it's an interesting story, it's very short.

24
00:03:38,000 --> 00:03:44,000
So what happened was he was not, he didn't take care of his requisite.

25
00:03:44,000 --> 00:03:50,000
He didn't take care of the furniture and the bedding and his belongings in general.

26
00:03:50,000 --> 00:03:55,000
So sometimes you'd have to bring a chair outside and then he'd leave it outside.

27
00:03:55,000 --> 00:04:00,000
Or he'd have his bedding outside to air it out.

28
00:04:00,000 --> 00:04:02,000
His sheets or mattress or something.

29
00:04:02,000 --> 00:04:09,000
And then he'd leave them outside and they'd get rained on or he would leave things unattended

30
00:04:09,000 --> 00:04:14,000
or uncared for and the mice would eat them or the ants would eat them or so on.

31
00:04:14,000 --> 00:04:18,000
And so he went through a lot of stuff and a lot of the belongings not just of his,

32
00:04:18,000 --> 00:04:23,000
but of the sangha of the monastery were ruined as it was found.

33
00:04:23,000 --> 00:04:27,000
So again, it's just one of those things that shouldn't be done.

34
00:04:27,000 --> 00:04:30,000
But it's not like he's going to hell for it.

35
00:04:30,000 --> 00:04:31,000
So the monks admonished him.

36
00:04:31,000 --> 00:04:33,000
They said, you know, look, you shouldn't do this.

37
00:04:33,000 --> 00:04:40,000
Or they asked him, you shouldn't, don't you think you should put your stuff away and take care of it?

38
00:04:40,000 --> 00:04:43,000
And he would say to them, you know, it's just a trifling.

39
00:04:43,000 --> 00:04:44,000
It's not a big deal.

40
00:04:44,000 --> 00:04:45,000
It really is.

41
00:04:45,000 --> 00:04:48,000
It's not even really worth worrying about.

42
00:04:48,000 --> 00:04:55,000
We got more important things like meditation and study and thought.

43
00:04:55,000 --> 00:05:00,000
And so he would do the same thing again and again, never learning, never changing.

44
00:05:00,000 --> 00:05:04,000
And so the monks kind of got fed up with this and they went to the Buddha and they said,

45
00:05:04,000 --> 00:05:06,000
you know, look what's going on.

46
00:05:06,000 --> 00:05:11,000
And so the teacher sent the Buddha sent for him and said, is it true that you're acting in this way?

47
00:05:11,000 --> 00:05:14,000
Is it true that you don't put your belongings away?

48
00:05:14,000 --> 00:05:19,000
And when admonished that you don't stop, you keep doing it.

49
00:05:19,000 --> 00:05:22,000
And so even to the Buddha, he said the same thing.

50
00:05:22,000 --> 00:05:25,000
He said it's such a, it's such a small thing.

51
00:05:25,000 --> 00:05:30,000
And only done, you know, it's wrong, but it's such a small, wrong, why worry about it?

52
00:05:30,000 --> 00:05:35,000
It's not worth worrying about.

53
00:05:35,000 --> 00:05:39,000
And the Buddha took this as an opportunity to teach.

54
00:05:39,000 --> 00:05:47,000
So he was concerned, not, not exactly with the act, although he did end up making a rule against leaving stuff out.

55
00:05:47,000 --> 00:05:51,000
But he was very, he seems concerned with the attitude.

56
00:05:51,000 --> 00:05:59,000
This attitude of, of making little of your faults or dismissing small faults.

57
00:05:59,000 --> 00:06:07,000
Which is interesting because there's this idea of being easy going and don't sweat the small stuff.

58
00:06:07,000 --> 00:06:10,000
And there's something to that, certainly.

59
00:06:10,000 --> 00:06:14,000
You shouldn't make things bigger than they actually are.

60
00:06:14,000 --> 00:06:19,000
But you can't leave, it's like you can't leave an infection unattended.

61
00:06:19,000 --> 00:06:24,000
As far as evil goes, there's no such thing as little.

62
00:06:24,000 --> 00:06:28,000
And there's another quote in here that I think we've skipped over.

63
00:06:28,000 --> 00:06:34,000
Comes from another one and I was trying to remember it's in regards to evil.

64
00:06:34,000 --> 00:06:36,000
Appakanti, nava manita bhanu.

65
00:06:36,000 --> 00:06:40,000
It is not proper to say, appakang, it's just a little.

66
00:06:40,000 --> 00:06:44,000
You should never look upon an evil deed as it's too little.

67
00:06:44,000 --> 00:06:49,000
The deed itself, I don't think is terribly an evil thing.

68
00:06:49,000 --> 00:06:52,000
It's negligent to not look after your belongings.

69
00:06:52,000 --> 00:06:54,000
But the attitude is much more important.

70
00:06:54,000 --> 00:07:00,000
And the import of this verse is, of course, far beyond leaving stuff out and so on.

71
00:07:00,000 --> 00:07:05,000
It's about the attitude of making light of something that is negligent.

72
00:07:05,000 --> 00:07:11,000
You might have anything that is evil.

73
00:07:11,000 --> 00:07:18,000
And this goes back to what we were talking about in the past couple of verses and even earlier verses.

74
00:07:18,000 --> 00:07:29,000
Like this monk who did whatever he wanted, he did and it was sexually active and so on.

75
00:07:29,000 --> 00:07:33,000
And it seemed pleasant.

76
00:07:33,000 --> 00:07:38,000
When he wasn't doing what he wanted, he was miserable.

77
00:07:38,000 --> 00:07:41,000
And he was suffering physically.

78
00:07:41,000 --> 00:07:44,000
And so then when he started doing whatever he wanted, he actually flourished.

79
00:07:44,000 --> 00:07:48,000
And prosperity was healthier and happier and so on.

80
00:07:48,000 --> 00:07:49,000
So it looked good.

81
00:07:49,000 --> 00:07:53,000
It was like, well, that's great.

82
00:07:53,000 --> 00:07:57,000
I think meditators, when they come here, they often fall into this kind of doubt.

83
00:07:57,000 --> 00:07:59,000
They think, you know, what am I doing here again?

84
00:07:59,000 --> 00:08:02,000
If I want to stop the suffering, we're talking about ending suffering.

85
00:08:02,000 --> 00:08:04,000
Why don't I just go home?

86
00:08:04,000 --> 00:08:07,000
Then there would be no suffering.

87
00:08:07,000 --> 00:08:13,000
It's actually quite remarkable that we come to meditate at all, especially with all the wonderful things out there,

88
00:08:13,000 --> 00:08:21,000
wonderful ways of finding happiness, finding pleasure anyway.

89
00:08:21,000 --> 00:08:29,000
But for some of us, for those of us who are perhaps more introspective and maybe more observant,

90
00:08:29,000 --> 00:08:33,000
they come to see that it's not actually satisfaction.

91
00:08:33,000 --> 00:08:35,000
It's not actually happiness.

92
00:08:35,000 --> 00:08:41,000
It's concerning as you watch yourself filling yourself up, not with happiness,

93
00:08:41,000 --> 00:08:47,000
but with desire, with greed, with attachment, and ultimately with evil.

94
00:08:47,000 --> 00:08:50,000
Evil meaning those things that cause you suffering.

95
00:08:50,000 --> 00:08:54,000
So you become more addicted and that's the addiction is pretty evil.

96
00:08:54,000 --> 00:08:58,000
I mean, in a sense, it brings suffering to you.

97
00:08:58,000 --> 00:09:04,000
And also anger and frustration and boredom and conflict.

98
00:09:04,000 --> 00:09:09,000
So just conflict whenever you can't get what you want or someone stands in the way of you getting what you want.

99
00:09:09,000 --> 00:09:20,000
If you have enough desire, you will create great conflict and adversity.

100
00:09:20,000 --> 00:09:24,000
Even go to war over greed in this world.

101
00:09:24,000 --> 00:09:35,000
Many wars are just over simple greed.

102
00:09:35,000 --> 00:09:40,000
And so it takes real introspection to get there.

103
00:09:40,000 --> 00:09:44,000
But that is the underlying truth, is that the evil doesn't go away.

104
00:09:44,000 --> 00:09:47,000
If you become, if you cultivate addiction, you become more addicted.

105
00:09:47,000 --> 00:09:52,000
If you cultivate anger and aversion, you become more set in those ways.

106
00:09:52,000 --> 00:09:54,000
It's how habits are formed.

107
00:09:54,000 --> 00:09:56,000
It's how we become who we are.

108
00:09:56,000 --> 00:10:06,000
We come to meditate and we think that we're surprised at how much we think and how much anger and aversion

109
00:10:06,000 --> 00:10:11,000
and how much greed and attachment and all these things that are inside it.

110
00:10:11,000 --> 00:10:13,000
Where did they all come from, we think?

111
00:10:13,000 --> 00:10:18,000
This is like you have water dripping into the pot and then you look down and you say,

112
00:10:18,000 --> 00:10:25,000
oh, the pot's quite full and you don't know how did it get full, but actually the water was dripping down.

113
00:10:25,000 --> 00:10:31,000
Mind is the same when you open it up and look and you actually take a look.

114
00:10:31,000 --> 00:10:34,000
That's when you see what you're doing to yourself.

115
00:10:34,000 --> 00:10:42,000
From most part, human beings are protected from the results on a large scale.

116
00:10:42,000 --> 00:10:51,000
For the most part, we can avoid the suffering that comes from not getting what we want or getting what we don't want.

117
00:10:51,000 --> 00:10:58,000
We're able to, because we live in a paradise, many of us, not all of us, of course.

118
00:10:58,000 --> 00:11:04,000
As relative to other animals, for sure we live in many of us live in quite a paradise.

119
00:11:04,000 --> 00:11:06,000
Always able to get food every day.

120
00:11:06,000 --> 00:11:14,000
It's quite remarkable. There's no reason why we should be able to get enough food to eat, but we do.

121
00:11:14,000 --> 00:11:22,000
We get more than enough food and further than that we get pleasure and entertainment and all of these things.

122
00:11:22,000 --> 00:11:29,000
We aren't able to see what the result is of addiction, what the result is of aversion.

123
00:11:29,000 --> 00:11:32,000
When we don't like something, we have ways of removing it.

124
00:11:32,000 --> 00:11:38,000
You have pests in your home, you have pesticides.

125
00:11:38,000 --> 00:11:42,000
We have many ways to get rid of the problems.

126
00:11:42,000 --> 00:11:47,000
You have a headache, take a pill, you have a backache, get a massage.

127
00:11:47,000 --> 00:11:51,000
You feel bored, there's lots of things to entertain you.

128
00:11:51,000 --> 00:11:56,000
And so little by little we fill up our pot. Not with good, but with evil.

129
00:11:56,000 --> 00:12:02,000
And the next verse, if you could guess, the next verse is going to be about doing good.

130
00:12:02,000 --> 00:12:06,000
You shouldn't, well we'll talk about that one next time.

131
00:12:06,000 --> 00:12:11,000
But what I wanted to say especially is meditation changes all that.

132
00:12:11,000 --> 00:12:15,000
Meditation allows you to see.

133
00:12:15,000 --> 00:12:19,000
And this is really how we understand karma in Buddhism.

134
00:12:19,000 --> 00:12:23,000
So people talk about karma as being kind of a belief and it's really not.

135
00:12:23,000 --> 00:12:28,000
Most people can, I have to believe it because they aren't able to see it.

136
00:12:28,000 --> 00:12:31,000
But karma is something that you have to see on the momentary level.

137
00:12:31,000 --> 00:12:37,000
A person who practices insight meditation is able to understand karma, is able to see it.

138
00:12:37,000 --> 00:12:39,000
Because you are able to see moment to moment.

139
00:12:39,000 --> 00:12:44,000
When I cling to things, it just leads to stress, it leads to suffering, it leads to busyness.

140
00:12:44,000 --> 00:12:48,000
It doesn't make me more content, more satisfied, more happy.

141
00:12:48,000 --> 00:12:53,000
When I get angry, the same thing, anger is just painful and unpleasant causes suffering.

142
00:12:53,000 --> 00:12:59,000
On the other hand, when I cultivate mindfulness, when I cultivate clarity, in that moment,

143
00:12:59,000 --> 00:13:04,000
it's sending out goodness. It's rippling out the effects of it.

144
00:13:04,000 --> 00:13:07,000
You can see. You don't know where it's going.

145
00:13:07,000 --> 00:13:12,000
Just like evil. When you become attached, it goes into the mix in your brain

146
00:13:12,000 --> 00:13:15,000
and it affects your brain and your body. You don't really see where it's going.

147
00:13:15,000 --> 00:13:19,000
Then to later, when it gets full and then it overflows and then you see the result.

148
00:13:19,000 --> 00:13:23,000
But when you meditate, you can see the source. You can see what you're sending out.

149
00:13:23,000 --> 00:13:30,000
You get to see how your reactions are affecting your brain or affecting your body or affecting the world around you,

150
00:13:30,000 --> 00:13:33,000
the people around you and everything you say and you do.

151
00:13:33,000 --> 00:13:35,000
When you're mindful, you can see all this.

152
00:13:35,000 --> 00:13:41,000
It's a great thing about meditation. It's one of the first things you learn in the meditation practice.

153
00:13:41,000 --> 00:13:46,000
Not intellectually, but you see it. You see how the mind works.

154
00:13:46,000 --> 00:13:50,000
You see how karma works.

155
00:13:50,000 --> 00:13:57,000
You lose doubt. You lose this doubt about good and evil and right and wrong.

156
00:13:57,000 --> 00:14:05,000
You can understand that there is a result. There are consequences to our action.

157
00:14:05,000 --> 00:14:18,000
This is an important fermentation. It's an important encouragement, I think, especially for those of us who are meditating to verify this and to say to ourselves,

158
00:14:18,000 --> 00:14:21,000
this is a great thing I'm doing that I'm able to see this.

159
00:14:21,000 --> 00:14:34,000
That I can adjust and that I can change my habits so that I feel my product not with addiction and aversion but with mindfulness and wisdom.

160
00:14:34,000 --> 00:14:41,000
So, that's the Dhamapada short story. Wonderful verse.

161
00:14:41,000 --> 00:15:05,000
Thank you all for tuning in, wishing you all the best.

