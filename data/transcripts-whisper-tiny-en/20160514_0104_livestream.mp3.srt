1
00:00:00,000 --> 00:00:24,800
Good evening, everyone, broadcasting live Friday the 13th, Friday May 13th.

2
00:00:24,800 --> 00:00:45,520
Day is quote, there's about good things, good things that are not good.

3
00:00:45,520 --> 00:00:52,320
There are things in this world that we think are good, that they turn out to be dangerous,

4
00:00:52,320 --> 00:01:10,800
dar uno, dar uno, dar uno means wicked, harsh, cruel.

5
00:01:10,800 --> 00:01:22,160
What is it in this world that is harsh and cruel, dangerous?

6
00:01:22,160 --> 00:01:49,600
What is it, three things, lumba, gained sakara, praise, and see the siloka, fame, sakara is

7
00:01:49,600 --> 00:02:02,280
honor, when we went through this, sakara is when people hold the esteem you, siloka is

8
00:02:02,280 --> 00:02:12,120
fame, lava is gained, these things are very bad, no?

9
00:02:12,120 --> 00:02:19,680
What are the sort of things you think of as bad, problematic?

10
00:02:19,680 --> 00:02:24,080
Why are these considered to be bad?

11
00:02:24,080 --> 00:02:51,800
Some people, I can see in their mind, they would not tell a lie, they would not knowing

12
00:02:51,800 --> 00:03:06,800
they would not tell a lie for gold or jewels, golden ball, golden ball filled with pieces

13
00:03:06,800 --> 00:03:20,960
of silver, well maybe, right?

14
00:03:20,960 --> 00:03:32,240
There are people who would not normally do evil things, they don't think they're not

15
00:03:32,240 --> 00:03:41,560
actually pure, this is the way of worldly goodness, you see people who are just naturally

16
00:03:41,560 --> 00:03:48,000
wholesome and good and kind.

17
00:03:48,000 --> 00:03:55,400
And then he says, by that same person I've seen telling lies because his heart was obsessed

18
00:03:55,400 --> 00:04:03,320
by gains honor and fame, though having been corrupted and they're like asops, fables

19
00:04:03,320 --> 00:04:16,280
about that sort of thing, someone becomes the classic story or someone loses sight, loses

20
00:04:16,280 --> 00:04:25,600
sight of what's good, becomes blinded by gains honor and fame.

21
00:04:25,600 --> 00:04:33,240
Good things blind us, there's a danger in too much, undeserved the Buddha often talked

22
00:04:33,240 --> 00:04:48,240
about undeserved and someone isn't capable, or isn't ready, or isn't worthy of gain,

23
00:04:48,240 --> 00:05:01,240
praise, fame, or anything else good, it's another reason why nature is such a useful environment

24
00:05:01,240 --> 00:05:17,840
to meditate in because it's quite harsh, it forces you to be patient and present, it's

25
00:05:17,840 --> 00:05:25,040
easy when things become, when we live in the world like in Asia when we went to meditate,

26
00:05:25,040 --> 00:05:35,080
it could be quite cold and it's a colder than it is here now, but here we turn on the

27
00:05:35,080 --> 00:05:43,600
heat when it gets below 20 degrees, when we have hot water showers, we couldn't imagine

28
00:05:43,600 --> 00:05:53,960
taking a cold water shower, it's easy to become complacent and lazy, when you actually

29
00:05:53,960 --> 00:06:09,280
live in nature, if you've ever lived in nature, it forces you to be content, to accept,

30
00:06:09,280 --> 00:06:14,920
to let go, because you can't run away and there's no, if you don't have a hot water

31
00:06:14,920 --> 00:06:18,920
shower then you have to take cold water shower, if you live in the forest you don't

32
00:06:18,920 --> 00:06:24,760
even have a hot water, a cold water shower, you have to bathe in the river or not bathe

33
00:06:24,760 --> 00:06:34,600
at all for days, so why we go on arms round, because it's a way of being content, you know,

34
00:06:34,600 --> 00:06:44,800
with whatever food there is easily and freely available through charity, bringing ourselves

35
00:06:44,800 --> 00:06:53,440
from the complacency of choosing and getting what we want all the time, so why we sleep

36
00:06:53,440 --> 00:07:01,360
on the floor, it's why we, so why we give up money and give up luxury, because these things

37
00:07:01,360 --> 00:07:10,200
are dangerous, world the good things are problematic, and this is another reason why many

38
00:07:10,200 --> 00:07:15,720
people are unable to progress in the practice, are unable to even think of meditation

39
00:07:15,720 --> 00:07:21,280
practice as a positive thing, there's many people in this world who couldn't imagine

40
00:07:21,280 --> 00:07:31,880
or reason for meditating, right, why, why would you waste your time, you could be enjoying

41
00:07:31,880 --> 00:07:38,120
life, right, because these people enjoy life, they have pleasant lives and they have

42
00:07:38,120 --> 00:07:44,560
good, they have gains that other people do not, there are people who think that all you

43
00:07:44,560 --> 00:07:49,920
have to do is think positively and good things come to you, and there's something to not

44
00:07:49,920 --> 00:07:56,160
being depressed and negative, but it's also quite clear that some people just have better

45
00:07:56,160 --> 00:08:08,040
luck and better situation than others, and they become blind and forgetful, you know, there's

46
00:08:08,040 --> 00:08:13,480
the story of Sakka, the King of the Gods, and became a Sotapana, so he really understood

47
00:08:13,480 --> 00:08:20,520
the Dhamma, but then one day Mughalana went to check on him and found that he was complacent,

48
00:08:20,520 --> 00:08:26,120
he had this beautiful palace, and he showed Mughalana this palace, and Mughalana used

49
00:08:26,120 --> 00:08:36,080
his magical powers to shake the palace up with an earthquake, and Sakka became afraid

50
00:08:36,080 --> 00:08:49,840
and he said, you've become complacent, this gain of yours, it's an angel, this is good

51
00:08:49,840 --> 00:09:06,160
things, we're like angels in many ways, we have such luxury in this world, we just have

52
00:09:06,160 --> 00:09:13,520
cooked food, you know, all the things that we have that we take for granted, cooked

53
00:09:13,520 --> 00:09:19,880
food, soft clothes, that we can actually wear cotton on our bodies to ward off heat and

54
00:09:19,880 --> 00:09:29,880
cold, that we have walls and a roof to keep out the insects and the elements that we

55
00:09:29,880 --> 00:09:34,680
have medicine, so we don't have to put up with our illness, we can just, whenever we're

56
00:09:34,680 --> 00:09:43,480
sick, for whatever reason we can take a pill and it goes away, very easy to get lost

57
00:09:43,480 --> 00:10:05,040
in these things and not realize the limited nature of our luxury and contentment, it's

58
00:10:05,040 --> 00:10:12,160
easy to be content when things are good, when you have happy things come to you, we practice

59
00:10:12,160 --> 00:10:21,600
meditation to be ready for anything, to be invincible, it's not a reason to be proud that

60
00:10:21,600 --> 00:10:30,560
you can deal with, that you can enjoy life, it's not a reason to be proud of when you're

61
00:10:30,560 --> 00:10:38,800
famous or when you're rich or when you're honored by others, something to be concerned

62
00:10:38,800 --> 00:10:45,360
about, now I will become complacent, these good things will make me forget about the potential

63
00:10:45,360 --> 00:10:55,840
for bad things, when the bad things come I'll be completely unprepared, very important

64
00:10:55,840 --> 00:11:10,400
and part aspect of Buddhism is our relationship with worldly good things, it's very important

65
00:11:10,400 --> 00:11:15,560
point in leading us to take the meditation practice seriously, it's our relationship with

66
00:11:15,560 --> 00:11:21,600
worldly happiness, worldly pleasure, worldly good things, the worldly dumbness and reminding

67
00:11:21,600 --> 00:11:29,080
ourselves that these are not permanent, that they're good and the bad are unpredictable

68
00:11:29,080 --> 00:11:41,520
unreliable and sustainable, okay, anyway that's our dumber for this evening, if we have

69
00:11:41,520 --> 00:11:56,400
any questions, good morning, thanks for the broken God teaching, glad you appreciated it,

70
00:11:56,400 --> 00:12:03,920
just to remind her I asked a couple of questions, well I don't see them, if they're before

71
00:12:03,920 --> 00:12:13,120
last night then I think they're lost, what should you do when other people suffering

72
00:12:13,120 --> 00:12:26,320
spills onto you, it's interesting how you say spills onto you, you might have to give

73
00:12:26,320 --> 00:12:33,600
me an example but I can talk about some of the ways it spills onto you, you know, when

74
00:12:33,600 --> 00:12:39,800
you feel sympathetic for other people suffering is one way or when people get you caught

75
00:12:39,800 --> 00:12:47,520
up in their own problems is another way, but you can't actually, suffering can't actually

76
00:12:47,520 --> 00:12:54,560
spread, suffering is one thing that can't spread, you can't suffer because someone else

77
00:12:54,560 --> 00:13:01,840
is suffering, it's very easy to suffer when someone else is suffering with these reasons,

78
00:13:01,840 --> 00:13:10,200
right, someone gets in trouble, suppose someone accumulates gambling debts or something,

79
00:13:10,200 --> 00:13:23,600
and then their whole family suffers, suppose someone is very ill, then their whole family

80
00:13:23,600 --> 00:13:31,440
suffers for another reason, from sadness, from loss, people taking problems out on you,

81
00:13:31,440 --> 00:13:39,240
right, so suppose someone is apparently when people are ill with cancer they can be very

82
00:13:39,240 --> 00:13:49,960
hard to deal with, rather than becoming sober they become more intoxicated or more caught

83
00:13:49,960 --> 00:14:01,280
up in themselves, more self-absorbed and crazy, you know, become very self-centered,

84
00:14:01,280 --> 00:14:09,000
me, me, me, poor me, something like that and it causes problems rather people or they're

85
00:14:09,000 --> 00:14:15,440
angry at you for not appreciating them, that kind of thing, but not appreciating their

86
00:14:15,440 --> 00:14:24,160
problems, and you have to deal with it as, with everything else, with mindfulness, don't

87
00:14:24,160 --> 00:14:32,360
let other people see, we're very connected with others, we react very quickly to other

88
00:14:32,360 --> 00:14:38,440
people's problems, so it's a reason why we go on what we call retreat, right, you're

89
00:14:38,440 --> 00:14:45,800
retreat, because being around people all day is tough, you know, being around worldly people

90
00:14:45,800 --> 00:14:55,280
is very hard on your mind, very difficult for your meditation practice, I mean you could

91
00:14:55,280 --> 00:15:01,160
call it the advanced practice, right, when you actually have to deal with people, meditating

92
00:15:01,160 --> 00:15:09,120
here in the monasteries, they're training, like boxing, in a boxing gym, you go to the gym

93
00:15:09,120 --> 00:15:15,680
and you train, but when you step into the ring it gets real and you have to put it all

94
00:15:15,680 --> 00:15:22,000
to use.

95
00:15:22,000 --> 00:15:28,080
Feeling sad for other people is a big problem I think, people think compassion is feeling

96
00:15:28,080 --> 00:15:34,680
sad for others, it's not really, it shouldn't be moved by other people's suffering, not

97
00:15:34,680 --> 00:15:40,480
in that way, sorry that's maybe wrong to say you shouldn't be moved, but moved to help,

98
00:15:40,480 --> 00:15:46,640
not moved to sadness, you shouldn't be upset by it's what I mean, not in that sense,

99
00:15:46,640 --> 00:15:51,360
it shouldn't be sad because of other people's suffering, and that's, people will often

100
00:15:51,360 --> 00:15:55,480
use that sort of thing to their advantage, pour me and they want everyone to feel sad

101
00:15:55,480 --> 00:15:59,400
for them and then it trains everyone else and that kind of thing, it doesn't actually

102
00:15:59,400 --> 00:16:07,520
help anyone, right, but you should help others and in fact you can help others because

103
00:16:07,520 --> 00:16:11,240
of the method, because of your contentment through the meditation practice, when you

104
00:16:11,240 --> 00:16:17,480
have fewer needs of your own, you're much better able to help others, you talk about

105
00:16:17,480 --> 00:16:25,400
other people abusing that in terms of using you and expecting you to help them, I think

106
00:16:25,400 --> 00:16:32,440
for a night and being there's a certain sense of willingness to let people use you, certain

107
00:16:32,440 --> 00:16:36,760
willingness to help others and to not worry so much about yourself because you don't

108
00:16:36,760 --> 00:16:43,160
get upset about things, you can be used, it doesn't bother you, you don't mind when

109
00:16:43,160 --> 00:16:46,880
other people want you to help them, it's just easier to go along with them so you just

110
00:16:46,880 --> 00:16:52,840
do it, and you can do things that most you're stronger, in fact invincible as an enlightened

111
00:16:52,840 --> 00:17:00,320
being, in our own, nothing bothers them, so they're able to help and work for the benefit

112
00:17:00,320 --> 00:17:13,160
of others, untirelessly, untireingly, so those are just some thoughts on that, ultimately

113
00:17:13,160 --> 00:17:19,320
you have to be mindful and it's going to depend on the situation, depend on your own

114
00:17:19,320 --> 00:17:23,520
ability is, you shouldn't pretend that you're enlightened and try to act like an hour

115
00:17:23,520 --> 00:17:31,880
hunt, you should sometimes protect yourself and sometimes you have to retreat, is eating

116
00:17:31,880 --> 00:17:39,760
meat considered killing, no it's not, eating meat is considered eating, chewing, nothing

117
00:17:39,760 --> 00:17:46,240
dies when you eat meat.

118
00:17:46,240 --> 00:17:54,360
Of all the Buddhas been men in born in India, apparently so, but not just men there, it's

119
00:17:54,360 --> 00:18:01,440
a very special being, a Buddha is born as a very special being, but yes, is male, doesn't

120
00:18:01,440 --> 00:18:06,640
mean that women are inferior, although there is that kind of a connotation, unfortunately

121
00:18:06,640 --> 00:18:10,640
I don't know what to say about that, maybe it's not true, maybe there are women Buddhas,

122
00:18:10,640 --> 00:18:16,160
but there's a sense that the Buddha form is male, it's masculine, the same goes for

123
00:18:16,160 --> 00:18:25,320
the Mara which is the evil angel, is also a male form, so it's a sense I think of the

124
00:18:25,320 --> 00:18:30,560
male being more powerful and I know that's kind of sexist or it's outright sexist, so I

125
00:18:30,560 --> 00:18:35,640
don't know what to say about that, maybe it's not true, it wouldn't put too much stock

126
00:18:35,640 --> 00:18:41,200
in it, it's not like it's an important teaching or anything, but yeah, curiously they

127
00:18:41,200 --> 00:18:47,200
seem, according to the commentaries, to have been all male born in India, and India seems

128
00:18:47,200 --> 00:19:01,320
to come again and again, each universe has an India, apparently, take that as you will.

129
00:19:01,320 --> 00:19:05,040
In your booklet under this section on sitting practice, you're right once you'd investigate

130
00:19:05,040 --> 00:19:12,280
sensations other than the breath, after some minutes, what if the mind stays easily with

131
00:19:12,280 --> 00:19:17,960
the breath, should one stick with the breath or pick other sensations, if the mind is

132
00:19:17,960 --> 00:19:22,800
with the breath and with the stomach, you focus on the stomach rising, but as soon as pain

133
00:19:22,800 --> 00:19:29,080
arises, focus on the pain, as soon as happiness arises, focus on the happiness, as soon as calm

134
00:19:29,080 --> 00:19:37,800
arises, focus on the calm, maybe the book is poorly written, I don't know, shouldn't say

135
00:19:37,800 --> 00:19:42,880
you should look for feelings, but if there's feelings you should focus on them, because

136
00:19:42,880 --> 00:19:48,120
that means your mind is already no longer on the stomach.

137
00:19:48,120 --> 00:19:56,560
Yes, he actually shook the palace using a single toe, someone who knows their texts, he

138
00:19:56,560 --> 00:20:01,840
touched it, so Sakha look, he said, look at this beautiful majestic palace and it's

139
00:20:01,840 --> 00:20:08,520
blunder and it's magnificent, it's awesomeness, and Morgan and I touched it with his big

140
00:20:08,520 --> 00:20:15,240
toe and which shook as though it was going to fall apart, and Sakha was horrified and

141
00:20:15,240 --> 00:20:20,800
later his grand palace of his was about to collapse, and Morgan and his shook his head

142
00:20:20,800 --> 00:20:26,480
and he are being negligent, this is not permanent, this is not stable, this is not awesome,

143
00:20:26,480 --> 00:20:36,840
this is nothing, it's the broken gong analogy used for people with preconceptions about

144
00:20:36,840 --> 00:20:43,280
you or a situation that happened, the broken gong is when you don't react, it means don't

145
00:20:43,280 --> 00:20:52,000
react, that's all, it's quite simple, I wouldn't read too much into it, it shouldn't

146
00:20:52,000 --> 00:20:58,520
be, choose when to say no that is wrong, because that would be right at the time, I mean

147
00:20:58,520 --> 00:21:03,000
you wouldn't use the exclamation points, I think it's the point, you wouldn't be upset

148
00:21:03,000 --> 00:21:10,920
about it, but you might say that's wrong, but the broken gong means there's something missing,

149
00:21:10,920 --> 00:21:16,120
a gong is unable to ring, it's actually kind of a negative thing, right, we think of a

150
00:21:16,120 --> 00:21:21,960
gong ringing as a good thing, but no, there's something missing from the mind of an enlightened

151
00:21:21,960 --> 00:21:37,520
being, they might say no that's wrong, but there's no upset, there's no reaction.

152
00:21:37,520 --> 00:21:42,520
What is the incentive for its continuance via factory farming and consuming it, but that's

153
00:21:42,520 --> 00:21:50,600
not immoral, that doesn't make it immoral, it may make it immoral in a intellectual sense,

154
00:21:50,600 --> 00:21:56,000
but eating meat is just chewing, chewing, chewing, nothing dies as you do it, and that's

155
00:21:56,000 --> 00:22:01,000
where morality is and Buddhism, because otherwise you can never win, you can never be truly

156
00:22:01,000 --> 00:22:06,480
moral, because you don't know whether the food you're eating is pesticides that they

157
00:22:06,480 --> 00:22:13,600
use, you don't know, driving, and you get on the bus, and you pay a bus token, well, more

158
00:22:13,600 --> 00:22:18,000
simply you get in your car and you drive, and you know your car is going to hit insects,

159
00:22:18,000 --> 00:22:21,000
right, well then you're responsible for the killing of the insects, okay, so then you

160
00:22:21,000 --> 00:22:25,120
take a bus, well, you're still paying for the bus, so aren't you contributing to the dead

161
00:22:25,120 --> 00:22:32,160
insects on the windshield, etc., etc., you can never be truly moral if your morality is based

162
00:22:32,160 --> 00:22:40,600
on an intellectual concept of this is supporting this, this is supporting that, it's not

163
00:22:40,600 --> 00:22:49,320
like that, morality is the intentional or the abstention from intentional acts of direct

164
00:22:49,320 --> 00:22:54,240
harm, so if you tell someone, you don't go kill me a chicken or something or kill me

165
00:22:54,240 --> 00:23:00,760
a fish, then you're not guilty of murder, but you're guilty of telling someone to commit

166
00:23:00,760 --> 00:23:07,840
murder, which is still a very harmful thing, it's the state of your mind, you don't want

167
00:23:07,840 --> 00:23:18,480
to be killed and yet you act in a way with the intention to bring about death, you don't

168
00:23:18,480 --> 00:23:22,720
do that when you eat, you don't have the intention to kill this thing, you also don't

169
00:23:22,720 --> 00:23:29,680
have an intention that people should go out and do it more, go out and kill more, I know

170
00:23:29,680 --> 00:23:37,600
people look at this as a yeah, but yeah, I don't overthink things, keep it simple, otherwise

171
00:23:37,600 --> 00:23:48,080
you get lost, what is the benefit of not eating afternoon, well it's just a line in the

172
00:23:48,080 --> 00:23:54,600
sand, there's nothing magical about noon, but you need a certain amount of energy for

173
00:23:54,600 --> 00:23:59,640
the day, a certain amount of calories and nutrients for the day, you use those during the

174
00:23:59,640 --> 00:24:05,720
day, so eating in the morning gives you that, it means what do I need to survive, I need

175
00:24:05,720 --> 00:24:11,040
food, if I eat in the evening it's not very useful, but if I eat in the morning that's

176
00:24:11,040 --> 00:24:16,480
helps me get through the day and I live and they have energy, so we only eat in the morning

177
00:24:16,480 --> 00:24:24,560
for that reason and the artificial line in the sand is dawn to midday, which is reasonable,

178
00:24:24,560 --> 00:24:35,520
it doesn't mean it's immoral to eat after midday, it's a reasonable convention, I mean

179
00:24:35,520 --> 00:24:41,800
a lot of the rules are just that, they're not necessary, they're just rules, and that's

180
00:24:41,800 --> 00:24:48,320
the thing is they're not thou shalt not, and this is evil, I undertake not to, so there

181
00:24:48,320 --> 00:24:57,320
are things that people undertake, they undertake them because they're useful, whoa,

182
00:24:57,320 --> 00:25:14,240
they're the big one, recently came off of medication, causes delirium, any tips on fear about

183
00:25:14,240 --> 00:25:27,520
dying incorrectly to let go, or concentrate, or observe during these overwhelming experiences.

184
00:25:27,520 --> 00:25:33,880
Well, make sure you don't have these things in these states in your mind, right?

185
00:25:33,880 --> 00:25:41,280
It's fearsome, and there's no way around it if you die with evil in your heart, you're

186
00:25:41,280 --> 00:25:53,600
going to a bad place, and on fear itself is a very unwholesome state, so it's something

187
00:25:53,600 --> 00:25:58,320
that you have to deal with, you die with fear in your mind, that's not a good thing, that's

188
00:25:58,320 --> 00:26:06,560
one of the prime reasons for going to a hellish state or a ghostly state, not a good state.

189
00:26:06,560 --> 00:26:13,200
Now ordinary fear won't send you to hell, but the intense fear can make you more afraid,

190
00:26:13,200 --> 00:26:14,200
right?

191
00:26:14,200 --> 00:26:20,600
Oh my gosh, he said, my fear is going to lead me to hell, well good, you shouldn't see

192
00:26:20,600 --> 00:26:25,120
how silly you're being in the end, sorry, I don't mean to trivialize it, but in the end

193
00:26:25,120 --> 00:26:34,000
it's just fear, we make more out of these things than they are, that's really the problem.

194
00:26:34,000 --> 00:26:40,920
If you're mindful of your fear, then it's no longer has power over you, mindfulness meditation,

195
00:26:40,920 --> 00:26:45,480
how should you do it, what is the tips, well, have you read my book then on how to meditate,

196
00:26:45,480 --> 00:26:54,880
I think you have, that's about it, read my booklet, learn how to meditate, practice that

197
00:26:54,880 --> 00:27:08,440
way, we'll help you deal with such things as fear and anxiety and so on, how long should

198
00:27:08,440 --> 00:27:14,120
you focus on your feelings, well, as long as they last is good, but if after a long time

199
00:27:14,120 --> 00:27:19,560
they don't go away then you can ignore them and come back to the stomach rising and

200
00:27:19,560 --> 00:27:26,880
falling, but you can stay with them, we're just trying to see them as they are, to see

201
00:27:26,880 --> 00:27:31,360
that they're not under our control, trying to learn to be objective, it's more about our

202
00:27:31,360 --> 00:27:36,280
state of mind than the actual experience, so whatever you focus on, trying to cultivate

203
00:27:36,280 --> 00:27:41,440
this objective experience, it doesn't matter what you focus on, it's feelings always there,

204
00:27:41,440 --> 00:27:48,920
just stay with it, good object, I have had some serious brain injuries in the past,

205
00:27:48,920 --> 00:27:56,520
would it affect my meditation, potentially, potentially not, I wouldn't worry about it, do

206
00:27:56,520 --> 00:28:04,000
it you can, as best you can, that's all you can do, we can only start where we are, we're

207
00:28:04,000 --> 00:28:09,040
not all going to become enlightened in this lifetime, but we take a step in that direction,

208
00:28:09,040 --> 00:28:17,040
it becomes our direction, we can turn ourselves in the right direction, we'll begin

209
00:28:17,040 --> 00:28:25,240
on the right path, you see so much more of it right before deaths and you realize you

210
00:28:25,240 --> 00:28:32,280
have, yeah, I don't think I've ever been in a situation where I was thinking I was going

211
00:28:32,280 --> 00:28:37,240
to die, I mean it was in a car accident once, that was kind of scary, I remember doing

212
00:28:37,240 --> 00:28:42,600
rock climbing and I got to the top, I tied myself in to be let down and halfway down

213
00:28:42,600 --> 00:28:48,280
and I realized the knot that was holding me was just a single knot, I had mist hide it,

214
00:28:48,280 --> 00:28:53,080
that was kind of freaky, it's 60 feet up, realizing I was about to slip loose and fall

215
00:28:53,080 --> 00:29:02,120
to my death, but no, I've never been in such a situation, but talking to people who

216
00:29:02,120 --> 00:29:07,320
have, they can imagine, and as a meditator you can imagine because you get these feelings,

217
00:29:07,320 --> 00:29:11,640
these things come up, that people talk about coming up when you die, that's why meditation

218
00:29:11,640 --> 00:29:18,040
and so great for preparing you for death because you've been there, seen it all, you know,

219
00:29:18,040 --> 00:29:27,120
you've gone through the dregs and the depths of your mind, have you ever read the book,

220
00:29:27,120 --> 00:29:38,600
I, the foregreens, no, I haven't, there's a way to sukha refer to Nibanda, some very good

221
00:29:38,600 --> 00:29:45,240
questions, very difficult words, I have no idea, a way to do taksukha, let's look up the word,

222
00:29:45,240 --> 00:29:51,880
I don't even know that word, it sounds like what Sariputas said, when he was asked, Yamaka,

223
00:29:51,880 --> 00:30:00,200
I think, no, the Yamaka, the monk Yamaka asked, sorry, put that, how is it that Nibanda

224
00:30:00,200 --> 00:30:06,040
can be happiness, there's no feeling, there's no feeling, how can it be happiness, right?

225
00:30:06,040 --> 00:30:13,080
Nibanda said it's precisely because there's no feeling that it's happening, so what's

226
00:30:13,080 --> 00:30:24,280
a way to die, no, I don't know, a way to die, I don't, it's not in the dictionary,

227
00:30:25,000 --> 00:30:31,400
but way to die is unf, is felt, so a way to die, I don't know, you have to give me context,

228
00:30:31,400 --> 00:30:55,320
I can search for it, it's not even in, it's not even in the, it's not even in the, the

229
00:30:55,320 --> 00:31:01,960
bitticus, the nikayas, so there, maybe it's in the commentary or something, you have to give me

230
00:31:01,960 --> 00:31:09,400
some context, way to die means experienced, a way to die means you haven't experienced it,

231
00:31:09,400 --> 00:31:19,800
so I don't think it refers to Nibanda, but it's, it's a way to nea probably, or it's without

232
00:31:19,800 --> 00:31:35,080
weed in the, you know, what are you doing, are you traveling, what are you doing, are you traveling

233
00:31:35,080 --> 00:31:40,760
against, with the robes, the robes are in Bangkok, we're going to pick them up when I get to Bangkok

234
00:31:40,760 --> 00:31:47,400
and travel to Chiang Mai with them, I have to get a ticket to Chiang Mai somehow, have we worked

235
00:31:47,400 --> 00:31:54,600
that out, it's Robin here, you have to work that out at some point, the ticket to Chiang Mai,

236
00:31:54,600 --> 00:32:00,920
maybe I'll just take a bus, a bus is so much cheaper, probably much more environmentally friendly as

237
00:32:00,920 --> 00:32:18,360
well, yeah, so I'll be leaving here May 31st, and I'll be back again July 1st, I think.

238
00:32:23,960 --> 00:32:29,960
Well, Sangha, you can search for it on the DPR, I can search for it and we can find it in the

239
00:32:29,960 --> 00:32:36,840
commentaries, maybe, way data means where does that, that's very common, where do we see that,

240
00:32:36,840 --> 00:32:47,080
way data, way data, yeah, it's very common, the word way data on this, the body uses that a lot,

241
00:32:49,640 --> 00:32:56,280
I can't think off the top of my head, one of the main suit does as the word way data, way data

242
00:32:56,280 --> 00:33:04,840
boys should be experienced that kind of thing, weekly interviews during June, probably, I'm

243
00:33:04,840 --> 00:33:10,840
going to try my best to have internet wherever I go, I guess I can't say for sure, but I'll try

244
00:33:10,840 --> 00:33:17,720
my best, I'll have to work it out with Satanya and Bangkok, I don't know if I have my SIM card here

245
00:33:17,720 --> 00:33:24,200
or if it's still working, I don't remember how that goes, I have a not a Thai number, a Thai

246
00:33:24,200 --> 00:33:30,760
cell phone number, which would give me internet there, Wi-Fi is still not that big of a thing there,

247
00:33:32,200 --> 00:33:38,600
anyway, I wouldn't have my own Wi-Fi where I'm going, a lot of people use mobile internet,

248
00:33:47,240 --> 00:33:52,680
okay enough, that was lots of questions, let's not overdo it, save some questions for tomorrow,

249
00:33:52,680 --> 00:33:58,200
tomorrow I might, tomorrow I won't be here, I don't think, I'm going to give a talk

250
00:33:59,160 --> 00:34:08,680
in Mississauga, at a funeral, or not even a funeral, some kind of memorial service which

251
00:34:10,280 --> 00:34:16,040
seems somewhat unbutous to me, I'm not really convinced that memorial services are terribly

252
00:34:16,040 --> 00:34:24,760
Buddhist, dead is dead, the person's born again, move on, I don't know, not convinced,

253
00:34:24,760 --> 00:34:30,280
it seems to me somehow clinging to the past, right, death is something that's already happened,

254
00:34:30,280 --> 00:34:37,560
you know, the person moved on, why can't you, but not something I thought deeply about,

255
00:34:39,000 --> 00:34:42,280
anyway, it's a good opportunity to teach the dumbness of whatever,

256
00:34:42,280 --> 00:34:49,400
anyway, so probably not be here tomorrow evening, but we'll see, it could be, now,

257
00:34:49,400 --> 00:34:53,720
I won't be back in time, I'm going to miss next, I'm going to miss tomorrow,

258
00:34:54,920 --> 00:34:57,960
so have a good night, wishing you all the best, be well,

259
00:34:57,960 --> 00:35:13,880
you know, we got video up here, right, turn that up.

