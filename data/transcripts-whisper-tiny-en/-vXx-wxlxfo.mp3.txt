Good evening, everyone, broadcasting live to live 5th, here to review, discuss, to unravel
the dumber. So there we have a rather odd sort of quote. It seems to me to be all that
of an essential quote. That's about associating with good people. I don't think it's quite a good
translation. Let me go about these translations. Seems more reasonable to me. There's a person
who should be associated with. We follow them to be served. Kind of a person. Kind of a person
should you follow. It starts off to start off by talking about those who should be looked upon
with disgust and not associating with. So if someone is immoral or bad character, impure of suspect
behavior, such a person one should not follow. Because even if one doesn't take their example,
this is how big a body doesn't take their example. Still people will assume the worst. You
will be branded tired when they say tired with the same brush. People will think, oh, you
must be like that person that you've fallen. Among other things. There's lots of trouble
that surrounds people who do bad things. Sometimes we remain friends and we kind of loyalty
think that we should stick with people who are in a bad way for reasons other than their
behavior. And kind of some of you agree with that. There's a certain amount of trouble
that follows from associating with corrupt individuals or immoral impure. They do bad things.
If you're a person in the end, such people should be avoided possible. There's lots of
reasons for what it gives one of the, I guess the most obvious is the bad reputation. And
it makes it hard to live your life because no one wants to be around you all of a sudden
because you know who your friends are. And there's another type of person who should
be looked upon with equanimity, but also not associated with. The first type of person
should be looked upon with disgust. She could take, she could cheat about to be shunned.
The second type of person should be seemingly equanimity, but also not associated. This
is a person who though they may not do bad things. Say bad things, they still, but they
don't do evil deeds, but they still have evil mind states. So they're prone to anger.
If they're slightly criticized, they become, they lose their temper and become irritable
hostile and stubborn. They display irritation, hatred and bitterness. Just as a festering
sore, it's struck by a sticker, a shard, with this charge even more matter so too. Or
just as a firebrand, with sizzle and crack, just as a pit of feces, if struck by a stick
will become even more foul smelling. And these sort of people, you best just leave them
alone. They're not, they're not identifiably evil in terms of giving you bad reputation
or getting you caught up in evil deeds or even worse. Encouraging you yourself to engage
in evil deeds, but there's a certain nastiness for people who get angry and for people
who are greedy. They sort of people, you should still try to keep your distance, not because
of the explicit evil, but because of the more, that's still explicit with the more inner
corruption that's going to, they poke it with a stick, it smells even worse. If you get involved
with such people, there's always conflict, there's always trouble. So the people are best
best treated like a pile of crap. It's okay as long as they don't touch it, poke it with
the stick or step in it. But in a such a person, it should be associated with followed
and served. This is what the quote says, someone is virtuous and a good character you
shouldn't hang out with. This isn't a sort of person you should stick around with.
I think it was just curious, it's just a reflection of the other one, but it's still kind
of curious that the only reason, but it is even if you don't, even if you don't follow
the example and become a good person yourself, people will give a good reputation. Oh yes,
this person has good friends. This person has good companions. I think it's a little bit
superficial to say, but there's something a little more to it than that. It's a sign of
good judgment. But I think more importantly, it's a doorway. Even if you don't become a good
person by associating with good people, there's always the opportunity. The sort of superficial
benefits of having people say good things about even being free from any of the dangers
of the troubles that come from associating with corrupt people, all that's fairly superficial.
Obviously more important is, even if you don't pick up their habits immediately, the
opportunity to pick up good habits and the potential to become a better person is of course.
It's greatly improved when you hang up with good people. So you may not be listening
to feel inferior or unworthy as on. You shouldn't seek out good people with the thought
that I will become a better person by self. Anyway, it's a fairly simple quote. Not much
to say about that. But there's so much to say about friendship, hanging out with good
people, being around good people, especially when you're doing something so personal as
meditation, so much to do with the individual. It's important to associate with individuals
for a good example. People who would like to become more similar to people who are engaged
in activities that we're attempting to cultivate ourselves. When it's the other people
meditating, it's more likely that you're going to meditate yourself. More importantly,
we're good qualities of such a personal life, we're going to rub off and provide a good
example. Sort of a roadside, something to look for when you're on the path to start becoming
more likely as people. As you see, the goodness in them is a great benefit from being
around good people. It's great to come to a meditation center. We've got lots of people
coming this year, this great. We've got people signing up for courses all the way through
October so far. It's really great to see such interest. We're still looking for a steward,
if there's anybody out there who would like to come and stay for a longer period. They've
had some people, there's one person from Israel who mentioned staying long term, so maybe
that'll work out. I don't know. I'm coming in September, I think. If anyone would like
to talk about that, then we'd be interested in helping out around here for months, some longer
period of time. That'd be great. Lots of stuff going on. So any questions? Does meditating
during an activity count does a formal practice? Would it be alright to log it as if time
spent in meditation? Yeah, I mean, it's not like we're going to check on you as to how you meditated
or so on. But I don't quite understand what you're saying during an activity. I don't understand
what you mean by activity. Does there a shared responsibility impact for one another's lives
than just one's own responsibility? Collective responsibility? Not really, but there's a sense of
rightness. If you don't help other people in giving the opportunity, there's a sense of tension
involved. And to some extent, you have your nervous stay at peace. You have to do some things
to benefit others. If you are cold and ignore the benefit of benefit to others, it can be a
hindrance to your practice. But no, we're not responsible for other people's being not directly.
We just have certain duties that as individuals, it's proper to perform. It's a part of
right, what's right, and the right action for the individual often involves helping other.
We're looking for disciples for your center, other amongst our people seeking to become
amongst how we've been interested. Being a steward would be the setup.
Well, the setup, you'd have to do about a month of meditation with us first. And then you just stay
on after that, but it would involve doing some of the things that intensive meditators, beginner
meditators, anyway, aren't able to do like some amount of cleaning and organizing,
caring for the monastery and helping me out, whatever I need. I mean, we had some recently
had a car, which was great, so I could drive, he could drive me places. That's not entirely
necessary, but it wouldn't be a plus. Some cooking made me, but something's to do with,
you know, we need a local person to organize things that I can't organize. Things with money and
buying basic supplies for the monastery. There's just, there's not a lot to do, but there are
things to be done. It would help to have a steward. I think, yeah, I think part of it was the
organization, one someone who had cooked for me, because right now I'm going to the restaurant, and
taking advantage of people's generosity in offering gift cards for certain restaurants, but it's
quite expensive. So it's not, it's not exactly, yeah, there you go. It's not efficient or economical
for doing that. I make more sense to somehow do cooking at the monastery.
As far as becoming a monk, I'm shying away from that, but long-term meditative is the option
certainly open, but we're not really advertising because it's too easy to miss the point,
ordaining should be a part of your meditation practice. It shouldn't be anything separate.
If you want to meditate long-term mode, becoming a monk means that would allow you to do that,
that's all. By calling someone's behavior, disgusting, is that not judging it? I mean, judging
is not always bad, discerning. I mean, disgusting is probably the wrong word. It's just a word,
but if you're actually disgusted by something, obviously that is a disliking event or a version
towards it. But we use disgusting as just a word in regards to a different kind of repulsion
in the sense of the inability to do it through wisdom and inability to get involved with something
out of a knowledge that it's bad, that it's wrong, that it leads to suffering, basically.
So if you know something that leads to suffering, the more you know the harder it is for you
to engage in it and when the potential to engage with people, for example, who are doing bad
things, arises when the opportunity arises. There's a certain recoiling that doesn't have anything
to do necessarily with a version, but it's based on a knowledge that that's not the way
to do things, getting involved with that person is not inefficient.
I clean the bathroom, I do it in my mind, my mind plays possibly. Right.
No, that's not quite what we're looking for. I mean, that's great. That's an awesome
a dream of doing it, but we're really looking for formal meditation practice.
That's what's meant here. I mean, do you actually do formal walking in Sydney?
May as the you do not cook your own food, I meditate with preparing food as part of my practice.
Amongst aren't allowed to keep food as part of a practice of renunciation as a lifestyle.
We don't keep foods, cook food. We can't eat anything unless it's given to us.
I mean, preparing your own food has its own problems because of the ability to choose
the mental activity involved with choosing your own food and thinking about what food you're
going to eat and some extent obsessing over the health and so on. There's something great about
just having to eat, whatever is being offered, and moreover, it's sort of the scraps that you get
by going through the village. That's the greatest. So that's sort of the model is for monks to go
walking through the village or scraps of arms. Whenever people are willing to give us charity,
because in India it was a big thing to give food to recklessness, to religious people,
and then get enough to survive and that was it. You avoid a lot of the trouble of having to cook
food and the potential, potential distraction involved in storing and cooking and having a kitchen
in the first place. Some monks aren't allowed, monks being a monk is on a different level.
Of course, for meditators cooking your own food is fine. There's just a sense that
it's an unwarranted distraction. If you're able to be free from that, you can focus more on
a deep meditation. It's too easy to get distracted. Sure, you can be mindful cooking.
It's great. But during a meditation course, much better not to have to cook. It's too much of a
distraction, especially for a beginner meditator.
Lots of things the Buddha said don't get involved and don't get caught up in work.
Don't get caught up in sleep. Don't get caught up in talk. Don't get caught up in
socializing. Don't get caught up in eating.
This thing is getting away of your practice.
So unfortunately, I don't think our quote was all that, 50 to night and few questions.
Doesn't look like there's any more. If you have any more questions, I'll be here again tomorrow.
I'm broadcasting a little bit every day.
So thank you all for tuning in. Keep up the good work.
