1
00:00:00,000 --> 00:00:05,820
Okay, so today we continue our study of the Bhamakarda,

2
00:00:05,820 --> 00:00:12,260
tenuing on with verses 13 and 14.

3
00:00:12,260 --> 00:00:15,820
Watch, read as follows.

4
00:00:15,820 --> 00:00:25,700
A-dha-a-garam tu-chanan uti, uti samati wijate,

5
00:00:25,700 --> 00:00:33,740
A-dha-garwitangjitan, raghos samati wijate.

6
00:00:33,740 --> 00:00:44,540
A-dha-garam tu-chanan, uti nah uti nah samati wijate,

7
00:00:44,540 --> 00:00:52,540
A-dha-garwitangjitan, ragho nah samati wijate,

8
00:00:52,540 --> 00:00:55,700
and the meaning as follows.

9
00:00:55,700 --> 00:01:01,980
A-dha-garam, just as a house or a building,

10
00:01:01,980 --> 00:01:08,280
Duchan nah, that is poorly attached or a roof,

11
00:01:08,280 --> 00:01:12,460
just as a roof that is poorly attached.

12
00:01:12,460 --> 00:01:15,660
The rain is able to penetrate,

13
00:01:15,660 --> 00:01:19,460
uti nah samati wijate.

14
00:01:19,460 --> 00:01:21,980
In the same way, an untrained mind,

15
00:01:21,980 --> 00:01:28,860
A-dha-garwitangjita, raghos samati wijate,

16
00:01:28,860 --> 00:01:35,140
the last is able to penetrate.

17
00:01:35,140 --> 00:01:42,460
And number 14, just as in regards to a well-trained mind,

18
00:01:42,460 --> 00:01:45,340
sorry, in regards to a well-fetched hut,

19
00:01:45,340 --> 00:01:47,940
worti nah samati wijate,

20
00:01:47,940 --> 00:01:51,500
rain is not able to penetrate.

21
00:01:51,500 --> 00:01:54,900
In the same way, a well-developed mind,

22
00:01:54,900 --> 00:01:59,100
lust is not able to penetrate.

23
00:01:59,100 --> 00:02:03,380
So, quite useful for meditators,

24
00:02:03,380 --> 00:02:05,700
system meditations teaching.

25
00:02:05,700 --> 00:02:08,620
We were taught in regards to the Buddha's cousin,

26
00:02:08,620 --> 00:02:11,980
the Buddha's cousin Nanda.

27
00:02:11,980 --> 00:02:16,500
When the Buddha went after Siddhartha Gautama became enlightened,

28
00:02:16,500 --> 00:02:20,500
he went back to his home,

29
00:02:20,500 --> 00:02:22,300
some couple of the watu,

30
00:02:22,300 --> 00:02:24,220
and he taught all of his relatives.

31
00:02:24,220 --> 00:02:28,220
And so, he taught his ex-wife, he taught his son,

32
00:02:28,220 --> 00:02:29,180
he taught his father,

33
00:02:29,180 --> 00:02:33,500
and they all were able to understand the teachings.

34
00:02:33,500 --> 00:02:36,420
But there was one person who,

35
00:02:36,420 --> 00:02:38,380
it seems the Buddha would understand,

36
00:02:38,380 --> 00:02:40,700
he had the ability to penetrate the teachings,

37
00:02:40,700 --> 00:02:43,420
but was kind of stuck in a bit of a rut.

38
00:02:43,420 --> 00:02:45,620
And that is that he was set to get married

39
00:02:45,620 --> 00:02:49,260
to the most beautiful woman in the country.

40
00:02:49,260 --> 00:02:52,060
And then the Bhadakaliani, which means

41
00:02:52,060 --> 00:02:53,580
the beauty of the land.

42
00:02:53,580 --> 00:02:55,380
So, in the area where they lived,

43
00:02:55,380 --> 00:02:58,900
she must have been the prize muti.

44
00:02:58,900 --> 00:03:00,540
So, she was well-known for her beauty,

45
00:03:00,540 --> 00:03:05,220
and Nanda had become infatuated with his beauty.

46
00:03:05,220 --> 00:03:07,460
And so, like ordinary human beings,

47
00:03:07,460 --> 00:03:11,340
he felt that that was going to bring him happiness

48
00:03:11,340 --> 00:03:20,100
to marry this woman to live a life of essential pleasure.

49
00:03:20,100 --> 00:03:22,380
But Buddhists, as Buddhists,

50
00:03:22,380 --> 00:03:24,220
we always throw monkey wrenches in today,

51
00:03:24,220 --> 00:03:29,700
so it's always with a rain on people's brains.

52
00:03:29,700 --> 00:03:33,660
And the Buddha was certainly our leader in this regard.

53
00:03:33,660 --> 00:03:36,780
So, on the day when they were to be married,

54
00:03:36,780 --> 00:03:39,420
or during the marriage preparation,

55
00:03:39,420 --> 00:03:43,380
they invited the Buddha to come to receive arms.

56
00:03:43,380 --> 00:03:46,780
And when the Buddha finished his arms,

57
00:03:46,780 --> 00:03:50,900
he handed the bull to Nanda.

58
00:03:50,900 --> 00:03:52,380
When Buddha finished his meal,

59
00:03:52,380 --> 00:03:56,220
and had given a talk on the teaching,

60
00:03:56,220 --> 00:03:57,340
he gave his bull to Nanda,

61
00:03:57,340 --> 00:04:00,100
and he started walking back to the monastery.

62
00:04:00,100 --> 00:04:02,860
And so, when he got to the top of the stairs,

63
00:04:02,860 --> 00:04:06,460
Nanda thought, well, I guess he'll take his bull back now.

64
00:04:06,460 --> 00:04:10,380
But he didn't say, excuse me, sir, can you take your bull back?

65
00:04:10,380 --> 00:04:12,500
Because he felt that that would be rude.

66
00:04:12,500 --> 00:04:15,380
And he had a lot of respect for the Buddha.

67
00:04:15,380 --> 00:04:21,260
And he never had great respect for the Buddha's teaching as well.

68
00:04:21,260 --> 00:04:23,100
And the Buddha didn't take back his bull.

69
00:04:23,100 --> 00:04:24,700
So, they got to the bottom of the stairs,

70
00:04:24,700 --> 00:04:26,900
and Nanda thought, oh, now he'll take back his bull,

71
00:04:26,900 --> 00:04:28,860
and the Buddha didn't take back his bull.

72
00:04:28,860 --> 00:04:30,260
Then he got to the door of the palace,

73
00:04:30,260 --> 00:04:31,380
and he thought, now he'll take his bull,

74
00:04:31,380 --> 00:04:32,460
and they got to the door to the city,

75
00:04:32,460 --> 00:04:35,580
and now he kept following after him

76
00:04:35,580 --> 00:04:40,260
with his bull, thinking, why does he wouldn't take back his bull?

77
00:04:40,260 --> 00:04:43,500
And so, people saw this, and they started passing it around,

78
00:04:43,500 --> 00:04:47,620
and they sent that word back to his bride to be,

79
00:04:47,620 --> 00:04:50,900
saying, it looks like this, I'm going to go to my house,

80
00:04:50,900 --> 00:04:55,500
has decided to capture your husband, your future husband,

81
00:04:55,500 --> 00:04:57,300
and he's going to spoil your whole plans.

82
00:04:57,300 --> 00:05:01,060
And so, she ran after them, and she called out to him,

83
00:05:01,060 --> 00:05:06,060
oh, please, no, no, please, no, no, sir, hurry back soon,

84
00:05:06,060 --> 00:05:08,620
in the most pitiful voice she can muster.

85
00:05:08,620 --> 00:05:11,580
And so, this was, of course, tearing at his heart,

86
00:05:11,580 --> 00:05:15,460
because he was very much in love with her,

87
00:05:15,460 --> 00:05:19,980
and he'll make him Buddha look like a bit of a meaning.

88
00:05:19,980 --> 00:05:24,220
And so, the Buddha, when he got back to the monastery,

89
00:05:24,220 --> 00:05:25,420
Nanda still thought, okay, well,

90
00:05:25,420 --> 00:05:30,100
when he went right all the way back to the Buddha's kutti,

91
00:05:30,100 --> 00:05:32,660
and the Buddha turned in Nanda and said,

92
00:05:32,660 --> 00:05:34,820
so, Nanda, would you like to become a monk?

93
00:05:36,180 --> 00:05:37,980
And when this thinking in his heart,

94
00:05:37,980 --> 00:05:40,140
no, no, no, no, no, I wouldn't.

95
00:05:40,140 --> 00:05:42,660
But he had so much respect for the Buddha,

96
00:05:42,660 --> 00:05:44,540
as people in this situation often are,

97
00:05:44,540 --> 00:05:47,380
when confronted by a person of authority,

98
00:05:47,380 --> 00:05:48,940
and he had been so used to, of course,

99
00:05:48,940 --> 00:05:52,660
to going, according to the flow,

100
00:05:52,660 --> 00:05:56,060
because he was a prince, and he was living in luxury,

101
00:05:56,060 --> 00:05:57,460
and you just kind of go along with things,

102
00:05:57,460 --> 00:06:01,980
and everything is wonderful, because my year of prince.

103
00:06:03,060 --> 00:06:06,220
So, right away, he said, he said,

104
00:06:06,220 --> 00:06:08,580
sure, I'll become a monk, but in his heart,

105
00:06:08,580 --> 00:06:12,540
he was so mortified that he would have to become a monk.

106
00:06:15,740 --> 00:06:16,780
And so, the Buddha said, okay,

107
00:06:16,780 --> 00:06:19,100
and he called the other monks over in the ordained in rights,

108
00:06:19,100 --> 00:06:19,940
and then there.

109
00:06:21,500 --> 00:06:25,060
And then the Buddha took him back,

110
00:06:25,060 --> 00:06:30,060
I believe, to Radhikaha, and in that time,

111
00:06:30,900 --> 00:06:34,340
Nanda started getting quite perturbed,

112
00:06:34,340 --> 00:06:36,820
and it was quite miserable as a monk, of course,

113
00:06:36,820 --> 00:06:40,300
because he was always thinking about his bride to be,

114
00:06:40,300 --> 00:06:42,060
who he was desperately in love with,

115
00:06:42,060 --> 00:06:43,860
and he thought, this is my soul mate,

116
00:06:43,860 --> 00:06:46,340
this is the one person who means anything to me.

117
00:06:47,540 --> 00:06:49,180
And here, the Buddha has taken this from me,

118
00:06:49,180 --> 00:06:50,300
he wasn't upset at the Buddha,

119
00:06:50,300 --> 00:06:52,260
but he was so miserable,

120
00:06:52,260 --> 00:06:55,260
because he was always thinking about the pleasure,

121
00:06:55,260 --> 00:06:57,700
and the great love that he had for her.

122
00:07:01,300 --> 00:07:02,900
And so, he said to his fellow monks,

123
00:07:02,900 --> 00:07:04,580
he finally had enough of him,

124
00:07:04,580 --> 00:07:05,820
and he said to them, he said,

125
00:07:05,820 --> 00:07:09,300
I've given up, I'm going to disrobe and become a lay person again,

126
00:07:09,300 --> 00:07:12,820
I can't take this anymore, this isn't my role in life,

127
00:07:12,820 --> 00:07:15,500
this isn't my path, and I'm in love with this woman,

128
00:07:15,500 --> 00:07:17,140
and she means everything to me.

129
00:07:18,340 --> 00:07:20,220
Of course, this is how I always justify it, right?

130
00:07:20,220 --> 00:07:22,540
And we have central pleasure, we always claim

131
00:07:22,540 --> 00:07:26,380
that there's a person who has some importance.

132
00:07:26,380 --> 00:07:28,420
We never get right down to it and say,

133
00:07:28,420 --> 00:07:29,820
I just think she's beautiful,

134
00:07:29,820 --> 00:07:31,420
and I want to see her and touch her,

135
00:07:31,420 --> 00:07:34,940
and we'll see a pleasure from her,

136
00:07:34,940 --> 00:07:36,540
and we'll say she has great meaning,

137
00:07:36,540 --> 00:07:38,500
and she's my soul mate, and so on.

138
00:07:40,340 --> 00:07:42,580
This is how we always feel, what happened?

139
00:07:42,580 --> 00:07:44,740
So they brought him to the Buddha,

140
00:07:44,740 --> 00:07:48,500
and they said, Lord Buddha, this mananda here,

141
00:07:48,500 --> 00:07:51,140
he says he's going to disrobe,

142
00:07:52,780 --> 00:07:55,860
and the Buddha takes Nanda, he said, okay,

143
00:07:55,860 --> 00:07:58,500
I'll look after this, he took Nanda by his arm,

144
00:07:59,940 --> 00:08:02,660
and brought him magically.

145
00:08:03,620 --> 00:08:08,620
First he brought him to this burnt out for us,

146
00:08:09,180 --> 00:08:12,260
like it was a forest fire, it was totally burnt down,

147
00:08:12,260 --> 00:08:14,740
and all there was left was this she monkey,

148
00:08:14,740 --> 00:08:18,820
this female monkey who was clinging to a stump of a tree,

149
00:08:18,820 --> 00:08:20,220
that she had probably used to live in,

150
00:08:20,220 --> 00:08:24,180
but it was like the last standing burnt out tree,

151
00:08:24,180 --> 00:08:25,860
and her tail had been burnt off,

152
00:08:25,860 --> 00:08:29,300
and her ears had been burnt off,

153
00:08:30,660 --> 00:08:34,860
and what else, her tails and her tail had her ears,

154
00:08:34,860 --> 00:08:37,300
and her fur had been cinged and so on.

155
00:08:37,300 --> 00:08:40,780
And there she was clinging in great suffering and pain,

156
00:08:40,780 --> 00:08:44,740
and just looking like a total miserable sight,

157
00:08:44,740 --> 00:08:46,460
and the Buddha said, you see that monkey over there,

158
00:08:46,460 --> 00:08:48,260
and he said, yes, yes, I see that,

159
00:08:48,260 --> 00:08:51,940
that monkey, and he didn't quite understand what the point was,

160
00:08:51,940 --> 00:08:55,340
this female monkey there, and then he said,

161
00:08:55,340 --> 00:08:59,420
okay, now let's go, and he took him magically up to the heavens,

162
00:08:59,420 --> 00:09:02,940
and he took him to the dawatings that happened, I think,

163
00:09:02,940 --> 00:09:07,660
and there he showed him 500 dove-footed nymphs,

164
00:09:07,660 --> 00:09:09,100
or pink-footed nymphs, I think,

165
00:09:09,100 --> 00:09:11,060
because I don't know true with the Paulians

166
00:09:11,060 --> 00:09:13,500
because something about their feet being pink,

167
00:09:13,500 --> 00:09:16,660
but just beautiful nymphs, of course, angels.

168
00:09:19,900 --> 00:09:21,820
And the thing about nymphs, of course,

169
00:09:21,820 --> 00:09:24,540
is that they're so much more beautiful,

170
00:09:24,540 --> 00:09:27,220
essentially attractive than any human being.

171
00:09:27,220 --> 00:09:31,660
It's hard to imagine, it's not like just beautiful women.

172
00:09:31,660 --> 00:09:35,100
These are, well, these are people with great merit,

173
00:09:35,100 --> 00:09:37,620
and so they have become very beautiful as a result

174
00:09:37,620 --> 00:09:40,340
of their beauty of mind,

175
00:09:40,340 --> 00:09:45,500
and so the physical has come to reflect that,

176
00:09:45,500 --> 00:09:47,220
beyond anything, most of us,

177
00:09:47,220 --> 00:09:48,820
they have ever come across.

178
00:09:48,820 --> 00:09:52,220
So, Nanda was quite blown away,

179
00:09:52,220 --> 00:09:55,220
and seeing these 500 nymphs playing around,

180
00:09:55,220 --> 00:09:59,100
or doing the thing, and the Buddha said,

181
00:09:59,100 --> 00:10:02,380
so tell me, Nanda, which one is more beautiful

182
00:10:02,380 --> 00:10:05,140
of your wife, Janapada Kaliyami,

183
00:10:05,140 --> 00:10:08,140
or these 500 nymphs?

184
00:10:08,140 --> 00:10:10,860
Nanda, who, by this time, had kind of forgotten

185
00:10:10,860 --> 00:10:13,380
about his wife, he said,

186
00:10:13,380 --> 00:10:18,380
venerable sir, if you compare my wife in that monkey

187
00:10:20,700 --> 00:10:25,700
that we saw, my wife, these dafu'ed nymphs,

188
00:10:25,700 --> 00:10:30,180
are as much more beautiful than my wife,

189
00:10:30,180 --> 00:10:35,460
as my wife is more beautiful than that monkey.

190
00:10:35,460 --> 00:10:41,140
And the Buddha said, well, then tell you what, Nanda,

191
00:10:41,140 --> 00:10:44,260
if you stay as a monk and you try your best

192
00:10:44,260 --> 00:10:47,260
to follow my instruction and to leave the holy life,

193
00:10:47,260 --> 00:10:50,780
I promise you, these 500 nymphs will be yours,

194
00:10:50,780 --> 00:10:53,300
will be your entourage,

195
00:10:53,300 --> 00:10:57,140
will be reborn into this state,

196
00:10:57,140 --> 00:11:00,140
to frolic around with the nymphs.

197
00:11:02,740 --> 00:11:05,980
And Nanda, he wasn't, I guess, at this point,

198
00:11:05,980 --> 00:11:07,420
they're very deep in the visual,

199
00:11:07,420 --> 00:11:10,140
because, well, like most of us,

200
00:11:10,140 --> 00:11:15,140
he gets caught up with the emotions, or with lust, you know?

201
00:11:15,140 --> 00:11:18,020
And this is because his mind is untrained.

202
00:11:21,060 --> 00:11:25,220
And so he, his mind was just wiped out the memory

203
00:11:25,220 --> 00:11:29,300
of his bride to be hers, or his soon-to-the-life.

204
00:11:31,540 --> 00:11:35,580
And was overjoyed at this thought.

205
00:11:35,580 --> 00:11:39,300
He said, well, in that case, I'll work really hard,

206
00:11:39,300 --> 00:11:40,980
because suddenly he had seen something

207
00:11:40,980 --> 00:11:43,060
that most human beings never see.

208
00:11:43,060 --> 00:11:46,060
This, something that he thought was,

209
00:11:46,060 --> 00:11:50,460
well, he thought his wife was on the top.

210
00:11:50,460 --> 00:11:52,740
And he was seeing something that in his mind

211
00:11:52,740 --> 00:11:56,900
was much greater, which, because the essential attraction

212
00:11:56,900 --> 00:11:58,580
would have been much greater,

213
00:11:58,580 --> 00:12:01,980
the perfection of these beings would have attracted him.

214
00:12:04,220 --> 00:12:07,660
And so he became quite excited,

215
00:12:07,660 --> 00:12:10,340
and he became much more interested in living the whole life,

216
00:12:10,340 --> 00:12:12,380
because he realized that even in India,

217
00:12:12,380 --> 00:12:14,140
this before, even before the Buddha,

218
00:12:14,140 --> 00:12:18,980
they believed this, that through the practice of austerity

219
00:12:18,980 --> 00:12:24,100
or whatever, you could go to heaven and enjoy great sensuality

220
00:12:24,100 --> 00:12:25,740
in heaven.

221
00:12:25,740 --> 00:12:28,340
So he thought, wow, it must be true.

222
00:12:28,340 --> 00:12:30,340
This must really be the result of practicing

223
00:12:30,340 --> 00:12:31,060
the moon is teaching.

224
00:12:31,060 --> 00:12:33,380
So he was really excited.

225
00:12:33,380 --> 00:12:37,100
And it just shows how thick of the mind is really.

226
00:12:37,100 --> 00:12:42,220
The whole idea of soul mates is kind of thrown out of the window.

227
00:12:42,220 --> 00:12:45,300
And so from that point on, he practiced quite diligently.

228
00:12:45,300 --> 00:12:47,260
And the Buddha kind of helped him along,

229
00:12:47,260 --> 00:12:49,140
as I understand, because the Buddha

230
00:12:49,140 --> 00:12:51,340
let it be known what their agreement was.

231
00:12:51,340 --> 00:12:54,100
And so all the other monks came to know that Nanda

232
00:12:54,100 --> 00:13:00,780
is only practicing because he wants to have intercourse

233
00:13:00,780 --> 00:13:05,540
with a bunch of celestial nymphs.

234
00:13:05,540 --> 00:13:08,900
And so they called him a higherling from that point on.

235
00:13:08,900 --> 00:13:11,220
And he said, oh, Nanda, there goes Nanda the higherling.

236
00:13:11,220 --> 00:13:14,420
Oh, hello, Nanda, the higherling.

237
00:13:14,420 --> 00:13:16,140
Because he has been, it seems that Nanda

238
00:13:16,140 --> 00:13:22,780
can be bought with a price, or can be bribed.

239
00:13:22,780 --> 00:13:25,260
So they were scorning him and so on.

240
00:13:25,260 --> 00:13:28,180
And so he felt quite ashamed about that.

241
00:13:28,180 --> 00:13:29,620
But nonetheless, he worked really hard

242
00:13:29,620 --> 00:13:32,140
because he felt like he now had the, he was now

243
00:13:32,140 --> 00:13:35,700
both in line with the Buddha and in line with his desires.

244
00:13:35,700 --> 00:13:40,460
Because he had done what most people do when this situation

245
00:13:40,460 --> 00:13:42,500
comes, he had made a new decision

246
00:13:42,500 --> 00:13:45,300
that he is where his soul mates, or this was his destiny

247
00:13:45,300 --> 00:13:46,300
with his wife.

248
00:13:49,020 --> 00:13:50,140
And so he worked very hard.

249
00:13:50,140 --> 00:13:54,260
And as a result of working very hard, slowly, slowly,

250
00:13:54,260 --> 00:13:57,380
his mind changed, his mind calmed down.

251
00:13:57,380 --> 00:14:01,820
His mind became relaxed, his mind became clear.

252
00:14:01,820 --> 00:14:06,660
And he was able to see the arising and ceasing of Nanda.

253
00:14:06,660 --> 00:14:09,060
And he was able to follow the Buddha's teaching

254
00:14:09,060 --> 00:14:13,300
and understand the five anger gets, able to break them up.

255
00:14:13,300 --> 00:14:19,900
To the point that he began to lose his desires and lose his lust.

256
00:14:19,900 --> 00:14:23,020
And that he was able to see that, really, whether it's

257
00:14:23,020 --> 00:14:25,900
his wife or whether it's these celestial nymphs,

258
00:14:25,900 --> 00:14:31,020
it's all just nama and group, all just experience,

259
00:14:31,020 --> 00:14:33,940
momentary experience that arises and ceasing.

260
00:14:33,940 --> 00:14:36,460
He came to say that this is in permanent unsatisfying

261
00:14:36,460 --> 00:14:38,780
and uncontrollable.

262
00:14:38,780 --> 00:14:42,660
And as a result, he gave it up and became an hour

263
00:14:42,660 --> 00:14:45,980
on, in the realization in his mind,

264
00:14:45,980 --> 00:14:49,100
let go, and he entered into Nibana,

265
00:14:49,100 --> 00:14:52,180
and was able to become an hour on.

266
00:14:52,180 --> 00:14:53,620
Now at the moment when he became an hour on,

267
00:14:53,620 --> 00:14:55,940
he went back to see the Buddha.

268
00:14:55,940 --> 00:15:00,420
And he said to the Buddha, you know that bargain we had.

269
00:15:00,420 --> 00:15:03,180
He wouldn't have been ashamed, but he said

270
00:15:03,180 --> 00:15:05,860
that bargain that we have, then or both sir,

271
00:15:05,860 --> 00:15:10,500
I release you from that bargain.

272
00:15:10,500 --> 00:15:11,700
And the Buddha said, from the moment

273
00:15:11,700 --> 00:15:12,780
that you became an hour on,

274
00:15:12,780 --> 00:15:15,860
that was released from the bargain.

275
00:15:15,860 --> 00:15:18,780
This is the story of Nanda.

276
00:15:18,780 --> 00:15:23,100
And then the monks heard about, or the monks, as usual,

277
00:15:23,100 --> 00:15:27,860
they would tease him and they said, so Nanda,

278
00:15:27,860 --> 00:15:32,420
still thinking about some nymphs, or what

279
00:15:32,420 --> 00:15:34,580
are the nymphs for today?

280
00:15:34,580 --> 00:15:36,740
What's the goal for today?

281
00:15:36,740 --> 00:15:39,620
Still thinking about your wife, still thinking about those nymphs,

282
00:15:39,620 --> 00:15:43,260
and they said, before my mind was untrained,

283
00:15:43,260 --> 00:15:44,820
and so I thought about them.

284
00:15:44,820 --> 00:15:49,500
But now my mind is trained, and so I don't think about them.

285
00:15:49,500 --> 00:15:50,860
And they listened to this, and they thought,

286
00:15:50,860 --> 00:15:54,460
listen to him, because this is a serious offense,

287
00:15:54,460 --> 00:15:55,940
if you lie about something like this,

288
00:15:55,940 --> 00:15:57,900
pretending to be a man.

289
00:15:57,900 --> 00:16:00,100
So they went to see the Buddha, and they said,

290
00:16:00,100 --> 00:16:01,260
you should hear what Nanda's saying,

291
00:16:01,260 --> 00:16:03,540
no, he's pretending to be enlightened.

292
00:16:03,540 --> 00:16:06,780
Maybe he thinks he'll get better nymphs that way.

293
00:16:06,780 --> 00:16:10,340
And the Buddha said, my son is correct.

294
00:16:10,340 --> 00:16:12,140
Before when his mind was untrained,

295
00:16:12,140 --> 00:16:14,140
it was like a roof.

296
00:16:14,140 --> 00:16:16,340
So that was ill-fetched.

297
00:16:16,340 --> 00:16:18,140
And now that his mind is trained,

298
00:16:18,140 --> 00:16:22,180
it is like a roof that is well-fetched.

299
00:16:22,180 --> 00:16:23,700
And so he told this verse, he said,

300
00:16:23,700 --> 00:16:29,300
just as poorly-fetched roof is penetrated by the rain.

301
00:16:29,300 --> 00:16:34,220
So, and untrained mind is penetrated by lust.

302
00:16:34,220 --> 00:16:37,340
I think part of what helped him,

303
00:16:37,340 --> 00:16:41,380
which is an interesting aspect of lust,

304
00:16:41,380 --> 00:16:43,060
was the, would have been the realization

305
00:16:43,060 --> 00:16:44,500
that his mind was just,

306
00:16:44,500 --> 00:16:47,180
that his mind was really just playing tricks on him.

307
00:16:47,180 --> 00:16:49,420
Because this is born out in reality.

308
00:16:49,420 --> 00:16:51,420
People who think that they have met their soul match,

309
00:16:51,420 --> 00:16:54,420
or that they get the idea of self,

310
00:16:54,420 --> 00:16:57,300
and they think, I like this, I like that.

311
00:16:58,380 --> 00:17:02,580
Or this is my preference, my wife, my lover,

312
00:17:02,580 --> 00:17:04,700
and so on, my soul mate.

313
00:17:06,700 --> 00:17:10,540
They can, they can, on a heart rate turn,

314
00:17:10,540 --> 00:17:11,980
and fall for something else.

315
00:17:11,980 --> 00:17:14,860
And the realization that this is the nature of the mind,

316
00:17:14,860 --> 00:17:17,340
that it jumps like a fire.

317
00:17:17,340 --> 00:17:22,340
The Buddha said, nati, raghos, nati, raghasma, angi.

318
00:17:23,060 --> 00:17:27,460
There is no fire like raghali, night last.

319
00:17:27,460 --> 00:17:30,500
Because it jumps from one object to another.

320
00:17:30,500 --> 00:17:33,260
Fire, you can contain it, you can contain most fires.

321
00:17:34,580 --> 00:17:36,540
By digging pits, the very good trenches,

322
00:17:36,540 --> 00:17:40,020
no fire won't jump to a rock, it won't jump to water.

323
00:17:41,140 --> 00:17:43,580
But lust jumps to anything.

324
00:17:43,580 --> 00:17:46,100
We can lust after, just about anything.

325
00:17:46,100 --> 00:17:47,820
This is why when you're born in a different realm,

326
00:17:47,820 --> 00:17:49,700
you become, begin to lust.

327
00:17:49,700 --> 00:17:51,540
If you're born a doggy, lust after dogs,

328
00:17:51,540 --> 00:17:53,900
if you're born a dangby, don't you lust after dangby,

329
00:17:57,180 --> 00:18:00,180
so what is the teaching here for us as meditators?

330
00:18:00,180 --> 00:18:01,820
This is the story part.

331
00:18:01,820 --> 00:18:03,220
As for what this can do for us,

332
00:18:03,220 --> 00:18:07,580
as we come to see our minds in this way,

333
00:18:07,580 --> 00:18:11,540
and come to see our minds as having holes like the hole

334
00:18:11,540 --> 00:18:14,140
in a roof, our minds are permeable.

335
00:18:15,900 --> 00:18:19,420
And so our job as meditators is to touch the holes,

336
00:18:19,420 --> 00:18:21,980
to patch up the holes.

337
00:18:21,980 --> 00:18:24,740
And the way we do this, we, by developing our minds,

338
00:18:24,740 --> 00:18:27,460
just as the roof has to be patched,

339
00:18:27,460 --> 00:18:30,580
has to be well covered over.

340
00:18:30,580 --> 00:18:34,180
So through the mind, has to be well sealed.

341
00:18:34,180 --> 00:18:36,220
The way to seal the mind is in three ways,

342
00:18:36,220 --> 00:18:38,500
if we're developing that development of the mind

343
00:18:38,500 --> 00:18:39,420
or the development.

344
00:18:40,380 --> 00:18:42,660
First is a development of morality.

345
00:18:42,660 --> 00:18:43,940
And that's where we're starting off here,

346
00:18:43,940 --> 00:18:44,780
because at the beginning,

347
00:18:44,780 --> 00:18:46,140
you don't have concentration,

348
00:18:46,140 --> 00:18:48,020
you just have your morality.

349
00:18:48,020 --> 00:18:49,020
What is your morality?

350
00:18:49,020 --> 00:18:51,580
It's, you've stopped so many things,

351
00:18:51,580 --> 00:18:54,060
you've stopped connecting with the world.

352
00:18:54,060 --> 00:18:56,300
And also, you've stopped letting your mind wander.

353
00:18:56,300 --> 00:18:58,860
So when you see something, instead of looking at it,

354
00:18:58,860 --> 00:19:00,620
you're saying to yourself, see,

355
00:19:00,620 --> 00:19:02,020
seeing that's morality.

356
00:19:03,420 --> 00:19:08,420
The narrowing, your activity, your mental activity,

357
00:19:09,180 --> 00:19:11,740
because that's what's gonna bring concentration.

358
00:19:13,300 --> 00:19:15,380
Normally, when you walk, you're also thinking,

359
00:19:15,380 --> 00:19:17,460
and you're doing, you have no morality,

360
00:19:17,460 --> 00:19:20,260
or no control over the mind.

361
00:19:20,260 --> 00:19:23,300
So now you're in some way trying to control,

362
00:19:23,300 --> 00:19:27,620
and restrict yourself, morality.

363
00:19:27,620 --> 00:19:30,260
So when you walk, you say walking, walking.

364
00:19:31,220 --> 00:19:33,540
And even more so when you're practicing meditation,

365
00:19:33,540 --> 00:19:36,020
you're severely restricting your mind

366
00:19:36,020 --> 00:19:37,660
to just compare awareness.

367
00:19:37,660 --> 00:19:39,820
When you hear a sound hearing,

368
00:19:39,820 --> 00:19:43,140
when you see, when you feel pain, pain, pain,

369
00:19:43,140 --> 00:19:53,780
not letting your mind dwell, or develop, or make more proliferate,

370
00:19:53,780 --> 00:19:54,540
that's all right.

371
00:19:55,700 --> 00:19:58,580
So that develops concentration from morality,

372
00:19:58,580 --> 00:20:01,420
morality is the first layer, and morality is good,

373
00:20:01,420 --> 00:20:05,340
because it prevents you from getting caught up

374
00:20:05,340 --> 00:20:10,420
in just in distraction, in doing many things

375
00:20:10,420 --> 00:20:14,100
that would, that would hurt your meditation.

376
00:20:14,980 --> 00:20:16,620
But the next level, as you continue on,

377
00:20:16,620 --> 00:20:18,860
to continue on, you'll develop concentration.

378
00:20:18,860 --> 00:20:23,860
Concentration is the second way to patch up our minds,

379
00:20:24,980 --> 00:20:26,340
to seal up our minds.

380
00:20:27,700 --> 00:20:29,060
Once you develop concentration,

381
00:20:29,060 --> 00:20:31,220
then your mind won't even give rise to liking,

382
00:20:31,220 --> 00:20:32,060
or just liking.

383
00:20:32,060 --> 00:20:36,060
Once you're able to see the object for you,

384
00:20:36,060 --> 00:20:38,380
watch the rising, and you watch the falling,

385
00:20:38,380 --> 00:20:39,420
and you know that it's rising.

386
00:20:39,420 --> 00:20:40,820
At the moment, when you know that it's rising,

387
00:20:40,820 --> 00:20:43,060
your mind is perfectly clear.

388
00:20:43,060 --> 00:20:44,740
It's perfectly dry.

389
00:20:44,740 --> 00:20:48,860
The lusting desires can come in,

390
00:20:48,860 --> 00:20:52,740
your, the fire can't, can't drive you off

391
00:20:52,740 --> 00:20:55,980
to go and chase after celestial nymphs, or whatever.

392
00:20:56,980 --> 00:21:01,580
And the point being that these things are all impermanent,

393
00:21:01,580 --> 00:21:03,900
and they're all relative.

394
00:21:03,900 --> 00:21:05,860
They don't really have the ability to satisfy.

395
00:21:05,860 --> 00:21:07,620
They'll create pleasure for some time,

396
00:21:07,620 --> 00:21:09,340
and even being born in heaven,

397
00:21:09,340 --> 00:21:11,260
is not something that is permanent,

398
00:21:11,260 --> 00:21:14,100
because it has a power, and it has a force,

399
00:21:14,100 --> 00:21:16,420
and that force arises and ceases.

400
00:21:20,540 --> 00:21:22,140
And so when you develop concentration,

401
00:21:22,140 --> 00:21:25,060
your mind will not give rise to these lusts.

402
00:21:25,060 --> 00:21:26,620
You'll just see things as they are.

403
00:21:26,620 --> 00:21:27,820
Once you see things as they are,

404
00:21:27,820 --> 00:21:29,300
wisdom will begin to arise.

405
00:21:30,660 --> 00:21:32,620
And the question is, why if concentration

406
00:21:32,620 --> 00:21:34,300
already stops these things from a rise,

407
00:21:34,300 --> 00:21:37,060
and why isn't it wisdom necessary?

408
00:21:37,060 --> 00:21:38,900
Because concentration, just like any roof,

409
00:21:38,900 --> 00:21:40,340
it's temporary.

410
00:21:40,340 --> 00:21:43,100
As long as you keep patching it up,

411
00:21:43,100 --> 00:21:47,500
it will protect you.

412
00:21:47,500 --> 00:21:48,580
But when you leave it alone,

413
00:21:48,580 --> 00:21:49,700
if you leave your roof alone,

414
00:21:49,700 --> 00:21:52,700
eventually the elements will penetrate it.

415
00:21:54,100 --> 00:21:57,900
The mind is the same when you start practicing meditation,

416
00:21:57,900 --> 00:22:00,580
when you leave it alone, all of it will come back.

417
00:22:00,580 --> 00:22:03,900
So this is a good way of protecting the mind

418
00:22:03,900 --> 00:22:05,300
that many people in the world use,

419
00:22:05,300 --> 00:22:09,220
but it's not the ultimate method of protecting the mind.

420
00:22:09,220 --> 00:22:11,780
The ultimate one is when you develop wisdom.

421
00:22:11,780 --> 00:22:13,700
As you're looking and you're seeing things clearly,

422
00:22:13,700 --> 00:22:15,900
you begin to penetrate the characteristics

423
00:22:15,900 --> 00:22:19,220
and the nature of things, the nature of reality.

424
00:22:20,700 --> 00:22:23,140
Our reality, even right here and now sitting here,

425
00:22:25,100 --> 00:22:29,300
is one of phenomena arising in season.

426
00:22:29,300 --> 00:22:30,460
Sometimes we're seeing,

427
00:22:30,460 --> 00:22:33,180
sometimes we're hearing, smelling, tasting,

428
00:22:33,180 --> 00:22:37,860
feeling, thinking, we have our emotions

429
00:22:37,860 --> 00:22:41,260
and all of these things arising in season.

430
00:22:41,260 --> 00:22:43,580
We begin to see this and we're able to break them apart

431
00:22:43,580 --> 00:22:46,540
and see them from a tear of one by one by one.

432
00:22:47,580 --> 00:22:49,620
And we realize that there is no,

433
00:22:49,620 --> 00:22:52,060
this idea of self, this idea of me

434
00:22:52,060 --> 00:22:54,140
being able to experience pleasure

435
00:22:54,140 --> 00:22:56,340
or become this or become that.

436
00:22:56,340 --> 00:22:57,700
It's really just a delusion.

437
00:22:58,940 --> 00:23:00,940
When the attraction towards some being

438
00:23:00,940 --> 00:23:04,900
or something is just a mind state that arises

439
00:23:04,900 --> 00:23:06,620
and it arises and it seems to be.

440
00:23:07,900 --> 00:23:12,100
You begin to see the progression of mind states.

441
00:23:12,100 --> 00:23:14,020
When you give rise to lust, what happens?

442
00:23:14,020 --> 00:23:15,900
It creates attachment and the attachment

443
00:23:15,900 --> 00:23:18,860
leads to disappointment when you don't get what you want.

444
00:23:20,300 --> 00:23:21,860
When you just like something,

445
00:23:21,860 --> 00:23:26,220
it leads to anger and frustration and directly to suffering.

446
00:23:26,220 --> 00:23:28,220
So when you see this, you start to lose interest.

447
00:23:28,220 --> 00:23:29,340
You start to give up.

448
00:23:29,340 --> 00:23:32,060
You start to realize that nature doesn't admit

449
00:23:32,060 --> 00:23:33,580
of liking or disliking it,

450
00:23:33,580 --> 00:23:36,780
doesn't admit that it's a good or bad.

451
00:23:36,780 --> 00:23:39,940
The liking and the disliking comes from creating ideas

452
00:23:39,940 --> 00:23:42,060
about it and ideas for self,

453
00:23:42,060 --> 00:23:45,860
that this is my experience and wanting that experience

454
00:23:45,860 --> 00:23:46,860
again for yourself.

455
00:23:48,060 --> 00:23:49,620
Once you just see it from what it is,

456
00:23:51,220 --> 00:23:53,140
your mind doesn't have anything to cling to.

457
00:23:53,140 --> 00:23:55,460
It doesn't have any interest in anything.

458
00:23:55,460 --> 00:23:57,780
This is, I want to be that, I want to be this.

459
00:23:57,780 --> 00:23:59,300
It's able to just experience.

460
00:24:00,540 --> 00:24:02,700
Once you do that, once you get to that point

461
00:24:02,700 --> 00:24:04,740
where you see things as they are,

462
00:24:04,740 --> 00:24:07,620
because there's nothing positive or negative

463
00:24:07,620 --> 00:24:09,180
in anything really.

464
00:24:09,180 --> 00:24:11,940
You think this is glitter, you think this is bad?

465
00:24:11,940 --> 00:24:15,620
It's all in, it's all a concept that arises in your mind.

466
00:24:15,620 --> 00:24:16,780
When you see through that,

467
00:24:18,260 --> 00:24:19,980
then it doesn't matter what comes to you.

468
00:24:19,980 --> 00:24:22,100
This is the ultimate protection

469
00:24:22,100 --> 00:24:24,300
because nothing can penetrate.

470
00:24:24,300 --> 00:24:27,820
The reason why we have give rise to likes

471
00:24:27,820 --> 00:24:30,540
and dislikes, we give rise to lust in the first place,

472
00:24:31,740 --> 00:24:32,900
is through ignorance,

473
00:24:32,900 --> 00:24:35,900
because ignorance is, well, ignorance is the default.

474
00:24:35,900 --> 00:24:39,540
Not knowing, everything in the universe comes together,

475
00:24:39,540 --> 00:24:40,980
it doesn't come together out of knowledge,

476
00:24:40,980 --> 00:24:43,340
it just comes together and arises and develops.

477
00:24:44,340 --> 00:24:46,140
Wisdom is something that isn't inherent

478
00:24:46,140 --> 00:24:47,820
in the universe, and the universe,

479
00:24:47,820 --> 00:24:49,700
wisdom has to come afterwards,

480
00:24:49,700 --> 00:24:53,060
and that's to come as a result of all of this coming up,

481
00:24:53,060 --> 00:24:58,060
and the systematic observation,

482
00:24:58,940 --> 00:25:01,260
and the eventual understanding.

483
00:25:01,260 --> 00:25:03,220
Once you understand,

484
00:25:03,220 --> 00:25:06,060
then none of it can affect you,

485
00:25:06,060 --> 00:25:08,340
then you don't give rise to likes of this.

486
00:25:09,860 --> 00:25:12,180
Then your mind is perfectly well-fetched,

487
00:25:12,180 --> 00:25:14,620
and so that is the development that we're trying for here.

488
00:25:14,620 --> 00:25:17,380
We're trying to just come to understand things.

489
00:25:17,380 --> 00:25:21,140
It's not intellectual, it's by repeated observation

490
00:25:21,140 --> 00:25:24,940
and direct experience once you see it again,

491
00:25:24,940 --> 00:25:26,780
and again, for what it is.

492
00:25:29,020 --> 00:25:30,780
Your mind will change,

493
00:25:30,780 --> 00:25:32,780
your way of approaching things will change.

494
00:25:32,780 --> 00:25:35,100
You'll have no desire to chase after things,

495
00:25:35,100 --> 00:25:36,780
you're knowing that when you chase after it,

496
00:25:36,780 --> 00:25:39,220
you know what it leads to, because you've seen that.

497
00:25:39,220 --> 00:25:41,060
You have no desire to be angry or upset

498
00:25:41,060 --> 00:25:43,620
at things or frustrated about things,

499
00:25:43,620 --> 00:25:46,180
because you know it, frustration leads to.

500
00:25:47,300 --> 00:25:49,380
And furthermore, you see these things for what they are,

501
00:25:49,380 --> 00:25:52,060
so you don't think that this is bad or this is good

502
00:25:52,060 --> 00:25:53,700
when something happens.

503
00:25:53,700 --> 00:25:54,900
You don't have any judgment for it.

504
00:25:54,900 --> 00:25:57,220
You see that it happens and you let it be.

505
00:25:58,700 --> 00:26:00,220
So this is the teaching for today.

506
00:26:00,220 --> 00:26:05,140
This is really what we're trying to start with morality,

507
00:26:05,140 --> 00:26:06,980
bringing our mind in,

508
00:26:06,980 --> 00:26:08,620
and then we develop a concentration,

509
00:26:08,620 --> 00:26:11,940
which allows us to see clearly once we see clearly,

510
00:26:11,940 --> 00:26:13,580
our minds will be,

511
00:26:13,580 --> 00:26:15,900
our minds will be well-fetched,

512
00:26:15,900 --> 00:26:19,580
so well, in fact, the word well is an understatement.

513
00:26:19,580 --> 00:26:22,340
They will be totally waterproof,

514
00:26:22,340 --> 00:26:26,260
our last proof in this case.

515
00:26:26,260 --> 00:26:28,140
So this is the teaching on the double bottom,

516
00:26:28,140 --> 00:26:31,500
based on verses 13 and 14.

517
00:26:31,500 --> 00:26:33,260
Thanks for tuning in,

518
00:26:33,260 --> 00:26:51,100
and back to meditation for you.

