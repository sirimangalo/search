1
00:00:00,000 --> 00:00:22,320
Good evening everyone, we're broadcasting live May 18th, 2016.

2
00:00:22,320 --> 00:00:37,440
The night's quote is from the wind up on her, and without context it's simply a description

3
00:00:37,440 --> 00:00:55,600
of the virtues of a virtuous person, ten virtues of a virtuous person.

4
00:00:55,600 --> 00:01:02,600
It's interesting to learn the context, it's not necessary, but it is interesting because

5
00:01:02,600 --> 00:01:13,080
the reason why he, the Melinda Panah is, if I haven't mentioned this before, it's a very large

6
00:01:13,080 --> 00:01:30,120
text, well, it's a sizeable text with questions and answers between King Melinda and Nagasena

7
00:01:30,120 --> 00:01:43,840
and it's really a great book for getting some insight into the Buddha's stance on various

8
00:01:43,840 --> 00:01:56,440
issues and on dilemmas which presented by the texts, by the canonical texts.

9
00:01:56,440 --> 00:02:03,400
It's so valuable that the Burmese edition of the Topitika, I understand, includes the Melinda

10
00:02:03,400 --> 00:02:13,480
Panah in the Kundakani Kaya, even though it's much later, but it's definitely worth studying,

11
00:02:13,480 --> 00:02:16,600
worth reading if you have time.

12
00:02:16,600 --> 00:02:31,080
So this, the context here is discussing suicide, Nabikavi, Adhanan-Bhati, when should not destroy

13
00:02:31,080 --> 00:02:44,840
oneself, whoever it is, so it's hard to move God at the moment, shouldn't be dealt with

14
00:02:44,840 --> 00:02:55,280
according to the Dhamma, I think, which means if a monk kills himself, there's a pen,

15
00:02:55,280 --> 00:03:03,160
I think there's, actually, I don't know, it's interesting if he's dead and, well,

16
00:03:03,160 --> 00:03:15,320
it's hard to move God at the moment, I think if someone tries to kill themselves, they should, they're guilty of an offence.

17
00:03:15,320 --> 00:03:21,320
So I, as I understand, I'm reading the poly and as usual, my poly's not great, but it looks like

18
00:03:21,320 --> 00:03:28,600
Melinda, if I remember correctly as well, he says, well, the Buddha says this, but he also

19
00:03:28,600 --> 00:03:37,880
says that Jadhyaya, Jaraya, Bhyadhi, no one's on, he teaches the Dhamma for the Samujaya,

20
00:03:37,880 --> 00:03:42,280
for the cutting off of these things of birth, old age, sickness, and death.

21
00:03:43,880 --> 00:03:47,960
So if he teaches the Dhamma for cutting off birth, old age, sickness, and death,

22
00:03:48,680 --> 00:03:52,600
what's wrong with killing yourself? Because if you kill yourself, these things only last up

23
00:03:52,600 --> 00:03:56,680
into death, and if you kill yourself, then you don't have these things.

24
00:03:56,680 --> 00:04:03,080
Then I guess the idea is, why wouldn't an R-100? Because an ordinary person, obviously,

25
00:04:03,080 --> 00:04:07,560
it's not an escape, if you kill yourself, it's just going to be born again.

26
00:04:07,560 --> 00:04:13,800
But why wouldn't an R-100? I think that's what he's saying.

27
00:04:13,800 --> 00:04:26,440
And then he said, why does he teach both of these? Why can these both be true?

28
00:04:27,000 --> 00:04:30,760
I think he teaches one, why does he say it's wrong for an R-100 to kill themselves?

29
00:04:32,280 --> 00:04:39,240
And the Buddha says, and then Nagasena says, wow, this is because a sea lover,

30
00:04:39,240 --> 00:04:48,760
a virtuous person, see the sampan, no. Nagasamos at Dhanang is like,

31
00:04:50,280 --> 00:04:59,400
and then he gives these 10 similes. One who is virtuous is like an antidote for destroying

32
00:04:59,400 --> 00:05:07,240
the poisons of the fountains and kings. So if you have this sickness of greed, anger, and

33
00:05:07,240 --> 00:05:17,240
delusion, what does the antidote? Where do you find the cure to the defilements of the mind?

34
00:05:18,920 --> 00:05:23,240
If you have an anger problem, if you have an addiction problem, what is the cure?

35
00:05:24,120 --> 00:05:31,160
The cure is a virtuous person, and by this he means an R-100. Someone who has understood the truth

36
00:05:31,160 --> 00:05:39,320
and who has freed themselves from all greed, anger, and delusion. You find such a person there,

37
00:05:39,320 --> 00:05:45,160
the antidote, and if you partake of them in terms of listening to their teaching,

38
00:05:46,040 --> 00:05:50,840
associating with them, listening to their teaching, remembering their teaching,

39
00:05:50,840 --> 00:05:56,360
understanding their teaching, and putting their teaching into practice, and you can cure yourself.

40
00:05:56,360 --> 00:06:06,680
They're the antidote. That's the first one. There are 10 similes. The second is they're a healing

41
00:06:06,680 --> 00:06:14,920
bomb for delaying the sickness of defilements and beings. So if you have the fever of defilements,

42
00:06:16,040 --> 00:06:21,160
defilements are seen as a fever, greed is a fever, it makes you hot and bothered.

43
00:06:21,160 --> 00:06:36,760
Anger is a fever, it burns you out, delusion is a fever, it makes you drunk and uncontrollable.

44
00:06:38,200 --> 00:06:44,120
But this is a healing bomb that cools and suits and heals your sickness.

45
00:06:44,120 --> 00:06:53,400
Number three is like a precious gem because they grant all beings their wishes.

46
00:06:54,760 --> 00:07:03,640
So there's this special gem in Indian mythology, a wishful filling gem. So this comes up in

47
00:07:03,640 --> 00:07:12,280
Buddhism often as a simile talking about the wishful filling jewel. This jewel, if you have this,

48
00:07:12,280 --> 00:07:18,200
it's like the genian is lamp, but in Indian mythology it's a gem and if you rub the gem or if you

49
00:07:18,200 --> 00:07:27,880
possess the gem all your wishes come true. And so in our hunt and lighten being a virtuous being

50
00:07:29,400 --> 00:07:37,560
is like this gem because they give people what they want happiness. Sometimes we don't even

51
00:07:37,560 --> 00:07:45,480
know what we want. We think we want something and obviously can't give you riches and so on and

52
00:07:45,480 --> 00:07:52,760
power, but they can give you what you need I guess you would say. We can't give you what you want

53
00:07:52,760 --> 00:08:00,920
necessarily. But what we want is always happiness. We want certain things because we think they

54
00:08:00,920 --> 00:08:07,800
will bring us happiness. We want satisfaction. We're just deluded into thinking that things that

55
00:08:07,800 --> 00:08:14,840
are unsatisfying are going to satisfy us. We're deluded into thinking that things that are

56
00:08:14,840 --> 00:08:19,400
impermanent and things that are uncontrollable are stable and controllable.

57
00:08:19,400 --> 00:08:32,840
But then lighten being when we follow their teachings, we find with this stable satisfying and

58
00:08:32,840 --> 00:08:40,200
controllable. Well, not controllable actually, but that one aside. Stable and satisfying anyway.

59
00:08:40,200 --> 00:08:50,200
That's number three. Number four is like a ship for beings to go beyond the four floods.

60
00:08:52,600 --> 00:08:55,960
So the floods are

61
00:08:55,960 --> 00:09:07,480
coming. I forget what are the four floods.

62
00:09:12,520 --> 00:09:17,240
Overcoming the defilements. Overcoming the asava.

63
00:09:17,240 --> 00:09:25,560
Aweja asava. Awejoga. I think that's it. So karma, sensuality, the flood of sensuality,

64
00:09:26,520 --> 00:09:29,880
Bhauga is becoming the flood of becoming.

65
00:09:29,880 --> 00:09:44,360
Titoga, I think the third one is views, flood of views. And the weitoga is the flood of

66
00:09:47,240 --> 00:09:52,600
ignorance. But in general, it's just another way of describing the

67
00:09:52,600 --> 00:09:59,000
defilements. But the idea of the imagery of the ocean is a common one, but some are as like an ocean

68
00:09:59,000 --> 00:10:09,000
that we all drown and we flounder around and never seeing the shore. We're just lost in this

69
00:10:09,000 --> 00:10:15,320
vast ocean of nothingness and meaninglessness until we find a ship that can sail us.

70
00:10:15,320 --> 00:10:22,920
Because if you're just swimming around in the ocean, you just go around in circles, you've got

71
00:10:22,920 --> 00:10:29,400
nowhere. But if you have a ship, you can cut through the ocean. You can go in whatever direction you

72
00:10:29,400 --> 00:10:40,280
like. You can find the shore. He is like he or she is like a caravan leader for taking beings

73
00:10:40,280 --> 00:10:47,800
across the desert of repeated births. So rebirth is like a desert. We're always thirsting. We're

74
00:10:47,800 --> 00:10:55,960
always hot with our defilements and our addictions and our aversion and the suffering that comes from

75
00:10:55,960 --> 00:11:04,120
old age, sickness and death. We suffer like being in a desert. But a caravan leader can lead us

76
00:11:04,120 --> 00:11:11,160
through this thing. He's like the wind. Here she is like the wind for extinguishing the three fierce

77
00:11:11,160 --> 00:11:18,840
fires and beings as his greed, anger and delusions. The wind, they come in and they douse the

78
00:11:18,840 --> 00:11:26,600
flyfire. They blow the fire out with the power of their teachings. They are like a great rain

79
00:11:26,600 --> 00:11:33,800
cloud for filling beings with good thoughts. So like a great rain cloud fills up the lakes and

80
00:11:33,800 --> 00:11:52,120
rivers, brings rain and waters to crops and cools the land. So to a great leader, a great teacher,

81
00:11:52,120 --> 00:11:58,440
a great being someone who has purified themselves. This is someone who is enlightened and why they

82
00:11:58,440 --> 00:12:11,880
should stay or stick around. Because they have so much to offer. The world is dry without enlightened

83
00:12:11,880 --> 00:12:19,640
beings without them. It's you can't grow anything. People don't grow spiritually. Not in any

84
00:12:19,640 --> 00:12:32,120
meaningful way. Just go around in circles. Dry up like crops without water. Like a teacher

85
00:12:32,120 --> 00:12:36,120
for encouraging beings to train themselves and when it's skilled, it's interesting that it's

86
00:12:36,120 --> 00:12:44,360
like a teacher. It's supposed to just being a teacher. Arjariya is a mom, but it's an interesting

87
00:12:44,360 --> 00:12:52,760
because in many ways the enlightened followers of the Buddha aren't considered teachers. We don't

88
00:12:52,760 --> 00:12:59,400
consider ourselves to be teachers per se or friends. We're good friends. Why? Because we give

89
00:12:59,400 --> 00:13:11,720
good things. We as in Buddhists who practice. We have good things to give.

90
00:13:11,720 --> 00:13:21,640
And so, I mean, enlightened being in Arahant has the greatest to give. And yeah, so we don't

91
00:13:21,640 --> 00:13:26,840
consider, we should never, even if one becomes enlightened, one would not consider oneself a teacher

92
00:13:27,560 --> 00:13:32,280
that rather a friend, someone who offers advice and passes on the Buddha's teaching.

93
00:13:33,240 --> 00:13:38,760
But they're like a teacher because they do the same. They might as well be called the teacher

94
00:13:38,760 --> 00:13:47,800
because they encourage beings to train themselves. And finally, like a good guide for pointing

95
00:13:47,800 --> 00:13:56,600
out the beings, the path to security. So we're lost. We're so lost. We're going in the wrong

96
00:13:56,600 --> 00:14:07,080
direction. We're running around in circles. You can't find the way out. You have ever been in

97
00:14:07,080 --> 00:14:13,320
the large forest. It's quite scary. You see to get lost. Easy to lose your direction. And you're

98
00:14:13,320 --> 00:14:20,120
constantly misjudging. So it's very easy to go around in circles because you think, oh, maybe

99
00:14:20,120 --> 00:14:23,720
you have to go a little bit to the left and you constantly think that until you wind up back.

100
00:14:24,360 --> 00:14:29,960
I did this once. I'm not back where I started because I actually walked into circle.

101
00:14:29,960 --> 00:14:36,280
It's very hard to get out of the forest if you don't know where you're going. You've never been

102
00:14:36,280 --> 00:14:43,160
there before if you're in a direction. You don't have a guide. But someone who has been through

103
00:14:43,160 --> 00:14:49,160
the forest who knows the forest and who knows the way out of the forest can guide you out of the

104
00:14:49,160 --> 00:15:04,360
forest. So that's our demo for tonight. Reasons why not to kill yourself. It seems like killing

105
00:15:04,360 --> 00:15:10,760
yourself wouldn't be, it can be more easily explained it potentially by just the idea that

106
00:15:11,800 --> 00:15:14,440
an enlightened being would have no reason to kill themselves.

107
00:15:14,440 --> 00:15:23,320
Killing yourself would require desire for it to all end. So it seems like an hour

108
00:15:23,320 --> 00:15:29,320
hunt wouldn't have any desire to stick around, but they would just stick around and wait,

109
00:15:29,320 --> 00:15:37,160
you know, let things work themselves out. But it seems like there's something a little more here,

110
00:15:37,160 --> 00:15:44,200
like the idea that they do things. They eat arms, for example, why they don't just stop eating.

111
00:15:45,160 --> 00:15:55,080
They take arms because of this, because of some sense of duty or some rightness to passing on

112
00:15:55,080 --> 00:16:03,720
what they have gained. And it seems they do our hunts seem to teach and pass on the teaching.

113
00:16:03,720 --> 00:16:12,120
Rather than just kill themselves. Hey, we got a whole bunch of people online. Good to see. Good

114
00:16:12,120 --> 00:16:22,520
crowd. Thank you all for showing up. So it looks like the iOS app is still in the works. We're

115
00:16:22,520 --> 00:16:29,000
going to try to, I'm going to try to proactively revamp this website. We've got a new version of it,

116
00:16:29,000 --> 00:16:35,400
but I just haven't put any effort into getting it online. So I'm going to work with the person.

117
00:16:37,000 --> 00:16:42,920
But that also means probably that the iOS app has to be fixed for the new server. I'm not sure.

118
00:16:43,480 --> 00:16:49,720
I'm going to have to work together to make this all, get it secure as well. The server has to,

119
00:16:49,720 --> 00:16:56,600
our whole server has to, apparently has to have some kind of, you know, the HTTPS thing.

120
00:16:56,600 --> 00:17:05,240
It's not, it's a bit beyond my pig grade, but somebody knows how to do it. So we got all work

121
00:17:05,240 --> 00:17:12,360
together to make this happen. If anybody wants to volunteer in our organization in any way,

122
00:17:12,360 --> 00:17:20,200
we're always looking for volunteers. I think, I think we are. Anyway, get in touch and if we

123
00:17:20,200 --> 00:17:26,760
need volunteers, we'll let you know as usual. As per last night, we're still looking for a steward

124
00:17:27,960 --> 00:17:36,120
as if anybody wants to come and stay with us for a while. And starting in July,

125
00:17:39,320 --> 00:17:41,080
that'd be great. Let us know.

126
00:17:41,080 --> 00:17:50,600
Right. So we got a couple of questions. Does anyone know what are the five

127
00:17:50,600 --> 00:18:01,160
scondas? We call them pandas in Pali. Pandas are aggregates. And these are root by the form,

128
00:18:01,160 --> 00:18:10,680
Waidana means feeling, sun yam means recognition, and so on. Sankara is

129
00:18:13,960 --> 00:18:21,880
mental formations. And Wignana is consciousness. So when you see something, there is the physical

130
00:18:21,880 --> 00:18:29,080
aspect. There is the Waidana, the feeling about it, pleasant, painful, neutral. There is a recognition,

131
00:18:29,080 --> 00:18:35,960
you recognize it as a cat or so on that you see. There's the mental formations means you judge it

132
00:18:35,960 --> 00:18:43,240
or you like it or dislike it or so on. You examine it and you react to it. And Wignana just

133
00:18:43,240 --> 00:19:01,400
means the consciousness that the faculty are aware. What does unsystematic intention mean?

134
00:19:03,160 --> 00:19:07,640
Unsystematic is just a loose translation. That's not really what the, I think that's

135
00:19:07,640 --> 00:19:16,600
re-referring to Aioni so Manasikara. Aioni so means Y is Aioni so means unwise. So when you observe

136
00:19:16,600 --> 00:19:22,600
something and you react to it, this is when you like it or dislike it, when you let yourself get

137
00:19:22,600 --> 00:19:29,240
caught up in reactions to it. If you've only so Manasikara means you see it wisely,

138
00:19:29,240 --> 00:19:32,360
you see it as impermanent suffering in non-self. You see it as it is,

139
00:19:32,360 --> 00:19:38,520
impermanent unsatisfying and controllable. You don't get attached to it. You see it objectively,

140
00:19:38,520 --> 00:19:44,920
just as something that arises and sees it. That's why is attention. But why is this probably

141
00:19:44,920 --> 00:19:59,400
better than systematic? Are all fears instances of instances of Dosa? Yeah, Bioniana

142
00:19:59,400 --> 00:20:04,520
isn't doesn't fear according to the Wisudimanga. Bioniana doesn't fear because fear is always

143
00:20:04,520 --> 00:20:09,720
associated with patika. Actually, it's not the Wisudimanga. I think it's the commentary

144
00:20:09,720 --> 00:20:15,880
that would be the sub-commentary that Wisudimanga asks, does Bioniana fear? No, it doesn't.

145
00:20:19,240 --> 00:20:23,880
I think maybe the Wisudimanga is the one that says no, it doesn't fear. And then the

146
00:20:23,880 --> 00:20:30,280
commentary to the Wisudimanga says, why doesn't it fear? Because fear is always associated with

147
00:20:30,280 --> 00:20:39,160
patika with aversion. So it's not really fear. Bioniana isn't fear. Bioniana is seeing the danger.

148
00:20:40,120 --> 00:20:43,400
When you see the danger and being reborn, you see the danger and

149
00:20:45,240 --> 00:20:52,280
in attachment, that kind of thing. It's like when you see fire off in the distance,

150
00:20:52,280 --> 00:20:56,760
with the Wisudimanga says, you see fire off in the distance, you think, wow, if anyone

151
00:20:56,760 --> 00:21:02,120
falls into that fire, they're going to burn themselves. But you yourself don't, you're not afraid

152
00:21:02,120 --> 00:21:09,640
because it's all over there. So Nioniana itself isn't afraid. But during meditation,

153
00:21:09,640 --> 00:21:14,200
of course, you can be afraid. It's just that the fear isn't Nioniana the knowledge.

154
00:21:14,200 --> 00:21:24,920
These are for Canada six months. Usually you can come to Canada for six months. If you want

155
00:21:24,920 --> 00:21:30,520
to extend it, I think there are ways to extend it, but there's probably a lot of paperwork involved.

156
00:21:30,520 --> 00:21:44,360
I just got back from New York today. We drove since morning.

157
00:21:52,440 --> 00:21:57,000
Saturday, Sunday, we've got something at Cambodian monastery.

158
00:21:57,000 --> 00:22:03,240
We might have another meditator tomorrow, we'll see, but I'm also going to try to make it over

159
00:22:03,240 --> 00:22:07,560
to my father's house. We'll see how that works. I'm going to work that.

160
00:22:08,600 --> 00:22:14,600
And I'll be around next weekend is ways that don't forget, Mr. Saugai, if you're in the area,

161
00:22:14,600 --> 00:22:28,600
Saturday the 28th, come on out.

162
00:22:28,600 --> 00:22:39,400
Are there any other questions?

163
00:22:39,400 --> 00:23:01,880
When a monk or aunt or so forth passes on, would they be mindful like when falling asleep?

164
00:23:01,880 --> 00:23:07,320
Are you trying to be mindful of each moment in a moment of transition?

165
00:23:09,160 --> 00:23:11,480
On our hand, when they pass on, they aren't born again.

166
00:23:15,880 --> 00:23:20,360
As for a monk, well, monks can be of course corrupt. So

167
00:23:23,640 --> 00:23:25,640
doesn't say anything just because someone is a monk.

168
00:23:25,640 --> 00:23:33,160
Let's ask if a meditator would be mindful like when falling asleep, not like when falling

169
00:23:33,160 --> 00:23:38,520
asleep because death isn't like falling asleep. Death is like an out of body experience.

170
00:23:39,400 --> 00:23:46,760
The body stops working so the mind leaves the body, stops working with the body.

171
00:23:46,760 --> 00:23:56,040
Sometimes it takes a lot, but the mind eventually is over a period of time

172
00:23:57,800 --> 00:24:03,880
sees that the body is, it's reacts to the fact that the body is no longer working properly

173
00:24:03,880 --> 00:24:08,840
and that's why it leaves. So you have either a near-death experience, if the body starts working,

174
00:24:08,840 --> 00:24:12,520
you have a death experience, if the body doesn't ever stop, start working.

175
00:24:12,520 --> 00:24:17,240
And then the mind goes on and does whatever, maybe back to be born again.

176
00:24:19,320 --> 00:24:25,240
But if your mind foot, just this process becomes a lot smoother and you're less likely to

177
00:24:25,240 --> 00:24:31,480
make improper choices as with everything in life. During the time of death, it's

178
00:24:33,640 --> 00:24:38,840
probably pretty quick. It's a lot that goes on and if you're not mindful, it's easy to get lost

179
00:24:38,840 --> 00:24:50,840
and go the wrong way. The 20th here is actually waste, the 20th in Thailand, I think, is wasted,

180
00:24:50,840 --> 00:24:56,920
maybe the 21st, first some places, but the 28th is when we're having our celebration here.

181
00:24:56,920 --> 00:25:08,920
So it's just a big thing here in Canada, an Ontario.

182
00:25:21,640 --> 00:25:25,400
Okay, well, I'm going to go spend a bit of a long day.

183
00:25:25,400 --> 00:25:33,960
But I'm trying, well, this next little while is going to be a little bit erratic, but for the next

184
00:25:33,960 --> 00:25:40,680
week or so, we should be fairly stable. So I'll be back again tomorrow, most likely.

185
00:25:40,680 --> 00:25:56,600
Anyway, have a good night, everyone. See you well.

