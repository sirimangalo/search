1
00:00:00,000 --> 00:00:02,480
We'll welcome back to Ask a Monk.

2
00:00:02,480 --> 00:00:06,440
Next question comes from Rolisk, who asks,

3
00:00:06,440 --> 00:00:09,760
how should one go about fixing on properly letting go

4
00:00:09,760 --> 00:00:12,840
of past, present, and future phenomena,

5
00:00:12,840 --> 00:00:16,760
especially events or people who have had a huge impact

6
00:00:16,760 --> 00:00:17,600
on your mind?

7
00:00:23,600 --> 00:00:27,880
The one thing about Buddhist practice and the path

8
00:00:27,880 --> 00:00:33,320
that we're on, that has to be understood,

9
00:00:33,320 --> 00:00:37,040
has to be emphasized again and again,

10
00:00:37,040 --> 00:00:39,400
is that it's a gradual teaching.

11
00:00:39,400 --> 00:00:42,920
There are people who are really ready to become enlightened

12
00:00:42,920 --> 00:00:45,240
and with just a little push, they're

13
00:00:45,240 --> 00:00:48,480
able to let go, they're able to free their minds.

14
00:00:48,480 --> 00:00:53,560
But that's because of the immense work

15
00:00:53,560 --> 00:00:59,080
that they've done in past lives, in their past.

16
00:00:59,080 --> 00:01:03,480
They're in a different plane, so to speak.

17
00:01:06,360 --> 00:01:09,880
If you still have these really strong attachments,

18
00:01:09,880 --> 00:01:17,160
which most of us do, and in fact, most of our problems

19
00:01:17,160 --> 00:01:26,400
are of this nature, then you have to have patience.

20
00:01:26,400 --> 00:01:32,880
You have to be able to see it as a long-term practice

21
00:01:32,880 --> 00:01:37,720
of changing really the core of who you are.

22
00:01:37,720 --> 00:01:44,760
You have to see that the mind is much, much more complex,

23
00:01:44,760 --> 00:01:52,360
deep, and messed up, really, than we realize,

24
00:01:52,360 --> 00:01:54,480
that when you first start practicing,

25
00:01:54,480 --> 00:01:56,320
it seems quite simple, and it is quite simple.

26
00:01:56,320 --> 00:01:58,520
You're dealing with very raw emotions,

27
00:01:58,520 --> 00:02:02,840
very coarse and crude mental problems

28
00:02:02,840 --> 00:02:06,000
that we have, very intense states of hatred,

29
00:02:06,000 --> 00:02:09,840
intense states of addiction, intense states of ego and delusion

30
00:02:09,840 --> 00:02:13,240
and ignorance and so on.

31
00:02:13,240 --> 00:02:19,000
But as you go on, the objects of your attachment,

32
00:02:19,000 --> 00:02:21,000
both positive and negative attachment,

33
00:02:21,000 --> 00:02:23,560
are not going to change necessarily.

34
00:02:23,560 --> 00:02:27,520
They're going to become weaker.

35
00:02:27,520 --> 00:02:30,200
The attachment is going to become weaker.

36
00:02:30,200 --> 00:02:38,160
So dealing with the very strong issues in our life

37
00:02:38,160 --> 00:02:40,240
has to go in stages, and the first stage

38
00:02:40,240 --> 00:02:43,520
is knowing that we're going to be in this for the long haul,

39
00:02:43,520 --> 00:02:47,080
not expecting the quick fix, because quick fixes

40
00:02:47,080 --> 00:02:55,280
are usually what destroys a person.

41
00:02:55,280 --> 00:02:58,840
If a person gets into drugs, gets into medication,

42
00:02:58,840 --> 00:03:02,520
even it's really what ends your spiritual life,

43
00:03:02,520 --> 00:03:05,640
a person on medication has a very difficult time

44
00:03:05,640 --> 00:03:07,760
on the spiritual path.

45
00:03:07,760 --> 00:03:10,800
And so even the most difficult, like, for instance,

46
00:03:10,800 --> 00:03:14,080
people who have schizophrenia, some people have asked me,

47
00:03:14,080 --> 00:03:17,720
I don't think I've gotten to these questions yet.

48
00:03:17,720 --> 00:03:21,520
And I'll try my best to.

49
00:03:21,520 --> 00:03:23,480
People with these, these are problems

50
00:03:23,480 --> 00:03:26,440
that probably can't be totally sorted out,

51
00:03:26,440 --> 00:03:28,240
because in many ways they're organic

52
00:03:28,240 --> 00:03:32,400
and have to do with the structure of the brain

53
00:03:32,400 --> 00:03:35,040
on the one hand.

54
00:03:35,040 --> 00:03:38,320
So we can look, but we can really look at all of our problems

55
00:03:38,320 --> 00:03:41,000
in this way, that we may not work them all out

56
00:03:41,000 --> 00:03:42,960
even in this lifetime.

57
00:03:42,960 --> 00:03:46,000
That's just the fact, there's nothing you can do about that.

58
00:03:46,000 --> 00:03:49,960
It may be that you're not going to solve all your problems

59
00:03:49,960 --> 00:03:52,840
even in this lifetime.

60
00:03:52,840 --> 00:03:58,560
Now, that being said, I think this leads people,

61
00:03:58,560 --> 00:04:00,680
this sort of teaching leads people to get lazy

62
00:04:00,680 --> 00:04:07,680
on the other hand, that we should work to the best of our ability

63
00:04:07,680 --> 00:04:09,840
to get rid of these things as quickly as possible.

64
00:04:09,840 --> 00:04:12,240
The Buddha said, as though your head were on fire,

65
00:04:12,240 --> 00:04:20,040
or you were shot with an arrow, what should you do?

66
00:04:20,040 --> 00:04:24,760
If your hair is on fire, we'll put it out as quickly

67
00:04:24,760 --> 00:04:25,480
as possible.

68
00:04:25,480 --> 00:04:27,720
If you're shot with an arrow, what you do pull it out

69
00:04:27,720 --> 00:04:30,160
as quickly as possible.

70
00:04:30,160 --> 00:04:31,280
There's no hesitation.

71
00:04:31,280 --> 00:04:33,560
There's no saying, well, it's OK.

72
00:04:33,560 --> 00:04:35,360
It might take a while for this to heal,

73
00:04:35,360 --> 00:04:37,200
so why pull it out right away or so.

74
00:04:37,200 --> 00:04:38,840
I mean, pull it out.

75
00:04:38,840 --> 00:04:42,640
And then your head is on fire.

76
00:04:42,640 --> 00:04:46,080
What are you waiting for?

77
00:04:46,080 --> 00:04:47,920
The Buddha said, in the same way we should work

78
00:04:47,920 --> 00:04:51,640
to get rid of our wrong views.

79
00:04:51,640 --> 00:04:53,520
And this is an interesting teaching as well,

80
00:04:53,520 --> 00:04:56,680
because this is the second stage.

81
00:04:56,680 --> 00:04:59,880
The first stage is realizing, not having

82
00:04:59,880 --> 00:05:02,600
expectations that we should somehow

83
00:05:02,600 --> 00:05:06,040
be able to, as you say, fix on and let go.

84
00:05:06,040 --> 00:05:08,680
It doesn't really work that way.

85
00:05:08,680 --> 00:05:13,280
You can't just say, OK, I understood that situation.

86
00:05:13,280 --> 00:05:14,720
And this happens with meditators.

87
00:05:14,720 --> 00:05:18,000
They go on a retreat for 10 days, 20 days, even a month.

88
00:05:18,000 --> 00:05:21,040
And they think, wow, I really got rid of all those

89
00:05:21,040 --> 00:05:21,720
defilements.

90
00:05:21,720 --> 00:05:23,680
And then they go back to life.

91
00:05:23,680 --> 00:05:26,240
And the defilements come right back.

92
00:05:26,240 --> 00:05:28,720
All of the same defilements come back.

93
00:05:28,720 --> 00:05:30,280
And this can be quite discouraging,

94
00:05:30,280 --> 00:05:32,600
because they think, well, then I gain nothing from the practice.

95
00:05:32,600 --> 00:05:38,360
But the point is, which you gain, on the one hand,

96
00:05:38,360 --> 00:05:42,600
you gain the defilements are lessened.

97
00:05:42,600 --> 00:05:46,240
And this is the point about that they're not

98
00:05:46,240 --> 00:05:49,480
going to go away all at once.

99
00:05:49,480 --> 00:05:52,640
You can't address a problem and say it's finished,

100
00:05:52,640 --> 00:05:53,680
because it's going to come back.

101
00:05:53,680 --> 00:05:56,080
But next time it will be weaker, your attachment to it

102
00:05:56,080 --> 00:05:57,480
will be weaker.

103
00:05:57,480 --> 00:06:00,600
And you'll find this over the years

104
00:06:00,600 --> 00:06:03,000
that you have the same attachments, the same addictions,

105
00:06:03,000 --> 00:06:05,640
the same aversion, the same angers.

106
00:06:05,640 --> 00:06:10,080
And they're all just as they were before, but weaker.

107
00:06:10,080 --> 00:06:12,400
And they don't have as much of a hold on you

108
00:06:12,400 --> 00:06:14,920
until they're finally done away with.

109
00:06:14,920 --> 00:06:18,520
But one thing that changes for sure,

110
00:06:18,520 --> 00:06:20,280
usually through intensive practice,

111
00:06:20,280 --> 00:06:25,840
but it can change through gradual, through daily practice.

112
00:06:25,840 --> 00:06:28,920
But definitely, I recommend the best thing for you

113
00:06:28,920 --> 00:06:32,480
to do is to undertake an intensive meditation course.

114
00:06:32,480 --> 00:06:34,840
That's the best way to get started on the Buddhist path.

115
00:06:34,840 --> 00:06:37,360
From my point of view, that's how I started.

116
00:06:37,360 --> 00:06:38,440
I didn't learn anything.

117
00:06:38,440 --> 00:06:39,200
I didn't study.

118
00:06:39,200 --> 00:06:42,280
I didn't really know hardly anything about Buddhism.

119
00:06:42,280 --> 00:06:43,920
And I went to practice meditation.

120
00:06:43,920 --> 00:06:50,080
I think it was a good luck for me,

121
00:06:50,080 --> 00:06:52,040
because I didn't have any preconceived notions.

122
00:06:52,040 --> 00:06:59,280
And I went in, and I just learned and came out with some direct

123
00:06:59,280 --> 00:07:04,400
understanding to whatever extent.

124
00:07:04,400 --> 00:07:08,120
So the one thing that does go away if you do this,

125
00:07:08,120 --> 00:07:10,040
if you undertake intensive practice,

126
00:07:10,040 --> 00:07:13,040
and if your diligent in it is wrong view.

127
00:07:13,040 --> 00:07:15,400
And so this is a Buddhist said we should work hard to get rid

128
00:07:15,400 --> 00:07:15,640
of.

129
00:07:15,640 --> 00:07:18,720
He didn't say we should work to get rid of our greed

130
00:07:18,720 --> 00:07:19,840
and anger right away.

131
00:07:19,840 --> 00:07:22,040
But he said, get rid of your wrong view.

132
00:07:22,040 --> 00:07:24,240
You're wrong understanding.

133
00:07:24,240 --> 00:07:27,480
And this is the wrong understanding in regards to these problems

134
00:07:27,480 --> 00:07:31,440
that it's me, that I was hurt, that I was injured,

135
00:07:31,440 --> 00:07:33,320
that this person.

136
00:07:33,320 --> 00:07:36,280
Now, you'll have thoughts about things you're talking specifically

137
00:07:36,280 --> 00:07:38,280
about something that happened in the past.

138
00:07:38,280 --> 00:07:39,360
You'll have a thought about that.

139
00:07:39,360 --> 00:07:43,400
And there's this big, like a car crash that happens in your mind.

140
00:07:43,400 --> 00:07:47,120
And you put that together as a person, the other person,

141
00:07:47,120 --> 00:07:50,000
and me, and you feel all sorts of guilt,

142
00:07:50,000 --> 00:07:54,280
and maybe ego, and self-righteousness, or whatever.

143
00:07:54,280 --> 00:07:58,080
You can feel jealousy, you can feel so many different emotions.

144
00:07:58,080 --> 00:08:04,120
But they all come from this attachment to, not all,

145
00:08:04,120 --> 00:08:09,720
but many of them only come after the attachment to it

146
00:08:09,720 --> 00:08:13,760
as me, as I was hurt by this person,

147
00:08:13,760 --> 00:08:19,360
or I hurt this person, or this person is gone,

148
00:08:19,360 --> 00:08:21,720
someone who we loved, who's passed away,

149
00:08:21,720 --> 00:08:26,080
who's left, who's changed, and so on.

150
00:08:26,080 --> 00:08:29,840
Once we can let go of this idea of self,

151
00:08:29,840 --> 00:08:33,320
this idea that it was me, and even the idea

152
00:08:33,320 --> 00:08:36,760
that that situation somehow exists, because that's really it.

153
00:08:36,760 --> 00:08:41,360
We believe this situation somehow is an entity.

154
00:08:41,360 --> 00:08:44,400
So this problem, I can't stop thinking about this person,

155
00:08:44,400 --> 00:08:48,200
that person, what they did to me, or the great times we had,

156
00:08:48,200 --> 00:08:50,800
or so on.

157
00:08:50,800 --> 00:08:53,680
You realize, instead, that it's simply

158
00:08:53,680 --> 00:08:57,240
a thought that arises in the mind, that when you're thinking

159
00:08:57,240 --> 00:08:59,360
about the past, there's no past.

160
00:08:59,360 --> 00:09:01,440
The past doesn't come up, although there

161
00:09:01,440 --> 00:09:03,840
is a thought in your mind, and there

162
00:09:03,840 --> 00:09:07,080
are several powerful emotions that could arise.

163
00:09:07,080 --> 00:09:10,920
It couldn't be anger, it could be greed.

164
00:09:10,920 --> 00:09:13,480
And so those often will come up automatically.

165
00:09:13,480 --> 00:09:20,400
But once you put the view on it, that, oh, I,

166
00:09:20,400 --> 00:09:24,240
you have this attachment to it as my experience,

167
00:09:24,240 --> 00:09:27,240
and as an experience, then you'll come up with all these things,

168
00:09:27,240 --> 00:09:31,600
about how I miss this person, and oh, I love them,

169
00:09:31,600 --> 00:09:34,040
and oh, they were the idea of them,

170
00:09:34,040 --> 00:09:37,600
and they were so great to me, or they were so awful to me,

171
00:09:37,600 --> 00:09:39,480
and so on.

172
00:09:39,480 --> 00:09:45,120
So instead of thinking of it as the experience in the past,

173
00:09:45,120 --> 00:09:48,720
some entity, think of it as what is going on right now.

174
00:09:48,720 --> 00:09:52,600
There is the thought process, and there

175
00:09:52,600 --> 00:09:54,280
is the emotive process.

176
00:09:54,280 --> 00:09:57,120
There's the physical sensations that come along

177
00:09:57,120 --> 00:10:00,400
with that headaches, or the fast-beating heart,

178
00:10:00,400 --> 00:10:06,160
or the tension, or so on, that goes along with it.

179
00:10:06,160 --> 00:10:11,000
This is the first real change that occurs,

180
00:10:11,000 --> 00:10:16,000
and the first step, really, in affecting a change.

181
00:10:16,000 --> 00:10:20,200
If you can work on getting rid of this,

182
00:10:20,200 --> 00:10:21,760
it's much more important.

183
00:10:21,760 --> 00:10:24,840
Work on changing the way you look at the situation,

184
00:10:24,840 --> 00:10:29,360
to simply see it as a risen desires, a risen aversion.

185
00:10:29,360 --> 00:10:33,800
Then whenever it comes up, it's not going to stick.

186
00:10:33,800 --> 00:10:38,120
You get angry, you get greedy, or you're longing

187
00:10:38,120 --> 00:10:40,680
for the past, or so on, longing for the future,

188
00:10:40,680 --> 00:10:43,880
angry, or worried, or scared, or so on.

189
00:10:43,880 --> 00:10:44,760
And it goes away.

190
00:10:44,760 --> 00:10:45,840
You don't think of it as I.

191
00:10:45,840 --> 00:10:48,040
You don't think of it as a problem.

192
00:10:48,040 --> 00:10:51,840
You don't give rise to this worry and this stress.

193
00:10:51,840 --> 00:10:53,800
What am I going to do about this?

194
00:10:53,800 --> 00:10:56,400
Once you identify with it, then it becomes your problem.

195
00:10:56,400 --> 00:10:58,360
It's something you have to deal with.

196
00:10:58,360 --> 00:11:00,240
But if you see it simply for what it is,

197
00:11:00,240 --> 00:11:03,240
this is why we have this mantra that we use, right?

198
00:11:03,240 --> 00:11:05,920
If you are angry, you say to yourself,

199
00:11:05,920 --> 00:11:10,200
angry, if you're worried, worried, because it changes

200
00:11:10,200 --> 00:11:11,320
the way you look at it.

201
00:11:11,320 --> 00:11:13,280
It gets rid of that nonsense of I

202
00:11:13,280 --> 00:11:16,000
and the attachment and the identifying.

203
00:11:16,000 --> 00:11:17,800
And it's simply something that has arisen.

204
00:11:17,800 --> 00:11:20,040
Oh, there's anger.

205
00:11:20,040 --> 00:11:22,320
And then it's just anger, and it's coming and coming

206
00:11:22,320 --> 00:11:24,240
and coming, and then it's gone.

207
00:11:24,240 --> 00:11:27,120
And if you can practice in this way,

208
00:11:27,120 --> 00:11:29,880
you'll eventually realize that that's really it.

209
00:11:29,880 --> 00:11:31,600
It just came and went.

210
00:11:31,600 --> 00:11:36,880
And then it rises this so wet and you let go.

211
00:11:36,880 --> 00:11:41,600
You have no reason to worry or to cling to the situation

212
00:11:41,600 --> 00:11:42,600
at all.

213
00:11:42,600 --> 00:11:46,080
And that's when it starts to recede.

214
00:11:46,080 --> 00:11:50,880
When you stop giving it power, when you stop feeding it

215
00:11:50,880 --> 00:11:57,720
and you stop re-reifying the situation where the tension

216
00:11:57,720 --> 00:12:00,080
the stress comes up and you identify with it

217
00:12:00,080 --> 00:12:04,440
and you say, yes, yes, yes, I'm tense and the terrible anger

218
00:12:04,440 --> 00:12:06,160
and you re-ify it.

219
00:12:06,160 --> 00:12:07,320
There was anger that came up.

220
00:12:07,320 --> 00:12:09,280
You apply more anger on top of it.

221
00:12:09,280 --> 00:12:12,600
And you say, yes, yes, I'm so angry about that.

222
00:12:12,600 --> 00:12:15,000
And then it goes back down and it will come again

223
00:12:15,000 --> 00:12:17,440
and get worse and worse and worse.

224
00:12:17,440 --> 00:12:20,400
And you do the opposite now once you start to get bored of it

225
00:12:20,400 --> 00:12:23,560
and you start to see, yeah, there's anger.

226
00:12:23,560 --> 00:12:25,240
Oh, yeah, anger.

227
00:12:25,240 --> 00:12:29,200
You start to realize, this is what I've developed

228
00:12:29,200 --> 00:12:32,480
over the years and this has a cause.

229
00:12:32,480 --> 00:12:35,960
It's because of my re-ifying and so on.

230
00:12:35,960 --> 00:12:39,040
And it's not that realization that's important.

231
00:12:39,040 --> 00:12:42,360
What's important is just realizing it is what it is.

232
00:12:42,360 --> 00:12:43,920
It's just anger.

233
00:12:43,920 --> 00:12:48,160
Once you see that all you were doing before is re-ifying it

234
00:12:48,160 --> 00:12:52,360
and magnifying it, then you don't do that anymore.

235
00:12:52,360 --> 00:12:55,000
You see that that's the wrong way and you just watch it.

236
00:12:55,000 --> 00:12:57,800
And once you are able to start watching it,

237
00:12:57,800 --> 00:13:00,440
then it will slowly slowly go away.

238
00:13:00,440 --> 00:13:03,040
So what I'd like to stress is that one,

239
00:13:03,040 --> 00:13:07,080
it's not going to go away overnight, not likely.

240
00:13:07,080 --> 00:13:10,520
If it does go overnight, then it's going to take one

241
00:13:10,520 --> 00:13:12,840
intense night of meditation.

242
00:13:12,840 --> 00:13:15,520
And I don't don't drive it.

243
00:13:15,520 --> 00:13:18,640
But for some people, it's possible that they might be able

244
00:13:18,640 --> 00:13:21,080
to realize the truth.

245
00:13:21,080 --> 00:13:23,640
It can happen very quickly, at least

246
00:13:23,640 --> 00:13:27,880
this stage of realizing the difference,

247
00:13:27,880 --> 00:13:32,040
realizing that our wrong understanding,

248
00:13:32,040 --> 00:13:34,800
once we realized that we're looking at things wrong,

249
00:13:34,800 --> 00:13:40,920
they were re-ifying, we're creating an identity

250
00:13:40,920 --> 00:13:45,520
and we're attaching and identifying with the experience.

251
00:13:45,520 --> 00:13:51,240
And when we're able to see reality as simply arising

252
00:13:51,240 --> 00:13:54,800
and ceasing, this can happen quite quickly.

253
00:13:54,800 --> 00:14:01,400
And it leads to an experience of freedom

254
00:14:01,400 --> 00:14:04,280
and it takes your mind to another level.

255
00:14:04,280 --> 00:14:07,400
After that, then it's just simply a matter of time

256
00:14:07,400 --> 00:14:13,440
as you work to slowly, slowly untangle the attachments.

257
00:14:13,440 --> 00:14:20,320
And as they slowly slowly go away by themselves,

258
00:14:20,320 --> 00:14:24,440
and through practice are worked out

259
00:14:24,440 --> 00:14:27,440
and then the knots are untied and so on.

260
00:14:27,440 --> 00:14:29,520
So understanding that, first of all,

261
00:14:29,520 --> 00:14:31,520
that it's not going to go away quickly.

262
00:14:31,520 --> 00:14:34,200
And second of all, understanding that the most important

263
00:14:34,200 --> 00:14:37,360
is not to get rid of our strong emotions,

264
00:14:37,360 --> 00:14:39,120
but to change the way we look at them,

265
00:14:39,120 --> 00:14:43,640
instead of re-ifying and identifying with them,

266
00:14:43,640 --> 00:14:47,800
seeing them simply as emotions, simply as realities

267
00:14:47,800 --> 00:14:51,120
that have arisen and will cease, that they come and go,

268
00:14:51,120 --> 00:14:54,760
that clinging to them is not going to satisfy,

269
00:14:54,760 --> 00:14:58,160
it's not going to bring happiness or resolution,

270
00:14:58,160 --> 00:15:03,160
that you can't fix it, there's nothing you can do

271
00:15:04,000 --> 00:15:09,000
to make it better or to set up some state of existence,

272
00:15:11,480 --> 00:15:14,640
state of being, that's going to be perfect.

273
00:15:14,640 --> 00:15:18,520
Realizing that it's out of your control,

274
00:15:18,520 --> 00:15:22,120
it's not something that is stable and lasting

275
00:15:22,120 --> 00:15:26,400
and controllable.

276
00:15:26,400 --> 00:15:29,360
And when you do that, you start to let go,

277
00:15:29,360 --> 00:15:31,880
you start to say that this is not a process

278
00:15:31,880 --> 00:15:36,880
or a system that is worth getting involved in.

279
00:15:38,520 --> 00:15:41,000
And so you let go of these attachments

280
00:15:41,000 --> 00:15:42,520
and that happens slowly.

281
00:15:42,520 --> 00:15:45,360
So, okay, so basically two things.

282
00:15:45,360 --> 00:15:47,360
And I hope that was clear.

283
00:15:47,360 --> 00:16:13,320
Thanks for the questions, all the best.

