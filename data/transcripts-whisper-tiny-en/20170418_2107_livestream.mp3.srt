1
00:00:00,000 --> 00:00:13,360
Livestream isn't working, audio isn't working, oh no it's working, yeah that's up as well.

2
00:00:13,360 --> 00:00:21,820
I don't know if I'm going to be what day is it today, 18th, I'm going to be away for the

3
00:00:21,820 --> 00:00:31,420
first half of me, so I think in maybe I'll just take a break for a while, there's no more meditators here and

4
00:00:31,420 --> 00:00:58,300
everyone's gone home, so I don't know does anyone have any questions, yeah I think I'll take a

5
00:00:58,300 --> 00:01:08,540
little while off school's finished, I think about enough of school for a while, and I'm going to go

6
00:01:08,540 --> 00:01:15,420
visit my parents for a couple weeks, my father, I'm going to go visit Minna Tullan Island where I was

7
00:01:15,420 --> 00:01:25,380
born, hopefully, and then mid-May we got meditators coming again and we'll get back into broadcasting as

8
00:01:25,380 --> 00:01:35,700
well, I'm just studying a few classes of courses at McMaster, I thought that going to the

9
00:01:35,700 --> 00:01:44,900
University would help me find a community and find people interested in meditation but it wasn't as

10
00:01:44,900 --> 00:01:52,500
successful as I'd hoped, so and the idea of studying some things, I mean, well the only really

11
00:01:52,500 --> 00:01:58,100
useful thing that came out of it that I can think of is French and Latin, I learned a couple

12
00:01:58,100 --> 00:02:02,580
languages, French especially I suppose, I don't know what Latin is going to do for me, but

13
00:02:02,580 --> 00:02:21,060
languages are always useful, I think let's leave the questions on the site until I can do a

14
00:02:21,060 --> 00:02:38,820
YouTube video about them, just tell it to stop, when you're swaying back and forth, just say stop,

15
00:02:38,820 --> 00:02:48,020
make it stop, there's what's called nicanti, this subtle enjoyment of it, reinforcement of an

16
00:02:48,020 --> 00:02:53,220
image, more comfortable to be swaying, to have to sit through the pain and the mental stress,

17
00:02:54,020 --> 00:02:59,140
it's quite comfortable to sway back and forth, so you have to tell it to stop and grow up

18
00:03:00,980 --> 00:03:04,900
and face the ordinary experiences,

19
00:03:04,900 --> 00:03:24,740
you know, meaning you have to make a determination to stop it, so that you don't subconsciously

20
00:03:24,740 --> 00:03:40,500
encourage it,

