1
00:00:00,000 --> 00:00:04,500
In sitting meditation is it okay to lean forward when it feels like you're

2
00:00:04,500 --> 00:00:09,280
falling back, even though you are averting some pain. But I do it mindful

3
00:00:09,280 --> 00:00:13,780
noting everything, or should I just force the position.

4
00:00:16,280 --> 00:00:22,680
Well, it's different from avoiding pain is different from trying to keep your

5
00:00:22,680 --> 00:00:25,920
sitting position. So if you start to fall backwards so that you actually can't

6
00:00:25,920 --> 00:00:29,780
keep your position, then I don't see what's wrong with pulling yourself back

7
00:00:29,780 --> 00:00:35,640
mindfully. You can consider it to just be an appropriate bag. It doesn't have to

8
00:00:35,640 --> 00:00:43,460
be because, oh, I don't like me, I don't like what's going on. It can be, but again

9
00:00:43,460 --> 00:00:50,200
it's one of those benign, even if there is a desire involved. The problem with

10
00:00:50,200 --> 00:00:55,520
the desire or the aversion to the pain, the aversion to the leaning back is

11
00:00:55,520 --> 00:01:03,160
going to be far less disruptive than having to fall back and readjust your

12
00:01:03,160 --> 00:01:07,120
meditation practice. Far better off to bring yourself back and say, no, no, I'm

13
00:01:07,120 --> 00:01:13,640
but my intention is to sit straight. Now if it means to, if you mean like having

14
00:01:13,640 --> 00:01:18,040
to sit up perfectly straight, then in Vipasana it's not considered to be

15
00:01:18,040 --> 00:01:24,080
advisable because you're not developing in Vipasana without samata

16
00:01:24,080 --> 00:01:28,400
because you're not developing high states of concentrations and you're going

17
00:01:28,400 --> 00:01:32,600
to have the pain whether you slouch or whether you set up straight and unless you're

18
00:01:32,600 --> 00:01:37,480
developing samata meditation, so that you're not going to benefit from trying

19
00:01:37,480 --> 00:01:43,640
to sit perfectly straight. But when you're falling over my right yourselves in

20
00:01:43,640 --> 00:02:02,920
moving within you, conductors.

