1
00:00:00,000 --> 00:00:24,760
I will tell you about a man who experienced this

2
00:00:24,760 --> 00:00:41,760
I will tell you about a man named Nakula Bita and the advice given to him by the Buddha regarding pain.

3
00:00:41,760 --> 00:01:08,760
We will also meet Fendergulsari Buddha in this discourse as he gave a father exposition of the advice given by the Buddha.

4
00:01:08,760 --> 00:01:21,760
There was a couple in a city called crocodiles voice.

5
00:01:21,760 --> 00:01:27,760
It is said that when that city was founded, a crocodile made a noise.

6
00:01:27,760 --> 00:01:31,760
So that city was called crocodile noise.

7
00:01:31,760 --> 00:01:41,760
So in that city there was a couple and their names were Nakula Bita and Nakula Marta.

8
00:01:41,760 --> 00:01:45,760
Now Nakula Bita means father of Nakula.

9
00:01:45,760 --> 00:01:49,760
And Nakula Marta means mother of Nakula.

10
00:01:49,760 --> 00:02:04,760
So although the commentary did not say anything about the sun, they seemed to have a sun called Nakula.

11
00:02:04,760 --> 00:02:14,760
They married when they were young and they were devoted to each other.

12
00:02:14,760 --> 00:02:18,760
First visited their city.

13
00:02:18,760 --> 00:02:27,760
They went to see the Buddha along with the other inhabitants of the city.

14
00:02:27,760 --> 00:02:35,760
Now as soon as they saw the Buddha, they thought that Buddha was their sun.

15
00:02:35,760 --> 00:02:42,760
So they said, oh my sun, where have you been all these years?

16
00:02:42,760 --> 00:02:46,760
You did not support us and so on.

17
00:02:46,760 --> 00:02:51,760
So when they were saying all these, Buddha did not say anything to them.

18
00:02:51,760 --> 00:02:58,760
But I did not tell them to stop.

19
00:02:58,760 --> 00:03:01,760
But I just let them to talk as they like because they were so heavy when they saw the Buddha.

20
00:03:01,760 --> 00:03:10,760
And so when their happiness was great, it was difficult to take the Dharma.

21
00:03:10,760 --> 00:03:15,760
So Buddha just let them talk as they liked.

22
00:03:15,760 --> 00:03:24,760
And when they calmed down, the Buddha thought them the discourse and at the end of the discourse,

23
00:03:24,760 --> 00:03:27,760
both of them became sought up on us.

24
00:03:27,760 --> 00:03:37,760
The commentary explained why they got the notion that Buddha was their sun.

25
00:03:37,760 --> 00:03:56,760
They had been the mother and father of the Buddha or the Bodhisattva for 500 lives and consecutive lives in the past.

26
00:03:56,760 --> 00:04:04,760
And also they had been the Buddha's uncle and aunt for 500 more lives.

27
00:04:04,760 --> 00:04:13,760
And then how do you call the youngest younger brother of a father?

28
00:04:13,760 --> 00:04:20,760
Then elder brother of a father.

29
00:04:20,760 --> 00:04:31,760
So they had been the younger brother and elder sister of Buddha's father and mother and also again for 500 consecutive lives.

30
00:04:31,760 --> 00:04:42,760
They had been the younger brother and younger sister of the Buddha's father and mother.

31
00:04:42,760 --> 00:04:50,760
So for 150 lives they had been very close relatives.

32
00:04:50,760 --> 00:04:57,760
So that attachment was carried over to the present life.

33
00:04:57,760 --> 00:05:03,760
So when they saw the Buddha, they just thought that Buddha was their sun.

34
00:05:03,760 --> 00:05:08,760
They have found that they are long lost sun.

35
00:05:08,760 --> 00:05:17,760
Sometimes you meet a person whom you have not met before.

36
00:05:17,760 --> 00:05:25,760
But as soon as you see that person, you think that you have seen that person

37
00:05:25,760 --> 00:05:30,760
very familiar with him or with her or you like him or you like her.

38
00:05:30,760 --> 00:05:36,760
So that happened to many people.

39
00:05:36,760 --> 00:05:45,760
And I think that is because they had been together in the past.

40
00:05:45,760 --> 00:05:49,760
They may have been close relatives in the past.

41
00:05:49,760 --> 00:05:57,760
And so the attachment they had during their past lives was carried over to the present life.

42
00:05:57,760 --> 00:06:02,760
And so when they saw each other, then they liked each other.

43
00:06:02,760 --> 00:06:05,760
They attracted to each other.

44
00:06:05,760 --> 00:06:08,760
It came sort of bananas.

45
00:06:08,760 --> 00:06:13,760
Buddha went on his journey to other places.

46
00:06:13,760 --> 00:06:20,760
And then it seemed that Buddha visited that place two or three times.

47
00:06:20,760 --> 00:06:29,760
And so every time Buddha went to the city, they went to the Buddha and talked to the Buddha and so on.

48
00:06:29,760 --> 00:06:39,760
So once Buddha visited that city, maybe years later, because at that time,

49
00:06:39,760 --> 00:06:44,760
Nakula Bida was an old man.

50
00:06:44,760 --> 00:06:52,760
So in this course, we find only Nakula Bida, not his wife.

51
00:06:52,760 --> 00:06:55,760
His wife was not mentioned in this order.

52
00:06:55,760 --> 00:07:02,760
But in other sodas, they too were always mentioned together.

53
00:07:02,760 --> 00:07:11,760
So this time, when Buddha visited the city of crocodile voice,

54
00:07:11,760 --> 00:07:14,760
Nakula Bida went to see the Buddha.

55
00:07:14,760 --> 00:07:20,760
And after he had paid respects to the Buddha, he said,

56
00:07:20,760 --> 00:07:32,760
I am now an old man. I have reached the third portion of my life. So maybe he was about 70 or 80 at the time.

57
00:07:32,760 --> 00:07:36,760
And he said, I am always sick.

58
00:07:36,760 --> 00:07:49,760
And I get the chance to see you or your disciples who pleased me just once in a while.

59
00:07:49,760 --> 00:08:02,760
So now please give me some advice. Please give me some dhamma talk.

60
00:08:02,760 --> 00:08:09,760
That would be good for me for a long time.

61
00:08:09,760 --> 00:08:15,760
Then the Buddha said, it is true that this body is,

62
00:08:15,760 --> 00:08:23,760
that means your body is sick, your body is like an egg, fragile,

63
00:08:23,760 --> 00:08:28,760
and your body is just covered with thin skin.

64
00:08:28,760 --> 00:08:32,760
So that means you are vulnerable.

65
00:08:32,760 --> 00:08:40,760
So a person who is holding such a body,

66
00:08:40,760 --> 00:08:49,760
if that person thinks that he is healthy,

67
00:08:49,760 --> 00:08:53,760
it would be very foolish of him.

68
00:08:53,760 --> 00:08:57,760
So that means we have this physical body.

69
00:08:57,760 --> 00:09:07,760
Although we think we are healthy, still there are many impurities

70
00:09:07,760 --> 00:09:13,760
oozing out of our body through the orifices.

71
00:09:13,760 --> 00:09:18,760
And so the body is said to be sick.

72
00:09:18,760 --> 00:09:26,760
And to say that I am healthy is just foolishness.

73
00:09:26,760 --> 00:09:38,760
So Buddha said, for one carrying this body to claim just for even for a moment,

74
00:09:38,760 --> 00:09:45,760
even for a moment's health would be sheer foolishness.

75
00:09:45,760 --> 00:09:50,760
And the Buddha said, you should train yourself thus,

76
00:09:50,760 --> 00:09:56,760
though my body is sick, my mind shall not be sick.

77
00:09:56,760 --> 00:10:02,760
So this is the advice given to Nakula by the Buddha.

78
00:10:02,760 --> 00:10:05,760
It is sick.

79
00:10:05,760 --> 00:10:17,760
You could be of advice and I think I wish it all memorized this

80
00:10:17,760 --> 00:10:20,760
and say it every day.

81
00:10:20,760 --> 00:10:28,760
Though my body is sick, my mind shall not be sick.

82
00:10:28,760 --> 00:10:31,760
Kula Bida had this advice.

83
00:10:31,760 --> 00:10:35,760
He was very glad.

84
00:10:35,760 --> 00:10:41,760
So after hearing this,

85
00:10:41,760 --> 00:10:47,760
he paid respects to the Buddha and left him.

86
00:10:47,760 --> 00:10:51,760
And he went to the venerable Sahri Buddha.

87
00:10:51,760 --> 00:10:56,760
So when the venerable Sahri Buddha saw him,

88
00:10:56,760 --> 00:11:02,760
then Sahri Buddha said, you look calm.

89
00:11:02,760 --> 00:11:07,760
And your complexion is clear and pure.

90
00:11:07,760 --> 00:11:13,760
Surely you must have some Dhamma talk from the Buddha.

91
00:11:13,760 --> 00:11:20,760
Then Nakula Bida said, how could be otherwise venerable Sahri?

92
00:11:20,760 --> 00:11:28,760
I have just been sprinkled by the...

93
00:11:28,760 --> 00:11:30,760
What about that?

94
00:11:30,760 --> 00:11:40,760
I have just been sprinkled by the nectar of the Dhamma talk, by the exalted one.

95
00:11:40,760 --> 00:11:47,760
So he was very happy and he reported this to the venerable Sahri Buddha.

96
00:11:47,760 --> 00:11:52,760
Then the venerable Sahri Buddha asked, how was it?

97
00:11:52,760 --> 00:11:57,760
What was the talk that you had from the Buddha?

98
00:11:57,760 --> 00:12:10,760
And so Nakula Bida reported the conversation that occurred between him and the Buddha.

99
00:12:10,760 --> 00:12:15,760
So what he said to the Buddha and what the Buddha said to him.

100
00:12:15,760 --> 00:12:20,760
And Nakula Bida reported his conversation with the Buddha.

101
00:12:20,760 --> 00:12:24,760
The venerable Sahri Buddha said,

102
00:12:24,760 --> 00:12:32,760
did not it occur to you to question further the Buddha?

103
00:12:32,760 --> 00:12:42,760
How far is body sick and mind sick and how far is body sick and mind not sick?

104
00:12:42,760 --> 00:12:50,760
Now, Nakula Bida did not ask about this because he was so pleased with the advice that

105
00:12:50,760 --> 00:12:57,760
maybe he forgot to ask the Buddha to explain any father.

106
00:12:57,760 --> 00:13:04,760
So when the Sahri Buddha asked him, did you ask the Buddha about this?

107
00:13:04,760 --> 00:13:14,760
He said, I would have travelled a long distance to learn from you

108
00:13:14,760 --> 00:13:20,760
to learn from the venerable Sahri Buddha, the meaning of this saying of the Buddha.

109
00:13:20,760 --> 00:13:29,760
So let the explanation of this saying be the responsibility of the venerable Sahri Buddha.

110
00:13:29,760 --> 00:13:33,760
So my responsibility is to listen to you.

111
00:13:33,760 --> 00:13:39,760
So please explain it to me, he said.

112
00:13:39,760 --> 00:13:47,760
So, venerable Sahri Buddha said, then listen, take good attention, and I will speak.

113
00:13:47,760 --> 00:13:56,760
And he explained to him about the body sick and mind sick and the body sick and mind not sick

114
00:13:56,760 --> 00:14:07,760
with reference to two kinds of persons, one who has the wrong view of personality

115
00:14:07,760 --> 00:14:17,760
or wrong view about the existing aggregates and the person who has no such wrong view.

116
00:14:17,760 --> 00:14:23,760
Advice, though my body is sick, my mind share not be sick.

117
00:14:23,760 --> 00:14:32,760
Now whenever we have pain, we always want to get rid of it.

118
00:14:32,760 --> 00:14:43,760
And we are depressed, we are angry, we may be crying, we may be grieved.

119
00:14:43,760 --> 00:14:52,760
So this is what most people do when they feel pain, when they are sick.

120
00:14:52,760 --> 00:14:56,760
But here Buddha said, you must train in this way.

121
00:14:56,760 --> 00:15:00,760
Do my body is sick, my mind share not be sick.

122
00:15:00,760 --> 00:15:06,760
So although you may be suffering bodily, do not let your mind suffer.

123
00:15:06,760 --> 00:15:09,760
So this is the advice given by the Buddha.

124
00:15:09,760 --> 00:15:24,760
But Buddha did not explain to him how to do so that only the body is in pain and not the mind.

125
00:15:24,760 --> 00:15:33,760
And by letting our mind become sick, we are actually making our condition worse.

126
00:15:33,760 --> 00:15:44,760
So first there is the sickness of pain in the body and that is difficult to endure, that is one suffering.

127
00:15:44,760 --> 00:16:01,760
And then we are sorry about that pain, we are grieved, we are depressed, and so there is another suffering for us.

128
00:16:01,760 --> 00:16:11,760
Now, Buddha once described such a person as a person who has been pierced by two thorns.

129
00:16:11,760 --> 00:16:20,760
First, there is a thorn in his flesh and then in order to take out that flesh, he put another thorn in the flesh and so he suffered twice.

130
00:16:20,760 --> 00:16:32,760
So in the same way, when we let our mind be sick, we are letting another thorn into our flesh.

131
00:16:32,760 --> 00:16:45,760
So that Buddha's advice is, though the body is sick, your mind should not be sick.

132
00:16:45,760 --> 00:16:58,760
So what to do so that our minds are not sick, although the body is sick, you all know what to do.

133
00:16:58,760 --> 00:17:03,760
Practice mindfulness.

134
00:17:03,760 --> 00:17:13,760
The mind to be sick means the mind is associated with mental defilements.

135
00:17:13,760 --> 00:17:35,760
So when there is pain or sickness in the body, then the mental states that arise in our mind are sorrow, anger, it will.

136
00:17:35,760 --> 00:17:50,760
And so letting our mind be contaminated with mental defilements is what we call letting our minds suffer or letting our mind be sick.

137
00:17:50,760 --> 00:18:13,760
In the advice of the Buddha, following the practice of foundations of mindfulness, we can keep our mind from being invaded by these mental defilements.

138
00:18:13,760 --> 00:18:26,760
What we do when there is a pain in our body, so we concentrate on that pain and be mindful of it, or make mental notes as pain, pain, pain.

139
00:18:26,760 --> 00:18:38,760
So in the beginning there may be a little mental suffering, but as we get concentration, then we are able to watch the pain.

140
00:18:38,760 --> 00:18:56,760
And so we can avoid anger and other depression, anger and other mental defilements from entering our minds.

141
00:18:56,760 --> 00:19:09,760
So when there is pain, we try to watch it, we try to be mindful of it so that we are able to avoid mental defilements from arising in our minds.

142
00:19:09,760 --> 00:19:19,760
And when mental defilements arise in our minds, our minds become calm and concentrated.

143
00:19:19,760 --> 00:19:28,760
So when mind is concentrated and calm, then we are able to live with that pain.

144
00:19:28,760 --> 00:19:44,760
So that pain may not disappear, but we are able to live with it or to accept it, to make notes of it, not let it...

145
00:19:44,760 --> 00:19:56,760
Not let it torment our minds.

146
00:19:56,760 --> 00:20:07,760
Now when there is pain in the body, the tendency is to do something to get rid of it.

147
00:20:07,760 --> 00:20:19,760
So we have the desire to get rid of it, and we make notes of the pain, or we try to be mindful of pain.

148
00:20:19,760 --> 00:20:33,760
But actually to be mindful of the pain in this case is not to get rid of it, but to understand it fully.

149
00:20:33,760 --> 00:20:40,760
So that is the point we have to understand.

150
00:20:40,760 --> 00:20:53,760
If we make notes of pain with the hope of getting rid of it, and when it does not go away, we will become frustrated.

151
00:20:53,760 --> 00:21:03,760
So we will become more pierced by the thorn, and so our mind will become sick.

152
00:21:03,760 --> 00:21:10,760
In another sort of Buddha said, monks, there are three kinds of feelings, pleasant, unpleasant and neutral.

153
00:21:10,760 --> 00:21:19,760
And it is for the full understanding of these feelings that four foundations of mindfulness should be practiced.

154
00:21:19,760 --> 00:21:37,760
So according to that teaching, practicing the foundations of mindfulness on the feeling is not to get rid of it, not for it to go away, but to understand it fully.

155
00:21:37,760 --> 00:22:06,760
Standing involves three things. Understanding the individual characteristic of feeling, understanding the general characteristic of feeling, and abandoning some wrong perception.

156
00:22:06,760 --> 00:22:12,760
So some wrong notions regarding the feelings.

157
00:22:12,760 --> 00:22:17,760
So these three things are called full understanding.

158
00:22:17,760 --> 00:22:35,760
When you watch, let's say the pain, and make notes as pain, pain, pain, you come to see that it is an experience of an unpleasant sensations in the body.

159
00:22:35,760 --> 00:22:45,760
So that experience of unpleasant sensations in the body is the characteristic of unpleasant feeling.

160
00:22:45,760 --> 00:22:52,760
Now, please understand that feeling is mental.

161
00:22:52,760 --> 00:23:03,760
When we say feeling, we mean the mental state or mental factor that is called Widana in Pali.

162
00:23:03,760 --> 00:23:09,760
So when we say feeling, we mean the mental state in the mind.

163
00:23:09,760 --> 00:23:15,760
And with that mental state, we experience the sensation in the body.

164
00:23:15,760 --> 00:23:28,760
So what we call pain is a physical thing in the body, the material properties that go wrong.

165
00:23:28,760 --> 00:23:33,760
So when they are all right, we do not feel anything, we do not feel pain.

166
00:23:33,760 --> 00:23:38,760
But something went wrong, and so there is pain in the body.

167
00:23:38,760 --> 00:23:48,760
And we feel that we experience that pain with our mind, and when we experience that pain with our mind,

168
00:23:48,760 --> 00:23:53,760
there arises in our mind what is called feeling or Widana.

169
00:23:53,760 --> 00:24:05,760
And when we watch pain, actually we watch that feeling, that experience of the pain in the body,

170
00:24:05,760 --> 00:24:17,760
of the unpleasant sensations in the body is the individual characteristic of unpleasant feeling.

171
00:24:17,760 --> 00:24:19,760
What is unpleasant feeling?

172
00:24:19,760 --> 00:24:26,760
So unpleasant feeling is the experience of the unpleasant sensations in the body.

173
00:24:26,760 --> 00:24:52,760
So when you watch pain and you say pain or pain in pain, pain in pain, you get this understanding that the unpleasant feeling is actually the experience of the unpleasant or undesirable sensations in the body.

174
00:24:52,760 --> 00:24:59,760
It is function, and that is, it depresses your mind.

175
00:24:59,760 --> 00:25:02,760
So when there is pain, you are depressed.

176
00:25:02,760 --> 00:25:07,760
So, depressing the mind is its function.

177
00:25:07,760 --> 00:25:12,760
That also you may see when you watch the pain.

178
00:25:12,760 --> 00:25:39,760
And when you, if you go on watching the pain, making notes as pain, pain or pain in pain in pain in pain, you will come to see that what you thought to be one solid continuous pain is not really not solid and not continuous.

179
00:25:39,760 --> 00:25:46,760
Just one pain arises and you are aware of that pain or you make note of that pain.

180
00:25:46,760 --> 00:25:50,760
And then that pain disappears and then noting disappears.

181
00:25:50,760 --> 00:25:52,760
And then there is another pain.

182
00:25:52,760 --> 00:25:56,760
And so there is another noting or mindfulness.

183
00:25:56,760 --> 00:25:59,760
And then there is another pain and so on.

184
00:25:59,760 --> 00:26:08,760
So you see that pain is not one solid thing and pain does not last for a long time.

185
00:26:08,760 --> 00:26:14,760
Pain arises and disappears and then another pain arises and disappears and another pain arises and disappears.

186
00:26:14,760 --> 00:26:25,760
So you see that pain arises and disappears and so pain is impermanent.

187
00:26:25,760 --> 00:26:33,760
So when you see that pain is an impermanent thing, then you see the general characteristic of that pain.

188
00:26:33,760 --> 00:26:42,760
The general characteristic means a characteristic common to all conditions phenomena.

189
00:26:42,760 --> 00:26:51,760
So when you see that pain also arises and disappears, comes and goes and so it is impermanent.

190
00:26:51,760 --> 00:26:59,760
You also see that pain itself is dukha, pain itself is oppressed by arising and disappearing.

191
00:26:59,760 --> 00:27:05,760
And you also see that you have no control over it.

192
00:27:05,760 --> 00:27:08,760
It comes and it goes by itself.

193
00:27:08,760 --> 00:27:15,760
And so there is the characteristic of non-utter there.

194
00:27:15,760 --> 00:27:21,760
Non-utter means not utter and also having no control over it.

195
00:27:21,760 --> 00:27:41,760
So you see the three characteristics, the impermanence, suffering or unsatisfactoriness and non-soul nature of things when you pay close attention to pain in this case.

196
00:27:41,760 --> 00:27:58,760
Now when you see the pain as impermanent, suffering and no-soul, and you are able to discard the wrong notion that pain is permanent.

197
00:27:58,760 --> 00:28:02,760
Our pain lasts for a long time.

198
00:28:02,760 --> 00:28:18,760
So when you are able to see the general or common characteristics of pain, you are able to live with it.

199
00:28:18,760 --> 00:28:33,760
You do not feel any ill will, you do not feel any sorrow or you do not feel any depression regarding that pain.

200
00:28:33,760 --> 00:28:42,760
So although pain may still be there, you can accept it, you can live with it.

201
00:28:42,760 --> 00:28:52,760
That means you can keep your mind free from mental defalments such as anger.

202
00:28:52,760 --> 00:29:03,760
The Buddha said that with regard to pleasant feeling, the inclination to greet should be abandoned.

203
00:29:03,760 --> 00:29:18,760
With regard to unpleasant feeling, the inclination to anger should be abandoned, and with regard to neutral feeling, the inclination to ignorance should be abandoned.

204
00:29:18,760 --> 00:29:37,760
Now when you pay just close attention to the pain or you make notes of the pain and you see pain as impermanent, you are able to prevent anger from arising regarding that pain.

205
00:29:37,760 --> 00:29:47,760
So that is your abandoning anger regarding that pain.

206
00:29:47,760 --> 00:30:06,760
So if you see pain in this way, if you see the individual characteristic, common characteristic, and if you are able to avoid anger regarding that pain,

207
00:30:06,760 --> 00:30:12,760
you are set to understand the pain fully.

208
00:30:12,760 --> 00:30:22,760
So for that full understanding, we practice mentalness on pain.

209
00:30:22,760 --> 00:30:29,760
Of course, pleasant feeling and neutral feeling also should be observed.

210
00:30:29,760 --> 00:30:45,760
But here in this sense, Nakula Bida said he was always sick, Buddha talked about only the painful feeling about pain.

211
00:30:45,760 --> 00:30:57,760
So with regard to pain, when it arises, we just try to be mindful of it or we just try to live with it.

212
00:30:57,760 --> 00:31:10,760
And when we try to be mindful of it, our intention is not for pain to go away or not to get rid of it, but to fully understand it.

213
00:31:10,760 --> 00:31:30,760
So if you are able to watch it and see the characteristics, then you will no longer be bothered by that painful feeling.

214
00:31:30,760 --> 00:31:41,760
You can stay with it and you will find that actually pain is a good object of meditation.

215
00:31:41,760 --> 00:31:54,760
When you have pain, your mind is pulled towards pain and so there is very little opportunity for distractions.

216
00:31:54,760 --> 00:32:04,760
Be patient with it, do not move, but just try to be mindful of it as long as you can.

217
00:32:04,760 --> 00:32:14,760
Only when it becomes really too much for you, only when you honestly think that you cannot bear it any longer,

218
00:32:14,760 --> 00:32:26,760
can you make movements to ease pain. But until then, please be patient and try to be mindful of it or make mental notes of it.

219
00:32:26,760 --> 00:32:34,760
Only when you patiently make notes of pain, can you get concentration.

220
00:32:34,760 --> 00:32:46,760
So once you get concentration, then you will find that pain and doesn't bother you as it did before.

221
00:32:46,760 --> 00:33:00,760
So next time we have pain, we will remember this advice of the Buddha, though my body is sick, my mind shall not be sick, and then apply mindfulness to pain.

222
00:33:00,760 --> 00:33:07,760
So do the explanation given by the Venerable Saripoda.

223
00:33:07,760 --> 00:33:23,760
Now the Venerable Saripoda said an untaught person who has not hurt much about the teachings, who has not read much about the teachings,

224
00:33:23,760 --> 00:33:32,760
and who does not associate with good friends.

225
00:33:32,760 --> 00:33:51,760
So such a person takes this body or takes matter as self.

226
00:33:51,760 --> 00:34:04,760
Or he may regard the self as having, let me use the word Rupa in Bali.

227
00:34:04,760 --> 00:34:12,760
So Rupa is not just the body, Rupa is material properties.

228
00:34:12,760 --> 00:34:30,760
So such a person who is not trained, who is not familiar with the teachings of the noble ones, regards...

229
00:34:30,760 --> 00:34:47,760
how shall I say... regard the Rupa as self. Or he may regard the self as having Rupa.

230
00:34:47,760 --> 00:35:02,760
Or he may regard Rupa as being in self. Or he may regard the self as being in Rupa.

231
00:35:02,760 --> 00:35:17,760
Now there are four kinds of wrong views about the first aggregate. Rupa is Ata, that is one thing.

232
00:35:17,760 --> 00:35:38,760
And then Ata has Rupa, second thing. And then Rupa is in Ata. And the fourth is the Ata is in Rupa.

233
00:35:38,760 --> 00:35:45,760
So there are different kinds of wrong views about Rupa and Ata.

234
00:35:45,760 --> 00:35:59,760
A person regards Rupa as self. That means he sees Rupa as self or Rupa as one and the same thing.

235
00:35:59,760 --> 00:36:13,760
So just like he sees the flame and the color of the flame as just one, so he also sees the Rupa and Ata as just one.

236
00:36:13,760 --> 00:36:29,760
So the first, the wrong understanding is Rupa is Ata, like the flame and its color.

237
00:36:29,760 --> 00:36:46,760
Wrong view is Ata has Rupa, like a tree having a shadow. So here Ata and Rupa are different.

238
00:36:46,760 --> 00:36:56,760
First, Ata and Rupa are the same. Now he takes Ata and Rupa as different and the Ata as having the Rupa.

239
00:36:56,760 --> 00:37:14,760
Like a tree having a shadow. Or he may take Ata and Rupa as some receptacle or some vessel.

240
00:37:14,760 --> 00:37:27,760
And Rupa is something in the vessel. So in this case he is taking Ata and Rupa as different.

241
00:37:27,760 --> 00:37:38,760
But Rupa sees Rupa in Ata. Like he sees smell in a flower.

242
00:37:38,760 --> 00:37:52,760
He may also think that Ata and Rupa are different, but this time he sees Ata residing in the Rupa.

243
00:37:52,760 --> 00:38:08,760
So seeing a gem, like seeing a gem in a casket, in a box, in which people see or regard Ata and Rupa.

244
00:38:08,760 --> 00:38:24,760
So the first is Ata and Rupa are the same and the other three are Ata and Rupa are different. But the second is Ata having the Rupa.

245
00:38:24,760 --> 00:38:43,760
And the fourth is Ata in Rupa. So this is in different ways. I am the Rupa or Rupa is mine. So he is possessed by this idea.

246
00:38:43,760 --> 00:38:59,760
Or he takes that I am the Rupa or Rupa is mine. As he is possessed by this idea, his body alters and changes.

247
00:38:59,760 --> 00:39:15,760
So due to the body ordering and changing sorrow, grief and other mental defilements arise in his mind.

248
00:39:15,760 --> 00:39:29,760
That means although he thinks that he is the Rupa, Rupa is his property. That means he can have control over it.

249
00:39:29,760 --> 00:39:50,760
As such thought, his body may change and change means change for the walls. So when his body changes, he is sorrowful because formally he thought that the body would not change, it would last forever.

250
00:39:50,760 --> 00:40:03,760
And now in spite of his wish, in spite of his idea that I am Rupa or Rupa is mine, the body is changing and so he comes to grief.

251
00:40:03,760 --> 00:40:17,760
He may have the same views about the other aggregates also. Feeling perception, mental formations and consciousness.

252
00:40:17,760 --> 00:40:28,760
Why I am the feeling, I am the perception, I am the mental formation, I am the consciousness. All the consciousness is mine.

253
00:40:28,760 --> 00:40:38,760
His consciousness may change. And when the consciousness changes, he comes to grief.

254
00:40:38,760 --> 00:40:58,760
So this way a person who is not well informed and who does not associate with knowledgeable persons or noble ones, let his mind be sick when his body becomes sick.

255
00:40:58,760 --> 00:41:16,760
So that means a person who has not got rid of what is called the Sakaya deity, wrong view regarding the existing aggregates.

256
00:41:16,760 --> 00:41:34,760
So he was in who has not got rid of the Sakaya deity, comes to grief, say when his body, his feeling and so on changes.

257
00:41:34,760 --> 00:41:42,760
So it is how the body is sick and also the mind is sick.

258
00:41:42,760 --> 00:42:08,760
Now for the person who lets his body only sick and not his mind is a person who is well informed, who associates with noble persons and who understand the teachings of the noble ones and who does not have the same opinions about the five aggregates.

259
00:42:08,760 --> 00:42:21,760
So he does not think that Rupa and Ada are the same or Ada has Rupa or Rupa is in Ada or Ada is in Rupa.

260
00:42:21,760 --> 00:42:33,760
So he has no such wrong views and so he does not take that I am the Rupa or Rupa is mine.

261
00:42:33,760 --> 00:42:56,760
While he has no such opinions, his body may change, his feelings and others may change, but although they change, he does not experience any grief, any sorrow or any despair.

262
00:42:56,760 --> 00:43:08,760
Because he has no wrong view regarding the five aggregates or he has no Sakaya deity regarding the five aggregates.

263
00:43:08,760 --> 00:43:22,760
So such a button is a one whose body only is sick and who does not let his mind be sick.

264
00:43:22,760 --> 00:43:42,760
Now the wrong opinions taken by the first person are what are called 20 Sakaya deities, 20 wrong views regarding the existing aggregates.

265
00:43:42,760 --> 00:43:57,760
They are 20 because there are four modes for each aggregate. For the Rupa aggregate, there are four modes and for the feeling, aggregate, also there are four modes and so on.

266
00:43:57,760 --> 00:44:01,760
And so there are 20 of them and they are called 20 Sakaya deities.

267
00:44:01,760 --> 00:44:25,760
And the 20 Sakaya deities can be eradicated only when you become a sort-upana. So when he becomes a sort-upana, he eradicates the Sakaya deity or any kind of wrong view and also doubts.

268
00:44:25,760 --> 00:44:44,760
So a sort-upana is the one who does not let his mind suffer although his body is suffering.

269
00:44:44,760 --> 00:45:00,760
But actually, it is only the errant who can do that perfectly because sort-upanas still have some mental defalments remaining.

270
00:45:00,760 --> 00:45:08,760
Sakadagami is also remaining. Anagami is also there are some mental defalments remaining.

271
00:45:08,760 --> 00:45:17,760
So the most perfect one who can follow that advice given by the Buddha is an errant.

272
00:45:17,760 --> 00:45:26,760
We do to get rid of Sakaya deity. Again, we practice Vipasana meditation.

273
00:45:26,760 --> 00:45:40,760
So by the practice of Vipasana meditation, we can understand fully the nature of feelings here in this order, the painful feelings.

274
00:45:40,760 --> 00:46:02,760
And when we understand these feelings fully, then we are able to keep ourselves from being our minds from being tormented by the mental defalments, although our bodies may be suffering.

275
00:46:02,760 --> 00:46:21,760
So when we have pain or when we are sick, we will try to follow the advice given by the Buddha and not at an extra suffering onto the one that we have been suffering through the body.

276
00:46:21,760 --> 00:46:37,760
So by following that advice, that means by the practice of mindfulness meditation, we will be able to manage pain.

277
00:46:37,760 --> 00:46:57,760
We will be able to live with pain and we will be able to keep ourselves from being our minds from being tormented by anger and other mental defalments when we have pain.

278
00:46:57,760 --> 00:47:18,760
Notice we cannot run away from pain or we cannot avoid pain, but we can avoid mental defalments that would arise from that pain or that would arise depending on the pain.

279
00:47:18,760 --> 00:47:42,760
So it is possible to prevent mental defalments such as anger and depression by the practice of mindfulness of Vipasana meditation, although we are helpless against physical pain.

280
00:47:42,760 --> 00:47:48,760
Because since we have this physical body, there will always be pain.

281
00:47:48,760 --> 00:48:06,760
We will always experience pain and not to speak of ourselves, even the Buddha still suffered bodily pain as long as he lived.

282
00:48:06,760 --> 00:48:20,760
To remember this Buddha's advice, though my body is sick, my mind shall not be sick.

283
00:48:20,760 --> 00:48:38,760
We will see it many times a day, and especially when we have pain or when we are sick, we remember this saying and try to live according to this advice.

284
00:48:38,760 --> 00:48:51,760
So my body is sick, my mind shall not be sick.

