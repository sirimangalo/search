1
00:00:00,000 --> 00:00:04,580
Hello, welcome back to our study of Riddam Bada.

2
00:00:04,580 --> 00:00:10,020
Today we continue on with verse number 5, which goes.

3
00:00:10,020 --> 00:00:20,280
Nahi wai rein wai rani, samantidha kudajana, a wai rein jal samanti.

4
00:00:20,280 --> 00:00:33,400
Isadam mo sanantam, which means, not indeed through enmity is enmity appears, not here or anywhere.

5
00:00:33,400 --> 00:00:59,400
This teaching was given in regards to a famous story, another famous one, about two women who got very much caught up in a cycle of enmity or revenge and would never have gotten out if it weren't for the teaching of the Buddha.

6
00:00:59,400 --> 00:01:09,400
This is the our understanding. These two women were married to the same man, and one of them was barren, the other one was fertile.

7
00:01:09,400 --> 00:01:26,400
And the fertile woman got pregnant three times, and each time the barren woman, being afraid of the being left outer, being cast side, caused in abortion in the fertile woman.

8
00:01:26,400 --> 00:01:34,400
It was slipping some kind of medicine into her food. And the third time the woman and the woman actually died as a result of the abortion.

9
00:01:34,400 --> 00:01:55,400
And while she was dying, she cursed her murderer, saying, valing revenge, valing to come back someday, and pay the pay revenge for this evil deed.

10
00:01:55,400 --> 00:02:08,400
And she died. And the woman who killed her or who caused the abortion was beaten by her husband for being such an evil person and so on.

11
00:02:08,400 --> 00:02:20,400
And her husband was a fairly rough person, I guess. And died as a result as well, was beaten to the point where she got so sick, she died.

12
00:02:20,400 --> 00:02:33,400
And was born in the same house as a hen, a female chicken. And so happened that the woman who died from the abortion was born in the same house as a cat.

13
00:02:33,400 --> 00:02:44,400
So you can see where this is going. The hen laid eggs and the chicks hatched, and the cat came along and ate the chicks three times.

14
00:02:44,400 --> 00:02:50,400
And the third time actually ate the chicken as well or killed the chicken as well.

15
00:02:50,400 --> 00:02:58,400
The chicken died and was born as a leopard. The cat died and was born as a deer.

16
00:02:58,400 --> 00:03:06,400
And the leopard came and they were well familiar with each other at this point.

17
00:03:06,400 --> 00:03:18,400
The leopard came and ate the doe, the deer's fawns and the third time, or killed the doe as well.

18
00:03:18,400 --> 00:03:24,400
The doe came back, each time valing revenge, or the person who was killed with each time, keep it in their mind.

19
00:03:24,400 --> 00:03:31,400
When the hen was dying, she said, you've done this to me three times now, one day I'm going to come back.

20
00:03:31,400 --> 00:03:36,400
The chicken came back and the deer came back as an ogre.

21
00:03:36,400 --> 00:03:45,400
This is the woman who had died from the abortion and made a vow to come back to eat this woman's children.

22
00:03:45,400 --> 00:03:53,400
The doe was born as an ogre and the leopard was born as a woman.

23
00:03:53,400 --> 00:04:02,400
An ordinary woman living near, I believe, in Sawati, a great city in the time of the Buddha.

24
00:04:02,400 --> 00:04:10,400
And twice this ogre, an ogre is like an evil spirit that eats your children.

25
00:04:10,400 --> 00:04:18,400
Again, this part of the story I don't vouch for, the idea that there could be an ogre eating, or children eating ogres,

26
00:04:18,400 --> 00:04:23,400
twenty-five hundred years ago, seems to me a little bit far-fetched.

27
00:04:23,400 --> 00:04:28,400
It may just have been a woman who was a cannibal or something.

28
00:04:28,400 --> 00:04:33,400
It may have just been whatever it was because these things do happen.

29
00:04:33,400 --> 00:04:38,400
Even in our life, there are people who eat other people who kill and murder them and so on.

30
00:04:38,400 --> 00:04:40,400
There are terrible things that happen in this world.

31
00:04:40,400 --> 00:04:43,400
I don't disbelieve that.

32
00:04:43,400 --> 00:04:46,400
This is just an extraordinary example.

33
00:04:46,400 --> 00:04:48,400
That it was an ogre, well, okay.

34
00:04:48,400 --> 00:04:56,400
It's not so important because it doesn't affect the moral of the story at all, really.

35
00:04:56,400 --> 00:04:58,400
But let's go along with it.

36
00:04:58,400 --> 00:05:01,400
It was an ogre and ate this woman's children twice.

37
00:05:01,400 --> 00:05:06,400
And the third time the woman saw her coming and ran.

38
00:05:06,400 --> 00:05:10,400
And how it happened was the third time the woman left with her husband and said,

39
00:05:10,400 --> 00:05:17,400
you know, every lifetime, we're in the cycle of revenge and so she ran away.

40
00:05:17,400 --> 00:05:26,400
And the ogre chased after her and found her near Jita Wanda,

41
00:05:26,400 --> 00:05:30,400
which is the monastery of the Buddha near Saluti.

42
00:05:30,400 --> 00:05:35,400
And she and her husband were bathing in the pool outside and her husband went to bathe

43
00:05:35,400 --> 00:05:40,400
and she was holding her child and then she turned and saw there was the ogre coming.

44
00:05:40,400 --> 00:05:44,400
And right away they recognized each other and she knew right away who this was.

45
00:05:44,400 --> 00:05:46,400
It was like they had this bond.

46
00:05:46,400 --> 00:05:54,400
And so the woman ran into Jita Wanda and the Buddha at that moment was giving a talk to the

47
00:05:54,400 --> 00:05:57,400
large group of people whose followers.

48
00:05:57,400 --> 00:06:01,400
And so she ran right up to the Buddha and dropped her son at the Buddha's feet and said,

49
00:06:01,400 --> 00:06:05,400
here's my child, please protect him, save his life.

50
00:06:05,400 --> 00:06:15,400
And fortunately the angels of the garden Jita Wanda at the time didn't let the ogre in.

51
00:06:15,400 --> 00:06:19,400
They have power over it and spirits go like this.

52
00:06:19,400 --> 00:06:22,400
The idea that there might be evil spirits doesn't really face me.

53
00:06:22,400 --> 00:06:27,400
The idea that they might eat your children is a little bit, as I said, far-fetched.

54
00:06:27,400 --> 00:06:32,400
But okay, so there was a contact and this evil spirit wasn't allowed in.

55
00:06:32,400 --> 00:06:42,400
And until the Buddha called Told Ananda, his attendant, to go and bring in the ogre, to invite her in.

56
00:06:42,400 --> 00:06:45,400
And the Buddha said, what are you doing?

57
00:06:45,400 --> 00:06:49,400
He said, how can you do not realize where this is headed?

58
00:06:49,400 --> 00:06:54,400
Three lifetimes you haven't been the aggressor every time.

59
00:06:54,400 --> 00:06:58,400
You can't hope to possibly win every time.

60
00:06:58,400 --> 00:07:00,400
And you're creating a cycle of revenge.

61
00:07:00,400 --> 00:07:09,400
And he said, if you didn't come and find me, if you didn't have someone here to explain to you the problem inherent in this sort of behavior,

62
00:07:09,400 --> 00:07:12,400
you would have continued on like this forever.

63
00:07:12,400 --> 00:07:19,400
And then he said as verse, this verse that goes, not by enmity, is enmity appeased.

64
00:07:19,400 --> 00:07:24,400
This is an eternal truth.

65
00:07:24,400 --> 00:07:35,400
And just by waking them up and explaining it to them in this way, he was able to help them to overcome it and actually the ogre was able to let go.

66
00:07:35,400 --> 00:07:40,400
Now, I think this is a good lesson for us because it's part of this helping us to open up.

67
00:07:40,400 --> 00:07:52,400
And see the bigger picture and see where we're leading ourselves and where our minds are taking us.

68
00:07:52,400 --> 00:08:02,400
That every act that we do does have an effect on our minds and or can potentially have an effect on our mind if we cling to it.

69
00:08:02,400 --> 00:08:05,400
And there's obviously some incredible clinging going on here.

70
00:08:05,400 --> 00:08:10,400
There's incredible clinging to revenge.

71
00:08:10,400 --> 00:08:12,400
This person heard me as I said in the other verses.

72
00:08:12,400 --> 00:08:16,400
We have your Akochi, manga, what the manga, Dini, manga, ha ha, see me.

73
00:08:16,400 --> 00:08:19,400
He hit me, he beat me, he hurt me and so on.

74
00:08:19,400 --> 00:08:24,400
When we think like that, it builds up in our mind.

75
00:08:24,400 --> 00:08:29,400
And in this story, we see an example of the result and the consequences.

76
00:08:29,400 --> 00:08:31,400
It's not only in this life.

77
00:08:31,400 --> 00:08:40,400
And this is something that we obviously lose sight of for the most part in this world.

78
00:08:40,400 --> 00:08:44,400
Because we think of things only in terms of this one life.

79
00:08:44,400 --> 00:08:47,400
And so we think of in terms of getting ahead.

80
00:08:47,400 --> 00:08:49,400
We think of getting the better.

81
00:08:49,400 --> 00:08:57,400
We talk about the doggy dog world and how if you can get the better of other people and come out on top and so on.

82
00:08:57,400 --> 00:09:03,400
So then by the time you're in your middle age, you've got money and you've got power and you've got influence and you've got stability.

83
00:09:03,400 --> 00:09:06,400
That's somehow you've achieved something.

84
00:09:06,400 --> 00:09:08,400
You've won in that sense.

85
00:09:08,400 --> 00:09:16,400
If you can do it morally and great, if you can't, it's not really important because in the end you're going to die anyway.

86
00:09:16,400 --> 00:09:20,400
So better to get the better of other people.

87
00:09:20,400 --> 00:09:29,400
And I think most of us in this world or the majority of people in this world do partake of this in some extent.

88
00:09:29,400 --> 00:09:33,400
We don't mind so much getting the better of other people.

89
00:09:33,400 --> 00:09:36,400
In business, we don't mind cheating other people.

90
00:09:36,400 --> 00:09:40,400
We don't mind conning other people and so on.

91
00:09:40,400 --> 00:09:44,400
We don't mind trying to get the better of people and getting their money and so on.

92
00:09:44,400 --> 00:09:51,400
Just so long as we can somehow come out on top or we can get something good for ourselves at the time.

93
00:09:51,400 --> 00:10:02,400
And I think this is, here we have a really good example of these two women who are trying to secure a happiness for themselves against the rival.

94
00:10:02,400 --> 00:10:09,400
This woman who was jealous of the woman who was fertile.

95
00:10:09,400 --> 00:10:18,400
But the point we have in common with all of our feuds is that it carries on.

96
00:10:18,400 --> 00:10:23,400
And it's really quite easy to see and this is why it's really quite easy to understand.

97
00:10:23,400 --> 00:10:28,400
It's funny to see so many people having trouble with the idea that the mind might continue on.

98
00:10:28,400 --> 00:10:37,400
We're building up an incredible power or an incredible energy through our deeds.

99
00:10:37,400 --> 00:10:46,400
People who cling, people who build up this cycle of revenge, who have quarrels with other people who fight and dispute and hurt and harm others.

100
00:10:46,400 --> 00:10:51,400
And fight and are victorious even over other people.

101
00:10:51,400 --> 00:10:59,400
They build up this incredible energy in their mind that drags them down and actually puts them in the place of the victim in the future.

102
00:10:59,400 --> 00:11:07,400
As a result of the defilement of the mind, because defilement is, as I've said, the problem with it is that it drags your mind down.

103
00:11:07,400 --> 00:11:11,400
It defiles and it soils, it sullies the mind.

104
00:11:11,400 --> 00:11:18,400
It hurts you. It takes away your ability to function and cripples you.

105
00:11:18,400 --> 00:11:24,400
So the reason why we're in these unpleasant states is because of our crippled mind in the past.

106
00:11:24,400 --> 00:11:31,400
When a person comes out on top, they've developed all this clinging and all this evil in their mind.

107
00:11:31,400 --> 00:11:37,400
And even though they might have great luxury and power externally, it drags them down.

108
00:11:37,400 --> 00:11:46,400
And if they don't use their power in a good way, it will drag them down and the cycle of revenge will continue.

109
00:11:46,400 --> 00:11:58,400
Our inability to see beyond one life leads us to, this is something I've said, it doesn't really matter whether the mind does continue on.

110
00:11:58,400 --> 00:12:09,400
You can see the difference in our beliefs, if you believe that at death something stops and that's it and you don't have to go on, then it really doesn't matter what you do.

111
00:12:09,400 --> 00:12:18,400
And we can see how that's the effect that that's happening on the world that we're deteriorating and we're depleting our resources.

112
00:12:18,400 --> 00:12:22,400
You know, the depletion of resources, well, who cares? I mean, when I die, that's it, right?

113
00:12:22,400 --> 00:12:24,400
Well, wrong, really.

114
00:12:24,400 --> 00:12:36,400
I mean, the effect of your mind states and the greed that's in your mind alone is something that will last you on because of our attachment and our clinging.

115
00:12:36,400 --> 00:12:54,400
On the other hand, if you understand life after, if you understand, or let's say if you believe, if we think of it parallel in terms of belief that the mind doesn't stop and this experience continues on and works itself out.

116
00:12:54,400 --> 00:13:00,400
So the energy that we build up through our actions has eventually to bring consequences.

117
00:13:00,400 --> 00:13:08,400
Then, do you really think, do you think it's possible you would go to war? Do you think it's possible you would cling to things?

118
00:13:08,400 --> 00:13:29,400
Or if you really believe this, would you ever have any reason to build up and to try to find pleasure in the year and now to try to become addicted to things and to reach, to crave for things or to run after a femoral pleasure?

119
00:13:29,400 --> 00:13:47,400
Well, you'd have no reason to, if you understand this, you really, when you look, when you sit back and think about it, you think, you know, this is really whatever I get in this world, whatever I achieve, whatever I accomplish, if I become rich, if I become famous, it's meaningless.

120
00:13:47,400 --> 00:14:00,400
And in the end, it's quite inconsequential because it lasts for a moment and then it's gone.

121
00:14:00,400 --> 00:14:13,400
And this is the idea that it will be gone, that everything does cease and the evil that we do now will drag us down and bring evil unto us.

122
00:14:13,400 --> 00:14:20,400
So, this is something that helps us to see the true nature of reality.

123
00:14:20,400 --> 00:14:37,400
And the answer is a lot of questions in regards to why people are victims, why people suffer, why people are in a place in a position of great inequality, where some people are very rich, some people are very poor and so on.

124
00:14:37,400 --> 00:14:45,400
We can see that there are these waves and the person who is rich now may be poor in the next life, depending on the state of their mind, really.

125
00:14:45,400 --> 00:15:05,400
If a rich person is very generous and there's no reason, if they use their power and their influence in the right way, then you can see that this is something that is going to be the last because their mind is powerful, their mind is strong, they're sure of themselves and they have this great charge of good energy, which will come back to them.

126
00:15:05,400 --> 00:15:32,400
And obviously it's something that you can feel, a person who does good deeds is able to hold their head up, they're able to sit up tall and they don't shrink down, they don't, there's no, none of this clinging that drags them down and actually drags them to help, something that lifts them up and makes them feel light and makes them feel free from guilt and so on.

127
00:15:32,400 --> 00:15:51,400
So this helps us, this lesson is in regards to helping us to give up our quarrels because any kind of victory that we could gain is ephemeral, is temporary and it only encourages more and more,

128
00:15:51,400 --> 00:16:14,400
more quarrelling, more and more enmity as the person who's defeated, unless they can somehow give up. One of us can in the end let go, then they will surely seek revenge, regardless of whether they speak revenge, the evil that we have done will drag us down and drag us down to a state of suffering and stress and so on.

129
00:16:14,400 --> 00:16:20,400
So that's the lesson here, this is verse number five, thanks for tuning in and all the best.

