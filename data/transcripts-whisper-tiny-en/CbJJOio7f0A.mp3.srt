1
00:00:00,000 --> 00:00:07,000
One good evening, everyone.

2
00:00:07,000 --> 00:00:10,000
Happy Manga Buddha.

3
00:00:10,000 --> 00:00:17,000
Manga, Phunami, in time of the call it.

4
00:00:17,000 --> 00:00:19,000
Manga Phuja.

5
00:00:19,000 --> 00:00:24,000
Phuja is paying respect or homage.

6
00:00:24,000 --> 00:00:36,000
Manga, because it's the full moon of Manga, the month of Manga.

7
00:00:36,000 --> 00:00:42,000
So what's important about this full moon, you ask?

8
00:00:42,000 --> 00:00:49,000
Well, tradition has it that on the full moon of Manga,

9
00:00:49,000 --> 00:00:56,000
the Buddha taught what is now known as the Ovada party mukha.

10
00:00:56,000 --> 00:01:01,000
Ovada party mukha.

11
00:01:01,000 --> 00:01:10,000
It's sort of a summary of Buddhism.

12
00:01:10,000 --> 00:01:13,000
It's well known.

13
00:01:13,000 --> 00:01:23,000
And so the story behind it is there was...

14
00:01:23,000 --> 00:01:24,000
There's not much of a story.

15
00:01:24,000 --> 00:01:29,000
There was a group of monks who were meditating,

16
00:01:29,000 --> 00:01:34,000
and it may have been the fire ascetics,

17
00:01:34,000 --> 00:01:38,000
the fire worshiping ascetics in Rajagah.

18
00:01:38,000 --> 00:01:46,000
Because there were 1,250 of them,

19
00:01:46,000 --> 00:01:52,000
which coincides with the number of fire worshipers that were.

20
00:01:52,000 --> 00:01:57,000
And...

21
00:01:57,000 --> 00:01:59,000
So they were meditating on their own,

22
00:01:59,000 --> 00:02:02,000
and one of them was practicing,

23
00:02:02,000 --> 00:02:10,000
which strenuously and became an Arat, and he realized that he had attained the goal.

24
00:02:10,000 --> 00:02:15,000
And so he went to see the Buddha or the Buddha was sitting.

25
00:02:15,000 --> 00:02:20,000
But when he got to the Buddha, he looked and saw there was another monk coming,

26
00:02:20,000 --> 00:02:26,000
and he said, well, I'll wait until this monk sits down.

27
00:02:26,000 --> 00:02:29,000
And the other monks, the other monk came and sat down.

28
00:02:29,000 --> 00:02:34,000
He came up and turned and saw another monk coming.

29
00:02:34,000 --> 00:02:38,000
And so he decided he wouldn't talk to the Buddha.

30
00:02:38,000 --> 00:02:40,000
He'd wait for the third monk to come.

31
00:02:40,000 --> 00:02:42,000
And so he sat down.

32
00:02:42,000 --> 00:02:45,000
The third monk came, and then a fourth monk came,

33
00:02:45,000 --> 00:02:48,000
and one by one, all the monks came together,

34
00:02:48,000 --> 00:02:53,000
until there was 1,250 monks sitting there.

35
00:02:53,000 --> 00:02:59,000
All who had just attained our hardship.

36
00:02:59,000 --> 00:03:01,000
And then the Buddha taught them a lot about the monk,

37
00:03:01,000 --> 00:03:05,000
and that's the tradition, that's what they say happened.

38
00:03:11,000 --> 00:03:15,000
But we're going to look up here.

39
00:03:15,000 --> 00:03:20,000
We're going to look at the Pali.

40
00:03:20,000 --> 00:03:30,000
Mm-hmm.

41
00:03:38,000 --> 00:03:41,000
So it starts,

42
00:03:41,000 --> 00:03:52,000
and then we're going to look at the Buddha.

43
00:03:52,000 --> 00:03:56,000
And then we're going to look at the Buddha.

44
00:03:56,000 --> 00:04:05,000
And we're going to look at the Buddha.

45
00:04:05,000 --> 00:04:12,000
And then we're going to look at the Buddha.

46
00:04:12,000 --> 00:04:17,000
So then we're going to look at the Buddha.

47
00:04:17,000 --> 00:04:21,000
So there was three or three in the sciences.

48
00:04:21,000 --> 00:04:29,000
So I was going to ask Tal consoleMen about HIM.

49
00:04:29,000 --> 00:04:36,000
And it's in the Diganic area, it's also in the in the Dambapada.

50
00:04:46,600 --> 00:04:57,000
It's one of those really well condensed summaries of the Buddhist teaching.

51
00:04:57,000 --> 00:05:00,000
We always summarize it with a second verse.

52
00:05:00,000 --> 00:05:04,000
Sabapapasa, Karanang, Kuzulasupa, Sampadasa, Jitaprio, Dapanang.

53
00:05:04,000 --> 00:05:06,000
Yaitang Buddha and Sassanang.

54
00:05:06,000 --> 00:05:13,000
So this is the Buddha and Sassanang in the teaching of the Buddha's plural.

55
00:05:13,000 --> 00:05:15,000
All Buddha's teach.

56
00:05:15,000 --> 00:05:20,000
Sabapapasa, Karanang, not doing any evil.

57
00:05:20,000 --> 00:05:24,000
Kuzulasupa, Sampadatha, the full of good.

58
00:05:24,000 --> 00:05:32,000
Satyitaprio Dapanang and the purification of one's own mind.

59
00:05:32,000 --> 00:05:37,000
So, not doing evil and doing good and purifying one's mind.

60
00:05:37,000 --> 00:05:42,000
This is the teaching of all the Buddha.

61
00:05:42,000 --> 00:05:46,000
I think in the Thai version that verse is the first of the three.

62
00:05:46,000 --> 00:05:49,000
I can't remember that.

63
00:05:49,000 --> 00:05:55,000
Anyway, so the first it starts, Kunti Bhramungtapotitika, which is also very important teaching.

64
00:05:55,000 --> 00:05:59,000
Patients is the highest form of austerity.

65
00:05:59,000 --> 00:06:01,000
It's in the time of the Buddha.

66
00:06:01,000 --> 00:06:05,000
Asetics were big into torturing themselves.

67
00:06:05,000 --> 00:06:09,000
So they said those kind of tortures are not really useful.

68
00:06:09,000 --> 00:06:11,000
What's the best kind of torture?

69
00:06:11,000 --> 00:06:15,000
The most the highest form of asceticism, patience.

70
00:06:15,000 --> 00:06:22,000
You know, bearing with not only unpleasant feelings, but also pleasant feelings.

71
00:06:22,000 --> 00:06:41,000
So not reacting to desires, not reacting to appealing experiences and not acting out in regards to unappealing experiences.

72
00:06:41,000 --> 00:06:48,000
Kunti Bhramungtapotitika is the highest.

73
00:06:48,000 --> 00:06:56,000
There's nothing in samsara that can compare to Kunti Bhramungtapotitika.

74
00:06:56,000 --> 00:07:00,000
Nahi Babajito Parupagati.

75
00:07:00,000 --> 00:07:05,000
One is not a mendicant.

76
00:07:05,000 --> 00:07:09,000
Babajita is one who has left the home life.

77
00:07:09,000 --> 00:07:16,000
It's not properly left the home life who attacks others.

78
00:07:16,000 --> 00:07:18,000
Who is violent towards others.

79
00:07:18,000 --> 00:07:21,000
Mean towards others.

80
00:07:21,000 --> 00:07:30,000
Nahi Babajita Nahi Babajita Nahi Babajita Nahi Babajita Nahi Babajita Nahi Babajita Nahi Babajita

81
00:07:30,000 --> 00:07:40,000
Like an workplace, something like,oked Hic, Cethers, or hurt Cethers or school.

82
00:07:40,000 --> 00:07:48,000
Nude was a puppet.

83
00:07:48,000 --> 00:07:51,000
And Loving others.

84
00:07:51,000 --> 00:07:31,920
Bing had a

85
00:07:47,900 --> 00:07:56,000
Ot domain not speaking harshly towards others.

86
00:07:56,000 --> 00:08:00,720
and harshly towards others, or not hurting others.

87
00:08:00,720 --> 00:08:11,040
Fatimo cage is a somewhat of being restrained by the fatimo kad, by a code of conduct,

88
00:08:11,040 --> 00:08:13,960
you could say.

89
00:08:13,960 --> 00:08:23,420
I am in cloaked not knowing moderation in regards to food,

90
00:08:23,420 --> 00:08:26,640
Pontontas, Ayana, Sananya....

91
00:08:26,640 --> 00:08:34,620
having a secluded dwelling, dwelling and exhibition.

92
00:08:34,620 --> 00:08:27,120
Nithi-jit-tya, Jay improves insents on all the

93
00:08:27,120 --> 00:08:40,040
ations,

94
00:08:40,040 --> 00:08:49,480
And the higher JITA mind, the higher mind, solicitors, meditation, meditative states, being

95
00:08:49,480 --> 00:08:53,000
fixed on it, focused on it.

96
00:08:53,000 --> 00:08:57,320
A Tungundan Sassavan says the teaching of all the Buddhists.

97
00:08:57,320 --> 00:09:02,920
So Maga Buddha is normally in Thailand, not in Sri Lanka, I don't think, you know,

98
00:09:02,920 --> 00:09:09,640
they don't know about it in Sri Lanka, it's very important holiday in Thailand, don't

99
00:09:09,640 --> 00:09:10,640
tell about it.

100
00:09:10,640 --> 00:09:21,400
I should not just an excuse to celebrate, I suppose, excuse to meditate, excuse to have some

101
00:09:21,400 --> 00:09:27,400
kind of religious activity.

102
00:09:27,400 --> 00:09:55,400
So happy Maga Buddha, everyone.

103
00:09:55,400 --> 00:10:21,080
So, we also have a quote today, I don't think I'll go into it, maybe I'll post the hanging out

104
00:10:21,080 --> 00:10:28,240
if anybody want, if anyone has any questions they want to come on, otherwise I have

105
00:10:28,240 --> 00:10:35,080
a midterm in a couple of days on Buddhism, we're studying Mahayana Buddhism, so I have to

106
00:10:35,080 --> 00:10:57,080
hear from that as well, here's the hangout link, if anyone wants to come on and that's

107
00:10:57,080 --> 00:11:22,160
it, I don't even know if I have a sound, come on Maga Buddha, hey, aren't you coming

108
00:11:22,160 --> 00:11:23,160
here today?

109
00:11:23,160 --> 00:11:30,560
Why don't you come in, you're like, oh, I'm confused, April 4th, okay, that's why I've

110
00:11:30,560 --> 00:11:31,560
heard yet.

111
00:11:31,560 --> 00:11:37,360
I have like four thumbs coming to meditate, I think actually four thumbs, not my good

112
00:11:37,360 --> 00:11:38,360
word.

113
00:11:38,360 --> 00:11:52,440
Chinese Europe at the top, of course, right, I do have a question, go ahead, what's your

114
00:11:52,440 --> 00:11:53,440
question?

115
00:11:53,440 --> 00:12:00,200
My question is, I've been thinking, you mentioned last night, I believe it was about

116
00:12:00,200 --> 00:12:06,040
you have the meditators there, you stick them in a room so to speak and they're meditating,

117
00:12:06,040 --> 00:12:11,560
and you mentioned, I've heard you mentioned any number of times about no more than six

118
00:12:11,560 --> 00:12:20,600
hours of sleep in a twenty-four hour period, so I'm curious, I noticed many things since

119
00:12:20,600 --> 00:12:27,240
I've been seriously meditating for a number of months, transformations, I would say, I'm

120
00:12:27,240 --> 00:12:35,080
curious about the possibility that you can actually replace sleep with meditation, is that

121
00:12:35,080 --> 00:12:43,720
something that's ever done by serious meditators, where you're really not doing much actual

122
00:12:43,720 --> 00:12:45,760
sleep but meditating instead?

123
00:12:45,760 --> 00:13:00,480
Oh yeah, it's quite funny, okay, the texts, their months would not sleep for months, got

124
00:13:00,480 --> 00:13:07,160
the first story of the Dhammapada, and I took a bar that spent three months not lying

125
00:13:07,160 --> 00:13:15,040
down with no ill effect.

126
00:13:15,040 --> 00:13:21,400
He lost his eye, he lost his eyes, he went blind, but it didn't have to do with not lying,

127
00:13:21,400 --> 00:13:26,480
well it wasn't exactly because he didn't lie down, there was some condition of the eyes

128
00:13:26,480 --> 00:13:31,200
and he had to lie down in order to cure it, in order to get better and he decided not

129
00:13:31,200 --> 00:13:35,000
to.

130
00:13:35,000 --> 00:13:39,520
When you don't sleep, one of the things that happens is you start to hallucinate, you start

131
00:13:39,520 --> 00:13:51,680
to lose some sense of balance, the effects, if you look on the internet, what happened

132
00:13:51,680 --> 00:13:53,640
to my voice?

133
00:13:53,640 --> 00:14:02,040
If you look on the internet, the effects of not sleeping are, there are effects of months,

134
00:14:02,040 --> 00:14:09,840
sleeping that they'll tell you about, well I want just to say that none of those effects

135
00:14:09,840 --> 00:14:19,360
are necessarily deleterious or problematic and that being said, once you really get into

136
00:14:19,360 --> 00:14:22,920
the meditation, if you get into the groove, you can sustain it for at least several

137
00:14:22,920 --> 00:14:34,360
days, on little to no sleep, most people can do that, to do it for months, I think you

138
00:14:34,360 --> 00:14:40,480
need to prepare, you'd have to leave society because the mind is so caught up in so much

139
00:14:40,480 --> 00:14:49,280
information and stimuli that it just works too hard to be without sleep, but if you're

140
00:14:49,280 --> 00:14:54,640
living in a forest for some time, your mind starts to work, not have to work so hard,

141
00:14:54,640 --> 00:15:04,520
it becomes relaxed, habitually relaxed and so you can spend your days without building

142
00:15:04,520 --> 00:15:09,520
up the need to sleep, don't tiring yourself out, and then when you don't sleep, your

143
00:15:09,520 --> 00:15:11,000
mind is already calm.

144
00:15:11,000 --> 00:15:16,080
The first night or two when you stop sleeping, you hallucinate, you'll see things in the

145
00:15:16,080 --> 00:15:26,480
floor walking around, you'll feel dizzy, you get that overtired feeling, but none of that

146
00:15:26,480 --> 00:15:32,560
is really all that deleterious, I mean they'll say things like we repair our body cells

147
00:15:32,560 --> 00:15:38,480
when we sleep, I don't know how that works or whether there's proof as to the need for

148
00:15:38,480 --> 00:15:43,440
it or whether you could show that through meditation, you're doing some kind of cell

149
00:15:43,440 --> 00:15:47,960
rebuilding, I don't really understand that part of it, but I know it's been done and it's

150
00:15:47,960 --> 00:15:53,360
it is done, it is possible, very possible.

151
00:15:53,360 --> 00:16:02,960
Well if I just sleep is important for me and quite frankly I got back into my meditation

152
00:16:02,960 --> 00:16:08,760
practice because I wanted to cure myself of insomnia and that's pretty much taken care

153
00:16:08,760 --> 00:16:15,920
of now, but now when I wake up in the middle of the night and I always meditate when I wake

154
00:16:15,920 --> 00:16:23,160
up in the middle of the night I want to release the anxiety of the fear of not being

155
00:16:23,160 --> 00:16:30,600
able to go back to sleep and if I understand that the meditation takes over for the sleep

156
00:16:30,600 --> 00:16:34,440
then I was hoping for that answer.

157
00:16:34,440 --> 00:16:39,800
Yeah absolutely, I mean that's a big part of getting over insomnia is like I did this

158
00:16:39,800 --> 00:16:49,160
once and besides of course I've done days without sleep but one time it wasn't even

159
00:16:49,160 --> 00:16:57,480
intentional, I had been on a flight from Thailand to Los Angeles and then I got in in the

160
00:16:57,480 --> 00:17:01,840
evening and they gave me a cough, someone gave me a coffee to drink, I didn't even think

161
00:17:01,840 --> 00:17:09,120
about it, I just drank the coffee, back then I was actually quite less strict in the

162
00:17:09,120 --> 00:17:15,080
rules as well and now I wouldn't really have coffee in the evening but I had this coffee

163
00:17:15,080 --> 00:17:19,720
and then I realized what am I doing drinking coffee in the evening because that was just

164
00:17:19,720 --> 00:17:24,800
what they were offering but I ended up not being able to sleep at all, there was no

165
00:17:24,800 --> 00:17:29,560
way I was going to sleep because of the jet lag, the time change and so I just lay in

166
00:17:29,560 --> 00:17:35,360
bed all night, being mindful, meditating and when I got up in the morning I really felt

167
00:17:35,360 --> 00:17:41,760
fine, I hadn't slept a week that I remember, would I remember up that night is I was

168
00:17:41,760 --> 00:17:52,560
up all night but because of being mindful in the morning there was no ill effect.

169
00:17:52,560 --> 00:18:05,440
Well thank you for that, absolutely to some extent you can replace, I don't want to say

170
00:18:05,440 --> 00:18:11,160
because I know people are going to say oh no science has evidence to the contrary but

171
00:18:11,160 --> 00:18:17,920
whatever, I'm skeptical of such evidence, because such evidence is always taken from people

172
00:18:17,920 --> 00:18:24,800
who need sleep because their minds are not focused, we need to do a study on meditators

173
00:18:24,800 --> 00:18:29,160
who are going without sleep and to see the effects and to see whether there was any

174
00:18:29,160 --> 00:18:40,160
difference between them and people who weren't meditating but it doesn't take a genius

175
00:18:40,160 --> 00:18:48,440
to realize the difference when the mind is calm and the mind is focused and the mind

176
00:18:48,440 --> 00:19:01,440
is clear, I have a question about it, okay, so we were reading today because we were

177
00:19:01,440 --> 00:19:08,320
discussing, we're in second life, how the Buddhist enlightenment is distinguished from

178
00:19:08,320 --> 00:19:16,800
other hands enlightenment and so we read a big Buddhist translation and then I was thinking

179
00:19:16,800 --> 00:19:21,760
in the teachings of the Buddha, it is often said that if you come and practice you are

180
00:19:21,760 --> 00:19:28,320
going to see for yourself and we were kind of getting on the topic of if there might

181
00:19:28,320 --> 00:19:34,080
be enlightened beings and if it is still possible for beings to become enlightened while

182
00:19:34,080 --> 00:19:41,400
the Buddha's sassana is still alive and in some place thriving even so I would love

183
00:19:41,400 --> 00:19:51,160
to hear your thoughts on that, well it's not a matter of whether it's still possible,

184
00:19:51,160 --> 00:19:55,320
I mean the teachings are there, the teachings are still here, the only way it wouldn't

185
00:19:55,320 --> 00:20:01,920
be possible is if there were either no teachings or no one practicing them, so if you

186
00:20:01,920 --> 00:20:09,440
practice them the results are, there you also need people teaching them and guiding through

187
00:20:09,440 --> 00:20:21,280
them because many of us teachings themselves aren't enough to push you to practice them.

188
00:20:21,280 --> 00:20:29,560
Wonderful, yeah, that was my kind of the direction of my thought as well, if one visits

189
00:20:29,560 --> 00:20:35,320
where the teaching of the Buddha is live and there are good teachings, I mean I was just

190
00:20:35,320 --> 00:20:41,400
like of course it's possible, it must be possible, so thanks, absolutely it's the

191
00:20:41,400 --> 00:20:59,600
endurance you're not closed, wonderful, okay have a good night, good night, good night, bye

192
00:20:59,600 --> 00:21:18,540
bye bye!

