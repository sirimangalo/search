1
00:00:00,000 --> 00:00:09,840
Hi, and welcome to this series of videos on why everyone should practice meditation.

2
00:00:09,840 --> 00:00:16,600
This series is meant to be an addition to my series on how to meditate, and it was brought

3
00:00:16,600 --> 00:00:25,840
about as a result of many comments or questions about not how to meditate, but why or

4
00:00:25,840 --> 00:00:31,080
whether ordinary people should meditate, whether it's something just for monks or whether

5
00:00:31,080 --> 00:00:35,680
it's something that is applicable to people living ordinary everyday lives.

6
00:00:35,680 --> 00:00:42,000
And so these videos are an attempt to answer that question and give a response, yes, that

7
00:00:42,000 --> 00:00:48,680
these videos are certainly not simply for people who have left the everyday ordinary

8
00:00:48,680 --> 00:00:55,720
life to live in the forest or so on, that these meditation techniques are totally applicable

9
00:00:55,720 --> 00:00:58,560
to everyday life.

10
00:00:58,560 --> 00:01:03,720
So these are the top five reasons why everyone should practice meditation.

11
00:01:03,720 --> 00:01:09,520
And the first reason is because meditation is something which purifies the mind.

12
00:01:09,520 --> 00:01:17,120
When you practice meditation, it brings about a state of purity and clarity in one's mind.

13
00:01:17,120 --> 00:01:25,000
Now this is when we look at the ordinary state of being, the ordinary state of our minds,

14
00:01:25,000 --> 00:01:29,080
we can see that in our own minds if we're willing to be honest with ourselves, that we

15
00:01:29,080 --> 00:01:36,480
have many things which we would rather do without things like anger, things like greed

16
00:01:36,480 --> 00:01:45,720
or addiction, things like worries, stress, things like fear, anxiety, and many things

17
00:01:45,720 --> 00:01:53,560
which we would maybe be able to admit to ourselves are a cause for suffering for ourselves,

18
00:01:53,560 --> 00:01:56,160
cause for suffering for other people.

19
00:01:56,160 --> 00:02:01,000
These sort of things that when people look at us, they are turned off by when they

20
00:02:01,000 --> 00:02:05,400
see in us, when they see that we're conceited, when we see that we are opinionated, when

21
00:02:05,400 --> 00:02:09,520
they see that we are jealous, when they see that we are stingy, when they see that we have

22
00:02:09,520 --> 00:02:17,160
all these qualities, they feel turned off by these things and we ourselves suffer from them.

23
00:02:17,160 --> 00:02:21,400
When we have these qualities of mind, it creates suffering for ourselves, it also creates

24
00:02:21,400 --> 00:02:23,720
suffering for the people around us.

25
00:02:23,720 --> 00:02:28,560
These are all what we call the defilements of the mind, these are things which are undesirable

26
00:02:28,560 --> 00:02:30,920
qualities of mind.

27
00:02:30,920 --> 00:02:37,240
Now normally we look at these things and we rationalize them in many different ways, for

28
00:02:37,240 --> 00:02:43,280
instance we say to ourselves, well it's just a part of who I am or we say these things

29
00:02:43,280 --> 00:02:48,960
are just human, you know, I'm only human and so we say if we didn't have these things

30
00:02:48,960 --> 00:02:55,800
it would be somehow inhuman and we say that these things are unavoidable and it's just

31
00:02:55,800 --> 00:03:00,880
who I am that I can't change who I am and so on.

32
00:03:00,880 --> 00:03:06,120
And the truth is very much different from what we think, actually it's very clear that

33
00:03:06,120 --> 00:03:11,320
people are able to live without these things, are able to train themselves out of these

34
00:03:11,320 --> 00:03:12,320
things.

35
00:03:12,320 --> 00:03:17,200
So if we simply say that these things are a part of who I am, it's clear that we're

36
00:03:17,200 --> 00:03:20,480
only saying this out of ignorance, we're saying this because we've never tried or we've

37
00:03:20,480 --> 00:03:24,320
never been given the tools with which to do away with them.

38
00:03:24,320 --> 00:03:27,960
So when people are angry they say well I'm just an angry person but they've never really

39
00:03:27,960 --> 00:03:32,360
looked at the anger to try to understand it and to see where it comes from and to see

40
00:03:32,360 --> 00:03:37,560
the disadvantages of it and to really teach themselves and to create an understanding in

41
00:03:37,560 --> 00:03:43,200
their minds, to the disadvantages of things like anger or things like craving, things like

42
00:03:43,200 --> 00:03:46,160
addiction and so on.

43
00:03:46,160 --> 00:03:50,880
When we practice meditation we actually have the ability to overcome these things and

44
00:03:50,880 --> 00:03:55,480
how we do it as we look at them because we can say for ourselves anger is not a good thing

45
00:03:55,480 --> 00:03:59,600
in which I wasn't so angry but we aren't actually looking at the anger and we don't

46
00:03:59,600 --> 00:04:05,720
actually realize for ourselves on an experiential level that anger is a bad thing.

47
00:04:05,720 --> 00:04:09,360
At the moment when we look at the anger, when we focus on the anger, when we say to ourselves

48
00:04:09,360 --> 00:04:15,720
angry, just reminding ourselves of what's going on right now, we say to ourselves angry

49
00:04:15,720 --> 00:04:20,680
angry for example, we're able to see clearly the anger and sort of teach ourselves.

50
00:04:20,680 --> 00:04:23,640
It's like teaching ourselves something that we already know.

51
00:04:23,640 --> 00:04:27,640
We know anger is not a good thing but when we look at it, we really see for ourselves

52
00:04:27,640 --> 00:04:32,080
that it's not a good thing and we're able to do away with it in here and now.

53
00:04:32,080 --> 00:04:37,640
When we have any sort of stress or worry or frustration, we can do the exact same thing

54
00:04:37,640 --> 00:04:41,880
and when we practice meditation we're able to do away with these things.

55
00:04:41,880 --> 00:04:45,920
The other thing that people often say is if I didn't have these things, who would I

56
00:04:45,920 --> 00:04:46,920
be?

57
00:04:46,920 --> 00:04:48,960
I'd be somehow inhuman.

58
00:04:48,960 --> 00:04:58,720
I think this is sort of an argument, argumentative sort of thing to say that actually,

59
00:04:58,720 --> 00:05:01,920
there's no reason for us to say that negative things are somehow human.

60
00:05:01,920 --> 00:05:04,720
In fact, you can just as easily say that these are inhuman.

61
00:05:04,720 --> 00:05:11,800
When we have cruelty or addiction or jealousy or stingyness or conceit or opinion,

62
00:05:11,800 --> 00:05:17,360
all of these things, we can easily say that these things are what make us inhuman and

63
00:05:17,360 --> 00:05:23,000
that if we really want it to be human or humane, then we would have to develop only the

64
00:05:23,000 --> 00:05:26,760
good things and we should really try to do away with the bad things.

65
00:05:26,760 --> 00:05:30,680
There's no reason why we should have to have these things, there's nothing intrinsic about

66
00:05:30,680 --> 00:05:31,680
them at all.

67
00:05:31,680 --> 00:05:36,760
In fact, there are things which we have developed over the years, over the course of

68
00:05:36,760 --> 00:05:37,760
our lifetime.

69
00:05:37,760 --> 00:05:45,520
As we get older and we get more bitter, we get more angry or as we indulge in sensuality,

70
00:05:45,520 --> 00:05:47,920
we get addicted to things more and more.

71
00:05:47,920 --> 00:05:52,440
It's clear that these are not intrinsic in any way to who we are and they can be changed

72
00:05:52,440 --> 00:05:56,960
just as we can become more angry, we can also become less angry and we can train ourselves

73
00:05:56,960 --> 00:05:57,960
out of these.

74
00:05:57,960 --> 00:06:03,440
This is the first benefit of practicing meditation and the number five counting down is that

75
00:06:03,440 --> 00:06:08,000
meditation helps to make our mind pure because it helps us to understand and to see the

76
00:06:08,000 --> 00:06:13,240
difference between good states of mind and bad states of mind, good qualities and bad qualities

77
00:06:13,240 --> 00:06:19,320
and helps us to learn how to develop only good qualities because we see clearly the difference

78
00:06:19,320 --> 00:06:24,000
and what is the result of carrying around good things and the result of carrying around bad

79
00:06:24,000 --> 00:06:27,680
things and we can see it's an entirely different thing.

80
00:06:27,680 --> 00:06:32,000
So that's number five and from now on, I'll be counting down going on to number four

81
00:06:32,000 --> 00:06:33,000
in the near future.

82
00:06:33,000 --> 00:06:53,000
Thanks for tuning in and it would be...

