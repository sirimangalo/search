1
00:00:00,000 --> 00:00:12,760
Hi, everyone. This is Adder, a volunteer for Siri-Mongolow International, the organization

2
00:00:12,760 --> 00:00:18,400
that supports Bantayu to Damabiku in his teachings. I'm here today to share with you

3
00:00:18,400 --> 00:00:24,440
some information about the Digital Poly Reader and to invite volunteers to support us

4
00:00:24,440 --> 00:00:30,840
in this project. The Digital Poly Reader, also known as the DPR, is a tool much like

5
00:00:30,840 --> 00:00:36,600
a hard copy language reader. The tool includes the poly cannon and related scriptures.

6
00:00:36,600 --> 00:00:42,440
It includes multiple dictionaries to facilitate reading the scriptures. It is also useful

7
00:00:42,440 --> 00:00:47,520
in the study of the poly language at an advanced level. The DPR is heavily used around

8
00:00:47,520 --> 00:00:54,880
the world by various folks, such as monks, researchers, poly language learners, and professors.

9
00:00:54,880 --> 00:01:00,320
Initially written by you to Damabiku as a Firefox extension, a team of volunteers has

10
00:01:00,320 --> 00:01:05,840
worked in the last few months to port a number of its features to cloud and web, see the

11
00:01:05,840 --> 00:01:13,840
links below. Currently, we're seeing up to 150 unique users on a daily basis. It is also used

12
00:01:13,840 --> 00:01:20,400
internally by the Siri-Mongolow community in our weekly Domino study sessions. Initial feedback

13
00:01:20,400 --> 00:01:26,240
has been positive, indicating the usefulness of the tool. However, there are a number of features

14
00:01:26,240 --> 00:01:32,880
that still need to be implemented. They are mainly around one, enacting a mobile first experience.

15
00:01:33,920 --> 00:01:41,200
Two, enabling a fully disconnected offline experience. And three, a bunch of enhancements based

16
00:01:41,200 --> 00:01:48,800
on user feedback. We wish to invite volunteers who can help complete the above to shape the DPR

17
00:01:48,800 --> 00:01:53,680
and make a deep and long-lasting impact on the Domma and poly learning communities.

18
00:01:54,720 --> 00:01:57,920
We would love to have you join us if you can help with the following.

19
00:01:58,800 --> 00:02:02,720
Beta testing and feedback, especially if you're a user of the DPR,

20
00:02:03,840 --> 00:02:08,800
building progressive web applications, building mobile first web applications,

21
00:02:08,800 --> 00:02:20,000
or skills in HTML5, CSS3, ES6, Knockout, JS, or Bootstrap4. If you're interested in helping out,

22
00:02:20,000 --> 00:02:26,080
please join our Discord with the link below and send a message on the general channel to be invited

23
00:02:26,080 --> 00:02:32,240
into the DPR channel. If you're interested in finding out other ways to help the Siri-Mongolow community,

24
00:02:32,240 --> 00:02:42,240
please also considering checking out our Discord server. Thank you. May you be well.

