1
00:00:00,000 --> 00:00:07,360
How to Meditate by Utadamobiku, read by Adirokes

2
00:00:07,360 --> 00:00:13,640
Chapter 4 Fundamentals In this chapter, I will explain four fundamental

3
00:00:13,640 --> 00:00:18,280
principles that are essential to the meditation practice.

4
00:00:18,280 --> 00:00:24,880
The practice of meditation is more than just walking back and forth and sitting still.

5
00:00:24,880 --> 00:00:29,800
The benefit one gains from meditation practice depends on the quality of one's mind

6
00:00:29,800 --> 00:00:36,040
in each moment, not the quantity of practice one undertakes.

7
00:00:36,040 --> 00:00:43,840
The first important principle is that meditation must be practiced in the present moment.

8
00:00:43,840 --> 00:00:50,680
During meditation, one's mind should be focused on the experience occurring at each moment,

9
00:00:50,680 --> 00:00:55,720
never dwelling in the past or skipping ahead to the future.

10
00:00:55,720 --> 00:01:02,360
One should avoid thoughts about how much time one has been sitting or how much time is left.

11
00:01:02,360 --> 00:01:07,760
One's mind should always be noting the objects as they arise in the present moment,

12
00:01:07,760 --> 00:01:12,920
not straying even one moment into the past or future.

13
00:01:12,920 --> 00:01:19,200
When one is out of touch with the present moment, one is out of touch with reality.

14
00:01:19,200 --> 00:01:25,600
Each experience only lasts a single moment, so it is important to note experiences

15
00:01:25,600 --> 00:01:33,960
at the moment they occur, recognizing their arising, persisting, and ceasing, using the mantra

16
00:01:33,960 --> 00:01:39,040
to create a clear awareness of their essential nature.

17
00:01:39,040 --> 00:01:45,040
Only in this way can we come to understand the true nature of reality.

18
00:01:45,040 --> 00:01:52,080
The second important principle is that meditation must be performed continuously.

19
00:01:52,080 --> 00:01:58,160
Using practice, like any training, must become habitual if it is to help one overcome

20
00:01:58,160 --> 00:02:02,800
bad habits of clinging and partiality.

21
00:02:02,800 --> 00:02:09,960
If one practices meditation intermittently and is on mindful between sessions, any clarity

22
00:02:09,960 --> 00:02:16,680
of mind gained from the practice will be weakened by subsequent distracted mind states,

23
00:02:16,680 --> 00:02:21,120
making the meditation practice seem useless.

24
00:02:21,120 --> 00:02:27,320
This is often a cause for frustration and disillusionment in new meditators until they learn

25
00:02:27,320 --> 00:02:34,200
to be mindful throughout their daily activities and continuously from one meditation technique

26
00:02:34,200 --> 00:02:36,440
to the next.

27
00:02:36,440 --> 00:02:42,120
Once they are able to be mindful continuously, their concentration will improve and they

28
00:02:42,120 --> 00:02:47,120
will realize the true benefit of the practice.

29
00:02:47,120 --> 00:02:52,440
One must try to practice continuously from one moment to the next.

30
00:02:52,440 --> 00:02:57,640
During formal meditation, one should keep one's mind in the present moment through the

31
00:02:57,640 --> 00:03:04,520
whole of the practice as best one can, using the mantra to create a clear thought from

32
00:03:04,520 --> 00:03:07,760
one moment to the next.

33
00:03:07,760 --> 00:03:13,920
When walking, one must be careful to transfer one's attention from one foot to the next

34
00:03:13,920 --> 00:03:16,040
without break.

35
00:03:16,040 --> 00:03:23,040
When sitting, one must pay careful attention to the rising and falling, noting each movement

36
00:03:23,040 --> 00:03:27,560
one after the other, without break.

37
00:03:27,560 --> 00:03:33,720
Moreover, after practicing walking meditation, one should maintain awareness and acknowledgement

38
00:03:33,720 --> 00:03:43,040
of the present moment until one is settled in sitting position, noting, bending, touching,

39
00:03:43,040 --> 00:03:50,160
sitting, etc. according to the movements required to change position.

40
00:03:50,160 --> 00:03:55,840
Once sitting down, one should begin immediate contemplation of the rising and the falling

41
00:03:55,840 --> 00:04:00,760
of the stomach for the duration of the sitting meditation.

42
00:04:00,760 --> 00:04:06,560
At the end of the sitting meditation, one should try to continue meditating on the present

43
00:04:06,560 --> 00:04:13,560
moment in daily life, carrying on noting to the best of one's ability until the next

44
00:04:13,560 --> 00:04:16,600
meditation session.

45
00:04:16,600 --> 00:04:19,560
Meditation practice is like falling rain.

46
00:04:19,560 --> 00:04:26,000
Every moment one is clearly aware of reality is like a single raindrop, though it may seem

47
00:04:26,000 --> 00:04:33,280
insignificant if one is mindful continuously from one moment to the next.

48
00:04:33,280 --> 00:04:36,920
Clearly aware of each moment one at a time.

49
00:04:36,920 --> 00:04:44,080
These moments of concentrated awareness will accumulate and give rise to strong concentration

50
00:04:44,080 --> 00:04:51,400
and clear insight into reality, just as miniscule drops of falling rain accumulate to fill

51
00:04:51,400 --> 00:04:54,920
a lake or flood an entire village.

52
00:04:54,920 --> 00:05:00,880
The third important principle of practice is in regards to technique of creating clear

53
00:05:00,880 --> 00:05:02,920
awareness.

54
00:05:02,920 --> 00:05:09,240
One area awareness of experience is inadequate, as it is present in non-meditators and

55
00:05:09,240 --> 00:05:16,080
even animals and does not produce insight into the nature of reality to the extent necessary

56
00:05:16,080 --> 00:05:20,200
to overcome bad habits and tendencies.

57
00:05:20,200 --> 00:05:26,920
To create the sort of clear awareness of ultimate reality that will facilitate such a state,

58
00:05:26,920 --> 00:05:30,160
three qualities of mind must be present.

59
00:05:30,160 --> 00:05:39,880
These three qualities are taken from the satsipatanas with the majima nakaya and one effort.

60
00:05:39,880 --> 00:05:45,600
In order to make a proper acknowledgement of an experience as it occurs, one cannot merely

61
00:05:45,600 --> 00:05:54,040
say words like rising, falling, and expect to gain any understanding about reality.

62
00:05:54,040 --> 00:06:01,560
One must actively send the mind to the object and keep the mind with the object as it arises

63
00:06:01,560 --> 00:06:08,160
while it persists and until it ceases, whatever object it may be.

64
00:06:08,160 --> 00:06:13,960
In the case of the rising and falling of the abdomen, for example, one must observe the

65
00:06:13,960 --> 00:06:21,880
abdomen itself, sending the mind out to each moment of rising or falling.

66
00:06:21,880 --> 00:06:27,440
Other than repeating the mantra in the head or at the mouth, one must send the mind to

67
00:06:27,440 --> 00:06:34,040
the object and make the note at the location of the experience.

68
00:06:34,040 --> 00:06:36,920
Two, knowledge.

69
00:06:36,920 --> 00:06:42,880
Once one has sent the mind out to the object, one must direct the mind to becoming aware

70
00:06:42,880 --> 00:06:45,200
of the object.

71
00:06:45,200 --> 00:06:51,120
Rather than simply saying, rising and falling while forcing the mind to focus blindly on

72
00:06:51,120 --> 00:06:59,440
the object, one must observe the motion as it occurs from beginning to end.

73
00:06:59,440 --> 00:07:05,280
If the object is pain, one must strive to observe the pain unflinchingly.

74
00:07:05,280 --> 00:07:11,120
If it is a thought, one must observe the thought itself, rather than getting lost in the

75
00:07:11,120 --> 00:07:14,520
content, and so on.

76
00:07:14,520 --> 00:07:17,240
Three, acknowledgement.

77
00:07:17,240 --> 00:07:23,840
Once one is aware of the object, one must make an objective note of the experience, establishing

78
00:07:23,840 --> 00:07:32,240
clear and accurate understanding of the object as it is, avoiding partiality and delusion.

79
00:07:32,240 --> 00:07:38,240
The acknowledgement is a replacement for the distracted thoughts that lead one to extrapolate

80
00:07:38,240 --> 00:07:48,440
upon the object, seeing it as good, bad, knee, mind, and so on.

81
00:07:48,440 --> 00:07:54,080
Rather than allowing the mind to give rise to projection or judgment of the object, one

82
00:07:54,080 --> 00:08:01,000
simply reminds oneself of the true nature of the object as it is, as explained in the

83
00:08:01,000 --> 00:08:03,040
first chapter.

84
00:08:03,040 --> 00:08:08,920
The fourth important fundamental quality of practice is the balancing of one's mental

85
00:08:08,920 --> 00:08:10,480
faculties.

86
00:08:10,480 --> 00:08:16,680
The mind is traditionally understood to have five important faculties beneficial for spiritual

87
00:08:16,680 --> 00:08:18,600
development.

88
00:08:18,600 --> 00:08:32,680
These are one, confidence, two, effort, three, mindfulness, four, concentration, five, wisdom.

89
00:08:32,680 --> 00:08:38,080
These five faculties are of general benefit to the mind, but if they are not properly

90
00:08:38,080 --> 00:08:42,560
balanced, they may actually lead to one's detriment.

91
00:08:42,560 --> 00:08:49,200
For example, a person might have strong confidence but little wisdom, which can lead one

92
00:08:49,200 --> 00:08:55,960
to cultivate blind faith, believing things simply out of self-confidence, and not because

93
00:08:55,960 --> 00:09:00,560
of any empirical realization of the truth.

94
00:09:00,560 --> 00:09:06,880
As a result, one will not bother to examine the true nature of reality, living instead

95
00:09:06,880 --> 00:09:12,640
according to faith in beliefs that may or may not be true.

96
00:09:12,640 --> 00:09:18,760
Such people must examine their beliefs carefully in contrast with reality, in order to

97
00:09:18,760 --> 00:09:24,960
adjust their faith according to the wisdom that they gain from meditation, rather than

98
00:09:24,960 --> 00:09:29,400
pre-judging reality according to their beliefs.

99
00:09:29,400 --> 00:09:35,960
One should one's belief be in line with reality, it will still be weak and unsteady if

100
00:09:35,960 --> 00:09:42,440
not supported by true realization of the truth for oneself.

101
00:09:42,440 --> 00:09:48,720
On the other hand, one might have strong wisdom but little faith, and so doubt one's path

102
00:09:48,720 --> 00:09:52,000
without giving it an honest trial.

103
00:09:52,000 --> 00:09:59,320
Such a person may refuse to suspend their disbelief long enough to make adequate inquiry.

104
00:09:59,320 --> 00:10:05,480
Even when a theory is explained by a respected authority, choosing to doubt and argue rather

105
00:10:05,480 --> 00:10:08,240
than investigating for oneself.

106
00:10:08,240 --> 00:10:13,800
This sort of attitude will make progress in the meditation practice difficult due to the

107
00:10:13,800 --> 00:10:20,800
meditator's lack of conviction, rendering one unable to focus the mind properly.

108
00:10:20,800 --> 00:10:27,000
Such a person must make effort to see their doubt as a hindrance to honest investigation,

109
00:10:27,000 --> 00:10:33,400
and try their best to give the meditation a fair chance before passing judgment.

110
00:10:33,400 --> 00:10:39,560
Likewise one might have strong effort but weak concentration, leaving one's mind to become

111
00:10:39,560 --> 00:10:47,880
distracted often and rendering one unable to focus on anything for any length of time.

112
00:10:47,880 --> 00:10:54,080
Some people truly enjoy thinking or philosophizing about their lives and their problems,

113
00:10:54,080 --> 00:10:59,360
not realizing the stress and distraction that comes from such activity.

114
00:10:59,360 --> 00:11:04,520
Such people are unable to sit still in meditation for any length of time, because their

115
00:11:04,520 --> 00:11:10,160
minds are too chaotic, caught up in their own mental fermentation.

116
00:11:10,160 --> 00:11:14,680
If they are honest with themselves, they should recognize this unpleasant state as

117
00:11:14,680 --> 00:11:21,360
resulting from habitual mental distraction, not from the meditation itself, and patiently

118
00:11:21,360 --> 00:11:28,760
train themselves out of this habit, in favor of simply seeing reality as it is.

119
00:11:28,760 --> 00:11:34,560
Though some mental activity is unavoidable in our daily lives, we should be selective

120
00:11:34,560 --> 00:11:40,200
of what thoughts we give importance to, rather than turning every thought that arises

121
00:11:40,200 --> 00:11:42,800
into a cause for distraction.

122
00:11:42,800 --> 00:11:49,200
Finally, one may have strong concentration but weak effort, which will make one lazy or

123
00:11:49,200 --> 00:11:52,280
drowsy during meditation.

124
00:11:52,280 --> 00:11:58,240
This will keep a meditator from affecting clear observation of reality, as the mind will

125
00:11:58,240 --> 00:12:03,040
incline towards drifting and nodding off to sleep.

126
00:12:03,040 --> 00:12:08,360
People who find themselves drifting off in meditation should practice standing or walking

127
00:12:08,360 --> 00:12:15,120
meditation when they are tired, so as to stimulate their body and mind into a more alert

128
00:12:15,120 --> 00:12:17,240
state.

129
00:12:17,240 --> 00:12:23,520
The fifth faculty, mindfulness, is another word for the acknowledgement or clear awareness

130
00:12:23,520 --> 00:12:26,240
of experience for what it is.

131
00:12:26,240 --> 00:12:32,240
It is the manifestation of a balanced mind, and so it is both the means of balancing the

132
00:12:32,240 --> 00:12:36,840
other faculties and the outcome of balancing them as well.

133
00:12:36,840 --> 00:12:41,640
The more mindfulness one has, the better one's practice will become.

134
00:12:41,640 --> 00:12:48,160
So one must strive both to balance the other four faculties and recognize reality for

135
00:12:48,160 --> 00:12:51,560
what it is at all times.

136
00:12:51,560 --> 00:12:56,800
Mindfulness is, in fact, the best means of balancing the other faculties.

137
00:12:56,800 --> 00:13:04,200
When one has desire or aversion based on overconfidence, one should acknowledge wanting,

138
00:13:04,200 --> 00:13:11,600
wanting, or disliking, disliking, and one will be able to see through one's attachment

139
00:13:11,600 --> 00:13:14,040
to partiality.

140
00:13:14,040 --> 00:13:19,200
When one has doubt, one should note doubting, doubting.

141
00:13:19,200 --> 00:13:23,520
When distracted, distracted, distracted.

142
00:13:23,520 --> 00:13:30,920
When drowsy, drowsy, drowsy, and the condition will correct itself naturally without

143
00:13:30,920 --> 00:13:38,760
special effort due to the intrinsic nature of mindfulness as balancing the mind.

144
00:13:38,760 --> 00:13:44,920
Once one has balanced the faculties, the mind will be able to see every phenomenon as simply

145
00:13:44,920 --> 00:13:52,160
arising and ceasing, without passing any judgment on any object of awareness.

146
00:13:52,160 --> 00:14:01,000
As a result, the mind will let go of all attachment and overcome all suffering without difficulty.

147
00:14:01,000 --> 00:14:07,680
Just as a strong man would easily be able to bend an iron bar, when one's mind is strong,

148
00:14:07,680 --> 00:14:13,360
one will be able to bend and mold and ultimately straighten the mind, freeing it from

149
00:14:13,360 --> 00:14:16,600
all crooked, unwholesome states.

150
00:14:16,600 --> 00:14:23,040
As a result of a balanced mind, one will realize for oneself a natural state of peace

151
00:14:23,040 --> 00:14:28,040
and happiness, overcoming all kinds of stress and suffering.

152
00:14:28,040 --> 00:14:35,720
So this is a basic explanation of the important fundamental qualities of meditation practice.

153
00:14:35,720 --> 00:14:46,160
To summarize, one, one must practice in the present moment, two, one must practice continuously,

154
00:14:46,160 --> 00:14:55,760
three, one must create a clear thought using effort, knowledge, and acknowledgement, four,

155
00:14:55,760 --> 00:14:59,800
one must balance the mental faculties.

156
00:14:59,800 --> 00:15:05,080
This lesson is an important addition to the actual technique of meditation, as the benefits

157
00:15:05,080 --> 00:15:09,000
of meditation come from quality, not quantity.

158
00:15:09,000 --> 00:15:14,640
I sincerely hope that you are able to put these teachings to use in your own practice,

159
00:15:14,640 --> 00:15:21,800
and that you are able to find greater peace, happiness, and freedom from suffering, thereby.

160
00:15:21,800 --> 00:15:40,920
Thank you again for your interest in learning how to meditate.

