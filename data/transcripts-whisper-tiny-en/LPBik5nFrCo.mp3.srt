1
00:00:00,000 --> 00:00:02,000
Do you want that one?

2
00:00:02,000 --> 00:00:04,000
Nope, not that one.

3
00:00:04,000 --> 00:00:07,000
Can there be a desire?

4
00:00:07,000 --> 00:00:09,000
Oh, okay.

5
00:00:09,000 --> 00:00:12,000
I don't see anything selected, so I just pick the top one.

6
00:00:12,000 --> 00:00:19,000
Can there be a desire that isn't harmful?

7
00:00:19,000 --> 00:00:20,000
It's semantics.

8
00:00:20,000 --> 00:00:24,000
I can't really answer this question, because you have to tell me what you mean by desire.

9
00:00:24,000 --> 00:00:32,000
I don't think there can, because I define desires as actually wanting something.

10
00:00:32,000 --> 00:00:35,000
Even that is ambiguous.

11
00:00:35,000 --> 00:00:42,000
There are very few cases where the words wanting or desire could be used that I wouldn't

12
00:00:42,000 --> 00:00:51,000
call a case of craving and therefore harm, but those few cases do exist.

13
00:00:51,000 --> 00:00:59,000
Like a person who desires to be free from suffering, a person who wants to practice meditation.

14
00:00:59,000 --> 00:01:09,000
They're used in a, believe the word, as semantic sense, not a literal sense.

15
00:01:09,000 --> 00:01:14,000
Figurative sense, maybe.

16
00:01:14,000 --> 00:01:21,000
In certain cases, a person can express those desires and wants without actually wanting

17
00:01:21,000 --> 00:01:23,000
or desiring anything.

18
00:01:23,000 --> 00:01:25,000
It's a figure of speech.

19
00:01:25,000 --> 00:01:27,000
I want to practice meditation.

20
00:01:27,000 --> 00:01:36,000
It may not be that I actually want that experience, but it's a way of saying that now

21
00:01:36,000 --> 00:01:40,500
I'm going to practice meditation through because I have the wisdom to know that meditation

22
00:01:40,500 --> 00:01:48,500
is the right thing to do at this point, but I don't consider that to be desire.

23
00:01:48,500 --> 00:01:53,500
So I would consider every desire is harmful, but there are bigger arguments about this that are really just

24
00:01:53,500 --> 00:02:18,500
semantics, so it's kind of silly.

