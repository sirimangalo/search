1
00:00:00,000 --> 00:00:07,920
Hello and welcome back to our study of the Danpada. Today we continue on with first number 124, which reads as follows

2
00:00:09,120 --> 00:00:11,680
Pani me J. Winona sa

3
00:00:12,240 --> 00:00:14,240
Hariya Pani na visan

4
00:00:15,680 --> 00:00:20,240
Nabana visan manvati nati papa nakubato

5
00:00:21,920 --> 00:00:23,920
Which means

6
00:00:23,920 --> 00:00:35,040
If one should not have a wound if there should not be a wound on the hand a cut on the hand

7
00:00:35,920 --> 00:00:37,920
Hariya Pani na visan one

8
00:00:39,280 --> 00:00:45,600
Can pick up one could or might pick up poison with the hand

9
00:00:45,600 --> 00:00:52,480
Or for Nabana visan manvati for poison doesn't enter

10
00:00:54,080 --> 00:00:56,080
It doesn't harm

11
00:00:56,320 --> 00:00:59,280
One without a cut one without a wound

12
00:01:00,720 --> 00:01:02,720
Nati papa nakubato

13
00:01:03,920 --> 00:01:05,920
And likewise evil

14
00:01:07,920 --> 00:01:10,080
There is no evil for one who does not do it

15
00:01:10,080 --> 00:01:16,560
Nati papa nakubato for one a kubato for one who does not perform it

16
00:01:17,360 --> 00:01:19,360
Nati papa there is no evil

17
00:01:21,920 --> 00:01:23,920
So this is actually an important verse

18
00:01:24,960 --> 00:01:26,960
kind of unique in

19
00:01:27,200 --> 00:01:29,440
answering this question a certain question

20
00:01:32,560 --> 00:01:34,560
And so then a bit of backstory

21
00:01:34,560 --> 00:01:39,200
It's a fairly well-known story of the hunter Kukkukkuktamita

22
00:01:42,240 --> 00:01:45,520
The story is more about his wife

23
00:01:47,680 --> 00:01:50,080
Or it's about the two of them so it starts with his wife

24
00:01:51,760 --> 00:01:55,440
His wife it seems became a soda panda when she was very young

25
00:01:56,640 --> 00:02:00,960
So she must have maybe listened to what the Buddha had said or maybe even

26
00:02:00,960 --> 00:02:06,400
Practice meditation based on what another monk can talk but

27
00:02:07,680 --> 00:02:10,000
See it was in Rajagaha, she lived

28
00:02:14,960 --> 00:02:16,960
And kind of like

29
00:02:19,840 --> 00:02:21,360
like

30
00:02:21,520 --> 00:02:23,200
Guesa

31
00:02:23,200 --> 00:02:25,200
Goudna Casey

32
00:02:25,200 --> 00:02:27,520
This woman who became an ascetic

33
00:02:27,520 --> 00:02:30,880
They became a Buddha ascetic and then a Buddhist monk

34
00:02:33,120 --> 00:02:39,600
When she was when she was young they kept her up on the seventh floor of this kind of palace to try and keep her away from men

35
00:02:40,160 --> 00:02:42,400
They remember the story of Goudna the Gasey

36
00:02:43,040 --> 00:02:45,040
How she snuck out while this

37
00:02:46,880 --> 00:02:51,680
Woman the wife of Kukkuktamita did the same thing

38
00:02:51,680 --> 00:02:57,200
But how it happened here is that she

39
00:02:58,000 --> 00:03:04,400
She looked at a window one day and saw hunter Kukkuktamita coming into the city to sell his

40
00:03:07,280 --> 00:03:12,080
Deer he killed deer so he brought 500 probably not 500 but many

41
00:03:14,000 --> 00:03:16,000
many many

42
00:03:16,000 --> 00:03:21,120
Many many deer put them in a cart and brought them into the city to sell

43
00:03:22,240 --> 00:03:27,040
Or cut them up and was selling their flesh and when she saw him she fell in love with him

44
00:03:27,680 --> 00:03:31,760
kind of love it first sight kind of thing and so

45
00:03:32,720 --> 00:03:39,120
She snuck out with the servants by dressing herself in rags and dirtying her base and

46
00:03:40,640 --> 00:03:42,640
and so on

47
00:03:42,640 --> 00:03:45,920
And followed after the hunter as he left the city

48
00:03:46,480 --> 00:03:53,120
Now he tried to get her to go away and she wouldn't go away until finally he realized what she was after and so he

49
00:03:53,600 --> 00:03:58,160
picture up and brought Tukkuktam put her in his cart and carried her on on his way

50
00:04:00,400 --> 00:04:03,680
And they lived together from that time on

51
00:04:07,280 --> 00:04:12,560
Her mother and father of when they found that she was missing they looked for her everywhere couldn't find her and thought that she

52
00:04:12,560 --> 00:04:15,840
Was dead and they had a funeral ceremony for her and everything

53
00:04:16,720 --> 00:04:23,040
But meanwhile she's living in the forest with this hunter and over time had seven sons

54
00:04:24,560 --> 00:04:26,480
And these seven sons got married and

55
00:04:27,360 --> 00:04:31,280
lived with seven women and so all together there were 16 of them

56
00:04:32,000 --> 00:04:34,000
This is the story

57
00:04:36,000 --> 00:04:39,040
And this is the backstory so we come to the present time

58
00:04:39,040 --> 00:04:42,960
Where the Buddha which he was want to do in the morning

59
00:04:44,000 --> 00:04:46,560
They say when the Buddha lay down first to sleep

60
00:04:47,440 --> 00:04:49,680
He he would spend his time

61
00:04:51,840 --> 00:04:57,040
It would lie down during the night to rest and he would send his mind out to the universe trying to

62
00:04:58,240 --> 00:05:00,240
discern just decide

63
00:05:00,800 --> 00:05:02,800
Who would who he would

64
00:05:04,160 --> 00:05:06,160
Teach that thing

65
00:05:06,160 --> 00:05:08,160
Who was ready?

66
00:05:08,560 --> 00:05:15,680
Well, it's it may be someone in the village that he was in or maybe it was someone far away or maybe it was even the angels up in heaven

67
00:05:17,120 --> 00:05:19,120
in one of the heavens and

68
00:05:21,200 --> 00:05:25,520
When he decided when he came upon someone who seemed ready

69
00:05:26,400 --> 00:05:28,640
He would he would find a way to get to them

70
00:05:29,600 --> 00:05:31,600
and so on this day

71
00:05:31,600 --> 00:05:33,600
he saw these 16 people

72
00:05:33,600 --> 00:05:35,600
And

73
00:05:35,600 --> 00:05:37,920
He considered that all of them first of all

74
00:05:37,920 --> 00:05:40,320
He knew that the wife was already a sort upon them

75
00:05:40,880 --> 00:05:43,520
But he knew the rest of them would benefit if he were to go to them

76
00:05:45,440 --> 00:05:48,720
And so he walked through the forest and came upon the nets

77
00:05:50,560 --> 00:05:52,560
And it happened on that day that

78
00:05:53,120 --> 00:05:55,120
The hunter didn't catch anything

79
00:05:55,360 --> 00:06:00,240
So maybe it's somehow karmic or somehow related to the Buddha, but at any rate

80
00:06:00,240 --> 00:06:06,320
He all of his nets were empty all of his nets and his snares and whereas normally he would always catch

81
00:06:08,560 --> 00:06:14,960
Catch in deer on this day. He didn't catch anything and so the Buddha went to one of the nets and stepped

82
00:06:16,480 --> 00:06:18,800
Stepped into the net with his foot leaving a footprint

83
00:06:20,720 --> 00:06:24,160
That the hunter would see leaving tracks enough footprints by the

84
00:06:25,840 --> 00:06:27,840
the net

85
00:06:27,840 --> 00:06:32,960
And so the hunter came by to check on his nets go to meet the

86
00:06:33,600 --> 00:06:38,640
He came to see what he had caught found that he hadn't caught anything and started getting more and more annoyed

87
00:06:39,600 --> 00:06:42,240
to finally he came upon the net with the footprints

88
00:06:43,920 --> 00:06:46,320
And he thought to himself someone's been

89
00:06:50,000 --> 00:06:51,760
Someone's been

90
00:06:51,840 --> 00:06:53,840
freeing these

91
00:06:54,000 --> 00:06:57,360
These deer someone's been setting the deer free has been

92
00:06:57,360 --> 00:06:59,360
Maybe robbing me of my game

93
00:07:08,640 --> 00:07:11,920
And so he went and he followed the tracks and he came upon the Buddha

94
00:07:12,720 --> 00:07:14,480
And here's the story gets a little bit

95
00:07:15,840 --> 00:07:17,840
supernatural and

96
00:07:18,560 --> 00:07:20,560
You know, I don't want to really

97
00:07:20,800 --> 00:07:24,400
Be too hard on these stories because I don't know. I mean magical things could happen

98
00:07:24,400 --> 00:07:29,680
But I know it's hard for a modern audience because we don't have these kind of magical powers now and

99
00:07:30,320 --> 00:07:36,080
Many people are skeptical that anyone ever did have these magical powers. So here's how it here's how it goes down

100
00:07:38,160 --> 00:07:41,440
He sees the Buddha and he and all of his sons

101
00:07:42,560 --> 00:07:45,120
Draw their bows and are ready to shoot the Buddha

102
00:07:45,600 --> 00:07:48,880
That part is is in the story and and that part

103
00:07:49,760 --> 00:07:51,760
Don't have to get into the magic for that

104
00:07:51,760 --> 00:07:56,960
The the white his wife sees

105
00:07:58,320 --> 00:08:01,520
them draw with their bows drawn pointed at the Buddha and

106
00:08:03,520 --> 00:08:05,520
cries out at them

107
00:08:06,800 --> 00:08:08,800
Don't kill my father. She says

108
00:08:11,200 --> 00:08:16,400
Don't kill my father. What are you doing? You're going to kill my father. Please don't

109
00:08:16,400 --> 00:08:20,960
And they were shocked they and they all

110
00:08:23,600 --> 00:08:25,600
Completely sobered up and

111
00:08:26,560 --> 00:08:27,520
Well

112
00:08:27,520 --> 00:08:34,080
We're in shock because they didn't know who her father was obviously because she had lost connection with her family

113
00:08:34,880 --> 00:08:39,120
But here this was her father her father was among of course that wasn't the case, but

114
00:08:39,920 --> 00:08:42,320
This is how Buddhists would look in light and

115
00:08:42,320 --> 00:08:46,720
Buddhists from Sotapana on up will look at the Buddha as being their father

116
00:08:48,640 --> 00:08:50,000
and

117
00:08:50,000 --> 00:08:52,880
So they were all ashamed and and and they

118
00:08:54,080 --> 00:08:57,200
Put their bows down went in pain respect him

119
00:08:57,200 --> 00:09:02,320
This is my father-in-law. This is your grandfather and son and they were all friendly towards him

120
00:09:03,760 --> 00:09:08,240
So see you see I didn't have to get into any supernatural know what that any magic now

121
00:09:08,240 --> 00:09:13,200
The story says is he came and he drew his bow and the Buddha made him unable to shoot

122
00:09:13,360 --> 00:09:18,160
So he froze him in place and then all the seven sons came slowly and they

123
00:09:18,960 --> 00:09:22,160
Held up their bows and they likewise were frozen became frozen in place

124
00:09:22,880 --> 00:09:27,040
And they stayed like that for a while actually but I had them frozen in place

125
00:09:28,240 --> 00:09:29,680
and

126
00:09:29,680 --> 00:09:33,840
The wife was wondering where they'd been and she where they went what happened to them

127
00:09:33,840 --> 00:09:38,800
Why they were back for lunch maybe and so she went out and

128
00:09:38,800 --> 00:09:43,840
Found where they were or the Buddha was just meditating there and they're all frozen in place and then she

129
00:09:44,640 --> 00:09:46,640
cried out

130
00:09:47,440 --> 00:09:49,440
Do not kill my father do not kill my father

131
00:09:50,720 --> 00:09:54,160
So you take which version you prefer. I don't mind

132
00:09:54,160 --> 00:10:05,520
And so they sat down and they asked forgiveness from the Buddha and they listened to his teaching

133
00:10:06,480 --> 00:10:13,200
And upon listening to his teaching all of the other 15 and they called the daughters of the seven daughter-in-law as well

134
00:10:15,920 --> 00:10:20,720
And all together all 16 of 15 of them became so dependent so along with

135
00:10:20,720 --> 00:10:27,200
The wife that made 16 of them were now were now in light and beat or so dependent

136
00:10:29,520 --> 00:10:34,960
Then he went back to the Buddha after preaching he went back to the monastery and Ananda saw him and Ananda

137
00:10:35,440 --> 00:10:40,800
Asked him wherever he been and the Buddha told him what he had done and he said oh did they become

138
00:10:41,920 --> 00:10:47,360
Did they all become so dependent? And he said well, they all did except for except for his wife

139
00:10:47,360 --> 00:10:51,760
But she because she had been a she had been a soap upon it since she was a girl

140
00:10:54,880 --> 00:10:56,880
And

141
00:10:56,880 --> 00:10:58,880
Ananda was impressed and

142
00:10:58,880 --> 00:11:00,480
whatever and

143
00:11:00,480 --> 00:11:04,400
But the monks got winded this because you see the curious thing there is

144
00:11:05,760 --> 00:11:09,440
The most they were talking about it. They thought well, that's understandable. I mean

145
00:11:10,080 --> 00:11:12,320
Angulimala became an aran so

146
00:11:12,320 --> 00:11:15,680
Hunters can become so dependent

147
00:11:16,240 --> 00:11:21,360
But the the problem here is the problem that the monks had when they were discussing it is

148
00:11:23,360 --> 00:11:27,760
She was a sort of benefit from a girl. So what the heck was she doing with a hunter?

149
00:11:29,440 --> 00:11:33,360
Because the so-to-pane is incapable of killing intentionally

150
00:11:34,800 --> 00:11:37,440
Really and a so-to-pane shouldn't be able to perform any

151
00:11:37,440 --> 00:11:44,000
Significantly immoral act like killing or stealing or cheating or lying

152
00:11:46,240 --> 00:11:48,560
Even drugs in alcohol. They wouldn't get involved

153
00:11:53,280 --> 00:11:57,520
And so they said you know what what the heck's going on? How could she have been a so-to-pane when she's

154
00:11:58,320 --> 00:12:00,160
cleaning his traps and his

155
00:12:00,160 --> 00:12:07,600
His arrows and preparing them for him and putting out his bow and arrows and doing all these things that

156
00:12:08,160 --> 00:12:12,400
Ostensibly she would have to do as a hunter's wife may be even cleaning carcasses and so on

157
00:12:13,120 --> 00:12:17,440
You know cooking the meat that he had killed even eating the meat that he had killed

158
00:12:19,120 --> 00:12:21,120
Certainly eating it right

159
00:12:23,360 --> 00:12:29,120
And when he says bring me my bow bring me my arrows bring me my hunting knife knife bring me my net

160
00:12:29,120 --> 00:12:31,120
She would obey him

161
00:12:31,680 --> 00:12:37,120
And then he using that he would go and take life. So how could she do that if she was a so-to-pane

162
00:12:38,720 --> 00:12:40,720
And here's was the Buddha's reply

163
00:12:42,720 --> 00:12:46,320
Monks of course those that have obtained so-to-pane do not take life

164
00:12:46,800 --> 00:12:48,800
Kukuta Mitra's wife did what she did

165
00:12:49,520 --> 00:12:53,440
Because she was actuated by the thought I will obey the commands of my husband

166
00:12:53,440 --> 00:13:01,040
What was the thing in India? It never occurred to her to think he will take what I give him and go hence to take and take life

167
00:13:01,920 --> 00:13:03,920
Never occurred to her to think that

168
00:13:04,560 --> 00:13:08,880
Well, I mean, I suppose that's maybe you're stretching it. Maybe the translations have been off

169
00:13:08,880 --> 00:13:13,920
But she didn't think that wasn't the thought that she had she didn't think about him taking life

170
00:13:15,040 --> 00:13:19,920
If a man's hand be free from wounds even though he take poison into his hand

171
00:13:19,920 --> 00:13:22,560
Yet the poison will not harm him

172
00:13:23,440 --> 00:13:29,360
Persuise precisely so a man who harbors no thoughts of wrong and who commits no evil

173
00:13:29,840 --> 00:13:36,240
may take down bows and other similar objects and present them to another and yet be guiltless of sin

174
00:13:37,840 --> 00:13:40,400
And then he gave his teeth this he thought that's worse

175
00:13:42,160 --> 00:13:45,360
And this is I think this is quite controversial

176
00:13:45,360 --> 00:13:53,600
You know this leads into the controversy that we often face about how Buddhists can eat meat some Buddhists

177
00:13:54,400 --> 00:13:56,400
As Buddhists in my tradition

178
00:13:57,120 --> 00:14:03,200
How can you eat meat? How can you be so hypocritical when you say killing is wrong and yet eating meat is okay?

179
00:14:04,400 --> 00:14:08,080
And it's this verse that really drives that teaching home

180
00:14:09,440 --> 00:14:11,440
Because ethics in Buddhism

181
00:14:11,440 --> 00:14:15,520
is quite different from ethics as we think of it

182
00:14:16,640 --> 00:14:18,640
and so this this kind of

183
00:14:19,200 --> 00:14:21,200
confusion and criticism comes

184
00:14:22,000 --> 00:14:25,280
Quite often as a result because people have such have

185
00:14:26,240 --> 00:14:30,320
Ordinary ideas of ethics something is wrong because it hurts someone else

186
00:14:31,760 --> 00:14:36,400
But you know since you can't know what is going to hurt someone else it makes many things potentially wrong

187
00:14:36,400 --> 00:14:41,520
I mean, there's no you can't then easily draw a line in the sand. I mean, I don't know what

188
00:14:41,520 --> 00:14:43,520
Consequences my actions are going to have

189
00:14:50,960 --> 00:14:53,680
And then you might even you I mean you might argue that

190
00:14:54,640 --> 00:14:59,520
Based on this there's a more like a greater likelihood or so on, but you see you can't draw a line in the sand

191
00:15:01,040 --> 00:15:04,240
Which points to the how problematic that kind of ethics is but

192
00:15:04,240 --> 00:15:12,400
It still misses the point that our ethics are categorically different our ethics don't involve the consequences to others

193
00:15:12,800 --> 00:15:14,800
Ethics are solely how they affect you

194
00:15:15,520 --> 00:15:17,520
So if I hurt you

195
00:15:18,160 --> 00:15:22,720
That's unethical from Buddhist point of view because of the damage it does to my mind

196
00:15:24,560 --> 00:15:27,520
The damage it does to your face is not not really

197
00:15:29,040 --> 00:15:30,240
not

198
00:15:30,240 --> 00:15:32,240
directly

199
00:15:32,240 --> 00:15:40,800
Involved in the equation now if you get angry because I hit you then that's that in and of itself is unethical of you

200
00:15:41,680 --> 00:15:43,680
so if if you

201
00:15:43,920 --> 00:15:48,640
Are sad and are hurt and suffer because of what I did to you

202
00:15:49,280 --> 00:15:53,760
Then that is unethical you suffering is unethical because you're hurting yourself

203
00:15:54,400 --> 00:15:56,400
so it's very much

204
00:15:56,560 --> 00:16:00,560
self-centered and that's another criticism that Buddhism faces

205
00:16:00,560 --> 00:16:02,560
but

206
00:16:04,480 --> 00:16:06,480
It's it's funny how

207
00:16:08,400 --> 00:16:12,000
We really get a lot of hard criticism like this and

208
00:16:12,800 --> 00:16:19,280
Vegetarians will often get quite upset about Buddhists and there's this divide that we can't bridge because

209
00:16:20,080 --> 00:16:23,520
They think they're right and getting upset and we think we think they're wrong

210
00:16:23,520 --> 00:16:30,160
We think you're the unethical and you getting angry at me for sitting here chewing something that is dead is a

211
00:16:30,640 --> 00:16:36,640
Problem the anger that you have me sitting here eating something that is dead is not a problem

212
00:16:37,280 --> 00:16:41,120
I'm just chewing chewing chewing swallow

213
00:16:42,320 --> 00:16:46,160
And so on our ethics is our idea of ethics is quite different

214
00:16:47,280 --> 00:16:49,280
and so if you

215
00:16:49,280 --> 00:16:58,960
And carry the the if you carry poison in your hand it only it only hurts you if if you have a cut and the same idea is with

216
00:16:59,520 --> 00:17:00,640
with acts

217
00:17:00,640 --> 00:17:03,760
So if you if I shoot a bow

218
00:17:05,840 --> 00:17:11,120
Add a treaty and it turns out that the tree is you know

219
00:17:11,440 --> 00:17:14,000
Maybe it's someone dressed up in a tree costume

220
00:17:14,000 --> 00:17:19,440
And the person dies or you know, maybe there's a person in the tree that I didn't see

221
00:17:20,560 --> 00:17:26,160
I'm not guilty. There's no crime in that. There's no problem. On the other hand if I

222
00:17:27,360 --> 00:17:32,720
If I shoot at the tree thinking I see a bird in the tree and it turns out there was no bird in the tree

223
00:17:33,120 --> 00:17:38,960
I've still done a very very evil thing my intention was evil. You see it's still a bad karma

224
00:17:38,960 --> 00:17:43,840
Because I've I had a bad intention

225
00:17:47,360 --> 00:17:52,720
There's an interesting story of the past I suppose I shouldn't go into it because it'll take way too much time

226
00:17:53,600 --> 00:18:00,160
But um, so the key here is understanding ethics and someone recently asked me about

227
00:18:01,360 --> 00:18:06,160
And how it was about monks not being able to act as doctors

228
00:18:06,160 --> 00:18:14,320
and the idea how cruel that would be if you came up on someone who was sick and didn't help them who was dying and didn't help them and

229
00:18:16,800 --> 00:18:18,800
This idea of the cruelty

230
00:18:21,120 --> 00:18:24,240
And the idea of lacking compassion and so on

231
00:18:25,120 --> 00:18:32,160
It's it's really looking at things and this is why people get kind of misled by compassion because

232
00:18:32,160 --> 00:18:43,760
It's just just like in this case of of not being able to do anything being totally paralyzed if you had to really consider the consequences of what you did

233
00:18:44,240 --> 00:18:49,840
But the same token if you were required to help anyone anytime you could then we'd never

234
00:18:50,560 --> 00:18:53,360
Rest we'd collapse in exhaustion

235
00:18:54,560 --> 00:18:58,080
Because we'd always be capable of helping others so the

236
00:18:59,120 --> 00:19:00,880
the idea that

237
00:19:00,880 --> 00:19:06,240
Compassion means helping people when you can or even when you hear about it is

238
00:19:08,160 --> 00:19:14,640
It's it's something that you have to you know has to definitely be tempered with wisdom and understanding

239
00:19:15,280 --> 00:19:19,840
It has to be led by wisdom and understanding if it was just about helping when you can you

240
00:19:20,800 --> 00:19:25,520
Then you never end and you never really help anyone because to and you never really

241
00:19:25,520 --> 00:19:30,400
Change the world because there'd always be more people there will always be

242
00:19:31,280 --> 00:19:34,960
People who need help and furthermore those people who you do help

243
00:19:35,840 --> 00:19:41,360
There's no saying there's no telling what's going to happen in the future except that really in the end they're going to die anyway

244
00:19:44,560 --> 00:19:49,840
So I mean all of it points to the idea of really having a wisdom based

245
00:19:49,840 --> 00:19:54,720
concept of ethics and morality and goodness and compassion

246
00:19:55,600 --> 00:20:02,960
Now definitely you should help people when you have the opportunity you know monks have certain rules and monks are very very

247
00:20:04,080 --> 00:20:06,880
Devoted themselves to a path

248
00:20:08,080 --> 00:20:10,080
but even still

249
00:20:10,080 --> 00:20:15,760
the idea that you have to help people and and there are stories about how you should kill

250
00:20:15,760 --> 00:20:23,520
You know these these dilemmas of how you should kill five people to save one or if there's a time bomb you should

251
00:20:25,520 --> 00:20:27,520
Maybe

252
00:20:27,520 --> 00:20:29,520
Let's put that one aside, but

253
00:20:29,920 --> 00:20:33,440
You keep you doing a lesser evil to prevent a greater evil

254
00:20:34,000 --> 00:20:36,000
but it it implies

255
00:20:37,040 --> 00:20:38,880
that you

256
00:20:38,880 --> 00:20:41,120
You are required to do something

257
00:20:42,080 --> 00:20:44,080
That they're that

258
00:20:44,080 --> 00:20:46,400
It's unethical to do nothing

259
00:20:47,840 --> 00:20:51,760
Or it's an essay or in the case of this story. It's an ethical to

260
00:20:53,280 --> 00:20:55,280
Do anything

261
00:20:55,280 --> 00:20:57,280
related to evil

262
00:20:58,080 --> 00:21:05,120
If if the consequences of the act are evil you see so the consequences of action or the consequences of omission

263
00:21:05,920 --> 00:21:11,440
And this isn't how Buddhism looks at it now if if someone is suffering in front of me

264
00:21:11,440 --> 00:21:16,560
Now technically I can be at peace with myself without helping them

265
00:21:18,640 --> 00:21:20,640
I mean this is possible now

266
00:21:20,640 --> 00:21:26,640
I would argue that in most cases you would not be at peace with yourself in most cases being at peace with yourself

267
00:21:27,440 --> 00:21:32,000
Just naturally is helping the person. I mean that's I think how compassion works

268
00:21:32,720 --> 00:21:34,000
but

269
00:21:34,000 --> 00:21:36,480
in certain circumstances where

270
00:21:36,480 --> 00:21:42,320
Maybe they're being at you know someone's attacking someone else, right?

271
00:21:43,760 --> 00:21:45,760
and

272
00:21:45,760 --> 00:21:52,160
The only way to to to fix the situation would be to maybe harm the first person

273
00:21:54,960 --> 00:21:56,960
Maybe hit them so on

274
00:21:57,200 --> 00:21:59,200
So hurt the other hurt the other person

275
00:21:59,200 --> 00:22:03,200
I

276
00:22:03,360 --> 00:22:06,240
Don't even know then. I think even then you could you could

277
00:22:07,600 --> 00:22:09,600
argue that you should act

278
00:22:09,600 --> 00:22:13,680
But there there is a line you see there's a line which requires

279
00:22:14,480 --> 00:22:16,480
defilement that you wouldn't act

280
00:22:18,000 --> 00:22:20,000
and

281
00:22:20,000 --> 00:22:23,360
The same I think goes for a mid for for the for acts

282
00:22:24,320 --> 00:22:26,400
Like if you if you were to not do it

283
00:22:26,400 --> 00:22:29,760
It would cause more it would cause problems and doing it is

284
00:22:30,560 --> 00:22:31,760
the

285
00:22:31,760 --> 00:22:33,760
Going with the flow

286
00:22:33,760 --> 00:22:35,760
We saw you say while her handing him

287
00:22:36,240 --> 00:22:39,760
His bow and arrow was enabling him. Well, that's not how she thought of it for her

288
00:22:39,760 --> 00:22:44,560
It was a mindful act my husband asked me for something. I get it for him

289
00:22:45,360 --> 00:22:49,200
I remember when I was many many years ago. I was working in a restaurant

290
00:22:50,560 --> 00:22:51,520
and

291
00:22:51,520 --> 00:22:56,400
We had to serve alcohol and I was already practicing meditation. I wasn't a monk yet and

292
00:22:57,120 --> 00:23:01,600
So I had to carry the alcohol to the people and I thought well, this isn't very ethical

293
00:23:01,600 --> 00:23:06,960
You know here. I'm enabling them, but I thought of this verse actually I read the Dhammapada and I

294
00:23:07,600 --> 00:23:13,120
Thought of this verse and so I thought okay. This is what I'm doing. I'm lifting the bottle lifting this thing

295
00:23:13,120 --> 00:23:16,480
You know moving it and placing it. I'm actually doing a good deed

296
00:23:16,480 --> 00:23:22,320
I'm serving someone I really enjoyed being a wager for the short time that I did it because I was serving people

297
00:23:22,320 --> 00:23:26,240
It was it was a kindness really or it was a humbling

298
00:23:28,560 --> 00:23:30,560
Activity

299
00:23:31,840 --> 00:23:33,840
And so

300
00:23:34,000 --> 00:23:39,280
This is important. I mean it's important as Buddhists philosophically to understand where we stand

301
00:23:40,640 --> 00:23:44,320
Understand why we stand that way we stand that we stand

302
00:23:44,320 --> 00:23:46,320
This way because

303
00:23:46,880 --> 00:23:50,000
We're interested in happiness and we're interested in peace and

304
00:23:50,800 --> 00:23:56,880
Because we have our understanding is that you have to find happiness and peace for yourself

305
00:23:56,880 --> 00:24:01,360
No one else can do it for you. No one else can also either make you suffer

306
00:24:03,760 --> 00:24:06,560
If someone hurts you, it's up to you how you react to that

307
00:24:06,560 --> 00:24:13,760
So our our idea of ethics and morality are very much to do with our state of mind

308
00:24:14,320 --> 00:24:16,480
When we do something and if you can eat meat

309
00:24:17,680 --> 00:24:22,000
Without any thoughts of harm and the Buddha said you know you send love and kindness

310
00:24:22,720 --> 00:24:24,800
compassion send out at least thoughts

311
00:24:25,760 --> 00:24:27,760
Then you can do that while you're eating meat

312
00:24:27,760 --> 00:24:36,640
And I know many people don't like this idea. They think this is horrible. I've actually had people get angry at me and

313
00:24:37,760 --> 00:24:40,880
This is there's this divide where you they can't understand us but

314
00:24:42,160 --> 00:24:44,640
My thought it was funny is because it doesn't really matter

315
00:24:44,640 --> 00:24:51,600
You know if you get angry at me and you believe I'm evil and cruel because I won't make medicines for people or because I won't

316
00:24:52,480 --> 00:24:55,120
Go out of my way to help people or so and it doesn't really matter

317
00:24:55,120 --> 00:24:58,560
It's a problem for you because it makes you suffer

318
00:24:59,280 --> 00:25:03,120
I feel sorry for you and I hope you can free yourself from that

319
00:25:04,160 --> 00:25:06,160
but

320
00:25:06,640 --> 00:25:08,640
It really fails, you know, I mean

321
00:25:09,520 --> 00:25:13,200
You can say someone is cruel but cruelty is a state of mind

322
00:25:14,720 --> 00:25:22,160
And we have these sort of nebulous ideas of ethics and morality and goodness and compassion right and wrong

323
00:25:22,160 --> 00:25:24,160
That just

324
00:25:25,280 --> 00:25:28,800
Have much more to do with our partiality. We don't want to see people suffer

325
00:25:28,800 --> 00:25:35,760
We get angry and upset when people suffer when we hear about these animals that get that die or we hear about animals that are killed

326
00:25:36,320 --> 00:25:41,680
We get arrogant and self-righteous and and angry and we think we're in the right

327
00:25:41,760 --> 00:25:43,760
And that's why we end up getting burnt out

328
00:25:44,000 --> 00:25:48,880
You know activists people who work for social justice often burn out because

329
00:25:48,880 --> 00:25:54,960
They are full of emotion and they're full of want and desire for change

330
00:25:56,400 --> 00:25:58,400
And it burns you out

331
00:25:58,800 --> 00:26:03,760
And they never look carefully to see that. Oh, yeah, you know in the end of the universe the world

332
00:26:04,960 --> 00:26:11,200
What the the sun is gonna explode and burn the earth to a crisp anyway and then you can't save the surface

333
00:26:11,200 --> 00:26:13,200
You can't save mankind humankind

334
00:26:13,840 --> 00:26:16,000
You can't solve the problems of the world

335
00:26:16,000 --> 00:26:19,120
There's no logical reason to try

336
00:26:20,640 --> 00:26:25,680
All you can do is what's right do good to do good things

337
00:26:26,640 --> 00:26:28,160
Better yourself

338
00:26:28,160 --> 00:26:32,320
Because your mind stays with you the only thing that stays with you is your mind

339
00:26:33,520 --> 00:26:36,640
It's really a very very solid cystic sort of viewpoint

340
00:26:36,640 --> 00:26:42,240
But I think that's proper it's not that we don't believe whether people exist just that they exist in their own world

341
00:26:42,240 --> 00:26:47,680
We can't change their we can't live their world for them

342
00:26:49,040 --> 00:26:51,600
And whatever good we can do for them is only as a catalyst

343
00:26:55,360 --> 00:26:57,360
I think there's room for

344
00:26:57,520 --> 00:27:02,800
for not for not supporting hunters for even telling hunters what's that it's a you know

345
00:27:03,760 --> 00:27:05,520
problematic

346
00:27:05,520 --> 00:27:07,520
I think there's even room you could argue for

347
00:27:07,520 --> 00:27:13,360
A husband or a wife to tell their husband or their wife that they disagree with hunting that they wish

348
00:27:14,000 --> 00:27:16,240
For beings to not not have to die or something

349
00:27:16,880 --> 00:27:20,560
I think there's room for being a vegetarian if you like. I think it's a good thing

350
00:27:21,360 --> 00:27:28,480
But we have to be very careful when we accuse or when we make claims of what is moral and what is ethical as these monks

351
00:27:29,680 --> 00:27:34,880
And so that so and it has to be based on our understanding of proper understanding of around

352
00:27:34,880 --> 00:27:36,880
So that's the teaching

353
00:27:37,520 --> 00:27:40,320
How it relates to our meditation practice is this is

354
00:27:40,960 --> 00:27:43,440
Meditation is a way that you really understand this

355
00:27:44,160 --> 00:27:47,760
You understand what the universe is made of how it works

356
00:27:48,480 --> 00:27:52,400
and you start to get very logical and rational about things

357
00:27:53,120 --> 00:27:59,120
You stop freaking out or getting upset and judging acts and and people

358
00:28:00,400 --> 00:28:04,800
Like when you see someone who does you hear about someone as evil things or you think

359
00:28:04,800 --> 00:28:06,800
of someone or you know someone who does evil things

360
00:28:07,120 --> 00:28:13,120
You don't think of them as an evil person because you know how it goes. They probably have some very good mind states at times

361
00:28:14,480 --> 00:28:16,480
And even if they don't

362
00:28:16,480 --> 00:28:20,240
It's still just temporary. They can always become a better person, but

363
00:28:20,960 --> 00:28:22,960
More more clearly

364
00:28:23,040 --> 00:28:25,040
They aren't really a person

365
00:28:25,120 --> 00:28:29,360
Those states are just states. It's not an evil person. It's cause and effect

366
00:28:29,440 --> 00:28:31,440
It's got reasons and causes

367
00:28:31,440 --> 00:28:37,760
Where it comes from and it's just states of mind states of body that arise and sees

368
00:28:38,400 --> 00:28:45,680
And to us it's just experiences this person yelling at me is just sound this person hitting me is just a feeling or then pain

369
00:28:47,040 --> 00:28:53,840
This person stealing my things is just a thought that I have that that's mine and that's a person stealing something that belongs to me

370
00:28:55,760 --> 00:29:00,960
Meditation is what helps you understand this and it really I was a vegetarian up until the time

371
00:29:00,960 --> 00:29:04,640
I practiced meditation and then I stopped and even today. I'm still not a

372
00:29:05,600 --> 00:29:12,960
Full of vegetarian. I mean, and I've always thought it's a good thing. You know, I understand that meat comes from a bad place usually

373
00:29:13,680 --> 00:29:17,920
So I like to be vegetarian, but I think that's an important sort of

374
00:29:18,960 --> 00:29:23,920
Attitude to have is not not be too serious or too obsessed with

375
00:29:24,960 --> 00:29:27,600
Trying to fix the world, you know

376
00:29:27,600 --> 00:29:31,600
Trying to solve all the problems. I think on the other hand

377
00:29:31,600 --> 00:29:37,360
It's good not to be complacent and it is good to help. It's good to speak out. It's good to say what's right

378
00:29:38,240 --> 00:29:40,240
It's good to try to help people

379
00:29:40,560 --> 00:29:42,560
It's good to try to be kind and compassionate

380
00:29:44,800 --> 00:29:47,200
But it's not about ethics it's about

381
00:29:48,320 --> 00:29:50,320
really about going with the flow and

382
00:29:51,280 --> 00:29:55,600
To something I mean not quite but going with not the flow of people necessarily

383
00:29:55,600 --> 00:29:57,600
But with the flow of life, which can be

384
00:29:58,720 --> 00:30:02,960
Somewhat friction and create conflict at times

385
00:30:05,440 --> 00:30:09,600
But it has more to do with your state of mind not upsetting the equilibrium in your mind

386
00:30:10,320 --> 00:30:17,760
Setting your mind that are giving rise to greed anger or delusion. That's really all because nothing unethical can come to you

387
00:30:18,640 --> 00:30:20,560
Except for one of through one of these three things

388
00:30:20,560 --> 00:30:28,320
Anyways, it's complex and I think people complicate it and I imagine that I'll probably

389
00:30:28,800 --> 00:30:34,240
For this kind of talk I'll get a lot of people confused and uncertain. I think that's why it's important to meditate

390
00:30:34,240 --> 00:30:36,240
I mean even rather than

391
00:30:36,800 --> 00:30:42,240
Taking anything I've said or any of these things as solid views or dogma or so and

392
00:30:43,120 --> 00:30:47,440
Most important is that you find out for yourself. You learn for yourself. What is ethical?

393
00:30:47,440 --> 00:30:54,240
Don't believe you're you're in guts or you're feeling or you've been taught because we have a lot of wrong ideas and wrong

394
00:30:55,040 --> 00:30:57,040
Habits of understanding

395
00:30:57,440 --> 00:30:58,960
Try to look

396
00:30:58,960 --> 00:31:03,200
Look at reality watch how your mind works learn how your mind works

397
00:31:04,160 --> 00:31:06,800
And come to see for yourself. What is it truly ethical?

398
00:31:10,080 --> 00:31:11,040
So

399
00:31:11,040 --> 00:31:13,920
Anyway, that's the dumb upon of teaching for tonight

400
00:31:13,920 --> 00:31:17,360
Thank you all for tuning in wish you all good practice

