1
00:00:00,000 --> 00:00:06,000
In the video of how you became a monk, you mentioned this wasn't even the most interesting part.

2
00:00:06,000 --> 00:00:11,000
That part started after you became a monk. Can you tell us about the time after your organization?

3
00:00:11,000 --> 00:00:25,000
The reason I can't really is because it deals with monks and lay people who are still alive and still around

4
00:00:25,000 --> 00:00:32,000
and still involved with many of the things and places that I am involved in.

5
00:00:32,000 --> 00:00:45,000
So if I were to tell my story, I suppose I could tell it in such a way as to not incriminate other people.

6
00:00:45,000 --> 00:01:05,000
But it could be a bit potentially problematic at this point in my life.

7
00:01:05,000 --> 00:01:11,000
It's the kind of thing that you'd have to wait for the biography or the autobiography.

8
00:01:11,000 --> 00:01:28,000
One day I would write the autobiography of me where I could sit down and regale you with or reflect upon my past.

9
00:01:28,000 --> 00:01:45,000
But at this point, it sounds like an excuse because I could go through my monk's life and not mention any names,

10
00:01:45,000 --> 00:01:47,000
but talk about the things that happened to me.

11
00:01:47,000 --> 00:01:49,000
Then I met this person, then I met that person.

12
00:01:49,000 --> 00:01:54,000
But that's the problem, then there's people who know those people and know what I'm talking about.

13
00:01:54,000 --> 00:02:02,000
And I have to be very, very careful that I frame things in such a way as to not incriminate people

14
00:02:02,000 --> 00:02:07,000
because there's been a lot of crazy things happen in my monastic way.

15
00:02:07,000 --> 00:02:18,000
So it may be something that has to wait for the distant future when it's all been cleared up and we can all laugh about it.

16
00:02:18,000 --> 00:02:27,000
I suppose I can laugh about it now, but some people are not, you know, I mean it's easy to talk about other people,

17
00:02:27,000 --> 00:02:31,000
but to have other people talk about you is not so nice.

18
00:02:31,000 --> 00:02:34,000
Anyway, that's sort of the idea of why I'm hesitant.

19
00:02:34,000 --> 00:02:56,000
That's the idea.

