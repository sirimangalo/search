1
00:00:00,000 --> 00:00:07,600
Okay, so this is our first monk radio for the wasmosa period, the rain period,

2
00:00:09,600 --> 00:00:17,520
we entered into the rains on the second. There was some confusion because in Thailand it's on the

3
00:00:17,520 --> 00:00:23,440
third and we had all been under the, are we the two of us here had been under the impression

4
00:00:23,440 --> 00:00:35,280
that it was starting on the third. Even, yes, and so I assumed that the full moon, the holiday

5
00:00:35,280 --> 00:00:45,600
would be on the second and on the 31st I asked them, I said, so when, how many, two days is the

6
00:00:45,600 --> 00:00:52,720
boy or something, the holiday and we said, no, tomorrow the first. And so it turned, and then we

7
00:00:52,720 --> 00:00:57,280
were all confused because the holiday was on the first but he said, no, no, then on the second is

8
00:00:57,280 --> 00:01:03,440
the real full moon and then the third is when we enter the rains. Anyway, so it was all confused and

9
00:01:03,440 --> 00:01:12,080
on the second he realized that this was, that we'd missed the full moon day and the second was the

10
00:01:12,080 --> 00:01:17,920
day to go into the rain, so he would quickly left and he's gone now, back to Ratnapura.

11
00:01:19,360 --> 00:01:28,160
And we have, I'm staying here now with just the old monk who's sick. Actually now he's gone to

12
00:01:28,160 --> 00:01:36,720
candy, new water, the city of candy along with the four Thai yogis and the Burmese nun.

13
00:01:36,720 --> 00:01:44,320
And several of the villagers, everyone piled into a van and they wanted me to go as well but

14
00:01:45,840 --> 00:01:49,440
you say they are not back yet, they said they would be back in time for this broadcast

15
00:01:50,160 --> 00:01:52,800
and they're not, they're not going to be back until nine, I don't think.

16
00:01:54,960 --> 00:02:04,800
So I would have missed this broadcast as well as other duties that I had to attend to today.

17
00:02:04,800 --> 00:02:16,000
So I'm here alone right now, waiting for them to come back and this is the rains, except it's

18
00:02:16,000 --> 00:02:19,600
raining very very little and that's a concern because our well is running dry.

19
00:02:22,000 --> 00:02:29,120
Tomorrow Peter is coming, Peter Wolf who is one of our online community members, which is really

20
00:02:29,120 --> 00:02:38,160
you know really exciting to see someone you already know through something very odd as our

21
00:02:39,200 --> 00:02:51,920
community is coming and actually make the or lead to the coming to meditate in a more traditional

22
00:02:51,920 --> 00:03:00,720
sense. So it's reaffirming for the benefit towards the benefit of the online community and

23
00:03:02,560 --> 00:03:08,560
should I think or really makes it seem worthwhile to have the online community.

24
00:03:10,560 --> 00:03:15,600
So he will be here tomorrow morning and then I just got an email that on the

25
00:03:15,600 --> 00:03:24,320
7th or 9th, I think Harish is coming and he's a Sri Lankan from Colombo.

26
00:03:25,840 --> 00:03:31,840
So the Thai people are leaving today, not sure about the novice because she hasn't

27
00:03:31,840 --> 00:03:37,440
disrobed yet, her plan was to disrobe because she still has to clear things with her mother,

28
00:03:37,440 --> 00:03:44,160
but I said if you're not back by 730 I'm not available to help you to disrobe so you'll just have

29
00:03:44,160 --> 00:03:47,840
to stay honest and as a novice and she was like yeah yeah don't worry we'll be back

30
00:03:48,480 --> 00:03:54,240
and we were joking if she's not back then she's just people the other people are going

31
00:03:54,240 --> 00:04:01,360
up to explain to her boss why she's not going back to work tomorrow and her mother who would be

32
00:04:01,360 --> 00:04:10,640
very concerned. So they're leaving and we still have Polly from Finland, he's just finishing his

33
00:04:10,640 --> 00:04:20,960
first course, he's here till October. So yeah things are going well, lots of good things happening.

34
00:04:20,960 --> 00:04:26,000
I started a translation project if you're following my web blog or on the online community you saw

35
00:04:26,000 --> 00:04:33,840
that. Translations are interesting, I was reminded after doing it that you can't just literally

36
00:04:33,840 --> 00:04:41,040
translate the Polly into English, it turns out a little bit awkward so I'm getting some

37
00:04:41,040 --> 00:04:45,920
comments on it, I was reminded of the fact that you do actually need to take some

38
00:04:46,640 --> 00:04:52,720
some literary license I think which is a difficult thing to do because you don't want to change

39
00:04:52,720 --> 00:04:59,440
the meaning but probably have to learn how to finesse it. But so the idea is to do some translations,

40
00:04:59,440 --> 00:05:05,200
the reason for starting to do translations is it's not that I'm looking for more work but

41
00:05:05,840 --> 00:05:09,360
there's been some for a long time there's been this

42
00:05:15,120 --> 00:05:23,760
I guess a difference of opinion, so there's Buddhist, there's Buddhist out there who have

43
00:05:23,760 --> 00:05:29,840
translated the majority of the Buddhist teaching but are not making those teachings available

44
00:05:30,960 --> 00:05:42,400
for the public on the internet and are threatening in some cases, threatening in some cases

45
00:05:42,400 --> 00:05:51,040
just asking anyone who does share their works on the internet to take them down.

46
00:05:51,040 --> 00:06:00,720
So in some cases actually threatening them with legal action and the response when we

47
00:06:00,720 --> 00:06:08,480
disagree with this, the responses will why don't you translate them yourself or in some cases

48
00:06:08,480 --> 00:06:17,840
it's even, I don't see you translating them so it's kind of like CEC, no it's more like

49
00:06:17,840 --> 00:06:24,880
if that's the way it's going to be and we're not going to be able because I've got many of

50
00:06:24,880 --> 00:06:33,520
these for-profit texts in PDF or Doc format in some sort of format that could easily be put

51
00:06:33,520 --> 00:06:41,440
on the internet but I'm not allowed to do it. So rather than create conflict, more conflict

52
00:06:41,440 --> 00:06:49,840
than necessary, I thought I'd do some translating myself and I'm not sure how far it'll get,

53
00:06:49,840 --> 00:06:57,840
I've done one sutta from the Maji Minikaya so far and it wasn't, it's not difficult it doesn't

54
00:06:57,840 --> 00:07:03,920
take that much time and it's a good, the reason why I actually decided in the end to do it is

55
00:07:03,920 --> 00:07:08,560
because it's not difficult and it's something that is very important for monks to engage in

56
00:07:08,560 --> 00:07:17,840
the study of both the scriptures and the poly language. I'm certainly not an expert

57
00:07:17,840 --> 00:07:25,600
a master of the poly language and it's really a help for me to continue my studies in that way so

58
00:07:26,560 --> 00:07:34,960
just another announcement that's on the go now. Not much else, I think that's enough talk

59
00:07:34,960 --> 00:07:43,360
now if there are any questions we can get on with the show.

