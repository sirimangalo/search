1
00:00:00,000 --> 00:00:11,940
Okay, here we are with Michael, so first question, why did you start meditating?

2
00:00:11,940 --> 00:00:17,120
What was it that made you think I got to try the... I got to try meditating.

3
00:00:17,120 --> 00:00:26,840
Right. Well, the genius of meditation, it isn't rooted in metaphysics, but is very deeply

4
00:00:26,840 --> 00:00:32,840
an intensely psychological. So what meditation teaches you to do is to just be

5
00:00:32,840 --> 00:00:37,320
aware and mindful of literally everything that happens in your life and through

6
00:00:37,320 --> 00:00:42,520
that, be able to see what is actually causing you all the pain and the things

7
00:00:42,520 --> 00:00:46,160
that you don't like. So in a way you kind of distance yourself from it and you

8
00:00:46,160 --> 00:00:52,520
you get this understanding that just obliterates pretty much everything. If you

9
00:00:52,520 --> 00:00:55,400
take it seriously enough, you can be sort of invincible in a way.

10
00:00:55,400 --> 00:00:59,240
So, but like did you have problems that you thought meditation would help you in?

11
00:00:59,240 --> 00:01:01,720
No, yeah. For sure. For sure.

12
00:01:01,720 --> 00:01:06,680
What sort of problems? Well, you know, just like the typical everyday family stuff, of

13
00:01:06,680 --> 00:01:11,240
course, but the other thing is too, you know, you look around in the state of the

14
00:01:11,240 --> 00:01:17,160
world and you don't really know how to help it. But then you...

15
00:01:17,160 --> 00:01:20,280
So you started meditation thinking you could help people with it or...

16
00:01:20,280 --> 00:01:24,080
A little bit, because you help yourself. You start understanding why people feel the way

17
00:01:24,080 --> 00:01:28,680
they do. And then through that you educate yourself in a way, you know what I mean?

18
00:01:28,680 --> 00:01:35,200
So what was your first experience of meditation night? Well, the beauty of

19
00:01:35,200 --> 00:01:39,840
YouTube is that you can really search anything and you can find it. And from

20
00:01:39,840 --> 00:01:50,880
from that on, I got into a lot of Tibetan Buddhism at first and so you

21
00:01:50,880 --> 00:01:57,120
started meditating the YouTube. Yeah, yeah. I um, where I'm from, there's not a

22
00:01:57,120 --> 00:02:00,560
whole lot of resources that I can get into. There's not a whole lot of temples, not

23
00:02:00,560 --> 00:02:05,280
even meditation centers. So I had to start with YouTube and of course, I

24
00:02:05,280 --> 00:02:09,120
truly had to find you and all that. But it was really from YouTube when I

25
00:02:09,120 --> 00:02:11,760
learned about it and then I started really reading a lot of books and

26
00:02:11,760 --> 00:02:15,760
getting into it from there and it just kind of exponentially got better.

27
00:02:15,760 --> 00:02:21,680
When you got here and started meditating, was this your first intensive

28
00:02:21,680 --> 00:02:26,080
experience? Oh yeah, for sure. How was that? How were your first few days here?

29
00:02:26,080 --> 00:02:31,360
The first few days were, I don't want to say hell or awful, but it was

30
00:02:31,360 --> 00:02:35,280
definitely a huge adjustment, you know, when you're so used to just doing

31
00:02:35,280 --> 00:02:39,120
stuff and thinking and all this, all this stuff. And it was really maybe

32
00:02:39,120 --> 00:02:43,520
we can have into where you finally get settled, you get the routine and you

33
00:02:43,520 --> 00:02:48,960
get, I guess, comfortable enough to where you can actually sit down and

34
00:02:48,960 --> 00:02:53,120
meditate for two hours at a time, take a break, and then keep doing that and

35
00:02:53,120 --> 00:02:55,360
keep doing that. And then it gets to the point to where you're just like,

36
00:02:55,360 --> 00:02:59,520
wow, I don't want to do anything else, but meditate. But it's very hard and

37
00:02:59,520 --> 00:03:02,720
it's, it's, it's really intensive.

38
00:03:02,720 --> 00:03:06,720
And everything during the first few days, I don't know what I got myself.

39
00:03:06,720 --> 00:03:10,640
Yeah, for sure. Maybe as you go home. Yeah. Oh, yeah.

40
00:03:10,640 --> 00:03:14,480
Oh, yeah. Is there any period of time where you really weren't sure if

41
00:03:14,480 --> 00:03:18,480
you were going to be able to make it and maybe I, I think I should go home

42
00:03:18,480 --> 00:03:20,000
better. Oh, yeah.

43
00:03:20,000 --> 00:03:22,560
You're stuck. Oh, for sure. Oh, for sure.

44
00:03:22,560 --> 00:03:23,600
How do you deal with that?

45
00:03:23,600 --> 00:03:28,560
Well, um, one is you accept that you can't leave. There's no way you actually

46
00:03:28,560 --> 00:03:32,400
can. What does that mean? Uh, well, I mean, other than catch a bus and

47
00:03:32,400 --> 00:03:34,720
get it all go all the way back to the, uh, the airport.

48
00:03:34,720 --> 00:03:38,160
I thought you've done maybe like what I often tell people is

49
00:03:38,160 --> 00:03:41,120
you can leave this place, but, but you take everything with you.

50
00:03:41,120 --> 00:03:45,440
Yeah. Yeah. That's true too. But that's not, you, you just, well, that's,

51
00:03:45,440 --> 00:03:50,080
that's the other thing too is, is that, you know, you, you don't want to, um,

52
00:03:50,080 --> 00:03:53,280
you want to finish it, you know, you, you know, it's just like, you know, there's

53
00:03:53,280 --> 00:03:56,400
work to be done. Exactly. And you know, there's something on the other side of

54
00:03:56,400 --> 00:03:59,120
this wall, you know, if I could just get over this wall,

55
00:03:59,120 --> 00:04:02,720
no matter how awful it is, because obviously it's working for a lot of people,

56
00:04:02,720 --> 00:04:06,080
you know, so, you know, it kind of, you, you kind of have to be really

57
00:04:06,080 --> 00:04:11,040
determined and really into it. Um, otherwise the, uh,

58
00:04:11,040 --> 00:04:14,000
you just, it's just not going to work. If you continue to fight it and, you know,

59
00:04:14,000 --> 00:04:18,160
you just like despair, like, what am I doing? I just wasted 600 bucks coming up

60
00:04:18,160 --> 00:04:21,680
here and stuff like that. So one, one of the things that people that we claim

61
00:04:21,680 --> 00:04:27,840
about meditation is it helps you face and deal with your, uh,

62
00:04:27,840 --> 00:04:35,760
problems or your, uh, mental issues. What, were there any issues that you found

63
00:04:35,760 --> 00:04:42,160
yourself confronting that may be surprised you or, or, or that, um,

64
00:04:42,160 --> 00:04:47,200
you didn't realize about yourself? Oh, yeah. A lot of, a lot of subtle little

65
00:04:47,200 --> 00:04:50,960
nuances just, um, obviously, you know, with the, with the, the

66
00:04:50,960 --> 00:04:54,640
bigger problems, but what I really found so profound were just a small

67
00:04:54,640 --> 00:04:58,080
little, little habits, little mental habits that you would, you would get.

68
00:04:58,080 --> 00:05:00,320
And you wouldn't, you're not even aware that you're doing it. And then you,

69
00:05:00,320 --> 00:05:03,440
you really feel so stupid after it that you, you realize that this is

70
00:05:03,440 --> 00:05:07,280
happening and like, wow, I mean, it could just be, I mean, I, I don't even

71
00:05:07,280 --> 00:05:10,480
know how to explain it, you know, just just small, little, little

72
00:05:10,480 --> 00:05:14,560
tidbits of, of thought process that don't really do anything for you.

73
00:05:14,560 --> 00:05:19,360
But, end up going into, you know, it's like, it's like a branches on a tree

74
00:05:19,360 --> 00:05:23,760
that is branch out into this more of this, this crap that you have to deal with.

75
00:05:23,760 --> 00:05:27,840
So I mean, I think that's where I found the most, uh, the most

76
00:05:27,840 --> 00:05:31,600
improvement, you know, obviously, you know, the bigger problems help. But,

77
00:05:31,600 --> 00:05:37,760
and did it surprise you, like, did you, did you, what you experienced in the

78
00:05:37,760 --> 00:05:41,520
meditation? Was it sort of more or less what you expected coming in or was

79
00:05:41,520 --> 00:05:45,280
there were there elements that you didn't expect? Did it, did it shock you?

80
00:05:45,280 --> 00:05:50,240
Did it change you? Did it change? Was it a life changing experience?

81
00:05:50,240 --> 00:05:55,200
Well, um, life changing makes it sound huge. And I mean, it is, it is a life

82
00:05:55,200 --> 00:05:59,040
changing, but not, not in the way I think, uh, at least to me that I, I thought,

83
00:05:59,040 --> 00:06:02,640
you know, I came in here expecting all. I didn't really know to expect, actually, I

84
00:06:02,640 --> 00:06:05,840
just expected to come out of here and be a happier, better person, which I am.

85
00:06:05,840 --> 00:06:10,160
But not at all in the ways I, I would think, you know, and, uh, it's very hard to

86
00:06:10,160 --> 00:06:16,560
describe until you actually do it, you know, and, uh, and it's really cool. It really is.

87
00:06:16,560 --> 00:06:19,440
So tell us now about the cool of our, what, what are some of the

88
00:06:19,440 --> 00:06:24,480
benefits that, first of all, what are some of the differences you see about

89
00:06:24,480 --> 00:06:29,920
yourself coming out of the course as opposed to that that weren't there or that

90
00:06:29,920 --> 00:06:35,120
we're different going into the course? Um, well, for starters, I've learned to

91
00:06:35,120 --> 00:06:43,280
really, um, whenever you, you feel any kind of strong emotion, um, you, I

92
00:06:43,280 --> 00:06:48,240
thought at least I find myself kind of distancing myself in a, in a way. Um,

93
00:06:48,240 --> 00:06:51,840
like if I'm feeling upset about anything, I don't really take it too

94
00:06:51,840 --> 00:06:55,680
seriously. I don't really put too much, you know, effort into it because, I mean,

95
00:06:55,680 --> 00:06:58,880
if you, if you sink into it and you let it, you know, you let it consume you

96
00:06:58,880 --> 00:07:02,880
pretty much, you know, it just, it kind of just keeps going and keeps going into

97
00:07:02,880 --> 00:07:06,240
this big, like, cycle that you just can't get out of. I don't ignore it.

98
00:07:06,240 --> 00:07:09,360
I just acknowledge it and I'm able to deal with it a lot better. You know, I,

99
00:07:09,360 --> 00:07:13,760
I don't let it consume me. Um, and then the other thing is too, I just, I find

100
00:07:13,760 --> 00:07:18,800
myself just not thinking about extraneous things, you know, no matter what

101
00:07:18,800 --> 00:07:24,240
they are, you know, it could be, I don't, I don't really plan ahead.

102
00:07:24,240 --> 00:07:28,560
Not, not that I don't plan ahead, but not, like, just not huge in the future.

103
00:07:28,560 --> 00:07:33,520
And, um, I don't find myself wishing for things or, or dreaming.

104
00:07:33,520 --> 00:07:38,000
But like, this is different from the four before you're, before you practice for

105
00:07:38,000 --> 00:07:45,440
you. Doing all that pretty much, pretty much. Um, not, not like a

106
00:07:45,440 --> 00:07:50,160
super, like, psych ward way, but, um, I, I guess the best way to say it is I,

107
00:07:50,160 --> 00:07:54,320
I wasn't fully always aware. I wasn't always present. I was always spending

108
00:07:54,320 --> 00:07:59,760
way too much time up here versus just kind of, you know, actually seeing

109
00:07:59,760 --> 00:08:02,880
things for how they are. You know, I was, I was making a lot of my

110
00:08:02,880 --> 00:08:08,800
problems in a way. You know, I mean, that's, that's where you'll really find

111
00:08:08,800 --> 00:08:12,640
the most improvement, I think, I think. And through that,

112
00:08:12,640 --> 00:08:17,760
you're, you're much happier and much better and more, more peaceful,

113
00:08:17,760 --> 00:08:21,280
more tranquil, I guess, is a good, good term.

114
00:08:21,280 --> 00:08:25,440
What can you say in general now? If you were to give it people advice or

115
00:08:25,440 --> 00:08:30,400
encouragement, let's start with encouragement. Um, what, what, what would you say

116
00:08:30,400 --> 00:08:34,160
are the general benefits that you've said some, but, but maybe going a little

117
00:08:34,160 --> 00:08:38,480
bit more general for people? What would you say are the, the, the best

118
00:08:38,480 --> 00:08:44,320
reasons that they should find to practice this? Right. To go through the, the

119
00:08:44,320 --> 00:08:48,160
course that you've just gone through. Right. Let's see, that you, you

120
00:08:48,160 --> 00:08:54,640
really, really end up just, um, understanding people for what they do and

121
00:08:54,640 --> 00:08:57,840
why, why they are, you, you get patients, you know, and you're able to deal with

122
00:08:57,840 --> 00:09:00,720
people no matter how much they may piss you off or do something that you

123
00:09:00,720 --> 00:09:03,760
don't think is right. You know, you just understand that they don't know.

124
00:09:03,760 --> 00:09:08,880
You know, and the benefits of that is that you can actually teach them

125
00:09:08,880 --> 00:09:13,120
now. Why, why what they're doing is wrong and how that's wrong

126
00:09:13,120 --> 00:09:16,400
through just awareness. It's not a matter of opinion or, or anything like that.

127
00:09:16,400 --> 00:09:19,760
You just, you show them the way and they teach themselves and

128
00:09:19,760 --> 00:09:23,360
you're able to do that now, which is, which is good. And I think, I think it's

129
00:09:23,360 --> 00:09:27,120
healthy for people to be able to have patients and understanding for people

130
00:09:27,120 --> 00:09:32,160
instead of just kind of, well, he's a bad person or no, it's just, you know,

131
00:09:32,160 --> 00:09:35,520
you don't, you don't understand them and they don't understand you.

132
00:09:35,520 --> 00:09:40,000
Kind of thing.

133
00:09:40,000 --> 00:09:55,680
Okay. Well, thank you. You're welcome. Thank you.

