1
00:00:00,000 --> 00:00:04,780
So, follow up on equanimity.

2
00:00:04,780 --> 00:00:08,480
The buffalo is an equilibrium, isn't it?

3
00:00:08,480 --> 00:00:17,220
Can be broken, unlike equanimity.

4
00:00:17,220 --> 00:00:23,340
So okay, this is important because there is a lot of misunderstanding about what is meant

5
00:00:23,340 --> 00:00:31,840
by equanimity and misunderstanding to the extent that people think equanimity is the most important

6
00:00:31,840 --> 00:00:43,680
and will actually create a sankara means they will push themselves into an equanimity state.

7
00:00:43,680 --> 00:00:51,120
And it almost becomes a mantra or becomes a motto or a habit, not a sankara that you're

8
00:00:51,120 --> 00:00:57,520
creating a formation in the mind.

9
00:00:57,520 --> 00:01:06,920
And a good way to understand and to overcome this is to study the abidama and to see where

10
00:01:06,920 --> 00:01:08,000
equanimity fits in.

11
00:01:08,000 --> 00:01:10,360
Equanimity is all across the board.

12
00:01:10,360 --> 00:01:17,320
There's a lot of unwholesome states that arise with equanimity.

13
00:01:17,320 --> 00:01:21,280
And then there's wholesome states that arise with pleasure.

14
00:01:21,280 --> 00:01:26,980
So a person can be happy doing the right thing.

15
00:01:26,980 --> 00:01:27,980
Meditators often miss this.

16
00:01:27,980 --> 00:01:34,260
They think they have to be, you know, dour faced and a flat lining to be meditating.

17
00:01:34,260 --> 00:01:37,120
But you can be very happy meditating.

18
00:01:37,120 --> 00:01:44,700
You can be very happy doing great things, theoretically, because wholesomeness can arise

19
00:01:44,700 --> 00:01:49,600
with either equanimity or pleasure.

20
00:01:49,600 --> 00:01:52,160
Wholesomeness can arise with a pleasurable feeling.

21
00:01:52,160 --> 00:01:56,240
Unwholesomeness can arise with all three types of feelings, a pleasurable feeling and neutral

22
00:01:56,240 --> 00:01:58,720
feeling and a painful feeling.

23
00:01:58,720 --> 00:02:09,560
So when we talk about the buffalo experience equanimity, from a point of view of the abidama,

24
00:02:09,560 --> 00:02:17,480
that is equanimity, but the point is not to differentiate it to be something other than

25
00:02:17,480 --> 00:02:18,480
equanimity.

26
00:02:18,480 --> 00:02:20,280
It's to see that equanimity isn't the point.

27
00:02:20,280 --> 00:02:24,400
The point is wisdom.

28
00:02:24,400 --> 00:02:37,000
Because a result of the attainment of wisdom is a form of equanimity that isn't a feeling.

29
00:02:37,000 --> 00:02:41,800
It isn't the feeling of, it isn't a sensation of equanimity.

30
00:02:41,800 --> 00:02:43,800
It's a judgment.

31
00:02:43,800 --> 00:02:45,840
It's a non-judgment.

32
00:02:45,840 --> 00:02:48,480
It's in terms of our judgment of the experience.

33
00:02:48,480 --> 00:02:54,160
A person who is in, for instance, San Garupakañana, which is the knowledge of the equanimity,

34
00:02:54,160 --> 00:03:00,440
knowledge of equanimity towards all formations, might have great pleasure, might be very happy

35
00:03:00,440 --> 00:03:02,120
in that state.

36
00:03:02,120 --> 00:03:05,640
There might be pleasurable feelings arising, and they might disappear.

37
00:03:05,640 --> 00:03:09,480
They will come and they will go to the neutral feelings, they will be pleasant feelings.

38
00:03:09,480 --> 00:03:25,160
San Garupakañana is the highest of the world in knowledge before the experience of cessation.

39
00:03:25,160 --> 00:03:27,640
But what is missing is the judgment.

40
00:03:27,640 --> 00:03:39,360
So a buffalo can have an equanimity feeling, but they don't have this, well, they obviously

41
00:03:39,360 --> 00:03:40,360
don't have this wisdom.

42
00:03:40,360 --> 00:03:41,880
They're nowhere near.

43
00:03:41,880 --> 00:03:43,480
They're on the total polar opposite.

44
00:03:43,480 --> 00:03:48,920
I mean, the truth is, buffaloes are not that dumb, but the stereotype of a buffalo is that

45
00:03:48,920 --> 00:03:57,320
it's really the opposite of why it's ignorant.

46
00:03:57,320 --> 00:04:02,960
So all we have to do is realign our thinking that the point is certainly not to become

47
00:04:02,960 --> 00:04:11,280
equanimists, and the point is to become wise, and wisdom leads to non-judgment.

48
00:04:11,280 --> 00:04:17,080
You might, we should understand the equanimity that we talk about in a Buddhist sense is

49
00:04:17,080 --> 00:04:19,160
just non-judgment.

50
00:04:19,160 --> 00:04:32,000
It's not experiencing something flatlined, like a feeling, and it's certainly, the most

51
00:04:32,000 --> 00:04:34,520
important thing certainly isn't the equanimity.

52
00:04:34,520 --> 00:04:40,600
It's the understanding and the seeing the object for what it is, which a buffalo isn't

53
00:04:40,600 --> 00:04:41,600
capable.

54
00:04:41,600 --> 00:04:48,240
The reason why a buffalo or most animals experience equanimity is because of their inability

55
00:04:48,240 --> 00:05:01,400
to give rise to enough thought, like a young child, is unable to give rise to the level

56
00:05:01,400 --> 00:05:06,640
of thought required to hold a grudge, for example.

57
00:05:06,640 --> 00:05:12,160
So people always say that children are so pure and innocent, but child or children are

58
00:05:12,160 --> 00:05:17,800
neither pure nor innocent, and anyone who knows anything about children can tell you this.

59
00:05:17,800 --> 00:05:22,760
And they're simple in the same way that animals are simple, because the mind hasn't developed.

60
00:05:22,760 --> 00:05:29,760
So I've had this question recently of a child who stole something from a store, and she

61
00:05:29,760 --> 00:05:34,880
was actually going to be arrested, and she had this kid who couldn't even write her name

62
00:05:34,880 --> 00:05:37,640
was forced to scribble on a piece of paper.

63
00:05:37,640 --> 00:05:42,920
The security guard forced her, you sign your name on this whatever, and they were going

64
00:05:42,920 --> 00:05:45,320
to have her arrested, and so on.

65
00:05:45,320 --> 00:05:49,760
I think that their parents were like, look, she's four years old, and she can't even write

66
00:05:49,760 --> 00:05:50,760
her name.

67
00:05:50,760 --> 00:05:55,280
She can't write a wildest type of scribble, and so she had to scribble with a pen on

68
00:05:55,280 --> 00:05:59,760
this piece of paper.

69
00:05:59,760 --> 00:06:11,080
Animals are in that realm, so they don't have the ability to manifest the kinds of things

70
00:06:11,080 --> 00:06:13,640
that we would call judgment, but they're judging all the time.

71
00:06:13,640 --> 00:06:17,360
Everything is a judgment for an animal.

72
00:06:17,360 --> 00:06:20,760
There is liking, and there is disliking.

73
00:06:20,760 --> 00:06:25,680
Mostly there is delusion, and I guess what you might say is that delusion itself leads

74
00:06:25,680 --> 00:06:34,000
to a certain amount of equanimity, because it doesn't allow for rational thinking, it doesn't

75
00:06:34,000 --> 00:06:41,080
allow for the mind to gather itself together long enough to make a judgment.

76
00:06:41,080 --> 00:06:44,640
Okay?

77
00:06:44,640 --> 00:06:47,000
Nothing to end.

78
00:06:47,000 --> 00:06:52,680
Oh, maybe yes.

79
00:06:52,680 --> 00:07:03,440
I think because Dhamma is writing equanimity is difficult as a concept, because of the Western

80
00:07:03,440 --> 00:07:15,040
philosophical construct of absolute good or evil, whereas there are only good or bad acts.

81
00:07:15,040 --> 00:07:25,160
Yes in our Western minds, equanimity is something different and difficult, maybe, than it

82
00:07:25,160 --> 00:07:34,080
is in the Buddhist sense.

83
00:07:34,080 --> 00:07:41,600
We have the idea that equanimity is just not reacting and being ignorant, but in the

84
00:07:41,600 --> 00:07:47,760
Buddhist sense, this is not the case, this is not what is meant by equanimity.

85
00:07:47,760 --> 00:07:50,000
That's a good point.

86
00:07:50,000 --> 00:08:01,600
You see, because a buffalo may not react to things, but I still say they are partial.

87
00:08:01,600 --> 00:08:07,960
But they may be inhibited from being partial due to their delusion, but it's certainly not

88
00:08:07,960 --> 00:08:08,960
the most important.

89
00:08:08,960 --> 00:08:17,320
Simon says, not go looking for equanimity states, yes, don't go looking for any states,

90
00:08:17,320 --> 00:08:21,480
because as soon as you start to look for a certain state, you're creating a sankara,

91
00:08:21,480 --> 00:08:27,040
you're creating a habit in the mind, and Buddhism is very much about the deconstruction

92
00:08:27,040 --> 00:08:36,800
of habits, and creating a natural state of being, you're seeing with wisdom will lead

93
00:08:36,800 --> 00:08:40,800
us not to judge and divide reality into good and bad.

94
00:08:40,800 --> 00:08:41,800
Sounds really good.

95
00:08:41,800 --> 00:08:55,360
Okay, we're going to miss, we're losing here, yes, so you got that, no.

96
00:08:55,360 --> 00:09:05,320
Point being that equanimity isn't, equanimity isn't the most important, the most important

97
00:09:05,320 --> 00:09:12,000
is wisdom, is that, we have friends to read what could pitagaras, yeah, I don't know

98
00:09:12,000 --> 00:09:41,840
which, we've lost this question now, oh, we're still recording.

