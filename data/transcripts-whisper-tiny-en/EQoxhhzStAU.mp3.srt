1
00:00:00,000 --> 00:00:05,760
I am welcome back to our study of the Damapada. Today we continue on with verse

2
00:00:05,760 --> 00:00:32,040
number 60 that reads as follows.

3
00:00:32,040 --> 00:00:46,160
The night is long for one who is alert or awake. One who is full

4
00:00:46,160 --> 00:00:58,320
of energy. The young Santasayo is in a league is long for one who is tired was

5
00:00:58,320 --> 00:01:12,720
weary. Digo Balana Santasayo is long wandering on is long for fools sat

6
00:01:12,720 --> 00:01:20,760
Damamang Abhijanatam who don't. Not knowing the satam or the good damma. So we have

7
00:01:20,760 --> 00:01:32,680
three types of things that are long. The story behind this verse is regarding

8
00:01:32,680 --> 00:01:44,400
a certain man but actually more regarding Pascainati but I think the name of it

9
00:01:44,400 --> 00:01:52,600
is Anyatara Bodhisattva, a story about some man or other. The story goes there was

10
00:01:52,600 --> 00:01:56,560
this man who had a very beautiful wife that he kept up on the top floor of his

11
00:01:56,560 --> 00:02:05,800
house or whatever she was in the house and there was a festival and King Pascainati

12
00:02:05,800 --> 00:02:24,520
when riding on his white elephant through the city. The sun-wise it says whatever it means maybe that would be a clockwise no.

13
00:02:24,520 --> 00:02:33,360
And as he was completing his journey around the city he looked up on his

14
00:02:33,360 --> 00:02:41,880
elephant and saw this woman standing in the window at the top floor of her house

15
00:02:41,880 --> 00:02:48,480
and she stood there for a second and he looked at her and then she went back

16
00:02:48,480 --> 00:02:56,880
into her house and he was totally enchanted by this completely enchanted and

17
00:02:56,880 --> 00:03:05,160
taken away by this woman's beauty and vowed that she should be his wife, his queen.

18
00:03:05,160 --> 00:03:11,760
So he completed his trip around the city, went back to the palace and he called

19
00:03:11,760 --> 00:03:18,400
his minister up to see him, one of his attendants and said did you see the, did you see

20
00:03:18,400 --> 00:03:21,480
that woman there near the end of the trip? He said yes I saw the woman.

21
00:03:21,480 --> 00:03:27,360
Go and find out if she has a husband and so they sent a man to the house and to

22
00:03:27,360 --> 00:03:33,440
go and find out what was up with this woman come back and says yes she's got a

23
00:03:33,440 --> 00:03:43,040
husband and so he says to him okay bring her bring her husband here and they

24
00:03:43,040 --> 00:03:50,520
have the husband brought before the king and the husband somehow knows I think

25
00:03:50,520 --> 00:03:54,720
I can't remember exactly how it was when he knows he gets an idea because he knows how

26
00:03:54,720 --> 00:03:58,880
beautiful his wife is and so he knows there's something going on and he goes

27
00:03:58,880 --> 00:04:05,880
before the king and the king says from now on I want you to be my servant and

28
00:04:05,880 --> 00:04:11,000
he says oh but I'm quite happy doing the work that I do in your majesty.

29
00:04:11,000 --> 00:04:16,080
Well regardless from now on from this day forward you are to be my servant

30
00:04:16,080 --> 00:04:20,960
of course if you don't listen to the king he's he can cut off your head and so

31
00:04:20,960 --> 00:04:27,960
the king thinks to himself that he'll find some way to find fault in this man

32
00:04:27,960 --> 00:04:34,000
and cut off his head and take this guy's wife and he spends a lot of time

33
00:04:34,000 --> 00:04:38,560
trying to do that he he hasn't gone dangerous errands and fight in battles

34
00:04:38,560 --> 00:04:44,040
but he's so meticulous and so concerned and he's so aware that the king is

35
00:04:44,040 --> 00:04:50,160
is out to get him that he performs his job flawlessly and the king is

36
00:04:50,160 --> 00:04:57,240
unable to find any fault in him so the king finds finally he he he finds this

37
00:04:57,240 --> 00:05:02,200
scheme he sends him on an errand to get he says I'm going look I'm going to

38
00:05:02,200 --> 00:05:11,040
bathe and I want this special red clay and I think lotus flowers as well white

39
00:05:11,040 --> 00:05:16,960
lotus flowers that can only be got in a location that is one yoh jana

40
00:05:16,960 --> 00:05:25,720
away when a yoh jana is about twelve kilometers so he says I want and I wanted

41
00:05:25,720 --> 00:05:29,280
there before my bath which is in like an hour or two hours or something or not

42
00:05:29,280 --> 00:05:33,440
two hours but it's it's in a short time short so sure that he knows it'd be

43
00:05:33,440 --> 00:05:40,280
very difficult to make it so he goes back to his home and he gets his wife to

44
00:05:40,280 --> 00:05:44,440
make him some or his wife is making making the food he said it's the rice

45
00:05:44,440 --> 00:05:47,840
ready and she said no it's not cooked yet so he takes some half cooked rice and

46
00:05:47,840 --> 00:05:51,480
puts it in a pot and carries it with him and it cooks well he's on the journey

47
00:05:51,480 --> 00:05:58,600
he was running we're running twelve twelve kilometers and he gets to this place

48
00:05:58,600 --> 00:06:06,000
and you know I'd like to just say he finds the the the clay and the lotus

49
00:06:06,000 --> 00:06:11,920
flowers but it's not that easy the the story goes and so let's let's say the

50
00:06:11,920 --> 00:06:25,240
un-elaborated or un-extrapulated no what's the un what do you call

51
00:06:25,240 --> 00:06:32,600
something you you not elaborate but you you make it more fancy exaggerated

52
00:06:32,600 --> 00:06:36,920
the un-exaggerated story I think that sort of it's not I think it's not but

53
00:06:36,920 --> 00:06:41,240
anyway the un-exaggerated story is there he got the got the the clay and the

54
00:06:41,240 --> 00:06:46,080
lotus flowers that came back but the story in the Dhammapada goes that the

55
00:06:46,080 --> 00:06:54,840
clay and the lotus flowers regarded by dragons and so he knows this and he

56
00:06:54,840 --> 00:06:58,360
knows it's gonna it's important it's a neat story even though I'm not

57
00:06:58,360 --> 00:07:06,640
asking to believe necessarily that this is what happened because he knew that

58
00:07:06,640 --> 00:07:10,160
this was gonna be something very very difficult to get and so he took he sat

59
00:07:10,160 --> 00:07:15,920
down to eat his his rice and he took the best part of his meal and put it

60
00:07:15,920 --> 00:07:22,920
aside and eat the rest or most of the rest and then a traveler was he saw a

61
00:07:22,920 --> 00:07:25,560
traveler by the side he was sitting by the side already saw a traveler going by

62
00:07:25,560 --> 00:07:31,040
and he said you friend come here I have set aside the best portion of my meal

63
00:07:31,040 --> 00:07:35,680
please partake of this and man's like oh wow that's very nice of you and he

64
00:07:35,680 --> 00:07:42,080
takes the food it's the food and goes on his way and the our hero takes the

65
00:07:42,080 --> 00:07:46,640
rest of the rice a little bit that's left he throws it into the water and then

66
00:07:46,640 --> 00:07:51,240
he makes a determination he said now I have done great merit a thousand times

67
00:07:51,240 --> 00:07:56,600
merit by giving to someone giving my food to someone and I've done a hundred

68
00:07:56,600 --> 00:08:03,160
gotten a hundred times merit for giving food to the fishes in the water by the

69
00:08:03,160 --> 00:08:07,920
power of this merit may I be worthy to receive this special red clay and these

70
00:08:07,920 --> 00:08:12,240
white lotus flowers king and the dragon comes along and I can't remember this

71
00:08:12,240 --> 00:08:17,240
talk about something forgot what it is and somehow he gets he gets these lotuses

72
00:08:17,240 --> 00:08:20,960
the thing is he gets the clay and the lotuses comes back to the king and the

73
00:08:20,960 --> 00:08:25,520
king thinking that he might be able to somehow actually get the clay and the

74
00:08:25,520 --> 00:08:30,040
lotuses from these dragons against all odds has them close to the gates to the

75
00:08:30,040 --> 00:08:34,400
city early that day so he's got no hope and he stands outside the gates of the

76
00:08:34,400 --> 00:08:40,400
city and he throws the clay down and he throws the lotuses down and he says I

77
00:08:40,400 --> 00:08:45,920
have brought these as I as I was required the king and he shouts out that the

78
00:08:45,920 --> 00:08:55,520
king has framed me set me up to be killed and no one heard him so he leaves them

79
00:08:55,520 --> 00:08:59,520
there and in fear of his life he goes to the monastery he says where will I

80
00:08:59,520 --> 00:09:03,000
go oh I'll go hang out with the monks

81
00:09:05,040 --> 00:09:10,520
where do you go when you got nowhere else to go and he goes to the monastery

82
00:09:10,520 --> 00:09:17,240
and he falls asleep at the monastery in some corner of would be Jato on I

83
00:09:17,240 --> 00:09:27,200
guess the king having bathed and having gone back into his room laid down and

84
00:09:27,200 --> 00:09:32,480
tried to sleep but found that he couldn't sleep so he was up all night tossing

85
00:09:32,480 --> 00:09:36,040
and turning thinking about this woman and how in the morning he was going to

86
00:09:36,040 --> 00:09:40,760
kill this man and take his wife and he was so full of this passion and just

87
00:09:40,760 --> 00:09:47,640
just rip bare and to go and that he couldn't sleep and so he was up all night

88
00:09:47,640 --> 00:09:54,120
and then in the morning the sun came up he had gotten no sleep he I know so

89
00:09:54,120 --> 00:09:59,160
in near the end of the night just before the sun was going to come up suddenly

90
00:09:59,160 --> 00:10:09,120
he heard a noise and even hear this heard this story what noise he heard do sa nah

91
00:10:09,120 --> 00:10:33,160
so four voices four different voices he heard do sa nah so like that it gave him the

92
00:10:33,160 --> 00:10:44,120
he be geebies imagine your king protected by all these all your guards and

93
00:10:44,120 --> 00:10:48,880
fortifications and suddenly out of me in the dark of the night you hear these

94
00:10:48,880 --> 00:10:55,480
four words he freaked out so in the morning he totally forgot about this man and

95
00:10:55,480 --> 00:10:59,320
he calls his ministers and he says look something something really strange

96
00:10:59,320 --> 00:11:03,000
happened last night I heard these three sounds were these four sounds were the

97
00:11:03,000 --> 00:11:12,880
four sounds do sa nah so and the brahmins are like oh yeah we know what that

98
00:11:12,880 --> 00:11:17,360
means that means you have to kill lots of animals they had an occluded

99
00:11:17,360 --> 00:11:21,680
but to hide their confusion they said of course of course yes this is

100
00:11:21,680 --> 00:11:29,400
terrible your majesty you're going to die is there any way of avoiding it oh

101
00:11:29,400 --> 00:11:34,720
yes yes of course there is and you can count on us to know the the cure it's

102
00:11:34,720 --> 00:11:39,080
you must get five hundred bowls and five hundred horses and five hundred sheep

103
00:11:39,080 --> 00:11:45,280
and five hundred goats and kill them all and that of course will will save

104
00:11:45,280 --> 00:11:49,680
your life it's just how physics works

105
00:11:49,680 --> 00:12:00,600
it's a lot of physics there's an equation for it five hundred X plus five hundred

106
00:12:00,600 --> 00:12:07,080
and the king his vicinity is not known for his erudition is that a word his

107
00:12:07,080 --> 00:12:15,120
smarts neither am I and so he believes them I think out there's them all up

108
00:12:15,120 --> 00:12:18,160
and everyone's going crazy trying to just stealing people I think it just

109
00:12:18,160 --> 00:12:22,680
confiscates people's livestock and everyone's so upset and and getting in a

110
00:12:22,680 --> 00:12:27,240
big couple and Malika she sees all this craziness going on Malika's

111
00:12:27,240 --> 00:12:31,120
his queen and she goes to King vicinity and she says your majesty what's

112
00:12:31,120 --> 00:12:36,400
going on oh my dear I was the most horrible thing I heard these four noises

113
00:12:36,400 --> 00:12:45,360
in the night and in order to save my life these four deadly noises that are

114
00:12:45,360 --> 00:12:51,840
just going to somehow kill me and in order to save my life I've had to arrange a

115
00:12:51,840 --> 00:12:58,880
sacrifice a great sacrifice of a thousand two thousand animals and Malika looks

116
00:12:58,880 --> 00:13:07,720
at him and says you're an idiot he says you're a simpleton let me

117
00:13:07,720 --> 00:13:15,000
take him back and he said okay what do you mean by that and she says you heard four

118
00:13:15,000 --> 00:13:22,600
sounds and these these brahman's tell you that you're going to die and that

119
00:13:22,600 --> 00:13:25,320
you have to secondly I mean what is the relationship between these four

120
00:13:25,320 --> 00:13:29,520
sounds and killing two thousand animals do you really see some sort of

121
00:13:29,520 --> 00:13:36,920
connection there do you really are you really that gullible and I says well

122
00:13:36,920 --> 00:13:40,040
what do you think it means which is I haven't a clue it didn't mean but at

123
00:13:40,040 --> 00:13:43,120
least I'm ready to admit it those brahman's haven't got a clue either they're

124
00:13:43,120 --> 00:13:47,720
just making stuff up because it makes the money to perform these sacrifices

125
00:13:47,720 --> 00:13:52,680
they said well then what would you have me do as well find someone who knows the

126
00:13:52,680 --> 00:13:58,760
answer well who knows the answer well the jaytawana Buddha ever hear of this

127
00:13:58,760 --> 00:14:04,760
guy and it's like you're right I should go to see they're fully enlightened

128
00:14:04,760 --> 00:14:08,680
Buddha and then so he says but you must come with me and so they go together

129
00:14:08,680 --> 00:14:14,760
King gets on his white elephant and she gets on her pink elephant and they go

130
00:14:14,760 --> 00:14:21,840
together to the jaytawana and step down from their elephants and go into the

131
00:14:21,840 --> 00:14:28,400
monastery and go before the Buddha and sit down and pay respect to him and

132
00:14:28,400 --> 00:14:33,280
the Buddha says so for what reason have you come your majesty the king is so

133
00:14:33,280 --> 00:14:37,720
frightened he doesn't talk he doesn't say anything so Malika says Malika

134
00:14:37,720 --> 00:14:48,360
speaks up and says this guy over here he had these saw these heard these

135
00:14:48,360 --> 00:14:52,560
four sounds and then the brahman's told him blah blah blah blah and he

136
00:14:52,560 --> 00:14:57,760
decided to do to follow along after them please tell us of an herbal

137
00:14:57,760 --> 00:15:00,460
server what is the meaning of the noises and so he says to King

138
00:15:00,460 --> 00:15:08,080
vicinity what within what were the four noises and vicinity says do sat not so

139
00:15:08,080 --> 00:15:17,120
that's what I heard I think something like that remember it says ah relax there's

140
00:15:17,120 --> 00:15:25,280
no danger to you your majesty what you heard well let me tell you a story he

141
00:15:25,280 --> 00:15:33,440
says sometime in the long in the in the in the in the past in the time of the

142
00:15:33,440 --> 00:15:43,720
Buddha Kaspa there were these four men and while everyone else was doing great

143
00:15:43,720 --> 00:15:49,560
good deeds and and performing wholesome acts giving gifts and keeping

144
00:15:49,560 --> 00:15:52,960
morality and practicing meditation and doing good things in the Buddhist

145
00:15:52,960 --> 00:15:57,940
dispensation these four guys looked at them and said man that's that's really

146
00:15:57,940 --> 00:16:05,680
tiresome to be engaged in in wholesome acts like that helping other people these

147
00:16:05,680 --> 00:16:09,640
guys were also very rich and so they had lots and lots of money I think they

148
00:16:09,640 --> 00:16:15,800
had a hundred goaties of gold or something like that and a lot of money and so

149
00:16:15,800 --> 00:16:21,160
they sat down they said well what should we do with our money and one of them

150
00:16:21,160 --> 00:16:26,760
said well let's get lots of the choices rice is three that's the only three

151
00:16:26,760 --> 00:16:31,960
year-old rice and curry or special fermented rice or you know aged rice

152
00:16:31,960 --> 00:16:37,640
somehow and then one said let's just have the nicest curries and let's

153
00:16:37,640 --> 00:16:40,360
this this and that and the fourth guy says he says you guys all have no

154
00:16:40,360 --> 00:16:46,440
imagination here's what we're gonna do he says there is no woman out there I

155
00:16:46,440 --> 00:16:49,840
think this is what he says not this is not Buddhist Buddhism this is the bad stuff

156
00:16:49,840 --> 00:16:55,800
so there's no woman out there who won't cheat on her husband for money so

157
00:16:55,800 --> 00:17:00,000
here's what we'll do we'll go and we'll offer money to all the beautiful women

158
00:17:00,000 --> 00:17:04,760
in all the beautiful vibes in the city and we'll commit adultery with them we'll

159
00:17:04,760 --> 00:17:12,000
bribe them to to to come and run away with us

160
00:17:12,360 --> 00:17:18,800
like oh that sounds like a good idea and so they decide that's what they're

161
00:17:18,800 --> 00:17:25,280
going to do with their money and they spend all their money on women

162
00:17:27,240 --> 00:17:36,760
what is that and when they passed away they fell into they appeared into

163
00:17:36,760 --> 00:17:41,280
the body disappeared the course body disappears and they're reborn with a

164
00:17:41,280 --> 00:17:49,200
with a fine body in a boiling cauldron called the great cauldron I think

165
00:17:49,200 --> 00:17:53,480
there's something the iron cauldron right it's the hell called the iron

166
00:17:53,480 --> 00:17:59,280
cauldron where it's full of molten lava maybe or just maybe water boiling oil

167
00:17:59,280 --> 00:18:09,200
maybe can't remember boiling oil maybe and it takes 30,000 I'm making this up

168
00:18:09,200 --> 00:18:15,280
I'm I'm not quite sure 30,000 years for them to sink to the bottom of the

169
00:18:15,280 --> 00:18:24,120
cauldron and then 30,000 years to float back up and then they take one breath

170
00:18:24,120 --> 00:18:32,640
and then they'll go back down and back up and they've been there for they've

171
00:18:32,640 --> 00:18:37,120
been there since the time of the Buddha cuss about how long ago that was

172
00:18:37,120 --> 00:18:41,640
so there's a time frame a long long time and they're going to be there until

173
00:18:41,640 --> 00:18:46,000
the karma the results of their bad karma is expedited

174
00:18:46,000 --> 00:18:57,160
I said so what you heard is you just caught them at the moment when they were

175
00:18:57,160 --> 00:19:03,160
all at the top of the cauldron and they wanted to say something to you but

176
00:19:03,160 --> 00:19:08,720
they couldn't they wanted to each say a verse but all they could get out was

177
00:19:08,720 --> 00:19:15,480
the first the first syllable so he said the man who said do that was do G

178
00:19:15,480 --> 00:19:20,920
with that we have did the bad life we had lots of wealth and we never gave it

179
00:19:20,920 --> 00:19:25,560
away to anyone we never did good deeds with all of our money we wasted our

180
00:19:25,560 --> 00:19:33,400
time in our lives do G with that in a wrong life an evil life a life of evil

181
00:19:33,400 --> 00:19:40,920
sat was the second man's mean satit wasa sata sata saying for 60,000 years

182
00:19:40,920 --> 00:19:45,800
right 30 down and 30 up we've been boiling I guess that's what it was the

183
00:19:45,800 --> 00:19:50,120
Buddha cuss about would have been 60,000 years ago then I guess 60,000 years and

184
00:19:50,120 --> 00:19:52,880
now finally they're at the top but then they have to go back down again it's

185
00:19:52,880 --> 00:19:59,440
not over yet there's 60,000 years we've been boiling in this cauldron for the

186
00:19:59,440 --> 00:20:08,560
evil that we've done it's a really good the third guy not means nutty nutty

187
00:20:08,560 --> 00:20:12,760
I don't know I think there is no end nutty until good doughnut oh where is the

188
00:20:12,760 --> 00:20:19,880
end good dough for where can there be an end I don't see any end because we

189
00:20:19,880 --> 00:20:27,520
have done these evil deans there's no way out and the fourth one so it's

190
00:20:27,520 --> 00:20:37,320
comes from so a hung so hung so hung means I when I get out of here and I'm

191
00:20:37,320 --> 00:20:42,920
born as a human being again you can be darn sure that I'm going to spend all

192
00:20:42,920 --> 00:20:49,480
my time doing good deeds giving charity keeping morality and practicing

193
00:20:49,480 --> 00:20:57,320
meditation so I said there's no harm to you in your majesty wink wink unless

194
00:20:57,320 --> 00:21:04,000
you like the under the under but the between the lines is unless you do

195
00:21:04,000 --> 00:21:12,320
exactly what they did by committing adultery and it was just the just these

196
00:21:12,320 --> 00:21:20,720
four just these four poor souls who had fallen into hell and then they hear a

197
00:21:20,720 --> 00:21:32,600
voice off to the side saying venerable sir I now know the I now know the true

198
00:21:32,600 --> 00:21:35,880
length of a league when they look over and there's excuse me saying he looks

199
00:21:35,880 --> 00:21:39,960
over and there's this man who is sleeping in the monastery and King

200
00:21:39,960 --> 00:21:43,280
the city looks at him and he shivers and he said to think I was going to kill

201
00:21:43,280 --> 00:21:47,000
this man for his wife I could have been in hell and he said

202
00:21:47,000 --> 00:21:57,120
then a remosser I now know the the length of the night and what I said well the

203
00:21:57,120 --> 00:22:01,880
length of the night is long the length of a yojana is quite long but even

204
00:22:01,880 --> 00:22:07,720
longer is the length of some sorrow for the for fools who don't see the true

205
00:22:07,720 --> 00:22:11,560
dumber and so that's the verse he told

206
00:22:11,560 --> 00:22:19,840
digang jaga diga jaga to roti digang yojana you digang santa sayojana

207
00:22:19,840 --> 00:22:30,360
digo bala anas anas anas anas anas anas aaru sattamamamavijanita so that's

208
00:22:30,360 --> 00:22:37,560
the story what's the meaning of the verse how does this verse apply to us

209
00:22:37,560 --> 00:22:42,720
obviously we're not fools everyone is here interested in the Buddha

210
00:22:42,720 --> 00:22:50,240
teaching but the reason we're not here is because we have heard and

211
00:22:50,240 --> 00:22:54,040
understand these teachings because we have these teachings and they we have

212
00:22:54,040 --> 00:23:01,400
them to remind us of the danger inherent in falling astray the mind is

213
00:23:01,400 --> 00:23:06,360
powerful and power can be used for good or evil it's not a game that you can

214
00:23:06,360 --> 00:23:10,540
just turn off the computer or the Xbox or whatever

215
00:23:10,540 --> 00:23:18,320
press reset you can't just wipe the slight clean everything has its effect if

216
00:23:18,320 --> 00:23:23,160
you engage in evil deeds evil results come to you if you're constantly engaging

217
00:23:23,160 --> 00:23:30,520
in evil deeds it becomes a habit and that habit lasts with you your mind carries

218
00:23:30,520 --> 00:23:42,280
it cult of it becomes who you are we become the things that we do and then when

219
00:23:42,280 --> 00:23:48,280
we pass away we'd be born in we can be born in even help if we've done lots

220
00:23:48,280 --> 00:23:58,560
and lots of evil deeds and our minds are impure and so on so the the meaning of

221
00:23:58,560 --> 00:24:02,000
the whole long as the night well long as the night isn't is actually an

222
00:24:02,000 --> 00:24:05,400
important Buddha's teaching because the Buddha said Buddha once was

223
00:24:05,400 --> 00:24:13,560
sleeping out in the open and on some with just his robes was sleeping on some

224
00:24:13,560 --> 00:24:20,200
dry leaves he was traveling somewhere and he got caught in the night and so he

225
00:24:20,200 --> 00:24:24,840
gathered together some leaves and put his robe one robe down but one robe on top

226
00:24:24,840 --> 00:24:29,800
of him in the middle of winter and this man came up okay remember who it was

227
00:24:29,800 --> 00:24:35,560
Brahman I think came up and said to a vendor will serve it's cold those leaves

228
00:24:35,560 --> 00:24:40,720
earth in your robes are thin and it could snow tonight this is the between the

229
00:24:40,720 --> 00:24:46,600
eights between the eights is the eighth the eighth of one month and the eighth of

230
00:24:46,600 --> 00:24:50,960
another month the coldest two coldest months it was the coldest period it's

231
00:24:50,960 --> 00:24:54,840
called the between the eights the period of the coldest period in India of the

232
00:24:54,840 --> 00:24:59,800
year they said it could snow this is during this period sometimes it snow so

233
00:24:59,800 --> 00:25:05,080
it was down near freezing the freezing point and the Buddha said to him

234
00:25:05,080 --> 00:25:08,520
well let me ask you he says how are you going to sleep how can you possibly sleep

235
00:25:08,520 --> 00:25:14,680
like that and the Buddha says to him he says let me ask you something suppose a

236
00:25:14,680 --> 00:25:19,240
person is full of lust now suppose a person sorry suppose there's a rich

237
00:25:19,240 --> 00:25:30,120
man on a down field what would it be like comfort no what do you call these

238
00:25:30,120 --> 00:25:35,600
things when you call the things people sleep

239
00:25:35,600 --> 00:25:40,320
mattress no but there's something else futon futon I'm thinking I don't know

240
00:25:40,320 --> 00:25:45,200
anyway some expensive mattress something water bed maybe in that is

241
00:25:45,200 --> 00:25:56,000
standing and with silk sheets or and pillows down down feather pillows and rich

242
00:25:56,000 --> 00:26:01,680
whatever bedding and the most softest delicate most comfortable bedding that

243
00:26:01,680 --> 00:26:07,120
there is warm quilts and comforters and so on but suppose their mind is full

244
00:26:07,120 --> 00:26:12,160
of lust do you think they would sleep well that night and the

245
00:26:12,160 --> 00:26:16,120
ramen said no probably they'd be up all night tossing and turning but if

246
00:26:16,120 --> 00:26:20,200
their mind was full of anger no likewise if their mind was

247
00:26:20,200 --> 00:26:25,560
full of anger and hatred they also wouldn't sleep well night no matter what

248
00:26:25,560 --> 00:26:29,720
luxury they were sleeping it would if their mind was full of delusion

249
00:26:29,720 --> 00:26:37,480
there's only arrogance or or conceit and self-righteousness or even just

250
00:26:37,480 --> 00:26:44,000
ignorance thinking and ruminating and worrying for example doubting all night

251
00:26:44,000 --> 00:26:48,560
no then they would be up all night as well and we said well none of those

252
00:26:48,560 --> 00:26:53,160
three things are present in me and so for me to sleep here for one night

253
00:26:53,160 --> 00:27:01,120
isn't a big deal so it's a important teaching is sleep doesn't comfort peace

254
00:27:01,120 --> 00:27:05,160
doesn't come from luxury it doesn't come from without it comes from within you

255
00:27:05,160 --> 00:27:08,880
can have to be surrounded by all the luxury you want if your mind is not pure

256
00:27:08,880 --> 00:27:14,760
you don't sleep well how long the league is well there's not much

257
00:27:14,760 --> 00:27:20,280
done by there I don't think but the point the point of the verse is compared

258
00:27:20,280 --> 00:27:24,600
to these things compared to some samsara compared to the wandering on of

259
00:27:24,600 --> 00:27:30,800
fools a league in the night or nothing they're they're they're not even

260
00:27:30,800 --> 00:27:36,840
we're talking about because a fool a fool someone who does evil deeds with

261
00:27:36,840 --> 00:27:41,720
body with speech and with mind so they kill they steal they cheat

262
00:27:41,720 --> 00:27:50,280
with adultery they hurt others they scam others they commit evil deeds by

263
00:27:50,280 --> 00:27:56,480
speech meaning they lie they cheat and they lie they back bite and gossip about

264
00:27:56,480 --> 00:28:03,320
others and have harsh speak harshly to others and speak useless speech and in

265
00:28:03,320 --> 00:28:08,960
the mind they're full of anger agreed and delusion thoughts of thoughts of

266
00:28:08,960 --> 00:28:13,920
lust thoughts of desire thoughts of hatred thoughts of arrogance and

267
00:28:13,920 --> 00:28:23,040
conceit and so on because of all that they they're they're like a drunk

268
00:28:23,040 --> 00:28:30,640
person like a person who is so tired with the journey was wandering through

269
00:28:30,640 --> 00:28:38,200
the wilderness looking for shade and they become so tired and and burnt out

270
00:28:38,200 --> 00:28:43,280
with heat stroke or dizzy with heat stroke that they don't even see a shady

271
00:28:43,280 --> 00:28:47,680
tree when they walk by it they're not even able to see the shade so to a fool

272
00:28:47,680 --> 00:28:51,200
is not able to see goodness they're not able to see the right path they're

273
00:28:51,200 --> 00:28:54,280
not able to find the right path and I'm able to see the danger in what they're

274
00:28:54,280 --> 00:28:58,760
doing like a person would have said like a person who is dazed and confused and

275
00:28:58,760 --> 00:29:03,080
has heat stroke and is walking down the path and at the end of the path there's a

276
00:29:03,080 --> 00:29:08,320
big pit full of burning blazing embers but they don't see it because they're

277
00:29:08,320 --> 00:29:20,120
to their mind is too dizzy now the the reason why they are so dizzy and why

278
00:29:20,120 --> 00:29:26,240
they're so in caught up and intoxicated is as the Buddha said satam

279
00:29:26,240 --> 00:29:30,920
mang avi janita because they don't see the sandam so our practice as Buddhists

280
00:29:30,920 --> 00:29:35,960
is the most important practice for us is to see the sandamma to see the truth

281
00:29:35,960 --> 00:29:43,040
see the not just the truth but the good truth the important truth so we have

282
00:29:43,040 --> 00:29:48,720
three aspects to this one is bariati sandamma two is pati pati sandamma and three is

283
00:29:48,720 --> 00:29:51,520
pati veda sandamma

284
00:29:51,520 --> 00:29:57,260
bariati sandamma means our study so as we study that when we when we hear these

285
00:29:57,260 --> 00:30:00,760
teachings about how these guys were in hell you might not believe it and

286
00:30:00,760 --> 00:30:05,680
you might think well that's ridiculous how it doesn't exist it's on you might

287
00:30:05,680 --> 00:30:10,160
believe such things but you might also start to understand in your mind and

288
00:30:10,160 --> 00:30:14,600
think oh well it really seems reasonable that there are consequences to our

289
00:30:14,600 --> 00:30:18,640
actions at least in this life we have pretty major consequences to evil if we

290
00:30:18,640 --> 00:30:26,680
do them and so by hearing this it it it gives you some pause sometimes some

291
00:30:26,680 --> 00:30:32,040
pause cause for reflection and so as a result you become less inclined to do

292
00:30:32,040 --> 00:30:40,480
say and think evil deeds evil of all kinds a fool who hasn't had this kind of

293
00:30:40,480 --> 00:30:45,600
learning is is a disadvantage I mean just learning the five precepts if

294
00:30:45,600 --> 00:30:48,160
someone had taught them to me when I was younger explained them to me I

295
00:30:48,160 --> 00:30:52,600
think I would have been a lot better off I think a lot of people would be

296
00:30:52,600 --> 00:30:58,840
if they had heard about the the heard other people speaking badly about killing

297
00:30:58,840 --> 00:31:02,200
and stealing and so on we're talking about the dangers and the dangers inherent

298
00:31:02,200 --> 00:31:08,000
to me how it corrupts the mind and how that therefore leads to suffering but

299
00:31:08,000 --> 00:31:11,720
it's not enough because you can you still have no experience so you might have

300
00:31:11,720 --> 00:31:20,280
doubt you might decide that well it's your desire to do and say and think

301
00:31:20,280 --> 00:31:26,040
bad things is overwhelm overwhelms it and when when you have desire you

302
00:31:26,040 --> 00:31:31,320
you aren't able to make yourself believe the things that you've studied

303
00:31:31,320 --> 00:31:35,760
because it's not strong it's not as strong as the desire the they have a

304
00:31:35,760 --> 00:31:41,400
ritual inclination of the mind you know the chemicals that are working in

305
00:31:41,400 --> 00:31:47,680
the brain and so so the second part is patipati sadhamma patipati sadhamma helps

306
00:31:47,680 --> 00:31:52,600
you to see helps you to compare good and evil and it cultivates habits of

307
00:31:52,600 --> 00:31:58,320
good to counteract the evil so if you cultivate habits of wholesomeness

308
00:31:58,320 --> 00:32:07,520
including knowledge that the observation that evil leads leads to suffering

309
00:32:07,520 --> 00:32:11,240
so as you sit in meditation and you see how anger inflames the mind how

310
00:32:11,240 --> 00:32:19,320
greed inflames the mind how delusion boils the mind you start to incline

311
00:32:19,320 --> 00:32:25,520
away from evil needs and they get less they have less power over you and the

312
00:32:25,520 --> 00:32:28,480
wholesome deal you have an inclination towards wholesomeness through our

313
00:32:28,480 --> 00:32:32,000
through the practice this is all we're getting from walking back and forth and

314
00:32:32,000 --> 00:32:36,280
sitting still we're gaining these habits sorry it's not all we're getting this

315
00:32:36,280 --> 00:32:45,000
is the the preliminary benefit the practical benefits but the third one is patipati

316
00:32:45,000 --> 00:32:50,080
Vedas sadhamma is the real true seeing the proper seeing of the true

317
00:32:50,080 --> 00:32:55,920
dhamma which is when the mind enters into nibhana when there is the realization

318
00:32:55,920 --> 00:33:02,720
of the four noble truths and the mind lets go of experience and as a result

319
00:33:02,720 --> 00:33:10,720
sees ever sees that everything ceases gains an experiential knowledge that

320
00:33:10,720 --> 00:33:16,200
there's nothing that arises that doesn't cease and so loses the desire to cling

321
00:33:16,200 --> 00:33:22,160
and to strive and to crave and to chase after evil or chase after any it

322
00:33:22,160 --> 00:33:31,920
chase after objects of desire objects of aversion to chase them away any desire

323
00:33:31,920 --> 00:33:37,680
to cling or get involved with anything and so as a result one is naturally

324
00:33:37,680 --> 00:33:42,800
unenclined to commit evil deeds knowing that they just lead to stress and

325
00:33:42,800 --> 00:33:54,200
suffering and wondering on that they just lead to mindless meaningless

326
00:33:54,200 --> 00:34:01,360
samsara this comes through the practice of the Buddha's teaching and so this is

327
00:34:01,360 --> 00:34:07,760
what we have to be thankful grateful and proud of as far as Buddhists are

328
00:34:07,760 --> 00:34:17,080
proud allowed to be proud that we're not fools that we are engaging and trying

329
00:34:17,080 --> 00:34:24,120
and fighting the fight to try to cultivate wisdom and understanding and goodness in

330
00:34:24,120 --> 00:34:31,080
our hearts so that's verse number 60 and that's the story and the meaning as far

331
00:34:31,080 --> 00:34:36,080
as I can tell it so thank you all for tuning in I hope this has been useful and

332
00:34:36,080 --> 00:34:40,320
I wish you all to find peace happiness and freedom from suffering thank you

333
00:34:40,320 --> 00:34:47,320
have a good day

