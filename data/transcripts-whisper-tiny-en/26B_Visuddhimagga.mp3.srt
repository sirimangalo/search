1
00:00:00,000 --> 00:00:04,960
So, as one only, I will walk about, do you write a recital, ask a question, or out of

2
00:00:04,960 --> 00:00:09,680
fewness of which, as he thinks, this is a monastery with few because, if someone comes,

3
00:00:09,680 --> 00:00:14,000
he will wonder, where I have all these big clues, who are all alike, come from.

4
00:00:14,000 --> 00:00:18,760
Surely it will be one of the elder's feet, and so he might get to know about me.

5
00:00:18,760 --> 00:00:23,840
Meanwhile, wishing, let me be, one only, he should attain the basic Jana and emerge.

6
00:00:23,840 --> 00:00:28,000
Then, after doing the preliminary work, thus, let me be one, he should again attain

7
00:00:28,000 --> 00:00:32,000
and emerge, and then resolve, thus, let me be one.

8
00:00:32,000 --> 00:00:36,640
He becomes one simultaneously with the resolving consciousness.

9
00:00:36,640 --> 00:00:44,640
So, if he wants to become one, then he has to enter into Jana and then make a resolution,

10
00:00:44,640 --> 00:00:50,640
and simultaneously with the direct knowledge consciousness, he becomes one again.

11
00:00:50,640 --> 00:00:55,600
But instead of doing this, he can automatically become one again, with the lips of the pre-determined

12
00:00:55,600 --> 00:01:03,360
time, that is, there we say, a thousand monks for one hour or two hours, then at the end

13
00:01:03,360 --> 00:01:06,800
of one hour or two hours, he will always disappear.

14
00:01:06,800 --> 00:01:16,080
But if he wants the creation to disappear before the time, then he had to enter into Jana

15
00:01:16,080 --> 00:01:22,160
and then get the direct knowledge consciousness.

16
00:01:22,160 --> 00:01:29,280
He appears and finishes, so he can appear before people or he can finish out from the view

17
00:01:29,280 --> 00:01:31,440
of people.

18
00:01:31,440 --> 00:01:38,400
Now, this possessor of supernormal power who wants to make an appearance makes darkness

19
00:01:38,400 --> 00:01:43,160
into light, or he makes a view for this hidden, or he makes what has not come into the

20
00:01:43,160 --> 00:01:46,360
visual field, come into the visual field, and so on.

21
00:01:46,360 --> 00:01:51,840
How, if he wants to make himself or another visible even to hidden or at the distance, he

22
00:01:51,840 --> 00:01:59,120
emerges from the basic Jana and Adverse thus, let this duck become light, or let this

23
00:01:59,120 --> 00:02:03,320
that is hidden, be revealed, and so on.

24
00:02:03,320 --> 00:02:11,280
Now, 71, but by whom was this miracle formally performed by the Buddha?

25
00:02:11,280 --> 00:02:18,560
So when the Blessed One had been invited by Tula Subhara, and was traversing the seven

26
00:02:18,560 --> 00:02:23,000
league journey between Sabhati and Saketa, and with five hundred parent queens created

27
00:02:23,000 --> 00:02:29,640
the wish of Kama, he was a god, Salisa being.

28
00:02:29,640 --> 00:02:34,400
He resolved in such wise that citizens of Saketa saw the inhabitants of Sabhati and citizens

29
00:02:34,400 --> 00:02:39,560
of Sabhati saw the inhabitants of Saketa, and when he had aligned in the center of the

30
00:02:39,560 --> 00:02:44,040
city, he split the earth in true and short of which he had helped, and he parted the

31
00:02:44,040 --> 00:02:46,680
sky in true and short of the Brahma world.

32
00:02:46,680 --> 00:02:57,840
It is Buddha's miracle, letting people see people in the city, people in another city,

33
00:02:57,840 --> 00:03:08,600
like letting people in San Francisco see people in Los Angeles, without going there.

34
00:03:08,600 --> 00:03:16,920
And this meaning should also be explained by means of decent of the gods, the Orowana,

35
00:03:16,920 --> 00:03:22,360
and when the Blessed One, it seems that performed the twin miracle and had liberated 84,000

36
00:03:22,360 --> 00:03:23,360
beings from bonds.

37
00:03:23,360 --> 00:03:34,000
You wonder where did the first enlightened ones go to, and this is a story connected with

38
00:03:34,000 --> 00:03:38,600
how Buddha taught Abidamma to the Salasya beings.

39
00:03:38,600 --> 00:03:44,160
In his seventh year, Buddha went up to Dao Dingsa heaven, after he had performed the

40
00:03:44,160 --> 00:03:53,200
twin miracle, I think later on we will know what twin miracle is.

41
00:03:53,200 --> 00:04:01,320
And then he went up to Dao Dingsa heaven, and he took a residence there for the rains,

42
00:04:01,320 --> 00:04:10,240
that means he observed, what's that there for three months, three months of human beings.

43
00:04:10,240 --> 00:04:15,880
And then taught Abidamma there, starting from the beginning, to the days of 10,000 world

44
00:04:15,880 --> 00:04:16,880
fears.

45
00:04:16,880 --> 00:04:21,120
At the time for wandering for arms, he created an artificial Buddha to teach the Dhamma

46
00:04:21,120 --> 00:04:22,120
and so on.

47
00:04:22,120 --> 00:04:27,400
And then I have told you about this also.

48
00:04:27,400 --> 00:04:37,120
And this is how we believe that Buddha went to Dao Dingsa heaven and taught non-stop.

49
00:04:37,120 --> 00:04:45,720
And they taught Abidamma non-stop to Salasya beings there, but when it was time to go for

50
00:04:45,720 --> 00:04:56,840
arms, then he left a created image, and then he went to, and he said, he went there for

51
00:04:56,840 --> 00:05:09,480
arms, and then he took food at Lake Anotada, and then he met Saripu, Sarigura, met him there,

52
00:05:09,480 --> 00:05:20,680
and then he would say, Sarigura, I have taught such and such portions of Abidamma.

53
00:05:20,680 --> 00:05:30,040
And then Buddha taught there for three months, and many people who were assembled at the

54
00:05:30,040 --> 00:05:34,920
twin miracles did not leave that place, because Buddha just disappeared from them, and

55
00:05:34,920 --> 00:05:39,920
so they were yearning to see the Buddha again.

56
00:05:39,920 --> 00:05:46,320
Buddha simply disappeared from their fear of vision, so they were waiting there for the

57
00:05:46,320 --> 00:05:47,880
Buddha to come back.

58
00:05:47,880 --> 00:05:58,600
So at the end of the wastha, Mokrana went up to the Buddha, and asked him where he would

59
00:05:58,600 --> 00:06:07,240
descend to the wall of human beings, and he said, in a certain place, some kastha.

60
00:06:07,240 --> 00:06:15,320
And so on that the Buddha came back to the human world, so that that is the last day

61
00:06:15,320 --> 00:06:20,480
of the three months rainy season retreat for the months, so it is in October, full Monday

62
00:06:20,480 --> 00:06:36,360
of October, that Buddha came down to the world of human beings.

63
00:06:36,360 --> 00:06:51,160
So very grand descent to the human-human wall, three staircases made of crystal, and then

64
00:06:51,160 --> 00:06:52,160
what?

65
00:06:52,160 --> 00:07:00,320
Metals, flight of stairs made of crystal, made of ruby, and so on.

66
00:07:00,320 --> 00:07:06,240
And in Burma to commemorate that event, we have festival of lights.

67
00:07:06,240 --> 00:07:12,280
It is believed that when Buddha came down to the human world, it was dark.

68
00:07:12,280 --> 00:07:17,440
So instead of offering him food, they just offer him lights.

69
00:07:17,440 --> 00:07:25,520
So in order to commemorate that, we have festival of light on the full Monday of October,

70
00:07:25,520 --> 00:07:36,280
and in Sri Lanka, the of our lives at Visak, full Monday of May.

71
00:07:36,280 --> 00:07:42,720
And then there are other stories too.

72
00:07:42,720 --> 00:07:58,080
And the next one is hiding himself or hiding others by miraculous power.

73
00:07:58,080 --> 00:08:04,240
So paragraph 82, but by whom was this miracle formally performed by the Blessed One?

74
00:08:04,240 --> 00:08:08,240
For the Blessed One, to act at the pointy, cleansed men, yes, I was sitting beside him,

75
00:08:08,240 --> 00:08:10,240
his father did not see him.

76
00:08:10,240 --> 00:08:18,680
Now, yes, I was a cleansed man, and he just left his home during the night, and then he

77
00:08:18,680 --> 00:08:28,080
went to where the Buddha was, just after giving the first summer, after giving first summer.

78
00:08:28,080 --> 00:08:34,280
So Buddha, he met Buddha there, and Buddha taught him and he became a sort of man, and

79
00:08:34,280 --> 00:08:36,320
also he became a man.

80
00:08:36,320 --> 00:08:45,440
And in the morning, yes, as father came to that place and asked Buddha, did you see my son?

81
00:08:45,440 --> 00:08:50,000
Then Buddha said, oh, you will see him, but just sit down.

82
00:08:50,000 --> 00:08:53,120
And then he taught him.

83
00:08:53,120 --> 00:08:55,720
He taught him the dhamma, and he became a sort of man.

84
00:08:55,720 --> 00:08:59,040
So only then did he show yes, I do him.

85
00:08:59,040 --> 00:09:08,280
So he hit, yes, by his supernormal power, so his father did not see him, because if the

86
00:09:08,280 --> 00:09:14,440
father saw the son, then the father would not be able to listen to the dhamma, and he would

87
00:09:14,440 --> 00:09:18,920
not have become a sort of man.

88
00:09:18,920 --> 00:09:24,360
And then likewise, after traveling 2,000 weeks to me, King Mahākābina and establishing

89
00:09:24,360 --> 00:09:31,000
him in the fruition of non-return, and his 1,000 ministers in the fruition of stream entry,

90
00:09:31,000 --> 00:09:35,720
he so acted at Queen Anorja, who had followed the King with 1,000 women, attendance, and

91
00:09:35,720 --> 00:09:41,600
was sitting near by a did not see the King and his right in here, so at the same, the

92
00:09:41,600 --> 00:09:44,200
same miracle.

93
00:09:44,200 --> 00:09:56,720
And that story, you may read in book, I mean, Buddhist legends, part 2, page 167, and

94
00:09:56,720 --> 00:09:57,720
following.

95
00:09:57,720 --> 00:10:07,640
There is, the story is given in detail there, Buddhist legends, part 2, page 167, and

96
00:10:07,640 --> 00:10:14,760
of the end of the story, furthermore, this was performed by the elder Mahinda.

97
00:10:14,760 --> 00:10:23,960
Now, elder Mahinda was the son of King Asoga, who brought Buddhism to Sri Lanka, so acted

98
00:10:23,960 --> 00:10:29,320
on the day of his arrival in Tampa, Panid Island, that means Sri Lanka, that the King did

99
00:10:29,320 --> 00:10:32,800
not see the others who had come with him.

100
00:10:32,800 --> 00:10:41,840
He went with five persons, but when he first met the King, and he did not let the King

101
00:10:41,840 --> 00:10:44,600
see other other persons.

102
00:10:44,600 --> 00:10:50,160
Furthermore, all miracles of making evidence are called an appearance, and all miracles

103
00:10:50,160 --> 00:10:58,320
of making an evident are called a vanishing, and then there are two kinds, making evidence

104
00:10:58,320 --> 00:11:04,840
both the subanormal power and the boosers are of the subanormal power, so power and person,

105
00:11:04,840 --> 00:11:10,240
evidence, power and person, not evident, that can be illustrated with a trin miracle, for

106
00:11:10,240 --> 00:11:12,520
in that Buddha displayed thus.

107
00:11:12,520 --> 00:11:18,800
Here, the perfect one performs a trin miracle which is not shared by disciples, so only

108
00:11:18,800 --> 00:11:21,520
the boosers can do the trin miracle.

109
00:11:21,520 --> 00:11:25,320
He produces a mess of fire from the upper part of his body and the shower of water from

110
00:11:25,320 --> 00:11:27,880
the lower part of his body and so on and so on.

111
00:11:27,880 --> 00:11:36,960
The trin miracle, it is called trin because both fire and water produce from his body,

112
00:11:36,960 --> 00:11:44,840
so fire comes out of the upper part and the water comes out of the lower part of even

113
00:11:44,840 --> 00:11:53,520
in more detail, fire comes out of one eye and the water comes out of the other eye and

114
00:11:53,520 --> 00:11:54,520
so on.

115
00:11:54,520 --> 00:12:03,920
So it is a trin miracle, it is called trin miracle and in the footnotes where the only

116
00:12:03,920 --> 00:12:09,840
boog in the T.B. to go to mention that trin miracle is the petisambita marga, so in other

117
00:12:09,840 --> 00:12:20,760
books it is not mentioned, but in the commentaries it is mentioned again and again.

118
00:12:20,760 --> 00:12:27,080
In the case of the miracle of making an evident only the supernormal power is displayed,

119
00:12:27,080 --> 00:12:29,200
not the boosers are of the supernormal power.

120
00:12:29,200 --> 00:12:35,240
That can be illustrated by means of Mahakasuda, that is San Yoda and the Brahma Nima, the

121
00:12:35,240 --> 00:12:43,200
Niga Soda, that is magic manigara and the petis from the San Yoda Niga is given below,

122
00:12:43,200 --> 00:12:50,000
so we do not have to give you a reference number.

123
00:12:50,000 --> 00:13:02,000
The Brahma Nima Niga Soda is Soda number 49 of metal lengths, here it is given as M1330,

124
00:13:02,000 --> 00:13:22,080
so Soda number 49, and here Mahakasuda is a miracle that only the, what you call, only

125
00:13:22,080 --> 00:13:27,920
the gaps in the fast fastenings and burn the hay without burning the abbarrow, he put the

126
00:13:27,920 --> 00:13:42,240
abbarrow on the hay, but he results that only the hay burn and not the abbarrow.

127
00:13:42,240 --> 00:13:57,880
And the other one is when Buddha met the Brahma, the Brahma's name was Boca, so he

128
00:13:57,880 --> 00:14:07,280
thought that he could make himself vanish from the side of all others, but in that Soda

129
00:14:07,280 --> 00:14:12,680
said that he was not able to do that, so Buddha said, now I will do it, and so Buddha

130
00:14:12,680 --> 00:14:22,720
disappeared from all the Brahma's, but in order that all Brahma know that he, the Buddha

131
00:14:22,720 --> 00:14:33,040
was there, he said this verse, stanza, I saw the fear and all kinds of becoming, including

132
00:14:33,040 --> 00:14:39,400
becoming that sixth non becoming, and no becoming do I recommend, I cling to know the

133
00:14:39,400 --> 00:14:47,120
like that and at all, so when he uttered this stanza, everybody knew that he was there,

134
00:14:47,120 --> 00:14:55,560
nobody could see him, next he goes unhindered through walls, through enclosures, through mountains

135
00:14:55,560 --> 00:15:03,560
as though in open space, here through walls is beyond walls and so on.

136
00:15:03,560 --> 00:15:14,400
And here I, an interesting piece of information is the paragraph 89, elder T.B. de Gajula

137
00:15:14,400 --> 00:15:31,240
Abaya, he had some different opinions about how to, how to perform these direct knowledge,

138
00:15:31,240 --> 00:15:36,200
so here he said friends, what is the use of attaining this space casino, just one who wants

139
00:15:36,200 --> 00:15:41,800
to create elephants, horses, etc., attain an elder fantasy in Adjana, or hoskas in Adjana and

140
00:15:41,800 --> 00:15:47,960
so on, surely the only standard is mastery in the eight attainments, and after the preliminary

141
00:15:47,960 --> 00:15:53,560
work has been done on any casino, it then becomes whatever he wishes.

142
00:15:53,560 --> 00:15:58,080
But the other big who said, whenever only the space casino has been given in the text,

143
00:15:58,080 --> 00:16:00,600
so it should suddenly be mentioned.

144
00:16:00,600 --> 00:16:07,640
So there was a little difference of opinion here between that elder and the other month,

145
00:16:07,640 --> 00:16:15,720
and it seems that the elder T.B. de Gajula bates his opinion on his experience, but the

146
00:16:15,720 --> 00:16:24,800
others have too much respect for the text, so they said, whatever it may be, only the

147
00:16:24,800 --> 00:16:35,640
space casino is given in the text, so it should be mentioned, and then the text is given.

148
00:16:35,640 --> 00:16:43,400
Now in paragraph 91, what if a mountain or tree is raised in this big who's way while

149
00:16:43,400 --> 00:16:49,400
he is traveling along after resolving, should he attain a result of a gain, there is no

150
00:16:49,400 --> 00:16:55,840
harm in that, for attaining or resolving a gain is like taking the dependence in the preceptor's

151
00:16:55,840 --> 00:17:00,880
presence, do you understand that?

152
00:17:00,880 --> 00:17:13,560
Now, when you become a monk, then you have to be dependent upon the preceptor or some

153
00:17:13,560 --> 00:17:22,520
other teacher, so for five years you are not to stay independently of them, the best is

154
00:17:22,520 --> 00:17:29,920
to stay with your preceptor, so he will teach you everything, he is like a father to you,

155
00:17:29,920 --> 00:17:36,440
so when we are living with our preceptor, we say, I regard you as my preceptor, something

156
00:17:36,440 --> 00:17:44,520
like that, but sometimes we cannot live with preceptor for five years, so we will have to

157
00:17:44,520 --> 00:17:52,080
move to another place, in that case, when I say, I live with my preceptor for one year,

158
00:17:52,080 --> 00:17:56,520
and then I want to go to another place for study or practice, so when I go there another

159
00:17:56,520 --> 00:18:04,720
place, then I have to approach a teacher there, it is of the text or teacher of my dissertation,

160
00:18:04,720 --> 00:18:15,040
and then I have to regard him as my teacher, and that is called dependence, so may you

161
00:18:15,040 --> 00:18:21,800
be my teacher, may you be one on whom I depend, something like that, and that is called

162
00:18:21,800 --> 00:18:23,960
taking dependence.

163
00:18:23,960 --> 00:18:29,720
So when you are living with preceptor, you do not have to take dependence, because there

164
00:18:29,720 --> 00:18:41,960
is preceptor, so in the same way here, when you have done the attaining or resolving

165
00:18:41,960 --> 00:18:56,320
them, then there is no sense in doing that again, so this only monks can understand,

166
00:18:56,320 --> 00:19:04,240
even though you pick up a vigna book and read it, still there may be a little something

167
00:19:04,240 --> 00:19:10,760
you do not understand, so taking dependence in the preceptor's presence really means when

168
00:19:10,760 --> 00:19:15,560
you are living with the preceptor, you do not have to take dependence, you do not have

169
00:19:15,560 --> 00:19:24,520
to regard another person as your teacher, formally regarding another person as your teacher,

170
00:19:24,520 --> 00:19:28,720
and because this book who has resolved, let there be space, there will be space, only space

171
00:19:28,720 --> 00:19:33,320
there, and because of the power of his first resolve, it is impossible that another

172
00:19:33,320 --> 00:19:41,320
model in a tree can have sprung up, meanwhile made by temperature, temperature means U2,

173
00:19:41,320 --> 00:19:46,800
climatic conditions, however it has been created by another person as a supernormal power

174
00:19:46,800 --> 00:19:50,360
and created for its previous and so on.

175
00:19:50,360 --> 00:19:56,720
He dives in and out of the ground, but here it is rising up, out of there is called diving

176
00:19:56,720 --> 00:20:07,840
out and it is sinking down into that is called diving in, here also, he is normally an

177
00:20:07,840 --> 00:20:21,600
obtainer of a war magazine at the moment, so this is how it monk must do in order to perform

178
00:20:21,600 --> 00:20:43,520
this miracle, so a 95 on unbroken water, this is walking on water, here water that one

179
00:20:43,520 --> 00:20:50,800
sinks into one trodden on is called broken, but one who wants to go in this way should

180
00:20:50,800 --> 00:20:59,920
attain the earth casino and emerge, so you have to enter into a property agenda, I mean

181
00:20:59,920 --> 00:21:07,840
a property agenda means you have to practice a property casino in order to channel from

182
00:21:07,840 --> 00:21:20,760
there, now you want to make water look like hard and broken or ungealing, so you

183
00:21:20,760 --> 00:21:35,240
have to enter into a casino agenda, otherwise you will not be able to do that, and then

184
00:21:35,240 --> 00:21:45,040
see that cross-legged he travels, sitting cross-legged and then travels in the air, like

185
00:21:45,040 --> 00:21:53,480
a winged bird, like a bird furnished with wings, it is always in the politics it is

186
00:21:53,480 --> 00:22:00,240
always that a winged bird, and the commentaries explain that there may be some birds

187
00:22:00,240 --> 00:22:06,680
which have no wings, so here it is that winged birds, that means a bird who reaches wings

188
00:22:06,680 --> 00:22:14,120
and which you can fly, one who wants to do this should attain the earth casino and emerge

189
00:22:14,120 --> 00:22:23,640
and so on, and see, here is the text, seated cross-legged he travels in space like a winged

190
00:22:23,640 --> 00:22:34,000
bird, he is normally an obtainer of the earth casino at the moment, so on, and the one

191
00:22:34,000 --> 00:22:41,800
and a big who wants to travel in space should be an obtainer of the divine eye, divine

192
00:22:41,800 --> 00:22:49,800
eye will come in the next chapter, quite, on the way there may be mountains, streets, etc.,

193
00:22:49,800 --> 00:22:57,280
that are temperature originated, that are created by climatic conditions, or jealous naga,

194
00:22:57,280 --> 00:23:07,160
soup anas, soup anas, there should be only one pea, soup anas, carudas, you know

195
00:23:07,160 --> 00:23:27,080
carudas, it is a mythical bird, it is a vehicle of Vishnu, one god in Hinduism, so they

196
00:23:27,080 --> 00:23:31,640
may create them, he will need to be able to see these, but what should be done on seeing

197
00:23:31,640 --> 00:23:35,680
them, he should attain the basic genre and emerge, and then he should do the preliminary

198
00:23:35,680 --> 00:23:42,680
work thus, let there be space and resolve, again, we have Tipitigatula Abayatira, friends,

199
00:23:42,680 --> 00:23:48,120
what is the use of attaining the attainment, is not his man concentrated, hence any area

200
00:23:48,120 --> 00:23:53,920
that he has resolved us, that it be space is space, though his book does, nevertheless,

201
00:23:53,920 --> 00:23:58,360
the matter should be treated as described under the miracle of coin and hindered through

202
00:23:58,360 --> 00:24:12,400
wars and so on, so this, this era didn't find any favor with Pudugosa and other monks,

203
00:24:12,400 --> 00:24:18,720
and then with his hands he touches and strokes the moon and Sam so mighty and powerful,

204
00:24:18,720 --> 00:24:30,040
so he could, he could stretch out his hand and then stroke him, sun and moon, and then

205
00:24:30,040 --> 00:24:34,240
this super normal power is successful, simply through the genre, that is made the basis

206
00:24:34,240 --> 00:24:41,960
for direct, direct knowledge, there is no special casino attainment here, so just any

207
00:24:41,960 --> 00:24:51,000
genre will do, for it is study in the British, and so on, and then in paragraph 104,

208
00:24:51,000 --> 00:25:00,040
we have the words clung to clung to, so in the, in the sub commentary, it is explained

209
00:25:00,040 --> 00:25:10,360
that clung to may mean just those one of karma and also those belonging to the living

210
00:25:10,360 --> 00:25:21,680
body, the bodies of living beings, here the era to, typical chula naga, but there is

211
00:25:21,680 --> 00:25:26,880
chula apria, here is chula naga said, but friends why does what is clung to not become

212
00:25:26,880 --> 00:25:33,160
small and big to, when a big who becomes, comes out through a keyhole, does not what is

213
00:25:33,160 --> 00:25:37,880
clung to become small and when he makes his body big, does it not then become big as in

214
00:25:37,880 --> 00:25:47,480
the case of the Adam Maglana, and his, his opinion is also rejected by protocols, paragraph

215
00:25:47,480 --> 00:25:58,160
117, so it is with reference to this enlarged form created during this taming of Nanopanana

216
00:25:58,160 --> 00:26:05,000
that it was said, when he makes his body big, does it not then become big as in the case

217
00:26:05,000 --> 00:26:11,080
of the Adam Maglana, although this was said, the big was observed, the, and he enlarges

218
00:26:11,080 --> 00:26:17,360
only what is not clung to, supported by what is clung to, and only this is correct here,

219
00:26:17,360 --> 00:26:28,320
so the opinion of chula naga was also not accepted by, by monks, now when you, when, when

220
00:26:28,320 --> 00:26:41,560
the monk creates a bigger body, then his normal body, then there is the enlargement,

221
00:26:41,560 --> 00:26:49,200
so the question here is, does he enlarge what is clung to, what is not clung to, does

222
00:26:49,200 --> 00:27:01,040
he enlarge the, the, the ruba born of kama only, or does he enlarge, ruba born of others,

223
00:27:01,040 --> 00:27:11,840
and the answer is, he does not enlarge or he, he could not enlarge, what is caused by, what

224
00:27:11,840 --> 00:27:24,560
is born of kama, but the, the mind, mind born, mind born material properties he can enlarge

225
00:27:24,560 --> 00:27:33,240
with his, the, the power of his mind, and along with that, and the material properties

226
00:27:33,240 --> 00:27:42,880
is born of temperature, can also be enlarged, and here the reference is given to Maha,

227
00:27:42,880 --> 00:27:51,880
elder Maha Maglana, and a, a long story follows, where Maha Maglana tamed the, the

228
00:27:51,880 --> 00:28:08,320
royal naga named Nandobananda. So here the elder Maha Maglana created, transformed himself

229
00:28:08,320 --> 00:28:16,520
into a naga, into a, into a, into a snake, and then I think, have you read the story,

230
00:28:16,520 --> 00:28:25,520
the story, the, the naga didn't like Mark flying through the above him. So he was angry

231
00:28:25,520 --> 00:28:33,760
and, so he, he, he, ah, quiled round the Mount Meru seven time, and covered it with his

232
00:28:33,760 --> 00:28:39,760
food. So when Buddha and his disciples went there, some disciples asked, formally when

233
00:28:39,760 --> 00:28:45,160
we reach to this place, we see the, the Mount Meru, now we don't see anything, what

234
00:28:45,160 --> 00:28:52,160
it happened, and Buddha said, there's a naga, he doesn't like you. So the, the, the,

235
00:28:52,160 --> 00:28:57,800
the disciples said, let me tame him, let me tame him, but Buddha did not allow other

236
00:28:57,800 --> 00:29:04,760
mom, and at last Maglana said, let me tame him, tame him, then Buddha said, who, Buddha

237
00:29:04,760 --> 00:29:14,600
knew that he, he alone, was able to, to tame that servant, because he was very powerful.

238
00:29:14,600 --> 00:29:25,480
So in order to, what to go, in order to tame him, Maglana turns himself into a naga,

239
00:29:25,480 --> 00:29:32,680
and then twice the size of dead naga, and quiled around him 14 times. And then, ah,

240
00:29:32,680 --> 00:29:39,840
what to go with it, put pressure on him so that he was, ah, he was crushed between the

241
00:29:39,840 --> 00:29:48,560
mountain and then the, the, the monk, the monk said that. So when Maglana did, did that,

242
00:29:48,560 --> 00:29:59,840
did he, did he enlarge the material property born of Kama only, or what did he do? So,

243
00:29:59,840 --> 00:30:08,960
so the answer is what? He enrages only what is not clantu, supported by what is clantu,

244
00:30:08,960 --> 00:30:16,480
and only this is correct here. Clantu, supported by, not clantu, supported by what is

245
00:30:16,480 --> 00:30:37,040
clantu. So he enrages only those, ah, born of or caused by, ah, mind and the temperature

246
00:30:37,040 --> 00:30:44,000
of climactic conditions. And they are supported by the, what is clantu means, ah, what is

247
00:30:44,000 --> 00:30:53,040
born of Kama? And that is the, the translation of the explanation given in the

248
00:30:53,040 --> 00:31:02,880
subcominary as a footnote. Now, at the end of that footnote, say, it should be taken that

249
00:31:02,880 --> 00:31:07,760
it is the consciousness born matter that is enlarged by the influence of the subonormal

250
00:31:07,760 --> 00:31:17,840
power. And the temperature born is enlarged. And what is that paripasu? It's foreign word,

251
00:31:17,840 --> 00:31:29,600
right? That's Latin. Ah, it means that, ah, something about passing.

252
00:31:29,600 --> 00:31:39,720
It's a tendency to make sure you have a picture. Oh, yeah, it might mean something like,

253
00:31:39,720 --> 00:31:51,840
um, in, in, in passing, but not, not as a consequence, but along with, along with, yeah,

254
00:31:51,840 --> 00:31:57,520
along with, but see, I think it, we should find out whether they need other consequences.

255
00:31:57,520 --> 00:32:04,160
The, the poly word is the de nozzare, and that means following that. So, along with this

256
00:32:04,160 --> 00:32:18,600
close to that. So, one can enlarge only, ah, matter, ah, born of consciousness. And when

257
00:32:18,600 --> 00:32:26,120
consciousness has gone, matter is enlarged, then temperature born is also enlarged. But one

258
00:32:26,120 --> 00:32:33,280
cannot enlarge, ah, comma born, comma born ruba, because they are caused by comma in the past.

259
00:32:33,280 --> 00:32:40,560
And so, ah, ah, ah, we cannot do anything about the comma born, ah, material properties.

260
00:32:40,560 --> 00:32:48,120
But, ah, with regard to consciousness born, ah, material property, you can do something.

261
00:32:48,120 --> 00:32:55,120
And then, even if we, if we don't get Jana, when we are happy, then our appearance and it's,

262
00:32:55,120 --> 00:33:03,000
it's different from when we are said, oh, that, these are caused by change. I mean, consciousness.

263
00:33:03,000 --> 00:33:16,960
So, again, and large consciousness is more. Okay. I think that's all we do today.

264
00:33:16,960 --> 00:33:39,080
Next week, we begin from paragraph 119. And then, we will go over to the next, next chapter

265
00:33:39,080 --> 00:34:02,600
also. So, 100, 50, maybe 158. We will have a description of how the world ends and how

266
00:34:02,600 --> 00:34:20,680
it begins. Buddhist Genesis. Okay. So, there are two more, two more classes. And the very

267
00:34:20,680 --> 00:34:32,520
first and then we will start up again on the 21st. So, the first. So, the first two classes

268
00:34:32,520 --> 00:34:39,400
in September, we won't leave. And then, we can't start in the third, we can start.

269
00:34:39,400 --> 00:34:44,760
The second line. The next chapter. Sorry, the chapter 14.

270
00:34:44,760 --> 00:34:51,760
Now, second and part two. Yes, part two of the, according to this book, yes.

271
00:34:51,760 --> 00:35:04,600
The soil and which understand the growth. Now, we, again, we begin with the aggregates.

272
00:35:04,600 --> 00:35:20,240
The basis, faculties, four noble tools, and then the soil of, the soil of understanding

273
00:35:20,240 --> 00:35:36,880
is something, but it's just a move on up. So, next.

274
00:35:36,880 --> 00:35:54,740
Anything else.

275
00:36:24,740 --> 00:36:27,740
.

276
00:36:27,740 --> 00:36:30,740
..

277
00:36:30,740 --> 00:36:32,740
..

278
00:36:32,740 --> 00:36:34,740
..

279
00:36:34,740 --> 00:36:38,740
..

280
00:36:38,740 --> 00:36:42,740
..

281
00:36:42,740 --> 00:36:45,740
...

282
00:36:45,740 --> 00:36:49,740
...

283
00:36:49,740 --> 00:36:51,240
...

284
00:36:51,240 --> 00:36:54,240
..

285
00:36:54,240 --> 00:36:59,240
.

286
00:36:59,240 --> 00:37:04,240
.

287
00:37:04,240 --> 00:37:09,240
.

288
00:37:09,240 --> 00:37:14,240
.

289
00:37:14,240 --> 00:37:19,240
..

290
00:37:19,240 --> 00:37:22,240
..

291
00:37:22,240 --> 00:37:23,240
..

