Hello and welcome back to our study of the Dhamma Padha.
Today we continue with verse 218, which reads as follows.
Bhagavad-Jato, Anakati, Manasaja, Portosia, Kami, Su-apati, Badhajito, Goodhang Soto-Dhi,
Ujati.
Chandajato, one who's mind, one who is intent upon, or inclined towards Anakati,
that which is indescribable.
One is intent upon that which is indescribable, inclined towards it, keen on it.
Anasaja, Portosia, Portosia, if one's mind leaps or plunges into, really gets into, is thrilled
by, is excited by, is fully into, we would say, in English, into that, the Anakata, the
indescribable, Kami, Su-apati, Badhajito, if one's mind is not bound up, or tied down by
kama, by sensuality, Ujang Soto, Ujang Soto-Dhi, Ujati, such a person is called, Ujang Soto,
one who is going upstream, one who has a stream going upwards.
So this story was taught, this verse was taught in response to a story about a elder monk
who was getting old, and his students, his Santa Viharika, the monk who lived with him
so in the time of the Buddha, it was established that new monks should spend some time
with a senior monk in order to learn how to become a good monk.
So, they would spend time, sometimes even staying in the same bedroom, they wouldn't
have beds, they wouldn't have the same bed, but they would live quite close so that
the young monk could learn how to live from the old monk, just watching and following
and really taking care of the older monk and so on, so it became sort of mentorship.
So it might not be correct to say this was a teacher student relationship, but it was
a mentor and dependent relationship.
It was explained like that, just said to you, Ujang Soto, one who lives with you, learn
how you would be a good monk, and so the monks, I guess there was more than one living
with him, asked him, and they said, bend or bhocir, what sort of benefits have you gained
from the practice, what sort of results, anything special?
Now this monk had indeed practiced and indeed gained something quite special from the
dhamma, he had become what we call an anagami, anagami, sorry, anagami, anagami means one
who doesn't come back, it's the third of four stages of enlightenment.
One one first, experience is nibana, they're called a Sotapana, here we have this word
Sotag, and Sotam means a stream, that word is used because it means someone who is not
going to fall back, they're being pulled towards enlightenment, there's the word stream
to indicate that they're on their way, it's like they get on an escalator and you can't
go back down again, it just takes you off.
But such a person still has a lot of defilements left potentially, and so they may still
get angry, they may still be craving things, they may still get worried or may still,
they won't be jealous or stingy, but they can still crave and get upset, sad, frustrated
and bored, but far less than ordinary people, they've come a long way and they've given
up any kind of wrong view about the dhamma, any doubt about the dhamma, any wrong practices
they have no inclination to do things that are useless, they'll never make a mistake
about what is useful and what is useless thinking, something that is useless is actually
spiritually beneficial, like rituals or rules, no, they know the way to become enlightened,
but the higher stages start to get rid of more of the defilements, and so we have what
to call the sakadagami, someone who will come back, but will come back only once, that's
because they've worked harder than the sotapana, they've moved on from there and they've
continued the work to the point where they're only going to come back once and then
after that they'll free themselves completely from suffering, but an anagami or the third
age, an anagami has gotten rid of completely any kind of sensual desire, they've come
to see it for the illusion that it is, and any kind of aversion or they're disliking
of things, any hatred of others, any anger or frustration or irritation.
And so this is who this monk wants, he was actually quite advanced, anyone who can claim
to be free from all any form of sensual desire is quite impressive, any kind of aversion
or irritation, it's quite a powerful state.
But he thought to himself, you see, because there's a fourth stage, and the fourth stage
is very powerful, they say someone who becomes an arahat, see an arahat is someone who
has gotten rid of all delusion as well, so an anagami might still get distracted, might
still have a conceit, a conceit is another big one, an anagami might still be conceited,
might still be proud of themselves, might still be have low self confidence, feeling bad
about the fact they're not yet an arahat. And it seems that that's what this monk was
feeling, he felt kind of guilty for not being an arahat, because he thought to himself,
you know, with an arahat, an arahat, if a lay person becomes an arahat, they have to
ordain, if they don't, they say they will die, there's just two ways either they pass
away, or they become a monk and they go on arms run, so they don't have to engage in practices
that they just see no use in, I mean like like getting a job and seeking out food and that
sort of thing, but an anagami, there are many lay people who became anagami, and so he
thought to himself, this is just, I'm no better than a lay person, even a lay person could
become an anagami, and he thought to himself, I'm not going to declare, you know, my attainment
until I become an arahat, while wait until I become an arahat and then I'll, then I'll let
these guys know what I've gotten out of the practice, I really wait till I've realized
the complete goal, and so he didn't say anything, and again and again time and again they
would ask him and they started to get worried thinking, why isn't he answering?
He must be, he must have gained nothing from the practice and feeling guilty about it,
so they were concerned and they asked him again and again and never said anything, and
then he died, he died without becoming an arahat and so when he passed away he was an anagami,
he didn't return, where did he go, he went to what we call the sudawasa, sudha means
pure and a wasa means abode or dwelling, so the pure abodes, they are Brahma world, but
they're very special Brahma world, they're a world that beings exist who have become anagami
and that's it, only people have become anagami, it's only beings that have become anagami
is a reborn there, it's a very special realm of existence and these monks were quite upset
because they didn't know any of this and they thought their teacher had or their mentor,
the guy who they'd looked up to and got nothing out of the practice, they felt bad for
him, maybe they felt bad for themselves for wasting their time on such a teacher and they
went to the Buddha and they were crying, apparently, and moaning and wailing and dawn,
and the Buddha said, what's this all about, it's an old vendor, both sir, our poor teacher
just passed away and the Buddha said, well, haven't I taught that this is the nature
of things to pass away, why are you crying about someone dying, aren't you real monks,
like hasn't your teacher taught you anything, crying about death when death is such an obvious
and glaringly certain part of life, something to be shocked about or upset about and they
said, well, it's not that, we know that, Benarbo, sir, it's just that he died without
getting anything, he asked him again and again and he didn't say anything, he must have
died and who knows where he went because he had no benefit from the practice and the
Buddha explained to him, he said, oh no, no, my son has been reborn in the Sudawasa,
he just was embarrassed because he was only Anana Gami and he thought he'd wait until
he became Anara hut and then he taught this verse explaining that really they should
have known or could have known or maybe not based on their own state of mind but it would
be possible to know and there's no possibility that that monk could have gone anywhere
else because not of some category but because of his state of mind, his qualities of
mind and that's what this verse is meant to highlight.
So we can get a couple of things from this story.
The first is, of course, the most obvious one and the main lesson of the story, I think,
is not to be complacent and Anana Gami is such a high attainment and yet even people
or maybe especially people who become Anana Gami, one thing it does to them is make them
not complacent.
I think there was a story about some Anana Gami that became complacent and the Buddha
had to scold them, admonish them, but here we have, this shows that this monk pointed
out an important lesson that even when you've gotten to that state, you have to remember
that you still have defilements, why would it means that there's a difference between
an Anana Gami and Anarahant, is that you still have to concede and restlessness and ignorant.
You still have desire for form and formlessness and no central desire that.
And so it's a really good example for us, for people who have maybe not gained anything
from the practice to remind ourselves, you know, some people become content just with calm,
they practice meditation for a bit and they feel calm and they become content with that.
Some people gain insight, gain some good understanding about how to live their lives and
they become content with that.
They become content with something that's really ineffective in the long run.
You know, it will have an effect on our lives, but it has no potential for, or it has
no guarantee, it has no power to free from suffering.
You feel calm and it's very easy to lose that and get back to square one.
Again, insight, maybe it changes your course in life, which is a very good thing, but who knows
how long that will last?
It will go away, you'll forget about it.
Then Buddha said, Asan-tutī bhāhālau, you want to be a great being, you want to really
do something great, something really and truly great.
You have to be discontent, not discontent in the way we normally think about it.
The Buddha taught and praised contentment, but discontent about your own spiritual attainments,
your own quality of mind, until you have freed yourself from all bad habits, bad qualities
of mind, till you've freed yourself from all ignorance, and you've come to understand
reality just as it is, and you should not be complacent, you should not be content with
what you've done.
Of course, people who have attained so Dapana become complacent with good reason, because
it's quite a change, it's quite an accomplishment, and your whole being feels different
at the peace and the security, the feeling of stability of mind.
Sakadagami Anagami is still possible to become complacent, so just a reminder, of course,
that in our practice, if even an Anagami is able to admonish themselves not to be complacent,
you should remember the reason why we're practicing, it's not just because it's beneficial
to us, that's one way of looking at it, but also because we have things that are unbeneficial
that we need to address.
We really do have an imperative task to perform, that is to learn, to study, to become
familiar and understand and overcome our bad habit, our unwholesome qualities of mind.
So that's the first lesson.
The second lesson, I think, is a little more subtle, and may or may not be there, but
it appears that the Buddha is perhaps, or you could take this as an admonishment, or
as a lesson for us, not to put too much emphasis on titles, proclamations, identities.
We have these four stages of enlightenment, it's very easy for people to obsess about them.
People saying to, I've heard people say, my goal is to become a Sotapana in this life,
something.
That's not, I don't mean to criticize that, but it really isn't a great way to focus.
I mean, it isn't the most important thing to focus on.
Your focus should be on, of course, being mindful.
And this is a good example of the distinction.
These monks were so focused on this idea, the switch that they could turn in their minds
where they switched from thinking of him as an ordinary person to an enlightened person,
that they never even saw how enlightened he was, right?
Because if they lived in the same room with him, it's certainly possible that it's
very, I mean, it is, maybe a third lesson, it is hard to know whether someone else is
enlightenment, it's a very personal thing, but they're so close to him.
And I think it's quite possible that had they not been so obsessed with trying to determine
whether he was or wasn't, they would be able to instead be very mindful and become
in tune with his state of mind and really get a sense that most likely he was not an
ordinary being, I think it's very hard to mistake an Anagami for an ordinary being.
But third lesson, it can be difficult.
You see, even these monks who lived with the Anagami didn't know, and they didn't
know because two reasons, one, because they weren't paying attention to, because it
is very hard to know.
The Dhamma is Pachatang, to be known for oneself.
It really isn't always the best to proclaim your attainment, so the monks aren't allowed
to talk about them with people who aren't monks, but even among monks, sometimes it's
better just to focus more on qualities of mind, you know, focus more on what's important
and focus more on oneself.
Someone asks you, are you enlightened to say, why do you care about me, you know?
You should turn it back on them and say, the only enlightenment you should be concerned
with is your own.
Sometimes people try to test, I don't get it so much, but I suppose I have, I don't
notice it so much, but I've heard of monks getting tested.
I think it happens a lot in Buddhist societies, Buddhist to know a lot, and they're very
skeptical of other Buddhists, and so they go around to monks and they try to test them
to see how enlightened they are.
In some ways it's a good service, because it does test the monks, you know, they might
try and make them an irritated or annoyed, but on the other hand it's really a waste
of that person's life, you know?
Why are they so focused on determining whether other people are enlightened?
And it becomes a bit of a problem from the other end as well, and when someone does identify
another person as an enlightened being correctly or incorrectly, I think it's very common
to incorrectly or with insufficient evidence, identify someone else as being enlightened
or oneself, even, I think it's possible to overestimate one's own attainment.
And it becomes, you see, this is the thing about labels, it becomes an entity, a person
who is an Arahan, a person who is an Anagami, and you put them up on a pedestal, and it
makes you feel good if they're your teacher, right?
My teacher is such and such, which is really awful because them being in such and such
says nothing about you, even if they are.
I've seen very pure monks who have very, very poor and impure students or followers,
just because you follow the money even said it, just because you follow around, if you
were to follow around holding on to my robe, I can't take you to Nibana, that's not
what he said exactly, but basically that.
It with me all day, hang out with me all day, there's nothing to you, it has potential
to be completely unbeneficial when you could be off meditating and following my teachings.
He did say that, basically.
So there's the lessons from the story, don't be complacent, but also don't obsess
about titles and categories and so on.
The focus should be on what the verse focuses on, and why I think it may be very well
with the Buddha, a part of the Buddha's lesson, this sort of admonishment not to do that.
It's because of what the Buddha says, he says, look, of course he was an Anagami, he had
all these qualities, where he had, that's not a lot of qualities, but this basic quality
of mind, that distinguishes someone who is not coming back from someone who is coming
back, so what is it?
So Nibana Anakat is the thing which cannot be told, which cannot be explained, not
be described, and apparently that's what ineffable means, so we can say it's the ineffable.
It's very different, it's ineffable, the problem with those sorts of words is that it
gives it this sort of mystery, which it really shouldn't, we should absolutely understand
that that is an essential quality of it, that being ineffable.
It doesn't mean it's something that if only we were smarter we could talk about it, we
could explain or we could understand, we could conceive of it.
It's like the inconceivable, it's a thing that is literally like actually inconceivable
in the sense of it's not, there's no concept involved with it, there's no entity per se.
It exists, but it exists in a different way than formed things that you can describe,
conceive of, that they exist, and that's exactly what it is, it's that thing that is
as those qualities, as the quality of being indescribable.
It's the thing that is indescribable, it's the, it's out, it's that which is outside
the realm of being indescribable.
That's just what it is, it's not something mysterious, it's just that thing, they haven't
made it much clearer by saying that, but I just want to explain that it's, it's not meant
to make it mysterious or obfuscate the meaning or anything, it's just talking about
what is different from everything else, here I am talking a lot about it, just trying
to explain why it's what it is.
And so Chandajata, Chandayas is an important word, he uses this I think because Chanda
can be used both ways, Chandayas is often used as a replacement for desire, entire they
say pajaya, which I think is a good, we don't really have a similar way of talking about
things in, in English, but someone is pajaya, jaya is the heart, pause enough, pajaya
means contentment or finding something to be enough, but they use it to mean liking, you
know, if you're pajaya in regards to food, it means you like it.
And so it's kind of used in a sense of liking something, but it more literally means interest
or inclination, intention, in a sense of being intent on something.
So one translator I think uses intent on the ineffable.
Incline is a good word, because the Buddha uses this imagery of a tree leaning in one direction,
and this is where the distinction is made here, someone who is inclined towards nibhana,
is really very, and categorically different from someone who is inclined towards kama, towards
sensuality.
And that's the real distinction here, it's a very simple description of the quality
of enlightenment that it inclines away from sensuality and towards the opposite of sensuality.
And so this is, it's an important point about this, is that it's a description of the
problem, what's wrong with us, with ordinary life, with ordinary inclination, because
when you hear about nibhana, for example, when you hear about nibhana, when you hear
about the path, when you hear about the direction, it's often quite hard to swallow,
it's unpalatable, fears to me even.
Some people who really like what the Buddha said will become inclined to practice, they
meet Buddhists and they really feel attracted to the path and to the ideas behind it.
But then they hear about nibhana and it scares them.
What is this, I've gotten myself into, one meditator told me, wait, do you know what this
means?
Do you really get what we're doing here?
It just shocked him and he couldn't go on, he couldn't get past this because he liked
the idea of gaining benefit from meditation, but he understood what the meaning was here.
You know, nibhana involves non-arising and so often our practice seems to be trying to
change our opinion about nibhana, trying to like it, trying to incline towards it.
We want to create in our minds this inclination towards nibhana, but it's not quite like
that.
It's in fact the disintegration that we take as our object.
It's not that we practice liking it, it's not that we practice or I guess more common,
it's not that we repress our distaste for nibhana, our distaste for having to give up
all of this, all of what we enjoy, all of what we seek, all of what we cling to, all
of what we are but the bandajita, all of what our minds are stuck on, are attached to.
Our practice is understanding this taste, I mean the struggle in the mind of a Buddhist
to come to turn with something as the goal that they really don't like, don't even like
the idea that they might not admit it, they'll say yes, nibhana, but when they think
about it, it scares them and it's repulsive.
It's not about repressing that or trying to be the person who likes that, no, it's
about looking at and that's the actual problem, looking at this, not just that, but looking
at the whole reason why that is, why it is that we're disinclined towards something
that really, I mean it's inevitable, how could you be disinclined, how could you
hate it, you see because our, our, our distaste for it, fear of it, it's nothing to do
with it.
There's nothing to fear about nibhana, there's nothing fearsome, it doesn't have fangs
or claws or poison, it's not going to say bad things about you or betray you, it's not
even going to arise, no, it's all of the things that it stands in opposition to, all
of those things again that we cling to.
And so our whole of our practice, our practice, this is why our practice is not about
nibhana, why is our practice about suffering?
Because all of those things that we cling to are actually suffering are actually dukha,
they may not be painful, not all the time, but they're caught up in pain, they're caught
up in disappointment, they have no suka, dukha means they can't bring suka, they can't
make us happy, they can't satisfy us, we strive for them again and again and again,
and we always have in our mind this idea that they are going to, those things that I don't
have yet are going to bring me happiness, not even intellectually, it's just an inclination,
we're a tree leaning in the wrong direction, so that's the way we're going to fall.
It's endless, there's no goal, we're like a dog that has a rash and it keeps scratching,
because there's some sort of appeasement that comes from scratching, but the scratching
just makes it worse.
And so either they end up scratching themselves to death until they bleed and infected
and die, or they stop, they find a way to stop themselves, perhaps they stop for some
time because of the pain that comes from it, but there's no end, you can't get satisfied
by getting the things you want.
And so our practice is to learn this, is to understand this, is to become freed from the
bind, from the shackles of central desire, they were really shackled to it, we can't be
free, we can't be at peace, because our minds are forcing us really, we're prisoners
to our own, but the abundance of our minds that are bound to sensuality, it's like we're
being pulled in that direction by a strong rope and the meditation may need to rope in
the other direction, and we pull ourselves until we incline towards the ineffable,
who don't and that's how you become, who don't sort that, who don't sort if you think
of it as against the stream, it's like a fish going against the stream, but if you think
of it as upstream, I think it probably means more like has a stream going upwards, they're
on the escalator upwards, they're heading in the right direction, they're bound in the
right direction, why? Because their mind is inclined in the right direction, and that's
really this emphasis and this focus on qualities, how does the mind incline, does your mind
leap at the thought of peace or does it incline towards conflict? If someone hears someone
does something, says something wrong, do you incline to correct them or make them punish
them for it? When something pleasurable comes to incline to get it, to keep it, to get
it as close to you and as much, possess it as much as possible, or do you see it mindfully,
do you see it, experience it and let it come and let it go? One is the way to stress
and present, the other is the way to freedom and peace. So I think focusing on, this
is a very simple way to describe this distinction that you shouldn't be concerned that
you don't like peace, that we aren't, that you aren't inclined towards things like nibana
and freedom from suffering and cessation, to never be born again, shouldn't be concerned
about that, that's the whole reason, that's the whole crux of this, that's what you have
to look at. It's like a challenge in a way, pointing out nibana, pointing out freedom
from suffering is a challenge, it shows you, you have to justify, and you really do have
to examine because now you have an alternative, the thing you don't want, I want these,
you have to be able to justify to yourself that it's better than this peaceful, this
ultimate peace, ultimate freedom and so it means you examine, you don't worry about nibana
for sure, not you examine the tree that you're in until you see there's no fruit on
this tree and then you can fly away, so some thoughts on the verse for tonight, that's
them upon the 218, thank you all for listening.
