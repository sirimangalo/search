1
00:00:00,000 --> 00:00:29,280
I'm good evening everyone, welcome to our live broadcast.

2
00:00:29,280 --> 00:00:41,080
So the Buddha's teaching is, if you spend any time looking into it, it's quite a vast ocean

3
00:00:41,080 --> 00:00:57,920
of teachings and histories and stories and there's a lot there.

4
00:00:57,920 --> 00:01:03,720
You can make it somewhat daunting to try and understand even the simple aspects of how

5
00:01:03,720 --> 00:01:11,040
to practice meditation.

6
00:01:11,040 --> 00:01:17,600
So there are different ways of dealing with this, of approaching Buddha's teaching when

7
00:01:17,600 --> 00:01:24,280
explaining one teaching.

8
00:01:24,280 --> 00:01:30,520
One of them is, we see this in the Buddha's teaching, one of the ways is to pick one

9
00:01:30,520 --> 00:01:35,280
quality and talk about it in great life.

10
00:01:35,280 --> 00:01:44,560
So this is the zooming on a teaching.

11
00:01:44,560 --> 00:01:51,240
They're called sutas and it's a good word, whether or not it's what it originally meant,

12
00:01:51,240 --> 00:01:53,920
but sutas means of thread.

13
00:01:53,920 --> 00:02:02,320
So the idea is that you can start from one point and you can connect it to the rest of

14
00:02:02,320 --> 00:02:13,760
the whole in many ways of looking at Buddhism and looking at the Buddha's teaching.

15
00:02:13,760 --> 00:02:32,040
And there are many ways of approaching the same concept or the same idea, the same core.

16
00:02:32,040 --> 00:02:37,520
Another way of going about it, something that I'd like to look at today is the idea of giving

17
00:02:37,520 --> 00:02:44,400
an overview of the Buddha's teaching.

18
00:02:44,400 --> 00:02:50,160
And as with finding a single thread, there are also many ways of looking at the entire

19
00:02:50,160 --> 00:02:52,720
tapestry of the Buddha's teaching.

20
00:02:52,720 --> 00:03:00,640
So there are many summaries, many ways of summarizing the Buddha's teaching, but I think

21
00:03:00,640 --> 00:03:10,600
one of the most macroscopic or all-encompassing is what the Buddha called the Bodipakya

22
00:03:10,600 --> 00:03:30,960
Dhamma, Bodhi meaning enlightenment, Bhakya having a part, Bodhi Bhagya Bhakya having a part

23
00:03:30,960 --> 00:03:31,960
in enlightenment.

24
00:03:31,960 --> 00:03:41,720
Why I think this is one of the most all-encompassing is because there's 37 of them.

25
00:03:41,720 --> 00:03:48,720
So it's a list of lists actually, it's a group of lists in total enumerating 37

26
00:03:48,720 --> 00:03:52,640
dhammas, 37 aspects of the Buddha's teaching.

27
00:03:52,640 --> 00:03:59,720
I don't want to go into detail with these, but with each of them, but hopefully give

28
00:03:59,720 --> 00:04:06,840
them a bit of an overview because they're all very much related to meditation, so it gives

29
00:04:06,840 --> 00:04:09,600
us a good idea of the sorts of things that we're cultivating.

30
00:04:09,600 --> 00:04:14,680
And the best way to look at this is not to think, wow, I have to study 37 individual

31
00:04:14,680 --> 00:04:17,040
teachings of it.

32
00:04:17,040 --> 00:04:24,040
That's how I get a complete understanding of practice of these two, enlightenment, but

33
00:04:24,040 --> 00:04:33,160
rather to understand that when we practice mindfulness, when we practice the force at

34
00:04:33,160 --> 00:04:38,160
Devatana, there's a lot more that comes along with it, so a lot of great things that

35
00:04:38,160 --> 00:04:44,680
come along with it sort of help give us an idea of what it's like to practice mindfulness

36
00:04:44,680 --> 00:04:51,080
or what it should be like, which should be involved, so we can have some encouragement

37
00:04:51,080 --> 00:04:56,520
that we're practicing properly.

38
00:04:56,520 --> 00:05:07,280
So the first four of these are the force at Devatana, the basis of the meditation practice,

39
00:05:07,280 --> 00:05:08,280
and these ones are obvious.

40
00:05:08,280 --> 00:05:12,840
This is where if you're not incorporating these four into your practice, well, it's hard

41
00:05:12,840 --> 00:05:21,000
to say that you're actually practicing to become enlightened, so it has to start.

42
00:05:21,000 --> 00:05:22,000
Right here.

43
00:05:22,000 --> 00:05:25,680
These are the ones that it should be easy to understand how this relates to our practice

44
00:05:25,680 --> 00:05:37,000
where the body, so when we watch, we watch the movements of the body, or the breath as

45
00:05:37,000 --> 00:05:44,360
it relates to the body, the stomach rising and falling, for example, mindfulness of

46
00:05:44,360 --> 00:05:49,960
the body when we watch the feelings and we're mindful that we remind ourselves, this

47
00:05:49,960 --> 00:05:58,840
is feeling, we do not, we do not know but seeing, we have it, this is pain, this is

48
00:05:58,840 --> 00:06:06,600
pleasure, this is calm, the reminding ourselves, this is the basis of the practice, because

49
00:06:06,600 --> 00:06:14,600
it puts us in a position to be able to see things objectively without judgment, reminds

50
00:06:14,600 --> 00:06:15,600
us.

51
00:06:15,600 --> 00:06:20,900
In me, it's not mind, it's not good, it's not bad, it focuses our attention on object

52
00:06:20,900 --> 00:06:34,120
to reality, the Kairi, the Najita, the mind focusing on the mind and dhammas when we focus

53
00:06:34,120 --> 00:06:41,080
on the hindrances or the senses or the aggregates, this is all under the four foundations

54
00:06:41,080 --> 00:06:45,440
of mindfulness, so this is the basis.

55
00:06:45,440 --> 00:06:51,440
This one is clear how this is in our practice, should be clear, and if it's not, wow, this

56
00:06:51,440 --> 00:06:57,720
is where you have to start your learning.

57
00:06:57,720 --> 00:07:05,480
The second one is called the sum of the tana, the four right exertions, another one of

58
00:07:05,480 --> 00:07:12,720
the Buddha's teachings and it also protects the enlightenment or has a place apart, but

59
00:07:12,720 --> 00:07:16,400
that means is that when you practice while you're cultivating these four as well, it's

60
00:07:16,400 --> 00:07:22,120
which would be encouraged that we're not just being mindful, we also have right effort,

61
00:07:22,120 --> 00:07:34,520
we're exerting ourselves properly, why, how do we know this, right effort is not just

62
00:07:34,520 --> 00:07:43,120
about pushing yourself harder or working harder and harder at something, it's about

63
00:07:43,120 --> 00:07:48,520
being meticulous in cultivating wholesomeness and maintaining wholesomeness, before some

64
00:07:48,520 --> 00:07:57,040
of the tana, the effort to guard against unwholesomeness and to eradicate it when it's

65
00:07:57,040 --> 00:08:05,400
arisen, and the effort to cultivate wholesomeness and to protect it once it's arisen, so

66
00:08:05,400 --> 00:08:10,200
there's very much about practicing mindfulness, as I was saying, the objectivity that

67
00:08:10,200 --> 00:08:18,160
comes with that's the goodness and the judgment, the partiality, the aversion and the

68
00:08:18,160 --> 00:08:24,080
diction, these are the bad that we try to do with it, so simply by being mindful, you

69
00:08:24,080 --> 00:08:29,480
have already a right exertion, and you're doing the right work and working in the right

70
00:08:29,480 --> 00:08:40,200
direction, the third is the third is the idi-bada, the third set, there's four of these

71
00:08:40,200 --> 00:08:49,320
as well, sort of up to 12, the idi-bada are perhaps a little more prescriptive in the

72
00:08:49,320 --> 00:08:54,200
sense that they'll make and break your practice, it's possible to practice without them,

73
00:08:54,200 --> 00:09:00,240
but it's not possible to succeed without them, so this is a measure of our success,

74
00:09:00,240 --> 00:09:05,120
if you're wondering how to succeed in practicing mindfulness, well, listen up, the idi-bada

75
00:09:05,120 --> 00:09:12,280
what you need, the first one is chanda, you need to like what you're doing, not exactly

76
00:09:12,280 --> 00:09:21,120
like, but you need to be content with it, chanda is this sort of catch-all phrase that

77
00:09:21,120 --> 00:09:27,680
can refer to desire, but it means that you're excited or energetic about what you're

78
00:09:27,680 --> 00:09:33,360
doing, there's something that's pushing into it, you have no interest in mindfulness,

79
00:09:33,360 --> 00:09:40,440
well, how can you possibly succeed, right, and this is what we see, you know, when you

80
00:09:40,440 --> 00:09:44,320
try to push mindfulness on others or when people bring their children to come in practice

81
00:09:44,320 --> 00:09:49,200
or their husbands and wives, try to drag their friends into practicing, you can see

82
00:09:49,200 --> 00:09:56,680
what is very low success rate, most of the time there's no chanda involved, there's

83
00:09:56,680 --> 00:10:07,080
no interest in it, interest isn't something you can give to someone else, it's something

84
00:10:07,080 --> 00:10:12,720
that must be cultivated, so you have to ask yourself, what is worth being interested in,

85
00:10:12,720 --> 00:10:17,720
is mindfulness worth being interested in, am I interested in it, you're not interested

86
00:10:17,720 --> 00:10:22,080
in mindfulness, well, that's quite a scary thing, maybe it's time that you started

87
00:10:22,080 --> 00:10:28,480
valuating your focus in life, what is it that you're interested in, why aren't you interested

88
00:10:28,480 --> 00:10:35,680
in mindfulness, second one is effort, well, this just goes back to the second set, you

89
00:10:35,680 --> 00:10:44,200
need to put out effort, you need to work at it, the third one is jitta, jitta means you

90
00:10:44,200 --> 00:10:53,160
have to keep it in mind, and to keep your mind on the practice, if you're interested

91
00:10:53,160 --> 00:10:59,760
in it but you never pay attention, you never actually put your mind to it, you know,

92
00:10:59,760 --> 00:11:12,560
you won't succeed, and the fourth is Riemungsau, which is sort of this discrimination

93
00:11:12,560 --> 00:11:18,000
where you evaluate your practice, there's the ability to step back and correct your

94
00:11:18,000 --> 00:11:29,720
practice, so jitta means pushing ahead, Riemungsau means adjusting,

95
00:11:29,720 --> 00:11:36,440
you know, not just pushing ahead all the time, but pushing ahead and then realizing you're

96
00:11:36,440 --> 00:11:41,080
off course going near the direction, so seeing what you're doing wrong, the ability to

97
00:11:41,080 --> 00:11:47,280
see the things that you're missing, the ability to evaluate, wait a second, I'm not really

98
00:11:47,280 --> 00:11:54,840
being mindful here, or just exceed, you need these four, but these come up in the practice

99
00:11:54,840 --> 00:12:00,960
and they're the challenge that a meditator faces, cultivating all four of them, this is how

100
00:12:00,960 --> 00:12:08,360
you wrestle with the practice until you progress to succeed.

101
00:12:08,360 --> 00:12:19,200
The fourth and fifth set are the injury and the balla, and it's the same, but different

102
00:12:19,200 --> 00:12:26,960
way of looking at the same five things, so we have sanda, vitya, sati, samadhi, panya.

103
00:12:26,960 --> 00:12:34,320
Sanda means confidence, we need confidence, vitya is effort, sati is mindfulness, samadhi

104
00:12:34,320 --> 00:12:42,760
is concentration, and panya is wisdom.

105
00:12:42,760 --> 00:12:46,520
And so here you start to say, oh, there's a lot of things to keep in mind, but it's not

106
00:12:46,520 --> 00:12:51,400
really like that, the point is that when you practice again, this five come up, but talked

107
00:12:51,400 --> 00:12:57,480
about this extensively, that you shouldn't try to cultivate any one of these besides mindfulness.

108
00:12:57,480 --> 00:12:59,800
Mindfulness is the balancing one.

109
00:12:59,800 --> 00:13:04,400
When you're being mindful, there's confidence, you're confident because you can see

110
00:13:04,400 --> 00:13:10,920
this is this, perfect, the most perfect confidence because you're actually knowing something.

111
00:13:10,920 --> 00:13:16,040
There's no belief involved, there's no logic or argumentation involved.

112
00:13:16,040 --> 00:13:23,080
This is this, when seeing is seeing, you can't argue with that, so you have confidence.

113
00:13:23,080 --> 00:13:28,960
And also that you see the way seeing works and hearing works and how the mind interacts

114
00:13:28,960 --> 00:13:36,040
with the body and there's perfect confidence because you're seeing it, you're knowing it.

115
00:13:36,040 --> 00:13:39,560
Effort comes because just by being mindful, there's right effort already, we've talked

116
00:13:39,560 --> 00:13:40,560
about that.

117
00:13:40,560 --> 00:13:46,560
And then mindfulness, concentration, while you're focused, when you say to yourself,

118
00:13:46,560 --> 00:13:53,600
rising, falling or seeing or hearing the mantra, the repetition and the reminding of yourself

119
00:13:53,600 --> 00:13:56,800
keeps you focused.

120
00:13:56,800 --> 00:14:01,040
And five wisdom, wisdom is the understanding that comes from being mindful because when

121
00:14:01,040 --> 00:14:06,200
you look, you see, when you see, you know, if you want to know, you have to look, if

122
00:14:06,200 --> 00:14:12,080
you have to see, if you want to see, you have to look, look, see, you know, this is the

123
00:14:12,080 --> 00:14:20,560
progression, the order of progression, but you can't look and not see, and you can't

124
00:14:20,560 --> 00:14:28,520
see and not know, it's sufficient, sufficient to see and sufficient to look, so you don't

125
00:14:28,520 --> 00:14:29,520
have to go looking.

126
00:14:29,520 --> 00:14:36,520
And is this true? Is that too? Where is impermanence? Where is suffering? If you look,

127
00:14:36,520 --> 00:14:45,360
you'll see it. So these are the ingredients. We all have these faculties, Bala, refers

128
00:14:45,360 --> 00:14:50,240
to the power. So the Buddha separated these out. When we talked about Indra, he would

129
00:14:50,240 --> 00:14:55,600
talk about the people's different levels of these things and how they can even become

130
00:14:55,600 --> 00:15:01,960
imbalanced, but Bala means the power. These are when they are well balanced. So he would

131
00:15:01,960 --> 00:15:09,040
talk about these when he talked about the qualities of a Bala, a lighten being or a powerful

132
00:15:09,040 --> 00:15:16,400
meditator. They have these five powers, based on the faculties. That's the same five.

133
00:15:16,400 --> 00:15:27,440
So we had another 10, let me get 22. The next set is called the Bodhjanga. Bodhjanga.

134
00:15:27,440 --> 00:15:36,440
So this is another, another use of the word Bodhi. Bodhjanga. Bodhjanga. Bodhjamini.

135
00:15:36,440 --> 00:15:42,220
It's the same as the word Bodhjaya, it means to wake up or to become enlightened or to

136
00:15:42,220 --> 00:15:53,640
know something, to come to know. So anger means just the factors or anger actually means

137
00:15:53,640 --> 00:16:02,640
like a finger. Well, no anger just means factor. Remember. So the Bodhjanga is the factors

138
00:16:02,640 --> 00:16:08,360
of enlightenment. A smaller list than the 37, but another useful one as well. As you

139
00:16:08,360 --> 00:16:14,200
can see, all of these are our ways of talking about the Bodhjaya. But they all come together

140
00:16:14,200 --> 00:16:26,320
as one. So the seven Bodhjanga are again sati and then we have Viriya, the sati and

141
00:16:26,320 --> 00:16:39,240
Dhamavicaya, Viriya, Viti, Bhasadhi, Samadhi and Viriya. So sati, we start with sati as mindfulness.

142
00:16:39,240 --> 00:16:44,000
The factors of enlightenment start with mindfulness. And then you have Dhamavicaya, Viriya,

143
00:16:44,000 --> 00:16:50,520
Viriya and Viti. As three are the effort based ones. Dhamavicaya means the investigation

144
00:16:50,520 --> 00:16:58,240
or the learning or the seeing that comes from being mindful. Viriya is the effort, the

145
00:16:58,240 --> 00:17:07,720
energetic nature of being mindful. And Viti is the sort of excitement or the energy, the

146
00:17:07,720 --> 00:17:19,200
static charge that comes from the rapture or the groove getting into a groove sort of

147
00:17:19,200 --> 00:17:29,040
is Viti. Building up the power. And then you have Bhasadhi, which means tranquility,

148
00:17:29,040 --> 00:17:39,760
samadhi, which is concentration again in Bhikkhu, which is equanimity. So the second, third,

149
00:17:39,760 --> 00:17:44,640
and fourth, these are effort based, the fifth, sixth, and seventh. These are tranquility

150
00:17:44,640 --> 00:17:50,720
based. And the effort and tranquility have to balance. If you're, this is where the one

151
00:17:50,720 --> 00:17:56,600
place where the Buddha actually said you could adjust and you should cultivate the former

152
00:17:56,600 --> 00:18:08,880
three. If you're feeling drowsy or lazy and you should cultivate the last three. If you're

153
00:18:08,880 --> 00:18:15,200
feeling distracted or restless. But then he says, you know, mindfulness. Mindfulness is

154
00:18:15,200 --> 00:18:20,200
what is always useful. Satin Chukhuang Bhikkhuang Bhikkhuang Bhikkhuang Bhikkhuang Bhikkhuang

155
00:18:20,200 --> 00:18:26,600
mindfulness is the one you use. And when you use it, the other rest do balance them, just

156
00:18:26,600 --> 00:18:33,640
by being mindful. You can, you can adjust them, but it does leave room for that. He says,

157
00:18:33,640 --> 00:18:45,600
so focus on, on investigation and effort and rapture, if you're feeling drowsy. And focus

158
00:18:45,600 --> 00:18:54,920
on the tranquility, concentration and equanimity, if you're feeling restless. But if you

159
00:18:54,920 --> 00:18:59,920
don't really need to go that far, many times the Buddha would just talk about mindfulness.

160
00:18:59,920 --> 00:19:05,960
It's good to know and it's good to see when you're, again, using we monks and to see

161
00:19:05,960 --> 00:19:17,440
which one is in excess or in deficiency. So you can apply mindfulness properly. Ultimately,

162
00:19:17,440 --> 00:19:24,320
what we're looking for is this last one, a big, a big in terms of mindfulness or practice

163
00:19:24,320 --> 00:19:29,560
of mindfulness. It means seeing things objective without any judgment, seeing everything

164
00:19:29,560 --> 00:19:36,960
that arises simply as it is. When you stop reacting to your experiences, that's the highest

165
00:19:36,960 --> 00:19:44,080
state of insight meditation. It's this, the peak, the pinnacle, is what we have to get

166
00:19:44,080 --> 00:19:51,720
to in order to become enlightened. Anyway, these seven are very much present in our practice.

167
00:19:51,720 --> 00:19:57,720
So there we go. We've added another seven quality. The final aid is the, the eight full

168
00:19:57,720 --> 00:20:05,120
noble path. So this one is, should be a very familiar to everybody. How this appears in

169
00:20:05,120 --> 00:20:09,680
your practice, it can often confound people. There's eight. It's the most so far. It's

170
00:20:09,680 --> 00:20:15,760
a lot. And so you think, well, you have to study a lot to learn about all eight of these.

171
00:20:15,760 --> 00:20:21,680
But again, it's not really like that in a practical sense. They really do come together.

172
00:20:21,680 --> 00:20:27,280
The easiest way to understand how they come together is to, to condense them down to

173
00:20:27,280 --> 00:20:33,320
the three training. The eight full noble path in its base is really only three things.

174
00:20:33,320 --> 00:20:43,880
We have morality or behavior. We have concentration and wisdom. Morality, concentration,

175
00:20:43,880 --> 00:20:49,960
wisdom. These are the basis of the full noble path. And you boil it down, that's

176
00:20:49,960 --> 00:20:56,320
what the eight full noble path talks about. And it's useful because in a practical sense,

177
00:20:56,320 --> 00:21:01,120
you don't see, oh, here's right, right, right view. Here's right thought and so on. It's

178
00:21:01,120 --> 00:21:06,680
not quite so clear. In practice, they sort of do all come together. It's very difficult

179
00:21:06,680 --> 00:21:11,000
to pick them out. Certainly, you don't see right action and right livelihood and right

180
00:21:11,000 --> 00:21:16,320
effort and right speech and right mindfulness, right concentration, picking them apart

181
00:21:16,320 --> 00:21:25,840
is possible, but not so useful. So morality is the, the body and speech involved with

182
00:21:25,840 --> 00:21:31,320
meditation, which it's right because it's not there. You know, you're not speaking,

183
00:21:31,320 --> 00:21:42,320
you're not acting. And moreover, your mind is not misbehaving. It's not creating unethical,

184
00:21:42,320 --> 00:21:49,080
acts of body or speech. And you're by unethical. We're really in an ultimate sense,

185
00:21:49,080 --> 00:21:58,080
meaning based on defilement. So when you walk quickly or when you wander around confused,

186
00:21:58,080 --> 00:22:05,600
those are all expressions of an unethical mind. They're considered to be unethical actions

187
00:22:05,600 --> 00:22:13,480
when you make a fist. That's unethical. When you like in a anger, when you tense up out

188
00:22:13,480 --> 00:22:19,400
of fear, that's unethical. These are all unethical, meaning they now unethical meaning they

189
00:22:19,400 --> 00:22:26,680
cause you suffering. That's all they mean. And so as you practice mindfulness, you become

190
00:22:26,680 --> 00:22:36,600
ethical. There's no more eating on mindfully. There's no more speaking on mindfully, drinking,

191
00:22:36,600 --> 00:22:42,480
walking, standing, sitting and lying are all done mindfully. So all of our action. And

192
00:22:42,480 --> 00:22:52,360
when we speak, we speak mindfully. All of this is from the practice becoming ethical.

193
00:22:52,360 --> 00:22:56,440
Concentration, of course. I'll actually be fairly obvious if you've already talked about it.

194
00:22:56,440 --> 00:23:03,320
You become concentrated. But you become concentrated on reality, which allows for wisdom to

195
00:23:03,320 --> 00:23:09,600
arise. The third one. Wisdom being right view and right side. So our thoughts are right

196
00:23:09,600 --> 00:23:21,360
and our view is right. We have right view because we see things as they are. So the idea

197
00:23:21,360 --> 00:23:28,200
here is to give an overview. And I was to see that wow, there's a lot involved. And

198
00:23:28,200 --> 00:23:33,440
get some, I think some encouragement as to the greatness of the Buddha's teaching. It really

199
00:23:33,440 --> 00:23:39,720
did cover a lot. I'm going to really did go in depth. He didn't just give us be mindful

200
00:23:39,720 --> 00:23:48,680
and we have to go on faith that that somehow the right path. No, he was quite detailed. Look

201
00:23:48,680 --> 00:23:54,680
when you're mindful, there's at least 37 things. There's a lot of different things that

202
00:23:54,680 --> 00:24:02,680
are involved here. A lot of good that comes from it. And it's quite complete in terms

203
00:24:02,680 --> 00:24:10,800
of talking about the various aspects of the mind, various aspects of experience. Also

204
00:24:10,800 --> 00:24:16,680
it can give us some, it's good to go in detail like this because it maybe reminds us of some

205
00:24:16,680 --> 00:24:27,720
qualities of mind that we were ignoring or dismissing. They were overlooking. So it helps us

206
00:24:27,720 --> 00:24:33,800
look over our practice and to see maybe what we're lacking and to adjust our practice

207
00:24:33,800 --> 00:24:38,560
accordingly by being mindful of things, simply by being mindful of things that we weren't

208
00:24:38,560 --> 00:24:50,040
being so mindful of. So there you go. A short exposition on the 37 states that partake

209
00:24:50,040 --> 00:25:11,160
of enlightenment. That's the demo for tonight. Thank you all for tuning in. Is there any questions

210
00:25:11,160 --> 00:25:28,040
I'll take them.

211
00:26:41,160 --> 00:27:01,280
No questions. We had one on the website that thinks a fairly important one. That's a difficult

212
00:27:01,280 --> 00:27:15,480
one to answer, but even important to address. Let's go see. Okay, so it's a long-ish

213
00:27:15,480 --> 00:27:26,680
question. So I'm genuinely confused as to why I've been stuck in my practice for so many

214
00:27:26,680 --> 00:27:31,800
months to know avail for this reason I continue to seek information, though I know it is

215
00:27:31,800 --> 00:27:37,280
of no benefit. The main areas I believe afflict me include expectations, wanting, clinging

216
00:27:37,280 --> 00:27:43,000
doubt, and a lack of concentration. I have addressed the clinging by choosing to meditate

217
00:27:43,000 --> 00:27:47,000
20 minutes a day, but I still have much doubt in a severe lack of concentration, most

218
00:27:47,000 --> 00:27:51,680
noting the objects. And so can't resist searching for information to see what I'm doing

219
00:27:51,680 --> 00:28:00,720
wrong and how to get my practice to work. What measures would you recommend? Well, 20 minutes

220
00:28:00,720 --> 00:28:09,920
a day, first of all, is a little. Somehow you're going to try to increase that if you really

221
00:28:09,920 --> 00:28:19,520
want to deal with your cravings and clinging and so on. And your doubts. But a little more

222
00:28:19,520 --> 00:28:32,840
important is to adjust your practice based on these areas of that afflict you and learn

223
00:28:32,840 --> 00:28:38,680
to actually meditate on your problems, which is difficult. It's much easier to ignore them

224
00:28:38,680 --> 00:28:48,480
and meditate on ordinary things, but that ends up being actually much more difficult. Because

225
00:28:48,480 --> 00:28:54,880
as you do that, you're ignoring reality and you're not really mindful. And so they build

226
00:28:54,880 --> 00:29:03,840
up and build up and they can create a real stumbling block in your practice. So the advice

227
00:29:03,840 --> 00:29:08,200
I give would be too full. There's one you have to increase your practice if you really

228
00:29:08,200 --> 00:29:16,400
want. And going along with that is the understanding that these are not simple problems.

229
00:29:16,400 --> 00:29:20,680
Meditation isn't a switch. It's not just pulling a switch and suddenly proof my problems

230
00:29:20,680 --> 00:29:27,520
are gone. These are habits and they're very deeply ingrained. So meditation is not magic.

231
00:29:27,520 --> 00:29:37,000
It's just about cultivating proper habits and overcoming or discerning that your bad habits

232
00:29:37,000 --> 00:29:44,360
are problematic and giving them up. So it's something that you really have to work at extensively

233
00:29:44,360 --> 00:29:53,000
and intensively. Those are two good words. Extensive and intensive. So the best thing if

234
00:29:53,000 --> 00:30:00,760
you really want the advice is to find a place to do a meditation course. Go through the

235
00:30:00,760 --> 00:30:10,360
intensive cultivation of mindfulness. The second aspect is the second answer to your question

236
00:30:10,360 --> 00:30:15,760
is to adjust your practice. Have a little bit of me monks at to see whether you're actually

237
00:30:15,760 --> 00:30:23,120
being mindful of the experiences that you have or are you avoiding them and trying to

238
00:30:23,120 --> 00:30:30,480
just be mindful of the easier aspects. Like the objective parts. Like are you just saying

239
00:30:30,480 --> 00:30:35,200
seeing seeing but there's already also liking of the seeing and you're not noting the

240
00:30:35,200 --> 00:30:40,080
liking for a simple example or your mindful of thoughts but you're not mindful of the

241
00:30:40,080 --> 00:30:47,760
fact that you're doubting or confused or so. Worrying, afraid, liking, it's liking.

242
00:30:47,760 --> 00:31:06,080
So I hope that helps. Someone's asking to make some form of live stream available on this

243
00:31:06,080 --> 00:31:14,880
side or YouTube. So we have live stream audio if I remember to turn it on. It's on right

244
00:31:14,880 --> 00:31:22,880
now. So you're welcome to listen to the live stream. Though in order to get this you probably

245
00:31:22,880 --> 00:31:28,000
have to hear this being spoken on the live stream which is unfortunate. So hopefully someone

246
00:31:28,000 --> 00:31:34,600
on the site can maybe I'll try to put in the description of the YouTube video because

247
00:31:34,600 --> 00:31:39,400
the YouTube video doesn't have these questions and answers in it. Thought that would

248
00:31:39,400 --> 00:31:47,720
be a little too much. Okay, a question about solitude is an extent to which isolation can

249
00:31:47,720 --> 00:31:57,520
be detrimental to ones how else? Yeah, if you're isolated from food or air or water it can

250
00:31:57,520 --> 00:32:03,720
be detrimental to your health. That's I think one extent but I don't think that's what

251
00:32:03,720 --> 00:32:13,520
you're asking. If what you're asking is detrimental to your being isolated from other people

252
00:32:13,520 --> 00:32:21,400
detrimental to your mental health then that's the answer is mostly no. And there's two

253
00:32:21,400 --> 00:32:25,360
ways I think it could be detrimental. The first is if you isolate yourself from good people

254
00:32:25,360 --> 00:32:30,720
in the sense that you don't ever go to see good people or find a good teacher or spend

255
00:32:30,720 --> 00:32:40,240
any time with people who can help you improve or that's detrimental to your mental health.

256
00:32:40,240 --> 00:32:58,000
The other way it could be detrimental is because you react violently or inappropriately

257
00:32:58,000 --> 00:33:08,960
at least to the results of staying alone. Most of us are attached to social interactions

258
00:33:08,960 --> 00:33:26,720
and not only that but social interactions help to keep us normal, help to keep us from having

259
00:33:26,720 --> 00:33:34,680
to deal with ourselves, right? So there's nothing wrong with solitude but it's dangerous,

260
00:33:34,680 --> 00:33:42,840
it's powerful. Solitude forces you to look at yourself and there's no escape or there's

261
00:33:42,840 --> 00:33:54,880
not the escape of externalizing your problems when you are relating to other people or

262
00:33:54,880 --> 00:34:03,480
engaging with other people. It allows you to look outward rather than look inward which

263
00:34:03,480 --> 00:34:10,880
avoids the problem or keeps you from having to face the problems. So solitude is something

264
00:34:10,880 --> 00:34:27,680
the Buddha recommended and quite strongly recommended but you have to have the tool. It's

265
00:34:27,680 --> 00:34:33,640
such a powerful thing that if you're not well equipped it can be to your detriment. So

266
00:34:33,640 --> 00:34:39,000
solitude isn't. But the craziness in our minds certainly is and when you have to taste

267
00:34:39,000 --> 00:34:45,200
that and you have to deal with it, you can go crazy being alone. A lot of monks that I

268
00:34:45,200 --> 00:34:52,560
met in the forest end up being a little bit wacky because they're not really well equipped

269
00:34:52,560 --> 00:34:58,200
to be alone in the forest. It's not an easy thing. The Buddha said how difficult it

270
00:34:58,200 --> 00:35:07,560
is to be alone, how difficult it is to be at peace in the forest. We all think about

271
00:35:07,560 --> 00:35:11,440
that silly and I go off in the forest and I'm at peace but try living alone in the

272
00:35:11,440 --> 00:35:24,960
forest for years without good training. It's not easy. So too full. I mean the one

273
00:35:24,960 --> 00:35:32,200
hand is, it can be dangerous because of our minds or not really equipped to deal with

274
00:35:32,200 --> 00:35:37,680
our own minds but the other one is in the first answer that there are good things that

275
00:35:37,680 --> 00:35:47,080
come from social interactions. We benefit from interaction with good people and a part of

276
00:35:47,080 --> 00:35:54,400
our religious practice has to be social I think at the very least the social interactions

277
00:35:54,400 --> 00:35:59,480
with the teacher but more commonly the social interactions with fellow meditators and fellow

278
00:35:59,480 --> 00:36:08,400
Buddhists, fellow practitioners. It helps keep us on the path and encourage us and we

279
00:36:08,400 --> 00:36:31,160
can discuss and support each other's practice.

280
00:36:31,160 --> 00:36:58,640
You're most welcome. That's it and we'll say good night to everyone. See you all next

281
00:36:58,640 --> 00:37:02,560
time. Thank you for coming.

