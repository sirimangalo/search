1
00:00:00,000 --> 00:00:10,160
It's a goal, another goal, not even having a measure of rice.

2
00:00:10,160 --> 00:00:20,840
Suppose they come into debt, because they have no money, they have to borrow money just

3
00:00:20,840 --> 00:00:31,160
to get food, maybe they have to borrow money to get a place to stay, borrow money

4
00:00:31,160 --> 00:00:36,320
just to get a bowl in which to eat out of.

5
00:00:36,320 --> 00:00:38,480
Isn't that great suffering?

6
00:00:38,480 --> 00:00:41,320
Debt, no?

7
00:00:41,320 --> 00:00:48,840
Many people, even if they're not incredibly poor, can understand the fear and the anguish

8
00:00:48,840 --> 00:00:50,960
that comes from being in debt.

9
00:00:50,960 --> 00:01:04,440
The feeling of helplessness, hopelessness, the feeling of slavery, of being in debt.

10
00:01:04,440 --> 00:01:24,440
So, listen to this also, suffering, and say, as indeed, and so, for one who has a debt,

11
00:01:24,440 --> 00:01:39,440
they have to pay interest on their debt, isn't that suffering, right, of course, because

12
00:01:39,440 --> 00:01:43,440
once you're in debt, it's not just about paying it back, sometimes all you can pay back

13
00:01:43,440 --> 00:01:44,440
is the interest.

14
00:01:44,440 --> 00:01:47,440
So, yes, indeed, that's greater suffering.

15
00:01:47,440 --> 00:01:51,720
He's building up to something here, it's actually not as simple as it sounds, he's got

16
00:01:51,720 --> 00:01:56,720
something, he's actually using this as a comparison to something, so there's more to this

17
00:01:56,720 --> 00:01:57,720
suit than this.

18
00:01:57,720 --> 00:02:05,520
Although even this, it's a good example of how sensuality can be used to suffering, because

19
00:02:05,520 --> 00:02:13,720
we often go into debt, and we suffer from debt due to our desires, wanting a big car,

20
00:02:13,720 --> 00:02:14,720
a big house.

21
00:02:14,720 --> 00:02:19,920
I have a couple of students in New Toronto, and they were telling, talking to me about this,

22
00:02:19,920 --> 00:02:24,920
they finally went to visit their house, and they might actually be listening, probably,

23
00:02:24,920 --> 00:02:25,920
they are listening.

24
00:02:25,920 --> 00:02:32,420
I mean, I'm explaining to me how they're going to sell this house, and before they

25
00:02:32,420 --> 00:02:41,220
bought this house, when they hadn't meditated yet, and many other people, they were caught

26
00:02:41,220 --> 00:02:46,920
up in the idea of having a really nice house, and indeed, their house is exceptionally nice.

27
00:02:46,920 --> 00:02:54,920
They don't have any children, so just the two of them in this big, nice, beautiful house,

28
00:02:54,920 --> 00:02:57,920
and then when they started meditating, they realized, you know, what is this?

29
00:02:57,920 --> 00:03:04,920
We've got, we're deeply in debt, and so they've decided to sell the house, and they're

30
00:03:04,920 --> 00:03:09,920
going to buy a house that is more reasonable, that they can still have to take care of their

31
00:03:09,920 --> 00:03:16,920
house, and have their parents move in with them, which is, of course, nice.

32
00:03:16,920 --> 00:03:23,920
But for those who are caught up in it, you see so many people buying these big, luxurious

33
00:03:23,920 --> 00:03:28,920
houses, living the rest of their lives in slavery.

34
00:03:28,920 --> 00:03:35,920
So interest, and for one who has interest, what else happens?

35
00:03:35,920 --> 00:03:46,920
They don't give the interest on time, it's really English, I'm going to mess this up.

36
00:03:46,920 --> 00:03:50,920
I was promised to pay interest, cannot pay it when it falls due.

37
00:03:50,920 --> 00:03:52,920
They reprove him.

38
00:03:52,920 --> 00:03:54,920
Isn't that suffering?

39
00:03:54,920 --> 00:03:55,920
Oh, yes.

40
00:03:55,920 --> 00:04:05,920
You've ever, I've sconded on a debt and had to deal with debt collection agencies.

41
00:04:05,920 --> 00:04:07,920
They are the worst.

42
00:04:07,920 --> 00:04:09,920
This once happened to my family.

43
00:04:09,920 --> 00:04:15,920
Some of them in our family had a long-standing debt student debt from many, many, many

44
00:04:15,920 --> 00:04:21,920
years before, and there was, they were still trying to catch up with her, with this person.

45
00:04:21,920 --> 00:04:31,920
They would call us and say nasty things to us, just anything they can do to make us upset.

46
00:04:31,920 --> 00:04:35,920
But you also have to deal with this as well.

47
00:04:35,920 --> 00:04:38,920
Anyway, isn't that suffering?

48
00:04:38,920 --> 00:04:39,920
Yes, it's suffering.

49
00:04:39,920 --> 00:04:45,920
And when they're not able to pay and they're chastised in this way, if they still don't

50
00:04:45,920 --> 00:04:51,920
pay, they'd persecute them, right? They'd take them to court.

51
00:04:51,920 --> 00:04:53,920
They find a way to make them pay.

52
00:04:53,920 --> 00:04:58,920
Maybe they repossessed their possessions, they do whatever they can, get a court in junction,

53
00:04:58,920 --> 00:05:03,920
they get them kicked out of their helm or whatever.

54
00:05:03,920 --> 00:05:07,920
And after being prosecuted, they even put them in prison.

55
00:05:07,920 --> 00:05:17,920
So this is the fate of a debtor, one who has debt.

56
00:05:17,920 --> 00:05:23,920
In and of itself, it's a somewhat stinging indictment of sensuality, right?

57
00:05:23,920 --> 00:05:26,920
Because this happens to many people.

58
00:05:26,920 --> 00:05:32,920
It happens to people even not living extravagant lifestyle, right?

59
00:05:32,920 --> 00:05:36,920
But it causes suffering when we have attachment to these things.

60
00:05:36,920 --> 00:05:40,920
When we've built up a stable life, and then this happens to us,

61
00:05:40,920 --> 00:05:42,920
I mean, it's great suffering in losing all that you have.

62
00:05:42,920 --> 00:05:46,920
Not having a home is certainly stressful, especially in the cold climate.

63
00:05:46,920 --> 00:05:47,920
What are you going to do?

64
00:05:47,920 --> 00:05:50,920
Having to go to homeless shelters or worse live on the street.

65
00:05:50,920 --> 00:05:52,920
It's great suffering.

66
00:05:52,920 --> 00:05:56,920
But it's much worse suffering for one who's attached to those things, right?

67
00:05:56,920 --> 00:06:01,920
It's devil suffering because those things had meaning to us, had value to us,

68
00:06:01,920 --> 00:06:10,920
because we clung to them, much more than just the functional value that they held.

69
00:06:10,920 --> 00:06:14,920
But it's more to the suit than actually the more wonderful thing about the suit is that

70
00:06:14,920 --> 00:06:22,920
he's only using this as a metaphor, an allegory.

71
00:06:22,920 --> 00:06:26,920
So he says, but it says,

72
00:06:26,920 --> 00:06:31,920
I suppose you have a person who doesn't have faith.

73
00:06:31,920 --> 00:06:34,920
He's like, this is what it's calling.

74
00:06:34,920 --> 00:06:40,920
It will be called even so monks.

75
00:06:40,920 --> 00:06:46,920
It will be.

76
00:06:46,920 --> 00:06:56,920
Even when they call it, even when they call it, they say that she has no sadha, no confidence.

77
00:06:56,920 --> 00:07:04,920
So in regards to the heteroism, person who doesn't think to keep moral precepts,

78
00:07:04,920 --> 00:07:08,920
doesn't have any interest in ethics, doesn't have any interest in meditation,

79
00:07:08,920 --> 00:07:12,920
has no thought to understand, to cultivate understanding.

80
00:07:12,920 --> 00:07:24,220
Here, in the tikus, the ace of dummies has no shame in regards to conscientiousness in regards to wholesome dummies.

81
00:07:24,220 --> 00:07:35,420
O, the pang that tikus, the ace of dummies, has no fear in regards to kus, no fear exactly, but something like that.

82
00:07:35,420 --> 00:07:43,420
In regards to wholesome dummies, the median that tikus, the ace of dummies, puts out no effort in regards to wholesome dummies.

83
00:07:43,420 --> 00:07:46,420
It doesn't work to cultivate wholesomeness.

84
00:07:46,420 --> 00:07:52,420
Banyan, tikus, the ace of dummies, has no wisdom in regards to wholesome dummies.

85
00:07:52,420 --> 00:07:56,420
Basically, it's devoid of any wholesome qualities.

86
00:07:56,420 --> 00:08:11,420
This is called umangks in the windy eye and the discipline of the noble ones.

87
00:08:11,420 --> 00:08:13,420
Alias, so windy.

88
00:08:13,420 --> 00:08:17,420
Dali, though, this is called a Dalit.

89
00:08:17,420 --> 00:08:22,420
We're in the Indian very familiar with the Dalit.

90
00:08:22,420 --> 00:08:26,420
This is called a Dalit in the Buddha's sassala.

91
00:08:26,420 --> 00:08:35,420
Asakou, one who has nothing, anahikou, not even a measure of rice, doesn't have the smallest bit of anything.

92
00:08:35,420 --> 00:08:36,420
Worthless.

93
00:08:36,420 --> 00:08:44,420
There's destitute, really, to point.

94
00:08:44,420 --> 00:08:54,420
Remember, this is the first one, it's a poor person.

95
00:08:54,420 --> 00:09:12,420
That poor destitute person in the dhamma, without any confidence, not having any shame or dread or energy with them.

96
00:09:12,420 --> 00:09:16,420
Such a person will kai in a duchary tung jharati.

97
00:09:16,420 --> 00:09:19,420
Will do evil deeds with the body.

98
00:09:19,420 --> 00:09:22,420
Wa jai a duchary tung jharati.

99
00:09:22,420 --> 00:09:25,420
Will do evil deeds with the speech.

100
00:09:25,420 --> 00:09:27,420
Manasa duchary tung jharati.

101
00:09:27,420 --> 00:09:29,420
Will do evil deeds with mind.

102
00:09:29,420 --> 00:09:36,420
Their mind will have evil thoughts.

103
00:09:36,420 --> 00:09:43,420
It's impoverished as someone who doesn't have wholesome dhammas right off the bat.

104
00:09:43,420 --> 00:09:50,420
But just like a person who's poor, the problem of being poor is you can't afford the things that you want.

105
00:09:50,420 --> 00:09:52,420
You can't afford good things.

106
00:09:52,420 --> 00:09:55,420
You can't afford things that bring happiness.

107
00:09:55,420 --> 00:10:03,420
Even much more so, a person devoid of wholesome qualities has no power to cultivate wholesome mind state,

108
00:10:03,420 --> 00:10:07,420
or to cultivate mind states that need to happiness.

109
00:10:07,420 --> 00:10:09,420
Happiness is not something that just falls on your lap.

110
00:10:09,420 --> 00:10:17,420
It doesn't even come just because you have a big house and a big car and beautiful clothes, delicious food.

111
00:10:17,420 --> 00:10:23,420
It doesn't come from any of that.

112
00:10:23,420 --> 00:10:26,420
Did you guys get chocolate today?

113
00:10:26,420 --> 00:10:29,420
Did you see the chocolate?

114
00:10:29,420 --> 00:10:35,420
It doesn't even come from chocolate.

115
00:10:35,420 --> 00:10:38,420
Huh?

116
00:10:38,420 --> 00:10:40,420
No, it's some chocolate.

117
00:10:40,420 --> 00:10:42,420
No, I said happiness doesn't come from chocolate.

118
00:10:42,420 --> 00:10:48,420
It was chocolate, but even chocolate doesn't bring happiness.

119
00:10:48,420 --> 00:10:51,420
It brings pleasure.

120
00:10:51,420 --> 00:10:54,420
Pleasure brings some happiness.

121
00:10:54,420 --> 00:10:59,420
But that's adulterated happiness.

122
00:10:59,420 --> 00:11:03,420
It's happiness that's associated with desire.

123
00:11:03,420 --> 00:11:05,420
It also makes you kind of edgy.

124
00:11:05,420 --> 00:11:11,420
You have to be careful not to eat too much chocolate as a meditator.

125
00:11:11,420 --> 00:11:13,420
Wherever it is.

126
00:11:13,420 --> 00:11:19,420
So a person as a result, they get in debt.

127
00:11:19,420 --> 00:11:26,420
So the debt in the Aryas had been in the Arya Vinaya as they do evil deeds.

128
00:11:26,420 --> 00:11:30,420
Not only that they can't do good deeds, but they also do evil deeds.

129
00:11:30,420 --> 00:11:33,420
Because they're devoid of any goodness.

130
00:11:33,420 --> 00:11:37,420
They fall prey to their own anger, their own greed, their own delusion.

131
00:11:37,420 --> 00:11:41,420
They hurt others, they hurt themselves, they hate others, they hate themselves,

132
00:11:41,420 --> 00:11:48,420
they desire things, they become addicted to things, they have conceited arrogance.

133
00:11:48,420 --> 00:11:51,420
And this is called debt.

134
00:11:51,420 --> 00:11:53,420
Karma is a debt.

135
00:11:53,420 --> 00:11:55,420
You ever heard of the karmic debt?

136
00:11:55,420 --> 00:12:00,420
A person who is in debt in Buddhism is a person who is cultivated unwholesomeness

137
00:12:00,420 --> 00:12:03,420
and just waiting to have to pay it back.

138
00:12:03,420 --> 00:12:06,420
Often they'll get into the cycle where they have to keep doing evil deeds

139
00:12:06,420 --> 00:12:12,420
just to push back the results.

140
00:12:12,420 --> 00:12:19,420
It's debelling down on their debt by running away from consequences,

141
00:12:19,420 --> 00:12:25,420
finding ways to dig themselves deeper and deeper.

142
00:12:25,420 --> 00:12:27,420
That's hard diction work.

143
00:12:27,420 --> 00:12:30,420
So you get addicted to something and then every time you indulge in the addiction

144
00:12:30,420 --> 00:12:36,420
you just dig yourself deeper and deeper, running away from the consequences.

145
00:12:36,420 --> 00:12:40,420
Of course the same goes with when you heard others and then in order to stop them

146
00:12:40,420 --> 00:12:47,420
burning you back you keep hurting them and keep acting in the nasty ways to keep them away.

147
00:12:47,420 --> 00:12:49,420
So that's debt.

148
00:12:49,420 --> 00:12:55,420
Debt in the Buddha's teaching is that evil karma that we do.

149
00:12:55,420 --> 00:12:59,420
Ida masai, yinada, nasmi, madami.

150
00:12:59,420 --> 00:13:05,420
This is called debt.

151
00:13:05,420 --> 00:13:10,420
I'm doing all those evil deeds.

152
00:13:10,420 --> 00:13:12,420
Let's see.

153
00:13:12,420 --> 00:13:15,420
Papi kangichang, pani dhati.

154
00:13:15,420 --> 00:13:18,420
Ma maang janyu, tea, tea.

155
00:13:18,420 --> 00:13:21,420
Ma maang janyu, tea, sang kapati.

156
00:13:21,420 --> 00:13:25,420
Ma maang janyu, tea wa jang vasa, tea.

157
00:13:25,420 --> 00:13:30,420
Ma maang janyu, kaii, napara kabati.

158
00:13:30,420 --> 00:13:32,420
They hide their evil deeds.

159
00:13:32,420 --> 00:13:34,420
This is what I was talking about.

160
00:13:34,420 --> 00:13:36,420
They hide their evil deeds.

161
00:13:36,420 --> 00:13:39,420
They may this person not.

162
00:13:39,420 --> 00:13:43,420
They wish, may people not know what I've done.

163
00:13:43,420 --> 00:13:50,420
They think they speak words to the effect or know they speak.

164
00:13:50,420 --> 00:13:53,420
The words may they not know me.

165
00:13:53,420 --> 00:13:55,420
They're not know what I've done.

166
00:13:55,420 --> 00:14:00,420
And they hide their karma data.

167
00:14:00,420 --> 00:14:04,420
They exert themselves with body.

168
00:14:04,420 --> 00:14:07,420
They do deeds to make sure that no one finds out what they've done.

169
00:14:07,420 --> 00:14:12,420
They speak words so that people live, for example.

170
00:14:12,420 --> 00:14:13,420
What is this called?

171
00:14:13,420 --> 00:14:15,420
This is the interest.

172
00:14:15,420 --> 00:14:20,420
This is compound interest on your loan.

173
00:14:20,420 --> 00:14:25,420
Making it worse, basically.

174
00:14:25,420 --> 00:14:31,420
The interest that you have to pay.

175
00:14:31,420 --> 00:14:36,420
You have to pay interest when you're not able to pay it back until you have wholesome.

176
00:14:36,420 --> 00:14:41,420
The wholesome qualities necessary to stop being such a terrible person.

177
00:14:41,420 --> 00:14:51,420
You pay the interest of having to hide it.

178
00:14:51,420 --> 00:14:58,420
That's number three, number four, well-behaved monks, fellow monks.

179
00:14:58,420 --> 00:15:00,420
That's not to say monks.

180
00:15:00,420 --> 00:15:01,420
It's not monks.

181
00:15:01,420 --> 00:15:04,420
Dummy nung bais salas, sabrammachari, your fellows in the holy life.

182
00:15:04,420 --> 00:15:06,420
So that's all of you guys.

183
00:15:06,420 --> 00:15:07,420
Fellows.

184
00:15:07,420 --> 00:15:08,420
It's all of us.

185
00:15:08,420 --> 00:15:09,420
It's a community.

186
00:15:09,420 --> 00:15:12,420
Our fellow, holy people.

187
00:15:12,420 --> 00:15:18,420
Fellow spiritual beings.

188
00:15:18,420 --> 00:15:23,420
Hello, religious.

189
00:15:23,420 --> 00:15:25,420
He were mung soom.

190
00:15:25,420 --> 00:15:27,420
They say, what do they say?

191
00:15:27,420 --> 00:15:28,420
They say good things.

192
00:15:28,420 --> 00:15:29,420
No, pay salas.

193
00:15:29,420 --> 00:15:31,420
These are the good people.

194
00:15:31,420 --> 00:15:32,420
The wise ones.

195
00:15:32,420 --> 00:15:33,420
Well, no, sorry.

196
00:15:33,420 --> 00:15:35,420
The well-behaved ones.

197
00:15:35,420 --> 00:15:38,420
I am just so I asma Ihwangari.

198
00:15:38,420 --> 00:15:45,420
Thus has this person done.

199
00:15:45,420 --> 00:15:48,420
Ihwang sabrammachari, they've acted in this way.

200
00:15:48,420 --> 00:15:52,900
Ihwang

201
00:15:52,900 --> 00:15:55,420
They say nasty things about this person.

202
00:15:55,420 --> 00:15:56,420
Not nasty things.

203
00:15:56,420 --> 00:15:57,420
They tell the truth about them.

204
00:15:57,420 --> 00:16:00,420
Everyone knows what they've done and the word spreads

205
00:16:00,420 --> 00:16:07,420
and who wants to be with such a person.

206
00:16:07,420 --> 00:16:09,420
When you go to meditation centers,

207
00:16:09,420 --> 00:16:12,420
you become known as the troublemaker.

208
00:16:12,420 --> 00:16:16,420
Thinking about it in society,

209
00:16:16,420 --> 00:16:21,420
as a mean and nasty person.

210
00:16:21,420 --> 00:16:25,420
People spread bad, bad report about you.

211
00:16:25,420 --> 00:16:28,420
This is when you're,

212
00:16:28,420 --> 00:16:30,420
because here's the word,

213
00:16:30,420 --> 00:16:35,420
a reprove to your reprove.

214
00:16:35,420 --> 00:16:37,420
Just as it's just like people,

215
00:16:37,420 --> 00:16:42,420
the people you've borrowed money from,

216
00:16:42,420 --> 00:16:45,420
hey, when are you going to pay me back?

217
00:16:45,420 --> 00:16:48,420
The person who's done evil deeds gets this.

218
00:16:48,420 --> 00:16:50,420
Hey, when are you going to pay back those evil deeds?

219
00:16:50,420 --> 00:16:55,420
When are you going to stop being such an evil, terrible person?

220
00:16:55,420 --> 00:16:59,420
They don't say that,

221
00:16:59,420 --> 00:17:02,420
but they're scolded for being such an evil person.

222
00:17:02,420 --> 00:17:04,420
No one likes.

223
00:17:04,420 --> 00:17:07,420
This is one of the reasons for just,

224
00:17:07,420 --> 00:17:12,420
for realizing the importance of true meditation.

225
00:17:12,420 --> 00:17:14,420
Just meditation that makes you feel happy with meditation.

226
00:17:14,420 --> 00:17:23,420
It helps you become a better person.

227
00:17:23,420 --> 00:17:30,420
That's number, number four.

228
00:17:30,420 --> 00:17:34,420
Number five,

229
00:17:34,420 --> 00:17:39,420
Ramyagatan wa rukkamulagatan wa sunyagaragatan wa

230
00:17:39,420 --> 00:17:42,420
having gone to the forest,

231
00:17:42,420 --> 00:17:44,420
having gone to the root of a tree,

232
00:17:44,420 --> 00:17:46,420
having gone to an empty place.

233
00:17:46,420 --> 00:17:53,420
Wipati sahara sahagata, papa kakus la vita kasamudajarati.

234
00:17:53,420 --> 00:17:59,420
They are overcome, they are remorseful.

235
00:17:59,420 --> 00:18:06,420
Evil and wholesome thoughts.

236
00:18:06,420 --> 00:18:12,420
Bad and wholesome thoughts accompanied by remorse.

237
00:18:12,420 --> 00:18:16,420
It's interesting, it's interesting use because it shows the Buddha's recognition

238
00:18:16,420 --> 00:18:20,420
that feeling remorse is not wholesome.

239
00:18:20,420 --> 00:18:22,420
It's not good that you feel remorse.

240
00:18:22,420 --> 00:18:26,420
It's naturally a feel remorse for doing bad deeds, but it's not good.

241
00:18:26,420 --> 00:18:30,420
So anyone who sits around feeling guilty about the bad things they've done

242
00:18:30,420 --> 00:18:32,420
isn't helping things.

243
00:18:32,420 --> 00:18:33,420
So I'm doing any benefit.

244
00:18:33,420 --> 00:18:35,420
And he makes this clear by saying there,

245
00:18:35,420 --> 00:18:40,420
a kusula vita ka, papa kadar, evil thoughts.

246
00:18:40,420 --> 00:18:42,420
So this person is so caught up in evil,

247
00:18:42,420 --> 00:18:45,420
and even the guilt that they feel is evil.

248
00:18:45,420 --> 00:18:49,420
They hate themselves from being such an evil person.

249
00:18:49,420 --> 00:18:51,420
They scold themselves, and that's more evil.

250
00:18:51,420 --> 00:18:56,420
It doesn't actually make you a better person.

251
00:18:56,420 --> 00:18:59,420
It might argue it comes from a knowledge of having,

252
00:18:59,420 --> 00:19:03,420
it's better than the opposite, which is feeling good about yourself

253
00:19:03,420 --> 00:19:06,420
because you've done evil things.

254
00:19:06,420 --> 00:19:10,420
But it's not the right response.

255
00:19:10,420 --> 00:19:12,420
It's a natural response.

256
00:19:12,420 --> 00:19:15,420
The better response is to start smartening up and working

257
00:19:15,420 --> 00:19:18,420
and becoming a better person than changing your way,

258
00:19:18,420 --> 00:19:21,420
realizing that what you've done is wrong and working

259
00:19:21,420 --> 00:19:27,420
and working as a result to change.

260
00:19:27,420 --> 00:19:32,420
It might not even be guilt, but it's fear and well,

261
00:19:32,420 --> 00:19:35,420
yeah, guilt, I guess it's remorse.

262
00:19:35,420 --> 00:19:36,420
So what do we call this?

263
00:19:36,420 --> 00:19:43,420
This is called being persecuted, prosecuted.

264
00:19:43,420 --> 00:19:48,420
Like the wording, no, this is like a court.

265
00:19:48,420 --> 00:19:49,420
It's not a person.

266
00:19:49,420 --> 00:19:50,420
It's not people.

267
00:19:50,420 --> 00:19:52,420
This is when you're off in the forest alone.

268
00:19:52,420 --> 00:19:58,420
That's when the law becomes persecute.

269
00:19:58,420 --> 00:20:01,420
The law persecutes you.

270
00:20:01,420 --> 00:20:05,420
You become an outlaw, a karmic outlaw,

271
00:20:05,420 --> 00:20:10,420
living off in the frontier.

272
00:20:10,420 --> 00:20:13,420
But you can't escape karma.

273
00:20:13,420 --> 00:20:15,420
No one can escape karma.

274
00:20:15,420 --> 00:20:20,420
It's my old friend Udi used to say.

275
00:20:20,420 --> 00:20:24,420
I'm going to be prosecuted at number six.

276
00:20:24,420 --> 00:20:25,420
I eventually put in jail.

277
00:20:25,420 --> 00:20:29,420
What does it mean to be put in jail in the Buddha's teaching?

278
00:20:29,420 --> 00:20:33,420
It's this poor number.

279
00:20:33,420 --> 00:20:37,420
We're talking about this person in terms of this destitute

280
00:20:37,420 --> 00:20:42,420
impoverished, penniless person having done evil deeds

281
00:20:42,420 --> 00:20:45,420
with body speech in mind.

282
00:20:45,420 --> 00:21:09,420
That's it.

