1
00:00:00,000 --> 00:00:26,520
Hi, good evening everyone from broadcasting live at the very 14th, which is an important

2
00:00:26,520 --> 00:00:42,240
day I about today is a very important day, now it's not a very important day.

3
00:00:42,240 --> 00:00:52,720
It is a day that has meaning for people, something to do with violence and torture and

4
00:00:52,720 --> 00:01:14,360
martyrdom, historical facts, valentine of Rome was a priest who was martyred and added to

5
00:01:14,360 --> 00:01:38,360
the same calendar of saints, martyrs, so they were persecuted, so today is where we celebrate

6
00:01:38,360 --> 00:01:47,840
a Christian martyr or a bunch of Christian martyrs, as a priest who was martyred, the flower crowns

7
00:01:47,840 --> 00:01:55,280
skull of Saint Valentine is exhibited in the Basilica, there's a reason why I'm telling

8
00:01:55,280 --> 00:02:13,160
this, same valentine, all that is reliably known as he was martyred and braid, so that

9
00:02:13,160 --> 00:02:32,920
little is known of him, something to get some juicy details, because it's a story of hate

10
00:02:32,920 --> 00:02:44,120
and prejudice and bigotry and arrogance and clinging to wrong views and so on, that's

11
00:02:44,120 --> 00:02:53,600
what today is about, how humans, it's a testament to how awful and terrible human beings

12
00:02:53,600 --> 00:03:04,480
can be, that's what today is on, and on behold, today's quote, our valentine's day quote,

13
00:03:04,480 --> 00:03:08,880
has the Buddha saying that if animals can be courteous, differential and polite to each

14
00:03:08,880 --> 00:03:15,120
other, then so should you be, basically, if animals can behave themselves, why the heck

15
00:03:15,120 --> 00:03:27,060
can you guys?

16
00:03:27,060 --> 00:03:33,060
Because how he loves me can pull myfood easy day,

17
00:03:33,060 --> 00:03:37,060
and myhe wants my food right next to me.

18
00:03:37,060 --> 00:03:42,020
Yes, this is so extraordinary.

19
00:03:42,020 --> 00:03:46,060
So beautiful.

20
00:03:46,060 --> 00:03:54,860
It is beautiful.

21
00:03:54,860 --> 00:03:56,980
Good result.

22
00:03:56,980 --> 00:04:03,980
humble, maybe, differential, yeah.

23
00:04:03,980 --> 00:04:15,980
Sambagavutika, having the practice of getting long,

24
00:04:15,980 --> 00:04:19,980
living in mutual courtesy.

25
00:04:19,980 --> 00:04:22,980
Animals can get along.

26
00:04:22,980 --> 00:04:25,980
He's telling the monks, why can't you guys get along?

27
00:04:25,980 --> 00:04:28,980
Why can't you monks get along?

28
00:04:28,980 --> 00:04:33,980
Apparently, in Thailand, these days,

29
00:04:33,980 --> 00:04:36,980
I just started with bizarre news this morning,

30
00:04:36,980 --> 00:04:39,980
all very apropos.

31
00:04:39,980 --> 00:04:44,980
Apparently, the monk who was in line to be the Sangaraja,

32
00:04:44,980 --> 00:04:46,980
the king of the monks,

33
00:04:46,980 --> 00:04:50,980
as we're saying English, the supreme patriarch,

34
00:04:50,980 --> 00:04:54,980
committed suicide.

35
00:04:54,980 --> 00:05:00,980
Yeah.

36
00:05:00,980 --> 00:05:05,980
Now, I think I'm not the only person to wonder how

37
00:05:05,980 --> 00:05:09,980
how it happens that

38
00:05:09,980 --> 00:05:13,980
someone who is high up in the fantastic order

39
00:05:13,980 --> 00:05:16,980
and in line to become the most powerful monk

40
00:05:16,980 --> 00:05:22,980
in Thailand suddenly commit suicide.

41
00:05:22,980 --> 00:05:25,980
And I think a lot of people are...

42
00:05:25,980 --> 00:05:26,980
I mean, I don't actually know this.

43
00:05:26,980 --> 00:05:35,980
I'm looking it up, but...

44
00:05:35,980 --> 00:05:48,980
bizarre.

45
00:05:48,980 --> 00:05:58,980
What I mean to say is a lot of people think he didn't kill himself.

46
00:05:58,980 --> 00:06:03,980
I don't see any quotes of it, maybe it's not true.

47
00:06:03,980 --> 00:06:06,980
Well, that's what I was told this morning by a Thai woman.

48
00:06:06,980 --> 00:06:09,980
I'm visiting to offer a spoon this morning.

49
00:06:09,980 --> 00:06:12,980
I asked her, how is it going in Thailand?

50
00:06:12,980 --> 00:06:16,980
Or she started talking about Thailand?

51
00:06:16,980 --> 00:06:19,980
Yeah, there's a big uproar.

52
00:06:19,980 --> 00:06:23,980
Monks can't get along to say at least.

53
00:06:23,980 --> 00:06:28,980
There's an old story of a monk who was responsible.

54
00:06:28,980 --> 00:06:30,980
I've talked about this before,

55
00:06:30,980 --> 00:06:34,980
who is responsible for bringing insight meditation,

56
00:06:34,980 --> 00:06:37,980
the meditation that we've practiced in to Thailand,

57
00:06:37,980 --> 00:06:39,980
which is now practiced by...

58
00:06:39,980 --> 00:06:41,980
It's now taught in the universities,

59
00:06:41,980 --> 00:06:44,980
in Monastic universities.

60
00:06:44,980 --> 00:06:48,980
The big insight practice in Thailand,

61
00:06:48,980 --> 00:06:52,980
sort of the establishment practice.

62
00:06:52,980 --> 00:06:56,980
He's responsible for bringing insight...

63
00:06:56,980 --> 00:07:00,980
Sorry, I've been done with studies as well.

64
00:07:00,980 --> 00:07:05,980
He was very big on working with other countries,

65
00:07:05,980 --> 00:07:09,980
bringing together monks from all the different countries,

66
00:07:09,980 --> 00:07:13,980
and learning from other countries.

67
00:07:13,980 --> 00:07:17,980
Monks were critical of him saying,

68
00:07:17,980 --> 00:07:27,980
why are we bringing Buddhism from other countries?

69
00:07:27,980 --> 00:07:30,980
Isn't our Buddhism good enough?

70
00:07:30,980 --> 00:07:36,980
Criticizing him like as though he was putting down Thai Buddhism.

71
00:07:36,980 --> 00:07:39,980
He was emboldened, he replied and said,

72
00:07:39,980 --> 00:07:44,980
forget about the Buddhism, forget about the Abhidhamma.

73
00:07:44,980 --> 00:07:47,980
We don't even teach Supta in Thailand.

74
00:07:47,980 --> 00:07:51,980
Not anyone who knows anything about Thai Buddhism can tell you.

75
00:07:51,980 --> 00:07:54,980
You can get to the highest level of Pauli

76
00:07:54,980 --> 00:07:58,980
and Dhamma studies that there is in Thailand today,

77
00:07:58,980 --> 00:08:02,980
and never have read the Deepika.

78
00:08:02,980 --> 00:08:05,980
You can get to the level nine Pauli studies

79
00:08:05,980 --> 00:08:09,980
without ever having translated anything

80
00:08:09,980 --> 00:08:13,980
besides the Dhamma part of verses and commentary

81
00:08:13,980 --> 00:08:15,980
and other commentaries.

82
00:08:15,980 --> 00:08:17,980
So you'll never have translated directly

83
00:08:17,980 --> 00:08:20,980
an actual Deepika.

84
00:08:20,980 --> 00:08:22,980
Which isn't really a problem when you're learning Pauli,

85
00:08:22,980 --> 00:08:26,980
but these people are considered to be the top Buddhist scholars,

86
00:08:26,980 --> 00:08:31,980
and they've never had any real instruction in the Buddhist teaching.

87
00:08:31,980 --> 00:08:33,980
There are three levels of Dhamma study,

88
00:08:33,980 --> 00:08:36,980
and as far as I know, even the third level.

89
00:08:36,980 --> 00:08:39,980
So three years, it's really, really basic.

90
00:08:39,980 --> 00:08:42,980
But that's it. You get to the third level

91
00:08:42,980 --> 00:08:44,980
and you're supposed to be an accomplished teacher.

92
00:08:44,980 --> 00:08:47,980
As far as I know, you don't ever have to have read anything

93
00:08:47,980 --> 00:08:51,980
from the actual Deepika, the Buddhist teachings.

94
00:08:55,980 --> 00:08:58,980
I don't remember where I was going with this.

95
00:08:58,980 --> 00:09:04,980
Totally lost my train of thought.

96
00:09:04,980 --> 00:09:06,980
Right, the monk.

97
00:09:06,980 --> 00:09:10,980
Anyway, the long story short, he was eventually put in.

98
00:09:10,980 --> 00:09:12,980
He was eventually accused.

99
00:09:12,980 --> 00:09:14,980
It was really terrible.

100
00:09:14,980 --> 00:09:19,980
He was the next in line to become the Sangharaja.

101
00:09:19,980 --> 00:09:24,980
And suddenly, he's being accused by some anonymous woman

102
00:09:24,980 --> 00:09:28,980
who was interviewed by the police

103
00:09:28,980 --> 00:09:30,980
and by the Sangharaja council,

104
00:09:30,980 --> 00:09:33,980
and that's the council, who says he raped her.

105
00:09:33,980 --> 00:09:35,980
And you believe it.

106
00:09:35,980 --> 00:09:39,980
Suddenly, to have these detractors,

107
00:09:39,980 --> 00:09:42,980
this woman giving a sign statement

108
00:09:42,980 --> 00:09:44,980
that he, in fact,

109
00:09:44,980 --> 00:09:47,980
actually, I don't think it was, I was probably signed,

110
00:09:47,980 --> 00:09:49,980
but yeah, anyway, she was interviewed

111
00:09:49,980 --> 00:09:57,980
and they accused him of raping this woman

112
00:09:57,980 --> 00:10:00,980
and said he should disrobe,

113
00:10:00,980 --> 00:10:01,980
and he replied, you know,

114
00:10:01,980 --> 00:10:04,980
and the Sangharaja, the king of the monks at the time,

115
00:10:04,980 --> 00:10:07,980
said he should disrobe, told him he should disrobe.

116
00:10:07,980 --> 00:10:11,980
And he should, and in Thailand, it's not disrobe,

117
00:10:11,980 --> 00:10:14,980
but it's stopped being a monk like quit.

118
00:10:14,980 --> 00:10:17,980
He should quit being a monk.

119
00:10:17,980 --> 00:10:20,980
And he replied by saying, you know, if I had actually done that,

120
00:10:20,980 --> 00:10:22,980
I would no longer be a monk, it was really absurd.

121
00:10:22,980 --> 00:10:26,980
They were really just trying to get rid of him.

122
00:10:26,980 --> 00:10:32,980
And they had a huge smear campaign,

123
00:10:32,980 --> 00:10:35,980
and everyone denouncing this monk,

124
00:10:35,980 --> 00:10:39,980
they really done awesome things for Buddhism.

125
00:10:39,980 --> 00:10:43,980
And finally, somehow, they found this woman

126
00:10:43,980 --> 00:10:48,980
and talked to her, and she came forward

127
00:10:48,980 --> 00:10:53,980
and retracted the entire thing and wrote this letter

128
00:10:53,980 --> 00:10:56,980
that just controlled me to tears when I read it.

129
00:10:56,980 --> 00:11:01,980
Well, shiver, I was shivering, just imagining her state of mind.

130
00:11:01,980 --> 00:11:04,980
She said, I just want to sleep at night.

131
00:11:04,980 --> 00:11:06,980
I can't live with what I did.

132
00:11:06,980 --> 00:11:09,980
I don't want to go to hell.

133
00:11:09,980 --> 00:11:13,980
She was really, I mean, that was just, can you imagine?

134
00:11:13,980 --> 00:11:15,980
She said it was not true.

135
00:11:15,980 --> 00:11:20,980
I don't, I've never been in close contact with that monk.

136
00:11:20,980 --> 00:11:22,980
I just want to be able to sleep at night.

137
00:11:22,980 --> 00:11:24,980
I don't want to go to hell.

138
00:11:24,980 --> 00:11:26,980
So that died down.

139
00:11:26,980 --> 00:11:28,980
And then later on, they tried to pin him as a communist

140
00:11:28,980 --> 00:11:31,980
when it was a communist scare in Thailand.

141
00:11:31,980 --> 00:11:34,980
And eventually, they put him in jail for being a communist

142
00:11:34,980 --> 00:11:36,980
without trial.

143
00:11:36,980 --> 00:11:40,980
And he spent the three years or something,

144
00:11:40,980 --> 00:11:42,980
translate in the wizardy manga into Thai,

145
00:11:42,980 --> 00:11:45,980
which is now the standard tri translation.

146
00:11:45,980 --> 00:11:48,980
People asked him about what it was like being in jail.

147
00:11:48,980 --> 00:11:49,980
He said it was great.

148
00:11:49,980 --> 00:11:52,980
People brought me food every day.

149
00:11:52,980 --> 00:11:54,980
I had guards guarding me 24-7.

150
00:11:54,980 --> 00:11:55,980
Didn't have to worry about anything.

151
00:11:55,980 --> 00:11:58,980
Didn't have to act as a head monk of the monastery.

152
00:11:58,980 --> 00:12:00,980
He was the head of what Mahatad at the time,

153
00:12:00,980 --> 00:12:05,980
one of the big, the big monastery back in the day.

154
00:12:05,980 --> 00:12:09,980
That's where an insect meditation in Thailand became a big thing.

155
00:12:09,980 --> 00:12:13,980
And that'd be done with studies in the first monastic university

156
00:12:13,980 --> 00:12:14,980
in Thailand.

157
00:12:14,980 --> 00:12:16,980
It's all under him.

158
00:12:16,980 --> 00:12:20,980
Then he got out and they actually put him on trial

159
00:12:20,980 --> 00:12:23,980
and he was exonerated of all wrongdoing.

160
00:12:23,980 --> 00:12:25,980
And they had some really hard words.

161
00:12:25,980 --> 00:12:28,980
The judges had some really strong wording for the people

162
00:12:28,980 --> 00:12:29,980
who had accused him.

163
00:12:29,980 --> 00:12:31,980
It was really it.

164
00:12:31,980 --> 00:12:35,980
And then eventually he was reinstated.

165
00:12:35,980 --> 00:12:37,980
So during this time when he was put in jail,

166
00:12:37,980 --> 00:12:39,980
of course he was removed of his rank.

167
00:12:39,980 --> 00:12:41,980
And they actually forcibly disrobed him

168
00:12:41,980 --> 00:12:43,980
and made him wear white robes.

169
00:12:43,980 --> 00:12:46,980
And he came back and started wearing robes again.

170
00:12:46,980 --> 00:12:48,980
And they said, well, look, you're no longer a monk.

171
00:12:48,980 --> 00:12:50,980
And he said, that's not true.

172
00:12:50,980 --> 00:12:52,980
You can't disrobe someone else.

173
00:12:52,980 --> 00:12:56,980
The only way to disrobe is in front of one monk in front of another.

174
00:12:56,980 --> 00:12:58,980
It was really interesting this whole,

175
00:12:58,980 --> 00:13:00,980
there's a book about it.

176
00:13:00,980 --> 00:13:04,980
It's a really fascinating read.

177
00:13:04,980 --> 00:13:08,980
But it's a good example of how people just can't get along.

178
00:13:08,980 --> 00:13:12,980
Human beings can be worse than animals.

179
00:13:12,980 --> 00:13:15,980
It's not that animals are better than humans.

180
00:13:15,980 --> 00:13:18,980
It's just that animals are stupider than humans.

181
00:13:18,980 --> 00:13:21,980
They don't have the intelligent.

182
00:13:21,980 --> 00:13:24,980
They aren't capable of the great good

183
00:13:24,980 --> 00:13:27,980
and the great evil that human beings are capable of.

184
00:13:27,980 --> 00:13:32,980
Which makes you wonder whether that's why Mara is actually an angel.

185
00:13:32,980 --> 00:13:38,980
A David considered to be in the David realms.

186
00:13:38,980 --> 00:13:43,980
Wonder whether angels are capable of even more evil.

187
00:13:43,980 --> 00:13:47,980
But Mara is actually an interesting case.

188
00:13:47,980 --> 00:13:51,980
Because it doesn't sound like Mara actually does actually hurt people.

189
00:13:51,980 --> 00:13:54,980
He just convinces people to stay in some sorrow.

190
00:13:54,980 --> 00:14:01,980
He intoxicates them with central pleasure, tricks them into being intoxicated.

191
00:14:01,980 --> 00:14:02,980
I don't know.

192
00:14:02,980 --> 00:14:06,980
There's lots of stories about this type of angel.

193
00:14:06,980 --> 00:14:11,980
But about human beings, human beings can be terrible.

194
00:14:11,980 --> 00:14:13,980
We are too intelligent.

195
00:14:13,980 --> 00:14:21,980
We let our intelligence guide us rather than wisdom.

196
00:14:21,980 --> 00:14:26,980
Wisdom intelligence is a weapon.

197
00:14:26,980 --> 00:14:31,980
But wisdom is not a tool.

198
00:14:31,980 --> 00:14:35,980
Wisdom is only useful for good.

199
00:14:35,980 --> 00:14:37,980
Wisdom cannot make you do evil.

200
00:14:37,980 --> 00:14:40,980
Intelligence can very much allow you

201
00:14:40,980 --> 00:14:45,980
and support you in cultivating evil.

202
00:14:45,980 --> 00:14:49,980
The intellect can be a great tool for evil people.

203
00:14:49,980 --> 00:14:54,980
You can be very clever.

204
00:14:54,980 --> 00:14:59,980
Information, logic, reason.

205
00:14:59,980 --> 00:15:04,980
One of these things will help you.

206
00:15:04,980 --> 00:15:06,980
Can help you.

207
00:15:06,980 --> 00:15:09,980
This is why they say the road to hell is paved with good intentions.

208
00:15:09,980 --> 00:15:13,980
Part of that is that you can convince yourself

209
00:15:13,980 --> 00:15:16,980
that you're doing the right thing and be doing the wrong thing.

210
00:15:16,980 --> 00:15:21,980
Think of the Spanish inquisition or these people who killed the Christian martyrs.

211
00:15:21,980 --> 00:15:24,980
I don't know the history.

212
00:15:24,980 --> 00:15:33,980
But I understand they were persecuted and killed.

213
00:15:33,980 --> 00:15:44,980
And this was all certainly justified.

214
00:15:44,980 --> 00:15:48,980
People had logical arguments as to why the earth was flat.

215
00:15:48,980 --> 00:15:53,980
Then our humans as to the earth being the center of the universe.

216
00:15:53,980 --> 00:16:01,980
The sun orbiting around the earth.

217
00:16:01,980 --> 00:16:05,980
So there's something else that's needed in that wisdom.

218
00:16:05,980 --> 00:16:08,980
Wisdom looks beyond intelligence.

219
00:16:08,980 --> 00:16:13,980
It doesn't describe what is like science tries to.

220
00:16:13,980 --> 00:16:18,980
It looks also at what shouldn't be.

221
00:16:18,980 --> 00:16:22,980
Or it looks deeper than intelligence.

222
00:16:22,980 --> 00:16:26,980
I think from a Buddhist point of view you can argue that it's still the same.

223
00:16:26,980 --> 00:16:31,980
It's just a different type of intelligence.

224
00:16:31,980 --> 00:16:36,980
Because in Buddhism we don't exactly look at wisdom as a value judgment.

225
00:16:36,980 --> 00:16:40,980
But it's a different type of intelligence or knowledge.

226
00:16:40,980 --> 00:16:43,980
It's knowledge from experience.

227
00:16:43,980 --> 00:16:48,980
Knowledge not based on thought or supposition or logic.

228
00:16:48,980 --> 00:16:49,980
It doesn't come from the mind.

229
00:16:49,980 --> 00:16:53,980
It comes directly and solely from the experience itself.

230
00:16:53,980 --> 00:16:56,980
There's no interpretation of the experience.

231
00:16:56,980 --> 00:16:59,980
There's no extrapolation.

232
00:16:59,980 --> 00:17:04,980
There's no...

233
00:17:04,980 --> 00:17:09,980
There's no part for the mind.

234
00:17:09,980 --> 00:17:12,980
It's solely what we experience.

235
00:17:12,980 --> 00:17:13,980
What is observed.

236
00:17:13,980 --> 00:17:18,980
And what is undeniably true.

237
00:17:18,980 --> 00:17:21,980
That's what...

238
00:17:21,980 --> 00:17:25,980
That's what leads to enlightenment.

239
00:17:25,980 --> 00:17:33,980
So I mean this quote isn't that.

240
00:17:33,980 --> 00:17:35,980
It's a nice quote.

241
00:17:35,980 --> 00:17:41,980
But it's not like a deep, dark, triangle teaching.

242
00:17:41,980 --> 00:17:43,980
Because of course, human animals are...

243
00:17:43,980 --> 00:17:45,980
He's just trying to shame them by saying,

244
00:17:45,980 --> 00:17:48,980
you know, even animals can get along.

245
00:17:48,980 --> 00:17:51,980
Why can't you guys?

246
00:17:51,980 --> 00:18:09,980
But the reason why animals are better able to get along is in part of the reason it's...

247
00:18:09,980 --> 00:18:13,980
They're just too dumb to plot against each other.

248
00:18:13,980 --> 00:18:20,980
They might attack each other, but they forget very quickly.

249
00:18:20,980 --> 00:18:24,980
They're not very good at putting things together.

250
00:18:24,980 --> 00:18:29,980
They may have evil thoughts, but they quickly go away.

251
00:18:31,980 --> 00:18:36,980
I suppose even more you could say that animals are very much stuck in their routines

252
00:18:36,980 --> 00:18:38,980
as a result of low intelligence.

253
00:18:38,980 --> 00:18:40,980
They're stuck in their ways.

254
00:18:40,980 --> 00:18:46,980
And that has the result of creating sort of harmony, right?

255
00:18:46,980 --> 00:18:50,980
Where the predator and prey are always balanced.

256
00:18:50,980 --> 00:18:53,980
And this is why when people look at nature, they think,

257
00:18:53,980 --> 00:18:55,980
oh, it's so harmonious.

258
00:18:55,980 --> 00:18:58,980
Of course, if you actually live in nature for any period of time,

259
00:18:58,980 --> 00:19:01,980
it's a war zone.

260
00:19:01,980 --> 00:19:05,980
Kill or be killed, always killing, always fighting, always.

261
00:19:09,980 --> 00:19:13,980
Not really as peaceful as it seems.

262
00:19:13,980 --> 00:19:17,980
But the animals can get along.

263
00:19:17,980 --> 00:19:20,980
It's a shaming because we have more intelligence.

264
00:19:20,980 --> 00:19:21,980
It's a shaming because we're not...

265
00:19:21,980 --> 00:19:27,980
We're not... We don't often use the intelligence for any good.

266
00:19:27,980 --> 00:19:32,980
And so I should all feel quite proud that we are using our intelligence

267
00:19:32,980 --> 00:19:36,980
for good using our intelligence to cultivate wisdom

268
00:19:36,980 --> 00:19:39,980
through the meditation practice.

269
00:19:39,980 --> 00:19:44,980
We're using our intelligence to refine our intelligence,

270
00:19:44,980 --> 00:19:47,980
to refine our knowledge, to create true knowledge

271
00:19:47,980 --> 00:19:52,980
and understanding of reality as it is.

272
00:19:52,980 --> 00:19:55,980
So good job, everyone.

273
00:20:01,980 --> 00:20:07,980
That's the... I'm going to say about that short quote.

274
00:20:07,980 --> 00:20:09,980
I'm posting the...

275
00:20:13,980 --> 00:20:17,980
...posting the hangout if anybody has a question they want to come on and ask.

276
00:20:17,980 --> 00:20:19,980
We need attention.

277
00:20:35,980 --> 00:20:39,980
We need attention.

278
00:20:39,980 --> 00:20:45,980
We need more people to get involved with our organization.

279
00:20:45,980 --> 00:20:52,980
So we're having a bit of an organizational...

280
00:20:52,980 --> 00:20:54,980
Am I allowed to say this online?

281
00:20:54,980 --> 00:20:55,980
Of course, Robin.

282
00:20:55,980 --> 00:20:58,980
I don't know if this is private information.

283
00:20:58,980 --> 00:21:01,980
There's a secret top secret information.

284
00:21:01,980 --> 00:21:07,980
I want to say that if anyone would like to get involved with the organization,

285
00:21:07,980 --> 00:21:13,980
please come on out.

286
00:21:13,980 --> 00:21:18,980
Because now that we've got an organization and we've got resources and support,

287
00:21:18,980 --> 00:21:26,980
we need people to manage and make use of this support.

288
00:21:36,980 --> 00:21:41,980
We also could use a bit of help on the IT side.

289
00:21:41,980 --> 00:21:46,980
It'd be nice if this website could be updated.

290
00:21:46,980 --> 00:21:49,980
It'd be nice if we could move to a more...

291
00:21:49,980 --> 00:22:01,980
There was at one point a guy from Germany who made a better organized version of this site.

292
00:22:01,980 --> 00:22:20,980
Like, I don't know, somehow more standard and modern programming-based version of this site.

293
00:22:20,980 --> 00:22:34,980
So, if anybody wants to help out with that kind of thing, also the IOS version of our app.

294
00:22:34,980 --> 00:22:41,980
If somebody wants to work on the Android app, there's people with IT skills, programming skills.

295
00:22:41,980 --> 00:22:46,980
Please let us know.

296
00:22:46,980 --> 00:22:51,980
Big part is when the organization of people, especially people in Ontario,

297
00:22:51,980 --> 00:22:57,980
if there's anybody in Ontario who wants to help out, please don't hesitate.

298
00:22:57,980 --> 00:23:04,980
We're having a volunteer meeting next Sunday at 12.

299
00:23:04,980 --> 00:23:25,980
I think there's even a Facebook group, Brian.

300
00:23:25,980 --> 00:23:35,980
Anyway, no questions that I'm off.

