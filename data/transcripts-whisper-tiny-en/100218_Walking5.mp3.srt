1
00:00:00,000 --> 00:00:12,480
Meditation is something that we use just like we would to train the body, except we're

2
00:00:12,480 --> 00:00:16,560
using it to train the mind.

3
00:00:16,560 --> 00:00:23,920
So everything we do should be seen as some kind of development, some kind of exercise.

4
00:00:23,920 --> 00:00:28,840
And just like with physical exercise, we start with something very simple because our

5
00:00:28,840 --> 00:00:36,760
body is not yet accustomed to the work, to the task.

6
00:00:36,760 --> 00:00:42,520
The same when we train the mind, when we give the mind a workout, we start with something

7
00:00:42,520 --> 00:00:48,200
very simple, say the rising and the falling of the abdomen, we're just walking, stepping,

8
00:00:48,200 --> 00:00:50,760
right, stepping left.

9
00:00:50,760 --> 00:00:58,160
And as we go on, we give more and more intricate, more complicated exercises.

10
00:00:58,160 --> 00:01:04,560
As the mind becomes sharp, so this is for two reasons, one because the mind becomes sharper

11
00:01:04,560 --> 00:01:15,200
and therefore wanders, if not given a more difficult exercise to contemplate.

12
00:01:15,200 --> 00:01:23,000
And also because we want to develop the mind that much further.

13
00:01:23,000 --> 00:01:31,120
There's a similar given in the Risudimaka, the path of purification, which is an ancient summary

14
00:01:31,120 --> 00:01:38,680
of the teachings of the Buddha and the explanation of the teachings of the Buddha.

15
00:01:38,680 --> 00:01:45,560
And he says just like when you look at a candle and you see the candle, it starts burning

16
00:01:45,560 --> 00:01:50,560
from the top and when it gets to the bottom, it's gone.

17
00:01:50,560 --> 00:01:58,200
When you say to yourself wow that's impermanent and you say that's the burning of a candle,

18
00:01:58,200 --> 00:02:01,720
it's gone and it starts and it ends.

19
00:02:01,720 --> 00:02:05,760
But that's really looking at it in a very coarse way.

20
00:02:05,760 --> 00:02:12,240
When you want to understand the rising and ceasing, you want to understand impermanence,

21
00:02:12,240 --> 00:02:17,640
you have to look at the candle in more fine detail.

22
00:02:17,640 --> 00:02:24,200
So another time you might look at the candle and see that now it's a third of the way

23
00:02:24,200 --> 00:02:25,200
gone.

24
00:02:25,200 --> 00:02:27,240
Now it's two thirds gone, now it's gone.

25
00:02:27,240 --> 00:02:31,720
So you might look at it in pieces, where you might look at it in smaller pieces and little

26
00:02:31,720 --> 00:02:32,720
bits.

27
00:02:32,720 --> 00:02:40,040
And finally you look at it very, very, very close and you see the strands burning, and

28
00:02:40,040 --> 00:02:45,120
you see the strands as you pull the, you see the rope being pulled apart, the wick being

29
00:02:45,120 --> 00:02:47,560
pulled apart into its little pieces.

30
00:02:47,560 --> 00:02:53,040
I'm burning up moment by moment by moment.

31
00:02:53,040 --> 00:02:55,640
The simile here is with one's life.

32
00:02:55,640 --> 00:03:00,400
So someone might say wow, look at that person, they were alive and now they're dead or

33
00:03:00,400 --> 00:03:05,600
look at me, I'm going to be dead someday and therefore life is impermanent.

34
00:03:05,600 --> 00:03:11,840
But that doesn't really give you any insight or any wisdom, it's some sort of course wisdom,

35
00:03:11,840 --> 00:03:17,200
but it's not strong enough to really help you understand impermanence.

36
00:03:17,200 --> 00:03:20,960
And when you think about how before you were young and now you're an adult and later

37
00:03:20,960 --> 00:03:28,160
you're going to be old, this is another, this is a much more detailed or a bit more detailed

38
00:03:28,160 --> 00:03:30,000
way of looking at it.

39
00:03:30,000 --> 00:03:34,040
And so you break it up further and further and further and you can break it up all the

40
00:03:34,040 --> 00:03:38,160
way to yesterday and today and tomorrow.

41
00:03:38,160 --> 00:03:41,520
And then you can go moment to moment to moment and they say until finally you break the

42
00:03:41,520 --> 00:03:43,960
walking step up into six parts.

43
00:03:43,960 --> 00:03:48,080
And so this is where we get these parts of walking step.

44
00:03:48,080 --> 00:03:51,520
For beginners we give you the first walking step which is stepping right, stepping left

45
00:03:51,520 --> 00:03:55,600
and then as you go on we're going to give you more and more complicated walking steps

46
00:03:55,600 --> 00:04:02,440
until there's altogether six pieces which is old tara na, anti-hara na, re-di-hara na,

47
00:04:02,440 --> 00:04:07,840
oh such a nut, so you keep a nut, so you don't put that, which are the six words and

48
00:04:07,840 --> 00:04:12,800
poly given to the six parts of the step.

49
00:04:12,800 --> 00:04:18,280
Being meditation is the same, we just give you more exercises as a training and something

50
00:04:18,280 --> 00:04:26,400
which helps you to develop your mind that much, how much more.

51
00:04:26,400 --> 00:04:30,720
So I don't think anyone should find any greater meaning than that.

52
00:04:30,720 --> 00:04:36,680
Why they broke it up into six, I don't know, I think it was just sort of a logical

53
00:04:36,680 --> 00:04:40,360
classification of the different parts like when you lift the heel, that's the first part

54
00:04:40,360 --> 00:04:44,120
when you lift the rest of the foot, that's the second part, when you move it out, that's

55
00:04:44,120 --> 00:04:46,000
the third part.

56
00:04:46,000 --> 00:04:51,440
Actually the six steps in the, we see demycler not exactly as we teach them, but I don't

57
00:04:51,440 --> 00:04:57,080
think that I have to go into detail about why that is.

58
00:04:57,080 --> 00:05:03,840
Second question is why do you write the rule about not discussing practices with co-students?

59
00:05:03,840 --> 00:05:14,000
This is because, well, discussing practice as well, if this was referring to teaching other

60
00:05:14,000 --> 00:05:17,960
meditators, then certainly this is totally forbidden and this is something that can even

61
00:05:17,960 --> 00:05:24,840
possibly get you kicked out if you get into teaching others and giving them advice about

62
00:05:24,840 --> 00:05:30,560
their practice and something that we strictly don't allow for many reasons.

63
00:05:30,560 --> 00:05:35,440
I don't think this is exactly what's being asked here, but I'll go into this first.

64
00:05:35,440 --> 00:05:40,640
First of all, it's very unlikely that you know exactly how to teach, if you haven't been

65
00:05:40,640 --> 00:05:41,640
trained as a teacher.

66
00:05:41,640 --> 00:05:46,280
Being trained as a meditator and trained as a teacher are two very different things.

67
00:05:46,280 --> 00:05:53,840
We have to spend many, many hours sitting and listening to another teacher teach before

68
00:05:53,840 --> 00:05:59,560
we can actually go off and teach by ourselves, we have to sit and listen for hours and

69
00:05:59,560 --> 00:06:06,560
days and days and months and months before you can actually be qualified just to begin

70
00:06:06,560 --> 00:06:13,960
to give people advice, so I mean, it's just so easy to give people the wrong advice.

71
00:06:13,960 --> 00:06:18,640
You think this is the right advice because you know the answer, but just because you know

72
00:06:18,640 --> 00:06:21,960
the answer doesn't mean you know how to give it to the person and how to give them something

73
00:06:21,960 --> 00:06:24,560
useful and how to give them what they need.

74
00:06:24,560 --> 00:06:28,560
Sometimes it's quite different from what they want.

75
00:06:28,560 --> 00:06:33,000
The other thing is even if you do know what you're talking about, they still don't any

76
00:06:33,000 --> 00:06:37,320
good teacher would never advise a student who's practicing under a different teacher, they

77
00:06:37,320 --> 00:06:39,920
would say go and ask your teacher.

78
00:06:39,920 --> 00:06:45,880
Just like a doctor would never prescribe medicine to a patient of another doctor.

79
00:06:45,880 --> 00:06:50,880
You have to choose one doctor or the other because otherwise even though the advice might

80
00:06:50,880 --> 00:06:59,000
be right it's a different path and a different technique or it's a different style and

81
00:06:59,000 --> 00:07:03,520
so this for sure is something that should be discouraged unless it's a very technical

82
00:07:03,520 --> 00:07:10,320
issue like you see someone closing their eyes when they walk and so they're swaying all

83
00:07:10,320 --> 00:07:11,320
over the place.

84
00:07:11,320 --> 00:07:14,760
You can mention to them that you're, as you understand it, you're supposed to keep

85
00:07:14,760 --> 00:07:19,000
your eyes open when you walk, but even that is you might find that the results are not

86
00:07:19,000 --> 00:07:22,840
as you expect.

87
00:07:22,840 --> 00:07:27,080
But here talking about your own practice with other people, this is what's much more common.

88
00:07:27,080 --> 00:07:32,680
We find meditators telling other meditators about their own practice which is still quite

89
00:07:32,680 --> 00:07:37,920
it, quite harmful to the other person's practice because in general it creates expectation

90
00:07:37,920 --> 00:07:45,080
in their mind and comparison and worry and distraction.

91
00:07:45,080 --> 00:07:49,360
Sometimes it will lead them to try and create the same state that they hear that you're

92
00:07:49,360 --> 00:07:50,360
experiencing.

93
00:07:50,360 --> 00:07:55,800
Often it will lead them to misunderstand their own states and it will lead them to become

94
00:07:55,800 --> 00:08:00,120
confused and unsure about what is the path.

95
00:08:00,120 --> 00:08:05,360
If you're on the wrong path it's very easy to lead someone else on the wrong path and

96
00:08:05,360 --> 00:08:14,960
so we have a rule against this because of the effect that it has on the other meditator.

97
00:08:14,960 --> 00:08:22,080
And because of the fact that they're not your teacher and they're not someone who you

98
00:08:22,080 --> 00:08:28,400
should trust with your practice and seek advice from and because you can spoil their

99
00:08:28,400 --> 00:08:29,400
practice.

100
00:08:29,400 --> 00:08:30,400
These two things.

101
00:08:30,400 --> 00:08:35,000
So I hope that answers those questions, that's about as best as I can do.

102
00:08:35,000 --> 00:08:40,800
Again, if you have more questions please leave them in the box either in Thai or in English.

103
00:08:40,800 --> 00:08:45,400
Just make sure I can read it if I can't read it, I won't answer it.

104
00:08:45,400 --> 00:08:50,280
I can read Thai that's no problem.

105
00:08:50,280 --> 00:08:54,560
So today's talk I actually wanted to talk about walking meditation and sitting meditation.

106
00:08:54,560 --> 00:08:59,440
I give this talk off to them about what are the benefits of walking and sitting meditation

107
00:08:59,440 --> 00:09:02,400
specifically.

108
00:09:02,400 --> 00:09:07,880
The benefits of walking meditation would give five different benefits and this is something

109
00:09:07,880 --> 00:09:12,120
that we should understand and we would wonder why do we do walking as well.

110
00:09:12,120 --> 00:09:16,200
First of all, on that many people think that the Buddha didn't practice walking meditation

111
00:09:16,200 --> 00:09:21,360
and they think this is something modern that has been invented.

112
00:09:21,360 --> 00:09:24,520
Actually in the Buddhist time not only did the Buddha walk, practice walking meditation

113
00:09:24,520 --> 00:09:33,160
other, other wonders of other sects and other religions also practiced walking meditation.

114
00:09:33,160 --> 00:09:39,080
And as far as we have written in the text, this is the case.

115
00:09:39,080 --> 00:09:46,200
We have the Buddha giving his own routine of how he would walk and sit all day.

116
00:09:46,200 --> 00:09:52,360
When he didn't have other things to do, he would just walk and sit all day in alteration.

117
00:09:52,360 --> 00:10:00,200
And then at night he would spend the first four hours from 6 to 10 pm walking and sitting

118
00:10:00,200 --> 00:10:10,160
from 10 to 2 he would do lying meditation and with a bit of sleep perhaps but sometimes

119
00:10:10,160 --> 00:10:16,040
giving talks or teaching in the lying position and then at 2 am he would get up and start

120
00:10:16,040 --> 00:10:20,120
walking again and walking and sitting for the rest of the night and then in the morning

121
00:10:20,120 --> 00:10:24,680
he could go for arms and continue walking and sitting.

122
00:10:24,680 --> 00:10:26,520
And he gave the same advice to meditators.

123
00:10:26,520 --> 00:10:31,640
He said, among should practice in this way, walking and sitting for the whole day and

124
00:10:31,640 --> 00:10:37,160
then at night he separated into 3 parts 6 to 10 same thing walking and sitting 10 to 2.

125
00:10:37,160 --> 00:10:42,040
That's your lying down period where if you want to sleep you can sleep.

126
00:10:42,040 --> 00:10:47,720
Four hours and you keep in mind the time when you're going to wake up and then at 2 am

127
00:10:47,720 --> 00:10:52,240
you wake up and start doing walking again.

128
00:10:52,240 --> 00:10:54,840
This is for someone who's very serious about the practice.

129
00:10:54,840 --> 00:11:02,040
This is sort of the ultimate goal for meditators to get to this point.

130
00:11:02,040 --> 00:11:03,960
So walking meditation did play a big part.

131
00:11:03,960 --> 00:11:10,440
The Buddha said that we have his words, what we understand to be his words, recorded

132
00:11:10,440 --> 00:11:17,120
the terrified benefits of walking meditation, why he didn't have monks just do sitting meditation.

133
00:11:17,120 --> 00:11:26,120
He even had prescribed the size of walking paths and how to make a walking path and how

134
00:11:26,120 --> 00:11:29,760
it should be made, how it shouldn't be made and so on.

135
00:11:29,760 --> 00:11:35,960
So the five benefits that he said are one and it helps you to be patient and endure walking

136
00:11:35,960 --> 00:11:38,400
long distances.

137
00:11:38,400 --> 00:11:42,360
So nowadays this might not be a big thing but in that time it was big.

138
00:11:42,360 --> 00:11:51,760
The first one walking long distances, the second one enduring hard work or just creating

139
00:11:51,760 --> 00:11:55,320
overall endurance.

140
00:11:55,320 --> 00:12:03,520
Number three, the food that has been eaten will be able to be digested easier.

141
00:12:03,520 --> 00:12:10,800
Number four sicknesses and ailments in the body will be worked out and ate it and even

142
00:12:10,800 --> 00:12:13,840
if it may disappear.

143
00:12:13,840 --> 00:12:19,240
And number five, the concentration that comes from walking meditation lasts a long time

144
00:12:19,240 --> 00:12:21,960
as a strength which lasts for a long time.

145
00:12:21,960 --> 00:12:26,800
They said the five benefits that the Buddha gave.

146
00:12:26,800 --> 00:12:30,720
So as I said, the first one is something that might not be of great benefit.

147
00:12:30,720 --> 00:12:36,720
Nowadays to many people we think that walking long distances is a pain anyway and something

148
00:12:36,720 --> 00:12:38,800
that we wouldn't ever consider doing.

149
00:12:38,800 --> 00:12:45,440
In the Buddha's time it was a big deal and it's kind of a shame that nowadays we feel

150
00:12:45,440 --> 00:12:49,400
this way because actually walking is something very special.

151
00:12:49,400 --> 00:12:55,600
If anyone's ever done long walking, it actually changes the way you look at the world

152
00:12:55,600 --> 00:12:57,920
instead of just whizzing by in a car.

153
00:12:57,920 --> 00:13:03,000
I know there were many philosophers who have written about this, well one philosopher anyway

154
00:13:03,000 --> 00:13:10,080
the philosopher I'm thinking of is Thoreau Henry David Thoreau was a naturalist who talked

155
00:13:10,080 --> 00:13:13,800
a lot about walking and he walked long distances.

156
00:13:13,800 --> 00:13:18,640
When I was in Thailand I did a lot of walking because I didn't touch money.

157
00:13:18,640 --> 00:13:23,760
I still don't touch money so in Thailand sometimes it was the case that I had to walk

158
00:13:23,760 --> 00:13:31,440
from here to there even in the city, the side of the highway in the forest, walked in

159
00:13:31,440 --> 00:13:35,520
many different situations.

160
00:13:35,520 --> 00:13:39,560
And you really look at the world in a whole different way, you see people working on

161
00:13:39,560 --> 00:13:45,800
the side of the road, they smile, you get to see cows, you see all sorts of many different

162
00:13:45,800 --> 00:13:50,480
things and you look at the world in a whole different way and it helps you to come back

163
00:13:50,480 --> 00:13:56,080
to reality instead of just whizzing around, always thinking about where you're going next.

164
00:13:56,080 --> 00:13:59,760
It brings you very much back to reality.

165
00:13:59,760 --> 00:14:05,080
And that we should all consider an important part of the human life, walking is something

166
00:14:05,080 --> 00:14:13,920
it's one part of what we're built for, this body is built to walk and so it's something

167
00:14:13,920 --> 00:14:24,040
that as I go into talk about it it helps the systems in the body but the walking meditation

168
00:14:24,040 --> 00:14:27,880
is something that allows you to do this sort of thing, allows you to walk long distances

169
00:14:27,880 --> 00:14:32,320
where now we might always consider getting in the car and just to go down to the end of

170
00:14:32,320 --> 00:14:33,320
the road.

171
00:14:33,320 --> 00:14:37,600
Once you do walking meditation you find that walking long distances is actually a pleasure

172
00:14:37,600 --> 00:14:44,840
and something that you're happy to do which is great because it helps the environment,

173
00:14:44,840 --> 00:14:52,440
it helps our lives, it makes things easier, we don't have to rely on these machines.

174
00:14:52,440 --> 00:14:56,960
And about this time of course it was necessary and for monks it is necessary to oftentimes

175
00:14:56,960 --> 00:15:04,840
turn their lives, they would have to walk long distances to see their teacher, just to

176
00:15:04,840 --> 00:15:07,000
go on arms around and so on.

177
00:15:07,000 --> 00:15:14,160
But we can consider that nowadays this is maybe one of the lesser advantages, a greater

178
00:15:14,160 --> 00:15:18,920
advantage is the overall endurance that it gives us and this is something that's useful

179
00:15:18,920 --> 00:15:25,240
for people not just monks, people living in the world who have to do hard work, who

180
00:15:25,240 --> 00:15:31,360
have to work every day, something that walking meditation gives you endurance because

181
00:15:31,360 --> 00:15:37,400
you're forced to do the same repetitious motion again and again, often people think that

182
00:15:37,400 --> 00:15:42,160
walking meditation is so silly because you're just walking back and forth for hours and

183
00:15:42,160 --> 00:15:47,480
hours a day, you're not getting anywhere.

184
00:15:47,480 --> 00:15:54,880
Actually this is a funny one because there's a red somewhere, there was a teacher he said,

185
00:15:54,880 --> 00:16:02,240
you can think that, where you're going, the answer is you're walking to new bandana,

186
00:16:02,240 --> 00:16:06,680
you're walking to freedom and as you walk back and forth people say, you know, where do

187
00:16:06,680 --> 00:16:11,080
you think you're going to go, you're walking to freedom and he said, suppose you consider

188
00:16:11,080 --> 00:16:19,560
that, suppose you pretend that freedom from suffering is like a thousand kilometers away

189
00:16:19,560 --> 00:16:27,080
and then you count how many kilometers you walk in a week or so and when walking meditation

190
00:16:27,080 --> 00:16:30,640
and you can really feel happy because it's not actually that hard you can actually get

191
00:16:30,640 --> 00:16:31,640
there.

192
00:16:31,640 --> 00:16:35,120
I don't know if thousand kilometers or how long, how far?

193
00:16:35,120 --> 00:16:40,680
It's supposed a thousand kilometers away, it's quite a bit but to consider how much

194
00:16:40,680 --> 00:16:48,160
you walk, if people would say certain number of kilometers away, then after a while maybe

195
00:16:48,160 --> 00:16:53,200
a year or two years or something, you could eventually become totally free from suffering.

196
00:16:53,200 --> 00:16:58,920
You would have said seven years at max.

197
00:16:58,920 --> 00:17:04,440
But on a more worldly level it gives you a great amount of endurance because you're

198
00:17:04,440 --> 00:17:12,400
doing this repetitious motion and so it helps you do away with things like boredom,

199
00:17:12,400 --> 00:17:21,000
agitation, distraction, it helps your mind focus and calm down and you know, these people

200
00:17:21,000 --> 00:17:26,120
who say, when you're all steamed up all around that, you should go and take a walk.

201
00:17:26,120 --> 00:17:31,160
Well, there's a reason for that because this walking motion, it's something that calms

202
00:17:31,160 --> 00:17:38,160
you down and gets you set on just walking and allows you to give up many, many different

203
00:17:38,160 --> 00:17:40,840
things.

204
00:17:40,840 --> 00:17:44,280
Something that's actually quite difficult to do in meditators to come here, they find

205
00:17:44,280 --> 00:17:48,760
much more difficult than the sitting meditation in the beginning.

206
00:17:48,760 --> 00:17:53,200
But then as they practice on, sometimes they even find it easier than the sitting in

207
00:17:53,200 --> 00:17:59,800
the end because it's something that develops your endurance and gives you a great amount

208
00:17:59,800 --> 00:18:08,680
of energy, a great amount of strength, it allows you to do any other thing.

209
00:18:08,680 --> 00:18:15,240
There's nothing that can be as boring or uninteresting as just walking back and forth so that

210
00:18:15,240 --> 00:18:22,080
any work that you have to do, whether it be at a job or at home or at school or so on,

211
00:18:22,080 --> 00:18:29,320
you're able to do it much easier without tiring, without becoming bored, without becoming

212
00:18:29,320 --> 00:18:31,240
disinterested.

213
00:18:31,240 --> 00:18:32,240
So this is a training.

214
00:18:32,240 --> 00:18:36,600
Again, these things are not meant to be fun or meant to be interesting.

215
00:18:36,600 --> 00:18:41,040
It's meant to be a training that changes the mind and gives the mind certain qualities

216
00:18:41,040 --> 00:18:46,240
that it doesn't yet have that it's lacking.

217
00:18:46,240 --> 00:18:51,400
The third benefit is that the food that we've eaten will be digested better.

218
00:18:51,400 --> 00:18:57,400
So for this reason, we often encourage meditators to do walking meditation after they

219
00:18:57,400 --> 00:19:05,000
eat because of course if we just sit around all the time in our digestive system, it's

220
00:19:05,000 --> 00:19:06,520
hard pressed to work properly.

221
00:19:06,520 --> 00:19:08,480
Many people don't realize this.

222
00:19:08,480 --> 00:19:13,440
In the world, many people who are after eating will lie down to sleep or so on, especially

223
00:19:13,440 --> 00:19:20,400
we hear about people who eat dinner at 9 p.m. 10 p.m. and then don't do anything with

224
00:19:20,400 --> 00:19:25,480
it and they lie down and it just sits and rots in their stomach.

225
00:19:25,480 --> 00:19:30,280
Whereas if you eat only in the morning and then you do walking afterwards, you'll find

226
00:19:30,280 --> 00:19:35,320
that your body is able to process it very quickly and make the best use of it and not

227
00:19:35,320 --> 00:19:39,760
just let it sit in the stomach and become rotten.

228
00:19:39,760 --> 00:19:51,320
It's something that allows you to use the food to the best of its best, to its best use.

229
00:19:51,320 --> 00:19:55,800
And so for this reason, often you find you don't need to eat as much when you do meditation,

230
00:19:55,800 --> 00:19:59,440
especially when you do this kind of walking and sitting meditation because you find that

231
00:19:59,440 --> 00:20:08,880
your body is able to use the food quite efficiently and you find that you don't have constipation

232
00:20:08,880 --> 00:20:14,800
that you don't have and this fatigue after you eat so you're able to eat quite a great

233
00:20:14,800 --> 00:20:21,200
amount at once and then it lasts you for the whole 24 hours.

234
00:20:21,200 --> 00:20:30,880
The fourth one, fourth reason is because walking meditation helps to do it with sicknesses.

235
00:20:30,880 --> 00:20:34,960
So this is of course things like constipation but it's also, this is just sort of a by-product.

236
00:20:34,960 --> 00:20:39,520
We're not here practicing because we have physical ailments.

237
00:20:39,520 --> 00:20:42,440
Although many people use that as a recent start meditating.

238
00:20:42,440 --> 00:20:50,760
It's not a terribly good reason but meditation can be said to help the body in many ways.

239
00:20:50,760 --> 00:20:55,560
Or even I've heard reported cases of meditation helping to cure cancer and I'm kind of

240
00:20:55,560 --> 00:21:00,720
shying away from that now because I don't have my own proof or anything and obviously

241
00:21:00,720 --> 00:21:05,760
any proof would only be anecdotal.

242
00:21:05,760 --> 00:21:12,880
But the rationale is, and I think it's quite sound, is that meditation affects the body

243
00:21:12,880 --> 00:21:14,440
very much.

244
00:21:14,440 --> 00:21:16,080
It's something that loosens up the body.

245
00:21:16,080 --> 00:21:20,800
You can notice it that the tension that you use to have in your shoulders or in your back

246
00:21:20,800 --> 00:21:26,280
is slowly down, actually it becomes very strong during meditation until finally it's

247
00:21:26,280 --> 00:21:30,080
totally down away with, it's like it's working at the kinks out.

248
00:21:30,080 --> 00:21:34,960
This is when you'd have a massage, you'd find that it works the kinks out.

249
00:21:34,960 --> 00:21:39,000
The problem with a massage the reason why it comes back is because the mind just creates

250
00:21:39,000 --> 00:21:40,960
the kinks again.

251
00:21:40,960 --> 00:21:47,760
Because you work them out in the mind, the body becomes very, very, very healthy and very,

252
00:21:47,760 --> 00:21:53,440
very balanced and very, very much centered and backed into its natural state.

253
00:21:53,440 --> 00:21:58,080
So I think there's nothing strange about the idea that it should cure many, many different

254
00:21:58,080 --> 00:21:59,080
sicknesses.

255
00:21:59,080 --> 00:22:05,280
Of course starting with things like high blood pressure, high blood pressure, maybe heart

256
00:22:05,280 --> 00:22:08,360
disease.

257
00:22:08,360 --> 00:22:14,320
I even had a woman who had multiple sclerosis and I taught her how to do the walking meditation

258
00:22:14,320 --> 00:22:19,520
and she did a lying on her walker back and forth, back and forth, just pushing her legs

259
00:22:19,520 --> 00:22:28,280
as though she were swimming and until finally she could walk without the help of any,

260
00:22:28,280 --> 00:22:37,640
you know, she was very, very, very adamant about doing it and so in the end she was able

261
00:22:37,640 --> 00:22:45,320
to walk without a cane, without anything, back and forth in her living room.

262
00:22:45,320 --> 00:22:49,160
So it's something that trains the body and brings the body back to a much more natural

263
00:22:49,160 --> 00:22:50,160
state.

264
00:22:50,160 --> 00:22:54,000
Of course many things can't be cured and will not be cured through meditation but I think

265
00:22:54,000 --> 00:23:01,400
it's possible that in some cases these things can even be, it can be ameliorated or even

266
00:23:01,400 --> 00:23:10,280
done away with, but again you can quote me on that, it makes perfect sense that a great

267
00:23:10,280 --> 00:23:16,040
many diseases would have and have been proven to be cured by meditation or help by

268
00:23:16,040 --> 00:23:17,040
meditation.

269
00:23:17,040 --> 00:23:24,040
And many other diseases should be possible to help in certain cases in certain instances

270
00:23:24,040 --> 00:23:29,600
as well, simply because of the effect that the body has on the mind to take it out of

271
00:23:29,600 --> 00:23:35,040
that state which can sometimes create things that cancer or so on, it may be that certain

272
00:23:35,040 --> 00:23:44,160
types of cancer are created by this tension or are built up by this, even they say a cancer

273
00:23:44,160 --> 00:23:51,940
can be made worse by eating the wrong foods because it gets stuck in the stomach and

274
00:23:51,940 --> 00:23:59,360
rocks and so how walking meditation helps to get rid of that rotten food and digested

275
00:23:59,360 --> 00:24:04,680
out, they expel it out quickly rather than just sitting around all the time and let it

276
00:24:04,680 --> 00:24:11,480
rotting in the stomach, which some people say helps to create, helps to make cancer worse

277
00:24:11,480 --> 00:24:16,920
or as I've heard things about this.

278
00:24:16,920 --> 00:24:23,200
So it could also be used to prevent certain diseases, maybe even prevent these incurable

279
00:24:23,200 --> 00:24:30,600
diseases, who knows, and any rate it has been clinically proven to help with things like

280
00:24:30,600 --> 00:24:38,920
high blood pressure which is pretty obvious and most meditators can verify that it helps

281
00:24:38,920 --> 00:24:44,080
with stress and bodily stress and aches and pains.

282
00:24:44,080 --> 00:24:50,360
The fifth reason why we practice meditation is walking meditation is because walking meditation

283
00:24:50,360 --> 00:24:56,520
has strong concentration, stronger than the sitting meditation they say, stronger in the

284
00:24:56,520 --> 00:25:03,120
sense that it has great energy or it has great power.

285
00:25:03,120 --> 00:25:07,880
The walking meditation is like an easing into the sitting meditation and the power that

286
00:25:07,880 --> 00:25:15,480
we gain from the walking is sort of static charge, then carries on into the sitting meditation.

287
00:25:15,480 --> 00:25:18,720
You can, one way of looking at this is like a segue into the sitting if you're just

288
00:25:18,720 --> 00:25:21,320
to sit down.

289
00:25:21,320 --> 00:25:29,120
It's like you don't have this chance to put the body in the right state or sorry put

290
00:25:29,120 --> 00:25:33,520
the mind in the right state and so when you sit down you might find that you're falling

291
00:25:33,520 --> 00:25:39,080
asleep because you have too much concentration and not enough effort or you find yourself

292
00:25:39,080 --> 00:25:44,920
becoming distracted and not being able to focus because you have too much energy and

293
00:25:44,920 --> 00:25:48,120
not enough concentration.

294
00:25:48,120 --> 00:25:52,160
When you walk the walking helps to balance this out and because when you're sitting

295
00:25:52,160 --> 00:25:56,000
if you have too much concentration you'll just fall asleep but when you're walking you

296
00:25:56,000 --> 00:26:00,960
find that your concentration is balanced with energy, with effort and when you feel

297
00:26:00,960 --> 00:26:07,160
tired you're able to acknowledge it and not fall asleep.

298
00:26:07,160 --> 00:26:12,520
Also when you have the, when you're distracted because you have the freedom to move and

299
00:26:12,520 --> 00:26:18,240
because you're moving your mind has more to think about and so it doesn't wander as much.

300
00:26:18,240 --> 00:26:22,880
Walking can be a great benefit to one's concentration for this reason.

301
00:26:22,880 --> 00:26:26,360
So these are the Buddha's words on walking meditation.

302
00:26:26,360 --> 00:26:30,560
I don't have to get into sitting meditation but I do want to keep these talks short so

303
00:26:30,560 --> 00:26:34,600
I think I'll wait until next time to talk about the benefits of sitting meditation and

304
00:26:34,600 --> 00:26:41,880
again I encourage you to give me your questions or suggestions for talks if you have any.

305
00:26:41,880 --> 00:26:46,880
It doesn't mean you should sit there thinking of what to suggest but if there's something

306
00:26:46,880 --> 00:26:51,680
you'd like to hear about please do write it either entire in English and put it in the

307
00:26:51,680 --> 00:26:53,640
box.

308
00:26:53,640 --> 00:26:55,040
So that's all for tonight.

309
00:26:55,040 --> 00:26:57,880
Now we'll continue with walking and sitting meditation.

310
00:26:57,880 --> 00:27:01,400
There's mind for frustration then walking and then sitting.

