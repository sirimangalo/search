1
00:00:00,000 --> 00:00:04,340
Hello and welcome back to our study of the Dhamapada.

2
00:00:04,340 --> 00:00:10,440
Today we continue on with first number 91, which reads as follows.

3
00:00:10,440 --> 00:00:26,580
U yun janti, satimanto, naniketi, ramanteti, hang sawa, palalang, hitwa, okamokang, janteti.

4
00:00:26,580 --> 00:00:38,360
Which means U yun janti satimanto, the mindful excerpt themselves, or the mindful work hard.

5
00:00:38,360 --> 00:00:51,640
Naniketi, ramanteti, they don't delight in dwellings or they don't delight in dwellings, in homes.

6
00:00:51,640 --> 00:01:06,480
Hang sawa, palalang, hitwa, just as a like a swan, having abandoned the lake, abandoned the lake, okamokang, janteti.

7
00:01:06,480 --> 00:01:14,760
They abandoned house after house, home after home.

8
00:01:14,760 --> 00:01:24,240
So this story, this verse, was told in relation to a story that's actually interesting in relation to the verse.

9
00:01:24,240 --> 00:01:29,200
Because it actually implies something of the opposite, what you would think.

10
00:01:29,200 --> 00:01:37,920
From the sounds of this verse, you would think that the Buddha was saying that the person who is mindful exerts themselves.

11
00:01:37,920 --> 00:01:49,680
Such a person should, you would most likely expect to avoid having a home and wander.

12
00:01:49,680 --> 00:02:04,120
So here we have this story, the Buddha was going on pilgrimage, wandering Charika, the Buddha would go around India to teach.

13
00:02:04,120 --> 00:02:14,520
So traveling from place to place and living and talking with people, debating sometimes with people, with other ascetics.

14
00:02:14,520 --> 00:02:28,760
Sometimes lay people would come and debate with him or ask him difficult questions, and he would teach and answer and spread his teachings throughout India.

15
00:02:28,760 --> 00:02:37,880
So he was getting ready to go, and all the monks knew that the Buddha was getting ready to go, and so they all prepared packed up their things.

16
00:02:37,880 --> 00:02:51,160
They scrubbed up their bowls and gathered up their robes, and then they saw Maha Kastapa, who was washing his robe.

17
00:02:51,160 --> 00:03:00,280
And they started snickering to themselves or frowning, turning up their noses at this.

18
00:03:00,280 --> 00:03:04,520
Because this is a clear indication that Maha Kastapa wasn't ready to go.

19
00:03:04,520 --> 00:03:09,440
And he wasn't going, and they said, oh, I know, one of them said, I know what's going on here.

20
00:03:09,440 --> 00:03:12,400
Here's a guy who is attached to houses.

21
00:03:12,400 --> 00:03:14,240
He's attached to his dwelling.

22
00:03:14,240 --> 00:03:16,080
I mean, who wouldn't be?

23
00:03:16,080 --> 00:03:26,880
He's famous, and he's loved by all the lay people, and they give him the best choices to food, and robes and everything he wants.

24
00:03:26,880 --> 00:03:28,240
He gets here.

25
00:03:28,240 --> 00:03:31,200
If I got that, I wouldn't leave either.

26
00:03:31,200 --> 00:03:38,960
So no doubt he's staying here because he's attached to it, and he's comfortable with it.

27
00:03:38,960 --> 00:03:47,560
And so they gossiped and were rather fairly mean-spirited, it sounds like.

28
00:03:47,560 --> 00:03:54,320
Maybe not mean-spirited, but they certainly misrepresented his intention, because the Buddha heard about it.

29
00:03:54,320 --> 00:04:02,880
What happened then, everyone left, all the monks left, and Maha Kastapa went with them, but he got to a certain point.

30
00:04:02,880 --> 00:04:09,280
And the Buddha turned and said to Maha Kastapa, Maha Kastapa, we used to be behind.

31
00:04:09,280 --> 00:04:20,240
And they had the monks said to each other, now there you go, even the Buddha knows that Maha Kastapa can't leave this place, he's too attached to it.

32
00:04:20,240 --> 00:04:29,480
And so when they were sitting down, I guess in the evening, or they were talking amongst themselves, when they were walking, maybe the Buddha turned and said, what are you guys talking about?

33
00:04:29,480 --> 00:04:35,720
What are you because engaged in discussion about?

34
00:04:35,720 --> 00:04:42,280
And they told him, and he said, that's ridiculous, that's not at all.

35
00:04:42,280 --> 00:04:49,640
I didn't use those words, but he said, that's not at all why Maha Kastapa was not ready to go.

36
00:04:49,640 --> 00:04:54,080
And that's not at all why I asked him to turn around.

37
00:04:54,080 --> 00:05:03,320
Because Maha Kastapa made a determination in his past lives to be a support for lay people.

38
00:05:03,320 --> 00:05:08,600
And so he gives a long story about when he made this determination, but for our purposes it's not really useful.

39
00:05:08,600 --> 00:05:13,200
The point is he had this intention to stay and be a benefit to people.

40
00:05:13,200 --> 00:05:27,320
So if all the monks left, then who would stay to teach and who would stay to care for and encourage and really support the meditators and the practitioners in the area.

41
00:05:27,320 --> 00:05:41,040
So Maha Kastapa knew that it was his, he and the Buddha were actually very close, and when the Buddha passed away, Maha Kastapa was sort of looked upon as the leader of sorts.

42
00:05:41,040 --> 00:05:51,760
So not to take the place of the Buddha, but to take over some of the role of guidance and direction.

43
00:05:51,760 --> 00:06:11,000
So when the Buddha left to go on his tour, Maha Kastapa was, he knew he was best for him to stay behind because otherwise there would be many things that left undone and when people looking to learn about the dhamma who wouldn't get to hear it and so on.

44
00:06:11,000 --> 00:06:22,680
When the Buddha said, this is, he told this long story about, well, it's all the story anyway about Maha Kastapa making this wish to be a benefit to people and to not go out of his way.

45
00:06:22,680 --> 00:06:39,960
And then the Buddha said that he pointed out using this verse, he actually pointed out that it's not that people who are mindful, who are enlightened and like an arahant.

46
00:06:39,960 --> 00:06:51,120
It's not that they are the ones, so they are not the ones who need to leave houses, they will need to leave their dwellings and need to be careful because they have no attachment.

47
00:06:51,120 --> 00:07:03,760
A person who has no attachment has no problems staying behind, it's the exact person that you want to have stay in one place and teach and to spread the teaching.

48
00:07:03,760 --> 00:07:15,480
So it kind of put the other monks in the place because the implication is, well, you guys have to come with me so I can take care of you and so you can not be attached to one place for too long.

49
00:07:15,480 --> 00:07:25,640
And so in Thailand you get this a lot, monks will wander and never spend more than one night or never spend more than one week in one place.

50
00:07:25,640 --> 00:07:37,680
I've talked amongst who spent the whole year outside of the rains, they wouldn't spend two nights in a row in the same spot, so they would constantly be moving.

51
00:07:37,680 --> 00:07:55,400
It's harder to do now. There are monks who do this. I'm not convinced that it's the greatest thing to do, but there's a sense that when you're still attached to dwellings, that can be quite useful to keep you on your toes.

52
00:07:55,400 --> 00:08:01,480
So this is our story, and the verse, which is fairly clear, but let's just go through it.

53
00:08:01,480 --> 00:08:11,840
So Satyamando un jandhi, that the mindful are not lazy as basically what's being said here because there was an implication that Makasabha was lazy.

54
00:08:11,840 --> 00:08:17,400
It just couldn't be bothered to go on a tour, why bother when everything is so comfortable here.

55
00:08:17,400 --> 00:08:23,960
The Buddha pointed out that even when mindful people stay in one spot, you can't call them lazy.

56
00:08:23,960 --> 00:08:32,440
They will never get lazy because they're mindful. A person who stays put, even a person who does nothing but walk and sit all day, shouldn't be considered lazy.

57
00:08:32,440 --> 00:08:39,000
Their mindfulness keeps them alert, keeps them energetic, keeps them from being lazy.

58
00:08:39,000 --> 00:08:45,240
So it's not necessarily the case that just because you stay in one place, you're not going to be energetic.

59
00:08:45,240 --> 00:08:50,360
The mindful are always energetic, never lazy.

60
00:08:50,360 --> 00:08:54,680
So those of you who feel like you're lazy out there, use mindfulness.

61
00:08:54,680 --> 00:08:59,480
You don't have to go running or you don't have to exert yourself, just be mindful.

62
00:08:59,480 --> 00:09:04,440
When you're mindful, you can be sitting still all day and still be considered energetic.

63
00:09:04,440 --> 00:09:10,200
Your mind will be alert, then you won't fall into the trap of being lazy.

64
00:09:10,200 --> 00:09:15,400
Nani ketai rama netite, they don't have love for dwellings.

65
00:09:15,400 --> 00:09:27,720
So it's not that they leave them behind, that they have to escape them, but that they have no love for them.

66
00:09:27,720 --> 00:09:32,360
And they will leave, and they're happy to go when it's time to go.

67
00:09:32,360 --> 00:09:35,960
So the second part says like a swan leaves behind a lake.

68
00:09:35,960 --> 00:09:37,560
The swan doesn't worry about the lake.

69
00:09:37,560 --> 00:09:42,440
The swan is not concerned with the lake or say, oh, that was a nice place, maybe I should stay.

70
00:09:42,440 --> 00:09:45,800
Swans when they leave, they leave.

71
00:09:45,800 --> 00:09:51,240
Swans are just dumb animals, but in the same vein, the swan is not worrying about the lake,

72
00:09:51,240 --> 00:09:56,600
or not loving the lake, or falling in love with the lake, or being attached to it.

73
00:09:56,600 --> 00:09:59,480
The swan goes and knows when it's time to go.

74
00:09:59,480 --> 00:10:04,200
In the same way, wise people are those who are mindful.

75
00:10:04,200 --> 00:10:12,360
They leave behind with the abandoned house after house, so basically saying,

76
00:10:12,360 --> 00:10:14,920
Mahakasupa has no problem leaving.

77
00:10:14,920 --> 00:10:21,160
He certainly has not attached to, I mean, Mahakasupa was one of the greatest of the Buddhist disciples.

78
00:10:21,160 --> 00:10:29,400
There's some traditions that say he's still alive waiting to give his robe to the next Buddha.

79
00:10:29,400 --> 00:10:37,640
He entombed himself in the mountain and went into a trance, and he's going to stay there

80
00:10:37,640 --> 00:10:44,040
for whatever thousands or hundreds of thousands of years before Mahakasupa comes.

81
00:10:44,040 --> 00:10:46,520
And when Mahakasupa comes, he's going to come out and give him his robe.

82
00:10:46,520 --> 00:10:51,960
I think it's a Mahayana tradition that says, or it's maybe just an Indian folklore or something.

83
00:10:51,960 --> 00:11:02,360
But he's highly revered, as he was apparently the tradition is, I think, that he

84
00:11:02,360 --> 00:11:04,440
aspired once to be a Buddha.

85
00:11:04,440 --> 00:11:10,440
But when he met our Buddha, he realized how difficult it would be and how much better he

86
00:11:10,440 --> 00:11:13,720
off he would be to learn from this Buddha.

87
00:11:13,720 --> 00:11:18,440
So he put aside his intention of it, and that was Mahakacaya, and I think that's a different

88
00:11:18,440 --> 00:11:25,720
monk, I'm confusing them. But no Mahakasupa was the hand of the sangha after the Buddha passed away

89
00:11:25,720 --> 00:11:29,960
or the father of the sangha is often called after the Buddha passed away.

90
00:11:29,960 --> 00:11:34,360
It's not exactly accurate, but that's sort of how he's looked upon.

91
00:11:36,440 --> 00:11:42,440
So he was a great finger in the sangha, and certainly had no attachments.

92
00:11:43,160 --> 00:11:45,240
But there's a couple of lessons we can learn here.

93
00:11:45,240 --> 00:11:53,480
The first is not to criticize people, not at all, criticizing others rarely does any good.

94
00:11:54,360 --> 00:12:04,520
Usually just feeds your ego and helps us wage any insecurities we have.

95
00:12:04,520 --> 00:12:07,160
You feel better bringing other people down a notch, not good.

96
00:12:07,160 --> 00:12:17,000
And it's often misguided, you know, people do things for reasons that if we thought about it,

97
00:12:17,000 --> 00:12:20,360
we might be able to empathize and understand why they're doing things.

98
00:12:20,360 --> 00:12:24,840
You know, the person who you're very angry at because of the evil things they've done well,

99
00:12:24,840 --> 00:12:28,040
who knows why they're doing them, probably they have evil inside of them.

100
00:12:29,000 --> 00:12:33,240
But they may be doing it because they feel hurt because they're in a position where

101
00:12:33,240 --> 00:12:37,800
they're not able to understand what's wrong with what they're doing and this kind of thing.

102
00:12:39,480 --> 00:12:44,200
And often they're doing it blamelessly, you know, and we get the constantly at Jantang,

103
00:12:44,200 --> 00:12:48,680
people would criticize him about the silliest of things.

104
00:12:50,440 --> 00:12:56,200
There was one monk who came and saw that at Jantang had all this wonderful food because people

105
00:12:56,760 --> 00:13:01,080
they adore him. So the food that they would give him is special, extra special food.

106
00:13:01,080 --> 00:13:07,880
And there was this one Western guy who ordained as a monk for a short time and he just was

107
00:13:07,880 --> 00:13:13,320
disgusted by his handle. What's he doing, eating all that good food? When we hear we have to,

108
00:13:13,320 --> 00:13:18,360
I mean, I think it came from the fact that he wanted some of the good food, but he just felt it

109
00:13:18,360 --> 00:13:28,760
was wrong and unfair. People criticize things. There's this story of this Zen story

110
00:13:28,760 --> 00:13:36,280
of these Western men, I guess, who became monks and they went to live with his Japanese monk,

111
00:13:36,280 --> 00:13:43,160
I think Japanese monk, Chinese monk maybe, and probably Chinese. And they were in the monastery.

112
00:13:43,160 --> 00:13:49,800
And every day the, to me, it was just one minute. In the monastery, every day the monk would

113
00:13:49,800 --> 00:13:55,560
bow down to this Buddha statue. And so the guy started doing it and then every day he saw

114
00:13:55,560 --> 00:14:00,680
every day we had to do bowing and bowing and bowing to this Buddha statue. And finally the guy got

115
00:14:00,680 --> 00:14:05,720
upset about it and he turns to him and he said, look, this is ridiculous. I don't want to bow

116
00:14:05,720 --> 00:14:12,040
down to this dumb statue. He said, I'd rather spit on that image than bow down to it.

117
00:14:13,160 --> 00:14:17,240
And the Chinese monk turns to him and says, okay, you spit all bow.

118
00:14:17,240 --> 00:14:26,760
Well, maybe sometimes we hold on to things quite strongly and we missed the fact that

119
00:14:27,720 --> 00:14:31,400
it's not wrong with the other person, what other people are doing. You know, they have reasons

120
00:14:31,400 --> 00:14:35,880
for it and we have to ask ourselves, this would have learned a lot in Thailand because I was

121
00:14:35,880 --> 00:14:42,120
that sort of way in Thailand criticizing this, criticizing that. But then I watched and I saw,

122
00:14:42,120 --> 00:14:47,480
especially Ajahn Tang, how uncritical he is and how accepting and how he just doesn't even

123
00:14:47,480 --> 00:14:53,320
comment on so many things. And then when he does comment, he finds the good in it.

124
00:14:54,680 --> 00:15:01,800
He finds a way to focus people's attention on what's good about it and to point out that

125
00:15:02,360 --> 00:15:06,600
sure he'll say sometimes like, you know, some people say this is just a ritual, but you know,

126
00:15:06,600 --> 00:15:10,440
it makes people feel good, it makes people mindful, it makes people think about the Buddha,

127
00:15:10,440 --> 00:15:16,360
et cetera, et cetera. And that really opened my eyes to, yeah, that's it. I mean,

128
00:15:16,360 --> 00:15:20,680
we're the ones with the unholesome mind. This guy, this guy was obsessed with spitting.

129
00:15:20,680 --> 00:15:25,000
Well, he was the one obsessed with the bowing. He was the one that got upset about it.

130
00:15:29,640 --> 00:15:34,760
That's the first thing we can learn is to not criticize others without knowing their reasons.

131
00:15:34,760 --> 00:15:39,640
You know, be careful. We should always worry more about our own faults. Not pretty

132
00:15:39,640 --> 00:15:45,160
strong, we know many, not pretty stung atagged. We shouldn't worry about the faults of others

133
00:15:45,160 --> 00:15:47,000
or the things they've done or left and done.

134
00:15:53,400 --> 00:15:59,400
The second thing is this point that I've sort of already made that, but it's a two-pronged point.

135
00:15:59,400 --> 00:16:10,200
There's the idea that people who are truly mindful don't need to do any special practice.

136
00:16:11,880 --> 00:16:15,800
And that's very much true. You know, a mindful person, there's this

137
00:16:15,800 --> 00:16:20,920
Zen thing about the Buddha riding in a Ferrari and wearing gold rings and so on

138
00:16:20,920 --> 00:16:29,880
because he wasn't attached to it. And so this kind of thing, you know, it's possible to go

139
00:16:29,880 --> 00:16:35,800
way too far with this idea because you say, well, okay, so a mindful person doesn't have to do

140
00:16:35,800 --> 00:16:39,080
any special practice. A mindful person doesn't attach the things. Well, then they could have

141
00:16:39,080 --> 00:16:43,480
sex and they could drink alcohol and they could have it do drugs and all these sorts of things

142
00:16:43,480 --> 00:16:50,600
that wouldn't hurt them. But the point is that mindful person wouldn't do these things.

143
00:16:50,600 --> 00:16:54,520
I mean, it's not to say that it couldn't happen. You can be raped or that kind of thing, but

144
00:16:56,120 --> 00:17:01,960
and you can drink something thinking it's not alcohol and wind up being alcohol or drugs or so on.

145
00:17:03,320 --> 00:17:12,040
But I once, I was once given these pills by this woman, I don't know what her deal was,

146
00:17:12,040 --> 00:17:16,520
but I don't really think she had bad intentions, but she gave me these pills saying it was like

147
00:17:16,520 --> 00:17:23,800
ginseng. That was ginseng, that's good for your mind. But when I looked it up on the internet,

148
00:17:23,800 --> 00:17:28,520
I took the pill and I swallowed it. She gave me this bottle and she said, be good for you as a monk.

149
00:17:29,480 --> 00:17:37,000
And so I took the pill, I swallowed it and as it was settling in my stomach, I looked on the

150
00:17:37,000 --> 00:17:43,080
internet to see what its characteristics were and it was an aphrodisiac.

151
00:17:43,080 --> 00:17:49,240
There it is. I guess that's the only one I'll be eating of that.

152
00:17:53,720 --> 00:18:00,760
Well, it's true that if you don't have the desires, then it shouldn't affect you. It shouldn't be a

153
00:18:00,760 --> 00:18:08,840
problem. But people use it as an excuse to do things that they want to do and claim that they're

154
00:18:08,840 --> 00:18:15,080
not attached. Yeah, I enjoy it, but I'm not attached to it, right? Well, it's a huge exercise in

155
00:18:15,080 --> 00:18:21,960
delusion and self-delusion that somehow you can be angry and not cling to it or be greedy and not

156
00:18:21,960 --> 00:18:31,560
cling to it. But it's certainly not the case. Anything that's mental, any of your mental

157
00:18:31,560 --> 00:18:37,080
judgment, that's what the problem or the problem lies. Anything that makes you afraid or worried or

158
00:18:37,080 --> 00:18:45,000
bored or sad, angry or greed or anger in its very base forms, they are

159
00:18:46,360 --> 00:18:55,160
and of themselves the problem. So you shouldn't use this as an excuse to stay put. In fact,

160
00:18:55,160 --> 00:19:02,600
often Mahakasabha may have stayed where he was, but he in the end went and lived up on the

161
00:19:02,600 --> 00:19:09,560
on the hill, on the mountain above Rajakaha. I think after the Buddha passed away,

162
00:19:10,200 --> 00:19:16,280
yeah, that's where he was staying. That's where they did the first council. And they invited him

163
00:19:16,280 --> 00:19:20,520
down. They wanted him to come and stay in the city because he was the big monk and he was getting

164
00:19:20,520 --> 00:19:26,680
old and they said, you know, we need you down here to support us and he refused. Every day he

165
00:19:26,680 --> 00:19:32,520
would go back up, walk all the way up. It's quite a walk. Every day he would walk up and live up on

166
00:19:32,520 --> 00:19:39,320
the mountain. Apparently he'd never let limber laid down either. He would always walking in the

167
00:19:39,320 --> 00:19:46,360
city and never lying down throughout his life or his later years anyway. But I think throughout

168
00:19:46,360 --> 00:19:57,720
his monastic life. So yeah, I don't take, so it's not like he was just living in the lap of luxury

169
00:19:57,720 --> 00:20:03,000
and not clinging to it. And they asked him why and he said it was out of compassion for Heather

170
00:20:03,000 --> 00:20:09,480
so that they were able to see the good example and because it's much more peaceful. It's just

171
00:20:09,480 --> 00:20:15,400
the right thing to do, living in the forest, living alone, living at peace. There's no sense of that.

172
00:20:16,040 --> 00:20:21,320
But one should not be attached to these things nor criticize someone. I mean a lot of criticism.

173
00:20:21,320 --> 00:20:26,760
There was this big criticism that went around recently about these monks who had their own private jet

174
00:20:26,760 --> 00:20:33,960
and were sunglasses. And at first I understood that that was the criticism and it didn't really

175
00:20:33,960 --> 00:20:39,640
phase me at all. I'm like, okay, well, it's possible that the monetary could have a jet for

176
00:20:40,600 --> 00:20:47,080
a bit outrageous. But my point being, those things in and of themselves don't necessarily because

177
00:20:47,080 --> 00:20:52,440
they're tools. Even sunglasses, lots of monks were sunglasses. I had Jan Tong were sunglasses.

178
00:20:52,440 --> 00:21:02,120
Why? Because he had eye surgery and the light or has been or was very sensitive to sunlight.

179
00:21:03,160 --> 00:21:11,080
And I know lots of monks in Thailand who wear sunglasses. I don't really think it's that great

180
00:21:11,080 --> 00:21:16,600
of an idea, but it's not even of itself a kind of huge corruption. I mean there are far

181
00:21:16,600 --> 00:21:22,840
worse things that can and probably do go on that we're not really aware of. But

182
00:21:24,520 --> 00:21:32,520
it'd be careful not to judge things by appearances or etc. I think it turns out that those monks

183
00:21:32,520 --> 00:21:37,880
were selling drugs or doing drugs or they had women or this or that. So yeah, that's something

184
00:21:37,880 --> 00:21:47,720
you can definitely criticize them about. But it's an interesting quote. I mean it's on the face,

185
00:21:47,720 --> 00:21:57,720
it's a sign of the importance of not clinging to our place, our possessions, not clinging

186
00:21:58,520 --> 00:22:04,920
to be mindful. But deeper down and if you know the backstory, it has more to do with the idea

187
00:22:04,920 --> 00:22:09,640
that you don't need to be too concerned about going off into the forest. Oh, how can I

188
00:22:09,640 --> 00:22:13,720
powerful the Buddha's teaching living in the home life? Yeah, it's more difficult. Yeah,

189
00:22:13,720 --> 00:22:19,640
you're going to be tested and tried, but it can be done. And we shouldn't be clear about two

190
00:22:19,640 --> 00:22:24,120
things. One, that it can be done. And two, that unless we're really, really mindful, it's much

191
00:22:24,120 --> 00:22:29,400
easier to get attached. So you have to be more on your regard, more vigilant when you're living

192
00:22:29,400 --> 00:22:36,040
in lay society, when you're living in a house, much easier to be mindful. It's actually kind of

193
00:22:36,040 --> 00:22:42,920
cheating. It's easier. It's an easier way to go off in the forest, where we think all going

194
00:22:42,920 --> 00:22:46,520
up in the forest, that's difficult. It's not really difficult. Living in the forest, there's nothing.

195
00:22:47,240 --> 00:22:55,240
It's just simple and easy and it's wonderful. Anyway, so that's the Dhammapada for tonight.

196
00:22:55,240 --> 00:23:01,160
Thank you all for tuning in. We're wishing you all good practice and that you all become

197
00:23:01,160 --> 00:23:31,000
free from suffering.

