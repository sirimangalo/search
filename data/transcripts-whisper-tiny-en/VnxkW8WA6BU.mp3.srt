1
00:00:00,000 --> 00:00:09,480
Here's a part of the monastery that I haven't seen yet.

2
00:00:09,480 --> 00:00:18,000
This is an old Quitty Cave Quitty that we haven't been able to get to until today.

3
00:00:18,000 --> 00:00:22,000
I haven't been brave enough to come to.

4
00:00:22,000 --> 00:00:27,000
It's an old broken down Quitty.

5
00:00:27,000 --> 00:00:32,000
Here we're maybe going to do three Quitties.

6
00:00:32,000 --> 00:00:35,000
At least two, but maybe three.

7
00:00:35,000 --> 00:00:38,000
First we have to clean this out.

8
00:00:38,000 --> 00:00:40,000
This is one jewel.

9
00:00:40,000 --> 00:00:46,000
Looking hard.

10
00:00:46,000 --> 00:00:51,000
This is below the upstairs cave where I was staying.

11
00:00:51,000 --> 00:00:57,000
And moving hard as up there.

12
00:00:57,000 --> 00:01:00,000
This is the next project to put some Quitties here.

13
00:01:00,000 --> 00:01:05,000
This is a hot place for meditators to stay.

14
00:01:05,000 --> 00:01:08,000
This is a great place.

15
00:01:08,000 --> 00:01:11,000
A good place to meditate.

16
00:01:11,000 --> 00:01:16,000
Okay, so another video I'm updating on.

17
00:01:16,000 --> 00:01:17,000
Well, I don't know.

18
00:01:17,000 --> 00:01:20,000
Talking more about our forest monastery.

19
00:01:20,000 --> 00:01:24,000
I'll try to do another couple of I have three videos.

20
00:01:24,000 --> 00:01:27,000
Ask among videos in mind.

21
00:01:27,000 --> 00:01:38,000
I've just been caught up in working and directing because you need someone to direct work.

22
00:01:38,000 --> 00:01:41,000
All right, it doesn't get done.

23
00:01:41,000 --> 00:01:45,000
Hopefully I'll get back to doing some videos soon.

24
00:01:45,000 --> 00:01:47,000
Try to be patient.

25
00:01:47,000 --> 00:01:51,000
Remember, the videos are only for the purpose of practicing meditation.

26
00:01:51,000 --> 00:01:56,000
So for videos, you better be meditating.

27
00:01:56,000 --> 00:01:59,000
Better off meditating.

28
00:01:59,000 --> 00:02:15,000
Video, two minutes.

