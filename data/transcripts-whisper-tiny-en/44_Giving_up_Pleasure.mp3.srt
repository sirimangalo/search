1
00:00:00,000 --> 00:00:07,600
liking, question, and answer. Giving up pleasure. Do we need to give up all materialistic pleasure?

2
00:00:08,880 --> 00:00:15,040
No, you do not have to give up any pleasure. This is really key. You have to give up your

3
00:00:15,040 --> 00:00:20,880
attachment to pleasure, and that is the difference here. A person can experience pleasure without

4
00:00:20,880 --> 00:00:26,080
being attached to it. There are many mental and material states that do not stem from greed or

5
00:00:26,080 --> 00:00:31,920
coexist with attachment. Attachment is a sort of stress that brings the mind out of its state of

6
00:00:31,920 --> 00:00:38,960
peace and happiness. If you engage in attachment or addiction, wanting, desire, etc., you will only

7
00:00:38,960 --> 00:00:45,040
become increasingly miserable as you compartmentalize reality into good and bad. You will not

8
00:00:45,040 --> 00:00:51,440
always get what you want for the very reason that you have once. If, on the other hand, you are

9
00:00:51,440 --> 00:00:56,560
able to accept reality for what it is, then it is not a question of whether there is pleasure or

10
00:00:56,560 --> 00:01:01,840
pain at all. You will be able to live in harmony with the world around you and in harmony with

11
00:01:01,840 --> 00:01:08,080
reality as it is. Accepting change, accepting the good and the bad, and not labeling things as good or

12
00:01:08,080 --> 00:01:14,160
bad. It is important to understand that you do not have to give up anything at all. The point is

13
00:01:14,160 --> 00:01:19,600
that you are suffering and you are likely not clearly aware of your own suffering. Most people

14
00:01:19,600 --> 00:01:25,440
are unable to see the extent to which their state of existence is conducive to stress and suffering.

15
00:01:25,440 --> 00:01:30,880
Ultimately, we are all suffering. Once we realize that there is suffering, we begin to want to

16
00:01:30,880 --> 00:01:35,680
find a way out of it. So we start looking and once you start looking in earnest, you begin to

17
00:01:35,680 --> 00:01:40,240
realize that the cause of suffering is really your attachments, attachments to pleasure,

18
00:01:40,240 --> 00:01:46,000
attachments to being, and attachments to not being. Our attachments are a cause of suffering

19
00:01:46,000 --> 00:01:51,680
because we are not able to accept reality for what it is, not able to accept the experience.

20
00:01:51,680 --> 00:01:55,520
When you realize the stress that you are causing, you let go automatically.

21
00:01:56,160 --> 00:02:01,520
If you see clearly something as causing you stress, there is no thought or intention involved in

22
00:02:01,520 --> 00:02:07,680
the response. It immediately changes who you are. Your behavior changes so that you do not repeat

23
00:02:07,680 --> 00:02:12,480
your mistake. You would never do something when you have verified for yourself that it causes you

24
00:02:12,480 --> 00:02:18,720
suffering. That is the nature of the mind. We only act the way we do because we think it is going

25
00:02:18,720 --> 00:02:24,560
to bring us pleasure, happiness, or some sort of benefit. You do not ever have to worry about

26
00:02:24,560 --> 00:02:30,000
having to give something up, thinking, oh, I am going to have to give up this or that. The point

27
00:02:30,000 --> 00:02:34,960
is to look closer and see more clearly and give up those things that stop you from seeing clearly.

28
00:02:35,520 --> 00:02:39,840
Of course, give up drugs and alcohol because you cannot see objectively when your mind is

29
00:02:39,840 --> 00:02:45,040
intoxicated. Give up entertainment to the degree that it impedes your clear investigation of

30
00:02:45,040 --> 00:02:50,080
reality. If you do not give it up, you will not be able to undertake scientific inquiry.

31
00:02:50,080 --> 00:02:55,040
Once you have done this, start to look. Do not take it on faith. Just start looking.

32
00:02:55,680 --> 00:03:00,640
The better you understand reality, the more you will give up until finally you give up everything

33
00:03:00,640 --> 00:03:06,720
as a matter of course. There is no forcing, pushing, or pulling involved at all. It is wisdom.

34
00:03:06,720 --> 00:03:11,840
Once you see things as they are, you do not want to cling because it is not a belief,

35
00:03:11,840 --> 00:03:41,680
nor is it a theory. It is understanding, understanding that no happiness comes from clinging.

