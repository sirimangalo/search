Hello and welcome back to our study of the Dhammapana. Today we continue on with
verse 167 which reads as follows.
The English is what it means is in English. One should not pursue inferior
dhamma, hina dhamma. One should not engage in negligence, pamadehana, sambhase.
One should not associate with wrong view, meets edithiness area, and one
should not be a person who holds up the world who thinks highly of the world basically.
Four parts to it. Four things one should not do. One should not associate with base or inferior dhamma.
One should not associate with negligence or get involved in negligence. One should not
get involved in wrong view, and one should not exalt the world. Think highly of it.
The story is a bit of an interesting story to this. It's not long, but again it's hard to tell
how it relates to the verse you wouldn't guess. It's related, but only ten
generally. The story is a young monk and an elder who went on arms round in
sauwati. They happened upon the house of this great late disciple, Wisaka. Wisaka was
one of the greatest late disciples of the Buddha always looking after the monks and teaching
her fellows and practicing up right justly and keeping moral precepts and so on. Just an all
around wonderful person. There's stories about her as well. She was constantly one of
these people who every day prepared food for any monks who went by. In Buddhist countries
you see this happening now that Buddhism has become a force and something that's very important
in various cultures of the world. You see people doing this. Putting out food every day,
always saccharose. One of the first people are one of the early Buddhists who gave daily
offerings as the monks would go by her house. She would invite the men and of course she
was quite rich, so of course she didn't every day come out herself. On this day, her grand
daughter was looking after arms and so the elder and the novice went in, or the novice
the elder and the young monk went in. First they received some rice gruel, rice porridge
that they offered in the morning. Then the elder left and went to get arms elsewhere
and left the young monk to receive arms in this house. This young woman, this granddaughter
who was looking after the monk, brought out food I guess, and at one point she was straining
water because a monk is not supposed to drink water that has not been strained properly
in order to be sure that there's nothing living in it and that sort of thing. It's just
one of those things. It's actually not really clear the, it's not especially a significant
rule in the sense that everyone should be worried about whether you're going to kill bacteria
by drinking water. That's not been filtered, but it was sort of more in order to avoid
criticism anyway, not terribly important. She's looking in this vessel that has the
filter, the strainer in it, this primitive filter, and she sees her face. She's just
a young girl, I guess, and she laughs. She's right in front of the monk, I guess, and the
monk looks down into it and he laughs as well. I guess it was kind of funny to see the
girl's face in the, in the ball. When he laughs, she looks up at him and she says, a cut
head is laughing. I guess it was surprising to see a monk laughing she had had monks come
in. Monks, one thing monks aren't really supposed to do is laugh. There would be a sort
of especially in, when going in and in a monk houses, they have to maintain a certain decorum,
a sense of propriety. She would have been familiar with all these serious monks who came
in and were very quiet and maybe smiled, maybe spoke quietly, but laughing. She saw this
monk laugh and she said, I could call them a cut head. Somehow she had this, I don't
know exactly if there's some, I was trying to find a better translation, but it really does
literally mean cut head. One who's head is cut. It's like a haircut, right? The point
is that he has his head is shaved, but I think it perhaps was as weird sounding to him
as it was, as it is to us, because you don't ever call someone with someone cut head unless
they actually have a cut and are bleeding or this literally, this would better mean that
someone who had their head cut off perhaps, but that's not what you meant. And the monk
got really angry and he abused her and said, by saying, you are a cut head. Your mother
and father cut heads too. He was quite vicious about it because she ran off crying and went
to see Risaka. She was terribly upset, perhaps worried that she'd said something wrong
and feeling just terrible for what she'd said, feeling terrible because the monk reprimanded
her or insulted her, attacked her basically. And Risaka asked the little girl what's wrong
and she told Risaka what happened. Risaka went down and said to the monk, Reverend
Sir, don't get upset at this young girl. You misunderstood what she meant. She didn't
mean anything bad by it. It's a measure of respect. She respects you for having your hair
shaved, basically. And the monk says, well, that may be so, but was it proper for the
girl to speak this way, proper for her to call me by such an insult, basically. So he
still took offense at him. He didn't have any real argument against it. I'd say he was
very much in the wrong for attacking this young girl who clearly didn't mean anything
by it. So then they were arguing and then the elder comes in and says, what's wrong? What's
going on? And when he heard the facts, he turns to the monk and he said, up he, he says,
get out of here. That was no insult. A monk who has their haircut is someone who is a
cuthead who's five. Hold your peace. And again, he says, well, what are you doing attacking
me? She's the one who basically arguing that he had done nothing wrong with this young
girl. He said, what are you doing calling her and her family a cuthead? Was she called
me a cuthead, basically? That's what it seems like. And then the Buddha came in. Some had
the Buddha was on his own turn as well, or he must have come to see Wisaka every day or
regularly. Some other Buddha came in and said, what's going on here? I would say I could
tell some the whole story from beginning to end. The teacher does something, and this
was, I think, what's interesting about the story. There's something to say here. The teacher
instead of rebuking the monk, he looks at the monk and he sees some potential in the
monk. And so instead of doing what my would assume would be proper, or you might think
is quite proper to rebuke this monk, he turns to Wisaka and he says, what's the meaning
here? Why don't you admit that? Is it proper for your granddaughter? Just because my disciples
go about with their haircut to call them cutheads, he turned on Wisaka. It's very interesting.
Again with these stories, you can never really be sure how authentic they are, but regardless
there's something to be said here. And the monk suddenly feels good about himself. He
becomes appeased by the Buddha's speech, and he says, only you correctly understand, only
you're wisdom. In your wisdom, you see through, you see the truth here. And then, so
now he sees that the monk's more at peace than he teaches him this verse. He says, an
attitude of ridicule with reference to the sense, this is a low attitude, an attitude that
is low, or not never to take, nor should one dwell together with heedlessness. How many
spoke this verse? So it's a redirect. The verse isn't actually speaking to the situation
exactly. It's something that we see with a lot of these stories and verses. A cynic
might say, well, these stories are just made up and that's why they don't fit quite well
with the verses, but I think that's a poor reason to think that the verses or the stories
are just made up, because that's what the Buddha would do, is he would redirect. It wasn't
of concern to him, who was right. And what's the Buddha interested in this argument, who's
right, who's wrong? And that speaks very much to what I think is the lesson of the story,
an interesting for us, is that for someone to become studied in the Dhamma, for someone
to progress in the Dhamma, their mind has to be steady. They can't be upset. You can't
be full of self-righteous indignation, for example. In this case, this monk was full of anger.
And more of the anger isn't so much a problem, but the reaction, the interaction with
the others, the fact that it became an argument, that it became a fight. We have this verse
up here on our wall, because I think it's quite an important verse about how getting
angry is one thing, but a person who responds with anger creates the real problem, does
the greater evil. And so when this monk was angry, everyone getting angry back at him was
not the proper response. Trying to show him that he was wrong was clearly not working and
it wasn't helpful. They may have been right. They were right, I think, that this monk
was in the wrong by attacking this little girl. I mean, he should have known better.
It's quite curious that the Buddha would, because it's not something that the Buddha insisted
he was much harder on the monks, right? It was much more important that the monks behave.
And he was very lax about trying to control ordinary laypeople. But in this instance,
especially because we saw it go as far more advanced than this monk, he had no problem because
it was a means of redirecting the monks' energy, so that the anger would be a swaged
or reduced giving him the opportunity to deeply understand the Dhamma, in relation to
the situation. There was a lot of unwholesunness involved in the monks' state of mind
that had to be fixed and had to be addressed. So what that speaks to, I think, is the need
for us, again, the need for the mind to be in the right position, right? Any clarity
of mind that you can't just, it's not enough to attack our defilements, right? This is
why mindfulness is very much about observing rather than attacking. This is sort of speaks
to the idea that opposition to our defilements is of limited use, if any, right? If you
get upset about or try to remove or try to fix your mind, fix the problems of the mind,
why it doesn't get anywhere, or speaks to the fact that it doesn't get anywhere. It's
important for us to understand this quality of meditation practice that it's about observing
and about neutralizing, really, that if anger comes up, it's not about opposing the anger
and getting upset about it, right? Internally, it's the same as externally. Here we had
an external attack on the monks' defilements. He was angry. Everybody was getting angry
at him and it just got worse and worse. But as meditators, it's quite, it's along the
same lines, you know? As meditators, both externally and internally, we have to find ways
to neutralize, not attack, not fix, right? Here's an attempt to fix this monk, fix
him, he's wrong, right? It speaks to how we should engage in relationships rather than trying
to win arguments. Winning arguments should never be the goal. The goal should be fixing
arguments so that there is no argument, so neutralizing the argument. There was this, I read
this story about a famous actress, I think, who was attacked by, was called some very
nasty name by some other person on the internet. It was this interesting, it was a whole
news article. Instead of ignoring him or whatever, she went through his history, his social
media history, and learned about him and responded back to him with compassion, with understanding.
And you would wonder, well, why would she waste her time on this guy? But in the end,
it's a long story, but he opened up to her and eventually apologized, and she found
out he had a bad back, and she ended up donating money, or finding him away for him to
get help with his back, and just the story went on and on about how they eventually
became friend. Then it was a really good example of how, you know, it's easy to talk
about someone, it's easy to label someone, and pigeonhole them as being evil, and say to
yourself, well, the Buddha said, don't get involved with those people. That's just pigeonhole
them. They're the bad ones, and let's stay away from them. We want to be good people.
Clearly, the Buddha didn't do that sort of thing. We have many examples of that, and
it's not the best way forward, not in terms of our practice, our development as individuals.
One of the big things about Buddhism is coming to terms with your karma, which means
with your situation, and whatever it might be. We're all, we've all been through all sorts
of situations. It's not that this situation is somehow special for us to learn from, but
it's about gaining the attitude of non-reactivity, of objectivity, of equanimity.
And so a big part of that is when violence or unholesomeness comes to us, that we don't
feed it, that we don't react, that we don't contribute to it. I think that's an interesting
example of that, or the Buddha found a way to, again, the story is kind of quirky, but clearly
the lesson is to find a way to appease and to neutralize the situation.
The bigger teaching here is in the verse itself, of course. The verse, I think, is one
that we should keep a tab on, maybe bookmark it, and if you're that sort of person, something
to come back to in the Dhamapada, because it talks, it's a good summary of the four aspects,
or four aspects, I'm not sure if it's meant to be comprehensive, but it seems fairly
comprehensive. The four aspects of unholesomeness of the problem that we're trying to
address, and the real problem. And so the four are Hinadama. Hinadama is base things,
base experiences, and this is sensuality, this is sensual desire, desire for beautiful
sights, lovely sounds and smells, delicious tastes, soft, comfortable, wonderful, pleasant
feelings. So this simply relates to our desire for these things. We want these things.
And the Buddha said, don't go after them. That's quite simple. From a meditative point
of view, I think this one is probably the easiest and the most clear, but what we're talking
about with all of these is an answer sort of to the question of why we suffer. We come
to practice meditation or religion in general. It's quite often because we're looking
for peace. We're looking for happiness. We're looking for some higher way out of the
life that we live. We're looking for some sort of greatness. We come with, we see the suffering
in life. Why am I not happy? Why am I not satisfied with life? I'm a big part of that.
So a big part of that is sensuality that we think it makes us happy. We chase after pleasure.
It brings us a brief moment of satisfaction and then it's gone. And if we're lucky, we
can keep chasing after it. We can chase after pleasure as much as we want until we get
unlucky until things change. And then we realize what it's actually done to us. It's made
us super sensitive to the senses, to, if it's visions, you see beautiful things, then
you become super sensitive on the one hand where every time you see something attractive,
you jump at it. You become increasingly sensitive to pleasurable stimuli. But on the other
side, you can see once you don't get what you want. And that's part of what you see here
as meditators because there's very little stimulating. How needy you are for that pleasure
when you don't get it. How your mind starts to make things up. And you become tormented
by not getting what you want. The meditation is a lot of discomfort in the beginning.
The meditator can't get what they want because there's so much one thing. Because our ordinary
life is all about chasing after or jumping from one pleasure to another and finding
ways. You know, we find ways to live our lives that allow us to engage in constant pleasure
by jumping, jumping, jumping. And now you come to meditate and you're not allowed to jump
anymore. That's where the suffering comes from. There should be no suffering in sitting
still and doing nothing because it's quite peaceful. It's free from so much trouble. And yet
the trouble comes from within. Her mind creates the trouble. But I said, these are base things,
these desires that we have are based because they create addiction. They don't actually,
they aren't some sort of happiness. You know, I had an argument recently about this. Someone
was telling me, well, it makes me happy, period. And then I said, well, you think it makes
you happy or something like that. I was just sort of idle and wasn't really interested in
getting into religious or philosophical or Buddhist debate of any sort. And the conversation
went on and I wasn't really involved so much in it. But at one point, they said, well,
it's not, I don't know how we got into, it's not black and white. Anyway, I said something.
And then this person got really angry and stormed off and another person, someone else
came up and said, you know, they're really angry and they're really upset. I don't know what's wrong.
I was thinking about it. I thought, you know what? If it really misses the point, the
point is it doesn't make you happy. These things, the sensuality doesn't make you happy.
That we were, I mean, maybe not explaining it, but I'm trying to be a fairly vague, but
this person was basically positing the argument that sensuality and these things make
us happy. And without getting into an argument because this person wasn't at all interested
in Buddhism or meditation, it's clear to see that they weren't happy. They were the sort
of person who gets angry easily. That comes from sensual desire, it comes from things
like killing and stealing and so on and so forth. It comes from the many states of mind
that encourage partiality. Encourage us to become content only with a specific subset of
experience or become dependent on certain experiences. So the first is not to, we shouldn't
engage in these things. We shouldn't engage because it's inconsistent. We want to be happy
and yet we engage in these things that aren't making us happy. You find this is one of
the big things about meditation practice. Once you finish a meditation course, all the
stress and suffering. And then you leave and you feel how peaceful you are. You notice
the change. It's like going through withdrawal. And you start to see the difference between
seeking pleasure, seeking happiness through pleasure and learning to be happy. You start
to see how it means to not find happiness outside of yourself. But the only thing that
we gain from seeking out pleasure is addiction, the need for it and displeasure when we
don't get it. So the second thing is heedlessness or I said negligence, right? Negligence
refers to lack of mindfulness. So speaking of pleasure, we can talk about, as I said, pleasure
is something that doesn't satisfy and when you don't get it, then you're disappointed.
But the interesting thing about mindfulness, this is what we mean by negligence. Negligence
means not being mindful, not being aware, not being present. Is that once you're present,
there's really, it's really a non issue because pleasurable things aren't desirable. There's
no connection between a pleasure, this is what we don't, it's hard to understand is that
there's no connection between a pleasurable state and desire for it. Just like pain. There's
no reason to be upset at pain. There's no reason to be attached to desire. I mean,
or to be attached to pleasure. Why do we think one is more desirable than the other?
That may seem hard to understand, but through the practice of meditation, it's not even
an intellectual thing because you're aware, when you're aware and alert, pleasurable
and no desire arises based on the pleasurable stimuli. And really, if you think about it,
that's how it should be. There's no reason for us to desire pleasurable. There's no logic
behind it. We say, well, it's pleasurable. I mean, what does that even mean? The truth
is it's an experience. Pleasure is an experience. Pain is an experience that we should desire
one and be averse to the other is actually artificial. And you can see that through meditation
as you practice. It's like it wasn't even, it's like, it never even, it didn't ever
exist. When you feel pain and you say pain, pain, there arises no aversion to the pain.
If you ask an enlightened person, if an enlightened person were to think about it, it would
be like, it would be completely nonsensical to be attached to. It wouldn't compute that
one should desire a pleasant experience. I mean, there really is no argument for it. And
that's not an intellectual thing to say. That's from experience. You experience that. When
there's pleasure, you say happy, happy, pleasure, pleasure. No desire comes. When you see
something that normally you'd like that, you say seeing seeing no desire comes. There's
no reason for it. And the Buddha's instruction here is to try and be mindful in the second
one. One should not be unmindful because it's unmindfulness that leads to, it's like being
in darkness or being blind. If you're blind, you walk around and you bump into things. That's
an analogy for what we do. It's not that we mean to do something that causes us suffering.
We're like more blind. All of our addictions, the reasons why we get build up all sorts
of habits of aversion and addiction. The reason why we become bad people, the reason why
we become good people is often just because we're blind, because we're walking around
blind. We hear some noise and we think it's something, I think it's this and it turns
out it's something else. We're guessing for the most part. We're living our lives
guessing. You think about it. Why we do those things that we do? Much of it is just
guesswork. We say intuition sometimes, but sometimes it's just chance. We fell in with
certain people and that's why we are the way we are. We learn from each other. We learn
from each other. It sounds better than it actually is because sometimes someone will say something
and it'll be taken as meaningful by another person. If you see me do something and maybe
it's something I do and then I think I feel the owl that was terrible. I'm never going
to do that again and you see me do it and you think oh that person does that. Or we ascribe
meaning to mouth. We ascribe meaning to things that don't necessarily have meaning. We go
around blind. We have very little, basically we have very little reason for gaining the habits
that we do. Often it's because of trauma that we become the people we are. Trauma or
any kind of intense experience. It's very dangerous, of course, because what does it take
to become an evil person? What does it take to become a person who has lost in addiction?
What does it take to become a person who has addicted to drugs or any number of things?
For most of us, those people who don't meditate, it's just chance. Look at those people
who are addicted and you say oh I never become such a person or those people who are evil
and you never become such a person. Most likely in the past life you already were. All
of those people. But even in this life you look and it doesn't take a special person.
It just takes a chance and circumstance. Of course in karma but karma is just temporary.
Karma is not the be all end of existence. If not in this life, some future life we could
all become evil, evil people. This is what leads us to desire things that are not worth
desiring and be of reverse to things that there's no reason to be upset about. And gain
wrong views, which is the third one. Number two first is not the heedless or negligent
to be mindful. To live our lives mindfully is really the best way. The second part of
the equation is this blindness or heedlessness. What means to all sorts of bad stuff?
The number three is wrong views. Wrong view is a special type of evil. When we say evil
and Buddhism we say evil and we mean it's sort of in the utilitarian sense. Evil is something
that causes you suffering. It leads to suffering. That's why it's evil. All of your views
are special because the view itself doesn't make you suffer. It makes you do things, right?
It was I think Pascal said if you want people to commit atrocities, what is it? The easiest
way to make people commit atrocities is to have them believe absurdities. Not exactly
but it points to wrong view. If you believe something, the belief in something. There are
wrong views that aren't dangerous. Like if I believe London is the capital of France, it's
not really a dangerous wrong view but it's wrong nonetheless. My opinion that or if I have
an opinion that it's warm outside, it's hot outside. Well, okay, we're starting to get
a little dangerous because I might walk out in my underrobes or something. But you see
the inherent problem with wrong views is that they're out of line with reality. The Buddha
talked about several. It's clear that wrong view is only limited by the number of facts
there are which of course is infinite. Facts that you can get wrong but there are certain
facts that have special significance. The Buddha was quite concerned about only a few
types of wrong view. There are mundane wrong views like the view that there is no cause
in the fact or the wrong view that there is no life after death, for example. These are
wrong views because they're out of touch with reality. It's just not true. It doesn't
matter whether there's no evidence, people say there's no evidence for rebirth. I mean,
I don't believe that but it doesn't really matter. See, the point we want to make, the
point I want to make here is that it doesn't matter whether you have evidence. If it's
wrong, it's wrong. The question is whether it's true or not. Is it true that when you
die the mind ceases to exist? If it's not true, then it doesn't matter whether there's
no evidence, evidence or no evidence. The science is quite clear. It understands this
but it's adamant that the best way still is to go, well, first you use Occam's razor
to find the simplest solution. But you go by the evidence, what evidence there is. If
we don't go by evidence, it's really hard to know what is true and what is not true.
But cause and effect is one big one. Because a person who doesn't believe in their consequences
of their actions, for whatever reason, who thinks that they can just get away with whatever.
We have really in relation to the afterlife, it's very important because if a person believes
that, believes that when we die there's nothing, then all you have to do is, I mean, it
leads people to believe things like they can just chase after pleasure eternally, that
they're never going to have to face the consequences of their actions. If you don't
like someone, just kill them. If you want something, just take it. There's another, I mean,
on a much more basic level, I think this is true that to a certain extent, this is what
leads certain people to do bad deeds is the belief that when they die, that's it. But on
a more basic level, they're also missing the fact that it doesn't actually make you happy
to live your life, like you're going to die, like when you die there's nothing. Because
a person who lives in such debauchery and lives their life uncaring about the consequences
is going to build up such an unwholesome mind that they won't be happy even in this
life. The cause and effect is a big one, karma, basically. Belief in things like gratitude,
the Buddha talked about gratitude, belief that your parents are worthy of your respect
for the most part. I know there are certain parents who's hard to find much to respect
about them. I mean, you could argue, hey, your mother carried you for nine months, that's
something. It's a big something, you know, for nine months. Anyway, for many of us we
have had parents who took great care of us. And I see people who have had great parents
not give a thought to any sort of gratitude. And I mean, that may sound if you're not
a meditative, that may sound surprising that we should place any emphasis on that. But
it's interesting how as you meditate naturally, this sort of gratitude comes out and you
think of all the people who've helped you. You think of all the people you've heard becomes
a very strong and very important without any sort of, you know, religious indoctrination.
You just start to remember all the bad things you've done and all the good things people
have done for you. And you think, God, I gotta go find that person and think that. And
your parents, how keenly you feel, generally, if they've not been terrible, terrible people,
the good that they've done for you. But the most important wrong view is in relation to
the four noble truths. If one has the view that craving doesn't need to suffer, one has
the view of view of self. That's another big one, right? One has to believe that there
is a self, that there's a soul that I am in control, belief in God, and be another one.
I don't have to get into much into those wrong view in meditation, basically, to summarize
it. Wrong view is, through not seeing reality as it is. So our meditation is very
much focused on the eradication of wrong view through seeing clearly. Through the practice
of meditation, of course, cause and effect is something you see keenly. Non-self is something
you see quite clearly. You can't even control your own mind of alone your body or the
things of the people around you. Further, you find no soul, no self. And you realize
that it's even the wrong way of speaking about things. It's not a question of whether
there is a self or I have a self or I exist or so on. Because none of those questions
are or concepts exist in experience. Experience is just moments of seeing, hearing, smelling,
tasting, feeling, thinking. Any thought about a soul or a self or God has no place
in the realm of experience. A meditator looks at the world in a different way and look
at it from a point of view of the most basic, what I see or I see there is seeing, there
is hearing, so on. And so they give up any kind of view really entirely. The only view
that a meditator holds too strongly is, it is what it is. This is seeing, this is hearing,
and they see their view is that this is seeing. When they hear their view is that this is
hearing, then that's it. Number four, Lokawadhano. Lokawadhano is a special kind of, they're
all special, but I distinct from the other three. Lokawadhano is someone who holds the
world up, who exalts the world, who sees the world as a good thing. So the Buddha encouraged
his followers, especially his monastic followers, to give up the concern for the world,
the attachment of the world. We have so many ambitions and goals. We think of all of the
ambitions and goals that we have in the world. Some of them are so ingrained that we don't
even notice them. Like our concern for politics, our concern for our country, how's my
country doing? Our concern for our future. And by our future, I don't mean in a philosophical
sense, I mean, our employability, our upward mobility, are we getting, are we progressing
in life, you know, our status in society? How other people look at us? How my family
sees me? How my friends see me? How other people see me? Am I famous? Am I rich? Am I
smart? Am I beautiful? Am I fat? Am I thin? So many things about the world internally about
us and about our position in it. Concern about the environment. Environment is a curious
one from a Buddhist perspective, because I think the best thing you could say about it
is it's a sign. The degrading environment is a sign of an increase in greed and increase
in attachment to the world. When we talk about progress, everyone is raving about the
progress of humanity and actually how things are doing better. Things like crime are down
and poverty is down and the things in the world are actually supposed to be getting better.
But it's hard. There's this niggling feeling that something's not right there. The environment,
I think, is sort of a canary in the mind-shaft telling us that I agree there. I think there
might be some really good things increasing in the world. Of course, there's the glaring
or the obvious evils in the world that seem to be increasing or at least not going away.
Statistically, there's a lot of good things. If you look at technology, how it's allowed
us, given us power that we didn't have before, supposedly, it seems. But it's curious,
it's interesting to think about, is it really giving us power? I don't have to, I don't
have to memorize the diptica because it's on disk here. But is that really a power? I mean,
the power to memorize the diptica is nearly gone. The Buddha is teaching. I don't think
there's hardly anyone in the world who can anymore. We're getting lazy. But more to the
point here, we're becoming much more physical, much more worldly. Technology is a huge
sign that we're advancing, but we're advancing in a worldly sense. We're using objects,
our objects, our materials. A computer is just material. The internet, it's all made up
of material. And we become so obsessed that we spend so much energy creating these materials.
And I think it's an example. Our lives become very much external. I think technology is
a good example. Of course, the Buddha was talking, I think more about things like politics
and society and the world, the natural world, the cosmos. One's resources like a house
and possessions and so on. Not to get caught up in these things. Not to get worried about
them. But I think it all boils down to our focus and our inclination. What are we thinking
about? When we get distracted by all these goals that end up being meaningless, that we
want to be rich or famous or have this or have that. Want the latest iPhone? Want the
latest computer? Biggest house and car and so on. Want to be this or that? Want to get
employed? None of this is meaningful and all of our goals, all of our ambitions. We have
no meaning. It's not that there's a problem with the fact that they have no meaning. The
problem is that we ascribe meaning to. Doing things that are meaningless, there's nothing
ostensibly wrong with that. But what's wrong with it, what's wrong is seeing them is having
meaning. And thereby getting obsessed by them. So a person who becomes enlightened does
very little in their lives. Enlightenment leads you to do very little. Why? Because all
of these things that we do that makes the world, make society, why we have society in
the first place. We have such intense ambitions and desires and obsession. The need for
things. I mean, we didn't need an iPhone, right? Until we did. Until our needs became
greater. I don't think people's lives were less fulfilling when they didn't have iPhones.
But we're going further and further and more and more into the world. We're making the
world dangerous, dangerous because the biggest danger of it is that it distracts us. The
world distracts us. It distracts us from the problems that we've already talked about.
It distracts us from our addictions. It distracts us from things like anger and hatred and
the pettiness when it encourages them. Distracts us from the work that needs to be done
to purify our minds. Distracts us from the present moment from reality. It distracts us
from reality because the world is not real. Politics. You think Donald Trump is a problem.
It doesn't even exist. Or as I shouldn't name specific people, I'm not supposed to do that.
But there's a lot of everything. I was just visiting my mother and it's amazing. The
TV's on all the time and all they talk about is this one man. It's quite impressive. Distracts
us from reality. All of our worries about the economy and the environment and politics
and society. Yeah, there's something there. There's something in the sense that other people
are engaged in that and so to relate to them and to just live our lives, sure you need
money, you need society. Don't let them distract you. Don't hold them up beyond a functional
importance. Yes, you need food. You need money to get food. That's it. You need to live.
No money is not something to be. Strive, strive, strive for. Don't hold it up. Don't
exalt it. Don't let money be your God or any of these other, any other thing like that.
Don't let the world be your God. So these four things are, four things that this first
tells us. This is the beginning of the local aga. So it's relating to the world. The local
means world. And it's a good, good description for enumeration of the kinds of things that
we want to try and get away from then go beyond. So that was long. I'm back. We'll try to
have more videos now. But anyway, thank you for all for tuning in and this has been another
episode of the Damapada v. No. 167, wish you all the best. Have a good night.
