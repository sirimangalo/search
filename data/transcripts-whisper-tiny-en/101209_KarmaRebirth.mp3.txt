All right then. Welcome everyone. Today's talk will be about two important concepts in
Buddhist teaching. These are karma and rebirth. Now, I see these are important concepts.
And I think this is a debated statement. Many people seem to think that karma and rebirth
have nothing to do with the Buddhist teaching, especially the teaching on rebirth. There's
a lot of belief that the Buddha taught only for one life and nothing to do with the idea
of rebirth. Really, these sorts of debates, I think, center around our attachment to
views, our opinions and views of what the Buddha taught, rather than actual understanding
of reality. Because when you practice the Buddha's teaching, it really makes no difference
as to what is one's opinion or the idea of what it was that the Buddha taught Buddha taught
or so on. And whether the Buddha taught about rebirth, about the past lives or future
lives, there's no argument that arises because through the practice you come to see for
yourself the reality of things. And what we come to see when we practice the Buddha's
teaching and understand the reality of things is that really in a way the Buddha didn't
teach karma and rebirth in their standard form. Because the word karma means action.
And the doctrine of karma means the belief that every action that we do has an effect on
our happiness and our unhappiness. Either that or certain actions when performed haven't
effect on our happiness and our unhappiness in and of themselves. And this was the doctrine
that was understood to be the truth by many people before the time of the Buddha. And
it was taught by the brahmanas, the priests. And the reason it was taught by them it seems
is because this was a very profitable thing to teach. When you teach people that certain
ritual actions are to their benefit for either warding off evil or bringing blessings
and so on. And those actions were actions that required a priest to perform. Then it was
a great way to make your living. And so this evolved into an entire doctrine based on
these sacrifices that the brahmanas did. It was also prior to the establishment of the
priestly caste or group. It seems it was a part of the nomadic culture of the audience.
These people who came in slaughtered all the natives in India. When they won the battle
they would always offer something to their god, to Indra. And you can see this of course
in all nomadic or native culture and not native tribal cultures. This idea of sacrifice.
You can see it in Judaism, in the Old Testament, the Hebrew Bible. This idea that some
certain activities had benefit. And the Buddha taught against this. This was the teaching
that the Buddha had to replace. Because obviously it's not for those of us who have an intellectual
or a scientific understanding of reality, we can see and we can verify for ourselves through
experimentation, through research, through empirical observation that this isn't true,
that these actions don't actually have any intrinsic benefit. And so with the Buddha
taught as karma, and he just borrowed the word, he really didn't teach karma at all.
He taught intention. Jaitana. And he said that what these people are talking about with
the word karma is actually the truth of it is that it is the job, it is the role of
Jaitana of one's intention, one's relation. When one has wholesome intentions, it doesn't
matter what one, what one does, one's mind has wisdom in it or has the desire to sacrifice
or love in it or so on. And everything one does with that mind, based on that mind,
the result of that mind, the result of that karma, that mental action, will inevitably
be positive. And so this is the Buddha's doctrine of karma. It's a very simple doctrine
and it's very scientific. Because in understanding both karma and rebirth, we have to understand
the nature of reality according to the Buddha, according to the Buddha's teaching.
That the Buddha, again, didn't talk about reality as something out there. He talked about
two kinds of reality. This conceptual reality that we talk about the universe, you might
even talk about second life as a conceptual reality. In the same way that the world around
us is a conceptual reality. So we can talk about the Deer Park, we can talk about the
hall, the meditation hall up there, the store, the hot spring, the mountains, but these
are all just concepts. They don't really exist. But we use them to refer to, refer these
places to other people. And we do the same with the world around us. We refer to countries
and cities and places. This three-dimensional reality that is out there. And has actually
been shown to be only merely conceptual by modern physics. That reality can only be thought
of in terms of events, in terms of experience. And this is very much the Buddha's teaching
that all of reality is dependent on the mind. There's the physical side of reality, but
it is only encompassed by experience. The ultimate reality of it is the experience of it.
When we experience the physical reality around us seeing and hearing and smelling and
tasting and feeling and thinking, this is what's real. And so our experience of reality
will define the results that we receive. If we choose to direct our minds in a certain
direction, that's the reality that the reality that arises will be based on that action,
on that intention. When we talk about karma, it's not something magical or mystical
where someone kills someone and then you sit around waiting for them to be hit by lightning
or something. Or even sit around waiting for them to go to smug in the fact that they're
going to go to hell or something. It's actually not a cut and dry doctrine in that way.
You can't say that if a person does all sorts of bad things, they're necessarily going
to go to hell. That's unscientific. Because it's a simplistic sort of magical pseudo-scientific
understanding or belief, this idea that certain things automatically lead you to certain
results. And that's unscientific. We can say that certain intentions incline in a certain
direction, but other than that, unless we can somehow take into account all the other
factors involved, our intention, our past intentions, the direction we're heading and so on,
that we can't really make those kinds of claims as to what a certain act is going to
affect it's going to have. It's like weather. You can look at a certain weather and pattern
and say, oh, this is going to create a different weather pattern. And then the results
come out completely different because there are so many different factors involved that
you can't possibly take into account. You can only offer probabilities unless you have
some super-advanced understanding of all of the different variables and factors involved
in the process. So the teaching of karma really is that basically in a basic understanding
is that bad things have a bad effect on our lives. That's why they're bad. Certain
things, certain intentions will have a bad result or will incline towards a bad result.
Certain things will incline towards a good result. Will it lead to our benefit?
And so if we understand karma in this way, we can really understand the details of
the doctrine. We can understand how it works. And we can see that it's actually not a
cut-and-dry thing where all we can say is that certain things are unadvisable because
they're dangerous. They have the potential to lead to our suffering.
So first on a phenomenological or an experiential level, there are four kinds of karma.
This is based on our every moment of the experience, every karma that we perform. When
we get angry, when we have greed in our minds, when we give rise to these unals and states,
they will have four results. One kind will actually give rise to a specific result.
So certain acts that we perform will give a result. Sometimes when you do something bad
or you do something good, right away you experience the result. Actually, this is generally
the case that most of the things we do do have a immediate result. And it's just that
we don't see it or we don't understand the relationship between the two. For instance, when
you're generous with someone, you give them something or you offer your time to them or
your kind words or so on, you find the right way you feel good about yourself. You feel
calm, you feel happy, you feel proud in a sense, or you feel good about what you've done.
And that's the direct result that comes from the good karma. But certain times we'll
do something and we're expecting a result of a certain sort and we don't get that result.
In fact, sometimes it isn't the case where we give something to someone. Sometimes we can
give or be generous towards someone or do something good in a very one of the various ways.
And get a bad result. We give something to someone and they become upset because of it.
Maybe we don't give them what they wanted or you give a person on the street, give them
a dollar bill and they get angry and they wanted more or so on. Maybe they give them
one dollar and then all of a sudden their friend comes and wants another dollar or so on.
And you feel upset by it.
So only certain types of karma will give a direct result. Other types of karma have
the effect of supporting the right arising of results. So sometimes we'll do something
and it doesn't give us a good result right away or it doesn't give us a bad result right
away. But as we perform the same action again and again or repeatedly perform wholesome
around wholesome actions, the accumulation of the evil deeds or the good deeds leads to the
result of the action, leads to the bad result or the good result. This is called supportive
karma. Sometimes it's required to do the same act again and again or to perform multiple
acts in order to bring about a result. If supporting factors aren't present then sometimes
our good deeds won't come to result to good results.
So the point is that we shouldn't feel upset when good deeds don't always lead to good
results or feel also not be negligent when our bad deeds don't always lead to bad results.
Sometimes they're just waiting for the accumulation of bad deeds. Sometimes all it takes
is one like the straw that broke the camel's back.
