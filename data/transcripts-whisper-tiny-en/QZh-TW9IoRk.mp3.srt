1
00:00:00,000 --> 00:00:07,400
I need each other, can't just work on morality in isolation.

2
00:00:07,400 --> 00:00:08,800
The other one's all far.

3
00:00:08,800 --> 00:00:11,080
Well, that's really important, I think,

4
00:00:11,080 --> 00:00:13,960
and it points to what true morality is.

5
00:00:13,960 --> 00:00:19,600
One someone had a comment on my video for children,

6
00:00:19,600 --> 00:00:21,520
because it was just teaching meditation

7
00:00:21,520 --> 00:00:26,000
with no context of morality or even wisdom, as he said.

8
00:00:26,000 --> 00:00:30,440
But I think that's really missing the point of morality

9
00:00:30,440 --> 00:00:33,720
and wisdom, and how they come together.

10
00:00:33,720 --> 00:00:35,640
True morality isn't intellectual,

11
00:00:35,640 --> 00:00:38,840
it doesn't come from taking on precepts,

12
00:00:38,840 --> 00:00:43,520
or really it doesn't come from working.

13
00:00:43,520 --> 00:00:45,520
It comes from enlightenment.

14
00:00:45,520 --> 00:00:49,000
Morality comes through meditation.

15
00:00:49,000 --> 00:00:51,600
The beginning of meditation is morality.

16
00:00:51,600 --> 00:00:52,880
That's true morality.

17
00:00:52,880 --> 00:00:55,880
Morality isn't taking precepts.

18
00:00:55,880 --> 00:00:59,200
It isn't even refusing to do something

19
00:00:59,200 --> 00:01:00,920
when you have the opportunity to do it.

20
00:01:00,920 --> 00:01:05,840
It's, sorry, at least not in the sense of saying,

21
00:01:05,840 --> 00:01:08,760
I'm not going to kill him when the opportunity comes to kill.

22
00:01:08,760 --> 00:01:09,880
It's much deeper than that.

23
00:01:09,880 --> 00:01:15,960
True morality is a mind state that refrains.

24
00:01:15,960 --> 00:01:22,080
So this applies to all states that become distracted.

25
00:01:22,080 --> 00:01:26,320
Morality is the development of concentration directly.

26
00:01:26,320 --> 00:01:29,200
It's the focusing of the mind.

27
00:01:29,200 --> 00:01:33,280
And that's why morality is said to lead to concentration.

28
00:01:33,280 --> 00:01:35,520
Things that are immoral, they're all immoral,

29
00:01:35,520 --> 00:01:38,320
and as a result, there's so many immoral things out there

30
00:01:38,320 --> 00:01:40,240
that we don't generally consider immoral.

31
00:01:40,240 --> 00:01:43,520
But they're immoral because they are diffusing the mind.

32
00:01:43,520 --> 00:01:46,200
They're creating chaos in the mind.

33
00:01:46,200 --> 00:01:48,600
Anything that makes the mind more chaotic

34
00:01:48,600 --> 00:01:50,360
is for that reason immoral,

35
00:01:50,360 --> 00:01:53,560
and that, therefore, is the definition of immorality

36
00:01:53,560 --> 00:01:54,440
in a Buddhist sense.

37
00:01:54,440 --> 00:01:58,600
It's totally utilitarian.

38
00:01:58,600 --> 00:02:01,560
So something is immoral because of its effect on one's mind.

39
00:02:01,560 --> 00:02:08,600
But it's also absolute because any number of things,

40
00:02:08,600 --> 00:02:11,200
anything that we would normally consider immoral,

41
00:02:11,200 --> 00:02:13,160
has that effect on the mind of killing

42
00:02:13,160 --> 00:02:14,240
has that effect on the mind.

43
00:02:14,240 --> 00:02:20,320
Stealing has useless speech has that effect on the mind.

44
00:02:20,320 --> 00:02:23,840
It does the mind in it diffuses the mind.

45
00:02:23,840 --> 00:02:28,880
It creates chaos and disorder in the mind.

46
00:02:28,880 --> 00:02:31,760
So in that sense, working on morality and isolation

47
00:02:31,760 --> 00:02:34,120
is a misunderstanding of what morality is.

48
00:02:34,120 --> 00:02:36,400
Morality is meditation.

49
00:02:36,400 --> 00:02:38,560
When you sit down to practice meditation,

50
00:02:38,560 --> 00:02:40,760
that's when true morality arises.

51
00:02:40,760 --> 00:02:41,960
Yeah, you should keep precepts.

52
00:02:41,960 --> 00:02:43,560
You should know what is right and what is wrong.

53
00:02:43,560 --> 00:02:46,000
But that's only a part of the framework

54
00:02:46,000 --> 00:02:49,320
that is going to allow you to approach meditation

55
00:02:49,320 --> 00:02:50,600
in the right way.

56
00:02:50,600 --> 00:02:53,960
Once you know what sorts of things are immoral,

57
00:02:53,960 --> 00:02:55,400
then you know the sorts of things

58
00:02:55,400 --> 00:02:58,120
that are going to create diffusion in the mind,

59
00:02:58,120 --> 00:03:03,800
that are going to cause distraction and mental upset.

60
00:03:03,800 --> 00:03:10,120
That's all because the precepts are just a guide.

61
00:03:10,120 --> 00:03:15,920
They aren't anything real, they're just concepts.

62
00:03:15,920 --> 00:03:19,600
I mean, given that there's no beings, right?

63
00:03:19,600 --> 00:03:22,000
Killing isn't, there's nothing wrong with killing

64
00:03:22,000 --> 00:03:24,080
because you can't kill someone.

65
00:03:24,080 --> 00:03:26,680
There's no being, there's no one to kill, right?

66
00:03:26,680 --> 00:03:29,400
This is one way, this is a view in the Buddhist time.

67
00:03:29,400 --> 00:03:31,280
When someone kills, there is no killing.

68
00:03:31,280 --> 00:03:34,720
It's just, the knife goes through parts, atoms,

69
00:03:34,720 --> 00:03:37,480
which is what this one teacher said.

70
00:03:37,480 --> 00:03:41,440
The Buddha denied this because of the nature of the mind.

71
00:03:41,440 --> 00:03:42,960
The mind is involved.

72
00:03:42,960 --> 00:03:45,360
Yeah, the person, what's wrong with killing

73
00:03:45,360 --> 00:03:47,240
isn't that the person dies?

74
00:03:47,240 --> 00:03:52,360
What I'm a killing is that you've created a horrible intention

75
00:03:52,360 --> 00:03:58,360
in your mind and that totally disrupts your own experience.

76
00:03:58,360 --> 00:04:01,280
It's a very powerful thing to kill someone,

77
00:04:01,280 --> 00:04:03,200
but not because of what it does to them.

78
00:04:03,200 --> 00:04:05,600
Because of what it does to you.

79
00:04:05,600 --> 00:04:08,200
And it goes beyond what we realize.

80
00:04:08,200 --> 00:04:13,040
It's only when you meditate if you've killed before.

81
00:04:13,040 --> 00:04:15,200
Once you meditate, you'll know what I'm talking about.

82
00:04:15,200 --> 00:04:17,640
Because you really do feel it.

83
00:04:23,760 --> 00:04:28,320
So to talk about, to get back to the original comment

84
00:04:28,320 --> 00:04:35,640
of how difficult it might be to be moral without meditation.

85
00:04:35,640 --> 00:04:39,440
Be careful that you're not trying to be moral too hard.

86
00:04:39,440 --> 00:04:43,920
Because true, good to try, in some sense,

87
00:04:43,920 --> 00:04:47,760
to keep the precepts, but much better to use them as a guide

88
00:04:47,760 --> 00:04:51,920
and really gain true realization that these things are wrong

89
00:04:51,920 --> 00:04:53,280
so that they just won't arise.

90
00:04:53,280 --> 00:04:55,360
You just won't have wrong speech.

91
00:04:55,360 --> 00:04:56,560
You just won't lie.

92
00:04:56,560 --> 00:04:59,240
You won't have harsh speech.

93
00:04:59,240 --> 00:05:00,280
Say bad things about it.

94
00:05:00,280 --> 00:05:02,200
You won't have divisive speech trying

95
00:05:02,200 --> 00:05:04,960
to break people apart and so on.

96
00:05:04,960 --> 00:05:11,000
You give up these kinds of behaviors naturally.

97
00:05:11,000 --> 00:05:14,200
And that's where it's tied into concentration in wisdom

98
00:05:14,200 --> 00:05:17,280
because wisdom actually leads back to morality.

99
00:05:17,280 --> 00:05:18,680
It's a circle.

100
00:05:18,680 --> 00:05:48,520
Once you have wisdom, that's what brings true morality.

