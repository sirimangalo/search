1
00:00:00,000 --> 00:00:04,560
Hello and welcome back to our study of the Damapanda.

2
00:00:04,560 --> 00:00:11,920
Today, we continue on with verse 145, which reads as follows.

3
00:00:11,920 --> 00:00:41,360
It means, indeed, the leader's lead water.

4
00:00:41,360 --> 00:00:42,360
Let me take it.

5
00:00:42,360 --> 00:00:47,360
It doesn't mean leader, but it means one who leads water.

6
00:00:47,360 --> 00:01:05,200
Usukara, those who make arrows, guide the shafts, so they straighten the arrow shaft.

7
00:01:05,200 --> 00:01:08,440
Carpenters bend the wood.

8
00:01:08,440 --> 00:01:15,960
And those with good conduct, Subata, tame themselves.

9
00:01:15,960 --> 00:01:25,000
Now, if this sounds familiar, it should, if you have any familiarity with the Damapanda.

10
00:01:25,000 --> 00:01:35,560
It should sound very much like one of the earlier verses in the Panditawaga.

11
00:01:35,560 --> 00:01:45,440
We have verse 80, except instead of those of good conduct, tame themselves.

12
00:01:45,440 --> 00:01:50,880
We have the wise tame themselves, because that was the story of Pandita Samanera.

13
00:01:50,880 --> 00:01:56,080
So, here we have a verse where the Buddha says almost the same thing, in a different chapter

14
00:01:56,080 --> 00:02:02,840
though.

15
00:02:02,840 --> 00:02:08,800
There is actually quite similar to Pandita, so we're going to skip some of it, or abbreviate,

16
00:02:08,800 --> 00:02:11,440
because it's not really important anyway.

17
00:02:11,440 --> 00:02:18,120
But the important part, there's an interesting past life story involved with this one.

18
00:02:18,120 --> 00:02:35,880
There he goes, there was a certain man in ancient times, who, the name was Ganda, and

19
00:02:35,880 --> 00:02:36,880
he was a rich.

20
00:02:36,880 --> 00:02:46,640
He was the son of a rich family, and when his parents died, he gained all the wealth of

21
00:02:46,640 --> 00:02:51,360
his family, so he didn't really realize how rich his family was, but then he was brought

22
00:02:51,360 --> 00:02:59,920
to the treasury, and shown all of his wealth, and they said to him, all of this is yours

23
00:02:59,920 --> 00:03:07,800
now, and it was just an immense amount of wealth, and he started walking through and looking

24
00:03:07,800 --> 00:03:16,200
at what he had, and just shaking his head, because he thought to himself, these fools.

25
00:03:16,200 --> 00:03:19,720
They amassed all this wealth, and for what, to leave it behind, they couldn't take it

26
00:03:19,720 --> 00:03:25,440
with them, couldn't stop them from dying, and they didn't ever enjoy it.

27
00:03:25,440 --> 00:03:33,720
Money is wealth is meant to be spent, they didn't take anything with them, when they

28
00:03:33,720 --> 00:03:40,320
died they had all this unspent wealth, which is really it, it was a good point, when we

29
00:03:40,320 --> 00:03:46,040
amassed all this wealth, and then, sometimes people mass great wealth more than they can

30
00:03:46,040 --> 00:03:57,360
ever use, and so he thought to himself, I'm going to take this wealth with me.

31
00:03:57,360 --> 00:04:01,040
Now when a Buddhist says that, of course, when you're that as a Buddhist, you think,

32
00:04:01,040 --> 00:04:05,360
oh, this person's going to do good deeds, they're going to do something good with it,

33
00:04:05,360 --> 00:04:11,040
you know, use it for a good cause, no, not this guy.

34
00:04:11,040 --> 00:04:17,720
This man, Gandhana, he thought, how will I take it with me?

35
00:04:17,720 --> 00:04:26,000
He said, I will eat up all this wealth before I go, and he became determined at that time

36
00:04:26,000 --> 00:04:30,720
to spend all of his money before he died, even though it was an incredible amount.

37
00:04:30,720 --> 00:04:36,080
It's the equivalent of a billion there today, trying to use up all their wealth, something

38
00:04:36,080 --> 00:04:42,760
like that.

39
00:04:42,760 --> 00:04:53,160
And so he spent 100,000 gold coins, Gahapana, probably, building a bathhouse on Crystal,

40
00:04:53,160 --> 00:05:02,560
and 100,000 coins, making a bath seat out of Crystal, to sit while he made it.

41
00:05:02,560 --> 00:05:12,760
And he spent 100,000 coins on a couch to sit, 100,000 coins on a bowl out of which to eat,

42
00:05:12,760 --> 00:05:23,000
100,000 coins on a copper plated receptacle for the bowl, so a place to sit the bowl on

43
00:05:23,000 --> 00:05:31,640
is to stand for the bowl, spend 100,000 coins on a window, a big, magnificent window.

44
00:05:31,640 --> 00:05:37,560
And this is important, this window actually plays a part in the story.

45
00:05:37,560 --> 00:05:40,280
And he spent 100,000 on the food itself.

46
00:05:40,280 --> 00:05:52,120
It's spent 100,000 on breakfast, 100,000 on supper, and 100,000 on lunch.

47
00:05:52,120 --> 00:05:59,360
And when it came the time to eat his food, he may not have done this every day, not quite

48
00:05:59,360 --> 00:06:02,840
sure, but on the full moon day.

49
00:06:02,840 --> 00:06:10,760
On the full moon day, he would spend 100,000 coins decorating the city, cause the drum

50
00:06:10,760 --> 00:06:18,360
to be beaten, and made it proclamation, that all come and behold the manner in which rich

51
00:06:18,360 --> 00:06:23,440
man, Ganda, eats his meals, so no, he wasn't beating a drum to get people to come and

52
00:06:23,440 --> 00:06:28,200
eat with him, come and watch me eat, and that's what the window is for.

53
00:06:28,200 --> 00:06:34,400
So he would sit on his chair and place his bowl in front of him, and having bathed in

54
00:06:34,400 --> 00:06:39,920
his crystal bathhouse.

55
00:06:39,920 --> 00:06:47,720
He would open the window and display himself to the whole city so they can see how a true

56
00:06:47,720 --> 00:06:54,200
rich person ate, probably even the king, certainly the king didn't even eat so well,

57
00:06:54,200 --> 00:07:00,040
as the king was in no hurry, surely to get rid of all of his wealth.

58
00:07:00,040 --> 00:07:04,080
And so his servants would place the bowl on the copper plated receptacle and serve him

59
00:07:04,080 --> 00:07:08,240
with the food, and it was a great splendor.

60
00:07:08,240 --> 00:07:13,920
And it was interesting, so people would come and watch, and it was just a real spectacle

61
00:07:13,920 --> 00:07:19,840
to watch him eat, and he would be surrounded by dancers as well, so it was a real festival

62
00:07:19,840 --> 00:07:27,840
on, I think, on the full moon, right on the, yeah, midday meal on the full moon.

63
00:07:27,840 --> 00:07:35,720
It happened that at the round this time there was a couple of friends, there was a man

64
00:07:35,720 --> 00:07:42,320
from a village and a man from the city, and the man from the village went to spend, went

65
00:07:42,320 --> 00:07:46,520
to visit his friend in the city where this rich man lived.

66
00:07:46,520 --> 00:07:54,360
And he heard this drum, and the city man said, hey, you know, you want to come and

67
00:07:54,360 --> 00:08:00,360
see something interesting, come and see this spectacle, this rich man who eats in front

68
00:08:00,360 --> 00:08:01,360
of the whole city.

69
00:08:01,360 --> 00:08:05,880
That was the fit happened to be the full moon, is that, have you ever seen it?

70
00:08:05,880 --> 00:08:09,920
Neither, no, I've never seen it, oh, come, you have to, you've got to see that.

71
00:08:09,920 --> 00:08:11,320
And so they went together.

72
00:08:11,320 --> 00:08:15,240
And when they got there, he had opened it up, we opened up this window and he was sitting

73
00:08:15,240 --> 00:08:18,400
there and eating.

74
00:08:18,400 --> 00:08:22,840
And this man from the village, from the countryside, smelt the food.

75
00:08:22,840 --> 00:08:26,920
And there arose in him an intense craving for that food.

76
00:08:26,920 --> 00:08:34,600
It's kind of craving that is most likely something beyond just a craving, because of course

77
00:08:34,600 --> 00:08:40,640
he probably would have never tasted such food, but there must be something about his character

78
00:08:40,640 --> 00:08:45,720
that, unlike everyone else, beyond what everyone else, because certainly there was many

79
00:08:45,720 --> 00:08:49,840
people in the audience who wished they could eat such food, but he went beyond that.

80
00:08:49,840 --> 00:08:55,720
He said, I want that rice, and this friend from the city said, forget about it, there's

81
00:08:55,720 --> 00:08:57,040
no way you're getting any of that.

82
00:08:57,040 --> 00:09:00,080
Each bite is like a hundred gold coins.

83
00:09:00,080 --> 00:09:08,000
And he says, look, if I don't get some of that food, I'm going to die.

84
00:09:08,000 --> 00:09:10,600
I shall not be able to live any longer in the city, guys.

85
00:09:10,600 --> 00:09:17,400
Not able to, he wasn't able to stop him, so he shrugged his shoulders and he stood up,

86
00:09:17,400 --> 00:09:23,280
and he said to the rich man, shout it out, I bow down before you master.

87
00:09:23,280 --> 00:09:26,920
And the treasure heard this shout, and he said, who is that?

88
00:09:26,920 --> 00:09:31,280
He said, me over here, what do you want?

89
00:09:31,280 --> 00:09:37,560
And he said, well, my friend here is from the countryside, and he really wants some of

90
00:09:37,560 --> 00:09:38,560
your rice.

91
00:09:38,560 --> 00:09:46,160
Please, if I'm just a little bite, and the rich man said he can't have my rice, said,

92
00:09:46,160 --> 00:09:49,880
and so the city guy turns to his friend, did you hear what he said?

93
00:09:49,880 --> 00:09:54,000
I'm just saying, yeah, I heard it.

94
00:09:54,000 --> 00:09:55,000
And he shook his head.

95
00:09:55,000 --> 00:09:59,240
He said, if I can have some of that rice, I can live, if I can't have that rice, I shall

96
00:09:59,240 --> 00:10:00,240
surely die.

97
00:10:00,240 --> 00:10:13,000
And so the city man who is quite perturbed, moved by this poor village man's plight, stood

98
00:10:13,000 --> 00:10:21,280
up again and said, master, my friend here from the countryside says that if he can't

99
00:10:21,280 --> 00:10:23,840
have this rice, he will surely die.

100
00:10:23,840 --> 00:10:26,560
Please spare his life, I pray you.

101
00:10:26,560 --> 00:10:33,480
The rich man said, Sarah, Sarah is I guess what, well, it's actually an English, I don't

102
00:10:33,480 --> 00:10:37,400
know what they said in a friend, maybe.

103
00:10:37,400 --> 00:10:45,920
Every mouthful of rice, in my bowl, is worth 100 gold coins, 200 gold coins.

104
00:10:45,920 --> 00:10:50,120
If I give rice to everyone who asks, what shall I have to eat myself?

105
00:10:50,120 --> 00:10:57,160
When I repeat it, it's a master, please, if he doesn't get this rice, he says he will

106
00:10:57,160 --> 00:11:00,200
die, please spare his life, he can't have it.

107
00:11:00,200 --> 00:11:11,120
He shook his head, he said, look, if it'd be really true that he's going to die without

108
00:11:11,120 --> 00:11:15,160
it, then let him come work for me.

109
00:11:15,160 --> 00:11:22,800
If he comes and works for me for three years, after three years of working for me every

110
00:11:22,800 --> 00:11:32,920
day, I will give him the entire bowl of rice, and when the village heard that, he

111
00:11:32,920 --> 00:11:37,040
said, so be it, let it be so.

112
00:11:37,040 --> 00:11:42,360
And he left his family, left his sons and his wife, told them he was going to work for

113
00:11:42,360 --> 00:11:46,800
this rich man, and he actually did, and he spent three years working for this rich guy

114
00:11:46,800 --> 00:11:51,600
just to get a bowl of rice.

115
00:11:51,600 --> 00:11:58,120
And every day he worked hard, but it changed him, you know.

116
00:11:58,120 --> 00:12:01,280
This is the thing about cravings, addictions.

117
00:12:01,280 --> 00:12:12,320
Over time you start to, many people start to, the goodness in them.

118
00:12:12,320 --> 00:12:21,760
Once to buzz in your ear, you start to give this nagging feeling that something's wrong,

119
00:12:21,760 --> 00:12:25,120
and it becomes greater and greater until you.

120
00:12:25,120 --> 00:12:27,520
For some people they do something about it.

121
00:12:27,520 --> 00:12:29,520
He didn't do anything about it.

122
00:12:29,520 --> 00:12:33,560
He spent three years single-mindedly thinking about this, right?

123
00:12:33,560 --> 00:12:40,680
But perhaps over those three years, some nagging feeling came to him.

124
00:12:40,680 --> 00:12:48,680
But perhaps what he was doing was not entirely sane, perhaps there was a better way.

125
00:12:48,680 --> 00:12:55,120
Nonetheless after three years, it came the day, and the rich man said, well, you've

126
00:12:55,120 --> 00:13:00,520
done what you didn't set out to do, and it's quite impressive, your determination.

127
00:13:00,520 --> 00:13:03,440
For today you will live as I do.

128
00:13:03,440 --> 00:13:07,600
Instead of me going to the bathhouse, you will go to this crystal bathhouse, bathe in

129
00:13:07,600 --> 00:13:13,360
this crystal seat, and yada, yada, yada, you will sit and you will eat.

130
00:13:13,360 --> 00:13:22,960
You will live as I do for this day, so you've got up on the seat, and they place the

131
00:13:22,960 --> 00:13:31,800
bowl down in front of him, and he was just getting ready to eat.

132
00:13:31,800 --> 00:13:36,680
As he was getting ready to eat, there suddenly came.

133
00:13:36,680 --> 00:13:45,000
He suddenly spined in the crowd, or was it in the crowd.

134
00:13:45,000 --> 00:13:57,480
He suddenly saw somehow, when he was watching his hands, yes, he came and stood before

135
00:13:57,480 --> 00:13:58,480
him.

136
00:13:58,480 --> 00:14:00,240
He came out of the crowd, not often the crowd.

137
00:14:00,240 --> 00:14:05,040
He came to the front of the crowd and he stood before this man, and he was just getting

138
00:14:05,040 --> 00:14:09,240
ready to put a bite in his mouth, or he was just getting ready to put his hand in the

139
00:14:09,240 --> 00:14:11,600
bowl, maybe.

140
00:14:11,600 --> 00:14:19,240
And he saw this particular Buddha, which would be an enlightened person who has become enlightened

141
00:14:19,240 --> 00:14:25,480
for themselves, even when the time when there is no Buddha to teach, the way to become

142
00:14:25,480 --> 00:14:26,480
free from suffering.

143
00:14:26,480 --> 00:14:30,960
There are beings who are able to find it for themselves, but not really able to explain

144
00:14:30,960 --> 00:14:37,240
it to others, but they have hit upon the way themselves, and then they normally stay

145
00:14:37,240 --> 00:14:44,120
off in the hills and the mountains and the forests, but sometimes, sometimes they come

146
00:14:44,120 --> 00:14:50,400
and they do great things, so here was this particular Buddha, private Buddha doing a great

147
00:14:50,400 --> 00:14:51,400
thing.

148
00:14:51,400 --> 00:14:55,560
There's clearly this guy was on the wrong path, clearly he was not doing anything good

149
00:14:55,560 --> 00:14:56,560
for himself.

150
00:14:56,560 --> 00:15:04,240
In fact, both of these guys were a bunch of a couple of them, and they were a real pair.

151
00:15:04,240 --> 00:15:10,200
The rich guy who had all this money and decided he was just going to waste it all in the

152
00:15:10,200 --> 00:15:17,920
most frivolous way possible, and this poor country fellow, instead of working for any real

153
00:15:17,920 --> 00:15:25,200
benefit, was working for some silly meal that was in the end, only going to last in

154
00:15:25,200 --> 00:15:28,680
the morning.

155
00:15:28,680 --> 00:15:31,120
And it came to a head when he saw this particular Buddha.

156
00:15:31,120 --> 00:15:36,960
He looked at him and he said, it just hit him and he said, what am I doing here?

157
00:15:36,960 --> 00:15:46,400
He said, what good is this meal to me?

158
00:15:46,400 --> 00:15:48,200
It struck him.

159
00:15:48,200 --> 00:15:58,200
And here was someone who had no food to eat, and was perhaps even truly worthy of such

160
00:15:58,200 --> 00:15:59,200
a great meal.

161
00:15:59,200 --> 00:16:11,400
And he himself had done nothing really special to deserve it, impressive perhaps, but...

162
00:16:11,400 --> 00:16:18,240
So he thought to himself, I've never done anything good.

163
00:16:18,240 --> 00:16:23,680
Here is this monk, this ascetic, who's looking for food, and I've never done anything

164
00:16:23,680 --> 00:16:25,720
really great like that.

165
00:16:25,720 --> 00:16:31,760
I've never given anything, and of course, the belief in karma, he said, it's probably

166
00:16:31,760 --> 00:16:38,920
why I'm so poor, because I've never really done anything good to deserve being rich.

167
00:16:38,920 --> 00:16:48,040
And so he looked at this particular Buddha, and he picked up the ball, and gave it to one

168
00:16:48,040 --> 00:16:52,680
of the servants, and he bowed down before this particular Buddha, and he took the ball back,

169
00:16:52,680 --> 00:16:59,240
and he began to pour this most expensive meals into the particular Buddha's bowl.

170
00:16:59,240 --> 00:17:04,720
Now, when it got half of it, the particular Buddha covered its hand, and in a sign that,

171
00:17:04,720 --> 00:17:07,920
it's okay, you share it with me, you can have half, I'll have half.

172
00:17:07,920 --> 00:17:13,600
And the man shook, he said, look, this is one portion for one person, and he said, and

173
00:17:13,600 --> 00:17:19,800
here's the quote, he said, do not bestow a favor, I ask you not to bestow a favor on

174
00:17:19,800 --> 00:17:26,040
me in this life, by leaving half the rice for me, I want full favor in the next life, in

175
00:17:26,040 --> 00:17:27,040
future lives.

176
00:17:27,040 --> 00:17:31,720
Now, for my spiritual benefit basically, I want this to be for my spiritual benefit, not

177
00:17:31,720 --> 00:17:32,720
my physical benefit.

178
00:17:32,720 --> 00:17:36,640
And this is important, because that's what food is, you know, if you eat a meal in the Buddha

179
00:17:36,640 --> 00:17:44,160
said this, if you eat a meal, you give benefit to yourself for a morning, you get a

180
00:17:44,160 --> 00:17:49,240
day's worth of life, and that's it, doesn't last beyond that.

181
00:17:49,240 --> 00:17:53,280
But if you do a good deed, if you give that same food to someone else, the greatness

182
00:17:53,280 --> 00:18:01,760
of the act changes you, it changes your heart, your soul, if you will, changes who you

183
00:18:01,760 --> 00:18:10,240
are, makes you a better person, and that lasts, that lasts for this life, and to the next

184
00:18:10,240 --> 00:18:19,360
life, it changes your, the course of the future, and so you gave all this rice, and

185
00:18:19,360 --> 00:18:20,520
you went hungry.

186
00:18:20,520 --> 00:18:25,720
After all that time, it's probably one of the more powerful gifts that we have as an

187
00:18:25,720 --> 00:18:33,680
example here, someone who spent three years working, because he wanted this food, and then

188
00:18:33,680 --> 00:18:34,680
gave it away.

189
00:18:34,680 --> 00:18:44,520
It's quite impressive, I don't remember the more impressive acts that we see.

190
00:18:44,520 --> 00:18:49,080
And he made a determination, he said, you know, here's something better, something better

191
00:18:49,080 --> 00:18:52,040
than this wonderful meal, far better.

192
00:18:52,040 --> 00:18:58,560
And he looked at him and he said, Venerable, Sir, may I, may I gain what you have gained?

193
00:18:58,560 --> 00:19:02,240
May I partake in the same truth which you have seen?

194
00:19:02,240 --> 00:19:07,640
He made a determination to become enlightened, and the, but you, Jacob would have said

195
00:19:07,640 --> 00:19:19,160
Avon hoped to, may it be the soul, and he gave him another blessing, which I won't go into,

196
00:19:19,160 --> 00:19:22,000
and then he left.

197
00:19:22,000 --> 00:19:27,680
Now the repercussions of such a great, the English, impressed the whole crowd, and impressed

198
00:19:27,680 --> 00:19:33,800
the rich man, Gandhana, who in turn shook his head and said, you know, full life

199
00:19:33,800 --> 00:19:35,800
been.

200
00:19:35,800 --> 00:19:39,880
Here I've had all this money all this time, and I've done nothing good with it, had so

201
00:19:39,880 --> 00:19:46,120
much opportunity to be a good person, to cultivate good things, to better myself spiritually,

202
00:19:46,120 --> 00:19:54,200
and I've done nothing of this sort, I've wasted it, for my own useless futile frivolous

203
00:19:54,200 --> 00:20:02,880
luxury, and he called this man to him, and he said, he took a thousand gold coins, and

204
00:20:02,880 --> 00:20:09,280
he said, here's a thousand gold coins, give me the goodness of your deed, hand it over

205
00:20:09,280 --> 00:20:18,000
to me, make it over to me, and so the poor guy said, sure, okay, I know rude to have

206
00:20:18,000 --> 00:20:22,840
the same thing happened to Annaruda, this is a common story, rich people trying to buy

207
00:20:22,840 --> 00:20:23,840
merit.

208
00:20:23,840 --> 00:20:28,000
Annaruda was concerned whether it's possible to buy goodness, so he went nasty, but Jacob

209
00:20:28,000 --> 00:20:31,360
wouldn't have taken it or said, well it's okay, you keep your goodness, if someone else

210
00:20:31,360 --> 00:20:39,160
wants it, they can have it too, it multiplies, and so he gave it over, and they became

211
00:20:39,160 --> 00:20:50,520
good friends, and they changed their lives, they decided, we have to do something, let's

212
00:20:50,520 --> 00:20:54,920
make this, make this our lives, the king got involved, the king heard about this, because

213
00:20:54,920 --> 00:20:59,480
it was a great spectacle in the city, it was a big festival watching this guy eat, so

214
00:20:59,480 --> 00:21:04,280
everyone knew about this great act of renunciation, because everyone was waiting, three

215
00:21:04,280 --> 00:21:10,240
years they had waited to see this poor guy get the meal, the meal, and then he gives it

216
00:21:10,240 --> 00:21:16,840
to the particular book, so I really shook up the whole populace, and so the king rewarded

217
00:21:16,840 --> 00:21:22,920
him and made him a rich man, and so these two rich guys dedicated themselves from that

218
00:21:22,920 --> 00:21:27,400
day on to doing good deals, that's the story of the past.

219
00:21:27,400 --> 00:21:33,600
The present story is of Sukha the novice, and it's quite short, there's a bit about the

220
00:21:33,600 --> 00:21:37,520
gods getting involved, but I don't want to get into it, and I'm not really convinced

221
00:21:37,520 --> 00:21:44,160
as to the veracity of all that, but hey who knows, I think my audience is probably less

222
00:21:44,160 --> 00:21:55,000
inclined than I am to indulge in the possibility, but so we'll stick to the simple version.

223
00:21:55,000 --> 00:22:03,520
So this guy, but the party covers his name, which means the one who works for a meal,

224
00:22:03,520 --> 00:22:09,080
so he became known as Bata Batika.

225
00:22:09,080 --> 00:22:23,480
He was reborn in Sabhati, and he was named Sukha, and his mother was one of the great

226
00:22:23,480 --> 00:22:28,840
supporters of Sariputa, just like Pandita, the story is very similar to another story

227
00:22:28,840 --> 00:22:35,360
that we've told him, in verse 80 I think, and but they call this, they name this boy

228
00:22:35,360 --> 00:22:41,800
Sukha, and he became your day-end at the age of seven, at the age of seven, he just decided

229
00:22:41,800 --> 00:22:48,720
he said, mother, I would like to our day-end, and his mother let him more day-end.

230
00:22:48,720 --> 00:22:54,720
And like Pandita, I think it was Pandita, he was walking through, walking after Sariputa

231
00:22:54,720 --> 00:23:03,520
on arms, and he saw these things, he had just like Pandita, he saw the ditch-diggers guiding

232
00:23:03,520 --> 00:23:09,040
the water, making the water go into their fields, he saw the arrow-makers straightening

233
00:23:09,040 --> 00:23:20,840
their shaft, the shafts, and he saw the carpenters shaping the wood.

234
00:23:20,840 --> 00:23:26,320
And he thought to himself, well, if they can do that with inanimate objects, surely I can

235
00:23:26,320 --> 00:23:29,240
tame my mind.

236
00:23:29,240 --> 00:23:33,080
So I guess the context has he had been hearing about taming the mind and he thought, well,

237
00:23:33,080 --> 00:23:40,640
how is that possible, how do you accomplish that, or he was considering how it was possible?

238
00:23:40,640 --> 00:23:45,040
And when he saw these guys, he started to realize what it was, you know, they can, you know,

239
00:23:45,040 --> 00:23:50,440
the working with, you know, if you think in modern terms, how we work with computers, how

240
00:23:50,440 --> 00:23:56,840
we work with machines, robots, we can get inanimate objects to do great things.

241
00:23:56,840 --> 00:24:03,920
Even compute, provide us with computations and information, we can manipulate, we put information

242
00:24:03,920 --> 00:24:10,800
in, we can manipulate the information and get all sorts of information out.

243
00:24:10,800 --> 00:24:19,280
So many wonderful things we can do with inanimate physical objects.

244
00:24:19,280 --> 00:24:25,360
And so it makes, gives him the analogy of dealing with the mind that the mind is, surely

245
00:24:25,360 --> 00:24:30,360
I can, I can work with the mind, I mean, I guess at least giving him, may realizing

246
00:24:30,360 --> 00:24:36,160
that actually can't be any harder than what these guys are doing.

247
00:24:36,160 --> 00:24:41,000
He sees them and it gives them an idea of how to tame the mind.

248
00:24:41,000 --> 00:24:47,640
That gives him an inclination to do the same with his mind, as he sees them working and

249
00:24:47,640 --> 00:24:49,160
he thinks, well, you know.

250
00:24:49,160 --> 00:24:51,320
My work is this, I should do it.

251
00:24:51,320 --> 00:24:55,960
So he stops on arms around, he doesn't go on arms, and he goes back to a good DNA meditates.

252
00:24:55,960 --> 00:25:03,240
And he meditates on taming the mind, you know, working with the mind.

253
00:25:03,240 --> 00:25:06,120
It's everything good analogy.

254
00:25:06,120 --> 00:25:12,760
It's impressive that such a young boy was able to work this out for himself, but definitely

255
00:25:12,760 --> 00:25:17,720
for us, it's a great way to think of our meditation practice.

256
00:25:17,720 --> 00:25:23,160
As you can't control the water, you can't control the shaft, just make them suddenly straight,

257
00:25:23,160 --> 00:25:25,440
but you can work with them.

258
00:25:25,440 --> 00:25:30,640
And the mind is the same, the mind is like working with an inanimate object, you have to

259
00:25:30,640 --> 00:25:39,400
be patient, work with it, train it and tame it in a figurative sentence, with your tame

260
00:25:39,400 --> 00:25:46,280
physical, you tame the wood so that it becomes straight, you tame the waters of the rivers

261
00:25:46,280 --> 00:25:52,800
and lakes, you tame it and you train it and you direct it and you do the same with the

262
00:25:52,800 --> 00:26:03,000
mind, you direct the mind, tame the mind, you tame yourself.

263
00:26:03,000 --> 00:26:10,400
And the Buddha found out about this long story short, and he said, this novice has realized

264
00:26:10,400 --> 00:26:15,880
this, that just as all these people tame inanimate physical objects.

265
00:26:15,880 --> 00:26:25,200
So too the wise tame the mind, and not the wise in this case those of good conduct.

266
00:26:25,200 --> 00:26:27,400
So what lessons do we have to learn?

267
00:26:27,400 --> 00:26:35,760
Well that's of course the main lesson, if we go back to the origin story, the great lesson

268
00:26:35,760 --> 00:26:45,800
there for me is the nature of desire, how intense it can be and how wrong it is.

269
00:26:45,800 --> 00:27:00,520
How it compares and contrasts to the good deeds of giving, of charity, of renunciation,

270
00:27:00,520 --> 00:27:06,800
how much happier it makes you, the contrast between the life of debauchery and the life

271
00:27:06,800 --> 00:27:20,120
of giving, and the life of goodness, and just how intense it can be to want, how your

272
00:27:20,120 --> 00:27:31,760
intense wanting can lead you to do the most extreme things, like leave your family behind

273
00:27:31,760 --> 00:27:34,640
and go and work for a single meal.

274
00:27:34,640 --> 00:27:41,440
There's no rationality behind it, but yet this is similar to our behavior and most of

275
00:27:41,440 --> 00:27:47,320
our behavior how we do such irrational things, for just a meal for example.

276
00:27:47,320 --> 00:27:52,360
We pay a lot of money sometimes for a good meal.

277
00:27:52,360 --> 00:27:55,120
Sometimes we'll pay money just for a good food, for example.

278
00:27:55,120 --> 00:28:04,600
We'll pay money for music, we'll pay money for art, for things that don't actually satisfy

279
00:28:04,600 --> 00:28:05,600
that.

280
00:28:05,600 --> 00:28:11,160
Thinking that would be satisfied, I suppose it's a little more blatantly obvious that

281
00:28:11,160 --> 00:28:15,440
this guy wasn't going to be satisfied, that this was totally out of proportion, working

282
00:28:15,440 --> 00:28:17,840
three years for a single meal.

283
00:28:17,840 --> 00:28:23,360
To some extent we do that, it's in the vigils, we'll work for an object that we want

284
00:28:23,360 --> 00:28:32,120
to get, maybe you want to buy a car or get a house or something, not all purchases are

285
00:28:32,120 --> 00:28:42,000
wrong or bad, but our desires do lead us to do more than we should, our desires do force

286
00:28:42,000 --> 00:28:53,080
us to work harder than we need, because it's more suffering than what otherwise we follow.

287
00:28:53,080 --> 00:28:56,920
That's sort of the lesson I get from that.

288
00:28:56,920 --> 00:29:01,120
And just that moment, it's an interesting moment to think of what you would do if you

289
00:29:01,120 --> 00:29:07,360
got to that point, where you were going to have this food, and then you saw someone, a

290
00:29:07,360 --> 00:29:08,360
Buddha.

291
00:29:08,360 --> 00:29:20,400
A Buddha came, instead before you, having not eaten that day, what would you do?

292
00:29:20,400 --> 00:29:25,600
And the feeling that it gives to think about that and to think of the greatness of being

293
00:29:25,600 --> 00:29:35,440
able to give your, it's most precious of meals away to someone who objectively deserved

294
00:29:35,440 --> 00:29:39,640
it, was worth giving it to.

295
00:29:39,640 --> 00:29:40,640
So that's that.

296
00:29:40,640 --> 00:29:52,320
And as for the verse, I mean the lesson is, as I stated, that meditation is like working

297
00:29:52,320 --> 00:29:56,560
in an intimate object, it's something you have to work at.

298
00:29:56,560 --> 00:30:02,240
First of all, it's not meant to be comfortable or pleasant, it's meant to be challenging,

299
00:30:02,240 --> 00:30:15,920
it's meant to take effort, focus, introspection, discrimination, it's meant to be something

300
00:30:15,920 --> 00:30:21,320
you put your heart into, but it's not something you can force, you can force the water

301
00:30:21,320 --> 00:30:30,040
and say, go into my field, you have to train it, you have to divert it, direct it, you

302
00:30:30,040 --> 00:30:34,760
can force the arrows to be straight, you have to work them, heat them over the fire and

303
00:30:34,760 --> 00:30:43,400
they slowly, somehow you can straighten the wood, I don't really know how, or as a carpenter

304
00:30:43,400 --> 00:30:45,640
you have to shape it.

305
00:30:45,640 --> 00:30:52,680
So we do this with the mind, we direct it, we straighten it, we shape it.

306
00:30:52,680 --> 00:31:03,480
We lead it to do our desires, but we don't force it, we train it to be straight through

307
00:31:03,480 --> 00:31:04,480
as a habit.

308
00:31:04,480 --> 00:31:14,200
The meditation is like a good habit that overrides all of our bad habits, you have a bad

309
00:31:14,200 --> 00:31:27,520
habit of judging, reacting, clinging, and we replace that with a good habit of knowing,

310
00:31:27,520 --> 00:31:37,400
seeing the habit of careful, the patient observation of things as they are without

311
00:31:37,400 --> 00:31:44,600
judgment, without partiality, without stress, without suffering.

312
00:31:44,600 --> 00:31:50,080
And it's a process, it's a process of cultivation, that's what we do in meditation.

313
00:31:50,080 --> 00:31:57,960
So another verse, as it's very, very similar to the other one, there actually isn't much

314
00:31:57,960 --> 00:32:03,920
more to say, but it's nice to go over these things again as I have, so there we go, verse

315
00:32:03,920 --> 00:32:08,160
145, thank you all for tuning in, we'll see you in our good practice.

