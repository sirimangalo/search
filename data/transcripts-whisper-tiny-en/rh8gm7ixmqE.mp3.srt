1
00:00:00,000 --> 00:00:08,360
The key in meditation is to stay in the present moment, so I mean the present moment is

2
00:00:08,360 --> 00:00:14,240
easy to find something we have to go looking for it anyway, in fact this is the key problem

3
00:00:14,240 --> 00:00:23,120
with meditation or the thing which gets in the way which does not allow us to meditate

4
00:00:23,120 --> 00:00:31,080
the reason why we aren't meditating all the time is because we're always looking for something

5
00:00:31,080 --> 00:00:41,320
when you stop looking and just being, this is meditation, this is living, this is freedom

6
00:00:41,320 --> 00:00:50,320
from suffering, so the acknowledgement is just a tool, it's just something, it's not like

7
00:00:50,320 --> 00:00:57,120
it's going to lead you anywhere, it's in fact going to stop you and the good is it, mindfulness

8
00:00:57,120 --> 00:01:02,160
is something which stops us, it doesn't do which leads us anywhere, it just stops the

9
00:01:02,160 --> 00:01:16,360
mind from chasing after, running away and brings the mind back to its natural ordinary

10
00:01:16,360 --> 00:01:23,920
everyday home which is the present moment and it's kind of funny that we, we know this

11
00:01:23,920 --> 00:01:32,320
is what's real and yet we're always somewhere else, this is how we should understand

12
00:01:32,320 --> 00:01:42,040
meditation, simple, easy, I think anyone can do something we all should do, something

13
00:01:42,040 --> 00:01:54,000
which leads us to true and lasting real peace, real ordinary peace, peace which is not

14
00:01:54,000 --> 00:02:00,680
hard to understand, it's not hard to imagine, it doesn't make us have to be anything,

15
00:02:00,680 --> 00:02:08,840
we don't even have to give up who we are, we just have to be who we are, be in the

16
00:02:08,840 --> 00:02:15,840
present moment.

