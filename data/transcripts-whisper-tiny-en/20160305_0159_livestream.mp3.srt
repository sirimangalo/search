1
00:00:00,000 --> 00:00:18,240
Okay everyone.

2
00:00:18,240 --> 00:00:48,160
It's a short quote, you can read it off and forth, I don't know if the polling

3
00:00:48,160 --> 00:00:57,520
in front of him, but he who puts aside his wants in order to do the right thing, even

4
00:00:57,520 --> 00:00:58,520
though it'd be difficult.

5
00:00:58,520 --> 00:01:12,520
It was like a patient drinking medicine later, he will rejoice.

6
00:01:12,520 --> 00:01:24,840
So yeah, sometimes doing the right thing hurts, sometimes you want something, but you know

7
00:01:24,840 --> 00:01:34,280
the right thing is to not take it, and do something, you know that I'm not doing it's

8
00:01:34,280 --> 00:01:42,800
probably the better thing.

9
00:01:42,800 --> 00:01:57,120
This is a really important teaching, I think, because it's hard to go against your desires,

10
00:01:57,120 --> 00:02:13,160
we're not equipped to, our brains, our brains are designed to accommodate addiction,

11
00:02:13,160 --> 00:02:20,440
accommodate our desires, and the knowing balls gets weaker and weaker has become more

12
00:02:20,440 --> 00:02:27,760
addicted, so the know system, whatever that is, the system that says no is quite different

13
00:02:27,760 --> 00:02:36,840
from the system and says yes, and so you can only say no for some long minutes, it takes

14
00:02:36,840 --> 00:02:50,280
effort and it's taxing to say no, to try and prevent yourself from following your desires.

15
00:02:50,280 --> 00:02:57,080
That's not the important part of this first, the important part of the teaching is not

16
00:02:57,080 --> 00:03:06,680
to deny your desires, it's about the happiness that comes from, what's right, the knowing

17
00:03:06,680 --> 00:03:13,720
what's right, just like the happiness that comes from taking medicine, the only reason

18
00:03:13,720 --> 00:03:20,680
we take medicine is not because of how it tastes, because it makes us feel that it's because

19
00:03:20,680 --> 00:03:37,720
it cures us of our illness, and that knowledge that reassurance confidence in our mind

20
00:03:37,720 --> 00:03:48,520
that is strong enough, that is strong enough to overcome your desire, you want something

21
00:03:48,520 --> 00:03:59,400
and well that's a hard argument to fight against, I want it, I want that, that thing makes

22
00:03:59,400 --> 00:04:12,880
me happy, it's a hard argument to fight, you can't say no, no, bad for you, but it's

23
00:04:12,880 --> 00:04:17,960
a the only way, it's not, it's understandable if you don't find something better than

24
00:04:17,960 --> 00:04:45,720
something more powerful, this can be enough to then, to cause you to let go, let go of your

25
00:04:45,720 --> 00:04:54,840
desire, this is why giving is better than receiving, it really is, it really feels better

26
00:04:54,840 --> 00:05:03,840
to give, and to receive, and this is how one conquers miserliness within oneself,

27
00:05:03,840 --> 00:05:18,920
within stinginess, practice giving, because it'll feel good, it'll feel better than getting,

28
00:05:18,920 --> 00:05:24,720
if you do it enough, it's actually a way to overcome stinginess, give, practice giving, it

29
00:05:24,720 --> 00:05:33,480
actually works because it feels better, breaking preset, this is always nice, we like

30
00:05:33,480 --> 00:05:43,520
to, we like to kill and steal and line, cheat when we get away with it, the only way

31
00:05:43,520 --> 00:05:49,800
we do away with it, we do, we prevent ourselves from breaking preset, and from being immoral

32
00:05:49,800 --> 00:05:57,200
it's, it's by telling us how bad, ourselves are bad, it is bad being immoral, but again

33
00:05:57,200 --> 00:06:05,720
doesn't really, it's not uplifting, it's not a substitute, okay, if you're life just saying

34
00:06:05,720 --> 00:06:13,960
everything's bad, this is bad, that's bad, but when you do keep the preset, when you

35
00:06:13,960 --> 00:06:23,720
keep the rules, when you look at the does uplifting, when you feel the power of being a good

36
00:06:23,720 --> 00:06:39,320
person, it's this power that actually, the power of goodness, the power of being moral,

37
00:06:39,320 --> 00:06:45,280
of knowing that you're a ethical person, you're not stealing enough, that power is at this

38
00:06:45,280 --> 00:06:53,640
game, you sit and you think about how you've given freedom from fear to all beings,

39
00:06:53,640 --> 00:07:01,240
and by and, and by and, and the, the gift of fearlessness, no one needs to be afraid of

40
00:07:01,240 --> 00:07:16,240
you, this is what the precepts do, and it's a gift and it feels good.

41
00:07:16,240 --> 00:07:34,160
And if the same goes with some good green, with anger, even with delusion, with ignorance,

42
00:07:34,160 --> 00:07:40,960
they say ignorance is bliss, so it's nice to just live your life and not care, and not

43
00:07:40,960 --> 00:07:45,440
carry the world on your shoulders, or think about death, or think about getting sick or

44
00:07:45,440 --> 00:07:51,960
old age, or think about suffering, think about the world's problems, blissful to not think

45
00:07:51,960 --> 00:07:58,120
about these things, it's really a horrible philosophy, because your problems don't go away

46
00:07:58,120 --> 00:08:03,720
just because you're ignorant of them, it's really bliss, it's not sustainable, it's not rational,

47
00:08:03,720 --> 00:08:09,400
it's not reasonable, but the opposite seems even worse to have to live your life and worry

48
00:08:09,400 --> 00:08:15,240
about these things, what good does that do, what good does it do to concern yourself with

49
00:08:15,240 --> 00:08:25,240
the problems of life, the problems of the world, just makes you stressed and sick and upset,

50
00:08:25,240 --> 00:08:34,480
so ignorance is better, but the only thing better than ignorance is knowledge, and not the

51
00:08:34,480 --> 00:08:42,040
kind of knowledge, not knowledge of your problems, but understanding of your problems, maybe

52
00:08:42,040 --> 00:08:50,240
knowledge is a bad way, but understanding, so ignorance is bliss, but because knowledge

53
00:08:50,240 --> 00:09:00,680
is suffering, but understanding that's peace, once you understand your problems, they

54
00:09:00,680 --> 00:09:10,040
cease to be problems, and then thinking about them is not stressful, then they have no power

55
00:09:10,040 --> 00:09:19,040
of you, when they do come to pass, when they do rear their heads, they have no power

56
00:09:19,040 --> 00:09:31,080
of you, because you understand them, you know their true nature, you're not shocked

57
00:09:31,080 --> 00:09:45,040
or sad if I can, but surprised by them, and so these wisdom that comes through meditation

58
00:09:45,040 --> 00:09:57,880
to understand their experience, this is more powerful than ignorance, it's better than

59
00:09:57,880 --> 00:10:08,320
it, the similarly with the medicine, a person who steeps themselves in greed, anger and

60
00:10:08,320 --> 00:10:16,520
delusion follows their desires, even when it means not doing the right thing, it's like

61
00:10:16,520 --> 00:10:21,520
a person who doesn't take their medicine, a sick person who doesn't take the medicine

62
00:10:21,520 --> 00:10:28,680
and dies, because it's going to die, because you do the bad and do the wrong thing, it

63
00:10:28,680 --> 00:10:41,080
feels good, it's like why I take my medicine, it's just bitter and unpleasant, and then

64
00:10:41,080 --> 00:10:46,480
they die because of that, because they didn't take their medicine, they didn't do something

65
00:10:46,480 --> 00:10:53,920
that was unpleasant, sometimes you have to do it as unpleasant, you have to go against your

66
00:10:53,920 --> 00:10:59,440
desires, you have to go against your anger, be patient with other people, make you upset

67
00:10:59,440 --> 00:11:08,400
when things make you upset with the patient, bear with unpleasantness, sometimes you have

68
00:11:08,400 --> 00:11:18,480
to make yourself up, slap yourself in the face, face your problems, learn about your problems,

69
00:11:18,480 --> 00:11:23,360
understand your problems, rather than run away from them or ignore them or pretend they don't

70
00:11:23,360 --> 00:11:29,120
exist or drown yourself in drugs and alcohol, so they don't have to think about them,

71
00:11:29,120 --> 00:11:51,600
if you can actually understand them, cultivate wisdom about them, then you rejoice and

72
00:11:51,600 --> 00:12:07,160
truly, truly find truth, peace and happiness, then it's lasting substantial, meaningful,

73
00:12:07,160 --> 00:12:21,440
so interesting little quote, I'm quite busy this weekend, still not over, but I don't

74
00:12:21,440 --> 00:12:31,000
know if it's over the second, so tomorrow I'm canceling second time, I'll see you

75
00:12:31,000 --> 00:12:44,120
about more nine o'clock broadcasts, I don't know, I'm behind in my school, I've got a

76
00:12:44,120 --> 00:12:53,560
good catch stop, catch stop in the next week, thank you all for tuning in, good bye.

