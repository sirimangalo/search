Good evening everyone, welcome back to the evening dumber, tonight we are ending the
sabbasavasita and we've recently been over the topic of this section so I'm not
going to go into great detail but it's the kind of thing that it's okay to go over
tonight we're looking at the taints or defilements to be abandoned by
developing or cultivating bawana, bawana is a very important word in Buddhism, bawat comes
from bhu means existence or to be being, it's a simple word if you talk about
something existing you say it, it exists bawati, bawat, bawa is something coming into
being, it's existing or being, but bawana means causing something to be cut to be
it's an important word because it describes Buddhist practice quite well
Buddhist practice isn't about relaxing or running away Buddhism is very much
about developing and cultivating some meditation this is why I have to repeat
in the, let me get the door, why is someone marching into our house, this is new, so
the idea is to become something, to not become something sorry, to cause things
to come into being and specific things, we practice meditation, we are not just, what's the
problem, I guess now that I've got the speakers on people can hear us outside, that's
interesting, we're trying to cultivate certain qualities, so meditation isn't just
about closing your eyes and blanking your mind, there's quite a bit more to
meditation than people think, it's not even just a question about it being
about relaxing, meditation isn't exactly about one single thing in their
ways of summing it up, but there's quite a bit involved with meditation in many
qualities of mind, many facets to the quality of mind that we're trying to
cultivate meditation and Buddhist practice in general involves producing,
coming, causing to come into being, into being, many qualities of mind, we're talking
about qualities of mind that we're weak or non-existent before, we're not
talking about sound physical, not talking about cultivating anything
physical, Buddhism isn't about building monasteries or shaving off your
head, your hair putting on special clothes, it's not about getting to the
point where you can sit in full-load this posture, we're cultivating here's
qualities of mind, that's something I think takes a little bit of patience, but
should be a focus when we have the question of what are we getting out of this
practice, where focus is too often on how happy is it making me, right, which is
fine and good, but it smacks of potential impatience and addiction really to
pleasure, right, if I say to you meditation is it's supposed to make you happy,
well, we tend to avoid all the work, we tend to ignore all the work that has to
be done and focus very much on that happiness, when in fact it's our
obsession with happiness that makes us unhappy, if you want to talk about
success and progress in the practice, you have to ask about your state of mind
because happiness isn't something that's easy to come by, happiness requires a
strong and healthy and pure mind, a well-developed mind, so bhavana, the Buddha
said, a bhavitang zitang raghu samatiri jity, just like rain penetrates and
undeveloped and poorly-thached roof, so to the undeveloped mind a bhavitang
zitang, allows the defilements in, the defilements meaning those things that get us
caught up in addiction and aversion and suffering, we suffer because our minds
are undeveloped, not because we're working too hard, but because we're not
really working hard enough, we're not working in the right way, if you want to
be happy there are many qualities that you have to develop, most importantly
mindfulness and this is the first one, mindfulness always is the first, it's
very important that it be placed at the beginning, it's the one the Buddha said
is always useful when you refer to the other, the bowjang is what we're supposed
to be developing are the seven factors of enlightenment, bowjang, bowdianga,
angas, just a word that means member, bowdi means enlightenment, bowdi is
that moment when everything comes together, when one is perfect in all the
many factors and qualities, faculties and qualities of mind that come
together to create enlightenment, that's bowdi. So these seven factors are the
seven things that cultivate and have been cultivated, realize bowdi,
realize enlightenment, sati is the first, we'll talk a lot about sati and of course
this is how we come to see, this is as we talked about last time, how we abandon
the defilements temporarily and with the pure mind that comes from being
mindful, we're able to abandon and free ourselves, see clearly and through
seeing clearly, free ourselves. So once we've developed sati then there comes
dhammavicaya which is means in regards to the dhammas, the understanding that
comes about, the shedding the light, mindfulness is like a very powerful light
and dhammavicaya is where you analyze, not intellectually but you start to
react really or not react but taste, taste your reactions, dhammavicaya is
where you start to discriminate, where you really start to be able to judge
properly. We talk a lot about how judgment is a real problem but it's only
wrong judgment that's a problem or ignorant judgment. Once you start to be
able to see, once you see things clearly, you start to be able to judge things
properly and undeniably discriminate between good and bad. This is undeniably
bad, why? Well, I've seen it with perfect clarity, that's what we do in the
practice, it's like picking up the bad stuff and throwing it away and sorting
everything into good and bad, not because someone told you so or because you
think so or because you feel so, because you know so and you see. Once you do
that, well, to develop, you need to develop effort so you can have mindfulness
and through mindfulness comes this discrimination but for it to succeed, you
need effort, the third one is effort. This means the first two are really the
practice but for it to be successful, you need to put effort in. You can't just
do it once and give up, right? Sit down for ten minutes and that was that. Now
I've meditated, I'm a meditator, something that you have to do with Buddha
and Satatachakiri, I was saying now, I'm going to get into the habit or the activity
of this continuous, Satatachanins, continuous mindfulness and investigation.
It means when you're eating, eating should become a meditation. When you're walking
around, sitting down, lying down to sleep. When you're in the shower, when you're on
the toilet, everything you do, every moment, there's a moment when you can be
cultivating these qualities, takes effort. But once you cultivate effort and the
effort comes, another thing about it is once you're mindful and you start to see
things clearly, you free up a lot of effort. When we talk about being tired, we
think of being tired is because you've worked too hard, but it's oftentimes not
even a physical tired. It's a mental stiffness. It's like a tension of the mind
really. And once you're mindful, you suddenly feel all this energy come back.
Gradually feel all this energy come back. As you start to let go and ease up,
your mind becomes more focused and more alert. And you gain more energy and
then the final of the energy side is rapture. And so once you become energetic,
then you start to become, it becomes habitual. So rapture is this state of gaining
momentum, really, becoming powerful. Through putting out effort, it becomes
stronger and more, it's like inertia, really. When it starts to become, you get in
the groove, it becomes habitual and easier, really, more efficient. Rapture
refers to anything you get caught up in, really. That's why we use the word
rapture. So it's easy to get caught up in many different things. Here you start
to get caught up in mindfulness. And that's a really good sign. Once it becomes
second nature, where you just mindful constantly throughout the day, or it
becomes very much a part of who you are, that's rapture. You become in rapture
by mindfulness, sort of, what a great thing. So those ones, well mindfulness is in
the center, but the other three are on the effort side. And then we have, on the
other side of things, your mind becomes calmer, so bussed in, trying it, your
mind becomes more tranquil, samadhi, your mind becomes more focused. And to
be a guy, your mind, this is the most important one, your mind becomes more
equanimous. And once you've completely tranquilized the mind and the body, to the
point where you're able to be still, and you feel very sorted out every
experience, it comes orderly. It's still chaos, but it's a manageable chaos, and
there's a quiet tune to it, the stillness, there's no more defilement really.
Samadhi means you're focused, there's perfect clarity of focus on each object
as it arises. And dupeika means you have no judging. These are the qualities that
we try to develop. We put mindfulness in the center, the other ones are on the
side of effort and concentration. So if you have a lot of effort, then you have
to be clear that you're a lot of energy over energy, you're distracted and
restless. Then you have to be clear about that and start to focus on the other
three, calming your mind down, focusing a bit better, trying to be less jumpy and
less judgmental or reactionary, more equanimous. I mean, just acknowledging those
things helps you focus on, focus your mindfulness really. Mindfulness is the
tool you don't have to get too distracted by this idea of balancing things. It's
more of an intellectual thing for you to step back and say, uh-huh. Well, that's
where I'm obviously not being mindful or it's harder for me to be mindful
because I'm out of balance. And so knowing this helps you to focus your
mindfulness and make focus your efforts to be more mindful of those things
that are important. So this one, this one really pairs with the first one. Remember,
we had the first one was seeing in this suit. So seeing is our practice. We're
trying to see the truth. It's a very lofty sounding goal, but it's quite simple.
It is lofty, but it's simple. We're trying to understand what's going on. Why are
we suffering? We're trying to understand our minds, understand our experience,
understand reality as a mundane thing, as a process of experience that causes us
so much stress and suffering and to change that. To fix that, really. Fix that
we're seeing through, through seeing the problem, understanding the problem,
and naturally inclining or changing the way we incline our minds. And so the
other five that we've been through up to today are more supportive practices.
And indeed, each one of those supportive practices cuts off a part of the problem.
And these are so much, which means these sort of threads that are getting us off
balance that are leading to stress and suffering, really. But the main
practice is still seeing. And Bhavan has just come bringing it back home that as
you start to see and start practicing, there are qualities that are your
cultivating. So Bhavan is the qualities. It's the same with the Satipatanas who
are right. The Buddha goes through the Satipatanas who've done it at the very
end. Well, before getting into the truth, he talks about the factors of
enlightenment. We're developing through the practice. So this is this section,
in these seven things, mindfulness, investigation, effort, rapture, tranquility,
concentration, and equanimity. These are the seven factors of enlightenment.
This is something interesting about each other that I didn't mention. It's
one of these factors is based on, based on seclusion, dispassion, cessation, and
as giving up relinquishment as its ending, as its result. So the point
the stress here is on the fact that these are the qualities that lead to
seclusion or are associated with seclusion, dispassion, and cessation.
These are the qualities of mind. These are them. These are the way to
true seclusion in the sense of being secluded from the defilements, where
your mind is away from all that heat and stress and fever of defilement.
Well, dispassion and cessation, the cessation of suffering. And have letting go
is there ends. So we talk about letting go a lot in Buddhism. How does one
let go? Letting go is the result, barinami. It's the ending. And it comes from
all of these. It comes when you're mindful and when you develop all the
qualities that lead to enlightenment. And so in conclusion the Buddha says when
one has removed all of these defilements, the ones through seeing that should
be abandoned by seeing the ones through using, through enduring, through
avoiding, through removing, and through developing. Then one is called a
bikhu, who dwells restrained with restraint of all the tanes. It's not quite the
language I would use. It's a difficult language.
It dwells restrained, dwells restrained, having restrained all of the ever
strained is not the best word. Sabasa, as samma, or sambu-toh, having restrained them,
I guess. So there's the ass of again, it's kind of this metaphor of
streams or this, it's an imagery of these things that get us lost. It's like a
leak. So it means you've plugged all the leaks. Our mind ordinarily is at
peace. Why can't we just be here and now? Ordinary reality as we describe it
and as we think of it is really pure, but it's the ass of that get us impure. They
get us caught up in busyness and stress and friction. They're the poison. And so
once one has done that, done this, one is plugged up on leaks.
A ji-ji-tah-n-hong, one has cut off craving. We, what the yi-i-i-i-sang-yo-jah-n-hong, one is untied,
we, we what they, I don't know, one is pickabodhi-se, flung off the fetters, samma-mana-bisamaya-antamakasi-dukasa,
with the complete penetration of conceit is made the end of suffering, made an
end of suffering. So again, what's great about this suit does, it gives a
really comprehensive look at the practice. I mean, there's much more that could
be said about each one of these and there's many details and many ways of
implementing all the aspects of the practice, but this gives a good summary of
Buddhist practice really, and it comes back, starts at seeing and comes back
to cultivating, and really gives us some good pointers and good reminders on how
to keep our practice going and keep it going in the right direction. So that's
the samasa-visuta, and that's the demo for tonight. You can see if there are
questions. Is it useful to expose ourselves the situations we are averse to in
order to learn about them? For example, I have an aversion to reading, so it
would be useful to try to read something and study my reactions using the
noting technique. I think I just answered this one. I mean, it's not really a
good idea to intentionally evoke defilements because then you're
cultivating that triggering activity. I mean, part of what we're trying to learn
is neutrality or objectivity, and if you're triggering it, there's the real
danger of getting impatient or ambitious about the practice. If I do this, is it
useful, like this question, or is it useful? It's potentially ambitious,
where instead you should just live your life and then add mindfulness, right? Let
the changes come naturally. I mean, instead of going out of your way to read a
book, when you actually naturally are in a position where reading is the
appropriate thing to do, then study it. It's more natural that way. There's a
problem with sort of artificially evoking defilements, except in the most
innocuous sorts of ways, like doing sitting meditation, walking meditation,
potentially evokes quite a few problematic mind states, but it's quite benign,
I think, in the sense of it's just walking and it's just sitting, right? But going
out of your way, I think, is problematic. Who is Sonamahate or that Sayadha
mentions on page 87, a manual to insert? There are two Sonas, but the main one is
is about entered effort. He was a person who had too much push too hard. So I think
this is where the seemingly of the three strings comes in, right? You have
overzealousness with this, 87. We have rogue efforts made by Sonamahate,
this is different, right? Thinking about teacher reflecting on the attributes
considering the heroic efforts made by Sona. So Sona was so, if you want to know
about any of these sorts of people, the dictionary of polypropernames is a
really good resource. It can look up Sona there. I don't give you all the
information, but he was so delicate that there were hairs apparently growing on
the bottoms of his feet. And the king, I think, even asked to look at his feet
because he was astonished that someone could possibly have hair there. But he had
these fine hairs on the bottom of his feet. He was so delicate, apparently. And
so when he became a monk, he did walking meditation and his feet started
bleeding. I'm hoping getting the story right, but his feet started bleeding, but
he persisted. And it was because of him, I think, the Buddha eventually allowed
the monks to our sandals. Monks would go barefoot before that. But at one
point it was, I think it was an abrama. I don't think it was the Buddha. The
similarly of these three strings, if one is too, if a string is too tight, not
may have been the Buddha. The string is too tight. It snaps. If a string is too
loose, it doesn't play. The string has to be perfectly tuned. And that's how
your effort should be. You shouldn't just push too hard. So he worked really
hard, and that's a good thing, but in the end he worked too hard. But I
recommend looking up the dictionary of polypropernames because I may have
butchered some of that. Could you go over there not the luck and the suit next?
Sure. I'll do that maybe Saturday. I want to become a monk. How can I go ahead?
I'm not going to answer questions about how to become a monk because I think
it's misguided and no misleading. It's problematic because people are
doing people put there's too much. It's often the case that people put too
much attention and attention to ordination. And that actually takes away from
people's attention to meditation. It's seen as sort of a quick fix. And that
causes problems from monastery. So I shy away from focusing on that aspect of
Buddhism. Ordination is certainly possible, but it should be related to your
meditation practice. And in that case, it'll come to that for them. You'll be
at a center. And the opportunity will arise and you'll just take it. I mean, part
of the problem is that it's hard to find a center where it's a good place to
ordain. And it's hard for me to give a recommendation because there are many
different, whether lots of obstacles to becoming a monk and for those who
have become a monk in modern times. Come from a Jewish family. My family is
worried that one day I might get the idea of becoming a monk. They say for years
I slowly transform to a fundamentalist in my views through Buddhism. How could I
deal with it with it with it's ignorance? It's very difficult to talk to people
of no idea of them. My patients and practice on your own. And once you become
very comfortable in the practice, people can't help but over time see that it's
benefiting you. I mean, well, they can. But it becomes a matter of disassociating
yourself with such people or bringing them over to your side. Where they accept
the fact that it's a good thing.
I think I'm sorry if that doesn't really answer your question, but it's not
really an easily answered question. You can't just fix people's ignorance. You
can't just solve their concerns. I could very well be sorry put his mother
he was scolding him to the very end. Sorry put to who was great and the
great wisdom. And it took until he was dying for his mother to see the
goodness of what he was doing. There's another question about being a monk and
just deleted it. Yeah, I'm not really going to answer that. Again, I don't want
people to fixate on this idea of being a monk. I know it's very romantic and
everyone wants there's many questions about it. It's quite an interest and
that's a good thing. But maybe it's not such a good thing. I mean, I think I would
discourage that and I would encourage people to focus more on meditation. And
because becoming a monk, if you have to ask me, you're probably not there yet.
The only time it should be asked is when you're in a meditation, in a
monastery and you're talking to your monastic teacher and you're just looking
to further your meditation practice that you're already undertaking. If you're
asking me on the internet, I would say stick to your meditation for now or find
a place to go and do a meditation course. It's much more practical,
especially in modern times, to focus in that way. So that's all for tonight. Thank
you all for tuning in. Have a good night.
