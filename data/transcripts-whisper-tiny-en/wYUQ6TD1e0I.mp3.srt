1
00:00:00,000 --> 00:00:24,480
Good evening, everyone, broadcasting live, May 3rd, 2016.

2
00:00:24,480 --> 00:00:45,920
Tonight's quote is about karma, and more generally it's about views,

3
00:00:45,920 --> 00:00:51,440
it's about why we do things.

4
00:00:51,440 --> 00:00:59,680
It's about reasons for doing things, reasons for abstaining, reasons for abandoning.

5
00:00:59,680 --> 00:01:03,120
Why would we give up?

6
00:01:03,120 --> 00:01:08,040
Why would we give up the things we do?

7
00:01:08,040 --> 00:01:13,520
And we give up the things we want to do.

8
00:01:13,520 --> 00:01:21,200
Why would we give up the things we think are right to do?

9
00:01:21,200 --> 00:01:31,280
So the giants from this sutta we have the idea that the giants claim that whoever kills,

10
00:01:31,280 --> 00:01:40,960
steals, cheats or lies, that person goes to hell.

11
00:01:40,960 --> 00:01:51,040
According to how one habitually acts, one goes to one's destiny.

12
00:01:51,040 --> 00:02:02,160
So, up by a konar, you go, you go, you go to your dilemma, in time.

13
00:02:02,160 --> 00:02:09,840
We are young baholan, yam baholan, we are at the, and everyone does again and again.

14
00:02:09,840 --> 00:02:19,680
Whatever things one does much, one dwells in, whatever one dwells in much,

15
00:02:19,680 --> 00:02:25,760
dwells often in, day in and day in and day in and day in and day in and day.

16
00:02:25,760 --> 00:02:39,280
And they go according to that, or they are led to it, they are carried according to that.

17
00:02:39,280 --> 00:02:43,920
What sounds fairly Buddhist, right, sounds, but the Buddha doesn't like this,

18
00:02:43,920 --> 00:02:47,200
and it doesn't agree with this.

19
00:02:47,200 --> 00:02:56,640
He says, well in that case, he's quite smart, he says, what if in the, what if in,

20
00:02:56,640 --> 00:03:06,640
does he say, what if a man kills by day or night, or from time to time?

21
00:03:06,640 --> 00:03:13,520
Right, he says, well in that case, no one's going to go to hell, because someone doesn't

22
00:03:13,520 --> 00:03:20,160
kill habitually, they kill from time to time, but most of the time they're not killing.

23
00:03:20,160 --> 00:03:27,200
So if they go according to whatever's habitual to them, then the ones in a while killing

24
00:03:27,200 --> 00:03:44,400
is someone going to have any effect, nobody will go to hell.

25
00:03:44,400 --> 00:03:51,840
And then he gives this long speech, and this long paragraph, not long, but it gives

26
00:03:51,840 --> 00:03:57,520
a fairly, I mean, he looks at it more in depth.

27
00:03:57,520 --> 00:04:04,800
He says, but curiously, he doesn't provide a view, right?

28
00:04:04,800 --> 00:04:08,800
He says, well that, you know, here you have this view that if you do this, you're going

29
00:04:08,800 --> 00:04:12,400
to go to hell, right?

30
00:04:12,400 --> 00:04:28,000
It doesn't offer an opposing view, and steady says, the Buddha, all right, no, sorry, first

31
00:04:28,000 --> 00:04:36,880
he points out, he says, you know, suppose there's a teacher who teaches, teaches this doctor

32
00:04:36,880 --> 00:04:42,920
and that whoever does these things goes to hell, someone kills or steals it, they go to

33
00:04:42,920 --> 00:04:45,920
hell.

34
00:04:45,920 --> 00:04:51,960
Then suppose there's a follower, someone who follows them out of faith, you know, if that

35
00:04:51,960 --> 00:04:58,080
person should hear this and suppose that they have done these things in the past, then

36
00:04:58,080 --> 00:05:10,760
they would feel upset and holding that view, not giving up thinking about it, ruminating

37
00:05:10,760 --> 00:05:18,000
over it, he does indeed go to hell because he's so worried and upset and holding onto

38
00:05:18,000 --> 00:05:28,560
this fear and anger and it's disliking for self like this, like for the future and so on.

39
00:05:28,560 --> 00:05:36,480
But also for holding wrong view, for holding onto a view that isn't founded in reality,

40
00:05:36,480 --> 00:05:42,040
holding onto views is very dangerous because it puts you out of sync with reality and it

41
00:05:42,040 --> 00:05:51,280
warps your mind and it changes the way you act, changes the way you think, the way you look

42
00:05:51,280 --> 00:06:01,040
at the world, views are very dangerous, you should not hold onto views whatsoever, even

43
00:06:01,040 --> 00:06:05,200
views like this that seem right if you do bad things, you go to a bad place, we do good

44
00:06:05,200 --> 00:06:09,080
things, you go to a good place.

45
00:06:09,080 --> 00:06:20,640
Why is it not true, it's not true because it's over simplistic, it tries to oversimplify

46
00:06:20,640 --> 00:06:28,040
things, it tries to categorically state cause and effect beyond what is real, the

47
00:06:28,040 --> 00:06:32,000
language is really subject to cause and effect.

48
00:06:32,000 --> 00:06:37,800
So with a subject to cause and effect on our momentary states, if you kill, it's made

49
00:06:37,800 --> 00:06:44,360
up of a whole bunch of momentary states of intention to kill and the action of kill, the

50
00:06:44,360 --> 00:06:50,280
observation of the experience of killing memories of having killed but which are all

51
00:06:50,280 --> 00:06:57,680
momentary and all have their own power, but that's all that has power is those moments.

52
00:06:57,680 --> 00:07:01,400
And so they do change your mind and they do make it more likely that you're going to

53
00:07:01,400 --> 00:07:03,720
go to a bad destination.

54
00:07:03,720 --> 00:07:12,760
They certainly bring a bad result, but what those bad results are is quite complicated and

55
00:07:12,760 --> 00:07:18,360
dependent on many other factors, whether you're doing good things as well and so on.

56
00:07:18,360 --> 00:07:24,120
When they bring fruit is also up in the air, who knows if you do bad things now when

57
00:07:24,120 --> 00:07:32,120
it's going to bear fruit because there's so many other factors vying for prominence, if

58
00:07:32,120 --> 00:07:36,760
you do good things, then those might show first and then the bad things you've done might

59
00:07:36,760 --> 00:07:46,040
come to fruition later because how they mix with so many other things happening.

60
00:07:46,040 --> 00:07:50,120
It's like the physical world, it's like trying to predict the weather.

61
00:07:50,120 --> 00:07:58,520
We know that the humans are having an effect on the global climate, but we can't predict

62
00:07:58,520 --> 00:08:04,120
exactly what that's going to be. We can't predict from one day to the next, but we can

63
00:08:04,120 --> 00:08:06,520
say, oh, we're doing something that's actually changing.

64
00:08:06,520 --> 00:08:12,360
And so karma is like this, so you can look at someone who gives me well that person's

65
00:08:12,360 --> 00:08:18,120
heading towards health and if they don't change then eventually they're going to go to

66
00:08:18,120 --> 00:08:25,560
a bad place, but you can't make it a view.

67
00:08:25,560 --> 00:08:34,200
And so the Buddha contrasts this with what the Buddha does and he says he censures strongly

68
00:08:34,200 --> 00:08:42,440
censures, killing, stealing, cheating, lying, saying abstain from these things.

69
00:08:42,440 --> 00:08:45,800
It doesn't say person who does these will go to hell, etc.

70
00:08:45,800 --> 00:08:53,680
It doesn't put a view on them, he says abstain from them and he says that they're harmful.

71
00:08:53,680 --> 00:09:00,200
So a person might feel bad because they've done them, but it encourages them to change.

72
00:09:00,200 --> 00:09:06,720
And that's another important aspect of Buddhism is the potential to change, the potential

73
00:09:06,720 --> 00:09:14,160
to start fresh, like with Angulimala who killed so many people and then became a monk

74
00:09:14,160 --> 00:09:16,280
and became enlightened.

75
00:09:16,280 --> 00:09:23,160
Or even Devadatta, who, though he's bound to hell and he's living in hell right now

76
00:09:23,160 --> 00:09:34,600
as we understand, still was able to change his mind at the last moment and is now considered

77
00:09:34,600 --> 00:09:40,840
to be headed in the right direction, to get sort of hell, he'll move on and become enlightened

78
00:09:40,840 --> 00:09:51,200
himself.

79
00:09:51,200 --> 00:10:02,720
And so, so there's a potential, this person sees, who hears the Buddha say abstain from

80
00:10:02,720 --> 00:10:13,440
these, they're not well done, they're not good, this person changes and does good things.

81
00:10:13,440 --> 00:10:18,800
And then the Buddha gives, he talks about how they have caused, how they have an effect.

82
00:10:18,800 --> 00:10:25,560
They're doing abstaining from evil is not just because someone says it's right or wrong.

83
00:10:25,560 --> 00:10:33,360
It's about getting people to stop doing these things so they can start to experience happiness.

84
00:10:33,360 --> 00:10:40,120
So he says, this noble disciple freed from greed and hatred, I'm sorry, not yet, by

85
00:10:40,120 --> 00:10:47,480
abandoning killing, by abandoning stealing, etc., etc., by bending all these things, by

86
00:10:47,480 --> 00:10:52,800
abandoning greed, he becomes generous, by abandoning hatred, he becomes kind, by abandoning

87
00:10:52,800 --> 00:11:17,040
wrong view, he becomes one with perfect view, so that's creed, anger, and delusion.

88
00:11:17,040 --> 00:11:27,640
I'm being young, I'm being young, I'm being a Hindu, we are a part of those, I'm being

89
00:11:27,640 --> 00:11:34,440
a panochito, I'm being a addicting, I'm a addicting, I'm a addicting, I'm actually

90
00:11:34,440 --> 00:11:35,440
quite simpler.

91
00:11:35,440 --> 00:11:44,980
Not as eloquent, it's actually just giving up abandoning desire, becomes one who

92
00:11:44,980 --> 00:11:53,460
It was free from the sire, abandoning anger and ill will, one becomes one who has a mind

93
00:11:53,460 --> 00:12:09,140
free from ill will, abandoning wrong view, one becomes one who has right view.

94
00:12:09,140 --> 00:12:15,500
And then free from greed and hatred not bewildered, but mindful and concentrated.

95
00:12:15,500 --> 00:12:25,420
They abide with Mehta, Karuna, Mudita, and Deepika, the four Brahmui Haras.

96
00:12:25,420 --> 00:12:30,220
And the Buddha often does this, he talks about the four Brahmui Haras, not as a practice,

97
00:12:30,220 --> 00:12:35,260
but more as a result of practice, and a result of right practice.

98
00:12:35,260 --> 00:12:38,620
Meaning if you practice mindfulness,

99
00:12:38,620 --> 00:12:44,700
you practice to see things clearly, the four Brahmui Haras come naturally.

100
00:12:44,700 --> 00:12:52,060
It's important to understand people who try to perfect the Brahmui Haras in advance are doomed to failure,

101
00:12:52,060 --> 00:12:57,140
because the Brahmui Haras will not eradicate greed, anger, and delusion.

102
00:12:57,140 --> 00:13:03,580
If you work to eradicate them, the four Brahmui Haras can be a support,

103
00:13:03,580 --> 00:13:07,700
but they don't become perfect, they don't become purified

104
00:13:07,700 --> 00:13:11,260
until one is free from greed, anger, and delusion,

105
00:13:11,260 --> 00:13:16,100
because they're always going to be tainted by these things.

106
00:13:16,100 --> 00:13:23,140
Love can be tainted by greed, compassion can be tainted by anger, Mudita,

107
00:13:23,140 --> 00:13:28,780
or Peka can be tainted by delusion.

108
00:13:28,780 --> 00:13:33,100
They can even though you practice them,

109
00:13:33,100 --> 00:13:37,500
they can be perverted by the unhulsiveness of the mind.

110
00:13:37,500 --> 00:13:46,900
Until that unhulsiveness is purified.

111
00:13:46,900 --> 00:13:52,460
And so when one does it this way,

112
00:13:52,460 --> 00:13:57,620
nothing whatsoever is left out of the love, the compassion, the sympathetic joy,

113
00:13:57,620 --> 00:14:04,900
but that equanimity.

114
00:14:04,900 --> 00:14:09,500
So this is about abandoning, it's about abandoning things.

115
00:14:09,500 --> 00:14:11,940
We don't abandon things because of views.

116
00:14:11,940 --> 00:14:16,020
We abandon things because of the benefits that come from abandoning.

117
00:14:16,020 --> 00:14:28,380
We've talked about this before that monks were unwilling or hesitant to give up.

118
00:14:28,380 --> 00:14:31,820
Simple things like not eating in the evening,

119
00:14:31,820 --> 00:14:37,100
but when they actually gave these things up and just acted according to necessity

120
00:14:37,100 --> 00:14:42,020
rather than according to desire, they found that they were much happier.

121
00:14:42,020 --> 00:14:50,940
They were free from so much suffering, so much stress and problems.

122
00:14:54,940 --> 00:15:00,540
It's like in Christianity you have this thing called Lent where they give things up.

123
00:15:00,540 --> 00:15:07,900
The Buddhism, it's not so much, it's not considered to be a hardship to give something up.

124
00:15:07,900 --> 00:15:14,900
We keep things up in a joyful way like I'm free, I'm free from this.

125
00:15:14,900 --> 00:15:20,660
It's something that we feel proud of that we're finally free of that crippling desire,

126
00:15:20,660 --> 00:15:27,100
that crippling addiction that only caused us more suffering and stress.

127
00:15:27,100 --> 00:15:31,380
That we're happier, we've become happier from giving up.

128
00:15:31,380 --> 00:15:37,580
That's really the key, we've become happier by giving up.

129
00:15:37,580 --> 00:15:40,820
And so that's what we work for.

130
00:15:40,820 --> 00:15:49,100
So that's our bit of dhamma for this evening.

131
00:15:49,100 --> 00:15:54,140
I see there are a bunch of questions, so I'll take them on.

132
00:15:54,140 --> 00:16:01,620
A few questions anyway, let's read through them.

133
00:16:01,620 --> 00:16:04,780
Is 10 feet enough room to practice walking meditation?

134
00:16:04,780 --> 00:16:05,780
Yeah.

135
00:16:05,780 --> 00:16:09,780
I mean you have to remember walking meditation isn't about walking, it's about lifting

136
00:16:09,780 --> 00:16:17,140
your feet and being aware of the movement of the feet, each piece by piece.

137
00:16:17,140 --> 00:16:20,980
I actually taught walking meditation to a group of kids just having them lift their feet

138
00:16:20,980 --> 00:16:27,340
up and place them down, lifting, lacing, because it wasn't enough room, you know how kids

139
00:16:27,340 --> 00:16:30,100
are if you get them walking, it wouldn't have been very mindful.

140
00:16:30,100 --> 00:16:43,860
So I just had them lift their feet, the same thing really.

141
00:16:43,860 --> 00:16:47,300
If the world were to end tomorrow, what would happen to all the beings that were meant

142
00:16:47,300 --> 00:16:48,300
to be reborn?

143
00:16:48,300 --> 00:16:55,980
The world does eventually end, being, to be reborn there has to be a physical base, to

144
00:16:55,980 --> 00:16:59,100
be reborn in the world, there doesn't have to be a physical base.

145
00:16:59,100 --> 00:17:04,100
So if that physical base and when that physical base isn't there, they will move on and

146
00:17:04,100 --> 00:17:09,300
they will seek out a base to be born where they can be reborn.

147
00:17:09,300 --> 00:17:20,740
You have to remember, we're not born into a living being, we're born into a womb where

148
00:17:20,740 --> 00:17:26,140
there's only these two cells, so we see, here's a start and we start there and we work

149
00:17:26,140 --> 00:17:29,940
together with the body to create that being.

150
00:17:29,940 --> 00:17:48,980
The mind is involved in the gestation process, so the mind will have to find a place that

151
00:17:48,980 --> 00:17:55,980
it can cling to, there's no human womb to cling to, then it will find somewhere else.

152
00:17:55,980 --> 00:18:00,500
So they'll probably be born and haven't in, as angel angelic types of beings, as beings

153
00:18:00,500 --> 00:18:10,860
out in space, kind of thing.

154
00:18:10,860 --> 00:18:32,620
Why are we talking about football?

155
00:18:32,620 --> 00:18:35,980
Someone won the, won the bet.

156
00:18:35,980 --> 00:18:43,700
It could be good karma, it could be just the karma of buying the ticket, buying the

157
00:18:43,700 --> 00:18:49,100
ticket leads you to win, it's not necessarily something from past life, actually winning

158
00:18:49,100 --> 00:18:54,500
lottery can be very painful and stressful, a lot of people commit suicide after winning

159
00:18:54,500 --> 00:19:03,100
the lottery, or are murdered in order to rob them of their money, lots of bad things happen,

160
00:19:03,100 --> 00:19:06,700
become depressed, they become alcoholics, they become drug addicts, there's an article

161
00:19:06,700 --> 00:19:10,580
about it in it, very interesting.

162
00:19:10,580 --> 00:19:15,420
So to say it's good karma, it may be because they've done lots of bad things that they

163
00:19:15,420 --> 00:19:24,260
had to win the lottery, it's because people want their money, you know, everyone's going

164
00:19:24,260 --> 00:19:33,420
to come asking for their first share, or you know, to lend borrow money, so many problems.

165
00:19:33,420 --> 00:19:39,300
When it comes to do not kill what is there to say about law enforcement or self-defense,

166
00:19:39,300 --> 00:19:46,780
no killing, nothing to say, not even any harming, you know, if you beat, if you hit someone

167
00:19:46,780 --> 00:19:52,620
in self-defense, you know, it's, you have to be careful, you know, it's allowed, monks

168
00:19:52,620 --> 00:20:00,020
are allowed to hit someone in self-defense, but better be measured, better not be out

169
00:20:00,020 --> 00:20:09,700
of anger, I've heard of Bawa being referred to also as habitual tendency when talking

170
00:20:09,700 --> 00:20:13,420
about dependent origination, would you say that this is precise, if not could you please

171
00:20:13,420 --> 00:20:19,300
give you a take on how to understand Bawa, Bawa is karma Bawa, that's how the commentary

172
00:20:19,300 --> 00:20:24,540
describes it, so that's maybe a bit controversial, some people don't like the commentaries,

173
00:20:24,540 --> 00:20:42,460
but kama Bawa means your desire leads you to create karma, so yeah, so for example, if

174
00:20:42,460 --> 00:20:49,460
you want to, suppose you want to get a bachelor degree, well then you go to university

175
00:20:49,460 --> 00:20:56,100
and the karma of going to university is that, suppose you want to kill lots of people

176
00:20:56,100 --> 00:21:02,660
let's say, well then that leads you to actually go and do the killing, now Bawa being

177
00:21:02,660 --> 00:21:08,500
referred to as habitual tendencies, I would say it's actually not quite accurate because

178
00:21:08,500 --> 00:21:13,940
it doesn't have to be habitual tendencies, but that's one way of being reborn, you

179
00:21:13,940 --> 00:21:20,900
reborn because of habitual tendency, but you can also be reborn, for example, by a single

180
00:21:20,900 --> 00:21:27,780
lead, if you kill your mother or your father, then that is what will lead, that one deed,

181
00:21:27,780 --> 00:21:31,180
regardless of what habitual tendencies you might have, but that one deed will lead you

182
00:21:31,180 --> 00:21:38,140
to hell, for example, if you've done bad things but then you enter into the John, then

183
00:21:38,140 --> 00:21:46,100
that one deed, that one, if you just quickly, you know, if you work for a short period

184
00:21:46,100 --> 00:21:50,900
of time and you obtain the Janus and then you die, I understand you go to the Brahma

185
00:21:50,900 --> 00:21:57,620
war, even though you've done, you may have, because you can suppress it, so no Bawa

186
00:21:57,620 --> 00:22:04,340
means, according to the commentaries it means karma, Bawa is the becoming, so you want

187
00:22:04,340 --> 00:22:10,260
something, well that's still in your mind, the becoming is when you actually make it happen.

188
00:22:10,260 --> 00:22:19,060
Now that also refers, I think, to the moment of conception, so you want to be reborn and

189
00:22:19,060 --> 00:22:28,820
therefore you act and are reborn, Bawa, I'm not quite clear on that, but I think Bawa

190
00:22:28,820 --> 00:22:35,180
is being reborn in the womb and now I'm not sure, I don't think that's correct, but I

191
00:22:35,180 --> 00:22:46,940
have to look it up, sorry, not a very good teacher, don't even know my theory.

192
00:22:46,940 --> 00:22:50,940
What role would karma play in killing by proxy, killing by proxy, the Buddha did

193
00:22:50,940 --> 00:22:55,900
address this, because there was the belief that if you tell someone to kill your guilty

194
00:22:55,900 --> 00:22:59,940
of the karma of killing and the Buddha said no, your guilty of the karma of telling

195
00:22:59,940 --> 00:23:05,780
someone to kill, believe this is something Buddha said, and that's important, again referring

196
00:23:05,780 --> 00:23:13,140
to our quote, it's not about the act or how you know views about this leads to this,

197
00:23:13,140 --> 00:23:18,780
that leads to that, it's about the mind-states, what's involved with wanting someone to kill,

198
00:23:18,780 --> 00:23:23,340
that's all you have to ask, whatever mind-states are involved, that's what the result

199
00:23:23,340 --> 00:23:26,140
is going to bring.

200
00:23:26,140 --> 00:23:29,820
So there might be a lot of good involved with killing, in certain instances, you want to

201
00:23:29,820 --> 00:23:34,580
help other people, you want to protect other people, so there's good intentions, but there's

202
00:23:34,580 --> 00:23:39,700
also a lot of bad, because you want to harm someone, because as we were talking about yesterday

203
00:23:39,700 --> 00:23:53,420
with the Dhammapanda, which turns out I've already recorded, so I'm not going to upload.

204
00:23:53,420 --> 00:24:09,100
Very lastly, I trained upside, anyway, if you killed by proxy, the mind-states involved

205
00:24:09,100 --> 00:24:15,340
with telling someone to kill and get involved with someone killing.

206
00:24:15,340 --> 00:24:21,580
Alright, the idea that it doesn't matter, it's not a matter of you killing someone else

207
00:24:21,580 --> 00:24:35,180
and them dying, it's the sense of desiring to live and yet acting to kill, which is out

208
00:24:35,180 --> 00:24:52,180
of line with your own desires, with your own hopes and wishes, your own intentions.

209
00:24:52,180 --> 00:25:18,180
They give us me to read her article, others put the smokes involved, I see.

210
00:25:18,180 --> 00:25:28,740
But ten monks from the temple are flown to Britain to bless the player, other club has

211
00:25:28,740 --> 00:25:39,540
a tie order and bless the players before they kick off.

212
00:25:39,540 --> 00:25:46,580
They spend the match deep in meditation, the monks meditate for these guys, and this

213
00:25:46,580 --> 00:25:57,180
is supposed to be Buddhist, not impressed.

214
00:25:57,180 --> 00:26:02,260
The fans outside do a good job of letting them know which way the game is going.

215
00:26:02,260 --> 00:26:08,260
We know how they're doing because of the cheers and chanting.

216
00:26:08,260 --> 00:26:13,340
We feel the vibrations, not cool, I'm not impressed.

217
00:26:13,340 --> 00:26:18,900
That's not what meditation is for, I mean, put the heck, you know, what happened to

218
00:26:18,900 --> 00:26:32,100
being objective, now they're siding with a football team, but what a wholesomeness

219
00:26:32,100 --> 00:26:35,220
could that possibly bring?

220
00:26:35,220 --> 00:26:42,420
Why would you bless one team over another, you know, and would us say winning breeds,

221
00:26:42,420 --> 00:26:43,420
enmity?

222
00:26:43,420 --> 00:26:52,140
When you win, there's a loser, why would a Buddhist monk side on one side or the other?

223
00:26:52,140 --> 00:26:53,940
I'm unimpressed.

224
00:26:53,940 --> 00:27:03,700
You know, if the other team was a bunch of psychopats, well, they're okay, and somehow winning

225
00:27:03,700 --> 00:27:12,420
meant that good would prevail, but it's not a good and evil kind of thing.

226
00:27:12,420 --> 00:27:20,660
It's not like Bernie Sanders winning the Democratic North face.

227
00:27:20,660 --> 00:27:23,420
Not impressed with these guys.

228
00:27:23,420 --> 00:27:29,100
That's worse to me than those monks with that plane, everyone was up in arms about these

229
00:27:29,100 --> 00:27:30,660
monks who had a jet plane.

230
00:27:30,660 --> 00:27:35,140
I don't know the details, I guess there were some worse details, but just owning a plane

231
00:27:35,140 --> 00:27:39,940
to me as well, okay, you know, monk's own cars, monk's own planes.

232
00:27:39,940 --> 00:27:46,700
I can understand people getting offended by it, but really, it's just a mode of transportation.

233
00:27:46,700 --> 00:27:54,220
Theoretically, it could be used for good, but blessing a football team, that's just wrong.

234
00:27:54,220 --> 00:27:59,860
You know, blessing people, if they ask to be blessed, that's fine, but maybe they're

235
00:27:59,860 --> 00:28:04,900
not actively involved, but it sounds like they're actively siding with one team.

236
00:28:04,900 --> 00:28:19,220
They should go out and bless both teams, which are really absurd, not impressed.

237
00:28:19,220 --> 00:28:23,780
But I guess your point is that the good karma of paying respect to these monks helps

238
00:28:23,780 --> 00:28:30,060
them win the game, potentially, sure, because it means their minds are clear during the

239
00:28:30,060 --> 00:28:33,940
time that they're playing, they're thinking about the monks, but it's a total abuse of

240
00:28:33,940 --> 00:28:39,300
monastic, monastic life, you know, yeah, monks can do lots of things.

241
00:28:39,300 --> 00:28:45,300
I know a monk who's a doctor, heels people's sickness, as I still don't think it's right.

242
00:28:45,300 --> 00:28:46,980
Buy and monks can do these things.

243
00:28:46,980 --> 00:28:50,980
Monks have the power to help people help football players win their matches.

244
00:28:50,980 --> 00:28:57,220
It doesn't make it right, it's just the wrong way of going about things.

245
00:28:57,220 --> 00:29:01,260
Things people think that put some important, think that Buddhists believe it's important

246
00:29:01,260 --> 00:29:02,620
to win football.

247
00:29:02,620 --> 00:29:08,660
Anyway, enough about that.

248
00:29:08,660 --> 00:29:12,220
I'm not mocking you, please don't, I think it's an interesting thank you for bringing

249
00:29:12,220 --> 00:29:17,860
me the article, but I certainly have some opinion about it, so thanks for bringing it

250
00:29:17,860 --> 00:29:20,900
to my attention.

251
00:29:20,900 --> 00:29:24,700
Other than accumulating good karma should we be aiming for the cessation of all karma,

252
00:29:24,700 --> 00:29:26,100
is this the correct understanding?

253
00:29:26,100 --> 00:29:30,220
You can't aim for the cessation of all karma.

254
00:29:30,220 --> 00:29:31,220
Let me see.

255
00:29:31,220 --> 00:29:34,140
Yeah, I suppose that's true, you can't, yes, that's true.

256
00:29:34,140 --> 00:29:39,900
And technically, because karma is your intentions, an hour hand doesn't create new karma,

257
00:29:39,900 --> 00:29:45,140
so there is the eradication, but it's not the eradication of all karma, because past

258
00:29:45,140 --> 00:29:50,860
karma can't be eradicated, it's already happened, so the result will come.

259
00:29:50,860 --> 00:29:57,020
If it has a chance, if it has a chance to come, so you're working for the eradication

260
00:29:57,020 --> 00:30:06,820
of ignorance and of defilement, which leads to karma, leads to new karma, but the results

261
00:30:06,820 --> 00:30:13,380
of old karma, as long as you're still in Samsara, they will come now when the monk

262
00:30:13,380 --> 00:30:22,260
passed away, sorry, when an hour hand passes away, future karma, or past karma that hasn't

263
00:30:22,260 --> 00:30:27,820
given it, that has a result in the future, loses its opportunity.

264
00:30:27,820 --> 00:30:36,460
So you don't actually eradicate that result, it has no opportunity, because of cessation,

265
00:30:36,460 --> 00:30:41,420
because of Nibbana, but yeah, I guess basically what you're saying, but you have to really

266
00:30:41,420 --> 00:30:49,660
explain that, because it sounds kind of strange to someone who doesn't understand Buddhism.

267
00:30:49,660 --> 00:30:55,380
So it's too easy to misunderstand cessation of all karma, to mean just doing nothing, that's

268
00:30:55,380 --> 00:30:56,780
not true.

269
00:30:56,780 --> 00:31:02,260
cessation of karma means that an hour hand still does good things, but they have no intention,

270
00:31:02,260 --> 00:31:08,620
no desire for that to bring a result, no greed, anger, delusion about it, no identification.

271
00:31:08,620 --> 00:31:17,460
It just do as a matter of course, things that are pretty much going with the flow.

272
00:31:17,460 --> 00:31:25,420
A new chicken was well taken care of, what would be the karmic implications of choosing

273
00:31:25,420 --> 00:31:31,620
and eating its unfurlyzed eggs as a food source be.

274
00:31:31,620 --> 00:31:39,580
Yeah, I mean, I've argued that you could look at it as kind of a livelihood for the chickens,

275
00:31:39,580 --> 00:31:46,740
and if you're taking care of them, which is hard to do, chickens are not very nice animals,

276
00:31:46,740 --> 00:31:51,020
and if you want to take care of them, when I was young, we took care of chickens.

277
00:31:51,020 --> 00:31:56,740
It's easy to mistreat them, it's much easier to mistreat them than to treat them well,

278
00:31:56,740 --> 00:32:01,620
so if you can claim that you're actually treating well, and on the other hand, you know, treating

279
00:32:01,620 --> 00:32:06,660
them passively well, because we don't have to, and no one has to live in luxury, and it's

280
00:32:06,660 --> 00:32:08,660
not to anyone's benefit to them actually.

281
00:32:08,660 --> 00:32:12,300
If you're taking care of them, you could think of it as some kind of livelihood.

282
00:32:12,300 --> 00:32:17,980
No, you could argue there's a problem with the fact that the birds consider the eggs

283
00:32:17,980 --> 00:32:22,300
to be their children and tend to be fairly protective of them, so if you've ever taken

284
00:32:22,300 --> 00:32:26,260
eggs from underneath the chicken, they're not very happy about it.

285
00:32:26,260 --> 00:32:29,380
And they have ways of getting around that as well, you can have these, I guess they have

286
00:32:29,380 --> 00:32:34,140
these machines now where the eggs actually set up to where the eggs actually roll down

287
00:32:34,140 --> 00:32:40,380
after the chicken lays them, I've seen those.

288
00:32:40,380 --> 00:32:42,900
So yeah, I wouldn't be that big of a deal.

289
00:32:42,900 --> 00:32:47,380
Milk would be the same, I suppose.

290
00:32:47,380 --> 00:32:53,860
There's something to be said about keeping animals in captivity that is looked down upon.

291
00:32:53,860 --> 00:33:02,100
The fact that you're locking them up, the fact that you're controlling them, and ultimately

292
00:33:02,100 --> 00:33:09,060
you tend to get angry at them frustrated and cruel, it's easy to fall into that.

293
00:33:09,060 --> 00:33:15,500
If you can avoid it, then there are worse things still problematic because of the captivity

294
00:33:15,500 --> 00:33:17,700
I would think.

295
00:33:17,700 --> 00:33:28,500
Bernie Sanders is winning Indiana here, too little to that, I think it's okay.

296
00:33:28,500 --> 00:33:33,780
Now the important thing about Bernie Sanders is the good that he talked about, how he

297
00:33:33,780 --> 00:33:39,700
talks about a moral economy, how he echoes Pope Francis, who's also, I think, a good

298
00:33:39,700 --> 00:33:45,860
guy, how they're interested in helping people.

299
00:33:45,860 --> 00:33:51,020
One pointed out to me how angry he is and how angry his followers are, I think that's

300
00:33:51,020 --> 00:33:52,020
important to acknowledge.

301
00:33:52,020 --> 00:34:01,020
We can't side with these people 100% because they're not perfect, you know?

302
00:34:01,020 --> 00:34:07,100
But we can appreciate, and we can side with them because their hearts are in the right

303
00:34:07,100 --> 00:34:11,540
place, in the sense that their views are in the right place, that's what that way they

304
00:34:11,540 --> 00:34:24,100
seem to have wholesome views that are amenable to spirituality and enlightenment, goodness.

305
00:34:24,100 --> 00:34:25,980
How does one prevent fruition of karma?

306
00:34:25,980 --> 00:34:32,420
Well, karma has many types, so there's karma that brings fruit, there's karma that supports

307
00:34:32,420 --> 00:34:38,820
other karma to bring fruit, there's karma that destroys the fruit of other karma, and

308
00:34:38,820 --> 00:34:43,860
there's karma that weakens the fruit of other karma, or types of karma.

309
00:34:43,860 --> 00:34:47,380
I mean, that just points out how complicated it is.

310
00:34:47,380 --> 00:34:55,260
So if you've done bad things, doing good things can outweigh the bad and actually free

311
00:34:55,260 --> 00:35:01,780
you from the results, and they nullify the cancel each other out, that is true, it's

312
00:35:01,780 --> 00:35:06,300
very possible.

313
00:35:06,300 --> 00:35:10,340
And it's all about making the mind as neutral as possible and avoiding excitement and

314
00:35:10,340 --> 00:35:11,340
distance.

315
00:35:11,340 --> 00:35:23,140
It's not about avoiding, meditation is all about the root's quote, quote in the article

316
00:35:23,140 --> 00:35:32,220
is it, I don't know, but yeah, that's one kind of meditation.

317
00:35:32,220 --> 00:35:37,900
When it says what meditation is about, it's kind of cringe-worthy, because what does that

318
00:35:37,900 --> 00:35:39,300
mean?

319
00:35:39,300 --> 00:35:42,580
How many types of meditation are that there's as many types of meditation as there are

320
00:35:42,580 --> 00:35:49,820
minds, they can meditate on anything in any way?

321
00:35:49,820 --> 00:35:51,660
So that's one way of meditation.

322
00:35:51,660 --> 00:35:57,180
Buddhism, what is the purpose of practicing meditation in Buddhism?

323
00:35:57,180 --> 00:36:00,700
To see things clearly, that's clearly responsible.

324
00:36:00,700 --> 00:36:05,460
That leads to neutrality, but that's the result, not the practice, it can't force it.

325
00:36:05,460 --> 00:36:07,260
Before said it's artificial.

326
00:36:07,260 --> 00:36:17,300
It's not about avoiding, it's about seeing excitement, seeing this journey.

327
00:36:17,300 --> 00:36:33,980
Elephants are very poorly treated, because elephants have a hard time because they don't

328
00:36:33,980 --> 00:36:36,220
belong in a human world.

329
00:36:36,220 --> 00:36:39,580
They don't fit into the human world, not easily, anyway, it's true, it's supposed to

330
00:36:39,580 --> 00:36:51,540
be very difficult to train ethically, and so they train them with a spike, how it is

331
00:36:51,540 --> 00:36:58,380
they have a stick with a very small spike, and when they're young, they poke them with

332
00:36:58,380 --> 00:37:06,020
the spike right here, it's very painful, and so they eventually develop a scar here, and

333
00:37:06,020 --> 00:37:12,180
so they poke them there, and they come to know, they come to fear, they're trainer, and

334
00:37:12,180 --> 00:37:15,100
they become kind of neurotic about it.

335
00:37:15,100 --> 00:37:20,540
Elephants that are trained, you can see it in their eyes and in the way they behave,

336
00:37:20,540 --> 00:37:28,460
they're messed up, it's really sad action, not really for elephant training, because

337
00:37:28,460 --> 00:37:33,260
they torture them, that's how they do it.

338
00:37:33,260 --> 00:37:36,580
I mean, actually, I don't know, there may be ethical ways, and there may be people doing

339
00:37:36,580 --> 00:37:45,260
it ethically, but I'm not really impressed.

340
00:37:45,260 --> 00:37:52,140
Yeah, I wouldn't worry too much about eating again, it's about karma isn't about the

341
00:37:52,140 --> 00:37:58,900
act, it's about the intent, it's about the state of mind, but there are worse things

342
00:37:58,900 --> 00:38:17,380
to worry about, and dead chicken embryos, dead unfertilized chicken embryos, anyway, that's

343
00:38:17,380 --> 00:38:23,100
all, then we'll say goodnight, and we'll maybe do a demo cloud again tomorrow, I hope

344
00:38:23,100 --> 00:38:30,100
I get the right one this time, have a good night everyone.

