1
00:00:00,000 --> 00:00:16,360
So I just sort of an addendum to my set of videos on how to meditate, I've received quite

2
00:00:16,360 --> 00:00:24,880
a few comments and some comments and some questions about the videos and about how to meditate.

3
00:00:24,880 --> 00:00:32,200
So I thought a good continuation of the series would be to answer some questions that

4
00:00:32,200 --> 00:00:40,160
people have about the meditation practice once they've started to practice meditation.

5
00:00:40,160 --> 00:00:47,920
So the first question which often comes up is in regards to the actual meditation practice

6
00:00:47,920 --> 00:00:58,360
itself, the act of acknowledging or noting the phenomena which arise during the meditation.

7
00:00:58,360 --> 00:01:02,640
So in the beginning we have meditators acknowledge the stomach as it rises and falls

8
00:01:02,640 --> 00:01:09,560
and say to themselves rising and people wonder exactly what it means to say to oneself

9
00:01:09,560 --> 00:01:13,920
or to note rising and to note falling.

10
00:01:13,920 --> 00:01:20,560
The point is to not only be aware of the object but also to straighten out the mind in

11
00:01:20,560 --> 00:01:24,880
regards to the object, normally the mind considers that the rising and the falling is

12
00:01:24,880 --> 00:01:29,160
me as mind as the stomach is my body and so on.

13
00:01:29,160 --> 00:01:35,720
And it does this with everything, we're only taking the rising and falling as an example.

14
00:01:35,720 --> 00:01:40,520
Now when we say to ourselves instead rising, when we say to ourselves falling, we remind

15
00:01:40,520 --> 00:01:46,440
ourselves that the word mindfulness comes from the word setty, which means to remember.

16
00:01:46,440 --> 00:01:51,360
So we're reminding ourselves that this is only rising, this is only falling, it's not

17
00:01:51,360 --> 00:01:56,760
me, it's not mind, it's not good, it's not bad, it's not any number of things which

18
00:01:56,760 --> 00:01:59,320
we might judge about it.

19
00:01:59,320 --> 00:02:06,160
And we do this with everything in order to keep the mind straight and to keep the mind

20
00:02:06,160 --> 00:02:11,360
in line with the ultimate reality of the phenomenon.

21
00:02:11,360 --> 00:02:17,400
So the method of doing this is not simply to say the words in your head, first of all,

22
00:02:17,400 --> 00:02:23,840
the method of practicing is to say the words in the stomach or at the phenomenon.

23
00:02:23,840 --> 00:02:28,360
When the stomach is rising, the mind is with the rising, it's not up in the head in the

24
00:02:28,360 --> 00:02:32,800
brain thinking rising and falling, it's actually in the stomach and the mind is thinking

25
00:02:32,800 --> 00:02:38,680
rising, falling, but in the stomach, when the pain, the mind goes up to the pain and it

26
00:02:38,680 --> 00:02:48,280
knows pain, pain, pain in the pain or at the pain, it is the first important point is

27
00:02:48,280 --> 00:02:51,560
that we're not just to say these words in our head or to say them out loud or to say

28
00:02:51,560 --> 00:02:59,240
them at the mouth or to imagine these words are rising in our mind, our mind should

29
00:02:59,240 --> 00:03:03,320
be with the object at the object and acknowledging that.

30
00:03:03,320 --> 00:03:07,800
The first point, the second question is whether eventually we don't have to acknowledge

31
00:03:07,800 --> 00:03:13,320
any more, whether eventually we can just go back to just knowing, just watching the

32
00:03:13,320 --> 00:03:18,720
phenomena arise and see if it's possible to do so, but this isn't the practice of

33
00:03:18,720 --> 00:03:19,720
mindfulness.

34
00:03:19,720 --> 00:03:25,080
The practice of acknowledging, noting, reminding ourselves of the true nature of the

35
00:03:25,080 --> 00:03:31,520
phenomenon leads us all the way to freedom from suffering when the mind lets go of the

36
00:03:31,520 --> 00:03:32,520
object.

37
00:03:32,520 --> 00:03:37,080
When the mind lets go, of course, there's no need to use the acknowledgement, but until

38
00:03:37,080 --> 00:03:44,160
that time, it is necessary to keep reminding and just straightening up the mind in regards

39
00:03:44,160 --> 00:03:47,800
to the ultimate reality of the phenomenon.

40
00:03:47,800 --> 00:03:54,360
So this is the first question which I thought to answer in regards to the meditation is

41
00:03:54,360 --> 00:03:59,120
in regards to the noting itself.

42
00:03:59,120 --> 00:04:05,920
Another question which often comes up during the course of meditation practice is what

43
00:04:05,920 --> 00:04:10,560
are we expecting to get from the practice of meditation?

44
00:04:10,560 --> 00:04:19,480
So some people who have pain or aching or sore soreness expect that through meditation

45
00:04:19,480 --> 00:04:24,760
the pain and the aching and the soreness will disappear, people who think a lot expect

46
00:04:24,760 --> 00:04:29,360
that by practicing meditation, the thinking will disappear and these things will no longer

47
00:04:29,360 --> 00:04:38,120
come back and they'll be cured of these dates once and for all.

48
00:04:38,120 --> 00:04:45,760
The truth about meditation is that yes, it is to cure the unpleasantness of existence

49
00:04:45,760 --> 00:04:51,720
and to bring about the real true peace and happiness, but this is something which comes

50
00:04:51,720 --> 00:04:58,040
about not through the disappearance of pain and suffering, but through the acceptance and

51
00:04:58,040 --> 00:05:05,520
the disassociation from pain and suffering from unpleasant states of existence.

52
00:05:05,520 --> 00:05:12,040
So when we acknowledge to ourselves pain, pain or aching, aching, aching, we're not looking

53
00:05:12,040 --> 00:05:16,800
specifically for it to disappear, although sometimes it will disappear and through the

54
00:05:16,800 --> 00:05:19,720
acknowledgement, sometimes it won't disappear.

55
00:05:19,720 --> 00:05:24,920
What we're trying to understand is that these states inside of ourselves and in the world

56
00:05:24,920 --> 00:05:30,760
around us are impermanent, means they're changing all the time, they're not something,

57
00:05:30,760 --> 00:05:34,880
they're therefore not something which we should look to satisfy us when they're unpleasant

58
00:05:34,880 --> 00:05:39,880
things, we should not try to force them to go away at thinking that would be therefore

59
00:05:39,880 --> 00:05:45,520
satisfied and content, because they're impermanent, they're unsure when they will come,

60
00:05:45,520 --> 00:05:52,120
when they will go, because they're impermanent, there's no way to find satisfaction from

61
00:05:52,120 --> 00:05:55,160
their appearance or disappearance.

62
00:05:55,160 --> 00:05:59,720
When we see that, then we let go of them, we disassociate ourselves from them, we don't

63
00:05:59,720 --> 00:06:04,800
see them as me, as mine or under my control, we become comfortable with them as simply

64
00:06:04,800 --> 00:06:11,240
a part of nature, something which is not under our control or not worth trying to force

65
00:06:11,240 --> 00:06:15,520
trying to make disappear or make appear.

66
00:06:15,520 --> 00:06:22,040
The same thing goes when we experience thinking, when we find ourselves thinking too

67
00:06:22,040 --> 00:06:27,000
much by ourselves distracted or we find ourselves worried or we find ourselves stressed

68
00:06:27,000 --> 00:06:28,000
out.

69
00:06:28,000 --> 00:06:32,640
Do we think that by acknowledging thinking, thinking, or simply by sitting in meditation that

70
00:06:32,640 --> 00:06:42,760
we will no longer have to think until we reach what they call Nirvana or the cessation

71
00:06:42,760 --> 00:06:50,440
of suffering, we're always going to have thinking, but during the time that we have thinking

72
00:06:50,440 --> 00:06:55,640
when we acknowledge to ourselves thinking, thinking, thinking, we come to become comfortable

73
00:06:55,640 --> 00:07:01,720
and also to see the causes which lead us to think too much, we can see the causes of

74
00:07:01,720 --> 00:07:06,800
upset, the causes of stress, the causes of worry, and we can come to see that there's

75
00:07:06,800 --> 00:07:11,240
no reason for us to get upset and there's no reason for us to worry, and in fact there's

76
00:07:11,240 --> 00:07:18,480
no reason for us to think on and on again and again about the same things until finally

77
00:07:18,480 --> 00:07:19,480
it leads to the headache.

78
00:07:19,480 --> 00:07:24,480
We come to realize that all of this thinking is really getting us nowhere and we bring

79
00:07:24,480 --> 00:07:30,360
the mind back to the present moment and the mind naturally moves, gravitates back towards

80
00:07:30,360 --> 00:07:36,520
the present moment and moves away from dreaming about the future or remembering about the

81
00:07:36,520 --> 00:07:43,120
past, better good things or bad things which have happened or might happen in the future.

82
00:07:43,120 --> 00:07:51,040
So to put it briefly we come to see that all of the things which we hold on to are

83
00:07:51,040 --> 00:07:56,280
are impermanent because they're impermanent, they're not satisfying because they're

84
00:07:56,280 --> 00:08:00,640
not satisfying then we shouldn't say it of these things that these are medies or minis

85
00:08:00,640 --> 00:08:06,760
under my control and as a result of seeing this we let go, the mind starts to let go

86
00:08:06,760 --> 00:08:12,120
to give up all of the things which it used to hold on to as me, as mine, and try to control

87
00:08:12,120 --> 00:08:17,520
and try to force and the mind no longer tries to force and control, the things inside of

88
00:08:17,520 --> 00:08:20,040
us and the things around us.

89
00:08:20,040 --> 00:08:25,760
This is another question which arises, the point is not for the ability to control the

90
00:08:25,760 --> 00:08:30,400
things inside of us and around us but simply to let go of them and allow the mind to

91
00:08:30,400 --> 00:08:35,880
gravitate naturally back to the present moment and back to a state of true peace and

92
00:08:35,880 --> 00:08:36,880
true happiness.

93
00:08:36,880 --> 00:09:06,720
This is the answer to the questions on the nature of the goal of meditation practice.

