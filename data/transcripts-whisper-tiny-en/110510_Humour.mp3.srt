1
00:00:00,000 --> 00:00:03,000
Hello and welcome back to Ask a Monk.

2
00:00:03,000 --> 00:00:08,000
The next question is one that's probably

3
00:00:08,000 --> 00:00:13,000
quite difficult to give an appropriate answer to.

4
00:00:13,000 --> 00:00:17,000
Only because it's a question that

5
00:00:17,000 --> 00:00:22,000
if I give my honest opinion about

6
00:00:22,000 --> 00:00:25,000
which I'm probably going to do,

7
00:00:25,000 --> 00:00:28,000
there's going to be a lot of people who disagree with me.

8
00:00:28,000 --> 00:00:33,000
So hopefully I can minimize that and explain myself

9
00:00:33,000 --> 00:00:36,000
and be as open-minded as possible.

10
00:00:36,000 --> 00:00:43,000
And I hope that my audience is open-minded as they can be as well.

11
00:00:43,000 --> 00:00:48,000
The question is about humor.

12
00:00:48,000 --> 00:00:52,000
What place does humor have in the Buddha's teaching?

13
00:00:52,000 --> 00:00:55,000
And what's wrong with, is there anything wrong with innocent

14
00:00:55,000 --> 00:00:57,000
or harmless, harmless jokes?

15
00:00:57,000 --> 00:01:01,000
I think it's the question in Buddhism.

16
00:01:01,000 --> 00:01:05,000
So as with everything,

17
00:01:05,000 --> 00:01:11,000
I have no dogma in regards to humor or in regards to anything.

18
00:01:11,000 --> 00:01:14,000
Any dogma should be thrown out.

19
00:01:14,000 --> 00:01:16,000
We should analyze things scientifically.

20
00:01:16,000 --> 00:01:19,000
We use the Buddha's teaching as a guide,

21
00:01:19,000 --> 00:01:22,000
but we do so because we've analyzed it

22
00:01:22,000 --> 00:01:24,000
and we think it's the best way.

23
00:01:24,000 --> 00:01:26,000
We've also practiced it and through our practice,

24
00:01:26,000 --> 00:01:31,000
we come to see that it truly is a way that leads

25
00:01:31,000 --> 00:01:35,000
to peace, happiness, and freedom from suffering.

26
00:01:35,000 --> 00:01:39,000
So looking scientifically at humor,

27
00:01:39,000 --> 00:01:42,000
we have to ask ourselves what good is it?

28
00:01:42,000 --> 00:01:43,000
And what bad is it?

29
00:01:43,000 --> 00:01:44,000
What good comes from it?

30
00:01:44,000 --> 00:01:46,000
What bad comes from it?

31
00:01:46,000 --> 00:01:50,000
And the first thing to say that people often miss in Buddhism

32
00:01:50,000 --> 00:01:58,000
is that there's nothing intrinsically wrong with happy feelings.

33
00:01:58,000 --> 00:02:00,000
When they hear the Buddha's teaching,

34
00:02:00,000 --> 00:02:04,000
they think we're trying to get rid of all happy feelings

35
00:02:04,000 --> 00:02:08,000
or if we practice the Buddha's teaching successfully,

36
00:02:08,000 --> 00:02:10,000
we'll never feel happy again.

37
00:02:10,000 --> 00:02:13,000
We'll never feel sad, but we'll also never feel happy.

38
00:02:13,000 --> 00:02:15,000
And that's not true.

39
00:02:15,000 --> 00:02:23,000
Happy feelings are not a...

40
00:02:23,000 --> 00:02:28,000
I'm not in the same level as sad feelings.

41
00:02:28,000 --> 00:02:32,000
A happy feeling is a feeling of pleasure.

42
00:02:32,000 --> 00:02:35,000
And a feeling of pleasure is on the same level

43
00:02:35,000 --> 00:02:37,000
as the feeling of pain.

44
00:02:37,000 --> 00:02:40,000
And through the Buddha's teaching,

45
00:02:40,000 --> 00:02:43,000
it's not the case that you will in the here and now

46
00:02:43,000 --> 00:02:51,000
be free from physical pain or be relieved of pleasurable feelings.

47
00:02:51,000 --> 00:02:57,000
Even enlightened beings still experience both physical pain

48
00:02:57,000 --> 00:02:59,000
and physical pleasure.

49
00:02:59,000 --> 00:03:01,000
They still experience these.

50
00:03:01,000 --> 00:03:06,000
They may also experience mental pleasure as well,

51
00:03:06,000 --> 00:03:10,000
but what they will not experience is liking or disliking.

52
00:03:10,000 --> 00:03:20,000
There will be no attachment to the pleasant or the unpleasant sensation,

53
00:03:20,000 --> 00:03:26,000
which makes the person attached and addicted.

54
00:03:26,000 --> 00:03:31,000
So with humor here, like so many different things,

55
00:03:31,000 --> 00:03:35,000
I think you can't disagree with the fact

56
00:03:35,000 --> 00:03:41,000
that it is creating, in most cases, a state of liking.

57
00:03:41,000 --> 00:03:47,000
Now, I don't know whether Arahant's enlightened beings laugh or not.

58
00:03:47,000 --> 00:03:49,000
I've been told that they don't.

59
00:03:49,000 --> 00:03:54,000
That when there's something that they find humorous,

60
00:03:54,000 --> 00:03:56,000
they smile.

61
00:03:56,000 --> 00:04:00,000
And from what I've seen or read or studied about,

62
00:04:00,000 --> 00:04:03,000
they smile at the oddest sort of things.

63
00:04:03,000 --> 00:04:07,000
Like, there's a case of a monk who saw a,

64
00:04:07,000 --> 00:04:11,000
he was a monk who had very advanced meditation

65
00:04:11,000 --> 00:04:15,000
and he'd done a lot of yogic meditations,

66
00:04:15,000 --> 00:04:17,000
very advanced meditations.

67
00:04:17,000 --> 00:04:20,000
And he was able to see things that most people couldn't see.

68
00:04:20,000 --> 00:04:22,000
And he was able to see ghosts.

69
00:04:22,000 --> 00:04:25,000
He was able to see angels and so on.

70
00:04:25,000 --> 00:04:28,000
And one day he saw a ghost flying through the air

71
00:04:28,000 --> 00:04:30,000
and he saw many actually.

72
00:04:30,000 --> 00:04:35,000
But one he saw I think was being chased by crows.

73
00:04:35,000 --> 00:04:39,000
It was a skeleton flying through the air on fire,

74
00:04:39,000 --> 00:04:41,000
being chased by crows or something.

75
00:04:41,000 --> 00:04:42,000
It was in terrible suffering.

76
00:04:42,000 --> 00:04:44,000
And he smiled.

77
00:04:44,000 --> 00:04:46,000
And this monk next to him said,

78
00:04:46,000 --> 00:04:47,000
why are you smiling?

79
00:04:47,000 --> 00:04:49,000
And he said, oh, I won't tell you.

80
00:04:49,000 --> 00:04:52,000
And he said, let's go talk to the Buddha.

81
00:04:52,000 --> 00:04:55,000
And so they went and talked to the Buddha and the Buddha.

82
00:04:55,000 --> 00:04:57,000
And he explained it in front of the Buddha.

83
00:04:57,000 --> 00:04:59,000
And why he didn't tell the monk is because

84
00:04:59,000 --> 00:05:01,000
he knew that this monk would get angry

85
00:05:01,000 --> 00:05:02,000
and wouldn't believe him.

86
00:05:02,000 --> 00:05:04,000
But in front of the Buddha, the Buddha said,

87
00:05:04,000 --> 00:05:06,000
yeah, I saw that same ghost.

88
00:05:06,000 --> 00:05:11,000
And I didn't say anything because I was waiting for a witness

89
00:05:11,000 --> 00:05:14,000
because people wouldn't believe me either.

90
00:05:14,000 --> 00:05:16,000
So they saw these fantastical things.

91
00:05:16,000 --> 00:05:21,000
And the reason they found it, they didn't find it humorous.

92
00:05:21,000 --> 00:05:23,000
But the reason that they smiled, it is said,

93
00:05:23,000 --> 00:05:27,000
is because they knew for themselves that they were free from it.

94
00:05:27,000 --> 00:05:30,000
And I think the thought that was going through this monk's mind

95
00:05:30,000 --> 00:05:34,000
at the time was, wow, it's amazing how far I've come

96
00:05:34,000 --> 00:05:37,000
and how free I am from these sorts of states

97
00:05:37,000 --> 00:05:42,000
because of not doing any bad karma.

98
00:05:42,000 --> 00:05:45,000
The Buddha himself is said to have smiled

99
00:05:45,000 --> 00:05:49,000
when he saw a young pig.

100
00:05:49,000 --> 00:05:54,000
And he is attendant on and asked him, why did you smile?

101
00:05:54,000 --> 00:05:56,000
And the Buddha said, you saw that pig over there?

102
00:05:56,000 --> 00:06:03,000
He said, that pig used to be a brahma, a god,

103
00:06:03,000 --> 00:06:10,000
up in one of the god realms.

104
00:06:10,000 --> 00:06:14,000
And on passing away from there,

105
00:06:14,000 --> 00:06:16,000
he is sheer.

106
00:06:16,000 --> 00:06:19,000
It is now a pig.

107
00:06:19,000 --> 00:06:20,000
It's now become a pig.

108
00:06:20,000 --> 00:06:22,000
And so the Buddha smiled.

109
00:06:22,000 --> 00:06:25,000
So they have the oddest sort of sense of humor

110
00:06:25,000 --> 00:06:28,000
if you want to call it that.

111
00:06:28,000 --> 00:06:30,000
But this isn't really the question.

112
00:06:30,000 --> 00:06:33,000
The question of whether we find things humorous or not,

113
00:06:33,000 --> 00:06:39,000
I think, is an much easier one to answer.

114
00:06:39,000 --> 00:06:43,000
I think finding things humorous as a Buddhist

115
00:06:43,000 --> 00:06:48,000
is certainly possible.

116
00:06:48,000 --> 00:06:52,000
And I mean, I just think it's also possible

117
00:06:52,000 --> 00:06:56,000
to find something humorous and then become attached

118
00:06:56,000 --> 00:06:59,000
because pleasure in the body,

119
00:06:59,000 --> 00:07:03,000
there's a lot of chemical reactions and endorphins

120
00:07:03,000 --> 00:07:07,000
and all of these chemicals that are released,

121
00:07:07,000 --> 00:07:13,000
dopamine, whatever they have they found in the brain.

122
00:07:13,000 --> 00:07:17,000
And it's easy to become addicted to that as a bit of a drug.

123
00:07:17,000 --> 00:07:20,000
But the question here is whether cracking jokes

124
00:07:20,000 --> 00:07:24,000
is wholesome or unwholesome thing.

125
00:07:24,000 --> 00:07:31,000
And I think it's probably unwholesome actually

126
00:07:31,000 --> 00:07:37,000
to be cracking jokes for the most part.

127
00:07:37,000 --> 00:07:42,000
But in the same way as saying something useless is

128
00:07:42,000 --> 00:07:46,000
if we're sitting around talking about sports,

129
00:07:46,000 --> 00:07:49,000
the Buddha would say this is unwholesome.

130
00:07:49,000 --> 00:07:53,000
This because our minds are polluting our minds

131
00:07:53,000 --> 00:07:55,000
with these useless topics.

132
00:07:55,000 --> 00:07:57,000
But on the other hand, so many people like to sit around

133
00:07:57,000 --> 00:07:58,000
and talk about sports.

134
00:07:58,000 --> 00:08:01,000
I think a lot of Buddhists and Buddhist meditators.

135
00:08:01,000 --> 00:08:05,000
This country now has a cricket match going on

136
00:08:05,000 --> 00:08:08,000
the world cup of cricket in Sri Lanka.

137
00:08:08,000 --> 00:08:13,000
And most people hear a Buddhist, but that certainly

138
00:08:13,000 --> 00:08:16,000
doesn't stop them from cheering on this

139
00:08:16,000 --> 00:08:20,000
Sri Lankan cricket team and talking about sports.

140
00:08:20,000 --> 00:08:22,000
I mean, the point is it's such a small thing

141
00:08:22,000 --> 00:08:24,000
to worry about.

142
00:08:24,000 --> 00:08:28,000
It's akin to people worrying about

143
00:08:28,000 --> 00:08:30,000
cussing or cursing.

144
00:08:30,000 --> 00:08:33,000
And someone asked that recently and I said,

145
00:08:33,000 --> 00:08:37,000
no, I think it is a technically it isn't unwholesome thing

146
00:08:37,000 --> 00:08:41,000
or probably unwholesome because it's saying something

147
00:08:41,000 --> 00:08:44,000
that is unpleasant to other people.

148
00:08:44,000 --> 00:08:49,000
It's brought about by mind states that are probably unwholesome.

149
00:08:49,000 --> 00:08:53,000
But this is really the point is what is the intention in the mind?

150
00:08:53,000 --> 00:08:56,000
And I think a lot of people justify humor

151
00:08:56,000 --> 00:09:00,000
by the fact that it breaks tension.

152
00:09:00,000 --> 00:09:04,000
And so when people are upset,

153
00:09:04,000 --> 00:09:06,000
you try to make them laugh.

154
00:09:06,000 --> 00:09:09,000
You find something that relaxes them.

155
00:09:09,000 --> 00:09:14,000
And I don't know actually.

156
00:09:14,000 --> 00:09:19,000
I know this is even a technique that meditation teachers use.

157
00:09:19,000 --> 00:09:23,000
Now I'm, you know, meditation teachers are,

158
00:09:23,000 --> 00:09:26,000
just because someone's a meditation teacher doesn't mean

159
00:09:26,000 --> 00:09:28,000
they're teaching the right thing.

160
00:09:28,000 --> 00:09:31,000
And even though they might be famous,

161
00:09:31,000 --> 00:09:35,000
it doesn't mean that they are on the right track.

162
00:09:35,000 --> 00:09:37,000
In fact, in the Buddhist time,

163
00:09:37,000 --> 00:09:39,000
there were several very famous teachers

164
00:09:39,000 --> 00:09:42,000
who it seems for very much on the wrong track.

165
00:09:42,000 --> 00:09:44,000
They had very strange ideas.

166
00:09:44,000 --> 00:09:52,000
And the point is to get things right

167
00:09:52,000 --> 00:09:55,000
and to get things scientifically correct.

168
00:09:55,000 --> 00:09:59,000
So from a scientific point of view,

169
00:09:59,000 --> 00:10:00,000
from a Buddhist point of view,

170
00:10:00,000 --> 00:10:02,000
from a practical point of view,

171
00:10:02,000 --> 00:10:05,000
is humor something that leads us out of suffering.

172
00:10:05,000 --> 00:10:07,000
Now the problem I see with it is this,

173
00:10:07,000 --> 00:10:10,000
is that if you're using,

174
00:10:10,000 --> 00:10:11,000
suppose you're a meditation teacher,

175
00:10:11,000 --> 00:10:14,000
suppose my student comes to me and they're all upset,

176
00:10:14,000 --> 00:10:16,000
and I start telling them jokes

177
00:10:16,000 --> 00:10:18,000
or trying to divert their attention.

178
00:10:18,000 --> 00:10:20,000
Because that's really what,

179
00:10:20,000 --> 00:10:23,000
well, that's one way of using humor.

180
00:10:23,000 --> 00:10:26,000
Suppose I bring up something humorous

181
00:10:26,000 --> 00:10:27,000
and make them laugh.

182
00:10:27,000 --> 00:10:29,000
I've diverted their attention.

183
00:10:29,000 --> 00:10:31,000
I've taken them away from the problem.

184
00:10:31,000 --> 00:10:34,000
And that's really the opposite

185
00:10:34,000 --> 00:10:36,000
of what the Buddha would have us do.

186
00:10:36,000 --> 00:10:39,000
It's the opposite of what I teach people to do,

187
00:10:39,000 --> 00:10:41,000
which is to look at the problem

188
00:10:41,000 --> 00:10:43,000
and come to overcome the fact

189
00:10:43,000 --> 00:10:45,000
that it's a problem for us.

190
00:10:45,000 --> 00:10:49,000
Overcome the assumption in our mind

191
00:10:49,000 --> 00:10:51,000
that there's a problem,

192
00:10:51,000 --> 00:10:53,000
the disliking for the problem,

193
00:10:53,000 --> 00:10:55,000
the attachment to the problem.

194
00:10:55,000 --> 00:10:58,000
Now, if we just divert our attention

195
00:10:58,000 --> 00:11:00,000
every time a problem arises,

196
00:11:00,000 --> 00:11:07,000
then we're never going to actually deal with it.

197
00:11:07,000 --> 00:11:10,000
I think another way of using humor

198
00:11:10,000 --> 00:11:12,000
that might be more in line with the Buddha's teaching

199
00:11:12,000 --> 00:11:15,000
is to come to see the problem

200
00:11:15,000 --> 00:11:17,000
that you have as humorous.

201
00:11:17,000 --> 00:11:19,000
If you come to see

202
00:11:19,000 --> 00:11:23,000
what a stupid way of looking at it,

203
00:11:23,000 --> 00:11:28,000
what a silly thing that we've come to see this as a problem.

204
00:11:28,000 --> 00:11:37,000
If we are all upset about the nature of things,

205
00:11:37,000 --> 00:11:40,000
we have some consensus

206
00:11:40,000 --> 00:11:45,000
and a very nice present

207
00:11:45,000 --> 00:11:47,000
or something and then the ants get added

208
00:11:47,000 --> 00:11:49,000
and we feel all upset.

209
00:11:49,000 --> 00:11:52,000
Being able to see that

210
00:11:52,000 --> 00:11:55,000
this is just a silly thing

211
00:11:55,000 --> 00:11:57,000
and there's no substance to it.

212
00:11:57,000 --> 00:11:59,000
It's something important

213
00:11:59,000 --> 00:12:02,000
to be able to look at the human condition

214
00:12:02,000 --> 00:12:03,000
and to look at our problems

215
00:12:03,000 --> 00:12:05,000
and to find them humorous.

216
00:12:05,000 --> 00:12:09,000
I think that's sort of what the Buddha came to see

217
00:12:09,000 --> 00:12:13,000
and the sort of things that the Buddha would smile at

218
00:12:13,000 --> 00:12:15,000
and the our ants would smile at.

219
00:12:15,000 --> 00:12:18,000
So I think if the humor is allowing us

220
00:12:18,000 --> 00:12:22,000
to see things in a more open-minded way

221
00:12:22,000 --> 00:12:24,000
and to be more open-minded,

222
00:12:24,000 --> 00:12:29,000
then I think that could be beneficial humor

223
00:12:29,000 --> 00:12:32,000
because it's associated with wisdom

224
00:12:32,000 --> 00:12:34,000
but if it's just humor

225
00:12:34,000 --> 00:12:38,000
for the purpose of making people laugh,

226
00:12:38,000 --> 00:12:40,000
the Buddha had a specific teaching

227
00:12:40,000 --> 00:12:44,000
this man who was a jester I think

228
00:12:44,000 --> 00:12:47,000
and his job was to make the king in the queen

229
00:12:47,000 --> 00:12:49,000
and the court laugh

230
00:12:49,000 --> 00:12:51,000
and so he would do all sorts of stupid things

231
00:12:51,000 --> 00:12:53,000
like the three stooges or whatever

232
00:12:53,000 --> 00:12:55,000
and he said, I've heard that by doing this,

233
00:12:55,000 --> 00:12:57,000
I'm going to go to the laughing,

234
00:12:57,000 --> 00:13:00,000
the heaven of laughter,

235
00:13:00,000 --> 00:13:02,000
this heaven that is called,

236
00:13:02,000 --> 00:13:04,000
the heaven of laughter when I die.

237
00:13:04,000 --> 00:13:05,000
Is that true?

238
00:13:05,000 --> 00:13:06,000
And the Buddha said,

239
00:13:06,000 --> 00:13:08,000
oh, I'm sorry, don't ask me that question

240
00:13:08,000 --> 00:13:11,000
and this jester he asked again

241
00:13:11,000 --> 00:13:13,000
and again and again and again and again and again

242
00:13:13,000 --> 00:13:14,000
the Buddha said,

243
00:13:14,000 --> 00:13:18,000
no, the truth is if you're so caught up

244
00:13:18,000 --> 00:13:20,000
in diverting these people's attention

245
00:13:20,000 --> 00:13:23,000
as serious and just dedicated

246
00:13:23,000 --> 00:13:28,000
to making them laugh,

247
00:13:28,000 --> 00:13:31,000
you go to the hell of laughter

248
00:13:31,000 --> 00:13:34,000
which is what that is I'm not sure

249
00:13:34,000 --> 00:13:37,000
but it's I believe it's a place of torture

250
00:13:37,000 --> 00:13:41,000
and it's just called the hell of laughter

251
00:13:41,000 --> 00:13:45,000
and the point being that it's

252
00:13:45,000 --> 00:13:49,000
we're wasting a greater part of our life,

253
00:13:49,000 --> 00:13:52,000
our lives in useless humor.

254
00:13:52,000 --> 00:13:54,000
If it's humor that is slapstick

255
00:13:54,000 --> 00:13:58,000
or whatever that has no real benefit

256
00:13:58,000 --> 00:14:00,000
in terms of opening the mind

257
00:14:00,000 --> 00:14:04,000
and allowing you to see the true nature of reality

258
00:14:04,000 --> 00:14:06,000
then I would say it's not beneficial.

259
00:14:06,000 --> 00:14:08,000
So I guess that's it.

260
00:14:08,000 --> 00:14:12,000
There are two kinds of humor that I can see.

261
00:14:12,000 --> 00:14:16,000
One that is

262
00:14:16,000 --> 00:14:19,000
citing with wisdom and one that is citing with ignorance

263
00:14:19,000 --> 00:14:22,000
and delusion and addiction

264
00:14:22,000 --> 00:14:24,000
and attachment really because

265
00:14:24,000 --> 00:14:27,000
the point of it is simply to bring about

266
00:14:27,000 --> 00:14:32,000
a state of pleasure in the mind

267
00:14:32,000 --> 00:14:36,000
which makes it no better than any drug out there.

268
00:14:36,000 --> 00:14:38,000
It's something that leads to addiction

269
00:14:38,000 --> 00:14:41,000
and attachment and ultimately

270
00:14:41,000 --> 00:14:44,000
most people in the world

271
00:14:44,000 --> 00:14:46,000
is why they get bored when they have to sit down

272
00:14:46,000 --> 00:14:48,000
and do meditation because it's not exciting

273
00:14:48,000 --> 00:14:52,000
and they want to go and watch sitcoms or cartoons

274
00:14:52,000 --> 00:14:56,000
or whatever it is that people watch these days.

275
00:14:56,000 --> 00:14:57,000
So hope that helps.

276
00:14:57,000 --> 00:14:59,000
Hope that's a proper answer.

277
00:14:59,000 --> 00:15:00,000
Thanks for the question.

278
00:15:00,000 --> 00:15:01,000
Really a good one.

279
00:15:01,000 --> 00:15:22,000
All the best.

