1
00:00:00,000 --> 00:00:29,840
You're good evening, everyone, and welcome back.

2
00:00:29,840 --> 00:00:49,720
Till evening, dhamma, tonight, I'd like to look at, you know, to one second.

3
00:00:49,720 --> 00:00:59,760
So many switches, hello, can you hear that, it's a lot enough, hello, hello, hello.

4
00:00:59,760 --> 00:01:20,760
Well, that's better, too loud, it's good, so today we'll be looking at the Bayabirosut.

5
00:01:20,760 --> 00:01:30,360
If you want to follow along, it's been Gmanikaya, sutta number four.

6
00:01:30,360 --> 00:01:49,360
In this talk, the Buddha is approached by Brahman Janusoni, who asks, basically asks whether the followers of the Buddha take after his example.

7
00:01:49,360 --> 00:02:01,840
Meaning, do they practice as you practice?

8
00:02:01,840 --> 00:02:15,920
Which is, basically, the idea is that the students are capable of the same, capable of the same sort of practice as the Buddha, of course,

9
00:02:15,920 --> 00:02:36,360
in that time, or in religion in general, it can happen that the headmaster, the guru is capable of practices that the students are not capable of.

10
00:02:36,360 --> 00:02:43,760
Do your students actually practice the things that you practice?

11
00:02:43,760 --> 00:02:51,200
That's basically what he's active, asking, do they follow your advice, do they do what you do?

12
00:02:51,200 --> 00:02:54,680
Are they able to keep your teachings?

13
00:02:54,680 --> 00:03:07,680
And the Buddha affirms all this, he says, yes, indeed my students do look to me for guidance, they take my advice, and they follow my example.

14
00:03:07,680 --> 00:03:14,960
Which in and of itself is quite impressive, because the Buddha's practice, of course, was quite rigorous.

15
00:03:14,960 --> 00:03:26,800
The Buddha himself only slept, only lay down for three or four hours a night, and the rest of the time was mostly actually spent in teaching.

16
00:03:26,800 --> 00:03:45,000
And they say even those three or four hours when he lay down were not actually sleeping, he would actually teach them as well.

17
00:03:45,000 --> 00:03:58,160
But the talk here is about, the talk here is about specifically the practice of living in the jungle.

18
00:03:58,160 --> 00:04:09,640
So, Janosone says, wow, I would think that it would be hard for the monks to live in the jungle.

19
00:04:09,640 --> 00:04:25,040
I would think that if someone were to go off and live in the jungle alone, if they weren't focused, if they didn't have a strong mind, they'd go crazy.

20
00:04:25,040 --> 00:04:46,680
Let's see what the Pauli is for, go crazy.

21
00:04:46,680 --> 00:05:06,280
Haranti Mano, Haranti means, take away Mano the mind, no, I don't get it, Haranti.

22
00:05:06,280 --> 00:05:16,520
Take the back to that one, anyway, your mind will be, he'll lose your mind basically.

23
00:05:16,520 --> 00:05:38,520
And the Buddha affirms this, he says, yes, it's hard to endure living in the forest, it's hard to live in seclusion, it's hard to enjoy solitude, one would think living in the jungle would drive you crazy, if you have no concentration.

24
00:05:38,520 --> 00:05:45,520
If your mind is not focused.

25
00:05:45,520 --> 00:05:57,160
And then he goes through, why I brought this Buddha up is because he goes through a bunch of qualities that are, we can say they're required for practice.

26
00:05:57,160 --> 00:06:13,640
And so, it well answers the questions of the question of what sort of qualities we need to develop, and the sort of qualities that differentiate a person who's capable of living in seclusion, and one who is not.

27
00:06:13,640 --> 00:06:32,640
What's it going to take, useful for us, what's it going to take for us to be able to successfully participate in a meditation course, what's it going to take for us to be able to stay alone with ourselves,

28
00:06:32,640 --> 00:06:45,640
to do the work that is required to understand ourselves. So this is what this suit is about.

29
00:06:45,640 --> 00:07:13,640
The Buddha uses himself as an example, he says, when I was unenlightened, I thought about how hard it would be, and then I thought about the qualities that I would need. And so the first three are, and the first three qualities you need are purity of conduct, physical conduct, verbal conduct, mental conduct.

30
00:07:13,640 --> 00:07:23,640
If you're a person who is full of corruption in body, speech, or mind, then it's not easy to stay alone with yourself.

31
00:07:23,640 --> 00:07:42,640
You're going to be overwhelmed with remorse, if you're a murderer, if you're a thief, if you're a liar, if you're a cheat, if you take drugs in alcohol, if you're addicted to this or that.

32
00:07:42,640 --> 00:07:48,640
It's not easy to stay alone in the forest, or to stay alone in general.

33
00:07:48,640 --> 00:07:59,640
Honestly, living in the jungle is quite peaceful. It's a romantic notion that often people think about, hey, I'll just go off and live in the jungle.

34
00:07:59,640 --> 00:08:11,640
It's not actually the jungle that's a problem, jungles. Well, there's other reasons why the jungle is dangerous, sickness, and dangerous animals, even the ants will kill you.

35
00:08:11,640 --> 00:08:30,640
But as far as just living alone in the forest, I lived alone in a tent in the jungle, trying to set up a monastery, a forest monastery, and I've told this story before about how I got chased up by a tiger.

36
00:08:30,640 --> 00:08:45,640
So there are those kind of dangers. But simply living alone in the forest is quite peaceful, but it's really interesting how difficult it is for us to be at peace.

37
00:08:45,640 --> 00:08:55,640
I mean, it's really like the crux of the situation. It's not that being alone is stressful or suffering, that we're not able to be at peace.

38
00:08:55,640 --> 00:09:05,640
We're not at peace. And so we find ways to distract ourselves, to avoid dealing with ourselves.

39
00:09:05,640 --> 00:09:15,640
In other words, we have no tools, we have no means of dealing with ourselves.

40
00:09:15,640 --> 00:09:43,640
And so, again, this is a list of ways of coming to terms with ourselves, being able to be alone with ourselves, to go inside and to experience here and now, to be without always getting what we want, or being stimulated by delightful things that were addictive, you know, our addiction.

41
00:09:43,640 --> 00:09:52,640
And so the first three steps, the first steps are purity of conduct, morality. It always starts with morality.

42
00:09:52,640 --> 00:09:59,640
It's not even that morality is all that important because you're not going to be killing or stealing or lying or cheating when you're here meditating.

43
00:09:59,640 --> 00:10:12,640
It's more that morality is like a roadblock. If you're not a moral ethical sort of person, you won't even get in the door, you want to come and meditate, you try to sit still and you'll be completely

44
00:10:12,640 --> 00:10:28,640
perturbed, distracted, great stress will come from the sickness in the mind that it's a result of unholyment.

45
00:10:28,640 --> 00:10:37,640
It's not to say that you can't. Having done bad deeds, you can't meditate. It makes it difficult. It might drive you crazy if you've done a lot of bad deeds.

46
00:10:37,640 --> 00:10:44,640
You have to go slowly. This is a reason why we make a determination to keep the five precepts.

47
00:10:44,640 --> 00:10:53,640
One good reason is to give you confidence that, okay, I did bad things in the past, but I don't have to feel guilty because I've changed.

48
00:10:53,640 --> 00:11:02,640
I understand that that was wrong and I hurt people and so on, but I've changed now.

49
00:11:02,640 --> 00:11:07,640
You get a great confidence by knowing that you're now a good person.

50
00:11:07,640 --> 00:11:13,640
It's all really psychological. That's what it's all about.

51
00:11:13,640 --> 00:11:22,640
Number four is livelihood.

52
00:11:22,640 --> 00:11:35,640
Livelyhood relates to the first three. He says if people are impure of action or a livelihood means they're business,

53
00:11:35,640 --> 00:11:46,640
they're means of livelihood is corrupt, cheating others and so on or hurting others or manipulating others to make a living,

54
00:11:46,640 --> 00:11:53,640
they're going to go crazy if they try to be alone with themselves. Not going to be easy, but he said I'm not like that.

55
00:11:53,640 --> 00:12:15,640
This gave him great confidence. He says I found great solace in dwelling in the forest.

56
00:12:15,640 --> 00:12:39,640
And then he goes the next step to talk about once you've talked about your conduct.

57
00:12:39,640 --> 00:12:47,640
And the next thing that you have to work on is the mental state, right?

58
00:12:47,640 --> 00:12:59,640
So he says some people have covered us in full of lust with great desires, addictions wanting this, wanting that, liking this, liking that.

59
00:12:59,640 --> 00:13:06,640
We don't see the danger. I often get this question, why should we give up the things that we like?

60
00:13:06,640 --> 00:13:13,640
To make us very happy. You get your answer when you try to sit still with yourself.

61
00:13:13,640 --> 00:13:22,640
When you try to do this very peaceful, seemingly simple thing of just being, just sitting.

62
00:13:22,640 --> 00:13:28,640
It's very difficult because your mind is tormented, really tormented.

63
00:13:28,640 --> 00:13:33,640
You wonder why meditation is so stressful, it's not that meditation is so stressful.

64
00:13:33,640 --> 00:13:39,640
It's that one thing, things are not getting those things as stressful.

65
00:13:39,640 --> 00:13:47,640
Can't blame the meditation for your addictions.

66
00:13:47,640 --> 00:13:54,640
Other people are full of ill will or hate.

67
00:13:54,640 --> 00:13:58,640
For some people, their mind is on fire.

68
00:13:58,640 --> 00:14:10,640
I mean, a big example of this is just hatred of pain.

69
00:14:10,640 --> 00:14:19,640
The inability to stand discomfort.

70
00:14:19,640 --> 00:14:24,640
We blame pain. We think pain is a bad thing. Pain is a problem.

71
00:14:24,640 --> 00:14:29,640
Why should I sit with pain? If I have pain, I should change my position.

72
00:14:29,640 --> 00:14:34,640
I should go somewhere else to do something else.

73
00:14:34,640 --> 00:14:40,640
The Buddha doesn't blame pain. He says, it's not the pain, it's the aversion.

74
00:14:40,640 --> 00:14:46,640
It's the disliking of the pain. It's very hard to see.

75
00:14:46,640 --> 00:14:54,640
Your mind in any way inflamed with hate, if you hate, if you have anger towards other people,

76
00:14:54,640 --> 00:15:04,640
if you feel you've been wrong, if you're consumed by thoughts of vengeance or thoughts of anger.

77
00:15:04,640 --> 00:15:10,640
Very difficult to be with yourself. Very difficult to be alone.

78
00:15:10,640 --> 00:15:17,640
You think about all the bad things people have done to you.

79
00:15:17,640 --> 00:15:25,640
Very hard to be at peace.

80
00:15:25,640 --> 00:15:33,640
Someone else is overcome by sloth and torpor laziness, sluggishness of mind.

81
00:15:33,640 --> 00:15:40,640
The ordinary mind is weak. The sloth and torpor is like the equivalent of a person

82
00:15:40,640 --> 00:15:43,640
who has never done any exercise.

83
00:15:43,640 --> 00:15:49,640
Basically, they're very weak. They're not able to lift any weight. It's not able to do any work.

84
00:15:49,640 --> 00:15:57,640
If you give the manual labors, they go and lift those blocks or build that wall or something.

85
00:15:57,640 --> 00:16:05,640
They're not able to use their bodies. This feeling of being too weak for something.

86
00:16:05,640 --> 00:16:10,640
When you get that feeling in the mind, that's a sign of sloth and torpor.

87
00:16:10,640 --> 00:16:14,640
The mind not being strong enough. You want to be mindful that your mind is not strong.

88
00:16:14,640 --> 00:16:20,640
You have a feeling, you can feel that the mind is not able to stay with the object.

89
00:16:20,640 --> 00:16:26,640
It's lazy. It gets easily distracted.

90
00:16:26,640 --> 00:16:33,640
It doesn't want to put out the effort. It's reluctant. It's stiff.

91
00:16:33,640 --> 00:16:39,640
It's unwieldy. It's weak, it's sloth and torpor.

92
00:16:39,640 --> 00:16:43,640
It's for such a person very difficult to stay alone in the forest.

93
00:16:43,640 --> 00:16:55,640
Very difficult to stay alone because, of course, you get easily distracted and caught up.

94
00:16:55,640 --> 00:17:02,640
You find it very difficult to be present.

95
00:17:02,640 --> 00:17:10,640
It's difficult to deal with all the challenges that arise.

96
00:17:10,640 --> 00:17:14,640
There's another person is full of doubt and certainty.

97
00:17:14,640 --> 00:17:24,640
Without an uncertainty, very difficult to stay alone with yourself, especially in the forest or the jungle.

98
00:17:24,640 --> 00:17:39,640
Without the certainty, the power of mind, you easily get lost and discouraged, overwhelmed by your defilements, your wants and your perversions.

99
00:17:39,640 --> 00:17:50,640
Very easily get overwhelmed and lose your ability to continue on with the practice.

100
00:17:50,640 --> 00:17:57,640
Some meditators are so full of doubt that it's very difficult for them to progress in the meditation practice.

101
00:17:57,640 --> 00:18:02,640
It's encouraging some of them are able to really break through and give it up.

102
00:18:02,640 --> 00:18:10,640
But it often happens that the doubt leads the meditator to stop practicing, at least temporarily.

103
00:18:10,640 --> 00:18:21,640
It's so discouraged and confused. They don't know right or wrong, good or bad.

104
00:18:21,640 --> 00:18:27,640
They have so much doubt.

105
00:18:27,640 --> 00:18:29,640
I'm going to say, I've gone beyond doubt.

106
00:18:29,640 --> 00:18:35,640
These are the five hindrances and these are really our enemies in the practice.

107
00:18:35,640 --> 00:18:45,640
Regardless of whether you think of them as right or wrong or dangerous or not, they get in the way of progress in meditation.

108
00:18:45,640 --> 00:18:51,640
And really they get in the way of peace. They get in the way of clarity of mind.

109
00:18:51,640 --> 00:18:54,640
They get in the way of happiness.

110
00:18:54,640 --> 00:18:59,640
They're a cause for stress and suffering.

111
00:18:59,640 --> 00:19:11,640
They prevent us from succeeding.

112
00:19:11,640 --> 00:19:17,640
So the Buddha said, you know, I develop uncomfortousness.

113
00:19:17,640 --> 00:19:20,640
I give up my desires.

114
00:19:20,640 --> 00:19:22,640
I have a mind of loving kindness.

115
00:19:22,640 --> 00:19:29,640
I'm without sloth and torpor. I have a peaceful mind, not restless.

116
00:19:29,640 --> 00:19:32,640
I missed one right, restlessness after sloth and torpor.

117
00:19:32,640 --> 00:19:35,640
We have restlessness.

118
00:19:35,640 --> 00:19:42,640
So the opposite of sloth and torpor is when your mind is too active, a hyperactive mind.

119
00:19:42,640 --> 00:19:48,640
Jumping here, jumping there, not able to focus on anything.

120
00:19:48,640 --> 00:19:54,640
I mean, this is really, it's a good focus for our practice.

121
00:19:54,640 --> 00:19:56,640
It's a good focus to think of these things.

122
00:19:56,640 --> 00:20:02,640
I mean, this is a very big part of what we have to be mindful of when we practice meditation.

123
00:20:02,640 --> 00:20:09,640
A very large part of our practice is to try and be mindful of the hindrances.

124
00:20:09,640 --> 00:20:14,640
And to learn to let them go to fix them to straighten our minds.

125
00:20:14,640 --> 00:20:24,640
And to cultivate such firmness and rectitude of mind that these defilements don't have power over us.

126
00:20:24,640 --> 00:20:31,640
It doesn't have a chance to consume our mind.

127
00:20:31,640 --> 00:20:41,640
It says some meditators, some religious people are given to self praise and disparagement of others.

128
00:20:41,640 --> 00:20:46,640
Some are subject to alarm and terror, fearful.

129
00:20:46,640 --> 00:20:49,640
They have great fear.

130
00:20:49,640 --> 00:20:54,640
Some are desirous of gain, honor and renown.

131
00:20:54,640 --> 00:21:02,640
Very difficult to go off into the forest if you have ambition.

132
00:21:02,640 --> 00:21:10,640
Some are lazy and wanting an energy, some are unmindful and not fully aware.

133
00:21:10,640 --> 00:21:15,640
Some are unconcentrated with straying minds.

134
00:21:15,640 --> 00:21:20,640
And here's a list of other more general qualities that are required.

135
00:21:20,640 --> 00:21:35,640
So self praise and disparagement of others, ego, ego is a real obstacle to the practice.

136
00:21:35,640 --> 00:21:42,640
Because an egoist, egoist, someone who is egotistical requires an audience.

137
00:21:42,640 --> 00:21:48,640
They require praise, they require constant validation.

138
00:21:48,640 --> 00:22:00,640
We love to make other people proud of us, have them look on us positively.

139
00:22:00,640 --> 00:22:04,640
Very difficult to find reality that way.

140
00:22:04,640 --> 00:22:16,640
Very difficult to stay with the present moment when you're consumed by the need for praise.

141
00:22:16,640 --> 00:22:20,640
Or when you're disparaging of others.

142
00:22:20,640 --> 00:22:25,640
So a nasty state.

143
00:22:25,640 --> 00:22:34,640
A subject to alarm and terror is particularly in the forest.

144
00:22:34,640 --> 00:22:36,640
It's very difficult to be in the forest.

145
00:22:36,640 --> 00:22:38,640
They're so peaceful at night.

146
00:22:38,640 --> 00:22:42,640
But can you imagine being full of fear and dread of the forest?

147
00:22:42,640 --> 00:22:46,640
I grew up in the forest, so going into the forest at night was never a scary thing.

148
00:22:46,640 --> 00:22:54,640
Of course, it's kind of troublesome about snakes and scorpions and so on.

149
00:22:54,640 --> 00:22:57,640
There are real dangers.

150
00:22:57,640 --> 00:23:01,640
And then of course, there's fear of ghosts, fear of spirits.

151
00:23:01,640 --> 00:23:12,640
There were times when I was in the forest and the noises, sometimes the noises were so disturbing.

152
00:23:12,640 --> 00:23:21,640
But just being alone with yourself, if you're worried, if you're worried, someone who worries a lot.

153
00:23:21,640 --> 00:23:34,640
If you have great fear, being alone can be such a fearsome crippling experience.

154
00:23:34,640 --> 00:23:37,640
If you're lazy, so I guess a lot in DARPA, we can't call laziness.

155
00:23:37,640 --> 00:23:39,640
It's just a weakness of mind.

156
00:23:39,640 --> 00:23:45,640
But laziness is when you really just can't be bothered.

157
00:23:45,640 --> 00:23:52,640
A lazy person living alone in the jungle will not last very long.

158
00:23:52,640 --> 00:24:01,640
It's not something that's going to, you know, they'll constantly be seeking out more comfort

159
00:24:01,640 --> 00:24:10,640
and avoiding their qualities, avoiding the cultivation of good quality qualities.

160
00:24:10,640 --> 00:24:14,640
All of the evil will seep in laziness.

161
00:24:14,640 --> 00:24:20,640
And not putting out effort, it's not actually in and of itself evil.

162
00:24:20,640 --> 00:24:26,640
It just is basically saying, allowing the evil to continue and grow.

163
00:24:26,640 --> 00:24:31,640
Those things that cause us suffering, those things that create stress in our minds,

164
00:24:31,640 --> 00:24:33,640
they grow because of laziness.

165
00:24:33,640 --> 00:24:37,640
When you might think, well, why should I bother?

166
00:24:37,640 --> 00:24:40,640
What's wrong with just living, right?

167
00:24:40,640 --> 00:24:48,640
The problem is that we're not just living. Our minds are full of things that cause us stress and suffering.

168
00:24:48,640 --> 00:24:55,640
And our laziness is like permission for them to continue.

169
00:24:55,640 --> 00:25:00,640
Some meditators are unmindful and not fully aware,

170
00:25:00,640 --> 00:25:05,640
but as I am established in mindfulness.

171
00:25:05,640 --> 00:25:21,640
There's of course key. If you're not mindful, this is where all of the problems of the mind gain hold, take control.

172
00:25:21,640 --> 00:25:28,640
Why we suffer? Why we're traumatized? Why we react violently to experiences?

173
00:25:28,640 --> 00:25:35,640
Because we're not clearly aware of the nature of the experiences as they happen.

174
00:25:35,640 --> 00:25:38,640
If you're mindful, you can never react improperly.

175
00:25:38,640 --> 00:25:45,640
If you're truly aware, in that moment there's a pure understanding of the experience.

176
00:25:45,640 --> 00:25:55,640
And because of that pure understanding, there's no reaction of liking or disliking.

177
00:25:55,640 --> 00:26:01,640
There's just experience.

178
00:26:01,640 --> 00:26:05,640
Some meditators are of unconcentrated and with straying minds.

179
00:26:05,640 --> 00:26:08,640
I am possessed of concentration, he says.

180
00:26:08,640 --> 00:26:11,640
So there's things he thought to himself.

181
00:26:11,640 --> 00:26:14,640
I'm not like this. I can go off and live in the forest.

182
00:26:14,640 --> 00:26:17,640
I can be alone with myself.

183
00:26:17,640 --> 00:26:28,640
Finally, because I'm focused. If your mind is not focused again, very easy for your mind to stray into unwholesomeness

184
00:26:28,640 --> 00:26:31,640
and things that cause you stress and suffering.

185
00:26:31,640 --> 00:26:36,640
By evil and by unwholesomeness, we just mean those things that cause you stress and suffering.

186
00:26:36,640 --> 00:26:43,640
Just sitting here, it's not always a pleasant experience because there is desire, because there is aversion,

187
00:26:43,640 --> 00:26:46,640
because there's all these things.

188
00:26:46,640 --> 00:26:59,640
It's a fairly comprehensive list. It's a good list of the sorts of things that we're going to have to work on in order to succeed in our meditation practice.

189
00:26:59,640 --> 00:27:03,640
He says something else after that, that's interesting.

190
00:27:03,640 --> 00:27:08,640
This question of whether we should avoid unpleasantness.

191
00:27:08,640 --> 00:27:20,640
He's talking particularly about fear. He says, when someone is devoid of wisdom,

192
00:27:20,640 --> 00:27:29,640
then they get afraid by living in the forest. They get upset and living in the forest.

193
00:27:29,640 --> 00:27:32,640
I know that's not the important point.

194
00:27:32,640 --> 00:27:41,640
The last one is wisdom. The final quality is wisdom.

195
00:27:41,640 --> 00:27:48,640
If one is unwise, this is the understanding. If one doesn't understand reality,

196
00:27:48,640 --> 00:27:50,640
take pain, for example.

197
00:27:50,640 --> 00:27:58,640
Now, true understanding of pain is really that pain is neutral. It's an experience.

198
00:27:58,640 --> 00:28:05,640
If you don't have that understanding, if you haven't seen that clearly, which most of us haven't,

199
00:28:05,640 --> 00:28:10,640
pain is intolerable.

200
00:28:10,640 --> 00:28:13,640
Pain is something to be done away with. Why?

201
00:28:13,640 --> 00:28:18,640
Because we don't understand it.

202
00:28:18,640 --> 00:28:27,640
An important claim in Buddhism that I repeat often is that when you understand something, it has no power over you.

203
00:28:27,640 --> 00:28:34,640
If you understand reality, as it is, if you have a clear understanding of reality,

204
00:28:34,640 --> 00:28:41,640
it's not possible for you to be disturbed, stressed, upset.

205
00:28:41,640 --> 00:28:44,640
Suffering can't arise.

206
00:28:44,640 --> 00:28:50,640
Suffering is...

207
00:28:50,640 --> 00:28:56,640
Suffering is a product of delusion, of ignorance.

208
00:28:56,640 --> 00:29:01,640
When you experience pain, if you see it just as pain, when you say to ourselves,

209
00:29:01,640 --> 00:29:05,640
pain, pain, it has no power over you.

210
00:29:05,640 --> 00:29:08,640
When you see something that would normally make you very angry,

211
00:29:08,640 --> 00:29:15,640
or you hear something that would make you very lustful or ambitious, you know, desire it.

212
00:29:15,640 --> 00:29:21,640
If you're mindful, that doesn't occur.

213
00:29:21,640 --> 00:29:26,640
When you experience it, it arises, it ceases.

214
00:29:26,640 --> 00:29:35,640
But no, the next thing I wanted to talk about was just a note because he talks about it as well in the suit days.

215
00:29:35,640 --> 00:29:41,640
When these experiences arise, what should you do? Should you do something else?

216
00:29:41,640 --> 00:29:47,640
The Buddha's practice as a Bodhisattva, before he became a Buddha,

217
00:29:47,640 --> 00:29:49,640
was to not run away from it.

218
00:29:49,640 --> 00:29:52,640
Suppose there's something that makes you afraid.

219
00:29:52,640 --> 00:30:01,640
Well, he said what he did is he went to forest shrines on the holidays.

220
00:30:01,640 --> 00:30:11,640
It's basically the equivalent of going to a haunted house on Halloween.

221
00:30:11,640 --> 00:30:17,640
In India, there would be these forest shrines where the angels or the demons,

222
00:30:17,640 --> 00:30:22,640
they would go to perpetuate these spirits, evil spirits, good spirits.

223
00:30:22,640 --> 00:30:28,640
And these spirits were simply active on the full moon, especially active on the full moon, on the new moon.

224
00:30:28,640 --> 00:30:30,640
It's superstition.

225
00:30:30,640 --> 00:30:35,640
But basically it's the equivalent of a haunted house on Halloween.

226
00:30:35,640 --> 00:30:38,640
It's the scariest possible situation.

227
00:30:38,640 --> 00:30:42,640
I went there and I did walking meditation.

228
00:30:42,640 --> 00:30:46,640
And if when I was walking, I got afraid.

229
00:30:46,640 --> 00:30:55,640
I would say to myself, I'm not going to stop walking until this fear goes.

230
00:30:55,640 --> 00:31:01,640
And if I was sitting, I would say I'm not going to stop sitting.

231
00:31:01,640 --> 00:31:04,640
Meaning he didn't even change his posture.

232
00:31:04,640 --> 00:31:14,640
The point is, when the fear came, he made a promise that he would stay and deal with the fear.

233
00:31:14,640 --> 00:31:18,640
It's actually just one example of the sorts of things that can arise,

234
00:31:18,640 --> 00:31:25,640
but it's an example of how to deal with these sorts of, with any sort of problem.

235
00:31:25,640 --> 00:31:31,640
When you're very angry, as a meditator, you shouldn't run away from the thing that makes you angry.

236
00:31:31,640 --> 00:31:36,640
In fact, to some extent, following the Buddha's example,

237
00:31:36,640 --> 00:31:41,640
it's good for you to be in association with the things that make you angry.

238
00:31:41,640 --> 00:31:50,640
To the extent that you're able, if you're strong enough to be able to change the way you look at the experience,

239
00:31:50,640 --> 00:31:53,640
it can be quite powerful.

240
00:31:53,640 --> 00:32:05,640
When you're able to experience the same thing without getting disturbed, stressed, upset by it.

241
00:32:05,640 --> 00:32:13,640
So with all these qualities, it's not something to be afraid of or discouraged by.

242
00:32:13,640 --> 00:32:20,640
If you have any of these problems that are going to get in the way of your practice,

243
00:32:20,640 --> 00:32:25,640
really the best way to deal with them is to spend some time being mindful of them.

244
00:32:25,640 --> 00:32:28,640
If you're lazy, be mindful of your laziness.

245
00:32:28,640 --> 00:32:31,640
If you're angry, be mindful of your anger.

246
00:32:31,640 --> 00:32:39,640
If you want something or you're addicted to something, spend time being mindful of it in all of its aspects.

247
00:32:39,640 --> 00:32:48,640
The feelings, the emotions, the thoughts, the experiences.

248
00:32:48,640 --> 00:32:53,640
And then he goes through the progression of enlightenment, which is fairly standard and I won't go into it.

249
00:32:53,640 --> 00:32:56,640
I thought this would be interesting to talk about these.

250
00:32:56,640 --> 00:33:03,640
It gives us a chance to go over things that get in the way of our practice.

251
00:33:03,640 --> 00:33:07,640
We're all very interested in learning how to stay with ourselves, how to be with ourselves,

252
00:33:07,640 --> 00:33:11,640
and how to have it be a productive experience.

253
00:33:11,640 --> 00:33:21,640
Now, they're suited as a good job explaining the ways that the difference between a productive meditator and an unproductive meditator.

254
00:33:21,640 --> 00:33:29,640
So, there you go. That's the demo for tonight. Thank you all for tuning in for coming out.

255
00:33:29,640 --> 00:33:34,640
Saturday's now we have visitors. It's advertised.

256
00:33:34,640 --> 00:33:39,640
I also give talks Monday and Wednesday. You're welcome to come out then.

257
00:33:39,640 --> 00:33:43,640
And of course our center is open anytime you're welcome to just come by.

258
00:33:43,640 --> 00:33:50,640
If you want to learn how to meditate, just make an appointment and we can set something up.

259
00:33:50,640 --> 00:33:58,640
So, questions are for Wednesday. I'm going to try that for a while.

260
00:33:58,640 --> 00:34:04,640
See how that goes. No questions tonight. We'll save all our questions once a week.

261
00:34:04,640 --> 00:34:08,640
There's too many questions. No, there's lots of questions.

262
00:34:08,640 --> 00:34:11,640
So Wednesday will be Question Day.

263
00:34:11,640 --> 00:34:18,640
Saturday is giving an official talk based on some teaching of the Buddha.

264
00:34:18,640 --> 00:34:25,640
So, thank you all for coming out. Again, have a good night.

