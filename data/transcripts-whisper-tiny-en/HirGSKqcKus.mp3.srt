1
00:00:00,000 --> 00:00:14,000
So, I thought I'd start off my new, new tube with a video on how to meditate.

2
00:00:14,000 --> 00:00:23,000
That's going to be how to do it yourself at home all along sort of video.

3
00:00:23,000 --> 00:00:26,000
The first thing is to explain a little bit about meditation.

4
00:00:26,000 --> 00:00:28,000
What does it mean to meditate?

5
00:00:28,000 --> 00:00:34,000
Well, the word meditation is a English word.

6
00:00:34,000 --> 00:00:38,000
It comes from Latin or it comes from Greek.

7
00:00:38,000 --> 00:00:46,000
And it comes from the same root as the word medication.

8
00:00:46,000 --> 00:00:52,000
But medication is something which cures the thickness,

9
00:00:52,000 --> 00:00:56,000
and remedy the sicknesses in the body.

10
00:00:56,000 --> 00:01:00,000
Meditation is something which remedies the sicknesses in the mind.

11
00:01:00,000 --> 00:01:04,000
And so someone might ask, well, I don't have mental illness.

12
00:01:04,000 --> 00:01:10,000
I'm not sick in my mind. Why should I meditate?

13
00:01:10,000 --> 00:01:17,000
But we consider mental sickness means any mental upset or mental stress.

14
00:01:17,000 --> 00:01:29,000
So people who are all stressed out if you have a friend who's got stress or depression or sadness or short attention span.

15
00:01:29,000 --> 00:01:32,000
A-D-D-D-A-D-A-D.

16
00:01:32,000 --> 00:01:35,000
So I'm hyperactive disorders.

17
00:01:35,000 --> 00:01:38,000
This is the reason why we meditate.

18
00:01:38,000 --> 00:01:40,000
We've got this too.

19
00:01:40,000 --> 00:01:44,000
I'll do away with any sort of mental upset, even if we're an ordinary person.

20
00:01:44,000 --> 00:01:50,000
Find our lives are just not as fulfilling as we wish them to be or so on.

21
00:01:50,000 --> 00:01:52,000
There are many reasons.

22
00:01:52,000 --> 00:01:59,000
In fact, it's meditation to make the mind strong and firm and to make the strong and peaceful and happy.

23
00:01:59,000 --> 00:02:05,000
And the focus of my YouTube is my channel.

24
00:02:05,000 --> 00:02:16,000
I thought this would be a good start to teach people how to find inner peace and inner happiness.

25
00:02:16,000 --> 00:02:22,000
Because this then, of course, unfolds into outer peace and outer happiness.

26
00:02:22,000 --> 00:02:24,000
We're all in early peaceful and happy.

27
00:02:24,000 --> 00:02:29,000
This is how we bring about world peace and world happiness.

28
00:02:29,000 --> 00:02:36,000
So let's get right into it.

29
00:02:36,000 --> 00:02:37,000
We'll talk about meditation.

30
00:02:37,000 --> 00:02:41,000
Meditation means to cure these sicknesses of the mind.

31
00:02:41,000 --> 00:02:44,000
Let's learn how to practice meditation.

32
00:02:44,000 --> 00:02:45,000
I'm going to start.

33
00:02:45,000 --> 00:02:47,000
This is only going to be part one.

34
00:02:47,000 --> 00:02:50,000
We'll go slowly through this and then my next part I'll teach in more advanced sort of meditation and so on and so on.

35
00:02:50,000 --> 00:02:58,000
But for the first part I'll teach by the teach just a brief sort of meditation, a very simple meditation.

36
00:02:58,000 --> 00:03:05,000
And what we're going to look at in meditation is we're going to look at our body and our mind.

37
00:03:05,000 --> 00:03:13,000
Because again, real meditation has to be able to cure the things which are wrong with our minds,

38
00:03:13,000 --> 00:03:19,000
wrong with our subconscious and our conscious mind.

39
00:03:19,000 --> 00:03:29,000
So we have to use that as our object.

40
00:03:29,000 --> 00:03:32,000
We can't do any of these enemy transcendental meditations or image-free or so on and so on.

41
00:03:32,000 --> 00:03:34,000
We need to visualizeations and so on.

42
00:03:34,000 --> 00:03:36,000
We can't do any of that.

43
00:03:36,000 --> 00:03:44,000
This won't cure us of the problem because it has nothing to do with what is the real problem which is up here and down here.

44
00:03:44,000 --> 00:03:49,000
I'm going to start by teaching sitting meditation.

45
00:03:49,000 --> 00:03:53,000
When those people might think, well, meditation is sitting with some special here.

46
00:03:53,000 --> 00:03:57,000
When this isn't so, I'm going to teach walking meditation, but first sitting meditation.

47
00:03:57,000 --> 00:04:00,000
Sitting meditation, so everyone hold up your right hand.

48
00:04:00,000 --> 00:04:02,000
I'm wearing no valves.

49
00:04:02,000 --> 00:04:08,000
I'm going to use our right hand and place it on your stomach.

50
00:04:08,000 --> 00:04:15,000
I'm going to have to get right in there and place it on top of your clothes or your robes and that's fine.

51
00:04:15,000 --> 00:04:25,000
Just so that you can feel your stomach and try to feel the breath going into the body and the breath leaving the body.

52
00:04:25,000 --> 00:04:31,000
So you can use the body as our subject, our object.

53
00:04:31,000 --> 00:04:36,000
When the breath goes into the body, the stomach will rise.

54
00:04:36,000 --> 00:04:39,000
This naturally, you don't have to force it.

55
00:04:39,000 --> 00:04:45,000
You don't have to artificially make it long or deep.

56
00:04:45,000 --> 00:04:49,000
The only just watch the breath as it naturally goes into the body.

57
00:04:49,000 --> 00:04:52,000
Watch the stomach as it rises.

58
00:04:52,000 --> 00:04:56,000
When the breath reaches the stomach, the stomach will rise naturally.

59
00:04:56,000 --> 00:05:00,000
When the breath goes out of the body, the stomach will fall naturally.

60
00:05:00,000 --> 00:05:10,000
We're going to use a word to keep our mind fixed on the stomach because otherwise our minds are going to wander.

61
00:05:10,000 --> 00:05:16,000
We're not going to see clearly about this body starting with the stomach.

62
00:05:16,000 --> 00:05:20,000
If we can't see clearly about the body, we're not going to be able to see clearly about the mind.

63
00:05:20,000 --> 00:05:23,000
Because of course the mind word is the mind bit that lives in the body.

64
00:05:23,000 --> 00:05:29,000
We're starting with the stomach we're going to use this as a way to see also the mind.

65
00:05:29,000 --> 00:05:36,000
What we're going to do when the stomach rises naturally, we're going to say to ourselves,

66
00:05:36,000 --> 00:05:37,000
we're going to see.

67
00:05:37,000 --> 00:05:42,000
When the stomach falls, we're going to say to ourselves,

68
00:05:42,000 --> 00:05:49,000
I see.

69
00:05:49,000 --> 00:05:54,000
I see.

70
00:05:54,000 --> 00:05:55,000
You can keep your hand there.

71
00:05:55,000 --> 00:05:57,000
If it gets to the point where you don't need to use your hand,

72
00:05:57,000 --> 00:06:00,000
then you can feel it even without having your hand there.

73
00:06:00,000 --> 00:06:03,000
You can take your hand away and put your hands on your lap,

74
00:06:03,000 --> 00:06:04,000
just like you see.

75
00:06:04,000 --> 00:06:06,000
You can see the Buddha do.

76
00:06:06,000 --> 00:06:08,000
Whenever you like, you can put your hands.

77
00:06:08,000 --> 00:06:09,000
You can do this in a chair.

78
00:06:09,000 --> 00:06:14,000
You can do this lying down, lying meditation.

79
00:06:14,000 --> 00:06:17,000
I think, however you choose to do it.

80
00:06:17,000 --> 00:06:21,000
We'll do it this altogether now for a few minutes.

81
00:06:21,000 --> 00:06:27,000
Then we'll look for a brief introduction to Vipasana meditation,

82
00:06:27,000 --> 00:06:30,000
meditation, which helps us to see clearly,

83
00:06:30,000 --> 00:06:36,000
which will be a cause for us to do our mental upset

84
00:06:36,000 --> 00:06:39,000
or mental illnesses, which will be all carried around with us

85
00:06:39,000 --> 00:06:40,000
to depression inside of it.

86
00:06:40,000 --> 00:06:42,000
We're going to work them out, slowly.

87
00:06:42,000 --> 00:06:46,000
We'll start here with a very simple meditation based on the stomach.

88
00:06:46,000 --> 00:06:49,000
We just say in our minds rising,

89
00:06:49,000 --> 00:06:55,000
saying, you don't have to say it out loud, please don't say it out loud.

90
00:06:55,000 --> 00:07:00,000
In the mind, in the mind, I'm arising in the mind.

91
00:07:00,000 --> 00:07:02,000
Being involved.

92
00:07:02,000 --> 00:07:27,000
We're going to do that now for a few minutes.

93
00:07:27,000 --> 00:07:36,000
We're going to do that.

94
00:07:36,000 --> 00:07:45,000
We're going to do that.

95
00:07:45,000 --> 00:08:08,000
We're going to do that.

96
00:08:08,000 --> 00:08:18,000
Now let's start at Vipasana meditation.

97
00:08:18,000 --> 00:08:22,000
You may be wondering exactly what this has to do with the mind

98
00:08:22,000 --> 00:08:24,000
and curing the mind.

99
00:08:24,000 --> 00:08:31,000
I'd just like to say in this episode that before we can start to change

100
00:08:31,000 --> 00:08:37,000
or cure the problems, we have to understand the disease

101
00:08:37,000 --> 00:08:40,000
like in medicine, first you have to take science.

102
00:08:40,000 --> 00:08:48,000
You have to understand what our organs and what our cells and all of these things

103
00:08:48,000 --> 00:08:50,000
and how their systems work together.

104
00:08:50,000 --> 00:08:54,000
In the same way, the first thing before we're going to work on the mind,

105
00:08:54,000 --> 00:08:57,000
first we have to understand the mind.

106
00:08:57,000 --> 00:08:59,000
In the beginning, we're going to look at the body,

107
00:08:59,000 --> 00:09:02,000
the rising and the falling and the mind,

108
00:09:02,000 --> 00:09:07,000
which says rising and the falling and the rising.

109
00:09:07,000 --> 00:09:11,000
Even in just a few minutes, probably you can already see something about your own mind.

110
00:09:11,000 --> 00:09:16,000
Maybe for many people, the mind is wandering and the mind is not with the rising

111
00:09:16,000 --> 00:09:21,000
and the falling, maybe the mind is getting bored or getting upset

112
00:09:21,000 --> 00:09:24,000
or maybe it feels very happy and peaceful.

113
00:09:24,000 --> 00:09:28,000
You can start to see how the mind works and how the body works.

114
00:09:28,000 --> 00:09:33,000
You can start to work on the problems and see what are the real problems and why it is

115
00:09:33,000 --> 00:09:42,000
that we as human beings still have pain and suffering and sadness and so on.

116
00:09:42,000 --> 00:09:45,000
Why these things still come up from time to time,

117
00:09:45,000 --> 00:09:50,000
when we can learn to overcome and do away with these things and be happy

118
00:09:50,000 --> 00:09:52,000
and peaceful all the time.

119
00:09:52,000 --> 00:09:59,000
Thank you for tuning in.

