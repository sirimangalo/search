1
00:00:00,000 --> 00:00:16,680
Okay, so tonight we gently garnee a mechanism, it's the discourse on which should be done

2
00:00:16,680 --> 00:00:38,680
or love, which should be done by one who wishes to attain freedom through loving kindness.

3
00:00:38,680 --> 00:00:46,360
When we went to the Dharma Center, they chanted it, oh no, they had me read it, I read it.

4
00:00:46,360 --> 00:00:56,960
They have someone read it every week, or maybe every time they meet, and they read it in English,

5
00:00:56,960 --> 00:01:00,800
because the idea is to understand what you're saying.

6
00:01:00,800 --> 00:01:15,720
The Karnia Mantis, it's actually more of a reminder, it's not actually, it's not actually

7
00:01:15,720 --> 00:01:17,600
a meditation practice, you see.

8
00:01:17,600 --> 00:01:23,400
There's the one part where it starts to talk about, he aids a band about that day,

9
00:01:23,400 --> 00:01:32,040
that's a lot, a lot, a lot, and so on.

10
00:01:32,040 --> 00:01:37,720
Sookie no one does something, he's a tad on the Sookie tad, so you should, it's explaining

11
00:01:37,720 --> 00:01:41,880
how to practice me at that.

12
00:01:41,880 --> 00:01:48,360
Because explaining what should be done, if you want, if your practice is made that, made

13
00:01:48,360 --> 00:01:55,680
that means love, so it starts off by telling you what the preliminaries are, if you want

14
00:01:55,680 --> 00:02:06,400
to be a person who is proficient or a loving, if you want to be a loving person, you need

15
00:02:06,400 --> 00:02:20,520
certain prerequisites, you have to be, suckle with you, just with you, so what you

16
00:02:20,520 --> 00:02:30,040
would just say, just someone doing it in mind, and so the first part is about how you should,

17
00:02:30,040 --> 00:02:40,120
you should train yourself if you would like to become a loving person.

18
00:02:40,120 --> 00:02:47,800
So it's, as with all of the good things in the world, mindfulness, wisdom, compassion,

19
00:02:47,800 --> 00:02:55,280
there are not things you can just set out and cultivate, you can't just say, I'm going

20
00:02:55,280 --> 00:03:01,200
to be, I'm going to be in love with everyone, I'm going to have love for the whole world,

21
00:03:01,200 --> 00:03:07,160
just as you can say, I'm going to be wise, you can't will it to happen, this is something

22
00:03:07,160 --> 00:03:12,880
that takes work, love is something, there's a quality of mind that takes work.

23
00:03:12,880 --> 00:03:23,120
And this is important, because otherwise we quite often have a wrong idea of what is

24
00:03:23,120 --> 00:03:38,440
meant by love, we have an idea of love that is actually perhaps even unwholesome.

25
00:03:38,440 --> 00:03:43,680
It's the kind of love that we have for our families, or that we have for our, what we

26
00:03:43,680 --> 00:03:46,720
say, loved ones.

27
00:03:46,720 --> 00:03:54,240
Now when you have love for loved ones, most of the, what you call love is actually some

28
00:03:54,240 --> 00:04:01,000
sort of attachment, and that's why it's one, that's why it's particular.

29
00:04:01,000 --> 00:04:09,440
If you ask anyone, do you love your family more than then you love perfect strangers

30
00:04:09,440 --> 00:04:16,160
on the street, and of course everyone would say, yes, I do, but the question is, well,

31
00:04:16,160 --> 00:04:23,680
why is that, there's various reasons we're closer to them, we know them more, we understand

32
00:04:23,680 --> 00:04:28,720
them better, but the biggest one is that we're attached to them more, we're more attached

33
00:04:28,720 --> 00:04:32,080
to our families than we are to our friends and we're more attached to our friends than

34
00:04:32,080 --> 00:04:36,080
we are to perfect strangers.

35
00:04:36,080 --> 00:04:49,080
So we have these, this idea of love that is actually based on our desires.

36
00:04:49,080 --> 00:04:58,160
So when you say, you love your friend, but the reason you say that, what you mean is that

37
00:04:58,160 --> 00:05:05,320
this person makes you happy, when you say, oh, I love so and so, why do you say that?

38
00:05:05,320 --> 00:05:11,600
You say that as an expression of pleasure, because that person brings you pleasure, right?

39
00:05:11,600 --> 00:05:18,360
You don't say it, we often don't say it because it means I want to help that person.

40
00:05:18,360 --> 00:05:23,920
See, there's the aspect that is love, but there's the aspect that this person makes me

41
00:05:23,920 --> 00:05:33,800
happy, oh, I love oranges, for example, oh, I love cheesecake, this we use this word love,

42
00:05:33,800 --> 00:05:38,760
but this isn't, see, it's not, it's not a pure love and there is love, especially for

43
00:05:38,760 --> 00:05:45,560
our families, for example, there is the desire to help them, but it's so caught up in

44
00:05:45,560 --> 00:05:52,520
the desire that they should be and act in a certain way, which is attachment.

45
00:05:52,520 --> 00:06:02,720
And this is, this is the word we use is caring, we say I care for the people I love.

46
00:06:02,720 --> 00:06:08,120
And so I had a question recently, someone asked me, if you give up attachment, how can

47
00:06:08,120 --> 00:06:13,840
you care for the ones that you love, do you, there's a person who lets go, not care

48
00:06:13,840 --> 00:06:20,280
anymore, they've actually gotten this sort of question before and it's actually true that

49
00:06:20,280 --> 00:06:27,080
love doesn't care, love isn't caring, love and caring are two very different things, it's

50
00:06:27,080 --> 00:06:34,400
difficult, it's a difficult teaching, but I'm quite sure it's true, love is ready to

51
00:06:34,400 --> 00:06:43,920
help, but it doesn't care in the sense that it doesn't become upset when things don't

52
00:06:43,920 --> 00:06:49,160
go as planned, you see, think of the word, the word care, it doesn't mean that you don't

53
00:06:49,160 --> 00:06:55,720
care for people, the way a nurse cares for people, you care for people as appropriate

54
00:06:55,720 --> 00:07:03,080
to your situation, out of love, in the sense of actively working for them or doing something

55
00:07:03,080 --> 00:07:06,480
for their benefit.

56
00:07:06,480 --> 00:07:14,560
But if you help them or if you do your part and they don't get better or they don't

57
00:07:14,560 --> 00:07:22,480
appreciate it, you don't care, you see, so you don't have this caring in terms of being upset

58
00:07:22,480 --> 00:07:29,160
or having your happiness dependent on something, you see, a person who is truly free

59
00:07:29,160 --> 00:07:38,320
from attachment, who won't care what happens, in the sense that they won't be moved by

60
00:07:38,320 --> 00:07:45,240
what happens, they will care in the sense that they will differentiate between actions,

61
00:07:45,240 --> 00:07:53,320
so they will act in such a way as to bring benefit, so they will care in the sense that

62
00:07:53,320 --> 00:08:03,400
they won't say, if I hurt you or if I help you at all the same, they care, they do the

63
00:08:03,400 --> 00:08:11,160
things that bring care to people, or bring care, or bring benefit, you see.

64
00:08:11,160 --> 00:08:18,440
So when we talk about caring, caring what happens to people, you see, we're dealing with

65
00:08:18,440 --> 00:08:25,720
two very different mind-states, the one that looks and thinks this is something that

66
00:08:25,720 --> 00:08:31,880
is helpful for that person and decides to do it, doesn't just sit back and watch them,

67
00:08:31,880 --> 00:08:36,960
suffer, or doesn't just sit back and watch them being in need, you know, when they need

68
00:08:36,960 --> 00:08:46,880
something, love and this kind of caring is the giving as it's appropriate to give, helping

69
00:08:46,880 --> 00:08:51,400
as it's appropriate to help.

70
00:08:51,400 --> 00:08:57,520
But there will be no anxiety, if it's not appropriate to help or if there's no way

71
00:08:57,520 --> 00:09:03,360
you can help when you see someone suffering greatly, you don't care in the sense that

72
00:09:03,360 --> 00:09:09,120
you don't get upset when they're suffering, you do what you can to help in your mind

73
00:09:09,120 --> 00:09:14,160
for and you're clear and mind about.

74
00:09:14,160 --> 00:09:22,720
And so the easiest way to understand the difference here is that true love is universal.

75
00:09:22,720 --> 00:09:30,080
You see, it won't, how we can tell the difference between love and attachment is that

76
00:09:30,080 --> 00:09:35,760
attachment is partial to certain things and certain people.

77
00:09:35,760 --> 00:09:40,520
The love that we have for our parents, the love that we have for our children, the part

78
00:09:40,520 --> 00:09:46,760
of it that is love is the same sort of thing that can be developed for all beings.

79
00:09:46,760 --> 00:09:59,440
But the attachment can only be developed for one segment of reality or one portion of

80
00:09:59,440 --> 00:10:07,000
reality, one segment of the population, so we have attachment for certain people.

81
00:10:07,000 --> 00:10:13,840
But love is something that it can exist the same extent between all beings.

82
00:10:13,840 --> 00:10:18,440
We can have love for a perfect stranger, something we don't normally think of.

83
00:10:18,440 --> 00:10:26,640
We normally think that love is something that you reserve for your loved ones, but that

84
00:10:26,640 --> 00:10:35,400
is actually more, that's a kind of love in the sense of being based on attachment.

85
00:10:35,400 --> 00:10:42,360
So when we develop love, this is, this is, first of all, understanding that it's something

86
00:10:42,360 --> 00:10:43,360
that takes work.

87
00:10:43,360 --> 00:10:48,320
It's not something that, it's not this easy kind of love that you have for apples and oranges

88
00:10:48,320 --> 00:10:50,480
and sweets and so on.

89
00:10:50,480 --> 00:10:56,360
It's not even the love that you have for a romantic involvement or even the love that

90
00:10:56,360 --> 00:11:00,200
you have for your children or your parents, per se.

91
00:11:00,200 --> 00:11:05,720
I mean, it's a large part of it, part of that is just attachment.

92
00:11:05,720 --> 00:11:07,880
The way you can tell is because love doesn't care.

93
00:11:07,880 --> 00:11:13,880
If a person dies, love doesn't get upset, love doesn't cause you to be sad when a person

94
00:11:13,880 --> 00:11:14,880
dies.

95
00:11:14,880 --> 00:11:15,880
That's attachment.

96
00:11:15,880 --> 00:11:21,720
You see, love doesn't cause you to get upset when a person leaves you or when a person

97
00:11:21,720 --> 00:11:30,040
changes when your friends betray you, this is attachment.

98
00:11:30,040 --> 00:11:37,320
The love that we have for our parents and for our children is the dedication to them

99
00:11:37,320 --> 00:11:48,840
and the going out of our way to help them, but the worrying and the sort of the caring

100
00:11:48,840 --> 00:11:59,160
part is that is based on attachment, which is much easier, much easier to love cheesecake

101
00:11:59,160 --> 00:12:02,960
than it is to love your enemies, for example.

102
00:12:02,960 --> 00:12:08,720
So this is a good way to test your love and to understand love is to cultivate it not only

103
00:12:08,720 --> 00:12:15,160
on those you love but cultivate it on those who you don't know and on those who you

104
00:12:15,160 --> 00:12:20,400
actually are upset at because then you can see where, then actually the best people are

105
00:12:20,400 --> 00:12:23,880
those you don't like because then you know the only thing that you're going to have

106
00:12:23,880 --> 00:12:24,880
is love.

107
00:12:24,880 --> 00:12:27,640
You'll have no attachment for someone you don't like.

108
00:12:27,640 --> 00:12:36,120
If you have someone who you're angry at when you try to cultivate love for that person you'll

109
00:12:36,120 --> 00:12:42,960
find out what true love is because you won't be able to get attached to them.

110
00:12:42,960 --> 00:12:48,200
You won't give rise to the same attachment as you will for someone you love, quote, unquote

111
00:12:48,200 --> 00:12:51,280
love.

112
00:12:51,280 --> 00:12:59,920
So first important point love is something that's difficult and takes training to cultivate.

113
00:12:59,920 --> 00:13:03,040
And so there's things that you have to do, you have to be upright, you have to be said

114
00:13:03,040 --> 00:13:11,440
in morality, you have to be a charitable person, you can't be stingy, you have to practice

115
00:13:11,440 --> 00:13:16,800
meditation, the best way to cultivate love and kindness is to clear your mind and your mind

116
00:13:16,800 --> 00:13:23,120
is pure and in light and being is naturally able to cultivate love and kindness and wish

117
00:13:23,120 --> 00:13:27,960
for good things for all being.

118
00:13:27,960 --> 00:13:34,160
So content easy to support with few duties living lightly controlled in senses, your mind

119
00:13:34,160 --> 00:13:42,120
has to be focused, your mind has to be free from these kind of attachments, essential attachments

120
00:13:42,120 --> 00:13:47,200
and has to be free from worry, it has to be free from anxiety fear, it has to be free

121
00:13:47,200 --> 00:13:54,840
from shame and guilt for having done bad things so you have to be moral and virtuous

122
00:13:54,840 --> 00:14:01,000
in order to cultivate love.

123
00:14:01,000 --> 00:14:06,040
And not attached to families, these are actually words for monks.

124
00:14:06,040 --> 00:14:11,720
This is a lot of the Buddha's teaching, some people ask, well then is this sort of teaching

125
00:14:11,720 --> 00:14:16,000
really applicable to lay people who are talking about things like being easy to support

126
00:14:16,000 --> 00:14:19,200
in terms of going on Armstrong or so on.

127
00:14:19,200 --> 00:14:24,400
But the Buddha would always frame it in terms of the training for the monks because they

128
00:14:24,400 --> 00:14:29,080
were the ones he gave the majority of his talks to.

129
00:14:29,080 --> 00:14:32,920
So this talk was actually given to a group of monks and there is a very famous story

130
00:14:32,920 --> 00:14:35,560
about the kind of Neumetasuta.

131
00:14:35,560 --> 00:14:41,200
They went into this forest to practice meditation and the angels tried to kick them out

132
00:14:41,200 --> 00:14:46,120
and so they came back with the Midasuta and when the angels heard the Midas, the kind

133
00:14:46,120 --> 00:14:52,640
of Neumetasuta they took care of the monks and they let them stay.

134
00:14:52,640 --> 00:14:56,440
So the Buddha taught this to these monks and they go back and chant the Midasuta.

135
00:14:56,440 --> 00:15:01,720
So we always go and chant it when we go to a new place, when we go to the forest.

136
00:15:01,720 --> 00:15:10,120
We always try to remember to chant this Buddha and think of all the good things in it

137
00:15:10,120 --> 00:15:17,080
to cultivate love and kindness for all the beings in the forest.

138
00:15:17,080 --> 00:15:27,400
But what I mean is the Buddha would give a teaching that was detailed directed towards

139
00:15:27,400 --> 00:15:32,960
the monks but the same principles, the meaning was that the same principles should be

140
00:15:32,960 --> 00:15:35,160
practiced by the late people only to a lesser degree.

141
00:15:35,160 --> 00:15:40,360
So when you have the whole of the Buddhist monastic code, all these 227 precepts and all

142
00:15:40,360 --> 00:15:45,720
the minor precepts, that is the detailed analysis of morality.

143
00:15:45,720 --> 00:15:49,480
But the basic morality is keeping five precepts.

144
00:15:49,480 --> 00:15:53,040
So if you keep five precepts, that is enough to become free from suffering.

145
00:15:53,040 --> 00:15:57,800
Of course, eight precepts, five or eight precepts you can keep.

146
00:15:57,800 --> 00:16:02,920
It is really the basis of the rest of the precepts.

147
00:16:02,920 --> 00:16:08,000
So in the same view, you shouldn't just discard the things that the Buddha says.

148
00:16:08,000 --> 00:16:15,480
In this example, a teaching that he gives to the monks, we should take this as the highest

149
00:16:15,480 --> 00:16:18,680
form of practice.

150
00:16:18,680 --> 00:16:22,960
So being unattached to families, so that means because monks shouldn't get caught up in

151
00:16:22,960 --> 00:16:31,360
their interactions with late people or being partial to going into the village and hanging

152
00:16:31,360 --> 00:16:33,080
out with late people.

153
00:16:33,080 --> 00:16:36,720
But then you think, well, why is that and what is the quality that we are trying to avoid?

154
00:16:36,720 --> 00:16:43,400
Of course, the busyness and socializing, which are very important qualities to avoid

155
00:16:43,400 --> 00:16:46,520
for late people as well.

156
00:16:46,520 --> 00:16:47,520
So I'm pointing.

157
00:16:47,520 --> 00:16:52,600
Being when we read many of the Buddha's teachings, we see that they are directed towards monks,

158
00:16:52,600 --> 00:16:57,320
but we should take the principle behind each one of them.

159
00:16:57,320 --> 00:16:59,720
Sometimes it is difficult to understand if you have a lived as a monk.

160
00:16:59,720 --> 00:17:04,720
So as you can have the monks explain, why is it that we shouldn't go to families or how

161
00:17:04,720 --> 00:17:06,960
is it that a monk gets attached to families?

162
00:17:06,960 --> 00:17:08,920
These are the ways.

163
00:17:08,920 --> 00:17:15,520
But these teachings are important for anyone that has got any amount of kusilina, someone

164
00:17:15,520 --> 00:17:23,360
who is skilled in good, anyone who is skilled in good and wishes to attain that state

165
00:17:23,360 --> 00:17:25,160
of peace.

166
00:17:25,160 --> 00:17:35,560
That state of peace refers to the John or the calm and the state of tranquility and absorption

167
00:17:35,560 --> 00:17:40,760
that comes from cultivating loving kayyas.

168
00:17:40,760 --> 00:17:46,200
And then it goes into a, if you read through it, you can see how the Buddha gives an example

169
00:17:46,200 --> 00:17:51,960
of how to practice loving kayyas, but it's sort of an overview.

170
00:17:51,960 --> 00:17:55,360
The actual practice can take two forms.

171
00:17:55,360 --> 00:18:04,440
This form is the one that I favor as sort of a supplement to Vipassana meditation.

172
00:18:04,440 --> 00:18:12,760
It's that you start with yourself, for example, and then you move outward physically.

173
00:18:12,760 --> 00:18:17,760
So here he's even the example of sending to all beings at once.

174
00:18:17,760 --> 00:18:22,280
But the method of practice would be to cultivate it for yourself first, say, may I be happy,

175
00:18:22,280 --> 00:18:24,960
may I be free from suffering?

176
00:18:24,960 --> 00:18:29,200
And then go to the people in the room and think about them, may they be happy and may

177
00:18:29,200 --> 00:18:35,120
they be free from suffering, and then go to the people in the building, and you can think

178
00:18:35,120 --> 00:18:39,280
of the beings, if you think of angels being in the building or ghosts or whatever, send

179
00:18:39,280 --> 00:18:44,200
to them as well, all of the animals and if there are animals in the area, of course in

180
00:18:44,200 --> 00:18:50,280
the forest, this is more importantly, send to all the mosquitoes and all the leeches, send

181
00:18:50,280 --> 00:18:54,880
to all the little beings and things and the monkeys and so on.

182
00:18:54,880 --> 00:19:01,720
And then you send to all the people in the city or the town or the area, the district and

183
00:19:01,720 --> 00:19:07,720
then you send to all the people in the province or the state and then you send to all

184
00:19:07,720 --> 00:19:20,640
the beings in the country and you send in degrees of size or physical space.

185
00:19:20,640 --> 00:19:29,880
And this helps you to sort of work up to this taking in the global or the universal population

186
00:19:29,880 --> 00:19:35,840
of taking in all beings, may all beings be happy and free from suffering.

187
00:19:35,840 --> 00:19:42,880
And then you can go in, of course you can go in in types as well, so you start with the

188
00:19:42,880 --> 00:19:47,200
humans and then go into the animals and the angels and the ghosts, and you can do that

189
00:19:47,200 --> 00:19:55,480
at every level until you get to, may all animals in the world, may all angels, may all

190
00:19:55,480 --> 00:19:59,160
Brahmins and so on.

191
00:19:59,160 --> 00:20:05,400
And then you can even start playing with it.

192
00:20:05,400 --> 00:20:09,600
So all beings in the north, may they all be happy, all beings in the south, may they

193
00:20:09,600 --> 00:20:17,840
all be the west, east, the Buddha gave many different examples of how you can practice

194
00:20:17,840 --> 00:20:18,840
mid-diets.

195
00:20:18,840 --> 00:20:24,360
The reason for quote unquote playing is that it cultivates your mind, it's a mental training,

196
00:20:24,360 --> 00:20:31,280
it's like running drills for tennis or some sport, whatever sport that you play, you

197
00:20:31,280 --> 00:20:37,440
run drills and you do it in different, you get in different scenarios to help to train

198
00:20:37,440 --> 00:20:41,320
your body and in this case to train your mind.

199
00:20:41,320 --> 00:20:45,960
To the point where you can send loving kindness easier, you're training yourself in being

200
00:20:45,960 --> 00:20:53,080
a loving person, in wishing people well, that's basically what I love you.

201
00:20:53,080 --> 00:20:59,600
The other way to cultivate loving kindness is I think more favored for people who are

202
00:20:59,600 --> 00:21:06,680
cultivating it for the Janus, because in the Visudhi manga I think this is how it

203
00:21:06,680 --> 00:21:15,200
advises you to cultivate it, is starting with yourself, but it's just as an example,

204
00:21:15,200 --> 00:21:20,320
the Visudhi manga says you can't enter the Janah by wishing love for yourself because

205
00:21:20,320 --> 00:21:24,960
it's not really loving, you know, talking about loving yourself is not really loving,

206
00:21:24,960 --> 00:21:31,000
the Visudhi manga says it won't lead to concentration.

207
00:21:31,000 --> 00:21:34,960
Start with the people you love, start with your family, start with your parents, your

208
00:21:34,960 --> 00:21:39,520
children, start with your, the ones that you love and pick someone on the same gender,

209
00:21:39,520 --> 00:21:46,480
to pick someone you're not going to be attracted to, because otherwise it gets confused

210
00:21:46,480 --> 00:21:52,480
with lust, it gets confused with attachment, and try to send loving kindness to them and

211
00:21:52,480 --> 00:21:56,960
cultivate it to the point where you're able to love these people, you're able to love

212
00:21:56,960 --> 00:22:01,920
the people who you already love, this is an easy thing to do, it's an easy exercise.

213
00:22:01,920 --> 00:22:07,880
Even when you can do that, try to think of someone that you don't know very well, you don't

214
00:22:07,880 --> 00:22:12,400
have an opinion about, maybe you know them, but let's say someone you work with or someone

215
00:22:12,400 --> 00:22:17,520
you don't have an opinion with, you say hello to them, they're not your friend, but they're

216
00:22:17,520 --> 00:22:23,920
not your enemy, find someone who is in the middle, and send loving kindness to them, this

217
00:22:23,920 --> 00:22:29,400
is more difficult, let's say this is at this takes training, you try it for a while and

218
00:22:29,400 --> 00:22:35,440
if it doesn't work then you come back and think of the person who you love, do that for

219
00:22:35,440 --> 00:22:42,440
a while and then try again, it's a means of training the mind, in degrees, and then when

220
00:22:42,440 --> 00:22:48,360
you can, when you can finally send love to this person, cultivate that and go back and forth

221
00:22:48,360 --> 00:22:53,240
between these two, sending love to the person you love, love to the person who you normally

222
00:22:53,240 --> 00:22:59,080
wouldn't even think about, or you don't really care about, and then when you can do

223
00:22:59,080 --> 00:23:06,280
that equally, then start sending love to someone you don't like, and this is where loving

224
00:23:06,280 --> 00:23:10,160
kindness can actually be useful too, where you use it for the people you don't like,

225
00:23:10,160 --> 00:23:14,480
but you can't start there because if you're not trained in loving kindness, you'll just

226
00:23:14,480 --> 00:23:19,280
get angry when you think of them, you see when you think of a person you don't like, you

227
00:23:19,280 --> 00:23:25,840
can't help but get angry, and if that happens then it's difficult, then it will be difficult

228
00:23:25,840 --> 00:23:32,160
to cultivate, to try and get the opposite to come in, they have to be careful, when the anger

229
00:23:32,160 --> 00:23:36,320
comes up, go back to the person who you don't normally think about, and if that doesn't

230
00:23:36,320 --> 00:23:41,440
work, go back to the person you love, so then now you have three hops, start with the

231
00:23:41,440 --> 00:23:47,760
person you love, go to the person you don't care about and then go to the person who

232
00:23:47,760 --> 00:23:55,200
you don't like, until finally you can actually love the person who you normally wouldn't

233
00:23:55,200 --> 00:24:00,000
like the person who is your enemy and send love to them, and then send love to all three

234
00:24:00,000 --> 00:24:07,120
of them, and then you begin to cultivate universal love when you start to cultivate love

235
00:24:07,120 --> 00:24:14,320
for all beings and you enter into a state of love and enter into the Janus, the loving

236
00:24:14,320 --> 00:24:17,840
kindness.

237
00:24:17,840 --> 00:24:22,800
So these are the two basic ways, one is in degrees in terms of the love that you have

238
00:24:22,800 --> 00:24:31,520
for them and the others in terms of degrees of physical proximity, and then you can do

239
00:24:31,520 --> 00:24:38,800
them together, as you can start with the degrees of love and then move on to say, okay,

240
00:24:38,800 --> 00:24:50,400
once you're skilled in love, then go on to the, go on to the degrees of proximity.

241
00:24:50,400 --> 00:25:05,280
And he explains that he gives a good simile as a mother would risk her own life to protect

242
00:25:05,280 --> 00:25:10,520
her only child, even so towards all living beings one should cultivate a bound of

243
00:25:10,520 --> 00:25:11,520
heart.

244
00:25:11,520 --> 00:25:15,840
So we cultivate loving kindness just as a mother has love for their child, and this is

245
00:25:15,840 --> 00:25:29,760
the point I was trying to make about how the love that a mother has for their child

246
00:25:29,760 --> 00:25:36,520
is the love part of it, or the part of it that is love is the part that we can send

247
00:25:36,520 --> 00:25:38,320
to all beings.

248
00:25:38,320 --> 00:25:43,480
If you can't have the same feeling as you have for your children, the same feeling

249
00:25:43,480 --> 00:25:53,160
for all beings, then you know that that part is not love, that part is an attachment.

250
00:25:53,160 --> 00:25:59,600
So we have to cultivate this great love that we have for our children, we have to transfer

251
00:25:59,600 --> 00:26:04,440
it to all beings.

252
00:26:04,440 --> 00:26:11,760
And the benefit of it is it takes us out of our idea of love and it helps us see what

253
00:26:11,760 --> 00:26:19,320
is the love and what is the attachment, so that we can become impartial and become a mother

254
00:26:19,320 --> 00:26:25,640
to all beings as the Buddha was, and Buddha has a love of a mother for all beings, and

255
00:26:25,640 --> 00:26:33,520
because he had or he had even greater love than a mother for all beings.

256
00:26:33,520 --> 00:26:40,320
So that's the love part, the interesting part about this, he reminds us at the end, he

257
00:26:40,320 --> 00:26:46,120
says one should develop, if you want to cultivate the practice of it, you should cultivate

258
00:26:46,120 --> 00:26:51,280
this whenever you're, whenever you're doing, and in another place the Buddha has recommended

259
00:26:51,280 --> 00:26:55,920
this is one of the protective meditations.

260
00:26:55,920 --> 00:27:02,400
So even if you're not practicing loving kindness for the purpose of gaining the janas,

261
00:27:02,400 --> 00:27:07,800
it protects your mind as you're cultivating insight meditation, for example, it protects your

262
00:27:07,800 --> 00:27:14,080
mind from anger, because anger can stop you from progressing on the path.

263
00:27:14,080 --> 00:27:19,120
Our practice is like a small tree that has to grow, and if it's not protected it will

264
00:27:19,120 --> 00:27:23,920
fall over in the wind or it will get blown over, it's stepped trampled on by animals,

265
00:27:23,920 --> 00:27:28,440
so we have to protect it, we have to guard it, and love is one way of guarding your

266
00:27:28,440 --> 00:27:35,320
meditation, and guarding you from getting angry and getting frustrated and getting turned

267
00:27:35,320 --> 00:27:41,480
off from the meditation, when we send love we'll feel happy, we'll feel peaceful, and

268
00:27:41,480 --> 00:27:54,120
we'll therefore be able to practice meditation quite easily.

269
00:27:54,120 --> 00:28:04,800
And this is what the Buddha gets to at the end, when he says, if you practice this,

270
00:28:04,800 --> 00:28:08,920
you know, the last part is the plus part, because it has nothing to do directly with

271
00:28:08,920 --> 00:28:16,600
loving kindness, but if as well you don't get caught up in views, you see, you use loving

272
00:28:16,600 --> 00:28:22,240
kindness to purify your mind, and then you cultivate repass in it, insight meditation,

273
00:28:22,240 --> 00:28:28,280
you have see, lie, you have morality, and you have vision, which means wisdom, dasana means

274
00:28:28,280 --> 00:28:38,800
we darshana, we passana, and remove Kami, Subhini again, removing desire for central

275
00:28:38,800 --> 00:28:42,840
pleasures, this is what leads you to not be reborn again.

276
00:28:42,840 --> 00:28:46,480
So he actually changes the subject in a way, or it seems like he's now not talking

277
00:28:46,480 --> 00:28:56,280
about Mehta, but what he's saying is, using Mehta to begin to overcome views, because Mehta

278
00:28:56,280 --> 00:29:00,760
is something that calms the mind and enters and takes you into a state of peace.

279
00:29:00,760 --> 00:29:06,080
So once you develop loving kindness, remember to use it, gives this as the last teaching

280
00:29:06,080 --> 00:29:12,920
to these monks, when he sends them off, so cultivate loving kindness for the purpose of

281
00:29:12,920 --> 00:29:19,480
helping you to overcome wrong views of attachment, so some views are actually based on anger,

282
00:29:19,480 --> 00:29:29,640
as a very good example, when we have these ideas that we don't deserve to be treated,

283
00:29:29,640 --> 00:29:37,320
the way we're treated by other people, or we should, this person deserves to be punished

284
00:29:37,320 --> 00:29:39,000
or so on.

285
00:29:39,000 --> 00:29:43,800
So when we have these wrong views are directly cut off to the practice of loving kindness,

286
00:29:43,800 --> 00:29:51,160
but because of the calm and the peace that comes from it, all views can be overcome with

287
00:29:51,160 --> 00:29:52,160
it as a base.

288
00:29:52,160 --> 00:29:57,840
Once you then turn your concentration, turn your focus towards reality, you're able

289
00:29:57,840 --> 00:30:01,360
to see things as they are and give up your erroneous views.

290
00:30:01,360 --> 00:30:09,320
And if you have morality and vision, then you're able to overcome central desire, and

291
00:30:09,320 --> 00:30:16,280
that's another important one, because the close enemy for love is lust, you see, it's

292
00:30:16,280 --> 00:30:20,760
the near enemy, it's called in the museum angle, because it's very easy to slip into

293
00:30:20,760 --> 00:30:24,440
the wrong one.

294
00:30:24,440 --> 00:30:30,120
So this is an important point to remember that if you're cultivating love, be careful not

295
00:30:30,120 --> 00:30:33,680
to fall into desire for central pleasure.

296
00:30:33,680 --> 00:30:39,200
If you can overcome all of these, you'll never come to birth in the womb again.

297
00:30:39,200 --> 00:30:40,200
That's the current you made to sit.

298
00:30:40,200 --> 00:30:46,080
Now, I thought I'd talk about this because we chanted it, but I thought it would be a good

299
00:30:46,080 --> 00:30:50,120
thing to talk about so that we could incorporate it into our practice.

300
00:30:50,120 --> 00:30:57,280
The way I would suggest it is at the end, when the bell goes, then take a moment before

301
00:30:57,280 --> 00:31:02,960
you open your eyes, it will give a few moments to actually practice loving kindness.

302
00:31:02,960 --> 00:31:08,360
Why we do that is because through the practice of insight meditation, your ability to send

303
00:31:08,360 --> 00:31:14,840
love becomes stronger, and that loving kindness then in turn protects the benefits that

304
00:31:14,840 --> 00:31:23,840
you've gained from the practice, it solidifies your concentration, the two work well together.

305
00:31:23,840 --> 00:31:29,240
So we'll do that at the end of our practice, we'll just take a few minutes to send loving

306
00:31:29,240 --> 00:31:30,240
kindness.

307
00:31:30,240 --> 00:31:34,960
As you like, as you've learned how to send it, or if you want to try one of the ways

308
00:31:34,960 --> 00:31:48,680
that I mentioned, it's I'm not going to get technical or too structured about it, we'll

309
00:31:48,680 --> 00:31:57,120
just do an experiment, experiment is try to develop loving kindness, okay, so we'll start

310
00:31:57,120 --> 00:32:00,560
meditating and then at the end we'll do that.

311
00:32:00,560 --> 00:32:05,720
Okay.

