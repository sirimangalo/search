1
00:00:00,000 --> 00:00:06,160
Hello, welcome back to a series of videos on the Dhamapada.

2
00:00:06,160 --> 00:00:11,120
Today I will continue with verses 3 and 4.

3
00:00:11,120 --> 00:00:17,400
But first I'd like to make a correction to the first video about the first verse, wherein

4
00:00:17,400 --> 00:00:28,200
I said that the reason the Buddha gave this teaching was because of this monks killing

5
00:00:28,200 --> 00:00:33,240
all these insects unknowingly, and then the monks were saying that you'd done a bad

6
00:00:33,240 --> 00:00:38,200
deed and the Buddha said that he indeed didn't, and he explained that suffering only

7
00:00:38,200 --> 00:00:41,480
follows from bad intentions.

8
00:00:41,480 --> 00:00:49,120
But actually, going over it again, I realized that actually, specifically the Buddha gave

9
00:00:49,120 --> 00:00:54,120
the teaching in regards to the reason why this monk went blind in the first place, which

10
00:00:54,120 --> 00:01:01,960
was because in the past life he had out of revenge for a woman, he had treated, he was

11
00:01:01,960 --> 00:01:07,880
a doctor who had given this treatment to a woman, and out of revenge for her trying to

12
00:01:07,880 --> 00:01:08,880
cheat him.

13
00:01:08,880 --> 00:01:14,440
He gave her something that would make her blind, and as a result of that bad deed, it came

14
00:01:14,440 --> 00:01:15,440
back to him.

15
00:01:15,440 --> 00:01:18,320
It was stuck in his mind, because he must have felt very guilty as a doctor.

16
00:01:18,320 --> 00:01:23,840
He was a good doctor, and feeling guilty about it, and it's something that he naturally

17
00:01:23,840 --> 00:01:28,480
would feel guilty about, and so it eventually caught up to him, and he realized what

18
00:01:28,480 --> 00:01:39,320
a horrible thing he had done, and it conditioned his life for him to have to go blind.

19
00:01:39,320 --> 00:01:45,680
So that's the origin that after that the Buddha said, you see, you can never escape your

20
00:01:45,680 --> 00:01:46,680
bad deeds.

21
00:01:46,680 --> 00:01:53,120
But it doesn't go both ways, I mean obviously there is reference to the fact that this

22
00:01:53,120 --> 00:02:02,160
Chakupala wasn't guilty for the killing that he had done, or for the death of the insects,

23
00:02:02,160 --> 00:02:08,320
but he was on the other hand guilty for something that he intended to do to hurt this woman,

24
00:02:08,320 --> 00:02:11,840
and so he taught the results of it.

25
00:02:11,840 --> 00:02:15,760
But I'm not going into these stories so much, I just wanted to correct something that

26
00:02:15,760 --> 00:02:21,760
I had said, I'm going to try to stick to a very simple version of the stories, and place

27
00:02:21,760 --> 00:02:25,160
more emphasis on the meaning of the verses, and how they apply to our lives, because

28
00:02:25,160 --> 00:02:28,360
I think that is a more important.

29
00:02:28,360 --> 00:02:32,520
So in this next story it's the same, it's a very simple story, but it's extended, and

30
00:02:32,520 --> 00:02:36,160
I'm going to avoid the extension.

31
00:02:36,160 --> 00:02:55,400
So verses three and four, number three goes,

32
00:02:55,400 --> 00:03:24,320
which means, this is a quote, he or she or this person scolded me, they hurt me, they beat

33
00:03:24,320 --> 00:03:36,280
me, they defeated me, they destroyed me, a person who clings or grasps on, or is bound,

34
00:03:36,280 --> 00:03:43,880
binds themselves to these thoughts, wait on days on the somebody, for them their quarrels

35
00:03:43,880 --> 00:03:53,080
never ceased, or never appeased, or never tranquilized, never finished, but a quote

36
00:03:53,080 --> 00:04:03,920
she hung up with me, he scolded me, he beat me, he hurt me, he defeated me, for a person

37
00:04:03,920 --> 00:04:11,960
who doesn't cling to these thoughts, wait on days on the somebody, their quarrels are

38
00:04:11,960 --> 00:04:17,840
appeased, and the quarrels are tranquilized, they're finished, the problems they have with

39
00:04:17,840 --> 00:04:21,760
people are solved, they're worked out.

40
00:04:21,760 --> 00:04:28,760
These two verses were given in regards to a monk named Tisa, Tisa who was very fat, he

41
00:04:28,760 --> 00:04:37,120
was quite overweight, he had quite a lot of weight, and he became a monk late in life,

42
00:04:37,120 --> 00:04:44,040
and rather than practicing meditation, he took to a very indulgent monk's life, he was

43
00:04:44,040 --> 00:04:51,320
a relative of the Buddha, so he was quite spoiled and being a part of the royalty, and

44
00:04:51,320 --> 00:04:58,080
also because of his conceit, because he was related to the Buddha, he didn't bother to exert

45
00:04:58,080 --> 00:05:04,680
himself in the practice of meditation, he just would sit around and make a general nuisance

46
00:05:04,680 --> 00:05:15,840
of himself, which in fact you do find in monasteries even today believe it or not, and

47
00:05:15,840 --> 00:05:22,280
so there was one occasion where he was sitting in the reception cell, I guess, in the

48
00:05:22,280 --> 00:05:26,160
middle of the monastery, maybe it wasn't a cell, but in the very middle where he would

49
00:05:26,160 --> 00:05:31,000
expect visitors to come, and so these visiting monks came who were actually quite senior,

50
00:05:31,000 --> 00:05:38,440
but were younger than him, because he had ordained when he was old, and they thought, well

51
00:05:38,440 --> 00:05:43,880
maybe he's an old monk, so they came up to him, they saw that he was just sitting there

52
00:05:43,880 --> 00:05:48,720
looking proud, and so they came and paid respect to him, and asked politely who he was,

53
00:05:48,720 --> 00:05:55,880
and so on, and how many years he had been a monk, and he told him, years, he said,

54
00:05:55,880 --> 00:06:00,880
years I've just ordained, I don't have any years, it's a monk, and they said, well,

55
00:06:00,880 --> 00:06:07,320
then how can you act like this, how can you just sit there, when visiting monks come,

56
00:06:07,320 --> 00:06:12,160
it's very important, there's a protocol, if the receiving monks, someone who sees a monk

57
00:06:12,160 --> 00:06:17,400
coming, is junior to the approaching monk, and they have a responsibility to take care

58
00:06:17,400 --> 00:06:22,240
of them, to take their ball, to bring them water, to show them to a room, and so on, and

59
00:06:22,240 --> 00:06:28,960
to make sure that they're well taken care of, and to pay respect to them, and they said,

60
00:06:28,960 --> 00:06:33,680
how can we not following this protocol, you're not behaving like a proper monk, and

61
00:06:33,680 --> 00:06:37,800
they said, that's not appropriate, and he said, who are you, you think, who do you think

62
00:06:37,800 --> 00:06:45,240
you are, he said, who, who, he snapped his fingers, this kind of rude gesture of the times

63
00:06:45,240 --> 00:06:49,960
in India, and they said, oh, we've come to see the Buddha, and he said, huh, and you think

64
00:06:49,960 --> 00:06:55,880
you can, you can, you can say, nasty thing, he was very upset actually, he was very spoiled,

65
00:06:55,880 --> 00:07:00,840
and so he said, who do you think you are, you know who I am, and the relatives of the

66
00:07:00,840 --> 00:07:06,320
Buddha, and his cousin, something like that, and he was very upset, and so he went to see

67
00:07:06,320 --> 00:07:11,440
the Buddha, and he started complaining, he started crying to the Buddha, and he said, these

68
00:07:11,440 --> 00:07:18,280
men have scolded me, and they've abused me, and the Buddha said, well, what did you do?

69
00:07:18,280 --> 00:07:22,360
He said, wow, it was just sitting there, and wow, and did you receive them when they came,

70
00:07:22,360 --> 00:07:29,080
no, did you take their balls, no, did you show them, and so on and so on, no, no, no,

71
00:07:29,080 --> 00:07:33,520
Buddha said, well, then you're, you yourself are to blame, you're the one who was acting

72
00:07:33,520 --> 00:07:40,200
inappropriate, and the Buddha said, you should apologize to them, and he said, I apologize,

73
00:07:40,200 --> 00:07:46,160
what do you mean, they abused me, I would not apologize, so he's very obstinate, so

74
00:07:46,160 --> 00:07:51,080
the Buddha told the story of the past life, and this monk, the monks remarked how obstinate

75
00:07:51,080 --> 00:07:55,800
he was, and the obstinate he was, the Buddha said, oh, he's been like this for a long time,

76
00:07:55,800 --> 00:08:01,040
it's kind of a habit of use that extent, an extended habit, and so he told the story

77
00:08:01,040 --> 00:08:12,240
of the past life, and he was, they were these two ascetics in a room together, and one

78
00:08:12,240 --> 00:08:15,240
of them was sleeping in front of the door, and one of them was sleeping inside, because

79
00:08:15,240 --> 00:08:20,920
it was a small room, and the one inside noticed that this monk had put his head down

80
00:08:20,920 --> 00:08:26,800
here, but in the middle of the night this monk got up and put his head the other way, turned

81
00:08:26,800 --> 00:08:31,680
around and sleep the other way, and the monk inside had to go outside to the washroom, so

82
00:08:31,680 --> 00:08:37,160
he went around where he thought the monks' feet were, and ended up stepping on his hair,

83
00:08:37,160 --> 00:08:44,240
and so the monk was very angry, and the monk said, oh, I'm sorry, or the ascetic,

84
00:08:44,240 --> 00:08:51,280
the ascetic said, oh, I'm sorry, I didn't mean to, and he went outside, and on the way,

85
00:08:51,280 --> 00:08:55,760
when he was out, the monk said, I'm not going to let him do that again, and so he flipped

86
00:08:55,760 --> 00:09:00,640
over and put his head the other way, again, back to where it originally was, and the other

87
00:09:00,640 --> 00:09:04,720
monk came back and thought to himself, okay, I'll avoid stepping on his head, so he ran

88
00:09:04,720 --> 00:09:07,720
around to the other side, and ended up stepping on the guy's neck.

89
00:09:07,720 --> 00:09:13,520
Ah, there was a big fight that ensued, and he said, well, look, you had your head there,

90
00:09:13,520 --> 00:09:16,720
what are you doing, changing all the time, how can I know where your head is, it's dark,

91
00:09:16,720 --> 00:09:23,040
I don't know if it's clearly enough flashlight, but the monk wasn't the ascetic wasn't

92
00:09:23,040 --> 00:09:28,760
to be appeased, and he was very angry, and he held on to this, and he made a big fuss

93
00:09:28,760 --> 00:09:33,200
over it, and he actually cursed the other ascetic, and then they were cursing back and

94
00:09:33,200 --> 00:09:38,640
forth, and so on and so on, it's a long story, I don't want to get into it, but the point

95
00:09:38,640 --> 00:09:44,640
being that people do hold on to these things, and this is important because this is what

96
00:09:44,640 --> 00:09:52,200
the Buddha emphasized, he didn't say, don't have these thoughts, he doesn't say for a person

97
00:09:52,200 --> 00:09:59,200
who has these thoughts, because these thoughts are wrong, it's wrong for us to think like

98
00:09:59,200 --> 00:10:00,200
this.

99
00:10:00,200 --> 00:10:01,200
Why is it wrong?

100
00:10:01,200 --> 00:10:06,400
It's unpleasant, it creates suffering for us, and it builds up a bad habit, but what

101
00:10:06,400 --> 00:10:09,480
the Buddha is saying here is actually a little more specific, he's saying, don't cling

102
00:10:09,480 --> 00:10:15,240
to them, he says, for a person who is bound to them, like the word is like the same

103
00:10:15,240 --> 00:10:23,680
as word we use for the U.S. and poly for a sandal, and you bind together a sandal, the

104
00:10:23,680 --> 00:10:29,400
bound, the rope of the sandal, and so on, so when you're tied to these things, when

105
00:10:29,400 --> 00:10:35,920
you're caught up in these thoughts, when you cling to them, basically, this is what

106
00:10:35,920 --> 00:10:42,000
where your problems never cease, because the point here that I think is important is that

107
00:10:42,000 --> 00:10:47,360
we do have problems, we do have disagreements, and I've got an angry at people, we get angry

108
00:10:47,360 --> 00:10:52,680
at people, we have people do things we don't like, and we get angry at them, we get upset

109
00:10:52,680 --> 00:10:53,680
at each other.

110
00:10:53,680 --> 00:11:00,200
A good example is with our families, when our parents tell us to do something or our parents

111
00:11:00,200 --> 00:11:04,280
criticize us, we can become very angry, even to the point where we yell at them and where

112
00:11:04,280 --> 00:11:10,560
we say nasty things and really hurt our parents, it can cause hurt for them or suffering

113
00:11:10,560 --> 00:11:16,400
for them, and we suffer ourselves and so on, but what we won't do is cling, generally,

114
00:11:16,400 --> 00:11:20,480
speaking in an ordinary family situation, we won't cling to it, so the next day or the

115
00:11:20,480 --> 00:11:25,920
next moment we'll forget about it, because we don't have this clinging to them, but

116
00:11:25,920 --> 00:11:31,280
we aren't this way in all cases, and there is the case where, as with this month, this

117
00:11:31,280 --> 00:11:38,000
up, he clung to it, and he couldn't get over his own anger, his own upset at being criticized,

118
00:11:38,000 --> 00:11:42,560
it's very difficult to be criticized, it's very difficult for young monks, new monks to

119
00:11:42,560 --> 00:11:47,360
be criticized, because they can be very proud of the fact that now they're in a special

120
00:11:47,360 --> 00:11:52,200
state and now they have some kind of special lifestyle and people respect them and look

121
00:11:52,200 --> 00:11:57,480
up to them and give them free food and shelter and so on, and so they can be very proud

122
00:11:57,480 --> 00:12:03,320
of that instead of putting it to good use and really making themselves into something

123
00:12:03,320 --> 00:12:10,680
worthy of it, and so this is where the problem arises with the conceitum, with the clinging

124
00:12:10,680 --> 00:12:17,400
to it, where you become self-righteous and you say, I don't deserve that and so on, you

125
00:12:17,400 --> 00:12:22,800
don't even, there's not even the rational thought there where you think, are they right,

126
00:12:22,800 --> 00:12:26,440
are they wrong and if they're wrong then you can say, well, I don't deserve that

127
00:12:26,440 --> 00:12:33,320
and you can reply to them, for instance, this Tissa, he had this complaint to the Buddha,

128
00:12:33,320 --> 00:12:37,880
and he said to the Buddha, these monks have done something nasty, while the Buddha did

129
00:12:37,880 --> 00:12:43,040
respond, it's not that the Buddha just said, oh yes, yes, letting him walk all over the

130
00:12:43,040 --> 00:12:46,240
Buddha, walk all over these monks, letting him get away with it, the Buddha was quite clear

131
00:12:46,240 --> 00:12:50,560
that he was wrong and we can do that if someone criticizes us and says, we did something

132
00:12:50,560 --> 00:12:55,200
wrong, we can say, no, I didn't do anything wrong, based on our rational thought, but

133
00:12:55,200 --> 00:13:02,840
we don't do this generally, generally we say, we don't think about, we just, out of anger,

134
00:13:02,840 --> 00:13:08,280
we refuse any criticism, we become upset and we become self-righteous and it festers in

135
00:13:08,280 --> 00:13:17,960
the mind, this is what gives rise to grudges and imity and feuds, war, when we build hate

136
00:13:17,960 --> 00:13:22,760
in our mind and it's a self-righteous, equal to mystical hate, this is how religious wars

137
00:13:22,760 --> 00:13:28,560
are fought, it's not because we're angry at each other, if you look at ideological wars

138
00:13:28,560 --> 00:13:36,080
in the world where this country comes to hate that country and our whole mindset is that

139
00:13:36,080 --> 00:13:42,920
this country is evil, this country is bad, many people feel this way about America, they've

140
00:13:42,920 --> 00:13:49,760
cultivated this evil and that's what happened in all this terrorism that's been a fear

141
00:13:49,760 --> 00:13:54,240
of the American people, I mean to some extent it's justified because there's a lot of people

142
00:13:54,240 --> 00:14:02,320
out there who don't like, we just have this anger towards this country as a whole, as

143
00:14:02,320 --> 00:14:07,080
an example and you have this throughout the world, you know, our prejudice against people

144
00:14:07,080 --> 00:14:13,960
and so on, but we build this up, we build this up quite often and this is what we have

145
00:14:13,960 --> 00:14:19,000
to be careful about because we will have fights, we will have disagreements and we will

146
00:14:19,000 --> 00:14:23,760
get angry at each other until we get to the point where we are able to see that anger

147
00:14:23,760 --> 00:14:31,560
is unpleasant and unuseful and able to get rid of it entirely, but what is dangerous

148
00:14:31,560 --> 00:14:37,640
and what is a hindrance to our path towards giving up the anger is this attachment, this

149
00:14:37,640 --> 00:14:44,440
clinging to it, it covers up the whole nature of the anger, it prevents you from actually

150
00:14:44,440 --> 00:14:49,640
looking at the anger from what it is, when you're angry and you say, this is right

151
00:14:49,640 --> 00:14:55,800
that I'm angry, I'm angry, these people deserve my wrath and so on, then you're justifying

152
00:14:55,800 --> 00:15:03,480
it and you're covering it up, it's like you have this great, this great, that, suppose

153
00:15:03,480 --> 00:15:11,000
you have this ball of hot iron or you have this hot coal and you have a holding in your

154
00:15:11,000 --> 00:15:16,600
hand and you say, and it's hurting you and so you grab it and you say, what is it

155
00:15:16,600 --> 00:15:21,080
that's hurting me, what is it that's hurting me, what are you saying, you're looking for

156
00:15:21,080 --> 00:15:28,680
the cause outside of yourself, when actually it's right here, when this person who's causing

157
00:15:28,680 --> 00:15:32,360
yourself, causing you to get angry and then you say, you're the cause, you're the reason

158
00:15:32,360 --> 00:15:37,880
why I'm suffering and you're not looking at what you're holding the anger, you're holding

159
00:15:37,880 --> 00:15:41,800
on to these thoughts, it's actually these thoughts, it's making us suffer, we have to be

160
00:15:41,800 --> 00:15:50,240
very careful because the anger itself is not so dangerous, it's unpleasant but it's, it's

161
00:15:50,240 --> 00:15:57,400
not the real problem, the danger in the anger is with the ego that comes along with it

162
00:15:57,400 --> 00:16:04,520
that if we have ego and we attach our ego to the anger and it becomes self righteous

163
00:16:04,520 --> 00:16:12,320
and every time we think of a person, we are upset by them, we're angry, we have hate

164
00:16:12,320 --> 00:16:19,920
towards people, we have ungrudged towards people, we're biased against people and so on.

165
00:16:19,920 --> 00:16:25,000
This is what, every time we see them, everything they do will be something we can criticize,

166
00:16:25,000 --> 00:16:29,000
we'll always be looking for, they did that wrong and so on and every time they do things

167
00:16:29,000 --> 00:16:34,080
good, we'll feel upset when good things happen to the movie upset when they act in a good

168
00:16:34,080 --> 00:16:40,240
way, we'll try to minimize it and so on and we'll create great suffering for ourselves.

169
00:16:40,240 --> 00:16:45,640
This is a very important lesson and it's in general a very important lesson with the

170
00:16:45,640 --> 00:16:51,400
defilements of the mind but they're really not that bad, you'll agree to anger, let's

171
00:16:51,400 --> 00:16:55,520
agree to anger, they're not that big of a deal, you can watch them and you can look

172
00:16:55,520 --> 00:17:00,440
at them but the problem is we don't, probably we never learn about these things, people

173
00:17:00,440 --> 00:17:04,720
think pleasure is such a great thing, you know, attaching the things is so great when you

174
00:17:04,720 --> 00:17:09,560
have sensual pleasure and sexual pleasure and so on, we think it's so great because

175
00:17:09,560 --> 00:17:16,360
we cling to it because there's the idea that I like this, I want this and so on but if

176
00:17:16,360 --> 00:17:19,880
we actually just looked at the pleasure when it came up instead of saying okay I have

177
00:17:19,880 --> 00:17:26,160
to get this, if we just looked at the wanting of it, if we just examined it for what it

178
00:17:26,160 --> 00:17:31,280
is, we see actually it's quite a bit of stress actually, it's this tension and the only

179
00:17:31,280 --> 00:17:35,880
way you can relieve that tension is either by seeing it and letting it go or by chasing

180
00:17:35,880 --> 00:17:41,560
after and getting what you want and then saying ah no I don't need it anymore so the happiness

181
00:17:41,560 --> 00:17:46,920
doesn't come from the wanting, it comes from giving up the wanting either in one of

182
00:17:46,920 --> 00:17:53,800
two ways, the problem of course is following and is that you're encouraging it and you're

183
00:17:53,800 --> 00:18:01,560
developing it more and more, you know, you're clinging more and more and there's no, there's

184
00:18:01,560 --> 00:18:06,800
no understanding of the problem, another thing goes with anger when you when you're angry

185
00:18:06,800 --> 00:18:13,880
at someone, it's suffering, yes but it's actually not such a big deal until you until you

186
00:18:13,880 --> 00:18:21,880
do what this is, when you come to identify with the anger and identify a source of the anger

187
00:18:21,880 --> 00:18:25,440
in another person, that they are the cause of the problem, that the problem is not

188
00:18:25,440 --> 00:18:29,240
big, the anger, the problem is the other person.

189
00:18:29,240 --> 00:18:33,280
So the real problem is our delusion and our ignorance, this is why the Buddha said alizya

190
00:18:33,280 --> 00:18:39,480
patsayya sankara, all sankara, all formations in the mind, all judgments, your sankara

191
00:18:39,480 --> 00:18:47,320
hear means, our judgments, our partialities, our projections that we place on things, these

192
00:18:47,320 --> 00:18:53,440
are all caused by ignorance, it's ignorance that is the root and so that's really what this

193
00:18:53,440 --> 00:19:03,400
verse is talking about, it's the delusion, the identification with the thoughts, that is

194
00:19:03,400 --> 00:19:11,480
the problem, and our defilements are in different levels, there is the misperception of

195
00:19:11,480 --> 00:19:19,960
something as pleasant or unpleasant, which comes to us all until you get to fully enlighten,

196
00:19:19,960 --> 00:19:24,240
you still will have this perception of something as being pleasant or unpleasant, something

197
00:19:24,240 --> 00:19:30,240
that's nice, something of the mess not nice, this is just a feeling that you get, something

198
00:19:30,240 --> 00:19:34,560
is, oh this is experience, it's pleasant, this is experience, it's unpleasant.

199
00:19:34,560 --> 00:19:38,720
The next level is our thoughts, and this is what he's talking about here, this thought,

200
00:19:38,720 --> 00:19:44,800
oh this is I'm thinking, no that's unpleasant thinking, this is pleasant, so person says

201
00:19:44,800 --> 00:19:49,480
something to you think, oh he's hurting me, oh he's abusing me, this person's saying

202
00:19:49,480 --> 00:19:54,640
that, see anything, oh that person's saying that, you think this is at the thought level,

203
00:19:54,640 --> 00:19:58,920
but the worst one is the views, when you have to view about it, that is good, this is

204
00:19:58,920 --> 00:20:08,640
bad, pleasure is good, pleasure is pain is bad and so on, and so this is when you

205
00:20:08,640 --> 00:20:18,120
identify, when you build up this eye and us and them, you know me and you hurt me and

206
00:20:18,120 --> 00:20:22,760
so on, when we build this up this is what really, really causes problems, because as I said

207
00:20:22,760 --> 00:20:27,560
it obscures the nature of the experience, the only way we can come to be free is if we

208
00:20:27,560 --> 00:20:32,840
are able to see the nature of the anger, the nature of the grieving, be able to work out

209
00:20:32,840 --> 00:20:36,640
for ourselves, ourselves what is truly right for us, the only way we can work out what

210
00:20:36,640 --> 00:20:42,000
is truly right is by seeing the individual experience, and we can't do this if we're

211
00:20:42,000 --> 00:20:51,040
clinging, we can't do this if we are, if we have some idea or identification with the

212
00:20:51,040 --> 00:20:59,080
object as me and mine and so on, okay so I think I've done over that enough, and this

213
00:20:59,080 --> 00:21:05,120
is a fairly important teaching, it helps us to, I think this will help us in our practice

214
00:21:05,120 --> 00:21:11,800
because it also helps us to accept, and to allow to come up the bad things in our mind,

215
00:21:11,800 --> 00:21:15,720
greed and anger are bad, but the only way we're going to come to see that they're really

216
00:21:15,720 --> 00:21:22,200
and truly bad is if we let them come up, we can't just say it's bad, it's good, until

217
00:21:22,200 --> 00:21:26,800
we have wisdom and we're able to see clearly, what is it, is it good, is it bad, not from

218
00:21:26,800 --> 00:21:32,520
here thinking us on and not because I say it because the book says it isn't by seeing

219
00:21:32,520 --> 00:21:37,160
it for yourself, so you don't have this view that, well I think anger is good, well I think

220
00:21:37,160 --> 00:21:42,560
greed is good, and then, or you don't have the idea that, oh, you're the devil said it's

221
00:21:42,560 --> 00:21:47,400
bad, you're the devil said it's good and so on, you know for yourself, and only when

222
00:21:47,400 --> 00:21:50,720
you know for yourself, are you going to be able to see, you're going to be able to let

223
00:21:50,720 --> 00:21:55,440
go of it and overcome it, and you will never be able to do this again, again and again

224
00:21:55,440 --> 00:22:00,400
now, never be able to do this if you're clinging to it, if you're holding on to it,

225
00:22:00,400 --> 00:22:07,040
and therefore weigh it on days on the summit, the quarrels, the problems that that person

226
00:22:07,040 --> 00:22:11,720
encounters will never cease, so thanks for tuning in, this has been another verse, and

227
00:22:11,720 --> 00:22:34,840
all the best.

