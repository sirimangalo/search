1
00:00:00,000 --> 00:00:04,000
Should one be noting, well, not informal meditation?

2
00:00:04,000 --> 00:00:08,000
I often find confusion arising when I am in a deep concentration.

3
00:00:08,000 --> 00:00:11,000
I will hear someone speak and then I note hearing.

4
00:00:11,000 --> 00:00:13,000
Then usually the next thing to arise is the breath.

5
00:00:13,000 --> 00:00:17,000
Then usually I thought like this person wants that question answered.

6
00:00:17,000 --> 00:00:20,000
But it stops there and I usually come back to the breath.

7
00:00:20,000 --> 00:00:23,000
And answer that one first.

8
00:00:23,000 --> 00:00:29,000
It depends on the situation.

9
00:00:29,000 --> 00:00:35,000
Of course, if possible, you should always note everything that is going on around you.

10
00:00:35,000 --> 00:00:37,000
You should be mindful.

11
00:00:37,000 --> 00:00:42,000
But you can learn to be a little bit selective on what you are mindful.

12
00:00:42,000 --> 00:00:51,000
And you can try to have your mind open on, for example,

13
00:00:51,000 --> 00:00:59,000
other people so that you are able to answer these questions.

14
00:00:59,000 --> 00:01:04,000
That means you are not only, when you are not meditating,

15
00:01:04,000 --> 00:01:09,000
you are not only mindful to the internal,

16
00:01:09,000 --> 00:01:15,000
but you are mindful on the external as well.

17
00:01:15,000 --> 00:01:20,000
You note that the person is approaching and you hear the question

18
00:01:20,000 --> 00:01:24,000
and then just don't go back to the breath.

19
00:01:24,000 --> 00:01:33,000
You can direct your mind and have the answer prepared for it

20
00:01:33,000 --> 00:01:39,000
and then feel your, how is this called?

21
00:01:39,000 --> 00:01:44,000
When you speak or you can hear, you can feel your lips

22
00:01:44,000 --> 00:01:57,000
or you can try to be mindful on the thoughts that arise in order to give a good answer.

23
00:01:57,000 --> 00:02:01,000
There are many different things you can be mindful on.

24
00:02:01,000 --> 00:02:09,000
So you don't have to go back to the breath which would be going internally.

25
00:02:09,000 --> 00:02:18,000
So as in the Satyapatana, it's set that you can be mindful internally and externally.

26
00:02:18,000 --> 00:02:29,000
And when you are not in form of meditation, it's quite good to be mindful externally as well.

27
00:02:29,000 --> 00:02:34,000
Okay, then I'll answer part two because I don't have anything to add.

28
00:02:34,000 --> 00:02:41,000
I feel as if my thoughts have shut off and I need to make a conscious decision to start thinking,

29
00:02:41,000 --> 00:02:45,000
is this proper practice or am I doing something wrong?

30
00:02:45,000 --> 00:02:51,000
It's like I have to stop my concentration and force a thought even if it doesn't arise.

31
00:02:51,000 --> 00:02:53,000
That's actually connected to the first one.

32
00:02:53,000 --> 00:02:55,000
You just wrote it in two parts.

33
00:02:55,000 --> 00:03:07,000
Okay, well, then I'll try to add something in there.

34
00:03:07,000 --> 00:03:20,000
Well, it is a problem with social interactions when you lose your connection with people's questions.

35
00:03:20,000 --> 00:03:25,000
But it's only a problem because of our expectations in society.

36
00:03:25,000 --> 00:03:31,000
I mean, this is why meditators become very weary of society because they either have to choose.

37
00:03:31,000 --> 00:03:35,000
They're either going to be mindful or they're going to interact with people.

38
00:03:35,000 --> 00:03:40,000
When you're really mindful, most people don't want to be around you.

39
00:03:40,000 --> 00:03:48,000
Most people, many people who are in the worldly things don't want to be around you when you answer from the heart,

40
00:03:48,000 --> 00:03:57,000
from a meditative point of view, then you wind up sitting in a hut in a jungle in a very short time.

41
00:03:57,000 --> 00:04:08,000
Well, I'm very short, no, in not so long of a time because your caught up in a whirlwind in society.

42
00:04:08,000 --> 00:04:15,000
In the world, you caught up in so much stress.

43
00:04:15,000 --> 00:04:20,000
People think about how people live their lives ordinarily, from day to day.

44
00:04:20,000 --> 00:04:26,000
It's with so much stress and hustle and bustle that is meaningless, that is worse than meaningless,

45
00:04:26,000 --> 00:04:40,000
that is causing them more stress and is feeding this stress and defilement, some greed and anger and so on.

46
00:04:40,000 --> 00:04:47,000
So, you're always going to have that conflict in a sense and you have to, when living in the world,

47
00:04:47,000 --> 00:04:56,000
you have to make a compromise as Paulini said and I think she said that you're not going to be mindful of everything.

48
00:04:56,000 --> 00:05:01,000
You're going to have to answer people and put up with it.

49
00:05:01,000 --> 00:05:04,000
But you can be mindful by doing so.

50
00:05:04,000 --> 00:05:14,000
And you can be mindful of other things, at least at the very least, I will always have people explain people to be mindful of your emotions.

51
00:05:14,000 --> 00:05:20,000
So, if you can't be mindful of talking or feeling your lips move when you talk or listening,

52
00:05:20,000 --> 00:05:24,000
even just hearing when the other person is talking or even thinking, thinking.

53
00:05:24,000 --> 00:05:29,000
If all of that is too quick and too difficult and actually gets in the way of,

54
00:05:29,000 --> 00:05:33,000
or seems to get in the way of the conversation, it doesn't have to.

55
00:05:33,000 --> 00:05:39,000
If you're really good at it, you can actually do that and still be involved in the conversation to an extent.

56
00:05:39,000 --> 00:05:43,000
It makes you look a little weird, seem a little weird when you're pausing and so on.

57
00:05:43,000 --> 00:05:50,000
But at the very least, when you get angry at someone or when you're stressed or when you like something that someone says,

58
00:05:50,000 --> 00:05:56,000
when you're happy or so on, be mindful of all of that and your worries and your fears and come to see that,

59
00:05:56,000 --> 00:06:05,000
you know, all of these many things that we normally miss in an ordinary conversation, that in every ordinary conversation there's a lot of excitement.

60
00:06:05,000 --> 00:06:14,000
There's our anger and frustration and annoyance, there's the worries and the fears and the self image of how do I look to this person?

61
00:06:14,000 --> 00:06:23,000
Did I say something funny or did I say something clever or did I say something stupid or did I make myself look bad or so on?

62
00:06:23,000 --> 00:06:28,000
Does this person, what do they think of me and so on?

63
00:06:28,000 --> 00:06:38,000
All of this we can mitigate, we can lessen so that we're more able to be there and to be present.

64
00:06:38,000 --> 00:06:44,000
The best thing I think is to enter into every situation mindfully with the intention to be mindful.

65
00:06:44,000 --> 00:06:49,000
Whether you're mindful of it or not, we always do these reflections.

66
00:06:49,000 --> 00:06:54,000
So before we eat, we reflect on the food, before we put our robes on, we reflect on the robes.

67
00:06:54,000 --> 00:07:01,000
Before we do things, we try to reflect and say, now I'm going on the arms round, I'm going to try to be mindful while I'm going on the arms round.

68
00:07:01,000 --> 00:07:05,000
Set this, establish this intention in your mind.

69
00:07:05,000 --> 00:07:09,000
Now this person I'm going to have to talk with this person, I'm going to be mindful.

70
00:07:09,000 --> 00:07:14,000
Now I'm on monk radio and I have to give a talk, I'm going to try to be mindful as I give the talk.

71
00:07:14,000 --> 00:07:20,000
That's something that I think is essential in all situations.

72
00:07:20,000 --> 00:07:24,000
It will change every situation just to go into it with the intention to be mindful.

73
00:07:24,000 --> 00:07:42,000
Start off with a mindful intention and then you will find yourself coming back to it and focusing on your emotions more and clearing up your mind so that you're able to deal with the situation

74
00:07:42,000 --> 00:07:49,000
in a wise and wholesome manner.

75
00:07:49,000 --> 00:08:13,000
Okay.

