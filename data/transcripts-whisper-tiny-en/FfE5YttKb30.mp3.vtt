WEBVTT

00:00.000 --> 00:06.680
How does one take the middle way in regards, for example, to food?

00:06.680 --> 00:13.040
We should eat enough to have energy for all day, all our daily duties.

00:13.040 --> 00:17.240
But when we are not sure what is enough, we should rather eat more than less because negative

00:17.240 --> 00:23.680
effects from restraining are more destructive than negative effects from indulging ourselves.

00:23.680 --> 00:33.680
Is this right?

00:33.680 --> 00:38.880
Well, I think it depends on what you are doing because obviously you are still living in

00:38.880 --> 00:39.880
the world.

00:39.880 --> 00:47.280
I think that would make it easier to judge how much you need really because if you are doing

00:47.280 --> 00:52.840
this, not like every time you eat, you have to sit down and have this whole meal, you could

00:52.840 --> 00:57.920
eat small things at various times instead.

00:57.920 --> 01:05.800
I think that that would make it a lot easier to regulate how much you take in and see how

01:05.800 --> 01:11.320
much you really need unless you are keeping aid precepts than you have to eat before

01:11.320 --> 01:20.920
noon, but indulging or overly restraining can be bad.

01:20.920 --> 01:26.080
I think it is going to be a matter of experimentation, really.

01:26.080 --> 01:35.160
You just have to realize how much energy you are using during the day you need accordingly.

01:35.160 --> 01:52.800
Yeah, I had the same thought that when you are working, you have another need for calories

01:52.800 --> 01:58.640
or for nutrients, then when you are just sitting and meditating.

01:58.640 --> 02:06.720
So as a layperson, you need more food and you need eventually a third meal.

02:06.720 --> 02:14.400
When you are a meditator here on a retreat, one meal could be enough or two meals.

02:14.400 --> 02:22.000
When you are on eight precepts and you have to work and you feel hungry, then something

02:22.000 --> 02:31.720
like a juice or so in the afternoon helps to overcome that hunger or that feeling of getting

02:31.720 --> 02:34.720
dizzy or getting nervous.

02:34.720 --> 02:43.640
Before I ordain, I thought, I need three meals, I need to eat a lot and I found out that

02:43.640 --> 02:53.640
I am fine with one meal when I am in meditation retreat and two meals when I am doing

02:53.640 --> 02:57.640
duties.

02:57.640 --> 03:04.800
It is happening in the mind as well that you think you really need it.

03:04.800 --> 03:13.960
You may have a little bit hunger in the evening when you start to get for two or one meals,

03:13.960 --> 03:27.400
but after a while you will find that this is nothing bad.

03:27.400 --> 03:28.400
For sure.

03:28.400 --> 03:36.000
I have a couple of things to say sort of corollaries, I think the questions are not even answered.

03:36.000 --> 03:44.560
First of all, to make clear that there are negative effects from indulging, that that

03:44.560 --> 03:52.200
may not be clear, food is a lot more dangerous than we make it out to be or that then

03:52.200 --> 03:56.960
we actually realize, because a lot of food will have an effect on your brain chemistry

03:56.960 --> 04:02.120
as well, a lot of rich food and fatty food and so on.

04:02.120 --> 04:11.000
Obviously you have negative effects from restraining in terms of the weakness that comes

04:11.000 --> 04:12.000
from it.

04:12.000 --> 04:18.700
Now, if you are living an ordinary life, an ordinary, if you are living a life in the human

04:18.700 --> 04:30.240
world, a human society, then you need to work, you need to think, you need to speak.

04:30.240 --> 04:37.800
So not having enough food can be dangerous in that regard, but in terms of our spirituality,

04:37.800 --> 04:45.920
it is really more, I would say, the other way that indulging is actually a lot more dangerous.

04:45.920 --> 04:52.400
It leads to lust or it supports lust to get stronger in the mind.

04:52.400 --> 04:56.160
It supports laziness to get stronger in the mind.

04:56.160 --> 05:00.920
It doesn't create these things in the mind, but because of the brain chemistry and the

05:00.920 --> 05:07.680
processes in the body and the blood flow and so on required to digest food and the chemicals

05:07.680 --> 05:13.440
that are added to the body, these feelings become stronger.

05:13.440 --> 05:17.560
This is why in the Buddhist time everyone was starving themselves because when they starved

05:17.560 --> 05:21.320
themselves or all the spiritual people, because when they starved themselves, these feelings

05:21.320 --> 05:25.360
just went away totally and they thought they were enlightened, wow, I don't get greedy

05:25.360 --> 05:34.640
at all because there is no chemicals left to have lust arise for the hormones to work.

05:34.640 --> 05:41.120
That's not the way out of suffering, but it points very much to the problem with going

05:41.120 --> 05:48.240
the other way, is that you're developing an incredible amount of these states.

05:48.240 --> 05:58.120
The other thing I wanted to say is, and it relates to the necessity to eat food in life

05:58.120 --> 06:04.760
and society, is that the question is what is the food for and this is a question that

06:04.760 --> 06:13.080
especially monks and meditators are not very clear on and often miss.

06:13.080 --> 06:21.200
So people think they will see the effects of eating less than they're used to in terms

06:21.200 --> 06:25.240
of maybe their wounds don't heal as quickly, maybe they don't have as much energy, they

06:25.240 --> 06:31.600
can see their muscles, atrophy, atrophying, their muscles getting smaller, maybe they're

06:31.600 --> 06:37.880
losing the color and the beauty in their face is getting thin and they can see the bone

06:37.880 --> 06:45.480
sticking out of their shoulders and this and then a million other things are excuses

06:45.480 --> 06:48.800
that people come up with to eat more.

06:48.800 --> 06:54.000
And this is what constantly I was getting in Los Angeles, for example, you're too thin,

06:54.000 --> 06:57.160
you have to eat more, you're not eating enough and so on.

06:57.160 --> 07:03.880
And the question is enough for what, because the Buddha's idea of eating enough is enough

07:03.880 --> 07:09.120
to live, enough to survive, if the food that you're eating is not enough for you to survive

07:09.120 --> 07:16.440
on and not enough for you to carry out your duties correctly, then it's not enough.

07:16.440 --> 07:20.280
But if the food you eat is enough for you to carry out your duties, if with the food

07:20.280 --> 07:26.600
you eat you can walk back and forth five hours a day and do five hours of walking meditation

07:26.600 --> 07:31.560
then I would say that's enough food and that's really not a lot of food because you're

07:31.560 --> 07:36.080
walking quite slowly, so you do five hours of walking, five hours of sitting or six hours

07:36.080 --> 07:40.560
of walking, six hours of sitting, that's enough to live.

07:40.560 --> 07:45.120
Anything that you have to do apart from that, if you have to teach, if you have to work

07:45.120 --> 07:53.080
in the monastery, if you have to then go out and work in daily in society, then you add

07:53.080 --> 08:00.600
on to that extent, with this food, can I live, can I survive?

08:00.600 --> 08:10.520
Because that's really the point, the monk in the monastic is the ideal or the perfect example

08:10.520 --> 08:17.280
of the bare minimum of requirements, so we eat because we need the food, we wear clothes

08:17.280 --> 08:21.600
because we need something to cover our bodies, we have shelter because we need something

08:21.600 --> 08:28.640
to protect against the rain or also to give us solitude and so on.

08:28.640 --> 08:35.160
And we take medicine so that we don't suffer from horrible, painful feelings that are

08:35.160 --> 08:42.800
going to leave us or drive us crazy even, that are going to hurt our meditation practice

08:42.800 --> 08:47.120
or disrupt our meditation practice.

08:47.120 --> 08:55.360
So point being, as Polanyani also said, we need far less than we think that we need.

08:55.360 --> 09:03.560
And when you really think about what do I need this food for, you realize that you really

09:03.560 --> 09:06.600
need a lot less than you think.

09:06.600 --> 09:13.040
And the point is that we, rather than using this as a guide, we always will use things

09:13.040 --> 09:17.480
like hunger as a guide or partiality as a guide, right?

09:17.480 --> 09:27.320
I know I only need maybe some some crackers and some crackers and cheese and that's enough

09:27.320 --> 09:35.280
but chips are better, so I'll have chips instead or I know that I've had enough food

09:35.280 --> 09:42.600
but I'm still hungry, so I'll go out and eat more and we'll eat until we're not hungry.

09:42.600 --> 09:44.920
This is our measure.

09:44.920 --> 09:50.840
I had a friend when I was younger and he studied, he became a biologist, became a veterinarian

09:50.840 --> 09:55.880
actually and he once told me that the way this gland works and the brain, the hunger

09:55.880 --> 09:58.560
gland works, it's always firing.

09:58.560 --> 10:02.200
It's nature as to fire and say, I'm hungry, I'm hungry, I'm hungry, I'm hungry, I'm

10:02.200 --> 10:03.200
hungry.

10:03.200 --> 10:07.760
That's it's ordinary state and it's only by putting a whole bunch of food to make it

10:07.760 --> 10:17.600
shut up, somehow it gets something and then it switches off and then when the food disappears,

10:17.600 --> 10:22.440
it turns back on and starts saying, I'm hungry, I'm hungry, I'm hungry again.

10:22.440 --> 10:26.120
It doesn't necessarily have anything to do with your level of health or your physical

10:26.120 --> 10:29.480
requirements or even how much food you have in your stomach.

10:29.480 --> 10:33.800
You can have enough food in your stomach to survive and yet it hasn't hit this gland yet

10:33.800 --> 10:38.840
and so it's still saying, eat more, eat more, eat more, I'm hungry.

10:38.840 --> 10:45.800
Something we don't realize, hunger is not an indication that we have to eat more.

10:45.800 --> 10:49.080
It's not a perfect indication that we have to eat more.

10:49.080 --> 10:54.080
It's also something that has evolved biologically, it's not a spiritual gland, this gland

10:54.080 --> 10:56.120
is not our spiritual friend.

10:56.120 --> 11:06.240
If anything, it's a product produced by Mara, the factories of Mara because it tends

11:06.240 --> 11:11.200
to tell us we have to eat more and get fat and fat and save up for the winter or save

11:11.200 --> 11:16.560
up for the times when we have no food, how evolution has led us to have this gland.

11:16.560 --> 11:21.680
There's nothing to do with any sort of spiritual development.

11:21.680 --> 11:28.560
So a big point in learning how to find the middle way in regards as the example you give

11:28.560 --> 11:37.360
food is to stop listening to hunger and just be rational and understand how much food

11:37.360 --> 11:38.360
you need.

11:38.360 --> 11:46.440
Okay, sure when you're starving, have some food but don't use the hunger as a guide.

11:46.440 --> 11:50.760
And this is apparent when you come to practice meditation because if I know you've eaten

11:50.760 --> 11:51.760
enough, you don't need more.

11:51.760 --> 11:55.560
And yet in the evening you're still going, I'm hungry, I'm hungry, I'm hungry, I'm hungry.

11:55.560 --> 12:01.200
And I've had meditators tell me, I was in the evening, I was so hungry, I'll complain

12:01.200 --> 12:04.800
about it the first day, so you tell them, say hungry, hungry and they're, come on, I'm

12:04.800 --> 12:09.440
hungry, I need more food and then they'll finally try it and they'll come back and tell

12:09.440 --> 12:11.440
you that it's amazing.

12:11.440 --> 12:15.000
Last night I was so hungry, I thought I need some food, I'm going to die here, I can't

12:15.000 --> 12:20.440
stay here in another day and so, but finally I said to myself, hungry, hungry.

12:20.440 --> 12:24.680
I woke up in the morning, I wasn't hungry at all and they're able to see that actually

12:24.680 --> 12:29.680
it's just their body playing games with them because they're used to eating and because

12:29.680 --> 12:38.520
they're used to eating the mind says it's time for dinner, where's my dinner and the

12:38.520 --> 12:55.120
grandstand's working.

