1
00:00:00,000 --> 00:00:10,560
Okay, so my meditation DVD, advertising, it's going to give out for free.

2
00:00:10,560 --> 00:00:16,760
This is what I send around the world, so people sign up on my website and we give them

3
00:00:16,760 --> 00:00:22,000
a two-disc set on how to meditate, totally free.

4
00:00:22,000 --> 00:00:31,720
This is advertising, goodness, not advertising, business, but anyway, that's just an added.

5
00:00:31,720 --> 00:00:34,360
Okay, so what am I reading?

6
00:00:34,360 --> 00:00:39,200
I'm reading through this, this is the middle length discourses of the Buddha, I'm not even

7
00:00:39,200 --> 00:00:44,360
sure if you can show that, anyway.

8
00:00:44,360 --> 00:00:48,800
This is the middle length discourses of the Buddha, middle length discourses, it's a collection

9
00:00:48,800 --> 00:00:55,640
of discourses taught by the Buddha, there's 152 of them, now I read them all through

10
00:00:55,640 --> 00:01:00,800
before, but now what I'm doing is I'm going over them and using the computer with this

11
00:01:00,800 --> 00:01:06,560
program that translates them and it's got them in there, it's got the polytech, so I have

12
00:01:06,560 --> 00:01:10,320
the poly language in the computer and then I have the English language here, so I'll

13
00:01:10,320 --> 00:01:17,320
be reading through, generally I'll read through the poly first and when I get stuck I'll

14
00:01:17,320 --> 00:01:21,680
move back to the English, so I'm using this as reference right now, I'm not really reading

15
00:01:21,680 --> 00:01:30,280
it, I'm reading the poly, maybe I can give you a screenshot of that, let's go through

16
00:01:30,280 --> 00:01:32,760
the books first.

17
00:01:32,760 --> 00:01:37,000
The second thing I'm reading sort of on and off, I use this one as reference as well,

18
00:01:37,000 --> 00:01:46,000
this is a Thai book, Vipasana Kama Tan, I'll read you Vipasana Kama Tan, this book is about

19
00:01:46,000 --> 00:01:59,320
insight meditation, bautini, tulat naipaputasasana, kama tulat, bala, mai kam, dala, yang,

20
00:01:59,320 --> 00:02:17,620
nang, nang, tulat, bai, yang, a kmai, kmai, kmai, kmai, kmai, kmai, kmai, kmai, kmai, kmai, kmai, kmai, ktai,

21
00:02:17,620 --> 00:02:26,980
ttoan, fazong, the meaning of this is tula which means business or work, work in

22
00:02:26,980 --> 00:02:32,000
Buddhism, so then they have the work tula which is a poly word and then he's going to explain

23
00:02:32,000 --> 00:02:37,440
what it means, so what is the work, what is the meaning of the word work, it can be translated

24
00:02:37,440 --> 00:02:45,160
in many ways, first of all it means a yoke, the word tula, it can be translated as yoke,

25
00:02:45,160 --> 00:02:50,360
but it means it's something that you're yoke to, so as a monk for instance where yoke

26
00:02:50,360 --> 00:02:54,400
to the practice of meditation, he's going to talk about what are the two tulas, the two

27
00:02:54,400 --> 00:03:07,800
works in Buddhism, one is, one is to study, here, bante, emasiming sassane, kati, tula-ni,

28
00:03:07,800 --> 00:03:14,400
in this religion, in this teaching, what are how many works are there, how many businesses,

29
00:03:14,400 --> 00:03:21,280
how many, I don't know the English word, how many things are there that you have to do

30
00:03:21,280 --> 00:03:28,560
or how many tasks, that's a good word, how many tasks are there, when the Buddha says

31
00:03:28,560 --> 00:03:44,940
kanda tula-ni, passana tula-ni, tula-ni, tula-ni, piku, he says, look here, oh, mong, there are two

32
00:03:44,940 --> 00:03:52,600
gandatura which means study, emasana tula means the task of insight, these are the two,

33
00:03:52,600 --> 00:03:57,960
there are indeed these, there are only these two tasks, so in Buddhism there are only two

34
00:03:57,960 --> 00:04:05,600
tasks, one is to study, reading the books, and the other is to practice, to understand

35
00:04:05,600 --> 00:04:12,080
reality, to understand things as they are, and this is something else that I read,

36
00:04:12,080 --> 00:04:19,160
the third thing that I'm reading right now is a book, I won't give the title, I'm not

37
00:04:19,160 --> 00:04:29,480
even sure if I can, then there's irreducible mind, this is a book about how the mind and

38
00:04:29,480 --> 00:04:37,040
the brain relate and giving a sort of analysis of the idea that the mind is only a part

39
00:04:37,040 --> 00:04:41,000
of the brain, now this is not Buddhist, but the people who wrote this, there are all

40
00:04:41,000 --> 00:04:49,280
PhDs we've got, this is the University of Virginia, this is a group of scientists who all

41
00:04:49,280 --> 00:04:55,160
PhDs are all psychologists or whatever, accredited scientists, well known in the field,

42
00:04:55,160 --> 00:05:01,200
and they've come together to study things that can't be explained by a materialist viewpoint,

43
00:05:01,200 --> 00:05:10,160
most especially near-death experiences and post-death experiences in terms of rebirth,

44
00:05:10,160 --> 00:05:16,080
so it actually has a lot to do with Buddhism, and it's useful for me in talking to people

45
00:05:16,080 --> 00:05:21,240
who are not Buddhist and people who are materialists, in being able to debate with them

46
00:05:21,240 --> 00:05:26,280
and understand some of the issues, because oftentimes people will say, well, we've already

47
00:05:26,280 --> 00:05:31,640
proven that mind is a product of the brain, and that's what they say in here, there are

48
00:05:31,640 --> 00:05:37,840
people who say that without looking at the evidence and the evidence, this is huge, the

49
00:05:37,840 --> 00:05:43,320
evidence in here, things like, there's a really good quote that I'm like, it's talking

50
00:05:43,320 --> 00:05:50,680
about near-death experiences, how people talk about when you're about to die, your brain

51
00:05:50,680 --> 00:05:59,000
stops functioning, after cardiac arrest, it's something like 20 seconds, zero brain activity,

52
00:05:59,000 --> 00:06:04,200
after five seconds, there's almost none, but after 20 seconds, it's zero, something like

53
00:06:04,200 --> 00:06:10,960
that, it says in here somewhere, and so whereas people would say that that means that there's

54
00:06:10,960 --> 00:06:16,560
no mental function, because the brain stops working, here's a quote, near-death experiences,

55
00:06:16,560 --> 00:06:22,120
these are the experiences that occur after cardiac arrest, seam instead to provide direct

56
00:06:22,120 --> 00:06:27,000
evidence for a type of mental functioning that varies inversely, rather than directly

57
00:06:27,000 --> 00:06:31,880
with the observable activity of the nervous system, such evidence we believe fundamentally

58
00:06:31,880 --> 00:06:36,760
conflicts with the conventional doctrine that brain processes produce consciousness, and

59
00:06:36,760 --> 00:06:41,160
supports the alternative view that brain activity normally serves as a kind of a filter,

60
00:06:41,160 --> 00:06:46,200
which somehow constrains the material that emerges into waking consciousness.

61
00:06:46,200 --> 00:06:51,480
On this latter view, the relaxation of the filter under certain still poorly understood circumstances

62
00:06:51,480 --> 00:06:57,520
may lead to drastic alterations in the normal mind-brain relation, and to an associated

63
00:06:57,520 --> 00:07:04,640
enhancement or enlargement of consciousness, to this interpretation of near-death experiences,

64
00:07:04,640 --> 00:07:07,960
which we favor, we now turn.

65
00:07:07,960 --> 00:07:14,560
The meaning that it's just a very profound to sort of think of from the point of view

66
00:07:14,560 --> 00:07:21,280
of a meditator, that there are scientists looking at these questions, and just the idea

67
00:07:21,280 --> 00:07:26,480
that the universe can be seen from the point of view of the mind where there's brain

68
00:07:26,480 --> 00:07:34,280
that we have is simply a squeezing of the mind into a certain shape, a certain experience,

69
00:07:34,280 --> 00:07:37,960
which is seeing hearing, smelling, tasting, feeling, and thinking, and I'm sure I've

70
00:07:37,960 --> 00:07:42,200
bored everyone, have to death, and this is nothing that most people would be interesting,

71
00:07:42,200 --> 00:07:47,800
but this is what I do for fun, so welcome to my world.

72
00:07:47,800 --> 00:08:01,800
Okay, so now I'm going to do some breathing for real, and that's all for now.

