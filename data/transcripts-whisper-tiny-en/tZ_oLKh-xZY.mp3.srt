1
00:00:00,000 --> 00:00:03,920
Hello, and welcome back to Ask a Monk.

2
00:00:03,920 --> 00:00:09,880
Today's question is part of the two-part question that I got mixed up last time, and

3
00:00:09,880 --> 00:00:12,600
I ended up answering two different questions together.

4
00:00:12,600 --> 00:00:20,720
So this is answering the first part of someone's two-part question that I missed.

5
00:00:20,720 --> 00:00:26,840
And the first part was in regards to remembering one's past lives.

6
00:00:26,840 --> 00:00:30,960
In regards to remembering one's past lives, we get generally two questions.

7
00:00:32,160 --> 00:00:37,600
Again, and again, the first question is about how we can believe in past lives.

8
00:00:37,600 --> 00:00:38,400
What is the proof?

9
00:00:38,400 --> 00:00:40,000
What is the basis of past lives?

10
00:00:40,000 --> 00:00:46,880
Because obviously, in Buddhism, we're both the boast is that we don't take things on faith, and so on.

11
00:00:48,080 --> 00:00:52,080
Actually, there are many things that we could take on faith, so to speak.

12
00:00:52,080 --> 00:00:58,400
I think past lives is actually partially under that category.

13
00:00:58,400 --> 00:01:08,320
We take a lot of things on faith, because we believe in the knowledge and the wisdom of those people who have realized them.

14
00:01:08,320 --> 00:01:13,520
Like, if you tell me what Australia is like, well, I've never been to Australia, but I believe you.

15
00:01:13,520 --> 00:01:21,840
I take it on faith, it's not something crucial, and it's a sort of a faith that is based on the fact that I've

16
00:01:21,840 --> 00:01:25,840
never been there, and I maybe don't have the ability to go there.

17
00:01:27,120 --> 00:01:32,320
If it were crucial to my practices, to whether Australia was like this or like that,

18
00:01:32,320 --> 00:01:36,480
then I'd probably want to go there and verify and see for myself.

19
00:01:36,480 --> 00:01:40,480
Past lives could be the same if someone hasn't remembered their past lives.

20
00:01:40,480 --> 00:01:44,000
They might take it on faith, so because it's not crucial.

21
00:01:44,000 --> 00:01:51,720
If it were crucial, this is the second question, is it not a beneficial part of the past

22
00:01:51,720 --> 00:01:57,280
to remember your past lives, and if it is, then why don't we teach it?

23
00:01:57,280 --> 00:02:00,280
Why don't we practice to remember our past lives?

24
00:02:00,280 --> 00:02:05,080
I think the answer is that we don't take it to be a crucial part of the past.

25
00:02:05,080 --> 00:02:10,280
The person's logic, who asked the person who asked this question, their logic was that

26
00:02:10,280 --> 00:02:18,040
there's the saying that we are what we are now because of what we were before, so we will be,

27
00:02:18,040 --> 00:02:24,440
what we will be is because of who we are now. So if we learn about the past, the mistakes

28
00:02:24,440 --> 00:02:30,760
we've made, and even the lessons we've learned, then we'll be able to avoid mistakes and

29
00:02:30,760 --> 00:02:37,480
having to relearn the same lessons again for the future so that our future will be better.

30
00:02:37,480 --> 00:02:44,240
I think there is some validity to that, and so if the question is simply whether remembering

31
00:02:44,240 --> 00:02:48,560
one's past lives is beneficial, I think absolutely. I think there's a lot of things that

32
00:02:48,560 --> 00:02:55,640
are beneficial to the meditation, to the practice of becoming enlightened, besides simply

33
00:02:55,640 --> 00:03:04,360
meditation, that if you remember your past lives, if you are able to have all these magical

34
00:03:04,360 --> 00:03:10,480
powers that they talk about, of being able to see far away, see Heaven, see Hell, to see

35
00:03:10,480 --> 00:03:20,000
beings being born and passing away, there's so many things that therapy is useful, regression

36
00:03:20,000 --> 00:03:24,920
therapy, where you go back to when you were a young person or even now, regression therapy

37
00:03:24,920 --> 00:03:29,920
regressing the past lives, going back and remembering your past lives through hypnosis

38
00:03:29,920 --> 00:03:37,680
or so on. It's said to allow people to become more comfortable and more understanding in regards

39
00:03:37,680 --> 00:03:46,360
to why they are the way they are, so that they don't, they understand better the situation

40
00:03:46,360 --> 00:03:50,520
that they're in and they don't become upset by it, they can see that there's actually

41
00:03:50,520 --> 00:03:55,440
a cause and effect relationship, and I mean seeing that sort of cause and effect relationship

42
00:03:55,440 --> 00:04:02,560
is really what the Buddha's teaching is about, so it's quite useful I think. It's not

43
00:04:02,560 --> 00:04:09,320
crucial though, and in the end it shouldn't be taken for a core practice, because it's

44
00:04:09,320 --> 00:04:17,000
too abstract. The remembering of one's past lives as a cause, having a causal relationship

45
00:04:17,000 --> 00:04:24,520
to the present is not really what we mean by cause and effect, the cause and effect

46
00:04:24,520 --> 00:04:29,360
that we're talking about is what's occurring here and now at every moment, and you'll

47
00:04:29,360 --> 00:04:37,840
never become enlightened by this conceptual analysis of your life that you used to

48
00:04:37,840 --> 00:04:44,280
be like this and now you're like this and so on. It's far too abstract in the concentration

49
00:04:44,280 --> 00:04:51,880
and the intensity of the experience is not anywhere near strong enough to allow you to

50
00:04:51,880 --> 00:04:57,280
really understand the truth of reality that everything is impermanent and that there

51
00:04:57,280 --> 00:05:04,120
is nothing worth clinging to and so on. And this is mainly because it's dealing with

52
00:05:04,120 --> 00:05:08,560
the past and the future, it's dealing with concept, it's not dealing with what you're

53
00:05:08,560 --> 00:05:13,600
seeing here and now, so it's not, the mind doesn't really get it, you can tell yourself

54
00:05:13,600 --> 00:05:18,240
that oh yes, I was a bad person in the past, that's why I have problems now, therefore

55
00:05:18,240 --> 00:05:22,600
I shouldn't be a bad person now because I'll have a bad future, it really doesn't have

56
00:05:22,600 --> 00:05:31,560
any lasting impact on the mind or it has a limited impact on the mind as compared to looking

57
00:05:31,560 --> 00:05:37,520
to see what are the causes and effects of your actions in the present moment when I get

58
00:05:37,520 --> 00:05:42,120
angry, what's it like when I want something, what's it like when I am loving and kind,

59
00:05:42,120 --> 00:05:49,080
what's it like when I am clearly aware, what's it like, looking at what does it mean

60
00:05:49,080 --> 00:05:54,840
to see something, to hear something and coming to change the way we relate to the world

61
00:05:54,840 --> 00:06:01,920
around us, this is really a lot more what the Buddha was teaching about, the Buddha said

62
00:06:01,920 --> 00:06:07,840
one should not go back to the past or worry about the future because what's in the past

63
00:06:07,840 --> 00:06:15,920
is gone and dwelling on it and thinking over it is only going to be a limited value

64
00:06:15,920 --> 00:06:22,160
if any and what's in the future hasn't come yet, but if you can see clearly what's in

65
00:06:22,160 --> 00:06:27,880
the present moment, this is where the cause and effect is occurring, this is empirical,

66
00:06:27,880 --> 00:06:36,600
this is scientific, not based on faith, so really whether we remember our past lives

67
00:06:36,600 --> 00:06:42,360
or not, if we come to see who we are right now then we don't have to worry about the

68
00:06:42,360 --> 00:06:51,440
future either and in fact I think a real part of the Buddha's teaching is giving up the

69
00:06:51,440 --> 00:06:58,720
future, not worrying about even who you're going to be in the future, not living in terms

70
00:06:58,720 --> 00:07:04,280
of past, present, future, living simply in terms of the here and now, who I am here

71
00:07:04,280 --> 00:07:07,880
and now what's happening here and now, not worrying about where's this going to lead,

72
00:07:07,880 --> 00:07:13,640
what's going to happen, the dangers and the problems and so on, coming to see things here

73
00:07:13,640 --> 00:07:20,600
and now as they are and I think in the end you really give up past and future and you don't

74
00:07:20,600 --> 00:07:27,600
ever have any thoughts about what the future holds and what was in the past, so I think

75
00:07:27,600 --> 00:07:33,120
it's misleading to go back to the past because it's going to send you equally as far

76
00:07:33,120 --> 00:07:40,640
into the future, worrying or wondering or trying, planning to try to create a better life

77
00:07:40,640 --> 00:07:46,000
in the future and well that might be useful in the short term, eventually you forget it

78
00:07:46,000 --> 00:07:51,360
anyway because it's not empirical wisdom, it's conceptual, it's the same as thinking

79
00:07:51,360 --> 00:07:59,920
back to when we were young and all the mistakes we made, well yeah it made us more wise in

80
00:07:59,920 --> 00:08:05,880
the worldly sense and it made us more able to live and to react to the situations around

81
00:08:05,880 --> 00:08:11,040
us, but it didn't help to stop us from getting greedy, getting angry, becoming attached

82
00:08:11,040 --> 00:08:15,680
and worried and stressed and so on, it's not strong enough, it's helpful but it's

83
00:08:15,680 --> 00:08:20,880
helpful on a limited scale, so if you're really interested and I know there are a lot of

84
00:08:20,880 --> 00:08:27,120
people who are interested in learning about your past lives, there are ways of doing that,

85
00:08:27,120 --> 00:08:35,120
ways of practicing that are taught in the textbooks really, these Buddha ancient Buddhist

86
00:08:35,120 --> 00:08:41,200
textbooks that have been around for so long and have been used by people to realize

87
00:08:41,200 --> 00:08:49,520
their past lives and so on, you're welcome to look into that but I don't think it will

88
00:08:49,520 --> 00:08:58,240
lead you to the goal and I think whether or not you've realized your past lives, there's

89
00:08:58,240 --> 00:09:02,400
still something much more important to do and especially for people who have limited time

90
00:09:02,400 --> 00:09:08,320
and energy and effort, you'll be much better off to focus on what's really important

91
00:09:08,320 --> 00:09:13,840
and that's the cause and effect relationship that's going on here and now that has caused

92
00:09:13,840 --> 00:09:20,960
you to make plenty of mistakes in the past and will continue to have you make mistakes

93
00:09:20,960 --> 00:09:27,600
into the future unless you can come to see clearly on a phenomenological level what is the

94
00:09:27,600 --> 00:09:39,360
truth of reality, so beneficial yes, necessary no and probably not worth the effort for most

95
00:09:39,360 --> 00:09:45,680
people unless you've done a lot of time and good concentration and the piece was surrounding

96
00:09:45,680 --> 00:09:53,200
and so on, where you can really devote your time to that, okay so thanks for the question,

97
00:09:53,200 --> 00:10:23,040
there you have it, all the best.

