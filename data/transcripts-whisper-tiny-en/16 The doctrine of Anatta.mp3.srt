1
00:00:00,000 --> 00:00:29,960
Today we come to the very important teaching of the Buddha, the doctrine of

2
00:00:29,960 --> 00:00:33,040
Anatta.

3
00:00:33,040 --> 00:00:40,720
The doctrine of Anatta is found only in Buddhism.

4
00:00:40,720 --> 00:00:50,920
And so the doctrine of Anatta makes Buddhism unique among religions because almost all other

5
00:00:50,920 --> 00:01:00,560
major religions believe in the opposite of the doctrine of Anatta.

6
00:01:00,560 --> 00:01:09,880
And this doctrine of Anatta is a central doctrine in the teachings of the Buddha.

7
00:01:09,880 --> 00:01:19,320
And it is important that we understand Anatta first in theory.

8
00:01:19,320 --> 00:01:29,320
And then we must understand it through practice of Vibhasana meditation.

9
00:01:29,320 --> 00:01:44,480
The Buddha taught this doctrine of Anatta to his five disciples, that was for the first time.

10
00:01:44,480 --> 00:01:53,520
Now, two months after his enlightenment, Buddha taught these five disciples, his first

11
00:01:53,520 --> 00:02:01,440
salmon, the salmon of the food of the Dhamma.

12
00:02:01,440 --> 00:02:15,520
And five days later, he taught to them the Anatta, Lakana Sutta, the characteristic of Anatta.

13
00:02:15,520 --> 00:02:29,800
And all these five disciples became at a hand after listening to this teaching of Anatta.

14
00:02:29,800 --> 00:02:41,840
In this Anatta, Lakana Sutta, Buddha declared that the five aggregates are Anatta.

15
00:02:41,840 --> 00:02:57,240
Now, actually he took the five aggregates, one by one, and then said that the Rupa of

16
00:02:57,240 --> 00:03:04,520
corporeality is Anatta with Anatta or feeling as Anatta, Sanya or perception is Anatta.

17
00:03:04,520 --> 00:03:11,480
Sankhara or mental formations are Anatta and with Anatta consciousness is Anatta.

18
00:03:11,480 --> 00:03:26,320
So he taught that all these five aggregates are Anatta and Anatta means not Ata.

19
00:03:26,320 --> 00:03:31,600
The word Anatta is a compound word.

20
00:03:31,600 --> 00:03:44,080
Anatta is compounded as Natta and Ata, so Nana means not an Ata means Ata.

21
00:03:44,080 --> 00:04:04,480
Now the word Ata in Pali has at least two meanings.

22
00:04:04,480 --> 00:04:08,440
One is like when we use oneself to refer to oneself or yourself or himself, we use the

23
00:04:08,440 --> 00:04:20,280
word Ata, so that is a reflexive pronoun and the other meaning of Ata is a permanent entity

24
00:04:20,280 --> 00:04:38,240
conceived by most people in the ancient days to be the core of our beings and so on.

25
00:04:38,240 --> 00:04:42,240
So the word Ata has at least two meanings.

26
00:04:42,240 --> 00:04:50,040
One is just self with a small S, so oneself, himself and so on.

27
00:04:50,040 --> 00:05:06,240
And the other is self or soul or permanent entity believed to be a truth by many people

28
00:05:06,240 --> 00:05:12,440
and during or before the time of the Buddha and during the time or even after the time

29
00:05:12,440 --> 00:05:23,840
of the Buddha, this word Ata is compounded with the word Nana and so far as there is Natta

30
00:05:23,840 --> 00:05:34,520
and that Natta is changed into Anatta following the Pali grammatical rules.

31
00:05:34,520 --> 00:05:40,600
So Anatta means not Ata.

32
00:05:40,600 --> 00:05:49,280
The Pali word Anatta or by the doctrine of Anatta, Buddha negated the Ata.

33
00:05:49,280 --> 00:05:53,040
And what was the other that Buddha negated?

34
00:05:53,040 --> 00:06:07,680
Now, as I said before, in those days, people believe in a soul or a permanent entity and

35
00:06:07,680 --> 00:06:15,760
they think that there is something that is permanent and that permanent something lives

36
00:06:15,760 --> 00:06:25,240
in the bodies and then when the physical body becomes old and decrepit and dies, it will

37
00:06:25,240 --> 00:06:28,920
take another body and so on.

38
00:06:28,920 --> 00:06:38,880
And also they believe that that permanent entity is the real one that acts or that does

39
00:06:38,880 --> 00:06:52,320
actions and that entity is the real one that experiences, happiness or suffering.

40
00:06:52,320 --> 00:07:05,840
And also that permanent entity is the Lord, it has the power to administer the activities

41
00:07:05,840 --> 00:07:07,400
of the body and so on.

42
00:07:07,400 --> 00:07:14,600
And that is what those people believed during that time.

43
00:07:14,600 --> 00:07:26,240
And so Buddha came and then declared that the five aggregates are not Ata.

44
00:07:26,240 --> 00:07:40,200
The five aggregates are not at the conceived to be a permanent entity where in the Anatta

45
00:07:40,200 --> 00:07:56,080
Anasudha, Buddha said, kafariality is not Ata and so on.

46
00:07:56,080 --> 00:07:58,160
And Buddha analyzed the whole world into the five aggregates.

47
00:07:58,160 --> 00:08:06,680
So there is nothing in addition to five aggregates that we can go to a world.

48
00:08:06,680 --> 00:08:14,400
So when Buddha said, these five aggregates are not Ata, it amounts to saying that there

49
00:08:14,400 --> 00:08:17,760
is no Ata at all in the world.

50
00:08:17,760 --> 00:08:31,800
So although Buddha did not see there is no Ata, when he said, these five aggregates are

51
00:08:31,800 --> 00:08:43,400
not ata, it amounts to saying that there is no Ata.

52
00:08:43,400 --> 00:08:51,040
Now what it is, that is Anatta, that is described as Anatta.

53
00:08:51,040 --> 00:09:01,240
Now I just told you that Buddha declared the five aggregates to be not Ata.

54
00:09:01,240 --> 00:09:11,160
Now there are three statements which are well known among Buddhists and these statements

55
00:09:11,160 --> 00:09:35,720
are all conditions are impermanent, all conditions are suffering and all things are Anatta.

56
00:09:35,720 --> 00:09:42,880
In the first two statements, Buddha said, all conditions, phenomena are impermanent and

57
00:09:42,880 --> 00:09:43,880
suffering.

58
00:09:43,880 --> 00:09:50,200
But in the third, Buddha did not use the raw conditions phenomena, but he used instead

59
00:09:50,200 --> 00:09:52,200
the raw dharma.

60
00:09:52,200 --> 00:10:06,640
So according to that statement, all dharma's are not ata and all dharma's mean the five

61
00:10:06,640 --> 00:10:11,840
aggregates and nibbana.

62
00:10:11,840 --> 00:10:23,840
So we must understand that not only are the five aggregates Anatta, but also nibbana is.

63
00:10:23,840 --> 00:10:33,240
So sometimes we might think that since nibbana is the object of the world, nibbana must

64
00:10:33,240 --> 00:10:43,640
be Ata, but here Buddha said, all dharma's are not ata and all dharma's include nibbana.

65
00:10:43,640 --> 00:10:50,400
So when we come to Anatta, we must understand that all five aggregates and also nibbana

66
00:10:50,400 --> 00:10:58,160
are not ata.

67
00:10:58,160 --> 00:11:07,800
Now we must understand that that statement that all dharma's are anatta correctly in different

68
00:11:07,800 --> 00:11:09,800
contexts.

69
00:11:09,800 --> 00:11:20,920
When it is put out as a general statement, we must understand all dharma's to me, five aggregates

70
00:11:20,920 --> 00:11:22,880
and nibbana.

71
00:11:22,880 --> 00:11:31,960
Often it is in the context of vipasana when all dharma's mean the five aggregates only

72
00:11:31,960 --> 00:11:51,840
and not nibbana because nibbana cannot be taken as an object for vipasana meditation.

73
00:11:51,840 --> 00:12:06,640
In the Anatta-Lakana Sutta, Buddha did not see nibbana was anatta.

74
00:12:06,640 --> 00:12:15,960
Now why did Buddha not mention nibbana as anatta in that Sutta?

75
00:12:15,960 --> 00:12:29,880
I think it is because Buddha wanted to direct the five disciples to the vipasana meditation.

76
00:12:29,880 --> 00:12:38,280
So for those who practice vipasana meditation, only the five aggregates can be the

77
00:12:38,280 --> 00:12:49,440
object of meditation, so that is probably it is probably because of this that Buddha did

78
00:12:49,440 --> 00:13:10,000
not mention nibbana as anatta in the Anatta-Lakana Sutta.

79
00:13:10,000 --> 00:13:20,160
The meaning of the word anatta, so we know that anatta means not atta.

80
00:13:20,160 --> 00:13:32,000
In the commentaries, it is explained as meaning not being the core of anything or not

81
00:13:32,000 --> 00:13:43,000
having a core or not being susceptible to the exercise of power.

82
00:13:43,000 --> 00:13:50,200
So at least we can say there are two meanings or interpretations to understand here.

83
00:13:50,200 --> 00:14:00,960
Not atta and not atta means no core or having no core and also it means we cannot have

84
00:14:00,960 --> 00:14:12,560
power over this.

85
00:14:12,560 --> 00:14:24,640
So when Buddha said say vupa or copperyality is anatta, he meant that vupa or copperyality

86
00:14:24,640 --> 00:14:37,800
is not a core or not substantial and also he meant that copperyality is the one over

87
00:14:37,800 --> 00:14:51,480
which we cannot have our power, as the birds, interpretations and that is being no core

88
00:14:51,480 --> 00:15:07,080
or having no core is accepted by our teachers having no core is appropriate in our opinion.

89
00:15:07,080 --> 00:15:19,600
If we say vupa or copperyality is anatta, because it has no core, then we can argue

90
00:15:19,600 --> 00:15:33,160
that vupa has no core, but there must be a core somewhere, only vupa does not possess

91
00:15:33,160 --> 00:15:48,520
that core, so that kind of argument can come according to the teachings of the Buddha that

92
00:15:48,520 --> 00:15:54,680
argument is not correct.

93
00:15:54,680 --> 00:16:01,200
Now what is it that we call a core here, his substance or essence?

94
00:16:01,200 --> 00:16:13,480
So in the commentaries it is stated that something that is wrongly conceived as a self, a

95
00:16:13,480 --> 00:16:22,880
permanent entity, so that is what we call core here, and also as I said before the

96
00:16:22,880 --> 00:16:34,800
some permanent entity that abides in the bodies of beings and that permanent entity

97
00:16:34,800 --> 00:16:46,760
lives forever, and when the present body becomes old and it dies then it transfers itself

98
00:16:46,760 --> 00:16:59,400
to another new body and so on, and also the permanent, I mean a core means a dua that

99
00:16:59,400 --> 00:17:10,080
is someone who performs the actions, now although we think that we are doing the actions

100
00:17:10,080 --> 00:17:18,100
according to that opinion, it is theatta that is doing the actions and not us, so

101
00:17:18,100 --> 00:17:29,920
theatta is the one that performs the actions and we are just the instruments and also

102
00:17:29,920 --> 00:17:42,340
whenever we experience happiness or suffering it is not us who experience, but theatta

103
00:17:42,340 --> 00:17:58,740
experiences and we are just the instrument in theatta experiencing happiness or suffering

104
00:17:58,740 --> 00:18:09,260
and also theatta is his own master, so one who is his own master is the most important

105
00:18:09,260 --> 00:18:21,660
part of the theme and so it is called a core, now ruba is not a core means ruba is not

106
00:18:21,660 --> 00:18:31,140
any one of these, ruba is not a self or a soul, ruba is not the abitah or not not one

107
00:18:31,140 --> 00:18:41,620
that lives in the body, for all the time, ruba is not a dua, ruba is not an experiencer

108
00:18:41,620 --> 00:18:52,660
and ruba is not one who which is its own master, when we understand in that way we

109
00:18:52,660 --> 00:19:04,380
as a to understand the another nature of things, now how do we know that ruba or corporeality

110
00:19:04,380 --> 00:19:14,420
for example is not self, not an abitah, not a dua, not an experiencer and not the one who

111
00:19:14,420 --> 00:19:27,740
is his own master, now the explanation given in the commentaries is first we understand

112
00:19:27,740 --> 00:19:36,940
that corporeality is impermanent and so it is suffering, so these two let us say we have

113
00:19:36,940 --> 00:19:52,180
understood, now if corporeality is impermanent and it is suffering then if it must have

114
00:19:52,180 --> 00:20:06,740
no core or no substance in it because it cannot prevent itself from being impermanent and

115
00:20:06,740 --> 00:20:17,700
from being suffering, so this is the argument taught in the commentaries, what is impermanent

116
00:20:17,700 --> 00:20:27,780
and suffering cannot even prevent its own impermanence and being suffering, so how could

117
00:20:27,780 --> 00:20:39,500
it have this state of a dua or an experiencer and so on, that means something that is impermanent,

118
00:20:39,500 --> 00:20:45,420
it cannot change itself into a permanent thing, something that is suffering, it cannot

119
00:20:45,420 --> 00:20:57,140
change itself into happiness, so if it cannot prevent itself from becoming impermanent

120
00:20:57,140 --> 00:21:07,980
and suffering, how can it be a master or a dua or an experiencer, so what is impermanent

121
00:21:07,980 --> 00:21:18,940
and what is suffering is actually not ata or anata, in the sense of having no core or

122
00:21:18,940 --> 00:21:31,820
in the sense of being insubstantial, now this takes us to the another interpretation of

123
00:21:31,820 --> 00:21:45,900
the rata anata that is being insus susceptible to exercise of power, it simply means it does

124
00:21:45,900 --> 00:21:58,020
not follow our wishes, now all these five aggregates are anata because of the words that

125
00:21:58,020 --> 00:22:10,420
by the Buddha, what is duka is anata, now this is a criterion, Buddha gave us to decide whether

126
00:22:10,420 --> 00:22:18,740
something is anata or not, so if we want to know whether something is anata or not, we must

127
00:22:18,740 --> 00:22:32,100
us know whether it is duka or not and what is it criterion for duka, now Buddha gave us

128
00:22:32,100 --> 00:22:46,860
another criterion and that is what is impermanent is duka, so we have two formulas here,

129
00:22:46,860 --> 00:22:57,940
what is impermanent is suffering and what is suffering is not so another, then what is

130
00:22:57,940 --> 00:23:20,580
the criterion for being anata and being impermanent, to put it plainly disappearing after arising,

131
00:23:20,580 --> 00:23:29,540
so disappearing after arising is a mark of being impermanent, so whatever arises and disappears

132
00:23:29,540 --> 00:23:39,300
is impermanent and whatever is impermanent is duka or suffering and whatever is duka is

133
00:23:39,300 --> 00:23:53,480
another, now we know that rupa or corporeality or feeling or perception and so on, so

134
00:23:53,480 --> 00:24:02,220
they arise and disappear or they disappear after arising, so when we see for ourselves

135
00:24:02,220 --> 00:24:08,060
that these arise and disappear we know that they are impermanent, when we know that they

136
00:24:08,060 --> 00:24:14,540
are impermanent we also know that they are duka, they are suffering and duka here means

137
00:24:14,540 --> 00:24:27,620
suffering here means not physically painful but here suffering means being oppressed by

138
00:24:27,620 --> 00:24:37,660
rise and fall, so the meaning of duka is being suppressed by rise and fall that means if

139
00:24:37,660 --> 00:24:46,060
anything has arising and disappearing, if anything arises and disappears it is called duka

140
00:24:46,060 --> 00:24:55,100
suffering because it is bombarded by or afflicted by rising and disappearing, now if you

141
00:24:55,100 --> 00:25:04,300
see something as impermanent you know that you cannot turn it into a impermanent thing

142
00:25:04,300 --> 00:25:11,940
and if you see something as duka you cannot turn it into zuka, so you cannot exercise

143
00:25:11,940 --> 00:25:20,860
any power over them and so they are not self or they are anata, so in the sense that

144
00:25:20,860 --> 00:25:28,340
they do not follow your wish, that means they arise and disappear according to their

145
00:25:28,340 --> 00:25:42,740
wish and not according to your wish, so mind and matter arise and disappear depending

146
00:25:42,740 --> 00:25:49,660
on the conditions and whether we do not want them to arise or not when there are conditions

147
00:25:49,660 --> 00:25:55,620
they will arise and they will disappear, so we have no control over the arising and

148
00:25:55,620 --> 00:26:09,020
disappearing, that is why the having no control over them having no excess of power over

149
00:26:09,020 --> 00:26:24,420
them is said to be a mark of a mark of being anata, so if you want to know whether something

150
00:26:24,420 --> 00:26:37,820
is anata or not we have to see, we have to find this mark or this sign and that is not

151
00:26:37,820 --> 00:26:46,340
being susceptible to the exercise of power, so in the commentaries the characteristic

152
00:26:46,340 --> 00:27:01,220
or the mark of anata is given as insusceptibility to the exercise of power, I think we should

153
00:27:01,220 --> 00:27:11,420
also understand that having no cool is also a characteristic of anata, so when we want

154
00:27:11,420 --> 00:27:19,320
to know whether something is anata we may look for one of these two marks whether it

155
00:27:19,320 --> 00:27:30,620
has a core or it is substantial and also whether it acts according to our wish or it is out

156
00:27:30,620 --> 00:27:46,560
of control by us, most people are unable to see the anata nature of things because this

157
00:27:46,560 --> 00:28:00,760
another nature is confused by compactness, we take things to be compact, we take things

158
00:28:00,760 --> 00:28:11,360
to be substantial and so we fail to see the another nature of things, you are confused

159
00:28:11,360 --> 00:28:21,440
by compactness means confused by the notion of compactness, now we think that we are

160
00:28:21,440 --> 00:28:31,680
a compact thing, the meta is compact and mine is also compact and so long as we think

161
00:28:31,680 --> 00:28:48,720
we are compact we cannot see the insubstantiality of ourselves and compactness or the

162
00:28:48,720 --> 00:29:06,760
notion of compactness conceives another nature because we do not give attention to resolution

163
00:29:06,760 --> 00:29:19,800
into elements, taking down different elements, so long as we see, mine and meta is one

164
00:29:19,800 --> 00:29:31,840
compact theme, we will not see it is another nature, but when we break down the body and

165
00:29:31,840 --> 00:29:45,440
mine into elements that is elements that compose then we will come to see the insubstantiality

166
00:29:45,440 --> 00:29:57,840
of things, now most people do not even see mine and meta as two separate things, sometimes

167
00:29:57,840 --> 00:30:07,320
we think we are just one, our mine and barrier just one, so long as we do not see the

168
00:30:07,320 --> 00:30:14,440
separateness of mine and meta or in different elements that constitute mine and that

169
00:30:14,440 --> 00:30:22,320
constitute meta, so long as we cannot see them we will not be able to break this notion

170
00:30:22,320 --> 00:30:28,400
of compactness, if we cannot break the notion of compactness we will not see the another

171
00:30:28,400 --> 00:30:41,280
nature of things, but when we pay attention to what is happening to us or what is happening

172
00:30:41,280 --> 00:30:49,920
in us or when we pay attention to the object, to the object at the present moment we will

173
00:30:49,920 --> 00:31:02,240
not fail to see that what we thought to be one compact theme is actually composed of

174
00:31:02,240 --> 00:31:11,880
small elements, that what we thought to be one compact theme is actually composed of

175
00:31:11,880 --> 00:31:20,720
small elements.

176
00:31:20,720 --> 00:31:31,240
If we do not practice meditation we think that this one mine sees, hears, smells, tastes,

177
00:31:31,240 --> 00:31:41,480
tashes and things, but when you watch your mind you know that seeing is one, you see

178
00:31:41,480 --> 00:31:49,400
with one mind and you hear with one mind and so on, so they are separate things, they

179
00:31:49,400 --> 00:31:59,160
are separate elements and also the body you know the full elements of art, water, fire

180
00:31:59,160 --> 00:32:12,880
and air and so when you are able to see these elements separately you break the notion

181
00:32:12,880 --> 00:32:21,760
of compactness, so when you can break the notion of compactness you know that what you

182
00:32:21,760 --> 00:32:35,200
thought to be substantial is really insubstantial and so you see that there is an nata

183
00:32:35,200 --> 00:32:46,260
and when you see the elements separately one distinct from the other and the arise

184
00:32:46,260 --> 00:32:54,440
and disappear depending on the conditions you come to the realization that there is no

185
00:32:54,440 --> 00:33:04,360
exercise of power over them, for example seeing mine or seeing consciousness, so when

186
00:33:04,360 --> 00:33:11,440
there is something to be seen and when it is brought into the avenue of your eyes there

187
00:33:11,440 --> 00:33:18,480
is the seeing consciousness, so you cannot prevent this seeing consciousness from arising

188
00:33:18,480 --> 00:33:26,920
when there are these conditions however much you do not want this seeing consciousness

189
00:33:26,920 --> 00:33:33,400
not to arise, it will arise because there are conditions for it to arise, so there is

190
00:33:33,400 --> 00:33:43,040
no exercise of power over the arising of that seeing consciousness, so it arises depending

191
00:33:43,040 --> 00:33:51,520
on the conditions and so long as there are conditions it will arise, so you have no power

192
00:33:51,520 --> 00:34:03,360
over them and having no power over them is one aspect of being a nata, so we have to

193
00:34:03,360 --> 00:34:15,880
we can really see the impermanent nature of things that is they are insubstantial, they

194
00:34:15,880 --> 00:34:25,280
have no inner core and also they are not susceptible to exercise of power only when we

195
00:34:25,280 --> 00:34:35,000
practice vipasana meditation only when we pay attention to the objects, without the practice

196
00:34:35,000 --> 00:34:42,920
of vipasana meditation we cannot see the nata nature clearly, we may read books and we

197
00:34:42,920 --> 00:34:51,320
may hear or listen to talks and we think we understand the nata nature, but that understanding

198
00:34:51,320 --> 00:35:00,440
is not our understanding, it is just a borrowed understanding, only when we practice vipasana

199
00:35:00,440 --> 00:35:07,560
meditation only when we pay close attention to the objects at the present moment can

200
00:35:07,560 --> 00:35:19,000
we see another nature of things along with the impermanent nature and suffering nature,

201
00:35:19,000 --> 00:35:34,680
so the nature of nata can be understood only through the practice of vipasana meditation,

202
00:35:34,680 --> 00:35:45,200
since when we practice vipasana meditation we do not see anything that is substantial,

203
00:35:45,200 --> 00:35:59,920
anything that we can exercise, power over, we come to realization that that is just the

204
00:35:59,920 --> 00:36:10,840
suffering and no person who suffers, because what we call a person is just a combination

205
00:36:10,840 --> 00:36:26,720
of the mind and matter and there is no person over and above the mind and matter, so

206
00:36:26,720 --> 00:36:34,120
through vipasana meditation we come to see that there is just suffering, there is just

207
00:36:34,120 --> 00:36:42,880
arising and disappearing and nothing that arises and disappears and also when we look at

208
00:36:42,880 --> 00:36:52,200
the activities we see that there is just the activity and actually no person say who

209
00:36:52,200 --> 00:37:05,680
performs these activities and also it is said that the physician of suffering or nibana

210
00:37:05,680 --> 00:37:17,440
is but there is no person who experiences nibana because there is just mind and matter

211
00:37:17,440 --> 00:37:24,800
and no person and also it is said that there is the the the path that nobody who walks

212
00:37:24,800 --> 00:37:36,560
on that path that means the path means of the the eight components of the path and apart

213
00:37:36,560 --> 00:37:45,360
from these eight components we do not see anything to call a path and so that is just these

214
00:37:45,360 --> 00:37:58,760
eight qualities I mean eight mental states and no person, no body who practices these

215
00:37:58,760 --> 00:38:10,720
eight mental states because in the ultimate analysis there are just mind and body I mean

216
00:38:10,720 --> 00:38:18,280
nama and rupa or mind and matter and in addition to mind and matter there is nothing we

217
00:38:18,280 --> 00:38:31,760
can call a person or an individual seeing the another nature of mind and matter or the

218
00:38:31,760 --> 00:38:41,200
five aggregates is not the end of the practice of vipasana actually it is the beginning

219
00:38:41,200 --> 00:38:52,200
of vipasana so people have to make more effort to go through the higher stages of vipasana

220
00:38:52,200 --> 00:39:03,400
practice until they reach the stage of realizing the four noble truths but the realization

221
00:39:03,400 --> 00:39:15,160
of four noble truths cannot come about without the understanding of the another nature

222
00:39:15,160 --> 00:39:24,560
of mind and matter actually not only another nature but also the impermanent nature

223
00:39:24,560 --> 00:39:35,320
and suffering nature of mind and matter or the five aggregates so it is very important

224
00:39:35,320 --> 00:39:43,560
that when we practice vipasana meditation we see for ourselves through our own experience

225
00:39:43,560 --> 00:39:51,200
the impermanent nature the suffering nature and the another nature of mind and matter

226
00:39:51,200 --> 00:40:05,040
so that we can go on on this path going through the higher stages of vipasana meditation

227
00:40:05,040 --> 00:40:22,200
until we reach our goal so we do recapitulate motor thought that five aggregates are not

228
00:40:22,200 --> 00:40:30,000
other and since there are only five aggregates in the world it amounts to say that there

229
00:40:30,000 --> 00:40:44,360
is no other and the characteristic of anata is and is one of the three general characteristics

230
00:40:44,360 --> 00:40:54,120
of all conditions phenomena and it is important that yogis see the characteristic of

231
00:40:54,120 --> 00:41:02,400
another along with the characteristic of impermanent and suffering and this seeing the

232
00:41:02,400 --> 00:41:13,800
three characteristics of all conditions phenomena will eventually lead yogis to the realization

233
00:41:13,800 --> 00:41:27,720
of the truth.

