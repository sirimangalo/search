1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Dhamapada.

2
00:00:05,000 --> 00:00:12,000
Today we continue on with verse number 94, which reads as follows.

3
00:00:12,000 --> 00:00:21,000
Yes, Indriani, Samatang Gattani, Asa Yata, Saratina, Sudhanta.

4
00:00:21,000 --> 00:00:31,000
Pahinaman, Asa, Anasu, Wasa, Deva, Pita, Pihayantita, Dino.

5
00:00:31,000 --> 00:00:39,000
Which means whose faculties have become tranquil,

6
00:00:39,000 --> 00:00:43,000
Samatang Gattani, who have gone to tranquility,

7
00:00:43,000 --> 00:00:53,000
like a horse tamed by Asarati, a charioteer.

8
00:00:53,000 --> 00:00:57,000
Pahinaman, Asa, Anasu, Wasa, a mind that is dedicated,

9
00:00:57,000 --> 00:01:02,000
for one whose mind is dedicated, who is without Asava.

10
00:01:02,000 --> 00:01:07,000
Again, we have these taints, without these outflowings,

11
00:01:07,000 --> 00:01:12,000
or these streams of defilement, or of attachment.

12
00:01:12,000 --> 00:01:15,000
Deva, Pita, Sa, Pihayantita, Dino.

13
00:01:15,000 --> 00:01:27,000
Even angels are pleased or hold dear, such a one, basically.

14
00:01:27,000 --> 00:01:32,000
Even the angels find such a one to be dear.

15
00:01:32,000 --> 00:01:35,000
It's quite a poetic verse.

16
00:01:35,000 --> 00:01:39,000
We got the nice imagery of the horse, trained by the charioteer,

17
00:01:39,000 --> 00:01:43,000
a person who trains their faculties.

18
00:01:43,000 --> 00:01:52,000
So this was taught, we are told, in regards to Mahakacayana.

19
00:01:52,000 --> 00:01:59,000
Mahakacayana, one of the 80 great disciples of the Buddha.

20
00:01:59,000 --> 00:02:04,000
And he lived much of his life in a one-tee, we are told,

21
00:02:04,000 --> 00:02:07,000
which is not significant in the story,

22
00:02:07,000 --> 00:02:10,000
but it's also significant if we want to know who

23
00:02:10,000 --> 00:02:11,000
Katayana was.

24
00:02:11,000 --> 00:02:20,000
Katayana is supposed to be the monk who sort of founded the study

25
00:02:20,000 --> 00:02:23,000
of Pauli.

26
00:02:23,000 --> 00:02:25,000
Because when he went to this country of Awandi,

27
00:02:25,000 --> 00:02:30,000
which is, I understand, outside of the area where they spoke the

28
00:02:30,000 --> 00:02:37,000
Megadun, or what we know often referred to as Pauli language.

29
00:02:37,000 --> 00:02:41,000
He had to translate the teachings, because people didn't understand

30
00:02:41,000 --> 00:02:42,000
teaching.

31
00:02:42,000 --> 00:02:47,000
So he had to first translate them into other people's language,

32
00:02:47,000 --> 00:02:51,000
but then he had to also teach people Pauli,

33
00:02:51,000 --> 00:02:54,000
so they could learn the Buddha's words for themselves,

34
00:02:54,000 --> 00:02:56,000
so they could help with translations,

35
00:02:56,000 --> 00:02:59,000
and rather than having to translate,

36
00:02:59,000 --> 00:03:02,000
so they could read the original text.

37
00:03:02,000 --> 00:03:06,000
So we have what is now called Katayana, which is a grammar.

38
00:03:06,000 --> 00:03:10,000
It's Nakachayana, which means the book that's supposed to have been

39
00:03:10,000 --> 00:03:15,000
written by or been handed down by the tradition of Mahakacayana himself.

40
00:03:15,000 --> 00:03:21,000
When it begins Ato Akara Sanyato, which is according to the traditions,

41
00:03:21,000 --> 00:03:23,000
supposed to be the words of the Buddha,

42
00:03:23,000 --> 00:03:29,000
the Buddha remarked upon a monk who was sitting by the bank of a lake,

43
00:03:29,000 --> 00:03:33,000
and he was trying to think,

44
00:03:33,000 --> 00:03:37,000
he was thinking of a rise, the arising and ceasing of phenomena,

45
00:03:37,000 --> 00:03:40,000
which is Udaya Baya.

46
00:03:40,000 --> 00:03:43,000
And so he was repeating this to himself,

47
00:03:43,000 --> 00:03:46,000
and then he started to doze off,

48
00:03:46,000 --> 00:03:51,000
and he started mixing things up,

49
00:03:51,000 --> 00:03:54,000
and he said he was an elderly fellow, I think,

50
00:03:54,000 --> 00:03:58,000
and there was a swan out on the lake or some bird out on the lake,

51
00:03:58,000 --> 00:04:00,000
and so he started saying to himself,

52
00:04:00,000 --> 00:04:02,000
because it sounded like what he was saying,

53
00:04:02,000 --> 00:04:04,000
sounded like Udaya,

54
00:04:04,000 --> 00:04:07,000
Udaya Paka, which means bird on the lake.

55
00:04:07,000 --> 00:04:09,000
So he started saying instead,

56
00:04:09,000 --> 00:04:14,000
Udaya Paka, Udaya Paka, and instead of Udaya Baya,

57
00:04:14,000 --> 00:04:17,000
and the Buddha noticed what he was doing,

58
00:04:17,000 --> 00:04:20,000
and kind of shook his head and said,

59
00:04:20,000 --> 00:04:25,000
Ato Akara Sanyato, which means the meaning is known by the Agara,

60
00:04:25,000 --> 00:04:27,000
the letters or the words.

61
00:04:27,000 --> 00:04:31,000
So the wording is important, is what he was saying.

62
00:04:31,000 --> 00:04:37,000
And that's supposed to have been heard by Mahakacayana,

63
00:04:37,000 --> 00:04:41,000
who thought, yes, it's important that people get this language right,

64
00:04:41,000 --> 00:04:44,000
and so he started teaching Pauli in the country

65
00:04:44,000 --> 00:04:47,000
where they, in the places where they didn't speak,

66
00:04:47,000 --> 00:04:50,000
Pauli or in Avanti.

67
00:04:50,000 --> 00:04:55,000
But what it meant is, what all this meant is that he was living far away from the Buddha,

68
00:04:55,000 --> 00:04:58,000
and even though he was living far away,

69
00:04:58,000 --> 00:05:04,000
because he was so well respected,

70
00:05:04,000 --> 00:05:09,000
and because he often apparently returned to pick the Buddha's words

71
00:05:09,000 --> 00:05:14,000
and to bring them back and translate them and share them with the people in Avanti,

72
00:05:14,000 --> 00:05:18,000
he would often travel great distances just to hear the Buddha talk,

73
00:05:18,000 --> 00:05:22,000
so they would always leave a seat out for Mahakacayana.

74
00:05:22,000 --> 00:05:27,000
In expectation that he might show up,

75
00:05:27,000 --> 00:05:32,000
but the story goes that Sakad was this Buddhist angel,

76
00:05:32,000 --> 00:05:35,000
so it was to be the king of the angels up in heaven,

77
00:05:35,000 --> 00:05:42,000
kind of like, I guess, a medieval Christian idea of concept of God up on a throne.

78
00:05:42,000 --> 00:05:50,000
And he would come down and listen to the Buddha's teaching,

79
00:05:50,000 --> 00:05:52,000
but he would also look out for the monks,

80
00:05:52,000 --> 00:05:56,000
and he was apparently quite fond of Mahakacayana,

81
00:05:56,000 --> 00:06:02,000
and would wait and see whether Mahakacayana was coming,

82
00:06:02,000 --> 00:06:06,000
and so he was thinking to himself,

83
00:06:06,000 --> 00:06:09,000
would be great if he showed up,

84
00:06:09,000 --> 00:06:13,000
and suddenly he saw that Mahakacayana suddenly saw that Mahakacayana had come,

85
00:06:13,000 --> 00:06:17,000
and he immediately went over to Mahakacayana,

86
00:06:17,000 --> 00:06:21,000
bowed down to him and grasped his feet

87
00:06:21,000 --> 00:06:25,000
and placed his head at his bow down with his head at his feet,

88
00:06:25,000 --> 00:06:31,000
then stood respectfully and gave him flowers and perfumes,

89
00:06:31,000 --> 00:06:34,000
whatever they would do in India,

90
00:06:34,000 --> 00:06:37,000
it was a common thing to put flowers on people's feet,

91
00:06:37,000 --> 00:06:39,000
and feed in respect for them.

92
00:06:39,000 --> 00:06:42,000
Why, because the feet are the lowest part of the body,

93
00:06:42,000 --> 00:06:44,000
and the head is the highest,

94
00:06:44,000 --> 00:06:48,000
so the greatest way of paying the most ultimate symbolic respect

95
00:06:48,000 --> 00:06:50,000
is to put your head at someone else's feet,

96
00:06:50,000 --> 00:06:56,000
so that's become a tradition from olden type.

97
00:06:56,000 --> 00:07:01,000
And again, as usual, we've got these troublemaker monks

98
00:07:01,000 --> 00:07:06,000
who have nothing better to do than to criticize other people's good deeds,

99
00:07:06,000 --> 00:07:09,000
or other people's deeds, let's say,

100
00:07:09,000 --> 00:07:16,000
who are intent upon criticism,

101
00:07:16,000 --> 00:07:19,000
and more on criticism later,

102
00:07:19,000 --> 00:07:21,000
and more on criticism can be a good thing,

103
00:07:21,000 --> 00:07:23,000
and it's important to be critical,

104
00:07:23,000 --> 00:07:26,000
but these guys seem to have been over-critical,

105
00:07:26,000 --> 00:07:28,000
and this was what was going on in the earlier.

106
00:07:28,000 --> 00:07:33,000
We've had this in this, seems to be a theme in this chapter,

107
00:07:33,000 --> 00:07:36,000
because we're talking, this is the,

108
00:07:36,000 --> 00:07:39,000
this is the Arahantalagwaka,

109
00:07:39,000 --> 00:07:41,000
so it's about Arahant,

110
00:07:41,000 --> 00:07:43,000
it's talking about people who are enlightened,

111
00:07:43,000 --> 00:07:45,000
and the theme seems to be,

112
00:07:45,000 --> 00:07:48,000
even though other people don't get that,

113
00:07:48,000 --> 00:07:51,000
or don't get how awesome that is,

114
00:07:51,000 --> 00:07:54,000
or try to discount that.

115
00:07:54,000 --> 00:07:56,000
So these other monks were saying,

116
00:07:56,000 --> 00:08:00,000
you know, what's going on with these two?

117
00:08:00,000 --> 00:08:04,000
You know, why is Saka

118
00:08:04,000 --> 00:08:07,000
paying respect only to Mahaka Jayana?

119
00:08:07,000 --> 00:08:11,000
Why doesn't he show that kind of respect to all the great disciples?

120
00:08:11,000 --> 00:08:14,000
So they're kind of criticizing Saka and not Mahaka Jayana,

121
00:08:14,000 --> 00:08:16,000
but it's indirect criticism,

122
00:08:16,000 --> 00:08:19,000
because they're saying, what's so special about him?

123
00:08:19,000 --> 00:08:22,000
Why should he pay respect to this one, monk?

124
00:08:22,000 --> 00:08:26,000
You know, he's just, just another one of the 80 great disciples,

125
00:08:26,000 --> 00:08:29,000
what's so special.

126
00:08:29,000 --> 00:08:32,000
And the Buddha heard what they were saying,

127
00:08:32,000 --> 00:08:35,000
silence them by saying,

128
00:08:35,000 --> 00:08:37,500
monks, those monks who like my son,

129
00:08:37,500 --> 00:08:39,000
Kachayana the Great,

130
00:08:39,000 --> 00:08:41,500
keep the doors of their sentences guarded,

131
00:08:41,500 --> 00:08:44,500
or beloved both of God's and men.

132
00:08:44,500 --> 00:08:49,500
And then he taught this verse.

133
00:08:49,500 --> 00:08:54,500
So it's not actually an answer to the question

134
00:08:54,500 --> 00:08:57,500
of why he's singled out Mahaka Jayana,

135
00:08:57,500 --> 00:09:03,500
but it's indirectly, the implication here is that

136
00:09:03,500 --> 00:09:05,500
Mahaka Jayana is worth the respect.

137
00:09:05,500 --> 00:09:07,500
Why do you care if they, you know,

138
00:09:07,500 --> 00:09:11,500
it's irrelevant as to whether someone

139
00:09:11,500 --> 00:09:14,500
does it to him or does it to all of the great monks?

140
00:09:14,500 --> 00:09:16,500
It's worth doing.

141
00:09:16,500 --> 00:09:19,500
This paying respect is a good thing

142
00:09:19,500 --> 00:09:21,500
because of the qualities of Mahaka Jayana

143
00:09:21,500 --> 00:09:23,500
that are worthy of respect.

144
00:09:23,500 --> 00:09:27,500
So it's funny you often get this in monasteries

145
00:09:27,500 --> 00:09:28,500
by jealous monks.

146
00:09:28,500 --> 00:09:30,500
They'll say, why did that monk get this?

147
00:09:30,500 --> 00:09:31,500
Why did some monks?

148
00:09:31,500 --> 00:09:35,500
Sometimes you just, you become obsessed

149
00:09:35,500 --> 00:09:39,500
because when your world is simplified

150
00:09:39,500 --> 00:09:42,500
it's an extent where the biggest central pleasure

151
00:09:42,500 --> 00:09:44,500
of the day is a bowl of food.

152
00:09:44,500 --> 00:09:47,500
That bowl of food can become quite an attachment

153
00:09:47,500 --> 00:09:50,500
and if your bowl of food has better food than mine,

154
00:09:50,500 --> 00:09:55,500
gosh, some of the problems that come up in monasteries.

155
00:09:57,500 --> 00:10:00,500
So this idea of favor,

156
00:10:00,500 --> 00:10:03,500
some people revert to childhood

157
00:10:03,500 --> 00:10:06,500
and you got more than me and yeah,

158
00:10:06,500 --> 00:10:09,500
one and so on.

159
00:10:09,500 --> 00:10:12,500
But it's, you know, all the Buddha is saying

160
00:10:12,500 --> 00:10:14,500
as I can see,

161
00:10:14,500 --> 00:10:16,500
as that it's worth doing.

162
00:10:16,500 --> 00:10:19,500
There's no, how can you criticize

163
00:10:19,500 --> 00:10:21,500
when people do a pay respect

164
00:10:21,500 --> 00:10:24,500
to someone worthy of respect?

165
00:10:24,500 --> 00:10:26,500
It's funny because people often

166
00:10:26,500 --> 00:10:28,500
seem in modern times especially in the West

167
00:10:28,500 --> 00:10:30,500
have problems with paying respect

168
00:10:30,500 --> 00:10:31,500
in general.

169
00:10:31,500 --> 00:10:33,500
This idea that anyone should put their head

170
00:10:33,500 --> 00:10:35,500
at anyone else's feet.

171
00:10:35,500 --> 00:10:40,500
Why should we hold someone else above ourselves?

172
00:10:43,500 --> 00:10:46,500
I don't know that the Buddha particularly

173
00:10:46,500 --> 00:10:48,500
pushed this sort of thing.

174
00:10:48,500 --> 00:10:50,500
I mean, he did talk about how

175
00:10:50,500 --> 00:10:53,500
it's a good thing to be humble

176
00:10:53,500 --> 00:10:55,500
and to be respectful

177
00:10:55,500 --> 00:10:57,500
and he seems to have followed it

178
00:10:57,500 --> 00:11:00,500
but it doesn't seem to be a huge deal.

179
00:11:00,500 --> 00:11:03,500
He would often, it's sort of one of those things

180
00:11:03,500 --> 00:11:06,500
that the Buddha would more often

181
00:11:06,500 --> 00:11:09,500
do this kind of thing and say, why wouldn't he?

182
00:11:09,500 --> 00:11:12,500
What's to be surprised about?

183
00:11:12,500 --> 00:11:14,500
Because here's someone who has

184
00:11:14,500 --> 00:11:17,500
tamed their mind, who has become

185
00:11:17,500 --> 00:11:20,500
free from all Asava,

186
00:11:20,500 --> 00:11:22,500
who is a mind that is fixed

187
00:11:22,500 --> 00:11:25,500
and focused and well bent

188
00:11:25,500 --> 00:11:28,500
on good things.

189
00:11:28,500 --> 00:11:32,500
Someone whose faculties are tranquilized.

190
00:11:32,500 --> 00:11:35,500
Yes, in Rihanna it's a Matangatani.

191
00:11:35,500 --> 00:11:41,500
It's a nice phrase.

192
00:11:41,500 --> 00:11:45,500
So what does this have to do with us?

193
00:11:45,500 --> 00:11:46,500
Well, this is the sort of thing

194
00:11:46,500 --> 00:11:48,500
we wish to emulate.

195
00:11:48,500 --> 00:11:50,500
We don't have to be our hands ourselves

196
00:11:50,500 --> 00:11:51,500
not today.

197
00:11:51,500 --> 00:11:53,500
That's the ultimate goal.

198
00:11:53,500 --> 00:11:57,500
But even though we aren't our hands,

199
00:11:57,500 --> 00:11:59,500
we can emulate them.

200
00:11:59,500 --> 00:12:00,500
My teacher always said,

201
00:12:00,500 --> 00:12:02,500
Ian, use this word, Ian.

202
00:12:02,500 --> 00:12:03,500
It's a tie word.

203
00:12:03,500 --> 00:12:05,500
When we have certain qualities,

204
00:12:05,500 --> 00:12:06,500
we are like them.

205
00:12:06,500 --> 00:12:07,500
We emulate them.

206
00:12:07,500 --> 00:12:09,500
Ian.

207
00:12:09,500 --> 00:12:12,500
We emulate the Arahats.

208
00:12:12,500 --> 00:12:13,500
Ian Prahan.

209
00:12:13,500 --> 00:12:16,500
We emulate the Arahats.

210
00:12:16,500 --> 00:12:19,500
Yes, in Rihanna it's a Matangatani.

211
00:12:19,500 --> 00:12:22,500
Because that's an important quality to emulate.

212
00:12:22,500 --> 00:12:24,500
It's something you should affect

213
00:12:24,500 --> 00:12:25,500
in a sense.

214
00:12:25,500 --> 00:12:28,500
It can be dangerous

215
00:12:28,500 --> 00:12:30,500
because it can just be a pretense

216
00:12:30,500 --> 00:12:32,500
where you just appear to be,

217
00:12:32,500 --> 00:12:35,500
you know, people can appear to be very calm and tranquil,

218
00:12:35,500 --> 00:12:39,500
but inside their minds are a raging mess.

219
00:12:39,500 --> 00:12:41,500
But guarding your faculties

220
00:12:41,500 --> 00:12:43,500
is a huge part of morality.

221
00:12:43,500 --> 00:12:47,500
It's something that protects your fragile mind

222
00:12:47,500 --> 00:12:50,500
as you cultivate it, as you're developing it.

223
00:12:50,500 --> 00:12:52,500
So when monks go into the city,

224
00:12:52,500 --> 00:12:55,500
they're careful to look down,

225
00:12:55,500 --> 00:12:56,500
look at the floor,

226
00:12:56,500 --> 00:12:58,500
not get caught up in the world around them,

227
00:12:58,500 --> 00:13:00,500
because it's too easy to lose track

228
00:13:00,500 --> 00:13:02,500
of your meditation subject.

229
00:13:02,500 --> 00:13:06,500
And so for all of us,

230
00:13:06,500 --> 00:13:08,500
this is a great reminder

231
00:13:08,500 --> 00:13:12,500
on even a superficial level

232
00:13:12,500 --> 00:13:14,500
to guard our faculties.

233
00:13:14,500 --> 00:13:19,500
It's also sort of a way of

234
00:13:19,500 --> 00:13:21,500
gauging our practice.

235
00:13:21,500 --> 00:13:24,500
How tranquil are our faculties?

236
00:13:24,500 --> 00:13:26,500
When we see something with the eye,

237
00:13:26,500 --> 00:13:29,500
are we immediately incensed with lust and desire

238
00:13:29,500 --> 00:13:32,500
or anger and aversion?

239
00:13:32,500 --> 00:13:34,500
When we hear sounds,

240
00:13:34,500 --> 00:13:35,500
when we smell smells,

241
00:13:35,500 --> 00:13:38,500
are we immediately disgusted or attracted?

242
00:13:38,500 --> 00:13:40,500
When we have tastes,

243
00:13:40,500 --> 00:13:44,500
are we partial obsessed with good food

244
00:13:44,500 --> 00:13:47,500
and good tastes?

245
00:13:47,500 --> 00:13:50,500
This is a great way to judge your practice,

246
00:13:50,500 --> 00:13:52,500
based on the faculties.

247
00:13:52,500 --> 00:13:54,500
We often miss this one,

248
00:13:54,500 --> 00:13:55,500
the senses.

249
00:13:55,500 --> 00:13:57,500
We miss how fundamental the senses are

250
00:13:57,500 --> 00:13:59,500
to experience.

251
00:13:59,500 --> 00:14:00,500
So with food,

252
00:14:00,500 --> 00:14:02,500
we might be clear on the chewing part,

253
00:14:02,500 --> 00:14:03,500
right?

254
00:14:03,500 --> 00:14:04,500
Okay, I can do eating meditation,

255
00:14:04,500 --> 00:14:05,500
chewing, chewing,

256
00:14:05,500 --> 00:14:06,500
swallowing.

257
00:14:06,500 --> 00:14:08,500
But we're not clear on the taste,

258
00:14:08,500 --> 00:14:12,500
and we miss the fact that

259
00:14:12,500 --> 00:14:15,500
that taste plays such an important role

260
00:14:15,500 --> 00:14:18,500
in enlightenment, really.

261
00:14:18,500 --> 00:14:20,500
People wonder there was a question of,

262
00:14:20,500 --> 00:14:22,500
in regards to food,

263
00:14:22,500 --> 00:14:24,500
how should we overcome our desire for food?

264
00:14:24,500 --> 00:14:25,500
Here it is.

265
00:14:25,500 --> 00:14:27,500
It's what's going on.

266
00:14:27,500 --> 00:14:29,500
You understand what's happening.

267
00:14:29,500 --> 00:14:30,500
That's how you deal with it,

268
00:14:30,500 --> 00:14:32,500
how you overcome it.

269
00:14:32,500 --> 00:14:36,500
You see the process of tasting something

270
00:14:36,500 --> 00:14:39,500
and being incensed with

271
00:14:39,500 --> 00:14:42,500
desire, less sensual lust for that taste,

272
00:14:42,500 --> 00:14:46,500
and you deal with anything to get that taste.

273
00:14:46,500 --> 00:14:49,500
Even just the thought of that taste

274
00:14:49,500 --> 00:14:51,500
inflames the mind.

275
00:14:51,500 --> 00:14:53,500
This is the Buddha said,

276
00:14:53,500 --> 00:14:54,500
hehe,

277
00:14:54,500 --> 00:14:58,500
the tongue is on fire

278
00:14:58,500 --> 00:14:59,500
gun,

279
00:14:59,500 --> 00:15:00,500
gun,

280
00:15:00,500 --> 00:15:01,500
gun,

281
00:15:01,500 --> 00:15:02,500
gun,

282
00:15:02,500 --> 00:15:03,500
no.

283
00:15:03,500 --> 00:15:05,500
It's the pooling.

284
00:15:05,500 --> 00:15:06,500
Thaku,

285
00:15:06,500 --> 00:15:07,500
Thaku,

286
00:15:07,500 --> 00:15:08,500
adi,

287
00:15:08,500 --> 00:15:09,500
Thaku,

288
00:15:09,500 --> 00:15:10,500
adi,

289
00:15:10,500 --> 00:15:11,500
tongue,

290
00:15:11,500 --> 00:15:13,500
the grammar I can't remember.

291
00:15:13,500 --> 00:15:15,500
The eye is on fire,

292
00:15:15,500 --> 00:15:17,500
the ears on fire.

293
00:15:17,500 --> 00:15:18,500
On fire with greed,

294
00:15:18,500 --> 00:15:24,500
Greed on fire with anger, on fire with passion, on fire with anger, on fire with delusion.

295
00:15:25,940 --> 00:15:28,020
And delusion is like arrogance, conceit.

296
00:15:30,980 --> 00:15:36,340
I like, you know, it's conceit just to say, I like this, or I prefer onions, I prefer carrots,

297
00:15:36,340 --> 00:15:45,860
I prefer salty food, sweet food. It's eagle, because we get very kind of proud in the sense,

298
00:15:45,860 --> 00:15:52,900
it's proud of having a preference. It's kind of like expressing our preference has some meaning.

299
00:15:54,500 --> 00:16:00,500
Look at someone says, oh, I'm partial to this, it's almost as though it's an important part

300
00:16:00,500 --> 00:16:09,300
of who they are, right? And Buddhist, just shake their heads. Who you are is not important.

301
00:16:09,300 --> 00:16:18,260
So, guarding the senses, but more tranquilizing the senses, so guarding is the first part,

302
00:16:18,260 --> 00:16:23,620
but this is talking about some atangatani, having gone to tranquility means putting out the fires.

303
00:16:23,620 --> 00:16:29,300
It's really a reference to that, the idea of quenching the fires, cooling the senses,

304
00:16:30,020 --> 00:16:34,260
so that seeing is just seeing a huge part of the Buddhist teaching. You remember the teaching

305
00:16:34,260 --> 00:16:40,500
to Bahia, deep-tay, deep-tay, deep-tamatangbavisati, that seeing, just be seeing, is the goal.

306
00:16:41,780 --> 00:16:46,900
Really, and very hard for a non-meditator to understand, for a newcomer to Buddhism,

307
00:16:47,780 --> 00:16:53,860
seeing is just seeing, well, isn't that all it is? It's the problem. That's what we think it is,

308
00:16:53,860 --> 00:16:59,140
and we don't realize it's on fire. Our eyes on fire, the ears on fire.

309
00:16:59,140 --> 00:17:06,820
So, this is the powerful teachings of the Buddha in regards to the senses. It's a very powerful,

310
00:17:06,820 --> 00:17:12,260
important, useful part of the Buddha's teaching. It's why the Buddha, again and again, talked about

311
00:17:12,260 --> 00:17:18,260
the senses, the Indraya, seeing, hearing, smelling, tasting, feeling, and thinking.

312
00:17:18,260 --> 00:17:26,340
So, asasaya tasaratina sudhanta, this is well-trained like a horse.

313
00:17:27,540 --> 00:17:35,140
Well, like, like, the meditator is the, not the horse, the meditator is the

314
00:17:36,180 --> 00:17:42,740
cart person, the person riding the cart sara ti, cherry it here, we often translate it as.

315
00:17:42,740 --> 00:17:53,780
So, you tame them like you tame a horse, and often the way you tame a horse is by tying it up,

316
00:17:53,780 --> 00:18:00,900
or fencing it up, or getting on its back, and letting it wear itself out. Just wearing it

317
00:18:00,900 --> 00:18:06,980
itself out. You can't tie it down, or you can't beat it in the submission. You just have to

318
00:18:06,980 --> 00:18:13,460
just have to usually, I think, just have to outlast it, and show it that you've got more patience

319
00:18:13,460 --> 00:18:20,180
than it does. So, it might buck and jump and run around, but eventually it will get tired.

320
00:18:21,620 --> 00:18:26,820
The mind is sort of the same. So, it's a fairly good analogy, because eventually the mind starts

321
00:18:26,820 --> 00:18:35,620
to notice that its behavior is hurting itself. If you're objective and alert, you start to see

322
00:18:35,620 --> 00:18:40,420
that you're causing yourself suffering. Oh yeah, this, this obsession with sights and sounds and

323
00:18:40,420 --> 00:18:47,620
smells and tastes like a ping pong match. Everything I'm just reacting and bouncing back and forth,

324
00:18:47,620 --> 00:18:57,460
like a ping pong ball. So, you slowly overcome that, and your senses calm down. Your mind becomes

325
00:18:57,460 --> 00:19:07,380
pahina, pahina means scent, or like bent in the right direction, or focused or fixed in the right

326
00:19:07,380 --> 00:19:16,820
direction. Anasa, why we had that yesterday, right? Without tanes, without the outflowing or

327
00:19:18,020 --> 00:19:23,540
without streams of defilements, without any connection, attachment to the world.

328
00:19:23,540 --> 00:19:29,860
Such a person, his dear, even to angel. So, we've got a lot of, well, throughout this, we've had

329
00:19:29,860 --> 00:19:35,060
a lot of talk about angels, and I think that turns a lot of people off the Dhamapada stories. That's

330
00:19:35,060 --> 00:19:40,980
fine. It's not really important whether Sakha actually came down and paid respect to Mahaka

331
00:19:40,980 --> 00:19:45,460
Jayana. The story is not really that important. They're kind of fun to read, and they often

332
00:19:45,460 --> 00:19:55,140
have at least a good moral to them, and they present an interesting framework. So, if you want,

333
00:19:55,140 --> 00:19:59,060
if you don't want to believe them, that's fine. If you want to scoff and say angels,

334
00:19:59,060 --> 00:20:05,860
who's ever seen an angel? I've never seen an angel. Then fine, that's fine. I like the stories.

335
00:20:05,860 --> 00:20:10,020
I think a lot of people do, and if you just put that aside, it's not important.

336
00:20:10,020 --> 00:20:13,860
What's important is whether the teaching is sound, and we can argue and debate about that.

337
00:20:13,860 --> 00:20:18,580
And so, some people want to argue and debate as to whether paying respect is a good thing,

338
00:20:19,780 --> 00:20:25,620
in Buddhism it is, because it leads to humility, humility is a good thing, not being proud,

339
00:20:26,660 --> 00:20:31,060
it leads to gratitude, a sense of gratitude when other people have done something for you,

340
00:20:31,060 --> 00:20:37,860
who have lifted you up. They've done something for you that you couldn't do for them.

341
00:20:37,860 --> 00:20:43,540
So, you respect them for that, and you honor and revere them for that.

342
00:20:44,580 --> 00:20:52,260
And that's fairly standard. Not just in Buddhism, not just in religion, but in amongst people who

343
00:20:52,260 --> 00:20:58,820
have this sort of sense. I think having been in a society that values that sort of thing,

344
00:20:58,820 --> 00:21:03,220
it changes you, and having been involved in Buddhism, it changes you. I think people have it in the

345
00:21:03,220 --> 00:21:08,340
west, they've kind of lost it. You know, the whole apple for the teacher thing, you know, it became

346
00:21:08,340 --> 00:21:12,500
eventually people started to scoff and say it was the people who were buttering up the teacher,

347
00:21:12,500 --> 00:21:24,980
the brown noses, that's an awful thing, right? But, you know, our Latin professor,

348
00:21:25,860 --> 00:21:31,300
his desk, the guy before us, uses the chalkboard, old school, mathematician guy,

349
00:21:31,300 --> 00:21:39,140
and he leaves the eraser on the desk. And there's a huge amount of chalk. He's just kind of

350
00:21:39,140 --> 00:21:44,100
ignorant about it. And there's a huge amount of chalk left on the desk for our art teacher who uses

351
00:21:44,100 --> 00:21:49,620
a MacBook that then gets all clogged up. And so, before his thing, he does when he comes in,

352
00:21:49,620 --> 00:21:54,420
and he blows the dust off and tries to get rid of as much dust as he can. And I've kept

353
00:21:54,420 --> 00:21:59,620
meaning to bring in a cloth to wipe it down for him beforehand. And finally, today, yesterday,

354
00:21:59,620 --> 00:22:08,580
I wiped it down for him before he got there. This was some toilet paper because I'd

355
00:22:08,580 --> 00:22:14,900
forgot to bring a cloth again. But it's just a feeling like that's a good thing to do because

356
00:22:14,900 --> 00:22:20,740
this is a person who I respect and who is doing a good thing for us. And as a student,

357
00:22:20,740 --> 00:22:27,540
I just feel moved to make his job easier. It helps me as well, but it's just a, you know,

358
00:22:27,540 --> 00:22:34,100
like respect and gratitude. But we can argue about that. And that's a valid point of argument.

359
00:22:34,100 --> 00:22:38,980
We shouldn't argue about whether Sakka came down and paid respect to Mahaka Jayana.

360
00:22:40,420 --> 00:22:45,540
What else can we argue about? We can argue about whether these monks were good to criticize

361
00:22:45,540 --> 00:22:49,780
or whether criticism is anything good. And I think we have to have a little bit of a discussion

362
00:22:49,780 --> 00:22:56,500
about criticism, but I'll save that for the live, the rest of the live session. So that's all

363
00:22:56,500 --> 00:23:09,460
for the Dhamapada for today. Thank you for tuning in. Keep practicing and wishing you all the best.

