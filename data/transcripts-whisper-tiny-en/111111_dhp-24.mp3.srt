1
00:00:00,000 --> 00:00:04,080
Okay, so today we continue to do a little bit of a comment.

2
00:00:04,080 --> 00:00:09,460
We want to verse number 24.

3
00:00:09,460 --> 00:00:12,460
Which goes as well.

4
00:00:12,460 --> 00:00:32,200
When I start again I need a number 24 to forget, I like, hang on.

5
00:00:32,200 --> 00:00:45,360
And that is the only one that is the one that is the one that is the one who is effort

6
00:00:45,360 --> 00:00:46,360
for.

7
00:00:46,360 --> 00:00:56,600
It puts forth effort, set the email to who is mindful, sujika masa, with pure actions, with

8
00:00:56,600 --> 00:01:07,320
pure acts, nisamakar, you know, one who acts conscientiously, or considering, sanya tasa,

9
00:01:07,320 --> 00:01:17,720
one who is subdued, indamati, we know, who lives by the dhamma, apamatasa, one who is apamat,

10
00:01:17,720 --> 00:01:33,320
we are vigilant, yasomi wa dakti, for such a one, glory ever increases, or greatly increases,

11
00:01:33,320 --> 00:01:41,840
constantly increases, and the glory of such a person who is always things.

12
00:01:41,840 --> 00:01:47,040
Now this story was told in regards to a, well it is a fairly ordinary story, it actually

13
00:01:47,040 --> 00:01:54,600
doesn't have much to do with meditation, but it is another example of how the story shouldn't

14
00:01:54,600 --> 00:02:01,280
set the context, or shouldn't eclipse the meaning of the verse, shouldn't define the

15
00:02:01,280 --> 00:02:02,280
meaning of the verse.

16
00:02:02,280 --> 00:02:06,880
It does set the context, and it helps us to understand the reason why the Buddha gave

17
00:02:06,880 --> 00:02:07,880
the teaching.

18
00:02:07,880 --> 00:02:12,880
But as I have said before, the Buddha, in giving the teaching, wasn't trying to give

19
00:02:12,880 --> 00:02:19,520
a teaching specifically about this instance, he was taking the example, and creating

20
00:02:19,520 --> 00:02:25,120
a rule out of it, which as you can tell from the verse actually does very much relate

21
00:02:25,120 --> 00:02:30,400
to the Buddha's teaching and the practice, so we have to, we have to be able to separate

22
00:02:30,400 --> 00:02:33,920
the story out, but anyway, we will tell the story.

23
00:02:33,920 --> 00:02:40,360
Story angle is that there was the sun of a very rich couple, and again it is a story of

24
00:02:40,360 --> 00:02:47,000
a flame, and the parents were, were afflicted with the plague, and so they told their

25
00:02:47,000 --> 00:02:51,400
son to, to run away, and they told them where to find all of their treasure that they

26
00:02:51,400 --> 00:02:56,320
had hidden, and said when you're older, come back and retrieve your treasure, retrieve

27
00:02:56,320 --> 00:03:01,200
all of our wealth, because they hid it away and buried it or so on.

28
00:03:01,200 --> 00:03:09,000
And so greatly distressed and crying, he left us a boy, and lived abroad for some time before

29
00:03:09,000 --> 00:03:13,040
coming back, 12 years later I think it says.

30
00:03:13,040 --> 00:03:17,680
When he came back, he had grown up and had the beard and so on, and so nobody recognized

31
00:03:17,680 --> 00:03:23,080
him, and he went, he wears parents and said the treasure was buried, and he found it, and

32
00:03:23,080 --> 00:03:30,960
saw that it was there, but he thought to himself, no one recognizes me, and I'm, I have

33
00:03:30,960 --> 00:03:34,400
no station and have no, no other wealth.

34
00:03:34,400 --> 00:03:38,320
If I were to take this wealth and, and start to live by it, no one would believe it was

35
00:03:38,320 --> 00:03:44,200
mine, and they would, they would capture me and think I'd stolen, and put me in jail.

36
00:03:44,200 --> 00:03:48,840
So he buried the treasure again, covered it up, and instead of taking it, he didn't take

37
00:03:48,840 --> 00:03:53,000
even a piece of it, because it would have been gold or so on, very valuable, instead

38
00:03:53,000 --> 00:03:57,680
he went to live his life as an ordinary, as an ordinary worker.

39
00:03:57,680 --> 00:04:03,800
He found a job as a manager, getting people to work, and telling people what to do in

40
00:04:03,800 --> 00:04:10,320
organizing labor, and I guess he figured that eventually he would slowly, once he had got

41
00:04:10,320 --> 00:04:14,200
some of his own money, he would slowly bring that money in, but he didn't dare to take

42
00:04:14,200 --> 00:04:19,760
it, and so he lived quite conscientiously, and this was in Rajika, that he was living

43
00:04:19,760 --> 00:04:24,680
in Rajika, had a king of Rajika, was a Sotapana, was a follower of the Buddha, who

44
00:04:24,680 --> 00:04:33,760
practiced the Buddha's teaching and become a Sotapana, and he also had some fairly special

45
00:04:33,760 --> 00:04:34,760
intuition of his own.

46
00:04:34,760 --> 00:04:39,160
He was able to tell the station of any man whose voice he heard, and it may have been

47
00:04:39,160 --> 00:04:50,720
because of his training as a king, but one day he heard this, this, which is in Kumba, Kumba,

48
00:04:50,720 --> 00:05:05,400
Kumba, Kumba, Kumba, Kumba, Kumba, Kumba, Kumba, Kumba, Kumba, Kumba, Kumba.

49
00:05:05,400 --> 00:05:07,280
One who has a voice, a voice of wealth, Kumba, or voice of buried treasure, I think it

50
00:05:07,280 --> 00:05:16,400
means actually, the sound of the buried treasure, the sound of hidden treasure, and so he

51
00:05:16,400 --> 00:05:23,400
heard this, and he heard the sound of this man, and he said, now there's a man who has

52
00:05:23,400 --> 00:05:29,080
great wealth, and one of the servants who was standing there, she went and looked and saw

53
00:05:29,080 --> 00:05:33,560
that it was just an ordinary man who was actually quite poor, and she thought, well,

54
00:05:33,560 --> 00:05:37,320
kings don't just say ordinary things, so she sent someone to find out about him, and

55
00:05:37,320 --> 00:05:45,120
turns out that Yahi is living in a poor and a small hut alone, with absolutely no wealth,

56
00:05:45,120 --> 00:05:48,720
and so she came back and told the king and the king said nothing, and yet the next time

57
00:05:48,720 --> 00:05:54,440
the king heard his voice, he said the same thing, he said, man, that's a man of great

58
00:05:54,440 --> 00:06:01,120
wealth, like more than, more than ordinary, and so that this slave couldn't understand

59
00:06:01,120 --> 00:06:07,080
and the servant sent a man to check, and again and again found out that there was nothing

60
00:06:07,080 --> 00:06:14,440
to, there was, he had no wealth in his own, and appeared that he had no wealth, and

61
00:06:14,440 --> 00:06:18,960
so this servant devised a trick, she took her daughter, and they went and she got her daughter

62
00:06:18,960 --> 00:06:24,360
to, they went and lived with him, pretending to be poor people, and got her daughter to

63
00:06:24,360 --> 00:06:33,560
become married to him, got him seduced by his, by her daughter, and ended up living together,

64
00:06:33,560 --> 00:06:39,680
and then she sent back to the king and told the king to order everyone to have a festival,

65
00:06:39,680 --> 00:06:45,760
but there was to be a festival, and everyone had to decorate, and they were part of their

66
00:06:45,760 --> 00:06:50,160
home, they had to be decorated, and anyone who didn't decorate would be, decorate would

67
00:06:50,160 --> 00:06:55,200
be punished, this was the king's order, of course kings can order just about anything.

68
00:06:55,200 --> 00:07:00,360
So this woman, she said, oh, my son, we need money in here, and he said, I have no money,

69
00:07:00,360 --> 00:07:01,360
what are we going to do?

70
00:07:01,360 --> 00:07:06,000
And she said, well, we can go borrow some, find some way to get money, and she kept asking.

71
00:07:06,000 --> 00:07:10,400
So he went back to where the treasure was, he was so pestered by her, and he didn't

72
00:07:10,400 --> 00:07:13,760
know what to do, he went back to the treasure and took one gold coin out, and he gave

73
00:07:13,760 --> 00:07:16,840
it, he brought it back and gave it.

74
00:07:16,840 --> 00:07:22,800
She took that gold coin, looked at it, it was some kind of older issue coin, and sent it

75
00:07:22,800 --> 00:07:29,840
off to the king, and used her own money as a royal servant to pay for the decorations, and

76
00:07:29,840 --> 00:07:33,760
again, she had to, later on, she had the king do the same thing again, and she pestered

77
00:07:33,760 --> 00:07:39,840
and again they get money for the decorations, and he went and got three, got, it says three

78
00:07:39,840 --> 00:07:43,880
more, but I don't think it's three more, because in total the royal three, anyway, in total

79
00:07:43,880 --> 00:07:49,840
I think there were three gold coins that she sent off to the king.

80
00:07:49,840 --> 00:07:54,480
Then she went, then she then, she went and told the king that he must have money, and

81
00:07:54,480 --> 00:07:58,200
there's something, the king can do what he wants, and so the king sent some men to bring

82
00:07:58,200 --> 00:08:02,600
him to the castle, and they tied him up and they run the castle and the king sent, what

83
00:08:02,600 --> 00:08:07,960
are you doing hiding your, all your wealth from, from, from the kingdom, because normally

84
00:08:07,960 --> 00:08:12,560
you'd have to pay taxes, I guess, and he said I have no wealth, where would I want

85
00:08:12,560 --> 00:08:17,800
to have wealth, I'm just an ordinary, I'm working, and as a day later, and the king pulled

86
00:08:17,800 --> 00:08:23,040
out the three gold coins and said then who are these, who are these, and he sent, and

87
00:08:23,040 --> 00:08:26,200
he knew that the king was up, and he looked and he saw the servants, the royal servants

88
00:08:26,200 --> 00:08:30,800
had gone back, the mother and the daughter, and he knew that he was in trouble, and so

89
00:08:30,800 --> 00:08:34,760
he told the king and the king said how much money do you have and he sent as much, and

90
00:08:34,760 --> 00:08:42,240
the king sent a bunch of carriages, got the money, and I see the thing was that they understood

91
00:08:42,240 --> 00:08:47,600
that it was, he told the whole story of how he had been a rich people's son and they had

92
00:08:47,600 --> 00:08:53,720
died and left the treasure to him and he didn't want to take it, and the new brod was

93
00:08:53,720 --> 00:08:57,360
gold, it was amazing that this man, because you got to think that during that whole time

94
00:08:57,360 --> 00:09:01,400
it's amazing that the greed didn't overcome, greed didn't overcome him, and he was able

95
00:09:01,400 --> 00:09:09,640
to live after being a rich, rich family son, and he was able to live as a poor man, and

96
00:09:09,640 --> 00:09:15,480
so the king was quite impressed, and he made him a royal treasure, a banker, whatever,

97
00:09:15,480 --> 00:09:20,040
and gave him his own daughter in marriage, or maybe this woman's daughter, it's not quite

98
00:09:20,040 --> 00:09:25,080
clear, and took him to see the Buddha, and said,

99
00:09:25,080 --> 00:09:31,520
Monday, look at this man, the likes of him has never been seen before, even though he

100
00:09:31,520 --> 00:09:37,640
had so much money, he was never tempted to go and take it, and he was content to live

101
00:09:37,640 --> 00:09:45,800
and to work as an ordinary laborer for his life, and this is the story, so the Buddha's

102
00:09:45,800 --> 00:09:54,640
reply was these two verses, showing how a person who acts properly, a person who is

103
00:09:54,640 --> 00:10:01,560
conscientious, a person who is mindful, a person who knows the consequences of one's actions,

104
00:10:01,560 --> 00:10:08,400
for them glory increases, so by the sounds of it it's actually quite a simple and mundane

105
00:10:08,400 --> 00:10:15,320
story, and it might actually seem to be encouraging us to live, to save up our wealth

106
00:10:15,320 --> 00:10:19,880
and so on, and for ordinary people it can be seen in that way, and people living in the

107
00:10:19,880 --> 00:10:25,080
world can see the home, when we work we shouldn't become greedy and become attached to our

108
00:10:25,080 --> 00:10:28,960
wealth, we should still work hard, even though we're wealthy and we should be conscientious,

109
00:10:28,960 --> 00:10:33,960
and so many things here that are good in the world, Uthano, people should living in the

110
00:10:33,960 --> 00:10:39,720
world should be hardworking, septima, of course they have to be mindful, when they

111
00:10:39,720 --> 00:10:45,560
do things they have to do it conscientiously, let's need some God, you know they have to

112
00:10:45,560 --> 00:10:52,840
be conscientious and do things methodically, and do the right things, suchi kamata kamasa,

113
00:10:52,840 --> 00:10:58,120
their actions should be pure, so they shouldn't be treacherous and so on, and they should

114
00:10:58,120 --> 00:11:02,440
make their livelihood by pure means, they're supposed to nowadays you see people when

115
00:11:02,440 --> 00:11:09,120
they have mass lots of money, they can become very corrupted and stingy and so many things

116
00:11:09,120 --> 00:11:15,000
and as a result now you see so much discord in the world over these rich people who refuse

117
00:11:15,000 --> 00:11:24,880
to support or who have basically cornered so much of the wealth in shady and underhanded

118
00:11:24,880 --> 00:11:31,720
ways by cheating basically, by corrupting politicians, by being politicians and deregulation

119
00:11:31,720 --> 00:11:39,400
and so on and all of this, so this is what is called suchi kamata, with us suchi kamasa,

120
00:11:39,400 --> 00:11:45,960
pure deeds, so the Buddha said a person with fear deeds, this is how true glory comes about,

121
00:11:45,960 --> 00:11:53,680
Sunyatasa, a person who is subdued, this means a person who isn't arrogant and isn't

122
00:11:53,680 --> 00:12:03,240
a rash, so who acts in a mindful, really in a mindful way that person isn't addicted

123
00:12:03,240 --> 00:12:08,840
to the sensuality, always looking for new pleasures and so on, and Dhamadhi we know

124
00:12:08,840 --> 00:12:13,040
living by the Dhamma, so of course living by the Buddha's teaching, living by the truth

125
00:12:13,040 --> 00:12:20,120
you could say, living truthfully and living according to goodness and righteousness,

126
00:12:20,120 --> 00:12:26,160
upamatasa, the person who overall and really all of these things are a description of

127
00:12:26,160 --> 00:12:33,560
upamata of heedfulness, the person who is heedful and always unguired, for such a person

128
00:12:33,560 --> 00:12:38,480
their glory increases, yes I guess it can just mean fame or like in glory I think

129
00:12:38,480 --> 00:12:44,520
of a good translation and it generally can mean simply worldly glory, but what we have

130
00:12:44,520 --> 00:12:51,280
to do here is think of who the Buddha was and what he was bringing to the world, and obviously

131
00:12:51,280 --> 00:12:55,720
he wasn't bringing people the teaching, yes you should become bankers and treasures and

132
00:12:55,720 --> 00:13:01,520
work hard to get the king's favor, so this obviously wasn't really the same, and in fact

133
00:13:01,520 --> 00:13:06,560
if you look at it and you think about it and you think about who has the true glory of

134
00:13:06,560 --> 00:13:11,640
all the people in India, of all people who have come out of India, I can't think of

135
00:13:11,640 --> 00:13:17,160
one person in the history of India who has more glory than the Buddha, the second might

136
00:13:17,160 --> 00:13:23,600
be Gandhi who came much later of course, but you'd have to think that of these two, there's

137
00:13:23,600 --> 00:13:29,600
no question that in all of India of all the kings, even the King Bimbisara or King Ashoka

138
00:13:29,600 --> 00:13:35,520
who came later, none of them have even a part of the glory of the Buddha, so then you ask

139
00:13:35,520 --> 00:13:43,440
yourself who where does the true glory come from, and you can see that these virtues

140
00:13:43,440 --> 00:13:48,880
apply far more to someone like the Buddha or to even to a Buddhist meditator than they

141
00:13:48,880 --> 00:13:57,280
do to this banker who acted in a very noble sort of way in a mundane sense, but it was

142
00:13:57,280 --> 00:14:01,440
actually quite a mundane sense, he was still very much, seems to be addicted to sensuality

143
00:14:01,440 --> 00:14:08,320
falling for this woman's daughter, and if you read the story actually, he seems like

144
00:14:08,320 --> 00:14:13,720
a really nice guy to tell you the truth, when these women came to live in his home and

145
00:14:13,720 --> 00:14:18,960
they started, first what they did is they cut his bed so that when he sat on it it fell

146
00:14:18,960 --> 00:14:23,840
apart because there was only one other bed and that was where the daughter was sleeping

147
00:14:23,840 --> 00:14:28,440
and so you go and sleep with my daughter then, she calls, she said, oh son you go and sleep

148
00:14:28,440 --> 00:14:33,240
with your sister, and of course it didn't happen like that, it wasn't meant to happen

149
00:14:33,240 --> 00:14:38,040
like that, but what he said at the beginning was he said, you know you guys were too much

150
00:14:38,040 --> 00:14:42,320
for me, when I was alone I can come and go as I please, and now I've got all these

151
00:14:42,320 --> 00:14:48,080
troubles now that you've come, which is really the truth, as you take on a family and

152
00:14:48,080 --> 00:14:52,600
the mother-in-law and the family children and so on, so much headaches and problems

153
00:14:52,600 --> 00:15:01,160
come, which was what he was realizing, but he fell for it, so but anyway these qualities

154
00:15:01,160 --> 00:15:06,840
we have to remember the Buddha's not referring specifically to this example, he's using

155
00:15:06,840 --> 00:15:13,800
this example to show how this reality works and it works for all people, most especially

156
00:15:13,800 --> 00:15:19,400
for us in meditation, so what we should do is look at this person in terms of its

157
00:15:19,400 --> 00:15:23,600
meditative applications, and we can use this guy, we can actually use this whole story

158
00:15:23,600 --> 00:15:30,240
as kind of an allegory, we can think of ourselves as working hard here in meditation,

159
00:15:30,240 --> 00:15:37,080
and you're working here, and actually you don't have, you don't have much salary in

160
00:15:37,080 --> 00:15:41,040
the sense that you're practicing and it seems like sometimes you're not getting much

161
00:15:41,040 --> 00:15:45,800
and you're working really hard and it's quite painful, quite difficult, well at times

162
00:15:45,800 --> 00:15:52,120
it seems like you're all I'm getting is bug bites and not sleeping enough and no pleasure

163
00:15:52,120 --> 00:16:01,080
and no entertainment and so on, none of that, not even good food, so sometimes you

164
00:16:01,080 --> 00:16:06,240
think oh what's it all for, but what you're doing is you're developing all of these qualities

165
00:16:06,240 --> 00:16:11,840
and as a result you can look at what happens to someone with these qualities, this person

166
00:16:11,840 --> 00:16:17,920
eventually was recognized by a wise individual, a king and a powerful individual, so

167
00:16:17,920 --> 00:16:22,520
you become recognized by the world, because the Buddha became recognized by the world

168
00:16:22,520 --> 00:16:28,800
and your glory, and we're going to put aside the idea of glory in the world, but here

169
00:16:28,800 --> 00:16:34,120
in his case, glory in the world came to him, but for you glory in the Dharma comes, so

170
00:16:34,120 --> 00:16:39,120
you become in the universe and in fact that's really what it is, the whole of the universe

171
00:16:39,120 --> 00:16:46,800
becomes, or reacts to your greatness and so many people who practice meditation will

172
00:16:46,800 --> 00:16:52,360
go on to become angels will go on to heaven or that's according to the texts anyway,

173
00:16:52,360 --> 00:16:56,200
but even in this life you become a great teacher, you become someone who people listen

174
00:16:56,200 --> 00:17:01,240
to, someone who people respect, the people who you find yourself surrounded by are good

175
00:17:01,240 --> 00:17:06,560
people who respect what you have to say, who listen to you, who care for you, who support

176
00:17:06,560 --> 00:17:12,480
you and so on, because you have these things, so what are we doing in meditation to develop

177
00:17:12,480 --> 00:17:13,480
these?

178
00:17:13,480 --> 00:17:18,520
Well, the first one that we're developing is the Tana, the Tana, which means effort, so

179
00:17:18,520 --> 00:17:22,960
the first quality of meditation, this is what you gain from things like walking meditation

180
00:17:22,960 --> 00:17:28,000
or even just sitting, the effort to stay still, and the effort to stay with the object

181
00:17:28,000 --> 00:17:35,520
and not run away from it and not avoid reality, and just the effort, just that effort

182
00:17:35,520 --> 00:17:41,120
to not fall into liking or disliking, even the effort just to say to yourself, Mariah,

183
00:17:41,120 --> 00:17:47,200
singing, following, or to say to yourself, sifting, or seeing, or hearing, or pain, or

184
00:17:47,200 --> 00:17:54,680
aching, or so on, and just simply that effort, it has a profound effect on your mind and

185
00:17:54,680 --> 00:17:56,560
on your whole being.

186
00:17:56,560 --> 00:18:00,320
Some people think that practicing here you're not getting any exercise, but in fact you're

187
00:18:00,320 --> 00:18:05,840
getting an incredible exercise, and your whole being is becoming stronger, you'll find

188
00:18:05,840 --> 00:18:10,720
that when you leave even after doing the foundation course, that your mind is, all of

189
00:18:10,720 --> 00:18:14,840
the wavering in your mind, or much of the wavering in your mind is gone, and you become

190
00:18:14,840 --> 00:18:19,520
a more powerful individual, and this is something that people respect, that the world

191
00:18:19,520 --> 00:18:25,080
respects, that the whole universe reacts to, and of course internally you become much more

192
00:18:25,080 --> 00:18:33,840
peaceful, and much more at ease, at much more in tune with reality, so we have to be

193
00:18:33,840 --> 00:18:39,920
someone who develops mindfulness, so this is the practice that we're using the effort for.

194
00:18:39,920 --> 00:18:45,320
So the effort is to develop mindfulness, really, we have this effort and we're developing

195
00:18:45,320 --> 00:18:46,320
mindfulness.

196
00:18:46,320 --> 00:18:52,520
You put those two together, means having effort and being mindful, then you really have

197
00:18:52,520 --> 00:18:56,880
the essence of the practice, because concentration is something that comes by itself when

198
00:18:56,880 --> 00:19:02,160
you have these two, they surround concentration, if you want to develop right concentration,

199
00:19:02,160 --> 00:19:06,400
you have effort, you have directed effort, so it's not just working hard like this

200
00:19:06,400 --> 00:19:14,640
mended, but in terms of meditation, it's working hard to, working hard in a focused

201
00:19:14,640 --> 00:19:15,640
manner.

202
00:19:15,640 --> 00:19:19,640
What this man, of course, actually the truth is he was focused, right, he was working

203
00:19:19,640 --> 00:19:26,160
hard and he hadn't go on, he knew his way, and he knew the reasons for doing what he

204
00:19:26,160 --> 00:19:27,160
did.

205
00:19:27,160 --> 00:19:33,000
So in a sense he was mindful, but what we're doing is even more so knowing what we're

206
00:19:33,000 --> 00:19:37,880
doing and understanding the reason and understanding the practice, understanding everything

207
00:19:37,880 --> 00:19:45,200
we do, when you go to eat, for example, you understand wanting to eat and wanting to

208
00:19:45,200 --> 00:19:51,520
take, you understand the desires and so on, that make you eat too much, for example, so

209
00:19:51,520 --> 00:19:55,240
you understand cause and effect.

210
00:19:55,240 --> 00:20:00,680
So basically you have mindfulness and you're aware of things in an ultimate sense, you're

211
00:20:00,680 --> 00:20:05,880
aware of every movement, every experience, that when you're walking, walking, walking,

212
00:20:05,880 --> 00:20:13,960
walking, you know it is walking, when rising, when pain, you know it is pain, this is mindfulness.

213
00:20:13,960 --> 00:20:23,360
So you have pure actions, and so this might sound like a person, like a teaching for people

214
00:20:23,360 --> 00:20:28,200
living in the world, because it means you don't cheat others and you don't lie and

215
00:20:28,200 --> 00:20:35,200
don't steal, but it is on a much more profound level for someone who's practicing meditation,

216
00:20:35,200 --> 00:20:39,800
because when you're practicing meditation, everything you do becomes pure, or you're purifying

217
00:20:39,800 --> 00:20:43,920
everything you do, even eating, an ordinary person when they eat, they don't think about

218
00:20:43,920 --> 00:20:44,920
it.

219
00:20:44,920 --> 00:20:49,240
Is it wrong to eat, is it right to eat, is it silly question, of course, there's nothing

220
00:20:49,240 --> 00:20:50,240
wrong with eating, right?

221
00:20:50,240 --> 00:20:54,080
I bought this food, I made this food, and it's mine, I can eat it, so they just eat it

222
00:20:54,080 --> 00:20:59,480
and they think that's suchikama, suchikama, that's a pure karma.

223
00:20:59,480 --> 00:21:02,640
But as a meditator, we know that that's actually not the case, even though it's yours

224
00:21:02,640 --> 00:21:06,600
and you bought it and so on, it can still be an evil act, in the sense that you can still

225
00:21:06,600 --> 00:21:11,200
become addicted to it and evil, in the sense that it's going to create addiction and suffering

226
00:21:11,200 --> 00:21:12,560
for you.

227
00:21:12,560 --> 00:21:17,120
And just eating food, we can see how much we become addicted and we lose our whole mindfulness

228
00:21:17,120 --> 00:21:21,200
and forget what we're doing and our mind goes somewhere else, and maybe if it's bad food,

229
00:21:21,200 --> 00:21:27,640
we get upset and angry and don't like it, and we can see the evil for us in that, that

230
00:21:27,640 --> 00:21:33,880
it causes us suffering, and causes us to do all sorts of nasty things as well.

231
00:21:33,880 --> 00:21:37,360
So the purity that we're gaining is on a whole other level, the purity that you're gaining

232
00:21:37,360 --> 00:21:41,800
in your practice is something, it's exactly what the Buddha was referring to and we

233
00:21:41,800 --> 00:21:46,760
have to be careful not to miss that, that the purity of karma doesn't come from intellectually

234
00:21:46,760 --> 00:21:51,320
thinking this is right, this is wrong, or even in terms of just the five precepts, it's

235
00:21:51,320 --> 00:21:57,800
on a much more profound level, not doing anything with greed or anger or delusion.

236
00:21:57,800 --> 00:22:08,840
Nisamma, god, you know, the person who acts conscientiously or aware, knowing the right

237
00:22:08,840 --> 00:22:13,680
way of actually, so this means knowing what you're doing when you act, it's in the same

238
00:22:13,680 --> 00:22:19,360
sense of having doing the right thing because when you're mindful and you're aware clearly

239
00:22:19,360 --> 00:22:23,000
what you're doing, is this the right thing to do, am I doing it for the right reason

240
00:22:23,000 --> 00:22:25,440
you do it in the right way?

241
00:22:25,440 --> 00:22:32,360
So when you walk, normally when you walk, you're walking, you know, kind of mindlessly,

242
00:22:32,360 --> 00:22:40,040
and so as a result, you might trip them fall or find that your body becomes tense as a

243
00:22:40,040 --> 00:22:44,920
result of your actions, what you find in meditation is actually a lot of our tension,

244
00:22:44,920 --> 00:22:51,880
and all of our stress is a result of the way we act and the way we move our bodies.

245
00:22:51,880 --> 00:22:57,280
So you come to refine your movement, you recover, refine your actions, even our speech,

246
00:22:57,280 --> 00:23:04,080
and find that we're speaking in a more clear way and we're speaking less, we find ourselves

247
00:23:04,080 --> 00:23:09,440
not going on and on and on, the mindless things and the things that are unrelated to

248
00:23:09,440 --> 00:23:14,040
to the mental development and so on and find ourselves less interested in it, we see that

249
00:23:14,040 --> 00:23:19,080
it's causing the stress and suffering, so our actions and our speech and even our thoughts

250
00:23:19,080 --> 00:23:24,800
become much more clear and much more careful, you know, it's what this means, acting

251
00:23:24,800 --> 00:23:31,040
carefully, which comes directly from meditation.

252
00:23:31,040 --> 00:23:41,280
Sunyatasa means we have sunyatasa, we have developing this subdued nature or controlled,

253
00:23:41,280 --> 00:23:45,120
controlled in the sense that we're not letting ourselves go out of control, it doesn't

254
00:23:45,120 --> 00:23:49,400
mean you have to force yourself not to see things or hear things, it means when you see

255
00:23:49,400 --> 00:23:54,120
things, you're controlled in terms of just knowing it that it's seeing and not letting

256
00:23:54,120 --> 00:23:59,360
yourself get caught up and enjoying beautiful things or being angry at unpleasant things

257
00:23:59,360 --> 00:24:06,320
and so on, but falling into this partiality that brings us so much out of balance.

258
00:24:06,320 --> 00:24:12,040
Dhamma dii vinoa perceive a bit by the dhamma, so the wording is kind of in terms of having

259
00:24:12,040 --> 00:24:19,360
a life livelihood that is righteous and so it does accord with the story, but you can never

260
00:24:19,360 --> 00:24:25,320
compare the life of a banker to the life of a Buddhist meditative, for example, someone

261
00:24:25,320 --> 00:24:30,000
with a Buddha is really trying to talk about here and he's trying to explain to the king

262
00:24:30,000 --> 00:24:34,440
that it's not this person, what he did that's so great, it's his qualities in mind,

263
00:24:34,440 --> 00:24:39,680
which are, which are to be found so much more in the meditative and the person who is

264
00:24:39,680 --> 00:24:42,000
practicing meditation.

265
00:24:42,000 --> 00:24:47,200
So one who lives by the dhamma, one who lives righteously, well if you look at the way

266
00:24:47,200 --> 00:24:55,600
we're living in a way that is cultivating goodness and developing peace and tranquility

267
00:24:55,600 --> 00:25:01,160
and is avoiding so much of the, we're building a society here in a sense, even in just

268
00:25:01,160 --> 00:25:05,080
a monastery like this, you're building a society that has so much, you know, there's no

269
00:25:05,080 --> 00:25:10,880
weapons of war, right, there's no drugs, there's no violence, sometimes the villagers

270
00:25:10,880 --> 00:25:16,960
bringing their red stuff, red juice stuff, but trying to keep it out, there's no, there's so

271
00:25:16,960 --> 00:25:22,960
many, so much of society, there's no sensuality, no entertainment, none of this Hollywood

272
00:25:22,960 --> 00:25:29,280
addiction, addiction to sensuality or pleasure or entertainment, beautification, all of

273
00:25:29,280 --> 00:25:35,200
this, so much of the things that bring, that are bringing the world down and are bringing

274
00:25:35,200 --> 00:25:40,840
chaos to the world, we're doing away with, we're developing a way of life that is pure

275
00:25:40,840 --> 00:25:51,280
that is according to the truth and reality and harmony and peace, more over, we are

276
00:25:51,280 --> 00:25:57,360
developing this for the world and we're creating, we're spreading this teaching, so when

277
00:25:57,360 --> 00:26:02,520
people come, they're able to see this and they're able to take the example back and they're

278
00:26:02,520 --> 00:26:07,440
able to use teachings like this, for example, when you record the teachings, just our

279
00:26:07,440 --> 00:26:12,760
living by the dhamma and sitting you're talking about, is something that helps the world

280
00:26:12,760 --> 00:26:16,280
because we're spreading, because by talking about it, we let other people know, we're

281
00:26:16,280 --> 00:26:20,560
talking about other people know that I'm saying this and they're nothing, they'll think

282
00:26:20,560 --> 00:26:25,600
of this, this man who worked in the right way and they'll think, well that's, that's

283
00:26:25,600 --> 00:26:29,920
on a level that I can do with it and they'll live their lives conscientiously and according

284
00:26:29,920 --> 00:26:36,520
to the truth, according to goodness, they won't become greedy and self-serving and when

285
00:26:36,520 --> 00:26:42,920
they do have wealth, they won't become obsessed by it and in this way, in all of these

286
00:26:42,920 --> 00:26:50,600
ways, they will become a pamata, you know, become peaceful or vigilant, not language

287
00:26:50,600 --> 00:27:01,280
or not people, and as I said yesterday, really the meaning of heatfulness is mindfulness

288
00:27:01,280 --> 00:27:07,480
and the Buddha said there's at one point there's a quote, Satya, a repulasso, a pamata

289
00:27:07,480 --> 00:27:14,000
with a pamata, a woodshifty, a person who is never without mindfulness, this is the meaning

290
00:27:14,000 --> 00:27:20,120
of one who is heatful or not negligent, but when you put all of these things together you

291
00:27:20,120 --> 00:27:24,720
get an idea of what it means to be heatful, a person who is country and just a person

292
00:27:24,720 --> 00:27:30,720
who is some dude, most importantly a person who is mindful and full of effort, never giving

293
00:27:30,720 --> 00:27:37,480
up, never becoming lazy, never slacking off, and always trying and trying to better themselves,

294
00:27:37,480 --> 00:27:42,560
no matter how good they are as a person, always trying to make themselves a better person.

295
00:27:42,560 --> 00:27:48,360
This is a person who is a pamata, a pamata, and what happens for such a person, their glory

296
00:27:48,360 --> 00:27:53,320
ever increases, so he's not trying to say that the purpose, our purpose should be without

297
00:27:53,320 --> 00:28:00,680
glory, he's just observing that what this man has done by putting into practice these

298
00:28:00,680 --> 00:28:10,240
traits, the practice of these traits is what has brought this fruit, it is because of

299
00:28:10,240 --> 00:28:14,520
these things that this fruit has come, so he's explaining what is most important, which

300
00:28:14,520 --> 00:28:18,960
is these virtues, actually so many things, good things come from there, he's not trying

301
00:28:18,960 --> 00:28:26,480
to say the only good things that you become glorious, but glorious is most reserved for

302
00:28:26,480 --> 00:28:31,600
those who have these traits, people who work hard and work in righteousness, and do good

303
00:28:31,600 --> 00:28:36,240
things for the world, as we can see that the Buddha did and people like Gandhi or so, and

304
00:28:36,240 --> 00:28:42,000
we tried to help people, or said to have tried to do good things and bring peace and happiness

305
00:28:42,000 --> 00:28:52,960
in harmony and prosperity, and glory, in the sense of goodness and raising up people's

306
00:28:52,960 --> 00:29:00,120
consciousness and people's lives to happiness in peace, so this is what leads, this is

307
00:29:00,120 --> 00:29:06,040
the Buddha's teaching on what leads to Yasa, what leads to glory, that's verse number

308
00:29:06,040 --> 00:29:11,400
24, and that's the Damupanda teaching for today, with one from meditation as well, very

309
00:29:11,400 --> 00:29:26,840
good one, then one from the Apamanda Vaga, so thanks for tuning in and back to meditation.

