1
00:00:00,000 --> 00:00:06,480
I've allowed two people for a long time. I'm finding it hard to face the fear of

2
00:00:06,480 --> 00:00:12,280
possibly losing them. If I tell them the truth, but I'm not happy now knowing that I'm

3
00:00:12,280 --> 00:00:16,760
lying. What's the right thing to do?

4
00:00:18,680 --> 00:00:25,160
Well, you don't have to tell the truth. Just don't lie. There's a Zen story that

5
00:00:25,160 --> 00:00:34,240
goes if it's a, I don't know, Zen story, some Buddhist quote unquote Buddhist

6
00:00:34,240 --> 00:00:42,560
story, that goes, that tries to point out skillful means that sometimes you

7
00:00:42,560 --> 00:00:48,640
have to do an evil thing for a good purpose. And this monk once told us

8
00:00:48,640 --> 00:00:52,640
the story in the rasping about lying and he said, well, you know, sometimes you

9
00:00:52,640 --> 00:00:57,280
can do bad things for a good purpose. And it's obviously the right thing to

10
00:00:57,280 --> 00:01:01,040
do. So he said, suppose there's a hunter you're standing in the woods and you see a

11
00:01:01,040 --> 00:01:08,280
rabbit go by and it goes down one path and then the hunter comes along. And he

12
00:01:08,280 --> 00:01:14,560
asked you which way did the rabbit go? Well, obviously the right thing to do

13
00:01:14,560 --> 00:01:19,400
he said is to lie. Say do your mother rabbit went down that way. It's good for

14
00:01:19,400 --> 00:01:24,920
both of them. And you know, Sam Harris has this wonderful argument against

15
00:01:24,920 --> 00:01:28,920
that sort of thing. But I asked the monk, I said, what about just not saying

16
00:01:28,920 --> 00:01:36,400
anything? Sam Harris, he says this as well. He says, I wouldn't tell you if I

17
00:01:36,400 --> 00:01:44,720
knew kind of thing. And his point is that that opens up a dialogue. You might

18
00:01:44,720 --> 00:01:52,120
not say it rough like that. But you might say, well, I'm sorry. But you know,

19
00:01:52,120 --> 00:01:57,440
for me, this is a horrible thing you're doing. And so on and so on. You stick

20
00:01:57,440 --> 00:02:01,000
to your principles. It could happen that he then beats you up or even shoots you.

21
00:02:01,000 --> 00:02:13,600
Right? But this is this is your path. This is nothing. This is this is the way

22
00:02:13,600 --> 00:02:20,320
your life is going. You are in a position now that is very difficult. You have

23
00:02:20,320 --> 00:02:27,440
demands put on you and the potential to lose, I guess, to lose something. Right?

24
00:02:27,440 --> 00:02:31,360
Right. Yes, that's exactly what you say. If you're possibly losing them if I tell them

25
00:02:31,360 --> 00:02:41,880
the truth. And so you think the better course of action is to lie to them in

26
00:02:41,880 --> 00:02:48,600
order to keep them. So you have to ask yourself, which is more important in your

27
00:02:48,600 --> 00:02:56,600
peace of mind, your clarity of mind, your happiness, or keeping what you think

28
00:02:56,600 --> 00:03:04,720
is yours and what you cling to, what you want and hold on to. For a Buddhist, it's not

29
00:03:04,720 --> 00:03:09,200
a difficult. I mean, it's it's a painful decision to make, but it's not a difficult

30
00:03:09,200 --> 00:03:18,960
decision to make. There's, there's this very famous book called Les Misrab, the name

31
00:03:18,960 --> 00:03:27,640
is Rab, by this is name. There's some guy in France, Victor Hugo. Yes. And there's

32
00:03:27,640 --> 00:03:37,880
a wonderful point in this book where this man, if he doesn't tell the truth, someone

33
00:03:37,880 --> 00:03:48,040
innocent is going to be put in jail. If he does tell the truth, he's going to be put

34
00:03:48,040 --> 00:03:55,360
in jail. And many people who depend on him for happiness are going to suffer as a result,

35
00:03:55,360 --> 00:04:00,520
the whole town that he's he's the mayor of this town. And it's going to destroy the whole

36
00:04:00,520 --> 00:04:08,480
industry of this that he's built up in this town. And he's he's so racking his brain and

37
00:04:08,480 --> 00:04:14,080
there's this woman who depends on him as well. And he's totally he almost goes insane

38
00:04:14,080 --> 00:04:20,720
trying to answer this question. And you feel for him. And you in the end, you know, what

39
00:04:20,720 --> 00:04:29,560
is the right answer? But you think the author sets it up as the most difficult decision

40
00:04:29,560 --> 00:04:37,600
is obviously that has an obvious answer. And so this is how this is this is how we understand

41
00:04:37,600 --> 00:04:45,760
it. It's painful, but it's correct. And if it's correct, it's correct. This is a problem

42
00:04:45,760 --> 00:04:54,520
that we have in the world is that it's correct, but but I can't ridiculous, horrible.

43
00:04:54,520 --> 00:05:00,120
How can you say such a thing? How can you be so dishonest with yourself that we are? I'm not

44
00:05:00,120 --> 00:05:06,040
criticizing. How is it that we can be so how dare we be dishonest with ourselves when we know

45
00:05:06,040 --> 00:05:10,360
that something is the right thing to do? And yet don't do it. This is what we have to

46
00:05:10,360 --> 00:05:15,600
remind ourselves of because we do this. We say it's right, but I can't bring myself

47
00:05:15,600 --> 00:05:26,440
to do it. How dare we? How, not how dare we, but how absurd of us? How can we possibly hold

48
00:05:26,440 --> 00:05:33,160
such an absurd, a sort of opinion to not do the correct thing by any means for any reason.

49
00:05:33,160 --> 00:05:39,600
If it's correct, it's correct. And it should be that way no matter what. Sometimes you

50
00:05:39,600 --> 00:05:45,040
have to just hold your nose and dive in, plug your nose and dive in, hold your breath and

51
00:05:45,040 --> 00:05:51,800
dive in. You don't know where it's going to lead you, but you know it's correct. You know,

52
00:05:51,800 --> 00:05:57,200
at the very least, it's going to lead you to peace. It's going to lead you to happiness.

53
00:05:57,200 --> 00:06:04,320
It may destroy everything you've worked very hard to build up, but it's the correct thing

54
00:06:04,320 --> 00:06:11,960
to do. And that supersedes everything, supersedes all concerns. It's correct, you must

55
00:06:11,960 --> 00:06:18,960
do it, no matter how difficult it is.

