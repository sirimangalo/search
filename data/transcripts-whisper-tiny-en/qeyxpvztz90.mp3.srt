1
00:00:00,000 --> 00:00:08,000
My flight to Sri Lanka is only in a few days I'm experiencing wave after wave of strong excitement and fear.

2
00:00:08,000 --> 00:00:13,000
Any special recommendation regarding one's practice before a retreat?

3
00:00:13,000 --> 00:00:27,000
Well, the first one is with anything, be fairly, I don't know, I guess, a little bit strict with yourself.

4
00:00:27,000 --> 00:00:36,000
Reminding yourself that it's never good to live in the future.

5
00:00:36,000 --> 00:00:38,000
That's the best way to put it.

6
00:00:38,000 --> 00:00:43,000
The excitement that we have for things is not correct.

7
00:00:43,000 --> 00:00:46,000
It's not correct to be excited about things.

8
00:00:46,000 --> 00:00:47,000
It's not beneficial.

9
00:00:47,000 --> 00:00:50,000
It's cause for suffering for you.

10
00:00:50,000 --> 00:01:01,000
And it's a cause for you to actually ruin future experiences with expectations or with just excitement by itself.

11
00:01:01,000 --> 00:01:04,000
Because you bring that into the experience.

12
00:01:04,000 --> 00:01:05,000
You bring the fear.

13
00:01:05,000 --> 00:01:07,000
You bring the excitement into the experience.

14
00:01:07,000 --> 00:01:14,000
And so you want to start on the right foot by reminding yourself this is not correct.

15
00:01:14,000 --> 00:01:16,000
It's not proper to be.

16
00:01:16,000 --> 00:01:20,000
It is natural when there are expectations in the future.

17
00:01:20,000 --> 00:01:22,000
You have to go up on stage and give a talk.

18
00:01:22,000 --> 00:01:28,000
For example, a great fear comes up great excitement when you have performance or so on.

19
00:01:28,000 --> 00:01:30,000
Great fear and excitement.

20
00:01:30,000 --> 00:01:32,000
But it's not proper.

21
00:01:32,000 --> 00:01:34,000
And it's not helpful.

22
00:01:34,000 --> 00:01:38,000
It's detrimental.

23
00:01:38,000 --> 00:01:45,000
So in the problem with that, you have to be careful there is what I don't mean to say you have to remind yourself that this is harmful.

24
00:01:45,000 --> 00:01:47,000
Because that's dangerous as well.

25
00:01:47,000 --> 00:01:49,000
Then you get even more worried and more afraid.

26
00:01:49,000 --> 00:01:51,000
I'm going to ruin it or so on.

27
00:01:51,000 --> 00:01:53,000
You just have to be strict with yourself.

28
00:01:53,000 --> 00:01:55,000
Always stay in the present moment.

29
00:01:55,000 --> 00:01:59,000
So the best way to say is remind yourself to stay in the present moment.

30
00:01:59,000 --> 00:02:07,000
What's most important now is now it's not Sri Lanka when you come here.

31
00:02:07,000 --> 00:02:10,000
It's not the meditation course that you're going to do.

32
00:02:10,000 --> 00:02:17,000
The meditation course, you'll be terribly disappointed because you'll realize it's just the same.

33
00:02:17,000 --> 00:02:19,000
I mean, you won't be intellectually.

34
00:02:19,000 --> 00:02:33,000
But your mind that's excited now will become bored and easily disenchanted because it's exactly the same as it was at home.

35
00:02:33,000 --> 00:02:38,000
And so then your mind with the same habit will begin to get excited about when you're going to go home.

36
00:02:38,000 --> 00:02:41,000
Well, then that's what's exciting when I go home.

37
00:02:41,000 --> 00:02:45,000
Then I'll be, then it's really going to be exciting because then I can put all this into practice.

38
00:02:45,000 --> 00:02:47,000
Yeah, when I go back to work, I'll be like this.

39
00:02:47,000 --> 00:02:49,000
Then you'll get more excited and more afraid.

40
00:02:49,000 --> 00:02:56,000
And you'll have the same conditions while you're here in the practice and becoming excited and afraid about having to go home.

41
00:02:56,000 --> 00:02:59,000
And this is how you'll develop your habits.

42
00:02:59,000 --> 00:03:03,000
So it doesn't mean that you can solve it now.

43
00:03:03,000 --> 00:03:08,000
Of course, coming to meditate is the best way to help to solve that.

44
00:03:08,000 --> 00:03:12,000
But it's important to have your right view about meditation.

45
00:03:12,000 --> 00:03:15,000
Meditation is to be in the present moment.

46
00:03:15,000 --> 00:03:19,000
So best way to prepare for the meditation practices to start meditating.

47
00:03:19,000 --> 00:03:28,000
To look at this excitement and fear and say, this is what I'm going to have to deal with at the meditation practice.

48
00:03:28,000 --> 00:03:33,000
This is what the meditation practice is all about.

49
00:03:33,000 --> 00:03:37,000
Start here, start now and focus on that worry and that fear.

50
00:03:37,000 --> 00:03:42,000
Or that excitement and that fear because it's the same sort of thing that's going to come up here.

51
00:03:42,000 --> 00:03:47,000
And it'll ruin your practice if you're here thinking about the future.

52
00:03:47,000 --> 00:03:51,000
Yes, yes, I'm here for the purpose of the future while you're not.

53
00:03:51,000 --> 00:03:57,000
You're here for the purpose of the present moment to make the present moment the best it can be.

54
00:03:57,000 --> 00:04:01,000
So right now, make the present moment the best it can be.

55
00:04:01,000 --> 00:04:04,000
That's the best way to prepare.

56
00:04:04,000 --> 00:04:07,000
Don't get excited about the course.

57
00:04:07,000 --> 00:04:12,000
I mean, if you do get excited, look at the excitement, not at the course.

58
00:04:12,000 --> 00:04:14,000
Look at that excitement.

59
00:04:14,000 --> 00:04:15,000
Don't repress it.

60
00:04:15,000 --> 00:04:16,000
Don't stop it.

61
00:04:16,000 --> 00:04:19,000
Don't deny it.

62
00:04:19,000 --> 00:04:20,000
Look at it.

63
00:04:20,000 --> 00:04:25,000
Try to understand the excitement because that's what you're going to try to be doing here.

64
00:04:25,000 --> 00:04:28,000
So get ahead, start.

