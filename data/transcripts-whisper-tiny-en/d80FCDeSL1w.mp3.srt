1
00:00:00,000 --> 00:00:06,000
Where do you find teachers who are willing to be with you 24 or 7?

2
00:00:06,000 --> 00:00:13,000
Because in a lot of Chinese tradition, they call many, I think this is supposed to say

3
00:00:13,000 --> 00:00:18,000
many lay people call high monks teacher, where they only see them once a year or a month.

4
00:00:18,000 --> 00:00:21,000
But the teacher probably doesn't even know their students.

5
00:00:21,000 --> 00:00:27,000
Can I call you my teacher, since I'm on the YouTube site every weekend?

6
00:00:27,000 --> 00:00:33,000
And then he says, it's easy to find a teacher, but even harder for a teacher to call you a student.

7
00:00:33,000 --> 00:00:36,000
Even if you live near lots of centers.

8
00:00:42,000 --> 00:00:47,000
Well, if it's just a question of what you can call me, you can call me whatever you like.

9
00:00:47,000 --> 00:00:49,000
You can call me Al.

10
00:00:49,000 --> 00:00:51,000
That's a song.

11
00:00:51,000 --> 00:00:52,000
Yeah.

12
00:00:52,000 --> 00:00:53,000
Okay.

13
00:00:53,000 --> 00:00:59,000
Well, why I say that is because it's not so important.

14
00:00:59,000 --> 00:01:06,000
What do you call me or whether you take me as your teacher or anyone as your teacher?

15
00:01:06,000 --> 00:01:17,000
If it helps, you give you some spiritual encouragement, then yeah, you're welcome to say this person is my teacher.

16
00:01:17,000 --> 00:01:23,000
That person is my teacher or that person is my teacher as you like and take that up.

17
00:01:23,000 --> 00:01:30,000
Of course, the danger that you might run into is maybe you'll pick the wrong person or maybe you'll get a wrong idea of the person.

18
00:01:30,000 --> 00:01:41,000
If you don't have direct knowledge, even the Buddha himself suffered from this or was faced with this sort of thing.

19
00:01:41,000 --> 00:01:46,000
His students spending all their time sitting around watching him and saying, there's my teacher.

20
00:01:46,000 --> 00:01:48,000
Oh, look at my teacher.

21
00:01:48,000 --> 00:01:49,000
How wonderful.

22
00:01:49,000 --> 00:01:50,000
This teacher.

23
00:01:50,000 --> 00:01:52,000
And the Buddha said, get away.

24
00:01:52,000 --> 00:01:53,000
What are you doing?

25
00:01:53,000 --> 00:01:59,000
Looking at this rotten, filthy body of mine full of urine and feces and all sorts of other things.

26
00:01:59,000 --> 00:02:00,000
A peyhee.

27
00:02:00,000 --> 00:02:03,000
The Buddha said, get lost.

28
00:02:03,000 --> 00:02:07,000
Kick them out of the monastery in order to help them.

29
00:02:07,000 --> 00:02:11,000
Eventually, the monk didn't become enlightened.

30
00:02:11,000 --> 00:02:19,000
So if it helps you, I mean, this idea of clinging, we've talked about clinging.

31
00:02:19,000 --> 00:02:21,000
Sometimes you have to cling to something.

32
00:02:21,000 --> 00:02:27,000
If you're clinging to something in order to help you up, then that's a good sort of clinging.

33
00:02:27,000 --> 00:02:30,000
When a child learns to walk, they have to cling to the table.

34
00:02:30,000 --> 00:02:34,000
If they don't have the table to cling to, they'll never be able to stand up.

35
00:02:34,000 --> 00:02:36,000
They're not strong enough yet.

36
00:02:36,000 --> 00:02:38,000
Once they're strong enough, they can stop clinging.

37
00:02:38,000 --> 00:02:43,000
If you're clinging to something to think that you don't have to stand up.

38
00:02:43,000 --> 00:02:46,000
I've got this bottle of mine, like a baby to its bottle.

39
00:02:46,000 --> 00:02:50,000
I'm not going to learn to walk foods right here.

40
00:02:50,000 --> 00:02:53,000
Then it's a bad sort of clinging.

41
00:02:53,000 --> 00:03:02,000
So if you take a teacher, you call someone your teacher because that encouragement or that stability

42
00:03:02,000 --> 00:03:06,000
is going to help you focus on one technique, which is always good.

43
00:03:06,000 --> 00:03:19,000
And it's going to help you to exert yourself in that technique and to apply yourself to that technique and to develop it.

44
00:03:19,000 --> 00:03:24,000
Then I would say eventually you won't need to cling to the teacher anymore.

45
00:03:24,000 --> 00:03:37,000
You'll become, as the Buddha said, you'll become independent in the Buddha's teaching.

46
00:03:37,000 --> 00:03:39,000
So you won't need the Buddha even anymore.

47
00:03:39,000 --> 00:03:45,000
This is the name or the description of an Arahant is someone who has become independent in the Buddha's teaching.

48
00:03:45,000 --> 00:03:49,000
No longer needs to depend even on the Buddha.

49
00:03:49,000 --> 00:03:54,000
So these are all just words.

50
00:03:54,000 --> 00:03:59,000
And you should do it if it helps you and if it brings you benefit.

51
00:03:59,000 --> 00:04:04,000
The best thing if you're asking what is the best type of teacher student relationship?

52
00:04:04,000 --> 00:04:10,000
The best teacher student relationship is when you are with your students,

53
00:04:10,000 --> 00:04:16,000
I don't want to say 24-7, but you are with your students in the same monastery.

54
00:04:16,000 --> 00:04:23,000
You don't need a teacher to be watching you 24-7, but I assume that means be the same monastery.

55
00:04:23,000 --> 00:04:26,000
That's the best.

56
00:04:26,000 --> 00:04:41,000
If you can't meet that right now, then do as you can and move slowly and try to find the best support that you can to help pull yourself up until eventually.

57
00:04:41,000 --> 00:04:49,000
Physically and spiritually, you're able to put yourself in a position to become free from suffering.

58
00:04:49,000 --> 00:04:51,000
Want to continue that?

59
00:04:51,000 --> 00:04:59,000
There's not much to add, but for the second part of the question, it's not easy to find a teacher,

60
00:04:59,000 --> 00:05:08,000
but even harder for a teacher to call you a student.

61
00:05:08,000 --> 00:05:21,000
I think that's more a word game, a play of words than it should really be in reality.

62
00:05:21,000 --> 00:05:40,000
Because we are our students of the border, we should be students of the border, and a teacher is a represent of the border.

63
00:05:40,000 --> 00:05:59,000
But he may be of the teaching, so I don't know if hard for a teacher to, I mean, if there is somebody dedicated to a teacher and not attached, not clinging,

64
00:05:59,000 --> 00:06:11,000
that would of course be a perfect student.

65
00:06:11,000 --> 00:06:17,000
How can you find a teacher who considers you their student?

66
00:06:17,000 --> 00:06:21,000
Somehow that, because I guess that's a spiritual encouragement as well.

67
00:06:21,000 --> 00:06:29,000
Oh, the teacher knows my name and so on. I've got a funny story for that, and I get my turn.

68
00:06:29,000 --> 00:06:38,000
I remember that many people get very, very happy when the monk or the teacher suddenly remembers the name.

69
00:06:38,000 --> 00:06:42,000
Did you know my story about that?

70
00:06:42,000 --> 00:06:46,000
You know my story about that, did I have told you?

71
00:06:46,000 --> 00:06:51,000
My teacher never remembered my name, and it was so demoralizing for me.

72
00:06:51,000 --> 00:07:05,000
I came back after a year of slugging it away at the university as a monk, and working with this Cambodian community that was very supportive, but had some problems.

73
00:07:05,000 --> 00:07:16,000
And I thought, okay, now I'm back in Thailand, I'll stay with my teacher. He said, where did this guy have a day in? You're a day in with you.

74
00:07:16,000 --> 00:07:25,000
I'm like, okay, well, that's fine. That's okay. And then he's proceeded to not remember my name.

75
00:07:25,000 --> 00:07:38,000
Every time I was like, what's your name? What's his name? Pranora. I'm in every day, every day, every time. Not every day, but it was clear that he wasn't remembering my name because I was watching for it.

76
00:07:38,000 --> 00:07:44,000
And I was like, look, I don't remember those people's name, you remember this person name. Does it remember my name?

77
00:07:44,000 --> 00:08:06,000
And then I did something really, really bad in the monastery. I can't remember exactly what it was, but it had to do with this politics that goes on between who's the teacher for the international students and who's running the international center and some people were trying to take it away from the group that I was involved in.

78
00:08:06,000 --> 00:08:18,000
And I was so up in arms about it that I went in and told that Jan that these people were trying to split up the Sangha and he scolded me. And he said, what are you talking about split up the Sangha? This is wrong thought.

79
00:08:18,000 --> 00:08:29,000
This is just delegating authority or something like this. So you totally, and the point was the remarks that were yelling at me at the time, who do you think you are? Who are you?

80
00:08:29,000 --> 00:08:42,000
Because I was fairly self-righteous about it. New monks can be fairly self-righteous. And from that point on he always remembered my name.

81
00:08:42,000 --> 00:08:53,000
And it was so embarrassing after all that time of wanting him to know my name at, from that point on I didn't want him to know my name at all. I'm like now, I know the reason why he remembers my name.

82
00:08:53,000 --> 00:09:00,000
It was that huge fight that we had in front of him that really, really made his day.

83
00:09:00,000 --> 00:09:07,000
And really showed him what's an important person I was to remember.

84
00:09:07,000 --> 00:09:13,000
So don't take that kind of thing too seriously. The knowing of names.

85
00:09:13,000 --> 00:09:19,000
But I can certainly, certainly feel for you the desire to have some connection.

86
00:09:19,000 --> 00:09:27,000
But it is just another attachment. And the dhamma is out there. And there's so much dhamma out there.

87
00:09:27,000 --> 00:09:33,000
The problem isn't the access to the dhamma or even the access to teachers. The problem is the practice of it.

88
00:09:33,000 --> 00:09:39,000
You have to dedicate yourself to practice. And that really does mean, in one sense, dedicate your life to it.

89
00:09:39,000 --> 00:09:50,000
You have to be willing to die for the truth. You have to be willing to give up your life if that is the path for.

90
00:09:50,000 --> 00:10:01,000
I mean, just to make it kind of go to that extreme to make it kind of clear to you that it has to become the guiding purpose in your life.

91
00:10:01,000 --> 00:10:12,000
Not God, not Buddha, not religion, but the truth and the practice and the cultivation of understanding and wisdom.

92
00:10:12,000 --> 00:10:19,000
So it has to be great. It's up to you.

93
00:10:19,000 --> 00:10:34,000
Buddha said, Akata wrote at Agata, to me, he could changa at the pang, you have to work for yourself. Akata wrote at Agata. The Buddhas, the Agatas are just those who show the way.

94
00:10:34,000 --> 00:10:58,000
Again, I'm talking too much now.

