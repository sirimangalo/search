1
00:00:00,000 --> 00:00:02,200
It says,

2
00:00:02,200 --> 00:00:05,200
Monty, I have been thinking recently.

3
00:00:05,200 --> 00:00:11,560
I have already made mention of how some of the Christians hand out magazines to me at the

4
00:00:11,560 --> 00:00:12,560
bus stop.

5
00:00:12,560 --> 00:00:21,200
Would it be wrong if I told them, I'll take it if they listen to what I have to say.

6
00:00:21,200 --> 00:00:29,560
Not so much, they are wrong, but they might have any

7
00:00:29,560 --> 00:00:32,560
effect upon others anyway.

8
00:00:32,560 --> 00:00:36,560
What do you think, Larry?

9
00:00:36,560 --> 00:00:41,560
Is that wrong?

10
00:00:41,560 --> 00:00:48,320
Yes, in a way, I think it is wrong because first of all, they are wasting other people's

11
00:00:48,320 --> 00:00:57,520
time and as he stated, he or she, handing out one pamphlet, so not how to make a difference

12
00:00:57,520 --> 00:01:04,520
in how someone feels about their inner beliefs in the first place, so really it is quite

13
00:01:04,520 --> 00:01:07,640
agitated.

14
00:01:07,640 --> 00:01:11,520
We have some people here in the states that go door to door some time to time, handing

15
00:01:11,520 --> 00:01:13,720
out pamphlets.

16
00:01:13,720 --> 00:01:18,840
I usually just out of courtesy take them, say, thank you and I go chunk them as soon

17
00:01:18,840 --> 00:01:24,160
as they leave the front door because I am not going to read them because I know basically

18
00:01:24,160 --> 00:01:29,680
I have read enough to basically know some of their beliefs and I just have no interest

19
00:01:29,680 --> 00:01:36,720
if they want to believe that way fine, but if not, I believe the way I want to believe

20
00:01:36,720 --> 00:01:44,800
so I think to be polite to those individuals and be just to say, I'd rather not take

21
00:01:44,800 --> 00:01:59,120
your pamphlets because I really need to be you sign, but no thank you, I don't want your

22
00:01:59,120 --> 00:02:04,500
pamphlets and leave it to that, a confrontation particularly on the station in the air

23
00:02:04,500 --> 00:02:11,760
40s, if futile to begin with, no one is going to convince anyone else of anything, so they

24
00:02:11,760 --> 00:02:17,320
believe that, they believe it's ostracizing and that's their belief, that's fine, but

25
00:02:17,320 --> 00:02:23,960
just don't take it out only.

26
00:02:23,960 --> 00:02:24,960
Anybody else?

27
00:02:24,960 --> 00:02:25,960
No wrong.

28
00:02:25,960 --> 00:02:31,500
Yeah, I don't think it's an eye for an eye kind of thing, like you have to listen to

29
00:02:31,500 --> 00:02:34,760
me and I'll listen to you.

30
00:02:34,760 --> 00:02:37,760
There's very...

31
00:02:37,760 --> 00:02:42,000
Yeah, I'll just say yeah, thanks for getting along my mind.

32
00:02:42,000 --> 00:02:50,360
I mean if it's wrong to them to be confrontational in forcing their views upon you, or if

33
00:02:50,360 --> 00:02:58,080
it seems kind of rude and so on, then adding rude to rude is not probably the best.

34
00:02:58,080 --> 00:03:04,280
What I find works much better is to inquire as to their beliefs politely, what I find

35
00:03:04,280 --> 00:03:16,640
as the best effect on people who engage in such blind faith in things like God and so

36
00:03:16,640 --> 00:03:27,840
on, and other imaginary things, is to inquire and actually investigate with them, the

37
00:03:27,840 --> 00:03:34,240
way Socrates did, not exactly the way Socrates did, but no politely asking.

38
00:03:34,240 --> 00:03:39,080
And because they have so much stress, I've talked about this before, these people have so

39
00:03:39,080 --> 00:03:44,040
much stress and tension, probably this is the person who asked the question before I don't

40
00:03:44,040 --> 00:03:49,840
remember it, but I remember answering this somehow that they have so much stress and

41
00:03:49,840 --> 00:03:55,520
tension built up that really all they want is to let go, not want, but really all they

42
00:03:55,520 --> 00:04:02,720
need and really what would make them happier, truly happy, is to let go and stop trying

43
00:04:02,720 --> 00:04:10,240
to justify something that's totally unjustifiable and irrational, trying to rationally

44
00:04:10,240 --> 00:04:14,480
in the United States, so many times.

45
00:04:14,480 --> 00:04:19,800
One particular denomination that I'm familiar with, they send your children around door

46
00:04:19,800 --> 00:04:20,800
to door.

47
00:04:20,800 --> 00:04:27,600
They sit in the car, I don't know if they came to come up or afraid or what, but they'll

48
00:04:27,600 --> 00:04:33,680
send their children to the door with these pamphlets, and you can't confront those kids,

49
00:04:33,680 --> 00:04:39,480
because quite frankly, I think they've been brainwashed and it's just like to church out

50
00:04:39,480 --> 00:04:46,320
in Missouri that the parents and their kids, I'm sure all of you're familiar with this,

51
00:04:46,320 --> 00:04:54,320
at least one here in the States, where they're protesting the military funerals of soldiers

52
00:04:54,320 --> 00:04:59,800
that have been killed in action, and these church members and their children, they've

53
00:04:59,800 --> 00:05:05,440
all been indoctrinated, the ones here locally are not affiliated with that at all, but

54
00:05:05,440 --> 00:05:11,640
I do google them from time to time to find out that it happens to be one of the, I guess

55
00:05:11,640 --> 00:05:16,960
you could call it a sect, they wouldn't call themselves a sect, but they happen to be

56
00:05:16,960 --> 00:05:23,800
a group that feel like that they're wrong and everyone, I mean they're right and everyone

57
00:05:23,800 --> 00:05:32,320
else is wrong, and the fact I've read some articles written by previous members of that,

58
00:05:32,320 --> 00:05:55,400
particular religious sect that say once you are losing you there, I think it's internet

59
00:05:55,400 --> 00:06:02,960
term that they use in that particular sect, but it is true too, so it's the very sticky

60
00:06:02,960 --> 00:06:03,960
question.

61
00:06:03,960 --> 00:06:10,800
I just feel like personally, I just really don't have the time for them, I try to get

62
00:06:10,800 --> 00:06:19,560
out of this situation as quickly as I can without confrontation with them, but they're not

63
00:06:19,560 --> 00:06:24,680
going to convince me of their ways, regardless of what they say or what they give to

64
00:06:24,680 --> 00:06:25,680
me.

65
00:06:25,680 --> 00:06:31,040
So to me, it's just a waste of my time and there's let them go find somebody that will

66
00:06:31,040 --> 00:06:38,920
believe, they say, no, for sure, with children it's especially difficult, I remember

67
00:06:38,920 --> 00:06:48,960
once these evangelicals came to the monastery and once they had me targeted, this woman,

68
00:06:48,960 --> 00:06:53,800
she was, there were some women there, and then one of them was the leader and she

69
00:06:53,800 --> 00:07:02,000
totally, totally cold, and I gave up, I said, would you like this card and they said,

70
00:07:02,000 --> 00:07:07,080
no, no, she said for everyone, no, we don't want your card, just in a really cold and

71
00:07:07,080 --> 00:07:12,280
a hard way, and then suddenly the next night she comes back with her husband and her

72
00:07:12,280 --> 00:07:21,000
two kids, and I think I started talking to the kids and it was well depressing because

73
00:07:21,000 --> 00:07:30,360
they're obviously have no ability to think for themselves and have never been taught

74
00:07:30,360 --> 00:07:40,280
how to use their, what do you say, the reason circuits in their brain are in the mind

75
00:07:40,280 --> 00:07:47,840
of the mind's ability to investigate and they've been totally, speak talking of the brain

76
00:07:47,840 --> 00:07:58,400
as a filter and well, their brain is like a vice grip and keeping them locked in and

77
00:07:58,400 --> 00:08:07,320
certain predefined use so I asked them and the parents said go ahead, tell me, tell

78
00:08:07,320 --> 00:08:15,080
him and then, no, no, no, no, no, no, no, no, no, no, no, no, no, no, no, no, no, no, no, no, no, no, no,

79
00:08:15,080 --> 00:08:24,840
kids know. They're so malleable that you can twist them and turn them in any sort of way.

80
00:08:24,840 --> 00:08:35,880
They're not strong enough to reject the things that their parents teach them when they get

81
00:08:35,880 --> 00:08:48,360
older they may be able to. But, you know, you still have to feel for these people and, you know,

82
00:08:48,360 --> 00:08:55,640
to some extent, not to a very big extent, but to some extent see their suffering, you know,

83
00:08:55,640 --> 00:08:59,240
or from my point of view, because what happened then, I think I've told this story before the

84
00:08:59,240 --> 00:09:03,320
next day, I said, well, if they're coming to visit me, I shouldn't go visit them. And then the

85
00:09:03,320 --> 00:09:09,800
next day, I went to their church, which was his huge church just down the road from what

86
00:09:09,800 --> 00:09:16,520
tie of Los Angeles. And it was such an interesting experience. I talked with this teacher of

87
00:09:17,640 --> 00:09:22,280
teacher of preachers. He was a preacher, teacher. He taught, taught, taught preachers,

88
00:09:22,280 --> 00:09:34,120
pastors. He taught pastors. And he was suffering. It was so easy to see. I made him see, not to

89
00:09:34,120 --> 00:09:40,440
great extent, but I, you know, he must be dealing with this conflict inside of himself every day.

90
00:09:40,440 --> 00:09:48,680
How difficult it is to keep this faith that is so ungrounded in any form of reality.

91
00:09:48,680 --> 00:09:58,920
And so, you know, to the extent that you're able to gently show them their own

92
00:10:02,520 --> 00:10:06,520
their own suffering, I think you do them a great, a great deal. But I mean,

93
00:10:06,520 --> 00:10:14,840
just showing them confidence, real confidence in the sense that I don't mind if you talk to me,

94
00:10:14,840 --> 00:10:24,040
I don't mind if you give me your brochure. Why? Because I'm happy. I'm, I'm, I'm settled in,

95
00:10:24,040 --> 00:10:29,480
in who I am. I mean, you want to give me your brochure? Sure. It's just a piece of paper. But

96
00:10:30,440 --> 00:10:37,000
somehow bringing across to them a sense of peace and happiness and helping them to see that

97
00:10:37,000 --> 00:10:44,840
that's not possible to gain with blind faith, that you have, that you are free from something

98
00:10:44,840 --> 00:10:50,440
that they are not free from, you know, I mean, not be, and I think this doesn't come from,

99
00:10:50,440 --> 00:10:55,560
from imposing your views on them. It comes from being accepting and open and loving and caring

100
00:10:55,560 --> 00:11:02,280
towards them because really that's generally why people go to such cults, you might say, or

101
00:11:02,280 --> 00:11:11,640
sects, is because of the love that they find. The, the, the need that they have for being

102
00:11:11,640 --> 00:11:20,360
controlled or for being taken care of, the need for, you know, in Judeo-Christian religions,

103
00:11:20,360 --> 00:11:28,200
the need for a father figure, that people become, you know, it's such a fatherly religion,

104
00:11:28,200 --> 00:11:33,080
if you read, if you actually read the things that Jesus says, it's, you know, it's very much,

105
00:11:33,640 --> 00:11:41,400
very oppressive actually. Some of it's nice, but a lot of it's just, you know, your father, your

106
00:11:41,400 --> 00:11:51,800
father, and how you have to, so if you, if you help people to see the possibility of just loving

107
00:11:51,800 --> 00:11:59,560
without, without needing, no, then you do them a favor. I mean, this is the thing,

108
00:11:59,560 --> 00:12:03,240
rather than seeing it as a conflict, rather than seeing it as a confrontation, you should

109
00:12:04,440 --> 00:12:10,680
see it, you know, as a fellow human being who, who, who might be able to help,

110
00:12:11,880 --> 00:12:17,320
and if you think in terms of helping this person, and not in a condescending or self-righteous way,

111
00:12:17,320 --> 00:12:21,880
but just in the way that you would help someone who needs food, and gives them some food,

112
00:12:21,880 --> 00:12:26,440
you know, you're not judging them, and you're not thinking to look down on them, or that you're

113
00:12:26,440 --> 00:12:32,040
better than them, but just look and see if there's any way you can help you with children, if it's

114
00:12:32,040 --> 00:12:36,680
children, bringing pamphlets to your daughter, there's not much you can do, but you can love them,

115
00:12:36,680 --> 00:12:41,400
it makes you happy, and these kids come to your door, and they want to give you some brochures and say,

116
00:12:41,400 --> 00:12:47,080
thank you, I hope you have a good day, you know, you're, that's good for you, that's a good thing

117
00:12:47,080 --> 00:12:51,800
that you've done for them, it's not going to change them or make them doubt their religion,

118
00:12:51,800 --> 00:12:56,760
not in any significant way, but it's what you can do, you give them, it's like you gave them

119
00:13:00,360 --> 00:13:07,960
a muffin or something, you gave them something that was good for them, it's not going to

120
00:13:07,960 --> 00:13:13,720
solve the world's problems, but we're not here to solve the world's problems, we're here to do

121
00:13:13,720 --> 00:13:18,680
good things, avoid evil things and purify our own minds, so whatever helps you to do that,

122
00:13:18,680 --> 00:13:25,000
that's what I would do, or that hopefully that's what I would do, you know, I might just get

123
00:13:25,000 --> 00:13:31,480
all self-righteous on them and start telling them about Buddhism and so on, but I should,

124
00:13:33,080 --> 00:13:37,000
I'm not saying that I practice everything that I preach, but do as I say not as a

125
00:13:37,000 --> 00:13:39,000
way, maybe do sometimes.

