1
00:00:00,000 --> 00:00:06,000
When I practice sitting meditation, usually about 20 minutes into it, I feel very relaxed,

2
00:00:06,000 --> 00:00:13,000
and my vision with my eyes closed gets darker and darker, and I have a feeling of falling.

3
00:00:13,000 --> 00:00:17,000
When this happens to me, am I on the right track?

4
00:00:17,000 --> 00:00:23,000
Do I have a focused concentration at this time, or it's just nothing?

5
00:00:23,000 --> 00:00:30,000
Who was that?

6
00:00:30,000 --> 00:00:38,000
First of all, you should just observe as it is when it is getting darker,

7
00:00:38,000 --> 00:00:46,000
then observe seeing, seeing, because it's your vision.

8
00:00:46,000 --> 00:00:49,000
It's getting darker and your vision you describe.

9
00:00:49,000 --> 00:00:54,000
So I would say seeing, seeing at that time,

10
00:00:54,000 --> 00:00:57,000
and then you have the feeling of falling,

11
00:00:57,000 --> 00:01:05,000
and then I would say feeling, feeling, feeling, and not make a deal out of it.

12
00:01:05,000 --> 00:01:09,000
So when you think of it, when you ponder over it

13
00:01:09,000 --> 00:01:18,000
and want it to be something that your concentration is high,

14
00:01:18,000 --> 00:01:23,000
or so, then you are looking for a gain,

15
00:01:23,000 --> 00:01:28,000
and this can lead you into what we were talking earlier

16
00:01:28,000 --> 00:01:33,000
into one of the Vipassana pakilesa.

17
00:01:33,000 --> 00:01:38,000
So it is dangerous to want to know that.

18
00:01:38,000 --> 00:01:42,000
Is that something good, or is that just nothing?

19
00:01:42,000 --> 00:01:44,000
Be relaxed about it.

20
00:01:44,000 --> 00:01:48,000
If you're relaxed, that's good.

21
00:01:48,000 --> 00:01:58,000
Notice, relaxed, relaxed, and then you will be on the right track.

22
00:01:58,000 --> 00:02:06,000
When you are looking for the gain, or when you are about to judge,

23
00:02:06,000 --> 00:02:13,000
is this good, or is this not good, then you are not on the right track?

24
00:02:13,000 --> 00:02:18,000
Again, this very basic principle that I mentioned,

25
00:02:18,000 --> 00:02:21,000
it's the practice that is the right track.

26
00:02:21,000 --> 00:02:23,000
It's the practice which leads to progress.

27
00:02:23,000 --> 00:02:29,000
It's a very obvious principle,

28
00:02:29,000 --> 00:02:31,000
and yet it's so easy to lose sight of it,

29
00:02:31,000 --> 00:02:33,000
because we're talking about human beings

30
00:02:33,000 --> 00:02:35,000
who are generally irrational.

31
00:02:35,000 --> 00:02:37,000
We act in very irrational ways.

32
00:02:37,000 --> 00:02:40,000
And so if you think about it,

33
00:02:40,000 --> 00:02:43,000
I think if you think about it objectively,

34
00:02:43,000 --> 00:02:45,000
you should be able to realize for yourself that,

35
00:02:45,000 --> 00:02:47,000
no, how could you be on the right track?

36
00:02:47,000 --> 00:02:50,000
At that moment, you're no longer meditating.

37
00:02:50,000 --> 00:02:55,000
But because of our irrational attachment

38
00:02:55,000 --> 00:03:00,000
to the strength, the mystical, the super mundane,

39
00:03:00,000 --> 00:03:09,000
the special, we think that maybe now all my problems will be over.

40
00:03:09,000 --> 00:03:12,000
Maybe now suffering will cease.

41
00:03:12,000 --> 00:03:16,000
Maybe I don't have to practice anymore.

42
00:03:16,000 --> 00:03:21,000
And this is all based on defilement and detachment.

43
00:03:21,000 --> 00:03:25,000
The practice is that which leads to progress,

44
00:03:25,000 --> 00:03:28,000
and only the practice.

45
00:03:28,000 --> 00:03:31,000
Another thing you can always remind yourself of,

46
00:03:31,000 --> 00:03:34,000
which it just said, this is not that.

47
00:03:34,000 --> 00:03:37,000
This is a motto you should have.

48
00:03:37,000 --> 00:03:40,000
This is not that.

49
00:03:40,000 --> 00:03:42,000
Whatever it is, it's not that.

50
00:03:42,000 --> 00:03:45,000
It's not what you're looking for.

51
00:03:45,000 --> 00:03:48,000
So if you just keep reminding yourself of that,

52
00:03:48,000 --> 00:03:50,000
then you won't cling to anything.

53
00:03:50,000 --> 00:03:52,000
And everything that comes up,

54
00:03:52,000 --> 00:03:54,000
you'll be able to let it go.

55
00:03:54,000 --> 00:03:57,000
And the other thing that went to say,

56
00:03:57,000 --> 00:03:59,000
though it was sort of already said,

57
00:03:59,000 --> 00:04:03,000
is that another principle that we always have to remind people

58
00:04:03,000 --> 00:04:08,000
of is we're not trying to gain concentration.

59
00:04:08,000 --> 00:04:10,000
It's not a sign that you're on the right track

60
00:04:10,000 --> 00:04:13,000
that you have concentration.

61
00:04:13,000 --> 00:04:17,000
Concentration is a mine state that has to be balanced with energy.

62
00:04:17,000 --> 00:04:20,000
True concentration, when balanced with energy,

63
00:04:20,000 --> 00:04:22,000
doesn't feel concentrated at all.

64
00:04:22,000 --> 00:04:24,000
You don't even notice it.

65
00:04:24,000 --> 00:04:26,000
It's totally natural.

66
00:04:26,000 --> 00:04:29,000
You feel that you're just present.

67
00:04:29,000 --> 00:04:33,000
That's in a sense more focused than concentration.

68
00:04:33,000 --> 00:04:38,000
Focus is where you're perfectly clear when a camera is focused.

69
00:04:38,000 --> 00:04:44,000
It's not that it's all the way in its halfway.

70
00:04:44,000 --> 00:04:49,000
It's right in the middle where you can see things clearly.

71
00:04:49,000 --> 00:04:53,000
And this is very important because people get the wrong idea

72
00:04:53,000 --> 00:04:55,000
that meditation is about concentration.

73
00:04:55,000 --> 00:04:57,000
It's not what the word meditation means.

74
00:04:57,000 --> 00:05:01,000
It's not what insight is.

75
00:05:01,000 --> 00:05:08,000
It has nothing intrinsically to do with the development of understanding.

76
00:05:08,000 --> 00:05:12,000
Understanding comes from focus, which means to focus the lens

77
00:05:12,000 --> 00:05:15,000
and to balance your faculties.

78
00:05:15,000 --> 00:05:20,000
Whenever you have some extreme experience of concentration or energy

79
00:05:20,000 --> 00:05:24,000
or whatever, this isn't the path.

80
00:05:24,000 --> 00:05:28,000
It's some by-product side phenomenon.

81
00:05:28,000 --> 00:05:52,000
The path is being mindful of it.

