1
00:00:00,000 --> 00:00:16,640
Today, I'll continue the series of talks on the ASMR, so the developments which caused

2
00:00:16,640 --> 00:00:26,880
the mind to ferment, to become sour, to go bad.

3
00:00:26,880 --> 00:00:37,400
Today's topic is on the Sariwatana Bahata par sava.

4
00:00:37,400 --> 00:00:51,920
The ASMR tea, the ASMR which are the method of removing the ASMR which consists of avoiding.

5
00:00:51,920 --> 00:00:59,640
The ASMR is getting rid of them, removing them simply by avoiding.

6
00:00:59,640 --> 00:01:06,640
Now this is an important, it's important to be clear about this one.

7
00:01:06,640 --> 00:01:15,840
It's a subject which is very easy to misunderstand, and indeed Buddhism is often very much

8
00:01:15,840 --> 00:01:25,800
misunderstood as being a religion which consists of avoiding things like responsibility

9
00:01:25,800 --> 00:01:39,960
or involvement, or which involves running away from reality, running away from suffering,

10
00:01:39,960 --> 00:01:53,040
running away from problems, and so it's important to clear this up first of all.

11
00:01:53,040 --> 00:01:59,840
I think it's fair that Buddhism does to some extent advocate of running away or avoiding

12
00:01:59,840 --> 00:02:14,760
or keeping out of what is generally called modern society or public society.

13
00:02:14,760 --> 00:02:25,280
But I think this is a fair statement because I think society or public society is something

14
00:02:25,280 --> 00:02:33,240
which is very much misunderstood and there's a great bias in our understanding of society,

15
00:02:33,240 --> 00:02:37,080
of what we call society.

16
00:02:37,080 --> 00:02:47,680
And we think of society as the right and the proper way to organize groups of people.

17
00:02:47,680 --> 00:02:53,720
And we base this on Aristotle or on Socrates, Plato and so on.

18
00:02:53,720 --> 00:03:01,000
On these ancient philosophers who we assume were infallible and whom we assume are being

19
00:03:01,000 --> 00:03:09,560
followed and followed and being followed to the letter.

20
00:03:09,560 --> 00:03:19,880
And yet we see that the aims and the goals and the fruits which come from these systems

21
00:03:19,880 --> 00:03:30,600
which we call society tend very much on the side of greed and on the side of lust,

22
00:03:30,600 --> 00:03:42,920
tend very much on the side of consumerism in the sense of reaching out for, seeking out

23
00:03:42,920 --> 00:03:52,240
for things which are impermanent, things which are unsatisfying and things which are not

24
00:03:52,240 --> 00:04:02,520
under our control, seeking out for things which are subject to dissolution which are ephemeral.

25
00:04:02,520 --> 00:04:11,200
And so what we find is that when we reach out and want more and more and more there are

26
00:04:11,200 --> 00:04:19,160
other beings who also reach out and want more and more and more and eventually there's

27
00:04:19,160 --> 00:04:28,280
not enough for anyone because our wants are far greater than our actual needs.

28
00:04:28,280 --> 00:04:35,000
And so we create what we call society and this is of course very much simplified but

29
00:04:35,000 --> 00:04:41,560
it's clear in my mind that there's a great element of truth in this and it's based

30
00:04:41,560 --> 00:04:48,040
on the Lord Buddha's idea of explanation of how society arose and so on.

31
00:04:48,040 --> 00:04:57,640
People were not satisfied simply by collecting wild roots and fruits and wild edibles

32
00:04:57,640 --> 00:05:01,240
which at that time would have been plentiful at the time when people lived in the forest

33
00:05:01,240 --> 00:05:09,600
and among the trees but nowadays we see that we see simply from the words where people

34
00:05:09,600 --> 00:05:14,280
say going off to live in the forest as a kind of running away, we say how far we've

35
00:05:14,280 --> 00:05:21,360
come from the nature which is our home which is our true home.

36
00:05:21,360 --> 00:05:30,000
And now we think of trees as something like an ornament that you put on your front lawn.

37
00:05:30,000 --> 00:05:36,280
We think of peace and quiet is something you find when you close your door or when you

38
00:05:36,280 --> 00:05:48,400
lie down to sleep in your cement room with a jail cell.

39
00:05:48,400 --> 00:06:02,280
And this is because of the unsatisfying unsatisfied, the lack of contentment being discontent

40
00:06:02,280 --> 00:06:06,760
with what we have when we have little we're not able to live and of course Socrates himself

41
00:06:06,760 --> 00:06:12,920
was very clear on this fact when he started to sat down and started to create his idea

42
00:06:12,920 --> 00:06:19,920
of society was that we would eat things like acorns and drink spring water and so on and

43
00:06:19,920 --> 00:06:27,920
great eat foods which were raw and which were bland and so on.

44
00:06:27,920 --> 00:06:32,720
Simple foods which could be easily found from foraging and so on.

45
00:06:32,720 --> 00:06:41,120
But we can see that the lack of contentment and Socrates pointed this out and it was clear

46
00:06:41,120 --> 00:06:44,720
that no one was going to go for such a society.

47
00:06:44,720 --> 00:06:49,560
And so then we have to start collecting food and when we collect food and keep food for

48
00:06:49,560 --> 00:06:53,520
the next day and the next day and the next day, when we keep it more and more and more

49
00:06:53,520 --> 00:07:02,800
and more while we have to extend our range and when our range interferes with other

50
00:07:02,800 --> 00:07:13,040
people's range, we have to set up fences, we have to set up territories.

51
00:07:13,040 --> 00:07:21,640
In setting up territories then we find that our territories not big enough to satisfy

52
00:07:21,640 --> 00:07:29,200
our desires, our desires become manifold and so we have to alter the territory, we have

53
00:07:29,200 --> 00:07:36,560
to cut down the trees and start specializing in a certain type of food and we specialize

54
00:07:36,560 --> 00:07:43,680
in something and maybe begin to barter or to trade or so on and so on.

55
00:07:43,680 --> 00:07:51,160
And eventually we have what is now this big message we call society and it has to do

56
00:07:51,160 --> 00:07:54,000
with a great population explosion.

57
00:07:54,000 --> 00:07:59,200
Of course, the isolation explosion, we all know where that comes from, comes from something

58
00:07:59,200 --> 00:08:06,640
which is considered nowadays to be natural and this is the sexual intercourse, something

59
00:08:06,640 --> 00:08:11,760
which probably wouldn't have been a big problem if we could keep our minds and our

60
00:08:11,760 --> 00:08:17,720
hearts and our eyes and our bodies under control and we can see how out of control it

61
00:08:17,720 --> 00:08:27,080
becomes and how our population explodes and because of course of our keen intellect and

62
00:08:27,080 --> 00:08:33,600
our ability to get what we want most of the time, we have come to take over most parts

63
00:08:33,600 --> 00:08:40,840
of this earth and all radically alter the face of it.

64
00:08:40,840 --> 00:08:50,800
We can see how someone who was able to control their bodies and not interested in running

65
00:08:50,800 --> 00:09:00,040
after objects of the sense is able to live in great peace and in a very ancient way.

66
00:09:00,040 --> 00:09:07,280
And so I think in this sense some of the criticisms are very much biased and of course

67
00:09:07,280 --> 00:09:13,160
rightly so that they come from people who are very much engrossed, very much enchanted

68
00:09:13,160 --> 00:09:20,680
by society and get involved in political debates and so on and create all sorts of systems

69
00:09:20,680 --> 00:09:22,880
and organizations.

70
00:09:22,880 --> 00:09:29,760
What we find there are still people in this world who refuse to be involved, refuse

71
00:09:29,760 --> 00:09:40,080
to indulge in the foolish running after chasing after things which are not truly going

72
00:09:40,080 --> 00:09:45,400
to satisfy and we can see this is when it comes down to this is the main goal of societies

73
00:09:45,400 --> 00:09:52,600
to get all the time what we want as much as possible and of course we try not to interfere

74
00:09:52,600 --> 00:09:55,880
with other people while doing so.

75
00:09:55,880 --> 00:10:01,720
But of course the main problem is the wanting and wanting is something which easily gets

76
00:10:01,720 --> 00:10:06,760
out of control and when we are free from wanting this or wanting that then we don't

77
00:10:06,760 --> 00:10:13,560
have to fight or argue or interfere with other people's business and so we are able

78
00:10:13,560 --> 00:10:19,720
to live in a very natural way and so the argument is that living in the forest is certainly

79
00:10:19,720 --> 00:10:27,920
not running away it's coming back to a natural, a very normal state of existence.

80
00:10:27,920 --> 00:10:36,280
And another important point which I think is less of a criticism but more of a problem

81
00:10:36,280 --> 00:10:47,920
for meditators or people who take up the Buddhist path and this is running away from suffering.

82
00:10:47,920 --> 00:10:53,760
I have heard it as a criticism that for as a reason why many people have taken up the

83
00:10:53,760 --> 00:10:59,880
Buddhist path some people from outside have said it's because they have had suffering

84
00:10:59,880 --> 00:11:04,640
thrown at them for so long that they just want to find a way out of it, they want to

85
00:11:04,640 --> 00:11:10,640
run away from it, they don't want to face it anymore and so they take a Buddhism which

86
00:11:10,640 --> 00:11:17,360
advocates so they say running away from suffering and Buddhism of course does no such

87
00:11:17,360 --> 00:11:23,040
thing, Buddhism is for the realization of suffering, it's the first noble truth, it's

88
00:11:23,040 --> 00:11:29,240
for the understanding of suffering and why we suffer and we don't ever have to run away

89
00:11:29,240 --> 00:11:35,160
from suffering what we have to do away with is the cause of suffering and we have to

90
00:11:35,160 --> 00:11:40,720
understand what it really is suffering and so we understand that suffering is not just

91
00:11:40,720 --> 00:11:46,200
simply a painful feeling, it is the attachment to the painful feeling which comes from

92
00:11:46,200 --> 00:11:51,440
craving which comes from clinging, wanting it to be this way, wanting it to be that way

93
00:11:51,440 --> 00:11:57,560
of course when it's not the way we want, this is when suffering arises but for a person

94
00:11:57,560 --> 00:12:03,640
who practices the Buddhist path as we can all see when we practice meditation we actually

95
00:12:03,640 --> 00:12:08,080
have to face suffering in a way that an ordinary person never would, never would dare

96
00:12:08,080 --> 00:12:17,720
to, never would be able to, we sit down and close our eyes and the pain comes up and

97
00:12:17,720 --> 00:12:25,200
rather than running away or shifting our position, we come closer to it, we come to see

98
00:12:25,200 --> 00:12:32,520
clearer about it, we come to focus on it and by focusing and keeping our mind clear saying

99
00:12:32,520 --> 00:12:37,480
to our self pain, pain is a simple reminder that this is only pain creating a clear

100
00:12:37,480 --> 00:12:45,800
thought which replaces the thoughts of disliking thoughts, the thoughts of upset to the

101
00:12:45,800 --> 00:12:51,640
point where the painful feeling is seen clearly for what it is, comes to register in

102
00:12:51,640 --> 00:12:57,840
our minds and in our memories as something which is neutral and because of our lack of clinging

103
00:12:57,840 --> 00:13:04,920
and our lack of craving in regards to the pain, there arises no suffering next time or

104
00:13:04,920 --> 00:13:11,240
the next time, we find ourselves free from the suffering which would arise from the pain

105
00:13:11,240 --> 00:13:17,680
or from the uncomfortable situation so it's completely due to lack of understanding, due

106
00:13:17,680 --> 00:13:22,600
to lack of thorough investigation of the Buddhist teaching that people would say that

107
00:13:22,600 --> 00:13:27,760
Buddhist runaway from suffering, it's got some element of truth to it but when it's

108
00:13:27,760 --> 00:13:32,840
using this as a criticism they think of suffering as things like pain or things of uncomfortable

109
00:13:32,840 --> 00:13:42,840
situations which of course are very much a part of the Buddhist path and people would

110
00:13:42,840 --> 00:13:51,040
continue this by saying, by closing our eyes, by going into our room, by taking a retreat

111
00:13:51,040 --> 00:14:01,720
and you say, you close your eyes, you can't see anything, you run away, it's easy to practice

112
00:14:01,720 --> 00:14:09,160
when you're running away in your room and not confronting anyone, so how is it that you

113
00:14:09,160 --> 00:14:13,240
are confronting uncomfortable situations if you're always running away and doing what

114
00:14:13,240 --> 00:14:20,320
they call retreats and there's an element of truth to this, it's true that when you close

115
00:14:20,320 --> 00:14:30,080
your eyes you don't see everything but it's also true that sometimes when you see too much

116
00:14:30,080 --> 00:14:36,080
you go blind, when you see too much you can't see anything and we know this very well

117
00:14:36,080 --> 00:14:41,120
from looking at a very powerful light source and when we look at the sun we can go blind

118
00:14:41,120 --> 00:14:49,760
when someone shines a bright light in your face you go blind and when I used to go out

119
00:14:49,760 --> 00:14:56,680
of my house in the winter, into the bright, into the snow, the six feet deep, snow and

120
00:14:56,680 --> 00:15:02,720
jump out of the second floor of the window, you can go blind briefly, you have to close

121
00:15:02,720 --> 00:15:07,240
your eyes and slowly, slowly open your eyes because of the blinding light reflecting

122
00:15:07,240 --> 00:15:17,920
from the ice crystals and so in the same way, not exactly the same, this is the physical

123
00:15:17,920 --> 00:15:25,960
light which we're talking about now but when we open our eyes we see too much, sure if we

124
00:15:25,960 --> 00:15:30,360
could be truly mindful with our eyes open, there'd be no reason to close our eyes but

125
00:15:30,360 --> 00:15:37,240
when we close our eyes an ordinary person who has in practice is able to focus their mind

126
00:15:37,240 --> 00:15:42,920
and fix their mind on one object, if our concentration is not strong and our eyes are open

127
00:15:42,920 --> 00:15:51,160
we'll easily flip from one thing to another and not be able to see clearly about anything,

128
00:15:51,160 --> 00:16:00,720
so I've called, excuse me, accused Buddhists of being on this, closing our eyes how can

129
00:16:00,720 --> 00:16:07,400
we see the truth, and the problem is we're like infants, we're like children, so we close

130
00:16:07,400 --> 00:16:13,720
our eyes and yes we go off into our room at first, we do this as a practice, as a means

131
00:16:13,720 --> 00:16:19,840
of training, as a training, like training wheels, you can't blame the child for using

132
00:16:19,840 --> 00:16:27,840
training wheels, he knows that if he didn't use them he would fall off the bike, so what

133
00:16:27,840 --> 00:16:36,960
we do here is simply a training for our lives, it's not running away, it's easing off or making

134
00:16:36,960 --> 00:16:42,000
it easier for us in the beginning, when we practice and practice when we're truly strong

135
00:16:42,000 --> 00:16:46,400
enough to deal with the things around us and we can open our eyes and we can walk around

136
00:16:46,400 --> 00:16:55,800
and of course meditators do do this and eventually do leave the centers, but usually in

137
00:16:55,800 --> 00:16:59,880
the end they end coming back because they realize that out there there's just a whole

138
00:16:59,880 --> 00:17:05,600
bunch of people excited and intoxicated by things which are only a source for more and

139
00:17:05,600 --> 00:17:10,080
more suffering, they see that there's society has come to take what we call society has

140
00:17:10,080 --> 00:17:14,200
come to take over most of the world and there's very little of the world which lives

141
00:17:14,200 --> 00:17:25,840
in an wholesome, you know, peaceful in a natural way, in this day and age, we try to find

142
00:17:25,840 --> 00:17:32,480
places to create our own society, a society which is based on contentment, which is based

143
00:17:32,480 --> 00:17:42,200
on renunciation, giving up, letting go, so this all is sort of a precursor to the actual

144
00:17:42,200 --> 00:17:48,760
teaching here and the things which the Lord Buddha said and it's very interesting to consider,

145
00:17:48,760 --> 00:17:58,360
the things which the Lord Buddha said are necessary to avoid and the first one is things

146
00:17:58,360 --> 00:18:12,080
like elephants or wild animals, any sort of thorn, thorn bush or something or like a deep

147
00:18:12,080 --> 00:18:20,840
pit or deep river or so on, like when you're walking and you come across a bramble bush

148
00:18:20,840 --> 00:18:25,800
while you go around it or you walk into the forest and you come to a place with fistles

149
00:18:25,800 --> 00:18:33,040
or something, you have to go around it or you come across a wild elephant while you don't

150
00:18:33,040 --> 00:18:44,400
just keep walking and say if I die, I die, you go around it, you would lose a great amount

151
00:18:44,400 --> 00:18:49,640
of benefit, you would be doing something which was when you had the choice, you'd be doing

152
00:18:49,640 --> 00:18:57,080
something which is unwise, you would choose to do something to destroy your practice,

153
00:18:57,080 --> 00:19:03,280
to destroy your ability to practice, something which would possibly lead to your great injury

154
00:19:03,280 --> 00:19:13,960
or death and of course not out of fear of death, this is important but out of a understanding

155
00:19:13,960 --> 00:19:19,920
of what is right and wrong, when you have the choice you do what is the appropriate

156
00:19:19,920 --> 00:19:31,080
thing, what wise people would appreciate or would laud, would say is a good thing to do.

157
00:19:31,080 --> 00:19:34,800
The first thing is things like wild animals and this can also mean places which have

158
00:19:34,800 --> 00:19:35,800
wild animals.

159
00:19:35,800 --> 00:19:41,560
Now of course the forest has certain wild animals and this can be a danger, you can't

160
00:19:41,560 --> 00:19:48,320
go to a place to practice where there are wild tigers or lions roaming about, it wouldn't

161
00:19:48,320 --> 00:19:54,880
be wise, it wouldn't be a conducive place and it would lead possibly to have more and more

162
00:19:54,880 --> 00:19:59,560
defilement to a rise in the mind, you would be sitting in meditation and always afraid

163
00:19:59,560 --> 00:20:06,760
or worried or always distracted by the prospect of being confronted by a wild animal or

164
00:20:06,760 --> 00:20:13,160
by something which could cause great problems and of course mosquitoes don't fall into

165
00:20:13,160 --> 00:20:19,160
this category, we can't consider mosquitoes to be wild animals, some things like animals

166
00:20:19,160 --> 00:20:24,960
which bite and so on, I have to be considered things which are, which we have to be patient

167
00:20:24,960 --> 00:20:32,320
and as I talked about last time, by the same token talking about mosquitoes we do have

168
00:20:32,320 --> 00:20:36,680
to be careful about things like malaria, we can't simply sit and let ourselves be bitten

169
00:20:36,680 --> 00:20:43,080
and bitten again, when we know there might be a potential risk of malaria which would

170
00:20:43,080 --> 00:20:50,560
compromise our practice and compromise our ability to see clearly and just to finish

171
00:20:50,560 --> 00:21:01,520
the practice, but this is the first part, the second part has to do with places and

172
00:21:01,520 --> 00:21:15,200
people which are unsuitable, for instance places like bars, places of entertainment,

173
00:21:15,200 --> 00:21:26,400
of merriment, the carnivals and festivals, places where the recreation, the activities

174
00:21:26,400 --> 00:21:33,560
which are going on which are occurring are involved with unholsomists, avoiding brothels,

175
00:21:33,560 --> 00:21:45,320
avoiding bars, avoiding drug, drug fest or places where opium dens and so on, nowadays

176
00:21:45,320 --> 00:21:52,640
it means avoiding what we call parties, avoiding these things as a practice, now this is

177
00:21:52,640 --> 00:22:00,560
not something which everyone has to do, if someone in ordinary person enjoys those things

178
00:22:00,560 --> 00:22:05,920
and likes those things then they will of course go and do those things, but this is

179
00:22:05,920 --> 00:22:10,800
a very objective thing, if a person wishes to be free from mental defilement, wishes for

180
00:22:10,800 --> 00:22:17,240
their mind to be pure, then there is a certain necessity to do away with these things,

181
00:22:17,240 --> 00:22:28,920
it's possible that by going into a pub one can stay without creating unholsomists, without

182
00:22:28,920 --> 00:22:36,240
oneself becoming intoxicated or taking intoxicating drink, but it is equally possible

183
00:22:36,240 --> 00:22:47,120
that by going in we might fall risk to temptation and we would be subject to a bad reputation

184
00:22:47,120 --> 00:22:55,520
subject to subject to reprimand, subject to being looked down upon by people who know

185
00:22:55,520 --> 00:23:03,720
better, when people are attending wild parties and going to bars and so on then we start

186
00:23:03,720 --> 00:23:10,080
to lose their interest in taking us as their friend, those people who are interested

187
00:23:10,080 --> 00:23:18,520
in spirituality, interest in peace and so on, they aren't able to stay with us and we

188
00:23:18,520 --> 00:23:23,560
aren't able to stay with them and we find ourselves being surrounded by people who are

189
00:23:23,560 --> 00:23:32,520
engaged in unholsom, unprofitable things and this is the other part is the part about

190
00:23:32,520 --> 00:23:37,760
people, people which are inappropriate, of course it's inappropriate for someone who wants

191
00:23:37,760 --> 00:23:43,240
to be free from mental defilement to surround themselves with friends who are full of mental

192
00:23:43,240 --> 00:23:51,920
defilement and who are encouraged, who are developing, who are on a path to develop and

193
00:23:51,920 --> 00:23:58,560
to increase unholsom states, people who are engaged in unholsom activities, engaged in

194
00:23:58,560 --> 00:24:08,080
things which bring about more and more craving and greed and anger and suffering and

195
00:24:08,080 --> 00:24:15,560
upset and those things which create delusion and create confusion and darkness in the

196
00:24:15,560 --> 00:24:21,280
mind and we have to avoid these people, this is the Lord Buddha's teaching on what we

197
00:24:21,280 --> 00:24:26,800
should avoid and these things, if we didn't avoid these things, if we didn't avoid things

198
00:24:26,800 --> 00:24:33,920
which were a danger to us or things which were a cause for distraction and then it would

199
00:24:33,920 --> 00:24:38,360
be easy for defilements to arise in our mind and if we didn't avoid places and people

200
00:24:38,360 --> 00:24:47,080
which were unholsom, places and people which were unprofitable which brought us no benefit

201
00:24:47,080 --> 00:24:52,520
which brought our minds down and let us down the wrong path and let us to greater and

202
00:24:52,520 --> 00:25:01,440
greater delusion and distraction and defilement, then we would clearly never become free

203
00:25:01,440 --> 00:25:09,200
from the thing which make our minds which would become pickled, our minds would go

204
00:25:09,200 --> 00:25:16,120
sour, our minds would become defiled, our minds would go bad and this is a way to stop

205
00:25:16,120 --> 00:25:21,200
our minds from going bad and to do away with the defilement in our mind, to clean up our

206
00:25:21,200 --> 00:25:30,200
minds, to clear up our minds, to bring about a very true sense of clarity of mind and

207
00:25:30,200 --> 00:25:38,120
this is the teaching in Lord Buddha's teaching on Bhairi Watsana, Bhajata, and that's

208
00:25:38,120 --> 00:25:52,600
all for today.

