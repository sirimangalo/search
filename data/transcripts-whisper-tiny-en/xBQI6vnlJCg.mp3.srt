1
00:00:00,000 --> 00:00:04,000
Jessia asks, in the tradition I have been practicing, Bhante,

2
00:00:04,000 --> 00:00:10,400
Guru Naratana has told me not to know, mental phenomena, such as thinking, et cetera,

3
00:00:10,400 --> 00:00:16,560
because it is likely you will become fixed in the words fixed in the mental noting.

4
00:00:16,560 --> 00:00:20,000
I have noticed you teach this practice, would you eventually

5
00:00:20,000 --> 00:00:24,240
have people deviate from this practice to examine the mental phenomena

6
00:00:24,240 --> 00:00:27,840
without notation simply the sensation?

7
00:00:27,840 --> 00:00:36,960
No. No, never. Not in my life, not in your life. Not in my life anyway.

8
00:00:36,960 --> 00:00:38,560
Never have never will.

9
00:00:38,560 --> 00:00:42,640
I want to make that clear because some people ask this question, they want to come and they say,

10
00:00:42,640 --> 00:00:46,480
is there some point where you'll tell us to stop and no, I won't.

11
00:00:46,480 --> 00:00:53,760
Not unless I get some instruction otherwise from my teacher, but I can't understand the point of it.

12
00:00:53,760 --> 00:00:59,200
I've heard so much about this noting thing. I used to give talks about this because it was so funny.

13
00:00:59,200 --> 00:01:05,760
I said, I've heard people tell me that noting is some at the meditation, not noting as we pass it now.

14
00:01:05,760 --> 00:01:10,960
I've heard people tell me that not noting is some at noting as we pass it now.

15
00:01:10,960 --> 00:01:20,160
I've heard people tell me that noting inhibits concentration prevents you from developing concentration.

16
00:01:20,160 --> 00:01:25,200
I've heard people tell me that noting gives you too much concentration.

17
00:01:25,200 --> 00:01:33,520
Everyone has an opinion about it and it's quite funny to listen to all these opinions.

18
00:01:33,520 --> 00:01:40,960
I mean, the Buddha's teaching is ahi bhasika. You come and see and if it does cause you to fix it on the

19
00:01:40,960 --> 00:01:44,960
words, then go and find another teacher. I've never seen that with my students. I've led

20
00:01:44,960 --> 00:01:49,200
hundreds of people through courses. I've seen my teacher lead hundreds of people through courses.

21
00:01:49,200 --> 00:01:54,400
I've never seen anybody get fixed. I have seen one person get fixated on words, not the words.

22
00:01:54,400 --> 00:02:00,160
She was using the wrong words. She started saying wisdom, wisdom. She went totally crazy.

23
00:02:00,160 --> 00:02:09,840
This woman was totally temporary and sane and the ten cups of coffee that she'd had

24
00:02:09,840 --> 00:02:14,720
the night before probably had something to do with it. The interference with people who are not

25
00:02:14,720 --> 00:02:19,840
our teachers had something to do with it and lots of things contributed to this and mostly just

26
00:02:19,840 --> 00:02:25,840
her state of mind that was not stable. I stayed with her four nights in a mental hospital

27
00:02:25,840 --> 00:02:32,320
in Chiang Mai, Thailand. It was interesting watching her wisdom. She said to me,

28
00:02:32,320 --> 00:02:38,880
you are my wisdom. Wisdom wisdom. Because she still had this idea of how to acknowledge.

29
00:02:38,880 --> 00:02:42,720
If you're just using them, if you are fixating on the words, then you're not practicing meditation

30
00:02:42,720 --> 00:02:48,160
and you should acknowledge the fixating, if you're investigating, say investigating.

31
00:02:49,200 --> 00:02:54,480
But more important in such situations where you think you're fixating or you feel you're

32
00:02:54,480 --> 00:03:00,000
fixating is to look at the feeling, look at the judgment of the practice. As I said earlier,

33
00:03:00,000 --> 00:03:08,160
that's 90% of the time, almost all the time, that's the problem. Your practice is fine,

34
00:03:08,160 --> 00:03:12,400
but then you start to doubt it because we doubt things, we doubt everything. When the doubt comes

35
00:03:12,400 --> 00:03:15,440
up, acknowledge the doubting, you'll see just like everything else it disappears.

36
00:03:16,640 --> 00:03:21,200
And you'll see that it disappears. How can you fixate on the words? You're watching the doubting,

37
00:03:21,200 --> 00:03:25,040
when you say there's a doubting, doubting, oh, the doubting's gone. That's what you'll see.

38
00:03:25,680 --> 00:03:31,520
Isn't that what we're trying to see in permanence? If all you see is the words, then you've got a problem.

39
00:03:31,520 --> 00:03:37,200
I remember when I first started it was true. When I first started all I saw were words in my head,

40
00:03:37,200 --> 00:03:42,720
very visual person, seeing, seeing, and boom, there'd be a word in my head seeing. See, I had

41
00:03:42,720 --> 00:03:48,000
crazy practice, my first practice. I heard them talk about the middle way and so I tried to

42
00:03:48,000 --> 00:03:52,400
envision this line down the middle of my body and I was trying to open this up. I saw this white

43
00:03:52,400 --> 00:03:58,160
line and I was like, trying to get into the middle way somehow. I'd have got a big headache.

44
00:03:58,960 --> 00:04:03,760
And my teacher was saying, you know, it's like, practice is like a hammer or something that's

45
00:04:03,760 --> 00:04:07,680
why I said, I got this big headache and I started to visualize my brain and I was like,

46
00:04:07,680 --> 00:04:12,880
hitting it with a hammer, trying to break it open. Oh, everything he said to me, I was just

47
00:04:12,880 --> 00:04:19,040
a visualizing it. It was pretty terrible. But there's only one answer to that. When you see

48
00:04:19,040 --> 00:04:22,800
something, say to yourself, seeing in the beginning, everything's going to be crazy. You're not going

49
00:04:22,800 --> 00:04:29,360
to practice correctly at all. No one will. But eventually, once you start to get the hang of it,

50
00:04:29,360 --> 00:04:35,280
you'll see that the noting is a very important part of the practice. I mean, if that were,

51
00:04:35,280 --> 00:04:40,240
if that were the case, then a mantra meditation would be useless. The word mantra would never

52
00:04:40,240 --> 00:04:47,440
have come into popular usage. But a mantra does have power. It does focus your mind on an object,

53
00:04:47,440 --> 00:04:51,600
whatever the object be. The Visudhi Magha would be totally wrong because the Visudhi Magha tells

54
00:04:51,600 --> 00:04:56,400
you again and again to use these words. When you look, when you practice the earth,

55
00:04:56,400 --> 00:05:04,800
the Visudhi Magha is quite clear. It says, you say to yourself,

56
00:05:04,800 --> 00:05:09,920
but the vi, but the vi, but the vi, or bhu, whatever the word is for earth. And you say that

57
00:05:09,920 --> 00:05:14,000
a hundred times a thousand times the Visudhi Magha is totally clear about the use of a mantra.

58
00:05:14,960 --> 00:05:19,760
When you practice the white casino, you say white, white, white. When you practice

59
00:05:19,760 --> 00:05:24,480
meditation, you say sub-based. As you get the hunter, when you practice, someone has a question about

60
00:05:24,480 --> 00:05:29,760
the 32 constituent parts. How do you practice it? You say kisa, al-amana kadantatajomansamnah

61
00:05:29,760 --> 00:05:35,680
ruatiyatiminsan and so on, all through the 32 parts. You actually have a mantra. And then you pick

62
00:05:35,680 --> 00:05:43,280
one and you say kisa, kisa, kisa, kisa, kisa, focusing on hair, hair, hair, just visualize it. The

63
00:05:43,280 --> 00:05:49,440
mantra certainly has a power. Now, we're not creating this, this noting is not some new meditation.

64
00:05:49,440 --> 00:05:57,280
It's actually the orthodox form of meditation before the Buddha came around. And when the Buddha

65
00:05:57,280 --> 00:06:01,920
was around, when the Buddha said kachantua, gachameti, bhajameti, when going, he knows I'm going.

66
00:06:02,560 --> 00:06:07,920
It's not, he knows the going. It's, he knows I am going, bhajameti, he knows fully.

67
00:06:08,560 --> 00:06:12,960
And completely, I am going. So there is a clear thought that arises there.

68
00:06:14,160 --> 00:06:17,920
This is not something new. The Buddha had it and then you read in the Visudhi Magha.

69
00:06:17,920 --> 00:06:24,000
It's explicitly stated that this is what they mean by meditating. It means to see the object

70
00:06:24,000 --> 00:06:29,360
just as it is, to see white as white, to see blue as blue, to see the earth as earth and so on.

71
00:06:30,320 --> 00:06:39,520
And, and the mantra is the development there. So I don't, I don't have anything to say about

72
00:06:39,520 --> 00:06:44,480
other teachers. I've heard many teachers decry this sort of practice, but you know, this is what we

73
00:06:44,480 --> 00:06:48,800
do. We decry everyone else's practice and tell you, no, no, no, do ours. I try not to.

74
00:06:50,400 --> 00:06:54,560
I would tell you, you know, if you like the practice, I teach to it. If you think that

75
00:06:54,560 --> 00:06:57,920
there's a better practice out there, go and practice that other practice. I'm not going to say

76
00:06:57,920 --> 00:07:03,520
anything about not noting or doing this or that going because body scanning techniques or so on.

77
00:07:04,080 --> 00:07:11,280
Unless I have some objective observation, like for example, I might say that in the going

78
00:07:11,280 --> 00:07:15,280
a tradition, they don't practice walking meditation or I've heard that actually they do do some

79
00:07:15,280 --> 00:07:22,240
walking, but no, no technical or formal walking meditation as far as I know. For that kind of

80
00:07:22,240 --> 00:07:27,280
example, I might give. I don't know what, how bunte, goon rat in the teachers and I might say,

81
00:07:27,280 --> 00:07:33,840
give you some advice about in regards to what you say, investigating or so on without

82
00:07:33,840 --> 00:07:42,480
notation, simply the sensation. Well, I've explained some of the benefits of noting and I've

83
00:07:42,480 --> 00:07:46,640
explained in other videos and if you agree with my explanations or you think that they have some

84
00:07:46,640 --> 00:07:51,120
merit, then try it. And if you find that it helps you, then use it to find that it doesn't help

85
00:07:51,120 --> 00:07:56,000
you. Well, I could explain to you why it's not helping you, but if you decide it's my explanations

86
00:07:56,000 --> 00:08:00,080
are no good, then go and practice. There are many different teachers in many different ways of

87
00:08:00,080 --> 00:08:05,440
approaching the Buddha's teaching that may all lead to enlightenment. I'm not going to say our way

88
00:08:05,440 --> 00:08:09,600
is the only way. I think it's the best that's why I'm doing it, whether people are visiting

89
00:08:09,600 --> 00:08:20,960
disagree. I met Bhantaguna Ratana and he's really a great wonderful being a wonderful person.

90
00:08:20,960 --> 00:08:34,000
So I have not had the chance to have done a course with him, so I'm not sure exactly what he's

91
00:08:34,000 --> 00:08:49,760
teaching, but it must have a great benefit. But I am used to the noting and I know

92
00:08:49,760 --> 00:09:04,240
by doing it that it has a great benefit for me. So I can just repeat what Bhantas said if you think

93
00:09:04,240 --> 00:09:11,040
what other teachers say is good, try it and do it when it is good for you and when this

94
00:09:11,040 --> 00:09:20,480
noting is good for you, then do that. When I started meditation with this kind of meditation,

95
00:09:22,640 --> 00:09:31,680
I had done a lot of Zen meditation before that, and I really had a strong strong version for that

96
00:09:31,680 --> 00:09:43,440
noting. I really disliked it, and I came to Bhantay and then said, oh, that is so annoying. I don't

97
00:09:43,440 --> 00:09:55,840
want to do that anymore. It's like noise in my head, and just, well, that's how it is.

98
00:09:55,840 --> 00:10:03,280
That's what is going on in your mind, noise in your head, and that was kind of convincing,

99
00:10:03,280 --> 00:10:14,480
and I noticed the noise in my head only because of the noting. Without noting, I would not

100
00:10:14,480 --> 00:10:22,000
have known, probably in a hundred years, how much is going on. But when I was so stressed,

101
00:10:22,000 --> 00:10:30,400
because I tried to note everything that is going on inside and outside, and that was just so

102
00:10:30,400 --> 00:10:38,240
overwhelming, overwhelming, much. There were such an abundance that I was so,

103
00:10:38,240 --> 00:10:49,040
it is an unallowed to say pissed off. I don't find another way. I saw a verse

104
00:10:50,560 --> 00:11:01,920
with his notes. This one too. No, all this anymore. I gave the fall to the noting,

105
00:11:01,920 --> 00:11:15,200
because I did not know better at that time. But I was forced to go through and continue noting,

106
00:11:15,200 --> 00:11:34,400
and now I know the benefits of it, and I think that the most things that are going on in our

107
00:11:34,400 --> 00:11:42,080
minds, all the thoughts and everything that we give importance, are really not important.

108
00:11:42,080 --> 00:11:54,640
There are some important things maybe coming up, but what is happening with this noting

109
00:11:54,640 --> 00:12:06,880
is that it is coming down. Noting becomes a habit, and thinking becomes less than.

110
00:12:06,880 --> 00:12:30,880
You can kind of buy this noting, have your mind have a vacation from having to do all this

111
00:12:30,880 --> 00:12:44,160
silly, useless thinking, your mind really calms down.

