1
00:00:00,000 --> 00:00:02,800
And to some extent, that's a useful crutch.

2
00:00:02,800 --> 00:00:03,640
But that's all it is.

3
00:00:03,640 --> 00:00:04,600
It's a crutch.

4
00:00:04,600 --> 00:00:06,400
It doesn't actually help you walk.

5
00:00:06,400 --> 00:00:09,200
It can help you when you're too weak to walk by yourself.

6
00:00:09,200 --> 00:00:14,360
But don't think it can do.

7
00:00:14,360 --> 00:00:15,520
I mean, I can't.

8
00:00:15,520 --> 00:00:17,440
There's no guarantee.

9
00:00:17,440 --> 00:00:20,040
And I think this is something that has to be

10
00:00:20,040 --> 00:00:25,040
made clear to all of us, and it's scary,

11
00:00:25,040 --> 00:00:30,360
that because we, I think we often fall into this sort

12
00:00:30,360 --> 00:00:34,400
of Hollywood illusion of the happily ever after.

13
00:00:34,400 --> 00:00:36,120
And we hear this a lot in spirituality.

14
00:00:36,120 --> 00:00:39,160
Everything works out in the end.

15
00:00:39,160 --> 00:00:42,640
Everything happens for a reason, and it all works out in the end.

16
00:00:42,640 --> 00:00:46,920
It will all work out in the end, which is just a bunch of rubbish.

17
00:00:46,920 --> 00:00:50,560
There's no reason to believe that any of that is true.

18
00:00:50,560 --> 00:00:53,400
Maybe it's true, but there's no reason to believe it.

19
00:00:53,400 --> 00:00:56,680
We have no evidence to support that.

20
00:00:56,680 --> 00:00:59,880
How often does it turn out all wrong, right?

21
00:00:59,880 --> 00:01:02,720
How many people's lives have just turned out miserably?

22
00:01:02,720 --> 00:01:04,680
And we say, oh, wow, you know.

23
00:01:04,680 --> 00:01:09,480
It all happened for a reason, which is just rubbish.

24
00:01:09,480 --> 00:01:19,840
It's this new age, feel good, unsubstantiated religion.

25
00:01:19,840 --> 00:01:21,480
It's not Buddhism.

26
00:01:21,480 --> 00:01:24,120
There is no guarantee that things are going to turn out, right?

27
00:01:24,120 --> 00:01:27,240
So how this applies to your question is I don't really

28
00:01:27,240 --> 00:01:31,520
have an answer necessarily for a lot of these questions.

29
00:01:31,520 --> 00:01:34,240
How do I, in my present circumstance, become enlightened?

30
00:01:34,240 --> 00:01:37,760
You just may not become enlightened, I'm sorry to say.

31
00:01:37,760 --> 00:01:39,160
You might fall away from the path.

32
00:01:39,160 --> 00:01:43,800
Many of us might, you know, unless you become a so dependent.

33
00:01:43,800 --> 00:01:45,280
There's no guarantee.

34
00:01:45,280 --> 00:01:47,280
That's why these guarantees are so important

35
00:01:47,280 --> 00:01:48,680
and why the Buddha reiterates.

36
00:01:48,680 --> 00:01:52,280
The guarantee when one has become a once one has seen the bond

37
00:01:52,280 --> 00:02:22,240
and that's a guarantee and why that's a.

