Hey, everyone. This is Adder again. I'm here with an update on our Meditation Center
project and a little bit of other news from Sarah Mangalow. Our fundraiser continues to
move along steadily. We've raised over $26,000 now and I and the rest of the Sarah Mangalow
community are very appreciative of all of the support that has shown up in this project.
So we continue to keep this going. We're continuing to try to gather some videos for
our testimonial series. If there's any way that you all want to help, please reach out
to us and we will make room for whatever is exciting for you.
I wanted to mention one thing about donating through our platform, GoFundMe. GoFundMe is
a for-profit company that is providing a really fantastic, easy-to-use service for us.
Part of their business model is that they leave an option for giving a tip to GoFundMe
to that organization. I just want to make sure everyone knows that that tip is optional.
When donating, it's not clear how to avoid doing it, but you can always select to choose
an other amount and manually enter zero. So I know there's some people that got stuck
giving a larger tip than they wanted or were confused about that process. GoFundMe does
have an offer refunds for people who have given that tip to them in error. So feel free
if you're having any trouble donating money and want to make sure that the most money is
getting to the organization and support of our projects. Feel free to reach out to us and
we can always help you. So I wanted to share a little bit about some of the other stuff going
on at Siri Mangalo. Our community is multifaceted and reaches people in different places. Some
are very involved in our YouTube live streams. Other people are very involved on our Discord
server. Others spend their time on meditation plus our community meditation app. And I wanted
to share a little bit about the book project that's been happening. We have and will continue
to be putting out some information about this project of transcribing talks from Bantayu
to Damo and putting them in written text form. We're doing a series of booklets. The
booklet on Enlightenment has been out for a while. And we've just released a booklet
on pilgrimage talks. These come from about one year ago exactly about a dozen of us had
gone to India and Nepal to visit the holy sites of Buddhist pilgrimage. And their Bantayu
gave a series of talks at each of the major pilgrimage sites. And this is a really great
series of talks that are all available on YouTube. But we've also now got them written out
in booklet form. So you can get that on our website. We'll have links in the video description
here. And this, the booklet project, this book project is an ongoing one. And we can always
use more people in, we have, if you want to contribute a little bit, it's pretty easy
to jump into the project and do a little bit of transcribing for it. But what we really
need right now is a lot of editing. So if there are people that want to get on and just
edit the text that people have transcribed or that we've gotten some automated transcription
for, we could use some more hands editing. So we'll have information in the video description
about that. And you can feel free to hop on our Discord server and help with that. And
if not, I do encourage you to check out that series of talks. It's really, really amazing
and valuable. A lot of wise words put together at some of the most important sites in Buddhist
history. We also have a team that's sort of following the, the production of these English
language booklets that is translating them into Spanish. So we'll make sure to keep you
updated with info on that, that they've got the Enlightenment booklet translated and will
be right behind with this, this pure pilgrimage booklet. So if you are native Spanish speaker,
I encourage you to take a look at that. And we can always use more translators to help
translate these teachings into, into various languages.
Well, I think that's about all I have for today. Like I said, please stay in touch with us
in the community. The Discord server is a great place to, to get involved. And if you
have any questions about the fundraiser or any of the things that we're doing, you
can shoot a message, you know, on, go fund me on the contact page from our website or
hop on the Discord server and reach out to us. I wish you all the best. I wish you health
and happiness. Thank you all. Take care.
