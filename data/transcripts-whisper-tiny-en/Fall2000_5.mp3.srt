1
00:00:00,000 --> 00:00:08,800
We have not finished with the basics of people's honor yet.

2
00:00:10,800 --> 00:00:19,400
So yesterday, we ended with when we to observe.

3
00:00:20,400 --> 00:00:27,800
So we are to observe the five aggregates of clinging when they are present.

4
00:00:27,800 --> 00:00:31,200
And so the present object is the most important.

5
00:00:31,800 --> 00:00:37,200
Now, the next question is how are we to observe?

6
00:00:39,600 --> 00:00:48,800
Now, you all know how we are to observe the five aggregates of clinging.

7
00:00:48,800 --> 00:01:05,800
Now, we are to observe so that we do not add anything unreal to the object and do not take away anything real from the object.

8
00:01:05,800 --> 00:01:10,800
That is how we must observe or how we must watch.

9
00:01:10,800 --> 00:01:29,800
So in order not to add anything and not to take away anything from the object, we try to take the object as it is.

10
00:01:29,800 --> 00:01:37,800
That means we just take what it is and we do not think about the object.

11
00:01:37,800 --> 00:01:45,800
We do not reflect upon the object. We do not speculate on the object. We do not analyze it.

12
00:01:45,800 --> 00:01:48,800
We do not make judgments on it.

13
00:01:48,800 --> 00:01:55,800
But we just take the object as it is presented to us through the six cents dollars.

14
00:01:55,800 --> 00:02:11,800
Taking the object as it is without any additions and omissions is in accordance with the advice given by the Buddha himself.

15
00:02:11,800 --> 00:02:25,800
Once an acity came to the Buddha, actually when he came Buddha was on his arms round in a village.

16
00:02:25,800 --> 00:02:31,800
So that acity wanted to see the Buddha immediately.

17
00:02:31,800 --> 00:02:44,800
He went to the monastery and when he was told that Buddha had gone out for arms round to the village, he followed the footsteps of the Buddha.

18
00:02:44,800 --> 00:02:54,800
And when he met the Buddha, he asked the Buddha to teach him in brief so that he could go to a secluded place and practice meditation.

19
00:02:54,800 --> 00:03:02,800
Then Buddha refused. Buddha said, it is not the time for giving a talk or giving instruction on meditation.

20
00:03:02,800 --> 00:03:07,800
He is on arms round. So he rejected.

21
00:03:07,800 --> 00:03:17,800
But the acity was persistent. He requested for the second time and he requested for the third time and the Buddha turned him down again.

22
00:03:17,800 --> 00:03:27,800
At last, he said, Dante, who knows what will happen to us to you and to me, we may die next moment.

23
00:03:27,800 --> 00:03:35,800
So please teach me something so that I can follow in your advice, I can practice.

24
00:03:35,800 --> 00:03:42,800
So Buddha gave him a very short instruction.

25
00:03:42,800 --> 00:03:48,800
But hearing that instruction, he understood what the Buddha meant.

26
00:03:48,800 --> 00:03:55,800
And so right away, he went to a secluded place and practiced meditation.

27
00:03:55,800 --> 00:03:59,800
And he became an arahant.

28
00:03:59,800 --> 00:04:09,800
But the unfortunate thing was after he became an arahant, he wanted to become a monk.

29
00:04:09,800 --> 00:04:19,800
So he was in search of robes when he was attacked by a cow and was killed.

30
00:04:19,800 --> 00:04:26,800
But he attained arahant ship or he became arahant before he died.

31
00:04:26,800 --> 00:04:41,800
So the very short instruction given by the Buddha was of great benefit to that acetic.

32
00:04:41,800 --> 00:04:53,800
The advice given to him by the Buddha was, let there be just seeing with regard to what is seeing.

33
00:04:53,800 --> 00:04:57,800
Let there be just hearing with regard to what is heard.

34
00:04:57,800 --> 00:05:09,800
Let there be just sensing as smell, taste and touch with regard to what is sensed.

35
00:05:09,800 --> 00:05:16,800
And let there be just thinking with regard to what is thought of.

36
00:05:16,800 --> 00:05:19,800
So this is the advice given by the Buddha.

37
00:05:19,800 --> 00:05:26,800
So that means when you see something, you stop it, you stop it just seeing.

38
00:05:26,800 --> 00:05:36,800
And do not come to the judgment that it is good or it is bad, that I like it or I don't like it and so on.

39
00:05:36,800 --> 00:05:44,800
So with regard to hearing also, when you hear something, you just stop it hearing.

40
00:05:44,800 --> 00:05:54,800
And not coming to the judgment that it is sweet, it is pleasant, it is ugly and so on.

41
00:05:54,800 --> 00:06:01,800
So this taught him to take the object as it is without any additions.

42
00:06:01,800 --> 00:06:14,800
And this he practiced and since he had accumulated practice in his first lives, he was to gain a relationship.

43
00:06:14,800 --> 00:06:25,800
Just following this short advice of the Buddha.

44
00:06:25,800 --> 00:06:41,800
In the book called The Heart of Buddhist Meditation, calls this kind of observing, paying bear attention.

45
00:06:41,800 --> 00:06:48,800
And he explains bear attention in his book.

46
00:06:48,800 --> 00:07:02,800
He said, bear attention is the clear and single-minded awareness of what actually happens to us and in us.

47
00:07:02,800 --> 00:07:08,800
At the successive moment of perception.

48
00:07:08,800 --> 00:07:27,800
It is called bear because it attains just to the bear facts of a perception as presented either through the five physical senses or through the mind which for Buddhist thought constitutes the sixth sense.

49
00:07:27,800 --> 00:07:38,800
When attending to that 6-4 sense impression, attention or mindfulness is kept to a bear registering of the facts observed.

50
00:07:38,800 --> 00:07:47,800
So it is just registering the facts observed without any additions of our own.

51
00:07:47,800 --> 00:08:01,800
Without reacting to them by deed, speech or by mental comment, which may be one of self-reference that is like, dislike, etc., or judgment or reflection.

52
00:08:01,800 --> 00:08:10,800
So that means we do not make judgments on the object, we do not reflect on an object, we do not speculate on the object.

53
00:08:10,800 --> 00:08:20,800
We just pay this simple and bear attention to the object without any additions.

54
00:08:20,800 --> 00:08:26,800
The practice of bear attention, any such comments arise in one's mind.

55
00:08:26,800 --> 00:08:46,800
So if when watching the object, you may have made the judgment as like or dislike or you have made reflection on it, then the themselves are made objects of bear attention.

56
00:08:46,800 --> 00:08:57,800
Now you make a judgment, this is good, this is better, this is ugly, this is beautiful. Then that judgment should be made the object of bear attention in its turn.

57
00:08:57,800 --> 00:09:14,800
And neither repudiated nor pursued, so those additions or judgments you made, you are not to repudiate, you are not to reject them and you are not to follow them.

58
00:09:14,800 --> 00:09:26,800
Neither repudiated nor pursued, but dismissed after a brief mental note has been made of them.

59
00:09:26,800 --> 00:09:29,800
So this is what we are doing at the retreat.

60
00:09:29,800 --> 00:09:41,800
So we take an object and we try to be neutral towards that object, we try to just see it, just hear it and so on.

61
00:09:41,800 --> 00:10:02,800
So if we made an speculation on it, then that speculation is made the object of our attention. Then we will say to ourselves, speculating, speculating, or reflecting, reflecting, or judgment, judgment and so on.

62
00:10:02,800 --> 00:10:09,800
So that is what the venerable is explaining in his book.

63
00:10:09,800 --> 00:10:17,800
Once again, you can summarize.

64
00:10:17,800 --> 00:10:27,800
Now we can take a copying machine as an example of bear attention.

65
00:10:27,800 --> 00:10:42,800
So you make some copies and the machine can just make the copy and it cannot add anything, which is not on the sheet or it cannot omit anything that is on the sheet.

66
00:10:42,800 --> 00:10:45,800
So what is on the sheet? It copies.

67
00:10:45,800 --> 00:11:01,800
So when we pay attention to the objects, we are copying machines, we just copy the object as it is without any additions or missions.

68
00:11:01,800 --> 00:11:24,800
Or we can take a photographer or a camera as an example of bear attention. So if a photographer can take the picture, but he cannot add anything to the picture or he cannot omit anything from the picture, not like an artist.

69
00:11:24,800 --> 00:11:36,800
So when the artist is drawing a scene and he may put in something he likes on the picture, he is free to do that, or he may omit something out of the picture.

70
00:11:36,800 --> 00:11:43,800
But if a photographer cannot do that, his picture will be the exact copy of what is there.

71
00:11:43,800 --> 00:11:56,800
So in the same way, when we watch objects, we are just taking the picture as it is at without any additions or no missions.

72
00:11:56,800 --> 00:12:15,800
And it is easy to say, stop it just seeing with regard to what is seeing and so on, but in practice it is very difficult. You need strong mindfulness and concentration to just take the object as it is.

73
00:12:15,800 --> 00:12:32,800
Suppose you hear a noise, do you stop at just hearing, hearing the noise, or you go over to saying it is a noise of a car, or it is a noise of an airplane.

74
00:12:32,800 --> 00:12:49,800
And if you are familiar with cars, you even know that this is the noise of a Toyota, or it is a noise of a Honda and so on. So before we know what we are doing, we have already made judgments on to the object.

75
00:12:49,800 --> 00:13:09,800
So in practice it is very difficult, but with perseverance and with strong mindfulness and concentration it is possible.

76
00:13:09,800 --> 00:13:21,800
If you want to make a test, you can take a clock, that sounds at every hour, say 1, 2, 3, 4 and so on.

77
00:13:21,800 --> 00:13:41,800
So you listen to that striking and pay attention to the sound and see if you know that it is 7 sounds or 8 sounds or 9 sounds and so on.

78
00:13:41,800 --> 00:13:57,800
So if you know that there are 7 sounds or 8 sounds and 9 sounds, you are not taking the object as it is. You are adding something onto the sound, this is the first, this is the second, this is the third, 1, 2, 3, 4 and so on.

79
00:13:57,800 --> 00:14:14,800
But if you hear just the sound and you don't know how many times it has sounded, then you are right in the practice of mindfulness and you are taking the sound object as it is without any additions.

80
00:14:14,800 --> 00:14:32,800
So when we observe the object at the present moment, we must observe so that we do not add anything and rear onto the object and we must not take away anything rear from the object.

81
00:14:32,800 --> 00:14:52,800
We must take the object as it is without any additions or omissions or we must be just bare attention to the object without the subjective additions.

82
00:14:52,800 --> 00:15:16,800
Now what will happen if we do not observe them, if we do not observe the object at the present moment, if we do not observe the object at the present moment, if we do not apply mindfulness to the object, then we will always cling to that object.

83
00:15:16,800 --> 00:15:25,800
We will always, like the object, we will always have a wrong view about the object.

84
00:15:25,800 --> 00:15:38,800
So if we do not observe them, if we do not pay attention to them, if we do not practice mindfulness towards them, then we will cling to them.

85
00:15:38,800 --> 00:15:53,800
So when we cling to them, we think wrongly that things are permanent, things are good and things are substantial.

86
00:15:53,800 --> 00:16:16,800
So this kind of cleaning will lead to acting on, will lead to doing sometimes wholesome and sometimes unwholesome actions, and these actions will lead to reaction in the form of rebut in the future.

87
00:16:16,800 --> 00:16:27,800
So if we do not observe them, we are actually prolonging the samsara or the round of rebut.

88
00:16:27,800 --> 00:16:39,800
Now we always say we want to shorten the samsara or we want to cut the samsara, we want to get out of samsara.

89
00:16:39,800 --> 00:16:49,800
But if we do not observe the objects, if we do not apply mindfulness to the objects, we are doing the opposite of what we want to do.

90
00:16:49,800 --> 00:17:13,800
We are prolonging this round of rebut because if we do not observe, we will always cling to them and when that is clinging, we cannot stop at just clinging, but we will go over to doing something and then we get the results.

91
00:17:13,800 --> 00:17:24,800
So when we see an object, that is a beautiful object, we are attached to it, we light it.

92
00:17:24,800 --> 00:17:32,800
That means we cling to it with attachment, with craving.

93
00:17:32,800 --> 00:17:46,800
Once we cling to this object with attachment, then the attachment will always be with the object.

94
00:17:46,800 --> 00:18:01,800
That means when we see this object again, this object, we will see this object with our attachment inlaid in the object.

95
00:18:01,800 --> 00:18:15,800
Or if we recall this object, it will appear in our minds with the attachment inlaid, because we have invested the attachment in the object.

96
00:18:15,800 --> 00:18:37,800
That means actually we cannot put attachment in the object, but in our minds, we connect these two together and so it could be said that we have invested, we have put the attachment into the object.

97
00:18:37,800 --> 00:18:49,800
That is what is called latency of mental defalments in objects.

98
00:18:49,800 --> 00:18:58,800
Again, we can take a photograph as an example of latency in the object.

99
00:18:58,800 --> 00:19:09,800
So when a photograph is taken, then the objects, therefore taken by the photograph are on the picture.

100
00:19:09,800 --> 00:19:27,800
So you look at it next time, then those pictures will be, those things will be in the picture, because they are impressed on the paper.

101
00:19:27,800 --> 00:19:34,800
And you cannot remove or you cannot make it look different.

102
00:19:34,800 --> 00:19:43,800
Now, the other day, we went to take a passport picture of ourselves.

103
00:19:43,800 --> 00:19:47,800
So there is a machine at the store.

104
00:19:47,800 --> 00:19:55,800
We have to put some money and then sit in front of the camera.

105
00:19:55,800 --> 00:20:05,800
So I sat down on the store and then put the money, and then it took pictures.

106
00:20:05,800 --> 00:20:11,800
But I did not know that it would take four pictures, one after another.

107
00:20:11,800 --> 00:20:20,800
I thought that just by taking one, it could produce four pictures.

108
00:20:20,800 --> 00:20:25,800
So after once, I thought it was finished.

109
00:20:25,800 --> 00:20:31,800
And then I saw the light again and it took another picture.

110
00:20:31,800 --> 00:20:38,800
But at the time, I was smiling.

111
00:20:38,800 --> 00:20:44,800
And then the next picture, I was getting up.

112
00:20:44,800 --> 00:20:54,800
So those pictures are with me, so I cannot change those pictures, because once they are taken, they are taken.

113
00:20:54,800 --> 00:21:04,800
So if I take those pictures out and look at them, I will see myself smiling or getting up, or just just the ropes and so on.

114
00:21:04,800 --> 00:21:08,800
So this is like latency in the objects.

115
00:21:08,800 --> 00:21:14,800
So we put the attachment into the object.

116
00:21:14,800 --> 00:21:36,800
So when we see the object again, or when we recall it, then it comes with those, the mental defense, the departments embedded in the object.

117
00:21:36,800 --> 00:21:41,800
So if we do not observe them, we will cling to them.

118
00:21:41,800 --> 00:21:47,800
And when there is clinging, there is action, and then when there is action, there will be reaction.

119
00:21:47,800 --> 00:21:52,800
Now what will happen if we observe them? This is the opposite.

120
00:21:52,800 --> 00:21:57,800
So if we observe them, then we will not cling to them.

121
00:21:57,800 --> 00:22:07,800
Because we try to be mindful of the object, or we make mental nodes as seeing, seeing, hearing, hearing and so on.

122
00:22:07,800 --> 00:22:16,800
So when we make notes of them, or when we apply mindfulness to them, then there is no chance for clinging to a rise.

123
00:22:16,800 --> 00:22:21,800
Or there is no chance for us to cling to the object.

124
00:22:21,800 --> 00:22:29,800
So there will be no clinging if we observe them, if we observe the five aggregates of clinging.

125
00:22:29,800 --> 00:22:35,800
When there is no clinging, there will be no acting following the clinging.

126
00:22:35,800 --> 00:22:39,800
When there is no acting, there will be no reaction.

127
00:22:39,800 --> 00:22:53,800
So in this way, we shorten the samsara.

128
00:22:53,800 --> 00:22:59,800
So when we do not cling to them, we see them as they are.

129
00:22:59,800 --> 00:23:02,800
We see them that they come and go.

130
00:23:02,800 --> 00:23:12,800
And so they are impermanent, and they are suffering in the sense that they are oppressed by rising and falling.

131
00:23:12,800 --> 00:23:17,800
And there is no control over them.

132
00:23:17,800 --> 00:23:25,800
And so we see them as impermanent suffering and insubstantial or non-soul.

133
00:23:25,800 --> 00:23:33,800
That means we see the three general characteristics of these objects.

134
00:23:33,800 --> 00:23:45,800
So when we see the general characteristics of these objects, we are well on the way to attainment of nibana.

135
00:23:45,800 --> 00:23:57,800
So if we want to stop this flow of rebirth and death, or if we want to stop this samsara, we must observe the objects.

136
00:23:57,800 --> 00:24:02,800
We must be mindful of the objects, or we must practice mindfulness.

137
00:24:02,800 --> 00:24:12,800
If we don't want the samsara to stop, but let it go on and on, then we do not observe them.

138
00:24:12,800 --> 00:24:14,800
We do not practice mindfulness.

139
00:24:14,800 --> 00:24:39,800
So those of us who are practicing mindfulness are actually at least trying to stop the flow of the rebirth and death in this long samsara.

140
00:24:39,800 --> 00:24:44,800
Now these are the basics of vipasana.

141
00:24:44,800 --> 00:25:02,800
So I am telling you the basics of vipasana in some detail, because as practitioners, you need to understand the basics of vipasana so that you can practice even by yourself.

142
00:25:02,800 --> 00:25:15,800
And also you can understand that what you are doing is right or wrong.

143
00:25:15,800 --> 00:25:30,800
And also I want you not only to understand the basics of vipasana, but to be able to explain these to other people as well.

144
00:25:30,800 --> 00:25:42,800
Because you may have friends and they may want to know what you do when you practice vipasana, and I want you to be able to tell them the basics of vipasana.

145
00:25:42,800 --> 00:25:56,800
So it is important that those who practice vipasana know these basics of vipasana, what vipasana is, what we do when we practice vipasana.

146
00:25:56,800 --> 00:26:09,800
And when we observe what must we observe, and why do we have to observe, when are we do observe, how are we do observe.

147
00:26:09,800 --> 00:26:21,800
So the what, why, when, how of vipasana, I want you to know.

148
00:26:21,800 --> 00:26:33,800
So when you know these details of the basics of vipasana, then when you practice, then you can be confident of your practice.

149
00:26:33,800 --> 00:26:49,800
And you know what you are doing is right or wrong.

150
00:26:49,800 --> 00:26:58,800
So if you want to copy of this node, you can ask this stuff for a copy.

151
00:26:58,800 --> 00:27:16,800
Now next, what is important is when you practice meditation, you balance what are called the faculties.

152
00:27:16,800 --> 00:27:33,800
Now, the other day, when I explain to you about the phrase, ardent, clearly comprehending and mindful, I told you that there are components of this practice.

153
00:27:33,800 --> 00:27:50,800
So by ardent is meant effort, by clearly comprehending is meant understanding, and by mindful is meant mindfulness.

154
00:27:50,800 --> 00:28:02,800
So three components are mentioned there, and I told you that the sub commentary said that we must take samadhi also.

155
00:28:02,800 --> 00:28:12,800
So we get four components of the practice, effort, mindfulness, concentration, and understanding.

156
00:28:12,800 --> 00:28:17,800
And also I told you that we need confidence of faith also.

157
00:28:17,800 --> 00:28:23,800
We need to have faith in the Buddha and his teachings and in the practice.

158
00:28:23,800 --> 00:28:28,800
So altogether, there are five components of the practice.

159
00:28:28,800 --> 00:28:35,800
And these five components are called controlling faculties.

160
00:28:35,800 --> 00:28:41,800
And these five controlling faculties must be well balanced.

161
00:28:41,800 --> 00:28:53,800
One must not be in excess of the others.

162
00:28:53,800 --> 00:28:59,800
So I want you to know the names of these faculties in Pali also.

163
00:28:59,800 --> 00:29:03,800
So the first is sadha.

164
00:29:03,800 --> 00:29:06,800
So it is faith or confidence.

165
00:29:06,800 --> 00:29:12,800
And the second is we rea, effort or energy.

166
00:29:12,800 --> 00:29:16,800
And the third is sati, mindfulness.

167
00:29:16,800 --> 00:29:25,800
And the fourth is samadhi, concentration, and the fifth panea, understanding or wisdom.

168
00:29:25,800 --> 00:29:49,800
So I want you to know both Pali and their English translation.

169
00:29:49,800 --> 00:30:17,800
And when we practice meditation, these five faculties,

170
00:30:17,800 --> 00:30:26,800
or these five mental states must be working together and harmoniously.

171
00:30:26,800 --> 00:30:37,800
So it is important that they are well balanced because if one of them is working too much,

172
00:30:37,800 --> 00:30:42,800
then the other faculties cannot do their respective functions.

173
00:30:42,800 --> 00:30:51,800
And so the mechanism of meditation will not work.

174
00:30:51,800 --> 00:30:58,800
Among these five, the mindfulness is very important.

175
00:30:58,800 --> 00:31:03,800
And it is said that mindfulness should be strong everywhere.

176
00:31:03,800 --> 00:31:13,800
That means mindfulness needs to be always strong.

177
00:31:13,800 --> 00:31:20,800
Among the five faculties, the three faith, energy, and wisdom.

178
00:31:20,800 --> 00:31:24,800
Sadha, we rea, and panea.

179
00:31:24,800 --> 00:31:32,800
So these three factors favor restlessness.

180
00:31:32,800 --> 00:31:41,800
So when you have faith, when you have confidence, then you are active,

181
00:31:41,800 --> 00:31:48,800
and you are like something floating with the waves.

182
00:31:48,800 --> 00:31:50,800
So you are shaking, something like that.

183
00:31:50,800 --> 00:31:52,800
Your mind is shaking.

184
00:31:52,800 --> 00:31:57,800
And energy, when you make effort, there is some kind of restlessness.

185
00:31:57,800 --> 00:32:03,800
And panea understanding was when you understand the objects clearly,

186
00:32:03,800 --> 00:32:08,800
and when you see the true nature and so on, you become agitated actually.

187
00:32:08,800 --> 00:32:16,800
So these three mental factors favor agitation or restlessness.

188
00:32:16,800 --> 00:32:30,800
If the mindfulness is not strong, mindfulness cannot harmonize these mental factors.

189
00:32:30,800 --> 00:32:38,800
So when mindfulness is strong, it can protect the mind against restlessness

190
00:32:38,800 --> 00:32:43,800
into which the mind may fall by the influence of those faculties,

191
00:32:43,800 --> 00:32:47,800
which inclined to aid namely faith, energy, and wisdom.

192
00:32:47,800 --> 00:32:55,800
So when mindfulness is strong, it can protect the mind from falling into restlessness

193
00:32:55,800 --> 00:33:01,800
by the influence of faith, energy, and wisdom.

194
00:33:01,800 --> 00:33:07,800
And also it can protect the mind against lessitude.

195
00:33:07,800 --> 00:33:15,800
That means laziness into which mind may fall by the influence of the faculty

196
00:33:15,800 --> 00:33:18,800
that inclines to aid namely concentration.

197
00:33:18,800 --> 00:33:24,800
Now when concentration is too much, you tend to become lazy.

198
00:33:24,800 --> 00:33:31,800
Now your mind is concentrated, and so when mind becomes concentrated,

199
00:33:31,800 --> 00:33:40,800
and it is not protected by strong mindfulness, then you tend to lose energy.

200
00:33:40,800 --> 00:33:49,800
So when energy level falls, and concentration is stronger than energy,

201
00:33:49,800 --> 00:33:55,800
you begin to feel sleepy, or you begin to feel lazy.

202
00:33:55,800 --> 00:34:01,800
So concentration favors laziness.

203
00:34:01,800 --> 00:34:13,800
But when sati is strong, sati is there, it can protect the mind from falling into laziness through concentration.

204
00:34:13,800 --> 00:34:24,800
That is why a strong sati is needed everywhere.

205
00:34:24,800 --> 00:34:34,800
Our ancient teachers compare mindfulness to salt in all dishes,

206
00:34:34,800 --> 00:34:40,800
and to a minister who is clever in all affairs.

207
00:34:40,800 --> 00:34:47,800
Now in eastern dishes, salt is an important ingredient.

208
00:34:47,800 --> 00:34:53,800
If something is legging in salt, we say it is tasteless.

209
00:34:53,800 --> 00:34:58,800
Although nowadays we are afraid of salt, and we, but less salt in the dishes,

210
00:34:58,800 --> 00:35:02,800
still we want it to be a little salty.

211
00:35:02,800 --> 00:35:12,800
So like shaking salt in all dishes, we need mindfulness or sati in all activities.

212
00:35:12,800 --> 00:35:20,800
So sati is required everywhere, and it is also compared to a minister who can do everything.

213
00:35:20,800 --> 00:35:30,800
Who can do giving advice to the king, who can do maybe fighting, and who can do other things as well.

214
00:35:30,800 --> 00:35:39,800
So that means that sati is like a, like a prince here.

215
00:35:39,800 --> 00:35:46,800
So we need sati everywhere, we need strong, strong sati or strong mindfulness everywhere.

216
00:35:46,800 --> 00:35:58,800
And only with strong mindfulness can we keep the other components or other faculties in their proper places,

217
00:35:58,800 --> 00:36:06,800
and let them do their respective functions properly.

218
00:36:06,800 --> 00:36:15,800
And when there is no mindfulness, there can be no encouraging of the mind when it is low in spirits.

219
00:36:15,800 --> 00:36:21,800
And there is no restraining of the mind when it is too active.

220
00:36:21,800 --> 00:36:30,800
But when there is mindfulness or sati, then it can harmonize the other factors.

221
00:36:30,800 --> 00:36:38,800
And so when it is fit to spur the mind, it would spur the mind.

222
00:36:38,800 --> 00:36:42,800
And when it is fit to restrain the mind, it would restrain the mind.

223
00:36:42,800 --> 00:36:54,800
So among the five faculties, sati is the very important factor.

224
00:36:54,800 --> 00:37:03,800
And it is called in the heart of Buddhist meditation, the harmonizing factor, or harmonizing faculty.

225
00:37:03,800 --> 00:37:16,800
Because it harmonizes the other faculties or other components of the practice.

226
00:37:16,800 --> 00:37:32,800
So among the five aggregates, if, let us say, sata is too strong, then the other factors cannot do their respective functions.

227
00:37:32,800 --> 00:37:42,800
Now the function of effort or energy is exerting or supporting or encouraging.

228
00:37:42,800 --> 00:37:47,800
The function of mindfulness is firmly establishing.

229
00:37:47,800 --> 00:37:57,800
The function of concentration is not distracting and the function of understanding is seeing.

230
00:37:57,800 --> 00:38:11,800
Now when one of these factors is too much, is in excess, then the other factors cannot do their functions properly.

231
00:38:11,800 --> 00:38:19,800
So when they cannot do their functions, probably meditation becomes bad.

232
00:38:19,800 --> 00:38:38,800
So it is important that these five mental faculties be kept in balance.

233
00:38:38,800 --> 00:38:46,800
So when sata of faith or confidence is too strong, and the others are weak,

234
00:38:46,800 --> 00:38:53,800
then we must do something to make sata less strong.

235
00:38:53,800 --> 00:39:07,800
And the commentary suggests that we reflect on the nature of the things when there is too much sata.

236
00:39:07,800 --> 00:39:17,800
And also we are not to do the reflection that causes sata to become strong.

237
00:39:17,800 --> 00:39:27,800
Now a person who has strong sata is happy with thinking about the Buddha, the Dhamma and the Sangha and so on.

238
00:39:27,800 --> 00:39:46,800
So if sata is too strong and others are weak, then he must do something to diminish his sata of faith by not reflecting upon the Buddha and so on.

239
00:39:46,800 --> 00:39:58,800
And the commentary said that here the story of the elder wakali is the illustration.

240
00:39:58,800 --> 00:40:09,800
So the commentary just said this, but the commentary did not give the story of wakali here.

241
00:40:09,800 --> 00:40:14,800
So we have to find the story somewhere else.

242
00:40:14,800 --> 00:40:25,800
Now this elder wakali was first a lay person, and when he first saw the Buddha,

243
00:40:25,800 --> 00:40:31,800
he was so pleased with the beauty of the body of the Buddha.

244
00:40:31,800 --> 00:40:36,800
So he was very glad, and then he decided to become a monk.

245
00:40:36,800 --> 00:40:44,800
Because when he become a monk, he could be near the Buddha and he could always see the Buddha.

246
00:40:44,800 --> 00:40:51,800
So he renounced the world and became a monk.

247
00:40:51,800 --> 00:40:59,800
And after he became a monk, he always stayed in a place where he could see the Buddha.

248
00:40:59,800 --> 00:41:08,800
And what he did was just looking at the Buddha and then be happy.

249
00:41:08,800 --> 00:41:14,800
So he did not study, he did not practice meditation.

250
00:41:14,800 --> 00:41:17,800
He just looked at the Buddha.

251
00:41:17,800 --> 00:41:25,800
But a new day, but Buddha waited for his wisdom to mature.

252
00:41:25,800 --> 00:41:38,800
Now Buddha did not teach when a wisdom is not mature, because it would be a waste of time and energy to teach a person who could not accept his teachings.

253
00:41:38,800 --> 00:41:53,800
So Buddha waited for some time, and then when he knew that the wisdom of wakali had become mature,

254
00:41:53,800 --> 00:42:04,800
he said to him one day, wakali, what is the use of looking at the stinking body?

255
00:42:04,800 --> 00:42:12,800
He who sees the Dharma sees me, and who sees me sees the Dharma.

256
00:42:12,800 --> 00:42:19,800
Although Buddha told this to him, he still looked at the Buddha.

257
00:42:19,800 --> 00:42:27,800
He still did not get away from that place.

258
00:42:27,800 --> 00:42:32,800
So he continued looking at the Buddha.

259
00:42:32,800 --> 00:42:42,800
So later Buddha thought, without giving him a shock, he will not come to understanding.

260
00:42:42,800 --> 00:42:50,800
So when the rainy season retreat was drawing near, a Buddha went to Rajagaha,

261
00:42:50,800 --> 00:42:58,800
and then he drove away wakali, saying, wakali, go away, wakali, go away.

262
00:42:58,800 --> 00:43:04,800
So wakali was not able to stay with the Buddha, and so he was very sad,

263
00:43:04,800 --> 00:43:08,800
because he did not get to see the Buddha for three months.

264
00:43:08,800 --> 00:43:13,800
So when he could not get the chance to see the Buddha,

265
00:43:13,800 --> 00:43:16,800
he decided to take his own lives.

266
00:43:16,800 --> 00:43:23,800
He said to him, sir, what is the use of being alive if I cannot see the Buddha?

267
00:43:23,800 --> 00:43:28,800
So I will throw myself down from the top of the mountain.

268
00:43:28,800 --> 00:43:38,800
So he went to the mountain called Vulture Peak, Kijakuda.

269
00:43:38,800 --> 00:43:44,800
And then he was trying to throw himself down.

270
00:43:44,800 --> 00:43:55,800
So at that moment, Buddha saw him, and then Buddha sent his radiant image to the wakali.

271
00:43:55,800 --> 00:44:01,800
So when wakali saw the image of the Buddha, he was glad,

272
00:44:01,800 --> 00:44:06,800
and so all his suffering disappeared right away.

273
00:44:06,800 --> 00:44:16,800
So when he was in that condition, Buddha preached to him one verse.

274
00:44:16,800 --> 00:44:28,800
So when he, after that verse, Buddha said, come wakali, look upon the dhatagata,

275
00:44:28,800 --> 00:44:40,800
I will, I will drag you up as one extricate,

276
00:44:40,800 --> 00:44:44,800
an elephant sunk in the mud.

277
00:44:44,800 --> 00:44:46,800
That is what Buddha said.

278
00:44:46,800 --> 00:44:52,800
I like that that in pali is very good, but in English,

279
00:44:52,800 --> 00:44:55,800
it doesn't sound very good.

280
00:44:55,800 --> 00:44:58,800
Come wakali, look upon the dhatagata.

281
00:44:58,800 --> 00:45:01,800
That means look upon me, look upon the dhatagata.

282
00:45:01,800 --> 00:45:03,800
I will drag you up.

283
00:45:03,800 --> 00:45:06,800
I will lift you up.

284
00:45:06,800 --> 00:45:13,800
Just as one extricate, an elephant that is sunk in the mud.

285
00:45:13,800 --> 00:45:16,800
So when he heard this, he was very glad.

286
00:45:16,800 --> 00:45:21,800
Now Buddha was calling me, and I was, I get the chance to see the Buddha.

287
00:45:21,800 --> 00:45:30,800
So then he did not know where he was going, but he leaped into the air.

288
00:45:30,800 --> 00:45:34,800
And it just said that one foot was on the ground and one was in the air.

289
00:45:34,800 --> 00:45:44,800
And then Buddha, he got a lot of PD at that time, and then he,

290
00:45:44,800 --> 00:45:53,800
he practiced wipasana on PT, and while he was,

291
00:45:53,800 --> 00:45:59,800
he said, in the air, he became an errand.

292
00:45:59,800 --> 00:46:04,800
So after he became an errand, he bowed onto the Buddha.

293
00:46:04,800 --> 00:46:10,800
And later on, he was appointed the foremost among those who,

294
00:46:10,800 --> 00:46:13,800
who have strong faith in the Buddha.

295
00:46:13,800 --> 00:46:19,800
So this is the story of the elder wakali.

296
00:46:19,800 --> 00:46:23,800
So when he had too much sada, he couldn't do anything else.

297
00:46:23,800 --> 00:46:27,800
He just want to, he just saw the look at the Buddha.

298
00:46:27,800 --> 00:46:34,800
So Buddha had to shake him up, and then when that sada becomes low,

299
00:46:34,800 --> 00:46:39,800
because he was sorry at that time, and Buddha taught him.

300
00:46:39,800 --> 00:46:49,800
And so he was able to reach at a handship.

301
00:46:49,800 --> 00:46:53,800
What if we have too much sada?

302
00:46:53,800 --> 00:46:59,800
There is no Buddha to shake us up.

303
00:46:59,800 --> 00:47:04,800
So we can reflect on the nature of the things.

304
00:47:04,800 --> 00:47:10,800
We can reflect that everything is impermanent,

305
00:47:10,800 --> 00:47:13,800
and so everything comes and goes,

306
00:47:13,800 --> 00:47:21,800
and also Buddha taught that although things may seem lovely to us

307
00:47:21,800 --> 00:47:25,800
in reality, all things are unlovely,

308
00:47:25,800 --> 00:47:35,800
and so we may reflect on such things to make our sada become less,

309
00:47:35,800 --> 00:47:41,800
and also to avoid listening to Dharma talks or reading books

310
00:47:41,800 --> 00:47:47,800
that could make our sada become stronger.

311
00:47:47,800 --> 00:47:52,800
So by avoiding listening to the talks and reading books

312
00:47:52,800 --> 00:47:56,800
that would inspire us much more,

313
00:47:56,800 --> 00:48:01,800
we can, we may be able to reduce sada.

314
00:48:01,800 --> 00:48:10,800
So in this way, we manage to keep sada

315
00:48:10,800 --> 00:48:22,800
in well within the limit.

316
00:48:22,800 --> 00:48:31,800
Similarly, when effort or energy is too much,

317
00:48:31,800 --> 00:48:40,800
then the other faculties, faith, mindfulness, concentration, and wisdom

318
00:48:40,800 --> 00:48:45,800
cannot do their respective functions properly.

319
00:48:45,800 --> 00:48:48,800
So when they do not function properly,

320
00:48:48,800 --> 00:48:52,800
then meditation will not bring results.

321
00:48:52,800 --> 00:48:59,800
And in this case, the commentaries is that the story of the elder sauna

322
00:48:59,800 --> 00:49:02,800
should be shown.

323
00:49:02,800 --> 00:49:06,800
So again, we want to know the story of sauna.

324
00:49:06,800 --> 00:49:12,800
So we'll have to add the books for the story of sauna,

325
00:49:12,800 --> 00:49:20,800
and you will hear the story of sauna tomorrow,

326
00:49:20,800 --> 00:49:22,800
not today.

327
00:49:22,800 --> 00:49:29,800
Thank you.

