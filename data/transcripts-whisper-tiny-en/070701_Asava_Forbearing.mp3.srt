1
00:00:00,000 --> 00:00:10,000
The day I'll continue the series of talks on the Asava.

2
00:00:10,000 --> 00:00:20,000
Again, the Asava are those mental state, those mind states, which cause the mind to go bad.

3
00:00:20,000 --> 00:00:23,000
They are the unhosted mind states.

4
00:00:23,000 --> 00:00:26,000
There are three types of Asava.

5
00:00:26,000 --> 00:00:36,000
The bad things inside, which we hold on to in regards to sensuality.

6
00:00:36,000 --> 00:00:42,000
So our desires for sensual pleasures, for things which are intoxicating.

7
00:00:42,000 --> 00:00:50,000
And our anger and aversion towards things which are unpleasant, which are unpleasant to us.

8
00:00:50,000 --> 00:00:59,000
Asava, in power, Asava means those defilements, those unhosting things inside, which have to do with being something.

9
00:00:59,000 --> 00:01:05,000
One thing can be this, one thing can be that, not one thing to be this, not one thing to be that.

10
00:01:05,000 --> 00:01:09,000
And the third said, Arija Asava.

11
00:01:09,000 --> 00:01:17,000
The Asava, the unhosted mind states, which simply come from ignorance, which come from delusion.

12
00:01:17,000 --> 00:01:23,000
Through not knowing, doing bad deeds or having bad intentions, not because of some external object,

13
00:01:23,000 --> 00:01:26,000
but simply out of simple ignorance.

14
00:01:26,000 --> 00:01:34,000
I'm getting angry or getting greedy out of ignorance of the consequences.

15
00:01:34,000 --> 00:01:41,000
All in all, it refers to any unhosted state which exists in our hearts.

16
00:01:41,000 --> 00:01:56,000
And the method today, the fourth method, which the Lord would have prescribed for overcoming the Asava, is Atiwasana.

17
00:01:56,000 --> 00:02:06,000
The Asava, the unhosted state, which we can do away with through patience, through forbearance.

18
00:02:06,000 --> 00:02:16,000
Atiwasana means overcoming or bearing with them.

19
00:02:16,000 --> 00:02:26,000
And this is a very important, very practical teaching in Buddhist tradition.

20
00:02:26,000 --> 00:02:37,000
So important, the Lord Buddha said it is the highest form of exertion.

21
00:02:37,000 --> 00:02:43,000
Patience is the highest form of exertion, of practice.

22
00:02:43,000 --> 00:02:47,000
And to take care of forbearance.

23
00:02:47,000 --> 00:02:55,000
Simply being able to stay with the objects, things which we like, or things which we don't like.

24
00:02:55,000 --> 00:03:04,000
Patience, from a Buddhist perspective, patience takes two sides.

25
00:03:04,000 --> 00:03:06,000
Patience has two sides to it.

26
00:03:06,000 --> 00:03:11,000
There is the act of being patient towards something which we don't like.

27
00:03:11,000 --> 00:03:13,000
Something which is unpleasant to us.

28
00:03:13,000 --> 00:03:18,000
So when we see here, when we're confronted with something which is unpleasant,

29
00:03:18,000 --> 00:03:28,000
or something which is difficult, something which is hard to accomplish, hard to stay with, hard to bear.

30
00:03:28,000 --> 00:03:33,000
Being able to stay without getting angry, without getting upset.

31
00:03:33,000 --> 00:03:45,000
To simply remain calm, and to keep our calm, and to keep our minds composed in the face of unpleasant situations and unpleasant experiences.

32
00:03:45,000 --> 00:03:51,000
But the other side of patience is being patient with things which we do like.

33
00:03:51,000 --> 00:03:54,000
Things which are pleasing to us.

34
00:03:54,000 --> 00:03:57,000
Things which are intoxicating.

35
00:03:57,000 --> 00:04:01,000
Things which have a intoxicating power over us.

36
00:04:01,000 --> 00:04:09,000
Which are addictive, which are a source of craving for us.

37
00:04:09,000 --> 00:04:15,000
Things which we see, or hear, or smell, or so on, or even thoughts, and ideas and concepts.

38
00:04:15,000 --> 00:04:20,000
I'm going the idea of wanting to be this, or wanting to be that.

39
00:04:20,000 --> 00:04:29,000
And patience on this side is being able to bear with, and not having the things which we want.

40
00:04:29,000 --> 00:04:37,000
Being able to build contentment inside, or be content within the face of things which are desirable.

41
00:04:37,000 --> 00:04:45,000
Where normally we would chase after the things which we desire, and run away from the things which are unpleasant to us.

42
00:04:45,000 --> 00:04:58,000
And when we're unable to achieve the things which we desire, or unable to remove or be away from the things which we find undesirable, we fall into suffering.

43
00:04:58,000 --> 00:05:04,000
And this is simply, this is a very simple explanation of why it is that we suffer.

44
00:05:04,000 --> 00:05:10,000
So for this reason, the Lord Buddha taught that patience is the highest form of exertion.

45
00:05:10,000 --> 00:05:13,000
Exerting yourself simply to be patient.

46
00:05:13,000 --> 00:05:15,000
This is the highest form of the bat.

47
00:05:15,000 --> 00:05:22,000
The bat was a word used in the Buddha's time, meaning some sort of practice, or exerting oneself.

48
00:05:22,000 --> 00:05:29,000
It's specifically used for meditation practices, or spiritual practices.

49
00:05:29,000 --> 00:05:37,000
So Lord Buddha said out of all spiritual practices, patience is the highest form of the mouth.

50
00:05:37,000 --> 00:05:45,000
And this is because at the time when we're patient, we won't give rise to greed, and we won't give rise to anger in regards to the states around us.

51
00:05:45,000 --> 00:05:50,000
Now, patience has three levels to it.

52
00:05:50,000 --> 00:06:04,000
The first one is called Titikha Kanti, patience which is simply forbearing or with standing.

53
00:06:04,000 --> 00:06:07,000
And it's just a name in what it refers to.

54
00:06:07,000 --> 00:06:19,000
Titikha Kanti means patience which is affected simply by repressing the desire or repressing the version towards the eyes.

55
00:06:19,000 --> 00:06:31,000
When we see something that we like, simply repressing the desire for it, repressing the desire to act, the desire to reach the desire to go for.

56
00:06:31,000 --> 00:06:44,000
And when we, on the other side, when we come across something which is uncomfortable or unpleasant, when we want to get angry, simply repressing, forcing ourselves not to act.

57
00:06:44,000 --> 00:06:50,000
This is the patience of an ordinary human being, so we might feel anger inside.

58
00:06:50,000 --> 00:06:57,000
We might feel, still feel upset, or we might still want something, but forcing ourselves not to act.

59
00:06:57,000 --> 00:07:01,000
And this is patience which is based on morality.

60
00:07:01,000 --> 00:07:04,000
So this is a start.

61
00:07:04,000 --> 00:07:10,000
When we first start out on the spiritual path, we don't have any sort of mental clarity or mental calm.

62
00:07:10,000 --> 00:07:20,000
But we at least start by giving up things, things which we hold on to, things which we want, even ideas or beliefs which we want self.

63
00:07:20,000 --> 00:07:34,000
Being able to bear with things which are addictive, things which are intoxicating for us, things which bring about craving in our minds, things which we find desirable.

64
00:07:34,000 --> 00:07:45,000
Or things which we find undesirable, bearing with our anger, when someone says it does something bad to us and makes us want to say bad things back to them.

65
00:07:45,000 --> 00:07:51,000
Stopping ourselves from saying or doing bad things, because of greed or because of anger.

66
00:07:51,000 --> 00:07:59,000
This is called titi ka kanti, and it's patience based on, it's another word for morality.

67
00:07:59,000 --> 00:08:08,000
The morality is another word for this kind of patience, for this patience is the same as equivalent to having morality.

68
00:08:08,000 --> 00:08:22,000
At the time when the opportunity arises to do something bad, we simply stay, keep our peace, even though inside we might be angry and we have all sorts of mental suffering.

69
00:08:22,000 --> 00:08:28,000
We don't act out on it, because we know that there will be more suffering and repercussions to follow when we do those bad deeds.

70
00:08:28,000 --> 00:08:37,000
This is sort of a basic spiritual practice, but of course it's not a very high spiritual practice and it can't last by itself.

71
00:08:37,000 --> 00:08:46,000
Eventually the repressed emotions simply explode and we find ourselves doing and saying bad things once again.

72
00:08:46,000 --> 00:09:01,000
So the second kind of patience is called tappa kanti, tappa, and this is the word tappa, tappa, tappa, and saying tappa, it means some sort of practice.

73
00:09:01,000 --> 00:09:11,000
In this case, tappa kanti means patience based on meditation practice or the practice of samatana and vipasana practice.

74
00:09:11,000 --> 00:09:22,000
When we are sitting and doing meditation and we want rising, falling, rising, falling, or when we watch seeing, seeing, or hearing, hearing, or so on.

75
00:09:22,000 --> 00:09:27,000
We find that we're able to give up our anger and our greed.

76
00:09:27,000 --> 00:09:37,000
We're able to create a state of concentration, a state of focus which is free from greed and free from anger, because it's free from delusion.

77
00:09:37,000 --> 00:09:41,000
At the time when there's no delusion, greed and anger can't arise.

78
00:09:41,000 --> 00:09:52,000
So at the time when we do walking meditation or sitting meditation or even mindful in our daily lives, we find ourselves at great peace with the things around us and things inside of us.

79
00:09:52,000 --> 00:10:03,000
And when good, when pleasant, pleasurable things arise, we find ourselves not reaching out for them, not needing for them, not even really attached to them.

80
00:10:03,000 --> 00:10:10,000
Sometimes pleasant things come and we don't even notice them as pleasant. We might not even notice that they're there.

81
00:10:10,000 --> 00:10:15,000
And pleasant things might come before they would be things which create anger inside of us.

82
00:10:15,000 --> 00:10:20,000
We find because of our concentration we're not even interested.

83
00:10:20,000 --> 00:10:25,000
We're not upset, we're not disturbed by the phenomenon.

84
00:10:25,000 --> 00:10:30,000
This is through the power of concentration, so this is equivalent to concentration.

85
00:10:30,000 --> 00:10:40,000
We say concentration, we're referring to having patience through these patience, through meditation practice.

86
00:10:40,000 --> 00:10:48,000
It's a form of patience that refers to concentration. These are equivalent.

87
00:10:48,000 --> 00:10:59,000
And the third type of patience is called atiwasana cantita. And this is the atiwasana and the teaching which I'll give today.

88
00:10:59,000 --> 00:11:04,000
And this is patience which comes from seeing clearly the object.

89
00:11:04,000 --> 00:11:16,000
So once we continue our practice of Vipassana, we find even when we do notice things that would be desirable for us.

90
00:11:16,000 --> 00:11:21,000
Through the wisdom which we have gained whether we practice meditation or don't practice meditation.

91
00:11:21,000 --> 00:11:25,000
We find we don't desire the things which we use to desire.

92
00:11:25,000 --> 00:11:34,000
We find that in the face of things which are terribly unpleasant, things which an ordinary person would find unbearable.

93
00:11:34,000 --> 00:11:43,000
We'll be unable to bear. We find we're able to be at peace and become and be happy even in the face of these things.

94
00:11:43,000 --> 00:11:50,000
Or things which are pleasurable, which are intoxicating, which are enticing for the ordinary person.

95
00:11:50,000 --> 00:11:56,000
We find them not enticing. And this time not simply through the power of concentration and not noticing them.

96
00:11:56,000 --> 00:12:03,000
In this case, we notice them in all of their characteristics, but we realize that they really aren't pleasing in the way that we thought they would.

97
00:12:03,000 --> 00:12:07,000
We realize that they're impermanent. We realize that they're unsatisfying.

98
00:12:07,000 --> 00:12:10,000
We realize that they're not under our control.

99
00:12:10,000 --> 00:12:16,000
We keep them. We can't force them to stay. We can't force the bad things to go.

100
00:12:16,000 --> 00:12:25,000
We can't do anything except be mindful and be aware and let them come and let them go and let them be.

101
00:12:25,000 --> 00:12:29,000
And this is the practice of patience.

102
00:12:29,000 --> 00:12:47,000
So when we are able to bear with painful feelings in the practice of meditation, we're able to bear with unpleasant sounds, unpleasant tastes, unpleasant feelings, unpleasant emotions.

103
00:12:47,000 --> 00:12:54,000
Or when we're able to bear with our wants and our desires, sitting in the forest, can be pleasant in the beginning.

104
00:12:54,000 --> 00:13:11,000
It might be that after some days, the meditator starts to think about the beaches or think about the bars or think about the discos or think about any number of things, friends and family and things which are the object over to desire.

105
00:13:11,000 --> 00:13:22,000
In the practice of meditation, we are trying to build patience so that wherever we go, we'll always be able to be at peace and happy wherever we are.

106
00:13:22,000 --> 00:13:31,000
The things that we want and we desire, we won't have to run after them ever and ever again, never being happy with what we have no matter where we are.

107
00:13:31,000 --> 00:13:35,000
And we'll be able to be happy in the peace wherever we are.

108
00:13:35,000 --> 00:13:43,000
So the method to do this is simply through the practice of meditation, through the practice of mindfulness.

109
00:13:43,000 --> 00:13:52,000
At the time, when we feel something unpleasant, perhaps a painful feeling in the body, we're so on.

110
00:13:52,000 --> 00:14:02,000
We simply acknowledge it, pain, pain, pain with a very high degree of patience.

111
00:14:02,000 --> 00:14:08,000
When we acknowledge, of course, one of the most important things is that we acknowledge patiently.

112
00:14:08,000 --> 00:14:17,000
We're not acknowledging, with acknowledging, with greed, the desire for it to go away or desire for it to stay or so on.

113
00:14:17,000 --> 00:14:22,000
We're simply being mindful of the reality of the object.

114
00:14:22,000 --> 00:14:37,000
We say pain, pain, becoming comfortable with it and seeing it clearly, creating a clear thought which is neither greedy or angry or upset about the object.

115
00:14:37,000 --> 00:14:42,000
And this is the way that we come to be a patient person.

116
00:14:42,000 --> 00:14:54,000
This is the way that we come to do away with unwholesome, unclean mind states, unpleasant mind states, like craving or anger or delusion.

117
00:14:54,000 --> 00:14:58,000
This is through the practice of patience.

118
00:14:58,000 --> 00:15:04,000
It's one of the most important virtues and most important things to keep in mind when we practice the meditation.

119
00:15:04,000 --> 00:15:12,000
Because, of course, the results of meditation don't ever come in simply one day or one sitting practice or something.

120
00:15:12,000 --> 00:15:18,000
We have to be very, very patient when we practice and consider that we are beginners.

121
00:15:18,000 --> 00:15:28,000
Just as anyone training in any sport or any activity, they have a very good saying in the West that a practice makes perfect.

122
00:15:28,000 --> 00:15:33,000
And it's important to always keep in mind that we're practicing meditation.

123
00:15:33,000 --> 00:15:36,000
We're not perfect in it.

124
00:15:36,000 --> 00:15:40,000
We haven't perfected the meditation for practicing it.

125
00:15:40,000 --> 00:15:47,000
And when we practice meditation, it's possible that if we practice with enough sincerity that we can make it perfect.

126
00:15:47,000 --> 00:15:55,000
Or we can at least come to the point where we are perfect in the sense of not getting ever getting angry or getting greedy.

127
00:15:55,000 --> 00:16:02,000
We come to see the truth about ourselves and the world around us and not ever giving rise to those

128
00:16:02,000 --> 00:16:12,000
emotions which are based on delusion and based on illusion and misunderstanding about the world.

129
00:16:12,000 --> 00:16:20,000
So, my wish for all of everyone here and the advice that I would give is,

130
00:16:20,000 --> 00:16:23,000
we must all try to be patient in our meditation practice.

131
00:16:23,000 --> 00:16:29,000
Not wanting for something in the future or thinking about something in the past.

132
00:16:29,000 --> 00:16:32,000
I'm not hurrying ahead and not standing still.

133
00:16:32,000 --> 00:16:35,000
It's simply continuing on at an even pace.

134
00:16:35,000 --> 00:16:38,000
It's slow and steady wins the race.

135
00:16:38,000 --> 00:16:44,000
If we continue on in this way, our practice will become perfect.

136
00:16:44,000 --> 00:16:54,000
Our practice will make us perfect in the sense of not being angry or greedy or deluded about the things inside or around us.

137
00:16:54,000 --> 00:17:00,000
And this is the teaching for today. Now we'll continue with meditation practice at your own speed.

