1
00:00:00,000 --> 00:00:20,000
Last month, when I talk about law of karma, I gave you one important and difficult

2
00:00:20,000 --> 00:00:31,000
message about karma. That is, I don't know whether you have the sheets still with you.

3
00:00:31,000 --> 00:00:42,360
So, that message says, Buddha represented Buddha as saying, I do not teach the extinction

4
00:00:42,360 --> 00:00:52,360
of karma, that a world performed and heaped up without their results having been experienced.

5
00:00:52,360 --> 00:01:00,360
And these results in this life or in the next life or in the lives after next.

6
00:01:00,360 --> 00:01:07,360
And never do I teach making an end of suffering, without the results of karma, that a world

7
00:01:07,360 --> 00:01:21,360
performed and heaped up having been experienced. That means, there is no extinction

8
00:01:21,360 --> 00:01:33,360
of karma if its results are not experienced. And the results can be experienced in this

9
00:01:33,360 --> 00:01:46,360
life or in the next life or in the lives after next.

10
00:01:46,360 --> 00:01:56,360
And Buddha continued saying, I do not teach making an end of suffering, without the results

11
00:01:56,360 --> 00:02:09,360
of karma as having been experienced. That means, there is no making an end of suffering

12
00:02:09,360 --> 00:02:27,360
if it results of karma or not experienced. At first reading, this message seems strange.

13
00:02:27,360 --> 00:02:42,360
So, the first sentence means, to not become extinct so long as their results are not experienced.

14
00:02:42,360 --> 00:02:50,360
And they can be experienced in this life or in the next life or in the lives after next.

15
00:02:50,360 --> 00:03:04,360
But according to the law of karma, karma has become defunct, it is called defunct.

16
00:03:04,360 --> 00:03:14,360
If they do not give results in their time of giving results.

17
00:03:14,360 --> 00:03:26,360
Now, there are kamas that give results in this life. And there is another that gives

18
00:03:26,360 --> 00:03:35,360
results in the next life. And then there are kamas that give results in the lives after next

19
00:03:35,360 --> 00:03:49,360
life. Until one becomes an anhant and passes away. Now, if a karma that must give

20
00:03:49,360 --> 00:04:03,360
results in this life, that will not give results because of lack of favorable conditions,

21
00:04:03,360 --> 00:04:15,360
then it becomes defunct. The karma was done, but if that karma does not give results

22
00:04:15,360 --> 00:04:22,360
in this life, then it becomes defunct. It will not give results in next life or in future

23
00:04:22,360 --> 00:04:29,360
lives. And also become a which is to give results in the next life. If it does not give

24
00:04:29,360 --> 00:04:37,360
results in the next life, then it becomes defunct. It will not give results in the lives

25
00:04:37,360 --> 00:04:51,360
after next until the end of samsara. And there are other kamas that give results beginning

26
00:04:51,360 --> 00:05:03,360
within life after next life until the end of samsara for that person. And both kamas

27
00:05:03,360 --> 00:05:13,360
will not become defunct until that person becomes an anhant and passes away. Or so long

28
00:05:13,360 --> 00:05:28,360
that there is samsara for him. Now, the first karma that gives result in this life.

29
00:05:28,360 --> 00:05:38,360
This karma will not become extinct or defunct so long as it has not given results and

30
00:05:38,360 --> 00:05:48,360
so long as there is this life. But after this life, if it does not become, if it does not

31
00:05:48,360 --> 00:05:57,360
give results, then it will become defunct. And the second type of karma that gives results

32
00:05:57,360 --> 00:06:04,360
in next life. So, so long as it has not given results in the next life and so long as

33
00:06:04,360 --> 00:06:12,360
that next life exists, then that karma does not become defunct. But when it is time

34
00:06:12,360 --> 00:06:20,360
of giving results is passed, that means when a person is rebounding the third existence,

35
00:06:20,360 --> 00:06:30,360
then the second type of karma also becomes defunct. The third type of karma that gives

36
00:06:30,360 --> 00:06:37,360
result from the third existence to the end of samsara. It will not become defunct so long

37
00:06:37,360 --> 00:06:48,360
as there is samsara for him. So, the comment releases that it is to show that you cannot

38
00:06:48,360 --> 00:07:05,360
escape the results of karma so long as you are in this samsara. Now, the second sentence

39
00:07:05,360 --> 00:07:13,360
that there is no making an end of suffering without the results of karma having been experienced.

40
00:07:13,360 --> 00:07:24,760
Here, we are not to understand that if we have not experienced the results of all

41
00:07:24,760 --> 00:07:34,160
kamas, we cannot make an end of suffering. We are not to take that way because if we take

42
00:07:34,160 --> 00:07:39,960
it that way that we cannot make an end of suffering until we have experienced all results

43
00:07:39,960 --> 00:07:50,560
of karma, actually there would be no making an end of suffering because we had a big amount

44
00:07:50,560 --> 00:08:02,960
of karma accumulated in the past and if we are to experience all the results of the good

45
00:08:02,960 --> 00:08:08,840
and bad kamas before we can make an end of suffering, there would be no making an end of

46
00:08:08,840 --> 00:08:22,480
suffering at all. So, here I think what we should understand is we must have experienced

47
00:08:22,480 --> 00:08:29,800
the result of karma which we did in the past before we can make an end of suffering before

48
00:08:29,800 --> 00:08:45,520
we can become an error hunt and be samsara. Now, every being has experienced the results

49
00:08:45,520 --> 00:08:52,800
of karma, there is no being who has not experienced the result of karma either got a

50
00:08:52,800 --> 00:09:05,760
bad. So, what this sentence means in my opinion is that all of us experienced the result

51
00:09:05,760 --> 00:09:15,920
of good or bad karma which we did in the past and if we have not experienced any results

52
00:09:15,920 --> 00:09:22,780
of the good or bad karma we did in the past, there can be no making an end of suffering

53
00:09:22,780 --> 00:09:40,320
let us take an example of the venerable Angulimala. Now, as you all know venerable Angulimala

54
00:09:40,320 --> 00:09:53,260
was first a robot and he kills thousands of people and he was feared by all people and

55
00:09:53,260 --> 00:10:05,620
it is said that no one could escape him but later Buddha went to him, met him and preached

56
00:10:05,620 --> 00:10:17,180
to him and he became his disciple, he threw away all his weapons and he followed the

57
00:10:17,180 --> 00:10:26,660
Buddha and later he became a monk and then he practiced meditation and became an error hunt.

58
00:10:26,660 --> 00:10:36,960
Now, during his life as a robot as he had killed thousands of people, he had all kinds

59
00:10:36,960 --> 00:10:43,120
of Akusala karma, those that will give result in dead life and those that will give

60
00:10:43,120 --> 00:10:51,580
results in the next life and there in the life after the next life. But, he became an

61
00:10:51,580 --> 00:10:58,100
error hunt and so those that will give result in the next life and those that will give

62
00:10:58,100 --> 00:11:07,680
result in the life after the next could not do anything to him. But, those that give

63
00:11:07,680 --> 00:11:23,260
result in the present life, he suffered ill consequences of dead and those kamas. Now, since

64
00:11:23,260 --> 00:11:28,860
he was feared, even when he became a monk, people were afraid of him and when people saw

65
00:11:28,860 --> 00:11:37,220
him, they went away and they closed their doors and so he was, he had difficulty in getting

66
00:11:37,220 --> 00:11:44,220
food enough for him. And also, he said that sometimes, if a person may throw a stone,

67
00:11:44,220 --> 00:11:53,980
not to him, but that stone would hit him and so he had a broken head and so on. So,

68
00:11:53,980 --> 00:12:03,260
he suffered the ill consequences of bad consequences of his kama, which gives results

69
00:12:03,260 --> 00:12:14,260
in that present life. But, with regard to the results, that would be experienced in the

70
00:12:14,260 --> 00:12:29,580
next life and then life after the next, these results cannot come about because he gets

71
00:12:29,580 --> 00:12:41,100
out of samsara after he died as an error hunt. So, in that case, Angulimala was able

72
00:12:41,100 --> 00:12:52,860
to make an end of suffering without experiencing the result of all of his kamas. But, he

73
00:12:52,860 --> 00:13:01,500
did experience the painful results of his kama, that could give results in the present

74
00:13:01,500 --> 00:13:18,500
life. After understanding the law of kama in general, we will now study the divisions

75
00:13:18,500 --> 00:13:31,460
of kama. Now, you have the seeds and first, kama is divided into 2 whole sama kama

76
00:13:31,460 --> 00:13:46,980
and whole sama kama. But, here the division is by way of function and so on. And I cannot

77
00:13:46,980 --> 00:14:02,460
explain in much detail these different kinds of kama. So, if you want to understand these

78
00:14:02,460 --> 00:14:10,780
kamas in detail, please read the comprehensive guide to a manual of a bita mama by big

79
00:14:10,780 --> 00:14:20,860
body. That is the translation of the abitamata sanga or the manual of abitamma and

80
00:14:20,860 --> 00:14:35,220
also explanations given. So, the kama is grouped in 4 groups and each group consisting

81
00:14:35,220 --> 00:14:47,060
of 4 kamas. So, the first group is by way of function, how these kamas function. So, by

82
00:14:47,060 --> 00:14:54,340
way of function, there are 4 kinds of kama and the first one is called productive kama.

83
00:14:54,340 --> 00:15:05,500
That means, kama that produces results and the second one is supportive kama. Now, this

84
00:15:05,500 --> 00:15:20,540
kama does not produce any results itself, but it supports the reproductive kama. And

85
00:15:20,540 --> 00:15:29,300
the third one is obstructive kama. Also, this kama does not produce any results, but it

86
00:15:29,300 --> 00:15:37,780
in the fears with the obstructs the results of the productive kama. And then the food kind

87
00:15:37,780 --> 00:15:54,660
of kama is destructive kama. This kama destroys the results of the productive kama.

88
00:15:54,660 --> 00:16:04,620
So, according to the way of their functions, according to their functions, kama divided

89
00:16:04,620 --> 00:16:23,580
into these 4. For example, the productive kama makes the person born as a human being.

90
00:16:23,580 --> 00:16:35,100
So, as a result of a productive kama, a person is born as a human being. And during his

91
00:16:35,100 --> 00:16:47,020
life, he enjoys good health, prosperity, long life and so on. And that is the result of

92
00:16:47,020 --> 00:16:59,420
a supportive kama. If he is not healthy, if he is poor, if he does not live long, then

93
00:16:59,420 --> 00:17:21,660
it may be the obstructive kama operating. If that person dies in an accident or dies abruptly

94
00:17:21,660 --> 00:17:34,980
of some disease, then it is said to be the destructive kama operating. So, a productive

95
00:17:34,980 --> 00:17:43,260
kama gives results at the moment of conception as well as during life and then supportive

96
00:17:43,260 --> 00:17:50,460
kama supports it. And the obstructive kama obstructs it and the destructive kama destroys

97
00:17:50,460 --> 00:18:08,980
the result of that productive kama. Now, kama is divided into 4 by order of giving results

98
00:18:08,980 --> 00:18:22,860
also. That is, these 4 types of kama give results in the order given here. And the first

99
00:18:22,860 --> 00:18:32,180
of them is weighty kama. Weighty kama means powerful kama that has a priority in giving

100
00:18:32,180 --> 00:18:47,540
results. If it is a wholesome kama, it is the Jana, Jana wholesome kama and if it is unwholesome

101
00:18:47,540 --> 00:18:57,060
then it is what is called the 5 grave sins that is killing once, own father, killing once

102
00:18:57,060 --> 00:19:06,140
own mother, killing an other hand, wounding the Buddha and causing division in the sanga.

103
00:19:06,140 --> 00:19:14,700
And also the fixed wrong view that means taking that there is no cause, no effect and

104
00:19:14,700 --> 00:19:23,300
so on. So, they are called weighty kama. And the second kind of kama is death, proximate

105
00:19:23,300 --> 00:19:37,060
kama. That means kama that a person does immediately before his death. Although that person

106
00:19:37,060 --> 00:19:54,260
may have done wholesome or unwholesome did habitually. But if immediately before his death, he

107
00:19:54,260 --> 00:20:01,620
does a wholesome kama or unwholesome kama, then that wholesome or unwholesome kama that he does

108
00:20:01,620 --> 00:20:11,300
immediately before his death has the priority to give results. That means priority over

109
00:20:11,300 --> 00:20:22,540
and the habitually kama or reserve kama. Habitually kama is kama that you do every day that

110
00:20:22,540 --> 00:20:34,180
you do by habit. People do both wholesome kama and unwholesome kama every day and so they

111
00:20:34,180 --> 00:20:42,500
become habitually kama. And the last one is called reserve kama. That means not included

112
00:20:42,500 --> 00:20:52,220
in the in the three given above. So, the last one is called reserve kama. If there are no weighty

113
00:20:52,220 --> 00:21:01,420
kama or death, proximate kama or habitually kama, then the reserve kama gets chance to give

114
00:21:01,420 --> 00:21:12,260
results. So, in order of giving results, these are the four kinds of kama. Now, the third

115
00:21:12,260 --> 00:21:22,700
division of kama is by time of giving results. So, there are also four of them. The first

116
00:21:22,700 --> 00:21:31,300
type is immediately effective kama. That means kama that gives results in this life.

117
00:21:31,300 --> 00:21:39,900
Number two is subsequently effective kama. That means kama that gives results in next life.

118
00:21:39,900 --> 00:21:49,860
Number three and definitely effective kama is that which gives results beginning with

119
00:21:49,860 --> 00:21:59,020
their life after next until the end of samsara. And the fourth is called defunct kama.

120
00:21:59,020 --> 00:22:07,740
Defunct kama is actually not a separate kama, but if immediately effective kama cannot

121
00:22:07,740 --> 00:22:16,780
give result in this life, then it becomes defunct. And if subsequently effective kama cannot

122
00:22:16,780 --> 00:22:25,820
give results for lack of favorable conditions, if it does if it cannot give results in the

123
00:22:25,820 --> 00:22:34,860
next life, then it becomes defunct. But the indefinitely effective kama can never become

124
00:22:34,860 --> 00:22:52,980
defunct until the end of samsara. So, when the first, the second and the third do not give

125
00:22:52,980 --> 00:23:01,780
results within the time of their giving results, then they become defunct kama.

126
00:23:01,780 --> 00:23:10,380
Now, among these four, the number three immediately effective kama is the most important

127
00:23:10,380 --> 00:23:18,540
for us because immediately effective kama may not give results in this life. And subsequently

128
00:23:18,540 --> 00:23:25,260
effective kama may not give results in the next life. And then they are gone. But indefinitely

129
00:23:25,260 --> 00:23:34,300
effective kama follows us all through the samsara. And it is this indefinitely effective

130
00:23:34,300 --> 00:23:48,540
kama that helps being who have fallen to hell and other awful states come up again and

131
00:23:48,540 --> 00:24:00,840
be reborn as human beings and as divas. So, all of us all beings have a stew of this

132
00:24:00,840 --> 00:24:08,300
indefinitely effective kama. And when the circumstances are favorable, we get the results

133
00:24:08,300 --> 00:24:22,260
of this kama. The fourth division is by place of giving results. Now, the first one is

134
00:24:22,260 --> 00:24:36,340
unwholesome kama that give results in four woeful states and also in sens sphere and material

135
00:24:36,340 --> 00:24:50,300
sphere beings. And the second is sens sphere wholesome kama. And it gives results in the

136
00:24:50,300 --> 00:24:58,540
sens sphere and also in material sphere and in material sphere. But the third material sphere

137
00:24:58,540 --> 00:25:05,180
wholesome kama gives results only in material sphere and fourth in material sphere wholesome

138
00:25:05,180 --> 00:25:14,860
kama gives results only in material sphere existence. Now, in order to understand that this

139
00:25:14,860 --> 00:25:26,980
you have to understand first that the one planes of existence and then in how the each

140
00:25:26,980 --> 00:25:37,060
one of them give results. You have to go to a bit of a book to understand the details

141
00:25:37,060 --> 00:25:58,300
of the results given by these four kinds of kamas. So, I gave you these divisions of kama

142
00:25:58,300 --> 00:26:09,220
just for you are information and also allows your curiosity so that you want to understand

143
00:26:09,220 --> 00:26:16,380
more. So, if you want to understand more you pick up the comprehensive guide and read it.

144
00:26:16,380 --> 00:26:27,380
Now, we come to the division again which is of practical applications. Because when we

145
00:26:27,380 --> 00:26:39,060
understand which are on wholesome kama and which are wholesome kamas then we can avoid

146
00:26:39,060 --> 00:26:50,020
what is wholesome kama and we can practice what is wholesome kama. Now, the on wholesome

147
00:26:50,020 --> 00:27:11,380
kama is divided into 10 kinds 3 dandhai body 4 by speech and 3 in mind only. So, on wholesome

148
00:27:11,380 --> 00:27:20,140
kama and dandhai body or dandhai body is of 3 times the first one is killing the second

149
00:27:20,140 --> 00:27:27,900
stealing and third sex illness conduct. Now, killing means killing it living being and

150
00:27:27,900 --> 00:27:36,220
killing a living being means killing a human being or an animal or event and this smallest

151
00:27:36,220 --> 00:27:43,980
insects. So, anything which is called a living being is not to be killed or not to be

152
00:27:43,980 --> 00:27:55,460
deprived of life and when a person kills any living being and he is doing and wholesome

153
00:27:55,460 --> 00:28:05,860
and wholesome kama and the second one is stealing stealing means taking anything that is

154
00:28:05,860 --> 00:28:19,940
not given by its owner and sexual misconduct means misconduct in sexual matters. So, not only

155
00:28:19,940 --> 00:28:30,620
adult free but other misconduct also is included here and unwholesome kama done by speech

156
00:28:30,620 --> 00:28:40,660
is divided into 4 and the first one is lying so telling lies telling what is not true

157
00:28:40,660 --> 00:28:54,540
and here it is it becomes a real unwholesome kama only when it is detrimental to the welfare

158
00:28:54,540 --> 00:29:08,860
of flows to whom you tell a lie and number 2 is slandering that means back by being with

159
00:29:08,860 --> 00:29:19,700
the intention of dividing two people and number 3 is harsh speech using strong language

160
00:29:19,700 --> 00:29:27,980
abusive language and so on and number 4 is frivolous talk that means talking nonsense

161
00:29:27,980 --> 00:29:37,260
talking what is not conducive to spiritual welfare. So, these are called unwholesome kama

162
00:29:37,260 --> 00:29:50,300
by speech and then there are 3 unwholesome kama and then in mind only and the first of them

163
00:29:50,300 --> 00:30:01,820
is covetousness that means the desire to possess something that belongs to another person

164
00:30:01,820 --> 00:30:12,540
now if you see something in the possession of another person and if you if you have to

165
00:30:12,540 --> 00:30:23,580
to greet that you want you want to possess it yourself then that is called covetousness

166
00:30:23,580 --> 00:30:31,780
it is not just not the testament to one's own one's own property or others but it

167
00:30:31,780 --> 00:30:50,500
is unfair desire to to possess a property or whatever which belongs to another person and

168
00:30:50,500 --> 00:31:02,060
number 2 is ill will that means hatred or the desire for other people to come to harm or

169
00:31:02,060 --> 00:31:10,980
to be injured or to die and then number 3 is wrong view that means taking that there is

170
00:31:10,980 --> 00:31:18,460
no kama there is no result of kama and so on and so these 3 are done not by by body or

171
00:31:18,460 --> 00:31:27,300
by speech but only in one's mind and so they are called 3 and that arises through

172
00:31:27,300 --> 00:31:36,740
mind only. So, these 10 are unwholesome kamas so when we know that these are unwholesome

173
00:31:36,740 --> 00:31:45,180
kama we are to avoid them now there is no drinking among these 10 in toxicants and

174
00:31:45,180 --> 00:31:56,860
others but the the commentaries explained that it should be included in number 3 by body

175
00:31:56,860 --> 00:32:07,940
misconduct. Now the pali word for that number 3 is kama is micha jara and kama is who

176
00:32:07,940 --> 00:32:18,220
means sensual things not just sexual things. So drinking and taking in toxicants is

177
00:32:18,220 --> 00:32:28,060
a kind of misconduct concerning sensual things that is taste. So drinking and taking in

178
00:32:28,060 --> 00:32:41,020
toxicants is that we included in number 3 done by body. Now we come to wholesome kama

179
00:32:41,020 --> 00:32:48,060
so there are 10 kinds of sensual wholesome kama and there are two sets of them. So the

180
00:32:48,060 --> 00:32:55,740
first set is just the opposite of the 10 kinds of unwholesome kama. So abstain abstain

181
00:32:55,740 --> 00:33:03,820
from killing, from stealing, from secular misconduct abstain from lying, slandering, harsh

182
00:33:03,820 --> 00:33:14,300
speech, frivolous talk and that the third group is non-covetousness or non-greed. That

183
00:33:14,300 --> 00:33:26,340
means not governing other people's property and non-illwheel or non-hatred. That means

184
00:33:26,340 --> 00:33:38,060
actually loving kindness and the third one right view or non-delusion is the view that

185
00:33:38,060 --> 00:33:49,100
there is cause, there is effect and everything in the world is dependent on some other

186
00:33:49,100 --> 00:34:04,980
thing and so on. So these 10 sensual wholesome kama, these are the negative side of sensual

187
00:34:04,980 --> 00:34:13,380
wholesome kama and there is another set of 10 sensual wholesome kama that are on the

188
00:34:13,380 --> 00:34:29,460
positive side and so there are 10 of them and the first one is giving dana. So making

189
00:34:29,460 --> 00:34:40,980
donations, charity is giving and the second one is virtue or moral purity. That means keeping

190
00:34:40,980 --> 00:34:51,460
once moral contact pure by abstention from killing, stealing, secular misconduct and so on.

191
00:34:51,460 --> 00:34:59,740
The third one is mental culture, development of one's mind, towards a spiritual welfare

192
00:34:59,740 --> 00:35:14,700
or spiritual advancement. The fourth is reverence. That means being respectful to the

193
00:35:14,700 --> 00:35:28,180
elders and to those who are worthy of our respect or greeting those people and giving

194
00:35:28,180 --> 00:35:37,540
seats to those people and so on. And then number five is service. That means doing things

195
00:35:37,540 --> 00:35:48,880
for other people, doing shows and so on and helping people in an act of meritorious

196
00:35:48,880 --> 00:36:05,420
deeds. And those who donate liver to the monastery, those who are practicing this service.

197
00:36:05,420 --> 00:36:13,940
And number six is sharing of merit. That means first you acquire merit yourself and then

198
00:36:13,940 --> 00:36:24,580
you share this merit with other beings. And sharing of merit is just letting them, other

199
00:36:24,580 --> 00:36:32,180
people get chance to get merit themselves depending on your merit. So it is not giving

200
00:36:32,180 --> 00:36:41,740
away your merit. And by sharing merit, your merit increases because sharing of merit is

201
00:36:41,740 --> 00:36:50,180
you see here an act of wholesome comma or an act of merit itself. And then rejoicing

202
00:36:50,180 --> 00:36:58,820
in others' merit. That means when merit is shared and you say sadhu, sadhu, sadhu.

203
00:36:58,820 --> 00:37:07,700
That means good, good, good. You are happy or glad at the other people doing merit. So when

204
00:37:07,700 --> 00:37:16,220
you rejoice at others' merit, you get merit yourself. Because when you rejoice, your

205
00:37:16,220 --> 00:37:23,720
rejoice takes the other persons' merit as an object. And so it is a wholesome mental

206
00:37:23,720 --> 00:37:32,060
state. Here the commentary says that whether the other person shares merit with you or

207
00:37:32,060 --> 00:37:43,220
not, you can rejoice in the merit of others. So if the other person shares merit, you

208
00:37:43,220 --> 00:37:50,060
can say sadhu, sadhu, sadhu. Or if he does not share merit with you, but you see or you

209
00:37:50,060 --> 00:37:58,140
learn that the other person does merit, then you can practice rejoicing in others'

210
00:37:58,140 --> 00:38:10,060
merit by being glad at that merit. And number eight is hearing the dhamma listening to

211
00:38:10,060 --> 00:38:30,580
the dhamma talks. And here, learning those, learning some kind of arts or crowds or some

212
00:38:30,580 --> 00:38:42,180
kind of knowledge, which does not conuse to harming or enduring others. It is also included

213
00:38:42,180 --> 00:38:53,900
here. Teaching the dhamma, number nine. Here also, giving talks on dhamma and so on, and

214
00:38:53,900 --> 00:39:05,060
also teaching as some kind of knowledge, which does not conuse to ensuring others, which

215
00:39:05,060 --> 00:39:14,340
does not go against the teachings of the Buddha. And number ten is strengthening out one's

216
00:39:14,340 --> 00:39:27,180
views. That means having a correct view concerning the cause and effect and so on. So this

217
00:39:27,180 --> 00:39:36,580
straightening of one's view is actually the opposite of the wrong view. Now straightening

218
00:39:36,580 --> 00:39:43,180
of one's view is very important, because if our views are not straight, our views are incorrect,

219
00:39:43,180 --> 00:40:00,340
then whatever we do will not be gusala or mental wholesome mental state. That is why

220
00:40:00,340 --> 00:40:08,780
straightening of one's view is that it will be very important and we need to have the correct

221
00:40:08,780 --> 00:40:22,660
view or right view about cause, about the effect and about things that are related as

222
00:40:22,660 --> 00:40:37,220
cause and effect. Now the first three of them, dhamna, sila and bhavana are the three steps to be

223
00:40:37,220 --> 00:40:50,020
followed by the disciples of the Buddha. Now the first one giving or dhamna, among the ten

224
00:40:50,020 --> 00:41:01,340
perfections, practiced and fulfilled by bodhisattas, dhamna comes first. So dhamna here means

225
00:41:01,340 --> 00:41:15,220
giving, helping other people, making donations and so on. Dhamna is important because it makes

226
00:41:15,220 --> 00:41:30,060
our mind, client or soft and also if it gives us chance to practice getting rid of attachment.

227
00:41:30,060 --> 00:41:39,940
So whatever we give, we not only give that thing but we also give up attachment to that

228
00:41:39,940 --> 00:41:55,020
thing. So by giving we are practicing the getting rid of attachment to that thing.

229
00:41:55,020 --> 00:42:09,260
And the second one is sila or more authority or it is called virtue and it is control of

230
00:42:09,260 --> 00:42:21,500
our bodily, bodily actions and speech. So so long as we keep sila, we keep the precepts,

231
00:42:21,500 --> 00:42:34,820
we keep our more and more moral purity, then we do not commit unwholesome actions by body

232
00:42:34,820 --> 00:42:44,740
and by speech. As lay persons you can keep five precepts and sometimes eight precepts like

233
00:42:44,740 --> 00:42:54,860
you do here and if you want to practice more, you can take ten precepts. And the third

234
00:42:54,860 --> 00:43:09,140
one, Mandelkacha, is the practice of samata and vipasana meditations. By practice of samata

235
00:43:09,140 --> 00:43:24,060
meditation you get strong concentration and if you are successful with any one of the 40 subjects

236
00:43:24,060 --> 00:43:36,540
of samata meditation you get psychic powers and others. And by the practice of vipasana you

237
00:43:36,540 --> 00:43:51,100
ultimately realize nibana or you achieve your education of all mental departments.

238
00:43:51,100 --> 00:43:59,140
In number four and number five, reverence of being respectful to others and doing service

239
00:43:59,140 --> 00:44:08,820
to others, these two can be included in number two, moral purity because when you practice

240
00:44:08,820 --> 00:44:18,820
sila you control your bodily actions and speech and reverence and service is a kind of controlling

241
00:44:18,820 --> 00:44:27,980
your bodily actions and verbal actions. So they can be included in sila or number two and

242
00:44:27,980 --> 00:44:35,340
sharing of merit can be included in number one dhana and rejoicing in others merit is

243
00:44:35,340 --> 00:44:43,580
also included in giving because it is connected with giving, not once giving but the

244
00:44:43,580 --> 00:44:50,580
others giving. And then hearing the dhama, teaching the dhama and straightening out

245
00:44:50,580 --> 00:44:59,540
one's view can be included in the third mental culture. So hearing the dhama is a kind

246
00:44:59,540 --> 00:45:04,180
of mental culture, teaching the dhama as a kind of mental culture because you are developing

247
00:45:04,180 --> 00:45:12,860
your mind and straightening out one's views as one's view is also a mental culture.

248
00:45:12,860 --> 00:45:22,460
So we can reduce all these ten into three if we include four and five in two, six and

249
00:45:22,460 --> 00:45:38,900
seven in one and eight nine and ten in number three. Now that we understand the unwholesome

250
00:45:38,900 --> 00:45:46,820
kama and wholesome kama, we can follow the teaching of the Buddha given in the dhama

251
00:45:46,820 --> 00:45:56,420
Buddha that is not doing all evil accomplishing what is wholesome to refine one's mind.

252
00:45:56,420 --> 00:46:06,980
This is the teachings of the Buddha. Now I gave this translation very little but it may

253
00:46:06,980 --> 00:46:15,260
not be good English, not doing all evil because when we want the pali what is sabha,

254
00:46:15,260 --> 00:46:21,980
bhapasasas, sabha means all and bhapha means evil, so all evil. But people said that if

255
00:46:21,980 --> 00:46:30,260
you say not doing all evil that means you can do a little evil. So in good English you

256
00:46:30,260 --> 00:46:38,460
will say not doing any evil, not all evil. So not doing any evil that means refraining

257
00:46:38,460 --> 00:46:46,620
from doing akhusala and wholesome kamas and then accomplishing what is wholesome that

258
00:46:46,620 --> 00:46:57,900
means cultivating wholesome kama or wholesome acts and purifying one's mind. So there are

259
00:46:57,900 --> 00:47:06,420
defalments or impurities in our minds and purifying one's mind is also the teachings

260
00:47:06,420 --> 00:47:19,300
of the Buddha. So in this verse with this verse Buddha admonishes us to avoid doing evil

261
00:47:19,300 --> 00:47:28,380
and to cultivate what is wholesome and also to purify our minds.

262
00:47:28,380 --> 00:47:37,220
The Dhamma Buddha commentary explains this verse as the first, the first foot sabha, bhapasas

263
00:47:37,220 --> 00:47:45,540
are occurring and not doing all and wholesome deeds. That's not difficult to understand.

264
00:47:45,540 --> 00:47:52,140
The second one is khusala sabha, upa sambhara, generating wholesome deeds and the development

265
00:47:52,140 --> 00:48:00,500
of what is generated. So here accomplishing what is wholesome or cultivating what is wholesome

266
00:48:00,500 --> 00:48:08,740
means, generating wholesome deeds and then doing these deeds again and again. So not

267
00:48:08,740 --> 00:48:19,500
just doing wholesome ones, but doing it again and again so that it is developed.

268
00:48:19,500 --> 00:48:26,700
And also the common research that it is to it is to be done from the renunciation to

269
00:48:26,700 --> 00:48:39,220
the Arahatama that means to the gaining of enlightenment. And I think the commentary connects

270
00:48:39,220 --> 00:48:52,140
this to the life of the Buddha. So as a prince, Buddha renounced the world and then for

271
00:48:52,140 --> 00:48:58,900
six years in the forest he practiced austereities and then ultimately at the end of this,

272
00:48:58,900 --> 00:49:10,740
at the end of six years he reached enlightenment. So from the moment of his renunciation

273
00:49:10,740 --> 00:49:23,740
until he attains the attain enlightenment, Buddha generated wholesome deeds and also developed

274
00:49:23,740 --> 00:49:32,820
them. The third one such as the bharyodhavna means causing one's mind to be cleansed

275
00:49:32,820 --> 00:49:40,340
of the five mental hindrances. So there are said to be five mental hindrances and you

276
00:49:40,340 --> 00:49:49,620
as meditators are all familiar with these five mental hindrances. So to clear one's

277
00:49:49,620 --> 00:49:58,980
mind of these five mental hindrances is also an important act because so long as there

278
00:49:58,980 --> 00:50:05,980
are mental hindrances in our minds we cannot.

