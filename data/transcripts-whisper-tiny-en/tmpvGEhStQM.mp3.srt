1
00:00:00,000 --> 00:00:02,400
Welcome back to Ask Among.

2
00:00:02,400 --> 00:00:11,280
Today's questions come from Papilon, BLN1, who asks some technical questions.

3
00:00:11,280 --> 00:00:14,240
How many times a day could one meditate?

4
00:00:14,240 --> 00:00:18,520
How long does one have to meditate for session for the first time?

5
00:00:18,520 --> 00:00:24,080
Then does one increase the session time of meditation if yes, when?

6
00:00:24,080 --> 00:00:32,080
Okay, so questions about a technical practice of meditation.

7
00:00:32,080 --> 00:00:40,600
The first thing to realize is that meditation, ideally, in its developed form, should

8
00:00:40,600 --> 00:00:41,600
be constant.

9
00:00:41,600 --> 00:00:47,600
It should be something that is carried out for all of your waking hours.

10
00:00:47,600 --> 00:00:53,320
So even sitting here, talking to you, I'm aware of the fact that I'm sitting, I meditate

11
00:00:53,320 --> 00:00:58,560
on the sitting position, the awareness of the sitting, aware of the lips moving, aware

12
00:00:58,560 --> 00:01:05,760
of the sounds of the nature around me, and the meditation is being aware of that and seeing

13
00:01:05,760 --> 00:01:11,200
it clearly for what it is using this mantra, and even when you're talking, you can

14
00:01:11,200 --> 00:01:17,320
say to yourself, feeling the lips moving or so on, you can carry this out.

15
00:01:17,320 --> 00:01:22,320
Now whether you're able to do that 24 hours a day or for all of your waking hours is

16
00:01:22,320 --> 00:01:23,320
another question.

17
00:01:23,320 --> 00:01:29,880
So in the beginning, we do formal meditation to train ourselves in this, because obviously

18
00:01:29,880 --> 00:01:37,880
to simply go out into life and attempt to be mindful, it's not really going to work in

19
00:01:37,880 --> 00:01:45,320
the same way that a person who practices martial arts and they say, okay, we learn some

20
00:01:45,320 --> 00:01:49,720
techniques, now I'm just going to go out and practice them in real fights.

21
00:01:49,720 --> 00:01:52,720
So obviously not going to work out that way.

22
00:01:52,720 --> 00:01:56,120
You have to train in a dojo in a gym and so on.

23
00:01:56,120 --> 00:02:00,200
And then if someone comes and picks a fight with you, you can defend yourself.

24
00:02:00,200 --> 00:02:02,360
But after the training.

25
00:02:02,360 --> 00:02:07,520
So in the same way, with meditation we train in the gym or the lab, whatever you want

26
00:02:07,520 --> 00:02:12,160
to call it, which is with a formal meditation practice.

27
00:02:12,160 --> 00:02:18,280
And I recommend this, there's no question that this helps, because most of us are beginners

28
00:02:18,280 --> 00:02:19,280
anyway.

29
00:02:19,280 --> 00:02:22,520
We're talking, the advanced state is for someone who is enlightened.

30
00:02:22,520 --> 00:02:27,960
If you're enlightened, you're always aware of everything that goes on and you're clearly

31
00:02:27,960 --> 00:02:30,360
aware and you see it for what it is.

32
00:02:30,360 --> 00:02:40,880
But along the path, we develop this, we train ourselves and this is what formal meditation

33
00:02:40,880 --> 00:02:42,960
is all about.

34
00:02:42,960 --> 00:02:48,400
So it really doesn't matter how long you do it, however long it's comfortable for you.

35
00:02:48,400 --> 00:02:57,720
I think most people are not lazy in the sense of doing too little.

36
00:02:57,720 --> 00:03:03,960
If you're intent upon it, I think more laziness comes with knowing that we've done a certain

37
00:03:03,960 --> 00:03:05,280
amount of meditation.

38
00:03:05,280 --> 00:03:10,240
People get good at doing a half an hour or an hour of meditation or even longer.

39
00:03:10,240 --> 00:03:14,680
They start to think that they've attained something, they've realized something and many

40
00:03:14,680 --> 00:03:20,200
times they haven't, many times all they've attained is the ability to sit perfectly still

41
00:03:20,200 --> 00:03:27,040
and come and to fix and focus their mind in a concentrated state that's not really aware

42
00:03:27,040 --> 00:03:31,360
and not really learning and not really meditating.

43
00:03:31,360 --> 00:03:37,560
So we should never ever fixate on the amount of time that we do.

44
00:03:37,560 --> 00:03:42,440
Even in a formal meditation course, I would never tell you that every day you should do

45
00:03:42,440 --> 00:03:43,440
so many hours.

46
00:03:43,440 --> 00:03:45,400
There are centers that do that.

47
00:03:45,400 --> 00:03:46,400
I don't.

48
00:03:46,400 --> 00:03:53,840
And my teacher was quite critical of that sort of thing because then you've turned meditation

49
00:03:53,840 --> 00:03:58,200
into an entity, you know, my half hour of meditation, my hour of meditation.

50
00:03:58,200 --> 00:04:03,680
If you say that you meditated for a certain amount of time, you're probably lying because

51
00:04:03,680 --> 00:04:08,960
most of the time your mind was wandering and so on.

52
00:04:08,960 --> 00:04:14,520
So meditation is not about how many minutes you do, it's about how many moments you're

53
00:04:14,520 --> 00:04:19,320
aware, how many moments you clearly see things as they are and it's developing this

54
00:04:19,320 --> 00:04:27,480
clear awareness, which is momentary, which is every experience that we have coming to learn

55
00:04:27,480 --> 00:04:35,760
about experience and so even in a few steps, you can learn something about yourself and

56
00:04:35,760 --> 00:04:41,200
if you're watching the breath, if you really are aware, just knowing this is the rising,

57
00:04:41,200 --> 00:04:48,520
this is the falling and saying, you're rising and simply being with it as it is.

58
00:04:48,520 --> 00:04:52,880
Even just a few times, it's much better than sitting for an hour and forcing the breath

59
00:04:52,880 --> 00:04:58,080
and getting angry and getting frustrated and so on, actually that's beneficial as well

60
00:04:58,080 --> 00:05:02,280
because it helps you learn about learning that you're an angry person and so on.

61
00:05:02,280 --> 00:05:08,720
But at any moment that you're actually meditating and learning and opening up and coming

62
00:05:08,720 --> 00:05:18,400
to accept and let go and simply be, you know, not just simply be but to learn, any moment

63
00:05:18,400 --> 00:05:26,320
that you're learning, that's a moment of meditation, but I think this isn't quite satisfactory

64
00:05:26,320 --> 00:05:32,920
for most people who want to set up a formal meditation during their life, so I recommend

65
00:05:32,920 --> 00:05:39,720
generally people to do meditation at least morning and evening, so do twice a day.

66
00:05:39,720 --> 00:05:44,920
If you have time during your normal daily routine, do another one.

67
00:05:44,920 --> 00:05:50,600
If you have a day off, you can take a day to do practice meditation.

68
00:05:50,600 --> 00:05:56,240
There's no limit, as I said, you should be mindful the whole day through, but I don't think

69
00:05:56,240 --> 00:05:59,720
you'll be benefited by just filling your day up with meditation.

70
00:05:59,720 --> 00:06:04,200
There's a limit, especially when you're not on a meditation course.

71
00:06:04,200 --> 00:06:08,960
If you do an intensive meditation course, you can build up to many hours because it's

72
00:06:08,960 --> 00:06:09,960
just natural.

73
00:06:09,960 --> 00:06:14,640
Your mind comes down and you're able to focus and you don't have any desire to do

74
00:06:14,640 --> 00:06:19,600
this or do that, and as a result, you can do many hours of meditation, but even in that

75
00:06:19,600 --> 00:06:25,520
case, you have to take breaks and more important is that your mind will between the time

76
00:06:25,520 --> 00:06:30,160
you're meditating, when you, the whole time, during the time you're doing sitting and

77
00:06:30,160 --> 00:06:34,320
walking, your mindful, and when you get up off the mat, you're also mindful when you

78
00:06:34,320 --> 00:06:39,640
walk to the washroom, when you walk to the kitchen, the dining room, and so on.

79
00:06:39,640 --> 00:06:42,360
You're also mindful, it's much more important.

80
00:06:42,360 --> 00:06:48,640
So there's no limit, but it's not really the point to set a time.

81
00:06:48,640 --> 00:06:54,200
Another technical notice that we, you know, obviously from the videos we like to balance

82
00:06:54,200 --> 00:06:59,080
walking and sitting, but that's simply a technical aspect of meditation.

83
00:06:59,080 --> 00:07:04,200
That's one of the reasons why a person might time their meditation, your time it, not so

84
00:07:04,200 --> 00:07:10,680
that you can be sure you meditate it so long, so that you know that you're balancing the

85
00:07:10,680 --> 00:07:15,440
tool, because obviously walking meditation is a beneficial thing.

86
00:07:15,440 --> 00:07:21,280
Another reason is because it is possible, you know, when you, if you don't time your

87
00:07:21,280 --> 00:07:25,640
meditation, if you don't see, I'm going to sit for X number of minutes, then as soon

88
00:07:25,640 --> 00:07:30,280
as something interesting arises, and by interesting, I mean difficult.

89
00:07:30,280 --> 00:07:37,240
So a stress, a suffering, a pain in the body, we get up and quit, you know, because

90
00:07:37,240 --> 00:07:41,400
we don't have any, there's nothing keeping us on the mat, but if you have got to set

91
00:07:41,400 --> 00:07:48,720
time limit, and if it's a little bit longer than you meditated before, then when those interesting

92
00:07:48,720 --> 00:07:53,920
experiences arise, you'll actually have a reason to stick around and learn about them.

93
00:07:53,920 --> 00:07:57,840
And that's where meditation has its real benefit.

94
00:07:57,840 --> 00:08:03,600
In a sense, forcing yourself to stick with the experience, but it's not forcing anything,

95
00:08:03,600 --> 00:08:09,840
it's just not reacting or not allowing yourself to react, studying your reactions instead

96
00:08:09,840 --> 00:08:15,120
of, you know, getting, when you're getting angry, to follow it and to run away from the

97
00:08:15,120 --> 00:08:22,080
experience, you study the anger and you stick with it, and you come, you overcome the

98
00:08:22,080 --> 00:08:26,480
anger, you come to realize that the problem is our disliking and so on.

99
00:08:26,480 --> 00:08:29,640
When you want something and instead of going to get it, you study the one thing and you

100
00:08:29,640 --> 00:08:31,680
see that that's stressful.

101
00:08:31,680 --> 00:08:37,320
So there are reasons for putting a limit on the meditation and there are reasons for increasing

102
00:08:37,320 --> 00:08:38,320
it.

103
00:08:38,320 --> 00:08:44,560
The best way to do this is under a teacher because that's even more external, then you're

104
00:08:44,560 --> 00:08:51,800
even less able to follow your preferences because the teacher's telling you, okay, today

105
00:08:51,800 --> 00:08:57,600
do more meditation, so, you know, you can't just increase it when it's comfortable for

106
00:08:57,600 --> 00:08:58,600
you.

107
00:08:58,600 --> 00:09:01,280
And it challenges you.

108
00:09:01,280 --> 00:09:06,320
I would recommend, you know, keeping it challenging it, but as long as you're learning,

109
00:09:06,320 --> 00:09:09,800
unless you're in a formal meditation practice, it's going to change.

110
00:09:09,800 --> 00:09:14,640
And if you're so absorbed in the time of meditation, you're always going to be disappointed

111
00:09:14,640 --> 00:09:19,760
and stressed, you'll start to stress about your meditation, oh, I've, today, I only did

112
00:09:19,760 --> 00:09:25,920
so much meditation or I didn't have time to do full meditation and which is really garbage,

113
00:09:25,920 --> 00:09:26,920
it's not real.

114
00:09:26,920 --> 00:09:30,800
I mean, when you're thinking like that, that's when you should be meditating.

115
00:09:30,800 --> 00:09:34,360
If you're so upset them because you weren't able to meditate or stressed or worried about

116
00:09:34,360 --> 00:09:38,000
are you going to have time to meditate, that's when you should be meditating.

117
00:09:38,000 --> 00:09:43,480
When you're stressed, you know, instead of, you know, that's when real progress is going

118
00:09:43,480 --> 00:09:47,440
to come about because obviously you're the sort of person who thinks about the future

119
00:09:47,440 --> 00:09:53,000
and the past and worries about these things and is upset by them.

120
00:09:53,000 --> 00:10:05,720
So don't fixate on the time and don't fixate on the necessity to do any formal meditation,

121
00:10:05,720 --> 00:10:15,720
try to fixate on the reality in the present moment and that's much more important doing

122
00:10:15,720 --> 00:10:21,720
formal meditation is good, but that should eventually come naturally where when the time

123
00:10:21,720 --> 00:10:27,720
comes to meditate and there's nothing pressing, you sit still and then you watch your

124
00:10:27,720 --> 00:10:30,320
breath or whatever.

125
00:10:30,320 --> 00:10:38,680
So I hope that helps and you're able to set up a dedicated meditation practice and continue

126
00:10:38,680 --> 00:10:47,800
to develop, to train yourself and be able to realize the truth of life and reality, okay,

127
00:10:47,800 --> 00:10:54,800
thanks, all of that.

