1
00:01:30,000 --> 00:01:48,960
so here I am in Sri Lanka first destination this is the monastery I'll be staying up for

2
00:01:48,960 --> 00:01:58,060
the next few days while we have this conference on Buddhism the the thing I'd like to

3
00:01:58,060 --> 00:02:03,120
share today I was I was thinking something I learned over the past two weeks in

4
00:02:03,120 --> 00:02:10,440
Thailand while I was meditating and it didn't come the clarity of it came or the

5
00:02:10,440 --> 00:02:15,880
inspiration for it came not from the meditation but from actually a deviation

6
00:02:15,880 --> 00:02:23,800
or a diversion from the meditation there's a monk who is a relative of my

7
00:02:23,800 --> 00:02:31,060
teacher his nephew or great nephew or whatever and he disrobed while I was

8
00:02:31,060 --> 00:02:37,360
there which which is kind of disappointing it's but he's an interesting

9
00:02:37,360 --> 00:02:44,720
monk and he he said something that really struck me that this really

10
00:02:44,720 --> 00:02:55,400
surprised me actually he had the understanding that the point of of the Buddha's

11
00:02:55,400 --> 00:03:01,160
teaching the point of the person of insight meditation is to suppress or cover

12
00:03:01,160 --> 00:03:09,040
up the defilements of greed of anger and delusion to the point that they

13
00:03:09,040 --> 00:03:16,720
become they they decompose basically is what he was saying they they

14
00:03:16,720 --> 00:03:25,240
rot and when they rot then they I guess disappear and I immediately

15
00:03:25,240 --> 00:03:29,200
argued with him but you know at the time it wasn't clear in my mind what

16
00:03:29,200 --> 00:03:33,280
exactly he was saying and what what what what the importance it had but when I

17
00:03:33,280 --> 00:03:39,000
went back to meditate and I'm putting it together with my meditation and the

18
00:03:39,000 --> 00:03:47,680
fact that it seems so much more so much so clear that meditation is is is not

19
00:03:47,680 --> 00:03:51,720
that or or insight meditation is not that the insight meditation is actually

20
00:03:51,720 --> 00:03:59,240
taking the lid off the pot so to speak of our rotting mind our mind that the

21
00:03:59,240 --> 00:04:05,360
Buddha said is he called the defilements asawa asawa means rotten and they

22
00:04:05,360 --> 00:04:11,320
rot the mind they cause the mind to become pickled actually or or I don't

23
00:04:11,320 --> 00:04:17,600
know what the right word would be and and so that they don't disappear they

24
00:04:17,600 --> 00:04:24,080
rot the mind they're in their rotting causing the mind to be rotten that's not a

25
00:04:24,080 --> 00:04:28,280
good thing it's not good you know it's not a compost pile where you know we're

26
00:04:28,280 --> 00:04:35,720
talking about ourselves and our minds something that is it's important to purify

27
00:04:35,720 --> 00:04:44,400
and to to make pure and so what I thought we're thinking of this morning you

28
00:04:44,400 --> 00:04:47,920
know I was going to talk about this and I thought you know what is this in terms

29
00:04:47,920 --> 00:04:51,440
of the Buddha's teaching and then I realized this is what the Buddha meant by

30
00:04:51,440 --> 00:04:56,800
the middle way because and this is the profundity that we don't often

31
00:04:56,800 --> 00:05:01,360
realize you know we hear the Buddha said don't get involved don't go to the

32
00:05:01,360 --> 00:05:05,560
extreme of sensual attachment to sensual pleasures where you're following

33
00:05:05,560 --> 00:05:11,200
happens we say okay fine I won't be a prince and so on I won't dedicate my

34
00:05:11,200 --> 00:05:16,160
life to sensuality and then don't torture yourself we say okay so I won't go

35
00:05:16,160 --> 00:05:23,240
off and and we'll go naked and stand in the sand and not eat and all these

36
00:05:23,240 --> 00:05:28,600
ascetic practices that they had in India but it's so much it's so much more

37
00:05:28,600 --> 00:05:34,160
immediate than that that's not what is meant by these these two extremes the

38
00:05:34,160 --> 00:05:38,520
two extremes are how we approach reality sensual the addiction

39
00:05:38,520 --> 00:05:43,400
essential pleasures is when the emotions come up whether it's greed or anger or

40
00:05:43,400 --> 00:05:48,360
delusion we follow after them we you know when we want something which is

41
00:05:48,360 --> 00:05:54,080
after when you have desire for something you go for you say okay I want it

42
00:05:54,080 --> 00:06:01,000
therefore I should get it I should chase after the addiction to our attachment

43
00:06:01,000 --> 00:06:06,080
to the torturing yourself is it's so perfectly clear and this is exactly what

44
00:06:06,080 --> 00:06:10,320
this monk was doing it's what most of us do especially when we hear about

45
00:06:10,320 --> 00:06:15,880
Buddhism we hear the Buddha taught and greed is bad anger is bad delusion is

46
00:06:15,880 --> 00:06:21,720
bad and we get this idea that the Buddha was condemning people who had these

47
00:06:21,720 --> 00:06:25,480
things condemning me the person for giving rise to these things you say okay

48
00:06:25,480 --> 00:06:30,360
so in that point is this is the stress and the stop them from me when they

49
00:06:30,360 --> 00:06:35,600
come up that's bad that's sinful that's evil but really this is not how the

50
00:06:35,600 --> 00:06:38,360
Buddha taught the Buddha taught us to understand these things we thought

51
00:06:38,360 --> 00:06:45,000
is to understand greed to understand anger to understand delusion and we don't

52
00:06:45,000 --> 00:06:48,280
get this because we haven't reached the middle way we think if you let them

53
00:06:48,280 --> 00:06:51,880
come up you're gonna follow them there's only two ways you either follow them

54
00:06:51,880 --> 00:06:55,280
or you suppress them but this is the point the Buddha said there's a third

55
00:06:55,280 --> 00:06:59,640
way you when they come up you don't have to follow them but you don't have to

56
00:06:59,640 --> 00:07:03,600
suppress them they come up we say it's a problem if I don't suppress it I'm

57
00:07:03,600 --> 00:07:09,000
going to follow it I'm going to to become greedy I'm going or I'm going to

58
00:07:09,000 --> 00:07:14,160
to chase after and to become addicted to these things but it's not the case so

59
00:07:14,160 --> 00:07:19,240
the truly person if you ask me from my understanding of the Buddha's teaching

60
00:07:19,240 --> 00:07:23,720
it from my own practice is that these things come up and they can be really

61
00:07:23,720 --> 00:07:27,480
scary because we think this is bad this shouldn't be happening you know you

62
00:07:27,480 --> 00:07:33,400
want something it's really lustful for you your hate someone you find some

63
00:07:33,400 --> 00:07:36,600
people find when they meditate suddenly they're full of hate they hate

64
00:07:36,600 --> 00:07:41,240
everyone everything that comes out they're angry at everything but these are

65
00:07:41,240 --> 00:07:44,600
the emotions that exist in our minds and normally we would be guilty about

66
00:07:44,600 --> 00:07:48,520
them and suppress them and say that bad that's sinful that's wrong and they

67
00:07:48,520 --> 00:07:55,760
just rot they don't go away they they pickle our mind they call it cause the

68
00:07:55,760 --> 00:08:04,760
mind to become rotten and corrupted so the the this is this is the scary

69
00:08:04,760 --> 00:08:09,000
truth of the Buddha the Buddha's teachings that you have to let these things

70
00:08:09,000 --> 00:08:13,440
come up you have to be able to accept the evil that's inside of you you we

71
00:08:13,440 --> 00:08:17,880
say it's evil the reason we say it's evil is because it's false as you're

72
00:08:17,880 --> 00:08:22,120
suffering but that doesn't mean suppressing it is going to be going to answer

73
00:08:22,120 --> 00:08:25,320
things it doesn't mean you can deny the existence you have to accept the

74
00:08:25,320 --> 00:08:29,400
fact that inside of me there are things that are if you don't like the word

75
00:08:29,400 --> 00:08:35,480
evil they're unwholesome or they're unskillful or they they hurt us and they

76
00:08:35,480 --> 00:08:38,800
hurt other people and we should get rid of them but they're not going to go away

77
00:08:38,800 --> 00:08:44,360
just by covering them up so this is this is what I really hit me talking to

78
00:08:44,360 --> 00:08:50,840
this monk and through the meditation practice is that they're not they're not

79
00:08:50,840 --> 00:08:55,880
going to to disappear by themselves they have to come up and you have to

80
00:08:55,880 --> 00:09:02,840
accept them in the sense of accepting that they're there and and and take them

81
00:09:02,840 --> 00:09:07,680
apart pull them apart piece by piece and see them for what they are because in

82
00:09:07,680 --> 00:09:12,280
fact all that they are is a knot you know you say you look at this and you

83
00:09:12,280 --> 00:09:17,120
say it's you know it's a knot it's tangled up my hands are tangled up but

84
00:09:17,120 --> 00:09:21,160
really when you pull it apart there's no tangle it is disappeared when you

85
00:09:21,160 --> 00:09:26,120
untie the knot all you have is a straight straight straight row the knot is

86
00:09:26,120 --> 00:09:32,160
in fact a knot entity and this is a very important part of what the Buddha

87
00:09:32,160 --> 00:09:36,800
meant by and that that of non-self that it's not only that we don't have

88
00:09:36,800 --> 00:09:43,280
itself that's the point is that every phenomenon that arises has is not

89
00:09:43,280 --> 00:09:47,280
is a non entity it's it's not something that exists in and of itself and it's

90
00:09:47,280 --> 00:09:52,760
going to be in our mind you know forever or that we have to attack or fight once

91
00:09:52,760 --> 00:09:57,920
we see it we see that it's it's nothing it's a material it arises and it

92
00:09:57,920 --> 00:10:02,080
ceases and it has causes and conditions and if we don't give rise to the cause

93
00:10:02,080 --> 00:10:07,040
the result won't happen so this is my thought for the day and I think it's a

94
00:10:07,040 --> 00:10:11,440
very important teaching very important from the point of view of the past

95
00:10:11,440 --> 00:10:15,840
and the meditation and even more so because it's the first thing that the Buddha

96
00:10:15,840 --> 00:10:20,040
taught when he turned the wheel of the dhamma at Saranat and for the five

97
00:10:20,040 --> 00:10:24,080
months these five first five people who are supposedly the first five people

98
00:10:24,080 --> 00:10:28,320
to understand the Buddha's teaching and this is the first thing he taught he

99
00:10:28,320 --> 00:10:32,320
said we may be away on top of the teaching and let's see what he taught these

100
00:10:32,320 --> 00:10:38,840
are the two there are these two extremes that no one who has left behind or who

101
00:10:38,840 --> 00:10:46,360
has gone forth in search of freedom should follow after this is the addiction

102
00:10:46,360 --> 00:10:55,280
the sensuality or in the sense of chasing after the phenomena that arise and the

103
00:10:55,280 --> 00:11:04,720
suppression or the torturing yourself they are suppressing and repressing

104
00:11:04,720 --> 00:11:10,120
one's desire the addiction it is addiction the torturing yourself but this is how

105
00:11:10,120 --> 00:11:13,880
we torture ourselves we feel guilty we hate ourselves we cause ourselves

106
00:11:13,880 --> 00:11:18,200
suffering thinking that somehow this is you don't have to you don't have to stop

107
00:11:18,200 --> 00:11:22,600
eating or stand on one leg or all these crazy things that they do in

108
00:11:22,600 --> 00:11:27,600
did in the Buddha's time in India in order to torture yourself just by

109
00:11:27,600 --> 00:11:31,760
hating yourself by feeling guilty about yourself that is directly a form of

110
00:11:31,760 --> 00:11:35,680
torture and that's exactly what it's meant here and this is exactly what you

111
00:11:35,680 --> 00:11:39,920
experience in meditation that you you go between these two extremes one

112
00:11:39,920 --> 00:11:43,640
moment you're chasing after them this is great I love it the next moment you're

113
00:11:43,640 --> 00:11:48,840
castigating yourself for this is evil this is bad I'm a bad person and so on and

114
00:11:48,840 --> 00:11:54,560
anger and hatred arises and you torture yourself so thanks for tuning in this

115
00:11:54,560 --> 00:12:15,320
is Sri Lanka

