Hello and welcome back to our study of the Dhamapada.
Today we continue on with verse number 121, which reads as follows.
Which means one should not look down and should not think little of evil.
This will not come to me.
Not look down upon not think little of evil deeds thinking evil results won't come of them.
I am so in the case of just a small evil deed at this point.
Because uda bin dunipatina uda kumbo people are dead.
Raindrops or water falling one drop at a time.
Even one drop even drops of water are able to fill a water pot I think.
Are able to make a flood of water is actually.
Uda kumbo flood of water or a water jug depending on you ask.
Puerati fills the water jug probably.
In the same way balo Puerati papasa the fool becomes full of evil.
Tokang tokang be a tinang even though be it is gathered a tinang tokang tokang.
I always remember this tokang tokang tokang tokang means a bit or a small amount.
So tokang tokang means little by little.
Little by little one becomes full of evil.
So this was taught in regards to a certain equal monk whose name isn't given.
But it was a monk who failed to keep who did something actually quite a minor.
And the act itself wasn't necessarily, well wasn't terribly evil.
You might even say that the act itself was negligible.
Not something certainly to make a story about.
But it's an interesting story, it's very short.
So what happened was he was not, he didn't take care of his requisite.
He didn't take care of the furniture and the bedding and his belongings in general.
So sometimes you'd have to bring a chair outside and then he'd leave it outside.
Or he'd have his bedding outside to air it out.
His sheets or mattress or something.
And then he'd leave them outside and they'd get rained on or he would leave things unattended
or uncared for and the mice would eat them or the ants would eat them or so on.
And so he went through a lot of stuff and a lot of the belongings not just of his,
but of the sangha of the monastery were ruined as it was found.
So again, it's just one of those things that shouldn't be done.
But it's not like he's going to hell for it.
So the monks admonished him.
They said, you know, look, you shouldn't do this.
Or they asked him, you shouldn't, don't you think you should put your stuff away and take care of it?
And he would say to them, you know, it's just a trifling.
It's not a big deal.
It really is.
It's not even really worth worrying about.
We got more important things like meditation and study and thought.
And so he would do the same thing again and again, never learning, never changing.
And so the monks kind of got fed up with this and they went to the Buddha and they said,
you know, look what's going on.
And so the teacher sent the Buddha sent for him and said, is it true that you're acting in this way?
Is it true that you don't put your belongings away?
And when admonished that you don't stop, you keep doing it.
And so even to the Buddha, he said the same thing.
He said it's such a, it's such a small thing.
And only done, you know, it's wrong, but it's such a small, wrong, why worry about it?
It's not worth worrying about.
And the Buddha took this as an opportunity to teach.
So he was concerned, not, not exactly with the act, although he did end up making a rule against leaving stuff out.
But he was very, he seems concerned with the attitude.
This attitude of, of making little of your faults or dismissing small faults.
Which is interesting because there's this idea of being easy going and don't sweat the small stuff.
And there's something to that, certainly.
You shouldn't make things bigger than they actually are.
But you can't leave, it's like you can't leave an infection unattended.
As far as evil goes, there's no such thing as little.
And there's another quote in here that I think we've skipped over.
Comes from another one and I was trying to remember it's in regards to evil.
Appakanti, nava manita bhanu.
It is not proper to say, appakang, it's just a little.
You should never look upon an evil deed as it's too little.
The deed itself, I don't think is terribly an evil thing.
It's negligent to not look after your belongings.
But the attitude is much more important.
And the import of this verse is, of course, far beyond leaving stuff out and so on.
It's about the attitude of making light of something that is negligent.
You might have anything that is evil.
And this goes back to what we were talking about in the past couple of verses and even earlier verses.
Like this monk who did whatever he wanted, he did and it was sexually active and so on.
And it seemed pleasant.
When he wasn't doing what he wanted, he was miserable.
And he was suffering physically.
And so then when he started doing whatever he wanted, he actually flourished.
And prosperity was healthier and happier and so on.
So it looked good.
It was like, well, that's great.
I think meditators, when they come here, they often fall into this kind of doubt.
They think, you know, what am I doing here again?
If I want to stop the suffering, we're talking about ending suffering.
Why don't I just go home?
Then there would be no suffering.
It's actually quite remarkable that we come to meditate at all, especially with all the wonderful things out there,
wonderful ways of finding happiness, finding pleasure anyway.
But for some of us, for those of us who are perhaps more introspective and maybe more observant,
they come to see that it's not actually satisfaction.
It's not actually happiness.
It's concerning as you watch yourself filling yourself up, not with happiness,
but with desire, with greed, with attachment, and ultimately with evil.
Evil meaning those things that cause you suffering.
So you become more addicted and that's the addiction is pretty evil.
I mean, in a sense, it brings suffering to you.
And also anger and frustration and boredom and conflict.
So just conflict whenever you can't get what you want or someone stands in the way of you getting what you want.
If you have enough desire, you will create great conflict and adversity.
Even go to war over greed in this world.
Many wars are just over simple greed.
And so it takes real introspection to get there.
But that is the underlying truth, is that the evil doesn't go away.
If you become, if you cultivate addiction, you become more addicted.
If you cultivate anger and aversion, you become more set in those ways.
It's how habits are formed.
It's how we become who we are.
We come to meditate and we think that we're surprised at how much we think and how much anger and aversion
and how much greed and attachment and all these things that are inside it.
Where did they all come from, we think?
This is like you have water dripping into the pot and then you look down and you say,
oh, the pot's quite full and you don't know how did it get full, but actually the water was dripping down.
Mind is the same when you open it up and look and you actually take a look.
That's when you see what you're doing to yourself.
From most part, human beings are protected from the results on a large scale.
For the most part, we can avoid the suffering that comes from not getting what we want or getting what we don't want.
We're able to, because we live in a paradise, many of us, not all of us, of course.
As relative to other animals, for sure we live in many of us live in quite a paradise.
Always able to get food every day.
It's quite remarkable. There's no reason why we should be able to get enough food to eat, but we do.
We get more than enough food and further than that we get pleasure and entertainment and all of these things.
We aren't able to see what the result is of addiction, what the result is of aversion.
When we don't like something, we have ways of removing it.
You have pests in your home, you have pesticides.
We have many ways to get rid of the problems.
You have a headache, take a pill, you have a backache, get a massage.
You feel bored, there's lots of things to entertain you.
And so little by little we fill up our pot. Not with good, but with evil.
And the next verse, if you could guess, the next verse is going to be about doing good.
You shouldn't, well we'll talk about that one next time.
But what I wanted to say especially is meditation changes all that.
Meditation allows you to see.
And this is really how we understand karma in Buddhism.
So people talk about karma as being kind of a belief and it's really not.
Most people can, I have to believe it because they aren't able to see it.
But karma is something that you have to see on the momentary level.
A person who practices insight meditation is able to understand karma, is able to see it.
Because you are able to see moment to moment.
When I cling to things, it just leads to stress, it leads to suffering, it leads to busyness.
It doesn't make me more content, more satisfied, more happy.
When I get angry, the same thing, anger is just painful and unpleasant causes suffering.
On the other hand, when I cultivate mindfulness, when I cultivate clarity, in that moment,
it's sending out goodness. It's rippling out the effects of it.
You can see. You don't know where it's going.
Just like evil. When you become attached, it goes into the mix in your brain
and it affects your brain and your body. You don't really see where it's going.
Then to later, when it gets full and then it overflows and then you see the result.
But when you meditate, you can see the source. You can see what you're sending out.
You get to see how your reactions are affecting your brain or affecting your body or affecting the world around you,
the people around you and everything you say and you do.
When you're mindful, you can see all this.
It's a great thing about meditation. It's one of the first things you learn in the meditation practice.
Not intellectually, but you see it. You see how the mind works.
You see how karma works.
You lose doubt. You lose this doubt about good and evil and right and wrong.
You can understand that there is a result. There are consequences to our action.
This is an important fermentation. It's an important encouragement, I think, especially for those of us who are meditating to verify this and to say to ourselves,
this is a great thing I'm doing that I'm able to see this.
That I can adjust and that I can change my habits so that I feel my product not with addiction and aversion but with mindfulness and wisdom.
So, that's the Dhamapada short story. Wonderful verse.
Thank you all for tuning in, wishing you all the best.
