1
00:00:00,000 --> 00:00:09,080
okay good evening everyone welcome to our evening dama tonight we're

2
00:00:09,080 --> 00:00:19,040
looking at right view no right what should we write action tonight we're

3
00:00:19,040 --> 00:00:34,400
looking at right action some come under and so again we have to separate

4
00:00:34,400 --> 00:00:40,040
between the preliminary paths the mundane paths the path that we're all

5
00:00:40,040 --> 00:00:46,000
following when we do a meditation and the noble path which is the moment

6
00:00:46,000 --> 00:00:53,520
where you're actually right where you're actually in the right on the right

7
00:00:53,520 --> 00:01:04,600
path so they the has with all the factors it's generally described in

8
00:01:04,600 --> 00:01:12,920
terms of the mundane path and the idea with all of this is to understand to help

9
00:01:12,920 --> 00:01:17,800
us understand how we get to the noble path just because you're not killing or

10
00:01:17,800 --> 00:01:26,040
stealing or cheating doesn't mean you're on the noble path right but if you

11
00:01:26,040 --> 00:01:32,120
refrain from all kinds of wrong action that's what will lead you that's part of

12
00:01:32,120 --> 00:01:37,440
what will lead you to the noble path this is the point with talking about the

13
00:01:37,440 --> 00:01:42,080
preliminary path and it's the point of actually describing the hateful

14
00:01:42,080 --> 00:01:47,600
noble path because at the moment it seems kind of silly to say well you're

15
00:01:47,600 --> 00:01:52,320
right action you're not killing or stealing or cheating well of course you're

16
00:01:52,320 --> 00:01:57,520
not when you're in the meditative state your mind is so fixed you couldn't kill

17
00:01:57,520 --> 00:02:06,320
or lie or you know you couldn't kill or lie or do anything bad but it's about

18
00:02:06,320 --> 00:02:13,400
how we get there so in terms of the mundane path there are three factors for

19
00:02:13,400 --> 00:02:20,000
speech there were four for action there are three not killing not stealing and

20
00:02:20,000 --> 00:02:26,800
not committing adultery or what we call sexual misconduct which means cheating

21
00:02:26,800 --> 00:02:34,800
really you are working with someone else to cheat on on their partner any kind

22
00:02:34,800 --> 00:02:43,360
of romantic engagement that is a requires permission but doesn't get it

23
00:02:43,360 --> 00:02:50,480
that is betraying someone's trust so these three it's quite simple no

24
00:02:50,480 --> 00:03:00,240
killing no stealing and no cheating seems a little too simple right can't be

25
00:03:00,240 --> 00:03:06,240
that simple and again these are the indicators they're like signposts or

26
00:03:06,240 --> 00:03:12,320
fence posts in a way because the fence still has to be filled in but these

27
00:03:12,320 --> 00:03:16,600
give you a marker if you're killing you're outside of the noble path you're

28
00:03:16,600 --> 00:03:20,560
you're not gonna get to the noble not the outside you're you're not going to

29
00:03:20,560 --> 00:03:27,520
get to the noble path you're not on a path that will lead you to the noble path

30
00:03:27,520 --> 00:03:35,200
if you're stealing no refraining from these things as part of what allows you

31
00:03:35,200 --> 00:03:41,240
to concentrate to focus your mind but it's important to note that it's even

32
00:03:41,240 --> 00:03:47,200
on the mundane path it's actually not about the action itself when we talk

33
00:03:47,200 --> 00:03:50,920
about even when we talk about these things as indicators like oh if you're

34
00:03:50,920 --> 00:03:55,680
killing let's assign that you're not going in the right direction and so on

35
00:03:55,680 --> 00:04:01,120
but it's not even about the killing when we talk about right action we're

36
00:04:01,120 --> 00:04:05,760
not even talking about the action itself we're talking about the minds

37
00:04:05,760 --> 00:04:15,400
intention to commit the action we're talking about curbing that intention so that

38
00:04:15,400 --> 00:04:21,840
it doesn't become physical right we are talking about when the

39
00:04:21,840 --> 00:04:27,240
intention becomes physical but the physical act itself is an important

40
00:04:27,240 --> 00:04:33,360
or isn't itself important why this is important to specify is because of

41
00:04:33,360 --> 00:04:38,400
course you can kill something without meaning it meaning without meaning

42
00:04:38,400 --> 00:04:44,680
too you can kill a person without any bad intention we don't call it

43
00:04:44,680 --> 00:04:50,520
killing in Buddhism it's kind of interesting how often this question comes up

44
00:04:50,520 --> 00:04:56,200
if you step on an end someone this morning actually I said to me he killed a

45
00:04:56,200 --> 00:04:59,960
spider in the bathroom but he didn't mean to and he wasn't clear that it was

46
00:04:59,960 --> 00:05:08,160
not it was not wrong so it's important to be clear on this that it's not

47
00:05:08,160 --> 00:05:16,680
about the action it's about the intention the leading to the action and so

48
00:05:16,680 --> 00:05:24,520
of course this is the first step towards bringing the mind to a pure state is

49
00:05:24,520 --> 00:05:31,000
destroying the the most course defilements which are the actions where

50
00:05:31,000 --> 00:05:37,880
defilements become physical you can have such anger for someone and that's a

51
00:05:37,880 --> 00:05:44,040
bad thing but the moment you let it spill over into your action it's gone to

52
00:05:44,040 --> 00:05:52,680
another level it's categorically different it's now become a physical

53
00:05:52,680 --> 00:06:02,120
reality you attack someone even if you just attempt to kill them it's become

54
00:06:02,120 --> 00:06:09,800
wrong action in that sense and so technically though it's not how the Buddha

55
00:06:09,800 --> 00:06:18,280
describes it but for the purposes of our meditation as Buddhists we keep these

56
00:06:18,280 --> 00:06:26,800
sort of course fence posts in mind yes killing stealing cheating but as

57
00:06:26,800 --> 00:06:30,920
meditators it's it's important that we have a much more refined sense of

58
00:06:30,920 --> 00:06:38,920
right action right action especially I think in the sense that we try to do

59
00:06:38,920 --> 00:06:47,320
nothing with our hands our feet our bodies that is

60
00:06:47,320 --> 00:06:54,200
mindful you know what about if you stub your toe and you kick the table or

61
00:06:54,200 --> 00:07:00,600
you smash the the chair you throw it over your your computer isn't working and

62
00:07:00,600 --> 00:07:06,360
you pick up the monitor and you throw it on the ground that's wrong action

63
00:07:08,360 --> 00:07:14,920
you storm around or you or worse you hit someone you hurt someone

64
00:07:16,920 --> 00:07:22,840
right torture isn't in the precepts you can keep the five precepts and be a

65
00:07:22,840 --> 00:07:31,160
very nasty person doing lots of very nasty things rape rape I think some

66
00:07:31,160 --> 00:07:36,600
people would say it breaks the third precept but either it's it's just a

67
00:07:36,600 --> 00:07:40,840
you know no matter what no matter whether it does or not it's clear that there

68
00:07:40,840 --> 00:07:45,960
are these things that are wrong and as meditators we have a very refined

69
00:07:45,960 --> 00:07:50,120
sense of this during the meditation course you start to see that even

70
00:07:50,120 --> 00:07:57,000
turning your body quickly is wrong reaching for something quickly out of

71
00:07:57,000 --> 00:08:02,760
desire or scratching your face out of aversion

72
00:08:04,600 --> 00:08:09,880
there's wrong action you feel pain and adjusting yourself you start to see

73
00:08:09,880 --> 00:08:15,400
that even that is wrong because it's an it's a bad intention

74
00:08:15,400 --> 00:08:21,880
become an action which really does make a difference if you want to

75
00:08:21,880 --> 00:08:27,240
scratch for example eventually sometimes you do have to scratch

76
00:08:27,240 --> 00:08:35,480
I mean you just can't bear it right but when you when you when you train

77
00:08:35,480 --> 00:08:38,680
and you can bear with it and you say one thing one thing it's

78
00:08:38,680 --> 00:08:45,960
you know that's where concentration begins you ruin your concentration

79
00:08:45,960 --> 00:08:51,080
the moment it becomes physical scratching your face

80
00:08:55,320 --> 00:08:59,240
so meditators begin to become quite careful with their actions

81
00:08:59,240 --> 00:09:05,560
when they walk Masi says they Masi Sayedas says they they walk like sick

82
00:09:05,560 --> 00:09:12,200
people it looks like kind of like a sick word

83
00:09:12,200 --> 00:09:15,720
here because everyone's moving around slowly and

84
00:09:15,720 --> 00:09:21,240
very neutral expressions on their faces

85
00:09:23,000 --> 00:09:27,400
they're being very careful because they begin to see the

86
00:09:27,400 --> 00:09:36,200
disturbance that's caused by not being mindful that's caused by acting

87
00:09:36,200 --> 00:09:45,080
especially unmindfully and so we should think of this in both ways

88
00:09:45,080 --> 00:09:49,640
on the one hand we always think about these big big issues of not killing

89
00:09:49,640 --> 00:09:57,400
not stealing of course not committing adultery but as meditators we

90
00:09:57,400 --> 00:10:01,960
think of it on a micro scale as well with everything it's just like

91
00:10:01,960 --> 00:10:07,880
speech right whatever you think you say it should be mindful

92
00:10:07,880 --> 00:10:12,760
right it's more difficult to be mindful speaking it's possible

93
00:10:12,760 --> 00:10:17,320
but with acting it's much more doable and much more part of her practice

94
00:10:17,320 --> 00:10:21,640
right actually the Buddha said with both if you read the Sati Patanasu

95
00:10:21,640 --> 00:10:26,200
maybe we'll go through the Sati Patanasu to eventually

96
00:10:27,720 --> 00:10:31,000
I said when you're speaking or when you're keeping silent

97
00:10:31,000 --> 00:10:38,120
you should be fully comprehending walking forward walking back reaching

98
00:10:38,120 --> 00:10:43,320
extending your arm flexing your arm eating drinking

99
00:10:43,320 --> 00:10:50,440
urinating defecating everything should be done mindfully

100
00:10:54,680 --> 00:10:59,240
and then you see this difference where the the

101
00:10:59,240 --> 00:11:04,280
the defilements the unwholesome the problematic intentions

102
00:11:04,280 --> 00:11:08,360
they don't break through to become actions

103
00:11:08,360 --> 00:11:13,240
technically that anytime they do break through it's wrong action

104
00:11:13,240 --> 00:11:16,360
I mean not actually not technically according to the Satas it doesn't

105
00:11:16,360 --> 00:11:20,440
go into that much detail but from a perspective of the you know the

106
00:11:20,440 --> 00:11:25,000
core teaching of the Buddha in an ultimate sense it's quite clear

107
00:11:25,000 --> 00:11:29,800
that it becomes what we might call wrong action

108
00:11:30,440 --> 00:11:35,880
it's called the kamakile say think where the defiled actions

109
00:11:35,880 --> 00:11:43,320
our actions based on defilement it's the the most coarse type of defilement

110
00:11:46,280 --> 00:11:53,400
so quite simple not easy but simple

111
00:11:53,400 --> 00:11:57,000
that's the teaching on right action

112
00:11:59,000 --> 00:12:02,200
do we have any questions

113
00:12:02,200 --> 00:12:08,760
again questions are all to be asked to our website

114
00:12:08,760 --> 00:12:12,280
and I understand that means you have to log in but

115
00:12:12,280 --> 00:12:15,560
it's a small price to pay

116
00:12:18,040 --> 00:12:21,720
can you progress by only practicing insight meditation no chanting may

117
00:12:21,720 --> 00:12:24,280
to et cetera

118
00:12:24,840 --> 00:12:30,440
progress is a fairly vague word so we have

119
00:12:30,440 --> 00:12:36,280
useful and necessary and there are a lot of things that are useful

120
00:12:36,280 --> 00:12:40,200
there's not so many things that are necessary

121
00:12:40,760 --> 00:12:47,400
so if your question were do you need to practice insight meditation to

122
00:12:47,400 --> 00:12:52,360
progress or can you do it simply by chanting and doing

123
00:12:52,360 --> 00:12:59,800
mitta then the answer is no or no the answer is you do need to practice insight

124
00:12:59,800 --> 00:13:04,440
meditation can chanting and mitta help do they help yes

125
00:13:04,440 --> 00:13:08,680
they are useful helpful they can be

126
00:13:08,680 --> 00:13:13,400
there are potential problems with like chanting for example you can get

127
00:13:13,400 --> 00:13:16,680
caught up in it and it can actually become a distraction

128
00:13:16,680 --> 00:13:21,000
mitta not so much if you're actually practicing mitta it's pretty much always

129
00:13:21,000 --> 00:13:24,680
useful but if you get caught up in mitta it can

130
00:13:24,680 --> 00:13:29,400
sidetrack you it's true because you take time away from doing

131
00:13:29,400 --> 00:13:33,320
insight meditation so that should be a little more specific with your

132
00:13:33,320 --> 00:13:37,480
question hopefully that answered it

133
00:13:37,480 --> 00:13:41,160
what is Buddhism's view on playing video games well

134
00:13:41,160 --> 00:13:45,160
there's nothing in the scriptures about playing video games I've answered

135
00:13:45,160 --> 00:13:48,360
this sort of question before I mean the question of when you

136
00:13:48,360 --> 00:13:51,480
kill someone we used to play when I was young

137
00:13:51,480 --> 00:13:55,640
doom this game where you're really pretty awful

138
00:13:55,640 --> 00:14:01,000
and lots of games we played so it's not killing

139
00:14:01,000 --> 00:14:05,640
it's an elaborate means of challenging

140
00:14:05,640 --> 00:14:10,760
your reflexes your hand-eye coordination

141
00:14:10,760 --> 00:14:16,440
your ability to process so there's actually some potentially good states I

142
00:14:16,440 --> 00:14:20,760
would say involved mundane good states not really Buddhist but

143
00:14:20,760 --> 00:14:24,360
mundane in the sense it's the kind of thing that could probably help you

144
00:14:24,360 --> 00:14:27,960
think better you know critical thinking skills or ability to

145
00:14:27,960 --> 00:14:34,360
strategize and that kind of thing but the biggest problem with video games

146
00:14:34,360 --> 00:14:39,640
is not the violence the biggest problem in my mind anyway

147
00:14:39,640 --> 00:14:44,440
the biggest problem is of course the attachment it's this

148
00:14:44,440 --> 00:14:48,120
it's this pleasure you know it's like classical conditioning

149
00:14:48,120 --> 00:14:52,200
or operant conditioning you can't remember you you do something

150
00:14:52,200 --> 00:14:55,480
you get what you want and it makes you pleasure and so you you know you

151
00:14:55,480 --> 00:14:58,680
keep practicing keep playing the reason we play games

152
00:14:58,680 --> 00:15:02,680
is because of these moments of success that give us such a rush

153
00:15:02,680 --> 00:15:06,760
it's really just a chemical addiction or addiction to these

154
00:15:06,760 --> 00:15:13,800
opioids or what are they called the chemicals in our brain we're addicted to them

155
00:15:13,800 --> 00:15:18,280
so we we've found a way using our computer to stimulate our brains

156
00:15:18,280 --> 00:15:22,360
to stimulate these chemicals but it's still just chemical

157
00:15:22,360 --> 00:15:26,920
and so it's addictive and if you know anything about the addiction cycle it

158
00:15:26,920 --> 00:15:31,080
becomes less and less satisfying

159
00:15:31,960 --> 00:15:34,680
and so you're ultimately you're constantly really

160
00:15:34,680 --> 00:15:41,400
met with dissatisfaction but I wouldn't worry about like

161
00:15:41,400 --> 00:15:44,760
I mean I wouldn't worry too much about it especially if you're as a lay person

162
00:15:44,760 --> 00:15:47,800
it's not the kind of thing that's going to send you to hell

163
00:15:47,800 --> 00:15:50,680
you know you play these games and you killed all these people is that

164
00:15:50,680 --> 00:15:54,440
gonna send you to hell no no I mean it works to just keep

165
00:15:54,440 --> 00:15:59,000
you being born and if you really get obsessed you can be born as a

166
00:15:59,000 --> 00:16:03,720
hungry ghost because of the amount the intense addiction

167
00:16:03,720 --> 00:16:07,720
but generally speaking it's not the worst I mean

168
00:16:07,720 --> 00:16:12,200
we talk about things like sexual activity and

169
00:16:12,200 --> 00:16:17,000
you know entertainment these are not these are not things that stop you

170
00:16:17,000 --> 00:16:20,840
from attaining enlightenment they just hinder it they make it slower and

171
00:16:20,840 --> 00:16:25,080
more difficult but they're not wrong action

172
00:16:25,080 --> 00:16:29,560
in that broader sense of of really blocking you from the practice so killing

173
00:16:29,560 --> 00:16:34,520
stealing and cheating these are things that will block you

174
00:16:34,520 --> 00:16:40,200
playing video games well it's not kind of helped you certainly

175
00:16:44,200 --> 00:16:47,720
why not make different Buddhist traditions because they teach different

176
00:16:47,720 --> 00:16:52,600
things I'll often teach contradictory things

177
00:16:52,600 --> 00:16:59,320
I can lead to doubt confusion it's not to say you don't always

178
00:16:59,320 --> 00:17:03,240
it's not about mixing traditions it's about finding the truth

179
00:17:03,240 --> 00:17:07,000
if this tradition teaches something that's good and beneficial then

180
00:17:07,000 --> 00:17:13,000
and good so it's about finding what's right and true

181
00:17:13,720 --> 00:17:17,640
that's not the whole end so that's part of it but the other thing is

182
00:17:17,640 --> 00:17:22,120
there are different ways to reach the same goal

183
00:17:22,120 --> 00:17:28,760
but having two different regimens can conflict just in a functional sense

184
00:17:28,760 --> 00:17:33,720
right like our our I follow the Mahasi lineage

185
00:17:33,720 --> 00:17:37,640
but if you went to Burma the Mahasi people would say no you don't

186
00:17:37,640 --> 00:17:41,160
because while some might because they're quite sticklers for a very specific

187
00:17:41,160 --> 00:17:47,080
way of doing this type of meditation and we do it differently so

188
00:17:47,080 --> 00:17:50,680
if you if you come to our center what you do it this way if you go to the

189
00:17:50,680 --> 00:17:54,600
Mahasi center you know you'll do it a different way

190
00:17:54,600 --> 00:17:59,160
and if you try and do it both ways

191
00:17:59,160 --> 00:18:03,320
within the Mahasi tradition not that big of a deal but

192
00:18:03,320 --> 00:18:08,280
say if you then went and went to a center where they did

193
00:18:08,280 --> 00:18:12,120
on upon a satay or something like that

194
00:18:13,720 --> 00:18:18,120
it you know it it complicates things because you're trying to do it two

195
00:18:18,120 --> 00:18:23,960
different ways at once and I think I would criticize people who like to

196
00:18:23,960 --> 00:18:28,680
try different ways and it usually I would argue

197
00:18:28,680 --> 00:18:32,680
because they simply don't want to put out the effort and they it's hard to

198
00:18:32,680 --> 00:18:36,920
put out the effort in one way so you look for something new that's going to

199
00:18:36,920 --> 00:18:40,600
make it easy and it's just this classical avoidance

200
00:18:40,600 --> 00:18:45,720
which is of course very common in spirituality as well as ordinary life

201
00:18:45,720 --> 00:18:50,680
so finding a tradition and sticking to it

202
00:18:50,680 --> 00:18:55,320
sticking to it is quite different from finding

203
00:18:55,320 --> 00:18:58,680
and you can run around and and try all sorts of different ways it's not been

204
00:18:58,680 --> 00:19:01,800
helped you and I would argue that it hinders you

205
00:19:01,800 --> 00:19:04,760
yeah you get lots of different skills from lots of different traditions and

206
00:19:04,760 --> 00:19:09,320
people think that's a good thing but you never become a master of any one

207
00:19:09,320 --> 00:19:22,280
skill of hair loss

208
00:19:22,840 --> 00:19:27,720
it hurts you I mean you don't like it if you don't like it then you say

209
00:19:27,720 --> 00:19:33,240
disliking disliking just liking again I hope you've read my my booklet if you

210
00:19:33,240 --> 00:19:35,480
haven't read my booklet I would encourage you to read it

211
00:19:35,480 --> 00:19:41,560
because it might help you deal with this but this isn't hurting you this is a

212
00:19:41,560 --> 00:19:45,160
disliking the hair loss isn't hurting you

213
00:19:45,160 --> 00:19:49,240
it's hurting you as you don't like it I assume unless there's something weird

214
00:19:49,240 --> 00:19:52,280
painful that I'm not understanding in which case it's just pain and you would say

215
00:19:52,280 --> 00:19:57,800
pain pain trying to be mindful of it

216
00:20:00,040 --> 00:20:02,840
okay so that's all the questions for tonight

217
00:20:02,840 --> 00:20:06,760
thank you all for tuning in

