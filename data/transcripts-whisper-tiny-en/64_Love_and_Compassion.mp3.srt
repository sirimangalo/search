1
00:00:00,000 --> 00:00:02,000
Love and compassion.

2
00:00:03,000 --> 00:00:06,000
What is the difference between meta and compassion?

3
00:00:07,000 --> 00:00:11,000
Meta and Corona are like two sides of the same coin.

4
00:00:11,000 --> 00:00:13,000
Meta means friendliness.

5
00:00:13,000 --> 00:00:16,000
It comes from the word mitter, meaning a friend.

6
00:00:16,000 --> 00:00:21,000
You strengthen the eye and it becomes an E and so you get meta.

7
00:00:22,000 --> 00:00:24,000
The state of being a friend.

8
00:00:24,000 --> 00:00:28,000
Meta refers to the inclination that beings should be happy,

9
00:00:28,000 --> 00:00:33,000
leading to act, speak and think in ways that promote the happiness of others.

10
00:00:34,000 --> 00:00:39,000
Corona compassion is the complementary wish for beings to not suffer.

11
00:00:39,000 --> 00:00:45,000
It leads one to act, speak and think in ways that help free other beings from suffering.

12
00:00:46,000 --> 00:00:50,000
Neither meta nor Corona themselves refer to actions.

13
00:00:50,000 --> 00:00:56,000
They are states of mind that will inform your actions helping to bring about positive results.

14
00:00:56,000 --> 00:01:02,000
One cultivates meta by repeating a mantra like may all beings be happy.

15
00:01:02,000 --> 00:01:07,000
And the results will be an inclination to bring happiness and peace to others.

16
00:01:07,000 --> 00:01:12,000
With Corona, the mantra is something like may all beings be free from suffering,

17
00:01:12,000 --> 00:01:17,000
which will result in an inclination to work to relieve suffering.

18
00:01:17,000 --> 00:01:24,000
In the end, both are working toward the same goal as true happiness is freedom from suffering.

19
00:01:24,000 --> 00:01:29,000
They are one in the same, but the state of mind is different.

20
00:01:29,000 --> 00:01:37,000
Friendliness is an inclination towards positive results and compassion is an inclination away from negative results.

21
00:01:37,000 --> 00:02:01,000
In brief, Corona is the inclination to take away suffering and meta is the inclination to bestow happiness.

