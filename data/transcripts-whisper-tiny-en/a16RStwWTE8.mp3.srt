1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study in the Namapada.

2
00:00:05,000 --> 00:00:11,000
Today we continue on with verse 158, which means it's follows.

3
00:00:11,000 --> 00:00:17,000
Atana me wa pattamong, pattirupi any way saiye.

4
00:00:17,000 --> 00:00:24,000
Atan yam manu sa saiye, nakide saiye, pandito.

5
00:00:24,000 --> 00:00:35,000
Which means one should set oneself in what is right first.

6
00:00:35,000 --> 00:00:43,000
Then if one should teach others, it will not be sullied.

7
00:00:43,000 --> 00:00:50,000
Not giles, the wise will not sullied themselves or will not be defiled.

8
00:00:50,000 --> 00:00:59,000
When they teach others, because they've set themselves in what is right.

9
00:00:59,000 --> 00:01:04,000
So this was told in regards to Upananda here.

10
00:01:04,000 --> 00:01:06,000
Upananda was Sakya.

11
00:01:06,000 --> 00:01:11,000
He was a relative of the Buddha and so used to a life of luxury.

12
00:01:11,000 --> 00:01:17,000
When he became a monkey, had a hard time giving that up.

13
00:01:17,000 --> 00:01:24,000
He was a greedy monk.

14
00:01:24,000 --> 00:01:30,000
Not only that, but he was somewhat crooked as we'll see.

15
00:01:30,000 --> 00:01:37,000
He had a lot of possessions and he went around from monastery to monastery,

16
00:01:37,000 --> 00:01:39,000
collecting possessions.

17
00:01:39,000 --> 00:01:46,000
He heard a teaching of the Buddha once on a fewness of wishes.

18
00:01:46,000 --> 00:01:49,000
He was a pitcher.

19
00:01:49,000 --> 00:01:56,000
A pitcher, a pitcher, a dadi, but he's a new tung-dung-dung-dung.

20
00:01:56,000 --> 00:02:06,000
So he heard a dhammatog that was connected with many things, including a fewness of wishes.

21
00:02:06,000 --> 00:02:14,000
One thing little, this idea that one thing is never going to make you happy.

22
00:02:14,000 --> 00:02:21,000
Never you want, rather more you get, but then the more you want.

23
00:02:21,000 --> 00:02:24,000
There's always one thing.

24
00:02:24,000 --> 00:02:32,000
The other way to be truly happy is to be content, to be a few or have no wishes,

25
00:02:32,000 --> 00:02:38,000
no ones, then you can be truly happy.

26
00:02:38,000 --> 00:02:42,000
So he heard this teaching and he made good use of it in his mind.

27
00:02:42,000 --> 00:02:47,000
He went around preaching it to others, and he found that by preaching it to others,

28
00:02:47,000 --> 00:02:50,000
they would give up other stuff.

29
00:02:50,000 --> 00:02:54,000
So as a result, he was able to collect even more,

30
00:02:54,000 --> 00:02:59,000
both because he was apparently a fairly proficient teacher,

31
00:02:59,000 --> 00:03:05,000
but also because the teaching itself inclined people to discard things.

32
00:03:05,000 --> 00:03:13,000
And so when it came to have the reigns, he went around to various monasteries

33
00:03:13,000 --> 00:03:17,000
and left some of his belongings in the various monasteries.

34
00:03:17,000 --> 00:03:21,000
At the end of the reigns, when they were divvying up,

35
00:03:21,000 --> 00:03:25,000
the gifts that had been given in support of the monks who had kept the reigns retreat.

36
00:03:25,000 --> 00:03:31,000
He took a portion from each of these monasteries because he had made it seem like he was living

37
00:03:31,000 --> 00:03:34,000
in each of these four monasteries.

38
00:03:34,000 --> 00:03:37,000
And by this time, he had so much stuff that he had to put in on a cart,

39
00:03:37,000 --> 00:03:40,000
and so he's carting this stuff around,

40
00:03:40,000 --> 00:03:47,000
and he comes upon these two monks who are trying to split up their possessions.

41
00:03:47,000 --> 00:03:53,000
They've acquired two pieces of cloth and one piece of wool,

42
00:03:53,000 --> 00:04:00,000
and the wool is, of course, very expensive, maybe it's cashmere or something.

43
00:04:00,000 --> 00:04:06,000
And so they look and they see, oh, here comes that monk who teaches about fewness of wishes.

44
00:04:06,000 --> 00:04:10,000
Well, he'll be able to teach us the right way to divide these things

45
00:04:10,000 --> 00:04:14,000
so that we're not cheating each other.

46
00:04:14,000 --> 00:04:19,000
An open undist looks at this and he sees this luxurious wool blanket.

47
00:04:19,000 --> 00:04:21,000
And he says, okay, I can fix this for you.

48
00:04:21,000 --> 00:04:25,000
And he gives one piece of cloth to one monk, the other piece of cloth to the other monk.

49
00:04:25,000 --> 00:04:31,000
And he says, I'll take the blanket for myself. They're decided.

50
00:04:31,000 --> 00:04:37,000
The monks away, and this place to say these two monks are not really happy with his method of dividing the possessions.

51
00:04:37,000 --> 00:04:39,000
And so they go see the Buddha.

52
00:04:39,000 --> 00:04:42,000
You know, the Buddha over the Buddha tells them a story of his past life.

53
00:04:42,000 --> 00:04:45,000
He says in the past, open undo is the same.

54
00:04:45,000 --> 00:04:53,000
And he tells one of the Chateka stories about a jackal who divided up a fish for two otters, I think.

55
00:04:53,000 --> 00:04:55,000
Same sort of idea.

56
00:04:55,000 --> 00:04:58,000
And the Buddha said, then the Buddha, what did he do?

57
00:04:58,000 --> 00:05:01,000
He just complained about open undo himself.

58
00:05:01,000 --> 00:05:05,000
He said, open undo, acting no monk should act in this way.

59
00:05:05,000 --> 00:05:11,000
And in general, I'm going to practice what one preaches.

60
00:05:11,000 --> 00:05:15,000
One should set oneself in what is right first.

61
00:05:15,000 --> 00:05:21,000
Only then, if one wishes, only then should one teach.

62
00:05:21,000 --> 00:05:24,000
Because your teaching is marred otherwise.

63
00:05:24,000 --> 00:05:26,000
That's what this verse is about.

64
00:05:26,000 --> 00:05:30,000
If you don't practice what you preach, there's something wrong with it.

65
00:05:30,000 --> 00:05:39,000
I mean, it's quite likely to be an incomplete or a skewed teaching based on your inner

66
00:05:39,000 --> 00:05:44,000
defilements or your inner inconsistency.

67
00:05:44,000 --> 00:05:49,000
The fact that you yourself haven't internalized the teaching.

68
00:05:49,000 --> 00:05:55,000
And so that's the most obvious message in this verse that we should practice what we preach.

69
00:05:55,000 --> 00:05:57,000
But there's more to it, I think.

70
00:05:57,000 --> 00:06:05,000
It points to a more general teaching of the difference between externalizing something and internalizing it.

71
00:06:05,000 --> 00:06:09,000
The outward gaze and the inner gaze, right?

72
00:06:09,000 --> 00:06:11,000
And Buddhism has this sort of quandary.

73
00:06:11,000 --> 00:06:19,000
And actually in ancient India, there was this dilemma of whether one should work on one's own.

74
00:06:19,000 --> 00:06:23,000
You'll go off into the forest and work on one's own.

75
00:06:23,000 --> 00:06:29,000
Mental powers or mental abilities or work for one's own freedom and enlightenment.

76
00:06:29,000 --> 00:06:36,000
Or whether one should stay at home and look after your family and be a productive member of society.

77
00:06:36,000 --> 00:06:42,000
So in Buddhism, of course, this problem should be going help others, teach others, bring other people to become enlightened

78
00:06:42,000 --> 00:06:45,000
or should be seek out enlightenment for ourselves.

79
00:06:45,000 --> 00:06:47,000
And what's the place of the two of them now?

80
00:06:47,000 --> 00:06:53,000
This verse is a clear coming down on the side of helping yourself.

81
00:06:53,000 --> 00:06:56,000
But it also makes clear that there are these two aspects.

82
00:06:56,000 --> 00:07:08,000
The our expression and our personalities are state of mind, that which we express with.

83
00:07:08,000 --> 00:07:18,000
And it's far too common for people who, especially with those who come or even those who come to Buddhism,

84
00:07:18,000 --> 00:07:21,000
to favor the outward expression.

85
00:07:21,000 --> 00:07:26,000
So they might favor outward practices of love and compassion.

86
00:07:26,000 --> 00:07:34,000
They might favor outward expressions of wisdom, of debating and discussing and explaining and teaching to others.

87
00:07:34,000 --> 00:07:44,000
They may become all preachy and arrogant and denouncing those who don't practice according to the teachings.

88
00:07:44,000 --> 00:07:51,000
When they themselves aren't necessarily practicing them either.

89
00:07:51,000 --> 00:07:55,000
And so there's nothing wrong with many of those things, right?

90
00:07:55,000 --> 00:08:03,000
But they're very much dependent or they should be dependent and they're very much below

91
00:08:03,000 --> 00:08:13,000
in terms of benefit, in terms of the outcome, the inward cultivation of these things.

92
00:08:13,000 --> 00:08:22,000
And so it's very easy to think of the Buddhist teachings intellectually and think,

93
00:08:22,000 --> 00:08:27,000
well, yes, I understand that intellectually and then you say, well, then I'll go and share that with other people.

94
00:08:27,000 --> 00:08:32,000
So in this case, he may have even thought that he was doing the right thing and may not even have connected.

95
00:08:32,000 --> 00:08:33,000
You know?

96
00:08:33,000 --> 00:08:38,000
We have this disconnect between our intellectual understanding and our experiential understanding.

97
00:08:38,000 --> 00:08:43,000
So I can intellectually say that this is bad and that is bad.

98
00:08:43,000 --> 00:08:51,000
So it was anger is bad, but then I might still get angry or greed is bad, but I might still want things.

99
00:08:51,000 --> 00:09:04,000
And so in terms of just a benefit of what actually leads to success, progress, happiness, peace freedom from suffering,

100
00:09:04,000 --> 00:09:10,000
there's quite a difference between the external and the internal.

101
00:09:10,000 --> 00:09:15,000
And so when we ask ourselves, what do we want to do with these teachings and really in general,

102
00:09:15,000 --> 00:09:17,000
what do we want to do with our lives?

103
00:09:17,000 --> 00:09:20,000
Because we can spend our whole lives helping others.

104
00:09:20,000 --> 00:09:23,000
Then you have to ask yourself, what does that really do for me?

105
00:09:23,000 --> 00:09:25,000
What does that really bring?

106
00:09:25,000 --> 00:09:29,000
And what good would it do if everyone were trying to help other people, right?

107
00:09:29,000 --> 00:09:34,000
Well, there would be external goodness, right?

108
00:09:34,000 --> 00:09:39,000
There would also be a lot of problems and conflicts when people have different ideas of helping

109
00:09:39,000 --> 00:09:44,000
and when they get frustrated and burnt out because others aren't amenable, right?

110
00:09:44,000 --> 00:09:49,000
We're all trying to help others, but no one's actually looking inside and fixing themselves,

111
00:09:49,000 --> 00:09:53,000
fixing their problems.

112
00:09:53,000 --> 00:10:00,000
And it's quite frustrating and fruitless, really.

113
00:10:00,000 --> 00:10:07,000
And so even with things like mindfulness and meditation, it's easy to talk about them and to understand them intellectually.

114
00:10:07,000 --> 00:10:14,000
It's even easy to sit down and sit still and close your eyes for ten minutes, half an hour, even an hour.

115
00:10:14,000 --> 00:10:20,000
But to actually make that shift where you're actually practicing things,

116
00:10:20,000 --> 00:10:27,000
we're actually cultivating, in this case, contentment, which is very much a part of the meditation practice.

117
00:10:27,000 --> 00:10:35,000
In terms of our meditation, this means the cultivation of objectivity, where we stop reacting to things.

118
00:10:35,000 --> 00:10:42,000
When we see something we like, we learn to change that, so we just see it and the liking doesn't arise.

119
00:10:42,000 --> 00:10:45,000
Why? Because we see that it's not worth liking.

120
00:10:45,000 --> 00:10:52,000
It's not even that we fix ourselves, but it's that we look at the world in a different way we turn inward.

121
00:10:52,000 --> 00:10:55,000
Or not even inwards, really.

122
00:10:55,000 --> 00:11:01,000
We root ourselves in reality, the present and the here and the next.

123
00:11:01,000 --> 00:11:08,000
We change from thinking about things abstract in terms of a room that I'm in and people and so on.

124
00:11:08,000 --> 00:11:20,000
To experience it, to the moment now there's the sound of my voice, there's the feelings and there's the sights and so on.

125
00:11:20,000 --> 00:11:24,000
There's all of that and it's arising and ceasing.

126
00:11:24,000 --> 00:11:29,000
When you actually look at that, not just here what I'm saying and appreciate it intellectually,

127
00:11:29,000 --> 00:11:35,000
and then maybe go and teach it to others, when you actually put that into practice,

128
00:11:35,000 --> 00:11:40,000
that's where the greatest benefit can come, that's where the teachings are meant to me.

129
00:11:40,000 --> 00:11:44,000
That's what it means to set yourself in something first.

130
00:11:44,000 --> 00:11:48,000
It's very easy to slip into this sharing with others.

131
00:11:48,000 --> 00:11:51,000
It's something that religious people do quite well.

132
00:11:51,000 --> 00:11:59,000
It's much more difficult to actually help yourself until the Buddha was constantly reminding his followers.

133
00:11:59,000 --> 00:12:03,000
Set yourself in what is right first.

134
00:12:03,000 --> 00:12:07,000
Because otherwise it's it's it's marred by that.

135
00:12:07,000 --> 00:12:13,000
If you go and help others, there's never going to be pure.

136
00:12:13,000 --> 00:12:15,000
Let me do this with everything really.

137
00:12:15,000 --> 00:12:18,000
I mean this is a way of explaining with anger, for example.

138
00:12:18,000 --> 00:12:26,000
When we're angry, we hold on to it and we use it to speak, to act.

139
00:12:26,000 --> 00:12:31,000
When we're greedy, when we're craving something, when we have desire,

140
00:12:31,000 --> 00:12:39,000
we use the desire to reach for it to seek out, to strive out, strive after what we want.

141
00:12:39,000 --> 00:12:45,000
When we have delusion, we become all arrogant and conceited and so on.

142
00:12:45,000 --> 00:12:49,000
Instead of looking at these things themselves, we never actually look at the anger,

143
00:12:49,000 --> 00:12:51,000
the greed, the delusion.

144
00:12:51,000 --> 00:12:57,000
We never actually look at our experiences, cleansing the source,

145
00:12:57,000 --> 00:13:03,000
the source of our actions and our speech and even our thought.

146
00:13:03,000 --> 00:13:07,000
And so it's this difference between, I mean of course it's good to be out in the world helping.

147
00:13:07,000 --> 00:13:12,000
It's good to teach, it's good to share the Buddha's teaching absolutely.

148
00:13:12,000 --> 00:13:17,000
But everything your expression is, it's always going to be an expression of what you've got inside.

149
00:13:17,000 --> 00:13:23,000
So if you're an angry, bitter, greedy, arrogant person,

150
00:13:23,000 --> 00:13:27,000
even sharing the Buddha's teaching is not going to be pure, there's going to be,

151
00:13:27,000 --> 00:13:34,000
and even if it is, it's not really the, it's not the most wonderful thing to do with the dhamma.

152
00:13:34,000 --> 00:13:36,000
It's not of any use to you.

153
00:13:36,000 --> 00:13:43,000
Like, if you use, materially, you become like Upananda and you get lots of rose and stuff.

154
00:13:43,000 --> 00:13:45,000
That's a great teacher.

155
00:13:45,000 --> 00:13:49,000
But you never become free from suffering, you never actually taste.

156
00:13:49,000 --> 00:13:57,000
But as I said, you're like a shepherd who looks after the other people's sheep.

157
00:13:57,000 --> 00:14:07,000
You never actually get the products of the cows, the cow herd, whenever it gets the milk.

158
00:14:07,000 --> 00:14:12,000
So the lesson here is work on yourself.

159
00:14:12,000 --> 00:14:19,000
Even if your goal is to help others purify the source, because just like a river, the source of the river,

160
00:14:19,000 --> 00:14:21,000
defines the quality of the water.

161
00:14:21,000 --> 00:14:25,000
If the source is in pure, the river can never be pure.

162
00:14:25,000 --> 00:14:30,000
All of our actions and our speech and our deeds, actions, speech, our thoughts,

163
00:14:30,000 --> 00:14:35,000
they all depend on our purity, the source of mind.

164
00:14:35,000 --> 00:14:46,000
If our mind is in pure, we can't expect anything we do say or even think to be of benefit to us or benefit to others.

165
00:14:46,000 --> 00:14:49,000
And so that's really what meditation is about.

166
00:14:49,000 --> 00:15:03,000
It's about this inner work, this inner cultivation, the activity of coming to purify the source,

167
00:15:03,000 --> 00:15:05,000
and set ourselves in what is right.

168
00:15:05,000 --> 00:15:07,000
That doesn't mean what but the mind.

169
00:15:07,000 --> 00:15:14,000
That doesn't mean that the mind is setting yourself in what is right first.

170
00:15:14,000 --> 00:15:18,000
Then, if you're going to teach, if you're going to share, if you're going to act in the world,

171
00:15:18,000 --> 00:15:24,000
just make sure you've got that purity and you won't defile yourself in your actions.

172
00:15:24,000 --> 00:15:26,000
You want to embarrass yourself.

173
00:15:26,000 --> 00:15:35,000
You want to regret all the things that you do with the lack of clarity and greed and the anger and the delusion.

174
00:15:35,000 --> 00:15:36,000
So there you go.

175
00:15:36,000 --> 00:15:38,000
That's the dhammapada for tonight.

176
00:15:38,000 --> 00:15:39,000
Thank you all for tuning in.

177
00:15:39,000 --> 00:16:00,000
I should all the best.

