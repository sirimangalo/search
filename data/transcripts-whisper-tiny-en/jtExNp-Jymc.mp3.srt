1
00:00:00,000 --> 00:00:07,280
oops oops it was reloaded

2
00:00:07,280 --> 00:00:18,240
which one are we at? all right i got it

3
00:00:18,240 --> 00:00:26,480
what about Burma down there do we miss one? um what is it like? why riots in Burma?

4
00:00:26,480 --> 00:00:36,960
why riots in Burma? i don't know because people are people

5
00:00:36,960 --> 00:00:41,920
people riot Burma has a lot of issues

6
00:00:41,920 --> 00:00:51,120
one thing i would like to talk about is the Buddhists i saw an article recently

7
00:00:51,120 --> 00:00:59,040
Buddhists outraged very recently Buddhists outraged over

8
00:00:59,040 --> 00:01:06,880
Buddha images on shoes i think pictures of Buddhists on the Buddha on shoes

9
00:01:06,880 --> 00:01:14,400
and it's such an absurdity to write an article saying Buddhist outrage it's an insult

10
00:01:14,400 --> 00:01:19,680
really how could a Buddhist be outraged how could you call yourself a Buddhist if

11
00:01:19,680 --> 00:01:23,840
you're outraged at anything the Buddha was very clear

12
00:01:23,840 --> 00:01:30,640
if you get angry when people are sawing you savagely limb from limb

13
00:01:30,640 --> 00:01:37,520
with a saw you're not a follower of the Buddha

14
00:01:37,520 --> 00:01:41,600
so how could you possibly be Buddhist if you're outraged how could you

15
00:01:41,600 --> 00:01:46,000
possibly write an article stating Buddhists outraged you have to say

16
00:01:46,000 --> 00:01:51,680
people claiming to be Buddhist outraged at etc etc

17
00:01:51,680 --> 00:01:55,440
well you know it's not i'm just making fun

18
00:01:55,440 --> 00:02:00,400
but it made me think that i didn't read the article i just read the headline

19
00:02:00,400 --> 00:02:09,280
it made me think that this is this is a real problem with labels

20
00:02:09,280 --> 00:02:14,240
and something that has to be understood by people

21
00:02:14,240 --> 00:02:17,760
you're not something because you say you say you're something

22
00:02:17,760 --> 00:02:22,960
Buddhists who riot or kill people or even protest governments

23
00:02:22,960 --> 00:02:25,760
it's not a Buddhist this is not a Buddhist thing to do

24
00:02:25,760 --> 00:02:31,120
they're not doing something that is in mind the Buddha's teaching

25
00:02:31,120 --> 00:02:35,760
even though they might claim that they are you know they might say it's in the

26
00:02:35,760 --> 00:02:38,960
name of Buddhism protecting Buddhism and so on

27
00:02:38,960 --> 00:02:42,560
this is not the Buddha's teaching you can you can't protect Buddhism by getting

28
00:02:42,560 --> 00:02:46,320
angry right these people thinking they're protecting Buddhism

29
00:02:46,320 --> 00:02:49,680
don't let the Buddha images on their feet down with them tear down their

30
00:02:49,680 --> 00:02:54,240
store Buddhists Buddhist monks who go and burn

31
00:02:54,240 --> 00:02:57,760
Muslim temples they built a Muslim temple on our sacred holy

32
00:02:57,760 --> 00:03:02,640
site burn it down tear it down throw them out kill them even

33
00:03:02,640 --> 00:03:09,440
protect Buddhism ridiculous can't protect Buddhism by getting upset

34
00:03:09,440 --> 00:03:12,960
let me protect Buddhism by letting go

35
00:03:12,960 --> 00:03:16,000
so that's what i think about riots and

36
00:03:16,000 --> 00:03:20,640
in a

37
00:03:20,640 --> 00:03:41,680
verb or whatever

