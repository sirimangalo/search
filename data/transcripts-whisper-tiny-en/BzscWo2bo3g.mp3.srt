1
00:00:00,000 --> 00:00:08,720
It seems to me that being a female a Buddhist nun is more difficult in a practical

2
00:00:08,720 --> 00:00:15,200
organizational sense than being a Buddhist monk. It seems that it is more difficult to find

3
00:00:15,200 --> 00:00:22,880
a good place to stay and generally organize life as a nun and not to be sort of a servant

4
00:00:22,880 --> 00:00:35,440
in a monastery. Am I under or wrong impression? It depends very much on where you are and with whom

5
00:00:35,440 --> 00:00:47,280
you are. It is certainly more difficult to find a good place to stay because there are not so many

6
00:00:47,280 --> 00:01:00,480
yet. It is getting better. There are bikinis who are becoming senior enough to ordain and there

7
00:01:00,480 --> 00:01:13,440
are more sponsors for bikuni places. So, it will become a little bit more simple in near future,

8
00:01:13,440 --> 00:01:26,240
I would say for now and for me I try not to focus on the difference between male and female. I try

9
00:01:26,240 --> 00:01:41,520
just to be a good monk. I think that is what is important for the Buddha. In the albedama, I read

10
00:01:43,520 --> 00:01:55,200
female and male is just a physical characteristic. The mind can be defiled in a male or in a female

11
00:01:55,200 --> 00:02:04,800
body or it can liberate it in a male or female body. So, in that sense we are kind of in the same

12
00:02:05,760 --> 00:02:14,160
situation, in the same bad situation that is still in some Sarah. There are in countries like

13
00:02:14,160 --> 00:02:26,320
Thailand for example, eight precept nuns or Sri Lanka here are many 10 precept nuns and for them

14
00:02:26,320 --> 00:02:37,920
it is such a situation for being a servant in a monastery. They coke and clean and are really

15
00:02:37,920 --> 00:02:46,720
servant and sometimes in some places like slaves of the monks. It is heartbreaking. But in some

16
00:02:46,720 --> 00:02:52,400
places it is not really an argument that I use but there is the argument in Thailand.

17
00:02:54,880 --> 00:02:58,480
There is an example I was staying up on the mountain and I commented to the one of the monks

18
00:02:59,120 --> 00:03:06,800
and in Deoisutab. I said the westerners are not really, they are kind of upset at this. They see

19
00:03:06,800 --> 00:03:14,400
the nuns all working in the kitchen and he said, well what do you expect these nuns to do? That is

20
00:03:14,400 --> 00:03:21,120
all they are skill is to cook. The monks are work that are working in the office. It is because

21
00:03:21,120 --> 00:03:27,840
they have come from an office job before and Deoisutab that is the truth. These monks have been

22
00:03:28,960 --> 00:03:35,120
in whatever government positions and he said we take people according to their abilities.

23
00:03:35,120 --> 00:03:41,120
That is no excuse of course for making them work like a slave which is really terrible.

24
00:03:42,880 --> 00:03:51,440
But there is something there that in Thailand or in Sri Lanka the monks in one sense the monks

25
00:03:51,440 --> 00:03:57,520
and the nuns are both slaves because they have put themselves in a position of being priests

26
00:03:57,520 --> 00:04:05,040
and priestesses or whatever. In the sense that they work for the people they will do ceremonies

27
00:04:05,040 --> 00:04:09,920
and go to ceremony after ceremony and they become slaves not only to the lay people they become

28
00:04:09,920 --> 00:04:16,000
slaves to their desires. Some of them have become slaves to their ambitions. Some of them become

29
00:04:16,000 --> 00:04:22,000
slaves to their desires to such an extent that they abuse children and horrific things.

30
00:04:22,000 --> 00:04:31,760
These sorts of things go on. So I would say it depends largely on the community because the

31
00:04:31,760 --> 00:04:40,960
example I would give in what John Tongue for example the nuns are now in some of the highest

32
00:04:40,960 --> 00:04:47,680
positions. In terms of bureaucracy they are not. They are still under the monks and that is the

33
00:04:47,680 --> 00:04:53,840
deal with Thailand. But when I go to John Tongue I get scolded by this nun in the office because

34
00:04:53,840 --> 00:04:58,880
she runs the office and she is like number two in charge of the monastery. Underneath this monk

35
00:04:58,880 --> 00:05:06,160
who is I don't want to talk too much about the place but it is not a place I like to spend my time

36
00:05:07,360 --> 00:05:14,640
except because my teacher is there. But she has quite a bit of power and that is how it has always

37
00:05:14,640 --> 00:05:19,920
been there. The nuns have actually run the main office. So people enjoy some tables they wire

38
00:05:19,920 --> 00:05:23,840
the monks on the office well in John Tongue and the nuns are in the office in what lampoon

39
00:05:23,840 --> 00:05:30,240
the nuns are in the office as well. The monks have their office, the nuns have their office and

40
00:05:31,040 --> 00:05:35,680
if you look at what lampoon I think it is a pretty good example of I don't know maybe the nuns

41
00:05:35,680 --> 00:05:41,520
do have to work harder than the monks. Some of the monks get away with murder at what lampoon

42
00:05:41,520 --> 00:05:51,200
where they used to. Yeah I think in general in what lampoon everybody has a job two week alternation.

43
00:05:51,200 --> 00:05:59,120
John Tongue as well. In fact in John Tongue it is a bit weighted towards the nuns. The nuns do

44
00:05:59,120 --> 00:06:05,840
harder work in the kitchen because those nuns who have been cooks before and most women in

45
00:06:05,840 --> 00:06:09,760
Thailand are pretty good at cooking that is what they were trained to do by the culture.

46
00:06:09,760 --> 00:06:15,760
So those nuns who will do work in the kitchen work pretty hard and I can attest to that having

47
00:06:15,760 --> 00:06:21,840
seen it but they only do it for two weeks and then they have two weeks off. The monks don't get that.

48
00:06:21,840 --> 00:06:27,200
The monks who work some of them are working every day, every day, every day and I don't know of

49
00:06:27,200 --> 00:06:33,280
any monks that have some of them there are some things and it is changing now. So just to say it

50
00:06:33,280 --> 00:06:37,360
largely the real problem of course is that the nuns in Thailand are not real nuns and they are

51
00:06:37,360 --> 00:06:45,200
not given status as nuns. There is this pretend kind of creation that we have developed and it

52
00:06:45,200 --> 00:06:50,160
has become so normal that everyone is saying well why don't you just be content with being a

53
00:06:50,160 --> 00:06:54,240
mate? Why don't you just be content with being this fake thing that we have set out but it is

54
00:06:54,240 --> 00:07:00,000
actually fake and it actually came later. The truth is they should be following that which the

55
00:07:00,000 --> 00:07:04,960
Buddha laid out and all of these rules that the Buddha laid out which are there for a purpose

56
00:07:04,960 --> 00:07:10,640
to keep the communal harmony and to keep the monks and the nuns from getting in trouble with

57
00:07:10,640 --> 00:07:17,040
each other and so on. I think that is more a problem than another slave thing but that so much

58
00:07:17,040 --> 00:07:24,240
of culture as well. Asian culture is not very friendly towards women in general. Buddhism aside

59
00:07:25,200 --> 00:07:32,400
but again this is probably a hot button topic. But I think it is really all about

60
00:07:32,400 --> 00:07:40,560
liberation of the mind and should not be about being female or being male.

61
00:07:45,040 --> 00:07:51,920
But then I think that there is a good point there that both of them are totally lost. If you

62
00:07:51,920 --> 00:07:56,240
are talking about monks and nuns in general in Sri Lanka, don't even go there. These aren't

63
00:07:56,240 --> 00:08:01,120
monks, these aren't nuns in the Sutta sense. Neither one of them are at all interested in liberation

64
00:08:01,120 --> 00:08:06,640
and their practice is not going to lead them towards liberation. And in fact, the nuns are often

65
00:08:06,640 --> 00:08:11,760
more sincere because they don't, this is what the argument was in Thailand, that the nuns would

66
00:08:11,760 --> 00:08:19,680
often practice harder because they didn't ordain for for for status. They didn't ordain to become

67
00:08:19,680 --> 00:08:26,400
some high nun because you can't really or you can but it's very difficult. So they were the ones

68
00:08:26,400 --> 00:08:30,880
who were there because they really wanted to practice because they saw suffering because they felt

69
00:08:30,880 --> 00:08:36,240
like their role as a woman in this sexist society was a lot of suffering. And so they wanted to

70
00:08:36,240 --> 00:08:43,120
become free from that. Most of the meditation centers in Thailand are full of women and they tend

71
00:08:43,120 --> 00:08:52,160
to practice a lot better than the monks. There's Adjansupan. He was saying the monks are first in

72
00:08:52,160 --> 00:08:59,920
everything. When it's time for food, monks first. When it's when it's time for getting a ride

73
00:08:59,920 --> 00:09:05,360
in the car or something, monks first, with everything the monks go first except meditation. When it

74
00:09:05,360 --> 00:09:11,760
comes to meditation, the monks say, oh, you go first. He actually said that in front of a room full

75
00:09:11,760 --> 00:09:17,600
of people, a room full of monks and lay people I think. I can't remember where he said it but I was there

76
00:09:17,600 --> 00:09:26,880
when he said it. Just anyway, but no, but that goes against this idea that for sure what you see

77
00:09:26,880 --> 00:09:34,400
in the bhikuni, what is it, the tarijatha? I think the tarijatha, Mara always comes to them and says,

78
00:09:34,400 --> 00:09:40,000
what does a woman have to do with enlightenment with your two finger wisdom? Two finger wisdom

79
00:09:40,000 --> 00:09:44,880
apparently has to do with stirring the pot or something or I don't know, measuring or something.

80
00:09:45,680 --> 00:09:54,560
Some women, stereotypical women thing. And the bhikuni would always say, what is there of a woman to

81
00:09:54,560 --> 00:10:04,880
someone who is let go of craving? They would answer in this way. What should I be relating to

82
00:10:04,880 --> 00:10:10,400
the word woman? What is there in me that is woman? What do I have to do with woman?

83
00:10:12,800 --> 00:10:20,080
This way they would make Mara very sad and upset because he couldn't trick them into becoming

84
00:10:20,080 --> 00:10:27,120
what's the word? I don't want to say feminist because that's probably that thing to say but

85
00:10:29,040 --> 00:10:36,800
feminist in a way that you know, really gung-ho about being a male or being a female or about the

86
00:10:36,800 --> 00:10:44,720
genders because in the end the mind is the same. There's a story in the commentary, the

87
00:10:44,720 --> 00:10:54,080
Dhammapada commentary. I think of a one commentary where this man, I think it was Mahakasupa.

88
00:10:54,640 --> 00:10:59,600
He saw Mahakasupa going on arms and he was he was some, you know, raunchy

89
00:11:01,200 --> 00:11:05,200
lay person and he looked at him and he said, oh, that elder is quite attractive.

90
00:11:06,000 --> 00:11:10,720
Wouldn't it be great if he were my wife? He actually thought this. He kind of thought if he

91
00:11:10,720 --> 00:11:17,600
was a woman, I'd take him as my wife right away. And the story goes that as a result because

92
00:11:17,600 --> 00:11:26,160
Mahakasupa was an aura hunt and as a result of fashioning this idea of taking a enlightened

93
00:11:26,160 --> 00:11:37,520
being as their wife, his gender changed. He became a woman. There was such a strong attraction there

94
00:11:37,520 --> 00:11:43,840
of, you know, heterosexual attraction, I guess that the commentary says he changed his gender

95
00:11:45,280 --> 00:11:52,960
to fit with the thought of a man and a woman together in that way. But, you know, that's just

96
00:11:52,960 --> 00:11:57,360
one of the commentaries that may or may not have happened. But it said to have happened right then

97
00:11:57,360 --> 00:12:02,480
in there that he actually changed genders and later on he, you know, and then he gave birth to two

98
00:12:02,480 --> 00:12:08,080
sons. He became a woman was like, oh, now I'm a woman. They chased him out because they didn't

99
00:12:08,080 --> 00:12:13,600
recognize him. They said, who's this crazy woman here? And then he found him, a husband,

100
00:12:13,600 --> 00:12:20,240
got married, had two kids. And then he met the Mahakasupa again. He saw him coming and he realized

101
00:12:20,240 --> 00:12:25,280
how silly he had been. And he went up to Mahakasupa and he asked forgiveness. When he asked

102
00:12:25,280 --> 00:12:33,920
forgiveness, he became a man again. So here he was the father of having given birth, the only man

103
00:12:33,920 --> 00:12:41,120
in the recorded history to ever have given birth to two sons. Something like that, it's one of

104
00:12:41,120 --> 00:12:48,240
those crazy stories. Thank you Phil. Two finger wisdom refers to checking the dundness of cooking

105
00:12:48,240 --> 00:12:58,240
rights. That's what the commentary says, I think. It's some kind of idiom.

