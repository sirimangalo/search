1
00:00:00,000 --> 00:00:03,720
Do you ever get frustrated with answering the same questions?

2
00:00:03,720 --> 00:00:08,720
And if not, how do you continue to do so and seem so patient?

3
00:00:08,720 --> 00:00:11,760
Teaching is really easy.

4
00:00:11,760 --> 00:00:17,760
You know, I mean, there's so much happiness that comes from helping other people.

5
00:00:17,760 --> 00:00:23,040
It can actually become intoxicating, it can become hindrance to the path.

6
00:00:23,040 --> 00:00:28,560
It can for sure be a hindrance, even if you're not intoxicated in it, because it takes

7
00:00:28,560 --> 00:00:35,120
your time away from your meditation.

8
00:00:35,120 --> 00:00:42,600
So that's one thing is that it's actually not as unpleasant as it seems.

9
00:00:42,600 --> 00:00:48,000
The more unpleasant thing is to sit around learning how to teach.

10
00:00:48,000 --> 00:00:54,320
No, when you have to sit and listen to someone teaching, the same thing's over and over

11
00:00:54,320 --> 00:00:55,320
again.

12
00:00:55,320 --> 00:00:58,080
So some of you have been with me for a long time and you hear me saying the same things

13
00:00:58,080 --> 00:01:00,880
over and over again.

14
00:01:00,880 --> 00:01:01,880
This is more difficult.

15
00:01:01,880 --> 00:01:07,240
But what I'm thinking of more is when I do reporting, when I'm sitting in reporting, I can

16
00:01:07,240 --> 00:01:09,600
do reporting for hours and hours and hours.

17
00:01:09,600 --> 00:01:10,960
Reporting means meeting with meditators.

18
00:01:10,960 --> 00:01:19,840
I can have 20, 30 meditators and actually feel quite relaxed and refreshed afterwards.

19
00:01:19,840 --> 00:01:23,960
But people who have to sit and listen to me say the same things again and again and

20
00:01:23,960 --> 00:01:26,120
again and again.

21
00:01:26,120 --> 00:01:31,560
That's where the suffering comes from because they don't have the same happiness that

22
00:01:31,560 --> 00:01:39,560
comes from it and the same hustle, the same wholesomeness that comes from it.

23
00:01:39,560 --> 00:01:45,160
But the other thing is there is something to be said because there is a, it's a training

24
00:01:45,160 --> 00:01:51,360
and I've been teaching now for, I started very early, I was teaching when I was only two

25
00:01:51,360 --> 00:01:52,360
years a month.

26
00:01:52,360 --> 00:01:55,080
So it's a skill like anything else.

27
00:01:55,080 --> 00:01:56,080
I used to give dumbah talks.

28
00:01:56,080 --> 00:02:03,680
If there's anyone still around from toys to tap times, they can probably attest to the fact

29
00:02:03,680 --> 00:02:10,480
that when I first started giving talks and teaching, it was not much fun for the meditators.

30
00:02:10,480 --> 00:02:16,880
Sometimes I would give horrible talks, you know, just totally not put people off but put

31
00:02:16,880 --> 00:02:28,280
people to sleep and not be able to get my point across and not be able to be interesting

32
00:02:28,280 --> 00:02:30,000
to people.

33
00:02:30,000 --> 00:02:37,120
So like anything, it's really a skill and that helps because when you make a misstep,

34
00:02:37,120 --> 00:02:44,120
when you say something inappropriate, when you lose your train of thought, when you get,

35
00:02:44,120 --> 00:02:53,560
you start getting flustered and, you know, like in this case, are unable to come up with

36
00:02:53,560 --> 00:02:58,440
the words properly, then it creates stress in the mind.

37
00:02:58,440 --> 00:03:04,440
So as you get better at things like teaching, it does get a lot easier.

38
00:03:04,440 --> 00:03:08,720
But the real thing I want to say in regards to this question, which I think makes an important

39
00:03:08,720 --> 00:03:11,960
question is that teaching is not the most difficult thing.

40
00:03:11,960 --> 00:03:15,520
Most difficult thing is the practice.

41
00:03:15,520 --> 00:03:21,760
People think that teaching is actually the sign that you are enlightened, it's a sign

42
00:03:21,760 --> 00:03:26,040
that you are advanced and as I said, it's really just a skill.

43
00:03:26,040 --> 00:03:35,720
People can be great teachers and not have the greatest practice on their own or even beyond

44
00:03:35,720 --> 00:03:36,880
the wrong path.

45
00:03:36,880 --> 00:03:40,920
There are examples in the scriptures of people who are great teachers but were not

46
00:03:40,920 --> 00:03:43,360
enlightened themselves.

47
00:03:43,360 --> 00:03:54,560
So don't get too confident in someone, don't think of me as some great enlightened being

48
00:03:54,560 --> 00:03:59,880
just because I can teach enlightenment is something that you get from yourself, from practicing

49
00:03:59,880 --> 00:04:01,120
the teachings.

50
00:04:01,120 --> 00:04:13,480
That's not the same as being able to give convincing and clear answers.

51
00:04:13,480 --> 00:04:17,880
Even though obviously, you know, clarity of mind does help, meditation does help if I wasn't

52
00:04:17,880 --> 00:04:26,440
meditating, I can't say that I would be able to stay patient and answer people's questions

53
00:04:26,440 --> 00:04:35,160
for sure not, for sure I would try and fail miserably, but they're not exactly the same.

54
00:04:35,160 --> 00:05:04,400
So it's important not to get too preoccupied with one's ability to teach.

