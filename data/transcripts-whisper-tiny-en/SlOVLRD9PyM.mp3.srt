1
00:00:00,000 --> 00:00:10,520
Okay, all the following has proven as life extending or being very healthy one doing sports or physical exercise

2
00:00:11,080 --> 00:00:12,880
two sex

3
00:00:12,880 --> 00:00:17,440
Three eating many times a day for sleeping at least six hours a night

4
00:00:18,000 --> 00:00:21,640
Which is some considers all these ideas bad. How do you explain this?

5
00:00:22,480 --> 00:00:24,480
Very good question. Oh

6
00:00:24,480 --> 00:00:26,480
Hmm

7
00:00:26,480 --> 00:00:28,480
It's a tough one. Oh

8
00:00:30,080 --> 00:00:35,280
Okay, well, let's let's start by countering some of these claims

9
00:00:37,600 --> 00:00:39,600
Okay

10
00:00:40,160 --> 00:00:46,080
Sports and physical exercise a lot of the reason why it's considered to be healthy is

11
00:00:46,640 --> 00:00:49,480
Because if people eat many times a day for example

12
00:00:50,320 --> 00:00:54,320
And eat well, let's just say eat too much and start there

13
00:00:54,320 --> 00:00:56,320
Hmm

14
00:01:00,800 --> 00:01:06,240
Also because their bodies operate tend to operate sub-optimally

15
00:01:07,680 --> 00:01:09,680
and so

16
00:01:09,680 --> 00:01:11,680
That's going to lead to

17
00:01:13,840 --> 00:01:17,840
Because of the mind now the mind will cause tension in the body

18
00:01:17,840 --> 00:01:22,720
we'll cause

19
00:01:24,880 --> 00:01:32,160
Overworking of the systems in the body you do to hormone releases, you know based on sex for example

20
00:01:33,200 --> 00:01:37,600
But just any kind of desire or anger or frustration

21
00:01:38,160 --> 00:01:40,160
this is going to

22
00:01:40,160 --> 00:01:51,840
Disrupt the bodily systems or over-tax them and so it actually can you know lead to build up of toxins that kind of thing

23
00:01:57,760 --> 00:02:02,320
And so hence you need there's the need to to to exercise

24
00:02:02,320 --> 00:02:09,840
Do sports and do exercise this helps people but you see

25
00:02:10,880 --> 00:02:16,720
All of these studies for all of these things are dealing most likely with people who don't meditate or

26
00:02:17,280 --> 00:02:20,000
You know, maybe do a little bit of meditation, but not

27
00:02:20,480 --> 00:02:25,680
Seriously, you know, you're dealing with ordinary people who consider sports to be their meditation

28
00:02:26,000 --> 00:02:29,880
So it's helpful for them. It's a way of relaxing. Well good for them

29
00:02:29,880 --> 00:02:33,640
If you practice meditation especially walking meditation

30
00:02:34,040 --> 00:02:38,920
It's you'd have to do a study of those people and to see whether sports

31
00:02:39,640 --> 00:02:44,440
You know whether they were actually lacking something and I think you'd find that no indeed. They're not I mean

32
00:02:45,240 --> 00:02:48,200
ridiculous because consider all these consider

33
00:02:49,640 --> 00:02:57,720
You know the the sort of the stereotypical Asian person living on the mountain who lives to be 90 or 100 who never does any sports

34
00:02:57,720 --> 00:02:59,640
I

35
00:02:59,640 --> 00:03:05,960
guess in some cases they're doing a lot of physical activity, but in many cases not in many cases

36
00:03:05,960 --> 00:03:07,960
It's just peaceful living, you know

37
00:03:08,760 --> 00:03:10,760
Peaceful living changes so much

38
00:03:11,400 --> 00:03:15,880
That's one thing so anyway talking about sports and physical exercise

39
00:03:15,880 --> 00:03:17,880
I think you can challenge that

40
00:03:17,880 --> 00:03:22,040
based on the meditative lifestyle that there's really no reason to

41
00:03:22,040 --> 00:03:28,360
To require your muscles to be bigger or your cardiovascular system to be extra to be

42
00:03:29,720 --> 00:03:31,160
Worked out

43
00:03:31,160 --> 00:03:33,160
if your mind is

44
00:03:35,960 --> 00:03:41,720
Mazing tune with the body because you will find through meditation that your body functions

45
00:03:42,440 --> 00:03:47,560
far more efficiently through meditation you can feel in the meditation practice that a lot of toxins are being

46
00:03:47,560 --> 00:03:51,240
Flushed out

47
00:03:53,480 --> 00:03:55,480
Okay, sex

48
00:03:56,680 --> 00:04:00,360
Sex well besides the fact that it's a physical activity

49
00:04:02,680 --> 00:04:08,200
I don't know what to say here because you're you're you know you study the addiction

50
00:04:09,160 --> 00:04:13,560
Centers in the brain, you know what you're talking about. I guess is the release of

51
00:04:13,560 --> 00:04:17,400
Endorphins and whatever

52
00:04:18,520 --> 00:04:20,520
There's a lot of dopamine. I'm sure

53
00:04:22,360 --> 00:04:29,480
Oxytocin. I don't know. I mean I'm sure there's a lot. Yeah, which one dopamine is desire and oxytocin is pleasure

54
00:04:29,480 --> 00:04:35,320
I don't I'm sure it's all caught up in there, but you have a serious problem with all of this whatever benefit

55
00:04:36,440 --> 00:04:42,760
I don't know that there's any physical benefit to to the expiration of dopamine or the you know the good production of dopamine oxytocin

56
00:04:42,760 --> 00:04:48,600
Whatever these chemicals are they I don't I mean, I'm not a chemist or a biologist

57
00:04:49,320 --> 00:04:50,920
but I do know

58
00:04:50,920 --> 00:04:53,800
You're just from listening and of course from practice

59
00:04:54,760 --> 00:05:00,040
That you've got a serious problem here because you're cultivating addiction these systems

60
00:05:01,160 --> 00:05:09,000
Seriously affect the brain in a bad way. You know they cultivate what we call the addiction cycle. It's studied, you know that

61
00:05:09,000 --> 00:05:13,560
sex leads you to desire more sex and

62
00:05:15,320 --> 00:05:17,320
actually makes you less

63
00:05:18,440 --> 00:05:20,440
satisfied, you know more

64
00:05:21,160 --> 00:05:23,160
Impatient

65
00:05:24,360 --> 00:05:31,000
That this is not theoretical. You can study how these addiction cycle works. It leads to addiction. There's no question

66
00:05:31,720 --> 00:05:34,360
What I think is probably not well

67
00:05:34,360 --> 00:05:41,920
Research is what addiction does to you? And or we don't realize it's like, okay, so I get addicted to sex. It's fine

68
00:05:41,920 --> 00:05:43,960
You know, I'm happy to have a active sex life

69
00:05:44,600 --> 00:05:45,640
but

70
00:05:45,640 --> 00:05:51,640
What does the question is that's not the question is what does this addiction cycle do to you? Does it have any negative effects?

71
00:05:51,640 --> 00:05:56,760
And I think absolutely it does it leads to the potential for greater anger and frustration depression

72
00:05:57,960 --> 00:05:59,960
boredom and

73
00:05:59,960 --> 00:06:04,120
dissatisfaction inability to

74
00:06:04,440 --> 00:06:08,600
I mean, these are all saying the same thing, but basically inability to stay at peace with yourself

75
00:06:13,800 --> 00:06:16,280
So the idea that sex is

76
00:06:18,840 --> 00:06:23,720
Wonder if you're saying about these things life extending or being very healthy

77
00:06:23,720 --> 00:06:31,240
Okay, well, let's get back to the life extending thing

78
00:06:31,960 --> 00:06:33,000
Um

79
00:06:33,480 --> 00:06:38,200
Yeah, okay, no, let's not let we have to include it there because life extending is even from a Buddhist point of view

80
00:06:38,200 --> 00:06:40,200
That's a good thing good to live longer

81
00:06:44,040 --> 00:06:46,040
At the expense of

82
00:06:46,040 --> 00:06:52,520
You're well as again, I would say the the benefits are

83
00:06:53,800 --> 00:06:57,800
Purely based on the fact that the person has too much stress in their lives and

84
00:06:59,240 --> 00:07:04,760
This is their only outlet so absolutely a person who is suppressing their sexual urges and and

85
00:07:07,160 --> 00:07:12,440
Sitting around as a couch potato all the time. This is unhealthy and so that

86
00:07:12,440 --> 00:07:16,920
Release is is the only way to physically cope with that

87
00:07:17,320 --> 00:07:24,600
You know, it's just a bouncing back and forth, but I would say definitely a person who is able to overcome those things

88
00:07:25,720 --> 00:07:27,720
Is is is

89
00:07:28,200 --> 00:07:33,000
Better off. I would I would argue will actually have the potential to live longer

90
00:07:33,880 --> 00:07:36,040
But moreover

91
00:07:36,040 --> 00:07:43,960
avoids the huge huge problem of addiction sports and and exercise the same. I mean sports can be quite addictive

92
00:07:45,160 --> 00:07:48,280
We have and if you don't believe you don't if you think that's absurd

93
00:07:49,320 --> 00:07:55,320
People who come and meditate can prove it anyone any sports person who comes to meditate has a very difficult time sitting still

94
00:07:55,800 --> 00:07:57,800
They're not able to because

95
00:07:58,360 --> 00:08:00,360
They have an addiction

96
00:08:00,360 --> 00:08:03,800
And that's going to spill over in your life. It's going to make you

97
00:08:03,800 --> 00:08:05,800
Jumpy is going to make you

98
00:08:07,080 --> 00:08:09,640
You know somehow require something

99
00:08:10,360 --> 00:08:13,480
Which means that when you're in a position to not be able to

100
00:08:14,600 --> 00:08:17,720
Indulge in your addiction you're going to be frustrated

101
00:08:17,720 --> 00:08:22,840
You know as you get older if you ever end up being bedridden. It's going to be frustrating. It's going to make you

102
00:08:23,560 --> 00:08:25,560
depressed and so on

103
00:08:25,560 --> 00:08:30,360
Definitely this happens sports people who suffer injuries in sports. They can become depressed and

104
00:08:30,920 --> 00:08:32,920
frustrated and turn to

105
00:08:32,920 --> 00:08:35,640
alcohol and get in extreme cases, but

106
00:08:38,600 --> 00:08:40,600
It's a problem so

107
00:08:41,400 --> 00:08:43,400
Which gets to sort of the

108
00:08:43,960 --> 00:08:48,520
Other and other part of this answer is that Buddhism doesn't isn't so concerned with the body

109
00:08:49,160 --> 00:08:51,560
Living long good thing living long with

110
00:08:52,600 --> 00:08:58,680
By cultivating addiction bad thing, you know if if as you if if if living a hundred years

111
00:08:58,680 --> 00:09:04,280
Means you can cultivate more addiction than better to die early, you know

112
00:09:05,080 --> 00:09:08,600
Horrible thing to say, but you're not doing anything good by living

113
00:09:11,480 --> 00:09:15,320
So bed is a Buddha said yo yo jawasa satan gee way

114
00:09:17,160 --> 00:09:19,160
A person would lay up a young

115
00:09:19,560 --> 00:09:21,560
a kahang gee vidang say yo

116
00:09:21,560 --> 00:09:23,560
person would lay up a

117
00:09:23,960 --> 00:09:25,000
person

118
00:09:25,000 --> 00:09:29,000
That's not quite the correct grammar. Um

119
00:09:29,400 --> 00:09:31,560
A better to live better. It is to live one day

120
00:09:32,440 --> 00:09:34,440
Better than to live a hundred years

121
00:09:35,160 --> 00:09:37,160
Not seeing the truth

122
00:09:37,640 --> 00:09:41,000
Not seeing a rising and ceasing that's seeing the nature of things

123
00:09:43,000 --> 00:09:46,200
Better to live one day having seen this truth

124
00:09:48,360 --> 00:09:50,360
So

125
00:09:50,360 --> 00:09:55,400
That's the the second more probably the more deep part of this answer

126
00:09:56,360 --> 00:09:58,360
But I don't think these claims should be

127
00:09:59,400 --> 00:10:04,360
Gone and shall should be left unchallenged so number two number three eating many times a day

128
00:10:05,000 --> 00:10:07,160
I've heard this and I've had doctors tell me

129
00:10:08,200 --> 00:10:15,320
Even that tie a tie a doctor told me he said I don't want to say bad things about monks and then he basically went on to say how bad it is

130
00:10:15,320 --> 00:10:17,320
That monks only even the morning

131
00:10:17,320 --> 00:10:19,320
Um

132
00:10:19,720 --> 00:10:23,400
Again, I want to say that it has to do with people's

133
00:10:26,440 --> 00:10:28,440
They're

134
00:10:28,440 --> 00:10:30,200
Lifestyles

135
00:10:30,200 --> 00:10:33,400
But I guess I'd have to look deeper into the science of it

136
00:10:33,400 --> 00:10:38,040
I want to say that eating once a day is great. You know, it feels regular

137
00:10:38,840 --> 00:10:41,320
Eating once a day is just one meal for example

138
00:10:42,440 --> 00:10:45,000
My guess you know my hypothesis is that

139
00:10:45,000 --> 00:10:51,800
You know the digestive cycle works on a 24-hour something like a 24-hour cycle anyway. You fully digest food

140
00:10:52,760 --> 00:10:55,800
Maybe that's myth, but as I understood it was a 24-hour thing

141
00:10:55,880 --> 00:11:04,360
So you get into a very regular cycle in your your bowel movements are regular everything seems, you know quite

142
00:11:06,840 --> 00:11:08,440
No

143
00:11:08,440 --> 00:11:10,040
efficient

144
00:11:10,040 --> 00:11:12,040
Eating only one meal a day

145
00:11:12,040 --> 00:11:14,040
But again

146
00:11:14,040 --> 00:11:18,280
The much bigger problem is could you imagine eating

147
00:11:19,080 --> 00:11:24,360
Ten meals a day for example. I don't know how many meals they they say is good at least four or six maybe

148
00:11:24,680 --> 00:11:29,080
You know eating throughout the day. Great. So it's good for your body. What are you a cow?

149
00:11:29,720 --> 00:11:30,920
I mean

150
00:11:31,480 --> 00:11:37,080
What is this how you want to live your life live to eat? You know that you know problem

151
00:11:37,880 --> 00:11:39,880
Problem is addiction

152
00:11:39,880 --> 00:11:46,280
If you eat many times a day who cares how good it is it's sort of this, you know, this stereotypical

153
00:11:47,080 --> 00:11:50,600
You know the cynic who looks at the health nut

154
00:11:52,120 --> 00:11:53,640
A person who

155
00:11:53,640 --> 00:11:58,040
Obsesses over their food and is all into you know everything has this this this this I mean

156
00:11:58,040 --> 00:12:00,040
I'm pretty particular personally. I knowing

157
00:12:00,840 --> 00:12:06,280
Not to use trans fatty acids and not to you know trying to stay away from fried foods

158
00:12:06,280 --> 00:12:12,680
You know red meat you know meat in general and try to stay away from too much sugar

159
00:12:13,320 --> 00:12:14,840
Etc etcetera

160
00:12:14,840 --> 00:12:19,480
Chemicals, you know not wanting to eat stuff if it's pumped full of chemicals. And this these I think

161
00:12:20,760 --> 00:12:22,760
I mean it feels kind of like a healthy

162
00:12:23,960 --> 00:12:25,640
You know sort of discrimination

163
00:12:25,960 --> 00:12:27,960
But people can go extreme and they live their lives

164
00:12:28,600 --> 00:12:31,720
You know about their food obsessing over their food and

165
00:12:31,720 --> 00:12:37,880
See some of these people and they have did their raw grains and nuts and it's it's just it's heaven for them

166
00:12:38,120 --> 00:12:41,560
You know this is an obsession. It feels good, but it's an addiction

167
00:12:42,680 --> 00:12:44,680
um

168
00:12:45,080 --> 00:12:47,160
It doesn't it's not necessarily that but

169
00:12:47,800 --> 00:12:49,800
Simply eating many times a day is

170
00:12:50,360 --> 00:12:53,640
Encouraging your addiction. So again, we're much more concerned with the mind

171
00:12:54,520 --> 00:12:57,160
You're going to die, but if you die full of

172
00:12:57,960 --> 00:12:59,960
lust and greed for food

173
00:12:59,960 --> 00:13:05,320
You might be born reborn as a cow or a pig pig. Look at how pigs eat to delete anything

174
00:13:06,840 --> 00:13:12,200
Because they're so caught up in food getting caught up in food very dangerous mentally spiritually

175
00:13:13,800 --> 00:13:14,840
Okay

176
00:13:14,840 --> 00:13:18,120
Number four sleeping at least at six hours a night now. That's the easiest

177
00:13:18,120 --> 00:13:24,120
Obviously that has much to do with the state of mind a person who is highly stressed out probably needs about eight hours of sleep

178
00:13:24,360 --> 00:13:25,400
you know

179
00:13:25,560 --> 00:13:27,560
maybe even more

180
00:13:27,560 --> 00:13:31,240
A person who isn't stressed out. I've got a meditator downstairs

181
00:13:31,960 --> 00:13:39,160
Who I told to sleep four hours a night. No, we have meditators who don't sleep who practice all day and all night

182
00:13:40,680 --> 00:13:47,640
There was a monk there's monks do this for months. You know, there's a story the first verse of the Dhamapada

183
00:13:49,240 --> 00:13:52,440
The background story is about a monk who did this for three months

184
00:13:52,440 --> 00:13:57,160
and

185
00:13:57,160 --> 00:14:01,640
Apropos he he became blind as a result of his practice. He destroyed his his eyes

186
00:14:01,800 --> 00:14:03,800
So he really hurt his body

187
00:14:04,440 --> 00:14:09,000
But at the same time he became an Arahad so they called they called him Jakupala

188
00:14:09,560 --> 00:14:12,280
Jakupu means I Bala is guardian

189
00:14:12,840 --> 00:14:14,840
He's one who guarded his eyes

190
00:14:15,720 --> 00:14:18,520
It's a it's a play on words because there's two kinds of

191
00:14:18,520 --> 00:14:22,840
Eyes the the eye of Dhamma and the the physical eye

192
00:14:23,640 --> 00:14:26,040
So he protected the important vision

193
00:14:26,520 --> 00:14:31,320
You know, he gained the vision that was most important and became blind physically

194
00:14:32,280 --> 00:14:36,840
He he he dispelled his spiritual blindness at the same moment

195
00:14:37,800 --> 00:14:39,800
where when he as

196
00:14:39,800 --> 00:14:41,800
losing his physical vision

197
00:14:43,160 --> 00:14:45,160
so

198
00:14:45,160 --> 00:14:55,800
Again two two parts to the answer one. It's certainly you know these kinds of studies are dealing with ordinary people who are doing intensive meditation

199
00:14:57,800 --> 00:15:00,440
but also not such a big deal to

200
00:15:01,720 --> 00:15:09,320
Neglect the body in favor of the mind to some extent. Of course you rely on the body. It's no good dying

201
00:15:11,080 --> 00:15:12,040
but

202
00:15:12,040 --> 00:15:15,960
Certainly better to die mindful than to live

203
00:15:17,640 --> 00:15:19,640
corrupt

204
00:15:20,360 --> 00:15:22,360
So thank you good question, but

205
00:15:22,360 --> 00:15:42,280
We'll have maybe it's a bit of a disagreement. So anyway

