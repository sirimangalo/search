1
00:00:00,000 --> 00:00:29,000
Reality is a difficult thing to understand.

2
00:00:29,000 --> 00:00:35,000
If you haven't ever trained your mind in just being present,

3
00:00:35,000 --> 00:00:43,000
it is difficult to tell the difference between reality and illusion.

4
00:00:43,000 --> 00:00:52,000
Go ahead and close your eyes and forget all of your beliefs and ideas.

5
00:00:52,000 --> 00:00:58,000
Forget science and culture and what your parents taught you.

6
00:00:58,000 --> 00:01:08,000
You will come to see after a short while that there are really only a maximum of six different kinds of phenomena.

7
00:01:08,000 --> 00:01:19,000
Seeing, hearing, smelling, tasting, feeling and thinking.

8
00:01:19,000 --> 00:01:27,000
This realization has made much easier by fixing the nature of each phenomenon that arises firmly in your mind.

9
00:01:27,000 --> 00:01:45,000
And affirming its essential reality with a clear thought as in seeing, hearing and so on.

10
00:01:45,000 --> 00:01:54,000
It is clear from this exercise that so many of our ideas about space, time and reality are just content

11
00:01:54,000 --> 00:01:59,000
or at best extrapolations of reality.

12
00:01:59,000 --> 00:02:02,000
Take time, for instance.

13
00:02:02,000 --> 00:02:05,000
In reality, there is just one moment.

14
00:02:05,000 --> 00:02:12,000
Neither the past nor the future exists outside of this one moment.

15
00:02:12,000 --> 00:02:15,000
And this moment is eternal.

16
00:02:15,000 --> 00:02:20,000
Whether we die or are born, these are just concepts.

17
00:02:20,000 --> 00:02:28,000
Like the word wave is just a concept used to describe a movement of part of the ocean.

18
00:02:28,000 --> 00:02:35,000
No matter how often the waves crash against the shore, it is still the same ocean.

19
00:02:35,000 --> 00:02:40,000
That is only to crash against the shore, really nothing has died.

20
00:02:40,000 --> 00:02:47,000
It is a physical process which is simply the dissolution of a collective structure of matter.

21
00:02:47,000 --> 00:03:01,000
In reality, there are still only six phenomena, just as the wave is still only water.

22
00:03:01,000 --> 00:03:11,000
And just as the waves come again and again, so too does the process we call death come to beings again and again.

23
00:03:11,000 --> 00:03:21,000
At that moment, the constructed reality that has built itself up since the time we were born comes crashing down in a single moment.

24
00:03:21,000 --> 00:03:32,000
The mind, however, seems to continue its search for happiness, building up a newly constructed reality according to its nature of that movement.

25
00:03:32,000 --> 00:03:39,000
The mind then follows after new experiences as they arise just as it always has.

26
00:03:39,000 --> 00:03:46,000
The experiences change, of course, but they are still the same six phenomena.

27
00:03:46,000 --> 00:03:54,000
The functioning of the six senses in the present moment never ceases for one who still seeks after more pleasurable phenomena.

28
00:03:54,000 --> 00:04:03,000
Since there will always be more and more building up of constructed reality, just as the ocean produces waves again and again.

29
00:04:03,000 --> 00:04:14,000
Death is just the end of one wave and the beginning of another.

30
00:04:14,000 --> 00:04:21,000
The problem arises from when we look at ourselves from without instead of from within.

31
00:04:21,000 --> 00:04:25,000
From without, we see only the body in its fully formed state.

32
00:04:25,000 --> 00:04:32,000
We cannot see the mind, and so we believe it to be only a product of the normal functioning of the body.

33
00:04:32,000 --> 00:04:44,000
From within, we can see that the body is merely an extension of the experiences of the mind as they coalesce to give us the false impression of itself.

34
00:04:44,000 --> 00:04:50,000
From without, it is easy to believe that when the body dies, so too does the mind.

35
00:04:50,000 --> 00:04:54,000
From within, it makes no sense at all.

36
00:04:54,000 --> 00:05:00,000
Just as it makes no sense that the crashing of the wave puts an end to the great ocean.

37
00:05:00,000 --> 00:05:05,000
Death is an illusion just as is the wave.

38
00:05:05,000 --> 00:05:11,000
For one who understands reality, it is not proper to say that one believes in rebirth.

39
00:05:11,000 --> 00:05:19,000
One should better say simply that one does not believe in death.

40
00:05:19,000 --> 00:05:26,000
The attainment of freedom from birth and death comes from the simple realization that this world

41
00:05:26,000 --> 00:05:35,000
is made up entirely of an endless process of arising and ceasing phenomena that is completely without purpose.

42
00:05:35,000 --> 00:05:44,000
Leading only to more misery for one who clings there too, or thirst for happiness therein.

43
00:05:44,000 --> 00:05:50,000
Such a realization as this leads to final freedom from the process of birth and death.

44
00:05:50,000 --> 00:05:57,000
As no more craving means no more building up and seeking out new phenomena.

45
00:05:57,000 --> 00:06:07,000
At the moment of death for one who no longer clings to birth in life, the waves cease once and for all.

46
00:06:07,000 --> 00:06:16,000
Or in the case of one who merely craves and clings less through training of the mind to a limited degree,

47
00:06:16,000 --> 00:06:27,000
there still may be waves, but they are quieter, calmer, free from the storm skies and blowing winds of some sigh.

48
00:06:27,000 --> 00:06:35,000
Reality is all around us. It is everything we take for granted and for the most part ignore.

49
00:06:35,000 --> 00:06:43,000
When we come to see this reality that exists so clearly around us, our minds become clear, calm,

50
00:06:43,000 --> 00:06:48,000
and clean of all imperfections and addictions.

51
00:06:48,000 --> 00:07:15,000
When we come to see reality, the truth will indeed set us free.

52
00:07:18,000 --> 00:07:22,000
Thank you.

