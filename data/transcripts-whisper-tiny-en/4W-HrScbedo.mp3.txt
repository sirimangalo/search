Good evening everyone, welcome to our live broadcast.
Today we're looking at the Buddha's teaching on beings for few and beings who are
in numerous types of beings that are common in the types of beings that are rare.
The number is if you're following along, it's number 333 in Book of Wands.
It points very much to the idea of the Buddha's teaching being gradual and not gradual
in the sense of plowing along, doing the same thing, but gradual and gradiated in the sense
of going in stages.
In many ways we're looking at the Buddha's teaching as a series of stages, but it means
is the practice that you begin with, it's not the same in nature, it's the practice that
you end with.
The practice at each level is going to be subtly different, and then this is true in many
different ways, for example in meditation, you will again and again solve a current issue
that you're dealing with, whether it be one of the hindrances or some experience that you
can't bear with, whether you can't understand, if you're unable to figure out, and then
you get it, and you come upon the means of interacting with your experience that frees
you from the suffering, and it's easy to become complacent, and easily then move into
a state of complacency, thinking that you figured out the problem, and so you behave,
or you enact the same behavior consistently, and then practice as though the problem
is still there, and so the next problems are the same, and you find that the solution
or whatever it was that you did to fix your problem, to cultivate a smooth meditation
practice is no longer working, because the situation is changed, and your mind is ever shifting
and reality is ever shifting, doesn't just mean that the objects are changing, but
the very foundation of reality changes, and it changes as you change, so as you progress,
as you become what one might call more spiritually advanced, the challenges change, the
only thing you can really count on is you're always going to be challenged, which means
that when you become proficient, when you overcome a challenge, the best thing is to expect
for the next challenge, a new challenge, rather than rejoice at having figured it out,
there's no figuring it out, because it is ever changing, and not even in the same way,
so it changes in new ways, and becomes in a way more challenging, as you get deeper, the
deeper, and more subtle, it becomes the more core issues, more core are the issues that
you deal with, more substantial and deeper rooted, and another way of looking at the
gradiation is in terms of the training, so you start with morality, you go into concentration,
and then even in wisdom, in the training in wisdom, there are many layers to the wisdom.
Now we have these 16 stages of knowledge that one goes through, and they're not the same,
so during the meditation practice you'll see you go through clear and distinct stages,
of course they're going to be, and it won't be exactly clear cut, but there will be clear
cut distinctions where your state of mind one day to the next is not the same, the game
that you're playing is not the same from day to day, the very rules, the very foundation
of what you're doing, so my mindfulness is so important, it's so important to have something
that grounds you, because there's nothing that will ground you, the experience won't
ground you, even your own state of mind, so if you don't have a way to relate objectively
and be flexible, that's what comes, easily take you off guard, so this chapter is one
way of looking at, but it's teaching in a graded sort of way, in terms of there being
few people, so he starts off, he gives a successive list of successive distinction between
those people who are many and those that are few, and what this is really I suppose
designed for is to help us understand how rare is the opportunity that we have, because
you know forget about coming to practice meditation or even listen to the dhamma, but it
starts by saying, not even being born human, forget about even being born human, the number
of beings he says that are born in water, the number of beings that are in the, and I suppose
the meaning here is in a state where there's not much potential for spiritual practice,
it's not so much the water as it is the state, so if you think about fish or you think
about crustaceans, squids, all these beings, even insects, insects, many insects born
in water mosquitoes, think of these types of beings, there's far more of them, and that
means something because the idea is that all of those doors are still open to us, as long
as we cling to some saran, as long as we have ambition and desires, the potential to fall
so low is to be born a mosquito larva, and upon somewhere, far more likely than to be
born even on dry land, and far more likely even to be born wherever than to be born as a human,
of course, are fewer humans than there are other beings than humans.
You're on earth, but even among human beings he says, hard to find a human being who is
born in a place where they can hear the Buddha's teaching, he talks about the middle
provinces and the jimapadesa, just a place where Buddhism was flourished and while the Buddha
was still alive, how many people, simply by virtue, probably all of us, simply by virtue
of being born in the wrong place, never had a chance to meet the Buddha, never had a chance
to meet one of his great disciples.
Even today, how many people, just by virtue of their status and life, their position,
their place, unable to come in contact with spiritual teachings with the Buddha's teaching,
of course, nowadays we're quite lucky to have, let's say we're lucky to have the internet
and to have such vast amounts of, vast potential for knowledge and dissemination of
teaching, but this is very rare, even if you consider that now it's very common, this
isn't going to last, this is only been what, 20 years, 30 years, 20 years, and certainly
not going to last long into the future, so we have this brief window where now, all across
the globe, we have the potential to learn about the Buddha's teaching, to learn about
anything, including good things and bad things, we have this door open to us.
And so then he says even more than that well, but even still, even for those who have
the opportunity, few are those who are wise, intelligent, astute, able to understand the
meaning of what has been well stated and badly stated, more numerous are those who are
unwise, stupid, too unable to understand the meaning of what has been well stated and badly
stated, no sugar coating it there, most of us are just too stupid to understand the
number.
I guess that's something you don't want to be most full about, but there is a sense
that all of us here are somewhat blessed with wisdom, you know, that's sort of to our own
horn and remark on how lucky we are to have the inclination, I mean, because objectively
even though you don't want to be proud, objectively, there are many people out there,
many, many who would look at it, the idea of finding happiness in other than sensual
pleasure or indulgence, the idea that you could somehow just be happy rather than having
to seek it out in things that are impermanent, you would find that incomprehensible, you
would be unable to fathom it, and when deride it and ridicule the practice of meditation,
very hard to find people who even find meditation interesting or like a positive, positive
practice, and get that it's more than simply blessing out or finding an escape to reality.
It says that the things that have already sort of gone through to hear that to see the Buddha,
to hear the dhamma, rarer the people who get to see the Buddha and hear the dhamma,
but even those who hear the dhamma rarer, those who actually keep it in mind, far more
of those who hear the dhamma, but don't keep it in mind, this is true, you can give talk
after talk, but it's much more common for us to forget the teachings and it's much more
common for far more people who listen to your talks, listen to the teachings of the
Buddha, and then put them aside and live their lives as they never heard such profound
and wonderful teachings, and here's perhaps the most well-known one that I quote often says,
few are those who, few are those who acquire a sense of urgency about things that are
worthy of inspiring urgency, somewhat jinni ye sutani sosangri jambi, in things that are appropriate
to be, that our basis of urgency, we're going to get sick, we're going to die, greed, anger
and delusion or cause for suffering, the power, the mind has power, these are things that
should stir us to action, very few people are stirred by these things, Mahasi Sayyad
of lichens us to ducks and chickens, you look at these chicken farms, the chickens cluck and
ducks cluck, and they live as though their lives were long and blessed with happiness,
they fight and they play and they, and they're as though they're in heaven, and they don't
have a sink of the fact that they're going to be slaughtered, they don't realize they're
not aware of their fate, humans, we have very much the same, in fact humans are even worse
because we know we're going to be slaughtered, we understand that death is coming, we ignore
it, we avoid it, we shun the concept of death, anyone who talks about death is considered
to be morbid, considered to be depressing, whoa, carpeting them, they seize the day, what
we really mean is forget about the truth, ignore the reality of our situation, pretend
that it's all going to work out, happily ever after, it's funny we have all these happily
ever after stories, and there is no happily ever after, and it's just all the empty sickness
and death, they don't have to be depressing things, but there are things that should inspire
urgency because you die in the wrong way, if you die unprepared, you die with a bad mind,
you even die with a confused mind, which is very common now, right, we're putting hospitals
and giving medication that confuses us, and we die in a muddled state, it's very common,
we've lost our sense of urgency about these things, of course we've lost our understanding
that there's anything to be urgent, to be distressed about it all, we don't have a sense
even that when we die there's anything after, you know, to the sense even that our minds
are efficacious, the sense that everything is random, a common thing to, you know, luck of the
draw, and our mind has no bearing, it's idea that I'm not even an idea, but we don't
realize, we don't get into it, we aren't awake to the fact that our minds are so important,
that there's nothing worse than an untrained mind, something better in the universe than
a trained mind, but then he says, what is most interesting and clever is that even among
those who are inspired with a sense of urgency, very few of those people actually do something
about it, actually, you only so badahati, you only so badahati, strive carefully wisely,
do something about it, how many people think it's a good thing to meditate, how many
Buddhists, how many Buddhists in the world, or non-buddists, I'm always approached by people
here in Canada telling me how much they think, how great they think Buddhism is, and how
admirable they think it is, and there's a sense that they would never themselves actually
undertake to practice it, it really is a sense that what they're saying is that, wow,
you know, good on you for doing something useful, I mean, perhaps there's the idea that
there are many different paths, and they look at Buddhism like a flower, something to be
admired, but nobody wants to be a flower, it's just something beautiful, so they don't
yet have the sense of urgency, but among Buddhists there is this sense that there are things
that need to be done, there's a sense even of fear of the bad effects of karma, and yet it's
so easy to make excuses, not to do something about it, to not ever actually get around to
to, bettering ourselves, striving for that which is what is striving for, all right,
so I'm going to stop it there. The book of One's is probably not all that, probably
doesn't feel like it's all that deep in terms of elucidating the teachings, but because it's just
One's, right, once we get into lists of three's and four's and five, we'll see there's far
more depth and meat to it, this is more pithy saying, it's much more quotable, I would say,
these were the one, this, the one that, that last one was quite quotable, if you get the
wording right, fewer those who are stirred by, fewer those who are stirred by things that are
stirring, far more of those who are not stirred, fewer those who stirred by things that are
stirring actually undertake to do something about it, find some poetic way of saying that,
all right, so move on to questions, I'm Robin's here, so keep on today, good evening Robin,
so we have questions, sorry I lost it,
but I would like to know if your meditation center accepts male devotees from Australia to
reside at the new center for short term, like three months to learn to meditate,
Dharma, and helping out at the center with daily chores, absolutely, that'd be great,
it'd be great to have someone come and stay, and we can't have probably more than one or two
such people, but we could use someone who would be willing, now the thing is,
A, you have to be practicing our teaching, our techniques, we have to be clear that this isn't
an open center, we're all practicing in one condition, two, you have to do a full month of intensive
meditation before you actually, I mean actually meditators here now because we don't have a
stewardard, are doing a lot of the things, household things on their own, so you would be doing
some things around us, but there would be many things that we wouldn't want you to do
until you'd done a full month of practice on your own, but certainly apply for a course and come
and practice, let us know that you want to stay on longer and we'll try to accommodate.
If you have a chronic physical condition that affects your quality of life,
and if you get drunk and listen to music that inflames your temperament in an empowering and inspiring
way, and as you're experiencing the rest of the energy, you turn your gaze upward to the sky,
and you stick a gun in your mouth and kill yourself, what happens to your soul or karma or
consciousness, do you go to hell? It's got to be the most bizarre question I've ever received.
I think you get no word for that one. Well it's very specific. Yeah, I mean it was that
I did not see that coming at the end. Suddenly, why would you suddenly stick a gun in your mouth
and kill yourself? Are you suggesting like maybe a better description would be you're in the state
of inspiration as you say and then suddenly you die or you get an accident, which is actually far
more likely that you're in the state of someone might call goodness or some sort of greatness,
and then suddenly is struck by lightning or in a car crash or something.
I mean there's a couple of, if that's what you're asking, then there's a couple of points that I
could make. The first one is, oh I see, but there's more to it here, right? So your life is
really terrible, and then you do something to remove yourself from that situation,
sort of distract you from that situation, and then you kill yourself while you're distracted,
thinking that your mind's going to be in a good way. I get it. It's like trying to
trying to weasel your way out of the law of karma really.
You can't outrace the mind, and you can't escape the mind.
Your intention to kill yourself, sorry I misunderstood that first, but your intention to kill yourself
dooms you. You can't escape that because your reason for killing yourself is an inability to accept
reality, your reality, your experience. There's nothing you can do to mitigate that. Maybe mitigate
yes, but you can't escape that. And by mitigate, I mean if you were to do some meditation and then
kill yourself rather than just killing yourself first, then yes, it's arguable that your mind would
be in a better position. You'd kill yourself in a more peaceful manner. You'd be more peaceful about
it. It doesn't fix the problem. It doesn't get you out of the fact that you're making a conscious
decision to run away from your problems. There's never ends well, running away from your problems
with never a good thing. And even more so, the other point I wanted to make is that getting drunk
and listening to music is not going to cultivate a state that Buddhism would consider to be
in any way wholesome. So the state of inspiration you're talking about is a state of intoxication
more than just the alcohol. The state of intoxication with really just pleasure and bliss and
energy, the adrenaline high or the chemical high that you get from the exhilaration of liking,
enjoying life. It's an intoxication and the best that intoxication needs you to be born again
as a human. But in this case, when you're drunk and coming off of a depression and aversion to the
pain, when you kill yourself, you put the gun in your mouth and kill yourself, the mind that reacts
to that violent state that is suddenly quite painful even for just a moment and disturbing.
If they couldn't handle the chronic physical condition, the pain that was
not life-threatening, how could they possibly be expected to handle the bullet going through their
head and react in the positive manner? They're certainly not going to react to it thinking,
wow, this is inspiring and empowering. They're going to be horrified by the results,
by the resulting loss of power and ability that comes from it, the confusion of no longer having
the use of one's physical faculties. There's a lot more to your question. The solution you're
providing is far from, it's not really at all viable, it's the best way to put it.
And by the way, if you're really interested, I would recommend you try to
read my booklet and meditate with this. It looks like you haven't done any meditation on here.
I don't know if you're doing your own meditation, but when you might, you can do a practice,
you might get a better understanding of how the mind works so for me. I think, and I'm
not mean to judge, but it does make, there's sort of a sentence that if you were to look closely
and see that when getting drunk and high on life, it's not going to mitigate the actions that
you perform.
Thank you, Monty. I miss your talks on the Dhamapada. Can you continue teaching this? I feel very
inspired. Yeah, sure. Not this text. These ones are not inspiring. I think he's saying he feels
very inspired by your talks on, maybe it does say not this text. I thought it was on those texts.
No, this text, we have to clarify there. Do you think this, these texts are not inspiring,
these talks I'm doing every day are boring, dull, uninteresting, or do you just, are you solely
referring to the Dhamapada? It's not all that interesting to me because there are just stories,
but people enjoy them and it's good entertainment, but it's the hard teachings that we need to
do. It's medicine, not candy, but I know we've had this talk. I've had this talk with many
different people and everyone argues how useful and how helpful the Dhamapada is.
I think it is too. I think the story is really stick with you. And they provide a good,
just a good frame of reference for remembering the teachings, I think.
How can I learn? How can I learn about the more advanced meditation technique without going to a
monastery? Well, you can do one of our online courses. That's what that's for. I've been teaching
people online. So there's a meat, you see the meat, the word up at the top that says meat,
you can click on that and figure out how to sign up for a slot and then how to call me and
do a video conference. That's one way.
Yesterday I had a conversation with a friend and he told me something that I couldn't digest.
I felt all the defilements taking over me. I tried letting go of feelings by acknowledging
and controlling my breath, but it was a very difficult task. Any advice on how to deal with such
a heavy load of emotions, acting out all at the time in a moment?
Well, I'm assuming you've read my booklet on how to meditate. If you haven't, that's a big place to start,
but it is quite common to lose sight when it's actually being of the actual meditation technique,
what we're actually trying to do. We're not trying to control the breath. We're not trying to let
go of feelings, but you say by acknowledging, don't worry about letting go of them. Don't worry
about controlling your breath. Controlling your breath is not at all useful. There's no solution.
You can't solve things. You react and that's the way the mind is going. You can't stop that.
The only way you can be free from that is if you over time learn that that's not the proper way
to react. Not just intellectually. You don't want to react that way, but you have to teach yourself
through observation, repeated observation, and intensive and continuous mindfulness.
You're eventually able to really get on a visceral and deep level that those defilements are not
properly to deal with things. There's the only way you can't just turn them off and let them go.
Let them go eventually once you see, and that's why meditation is so important. This is something
that should probably be an impetus to stir you to practice more strenuously because you can see
the danger. You've got like a tank full of gasoline and all you're waiting for is a match,
and when the match comes you explode.
Is taking a human birth the result of merit that was earned in a previous life or lives?
I mean, yes, yes, it's a sense. There's a sense that the only way you can be born as a human is if
you've got a wholesome mind, you've got a mind that is well composed.
The only way you can get to the top of Mount Everest, not that being a human is like that,
but the only way you can get to the top of a mountain is by being physically strong.
In the same way if you see someone born a human, you have to know that the only way they could get
there. There's only the only way possible to get there because they had to do it on their own
the only way to get there is through wholesome mind states.
What is merit exactly? From what I can tell, it is a Mahayana concept.
Is it just another word for good karma, or is it different?
My merit is a translation, fairly poor translation of the word Bunya.
Bunya means goodness, or...
Yeah, goodness is probably the best translation.
Literally, I don't know what the etymology is, but the etymology they give us is
that which purifies you. Anything that purifies you is goodness is Bunya.
It's not Mahayama. Is it actually real? It's refers to good karma.
It just means anything that is based on the wholesome state of mind, any actor, speech,
or even thought. It's based on the wholesome state of mind that's Bunya.
The opposite is Baba. Baba means evil.
But they translate it as merit. It's a pretty awful translation, really.
How did it get over the past life? The past life of friends no matter how dark it was.
Can we consider them as bad friends because of their wrongdoings in the past?
Yes. Yes, you should be very careful about such people.
You should try not to judge them for sure. Of course, be aware of your own aversion and anger
and frustration with them, sadness about them. But at the same time, you should be careful not
to associate too closely with such people. I don't think you necessarily have to completely
get over it. You can forgive, but you should never forget it. I don't think,
yeah, sure you can, but it's in your best interest to not forget,
I would say, because you just pretend, you know, and what I forget means doesn't actually forget.
I mean, you know, treat them as though it never happened. I don't think that's really
proper because it did happen, and it's a sign of something, unless it's clear that they've changed,
chances are they're going to do the same thing again. You don't want to be
an enabler, and be someone who allows people to make the same mistakes again and again.
So, and you certainly don't want it to affect your own practice and keep you from purifying
your own mind.
I'm reading chapter five of your second book, and the path to insight is a difficult one.
To meditators get more peace gradually or until the final stages.
I wonder if that sort of means that chapter five is somewhat depressing. It may be,
it may be that I have to go through it and make it clear that there's not much you can do really,
because I mean, as you I'm sure are aware, there is a lot of deep dark stuff that you have to go
through, and a lot of the stages of knowledge are dealing with that. I mean, it's like when
you're cleaning your house, the best part of cleaning your house is the most dirty part, the most
disgusting part, the most revolting and terrible part, because that's when you're really doing
the cleaning, that's the most important part. If you were to avoid that and you're going to
clean your bathroom, say, and you say, oh, that part's too dirty, I'm just going to avoid it,
that part's too disgusting. Let's just do the parts that are pleasing, right?
Let's just clean those parts of our toilet that are already clean.
It wouldn't amount to much, right? It wouldn't accomplish much, certainly when it accomplished the
task. So, I mean, it's partial answer to your question that, yeah, a lot of the piece that
we're talking, but it's really only going to come at the end. Fortunately, that is the end
game that's where we're headed. When you get near the end of the course, not even having
finished the course, but as you get to the end of the course, everything starts to clear up
almost miraculously, and suddenly you're no longer reacting to things. Fairly, suddenly, I would
say it's like stumbling through the forest and then suddenly you break through to a clearing
and the whole game changes, suddenly you're mindful, suddenly you're objective.
It's not the end of the course, but that's what it means is you've brought everything together,
and you've come to gradually, to the final realization that nothing's worth clinging to,
and so you're at this state where you're getting it. And at that point, it's only a matter of time
for it to coalesce and for there to be enough power to that realization to make the switch and to
suddenly drop your mind out of some sara, enter into nibana.
So there are two examples of peace, and it's probably what chapter 6 is going to have to talk
about. The first one is, I was talking about it in chapter 5, so I'm going to pick a nyanana,
where the mind is equanimous towards all formations, and then there's clarity in peace.
It's actually quite comfortable, but the second one is the true peace of freedom. I have to
experience nibana, then there's the 16th stage of now, the 16th stage of now, the 16th stage of now,
just the best. Well, it's the one that it's easy to, easiest to, most memorable as being
pleasant, because at that point there's just peace, and you're just walking on walking on
walking on clouds. You're on cloud nine. You're floating for sometimes days.
Everything is at peace. There's no suffering. Well, there's the sense of great peace, profound peace.
So yeah, a lot of the true peace really only comes after the course, unfortunately,
but again, it's because, you know, we keep challenging. The course is designed to push you.
You get good at it. Let's push you a little further. Push you and push you and push you until
finally you. I guess you could say snap, but it's not like that. You don't get more wound up.
You just become more and more refined. So you've worked out these problems,
so let's look at deeper ones. When you look at more and more deeper problems, until finally
solves the problem.
Ponte, what is your advice on dealing with poor posture while meditating, sitting meditation?
I find myself slouching when meditating is this common for meditators. When I try to correct
the back posture to an upright, straight position, it feels like I'm trying to force it. I get a
lot of tension and stiffness when I do, which I know, but it would be better to focus more,
but would it be better to focus more on the worrying about the posture than trying to correct it?
Yeah, it would be better to focus on the worrying. I mean, if you're not working or doing anything,
strenuous or stressful, it's quite easy. It's much easier to have good posture on your meditating.
There's much less weighing you down, and your body is in a more relaxed and
untaxed state. You have the strength, the energy to stay upright, but if you've been working
and if you've been taxing your body all day, certainly then wouldn't worry about it,
but even during a meditation course. At least if you're just coming out of a life that is stressful,
you're going to have a lot of stress during the course, and so it's going to take its toll on your
body as well. It's not really... They talk a lot about good posture and meditation, but that's
for the type of meditation where you can transcend that summit to meditation. If you practice
summit to meditation, you can transcend the body because you put yourself in a single
focused state. Your body can cultivate the strength and the mind cultivates the strength
to sit perfectly upright without moving. In insight meditation, it's not at all like that.
You don't have that. Instead, you're focusing on the ups and downs, the vicissitudes, the changes,
and so unless you're practicing for days and weeks on end, on end, practicing course,
after course, it's not really likely that you're going to get good posture in this technique,
so I wouldn't worry about it. In fact, yes, focus more on the worry. It can slouch like this,
your back won't hurt your back as soon as so. My teachers, when he's now 90-some years old,
and he always sits out, almost always. Sometimes he sits out, but he works really hard and
teaches a lot on days, so when he's very old, right? When his posture is played.
As we evolve spiritually, our life challenge is always in sync with our abilities to deal with
them or our challenges are betraying, just happening.
That's a good question. I don't know that there's any mechanism by which the challenges we face
are proportionate to our ability to deal with them, but you might be on to something in the
sentence that our perception of reality is going to determine the challenges.
We won't even be aware or we won't even ever deal with. It's not exactly true because, of course,
you can be unprepared. We're unprepared. We're unprepared for so many of our challenges in life.
I guess the quick answer is, no, obviously. I don't think it's really very much
in doubt. Could you say that the people who suffered through the tsunami and died in that tsunami
when was it? When was it 14,000?
7, when was that? When was the big tsunami in Asia? All the people who died and all the
horror stories that were heard of the being in Asia. I was in Thailand at the time.
None of them were, that was proportionate to people's ability to deal with it.
The Khmer Rouge, the Holocaust, all these things totally out of proportioned, and that's only
the gross examples. The countless number of people have challenges that they are unable to deal with
and kill themselves. I wanted to say that there probably is something going on there in the
sense that we tend to focus on take meditation course, for example. You won't even notice
the more subtle issues because you're still dealing with the course ones. So you only deal with
what you can. The challenges in spiritual practice, the challenges we face spiritually
are probably going to be very much in line with what we can handle. So say someone who's
in a tragic situation when they approach it and try to solve it, they're only going to try to solve
the, that which they can comprehend, right? That goes, I think, for meditations that we do focus
on problems and the issues that were best, that were, that resonates resonate with us.
Yeah, the question reminds me of a kind of a Christian thing that Christians say that God won't
give you any more than you can handle something along those lines.
But I thought, so do you mean to you a little bit, that your rebels argument?
Yeah, silly Christians. I have heard that Lausu from the
Tao Tejing may have a, may have been a Pacheka Buddha. Can you explain the difference between a
Pacheka Buddha and in our hands? Well, they're all our hearts, but we usually use the word
our hands. We mostly use it to talk about people who are unable to realize the teachings,
the truth for themselves. Pacheka Buddha is able to realize the truth for themselves. That's
really the biggest difference. That's considered to be a much more difficult thing, so
most people would never have the opportunity to ever become a Pacheka Buddha, but some beings are
so well practiced and well learned in their
wives, let's say, that they're in their, in their, the course of their journey and some,
through some sorrow, that they're able to realize the teachings without any help.
That doesn't mean they're able to understand them fully. Like, take an Arahan, for example,
or a Sota Pandas, so on. They can experience Niamana and not really be able to describe it or
explain it or even explain how they got there. A Pacheka Buddha is kind of like that, even
though they're able to realize it for themselves. Their ability to explain what they realize
is, is, is deficient. So there's, there is a type of being a Buddha that is able to explain
perfectly what they've been through. And, and much more than that, it's considered to be able to
explain pretty much anything. But a Pacheka Buddha is not able to do that, but they are such a
position that they, they just, without any instruction, go off into the forest and become a
enlightened, or even living their lives, they become like, not probably likely as, as an ordinary
person, but much more likely they would go off as an ascetic and not realize the teachings
in the forest or something. And then just fade away into Niamana.
I have read your booklet. Is there any way you can do a demonstration of meditation for us to see?
Well, there's videos. In fact, I did, which means you probably haven't looked through my YouTube
channel. There's a playlist on how to meditate. Maybe someone could even link that here.
But they are Punea and Kusula, the same thing.
Um, well, they're, they're used in two different ways. You could argue that, yes, they are, but
Kusula is an adjective. Punea is a noun. So Punea is a thing. It's a concept. It's a category of
things. Certain things are Punea, which may have been certain acts. So Punea would generally
refer to Kusula karma, refer to the karma rather than Kusula. Kusula is a description of something.
So something can be Kusula.
Punea is, when you do a good deed, that's Punea. And it's Punea because
that thing has the quality of being Kusula. Right? So if you have a thought,
that's a mind state that is associated with Kusula Jetana. Jetana, Kusula Jetana.
Then it's Punea. If you have a, if you speak in the speech is associated or stems from Kusula
karma, Kusula Jetana. Then it's Punea. And if you act as well. But the act, the speech, the thought,
those are generally understood to be Punea. You don't talk about a Punea Jetana, Jetasika,
something like that. Punea is a thing. It's a more conceptual thing. It's more of a
suta teaching, whereas Kusula is more of a, a bit armamenting, a bit armamenting.
Sometimes I experience a negative feeling that is difficult to discern what exactly it is.
Is it necessary to distinguish what it is or is awareness of the experience enough?
Well, not awareness. We would actually want to say feeling, feeling, use the word to remind yourself.
It is what it is. But if it's negative, there's a disliking of something. That's what we mean
by negative. So I would want you to be a little more specific, at least in terms of saying
disliking, disliking, angry, angry. Angry, even though it may not. No, we use anger to describe
all the negative states. So it does work, and they're disliking or angry.
I think you caught up on all the questions one day.
Alrighty, then, stop there. Thank you everyone. It's great to see our community active.
Thanks, Robin, for joining me. Have a good night, everyone.
Thank you, Dante. Good night.
