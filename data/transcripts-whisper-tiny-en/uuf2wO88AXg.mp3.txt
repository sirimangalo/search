While not meditating, should life itself be a continuous meditation, like driving to work,
saying driving, or getting fired, saying angry, angry, etc.?
Yeah, absolutely.
The reason why we can't is the same reason why we're still in a life where we drive cars and get fired from jobs.
A person who is able to do that, where they're mindful all the time, wouldn't have a job, wouldn't be driving a car most likely.
So, in a sense, the question is another one of these conflating two issues, you know, trying to apply one thing that should be applied another thing.
So, you think, wow, I could never imagine being mindful all the time during my life.
And you literally couldn't, because the reason why you're not able to be mindful all the time is you're the greed, anger, and delusion.
That same greed, anger, and delusion is also the reason why you're still in a life that is caught up in the world.
Once you get free from your attachments and your confusions and delusions and views and all that, you'll give up that life.
And so, life becomes quite simple, life becomes about right here and now, now I'm sitting.
I'm sitting, it's easy, now I'm walking, maybe now I'm talking, eating.
And so, it's actually not that complicated, certainly doable.
For, in the meantime, you're just trying to get yourself in that state, you know, bring the meditative state to your life.
Your life is over here with all the craziness, the meditative state is over here.
You're trying to bring them together, and sometimes you're able to bring them together and then they go apart.
It's a matter of training, but eventually, your life is a meditation.
