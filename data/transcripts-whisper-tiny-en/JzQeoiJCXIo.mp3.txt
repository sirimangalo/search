Hey, welcome back to our study of the Dhamupana.
Today, we continue on with verses number 9 and 10, which read,
Okay, and the translation is Anika Saaboka Saabung,
for a person who hasn't removed the colors from themselves.
Yo Watan, Parydi Yisidi, Yo Ka Saabung, Watan, Parydi Yisidi,
the order is poetic.
But actually, the meaning is one who without removing the color or the stain
from one's mind, dawns the stain or the colored robe as their cloth.
Appeto Dhamasati, now with one who is without patience and truth or truth.
Such a person, so Ka Saabung, Latiti, such a person is not worthy of the colored robe,
meaning the monks robe.
Yo Cha Wantaka Saabasa, for a person who has dispelled or gone or removed the stains
from their mind.
Silesu Sussamahito, who is well-balanced in their morality.
Upeto Dhamasati, who is endowed with patience or training and truth.
Silesu Silesamahito, such a person is indeed worthy of the colored robe.
So, this is a story about the different types of monks.
A person who is a monk who is worthy of wearing the monk robe and a monk who is not worthy.
Now, there's a play on words here, the word colored or Ka Saabu, which can mean stained
or the dye, which is used to color the robe, but it's used also to mean the stains that exist in the mind.
A person who has stains isn't worthy of the stained robe.
A person who is stainless is worthy of the stained robe.
This is the play on words.
Okay, and this was told in regards to a story about Deva Datta.
Now, Deva Datta, there are going to be many stories about him.
He's another one of the Buddhist cousins, and he plays a very prominent role in Buddhist legend or Buddhist history, however you want to look at it.
In that he tried several times to kill the Buddha in various ways, according to the stories.
And we'll come to them, hopefully eventually.
He also was responsible for a schism in the sangha in the Buddhist community.
He divided the monks based on hopefully something that will come to as well.
But this story specifically relates to during this time when he was causing a whole lot of trouble and really being a general nuisance.
And was very arrogant and self elevating and thought a lot of himself.
And it was acting very poor, as a very poor specimen of a monk, having tried to kill the Buddha and so on.
That it so happened that there was a very large gift giving.
And one of the things that Deva Datta was guilty of was corrupting his lay supporters.
So the people who supported him, he had ways of giving them passing on wrong views to them.
And he was actually able to convince a prince.
And the prince of Rajika had to kill his father, King Vimbisara.
But there are many examples of this.
He was in general guilty of that and guilty of encouraging wrong views in his followers and wrong understanding.
And as a result, pitting them against each other and against the other monks.
So he would do things to ingratiate himself to the lay people.
He would perform services and act in general a lot like a priest.
And I'll talk a little bit more about that.
He would do ceremonies and basically following after people's desires and wishes.
So when monks came who were just going to teach meditation, people were like,
well, we're looking for someone to bless our crops or to bless our children or to find us a way so we can make lots of money or something.
And that's what Deva Datta does.
So what good are you?
And they wouldn't even listen or follow after them.
They had so much defilement and stain in their mind because of their encouragement.
At the hands of Deva Datta and other corrupt monks that they weren't willing to listen to those monks who taught meditation.
And so at this given giving, it happened that there was a very expensive robe and they were trying to decide whether to give it to the Buddha's chief disciples,
Sariputa, who will hear about in the next story, or whether they should give it to another monk.
And some people say, well, let's give it to Deva Datta because Sariputa, he comes and goes.
It seems like he's here when there's food and he's gone when there's not.
And he doesn't come to visit us. He doesn't come to see us other times.
But Deva Datta is always there.
And they say, just like a water pot.
It's always there when you need it.
Because indeed he was very in very close relationship with the lay people, which is considered very improper, especially in the way that he was engaging with them.
But so they decided by a very slim majority that they would give it to Deva Datta.
Because there was a lot of misunderstanding about what a monk, what a proper monk is like.
And so they gave it to this monk who was an attempt at murder on the Buddha and created this gism.
And it was in general a very poor specimen of a monk.
So the monk started spreading this around amongst each other, like, how on earth could Deva Datta have gotten this great robe?
And they brought it back to the Buddha and asked the Buddha, isn't this really crazy that he should be able to get this monk?
I mean, isn't there such a thing as justice in the world where you can, how is this possible?
And the Buddha told a story of the past. He said, well, this is just like Deva Datta.
He's that sort of person who tricks people and is able to get his way, simply because he's wearing this robe and he's able to abuse the position.
Because actually, the truth is he has some good karma or he's done some good in terms of his meditation practice or in terms of his renunciation.
But then he's abused the position.
He came a monk and there's a great power and a great responsibility that comes with this.
So he took the power and threw his hand in responsibility as a result.
He became a great leader in his community and people followed him and he abused their trust in many ways.
And including receiving these fine gifts and encouraging people to give him these fine gifts and becoming very attached and very proud about them.
But so the story of the Buddha told was that the time in whenever Iyans and Iyans ago, when Deva Datta was a hunter and he would hunt elephants.
And he found it very difficult, of course, to hunt elephants after a while.
They became suspicious of all humans and they would be very careful when they saw humans coming because they knew that humans were out to kill them for their tasks and so on.
And you can even see that in Sri Lankan, the elephants can be very suspicious of humans because they have suffered a lot at the hands of humans in the past.
But they had one exception and this hunter found out.
He watched them and he found out that they had one exception among them and that is the monks who were wearing these rags.
They were the people who were in these rags who were ascetics. So anyone who was wearing these ragged cloth who had gone off into the forest or practice meditation and had left the evils and the corruptions of society behind, they would honor and revere such a person so when they saw them they would bend down and pay respect to them.
And just being general, very receptive towards them.
I've heard that this also happens here in Sri Lankan although I don't think it happens for the majority of cases, I would say.
In the majority of cases, monks have trouble with elephants as well.
But it is said that at this time there was an exception among the elephants because they, well, what happened I guess is they would get used to the nature of the mind of someone who was wearing the cloth and they would come to see that they come to associate the cloth with a piece of her mind because they would see so many peaceful and even enlightened beings.
So this Hunter saw this and he created this idea in his mind that he would pretend to be an ascetic and as a result he wouldn't be able to hunt these elephants.
So there was a private, a particular Buddha and enlightened being who the elephants particularly revered and thought very highly.
When this ascetic was bathing in the river, he went and stole the ascetics robes which were left by the side of the river.
Yeah, it was that bad.
And he put the robes on and he went and stood by the elephants and when the elephants went by the last elephant to go by would come and bend down and pay respect to him
and then he would throw his spear and kill the elephant.
And he did this actually.
He was able to do this several times until the leader of the elephant heard, it came that the Bodhisatta, the Buddha to be, was born as an elephant and came to be the head of the elephant heard.
And he started to notice that the herd was decreasing.
There were fewer and fewer elephants in his herd and he realized there must be some reason for this.
And then he thought of this ascetic and he said, maybe there's a connection here with this ascetic that we go by every day and then suddenly there's one of our group is missing.
And so after that he said, I'll go at the end, you guys go ahead and I'll take up the rear and all the elephants will go by.
And at the end then they came to this ascetic or this hunter and he saw the hunter and so he stood there and waited and looked at the hunter and the hunter pulled out his spear and the elephant stepped back and the spear when he threw the spear and missed him.
And then the elephant was sort of talking to the hunter and saying, oh, what a terrible thing you do, how can you do this?
At first he tried to, what happened was at first he tried to kill the hunter and he was very angry and he was going to smash the hunter a bit.
And then the hunter, in his hurry to get away he held up the robe or he put the robe over his head or something.
And the elephant saw the robe and he realized, this is not right, he said, that robe has some great meaning and if I were to commit violence in towards this person I would be committing a horrible sin and I would be disrupting this tradition of paying respect to people in the robe.
This is what he thought.
And so he didn't and he just, he scolded the hunter and said, oh, what a terrible thing you've done.
So the Buddha said, this is the sort of thing David at the desk.
He didn't really ordain with the right reasons or with the right idea of what he was doing and so his meditation went in the way of gaining magical powers and didn't go anywhere beyond that.
Once he got these magical powers he became conceited and very much attached to them and attached to his gain and so on.
And as a result he, his corruptions and his mental stains never left him.
So this is the story.
Now in regards to what the teaching that this has for us, I think it's a good opportunity for us to talk in general,
what it means to be a monk and really what the difference between is between a monk and a priest.
Because a priest is someone who takes on the role of a monk or a religious leader as a position or as a role in the community.
Whereas a monk is really not like that, the priest is the official and so they wear these robes and then they will even take them off when they're not officiating at a ceremony.
A priest is someone who acts as an intermediary between oneself and the higher powers or the higher aspects of reality.
Obviously thinking about God and heaven and so on. So it's someone who leads people in prayer or brings people's prayers to heaven or however you want to look at it.
And so I think in Buddhism we have both of these and you can hear people call the Buddhist monks priests and it's really an unfortunate name and it really doesn't have any place in the Buddhist teaching.
But the people who they call priests in general tend to be fulfilling that role.
Now whether it's because of the expectations of the lay people or the expectations of the lay people are based on the actions of the, the quote unquote, priests.
Hard to tell and I would say it goes both ways that people are people and they're encouraging each other's to families.
So what happens is in general is maybe people had some good intentions in ordaining or thought, hey, this is any idea. They see the problems in society and they aren't able to find happiness in society.
So they hear about this teaching and they think that'd be a good idea. Maybe that will bring me happiness.
But they're unable to leave behind their ideas of what happiness is, the ideas that central pleasure can bring happiness. And so when faced with the choice between finding a material gain and praise and fame and material happiness.
As opposed to putting out work and effort in terms of giving up and renouncing and leaving behind and sort of straightening out your act and purifying the mind really.
They will tend to incline more towards the material side and so they will tend to gravitate towards those acts that bring us about and as a result they will become good at chanting, good at ceremony, good at ritual.
So simply based on the animal urge towards this material pleasure or based on these addictions which are really what we're trying to do away with. And it's just a natural course of affairs because they have these strong addictions and they're acting and cultivating them.
And of course as they interact more with lay people as a result and lay people come to the monks with their defilements and say I want this and want that.
It starts to wear off onto the monks as we saw with the story of Julekala and Mahakala. Julekala didn't do any meditating. He spent all his time surrounding by lay people and so on. And as a result he never really changed. He was never able to give up his addictions and his attachments.
When the time came for him to disroad he was like yeah okay fine and he went back to his home life with his wives and his family. We see this nowadays.
I think one thing I'd like to talk about in this regard is really how we should relate to it because I see some ways of relating to these sorts of people that are really improper.
On the one hand, well there's many ways actually. One of the ways is the ways that we monks will relate to these sorts of people.
And I think it's with far too much tolerance and not exactly tolerance. Yeah I think tolerance is maybe a good word.
With far too much, I don't know what the right word is, but we do far too little in regards to these sorts of people.
Even just in terms of informing and opening up and making clear the actions of our fellow Robees or fellow robed individuals.
And what I'm thinking of specifically is in regards to the more extreme versions of this story.
So in the case of people like David Datta it is never made clear even to this day and age that there are people who do this sort of thing that this sort of thing exists in the Sangha.
And so it's a really hush-hush and keep it quiet, put it under the carpet sort of thing. And this is really going to be a real problem for Buddhism and for the Buddha's Sangha in the future, as we can see with the Catholic Church.
The disgrace and the problems that the Catholic Church has, the scandal of the Catholic Church is really not something that Buddhism or the Buddhist monastic order is at large is free from.
Now those people who are acting as the Buddha taught for monks to act are not a problem at all.
The problem is that there are what might one might call priests in their midst and in the Buddhist order.
So you will see many temples which actually should be called Buddhist monasteries that are just full of priests when they're posing as monks.
They're wearing these robes, but all they're doing is doing the official ceremony. When they go back to their good, their rooms or houses or whatever.
And behave as ordinary human beings and often behave far worse than ordinary human beings because they don't have the social structure.
They don't have the oversight that an ordinary human would have.
So because they're thought of as someone wearing the robe, in the case of this elephant, they're put on a pedestal.
And so the lay people will say, you know, that's a monk, we can't touch him.
The other monks will say, you know, we're not supposed to talk about these sorts of things.
And I think get the wrong idea that in general it's wrong, it's bad, it's unwholesome to talk about these things.
And in fact, it's something that's really going to hit us hard and have a huge invite already is having an impact.
Buddhism is getting a bad name in America, it's making the papers.
What do Buddhist monasteries do when a monk commits child abuse or sexual abuse?
They move the monk to another monastery and try to pretend it never happened.
They don't tell anyone that it happened, they don't tell the other monastery that this monk is a child abuser or so on.
And this is how it goes.
So what's going to happen, you know, obviously this sounds quite familiar.
This is exactly what the Catholic Church did.
And you can see what has happened to the Catholic Church.
It's obviously lost a lot of its reputation and its accountability.
And Buddhism is very much the same.
I think it's important that we talk about these things and that we open up.
There are many different types of people wearing this robe.
Just because you're wearing this robe doesn't mean you're like me.
I'm not that I'm a good specimen.
I'm one type of monk.
There are other monks who are far more strict and straight and in line with the ancient code of discipline than I am.
And then there are people wearing this robe who have no discipline whatsoever.
And there are many different interpretations in between.
There are monks who I think are very worthy and noble, but they still break many of the rules.
And I think, well that's fine.
They have nobility and they practice meditation and they are great beings.
Often beings that I look very much up to.
And then there are other beings who maybe keep some of the rules or keep very strict rules.
But really have no, you know, nobility inside of them at all.
There are people who don't have either who don't have any sort of rules.
And don't have any sort of nobility.
So people who have the Dhamma, but not the Vinayya, people who have the Vinayya, but not the Dhamma.
And people who have both and people who have neither.
I would say as long as you have the Dhamma, it's not really a problem.
Because lay people can have the Dhamma without the Vinayya.
Without all the rules, they can be noble in their hearts.
So if a monk is noble and is hard and maybe doesn't keep the rules very strictly,
I don't have a problem at all with that.
In fact, I look very much up to that person.
But if a person doesn't have nobility, then the rules really won't help them at all.
And the road certainly won't.
And so the short story is here, two things.
One that I would caution people going to temples and talking to priests
and thinking that some other going to come out with Buddhism.
There are many temples, Buddhist temples around the world.
And I would say as soon as you find out that it's a Buddhist temple,
I would be at least 50% suspicious.
When you find out that the monks are called priests,
that would be at least 75% suspicious.
And there's always a chance that the monastery doesn't understand
what the word temple and what the word priest means.
And so as a result, they just follow, use these words.
They're just words.
And in fact, you could twist them both to mean very good things.
And if you want.
But in general, 90% of the time, there's no meditation going on.
There's very little study of the Buddha's teaching.
And there are a lot of corrupt and unholesome activities going on.
There's infighting.
I've been to temples where there were three different groups of monks in the monastery.
And they were at each other's throats.
I've been through many, many different situations at many, many different Buddhist temples.
So that's the one side of it.
On the other side, I would say, we should not be afraid to, and we should be aware.
And we should make a clear division in regards to those monks who are doing these corrupt things.
That if someone is found, if there are charges against the monk,
this monk apparently, they had DNA evidence that he had fathered the child of this 12-year-old girl,
or whatever it was.
A case of obvious child abuse.
Even if it wasn't such an obvious case, there should be a trial.
The monks should hold a tribunal according to the Vinaya, and it should come out clear.
And it should be clear to the laypeople that this is what we're doing.
What are you doing well?
We're having our investigation, and we're going to see if there is, and we're going to be impartial.
And we're going to be open.
And then we're going to come and tell you, these are our findings.
And if we find that this monk has, you know, beyond reasonable doubt, committed this offense,
then we're going to kick him out, and we're going to ostracize, and we're going to say,
this is no longer a monk.
He shouldn't be wearing the robe.
If he's wearing the robe, he's wearing it in theft.
And we're going to come out and tell you, because according to the Vinaya,
according to the Buddhist monks, that's what we're supposed to do.
We're supposed to have a...
See, people misunderstand, they think, oh no, according to the monks' rules,
you're not allowed to talk about a monk's offense, to lay people.
But the exception is, unless you are that person who was supposed to be appointed
by all of those monks keeping their mouth shut, to be the person to tell the lay people.
The point being not to hide it, the point being not to let just anyone go out and tell anything.
Because then Davidatta says, oh yeah, the Buddha was committing,
you know, he was committing these offenses against the Vinaya,
having sex with women and so on. You know, anyone can just go out and say anything.
But according to the rules, it's only the spokesman person
who has been elected as the representative.
Your job is to go out and tell the lay people, they're waiting to hear what happened.
We're going to be...
You know, now it's very easy.
At this point, you could go on the news and be this representative and say,
this is what we've found.
And then it's up to the lay people to decide whether it's a reasonable finding or not.
Now that's not happening. What's happening is half of it.
The negative side, you know, okay, we're not going to say anything.
And then, you know, so what? So nobody's going to say anything.
And then people think, oh great, we can just hide it and pretend it didn't happen
and we don't have to get involved. But no, it's not great.
People will find out. And when they eventually do find out,
we're going to have a problem as big or even bigger than the Catholic Church.
And that will be of a great detriment to Buddhism.
The critics in Buddhism will be able to say, see, see people who are jealous of the fame
and the greatness that Buddhism has found and that the Buddha's teaching has brought.
The great things that the Buddha's teaching has brought,
the people who are looking to undermine that will have great ammunition for it.
And it will be a great loss to the world.
So this is one very important reason to talk about the difference
between these two kinds of monk.
Just because someone is wearing the robe doesn't mean they are a great person.
And some people are not worthy of the robe.
It can happen that someone wears a beautiful, expensive, wonderful robe
and be totally corrupt inside.
Because the point is not the color of the cloth.
The point is the color of your mind.
Are there stains in your mind?
And it's not the stains on your robe, that count.
Yeah, so I think that's a very important issue here.
I think another point that I wanted to make in this regard
was in regards to David, that is behavior in general,
which is in terms of corrupting lay people.
And this goes on the other side where people encourage,
accept and appreciate the activities of such a priest or monk.
And they come to expect it from the monks.
Many people wonder, well, why shouldn't the monk do things for lay people?
Why shouldn't the monk act as a doctor or a fortune teller?
Or so why shouldn't the monk do these things
doing good things that the lay people want?
I would say there are three very good reasons.
And it's very much against the Buddhist code of conduct
for a monk to act as a doctor, for a monk to be a fortune teller,
for a monk to do all of these things,
which actually some of them can be very good and wholesome things.
You know, cannot act as a messenger for lay people,
cannot do work for lay people, cannot act on their behalf.
I've had people want me to translate the legal documents
into English or so on, and I've had to say no.
And then they get angry, and they think,
well, why won't you do this for me?
Why won't you do this favor for me?
Three reasons.
The first one is that it encourages
defilement among the lay people.
Because what monks are allowed to do for lay people
is help them to give up their defilements.
If it's something that directly improves your state of mind
and your purity of mind,
that's what monks are allowed to do.
When you do other things, you are getting involved in the defilement.
So you're translating legal documents,
or even if you're acting as a doctor.
These may be in a worldly sense good things,
but they're actually, what they do instead is encourage people
to become attached to physical health, for instance, and so on.
There's nothing wrong with the doctors in a worldly sense.
It's a part of the world where you have doctors,
you have teachers, you have these people who do,
quote unquote, good things,
and allow people to gain their livelihood and so on.
But in the end, all of it becomes meaningless.
And when you engage in that, you're encouraging,
and you're setting this idea in lay people's mind
that this is the ultimate goal.
The ultimate is to find material wealth and health
and well-being and so on.
And it prevents them from thinking that there might be
something more important in terms of their mental well-being.
This is the one reason for, and of course,
all of these ceremonies and blessings and so on
and helping people to find material benefit
and encouraging people to become superstitions and so on,
even telling their horoscope or so on,
which actually may have some grain of truth.
Apparently, the stars somehow can tell something
and there's a science to how the interconnectedness
of the universe, how you can look at a certain aspect
of the universe and tell something about another aspect of it.
So the horoscopes could have some truth to them.
It may not.
I don't know.
But regardless, that doesn't mean that we should go
about using this for material gain or for any sort of gain
whatsoever, because obviously the idea here is to let go
and give up and accept and to be happy and content with whatever comes.
The first reason, the second reason is it corrupts one's own mind
because as a result of doing all of these things,
as I said, you're surrounded by people who are also thinking
about these things.
Your interactions with people become more base
and less interested in the goal of the Buddhist teaching
and giving up and purification and so on.
And the great majority of them,
almost every time all of the examples of this
are you become caught up in material gain.
Now, most people will become corrupt as they start to get more
money and fame and wealth.
Many people wear the robe and still keep money.
And it's possible you can do that without becoming corrupt,
but for most people they do become corrupt.
And what you see for the majority of people wearing the robe
when they start to touch money is they become very much attached to money
and always thinking about ways that they can make more money
and get more wealth.
And they're always worried about money.
They have bank accounts and they're always thinking about the money they have
and how to keep it and so on.
And then people come, well, this is the ones that is how it corrupts your mind.
I guess another separate reason, but the same sort of reason is
even if your mind isn't corrupt,
the interactions in this regard for monks who keep money
for monks who get involved in the activities of lay people get involved
in so many priestly activities is that it obviously takes away
from your time that could better be spent meditating.
There's an example of a monk who has a great amount of respect
from the lay people.
And appears to be doing it out of the goodness of his heart.
He acts as a doctor.
So he has all these cures for everything.
He's able to find a cure in some of the users, his mind power,
to see what's wrong inside of people.
Sometimes he gives herbs or he gives special cures,
therapeutic cures or methods to cure various illnesses.
Here's that he's doing it out of the goodness of his heart.
Now, he's also getting very rich as a result.
He's able to build a huge temple or monastery and meditation center.
And the problem is no one's coming to meditate
or people are not really at all interested in coming to meditate there.
So wherever he goes somewhere,
he was invited to give a talk on meditation
and he couldn't give the talk because he was swarmed by people
from all over the area trying to get a cure for their illness.
And so he said, you know, I want to teach meditation,
but this is what he claims.
It could be true.
He said, I want to teach meditation,
but there's all these people who want to be cured.
And so he never has time because people are always asking me.
I went to eat lunch with him and he didn't have time hardly to eat lunch.
There were people coming up and asking him for cures to this and cures to that.
I want to get pregnant, one woman said,
so he gave her a way to become pregnant.
So go figure.
This is one of the results.
Another result is simply from keeping money.
I've seen very good monks, very good monks who keep money
and end up with a lot of money.
And they're never at hatch to it.
What you see is that the people who sort of type of people
who gravitate towards them and tend to surround them
are generally not the sort of people who meditate
or who are as pure as they are.
Because a monk, if he's very pure,
can attract a lot of support from lay people.
But the question is whether the people surrounding that monk
will be of a similar caliber.
And it can often happen that they are not.
The more moral, the disciples of this monk who are interested
in meditation will go off and meditate.
And those people who are interested in money
will surround this monk and start to control.
And as a result, you see a lot of corruption in relation to
the monasteries, the places where these sorts of people reside.
So even if these sorts of activities are living as a priest
or living in ways that supporting lay people,
helping lay people in worldly things,
even if it doesn't corrupt your mind,
it certainly takes away from your ability to run a meditation
center or to encourage people in good things in general.
The third problem with this sort of behavior
and another reason why the Buddha forbid it,
and it's very clearly forbidden in these sorts of things,
is that it corrupts or it's harmful towards other monks.
So here I said it's harmful towards the lay people,
it's harmful towards one's own practice,
but it's also harmful towards good and wholesome and moral
and well-behaved monks because if they are not doing these sorts of things,
then they're not going to have hardly any support from the lay people.
I've experienced this and I know many monks have,
where as I said, you know,
unless you do the things that they want to do,
they get angry and upset at you, and they leave,
and they might even become your enemies,
and they might even start to turn other people against you
and can cause a lot of problems for you,
and even just getting the basic necessities of life,
which are getting food on arms round.
I've had a monk go around telling people
that actually in a whole other story, it's not related,
but when people, when you don't act in the ways that people expect,
for instance, here in Sri Lanka,
people expect me to do these chants
in the morning they want me to do chanting with them
before breakfast, they want me to do chanting
when they bring lunch, they want me to do chanting before they give me lunch.
So I just go on an arms round, and luckily,
there are many people here who are very happy to just give me,
you know, whatever food that they have,
some people give me food every day, even as I walk by their house,
and they do it out of the goodness of their hearts,
and because they respect and they appreciate what I do.
But there are many wrong expectations of people
that a monk should be somehow at priest,
and they talk about monks, or they call them priests.
Now they don't maybe understand what the meaning of the word priest is
because it's an English word, but they sure,
many cases act like it, as though they expect the monks to work
for the lay people.
And certainly a monk can and should teach people
how to become free from suffering,
but he certainly should not teach people how to cling
and become superstitious and to engage in blind ritual.
And he certainly shouldn't work for other people
in terms of being forced into doing these rituals and so on.
Anyway, it's something that has come up here,
but people are not, it's not that they're forcing,
but they don't understand about this.
And so I think it's important to explain why it is that,
you know, actually we are a dain to become free from suffering,
and to look, you know, we are dain to give up really
to not need this kind of support.
So the basic necessities of life I get from people,
as I walk by their door, and they don't have any thought
that I'm going to do something for them.
Of course, if they want to come meditate,
they can, but they do it out of the goodness of their heart,
really nothing to them.
They make breakfast and they give me a part of it.
Or they make lunch and they, as I walk by,
they're giving me some little bit of curry,
or a little bit of rice, or whatever they have on hand.
And because they appreciate what I stand for them,
what I do, and because they understand the importance of giving
and so on.
And it's just in general a very wonderful thing.
But the reason or the whole meaning of it, or of our lifestyle,
is that we don't want to have to participate in waste our time
with amassing wells.
I could go out and make money and have all the food that I want,
have all the clothes that I want,
whatever I want, whenever I want it.
But that, of course, would leave me no time to understand
and to investigate the nature of reality,
and to practice meditation, and so on.
So, which, of course, is our duty as monks.
So, I think we should be clear in terms of our understanding
of the difference between these types of individuals,
the one who puts on the robe,
as a, and begins to act as an official,
or officiate over ceremonies and rituals,
and a person who takes it as a way of giving up,
of leaving society, of taking on a lifestyle,
of renunciation, where one wants to devote all of one's time
to the practice and the study and the teaching
of those things that lead to peace, happiness, and freedom from suffering.
And this is in line with the Buddha's teaching,
what she gives in these two verses,
that a true person who is truly worthy to wear the stained cloth.
It's the one who has given up the stains in their mind,
who has training, who has trained themselves
in the Buddha's teaching and who has truth.
And is in line with the truth.
So, what they teach with the practice,
what they study is in line with the truth.
They're not someone who is stained inside,
but wearing something very pure and noble outside.
So, they have the stains outside on their robe
and the stains inside on their mind.
This is someone who is not worthy of the stained cloth.
So, rather lengthy, but I hope there was some benefit to that.
I think it's an important topic and something
that is important to bring up.
And this is a good chance, a good opportunity.
So, thanks for tuning in.
This has been another episode of our discussion of the verses,
of the study of the verses in the Dhamapada.
So, thanks for tuning in all the best.
