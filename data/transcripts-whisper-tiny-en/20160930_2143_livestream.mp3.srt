1
00:00:00,000 --> 00:00:07,000
It's explicitly true, because there are ways of getting there to that state.

2
00:00:07,000 --> 00:00:13,000
Don't require the summit to restrict summit to practice.

3
00:00:13,000 --> 00:00:20,000
You want to come and do a course with me, I'll show you.

4
00:00:20,000 --> 00:00:24,000
But definitely, you're suggesting that that's the path to enlightenment, absolutely not.

5
00:00:24,000 --> 00:00:28,000
Read on in the sita.

6
00:00:28,000 --> 00:00:30,000
The Buddha gives us an elsewhere.

7
00:00:30,000 --> 00:00:34,000
The Buddha says that summit to meditation, the jhanas are just...

8
00:00:34,000 --> 00:00:36,000
I have a problem.

9
00:00:36,000 --> 00:00:37,000
They're still just waiting.

10
00:00:37,000 --> 00:00:42,000
They're still impermanent.

11
00:00:42,000 --> 00:00:43,000
The path to enlightenment.

12
00:00:43,000 --> 00:00:47,000
The Buddha said, what is the path?

13
00:00:47,000 --> 00:00:48,000
Some based on kara anita.

14
00:00:48,000 --> 00:00:53,000
When you see that all formations are impermanent, that's the path to enlightenment.

15
00:00:53,000 --> 00:00:56,000
The Ace of Mango Visudya, this is the path.

16
00:00:56,000 --> 00:00:59,000
Purification.

17
00:00:59,000 --> 00:01:26,000
Thank you for answering my question regarding what is required to stay at Suri Mango,

18
00:01:26,000 --> 00:01:29,000
and what may be necessary when considering to ordain.

19
00:01:29,000 --> 00:01:34,000
I very much appreciate your advice and believe it is wise to take your meditation course

20
00:01:34,000 --> 00:01:37,000
before making a commitment to ordain.

21
00:01:37,000 --> 00:01:42,000
Are there specific dates I should plan to travel to make the best use of our time?

22
00:01:42,000 --> 00:01:46,000
I don't want to be inconvenient or burdened to you or the other meditators.

23
00:01:46,000 --> 00:01:49,000
I'm simply interested in learning.

24
00:01:49,000 --> 00:01:54,000
PSI attempted to write this message to the content form on the Suri Mango website,

25
00:01:54,000 --> 00:01:57,000
but receive the error failed to sit.

