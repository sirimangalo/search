1
00:00:00,000 --> 00:00:05,160
Okay, so this is a question that's fairly practical.

2
00:00:05,160 --> 00:00:11,720
What did the Buddha and what do you or other teachers recommend in order to generate greater

3
00:00:11,720 --> 00:00:18,920
motivation to practice when one's dedication and enthusiasm is waning?

4
00:00:18,920 --> 00:00:24,160
I think the biggest thing that the Buddha recommended, no, I don't even know if it's the

5
00:00:24,160 --> 00:00:28,360
Buddha, the biggest thing that I can think of and it's actually from the commentaries is

6
00:00:28,360 --> 00:00:34,760
to associate yourself with people who have the qualities that you lack.

7
00:00:34,760 --> 00:00:40,960
I mean, this is based very much on the Buddha's famous suit to where he tells Ananda that

8
00:00:40,960 --> 00:00:47,480
living with association with good people is the entirety of the holy life, the entirety

9
00:00:47,480 --> 00:00:49,600
of the spiritual life.

10
00:00:49,600 --> 00:00:53,560
Ananda says he figures it's about half and the Buddha says shame on you, don't

11
00:00:53,560 --> 00:00:58,200
say that it's the entirety of it.

12
00:00:58,200 --> 00:01:08,800
So association and that doesn't just mean talking with, but even hanging around such

13
00:01:08,800 --> 00:01:18,000
people will lead you to cultivate the same sorts of states.

14
00:01:18,000 --> 00:01:19,480
So what do I do with my students?

15
00:01:19,480 --> 00:01:28,320
I mean, you encourage them, it often depends very much on what's getting in their way.

16
00:01:28,320 --> 00:01:29,320
It can be doubts.

17
00:01:29,320 --> 00:01:34,800
If doubts are getting in the way, then they need an explanation, I'm sorry if it's intellectual

18
00:01:34,800 --> 00:01:39,440
doubts, then they need an explanation or they need an answer, something that at least

19
00:01:39,440 --> 00:01:48,400
tells them that shows them that their intellectual pursuits are useless and I'm beneficial.

20
00:01:48,400 --> 00:01:55,400
If it's doubt about themselves or feelings of doubt about their practice, like when practice

21
00:01:55,400 --> 00:02:00,680
is not going well or I'm useless, I'm not good at this, then they need some reassurance

22
00:02:00,680 --> 00:02:07,840
and they need an argument, you know, not a way of helping them to see things in another

23
00:02:07,840 --> 00:02:18,120
way, discussion is very important, if or often just a dumbah talk, giving them some kind

24
00:02:18,120 --> 00:02:25,400
of encouragement, you know, so if they feel discouraged in the sense like they don't,

25
00:02:25,400 --> 00:02:31,760
they aren't so interested in meditation, then you have to give them a talk explaining

26
00:02:31,760 --> 00:02:36,640
about the things that they are interested in and showing those things are perhaps known

27
00:02:36,640 --> 00:02:43,360
as meaningful as they thought they were and it's talking about the importance of meditation.

28
00:02:43,360 --> 00:02:53,640
So, dumbah talks about the benefits of renouncing and giving up and disadvantages of

29
00:02:53,640 --> 00:03:04,440
clinging, talks that are encouraging discussion that is encouraging, you know, the best

30
00:03:04,440 --> 00:03:08,720
thing of course is to meditate because meditation helps you want to meditate more, more

31
00:03:08,720 --> 00:03:17,480
you meditate the more you become interested in it, the more benefit you see from it.

32
00:03:17,480 --> 00:03:18,480
Anybody else?

33
00:03:18,480 --> 00:03:20,640
What do you think motivates people?

34
00:03:20,640 --> 00:03:26,960
What do we do to motivate ourselves and motivate others?

35
00:03:26,960 --> 00:03:39,040
What do you do when you don't want to practice?

36
00:03:39,040 --> 00:03:44,480
I think like when it comes to like, sloth and torpor, rising into mind, a couple of

37
00:03:44,480 --> 00:03:50,640
simple things that you can do to overcome those, so I give yourself a little bit more motivation

38
00:03:50,640 --> 00:03:59,440
to go out and sort of cold air, some water on your face, changing conditions during

39
00:03:59,440 --> 00:04:13,360
meditation or if you're not like sitting, formally you can put your awareness on the

40
00:04:13,360 --> 00:04:19,160
tiredness, on the lazy feeling, on the feeling of not wanting to meditate things like

41
00:04:19,160 --> 00:04:21,320
that.

42
00:04:21,320 --> 00:04:25,320
Yeah I don't know so much about, you know, if you're just tired because people can really

43
00:04:25,320 --> 00:04:31,000
want to meditate even though they're tired, but it can be very discouraging and I think

44
00:04:31,000 --> 00:04:37,600
it's more the discouragement that's being asked about here, then how do you deal with

45
00:04:37,600 --> 00:04:38,600
being discouraged?

46
00:04:38,600 --> 00:04:42,120
The best thing is to have a teacher and to have a community because when you do all

47
00:04:42,120 --> 00:04:48,960
of these things will come, you'll have talks, you'll have discussions and so on.

48
00:04:48,960 --> 00:04:52,600
But one interesting thing about you talking about is tricks, right?

49
00:04:52,600 --> 00:04:55,880
Sometimes you have tricks to make yourself more motivated, the way you actually hear

50
00:04:55,880 --> 00:05:01,120
is commenting about doing meta meditation, which is yeah another good trick, do something

51
00:05:01,120 --> 00:05:03,840
to give you encouragement.

52
00:05:03,840 --> 00:05:06,000
Think about the Buddha we talked about this yesterday, right?

53
00:05:06,000 --> 00:05:09,880
Something that's very encouraging is to think about the Buddha, the Buddha said whenever

54
00:05:09,880 --> 00:05:15,520
you feel afraid, you're often the force and suddenly you feel afraid, say to yourself,

55
00:05:15,520 --> 00:05:18,960
if you tip yourself, I go, I think about the Buddha and the fear will go away immediately

56
00:05:18,960 --> 00:05:19,960
and it does.

57
00:05:19,960 --> 00:05:27,000
It's quite impressive how the thinking about the Buddha can make your fearless because

58
00:05:27,000 --> 00:05:31,480
you start realizing how fearless is remembering how fearless is in this.

59
00:05:31,480 --> 00:05:44,080
Okay, if anybody wants to pick through the comments, don't wait, stop this one.

