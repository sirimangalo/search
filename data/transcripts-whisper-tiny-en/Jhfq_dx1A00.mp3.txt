All right, good evening, we're back again Wednesday or another meditation Q&A session.
We have ever and again more people signing up for the at-home meditation course.
This is a chance for some of them to ask questions if they have in the middle of the
week in between sessions.
We have a weekly meeting with each at-home meditator and they can ask questions then, but
sometimes it seems they come on here and we'll ask questions they forgot to ask.
And then of course all the people who've done the at-home course or those who haven't
but are interested come here to ask questions.
So the way this works, you may have noticed you can't see my face, you can't see me,
you can't see Shradha, you can't see anything except the main the text screen.
And that's on purpose because the way this works is you're encouraged to close your eyes,
bring your mind to the present moment.
You're asked not to chat, not to use the chat for chatting.
The chat is actually for asking questions.
If you have a question, open your eyes, type your question in and close your eyes again.
We're meant to try to take this opportunity to practice together, to cultivate mindfulness,
clarity of mind, purity of mind, to cultivate good habits together.
And so it's a special Dhamma session, it's not an intellectual Dhamma session, it's not
about asking questions for your intellectual edification.
The criteria for whether we're answering questions is based on a simple question of whether
the answer to that question will help that person in their practice.
And it's up to Shradha, the burden is on her to decide.
She's the first ranks, front line ranks, she gets to decide whether that's a question
that the answer will help the person who asked it.
And then if she asks it, and I still don't think that it's important to answer, I might
say, I pass, we'll try to be generous with, we have an hour, so try to be generous
as far as time permits in answering questions.
Another thing that we ask is that you try to be respectful.
So when you ask a question, make sure that your grammar is the best you can do, that you're
not sloppy or lazy, and leave it up to Shradha if someone has been disrespected, was
been negligent in their wording and overly sloppy, and it's clear that they don't understand
English, they just didn't put any effort into the question, that's a sign that maybe
they're not ready to appreciate the answer.
Of course, if your English isn't your first language, that's totally understandable, and
we'll try to be generous, we're here to be kind and generous as best we can to share.
But two things that you might want to keep in mind is that a lot of answers are going
to be simply that you should read the booklet that we have.
We have this booklet on how to meditate, it's, I wrote it, but a lot of it is just verbatim
what I was taught, translated from Thai, and so it basically sums up the practice that
we follow in the basic steps.
So the first thing to note is that you might just get an answer while I'd recommend
you read the booklet because you probably haven't if you're asking this and the practice
that's in the booklet very well might help you overcome the problem that you're facing
or find an answer to your question.
The second thing to keep in mind is a lot more of your questions can be answered through
doing an actual course in meditation if you never have or if you've done it in a different
tradition, a different spiritual path.
You might want to consider doing an at-home meditation course.
We offer that through our website where we meet once a week using Skype, this program
called Skype, some video conferencing program we don't use video, you don't need a camera,
just audio.
And we go through more advanced techniques, we slowly advance you through a meditation
course, with a lot of stuff that's not in the booklet and a lot of questions can be answered
in the sessions or just through the practice that you do on your own.
We get a lot of later in the course, a lot of your questions are just answered by meditating.
I guess the third thing I can say is that our center is opening again on a limited basis.
We're going to invite Canadian meditators if you're in Canada and you're interested in
coming to meditate.
We're accepting up to two people.
We ask that you make sure that you haven't exposed yourself to the coronavirus, so that you
don't share it with the other people here, sort of the best of your ability, your knowledge,
you don't have it, and then take best of care not to spread it, not to bring it.
Well, you're here, you have to try and be cleanly and careful about spreading germs,
sharing germs, but you could come and do an intensive course if you're really brave.
Right, stay with us and feed you, how's you teach you all for free, everything's free
here, and that'll answer, that should answer the most important questions.
Sometimes it might give you more questions as you go deeper, but a lot of the basic questions
will be answered.
So apart from that, and even with all that, we're still going to answer people's questions
here.
You have questions, feel free to open your eyes and then ask them.
If you don't have any questions, we ask that you take part in this session by meditating
with us, close your eyes, take up the present moment as an object, all of the experiences
physical, mental, and remind yourself, it is what it is, this is this.
Find a word for it, note it, move on.
God, they will be asking questions, I'll be answering them, you can just keep meditating.
We have questions, I'm ready.
In order to observe something, we suspend judgment.
We don't say it is bad, but to we like it as unhold some if it is, to we need the intention
to eradicate it.
So let's think, what would it mean to mark it as unwholesome?
You would be, I guess you would mean you would note it as unwholesome, right?
You would say to yourself, that's unwholesome.
And how do you know it's unwholesome?
Because you've read about it being unwholesome, well that's not the goal, right?
That's not good enough.
Understanding that something is unwholesome comes as a result of the practice.
So you saying that it's unwholesome isn't going to have any beneficial effect.
I mean maybe in the preliminary sense, oh yeah, don't get caught up in anger, don't get
caught up in greed.
It means of helping you to be careful in your noting, it's not going to solve the problem.
So we know things as they are objectively, because not because we don't discriminate, but
because we want to be able to discriminate, we want to know whether things are wholesome
or unwholesome.
That knowledge comes from observation, from objectivity, it's like you're in a lab, you can't
use the conclusions as a part of your study.
And then the intention to eradicate it, yeah, so if it is referring to the experience
you just had, it's already gone, only lasts a moment.
But more likely it refers to the to what, right?
The anger, suppose it's anger, we think of our addiction, you want to eradicate the addiction.
Well, addiction isn't a thing that exists.
Addiction is a collection of moments that are gone as soon as you realize they're there.
By the time you've realized they're there, they're already gone, but the time you're
going to note them, they're already gone, they've eradicated themselves.
What we try to eradicate is the underlying tendency to give rise to those states.
And underlying tendencies are caused by what?
A caused by ignorance, delusion, wrong view, which of course is eradicated by wisdom,
by knowledge, by clarity of mind, and that's why we practice the way we do.
We don't so much suspend judgment, I think.
We note when we're judging as well, we're just trying to see clearly.
Well, sometimes you'll notice the eye, sometimes you'll notice the object.
Sometimes you might notice the eye consciousness.
It's all just seeing, seeing is how you encapsulate it all.
Of course, if there's thinking, you should not think if there's feeling, you should not
feel it.
Note whatever's clearest.
Is there a way to stabilize mindfulness?
So mindfulness mental space lasts a long time, a longer time.
No mindfulness lasts only a moment.
All you can do is increase your inclination to be mindful of the mind's habit, inclination
towards being mindful.
When you do that by cultivating it as a habit, becoming more inclined, more accustomed to it,
and also by seeing more clearly that there was a truth of reality which inclines you
more towards mindfulness anyway.
What is the best way to start meditation for people who are not used today?
You should meditate on the things you're not, the being not used to it, because that
might mean that you're agitated or bored or uncomfortable and you shouldn't be mindful
of all those things.
But yeah, the book that talks about some of that, of course, you can also do a meditation
course with us.
We have ad home courses.
That's the best way to do it with the teacher.
You're more inclined to do it, you're more motivated, and you get better at it because you
have advice, you have support.
It's much better than doing it on your, you'll try to learn on your own for most people.
Do you find inner peace?
Well, peace is in many ways just the absence of war.
People say it's not just the absence of war.
In many ways it really is, and you have to broaden your definition of war, so thinking
in a worldly sense, right?
It's not that ending war isn't enough, it's that you really have to understand it's
about ending violence, really.
I guess the point is that it's not just ending war, it's ending violence, but it's
ending the opposite of peace, really, because peace can be varied to some, well, peace
doesn't depend upon surroundings.
That's the point.
When you give up the violence, you give up the reaction, you have peace, you give up the
clinging to put a fine point on it.
When you give up attachment, a nisito juriharati, one-one dwells, independent, not depending
on anything.
That'll make me happy, that'll make me happy, when you give up all of that, you're independent.
That jikinji lo-kiyo-pa-di-ati, not clinging to anything in the world.
That's how you find inner peace.
It sounds nice, and then the question, of course, how do you do that?
Well, really, that's what we're trying to teach here.
That's what the booklet is for, that's what these courses are for.
We've got a program, if you want to believe us, if you want to give us the benefit of
the doubt, we've got something we could show you that might help, you might find leads
you closer to inner peace.
On one day, the next questions are not about meditation practice, but I'll ask them
to put this on.
I'll keep telling them which crimes and mistakes in our past, specifically before discovering
Buddhism we should contest to.
Our confessing is, I don't know what exactly I mean, confessing, where are you going
to confess to, and what do you mean by confessing, we don't have confession and Buddhism.
I mean, monks have confession, actually, but it's a monastic thing, I'm assuming you're
not a monk, and of course you're not talking about things you've done as a monk,
which is the only thing you're supposed to confess, but that's just a tool, and it's a fairly
crude one at that.
The ultimate reality is you've done something wrong, there's no one doing it, you can't
undo it, and you shouldn't try to, and you should try to face the consequences, and you
should try to change.
You should apologize when the occasion arises for those people who feel hurt, who feel wronged
by you, if they still feel wronged by you apologizing to them is a great thing, so that's
not so much about confessing, but apologizing in Buddhism is a great thing.
We always apologize to each other when we do a course, the tradition is at the end of
the course you apologize to your teacher for anything you might have done wrong, and then
your teacher turns around and apologizes to you in the same way, if I've done anything
wrong, there's no ego, and there's no, yeah, well you're the student, you have to apologize
to me, no, you apologize to me, I apologize right back to you, where we're both fallible,
apologies an important thing, because we shouldn't hold on to things, right, we shouldn't
hold on to things we've done, shouldn't hold on to things other people have done, we
want to move past it, we want to move forward, we don't want to ignore it so it happens
again and again, and again, we want to work with each other, that's why we confess so that
we can get help from each other, that's why we apologize so we can realize that we're
holding on and that we can change our mind so that we let go and move on, but things you've
done, you've done them, they're done in God, there's no, there's nothing you, there's
no benefit to, going back to them, I don't bring up the past, don't dredge it up, it's
on, what should one focus on during meditation, I understand everything is a start, but
for further more developed practice, what should one focus on?
So you've most likely not read the booklet, recommend you read our booklet because
it's probably not the same as what you're thinking about meditation and then you understand
what we do, I can't offer you advice on anything else, so that's where I would start
if you're interested in what we do and then if you want to learn further practices in
our tradition after you've read that, you can do an atom course or even in the future
potentially come and do an intensive course at our center, that's how you develop further.
When using the mantra, how do you prioritize when there is too much for now, sometimes
hearing and thinking tend to clash and there is too much going on?
Well, you can always note overwhelmed if it feels overwhelming, but otherwise you just
pick what's clearest, it's not magic that you have to catch everything, just trying
to set your mind in a mindful way, an objective way, so by noting things in the present,
it helps build that clarity of mind, that habit, that helps evoke a mindfulness state,
you can't force it to come by noting everything, it's not like a video game or something,
just trying to get your mind in the right mode, the right mood, the right state, pick
whatever's clearest, and a lay person managed to end all of life suffering by meditating
or rather to stop suffering from them, we should maybe add this to the frequently asked
questions, it's not directly a meditation question, but boy, it's asked a lot, yes, you
don't have to be a monk, I guess what it's asked is because I'm a monk and people wonder
whether they have to be like me and you certainly don't, what does it mean to be a monk?
Well, suppose it means less now than it used to, I don't want to diminish the great and
the wonder of being, I appreciate very much the monastic life, but it's not the most
important thing by any means, are the heart practices from my Vyhayas asamata or Vipas
So the object of the Brahma Vyhayas are beings, and beings are concepts, and because
beings are concepts, it can't help you to see impermanence suffering, and I can't help
to see you, then help you to see the nature of reality, impermanence suffering and non-self,
instead you'll see things that are stable, satisfying, controllable, and so it's amatana
Vipasana.
I don't want my at-home meditation course to end, and can't come to Canada during the
pandemic, is it possible to do an intensive course at home or continue to meet and learn
from you once it's done?
So first of all, the feeling of not wanting something to end, that's getting caught up
in the future, you're going to have to come to terms with that, sounds like this course
that you're doing hasn't ended, so you're not present, you're now living in the future,
although maybe it has ended, or maybe it's ending now, I don't know.
The second thing I'd say is that the next step, if you can't do an intensive course,
is to learn how to do it on your own.
In many ways, in many ways, in some ways, let's say part of the benefit of the at-home
meditation course is like a dumbo feather, you ever saw that old movie dumbo.
It's a feather that he has this feather and it helps him fly, but it doesn't actually
need it, so it's not really something you need to meet with a teacher once a week, but
boy does it help your practice, right?
So the next step is to learn to be independent, and it's a nisito javiarati, then dwell
independent.
Can you do it without a teacher?
That's your next challenge, right?
Every week, there's a new challenge, well, the next challenge is do it without the teacher.
As far as doing intensive courses, I'm reluctant because of the intensive nature, and
it goes beyond what we try to accomplish in the at-home course.
Unless I know you well, and you have some maybe some experience doing intensive courses,
I mean, we'd have to talk, I'm reluctant to do it for your first intensive course in our
tradition.
It's not something that's recommended.
Maybe if someone had a, someone had practice in meditation for years or something, or
maybe more also important if they had a place, like some people have talked about a monastery
close by them, where they can go, and from there we can meet once every day.
It's also hard to schedule because I have already a fairly full schedule, and then every
day to have to meet one person, we have to fit it in somehow, and then if more than one
person doesn't, it's easy when we're here, we can do them, everybody in a row, one person
meet with one person, and then they get the next person, but if we're trying to schedule
all that over the internet, it gets more complicated.
That's not the main reason where we should, it's just more that, it's not really the kind
of thing you want to do without a stable and controlled environment, because it's quite
uncontrolled.
The states that you go through can be quite wild, and most likely what's going to happen
is that people will try to do it, and then halfway through they'll stop, and that's
discouraging, it's hard to do without a controlled environment.
If I desire to accomplish a goal later in life, is that desire healthy?
There's no such thing as healthy desire, there's no such thing, but you don't have to ask
me that, do some meditation, study the desire, learn about it, and you'll see for yourself,
you'll just start letting it go, you'll be less inclined to worry about wanting to do
things in the future, it's just such a stressful way of living.
This question is very similar to question, or question, it's not similar to the question
earlier.
Is it necessary to enunciate the world, to pursue enlightenment, or is it possible to
be involved in the system, while having a full spiritual awareness disposition?
So there's two ways of renouncing, you can renounce physically, which is what I've done,
as I'm becoming a monk, that sort of thing, and then there's renouncing it mentally,
which anyone can do, you don't have to become a monk.
The first, well the first is not necessary, the second is necessary, so it depends what
you mean by renounce the world, and it depends what you mean by being involved in the
system, being involved physically, sure, that's possible, being involved mentally, can't
do it.
If you're mentally invested in the system, you're never going to have a full spiritual awareness
disposition.
In fact, I would say, and while what the tradition says is that once you become fully enlightened,
you can't do the former either, you just have to leave, you can't even be involved in
it physically, you're just not inclined to, you just can't.
Some pajanya is a, I don't probably talk about it enough, it's a partner in the mindfulness
or sati, what we call sati, so sati is remembering or reminding yourself, so in some sense
it's more practical side, it's what you do, some pajanya is sort of the result, the
commentary is talking about it as being wisdom, but why the Buddha used some pajanya instead
of panya is sort of to give a flavor to it, that it's the clear awareness, the clear
state of mind, the clarity, the pinpointed nature of the mind, we're able to zoom in and
focus and see things so clearly, see things as they are, so it's the result of mindfulness
really or it's the accompanying quality that arises based on the practice, specifically,
I mean seeing the three characteristics, but you don't have to think too much about them,
it's good to understand them, but they're really just seeing things as they are and
as a result letting go of your delusions of things being stable, satisfying, controllable
me, mind, et cetera, give up all that delusion when you see clearly, I can't think
too much, should I try to attempt the original pajanya or do it in my nature thing?
I guess it depends what kind of mantra you're talking about, if you mean the mantra that
I talk about in the book, it should be in your native language for sure, but if you
mean a mantra like sambhi sattasu kitaon to or something like that, the pajanya can
be useful, English is still, or whatever you're on the native language, native English is also
fine, and I don't really encourage mantras outside of, no, that's not fair to say, things
like sambhi sattasu kitaon, may all beings be happy, but still use English, and the way
ideas probably is if you're really fluent in poly and understand what it means, it can be
good then.
Okay, the next questions are not about meditation, and even though the early ones were not
kind of, but we've run out of good questions, alright, what's up, people, we must have answered
all the good questions, and people are just, they know it all now, so we don't need to answer
anymore, we can just go, we can stop early, because we've done our job, that's great.
Alright, well let's look at some of the other questions that people have, they know all
the good answers already, what is the best way to live within capital and society, and
make and meet if we give up materialism, how do we get by?
Yeah, make me work, I have to think about this one, but there's nothing about mindfulness
that says you can make ends meet, not unless you go all the way and take it to its final
result, but at that point there's no concern in the mind about living in a capitalist
society or making ends meet, if you starve to death there's no concern about that, there's
no concern about anything anymore, that would be, you know that's really gone, you know
that's where you've, it's the equivalent of graduating from the universe, like if the
universe were something that to be understood, you've understood it, you've just, you're
so beyond that, you're, it's like you've, I mean it's just trying to explain, trying to
pass on how profound it is, like it's not something you have to ask yourself whether you
want or not, it's a very profound thing where you've had enough of everything, that's
a very hard thing to do, very rare thing, but apart from that everyone who practices
mindfulness has to make ends meet, has to have a livelihood, basically what I do is sort
of a livelihood as well, people feed me, so I teach, I teach, so people feed me, I'm
supported to live because I support other people or it's a mutual thing, so livelihood
in some sense is just that, it's mutual, you help other people, so they help you and helping
is a part of our life, doing work is a part of our life, because life can't exist and
can't continue without it, okay, give you specific advice, you just try to be mindful
and apply mindfulness to whatever it is you do, you'll do it better, probably get, probably
find it easier to make money, to live harder to get rich as both, but easier to live
because you're smarter, wiser, could more clear-headed, more level-headed, more patient, more
peaceful.
One day, the next question, I could not understand, but maybe you wouldn't, when I forget my doubts
about meditation, I understand it, or I simple, forget, no clue, I don't know, it's not
even a question, but I don't know what the question would be there, how do we investigate
the body using for elements, do we all just stop watching the stomach rising and falling
is the air element, when you walk you're aware of all the elements, three of them anyway,
you don't have to worry too much about the elements, you can understand that that's what
we're doing when we say rising, falling, we say stepping right, stepping left, we're
using the elements as an object of meditation, it's a bit misleading to say we investigate
the body, we don't really investigate anything, maybe it's a way of describing it, I don't
know, but you have to be careful not to fall into the idea that we're going to use a magnifying
glass and try to see things that we wouldn't normally see, we're just trying to put ourselves
there and observe, we're much more about observing than investigating, and we're much more
about cultivating clarity of mind than about worrying what we might see, hmm, oops,
oh, much meditation, a day isn't enough, well, it's hard to say, it depends, it's
where you're at in your practice, it depends how proficient you are at it, do some every
day, I think if you get proficient at it two hours a day is a good sort of base, it really
depends, you know, how much is enough, well really, there's no, there's no maximum, if you
really want enough you have to be doing it all day and all night, so the answer, the answering
question would be enough for what, enough to be enlightened tomorrow, enough to become enlightened
next week, enough to become enlightened next life, enough to eventually in some lifetime
become enlightened, there's different answers for each of those, and it's where it's
that adventure, you concept, how is it that no thing helps facilitate mindfulness rather
than error?
I don't use the word noting for this, because this sort of question comes up, I use the
word mantra because I think it helps people understand what exactly we're doing, as if I
asked you, do you not think mantra meditation brings the mind closer to the object, you'd
probably say, oh, yeah, well, that's true.
So what's the difference?
I mean, there really isn't a difference.
The technical answer to your question
is that while words are concepts, absolutely,
realities have names.
And our mind works in that way when we name something,
we associate the name with the object,
and concepts have names, and realities have names.
That's why there's two kinds of concepts.
One kind of concept is the name, and one kind of concept
is the object.
Namapagnati and R. Amenapagnati, I think, is the difference.
So Namapagnati is the name, but ultimate realities have names.
And so the important thing to avoid error
is not to worry about the name being a concept,
but to worry about the object, the thing that you're naming.
So if you say to yourself, cat, cat, cat, it's not
going to be an error.
It's just your object is not going to lead to enlightenment.
In fact, I think the whole question is a bit odd,
because leading to error, what do you mean error?
No, a word will always bring you closer to a name,
will always bring you closer to the object.
So it won't lead to error.
It'll either lead to samata, or lead to repassant,
or lead to tranquility, or lead to insight.
And that depends not on the usage of a name.
It depends on the thing that you're naming,
the thing that you're coming closer to.
The thing that you're focusing on, because when you use the name,
it will evoke the awareness of the object.
I mean, that's not theory.
That's what happens.
That's how mantra meditation works.
That's why it's been around for thousands of years.
That's why people in all religious traditions still use it,
because things have names.
It's meditation is better, samata, or vipassana.
Am I wasting my effort if I do one or the other?
Well, so samata meditation is different,
shame it because it doesn't lead to vipassana.
vipassana tends to have samata in it.
But the point is, and the differentiation is made,
because there are some meditations, as I just said,
wherein the object is not a reality.
And if the object is not real, if it's conceptual,
like a cat or the Buddha, then it doesn't
have the potential to lead you closer to insight.
It does the potential to calm the mind down,
which can be useful later for practicing insight.
So there are two paths to enlightenment.
One is where you practice samata first,
the meditation based on a concept.
And then you later use that power of mind to cultivate vipassana.
And the other is you start practicing both vipassana
with some sense of samata without the deep,
tranquilizing states that are involved with focusing on a conceptual
object.
And then the next few questions, I'm not really sure
about that, but allow them.
Can you awaken to blissful love through meditation?
If you practice that sort of meditation,
you probably could.
There's many different types of meditation.
It wouldn't last though.
The problem with that is it's not permanent.
So it will come, and then it will go.
And because it's associated with liking,
love is a sort of enjoyable state.
There will be what's craving for it.
And there will be disappointment when it goes.
That's why it's not the goal of Buddhism.
Not because it's a bad thing necessarily.
It's just not an ultimate goal.
What vipaka and vichara are?
How do you notice there is that what's
a difference about practicing with all without them?
There ain't got a pass on this one.
It's too technical.
I don't think the answer is all that useful.
I'm not sure if there are many good questions left.
All right, well, let's finish.
This sounds like today was everyone was satisfied.
Probably they were mindful, which is great.
We had a chance to meditate together.
We don't have to, it's not a bad thing.
We could just spend the last 10 minutes meditating together.
Let's just quietly sit here for another 10 minutes.
Thank you all, Sade, and we'll say that at the end.
Let's do 10 minutes now, and then at the end
we'll all say Sade, which is good.
Sade.
Sadhguru, thank you all, wish you all peace, happiness and freedom from suffering.
Thank you Shrada for your help, may the goodness that you've gained today be a benefit
to you as well.
Good night everyone.
