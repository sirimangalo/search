1
00:00:00,000 --> 00:00:21,800
Welcome, everyone, to our international celebration of the anniversary of Buddhism, really.

2
00:00:21,800 --> 00:00:45,800
This is the anniversary of the first day that the Buddha gave an official teaching, and the first day that there was someone who put into practice and came to realize the truth according to the Buddha's teaching.

3
00:00:45,800 --> 00:00:53,800
I'll talk a little bit about that after, but we should be aware that this is a very important day.

4
00:00:53,800 --> 00:01:00,800
It's also the day before the rainy season.

5
00:01:00,800 --> 00:01:12,800
In India they split the year up into three seasons, so this is the beginning of the wet season.

6
00:01:12,800 --> 00:01:28,800
This has become a tradition based on the conditions in India that this would be the time of year where we undertake intensive practice of the Buddha's teaching.

7
00:01:28,800 --> 00:01:35,800
Because when it's raining you won't move around as much, and so the monks are required to stay put.

8
00:01:35,800 --> 00:01:43,800
Starting tomorrow I won't be going anywhere for three months, not that I go anywhere anyway.

9
00:01:43,800 --> 00:01:49,800
The Buddha required that the monks stay put for three out of the four months of the rainy season.

10
00:01:49,800 --> 00:01:54,800
You can either do the first three months or the second three months.

11
00:01:54,800 --> 00:01:59,800
We'll be starting that tomorrow.

12
00:01:59,800 --> 00:02:08,800
This is an important landmark.

13
00:02:08,800 --> 00:02:13,800
It's a point in the beginning of the year in Buddhism.

14
00:02:13,800 --> 00:02:18,800
That's how we count the years by rain, rains.

15
00:02:18,800 --> 00:02:31,800
This will be a time for everyone to...it's an excuse for us to undertake reflection, especially since this is the time where practice of the Buddha's teaching first began.

16
00:02:31,800 --> 00:02:39,800
This is the time of year where these five monks came to listen to the Buddha's teaching,

17
00:02:39,800 --> 00:02:48,800
and then spent three months practicing it with the Buddha in India.

18
00:02:48,800 --> 00:02:59,800
For people who know about these things or for people who are interested in these things, this is an important day of the year in Buddhism.

19
00:02:59,800 --> 00:03:15,800
So, with that in mind, we have something of recognizing it today and that is we're going to take the refuges and precepts.

20
00:03:15,800 --> 00:03:28,800
We're going to commit ourselves to the practice of the Buddha's teaching and the abstention from those things which are detrimental for our practice.

21
00:03:28,800 --> 00:03:51,800
So, we have this ceremony and it's simply an observation, a re-affirmation in our minds of the intentions that we already have, reminding us of our intentions and of our path.

22
00:03:51,800 --> 00:04:01,800
So, the way it works is we have this text below and you can choose from either the five precepts or the eight precepts, but you have to know what you're getting yourself into.

23
00:04:01,800 --> 00:04:07,800
So, if you choose the eight precepts, it's really important that you actually intend to keep them.

24
00:04:07,800 --> 00:04:20,800
Not normally people would only keep these during periods of intensive meditation practice, but it is possible to keep them on a daily basis for certain people, depending on your lifestyle.

25
00:04:20,800 --> 00:04:26,800
But you should read through them and understand them first.

26
00:04:26,800 --> 00:04:42,800
If that's not feasible for you or you're not sure about the eight precepts, then generally we say a person who's interested in practicing these things is required to take the five precepts.

27
00:04:42,800 --> 00:04:55,800
It's not a institutional requirement, it's a requirement based on the reality of it, the reality of the mind.

28
00:04:55,800 --> 00:05:01,800
If you don't keep these things, your practice will not bring fruit and there's the requirement.

29
00:05:01,800 --> 00:05:09,800
If you don't keep these, there's no way you can really advance in the practice.

30
00:05:09,800 --> 00:05:11,800
It's quite difficult.

31
00:05:11,800 --> 00:05:23,800
There's many other things that make it difficult or could pose problems, but these five are singled out as quite extreme in their effect on the mind.

32
00:05:23,800 --> 00:05:34,800
They're basic morality, not to kill, not to steal, not to cheat, not to lie, and not to take intoxicants, drugs, and alcohol.

33
00:05:34,800 --> 00:05:50,800
I think for most people it would be just taking the five precepts, but you're welcome to take whatever precepts appear to you.

34
00:05:50,800 --> 00:06:07,800
Doable and pollinable that are appropriate for you.

35
00:06:07,800 --> 00:06:24,800
For me, for the whole ceremony, we'll do it piece by piece, and I'm recording this, so you'll be able to go over it again on the website and practice it for next time as well.

36
00:06:24,800 --> 00:06:50,800
So, without further ado, we'll get started unless someone has any questions or problems.

37
00:06:50,800 --> 00:06:58,800
Okay, well, if problems come up or if you have questions afterwards, you're welcome to ask them after.

38
00:06:58,800 --> 00:07:03,800
So, starting at the beginning, you can click on either the five precepts or the eight precepts.

39
00:07:03,800 --> 00:07:16,800
We're starting at what is the request, and I've changed the text a little bit from the original source because it's actually more appropriate to do it as one person.

40
00:07:16,800 --> 00:07:27,800
And the pollin originally said, we request the precepts, but you're doing it individually, and you don't know whether anyone else is taking the same precepts.

41
00:07:27,800 --> 00:07:32,800
So, we should all just do it individually and say, I am requesting these things.

42
00:07:32,800 --> 00:07:39,800
Especially since the rest of the ceremony already says, I do this, I do that, I undertake not to do this.

43
00:07:39,800 --> 00:07:46,800
It's all singular except for the request, so the request also should be singular, which I've done a change.

44
00:07:46,800 --> 00:07:48,800
So, repeat after me.

45
00:07:48,800 --> 00:07:54,800
Again, I'm going to be repeating the eight precepts.

46
00:07:54,800 --> 00:08:02,800
So, I will say, I will say, I will say, at the end there, for those of you taking the five precepts,

47
00:08:02,800 --> 00:08:12,800
you will say, Pancha, Silani, Yajami, and when we go to the precepts, they'll be a little bit different, and I'll walk you through that.

48
00:08:12,800 --> 00:08:20,800
The third precept is different for eight and for five, and I'll repeat both and have you repeat them after me depending on which set you're taking.

49
00:08:20,800 --> 00:08:30,800
The sixth, seventh, and eighth precepts are obviously, if you're taking the five precepts, just stay quiet during that time and wait for us to finish.

50
00:08:30,800 --> 00:08:33,800
If you're taking the eight precepts, just continue on with this.

51
00:08:33,800 --> 00:08:41,800
And one last thing is, in the top right corner of the text frame, there's a plus sign beside the word pronunciation.

52
00:08:41,800 --> 00:08:50,800
If you click on the plus, you get this little box that pops up, and it has pronunciation hints.

53
00:08:50,800 --> 00:09:00,800
So, try to keep those in mind because some of the letters are not going to be the same as they are in English.

54
00:09:00,800 --> 00:09:06,800
C is always chat, and the M with the dot under it is an ung.

55
00:09:06,800 --> 00:09:13,800
So, if you see an M with a dot under it, it's not an ung, it's ung, and G.

56
00:09:13,800 --> 00:09:16,800
Those are the two big ones.

57
00:09:16,800 --> 00:09:22,800
And the V is a W. A few things that you have to remember.

58
00:09:22,800 --> 00:09:25,800
Okay, so repeat after me.

59
00:09:25,800 --> 00:09:48,800
Thank you.

60
00:09:48,800 --> 00:09:58,800
Thank you.

61
00:09:58,800 --> 00:10:23,800
Thank you.

62
00:10:23,800 --> 00:10:43,800
Thank you.

63
00:10:43,800 --> 00:11:02,800
Thank you.

64
00:11:02,800 --> 00:11:21,800
Thank you.

65
00:11:21,800 --> 00:11:40,800
Thank you.

66
00:11:40,800 --> 00:11:59,800
Thank you.

67
00:11:59,800 --> 00:12:20,800
Thank you.

68
00:12:20,800 --> 00:12:45,800
Thank you.

69
00:12:45,800 --> 00:13:10,800
Thank you.

70
00:13:10,800 --> 00:13:35,800
Thank you.

71
00:13:35,800 --> 00:14:00,800
Thank you.

72
00:14:00,800 --> 00:14:25,800
Thank you.

73
00:14:25,800 --> 00:14:50,800
Thank you.

74
00:14:50,800 --> 00:15:15,800
Thank you.

75
00:15:15,800 --> 00:15:42,800
Thank you.

76
00:15:42,800 --> 00:16:07,800
Thank you.

77
00:16:07,800 --> 00:16:32,800
Thank you.

78
00:16:32,800 --> 00:16:57,800
Thank you.

79
00:16:57,800 --> 00:17:24,800
Thank you.

80
00:17:24,800 --> 00:17:50,800
Thank you.

81
00:17:50,800 --> 00:18:15,800
Thank you.

82
00:18:15,800 --> 00:18:40,800
Thank you.

83
00:18:40,800 --> 00:19:05,800
Thank you.

84
00:19:05,800 --> 00:19:30,800
Thank you.

85
00:19:30,800 --> 00:19:55,800
Thank you.

86
00:19:55,800 --> 00:20:21,800
Thank you.

