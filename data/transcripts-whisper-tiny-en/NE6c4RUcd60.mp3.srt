1
00:00:00,000 --> 00:00:05,280
Sometimes when I feel sleepy in the morning I do walking meditation before

2
00:00:05,280 --> 00:00:09,920
sitting. Do you recommend it for every session? How can I understand the best

3
00:00:09,920 --> 00:00:19,800
moment to attain to shift from walking to sitting? Yes, we recommend walking

4
00:00:19,800 --> 00:00:26,640
before every round of sitting a formal meditation. Now in daily life it's

5
00:00:26,640 --> 00:00:33,080
sometimes inconvenient and even undesirable to do walking each before each

6
00:00:33,080 --> 00:00:37,560
round potentially because you've been walking during the day you've been

7
00:00:37,560 --> 00:00:43,840
physically active or you're drained at the end of the day so off it can be the

8
00:00:43,840 --> 00:00:48,280
case that often the case that they'd rather just do sitting. On the other hand

9
00:00:48,280 --> 00:00:53,080
you'll find that sometimes walking is more more helpful for you after a long

10
00:00:53,080 --> 00:00:58,400
day of stress. Sometimes difficult to sit down and you find that walking

11
00:00:58,400 --> 00:01:02,960
allows you to calm down allows you to focus yourself. Center yourself before you

12
00:01:02,960 --> 00:01:10,040
have to sit and it sets yourself up for good meditation. Our rule of thumb is

13
00:01:10,040 --> 00:01:15,760
to do half walking and half sitting except for the in the case for an

14
00:01:15,760 --> 00:01:21,520
advanced meditator who is deeply involved in meditation eventually and this

15
00:01:21,520 --> 00:01:26,880
is for someone who's practicing intensively means all day potentially all

16
00:01:26,880 --> 00:01:35,720
night. They can come to a point where they decide for themselves to walking as

17
00:01:35,720 --> 00:01:41,720
long as they wish and then sitting. This is an art tradition. We almost

18
00:01:41,720 --> 00:01:47,280
always prefer to do half walking. Have sitting one good reason because

19
00:01:47,280 --> 00:01:52,760
otherwise you just follow your partial to one and so you do it and in

20
00:01:52,760 --> 00:01:58,040
fact that that is the wrong reason. That's the reason you should switch to be

21
00:01:58,040 --> 00:02:01,920
able to break and let go of the partiality. One thing to sit so you walk

22
00:02:01,920 --> 00:02:12,920
wanting to walk and so you sit and so the half and half thing equal equal

23
00:02:12,920 --> 00:02:19,920
izing. Doing them equal amounts helps to break that attachment.

