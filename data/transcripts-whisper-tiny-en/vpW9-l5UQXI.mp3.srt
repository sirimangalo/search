1
00:00:00,000 --> 00:00:05,120
Many people in the West believe there's nothing after death because there's no memory of

2
00:00:05,120 --> 00:00:07,400
what's before life.

3
00:00:07,400 --> 00:00:12,760
Is there a difference between the idea of nibana and this assumed nothingness after death?

4
00:00:12,760 --> 00:00:16,160
It's really too subject here.

5
00:00:16,160 --> 00:00:20,720
I've never actually heard that I can think of someone who believes there's nothing

6
00:00:20,720 --> 00:00:25,200
after death simply because there's no memory of what's before life.

7
00:00:25,200 --> 00:00:27,440
But it's a curious logic there.

8
00:00:27,440 --> 00:00:29,640
You can see the logic in it.

9
00:00:29,640 --> 00:00:41,160
It were true that consciousness starts at conception or birth or whatever you want to

10
00:00:41,160 --> 00:00:47,920
believe, then doesn't it make sense that it would end at death?

11
00:00:47,920 --> 00:00:56,480
There's still not much logic to that because I guess you would necessitate the question

12
00:00:56,480 --> 00:01:01,040
of if there were something after, where would all the consciousnesses that were created

13
00:01:01,040 --> 00:01:03,640
at birth go?

14
00:01:03,640 --> 00:01:07,120
Anyway, that's not your question.

15
00:01:07,120 --> 00:01:09,040
It's an interesting logic.

16
00:01:09,040 --> 00:01:13,720
I didn't realize that many people had that belief.

17
00:01:13,720 --> 00:01:19,640
But the question is whether there's difference between the idea of nibana and an assumed

18
00:01:19,640 --> 00:01:29,640
nothingness, which is assumed to occur at physical death.

19
00:01:29,640 --> 00:01:30,640
It's interesting.

20
00:01:30,640 --> 00:01:33,440
It's an interesting question that you catch me in because you want to write away saying

21
00:01:33,440 --> 00:01:36,960
no, there's nothing different, but it's not true.

22
00:01:36,960 --> 00:01:42,720
It is and it isn't, but you're dealing with two different views of reality.

23
00:01:42,720 --> 00:01:50,440
nibana is based on experience and physical death is based on materialism.

24
00:01:50,440 --> 00:01:55,760
We don't believe in physical death because we don't subscribe to materialism.

25
00:01:55,760 --> 00:02:00,600
We believe in experience that is arising and ceasing from moment to moment.

26
00:02:00,600 --> 00:02:09,920
Nibana is the cessation of that experience, and the cessation of the five aggregates

27
00:02:09,920 --> 00:02:12,640
to the arising of the five aggregates.

28
00:02:12,640 --> 00:02:17,640
There's no more when there's no more arising of that.

29
00:02:17,640 --> 00:02:19,600
It sounds a lot like death.

30
00:02:19,600 --> 00:02:24,840
It sounds a lot like what you would expect if you were a materialist to occur when you

31
00:02:24,840 --> 00:02:38,920
die, but maybe it's just a semantic difference or maybe it's a trivial difference, but

32
00:02:38,920 --> 00:02:43,000
there's a different supposition there because what you're thinking of something having

33
00:02:43,000 --> 00:02:52,400
died, so the idea that death there is nothing, is the idea that there was a being that

34
00:02:52,400 --> 00:03:00,920
has ceased, which is why I thought of sort of there being a distinction, but conceptually

35
00:03:00,920 --> 00:03:08,560
there's not much difference, but by making the distinction, the idea was to force you

36
00:03:08,560 --> 00:03:13,880
to look at it a little differently and stop thinking in terms of I'm, it's the cessation

37
00:03:13,880 --> 00:03:21,240
of me, like Nibana is the death of me, the death of my experience or something like that.

38
00:03:21,240 --> 00:03:28,360
Nibana is an experience that can be realized for a single moment, can be realized for

39
00:03:28,360 --> 00:03:36,800
five minutes, for ten minutes, up to seven days, and still come back, still still be followed

40
00:03:36,800 --> 00:03:45,000
by experience of the five aggregates, so Nibana is not just an experience that causes the

41
00:03:45,000 --> 00:03:51,400
cessation of a stream of consciousness, or the permanent cessation of a stream of consciousness,

42
00:03:51,400 --> 00:03:59,920
it's an experience of cessation, that's what Nibana is, so it's what people understand

43
00:03:59,920 --> 00:04:08,320
right away is what you talk about, there's death, but it's inaccurate because that's dealing

44
00:04:08,320 --> 00:04:17,120
with a different set of beliefs and views from Buddhist beliefs and views or Buddhist

45
00:04:17,120 --> 00:04:24,080
theory and doctrine, which is experience in the cessation of experience, and so just want

46
00:04:24,080 --> 00:04:34,840
to say that it's a little bit deeper than just being some kind of death.

