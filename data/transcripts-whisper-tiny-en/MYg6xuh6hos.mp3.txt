Good evening, broadcasting live from British Columbia and wherever Robman is from
Meghan, I always forget. Not messages, it's a Connecticut. August 15th, 2015, 2015.
Yeah. So today we had the first day of our meditation course. Last night I
gave a talk to a fairly small group and they recorded it for YouTube but that
didn't go very well and the audio was pretty awful so I can probably pull it up
here. Let me see if I can show you it. I don't think you could hear it, but...
I'll show you a picture there do you see me do you see that yes yeah actually it
looks really good. The picture was fine. First problem was he had this
camera set to take 30-minute shots. After 30 minutes it cut out and he had to
start it again.
The funny thing was there was some sort of filter on it that my voice was
fine when I spoke but when I wasn't speaking it was a very loud hissing so if
someone out there would like to take the time to remove the hissing from the
silent parts of the audio that would be about it. If you can do that without
hurting the actual speech then we could post it but I think the really the best
way to do that would be to go through the one hour long talk and manually silence
each his could probably take some time. It's cut up into half hour chunks so
someone could take half and another person could take half to see if we get
any volunteers for that. We have our volunteer group that will be meeting on
Sunday nights starting next Sunday so maybe we'll give volunteers for things
like that. Why it was a pretty good talk. I mean it went over quite well and
I didn't forget myself or get lost or anything covered all the points. It's a
talk I've given before so I'm pretty sure it's already on YouTube a couple of
times but it was a good version of it was a talk about the five types of people
get there being a dummy hurry something to talk about often. So tonight we have a
quote and we have only a few people logged in what's happening there everyone
still meditating. So this quote is about criticism criticizing and praise about
passing judgment. It's about the debate over action and inaction and the
Buddha seems to have overwhelmingly favored action over inaction. Now I'm
not sure why whether that was just because of his own mission which he had
started on because originally he inclined to inaction. So it may be that his
exhortation for his students to incline towards action was just a part of his
own intention once he had made up his mind to teach that therefore it was
proper for his students to incline towards action because this is a question
of whether it's good to stay upon miss and not criticize or praise others not
to get involved and give your opinion whether that is more praised worthy than
then the effective change. I think most of us would agree with the Buddha here
unconditionally but I think we do that out of a sense of a desire to see
things change to see things get better which I think the Buddha pretty clearly
didn't have. Buddha had his mission but it was it was out of invitation and it
was a part of him being the Buddha. So a lot of our hands became guilty of of
inaction and it seems as though that was a viable behavior for an
arahan. I mean in some to some extent an arahan doesn't concern about whether
it's right or wrong. They don't have a sense of feeling guilty or embarrassed
that they're not helping that they're not changing the world. They don't worry
or feel sad because the world is a mess or because people are suffering. They
don't have any of that. If they did and then how can you say that they are
from how can you say that they're unattached that they have given up
craving. I think a lot of people take that the wrong way as well and try to
emulate the arahans in this regard and and become lazy and do nothing. The
reason an arahan is is of that bend is because they've done everything they've
done all that needs to be. For the rest of us it could be considered laziness to
do that because all the good things helping others and so on is work in the
right direction. It's a support for our practice. It surrounds us with good people
and keeps our minds happy and confident and inclined towards happiness and
peace. But for an arahan they have no goal left no destination after
each. And yet the Buddha seems to be pushing for even such pains to act. And in
this is subtle he's not giving an injunction here but he's giving his opinion
that it is more admirable. And it's his name Bodalya. Bodalya uses the word
Upeek. He translates here as indifference, but Upeek means literally or he
usually translated as equanimity. It's used for the fourth Brahmuihaar among other
things. But many types of equanimity and so he's interpreting this one to
mean indifference. But in the really means equanimity and only how Bodalya
meant it. He didn't mean that the guy was indifferent. Someone like that is
equanimous. They're undisturbed by good people or bad people. Another way of
looking at this is the potential idea is what is the path of least
for distance. What would an arahan really do? And I don't think it's clear to
say that an arahan would incline towards inaction in this case. If confronted
at the right time with an opportunity to criticize that which is worthy of
criticism or to praise that which is worthy of praise. It's not clear that they
wouldn't incline towards inaction. It seems to me actually quite reasonable
to suggest that they would speak when they have the opportunity. Sometimes
they didn't. There was the case. If you read the Vinay, there's interesting
cases, one of which is regards to the third council. There were many
monks who were breaking rules. Wait, is it second council? I think second
council. Second council. Many monks were breaking rules and they went to this
very senior monk who was a believer in arahan and he didn't say anything. He
wouldn't give a judgment. I think the story goes that they were waiting for
the judgment of the chief or the most senior monk or our monk who was an
expert in the Vinay, so that's why they didn't answer. But there certainly
seem to be cases where they incline towards inaction. Anyway, certainly as
followers of the Buddha and out of respect for his teaching, respect for the
fact that he took it upon himself to teach and made it a mission to spread
the dhamma to give to share. This is because criticism and praise is an
important part of any religious spiritual teaching. The establishment
establishing where we stand, where do we stand on these issues? Where do we stand
on things like abortion? Where do we stand on homosexuality? Where do we stand on
racism or I don't know any of these things? So praising and criticizing, we
don't criticize homosexuality. We make criticize sexuality in general. We criticize
abortion and that sort of thing. And drugs, alcohol, criticizing these things.
More importantly, most importantly, we criticize unhosenness and we praise
unhosenness. If the mind has a wholesome quality to it, we praise that. A person
who is inclined towards wholesomeness, we praise the wholesomeness. I think an
interesting question is about whether you praise the act or you praise the
person. If this hate the sin or not, hate the sin, not the sinner is a Christian
common Christian praise. So I think that's an interesting thing too. I mean, it's
fairly obvious that Buddhism would be very much of that balance. We don't
exactly criticize people, but that's important, I think, because it can never
be more important than Buddhism especially because you can never say that a
person is good or a person is evil. Because people don't exist. And so we have
to be very careful when we praise that we don't praise people. It would be in
proper to do so. Not only dangerous, it is dangerous to do so, but in proper. And
maybe dangerous because it's improper. If you praise someone, a person, then you
have to own up to their evil deeds. If they then break from their trend of
being a good person, which is completely possible, anytime they do something
wrong, you get stuck because you praise that person. So praising people is
something that can be that it is a great danger. And we should be careful to
be clear what we are praising, which is why one reason probably why this says
praise that, which is praise or it doesn't talk about beings. Praise the acts,
we praise the mind states, we criticize acts in mind state. There's also much
safer in the sense that you don't make enemies and you don't people don't
say, we'll put you on the side. And politics, for example, right now there's an
election here in Canada, there's an election in America. I mean, there's always an
election because it's become a circus and they just like to have it always on
the news. Right after this election, over people will be talking about the
next one. It's how we, it's part of entertainment in the West, I think, but it's
a bit interesting sometimes because it deals with issues of morality and
people are as are forced to ask themselves where they stand on issue, like
abortion, homosexuality. Right now, a big one is is police brutality and racism.
Racism is a funny word just to get off track here, but to talk about things
that are worth criticizing or worth praising because from Buddhist point of
we were all one race, right? It's funny because we all know we are one race and
everyone knows that. And so the idea of racism, I don't know if I want to really
get into it because it's it's much more than just the word race, but it's
been misleading. We're none of us white or black or brown.
I guess we're a little bit pink. I think I'm actually on the number ones
one called the kind of a black jot. And it's a wonderful jot. If you read this
number 440, you can find it on sacred and dash texts dot org. You can find the
whole jot. Because if you read number 440, it's a wonderful jot. But it's about
some guy who was very dark skin. The body said to had black skin or dark skin.
And the king is a king down black. And he said, I'm not black. Black is evil.
Black is a cusada. It's unwholesome. But it's more than a very beautiful
conversation that he has with the king of the gods. But racism isn't about that.
The issue of racism is people are very different. And don't respect
differences. And much more than that are openly cruel and hostile and evil
towards people because of the color of their skin, because of their culture,
et cetera, which of course Buddhism would criticize. So, but my point was
about elections. It's very interesting to talk about the issues and to look at
them. But I don't think you could ever say you should support one person or
another in an election. I mean, obviously, as people we do and voting isn't
wrong as a Buddhist, I don't think, because you're choosing, right? You have to
choose between a sentence or you choose. Doesn't mean that you support
everything that person's status or you believe in it. Absolutely. But from a
monastic point of view, there isn't, there is a point there to be made.
That monastic stand that we take and should take is to never endorse one candidate
to talk about the issues, to praise acts in the speech.
The person says this, this great thing. If you endorse one, then you make
enemies with the other. And monks are not in the position to do that. We're not in
society. We're not able to take the Buddha as, Buddha as our example. In order to
be an argument with a monk about this one, he said that we shouldn't take
signs on issues. He was talking about the Middle East. We should take
signs. That's how else can the number be effective. And that's exactly how it's
effective, because it doesn't take signs. And it teaches what is right, what it
is true. It is an outsider. By not taking sides, by not getting involved, we're
able to teach the truth. We're able to teach objectively. And without getting
caught up, and letting our emotions take over. Anyway, there isn't that much to
this quote, I don't think. It's an interesting one. Interesting for us to learn.
But I've been teaching all day since eight o'clock. And you wouldn't believe I
had 40 people. Today's the course we had 40 people. I think maybe they didn't
know Shavat, but there were 40 people signed up for the course. Probably we had
a little bit less than that. But they were, I had a room off to the side and in
and out all day, people were coming in to see me all day, all day. I met with
at least 40 people. Some people came in twice three times asking questions and
all sorts of questions. And that's the best. That's really the best form of
teaching that I know of, because it's one-on-one. It's personal. You get to
act. You get to take the role of a therapist or a psychologist. But by
teaching Buddhism, you have one and one percent confidence and defectiveness. And you
really can change people's lives. So that's awesome. So today was a good day. We
have one more day tomorrow. Are there any questions tonight? There are some
questions. Well, you see, I'm not updating. This page doesn't always update. So
I see nothing there. How do you interpret differences among countries from
Buddhist point of view? Well, habit, really. It's become cult. In every
country, habit, it's a glance towards a certain habit in general. And due to
isolation, habits only catch on in in one specific culture. Now that's
changing now, right? Habits become international. So culture becomes
international, but the internet culture. And then once they become culture,
then views mix in like we are I am Canadian. And we did this connection.
We did with the live stream. Let me know there started it again. It shows
right there. I should look at that. But culture becomes views. And then it's
like I am Canadian. I am American. I am French. I am British. I am German. And
then it follows you from life to life. If I would assume people are reborn in
the same country. And so it becomes incredibly ingrained at any time. But views
reinforce culture. Then religion. I believe that something is right. There's the
thought and there's the behavior. But then there's the affirmation that this is
right and proper in me and mine. But in the end, it's all just habits. So
it's habits and views. So your chances are, you could also say it's craving.
It's attachment. Like you're sure there is that when you when if you know a
French person eats a with a crepe and gets pleasure from it. And a British
person eats a whatever British people in English muffin. Pudding with the British
biscuits. Let's get tea. Whatever I'd biscuit and gets pleasure about it. Pleasure
from it. Then the one person's brain is wired for craves and the other
person's brain is wired for. I mean, cultural food is wired for a specific type of
food. And then they don't have that connection with the other types of food,
with other cultural types of food, not in the same way. And music. So becomes
very much ingrained. It's really a human. We would say humans are that way,
being the state of being a human is just an ingrained habit. Being a specific
type of animal is just a habit. That's become so terribly ingrained that it
becomes an identification. I am human. I am dog. I am this. So we're born again
and again the same way. See, that's where mutation comes from from deviants.
Mediviants isn't rewarded by the group. So for humans, we stay very much
fairly much the same because we attach to this state. So when someone is
different, different color skin, different height, different weight, you know,
any sort of mutation is immediately look down.
It's not really an invitation question. No, isn't. It's not exactly. It's
been interesting. Sorry, rather than I interrupted you.
Oh, but you actually were answering what I was going to ask about. So yeah, that's
just so interesting that you're more likely to be reborn in in the same
culture that you're in. Not necessarily advantageous for those of us in the
West where there are so few Buddhists here that might be more advantageous to
be born elsewhere, maybe.
There are benefits to be being born in a free country. Many Buddhist countries have had
to have their share. It's a good point. Ready for another question? I'm new to
Buddhism and I don't know where to start learning more about the religion. Is
there a book I can read that will teach me about the core fundamentals of
Buddhism? Well, there's the book with the Buddha Todd, which we always
recommend. I'm not sure how much I recommend it in the end, but it's pretty
good. You can always read my book, let me just start meditating.
Good. Come meditate with us. So I started. I went for a month's meditation course,
not knowing hardly anything about Buddhism, certainly nothing about
teravada Buddhism. And after that month I was hooked and I went and read
everything. Started reading directly the Buddha's teaching. I read the Visudhi
manga, not all of it, but much because I had gone to practice meditation first.
Meditation opened up a whole new world. So what's Jataka? Was it? No.
Let's go see. I mean, it had the word kana in it, so that's a problem.
And after that, I didn't think it was 444. Maybe it wasn't.
No, 440 kana in Jataka. That was right. The black Jataka.
Kanho, what a young person, so black indeed is this man. Kanhang Boonjati Boonjana,
and black is the food that he eats. Kanhang Boonjana, but they
sesame not my Hang Manasopio in a black place, in a black realm, does he
have a black realm and country? Not my Hang Manasopio. Our mind is not, my mind is
not dear. He is not dear, but he is not dear to my mind. This is the King of the God's
talking. He's a terrible racist, but it's a wonderful story because he really
sets him straight.
So you can find them there, but they're at sacred-texts.org.
Thank you.
I got them from all 657 and 547. Behold, the Hang Man, all black of hue that dwells
on this black spot. Black is the meat that he does eat. My spirit likes him not.
And then he replies, the black of hue, a brahmin true at heart, Osaka sea, not by the
skin, but if he sinned, then black of man must be. Fair spoken brahmin, nobody put
the most excellently said, choose what you will as bids your heart, so let your choice
be made.
That's not quite right.
Saka the Lord of all the world, a choice of blessings gained, from malice hatred, covetous,
ignorance, I would hate, and to be free from every lust, these blessings for I crave.
You have to read the whole job together. Very much worth reading. One of my favorites.
When you have two different processes going on during meditation, like feeling pain and
feeling an itch at the same time, how should you note this? Pick whichever one is clearest.
That's all. It's not that important. As long as you pick one, then try to find the one
that's clear.
What do you explain how to relate the four foundations with the meditation practice?
This is the second.
How to relate the four foundations with the meditation practice?
I don't understand. Have you read my booklet?
If you haven't, please read my booklet. Sounds like it might answer your question.
Thank you for the link there for the sacred texts.
What are the benefits of chanting? What chant would you recommend to recite daily?
I wouldn't really. I guess you need a chant, right?
It's a chant. You should do it. You should do it. You should do it. You should do it.
You can knock them up. A recollection of the Buddha, a recollection of the Dhamma, a recollection
of the song. That was every Buddhist, I think. You should know.
What even said you should know them? You should chant them whenever you're afraid because
they would free you from fear to spell the fear.
Or you could come to our poly group and take refuge in poly at the beginning of the class.
We do that every Sunday. It's nice.
One day, is it best to try to relocate to Sri Lanka or Thailand during our toilet years to
pick up on the path again? Or is it a roll of the dice? That's what I was thinking.
Try to shift the odds there to be in a more Buddhist environment.
I guess Sri Lanka is becoming a better place. I definitely recommend Sri Lanka.
And I hear it's a better place now, even more because of the stability of the government.
I mean, the government has problems, but where doesn't it?
But in Sri Lanka, there is peace now. There's no more civil war.
So that always makes life easier. And it's a great culture for practice.
So yeah, I think that could potentially be a good thing.
The thing about Sri Lanka is that from what I see, among even a foreigner, potentially could just get lost.
Even if you're a layperson, if you're a layperson who has like a pension and living in Sri Lanka would be easy.
The problems are dengue, really. I would say the one big problem with Sri Lanka is it's got a lot of dengue, dengue problem.
So you have to be aware that there's a potential to get a fairly problematic sickness.
I don't know, Sri Lankan people would like to hear me say that because they all come out.
But I got in when I was there. Of course, I lived in a living with mosquitoes.
But if you want to live in the forest of Sri Lanka, there's going to be mosquitoes and potentially dengue.
Quite a lot of dengue there.
As for others.
Sorry. Hello. Sorry.
Hi, you're back.
Yeah.
Panty does Sri Lanka welcome immigrants. Yeah. Yeah, and more so especially now.
There's a big, in a big tourism push, I think, or I think, I'm actually not sure.
Immigrants, like people migrating there.
Yes, you know, we were asking about going to live in Sri Lanka, you know, in later years to pick up on the culture.
Yeah, well, you don't have to become a citizen. I think becoming a citizen is pretty difficult.
But going to live there on a non-immigrant, or I don't know, a resident visa, I think that's too long.
Probably a little expensive to get the visa a bit. Once you got it, you live there.
I guess I don't know whether it would be any better than living here as a, as a retiree.
Doing, doing the dhamma, it certainly would be more comfortable here culturally.
You know, the food here would be more suitable. If you can find a place in your own own land,
it's often better just for the car.
And that's important, you know, if you have the wrong type of food, everybody's been used to
a certain type of food, and then you have to adjust to a whole new type of food.
Sri Lankan food is very salty. It's very healthy. It's very good food, but it's very salty.
It's just different, you know, for all the people, I think that's more difficult,
because your body's been used to something for a longer time.
When I meditate for long, like for one hour, I used to get terrible pain in the knee,
which does not go off, even though I become mindful as pain, pain. It was unbearable.
I still tried to become without getting frustrated. Would you please advise on this?
Well, if you get frustrated, then you focus on the frustration. If you don't like it,
then say, dislike it. If it's really unbearable,
then you just find where you're going to win in one sitting. So you back off, retreat,
move your legs out, say moving, moving, or get up and walk, or lie down. It's unbearable,
just change. Don't have to try to become. The calm will come naturally once you understand
the situation and you no longer react to it, but that will come by itself. All you have to do
is see clearly watch and see. Be patient, you'll get there.
Okay, I'm going to cut it off there because it's been a long day. Thank you all for tuning in.
Good to see you all again. Beyond again, once more tomorrow and then Monday, I'm probably not here.
I don't know, wait, it's a second. Let's see, schedule Monday. Anyway, tomorrow I'm here.
I mean, it doesn't tell me. I think I might be back in time.
Yeah, we'll probably be back in time. We'll see Monday, I might actually be able to do it.
Anyway, I'll see you all tomorrow. Good night.
Thank you, Monday. Thank you, Raman. Take care.
