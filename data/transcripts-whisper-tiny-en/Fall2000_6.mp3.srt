1
00:00:00,000 --> 00:00:13,760
Yesterday we stopped at the story of Sona.

2
00:00:13,760 --> 00:00:27,880
Now you all know that these five faculties should be balanced and when one of them is strong

3
00:00:27,880 --> 00:00:35,360
that is when one of them is stronger than the others, then the others cannot perform their

4
00:00:35,360 --> 00:00:38,240
respective functions.

5
00:00:38,240 --> 00:00:50,720
So when there is too much sadha of faith, the other faculties cannot perform their respective

6
00:00:50,720 --> 00:01:00,960
functions and that was shown by the story of workily.

7
00:01:00,960 --> 00:01:12,640
Now when the energy faculty is strong, that means stronger than the other faculties,

8
00:01:12,640 --> 00:01:22,200
then the other faculties beginning with faith or confidence cannot perform their respective

9
00:01:22,200 --> 00:01:24,200
functions.

10
00:01:24,200 --> 00:01:43,440
And in this case the commentary pointed out to us the story of Sona the elder Sona.

11
00:01:43,440 --> 00:01:53,320
Sona was a man from the city of Champa and it is said that his body was so delicate

12
00:01:53,320 --> 00:02:02,640
that he had hairs on the soles of his feet.

13
00:02:02,640 --> 00:02:18,400
One day he was invited to Rajagaha by King Bimbisara and after talking with him, King Bimbisara

14
00:02:18,400 --> 00:02:30,800
sent him along with other many families to the Buddha to listen to the Dhamma talk

15
00:02:30,800 --> 00:02:33,800
of the Buddha.

16
00:02:33,800 --> 00:02:41,920
After listening to the Dhamma talk of the Buddha, Sona came to the conclusion that it

17
00:02:41,920 --> 00:02:50,880
would be very difficult for him to practice the Dhamma to the full extent taught by the

18
00:02:50,880 --> 00:02:55,720
Buddha if he remained a householder.

19
00:02:55,720 --> 00:03:03,000
So he decided to become a monk, he decided to do, decided to join the order.

20
00:03:03,000 --> 00:03:12,960
So he requested ordination from the Buddha and he was duly ordained.

21
00:03:12,960 --> 00:03:22,160
So after his ordination, he went to a place near the city of Rajagaha and practiced

22
00:03:22,160 --> 00:03:25,440
meditation there.

23
00:03:25,440 --> 00:03:40,640
He, when he practiced meditation, he make much effort, actually too much effort.

24
00:03:40,640 --> 00:03:49,320
Now he practiced meditation walking up and down and he walked up and down so vigorously

25
00:03:49,320 --> 00:03:54,200
that he had blisters on the soles of his feet.

26
00:03:54,200 --> 00:04:01,560
And then the place where he walked up and down was smeared with blood.

27
00:04:01,560 --> 00:04:13,240
But however much he made effort, he was unable to reach any attainment.

28
00:04:13,240 --> 00:04:21,760
So he got tired and then he sat down, maybe on the rock near the place where he walked

29
00:04:21,760 --> 00:04:23,880
and then thought.

30
00:04:23,880 --> 00:04:33,240
I am one of those who make effort, who are energetic.

31
00:04:33,240 --> 00:04:37,960
But yet I am not able to reach any attainment.

32
00:04:37,960 --> 00:04:47,240
But if I go back to lay life, enjoy the pleasures of lay life and at the same time do

33
00:04:47,240 --> 00:04:48,960
meritorious deeds.

34
00:04:48,960 --> 00:04:52,360
See he was thinking that way.

35
00:04:52,360 --> 00:04:56,160
Now the Buddha knew his thoughts.

36
00:04:56,160 --> 00:05:07,480
So Buddha went to him and gave advice to him.

37
00:05:07,480 --> 00:05:17,280
So Buddha went to where this where Sona was and sat on his seat prepared for him.

38
00:05:17,280 --> 00:05:25,040
Now it is said that when monks practiced meditation during the time of the Buddha, they

39
00:05:25,040 --> 00:05:33,320
always had a seat ready in case Buddha might come to them and help them.

40
00:05:33,320 --> 00:05:43,560
So Buddha went to him and sat on the seat and then said to him, Sona, why you are not

41
00:05:43,560 --> 00:05:51,160
thinking like this, that you are going to go back to lay life and so on, he said yes.

42
00:05:51,160 --> 00:05:58,840
Then Buddha said, Sona, when you were a lay person, why you are not scared in playing

43
00:05:58,840 --> 00:06:07,940
the loot or playing the harp, he said yes, then Buddha asked him, if the strings of the

44
00:06:07,940 --> 00:06:19,640
loot were too tight, can the loot have good sound and can you play it easily?

45
00:06:19,640 --> 00:06:22,280
He said no.

46
00:06:22,280 --> 00:06:32,880
Then if the strings are too loose, can it have good sound and can you play it easily?

47
00:06:32,880 --> 00:06:34,800
He said no.

48
00:06:34,800 --> 00:06:46,680
But when the strings are neither too tight, not too loose and well tuned, can you

49
00:06:46,680 --> 00:06:51,520
play it easily and will it have good sound?

50
00:06:51,520 --> 00:06:53,960
He said yes.

51
00:06:53,960 --> 00:07:04,780
Then Buddha said, similarly, if energy is applied too strongly, it will lead to the

52
00:07:04,780 --> 00:07:07,940
restlessness.

53
00:07:07,940 --> 00:07:14,220
And if energy is too lax, it will lead to lessitude.

54
00:07:14,220 --> 00:07:23,760
Therefore, Sona, keep your energy in balance with concentration, penetrate to a balance of

55
00:07:23,760 --> 00:07:34,500
the spiritual faculties and therein sees your object, that is what Buddha said to Sona.

56
00:07:34,500 --> 00:07:50,580
So I want you to remember this statement by the Buddha and it is on the sheet.

57
00:07:50,580 --> 00:08:02,420
So advice given to Sona is that too much energy leads to restlessness or agitation and

58
00:08:02,420 --> 00:08:08,460
too little energy leads to laziness.

59
00:08:08,460 --> 00:08:22,300
So the practitioners of meditation should understand this and act according to this advice.

60
00:08:22,300 --> 00:08:29,620
So if you make too much effort, you become agitated, you become restless and you find you

61
00:08:29,620 --> 00:08:32,180
cannot concentrate.

62
00:08:32,180 --> 00:08:38,820
And if you make too little effort, you become lazy or you become sleepy, you may dose off

63
00:08:38,820 --> 00:08:42,060
and so there can be no concentration.

64
00:08:42,060 --> 00:08:46,900
So Buddha said, keep your energy in balance with concentration.

65
00:08:46,900 --> 00:08:56,180
So energy and concentration must be balance, penetrate to a balance of the spiritual

66
00:08:56,180 --> 00:09:05,660
faculties, that means just try to have a balance of these faculties.

67
00:09:05,660 --> 00:09:15,140
Let them be balanced, all these five controlling faculties.

68
00:09:15,140 --> 00:09:23,460
And therein sees your object means just practice the Matam meditation or Vipasana meditation.

69
00:09:23,460 --> 00:09:30,420
So when you practice meditation, whether it is a Matam or Vipasana, these faculties should

70
00:09:30,420 --> 00:09:33,340
be balanced.

71
00:09:33,340 --> 00:09:48,060
Not one of them must be too strong so that the others cannot do their respective functions.

72
00:09:48,060 --> 00:09:58,140
So when we practice meditation, we must be reminded of this statement so that we do not

73
00:09:58,140 --> 00:10:05,260
make too much effort and at the same time, we do not make too little effort.

74
00:10:05,260 --> 00:10:11,900
And if we make too much effort, then we become agitated and we cannot concentrate.

75
00:10:11,900 --> 00:10:19,260
And so in that case, we have to reduce the effort or take it easy.

76
00:10:19,260 --> 00:10:27,260
Sometimes we want to achieve a great deal or we are worried about not getting results.

77
00:10:27,260 --> 00:10:35,700
And so we make more effort than is needed and the result is the opposite of what we want.

78
00:10:35,700 --> 00:10:40,780
We want to get concentration, we want to have a good practice.

79
00:10:40,780 --> 00:10:45,860
But the result is no concentration, no good practice.

80
00:10:45,860 --> 00:10:53,340
So in that case, you have to slow down and practice tranquility.

81
00:10:53,340 --> 00:11:05,620
So the eldest sauna took the advice of the Buddha and he performed the balance of energy

82
00:11:05,620 --> 00:11:17,700
with concentration. And so he practiced and before long, he reached a relationship.

83
00:11:17,700 --> 00:11:24,900
We can effort.

84
00:11:24,900 --> 00:11:27,820
We can add one more story.

85
00:11:27,820 --> 00:11:31,740
That is the story of the venerable Ananda.

86
00:11:31,740 --> 00:11:42,340
You all know that the venerable Ananda was the personal attendant of the Buddha for 25 years.

87
00:11:42,340 --> 00:11:48,820
And during the first 20 years, Buddha had no permanent attendant.

88
00:11:48,820 --> 00:11:58,300
But from the 26th year onward, the venerable Ananda was appointed the permanent personal

89
00:11:58,300 --> 00:12:01,060
attendant of the Buddha.

90
00:12:01,060 --> 00:12:08,820
And at that time, the venerable Ananda had become a sortapana.

91
00:12:08,820 --> 00:12:15,700
But he did not reach any further stages of enlightenment, although he was attending on

92
00:12:15,700 --> 00:12:25,060
the Buddha, although he was very close to the Buddha for 25 years, because he was occupied

93
00:12:25,060 --> 00:12:31,300
with how to keep the Buddha in good health and so on.

94
00:12:31,300 --> 00:12:43,700
And so he did not attain any higher stages of enlightenment during the life of the Buddha.

95
00:12:43,700 --> 00:12:51,740
After the death of the Buddha, the venerable Mahakasabha decided to hold the first Buddhist

96
00:12:51,740 --> 00:12:58,940
counsel. And 500 monks were to participate in that counsel.

97
00:12:58,940 --> 00:13:03,860
And the venerable Mahakasabha was to be the president of that counsel.

98
00:13:03,860 --> 00:13:15,500
So venerable Mahakasabha selected 499 arhans to participate in the counsel.

99
00:13:15,500 --> 00:13:25,620
And he left one for venerable Ananda. He was unable to select venerable Ananda because

100
00:13:25,620 --> 00:13:29,020
he was not an arhant.

101
00:13:29,020 --> 00:13:38,460
And it is difficult for him not to select venerable Ananda at all because it was venerable

102
00:13:38,460 --> 00:13:46,220
Ananda who had memorized all that they were taught by the Buddha.

103
00:13:46,220 --> 00:13:53,460
So he just left it to the discretion of other monks.

104
00:13:53,460 --> 00:14:08,540
Later all the other monks requested venerable Mahakasabha to select venerable Ananda to be present

105
00:14:08,540 --> 00:14:12,220
at the first Buddhist counsel and so he was elected.

106
00:14:12,220 --> 00:14:19,660
But he was still he sought up Ananda at that time.

107
00:14:19,660 --> 00:14:28,220
And the day for the counsel was drawing near some monks said referring to the venerable

108
00:14:28,220 --> 00:14:29,220
Ananda.

109
00:14:29,220 --> 00:14:42,060
They said among us there is one monk who goes about putting out the smell of raw flesh.

110
00:14:42,060 --> 00:14:51,580
That means Ananda still had mental defilements and so these mental defilements had a kind

111
00:14:51,580 --> 00:14:55,580
of smell of raw flesh.

112
00:14:55,580 --> 00:15:00,580
Now venerable Ananda knew that it was directed to him.

113
00:15:00,580 --> 00:15:04,500
So he was moved.

114
00:15:04,500 --> 00:15:12,140
So on the night before the counsel was to take place he was practicing meditation.

115
00:15:12,140 --> 00:15:21,620
You can imagine how eager he would be to become an arhant because tomorrow there is going

116
00:15:21,620 --> 00:15:29,420
to be the counsel and he thought to himself that it is not proper for him to attend the

117
00:15:29,420 --> 00:15:35,060
counsel while he is a sort of Ananda and not an arhant.

118
00:15:35,060 --> 00:15:44,060
So he practiced the whole night walking up and down but he made too much effort.

119
00:15:44,060 --> 00:15:46,700
So he couldn't achieve anything.

120
00:15:46,700 --> 00:15:56,420
So early morning about dawn he stopped and then he thought to himself I have been practicing

121
00:15:56,420 --> 00:16:02,740
meditation the whole night but I could not make any progress.

122
00:16:02,740 --> 00:16:14,900
And Buddha had said that Buddha once said Ananda you have a stew of merit accumulated

123
00:16:14,900 --> 00:16:20,340
in the past and so you will become an arhant quickly.

124
00:16:20,340 --> 00:16:27,140
So he remembered these words of the Buddha and he said Buddha's words are never wrong.

125
00:16:27,140 --> 00:16:38,060
So he reviewed his practice and then he decided that I have made too much effort.

126
00:16:38,060 --> 00:16:40,660
So he must relax.

127
00:16:40,660 --> 00:16:50,180
So he went into the cottage and then he washed his feet and went into the cottage and

128
00:16:50,180 --> 00:16:59,580
sat down on the couch and then to relax he tried to lie down on the couch.

129
00:16:59,580 --> 00:17:09,900
So his feet were off the floor and his head had not reached the pillows.

130
00:17:09,900 --> 00:17:16,780
So at that awkward moment he became an arhant.

131
00:17:16,780 --> 00:17:24,140
So in the commentaries it is said in the dispensation of the Buddha who is a monk who

132
00:17:24,140 --> 00:17:30,860
became an arhant while he is not sitting, not standing, not walking and not lying down

133
00:17:30,860 --> 00:17:35,100
then we should say it is the Vanarabur Ananda.

134
00:17:35,100 --> 00:17:45,580
So he became, he reached an arhant ship only when he slowed down, only when he relaxed.

135
00:17:45,580 --> 00:18:02,660
So too much effort is as the tremendous as too little effort.

136
00:18:02,660 --> 00:18:14,060
So next day at the council other monks had taken their seats.

137
00:18:14,060 --> 00:18:18,340
Ananda's seat was still empty.

138
00:18:18,340 --> 00:18:22,380
So monks were asking where is Ananda, where is Ananda?

139
00:18:22,380 --> 00:18:34,460
So at that time he entered the assembly of monks but he went, he typed through the earth

140
00:18:34,460 --> 00:18:42,500
and then he showed himself out of the earth at his seat and took his seat there to let

141
00:18:42,500 --> 00:18:59,500
know other arhants that he had become an arhant too.

142
00:18:59,500 --> 00:19:05,500
So in general these five faculties should be balanced.

143
00:19:05,500 --> 00:19:10,100
So mindfulness is different.

144
00:19:10,100 --> 00:19:15,540
It is said that there is no case where mindfulness is too strong.

145
00:19:15,540 --> 00:19:27,260
So mindfulness can never be too strong and so is strong mindfulness is needed to equalize

146
00:19:27,260 --> 00:19:31,020
the other controlling factors.

147
00:19:31,020 --> 00:19:40,820
So when there is a city, a strong city standing like a god then the other faculties will

148
00:19:40,820 --> 00:19:49,780
do their functions, their respective functions properly and so the meditation practice will

149
00:19:49,780 --> 00:19:55,820
be fruitful.

150
00:19:55,820 --> 00:20:11,260
But the commentary is that in particular faith and wisdom should be balanced and energy

151
00:20:11,260 --> 00:20:15,380
and concentration should be balanced.

152
00:20:15,380 --> 00:20:30,220
Now a person who has too much faith and who is weak in wisdom will have faith in everything.

153
00:20:30,220 --> 00:20:38,380
He will have faith in the object that is not the right object for faith.

154
00:20:38,380 --> 00:20:53,300
He will be an easy prey to those who want to deceive him because a person who is weak

155
00:20:53,300 --> 00:21:01,460
in wisdom and who is strong in faith will believe in anything.

156
00:21:01,460 --> 00:21:15,540
So that is why the faith should be balanced with understanding or wisdom.

157
00:21:15,540 --> 00:21:31,300
A person who is strong in wisdom and weak in faith tends to become crafty, tends to

158
00:21:31,300 --> 00:21:38,420
become Hippocratic.

159
00:21:38,420 --> 00:21:47,300
Such a person is compared to a disease caused by the medicine itself.

160
00:21:47,300 --> 00:21:53,820
So the disease caused by the medicine cannot be cured.

161
00:21:53,820 --> 00:22:01,820
So in the same way it was and who is too wise actually, who is too wise and who is weak in

162
00:22:01,820 --> 00:22:07,220
faith tends to become crafty.

163
00:22:07,220 --> 00:22:19,700
Now the subcommittee explains to us in this way, if you are too wise then you may distort

164
00:22:19,700 --> 00:22:21,500
things.

165
00:22:21,500 --> 00:22:26,380
Now take for example Dana giving.

166
00:22:26,380 --> 00:22:40,140
So Dana is defined in the books as the volition by which you give something.

167
00:22:40,140 --> 00:22:45,940
So when you give something this mental state arises in the mind and that mental state

168
00:22:45,940 --> 00:22:50,780
is called volition or in Pali Chaitana.

169
00:22:50,780 --> 00:23:04,420
So the probably defined Dana means the Chaitana of volition, a volition that accompanies

170
00:23:04,420 --> 00:23:10,780
the consciousness when you make a donation.

171
00:23:10,780 --> 00:23:21,540
But a person who is too wise will say like this, since Chaitana is what we call Dana, we can

172
00:23:21,540 --> 00:23:24,940
do Dana without giving anything.

173
00:23:24,940 --> 00:23:30,820
We just need to have with Chaitana in our minds, we just need to have the will to give

174
00:23:30,820 --> 00:23:34,860
in our minds and then we will have Dana.

175
00:23:34,860 --> 00:23:41,380
So we don't need to give anything but we can have Dana by having Chaitana arise in our

176
00:23:41,380 --> 00:23:42,380
mind.

177
00:23:42,380 --> 00:23:51,140
So he may say such things but actually that cannot be true because without giving

178
00:23:51,140 --> 00:23:56,020
there can be no giving Chaitana.

179
00:23:56,020 --> 00:24:04,580
Only when you give does Chaitana arise in your mind and so there can be no Chaitana

180
00:24:04,580 --> 00:24:07,100
without the act of giving.

181
00:24:07,100 --> 00:24:15,140
But he may argue like that and he may lead people astray by telling it's okay.

182
00:24:15,140 --> 00:24:18,380
You may not give anything but you can have Dana.

183
00:24:18,380 --> 00:24:28,740
So this is an example of a person who is too wise.

184
00:24:28,740 --> 00:24:37,500
When these two feet and wisdom or understanding are balanced then a person will have

185
00:24:37,500 --> 00:24:43,220
faith only in the right object.

186
00:24:43,220 --> 00:24:57,580
So first, faith and wisdom should be balanced and this balance of faith and wisdom can

187
00:24:57,580 --> 00:25:05,260
be practiced even when you are not meditating.

188
00:25:05,260 --> 00:25:14,020
Now when you follow a teaching like teachings of the Buddha you need to have the balance

189
00:25:14,020 --> 00:25:19,860
of faith and understanding of wisdom.

190
00:25:19,860 --> 00:25:28,020
Now if you have too much understanding you will just be criticizing this and you will not

191
00:25:28,020 --> 00:25:39,580
come to the practice and you may find for everything that you read or that you understand.

192
00:25:39,580 --> 00:25:48,420
So it is good to balance understanding with faith or confidence.

193
00:25:48,420 --> 00:25:55,420
So when you have faith or confidence in the Buddha and in his teachings then you will not

194
00:25:55,420 --> 00:25:59,620
criticize much and you will practice.

195
00:25:59,620 --> 00:26:09,460
Now sometimes there are people who are chanting and all these things and they are not

196
00:26:09,460 --> 00:26:15,500
really taught by the Buddha and so they are not beneficial or something like that.

197
00:26:15,500 --> 00:26:17,740
They may want to say.

198
00:26:17,740 --> 00:26:24,700
And they do not do these things themselves and so they are deprived of the benefits they

199
00:26:24,700 --> 00:26:32,660
would get from such performances or such practices.

200
00:26:32,660 --> 00:26:39,060
So everywhere these two should be balanced.

201
00:26:39,060 --> 00:26:47,820
There are too much faith not too much understanding.

202
00:26:47,820 --> 00:26:58,900
When practicing meditation to faith and wisdom or understanding can become too much.

203
00:26:58,900 --> 00:27:06,060
Suppose you are practicing meditation and you begin to see the rising and falling and

204
00:27:06,060 --> 00:27:14,020
when you see the rising and falling you are very happy because you have not seen it before

205
00:27:14,020 --> 00:27:17,460
and now you are seeing it for yourself.

206
00:27:17,460 --> 00:27:23,020
And so you have so much santa at that time that you may want to bow round to the Buddha

207
00:27:23,020 --> 00:27:24,540
many times.

208
00:27:24,540 --> 00:27:29,540
You may want to go to the Dshantam that you are very grateful to him or you may want to

209
00:27:29,540 --> 00:27:35,940
relate this experience to your friends and you may think that you can give it a more talk.

210
00:27:35,940 --> 00:27:49,180
And so you are agitated and you become restless and you lose concentration and also understanding

211
00:27:49,180 --> 00:27:50,900
can be too much.

212
00:27:50,900 --> 00:27:59,980
Now you begin to see the rising and falling and so you are pleased with the understanding

213
00:27:59,980 --> 00:28:07,340
of the rising and falling and then you become agitated because you are happy and then

214
00:28:07,340 --> 00:28:15,340
you want to take the other objects also and watch them so that you see the rising and

215
00:28:15,340 --> 00:28:21,540
falling of them or something like playing with the object and then you become agitated.

216
00:28:21,540 --> 00:28:29,420
So in such cases mindfulness is very important.

217
00:28:29,420 --> 00:28:36,660
So when you have strong mindfulness then it can protect the faith from becoming too much

218
00:28:36,660 --> 00:28:40,420
or understanding from becoming too much.

219
00:28:40,420 --> 00:28:57,500
So the mindfulness can regulate these factors and keep them in their proper balance.

220
00:28:57,500 --> 00:29:08,140
Just as faith and understanding should be balanced so also should effort and concentration

221
00:29:08,140 --> 00:29:11,300
should be balanced.

222
00:29:11,300 --> 00:29:21,100
When there is too much effort as you know you fall into agitation and you cannot concentrate

223
00:29:21,100 --> 00:29:30,180
and if you have too much concentration you fall into laziness.

224
00:29:30,180 --> 00:29:39,460
That means when you have good concentration you tend to lose effort.

225
00:29:39,460 --> 00:29:42,300
You tend to make less effort.

226
00:29:42,300 --> 00:29:49,460
Now you can be mindful of the object easily then your mind can be on the object easily.

227
00:29:49,460 --> 00:29:54,340
You want you tend to make less effort.

228
00:29:54,340 --> 00:30:04,700
So when the level of effort goes down and the concentration becomes stronger than you tend

229
00:30:04,700 --> 00:30:11,780
to become sleepy or you tend to become lazy.

230
00:30:11,780 --> 00:30:21,620
So the effort and concentration should also be balanced.

231
00:30:21,620 --> 00:30:34,740
So when effort is balanced with concentration then it will not let you fall into restlessness

232
00:30:34,740 --> 00:30:44,460
and when concentration is balanced with effort it will not let you fall into laziness.

233
00:30:44,460 --> 00:31:04,460
So we should take care that we do not have too much effort or too much concentration.

234
00:31:04,460 --> 00:31:13,260
Sometimes you practice meditation and you are not tired and you have a good practice,

235
00:31:13,260 --> 00:31:18,340
good mindfulness and good concentration.

236
00:31:18,340 --> 00:31:28,380
But when concentration is good you tend to lose off or you tend to feel sleepy.

237
00:31:28,380 --> 00:31:33,740
So in that case chances are you have too much concentration.

238
00:31:33,740 --> 00:31:44,940
So if you think you have too much concentration you have to reduce it and at the same time step up effort

239
00:31:44,940 --> 00:31:56,300
by paying closer attention to the object or by increasing the objects the main objects

240
00:31:56,300 --> 00:31:58,980
of your meditation.

241
00:31:58,980 --> 00:32:04,580
So that means if you think you have too much effort and the energy is low then you step

242
00:32:04,580 --> 00:32:12,460
up your energy by taking four or five things at a time like touching, touching, touching,

243
00:32:12,460 --> 00:32:17,660
touching, touching, touching, touching, sitting and so on.

244
00:32:17,660 --> 00:32:28,540
So the practice of meditation is a very delicate one and you have to know when and when

245
00:32:28,540 --> 00:32:47,300
effort is too much or the concentration is too much and then act accordingly.

246
00:32:47,300 --> 00:32:53,540
So now we know that the five faculties should be balanced.

247
00:32:53,540 --> 00:33:01,980
But there are some special statements mentioned in the commentaries and that is for a person

248
00:33:01,980 --> 00:33:10,700
who practices some at a meditation, faith all those strong is permissible.

249
00:33:10,700 --> 00:33:19,340
That means if he has much faith it was a new practice, some at a meditation, if he has

250
00:33:19,340 --> 00:33:24,620
much faith it is good for him.

251
00:33:24,620 --> 00:33:34,700
It is permissible because having faith he will reach absorption or he will reach Jana.

252
00:33:34,700 --> 00:33:49,100
So that means he will not be thinking how can it be possible to attain Jana just by reflecting

253
00:33:49,100 --> 00:34:00,940
on the disc saying, but he thinks that it was taught by the Buddha and what the Buddha

254
00:34:00,940 --> 00:34:09,220
taught must be correct and so with faith in the Buddha he makes effort and he will reach

255
00:34:09,220 --> 00:34:12,340
absorption or he will reach Jana.

256
00:34:12,340 --> 00:34:25,620
So for a person who practices some at a meditation a strong faith is permissible.

257
00:34:25,620 --> 00:34:34,420
Another statement is about concentration and understanding.

258
00:34:34,420 --> 00:34:42,740
Now normally concentration and effort should be balanced.

259
00:34:42,740 --> 00:34:47,500
But what about between concentration and understanding?

260
00:34:47,500 --> 00:34:57,140
Now it is said that for a person who practices some at a meditation concentration if strong

261
00:34:57,140 --> 00:34:58,940
is permissible.

262
00:34:58,940 --> 00:35:06,900
So for a person who practices some at a meditation strong concentration is permissible.

263
00:35:06,900 --> 00:35:19,260
It is okay if he has strong concentration because by strong concentration he will reach absorption.

264
00:35:19,260 --> 00:35:23,700
That means he will reach Jana.

265
00:35:23,700 --> 00:35:30,420
Because when you practice some at a meditation you need strong concentration.

266
00:35:30,420 --> 00:35:36,140
Only strong concentration will lead to the attainment of Jana.

267
00:35:36,140 --> 00:35:47,260
So for a person who practices some at a meditation strong concentration is okay.

268
00:35:47,260 --> 00:35:56,220
And for a person who practices a sweet person understanding if strong is permissible.

269
00:35:56,220 --> 00:36:05,220
That means if he has strong understanding it is alright.

270
00:36:05,220 --> 00:36:16,220
Because by strong understanding by strong wisdom he reaches penetration of the characteristics.

271
00:36:16,220 --> 00:36:24,700
Because we persona actually the word we persona is a synonym for Panya or understanding.

272
00:36:24,700 --> 00:36:31,420
So when you practice we persona Panya or understanding is important.

273
00:36:31,420 --> 00:36:39,060
When it is strong it is good because only by strong Panya can you penetrate into the three

274
00:36:39,060 --> 00:36:42,540
characteristics of all conditions phenomena.

275
00:36:42,540 --> 00:36:51,260
So for a person who practices we persona is strong Panya is strong understanding.

276
00:36:51,260 --> 00:36:59,820
That means a stronger understanding is permissible.

277
00:36:59,820 --> 00:37:08,380
Now what about sati or mindfulness?

278
00:37:08,380 --> 00:37:14,740
mindfulness should be strong in every case.

279
00:37:14,740 --> 00:37:24,220
Because when mindfulness is strong it can protect the mind from falling into restlessness

280
00:37:24,220 --> 00:37:30,580
through the influence of faith, energy and wisdom.

281
00:37:30,580 --> 00:37:39,740
Now faith leads to agitation, energy leads to agitation and wisdom or understanding leads

282
00:37:39,740 --> 00:37:42,180
to agitation.

283
00:37:42,180 --> 00:37:53,780
If they are not protected by strong mindfulness the yogi will fall into restlessness.

284
00:37:53,780 --> 00:38:09,180
So sati is necessary and it should be strong everywhere.

285
00:38:09,180 --> 00:38:16,660
And concentration can lead to lessitude, it can lead to laziness.

286
00:38:16,660 --> 00:38:22,100
That is if it is not protected by mindfulness.

287
00:38:22,100 --> 00:38:34,900
So a strong mindfulness is needed to protect concentration from falling into laziness

288
00:38:34,900 --> 00:38:37,340
or lassitude.

289
00:38:37,340 --> 00:38:48,180
That is why it is said mindfulness should be strong everywhere.

290
00:38:48,180 --> 00:38:58,020
So sati is compared to salt in all dishes and a minister who is clever in all affairs

291
00:38:58,020 --> 00:39:00,580
of the king.

292
00:39:00,580 --> 00:39:11,060
That means mindfulness is useful everywhere and there is no occasion where mindfulness is too

293
00:39:11,060 --> 00:39:12,660
strong.

294
00:39:12,660 --> 00:39:22,420
So we have to have strong mindfulness to protect the mind from falling into restlessness

295
00:39:22,420 --> 00:39:27,500
and lassitude through the influence of the other faculties.

296
00:39:27,500 --> 00:39:37,420
So when we practice meditation we must see to it that these five faculties are balanced,

297
00:39:37,420 --> 00:39:43,180
not one too much than the others.

298
00:39:43,180 --> 00:39:48,980
When they are balanced they will be doing their own functions properly.

299
00:39:48,980 --> 00:39:58,660
So when each one of them is doing its own function properly then the mechanism of meditation

300
00:39:58,660 --> 00:40:00,700
will go smoothly.

301
00:40:00,700 --> 00:40:09,620
Now it is like a machine, there are many parts in the machine and every part has its

302
00:40:09,620 --> 00:40:14,580
function to do, they are put there not for nothing.

303
00:40:14,580 --> 00:40:22,500
But if one of the parts does not work properly then the whole machine will not work.

304
00:40:22,500 --> 00:40:25,500
Just one, maybe one, one, what about that?

305
00:40:25,500 --> 00:40:31,900
One clock or one part, one small part, when it does not work properly then the whole

306
00:40:31,900 --> 00:40:34,540
machine will not work properly.

307
00:40:34,540 --> 00:40:41,900
So it is very important that every part in the machine must do its own function or must

308
00:40:41,900 --> 00:40:43,380
work properly.

309
00:40:43,380 --> 00:40:51,100
So in the same way this mechanism of meditation is composed of five components, five parts

310
00:40:51,100 --> 00:40:56,060
and these five parts must be doing their own function properly.

311
00:40:56,060 --> 00:41:07,980
They must be doing their function together and not in excess of others.

312
00:41:07,980 --> 00:41:20,220
So when these five faculties are regulated then they will function properly and the meditation

313
00:41:20,220 --> 00:41:29,980
will be good.

314
00:41:29,980 --> 00:41:38,140
And these five factors are here called faculties, in Pali they are called endreas, so

315
00:41:38,140 --> 00:41:51,500
endria mean governing, that means they have authority in their own fields.

316
00:41:51,500 --> 00:42:00,620
Now they are like ministers, so ministers have power or authority in their respective ministries.

317
00:42:00,620 --> 00:42:09,460
So in the same way free the confidence has authority when it comes to having free or

318
00:42:09,460 --> 00:42:18,900
resolving and effort or energy, exercise is authority when it comes to exciting and so on.

319
00:42:18,900 --> 00:42:28,820
Now these five faculties are also called powers because they could stand firm against

320
00:42:28,820 --> 00:42:34,580
the opposition or against opposite factors.

321
00:42:34,580 --> 00:42:44,340
So they are sometimes they are called powers or strength and some of them may also be found

322
00:42:44,340 --> 00:42:50,140
among the seven factors of enlightenment.

323
00:42:50,140 --> 00:43:01,780
I hope you know the seven factors of enlightenment, what are the mindfulness is one, investigation

324
00:43:01,780 --> 00:43:14,060
of demos, that means understanding and then effort really are right, PT and joy and

325
00:43:14,060 --> 00:43:30,380
pass a D tranquility, they are not here and then what else, concentration and then equanimity.

326
00:43:30,380 --> 00:43:37,180
So concentration is among the five faculties, understanding is among the five faculties, mindfulness

327
00:43:37,180 --> 00:43:41,580
is among the five faculties, effort is among the five faculties.

328
00:43:41,580 --> 00:43:49,700
So these mental states are given different names in different groups.

329
00:43:49,700 --> 00:43:59,900
So here they are faculty and in another group they are called power and yet another group

330
00:43:59,900 --> 00:44:05,580
they are called factors of enlightenment and also what about among the eight factors of

331
00:44:05,580 --> 00:44:13,020
the path and the noble eightfold path.

332
00:44:13,020 --> 00:44:15,980
Is there effort in the noble eightfold path?

333
00:44:15,980 --> 00:44:21,540
Yes, mindfulness, yes, concentration, yes, wisdom, yes.

334
00:44:21,540 --> 00:44:29,500
So they have different names although they are one and the same mental state.

335
00:44:29,500 --> 00:44:49,220
So just as the men may be a house father at his house, but he may be a boss at the office

336
00:44:49,220 --> 00:44:52,100
or he may be a crack and so on.

337
00:44:52,100 --> 00:45:06,740
And so one person can have different jobs or different works and so here also one and

338
00:45:06,740 --> 00:45:13,340
the same factor can be called a faculty or a power or a factor of enlightenment or factor

339
00:45:13,340 --> 00:45:15,580
of path.

340
00:45:15,580 --> 00:45:26,220
Okay, let us go back to the passage we recite every morning.

341
00:45:26,220 --> 00:45:31,820
In the phrase, ardent, clearly comprehending and mindful.

342
00:45:31,820 --> 00:45:34,620
How many factors do you find?

343
00:45:34,620 --> 00:45:39,820
Ardent, clearly comprehending and mindful.

344
00:45:39,820 --> 00:45:43,020
Three, we find three.

345
00:45:43,020 --> 00:45:52,460
In the first one, Ardent means energy or effort and clearly comprehending means understanding

346
00:45:52,460 --> 00:45:55,500
and mindful means mindfulness.

347
00:45:55,500 --> 00:46:00,380
And what did the subcommittees say here?

348
00:46:00,380 --> 00:46:05,940
We must also understand samadhi, concentration.

349
00:46:05,940 --> 00:46:12,820
So by mindfulness we must also understand concentration because they go together.

350
00:46:12,820 --> 00:46:21,980
So taking the concentration along with the other factors, we get four factors there,

351
00:46:21,980 --> 00:46:22,980
right?

352
00:46:22,980 --> 00:46:28,380
Effort, understanding, mindfulness and concentration.

353
00:46:28,380 --> 00:46:39,660
But in the sequence of practice we say effort, mindfulness, concentration, understanding.

354
00:46:39,660 --> 00:46:41,660
Only faith is not mentioned there.

355
00:46:41,660 --> 00:46:48,220
But we may take all of the faith also because without faith, without confidence, we will

356
00:46:48,220 --> 00:46:50,700
not practice at all.

357
00:46:50,700 --> 00:46:59,860
So we may say that by the phrase, ardent, clearly comprehending, mindful, but assured as these

358
00:46:59,860 --> 00:47:08,420
five controlling faculties or five components of practice.

359
00:47:08,420 --> 00:47:16,820
And these five components must work together and each of them must do its own function

360
00:47:16,820 --> 00:47:39,100
and not.

