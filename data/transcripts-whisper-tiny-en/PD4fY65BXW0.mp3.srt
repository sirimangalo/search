1
00:00:00,000 --> 00:00:07,000
Okay, and another question today. This one from BD-951.

2
00:00:07,000 --> 00:00:11,000
Thanks for the videos. Can you speak about working with physical pain?

3
00:00:11,000 --> 00:00:14,000
How important does it not to move during meditation?

4
00:00:14,000 --> 00:00:17,000
You recommend any type of stretching or yoga?

5
00:00:17,000 --> 00:00:23,000
Thanks. Okay, working with physical pain.

6
00:00:23,000 --> 00:00:30,000
Well, short and sweet, I think there are recognized to be two types of pain.

7
00:00:30,000 --> 00:00:35,000
The kind of pain that is going to hurt but not injure you.

8
00:00:35,000 --> 00:00:44,000
And the kind of pain that is not only going to hurt but is going to injure you or is associated with an injury.

9
00:00:44,000 --> 00:00:49,000
Pain that is just going to hurt, it doesn't matter how long you sit with it.

10
00:00:49,000 --> 00:00:53,000
It is just going to hurt and when you get up it is going to go away.

11
00:00:53,000 --> 00:01:03,000
Another type of pain, if you sit for too long it is going to injure you or it is going to have severe consequences.

12
00:01:03,000 --> 00:01:10,000
It may be that if you are really serious about meditation that there isn't this sort of distinction but I think for most people

13
00:01:10,000 --> 00:01:16,000
you have to be realistic that there is only a certain level of pain that you can take.

14
00:01:16,000 --> 00:01:24,000
If you can take it and if you know that it is just pain, like for most of us sitting cross-legged leads to some pain in the legs

15
00:01:24,000 --> 00:01:31,000
or pain in the back or in the shoulders or so on, all of that kind of pain is going to go away.

16
00:01:31,000 --> 00:01:38,000
It is not going to lead to your injury unless you have say a bad back and it is just so excruciatingly painful

17
00:01:38,000 --> 00:01:43,000
that sit there is just going to whatever cause you to pass out.

18
00:01:43,000 --> 00:01:49,000
I am not even sure about that. There are other pains where people have injuries where they have been in an accident

19
00:01:49,000 --> 00:01:57,000
or had a sports injury or something and I think it is not advisable for them to try to work through the pain.

20
00:01:57,000 --> 00:02:05,000
For most natural pains that occur in meditation in the shoulders, in the back, in the legs,

21
00:02:05,000 --> 00:02:14,000
they are almost entirely stress-related and so there is something that we do have to work through in meditation.

22
00:02:14,000 --> 00:02:20,000
Obviously the technique is very simple and it is just to see the pain from what it is.

23
00:02:20,000 --> 00:02:27,000
The way we overcome pain is to overcome the disliking of it is to learn to see it in a different way

24
00:02:27,000 --> 00:02:33,000
that it is just pain, to be able to accept that part of reality when it really is painful,

25
00:02:33,000 --> 00:02:40,000
to be able to accept it and to not live our lives running away from the pain, to running away from things that we don't like.

26
00:02:40,000 --> 00:02:48,000
When we run away from things, obviously we create a habit of a version where every time the pain comes up,

27
00:02:48,000 --> 00:02:58,000
it becomes more and more of a problem and we become more and more adverse to it and less and less able to take even the smallest amount of pain.

28
00:02:58,000 --> 00:03:08,000
We are trying to go in the other way in the other direction and be able to accept more and more of reality, even the more uncomfortable part.

29
00:03:08,000 --> 00:03:16,000
In this sense, this sort of pain is very much associated with things like hunger or heat, cold,

30
00:03:16,000 --> 00:03:21,000
which are all things that we can put up with but we don't want to.

31
00:03:21,000 --> 00:03:29,000
Loud noises when people are saying bad things or speaking loudly,

32
00:03:29,000 --> 00:03:33,000
we are trying to meditate and they are making loud noise in the background.

33
00:03:33,000 --> 00:03:36,000
None of these things are going to hurt us.

34
00:03:36,000 --> 00:03:41,000
You wouldn't sit there through the loud noise and somehow be harmed by it in any way,

35
00:03:41,000 --> 00:03:43,000
but you get more and more frustrated.

36
00:03:43,000 --> 00:03:48,000
A lot of our pain is very similar and we have to be honest with ourselves

37
00:03:48,000 --> 00:03:52,000
and ask ourselves, is this pain really going to cause a permanent injury?

38
00:03:52,000 --> 00:03:55,000
Is it really going to hurt me if I sit through it?

39
00:03:55,000 --> 00:04:01,000
If it's not, then it's certainly in your best interest to overcome the aversion to the pain.

40
00:04:01,000 --> 00:04:10,000
The only time that wouldn't be the case is if you are reasonably sure that sitting through the pain is just going to aggravate an injury or so on.

41
00:04:10,000 --> 00:04:15,000
That can be generally the case of, as I said, old injuries may be in the case of back problems,

42
00:04:15,000 --> 00:04:18,000
although I'm not sure about that.

43
00:04:18,000 --> 00:04:22,000
I would think that someone with a bad book back could eventually work it out in meditation

44
00:04:22,000 --> 00:04:24,000
as it's often stress related.

45
00:04:24,000 --> 00:04:33,000
But then there are back injuries like chronic back injuries based on old age and so on

46
00:04:33,000 --> 00:04:42,000
that probably can be worked out through meditation and therefore you should think about sitting in a chair or up against the wall,

47
00:04:42,000 --> 00:04:44,000
which is totally fine.

48
00:04:44,000 --> 00:04:52,000
That being said, even the next question, how important is it not to move during meditation?

49
00:04:52,000 --> 00:04:58,000
It's quite important because as I said, this is a sort of aversion.

50
00:04:58,000 --> 00:05:04,000
But that being said, you have to know your limits and if it's just driving you insane

51
00:05:04,000 --> 00:05:06,000
and it's too powerful for you.

52
00:05:06,000 --> 00:05:10,000
Like you consider, okay, in my practice I'm here, the pain is up here.

53
00:05:10,000 --> 00:05:18,000
I'm not ready to sit through this pain. I know that I probably should, but realistically speaking, I'm not going to be able to.

54
00:05:18,000 --> 00:05:22,000
And then you can move. If you do move, you shouldn't feel guilty about it.

55
00:05:22,000 --> 00:05:29,000
You should accept the fact that you're only able to at this point accept a certain amount of pain.

56
00:05:29,000 --> 00:05:35,000
So when you do move, what is important, then what you should be very careful to do, is to acknowledge the movements.

57
00:05:35,000 --> 00:05:42,000
So as you stretch, say to yourself, stretching, stretching, when you slouch, slouching,

58
00:05:42,000 --> 00:05:47,000
slouching, when you lift your leg lifting, lifting and move it out moving, moving.

59
00:05:47,000 --> 00:05:54,000
If you grasp, grasp your leg grasping, lifting, placing and so on, being mindful of the movements.

60
00:05:54,000 --> 00:05:59,000
And then you can say, you're not really cheating or you're not really outside of the meditation.

61
00:05:59,000 --> 00:06:02,000
At least you're being mindful and your mindfulness is continuous.

62
00:06:02,000 --> 00:06:11,000
You should acknowledge from the very beginning of disliking, disliking and wanting to move the leg wanting, wanting and lifting and placing.

63
00:06:11,000 --> 00:06:19,000
But if you can, the best thing is to say to yourself, disliking, disliking, disliking, disliking until the disliking goes away.

64
00:06:19,000 --> 00:06:25,000
When it's gone, go back to the pain and say to yourself, pain, reassuring yourself that it's just pain.

65
00:06:25,000 --> 00:06:30,000
It's nothing intrinsically bad when it's gone, which it will eventually go away.

66
00:06:30,000 --> 00:06:35,000
Because the great thing is that if you do stick with it, it'll eventually go away and not come back.

67
00:06:35,000 --> 00:06:41,000
Because you're losing the stress, you're giving up, you're letting go, and your body becomes less stressful.

68
00:06:41,000 --> 00:06:45,000
Your shoulders, your back, your legs, they become more relaxed.

69
00:06:45,000 --> 00:06:50,000
And as a result, these stress pains don't come back.

70
00:06:50,000 --> 00:06:53,000
Finally, do you recommend any type of stretching or yoga?

71
00:06:53,000 --> 00:07:07,000
No, I don't because I think these are a means of escaping, a means of finding a state of being that is pleasurable, that is peaceful, that is free from this sort of thing.

72
00:07:07,000 --> 00:07:13,000
And it's a compartmentalizing of reality, it's saying, okay, when this comes, I have to find a way to get away from it.

73
00:07:13,000 --> 00:07:16,000
I have to not allow this state of reality to arise.

74
00:07:16,000 --> 00:07:23,000
So stretching is an artificial, it's a construct, it's an artificial bodily state.

75
00:07:23,000 --> 00:07:28,000
It's trying to create a state of body that is healthy, that is perfect, and so on.

76
00:07:28,000 --> 00:07:35,000
And I think we would all have to agree that this body is not perfect, and there's no way to create that state.

77
00:07:35,000 --> 00:07:41,000
Because it's an artificially constructed state, it's not sustainable, and it's not natural.

78
00:07:41,000 --> 00:07:44,000
Natural is to let the body be as it is.

79
00:07:44,000 --> 00:07:50,000
It's a difference of opinion, and I'm sure yoga teachers would say something completely different, and you know, power to them.

80
00:07:50,000 --> 00:07:52,000
We all have different ideas.

81
00:07:52,000 --> 00:08:04,000
But I've seen clearly that I don't need yoga to be flexible, to have a calm bodily state free from these stresses and free from the pains.

82
00:08:04,000 --> 00:08:06,000
It's the same as a massage.

83
00:08:06,000 --> 00:08:11,000
People who give massages, they say, yeah, it's necessary to have a massage every week or so on.

84
00:08:11,000 --> 00:08:21,000
And I've never had a massage in my life, and I used to have lots of stress problems, and the shoulders, and the back, and the legs when I first started meditating, it was torture.

85
00:08:21,000 --> 00:08:25,000
But after you practice honestly, you don't need any of this.

86
00:08:25,000 --> 00:08:30,000
Your body is very flexible, it's very loose, your breathing is very deep, it's very natural.

87
00:08:30,000 --> 00:08:35,000
And it comes by itself, there is no creating of this state.

88
00:08:35,000 --> 00:08:42,000
It's bringing the body back to a natural state, a healthy state, which takes no training of the body whatsoever.

89
00:08:42,000 --> 00:08:46,000
When the mind lets go of the body, the body naturally comes back to its state.

90
00:08:46,000 --> 00:08:53,000
So I don't recommend any sort of artificial bodily, even exercise, I don't recommend it.

91
00:08:53,000 --> 00:08:57,000
If you want to do it, living in the world, if it's necessary for your job or so on.

92
00:08:57,000 --> 00:09:07,000
But if it's just to make you look more attractive to each their own, the most important in the meditation is to free the mind from clinging.

93
00:09:07,000 --> 00:09:14,000
So especially clinging from clinging to the body, it's one of the great clinging.

94
00:09:14,000 --> 00:09:21,000
So in the end we do have to let go, and we have to be prepared for whatever comes trying to be.

95
00:09:21,000 --> 00:09:41,000
To make our mind so strong that nothing can ever face us, to make our mind so clearly aware and so wise and understanding about reality that whatever comes we're able to accept it.

96
00:09:41,000 --> 00:09:51,000
So pain is a difficult one, but that's really one of the big reasons why we're practicing for physical and pain and mental pain.

97
00:09:51,000 --> 00:09:59,000
To become free from the power of these things, the power that they have over us.

98
00:09:59,000 --> 00:10:06,000
The only way they have power over us is because we give it to them, we say they're bad, we say this is bad, this is unacceptable.

99
00:10:06,000 --> 00:10:11,000
And as a result we create friction, we create a state of aversion towards them.

100
00:10:11,000 --> 00:10:37,000
Okay, so I hope that helped and good luck dealing with the pain, whatever pain you might have, and may you in the end, I'll be free from pain and suffering.

