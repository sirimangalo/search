1
00:00:00,000 --> 00:00:07,000
Good evening, everyone. Welcome to our daily Dhamma session.

2
00:00:14,000 --> 00:00:23,000
It might be interesting for you all to know that the whole idea of doing a daily Dhamma talk,

3
00:00:23,000 --> 00:00:29,000
but actually it's something I've just started, but I've done it before.

4
00:00:29,000 --> 00:00:33,000
And it comes out of our...

5
00:00:37,000 --> 00:00:45,000
They set up at my teacher's monastery back when I was a young monk.

6
00:00:45,000 --> 00:00:53,000
Every day, actually morning and evening at Chantong,

7
00:00:53,000 --> 00:00:58,000
as long as you didn't have something else to do, or somewhere else to be.

8
00:01:01,000 --> 00:01:06,000
He would come for morning chanting, evening chanting, and every day

9
00:01:06,000 --> 00:01:12,000
would give a talk in the morning and evening, sometimes five minutes,

10
00:01:12,000 --> 00:01:19,000
sometimes ten minutes, sometimes longer.

11
00:01:19,000 --> 00:01:22,000
But every day he would give a little bit of Dhamma.

12
00:01:22,000 --> 00:01:37,000
It really created a sense of community and a sense of being together in a monastery.

13
00:01:37,000 --> 00:01:43,000
So it made me think because it's just sort of what we're creating here, right?

14
00:01:43,000 --> 00:01:46,000
That we're all around the world.

15
00:01:46,000 --> 00:01:49,000
We have meditators here at the monastery.

16
00:01:49,000 --> 00:01:54,000
It's our one chance to come together.

17
00:01:54,000 --> 00:02:02,000
The meditators are often in their rooms, meditating, and everyone here is off-living your lives.

18
00:02:02,000 --> 00:02:07,000
But it's a lot like being together in the monastery, right?

19
00:02:07,000 --> 00:02:14,000
Every day we can do a meditation session together around 7 p.m., 8 p.m.,

20
00:02:14,000 --> 00:02:17,000
and then at 9 p.m., we come together.

21
00:02:17,000 --> 00:02:20,000
Just as early we were living together in monastery,

22
00:02:20,000 --> 00:02:25,000
come together and listen to Dhamma.

23
00:02:25,000 --> 00:02:35,000
Carrying on this tradition, carrying on Buddhist tradition,

24
00:02:35,000 --> 00:02:44,000
the Buddhist tradition that I follow is fairly specific.

25
00:02:44,000 --> 00:02:50,000
We have what we call the teruada, which means teru means elder,

26
00:02:50,000 --> 00:03:00,000
or wada, or wada means the teaching or the point of view or the doctrine.

27
00:03:00,000 --> 00:03:09,000
And by elders we mean this specific set of texts that have been handed down by specific set of monks,

28
00:03:09,000 --> 00:03:11,000
not all monks.

29
00:03:11,000 --> 00:03:15,000
Some monks don't follow these texts.

30
00:03:15,000 --> 00:03:24,000
We have texts that claim to be written by the came to be copies of what the Buddha actually said.

31
00:03:24,000 --> 00:03:32,000
We have texts that are explanations and strap relations on those texts.

32
00:03:32,000 --> 00:03:40,000
And then we have texts that are, again, extrapolations and explanations of those texts.

33
00:03:40,000 --> 00:03:52,000
We have stories that are supposed to be stories that Buddha told or stories that recount events in the Buddha's life.

34
00:03:52,000 --> 00:03:54,000
We've got the rules for monks.

35
00:03:54,000 --> 00:03:57,000
We've got the talks that the Buddha gave.

36
00:03:57,000 --> 00:04:06,000
We've got the abidhamma, which is lists and lists and classifications of dhammas

37
00:04:06,000 --> 00:04:11,000
of the reality of the experience.

38
00:04:11,000 --> 00:04:14,000
So we have a cohesive tradition.

39
00:04:14,000 --> 00:04:22,000
This is Buddhist teruada, Buddhist tradition.

40
00:04:22,000 --> 00:04:25,000
And also we have a meditation tradition.

41
00:04:25,000 --> 00:04:28,000
So the meditation, the tradition that I teach,

42
00:04:28,000 --> 00:04:39,000
is goes the way of the Mahasi Sayyada and his teacher, the Mingun Sayyada, Mingun Jaituan Sayyada.

43
00:04:39,000 --> 00:04:48,000
He goes the way of Ajahn Tong, who I think personalized the teaching of his own.

44
00:04:48,000 --> 00:04:50,000
It's a specific tradition.

45
00:04:50,000 --> 00:04:55,000
Again, this idea of tradition.

46
00:04:55,000 --> 00:05:04,000
But these two together, I think, make up the essence of what the tradition would call Buddhism.

47
00:05:04,000 --> 00:05:17,000
We have in one of these ancient texts this quote, apparently the Buddha said.

48
00:05:17,000 --> 00:05:19,000
He was asked by an old monk.

49
00:05:19,000 --> 00:05:25,000
He was an old monk who was concerned about being able to fulfill all of the duties.

50
00:05:25,000 --> 00:05:28,000
Because he had become a monk in his old age.

51
00:05:28,000 --> 00:05:40,000
And so he asked the Buddha, Kati Bandedurani, how many duties are there in this sasana?

52
00:05:40,000 --> 00:05:50,000
And the Buddha said, do we maybe go to Rani, do Rani?

53
00:05:50,000 --> 00:05:54,000
There are these two, there are only two.

54
00:05:54,000 --> 00:06:04,000
There are these two duties in this sasana, in this religion, in this teaching.

55
00:06:04,000 --> 00:06:13,000
Gantaturang, Gantaturang, Vipassanaturang.

56
00:06:13,000 --> 00:06:15,000
Actually, he knows a little bit.

57
00:06:15,000 --> 00:06:16,000
He goes a little more detail.

58
00:06:16,000 --> 00:06:18,000
I can't remember the quote.

59
00:06:18,000 --> 00:06:21,000
So this comes in one of the common areas.

60
00:06:21,000 --> 00:06:25,000
Some people might say, well, this is actually something the Buddha said.

61
00:06:25,000 --> 00:06:29,000
It doesn't really matter.

62
00:06:29,000 --> 00:06:37,000
The point is, here we have a concise enumeration of the things we have to do.

63
00:06:37,000 --> 00:06:40,000
Only two things.

64
00:06:40,000 --> 00:06:45,000
It's often a question, maybe not a conscious question, but one in our minds.

65
00:06:45,000 --> 00:06:48,000
What does it mean to be Buddha?

66
00:06:48,000 --> 00:06:52,000
Especially when we come to practice intensively.

67
00:06:52,000 --> 00:06:55,000
One or two things that we should do.

68
00:06:55,000 --> 00:06:58,000
Maybe we get the idea that there are many things we should be doing.

69
00:06:58,000 --> 00:07:01,000
We think of all the many teachings of the Buddha.

70
00:07:01,000 --> 00:07:03,000
What does it all boil down to?

71
00:07:03,000 --> 00:07:06,000
What do we have to do?

72
00:07:06,000 --> 00:07:11,000
So Gantaturang, Gantaturang, Gantaturang.

73
00:07:11,000 --> 00:07:18,000
Gantam means gathering or something or friends or something.

74
00:07:18,000 --> 00:07:21,000
What it means is study.

75
00:07:21,000 --> 00:07:28,000
We have to spend some time in scholastic endeavor.

76
00:07:28,000 --> 00:07:32,000
Studying the Buddha's teaching.

77
00:07:32,000 --> 00:07:34,000
And of course, we've asked the Gantaturang,

78
00:07:34,000 --> 00:07:42,000
this is a duty or the task of trying to come to see clearly.

79
00:07:42,000 --> 00:07:44,000
So on the one hand, we have all this text.

80
00:07:44,000 --> 00:07:47,000
So we have the textual tradition that we have to follow.

81
00:07:47,000 --> 00:07:52,000
We have to carry on in fact.

82
00:07:52,000 --> 00:08:00,000
And we have the meditation tradition where it's our duty and our task

83
00:08:00,000 --> 00:08:18,000
to practice to see clearly.

84
00:08:18,000 --> 00:08:23,000
And in our story with this old monk, he says to the Buddha,

85
00:08:23,000 --> 00:08:27,000
I don't think I'll be able to practice Gantaturang.

86
00:08:27,000 --> 00:08:31,000
I won't be able to study to completion.

87
00:08:31,000 --> 00:08:34,000
There's just so much in here.

88
00:08:34,000 --> 00:08:35,000
My memory is not good.

89
00:08:35,000 --> 00:08:37,000
I'm old.

90
00:08:37,000 --> 00:08:39,000
But we pass in and do that.

91
00:08:39,000 --> 00:08:40,000
Something I can do.

92
00:08:40,000 --> 00:08:42,000
Is it OK if I just stick to be passing the Buddha?

93
00:08:42,000 --> 00:08:45,000
And the Buddha is OK with that.

94
00:08:45,000 --> 00:08:50,000
It seems these two are not something that everyone has to do.

95
00:08:50,000 --> 00:08:56,000
But they are that which we as Buddhists have to carry on.

96
00:08:56,000 --> 00:09:00,000
So if no one were to carry on the texts and to study the texts,

97
00:09:00,000 --> 00:09:04,000
well, we would lose the path.

98
00:09:04,000 --> 00:09:09,000
We would lose the way to become enlightened.

99
00:09:09,000 --> 00:09:12,000
On the other hand, if we were to just focus on the texts

100
00:09:12,000 --> 00:09:15,000
and no one were to practice in sight meditation,

101
00:09:15,000 --> 00:09:20,000
of course the texts would become useless and incomprehensible.

102
00:09:20,000 --> 00:09:26,000
We don't actually take the time to practice the gain in sight.

103
00:09:26,000 --> 00:09:28,000
We would not likely to...

104
00:09:28,000 --> 00:09:31,000
The texts become meaningless.

105
00:09:31,000 --> 00:09:35,000
We're not likely to understand them in the future without people to verify

106
00:09:35,000 --> 00:09:41,000
and explain them from a practical perspective.

107
00:09:41,000 --> 00:09:44,000
So let's talk about these two because this is the textual tradition

108
00:09:44,000 --> 00:09:45,000
and the meditative tradition.

109
00:09:45,000 --> 00:09:49,000
These are the two Buddhist traditions that we follow.

110
00:09:49,000 --> 00:09:54,000
So the textual tradition we've got lots of teachings.

111
00:09:54,000 --> 00:10:00,000
We have 84,000 teachings by one estimation.

112
00:10:00,000 --> 00:10:04,000
This is what the texts themselves say that they contain.

113
00:10:04,000 --> 00:10:06,000
84,000 teachings.

114
00:10:06,000 --> 00:10:11,000
That may just be a euphemism or a simplification for a lot.

115
00:10:11,000 --> 00:10:16,000
Because it seems 84,000 was a common number.

116
00:10:16,000 --> 00:10:23,000
But at any rate, we do have a lot.

117
00:10:23,000 --> 00:10:25,000
We've got the vinaiopiti calendar.

118
00:10:25,000 --> 00:10:32,000
It's full of all the different rules and norms of behavior.

119
00:10:32,000 --> 00:10:36,000
Mostly things we shouldn't do, but a lot of prescriptive things

120
00:10:36,000 --> 00:10:38,000
as well that we shouldn't do.

121
00:10:38,000 --> 00:10:43,000
The way to act and speak and deport oneself properly

122
00:10:43,000 --> 00:10:48,000
is sort of a basis for the practice of meditation.

123
00:10:48,000 --> 00:10:56,000
And we have the vinaiopiti come, which is full of a lot of meditative teachings.

124
00:10:56,000 --> 00:10:59,000
It describes all of the many talks that the Buddha gave.

125
00:10:59,000 --> 00:11:02,000
The Buddha was here and said this to this person,

126
00:11:02,000 --> 00:11:09,000
ways and means by which the Buddha passed on his message

127
00:11:09,000 --> 00:11:16,000
and evoked proper inclination.

128
00:11:16,000 --> 00:11:18,000
So it wasn't just about passing on information,

129
00:11:18,000 --> 00:11:27,000
but the vinaiopiti is full of inspiration and adaptation of the teachings

130
00:11:27,000 --> 00:11:31,000
to suit particular audiences.

131
00:11:31,000 --> 00:11:33,000
What you get out of is not just information,

132
00:11:33,000 --> 00:11:42,000
but you get a sense of the real feeling of being a meditator.

133
00:11:42,000 --> 00:11:45,000
And you get this encouragement from the Buddha

134
00:11:45,000 --> 00:11:48,000
and this shaping and molding of our minds

135
00:11:48,000 --> 00:11:55,000
by the ways the Buddha taught and adapted the teachings.

136
00:11:55,000 --> 00:11:58,000
But the great thing about studying all of these

137
00:11:58,000 --> 00:12:03,000
is that you get a broad idea of the different ways

138
00:12:03,000 --> 00:12:05,000
that the teaching can be presented

139
00:12:05,000 --> 00:12:08,000
and you're able to pick that, which resonates with you

140
00:12:08,000 --> 00:12:14,000
and approach the teaching from different ways.

141
00:12:14,000 --> 00:12:18,000
And then we have the abhidhamabhittika which really lays out

142
00:12:18,000 --> 00:12:24,000
the foundation for insight practice by not containing talks

143
00:12:24,000 --> 00:12:29,000
that the Buddha gave, but actually containing pure information.

144
00:12:29,000 --> 00:12:32,000
So giving a pure classification of the dhammas

145
00:12:32,000 --> 00:12:40,000
that really allows, provides an actual precise roadmap.

146
00:12:40,000 --> 00:12:44,000
The connections between good states and happiness

147
00:12:44,000 --> 00:12:48,000
and bad states and suffering

148
00:12:48,000 --> 00:12:53,000
and the different connections of cause and effect

149
00:12:53,000 --> 00:13:04,000
and supportive causes and hindering causes and so on.

150
00:13:04,000 --> 00:13:07,000
And really the way things work together

151
00:13:07,000 --> 00:13:10,000
really gives it a detailed analysis of reality.

152
00:13:10,000 --> 00:13:15,000
And on the whole allows us or gives us a glimpse

153
00:13:15,000 --> 00:13:19,000
and the way we should understand reality

154
00:13:19,000 --> 00:13:23,000
or sort of the paradigm we should undertake.

155
00:13:23,000 --> 00:13:27,000
Even if you don't understand the whole of the abhidhamma,

156
00:13:27,000 --> 00:13:30,000
it's easy to see through the Buddha's description

157
00:13:30,000 --> 00:13:35,000
of the various mind states and the qualities of mind and so on.

158
00:13:35,000 --> 00:13:38,000
That when we speak of reality and Buddhism

159
00:13:38,000 --> 00:13:40,000
what we mean is experience.

160
00:13:40,000 --> 00:13:43,000
It means it's not time and space

161
00:13:43,000 --> 00:13:47,000
and the universe and the cosmos and the planets and the stars.

162
00:13:47,000 --> 00:13:52,000
Not even particles or subatomic particles

163
00:13:52,000 --> 00:13:56,000
and the molecules.

164
00:13:56,000 --> 00:14:00,000
What we mean is the physical and mental aspects of experience.

165
00:14:00,000 --> 00:14:03,000
So it gives us, again, not just information

166
00:14:03,000 --> 00:14:08,000
but it helps us understand, provides a framework

167
00:14:08,000 --> 00:14:12,000
or a foundation for reality.

168
00:14:12,000 --> 00:14:15,000
But again, like this old monk we can bleed off

169
00:14:15,000 --> 00:14:18,000
while studying all of this seems daunting.

170
00:14:18,000 --> 00:14:21,000
For many of us we do have the time.

171
00:14:21,000 --> 00:14:24,000
Many of us don't have the time.

172
00:14:24,000 --> 00:14:29,000
It's important to understand that this is not the end

173
00:14:29,000 --> 00:14:31,000
of the Buddha's teaching.

174
00:14:31,000 --> 00:14:34,000
It's not enough to spend all of your time studying

175
00:14:34,000 --> 00:14:36,000
and so if that's all that you have time for

176
00:14:36,000 --> 00:14:41,000
you really miss the point of the teachings.

177
00:14:41,000 --> 00:14:43,000
So we have to adapt the teachings

178
00:14:43,000 --> 00:14:46,000
and we can expand and contract the teachings.

179
00:14:46,000 --> 00:14:51,000
It's like a hologram in some ways, I think.

180
00:14:51,000 --> 00:14:56,000
You can pick one part and you find the whole and that part.

181
00:14:56,000 --> 00:15:01,000
For example, the three, the three epitchas,

182
00:15:01,000 --> 00:15:07,000
in summary, is deal with silasamadhi and bhanya

183
00:15:07,000 --> 00:15:11,000
morality, concentration and wisdom.

184
00:15:11,000 --> 00:15:21,000
So morality is the things that keep us, keep our actions in our speech and check.

185
00:15:21,000 --> 00:15:40,000
Those actions and words that we should avoid are ways of avoiding gross defilement, actions that we're going to regret and that are going to cause stress and suffering.

186
00:15:40,000 --> 00:15:45,000
Same with speech.

187
00:15:45,000 --> 00:15:49,000
That's basically what the William Piteken does and what it's for.

188
00:15:49,000 --> 00:15:52,000
Why it was organized into that group.

189
00:15:52,000 --> 00:15:56,000
It's what the Piteken mainly deals when it boils down.

190
00:15:56,000 --> 00:16:00,000
It mainly deals with meditation or concentration or focus.

191
00:16:00,000 --> 00:16:04,000
It's about actual practice.

192
00:16:04,000 --> 00:16:24,000
It's practical. It has many methods and means by which the Buddha would help us to cultivate or help us audience to cultivate wholesome states and progress on the path.

193
00:16:24,000 --> 00:16:30,000
And the Abhidhamma Piteken steals with pure wisdom.

194
00:16:30,000 --> 00:16:35,000
It mimics the understanding that a meditator gets through practice.

195
00:16:35,000 --> 00:16:39,000
It describes it, describes what wisdom is.

196
00:16:39,000 --> 00:16:44,000
You can't gain wisdom by reading the Abhidhamma, but you can get a map.

197
00:16:44,000 --> 00:16:49,000
Sort of an outline of framework for what wisdom would look like.

198
00:16:49,000 --> 00:16:54,000
Which can be quite useful. Of course, all of this can get in the way if you study too much.

199
00:16:54,000 --> 00:16:58,000
When you meditate, you're just thinking because thinking is a habit.

200
00:16:58,000 --> 00:17:03,000
Studying itself is a practice and it has consequences.

201
00:17:03,000 --> 00:17:14,000
But if you study just enough and in proportion with your practice, it gives you this map, this framework to allow you to understand reality.

202
00:17:14,000 --> 00:17:19,000
The Deepika really built boils down to morality, concentration and wisdom.

203
00:17:19,000 --> 00:17:29,000
Which can then be expanded if you want to go to understand, it's expanded into the 8-fold number of paths.

204
00:17:29,000 --> 00:17:39,000
The 8-fold number of paths we have right view and right thought, this is wisdom, right speech, right action, right livelihood.

205
00:17:39,000 --> 00:17:47,000
This is concentration, this is morality and right effort, right mindfulness, right concentration.

206
00:17:47,000 --> 00:17:54,000
This is the concentration of the samadhi aspect.

207
00:17:54,000 --> 00:18:06,000
And each of those can then be expanded as well and sometimes you just pick one and you can cultivate each one of them, can become the essence of your practice.

208
00:18:06,000 --> 00:18:12,000
For example, right view is again the Four Noble Truth which also contain the 8-fold noble path.

209
00:18:12,000 --> 00:18:16,000
Right mindfulness of course is the most common one.

210
00:18:16,000 --> 00:18:20,000
Right mindfulness is the Four Foundations of Mindfulness.

211
00:18:20,000 --> 00:18:38,000
And the Four Foundations of Mindfulness again contain all the other elements of contain the Four Noble Truths, for example.

212
00:18:38,000 --> 00:18:46,000
Ultimately if you're really lazy or in a hurry to become enlightened, you don't have to worry too much about studying at all.

213
00:18:46,000 --> 00:18:58,000
In the commentary, the tradition, the texts themselves, the commentary texts, talk about the canonical texts when they say,

214
00:18:58,000 --> 00:19:22,000
sakalam pihiti, pitakang, the entirety of the three pizzicas can be summarized under one word and that's a pamada pad, the path of the path of a pamada, which is vigilance or religions.

215
00:19:22,000 --> 00:19:29,000
A heedfulness sometimes translated.

216
00:19:29,000 --> 00:19:37,000
Which again just means to be mindful.

217
00:19:37,000 --> 00:19:51,000
And so our study whether it be much or whether it be little that comes down to this, it comes down to understanding and getting a sense of the right direction for our practice.

218
00:19:51,000 --> 00:20:02,000
As to Vipassana Dura, Vipassana is a word that probably doesn't need much introduction.

219
00:20:02,000 --> 00:20:05,000
Most of us are familiar with it.

220
00:20:05,000 --> 00:20:10,000
But our understanding may be partial or skewed or so on.

221
00:20:10,000 --> 00:20:11,000
It's important that we understand this.

222
00:20:11,000 --> 00:20:16,000
Vipassana is, I remember many, many years ago, a monk.

223
00:20:16,000 --> 00:20:23,000
I think in a poly lesson, he was talking about poly words and he mentioned the word.

224
00:20:23,000 --> 00:20:26,000
Someone mentioned the word Vipassana and where it comes from.

225
00:20:26,000 --> 00:20:31,000
He said, this word isn't found in the pithika.

226
00:20:31,000 --> 00:20:38,000
This was a scholarly well-versed monk and I was quite surprised to hear this.

227
00:20:38,000 --> 00:20:45,000
The word Vipassana does appear in the pithika, but I think what he meant is that it's not.

228
00:20:45,000 --> 00:20:51,000
Vipassana meditation isn't described in the pithika.

229
00:20:51,000 --> 00:20:57,000
And nonetheless, there's some very good quotes that help us understand Vipassana.

230
00:20:57,000 --> 00:21:00,000
What is it that we're doing here when we come to practice meditation?

231
00:21:00,000 --> 00:21:02,000
What are we practicing?

232
00:21:02,000 --> 00:21:10,000
Very much in the pithika means, which means in the closest thing we have to the actual teachings in the Buddha.

233
00:21:10,000 --> 00:21:21,000
What we are told and what we tend to believe are actual direct teachings handed down that were given by the Buddha.

234
00:21:21,000 --> 00:21:27,000
So the most important one, there's several, but the most important one is,

235
00:21:27,000 --> 00:21:34,000
it doesn't actually mention the word Vipassana, but it uses the words,

236
00:21:34,000 --> 00:21:43,000
it sees with wisdom. So Vipassana, Passana means seeing the prefix,

237
00:21:43,000 --> 00:21:45,000
this is the question, what does it mean?

238
00:21:45,000 --> 00:21:49,000
But we have a very good example of what it means when the Buddha says,

239
00:21:49,000 --> 00:22:06,000
Vipassana, Yaya, Passati, sees with wisdom. When one sees with wisdom, it just means wisdom.

240
00:22:06,000 --> 00:22:14,000
But as we see three things with wisdom. We practice Vipassana meditation, what are we trying to see?

241
00:22:14,000 --> 00:22:24,000
The Buddha said, Sambay, Sankara, Anita, all formations are impermanent.

242
00:22:24,000 --> 00:22:53,000
That looks like the sound is cutting out in second life, which is probably just a problem with that anyway.

243
00:22:53,000 --> 00:22:57,000
Sorry for the interruption.

244
00:22:57,000 --> 00:23:05,000
Impermanent, everything is impermanent. Sambay, Sankara, Anita, all formations are impermanent.

245
00:23:05,000 --> 00:23:15,000
So we come to see through our practice that reality is not made up of things.

246
00:23:15,000 --> 00:23:23,000
People, places, things, these are all concepts.

247
00:23:23,000 --> 00:23:35,000
They arise and cease in the mind, but they arise and cease momentarily, and reality has just been up for moments.

248
00:23:35,000 --> 00:23:52,000
Sambay, Sankara, Dukkha, all formations are Dukkha, which is something we have to hedge and sort of explain grudgingly because it's too harsh if you just say,

249
00:23:52,000 --> 00:23:59,000
all formations are suffering. And we think, well, that's kind of a dreary outlook, and it's not really what is meant.

250
00:23:59,000 --> 00:24:08,000
Again, these are qualities of things. Dukkha means in relation to us, they can't make us happy, and they cause us suffering.

251
00:24:08,000 --> 00:24:13,000
They're not suffering in themselves, but they cause us suffering.

252
00:24:13,000 --> 00:24:18,000
Or they are capable of only providing suffering.

253
00:24:18,000 --> 00:24:22,000
Just like if you have a drink and you say, it's poisonous.

254
00:24:22,000 --> 00:24:26,000
Well, the drink sitting there is not poisonous, it's not going to kill you.

255
00:24:26,000 --> 00:24:30,000
But it's poisonous in relation to us if we drink it.

256
00:24:30,000 --> 00:24:42,000
So all formations are Dukkha means when you cling to them, when you take them on, this is me, this is mine, and so on.

257
00:24:42,000 --> 00:24:47,000
When you seek them out, you suffer.

258
00:24:47,000 --> 00:25:04,000
They are unsatisfying. They are not happiness. They themselves cannot be a source of your happiness.

259
00:25:04,000 --> 00:25:17,000
And finally, Sambheya, Sambheya, and Tamma, and all dhammas, formations, and all realities really are non-self.

260
00:25:17,000 --> 00:25:24,000
Means they have no last thing self. They arise and they cease momentarily.

261
00:25:24,000 --> 00:25:28,000
That's not even true at all dhammas, but the point is there is no self.

262
00:25:28,000 --> 00:25:38,000
There is nothing in them that arises or nothing arisen, and they are not the self.

263
00:25:38,000 --> 00:25:46,000
They are not ours. They are not under our control.

264
00:25:46,000 --> 00:25:55,000
They don't belong to us. They are not me. They are not mine.

265
00:25:55,000 --> 00:26:05,000
Again, this is not theory, but this is how a meditator experiences reality.

266
00:26:05,000 --> 00:26:11,000
Everything will be chaotic when you start to practice. It will shake you up.

267
00:26:11,000 --> 00:26:16,000
This meditation will not be a comfortable thing, at least not in the beginning.

268
00:26:16,000 --> 00:26:23,000
As it forces you to experience this reality, to not have anything to cling to.

269
00:26:23,000 --> 00:26:27,000
How can you survive if you have nothing to hold on to?

270
00:26:27,000 --> 00:26:32,000
This is the challenge. We are accustomed to holding on to fixing in our mind.

271
00:26:32,000 --> 00:26:36,000
This is what I am aiming for. This is my goal. This is what I want.

272
00:26:36,000 --> 00:26:41,000
That keeps us stable. Suffering, but stable.

273
00:26:41,000 --> 00:26:46,000
When you let go of it, how do you live? How do you find peace? How do you be at peace?

274
00:26:46,000 --> 00:26:51,000
It's not easy.

275
00:26:51,000 --> 00:26:55,000
You see the suffering, impermanence, you see suffering.

276
00:26:55,000 --> 00:27:01,000
The stress of things, and you see how when you do hold on to anything.

277
00:27:01,000 --> 00:27:06,000
It's a stressful, watching as things arise, and sees things that you hold on to.

278
00:27:06,000 --> 00:27:08,000
Stay may it stay, they disappear.

279
00:27:08,000 --> 00:27:13,000
Things you worry about, may they not come, may they not come, and then they come.

280
00:27:13,000 --> 00:27:22,000
As you can't control, you're not in charge.

281
00:27:22,000 --> 00:27:26,000
Another important aspect of Yipassana is the present moment.

282
00:27:26,000 --> 00:27:30,000
Another important quote that we have at the Buddha where he says,

283
00:27:30,000 --> 00:27:44,000
But jupana, jayo, dhamma, datatatat, Yipassati. Yipassati is just another form of the word Yipassana.

284
00:27:44,000 --> 00:27:50,000
So one sees clearly everything that arises in the present moment.

285
00:27:50,000 --> 00:27:58,000
Whatever dhamma is arise in the present moment, one sees them clearly.

286
00:27:58,000 --> 00:28:03,000
Then the point here being, it sees clearly impermanence, suffering, non-self,

287
00:28:03,000 --> 00:28:08,000
it sees the three characteristics, but the important thing is here and now.

288
00:28:08,000 --> 00:28:12,000
Meditation isn't about the future, it's not about a goal that you're striving for,

289
00:28:12,000 --> 00:28:18,000
it's not about the past, who you've become and who you are.

290
00:28:18,000 --> 00:28:24,000
It's about seeing reality, which is only in the present moment, and it's only in our experience.

291
00:28:24,000 --> 00:28:28,000
It's not in the world out there, in who we are as our being.

292
00:28:28,000 --> 00:28:32,000
It's not in our relationships with other people.

293
00:28:32,000 --> 00:28:37,000
Reality is in the experience moment to moment.

294
00:28:37,000 --> 00:28:44,000
So we come to see impermanence, suffering, non-self, in our experiences.

295
00:28:44,000 --> 00:28:49,000
Seeing that anytime we cling to anything, we suffer.

296
00:28:49,000 --> 00:28:54,000
Reality is not meant to be clung to it, there's no benefit from clinging,

297
00:28:54,000 --> 00:28:59,000
identifying with things.

298
00:28:59,000 --> 00:29:10,000
We see all this for ourselves, this is what it means, Yipassana.

299
00:29:10,000 --> 00:29:16,000
This is the great duty that we're a great task that we're undertaking.

300
00:29:16,000 --> 00:29:21,000
And this is how we carry on the Buddhist tradition, this is how we carry on.

301
00:29:21,000 --> 00:29:26,000
These traditions that have been handed down century after century,

302
00:29:26,000 --> 00:29:30,000
we continue through our patience, through our care,

303
00:29:30,000 --> 00:29:33,000
we continue to care for the Buddhist teaching,

304
00:29:33,000 --> 00:29:37,000
and pass it on and keep it pure, and keep it alive.

305
00:29:37,000 --> 00:29:44,000
And so I'd like to express my appreciation for everyone for doing this,

306
00:29:44,000 --> 00:29:47,000
for taking up the study of the Buddhist teaching,

307
00:29:47,000 --> 00:29:49,000
even if it just means listening to talks,

308
00:29:49,000 --> 00:29:53,000
and most especially for taking up like this old monk,

309
00:29:53,000 --> 00:29:56,000
while at the very least we take up meditation,

310
00:29:56,000 --> 00:30:01,000
which is still perfectly valid way of carrying on the Indigenous tradition.

311
00:30:01,000 --> 00:30:04,000
Because it's only when we understand the teachings

312
00:30:04,000 --> 00:30:07,000
that we can really and truly pass them on.

313
00:30:07,000 --> 00:30:09,000
So there you go.

314
00:30:09,000 --> 00:30:14,000
So there's the dhamma for tonight. Thank you all for tuning in.

315
00:30:14,000 --> 00:30:39,000
I wish you all good practice.

