1
00:00:00,000 --> 00:00:09,000
Is it appropriate for lay people to tell others with non-Buddhist beliefs about the teachings of the Buddha as long as it isn't a personal interpretation?

2
00:00:09,000 --> 00:00:17,000
I feel I'm in no position to have a perfect understanding of the Buddha's teaching and don't feel I should be teaching others informally.

3
00:00:17,000 --> 00:00:29,000
Well, there's no problem with talking to other people about the teachings of the Buddha.

4
00:00:29,000 --> 00:00:38,000
I think there's a problem with teaching in general. You shouldn't ever think of yourself as a teacher or be trying to teach people.

5
00:00:38,000 --> 00:00:57,000
It's all the better when you know that you don't have a perfect understanding because the real problem would be if you thought you had perfect understanding and you start maybe making up your own understanding and inserting your own ideas into it.

6
00:00:57,000 --> 00:01:09,000
The best is when you admit to yourself as we all do that we don't have perfect understanding and therefore stick to the texts and pass on what you've been told.

7
00:01:09,000 --> 00:01:19,000
This is one important part of the teravada, the teravada, the teaching of the elders. There's a great emphasis placed on the idea that savai heard.

8
00:01:19,000 --> 00:01:27,000
So all the students start a-one may suit tongue-ins based on what you have heard. It's not based on what you think or it's not something you've made up for yourself.

9
00:01:27,000 --> 00:01:36,000
And one should only consider that one is teaching the Buddha's teaching, one one has heard it from someone who is reliable.

10
00:01:36,000 --> 00:01:51,000
So the meaning is you're not inserting your own interpretation, you're trying your best to stick with the interpretation of those who have come before and most especially with the teachings of the Buddha with the teachings of the Buddha himself.

11
00:01:51,000 --> 00:02:13,000
And in that case there's no problem letting people know about the teachings, even leading people in meditation isn't a problem as long as you can avoid it. The important thing is not knowing everything. The important thing is not inserting things that you haven't learned, like making stuff up or interpreting things.

12
00:02:13,000 --> 00:02:22,000
It's okay to say, I don't know, it's okay to say, I really don't have an answer for that, but I can get back to you, or maybe I can find someone who does, or maybe I can't.

13
00:02:22,000 --> 00:02:26,000
But I do know this and this and this and I haven't heard this and this and this.

14
00:02:26,000 --> 00:02:38,000
And give people just that. I mean if you went out and taught people basic meditation, for example, rising, falling and then went on to the feelings, pain, aching soreness and thinking, thinking in here.

15
00:02:38,000 --> 00:02:55,000
If you just went by the book, you could lead someone to become enlightened. You could lead someone through an entire meditation course. The problem is when people start including their own interpretations and that's really what makes or breaks a teacher, I think, or makes one a really good teacher.

16
00:02:55,000 --> 00:03:13,000
Whether they can overcome this fear of being without an answer or this ego, whatever it is, many different things that lead them to add their own interpretations and a lot of weird things.

17
00:03:13,000 --> 00:03:26,000
People who call themselves teachers who have come, for instance, to our monastery and done a little bit of teaching with a little bit of training with our teacher, gone up on their own and just listening to some of the things they say.

18
00:03:26,000 --> 00:03:37,000
And then their students go crazy and so on as a result. Your students won't go crazy. You won't drive your students crazy if you just give them what's in the books.

19
00:03:37,000 --> 00:03:54,000
If you just tell them, say, rising, falling, if anything comes up thinking, if you are pedantic or pedantic is the right word, but if you stick very much by the books to the point that people get bored of you, then you can say, at the very least, they won't go crazy.

20
00:03:54,000 --> 00:04:02,000
Because you can be inspiring and say, yes, yes, follow that, go with it, yes, that's the real self, that's God, that's so hard.

21
00:04:02,000 --> 00:04:15,000
Then you can drive them crazy, for sure, you can be a cause for people that go crazy, but if you just stick to the books and then people say, you know, this is boring, I want something exciting, then you can say, well, this is what I have, and I know that I'm not going to drive them crazy.

22
00:04:15,000 --> 00:04:19,000
It just might drive them away, that's the worst thing.

23
00:04:19,000 --> 00:04:32,000
But the other thing you have to realize, and another important part of your answer to your question is that you can't expect all people in the world or even most people in the world to have any appreciation for these teachings.

24
00:04:32,000 --> 00:04:41,000
They're just not in a position, their ideas are very much more in tune with the ways of the world, this clinging in partiality.

25
00:04:41,000 --> 00:05:09,000
Many people are firm belief in the importance of partiality, the importance of our likes and dislikes, the importance of ego, the importance of self-confidence, the importance of self image, the importance of ambition, the importance of passion, the importance of sexual intercourse, the importance of society, the importance of humanity, the list just goes on and on and on and on, and there are people who will fight very, ardently, in favor of these things.

26
00:05:09,000 --> 00:05:15,920
things. I would say more people will then will be able to see through them and give them

27
00:05:15,920 --> 00:05:23,800
up and be at all interested in giving them up. So I encourage, always encourage and I think

28
00:05:23,800 --> 00:05:28,720
it's important to encourage people to share the teachings. And as I said, give them basic

29
00:05:28,720 --> 00:05:35,680
whatever you've learned, whatever you've learned from this tradition anyway, I don't

30
00:05:35,680 --> 00:05:41,920
want to talk, I don't have any confidence in myself to talk about other traditions. But

31
00:05:41,920 --> 00:05:46,800
whatever you learn from this tradition, for sure, spread that. If you tell other people about

32
00:05:46,800 --> 00:05:55,440
that, I say no problem. There will be no problem at worst. They'll, they will, they will be

33
00:05:55,440 --> 00:06:01,600
turned off by it or so on. And, you know, the worst thing is that they might, if they

34
00:06:01,600 --> 00:06:11,320
practice it, is that they might be a little bit suspicious or unable to develop faith in

35
00:06:11,320 --> 00:06:16,280
it. So they either run away or they move slowly. The point being, even if they stick with it,

36
00:06:16,280 --> 00:06:19,600
they might move slowly because you're not able to explain everything because you don't

37
00:06:19,600 --> 00:06:26,000
have all the answers. And I would, I would say much better that you don't try to make up

38
00:06:26,000 --> 00:06:30,840
answers and let them be suspicious. Let them go slowly. Because all that happens when someone

39
00:06:30,840 --> 00:06:34,320
doesn't have faith in, in what they're doing, it's not that they go on the wrong path is

40
00:06:34,320 --> 00:06:38,240
that they don't go anywhere. So if you can give them even a little bit of faith to, to

41
00:06:38,240 --> 00:06:43,840
walk slowly, slowly, slowly, then that's actually an incredible gift that you've given

42
00:06:43,840 --> 00:06:49,360
to them. You've given them something. It's obviously not as good as if you're an amazing

43
00:06:49,360 --> 00:06:56,840
profound, you know, articulate and skilled, you know, super teacher who can just guide

44
00:06:56,840 --> 00:07:02,320
people like, like the Buddha did for example, could guide anyone, almost anyone, could guide

45
00:07:02,320 --> 00:07:10,760
a great number of people to enlightenment. But what I wouldn't encourage people is trying

46
00:07:10,760 --> 00:07:16,560
to push Buddhism on anyone. Though I suppose you, you'll find that out for yourself. When

47
00:07:16,560 --> 00:07:20,960
you push Buddhism on anyone, people will run away from you in drooms, even people who

48
00:07:20,960 --> 00:07:25,760
might be interested in it will, will leave you behind. Well, we'll not want to have anything

49
00:07:25,760 --> 00:07:34,680
to do with you. So, yes, it's all right. But if you're asking, is it all right for me to

50
00:07:34,680 --> 00:07:41,320
go out and, and proselytize or push people into practicing, then no. But if you're just

51
00:07:41,320 --> 00:07:45,280
asking, then when people ask about Buddhism and say, hey, you know about meditation,

52
00:07:45,280 --> 00:07:51,240
can you teach me? Do it by all means. When I went back home after my course, I started

53
00:07:51,240 --> 00:07:57,480
teaching people, probably, probably far too zealous. And yeah, I turned some people away.

54
00:07:57,480 --> 00:08:03,800
But I got a little bit of instruction on, on, on the stages of knowledge after I'd done

55
00:08:03,800 --> 00:08:10,880
a few courses. And I went and taught some people. There's one, one man who said, a friend

56
00:08:10,880 --> 00:08:16,360
of a friend, the friend came to me and said, this guy, he's, he's interested in Buddhism.

57
00:08:16,360 --> 00:08:19,960
I told him to come and talk to you. Is that okay? And I said, yeah, and so the friend of a friend

58
00:08:19,960 --> 00:08:23,320
came to me and said, can you teach me about Buddhism? I said, sure, I'll take you through

59
00:08:23,320 --> 00:08:29,480
the meditation. So I led him through these stages, you know, first walking, step, second walking,

60
00:08:29,480 --> 00:08:34,440
step, all the sitting techniques that we have, giving him more time, more time. Oh, it was

61
00:08:34,440 --> 00:08:38,840
wonderful. He ended up giving up his, he was a biologist and he cut up rats and he ended

62
00:08:38,840 --> 00:08:44,200
up giving it up. He got to the point where he had so much pain. And I said to him, I said,

63
00:08:44,200 --> 00:08:50,520
you have to make a choice. You have to either be a biologist or be a Buddhist

64
00:08:50,520 --> 00:08:55,160
meditator because you can't do both. I mean, not, not, not, not in, you can't cut up rats

65
00:08:55,160 --> 00:09:00,440
anyway. And he gave it up. I mean, he was in his fourth year in university. He was ready to get his

66
00:09:00,440 --> 00:09:07,000
degree and he gave it up and he became a, he works for the government now, I think in, in Canada.

67
00:09:07,000 --> 00:09:15,320
Really nice guy. And, and there was another man as well who I kind of kept in touch with.

68
00:09:16,040 --> 00:09:20,520
And I wasn't able to give them faith. I think neither of them has any faith in me as a teacher

69
00:09:20,520 --> 00:09:27,320
anymore because, you know, I wasn't able to, you know, obviously I wasn't the, the kind of

70
00:09:27,320 --> 00:09:36,200
person who should inspire faith. Not to say that I am now either, but, so in the end, I don't think,

71
00:09:36,200 --> 00:09:41,080
both of them moved on and have taken other teachers or have, have kind of lost touch with them and

72
00:09:41,080 --> 00:09:45,480
they're not at all interested in what I do. I don't know if they would be now, but, but I feel

73
00:09:45,480 --> 00:09:49,320
like I did something good. So I'm not their teacher and they, they don't think of me as some great

74
00:09:49,320 --> 00:09:54,520
teacher and it's not in my business, but I gave them a gift. No, that's how I feel like good,

75
00:09:54,520 --> 00:10:00,280
good thing I did. And based on that experience, I think anyone could do that because it wasn't

76
00:10:00,280 --> 00:10:05,880
anything special that I had. It was just basic understanding of meditation. If, if you haven't

77
00:10:05,880 --> 00:10:12,680
done, say a meditation course that I wouldn't recommend that leading someone through a course.

78
00:10:12,680 --> 00:10:37,960
But as I said, giving them the basics is certainly not harmful as long as you stick to the books.

