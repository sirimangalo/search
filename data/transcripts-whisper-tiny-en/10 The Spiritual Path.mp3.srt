1
00:00:00,000 --> 00:00:22,920
So, last time I talked about beams on spiritual path, so at this time I will talk about

2
00:00:22,920 --> 00:00:34,860
the spiritual path itself, so that time we learn different individuals going along the

3
00:00:34,860 --> 00:00:49,200
spiritual path, and so this time we need to understand what the spiritual path is, so spiritual

4
00:00:49,200 --> 00:01:01,960
path really means on the practice, so what we do when we claim ourselves to be the

5
00:01:01,960 --> 00:01:12,680
disciples of the Buddha, and we are lucky that Buddha taught us the spiritual path,

6
00:01:12,680 --> 00:01:21,840
but what gave us the spiritual path in stages.

7
00:01:21,840 --> 00:01:35,840
Now the first thing we do as Buddhists, as the followers of the Buddha, is taking refuge,

8
00:01:35,840 --> 00:01:45,040
so we will begin this spiritual path with taking refuge.

9
00:01:45,040 --> 00:01:55,680
When we were little, and when we were afraid, then we ran to our parents for refuge,

10
00:01:55,680 --> 00:02:10,400
so once we got into the lap of our parents, our fears disappeared.

11
00:02:10,400 --> 00:02:23,120
And also when there is some danger or something, we try to go to some place for shelter

12
00:02:23,120 --> 00:02:31,640
from that danger, or say when it is too hot outside, so we get into the building, and the

13
00:02:31,640 --> 00:02:43,000
building becomes a refuge for us from heat outside, and when it is very cold, the same

14
00:02:43,000 --> 00:02:55,080
thing we get into the building, and then we can protect ourselves from cold, so the building

15
00:02:55,080 --> 00:03:07,320
of the house is our refuge from heat or cold, and also other natural disasters like storm

16
00:03:07,320 --> 00:03:13,920
and so on.

17
00:03:13,920 --> 00:03:22,120
When we go along with the journey of Samsara, sometimes we have fears, sometimes there

18
00:03:22,120 --> 00:03:32,200
are dangers, and so when we are confronted with these fears or dangers, we need to have

19
00:03:32,200 --> 00:03:45,320
some kind of refuge, we need to have some persons or something that will help us say

20
00:03:45,320 --> 00:03:51,960
to get free from such fears or dangers.

21
00:03:51,960 --> 00:04:05,560
Now, what is a refuge? In the ancient commentaries, refuge is explained as something

22
00:04:05,560 --> 00:04:18,800
or someone that crushes or destroys our fear or terror or suffering or affliction in

23
00:04:18,800 --> 00:04:36,400
local states and suffering in the Samsara, so in this sense, all the three gems in Buddhism,

24
00:04:36,400 --> 00:04:41,600
the Buddha, the Dhamma, and the Sangha are called refuge.

25
00:04:41,600 --> 00:04:58,160
Now, Buddha is called a refuge because he oppresses the fear in us or fear of beings.

26
00:04:58,160 --> 00:05:08,360
That means he helps us to avoid danger by engaging them in what is beneficial or what

27
00:05:08,360 --> 00:05:20,160
is wholesome and by deterring us from what is an wholesome or what is non-beneficial.

28
00:05:20,160 --> 00:05:24,680
Now, Buddha was like a good teacher.

29
00:05:24,680 --> 00:05:39,320
Buddha always said, to merit and avoid the merit. So, when Buddha said, do merit, that

30
00:05:39,320 --> 00:05:49,600
means he is engaging us to the ex of merit which will be beneficial to us. And when

31
00:05:49,600 --> 00:06:00,480
Buddha said, do not do merit, do not do evil, then he is deterring us from what is not

32
00:06:00,480 --> 00:06:08,800
beneficial for us, the the and wholesome states.

33
00:06:08,800 --> 00:06:24,000
Dhamma is also called refuge. Now, when Dhamma is realized, a person becomes enlightened

34
00:06:24,000 --> 00:06:39,000
and he will be able to cross over to the other shore of the Samsara. And also, even

35
00:06:39,000 --> 00:06:48,560
if he reaches the lowest stages of enlightenment, he gets consolation in the Samsara or

36
00:06:48,560 --> 00:06:57,360
he gets a foothold in this Samsara. So, in this sense, Dhamma is also called a refuge

37
00:06:57,360 --> 00:07:17,200
because it it oppresses fear or danger of beings. Sangha is also called refuge. Now, when

38
00:07:17,200 --> 00:07:25,840
we make offerings to the sangha, then we get abundant results from this offering. When

39
00:07:25,840 --> 00:07:32,840
we be respects to the sangha, we get abundant results from this giving respects and so

40
00:07:32,840 --> 00:07:52,200
on. And so, in one way, sangha helps us to avoid the danger of not getting enough results.

41
00:07:52,200 --> 00:08:01,600
If we give to those who are not worthy of our gifts, we will not get as abundant results

42
00:08:01,600 --> 00:08:10,960
as we give to the sangha. So, sangha is called refuge because it makes our offerings

43
00:08:10,960 --> 00:08:25,720
to them, it makes our ex of an duration to them bring abundant results. As Buddhists,

44
00:08:25,720 --> 00:08:40,120
the Buddha, the Dhamma and the sangha are refuge. And in order to affirm that we are

45
00:08:40,120 --> 00:08:51,240
followers of the three Dhamms, we take refuge. Now, you all know how to take refuge.

46
00:08:51,240 --> 00:09:05,760
So, what is it that we call taking refuge? According to Abidhamma, when you take refuge,

47
00:09:05,760 --> 00:09:11,200
the wholesome consciousness arises in your mind, a crucial consciousness arises in your

48
00:09:11,200 --> 00:09:21,760
mind. And that crucial consciousness is free from defilements that means free from

49
00:09:21,760 --> 00:09:33,360
ignorance of the three Dhamms, free from non-confidence in the three Dhamms. And when

50
00:09:33,360 --> 00:09:46,800
it arises, it takes the three Dhamms as object. And so, taking refuge really means

51
00:09:46,800 --> 00:10:04,040
the arising of that type of consciousness. When we say, I take Buddha as my refuge, that

52
00:10:04,040 --> 00:10:14,080
means I regard the Buddha, the Dhamma and the sangha as the supreme resort, that means

53
00:10:14,080 --> 00:10:26,720
the ultimate resort, the ultimate refuge. So, when we take refuge or the taking refuge

54
00:10:26,720 --> 00:10:33,040
means the arising of that type of consciousness. And when we say the formula, Bodhansar

55
00:10:33,040 --> 00:10:41,360
Nangachami and so on, that consciousness arises in our mind. And it is free from mental

56
00:10:41,360 --> 00:10:54,120
defilements. And it has reverence for the three Dhamms. And at the same time, it takes

57
00:10:54,120 --> 00:11:07,000
the three Dhamms and not other things as the real refuge. And in the commentary, it

58
00:11:07,000 --> 00:11:17,160
is explained that, who is it that takes the refuge? That means what kind of a person

59
00:11:17,160 --> 00:11:26,560
that takes refuge? So, the answer is a person who is endowed with that type of consciousness

60
00:11:26,560 --> 00:11:46,080
or who has that type of consciousness is the one who takes refuge. Take refuge. We simply

61
00:11:46,080 --> 00:11:56,120
regard the Buddha, the Dhamma and sangha as our guides or as our teachers. And taking

62
00:11:56,120 --> 00:12:05,120
refuge does not mean that we have faith in the Buddha and then we don't do anything relying

63
00:12:05,120 --> 00:12:13,280
on the Buddha to give us results. So, taking refuge is just regarding them as our spiritual

64
00:12:13,280 --> 00:12:23,360
guides. And there are two kinds of taking refuge. The first one is super mundane and the

65
00:12:23,360 --> 00:12:32,440
second is mundane. The super mundane taking refuge occurs at the moment of attainment

66
00:12:32,440 --> 00:12:40,800
of enlightenment, at the moment of the arising of path consciousness. So, when path consciousness

67
00:12:40,800 --> 00:12:50,840
arises, a person realizes the four noble truths, taking neighbor as object at that time.

68
00:12:50,840 --> 00:13:00,840
So, at that moment, the taking of refuge is said to be accomplished, firmly established

69
00:13:00,840 --> 00:13:14,640
in that person. So, that is a super mundane taking refuge, mundane taking refuge is for

70
00:13:14,640 --> 00:13:29,360
Buddhism. So, when they take refuge, they suppress the defilements or obstacles to taking

71
00:13:29,360 --> 00:13:38,280
refuge. That means non-confidence in the three gems or ignorance of the three gems

72
00:13:38,280 --> 00:13:48,040
and so on. So, when they take refuge, they understand the three gems properly and so

73
00:13:48,040 --> 00:13:57,920
and they have confidence of faith in the three gems. And also when taking refuge, they

74
00:13:57,920 --> 00:14:07,280
take the excellent qualities of them as object. That means when we take refuge, we think

75
00:14:07,280 --> 00:14:15,960
of the qualities of the Buddha, the Dhamma and the Sangha. So, for Buddhists, the Buddha,

76
00:14:15,960 --> 00:14:26,320
the Dhamma and the Sangha are the objects of faith or confidence. So, when we take refuge,

77
00:14:26,320 --> 00:14:36,880
that means we gain faith in the Buddha, the Dhamma and the Sangha. And that faith helps

78
00:14:36,880 --> 00:14:53,800
us to attain the right view because through faith, we learn the teachings of the Buddha

79
00:14:53,800 --> 00:15:04,040
and we gain the right understanding. So, at the moment of taking refuge, there is

80
00:15:04,040 --> 00:15:14,200
this faith and the right understanding occurring in our minds. Now, there are ten kinds

81
00:15:14,200 --> 00:15:21,680
of meritorious deeds and the last of them is called straightening of view. Straightening

82
00:15:21,680 --> 00:15:36,320
of view means having the right view. So, this right view is rooted in the faith or

83
00:15:36,320 --> 00:15:45,200
confidence in the three gems. Now, the mundane taking refuge is said to be of four kinds

84
00:15:45,200 --> 00:15:52,720
that means, how do we take refuge? So, there are four kinds of taking refuge and the

85
00:15:52,720 --> 00:16:00,000
first is by surrendering oneself through the Buddha, through the Dhamma and through the

86
00:16:00,000 --> 00:16:10,960
Sangha. So, instead of seeing, I dig refuge in the Buddha, we can see, I surrender myself

87
00:16:10,960 --> 00:16:16,240
through the Buddha, through the Dhamma, through the Sangha, I really increase myself, through

88
00:16:16,240 --> 00:16:23,600
the Buddha, through the Dhamma and through the Sangha. So, surrendering oneself to the

89
00:16:23,600 --> 00:16:31,760
Buddha, Dhamma and Sangha is a kind of taking refuge in the three gems.

90
00:16:31,760 --> 00:16:41,280
So, when we surrender ourselves and we see making this day, the beginning, I surrender

91
00:16:41,280 --> 00:16:48,040
myself to the Buddha and so on. Now, making this day, the beginning, I have translated

92
00:16:48,040 --> 00:16:55,680
literally from the Pali, that means just from this day onwards, I surrender myself to

93
00:16:55,680 --> 00:17:09,240
the Buddha and so on. Now, second, kind of mundane taking refuge is by taking the

94
00:17:09,240 --> 00:17:19,360
triple gem as one supreme resort, that means by regarding them as one's best resort.

95
00:17:19,360 --> 00:17:31,200
So, when we use that kind or that method of taking refuge, then we may see making this

96
00:17:31,200 --> 00:17:37,960
day, the beginning, I am the one who have the Buddha as my supreme resort, who have the

97
00:17:37,960 --> 00:17:44,880
Dhamma as my supreme resort and who have the Sangha as my supreme resort. Thus, we got

98
00:17:44,880 --> 00:18:03,000
me, so this is how they express their taking refuge. Sometimes, we may not say I surrender

99
00:18:03,000 --> 00:18:09,280
myself to the Buddha or we will not say I am the one who have the Buddha as my supreme

100
00:18:09,280 --> 00:18:16,720
resort, but we may say I am the cube rule of the Buddha, people of the Dhamma and people

101
00:18:16,720 --> 00:18:23,520
of the Sangha. So, when we say this, we have virtually taken refuge in the Buddha, the

102
00:18:23,520 --> 00:18:33,080
Dhamma and the Sangha. So, that that kind of mundane taking refuge is by becoming a

103
00:18:33,080 --> 00:18:50,160
pupil of the triple gem or by admitting that one is a disciple of the three gems.

104
00:18:50,160 --> 00:19:00,800
The fourth way of mundane taking refuge is by paying homage, etc. by buying down to the

105
00:19:00,800 --> 00:19:10,600
Buddha and so on or by rising from the seat when the Buddha and others come or by showing

106
00:19:10,600 --> 00:19:22,320
reverence to them by with folded hands and also the other acts of respect. So, all of

107
00:19:22,320 --> 00:19:33,120
these are called here, say, paying respect or something like that. So, we may not see the

108
00:19:33,120 --> 00:19:40,600
words like I surrender myself to the Buddha, but when we bow down to the Buddha, then

109
00:19:40,600 --> 00:19:46,440
we are virtually taking refuge in the Buddha. When we show respect to the Buddha, we are

110
00:19:46,440 --> 00:19:58,320
virtually taking refuge in the Buddha and so on. Now, this fourth one is described

111
00:19:58,320 --> 00:20:11,800
as prostration in some translations, but it is not necessarily a prostration.

112
00:20:11,800 --> 00:20:21,120
It is one act of paying respects, but it is not not necessarily a prostration, but just

113
00:20:21,120 --> 00:20:28,680
paying homage or paying respects. So, if you do not bow down to him, but choose respects

114
00:20:28,680 --> 00:20:35,400
to do the Buddha and others in some other ways, then you are virtually taking refuge

115
00:20:35,400 --> 00:20:48,040
in the three chains. So, it is not prostration only, but any form of paying homage.

116
00:20:48,040 --> 00:20:58,360
Now, when we take refuge, we see the formula, Buddha, Saranan, Kachami and so on. So, what

117
00:20:58,360 --> 00:21:08,400
is this? Is it number one? Self-southernally, we do not say I surrender myself to the

118
00:21:08,400 --> 00:21:18,920
Buddha. Then, number three, we do not say I am the people of the Buddha. And number four,

119
00:21:18,920 --> 00:21:27,360
maybe we will be over to the Buddha, but when we say I take Buddha as my refuge, I think

120
00:21:27,360 --> 00:21:34,720
we are following the second one, that means taking the Buddha, Dhamma and the Sanga as

121
00:21:34,720 --> 00:21:45,800
our supreme resort as our refuge. In the formulas given in these four kinds of taking

122
00:21:45,800 --> 00:21:56,480
refuge, there is a phrase making this day the beginning of from this day onward.

123
00:21:56,480 --> 00:22:12,000
I think it is proper to say these words only when we first take refuge. Now, we take refuge

124
00:22:12,000 --> 00:22:18,560
every day. Actually, we took refuge when we were young and then we take refuge every day.

125
00:22:18,560 --> 00:22:24,880
So, when we take for the second time that I am and so on, I do not think we need to say

126
00:22:24,880 --> 00:22:33,560
from this day on, from this day on. If we say, suppose we say today, from this day on,

127
00:22:33,560 --> 00:22:39,840
I take the Buddha as my refuge. That might mean we do not take the Buddha as a refuge

128
00:22:39,840 --> 00:22:48,120
yesterday or in the days long fast. So, these formulas are for those who take for the

129
00:22:48,120 --> 00:22:56,080
first time, the refuge in the Buddha, Dhamma and the Sanga. So, as Buddha is who have

130
00:22:56,080 --> 00:23:03,680
been taking refuge in the three gems almost every day, we do not need to say from today

131
00:23:03,680 --> 00:23:17,000
onwards, from today onwards. Now, the last one, taking refuge by being

132
00:23:17,000 --> 00:23:28,440
homage and others and the last kind is a gay of four kinds. That means, homage and others

133
00:23:28,440 --> 00:23:40,800
shown towards relatives and homage and others motivated by fear because we are afraid

134
00:23:40,800 --> 00:23:56,080
so we will be respite and so on. Our respite shown towards a teacher or he paid homage

135
00:23:56,080 --> 00:24:13,520
based on esteem for spiritual worthiness. That means, the homage paid as a person who is

136
00:24:13,520 --> 00:24:36,200
worthy of accepting our reverence and is worthy of accepting offerings. Now, if a member

137
00:24:36,200 --> 00:24:43,960
of Saka, Kaleen or Kalya, Kaleen pays homage to the Buddha thinking, he is our relative, he

138
00:24:43,960 --> 00:24:52,920
has not thereby taken refuge. So, if you pay homage to the Buddha because Buddha is your

139
00:24:52,920 --> 00:25:07,720
relative, then you do not take refuge. If you pay homage just because Buddha is your relative

140
00:25:07,720 --> 00:25:16,720
and if you pay homage because you are afraid or because you are motivated by fear, then

141
00:25:16,720 --> 00:25:22,800
you do not take precepts. So, it is explained in the commentaries that if one place

142
00:25:22,800 --> 00:25:29,000
homage to the Buddha out of fear thinking, the recluse Gautama is honored by kings and

143
00:25:29,000 --> 00:25:38,120
has great power. If I do not pay homage to him, he may harm me. So, if he pays homage

144
00:25:38,120 --> 00:25:48,320
to the Buddha because of this fear, then he is not taking refuge. Now, in the eastern

145
00:25:48,320 --> 00:26:00,400
cultures, teachers are very much respected. So, people or pupils pay homage to the teachers,

146
00:26:00,400 --> 00:26:12,800
only because they are their teachers. So, if a person pays homage to a person or homage

147
00:26:12,800 --> 00:26:22,640
to a person because that person is his teacher, then he is not taking refuge. Now, the

148
00:26:22,640 --> 00:26:33,840
Buddha, before he became the Buddha, he was a Bodhisattva. So, during the time of Bodhisattva,

149
00:26:33,840 --> 00:26:44,400
he may have taught something to somebody. And if that somebody pays homage to the Buddha

150
00:26:44,400 --> 00:26:55,720
thinking of that advice, he caught when Buddha was a Bodhisattva, then he is not taking refuge.

151
00:26:55,720 --> 00:27:04,720
Not only that, even after he became the Buddha, he may give some advice to some lay people

152
00:27:04,720 --> 00:27:16,000
for their worldly prosperity. Now, sometimes, Buddha give instructions to people how to spend

153
00:27:16,000 --> 00:27:32,640
their money. You want to know? One portion of their income, they must use for their

154
00:27:32,640 --> 00:27:40,400
life, for their living and for their enjoyment. And with two portions of their income,

155
00:27:40,400 --> 00:27:48,320
they are to do business. And the last or the fourth portion, they must put aside for emergencies.

156
00:27:48,320 --> 00:27:56,840
So, you divide your income into four parts. And then with one part, you spend at home

157
00:27:56,840 --> 00:28:02,480
and for your enjoyment and for your families enjoyment. And with fifty percent of your income,

158
00:28:02,480 --> 00:28:10,720
you do business. And the last twenty-five percent, you put in a bank. So, when emergencies

159
00:28:10,720 --> 00:28:20,080
come, you can solve this problem. So, sometimes, Buddha gave such an advice. Suppose

160
00:28:20,080 --> 00:28:28,400
a person hears that advice, and then he pays so much to the Buddha as a teacher, because

161
00:28:28,400 --> 00:28:35,600
Buddha taught this. Then he is not taking refuge in the Buddha, because he pays so much

162
00:28:35,600 --> 00:28:46,600
to the Buddha, because he learns that thing from the Buddha, that way of how to spend

163
00:28:46,600 --> 00:28:58,880
his money, and not for the attainment of enlightenment. So, in such a case, a person

164
00:28:58,880 --> 00:29:17,720
lying down to the Buddha is not taking refuge in the Buddha. So, even though you pay

165
00:29:17,720 --> 00:29:25,520
homage to a person, if you pay homage to him, because he is your relative, or because

166
00:29:25,520 --> 00:29:34,840
you are afraid that if you do not pay homage to him to you, or because he is your teacher,

167
00:29:34,840 --> 00:29:47,120
then you are not taking refuge. So, the real taking refuge happens only when you pay homage

168
00:29:47,120 --> 00:29:58,560
and others based on a respect for his spiritual worthiness. That means you pay homage

169
00:29:58,560 --> 00:30:07,240
to the Buddha, because you believe, or you understand that the Buddha was the best of the

170
00:30:07,240 --> 00:30:20,720
person who are worthy of accepting reverence and accepting offerings. So, only in such a case

171
00:30:20,720 --> 00:30:35,160
is refuge set to be taken. Now, we go the other way around. Suppose you have taken refuge,

172
00:30:35,160 --> 00:30:44,000
not suppose, you have taken refuge in the treble gem, then you have a relative who belongs

173
00:30:44,000 --> 00:30:52,960
to another religion in the treble gem, then you have a relative who belongs to another

174
00:30:52,960 --> 00:31:06,960
religion. Let us say, a priest, a Catholic priest, you will be homage to that Catholic

175
00:31:06,960 --> 00:31:16,920
priest who is your relative. If you are taking refuge program, you have taken refuge

176
00:31:16,920 --> 00:31:25,520
in the three gems. Now, you have paying respects or you are paying homage to a priest

177
00:31:25,520 --> 00:31:35,600
of another religion, but he is your relative. If you are taking refuge, broken or breached

178
00:31:35,600 --> 00:31:54,760
that way, no. Now, during the time of the kings, people have to pay homage to the kings,

179
00:31:54,760 --> 00:32:04,000
people have bow down to the kings. So, you bow down to the king, because if you do not bow

180
00:32:04,000 --> 00:32:15,080
down, he may do harm to you. So, out of fear, you bow down to the king. So, in that case

181
00:32:15,080 --> 00:32:37,800
to you are taking refuge is not broken. You have a teacher and that is a happens to be

182
00:32:37,800 --> 00:32:47,600
a priest of another religion. You bow down to the teacher, because he is your teacher.

183
00:32:47,600 --> 00:33:00,560
Yes, you are taking refuge in the three gems, breached, no. You may be preached. He

184
00:33:00,560 --> 00:33:08,960
is not your relative. He is not your teacher and you are not afraid of him. But you think

185
00:33:08,960 --> 00:33:18,240
that since he is a priest, although he belongs to another religion, you have to pay, it

186
00:33:18,240 --> 00:33:26,240
is good to be homage to him and you be homage to him. What about you are taking refuge

187
00:33:26,240 --> 00:33:41,120
in the three gems? Is it broken or not? It is not explaining me in the commonaries. So,

188
00:33:41,120 --> 00:33:54,760
we have to decide for ourselves. I think so long as you do not take that person to be

189
00:33:54,760 --> 00:34:12,600
your teacher in getting out of Samsara, your teacher in the practice of meditation, your

190
00:34:12,600 --> 00:34:21,680
teacher in getting rid of mental defilements. I think your taking refuge in the three

191
00:34:21,680 --> 00:34:40,240
gems is still intact. Okay, suppose there is a woman who is pregnant and then she says,

192
00:34:40,240 --> 00:34:57,520
My child in my womb takes refuge in the Buddha and so on. For the child in my womb,

193
00:34:57,520 --> 00:35:08,360
I take refuge in the Buddha and so on. Has the child, has the heart of that? Has the

194
00:35:08,360 --> 00:35:24,840
child taken refuge in that way? During the time of the Buddha, there was a prince called

195
00:35:24,840 --> 00:35:41,640
Bodhi. So, he invited the Buddha to his house and offered me to the Buddha and then he

196
00:35:41,640 --> 00:35:53,640
asked about Buddha's practice, Buddha's striving to become the Buddha and Buddha related

197
00:35:53,640 --> 00:36:08,200
to him in detail, what she practiced during six years in the in the forest. And then at the end,

198
00:36:08,200 --> 00:36:16,720
a friend of his said, friend, you are praising the Buddha, but you did not take refuge in the

199
00:36:16,720 --> 00:36:24,240
Buddha. Then prince Mori said, although I do not take refuge in the Buddha when I am grown

200
00:36:24,240 --> 00:36:38,240
up, when I was in the womb of my mother, my mother took refuge for me. So, he considered

201
00:36:38,240 --> 00:36:51,520
himself to be one who has taken refuge in the three chains. But the commentary explained that there

202
00:36:51,520 --> 00:37:05,680
is no taking refuge without knowing, without mind. So, that means although his mother took refuge

203
00:37:05,680 --> 00:37:15,360
for him when he was in a womb, actually he did not take refuge because he did not know. But after his

204
00:37:15,360 --> 00:37:30,080
birth, the mother told him that she had taken refuge for him when he was in a womb and he knows at

205
00:37:30,080 --> 00:37:37,920
that time that his mother had taken refuge. And so, he takes refuge at that time, then he

206
00:37:40,720 --> 00:37:59,040
he takes refuge at that time. So, taking refuge is to be done with full awareness or

207
00:37:59,040 --> 00:38:11,120
understanding. So, when we take refuge or when we say bow down Sarana and Kachami and so on,

208
00:38:12,960 --> 00:38:19,520
we must have the understanding that we are taking refuge in the Buddha and so on.

209
00:38:20,720 --> 00:38:27,920
Now, in countries like Burma, people are so used to saying these formulas that they just say

210
00:38:27,920 --> 00:38:36,000
the formula, they will not even know the meaning of what the formula is. I have asked many,

211
00:38:36,000 --> 00:38:44,880
many people, what was it that you say, the Buddha and Sarana and Kachami and so on, what was that,

212
00:38:44,880 --> 00:38:59,280
what is the meaning of that, that didn't know. So, in order to get to get refuge seriously,

213
00:38:59,280 --> 00:39:04,720
you have to understand what you are saying when you say bow down Sarana and Kachami. So,

214
00:39:04,720 --> 00:39:14,000
at least even if you do not know the meaning of the individual words, please understand that when

215
00:39:14,000 --> 00:39:19,680
you say this formula, you are taking refuge in the Buddha. This much you must understand,

216
00:39:19,680 --> 00:39:31,040
that is what I tell them always, because taking refuge is a serious undertaking, a serious act of

217
00:39:32,240 --> 00:39:43,840
confidence in the triple-gen and so we must do it with full conviction, not as an act

218
00:39:43,840 --> 00:40:00,480
we do routinely or not doing it slightly. So, we must understand the meaning of the formula

219
00:40:01,280 --> 00:40:09,680
and also we must know at that time, at that time we say bow down Sarana and Kachami and so on,

220
00:40:09,680 --> 00:40:16,720
that we are taking refuge in the Buddha or we are regarding the Buddha as our spiritual guide.

221
00:40:23,440 --> 00:40:33,280
Now, Buddhists take refuge many times in their lives, almost every day they take refuge.

222
00:40:33,280 --> 00:40:43,520
So, we must take refuge every day, it is absolutely necessary to take refuge every day

223
00:40:45,680 --> 00:40:55,920
or what if a person is not able to take refuge because of busyness,

224
00:40:55,920 --> 00:41:03,200
Kannita, what can you say that on that day he is without refuge.

225
00:41:08,880 --> 00:41:14,720
Now, the refuge once taken, we will stay with you

226
00:41:14,720 --> 00:41:37,600
how many years, three years, five years, as long as you keep it until you give it up or until you die.

227
00:41:37,600 --> 00:41:52,000
So, actually, it is not necessary to take refuge every day but we take refuge every day or almost

228
00:41:52,000 --> 00:42:03,200
every day because we want to affirm our confidence in the triple-gen, we want to affirm our faith

229
00:42:03,200 --> 00:42:11,840
in the triple-gen, we want to affirm that we are always the disciples of the three chains

230
00:42:13,040 --> 00:42:23,040
and by taking refuge, we get only kusala, by taking refuge, we get kusala and so taking refuge

231
00:42:23,040 --> 00:42:32,400
is a good act and so even though it is not absolutely necessary, I think we should do it every day.

232
00:42:36,800 --> 00:42:46,880
And the commentary tells us about what is called defilement of the refuge,

233
00:42:46,880 --> 00:42:55,920
that means defilement in having refuge or impurities in having refuge.

234
00:42:58,480 --> 00:43:06,560
Now, as to supermunding refuge, there is no defilement because supermunding

235
00:43:06,560 --> 00:43:09,200
refuge is taken by those who are enlightened.

236
00:43:09,200 --> 00:43:19,040
But in mundane refuge, of mundane refuge, there can be defilement.

237
00:43:19,760 --> 00:43:28,480
When we are ignorant of the three gems or when we have doubts about the three gems

238
00:43:28,480 --> 00:43:40,080
or when we have misunderstanding about the three gems, then our refuge or having refuge is said

239
00:43:40,080 --> 00:43:55,840
to be impure, it is said to be contaminated. When is the refuge broken?

240
00:43:55,840 --> 00:44:10,800
Now, when it is breached, there can be breach of mundane refuge only.

241
00:44:13,200 --> 00:44:15,440
There is no breach of supermunding refuge.

242
00:44:15,440 --> 00:44:31,120
And there are two kinds of breach of mundane refuge and that is blame worthy and blameless.

243
00:44:33,200 --> 00:44:41,440
Blame worthy means bringing painful results and blameless means bringing no results.

244
00:44:41,440 --> 00:44:53,440
Now, the blame worthy breach of refuge occurs when one does surrendering oneself, etc.

245
00:44:53,440 --> 00:45:06,080
to other teachers, etc. That means one goes to another teacher, another religion and take that

246
00:45:06,080 --> 00:45:16,800
religion. That means when one gives up once a confidence in the triple gem,

247
00:45:19,040 --> 00:45:26,080
and that kind of breach is called blame worthy breach because from the Buddhist one of you,

248
00:45:26,080 --> 00:45:41,200
it is blame worthy and it is said that it brings undesirable results. A blameless breach of refuge

249
00:45:41,200 --> 00:45:59,120
occurs when a person dies. And when that blameless breach occurs, there is no no results

250
00:45:59,120 --> 00:46:11,760
because death is itself a result of karma in the past. Since it is a result, it has no

251
00:46:16,320 --> 00:46:25,120
ability to give results or it is not result giving. So, that kind of breach does not bring

252
00:46:25,120 --> 00:46:36,960
any undesirable results or others. Also, now,

253
00:46:36,960 --> 00:46:56,560
at Buddhist funeral services, the monks contacted the services and then what

254
00:46:56,560 --> 00:47:14,240
we will do at these services is first take the refuge and precepts and then they may make some

255
00:47:14,240 --> 00:47:29,440
offerings and then they share a marriage with the one who has passed away. Now, in Bama,

256
00:47:31,840 --> 00:47:40,720
that service is called something like giving refuge. So, having someone takes refuge.

257
00:47:40,720 --> 00:47:54,000
And many people think that that is making that dead body take refuge. Actually, dead body is

258
00:47:54,000 --> 00:48:02,080
dead body and it is like a lock and so it does not know anything at all. But I think the idea behind

259
00:48:02,080 --> 00:48:12,160
it is that it was not dies and then he is reborn in another existence. So, when he is reborn in

260
00:48:12,160 --> 00:48:24,640
another existence, he has no refuge in that new life. So, those who are left behind want him to take

261
00:48:24,640 --> 00:48:34,960
refuge or to have refuge in his new existence. And so, they invite him to take refuge along with them

262
00:48:35,920 --> 00:48:45,840
and take refuge from the say from the take refuge in the triple gym following the

263
00:48:45,840 --> 00:49:03,760
lead of the monk. So, these services, the funerals of Buddhist funeral services are to have those

264
00:49:03,760 --> 00:49:16,400
who have departed and in one sense, especially in Bama, it is having them take refuge in their next

265
00:49:16,400 --> 00:49:21,520
life so that they are persons who have refuge in the three or three gems.

266
00:49:21,520 --> 00:49:38,560
So, the Bama is called this, how do you translate that? Putting him on refuge or something like that.

267
00:49:40,480 --> 00:49:48,160
Now, there is no breach of super mundane refuge because that is the refuge of the enlightened ones.

268
00:49:48,160 --> 00:49:57,520
And it is explained in the commentaries that even in another life, a noble disciple does not point

269
00:49:57,520 --> 00:50:09,040
to some other teacher. That means, suppose he sought up on a dice here and he is reborn as a human being

270
00:50:09,040 --> 00:50:22,880
in another place. Even though he may be reborn of a appearance of other religion, he will

271
00:50:24,880 --> 00:50:32,240
not lose his refuge in the Buddha Dhamma and the Sangha. That means, he will be a Buddhist

272
00:50:32,240 --> 00:50:39,760
even though he is born to non Buddhist parents. So, there is no breach of super mundane refuge

273
00:50:39,760 --> 00:50:50,880
even in other lives. But, the refuge of a Buddha in a can be broken even when he is living

274
00:50:51,680 --> 00:50:54,720
and surely at the time of his death.

275
00:50:54,720 --> 00:51:08,960
Now, in the beginning I said taking refuge is the first act we do as Buddhists. So,

276
00:51:10,240 --> 00:51:14,000
if a person wants to become a Buddhist, what must he do?

277
00:51:16,880 --> 00:51:24,080
So, just take refuge in the three gems. So, by taking three gems, he declares himself to

278
00:51:24,080 --> 00:51:31,440
be a disciple of the Buddha or in modern times, he declares himself to be a Buddhist.

279
00:51:35,040 --> 00:51:40,720
So, as Buddhist we take refuge every day and it is a good act.

280
00:51:40,720 --> 00:51:52,080
But, before he passed away, Buddha advised his disciples,

281
00:51:52,080 --> 00:52:09,760
be like islands unto yourselves, be refuge to yourself do not have any other S refuge.

282
00:52:12,240 --> 00:52:20,400
So, we have two statements, it is good to take refuge in the three gems

283
00:52:20,400 --> 00:52:25,760
and the Buddha said do not take any other S refuge, take yourself as your own refuge.

284
00:52:31,280 --> 00:52:41,920
Now, when we take refuge in the three gems, we record them as our guides, as our spiritual

285
00:52:41,920 --> 00:52:54,960
teachers, that does not mean that we rely on them totally for our deliverance from the suffering.

286
00:52:56,640 --> 00:53:06,720
But when Buddha said do not have any other S refuge, that means rely on your own self

287
00:53:06,720 --> 00:53:19,120
for your deliverance, do not rely on any other person for your own deliverance, because that is an

288
00:53:19,120 --> 00:53:28,560
impossible thing. So, it is right to say that we take refuge in the Buddha, Dhamma and Sanga,

289
00:53:29,120 --> 00:53:34,320
and also it is right to say that we do not take any other for refuge, we take ourselves as

290
00:53:34,320 --> 00:53:42,960
refuge, and then Buddha continued his explanation that taking oneself as refuge means actually

291
00:53:44,640 --> 00:53:52,560
what? Taking Dhamma as refuge, and taking Dhamma as refuge means practice of full foundation

292
00:53:52,560 --> 00:54:04,160
of mindfulness. So, when Buddha said take yourself as refuge, that means practice mindfulness,

293
00:54:04,160 --> 00:54:11,840
there are foundations of mindfulness. So, these two are not in, how about that?

294
00:54:11,840 --> 00:54:24,480
Not in contrast, or what do you say then, in a contradiction with each other.

295
00:54:24,480 --> 00:54:49,360
So, in two, to express our confidence, in the triple gym, we will take refuge every day,

296
00:54:49,360 --> 00:54:59,200
and to follow the advice of the Buddha that we have ourselves as our refuge,

297
00:55:00,880 --> 00:55:10,480
we will practice the full foundations of mindfulness. So, when you are practicing the full

298
00:55:10,480 --> 00:55:16,880
foundations of mindfulness, when you are practicing vipasana, meditation, and you are following the

299
00:55:16,880 --> 00:55:28,320
advice of the Buddha, and you are taking yourself as refuge. So, we take refuge in the three

300
00:55:28,320 --> 00:55:39,520
gyms, we regard the three gyms as our infallible spiritual guides, and we practice the four

301
00:55:39,520 --> 00:55:48,320
foundations of mindfulness to follow the advice that we should have ourselves as refuge.

302
00:55:49,280 --> 00:55:58,000
So, by taking refuge in the three gyms, and by taking refuge in ourselves,

303
00:55:58,000 --> 00:56:13,040
we hope to be able to reach the final goal of all Buddhists, that is, total purification of mind,

304
00:56:13,040 --> 00:56:28,960
and realization of nibhana.

