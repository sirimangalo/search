1
00:00:00,000 --> 00:00:07,000
So today we continue on first study with Emma Pama, going on to verses 19 and 20.

2
00:00:07,000 --> 00:00:10,000
It's comprised of single stories.

3
00:00:10,000 --> 00:00:12,000
The verses are as follows.

4
00:00:12,000 --> 00:00:19,000
The verses are as follows.

5
00:00:19,000 --> 00:00:26,000
The verses are as follows.

6
00:00:26,000 --> 00:00:33,000
The verses are as follows.

7
00:00:33,000 --> 00:00:40,000
The verses are as follows.

8
00:00:40,000 --> 00:00:47,000
The verses are as follows.

9
00:00:47,000 --> 00:00:54,000
The verses are as follows.

10
00:00:54,000 --> 00:01:05,000
The verses are exceptionally common.

11
00:01:05,000 --> 00:01:12,000
The verses are as Operators are singing.

12
00:01:12,000 --> 00:01:16,000
The verses are even together.

13
00:01:16,000 --> 00:01:23,000
The verses are opportunities.

14
00:01:23,000 --> 00:01:31,000
being able to teach much that is related to the goal or that is abuse.

15
00:01:31,000 --> 00:01:38,800
But Nattakarohoti, being one who doesn't practice accordingly.

16
00:01:38,800 --> 00:01:43,400
Nattaroh, a man who, a person who doesn't practice accordingly,

17
00:01:43,400 --> 00:01:51,800
who is rather pamato, who is negligent or heedless.

18
00:01:51,800 --> 00:01:55,600
So as a person who, though they know a lot and they teach a lot,

19
00:01:55,600 --> 00:02:00,600
doesn't practice accordingly and is negligent.

20
00:02:00,600 --> 00:02:06,600
Gopo, wagah, wagah, nayamri, some like a person who looks after the cows of another,

21
00:02:06,600 --> 00:02:10,200
who counts the cows of another person.

22
00:02:10,200 --> 00:02:13,000
Naba, govassam, a manyasohoti.

23
00:02:13,000 --> 00:02:21,200
Such a person doesn't take, doesn't partake of the blessings of the holy life or the fruits of the holy life.

24
00:02:21,200 --> 00:02:27,200
And the opposing verse, a pampijasamitamasam, a person who knows even a little bit,

25
00:02:27,200 --> 00:02:32,600
even though they know a little bit, or are able to speak a little bit of the truth.

26
00:02:32,600 --> 00:02:41,800
But they are damasa, they are anudamachari, a person who practices the dhamma in according to the dhamma,

27
00:02:41,800 --> 00:02:46,200
or in order to realize the dhamma and realize who is the dhamma.

28
00:02:46,200 --> 00:02:54,200
Aragan, jedbo, sanchat, paha, yamohang, they abandon with the abandoning of delusion,

29
00:02:54,200 --> 00:02:59,200
greed, greed, anger, and delusion.

30
00:02:59,200 --> 00:03:12,200
They have clear comprehension, samat, pajanoh, and surimutajitoh, their mind is well freed, well liberated.

31
00:03:12,200 --> 00:03:19,200
Anupadhiya, no, not clinging to anything.

32
00:03:19,200 --> 00:03:23,200
Or in terms of the future of the future of life.

33
00:03:23,200 --> 00:03:26,200
Satba, govassam, a manyasohoti.

34
00:03:26,200 --> 00:03:33,200
Such a person partakes, is one who partakes in the holy life or in the fruits of the holy life.

35
00:03:33,200 --> 00:03:36,200
So, this is word by word translation, basically.

36
00:03:36,200 --> 00:03:42,200
It means a person who, though they know a lot, but doesn't practice accordingly.

37
00:03:42,200 --> 00:03:45,200
And our negligent and heedless and so on.

38
00:03:45,200 --> 00:03:49,200
They don't have a part in the fruit of the teachings.

39
00:03:49,200 --> 00:03:55,200
But a person who even though they've studied a little bit and maybe no very little,

40
00:03:55,200 --> 00:03:57,200
and can explain very little.

41
00:03:57,200 --> 00:04:05,200
But still, by practicing according to the teaching, they become one who partakes

42
00:04:05,200 --> 00:04:07,200
of the fruit of the truth.

43
00:04:07,200 --> 00:04:09,200
Obviously, this makes sense.

44
00:04:09,200 --> 00:04:14,200
Through practicing the teachings, if you practice accordingly, then give up.

45
00:04:14,200 --> 00:04:21,200
Loba dosamoharaga, lust, dosa, anger, and mohar delusion.

46
00:04:21,200 --> 00:04:23,200
Someone who has clear comprehension.

47
00:04:23,200 --> 00:04:27,200
So, it's actually practicing in the Buddhist teaching on mindfulness.

48
00:04:27,200 --> 00:04:33,200
And he becomes surimutajitohas, freedom in the mind.

49
00:04:33,200 --> 00:04:36,200
So, this is given in regards to a story.

50
00:04:36,200 --> 00:04:43,200
Again, I'm pulling out the commentary here because it turns out that the English is quite negligent

51
00:04:43,200 --> 00:04:44,200
for motto.

52
00:04:44,200 --> 00:04:48,200
The person who translated it didn't do a very good job.

53
00:04:48,200 --> 00:04:53,200
So, in the last story, and in this story, I realized I'm probably going to have to use this

54
00:04:53,200 --> 00:05:00,200
book to check every one and actually read the poly because it's not precise.

55
00:05:00,200 --> 00:05:02,200
The meaning is not as given.

56
00:05:02,200 --> 00:05:06,200
And in the last one, it was actually quite important because what it said is that a person

57
00:05:06,200 --> 00:05:16,200
who has reached the second stage, this woman who called her younger brother, and they

58
00:05:16,200 --> 00:05:20,200
say the reason she got sick is because she wanted a husband.

59
00:05:20,200 --> 00:05:22,200
But she was a second academy.

60
00:05:22,200 --> 00:05:26,200
So, the idea that she would starve herself because she didn't have a husband.

61
00:05:26,200 --> 00:05:28,200
She was a bit ludicrous.

62
00:05:28,200 --> 00:05:33,200
She was someone who was very high in teaching, and that's not what actually was the text

63
00:05:33,200 --> 00:05:34,200
says.

64
00:05:34,200 --> 00:05:39,200
This one is not so important, but it's mixing up some parts of it.

65
00:05:39,200 --> 00:05:42,200
And it's saying that the...

66
00:05:42,200 --> 00:05:45,200
Anyway, I'll read the story and then I'll explain what the disparity is.

67
00:05:45,200 --> 00:05:52,200
So, there were these two friends, inseparable friends, and they became monks in the Buddhist

68
00:05:52,200 --> 00:05:53,200
teaching.

69
00:05:53,200 --> 00:05:58,200
One was older, one was...they weren't the same age, one older, one younger.

70
00:05:58,200 --> 00:06:01,200
And the Buddha said so.

71
00:06:01,200 --> 00:06:05,200
Tell them that there are two duties in the Buddhist teaching.

72
00:06:05,200 --> 00:06:08,200
When someone becomes a monk, they have two duties.

73
00:06:08,200 --> 00:06:13,200
One is to practice Bipasana and the others to study the text or to memorize the text.

74
00:06:13,200 --> 00:06:19,200
The older one said, well, being old, probably it's going to be difficult for me to learn

75
00:06:19,200 --> 00:06:23,200
all of the teachings and to memorize them and be able to recite them.

76
00:06:23,200 --> 00:06:25,200
Can I just do the Bipasana part?

77
00:06:25,200 --> 00:06:28,200
Or he said to himself, I'll just do the Bipasana part.

78
00:06:28,200 --> 00:06:33,200
I'll just do the part of having to see things clearly as they are.

79
00:06:33,200 --> 00:06:37,200
So, he took up some ragrobes.

80
00:06:37,200 --> 00:06:41,200
He went and got some rags and put together a set of robes.

81
00:06:41,200 --> 00:06:45,200
And went to the Buddha and asked for a meditation teaching.

82
00:06:45,200 --> 00:06:50,200
And the Buddha gave him a teaching in regards to the practice of Bipasana meditation,

83
00:06:50,200 --> 00:06:52,200
all the way up to Arahanshi.

84
00:06:52,200 --> 00:06:55,200
He went off into the forest and became an Arahan.

85
00:06:55,200 --> 00:06:59,200
The other one, because he was younger, he said, well, I'll just do the study side of things.

86
00:06:59,200 --> 00:07:03,200
So, he said, okay, I'll take up and learn all of the Buddha's teaching.

87
00:07:03,200 --> 00:07:06,200
And he actually was able to memorize all of the Topitika.

88
00:07:06,200 --> 00:07:09,200
I believe that is what it says.

89
00:07:09,200 --> 00:07:14,200
Anyway, he was able to memorize quite a bit of the...if not all of the Topitika, the Buddha's teaching.

90
00:07:14,200 --> 00:07:17,200
But he didn't practice.

91
00:07:17,200 --> 00:07:22,200
And because he was so famous from his teaching, he had lots and lots of students coming to see him.

92
00:07:22,200 --> 00:07:25,200
So, he was a very well-known teacher and well-respected and lots of...

93
00:07:25,200 --> 00:07:32,200
Something like 18 groups or something, I don't know what it says in here.

94
00:07:32,200 --> 00:07:37,200
And the one in the forest has an Arahan.

95
00:07:37,200 --> 00:07:41,200
It so happened that a group of monks, they got training from the Buddha,

96
00:07:41,200 --> 00:07:44,200
and then they went off to the forest and went to where he was staying.

97
00:07:44,200 --> 00:07:48,200
Practiceed under him and became Arahans as well.

98
00:07:48,200 --> 00:07:54,200
When they became Arahans, they went to see the elder and they asked permission to go back and see the Buddha.

99
00:07:54,200 --> 00:07:56,200
They said, we'd like to see the Buddha, the Buddha said, fine.

100
00:07:56,200 --> 00:08:01,200
Go back and pay respect to the Buddha, and pay respect to the Buddha's tutored,

101
00:08:01,200 --> 00:08:04,200
chief disciples, and all of the 80 great disciples.

102
00:08:04,200 --> 00:08:09,200
And then go and pay respect to my friend and tell them, you're paying respect in the name of your teacher.

103
00:08:09,200 --> 00:08:14,200
So, they did this, they went to the Buddha and so on, and they went to find this old friend

104
00:08:14,200 --> 00:08:19,200
and said, we pay respect to the name of our teacher and they said, who is your teacher?

105
00:08:19,200 --> 00:08:22,200
And he said, it's your friend who you became a monk together.

106
00:08:22,200 --> 00:08:26,200
And he said, he has students, what did he teach you?

107
00:08:26,200 --> 00:08:27,200
Did he teach you?

108
00:08:27,200 --> 00:08:28,200
What could you have learned from him?

109
00:08:28,200 --> 00:08:35,200
Could you have learned even one Nikai or one Pithika or what could you possibly have learned from him?

110
00:08:35,200 --> 00:08:40,200
Because he doesn't know anything, he didn't study, what do you mean teacher?

111
00:08:40,200 --> 00:08:45,200
And so he said this kind of thing, and then he thought to himself, when this monk comes,

112
00:08:45,200 --> 00:08:47,200
I'm going to ask him some questions.

113
00:08:47,200 --> 00:08:53,200
I'm going to show everyone that he is a frog, really.

114
00:08:53,200 --> 00:08:58,200
Because he can't be a teacher, he doesn't know anything.

115
00:08:58,200 --> 00:09:04,200
And it so happened that eventually this senior, the arrow hunt, came back to where the Buddha

116
00:09:04,200 --> 00:09:09,200
and the Buddha was paid respect to the Buddha and went to see his old friend and sat down.

117
00:09:09,200 --> 00:09:18,200
And the younger monk, who was the study monk, he thought, okay, now I'll ask him some questions.

118
00:09:18,200 --> 00:09:22,200
And I'll show him and his students were there and the older monk students were there and he said,

119
00:09:22,200 --> 00:09:27,200
in front of all these people, we'll show everyone what this monk, that this monk knows nothing.

120
00:09:27,200 --> 00:09:32,200
And the Buddha found out about this, he knew what was going on and got the gist of it

121
00:09:32,200 --> 00:09:36,200
and realized that if he let this happen, this younger monk might go to hell.

122
00:09:36,200 --> 00:09:40,200
He says, nearly young, it says in here somewhere that, oh no, it's on the last page,

123
00:09:40,200 --> 00:09:41,200
that's why I don't see him.

124
00:09:41,200 --> 00:09:46,200
He said, if I let him be, he'll go to near you, he'll go to hell near you,

125
00:09:46,200 --> 00:09:52,200
but he might go to near you to hell if I leave him, if I let this happen.

126
00:09:52,200 --> 00:09:58,200
So the Buddha went walking through the monastery and went to see these months.

127
00:09:58,200 --> 00:10:04,200
And he sat down on the seat that they had ready because they always have a buddha seat ready.

128
00:10:04,200 --> 00:10:08,200
And he asked the Buddha to ask questions.

129
00:10:08,200 --> 00:10:18,200
And this is where the disparity is because what really happens is he asked first he asked the study monk,

130
00:10:18,200 --> 00:10:20,200
about the first genre.

131
00:10:20,200 --> 00:10:22,200
And the first monk can't answer.

132
00:10:22,200 --> 00:10:26,200
He asks, the other monk, the monk has been practicing about the first genre,

133
00:10:26,200 --> 00:10:28,200
no problem answering.

134
00:10:28,200 --> 00:10:31,200
And he asks, second genre, same thing.

135
00:10:31,200 --> 00:10:33,200
Third genre, fourth genre.

136
00:10:33,200 --> 00:10:40,200
Although all of the eight, the fourth group of genres and the fourth group of genres,

137
00:10:40,200 --> 00:10:42,200
he asks about all of them.

138
00:10:42,200 --> 00:10:49,200
And it says in the text that the study monk wasn't able to answer a single one of them.

139
00:10:49,200 --> 00:10:54,200
And on the other hand, the practice monk was able to answer every single one of them.

140
00:10:54,200 --> 00:10:56,200
He was able to answer them.

141
00:10:56,200 --> 00:11:00,200
Then he asked about the first path, the sort upon the path.

142
00:11:00,200 --> 00:11:04,200
And the study monk wasn't able to answer in this practice monk was.

143
00:11:04,200 --> 00:11:06,200
So he's asking questions that really directly to the practice.

144
00:11:06,200 --> 00:11:09,200
So it must be something like, what is this experience?

145
00:11:09,200 --> 00:11:10,200
Like, what is that experience?

146
00:11:10,200 --> 00:11:12,200
Or what happens during this experience?

147
00:11:12,200 --> 00:11:14,200
Can you describe it to me?

148
00:11:14,200 --> 00:11:18,200
And because it wasn't in the text, it wasn't something you learned from heart.

149
00:11:18,200 --> 00:11:23,200
You had no knowledge of it and no answer.

150
00:11:23,200 --> 00:11:26,200
So the first, second, third, fourth.

151
00:11:26,200 --> 00:11:31,200
And each time the Buddha, when the practice monk answered correctly,

152
00:11:31,200 --> 00:11:37,200
he would hold his hands up inside and express his appreciation.

153
00:11:37,200 --> 00:11:42,200
And then all of the students of the study monk,

154
00:11:42,200 --> 00:11:44,200
they started muttering about this.

155
00:11:44,200 --> 00:11:47,200
And they said, why is the Buddha keep praising the younger monk?

156
00:11:47,200 --> 00:11:51,200
And they said, isn't it crazy that the Buddha keeps praising this monk when there was nothing?

157
00:11:51,200 --> 00:11:54,200
And the Buddha turned to them and said, what are you talking about?

158
00:11:54,200 --> 00:11:56,200
And they said, let me explain what they're saying.

159
00:11:56,200 --> 00:12:00,200
And the Buddha said, monks, your teacher is like a person

160
00:12:00,200 --> 00:12:04,200
who looks after the cows of another farmer.

161
00:12:04,200 --> 00:12:05,200
He looks after them.

162
00:12:05,200 --> 00:12:06,200
He doesn't get any of the milk.

163
00:12:06,200 --> 00:12:08,200
He doesn't get the butter.

164
00:12:08,200 --> 00:12:09,200
He doesn't get the cheese.

165
00:12:09,200 --> 00:12:12,200
He doesn't get any of the fruits of the cow.

166
00:12:12,200 --> 00:12:14,200
But my son, mama puta.

167
00:12:14,200 --> 00:12:21,200
My mama puta was being said, I think, I believe,

168
00:12:21,200 --> 00:12:24,200
is like the farmer who owns the cow.

169
00:12:24,200 --> 00:12:27,200
And then he gave these two verses.

170
00:12:27,200 --> 00:12:31,200
So the discrepancy is that the English translation makes it out

171
00:12:31,200 --> 00:12:36,200
that the study monk was able to answer them,

172
00:12:36,200 --> 00:12:40,200
which isn't the case anyway, just a small point.

173
00:12:40,200 --> 00:12:41,200
More important.

174
00:12:41,200 --> 00:12:44,200
This is a very important one from practical point of view,

175
00:12:44,200 --> 00:12:48,200
especially because it details some points of practice.

176
00:12:48,200 --> 00:12:52,200
And some characteristics of a person who doesn't practice.

177
00:12:52,200 --> 00:12:55,200
So this is a reoccurring theme in the Buddha's teaching

178
00:12:55,200 --> 00:13:01,200
that the study of the text doesn't make one of the followers of the Buddha.

179
00:13:01,200 --> 00:13:03,200
Even though you can study all of this,

180
00:13:03,200 --> 00:13:07,200
and many monks will practically memorize this commentary.

181
00:13:07,200 --> 00:13:09,200
This is how they learn poly.

182
00:13:09,200 --> 00:13:11,200
They did a little bit of it.

183
00:13:11,200 --> 00:13:14,200
I learned maybe a half of this.

184
00:13:14,200 --> 00:13:15,200
I tried.

185
00:13:15,200 --> 00:13:18,200
And it's a good thing to study.

186
00:13:18,200 --> 00:13:20,200
But the person who, not takaro,

187
00:13:20,200 --> 00:13:23,200
who, the person doesn't practice accordingly.

188
00:13:23,200 --> 00:13:25,200
It doesn't take the teachings to heart.

189
00:13:25,200 --> 00:13:26,200
It isn't mindful.

190
00:13:26,200 --> 00:13:29,200
Pamatu, if you know the last words of the Buddha,

191
00:13:29,200 --> 00:13:32,200
it was a pamat, a pamat, and some pamat.

192
00:13:32,200 --> 00:13:35,200
So don't be negligent.

193
00:13:35,200 --> 00:13:37,200
These were his last words.

194
00:13:37,200 --> 00:13:43,200
So here, a person who's being negligent is very much the core of what the Buddha told us not to do.

195
00:13:43,200 --> 00:13:46,200
So that means that for someone who's teaching,

196
00:13:46,200 --> 00:13:48,200
wasting their time,

197
00:13:48,200 --> 00:13:51,200
just with teaching, just with spreading the dominant two others,

198
00:13:51,200 --> 00:13:54,200
and not actually practicing on their own,

199
00:13:54,200 --> 00:13:56,200
that it's considered to be negligent.

200
00:13:56,200 --> 00:14:00,200
And, of course, in their mind, they're heedless.

201
00:14:00,200 --> 00:14:02,200
They're not aware when they're emotional arising.

202
00:14:02,200 --> 00:14:05,200
You see this in many scholarly monks,

203
00:14:05,200 --> 00:14:07,200
or scholars of anything.

204
00:14:07,200 --> 00:14:11,200
They can become quite conceited and attached to their knowledge,

205
00:14:11,200 --> 00:14:17,200
and aren't able to recognize when,

206
00:14:17,200 --> 00:14:19,200
or aren't able to recognize the defilements

207
00:14:19,200 --> 00:14:22,200
in themselves, and aren't able to eradicate them.

208
00:14:22,200 --> 00:14:26,200
On the other hand, a person who doesn't even study anything,

209
00:14:26,200 --> 00:14:30,200
or, let's say, doesn't study anything up, up,

210
00:14:30,200 --> 00:14:32,200
means little.

211
00:14:32,200 --> 00:14:36,200
Because the story goes that he studied enough to become an aura.

212
00:14:36,200 --> 00:14:38,200
This is like when someone has a map,

213
00:14:38,200 --> 00:14:40,200
and one studies the route, one's going.

214
00:14:40,200 --> 00:14:42,200
One doesn't have to study over here or over there,

215
00:14:42,200 --> 00:14:44,200
one just has to study this way.

216
00:14:44,200 --> 00:14:46,200
Okay, and then I reach here and there,

217
00:14:46,200 --> 00:14:48,200
and then there's this sign and turn right and so on.

218
00:14:48,200 --> 00:14:49,200
And that's it.

219
00:14:49,200 --> 00:14:52,200
You don't have to worry about the rest of the map.

220
00:14:52,200 --> 00:14:55,200
This is what it means by up that means enough.

221
00:14:55,200 --> 00:14:57,200
You learn just enough.

222
00:14:57,200 --> 00:15:02,200
And so this is the difference between these two months.

223
00:15:02,200 --> 00:15:04,200
One of them learned the whole map,

224
00:15:04,200 --> 00:15:06,200
which is useful because he had lots of students

225
00:15:06,200 --> 00:15:08,200
and maybe some of his students became a light.

226
00:15:08,200 --> 00:15:10,200
There was a story that I always tell him,

227
00:15:10,200 --> 00:15:12,200
but this monk who just did teaching,

228
00:15:12,200 --> 00:15:14,200
and all of his students became a light.

229
00:15:14,200 --> 00:15:16,200
One of his students became an anigami,

230
00:15:16,200 --> 00:15:18,200
and thought, what is our teacher?

231
00:15:18,200 --> 00:15:21,200
What is our teacher game?

232
00:15:21,200 --> 00:15:23,200
And he thought about it and realized our teacher

233
00:15:23,200 --> 00:15:25,200
is still an ordinary human being.

234
00:15:25,200 --> 00:15:28,200
So he went and he performed some magic

235
00:15:28,200 --> 00:15:31,200
and he levitated off the floor in front of his teacher.

236
00:15:31,200 --> 00:15:33,200
He said, time for a lesson,

237
00:15:33,200 --> 00:15:35,200
but he didn't mean that the way his teacher thought in his eyes.

238
00:15:35,200 --> 00:15:37,200
I have no time for a lesson and it's so busy.

239
00:15:37,200 --> 00:15:39,200
And then he floated up in the air and he said,

240
00:15:39,200 --> 00:15:40,200
you have no time for yourself.

241
00:15:40,200 --> 00:15:42,200
I don't want to lesson from you.

242
00:15:42,200 --> 00:15:44,200
He flew out the window.

243
00:15:44,200 --> 00:15:47,200
Apparently.

244
00:15:47,200 --> 00:15:51,200
And this monk got very distressed

245
00:15:51,200 --> 00:15:54,200
and he actually left and went off to practice meditation as a result.

246
00:15:54,200 --> 00:15:57,200
And if you've heard the story,

247
00:15:57,200 --> 00:15:58,200
if you have an end of the story,

248
00:15:58,200 --> 00:16:01,200
he goes off into the forest and he's all alone.

249
00:16:01,200 --> 00:16:03,200
But he's so famous that the angels think

250
00:16:03,200 --> 00:16:06,200
they can learn from him.

251
00:16:06,200 --> 00:16:08,200
So the angels come and they want to,

252
00:16:08,200 --> 00:16:09,200
oh, he's practicing meditation.

253
00:16:09,200 --> 00:16:11,200
Let's practice accordingly.

254
00:16:11,200 --> 00:16:14,200
And so he walks and they walk and he sits and they sit.

255
00:16:14,200 --> 00:16:17,200
And then he gets so frustrated that he's not gaining anything

256
00:16:17,200 --> 00:16:19,200
that he sits down and starts crying.

257
00:16:19,200 --> 00:16:21,200
And suddenly this angel appears beside him

258
00:16:21,200 --> 00:16:23,200
and it's sitting there crying.

259
00:16:23,200 --> 00:16:26,200
And he said, what are you crying for?

260
00:16:26,200 --> 00:16:28,200
And the angel said, well, I see you crying.

261
00:16:28,200 --> 00:16:30,200
I thought that must be the way to do it.

262
00:16:30,200 --> 00:16:32,200
The way to practice.

263
00:16:32,200 --> 00:16:35,200
I'm not worried.

264
00:16:35,200 --> 00:16:39,200
He felt so ashamed that he actually started taking it seriously

265
00:16:39,200 --> 00:16:41,200
and he woke up and said, this is stupid.

266
00:16:41,200 --> 00:16:44,200
And he just gave himself a slap in the face

267
00:16:44,200 --> 00:16:46,200
and sobered up, I guess.

268
00:16:46,200 --> 00:16:49,200
And then he became an aura.

269
00:16:49,200 --> 00:16:51,200
So the key is to not be language.

270
00:16:51,200 --> 00:16:54,200
And the person who is negligent means someone

271
00:16:54,200 --> 00:16:56,200
who is not being mindful is the Buddha's side.

272
00:16:56,200 --> 00:16:58,200
The person who is always mindful,

273
00:16:58,200 --> 00:17:03,200
this is what means to be vigilant or to be hateful.

274
00:17:06,200 --> 00:17:08,200
And so here, and then in the next verse it talks about,

275
00:17:08,200 --> 00:17:12,200
what are some of the requisites for being hateful?

276
00:17:12,200 --> 00:17:15,200
The must devotee, anu dhamma kadim,

277
00:17:15,200 --> 00:17:18,200
someone who practices the teacher.

278
00:17:18,200 --> 00:17:20,200
So you take the four foundations of mindfulness

279
00:17:20,200 --> 00:17:22,200
and they don't sit around thinking about them

280
00:17:22,200 --> 00:17:24,200
or studying about them or teaching them.

281
00:17:24,200 --> 00:17:26,200
When we're citing them, however,

282
00:17:26,200 --> 00:17:29,200
you take the time to go and find a cave

283
00:17:29,200 --> 00:17:33,200
or find a tree or find a hut and practice.

284
00:17:36,200 --> 00:17:37,200
And then what do you do?

285
00:17:37,200 --> 00:17:40,200
Raghan, chadhu, santraha, yamoham.

286
00:17:40,200 --> 00:17:43,200
This is key because some people don't quite,

287
00:17:43,200 --> 00:17:46,200
it isn't quite clear what we're trying to do

288
00:17:46,200 --> 00:17:49,200
and what are the measurements of our practice.

289
00:17:49,200 --> 00:17:51,200
So the way we measure our practice is based on

290
00:17:51,200 --> 00:17:53,200
the amount of Raghan, the amount of dose,

291
00:17:53,200 --> 00:17:57,200
the amount of moha, the greed, anger, and delusion.

292
00:17:57,200 --> 00:17:59,200
The more that we're able to do away with

293
00:17:59,200 --> 00:18:01,200
the further we've gone in the practice,

294
00:18:01,200 --> 00:18:04,200
the further our measure of our progress

295
00:18:04,200 --> 00:18:07,200
is how much we've been able to do away with this.

296
00:18:07,200 --> 00:18:10,200
So do we have less Raghan, less lust, less anger,

297
00:18:10,200 --> 00:18:13,200
less delusion, and the core.

298
00:18:13,200 --> 00:18:17,200
And then the way to abandon it is in the next verse

299
00:18:17,200 --> 00:18:21,200
from the map, pajan, no, through the practice of

300
00:18:21,200 --> 00:18:24,200
some map, which means right, and pajan,

301
00:18:24,200 --> 00:18:29,200
which means clear seeing, we're seeing thoroughly.

302
00:18:29,200 --> 00:18:32,200
We translate this off to this clear comprehension,

303
00:18:32,200 --> 00:18:37,200
some pajan, some pajan, no, some pajan, yet.

304
00:18:37,200 --> 00:18:43,200
But it really means the perfect awareness of reality.

305
00:18:43,200 --> 00:18:45,200
So in a sense, when you watch the stomach,

306
00:18:45,200 --> 00:18:49,200
it's simply simple, perfectly knowing what's going on.

307
00:18:49,200 --> 00:18:51,200
This is the rise, and this is the fall.

308
00:18:51,200 --> 00:18:54,200
When you're walking, knowing that you're for this movie,

309
00:18:54,200 --> 00:18:56,200
when you're sitting, knowing that you're sitting,

310
00:18:56,200 --> 00:18:58,200
so when you're in this awareness,

311
00:18:58,200 --> 00:19:00,200
when you have this full and complete,

312
00:19:00,200 --> 00:19:04,200
and some, which means proper awareness.

313
00:19:04,200 --> 00:19:08,200
So it means there's no, it's just full knowing.

314
00:19:08,200 --> 00:19:10,200
You know, this is rising, you don't like it,

315
00:19:10,200 --> 00:19:12,200
or dislike it, or cling to it.

316
00:19:12,200 --> 00:19:14,200
And whatever experience arises,

317
00:19:14,200 --> 00:19:16,200
so we hear the noise, you're not getting upset,

318
00:19:16,200 --> 00:19:18,200
or attracted to it.

319
00:19:18,200 --> 00:19:21,200
We see something not to be attached to it,

320
00:19:21,200 --> 00:19:23,200
or a pulse band.

321
00:19:23,200 --> 00:19:26,200
To just rightly seeing it as it is,

322
00:19:26,200 --> 00:19:28,200
this is some map pajan, no.

323
00:19:28,200 --> 00:19:30,200
So we motif it.

324
00:19:30,200 --> 00:19:34,200
So we motif it, though, with a mind that is well released.

325
00:19:34,200 --> 00:19:37,200
Well released means, and there are different kinds of release,

326
00:19:37,200 --> 00:19:39,200
the commentary talks about this,

327
00:19:39,200 --> 00:19:41,200
but in the next page.

328
00:19:41,200 --> 00:19:43,200
But the most important one is,

329
00:19:43,200 --> 00:19:45,200
it's like the covering up of the roof.

330
00:19:45,200 --> 00:19:47,200
You can cover it up with morality,

331
00:19:47,200 --> 00:19:49,200
you can cover it up with concentration.

332
00:19:49,200 --> 00:19:51,200
The most important is to cover,

333
00:19:51,200 --> 00:19:53,200
cover up the roof with wisdom.

334
00:19:53,200 --> 00:19:56,200
To create a seal with wisdom,

335
00:19:56,200 --> 00:20:00,200
so that your mind has no room for it to arise,

336
00:20:00,200 --> 00:20:03,200
because the mind is perfectly airtight,

337
00:20:03,200 --> 00:20:06,200
perfect, complete knowledge.

338
00:20:06,200 --> 00:20:08,200
You know the truth.

339
00:20:08,200 --> 00:20:10,200
I mean, basically, it means to see the bottom,

340
00:20:10,200 --> 00:20:12,200
because once you see the bottom of your mind

341
00:20:12,200 --> 00:20:14,200
is perfectly protected.

342
00:20:14,200 --> 00:20:17,200
The first time when a person becomes a sotapana,

343
00:20:17,200 --> 00:20:20,200
the mind is perfectly protected from,

344
00:20:20,200 --> 00:20:23,200
or is perfectly freed from,

345
00:20:23,200 --> 00:20:26,200
what is it,

346
00:20:26,200 --> 00:20:28,200
Sakaya Dukti,

347
00:20:28,200 --> 00:20:29,200
some view of self,

348
00:20:29,200 --> 00:20:32,200
Wichikitat,

349
00:20:32,200 --> 00:20:34,200
Sila Bhattaparamasa,

350
00:20:34,200 --> 00:20:38,200
which means attachment to wrong practices,

351
00:20:38,200 --> 00:20:41,200
and wrong morality.

352
00:20:41,200 --> 00:20:44,200
So, abstaining from things that it's not necessary to abstain from,

353
00:20:44,200 --> 00:20:48,200
taking on practice that are in practices that are not necessary to practice,

354
00:20:48,200 --> 00:20:51,200
thinking that by abstaining and by practicing in this way,

355
00:20:51,200 --> 00:20:55,200
or thinking that there's abstentions in these practices are necessary.

356
00:20:55,200 --> 00:20:57,200
You never do that, because you know what is the right practice,

357
00:20:57,200 --> 00:21:00,200
and you know what leads to the result,

358
00:21:00,200 --> 00:21:02,200
because you've been there.

359
00:21:02,200 --> 00:21:04,200
And Wichikitat which means no doubt,

360
00:21:04,200 --> 00:21:06,200
because you've seen the bhana,

361
00:21:06,200 --> 00:21:08,200
you have no doubt about whether the bhana exists,

362
00:21:08,200 --> 00:21:13,200
or look at Wichikitat, because you've come to see the truth for yourself.

363
00:21:13,200 --> 00:21:15,200
Then when you get Sakithikami,

364
00:21:15,200 --> 00:21:17,200
Anangami, and Arahantah,

365
00:21:17,200 --> 00:21:19,200
then you become an Arahantah,

366
00:21:19,200 --> 00:21:21,200
and the mind is suimutatat,

367
00:21:21,200 --> 00:21:23,200
and the mind is suimutat,

368
00:21:23,200 --> 00:21:25,200
which means well freed,

369
00:21:25,200 --> 00:21:27,200
there's no greed, no anger, no delusion,

370
00:21:27,200 --> 00:21:29,200
no ignorance,

371
00:21:29,200 --> 00:21:32,200
so full understanding of the vulnerable truth,

372
00:21:32,200 --> 00:21:34,200
everything that you experience has seen,

373
00:21:34,200 --> 00:21:35,200
just for what it is,

374
00:21:35,200 --> 00:21:37,200
there's some up and down,

375
00:21:37,200 --> 00:21:41,200
some pajanya.

376
00:21:41,200 --> 00:21:46,200
So that's the final story of the Yamakawaga,

377
00:21:46,200 --> 00:21:49,200
in a group of pairs,

378
00:21:49,200 --> 00:21:51,200
the first wagga.

379
00:21:51,200 --> 00:21:54,200
And it's a reminder for us that

380
00:21:54,200 --> 00:21:58,200
what we are doing is the proper practice of the Buddha,

381
00:21:58,200 --> 00:21:59,200
we should have proud,

382
00:21:59,200 --> 00:22:00,200
because many people,

383
00:22:00,200 --> 00:22:02,200
even people who are born Buddhist,

384
00:22:02,200 --> 00:22:05,200
have found Buddhism, Westerners,

385
00:22:05,200 --> 00:22:07,200
who find Buddhism,

386
00:22:07,200 --> 00:22:09,200
will often just spend their life studying,

387
00:22:09,200 --> 00:22:11,200
and talking and discussing,

388
00:22:11,200 --> 00:22:14,200
and trying to appreciate intellectual,

389
00:22:14,200 --> 00:22:17,200
and they consider intellectual appreciation

390
00:22:17,200 --> 00:22:20,200
to be the realization.

391
00:22:20,200 --> 00:22:22,200
They miss the fruit of the holy life,

392
00:22:22,200 --> 00:22:25,200
because they don't actually practice it.

393
00:22:25,200 --> 00:22:28,200
So, you should feel proud,

394
00:22:28,200 --> 00:22:29,200
not proud and seated,

395
00:22:29,200 --> 00:22:30,200
but you should feel confident,

396
00:22:30,200 --> 00:22:32,200
and feel good about yourself.

397
00:22:32,200 --> 00:22:33,200
So we should remind ourselves

398
00:22:33,200 --> 00:22:36,200
that what we are doing is a good thing.

399
00:22:36,200 --> 00:22:37,200
Remind ourselves means,

400
00:22:37,200 --> 00:22:39,200
look at the benefits that we've gained,

401
00:22:39,200 --> 00:22:43,200
and not disparage the wisdom

402
00:22:43,200 --> 00:22:44,200
that we're gaining in the practice.

403
00:22:44,200 --> 00:22:45,200
It's just every moment

404
00:22:45,200 --> 00:22:46,200
you're gaining something,

405
00:22:46,200 --> 00:22:48,200
learning something more about yourself.

406
00:22:48,200 --> 00:22:50,200
On the other side,

407
00:22:50,200 --> 00:22:52,200
at the same time, it's easy to become discouraged,

408
00:22:52,200 --> 00:22:54,200
because it's very difficult,

409
00:22:54,200 --> 00:22:56,200
and you're experiencing things,

410
00:22:56,200 --> 00:22:57,200
you're dealing with things

411
00:22:57,200 --> 00:23:00,200
that you're so used to covering up and avoiding.

412
00:23:00,200 --> 00:23:02,200
So when they come up again,

413
00:23:02,200 --> 00:23:05,200
that the instinct is to repress them

414
00:23:05,200 --> 00:23:08,200
to push them away to run away from.

415
00:23:08,200 --> 00:23:10,200
So it can be quite disheartening,

416
00:23:10,200 --> 00:23:14,200
and some people actually decide they want to run away,

417
00:23:14,200 --> 00:23:16,200
they don't want to deal with it anymore.

418
00:23:16,200 --> 00:23:18,200
It's important to remind us something,

419
00:23:18,200 --> 00:23:20,200
this is really what we've gained already,

420
00:23:20,200 --> 00:23:22,200
or what we're doing here is something

421
00:23:22,200 --> 00:23:26,200
quite important and quite incredible.

422
00:23:26,200 --> 00:23:29,200
Remember the goodness of what we're doing,

423
00:23:29,200 --> 00:23:33,200
we're able to don't lose sight

424
00:23:33,200 --> 00:23:35,200
of what we've gained or what we're gaining,

425
00:23:35,200 --> 00:23:37,200
because you're learning all the time

426
00:23:37,200 --> 00:23:38,200
about yourself,

427
00:23:38,200 --> 00:23:39,200
about how the mind works,

428
00:23:39,200 --> 00:23:41,200
about how reality works,

429
00:23:41,200 --> 00:23:44,200
and that's a very precious,

430
00:23:44,200 --> 00:23:46,200
precious gift.

431
00:23:46,200 --> 00:23:49,200
So that's the story for today,

432
00:23:49,200 --> 00:23:51,200
and it's always a little bit on the battlefield,

433
00:23:51,200 --> 00:23:54,200
you set this session,

434
00:23:54,200 --> 00:23:58,200
and tomorrow or whenever we'll get on to the second,

435
00:23:58,200 --> 00:24:00,200
second chapter,

436
00:24:00,200 --> 00:24:29,200
so thanks for tuning in and back to the competition.

