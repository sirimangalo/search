1
00:00:00,000 --> 00:00:03,000
.

2
00:00:03,000 --> 00:00:06,000
.

3
00:00:06,000 --> 00:00:11,000
.

4
00:00:11,000 --> 00:00:16,000
.

5
00:00:16,000 --> 00:00:19,000
.

6
00:00:19,000 --> 00:00:24,000
..

7
00:00:24,000 --> 00:00:25,000
.

8
00:00:25,000 --> 00:00:26,000
.

9
00:00:26,000 --> 00:00:27,000
.

10
00:00:27,000 --> 00:00:55,400
Good evening, everyone.

11
00:00:55,400 --> 00:01:03,400
Welcome to Evening Dhamma.

12
00:01:03,400 --> 00:01:10,000
Today we're looking at...

13
00:01:10,000 --> 00:01:18,400
Today we're looking at Anapanasati.

14
00:01:18,400 --> 00:01:22,400
Mindfulness of breathing.

15
00:01:22,400 --> 00:01:27,400
This is part of the Satipatanasupa.

16
00:01:27,400 --> 00:01:32,400
The first real practical teaching.

17
00:01:32,400 --> 00:01:39,400
So we've gotten into the actual instruction, meditation instruction.

18
00:01:39,400 --> 00:01:48,400
And predictably, understandably, the Buddha begins with the breath.

19
00:01:48,400 --> 00:02:03,400
Buddhist meditation is very much involved with the mindfulness of breathing.

20
00:02:03,400 --> 00:02:12,400
It's a staple Buddhist meditation practice.

21
00:02:12,400 --> 00:02:18,400
In fact, it's not even something unique to Buddhism.

22
00:02:18,400 --> 00:02:27,400
And it's not a mystery as to why we choose the breath, always as our foundation for the practice.

23
00:02:27,400 --> 00:02:32,400
Even the...

24
00:02:32,400 --> 00:02:41,400
A little bit of experience, practicing meditation will show you that the breath is a prominent.

25
00:02:41,400 --> 00:02:51,400
Experience.

26
00:02:51,400 --> 00:02:55,400
When you're sitting still and you've closed your eyes,

27
00:02:55,400 --> 00:03:05,400
unless there's loud noises or sharp pains in your leg, maybe from sitting cross-legged.

28
00:03:05,400 --> 00:03:14,400
Once you get comfortable sitting, the most prominent object is going to be the breath.

29
00:03:14,400 --> 00:03:22,400
Not only is it prominent, but it's stable and constant.

30
00:03:22,400 --> 00:03:25,400
Compared to everything else, you're going to experience it.

31
00:03:25,400 --> 00:03:36,400
And it's conveniently easy to pay attention to.

32
00:03:36,400 --> 00:03:39,400
So starting with the breath really makes sense.

33
00:03:39,400 --> 00:03:41,400
First of all, it's physical.

34
00:03:41,400 --> 00:03:47,400
And the physical is obviously the most...

35
00:03:47,400 --> 00:04:00,400
obvious, or the most easily discernible of all the foreign sati-patanda.

36
00:04:00,400 --> 00:04:05,400
So this is under mindfulness of the body, mindfulness of the body comes first,

37
00:04:05,400 --> 00:04:16,400
with good reason, because it really is the easiest for especially a new meditator to focus on.

38
00:04:16,400 --> 00:04:29,400
The mindfulness of breathing, interestingly enough, the practice that we follow,

39
00:04:29,400 --> 00:04:35,400
we often don't shut we should often shy away from calling and mindfulness of breathing.

40
00:04:35,400 --> 00:04:41,400
Because mindfulness of breathing is technically a little bit complicated.

41
00:04:41,400 --> 00:04:57,400
It's an involved process, and it's complicated because it doesn't fit squarely in some of the practice, or we pass in a practice.

42
00:04:57,400 --> 00:05:07,400
When you're practicing mid-double on a mind for the cultivation of love or kindness, that's clearly some of that.

43
00:05:07,400 --> 00:05:09,400
There's no question there.

44
00:05:09,400 --> 00:05:14,400
Because your object is a human being, human beings are just concept.

45
00:05:14,400 --> 00:05:17,400
So the focus of your attention is not something that really exists.

46
00:05:17,400 --> 00:05:20,400
It's not your actual experience.

47
00:05:20,400 --> 00:05:25,400
It's a person or people.

48
00:05:25,400 --> 00:05:29,400
If you focus on a color, say white,

49
00:05:29,400 --> 00:05:42,400
well white is a concept, and it's something that you imagine in your mind, you create it.

50
00:05:42,400 --> 00:05:53,400
And as a result, these kinds of meditation allow you to get very focused and calm, fixed on a single object.

51
00:05:53,400 --> 00:05:58,400
But it's not awareness of what's really happening.

52
00:05:58,400 --> 00:06:03,400
It's not awareness that allows you to understand impermanence, suffering, and know them so.

53
00:06:03,400 --> 00:06:04,400
So it's not we pass it.

54
00:06:04,400 --> 00:06:06,400
Now on upon us that the...

55
00:06:06,400 --> 00:06:14,400
Even though the we see the manga places it in with all the other summit practices,

56
00:06:14,400 --> 00:06:19,400
and the tradition, the terabyte of tradition, would like to say,

57
00:06:19,400 --> 00:06:20,400
hey, all of those are summit.

58
00:06:20,400 --> 00:06:24,400
It's not really so cut and dried.

59
00:06:24,400 --> 00:06:34,400
Not just with on upon us that, but especially with on upon us that.

60
00:06:34,400 --> 00:06:35,400
I'm going to go some...

61
00:06:35,400 --> 00:06:42,400
It does a good job, I think, of explaining the various uses of on upon us that the end.

62
00:06:42,400 --> 00:06:48,400
The way he puts it is really based on, I think, how the Buddha taught it.

63
00:06:48,400 --> 00:06:57,400
I'm starting with summit and cultivating the practice up into the point where you're really no longer focusing on the breath.

64
00:06:57,400 --> 00:07:00,400
You're focusing on the concept of the breath.

65
00:07:00,400 --> 00:07:03,400
And it's some kind of...

66
00:07:03,400 --> 00:07:08,400
Some people don't agree with his assessment and have different experiences,

67
00:07:08,400 --> 00:07:14,400
but he himself says people, everyone has different experiences.

68
00:07:14,400 --> 00:07:19,400
And experiences, the absorption differently, but the point is,

69
00:07:19,400 --> 00:07:23,400
when you focus on the breath, which really we say,

70
00:07:23,400 --> 00:07:27,400
well, that's not quite what we're doing in our practice,

71
00:07:27,400 --> 00:07:30,400
but when you focus on the breath going in and going out,

72
00:07:30,400 --> 00:07:36,400
it becomes very subtle to the point where what you're focusing on is really a concept.

73
00:07:36,400 --> 00:07:41,400
Some kind of conception of breath, and he says it's like a spider web,

74
00:07:41,400 --> 00:07:45,400
or it's like a puff bear or that kind of thing.

75
00:07:45,400 --> 00:07:50,400
It's an idea of the breath, because only that is only a concept

76
00:07:50,400 --> 00:07:57,400
allows you to fix your attention single pointedly without wavering.

77
00:07:57,400 --> 00:08:03,400
The reality doesn't admit that because it's always changing.

78
00:08:03,400 --> 00:08:08,400
And so you enter into the John as that way and then come out of it

79
00:08:08,400 --> 00:08:11,400
and practice in a different way.

80
00:08:11,400 --> 00:08:17,400
You say, okay, well, I've calmed into my mind down and I've fixed and focused my mind.

81
00:08:17,400 --> 00:08:20,400
Now what if I look at the breath and see what's really going on?

82
00:08:20,400 --> 00:08:25,400
And then you separate it out and then you do it while what we start with.

83
00:08:25,400 --> 00:08:32,400
So in our technique, we start with the repass in the first and gradually build up the summit.

84
00:08:32,400 --> 00:08:37,400
So instead of focusing on the concept of the breath,

85
00:08:37,400 --> 00:08:43,400
we're focusing on the materiality of the breath, the physical experience.

86
00:08:43,400 --> 00:08:46,400
It's not the idea of anything going in or going out.

87
00:08:46,400 --> 00:08:48,400
It's not really the breath at all.

88
00:08:48,400 --> 00:08:52,400
But if you look at it, well, clearly it's what else is it.

89
00:08:52,400 --> 00:08:55,400
It's really when you breathe in, there's the expansion.

90
00:08:55,400 --> 00:08:57,400
When you breathe out, there's the contraction.

91
00:08:57,400 --> 00:09:01,400
So those two feelings are very much mindfulness of breathing.

92
00:09:01,400 --> 00:09:05,400
It's just mindfulness of breathing from a vipass in the point of view.

93
00:09:05,400 --> 00:09:15,400
It's something that people really have a hard time getting their head around when they begin this practice.

94
00:09:15,400 --> 00:09:20,400
It's quite challenging intellectually, mentally, psychologically.

95
00:09:20,400 --> 00:09:22,400
Because it's not calming.

96
00:09:22,400 --> 00:09:25,400
It's everything it shouldn't be.

97
00:09:25,400 --> 00:09:29,400
We practice mindfulness of breathing, generally.

98
00:09:29,400 --> 00:09:37,400
People who start to practice this, to calm the mind down, to seek stability, to seek satisfaction,

99
00:09:37,400 --> 00:09:40,400
to seek control of their mind.

100
00:09:40,400 --> 00:09:42,400
These three things are what we think meditation is.

101
00:09:42,400 --> 00:09:44,400
And that is a certain type of meditation.

102
00:09:44,400 --> 00:09:47,400
We call it summit meditation.

103
00:09:47,400 --> 00:09:48,400
It's limited.

104
00:09:48,400 --> 00:09:49,400
It's great.

105
00:09:49,400 --> 00:09:50,400
It's powerful.

106
00:09:50,400 --> 00:09:54,400
But in the end, it's not going to free you from suffering.

107
00:09:54,400 --> 00:09:56,400
Because it's not real.

108
00:09:56,400 --> 00:09:58,400
The reality isn't like that.

109
00:09:58,400 --> 00:10:04,400
Some sara is impermanent, unsatisfying, and uncontrollable.

110
00:10:04,400 --> 00:10:06,400
So what you can...

111
00:10:06,400 --> 00:10:10,400
The encouragement you can get from practicing the way we practice is that,

112
00:10:10,400 --> 00:10:15,400
well, yes, I definitely am seeing impermanent suffering in non-cell.

113
00:10:15,400 --> 00:10:18,400
You're gaining what we call vipassana.

114
00:10:18,400 --> 00:10:21,400
You're coming to see the three characteristics.

115
00:10:21,400 --> 00:10:25,400
And the Buddha said, this is the path of purification.

116
00:10:25,400 --> 00:10:30,400
Eventually, after practicing to calm the mind down,

117
00:10:30,400 --> 00:10:35,400
using anapanasati, you have to see impermanence.

118
00:10:35,400 --> 00:10:38,400
Anita anupasi, we have a tea.

119
00:10:38,400 --> 00:10:45,400
Anita anupasi, a sasami, T.C. Kati, or something like that.

120
00:10:45,400 --> 00:10:48,400
As seeing impermanence, I will breathe in.

121
00:10:48,400 --> 00:10:51,400
I will breathe in, seeing impermanence.

122
00:10:51,400 --> 00:10:53,400
How does that work?

123
00:10:53,400 --> 00:10:57,400
It doesn't matter whether your focus is on the nose or on the stomach.

124
00:10:57,400 --> 00:11:00,400
We prefer the stomach because it's quite coarse,

125
00:11:00,400 --> 00:11:03,400
but the point is seeing it's...

126
00:11:03,400 --> 00:11:06,400
Like this one time, like this another time,

127
00:11:06,400 --> 00:11:08,400
different each time.

128
00:11:08,400 --> 00:11:14,400
Sometimes, digangwa, asasami, tea, pajana, tea.

129
00:11:14,400 --> 00:11:18,400
I know I am breathing in long.

130
00:11:18,400 --> 00:11:25,400
I'm breathing in short.

131
00:11:25,400 --> 00:11:31,400
Sometimes short, sometimes long, seeing impermanence.

132
00:11:31,400 --> 00:11:36,400
It's important to understand the distinction

133
00:11:36,400 --> 00:11:41,400
based on where your mind is focused.

134
00:11:41,400 --> 00:11:47,400
And to be clear that if your mind is focused on the concept of

135
00:11:47,400 --> 00:11:53,400
the breathing, to the point where you get very fixed on this idea,

136
00:11:53,400 --> 00:11:56,400
then that's some atom adaptation.

137
00:11:56,400 --> 00:12:00,400
That's great, fine and good in calming,

138
00:12:00,400 --> 00:12:05,400
but it won't lead to insight.

139
00:12:05,400 --> 00:12:09,400
So the Buddha seems clearly to have taught both of these,

140
00:12:09,400 --> 00:12:11,400
and generally both of them together.

141
00:12:11,400 --> 00:12:16,400
Generally, he taught to enter into the janas and gain all sorts of magical powers,

142
00:12:16,400 --> 00:12:23,400
so because what we have in the Dopitika is a very full teaching,

143
00:12:23,400 --> 00:12:25,400
we have a representative teaching.

144
00:12:25,400 --> 00:12:28,400
It's not the way everyone practiced,

145
00:12:28,400 --> 00:12:35,400
but it's the arch-tipple.

146
00:12:35,400 --> 00:12:42,400
It's the blueprint for a complete practice,

147
00:12:42,400 --> 00:12:45,400
or someone who practiced with samata and gains the janas,

148
00:12:45,400 --> 00:12:47,400
as well as we passed them, which is,

149
00:12:47,400 --> 00:12:50,400
I'm not certainly not one who says that that's wrong

150
00:12:50,400 --> 00:12:53,400
or that our way is better somehow.

151
00:12:53,400 --> 00:12:56,400
I might perhaps say that our way is simpler,

152
00:12:56,400 --> 00:12:59,400
and Ajahn Tong used to say it's,

153
00:12:59,400 --> 00:13:02,400
it's like if you have this medicine,

154
00:13:02,400 --> 00:13:05,400
and it's in these fancy bottles,

155
00:13:05,400 --> 00:13:11,400
but then it's got a trademark and it's got all,

156
00:13:11,400 --> 00:13:15,400
it's got a label,

157
00:13:15,400 --> 00:13:17,400
I think is what he was trying to say.

158
00:13:17,400 --> 00:13:19,400
It's a brand name medicine.

159
00:13:19,400 --> 00:13:21,400
It's very expensive,

160
00:13:21,400 --> 00:13:26,400
but for poor people, people who can't afford this great medicine,

161
00:13:26,400 --> 00:13:29,400
they get the same medicine, but they get a knock-off brand.

162
00:13:29,400 --> 00:13:31,400
So it's got the same stuff inside.

163
00:13:31,400 --> 00:13:33,400
It just doesn't have the brand name,

164
00:13:33,400 --> 00:13:35,400
it doesn't have the fancy bottle,

165
00:13:35,400 --> 00:13:38,400
it's not maybe the instructions are printed in poor English,

166
00:13:38,400 --> 00:13:40,400
that kind of thing.

167
00:13:40,400 --> 00:13:44,400
But the medicine is the same.

168
00:13:44,400 --> 00:13:46,400
The idea is that the essence,

169
00:13:46,400 --> 00:13:48,400
what really cures is fine,

170
00:13:48,400 --> 00:13:52,400
but it's not as wonderful.

171
00:13:52,400 --> 00:13:54,400
It's not, for the meditation,

172
00:13:54,400 --> 00:13:56,400
it's not as pleasant.

173
00:13:56,400 --> 00:14:02,400
Obviously the practice we do is a lot less fancy

174
00:14:02,400 --> 00:14:04,400
than someone practicing

175
00:14:04,400 --> 00:14:07,400
some at an even gaining all sorts of magical powers,

176
00:14:07,400 --> 00:14:10,400
and a lot of people are attracted to that,

177
00:14:10,400 --> 00:14:13,400
understandably.

178
00:14:13,400 --> 00:14:16,400
If you have the time, if you have the inclination,

179
00:14:16,400 --> 00:14:20,400
and a lot of people prefer to do all of that as well,

180
00:14:20,400 --> 00:14:23,400
which is of course fine.

181
00:14:23,400 --> 00:14:24,400
Point being in the end,

182
00:14:24,400 --> 00:14:27,400
they have to come back to this sort of practice anyway.

183
00:14:27,400 --> 00:14:30,400
You'll have to come back to focus on reality

184
00:14:30,400 --> 00:14:39,400
in order to see the truth of suffering.

185
00:14:39,400 --> 00:14:43,400
But Anapana Satinas is our first section,

186
00:14:43,400 --> 00:14:46,400
as I said that we're actually thought learning about practice.

187
00:14:46,400 --> 00:14:51,400
The Buddha was especially praised worthy of Anapana Satinas.

188
00:14:51,400 --> 00:14:53,400
He said,

189
00:14:53,400 --> 00:15:19,220
What does you think of Beethoven's

190
00:15:19,220 --> 00:15:30,540
is peaceful, santa, peaceful. Even the practice that we do is quite peaceful. You can throw

191
00:15:30,540 --> 00:15:35,860
you for a loop in the beginning, but there's a great amount of peace that comes from focusing

192
00:15:35,860 --> 00:15:41,540
on the breath and not having to seek out a meditation object. The breath is always here.

193
00:15:41,540 --> 00:15:47,220
It's reassuring. Sometimes when things are, when your life is overwhelming and you just

194
00:15:47,220 --> 00:15:51,620
come back to the breath, of course, with some at the practice, it's quite easy, but

195
00:15:51,620 --> 00:15:57,620
giving with we pass into practice. With some at a, it's quite pleasant. And it calms you down

196
00:15:57,620 --> 00:16:04,780
with we pass into practice. It clears your mind. It helps you see clearly.

197
00:16:04,780 --> 00:16:13,340
Panitoja, it's subtle. Again, this more relates to some at the meditation. Being something

198
00:16:13,340 --> 00:16:19,380
sublime, something that gets more and more subtle. But even with we pass into perhaps

199
00:16:19,380 --> 00:16:28,820
even more when you focus on the stomach rising and falling, for example. You start to see

200
00:16:28,820 --> 00:16:36,020
more subtly and have a deeper understanding and appreciation of reality, of impermanence.

201
00:16:36,020 --> 00:16:42,820
You really learn to let go by focusing on the stomach because it's uncontrollable. It's

202
00:16:42,820 --> 00:16:48,100
impermanent. It's unsatisfying.

203
00:16:48,100 --> 00:16:56,780
A sage and a koja. A sage and a kai is an interesting word. Sage and a kai means mixed,

204
00:16:56,780 --> 00:17:12,780
caught up, or really sort of like a mixed up with other things. A sage and a kō means

205
00:17:12,780 --> 00:17:20,700
unadulterated or unmixed. It means it's pure. What it means is the breath is something

206
00:17:20,700 --> 00:17:26,900
very specific. There's no, it's not confusing or mixed up. There's no uncertainty about

207
00:17:26,900 --> 00:17:35,780
what to focus on. The breath is an object that you can always rely upon to be just the breath.

208
00:17:35,780 --> 00:17:42,780
It's not something hard to find.

209
00:18:05,780 --> 00:18:24,180
It's so cold, Javi Haru. It is a peaceful dwelling. There's a dwelling in happiness.

210
00:18:24,180 --> 00:18:32,060
There's a happy dwelling. Again, with some of the meditation, it's because it leads

211
00:18:32,060 --> 00:18:40,460
very much to tranquility. With Vipasana, it's because it again clears the mind. It's something

212
00:18:40,460 --> 00:18:46,340
that you can always come back to. When you reach Tancadrupek and Yana, when you fully

213
00:18:46,340 --> 00:18:57,740
cultivated Vipasana inside, your breath becomes a sort of a refuge in a sense of something

214
00:18:57,740 --> 00:19:05,540
that can keep you going and keep you very much present. It's an anchor to keep you here

215
00:19:05,540 --> 00:19:17,340
and now and keep you from getting caught up in extrapolation and judgment, reaction.

216
00:19:17,340 --> 00:19:27,140
And then Upa Nupaneh Papakayukusa Laidami, whatever a risen evil unwholesome states.

217
00:19:27,140 --> 00:19:38,420
Whenever evil unwholesome states might arise, as they arise, Tana, so right then and there,

218
00:19:38,420 --> 00:19:51,740
Andrada-pati-vupasamati destroys them. Mindfulness of breathing destroys the development.

219
00:19:51,740 --> 00:20:04,580
So with some of the meditation, it destroys them temporarily by entering into the Jana's

220
00:20:04,580 --> 00:20:09,300
with Vipasana meditation, it destroys them because it's such again, again, such a good

221
00:20:09,300 --> 00:20:16,060
object for seeing clearly. But this is a quote from the Buddha, where he praised the point

222
00:20:16,060 --> 00:20:22,860
as the point of relating this to is because he praised specifically mindfulness of breathing

223
00:20:22,860 --> 00:20:31,260
for having these qualities, for being particularly peaceful and particularly beneficial

224
00:20:31,260 --> 00:20:37,780
as far as rooting out the defilement. Many other types of meditation, even other types of

225
00:20:37,780 --> 00:20:44,380
meditation that the Buddha recommended that aren't mindfulness of breathing. But he recommended

226
00:20:44,380 --> 00:20:50,020
this one specifically, and he always came back to it. There was a time where he taught

227
00:20:50,020 --> 00:20:55,940
mindfulness of death, I think it was a mindfulness of lotesomeness of the body or something,

228
00:20:55,940 --> 00:21:01,140
and it led some people to kill themselves. And on the island, we're reported to the Buddha

229
00:21:01,140 --> 00:21:09,300
and the Buddha said, well, then teach, and teach on Upa Nasati because it's more peaceful.

230
00:21:09,300 --> 00:21:19,340
Sometimes people just have bad, their minds are too mixed up, and meditation is, they react

231
00:21:19,340 --> 00:21:46,900
too strongly. So, Upa Nasati is considered to be the most reliable practice.

232
00:21:46,900 --> 00:22:05,580
So, as to what the text itself says, there is some things that I shouldn't skip over

233
00:22:05,580 --> 00:22:12,380
to. We're talking about mindfulness of the body, but this is the one where the Buddha,

234
00:22:12,380 --> 00:22:17,900
because in places where he explains mindfulness of breathing elsewhere, he gives some

235
00:22:17,900 --> 00:22:23,980
details about the practice, about some practical details about how to cultivate mindfulness

236
00:22:23,980 --> 00:22:33,380
of breathing. He says, Aranyagatova, Rukamulagatova, Sunyagagatova, having gone to the forest,

237
00:22:33,380 --> 00:22:41,540
having gone to the foot of a tree, or having gone to an empty dwelling. This is a standard

238
00:22:41,540 --> 00:22:49,580
enumeration of the good places to practice. It's not to say you can't practice anywhere.

239
00:22:49,580 --> 00:23:00,900
This is a standard advice given by the Buddha. If you can, head out to the forest, or just

240
00:23:00,900 --> 00:23:09,300
find a tree somewhere, or really the best way is just find an empty place. It doesn't

241
00:23:09,300 --> 00:23:18,740
matter whether it's in the forest or in the city. I didn't find a spot where you can

242
00:23:18,740 --> 00:23:26,700
be alone, is basically what he's saying. Nisi datipa lang kang abu jitua. So, this is having

243
00:23:26,700 --> 00:23:31,700
sitting cross-legged and you recommend sitting in the lotus position. I mean, in India,

244
00:23:31,700 --> 00:23:37,580
of course, this would have been a dealing with people who can easily sit on the floor

245
00:23:37,580 --> 00:23:50,580
and can sit cross-legged. They've been able to sit full lotus, ujung kaiyang panidhaia,

246
00:23:50,580 --> 00:23:58,660
make your body straight, sit up straight. A lot of this instruction is sort of instruction,

247
00:23:58,660 --> 00:24:03,540
remember, has a lot more to do with some of the practice. We're going to see that as we

248
00:24:03,540 --> 00:24:07,700
go through the Satipa tana, it's pretty clear that it really doesn't matter what position

249
00:24:07,700 --> 00:24:12,620
your body is in. The Buddha even says, whatever position your body is in, be mindful

250
00:24:12,620 --> 00:24:18,580
of that. So, this specific one with anabana sati, as much more to do with the arch-type

251
00:24:18,580 --> 00:24:25,900
of, I don't know if that's the right word, but this template of a course of practice

252
00:24:25,900 --> 00:24:31,780
whereby you enter into the Johnus first, and he's just following that. I mean, this

253
00:24:31,780 --> 00:24:40,060
section is elsewhere in many different places in the Anapana Satisutta, for example. There's

254
00:24:40,060 --> 00:24:45,700
much more detailed explanation of this sort of practice that goes through the Johnus.

255
00:24:45,700 --> 00:24:59,460
Anyhow, for some similes about it that I won't go into, but in each of the sections, he's

256
00:24:59,460 --> 00:25:07,580
going to repeat himself at the end and say, in this way, one is mindful of the body internally

257
00:25:07,580 --> 00:25:18,220
or externally, or internally and externally. It's mindful of the beginning, the arising

258
00:25:18,220 --> 00:25:28,020
of experiences in regards to the body, or the arising of the body, or the cessation

259
00:25:28,020 --> 00:25:38,820
of the body, or the arising in the cessation of the body. So, what does all that mean?

260
00:25:38,820 --> 00:25:45,300
In terms of Yipasana practice, I think it depends how you interpret this, but it's mostly

261
00:25:45,300 --> 00:25:51,500
an internal, I think strictly speaking, it's an internal practice, meaning you're only

262
00:25:51,500 --> 00:25:56,460
aware of your own body. Yipasana inside can't, of course, arise when you're focused on

263
00:25:56,460 --> 00:26:03,900
someone else's body. There are certain types of mindfulness of the body, your mindfulness,

264
00:26:03,900 --> 00:26:09,620
that have to do with someone else's body, mindfulness of death, for example. Mindfulness

265
00:26:09,620 --> 00:26:15,900
of the load, the sameness of the body, you can focus on a dead body or a decomposing

266
00:26:15,900 --> 00:26:22,900
body, or you can focus on a body that's cut up and focused on the various pieces of the

267
00:26:22,900 --> 00:26:29,420
body, and we're going to go through, we'll get to that sort of practice. But that's not

268
00:26:29,420 --> 00:26:38,860
Yipasana. He's saying it here because internally or externally, there are different ways

269
00:26:38,860 --> 00:26:48,420
of meditating. Some are samata, some are Yipasana. But the part about the beginning and

270
00:26:48,420 --> 00:26:54,060
the arising or the ceasing or the arising and the ceasing, well this is much more to

271
00:26:54,060 --> 00:26:59,660
do with the actual core of this suit as a bell, which is insight practice, coming to see

272
00:26:59,660 --> 00:27:06,340
things as they are. So when we focus on the arising of an experience, we try and see it

273
00:27:06,340 --> 00:27:12,220
as it arises, see it as it ceases. The point being, trying to eventually go through the

274
00:27:12,220 --> 00:27:18,740
stages of knowledge where we come to see impermanence. We see that there's not a lasting

275
00:27:18,740 --> 00:27:25,980
entity involved in this experience. Experience is a rise and sees moment after moment after

276
00:27:25,980 --> 00:27:38,740
moment. There is no mean or mind, no self involved. So a big part of our practice is

277
00:27:38,740 --> 00:27:46,820
being able to see experiences as atomic, as something that arises and ceases and that

278
00:27:46,820 --> 00:27:54,420
is in and of itself. And it's not the same thing that was there a moment ago. It is something

279
00:27:54,420 --> 00:28:01,460
that arises and ceases. The arising and ceasing of phenomena plays a big part in insight

280
00:28:01,460 --> 00:28:07,380
practice, in the understanding of impermanence, suffering in non-self. It's a big part

281
00:28:07,380 --> 00:28:12,820
of our observation as we practice, seeing things come and go and come and go. And it's

282
00:28:12,820 --> 00:28:24,500
eventually the only salient observation that the meditator engages in. When they reach

283
00:28:24,500 --> 00:28:30,580
San Garupekanyana, they're merely seeing experiences arise and cease without any kind of

284
00:28:30,580 --> 00:28:39,300
judgment or attachment. It's merely a sequence of arising and ceasing arise. And it's

285
00:28:39,300 --> 00:28:49,620
completely peaceful. So watching the stomach begin and begin and watching the thoughts arise

286
00:28:49,620 --> 00:28:58,620
and cease. Rising and ceasing, this is the salient characteristic. It's really the only

287
00:28:58,620 --> 00:29:04,100
characteristic in the end. Everything else is just a judgment and a reaction or an extrapolation

288
00:29:04,100 --> 00:29:10,020
or a conception. The only thing that's important is the arising or the only thing that's

289
00:29:10,020 --> 00:29:18,020
real. And the only thing you should or can say when you get to the pinnacle of insight

290
00:29:18,020 --> 00:29:34,060
practices, it arises and it ceases. And then he says, a tikayo tiwapanasasatipatupatitahoti,

291
00:29:34,060 --> 00:29:46,180
or one establishes mindfulness just to the extent this is body. And this is body is really

292
00:29:46,180 --> 00:29:53,180
just an example. What he's saying is, this is this. So whenever you do with the body and

293
00:29:53,180 --> 00:29:57,740
he's going to repeat this, this is not just with anabanasatipat. With each part of the body,

294
00:29:57,740 --> 00:30:07,860
with each part of the sutra in it, seeing this is this, it is what it is. Body is body.

295
00:30:07,860 --> 00:30:14,340
So there's practice of using the mantra to say, rise in falling or stepping right, stepping

296
00:30:14,340 --> 00:30:22,740
left or with the other satipatanas as well. It is that. That's all it is. It is what

297
00:30:22,740 --> 00:30:37,300
it is. Yavand even yanamataya, patisati mantaya, with just knowledge, for only for the

298
00:30:37,300 --> 00:30:49,340
purpose of knowledge, patisati mantaya. But it's only for being mindful of it. Meaning

299
00:30:49,340 --> 00:30:55,060
our perception has none of the baggage. When we're talking about being mindful, what we

300
00:30:55,060 --> 00:31:02,580
mean is just knowing it, just remembering it, recognizing it. This is seeing, this is hearing,

301
00:31:02,580 --> 00:31:09,500
this is rising, this is falling. This is the in breath. This is the out breath. Mindfulness

302
00:31:09,500 --> 00:31:20,820
is often called bear attention for this reason because it's a one to one experience. Whatever

303
00:31:20,820 --> 00:31:31,220
you experience, that's how you perceive it. That's the state of being mindful. Anisito

304
00:31:31,220 --> 00:31:40,100
joviyarati, one dwells, unclean, or independent. Anisita means independent. Yes, mindfulness

305
00:31:40,100 --> 00:31:46,100
is what makes you independent. You are not dependent. Oh, I hope it's like this. Or if it's

306
00:31:46,100 --> 00:31:56,180
like this, and it's like that, I will be lost. When it's like this, I can't meditate. When

307
00:31:56,180 --> 00:32:03,500
it's like this, my meditation is his best. Now, my meditation is good. Now, my meditation

308
00:32:03,500 --> 00:32:11,260
is bad. Judgments like this, this makes you dependent. One who is mindful is independent.

309
00:32:11,260 --> 00:32:18,220
Doesn't matter what comes, doesn't matter what the experiences are. It doesn't affect

310
00:32:18,220 --> 00:32:25,420
the meditator. Not chicken, chiloke, body, a tain. It doesn't cling to anything in the

311
00:32:25,420 --> 00:32:32,180
world. It's a very good description of what it means to be mindful. You had any doubts

312
00:32:32,180 --> 00:32:38,060
about what the Buddha was talking about, really reading the Satipatana Sutta and focusing

313
00:32:38,060 --> 00:32:45,860
on these words. It makes it clear. It says what it is, not clinging to anything. There's

314
00:32:45,860 --> 00:32:50,740
no doubt, and while it's quite clear as we practice, that, okay, I'm clinging to quite

315
00:32:50,740 --> 00:32:58,780
a few things. So, this is where our practice takes place, overcoming these clings, being

316
00:32:58,780 --> 00:33:04,860
aware of them, watching them, rising above them, cleaning them out to the point where you

317
00:33:04,860 --> 00:33:15,540
just experience. A 1 p. co. bikho, bikho kai, a kai, a noplasi, we had it. Thus one dwells mindful

318
00:33:15,540 --> 00:33:22,660
of the body and body. This paragraph is going to repeat for each section. It's the most

319
00:33:22,660 --> 00:33:28,340
salient aspect of, for it's the summary of what it means to be mindful of the body. Each

320
00:33:28,340 --> 00:33:35,900
section is just going to go through various ways of describing mindfulness of the body and

321
00:33:35,900 --> 00:33:57,700
then feelings and then mind and then dumbness. So, that's Anapana Sutta for tonight. Looks

322
00:33:57,700 --> 00:34:13,020
like the website is down. Spinning, spinning. I think no questions tonight. Thank you all

323
00:34:13,020 --> 00:34:40,740
for tuning in. Have a good night.

