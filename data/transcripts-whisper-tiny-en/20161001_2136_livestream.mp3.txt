Can we do walking and then meditation on death during sitting?
You can do whatever you want, it's a free world.
I'm sorry, it's your oyster, it's not one I teach, but unless you're referring to momentary death, in which case that's what we do, we focus on the momentary born and birth and death, birth and death, every moment.
When a monk becomes ill and decides to accept their possible death instead of seeking treatment, would they be making bad karma?
Not necessarily, karma is momentary, so if they have moments of greed, anger or delusion, that would be bad karma.
There's nothing inherently, there's no inherent necessity, it doesn't necessitate those mind states to refuse treatment necessarily.
There are probably situations where it would, where you'd just be being stubborn, because people were trying to treat you, or there's simple treatment, and you refused it.
I think if there are cases where that would be stubbornness and ego and delusion, attachment, maybe aversion to life, that could be a shame.
You're all caught up on questions on the website, Dante, I just have a simple one, are you still going to New York City next weekend?
Yes.
Let's see, you have two events that are open to people to attend?
Yes, on the 8th and the 9th, it's kind of one event I think, but two days, I'm not sure actually.
There'll be only different people, different days, but yeah, two day, day meditation courses.
On the 8th and the 9th, I walked over New York City, and then I'll be at my father's, so I might just take a week off broadcasting.
But I have to be back Thursday, because this is a special six day.
It's interesting.
I may have to return to the monastery on the 10th, because it's during the rains, right?
I can't just leave.
I may have to come back on the 10th, so then I can go for another seven day.
I only have seven days periods that we can leave.
I'll go another seven day period to see my father, which is also an exception to that.
Where can we get more information about your New York City that's it?
I go on Facebook, they have something called Buddhist Insights.
I think it's a much more...
Look at Buddhist Insights in New York.
Yeah, I think you put it on your page as well.
I think I did.
I'm pretty sure.
Yeah, it's called Buddhist Insights in New York, New York.
Look them up, and you can see what they're doing.
They're trying to start a meditation center, trying to find a home.
And they'll be advertising it in the coming days.
