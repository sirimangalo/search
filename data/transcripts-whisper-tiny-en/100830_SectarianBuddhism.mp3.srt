1
00:00:00,000 --> 00:00:14,240
Okay, welcome everyone, today I thought I'd start with a quote and this is a quote from

2
00:00:14,240 --> 00:00:25,680
a English poet I believe.

3
00:00:25,680 --> 00:00:32,480
So often theologic war is the Disputant Cyrene, rail on and other incurrence of what each

4
00:00:32,480 --> 00:00:43,720
other means and pray about an elephant, not one of them has seen.

5
00:00:43,720 --> 00:01:00,480
This quote is taken from some poem in medieval England I believe but it's original sources

6
00:01:00,480 --> 00:01:07,680
from India, this is an old Indian legend and if you want to read about the Buddhist version

7
00:01:07,680 --> 00:01:15,920
of it, it's on the internet as well.

8
00:01:15,920 --> 00:01:22,960
And the story goes that there was once a king who wanted to have some fun and so he called

9
00:01:22,960 --> 00:01:38,720
together a bunch of blind men and he had them examine an elephant and they each looked

10
00:01:38,720 --> 00:01:45,720
at, each came up to the elephant and being blind of course they had to grab a piece of

11
00:01:45,720 --> 00:01:56,040
the elephant with their hands and so one of the blind men looked at, grabbed the head

12
00:01:56,040 --> 00:02:03,000
of the elephant and they said to him, oh this is an elephant, some got the ear and they

13
00:02:03,000 --> 00:02:08,120
said this is the elephant, some got a task, the body, the foot got different pieces of

14
00:02:08,120 --> 00:02:17,240
the elephant and then they came back to the king and the king said well have you seen

15
00:02:17,240 --> 00:02:21,280
the elephant and they said yes we've seen the elephant and he said well then tell me

16
00:02:21,280 --> 00:02:27,400
would so what's an elephant like and the ones who had touched the head of the elephant

17
00:02:27,400 --> 00:02:34,560
said oh an elephant is just like a water jar and the ones who had touched the ear they

18
00:02:34,560 --> 00:02:41,200
said oh no no no, an elephant is like a winnowing basket, big basket that they win a

19
00:02:41,200 --> 00:02:47,040
rice in and another the other group that had been shown that had been touched the

20
00:02:47,040 --> 00:02:54,840
tasks said no no no, a task is like a plow share, an elephant is like a plow share and

21
00:02:54,840 --> 00:02:59,840
the one that touched the trunk said no no no it's like a plow pool and the other said

22
00:02:59,840 --> 00:03:10,600
it's like a store room, it's like a post, it's like a mortar, it's like a room and so on

23
00:03:10,600 --> 00:03:16,160
based on the pieces of the elephant that they had touched because none of them their hands

24
00:03:16,160 --> 00:03:26,480
are only so big and can only touch a piece of the elephant and so as a result they came

25
00:03:26,480 --> 00:03:36,120
to understand the elephant to be just a piece of the elephant and this teaching is so

26
00:03:36,120 --> 00:03:45,960
popular because this is exactly how humans behave in what is most important in life, what

27
00:03:45,960 --> 00:03:53,360
is most important to people one of the things that's most important being religion and

28
00:03:53,360 --> 00:03:59,360
this is especially true in religion and so it's passed along with with many of different

29
00:03:59,360 --> 00:04:18,520
religious traditions as as a teaching for people who would spend all of their time denouncing

30
00:04:18,520 --> 00:04:30,760
other sects and other religions and promoting their own and I think this is incredibly

31
00:04:30,760 --> 00:04:39,840
apt for Buddhism and it shows some foresight of the Buddha in relating this story even

32
00:04:39,840 --> 00:04:44,840
in the time of the Buddha there were many different religious groups who were fighting

33
00:04:44,840 --> 00:04:54,760
and wrangling and unable to agree on what was the truth but even nowadays even in Buddhism

34
00:04:54,760 --> 00:05:03,080
we find sometimes that we're unable to agree on even unable to agree on what Buddhism

35
00:05:03,080 --> 00:05:16,200
is or find ourselves separating the various practitioners of Buddhism and separating

36
00:05:16,200 --> 00:05:24,240
into us and them and trying to distinguish ourselves from the other groups of Buddhism so

37
00:05:24,240 --> 00:05:34,720
we have the teravada, we have the Mahayana, we have Zen Buddhism, we have Vajrayana Buddhism

38
00:05:34,720 --> 00:05:46,120
we have many different names for Buddhism in many different places and one of the things

39
00:05:46,120 --> 00:05:52,360
that really strikes me when you look at all of these traditions is that really they're

40
00:05:52,360 --> 00:06:02,920
just like the elephant it's as though each of these schools has picked a piece of the elephant

41
00:06:02,920 --> 00:06:07,800
in this case a piece of Buddhism and has said this is Buddhism and then they develop

42
00:06:07,800 --> 00:06:16,600
that piece to the extent that they blot out or forget about or deny the importance of

43
00:06:16,600 --> 00:06:27,600
the other pieces as though an elephant were only its trunk or only its tail I think this

44
00:06:27,600 --> 00:06:32,920
is one mistake that Buddhists make in this regard the other mistake that we often make

45
00:06:32,920 --> 00:06:41,640
is one of I guess the word you could use is bigotry where we say only my school does

46
00:06:41,640 --> 00:06:55,240
x or my school focuses on why or this or that and we make the claim that other schools

47
00:06:55,240 --> 00:07:04,240
don't concern themselves with this and the funny thing is you can find just about every

48
00:07:04,240 --> 00:07:14,360
school of Buddhism saying the same thing about the same subject you can find various groups

49
00:07:14,360 --> 00:07:19,280
and if you go around and talk especially coming from one group and talking to people of

50
00:07:19,280 --> 00:07:38,160
another group they will say yeah you can turn that off they will say oh yes I know

51
00:07:38,160 --> 00:07:49,240
your school but in my school we focus on this I know your school focuses on that

52
00:07:49,240 --> 00:07:55,680
and it's quite interesting to hear these sorts of things because they're rarely if

53
00:07:55,680 --> 00:08:24,600
ever true and they most often have to do with things like

