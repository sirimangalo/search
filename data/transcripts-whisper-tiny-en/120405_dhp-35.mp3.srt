1
00:00:00,000 --> 00:00:03,600
Hello and welcome back to our study of the Dhamapanda.

2
00:00:03,600 --> 00:00:10,000
Today, we continue on with verse number 35, which goes as follows.

3
00:00:10,000 --> 00:00:38,400
This is the Dhamapanda, which means difficult to restrain flighty, landing upon or

4
00:00:38,400 --> 00:00:43,800
lighting upon whatever it desires.

5
00:00:43,800 --> 00:00:54,200
The taming of the mind is good, the tamed mind brings happiness.

6
00:00:54,200 --> 00:01:05,440
So another really pointed teaching from a meditative point of view.

7
00:01:05,440 --> 00:01:07,840
And this one has very much to do with meditation.

8
00:01:07,840 --> 00:01:13,720
It's actually quite a story, well known among Buddhist circles.

9
00:01:13,720 --> 00:01:22,720
The story goes that there was a village called Matika and there was a, there was a group

10
00:01:22,720 --> 00:01:29,000
of thirteen bikhus who went to practice meditation there.

11
00:01:29,000 --> 00:01:34,000
And the headmen of the village, his mother had great faith.

12
00:01:34,000 --> 00:01:41,280
And when she saw these Buddhist monks coming, she thought she was quite excited to see them.

13
00:01:41,280 --> 00:01:42,280
And that's the way they were going.

14
00:01:42,280 --> 00:01:47,440
They said, well, they're just looking for a place to spend the rains.

15
00:01:47,440 --> 00:01:52,000
And so she immediately jumped upon it and said, look, if you stay here, then I'll be able

16
00:01:52,000 --> 00:01:59,240
to take the precepts and follow your teeth, listen to your teachings and learn more about

17
00:01:59,240 --> 00:02:05,200
the Buddhist teaching and have a chance, a very good chance to study and practice the

18
00:02:05,200 --> 00:02:06,200
Buddhist teaching.

19
00:02:06,200 --> 00:02:08,480
So she said, please stay here and we'll build you a monastery.

20
00:02:08,480 --> 00:02:14,920
And so a proof just like that, they built off in the forest, a monastery for these monks

21
00:02:14,920 --> 00:02:21,000
with the hopes or under the promise that they would stay there for the rains.

22
00:02:21,000 --> 00:02:26,800
And they promised that they would bring them food every day and whatever they wanted them,

23
00:02:26,800 --> 00:02:29,000
they would, they would bring for these monks.

24
00:02:29,000 --> 00:02:33,480
And so the monks were quite happy about this because it would mean that for the rains,

25
00:02:33,480 --> 00:02:38,920
they would have a chance to practice meditation strenuously and put all their effort into

26
00:02:38,920 --> 00:02:41,160
the practice.

27
00:02:41,160 --> 00:02:45,360
And so once the monastery was finished, they all went in and they had a meeting amongst

28
00:02:45,360 --> 00:02:50,280
themselves as to how they were going to conduct themselves for the rains.

29
00:02:50,280 --> 00:02:54,640
And what they said just incidentally, the things that they said were kind of interesting.

30
00:02:54,640 --> 00:03:00,520
One of them is quite pertinent and it sort of gives you an idea of the intentions that

31
00:03:00,520 --> 00:03:03,200
they had going into this.

32
00:03:03,200 --> 00:03:32,120
They say, the Pali is, just like our own house, the eight hells, we wah, wah, wah, wah,

33
00:03:32,120 --> 00:03:42,520
but the eight great hells are wide open for us, have their gates wide open for us.

34
00:03:42,520 --> 00:03:51,360
Just as if they were our own home, they say, it would not be good for us to be pamanda,

35
00:03:51,360 --> 00:04:02,400
pamanda, tarang, tari, tomb, nah, what the deep, it is not proper, it will not be, it is

36
00:04:02,400 --> 00:04:08,160
not the case that we should be negligent because the eight hells are wide open, meaning

37
00:04:08,160 --> 00:04:10,240
they still had greed, anger and delusion.

38
00:04:10,240 --> 00:04:15,240
And so if they were negligent, then these things would lead them into one of the eight great

39
00:04:15,240 --> 00:04:19,560
hells or couldn't possibly, at the very least it would lead them to suffering.

40
00:04:19,560 --> 00:04:33,840
So for that reason they made a vow amongst themselves out of this great anxiety or fear

41
00:04:33,840 --> 00:04:40,480
that they all had of the possibility of going to a bad place and out of a desire to become

42
00:04:40,480 --> 00:04:50,840
free from it, they made a very extreme sort of agreement, a very hard agreement with

43
00:04:50,840 --> 00:04:56,320
them, some strict agreement with themselves, that during the reigns retreat they would,

44
00:04:56,320 --> 00:04:58,680
as best as possible, not talk to each other.

45
00:04:58,680 --> 00:05:03,360
Now what it says that they did is in the morning they would meet for alms, so then they

46
00:05:03,360 --> 00:05:06,520
would maybe even discuss where they're going to go on alms, who's going to go on which

47
00:05:06,520 --> 00:05:10,880
route and who's going to go in which direction.

48
00:05:10,880 --> 00:05:18,640
And in the evening they would meet together to pay respect to the elder monks and to do

49
00:05:18,640 --> 00:05:22,600
the duties towards whatever monk is elder and so on.

50
00:05:22,600 --> 00:05:27,880
But apart from that they would not, they would say we will not meditate two monks in

51
00:05:27,880 --> 00:05:33,280
the same place, we will all, we will not go on alms, round two monks in the same road

52
00:05:33,280 --> 00:05:40,680
we will not meet eye to eye and out of fear that we might begin to converse and therefore

53
00:05:40,680 --> 00:05:45,040
give rise to the family.

54
00:05:45,040 --> 00:05:51,320
And so with this promise they undertook to themselves to spend all their time in their

55
00:05:51,320 --> 00:05:52,320
kutti.

56
00:05:52,320 --> 00:05:56,880
Now the one thing they did say, and if a monk is sick, the only exception is if a monk

57
00:05:56,880 --> 00:06:02,360
gets sick, then that monk should go out and ring the bell in the middle of the monastery

58
00:06:02,360 --> 00:06:07,680
and then all of us must come to attend on the sick monk, so that was the only one exception.

59
00:06:07,680 --> 00:06:10,440
Apart from that they spend all their time, they decided they would spend all their time

60
00:06:10,440 --> 00:06:16,480
in their kutti, so immediately they all went to their own huts and undertook the practice

61
00:06:16,480 --> 00:06:18,400
of meditation.

62
00:06:18,400 --> 00:06:23,760
Now the woman who was the mother of Mahthika, the mother of the village headmen who was

63
00:06:23,760 --> 00:06:27,280
looking after them was quite excited and wanted to come and make sure that everything was

64
00:06:27,280 --> 00:06:28,280
all right for the monks.

65
00:06:28,280 --> 00:06:32,880
So she decided that she would come to the monastery and see how her sons were doing as

66
00:06:32,880 --> 00:06:34,800
she put it.

67
00:06:34,800 --> 00:06:40,000
But when she got there, it wasn't as she expected that her sons would be happily enjoying

68
00:06:40,000 --> 00:06:45,960
their monastery and discussing the Buddha's teaching and so on and so on.

69
00:06:45,960 --> 00:06:50,920
And so when she saw these monks, she went to the monastery and said, well where are

70
00:06:50,920 --> 00:06:52,600
all the monks?

71
00:06:52,600 --> 00:06:56,640
And the man who was looking after the monasteries and while they're off in their kutti

72
00:06:56,640 --> 00:07:01,680
is one by one, they're all in different places and she saw that this was the case that

73
00:07:01,680 --> 00:07:05,680
they were all avoiding each other.

74
00:07:05,680 --> 00:07:09,120
So she said, what can I do if I want to talk to them?

75
00:07:09,120 --> 00:07:12,760
And he said, well ring the bell and so she went up to the bell and she rang it.

76
00:07:12,760 --> 00:07:15,960
And she watched and they all came from different places and she said, sure enough, they're

77
00:07:15,960 --> 00:07:20,160
all avoiding each other and she thought this was the most peculiar behavior.

78
00:07:20,160 --> 00:07:25,160
So they said, they said, what's wrong with that someone was maybe sick and she said,

79
00:07:25,160 --> 00:07:29,920
well I'm wondering why it is that you're not talking to each other, why are you avoiding

80
00:07:29,920 --> 00:07:31,640
each other?

81
00:07:31,640 --> 00:07:36,280
And she said, you must be angry at each other, there must be some problem with your harmony

82
00:07:36,280 --> 00:07:40,560
and here I was hoping to gain lots of merit having you here but now I'm afraid that there

83
00:07:40,560 --> 00:07:44,640
may be some problem and they said, no no, it's not the case for practicing meditation

84
00:07:44,640 --> 00:07:49,840
and when you practice meditation you have to be in solitude.

85
00:07:49,840 --> 00:07:53,200
And she said, what does this mean, practice meditation?

86
00:07:53,200 --> 00:07:58,080
We had no idea what this was and we'll practice meditation in our case where we're thinking

87
00:07:58,080 --> 00:08:04,880
of the parts of the body, we say to ourselves here, hair and skin, skin and so on, focusing

88
00:08:04,880 --> 00:08:10,440
on the 32 parts of the body and as a result developing common tranquility and then developing

89
00:08:10,440 --> 00:08:14,800
insight based on that, this is what we've been taught to do.

90
00:08:14,800 --> 00:08:19,800
And she said, oh, that sounds, she said, oh that's quite interesting actually and she said,

91
00:08:19,800 --> 00:08:23,000
is it only monks who can practice this teaching and he said, no no.

92
00:08:23,000 --> 00:08:27,000
And they said, no no, anyone can practice this teaching and she said, well then can you

93
00:08:27,000 --> 00:08:28,000
teach it to me?

94
00:08:28,000 --> 00:08:33,560
And so they taught her these 32 parts of the body and to just get to the end of the story,

95
00:08:33,560 --> 00:08:40,040
she practiced diligently and actually gained the third stage of enlightenment on the

96
00:08:40,040 --> 00:08:41,040
ground.

97
00:08:41,040 --> 00:08:47,280
She had no more sensual desire, no more anger but she would still have these subtle desires

98
00:08:47,280 --> 00:08:52,960
to become or to not become, desire to get rid of things and then desire to achieve things,

99
00:08:52,960 --> 00:08:56,080
to be this and to not be that and she still would have conceived and so on, some of the

100
00:08:56,080 --> 00:09:03,760
subtle defines but she was totally freed from the lower defilements of lust and craving

101
00:09:03,760 --> 00:09:11,480
and so on and anger and aversion and she said, wow, this is wonderful, she said, I wonder

102
00:09:11,480 --> 00:09:17,480
when did these monks attain this state and she used these powers that she in as a result

103
00:09:17,480 --> 00:09:22,880
of her enlightenment, she to send her mind out and to examine these monks and she said

104
00:09:22,880 --> 00:09:27,000
these monks are still full of greed, anger and delusion, they haven't gained anything.

105
00:09:27,000 --> 00:09:28,000
She said, what's wrong?

106
00:09:28,000 --> 00:09:32,480
They've been here for some time practicing meditation, I wonder what can be wrong and

107
00:09:32,480 --> 00:09:36,480
she looked and she said, do they have suitable lodging?

108
00:09:36,480 --> 00:09:41,520
Yes, they have suitable lodging, do they have suitable company or are there any problems

109
00:09:41,520 --> 00:09:42,520
in between them?

110
00:09:42,520 --> 00:09:46,400
No, there's no problems between them, do they have suitable food and she said, aha,

111
00:09:46,400 --> 00:09:51,840
they don't have suitable food and so she thought to herself, you know, she sent her mind

112
00:09:51,840 --> 00:09:56,960
to all the monks and she thought which monk needs what sort of food and she realized

113
00:09:56,960 --> 00:10:01,800
what sort of food that the monks wanted and what sort of food would make them, would bring

114
00:10:01,800 --> 00:10:06,840
them comfort and their meditation and so she prepared these foods for them and apart

115
00:10:06,840 --> 00:10:10,360
from them prepared everything for them, whatever they needed, whatever they were lacking

116
00:10:10,360 --> 00:10:16,080
or whatever she knew for herself would help them, she brought it to them and as a result

117
00:10:16,080 --> 00:10:22,040
the monks were all able to attain our hardship, as a result of attaining our hardship, they

118
00:10:22,040 --> 00:10:26,040
said to themselves, wow, at the end of the reigns they said to themselves, the time has

119
00:10:26,040 --> 00:10:29,320
come for us to go and to see the Buddha and so they went back to see the Buddha and

120
00:10:29,320 --> 00:10:35,040
the Buddha said, oh, you all look bright and radiant, you must have had suitable lodging

121
00:10:35,040 --> 00:10:41,320
and food and so on and they said, oh, did we ever this, this laywoman, she was, she

122
00:10:41,320 --> 00:10:45,160
attained Anagami and she was able to read our minds and she got us whatever we want and

123
00:10:45,160 --> 00:10:51,000
the Buddha said, oh, that's great, good for you and so on and there's something profound

124
00:10:51,000 --> 00:10:53,480
of course, they had become our hands.

125
00:10:53,480 --> 00:10:59,560
Now the story goes that there was one monk listening to this and overheard the Buddha talking

126
00:10:59,560 --> 00:11:03,840
about, talking to these monks and overheard these monks extolling the virtues of this

127
00:11:03,840 --> 00:11:12,760
laywoman who was so so perfect or such a clear mind that she was actually able to attain

128
00:11:12,760 --> 00:11:18,480
enlightenment before these monks but what he's got stuck up on is he said, this, he heard

129
00:11:18,480 --> 00:11:24,200
that this woman was bringing these monks whatever they want and this was so single-handedly

130
00:11:24,200 --> 00:11:30,560
taken care of 30 monks and as a result allowed them to attain our hardship and kind of

131
00:11:30,560 --> 00:11:35,360
a degree he thought, wow, if I go there I'll get whatever I want, if I want water I'll

132
00:11:35,360 --> 00:11:39,920
get water if I want food, I'll get food whatever kind of food I want, I'll get it and

133
00:11:39,920 --> 00:11:43,320
so he asked permission to go and say there are, I'm not even sure if he asked permission

134
00:11:43,320 --> 00:11:47,280
he just immediately went to this monastery and he said, okay, if this woman can read my

135
00:11:47,280 --> 00:11:55,240
mind, may she send someone to clean up this monastery for me because it would have been

136
00:11:55,240 --> 00:12:01,560
all covered over and leaves and so on and immediately this man shows up and or not immediately

137
00:12:01,560 --> 00:12:07,720
but after some time this man shows up and starts sweeping the monastery, wow, that's quite

138
00:12:07,720 --> 00:12:12,720
incredible and so he thinks there's no water here, may she send some sweet water and

139
00:12:12,720 --> 00:12:17,520
suddenly and after some time someone comes with some sweet water and then she thinks he

140
00:12:17,520 --> 00:12:26,040
thinks, oh and in the morning may she bring me this kind of good food and yago or congee

141
00:12:26,040 --> 00:12:31,920
or so on with sugar and this and that and dainties and so feel like that, may she bring

142
00:12:31,920 --> 00:12:37,440
me exactly what I want and she brings exactly what he thinks for, he thinks of and

143
00:12:37,440 --> 00:12:40,200
then for lunch the same thing and then he says, wow, this is incredible, I mean really

144
00:12:40,200 --> 00:12:48,680
like to meet this woman and so she and it within a no long time she comes to see him and

145
00:12:48,680 --> 00:12:53,240
he asks her, he says, wow, is it true that you can read people's minds in this way and

146
00:12:53,240 --> 00:12:54,720
he said, why do you ask?

147
00:12:54,720 --> 00:13:01,720
He said, well, you know, people send us, oh and she did her best, she avoided the question

148
00:13:01,720 --> 00:13:05,760
completely and he thought to himself, he thought for sure she can read my mind because

149
00:13:05,760 --> 00:13:11,360
she isn't saying no but because of her purity if she doesn't want to say yes and he

150
00:13:11,360 --> 00:13:13,520
said, what am I doing here?

151
00:13:13,520 --> 00:13:19,520
Suddenly just the gravity of a situation occurred to him, he said if I have one thought

152
00:13:19,520 --> 00:13:24,880
of lust, imagine this woman is going to know right away and so she'll probably grab

153
00:13:24,880 --> 00:13:30,000
me by my robe and throw me out of the monster and if she finds out all the defilements

154
00:13:30,000 --> 00:13:35,360
that I have in my mind and so the rest of the day he was just was walking around

155
00:13:35,360 --> 00:13:40,320
the paranoid or thinking, oh, I can't stay here, I can't stay ran back to the monster

156
00:13:40,320 --> 00:13:42,480
where the Buddha was.

157
00:13:42,480 --> 00:13:47,800
The Buddha saw him coming and said, you know, why aren't you staying at that monster

158
00:13:47,800 --> 00:13:51,920
and he said, I can't stay there if I have one impure my thought this woman is going to

159
00:13:51,920 --> 00:13:58,840
know it right away and then the Buddha and then the Buddha gives this verse, this is

160
00:13:58,840 --> 00:14:02,880
the teaching that he gives to this monk, he says, look, you don't have to guard your

161
00:14:02,880 --> 00:14:07,840
father that thought, can you do one thing and he said, what is the one thing and he said,

162
00:14:07,840 --> 00:14:13,640
you just guard your mind, watch your mind, stay with the mind and train your mind, the

163
00:14:13,640 --> 00:14:24,120
word is to tame the mind, just go back there, train your mind and overcome these thoughts,

164
00:14:24,120 --> 00:14:26,320
work on them.

165
00:14:26,320 --> 00:14:30,880
Because really the truth is, this woman wouldn't have thought about the monster as an

166
00:14:30,880 --> 00:14:34,560
enemy, she would have known very well the defilements that were in his mind and she would

167
00:14:34,560 --> 00:14:40,120
never complain at all and she would always work in a way as we all do to help people to overcome

168
00:14:40,120 --> 00:14:41,120
these.

169
00:14:41,120 --> 00:14:46,400
We never get angry or upset at students when they come and tell us they have lust.

170
00:14:46,400 --> 00:14:50,120
In fact, because we can't read their students' minds, we need our students to tell

171
00:14:50,120 --> 00:14:54,360
us that lust arose or anger arose or so on.

172
00:14:54,360 --> 00:14:56,960
Mahasi Sayada was saying, like this, he was saying, well, it would be great if you

173
00:14:56,960 --> 00:15:02,080
could have a teacher like that who could read your minds.

174
00:15:02,080 --> 00:15:06,680
But if you don't have one, all you have to do is explain your situation or it becomes

175
00:15:06,680 --> 00:15:09,520
your duty to explain your situation.

176
00:15:09,520 --> 00:15:11,280
That is the answer.

177
00:15:11,280 --> 00:15:15,560
The answer is for you just to tell what is going on in your practice.

178
00:15:15,560 --> 00:15:21,760
Because the problem, as I've always said, is not this greed and not the anger, it's

179
00:15:21,760 --> 00:15:26,760
the delusion, it's our ignorance, our inability to understand this situation.

180
00:15:26,760 --> 00:15:33,240
The fact that we don't see the problem with these things, the fact that we don't understand

181
00:15:33,240 --> 00:15:35,400
these things as they are.

182
00:15:35,400 --> 00:15:39,160
If we actually look at them, except that they're there and take a look at our last take

183
00:15:39,160 --> 00:15:43,640
a look at our desire, take a look at our anger, take a look at our aversion or frustration

184
00:15:43,640 --> 00:15:46,440
or boredom or depression, look at it.

185
00:15:46,440 --> 00:15:48,840
And simply see it for what it is.

186
00:15:48,840 --> 00:15:53,680
Then we will be able to become only then, we will be able to become free from it.

187
00:15:53,680 --> 00:15:58,240
So rather than worrying about these things or what other people think of us or anything,

188
00:15:58,240 --> 00:16:03,080
let us just, he said, all you have to do is train your mind, straighten your mind as

189
00:16:03,080 --> 00:16:04,160
we were talking about before.

190
00:16:04,160 --> 00:16:08,920
But here he uses the word to tame the mind or to train the mind.

191
00:16:08,920 --> 00:16:09,920
And then he said this verse.

192
00:16:09,920 --> 00:16:18,360
He said, the mind is difficult to restrain and flighty, lighting upon whatever it desires.

193
00:16:18,360 --> 00:16:24,680
The taming of the mind is good, the tamed mind brings happiness, that's the verse.

194
00:16:24,680 --> 00:16:31,720
And the story goes that as a result, he didn't go back and became enlightened as a result.

195
00:16:31,720 --> 00:16:39,000
So for our practice, I think it's quite obvious and it's quite obvious in this example as

196
00:16:39,000 --> 00:16:43,040
to the meaning of this verse.

197
00:16:43,040 --> 00:16:48,880
And yes, this is the case, the mind will run after, it's desires, it will run after what

198
00:16:48,880 --> 00:16:55,080
it wants, if it wants, or if it wants something, then it will chase after it.

199
00:16:55,080 --> 00:17:00,120
And the commentary here has something interesting to say, it says so much so that it won't

200
00:17:00,120 --> 00:17:06,240
worry about its reputation, it won't worry about what is right or what is wrong, it won't

201
00:17:06,240 --> 00:17:12,160
worry about things like, in the case of lust, someone else's wife, someone else's husband

202
00:17:12,160 --> 00:17:16,680
won't worry about things like someone else's possession, it won't worry about suffering

203
00:17:16,680 --> 00:17:22,360
for oneself, for themselves, won't worry about suffering for others, if one is angry,

204
00:17:22,360 --> 00:17:26,200
even though one knows that one is hurting oneself, one will kick things and punch things,

205
00:17:26,200 --> 00:17:29,880
punching the wall and causing great suffering to oneself.

206
00:17:29,880 --> 00:17:34,480
This is the meaning of the word yatakama, nipati, you know, whatever it wants, no matter

207
00:17:34,480 --> 00:17:40,640
what, whatever the defilements lead one to, it will go there no matter what, in complete

208
00:17:40,640 --> 00:17:48,800
disregard for what is right or wrong, so this is why we see infidelity and this is why we

209
00:17:48,800 --> 00:17:55,400
have theft, this is why we have murder, this is why we have suicide, all of these things

210
00:17:55,400 --> 00:18:02,040
because of our desires and our versions and our inability to control them.

211
00:18:02,040 --> 00:18:06,640
So rather than control them, the Buddha says to train the mind, to train the mind to

212
00:18:06,640 --> 00:18:10,520
see them clearly, to see them as they are, this is the word vipasana.

213
00:18:10,520 --> 00:18:15,960
Just to see them from what they are, because the Buddha says you can't restrain the mind,

214
00:18:15,960 --> 00:18:21,480
you can't tie it down and stop it from going these things, it's lahu, the word is lahu,

215
00:18:21,480 --> 00:18:26,520
which means light or flighty, like a bird, these words are actually what you would use

216
00:18:26,520 --> 00:18:31,000
to talk about a bird, the last time we had a fish now we have a bird, it's like a bird

217
00:18:31,000 --> 00:18:35,160
because it lights on whatever it wishes, but it doesn't like it here, it will go there,

218
00:18:35,160 --> 00:18:41,840
it won't go here, as soon as it's something scares it, it will be gone, this is the mind,

219
00:18:41,840 --> 00:18:47,480
you can't catch it, it's not a physical thing that you can put in the cage, if it were

220
00:18:47,480 --> 00:18:52,120
the body you can tie the body up, you can cover your mouth, you can plug your ears,

221
00:18:52,120 --> 00:18:57,480
you can close your eyes, you can stop up your nose, you can stop, you can, the body you

222
00:18:57,480 --> 00:19:02,680
can stop, but the mind you can't stop, no matter if you do all of these things, the mind

223
00:19:02,680 --> 00:19:07,480
will still escape to cause you endless endless grief and suffering, in fact if you do such a

224
00:19:07,480 --> 00:19:14,040
thing you'll find for most people that just go crazy as a result, this is the funny thing

225
00:19:14,040 --> 00:19:18,600
because you think you can control it and the more you try to control your mind, maybe you try

226
00:19:18,600 --> 00:19:22,600
to take medicine, some people when they have headaches or when they're thinking too much,

227
00:19:22,600 --> 00:19:28,840
they'll take pills or drugs and the more you take them, the worse it gets and the more wound up

228
00:19:28,840 --> 00:19:35,320
the mind becomes, so the more you try to tie the mind down, the more, the more of a problem

229
00:19:35,320 --> 00:19:40,280
your mind becomes, so this is the Buddha taught us to train the mind to see clearly, this is

230
00:19:40,280 --> 00:19:45,320
in the last one we were talking about straightening the mind, but the word tame here or dhamma

231
00:19:45,320 --> 00:20:00,200
or dhamta, this means making the mind like a trained animal, like a horse or a buffalo or

232
00:20:00,200 --> 00:20:05,640
something, something that is trained and understands what is the important thing to do,

233
00:20:05,640 --> 00:20:13,800
so teaching the mind to understand what is of its benefit, what is to its detriment, understanding

234
00:20:13,800 --> 00:20:21,080
the anger, understanding the greed, seeing it for what it is, so this is very much a reminder to

235
00:20:21,080 --> 00:20:27,880
us that the untrained mind is the cause for all of our suffering and it's simply by training the

236
00:20:27,880 --> 00:20:31,880
mind that it leads to happiness, the first kind of happiness of course that it leads to is that

237
00:20:31,880 --> 00:20:37,320
it overcomes help a system overcomes suffering, so as these monks said it was the untrained mind

238
00:20:37,320 --> 00:20:43,560
that had agreed, that had anger, that had delusion, that was, that had opened up the hells in front of

239
00:20:43,560 --> 00:20:49,160
them, make it even see in front of them that if they were to just follow their desires, follow their

240
00:20:49,160 --> 00:20:55,400
versions, it's open like the doors of their own house with a big welcome that in front, this is

241
00:20:55,400 --> 00:21:00,360
what they've said, so the training of the mind, the taming of the mind stops that, it stops the

242
00:21:00,360 --> 00:21:06,840
mind from exploding, it stops the mind from going insane, it stops the mind from following after

243
00:21:06,840 --> 00:21:11,720
desires and the versions and causing so much suffering and what we can see it, even by when people

244
00:21:11,720 --> 00:21:16,600
get angry they kick things and they punch things as they said or they hurt other people, how we

245
00:21:16,600 --> 00:21:22,440
destroy our lives, even in this world you can understand how training the mind taming the mind brings

246
00:21:22,440 --> 00:21:29,400
happiness, if you think of how the world, mostly people think that happiness comes from war, from

247
00:21:29,400 --> 00:21:35,320
conflict and from victory, so what is the result of that, we see how the world has gone to war

248
00:21:35,320 --> 00:21:41,640
and when the world is at war, so a little peace in the world, the peace vanishes like

249
00:21:43,400 --> 00:21:52,680
a mirage and what you're left with is conflict and the victor gains the ashes of the spoils,

250
00:21:53,880 --> 00:22:03,240
when you get a world of rubble and fire and suffering, the suffering that comes from war is something

251
00:22:03,240 --> 00:22:11,880
that we have all been told about and read about and heard the records of even in our society

252
00:22:11,880 --> 00:22:19,800
when we look at capitalism or materialism or when we look at this competitive society,

253
00:22:19,800 --> 00:22:30,600
when it is brought to us, how much suffering, how much poverty, how much lack of trust

254
00:22:30,600 --> 00:22:35,400
this is brought to the world, how we can't even trust our neighbors, we can't trust our community,

255
00:22:36,840 --> 00:22:42,120
we can't trust the people at the stores, we can't trust the banks, we can't trust anybody,

256
00:22:42,120 --> 00:22:46,920
we can no longer trust each other because our minds, because their minds are not trained,

257
00:22:46,920 --> 00:22:52,840
because their minds are full of these things, even in the family, the untrained mind amongst family,

258
00:22:52,840 --> 00:23:00,440
friends at work, if you talk to anyone about their job situation and how it's going

259
00:23:00,440 --> 00:23:05,320
almost anyone in the world will be able to tell you about the fighting and the politics that

260
00:23:05,320 --> 00:23:10,280
goes on at work and if you ask people about their home life, most people nowadays because of

261
00:23:10,280 --> 00:23:17,800
the nature of our minds and our desires and everyone's conflicting desires, the anger and the

262
00:23:17,800 --> 00:23:24,520
fighting and the harsh words, even the abuse that goes on as a result of our untrained mind,

263
00:23:24,520 --> 00:23:30,040
how people even know they want to find happiness and they want others to be happy as well,

264
00:23:30,040 --> 00:23:33,960
they will end up bringing suffering, suffering to both themselves and to others.

265
00:23:34,680 --> 00:23:41,720
This is all from the untrained mind, if we begin to practice meditation and then we consider our

266
00:23:41,720 --> 00:23:49,800
own lives, because of our, the stability of mind that comes in the ability to reflect clearly

267
00:23:49,800 --> 00:23:58,600
on what we've come from, we'll be able to see the suffering that we've been causing in our

268
00:23:58,600 --> 00:24:03,960
lives, it become quite clear to us where all of our suffering comes from and it'll become clear to

269
00:24:03,960 --> 00:24:09,560
us that we didn't read, that we were in, we were much suffering, that we didn't even realize

270
00:24:10,600 --> 00:24:15,240
that we thought we were living in great peace and, and more or less lying to ourselves.

271
00:24:15,240 --> 00:24:19,560
Once you begin to practice meditation, you start to, all this, all of this comes up in much

272
00:24:19,560 --> 00:24:24,280
guilt about all of the things that you've done to her, others when you were just avoiding it the

273
00:24:24,280 --> 00:24:29,000
whole time, pretending it didn't exist and now as your mind starts to calm down, you get to see

274
00:24:29,000 --> 00:24:34,840
everything that's in there. So for someone who's practiced meditation, this is a clear teaching

275
00:24:34,840 --> 00:24:41,800
that it is the trained mind, the tamed mind, the straight in mind that brings happiness. So it's a good

276
00:24:42,680 --> 00:24:47,880
teaching for us to always remember, jit dang dang tang sukha wa hang, it's one that is often

277
00:24:47,880 --> 00:24:53,960
taught in Buddhist circles, that the trained or the tamed mind brings happiness.

278
00:24:54,840 --> 00:25:02,920
So just one more verse in our series on the dambapada. Thank you all for tuning in and we'll see you

279
00:25:02,920 --> 00:25:18,840
in the next time.

