1
00:00:00,000 --> 00:00:07,120
I live with a Christian family and I'm trying to fix my sleep habits.

2
00:00:07,120 --> 00:00:12,480
When I am awake, they ask me questions and bug me during my meditation.

3
00:00:12,480 --> 00:00:15,160
If I ignore them, they get mad.

4
00:00:15,160 --> 00:00:17,160
What should I do?

5
00:00:17,160 --> 00:00:19,240
It's an odd sort of question.

6
00:00:19,240 --> 00:00:25,920
I think we need a little more information.

7
00:00:25,920 --> 00:00:36,760
The feeling I'm getting is that your sleep all the time because when you're awake, they ask

8
00:00:36,760 --> 00:00:41,840
you questions and you want to stop that, but you don't want to be awake because they ask

9
00:00:41,840 --> 00:00:43,800
you questions.

10
00:00:43,800 --> 00:00:51,680
I'm not quite sure how to answer, I mean the second part is quite easy to answer.

11
00:00:51,680 --> 00:00:59,680
But the whole thing about the sleep habits seems like a bit of a non sequitur or I don't

12
00:00:59,680 --> 00:01:05,440
quite see how the sleeping habits relates to people making you mad, except maybe you

13
00:01:05,440 --> 00:01:11,240
can't sleep because they're always bugging you.

14
00:01:11,240 --> 00:01:21,560
So how do we answer this if people bug you when you're awake and when you're practicing

15
00:01:21,560 --> 00:01:27,400
they ask you questions when you're awake and bug you during your meditation.

16
00:01:27,400 --> 00:01:31,440
If you ignore them, they get mad, what should you do?

17
00:01:31,440 --> 00:01:44,600
Well, when people ask you questions, that's a wonderful thing, I think the bad part is

18
00:01:44,600 --> 00:01:53,920
when people stop asking questions when there is no dialogue and I assume eventually you'll

19
00:01:53,920 --> 00:02:01,440
come to that once you are able to answer their questions and ask your own questions because

20
00:02:01,440 --> 00:02:05,240
they won't be able to answer your questions but through the practice you should be able

21
00:02:05,240 --> 00:02:09,880
to answer their questions.

22
00:02:09,880 --> 00:02:18,080
I see, you're usually up at night trying to make it during the day, they are up during

23
00:02:18,080 --> 00:02:19,080
the day.

24
00:02:19,080 --> 00:02:22,280
When I practice during the day they ask me questions about what I'm doing and why they

25
00:02:22,280 --> 00:02:27,560
don't like me practicing.

26
00:02:27,560 --> 00:02:42,040
Well, you're practicing to make yourself a better person, you're practicing to clear your

27
00:02:42,040 --> 00:02:46,200
mind, to calm your mind if they're Christians they should understand because Jesus himself

28
00:02:46,200 --> 00:02:54,920
went off into the jungle and practiced meditation but of course they have exposed different

29
00:02:54,920 --> 00:03:01,400
ideas and theories about it.

30
00:03:01,400 --> 00:03:07,680
I think you can take that route with them especially if they're getting angry at you.

31
00:03:07,680 --> 00:03:11,440
You stick with your answers that you're doing it to become a better person because you

32
00:03:11,440 --> 00:03:20,040
have anger inside and do have aversion towards things like people bugging you, you have

33
00:03:20,040 --> 00:03:27,040
attachments and delusions and so I'm going to admit this to them and tell them that's why

34
00:03:27,040 --> 00:03:31,560
you're practicing because when you practice it helps you to learn about yourself, it helps

35
00:03:31,560 --> 00:03:44,040
you to see what's causing suffering and once you see what's causing suffering to change

36
00:03:44,040 --> 00:03:52,200
yourself to become a better person and then if they argue with that well you can always

37
00:03:52,200 --> 00:03:56,680
answer their questions to a certain point if there are questions just become silly then

38
00:03:56,680 --> 00:04:02,120
you can just ignore them and when they get angry at you, I mean that's at least you've

39
00:04:02,120 --> 00:04:06,840
made it clear that this is what you're trying to do away with so then you can ask them

40
00:04:06,840 --> 00:04:09,840
so when you're angry at me like this how does it feel?

41
00:04:09,840 --> 00:04:18,880
Do you feel good being angry like that? Does it make you, does it make you satisfied

42
00:04:18,880 --> 00:04:25,600
to get angry like this? How do you feel when you're angry and the idea is that they

43
00:04:25,600 --> 00:04:30,440
will be able to answer that it's not pleasant but that's not the point, the point is you're

44
00:04:30,440 --> 00:04:36,720
making me angry and you can say well I can't make you angry just as you can't make

45
00:04:36,720 --> 00:04:44,880
me angry, the anger comes from within yourself and it comes from your inability to or

46
00:04:44,880 --> 00:04:55,200
your preference for something else, your inability to bear what's going on and it's a cause

47
00:04:55,200 --> 00:04:59,360
of suffering and that's why I'm practicing meditation, this is why I'm practicing meditation

48
00:04:59,360 --> 00:05:04,240
because I understand that anger is something that brings suffering and something that's

49
00:05:04,240 --> 00:05:10,640
useless, it brings conflict, anger is something that causes suffering and others, causes stress

50
00:05:10,640 --> 00:05:18,600
and destroys friendships, destroys family bonds and so on. So you have them to see what's

51
00:05:18,600 --> 00:05:23,480
really the problem, the problem isn't that you're not Christian, they're not practicing

52
00:05:23,480 --> 00:05:30,800
the teachings of Jesus Christ, the problem is that we all have defilements, if we didn't

53
00:05:30,800 --> 00:05:38,000
have defilements there would be no problem. But as far as you're sleeping habits it's

54
00:05:38,000 --> 00:05:42,560
not a real problem, you know in the worst case yeah you could just sleep all day and stay

55
00:05:42,560 --> 00:05:50,000
up all night and avoid argument but you should make it clear to them that from your point

56
00:05:50,000 --> 00:05:54,480
of view the problem is not that you're practicing something outside of Christianity, the

57
00:05:54,480 --> 00:06:00,640
problem is that you and they have defilements and that's a real problem with this

58
00:06:00,640 --> 00:06:04,720
world has and you can point out to them what's going on in the world is the world's problems

59
00:06:04,720 --> 00:06:09,840
because people don't follow Christ or is it because people have anger, is it because people

60
00:06:09,840 --> 00:06:17,760
have this or that defilement and you can point out that actually when it seems like the

61
00:06:17,760 --> 00:06:22,800
teachings of Christ are very much to purify your mind to make yourself a better person.

62
00:06:22,800 --> 00:06:32,800
So I hope that helps.

