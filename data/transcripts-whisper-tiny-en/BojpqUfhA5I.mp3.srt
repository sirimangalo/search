1
00:00:00,000 --> 00:00:19,400
Okay, good evening, everyone, welcome to Evening Dumber.

2
00:00:19,400 --> 00:00:29,880
Tonight we are looking at purification of view, and this is the first of the purification

3
00:00:29,880 --> 00:00:49,760
we're relating to wisdom, the first two purifications correspond directly with morality and

4
00:00:49,760 --> 00:00:58,480
concentration or sealant's money, the third to the seventh, as I said, have to do with wisdom,

5
00:00:58,480 --> 00:01:04,080
so this is number three.

6
00:01:04,080 --> 00:01:14,160
And in a way, this is where it becomes more about results and about practice, it's hard

7
00:01:14,160 --> 00:01:21,520
to really say that you're practicing wisdom, you're training in wisdom, and it is described

8
00:01:21,520 --> 00:01:22,520
that way.

9
00:01:22,520 --> 00:01:35,160
In fact, to some extent you wouldn't, to some extent you don't even practice concentration.

10
00:01:35,160 --> 00:01:44,840
Concentration is a result, but the point is really what you're practicing is mindfulness throughout

11
00:01:44,840 --> 00:01:53,720
the past, when you cultivate ethics, it's really about being mindful, true ethical

12
00:01:53,720 --> 00:02:02,840
behavior, morality, proper behavior and speech on the past comes from being mindful.

13
00:02:02,840 --> 00:02:11,240
It doesn't come from saying I'm not going to do this, I'm not going to do that, so of

14
00:02:11,240 --> 00:02:19,240
course temporary, it is a form of morality, but it's only make sure it doesn't actually

15
00:02:19,240 --> 00:02:27,120
set the mind in the right state to cultivate concentration, but when your mind is, when

16
00:02:27,120 --> 00:02:34,160
your mind for morality, your ethical behavior, the behavior that results for mindfulness

17
00:02:34,160 --> 00:02:44,320
needs to focus, to samadhi, and when you're focused in the right way, and you continue

18
00:02:44,320 --> 00:02:56,600
to be mindful with a focused sort of mindfulness, then wisdom arises.

19
00:02:56,600 --> 00:03:07,400
And so when we talk about right view or purification of view, we're not exactly talking

20
00:03:07,400 --> 00:03:20,880
about views, we were certainly considering the danger of beliefs and views, especially the

21
00:03:20,880 --> 00:03:34,360
views of self and identity, but the purification of view is much more about a purification

22
00:03:34,360 --> 00:03:49,400
of the way we look at the world, so view in a sense of literally how we view reality.

23
00:03:49,400 --> 00:03:59,120
Our ordinary way of viewing reality is based on concepts of people, places, things, and

24
00:03:59,120 --> 00:04:06,160
so if I tell you people and places and things don't exist, it seems absurd.

25
00:04:06,160 --> 00:04:15,680
And in fact, it is rather meaningless to say something like that, because in order to say

26
00:04:15,680 --> 00:04:23,160
that something exists or doesn't exist, you have to ask in what sense, right, in what

27
00:04:23,160 --> 00:04:31,440
sense do I not exist, do you not exist, obviously clearly in some sense people, places, and

28
00:04:31,440 --> 00:04:36,920
things do exist.

29
00:04:36,920 --> 00:04:44,000
And so to some extent, it appears somewhat arbitrary, which way of looking at the world

30
00:04:44,000 --> 00:04:50,120
you choose, obviously if you choose to look at the world from a phenomenological point

31
00:04:50,120 --> 00:04:58,160
of view, based on experiences, where you're never going to experience a person, a place

32
00:04:58,160 --> 00:05:02,840
or a thing.

33
00:05:02,840 --> 00:05:09,040
Someone asked me this morning, I think, when it was one of you, lastly, recently, does

34
00:05:09,040 --> 00:05:15,000
the brain exist?

35
00:05:15,000 --> 00:05:20,560
The mind and the brain exist independent of each other, and it's a good question because

36
00:05:20,560 --> 00:05:30,320
it illustrates this point quite well, because from a point of view of concepts of entities,

37
00:05:30,320 --> 00:05:40,760
the brain exists, and the mind doesn't exist, because you don't, you look around and

38
00:05:40,760 --> 00:05:46,440
you don't find the mind, right, the mind isn't a physical thing that you can identify,

39
00:05:46,440 --> 00:05:54,240
there it is, the mind isn't anywhere, it doesn't take up space, the mind is not like

40
00:05:54,240 --> 00:06:01,120
this table or this rug or a person, which is why we get into this, based on this sort

41
00:06:01,120 --> 00:06:09,600
of view, we get into the idea of the person being made up of the brain, and so the mind

42
00:06:09,600 --> 00:06:16,200
is just the brain, because we don't have any way of thinking about the mind apart from

43
00:06:16,200 --> 00:06:22,880
the brain, it's completely based on the way we look at the world, our view.

44
00:06:22,880 --> 00:06:31,960
From the point of view, from the phenomenological point of view, the mind exists clearly,

45
00:06:31,960 --> 00:06:40,560
the mind is how phenomena are experienced, and the brain doesn't exist, because suppose

46
00:06:40,560 --> 00:06:47,520
you see a brain, well, that's just seeing, and based on the seeing, there is the conceiving,

47
00:06:47,520 --> 00:06:57,840
it's just the conceiving, I don't know if this seems some theoretical, but it's clearly

48
00:06:57,840 --> 00:07:03,760
not, for anyone who's practiced meditation, this is the shift that needs to take place

49
00:07:03,760 --> 00:07:11,680
from one to the other, and the point, the claim I think that we would clearly try to make

50
00:07:11,680 --> 00:07:20,200
quite clearly, is that they're not equal ways of looking at the world, a conceptual way

51
00:07:20,200 --> 00:07:25,920
of looking at the world doesn't really help you, doesn't really change, doesn't change

52
00:07:25,920 --> 00:07:33,280
the way you are, when we talk about experience and wisdom in an ordinary sense, not

53
00:07:33,280 --> 00:07:41,680
in a Buddhist or meditative sense, has a very hard thing for us to grasp, because we're mostly

54
00:07:41,680 --> 00:07:47,280
not cultivating wisdom, the wisdom has nothing to do with concepts, it can't have anything

55
00:07:47,280 --> 00:07:54,800
to do with concepts, and you can look at, you can take a microscope and look at how

56
00:07:54,800 --> 00:08:00,280
the brain works, and I can tell you all about how the addiction cycle and the brain works,

57
00:08:00,280 --> 00:08:05,240
I don't know too much about it, but I have asked people about it, and their scientists

58
00:08:05,240 --> 00:08:11,480
can tell you so much about how addiction works, they can be very clear about it, all the

59
00:08:11,480 --> 00:08:16,600
addiction cycle works in the brain, but be completely powerless to free themselves from

60
00:08:16,600 --> 00:08:26,120
addiction, let alone anyone else, and so they offer all these medications, psychoactive

61
00:08:26,120 --> 00:08:35,000
drugs that appear to change the makeup of the brain, and therefore somehow cure the problem,

62
00:08:35,000 --> 00:08:40,840
but of course they don't come close to carrying the problem.

63
00:08:40,840 --> 00:08:53,560
When we talk about wisdom, we're talking about experience, we don't perhaps realize it,

64
00:08:53,560 --> 00:09:00,440
we talk about people who have lived older people, who have lived a long life, having

65
00:09:00,440 --> 00:09:05,240
wisdom, and as very much to do with the experiences, usually the suffering that they've

66
00:09:05,240 --> 00:09:15,800
gone through, personally, their personal experiences of reality, how they've lived through

67
00:09:15,800 --> 00:09:23,960
addiction, how they've lived through suffering, how they live through it through difficulty,

68
00:09:23,960 --> 00:09:31,720
their perspective on the world, not in terms of people they've met or so on, things

69
00:09:31,720 --> 00:09:40,760
they've seen, but completely in regards to their experiences, and so it's absolutely

70
00:09:40,760 --> 00:09:48,520
essential for to gain true wisdom, that we take up this, that we take up an intensive

71
00:09:48,520 --> 00:09:55,960
form of this experience-based learning, and by learning we mean learning wisdom, not

72
00:09:55,960 --> 00:10:03,160
learning intelligence. What are you learning? You're learning the content of your learning is

73
00:10:03,160 --> 00:10:09,400
wisdom. The content is not intelligent, you're not trying to gain intelligence, and there's

74
00:10:09,400 --> 00:10:13,880
just words, but they're kind of important, because by intelligence we would of course

75
00:10:13,880 --> 00:10:19,640
mean information, like I'm giving you information right now. I can't give you enlightenment,

76
00:10:19,640 --> 00:10:27,880
I can't give you wisdom. Wisdom is a useful word to separate away, and this is where wisdom

77
00:10:27,880 --> 00:10:35,720
really resides. This is how we distinguish wisdom from intelligence. Wisdom is what you gain

78
00:10:35,720 --> 00:10:46,200
through experience, so this is what you begin to acquire through the meditation practice,

79
00:10:47,480 --> 00:10:59,560
and it's the one thing about wisdom and wisdom that I think confounds people is they think of

80
00:10:59,560 --> 00:11:19,880
it as something deep, abstruse, far away, something enigmatic, elusive. But in fact, wisdom is,

81
00:11:20,440 --> 00:11:25,560
when you know that the stomach is rising, my teacher used to say, knowing the stomach is rising,

82
00:11:25,560 --> 00:11:37,960
when you know the stomach is falling, that's wisdom. Wisdom is actually quite simple. Wisdom is

83
00:11:37,960 --> 00:11:44,440
to see things as they are, and the first step in wisdom is to be able to see it rising,

84
00:11:45,160 --> 00:11:50,520
to know rising is rising, falling is falling. I mean the Buddha said it's much,

85
00:11:50,520 --> 00:12:00,840
let's seeing, be seeing, that's how we train. The first and most, sort of the most essential

86
00:12:04,600 --> 00:12:12,600
stage of wisdom, in some sense, most essential because it really sets you on the path,

87
00:12:12,600 --> 00:12:22,040
without it, you're not even on the path. The most essential is this acquiring of right view

88
00:12:23,320 --> 00:12:29,880
looking at the world in the right way. So when you walk, it's not you walking,

89
00:12:30,760 --> 00:12:37,080
an ordinary way of looking at walking, this is me walking. So I asked you

90
00:12:37,080 --> 00:12:41,880
to, right, stepping right, stepping left, or they're one thing or something that things in the world,

91
00:12:42,440 --> 00:12:47,960
you know, it's me walking, that's one thing. Of course, the meditate of things, that's absurd,

92
00:12:47,960 --> 00:12:52,680
right? I told you, this is one thing, and this is the same thing as you, at this point,

93
00:12:52,680 --> 00:12:57,720
I'm hoping that you're quite clear that they're two very separate things, because they're two

94
00:12:57,720 --> 00:13:05,960
experiences, very clear that something you should feel proud about, and don't feel proud,

95
00:13:05,960 --> 00:13:11,960
but it should give you confidence. Oh yeah, I'm not. I'm not without wisdom,

96
00:13:13,960 --> 00:13:22,280
because it seems quite simple, but it confounds people who have a practice meditation.

97
00:13:24,840 --> 00:13:31,320
So the first thing is the ability to separate reality in the moment, instead of things,

98
00:13:31,320 --> 00:13:43,880
an important milestone to realize, is when the meditator makes the shift and realizes

99
00:13:44,760 --> 00:13:51,480
that entities don't exist. For a moment to moment, all that exists is experiences that arise

100
00:13:51,480 --> 00:13:56,680
and sees. When you're walking, what is real? This idea of me walking, it's all up here,

101
00:13:56,680 --> 00:14:02,520
and something you think, yeah, I'm walking. No, this is you thinking, this is a thought,

102
00:14:03,080 --> 00:14:09,240
and that thought arises and sees this. The foot, the right foot moving,

103
00:14:10,520 --> 00:14:14,840
arises and sees this, the left foot moving, and then this is intellectual, I can tell you this,

104
00:14:15,480 --> 00:14:21,320
it doesn't really help you. There's two groups, some people hear this, and they think

105
00:14:21,320 --> 00:14:27,080
they understand it conceptually, they understand it rationally, and other people are saying,

106
00:14:27,080 --> 00:14:33,400
well of course, yes, this is how I see things, they think that people have reached this purification

107
00:14:33,400 --> 00:14:43,320
of you. So there's probably not, it's not incredibly helpful that I'm describing it to people who

108
00:14:43,320 --> 00:14:50,280
haven't meditated, but I assume that everyone here is on the verge of this. So I urge you at this

109
00:14:50,280 --> 00:14:55,640
point, and from this point, in this series of talks, it's all going to be very much about your

110
00:14:55,640 --> 00:15:00,760
meditation practice, if you're not meditating, this sort of talk's not going to be very useful to

111
00:15:00,760 --> 00:15:06,200
you. For some people, it might even be to their detriment, because when they begin to meditate,

112
00:15:06,200 --> 00:15:10,120
they'll be looking and trying to, okay, he said, I'm going to see this, where is it?

113
00:15:10,120 --> 00:15:21,720
And a mild detriment to the sense that you might obsess over it, and it's not prohibitive

114
00:15:21,720 --> 00:15:30,120
in the sense that it's going to prevent you from realizing you just have to be mindful

115
00:15:30,120 --> 00:15:40,840
and you're thinking, thinking. But this comes through mindfulness. The ability to understand death

116
00:15:40,840 --> 00:15:47,560
is what we call it. The death of a person is just a concept, that's not what real death is,

117
00:15:47,560 --> 00:15:55,000
real death is every moment. Understand that everything that arises ceases for just a moment.

118
00:15:55,000 --> 00:16:06,120
What really exists is not people, places, and things that experience it. To understand that

119
00:16:06,120 --> 00:16:16,520
this world of concepts is quite problematic, because it belies a underlying impermanence,

120
00:16:16,520 --> 00:16:23,480
or it masks an underlying impermanence. If you say this person exists, this is where the shock comes

121
00:16:23,480 --> 00:16:32,760
when they die. I think, well, what happened to the person? When we think of things as being this,

122
00:16:33,560 --> 00:16:40,760
when they change, we get upset, when they break, when they disappear. If we understand reality

123
00:16:40,760 --> 00:16:46,520
from a point of view of experiences, it's quite useful. And there's much that comes from it,

124
00:16:46,520 --> 00:16:52,120
but basically what comes is we don't have these expectations. If you understand that stepping

125
00:16:52,120 --> 00:16:58,520
right, stepping left, and the stomach rising and falling, if you understand that, these things arise

126
00:16:58,520 --> 00:17:05,720
and cease. There's nothing to hold on to. When you look at a person, it's just an experience. When

127
00:17:05,720 --> 00:17:12,840
you think about your own person, who you are is just moments of experience. What are you going to

128
00:17:12,840 --> 00:17:20,440
hold on to? What will you cling to? When you stop clinging, of course, well, this is the chain

129
00:17:20,440 --> 00:17:27,960
reaction of stages of wisdom and income. And the second thing about purification of view

130
00:17:29,400 --> 00:17:39,240
is the ability to distinguish body and mind. So the first part is a distinguishing body from

131
00:17:39,240 --> 00:17:44,680
body and mind, for mind, meaning moments of physical experience from the next moment,

132
00:17:44,680 --> 00:17:51,720
born, die, born, die, and mental the same. When you know that the right foot is moving,

133
00:17:53,400 --> 00:17:58,760
that knowledge arises and ceases as well. You know, the left foot is moving, that knowledge arises.

134
00:17:58,760 --> 00:18:03,240
So this is a rising and ceasing moment, the discreetness of phenomena.

135
00:18:05,240 --> 00:18:08,360
But the second one is the ability to distinguish physical and mental.

136
00:18:08,360 --> 00:18:15,480
And it's a bit of a mind game to think, well, is the physical different from the mental. That's

137
00:18:15,480 --> 00:18:21,640
not really so important. Although for a meditator, it should be conceptually quite clear to say,

138
00:18:21,640 --> 00:18:27,480
yes, mind is obviously something quite different from the body or the physical aspect of experience

139
00:18:27,480 --> 00:18:34,280
is quite different from the mental aspect. But, you know, the real point is simply to be able to

140
00:18:34,280 --> 00:18:41,080
say that's all there is. What is reality made only really these two things. There's physical

141
00:18:41,080 --> 00:18:47,960
aspects clearly. I mean, when I walk, there's the feelings of pressure and hot and cold and hard

142
00:18:47,960 --> 00:18:54,600
and soft. But there's also clearly the awareness of it. My foot can move in my mind somewhere else

143
00:18:54,600 --> 00:19:00,680
and thinking about something else entirely. So clearly there are two distinct and individual

144
00:19:00,680 --> 00:19:06,920
and entities. If the mind doesn't go to the foot, there's no experience of walking.

145
00:19:09,160 --> 00:19:15,400
Right? So by that, you think, well, if my mind is somewhere else, am I not walking? It's like

146
00:19:15,400 --> 00:19:20,200
if a tree falls in the forest and no one's there to hear it, does it really make a sound?

147
00:19:23,560 --> 00:19:28,760
And what that tells us is that, yes, when the physical, in some sense, exists independent of the

148
00:19:28,760 --> 00:19:33,480
mental because clearly if my mind is distracted while my foot is still moving, right?

149
00:19:35,960 --> 00:19:41,320
It's not like reality goes on pause because the mind isn't focused on it. If you're driving

150
00:19:41,320 --> 00:19:45,720
down the street and you get distracted, it doesn't mean the car stops. The physical reality continues.

151
00:19:47,480 --> 00:19:53,080
But the mental reality is distinct from the physical reality.

152
00:19:53,080 --> 00:20:03,160
And what's important about this is simply to destroy the notion of a self or a soul

153
00:20:08,200 --> 00:20:16,920
or a god or anything of that sort. And this would mean this is where really impacts our views,

154
00:20:16,920 --> 00:20:30,120
changes our views, people can't exist, souls, selves, to understand that all of these things

155
00:20:30,120 --> 00:20:37,320
exist really only on a conceptual level. They end up being groupings of experience

156
00:20:39,000 --> 00:20:44,920
that are conveniently convenient and practical. It's practical to say that this is a microphone

157
00:20:44,920 --> 00:20:49,240
and this is a computer and it's practical to know those things and not just say, oh, here's an

158
00:20:49,240 --> 00:20:56,040
experience of seeing a meditative point. All you need is experience, but from a practical world,

159
00:20:56,040 --> 00:21:02,680
they point, of course, you need to live in the world of concepts. So again, these two different

160
00:21:02,680 --> 00:21:10,920
ways of looking at reality, but in order to cultivate wisdom, you need this way of looking at the

161
00:21:10,920 --> 00:21:18,840
world. And so this is the first important step on the path to wisdom, to give you certainty

162
00:21:20,600 --> 00:21:28,360
of the leading right view or right outlook. There you go, that's number three in the purification.

163
00:21:28,360 --> 00:21:38,360
That's our number four tonight.

164
00:21:38,360 --> 00:22:00,360
All right, we've got some questions now.

165
00:22:00,360 --> 00:22:06,360
Hello, everyone.

166
00:22:30,360 --> 00:22:37,360
Alright, the website's not loading, so that's an excuse not to answer questions.

167
00:22:37,360 --> 00:22:48,360
That's all for tonight. Thank you all for tonight.

168
00:22:48,360 --> 00:22:54,360
I got the audio feed working as well, so hopefully there was an audio stream for anyone

169
00:22:54,360 --> 00:23:00,360
who might happen to be following that or what listening to the mp3 later.

170
00:23:00,360 --> 00:23:02,360
Those mp3s shouldn't be.

171
00:23:02,360 --> 00:23:06,360
If all is working as planned up on our website.

172
00:23:06,360 --> 00:23:13,360
Thank you all again for tuning in. Have a good night.

