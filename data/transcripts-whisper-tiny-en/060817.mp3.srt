1
00:00:00,000 --> 00:00:28,000
Starting today, I'd like to have a series of talks on the Mankarasuta.

2
00:00:28,000 --> 00:00:46,000
Mankarasuta is a very important discourse given by the Lord Buddha on the blessings which we can find in the world.

3
00:00:46,000 --> 00:00:54,000
The true blessings which exist in the world.

4
00:00:54,000 --> 00:01:10,000
These things which are truly and in an ultimate sense are very real and really are blessings.

5
00:01:10,000 --> 00:01:23,000
The word Mankarla is a word which will translate normally as blessing, but it also means things which are auspicious.

6
00:01:23,000 --> 00:01:42,000
In the time of the Lord Buddha, the word Mankarla was used for things which were said to bring luck.

7
00:01:42,000 --> 00:02:00,000
Never said to bring happiness or said to bring good results in the future.

8
00:02:00,000 --> 00:02:26,000
For instance, people would believe that if you heard a certain bird in the morning, a certain bird if you heard this bird in this bird crower or making its noise in the singing in the morning, this is good luck.

9
00:02:26,000 --> 00:02:32,000
If you heard another bird crowing, this was bad luck.

10
00:02:32,000 --> 00:02:43,000
If you see something, you see this, you see that, it was good luck.

11
00:02:43,000 --> 00:03:02,000
Some people have superstition if you saw a record, an ascetic in the morning or if you saw an ascetic.

12
00:03:02,000 --> 00:03:15,000
It had different meanings.

13
00:03:15,000 --> 00:03:35,000
There are many superstitions which we've heard about.

14
00:03:35,000 --> 00:03:54,000
Just because a black cat crosses your path, you carry around the foot of a dead rabbit, the shoe of a horse, that for some reason it should bring good luck to or bad luck to.

15
00:03:54,000 --> 00:04:00,000
The Lord Buddha said, these are not real blessings.

16
00:04:00,000 --> 00:04:07,000
So a question arose in the time of the Lord Buddha.

17
00:04:07,000 --> 00:04:15,000
Among the angels asking among themselves, what were the real things which brought good luck?

18
00:04:15,000 --> 00:04:22,000
What things really were blessings or were auspicious or a cause for goodness?

19
00:04:22,000 --> 00:04:30,000
What things which led to goodness, to happiness, peace?

20
00:04:30,000 --> 00:04:44,000
And they were asking among themselves, and finally it came to the King of the Angels.

21
00:04:44,000 --> 00:04:52,000
The King of the Angels, of course, was a follower of the Lord Buddha.

22
00:04:52,000 --> 00:05:03,000
When he heard all the angels asking themselves when to ask the King of the Gods, what are the real blessings in the world?

23
00:05:03,000 --> 00:05:15,000
He was having a horse shoe, a really good luck, or a rabbit's foot, a really good luck, or so on, whatever other things which they thought may be blessings,

24
00:05:15,000 --> 00:05:26,000
be good things at the time.

25
00:05:26,000 --> 00:05:34,000
And the King of the Angels said, I can't answer this question for you, there's only one person in the universe.

26
00:05:34,000 --> 00:05:48,000
In the universe with its angels and its gods and its men and beasts, there's only one being which can answer this question.

27
00:05:48,000 --> 00:05:51,000
You have to go see the Lord Buddha.

28
00:05:51,000 --> 00:05:56,000
Now he's staying in Jade the one, and go to see him.

29
00:05:56,000 --> 00:05:58,000
And this is how the monk of the Suta starts.

30
00:05:58,000 --> 00:06:07,000
He will meet Suta, and he comes, and the young Hakova we had at the Jade the one, and then the King of the Gods are on me.

31
00:06:07,000 --> 00:06:18,000
Thus have I heard this, said by Ananda, Ananda was the secretary of the Lord Buddha, the caretaker.

32
00:06:18,000 --> 00:06:22,000
The Lord Buddha's personal attendant.

33
00:06:22,000 --> 00:06:40,000
Ananda said, thus have I heard, once the Lord Buddha was staying in Sawati, in the Park of Jita, the Forest Park of Jita, which was bought by Ananda to be the God.

34
00:06:40,000 --> 00:06:49,000
A great rich man who is a supporter of the Lord Buddha.

35
00:06:49,000 --> 00:06:59,000
At the Koh, Anitraadeh, at that time a certain angel, this is the angel who was asking the question.

36
00:06:59,000 --> 00:07:04,000
It's responsible for finding the answer from the Lord Buddha.

37
00:07:04,000 --> 00:07:14,000
He came down to Jita, and lit up with his brightness, lit up the whole of the Park.

38
00:07:14,000 --> 00:07:23,000
They respected the Lord Buddha, having paid respect to the Lord Buddha sat down to one side.

39
00:07:23,000 --> 00:07:30,000
Sat down respectfully, and asked the Lord Buddha the question.

40
00:07:30,000 --> 00:07:59,000
The Lord Buddha, the angels and men of all times, have wondered about the blessings that exist in the world.

41
00:07:59,000 --> 00:08:10,000
Please, for the benefit of the man and for the benefit of the angels, for the benefit of all beings,

42
00:08:10,000 --> 00:08:17,000
please tell us what are the real blessings in the world.

43
00:08:17,000 --> 00:08:32,000
And the Lord Buddha, without any hesitation, without any cause to think, to consider, without having to study or come up with a list of things,

44
00:08:32,000 --> 00:08:39,000
immediately responded with 38 true blessings which exist in the world.

45
00:08:39,000 --> 00:08:49,000
So, the first part of the day is Haseva Najapal, and Nang Pandita, Nang Taseva Najapu, and Nang

46
00:08:49,000 --> 00:09:08,000
Nang, Nang, Nang, Nang, Nang, Nang, Nang, Nang, Nang, Nang, Nang, Nang, Nang, Nang, Nang, Nang.

47
00:09:08,000 --> 00:09:20,000
Nang, Nang, Nang.

48
00:09:20,000 --> 00:09:27,000
Meaning, through the 10th Man, Nang, Nang, Nang, Nang, Nang, Nang, Nang, Nang, Nang.

49
00:09:27,000 --> 00:09:37,200
Phu-cha-chapu-chani, and paying respect to those who are worthy of respect.

50
00:09:37,200 --> 00:09:47,200
Kita-Manga-Lamutamung, this is a very real, a blessing in the highest sense, and so today

51
00:09:47,200 --> 00:09:48,200
the first three.

52
00:09:48,200 --> 00:10:00,400
I say when I tapala and not to associate or be around or be friends or even associates with foolish people.

53
00:10:00,400 --> 00:10:10,000
This is the first real, very real blessing which exists in the world.

54
00:10:10,000 --> 00:10:15,200
Not to associate with foolish people is a very real blessing.

55
00:10:15,200 --> 00:10:24,600
They say there's no, there's no happiness which can come from being friends or associates with foolish people.

56
00:10:24,600 --> 00:10:30,800
But they're all, it only leads to sorrow and suffering.

57
00:10:30,800 --> 00:10:37,000
First of all, foolish people have, what does it mean to be a foolish person or to have friends or foolish people?

58
00:10:37,000 --> 00:10:41,600
There are four characteristics of a foolish person.

59
00:10:41,600 --> 00:10:57,200
A person who is not worthy to be a friend, a person who is not a real friend, not someone we should keep as our friend.

60
00:10:57,200 --> 00:11:11,400
And they are friends with us only because of what they can get from us.

61
00:11:11,400 --> 00:11:35,200
To the only, they only have good things to say when asked to follow through with their words, they can't follow through with their words.

62
00:11:35,200 --> 00:11:48,800
It means that they can't keep what they say they're going to do.

63
00:11:48,800 --> 00:11:58,400
They say they're going to do many good things for us when in the end they don't do it.

64
00:11:58,400 --> 00:12:08,600
Number three, they are the only flatter.

65
00:12:08,600 --> 00:12:24,800
They say good things to our face, behind our back they're always, they will go ahead and say bad things.

66
00:12:24,800 --> 00:12:45,600
Say bad things about us.

67
00:12:45,600 --> 00:13:09,800
Number four, they lead the, they lead the friend into evil ways.

68
00:13:09,800 --> 00:13:24,800
These are the four characteristics of a, of a four.

69
00:13:24,800 --> 00:13:39,200
A foolish person, a foolish person who associates with friends only for what they can gain themselves.

70
00:13:39,200 --> 00:13:53,600
Is a, a fool is someone not worth what worth taking as a friend.

71
00:13:53,600 --> 00:13:59,400
They give a little bit and they, they expect a lot.

72
00:13:59,400 --> 00:14:08,000
They associate with the friend thinking what can I get, what can I get from being friends with this person?

73
00:14:08,000 --> 00:14:13,000
The person who is good, good only by speech, but not by deed.

74
00:14:13,000 --> 00:14:19,200
They say things, they, they talk about things they've done for you in the past.

75
00:14:19,200 --> 00:14:28,800
They talk about things they've, they're going to do for you in the future, but in the present, they never really do anything for you at all.

76
00:14:28,800 --> 00:14:33,000
You can't believe them what they say, they say they're going to do this, they say they're going to that.

77
00:14:33,000 --> 00:14:46,000
In the end, they never really do it.

78
00:14:46,000 --> 00:14:53,000
The person who, who is a flatter, who flatters.

79
00:14:53,000 --> 00:14:55,000
A flatter is very dangerous.

80
00:14:55,000 --> 00:14:59,000
You think, you might think someone who speaks well of you all the time is a good thing.

81
00:14:59,000 --> 00:15:05,000
It's very dangerous. First of all, it can lead us to feel conceited about ourselves.

82
00:15:05,000 --> 00:15:10,000
But the worst thing is for a flatter, when you do evil deeds, they, they praise you.

83
00:15:10,000 --> 00:15:12,000
They say, yes, good, keep going.

84
00:15:12,000 --> 00:15:19,000
When you do good deeds, of course, they praise you, but when you do bad deeds, they don't stop you.

85
00:15:19,000 --> 00:15:27,000
They go ahead and do it with you, they go ahead and say good about it.

86
00:15:27,000 --> 00:15:37,000
Speak good about your bad deeds as well.

87
00:15:37,000 --> 00:15:45,000
And the person who, the fourth kind is a person who leads you in a, in a bad direction.

88
00:15:45,000 --> 00:15:51,000
So before you even think to do bad deeds, this is a person who leads you to drink alcohol,

89
00:15:51,000 --> 00:15:59,000
who leads you to go partying at night, who leads you to, to gamble, who leads you to,

90
00:15:59,000 --> 00:16:07,000
to waste your money, to waste your time, to waste your energy.

91
00:16:07,000 --> 00:16:16,000
The lead you into danger, and, and you're into despair.

92
00:16:16,000 --> 00:16:20,000
The person who would never think to bring you to come to practice meditation.

93
00:16:20,000 --> 00:16:31,000
These are people who can really create a problem, a problem for us in our life.

94
00:16:31,000 --> 00:16:42,000
So the first thing the Lord Buddha said is for, to be truly blessed, if you want to know what is really going to bring goodness in the future.

95
00:16:42,000 --> 00:16:47,000
The first thing is to, to avoid all foolish people.

96
00:16:47,000 --> 00:16:53,000
People who don't waste good for you, who don't want you to be happy.

97
00:16:53,000 --> 00:17:00,000
People who, who don't care, who don't really care that you find peace and happiness.

98
00:17:00,000 --> 00:17:10,000
They only want to gain from themselves or, or maybe they, they don't even want, gain from themselves.

99
00:17:10,000 --> 00:17:14,000
They don't know what is, gain and what is lost, and they lead you in bad direction.

100
00:17:14,000 --> 00:17:19,000
They lead you to suffering.

101
00:17:19,000 --> 00:17:27,000
So the first, so the first blessing is the, staying away from foolish people.

102
00:17:27,000 --> 00:17:35,000
The second blessing is the exact opposite association with wise people, to take wise people as one's friend.

103
00:17:35,000 --> 00:17:43,000
Wise and good friends have for, also have for characteristics.

104
00:17:43,000 --> 00:17:50,000
One is someone who is a support for you.

105
00:17:50,000 --> 00:17:59,000
Two is a person who is a friend in, in a friend in good times and a friend in bad times.

106
00:17:59,000 --> 00:18:06,000
Three is a friend who, who can teach you, who can lead you in the right direction.

107
00:18:06,000 --> 00:18:20,000
And four is a friend who truly, truly loves you, who is true, true love and friendship and respect for you.

108
00:18:20,000 --> 00:18:30,000
The friend who is a support, this is a blessing because they can guard you in times when you are negligent.

109
00:18:30,000 --> 00:18:44,000
They can guard your possessions.

110
00:18:44,000 --> 00:18:51,000
When there is danger, they can, they can be a refuge for you and you have problems when you have difficulties.

111
00:18:51,000 --> 00:18:58,000
They will always support you and even give, give more support than, than you might expect.

112
00:18:58,000 --> 00:19:05,000
More support than might be expected of them.

113
00:19:05,000 --> 00:19:13,000
A person who is a real support is someone who, when, when times of danger come that, that's the person who,

114
00:19:13,000 --> 00:19:27,000
who helps you, who stays with you, who does whatever they can to, to keep you free from suffering.

115
00:19:27,000 --> 00:19:39,000
A friend who is a friend in, in suffering and a friend in happiness.

116
00:19:39,000 --> 00:19:53,000
This is someone who doesn't, is someone who doesn't abandon you in times of danger.

117
00:19:53,000 --> 00:20:00,000
Whatever problems you have, they will support you in.

118
00:20:00,000 --> 00:20:07,000
They will never run away from you and never, never abandon you in times of difficulty.

119
00:20:07,000 --> 00:20:18,000
The friend who, the friend who can lead you in the right direction, this is the most important friend

120
00:20:18,000 --> 00:20:23,000
of all the four types of, of good friend, of real friend.

121
00:20:23,000 --> 00:20:27,000
The friend who can lead you in, lead a, lead a friend in right, in the right direction.

122
00:20:27,000 --> 00:20:38,000
This is called the Kali Anamita, a beautiful friend or a true friend or a good friend.

123
00:20:38,000 --> 00:20:45,000
Why? Because all of the bad things which you might do are all of the suffering which you might fall into,

124
00:20:45,000 --> 00:20:56,000
comes from the, from the, from the things which stay in our hearts which lead us to do bad things to say bad things.

125
00:20:56,000 --> 00:21:08,000
It comes from the anger and the greed and the delusion which exists in our heart.

126
00:21:08,000 --> 00:21:13,000
Until we find someone who can point these things out and point out those suffering and the cause of suffering,

127
00:21:13,000 --> 00:21:22,000
and lead us out of suffering, lead us to freedom from suffering.

128
00:21:22,000 --> 00:21:34,000
Unless we have someone who can lead us out of suffering, we have no way to,

129
00:21:34,000 --> 00:21:48,000
we have no real hope for, in the, in the holy life and the spiritual life.

130
00:21:48,000 --> 00:21:58,000
We have no safety or security from pain and suffering and loss.

131
00:21:58,000 --> 00:22:04,000
Without a friend it teaches that to show us the way, to show us the way out of suffering.

132
00:22:04,000 --> 00:22:14,000
It's not likely that we by ourselves are going to really find the true way out of suffering.

133
00:22:14,000 --> 00:22:22,000
Before we come to practice meditation, if we didn't have a meditation,

134
00:22:22,000 --> 00:22:28,000
people to guide us in meditation or to show us how to practice meditation.

135
00:22:28,000 --> 00:22:32,000
It's not likely that we would be able to find for ourselves through the way to be,

136
00:22:32,000 --> 00:22:38,000
to be fast in the meditation, to make our minds pure.

137
00:22:38,000 --> 00:22:42,000
We may not have even thought that there was a way out.

138
00:22:42,000 --> 00:22:46,000
We might not have thought it was necessary to find such a friend.

139
00:22:46,000 --> 00:22:59,000
Most of our friends are good as far as bringing pleasure and happiness in the world, in a worldly sense.

140
00:22:59,000 --> 00:23:06,000
But normally most of our friends are not very good at bringing happiness inside,

141
00:23:06,000 --> 00:23:13,000
bringing peace inside, because they also don't know the way out of suffering.

142
00:23:13,000 --> 00:23:17,000
So this is a most important friend and Buddhism.

143
00:23:17,000 --> 00:23:26,000
It's the most important thing in living the spiritual life is to have a good friend.

144
00:23:26,000 --> 00:23:29,000
A good friend who can lead you in the right direction.

145
00:23:29,000 --> 00:23:34,000
They can lead you to heaven, they can lead you to nibana.

146
00:23:34,000 --> 00:23:37,000
They tell you what is of use.

147
00:23:37,000 --> 00:23:42,000
They stop you from doing things which are useless, which are harmful.

148
00:23:42,000 --> 00:23:45,000
And so on.

149
00:23:45,000 --> 00:23:50,000
And the fourth kind of friend is someone who truly loves and cares for you.

150
00:23:50,000 --> 00:23:58,000
The person who truly loves you when you're in suffering, they also feel you this suffering.

151
00:23:58,000 --> 00:24:06,000
When you are happy, they are very happy as well.

152
00:24:06,000 --> 00:24:14,000
You will never talk badly about you.

153
00:24:14,000 --> 00:24:17,000
When people say bad things about you, they stop them.

154
00:24:17,000 --> 00:24:22,000
When people say good things about you, they...

155
00:24:22,000 --> 00:24:27,000
To be a real friend, a real friend who has real love for their friends.

156
00:24:27,000 --> 00:24:32,000
It's a person who...

157
00:24:32,000 --> 00:24:37,000
Only agrees when people say good things about the friend.

158
00:24:37,000 --> 00:24:43,000
When people say bad things, they will stop them right away.

159
00:24:43,000 --> 00:24:47,000
They say the characteristics in brief of a good...

160
00:24:47,000 --> 00:24:50,000
They say the brief characteristics of a good friend.

161
00:24:50,000 --> 00:24:55,000
Someone who is worth associating with.

162
00:24:55,000 --> 00:25:03,000
Someone who knows good from bad, someone who is...

163
00:25:03,000 --> 00:25:07,000
When associating with that person, this would be a real blessing.

164
00:25:07,000 --> 00:25:15,000
When we associate with good people like this, it's a real blessing for our lives.

165
00:25:15,000 --> 00:25:20,000
And the third blessing is Poochach, a Poochandhyanam.

166
00:25:20,000 --> 00:25:32,000
Being homage to those who are worthy of homage.

167
00:25:32,000 --> 00:25:43,000
homage is something which is also very important in...

168
00:25:43,000 --> 00:25:52,000
In the culture that the Lord Buddha grew up in and practiced in and taught in.

169
00:25:52,000 --> 00:26:01,000
In the time of the Lord Buddha, they would practice homage or Poochach.

170
00:26:01,000 --> 00:26:11,000
But they would pay homage to things like trees or rivers or the sky or the earth,

171
00:26:11,000 --> 00:26:20,000
fire, they paid respect to many different things.

172
00:26:20,000 --> 00:26:33,000
They respected God, they respected angels, they respected many different things.

173
00:26:33,000 --> 00:26:38,000
There was one man in the time of the Buddha who paid respect to the six directions.

174
00:26:38,000 --> 00:26:46,000
His father was dying, he said to his son, he must pay respect, pay homage to the six directions.

175
00:26:46,000 --> 00:26:51,000
Every morning he must pay homage to the six directions.

176
00:26:51,000 --> 00:27:02,000
So every morning when after his father died, his son would go down to the river and bathe in the river,

177
00:27:02,000 --> 00:27:12,000
and go around cross-strating to the six directions.

178
00:27:12,000 --> 00:27:20,000
These directions are north, south, east, west, up and down.

179
00:27:20,000 --> 00:27:29,000
And the Buddha one day saw this man cross-strating to the six directions.

180
00:27:29,000 --> 00:27:36,000
Acting like a...

181
00:27:36,000 --> 00:27:44,000
Acting like a crazy person.

182
00:27:44,000 --> 00:27:52,000
And said to him, asked him, what is it you're doing and the young man explained

183
00:27:52,000 --> 00:27:58,000
that his father before his father passed away and instructed him to pay respect to this

184
00:27:58,000 --> 00:28:04,000
action?

185
00:28:04,000 --> 00:28:09,000
And the Buddha responded to him and said, this is not the true way to pay respect to the six directions.

186
00:28:09,000 --> 00:28:12,000
The pay homage to the six directions.

187
00:28:12,000 --> 00:28:19,000
These are not the six directions.

188
00:28:19,000 --> 00:28:23,000
To which it is necessary to pay homage.

189
00:28:23,000 --> 00:28:30,000
And he proceeded to explain to the young man what were the six directions that his father

190
00:28:30,000 --> 00:28:39,000
surely meant.

191
00:28:39,000 --> 00:28:47,000
The six directions, the north, is one's parents.

192
00:28:47,000 --> 00:28:54,000
And the east is one's teachers.

193
00:28:54,000 --> 00:29:01,000
The south is one's... one's spouse and one's children.

194
00:29:01,000 --> 00:29:05,000
The west is one's friends.

195
00:29:05,000 --> 00:29:06,000
Up is one.

196
00:29:06,000 --> 00:29:13,000
Up is the religious, the religious people.

197
00:29:13,000 --> 00:29:24,000
The seminars and the Brahmanas, the priests and the recklessness, the ascetics and so on.

198
00:29:24,000 --> 00:29:30,000
And down is one's servants or workers, one's employees.

199
00:29:30,000 --> 00:29:36,000
And the Lord Buddha said, these are the six directions which one has to pay homage to.

200
00:29:36,000 --> 00:29:43,000
These one has to attend, one has to pay respect to, and has to respect these six directions.

201
00:29:43,000 --> 00:29:46,000
One's parents, one has to pay homage.

202
00:29:46,000 --> 00:29:52,000
These are... one's parents, one's parents are most worthy of paying homage to.

203
00:29:52,000 --> 00:29:55,000
Our parents gave birth to us.

204
00:29:55,000 --> 00:30:01,000
Before giving birth, our mother carried us around for nine months, ten months in the womb.

205
00:30:01,000 --> 00:30:06,000
And free of charge, like a hotel.

206
00:30:06,000 --> 00:30:14,000
And then carried for us for many years, brought us up, kept us free from suffering.

207
00:30:14,000 --> 00:30:18,000
So in return, we take care of them as best we can.

208
00:30:18,000 --> 00:30:26,000
We carry on their lineage and so on.

209
00:30:26,000 --> 00:30:31,000
Our teachers also are worthy of respect, worthy of homage, because of what they've taught us,

210
00:30:31,000 --> 00:30:34,000
even in public school and high school, whatever they've given to us,

211
00:30:34,000 --> 00:30:39,000
they've given us something wishing for us, wishing goodness for us.

212
00:30:39,000 --> 00:30:44,000
When we learn from teachers, we should always be respectful.

213
00:30:44,000 --> 00:30:51,000
It's a very difficult thing to teach.

214
00:30:51,000 --> 00:31:02,000
Our spouse and our children, these are people who make our house life, make our house possible.

215
00:31:02,000 --> 00:31:07,000
When we live in the world, so we have a family or so on.

216
00:31:07,000 --> 00:31:12,000
When we live in the monastery, we just think of everything of the whole group,

217
00:31:12,000 --> 00:31:17,000
the whole monastery is our big family.

218
00:31:17,000 --> 00:31:21,000
So whether the Lord Buddha was giving an instruction to a lay person,

219
00:31:21,000 --> 00:31:25,000
we can also take it as in a monastic setting.

220
00:31:25,000 --> 00:31:36,000
We are all brothers and sisters and mothers and fathers and sons and daughters.

221
00:31:36,000 --> 00:31:43,000
We become a very large family.

222
00:31:43,000 --> 00:31:54,000
We have respect for each other, but they're good things that we bring to each other.

223
00:31:54,000 --> 00:32:02,000
Lord Buddha said it's important to respect one's family.

224
00:32:02,000 --> 00:32:12,000
When children, one should teach them a way out of evil and teach them to do good things and so on.

225
00:32:12,000 --> 00:32:17,000
And one's friends is important to respect us.

226
00:32:17,000 --> 00:32:20,000
They said it's a great blessing to have good friends.

227
00:32:20,000 --> 00:32:34,000
A person who has no friends can be said to be truly, truly poor, truly destitute.

228
00:32:34,000 --> 00:32:37,000
There's no friends who are who has no good friends.

229
00:32:37,000 --> 00:32:50,000
When we said to be a true person, a person who is truly bereft of wealth, truly has no wealth.

230
00:32:50,000 --> 00:33:04,000
We have a respect to our friends and supporting them with good, with nice, pleasant speech towards our friends.

231
00:33:04,000 --> 00:33:17,000
Doing things which are of service to them, being generous with them and so on.

232
00:33:17,000 --> 00:33:19,000
And religious people are worthy of respect.

233
00:33:19,000 --> 00:33:32,000
People like the Lord Buddha, people like the monks and the nuns and people who have taken on religious vows to become completely free from suffering.

234
00:33:32,000 --> 00:33:38,000
Because there are the people who can teach us the way out of suffering in turn.

235
00:33:38,000 --> 00:33:43,000
Once they have found the way out of suffering and they can in turn teach us the way out of suffering.

236
00:33:43,000 --> 00:33:50,000
We bear respect with speech, with actions, with even with thoughts of good will.

237
00:33:50,000 --> 00:33:58,000
The thoughts of loving kindness towards them.

238
00:33:58,000 --> 00:34:08,000
And even our employees, even the people below us are the people who serve us or who work for us.

239
00:34:08,000 --> 00:34:27,000
Maybe our students or maybe the people who are under our care, we have to respect them as well, giving them work and so on according to their abilities.

240
00:34:27,000 --> 00:34:31,000
This, the Lord Buddha said, is how we pay respect to this extra action.

241
00:34:31,000 --> 00:34:33,000
So we pay homage to this extra action.

242
00:34:33,000 --> 00:34:56,000
The Buddha said, we pay homage to the Buddha, the Lord Buddha said, we pay homage to the Buddha.

243
00:34:56,000 --> 00:35:04,000
The way to pay homage is to practice the teachings of the Lord Buddha.

244
00:35:04,000 --> 00:35:21,000
If you want to pay homage to someone, if you want to show your respect for that person, the best way to do so is to follow their teachings and practice according to their teachings.

245
00:35:21,000 --> 00:35:30,000
When the Lord Buddha was passing away, he said to ananda, he said, we have to pay homage to the Buddha.

246
00:35:30,000 --> 00:35:36,000
The Lord Buddha said, we have to pay homage to the Buddha.

247
00:35:36,000 --> 00:35:48,000
The Lord Buddha said, we have to pay homage to the Buddha.

248
00:35:48,000 --> 00:35:54,000
In the time, the Lord Buddha was passing away all the angels and all the men and women and beings in the world.

249
00:35:54,000 --> 00:36:02,000
They brought flowers and candles and incense to pay respect to the Lord Buddha before He passed away.

250
00:36:02,000 --> 00:36:09,000
And the Lord Buddha said, this is not the way one should really pay respect to the fully enlightened Buddha.

251
00:36:09,000 --> 00:36:11,000
This is not the best way to pay respect.

252
00:36:11,000 --> 00:36:19,000
The highest way to pay respect is whatever monk or nun or layman or laywoman.

253
00:36:19,000 --> 00:36:21,000
Practice is the teaching of the Lord Buddha.

254
00:36:21,000 --> 00:36:26,000
Practice is according to the, in line with the teaching.

255
00:36:26,000 --> 00:36:31,000
Practice is perfectly, practice is correctly.

256
00:36:31,000 --> 00:36:40,000
This person, whoever they may be, someone who pays homage to the Lord Buddha, pays respect to the Lord Buddha.

257
00:36:40,000 --> 00:36:46,000
Worship, pays worship to the Lord Buddha with the highest sort of, of homage.

258
00:36:46,000 --> 00:36:55,000
The highest sort of respect, the highest sort of worship.

259
00:36:55,000 --> 00:36:58,000
So we practice according to the four foundations of mindfulness.

260
00:36:58,000 --> 00:37:06,000
This is the greatest blessing of being homage to the teachings which will lead us out of suffering.

261
00:37:06,000 --> 00:37:11,000
Being homage to the teacher, who teaches us the way out of suffering.

262
00:37:11,000 --> 00:37:14,000
This is the way to pay homage to the Lord Buddha.

263
00:37:14,000 --> 00:37:22,000
As I said to be, blessing in the highest sense, blessing because it leads us out of suffering.

264
00:37:22,000 --> 00:37:31,000
When we practice the teachings which are worthy of practice, we pay homage to the teachings which are worthy of being homage to.

265
00:37:31,000 --> 00:37:40,000
And so this is the start of the mongla supta.

266
00:37:40,000 --> 00:37:44,000
We'll leave it there today and now we continue to practice.

267
00:37:44,000 --> 00:38:09,000
We do mindful frustration and then walking in the mongla supta.

