WEBVTT

00:00.000 --> 00:05.000
The next question comes from Luke Ulelees.

00:05.000 --> 00:10.000
Some Buddhist centers operate on a business model setting,

00:10.000 --> 00:15.000
setting strict financial requirements to attend dhammataks and or retreat.

00:15.000 --> 00:18.000
Others work on a model of donation only,

00:18.000 --> 00:20.000
not even making a suggested donation known.

00:20.000 --> 00:23.000
What are your ideas on this?

00:23.000 --> 00:33.000
It's a big debate in Buddhist circles as to how to organize retreats,

00:33.000 --> 00:39.000
dhammataks and so on because obviously their expenses involved.

00:39.000 --> 00:43.000
I would say theoretically it's just a model.

00:43.000 --> 00:46.000
The expenses have to be covered by the group.

00:46.000 --> 00:53.000
And theoretically if you could have a group of people

00:53.000 --> 01:02.000
all chipping in to get something going into to run the center,

01:02.000 --> 01:07.000
then setting a specific cost or membership wouldn't really be a problem.

01:07.000 --> 01:18.000
I think in practice and the way it carries out,

01:18.000 --> 01:24.000
the feeling that it brings is one of business model

01:24.000 --> 01:31.000
and a feeling of sort of give and take rather than

01:31.000 --> 01:36.000
giving and expecting something as opposed to giving.

01:36.000 --> 01:43.000
Rather than say that it's wrong to charge and it's wrong to set up these fees,

01:43.000 --> 01:50.000
I would say there are obvious advantages to doing it the other way of offering things for free.

01:50.000 --> 01:55.000
Yeah, you feel a lot better. It's actually a gift.

01:55.000 --> 02:05.000
It feels like a gift and it brings about a state of genuine gratitude

02:05.000 --> 02:11.000
in the students, in the people who receive the teachings that they take it seriously

02:11.000 --> 02:14.000
and they take you seriously.

02:14.000 --> 02:22.000
And they're able to respect the sacrifice that you're making.

02:22.000 --> 02:27.000
There is an argument that people are so used to the business model

02:27.000 --> 02:34.000
that I think that anything is free is unprofessional

02:34.000 --> 02:38.000
and that's an interesting point.

02:38.000 --> 02:41.000
So there's a debate that goes back and forth.

02:41.000 --> 02:45.000
I'm not really so concerned about,

02:45.000 --> 02:49.000
there's this idea that no one will come if you don't charge for things,

02:49.000 --> 02:55.000
which is really an interesting idea and apparently it holds true in some situations.

02:55.000 --> 02:59.000
But to me points to a sort of a perversion in people's thinking

02:59.000 --> 03:05.000
and rather than any sort of virtue in that model

03:05.000 --> 03:11.000
and having worked in the model of offering things totally free

03:11.000 --> 03:16.000
even when that sometimes means hardship and difficulty.

03:16.000 --> 03:23.000
I know the great feeling of happiness and

03:23.000 --> 03:29.000
I'm glad I'm a self-assurance in being able to offer things for free

03:29.000 --> 03:33.000
when you teach without expecting anything in return

03:33.000 --> 03:39.000
when you give without expecting anything in return

03:39.000 --> 03:44.000
or without being told to when it comes from the heart

03:44.000 --> 03:50.000
and when there's no coercion or insinuation.

03:50.000 --> 03:55.000
And the other thing I would say is

03:55.000 --> 04:03.000
that the model of offering things for free

04:03.000 --> 04:06.000
is a very pure way to live.

04:06.000 --> 04:10.000
It's a part and parcel of the monastic life of being a monk where

04:10.000 --> 04:14.000
you have no attachments. You're not working for people.

04:14.000 --> 04:22.000
The fact that monks teach doesn't mean that they are bound to help other people

04:22.000 --> 04:26.000
and this is a mistake sometimes people make that somehow we have a duty to help

04:26.000 --> 04:29.000
and the truth is we don't.

04:29.000 --> 04:32.000
I could leave tomorrow I could stop doing YouTube videos tomorrow

04:32.000 --> 04:38.000
or stop teaching my students here in Los Angeles tomorrow and so on.

04:38.000 --> 04:41.000
I'm afraid to do that. I don't have any contract

04:41.000 --> 04:45.000
or any requirement from them.

04:45.000 --> 04:51.000
So I'm not trapped into receiving some kind of support from people.

04:51.000 --> 04:57.000
Being totally free and the offering is genuine from the heart

04:57.000 --> 05:01.000
but non-binding to both parties.

05:01.000 --> 05:06.000
So there's no requirement for me if you want to come and practice and leave tomorrow.

05:06.000 --> 05:11.000
That's fine. When you're hungry you come to eat.

05:11.000 --> 05:19.000
So I would just say that it's a much more

05:19.000 --> 05:28.000
pure and perfect system to offer things for free

05:28.000 --> 05:34.000
and there are many disadvantages to offering courses

05:34.000 --> 05:38.000
based on the business model and charging money and so on.

05:38.000 --> 05:41.000
Some of them should be glaringly obvious.

05:41.000 --> 05:46.000
Say some of them are not so obvious and you'd have to hang out at such centers to see them

05:46.000 --> 05:50.000
and you'll see that there's often a great amount of corruption

05:50.000 --> 05:56.000
and insincerity because of the involvement of money

05:56.000 --> 06:02.000
and the involvement of the reception of materials and so on.

06:02.000 --> 06:06.000
So that's my opinion. There are many opinions on this subject.

06:06.000 --> 06:10.000
Having lived the pure model, the free model.

06:10.000 --> 06:14.000
I can say that there's no doubt in my mind which one is superior.

06:14.000 --> 06:36.000
So there we go.

