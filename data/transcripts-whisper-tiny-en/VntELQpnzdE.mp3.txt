Good evening everyone, welcome to our live broadcast evening done my session.
So I'm just going, still going over my stocks that I have to give this weekend, thought
it would be nice to go over a little more of it with you, be my test audience.
So I'm going to give this talk on wrong mindfulness or wrong practice.
And it's a somewhat difficult talk to get right because mindfulness is always good
and, you know, we would have said Satin Chakwang, Bhikhavaya, Sabatikha, and Wandaan, mindfulness
is always useful.
It's the only quality if you take the seven bold junghas, seven factors of enlightenment,
some of them are only useful when you're agitated to calm you down, some are only useful
when you're lazy or overly concentrated to perk you up.
But mindfulness is always useful, but it's it.
And the idea that mindfulness practice could lead to problems, could be dangerous.
Because especially, especially strange because mindfulness stops problems, that's what
it's for.
That's what it does.
It stops, papan chat, stops us from making more out of things than they actually are.
We would have said yani, sotani, low-customing, sati, taisang, niwarayan.
Whatever streams there are in the world.
Nobody used this word, stream, it's quite interesting.
Whatever channels there are for problems basically, for stress, for suffering, for
mental illness, if you will.
Mindfulness stops them, mindfulness prevents them.
Mindfulness stops all problems is basically it because mindfulness is about seeing things
just as they are, it's about grasping experience for what it is.
It's not getting lost in what we want it to be or what we think it is.
What we think of it, right, wrong, good, bad, me, mine, all of those have no place in mindfulness.
So how is it that people practice meditation and have problems?
I'm happy to say, I mean maybe, verging on a little bit proud to say that I've never
had a meditator go crazy on me, but I have seen meditators go crazy.
I spent three nights in a mental hospital before I was a monk with a crazy meditator.
Who had gone off the reservation, said, no, off the, what's the word, I'm gone crazy.
And it was quite an experience.
So how does it happen?
And my last talk, I already talked about these, we'll go over them again.
On your unmindful, your mindfulness is misdirected.
Three, your mindfulness is lapsed.
And four, your mindfulness is impotent or ineffectual.
It's the only ways I can see that mindfulness could be, could go wrong.
The first to be unmindful goes without saying that if you don't have a practice mindfulness,
it can't possibly help you, but there are people who go to meditation centers and whatever
reason don't actually practice mindfulness.
I remember once I had one, it's my most shameful experience as a teacher, I wasn't
going to teach her that long, so there was this young Thai man, and I said to him, okay,
so I start off with this many minutes walking, this many minutes sitting, you know, start
off, we start off with like 10 of walking and 10 of sitting, just do them together.
And he came back the next day and everything's fine.
The next day and the next day and it was about six, seven days, nothing.
No pain, no stress, didn't have any challenges, like he wasn't dealing with anything.
And so I asked him, how many rounds are you doing a day?
He said one.
I told him to do 10 minutes walking, 10 minutes sitting, that's what he did.
I said, what did you do for the rest of the day?
I said around the red, red comics or something, yeah, I didn't know what to do.
He'd gone through half the course by this time, so I sent him off to one of the senior
teachers and said, yeah, I'm making their problem.
Well, I felt kind of bad, it was, it was personally my fault for not being cleared.
So in 10 minutes, 10 minutes is per session and we don't, of course, time, we aren't
exact, we aren't, of course, looking for a quote of hours a day or something like that because
the other thing, of course, you can walk and sit for hours and get no benefit if you're
not mindful.
If anyone tells you how long they've been meditating, it's interesting information,
but it's not all that useful because they haven't been meditating for 10 years or 20
years.
I don't know how many minutes, how many moments they've been mindful for, that's much
more interesting.
So if you're on mindful, of course, there's not much we can say, you certainly can't blame
the meditation practice, but living in an environment like this without being mindful,
that could very easily drive you crazy.
The second is misdirected mindfulness.
This is, I think potentially, where a lot of the problems come from, because if you're mindful
of concepts, there is a lot more danger.
I would go on a limb and say, summit to meditation, which focuses on concept, it's much
more dangerous than we pass in the meditation, and I would bet there are statistics you
could find that you could accumulate statistics to show that.
Because reality is quite limited, I mean, isn't this, it can be quite boring at times.
Reality turns out to be seeing, hearing, smelling, tasting, feeling, thinking, where's
the fun in that?
I mean, if you get into it, it's quite exciting, but there's none of the endless possibilities
of tranquility meditation.
What would you rather do?
Remember your past dives or sit here saying rising, falling, rising, falling, right?
Or again, magical powers, read people's minds, all these things, that would be more fun.
The world of concepts is infinite.
You can imagine a hair with horn, a rabbit with horns.
You can imagine a rabbit with antlers.
You can imagine a pink rabbit with antlers or a blue or a yellow, right?
Because concepts are infinite, you can easily get lost very easily without strict guidance
and regulation.
When you're mindful of concepts, and by mindful here, again, we just mean when you grasp
the concept, it's possible to get caught up in the concept, and I would still argue that
mindfulness isn't to blame here.
Because in Samatai, you still need mindfulness, and if you have mindfulness, it can't
possibly go wrong, because you grasp the object.
For example, a candle flame, say an easy example, you focus on the candle flame, and if
you're mindful of it, then you know that this is fire, and you say fire, fire, fire,
and you're grasping the concept of fire, well, that mindfulness is a good thing.
That's what keeps you from seeing it as more or different, but the thing about concepts
is that they can become, because they're in your mind, eventually you get the picture
of the fire in your mind, they can more and become different things.
You're mindful of the past, mindful of the future.
Much more likely, and why this is really considered wrong, we talked about this, is that
it's just not the way to enlightenment, there's nothing wrong with it, in fact, it can be
actually indirectly helpful, because it gained great concentration.
Much more common, and you do see this quite a bit, are people, meditators who've been
practicing for years, and have a very strong meditation practice, but I've never attained
Nibana, I've never realized cessation of suffering, because they have pleasant experiences,
because they're focusing on concepts, and really clear on reality.
The third lapsed mindfulness, lapsed mindfulness I think is where real danger for one's mental
health is, and this you see, because when you meditate, it brings up, this is where meditation
is potentially quite dangerous, simply because it brings up deep dark emotions that can
be dangerous, both, I mean, I want to scare you, and most people, this isn't a problem,
but it's important to go slow, and it's important, in some cases, it's really important
to have a teacher, most people don't have really crazy things inside, but it can happen
that things come up that you're not easily able to deal with, one of the great things
of watching an experienced teacher in action, my teacher, there was one time where it
stands out, I remember, sitting up in his kutti, waiting for meditators, and suddenly
one of the nuns who worked in the office says, couple runs in and says, there's a meditator
outside of the office, and she's going crazy, she's saying nonsense and running around
like a crazy person, and he goes, tell her to be mindful, or no, he just said, be mindful,
be mindful, because this nun herself was quite unmindful, and the way he just sat there
and said, yeah, yeah, just be mindful, like we're all jumping up and getting ready to go
and save the day, because things come up and you react to them.
You get disturbed by them, you get taken off guard, it happens with both positive and negative
situations, with positive conditions, you get lost in them, hey, that's interesting,
that's nice, and because you stop being mindful, and both because you stop being mindful
and because of the, they're so strong, and they're so powerful, they can really suck
you in negative as well, if you have negative things from the most of us have something
negative from the past, and it sucks you in, it comes as a result, you know, it shows
itself as a result of the practice, so that's a good thing, both of them are really good,
I mean, it's good that positive things come out, but negative things come out, it's good
that the deep stuff comes out, because it's, it means you're becoming more in tune with
yourself and more in tune with reality, and you're getting a deeper experience of who you are,
but without a, a stabilizing presence, someone to remind you or without reminding yourself
that, hey, it's still just an experience, it can be dangerous, there were these in the,
in the texts, in the Senutany guy, I think, there are these monks who would have taught
them mindfulness of, I think it was mindfulness of the load, soonness of the body, I can't
remember what it was now, but they taught them something, and they all killed themselves
as a result, but they had someone killed them, they hired some guy to kill them, because
they were so revolted by, it was just such a bizarre story, like they reacted so violently
to this, commentary says they had really bad karma, they had been hunters in past lives,
but the point is that things can come out, and if you're not, if you're not ready for
the truth, you can't handle the truth, and it can be potentially problematic, so not
to be clear that mine, this is one way that mindfulness practice is, I've never seen it
happen with any of my students, but mindfulness meditation is potentially dangerous.
There was one, one meditator that, this meditator that I spent time in the mental hospital
with, I was in her teacher, but the problem was she had, I think, too much instruction
from wrong people, there were some people who weren't her teachers going in and going
into her room and talking to her and feeding her all sorts of weird ideas, like there's
a lot of superstition and folk Buddhism and telling her things about past lives and karma,
and really getting her lost and winding her up, I think, because suddenly she's out
running around and she was using the mantras, but she was saying, she was using them
to wind herself up, so she would say to herself wisdom, wisdom, wisdom, I remember that
was one, like she would just repeat a mantra to her, but it would wind her up instead
of calmer down, because it wasn't based on any experience.
This is an example, people get the mantra wrong, instead of using it to note what's there,
they use it to note what they want to be there, and that'll wind you up.
It's quite easy to get lost if you're not clear about the practice, and my mindfulness
is just the reason I think you would, you don't see people going crazy practicing mindfulness
is because it's so, well, just because it's such a calming influence, mindfulness itself
stops all that anyway, so the lapse to mindfulness, it happens with good experiences,
it happens with bad experiences, if you're not careful to catch it and keep going.
At least at the least you'll get stuck, at the worst you can get caught up and go
temporarily AWOL, and the fourth is, when I talked about impotent mindfulness, or probably
what the Buddha would have explained as wrong mindfulness, and that is wrong mindfulness
that is blocked by something.
So one common example is with those people who have taken the body sat for vows, they've
taken a vow to become a Buddha, they've taken a vow to not become enlightened, that's
basically what it says, I vow not to enter into Nibbana until all beings are ready to become
enlightened, and that prevents them, of course, from, I mean it actually does, this is not
a theoretical thing, we've had meditators, I had one monk, another guy I took him to the
mental hospital, I remember that night, he tried to kill himself on several occasions,
because he was, on one hand, trying to become a Buddha, and on the other hand, trying
to practice insight meditation to become an Aran, and he had other issues, it was probably
fairly specific to him, but he didn't eventually drive himself greatly, but a more common
one, I mean people who have a very strong belief in self, or belief in God, or that kind
of thing, this prevents you from letting go and experiencing things objectively, if you have
a sense that everything's going to work out, or God has a plan for us, or something like
that, you can very much get in the way, I mean this is wrong view, according to Buddhism,
and I went through them, you know, if you have a wrong thought, if you're wishing ill of
other people, and it obstructs your path, or if you have lots of desires or ambitions,
if you're arrogant to that kind, I think these will get in the way, if you have a wrong
speech, if you're saying nasty things about people, if you're a liar, or a gossip, or that
kind of thing, if you just talk too much, you know, that'll get in your way, wrong action,
if you're a thief, a murderer, an adulterer, an adulterer, if you have wrong livelihood,
if you make money through killing or stealing, that kind of thing, if you have wrong
effort, if you're lazy, or if you just put your effort out to become to do bad things,
if you have wrong concentration, if you're unfocused, or if you're focused on the wrong
things, mindfulness needs all of these other qualities, and most especially things like
right view and right action, I think if you don't have morality and wisdom, concentration
aspect with mindfulness and so on, can't develop, so for most people the consequence of
wrong mindfulness is simply they'll stop practicing, they'll be discouraged and they'll
feel like they'll reach an impasse where they can get past people practice, whether
they practice properly or improperly, without a solid training in mindfulness, a good
teacher and a good mind that is able to receive it, it's quite common for people to just
give it up, they get afraid or they get discouraged or they get disinterested because
they don't get it, but as I said, another consequence is that they continue practicing
but don't get anywhere from practice and they just feel peaceful or calm, that kind
of thing, thought of letting go without ever becoming free from ego or desire, that kind
of thing, and in extreme cases the consequences that people will crazy, the crazy is usually
temporary, I don't think I've ever heard of meditation driving you crazy for the rest
of your life, but it's temporary insanity, they get so wound up that they find it hard
to come back down, and usually what happens is they get medicated, usually they go to,
there's intervention because they start going crazy and hitting people and running around
tearing their clothes off that kind of thing, so eventually they get institutionalized
and medicated by society and then they gradually calm down and maybe they go see a therapist
and people start to think Buddhism is maybe a bad thing, they're papers about this,
they just, someone sent me a paper about how a mindfulness practice or meditation practice,
I guess in general, I didn't really read it because I don't know how interested I am
in what it says, but I can say as I've never had it happen to any of my students, so
across our fingers, no, quite confident it won't happen, this technique is quite solid
and concrete, right, it's hard to do it wrong, it's hard to have a wrong understanding
of how it works after a few days of me drilling it into you, exactly what you have to
do, it's pretty hard to get on the wrong path, so it's not something anyone should worry
about, the only thing you have to worry about is if you're not doing it, that's all, so
there you go, a little bit more done with today, hopefully I wasn't too much of a repetition,
thank you all for coming up.
Thank you all for coming up.
