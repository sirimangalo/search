1
00:00:00,000 --> 00:00:10,000
How many meditation techniques did the Buddha teach?

2
00:00:10,000 --> 00:00:19,000
As I said, I think you can't really put a number on them.

3
00:00:19,000 --> 00:00:26,000
So why I say this is because the Buddha had something called

4
00:00:26,000 --> 00:00:39,000
Indria-proprian-yuta, the knowledge of the

5
00:00:39,000 --> 00:00:43,000
the faculties of beings, knowledge of which faculties were lacking

6
00:00:43,000 --> 00:00:53,000
in which faculties were present in the people that he taught.

7
00:00:53,000 --> 00:00:56,000
So he knew right away how to teach one monk,

8
00:00:56,000 --> 00:01:00,000
he taught him by having him hold a cloth up in the sun.

9
00:01:00,000 --> 00:01:03,000
And as he was holding the cloth up in the sun, it became dirty.

10
00:01:03,000 --> 00:01:07,000
And because of something that he had cultivated in a past life,

11
00:01:07,000 --> 00:01:09,000
he was able to become enlightened.

12
00:01:09,000 --> 00:01:17,000
So the techniques of meditation that the Buddha taught are,

13
00:01:17,000 --> 00:01:21,000
I would say, only limited by the number of people that he taught.

14
00:01:21,000 --> 00:01:27,000
When he would give a talk, he knew exactly how to teach for that audience.

15
00:01:27,000 --> 00:01:31,000
Sometimes he would travel a long ways just to teach one person,

16
00:01:31,000 --> 00:01:35,000
because he had seen that they were ready to hear the teaching,

17
00:01:35,000 --> 00:01:40,000
and ready to realize the truth.

18
00:01:40,000 --> 00:01:42,000
So that's one thing they can be said.

19
00:01:42,000 --> 00:01:49,000
Now in Theravada Buddhist tradition, we separate meditation into two types.

20
00:01:49,000 --> 00:01:57,000
And the Buddha, it's possible to suggest that the Buddha did as well,

21
00:01:57,000 --> 00:02:02,000
that the Buddha said there are two things that are of benefit.

22
00:02:02,000 --> 00:02:05,000
And this is samata and vipassana.

23
00:02:05,000 --> 00:02:09,000
So we therefore tried to explain the Buddha's teachings

24
00:02:09,000 --> 00:02:12,000
and the many meditations that the Buddha taught under these two headings.

25
00:02:12,000 --> 00:02:15,000
Samata is the cultivation of tranquility.

26
00:02:15,000 --> 00:02:18,000
Vipassana is the cultivation of insight,

27
00:02:18,000 --> 00:02:22,000
because it seems quite clear,

28
00:02:22,000 --> 00:02:25,000
and quite readily apparent that there are certain meditations

29
00:02:25,000 --> 00:02:28,000
that don't directly lead to wisdom,

30
00:02:28,000 --> 00:02:32,000
because they are focused on a concept.

31
00:02:32,000 --> 00:02:35,000
For instance, if you focus on a light,

32
00:02:35,000 --> 00:02:37,000
there's many of these traditional meditations

33
00:02:37,000 --> 00:02:40,000
where you focus on a white color,

34
00:02:40,000 --> 00:02:41,000
or something.

35
00:02:41,000 --> 00:02:43,000
I did this video for kids now,

36
00:02:43,000 --> 00:02:47,000
just to teach them the basics of how to focus on something.

37
00:02:47,000 --> 00:02:50,000
So I've had them focus on colors in this video.

38
00:02:50,000 --> 00:02:53,000
I don't know if many kids actually watched it,

39
00:02:53,000 --> 00:02:55,000
but it's out there.

40
00:02:55,000 --> 00:02:58,000
But this one can't lead to wisdom. It can't lead to insight.

41
00:02:58,000 --> 00:03:01,000
So we call this one samata meditation.

42
00:03:01,000 --> 00:03:05,000
Because looking, you can stare at white for as long as you want.

43
00:03:05,000 --> 00:03:09,000
There's nothing about the way reality works

44
00:03:09,000 --> 00:03:11,000
in the contemplation of white.

45
00:03:11,000 --> 00:03:15,000
If you meditate on the Buddha or you meditate on God,

46
00:03:15,000 --> 00:03:20,000
for example, there's nothing in those objects

47
00:03:20,000 --> 00:03:23,000
that will lead you to insight.

48
00:03:23,000 --> 00:03:26,000
Yeah, the Buddha had us practice meditation

49
00:03:26,000 --> 00:03:31,000
or to some sense, practice meditation on the Buddha

50
00:03:31,000 --> 00:03:34,000
to recite to ourselves.

51
00:03:34,000 --> 00:03:39,000
It'd be so Bhagavad to recollect the Buddha's virtues.

52
00:03:39,000 --> 00:03:41,000
And so this is samata.

53
00:03:41,000 --> 00:03:43,000
Now we pass on that,

54
00:03:43,000 --> 00:03:45,000
is the practice to see clearly.

55
00:03:45,000 --> 00:03:47,000
So you would take this meditation object.

56
00:03:47,000 --> 00:03:50,000
For example, this white disc,

57
00:03:50,000 --> 00:03:53,000
and you would begin to look at it differently.

58
00:03:53,000 --> 00:03:57,000
Once your mind became calm through the practice of tranquility,

59
00:03:57,000 --> 00:04:03,000
you would then begin to examine it simply as seeing,

60
00:04:03,000 --> 00:04:04,000
as the Buddha said,

61
00:04:04,000 --> 00:04:07,000
deep pain, deep pamatam, hoi sati.

62
00:04:07,000 --> 00:04:12,000
If you want, this monk asked for the most concise teaching,

63
00:04:12,000 --> 00:04:14,000
the concise practice.

64
00:04:14,000 --> 00:04:16,000
So none of this long practice of having to develop

65
00:04:16,000 --> 00:04:18,000
common tranquility first,

66
00:04:18,000 --> 00:04:24,000
what's the practice that is going to be the most brief,

67
00:04:24,000 --> 00:04:30,000
the quickest to the shortest path to become enlightened.

68
00:04:30,000 --> 00:04:32,000
And so the Buddha gave him the shortest path

69
00:04:32,000 --> 00:04:35,000
and is a very important sutta for this reason.

70
00:04:35,000 --> 00:04:38,000
Because it's an answer to the question,

71
00:04:38,000 --> 00:04:40,000
what is the shortest path?

72
00:04:40,000 --> 00:04:42,000
And the Buddha said, deep taini, tamatam, hoi sati,

73
00:04:42,000 --> 00:04:46,000
train yourself so that seeing will just be seen.

74
00:04:46,000 --> 00:04:49,000
Hearing will just be hearing, smelling will just be tasting,

75
00:04:49,000 --> 00:04:54,000
feeling will just be feeling, thinking will just be thinking.

76
00:04:54,000 --> 00:04:56,000
And when you do this, there will be no,

77
00:04:56,000 --> 00:04:58,000
basically there will be no self.

78
00:04:58,000 --> 00:05:01,000
You will not have any attachment to any of these things.

79
00:05:01,000 --> 00:05:05,000
You will not find yourself in any of these things.

80
00:05:05,000 --> 00:05:10,000
And when you do that, you will, your mind will become free.

81
00:05:10,000 --> 00:05:12,000
And so simply by practicing that, in that way,

82
00:05:12,000 --> 00:05:15,000
Bahia was able to become enlightened,

83
00:05:15,000 --> 00:05:18,000
getting there, prostrating himself there at the Buddhist feet

84
00:05:18,000 --> 00:05:23,000
or standing there with the Buddha, listening to the Buddha teach.

85
00:05:23,000 --> 00:05:25,000
Now this is the practice of Yipasana.

86
00:05:25,000 --> 00:05:30,000
When you begin to look at things as they are,

87
00:05:30,000 --> 00:05:37,000
rather than trying to cultivate some specific state

88
00:05:37,000 --> 00:05:39,000
or some specific concentration,

89
00:05:39,000 --> 00:05:42,000
you begin to look at even the states of concentration.

90
00:05:42,000 --> 00:05:44,000
So these women, the next question these women had,

91
00:05:44,000 --> 00:05:46,000
or the one woman had, is the question that I always get

92
00:05:46,000 --> 00:05:49,000
from people who are practicing on their own,

93
00:05:49,000 --> 00:05:53,000
who don't have, who haven't cultivated meditation

94
00:05:53,000 --> 00:05:56,000
to, over the long term with the teacher,

95
00:05:56,000 --> 00:06:00,000
they will ask, they will explain that they,

96
00:06:00,000 --> 00:06:04,000
after some time they get to a state of nothingness,

97
00:06:04,000 --> 00:06:06,000
where there is nothing.

98
00:06:06,000 --> 00:06:08,000
So they'll be practicing whatever meditation they were

99
00:06:08,000 --> 00:06:09,000
practicing for some time.

100
00:06:09,000 --> 00:06:13,000
And then suddenly they enter into a state where there is nothing.

101
00:06:13,000 --> 00:06:17,000
There is no breathing if they were watching the breath

102
00:06:17,000 --> 00:06:22,000
or no Buddha if they were thinking of the Buddha.

103
00:06:22,000 --> 00:06:23,000
There would just be nothing.

104
00:06:23,000 --> 00:06:26,000
And so they don't know what to do at that point.

105
00:06:26,000 --> 00:06:31,000
And so the practice of Yipasana,

106
00:06:31,000 --> 00:06:33,000
this is where the practice of Yipasana kicks in.

107
00:06:33,000 --> 00:06:37,000
This is where you begin to look at even the state of,

108
00:06:37,000 --> 00:06:41,000
in this case, peace or calm or tranquillity of mind.

109
00:06:41,000 --> 00:06:46,000
There's a capacity to begin to examine that state.

110
00:06:46,000 --> 00:06:50,000
Because you'll see that actually it is also a contrived state.

111
00:06:50,000 --> 00:06:53,000
You say to yourself, calm, calm, quiet, quiet,

112
00:06:53,000 --> 00:06:56,000
and you'll get a grasp of this feeling.

113
00:06:56,000 --> 00:07:00,000
And you'll pick up this subtle liking and the subtle attachment

114
00:07:00,000 --> 00:07:05,000
to it and the subtle encouragement that exists in the mind

115
00:07:05,000 --> 00:07:09,000
that leads you to fall into this state again and again.

116
00:07:09,000 --> 00:07:11,000
And you begin to see that these are also impermanent

117
00:07:11,000 --> 00:07:13,000
and satisfying and uncontrollable.

118
00:07:13,000 --> 00:07:15,000
They can't, they don't last forever.

119
00:07:15,000 --> 00:07:20,000
They don't really bring you true and lasting peace and happiness.

120
00:07:20,000 --> 00:07:22,000
And so you're able to give them up.

121
00:07:22,000 --> 00:07:27,000
And of course, as a result give up anything else that might be,

122
00:07:27,000 --> 00:07:32,000
that might be even less pleasant and less comfortable.

123
00:07:32,000 --> 00:07:35,000
So these are the two types of meditation.

124
00:07:35,000 --> 00:07:37,000
The first one is for the practice of bringing calm.

125
00:07:37,000 --> 00:07:41,000
The second one is for the practice of bringing about insight.

126
00:07:41,000 --> 00:07:45,000
And you might say that basically the Buddha taught these two practices.

127
00:07:45,000 --> 00:07:48,000
For people who had time, you would teach them the practice of samata first

128
00:07:48,000 --> 00:07:50,000
and then give them insight.

129
00:07:50,000 --> 00:07:53,000
For people who didn't have time, you would teach them what you might call

130
00:07:53,000 --> 00:07:55,000
samata and rebass in it together.

131
00:07:55,000 --> 00:07:59,000
So the way I would like to explain it, let's use a graphic here.

132
00:07:59,000 --> 00:08:02,000
Some people, there's two qualities in mind.

133
00:08:02,000 --> 00:08:09,000
One is you say concentration or focus and the other is wisdom or insight.

134
00:08:09,000 --> 00:08:10,000
And you need them both.

135
00:08:10,000 --> 00:08:12,000
You know, samata and rebass in it.

136
00:08:12,000 --> 00:08:13,000
You need them both.

137
00:08:13,000 --> 00:08:21,000
Some people will practice samata first and their concentration comes to a peak without any insight,

138
00:08:21,000 --> 00:08:23,000
without any knowledge whatsoever.

139
00:08:23,000 --> 00:08:27,000
Their mind is focused, but they don't really have a clue about reality.

140
00:08:27,000 --> 00:08:31,000
And then they start to develop wisdom until it comes up and joins together.

141
00:08:31,000 --> 00:08:34,000
And it joins together, that's the moment where you enter into an environment.

142
00:08:34,000 --> 00:08:39,000
Now, another way, and one that most teachers will, nowadays,

143
00:08:39,000 --> 00:08:43,000
will encourage of their students, is develop both together.

144
00:08:43,000 --> 00:08:47,000
So at the same time that you're developing calm, that you're focusing on an object,

145
00:08:47,000 --> 00:08:49,000
because we have people sit and do meditation.

146
00:08:49,000 --> 00:08:52,000
For example, watching the rising and falling of the stomach,

147
00:08:52,000 --> 00:08:55,000
or other people will watch the nose or so.

148
00:08:55,000 --> 00:09:01,000
To focus on reality as well, but to do it in a concentrated way as well.

149
00:09:01,000 --> 00:09:03,000
So at the same time as you're developing insight,

150
00:09:03,000 --> 00:09:06,000
you're also developing concentration.

151
00:09:06,000 --> 00:09:09,000
And so they still come together, and you still need both of them.

152
00:09:09,000 --> 00:09:16,000
And they still lead you to nibana, but they come up together.

153
00:09:16,000 --> 00:09:24,000
There's the third way that the Buddha taught is where you practice to see things clearly first,

154
00:09:24,000 --> 00:09:25,000
without any concentration.

155
00:09:25,000 --> 00:09:29,000
So this might be where you're studying, and when you're thinking a lot,

156
00:09:29,000 --> 00:09:32,000
when you're examining reality, but you don't have much concentration.

157
00:09:32,000 --> 00:09:36,000
And then you start to quiet your mind down afterwards.

158
00:09:36,000 --> 00:09:40,000
So first, we pass it in and then sum it up.

159
00:09:40,000 --> 00:09:44,000
There's ones who do what the Buddha talked about, these differences,

160
00:09:44,000 --> 00:09:47,000
the different ways of becoming enlightened.

161
00:09:47,000 --> 00:09:50,000
And this is where a lot of the controversy in Buddhism comes from,

162
00:09:50,000 --> 00:09:53,000
because everyone wants to say that no, no, only this way is right,

163
00:09:53,000 --> 00:09:55,000
or only that way is right and so on.

164
00:09:55,000 --> 00:10:01,000
But the point being that there are these two aspects of meditation practice.

165
00:10:01,000 --> 00:10:10,000
And observation suggests that they can be developed individually,

166
00:10:10,000 --> 00:10:17,000
but eventually have to balance out and come together in order to lead one to enlightenment.

167
00:10:17,000 --> 00:10:21,000
So I think one answer on how many meditation techniques,

168
00:10:21,000 --> 00:10:26,000
you can see how many types of meditation or aspects of the meditation practice are there.

169
00:10:26,000 --> 00:10:51,000
There's two aspects.

