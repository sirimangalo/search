WEBVTT

00:00.000 --> 00:04.400
Hello, welcome back to Ask a Monk.

00:04.400 --> 00:18.040
Today's question is on whether I think that there is more suffering in America, this is

00:18.040 --> 00:26.360
the question, where it is almost impossible not to find food and I guess because we are

00:26.360 --> 00:34.280
the people in America had developed their society, or more suffering in a tribal society.

00:34.280 --> 00:37.280
This is the question.

00:37.280 --> 00:46.640
So first of all, I'd like to sort of expand the question because I think the important

00:46.640 --> 00:57.200
point is whether not whether America now or America when it was owned by the Native American

00:57.200 --> 01:00.800
or First Nations people is better.

01:00.800 --> 01:10.000
The question is whether there is more suffering to be found in a society with technological

01:10.000 --> 01:23.000
advancements and politics and government and roads and cars and computers and iPods and so on.

01:23.000 --> 01:39.000
Or whether there is more suffering in a lifestyle that is simple, unrefined, unadvanced,

01:39.000 --> 01:44.960
unorganized, unorganized on a level similar to a tribe.

01:44.960 --> 01:51.160
And it's a difficult question, but why it's difficult is because I think there's merits

01:51.160 --> 01:52.160
to both sides.

01:52.160 --> 02:02.520
I mean, I want to be able to say that material advancement in some way heightens people's

02:02.520 --> 02:14.880
ability or interest in things like science, things like objective investigation and analysis

02:14.880 --> 02:15.880
and so on.

02:15.880 --> 02:20.800
I mean, the education side of things, I want to be able to say that that's a good thing

02:20.800 --> 02:22.880
into some degree.

02:22.880 --> 02:29.960
But I think given the choice, I would have to say that there's probably less suffering

02:29.960 --> 02:36.280
to be found in a less advanced society.

02:36.280 --> 02:43.000
And I think we have to advance this beyond the tribal societies of, say, North America

02:43.000 --> 02:45.600
in olden times now.

02:45.600 --> 02:51.680
There are still some tribal societies, but not really to the extent that you would have

02:51.680 --> 02:53.800
found some time ago.

02:53.800 --> 02:58.760
Because I think this is a rather limited sort of way of looking at things.

02:58.760 --> 03:04.760
There are many societies or cultures or groups of individuals, groups of people living in

03:04.760 --> 03:11.960
the world who are living in limited technological advancement and so on.

03:11.960 --> 03:19.280
I think living in Thailand and living in Sri Lanka, I've been able to see some of these

03:19.280 --> 03:20.280
groups.

03:20.280 --> 03:24.760
I mean, Thailand, for example, is quite advanced in many ways.

03:24.760 --> 03:31.560
But in the villages, in the countryside, you'll still find people who are very technologically

03:31.560 --> 03:41.960
behind and living in fairly unorganized manners, in terms of political structure and so on.

03:41.960 --> 03:45.320
And you're in Sri Lanka even more so.

03:45.320 --> 03:54.560
You don't see the impact of modern society to such a great extent as you would in the

03:54.560 --> 03:55.560
West.

03:55.560 --> 04:05.560
And I think, beneficially, at least to some extent, that it has allowed people to be free

04:05.560 --> 04:07.720
from some of the sufferings.

04:07.720 --> 04:12.360
So stacking up the sufferings, I think the obvious suffering that was pointed out in the

04:12.360 --> 04:22.280
question is living in a tribal society or an uncivilized quote-unquote society, it's going

04:22.280 --> 04:27.400
to be more difficult to find food and I don't really think this is that big of a deal.

04:27.400 --> 04:33.600
In fact, I probably would care to wager that North America, it was a lot easier to find

04:33.600 --> 04:41.200
food before technology came into effect and now the food quality has gone down and people

04:41.200 --> 04:44.040
have to work hard for it and so on.

04:44.040 --> 04:49.000
I would say that anyway, that's maybe a bit speculative.

04:49.000 --> 04:54.400
But the real suffering, I think, that comes, well, it was being pointed here, pointed

04:54.400 --> 05:05.280
to here, is the lack of amenities, you know, living in rough conditions, having to put

05:05.280 --> 05:11.080
up with insects, having to put up with leeches, having to put up with snakes and scorpions,

05:11.080 --> 05:15.880
having to put up with mosquitoes because you don't have screens on your windows or you

05:15.880 --> 05:23.280
don't have the technology to prevent these things, having to put up with limited medical

05:23.280 --> 05:25.160
facilities and so on.

05:25.160 --> 05:32.360
There are a lot of sufferings that come with living in a simple manner and I think it's

05:32.360 --> 05:38.960
fair to point out that, you know, the monastic life is by its nature, the Buddhist monastic

05:38.960 --> 05:47.440
life is of a sort of simple life and as a result, there is a certain amount of suffering.

05:47.440 --> 05:50.280
This is why I think it was an interesting question and why I'd like to take some time

05:50.280 --> 05:56.520
to answer it because I have to personally go through some of the sufferings that, you

05:56.520 --> 06:01.080
know, the villagers here even have to go through, perhaps even more so than they have

06:01.080 --> 06:02.240
to go through.

06:02.240 --> 06:07.840
I might be going through some of the sufferings that you might expect from a tribal

06:07.840 --> 06:15.520
society because, you know, for me food is limited, I only eat one meal a day so you could

06:15.520 --> 06:21.400
say that's similar to the suffering that is brought about by having to hunt for your

06:21.400 --> 06:26.120
food, not knowing where you're going to get your next meal, not having a farm land or

06:26.120 --> 06:34.080
a grocery store or money that you can easily access these things with and having to

06:34.080 --> 06:41.760
put up with limited medical facilities, but I think the real key here is that, or the

06:41.760 --> 06:49.600
a real good way to answer this question is to point out the fact that so many people undertake

06:49.600 --> 06:56.880
this lifestyle on purpose and voluntarily and why would they do it if it was just going

06:56.880 --> 06:58.040
to lead to their suffering?

06:58.040 --> 07:02.120
And I think that's a question that a lot of people ask why do people decide to live in

07:02.120 --> 07:09.920
caves and where, you know, robes, you know, a single set of robes, et cetera, et cetera,

07:09.920 --> 07:15.360
et cetera, are eating only one meal a day, putting up with all of these difficulties.

07:15.360 --> 07:19.360
And I think it's because it's a trade-off.

07:19.360 --> 07:30.720
If you give up or if you want to be free from all of the stresses and all of the sufferings

07:30.720 --> 07:38.200
that are on the other side of the defense, then you have to, you can't expect to have

07:38.200 --> 07:39.200
all the amenities.

07:39.200 --> 07:40.240
I mean, they go hand in hand.

07:40.240 --> 07:46.040
The sufferings on the other side of a technologically advanced society are the stresses

07:46.040 --> 07:51.400
of having to work a job, you know, nine to five job, the stresses of having to think a

07:51.400 --> 07:56.520
lot of having to deal with people, crowds of people having to deal with traffic, having

07:56.520 --> 08:03.560
to deal with the complications that come with technology, like fixing your car and, you

08:03.560 --> 08:11.080
know, your house and all of the laws and having to deal with laws and lawyers and people

08:11.080 --> 08:13.480
suing you and so on.

08:13.480 --> 08:18.440
There's a huge burden of stress having to be worried about thieves, having to be worried

08:18.440 --> 08:26.240
about crooks, not crook butt, people cheating you, having to be worried about your

08:26.240 --> 08:32.200
boss, your co-workers, your clients and so on and a million other things.

08:32.200 --> 08:37.440
I mean, no matter what work you apply yourself to in an advanced society, there are more

08:37.440 --> 08:44.240
complications and as a result, more stress and as a result, more suffering.

08:44.240 --> 08:52.960
So I think the trade-off there is quite worth it in my mind because I'd like to, you

08:52.960 --> 08:57.200
know, the answer for me is quite clear because having put up with all these sufferings

08:57.200 --> 09:03.920
of having to have mosquitoes bite me and who knows what bite me and having to put up

09:03.920 --> 09:09.800
with leeches and you know, the worries that I have to put up with worrying about stepping

09:09.800 --> 09:15.880
on a snake, worrying about that forefoot lizard down below me and the monkey scrapping

09:15.880 --> 09:17.880
on my head and so on.

09:17.880 --> 09:25.240
I really don't feel upset or affected by these things, they're physical, a physical reality

09:25.240 --> 09:28.160
and they're much more physical than they are mental.

09:28.160 --> 09:34.600
I feel so much more at peace here living in the jungle which, you know, may look peaceful

09:34.600 --> 09:41.200
but it's actually quite dangerous and it keeps you on your toes in many ways, even during

09:41.200 --> 09:45.480
meditation, you have to do these little bugs that you can't even see them, they're

09:45.480 --> 09:53.240
worse than mosquitoes and they bite you but it's not a stress for me, not in the same

09:53.240 --> 09:59.560
way as living in, say, the big cities of Colombo, Bangkok, Los Angeles, the stress is

09:59.560 --> 10:04.960
that's involved there, you know, it's an order of magnitude greater.

10:04.960 --> 10:09.760
So I'm not sure if this exactly answers the question but this is how I would prefer

10:09.760 --> 10:20.160
to approach it, I think there's far less stress to be had from a simple lifestyle because

10:20.160 --> 10:30.760
the stress is much more physical than they are mental and the mental stress is far less.

10:30.760 --> 10:38.120
Another thing about living in a simple society in a simple lifestyle is that it's much

10:38.120 --> 10:43.240
more in tune with our programming physically, I mean we're still very much related to

10:43.240 --> 10:51.320
the monkeys and so this kind of atmosphere jives with us, seeing the trees, why people

10:51.320 --> 10:55.360
go to nature and they feel peaceful of a sudden is because of how, it's not because there's

10:55.360 --> 11:00.680
anything special there, it's because of what's not there, there's nothing jarring with

11:00.680 --> 11:06.440
your experience, no horns blasting, no bright lights, no beautiful pictures catching your

11:06.440 --> 11:14.520
attention, no ugly sights and so on, it's very peaceful, very calm, very natural, right?

11:14.520 --> 11:22.400
I mean that's why we're here and I think you'll find that in a traditional society.

11:22.400 --> 11:27.360
So I think, and I think this is borne out by the Buddhist teaching, it's borne out by

11:27.360 --> 11:32.920
the examples, obviously, of those people who have ordained us monks.

11:32.920 --> 11:36.600
There was a story in the typical, in the Buddhist teaching that we always go back to,

11:36.600 --> 11:42.720
I think in the commentaries actually, I'm not sure, well at least it's in the commentaries

11:42.720 --> 11:53.000
about this king, relative of the king who became a monk, Mahakapina and after he became

11:53.000 --> 11:57.000
a monk, he would sit under this tree, used to be a king, right?

11:57.000 --> 12:04.040
So after he became a monk, he would sit under a tree and go, a horse, a horse, a horse,

12:04.040 --> 12:09.440
a horse, which means, oh, what happiness, oh, what bliss, oh, what happiness.

12:09.440 --> 12:15.520
So you might say he was actually contemplating the happiness, he was looking at it and examining

12:15.520 --> 12:20.680
it in a much in the same way that we say to ourselves, happy, happy.

12:20.680 --> 12:26.160
But the monks heard him saying this and remarking this and they took it the wrong way,

12:26.160 --> 12:29.840
they took it to me and he was, for some reason, they took it that he must be thinking

12:29.840 --> 12:36.720
about his kingly happiness, he was remembering, oh, what happiness it was to be a king.

12:36.720 --> 12:39.680
And so they took him to the Buddha and the Buddha asked him, you know, what was it?

12:39.680 --> 12:47.640
And he said, no, when I was a king, I had to worry about all of these stresses and concerns.

12:47.640 --> 12:51.920
My life was always in danger of assassination, I had all this treasure that I had to guard

12:51.920 --> 12:58.120
all these ministers that I had to manage and, you know, to stop from cheating me and all

12:58.120 --> 13:02.120
the people who would come to me with their problems and all the laws I had to enforce

13:02.120 --> 13:04.720
and so on and so on.

13:04.720 --> 13:05.760
And I have none of that now.

13:05.760 --> 13:12.800
Now I have this tree, three robes, my bull, my tree and this is the greatest happiness

13:12.800 --> 13:15.320
I've ever known in my life, he said.

13:15.320 --> 13:20.840
So there's a story to go with this question.

13:20.840 --> 13:27.480
I think one final note is that regardless of the answer to this question, whether there's

13:27.480 --> 13:35.000
more suffering in either one, I think the point is that whatever lifestyle you can undertake,

13:35.000 --> 13:40.720
not that has the least amount of suffering, but which has the least amount of unwholesumness

13:40.720 --> 13:43.600
involved and that is the best lifestyle.

13:43.600 --> 13:47.600
So for those people, you know, who might be discouraged then by the fact that they have

13:47.600 --> 13:52.440
to live in a technologically advanced society as a Buddhist and they feel like they should

13:52.440 --> 13:57.000
be living in the forest, I don't mean to say that there's anything wrong with living

13:57.000 --> 13:58.000
in the city.

13:58.000 --> 13:59.000
I've lived in Los Angeles.

13:59.000 --> 14:05.880
I've gone on arms round in North Hollywood and in places, you know, in Bangkok, in Colombo.

14:05.880 --> 14:12.560
I've gone, I've lived in these places in fairly chaotic and stressful situations, but

14:12.560 --> 14:16.800
the point is to live your life in such a way that it's free from unwholesumness.

14:16.800 --> 14:23.400
So this is where the question gets interesting because many tribal societies are involved

14:23.400 --> 14:26.400
in what Buddhists would call great unwholesumness.

14:26.400 --> 14:31.400
Their way of lifestyle is hunting, as an example, and though they do it to serve, you

14:31.400 --> 14:39.680
could say it to survive, it's by nature caught up with these mistakes of cruelty, states

14:39.680 --> 14:50.880
of anger and delusion, and as a result, it keeps them bound to the wheel of life that they

14:50.880 --> 14:53.680
are going to have to take turns with the deer.

14:53.680 --> 14:56.880
You know, this is what the First Nations people believe.

14:56.880 --> 15:01.000
They still believe that, you know, you take turns.

15:01.000 --> 15:06.160
I hunt the deer this life and in the next life, I'm the deer and they hunt me and so on.

15:06.160 --> 15:10.240
There are these kinds of beliefs and this is really in line with the Buddha's teaching.

15:10.240 --> 15:16.720
So I mean, if you're fine with that, having to be hunted every other lifetime, every few

15:16.720 --> 15:19.840
lifetimes, then so be it.

15:19.840 --> 15:24.760
But this isn't the, from a Buddhist point of view, this isn't the way out of suffering.

15:24.760 --> 15:26.280
It's because there's a lot of suffering.

15:26.280 --> 15:30.720
I mean, tribes, of course, then there's tribal warfare and, you know, they're not free from

15:30.720 --> 15:32.280
all of this.

15:32.280 --> 15:37.120
And we're not free from it on the other side as living in technologically advanced societies.

15:37.120 --> 15:41.080
You know, living in Canada, I wasn't, you know, free from these things.

15:41.080 --> 15:42.080
I was hunting as well.

15:42.080 --> 15:44.920
I didn't need to, but I went hunting with my father.

15:44.920 --> 15:50.480
You know, we engage in a lot of unholesumness in advanced societies.

15:50.480 --> 15:52.920
So that's the most important.

15:52.920 --> 15:53.920
You can live in a city.

15:53.920 --> 15:54.920
You can live in nature.

15:54.920 --> 16:01.120
The reason why we choose to live in nature is, I think, overall, you know, the amount of

16:01.120 --> 16:05.920
peace that you have is the one side, but there's also a lot less unholesumness that you're

16:05.920 --> 16:13.200
free from having to deal with, you know, other people's unholesumness and having to see

16:13.200 --> 16:18.880
all these beautiful, attractive sites and wanting to get this wanting to get that being

16:18.880 --> 16:21.080
annoyed by people being upset by people.

16:21.080 --> 16:28.480
So you're able to focus much clearer on the very core aspects of existence, you know, what

16:28.480 --> 16:35.120
really is reality, without having to be, get caught up in unholesum mindset, stress and

16:35.120 --> 16:37.840
addiction and so on.

16:37.840 --> 16:38.840
And I think that's clear.

16:38.840 --> 16:44.320
I think being here in the forest is much better for my meditation practice than being

16:44.320 --> 16:45.800
in the city.

16:45.800 --> 16:51.520
But in the end, it's not the biggest concern, and I'm not afraid to live in the city if

16:51.520 --> 16:57.040
I have to, because, for me, the most important is to continue the meditation practice

16:57.040 --> 17:02.120
and to learn more about my reactions and my interactions with the world around me.

17:02.120 --> 17:05.480
And sometimes that can be tested by suffering.

17:05.480 --> 17:09.240
Again, the Buddha's teaching is not to run away from suffering.

17:09.240 --> 17:12.200
It's to learn and to come to understand suffering.

17:12.200 --> 17:16.080
When you understand suffering, you'll understand what is really causing suffering, that

17:16.080 --> 17:20.880
it's not the objects, it's your relationship and your reactions to them, your attachment

17:20.880 --> 17:25.600
to things being in a certain way and not being in another way, your attachment to the

17:25.600 --> 17:27.960
objects of the sense and so on.

17:27.960 --> 17:31.960
When you can be free from that, it doesn't matter where you live, whether it be in a cave

17:31.960 --> 17:34.160
or in an apartment complex.

17:34.160 --> 18:03.240
So there's the answer to your question, thanks, and have a good day.

