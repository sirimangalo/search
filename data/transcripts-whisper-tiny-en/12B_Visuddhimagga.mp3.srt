1
00:00:00,000 --> 00:00:07,000
Locos view means it takes that there is no cause for something to happen.

2
00:00:07,000 --> 00:00:11,000
Anything can happen without a cause.

3
00:00:11,000 --> 00:00:13,000
So that is called no cause view.

4
00:00:13,000 --> 00:00:17,000
But if you deny the cause, you also deny the effect.

5
00:00:17,000 --> 00:00:23,000
So in reality these three kinds of wrong view are the same.

6
00:00:23,000 --> 00:00:27,000
One denies the cause, the other denies the effect.

7
00:00:27,000 --> 00:00:35,000
So if you deny the effect, you deny the cause also.

8
00:00:35,000 --> 00:00:43,000
And the other is nailistic view.

9
00:00:43,000 --> 00:00:46,000
That is, there is nothing.

10
00:00:46,000 --> 00:00:53,000
Whatever you do, we will not constitute a comma.

11
00:00:53,000 --> 00:00:57,000
That is, there is second one, more in the efficacy of action view.

12
00:00:57,000 --> 00:01:04,000
And the nailistic view.

13
00:01:04,000 --> 00:01:12,000
No cause, no doing.

14
00:01:12,000 --> 00:01:21,000
And then there is no results from the practice of keeping.

15
00:01:21,000 --> 00:01:33,000
There is no nothing like to be held as respect to your parents,

16
00:01:33,000 --> 00:01:36,000
to your elders and so on.

17
00:01:36,000 --> 00:01:41,000
So you don't have to pay respect to your elders or whatever.

18
00:01:41,000 --> 00:01:43,000
Something like that.

19
00:01:43,000 --> 00:01:46,000
And there is no this wall, there is no other wall.

20
00:01:46,000 --> 00:01:56,000
There are no holy men who have practiced and then a change would be speech,

21
00:01:56,000 --> 00:01:58,000
or a change meant, and so on.

22
00:01:58,000 --> 00:02:02,000
And that kind of thing is called a nailistic view here.

23
00:02:02,000 --> 00:02:09,000
So if a person has one of these wrong views, or if he is a hammer for that,

24
00:02:09,000 --> 00:02:13,000
or if he is a unit, and he will not be able to change Anna,

25
00:02:13,000 --> 00:02:18,000
the comma result, that is, who have had a rebirth linking with no profitable root cause,

26
00:02:18,000 --> 00:02:23,000
or with only two profitable root causes.

27
00:02:23,000 --> 00:02:28,000
That means when a person takes a rebirth,

28
00:02:28,000 --> 00:02:32,000
there is a type of consciousness which arises.

29
00:02:32,000 --> 00:02:37,000
And with that type of consciousness, the roots also arise.

30
00:02:37,000 --> 00:02:43,000
It is the root of root or the roots of either.

31
00:02:43,000 --> 00:02:54,000
The roots of either are attachment, hatred, and delusion.

32
00:02:54,000 --> 00:02:59,000
And that the root of the opposite of these three.

33
00:02:59,000 --> 00:03:12,000
Now, when a person takes rebirth, his rebirth consciousness is not accompanied by any of these roots at all,

34
00:03:12,000 --> 00:03:19,000
or they may be accompanied by two or three of the good roots.

35
00:03:19,000 --> 00:03:32,000
And there is no rebirth consciousness, which is accompanied by any of the evil roots.

36
00:03:32,000 --> 00:03:41,000
So if a person's rebirth consciousness is not accompanied by any of these roots,

37
00:03:41,000 --> 00:03:49,000
then he cannot change Anna in that life.

38
00:03:49,000 --> 00:03:56,000
Or if his rebirth consciousness is accompanied by only two good roots,

39
00:03:56,000 --> 00:04:00,000
in that case too, he cannot change Anna in that life.

40
00:04:00,000 --> 00:04:04,000
Two root means non-grid and non-hatred.

41
00:04:04,000 --> 00:04:12,000
That means without understanding all this, Anna of Banya.

42
00:04:12,000 --> 00:04:22,000
So our rebirth consciousness must be accompanied by Banya,

43
00:04:22,000 --> 00:04:27,000
in order for us to attain Jana in this life.

44
00:04:27,000 --> 00:04:35,000
Anna and also the evil and enlightenment in this life.

45
00:04:35,000 --> 00:04:42,000
Otherwise, it is impossible for us to attain Jana or the enlightenment.

46
00:04:42,000 --> 00:04:45,000
So this is called by Tama Rizha.

47
00:04:45,000 --> 00:04:50,000
And Black faith means a deathitude of faith in the Buddha, the Dhamma, and Gaa,

48
00:04:50,000 --> 00:04:57,000
of the lack of zeal, a deathitude of zeal for the unopposed will lack of understanding

49
00:04:57,000 --> 00:05:07,000
that deathitude of mundane and supermanning right view, right view means understanding

50
00:05:07,000 --> 00:05:11,000
of Kama and Rizha.

51
00:05:11,000 --> 00:05:21,000
This is called right view, and then the incapable of entering into this study or right net

52
00:05:21,000 --> 00:05:27,000
in profitable statement that they are incapable of entering into the noble fact,

53
00:05:27,000 --> 00:05:31,000
or certainty and right net in profitable state.

54
00:05:31,000 --> 00:05:40,000
That just simply means incapable of enlightenment, incapable of attaining Jana's.

55
00:05:40,000 --> 00:05:45,000
And this does not apply only to Kṛṣṇa.

56
00:05:45,000 --> 00:05:50,000
For none of them will succeed in developing any meditation subject at all.

57
00:05:50,000 --> 00:05:56,000
So the task of devotion to a meditation subject must be undertaken by a grandpa who has no hindrances,

58
00:05:56,000 --> 00:05:59,000
no hindrances by Kama Rizha.

59
00:05:59,000 --> 00:06:11,000
He stands hindrances by Kama and by Tama and Kṛṣṇa.

60
00:06:11,000 --> 00:06:20,000
So in order to practice successfully and you should be free from these defects.

61
00:06:20,000 --> 00:06:26,000
There is a fifth chapter.

62
00:06:26,000 --> 00:06:31,000
The next chapter is unpleasant chapter.

63
00:06:31,000 --> 00:06:34,000
Because it is scheduled.

64
00:06:34,000 --> 00:06:43,000
What is Lothan or what is foul about the dead body and also applying the state of dead

65
00:06:43,000 --> 00:06:47,000
body to our living bodies?

66
00:06:47,000 --> 00:06:57,000
And this is the fondness of the body meditation or lothameness of the body meditation.

67
00:06:57,000 --> 00:07:04,000
Most people are, I think maybe in the worst most people are afraid of this meditation.

68
00:07:04,000 --> 00:07:15,000
And the man asked me, do I have to tell my wife that I imagine how to be a staring

69
00:07:15,000 --> 00:07:18,000
or something that they are going to go back home?

70
00:07:18,000 --> 00:07:26,000
He asked, if you practice this kind of meditation, then you have to do it.

71
00:07:26,000 --> 00:07:30,000
Whether you tell your wife, that's another medical.

72
00:07:30,000 --> 00:07:32,000
You may or may not do so.

73
00:07:32,000 --> 00:07:37,000
But if you are going to be practicing this, then you have to see not only your wife anybody,

74
00:07:37,000 --> 00:07:39,000
including yourself.

75
00:07:39,000 --> 00:07:46,000
So as Lothan, as well as foul or whatever.

76
00:07:46,000 --> 00:07:52,000
So if you don't like it, you don't practice it, it's okay.

77
00:07:52,000 --> 00:08:06,000
But it is very powerful and effective in removing attachment to bodies to self.

78
00:08:06,000 --> 00:08:22,000
Because once you see the various conditions of your dead body become less interested in your

79
00:08:22,000 --> 00:08:25,000
body or in the body of any other person.

80
00:08:25,000 --> 00:08:29,000
Just today, I was talking with the monk in L.A.

81
00:08:29,000 --> 00:08:36,000
And he said, he lived in villages for some time.

82
00:08:36,000 --> 00:08:44,000
So sometimes he had a chance to create a dead person and sometimes a dead monk.

83
00:08:44,000 --> 00:08:53,000
And he said, after doing this for two or three times, he got disgusted of his body too.

84
00:08:53,000 --> 00:09:00,000
This type of attachment is particularly useful and recommended for people to look for strong

85
00:09:00,000 --> 00:09:01,000
degree.

86
00:09:01,000 --> 00:09:02,000
Yeah.

87
00:09:02,000 --> 00:09:10,000
An attachment to one's body is discouraged with people who run anger or hatred.

88
00:09:10,000 --> 00:09:11,000
No.

89
00:09:11,000 --> 00:09:12,000
A specialist?

90
00:09:12,000 --> 00:09:13,000
No.

91
00:09:13,000 --> 00:09:14,000
A specialist?

92
00:09:14,000 --> 00:09:16,000
Not so bad.

93
00:09:16,000 --> 00:09:25,000
But it is recommended for those who have strong attachment to their body, to their life.

94
00:09:25,000 --> 00:09:28,000
We will come to it later.

95
00:09:28,000 --> 00:09:33,000
So you may have read all of this.

96
00:09:33,000 --> 00:09:40,000
So you know what is meditation, subjects of meditation.

97
00:09:40,000 --> 00:09:49,000
Actually, these are the different stages of, what about that, decomposition, different stages

98
00:09:49,000 --> 00:09:52,000
of decomposition in a court.

99
00:09:52,000 --> 00:09:56,000
The first, after about two or three days, it becomes bloated.

100
00:09:56,000 --> 00:09:58,000
And then it deteriorated.

101
00:09:58,000 --> 00:10:05,000
And then it comes to the stage of the living one, then the first during, then the cut-up

102
00:10:05,000 --> 00:10:11,000
and so on.

103
00:10:11,000 --> 00:10:15,000
The first one, I want to say something about the first one, the bloated.

104
00:10:15,000 --> 00:10:22,000
It is bloated because bloated by a graduate, the relation and swelling after.

105
00:10:22,000 --> 00:10:30,000
Odom means above or after, the close of light as a bellows is with wind.

106
00:10:30,000 --> 00:10:34,000
What is bloated is the same as the bloated?

107
00:10:34,000 --> 00:10:37,000
But please look at the polly words.

108
00:10:37,000 --> 00:10:41,000
The first one is, Odomaata, right?

109
00:10:41,000 --> 00:10:45,000
And the second one is, Odomaata, cut.

110
00:10:45,000 --> 00:10:48,000
So the cut, cut is added to the word.

111
00:10:48,000 --> 00:11:02,000
But the commentary wants us to understand is that cuts denote the notes of the

112
00:11:02,000 --> 00:11:05,000
discussed.

113
00:11:05,000 --> 00:11:09,000
So if you say, Odomaata, it is frustrating.

114
00:11:09,000 --> 00:11:17,000
But if one you say, Odomaata, it is frustrating body, which is disgusting, something like that.

115
00:11:17,000 --> 00:11:23,000
So the particle cut is added to show that it is disgusting.

116
00:11:23,000 --> 00:11:25,000
That is what we should understand.

117
00:11:25,000 --> 00:11:31,000
But it is very difficult to convey this meaning in a translation.

118
00:11:31,000 --> 00:11:35,000
You have to have a footnote and then say about it.

119
00:11:35,000 --> 00:11:42,000
So all the ten kinds of meditations of the cut at the end of this.

120
00:11:42,000 --> 00:11:46,000
Odomaata, and the second one is, windy, like, cut.

121
00:11:46,000 --> 00:11:55,000
And the third one is, we took a gun, so on.

122
00:11:55,000 --> 00:12:01,000
And then in order to practice this, you have to do a lot of preparation.

123
00:12:01,000 --> 00:12:08,000
And then everything is explained in detail in this book.

124
00:12:08,000 --> 00:12:17,000
For example, you are not to go to the court as soon as you hear that there is a court in such and such a place.

125
00:12:17,000 --> 00:12:19,000
You are not to go there directly.

126
00:12:19,000 --> 00:12:25,000
And it is said that sometimes there may be a while anywhere around the court,

127
00:12:25,000 --> 00:12:31,000
or there may be some goes around the court, and then maybe your life may be in danger.

128
00:12:31,000 --> 00:12:39,000
So you must not go directly to the place, but you have to study the road to and from it.

129
00:12:39,000 --> 00:12:43,000
And then things on the road, everything is to be noticed.

130
00:12:43,000 --> 00:12:51,000
There is a stone rock here, and this road goes here, advanced to us to the left,

131
00:12:51,000 --> 00:12:55,000
or at another place, it goes to the right, and so on.

132
00:12:55,000 --> 00:13:02,000
So you have to take note of everything on the way to the place and the path to the point.

133
00:13:02,000 --> 00:13:07,000
Because this meditation is very frightening meditation.

134
00:13:07,000 --> 00:13:14,000
And you have to go alone, it is at two or three times in this book, no company and no company.

135
00:13:14,000 --> 00:13:16,000
So you have to do it alone.

136
00:13:16,000 --> 00:13:20,000
And sometimes when you are alone, you are fighting.

137
00:13:20,000 --> 00:13:27,000
And even the drop of a dead branch will make you fight them.

138
00:13:27,000 --> 00:13:30,000
Maybe it is a cause or something.

139
00:13:30,000 --> 00:13:32,000
So you have to be very careful.

140
00:13:32,000 --> 00:13:40,000
And then you have to inform at least the head of the modern or monastery where you live,

141
00:13:40,000 --> 00:13:42,000
that you are going there.

142
00:13:42,000 --> 00:13:49,000
And that is because sometimes, semadries are not like here.

143
00:13:49,000 --> 00:13:51,000
In this country, they are beautiful places.

144
00:13:51,000 --> 00:13:58,000
But semadries are places where some thieves or robbers frequent,

145
00:13:58,000 --> 00:14:03,000
because it is a place not many people go through.

146
00:14:03,000 --> 00:14:06,000
And so it is secreted for them.

147
00:14:06,000 --> 00:14:10,000
And then they may go to that place and followed by some people,

148
00:14:10,000 --> 00:14:16,000
they may drop, they think they have dropped near the mud.

149
00:14:16,000 --> 00:14:23,000
And then those who followed them might see the property close to the mud.

150
00:14:23,000 --> 00:14:27,000
And they may take the mud to be a robot to be a thief.

151
00:14:27,000 --> 00:14:33,000
And so in that case, if he had informed the head monk of the monastery,

152
00:14:33,000 --> 00:14:37,000
then he could explain which would be a thief.

153
00:14:37,000 --> 00:14:43,000
So you have to do a lot of preparation if you want to practice this kind of meditation,

154
00:14:43,000 --> 00:14:48,000
which is impossible even in our countries now at least,

155
00:14:48,000 --> 00:14:57,000
because dead bodies are not left, not not what about, but left on the ground.

156
00:14:57,000 --> 00:15:00,000
They are either cremated or buried.

157
00:15:00,000 --> 00:15:08,000
So it is almost impossible to see a cause and bloated cause or whatever.

158
00:15:08,000 --> 00:15:12,000
But let us say it is possible.

159
00:15:12,000 --> 00:15:15,000
So then you have to make these preparations.

160
00:15:15,000 --> 00:15:20,000
And when you go and sit and you are not to sit too close to the cause or too far away from the cause,

161
00:15:20,000 --> 00:15:25,000
and you are sit down, not downwind.

162
00:15:25,000 --> 00:15:33,000
You must sit up when, because it is a downwind you will be offended by that in the fight.

163
00:15:33,000 --> 00:15:40,000
So there are many things in detail you have to take care of before you go and try to sit.

164
00:15:40,000 --> 00:15:54,000
You know, it is a very frightening meditation and not many people would do this.

165
00:15:54,000 --> 00:15:59,000
You have to have some kind of courage.

166
00:15:59,000 --> 00:16:05,000
And one thing in our countries is that parents used to,

167
00:16:05,000 --> 00:16:11,000
or try to scare their children by saying, there is a ghost here or there is a ghost there.

168
00:16:11,000 --> 00:16:13,000
And so we are afraid of course.

169
00:16:13,000 --> 00:16:19,000
And even though we have grown up, there is some residue of it in our minds.

170
00:16:19,000 --> 00:16:25,000
So we cannot shake it altogether from ourselves,

171
00:16:25,000 --> 00:16:28,000
not people like people in this country.

172
00:16:28,000 --> 00:16:31,000
People in this country are not afraid of course.

173
00:16:31,000 --> 00:16:41,000
They may even talk with ghosts or they may even adopt ghosts, something like that.

174
00:16:41,000 --> 00:16:52,000
But one thing is, when you are invited to a funeral service,

175
00:16:52,000 --> 00:16:59,000
when months are, then you have to go to a cemetery and cemetery is not a place like here.

176
00:16:59,000 --> 00:17:03,000
So it is frightening, dirty, and then smelly.

177
00:17:03,000 --> 00:17:11,000
And sometimes what happens is, say, they do not have and pounding or whatever.

178
00:17:11,000 --> 00:17:14,000
Most people do not have that.

179
00:17:14,000 --> 00:17:22,000
So when the cause was carried to the cemetery, it may have to be comfortable and smile.

180
00:17:22,000 --> 00:17:24,000
So I had that experience.

181
00:17:24,000 --> 00:17:31,000
And once I was invited, and I went to the cemetery, and then the body was put in front of me.

182
00:17:31,000 --> 00:17:36,000
They are about, say, one and a half feet from me.

183
00:17:36,000 --> 00:17:47,000
And then you have to get smile, and then you have a dripping, interesting story.

184
00:17:47,000 --> 00:17:54,000
And that is always not good smell, that is a matter of it.

185
00:17:54,000 --> 00:18:01,000
There are months who live very close to, or in the cemetery.

186
00:18:01,000 --> 00:18:11,000
They do the small huts of goodies, and they practice this kind of meditation.

187
00:18:11,000 --> 00:18:18,000
So here is a bit no company here.

188
00:18:18,000 --> 00:18:25,000
And then the body should be of the opposite, not of the opposite sense.

189
00:18:25,000 --> 00:18:29,000
There is a mansion here two or three times.

190
00:18:29,000 --> 00:18:34,000
The men must look at the corpse of the men, and the women must look at the corpse of the woman.

191
00:18:34,000 --> 00:18:43,000
Because even a dead body can call some mental excitement, if it is not that long enough,

192
00:18:43,000 --> 00:18:45,000
not yet decomposed.

193
00:18:45,000 --> 00:18:53,000
So some people may have thoughts of lust, even looking at the corpse.

194
00:18:53,000 --> 00:19:00,000
So it is not recommended for a month to look at the corpse of a woman.

195
00:19:00,000 --> 00:19:28,000
And the skeleton, the last one is a skeleton.

196
00:19:28,000 --> 00:19:36,000
So you can practice looking at the whole skeleton, or you can practice looking at only one bone,

197
00:19:36,000 --> 00:19:45,000
and then think of the whole body as bone.

198
00:19:45,000 --> 00:19:57,000
And then on page 200, for a graph 85.

199
00:19:57,000 --> 00:20:02,000
And individually, the bloated suits one who is greedy about shape,

200
00:20:02,000 --> 00:20:07,000
since it makes evidence that it is figramal of the body's shape.

201
00:20:07,000 --> 00:20:14,000
The vivid suits one who is greedy about the body's color, since it makes evidence that this figramal of the skin's color.

202
00:20:14,000 --> 00:20:19,000
The festering suits one who is greedy about the smell of the body aroused by sense.

203
00:20:19,000 --> 00:20:25,000
Assume that the trust in it makes sense it makes evidence that it even smells connected with it so the body.

204
00:20:25,000 --> 00:20:34,000
The cut-up suits one who is greedy about compactness in the body, since it makes evidence the holowness inside it.

205
00:20:34,000 --> 00:20:40,000
The north suits one who is greedy about accumulation of flesh in such parts of the body.

206
00:20:40,000 --> 00:20:46,000
At the bridge, since it makes it evident how it finds accumulation of flesh comes to nothing.

207
00:20:46,000 --> 00:20:51,000
The scattered suits one who is greedy about the grace of the limbs,

208
00:20:51,000 --> 00:20:54,000
since it makes it evident how limbs can be scattered.

209
00:20:54,000 --> 00:20:59,000
The hex and scattered suits one who is greedy about your fine body as a whole,

210
00:20:59,000 --> 00:21:04,000
since it makes evidence that this integration and alteration of the body as a whole.

211
00:21:04,000 --> 00:21:08,000
The bleeding suits one who is greedy about elegant produce by elements,

212
00:21:08,000 --> 00:21:14,000
since it makes evidence its recogniziveness when smeared with blood.

213
00:21:14,000 --> 00:21:19,000
The warm infrastructure suits one who is greedy about ownership of the body,

214
00:21:19,000 --> 00:21:25,000
since it makes it evident how the body is shared with many families of bones.

215
00:21:25,000 --> 00:21:32,000
It's carried on suits one who is greedy about fine teeth, since it makes evidence that it is put upon it.

216
00:21:32,000 --> 00:21:39,000
If you have to stop the process of the body, it will be understood,

217
00:21:39,000 --> 00:21:46,000
because it comes to be ten full according to the subdivisions of the greedy cameraman.

218
00:21:46,000 --> 00:21:51,000
Now, it wasn't practices this formless meditation, either one of them.

219
00:21:51,000 --> 00:21:57,000
He can get only first chana, not the other chalets.

220
00:21:57,000 --> 00:22:03,000
And it is explained here with this simile.

221
00:22:03,000 --> 00:22:10,000
But as regards to ten full phone is just as it is only by virtue of its rudder.

222
00:22:10,000 --> 00:22:12,000
I don't think it is rudder.

223
00:22:12,000 --> 00:22:13,000
Radha means what?

224
00:22:13,000 --> 00:22:14,000
What is rudder?

225
00:22:14,000 --> 00:22:16,000
The steering spin.

226
00:22:16,000 --> 00:22:17,000
Yeah.

227
00:22:17,000 --> 00:22:24,000
But it doesn't mean rudder in terms is that the poly word is the pole.

228
00:22:24,000 --> 00:22:31,000
We use pole to push the poles in a strong string.

229
00:22:31,000 --> 00:22:42,000
So only by virtue of the poles that a boat keeps steady in a river with turbulent waters and a rabbit caron.

230
00:22:42,000 --> 00:22:45,000
And it cannot be steady without a pole.

231
00:22:45,000 --> 00:22:46,000
A pole.

232
00:22:46,000 --> 00:22:50,000
So too, here, owing to the weak hole on the object.

233
00:22:50,000 --> 00:22:56,000
Consciousness, when unified, only keeps steady by virtue of weak alcohol applied to us.

234
00:22:56,000 --> 00:22:59,000
And it cannot be steady without applied to us.

235
00:22:59,000 --> 00:23:03,000
Which is why there is only the first chana here, not the second and the right.

236
00:23:03,000 --> 00:23:09,000
So you can get only first chana if you practice the powerness of the body of meditation.

237
00:23:09,000 --> 00:23:11,000
Because you need weak alcohol.

238
00:23:11,000 --> 00:23:16,000
I mean initial application of mine for your mind to be on the object.

239
00:23:16,000 --> 00:23:21,000
Because the object is close or gross and so you need this weak alcohol.

240
00:23:21,000 --> 00:23:23,000
You keep your mind there.

241
00:23:23,000 --> 00:23:27,000
And Vittaga is present only in the first chana.

242
00:23:27,000 --> 00:23:30,000
It is not with sagin of that chana and so on.

243
00:23:30,000 --> 00:23:36,000
So you can get only first chana by this kind of meditation.

244
00:23:36,000 --> 00:23:48,000
And for the dawn, for a graph 88, about four or five, I have read all of the paragraph.

245
00:23:48,000 --> 00:23:53,000
This wholeness, five of ten kinds, has only one characteristic.

246
00:23:53,000 --> 00:23:56,000
Although it is of ten kinds, nevertheless it is characteristic.

247
00:23:56,000 --> 00:24:01,000
It is only its impure, stinking, disgusting, and repulsive state.

248
00:24:01,000 --> 00:24:07,000
And wholeness appears with this characteristic, not only in a dead body, but also in a dead one.

249
00:24:07,000 --> 00:24:09,000
That is a point here.

250
00:24:09,000 --> 00:24:13,000
As it is to Adam, Arthas, Arundit, and J.D.

251
00:24:13,000 --> 00:24:18,000
And Bhavadha, and who, and to the novice, attend and on the Augustan Karakata.

252
00:24:18,000 --> 00:24:21,000
Why he was watching the king riding in Africa.

253
00:24:21,000 --> 00:24:24,000
For a living body is just as foul as a dead one.

254
00:24:24,000 --> 00:24:28,000
Only the characteristic of wholeness is not evident in the living body.

255
00:24:28,000 --> 00:24:32,000
The incident by at 20 years embellishment.

256
00:24:32,000 --> 00:24:41,000
So we watch ourselves, we clean ourselves every day, and say, you will use cosmetics,

257
00:24:41,000 --> 00:24:43,000
bassoons, and so on.

258
00:24:43,000 --> 00:24:50,000
So the characteristic of wholeness is not evident in the living body.

259
00:24:50,000 --> 00:25:04,000
When it is dead, then all the characteristic of wholeness comes evident.

260
00:25:04,000 --> 00:25:21,000
So this can be practiced.

261
00:25:21,000 --> 00:25:24,000
If you want to attain Jana through this practice, then you have to really go to a cemetery

262
00:25:24,000 --> 00:25:34,000
and look at the cause and practice, meditation on it.

263
00:25:34,000 --> 00:25:38,000
But if you want to practice just as a mindfulness of body meditation, then you don't have to go to a cemetery.

264
00:25:38,000 --> 00:25:45,000
But you imagine in your mind, it calls.

265
00:25:45,000 --> 00:25:55,000
And then apply the nature of the dead cause to your own body.

266
00:25:55,000 --> 00:26:00,000
So just as this body is loaded and so on, my body also will not escape this stage.

267
00:26:00,000 --> 00:26:05,000
So you apply the nature of the dead body to the living body.

268
00:26:05,000 --> 00:26:12,000
So that way you can practice satipatana mindfulness meditation.

269
00:26:12,000 --> 00:26:17,000
In that case, you don't have to look for a cause or whatever.

270
00:26:17,000 --> 00:26:22,000
But if you want to practice so that you get Jana through this meditation,

271
00:26:22,000 --> 00:26:29,000
then you have to find a cause or something, which is almost impossible in this country.

272
00:26:29,000 --> 00:26:41,000
But if you go to some medical college, you may be able to see causes there.

273
00:26:41,000 --> 00:26:46,000
But outside there is not allowed, not allowed there.

274
00:26:46,000 --> 00:26:51,000
If you are a special guest that you see because otherwise they won't let you see.

275
00:26:51,000 --> 00:26:54,000
So it is almost impossible.

276
00:26:54,000 --> 00:27:03,000
Now it is even in our country to practice this kind of meditation.

277
00:27:03,000 --> 00:27:12,000
It was only to be the best day to have a problem.

278
00:27:12,000 --> 00:27:14,000
Yeah, it is against the thought.

279
00:27:14,000 --> 00:27:17,000
So we can see this in the animals.

280
00:27:17,000 --> 00:27:26,000
We can see the stages and the animals.

281
00:27:26,000 --> 00:27:34,000
But even that is not not, not so calm, not calm on because hell, hell, obviously I won't let you.

282
00:27:34,000 --> 00:27:41,000
Let the cause be in that stage.

283
00:27:41,000 --> 00:27:51,000
But one thing is, if that won't be easy, we can take pictures of the causes.

284
00:27:51,000 --> 00:27:56,000
But even even it is difficult to take pictures.

285
00:27:56,000 --> 00:28:05,000
You can look at the picture and develop meditation.

286
00:28:05,000 --> 00:28:20,000
So this is a fullness, fullness of the body meditation.

287
00:28:20,000 --> 00:28:34,000
And in our country, when a person dies and is sent out in position, it is sent in the

288
00:28:34,000 --> 00:28:35,000
country.

289
00:28:35,000 --> 00:28:44,000
Please come to say such and such a place to practice a super meditation.

290
00:28:44,000 --> 00:28:56,000
They are not invited to come to a funeral service or to go there.

291
00:28:56,000 --> 00:29:04,000
To go down, to go to a barrier place, where you get that body.

292
00:29:04,000 --> 00:29:11,000
But they are invited to practice a fullness of body meditation along with a santa naka.

293
00:29:11,000 --> 00:29:20,000
So I went to LA this time to perform a funeral ceremony, a funeral service.

294
00:29:20,000 --> 00:29:24,000
One of the one of my devotees died very lately.

295
00:29:24,000 --> 00:29:30,000
So I told people that you are invited here.

296
00:29:30,000 --> 00:29:34,000
Not to come and just look at an old poor lady or whatever.

297
00:29:34,000 --> 00:29:42,000
But you are to look at her and then practice meditation, a fullness meditation, and the

298
00:29:42,000 --> 00:29:53,000
other kind of meditation which we will produce later in the recollection of death.

299
00:29:53,000 --> 00:30:00,000
So at least these two kinds of meditation people should practice when they go to a funeral.

300
00:30:00,000 --> 00:30:05,000
So this is the budget way of attending a funeral service.

301
00:30:05,000 --> 00:30:12,000
So we don't say we are assembled here to pay the last respects to you to the person or

302
00:30:12,000 --> 00:30:13,000
something.

303
00:30:13,000 --> 00:30:22,000
But we come here to practice this meditation and also to apply the nature of this body

304
00:30:22,000 --> 00:30:31,000
and the nature of this person to ourselves and to reduce attachment to ourselves.

305
00:30:31,000 --> 00:30:39,000
And then one other thing is to share marriage with a dead person.

306
00:30:39,000 --> 00:30:46,000
So that is the purpose where people go to a funeral service actually.

307
00:30:46,000 --> 00:30:52,000
But most people actually don't know what to do and why they are invited.

308
00:30:52,000 --> 00:30:56,000
They just go there and they say for some time they go back.

309
00:30:56,000 --> 00:31:06,000
So I told them that in order to be the real followers of the Buddha and we ought to do this.

310
00:31:06,000 --> 00:31:14,000
And we come here and ought to do this type of meditation on yourself and then share marriage with the dead person.

311
00:31:14,000 --> 00:31:20,000
That means I share my marriage with you.

312
00:31:20,000 --> 00:31:32,000
You tell that person with the understanding that he may be somewhere around as a ghost or as a lower celestial being.

313
00:31:32,000 --> 00:31:35,000
So is it a lot of lesson?

314
00:31:35,000 --> 00:31:39,000
No, not blessing.

315
00:31:39,000 --> 00:31:49,000
And we call it a blessing, no, but helping, helping that person in his life, in his life, in his life, in his life.

316
00:31:49,000 --> 00:31:58,000
A person can be reborn as any being, according to teachings of the Buddha.

317
00:31:58,000 --> 00:32:12,000
Although if a person may be a very good man, the whole life, but if he had some bad thoughts at the moment of death, and he could be reborn in a lower war.

318
00:32:12,000 --> 00:32:24,000
So those who are reborn as ghosts, hungry ghosts, and also who are reborn as lower celestial being, can be helped by those from this life.

319
00:32:24,000 --> 00:32:33,000
And that is the basis of the funeral service in the Ramata Buddhism.

320
00:32:33,000 --> 00:32:48,000
So when we share marriage with that person, and that person rejoices at the marriage, then he gets married himself by rejoicing.

321
00:32:48,000 --> 00:32:54,000
And that merit of his actually helps him in his life.

322
00:32:54,000 --> 00:33:00,000
Not the merit of the people here directly giving him the results.

323
00:33:00,000 --> 00:33:06,000
Because the teaching in Buddhism is, you have to do it yourself with that result.

324
00:33:06,000 --> 00:33:10,000
The body can give you any end and with others.

325
00:33:10,000 --> 00:33:19,000
But the act of other people sharing, the new accepting that it's sharing back, changes your consciousness.

326
00:33:19,000 --> 00:33:25,000
That is the merit, the merit he will have.

327
00:33:25,000 --> 00:33:36,000
But the merit here, the merit of those who share becomes a basis for merit for those who accept.

328
00:33:36,000 --> 00:33:56,000
So it is important that going to a funeral of whatever practice, at least, fondness of the body, meditation, and then recollection of that.

329
00:33:56,000 --> 00:34:03,000
Our reflection on that.

330
00:34:03,000 --> 00:34:14,000
Okay, thank you.

331
00:34:14,000 --> 00:34:35,000
Next time.

332
00:34:35,000 --> 00:34:45,000
Oh, no, this is, we're going to continue to be through it and whether it's not in between for this class.

333
00:34:45,000 --> 00:34:49,000
We're going to continue through, we're going to continue through, and then we'll take a little bit of break.

334
00:34:49,000 --> 00:34:52,000
We're going to keep going every night.

335
00:34:52,000 --> 00:34:59,000
But there's no break at all.

336
00:34:59,000 --> 00:35:25,000
So about 20 pages, up to 221.

337
00:35:25,000 --> 00:35:31,000
The next chapter is on the six recollections and up to page 221.

338
00:35:31,000 --> 00:35:37,000
Where the known disc is at the cutoff point.

339
00:35:37,000 --> 00:35:43,000
It gives that page 204.

340
00:35:43,000 --> 00:36:02,000
That's the paragraph 45.

341
00:36:02,000 --> 00:36:21,000
Thank you.

342
00:36:21,000 --> 00:36:40,000
Thank you.

343
00:36:40,000 --> 00:37:00,000
Thank you.

