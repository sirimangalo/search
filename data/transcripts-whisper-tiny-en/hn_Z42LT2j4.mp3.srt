1
00:00:00,000 --> 00:00:03,600
There is a question, is it possible to remove insights completely?

2
00:00:03,600 --> 00:00:09,200
Or is it just starting off off of you, or part of you, or part of you, or is it just part of you?

3
00:00:10,240 --> 00:00:12,160
Yeah, it is a part of you. Can't get rid of it.

4
00:00:13,120 --> 00:00:14,320
Is that what your answer look at us?

5
00:00:15,280 --> 00:00:27,360
Yeah, you can't get rid of it in the sense that

6
00:00:27,360 --> 00:00:34,560
I mean, I mean, I mean, I mean, that you cannot actually like,

7
00:00:34,560 --> 00:00:38,800
period or so, but I think that's what I try to say is.

8
00:00:38,800 --> 00:00:44,640
You can, you can, no, no, no, no, no. You can, you can, you can, you can,

9
00:00:44,640 --> 00:01:08,320
I mean, I mean, if you can't give up anxiety, what is the point?

10
00:01:08,320 --> 00:01:16,320
No, you can't give up anxiety, but the anxiety is a part of your, of your, of your,

11
00:01:16,320 --> 00:01:16,800
why?

12
00:01:16,800 --> 00:01:20,320
Well, yes, but there's no, there's no creation.

13
00:01:20,320 --> 00:01:24,640
Yeah, it's just another, the Buddha said we're like heaps of stuff.

14
00:01:25,600 --> 00:01:28,320
And, you know, if you pull this out of the heap, it's gone.

15
00:01:31,120 --> 00:01:34,560
Okay, like, like, part of the entity.

16
00:01:34,560 --> 00:01:42,320
Well, I do have a question here. In the, the ordination of bikhus,

17
00:01:43,840 --> 00:01:48,320
there is a provision that says that you cannot ordain if what you are like.

18
00:01:48,960 --> 00:01:55,760
I don't know that the term we would use mentally ill in modern language, but there was a

19
00:01:55,760 --> 00:02:00,560
problem to that if you were not sane, you could not ordain.

20
00:02:00,560 --> 00:02:12,480
And so there, I guess there is a distinction between, oh, actual organic, like, problems with

21
00:02:12,480 --> 00:02:21,600
the thought processes through like brain damage and, and, and so on. And the idea is that

22
00:02:21,600 --> 00:02:28,480
someone who actually suffers from like brain damage, well, you can't expect them to change.

23
00:02:28,480 --> 00:02:34,160
Well, first of all, in a, in a terabyte of tradition, anyway, and I'm assuming that it holds

24
00:02:34,160 --> 00:02:39,920
further traditions, they're insanity actually isn't one of the quality disqualifying factors to

25
00:02:39,920 --> 00:02:46,000
ordination strangely enough, although given the, the fact that you need to go through a ceremony,

26
00:02:46,000 --> 00:02:49,920
which, which requires you to answer these questions in the first place, like, are you,

27
00:02:52,320 --> 00:02:57,200
are you a human being, are you a man, right, are you an after die, exactly?

28
00:02:57,200 --> 00:03:04,000
So that is not the kind of questions an insane person could probably answer, but, um, you know,

29
00:03:04,000 --> 00:03:09,600
mental problems are not, there's no mental problem that's a disqualifier to, to ordination.

30
00:03:09,600 --> 00:03:14,800
But the other thing you would have had, that's why I said, uh, in modern language mental illness,

31
00:03:14,800 --> 00:03:22,480
because there was no term back then. Well, there were, there were many rules that where they were

32
00:03:22,480 --> 00:03:31,120
actually, um, the rules were most of the rules or maybe not all the rules are, um, non offenses

33
00:03:31,120 --> 00:03:36,800
for someone who is insane. So you could not only be a monk, but you could break all the rules.

34
00:03:36,800 --> 00:03:41,600
Now, um, the point, the point is that you're temporarily insane or you, you went insane as a monk,

35
00:03:41,600 --> 00:03:46,720
if a person goes insane, they aren't held accountable for breaking rules. They're just insane.

36
00:03:46,720 --> 00:03:54,080
So there was a concept of it, concept of it. Um, but the other thing you could say in response

37
00:03:54,080 --> 00:04:03,600
that it's not really, um, you know, or the rules against ordination are just mainly for the

38
00:04:03,600 --> 00:04:13,360
purposes of, um, keeping the Sangha going and working. Um, well, my point was really coming

39
00:04:13,360 --> 00:04:22,080
to the, it's going back to the anxiety issue, whereas it wouldn't have been acknowledged that

40
00:04:22,080 --> 00:04:29,840
there are some people that cannot control themselves. And, you know, the Dharma really wouldn't

41
00:04:29,840 --> 00:04:35,280
be effective for them. No, I just, it's interesting. I was just, I just finished up with one of my

42
00:04:35,280 --> 00:04:42,160
chapters in my new book, which will come out one, one day. Um, and I was just adding this part in

43
00:04:42,160 --> 00:04:46,960
there. I mean, from my point of view, I'll give you what sort of the lowdown would I wrote there.

44
00:04:47,600 --> 00:04:53,840
Um, you know, we've been talking several times later, or at least at one point, we were talking

45
00:04:53,840 --> 00:04:59,120
about how the body doesn't exist. And so the brain doesn't exist. These are manifestations of

46
00:04:59,120 --> 00:05:04,080
habits and constant effect and so on. And are therefore completely and totally mutable.

47
00:05:04,080 --> 00:05:11,280
Um, so, so we shouldn't, we shouldn't distinguish categorically between, uh, what is, um,

48
00:05:13,360 --> 00:05:19,200
alterable and what is unalterable, what is hardwired and what is not. We, we, we rather have to think

49
00:05:19,200 --> 00:05:24,720
in terms of degrees. So there, there are certain problems which may not be solvable in this life alone,

50
00:05:24,720 --> 00:05:31,200
or put it even broader. There are certain problems that will take a long time to, to solve.

51
00:05:31,200 --> 00:05:40,480
And it may be the death occurs where you're in there and start and finish. But, um, that doesn't

52
00:05:40,480 --> 00:05:46,720
make them categorically different. And so even, even in terms of ordaining such people, people who

53
00:05:46,720 --> 00:05:54,320
have, you know, at least depression or anxiety or, or suicidal tendencies, you know, can be

54
00:05:54,560 --> 00:06:00,400
a very good start for them, even if they, they die not having come to terms with the problem.

55
00:06:00,400 --> 00:06:06,880
Because, you know, put it, put it some doesn't, describe the idea. And, yeah.

56
00:06:08,640 --> 00:06:14,320
Yeah, that's, uh, can I have one thing. There is no such thing as being inside and saying

57
00:06:14,880 --> 00:06:18,480
that these, there are only things that are acceptable inside.

58
00:06:18,480 --> 00:06:24,320
Believe me, there are unacceptable things. There's no such thing as a, sorry.

59
00:06:24,320 --> 00:06:28,240
Oh, there are. No, I've, I've dealt with people who are really, really crazy.

60
00:06:28,240 --> 00:06:35,360
They can't tell the difference between a, a mountain and a molehill, poison and water.

61
00:06:36,160 --> 00:06:42,320
Wow. Well, how is it, Mikey, but first it. I mean, I've got anxiety and I'll be saying,

62
00:06:42,320 --> 00:06:48,080
but I'm all right. I don't know. Insane is, is insane where they really don't have a clue

63
00:06:48,080 --> 00:06:50,000
what they're doing. That really happens.

64
00:06:50,000 --> 00:06:58,080
You know, meditators go and say, there may be degrees and types, but it's quite different from

65
00:06:58,080 --> 00:07:06,640
you. Look, it's trying to add a little bit humor. Oh, like, because for instance,

66
00:07:07,440 --> 00:07:15,200
schizophrenia is a real medical condition. But that's just heard. That's not really.

67
00:07:15,200 --> 00:07:22,320
Wow. No, no, no, no, you have to. I mean, look, it's real. It's real. You can see it on

68
00:07:22,320 --> 00:07:31,520
on brain, brain scans. Don't exist. It exists conventionally. Yeah, it just doesn't exist.

69
00:07:31,520 --> 00:07:39,040
Absolutely. But that's two sides of one coin. You got a coin. Well, the pace of the coin can

70
00:07:39,040 --> 00:07:50,160
exist without the rear, the backside of it. So, this is known as the two truths theory in the

71
00:07:50,160 --> 00:07:56,960
Dharma, where you, if you take the conventional view over the absolute view, that's not right

72
00:07:56,960 --> 00:08:03,280
view. That's not Satya Dita. And if you take the absolute view over conventional view,

73
00:08:03,280 --> 00:08:12,400
it's still not Satya Dita. Why not? Why not? Because we were living, first of all, in a conventional

74
00:08:12,400 --> 00:08:22,240
world. And, you know, all of our absolutist views are theoretical. They are how we view things with

75
00:08:22,240 --> 00:08:28,320
the eye of Bodhi, but not with the actual material eye. With the material eye, we see things

76
00:08:28,320 --> 00:08:37,680
conventionally. With the eye and with them, we may, if we have that, with them, see things

77
00:08:38,560 --> 00:08:48,320
in a enlightened way, which I really, you know, I can only tell you conventional truths.

78
00:08:48,320 --> 00:08:59,040
You know, I don't know about that. In Dharma Prada, the world is mentally blind. How can we

79
00:08:59,040 --> 00:09:07,200
can say that we are so sane? How we know we are sane, just because we say so, just because we

80
00:09:07,200 --> 00:09:14,160
think we are, I mean, if you look at the people, you can drop every day. That person is clearly

81
00:09:14,160 --> 00:09:21,840
insane. That's just a, that's just a, that's just a, you can know that you're insane. Some people

82
00:09:21,840 --> 00:09:28,000
say, hey, what you don't drink? You don't go to the club today? Oh, it must have been something wrong

83
00:09:28,000 --> 00:09:35,360
with you. This is this is postmodernism. That's what it is, which says that you can't really

84
00:09:35,360 --> 00:09:40,640
say that this is better than that or this is right and this is wrong. There's no absolute truth

85
00:09:40,640 --> 00:09:47,280
basically, but there is absolute truth. It's part of reality. Especially, especially, most of

86
00:09:47,280 --> 00:09:54,960
Buddhist, I mean, non, non Buddhist say about Buddhist, they are quite insane. I mean, that's what

87
00:09:54,960 --> 00:10:07,440
like, uh, as it is, I mean, as it is a extension from alcohol, music, some people consider it as

88
00:10:07,440 --> 00:10:13,920
insane. These something wrong with these people. I mean, that's, that's all the, speech opinions.

89
00:10:16,400 --> 00:10:20,720
No, I just mean to say, I agree completely with what you're saying that, you know, schizophrenia

90
00:10:20,720 --> 00:10:29,520
is in a conventional sense real. I was just trying to open and open the understanding to the point

91
00:10:29,520 --> 00:10:38,080
that it doesn't mean anything. It may mean that as long as this brain exists, you're not going to,

92
00:10:38,320 --> 00:10:42,640
I don't even think you could say, not going to become enlightened. I think it's theoretically

93
00:10:44,640 --> 00:10:49,280
possible. I don't know. I mean, it's beyond any of our ability to say whether it's possible

94
00:10:49,280 --> 00:10:54,800
or not. I would, uh, to become enlightened. The person who has schizophrenia, because it's just

95
00:10:54,800 --> 00:11:02,000
a physical realm of the physical realm. It could also be an indication that the mind is so messed up

96
00:11:02,000 --> 00:11:08,160
that the knots are just going to be so tied that 100 years is not enough to become enlightened.

97
00:11:08,160 --> 00:11:12,800
I think we're not in a position to say that, but it doesn't really matter anyway, because

98
00:11:12,800 --> 00:11:18,480
well, then take 200 years or 500 years or, you know, millions can't even say that because none of us

99
00:11:18,480 --> 00:11:25,200
have reached enlightenment, so we don't know how far you are from being enlightened, and then you

100
00:11:25,200 --> 00:11:36,160
know, you know how it was. See, I think that someone who is legitimately insane is, uh, you know,

101
00:11:36,160 --> 00:11:44,480
the type of karma that they create is not really based on like intention, whereas somebody that

102
00:11:44,480 --> 00:11:52,000
is really malevolent and causes harm and rapes and, you know, whatever they do, kills children,

103
00:11:52,000 --> 00:11:59,440
rapes them, and knows what they're doing. You know, someone that commits the five heinous crimes,

104
00:12:00,160 --> 00:12:07,040
you know, they have, according to the Buddha Dharma, a whole different set of karma that they have

105
00:12:07,040 --> 00:12:16,240
to deal with. And, uh, you know, it's interesting as to where that distinction takes place as to

106
00:12:17,120 --> 00:12:25,680
why it's so heinous. Is it because it lacks sanity or is it because it's just so cruel?

107
00:12:27,840 --> 00:12:32,880
Because I really believe someone who is legitimately insane, it's like being legitimately ignorant.

108
00:12:32,880 --> 00:12:41,760
Uh, if you, if an, if you're on an act committed by say a monk that is harmful, it's a lot worse

109
00:12:41,760 --> 00:12:48,160
than committed by someone that's never taken a precept for instance. So I just wanted to throw that in

110
00:12:49,360 --> 00:12:52,240
because I think intention has a lot to do with it.

111
00:12:52,240 --> 00:12:56,560
Well, if you have an desire, can you become a monk if you have an desire?

112
00:12:56,560 --> 00:13:03,280
Absolutely. Become a monk. I'll take care of it.

113
00:13:04,800 --> 00:13:06,240
Can you move to the UK then?

114
00:13:07,600 --> 00:13:09,040
Do you find me in a place? I'm there.

115
00:13:13,040 --> 00:13:16,160
But, uh, you also have to reassure me that I can get a passport.

116
00:13:16,160 --> 00:13:23,200
At least a lifetime visa or something.

