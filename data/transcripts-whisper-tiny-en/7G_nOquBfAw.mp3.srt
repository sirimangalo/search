1
00:00:00,000 --> 00:00:28,900
Good evening everyone, tonight's quote is about wealth.

2
00:00:28,900 --> 00:00:42,880
Many of you probably are aware Buddhism is not a well-thorean to religion, doesn't

3
00:00:42,880 --> 00:00:51,720
place much importance on wealth.

4
00:00:51,720 --> 00:01:04,120
I think of any religion that does, I think Buddhism especially has some very

5
00:01:04,120 --> 00:01:09,600
fairly pointed things to say, or at least has a reputation of

6
00:01:09,600 --> 00:01:21,840
probably being anti-wells, I would think, I mean monks are supposed to be monks who are the

7
00:01:21,840 --> 00:01:28,580
spokespeople of the religion are supposed to live in poverty, they are not allowed to

8
00:01:28,580 --> 00:01:38,280
touch money or valuable objects and gold and jewels.

9
00:01:38,280 --> 00:01:56,540
We have to wear these robes, we have to eat the food that we're given.

10
00:01:56,540 --> 00:02:03,580
Their poverty is a big part of Buddhism, but to himself said,

11
00:02:03,580 --> 00:02:18,580
sent to the Bhramang Dhanang, contentment is the greatest well.

12
00:02:18,580 --> 00:02:31,580
That was unexpected, oh I had my timer set, right, in the diary.

13
00:02:31,580 --> 00:03:00,380
Contentment is the greatest form of wealth.

14
00:03:00,380 --> 00:03:15,340
That's because

15
00:03:15,340 --> 00:03:27,020
what wealth is supposed to do is fulfill our wants, our needs.

16
00:03:27,020 --> 00:03:36,300
So ordinary wealth is able to temporarily fulfill our wants and our desires and our needs,

17
00:03:36,300 --> 00:03:48,500
which our needs anyway are making it an important thing.

18
00:03:48,500 --> 00:03:53,740
So in this quote that we have tonight, the Buddha acknowledges that wealth is good for you,

19
00:03:53,740 --> 00:03:58,180
it can make you happy, it can make your parents happy, it can make your spouse, your children,

20
00:03:58,180 --> 00:04:06,340
your servants and workers happy, your friends and companions happy, and you can use it to

21
00:04:06,340 --> 00:04:21,420
give charity or religious donations, that's what the quote refers to, but charity in general.

22
00:04:21,420 --> 00:04:32,420
So if you do, if obviously poverty is a problem, but the dangers of that wealth is that

23
00:04:32,420 --> 00:04:38,140
it's temporary, it disappears.

24
00:04:38,140 --> 00:04:40,740
Wealth can be incredibly dangerous, right?

25
00:04:40,740 --> 00:04:47,580
More wealth you are, the more trapped you are by it can be, in many ways wealthy people

26
00:04:47,580 --> 00:04:52,100
are trapped or can be trapped by their money.

27
00:04:52,100 --> 00:04:59,380
There was an interesting story read about people who in the lottery that apparently

28
00:04:59,380 --> 00:05:03,340
winning the lottery is one of the worst things that can happen to you.

29
00:05:03,340 --> 00:05:09,140
They look it up, it's quite interesting what happens to people, what the statistics are

30
00:05:09,140 --> 00:05:23,140
for people who have won the lottery, homicide, suicide, a lot of robbery, you lose friends,

31
00:05:23,140 --> 00:05:32,340
you lose your family, lose your life, wealth that kind of wealth can be dangerous, but

32
00:05:32,340 --> 00:05:45,300
even putting that aside, worldly wealth, though it's necessary to maintain livelihood,

33
00:05:45,300 --> 00:05:50,580
and this is where Buddhism does acknowledge the need for wealth, I mean even monks have

34
00:05:50,580 --> 00:05:57,260
to have balls, they have to have things, they have to have tools, the things that allow

35
00:05:57,260 --> 00:06:05,220
them to continue their life.

36
00:06:05,220 --> 00:06:15,020
But beyond that wealth isn't actually able to provide contentment, it isn't actually

37
00:06:15,020 --> 00:06:22,300
able to satisfy, it isn't able to bring happiness, so the happiness that it can bring

38
00:06:22,300 --> 00:06:29,620
is the ability to live one's life without worry, without fear, and potentially without

39
00:06:29,620 --> 00:06:39,060
having to go hungry, without having to be cold or hot, without having to suffer through

40
00:06:39,060 --> 00:06:51,580
lack of the amenities or the necessities, but as far as fulfilling our wants, our desires,

41
00:06:51,580 --> 00:07:00,620
there's no wealth, no amount of wealth that can do that, and so in terms of our wants,

42
00:07:00,620 --> 00:07:08,700
this is why contentment is the greatest form of wealth, because that's what we're looking

43
00:07:08,700 --> 00:07:15,420
for from wealth, we have to understand that the problem is we want something, this is

44
00:07:15,420 --> 00:07:24,060
an issue, when you want something it's actually a problem, if you don't get it, to the

45
00:07:24,060 --> 00:07:29,100
extent that you want it, it's going to cause suffering for you until you get it, if you

46
00:07:29,100 --> 00:07:35,140
want it alive, it can cause great suffering, if you want it a little around just a little

47
00:07:35,140 --> 00:07:40,660
suffering, until you get it, of course once you get it, then you're pleased, but that

48
00:07:40,660 --> 00:07:50,900
pleasure reinforces the desire, and so you want it more, and it becomes more acute, and

49
00:07:50,900 --> 00:07:56,260
eventually you don't get what you want and you suffer, so you're constantly living your

50
00:07:56,260 --> 00:08:07,420
life chasing after your desires, contentment on the other hand accomplishes the same thing,

51
00:08:07,420 --> 00:08:14,300
but it doesn't, by going the other way, by destroying the wanting, when you remove the

52
00:08:14,300 --> 00:08:26,300
wanting without reinforcing it, without following it, then you weaken it, when you can find

53
00:08:26,300 --> 00:08:31,820
peace without the things you want, and you can give up your wants and find happiness

54
00:08:31,820 --> 00:08:38,580
without needing this or that to make you happy, which is a foreign concept to many of us,

55
00:08:38,580 --> 00:08:49,060
we're so used to finding happiness in things and experiences, and it becomes kind of religious

56
00:08:49,060 --> 00:09:01,300
or a view that we have, our belief, and if you challenge that, there's a tendency to

57
00:09:01,300 --> 00:09:09,940
react quite negatively, we're going to upset, you know, again, turned off by the things

58
00:09:09,940 --> 00:09:17,380
you're saying, you say, find happiness by letting go, by not clinging, by giving up your

59
00:09:17,380 --> 00:09:23,540
desires, or if you people will be interested, as I've talked about recently, most of us

60
00:09:23,540 --> 00:09:30,140
don't even understand that statement, again, we can't even comprehend it or so fix, didn't

61
00:09:30,140 --> 00:09:36,460
fixate it on our desires, that, and it hard to even comprehend the idea of being happy

62
00:09:36,460 --> 00:09:42,100
without, it's not maybe that's not giving people enough credit, especially all of you

63
00:09:42,100 --> 00:09:47,860
here, anyone who's watching this, hasn't, I would think, has an understanding, some

64
00:09:47,860 --> 00:09:56,860
understanding of the problem, they've had suffering in their lives, and so they can see how

65
00:09:56,860 --> 00:10:02,620
our wants, when we want things to be a certain way, when we don't get that, it's quite

66
00:10:02,620 --> 00:10:07,580
suffering, quite a bit of suffering, great suffering.

67
00:10:07,580 --> 00:10:17,540
And so we turn to meditation thinking to find contentment, this is really what it is, if

68
00:10:17,540 --> 00:10:22,780
it has an accompany of all of the years, here's an idea, a reason why meditation might

69
00:10:22,780 --> 00:10:34,060
be interesting, is to find contentment, to be able to live without need, so how does it

70
00:10:34,060 --> 00:10:35,060
do this?

71
00:10:35,060 --> 00:10:43,460
Meditation teaches us objectivity, it's like straightening the mind, the mind is bent and

72
00:10:43,460 --> 00:10:49,500
crooked all out of shape, so you have to strike it and straighten it, and every moment

73
00:10:49,500 --> 00:10:53,500
that you see things as they are, you're straightening the mind, not just every moment

74
00:10:53,500 --> 00:10:57,940
you're meditating, that's not the case, but while you're meditating, when you find

75
00:10:57,940 --> 00:11:02,540
a moment where you just see something as it is, when you say rise, and you're just

76
00:11:02,540 --> 00:11:12,140
aware of the rising, that moment is a pure state, and it's straight, the mind is straight

77
00:11:12,140 --> 00:11:22,420
is wholesome, it's pure, when you cultivate this, the cultivation of this state, these

78
00:11:22,420 --> 00:11:34,780
states, this is the path to contentment, it was in that time, in that moment there's no need

79
00:11:34,780 --> 00:11:40,900
for anything, there's no want for anything, when you cultivate these moments after moments

80
00:11:40,900 --> 00:11:49,020
consecutively, you suddenly become content, now you're dealing with so many likes and dislikes,

81
00:11:49,020 --> 00:11:55,140
so it's not easy and it's not very comfortable, that's mostly not content, but in those

82
00:11:55,140 --> 00:12:00,020
moments you can see the contentment, that's what we're fighting for, that's what we're

83
00:12:00,020 --> 00:12:06,580
working for, it takes time, that through practice you can't, this is what you cultivate,

84
00:12:06,580 --> 00:12:18,460
this is the direction that you steer yourself towards, so I don't want to talk too much

85
00:12:18,460 --> 00:12:23,780
about wealth, I don't like talking about money and so on, but contentment, that's for

86
00:12:23,780 --> 00:12:37,820
itself, so there's your number bit for tonight, you know, open up the hangout, if anybody

87
00:12:37,820 --> 00:12:47,660
wants to come on and ask questions, if you don't have any questions, then just say good

88
00:12:47,660 --> 00:12:54,500
night, met a couple of Sri Lankan people on the street today and they're coming tomorrow

89
00:12:54,500 --> 00:13:01,140
morning to visit, it's nice, people in the community, there actually are people on this area,

90
00:13:01,140 --> 00:13:07,900
there's a Sri Lankan monk or Bangladeshian monk in Toronto, said he was going to get

91
00:13:07,900 --> 00:13:16,660
me in touch with the Sri Lankan community here, he never did, he was very busy, and apparently

92
00:13:16,660 --> 00:13:23,660
there's quite a few Sri Lankan people in Hamilton, there's some western people coming

93
00:13:23,660 --> 00:13:32,940
out to meditate, visit, so this place could become a real part of the community, to see

94
00:13:32,940 --> 00:13:41,060
how it goes, I gave meditation booklet to the woman who serves me at Tim Hortons, really

95
00:13:41,060 --> 00:13:50,540
a good group of people at Tim Hortons, that's funny, they've gotten done on them, Robin,

96
00:13:50,540 --> 00:13:55,260
you're echoing me, do you have something else on like YouTube or the audio stream?

97
00:13:55,260 --> 00:14:13,540
I don't know, hey Robin, he doesn't hear me, I don't think, hello hello, I'm sorry,

98
00:14:13,540 --> 00:14:19,300
it's been so long, I forgot that I have to turn the other one off, sorry about that,

99
00:14:19,300 --> 00:14:26,300
so I had a question, so we have a teaching that we hear all the time, we heard it

100
00:14:26,300 --> 00:14:32,620
pretty recently, so let's see, only be seeing, let hearing only be hearing, let's

101
00:14:32,620 --> 00:14:39,140
smelling only be smelling, and I mean it sounds good, but that's my idea, we're going

102
00:14:39,140 --> 00:14:46,100
to have to turn the other one off, sorry about that, I had a question, you're echoing

103
00:14:46,100 --> 00:14:49,340
us, please you have to, if you're going to want to hang out, you have to turn the other

104
00:14:49,340 --> 00:15:03,580
thing off first, let's see, only be seeing, hello, okay, okay, so with this teaching I mean

105
00:15:03,580 --> 00:15:11,060
it sounds good, but my common sense tells me that we have to judge what comes in contact

106
00:15:11,060 --> 00:15:19,100
with our senses to be safe, if you're truly just tasting, tasting, but not judging the

107
00:15:19,100 --> 00:15:25,860
taste, you could be eating rancid food, if you're smelling but not recognizing the smell,

108
00:15:25,860 --> 00:15:34,180
your house could be burning, I mean how does this teaching work in the real world where

109
00:15:34,180 --> 00:15:44,460
we do have to evaluate our surroundings, you know, I mean it's not the whole truth, it's

110
00:15:44,460 --> 00:15:51,740
not like a way of living your life, but it's necessarily that you need to perform during

111
00:15:51,740 --> 00:15:57,940
a meditation practice, because if you don't get to the point where seeing is just seeing

112
00:15:57,940 --> 00:16:07,540
you'll never get to be the root, but I mean even in Iran has to think and has to process

113
00:16:07,540 --> 00:16:15,140
as the talk has to interact with people, has to judge, seeing is not just seeing, seeing

114
00:16:15,140 --> 00:16:26,260
is a pit in front of you, you have to go around, you don't have that in minutes, it's

115
00:16:26,260 --> 00:16:34,180
still seeing, you see the pit and you say seeing, and then there's still the thoughts

116
00:16:34,180 --> 00:16:44,980
that arise based on the pit, they still arise anyway, the point of your mind is straight,

117
00:16:44,980 --> 00:16:51,740
so those thoughts are free from oh my gosh it's a pit, they can still arise and they

118
00:16:51,740 --> 00:17:00,900
still arise, they were in this, so it's only part of the picture, right, those thoughts

119
00:17:00,900 --> 00:17:05,300
of oh that's a pit, well anyway you're not seeing seeing, but when you said seeing is

120
00:17:05,300 --> 00:17:10,860
seeing, when you said seeing to neutralize the experience, at least for a moment you could

121
00:17:10,860 --> 00:17:17,980
still get react, but the first moment of seeing the pit or the tiger or whatever has

122
00:17:17,980 --> 00:17:26,660
neutralized the fear and the shock, I mean that being said, to some extent there is a sense

123
00:17:26,660 --> 00:17:32,980
of having to stay neutral, like of course if it's rancid food you know that it's rancid

124
00:17:32,980 --> 00:17:38,380
food you have no reason to eat it, but if it's a tiger then the tiger's going to eat

125
00:17:38,380 --> 00:17:47,940
you, well you run, once it catches you, see that seeing, just be seeing you, and

126
00:17:47,940 --> 00:17:54,940
pain, just be pain, so you kind of have to go in and out of this, is that, would that

127
00:17:54,940 --> 00:18:05,180
be correct, not even, it's arguable that yeah you'd have to go a little, but it's not

128
00:18:05,180 --> 00:18:12,780
really out of it, because your mind is pure, it's like when you walk, you're walking meditation,

129
00:18:12,780 --> 00:18:16,620
you're only saying step in right, stepping left, but you're aware of a lot else, you're

130
00:18:16,620 --> 00:18:22,620
aware of the shifting of the body, you know, maybe aware of stray thoughts, but you don't

131
00:18:22,620 --> 00:18:27,740
have to note them all, as long as you keep your mind straight, as long as you keep your

132
00:18:27,740 --> 00:18:36,540
mind pure, and so by noting every second or so, you know, frequently your mind will

133
00:18:36,540 --> 00:18:47,780
say stay straight, so it's important to do this, but it's not, it's not everything, especially

134
00:18:47,780 --> 00:18:55,980
of your life, but that being said, when you actually do a meditation, or the best way

135
00:18:55,980 --> 00:18:59,780
to understand it, the easiest way to understand it is during a meditation, if you want

136
00:18:59,780 --> 00:19:04,460
to follow it, where you can follow it perfectly strictly, and you're sitting with your

137
00:19:04,460 --> 00:19:12,980
eyes closed, well that's the time where you're going to look at this, but all it takes

138
00:19:12,980 --> 00:19:19,940
is a moment, and like, I call this story, I told you this story about this monk who is

139
00:19:19,940 --> 00:19:27,180
teaching, I was watching, and it says, daring, hearing, and, you know, if I didn't

140
00:19:27,180 --> 00:19:35,740
say, while he was teaching, probably one of the most impressive things I've ever seen.

141
00:19:35,740 --> 00:19:42,740
So all it was is in that moment for him hearing was just hearing, as he was teaching,

142
00:19:42,740 --> 00:19:46,620
and that's what it was.

143
00:19:46,620 --> 00:19:57,340
My challenge is just to make it enough of a part of my day to ever, to ever get to that

144
00:19:57,340 --> 00:19:58,340
point.

145
00:19:58,340 --> 00:20:06,380
Okay, I'm doing it, but I, one of our recent meditators, finished his course, did okay,

146
00:20:06,380 --> 00:20:14,780
finished it, and was a little bit disappointed, I think, with the results, even though

147
00:20:14,780 --> 00:20:22,420
I was pretty sure he got where he was supposed to be, and you can't tell, it's hard

148
00:20:22,420 --> 00:20:30,260
for him to tell, it's hard for me to tell, but then he emailed me, and he'd gotten stuck

149
00:20:30,260 --> 00:20:38,340
on a flight, and he was on this plane for like nine hours or something, on the runway,

150
00:20:38,340 --> 00:20:47,220
or just an insane amount of time sitting on the runway, and what he described to me what

151
00:20:47,220 --> 00:20:57,620
happened on the runway suddenly he came back, and I said, that sounds exactly.

152
00:20:57,620 --> 00:21:00,500
On the plane, leaving to go home.

153
00:21:00,500 --> 00:21:05,180
You never know when, when you're ready.

154
00:21:05,180 --> 00:21:10,580
You see, it's so stressed during the course, or you get stressed during the course, and

155
00:21:10,580 --> 00:21:17,300
it inhibits, it prevents, and from your worried or near the end it can get stressful, and

156
00:21:17,300 --> 00:21:25,860
if you let it get to you, if you're not mindful, then it just happens when you relax, and

157
00:21:25,860 --> 00:21:30,660
you, it's like Ana, that right, Ana was doing walking meditation all night, trying to

158
00:21:30,660 --> 00:21:38,220
become enlightened, and nothing, and then it was almost done, and he had gotten, he felt

159
00:21:38,220 --> 00:21:42,180
like he got nowhere, and he was really stressed out, and then he thought, and he said,

160
00:21:42,180 --> 00:21:46,900
oh, I'm pushing too hard, and so he laid down, and before his head touched the pillow,

161
00:21:46,900 --> 00:21:49,900
he was not.

162
00:21:49,900 --> 00:22:06,980
Okay, Tom, have a good time, give a question, now, I'm just about to show the flag, alright,

163
00:22:06,980 --> 00:22:12,620
I'm nice to see you, good to see you.

164
00:22:12,620 --> 00:22:21,700
Thank you, Dante, welcome, have a good night, everyone, bye.

