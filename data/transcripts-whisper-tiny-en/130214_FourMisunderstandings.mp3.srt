1
00:00:00,000 --> 00:00:16,440
One of the questions, the commentary to the Satipatanas, which I asked, is why did the Buddha

2
00:00:16,440 --> 00:00:22,440
choose for Satipatanas?

3
00:00:22,440 --> 00:00:24,840
Why are there four Satipatanas?

4
00:00:24,840 --> 00:00:47,280
Because the five aggregates are really the same as the four Satipatanas.

5
00:00:47,280 --> 00:00:55,200
When we are mindful of the body, this is Rupa-kanda, mindfulness of Rupa-kanda.

6
00:00:55,200 --> 00:01:05,200
When we are mindful of the Vedana, this is Vedana, Vedana-kanda.

7
00:01:05,200 --> 00:01:13,840
When we are mindful of the mind of Jitta, this is Vinyana-kanda.

8
00:01:13,840 --> 00:01:26,320
When we are Sanyan, Sankana, which are in the very places, the five hindrances are

9
00:01:26,320 --> 00:01:34,480
Sankana, for example, and it's really all the same.

10
00:01:34,480 --> 00:01:39,080
It's just a different way of explaining the same thing.

11
00:01:39,080 --> 00:01:46,520
And it offers two answers to the question, but one of them is quite interesting and

12
00:01:46,520 --> 00:02:00,880
makes for really good food for thought, for a very practical application of the four

13
00:02:00,880 --> 00:02:17,160
Satipatanas, because we have this teaching on the four Vipalas that the four perversions.

14
00:02:17,160 --> 00:02:25,040
And if it's quite nicely with the four Satipatanas, the four perversions Vipalasa means

15
00:02:25,040 --> 00:02:32,800
looking at something, or seeing something not as it is, other than how it really is.

16
00:02:32,800 --> 00:02:40,200
So it's like when you step on a rope, if you've ever stepped on a rope, then suddenly you

17
00:02:40,200 --> 00:02:42,200
jump because you think it was a snake.

18
00:02:42,200 --> 00:02:49,960
If you ever stepped on a rope in a dark room, it's exactly like stepping on it as you imagine

19
00:02:49,960 --> 00:02:57,480
stepping on the snake would feel it, quite scary, quite a shock.

20
00:02:57,480 --> 00:03:03,920
So this kind of thing, seeing something not as it is, perceiving something other than how

21
00:03:03,920 --> 00:03:06,240
it really is.

22
00:03:06,240 --> 00:03:13,400
But there are four that are truly important and are at the core of the Buddha's teaching.

23
00:03:13,400 --> 00:03:29,000
These are the Sumba Vipalasa, Sukhavibalasa, Nithya Vipalasa and Atavipalasa.

24
00:03:29,000 --> 00:03:40,480
So the perversion in, perversion in regards to beauty, perversion in regards to happiness,

25
00:03:40,480 --> 00:03:50,480
perversion in regards to permanence, and perversion in regards to self.

26
00:03:50,480 --> 00:03:56,560
And these correspond perfectly with the four foundations of mindfulness.

27
00:03:56,560 --> 00:04:03,960
The best way to overcome the idea of beauty or the perversion of perceiving beauty in that

28
00:04:03,960 --> 00:04:10,200
which is ugly is to look at that which is ugly, which is the physical, it's the physical

29
00:04:10,200 --> 00:04:13,200
that we think of is beautiful.

30
00:04:13,200 --> 00:04:22,480
We see beauty in the physical world, and so here comes the Buddhists to tell you that

31
00:04:22,480 --> 00:04:28,760
there's no beauty in the physical world, unfortunately no.

32
00:04:28,760 --> 00:04:37,120
We use the body as a basis for our understanding, overcoming this perversion because the body

33
00:04:37,120 --> 00:04:45,960
is certainly not something beautiful, there's nothing, well there's nothing objectively

34
00:04:45,960 --> 00:04:55,160
beautiful about anything, beauty is something that's subjective to do with the feelings

35
00:04:55,160 --> 00:05:04,600
that it arouses in the mind, the sensations that come from seeing certain things that

36
00:05:04,600 --> 00:05:11,960
perhaps remind us of certain things that are calming to the senses and so on.

37
00:05:11,960 --> 00:05:21,760
They bring feelings of pleasure or feelings of calm to the mind, or to the brain, whatever.

38
00:05:21,760 --> 00:05:26,560
Is opposed to seeing jarring sight.

39
00:05:26,560 --> 00:05:32,760
So the one thing that is most pleasing to the mind, the Buddha said, is the human body.

40
00:05:32,760 --> 00:05:39,400
We find the human body to be quite pleasing, and it reminds us of so many different

41
00:05:39,400 --> 00:05:49,640
pleasant sensations from our parents' embrace, and of course more to the point of this

42
00:05:49,640 --> 00:05:59,240
sexual, dry, and this sexual attraction that comes from physical body.

43
00:05:59,240 --> 00:06:13,080
And it comes from seeing the same body as something that is somehow beautiful.

44
00:06:13,080 --> 00:06:23,160
So, I mean this isn't a teaching to take lightly, it's not a teaching that you can present

45
00:06:23,160 --> 00:06:29,200
to the world at large with much success, and that's because the world at large is

46
00:06:29,200 --> 00:06:36,800
large, has no interest in objectivity, really, and they think of this as something dry

47
00:06:36,800 --> 00:06:55,920
or something, unwelcome, something uninteresting, seeing things just as they are.

48
00:06:55,920 --> 00:07:05,920
And people mostly are happy to have their likes and dislikes and their partialities.

49
00:07:05,920 --> 00:07:17,920
They like to find beauty in things and their for ugliness and other things.

50
00:07:17,920 --> 00:07:23,880
Then we can't hide the truth, and the very truth is that happiness doesn't come from

51
00:07:23,880 --> 00:07:30,440
beauty, it doesn't come from the appreciation of beauty, the appreciation of beauty only

52
00:07:30,440 --> 00:07:38,080
leads to addiction, it's something that might for a short time, even a whole lifetime,

53
00:07:38,080 --> 00:07:44,360
which is of course a short time, it might lead to pleasurable states, because of how the

54
00:07:44,360 --> 00:08:00,680
mind reacts to the experience, it gives rise to pleasure, chemicals in the brain provide

55
00:08:00,680 --> 00:08:06,600
us this stimulus saying that's beautiful, that's good, that makes me feel good inside,

56
00:08:06,600 --> 00:08:13,440
or warm and fuzzy, or when you see the human body, when you see a beautiful body, it

57
00:08:13,440 --> 00:08:22,160
leads to hormones and very strong chemical reactions, which then are taken as pleasurable

58
00:08:22,160 --> 00:08:36,240
and lead to forced addiction, the sexual desire and central desire and so on.

59
00:08:36,240 --> 00:08:44,120
So all of this is the point is that, especially with beauty, it's a cause for greatness and

60
00:08:44,120 --> 00:08:48,920
great stress, you think of how much work we have to go through in this world, just to

61
00:08:48,920 --> 00:09:09,320
be able to enjoy beauty, and how our intense addiction to pleasurable stimuli, in terms

62
00:09:09,320 --> 00:09:16,760
of beauty, even immediate, the need for immediate beauty, it's actually degrading the

63
00:09:16,760 --> 00:09:24,000
beauty of the world around us, the beauty of the environment, for example, the natural

64
00:09:24,000 --> 00:09:28,920
environment is something that is very calming to the mind if something that would have

65
00:09:28,920 --> 00:09:40,320
given our ancestors a great amount of pleasure to behold, but nowadays our unending quest

66
00:09:40,320 --> 00:09:59,760
for immediate pleasure, which brings us high definition televisions and movies and music

67
00:09:59,760 --> 00:10:10,320
videos and so on, it brings us pornography and all of these things causing us to lose sight

68
00:10:10,320 --> 00:10:20,280
of what's important, our need for intense experience, it's causing us to know most people

69
00:10:20,280 --> 00:10:24,640
now, don't go out and look at the stars anymore, they sit inside and watch television

70
00:10:24,640 --> 00:10:32,800
or use the computer, of course all the beautiful, stimulating visual stimuli, stimulants

71
00:10:32,800 --> 00:10:47,800
on the internet, pictures, videos, YouTube and so on, when I was young, we still went

72
00:10:47,800 --> 00:10:53,400
out and looked at the stars, it's a tent and go and look at the stars and watch the

73
00:10:53,400 --> 00:10:57,360
stars, then we don't do that so much, and we're destroying the environment and it's

74
00:10:57,360 --> 00:11:08,240
making and becoming hotter and so on, so you can see how we, our life degrades through

75
00:11:08,240 --> 00:11:14,320
our addictions, especially to the human body, how much work we have to do, people have

76
00:11:14,320 --> 00:11:23,040
to now work 40, 50 hours a week, even more sometimes just to support their family, for

77
00:11:23,040 --> 00:11:28,960
example, you get married and you have to support a family, you need a big house and so

78
00:11:28,960 --> 00:11:42,360
on, all in order to pay for our central addiction, so I mean this isn't something to preach

79
00:11:42,360 --> 00:11:50,160
this evil and how bad we are, but it is something to give us our instrument to give up

80
00:11:50,160 --> 00:11:57,160
this kind of addiction, that hey, maybe if we could give up whatever addiction we have

81
00:11:57,160 --> 00:12:04,400
to beauty, for example, maybe we can be more content, certainly we can be more content

82
00:12:04,400 --> 00:12:09,720
and it would save us a lot of hassle and a lot of trouble, we could live simpler, we

83
00:12:09,720 --> 00:12:16,000
wouldn't need a high paying job, or we wouldn't need to go into debt, we wouldn't need

84
00:12:16,000 --> 00:12:23,360
to stress and we wouldn't need to do evil deeds, we wouldn't need to trick others and

85
00:12:23,360 --> 00:12:30,160
other people and cheat and deceive others in order to get what we want, we wouldn't

86
00:12:30,160 --> 00:12:37,320
fight with our siblings or our spouses or our children or our parents, we wouldn't fight

87
00:12:37,320 --> 00:12:46,480
so much because we wouldn't be partial to this or that experience, when it's time to

88
00:12:46,480 --> 00:12:57,840
take out the trash we wouldn't wrinkle up our nose, we'd just take out the trash, so

89
00:12:57,840 --> 00:13:02,560
giving up beauty, well first of all it doesn't mean not having beauty in life, it just

90
00:13:02,560 --> 00:13:09,240
means not being attached to it, not putting labels on things, many people like to think

91
00:13:09,240 --> 00:13:14,440
of this as finding beauty in everything, it's not really the case, you don't find beauty

92
00:13:14,440 --> 00:13:20,840
in anything really, but you don't need beauty, the problem, people always have such a

93
00:13:20,840 --> 00:13:26,520
big problem with teachings like this, but the fact that they have a problem with them

94
00:13:26,520 --> 00:13:38,680
really is an example of the problem, this partiality, when someone tells you, if I were

95
00:13:38,680 --> 00:13:42,200
to tell you stop, you're no longer allowed to look at beautiful things, you'd be upset,

96
00:13:42,200 --> 00:13:47,200
most people would be quite upset, which is really the point, a person who doesn't need

97
00:13:47,200 --> 00:13:51,880
beauty will never get upset when they have no beauty, it doesn't mean they're just

98
00:13:51,880 --> 00:13:58,760
a blind robot, but it means they always have peace in their minds, when they experience

99
00:13:58,760 --> 00:14:03,080
beauty, they have peace in their minds, when they experience ugliness, they have peace

100
00:14:03,080 --> 00:14:11,160
in their mind, so it's a useful thing to be able to free yourself, it's not becoming

101
00:14:11,160 --> 00:14:17,800
a flat line, you see, it's freeing yourself from the need for things, so that you can

102
00:14:17,800 --> 00:14:21,680
still experience these things, you can even experience all the pleasure that comes from

103
00:14:21,680 --> 00:14:27,000
beauty, but you don't care for it, you aren't partial towards it, even when there's great

104
00:14:27,000 --> 00:14:34,040
pleasure in the mind, you see it simply as it's experienced and arises in Jesus, and so

105
00:14:34,040 --> 00:14:41,680
the same with ugliness and the displeasure or the disharmony that arises, the pain that arises

106
00:14:41,680 --> 00:14:51,160
from seeing other things, and you don't see it as pain, but you don't see it as bad.

107
00:14:51,160 --> 00:15:00,360
So this is why the practice is to use mindfulness of the body, so even just watching the

108
00:15:00,360 --> 00:15:08,360
movements of the body helps us to break down this idea of there being an entity that

109
00:15:08,360 --> 00:15:18,600
we call the body, the body being something concrete or whole, when we see that when we

110
00:15:18,600 --> 00:15:25,720
lose this concept of the body, it really takes away the idea of something to worry about

111
00:15:25,720 --> 00:15:38,000
as being beautiful or ugliness, it helps us to see that in any case the body is the Buddha

112
00:15:38,000 --> 00:15:47,000
is at a nest of sores, it can be a cause for great suffering, and so for partial towards

113
00:15:47,000 --> 00:15:56,720
the good positive sensations, it will be partial against the unpleasant ones.

114
00:15:56,720 --> 00:16:03,080
So this is the idea that when we're actually mindful of the body, we won't be so attracted

115
00:16:03,080 --> 00:16:09,480
to the human body, we'll see that it's just a series of experiences, it's not really

116
00:16:09,480 --> 00:16:15,600
an entity when you start, and you slowly, through the watching the body, you slowly

117
00:16:15,600 --> 00:16:25,080
lose your attachment to beauty, to the physical beauty, at the body is a beautiful thing.

118
00:16:25,080 --> 00:16:35,560
The first one, the second one is happiness, the perversion of finding happiness, and this

119
00:16:35,560 --> 00:16:41,400
is overcome through the practice of Vedana and the person, and this is of course even worse.

120
00:16:41,400 --> 00:16:46,600
Now, first I'm telling you to give up beauty, and next we're telling you to give up happiness,

121
00:16:46,600 --> 00:16:47,600
right?

122
00:16:47,600 --> 00:16:55,000
It just gets worse and worse, but again that's the point is when you need happiness,

123
00:16:55,000 --> 00:17:01,800
no, not when you need happiness, when you believe that pleasure, pleasurable feelings

124
00:17:01,800 --> 00:17:09,800
are happiness, and then you're always going to be partial towards them, upset when you

125
00:17:09,800 --> 00:17:10,800
can't have them.

126
00:17:10,800 --> 00:17:18,680
So if I told you, you're not allowed to enjoy or chase after pleasant feelings, most

127
00:17:18,680 --> 00:17:20,440
people would be quite upset about that.

128
00:17:20,440 --> 00:17:27,160
I think, well how can I be happy then, and if they were, most people when they're without

129
00:17:27,160 --> 00:17:33,360
these feelings for some amount of time, they become quite upset, they become bored,

130
00:17:33,360 --> 00:17:48,840
they become frustrated, they become angry, and they become displeased, and hard to deal with.

131
00:17:48,840 --> 00:17:55,560
So in this way we're much like drug addicts, we're addicted to our pleasure, so we find

132
00:17:55,560 --> 00:18:01,720
ways to obtain our pleasurable feelings.

133
00:18:01,720 --> 00:18:06,960
We get quite good at it actually, if you study yourself, you can see where you, throughout

134
00:18:06,960 --> 00:18:12,920
the day where you try to find pleasure, and where you do things for the purpose of finding

135
00:18:12,920 --> 00:18:18,360
pleasure, and if you watch other people you can watch them as well, trying to find their

136
00:18:18,360 --> 00:18:23,560
pleasure throughout the day, this leads to this, okay now it's time for this, why, because

137
00:18:23,560 --> 00:18:26,120
it's going to make you me pleasure.

138
00:18:26,120 --> 00:18:29,280
We take it for granted, we think well of course, because that's what happiness comes

139
00:18:29,280 --> 00:18:39,880
with, and this is the misperception, the perversion, perverted perception, this means

140
00:18:39,880 --> 00:18:43,880
not seeing things as they are, because there's nothing positive about pleasant feelings

141
00:18:43,880 --> 00:18:48,000
at all.

142
00:18:48,000 --> 00:18:57,320
The moment that you find them positive that you find them somehow better than other

143
00:18:57,320 --> 00:19:05,880
feelings, you become partial towards them, and you become dependent on them, to the extent

144
00:19:05,880 --> 00:19:17,080
that this can lead to a real addiction, where when we don't get what we want we become

145
00:19:17,080 --> 00:19:25,240
frustrated, we become upset, and become difficult to deal with, fight with other people,

146
00:19:25,240 --> 00:19:32,840
we argue, if you ever found yourself being indulging in pleasure, it's interesting how

147
00:19:32,840 --> 00:19:36,080
when you're indulging, when you're finding pleasure in something, you're so pleased by

148
00:19:36,080 --> 00:19:42,120
then you're interrupted, how quickly we become angry, isn't interesting, if it really

149
00:19:42,120 --> 00:19:48,080
made you a happier person, why would you get angry so quickly?

150
00:19:48,080 --> 00:19:52,000
Compare the two, a person who doesn't, who isn't addicted to things, when they're

151
00:19:52,000 --> 00:19:57,920
interrupted, they're at peace, and they weren't interrupted, things just change, and

152
00:19:57,920 --> 00:20:05,000
they're able to deal with the change, but people who enjoy pleasure so much, very easily

153
00:20:05,000 --> 00:20:09,120
annoyed when they don't get what they want, so we just get better and better throughout

154
00:20:09,120 --> 00:20:15,960
our lives of chasing the pleasure, and finding ways to obtain pleasure throughout our

155
00:20:15,960 --> 00:20:25,080
day and throughout our lives, and so we find this comfort zone, and we think we've got

156
00:20:25,080 --> 00:20:29,960
it made, and we forget about all the suffering that we had to go through just to get to

157
00:20:29,960 --> 00:20:36,560
where we are today, it's very easy to forget, we're good at forgetting things, forgetting

158
00:20:36,560 --> 00:20:42,960
our pain, all we're going to remember in it as well, depends on the person, it depends

159
00:20:42,960 --> 00:20:48,360
on the experience, but we're good at forgetting all the lessons that we had today,

160
00:20:48,360 --> 00:20:55,920
we're forgetting the fact that forgetting how hard it was to get where we are, forgetting

161
00:20:55,920 --> 00:21:00,640
about the future, forgetting about death, forgetting about all the people we've seen

162
00:21:00,640 --> 00:21:06,480
who died miserable deaths, and how we might be one of them, because that's the other thing

163
00:21:06,480 --> 00:21:12,440
when you're addicted to pleasure, it gets a lot more difficult to deal with death, dying,

164
00:21:12,440 --> 00:21:22,200
old age, some people cry like babies when they die, or we might be lucky when we might

165
00:21:22,200 --> 00:21:28,800
die in peace, but we have to come back and do it all over again, the more lust, the more

166
00:21:28,800 --> 00:21:35,360
on desire we have in the mind, the more attachment to pleasant feelings we have, the more

167
00:21:35,360 --> 00:21:39,200
hungry we will be when we come back, this is why people are born as, beings are born

168
00:21:39,200 --> 00:21:46,760
as ghosts, because they die with much attachment, and this is why they haunt ghosts, haunt

169
00:21:46,760 --> 00:21:53,040
certain areas, because of their attachment to certain people and so on.

170
00:21:53,040 --> 00:22:03,440
You know, there's nothing positive about pleasant feelings, there's nothing wrong with

171
00:22:03,440 --> 00:22:11,240
them, doesn't mean that we practice again to never experience pleasant feelings, that

172
00:22:11,240 --> 00:22:16,040
become to be free from our need from them, or we become free, whatever you say about

173
00:22:16,040 --> 00:22:21,680
a person who gives up these things, you've got to admit that they're free, they're no

174
00:22:21,680 --> 00:22:25,040
longer a slave to their desires.

175
00:22:25,040 --> 00:22:33,080
This is really the key benefit, freedom, right, and freedom is, if you read the Buddhist

176
00:22:33,080 --> 00:22:36,280
teaching, you see it's all about freedom, and that's really what this is, freedom from

177
00:22:36,280 --> 00:22:46,120
you, say, not being a slave to beauty, not being a slave to pleasure, and the mindful

178
00:22:46,120 --> 00:22:51,960
of different feelings will come to see this, or acknowledge the happy, happy, or pleasure

179
00:22:51,960 --> 00:22:59,280
for being knowledge of pain as well, acknowledge the neutral feelings calm, calm.

180
00:22:59,280 --> 00:23:05,160
We come to see, realize the feelings that are all just feeling, they come and they go, sometimes

181
00:23:05,160 --> 00:23:10,360
they're happy feeling, sometimes they're painful feelings, and we come to be free from

182
00:23:10,360 --> 00:23:24,240
any partiality or any need for one type of feeling or another, the third is the perversion

183
00:23:24,240 --> 00:23:35,880
of permanence, permanence here meaning last something lasting, the idea of a lasting being

184
00:23:35,880 --> 00:23:43,680
or a lasting entity, or lasting anything really, for example, the idea that the body is

185
00:23:43,680 --> 00:23:52,840
an entity that persists for a moment to moment.

186
00:23:52,840 --> 00:23:57,040
Mindfulness of the mind, why mindfulness of the mind helps with this is because nothing

187
00:23:57,040 --> 00:24:03,880
lasts longer than the mind, no reality lasts longer than the mind that is aware of it.

188
00:24:03,880 --> 00:24:12,040
Well, according to the domain, technically, physical objects last for 17 mind moments

189
00:24:12,040 --> 00:24:17,560
or something like this, or maximum, but really it's not really reality, the reality is

190
00:24:17,560 --> 00:24:26,960
the experience, in one moment there is the physical and the mental, so these 17 thought

191
00:24:26,960 --> 00:24:35,400
moments, you say it's the same rupa, 17 namas, one rupa, but it's actually 17 rupas as well,

192
00:24:35,400 --> 00:24:42,240
just it's the same type of rupa, the same characteristic, because in that moment there is

193
00:24:42,240 --> 00:24:48,720
rupa and there is namas, now this isn't exactly how the happy domain explains it, experiences

194
00:24:48,720 --> 00:24:57,920
only momentary, so reality is only momentary.

195
00:24:57,920 --> 00:25:01,920
When your mindful of the mind, when you watch the mind during the practice, only then

196
00:25:01,920 --> 00:25:08,920
you can see how one mind arises another mind, one mind ceases another mind arises, sometimes

197
00:25:08,920 --> 00:25:15,720
totally disconnected, it's easy to see as you practice practically speaking, because

198
00:25:15,720 --> 00:25:19,840
you'll see that the mind just coming out of nowhere is something totally unconnected

199
00:25:19,840 --> 00:25:30,440
to the past mind, you'll see how judgments and ideas arise in the mind, seeming out

200
00:25:30,440 --> 00:25:46,120
of nowhere, and sometimes at random and sometimes chaotic things, sometimes overwhelmingly,

201
00:25:46,120 --> 00:25:53,240
so when your mindful of the mind thinking, thinking, knowing, knowing as well, you catch

202
00:25:53,240 --> 00:25:58,160
the mind thinking, you can see the mind arising, you see the mind too, you see the thoughts

203
00:25:58,160 --> 00:26:06,320
arising, you see, you come to see how impermanent reality is, and of course when we're mindful

204
00:26:06,320 --> 00:26:12,320
of the body and the feelings as well, because the mind arises and ceases with the object,

205
00:26:12,320 --> 00:26:20,160
this is why we see impermanent, when we realize that there is no mind or being or soul

206
00:26:20,160 --> 00:26:26,920
that lasts, that is eternal, this idea of an eternal soul is just a concept, it does nothing

207
00:26:26,920 --> 00:26:32,240
to do with reality, you can believe in one all you want, you can believe in God, you

208
00:26:32,240 --> 00:26:38,560
can believe in whatever, but they're just belief, they have no basis in reality, reality

209
00:26:38,560 --> 00:26:52,600
doesn't admit of such things, this is the third one, the fourth is perversion in regards

210
00:26:52,600 --> 00:27:02,120
to self, the idea that there is not now a lasting self, but a controlling self, the acting

211
00:27:02,120 --> 00:27:10,280
self, and so the dhamma is mindfulness of the dhamma, if you look at the dhamma in the

212
00:27:10,280 --> 00:27:17,440
sati batanas of days, easy to understand how this works, so first of all the five hindrances,

213
00:27:17,440 --> 00:27:24,160
not exactly directly related to the perception of a non-self, but very helpful, because

214
00:27:24,160 --> 00:27:31,400
the five hindrances are a good part of our emotions, and so we think of emotions as being

215
00:27:31,400 --> 00:27:38,520
me and mine, I am the one who gets angry, I am the one who wants things, I am the one

216
00:27:38,520 --> 00:27:47,400
who doubts when there's doubt and so on, and when we look at these things and see them

217
00:27:47,400 --> 00:27:54,880
just as they are, as a liking, liking, disliking, drowsy, drowsy, distracted, doubting,

218
00:27:54,880 --> 00:27:59,160
doubting, worrying and afraid and so on, all of these things that we thought were ours,

219
00:27:59,160 --> 00:28:07,080
we see that they're not, we're not giving rise to them, we're not the ones getting angry,

220
00:28:07,080 --> 00:28:12,680
sometimes people, no matter how much they acknowledge anger, it just bubbles up, and

221
00:28:12,680 --> 00:28:19,400
oils over, it's impossible to control, and they think they're doing something wrong and

222
00:28:19,400 --> 00:28:23,960
they think they're not capable, or meditations are not helping, sometimes come with these

223
00:28:23,960 --> 00:28:28,280
doubts that, and this meditation is not working, they keep my knowledge, angry, angry,

224
00:28:28,280 --> 00:28:35,120
and just get more angry, so they try to blame the noting as bringing the mind, which

225
00:28:35,120 --> 00:28:42,560
of course isn't the case, it's something that is cooped up inside, and has been building

226
00:28:42,560 --> 00:28:52,240
up inside and this habit, I'm getting angry, or greedy, or whatever, habits and doubting

227
00:28:52,240 --> 00:29:00,280
very, very, we're still going to tricking ourselves, these five hindrance, as we trick

228
00:29:00,280 --> 00:29:09,160
ourselves with them, or actually not even we trick ourselves, these are habits that form

229
00:29:09,160 --> 00:29:15,400
based on cause and effect, they're not under anyone, they're not under our control,

230
00:29:15,400 --> 00:29:20,920
they don't belong to us, they just come and they go, but then of course you have the

231
00:29:20,920 --> 00:29:27,280
five aggregates are also here, the six senses are also here, they endame you have many

232
00:29:27,280 --> 00:29:39,400
things that are useful for giving up the idea of self, the point of non-self, of realizing

233
00:29:39,400 --> 00:29:46,560
that self is just illusion or a perversion of perception, is that you can't really control

234
00:29:46,560 --> 00:29:57,120
things, the mind that intends to control creates a sankara, creates an artificial formation

235
00:29:57,120 --> 00:30:04,680
and something that seems like control, but it's actually just stress and pressure that

236
00:30:04,680 --> 00:30:11,680
builds up and builds up and then collapses, the mind that wants to control does not get

237
00:30:11,680 --> 00:30:24,200
angry, for example, just bottles up the anger, reacts to the anger, forces the forces

238
00:30:24,200 --> 00:30:32,960
the reaction to the anger, instead of blowing up and yelling and implodes the reacts

239
00:30:32,960 --> 00:30:40,080
by hurting suffering from itself, reacts to the anger again and again and again, it doesn't

240
00:30:40,080 --> 00:30:49,280
actually stop the anger, for example, or the greed of the wanting, when monks don't practice

241
00:30:49,280 --> 00:30:53,400
meditation, then wanting to just get bottled up and bottled up and eventually it spills

242
00:30:53,400 --> 00:31:00,080
over and you can't escape it.

243
00:31:00,080 --> 00:31:06,640
This is why we have people acknowledge the anger, acknowledge the desire, really just

244
00:31:06,640 --> 00:31:11,800
live through it, let it come up, really have to let it come up, it's really the best thing

245
00:31:11,800 --> 00:31:17,600
you can do for yourself, to just let it be and watch it and understand it, because

246
00:31:17,600 --> 00:31:23,080
as soon as you do, that moment it's gone, that moment there's mindfulness, very quick

247
00:31:23,080 --> 00:31:28,880
to disappear, and the problem is it comes back and we become easily discouraged because

248
00:31:28,880 --> 00:31:32,720
we think it's having no effect, but that's exactly the effect, it's showing you that

249
00:31:32,720 --> 00:31:38,360
it's not under control, of course it comes back, it's not yours, it's not like you can

250
00:31:38,360 --> 00:31:44,440
control it and poof it's gone, comes back because it's a habit, and so you watch it

251
00:31:44,440 --> 00:31:49,480
more and more and you see, this isn't me, this isn't mine, I'm not even control here

252
00:31:49,480 --> 00:31:57,040
and you give it up, you stop encouraging it, you stop chasing after it, you stop giving

253
00:31:57,040 --> 00:32:03,280
it power and feeding it, then it's slowly waste away, as you see that you can't control,

254
00:32:03,280 --> 00:32:07,880
you see that yes, when I acknowledge it, it doesn't go, it goes away, but it doesn't,

255
00:32:07,880 --> 00:32:13,160
that's not controlling it, it comes back and you have to acknowledge again, you have

256
00:32:13,160 --> 00:32:22,920
to clear your mind, stop your mind from building it, creating more of it, until you see

257
00:32:22,920 --> 00:32:31,920
that it actually is not worth doing, so these are the four perceptions, the four

258
00:32:31,920 --> 00:32:36,640
perversions of perception, the commentary says that, those are a reason for using the

259
00:32:36,640 --> 00:32:41,440
Satyapatana, I just thought this, something useful for us to think about when we practice

260
00:32:41,440 --> 00:32:47,800
or before we practice, clearing up what it means to practice, or so when we practice

261
00:32:47,800 --> 00:32:55,400
we're just mindful, but we just use the acknowledgement, but it helps often clear up doubts

262
00:32:55,400 --> 00:33:05,200
to understand why this is such an important thing, what it's doing for us, why it's useful,

263
00:33:05,200 --> 00:33:14,360
that's the number for today, just more, useful knowledge that we can put into practice,

264
00:33:14,360 --> 00:33:43,000
so we'll then we'll practice together.

