1
00:00:00,000 --> 00:00:07,000
Hello, and the next question comes from C.C. Pathless Assist.

2
00:00:07,000 --> 00:00:11,000
Would you maybe talk a bit about having children in relation to Buddhism?

3
00:00:11,000 --> 00:00:14,000
I know it will bring feelings, attachment, and so on.

4
00:00:14,000 --> 00:00:17,000
What can you tell about this?

5
00:00:17,000 --> 00:00:19,000
Yeah?

6
00:00:19,000 --> 00:00:24,000
The first thing, obviously, to say, is from a monks point of view,

7
00:00:24,000 --> 00:00:31,000
you know, don't have a children if you can at all help it.

8
00:00:31,000 --> 00:00:36,000
And, of course, the argument right away is, well, if nobody had children,

9
00:00:36,000 --> 00:00:41,000
there would be no more humans, and then we get into this whole debate about whether it's useful somehow

10
00:00:41,000 --> 00:00:47,000
for this world to be populated with humans, whether the human existence is somehow beneficial.

11
00:00:47,000 --> 00:00:52,000
But it's really a silly argument in terms of today's world,

12
00:00:52,000 --> 00:00:55,000
because there's enough people already.

13
00:00:55,000 --> 00:01:02,000
If we were, for some reason, lacking of people of humans in this world,

14
00:01:02,000 --> 00:01:05,000
then maybe you could say, yeah, we have to bring some more into the world.

15
00:01:05,000 --> 00:01:09,000
But considering there's so much work that has to be done just to support each other,

16
00:01:09,000 --> 00:01:17,000
just to develop humanity as it is,

17
00:01:17,000 --> 00:01:22,000
they don't really have that argument.

18
00:01:22,000 --> 00:01:29,000
I guess the argument is more personal that you want someone to take care of or so on.

19
00:01:29,000 --> 00:01:40,000
You know, somehow you feel the need to have children for some reason or other.

20
00:01:40,000 --> 00:01:46,000
Once you do have children, how to take care of them because you can't send them back, obviously.

21
00:01:46,000 --> 00:01:50,000
And so it becomes imperative for you to take care of them.

22
00:01:50,000 --> 00:01:56,000
It's the duty of parents to teach their children to stay away from evil,

23
00:01:56,000 --> 00:01:59,000
to tell them what is good.

24
00:01:59,000 --> 00:02:04,000
One of the most important lessons that I've found in regards to parents and children

25
00:02:04,000 --> 00:02:09,000
is that as parents, you should never think that your children aren't listening to you.

26
00:02:09,000 --> 00:02:13,000
You should never become frustrated when you say something again and again and again

27
00:02:13,000 --> 00:02:17,000
that children still don't seem to respond.

28
00:02:17,000 --> 00:02:21,000
You have to understand that they're not you and they're not yours.

29
00:02:21,000 --> 00:02:25,000
The children are people in and of themselves.

30
00:02:25,000 --> 00:02:27,000
They're human beings and they're on their own path.

31
00:02:27,000 --> 00:02:31,000
They came of their own volition and they'll go of their own volition.

32
00:02:31,000 --> 00:02:35,000
We aren't in control of their lives.

33
00:02:35,000 --> 00:02:39,000
That being said, from what I've seen,

34
00:02:39,000 --> 00:02:44,000
most children do keep a very good record of what their parents have told them,

35
00:02:44,000 --> 00:02:51,000
but because of their own mental problems,

36
00:02:51,000 --> 00:02:54,000
the things that are going on in their minds,

37
00:02:54,000 --> 00:02:58,000
they're not able to process and they're not able to respond

38
00:02:58,000 --> 00:03:02,000
and to obviously be subservient all the time.

39
00:03:02,000 --> 00:03:05,000
But they do keep in mind that once they're able,

40
00:03:05,000 --> 00:03:10,000
once they've given up and they've gained some sense of responsibility

41
00:03:10,000 --> 00:03:13,000
and they're able to tell the difference between right and wrong.

42
00:03:13,000 --> 00:03:17,000
The teachings that parents have given their children will become invaluable.

43
00:03:17,000 --> 00:03:25,000
So the Buddha said parents are the first teachers and I think that is the best role one can have

44
00:03:25,000 --> 00:03:27,000
in relation with one's children.

45
00:03:27,000 --> 00:03:29,000
One shouldn't be attached to them.

46
00:03:29,000 --> 00:03:32,000
Obviously from a Buddhist point of view, one should understand

47
00:03:32,000 --> 00:03:35,000
they are human beings, they're going to have their own lives.

48
00:03:35,000 --> 00:03:41,000
And my duty, which if I fail to do, I fail to do my duty,

49
00:03:41,000 --> 00:03:47,000
is to teach them and to give them information with which they can do whatever they like,

50
00:03:47,000 --> 00:03:52,000
but give them information that's useful, that's true, that's a wholesome

51
00:03:52,000 --> 00:03:57,000
and that teaches them the difference between what is right and what is wrong

52
00:03:57,000 --> 00:04:08,000
and helps them to make decisions and to have the information available to them.

53
00:04:08,000 --> 00:04:17,000
Other than that, I would say try to let them live their lives,

54
00:04:17,000 --> 00:04:24,000
try not to think of them as our possessions or as ours

55
00:04:24,000 --> 00:04:29,000
and try to use it as an objective meditation.

56
00:04:29,000 --> 00:04:34,000
Our children can be very frustrating at times,

57
00:04:34,000 --> 00:04:43,000
can be very addictive at times where we have feelings of great attachment towards them.

58
00:04:43,000 --> 00:04:52,000
And from a secular point of view, this is genetic and something natural

59
00:04:52,000 --> 00:04:55,000
and it's something that we should cultivate.

60
00:04:55,000 --> 00:04:59,000
But from a Buddhist point of view, it's a constructed phenomenon.

61
00:04:59,000 --> 00:05:02,000
It's something that we've developed from lifetime after lifetime

62
00:05:02,000 --> 00:05:06,000
and something that we've ingrained into ourselves artificially.

63
00:05:06,000 --> 00:05:10,000
And so the addiction, there's nothing special about it.

64
00:05:10,000 --> 00:05:14,000
It's something that is not helpful to us, not helpful to the children.

65
00:05:14,000 --> 00:05:20,000
It's not something that allows them spiritual development and freedom.

66
00:05:20,000 --> 00:05:27,000
So that's an objective meditation for us, something that we should consider very seriously

67
00:05:27,000 --> 00:05:29,000
and try to see objectively.

68
00:05:29,000 --> 00:05:35,000
And not say, I'm a mother, I'm a father, and therefore it's right for me to feel this way.

69
00:05:35,000 --> 00:05:39,000
Right is just a judgment call. You say it's right.

70
00:05:39,000 --> 00:05:42,000
Therefore it's right. It's really meaningless.

71
00:05:42,000 --> 00:05:44,000
The point is whether it's useful or not.

72
00:05:44,000 --> 00:05:51,000
And it's really not. If you love someone, set them free.

73
00:05:51,000 --> 00:05:54,000
That's the best thing to do, best thing you can do.

74
00:05:54,000 --> 00:05:59,000
Learning how to meditate on these feelings can be very, very, very important for parents.

75
00:05:59,000 --> 00:06:01,000
So I hope that helps.

76
00:06:01,000 --> 00:06:06,000
I guess I don't have a lot to say about being a parent.

77
00:06:06,000 --> 00:06:12,000
Not being with myself, but from the Buddhist teaching, this is what I have to offer.

78
00:06:12,000 --> 00:06:14,000
So there you go.

