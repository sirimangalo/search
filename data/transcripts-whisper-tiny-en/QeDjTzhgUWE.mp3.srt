1
00:00:00,000 --> 00:00:08,000
There's a question whether in this life or a future life, to be free from suffering, one must

2
00:00:08,000 --> 00:00:21,000
rid oneself of desire, is one required to be a monk to be enlightened?

3
00:00:21,000 --> 00:00:28,000
No, not explicitly required.

4
00:00:28,000 --> 00:00:40,000
The Buddha even said that even though one is decked out in jewels and wearing a royal attire,

5
00:00:40,000 --> 00:00:48,000
if they become free from desire and free from defilement, they should be considered a monk.

6
00:00:48,000 --> 00:00:54,000
I believe if I'm not wrong, it was Santeti, this minister of the king.

7
00:00:54,000 --> 00:00:59,000
If I haven't got my stories mixed up, who the Buddha told this verse about,

8
00:00:59,000 --> 00:01:07,000
because Santeti was a minister to some king, percenna, maybe.

9
00:01:07,000 --> 00:01:17,000
And he was very clever and was able to defeat an enemy or quell an uprising.

10
00:01:17,000 --> 00:01:20,000
In a border village.

11
00:01:20,000 --> 00:01:26,000
And so the king as a reward to him allowed him to live as a king for seven days.

12
00:01:26,000 --> 00:01:32,000
Not to be the king, but to live the luxury and to have as much luxury as the king had for seven days.

13
00:01:32,000 --> 00:01:40,000
And so for seven days he lived in complete luxury and sensuality.

14
00:01:40,000 --> 00:01:47,000
And he was even drinking alcohol and sporting himself with women and prostitutes and so on.

15
00:01:47,000 --> 00:01:52,000
And he had this one dancing girl who was in his employment.

16
00:01:52,000 --> 00:01:56,000
And he was so in love with her or in lust with her or whatever.

17
00:01:56,000 --> 00:02:01,000
And he would just love to just sit there and drink and get really drunk and watch her dance.

18
00:02:01,000 --> 00:02:11,000
And I guess there was another thing at the time that similar to now as I understand nowadays is women were,

19
00:02:11,000 --> 00:02:14,000
had to be really, really thin in order to be attractive or something.

20
00:02:14,000 --> 00:02:18,000
I don't know if that's still the case, but I did hear this whole thing about anorexia.

21
00:02:18,000 --> 00:02:20,000
Women think they have to be thinner and thinner.

22
00:02:20,000 --> 00:02:23,000
If you come to our monastery, you don't really have a choice.

23
00:02:23,000 --> 00:02:25,000
You just get thinner and thinner.

24
00:02:25,000 --> 00:02:35,000
But this was her, so she was abstaining from food because she wanted to be, quote unquote, beautiful.

25
00:02:35,000 --> 00:02:42,000
And she got so bad that when she got up on stage to dance on the seventh day,

26
00:02:42,000 --> 00:02:47,000
she had some nerve, some nerve broke or something broke inside of her.

27
00:02:47,000 --> 00:02:51,000
There's the story, same thing happened to a monk apparently in the Buddhist name.

28
00:02:51,000 --> 00:02:56,000
And she died, just suddenly boom died right there on the stage.

29
00:02:56,000 --> 00:03:00,000
And he's looking at her and he's drunk and he got root.

30
00:03:00,000 --> 00:03:05,000
So I'm sad about this that he went to find the Buddha.

31
00:03:05,000 --> 00:03:08,000
I'll decked out in his jewels and his robes and so on.

32
00:03:08,000 --> 00:03:16,000
And he was so sobered up by the affair that in the time it took him to walk to the Buddha,

33
00:03:16,000 --> 00:03:20,000
he was no longer was drunk or something like that because he got to the Buddha.

34
00:03:20,000 --> 00:03:23,000
And the Buddha taught him and he became an arahat.

35
00:03:23,000 --> 00:03:29,000
If I remember correctly, why the story is so not so good with my stories,

36
00:03:29,000 --> 00:03:32,000
but either he became an arahat.

37
00:03:32,000 --> 00:03:37,000
I think he became an arahat and then he passed away or something like that.

38
00:03:37,000 --> 00:03:41,000
There was one man, if it wasn't sent to the who passed away.

39
00:03:41,000 --> 00:03:44,000
And then they were all asking, well, what is he?

40
00:03:44,000 --> 00:03:48,000
Is he a brahmin or is he a shaman or is he a monk?

41
00:03:48,000 --> 00:03:50,000
What is he?

42
00:03:50,000 --> 00:03:59,000
And the Buddha said, even whether they are in these royal clothes and be decked in jewels,

43
00:03:59,000 --> 00:04:02,000
if they practice correctly, they may be.

44
00:04:02,000 --> 00:04:06,000
They're considered a brahmin, they're considered a shaman or something like that.

45
00:04:06,000 --> 00:04:13,000
But for sure the theory is sound that one can become a monk or one can become enlightened

46
00:04:13,000 --> 00:04:15,000
without becoming a monk.

47
00:04:15,000 --> 00:04:24,000
The orthodox theory is that our doctrine is that if they don't ordain they will pass away.

48
00:04:24,000 --> 00:04:29,000
That day or within seven days, I think within seven days is the orthodox doctrine.

49
00:04:29,000 --> 00:04:36,000
And it seems to be the case that either they became monks or else they pass away.

50
00:04:36,000 --> 00:04:43,000
And the question of course is whether that means a full ordination or just means leaving the home life.

51
00:04:43,000 --> 00:04:45,000
Anything to add?

52
00:04:45,000 --> 00:05:09,000
Anything to add to that?

