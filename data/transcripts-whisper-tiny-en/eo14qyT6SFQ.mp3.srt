1
00:00:00,000 --> 00:00:03,000
Hello, welcome back to Ask a Monk.

2
00:00:03,000 --> 00:00:06,000
Today, I will be answering the question as to whether

3
00:00:06,000 --> 00:00:09,000
Greens have a karmic potency.

4
00:00:09,000 --> 00:00:13,000
So whether they have the ability to give rise to

5
00:00:13,000 --> 00:00:19,000
ethical states, do they have ethical implications?

6
00:00:19,000 --> 00:00:22,000
In the sense that can they affect our lives,

7
00:00:22,000 --> 00:00:24,000
and can they affect the world around us?

8
00:00:24,000 --> 00:00:28,000
Is it possible to perform Buddhist karma,

9
00:00:28,000 --> 00:00:31,000
to make bad karma while you're asleep?

10
00:00:31,000 --> 00:00:35,000
And the answer, I think, is quite clearly

11
00:00:35,000 --> 00:00:39,000
yes, and it's important that we understand

12
00:00:39,000 --> 00:00:41,000
what we mean by karma.

13
00:00:41,000 --> 00:00:43,000
And this sort of helps us to understand

14
00:00:43,000 --> 00:00:46,000
what we mean by karma in Buddhism.

15
00:00:46,000 --> 00:00:49,000
Karma isn't an action that you do with the body or speech.

16
00:00:49,000 --> 00:00:52,000
It's something that arises in the mind.

17
00:00:52,000 --> 00:00:54,000
When you intend to do something that's karma,

18
00:00:54,000 --> 00:00:57,000
so the only reason to say killing is wrong or stealing is wrong

19
00:00:57,000 --> 00:01:00,000
or if we call bad karma is because of the effect

20
00:01:00,000 --> 00:01:02,000
that such things have on our mind,

21
00:01:02,000 --> 00:01:05,000
and the mental aspect of the act

22
00:01:05,000 --> 00:01:09,000
that your mind is building up for tendency

23
00:01:09,000 --> 00:01:14,000
to be angry or greedy or corrupt.

24
00:01:14,000 --> 00:01:17,000
So in that sense, because dreams

25
00:01:17,000 --> 00:01:23,000
are tend to be ethically charged,

26
00:01:23,000 --> 00:01:26,000
so we give rise to the same states in our mind

27
00:01:26,000 --> 00:01:29,000
of greed and anger and delusion,

28
00:01:29,000 --> 00:01:31,000
they can all arise in our mind

29
00:01:31,000 --> 00:01:33,000
quite clearly there are ethical implications.

30
00:01:33,000 --> 00:01:36,000
Now, I want to talk about a few things here,

31
00:01:36,000 --> 00:01:40,000
so I'm going to try to keep them in a clear order.

32
00:01:40,000 --> 00:01:44,000
The first thing I should talk about is

33
00:01:44,000 --> 00:01:47,000
what we understand dreams to be in Buddhism.

34
00:01:47,000 --> 00:01:49,000
So what are we talking about?

35
00:01:49,000 --> 00:01:55,000
We have a description of the different kinds of dreams.

36
00:01:55,000 --> 00:01:58,000
In one of the old Buddhist texts,

37
00:01:58,000 --> 00:02:01,000
the questions of King Melinda, the Melinda Panha.

38
00:02:01,000 --> 00:02:06,000
And that text is mentioned that there are six types of dreams.

39
00:02:06,000 --> 00:02:09,000
And so this will help us to understand

40
00:02:09,000 --> 00:02:11,000
what exactly we mean by dreams

41
00:02:11,000 --> 00:02:16,000
and exactly how relevant they are to our practice,

42
00:02:16,000 --> 00:02:18,000
and so on.

43
00:02:18,000 --> 00:02:21,000
Because I also want to talk about how we should approach dreams

44
00:02:21,000 --> 00:02:24,000
and what it means in terms of our practice.

45
00:02:24,000 --> 00:02:26,000
So there are six kinds.

46
00:02:26,000 --> 00:02:30,000
The first three are physical, wind, bile, flam,

47
00:02:30,000 --> 00:02:35,000
and this is just based on the medical terminology

48
00:02:35,000 --> 00:02:37,000
that they would use in the time

49
00:02:37,000 --> 00:02:39,000
of the Buddha or in ancient India.

50
00:02:39,000 --> 00:02:42,000
So basically it means a disruption in the body.

51
00:02:42,000 --> 00:02:45,000
There's something, you know, maybe eat bad food

52
00:02:45,000 --> 00:02:48,000
or maybe your body is out of order.

53
00:02:48,000 --> 00:02:50,000
You have a sickness or something.

54
00:02:50,000 --> 00:02:53,000
That can easily give rise to dreams.

55
00:02:53,000 --> 00:02:58,000
And the other three are some external influence,

56
00:02:58,000 --> 00:03:01,000
which could be a person around you.

57
00:03:01,000 --> 00:03:03,000
It could be spirits.

58
00:03:03,000 --> 00:03:04,000
It could be angels.

59
00:03:04,000 --> 00:03:06,000
I mean, I think what they're talking about is angels,

60
00:03:06,000 --> 00:03:08,000
but actually if you think about it,

61
00:03:08,000 --> 00:03:10,000
we don't even have to go so far.

62
00:03:10,000 --> 00:03:12,000
You know, if someone starts whispering in your ear

63
00:03:12,000 --> 00:03:14,000
where you're sleeping or if you hear noises,

64
00:03:14,000 --> 00:03:17,000
if you've ever had something going on around you

65
00:03:17,000 --> 00:03:20,000
and you start dreaming about something based on the sound

66
00:03:20,000 --> 00:03:23,000
that you hear or, you know, if it gets hot,

67
00:03:23,000 --> 00:03:26,000
that can affect your state of mind

68
00:03:26,000 --> 00:03:29,000
and so on even cause the degree.

69
00:03:29,000 --> 00:03:32,000
The fifth is something from the past

70
00:03:32,000 --> 00:03:36,000
and the sixth is something from the future.

71
00:03:36,000 --> 00:03:38,000
This is where it gets interesting.

72
00:03:38,000 --> 00:03:44,000
So some people have visions of things in their dreams

73
00:03:44,000 --> 00:03:47,000
can be from the past in this life.

74
00:03:47,000 --> 00:03:49,000
Sometimes people are suspicious that it's something

75
00:03:49,000 --> 00:03:51,000
from their past lives.

76
00:03:51,000 --> 00:03:54,000
But let's just say something in the past in this life

77
00:03:54,000 --> 00:03:56,000
because that's really obvious.

78
00:03:56,000 --> 00:03:58,000
Memories that we've had things that we keep up

79
00:03:58,000 --> 00:04:01,000
and we keep in our minds can cause us to dream.

80
00:04:01,000 --> 00:04:06,000
But the sixth one is considered to be important.

81
00:04:06,000 --> 00:04:11,000
And it's worth noting that only the sixth is considered

82
00:04:11,000 --> 00:04:14,000
to be important in terms of actually

83
00:04:14,000 --> 00:04:17,000
paying some kind of attention to it.

84
00:04:17,000 --> 00:04:21,000
But the sixth kind is something from the future

85
00:04:21,000 --> 00:04:24,000
and some people may not even believe this,

86
00:04:24,000 --> 00:04:26,000
but other people will be empathetic

87
00:04:26,000 --> 00:04:28,000
that this sort of thing happens to them

88
00:04:28,000 --> 00:04:31,000
where they'll dream something and then it happens.

89
00:04:31,000 --> 00:04:34,000
Suddenly it happens, it comes in their life.

90
00:04:34,000 --> 00:04:37,000
People can even have this in meditation.

91
00:04:37,000 --> 00:04:40,000
But it's possible that something from the future

92
00:04:40,000 --> 00:04:43,000
can cause a dream, something that's about to happen.

93
00:04:43,000 --> 00:04:46,000
And you can understand this in different ways.

94
00:04:46,000 --> 00:04:52,000
It could be simply because of the power of what's going to happen

95
00:04:52,000 --> 00:04:55,000
because we consider that the universe is much more

96
00:04:55,000 --> 00:04:59,000
than we can see and experience normally.

97
00:04:59,000 --> 00:05:03,000
So there are a lot of forces that are at work, potentially.

98
00:05:03,000 --> 00:05:09,000
And those forces can be experienced if you know about how animals

99
00:05:09,000 --> 00:05:13,000
are able to predict the weather and are able to predict disasters

100
00:05:13,000 --> 00:05:16,000
like before the tsunami in Asia.

101
00:05:16,000 --> 00:05:20,000
They say that the elephants all ran in land,

102
00:05:20,000 --> 00:05:23,000
like they knew it was going to come.

103
00:05:23,000 --> 00:05:25,000
And so on.

104
00:05:25,000 --> 00:05:28,000
They're just kind of the forces of nature

105
00:05:28,000 --> 00:05:33,000
that we're not able to sense in our normal state

106
00:05:33,000 --> 00:05:36,000
because actually when you're sleeping,

107
00:05:36,000 --> 00:05:39,000
let's talk a little bit about what exactly dreams are.

108
00:05:39,000 --> 00:05:41,000
So these are the six sources of dreams.

109
00:05:41,000 --> 00:05:44,000
But what's happening in the dream state is your mind

110
00:05:44,000 --> 00:05:46,000
is not asleep and is not awake.

111
00:05:46,000 --> 00:05:49,000
This is what we know in science as well.

112
00:05:49,000 --> 00:05:51,000
But from a meditative point of view,

113
00:05:51,000 --> 00:05:55,000
there's still a great amount of concentration,

114
00:05:55,000 --> 00:05:57,000
but there's no mindfulness.

115
00:05:57,000 --> 00:06:00,000
So you're not really in control, obviously,

116
00:06:00,000 --> 00:06:03,000
for most of us, unless you train yourself

117
00:06:03,000 --> 00:06:05,000
because there are actually people who train themselves

118
00:06:05,000 --> 00:06:06,000
in dreams.

119
00:06:06,000 --> 00:06:08,000
And I'm not going to teach you to do this.

120
00:06:08,000 --> 00:06:09,000
This isn't what I teach.

121
00:06:09,000 --> 00:06:13,000
I just want to help people to understand this

122
00:06:13,000 --> 00:06:16,000
so we can move on and continue with our meditation.

123
00:06:16,000 --> 00:06:18,000
Because this will interrupt your life

124
00:06:18,000 --> 00:06:20,000
and you wonder, well, people do wonder,

125
00:06:20,000 --> 00:06:23,000
what is the significance of dreams?

126
00:06:23,000 --> 00:06:25,000
Most dreams are not significant

127
00:06:25,000 --> 00:06:27,000
because as I said, you're in this state

128
00:06:27,000 --> 00:06:29,000
and you can be easily influenced by things

129
00:06:29,000 --> 00:06:31,000
because you don't have the ability to judge,

130
00:06:31,000 --> 00:06:34,000
to discriminate and to catch yourself.

131
00:06:34,000 --> 00:06:39,000
And so you're easily slip into illusion and fantasy,

132
00:06:39,000 --> 00:06:43,000
but there are certain dreams that are affected by the future.

133
00:06:43,000 --> 00:06:46,000
And this is what leads people, I think,

134
00:06:46,000 --> 00:06:49,000
to this end the dreams about the past

135
00:06:49,000 --> 00:06:51,000
which is what leads people to put significance in dreams.

136
00:06:51,000 --> 00:06:54,000
So they might experience a dream that they feel

137
00:06:54,000 --> 00:06:57,000
has some significance and that should be explored

138
00:06:57,000 --> 00:07:00,000
and has some meaning and importance

139
00:07:00,000 --> 00:07:03,000
based on the past and something they might be repressing

140
00:07:03,000 --> 00:07:04,000
and so on.

141
00:07:04,000 --> 00:07:08,000
Or they might feel that they see they have some dream

142
00:07:08,000 --> 00:07:12,000
and then it sort of comes true in the future.

143
00:07:12,000 --> 00:07:15,000
And so as a result, they feel that dreams have great significance.

144
00:07:15,000 --> 00:07:20,000
It's psychotherapists and Freud

145
00:07:20,000 --> 00:07:24,000
and Jung put great emphasis on dreams and the importance of them.

146
00:07:24,000 --> 00:07:27,000
I want to say that we shouldn't put great importance

147
00:07:27,000 --> 00:07:29,000
on our dreams and this comes back to the idea

148
00:07:29,000 --> 00:07:31,000
of whether you can create karma.

149
00:07:31,000 --> 00:07:34,000
As I said, there are karmic states that arise

150
00:07:34,000 --> 00:07:42,000
and so you are potentially creating more karma.

151
00:07:42,000 --> 00:07:45,000
But the point is that you can't really control that.

152
00:07:45,000 --> 00:07:47,000
You can't say I'm not going to create karma

153
00:07:47,000 --> 00:07:49,000
or even you can't say stop.

154
00:07:49,000 --> 00:07:52,000
You can't stop yourself from giving rise to fear

155
00:07:52,000 --> 00:07:54,000
or fear for nightmares or anger

156
00:07:54,000 --> 00:07:57,000
or attachment and lust and so on.

157
00:07:57,000 --> 00:08:01,000
So it's much more useful.

158
00:08:01,000 --> 00:08:04,000
What I think dreams are really useful for

159
00:08:04,000 --> 00:08:07,000
is for understanding our mind state.

160
00:08:07,000 --> 00:08:12,000
And we should never take the whole of the dream

161
00:08:12,000 --> 00:08:16,000
as somehow significant, even if it means something for the future

162
00:08:16,000 --> 00:08:19,000
or it has a potential to be a prediction

163
00:08:19,000 --> 00:08:26,000
or when you call it for telling the future

164
00:08:26,000 --> 00:08:31,000
or even if it comes from some repressed state that we have in the past,

165
00:08:31,000 --> 00:08:35,000
the point is that you're in a state where the mind is not connected.

166
00:08:35,000 --> 00:08:39,000
It's not functioning in a logical manner.

167
00:08:39,000 --> 00:08:41,000
So it's going to make connections

168
00:08:41,000 --> 00:08:43,000
between things that otherwise don't have connections.

169
00:08:43,000 --> 00:08:46,000
So maybe part of it comes from the fact

170
00:08:46,000 --> 00:08:49,000
that you had some traumatic experience in the past,

171
00:08:49,000 --> 00:08:52,000
but part of it just comes from your mind,

172
00:08:52,000 --> 00:08:58,000
the chaos in the mind and the nature of the mind to dream and to imagine.

173
00:08:58,000 --> 00:09:01,000
Likewise, you might remember think of something in the future

174
00:09:01,000 --> 00:09:05,000
or you might have an ability to see what's going to happen in the future,

175
00:09:05,000 --> 00:09:07,000
but it becomes distorted.

176
00:09:07,000 --> 00:09:09,000
It is distorted by the mind.

177
00:09:09,000 --> 00:09:12,000
It's the same as when you go in a hallucinogenic drug

178
00:09:12,000 --> 00:09:18,000
and you might have visions of angels, deities and all sorts of spiritual things.

179
00:09:18,000 --> 00:09:23,000
And so you think that this is the truth and something that you're seeing something that we can't normally see,

180
00:09:23,000 --> 00:09:31,000
but it's also the brain that is creating, is imagining, is hallucinating.

181
00:09:31,000 --> 00:09:38,000
So whether part of it is real and whether it is sending the mind off into a dimension that can see things that we otherwise couldn't see,

182
00:09:38,000 --> 00:09:43,000
it's also the brain and it's also doing all sorts of crazy things.

183
00:09:43,000 --> 00:09:47,000
I think if you look carefully, you'll see that this is true with dreams

184
00:09:47,000 --> 00:09:50,000
and that they shouldn't be trusted.

185
00:09:50,000 --> 00:09:56,000
And also I think in the sense of whether you can create karma or the karma that you're creating,

186
00:09:56,000 --> 00:10:02,000
because for one thing, the karma is not going to be strong

187
00:10:02,000 --> 00:10:05,000
because you don't really intend to do this or that in your dreams.

188
00:10:05,000 --> 00:10:09,000
You just kind of get angry or worried or afraid in the case of nightmares

189
00:10:09,000 --> 00:10:15,000
or have lustful dreams, but you don't really have the strong intention

190
00:10:15,000 --> 00:10:25,000
because your mind is not clear, your mind doesn't have the will or the awareness or the mindfulness,

191
00:10:25,000 --> 00:10:28,000
which is able to make decisions clearly.

192
00:10:28,000 --> 00:10:32,000
So it's going to be weak karma for one thing.

193
00:10:32,000 --> 00:10:40,000
And also for another thing, there's many states that are mixed up,

194
00:10:40,000 --> 00:10:44,000
and as I said, connections can be made in an illogical manner

195
00:10:44,000 --> 00:10:47,000
and you have all these crazy dreams.

196
00:10:47,000 --> 00:10:51,000
So what we should use dreams for is to be able to judge our mind state

197
00:10:51,000 --> 00:10:54,000
because what's good about them is you don't have this control.

198
00:10:54,000 --> 00:10:57,000
You aren't covering things up because there's no control.

199
00:10:57,000 --> 00:11:02,000
You're able to see many things about yourself that you wouldn't otherwise see.

200
00:11:02,000 --> 00:11:07,000
And this isn't the content of the dream, but it's the quality of the dream.

201
00:11:07,000 --> 00:11:11,000
So if you have a dream that's based on anger, if you have a dream that's based on fear, nightmares,

202
00:11:11,000 --> 00:11:15,000
you have a dream that's based on lust or so on, this shows you what's in your mind

203
00:11:15,000 --> 00:11:18,000
and often in a way that you otherwise wouldn't be able to see.

204
00:11:18,000 --> 00:11:23,000
So people who are otherwise calm and controlled might find themselves suddenly having nightmares.

205
00:11:23,000 --> 00:11:25,000
And so they think something is wrong.

206
00:11:25,000 --> 00:11:28,000
Well, the truth is this is you seeing something deep down that you're repressing

207
00:11:28,000 --> 00:11:33,000
because we're only able to repress things in our daily life by force of will,

208
00:11:33,000 --> 00:11:40,000
by actually some sort of mindfulness and the ability to choose not to follow.

209
00:11:40,000 --> 00:11:43,000
Which we don't have when we're dreaming when we're asleep.

210
00:11:43,000 --> 00:11:48,000
So in that sense, I would say that really the only thing that we should do with dreams

211
00:11:48,000 --> 00:11:53,000
is use them to see where we have work to do and to help us to understand how our mind works.

212
00:11:53,000 --> 00:11:56,000
Don't take the content of the dream as important.

213
00:11:56,000 --> 00:12:00,000
This is a big mistake because you don't know where it's coming from.

214
00:12:00,000 --> 00:12:03,000
It could come becoming simply because you had bad food.

215
00:12:03,000 --> 00:12:09,000
It could be coming from the way your body, the way your brain is functioning, the chemicals and whatever substances.

216
00:12:09,000 --> 00:12:14,000
You've been taking the different foods that don't react well and so on.

217
00:12:14,000 --> 00:12:16,000
It could be coming partly from the past.

218
00:12:16,000 --> 00:12:18,000
It could be coming from the future.

219
00:12:18,000 --> 00:12:21,000
It could become external influences, sounds that you hear.

220
00:12:21,000 --> 00:12:23,000
Or even angels.

221
00:12:23,000 --> 00:12:24,000
This is what they say.

222
00:12:24,000 --> 00:12:29,000
There could be spirits around them because you have strong concentration.

223
00:12:29,000 --> 00:12:34,000
You're put into a state where you might be able to, you know, similar to meditation

224
00:12:34,000 --> 00:12:38,000
where people practice meditation, go into great states of concentration

225
00:12:38,000 --> 00:12:42,000
and are able to see and hear and experience things that we can't analyze.

226
00:12:42,000 --> 00:12:45,000
It can come from all sorts of different sources.

227
00:12:45,000 --> 00:12:50,000
It's not nearly as reliable as a meditation, for example, in any way.

228
00:12:50,000 --> 00:12:53,000
If you want to predict the future, go into practice meditation.

229
00:12:53,000 --> 00:12:57,000
If you want to understand about your past and what sort of things you're repressing,

230
00:12:57,000 --> 00:12:59,000
you'll practice meditation.

231
00:12:59,000 --> 00:13:03,000
If you want to see angels and spirits and so on, practice meditation.

232
00:13:03,000 --> 00:13:05,000
There are different meditations for this.

233
00:13:05,000 --> 00:13:12,000
But as I said, when you're dreaming, your dreams will show you something about yourself,

234
00:13:12,000 --> 00:13:13,000
about how your mind works.

235
00:13:13,000 --> 00:13:18,000
Because as I'm told, a perfectly enlightened person, a person who's done away

236
00:13:18,000 --> 00:13:22,000
with all of their mental defilements won't dream.

237
00:13:22,000 --> 00:13:28,000
When they sleep, they sleep soundly and they sleep mindfully.

238
00:13:28,000 --> 00:13:33,000
So actually there's some sort of clarity of mind during the sleep

239
00:13:33,000 --> 00:13:36,000
and when they wake up, they're fully refreshed and rested.

240
00:13:36,000 --> 00:13:42,000
But regardless, it's just important that, like anything, we don't follow them.

241
00:13:42,000 --> 00:13:44,000
And we don't project on them.

242
00:13:44,000 --> 00:13:46,000
A dream is what it is.

243
00:13:46,000 --> 00:13:48,000
You had that experience in your dream.

244
00:13:48,000 --> 00:13:50,000
It doesn't mean that it's going to happen in the future,

245
00:13:50,000 --> 00:13:53,000
or that somehow it has some special significance,

246
00:13:53,000 --> 00:13:56,000
or someone's trying to tell you something.

247
00:13:56,000 --> 00:13:59,000
Or so, even if it's an angel or a spirit trying to tell you something,

248
00:13:59,000 --> 00:14:01,000
or even your mind trying to tell you something,

249
00:14:01,000 --> 00:14:03,000
that doesn't mean you should follow it.

250
00:14:03,000 --> 00:14:12,000
Because the source of the dream may still be full of delusion

251
00:14:12,000 --> 00:14:15,000
and misunderstanding is roundly due on the wrong path.

252
00:14:15,000 --> 00:14:20,000
So dreams can be karmic, and that's been the last thing that I would say

253
00:14:20,000 --> 00:14:22,000
is that this should be a caution for us,

254
00:14:22,000 --> 00:14:25,000
that we can't always control our minds in the states like dream.

255
00:14:25,000 --> 00:14:30,000
But even in our waking states, dreams are just another reason for us

256
00:14:30,000 --> 00:14:35,000
to work on our mind, because otherwise we're going to,

257
00:14:35,000 --> 00:14:37,000
when we fall asleep, we're going to dream.

258
00:14:37,000 --> 00:14:40,000
And what we mean by creating karma, even when we dream,

259
00:14:40,000 --> 00:14:43,000
is that we're building up dependency to act in that way.

260
00:14:43,000 --> 00:14:46,000
So the dreams are reinforcing these emotions,

261
00:14:46,000 --> 00:14:49,000
reinforcing our fear, reinforcing our stress,

262
00:14:49,000 --> 00:14:51,000
reinforcing our anger, reinforcing our lust,

263
00:14:51,000 --> 00:14:54,000
reinforcing the states that we're trying to do away with.

264
00:14:54,000 --> 00:14:57,000
So they can, anyway.

265
00:14:57,000 --> 00:15:02,000
You can also have positive dreams where you have love and kindness

266
00:15:02,000 --> 00:15:04,000
towards other beings as possible.

267
00:15:04,000 --> 00:15:08,000
But generally, you'll see the things you're clinging to when you dream.

268
00:15:08,000 --> 00:15:11,000
And so it's an example of what we have to get rid of.

269
00:15:11,000 --> 00:15:15,000
And it's also a reason for us to, in a sense, be concerned

270
00:15:15,000 --> 00:15:18,000
and take our meditation seriously.

271
00:15:18,000 --> 00:15:21,000
Because otherwise, this is the opposite of meditating

272
00:15:21,000 --> 00:15:25,000
where you're not mindful, and you will develop these states

273
00:15:25,000 --> 00:15:28,000
and they will become more pronounced.

274
00:15:28,000 --> 00:15:31,000
So I hope that helps and answers the question.

275
00:15:31,000 --> 00:15:33,000
Thank you for tuning in once again.

276
00:15:33,000 --> 00:15:36,000
This has been another episode of Ask a Month.

277
00:15:36,000 --> 00:15:38,000
This is the first time using this new microphone.

278
00:15:38,000 --> 00:15:40,000
So I've got a microphone I hope it works.

279
00:15:40,000 --> 00:15:45,000
I can sit further away from the camera and let you see more

280
00:15:45,000 --> 00:15:47,000
than just my face.

281
00:15:47,000 --> 00:15:56,000
So thanks for tuning in all the way.

