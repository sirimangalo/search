Good evening, everyone, broadcasting live January 21st, so I know Thursday I'm supposed
to be doing something interesting. I have been here, I think. But I just spent the day
pretty busy day today, so I think I'm not going to be doing it really well. And I've got
these TEDx talk coming up, so maybe scaling back some of my activities for the next week.
So next week I have an interview that I have to prepare for for this TEDx talk.
And I'm going to talk isn't even that big of a deal to me, it's more about me and my work
than about the dumbness, so it's not like it's going to be a teaching experience.
Talk, I'm going to be giving this about. There's some of the interesting things I found
about using the internet and social media and modern technology to spread the ancient
teachings of the Buddha, so it's a juxtaposition of ancient and modern. Anyway, that's
what I thought to talk to them about, so that's what I'll be talking about. Let me get
started. Any questions here? At the end I can see the car, one end I can see the cars
in the fact that my thoughts actually, the other end, it's not really so much what to
me, the things that have it recycled. It's really frustrating. One can be frustrating when
you have the idea of yourself, the idea of control. So what you're seeing is impermanence
suffering in oneself. You should read my video or watch my video on, I think it's called
experience of reality. So seeing the three characteristics of stuff, it's not comfortable.
Seeing that you're not in control makes you frustrated. I mean the real answer to your
question, simple answer to your question is, if you don't yourself frustrated, frustrated.
Seeing that you help us to change is a good lesson. These aren't easy questions and not
easy issues. They're not going to be solved in a day. You solve them a lot better when you
do intensive practice, but if you're practicing just on a daily basis, takes time, can
take lifetimes. It took the Buddha for uncountable, uncountable periods of time, plus a hundred
thousand eons, which are relatively short compared to the four uncountable. Oh, and here's
the thing. I'm so Saturday. Saturday, I'm giving, let's see, what am I giving to talk.
Saturday, I'm giving a talk in second life. What did we agree on? What did we disagree
about? It sounds like, I'll be giving a talk at 12 p.m. Just 12 noon, second life time.
The second life.
