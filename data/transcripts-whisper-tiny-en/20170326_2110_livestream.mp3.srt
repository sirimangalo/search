1
00:00:00,000 --> 00:00:09,000
moving your arms whenever you want, while trying to try watching a simple movement and we watch the stomach, rising and falling.

2
00:00:09,000 --> 00:00:16,000
Beginning we think I am the one making it rise and fall, but as you watch and watch.

3
00:00:16,000 --> 00:00:23,000
And when you walk and you think I am walking, I'm making myself walk, but when you really watch and see what's going on, you see, oh, I see.

4
00:00:23,000 --> 00:00:30,000
There's actually just this interplay between body and mind moments of mind's mind and body state.

5
00:00:30,000 --> 00:00:33,000
There's no self involved.

6
00:00:33,000 --> 00:00:43,000
And you come to see how constructed and artificial it always.

7
00:00:43,000 --> 00:00:48,000
This is the sort of thing that you start to let go.

8
00:00:48,000 --> 00:00:56,000
When you see clearly in this way, you start to see how the word sun-caro would it actually means.

9
00:00:56,000 --> 00:01:06,000
That is our arm.

