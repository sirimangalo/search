1
00:00:00,000 --> 00:00:13,280
Okay, so today's talk is going to be about contentment and so here's a quote to start us off.

2
00:00:16,560 --> 00:00:23,520
Health is the greatest possession. Contentment is the greatest wealth. Trustworthiness is the

3
00:00:23,520 --> 00:00:30,720
greatest relation and Nirvana is the greatest happiness.

4
00:00:39,200 --> 00:00:44,560
So today's talk is on specifically contentment and how it relates to the Buddhist teaching.

5
00:00:44,560 --> 00:00:55,600
I was asked about a week ago to give a talk on this subject. So here we are. Thank you all for

6
00:00:55,600 --> 00:01:02,880
coming. Good to see you on this lovely Sunday afternoon. Great thing about second life is it

7
00:01:02,880 --> 00:01:10,560
never rains unless you want it to. We can always make use of this wonderful always burning

8
00:01:10,560 --> 00:01:23,200
campfire. So what is contentment? I have to do with the Buddhist teaching. How does

9
00:01:23,200 --> 00:01:35,440
contentment play a part in Buddhism? I was asked to talk about this and so I had to do a little

10
00:01:35,440 --> 00:01:58,160
bit of research. But what I know about contentment in the Buddhist teaching leads me to understand

11
00:01:58,160 --> 00:02:07,280
that there are three kinds of contentment. And each one of these three plays a different role in

12
00:02:07,280 --> 00:02:08,640
Buddhism in the Buddhist teaching.

13
00:02:22,960 --> 00:02:26,160
The first type of contentment to get it out of the way

14
00:02:26,160 --> 00:02:37,280
is the type of contentment that the Buddha taught against. So you read in the Buddhist teaching

15
00:02:37,280 --> 00:02:45,760
that the Buddha taught if you want to be a great being. If you are striving for true greatness,

16
00:02:47,520 --> 00:02:49,600
the Buddha said you have to be full of discontent.

17
00:02:49,600 --> 00:02:56,240
And he taught against a certain type of contentment.

18
00:03:06,320 --> 00:03:13,760
And so this type of contentment, it is important to talk about because it often pops its

19
00:03:13,760 --> 00:03:25,200
ears, its head, both in the world and in spirituality as well. This is a contentment with one's

20
00:03:26,720 --> 00:03:27,040
self.

21
00:03:31,760 --> 00:03:36,960
I've often said it's a mark of a high mind

22
00:03:36,960 --> 00:03:43,040
that they are constantly working to improve themselves.

23
00:03:46,000 --> 00:03:51,520
I would say it's a mark that someone is to sign that someone is spiritually developed,

24
00:03:51,520 --> 00:03:54,160
that they are constantly trying to develop themselves.

25
00:03:55,840 --> 00:04:03,840
That the undeveloped person is known by their lack of interest in developing themselves.

26
00:04:03,840 --> 00:04:06,720
They are feeling that everything is fine the way it is.

27
00:04:11,120 --> 00:04:18,720
That there is no need to develop those things that wise people encourage us to develop,

28
00:04:18,720 --> 00:04:38,000
and there is no need to do away with those things that wise people encourage us to do away with.

29
00:04:48,720 --> 00:04:58,000
Just make sure everyone can hear me here.

30
00:05:11,120 --> 00:05:16,640
Okay, that's the yes. If you can't hear me, it's probably because you don't have voice on

31
00:05:16,640 --> 00:05:24,800
either that or you don't have, if you can't hear me, this is useless. Can someone explain

32
00:05:26,400 --> 00:05:33,920
the reason why you can't hear me? It doesn't do much good to speak to you and tell you why you can't

33
00:05:33,920 --> 00:06:02,880
hear me. So it's a mark of someone who is developing that they are interested in developing

34
00:06:02,880 --> 00:06:04,960
themselves, obviously.

35
00:06:10,400 --> 00:06:17,840
So it's one easy way to distinguish between spiritual people and people who are not interested

36
00:06:17,840 --> 00:06:25,840
in spirituality. It's also a way of reassuring ourselves that even when our mental development

37
00:06:25,840 --> 00:06:31,600
is progressing with difficulty, that we should be encouraged by the fact that we're trying

38
00:06:31,600 --> 00:06:44,560
and effort in the Buddha's teaching is really a very core value that failure in the meditation

39
00:06:44,560 --> 00:06:51,840
is really only possible when one ceases to practice or ceases to practice correctly.

40
00:06:51,840 --> 00:07:01,360
But when you stop trying or when you start putting out effort in another direction,

41
00:07:03,200 --> 00:07:12,880
in a way that is antithetical to the path or give up your effort, that's the only way you can

42
00:07:12,880 --> 00:07:19,840
really fail. As long as you're trying, striving, you're watching the breath, watching the stomach,

43
00:07:19,840 --> 00:07:28,080
when it rises and falls, for example. But you can't focus on it. You try and you try and it's

44
00:07:28,080 --> 00:07:47,040
easy to become frustrated and disheartened. But we shouldn't become discouraged just because we're

45
00:07:47,040 --> 00:07:57,520
not quote unquote getting anywhere. Our effort is our discontent and our intention to improve

46
00:07:57,520 --> 00:08:04,720
ourselves, self-improvement is a sign of Buddhism. And so in one way this distinguishes spiritual

47
00:08:04,720 --> 00:08:10,720
people between spiritual people and people who are content with being a mediocre person.

48
00:08:10,720 --> 00:08:17,680
It also distinguishes between two types of spiritual people. The type of person, the type of

49
00:08:17,680 --> 00:08:26,480
spiritual person who is content with their level of spirituality and can often become content with

50
00:08:28,400 --> 00:08:35,920
simple states of bliss or happiness or sort of pseudo-spirituality. Never challenging one's views

51
00:08:35,920 --> 00:08:41,760
and beliefs, practicing only as it's comfortable, so you see often people when they sit in meditation,

52
00:08:41,760 --> 00:08:48,640
they'll sit against the wall or they'll sit in a chair or they'll sit on a bench or so. And always

53
00:08:49,600 --> 00:08:55,840
striving to make the meditation pleasurable. And at any time when it's not pleasurable,

54
00:08:55,840 --> 00:09:07,440
one other stops practicing or adjusts one's practice. These are the kind of people who when

55
00:09:07,440 --> 00:09:12,800
they're sitting in meditation need everybody to be quiet. I had a man ask him what I thought about

56
00:09:12,800 --> 00:09:25,200
using earplugs in meditation. And I don't think, I think it should be pretty clear that earplugs

57
00:09:25,200 --> 00:09:38,320
are a sign that you have some sort of attachment to the sound. So this sort of contentment is

58
00:09:38,320 --> 00:09:44,560
one that we want to try to avoid resting on our laurels per se. If you're enlightened and you have

59
00:09:44,560 --> 00:09:51,120
no defilements left, then I think this is the only time one should become content with one

60
00:09:51,120 --> 00:09:56,960
spiritual development. But as long as we know that in ourselves we still have more work to do,

61
00:09:56,960 --> 00:10:04,240
we should set ourselves in doing that work. So this is the first sense that we might miss this

62
00:10:04,240 --> 00:10:09,200
and thinking that the Buddha taught us to be content and so don't worry, don't try, don't do

63
00:10:09,200 --> 00:10:15,760
anything. Practicing meditation is too much work. Going on meditation treats is just torturing

64
00:10:15,760 --> 00:10:22,800
yourself. You should just live your life peacefully and somehow spiritually.

65
00:10:25,600 --> 00:10:31,840
The type of contentment that the Buddha did teach and encourage can be broken up into two types.

66
00:10:31,840 --> 00:10:35,280
So there are two other types of contentment that the Buddha did encourage.

67
00:10:36,560 --> 00:10:43,040
And the first type is what he generally referred to as contentment itself and the word he uses

68
00:10:43,040 --> 00:10:56,480
a Santu team, which means being contentment. The Buddha used the word contentment

69
00:10:56,480 --> 00:11:07,040
he most often was referring to the conceptual objects of our reality. So in Buddhism we separate

70
00:11:07,040 --> 00:11:15,280
reality into two parts, the conceptual and the ultimate. Ultimate reality is the building blocks

71
00:11:15,280 --> 00:11:22,640
of reality. Just like in physical reality and physics they talk about what are the building blocks

72
00:11:22,640 --> 00:11:27,440
the atoms and then they had atoms and suddenly there were subatomic particles and so on.

73
00:11:27,440 --> 00:11:32,320
And they're still not sure what is the essence of the physical in the scientific community.

74
00:11:32,320 --> 00:11:42,080
But in Buddhism we have a very good understanding of what are the essential building blocks.

75
00:11:42,080 --> 00:11:47,440
So in the physical we're talking about the four elements and these are simply the four aspects

76
00:11:47,440 --> 00:11:55,920
of matter as it's experienced. Matter can be experienced as hot or cold. It can be experienced

77
00:11:55,920 --> 00:12:02,160
as hard or soft and it can be experienced as tense or flaccid.

78
00:12:06,800 --> 00:12:12,320
The mental side of experience is the awareness of the physical or the awareness of an object.

79
00:12:12,320 --> 00:12:15,920
So when we see something and we know that we're seeing when we hear something and we know that

80
00:12:15,920 --> 00:12:21,120
we're hearing. This is the ultimate reality. Everything else is conceptual. The chair that you're

81
00:12:21,120 --> 00:12:26,560
sitting in is conceptual. The word chair arises in your mind. You only know that it's a chair

82
00:12:29,280 --> 00:12:36,080
once you process the experience in your mind. So without the processing in the mind

83
00:12:36,080 --> 00:12:41,200
there's simply the feelings that arise from touching the chair and when you see it there's a scene.

84
00:12:41,840 --> 00:12:47,680
In the same way that the seeds that some of us are sitting on here in the virtual reality

85
00:12:47,680 --> 00:12:55,840
are only conceptual. So the the seats in second life that we're seeing are our seats because we

86
00:12:55,840 --> 00:13:03,600
process the site. The seats that we're sitting on in real life are conceptual. There are only

87
00:13:03,600 --> 00:13:10,000
seats because we process the feeling or the site or so. But at any rate it's still a concept. Our body

88
00:13:10,000 --> 00:13:17,200
is a concept because the reality is the experience. This is according to the Buddha.

89
00:13:20,240 --> 00:13:24,480
So the first type of contentment is talking about conceptual contentment, contentment with things,

90
00:13:25,200 --> 00:13:35,120
contentment with objects. This is one of those teachings that is not not exactly core. So it's not

91
00:13:35,120 --> 00:13:42,720
a theoretical teaching. It's a practical teaching for everyday life. And for most of us it's

92
00:13:42,720 --> 00:13:53,360
a very important teaching. One of the criticisms of many types of Buddhism, modern Buddhism,

93
00:13:53,360 --> 00:14:01,200
is that it often overlooks practical but conventional teachings like being content with

94
00:14:01,200 --> 00:14:07,360
possessions, being content with the things that you own with your belongings. And often

95
00:14:07,360 --> 00:14:13,040
note people dive right into the theoretical and talking about ultimate reality and so on.

96
00:14:13,680 --> 00:14:20,960
And not that this isn't the most important but we have so many delusions and we're so attached

97
00:14:20,960 --> 00:14:28,000
to the conceptual reality that it's like overlooking a very important part of the path which is going

98
00:14:28,000 --> 00:14:33,200
to be our relationship with those things that we cling to, our relationship with those things,

99
00:14:33,200 --> 00:14:40,080
those conceptual objects that we interact with. So on a practical level it's very important

100
00:14:40,080 --> 00:14:50,320
that we come to terms with. Our needs, our materialistic desires, not being content with

101
00:14:50,320 --> 00:14:58,400
our clothes, the clothes that we wear. And when it's said you use clothing to protect the body,

102
00:14:58,400 --> 00:15:10,960
to cover up the parts of the body that are best left covered. You use food simply for doing

103
00:15:10,960 --> 00:15:24,240
the way with hunger to bring energy to the body. You use shelter simply to guard from the elements

104
00:15:24,240 --> 00:15:35,600
and medicines to guard off sickness. And the Buddha said these are the four

105
00:15:35,600 --> 00:15:42,560
requisites of a human being. These four things are what we all cannot do without.

106
00:15:43,440 --> 00:15:48,000
But for most of us that's not nearly enough especially in modern times we're incredibly

107
00:15:48,000 --> 00:15:58,800
materialistic. So we are always needing more and we become slaves actually of our belongings

108
00:15:58,800 --> 00:16:06,400
and of our addictions that we always need more and more. And as a result our minds are not

109
00:16:06,400 --> 00:16:14,240
focused on the here and the now. We're not content with this state of reality. We need another

110
00:16:14,240 --> 00:16:18,720
state where there's this object or that object where we can see this thing or hear that thing

111
00:16:19,760 --> 00:16:25,520
or feel this or smell that or taste this or even simply just conceive of the fact that we own this

112
00:16:25,520 --> 00:16:33,520
and own that and we have so many belongings to this and that delicious food, beautiful clothes

113
00:16:36,560 --> 00:16:47,920
games and toys and possessions of all sorts. And because our mind is constantly thinking about

114
00:16:47,920 --> 00:16:52,240
getting more and more and more and is really addicted to the pleasure that comes from getting what

115
00:16:52,240 --> 00:16:57,680
we want. It's very difficult for us to find contentment. It's very difficult for us to stay

116
00:16:57,680 --> 00:17:04,720
in the present reality to accept reality for what it is because we're so used to being able to

117
00:17:06,960 --> 00:17:11,520
segment to categorize reality or to compartmentalize reality.

118
00:17:14,320 --> 00:17:18,480
We don't have to accept everything. We don't have to accept reality for what it is because when

119
00:17:18,480 --> 00:17:23,040
it's not the way we like, we have this idea that we can change it, that we can control it.

120
00:17:23,040 --> 00:17:35,280
And so we set ourselves on quite an imbalanced path and are always unfulfilled, always unsatisfied,

121
00:17:35,280 --> 00:17:40,880
and always in a state of discontent. And at any time that we don't get what we want, we crash

122
00:17:40,880 --> 00:17:48,800
the perpetual building up and building up of greater states of attachment, eventually snowballs

123
00:17:48,800 --> 00:17:56,800
leads and leads us to a great disappointment when we can't give what we want, when we're unable

124
00:17:56,800 --> 00:17:59,840
to compartmentalize reality any further.

125
00:17:59,840 --> 00:18:19,520
So I think this is perhaps one way of seeing the benefits of meditation practice or one use

126
00:18:20,240 --> 00:18:28,480
good use that comes that is that the meditation practice has in our lives is in looking at

127
00:18:28,480 --> 00:18:35,440
our desires for things, when we want something, when we need something, looking at our possessions

128
00:18:35,440 --> 00:18:41,760
and coming to see that the important truth that these are just conceptual, that we say I own this

129
00:18:41,760 --> 00:18:52,480
and I own that and to realize that this entire sentence is every word of it as false,

130
00:18:52,480 --> 00:19:06,960
the I, the owned this, I is not true, it's not mine, even our own self is just a conglomeration

131
00:19:06,960 --> 00:19:14,720
of mind and body and states arising and ceasing, most of which are very far out of our control,

132
00:19:16,240 --> 00:19:22,160
our states of emotion, our wants and our needs, we find that we really can't control them as we

133
00:19:22,160 --> 00:19:35,360
think we do, the own is not true, we can't control things and say let it always be like this,

134
00:19:36,400 --> 00:19:42,000
we can't even control the happiness that comes from the object because after a while

135
00:19:42,000 --> 00:19:46,640
we get bored of our possessions and we need more, we need the next thing, the newest thing

136
00:19:46,640 --> 00:19:55,280
and the this or the word this, the identification of the thing as an object

137
00:20:00,480 --> 00:20:07,360
is also not a part of reality because the object itself is either seeing, hearing, smelling,

138
00:20:07,360 --> 00:20:24,800
tasting, feeling, or thinking, when we practice meditation we're able to

139
00:20:29,120 --> 00:20:35,520
to address this issue of materialism, of being attached to things and needing more and not being

140
00:20:35,520 --> 00:20:45,360
content, the Buddha said it's a mark of a noble person, it's the lineage, which the word

141
00:20:45,360 --> 00:20:58,960
lungsat, I believe it means the tradition, I guess, of the noble ones to be content with whatever

142
00:20:58,960 --> 00:21:06,880
robes, whatever clothes, whatever food, whatever shelter, whatever medicines they get, to be content

143
00:21:06,880 --> 00:21:13,680
with these things is the way of the noble ones, it's a mark of nobility or noble in the Buddhist

144
00:21:13,680 --> 00:21:20,960
sense in terms of being enlightened. So this is something that's very important, something we

145
00:21:20,960 --> 00:21:25,680
should always be looking at in our lives, it's easy to think we're practicing meditation and

146
00:21:25,680 --> 00:21:31,760
then we're accumulating and accumulating and always getting more and more and new and so on.

147
00:21:31,760 --> 00:21:37,840
It's very difficult to control, it's very easy to rationalize why we need more and more in this

148
00:21:37,840 --> 00:21:45,520
and that, it's very difficult to be without. But the most important form of contentment

149
00:21:45,520 --> 00:21:49,040
is not actually called contentment in the Buddha's teaching as far as I've found,

150
00:21:49,040 --> 00:21:55,360
but to me it's the most important type of contentment and it's what is really being

151
00:21:55,360 --> 00:22:02,720
talked about when the Buddha talks about contentment with robes with clothes and food and so on.

152
00:22:03,840 --> 00:22:07,040
He's really talking about contentment with reality as it is,

153
00:22:07,040 --> 00:22:16,240
contentment with experience as it is,

154
00:22:23,280 --> 00:22:28,800
being totally in tune with reality to the point that nothing faces you.

155
00:22:29,840 --> 00:22:34,480
And it's not contentment, it's not called contentment I think because the word contentment

156
00:22:34,480 --> 00:22:44,080
can be quite misleading in this sense. It can be used to, it can slip into enjoyment.

157
00:22:44,880 --> 00:22:49,120
So we say, you know, I'm, you know, enjoying smelling the flowers, you know, stopping and

158
00:22:49,120 --> 00:22:53,280
smelling the flowers and just enjoying life being content with my life.

159
00:22:54,640 --> 00:23:00,080
This isn't what is meant here. What is meant here is contentment with every experience

160
00:23:00,080 --> 00:23:09,760
in terms of not being, not being discontent, not being drawn into a judgment about reality,

161
00:23:09,760 --> 00:23:16,320
not being drawn into a need. And in this sense there are two kinds of discontent, discontent

162
00:23:16,320 --> 00:23:22,720
based on anger and discontent based on greed. You could even say third, discontent based on

163
00:23:22,720 --> 00:23:31,200
delusion. We want to go, go there as well. But what we, what we notice most in our lives is,

164
00:23:31,200 --> 00:23:37,920
is these two discontentment based on anger and discontent, discontent based on greed that

165
00:23:37,920 --> 00:23:42,480
will be sitting in meditation, even just sitting here listening to a talk and it's very difficult

166
00:23:42,480 --> 00:23:47,360
to keep the mind content, simply to listen, simply to be here and now.

167
00:23:47,360 --> 00:23:54,720
Content with what I'm saying, content with the feelings going on in your body,

168
00:23:55,920 --> 00:23:59,280
content with the things around you, content with the thoughts in your mind.

169
00:24:01,120 --> 00:24:07,120
There's so many things that make us angry and upset, worried, afraid, bored.

170
00:24:07,120 --> 00:24:20,800
And our ordinary way of looking at this is to immediately need to change, need to alter our

171
00:24:20,800 --> 00:24:28,480
reality, to suit our, our defilement, to suit our, our defile state of mind, to suit our anger,

172
00:24:28,480 --> 00:24:32,800
our dislike. When we don't like something, we should get rid of it, we should remove it. This is

173
00:24:32,800 --> 00:24:39,360
the way we look at things, not just in terms of our, our innate sense, but also in terms of

174
00:24:39,360 --> 00:24:43,760
our views, our idea of what is right. We think that it's right, that when you feel pain, you

175
00:24:43,760 --> 00:24:48,320
should adjust, you should move, you should do whatever necessary to get rid of the pain.

176
00:24:51,040 --> 00:24:55,120
This is one kind of discontent that is, is antithetical to the, what is teaching in its,

177
00:24:55,680 --> 00:25:00,640
it's the purpose of the meditation practice to remove this sort of discontent.

178
00:25:00,640 --> 00:25:08,960
Rather than trying to change the pain, to change the experience, we change the way we look at the

179
00:25:08,960 --> 00:25:16,000
experience. So we, we, we simply see it for what it is and when we're discontent, or when we're

180
00:25:17,760 --> 00:25:27,120
in pain or, or maybe too hot, too cold, when we're hungry, when there's loud noises or, or whatever,

181
00:25:27,120 --> 00:25:33,120
when we're thinking about bad things, that we simply see it for what it is and the word bad

182
00:25:33,120 --> 00:25:38,400
disappears. So we're no longer thinking about bad things or feeling bad feelings. We're just

183
00:25:38,400 --> 00:25:45,760
thinking or feeling or seeing or hearing or smelling and tasting. And all of our experience of

184
00:25:45,760 --> 00:25:53,120
reality is, is one of contentment in the sense of simply seeing it for what it is, not discontent

185
00:25:53,120 --> 00:26:00,640
in terms of needing, needing it to be different. The other form of discontent is based on,

186
00:26:00,640 --> 00:26:06,880
on desire. It's not that there's anything wrong with our present state of being here.

187
00:26:08,080 --> 00:26:14,720
It's that we want something more or we, we begin thinking about something and it makes us want it,

188
00:26:14,720 --> 00:26:25,200
makes us desire to do this or do that to obtain this or obtain that experience. And I, I would

189
00:26:25,200 --> 00:26:33,120
say from, from the practice looking at these two, the, really the, the, the only way to deal with

190
00:26:33,120 --> 00:26:41,040
them adequately is through meditation is through breaking them up into their, their building blocks,

191
00:26:41,040 --> 00:26:49,520
the pieces and seeing that actually we're not talking about a, an entity or a single experience,

192
00:26:49,520 --> 00:26:54,400
we're talking about several different experiences all jumbled up into one. First, there's the

193
00:26:54,400 --> 00:27:01,680
experience of the object. Then there's the feeling that the, the, the recognition of it as something

194
00:27:03,280 --> 00:27:06,720
good or something bad. Then there's the feeling that comes of happiness or pain.

195
00:27:06,720 --> 00:27:14,400
Then there's the liking under the disliking it. Once there's the liking and the disliking it,

196
00:27:14,400 --> 00:27:19,840
there's the intention to do this and or to do that, to change the experience, to attain,

197
00:27:19,840 --> 00:27:23,840
obtain something different or to remove something from our experience.

198
00:27:26,880 --> 00:27:33,280
And only then is there the acting out on it. And all of these can be broken up and separated

199
00:27:33,280 --> 00:27:39,360
from each other at any, at any one time. When you see something or, or let's take an easy one,

200
00:27:39,360 --> 00:27:43,280
when you hear something, suppose you're listening to what I'm saying and you really don't like it.

201
00:27:43,280 --> 00:27:53,120
Maybe I'm saying nasty things about, maybe what I'm saying just seems totally in a pose,

202
00:27:53,120 --> 00:28:00,560
diametrically opposed to what you believe. Or suppose I start yelling at you and saying nasty things

203
00:28:00,560 --> 00:28:08,320
to you or so on. Suppose someone comes into your room and starts, you know, complaining and so on.

204
00:28:11,440 --> 00:28:15,520
Right away, you can say to yourself hearing, hearing, remind yourself that it's just a sound.

205
00:28:16,400 --> 00:28:20,080
You can try that right now when I'm talking. You like it. You don't like it's not important.

206
00:28:20,080 --> 00:28:26,320
As a meditation exercise, simply when you hear my voice saying to yourself, hearing, hearing,

207
00:28:26,320 --> 00:28:32,960
hearing, it's a great way to listen to Dhamma talks. People can even become enlightened this way,

208
00:28:32,960 --> 00:28:40,560
listening to the Dhamma realizing the truth at the same time. One teacher in Thailand would always say

209
00:28:40,560 --> 00:28:44,320
best way to listen to a Dhamma talk. Just say hearing, hearing, hearing the whole time.

210
00:28:45,360 --> 00:28:48,640
Because that's really why we're teaching is to encourage people to meditate.

211
00:28:48,640 --> 00:28:58,000
And if that doesn't work, if that's, if you're not quick enough with that,

212
00:28:58,960 --> 00:29:05,840
then when the feeling arises, when you feel happiness or pain, you just focus on the happy,

213
00:29:06,560 --> 00:29:11,440
happy, happy or pain pain or sad sad or whatever.

214
00:29:11,440 --> 00:29:21,920
And again, you don't let it build into a real liking or disliking and the intention to change things.

215
00:29:21,920 --> 00:29:26,400
You simply, when you have pain in the body, for instance, knowing that it's pain and just seeing

216
00:29:26,400 --> 00:29:31,760
it for what it is. And if you're still, if you can't catch it there, then you can catch it at the

217
00:29:31,760 --> 00:29:38,800
liking or the disliking, saying to yourself, liking, liking or wanting, wanting, disliking,

218
00:29:38,800 --> 00:29:47,680
angry, upset, bored, scared, sad, as well. We still can't catch it there, then you can catch the

219
00:29:47,680 --> 00:29:55,040
intention. You want to do this. You want to obtain something. You want to say something. You want

220
00:29:55,040 --> 00:30:03,200
to chase someone away or run away or so on. And you can focus on that intention, wanting,

221
00:30:03,200 --> 00:30:09,360
wanting or intending, intending. In this sense, not wanting as greed or as desire, but as an intention

222
00:30:09,360 --> 00:30:19,040
to do something. If you still can't catch it there, you're in trouble, because at that point,

223
00:30:19,040 --> 00:30:22,800
you're going to go out and do something about it. But the amazing thing is, is when you focus on

224
00:30:22,800 --> 00:30:32,960
all of these things, you can see the problem with this line of reaction. You see how when you,

225
00:30:32,960 --> 00:30:39,440
when you want things to be different, what they are, your whole body is tense. And your mind is

226
00:30:39,440 --> 00:30:47,440
in a state of upset, of turmoil. Even if it's a desire and you feel happy and you really want

227
00:30:47,440 --> 00:30:51,680
something, you think, wow, this is going to make me happy. When you look at it, when you analyze it,

228
00:30:51,680 --> 00:30:58,240
you see that it's not so pleasurable at all, really. Your mind is in a state of clinging. You can

229
00:30:58,240 --> 00:31:03,520
really feel like you're clinging. It's as though you're grabbing something in your fist and holding

230
00:31:03,520 --> 00:31:08,880
tightly. That's how your mind feels at the moment when you want something. When you think you have

231
00:31:08,880 --> 00:31:16,160
to do something to obtain your pleasure, the object of your desires, even then, you can see that

232
00:31:16,160 --> 00:31:22,560
it's not really pleasurable, not to speak of anger when you want to hurt someone. When you look at

233
00:31:22,560 --> 00:31:28,960
it, you can see that this is not a wholesome state of mind. It's not a proper state of mind.

234
00:31:42,640 --> 00:31:50,000
And so this is how we really develop true contentment and Buddhism, coming to separate things into

235
00:31:50,000 --> 00:31:55,520
their reality. When you see something instead of being attached to it, wanting it, needing it,

236
00:31:55,520 --> 00:32:00,080
clinging to it, simply seeing it for what it is. If you're happy, you're happy. If you're unhappy

237
00:32:00,080 --> 00:32:07,920
or unhappy, contentment in the sense of doing away with any need for things to be different,

238
00:32:08,880 --> 00:32:11,200
any need for reality to be other than what it is.

239
00:32:11,200 --> 00:32:23,040
This is really the ultimate state and it's difficult. It's dangerous to say this, in a sense,

240
00:32:23,040 --> 00:32:28,640
just to leave it at that because it sounds as though, you know, don't do anything then.

241
00:32:28,640 --> 00:32:33,120
Being content with things as they are is, you know, as I said, what it taught us to be discontent.

242
00:32:33,120 --> 00:32:47,360
But the key is it's not easy. This is not the ordinary nature of our minds. We're working hard

243
00:32:48,240 --> 00:32:55,120
striving to stop striving. We have to work in order that we don't have to do anything,

244
00:32:55,120 --> 00:33:01,040
in order that we no longer need to attain anything, to do away with our need to attain things.

245
00:33:01,040 --> 00:33:08,400
We're working hard to stop our minds from working so hard, in a sense.

246
00:33:10,960 --> 00:33:19,440
As we practice, as we develop the meditation practice, we find ourselves more and more content.

247
00:33:19,440 --> 00:33:31,440
Through working in this way, through developing ourselves, we're able to experience all things,

248
00:33:31,440 --> 00:33:37,360
and we no longer compartmentalize reality. We no longer separate things into the variable and

249
00:33:37,360 --> 00:33:45,120
the unbearable, the acceptable and the unacceptable. We're able to accept and react

250
00:33:45,120 --> 00:33:51,040
rationally and honestly and with wisdom to everything that arises.

251
00:33:52,960 --> 00:34:00,800
This is the Buddha's teaching on contentment. So that was the dhamma I would like to give today.

252
00:34:00,800 --> 00:34:16,720
And if there are any questions, I'm happy to take them. Otherwise, thanks everyone for coming.

