1
00:00:00,000 --> 00:00:06,200
The word aggregated, not so good here.

2
00:00:06,200 --> 00:00:15,360
So we should substitute some other word, like a group.

3
00:00:15,360 --> 00:00:21,000
Silla group, somebody group, a group or something like that.

4
00:00:21,000 --> 00:00:27,120
The body word is kanda, but here, kanda does not mean the same thing as a root

5
00:00:27,120 --> 00:00:29,720
kanda or with a nakanda and so on.

6
00:00:29,720 --> 00:00:33,120
Here, Silla, kanda, somebody, kanda, kanda, kanda.

7
00:00:33,120 --> 00:00:38,880
So I think it's a group, a group is better word than aggregate here.

8
00:00:38,880 --> 00:00:47,920
So according to three groups, for the birth being selective is included by the three aggregates

9
00:00:47,920 --> 00:00:54,200
which are comprehensive as a city is by a kingdom, according as it is said and so on.

10
00:00:54,200 --> 00:01:04,280
Now, what is the meaning of the fact being selective?

11
00:01:04,280 --> 00:01:12,520
The Silla group, Silla group means both mundane and super mundane.

12
00:01:12,520 --> 00:01:21,320
There is mundane Silla and there is super mundane Silla, right?

13
00:01:21,320 --> 00:01:26,120
So when we say Silla group, we include both mundane and super mundane.

14
00:01:26,120 --> 00:01:31,680
So it is more comprehensive, but MAGA is only super mundane.

15
00:01:31,680 --> 00:01:37,480
So it is called selective here.

16
00:01:37,480 --> 00:01:47,200
There may be a better word for the word selective, I don't know.

17
00:01:47,200 --> 00:02:00,400
So the super mundane fact is just a part of the whole group of Silla, right?

18
00:02:00,400 --> 00:02:07,640
So Silla group means mundane and super mundane Silla, but MAGA means only super mundane.

19
00:02:07,640 --> 00:02:14,840
So it is less comprehensive as a city is by a kingdom.

20
00:02:14,840 --> 00:02:24,120
So according as it is said and the quotation is from Isota and MAGA, MAGA.

21
00:02:24,120 --> 00:02:29,320
The three groups are not included in the Nobel Eightfold Path, friend Visaga, but the

22
00:02:29,320 --> 00:02:37,960
Nobel Eightfold Path is included by the three aggregates or three groups.

23
00:02:37,960 --> 00:02:47,000
That means three groups are more comprehensive than the Nobel Eightfold Path.

24
00:02:47,000 --> 00:02:55,680
So it is not said that these groups are included in the Nobel Eightfold Path, but that the

25
00:02:55,680 --> 00:03:03,120
Nobel Eightfold Path is included by the three aggregates.

26
00:03:03,120 --> 00:03:11,440
Any right speech, any right action, any right tablet, these are included in the Vaju group.

27
00:03:11,440 --> 00:03:21,360
So when we divide the eight factors into three groups, does the right speech, right action

28
00:03:21,360 --> 00:03:32,360
and right livelihood fall under Vaju, Silla, and then any right effort, right mindfulness

29
00:03:32,360 --> 00:03:37,000
and right concentration, these are included in the concentration group.

30
00:03:37,000 --> 00:03:40,800
So these three are and they belong to concentration group.

31
00:03:40,800 --> 00:03:46,840
And then right view and right thinking, they are included in the understanding group.

32
00:03:46,840 --> 00:03:54,240
So Silla, Samadhi, and Panya, as to the three beginning with right effort, concentration

33
00:03:54,240 --> 00:03:59,600
cannot of its own nature, cause absorption through unification on the object and so on.

34
00:03:59,600 --> 00:04:09,600
So this explanation is very, very important when you try to explain how these factors work

35
00:04:09,600 --> 00:04:11,680
during meditation.

36
00:04:11,680 --> 00:04:22,000
Now, here we just said that concentration alone by its nature cannot cause absorption

37
00:04:22,000 --> 00:04:26,800
through unification of the object, but with energy accomplishing, accomplishing its function

38
00:04:26,800 --> 00:04:39,200
and so on, that means Samadhi, when only helped by energy and mindfulness, will really

39
00:04:39,200 --> 00:04:40,520
penetrate.

40
00:04:40,520 --> 00:04:47,000
So if we want to get Samadhi and we have to have really a mean effort, we must make

41
00:04:47,000 --> 00:04:51,440
effort and we must have mindfulness.

42
00:04:51,440 --> 00:04:58,280
So only when there is effort and mindfulness can there be concentration.

43
00:04:58,280 --> 00:05:09,360
So concentration is helped by effort and mindfulness, so that is explained in this paragraph.

44
00:05:09,360 --> 00:05:22,480
And then it similarly is given for people going to the current and then kicking the fruit.

45
00:05:22,480 --> 00:05:30,280
Then paragraph 99, here also, Panya is assisted by right thinking or Vitaka.

46
00:05:30,280 --> 00:05:34,240
That is why right thinking is included in the group of Panya.

47
00:05:34,240 --> 00:05:39,960
So as regards right view and right thinking, understanding cannot of its own nature define an object

48
00:05:39,960 --> 00:05:47,200
as informative painful not so, but with applied thought that is Vitaka, giving assistance

49
00:05:47,200 --> 00:05:50,560
by repeatedly hitting the object it can.

50
00:05:50,560 --> 00:05:58,880
That means the right, I mean right thought or initial application takes the mind to the

51
00:05:58,880 --> 00:06:04,320
object, mind together with the pre-confidence.

52
00:06:04,320 --> 00:06:11,480
So only when Vitaka takes the mind to the object can understanding of Vanya, but it

53
00:06:11,480 --> 00:06:12,480
traces.

54
00:06:12,480 --> 00:06:19,080
It is like if you don't take a person to a certain place, that person will not know

55
00:06:19,080 --> 00:06:21,080
anything about that place.

56
00:06:21,080 --> 00:06:27,320
So you have to take it, take it there and then he sees it and then he knows everything

57
00:06:27,320 --> 00:06:29,240
about it.

58
00:06:29,240 --> 00:06:31,560
If you do not take him there then he will not know about it.

59
00:06:31,560 --> 00:06:38,960
So in the same way, if Vitaka or here right thought does not take the mind to the object

60
00:06:38,960 --> 00:06:42,520
then Panya cannot do anything.

61
00:06:42,520 --> 00:06:47,400
That is why the Panya is assisted by Vitaka or right thinking.

62
00:06:47,400 --> 00:06:54,680
So they are grouped together as understanding group.

63
00:06:54,680 --> 00:07:00,640
And also it is seemingly given.

64
00:07:00,640 --> 00:07:08,800
So the path is included by the three aggregates and then paragraph 103 has to similar and

65
00:07:08,800 --> 00:07:09,800
dissimilar.

66
00:07:09,800 --> 00:07:19,040
It is just something I am playing with the different meanings of the noble truths.

67
00:07:19,040 --> 00:07:24,800
See all the truths are similar to each other because they are not unreal, are wide of self

68
00:07:24,800 --> 00:07:27,600
and are difficult to penetrate according as it is said.

69
00:07:27,600 --> 00:07:33,120
Say what do you think Anna which is more difficult to do, more difficult to perform.

70
00:07:33,120 --> 00:07:38,160
That immense should shoot an arrow through a small keyhole from a distance time after time

71
00:07:38,160 --> 00:07:44,560
without missing or that he should penetrate the tip of a hair split, split a hundred times

72
00:07:44,560 --> 00:07:46,600
with the tip of a similar hair.

73
00:07:46,600 --> 00:07:51,760
And this is more difficult to do, one was more difficult to perform, that immense should

74
00:07:51,760 --> 00:07:56,920
penetrate the tip of a hair split a hundred times with the tip of a similar hair.

75
00:07:56,920 --> 00:08:02,240
They penetrate something more difficult to penetrate than that, Anna, who penetrate correctly

76
00:08:02,240 --> 00:08:04,680
does, this is suffering and so on.

77
00:08:04,680 --> 00:08:09,560
So it is more difficult to penetrate the full noble truths than to penetrate the tip of

78
00:08:09,560 --> 00:08:14,480
a hair split a hundred times with another tip of a hair split a hundred times with another

79
00:08:14,480 --> 00:08:25,800
tip of a needle with another needle, something like that, that's right.

80
00:08:25,800 --> 00:08:30,440
And the first two are similar things they are to form and so on, they are similar in one

81
00:08:30,440 --> 00:08:33,640
sense and dissimilar in another sense.

82
00:08:33,640 --> 00:08:43,760
So this is like just shuffling the full noble truths as to similarity and dissimilarity.

83
00:08:43,760 --> 00:08:56,200
Now next chapter, the dependent origination, first the word, the pali word.

84
00:08:56,200 --> 00:09:06,640
So please look at the seats and I will explain to you and then you can read the book.

85
00:09:06,640 --> 00:09:12,720
So we cannot do away with the pali word, so we will retain the pali words.

86
00:09:12,720 --> 00:09:15,480
So what is pateja samobara?

87
00:09:15,480 --> 00:09:25,880
Now, in the briefly much in this book, pateja samobara is said to mean states that are conditions

88
00:09:25,880 --> 00:09:34,240
that means causes, states that are condition, namely a which means ignorance, ignorance

89
00:09:34,240 --> 00:09:35,240
and so on.

90
00:09:35,240 --> 00:09:44,280
Now, before we explain to this, I want you to be familiar with the formula of the dependent

91
00:09:44,280 --> 00:09:53,480
origination, depending upon a which and I mean, ignorance, there are formations, depending

92
00:09:53,480 --> 00:10:06,640
on formations, there are there is consciousness and so on, so ignorance, formations, consciousness,

93
00:10:06,640 --> 00:10:16,880
mentality, materiality, six bases, what else, what what next?

94
00:10:16,880 --> 00:10:30,240
That feeling, craving, cleaning, becoming, birth, old agent death, so this sequence you have

95
00:10:30,240 --> 00:10:32,480
to be familiar with.

96
00:10:32,480 --> 00:10:36,680
Now, what does the word pateja samobara mean?

97
00:10:36,680 --> 00:10:43,720
Now, but it's a samobara means states that are conditions that is according to this

98
00:10:43,720 --> 00:10:52,840
commentary and then there is another word pateja samobara, so pateja samobara means states

99
00:10:52,840 --> 00:11:01,920
that are conditions, so there are results or fruits, namely jara marana etc.

100
00:11:01,920 --> 00:11:07,720
Now, in reality, all demos mentioned in the pateja samobara are both included in both,

101
00:11:07,720 --> 00:11:16,320
they are both pateja samobara and pateja samobara, only the first indicates conditions

102
00:11:16,320 --> 00:11:22,800
and the second conditions, because all those mentioned in the pateja samobara are in

103
00:11:22,800 --> 00:11:33,320
the dependent, so first you should understand this and then the paragraph 4 you may be,

104
00:11:33,320 --> 00:11:37,200
and please go and turn to the bottom.

105
00:11:37,200 --> 00:11:48,280
In the word pateja samobara, we have the word opara there, so the word opara has three meanings,

106
00:11:48,280 --> 00:11:56,760
one is that which arises, the second meaning is that which causes others to arise, this

107
00:11:56,760 --> 00:12:03,240
is a causative sense, and the third meaning is just arising, it's an action.

108
00:12:03,240 --> 00:12:13,480
We call it verbal noun, so the word opara can mean three meanings, but with regard to

109
00:12:13,480 --> 00:12:20,560
the word pateja samobara, only the two, the first two are accepted, and the last one

110
00:12:20,560 --> 00:12:30,520
is rejected in the Visodimada, and so that rejection you can read in paragraph 8 through

111
00:12:30,520 --> 00:12:52,640
13, and then the meaning of the word pateja samobara, there are four meanings given to the

112
00:12:52,640 --> 00:13:00,000
word pateja samobara, so the first meaning, that which is to be arrived at, or that which

113
00:13:00,000 --> 00:13:09,640
is to be known, and it is the meaning of pateja here, and which arises together and rightly,

114
00:13:09,640 --> 00:13:22,120
so I put the colours to correspond, so the word samobara is composed of sam and opara,

115
00:13:22,120 --> 00:13:32,640
and sam is made to mean together and rightly here, and opara is made to mean that which

116
00:13:32,640 --> 00:13:40,800
arises, so samobara means that which arises together and rightly, and together means

117
00:13:40,800 --> 00:13:54,360
not singly, not, say, not formation only, not consciousness only, because when consciousness

118
00:13:54,360 --> 00:14:02,040
arises, then matter of fact, there's also a rise, right, and at the same time, there

119
00:14:02,040 --> 00:14:09,440
arise also material properties, so in that way, and together means not singly, not one,

120
00:14:09,440 --> 00:14:18,200
one, and rightly means not without cause, so they arise, rightly means they arise with

121
00:14:18,200 --> 00:14:24,440
their respective causes, not without cause, so by this definition, the conditioned states

122
00:14:24,440 --> 00:14:32,320
are called pateja samobara, so by this definition, the results are called pateja samobara,

123
00:14:32,320 --> 00:14:42,720
now, second definition is that which, depending upon the convergence of conditions, and

124
00:14:42,720 --> 00:14:52,320
that is the meaning of pateja arises together, here, sam has only one meaning together,

125
00:14:52,320 --> 00:15:01,840
and opara has meaning that which arises, and pateja means having depended upon, or

126
00:15:01,840 --> 00:15:08,840
depending upon, so by this definition, to the conditioned states, there is the fruit

127
00:15:08,840 --> 00:15:19,440
are meant, now, if you look at the first line, you see that pateja samobara means

128
00:15:19,440 --> 00:15:25,560
states that are conditions, but here, the first meaning is, according to the first and

129
00:15:25,560 --> 00:15:35,080
the second meaning, pateja samobara means results, and not causes, right, but the

130
00:15:35,080 --> 00:15:45,640
commentary, the vulnerable border gossa is very powerful, persistent, in making us accept

131
00:15:45,640 --> 00:15:54,400
that, pateja samobara really means the conditions of causes, and not the effects, not

132
00:15:54,400 --> 00:16:01,760
the fruits, and how to explain that, according to the definition given by himself, number

133
00:16:01,760 --> 00:16:10,360
one definition and number two definition means the results, and not the cause, so he has

134
00:16:10,360 --> 00:16:18,400
to, I will not conjure up something, so although, but definitions one and two, the word

135
00:16:18,400 --> 00:16:25,400
pateja samobara means conditioned states, we are here to understand it in a figurative

136
00:16:25,400 --> 00:16:30,880
sense, so it means states that are conditioned, so actually, we must take that the conditions

137
00:16:30,880 --> 00:16:36,800
are meant by the word pateja samobara, even though we follow these two definitions,

138
00:16:36,800 --> 00:16:53,800
how that is explained in paragraph 16, sometimes we, not only we, and even in the text,

139
00:16:53,800 --> 00:17:01,080
there are some states which are to be taken figuratively, now there is one statement in

140
00:17:01,080 --> 00:17:10,980
the samobara which states, the appearance of boulders is bliss, now the appearance of

141
00:17:10,980 --> 00:17:18,600
boulder is not bliss actually, it is the cause of bliss, because when boulders appear then

142
00:17:18,600 --> 00:17:25,680
people get enlightened and so, and there is bliss for them, so the appearance of boulder

143
00:17:25,680 --> 00:17:31,720
is actually not bliss, but the cause of bliss, but it is just the appearance of boulder

144
00:17:31,720 --> 00:17:47,880
is bliss, or in other words, like saying, diabetes is sugar, something like that,

145
00:17:47,880 --> 00:17:56,880
it is not sugar, but it is caused by sugar, but we might say diabetes is sugar, something

146
00:17:56,880 --> 00:18:03,360
like that, so here also, although the word pateja samobara means, conditions, things or

147
00:18:03,360 --> 00:18:11,880
results, we must understand that it means the conditions or causes and not the results,

148
00:18:11,880 --> 00:18:20,680
so that is explaining in paragraph 16, now the third meaning, that which is to be gone

149
00:18:20,680 --> 00:18:29,040
towards and which originates, states together, now here the word pateja means that

150
00:18:29,040 --> 00:18:38,640
which causes to arise, that which produces, and some means together, so which produces

151
00:18:38,640 --> 00:18:46,920
things together, things that arise together, that which is to be gone towards, that is

152
00:18:46,920 --> 00:19:00,320
the meaning of the word pateja, which is to be gone towards really means, now in Buddhism,

153
00:19:00,320 --> 00:19:16,000
the theory of causation is like that, on a call of many causes, there are many results,

154
00:19:16,000 --> 00:19:25,680
we will come to that later, not today, so there is not one cause and one effect, but

155
00:19:25,680 --> 00:19:31,840
not one cause and many effects or many causes and one effect, but there is many causes

156
00:19:31,840 --> 00:19:44,480
and many effects, that is accepted by Buddhism, so here which is to be gone towards means,

157
00:19:44,480 --> 00:19:49,760
the conditions which are two or three conditions meet together, they must meet together

158
00:19:49,760 --> 00:19:58,360
in order to produce the result, like three sticks, say put together, so they must come

159
00:19:58,360 --> 00:20:03,640
together and they must depend on each other in order to produce something, so that is

160
00:20:03,640 --> 00:20:11,160
why they are called here, which is to be gone towards, and when they produce states, they

161
00:20:11,160 --> 00:20:23,960
produce states together, not only, not one stick only, now the obvious example is the

162
00:20:23,960 --> 00:20:31,200
relinking, so when karma produces its effects, then there is relinking, so relinking means

163
00:20:31,200 --> 00:20:41,080
what, at the same moment there is consciousness, there are mental factors and there are

164
00:20:41,080 --> 00:20:53,640
some major bonds of karma, so they arise together, so but it just some more gara means

165
00:20:53,640 --> 00:21:02,360
here, that which is to be gone towards and which originates states together, now by

166
00:21:02,360 --> 00:21:10,800
this definition in what is meant causes conditions, because they originate or they produce

167
00:21:10,800 --> 00:21:18,720
other states, and then the fourth meaning that which depending upon one another, it is

168
00:21:18,720 --> 00:21:27,400
more or less the same as the third one, originates evenly and together, here same is made

169
00:21:27,400 --> 00:21:36,600
to mean two, two things evenly and together in the book it is translated equally, so

170
00:21:36,600 --> 00:21:45,360
evenly means not piecemeal, not one and then next one and then next one, but two or three

171
00:21:45,360 --> 00:21:59,400
things together, so not piecemeal, and together means not one after the other, whether

172
00:21:59,400 --> 00:22:15,480
the product or the result is consciousness or material property, it arises with other things

173
00:22:15,480 --> 00:22:21,560
too, right, when consciousness arises there are mental factors, and when material properties

174
00:22:21,560 --> 00:22:30,360
are produced, they are produced in what we call kalabas, the groups, so they arise in

175
00:22:30,360 --> 00:22:36,680
groups, and so they are together, so they are not produced just one at a time, but they

176
00:22:36,680 --> 00:22:41,920
are produced, so if there are eight material properties and the eight are produced at the

177
00:22:41,920 --> 00:22:50,960
same moment, so they are produced evenly, they are produced together, so by this definition

178
00:22:50,960 --> 00:23:02,120
to the conditions are made, so the definition three and four tells us that the potential

179
00:23:02,120 --> 00:23:14,440
samubara means conditions, right, here we do not have to have recourse to the figurative

180
00:23:14,440 --> 00:23:26,800
usage, so here directly the word, but it just samubara means conditions, now in all

181
00:23:26,800 --> 00:23:40,800
these definitions that you see, so that means a group of conditions or group of states

182
00:23:40,800 --> 00:23:49,920
that are conditions, that is why the singular number is used here, although actually it

183
00:23:49,920 --> 00:24:03,680
means multiplicity of conditions and also multiplicity of states that are conditions, now

184
00:24:03,680 --> 00:24:12,160
what about the potential samubara as a doctrine, here the potential samubara means not

185
00:24:12,160 --> 00:24:19,200
doctrine, but it causes party fence, but we call this doctrine also potential samubara,

186
00:24:19,200 --> 00:24:31,920
so as a doctrine, what should we call it by in English, it is translated as dependent

187
00:24:31,920 --> 00:24:45,840
or determination, dependent arising, dependent coarising, dependent coarising is the

188
00:24:45,840 --> 00:24:51,280
right translation, because there are, you see they are together, right, but I have

189
00:24:51,280 --> 00:25:00,960
one one fear about using coarising, because somebody might take that coarising means

190
00:25:00,960 --> 00:25:12,840
a cause and effect arising together, it does not necessarily mean that the cause and effect

191
00:25:12,840 --> 00:25:23,600
arise together, together here means just the effects arising in a group or something

192
00:25:23,600 --> 00:25:30,880
like that, not cause and effect arising together, but there is cause and there is effect

193
00:25:30,880 --> 00:25:37,040
and effect consists of say more than one thing, so that is what is meant by together in

194
00:25:37,040 --> 00:25:47,360
these two definitions, but using the term dependent doesn't make it clear on the dependent

195
00:25:47,360 --> 00:26:02,960
coarising, it was just coarising then you could see good, but it is not a cause and effect.

196
00:26:02,960 --> 00:26:09,360
We often say coarigination, coarising, is that any better?

197
00:26:09,360 --> 00:26:18,360
Ah, I don't think so, because you know, two meanings are given to the word kubara here and

198
00:26:18,360 --> 00:26:26,280
in English we cannot, we cannot have one word which means two things, so we have to choose

199
00:26:26,280 --> 00:26:30,280
one or the other, arising or origination.

200
00:26:30,280 --> 00:26:40,240
But now we say that creepy, twinging arises from craving, is that not a cause or relationship,

201
00:26:40,240 --> 00:26:45,720
you are saying it is not a cause or relationship, there is cause or relationship between

202
00:26:45,720 --> 00:26:57,360
these, but sometimes, no, there are, we call them links, say, ignorance and relationship

203
00:26:57,360 --> 00:27:02,560
between ignorance and formations, formations and consciousness and so on.

204
00:27:02,560 --> 00:27:14,360
Some, some relations are as producer and produced, but most of them are, like, arising

205
00:27:14,360 --> 00:27:18,400
together and helping each other.

206
00:27:18,400 --> 00:27:31,680
So, coarising can mean, cause and effect arising together, but not always, we mean sometimes

207
00:27:31,680 --> 00:27:32,680
separation.

208
00:27:32,680 --> 00:27:44,040
That's right, yeah, because let's say, the relationship between mental formations and consciousness,

209
00:27:44,040 --> 00:27:51,720
mental formations means karma and consciousness means a result in consciousness, so they

210
00:27:51,720 --> 00:27:57,720
belong to different times, they don't, they don't arise together, but they are, they

211
00:27:57,720 --> 00:28:05,320
are related as cause and effect.

212
00:28:05,320 --> 00:28:16,160
Then, consciousness and namaru-bar namaru-bar mentality and materiality, then in that case,

213
00:28:16,160 --> 00:28:21,840
they arise together, consciousness and mentality and materiality arise together.

214
00:28:21,840 --> 00:28:30,120
And there, their relationship is not, not producer and the produced, but just help us,

215
00:28:30,120 --> 00:28:33,640
helping each other, supporting each other.

216
00:28:33,640 --> 00:28:42,440
So, that is why, it is important to, to understand, but it's just a mubara with reference

217
00:28:42,440 --> 00:28:51,280
to the botana, that is why the botana is given in this book, so we'll come to this later.

218
00:28:51,280 --> 00:28:58,840
Only when you understand, with reference to botana, you really understand, but it's a

219
00:28:58,840 --> 00:28:59,840
mubara.

220
00:28:59,840 --> 00:29:05,640
Otherwise, there will be something missing in, in your knowledge of understanding of

221
00:29:05,640 --> 00:29:15,960
potential mubara, because not, not, we, we, we, we think that, say, when we see, when

222
00:29:15,960 --> 00:29:20,760
there is a, ignorance, there is, there are formations, when there is formation, when there

223
00:29:20,760 --> 00:29:27,400
are formations, there is consciousness, we normally think that one is caused by the other,

224
00:29:27,400 --> 00:29:31,840
it is not the case with every of the links.

225
00:29:31,840 --> 00:29:38,600
Some links are related as cause and effect, and some are, not, not cause and effect, but

226
00:29:38,600 --> 00:29:47,720
just supporting each other, like people, a group of people doing the same work, so they,

227
00:29:47,720 --> 00:29:52,400
they help each other, they support each other, and then they do the work.

228
00:29:52,400 --> 00:30:02,400
So, those relationship are to be understood with reference to botana, because of relations,

229
00:30:02,400 --> 00:30:07,000
the 34 causal relations given in this chapter.

230
00:30:07,000 --> 00:30:20,400
So this is a bedroom, understanding, and then we can go to the, the chapter itself.

231
00:30:20,400 --> 00:30:41,040
Now, let me see, so in the paragraph 16, something 18, and so on, if it is difficult there

232
00:30:41,040 --> 00:30:50,360
to understand, just come back here, there are so many, say, parenthesis is, is, is, is

233
00:30:50,360 --> 00:31:11,800
this, brackets, and then it is difficult to read actually, and then pali words, and, so we

234
00:31:11,800 --> 00:31:20,320
will do it next week, so we may not not read that if we, conditions, the next week,

235
00:31:20,320 --> 00:31:47,200
but you, you can find them, let me see, yeah, 611, so they are 24 conditions, so please,

236
00:31:47,200 --> 00:31:54,960
please read about, you will have to read more on this week, so you have to be, end of

237
00:31:54,960 --> 00:32:12,240
the 24 conditions, so if you read with this, I think, I think it will be a great help,

238
00:32:12,240 --> 00:32:19,960
and in the word explanations, there is something like, what you call, I think, I think

239
00:32:19,960 --> 00:32:45,400
it is difficult, I think, I think, I think it is difficult, I think it is difficult,

