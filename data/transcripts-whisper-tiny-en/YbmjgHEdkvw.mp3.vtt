WEBVTT

00:00.000 --> 00:28.000
Good evening, everyone.

00:28.000 --> 00:52.000
Welcome to our evening session.

00:52.000 --> 01:06.000
There was a meeting for that McMaster University for something called Pac-Bake.

01:06.000 --> 01:13.000
And it's something about an inclusive community.

01:13.000 --> 01:41.000
It's sort of in response to racism, bigotry, fear and discomfort in the community.

01:41.000 --> 01:55.000
People feeling excluded or threatened, disregarded.

01:55.000 --> 02:06.000
Is even here in Canada we have bullies.

02:06.000 --> 02:15.000
We have the rising of views.

02:15.000 --> 02:17.000
The Buddhism we don't really talk about people.

02:17.000 --> 02:21.000
When we get right down to it, it's not about people being bullies.

02:21.000 --> 02:24.000
It's about the rising of views.

02:24.000 --> 02:26.000
And they can be contagious.

02:26.000 --> 02:31.000
So they might arise in this person and they might arise in that person.

02:31.000 --> 02:34.000
And they're contagious.

02:34.000 --> 02:36.000
And they require breeding grounds.

02:36.000 --> 02:43.000
So they require a supportive condition.

02:43.000 --> 02:50.000
Really, if you look at problems even societal problems, political problems,

02:50.000 --> 03:03.000
from a phenomenological point of view, an experiential point of view.

03:03.000 --> 03:06.000
It's not so difficult to understand how they form.

03:06.000 --> 03:14.000
They're not as complicated as we think, far as mysterious as unknowable and insolvable.

03:14.000 --> 03:36.000
As hopelessly complex, as we often have, as they appear, you'll just suddenly see an onslaught of violent bigotry.

03:36.000 --> 03:42.000
You'll see changes, trends and disdirection of that direction.

03:42.000 --> 03:46.000
You'll wonder where they're coming from.

03:46.000 --> 03:57.000
The site all sorts of socioeconomic issues that are catalysts for you.

03:57.000 --> 04:06.000
So we can find much simpler reasons if we look at what's really going on in the mind, in the individual.

04:06.000 --> 04:14.000
To understand how it's not just one person, a view is not isolated to an individual.

04:14.000 --> 04:18.000
To make sense, you can say, well, that's just one person.

04:18.000 --> 04:20.000
It's infectious.

04:20.000 --> 04:35.000
And yes, there are socioeconomic, political, environmental issues that help or hinder the formation of these views.

04:35.000 --> 04:54.000
And those views and emotions and attachments, addictions and versions and so on.

04:54.000 --> 05:02.000
But the root cause is the mind.

05:02.000 --> 05:09.000
And so even the work that we have to is not that difficult to understand.

05:09.000 --> 05:16.000
I got last night we screened a documentary and I had a little bit of a disagreeing that was a director.

05:16.000 --> 05:19.000
Not purposefully.

05:19.000 --> 05:29.000
I suggested something about because her film was quite gentle.

05:29.000 --> 05:33.000
It's about the prison system in America, but it was quite gentle.

05:33.000 --> 05:39.000
Didn't actually go into the prisons and it dealt more with people.

05:39.000 --> 05:51.000
And just letting people talk and watching what people lived and the things they said about the prisons.

05:51.000 --> 06:04.000
And I suggested whether that's a valid way of sort of with the suggestion that changing transforming energies.

06:04.000 --> 06:13.000
This idea of rather than engaging directly with the enemy or the problem or those people who are problems,

06:13.000 --> 06:24.000
rather than engaging and opposing and protesting, whether there's another way.

06:24.000 --> 06:34.000
And I mentioned how we can see or we can see a hint of the problem with protests.

06:34.000 --> 06:43.000
It's somewhat clear I think these days with this much talked about election in America.

06:43.000 --> 06:49.000
And there was so much opposition to the man who's going to be the president.

06:49.000 --> 06:55.000
So much hatred towards him and people would say justified hatred.

06:55.000 --> 07:11.000
But anger, maybe they wouldn't say hatred that they'd say anger, vilification, exposition, exposing his faults.

07:11.000 --> 07:17.000
Repeating his flaws, risk repeating all the bad things about him and yet he still won, right?

07:17.000 --> 07:21.000
There was an intense opposition to this man.

07:21.000 --> 07:31.000
And so there's certainly other reasons for that, but it certainly didn't make him go away, right?

07:31.000 --> 07:37.000
People don't crawl back into the woodwork when you vilify them.

07:37.000 --> 07:41.000
It's interesting.

07:41.000 --> 07:56.000
There's one of the Jatakas I think, and I think it's a story that has been told in different cultures.

07:56.000 --> 08:05.000
And I think it's just a myth I'm not sure where it comes from, but I think it's told as one of the Jatakas are told by the Buddha as a story.

08:05.000 --> 08:15.000
So someone here probably knows it and can remind me where it's from.

08:15.000 --> 08:22.000
Up in heaven, in heaven of the 33, there's this King Sakka is the King of the Gods.

08:22.000 --> 08:24.000
It was just a wonderful person.

08:24.000 --> 08:34.000
He's got this beautiful throne and everyone loves him and they made him, I guess, this incredible massive throne of all thrones.

08:34.000 --> 08:58.000
And one day the angels were, some angels were walking by the throne and they saw this ungliest, ugly little angels or demons up on the throne.

08:58.000 --> 09:07.000
And they scolded him and they said, get off, how dare you sit on that throne, that throne is not for you and they were quite upset.

09:07.000 --> 09:13.000
And so they vilified him and the more they spoke badly to him, the bigger he grew, the more grotesque he became.

09:13.000 --> 09:22.000
Until all the angels were gathered around him, trying to shame this demon, get a quite upset.

09:22.000 --> 09:26.000
Then he just kept getting bigger and bigger and bigger.

09:26.000 --> 09:32.000
Then finally Sakka, King of the Angels, came to the call demover and he came over and saw.

09:32.000 --> 09:37.000
Then he knew right away what sort of demon this was.

09:37.000 --> 09:46.000
He walked over and he put his robe over one shoulder just there.

09:46.000 --> 09:53.000
Sort of a respectful gesture and then he went down on one knee and he bowed before the demon and he said,

09:53.000 --> 09:59.000
my name is Sakka, or I think he even said, my name is Mugova or something.

09:59.000 --> 10:09.000
He gave his personal name and he said, maybe be happy and the demon shrunk.

10:09.000 --> 10:17.000
And he repeated and he behaved in such a respectful way that the demon eventually just disappeared.

10:17.000 --> 10:21.000
That's one of these parables.

10:21.000 --> 10:30.000
And it's curious that that should be told that it lends sort of to this idea of feeding into the problem.

10:30.000 --> 10:34.000
Our opposition to something feeds it quite often.

10:34.000 --> 10:36.000
Not obviously quite often.

10:36.000 --> 10:43.000
And it's an interesting question of when it feeds and when it doesn't.

10:43.000 --> 11:00.000
And how desire, I think the least we can say is that it ties into the idea that desire doesn't lead to success or obtaining the object of your desire.

11:00.000 --> 11:13.000
It adds something creates vigilance and the desire and a focus on the object of the desire.

11:13.000 --> 11:17.000
But there are many other conditions that are required.

11:17.000 --> 11:23.000
And so you might say that it's not usually opposition or something that makes it grow or something.

11:23.000 --> 11:32.000
But it's certainly not enough. One thing someone to go with, one thing is to change.

11:32.000 --> 11:40.000
It makes so pertinent that this concept, this core Buddhist concept of the three characteristics of all things.

11:40.000 --> 11:45.000
In permanence.

11:45.000 --> 11:48.000
Good things will change, bad things will change.

11:48.000 --> 11:52.000
And uncertainty in the same way.

11:52.000 --> 11:58.000
Suffering that you can't fix and make things exactly the way you want.

11:58.000 --> 12:05.000
Because without these kinds of understandings we get the false security of stability.

12:05.000 --> 12:15.000
And we get this false idea that we can fix things and that we can, things are going to be all right.

12:15.000 --> 12:18.000
And such that we're shocked when things go wrong.

12:18.000 --> 12:22.000
We have this happily ever after mentality.

12:22.000 --> 12:31.000
Listen to a lot of spiritual people say everything happens for a reason or everything will turn out right in the end.

12:31.000 --> 12:40.000
Everything is going to be all right to allow ourselves with these pseudo-spiritual views that are from Buddhist point.

12:40.000 --> 12:47.000
As a Buddhist we're just listening and saying no, no, no, no, you don't get it.

12:47.000 --> 12:49.000
There's no reassurance.

12:49.000 --> 12:51.000
We're not all going to become enlightened.

12:51.000 --> 12:54.000
We're not all going to heaven.

12:54.000 --> 12:57.000
Some of us might very well go to hell.

12:57.000 --> 13:06.000
We'll be on our way there in the handbasket.

13:06.000 --> 13:18.000
And non-self, especially non-self, the inability to control and to affect the change that you want.

13:18.000 --> 13:23.000
And there are traditions that have a sense of this Christian prayer.

13:23.000 --> 13:35.000
But God granted me the certainty, the things I can't change, the strength to accept the things I can't.

13:35.000 --> 13:44.000
The strength to change, the things I can change, the certainty, the patience to bear with those I can't.

13:44.000 --> 13:47.000
And the wisdom to know the difference or something like that.

13:47.000 --> 13:51.000
This sense that you can't change everything.

13:51.000 --> 13:56.000
You can't, you can't always get what you want.

13:56.000 --> 14:04.000
So the basic realization of non-self.

14:04.000 --> 14:14.000
But I want to go deeper and with the core idea here is that there is great work that can be done.

14:14.000 --> 14:23.000
And it's often not involved with wishing for certain results, but it's about understanding the way the world works.

14:23.000 --> 14:25.000
And that's what meditation is all about.

14:25.000 --> 14:36.000
You talk about meditation being only used for your own freedom from suffering and inner peace kind of thing.

14:36.000 --> 14:51.000
There's so much outer peace that comes from inner peace or so much outer peace, external peace, like peace of your surroundings, peaceful relationships, peaceful interactions with society.

14:51.000 --> 15:01.000
And peace that you bring to society and to the world through wisdom.

15:01.000 --> 15:07.000
So meditation isn't the practice of peace, it's the practice of wisdom.

15:07.000 --> 15:13.000
And wisdom brings peace, brings inner peace and outer peace.

15:13.000 --> 15:25.000
And so anyway, I didn't quite finish about this documentary. I had this idea that her documentary was this sort of peaceful way of making people, opening people's eyes to something.

15:25.000 --> 15:53.000
You know, opening our eyes to how we deal with problems and how society is geared towards pretending problems don't exist and perhaps even ignoring the problems with excessive greed.

15:53.000 --> 16:17.000
I'm trying to get rid of low class, a low class element of society, a colored black, indigenous minority, creating laws and systems that just put them all in jail.

16:17.000 --> 16:29.000
Anyway, she didn't agree with me. Not entirely. She got what I was saying, but wanted to be clear that she was pro activist and she said anger can be useful.

16:29.000 --> 16:36.000
So I think we have a disagreement to that extent.

16:36.000 --> 16:47.000
But it's okay, there's room for everybody. I'm not interested exactly in talking about activism, whether it's right or wrong.

16:47.000 --> 16:54.000
I want to talk about this inner change and inner.

16:54.000 --> 17:10.000
The inner workings of change that come about from change that comes about from practice of meditation, let's say.

17:10.000 --> 17:26.000
And not only, but also the acts that stem from meditation through seeing things clearly. It's such a simple thing, but let's put it clearly by watching your stomach.

17:26.000 --> 17:49.000
When you watch your stomach rise and fall, the things you can learn from simply watching your stomach rise and fall and trying, learning, training to see the rising and falling just as rising.

17:49.000 --> 18:03.000
Or anything like that. When you feel pain or any object and you think even the sound of my voice saying hearing, listening to the sound of my voice arise and sees it's quite jarring when you do that.

18:03.000 --> 18:06.000
Actually, it's not that much fun.

18:06.000 --> 18:25.000
Because we want to be able to anticipate, it forces us to wake up, and it's not peaceful or simple to try and anticipate when I'm going to talk next.

18:25.000 --> 18:36.000
What I'm going to say next is much more fun to just let your mind wander and leave your mind go free.

18:36.000 --> 18:39.000
Of course, we know the problems with that.

18:39.000 --> 18:56.000
We'll have all sorts of bad inclinations that we listen to, stress and suffering, judgment and so on.

18:56.000 --> 19:11.000
Learning to see them as they are. The mushroom effect, the snowball effect comes from that. It expands outward, incredibly.

19:11.000 --> 19:27.000
If you look at the Buddha, one person, the great things he did and think of how many people, this one guy, how many lives he's changed.

19:27.000 --> 19:47.000
What people like, you know, if it learned it, one person, they had help. They certainly had help. But they became catalysts for great change, good and bad.

19:47.000 --> 19:58.000
That the examination of how this works, quite interesting. How a person makes change.

19:58.000 --> 20:06.000
I'd like to argue for real importance for us not to focus on changing political systems.

20:06.000 --> 20:17.000
It's communism, better than capitalism. Judging political figures or parties.

20:17.000 --> 20:25.000
But talking about views, talking about emotions, talking about attachments.

20:25.000 --> 20:41.000
Talking about ignorance and delusion, talking about impermanence, suffering, talking about non-self, ego, conceit.

20:41.000 --> 20:59.000
There's that's all it is. If you look at the environment, we're all worried about climate now and we should become quite close to destroying our home.

20:59.000 --> 21:15.000
Not that the earth is going to disappear, but it's going to become less and less inhabitable as we go along.

21:15.000 --> 21:43.000
Without greed, it would have never come to this. Without greed, we wouldn't have any of us been born, but without the intense greed, the greed in society is getting worse and worse and more and more caught up by and obsessed with getting what we want as quickly as possible.

21:43.000 --> 22:01.000
Never stopping to look, stopping to examine.

22:01.000 --> 22:20.000
In the context of society, in the context of the world, even in context of the universe, with all its many realms, heavens, hells, animal realm, god realm, ghosts, demons,

22:20.000 --> 22:38.000
never kinds of beings there may be. There's a whole world that's going on behind the scenes, behind the people, behind the structures and establishments, behind the societies and politics and economics.

22:38.000 --> 22:52.000
There's a whole world that we rarely see, let alone investigate, study, that's going on all the time with all of us, right?

22:52.000 --> 23:08.000
We're none of us categorically different from each other. There are no enemies. There are only enemies in our hearts.

23:08.000 --> 23:13.000
And I think there's a great power there. There's a great potential for power.

23:13.000 --> 23:28.000
Not that I think we all have to make our lives become Gandhi or Martin Luther King or Buda, but we all have energy.

23:28.000 --> 23:48.000
It's a question of where we focus our energy. We all have commitments and carmas that we've performed that aren't we're now having to pay off or deal with and live with. But we many of us have the energy to learn about ourselves, to study.

23:48.000 --> 24:00.000
And then to focus on that, focus our lives on that, on sharing meditation practices, but on also focusing on peace in every situation.

24:00.000 --> 24:17.000
When you get in an argument with someone, is it more important to be right and to win the argument or is it more important to have a peaceful relationship again with this person that is somehow escalated out of control, which one's better?

24:17.000 --> 24:30.000
Do you want political change? What do you do to change society? What sort of a society do you want? Do you want a society where all the people you choose win the elections?

24:30.000 --> 24:46.000
Or do you want a society that is peaceful? In many ways, the US presidential election has been a big diversion for people from what's most important.

24:46.000 --> 24:53.000
I mean, not to say that politics doesn't have power, but we've given it power.

24:53.000 --> 25:02.000
And obviously the work that people did to try and stop people from happening didn't work.

25:02.000 --> 25:17.000
As far as I can see, it sounds like it's quite problematic, the outcome, acrimony. There's now a deeper divide in society.

25:17.000 --> 25:28.000
In our campus, people were putting up posters, something about why is it only racism when white people do it?

25:28.000 --> 25:34.000
So it's like white supremacist or white pride or something.

25:34.000 --> 25:42.000
There's a big hubbub about big to do about what to do about this.

25:42.000 --> 25:49.000
There are many things we can do in society, but the question is where they come from and what they're for.

25:49.000 --> 26:00.000
Any things we can do in our lives, we have all sorts of choices to make in our families, in our jobs, a career study, what we're going to do.

26:00.000 --> 26:07.000
The real question is why are we doing it? It's easy to lose sight of what's most important.

26:07.000 --> 26:16.000
It's easy to lose sight of the fact that this earth isn't going to last forever. No matter what we do, it's just going to burn to a crisp in a couple of billion years.

26:16.000 --> 26:22.000
Not much we can do stuff that. So what does that mean and how do we relate to that?

26:22.000 --> 26:26.000
It's most important.

26:26.000 --> 26:30.000
Of course, I submit to you most important to the mind.

26:30.000 --> 26:39.000
Much more important than this earth, this earth is a temporary home.

26:39.000 --> 26:42.000
We'll always go according to our karma.

26:42.000 --> 26:49.000
If the earth is getting worse and we keep coming back again and again, it's a sign we might be on the wrong path.

26:49.000 --> 26:56.000
Whether the earth gets worse and worse place to live, it's not really relevant to us.

26:56.000 --> 27:00.000
Acceptance so far as we're a part of the problem.

27:00.000 --> 27:06.000
We can have heaven on earth if we work for it.

27:06.000 --> 27:13.000
We can certainly have peace on earth if we work for it. Big if.

27:13.000 --> 27:19.000
But the great thing is that one should never as a Buddhist should never be discouraged.

27:19.000 --> 27:30.000
Suppose some people would disagree or argue against such an outlook, but it's most important about you.

27:30.000 --> 27:39.000
We meet with the people who are similar to us, we meet and we join like Satka, this angel.

27:39.000 --> 27:44.000
It's called the heaven of the 33, apparently, because there were 33 of them here on earth.

27:44.000 --> 27:51.000
They did so many good things for society. The things they did always helping people.

27:51.000 --> 28:02.000
They had this, their society was kind of a poor, corrupt village that was run by this crony, headmen guy.

28:02.000 --> 28:10.000
They had this market place that was not really well-run and they had to stamp down the grass to find a place to set up their wares.

28:10.000 --> 28:14.000
This guy was, Satka was a merchant.

28:14.000 --> 28:21.000
He went and he sort of stamped down his grass and set out a place where he could set up his wares.

28:21.000 --> 28:24.000
He went back to get it to his wagon to get whatever he was selling.

28:24.000 --> 28:28.000
By the time he came back, someone had taken his spot.

28:28.000 --> 28:30.000
What do you do when someone takes your spot?

28:30.000 --> 28:35.000
He went and made another spot.

28:35.000 --> 28:42.000
And then the same thing happened, but this time he was more watching, I think, to see if someone else would take it.

28:42.000 --> 28:45.000
He was happy. He said, okay, I've done it.

28:45.000 --> 28:51.000
Then he made a third spot and he realized, wow, I can do this for people.

28:51.000 --> 28:53.000
This is a service.

28:53.000 --> 28:57.000
He started from there and he started doing all these great social works,

28:57.000 --> 29:02.000
eventually building up a pavilion so they could have a proper market.

29:02.000 --> 29:06.000
Even when it was raining during the rainy season,

29:06.000 --> 29:14.000
it would be safe from the sun and not have to deal with tall grasses or whatever.

29:14.000 --> 29:24.000
Eventually, they became these, with all these friends, these 33 large group committee,

29:24.000 --> 29:26.000
I guess.

29:26.000 --> 29:32.000
Eventually they got in big trouble with the headmen who liked keeping people under his thumb

29:32.000 --> 29:41.000
and not having this more socialist, helping each other way of living.

29:41.000 --> 29:45.000
It's much better to have people in fear of you, right?

29:45.000 --> 29:50.000
And so he told the king that they were.

29:50.000 --> 29:56.000
These were thieves and murderers and the king had them killed by trampling.

29:56.000 --> 30:03.000
They had been trampled by an elephant.

30:03.000 --> 30:05.000
And so the elephant came out and they said,

30:05.000 --> 30:10.000
no where to go because they were all tied up and tossed into this elephant's

30:10.000 --> 30:12.000
romping ground.

30:12.000 --> 30:15.000
They said, let's send loving kindness to the elephant.

30:15.000 --> 30:19.000
And so all 33 of them practiced wishing this elephant well,

30:19.000 --> 30:22.000
and the elephant didn't dare to step on the vibes they were giving out

30:22.000 --> 30:27.000
or so strong that the elephant wouldn't step on this.

30:27.000 --> 30:28.000
And their wives as well.

30:28.000 --> 30:29.000
Their wives got involved with it.

30:29.000 --> 30:33.000
There's an interesting story about Saka's wives.

30:33.000 --> 30:38.000
Anyway, the king made them, these 33 sort of the committee to run the village

30:38.000 --> 30:42.000
and through the headmen in jail and maybe kill them, I don't know.

30:42.000 --> 30:45.000
Kings were not that nice back then.

30:45.000 --> 30:47.000
But their wives got involved as well.

30:47.000 --> 30:51.000
Which was a big deal because women weren't all that.

30:51.000 --> 30:54.000
They didn't actually want the women to get involved.

30:54.000 --> 30:55.000
They were quite sexist.

30:55.000 --> 30:59.000
But I think the way they phrased it was,

30:59.000 --> 31:04.000
they didn't want to have to deal with sort of the sexual or the romance.

31:04.000 --> 31:08.000
They wanted to be, you know, men wanted to be away from women

31:08.000 --> 31:09.000
so they could do their good work.

31:09.000 --> 31:12.000
Because if that women will distract us, something like that.

31:12.000 --> 31:16.000
Anyways, if not overtly sexist,

31:16.000 --> 31:18.000
it was at least unfeeling.

31:18.000 --> 31:23.000
But the women were clever and his wife made a

31:23.000 --> 31:28.000
found a way to get involved and to trick him.

31:28.000 --> 31:31.000
I don't remember.

31:31.000 --> 31:35.000
Maybe he helped or to trick the other men who were sexist.

31:35.000 --> 31:37.000
I think that sort of was.

31:37.000 --> 31:40.000
And finally he said, oh, we need a,

31:40.000 --> 31:43.000
now there was someone, anyway, someone helped.

31:43.000 --> 31:44.000
I don't know, but it's actually.

31:44.000 --> 31:49.000
Women got involved and everybody went to have it.

31:49.000 --> 31:55.000
I'm sorry, the point to someone was starting out to go into too much detail.

31:55.000 --> 31:59.000
The point being they did this all from some greatness that came within

31:59.000 --> 32:02.000
without actually confronting this corrupt headman

32:02.000 --> 32:07.000
and they ended up winning the day, not just by meditating.

32:07.000 --> 32:10.000
And I think there's a point there for us as well,

32:10.000 --> 32:13.000
is that you don't have to think of us just as meditators.

32:13.000 --> 32:16.000
And then we go and live our lives as horrible people.

32:16.000 --> 32:18.000
Then we bring meditation into our lives

32:18.000 --> 32:20.000
and our whole lives become meditative,

32:20.000 --> 32:22.000
how we interact with other people.

32:22.000 --> 32:27.000
We no longer try to compete or try to correct other people,

32:27.000 --> 32:30.000
fight, protest.

32:30.000 --> 32:33.000
We think of everyone may they be happy.

32:33.000 --> 32:35.000
We wish for everyone to be happy

32:35.000 --> 32:39.000
and we work person by person, relationship,

32:39.000 --> 32:45.000
and it's contagious, right?

32:45.000 --> 32:47.000
We plant seeds in others.

32:47.000 --> 32:50.000
They're seeds grow.

32:50.000 --> 32:56.000
Change people's lives.

32:56.000 --> 32:59.000
So much comes from the mind.

32:59.000 --> 33:10.000
Manot bangamadhamma, everybody said,

33:10.000 --> 33:13.000
all dhammas, meaning all good things and bad things come from the mind.

33:13.000 --> 33:22.000
So, therefore, it behooves us all to focus and to exert ourselves

33:22.000 --> 33:30.000
in cultivating understanding,

33:30.000 --> 33:31.000
understanding of reality,

33:31.000 --> 33:34.000
even understanding of your stomach and telling it,

33:34.000 --> 33:37.000
it'll teach you so many lessons.

33:37.000 --> 33:39.000
It'll whip you into shape,

33:39.000 --> 33:43.000
it'll have you crying out of frustration,

33:43.000 --> 33:48.000
can not inability to make it do it more.

33:48.000 --> 33:55.000
It'll teach you your whole body, reality will teach you all you need to know.

33:55.000 --> 33:56.000
So, there you go.

33:56.000 --> 33:58.000
That's the dhamma for tonight.

33:58.000 --> 34:06.000
Thank you all for coming out.

34:06.000 --> 34:09.000
You can go ahead.

34:09.000 --> 34:11.000
Edward Gray?

34:11.000 --> 34:14.000
Yeah, I'm going to answer questions,

34:14.000 --> 34:15.000
but I'd rather you don't say.

34:15.000 --> 34:19.000
You don't keep meditating, okay?

34:19.000 --> 34:23.000
No, you should go.

34:23.000 --> 34:49.000
Thank you.

34:49.000 --> 35:00.000
So, he came to ask the Buddha,

35:00.000 --> 35:02.000
what we call the Sakapanha Sutta,

35:02.000 --> 35:04.000
and I believe it was during,

35:04.000 --> 35:07.000
I'm pretty sure it was during the Sakapanha Sutta.

35:07.000 --> 35:09.000
The questions of Sakapanha,

35:09.000 --> 35:10.000
that he became a mind.

35:10.000 --> 35:11.000
It's quite a good Sutta.

35:11.000 --> 35:18.000
The Mahasi Sayada wrote a book on an explanation or anything.

35:18.000 --> 35:22.000
The commentary on the Sakapanha Sutta,

35:22.000 --> 35:26.000
when it's made trade are coming, I don't know.

35:26.000 --> 35:30.000
If you believe the Sutta's, you'll have to have a life span of 100,

35:30.000 --> 35:32.000
you know, 10,000 years.

35:32.000 --> 35:44.000
People will be living to be 10,000 years before my day it comes.

35:44.000 --> 35:49.000
Which may not be, you know,

35:49.000 --> 35:53.000
may think, well, that's just a myth or legend, may not be.

35:53.000 --> 35:55.000
Potential for us to live thousands of years,

35:55.000 --> 35:59.000
it's not unthinkable with just looking at science.

35:59.000 --> 36:01.000
Of course, today's science couldn't do,

36:01.000 --> 36:08.000
but if we stop messing up our planet and fighting over the color of our skin of all things,

36:08.000 --> 36:13.000
whether people wear this or that dress.

36:13.000 --> 36:16.000
Well, believe in this or that imaginary person,

36:16.000 --> 36:19.000
you know, if we got rid of all these imaginary people,

36:19.000 --> 36:23.000
imaginary friends,

36:23.000 --> 36:25.000
and being somewhat facetious,

36:25.000 --> 36:29.000
we got rid of God, I think we can do a lot now.

36:31.000 --> 36:35.000
It's not the only problem, but I'm not keen on the whole God thing.

36:35.000 --> 36:38.000
I don't think it's done as much good.

36:38.000 --> 36:39.000
It's a good question.

36:39.000 --> 36:41.000
What do you think God has done for society?

36:41.000 --> 36:44.000
I wonder what the deists would say.

36:44.000 --> 36:46.000
What has God done for the world?

36:46.000 --> 36:51.000
What has a belief in God done for God done for society?

36:56.000 --> 36:59.000
Some could just spend a half an hour meditation.

36:59.000 --> 37:02.000
Let's do that.

37:02.000 --> 37:05.000
Some could think it's his voice after something,

37:05.000 --> 37:15.000
sometimes the voice doesn't work.

37:15.000 --> 37:17.000
The problem with those files, they're not all right.

37:17.000 --> 37:20.000
I've looked at them in some of them aren't full and complete,

37:20.000 --> 37:23.000
but I got them from another website.

37:23.000 --> 37:27.000
If any of those files are not complete,

37:27.000 --> 37:30.000
that may know because I have the complete files on my computer,

37:30.000 --> 37:36.000
and I upload the right one.

37:36.000 --> 37:38.000
Yeah, you guys are all into Nyanavira.

37:38.000 --> 37:40.000
There's a whole group of you.

37:40.000 --> 37:44.000
Someone just asked me today about Nyanavira.

37:44.000 --> 37:48.000
And I said, I don't, I'm not all that keen on him.

37:48.000 --> 37:54.000
I don't know that much about him, but I'm somewhat suspicious.

37:54.000 --> 38:00.000
Again, I don't like to talk too much, but he apparently has,

38:00.000 --> 38:03.000
I think, and I'm not apparently in pretty sure I read,

38:03.000 --> 38:06.000
he has this view of the teacher, Samupada,

38:06.000 --> 38:08.000
that it's only this life.

38:08.000 --> 38:13.000
Once people start saying that kind of thing, I do.

38:13.000 --> 38:17.000
There's a big group of people who believe for some reason

38:17.000 --> 38:23.000
that the teacher, Samupada, only relates to this life.

38:23.000 --> 38:27.000
Not that it doesn't, but only that.

38:27.000 --> 38:31.000
Anyway, he's apparently fairly controversial.

38:31.000 --> 38:35.000
Apparently, again, I don't know too much about him.

38:35.000 --> 38:42.000
I have a very select group of texts that I'm at all interested in.

38:42.000 --> 38:46.000
I'm quite particular with the things I read and study.

38:46.000 --> 38:48.000
It goes somewhat like this.

38:48.000 --> 38:51.000
Mahasi Sayada, besides the Topitika and the commenters.

38:51.000 --> 38:58.000
Okay, so the Topitika, the commentaries, will be Sudimanga,

38:58.000 --> 39:04.000
Mahasi Sayada, Loompo Chodok, because he has so many talks

39:04.000 --> 39:11.000
and he's just really scholar monk and adjunct.

39:11.000 --> 39:20.000
Okay, that's a good way to get me to leave the group.

39:20.000 --> 39:27.000
All right, if there aren't any other serious questions,

39:27.000 --> 39:32.000
I'm going to go for tonight.

39:32.000 --> 39:40.000
All right, have a good night everyone.

39:40.000 --> 40:05.000
Thank you very much.

