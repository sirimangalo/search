1
00:00:00,000 --> 00:00:06,760
Hello and welcome back to our study of the Dhamma Padha.

2
00:00:06,760 --> 00:00:19,440
Today we continue with verse 218, which reads as follows.

3
00:00:19,440 --> 00:00:35,400
Bhagavad-Jato, Anakati, Manasaja, Portosia, Kami, Su-apati, Badhajito, Goodhang Soto-Dhi,

4
00:00:35,400 --> 00:00:39,720
Ujati.

5
00:00:39,720 --> 00:00:59,080
Chandajato, one who's mind, one who is intent upon, or inclined towards Anakati,

6
00:00:59,080 --> 00:01:04,280
that which is indescribable.

7
00:01:04,280 --> 00:01:18,240
One is intent upon that which is indescribable, inclined towards it, keen on it.

8
00:01:18,240 --> 00:01:40,600
Anasaja, Portosia, Portosia, if one's mind leaps or plunges into, really gets into, is thrilled

9
00:01:40,600 --> 00:01:53,280
by, is excited by, is fully into, we would say, in English, into that, the Anakata, the

10
00:01:53,280 --> 00:02:09,760
indescribable, Kami, Su-apati, Badhajito, if one's mind is not bound up, or tied down by

11
00:02:09,760 --> 00:02:32,920
kama, by sensuality, Ujang Soto, Ujang Soto-Dhi, Ujati, such a person is called, Ujang Soto,

12
00:02:32,920 --> 00:02:52,840
one who is going upstream, one who has a stream going upwards.

13
00:02:52,840 --> 00:03:09,440
So this story was taught, this verse was taught in response to a story about a elder monk

14
00:03:09,440 --> 00:03:24,280
who was getting old, and his students, his Santa Viharika, the monk who lived with him

15
00:03:24,280 --> 00:03:33,280
so in the time of the Buddha, it was established that new monks should spend some time

16
00:03:33,280 --> 00:03:37,200
with a senior monk in order to learn how to become a good monk.

17
00:03:37,200 --> 00:03:43,760
So, they would spend time, sometimes even staying in the same bedroom, they wouldn't

18
00:03:43,760 --> 00:03:48,720
have beds, they wouldn't have the same bed, but they would live quite close so that

19
00:03:48,720 --> 00:03:56,480
the young monk could learn how to live from the old monk, just watching and following

20
00:03:56,480 --> 00:04:02,640
and really taking care of the older monk and so on, so it became sort of mentorship.

21
00:04:02,640 --> 00:04:06,440
So it might not be correct to say this was a teacher student relationship, but it was

22
00:04:06,440 --> 00:04:11,680
a mentor and dependent relationship.

23
00:04:11,680 --> 00:04:18,800
It was explained like that, just said to you, Ujang Soto, one who lives with you, learn

24
00:04:18,800 --> 00:04:27,240
how you would be a good monk, and so the monks, I guess there was more than one living

25
00:04:27,240 --> 00:04:38,160
with him, asked him, and they said, bend or bhocir, what sort of benefits have you gained

26
00:04:38,160 --> 00:04:46,640
from the practice, what sort of results, anything special?

27
00:04:46,640 --> 00:04:52,360
Now this monk had indeed practiced and indeed gained something quite special from the

28
00:04:52,360 --> 00:05:11,160
dhamma, he had become what we call an anagami, anagami, sorry, anagami, anagami means one

29
00:05:11,160 --> 00:05:29,000
who doesn't come back, it's the third of four stages of enlightenment.

30
00:05:29,000 --> 00:05:35,400
One one first, experience is nibana, they're called a Sotapana, here we have this word

31
00:05:35,400 --> 00:05:44,280
Sotag, and Sotam means a stream, that word is used because it means someone who is not

32
00:05:44,280 --> 00:05:51,560
going to fall back, they're being pulled towards enlightenment, there's the word stream

33
00:05:51,560 --> 00:06:00,680
to indicate that they're on their way, it's like they get on an escalator and you can't

34
00:06:00,680 --> 00:06:09,520
go back down again, it just takes you off.

35
00:06:09,520 --> 00:06:14,840
But such a person still has a lot of defilements left potentially, and so they may still

36
00:06:14,840 --> 00:06:23,280
get angry, they may still be craving things, they may still get worried or may still,

37
00:06:23,280 --> 00:06:34,360
they won't be jealous or stingy, but they can still crave and get upset, sad, frustrated

38
00:06:34,360 --> 00:06:38,640
and bored, but far less than ordinary people, they've come a long way and they've given

39
00:06:38,640 --> 00:06:47,160
up any kind of wrong view about the dhamma, any doubt about the dhamma, any wrong practices

40
00:06:47,160 --> 00:06:52,360
they have no inclination to do things that are useless, they'll never make a mistake

41
00:06:52,360 --> 00:06:58,520
about what is useful and what is useless thinking, something that is useless is actually

42
00:06:58,520 --> 00:07:11,160
spiritually beneficial, like rituals or rules, no, they know the way to become enlightened,

43
00:07:11,160 --> 00:07:17,040
but the higher stages start to get rid of more of the defilements, and so we have what

44
00:07:17,040 --> 00:07:24,600
to call the sakadagami, someone who will come back, but will come back only once, that's

45
00:07:24,600 --> 00:07:29,160
because they've worked harder than the sotapana, they've moved on from there and they've

46
00:07:29,160 --> 00:07:33,400
continued the work to the point where they're only going to come back once and then

47
00:07:33,400 --> 00:07:42,960
after that they'll free themselves completely from suffering, but an anagami or the third

48
00:07:42,960 --> 00:07:51,680
age, an anagami has gotten rid of completely any kind of sensual desire, they've come

49
00:07:51,680 --> 00:08:03,600
to see it for the illusion that it is, and any kind of aversion or they're disliking

50
00:08:03,600 --> 00:08:14,800
of things, any hatred of others, any anger or frustration or irritation.

51
00:08:14,800 --> 00:08:20,920
And so this is who this monk wants, he was actually quite advanced, anyone who can claim

52
00:08:20,920 --> 00:08:26,640
to be free from all any form of sensual desire is quite impressive, any kind of aversion

53
00:08:26,640 --> 00:08:35,120
or irritation, it's quite a powerful state.

54
00:08:35,120 --> 00:08:44,640
But he thought to himself, you see, because there's a fourth stage, and the fourth stage

55
00:08:44,640 --> 00:08:53,640
is very powerful, they say someone who becomes an arahat, see an arahat is someone who

56
00:08:53,640 --> 00:09:01,880
has gotten rid of all delusion as well, so an anagami might still get distracted, might

57
00:09:01,880 --> 00:09:09,760
still have a conceit, a conceit is another big one, an anagami might still be conceited,

58
00:09:09,760 --> 00:09:19,040
might still be proud of themselves, might still be have low self confidence, feeling bad

59
00:09:19,040 --> 00:09:24,400
about the fact they're not yet an arahat. And it seems that that's what this monk was

60
00:09:24,400 --> 00:09:29,680
feeling, he felt kind of guilty for not being an arahat, because he thought to himself,

61
00:09:29,680 --> 00:09:35,520
you know, with an arahat, an arahat, if a lay person becomes an arahat, they have to

62
00:09:35,520 --> 00:09:41,920
ordain, if they don't, they say they will die, there's just two ways either they pass

63
00:09:41,920 --> 00:09:48,040
away, or they become a monk and they go on arms run, so they don't have to engage in practices

64
00:09:48,040 --> 00:09:57,240
that they just see no use in, I mean like like getting a job and seeking out food and that

65
00:09:57,240 --> 00:10:06,640
sort of thing, but an anagami, there are many lay people who became anagami, and so he

66
00:10:06,640 --> 00:10:14,480
thought to himself, this is just, I'm no better than a lay person, even a lay person could

67
00:10:14,480 --> 00:10:21,960
become an anagami, and he thought to himself, I'm not going to declare, you know, my attainment

68
00:10:21,960 --> 00:10:26,960
until I become an arahat, while wait until I become an arahat and then I'll, then I'll let

69
00:10:26,960 --> 00:10:31,240
these guys know what I've gotten out of the practice, I really wait till I've realized

70
00:10:31,240 --> 00:10:37,960
the complete goal, and so he didn't say anything, and again and again time and again they

71
00:10:37,960 --> 00:10:43,240
would ask him and they started to get worried thinking, why isn't he answering?

72
00:10:43,240 --> 00:10:47,920
He must be, he must have gained nothing from the practice and feeling guilty about it,

73
00:10:47,920 --> 00:10:55,680
so they were concerned and they asked him again and again and never said anything, and

74
00:10:55,680 --> 00:11:03,600
then he died, he died without becoming an arahat and so when he passed away he was an anagami,

75
00:11:03,600 --> 00:11:09,480
he didn't return, where did he go, he went to what we call the sudawasa, sudha means

76
00:11:09,480 --> 00:11:21,400
pure and a wasa means abode or dwelling, so the pure abodes, they are Brahma world, but

77
00:11:21,400 --> 00:11:27,720
they're very special Brahma world, they're a world that beings exist who have become anagami

78
00:11:27,720 --> 00:11:33,360
and that's it, only people have become anagami, it's only beings that have become anagami

79
00:11:33,360 --> 00:11:45,240
is a reborn there, it's a very special realm of existence and these monks were quite upset

80
00:11:45,240 --> 00:11:49,960
because they didn't know any of this and they thought their teacher had or their mentor,

81
00:11:49,960 --> 00:11:55,800
the guy who they'd looked up to and got nothing out of the practice, they felt bad for

82
00:11:55,800 --> 00:12:02,880
him, maybe they felt bad for themselves for wasting their time on such a teacher and they

83
00:12:02,880 --> 00:12:08,480
went to the Buddha and they were crying, apparently, and moaning and wailing and dawn,

84
00:12:08,480 --> 00:12:19,080
and the Buddha said, what's this all about, it's an old vendor, both sir, our poor teacher

85
00:12:19,080 --> 00:12:27,720
just passed away and the Buddha said, well, haven't I taught that this is the nature

86
00:12:27,720 --> 00:12:33,360
of things to pass away, why are you crying about someone dying, aren't you real monks,

87
00:12:33,360 --> 00:12:41,400
like hasn't your teacher taught you anything, crying about death when death is such an obvious

88
00:12:41,400 --> 00:12:51,360
and glaringly certain part of life, something to be shocked about or upset about and they

89
00:12:51,360 --> 00:12:57,440
said, well, it's not that, we know that, Benarbo, sir, it's just that he died without

90
00:12:57,440 --> 00:13:03,440
getting anything, he asked him again and again and he didn't say anything, he must have

91
00:13:03,440 --> 00:13:09,400
died and who knows where he went because he had no benefit from the practice and the

92
00:13:09,400 --> 00:13:16,600
Buddha explained to him, he said, oh no, no, my son has been reborn in the Sudawasa,

93
00:13:16,600 --> 00:13:21,760
he just was embarrassed because he was only Anana Gami and he thought he'd wait until

94
00:13:21,760 --> 00:13:30,600
he became Anara hut and then he taught this verse explaining that really they should

95
00:13:30,600 --> 00:13:40,960
have known or could have known or maybe not based on their own state of mind but it would

96
00:13:40,960 --> 00:13:48,400
be possible to know and there's no possibility that that monk could have gone anywhere

97
00:13:48,400 --> 00:13:55,680
else because not of some category but because of his state of mind, his qualities of

98
00:13:55,680 --> 00:14:03,080
mind and that's what this verse is meant to highlight.

99
00:14:03,080 --> 00:14:07,360
So we can get a couple of things from this story.

100
00:14:07,360 --> 00:14:12,200
The first is, of course, the most obvious one and the main lesson of the story, I think,

101
00:14:12,200 --> 00:14:21,280
is not to be complacent and Anana Gami is such a high attainment and yet even people

102
00:14:21,280 --> 00:14:26,600
or maybe especially people who become Anana Gami, one thing it does to them is make them

103
00:14:26,600 --> 00:14:28,920
not complacent.

104
00:14:28,920 --> 00:14:35,240
I think there was a story about some Anana Gami that became complacent and the Buddha

105
00:14:35,240 --> 00:14:47,920
had to scold them, admonish them, but here we have, this shows that this monk pointed

106
00:14:47,920 --> 00:14:54,480
out an important lesson that even when you've gotten to that state, you have to remember

107
00:14:54,480 --> 00:14:59,720
that you still have defilements, why would it means that there's a difference between

108
00:14:59,720 --> 00:15:15,040
an Anana Gami and Anarahant, is that you still have to concede and restlessness and ignorant.

109
00:15:15,040 --> 00:15:30,080
You still have desire for form and formlessness and no central desire that.

110
00:15:30,080 --> 00:15:35,000
And so it's a really good example for us, for people who have maybe not gained anything

111
00:15:35,000 --> 00:15:43,760
from the practice to remind ourselves, you know, some people become content just with calm,

112
00:15:43,760 --> 00:15:52,000
they practice meditation for a bit and they feel calm and they become content with that.

113
00:15:52,000 --> 00:15:57,600
Some people gain insight, gain some good understanding about how to live their lives and

114
00:15:57,600 --> 00:16:00,080
they become content with that.

115
00:16:00,080 --> 00:16:06,160
They become content with something that's really ineffective in the long run.

116
00:16:06,160 --> 00:16:14,520
You know, it will have an effect on our lives, but it has no potential for, or it has

117
00:16:14,520 --> 00:16:25,840
no guarantee, it has no power to free from suffering.

118
00:16:25,840 --> 00:16:30,880
You feel calm and it's very easy to lose that and get back to square one.

119
00:16:30,880 --> 00:16:39,400
Again, insight, maybe it changes your course in life, which is a very good thing, but who knows

120
00:16:39,400 --> 00:16:41,400
how long that will last?

121
00:16:41,400 --> 00:16:47,920
It will go away, you'll forget about it.

122
00:16:47,920 --> 00:16:53,560
Then Buddha said, Asan-tutī bhāhālau, you want to be a great being, you want to really

123
00:16:53,560 --> 00:16:58,240
do something great, something really and truly great.

124
00:16:58,240 --> 00:17:04,000
You have to be discontent, not discontent in the way we normally think about it.

125
00:17:04,000 --> 00:17:12,000
The Buddha taught and praised contentment, but discontent about your own spiritual attainments,

126
00:17:12,000 --> 00:17:20,600
your own quality of mind, until you have freed yourself from all bad habits, bad qualities

127
00:17:20,600 --> 00:17:27,360
of mind, till you've freed yourself from all ignorance, and you've come to understand

128
00:17:27,360 --> 00:17:36,120
reality just as it is, and you should not be complacent, you should not be content with

129
00:17:36,120 --> 00:17:38,600
what you've done.

130
00:17:38,600 --> 00:17:44,120
Of course, people who have attained so Dapana become complacent with good reason, because

131
00:17:44,120 --> 00:17:51,920
it's quite a change, it's quite an accomplishment, and your whole being feels different

132
00:17:51,920 --> 00:18:03,920
at the peace and the security, the feeling of stability of mind.

133
00:18:03,920 --> 00:18:09,360
Sakadagami Anagami is still possible to become complacent, so just a reminder, of course,

134
00:18:09,360 --> 00:18:19,360
that in our practice, if even an Anagami is able to admonish themselves not to be complacent,

135
00:18:19,360 --> 00:18:26,400
you should remember the reason why we're practicing, it's not just because it's beneficial

136
00:18:26,400 --> 00:18:33,240
to us, that's one way of looking at it, but also because we have things that are unbeneficial

137
00:18:33,240 --> 00:18:37,000
that we need to address.

138
00:18:37,000 --> 00:18:47,480
We really do have an imperative task to perform, that is to learn, to study, to become

139
00:18:47,480 --> 00:18:58,200
familiar and understand and overcome our bad habit, our unwholesome qualities of mind.

140
00:18:58,200 --> 00:19:00,520
So that's the first lesson.

141
00:19:00,520 --> 00:19:05,360
The second lesson, I think, is a little more subtle, and may or may not be there, but

142
00:19:05,360 --> 00:19:16,040
it appears that the Buddha is perhaps, or you could take this as an admonishment, or

143
00:19:16,040 --> 00:19:32,200
as a lesson for us, not to put too much emphasis on titles, proclamations, identities.

144
00:19:32,200 --> 00:19:39,720
We have these four stages of enlightenment, it's very easy for people to obsess about them.

145
00:19:39,720 --> 00:19:47,600
People saying to, I've heard people say, my goal is to become a Sotapana in this life,

146
00:19:47,600 --> 00:19:48,600
something.

147
00:19:48,600 --> 00:19:56,520
That's not, I don't mean to criticize that, but it really isn't a great way to focus.

148
00:19:56,520 --> 00:20:00,600
I mean, it isn't the most important thing to focus on.

149
00:20:00,600 --> 00:20:03,840
Your focus should be on, of course, being mindful.

150
00:20:03,840 --> 00:20:06,320
And this is a good example of the distinction.

151
00:20:06,320 --> 00:20:13,000
These monks were so focused on this idea, the switch that they could turn in their minds

152
00:20:13,000 --> 00:20:18,360
where they switched from thinking of him as an ordinary person to an enlightened person,

153
00:20:18,360 --> 00:20:22,440
that they never even saw how enlightened he was, right?

154
00:20:22,440 --> 00:20:27,240
Because if they lived in the same room with him, it's certainly possible that it's

155
00:20:27,240 --> 00:20:34,800
very, I mean, it is, maybe a third lesson, it is hard to know whether someone else is

156
00:20:34,800 --> 00:20:40,360
enlightenment, it's a very personal thing, but they're so close to him.

157
00:20:40,360 --> 00:20:45,480
And I think it's quite possible that had they not been so obsessed with trying to determine

158
00:20:45,480 --> 00:20:50,080
whether he was or wasn't, they would be able to instead be very mindful and become

159
00:20:50,080 --> 00:20:56,600
in tune with his state of mind and really get a sense that most likely he was not an

160
00:20:56,600 --> 00:21:09,800
ordinary being, I think it's very hard to mistake an Anagami for an ordinary being.

161
00:21:09,800 --> 00:21:13,760
But third lesson, it can be difficult.

162
00:21:13,760 --> 00:21:19,640
You see, even these monks who lived with the Anagami didn't know, and they didn't

163
00:21:19,640 --> 00:21:25,080
know because two reasons, one, because they weren't paying attention to, because it

164
00:21:25,080 --> 00:21:27,200
is very hard to know.

165
00:21:27,200 --> 00:21:31,680
The Dhamma is Pachatang, to be known for oneself.

166
00:21:31,680 --> 00:21:37,760
It really isn't always the best to proclaim your attainment, so the monks aren't allowed

167
00:21:37,760 --> 00:21:45,520
to talk about them with people who aren't monks, but even among monks, sometimes it's

168
00:21:45,520 --> 00:21:53,880
better just to focus more on qualities of mind, you know, focus more on what's important

169
00:21:53,880 --> 00:21:56,280
and focus more on oneself.

170
00:21:56,280 --> 00:22:01,960
Someone asks you, are you enlightened to say, why do you care about me, you know?

171
00:22:01,960 --> 00:22:07,160
You should turn it back on them and say, the only enlightenment you should be concerned

172
00:22:07,160 --> 00:22:11,840
with is your own.

173
00:22:11,840 --> 00:22:17,680
Sometimes people try to test, I don't get it so much, but I suppose I have, I don't

174
00:22:17,680 --> 00:22:21,600
notice it so much, but I've heard of monks getting tested.

175
00:22:21,600 --> 00:22:29,240
I think it happens a lot in Buddhist societies, Buddhist to know a lot, and they're very

176
00:22:29,240 --> 00:22:35,120
skeptical of other Buddhists, and so they go around to monks and they try to test them

177
00:22:35,120 --> 00:22:39,640
to see how enlightened they are.

178
00:22:39,640 --> 00:22:43,960
In some ways it's a good service, because it does test the monks, you know, they might

179
00:22:43,960 --> 00:22:50,360
try and make them an irritated or annoyed, but on the other hand it's really a waste

180
00:22:50,360 --> 00:22:52,160
of that person's life, you know?

181
00:22:52,160 --> 00:22:57,000
Why are they so focused on determining whether other people are enlightened?

182
00:22:57,000 --> 00:23:02,960
And it becomes a bit of a problem from the other end as well, and when someone does identify

183
00:23:02,960 --> 00:23:08,840
another person as an enlightened being correctly or incorrectly, I think it's very common

184
00:23:08,840 --> 00:23:16,960
to incorrectly or with insufficient evidence, identify someone else as being enlightened

185
00:23:16,960 --> 00:23:23,040
or oneself, even, I think it's possible to overestimate one's own attainment.

186
00:23:23,040 --> 00:23:29,080
And it becomes, you see, this is the thing about labels, it becomes an entity, a person

187
00:23:29,080 --> 00:23:36,280
who is an Arahan, a person who is an Anagami, and you put them up on a pedestal, and it

188
00:23:36,280 --> 00:23:38,360
makes you feel good if they're your teacher, right?

189
00:23:38,360 --> 00:23:44,400
My teacher is such and such, which is really awful because them being in such and such

190
00:23:44,400 --> 00:23:49,720
says nothing about you, even if they are.

191
00:23:49,720 --> 00:23:58,000
I've seen very pure monks who have very, very poor and impure students or followers,

192
00:23:58,000 --> 00:24:02,280
just because you follow the money even said it, just because you follow around, if you

193
00:24:02,280 --> 00:24:08,040
were to follow around holding on to my robe, I can't take you to Nibana, that's not

194
00:24:08,040 --> 00:24:12,040
what he said exactly, but basically that.

195
00:24:12,040 --> 00:24:18,240
It with me all day, hang out with me all day, there's nothing to you, it has potential

196
00:24:18,240 --> 00:24:25,920
to be completely unbeneficial when you could be off meditating and following my teachings.

197
00:24:25,920 --> 00:24:33,080
He did say that, basically.

198
00:24:33,080 --> 00:24:38,880
So there's the lessons from the story, don't be complacent, but also don't obsess

199
00:24:38,880 --> 00:24:42,760
about titles and categories and so on.

200
00:24:42,760 --> 00:24:46,640
The focus should be on what the verse focuses on, and why I think it may be very well

201
00:24:46,640 --> 00:24:51,440
with the Buddha, a part of the Buddha's lesson, this sort of admonishment not to do that.

202
00:24:51,440 --> 00:24:58,440
It's because of what the Buddha says, he says, look, of course he was an Anagami, he had

203
00:24:58,440 --> 00:25:07,000
all these qualities, where he had, that's not a lot of qualities, but this basic quality

204
00:25:07,000 --> 00:25:13,320
of mind, that distinguishes someone who is not coming back from someone who is coming

205
00:25:13,320 --> 00:25:39,800
back, so what is it?

206
00:25:39,800 --> 00:25:45,920
So Nibana Anakat is the thing which cannot be told, which cannot be explained, not

207
00:25:45,920 --> 00:25:59,880
be described, and apparently that's what ineffable means, so we can say it's the ineffable.

208
00:25:59,880 --> 00:26:05,360
It's very different, it's ineffable, the problem with those sorts of words is that it

209
00:26:05,360 --> 00:26:11,520
gives it this sort of mystery, which it really shouldn't, we should absolutely understand

210
00:26:11,520 --> 00:26:15,840
that that is an essential quality of it, that being ineffable.

211
00:26:15,840 --> 00:26:19,680
It doesn't mean it's something that if only we were smarter we could talk about it, we

212
00:26:19,680 --> 00:26:22,960
could explain or we could understand, we could conceive of it.

213
00:26:22,960 --> 00:26:33,760
It's like the inconceivable, it's a thing that is literally like actually inconceivable

214
00:26:33,760 --> 00:26:40,240
in the sense of it's not, there's no concept involved with it, there's no entity per se.

215
00:26:40,240 --> 00:26:48,160
It exists, but it exists in a different way than formed things that you can describe,

216
00:26:48,160 --> 00:26:58,000
conceive of, that they exist, and that's exactly what it is, it's that thing that is

217
00:26:58,000 --> 00:27:02,200
as those qualities, as the quality of being indescribable.

218
00:27:02,200 --> 00:27:09,200
It's the thing that is indescribable, it's the, it's out, it's that which is outside

219
00:27:09,200 --> 00:27:11,640
the realm of being indescribable.

220
00:27:11,640 --> 00:27:19,080
That's just what it is, it's not something mysterious, it's just that thing, they haven't

221
00:27:19,080 --> 00:27:23,040
made it much clearer by saying that, but I just want to explain that it's, it's not meant

222
00:27:23,040 --> 00:27:28,160
to make it mysterious or obfuscate the meaning or anything, it's just talking about

223
00:27:28,160 --> 00:27:37,960
what is different from everything else, here I am talking a lot about it, just trying

224
00:27:37,960 --> 00:27:48,080
to explain why it's what it is.

225
00:27:48,080 --> 00:27:54,640
And so Chandajata, Chandayas is an important word, he uses this I think because Chanda

226
00:27:54,640 --> 00:28:01,120
can be used both ways, Chandayas is often used as a replacement for desire, entire they

227
00:28:01,120 --> 00:28:05,760
say pajaya, which I think is a good, we don't really have a similar way of talking about

228
00:28:05,760 --> 00:28:13,120
things in, in English, but someone is pajaya, jaya is the heart, pause enough, pajaya

229
00:28:13,120 --> 00:28:21,440
means contentment or finding something to be enough, but they use it to mean liking, you

230
00:28:21,440 --> 00:28:34,600
know, if you're pajaya in regards to food, it means you like it.

231
00:28:34,600 --> 00:28:40,440
And so it's kind of used in a sense of liking something, but it more literally means interest

232
00:28:40,440 --> 00:28:48,360
or inclination, intention, in a sense of being intent on something.

233
00:28:48,360 --> 00:28:54,040
So one translator I think uses intent on the ineffable.

234
00:28:54,040 --> 00:29:02,520
Incline is a good word, because the Buddha uses this imagery of a tree leaning in one direction,

235
00:29:02,520 --> 00:29:11,200
and this is where the distinction is made here, someone who is inclined towards nibhana,

236
00:29:11,200 --> 00:29:18,200
is really very, and categorically different from someone who is inclined towards kama, towards

237
00:29:18,200 --> 00:29:19,520
sensuality.

238
00:29:19,520 --> 00:29:26,400
And that's the real distinction here, it's a very simple description of the quality

239
00:29:26,400 --> 00:29:40,400
of enlightenment that it inclines away from sensuality and towards the opposite of sensuality.

240
00:29:40,400 --> 00:30:01,160
And so this is, it's an important point about this, is that it's a description of the

241
00:30:01,160 --> 00:30:14,400
problem, what's wrong with us, with ordinary life, with ordinary inclination, because

242
00:30:14,400 --> 00:30:18,640
when you hear about nibhana, for example, when you hear about nibhana, when you hear

243
00:30:18,640 --> 00:30:26,720
about the path, when you hear about the direction, it's often quite hard to swallow,

244
00:30:26,720 --> 00:30:31,240
it's unpalatable, fears to me even.

245
00:30:31,240 --> 00:30:37,800
Some people who really like what the Buddha said will become inclined to practice, they

246
00:30:37,800 --> 00:30:47,280
meet Buddhists and they really feel attracted to the path and to the ideas behind it.

247
00:30:47,280 --> 00:30:52,480
But then they hear about nibhana and it scares them.

248
00:30:52,480 --> 00:30:57,400
What is this, I've gotten myself into, one meditator told me, wait, do you know what this

249
00:30:57,400 --> 00:30:58,400
means?

250
00:30:58,400 --> 00:31:00,880
Do you really get what we're doing here?

251
00:31:00,880 --> 00:31:08,240
It just shocked him and he couldn't go on, he couldn't get past this because he liked

252
00:31:08,240 --> 00:31:16,520
the idea of gaining benefit from meditation, but he understood what the meaning was here.

253
00:31:16,520 --> 00:31:31,600
You know, nibhana involves non-arising and so often our practice seems to be trying to

254
00:31:31,600 --> 00:31:44,800
change our opinion about nibhana, trying to like it, trying to incline towards it.

255
00:31:44,800 --> 00:31:54,000
We want to create in our minds this inclination towards nibhana, but it's not quite like

256
00:31:54,000 --> 00:31:57,000
that.

257
00:31:57,000 --> 00:32:07,200
It's in fact the disintegration that we take as our object.

258
00:32:07,200 --> 00:32:17,760
It's not that we practice liking it, it's not that we practice or I guess more common,

259
00:32:17,760 --> 00:32:28,320
it's not that we repress our distaste for nibhana, our distaste for having to give up

260
00:32:28,320 --> 00:32:35,760
all of this, all of what we enjoy, all of what we seek, all of what we cling to, all

261
00:32:35,760 --> 00:32:51,400
of what we are but the bandajita, all of what our minds are stuck on, are attached to.

262
00:32:51,400 --> 00:33:05,360
Our practice is understanding this taste, I mean the struggle in the mind of a Buddhist

263
00:33:05,360 --> 00:33:14,280
to come to turn with something as the goal that they really don't like, don't even like

264
00:33:14,280 --> 00:33:19,560
the idea that they might not admit it, they'll say yes, nibhana, but when they think

265
00:33:19,560 --> 00:33:24,360
about it, it scares them and it's repulsive.

266
00:33:24,360 --> 00:33:30,320
It's not about repressing that or trying to be the person who likes that, no, it's

267
00:33:30,320 --> 00:33:41,080
about looking at and that's the actual problem, looking at this, not just that, but looking

268
00:33:41,080 --> 00:33:47,800
at the whole reason why that is, why it is that we're disinclined towards something

269
00:33:47,800 --> 00:33:51,560
that really, I mean it's inevitable, how could you be disinclined, how could you

270
00:33:51,560 --> 00:34:03,240
hate it, you see because our, our, our distaste for it, fear of it, it's nothing to do

271
00:34:03,240 --> 00:34:04,240
with it.

272
00:34:04,240 --> 00:34:08,160
There's nothing to fear about nibhana, there's nothing fearsome, it doesn't have fangs

273
00:34:08,160 --> 00:34:18,880
or claws or poison, it's not going to say bad things about you or betray you, it's not

274
00:34:18,880 --> 00:34:31,440
even going to arise, no, it's all of the things that it stands in opposition to, all

275
00:34:31,440 --> 00:34:35,880
of those things again that we cling to.

276
00:34:35,880 --> 00:34:40,480
And so our whole of our practice, our practice, this is why our practice is not about

277
00:34:40,480 --> 00:34:45,600
nibhana, why is our practice about suffering?

278
00:34:45,600 --> 00:34:50,640
Because all of those things that we cling to are actually suffering are actually dukha,

279
00:34:50,640 --> 00:34:55,600
they may not be painful, not all the time, but they're caught up in pain, they're caught

280
00:34:55,600 --> 00:35:02,600
up in disappointment, they have no suka, dukha means they can't bring suka, they can't

281
00:35:02,600 --> 00:35:14,600
make us happy, they can't satisfy us, we strive for them again and again and again,

282
00:35:14,600 --> 00:35:18,720
and we always have in our mind this idea that they are going to, those things that I don't

283
00:35:18,720 --> 00:35:29,520
have yet are going to bring me happiness, not even intellectually, it's just an inclination,

284
00:35:29,520 --> 00:35:36,160
we're a tree leaning in the wrong direction, so that's the way we're going to fall.

285
00:35:36,160 --> 00:35:43,880
It's endless, there's no goal, we're like a dog that has a rash and it keeps scratching,

286
00:35:43,880 --> 00:35:53,040
because there's some sort of appeasement that comes from scratching, but the scratching

287
00:35:53,040 --> 00:35:55,160
just makes it worse.

288
00:35:55,160 --> 00:35:59,800
And so either they end up scratching themselves to death until they bleed and infected

289
00:35:59,800 --> 00:36:09,200
and die, or they stop, they find a way to stop themselves, perhaps they stop for some

290
00:36:09,200 --> 00:36:19,480
time because of the pain that comes from it, but there's no end, you can't get satisfied

291
00:36:19,480 --> 00:36:31,240
by getting the things you want.

292
00:36:31,240 --> 00:36:41,680
And so our practice is to learn this, is to understand this, is to become freed from the

293
00:36:41,680 --> 00:36:53,640
bind, from the shackles of central desire, they were really shackled to it, we can't be

294
00:36:53,640 --> 00:37:03,560
free, we can't be at peace, because our minds are forcing us really, we're prisoners

295
00:37:03,560 --> 00:37:13,640
to our own, but the abundance of our minds that are bound to sensuality, it's like we're

296
00:37:13,640 --> 00:37:17,440
being pulled in that direction by a strong rope and the meditation may need to rope in

297
00:37:17,440 --> 00:37:22,880
the other direction, and we pull ourselves until we incline towards the ineffable,

298
00:37:22,880 --> 00:37:28,200
who don't and that's how you become, who don't sort that, who don't sort if you think

299
00:37:28,200 --> 00:37:34,200
of it as against the stream, it's like a fish going against the stream, but if you think

300
00:37:34,200 --> 00:37:41,600
of it as upstream, I think it probably means more like has a stream going upwards, they're

301
00:37:41,600 --> 00:37:51,200
on the escalator upwards, they're heading in the right direction, they're bound in the

302
00:37:51,200 --> 00:37:56,600
right direction, why? Because their mind is inclined in the right direction, and that's

303
00:37:56,600 --> 00:38:04,680
really this emphasis and this focus on qualities, how does the mind incline, does your mind

304
00:38:04,680 --> 00:38:16,080
leap at the thought of peace or does it incline towards conflict? If someone hears someone

305
00:38:16,080 --> 00:38:21,120
does something, says something wrong, do you incline to correct them or make them punish

306
00:38:21,120 --> 00:38:34,040
them for it? When something pleasurable comes to incline to get it, to keep it, to get

307
00:38:34,040 --> 00:38:43,080
it as close to you and as much, possess it as much as possible, or do you see it mindfully,

308
00:38:43,080 --> 00:38:50,520
do you see it, experience it and let it come and let it go? One is the way to stress

309
00:38:50,520 --> 00:39:03,920
and present, the other is the way to freedom and peace. So I think focusing on, this

310
00:39:03,920 --> 00:39:11,400
is a very simple way to describe this distinction that you shouldn't be concerned that

311
00:39:11,400 --> 00:39:19,560
you don't like peace, that we aren't, that you aren't inclined towards things like nibana

312
00:39:19,560 --> 00:39:25,840
and freedom from suffering and cessation, to never be born again, shouldn't be concerned

313
00:39:25,840 --> 00:39:31,480
about that, that's the whole reason, that's the whole crux of this, that's what you have

314
00:39:31,480 --> 00:39:41,200
to look at. It's like a challenge in a way, pointing out nibana, pointing out freedom

315
00:39:41,200 --> 00:39:49,080
from suffering is a challenge, it shows you, you have to justify, and you really do have

316
00:39:49,080 --> 00:39:56,000
to examine because now you have an alternative, the thing you don't want, I want these,

317
00:39:56,000 --> 00:40:01,040
you have to be able to justify to yourself that it's better than this peaceful, this

318
00:40:01,040 --> 00:40:12,360
ultimate peace, ultimate freedom and so it means you examine, you don't worry about nibana

319
00:40:12,360 --> 00:40:21,040
for sure, not you examine the tree that you're in until you see there's no fruit on

320
00:40:21,040 --> 00:40:32,160
this tree and then you can fly away, so some thoughts on the verse for tonight, that's

321
00:40:32,160 --> 00:40:59,280
them upon the 218, thank you all for listening.

