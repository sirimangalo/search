1
00:00:00,000 --> 00:00:10,000
Hi, in this video I will close my series on how to meditate with an explanation of the fundamentals of meditation practice.

2
00:00:10,000 --> 00:00:21,000
The things which is necessary to keep in mind during the time we practice, to ensure that our practice is correct and that our practice will indeed bring fruit.

3
00:00:21,000 --> 00:00:33,000
The four fundamentals of meditation practice, as described in these series of videos, are one, the present moment, and two, continuity.

4
00:00:33,000 --> 00:00:43,000
Three, the acknowledgement, the clear thought, and four, balancing the mental faculties.

5
00:00:43,000 --> 00:00:51,000
These four fundamentals are necessary parts of the meditation practice, so it's not enough that we simply do the frustration.

6
00:00:51,000 --> 00:01:04,000
We do the walking, we do the sitting, and expect somehow to become free from suffering or free from the unpleasant, stressful condition which exists inside of ourselves.

7
00:01:04,000 --> 00:01:12,000
The first one, the present moment, means that at the time that we practice, we have to have presence, we have to be here and be now.

8
00:01:12,000 --> 00:01:23,000
We can be acknowledging after the fact or acknowledging before the fact, so when we walk, or when we do the frustration, or when we sit, we have to acknowledge as it happens.

9
00:01:23,000 --> 00:01:45,000
When the belly rises, we have to say to ourselves, as it rises. When it falls, we have to say to ourselves, when we're doing frustration, we have to keep our mind in the present moment, and acknowledge as the object appears as you object occurs in front of us.

10
00:01:45,000 --> 00:02:04,000
This is the first fundamental. The second fundamental is that we practice continuously, so it's not enough to say that in the morning, we're going to do 10 minutes walk, mindful frustration, then 10 minutes walking, 10 minutes sitting, and then the evening at night, we're going to do the same, but during the day, we're not going to be mindful.

11
00:02:04,000 --> 00:02:29,000
Now, for this technique to work, this has to be a basis for our life. Our life has to be a continuous series of creating clear thoughts about the situation, so when we meet with situations in our daily life, when people getting upset at us, or people saying things which make us upset, that we're able to say to ourselves upset, or simply hearing, hearing, hearing.

12
00:02:29,000 --> 00:02:36,000
We have to practice continuously. But most important is during the time we're practicing that it is also continuous.

13
00:02:36,000 --> 00:02:46,000
So when we do the mindful frustration, we're continuously mindful of the movement of the hands. When we finish that, we go right away to do walking meditation.

14
00:02:46,000 --> 00:02:56,000
Walking meditation should be done maybe 10 minutes, 15 minutes, sitting meditation, also 10 minutes, 15 minutes, have them equal, but have them connected.

15
00:02:56,000 --> 00:03:08,000
So continuity means that after we do the frustration, we do the walking. After we do the walking, we do the sitting. If we want to continue, we can do walking, sitting, walking, sitting continuously if we have time.

16
00:03:08,000 --> 00:03:17,000
But during the time we, from the time when we start, to the time that we finish, we are continuously mindful. This is another important foundation.

17
00:03:17,000 --> 00:03:27,000
Another part of this is, as I mentioned in daily life, we have to be mindful. During the time that we're not practicing, for it to be truly fruitful and truly have an impact on our lives.

18
00:03:27,000 --> 00:03:36,000
This is number two. Number three, we have to create the clear thought. So it's not enough for us to use the word as a mantra.

19
00:03:36,000 --> 00:03:50,000
As though it were all more though, as though it were Buddha, Buddha or Allah, Allah or Jesus, as we use these words as mantras to make ourselves peaceful and calm. This meditation, the purpose is not simply to make the mind peaceful and calm.

20
00:03:50,000 --> 00:03:59,000
The purpose is to seek clearly, to seek clearly the causes and conditions which are creating suffering inside. And it can be very unpleasant to face.

21
00:03:59,000 --> 00:04:10,000
But it requires this clear thought. We can't simply say to ourselves, rising, falling and not be aware of the rising and falling. We need three parts to the clear thought.

22
00:04:10,000 --> 00:04:20,000
One, we need to have the effort. We need to have put energy into it, put effort into sending the mind out to the object again and again and again.

23
00:04:20,000 --> 00:04:29,000
We're not simply saying rising, falling at the mouth, but not actually watching the stomach. We're saying it in the mind, but not actually watching, not with the mind down in the stomach.

24
00:04:29,000 --> 00:04:38,000
When we do mindful frustration, our mind should be with the hand. When we walk, our mind should be with the foot. When we sit, our mind should be with the rising and the fall.

25
00:04:38,000 --> 00:04:52,000
So this is the first part as we need effort. The second part is we need to focus. We need to know the rising and the falling. Know the object of our attention. Once we have the effort to put the mind there, we have to know from the beginning to the end.

26
00:04:52,000 --> 00:05:00,000
From the beginning, we know this is rising, we know this is falling. And the third part is that we need to actually say the word.

27
00:05:00,000 --> 00:05:14,000
When we have to, not with the mouth, say the word, but with the mind, say the word. Our mind should be in the stomach. And we should be saying at the stomach, rising in our mind, falling, rising, falling.

28
00:05:14,000 --> 00:05:30,000
These three together mean making a clear thought, one effort, two knowing, knowing the object, and three making the acknowledgement, saying to ourselves, creating the clear thought, like a man can. This is the third fundamental.

29
00:05:30,000 --> 00:05:42,000
The fourth fundamental in meditation practices that we balance our mental faculties. Now, according to the teaching, which is a foundation for this meditation practice, all people in the world have five mental faculties.

30
00:05:42,000 --> 00:05:52,000
But because they're not balanced correctly, it leads us to fall into all sorts of hindrances in our lives that we meet with all sorts of obstacles in our own minds.

31
00:05:52,000 --> 00:06:09,000
Obstacles like greed or craving, a doubt, a distraction, drowsiness, and so on. But when we are able to practice using mindfulness to balance these outcomes, we use the clear thought to balance our mental faculties.

32
00:06:09,000 --> 00:06:27,000
They will gain a power which is able to overcome stress, overcome worry, overcome fear, overcome depression, is able to overcome even anger and greed so that we're able to throw away with our addictions and do away with our frustration.

33
00:06:27,000 --> 00:06:41,000
The five mental faculties are faith or confidence, effort, mindfulness, concentration, and wisdom. And they balance together as follows. Confidence and wisdom have to balance together.

34
00:06:41,000 --> 00:06:58,000
If there's one has too much confidence, lots of confidence, but not a lot of wisdom. It leads one to be greedy, believing that whatever one wants one should go for, one should take. It leads to greed. If there's a strong wisdom, but weak confidence, this leads to doubt.

35
00:06:58,000 --> 00:07:09,000
So we may know a lot, but still doubt the things that we know. If we have effort and concentration have to balance each other. If we have strong effort, but we concentration,

36
00:07:09,000 --> 00:07:23,000
we tend to become distracted in the mind. If we have strong concentration, but weak effort will tend to become drowsy or lazy. And mindfulness, the fifth faculty is the one which balances the other side.

37
00:07:23,000 --> 00:07:35,000
When these are balanced, they gain a power. They become one and they have a power to cut off all sorts of unwholesome unpleasant states so that our minds are able to feel peaceful and calm all the time.

38
00:07:35,000 --> 00:08:02,000
So that we're able to live our lives rationally and maturely and with wisdom. If we can put together these four foundations or four fundamentals of meditation practice, in the practice according to, as I've described in the other videos, then surely, through time over time, we'll be able to do away with all sorts of unpleasant, stressful, suffering states of being.

39
00:08:02,000 --> 00:08:11,000
And that's all for now. I wish you all the best and the meditation practice bringing your fruit and bringing you happiness and peace in your life. Thank you.

