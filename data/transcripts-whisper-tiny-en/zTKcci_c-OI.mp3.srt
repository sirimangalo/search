1
00:00:00,000 --> 00:00:10,000
Bante, how would you advise someone to deal with low self-esteem from a meditation point of view?

2
00:00:10,000 --> 00:00:16,000
Really, I'm like this, there's a story that I read once.

3
00:00:16,000 --> 00:00:22,000
There's this funny Burmese monk in ancient old times anyway, like 100, 200 years ago,

4
00:00:22,000 --> 00:00:29,000
and it was very quick with responses to people.

5
00:00:29,000 --> 00:00:33,000
There was this one monk who kept teaching the same thing over and over again,

6
00:00:33,000 --> 00:00:41,000
and this teacher monk came up to him and found out that he was just teaching the same thing over and over again.

7
00:00:41,000 --> 00:00:47,000
His way of teaching was to tell people stories.

8
00:00:47,000 --> 00:00:55,000
So his story for this man was once there was a man who had a cure for everything,

9
00:00:55,000 --> 00:01:01,000
it was this magical elephant hide, the skin of the elephant,

10
00:01:01,000 --> 00:01:06,000
and he would carry it around with him and whenever anyone had any sickness, he was a doctor.

11
00:01:06,000 --> 00:01:11,000
I mean, whatever sickness anyone had, he'd give them a small piece of the elephant hide and it would cure them.

12
00:01:11,000 --> 00:01:18,000
So he went around carrying around this elephant hide with him.

13
00:01:18,000 --> 00:01:24,000
Eventually it got cumbersome to carry it around with him,

14
00:01:24,000 --> 00:01:28,000
and he wouldn't always carry it around and then he'd meet someone and they needed something.

15
00:01:28,000 --> 00:01:31,000
What can I do so that I always have this with me?

16
00:01:31,000 --> 00:01:36,000
Finally, he devised a way he made sandals out of the leather from the elephants,

17
00:01:36,000 --> 00:01:45,000
so he would walk around wearing these sandals and if anyone happened and needed to cut a small piece off the sandal and feed it to the person.

18
00:01:45,000 --> 00:01:50,000
And this went on for some time until someone finally realized that this was what they were doing.

19
00:01:50,000 --> 00:02:00,000
And I remember the exact story, but it was basically that they got fed up and they kicked him out and chased him away as some kind of crazy man.

20
00:02:00,000 --> 00:02:09,000
Because I mean, his stuff is on his feet, especially in a place like Burmese.

21
00:02:09,000 --> 00:02:15,000
He's considered an awful thing to do is to feed someone something that your feet have been walking on all day.

22
00:02:15,000 --> 00:02:22,000
So I'm kind of like that sort of teacher and that I really only have one thing to offer and that's meditation.

23
00:02:22,000 --> 00:02:27,000
But let's talk a little bit about not going to cop out here.

24
00:02:27,000 --> 00:02:35,000
So basically the basic answers do some meditation, but why does that help and what to focus on in the case of low self esteem?

25
00:02:35,000 --> 00:02:46,000
First thing is to try to get away from the idea of calling it low self esteem because it's still experiential, which means it doesn't last.

26
00:02:46,000 --> 00:02:51,000
It's impermanent, it's unsatisfying, it's uncontrollable, it comes and it goes.

27
00:02:51,000 --> 00:02:57,000
There's no way to fix it because it doesn't exist, there are only experiences.

28
00:02:57,000 --> 00:03:07,000
Once you understand those experiences, then they will go away or they will lose their power over you and slowly go away.

29
00:03:07,000 --> 00:03:13,000
Now low self esteem is related to the ego.

30
00:03:13,000 --> 00:03:18,000
It's about taking things personally instead of just seeing them as they are.

31
00:03:18,000 --> 00:03:25,000
So you have to also be able to see what are the things that you take personally.

32
00:03:25,000 --> 00:03:35,000
So maybe it's a sense of not having as many friends as others or not being as successful as others.

33
00:03:35,000 --> 00:03:46,000
It's popular as successful, as smart, as beautiful, as whatever, as spiritually advanced.

34
00:03:46,000 --> 00:03:53,000
As good at meditation, all of these things lead to low self esteem.

35
00:03:53,000 --> 00:04:00,000
So we have these judgments arise in the mind and they're not good, they're not helpful.

36
00:04:00,000 --> 00:04:04,000
And we had some important for us to remind ourselves of that and to see it.

37
00:04:04,000 --> 00:04:14,000
So to acknowledge knowing, knowing when you know, for example, you compare yourself to someone else.

38
00:04:14,000 --> 00:04:20,000
You have to know that realize that you have this awareness in your mind.

39
00:04:20,000 --> 00:04:24,000
So you look at someone, you realize they're better than me at this.

40
00:04:24,000 --> 00:04:26,000
Be aware of that.

41
00:04:26,000 --> 00:04:29,000
Be aware of that and I'll save yourself knowing, knowing.

42
00:04:29,000 --> 00:04:35,000
Just to keep it at just a knowledge so that when you're aware of this, you don't ever get upset about it.

43
00:04:35,000 --> 00:04:40,000
Because it's really a two step thing or three step thing.

44
00:04:40,000 --> 00:04:43,000
You have the experience of some state.

45
00:04:43,000 --> 00:04:48,000
You interpret that to mean that you're lesser than someone else.

46
00:04:48,000 --> 00:04:52,000
And then the third step is you dislike it.

47
00:04:52,000 --> 00:04:56,000
That disliking starts the snowball and gets out of hand and makes you quite upset.

48
00:04:56,000 --> 00:04:58,000
It can make you quite upset.

49
00:04:58,000 --> 00:05:02,000
Feeling that you are lesser, you are inferior.

50
00:05:02,000 --> 00:05:05,000
You have low self esteem.

51
00:05:05,000 --> 00:05:09,000
The worst you could do is start to think that you are someone who has low self esteem.

52
00:05:09,000 --> 00:05:14,000
Because then it's become, again, it's become personal.

53
00:05:14,000 --> 00:05:19,000
It's aggravating the condition, which is already ego based.

54
00:05:19,000 --> 00:05:26,000
You become attached to the idea that I am for I have low self esteem.

55
00:05:26,000 --> 00:05:30,000
When in fact, it's experiential, it comes and it goes.

56
00:05:30,000 --> 00:05:37,000
What you, you know, technically you should say is I have the tendency to feel inferior towards others.

57
00:05:37,000 --> 00:05:40,000
Which is a sort of, it's called conceit.

58
00:05:40,000 --> 00:05:48,000
The word conceit in poly is mana, which, which is how you esteem yourself.

59
00:05:48,000 --> 00:05:54,000
It can be overestimation, can be low, low estimation.

60
00:05:54,000 --> 00:05:56,000
And it can be even equal estimation.

61
00:05:56,000 --> 00:06:02,000
So even attaching to yourself is equal to others.

62
00:06:02,000 --> 00:06:04,000
This is still conceit.

63
00:06:04,000 --> 00:06:14,000
It can be whether you are better than someone or are worse than someone, but it's your estimation of yourself.

64
00:06:14,000 --> 00:06:18,000
Once you esteem yourself, it's better as worse as equal.

65
00:06:18,000 --> 00:06:20,000
That's already unwholesome.

66
00:06:20,000 --> 00:06:22,000
It's a cause for suffering.

67
00:06:22,000 --> 00:06:30,000
It's a cause for delusion in the mind because it relates to self.

68
00:06:30,000 --> 00:06:41,000
So another thing is to folk it to, to realign your priorities and to learn to let go, to analyze the sorts of things that you are concerned about.

69
00:06:41,000 --> 00:06:51,000
Like success in the world, beauty, physical beauty, wealth, popularity.

70
00:06:51,000 --> 00:06:53,000
All of these things are meaningless.

71
00:06:53,000 --> 00:07:05,000
The eight world, the conditions, right? There's fame and no fame and lack of fame.

72
00:07:05,000 --> 00:07:11,000
Or a highest steam, lowest steam of other people.

73
00:07:11,000 --> 00:07:23,000
Praise and blame. I'm getting this wrong. Yes, that.

74
00:07:23,000 --> 00:07:29,000
Fame.

75
00:07:29,000 --> 00:07:42,000
Yeah, fame and praise and, or fame is like station in life. Yes, which means fame is fame or, or, yeah, fame.

76
00:07:42,000 --> 00:07:48,000
Having many friends and also being highest of high station and high social status.

77
00:07:48,000 --> 00:07:50,000
Social status is maybe the best one.

78
00:07:50,000 --> 00:08:02,000
So high social status, low, versus low social status. Praise and blame, gain and loss and happiness and suffering.

79
00:08:02,000 --> 00:08:05,000
So these four good things and four bad things.

80
00:08:05,000 --> 00:08:08,000
They are in a constant flux.

81
00:08:08,000 --> 00:08:10,000
So they can come and go at any time.

82
00:08:10,000 --> 00:08:14,000
Some people have a lot of the good ones. Some people have a lot of the bad ones.

83
00:08:14,000 --> 00:08:18,000
But they can come and go. They're not, they're not static.

84
00:08:18,000 --> 00:08:22,000
So any of these things, none of these things should be given any weight.

85
00:08:22,000 --> 00:08:28,000
At the very, the very outset of your practice, even before you're able to cut off your attachment to these things,

86
00:08:28,000 --> 00:08:32,000
you should determine in your mind that these are not really worth clinging to.

87
00:08:32,000 --> 00:08:36,000
If you're going to cling to something and beginning cling to your meditation practice,

88
00:08:36,000 --> 00:08:41,000
because eventually that will lead you away from clinging.

89
00:08:41,000 --> 00:08:46,000
If you cling to these other things, but you can, you can help yourself by reminding yourself

90
00:08:46,000 --> 00:08:49,000
how they're not worth it, because they're subject to fluctuation.

91
00:08:49,000 --> 00:08:56,000
It's very difficult to get the good ones, very easy to lose them in many cases.

92
00:08:56,000 --> 00:09:04,000
So as a special case, be careful about worrying about worldly things,

93
00:09:04,000 --> 00:09:09,000
because they're not things that you can ever get independently.

94
00:09:09,000 --> 00:09:16,000
They can disappear at any time.

