1
00:00:00,000 --> 00:00:07,440
Okay, about duality, what meditating in loving kindness or what is considered good or

2
00:00:07,440 --> 00:00:12,340
wholesome create the opposite since from what I understand because we create good

3
00:00:12,340 --> 00:00:24,960
then we also create evil. Kind of like how every time you create a positive

4
00:00:24,960 --> 00:00:29,920
I, what is this that physics says when you create matter, it also creates

5
00:00:29,920 --> 00:00:37,680
anti matters that sort of the idea here. Interesting question, I mean that would

6
00:00:37,680 --> 00:00:43,800
kind of suggest in a general sense that when you create wisdom, you also are

7
00:00:43,800 --> 00:00:54,160
creating ignorance which I don't think is the case. No change occurs, right?

8
00:00:54,160 --> 00:01:00,200
If you just look in the physical realm, they say it's all just energy but it's

9
00:01:00,200 --> 00:01:09,520
changing its form. So it's not to say that when when you create one type of

10
00:01:09,520 --> 00:01:14,320
energy or one formation of energy, you're also creating its its opposite except

11
00:01:14,320 --> 00:01:19,520
maybe there is something called anti matter, I'm not sure. But the same

12
00:01:19,520 --> 00:01:26,680
really goes for the mind, you change the nature of the mind and so practicing

13
00:01:26,680 --> 00:01:30,960
loving kindness changes the layout of the mind. It doesn't create more hate in

14
00:01:30,960 --> 00:01:39,320
the mind. It changes the mind to be less hateful, less spiteful, less

15
00:01:39,320 --> 00:01:46,560
reactionary. It goes the same with the last question about memories

16
00:01:46,560 --> 00:01:53,440
associated with with physical pain. That's a change that occurs in the mind.

17
00:01:53,440 --> 00:01:57,280
It's the habit of the mind. So the idea of practicing loving kindness a

18
00:01:57,280 --> 00:02:06,560
daily is that it creates a habit of being loving and being kind. There's

19
00:02:06,560 --> 00:02:10,440
several other things that can be said about this. One is that loving kindness

20
00:02:10,440 --> 00:02:14,800
often simply represses negative feelings. Loving kindness itself isn't

21
00:02:14,800 --> 00:02:20,920
enough to destroy anger. It can it can affect the mind and make the person

22
00:02:20,920 --> 00:02:31,200
less prone to anger. But because it doesn't affect delusion, it the best it

23
00:02:31,200 --> 00:02:42,880
can do is well at least in the short term can to suppress the anger to

24
00:02:42,880 --> 00:02:46,360
suppress our responses. So when we feel angry and then we covered up with

25
00:02:46,360 --> 00:02:55,120
loving kindness, we haven't removed the causes of getting anger angry. And so

26
00:02:55,120 --> 00:03:03,800
we rather than developing a core sense of being a loving kind individual or

27
00:03:03,800 --> 00:03:10,480
core habit. We're creating the habit where of reacting to anger. When we get

28
00:03:10,480 --> 00:03:15,560
angry, covered up with love, we get anger, covered up with love. And that

29
00:03:15,560 --> 00:03:20,960
can as the potential to balloon. If you're not also practicing insight to

30
00:03:20,960 --> 00:03:25,480
see clearly and to destroy the roots of getting angry in the first place, which

31
00:03:25,480 --> 00:03:29,520
has nothing to do with love, the roots of getting anger or delusion, misunderstanding

32
00:03:29,520 --> 00:03:34,960
that somehow this is an entity that like for instance with the memory,

33
00:03:34,960 --> 00:03:40,040
thinking, Oh, this is me that experienced this in the past. And this is is

34
00:03:40,040 --> 00:03:51,280
something. And to see that it's just a thought that arises in ceases. So the root

35
00:03:51,280 --> 00:03:57,120
cause of the disliking is based on not seeing things as they are not seeing

36
00:03:57,120 --> 00:04:00,800
that it's simply something arises out of nothing disappears back into

37
00:04:00,800 --> 00:04:08,040
nothing. If you don't cut those off, then you can actually create a problem with

38
00:04:08,040 --> 00:04:14,120
loving kindness. If that's your, your only meditation source, though it

39
00:04:14,120 --> 00:04:22,800
eventually can change your habits, it, it, it doesn't, it doesn't get to the

40
00:04:22,800 --> 00:04:28,320
root. And so the point being that sometimes you'll find people who are very

41
00:04:28,320 --> 00:04:32,720
kind and generous and then suddenly explode. People who come to practice

42
00:04:32,720 --> 00:04:36,800
meditation, who have been practicing Buddhism in a general sense and practicing

43
00:04:36,800 --> 00:04:40,880
basic meditation, they can be very kind and gentle. And then when they come to

44
00:04:40,880 --> 00:04:45,200
meditate, they get very angry and think, wow, this meditation is terrible. I

45
00:04:45,200 --> 00:04:48,240
should go back to this loving kindness meditation, thinking that the

46
00:04:48,240 --> 00:04:52,480
meditation is actually bringing or is actually creating the anger. But all

47
00:04:52,480 --> 00:05:01,920
it's doing is is is bringing it back to the form. The only the only example of

48
00:05:01,920 --> 00:05:09,120
how I understand that in some sense one state of mind can bring its

49
00:05:09,120 --> 00:05:17,840
opposite is with anger and greed, with liking and disliking. And there was a

50
00:05:17,840 --> 00:05:20,480
question on the forum recently that I didn't answer or have an

51
00:05:20,480 --> 00:05:26,120
answer yet. But I think that's how I would answer this one. Some people think

52
00:05:26,120 --> 00:05:32,600
that everything is based on anger, right? Because you're, you only want

53
00:05:32,600 --> 00:05:36,240
something because you're unsatisfied with what you have. But you can turn

54
00:05:36,240 --> 00:05:42,200
the tables and say you're only angry about things because you like certain

55
00:05:42,200 --> 00:05:48,960
other things. So in some sense greed and anger create each other. And the

56
00:05:48,960 --> 00:05:52,880
more greedy you are, the more prone you are to anger. This I think is born out

57
00:05:52,880 --> 00:05:57,920
in meditation practice. You can see that you go from one to the other quite

58
00:05:57,920 --> 00:06:05,520
quickly. And that they they do prompt each other. That is, that's why

59
00:06:05,520 --> 00:06:10,400
craving is is considered the cause of suffering. This is a really profound

60
00:06:10,400 --> 00:06:20,560
teaching. Think about in our society how how how opposite we are thinking

61
00:06:20,560 --> 00:06:24,040
is. We think that one thing things is the cause for happiness, right? It's

62
00:06:24,040 --> 00:06:27,680
very difficult to understand how one thing things could be the cause of

63
00:06:27,680 --> 00:06:32,760
suffering. But basically what it's saying is because craving liking certain

64
00:06:32,760 --> 00:06:37,840
things creates partiality and partiality by definition has two sides. And so

65
00:06:37,840 --> 00:06:41,800
this is where I would say there is a duality with when you like things you're

66
00:06:41,800 --> 00:06:46,000
setting yourself up for disliking and disliking is the only form of true

67
00:06:46,000 --> 00:06:55,720
suffering. The only mental states, the only jitta, the only minds that have

68
00:06:55,720 --> 00:07:02,160
suffering in them are the two anger minds. If you ever study the abidhamma, the

69
00:07:02,160 --> 00:07:05,720
only pain, the only mental suffering that can arise is associated with anger.

70
00:07:05,720 --> 00:07:13,760
That's what they're almost synonymous anger and and and mental suffering. So

71
00:07:13,760 --> 00:07:20,080
basically what the Buddha saying is that craving liking leads to disliking. He's

72
00:07:20,080 --> 00:07:28,120
actually saying that and I know there's any way that in this way we can

73
00:07:28,120 --> 00:07:32,520
understand that there is a creation of a duality is with partiality but you

74
00:07:32,520 --> 00:07:36,120
don't do it with with all mind states. Otherwise when you as I said when you

75
00:07:36,120 --> 00:07:40,880
create wisdom, you're also creating delusion when when you change your mind in

76
00:07:40,880 --> 00:07:45,800
one way you're also somehow changing it in the opposite way. The only time that I

77
00:07:45,800 --> 00:07:51,920
can see it being applicable is with with a greed and anger. So I don't so

78
00:07:51,920 --> 00:07:54,640
basically answer your question. I don't think it's the case with loving

79
00:07:54,640 --> 00:07:58,880
kindness. Why it might seem that way is because as I said with the repression

80
00:07:58,880 --> 00:08:04,000
it's not dealing with the root causes of it's opposite of the anger. So people

81
00:08:04,000 --> 00:08:08,200
think practicing loving kindness and then suddenly I explode. What is this? It's

82
00:08:08,200 --> 00:08:14,200
because the cause of anger is not not having is not not having enough love. It's

83
00:08:14,200 --> 00:08:21,920
not having enough wisdom. So the only way to really overcome the anger that's

84
00:08:21,920 --> 00:08:32,600
already in you is with wisdom. What I found out in meditation is that in the

85
00:08:32,600 --> 00:08:39,480
mind there can only be one thing at a time. So there at least with my mind it is

86
00:08:39,480 --> 00:08:46,440
so there can be either loving kindness or

87
00:08:46,440 --> 00:08:58,000
meta or there can be anger or disliking. And the moments, the present moments in

88
00:08:58,000 --> 00:09:04,800
which I feel or in which I experience the meta present or in which I feel the

89
00:09:04,800 --> 00:09:13,600
anger present can change can alter very very quickly so that it might appear

90
00:09:13,600 --> 00:09:23,320
that that one is kind of coming together or almost at the same time as the

91
00:09:23,320 --> 00:09:31,600
other but actually it's just because the present moment is so short it looks

92
00:09:31,600 --> 00:09:37,440
like that. Yeah that changes as you you fall into meditation right if you

93
00:09:37,440 --> 00:09:44,240
practicing intensely. After intensively after some days you're able to break

94
00:09:44,240 --> 00:09:46,840
it down and you're able to say no they're not leading to each other. They're

95
00:09:46,840 --> 00:09:52,920
actually coming from different sources. It's mostly in exact or in

96
00:09:52,920 --> 00:09:56,280
exact understanding it's why psychology really hasn't gotten anywhere

97
00:09:56,280 --> 00:10:01,320
because it's sorry this terrible thing to say but it's why we often run into

98
00:10:01,320 --> 00:10:06,160
very conflicting theories because they're based what are they based on?

99
00:10:06,160 --> 00:10:11,080
They're based on lab results and they're based on nothing. The only way to

100
00:10:11,080 --> 00:10:15,200
really understand how the mind works is as the Buddha did to go in and spend the

101
00:10:15,200 --> 00:10:21,080
time you know very scientifically more scientific than most science going

102
00:10:21,080 --> 00:10:25,680
moment-by-moment and documenting it. Years, year after year you know as the

103
00:10:25,680 --> 00:10:31,080
Buddha took lifetime after lifetime to to really understand how the mind works

104
00:10:31,080 --> 00:10:42,600
and the different things that lead to different results so it is basically

105
00:10:42,600 --> 00:10:46,640
and this is you could say for so many different things why we come to these

106
00:10:46,640 --> 00:10:52,240
views is based on in exact understanding of reality our inability to see things

107
00:10:52,240 --> 00:10:57,560
going to be nude level in a moment-to-moment level.

108
00:10:57,560 --> 00:11:13,640
I don't necessarily think so it doesn't need to be like that it's when you come to a

109
00:11:13,640 --> 00:11:20,920
equanimity it is not like that anymore until then until you find equanimity it

110
00:11:20,920 --> 00:11:24,960
might be what he's saying is with with liking and disliking which I think is

111
00:11:24,960 --> 00:11:30,280
what I was saying that as far as I can tell it is and it is the case liking

112
00:11:30,280 --> 00:11:35,040
and disliking do lead to each other that's that's the only one I can think of

113
00:11:35,040 --> 00:11:40,480
that that could be considered to be a duality but that's not your original

114
00:11:40,480 --> 00:11:46,000
question was loving kindness and or or wholesomeness and unwholesomeness but

115
00:11:46,000 --> 00:11:51,280
see liking and disliking are both unwholesome they're they're both this

116
00:11:51,280 --> 00:11:57,440
partiality so obviously the the goal to is to become

117
00:11:57,440 --> 00:12:01,400
equanimous and I've talked about this before because equanimous doesn't

118
00:12:01,400 --> 00:12:05,320
mean having no happiness or unhappiness but it means having no liking or

119
00:12:05,320 --> 00:12:08,800
disliking it doesn't mean you become a dull it doesn't mean you become a

120
00:12:08,800 --> 00:12:14,000
as a giant I think said you don't become like a buffalo it's not the

121
00:12:14,000 --> 00:12:18,880
equanimity we're looking for equanimity means you know when you're happy you

122
00:12:18,880 --> 00:12:23,440
know you're happy when you're I can't say unhappy but when you have pain and

123
00:12:23,440 --> 00:12:29,200
stress maybe even like like doing a lot of work and suffering a lot then

124
00:12:29,200 --> 00:12:35,080
you're you have no partiality there your mind is unperturbed and and

125
00:12:35,080 --> 00:12:40,440
invincible really that's the kind of equanimity we're looking for it so it

126
00:12:40,440 --> 00:12:45,000
totally preserves the range of experiences the only thing it takes out is the

127
00:12:45,000 --> 00:12:49,960
liking and the disliking and therefore takes out all suffering

