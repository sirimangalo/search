1
00:00:00,000 --> 00:00:11,000
Hello. Welcome back to Ask a Monk. This next question is a pretty good example, I think, of how not to ask questions on this forum.

2
00:00:11,000 --> 00:00:29,000
And that's because it's specifically, well, it seems quite clearly to be a presentation of one's own philosophy in the form of a question, because here we have a premise and a question.

3
00:00:29,000 --> 00:00:38,000
So I'll read it to you. I want to ask about non-duality. When one doesn't distinguish the two realms of some sara and nirvana, then there is no goal.

4
00:00:38,000 --> 00:00:47,000
So my question is, is wanting to transcend reverb selfish? But what you have here is a complete philosophy that exists in Buddhism.

5
00:00:47,000 --> 00:00:55,000
Buddhism, there are many philosophies, and they're not all compatible. This one I happen to not agree with.

6
00:00:55,000 --> 00:01:19,000
And this is a complete presentation of this philosophy. And it's clearly so because the premise and the conclusion that is presented in the premise, which the implicit conclusion being that striving for the end of reverb is selfish.

7
00:01:19,000 --> 00:01:29,000
There's nothing to do with each other unless accepting the greater framework of this philosophy.

8
00:01:29,000 --> 00:01:40,000
So it seems clear that the purpose of this post is not to ask a question, it's to present the philosophy, which is not really appropriate.

9
00:01:40,000 --> 00:01:51,000
Now, I'm not sure this is the case. It may be that the person asking is really interested in really things that I believe that these two things are indistinguishable.

10
00:01:51,000 --> 00:02:04,000
And somehow thinks that that effect has some effect on the question as to whether strengthening reverb is selfish. I don't see it.

11
00:02:04,000 --> 00:02:11,000
So briefly, I'll go over this, but this isn't the kind of question that I'm interested in asking. I would like to answer.

12
00:02:11,000 --> 00:02:29,000
I would like to be answering questions about meditation and that really help you understand the Buddhist teaching and understand the meditation practice and improve your meditation practice and help you along the path.

13
00:02:29,000 --> 00:02:36,000
So, but anyway, let's, the premise, why I disagree with this philosophy.

14
00:02:36,000 --> 00:02:44,000
The idea that some sara and nirvana are indistinguishable is just a sophism. It's a theory.

15
00:02:44,000 --> 00:02:49,000
It's a view. It has nothing to do with reality. If you experience some sara, some sara is one thing.

16
00:02:49,000 --> 00:02:59,000
This is some sara, seeing, hearing, smelling, the tasting, feeling, and thinking. It's theorizing of suffering and the pursuit of suffering.

17
00:02:59,000 --> 00:03:05,000
Nirvana is the opposite. It's the non-arising of suffering, the non-pursuit of suffering.

18
00:03:05,000 --> 00:03:09,000
And the experience of it is completely different.

19
00:03:09,000 --> 00:03:14,000
The reason why people are unable to distinguish it is because they haven't experienced it.

20
00:03:14,000 --> 00:03:22,000
They, everything they've experienced is the same. They practice meditation and their experiences come and go and so they relate that back to some sara.

21
00:03:22,000 --> 00:03:26,000
And it also comes and goes and everything comes and goes and they say nothing is different.

22
00:03:26,000 --> 00:03:33,000
So they're unable to distinguish one from the other, which is correct really because everything is indistinguishable except nirvana.

23
00:03:33,000 --> 00:03:45,000
Everything else arises in sieces. And this is why the Buddha said nothing is worth clinging to because it all arises in sieces.

24
00:03:45,000 --> 00:03:52,000
So it's basically equating two opposites or confusing two opposites.

25
00:03:52,000 --> 00:04:03,000
It's like saying that night and day are indistinguishable, light and dark and a certain distinguishable.

26
00:04:03,000 --> 00:04:15,000
Theoretically, you can come up with a theory that says that is the case, but doesn't affect the reality that light is quite different from darkness.

27
00:04:15,000 --> 00:04:24,000
And that's objectively so. And nirvana and samsara are objectively so as well, no matter what theory or philosophy you come up with.

28
00:04:24,000 --> 00:04:29,000
Because you can come up with any philosophy you like.

29
00:04:29,000 --> 00:04:33,000
And it doesn't affect, it doesn't affect on reality.

30
00:04:33,000 --> 00:04:57,000
As to the question, I'm assuming that the implication here is that somehow that premise implies that or leads to the conclusion, supports the conclusion that freedom or the desire wanting to transcend rebirth is selfish.

31
00:04:57,000 --> 00:05:04,000
I'm assuming that's what's implied here. So I can talk about that.

32
00:05:04,000 --> 00:05:12,000
Selfishness only really comes into play and Buddhism as I understand it in terms of clinging.

33
00:05:12,000 --> 00:05:20,000
So if you cling to something and you don't want someone else to have it, if you ask me for something and I don't want to give it to you, it's only because I'm clinging to the object.

34
00:05:20,000 --> 00:05:30,000
There's no true selfishness that arises. It's a clinging. It's a function of the mind that wants an object.

35
00:05:30,000 --> 00:05:37,000
You don't think about the other person. What we call selfishness is this absorption in one's attachment.

36
00:05:37,000 --> 00:05:44,000
And only by overcoming that can you possibly give up something that is a benefit to you.

37
00:05:44,000 --> 00:05:53,000
You're able to do that as we grow up. It's not because it could be because we become more altruistic.

38
00:05:53,000 --> 00:06:00,000
But that is not the way of the Buddha. That's not the answer.

39
00:06:00,000 --> 00:06:04,000
Because this idea of altruism is just another theory. It's another mind game.

40
00:06:04,000 --> 00:06:12,000
And there are people who even refute this theory that say, how can you possibly work for the benefit of other people?

41
00:06:12,000 --> 00:06:22,000
That the Buddha's teaching doesn't go either way. There's no idea that working for one's benefit is better than working for one's own benefit.

42
00:06:22,000 --> 00:06:28,000
You act in such a way that brings harmony. You act in such a way that leads to the least conflict.

43
00:06:28,000 --> 00:06:34,000
You act in such a way that is going to be most appropriate at any given time.

44
00:06:34,000 --> 00:06:42,000
And it's not a intellectual exercise. You don't have to study the books and learn what is most appropriate though.

45
00:06:42,000 --> 00:06:46,000
That can help for people who don't have the experience.

46
00:06:46,000 --> 00:06:54,000
You understand the nature of reality through your practice and therefore are able to clearly see what is of the greatest benefit.

47
00:06:54,000 --> 00:06:59,000
What is the most appropriate at any given time? And it might be acting for your own benefit.

48
00:06:59,000 --> 00:07:04,000
It might be acting for someone else's benefit. But the point is not either or.

49
00:07:04,000 --> 00:07:09,000
The point is the appropriateness of the act.

50
00:07:09,000 --> 00:07:15,000
So if someone comes and asks you for something, many things might come into play.

51
00:07:15,000 --> 00:07:22,000
It depends on the situation. If you ask me for something, I might give it to you even though it might cause me suffering.

52
00:07:22,000 --> 00:07:26,000
There may be many reasons for that. It could be because you've helped me before.

53
00:07:26,000 --> 00:07:30,000
Because I am able to deal with suffering. I know that you're not.

54
00:07:30,000 --> 00:07:35,000
You know, I have practiced meditation and I am able to be patient and so on.

55
00:07:35,000 --> 00:07:38,000
And I can see that you're not. I can see that I'm giving to you.

56
00:07:38,000 --> 00:07:42,000
We'll lead to less suffering overall because for me it's only physical suffering for you.

57
00:07:42,000 --> 00:07:48,000
It's mental and so on. I might not give you any of it because I might see that you are not going to use it.

58
00:07:48,000 --> 00:07:51,000
Or I might see that if you're without it, you're going to learn something.

59
00:07:51,000 --> 00:07:54,000
You're going to learn patience and so on.

60
00:07:54,000 --> 00:07:57,000
You know, there are many responses.

61
00:07:57,000 --> 00:08:02,000
This is talking about enlightened person. If a person is enlightened, they will act in this way.

62
00:08:02,000 --> 00:08:08,000
They will never rise to them. I benefit your benefit because there's no clinging.

63
00:08:08,000 --> 00:08:11,000
And that's the only time that selfishness arises.

64
00:08:11,000 --> 00:08:18,000
So the idea that wanting to transcend rebirth is selfish, really, doesn't...

65
00:08:18,000 --> 00:08:21,000
You know, the idea of selfishness doesn't come into play.

66
00:08:21,000 --> 00:08:25,000
It has nothing to do with anyone else. And the only way you could call it selfish

67
00:08:25,000 --> 00:08:31,000
is if you start playing these mind games and logic and, you know, it's an intellectual exercise

68
00:08:31,000 --> 00:08:36,000
where you say, well, if you did stay around, you could help so many people.

69
00:08:36,000 --> 00:08:41,000
And therefore it is selfish to, you know, but it's just a theory, right?

70
00:08:41,000 --> 00:08:45,000
It has nothing to do with the state of mind. The person could be totally unselfish.

71
00:08:45,000 --> 00:08:47,000
And if anyone asked them for something, they would help them.

72
00:08:47,000 --> 00:08:51,000
But they don't ever think, well, I should stick around otherwise it's going to be selfish

73
00:08:51,000 --> 00:08:56,000
because there's no clinging. There's no attachment to this state or that state.

74
00:08:56,000 --> 00:09:01,000
The person simply acts as is appropriate in that instance.

75
00:09:01,000 --> 00:09:09,000
So the realization of freedom from rebirth or the attainment of freedom from rebirth

76
00:09:09,000 --> 00:09:12,000
is of the greatest benefit to a person.

77
00:09:12,000 --> 00:09:15,000
If a person wants to strive for that, then that's a good thing.

78
00:09:15,000 --> 00:09:25,000
It creates benefit. If a person decides to extend their or not work for the

79
00:09:25,000 --> 00:09:28,000
towards the freedom from rebirth and they want to be reborn again and again,

80
00:09:28,000 --> 00:09:33,000
then that's their prerogative. The Buddha never laid down any, you know,

81
00:09:33,000 --> 00:09:37,000
laws of nature in this regard because there are none.

82
00:09:37,000 --> 00:09:43,000
There is simply the cause and effect. If a person decides that they want to

83
00:09:43,000 --> 00:09:47,000
extend their existence and come back again and again and again and be reborn again

84
00:09:47,000 --> 00:09:50,000
and again thinking that they're going to help people and that's fine.

85
00:09:50,000 --> 00:09:54,000
Or that's their choice. That's their path.

86
00:09:54,000 --> 00:09:58,000
There's no need for any sort of judgment at all.

87
00:09:58,000 --> 00:10:03,000
If a person decides that they want to become free from suffering in this life

88
00:10:03,000 --> 00:10:09,000
and not come back, be free from rebirth, then that's their path as well.

89
00:10:09,000 --> 00:10:13,000
There's only the cause and effect. So a person who is reborn again and again

90
00:10:13,000 --> 00:10:17,000
and again, will have to come back again and again and suffer again and again.

91
00:10:17,000 --> 00:10:23,000
It's questionable as to how much help they can possibly be given that they are

92
00:10:23,000 --> 00:10:29,000
not yet free from clinging because a person who is free from clinging would not be reborn.

93
00:10:29,000 --> 00:10:42,000
And the person, actually the person who does become free from rebirth,

94
00:10:42,000 --> 00:10:48,000
who does transcend rebirth. So to speak, is able to help people

95
00:10:48,000 --> 00:10:53,000
because their mind is free from clinging and therefore they are able to

96
00:10:53,000 --> 00:10:56,000
provide great support and help and great insight to people.

97
00:10:56,000 --> 00:11:02,000
The Buddha is a good example.

98
00:11:02,000 --> 00:11:08,000
There is the question of whether the Buddha and enlightened beings do come back

99
00:11:08,000 --> 00:11:13,000
because there are even philosophies that believe a person who is free from clinging still is reborn.

100
00:11:13,000 --> 00:11:17,000
But that's not really the question here.

101
00:11:17,000 --> 00:11:22,000
Or it's getting a little bit too much into other people's philosophies

102
00:11:22,000 --> 00:11:26,000
and I don't want to create more confusion than is necessary.

103
00:11:26,000 --> 00:11:31,000
So the point here is that we work for benefit, we work for welfare.

104
00:11:31,000 --> 00:11:37,000
We do it's appropriate or we try to. This is our practice.

105
00:11:37,000 --> 00:11:41,000
And then enlightened being if someone who is perfect at it,

106
00:11:41,000 --> 00:11:49,000
who doesn't have any thought of self or other and is not subject to suffering

107
00:11:49,000 --> 00:11:56,000
when they pass away from this life, they are free from all suffering.

108
00:11:56,000 --> 00:12:02,000
There's no more coming back, no more rising.

109
00:12:02,000 --> 00:12:14,000
So basically I just wanted to say that this is not the sort of question that I'm hoping to entertain.

110
00:12:14,000 --> 00:12:17,000
And I think probably in the future I'm just going to skip over

111
00:12:17,000 --> 00:12:20,000
because I got a lot of questions and I'm going to try to be selective.

112
00:12:20,000 --> 00:12:25,000
I've been quite random actually in choosing which questions to answer

113
00:12:25,000 --> 00:12:31,000
because it's difficult to sort them.

114
00:12:31,000 --> 00:12:35,000
I've got a lot of them and Google doesn't make it easy by any means.

115
00:12:35,000 --> 00:12:41,000
So I'm going to try to skip and go directly to those ones

116
00:12:41,000 --> 00:12:45,000
that I think are going to be useful for people.

117
00:12:45,000 --> 00:12:48,000
Okay, so anyway, thanks for tuning in.

118
00:12:48,000 --> 00:12:50,000
So it's been another episode of Ask a Monk.

119
00:12:50,000 --> 00:13:16,000
And wishing you all peace, happiness, and freedom from suffering.

