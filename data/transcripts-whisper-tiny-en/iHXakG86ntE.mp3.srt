1
00:00:00,000 --> 00:00:07,120
Bante Buddhism emphasizes a good and respectful relationship with parents, and the opposite

2
00:00:07,120 --> 00:00:13,600
of that can create terrible karma. But how should a child, now an adult, act when he

3
00:00:13,600 --> 00:00:23,340
or she had a bad parent, alcoholic, unfair, or abusive, for example? Well, one of the reasons

4
00:00:23,340 --> 00:00:32,280
there's many different aspects to this question that we have to understand. The first

5
00:00:32,280 --> 00:00:41,840
one, and probably the most important, is the different, the ability to, or the importance

6
00:00:41,840 --> 00:00:54,160
of differentiating between your circumstances and your actions. So there are, well, the vast

7
00:00:54,160 --> 00:01:04,240
majority of us are brought into this world in less than ideal circumstances, to say

8
00:01:04,240 --> 00:01:11,900
at least some people are born in terrible circumstances. It's important not to confuse

9
00:01:11,900 --> 00:01:23,480
that with how you, with your responsibility, or your own actions. So in other words, not to

10
00:01:23,480 --> 00:01:33,360
let that drag you down, to say, I am unlucky, or my life sucks, my life is terrible,

11
00:01:33,360 --> 00:01:39,520
and so on, and let that drag you down, and let that determine how you live your life. Because

12
00:01:39,520 --> 00:01:47,720
that's what we can't control. And that is merely a product of where we are in this moment,

13
00:01:47,720 --> 00:01:55,480
where we're coming from. So it could be a curve up, and so the things are getting better,

14
00:01:55,480 --> 00:01:59,040
but then it could be a curve down, and things are getting worse. We could be at the, at

15
00:01:59,040 --> 00:02:06,240
the low point. We don't know what the future holds. The circumstances that we find ourselves

16
00:02:06,240 --> 00:02:15,160
in are circumstantial. So they don't really have a bearing on us, on our quality, of the

17
00:02:15,160 --> 00:02:25,160
quality of who we are. So the meaning is, there's no excuse, therefore. There's no benefit

18
00:02:25,160 --> 00:02:34,560
in trying to excuse yourself for your behavior based on your situation. Now, it's obviously

19
00:02:34,560 --> 00:02:40,520
more likely that people in bad situations are going to behave poorly. Actually, I'm not

20
00:02:40,520 --> 00:02:46,160
even sure if that's true. It seems to have, for some people, there's a correlation there.

21
00:02:46,160 --> 00:02:51,720
But for some people, there's an inverse correlation. So as people do better in life, they

22
00:02:51,720 --> 00:02:56,620
act worse, right? People, when they were poor, they were kind and generous because they

23
00:02:56,620 --> 00:03:02,360
understood suffering, understood what it meant to be without. But once they attained

24
00:03:02,360 --> 00:03:11,880
to luxury, to opulence, to wealth, they become mean and selfish and, and, and addicted

25
00:03:11,880 --> 00:03:20,680
to their pleasure. So there, there's no excuse there. You can't say, I have a bad relationship

26
00:03:20,680 --> 00:03:25,720
with such a person. So, therefore, I should treat them poorly. I mean, that's basically,

27
00:03:25,720 --> 00:03:34,040
that's the implication here. So the, the, if you take, if you were to ask this about an

28
00:03:34,040 --> 00:03:39,720
ordinary person, well, let's, let's rephrase the question and say, I know we're supposed

29
00:03:39,720 --> 00:03:45,280
to treat people kindly, but what if someone's really mean to you, right? Obviously, if you

30
00:03:45,280 --> 00:03:49,880
understand anything about what, what is teaching, you know that that's not an excuse at

31
00:03:49,880 --> 00:03:57,000
all. There's the Buddha himself was our best example of this, how he treated, he said how,

32
00:03:57,000 --> 00:04:01,440
he said his love for Davidata, the monk who tried to, his cousin who became a monk and

33
00:04:01,440 --> 00:04:06,880
tried to kill him and, and on several occasions split up the sun, the, the sun got the followers

34
00:04:06,880 --> 00:04:14,640
of the Buddha, encouraged the prince to this prince, Ajata Satutakila's father and so

35
00:04:14,640 --> 00:04:21,200
on and so on. But I said, my love for him is equal to my love for Rahola, my son.

36
00:04:21,200 --> 00:04:29,680
And so the idea that you can somehow be excused for treating one person, someone poorly

37
00:04:29,680 --> 00:04:35,000
based on, because they're just too difficult to deal with is erroneous. The, the point

38
00:04:35,000 --> 00:04:40,720
being that there, it's not about some judge say, letting you often saying, oh, that's

39
00:04:40,720 --> 00:04:45,000
okay, you know, I understand how you can be upset at that person because they're really

40
00:04:45,000 --> 00:04:50,200
a mean and nasty person. Karma and, and reality isn't like that. It's harsh, it's

41
00:04:50,200 --> 00:04:56,240
cold, it's impersonal. There is no one go, there's no mitigating circumstance or there,

42
00:04:56,240 --> 00:05:03,280
there's no, um, wiggle room, you know, if you get angry at someone, whether they're a nice

43
00:05:03,280 --> 00:05:13,240
person, a good person or a bad person, the anger, the quality of your mind, is the, is

44
00:05:13,240 --> 00:05:18,360
a measure of the bad karma. Now, that's the first thing you have to understand. It's

45
00:05:18,360 --> 00:05:28,640
not the whole answer, I don't think because, um, the, there is a mitigating factor to how

46
00:05:28,640 --> 00:05:33,520
strong your, your bad intentions are going to be. So if someone's very, very nice to you

47
00:05:33,520 --> 00:05:39,320
or very, very kind to you, has done good things to you for you and you harm them. Or

48
00:05:39,320 --> 00:05:44,040
if someone is pure is good is, whether they've done anything good for you, if they're,

49
00:05:44,040 --> 00:05:51,240
they're in general, pure individual. If you harm them, there's a greater disturbance

50
00:05:51,240 --> 00:05:57,560
in the, the force or the disturbance in the force, disturbance in whatever it is that makes

51
00:05:57,560 --> 00:06:05,320
up the universe. Uh, in your own mind, it requires greater corruption. Uh, and so the karma

52
00:06:05,320 --> 00:06:15,240
that you create will most likely be more powerful. So the, well, while there is no excuse

53
00:06:15,240 --> 00:06:23,480
for treating someone poorly, you're, you're less likely to be, uh, incredibly corrupt when

54
00:06:23,480 --> 00:06:31,960
you get angry at someone who is, or you treat someone poorly, who is a nasty person.

55
00:06:31,960 --> 00:06:40,400
So in regards to our parents, the, there are two reasons that I can think of, or that I understand

56
00:06:40,400 --> 00:06:47,640
based on the teaching of the Buddha, as to why it's important to be kind to them. And

57
00:06:47,640 --> 00:06:52,720
this should help to answer the question. The first one is in regards to our roles and

58
00:06:52,720 --> 00:07:00,680
responsibilities, our, uh, not, not exactly roles and responsibilities. Let's put it more,

59
00:07:00,680 --> 00:07:08,040
those things that lead to social harmony, lead to mental harmony, lead to interpersonal

60
00:07:08,040 --> 00:07:17,240
harmony, uh, what, which we call our responsibilities or our roles. They are important

61
00:07:17,240 --> 00:07:23,120
insofar as they lead to harmony, both inside of ourselves and in the world around us.

62
00:07:23,120 --> 00:07:34,320
So without having, um, or the, the, the lack of parents, a parental support is a very,

63
00:07:34,320 --> 00:07:40,400
very bad thing. It's something that causes harm to us. It's something that disrupts a

64
00:07:40,400 --> 00:07:46,760
family, disrupts society when, when parents are no longer caring for their children looking

65
00:07:46,760 --> 00:07:53,000
after their children. And likewise, when children are no longer respecting their parents,

66
00:07:53,000 --> 00:08:01,240
when it, it, it's a disturbance, it's a chaos. And it's a breeding ground for unholism

67
00:08:01,240 --> 00:08:09,680
ness. So I think you could probably make a strong case for the amount of unholism activity,

68
00:08:09,680 --> 00:08:17,040
increasing in those families that are dysfunctional. I think there's, there's, it's pretty

69
00:08:17,040 --> 00:08:21,880
much a given that that's, that there's a general relationship there, whereas families that

70
00:08:21,880 --> 00:08:29,800
are functional, that are supportive, that are respectful, that have a clear, um, sense of,

71
00:08:29,800 --> 00:08:38,680
of respect in terms of, and, and support of going both ways are more likely to cultivate

72
00:08:38,680 --> 00:08:45,520
unholismness. So, uh, in that sense is going back to where you find yourself, you find

73
00:08:45,520 --> 00:08:52,920
yourself being born into a situation where your parents are abusive, alcoholic, um, are,

74
00:08:52,920 --> 00:08:59,320
are, are unsupportive to their children. Now, that, that means as, here's the perfect

75
00:08:59,320 --> 00:09:08,440
family you're here, nor your, you, you, you, you, um, have, have lost something of that. Now, this

76
00:09:08,440 --> 00:09:15,120
doesn't, you know, this just means that your family situation is less than perfect. Now, this

77
00:09:15,120 --> 00:09:23,680
question kind of implies that it would be somehow beneficial, therefore, or somehow reasonable

78
00:09:23,680 --> 00:09:32,000
to make it even worse, right? Or to, to take what little potential there is for, for

79
00:09:32,000 --> 00:09:45,280
familial harmony, family harmony, um, domestic harmony, and, and reduce it. Let's say, so you,

80
00:09:45,280 --> 00:09:50,880
the point being you still have, you, you don't have the ability to control your parents,

81
00:09:50,880 --> 00:09:57,880
but each individual in the family has the potential to, to add or subtract, to, to benefit

82
00:09:57,880 --> 00:10:04,960
or to harm the family. And so this is what you're looking at. So my answer to this question

83
00:10:04,960 --> 00:10:15,040
would, would be, um, in regards to the difference between your, your poor relationship

84
00:10:15,040 --> 00:10:19,880
with your parents, which actually makes things worse, which can't hope to benefit the

85
00:10:19,880 --> 00:10:30,800
family and your patients and forbearance and kindness and compassion and equanimity in, in

86
00:10:30,800 --> 00:10:38,480
the face of, uh, an imperfect family situation, which, which has the potential to benefit

87
00:10:38,480 --> 00:10:43,440
the family. And this is the case with all things. When, when life hands you lemons make

88
00:10:43,440 --> 00:10:49,000
lemonade the, you're, you're, you can't change what you're given, but that doesn't mean

89
00:10:49,000 --> 00:10:53,400
you should sit back and say, well, I was given a bunch of lemons, so I obviously can't

90
00:10:53,400 --> 00:11:02,080
make anything good out of this. I was given a, a, a poor hand, dealt a poor hand in life,

91
00:11:02,080 --> 00:11:06,560
which actually is, as I said, just a, a measure of where you are now. It's not a measure

92
00:11:06,560 --> 00:11:18,040
of who you are or it's not an indicator of how you should act at all. Um, so the, the,

93
00:11:18,040 --> 00:11:24,800
the important thing is how you react, how you behave in regards to the cards that you're

94
00:11:24,800 --> 00:11:35,000
dealt, the hand that you're dealt. So, that's the, the first aspect of this question

95
00:11:35,000 --> 00:11:42,960
is that just as a function of creating harmony, it's just like a relationship with anyone,

96
00:11:42,960 --> 00:11:55,880
except that there is, I think room for a sort of parental role that one should accept.

97
00:11:55,880 --> 00:12:03,680
So these are my parents and there is a benefit to respecting them, no matter who they are

98
00:12:03,680 --> 00:12:10,640
in, in the sense of, of helping them to fulfill their roles or supporting them and fulfilling

99
00:12:10,640 --> 00:12:15,520
their roles, even if they're crappy at it, even if they're failing at it. You don't help

100
00:12:15,520 --> 00:12:21,720
them fulfill that role by having a crappy relationship, by, by responding poorly to them.

101
00:12:21,720 --> 00:12:25,760
So anything that you can do to help them to fulfill that role and, and, and you can feel

102
00:12:25,760 --> 00:12:31,240
this, you can, you can sense this. It doesn't mean you allow them to be alcoholics or

103
00:12:31,240 --> 00:12:39,400
you support them in their alcoholism. It means, you find ways to, to mold the family as

104
00:12:39,400 --> 00:12:45,000
best you can, at least in so far as it's your responsibility to do so, which means you

105
00:12:45,000 --> 00:12:50,880
aren't responsible for the entire family, but you can at least do your part. By doing

106
00:12:50,880 --> 00:12:59,880
that, it's much better to act in that way than to act poorly. So the first part, second

107
00:12:59,880 --> 00:13:08,280
part is, I think there is also that being said also room for discrimination in the sense

108
00:13:08,280 --> 00:13:17,160
that if the family, in a sense of that one of the other reason why parents are so worthy

109
00:13:17,160 --> 00:13:21,720
of our respect is because they're actually worthy of our respect, that they've actually

110
00:13:21,720 --> 00:13:29,200
done something to support us. So a parent who had sexual intercourse with your mother and

111
00:13:29,200 --> 00:13:34,120
you who you've never met. So I mean, in the, in the most extreme case, the man who raped

112
00:13:34,120 --> 00:13:42,440
your mother for that, and who you never met, obviously, in regards to roles, it would

113
00:13:42,440 --> 00:13:52,960
be great if somehow you could get that person to be the father. But that being said,

114
00:13:52,960 --> 00:14:00,960
a substitute might be, for example, a stepfather. Now often due to emotions and relations

115
00:14:00,960 --> 00:14:08,520
and connections, that doesn't happen. Step, step parents are often unable to find the same

116
00:14:08,520 --> 00:14:18,960
level of attachment or endearment or caring for their children. But for a person who has

117
00:14:18,960 --> 00:14:25,680
been negligent, for a parent who's been negligent, who has been uninvolved with the family,

118
00:14:25,680 --> 00:14:32,000
while the role might be, would have been nice. Sometimes you have to cut your losses

119
00:14:32,000 --> 00:14:42,720
and accept a one parent family, where you're not at all related with the parent. Sometimes

120
00:14:42,720 --> 00:14:48,560
it's just making do with what you have. And sometimes the best way to relate to a parent

121
00:14:48,560 --> 00:14:59,360
is to stay away from them. And eventually, in many cases, I think especially as one, in most,

122
00:14:59,360 --> 00:15:07,960
or in all cases, once one begins to practice the Buddha's teaching, one becomes more

123
00:15:07,960 --> 00:15:13,840
and more detached from one's family, because eventually one begins to get this universal

124
00:15:13,840 --> 00:15:20,480
sense of love, where one treats all beings equally. And so then it's just a matter of responding

125
00:15:20,480 --> 00:15:25,680
to people appropriately, and you would respond to your parents as a child should in whatever

126
00:15:25,680 --> 00:15:38,520
society you're in, just as a matter of duty. But I think for an abusive parent, there

127
00:15:38,520 --> 00:15:46,760
is often, or in a negligent parent, there is room to simply say, that person's not a part

128
00:15:46,760 --> 00:15:53,640
of my life. And to, I guess, to rearrange the roles, or to rearrange the structure of one's

129
00:15:53,640 --> 00:15:58,840
family, where one says single parent family, or no parent family, if one was put up for adoption,

130
00:15:58,840 --> 00:16:07,480
or so, I mean, it gets complicated, you see. So part of it is in regards to the roles and the

131
00:16:07,480 --> 00:16:11,880
structure, the other part of it is in regards to the actual relationship. If a parent

132
00:16:11,880 --> 00:16:18,680
has been abusive or negligent, then they don't really fulfill all of the greatness that

133
00:16:18,680 --> 00:16:21,800
is involved with being a parent. I mean, it's a great thing that your mother carried you

134
00:16:21,800 --> 00:16:28,280
for nine months. It may not have been such a great thing that your father did, by disseminating

135
00:16:28,280 --> 00:16:35,160
her, or more importantly, in abandoning you at birth, or having never met your father or so on.

136
00:16:35,160 --> 00:16:44,280
So definitely, if you can, you want to try to do your best to create a wholesome family environment.

137
00:16:44,280 --> 00:16:51,320
It's just practically speaking useful and beneficial in harmonious, source of peace. But

138
00:16:52,040 --> 00:16:57,640
practically speaking, you sometimes have to cut your losses. And as a Buddhist, I think,

139
00:16:57,640 --> 00:17:04,280
eventually, it means less and less to you. Certainly, I think there's a misunderstanding among

140
00:17:04,280 --> 00:17:10,440
many Buddhists that family relations are somehow important and intrinsic to the path, and I think

141
00:17:10,440 --> 00:17:16,200
definitely they're not. The teachings that the Buddha gave in regards to family were conventional

142
00:17:16,200 --> 00:17:23,400
teachings. There's no sense that any relationship is really important, except maybe one's relationship

143
00:17:23,400 --> 00:17:29,800
with one's teacher, which has a sense of being somewhat intrinsically important, if you can put

144
00:17:29,800 --> 00:17:36,280
that, or in some sense intrinsically important insofar as it leads one directly to enlightenment.

145
00:17:36,280 --> 00:17:45,800
Other relationships are functional and auxiliary and incidental. But important, there's certainly

146
00:17:46,440 --> 00:17:50,840
a person who has bad relationship with their parents will have a difficult time practicing,

147
00:17:51,720 --> 00:17:57,640
just because of how intense the feelings are and often the feeling, the ingratitude that's involved

148
00:17:57,640 --> 00:18:02,120
in that, when a parent has been supportive, has tried their best, has done good things.

149
00:18:03,560 --> 00:18:08,280
Or when a person is, when a parent has been abusive, and that affects you, and the person

150
00:18:08,280 --> 00:18:13,720
hasn't forgiven, hasn't let go, and still has bitter feelings towards their parents. It creates a

151
00:18:13,720 --> 00:18:19,960
chaos in one's mind. It creates a depression, a sadness, an upset, a feeling of low self-worth,

152
00:18:19,960 --> 00:18:27,480
and there's a lot of mental repercussions, so often forgiving your parents as a part of that and so on.

153
00:18:27,480 --> 00:18:32,840
So certainly something not to be taken lightly, but not something to be dogmatic about either.

154
00:18:33,480 --> 00:18:39,480
Parents do have a strong, generally a strong relationship with their children,

155
00:18:39,480 --> 00:18:45,880
even psychologically when the parent who's abandoned you, often it's an important part of your

156
00:18:45,880 --> 00:18:55,000
practice, just to settle it, to actually go and find that parent and meet them. There's no exact

157
00:18:55,000 --> 00:19:07,000
science, but it is an important, weighty sort of aspect of part of life, something that you have

158
00:19:07,000 --> 00:19:17,000
to take seriously. Anyway, I hope that...

