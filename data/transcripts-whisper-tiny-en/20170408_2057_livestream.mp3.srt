1
00:00:00,000 --> 00:00:28,000
Okay, good evening, everyone.

2
00:00:28,000 --> 00:00:32,000
Welcome to our evening demo session.

3
00:00:32,000 --> 00:00:43,000
Live here from Hamilton, Ontario, Canada.

4
00:00:43,000 --> 00:00:52,000
So tonight, we can go on and talk about the third discourse of the Buddha.

5
00:00:52,000 --> 00:01:04,000
Not actually the third discourse, but it really is the third discourse that we have recorded chronologically.

6
00:01:04,000 --> 00:01:08,000
As far as I can think of.

7
00:01:08,000 --> 00:01:18,000
So to recap, the Buddha gave his first discourse, which was called the Turning of the Wheel of the Dhamma.

8
00:01:18,000 --> 00:01:24,000
So at that point, he had said in motion this knowledge, right?

9
00:01:24,000 --> 00:01:26,000
He had given this to the world.

10
00:01:26,000 --> 00:01:28,000
There was no turning back.

11
00:01:28,000 --> 00:01:37,000
Not that there was any desire to turn back, but at that point, the cat was out of the bag and so to speak.

12
00:01:37,000 --> 00:01:44,000
It was inevitable that Buddhism would spread because the teaching is powerful.

13
00:01:44,000 --> 00:01:56,000
And the power was in the realization of the five monks, which ensured that it would be spread.

14
00:01:56,000 --> 00:02:04,000
Five days later, he gave the second discourse and they all became our hunt, which means they became enlightened.

15
00:02:04,000 --> 00:02:12,000
They were no longer subject to rebirths and freed themselves from all the foundment.

16
00:02:12,000 --> 00:02:15,000
He spent the three months of the rains in Isipatana.

17
00:02:15,000 --> 00:02:32,000
Isipatana was this some kind of grove, some kind of park where religious people hung out ascetics, maybe.

18
00:02:32,000 --> 00:02:43,000
And during the rains, there was a man called Yasa, who was very rich. His parents were very rich.

19
00:02:43,000 --> 00:02:48,000
And so he lived in the lap of luxury and had everything he could ever want.

20
00:02:48,000 --> 00:02:56,000
One night, and he lived in Varanasi, which is near Isipatana, was just outside of Varanasi.

21
00:02:56,000 --> 00:03:02,000
Which was a big city in India, still to this day, ancient city, the city of Gaiya, really.

22
00:03:02,000 --> 00:03:11,000
I'm not the city of Varanasi, but the river Ganges, it's where the Ganga River is.

23
00:03:11,000 --> 00:03:21,000
Kasi, I'm thinking of, it's also called Kasi, it used to me.

24
00:03:21,000 --> 00:03:27,000
And so one night, Yasa wakes up in the middle of the night, and he looks around his palace,

25
00:03:27,000 --> 00:03:38,000
and he sees his consorts, the women who are meant to please him as a very, very rich man.

26
00:03:38,000 --> 00:03:43,000
His concubines, maybe, I don't know, just dancing girls, maybe.

27
00:03:43,000 --> 00:03:52,000
And he sees them all asleep, and they look like corpses, and he gets this strong sense of death.

28
00:03:52,000 --> 00:04:01,000
This is what it's like to die. It's curious, I can empathize, I had this sort of feeling myself when I was young.

29
00:04:01,000 --> 00:04:04,000
Everyone would go to sleep, and I think they were all dead.

30
00:04:04,000 --> 00:04:15,000
And it makes one wonder whether this is a echo of our experiences of death, which, of course, very strong,

31
00:04:15,000 --> 00:04:22,000
and would be one of the stronger memories that might carry over with us in some form.

32
00:04:22,000 --> 00:04:33,000
Anyway, he became quite distraught, left his palace or his mansion, and wandered into Isipatana.

33
00:04:33,000 --> 00:04:41,000
He wandered out of the city, went for a walk, ended up in Isipatana, muttering to himself.

34
00:04:41,000 --> 00:04:53,000
This place is, and this world is crazy, and this world is all messed up.

35
00:04:53,000 --> 00:04:57,000
And the Buddha heard him and said to him, yes, that come here.

36
00:04:57,000 --> 00:05:07,000
Here it is not messed up. Here it is not confused. There's not entangled.

37
00:05:07,000 --> 00:05:15,000
And yes, I heard this voice, and so he went to the Buddha, and he sat down in the Buddha, taught him the Dharma, and he became Isotapana.

38
00:05:15,000 --> 00:05:21,000
And his parents came looking for him, found him, found the Buddha, the Buddha taught them.

39
00:05:21,000 --> 00:05:40,000
And they became Isotapana, and while he was teaching them, yes, I became an Aran, and he heard the Dharma the second time.

40
00:05:40,000 --> 00:05:47,000
And then yes, as friends, 54 of his friends became monks.

41
00:05:47,000 --> 00:05:53,000
They heard that, yes, I had gone forth, had left the home life, because as an Aran, he became a monk right away.

42
00:05:53,000 --> 00:06:04,000
And they heard this, and so 54 of them, he had a large society in the city, became monks, just on his reputation.

43
00:06:04,000 --> 00:06:11,000
And they all became our hands as well. So at that time, there was the Buddha, plus 5, plus 55.

44
00:06:11,000 --> 00:06:19,000
No, the Buddha, plus 5, yes, plus 55, so there was 61.

45
00:06:19,000 --> 00:06:28,000
At that time, 61 are our hands in the world.

46
00:06:28,000 --> 00:06:31,000
And then the Buddha at the end of the reigns, he said to the monks,

47
00:06:31,000 --> 00:06:44,000
Jaraata, Bhikkhui, go forth, monks, for the benefit of all, for the benefit of the many, Bhauchana, Sukhaya, Bhukhana, Hitaya.

48
00:06:44,000 --> 00:06:50,000
But don't go by the same road, he said, go separate, go your separate ways.

49
00:06:50,000 --> 00:06:58,000
Meaning, in order for this to spread, you're all enlightened, there's no need for you to depend on anyone,

50
00:06:58,000 --> 00:07:04,000
so be a refuge to others. And he sent them all in 60 different directions.

51
00:07:04,000 --> 00:07:13,000
Pretty powerful. That was the start of the dispensation of the Buddha, the Buddhist Missionaries.

52
00:07:13,000 --> 00:07:18,000
And people say, Buddhism is a missionary religion, which isn't really misleading,

53
00:07:18,000 --> 00:07:25,000
because these monks didn't go out and knock on people's door and say, hey, have you heard the good word?

54
00:07:25,000 --> 00:07:30,000
They would just go and live in various places. For example, I said, gee, left.

55
00:07:30,000 --> 00:07:35,000
And when Sariputa saw him, he just saw him.

56
00:07:35,000 --> 00:07:40,000
He knew right away that this was someone we're listening to, and you came and listened to his teaching.

57
00:07:40,000 --> 00:07:45,000
That happens today in Buddhism. You know, monks don't have to go to their way to teach.

58
00:07:45,000 --> 00:07:48,000
There's so many people interested in learning.

59
00:07:48,000 --> 00:07:50,000
People are teaching out there.

60
00:07:50,000 --> 00:07:57,000
Everyone, many people, not everyone, most people don't, but there are always people who want to hear.

61
00:07:57,000 --> 00:08:03,000
Always people who will come and ask and learn and ask to learn.

62
00:08:03,000 --> 00:08:07,000
And the Buddha himself went to Uruwala.

63
00:08:07,000 --> 00:08:10,000
And in Uruwala, he met these.

64
00:08:10,000 --> 00:08:18,000
Well, first why he was going to Uruwala is he wanted to go to Rajagaha to meet Bhisara.

65
00:08:18,000 --> 00:08:25,000
And Bhimisara was the king of Rajagaha, and so he knew that this would be a great place to start.

66
00:08:25,000 --> 00:08:28,000
Spreading his teaching was a great place to live in.

67
00:08:28,000 --> 00:08:32,000
Bhimisara would be a great ally, which of course he eventually was.

68
00:08:32,000 --> 00:08:38,000
Because what I want to see was perhaps too much entrenched in Brahmanism,

69
00:08:38,000 --> 00:08:43,000
and it would be very difficult and there would be too much conflict.

70
00:08:43,000 --> 00:08:50,000
It's like a general knows that he has to retreat and build up his forces because there were only 60 of them.

71
00:08:50,000 --> 00:08:52,000
And so he wanted to start.

72
00:08:52,000 --> 00:08:55,000
He wanted to come in with a bang.

73
00:08:55,000 --> 00:08:59,000
But he knew that if he went to Rajagaha, there would be a similar situation.

74
00:08:59,000 --> 00:09:08,000
And so instead of going directly to Rajagaha, he went to Uruwala, where the three most famous ascetics were living.

75
00:09:08,000 --> 00:09:11,000
It was just outside of Rajagaha.

76
00:09:11,000 --> 00:09:18,000
And I guess with the idea or we guess with the idea that if he converted these three,

77
00:09:18,000 --> 00:09:32,000
then everyone not just Bhimisara, but the whole of the kingdom of Rajagaha would know how powerful or how great the Buddhist teaching was.

78
00:09:32,000 --> 00:09:42,000
And so he converted these three ascetics through various ways, and all of their disciples,

79
00:09:42,000 --> 00:09:50,000
which ended up being 500 followers of the oldest brother.

80
00:09:50,000 --> 00:09:55,000
There were the three brothers, the 500 followers of him, his second youngest brother,

81
00:09:55,000 --> 00:10:04,000
his second brother, 300 followers, and the youngest brother, 200, according to Buddhism, or according to Taravada Buddhism.

