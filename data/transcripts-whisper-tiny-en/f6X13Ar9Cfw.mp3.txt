We're right. Good afternoon. Good evening. Good morning. Good day. Welcome.
July 4th. Today we are here to answer questions about meditation and to spend an hour in
mindfulness practice. This is our mindfulness hour. So we're here. We come together to engage in
the study of mindfulness. Spend the rest of the week practicing and this is one of the hours
of the week that we come together to study it. To ask questions to answer questions
and to practice together. So we close our eyes and focus our attention on the present moment.
The body, the feelings, the mind, the mind. Focus our attention on the present moment,
keeping our mind here and now catching our experiences, catching the mind as it fits here and there
before it has a chance to react to judge, to extrapolate, before it has a chance to make more
out of things and they actually are. We step in and remind ourselves that that's all it is.
It's just that. If you have questions, you can post them in the chat section. If you don't have
questions, just keep your eyes closed. The chat section is not for chatting. If you're here to chat,
you can post them somewhere else.
Shrada is here to answer. I have to ask the question. So just post them and close your eyes
and we'll get to them. Questions should be about meditation based on our practice. So if you
haven't read the booklet that we have on how to meditate, probably best to go do that
because without it, you might not have a good understanding of what we're even talking about.
If you have read the booklet and you're interested in more,
best thing to do right now is to sign up for an at-home meditation course.
You can go to our website and sign up. It's all free.
Got saw someone asking recently again about how much, whether there's a
minimum recommended donation or something like that and there certainly is not.
There's not even a recommended donation period or suggested donation or a donation at all.
Nothing related to the courses that we offer. If you want to support the organization, you can.
But I'm not here to talk about that. You can go to our website and learn about that if you want
to support. That's not over here for you. There's no hidden agenda. Everything's free.
I'm ready. I'm ready. There's something I'm always ready for.
It's more comfortable answering questions than it is giving talks because giving talks you have
to do a lot more preparation on a specific topic. If I really want to answer people's questions,
I suppose I'd have to do a lot of preparation as well. But answering this way, we can
guess the person's there. We can talk about what they're actually dealing with. It's much more
direct and specific questions are the easy part.
On to this first question, I wasn't sure about that. I'll ask you. You could decide.
When one here is who or what is actually hearing, there's the sound the ear and then some sort
of me involved. What is it how to investigate it? Delusion. Ignorance. Wrong view.
That sense of a me is just a sense. You should note maybe sensing or feeling or thinking.
Feeling might be good if you feel like there's a me involved. It's based on consciousness. There is
consciousness, but that consciousness is momentary. The problem is it's so quick that we don't really
get a good sense of it being momentary because it's moving from one thing to another and it feels
like there's a continuity when in fact it's discrete distinct. You're talking about it's the self
and it's not something you're ever going to find. You should just practice, continue practicing
and that sense will slowly fade away because it's not a part of what's really happening.
It's like
it's like when you watch a film. If you watch a film 24 frames per second or 30 frames per second,
25 some places. It looks like it's continuous, but in fact it's discrete frames. Each frame can
be paused. In fact now with digital it's all mixed in. It's like looking at a picture and thinking
seeing you see a face of someone, but it's not actually a face. It's just pixels discrete,
but there's an illusion. Until you look with a microscope or slow the film down,
you can't see the individual frames. You don't realize that it's actually an illusion.
You know those picture books. You flip, if you probably many people don't, but way back before
movies they had these books and you flip through them with your finger really quickly and
it looks like there's an animation. But it's actually just pages and if you flip to one page,
the animation's not moving at all. The effect is when the effect of flipping to a single page
is quite remarkable actually. Your brain has to be shaken out of it. It's shaken out of it
sense that there's actual movement.
Not all there is. There's the one frame.
Oh, justness is like that. We've lived so long with the illusion of self,
me, mind that it's just become our sense, the feeling we get. It's our perspective, our perception.
It's false. It's an illusion.
The next question is, we had this question last time. We didn't get to it. It's asked again.
And meditation helped you find meaning in life, feeling lost right now as to why I am here
and what to do with my life. Well, it can help you come to terms with that feeling.
You can say feeling, feeling.
You have confusion or doubt or worry. Those are all real.
There is no meaning of life. It's an odd sort of idea. It's hard to put a point on what exactly
is being referred to when you say meaning of life, meaning in life, doing something meaningful.
You really have to be specific. You have to ask yourself, what sort of meaning?
Do you mean something that benefits me? That's a little bit. It's not quite it. Is it something
that is my destiny? Something that has meaning, something that has purpose? It's an odd sort of
idea. It relates to something I can feel confident in, really. That's what it's really more about.
When I do this, I'm confident. I have a confidence. That's what religion gives us. It gives us
faith. Faith. It's really just consciousness. Confidence. You have a strength of mind. You're not
divided in your purpose. You're not divided in your inclination. But that can come from wrong
activities. It can be doing the wrong thing and be very encouraged into doing it.
We can be doing the right thing. Maybe if we talk in terms of wrong or right, it has more meaning.
Some things we do are wrong, some things are right, and that has real inherent truth to it.
There are things that are wrong and there are things that are right, but that's really just
based on whether it causes suffering or whether it freezes from suffering.
So if you want to find direction, something that you can have confidence in, the best place to put
your confidence is in suffering, understanding, suffering, and thereby freeing yourself from suffering.
But feeling lost and so on, that's all just an object of mindfulness because it's a cause of
suffering itself.
It's okay to take time away from noting the present moment to contemplate the Buddha's teaching.
I mean, there are of course worse things you could be doing. So yeah, it's okay.
Maybe a better way to phrase it would be, is it a valuable part of our practice?
Is there because is it okay would be something I might ask if one was addicted to something
or had some other reason for doing it. But I don't think the Buddha's teaching
contemplating the Buddha's teaching was in that category. It really would be more like,
is it beneficial to, is it going to be helpful as opposed to I really crave this and is it okay
if I think. So the real interesting question is whether it's beneficial, whether it's
going to be a detriment to my practice or whether it's going to support my practice. And I think
studying the Buddha's teaching has great benefit. Whether contemplating the Buddha's teaching
is going to support your practice or is it going to be to the detriment? I guess that's really
what's being asked here. If I do that, is it going to hurt my practice? But I think there's
something to the idea that it might be a sort of addiction. Even though that might not be clear
when you ask the question that there is kind of a sense that it's something that we like to do.
Something that we enjoy doing, that it could be something that settles your mind even. If you
have doubts about the practice, contemplating them can reassure you. I can help you deal with your
doubts and that could be good. I think even then better would be to focus on the doubt and try and
see that doubt itself is inherently problematic rather than try to solve doubt because then it's just
about solving individual experiences and it becomes a habit where you try to fix things as opposed
to deal with them and see that they're not worth even worrying about or you're not worth engaging
in doubt is something that's inherently harmful. It's disruptive to the mind. It's a hindrance to
focus and concentration. I don't think there's a lot of room for contemplating teachings.
I think, to some extent, learning them and understanding them is important. If you learn about
the fourth Satipatana and then learn what they mean until you can understand, okay, that's what
I get it. And there may be some contemplation involved there but often there's just an explanation
from a teacher and it shouldn't be a deeper or complicated explanation. I don't think there's a lot
of room for stepping back and taking it as a practice of contemplating the teachings.
There's not a lot of room for intellectualizing. I think any benefit you get from the reassurance
is just going to potentially become a bit of a bad habit. It's not quite right to try and
assuage your doubts by getting explanations and so on, by thinking. It's a nana. If anything,
it's in this category of nana which is a uppuleza where you get caught up in knowledge.
And you can just go with that. Sometimes meditators will sit and just, oh, and this means this
and that means that I get it. I understand. It can be quite intoxicating.
There's cerebral activity. Get you off track and you're no longer mindful.
I don't think it's really necessary. One thing that you should step back once in a while and
be incorporating to your practice is the reflection on your practice, whether the way you're
practicing is proper. Just take a moment to reflect on whether you are actually noting things or
to say to yourself, hey, I wasn't being mindful of this. My practice was really awful there.
Oh, it was because I wasn't being mindful of this. I think there's room for that.
And lifting a foot, we have to have the intention and desire to lift the foot. Is it helpful
to note the thinking of lift foot up?
I don't quite get, I think what you're trying to ask is, is it helpful to note the wanting
or the intention? Oh, thinking, yeah, the thinking to do it. It's a bit, the English isn't quite
there, but I think I get it. I wouldn't recommend doing that for every movement. It can just,
it's just adding a lot. It's not wrong. It's great that you can see that, but maybe more
reasonable would be to note when you switch postures, for example, when you go to stand up,
say wanting to stand, when you go to walk, say wanting to walk, when you go to sit, wanting to sit,
when you go to lie down, wanting to lie down. Big things like that, because if you were to
note an intention for every movement, it would start to get cluttered. You can, it's not wrong,
it's great that you notice it and it's good to note. I just wouldn't do it with every little movement
of the foot.
How can I overcome fear? I know fears here, but my fear is very good.
Well, that's part of what you're coming to see. The purpose of noting is not to make things
go away. It's to help you see that they're not under your control, among other things,
but one important part of it is to see that you're not in control, that they don't go away
just because you want them to. It's something that people don't perhaps realize. It's not
quite clear because the mantra seems like a fix. It seems like, well, logically, if you're
telling me to say pain, pain, it must make the pain go away. Unfortunately, not. It must make
the fear go away. No, no, it doesn't. Sometimes it does. Sometimes there is a connection, but it's
not always, it's not direct, necessary result. Noting is not a sufficient cause for the problems to go
away. How can I overcome fear is probably asking that question is probably based on a desire for
the pain to go away. That is really a part of the problem. I'm sorry, wanting the fear to go away,
which is a part of the problem. Wanting it to go away is just going to exacerbate things.
What we're trying to do, and why we use the noting, why the heck do we use it,
to understand fear, to understand the objects of our experience,
and more you understand fear, the less likely you are to engage in things like fear as a reaction,
because you've seen more clearly that it's really not very useful. It's harmful, in fact.
The next question is very similar. What does one do when feeling guilty,
can meditation and enlightenment help? Only this is a little interesting, because what does it
actually mean? It's probably a combination of liking and disliking, you know, liking, craving and
aversion. You crave engagement with others in various ways, and you're averse to being alone.
There's nothing to do with states. There's nothing to do directly with whether you're alone
or with other people. It's the aversion and the craving, and you should focus on those when
they arise. I think the reason why loneliness becomes a real problem and becomes unpleasant
is because of the perspective we have. There's some sense that being alone is a bad thing.
There's some sense that our worth, our worth is less, or reality is a less value when we're alone,
that there is some state of engagement that is better, the value judgment of being around people,
the excitement, just the excitement of being around people, but it's exacerbated by our
perceptions of, I'm alone now. I have no friends, no one likes me. I wish I had a partner,
I want to get married. There's some abstract ideas involved that create the idea of being lonely.
It becomes loneliness when you have a concept of being alone, that is negative. Being alone is a
problem. So all of that, you should notice while thinking, if you have thoughts,
and being alone, lots of having no friends, lots of wishing you had lots of friends, if you
didn't know at all those. How to handle guided meditation. I'm not sure whether to concentrate on
the speaker or my address. Well, I don't generally recommend guided meditation. The idea behind
the guided meditation is someone who doesn't know how to meditate, listening to the speaker,
and as a result of listening, starting to apply their teachings as they're listening.
So it's both, you're going back and forth, where someone already knows how to meditate really,
your best is to just meditate. Being guided is as your question suggests, it's limiting because
you have to listen and meditate. You have to do two things at once. Obviously, if it's guided,
you have to, for it to be guided, there has to be a concentrating on what the person's saying,
but for it to be meditation, there has to also be a focus on not the breath, but whatever the
object is that the person's talking about. So it's more of an introductory thing. I wouldn't
recommend it as a daily practice. I suppose, artificially, there's some artificial, you're getting
this direction. Now do this, now do that, and that could be useful. I think the bigger problem
there is the reliance on it. If you're relying on someone else, that's a crutch, and crutches are
good if you're crippled, but they don't help you learn to run, for example.
If you rely on them, you're never going to learn how to run, and they were going to get really strong.
There are some of the questions I skipped earlier. It's a 10-day vipassana retreat, meditating,
all the valuable to someone who meditates an hour a day, or is it better to build up to it gradually?
I don't think there's any reason to build up to it gradually. You're able to do an hour a day,
you're probably able to do a 10-day course, or a 12-day course, or a 14-day course.
You're probably talking about a course in a different tradition. In our tradition,
when a person comes the first day, they don't have to do very intense rounds of meditation.
They're able to gradually work into it, so our courses are longer, generally. That's one reason.
They can find information on our website. It's being updated. We should update it soon,
but that'll be because right now the course there's still 21 days, but once you finish the
at-home course then. If you do the at-home course, it's not 21. You don't have to come for 21 days.
You can finish it in a shorter time, maybe 15. It still depends on the individual, but say 15 just
to be safe. Some people take longer and there's no shame in that. It doesn't mean you're a
worst meditator. It's just the way it is.
How do you know if you're enlightened? I find that I have made progress in my mindfulness,
but is there such a thing as fully enlightened?
Yeah, I wouldn't worry about it. One thing you can be clear on is that just being mindful and
gaining progress in mindfulness isn't the goal. It's meant to lead up to a moment where the
mind lets go of everything and there's an experience of cessation, or the mind let's go of seeing,
hearing, smelling, tasting, feeling, thinking. Let's go of some sara completely just for a moment.
There's a state. It's hard to describe because anything I say is going to make it sound like
a trance or something and there's no trance. There's no kind of a trance, but even saying that
is misleading. It's beyond. It's not really something you can describe with words because all
words are related to some sara related to experiences. It's outside of experience
in the sense and that changes you. That experience is more than life-changing. It doesn't just
change this life, changes the core of the stream of consciousness. It just changes everything.
While being mindful on daily life suddenly something makes you fearful, how to avoid coming
out of the meditative state. Having a hard time staying mindful when sudden or scary things happen
around. Generally this sort of description is a conflation of concentration in mindfulness.
There's no mindful state. Mindfulness isn't something you can stay. Those terms relate much
more to what we call concentration. Mindfulness will increase concentration, but it's not mindfulness.
Mindfulness is the clarity, the objectivity. It's the confronting of an experience and that's
every moment. Don't be misled by the states. When you have states of calm or tranquility,
those are good results. They can be. They can also be misleading. They can be things you get attached
to and intoxicated by. Generally you're gaining something if you're more calm and more focused.
But mindfulness is the practice. When you can engage in it and should engage in it whenever you
think of it whenever you're there for it. So there's no staying. There's no state being in the state.
When there's a scary thing, of course it can be harder to be mindful because you've forgotten
yourself. But don't try to get back into some concentrated state. Don't worry about that. That
was never the practice in the first place. Just say afraid. Don't worry that now you're
agitated that now there's stress in the body, stress in the mind. Take those as the object of
mindfulness. We're quite often we can become complacent and dependent on calm and peaceful
and pleasant states. And then when they're gone, when the next day or the next hour or the next
moment, there's a different experience of stress or whatever. We are lost and displeased, disappointed,
confused, upset, more upset because we want what we had. We don't know how to get it.
We can't even come to think of it as something you're doing. Look at me. I'm very focused.
And then when it's gone, you try to turn it back on you. Why can't I make it happen again?
I made it happen before, but the truth is you didn't. Truth as it comes and goes.
And it's not as predictable as you thought it was.
One day, I find that bad thoughts come more when not during the meditation session.
Would it be of use to actively think of these during meditation instead of waiting for it to appear
to then move? No. No, just not them when you're not meditating as well. When bad thoughts come,
not them. There's nothing particular that we have to focus on. The real problem and the biggest
problem with trying to induce problematic states, challenging states, is that that is habit forming
as well. The process of true understanding and objectivity can't occur when you're encouraging.
So you're in the long run, you're just going to, you're not going to benefit from inducing
things. It's based on the idea of control and fixing. You have some purpose in doing so.
And that purpose becomes an inclination. When you try to start fixing things,
well, it just feeds into our inclination to try to see things as problems and try and fix them.
And it feeds into the aversion towards our problems, which is itself a problem.
If we're perfectly still despite pain, is this a more advanced meditation or beneficial,
or is it too physical endurance oriented?
Yeah, I would say that's too much. So try and don't focus on the sitting still aspect,
let's put it there. Try and focus on the pain. No tears of pain. If you find yourself moving
as a result of the pain, try and note that. If you have an intention to move, if you say to yourself,
I need to move because this pain is too much. Then the best rule of thumb is to just
note to yourself wanting to move, wanting to move. And if you're still wanting to move, then
not moving, moving and move slowly. If you use the hand to lift the foot for the leg, for example,
say moving, placing, pushing and thought. And just keep going. Try and note the pain first, yes.
But then if you do move, if you want to move, note that and move.
Sometimes you should move because it can be injurious. It can cause injury if you
stay in a painful position for too long. That's rare, I think. It wouldn't be obsessed with that
idea, but people who have old injuries and so on. You can make them worse. It is possible
to move and can help. I talked with someone recently about, they said their practice was as
soon as you and then there's apparently people teaching that as soon as you feel pain, you should
move. As soon as you feel pain, you acknowledge that there's pain and then you move to a different
posture as well. That is different from how we practice because the Buddha said that we should face
pain and try and understand pain. Suffering is to be understood. So taking it as a practice to move
as soon as you feel pain is problematic to say the least. You'll never really face it.
But the general practical application is that you face it as much as you can and when it's too much
than you move. Gradually, this is what you should be, this is the framework you should be working
under, is that gradually you will face it more. Gradually you'll face it for a longer period of time
until eventually you no longer have to move. You shouldn't put judgment on it. Again, if you
move, you move. It's not actually a problem to do that. Just try and be as mindful as you can and
you'll find eventually. Naturally, you just don't have to move. You don't have to move as quickly.
Eventually, you don't have to move at all.
Don't focus too much on the, I won't move. I won't move kind of thing. I'm pushing it
right because first of all, it's not, your mind is misdirected in that moment if you're obsessed
about I will not move, I will not move. That's not mindfulness. You should be mindful of the pain,
mindful of the moving, mindful of the wanting to move. That's how you started out. It'll
start itself out eventually. If you move, you move.
Can meditation while doing yoga, be considered meditation?
It will be a substitute to sitting meditation or does sitting meditation need to be practiced every day as well.
Let's rephrase this question just so you can understand how it appears to a mindfulness meditator.
Can you do meditation while moving the body? Yes, yes, absolutely, you can.
The question is, why are you moving the body in this particular way?
So we do walking meditation, or we can even do meditation while we're walking ordinarily,
like walking down the street. Question is, why are we walking? If you're walking somewhere to get
to your drug dealer, well, yes, you can walk to your drug dealer mindfully, but you probably
shouldn't be walking to your drug dealer. There's a whole host of things you shouldn't involve
there, like you shouldn't be doing drugs in the first place. Why are you doing yoga is the better
question. I don't really know. I think people describe health benefits to yoga, so you have
to ask yourself why you're to what extent you're concerned about your health and whether it's a healthy
concern, healthy concern for your health. And I think a big reason why people do things like yoga
or Tai Chi is for the result, the feelings that come from it, for Tai Chi and so on. I think there's
the feeling of calm. Yoga, I think there's perhaps even pleasure, but calm as well. People like that.
I think there are probably chemicals involved, bodily chemicals, brain chemicals.
So I don't think yoga is an evil thing, certainly not demonic or anything,
but I'd be suspicious of why people do yoga. I guess I would be suspicious of why
someone doing a meditation course would be doing yoga. So if you're doing it in your daily life,
that's nothing to me. Really great. It's far better than other things you could be doing in your
life like drugs or going out to parties or so on. If you're doing yoga, don't let me stop you,
but if you're doing a meditation course, I would say it doesn't have any place.
Stretching in general, some people will do it because they feel, and this is, I think, another
important point to make. People do it because they feel that it will help them sit better,
and that's a real problem. One thing to sit better, usually relating to not feeling as much pain,
real problem. Pain is our friend. Pain is our pain is our lab rat. That's what we focus on.
It's what we study. Pain is the object of meditation. It's a very important one,
trying to avoid it. It's a big problem. Trying to, so that when you sit in meditation,
you'll feel strong. Your body will feel strong. Your mind will feel strong. All of that
is problematic. You have to be able to deal with your mind that is,
a mind that is weak. You have to understand weakness and disturbance.
That's what we want to understand. Why are we disturbed?
I've been getting strong, pretty, which
of my body and makes me have spas and like shaking any tips.
If it's just a one of spas and then you can just note it
and move on.
Note any emotions you might have about it.
There's no asking about any tips.
I'm not trying to be so be critical,
but when I analyze your questions
that I'm not trying to criticize and say,
boy this person, why would they, what would a dumb question?
It's not a dumb question.
It's just analyze to help you understand
what's going on when you ask this.
So when you ask any tips, there's a sense
that you have a problem with this.
And I don't know.
I have to go by only words, but I assume
that there is possibly probably a problem with it.
And that's a part of the problem.
So there's nothing wrong with a spas and it's not
like it's a good thing, but it's not a bad thing either.
We should just note it and go on,
but if shaking occurs continuously,
there's often the mental aspect.
The mind is egging it on, it's encouraging it.
And then you can tell yourself to stop.
Just say to yourself, stop, that helps.
If there's tension in the body,
you should note that as well, 10, 10.
But most importantly, note any reactions to it.
Worry about it, fear of it, just liking of it.
I'm not exactly about meditation, but they ask twice.
I know the things I need to say.
Yes, twice, so it must need an answer.
Maybe I'll refuse just because they're so stubborn.
Let's see.
I know the things I need to do to improve my life,
but I find it hard to motivate myself,
myself to do these things, to these,
when there are people in my life that I want,
that are not with me.
How do I depend less on others?
How do I depend less on others?
Well, meditation helps.
Really, there's not a complicated answer.
Your dependency on others is an attachment.
It's something that causes us stress and suffering.
So if you meditate, you'll, if you're mindful,
noting, if you read the booklet, that might help.
If you do an at-home course, that should help,
you'll find yourself less attached,
less inclined to cling because you see more clearly
how stupid it is, the cling.
Know how useless it is, how harmful it is to you.
I didn't mean to call you stupid,
but you really, it really is a waking up
where you get us feeling like, I was so, I was asleep.
I was, I was lost.
How could I not see this?
Because when you're asleep, you can't,
you can't understand things.
This is why dreams are so chaotic.
When you have your eyes closed,
you can't help but bump into things.
You bump into and you just can't.
It sort of feels like it feels like we were so blind.
What we should noting be done.
About once per second,
you don't have to worry too much about the rate,
but it's a good rule of thumb,
because if you're noting a lot more than that,
it's not really that beneficial.
And meditation reveal our real self.
So how much time it takes?
And if not,
it doesn't talk about a real self.
We have no sense that such a thing exists.
Any views that you might have relating to a real self
are just going to cause you stress and suffering.
You better let go of those views.
I'm going back to some of the other questions
that was skipped before I'm gonna be able to decide.
Sure.
Is there a limit to what we can achieve with breathing meditation?
The things like meditation go to very many.
Like do we move beyond the practice
that you read about in the booklet?
Because what we practice isn't technically breathing meditation.
It's mindfulness of the body, the tension in the body.
Though you could call it breathing meditation,
but I like to be specific
because there are many things that are called breathing meditation.
But if you're referring to the watching,
the stomach rising and falling,
then yes, there are more, we might say advanced techniques,
but they're still related to the stomach.
If you want to learn about those quote, advanced techniques,
then you can take the at home course
and we can go through them.
It's not like they're,
it's not like you have to be some advanced meditator
to learn about them.
It's more like we want to give them personally
and when you're ready for them.
As you go through the course,
we just put them out on the internet
and people would just decide for themselves
when they were ready and that's a really bad way to do.
Of course, especially in the beginning,
because a person,
well, it's not easy for one to
gauge one's own practice.
It's like trying to see yourself without a mirror.
And meditation be unbeneficial.
We've had this question recently
and we get it, I guess, often or we had it recently anyway.
I don't really want to answer it.
If I try to think of what way someone might ask this
if they're really need an answer
is they might be worried about the practice causing harm.
Those are some dangers that I should be aware of
when I undertake meditation and they're really aren't.
I wouldn't worry about it.
I would point out that there are different kinds
of meditation and I can't vouch for any other type but ours,
but I can say if you're doing the practice,
I would line in the booklet.
It's not likely that you'll fall into much trouble.
You should be aware that the mind is a powerful thing, of course.
And if you have mental illness,
it's quite possible that you'll misinterpret the practice
and start getting lost in practices that could be harmful.
Mind is a powerful thing and if you engage
and encourage harmful habits, guess what, they hurt you.
I'm not sure if it's the same person I didn't go back
but there was another question saying,
some people advise me to get protection if I meditate
too much because one may attract all sorts of talk energies
to what extent is something like this true?
I mean, that's very sort of poetic and mystical,
magical way of talking about it,
but to some extent there is truth to the idea
that if you meditate, it encourages problems
because those problems are in us
that we generally try to avoid,
but that's more what it is.
It's just that you're gonna be facing things
and without help, it can feel like
here being possessed by demons sometimes.
It's really just the mind.
You should, so if you want to talk in these terms,
you shouldn't be protected by your teacher.
I think there is some talk of meditation,
attracting spirits, perhaps even evil spirits
and that's it actually different.
So if we talk about it, actually attracting spirits,
it in fact is a protection itself in that sense
because any spirit that would mess with a meditator
very bad things would happen to them.
It can happen, it can happen.
And meditation can also bring you in closer contact,
with contact with beings that you might have bad karma with.
I'm gonna scare you all,
scare some people anyway, people who think like this,
but it's talk about this and they talk about going,
doing a meditation course sometimes attracts.
One thing it can attract is beings' ghosts that
want to benefit, they have some connection with you
and they're looking to benefit themselves as well
and that can scare the heck out of meditators
from what I hear, but those sorts of beings are not harmful.
And really there's no danger from dark energies
or malevolent spirits.
There's no danger from anything, the thing is dangerous.
The only thing that's really dangerous
is lack of mindfulness, the thing can hurt you
unless you're going to react to it.
And that's where people run into trouble, I suppose,
even potentially with spirits coming to trick you
into being unmindful, that could happen, I suppose.
I think much more common is the dark energy
that comes from within you, yourself,
get caught up in reacting to things and so on.
Is that something you deal with in meditation
that you might not deal with outside of meditation?
So it can feel like it's worse to meditate.
Is there finally facing and dealing with your problems?
Could meditation alone help you recover from an addiction
or mental illness like anorexia?
Absolutely, yes.
Anorexia seems very much like it was a neurosis, I think, right?
So science, and I don't know how valid it is,
but science has sort of distinguished
between what they call psychosis and neurosis.
Psychoses, and I'm not a sappy,
and I'm not a professional in this field,
so I'm just using jargon from someone else's field of practice,
but my understanding, from what I learned in high school,
actually, is that psychosis are organic,
they're ingrained, they're not something that changes,
they're not something that's developed over time,
they're brain-based, it's like a problem in the brain
that's inherent to where the brain is made up.
And neurosis are learned, they are acquired.
Now, I think it's over simplistic,
I think it's fair to say, I think probably the scientific
community has to some extent, moved on,
or qualified these two categories,
but it can be helpful, practically speaking,
to say that anorexia is neurosis and neurosis,
in the sense that it's acquired, it's a habit.
Then it becomes overwhelming, or you just don't know
how to deal with it, you don't even think about it.
You're just, right, you start by going on a diet,
and then it becomes unhealthy,
where you're just in a loop, basically.
Of course, mindfulness helps us break out of all of our loops.
It gives us better loops, better habits.
So absolutely, I think that would be an interesting thing
to see someone who had anorexia,
take that mindfulness meditation, and prevail over it.
I think for sure that's a possibility.
A real problem is, if someone has strong mental illness,
the reason why it often seems
what useless to do things like meditation
is because their minds are in a bad way,
and if your minds are already in a bad way,
it's that much harder to get out of it.
But that just means it's gonna take longer,
it's not something you can easily do a study on
because it can take years or even lifetimes
to overcome your mind being in a bad way.
There's just no shortcut.
Well, that being said, if someone does have the fortitude
of mind, you can often see people change quickly,
but you need that fortitude.
You need some goodness in you, some good karma.
So my karma can be such a powerful thing
because without it, you just don't have the power
to succeed, you don't have the fortitude of mind to succeed
and get lost in your bad habits.
So let this be a lesson, children, do good,
do good, it supports your meditation,
and meditate because meditation is the best goodness,
and that will support future meditation,
even if it doesn't seem fruitful,
the power that you get from meditation is fruitful.
You see, the fruit is easier meditation in the future.
Are there any other noting terms to be used
when coming in contact with spirits?
I use hearing, when having
clear audience experiences and feelings
and feeling the almost magnetic pull of their touch.
Yeah, that's right.
That's all.
Pointless beings don't exist, beings are concept.
So you're a concept, they're a concept.
From the perspective of mindfulness,
there's only experiences.
And so it doesn't matter whether it's someone talking to you
in the room next to you or someone talking to you
from beyond the grave or whatever.
It's still just sound, still just hearing.
Neither one is more real than the other.
Well, maybe that's not true, not more real, but different
because if someone's shouting in your ear,
there's also sound involved, right?
There's also sound waves in the physical ear involved,
but it's not more real, it's just different.
You hear something in the mind, it's still hearing,
it's just a different kind of hearing.
But that doesn't make any, that doesn't mean anything.
We don't distinguish between different kinds of experience.
I mean, different kinds of hearing, for example,
or different kinds of seeing, seeing is still just seeing.
Hearing is still just hearing.
Being able to distinguish between hearing
and seeing that's enough, that's sufficient.
Okay, that's all then, do we have any really important
questions we need and that need answers?
No, I think most of the other questions are more.
Let's off go or not really related to meditations.
All right, then bring us to the end.
Please.
Sadhguru, thank you.
It is good, another good hour of mindfulness.
Thank you all for participating with your questions
and your interest in the meditation
and your practice of mindfulness.
May you all find peace, happiness and freedom from suffering.
Have a good day.
