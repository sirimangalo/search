1
00:00:00,000 --> 00:00:26,400
Good evening, broadcasting live January 8th, 2016.

2
00:00:26,400 --> 00:00:48,920
So I heard back from Ted, Ted X, the Ted X people, and I'm a semi-finalist for their

3
00:00:48,920 --> 00:00:55,160
Ted X conference, or 16 of us.

4
00:00:55,160 --> 00:01:08,480
I think they only select four, looking more than four.

5
00:01:08,480 --> 00:01:16,520
So now I have to write out a talk, now it's real work, I have to write out a talk and go

6
00:01:16,520 --> 00:01:27,720
to an interview and work job, I'm not sure what I've gotten myself into a bit, I have to

7
00:01:27,720 --> 00:01:38,600
write out the talk, 20 minutes, no these talks, or 10 minutes, how long are they?

8
00:01:38,600 --> 00:01:48,400
20 minutes long, I can't remember, I thought Ted X was 20 minutes, maybe they're 10 minutes.

9
00:01:48,400 --> 00:01:49,400
Maybe 10?

10
00:01:49,400 --> 00:02:00,040
Yeah, they didn't let us know that at all, am I supposed to know these things?

11
00:02:00,040 --> 00:02:04,720
I think I'm supposed to be like watching Ted X every day, I haven't watched the Ted

12
00:02:04,720 --> 00:02:22,160
talk in a long time, someone else, they move, our Finnish meditator just finished, he's

13
00:02:22,160 --> 00:02:31,680
advanced course, so congratulations to him, he's downstairs, asked if you wanted to come

14
00:02:31,680 --> 00:02:41,760
up and say hello, he said can we do it another time, okay, he's still going strong, he's

15
00:02:41,760 --> 00:02:49,440
been an intensive meditation for the past 10 days and isn't showing any signs of

16
00:02:49,440 --> 00:03:18,760
liking, and there's more Wednesday, this Wednesday I think, I've been invited to give a

17
00:03:18,760 --> 00:03:26,120
talk in Toronto, so I need a ride to Toronto, anybody want to give me a ride from Hamilton

18
00:03:26,120 --> 00:03:34,800
to Toronto, Wednesday afternoon, why because I have a class and I wouldn't have time to

19
00:03:34,800 --> 00:03:40,240
take the bus there, wouldn't make it on time, I have to get a ride that's the only way

20
00:03:40,240 --> 00:03:50,280
I could make it, I'll have to talk to them, maybe they can come pick me up.

21
00:03:50,280 --> 00:03:55,480
And then just looking at Ted X talks here, it says that they must be under 18 minutes,

22
00:03:55,480 --> 00:04:02,840
but some are as short as five minutes long, yeah probably I have to cut it down, I'm

23
00:04:02,840 --> 00:04:18,680
already a full page of writing, here's one of my paragraphs, this is all very, I just

24
00:04:18,680 --> 00:04:26,200
wrote it up just now and it's probably going to change, but I've spent the past 16 years

25
00:04:26,200 --> 00:04:31,600
practicing studying and teaching meditation as Buddhists, as a Buddhist and eventually

26
00:04:31,600 --> 00:04:38,920
a Buddhist monk, following a monastic code that is over 2,500 years old, I've lived in

27
00:04:38,920 --> 00:04:48,200
Strahitz, and even caves in the jungles of Thailand and Sri Lanka, and I haven't

28
00:04:48,200 --> 00:04:54,480
worn underwear in over 14 years, and at the same time since I've become a monk, I've

29
00:04:54,480 --> 00:04:59,040
learned how to program in several different computer languages, become proficient in

30
00:04:59,040 --> 00:05:04,280
video editing and conversion, and developed a website that holds live internet broadcasts

31
00:05:04,280 --> 00:05:11,000
daily, as well as an online community of several hundred members, I'm just writing probably

32
00:05:11,000 --> 00:05:18,080
the tone has to change as well, it has to be more lively probably, right, and it is

33
00:05:18,080 --> 00:05:22,960
these contrast that interests me very much. During my early years as a Buddhist monk,

34
00:05:22,960 --> 00:05:32,080
I remember sitting at the feet of my 80 year old meditation, Master of Teacher, I like

35
00:05:32,080 --> 00:05:41,040
the word Master so much, teacher listening to him in part, the ancient wisdom of the Buddha

36
00:05:41,040 --> 00:05:45,840
to Thai meditators and think to myself, that really these teachings are not that hard

37
00:05:45,840 --> 00:05:51,120
to understand, the real problem with Buddhism and spirituality in general, is that will

38
00:05:51,120 --> 00:05:58,320
get lost in translation and adaptation and the degradation inherent in word of monk transmission

39
00:05:58,320 --> 00:06:02,920
that winds up like a game of telephone or the message that we the modern world get is something

40
00:06:02,920 --> 00:06:23,280
very different from the original teachings. Does it sound too dry? No, it sounds good. It sounds

41
00:06:23,280 --> 00:06:37,800
good. So that was how long did that take to say? That was, I didn't tell you, but that

42
00:06:37,800 --> 00:06:42,680
was probably like two minutes or a minute and a half, not very long. So I may want to

43
00:06:42,680 --> 00:06:48,200
shorten it a little bit, because this is all just introductory stuff. And then I get

44
00:06:48,200 --> 00:06:54,720
on to, it is this work, although I've gotten into the, see the idea is to contrast ancient

45
00:06:54,720 --> 00:07:02,320
teachings with modern technology and actually maybe ancient teachings in the modern world,

46
00:07:02,320 --> 00:07:07,640
how do you bridge these two? How do you bring modern teachings, ancient teachings to the

47
00:07:07,640 --> 00:07:16,000
modern world? Maybe that's not even the best subject, but it's going to be a part of

48
00:07:16,000 --> 00:07:35,040
it. I think the key is to strip it, that which makes it ancient, which is culture, language.

49
00:07:35,040 --> 00:07:40,600
The sheer access to the information that we have is, you know, compared to people at the

50
00:07:40,600 --> 00:07:47,400
time, I mean, we have, you know, we have such a broader, so much broader information.

51
00:07:47,400 --> 00:08:02,000
Yeah. And yet, I mean, the danger is that we're much more, we have much more immediate

52
00:08:02,000 --> 00:08:11,840
gratification, pleasure, and gratification of desire. Pick up your smartphone and you've

53
00:08:11,840 --> 00:08:18,840
got something entertaining right away. Well, I'm thinking, pick up your smartphone and

54
00:08:18,840 --> 00:08:23,280
you have, you know, the 40 years of the teachings of the Buddha, right away, is compared

55
00:08:23,280 --> 00:08:30,240
to people at the time, you know, maybe they heard a few lines or, you know, one supta,

56
00:08:30,240 --> 00:08:38,080
we have so much. Yeah. But the point I was trying to make was in terms of the quality

57
00:08:38,080 --> 00:08:43,600
of the mind, because the teachings can be there and if there's no one interested in reading

58
00:08:43,600 --> 00:08:52,360
them. So a person has to have a quality of mind to want to read the teachings. And so I think

59
00:08:52,360 --> 00:09:00,240
a part of modern technology is pulling us away from spirituality in general, you know,

60
00:09:00,240 --> 00:09:04,000
make the Buddha's teaching less interesting. But there's a whole other part of it. I think

61
00:09:04,000 --> 00:09:08,960
you could argue that there's a significant part of it that brings us closer.

62
00:09:08,960 --> 00:09:22,320
Well, our breaking barriers and expanding our horizons, the focus on truth and knowledge,

63
00:09:22,320 --> 00:09:32,240
truth, because, you know, the internet is very much about knowledge, about science and

64
00:09:32,240 --> 00:09:42,600
very much, but much of it is, sharing and sharing in general, things that are, by making

65
00:09:42,600 --> 00:09:50,840
things free, we've sort of broken down the capitalist model of charging for things.

66
00:09:50,840 --> 00:09:59,200
I'm telling you, your audio is breaking up a little bit. Well, yeah. Well, then, either

67
00:09:59,200 --> 00:10:06,240
be my internet or it could be your internet or it could be the internet. And then there's

68
00:10:06,240 --> 00:10:29,640
this terrible technical aspect of the internet, it's supposed to make things easier.

69
00:10:29,640 --> 00:10:39,920
Is it still breaking up? No, it sounds better now, much more clear. So do we have any questions?

70
00:10:39,920 --> 00:10:53,880
We do. I think we have a long one here. That's not really a question, though. I think

71
00:10:53,880 --> 00:11:04,000
we have to skip that one. There is a long question that is asking about Mahayana Suta,

72
00:11:04,000 --> 00:11:15,400
the Diamond Suta, and a concept of arbitrary ideas, which I think is from the Suta mentioned.

73
00:11:15,400 --> 00:11:23,240
Looks like we have no questions. What are you all doing here if you don't have any

74
00:11:23,240 --> 00:11:29,440
questions? Does anyone want to come on the hangout and talk with us? Do something different

75
00:11:29,440 --> 00:11:36,040
tonight? Come on, it's a Friday night, live a little. I put the link out for the hangout.

76
00:11:36,040 --> 00:11:41,960
You just click on that link, join us in the hangout. Come and say hello.

77
00:11:41,960 --> 00:11:47,080
Bond wants to know if you can meet your teacher, if you can meet your teacher through

78
00:11:47,080 --> 00:11:56,800
Google Hangouts. Oh, Mimi. A line would probably be better.

79
00:11:56,800 --> 00:12:04,000
May I could call him on the phone? That's probably the easiest is to just call him in

80
00:12:04,000 --> 00:12:12,320
his room. Does that jump how he used much technology? He uses telephone, not much else.

81
00:12:12,320 --> 00:12:17,760
He has an iPad. They got him an iPad, so they used that to show him photos and so on.

82
00:12:17,760 --> 00:12:24,240
His vision is not that good. He had surgery and it never really got better. His vision

83
00:12:24,240 --> 00:12:37,960
is not really good at all. He's 92 or something. Very tired. Not all the time, but gets

84
00:12:37,960 --> 00:12:52,000
tired easier. Did you hear how he gave up the will to live when he was 80? No. You know

85
00:12:52,000 --> 00:12:56,880
the Buddha gave up the will to live at 80. On his 80th birthday, he asked permission from

86
00:12:56,880 --> 00:13:02,160
everyone to die. The head monk for the northern region of Thailand came to his birthday

87
00:13:02,160 --> 00:13:10,640
and while he was sitting there, our teacher gets a John gets up and asks him permission

88
00:13:10,640 --> 00:13:16,400
to die. He said, you know, we're working really hard and doing a lot of things, but you

89
00:13:16,400 --> 00:13:23,840
know, maybe he was so round the body. He took a half an hour really to ask permission.

90
00:13:23,840 --> 00:13:30,280
He was like, you know, so it's good to see you here again and maybe it's for the last

91
00:13:30,280 --> 00:13:39,880
time. No, not that. Well, it's just that I've, you know, been working spent, you know,

92
00:13:39,880 --> 00:13:45,160
since I and he kept interrupting himself and starting over and trying to just, and then

93
00:13:45,160 --> 00:13:53,560
he said, you know, maybe might be that I just want to ask to die.

94
00:13:53,560 --> 00:13:59,040
So he wasn't sad. He was asking like if he was asking if he could go on vacation or something?

95
00:13:59,040 --> 00:14:04,320
Pretty much yet. That's, that's awesome. It's not, and he was really trying to reassure

96
00:14:04,320 --> 00:14:11,960
the head monk that he wasn't, you know, it's not if I'm here. I'll, as long as I'm here,

97
00:14:11,960 --> 00:14:18,960
I'll help and I've been working, you know, I've been, he said, I've been boxing for

98
00:14:18,960 --> 00:14:25,400
many years and he said, I think maybe it's just time and he used a very northern Thai

99
00:14:25,400 --> 00:14:30,840
word that they had monk didn't understand because he's from Bangkok. That means tired

100
00:14:30,840 --> 00:14:37,760
out. And he said, I think I'm just going to think it's time to shelf my gloves, box and

101
00:14:37,760 --> 00:14:44,720
gloves. He said, I want to focus on meditation, but he said, and he said like three times

102
00:14:44,720 --> 00:14:51,600
I'd like to just die. And they had monk refused to let him and all the one, one laywoman

103
00:14:51,600 --> 00:14:57,080
who had just given him a very, very large donation, a sizeable donation and was, you know,

104
00:14:57,080 --> 00:15:03,960
giving him all such a nation's pride in front of the whole congregation. She was not

105
00:15:03,960 --> 00:15:16,160
a very spiritual person. And then that night there was an earthquake in Chantong. And there's

106
00:15:16,160 --> 00:15:21,080
never an earthquake in Chantong. It's not an earthquake zone as far as I know. There's

107
00:15:21,080 --> 00:15:26,720
never, no other time that I've heard of an earthquake or felt an earthquake there. But

108
00:15:26,720 --> 00:15:32,760
that night the earthquake. So in the morning we were all like, we got up early in the

109
00:15:32,760 --> 00:15:37,240
morning to go for Chantong and we're like waiting to see whether he shows up or not.

110
00:15:37,240 --> 00:15:42,320
Because doesn't that happen when an enlightened being dies, there's an earthquake or something?

111
00:15:42,320 --> 00:15:47,600
I don't know. When the Buddha gave up the will to live, there was an earthquake. I think

112
00:15:47,600 --> 00:15:58,600
when he passed away, there's an earthquake. So that's our legend that we're, well, it's

113
00:15:58,600 --> 00:16:04,880
true. That's our magical story of our teacher. Which will be, is that he's still hanging

114
00:16:04,880 --> 00:16:11,480
in there? So did he reread? I know that he's hanging in. I think it's just, he said, you

115
00:16:11,480 --> 00:16:24,800
know, no one knows when they're gonna die. We can't just say you're gonna die. So he's

116
00:16:24,800 --> 00:16:33,000
still here. But he changed and became far less, felt like he became a lot less interested

117
00:16:33,000 --> 00:16:39,160
in a lot less kung-ho about teaching and so on. He really sort of retreated inward.

118
00:16:39,160 --> 00:16:50,480
Actually that. I know we have any still teachers, a lot less, but even today he's probably

119
00:16:50,480 --> 00:17:05,440
still teaching. Well, I can guarantee he's still teaching. Unless he's too ill.

120
00:17:05,440 --> 00:17:10,600
You know, I mean, at some point I probably should go to see him. I don't think there's

121
00:17:10,600 --> 00:17:19,120
any way around it. I mean, if I don't, then yeah, that might be an alternative is to call

122
00:17:19,120 --> 00:17:32,000
him. It's not the same. And it's not that important. I don't get to go see him. I don't

123
00:17:32,000 --> 00:17:42,360
get to go see him. If I have the chance, I probably should. Have you gotten any more information

124
00:17:42,360 --> 00:17:46,720
on the conference or? He's sending it to me tomorrow.

125
00:17:46,720 --> 00:18:01,120
You could call me tonight. And Sunday, we're having this peace meeting group thing. So many

126
00:18:01,120 --> 00:18:07,720
things going on. I should probably go because I've got things to think about and plan and

127
00:18:07,720 --> 00:18:17,360
we're going to. Are you all set for the event on Sunday? I don't know. I'll say maybe

128
00:18:17,360 --> 00:18:26,400
no tomorrow. Okay. Are you, we're going to go to the volunteer meeting before or no?

129
00:18:26,400 --> 00:18:35,200
Right. I'll probably go to it. I just turn on my phone. Yeah, I'm sure we'll have everything

130
00:18:35,200 --> 00:18:51,320
set up for that before that. I'll probably make both meetings. That's not a problem.

131
00:18:51,320 --> 00:18:57,400
Yeah, volunteer meeting if anyone wants to join. We need more volunteers. People to help

132
00:18:57,400 --> 00:19:06,440
us figure stuff out. Right? Do we? We don't. We do. Yes. So it's Sunday at 1 p.m. Eastern

133
00:19:06,440 --> 00:19:13,600
time. And we meet on Google Hangouts. And if you just let me know who you are. If you'd

134
00:19:13,600 --> 00:19:20,000
like to be in the Hangout, it's broadcasted online. Not a lot of people watch the volunteer

135
00:19:20,000 --> 00:19:24,840
meeting some do, but it's just a good way of us, you know, to get together, talk up on

136
00:19:24,840 --> 00:19:29,600
the and find out what type of things are needed each week for the monastery. So anyone

137
00:19:29,600 --> 00:19:42,760
would like to join? Just let me know. I think that's it. No questions. No questions. We've

138
00:19:42,760 --> 00:19:52,200
taught everyone everything. Okay. Thank you all. Thanks Robin. Have a good evening.

