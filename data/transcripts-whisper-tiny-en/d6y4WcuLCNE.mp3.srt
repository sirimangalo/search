1
00:00:00,000 --> 00:00:12,920
I'm sorry to think that one of the things I've overlooked for a long time is the fact

2
00:00:12,920 --> 00:00:20,640
that we're not here to change the world.

3
00:00:20,640 --> 00:00:28,000
We weren't put on this earth or we didn't come into this world trying to change it.

4
00:00:28,000 --> 00:00:35,000
We didn't come into this world for the purpose of changing other people.

5
00:00:35,000 --> 00:00:46,000
But if you want to say why or what was the reason why we came, maybe that's also not quite accurate.

6
00:00:46,000 --> 00:00:54,000
But if you want to ask, what is the most effective thing we can do having been born as human beings?

7
00:00:54,000 --> 00:00:58,000
Where can we have the most effect? Where can we affect real change?

8
00:00:58,000 --> 00:01:03,000
Where can we create real peace, lasting peace and lasting happiness?

9
00:01:03,000 --> 00:01:05,000
And that's with ourselves.

10
00:01:05,000 --> 00:01:13,000
The answer for me is, in this world we can change ourselves.

11
00:01:13,000 --> 00:01:17,000
It's probably a very slow process.

12
00:01:17,000 --> 00:01:21,000
A lot harder than we might wish.

13
00:01:21,000 --> 00:01:26,000
But it's where you find real success and it's where you find real results.

14
00:01:26,000 --> 00:01:37,000
It's where you find that you can create real and lasting peace, happiness and freedom from all the suffering that exists.

15
00:01:37,000 --> 00:01:42,000
So this is where I'm headed.

16
00:01:42,000 --> 00:01:45,000
This is how I'm looking at the world right now.

17
00:01:45,000 --> 00:01:53,000
How I can change not the world around being the wonderful world inside of me.

18
00:01:53,000 --> 00:02:22,000
They create lasting peace, happiness and freedom from suffering.

