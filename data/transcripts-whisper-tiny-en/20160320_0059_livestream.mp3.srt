1
00:00:30,000 --> 00:00:55,860
Good evening, everyone broadcasting live, broadcasting live, March 19th, I think everyone's

2
00:00:55,860 --> 00:01:01,280
doing everything's okay.

3
00:01:25,860 --> 00:01:46,900
Alright, just reading through the comments.

4
00:01:46,900 --> 00:01:47,900
The night's quote.

5
00:01:47,900 --> 00:01:59,960
There's actually a series of quotes, four verses from the Damapanda.

6
00:01:59,960 --> 00:02:08,960
One of which we've actually studied in our Damapanda verses.

7
00:02:08,960 --> 00:02:34,200
Not for a song we don't mind, but for a song we don't mind, but for a song we don't mind.

8
00:02:34,200 --> 00:02:44,300
Atanova, a week here, one should look upon one's self, look upon those of one's self,

9
00:02:44,300 --> 00:02:55,620
katani, a katani-ta, done, things done and not done.

10
00:02:55,620 --> 00:02:59,300
This is a theme that runs through teravada Buddhism.

11
00:02:59,300 --> 00:03:08,020
So I'm understanding that the Buddha actually taught us to look after ourselves.

12
00:03:08,020 --> 00:03:13,660
Let me actually, on face value, this one says something a little bit different.

13
00:03:13,660 --> 00:03:21,540
She says, don't go around blaming other people.

14
00:03:21,540 --> 00:03:26,780
Looking at the faults of others, criticizing others.

15
00:03:26,780 --> 00:03:33,100
But you could expand that to think about concerning, worrying about others, trying to fix

16
00:03:33,100 --> 00:03:39,740
everyone else's problems.

17
00:03:39,740 --> 00:03:51,820
There's another Damapanda verse actually, it might be one of these four.

18
00:03:51,820 --> 00:03:57,100
I think it's missing here.

19
00:03:57,100 --> 00:04:06,620
There's another one that's atanamirapatamun, but you will be when you say, one should

20
00:04:06,620 --> 00:04:13,180
set one's self and one is right first, and only then should one teach other.

21
00:04:13,180 --> 00:04:18,740
That's why it's one wouldn't defy all of themselves, which is really the best explanation

22
00:04:18,740 --> 00:04:31,660
of what's going on in some of these quotes is that in order to help others, you really

23
00:04:31,660 --> 00:04:36,860
have to help yourself.

24
00:04:36,860 --> 00:04:52,060
And so there's this perception that it's selfish, self-centered selfish, to go off in your

25
00:04:52,060 --> 00:04:59,180
room and practice meditation, to come to a meditation center, spend your time meditating

26
00:04:59,180 --> 00:05:07,660
apropos, just this morning, something happened that I didn't really expect to happen

27
00:05:07,660 --> 00:05:13,060
in Canada, though I suppose thinking about it, it's happened many years ago, but it hasn't

28
00:05:13,060 --> 00:05:17,060
happened in many years.

29
00:05:17,060 --> 00:05:26,460
Someone in a van just down the street here was walking down the street and a white van.

30
00:05:26,460 --> 00:05:34,220
Because it was driving by, someone shouted out, get a job, couldn't really put it,

31
00:05:34,220 --> 00:05:38,860
it was a surprise.

32
00:05:38,860 --> 00:05:46,300
Someone told me I should get a job, which I think is apropos because there's a sense that

33
00:05:46,300 --> 00:06:06,300
spiritual practice is selfish, lazy, that shout, experience this morning, actually there's

34
00:06:06,300 --> 00:06:17,460
much more I could say on that, but not that road and that's not cool, too far afield, but there

35
00:06:17,460 --> 00:06:25,820
is this sense, which is a shame, it shows a misunderstanding of what is truly a benefit

36
00:06:25,820 --> 00:06:34,380
and where one can be of most benefit, how one can best benefit the world, if everyone

37
00:06:34,380 --> 00:06:40,620
were to better themselves, it's the first thing you can say, if everyone were to work

38
00:06:40,620 --> 00:06:49,660
on themselves, if everyone in the world and who would need to help anyone else, the second

39
00:06:49,660 --> 00:06:56,020
thing you could say is that someone who has worked on themselves is in a far better position

40
00:06:56,020 --> 00:07:04,300
to help others, but if I just think about all the horrible things I did to other people

41
00:07:04,300 --> 00:07:14,460
before I knew anything about meditation, mean things I've done, and pleasantness I've

42
00:07:14,460 --> 00:07:22,420
created suffering I've caused, and people who try to help do this as well, you try to help

43
00:07:22,420 --> 00:07:30,220
and you just end up confusing this situation because you're yourself full of biases and

44
00:07:30,220 --> 00:07:45,380
delusions and opinions and views that are not really based on reality.

45
00:07:45,380 --> 00:07:58,420
Meditation is actually really the highest duty that we have, not a indulgence or laziness,

46
00:07:58,420 --> 00:08:07,140
meditation is doing your duty, just like you wouldn't walk into a crowded place if you

47
00:08:07,140 --> 00:08:13,700
haven't showered, you haven't cleaned your body, if you haven't cleaned your clothes,

48
00:08:13,700 --> 00:08:21,060
washed your clothes, you wouldn't go walking into a public place because there's a sense

49
00:08:21,060 --> 00:08:33,020
that you have a duty to be clean and not smell, it's unpleasant in this world, and the

50
00:08:33,020 --> 00:08:41,780
same goes far more important scale with our mind, though we don't do our duty, we have

51
00:08:41,780 --> 00:08:50,900
a duty to clean our minds because we don't do this, we fight and we aren't you and we

52
00:08:50,900 --> 00:09:01,300
manipulate each other and we cling to each other, we're not ready to face the world,

53
00:09:01,300 --> 00:09:12,900
we're not equipped to live in peace and harmony, we're defiled, we want to put it in

54
00:09:12,900 --> 00:09:20,380
very harsh and sort of negative ways, in a very real sense we are defiled until we clean

55
00:09:20,380 --> 00:09:25,460
ourselves, we shouldn't, we shouldn't give up to go in public, we shouldn't try to interact

56
00:09:25,460 --> 00:09:33,500
with other people, so why people, this is why people become monks go off in the forest

57
00:09:33,500 --> 00:09:40,580
because they have a job to do, because they don't want to get a job, because they have

58
00:09:40,580 --> 00:09:47,500
a job, they're doing the most important job, this is why we appreciate and welcome people

59
00:09:47,500 --> 00:09:53,660
who want to come and meditate because we feel they're doing the world of service, they're

60
00:09:53,660 --> 00:09:59,940
bettering the world just by learning about themselves, it's like I would say someone

61
00:09:59,940 --> 00:10:06,620
who keeps five preset, it's actually, it's giving a great gift to the world, not really

62
00:10:06,620 --> 00:10:14,860
doing something for themselves, they're, they're bestowing freedom, freedom from danger,

63
00:10:14,860 --> 00:10:24,060
but no being in the universe has reason to fear this person, when they stop killing and

64
00:10:24,060 --> 00:10:32,860
stealing and cheating and lying, they free so many beings from fear, all beings actually,

65
00:10:32,860 --> 00:10:56,900
no being in the universe has reason to fear that person, so helping one's self is very

66
00:10:56,900 --> 00:11:03,820
important.

67
00:11:03,820 --> 00:11:10,540
When one looks at another's faults and is always let's try and find this quote as well,

68
00:11:10,540 --> 00:11:34,740
Okay let's get a very good look

69
00:11:34,740 --> 00:11:44,860
R. R. So, R. So, R. So, V. When a person looks at their faults, so that there is

70
00:11:44,860 --> 00:11:53,620
Nith Zhang Wudjanas and, you know, always perceiving faults, right, always full of envy,

71
00:11:53,620 --> 00:12:04,140
you know, Nith Zhang Wudjanas and, you know, it's funny that envy is there, it's not envy.

72
00:12:04,140 --> 00:12:14,380
Wudjanas and news, picking on others, really, seeing faults in others.

73
00:12:14,380 --> 00:12:23,100
Varo and Janupasisa.

74
00:12:23,100 --> 00:12:26,060
For such a person, asked about the Sawa-Danti.

75
00:12:26,060 --> 00:12:31,540
Such a person, they asked, was the taints grow, increase.

76
00:12:31,540 --> 00:12:54,820
R. R. So, R. So, R. So, V. The ending of the developments.

77
00:12:54,820 --> 00:13:23,820
If only you would do, right, if you would do, if oneself would do with one teaches others,

78
00:13:23,820 --> 00:13:35,820
then it would do, just mind, it would just be wonderful.

79
00:13:35,820 --> 00:13:52,260
All right, so don't do what the dameeta.

80
00:13:52,260 --> 00:14:01,140
Well trained oneself one could then train one should then train others, for it is difficult

81
00:14:01,140 --> 00:14:12,100
to train oneself a tahikira dudamu, the self is difficult to tame, but it's easy to give advice

82
00:14:12,100 --> 00:14:13,100
to others, right?

83
00:14:13,100 --> 00:14:19,740
Actually teaching is a lot easier than actually practicing, so if people say thank you to

84
00:14:19,740 --> 00:14:26,500
me I say no thank you, the other one is doing all the work, I'm just teaching you, I'm just

85
00:14:26,500 --> 00:14:34,260
leading you on the path, you have the one who has to do the work for yourself, even

86
00:14:34,260 --> 00:14:55,260
the Buddha just pointed away, then the last one which is I think probably the most,

87
00:14:55,260 --> 00:15:10,260
it's very 59 to 79, it's very 79, it's very 79, it's just the most inspiring, tah tah nah

88
00:15:10,260 --> 00:15:21,540
to the dah nah, but they don't say tah tah nah, so at a good dose, tah tah nah, so can be

89
00:15:21,540 --> 00:15:45,100
seen, to the dah tah nah, let's roll it, the self should guard oneself,

90
00:15:45,100 --> 00:16:14,380
it should examine oneself, when oneself guarded and mindful, so at a good dose, tah tah

91
00:16:14,380 --> 00:16:25,180
nah, so can be clearly seen, it's a good name for a monk, at the good dose, one who

92
00:16:25,180 --> 00:16:31,420
guards oneself, if you want a good poly name there's a good one, self guarded means

93
00:16:31,420 --> 00:16:39,740
the guard your senses, the guard the eye, the ears, the nose, the tongue, the body and

94
00:16:39,740 --> 00:16:46,420
the mind, so the six doors, it's a good description of the meditation practice because

95
00:16:46,420 --> 00:16:54,820
all of our experiences come through the six doors, and if you get caught up with any one

96
00:16:54,820 --> 00:17:04,940
of them, it leads to bad habits, you know, to put it simply, because it becomes a bit

97
00:17:04,940 --> 00:17:11,940
visual, and it gets you caught up in it, gets you off track, it's you caught up in some

98
00:17:11,940 --> 00:17:24,260
kind of cycle of addiction or aversion or conceit or views or whatever, and it needs to

99
00:17:24,260 --> 00:17:33,540
suffering, needs to stress, as you don't see things clearly biased in your bias, it's

100
00:17:33,540 --> 00:17:42,220
create embarrassment and create stress and create conflict, create disappointment and

101
00:17:42,220 --> 00:17:55,740
expectations, so the nice verses are all about yourself, because that's where we focus,

102
00:17:55,740 --> 00:18:04,060
a river is only as pure as its source, all of our life and our interactions with the

103
00:18:04,060 --> 00:18:09,260
world around us, it's all dependent on our minds, your state of mind is not pure, you

104
00:18:09,260 --> 00:18:16,740
can't expect to help others or be a good influence on others or have a good relationship

105
00:18:16,740 --> 00:18:24,500
with others, work on yourself, it's the best thing you can do, and in fact it's the only

106
00:18:24,500 --> 00:18:30,740
thing you need to do, everything else comes naturally, once your mind is pure, the more your

107
00:18:30,740 --> 00:18:37,740
mind is pure, the better your relationship with others, your influence on others, your impact

108
00:18:37,740 --> 00:18:46,060
on the world, and that's why we meditate, that's why we work on ourselves, that's

109
00:18:46,060 --> 00:18:54,220
why we study ourselves, that's why we try to free ourselves from suffering, if you end

110
00:18:54,220 --> 00:19:01,540
how better is if you're in pain and stressed about your own problems, but a person who's

111
00:19:01,540 --> 00:19:09,500
truly happy, they're a benefit to the world and to everyone they come in contact with,

112
00:19:09,500 --> 00:19:23,260
all right, that's the quote for tonight, I'm going to post the Hangout if anyone has questions,

113
00:19:23,260 --> 00:19:30,660
you're welcome to click on the link and come ask for hang around for a few minutes to

114
00:19:30,660 --> 00:19:49,740
see if anyone has questions, this is the second broadcast today, today was second life

115
00:19:49,740 --> 00:20:14,020
day, so I broadcast it in virtual reality, 20 people, only 20 people can listen to a second

116
00:20:14,020 --> 00:20:26,180
life talk, it's actually a limit placed on the place by the company, it's quite expensive,

117
00:20:26,180 --> 00:20:34,340
second life is quite expensive for people who have land like the Buddha Center, Stephen

118
00:20:34,340 --> 00:20:49,740
your back, but you're still, hello, yep, I hear you, do you have a question for me?

119
00:20:49,740 --> 00:21:16,540
I hear you there, hello, hi, I'm glad to look there, you're here, yeah, we just talked,

120
00:21:16,540 --> 00:21:23,540
yeah, I didn't have a question, I just went to say a comment if you think it's proper,

121
00:21:23,540 --> 00:21:38,740
maybe, sure, just say something about how people feel about meditation, that they think

122
00:21:38,740 --> 00:21:52,060
sometimes we are kind of wasting our time, I used to have a mental illness, I had depression

123
00:21:52,060 --> 00:22:03,300
and anxiety, severe depression and anxiety, and naturally I went through a paranoia state

124
00:22:03,300 --> 00:22:18,660
for one and a half more, at that time all my people, all my family took out and superb me,

125
00:22:18,660 --> 00:22:34,460
but after the crisis passed, they think that people tend necessary to do more things

126
00:22:34,460 --> 00:22:51,900
about it, even though I didn't want to leave taking pills and all the time, so I decided

127
00:22:51,900 --> 00:23:10,580
to look to not take pills anymore and start meditating more seriously, and I think that

128
00:23:10,580 --> 00:23:20,860
sometimes we don't, we don't put attention on what other people say about leaving our

129
00:23:20,860 --> 00:23:31,540
jobs and stop taking medics, if we feel that meditating is going to help us, we should

130
00:23:31,540 --> 00:23:49,260
just do that, that's how we come in, hmm, I don't think you, hey you're smoking, who's

131
00:23:49,260 --> 00:23:56,460
this guy, I'm not convinced, who, who, Stephen, can you tell us a little bit about yourself,

132
00:23:56,460 --> 00:24:05,700
why are you here, I don't know who this guy is, but I think smoking on our broadcast

133
00:24:05,700 --> 00:24:20,940
is not allowed, apologies, should we have rules like that, appropriate, well yes, I mean

134
00:24:20,940 --> 00:24:28,260
that's not very respectful, you have to stop smoking if you want to join our broadcast,

135
00:24:28,260 --> 00:24:45,660
no smoking, no eating, that kind of thing, do you have a question sir, right, well

136
00:24:45,660 --> 00:24:50,140
the reason I stick around is in case people have questions, if nobody has questions,

137
00:24:50,140 --> 00:24:57,260
I'm going to end the broadcast, thank you, thank you all have a good night, in there,

138
00:24:57,260 --> 00:25:25,260
thank you waaaah.

