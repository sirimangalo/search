1
00:00:00,000 --> 00:00:04,480
Hi, and welcome back to our study of the Dhamapana.

2
00:00:04,480 --> 00:00:32,920
Tonight, we continue on with verse number 63, which reads as follows.

3
00:00:34,480 --> 00:00:42,880
Yo ballo, manyati balyang, the fool who knows that they are a fool.

4
00:00:42,880 --> 00:00:52,560
Panditova, Pitinas, so for that reason, or to that extent, they can be called wise.

5
00:00:52,560 --> 00:01:03,600
Pandi, Balocha, Panditamani, but the fool who thinks of themselves as wise

6
00:01:03,600 --> 00:01:12,320
is that way, Balocha, which is the, that person indeed is called a fool.

7
00:01:12,320 --> 00:01:22,000
So, it's one of the more important Dhamapana verses, I don't know, more memorable, I suppose.

8
00:01:22,000 --> 00:01:28,000
It's a powerful one, especially for those of us beginning the path and feeling discouraged

9
00:01:28,000 --> 00:01:35,720
sometimes. Anyway, we'll go through the story and then we'll talk about the verse.

10
00:01:35,720 --> 00:01:39,200
Story goes, it's a very short story, actually, we're going to find several of the Dhamapana

11
00:01:39,200 --> 00:01:44,360
stories are quite short, so there's not much to say about them.

12
00:01:44,360 --> 00:01:51,800
When the Buddha was dwelling in Sabhati, there were these two thieves who decided that

13
00:01:51,800 --> 00:02:00,560
they would go off to the monastery and try to rob the people who came to listen to the Buddha's

14
00:02:00,560 --> 00:02:01,560
teaching.

15
00:02:01,560 --> 00:02:07,480
So, here in Sabhati, there was a big city, walled city.

16
00:02:07,480 --> 00:02:11,200
You can even still see the walls if you go now.

17
00:02:11,200 --> 00:02:15,880
To visit, that's all that's left is a big field with walls.

18
00:02:15,880 --> 00:02:20,520
Ruins, the ruins are just covered, mounds of, who knows what, the whole sequence here.

19
00:02:20,520 --> 00:02:26,360
If you go up on these pagodas, you can see the walls all around the city.

20
00:02:26,360 --> 00:02:32,880
So there would have been rich people and high-class people who would go to see the Buddha

21
00:02:32,880 --> 00:02:38,680
and hear the Buddha talk, hear the Buddha teach, and because they would be so intent

22
00:02:38,680 --> 00:02:43,040
upon what the Buddha was saying, they would often just lose track of their personal belongings,

23
00:02:43,040 --> 00:02:47,320
lose their care for their possessions.

24
00:02:47,320 --> 00:02:53,400
It would actually be easy picking, if this happens in Thailand actually, or it has happened

25
00:02:53,400 --> 00:02:54,400
in Thailand.

26
00:02:54,400 --> 00:02:59,840
So, they say when you go to, when you go to hear the monks talk, don't keep your belongings

27
00:02:59,840 --> 00:03:00,840
close.

28
00:03:00,840 --> 00:03:06,800
If you're going to meditate, meditate with your person, your hands or something like that.

29
00:03:06,800 --> 00:03:18,320
Because unfortunately, by product of letting go is not worrying too much about your belongings.

30
00:03:18,320 --> 00:03:27,080
So, the best advice is to leave your valuables at home when you go to the monastery.

31
00:03:27,080 --> 00:03:32,760
In large crowds, you know, you're sitting around meditating.

32
00:03:32,760 --> 00:03:36,840
So they got there, these two thieves went and knew that the Buddha was giving a talk and

33
00:03:36,840 --> 00:03:45,880
so they went and they saw the crowd and they split up to go and pickpocket the belongings

34
00:03:45,880 --> 00:03:49,160
of the audience.

35
00:03:49,160 --> 00:03:54,640
But when you heard the Buddha speak, he stopped for a second, kind of looking and scoping

36
00:03:54,640 --> 00:03:59,760
out his target and then he stopped and listened to what the Buddha had to say.

37
00:03:59,760 --> 00:04:04,320
And he became so enchanted by what the Buddha said that he just forgot all about stealing

38
00:04:04,320 --> 00:04:09,080
and he really listened to what the Buddha had to say and got the deeper meaning and realized

39
00:04:09,080 --> 00:04:19,480
there was a higher purpose to life and that theft of a few coins was meaningless in the

40
00:04:19,480 --> 00:04:22,280
whole scheme of grand scheme of things.

41
00:04:22,280 --> 00:04:26,000
And so he sat down and he started meditating and based on the Buddha's teaching, he was

42
00:04:26,000 --> 00:04:34,720
able to become a soda panda, just sitting there listening.

43
00:04:34,720 --> 00:04:38,760
And so at the end of the night, he just went back home without stealing anything.

44
00:04:38,760 --> 00:04:45,520
The other guy went, found his target to store some, I think it was five gold coins, pocketed

45
00:04:45,520 --> 00:04:54,760
them, went back home and had his wife cook him up some rich food and he'd gotten the

46
00:04:54,760 --> 00:05:05,920
dough and gotten the spoils and so he had his wife prepare a meal with buying groceries

47
00:05:05,920 --> 00:05:08,040
or over and preparing a meal for him.

48
00:05:08,040 --> 00:05:12,200
The other guy went home and knowing he had no money and he had nothing to show for it went

49
00:05:12,200 --> 00:05:15,560
home and ate nothing.

50
00:05:15,560 --> 00:05:23,480
And then the thief who actually stole, found out about this, they bragging to each other,

51
00:05:23,480 --> 00:05:27,560
they went up and asked him what he had gotten and he said he didn't get anything and he

52
00:05:27,560 --> 00:05:34,160
looked at them and he said, oh, he said what I got from this was wisdom.

53
00:05:34,160 --> 00:05:38,520
And the first thief says, oh, really wise, you're so wise, you can't even feed your

54
00:05:38,520 --> 00:05:39,520
family.

55
00:05:39,520 --> 00:05:44,560
Look at me, what I've got, you call yourself wise, who's the one who's able to feed,

56
00:05:44,560 --> 00:05:49,240
who's able to put a meal on the table.

57
00:05:49,240 --> 00:05:55,880
And the other thief, ex thief, looked at his ex friend and shook his head and thought

58
00:05:55,880 --> 00:06:02,320
to himself, look at this guy, this fool who thinks he's a wise man, not realizing how

59
00:06:02,320 --> 00:06:03,320
foolish he is.

60
00:06:03,320 --> 00:06:07,560
And so he went to the Buddha, he went back to Jita Wanda where the Buddha was staying

61
00:06:07,560 --> 00:06:14,840
in the monastery and related this to the Buddha and said, it's amazing how foolish people

62
00:06:14,840 --> 00:06:21,600
they think passes for wisdom, and that's the story the Buddha then told us, he said very

63
00:06:21,600 --> 00:06:27,360
much, that's very much the case, the true fool thinks they're wise, but a person if they're

64
00:06:27,360 --> 00:06:34,240
able to know that they're foolish, to that extent you can call them wise.

65
00:06:34,240 --> 00:06:39,800
This is an important verse because it highlights the importance of wisdom, the importance

66
00:06:39,800 --> 00:06:48,280
of truth, of understanding the truth, where true wisdom, true wisdom means understanding

67
00:06:48,280 --> 00:06:58,720
the truth, doesn't mean being able to lie and cheat and can I or plunder and so on.

68
00:06:58,720 --> 00:07:05,560
There's a difference between worldly intelligence and wisdom.

69
00:07:05,560 --> 00:07:12,840
And the key here that does have some interesting implications is that knowledge is always

70
00:07:12,840 --> 00:07:19,280
better.

71
00:07:19,280 --> 00:07:26,000
Many people are shocked when they hear that in Buddhism, we say it's better to know that

72
00:07:26,000 --> 00:07:29,000
you're doing, that what you're doing is wrong.

73
00:07:29,000 --> 00:07:37,680
So this intuitive idea that if you commit a bad deed, we talked about this on Sunday,

74
00:07:37,680 --> 00:07:42,720
the Dharma at the M.B.B.C.A.

75
00:07:42,720 --> 00:07:47,600
If we have this intuitive notion that if someone does a bad, performs it on wholesome

76
00:07:47,600 --> 00:07:53,600
deed, not knowing that it's unwholesome, that there's somehow less responsible, less

77
00:07:53,600 --> 00:07:56,960
culpable for their behavior.

78
00:07:56,960 --> 00:08:00,520
Whereas a person who does something knowing it's wrong, we always say, you should know

79
00:08:00,520 --> 00:08:01,520
better.

80
00:08:01,520 --> 00:08:03,320
You know better than that.

81
00:08:03,320 --> 00:08:09,360
So if the younger sibling steals something, they say he doesn't know any better and so

82
00:08:09,360 --> 00:08:10,520
they let him offer that.

83
00:08:10,520 --> 00:08:17,160
But the older sibling who knows better, this is how it goes in accordance as well, right?

84
00:08:17,160 --> 00:08:23,600
The kid who's 12 to 18 doesn't know any better, under 18 doesn't know any better.

85
00:08:23,600 --> 00:08:26,240
Once you're 18, you know better.

86
00:08:26,240 --> 00:08:27,240
And so you're punished.

87
00:08:27,240 --> 00:08:31,960
So we get this idea that somehow that wouldn't be the case for karma.

88
00:08:31,960 --> 00:08:38,560
It would be less karmically potent to do something not knowing that it's wrong.

89
00:08:38,560 --> 00:08:47,800
And that's really what this verse is talking about, that if all you know is that you're

90
00:08:47,800 --> 00:08:52,000
a fool, that's better than not knowing you're a fool and going about doing things thinking

91
00:08:52,000 --> 00:08:53,560
your wise.

92
00:08:53,560 --> 00:08:59,960
This story aptly illustrates that point, that the person who knows what they're doing

93
00:08:59,960 --> 00:09:06,600
is wrong, even if they're engaging in it, the idea is that they will engage in it hesitantly.

94
00:09:06,600 --> 00:09:10,440
But a person who thinks they're wise in doing well, not only perform evil needs but

95
00:09:10,440 --> 00:09:15,120
will boast and brag about them.

96
00:09:15,120 --> 00:09:22,040
An act of theft, whether you know it's wrong or not, is a corrupting act.

97
00:09:22,040 --> 00:09:31,760
It's a violation of other people's property and people's very being.

98
00:09:31,760 --> 00:09:44,200
If you can't see that, the intent to do it, the zest and the zeal in performing the act

99
00:09:44,200 --> 00:09:48,800
is much higher than if you can see that you're actually violating the person's being.

100
00:09:48,800 --> 00:09:55,120
If you know that what you're doing is reprehensible, there's that force against you, the internal

101
00:09:55,120 --> 00:09:57,160
force acting against you.

102
00:09:57,160 --> 00:10:06,280
The opposite holds for doing good deeds, which is an interesting truth, that if a person

103
00:10:06,280 --> 00:10:12,440
perform good deeds, they're more powerful when you know that they're good deeds.

104
00:10:12,440 --> 00:10:16,840
If you know something that is good for you, it's beneficial.

105
00:10:16,840 --> 00:10:23,000
You're more inclined to perform a more confidence in performing it.

106
00:10:23,000 --> 00:10:28,600
You're more content and interested in zealous about it.

107
00:10:28,600 --> 00:10:32,800
Whereas if you don't know that something's good, your parents drag you off to the monastery

108
00:10:32,800 --> 00:10:38,320
to listen to the Buddhist teaching or you drag you off to give offerings to give food

109
00:10:38,320 --> 00:10:45,520
to the monks or if someone's nagging you to give charity or you're keeping the precepts

110
00:10:45,520 --> 00:10:56,920
out of keeping religious precepts out of some kind of traditional compulsion or so on.

111
00:10:56,920 --> 00:11:03,920
This is substandard to actually meaning it, actually, performing it from the heart.

112
00:11:03,920 --> 00:11:09,720
The point in both cases is knowledge is greatest importance.

113
00:11:09,720 --> 00:11:13,720
It's a catchphrase that you can use for people.

114
00:11:13,720 --> 00:11:16,560
If someone boasts about bad deeds, you say, well, I may not be perfect, but at least

115
00:11:16,560 --> 00:11:19,560
I know my fault.

116
00:11:19,560 --> 00:11:25,080
This is really one of the most dangerous flaws that there is, along with like wrong view

117
00:11:25,080 --> 00:11:29,600
or belief in wrong view, it is a form of wrong view.

118
00:11:29,600 --> 00:11:35,720
If you believe that what you're doing is, if you believe that bad deeds are a good thing

119
00:11:35,720 --> 00:11:40,640
and that you're somehow wise in your foolishness, that's the most dangerous, that's more

120
00:11:40,640 --> 00:11:46,320
dangerous than the deeps themselves because that's what's going to compel you to perform

121
00:11:46,320 --> 00:11:53,000
the deeds with enthusiasm and perpetually.

122
00:11:53,000 --> 00:11:54,000
What does it mean to be a fool?

123
00:11:54,000 --> 00:12:01,280
A fool is someone who acts, speaks, and thinks unholesomely.

124
00:12:01,280 --> 00:12:08,600
They perform unholesome deeds with the body, they kill, they steal, they cheat, they perform

125
00:12:08,600 --> 00:12:19,920
unholesome deeds with speech, they lie, they gossip, they scold and they prattle or chatter.

126
00:12:19,920 --> 00:12:24,920
They perform unholesome deeds with mind, it means their minds are sort of greed and anger

127
00:12:24,920 --> 00:12:33,280
and delusion, ill will and conceit and so on.

128
00:12:33,280 --> 00:12:35,640
We all have these tendencies, right?

129
00:12:35,640 --> 00:12:41,480
You may not all kill and steal and lie and so on, but we all have the anger, the greed

130
00:12:41,480 --> 00:12:48,600
and the delusion until you become an hour on, these things exist in the mind.

131
00:12:48,600 --> 00:12:59,360
That's not the worst, it's not the most important or the most horrific fact of the most

132
00:12:59,360 --> 00:13:04,720
horrific evil that there is, worst than actually, it's not the real problem, it's not the

133
00:13:04,720 --> 00:13:10,360
most compelling problem, the most compelling problem is our knowledge of what these are

134
00:13:10,360 --> 00:13:15,200
doing to ourselves, a person who is angry, perpetuate, if they know that they're angry,

135
00:13:15,200 --> 00:13:20,840
there's a potential to work it out, if they know that it's a bad thing to be angry,

136
00:13:20,840 --> 00:13:26,120
if a person is self-righteously angry, these are the people you have to watch out for,

137
00:13:26,120 --> 00:13:32,480
are the people who feel that there's benefit to being angry, people who think that there's

138
00:13:32,480 --> 00:13:35,800
benefit to killing, there's benefit to stealing and so on.

139
00:13:35,800 --> 00:13:36,800
This is the problem.

140
00:13:36,800 --> 00:13:40,880
A person who steals knowing it's wrong and feeling guilty about it but steals a loaf

141
00:13:40,880 --> 00:13:49,280
of bread to feed their family, for example, this is an unwholesome act, but nothing compared

142
00:13:49,280 --> 00:13:58,480
to, nothing compared to stealing, not realizing there's anything wrong with it, stealing

143
00:13:58,480 --> 00:14:08,320
things that's a good thing, stealing from poor people, for example.

144
00:14:08,320 --> 00:14:11,440
So how does this relate to our meditation?

145
00:14:11,440 --> 00:14:16,120
It relates directly to insight meditation because insight meditation is what allows

146
00:14:16,120 --> 00:14:18,480
you to see this.

147
00:14:18,480 --> 00:14:25,080
The first step in insight meditation is not removing of greed, anger and delusion, it focuses

148
00:14:25,080 --> 00:14:29,680
on one aspect of delusion and that's the wrong view aspect.

149
00:14:29,680 --> 00:14:37,080
The first step towards becoming wise is realizing that you're full, that's really it.

150
00:14:37,080 --> 00:14:41,080
This verse is very important for us at the beginning because this is our first goal, our

151
00:14:41,080 --> 00:14:42,080
first step.

152
00:14:42,080 --> 00:14:47,400
Our first step is to see what we're doing wrong, to realize that we're not doing the right

153
00:14:47,400 --> 00:14:52,800
things, that what we're doing is leading us to suffer.

154
00:14:52,800 --> 00:14:58,080
In fact, deep down, that's really the final step as well.

155
00:14:58,080 --> 00:15:04,400
Beginning, it's intellectual realization that killing is wrong, eventually it's just

156
00:15:04,400 --> 00:15:12,040
realizes realization that all greed, all anger and all delusion is causing us suffering,

157
00:15:12,040 --> 00:15:16,160
is harmful to our minds.

158
00:15:16,160 --> 00:15:30,440
So this constant and systematic observation, introspection and analysis, that comes from

159
00:15:30,440 --> 00:15:46,200
just reminding ourselves and staying objective about our experience.

160
00:15:46,200 --> 00:15:53,560
This allows us to see what we're doing wrong and allows us to change our behavior.

161
00:15:53,560 --> 00:15:58,800
So in the beginning to know that we're foolish and then to do something about it.

162
00:15:58,800 --> 00:16:05,240
In fact, it really is deep down, it's the only step that needs to be accomplished.

163
00:16:05,240 --> 00:16:08,040
Every moment that you see that you're doing something foolish, that what you're doing is

164
00:16:08,040 --> 00:16:13,840
truly foolish at that moment, when you see that this reaction of yours to things, when

165
00:16:13,840 --> 00:16:20,120
you react, when you overreact, simple real life examples, when you get anxious, so if I

166
00:16:20,120 --> 00:16:24,720
get anxious that I have to talk in front of you, so I'm giving this talk and I feel anxious.

167
00:16:24,720 --> 00:16:30,360
When I see how ridiculous that is, how it's not helpful, it's not reasonable, it's not

168
00:16:30,360 --> 00:16:36,720
a proper response, that's really a silly thing to do, it's foolish.

169
00:16:36,720 --> 00:16:44,160
Seeing that wisdom, that is what changes my behavior.

170
00:16:44,160 --> 00:16:51,720
Next time I have no reason to act in that way, no reason to set my mind in that direction.

171
00:16:51,720 --> 00:16:55,720
When you truly realize that it's wrong, your mind doesn't go in that direction.

172
00:16:55,720 --> 00:17:00,720
This is the key to Buddhism, the key to the Buddha's teaching, this truth.

173
00:17:00,720 --> 00:17:09,360
That knowledge is really true knowledge, true wisdom, true understanding of the mistakes

174
00:17:09,360 --> 00:17:24,840
that we're making, knowledge of the problems with our reactions.

175
00:17:24,840 --> 00:17:31,200
This is the way out of suffering, this knowledge, knowledge of suffering, knowledge that

176
00:17:31,200 --> 00:17:34,520
what we're doing is causing us suffering.

177
00:17:34,520 --> 00:17:39,720
It's a very important verse, maybe even more important than it appears, but it's a very

178
00:17:39,720 --> 00:17:43,560
catchy one, and it's one that we should always think of, and you can flaunt it to people

179
00:17:43,560 --> 00:17:56,600
who say such things as when you're good at issues, and who brag and boast about their bad

180
00:17:56,600 --> 00:18:02,400
deeds, their unwholesumness, their treachery, their trickery and so on, they shouldn't boast

181
00:18:02,400 --> 00:18:11,000
but it's a reminder for people, it's a good thing to remind our friends, to remind ourselves

182
00:18:11,000 --> 00:18:16,280
that it's not, we don't have to be perfect, not at the beginning, the path to perfection

183
00:18:16,280 --> 00:18:22,040
is to realize your imperfections, the path to become wise is to realize your foolishness,

184
00:18:22,040 --> 00:18:24,560
that's it, that's the path.

185
00:18:24,560 --> 00:18:32,560
So, very important, and I think I've about beaten that horse dead, so there's the verse

186
00:18:32,560 --> 00:18:37,040
for tonight, thank you all for listening, and we'll do some meditation, wishing you all

187
00:18:37,040 --> 00:19:02,080
peace, happiness, and freedom from suffering, have a good…

