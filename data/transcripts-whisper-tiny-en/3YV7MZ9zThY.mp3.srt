1
00:00:00,000 --> 00:00:10,880
Okay, good evening, everyone.

2
00:00:10,880 --> 00:00:29,760
Welcome to our Wednesday dhamma, tonight we're here to study and learn about Buddhist

3
00:00:29,760 --> 00:00:38,600
teaching on mindfulness meditation for the purpose of seeing clearly, which is in turn

4
00:00:38,600 --> 00:00:46,280
for the purpose of freeing ourselves from suffering.

5
00:00:46,280 --> 00:00:59,480
It's easy to underestimate the power of that statement to become free from

6
00:00:59,480 --> 00:01:02,760
suffering.

7
00:01:02,760 --> 00:01:05,520
Think about that, think about that statement.

8
00:01:05,520 --> 00:01:12,440
It's a bold claim, it's an easy one to be skeptical of, there's always that.

9
00:01:12,440 --> 00:01:22,520
But putting aside skepticism, it's a unique phrasing, I can't think of any other

10
00:01:22,520 --> 00:01:29,040
religious or spiritual path that phrases things that way.

11
00:01:29,040 --> 00:01:36,160
There's usually something else, some other baggage, some other idea.

12
00:01:36,160 --> 00:01:48,760
Usually it's phrased in terms of happiness, there's a lot of appealing to our desire for happiness.

13
00:01:48,760 --> 00:02:00,400
It's quite appealing to hear someone talk about this happiness, some of the Buddha talked

14
00:02:00,400 --> 00:02:03,880
about that as well.

15
00:02:03,880 --> 00:02:13,240
But he made a claim that seeing clearly frees you from suffering.

16
00:02:13,240 --> 00:02:26,200
And yeah, you pass it to one one sees with wisdom.

17
00:02:26,200 --> 00:02:33,520
It's easy to underestimate it and what I mean, what I'm thinking of there specifically

18
00:02:33,520 --> 00:02:42,800
is how that really covers everything, covers all of our all of life's problems.

19
00:02:42,800 --> 00:02:52,920
And it changes perspective and understanding about a lot of the issues that we face in the

20
00:02:52,920 --> 00:02:53,920
world.

21
00:02:53,920 --> 00:03:13,480
If I get asked questions, questions about politics, economics, science, religion, what's

22
00:03:13,480 --> 00:03:20,520
important, what is important, what's better, someone asked me recently is what do I think

23
00:03:20,520 --> 00:03:26,520
of democracy, I don't get asked that very often, I mean I think, I've thought about it before,

24
00:03:26,520 --> 00:03:35,120
but don't be asked it much, I've been asked what I think of socialism, capitalism, I've

25
00:03:35,120 --> 00:03:42,600
asked myself that question, what the Buddha thought of those things.

26
00:03:42,600 --> 00:03:47,960
I've argued with communists and socialists, not because I think socialism or communism is

27
00:03:47,960 --> 00:03:58,600
bad necessarily, but because there's a deeper issue, the environment is another good

28
00:03:58,600 --> 00:03:59,600
one.

29
00:03:59,600 --> 00:04:14,080
The environment is responding to our lack of care for it, and we're likely to be in some

30
00:04:14,080 --> 00:04:25,600
great danger in the coming years, in our lifetimes because of our negligence, and so should

31
00:04:25,600 --> 00:04:31,760
a Buddhist be concerned about climate change, should a Buddhist be concerned about the environment,

32
00:04:31,760 --> 00:04:38,720
and it's easy to conflate Buddhism with something else, right?

33
00:04:38,720 --> 00:04:48,800
Perhaps you got into Buddhism because you were a liberal or libertarian, maybe I don't

34
00:04:48,800 --> 00:05:02,280
know about that, because you were interested in anarchy or counter culture, hippies got

35
00:05:02,280 --> 00:05:12,280
into Buddhism, and so they made mixed, and it's easy to think that the Buddha must have

36
00:05:12,280 --> 00:05:18,200
agreed with your worldview, you interpret Buddhism based on your worldview, and you think

37
00:05:18,200 --> 00:05:33,000
all that Buddhism is this or that, do we have to be careful there, but what's more important

38
00:05:33,000 --> 00:05:40,880
than trying to figure out, and this is really important, what's more important than trying

39
00:05:40,880 --> 00:05:59,760
to figure out which system or pursuit the Buddha might have agreed with, is to understand

40
00:05:59,760 --> 00:06:13,600
the cause of the problems that those systems and activities, undertakings, philosophies

41
00:06:13,600 --> 00:06:22,680
and so on seek to remedy, because if they're not talking about alleviating suffering,

42
00:06:22,680 --> 00:06:26,680
if they're not involved with alleviating suffering, then they're not really addressing any

43
00:06:26,680 --> 00:06:30,760
problem, and if they are involved with alleviating suffering, well, we've already found

44
00:06:30,760 --> 00:06:37,760
the answer, so we can pack it up and go home as they say, I don't need to engage in

45
00:06:37,760 --> 00:06:40,200
those things theoretically, right?

46
00:06:40,200 --> 00:06:51,480
Buddhism does that for us, and that's obviously not a fair thing to say, it's an easy

47
00:06:51,480 --> 00:06:58,040
conclusion to come to, but the problem with that statement of course is that Buddhism can't,

48
00:06:58,040 --> 00:07:05,280
the Buddha's teaching can't be practiced unless certain conditions are met, so if you're

49
00:07:05,280 --> 00:07:16,080
in a society that is unstable, how could you possibly practice Buddhism?

50
00:07:16,080 --> 00:07:24,120
Nonetheless, nonetheless, we don't have to talk about practicing meditation or Buddhism,

51
00:07:24,120 --> 00:07:35,480
but we have to understand what are the root causes of suffering, the root causes of problems,

52
00:07:35,480 --> 00:07:43,840
so if we're going to ask which system is best, we have to ask which ones contribute

53
00:07:43,840 --> 00:07:53,480
to our problems, which ones get in the way or serve to take us away from the path to

54
00:07:53,480 --> 00:08:01,160
freedom from suffering.

55
00:08:01,160 --> 00:08:06,840
And I'm not trying to say that I have any answers there, and I think the point is not

56
00:08:06,840 --> 00:08:12,800
to determine, oh yes,ism, communism, that's much better for people, much better for the

57
00:08:12,800 --> 00:08:17,040
practice of Buddhism somehow, right?

58
00:08:17,040 --> 00:08:23,840
Or vice versa, maybe capitalism is better because, well, it's maybe a little bit simpler,

59
00:08:23,840 --> 00:08:36,320
I don't know.

60
00:08:36,320 --> 00:08:46,040
The point is that it requires the proper perspective that you can't argue for any sort

61
00:08:46,040 --> 00:08:57,400
of system or philosophy or undertaking scientific pursuit, religion, philosophy, etc.

62
00:08:57,400 --> 00:09:03,840
You can't argue for it unless you're arguing in terms of the right conditions, unless

63
00:09:03,840 --> 00:09:11,200
you're understanding what it's for.

64
00:09:11,200 --> 00:09:20,680
And without addressing the root causes of suffering, you can, that are good to undertake.

65
00:09:20,680 --> 00:09:31,360
Like suppose there were some political structure that was just perfect led to utopia, just

66
00:09:31,360 --> 00:09:33,480
perfectly designed.

67
00:09:33,480 --> 00:09:37,600
There's no question that it would be useless.

68
00:09:37,600 --> 00:09:40,400
It would be utterly useless.

69
00:09:40,400 --> 00:09:46,400
The perfect system would be utterly useless, that's not fair.

70
00:09:46,400 --> 00:09:59,120
But utterly ineffectual in its goals, if it was implemented by corrupt individuals, right?

71
00:09:59,120 --> 00:10:03,520
You can say capitalism is a horrible system, maybe it is.

72
00:10:03,520 --> 00:10:13,120
You can say communism is a horrible system, I know a lot of people do, but ultimately,

73
00:10:13,120 --> 00:10:19,960
if either of those systems, first of all, were implemented by people who were of a pure

74
00:10:19,960 --> 00:10:31,600
mind and a pure intention, first of all, the system would not be, would lose any capacity

75
00:10:31,600 --> 00:10:35,040
to harm those involved.

76
00:10:35,040 --> 00:10:43,720
I mean, as a basic principle, it doesn't hold for everything, because there's also systemic

77
00:10:43,720 --> 00:10:52,320
violence, which is just because of the nature of the system.

78
00:10:52,320 --> 00:10:58,600
And of course, there's always institutions can institutionalization, red tape, etc. can cause

79
00:10:58,600 --> 00:11:01,400
harm to people.

80
00:11:01,400 --> 00:11:10,280
But, and this is the second thing, any system implemented by people with a pure heart would

81
00:11:10,280 --> 00:11:19,960
be quickly and constantly be refined and adjusted and changed by those people with good

82
00:11:19,960 --> 00:11:22,240
intentions.

83
00:11:22,240 --> 00:11:23,920
And this goes for really anything.

84
00:11:23,920 --> 00:11:32,200
You can't have positive change without positivity, without good intentions.

85
00:11:32,200 --> 00:11:40,920
And so you see some political leaders that you can identify who really care, who really

86
00:11:40,920 --> 00:11:54,360
are sensitive and thoughtful, conscientious and have a sense of the communal nature of life

87
00:11:54,360 --> 00:12:01,280
where we all are in this together, that harming someone else is really just as bad as

88
00:12:01,280 --> 00:12:06,520
harming yourself, taking advantage of someone else is no better than them taking advantage

89
00:12:06,520 --> 00:12:19,280
of you, understanding these things, there are people, and it's those people who work to

90
00:12:19,280 --> 00:12:26,520
change systems and to undertake activities that are positive.

91
00:12:26,520 --> 00:12:33,800
And so if we put our emphasis on worldly things, politics of course comes to mind with

92
00:12:33,800 --> 00:12:40,200
a large American audience, if we put our emphasis on these things, and I'm not saying

93
00:12:40,200 --> 00:12:45,560
that you shouldn't engage in your civic duty, I think we should, I think you should, I

94
00:12:45,560 --> 00:12:52,360
don't, but those of you who are a part of society should, but I don't think that's where

95
00:12:52,360 --> 00:12:58,840
your heart should be, that's where your interest should lie, your interest should lie,

96
00:12:58,840 --> 00:13:04,600
in what truly frees us from suffering, it's quite simple, there's quite not so many things,

97
00:13:04,600 --> 00:13:11,480
and it boils down to greed, anger, and delusion, just three things, that's all, if you

98
00:13:11,480 --> 00:13:14,840
want to break it down, even boil it down even further, it's just delusion because without

99
00:13:14,840 --> 00:13:20,840
delusion you couldn't have the greed or anger, but nonetheless it's the greed and the

100
00:13:20,840 --> 00:13:34,360
anger that do most of the heavy, or a lot of the heavy lifting.

101
00:13:34,360 --> 00:13:40,720
And so ultimately, ultimately meditation is essential, meditation being the cultivation of clarity

102
00:13:40,720 --> 00:13:49,400
of mind that frees us from delusion and in turn frees us from greed and anger and in

103
00:13:49,400 --> 00:13:57,240
turn frees us from causing suffering to ourselves and others.

104
00:13:57,240 --> 00:14:02,480
All good things, if you want to talk about something good, if you have a question about

105
00:14:02,480 --> 00:14:09,160
what you should do with your life, maybe I should become a politician or maybe I should

106
00:14:09,160 --> 00:14:20,800
work to cure cancer or something like that, none of those things is ever going to be

107
00:14:20,800 --> 00:14:27,800
effective for as long as the world contains corruption.

108
00:14:27,800 --> 00:14:35,000
And so it appears to some extent that we're becoming more corrupt, and this is a common

109
00:14:35,000 --> 00:14:40,480
thing to hear with religions that things are getting worse, doomsday is coming.

110
00:14:40,480 --> 00:14:46,120
It doesn't mean it's not true though, doesn't mean it is true, so a lot of people say things

111
00:14:46,120 --> 00:14:47,120
are getting better.

112
00:14:47,120 --> 00:14:55,560
Stephen Pinker, a very famous scientist of some sort, he's put some, for some conclusive

113
00:14:55,560 --> 00:15:04,480
evidence that says that things aren't good because we can be inclined to doom and gloom.

114
00:15:04,480 --> 00:15:13,400
But there are other indicators, the environment's not in very good shape clearly, and

115
00:15:13,400 --> 00:15:23,520
sickness, we're now in an age that may be, may not be reversible where sickness is becoming

116
00:15:23,520 --> 00:15:25,320
a greater thing.

117
00:15:25,320 --> 00:15:29,720
The Buddha said that our lifespan is going to decrease.

118
00:15:29,720 --> 00:15:35,080
I think as I said before, something that might be countering, and it kind of seems arrogant

119
00:15:35,080 --> 00:15:41,560
or proud thing to say, but by the theory you've got to accept the idea that Buddhism

120
00:15:41,560 --> 00:15:51,200
has probably had not that we can see it or point to it anywhere, has had a positive impact

121
00:15:51,200 --> 00:15:56,480
on the world to make things get better in some ways, of course things like ethics and

122
00:15:56,480 --> 00:16:01,640
so on, I think a lot of it wouldn't have been there without the influence of Buddhism,

123
00:16:01,640 --> 00:16:06,840
not all of it, but a fair amount of it because it's such an emphasis, and there are so

124
00:16:06,840 --> 00:16:17,240
many people who have practiced Buddhism throughout the world, throughout history.

125
00:16:17,240 --> 00:16:21,880
But when that's gone and as Buddhism starts to decay and even at the same time there are,

126
00:16:21,880 --> 00:16:26,400
of course, the corrupting influences.

127
00:16:26,400 --> 00:16:35,240
It's those, it's there where the battle lies, there where the work must be done.

128
00:16:35,240 --> 00:16:39,040
So if you want your world to be a good place, if you really want to do something worthwhile,

129
00:16:39,040 --> 00:16:46,440
I mean, there really is no avoiding the very basis of practice or mindfulness, and then,

130
00:16:46,440 --> 00:16:52,920
of course, if you want to become a mindful politician or a mindful philosopher, a mindful

131
00:16:52,920 --> 00:17:03,520
scientist, then, by all means, you'll do very well, but without that, and so by extension

132
00:17:03,520 --> 00:17:09,720
without people who are spreading that, who are helping others to come to the practice

133
00:17:09,720 --> 00:17:18,840
of clarity, practice of mindfulness and the clarity that comes from it, there will be no

134
00:17:18,840 --> 00:17:30,240
reason, no benefit, no gain from focusing on more theoretical or conceptual goods in the

135
00:17:30,240 --> 00:17:31,240
world.

136
00:17:31,240 --> 00:17:40,800
So anyway, that's my thought for tonight, we're going to mini-dumbatok, I guess we have

137
00:17:40,800 --> 00:17:42,760
some questions.

138
00:17:42,760 --> 00:17:50,040
So I'm going to say, at this point, chat will only be, should only be used for questions

139
00:17:50,040 --> 00:17:51,040
from here on.

140
00:17:51,040 --> 00:17:56,160
If there's anything that's not a question, I'm going to ask Chris to delete it, Chris

141
00:17:56,160 --> 00:18:04,760
is again here, kindly to help answer, ask the questions, and also to moderate the chat.

142
00:18:04,760 --> 00:18:14,760
You may have other moderators, I think we were trying to get some.

143
00:18:14,760 --> 00:18:19,800
Okay, let's begin.

144
00:18:19,800 --> 00:18:25,800
Far into meditation, but seeing a lot and suffering from that seeing, having a hard time

145
00:18:25,800 --> 00:18:31,840
during this unfolding, how do I motivate myself to keep meditating?

146
00:18:31,840 --> 00:18:39,880
You don't suffer from the seeing, you do suffer from the desire not to see, meaning we

147
00:18:39,880 --> 00:18:47,280
suffer from our aversion towards things, and because meditation is about facing things,

148
00:18:47,280 --> 00:18:56,880
absolutely, it shouldn't be a big mystery as to why meditation seems to cause suffering.

149
00:18:56,880 --> 00:19:04,400
But it's not the meditation that's doing that, it's the aversion towards facing things.

150
00:19:04,400 --> 00:19:10,560
We're triggered by things, the meditation is about showing yourself that until you see

151
00:19:10,560 --> 00:19:15,560
that, oh yes, this thing that I'm triggered by is actually not a problem, it's the being

152
00:19:15,560 --> 00:19:18,200
triggered, that's the problem.

153
00:19:18,200 --> 00:19:22,960
Once you do that, you're no longer triggered, because you're no longer triggered, you're

154
00:19:22,960 --> 00:19:23,960
no longer suffer.

155
00:19:23,960 --> 00:19:29,240
There is no other way, it is impossible to avoid things as a means of freeing yourself

156
00:19:29,240 --> 00:19:33,080
from suffering, so that should motivate you.

157
00:19:33,080 --> 00:19:40,960
There's absolutely nothing wrong with that experience, except for the fact that you're

158
00:19:40,960 --> 00:19:45,680
reacting to it, to the experience, and probably reacting to the reaction, because it

159
00:19:45,680 --> 00:19:54,800
makes you depressed and discouraged, and it shouldn't, but when you're discouraged, try and

160
00:19:54,800 --> 00:20:02,240
know that, more importantly, just try and note the disliking, don't like.

161
00:20:02,240 --> 00:20:05,280
And be patient, you're not going to get rid of the disliking, you're not going to get rid

162
00:20:05,280 --> 00:20:11,200
of any of your habits, overnight, their habits, their habitual, they take work and effort

163
00:20:11,200 --> 00:20:31,680
and time to overcome, yuck, well, I think it's being interrupted, how is this stream

164
00:20:31,680 --> 00:20:55,840
everyone, not receiving enough video, I don't really know what's going on.

