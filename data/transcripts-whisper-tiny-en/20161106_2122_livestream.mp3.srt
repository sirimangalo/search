1
00:00:00,000 --> 00:00:11,000
And by body it's a replacement for any type of body, so when the stomach rises, you're aware that that's the stomach rising.

2
00:00:11,000 --> 00:00:26,000
That's the body, it's not me, it's not mine, it's not good, it's not that the stomach rises, you know that's the rising.

3
00:00:26,000 --> 00:00:36,000
And by body it's not me, it's not me, it's not me, it's not me, it's not me.

4
00:00:36,000 --> 00:00:48,000
One dwells, a lysito, independent, simply with to the extent of being mindful.

5
00:00:48,000 --> 00:00:52,000
But the Satyamata, I like this word.

6
00:00:52,000 --> 00:00:58,000
I don't think it quite means what I always thought it means, but I still like it nonetheless because it means simply being mindful.

7
00:00:58,000 --> 00:01:05,000
There's nothing else, simply in mind for getting the wrong word, but simply remembering things as they are.

8
00:01:05,000 --> 00:01:21,000
That's just that party, there's muta, muta means only or merely for mere recollection, mere remembrance of things as they are.

9
00:01:21,000 --> 00:01:32,000
Yana, muta, just enough to know, and to know clearly, to see clearly.

10
00:01:32,000 --> 00:01:39,000
We pass and now we hear, we talk about we pass and we're seeing clearly, it's not anything hard to understand or hard to grasp.

11
00:01:39,000 --> 00:01:49,000
It really does just mean seeing mundane experience as it is, and it's not even that mysterious.

12
00:01:49,000 --> 00:01:53,000
Many of the truths that we come across in Buddhism, we already know.

