1
00:00:00,000 --> 00:00:04,000
Hello and welcome back to our study of the Damapada.

2
00:00:04,000 --> 00:00:14,000
Today we will continue on with verse number 28 which reads as follows.

3
00:00:34,000 --> 00:00:44,000
You have done with the Tipandito when a wise person

4
00:00:44,000 --> 00:00:51,000
discards negligence in favor of heedfulness.

5
00:00:51,000 --> 00:00:59,000
Panya Pasa Damaruya having ascended the tower of wisdom.

6
00:00:59,000 --> 00:01:06,000
A so-called sokining pajang looks upon the sorrowing people,

7
00:01:06,000 --> 00:01:14,000
or the suffering people, as one who doesn't suffer anymore.

8
00:01:14,000 --> 00:01:21,000
Pabatataywa Bhumatay, it's like a person who has climbed a mountain

9
00:01:21,000 --> 00:01:26,000
and is looking at the people on the mountain below, on the ground below.

10
00:01:26,000 --> 00:01:37,000
So Diro Bali Awikati, so the wise person looks upon foolish people.

11
00:01:37,000 --> 00:01:42,000
Like a person on top of the mountain looking down at the people below,

12
00:01:42,000 --> 00:01:47,000
so the wise person sees foolish people.

13
00:01:47,000 --> 00:01:54,000
It sounds actually like what it's saying is that the wise person looks down upon foolish people.

14
00:01:54,000 --> 00:01:57,000
But we should understand the context.

15
00:01:57,000 --> 00:02:04,000
The story is of this person is told in regards to the venerable Mahakasapada.

16
00:02:04,000 --> 00:02:07,000
Mahakasapada lived actually up on top of the mountain.

17
00:02:07,000 --> 00:02:11,000
He declined to live in the city even when he got old.

18
00:02:11,000 --> 00:02:18,000
He would walk all the way up this mountain every day to live in the forest.

19
00:02:18,000 --> 00:02:22,000
And when we were in India we actually walked up the stairs and it's quite a way.

20
00:02:22,000 --> 00:02:26,000
Now they have stairs there, but I suppose in the time of Mahakasapada would have just been,

21
00:02:26,000 --> 00:02:29,000
maybe there were rock stairs or something.

22
00:02:29,000 --> 00:02:31,000
But anyway, quite a walk.

23
00:02:31,000 --> 00:02:37,000
It's amazing that he was able to do it every day and go on Armstrong every day.

24
00:02:37,000 --> 00:02:43,000
But this he did, and on top of that he had great magical power,

25
00:02:43,000 --> 00:02:45,000
or great mental powers.

26
00:02:45,000 --> 00:02:50,000
And so he was able to, as with many of the yogis in that time,

27
00:02:50,000 --> 00:02:52,000
and of course the many of the Buddhist disciples,

28
00:02:52,000 --> 00:02:55,000
he was able to see things that people ordinary people can't see

29
00:02:55,000 --> 00:02:59,000
because of the clarity and the strength of his mind.

30
00:02:59,000 --> 00:03:04,000
And so one morning he spent this morning after he went on Armstrong and came back

31
00:03:04,000 --> 00:03:09,000
and had eaten his daily meal, he spent the morning looking out at the beings

32
00:03:09,000 --> 00:03:14,000
in the world, being so were born in the earth, on the earth, in the water,

33
00:03:14,000 --> 00:03:26,000
in the air, things who lived in all sorts of births or all sorts of realms.

34
00:03:26,000 --> 00:03:31,000
And he was looking at them watching them chase after things,

35
00:03:31,000 --> 00:03:36,000
chase after their desires and ambitions and becoming this and becoming that

36
00:03:36,000 --> 00:03:41,000
and setting up goals and purposes and societies and governments

37
00:03:41,000 --> 00:03:44,000
and ideas looking at the people.

38
00:03:44,000 --> 00:03:48,000
And it was really actually the case where he was sitting up in the mountain

39
00:03:48,000 --> 00:03:52,000
and many much of this was going on down below him.

40
00:03:52,000 --> 00:03:56,000
And so this verse was told by the Buddha who came to see him

41
00:03:56,000 --> 00:04:00,000
and commented on this state of affairs,

42
00:04:00,000 --> 00:04:04,000
where we had this person who was free from suffering

43
00:04:04,000 --> 00:04:09,000
and who was free from negligence and he was alert and aware

44
00:04:09,000 --> 00:04:15,000
and seeing reality clearly in a way that ordinary people can't see.

45
00:04:15,000 --> 00:04:26,000
And so he was in a peaceful and a strait or a pure state of mind

46
00:04:26,000 --> 00:04:29,000
as opposed to all of these beings down below,

47
00:04:29,000 --> 00:04:33,000
who were running around chasing after so many things being born

48
00:04:33,000 --> 00:04:36,000
and die born and die again, again, coming and going

49
00:04:36,000 --> 00:04:41,000
and wishing and wanting and hoping and dreaming and building up their lives

50
00:04:41,000 --> 00:04:46,000
saying, this is life, this is happiness and trying to get the things that they want

51
00:04:46,000 --> 00:04:49,000
and then dying again and coming back and trying it all again

52
00:04:49,000 --> 00:04:54,000
and suffering and the anguish and the work that they went through.

53
00:04:54,000 --> 00:05:01,000
And so it's this comparison and it's an expression by the Buddha

54
00:05:01,000 --> 00:05:08,000
and sort of a teaching in the sense of the difference between these two.

55
00:05:08,000 --> 00:05:12,000
How a person, a person living in the world is like these creatures

56
00:05:12,000 --> 00:05:18,000
running around chasing after things, living in the world,

57
00:05:18,000 --> 00:05:24,000
like people live in cities down on the level ground

58
00:05:24,000 --> 00:05:27,000
and then you have this one person up on the mountain

59
00:05:27,000 --> 00:05:31,000
and peace and quiet and in a tranquil environment like this,

60
00:05:31,000 --> 00:05:40,000
looking down at them and saying, wow, like a game or like a movie

61
00:05:40,000 --> 00:05:45,000
looking down and just like ants crawling along.

62
00:05:45,000 --> 00:05:48,000
So the point isn't that a wise person

63
00:05:48,000 --> 00:05:52,000
looking down upon these people and says, oh, those I'm much better than them or something

64
00:05:52,000 --> 00:05:59,000
but it's this state of being withdrawn or being in such a better situation, really.

65
00:05:59,000 --> 00:06:02,000
Because objectively it is a better situation.

66
00:06:02,000 --> 00:06:06,000
It's not that the person in the situation will disdain these people

67
00:06:06,000 --> 00:06:09,000
and look at them all those fools kind of thing.

68
00:06:09,000 --> 00:06:12,000
They might be amazed or not amazed,

69
00:06:12,000 --> 00:06:22,000
but they might find it quite interesting, I suppose, to watch.

70
00:06:22,000 --> 00:06:25,000
As Mahakasabha just spent the morning watching this,

71
00:06:25,000 --> 00:06:29,000
they go on watching these people, watching people and animals,

72
00:06:29,000 --> 00:06:35,000
chase and kill each other, of course.

73
00:06:35,000 --> 00:06:38,000
Well, in any realm of existence we see this.

74
00:06:38,000 --> 00:06:42,000
We see the humans building up societies and rules

75
00:06:42,000 --> 00:06:48,000
and having wars and politics and treaties and diplomacy

76
00:06:48,000 --> 00:06:54,000
and families and societies and religions,

77
00:06:54,000 --> 00:06:56,000
all of these things.

78
00:06:56,000 --> 00:06:58,000
And then you have, of course, the animals.

79
00:06:58,000 --> 00:07:01,000
And if you look at the animal realm, it gets even worse

80
00:07:01,000 --> 00:07:04,000
where you have life as a struggle, life as a fight,

81
00:07:04,000 --> 00:07:07,000
eat or be eaten, killer, be killed.

82
00:07:07,000 --> 00:07:09,000
And then you have in the angel realms.

83
00:07:09,000 --> 00:07:14,000
You have them, they're watching out over the world,

84
00:07:14,000 --> 00:07:17,000
some of them guarding the earth and guarding humans,

85
00:07:17,000 --> 00:07:20,000
some of them living in great luxury

86
00:07:20,000 --> 00:07:26,000
and enjoying their time in heaven and in these paradise realms and so on.

87
00:07:26,000 --> 00:07:36,000
And so looking at all of this, it's this state of being removed from it all

88
00:07:36,000 --> 00:07:40,000
because you're no longer a part of it, a person who's given it all up

89
00:07:40,000 --> 00:07:43,000
sees it as one who sees people down below up on the mountain.

90
00:07:43,000 --> 00:07:46,000
If you've ever been up in a tower, right, it says,

91
00:07:46,000 --> 00:07:50,000
paniapasada.

92
00:07:50,000 --> 00:07:54,000
Then if you're up in a tower, you look down and you see all the beings,

93
00:07:54,000 --> 00:07:56,000
it looks quite peaceful actually.

94
00:07:56,000 --> 00:08:00,000
Suddenly the world with all this craziness and all of the stress and anxiety

95
00:08:00,000 --> 00:08:02,000
looks quite peaceful.

96
00:08:02,000 --> 00:08:04,000
You see little dots moving.

97
00:08:04,000 --> 00:08:07,000
And this is the Buddha said, this is how it is

98
00:08:07,000 --> 00:08:10,000
for a person who has become free from suffering,

99
00:08:10,000 --> 00:08:12,000
because actually you can be among this,

100
00:08:12,000 --> 00:08:16,000
a person who has become free from suffering could walk

101
00:08:16,000 --> 00:08:20,000
as Mahakasabha would walk through the village on arms,

102
00:08:20,000 --> 00:08:22,000
but he wouldn't get involved with anything.

103
00:08:22,000 --> 00:08:24,000
Like in another verse, the Buddha says,

104
00:08:24,000 --> 00:08:28,000
like a bee that takes the pollen and leaves the flower unharmed

105
00:08:28,000 --> 00:08:30,000
without any connection with the flower,

106
00:08:30,000 --> 00:08:34,000
but just takes the pollen and leaves.

107
00:08:34,000 --> 00:08:36,000
The same is with a person who is mindful,

108
00:08:36,000 --> 00:08:39,000
goes through the village as they go through the village,

109
00:08:39,000 --> 00:08:41,000
and they don't cling to anything,

110
00:08:41,000 --> 00:08:48,000
they aren't attached to anything, or delighted by anything.

111
00:08:48,000 --> 00:08:51,000
And so they come in, and even though they might be craziness,

112
00:08:51,000 --> 00:08:53,000
when we go on arms around, sometimes people are fighting,

113
00:08:53,000 --> 00:08:56,000
sometimes there's yelling, sometimes there's parties,

114
00:08:56,000 --> 00:08:59,000
sometimes there's funerals, sometimes there's traffic,

115
00:08:59,000 --> 00:09:04,000
and accidents, and so on, and all of the stresses of the world

116
00:09:04,000 --> 00:09:06,000
by people who have goals and ambitions,

117
00:09:06,000 --> 00:09:09,000
and who want to be rich, and want to be powerful,

118
00:09:09,000 --> 00:09:11,000
and want to have stability in their family,

119
00:09:11,000 --> 00:09:16,000
and want to be healthy, and seeing all of this going on.

120
00:09:16,000 --> 00:09:19,000
But at the same time, even walking through it,

121
00:09:19,000 --> 00:09:22,000
being as though you're way up on a mountain looking down on it.

122
00:09:22,000 --> 00:09:26,000
It's not that it's not looking down in the English sense

123
00:09:26,000 --> 00:09:30,000
of thinking badly of those people.

124
00:09:30,000 --> 00:09:32,000
But it's as though you were actually up on the mountain

125
00:09:32,000 --> 00:09:35,000
looking at it from above, like the little specs.

126
00:09:35,000 --> 00:09:40,000
As you walk through the village, everything is just craziness.

127
00:09:40,000 --> 00:09:43,000
But it's very peaceful to you.

128
00:09:43,000 --> 00:09:46,000
This is the tower of wisdom.

129
00:09:46,000 --> 00:09:50,000
It's not actually a tower, but the mind is so much,

130
00:09:50,000 --> 00:09:52,000
so far removed from all of that,

131
00:09:52,000 --> 00:09:56,000
and has no thoughts of desire for this or that.

132
00:09:56,000 --> 00:10:00,000
So this is what this verse means.

133
00:10:00,000 --> 00:10:02,000
How it relates to our practice,

134
00:10:02,000 --> 00:10:09,000
I think it really points out the contrast

135
00:10:09,000 --> 00:10:12,000
between the path that we're following

136
00:10:12,000 --> 00:10:14,000
to try to become free from suffering,

137
00:10:14,000 --> 00:10:17,000
in the path that most of the world,

138
00:10:17,000 --> 00:10:20,000
most of the universe follows.

139
00:10:20,000 --> 00:10:25,000
If you think of the goals and the really the framework

140
00:10:25,000 --> 00:10:28,000
in which most of the universe works,

141
00:10:28,000 --> 00:10:30,000
the actual framework is quite different.

142
00:10:30,000 --> 00:10:35,000
And so this is how the Buddha explained this teaching

143
00:10:35,000 --> 00:10:39,000
of Petitya Samupada, the dependent origination,

144
00:10:39,000 --> 00:10:44,000
how when a person has these beliefs and these ideas

145
00:10:44,000 --> 00:10:46,000
that there is some purpose,

146
00:10:46,000 --> 00:10:48,000
so people who believe in God and that God

147
00:10:48,000 --> 00:10:50,000
has a purpose for them or so on,

148
00:10:50,000 --> 00:10:52,000
or that society is the purpose

149
00:10:52,000 --> 00:10:54,000
when people want to build up society

150
00:10:54,000 --> 00:10:57,000
and believe they have a duty and society

151
00:10:57,000 --> 00:11:00,000
to build up and to be politically active

152
00:11:00,000 --> 00:11:02,000
and protest and so on,

153
00:11:02,000 --> 00:11:05,000
and demonstrate and express their opinions

154
00:11:05,000 --> 00:11:10,000
and get involved and create and build and organize.

155
00:11:10,000 --> 00:11:16,000
But this is what leads to one that leads them to do things.

156
00:11:16,000 --> 00:11:17,000
Sometimes they will do good things,

157
00:11:17,000 --> 00:11:19,000
sometimes they will do bad things.

158
00:11:19,000 --> 00:11:21,000
When they are angry at people,

159
00:11:21,000 --> 00:11:24,000
they will hurt others, and there's wars and conflicts

160
00:11:24,000 --> 00:11:27,000
and even people who are into good things

161
00:11:27,000 --> 00:11:29,000
can get very angry and passionate about it.

162
00:11:29,000 --> 00:11:32,000
And as a result, can hurt other people and create conflict.

163
00:11:32,000 --> 00:11:34,000
And when there is greed,

164
00:11:34,000 --> 00:11:38,000
the desire that comes from commercialism and materialism,

165
00:11:38,000 --> 00:11:40,000
wanting this and wanting that,

166
00:11:40,000 --> 00:11:42,000
seeing these new electronic devices

167
00:11:42,000 --> 00:11:46,000
and wanting them, seeing beautiful people

168
00:11:46,000 --> 00:11:54,000
dressed attractively and seeing movies

169
00:11:54,000 --> 00:12:00,000
and having music or music players

170
00:12:00,000 --> 00:12:03,000
where you can always be listening to something

171
00:12:03,000 --> 00:12:07,000
intoxicating something attractive

172
00:12:07,000 --> 00:12:10,000
and then having good food and all of these things.

173
00:12:10,000 --> 00:12:16,000
This, the doing about it, the creating about this builds up

174
00:12:16,000 --> 00:12:21,000
a huge system in order to fulfill the requirements.

175
00:12:21,000 --> 00:12:24,000
The need for food, the need for good food,

176
00:12:24,000 --> 00:12:27,000
the need for special food, the need for good sound,

177
00:12:27,000 --> 00:12:29,000
the need for beautiful sights.

178
00:12:29,000 --> 00:12:32,000
When you want to be able to see things when you want to obtain

179
00:12:32,000 --> 00:12:37,000
the family or husband or wife, children,

180
00:12:37,000 --> 00:12:39,000
when you want to be rich,

181
00:12:39,000 --> 00:12:41,000
the things you have to do to get rich,

182
00:12:41,000 --> 00:12:44,000
this is all creating and going on.

183
00:12:44,000 --> 00:12:48,000
And so the clinging, the ambition

184
00:12:48,000 --> 00:12:51,000
in order to gain all of this.

185
00:12:51,000 --> 00:12:53,000
This is the teaching on Patitra Samupa

186
00:12:53,000 --> 00:12:56,000
that goes from one thing to the next to the next,

187
00:12:56,000 --> 00:12:59,000
where the desires, they lead to craving,

188
00:12:59,000 --> 00:13:03,000
the craving leads to addiction or attachment

189
00:13:03,000 --> 00:13:07,000
and attachment leads us to build, to create.

190
00:13:07,000 --> 00:13:11,000
This is what we see going on when you stand up on the mountain.

191
00:13:11,000 --> 00:13:13,000
This is what you see going on in the city down below.

192
00:13:13,000 --> 00:13:19,000
You see constantly building and rushing around and business,

193
00:13:19,000 --> 00:13:21,000
all busyness though.

194
00:13:21,000 --> 00:13:23,000
You see the market, you see.

195
00:13:23,000 --> 00:13:28,000
The car is rushing around and you see so much activity.

196
00:13:28,000 --> 00:13:30,000
When you go up on a mountain and you look down on it,

197
00:13:30,000 --> 00:13:33,000
you can see how crazy actually it all is and how much stress.

198
00:13:33,000 --> 00:13:35,000
And if you look at it as a whole,

199
00:13:35,000 --> 00:13:37,000
actually, if you think about it in a Buddhist sense,

200
00:13:37,000 --> 00:13:40,000
looking up if you were up on a real mountain,

201
00:13:40,000 --> 00:13:44,000
looking down, it would actually give you some intellectual insight,

202
00:13:44,000 --> 00:13:46,000
not meditative insight,

203
00:13:46,000 --> 00:13:48,000
but it would allow you to say, wow,

204
00:13:48,000 --> 00:13:51,000
this really is the case that we really are

205
00:13:51,000 --> 00:13:56,000
so much stress and so much activity.

206
00:13:56,000 --> 00:13:59,000
And what we come to see in the meditation

207
00:13:59,000 --> 00:14:03,000
is how this in the end just leads to suffering.

208
00:14:03,000 --> 00:14:05,000
It leads to dissatisfaction.

209
00:14:05,000 --> 00:14:09,000
It leads us to try and to attain and to not be satisfied

210
00:14:09,000 --> 00:14:11,000
and to want more.

211
00:14:11,000 --> 00:14:14,000
And as with what Mahakasabha said,

212
00:14:14,000 --> 00:14:19,000
it leads to old age sickness and death and the end of it all.

213
00:14:19,000 --> 00:14:22,000
So anything that you build up, anything that you create,

214
00:14:22,000 --> 00:14:24,000
in the end you have to let go of.

215
00:14:24,000 --> 00:14:27,000
You have to be deprived of and you have to leave behind.

216
00:14:27,000 --> 00:14:29,000
And so when someone can see this,

217
00:14:29,000 --> 00:14:32,000
and these special powers that no one wants to hear about,

218
00:14:32,000 --> 00:14:34,000
because it's like, we don't have them,

219
00:14:34,000 --> 00:14:36,000
and so we therefore think that they're probably

220
00:14:36,000 --> 00:14:39,000
don't exist and there's no proof and so on like that.

221
00:14:39,000 --> 00:14:44,000
But when you have these magical powers,

222
00:14:44,000 --> 00:14:47,000
not magical, when you have this clear vision

223
00:14:47,000 --> 00:14:51,000
that allows you to penetrate into the very fabric of reality,

224
00:14:51,000 --> 00:14:57,000
you can see the beings being big born and dying.

225
00:14:57,000 --> 00:15:00,000
And so you'll see, oh, look at them building, building,

226
00:15:00,000 --> 00:15:02,000
building, building and then dying.

227
00:15:02,000 --> 00:15:05,000
And then born again and trying the same thing all over again.

228
00:15:10,000 --> 00:15:13,000
Now for most of us, this isn't possible, right?

229
00:15:13,000 --> 00:15:15,000
So as I said, people have a problem with this

230
00:15:15,000 --> 00:15:16,000
because they don't believe it.

231
00:15:16,000 --> 00:15:19,000
They think this is silly while talking about this

232
00:15:19,000 --> 00:15:23,000
and basically how can we believe in these sorts of things?

233
00:15:23,000 --> 00:15:27,000
So it might be good to address that as well here.

234
00:15:27,000 --> 00:15:31,000
If we talk about, we talk about understanding,

235
00:15:31,000 --> 00:15:33,000
but teach us a mupada.

236
00:15:33,000 --> 00:15:37,000
It talks about getting old, sick and dying and being born again.

237
00:15:37,000 --> 00:15:41,000
And so people say, well, how can you,

238
00:15:41,000 --> 00:15:45,000
how can you, you're just accepting this on blind faith?

239
00:15:45,000 --> 00:15:48,000
How can you accept this teaching when you yourself

240
00:15:48,000 --> 00:15:51,000
have an experience that if you practice meditation,

241
00:15:51,000 --> 00:15:54,000
maybe you'll never gain these magical powers.

242
00:15:54,000 --> 00:15:56,000
But let's see what you gain in meditation

243
00:15:56,000 --> 00:15:58,000
is much more important.

244
00:15:58,000 --> 00:16:00,000
What you gain in meditation is the moment to moment

245
00:16:00,000 --> 00:16:02,000
understanding of how this works.

246
00:16:02,000 --> 00:16:05,000
You see how from moment to moment we cling

247
00:16:05,000 --> 00:16:07,000
and it leads us to create.

248
00:16:07,000 --> 00:16:09,000
When something good comes up,

249
00:16:09,000 --> 00:16:11,000
we hold on to it and we want to gain it

250
00:16:11,000 --> 00:16:13,000
and so we create more.

251
00:16:13,000 --> 00:16:15,000
You do this every moment,

252
00:16:15,000 --> 00:16:18,000
not just talking about in your life how you have plans

253
00:16:18,000 --> 00:16:21,000
and ambitions and goals and dreams and dreams

254
00:16:21,000 --> 00:16:25,000
and how you get caught up in family life and in debt

255
00:16:25,000 --> 00:16:30,000
and so many different things and conflict with others,

256
00:16:30,000 --> 00:16:33,000
it actually occurs moment to moment.

257
00:16:33,000 --> 00:16:37,000
Every time you get angry or you attach to something,

258
00:16:37,000 --> 00:16:39,000
you change who you are.

259
00:16:39,000 --> 00:16:47,000
You become more coarse and more stressed,

260
00:16:47,000 --> 00:16:54,000
more active state.

261
00:16:54,000 --> 00:16:57,000
You are more creative, creating that,

262
00:16:57,000 --> 00:17:00,000
causing trouble for yourself.

263
00:17:00,000 --> 00:17:04,000
Making plans and ambitions and wanting more and getting in debt

264
00:17:04,000 --> 00:17:07,000
and so on and so on.

265
00:17:07,000 --> 00:17:13,000
And this is really why I think

266
00:17:13,000 --> 00:17:17,000
there's really no problem with even with understanding

267
00:17:17,000 --> 00:17:21,000
with talking about this idea of getting old sick

268
00:17:21,000 --> 00:17:24,000
and being born again and of people who can actually see this

269
00:17:24,000 --> 00:17:26,000
and rebirth and so on.

270
00:17:26,000 --> 00:17:28,000
Because in the meditation practice,

271
00:17:28,000 --> 00:17:31,000
you can see reality working for moment to moment

272
00:17:31,000 --> 00:17:33,000
and you can see how it leads in this direction,

273
00:17:33,000 --> 00:17:36,000
how it leads to creation

274
00:17:36,000 --> 00:17:39,000
and then how the creation leads to wanting more and eventually

275
00:17:39,000 --> 00:17:43,000
how it all collapses and collapses on a moment to moment basis.

276
00:17:43,000 --> 00:17:49,000
The thing that Mahakasabhasal was on a grand scale,

277
00:17:49,000 --> 00:17:52,000
he was able to extend this out

278
00:17:52,000 --> 00:17:55,000
and to see how it was working all around him

279
00:17:55,000 --> 00:17:56,000
for a moment to moment

280
00:17:56,000 --> 00:18:00,000
and to see how beings were giving rise to these mind states

281
00:18:00,000 --> 00:18:04,000
and going on and having their dreams destroyed.

282
00:18:04,000 --> 00:18:06,000
You don't need to understand,

283
00:18:06,000 --> 00:18:11,000
you don't need to have a realization of a past life

284
00:18:11,000 --> 00:18:14,000
or a future life or so on.

285
00:18:14,000 --> 00:18:19,000
And you don't need to have these powers yourself

286
00:18:19,000 --> 00:18:20,000
to understand how it works

287
00:18:20,000 --> 00:18:22,000
because in practicing meditation,

288
00:18:22,000 --> 00:18:27,000
you're actually playing with or observing,

289
00:18:27,000 --> 00:18:30,000
interacting with the very fabric of reality

290
00:18:30,000 --> 00:18:32,000
and so you can see how this works.

291
00:18:32,000 --> 00:18:35,000
You can see how with a strong mind you can penetrate into it

292
00:18:35,000 --> 00:18:39,000
and you can see things far away

293
00:18:39,000 --> 00:18:42,000
and see things in the past as people remember past lives

294
00:18:42,000 --> 00:18:46,000
you can see other people car away and so on.

295
00:18:46,000 --> 00:18:51,000
But the other thing to say is that the magical powers

296
00:18:51,000 --> 00:18:53,000
isn't really the point of the verse here

297
00:18:53,000 --> 00:18:55,000
and it's not the point of any of the Dhamma Bhata,

298
00:18:55,000 --> 00:18:58,000
any of the verses at all,

299
00:18:58,000 --> 00:19:00,000
the verses aren't about the magical powers.

300
00:19:00,000 --> 00:19:03,000
These background stories are just providing some context

301
00:19:03,000 --> 00:19:06,000
and so whether or not you believe in these powers

302
00:19:06,000 --> 00:19:08,000
that Mahakasabha had,

303
00:19:08,000 --> 00:19:12,000
it has no bearing on the actual teaching itself,

304
00:19:12,000 --> 00:19:15,000
which is that through seeing this,

305
00:19:15,000 --> 00:19:16,000
whether it's in a meditative sense

306
00:19:16,000 --> 00:19:20,000
or whether it gets further than that

307
00:19:20,000 --> 00:19:23,000
and you're able to see all beings arising

308
00:19:23,000 --> 00:19:25,000
and seeing and coming and going

309
00:19:25,000 --> 00:19:27,000
as the Buddha was able to do

310
00:19:27,000 --> 00:19:30,000
or even somewhere in between where Mahakasabha was able to see

311
00:19:30,000 --> 00:19:32,000
some beings arising and seeing

312
00:19:32,000 --> 00:19:35,000
even though he's not a Buddha.

313
00:19:35,000 --> 00:19:40,000
Is that it changes your ambition,

314
00:19:40,000 --> 00:19:43,000
it changes your drive, it changes your goal

315
00:19:43,000 --> 00:19:45,000
and your path in life

316
00:19:45,000 --> 00:19:47,000
and it turns you around in the opposite direction

317
00:19:47,000 --> 00:19:49,000
and this is where the contrast comes in.

318
00:19:49,000 --> 00:19:52,000
That most people are through their ambitions

319
00:19:52,000 --> 00:19:54,000
and their goals are creating more and more

320
00:19:54,000 --> 00:19:57,000
and more and building up their stress and suffering

321
00:19:57,000 --> 00:20:00,000
and going on and on and again and again,

322
00:20:00,000 --> 00:20:03,000
chasing after and building up

323
00:20:03,000 --> 00:20:06,000
and thinking that they've finally found true peace

324
00:20:06,000 --> 00:20:09,000
only to have it taken away from them

325
00:20:09,000 --> 00:20:13,000
through calamity or disaster

326
00:20:13,000 --> 00:20:16,000
or finally through old age sickness and death

327
00:20:16,000 --> 00:20:19,000
and then building it up again and again

328
00:20:19,000 --> 00:20:22,000
having to learn the same lessons again and again

329
00:20:22,000 --> 00:20:23,000
and if not learning them

330
00:20:23,000 --> 00:20:27,000
then falling into disaster and suffering.

331
00:20:27,000 --> 00:20:31,000
As opposed to going the other way

332
00:20:31,000 --> 00:20:34,000
where when a person sees this

333
00:20:34,000 --> 00:20:36,000
and when a person sees what this is leading to

334
00:20:36,000 --> 00:20:38,000
how it's not leading to peace and happiness

335
00:20:38,000 --> 00:20:39,000
and freedom from suffering,

336
00:20:39,000 --> 00:20:42,000
how they start deconstructing

337
00:20:42,000 --> 00:20:45,000
and they give up their desires,

338
00:20:45,000 --> 00:20:47,000
they give up their attachments,

339
00:20:47,000 --> 00:20:50,000
they give up their aversionings

340
00:20:50,000 --> 00:20:53,000
and they simply see everything

341
00:20:53,000 --> 00:20:55,000
clearly as it is.

342
00:20:55,000 --> 00:20:56,000
They come to straighten their minds

343
00:20:56,000 --> 00:20:58,000
without their able to see the experience

344
00:20:58,000 --> 00:21:01,000
whether it be a positive experience

345
00:21:01,000 --> 00:21:02,000
or a negative experience

346
00:21:02,000 --> 00:21:04,000
to see it clearly.

347
00:21:04,000 --> 00:21:07,000
So to have a powerful amount of mind

348
00:21:07,000 --> 00:21:08,000
that is so sharp

349
00:21:08,000 --> 00:21:11,000
that it's able to see just the bare reality

350
00:21:11,000 --> 00:21:15,000
and to cut to the very core of experience

351
00:21:15,000 --> 00:21:18,000
so that they don't give rise the desire

352
00:21:18,000 --> 00:21:20,000
or addiction and they don't have ambitions

353
00:21:20,000 --> 00:21:22,000
and they don't want to become something

354
00:21:22,000 --> 00:21:25,000
and so they don't chase after and they don't create

355
00:21:25,000 --> 00:21:27,000
and because they don't create

356
00:21:27,000 --> 00:21:32,000
and they don't have all of this

357
00:21:32,000 --> 00:21:34,000
burden to carry around

358
00:21:34,000 --> 00:21:35,000
and all of the suffering

359
00:21:35,000 --> 00:21:38,000
and they aren't in amongst

360
00:21:38,000 --> 00:21:41,000
the suffering and the stress

361
00:21:41,000 --> 00:21:43,000
so when an ordinary person

362
00:21:43,000 --> 00:21:45,000
goes into the city,

363
00:21:45,000 --> 00:21:47,000
a person who has never practiced meditation

364
00:21:47,000 --> 00:21:49,000
is not interested in mental development

365
00:21:49,000 --> 00:21:51,000
and who is really keen on materialism

366
00:21:51,000 --> 00:21:53,000
when they go into the city,

367
00:21:53,000 --> 00:21:56,000
they go crazy, they see beautiful things

368
00:21:56,000 --> 00:21:58,000
and they chase after them

369
00:21:58,000 --> 00:21:59,000
and they want this and want that

370
00:21:59,000 --> 00:22:01,000
go out to the restaurants and get food

371
00:22:01,000 --> 00:22:03,000
and maybe alcohol

372
00:22:03,000 --> 00:22:06,000
and dancing and buying things here and there

373
00:22:06,000 --> 00:22:10,000
and really get caught up in it

374
00:22:10,000 --> 00:22:13,000
but when a wise person,

375
00:22:13,000 --> 00:22:14,000
as I was saying,

376
00:22:14,000 --> 00:22:16,000
goes into the big city

377
00:22:16,000 --> 00:22:18,000
they go in looking at it

378
00:22:18,000 --> 00:22:19,000
like a person up on a mountain

379
00:22:19,000 --> 00:22:21,000
because they're actually standing

380
00:22:21,000 --> 00:22:23,000
on the mountain of wisdom.

381
00:22:23,000 --> 00:22:25,000
So for our practice

382
00:22:25,000 --> 00:22:27,000
we understand these are the two paths.

383
00:22:27,000 --> 00:22:30,000
The path towards accumulation

384
00:22:30,000 --> 00:22:32,000
and the path away from the community

385
00:22:32,000 --> 00:22:34,000
and the path towards becoming

386
00:22:34,000 --> 00:22:37,000
busier and busier and getting involved

387
00:22:37,000 --> 00:22:40,000
in creating

388
00:22:40,000 --> 00:22:42,000
and taking the reality that we have

389
00:22:42,000 --> 00:22:44,000
and making something out of it

390
00:22:44,000 --> 00:22:45,000
as opposed to letting it go

391
00:22:45,000 --> 00:22:47,000
and letting it be

392
00:22:47,000 --> 00:22:49,000
and not creating

393
00:22:49,000 --> 00:22:51,000
and simply seeing it for what it is.

394
00:22:51,000 --> 00:22:53,000
So people sometimes ask

395
00:22:53,000 --> 00:22:56,000
why, if they've never practiced meditation,

396
00:22:56,000 --> 00:22:58,000
why do we just do walking back and forth?

397
00:22:58,000 --> 00:22:59,000
What is the point of it?

398
00:22:59,000 --> 00:23:01,000
Why do you just do sitting there?

399
00:23:01,000 --> 00:23:03,000
Why are you wasting your time?

400
00:23:03,000 --> 00:23:06,000
They don't understand what is the purpose of it

401
00:23:06,000 --> 00:23:07,000
because in their mind

402
00:23:07,000 --> 00:23:09,000
everything has to have a purpose.

403
00:23:09,000 --> 00:23:10,000
You do something

404
00:23:10,000 --> 00:23:12,000
in order to get something in the future.

405
00:23:12,000 --> 00:23:14,000
It has to create something.

406
00:23:14,000 --> 00:23:17,000
What you do has to bring some result.

407
00:23:17,000 --> 00:23:19,000
So they see you're walking back and forth

408
00:23:19,000 --> 00:23:20,000
and there's no result.

409
00:23:20,000 --> 00:23:22,000
Well, that's the point.

410
00:23:22,000 --> 00:23:25,000
The point is that by walking back and forth,

411
00:23:25,000 --> 00:23:27,000
I'm clearing out the results.

412
00:23:27,000 --> 00:23:29,000
I'm doing something that is not bringing results.

413
00:23:29,000 --> 00:23:32,000
And it's not just walking in back and forth, of course.

414
00:23:32,000 --> 00:23:33,000
Because the truth is

415
00:23:33,000 --> 00:23:35,000
when an ordinary person walks back and forth,

416
00:23:35,000 --> 00:23:37,000
they do create results.

417
00:23:37,000 --> 00:23:38,000
Walking back and forth,

418
00:23:38,000 --> 00:23:39,000
they're thinking about,

419
00:23:39,000 --> 00:23:40,000
oh, tomorrow I've got to do this

420
00:23:40,000 --> 00:23:42,000
and oh, wouldn't it be great if I did that?

421
00:23:42,000 --> 00:23:45,000
And so even just walking back and forth,

422
00:23:45,000 --> 00:23:47,000
they're still creating

423
00:23:47,000 --> 00:23:48,000
plans and ambitions

424
00:23:48,000 --> 00:23:50,000
and after they finish,

425
00:23:50,000 --> 00:23:52,000
they'll go off and enact those plans.

426
00:23:52,000 --> 00:23:55,000
But the wise person or the path

427
00:23:55,000 --> 00:23:57,000
that we're trying to follow here

428
00:23:57,000 --> 00:23:59,000
is to go in the opposite direction.

429
00:23:59,000 --> 00:24:01,000
That the more we walk,

430
00:24:01,000 --> 00:24:03,000
the closer we come to just walking

431
00:24:03,000 --> 00:24:04,000
so that when the foot moves,

432
00:24:04,000 --> 00:24:05,000
we know the foot is moving.

433
00:24:05,000 --> 00:24:06,000
When a thought arises,

434
00:24:06,000 --> 00:24:08,000
we know the thought arises.

435
00:24:08,000 --> 00:24:09,000
And all of our ambitions

436
00:24:09,000 --> 00:24:11,000
and our attachments

437
00:24:11,000 --> 00:24:14,000
are washed or given up.

438
00:24:14,000 --> 00:24:16,000
Once we see the stress

439
00:24:16,000 --> 00:24:17,000
and we see the suffering,

440
00:24:17,000 --> 00:24:19,000
the idea is to be able to see them

441
00:24:19,000 --> 00:24:22,000
clearly and see them for what they are.

442
00:24:22,000 --> 00:24:25,000
See them that these are pointless

443
00:24:25,000 --> 00:24:27,000
and meaningless.

444
00:24:27,000 --> 00:24:29,000
They have no benefit and no purpose for us

445
00:24:29,000 --> 00:24:32,000
to be able to let go of them

446
00:24:32,000 --> 00:24:34,000
and see them for what they are.

447
00:24:34,000 --> 00:24:36,000
So this is really the core

448
00:24:36,000 --> 00:24:37,000
of what we're practicing.

449
00:24:37,000 --> 00:24:40,000
We're practicing to

450
00:24:40,000 --> 00:24:42,000
rise above

451
00:24:42,000 --> 00:24:46,000
all of the reality

452
00:24:46,000 --> 00:24:48,000
that we find around us.

453
00:24:48,000 --> 00:24:51,000
Because really, it's all contrived.

454
00:24:51,000 --> 00:24:53,000
All of the beauty that we see,

455
00:24:53,000 --> 00:24:55,000
all the wonder that we see.

456
00:24:55,000 --> 00:24:58,000
It's all just formations.

457
00:24:58,000 --> 00:24:59,000
It's all artificial.

458
00:24:59,000 --> 00:25:01,000
All of what we call natural.

459
00:25:01,000 --> 00:25:02,000
It's really just

460
00:25:02,000 --> 00:25:05,000
century after century

461
00:25:05,000 --> 00:25:07,000
as time goes on

462
00:25:07,000 --> 00:25:08,000
has just been built up.

463
00:25:08,000 --> 00:25:09,000
And it's been built up

464
00:25:09,000 --> 00:25:11,000
based on our desires

465
00:25:11,000 --> 00:25:12,000
and our attractions.

466
00:25:12,000 --> 00:25:13,000
We've built this.

467
00:25:13,000 --> 00:25:16,000
We've created this state

468
00:25:16,000 --> 00:25:18,000
and this body that we've created.

469
00:25:18,000 --> 00:25:20,000
We've created it out of a desire

470
00:25:20,000 --> 00:25:21,000
out of an attachment

471
00:25:21,000 --> 00:25:23,000
out of partiality.

472
00:25:23,000 --> 00:25:26,000
And it's all just artificial.

473
00:25:26,000 --> 00:25:30,000
It's the result of the craziness

474
00:25:30,000 --> 00:25:32,000
that we've fallen into.

475
00:25:32,000 --> 00:25:35,000
And so by following after it

476
00:25:35,000 --> 00:25:36,000
and developing it,

477
00:25:36,000 --> 00:25:38,000
we build up more and more

478
00:25:38,000 --> 00:25:40,000
stress and more and more suffering.

479
00:25:40,000 --> 00:25:42,000
What we're doing here is to

480
00:25:42,000 --> 00:25:43,000
let go of it

481
00:25:43,000 --> 00:25:44,000
to give it up

482
00:25:44,000 --> 00:25:46,000
and even come out of this body

483
00:25:46,000 --> 00:25:48,000
so that we don't need any

484
00:25:48,000 --> 00:25:50,000
particular contrivance

485
00:25:50,000 --> 00:25:52,000
or any construct

486
00:25:52,000 --> 00:25:54,000
to bring us peace and happiness.

487
00:25:54,000 --> 00:25:56,000
Even being a human,

488
00:25:56,000 --> 00:25:58,000
even the state of humanity

489
00:25:58,000 --> 00:26:00,000
we come out of that.

490
00:26:00,000 --> 00:26:01,000
And in the end,

491
00:26:01,000 --> 00:26:02,000
you can't say that

492
00:26:02,000 --> 00:26:04,000
an enlightened person is a human

493
00:26:04,000 --> 00:26:06,000
or a God or an angel

494
00:26:06,000 --> 00:26:08,000
or anything

495
00:26:08,000 --> 00:26:09,000
because they've come out of it

496
00:26:09,000 --> 00:26:10,000
and they have no attachment

497
00:26:10,000 --> 00:26:12,000
they have no desire

498
00:26:12,000 --> 00:26:13,000
or no ambitions

499
00:26:13,000 --> 00:26:15,000
in regard to any of that.

500
00:26:15,000 --> 00:26:17,000
So they stand above

501
00:26:17,000 --> 00:26:18,000
the world

502
00:26:18,000 --> 00:26:19,000
like a person who stands

503
00:26:19,000 --> 00:26:20,000
on an mountain

504
00:26:20,000 --> 00:26:21,000
looks down at the world

505
00:26:21,000 --> 00:26:22,000
a lot.

506
00:26:22,000 --> 00:26:24,000
So that's the

507
00:26:24,000 --> 00:26:26,000
meaning and the significance

508
00:26:26,000 --> 00:26:27,000
of this verse

509
00:26:27,000 --> 00:26:29,000
and that is another teaching

510
00:26:29,000 --> 00:26:30,000
on the Damapada.

511
00:26:30,000 --> 00:26:32,000
Thank you all for tuning in

512
00:26:32,000 --> 00:26:47,000
and see you next time.

