1
00:00:00,000 --> 00:00:07,000
And so that will go up as its own separate video tonight, and you'll see it probably tomorrow.

2
00:00:07,000 --> 00:00:16,000
This video is going to be... Sorry.

3
00:00:16,000 --> 00:00:24,000
Just answering questions.

4
00:00:24,000 --> 00:00:29,000
I may be talking about a verse, or a quote.

5
00:00:29,000 --> 00:00:32,000
I have quotes.

6
00:00:32,000 --> 00:00:36,000
So with me tonight is Robin.

7
00:00:36,000 --> 00:00:37,000
Robin?

8
00:00:37,000 --> 00:00:40,000
Hello, Dante.

9
00:00:40,000 --> 00:00:47,000
And all the people at meditation.ceremungalow.org.

10
00:00:47,000 --> 00:00:49,000
Is there anybody there?

11
00:00:49,000 --> 00:00:51,000
We have questions.

12
00:00:51,000 --> 00:00:53,000
Oh, good.

13
00:00:53,000 --> 00:00:54,000
Okay.

14
00:00:54,000 --> 00:00:55,000
Shoot.

15
00:00:55,000 --> 00:00:56,000
Okay.

16
00:00:56,000 --> 00:00:57,000
This is a long question.

17
00:00:57,000 --> 00:01:05,000
Dear Bante, when one starts practicing Sati Patana vipassana without any prior training in any kind of meditation,

18
00:01:05,000 --> 00:01:09,000
is it necessary to have distraction-free surroundings?

19
00:01:09,000 --> 00:01:17,000
I understand that the practice has to do with being mindful of absolutely any kind of experience that arises out of the six-cent stores,

20
00:01:17,000 --> 00:01:22,000
no matter how chaotic the environment, always noting the most distinct and strong experience.

21
00:01:22,000 --> 00:01:27,000
So basically, there is no such thing as necessity for distraction-free surroundings.

22
00:01:27,000 --> 00:01:34,000
But at the beginning, it feels like the practice cannot go anywhere when there are so many things happening every second.

23
00:01:34,000 --> 00:01:38,000
I understand that the practice is not about concentration.

24
00:01:38,000 --> 00:01:46,000
Is one guarantee to eventually progress on the path by following the teaching, even if the practicing environment is chaotic from the beginning,

25
00:01:46,000 --> 00:01:50,000
just by being able to have mindful moments here and there?

26
00:01:50,000 --> 00:01:56,000
Even though momentary concentration is the only kind of concentration that has something to do with the practice,

27
00:01:56,000 --> 00:02:02,000
does one anyway need stillness and quietness at the beginning?

28
00:02:02,000 --> 00:02:08,000
I think you might be confusing two things, you see, because it's not the environment.

29
00:02:08,000 --> 00:02:17,000
But if you're talking about, so you mentioned something about just having mindful moments here and there.

30
00:02:17,000 --> 00:02:26,000
But okay, see, if you meant by that that you don't have an environment that you can actually physically sit down and meditate it,

31
00:02:26,000 --> 00:02:32,000
like if you do that people are going to hit you or scold you or think you're crazy from garbage at you or something.

32
00:02:32,000 --> 00:02:37,000
Then yeah, that's problematic.

33
00:02:37,000 --> 00:02:42,000
If you're talking just about the distraction of your mind,

34
00:02:42,000 --> 00:02:55,000
but there are limits I think, and the whole reason for doing formal meditation in the first place is to limit our distractions.

35
00:02:55,000 --> 00:03:06,000
So yes, there is something to what you're saying that it shouldn't be just full contact right away.

36
00:03:06,000 --> 00:03:13,000
You said there are different levels of distraction if people are talking around you, that can be really difficult.

37
00:03:13,000 --> 00:03:19,000
But I've had meditators who were able to sit through it.

38
00:03:19,000 --> 00:03:20,000
I've done it.

39
00:03:20,000 --> 00:03:28,000
I've had number ones doing a course in Thailand and there were monks sitting right outside my kuti at the table,

40
00:03:28,000 --> 00:03:35,000
right outside my hat, talking all day, they sat there talking right in front of my hat.

41
00:03:35,000 --> 00:03:38,000
So I heard them walking and sitting.

42
00:03:38,000 --> 00:03:40,000
I couldn't really understand.

43
00:03:40,000 --> 00:03:42,000
It was difficult.

44
00:03:42,000 --> 00:03:50,000
But eventually, really the key is that eventually you're able to, your mind is able to put it aside.

45
00:03:50,000 --> 00:03:52,000
And that's really what we're looking for.

46
00:03:52,000 --> 00:03:55,000
I mean, you say your practice isn't able to go anywhere.

47
00:03:55,000 --> 00:03:57,000
Well, I wonder where you're trying to go.

48
00:03:57,000 --> 00:03:59,000
Does that mean going anywhere?

49
00:03:59,000 --> 00:04:01,000
It's a whole desire to go somewhere.

50
00:04:01,000 --> 00:04:03,000
It's really a problem.

51
00:04:03,000 --> 00:04:05,000
You're not trying to go somewhere.

52
00:04:05,000 --> 00:04:07,000
You're trying to understand what's going on in your mind.

53
00:04:07,000 --> 00:04:09,000
So at that moment, what's going on in your mind?

54
00:04:09,000 --> 00:04:11,000
Your mind is being distracted by things.

55
00:04:11,000 --> 00:04:13,000
So you're learning about that.

56
00:04:13,000 --> 00:04:17,000
Meditation is for the purpose of learning about that, understanding it.

57
00:04:17,000 --> 00:04:19,000
What's going on?

58
00:04:19,000 --> 00:04:22,000
It's about learning about your habits, how you react to things.

59
00:04:22,000 --> 00:04:24,000
It's about learning to react differently.

60
00:04:24,000 --> 00:04:26,000
Learning why your reactions have problems.

61
00:04:26,000 --> 00:04:32,000
Learning about the things that you're reacting to and realizing that they're not worth reacting to.

62
00:04:32,000 --> 00:04:35,000
So it's actually a very important learning practice.

63
00:04:35,000 --> 00:04:44,000
Now, if it's overwhelming, then you have to decide for yourself how much of a retreat you need.

64
00:04:44,000 --> 00:04:49,000
But that's certainly not inherently counterproductive.

65
00:04:49,000 --> 00:04:51,000
It just means you're weak.

66
00:04:51,000 --> 00:04:52,000
Most of us are fairly weak.

67
00:04:52,000 --> 00:04:55,000
So if you want, that's what you're suggesting here.

68
00:04:55,000 --> 00:04:59,000
And yes, you can admit your weakness and you can back off.

69
00:04:59,000 --> 00:05:01,000
But you have to admit your weakness and say,

70
00:05:01,000 --> 00:05:03,000
OK, this isn't a problem with the situation.

71
00:05:03,000 --> 00:05:05,000
This is a problem with me.

72
00:05:05,000 --> 00:05:10,000
And then you have to assess as to whether you're hiding from your challenges,

73
00:05:10,000 --> 00:05:14,000
or whether you are retreating temporarily.

74
00:05:14,000 --> 00:05:17,000
As eventually, you're going to have to deal with it.

75
00:05:17,000 --> 00:05:25,000
I remember once I was in Israel and I went and did meditation on the beach in Tel Aviv.

76
00:05:25,000 --> 00:05:32,000
And these flies came and my whole face was covered in flies, fly, it's fly.

77
00:05:32,000 --> 00:05:33,000
It was bizarre.

78
00:05:33,000 --> 00:05:35,000
Well, not covered, but the room may be five or ten flies.

79
00:05:35,000 --> 00:05:41,000
And then first I thought, if you've ever had a fly on your face, imagine five or ten there.

80
00:05:41,000 --> 00:05:44,000
And it's not like having an ant or something.

81
00:05:44,000 --> 00:05:48,000
House flies because they have tongues on their feet or something.

82
00:05:48,000 --> 00:05:55,000
They have really, I shook them off and had to walk away.

83
00:05:55,000 --> 00:05:59,000
And I went and told them, the teacher of this Israeli man who's now a monk,

84
00:05:59,000 --> 00:06:01,000
he's a very teacher.

85
00:06:01,000 --> 00:06:09,000
And he said, no, if you don't deal with it, now you'll have to deal with it eventually.

86
00:06:09,000 --> 00:06:15,000
And his accent, but he had this thick, thick Middle Eastern accent.

87
00:06:15,000 --> 00:06:19,000
He said, whatever experience you don't deal with is now you'll have to deal with it eventually.

88
00:06:19,000 --> 00:06:25,000
I'm still a bit skeptical because we have that one's pretty tough.

89
00:06:25,000 --> 00:06:27,000
No, he's right.

90
00:06:27,000 --> 00:06:31,000
I don't know that I totally agree that you'll have to eventually deal with it.

91
00:06:31,000 --> 00:06:33,000
It's not really that.

92
00:06:33,000 --> 00:06:36,000
It's just a sign that you are weak.

93
00:06:36,000 --> 00:06:40,000
And I think maybe what he was meaning and what he was saying,

94
00:06:40,000 --> 00:06:43,000
was not that eventually I'd have to deal with flies on my face.

95
00:06:43,000 --> 00:06:45,000
And that's not really the point.

96
00:06:45,000 --> 00:06:47,000
But there's some reason why I can't deal with it.

97
00:06:47,000 --> 00:06:49,000
But I couldn't deal with the flies on my face.

98
00:06:49,000 --> 00:06:52,000
And eventually I'm going to have to deal with that reason.

99
00:06:52,000 --> 00:06:57,000
There's no way around our versions,

100
00:06:57,000 --> 00:06:59,000
no way around our problem.

101
00:06:59,000 --> 00:07:07,000
Eventually you have to deal with it.

102
00:07:07,000 --> 00:07:10,000
So that's certainly an answer, yeah.

103
00:07:10,000 --> 00:07:14,000
I don't think anyone needs stillness or quiet,

104
00:07:14,000 --> 00:07:17,000
but you retreat sometime.

105
00:07:17,000 --> 00:07:18,000
Just be careful.

106
00:07:18,000 --> 00:07:20,000
Because I know that I had one meditator once.

107
00:07:20,000 --> 00:07:22,000
I was watching one meditator once.

108
00:07:22,000 --> 00:07:23,000
I think I've mentioned this.

109
00:07:23,000 --> 00:07:25,000
He started sitting on the floor.

110
00:07:25,000 --> 00:07:26,000
And then we said he could use a cushion.

111
00:07:26,000 --> 00:07:28,000
And so they put a little cushion underneath.

112
00:07:28,000 --> 00:07:31,000
And then I watched him throw it from day to day,

113
00:07:31,000 --> 00:07:33,000
like over a couple of days.

114
00:07:33,000 --> 00:07:36,000
He got higher and higher and higher.

115
00:07:36,000 --> 00:07:40,000
And then it was might as well be sitting on the chair.

116
00:07:40,000 --> 00:07:43,000
And I said, we're really trying to go the other way.

117
00:07:43,000 --> 00:07:48,000
No, we need to get lower and see if it's going to get more tolerance

118
00:07:48,000 --> 00:07:51,000
of pain than so.

119
00:07:51,000 --> 00:07:52,000
So that's important.

120
00:07:52,000 --> 00:08:00,000
Just to be careful that you're not going in the wrong direction.

121
00:08:00,000 --> 00:08:01,000
Hello, Bante.

122
00:08:01,000 --> 00:08:05,000
There are times when I become disconcerted by the breath that I draw.

123
00:08:05,000 --> 00:08:10,000
Whether it be too little or too much and feel anxious and forced to control it.

124
00:08:10,000 --> 00:08:14,000
Instead of observing it objectively as it arises naturally.

125
00:08:14,000 --> 00:08:17,000
I also have a diagnosed anxiety disorder.

126
00:08:17,000 --> 00:08:23,000
And was wondering, is the anxiety acting as a hindrance by rising alongside this disruption?

127
00:08:23,000 --> 00:08:29,000
Or is it or is my anxiety only another object that could use more attention as it arises?

128
00:08:29,000 --> 00:08:33,000
And therefore is the disruption I mentioned only the product of a working,

129
00:08:33,000 --> 00:08:35,000
yet challenging practice.

130
00:08:35,000 --> 00:08:37,000
I'm sorry if this is a little confusing.

131
00:08:47,000 --> 00:08:50,000
Anxiety is acting as a hindrance.

132
00:08:50,000 --> 00:08:51,000
That's certain.

133
00:08:51,000 --> 00:09:00,000
But it's only an entrance in a sense of hindering your happiness,

134
00:09:00,000 --> 00:09:03,000
your peace of mind.

135
00:09:03,000 --> 00:09:05,000
In the past, we don't really think of the mishingrances.

136
00:09:05,000 --> 00:09:09,000
We call them hindrances, but only because that's what they're called elsewhere.

137
00:09:09,000 --> 00:09:16,000
And the practice hindrances are considered important meditation objects.

138
00:09:16,000 --> 00:09:20,000
So you do take them as a meditation object just like anything else.

139
00:09:20,000 --> 00:09:21,000
And I've talked about anxiety before.

140
00:09:21,000 --> 00:09:25,000
Not only anxiety, you also have to focus on the...

141
00:09:25,000 --> 00:09:30,000
As with everything, you have to focus on what surrounds the anxiety, the physical aspect.

142
00:09:30,000 --> 00:09:34,000
So the tension that's going to arise in your breath probably,

143
00:09:34,000 --> 00:09:41,000
the headache sometimes tension in the shoulders, the heart beating fast, that kind of thing.

144
00:09:41,000 --> 00:09:42,000
Many different things.

145
00:09:42,000 --> 00:09:44,000
Fear is involved.

146
00:09:44,000 --> 00:09:55,000
So you're seeing how your mind works, and it works not very well.

147
00:09:55,000 --> 00:09:56,000
That's normal.

148
00:09:56,000 --> 00:10:04,000
There's nothing that should be disconcerting about that.

149
00:10:04,000 --> 00:10:08,000
Shouldn't be something that disheartening.

150
00:10:08,000 --> 00:10:10,000
There's nothing disheartening about that.

151
00:10:10,000 --> 00:10:22,000
What you're trying to do is understand and learn to let go of what you can let go of.

152
00:10:22,000 --> 00:10:34,000
And through understanding to not exactly change, but the understanding itself will change many things about your mind.

153
00:10:34,000 --> 00:10:42,000
It will help change the anxiety, as you watch the anxiety, you're basically teaching yourself the problem with anxiety.

154
00:10:42,000 --> 00:10:50,000
And your mind starts too.

155
00:10:50,000 --> 00:10:53,000
There's the change, how it reacts to things.

156
00:10:53,000 --> 00:10:59,000
Being less anxious, less inclined to react with anxiety, you're going to worry about things.

157
00:10:59,000 --> 00:11:02,000
Because you see anxiety is useless, it's harmful.

158
00:11:02,000 --> 00:11:07,000
You really see it, like normally we think of it, I know it's useless, I want to get rid of it.

159
00:11:07,000 --> 00:11:09,000
It's not the same.

160
00:11:09,000 --> 00:11:13,000
Still have this aspect of our mind that thinks that it's good.

161
00:11:13,000 --> 00:11:18,000
So you change that by learning that it's not good.

162
00:11:18,000 --> 00:11:34,000
And then just a continuation of the earlier question about things being chaotic.

163
00:11:34,000 --> 00:11:38,000
I kind of meant that the environment is chaotic from the beginning.

164
00:11:38,000 --> 00:11:47,000
Can the meditate or ever really have mindful moments?

165
00:11:47,000 --> 00:11:48,000
Absolutely.

166
00:11:48,000 --> 00:11:50,000
You have to understand what mindfulness is.

167
00:11:50,000 --> 00:11:53,000
Even if the environment is chaotic, what does that mean?

168
00:11:53,000 --> 00:11:54,000
There is sound.

169
00:11:54,000 --> 00:11:57,000
If there's sound, then you say hearing, hearing, then you're mindful.

170
00:11:57,000 --> 00:12:01,000
What does it mean to say the environment is chaotic?

171
00:12:01,000 --> 00:12:05,000
If someone's hitting you, then you say pain.

172
00:12:05,000 --> 00:12:07,000
Usually that would mean sound, right?

173
00:12:07,000 --> 00:12:08,000
The environment is chaotic.

174
00:12:08,000 --> 00:12:11,000
It just means there's sound arising at the air while sound is just sound.

175
00:12:11,000 --> 00:12:17,000
Say hearing.

176
00:12:17,000 --> 00:12:21,000
So even if your entire meditation was noting hearing, that would be okay.

177
00:12:21,000 --> 00:12:23,000
Sound arises and ceases.

178
00:12:23,000 --> 00:12:27,000
Did I tell you the story of what was watching someone and they were saying,

179
00:12:27,000 --> 00:12:31,000
they were teaching meditation and they said, when you see, you say seeing,

180
00:12:31,000 --> 00:12:33,000
when you hear, say hearing.

181
00:12:33,000 --> 00:12:36,000
And as they said that, they were clearly following their own advice.

182
00:12:36,000 --> 00:12:40,000
And as they said, you say hearing, hearing.

183
00:12:40,000 --> 00:12:49,000
And they literally did this.

184
00:12:49,000 --> 00:12:52,000
And they stayed like that for a minute or two.

185
00:12:52,000 --> 00:12:55,000
And the meditative was confused.

186
00:12:55,000 --> 00:13:01,000
But I was sitting watching this happen in front of me.

187
00:13:01,000 --> 00:13:03,000
Incredible.

188
00:13:03,000 --> 00:13:07,000
It means you can become enlightened through sound.

189
00:13:07,000 --> 00:13:16,000
If your mind is clear enough.

190
00:13:16,000 --> 00:13:17,000
Hey, Bhante.

191
00:13:17,000 --> 00:13:21,000
My area is very dominated by fundamentalist Christians who are very judgmental

192
00:13:21,000 --> 00:13:25,000
of anyone who doesn't follow their particular religion.

193
00:13:25,000 --> 00:13:28,000
What would be the Buddhist way to approach this?

194
00:13:28,000 --> 00:13:31,000
I'm not out spreading Buddhism on the street corners or making myself known.

195
00:13:31,000 --> 00:13:35,000
But I do not want to draw unwanted attention to myself or be an

196
00:13:35,000 --> 00:13:45,000
object of discrimination.

197
00:13:45,000 --> 00:13:48,000
I don't, I don't, I don't, that thing gets me right away.

198
00:13:48,000 --> 00:13:51,000
I mean, be mindful.

199
00:13:51,000 --> 00:13:56,000
There's a story of a monk who came to the Buddha.

200
00:13:56,000 --> 00:13:59,000
I think he was an Arahan.

201
00:13:59,000 --> 00:14:00,000
What was that?

202
00:14:00,000 --> 00:14:02,000
But he came to the Buddha.

203
00:14:02,000 --> 00:14:06,000
And he said to the Buddha, he was going back to his homeland,

204
00:14:06,000 --> 00:14:09,000
or he was going to stay in a certain land.

205
00:14:09,000 --> 00:14:13,000
And the Buddha said, oh, well, those people are really harsh.

206
00:14:13,000 --> 00:14:15,000
They're not very friendly.

207
00:14:15,000 --> 00:14:17,000
He said, what if they say nasty things to you?

208
00:14:17,000 --> 00:14:19,000
What will you do?

209
00:14:19,000 --> 00:14:22,000
I don't know, well, sir, if they say nasty things to me,

210
00:14:22,000 --> 00:14:26,000
I'll just think to myself, oh, these people are so civilized

211
00:14:26,000 --> 00:14:29,000
that they don't hit me.

212
00:14:29,000 --> 00:14:32,000
What if they hit you?

213
00:14:32,000 --> 00:14:35,000
They said, well, they're not just think these people are so civilized

214
00:14:35,000 --> 00:14:38,000
that they don't break my bones.

215
00:14:38,000 --> 00:14:40,000
Or cause real harm.

216
00:14:40,000 --> 00:14:44,000
They said, well, what if they cause real harm?

217
00:14:44,000 --> 00:14:46,000
And then he said, well, if they do, I'll just think

218
00:14:46,000 --> 00:14:49,000
well, these people are civilized, so civilized,

219
00:14:49,000 --> 00:14:52,000
that they don't kill me.

220
00:14:52,000 --> 00:14:55,000
So what if they kill you?

221
00:14:55,000 --> 00:15:00,000
And he said, if they kill me, then I'll just think to myself,

222
00:15:00,000 --> 00:15:06,000
many Buddhist, many, many, many of the beings who have sought

223
00:15:06,000 --> 00:15:08,000
for an assassin.

224
00:15:08,000 --> 00:15:09,000
It's kind of weird.

225
00:15:09,000 --> 00:15:12,000
I think you have to be an arrowhead to really understand.

226
00:15:12,000 --> 00:15:13,000
Well, not even an arrowhead.

227
00:15:13,000 --> 00:15:17,000
You have to understand the idea of an arrowhead to get this.

228
00:15:17,000 --> 00:15:23,000
He said, many, many are the enlightened beings,

229
00:15:23,000 --> 00:15:28,000
as many followers of the Buddha have wished for an assassin,

230
00:15:28,000 --> 00:15:32,000
someone to end their life.

231
00:15:32,000 --> 00:15:34,000
I don't know if wished is exactly the right word,

232
00:15:34,000 --> 00:15:39,000
but what he's trying to say is that there's no problem with

233
00:15:39,000 --> 00:15:40,000
death.

234
00:15:40,000 --> 00:15:43,000
Death isn't a problem because life is,

235
00:15:43,000 --> 00:15:46,000
there's no meaning anymore for a person who's enlightened.

236
00:15:46,000 --> 00:15:49,000
They're free from attachment.

237
00:15:49,000 --> 00:15:52,000
And meaning he has no fear of death.

238
00:15:52,000 --> 00:15:53,000
It wasn't a problem for him.

239
00:15:53,000 --> 00:15:54,000
He was like, okay.

240
00:15:54,000 --> 00:15:57,000
In that case, you're ready to go and live with these people.

241
00:15:57,000 --> 00:15:58,000
And he did.

242
00:15:58,000 --> 00:16:00,000
And he actually established the Sangha there.

243
00:16:00,000 --> 00:16:02,000
That's what the Buddha ends by saying.

244
00:16:02,000 --> 00:16:05,000
He went and lived there and he established the Sangha people

245
00:16:05,000 --> 00:16:08,000
meditating to be free from suffering.

246
00:16:08,000 --> 00:16:12,000
And the place was full of barbarians.

247
00:16:12,000 --> 00:16:17,000
So I think some of these places are kind of barbaric.

248
00:16:17,000 --> 00:16:21,000
People are probably not surrounded by barbarians,

249
00:16:21,000 --> 00:16:22,000
but barbarians.

250
00:16:22,000 --> 00:16:26,000
But as I'm extent, you know, religious fundamentalists can be

251
00:16:26,000 --> 00:16:31,000
pretty barbaric.

252
00:16:31,000 --> 00:16:33,000
Very judgmental.

253
00:16:33,000 --> 00:16:35,000
So you just laugh it off.

254
00:16:35,000 --> 00:16:40,000
You have to learn to be mindful and let it be like water off of

255
00:16:40,000 --> 00:16:42,000
a lotus leaf.

256
00:16:42,000 --> 00:16:45,000
It's like a lotus doesn't get wet.

257
00:16:45,000 --> 00:16:49,000
Water, raindrops.

258
00:16:49,000 --> 00:16:54,000
They just fall off.

259
00:16:54,000 --> 00:16:57,000
Like when do we say in English water off and duck's back?

260
00:16:57,000 --> 00:16:58,000
It's not what we say.

261
00:16:58,000 --> 00:16:59,000
You say something like that?

262
00:16:59,000 --> 00:17:00,000
Yes.

263
00:17:00,000 --> 00:17:09,000
That's the way to deal with Christians.

264
00:17:09,000 --> 00:17:10,000
I've talked about this before.

265
00:17:10,000 --> 00:17:12,000
Religious people is to ask them about their religion.

266
00:17:12,000 --> 00:17:13,000
Learn about it.

267
00:17:13,000 --> 00:17:15,000
Take energy though.

268
00:17:15,000 --> 00:17:18,000
I think after a while you're probably a tired thing.

269
00:17:18,000 --> 00:17:22,000
But if they approach you, it is often the best way just to just ask them.

270
00:17:22,000 --> 00:17:25,000
Or else just be silent, I suppose, you know.

271
00:17:25,000 --> 00:17:27,000
You really just don't want to talk.

272
00:17:27,000 --> 00:17:29,000
Just don't say anything.

273
00:17:29,000 --> 00:17:34,000
But if you talk to them, if you ask them questions, they end up

274
00:17:34,000 --> 00:17:40,000
tying their own news, hanging themselves,

275
00:17:40,000 --> 00:17:44,000
meaning they end up damning themselves.

276
00:17:44,000 --> 00:17:48,000
And contradicting themselves because these religions are not very

277
00:17:48,000 --> 00:17:52,000
solid, not very well-founded on reality.

278
00:17:52,000 --> 00:17:57,000
It's by being unchallenged and unquestioned.

279
00:17:57,000 --> 00:18:05,000
Not challenged, but not being forced to

280
00:18:05,000 --> 00:18:09,000
to back up their claims in that country.

281
00:18:09,000 --> 00:18:10,000
But they continue that.

282
00:18:10,000 --> 00:18:12,000
They're able to stay strong.

283
00:18:12,000 --> 00:18:21,000
So there's that.

284
00:18:21,000 --> 00:18:26,000
And to follow up on the question about concentration in a

285
00:18:26,000 --> 00:18:28,000
chaotic environment.

286
00:18:28,000 --> 00:18:32,000
But if there is absolutely no concentration, can someone

287
00:18:32,000 --> 00:18:36,000
without any prior experience in meditation just decide to be mindful

288
00:18:36,000 --> 00:18:39,000
now in a chaotic situation and have benefit?

289
00:18:39,000 --> 00:18:42,000
I'm not worried about concentration or concentration or

290
00:18:42,000 --> 00:18:45,000
rises through mindfulness.

291
00:18:45,000 --> 00:18:47,000
Yeah, you start being mindful in one moment.

292
00:18:47,000 --> 00:18:51,000
The person who's new to it, it's just not very good at creating

293
00:18:51,000 --> 00:18:53,000
mindfulness.

294
00:18:53,000 --> 00:18:56,000
Yeah, no, maybe you're right.

295
00:18:56,000 --> 00:19:01,000
It is a point that they're probably just going to get distracted

296
00:19:01,000 --> 00:19:04,000
and frustrated and so on.

297
00:19:04,000 --> 00:19:08,000
But I think that's more of a view than anything, you know?

298
00:19:08,000 --> 00:19:13,000
If you have the view that, oh, this is, I can't meditate,

299
00:19:13,000 --> 00:19:15,000
this is too chaotic.

300
00:19:15,000 --> 00:19:16,000
And that's the problem.

301
00:19:16,000 --> 00:19:19,000
So if someone, you know, like I've explained, and as Simon,

302
00:19:19,000 --> 00:19:25,000
I think added in something useful there.

303
00:19:25,000 --> 00:19:28,000
If you can see it, just as experience.

304
00:19:28,000 --> 00:19:31,000
Then it's not very difficult to be mindful.

305
00:19:31,000 --> 00:19:33,000
And it's in fact quite rewarding.

306
00:19:33,000 --> 00:19:35,000
But again, it is more challenging.

307
00:19:35,000 --> 00:19:39,000
It's much easier to have a quiet place to meditate with one

308
00:19:39,000 --> 00:19:41,000
thing to be mindful of.

309
00:19:41,000 --> 00:19:45,000
It's a very challenging.

310
00:19:45,000 --> 00:19:47,000
So you had a choice, Dante.

311
00:19:47,000 --> 00:19:50,000
Say the house is noisy, but the backyard is quiet.

312
00:19:50,000 --> 00:19:58,000
Is there anything inherently wrong with just seeking out the quiet spot?

313
00:19:58,000 --> 00:20:03,000
Well, it makes you, it is indulging a preference, right?

314
00:20:03,000 --> 00:20:05,000
It's potentially.

315
00:20:05,000 --> 00:20:09,000
There are other reasons I suppose.

316
00:20:09,000 --> 00:20:15,000
It could be endorsing quiet in the sense that, you know,

317
00:20:15,000 --> 00:20:20,000
the reasons why it's loud could be that people are engaging in entertainment

318
00:20:20,000 --> 00:20:25,000
or talking, yacking, chatting, that kind of thing.

319
00:20:25,000 --> 00:20:32,000
But if you're preference, if you're not able to deal with the noise,

320
00:20:32,000 --> 00:20:41,000
if it's just a matter of, like, music or, I don't know.

321
00:20:41,000 --> 00:20:45,000
I mean, certain noises that are difficult, like music is probably difficult.

322
00:20:45,000 --> 00:20:49,000
But definitely talking is difficult.

323
00:20:49,000 --> 00:20:57,000
Difficult why, because your mind gets naturally involved in that.

324
00:20:57,000 --> 00:20:59,000
It's really distracting.

325
00:20:59,000 --> 00:21:01,000
It's really difficult to stay on distracting.

326
00:21:01,000 --> 00:21:04,000
These are minds are receptive of that.

327
00:21:04,000 --> 00:21:06,000
But just loud noise.

328
00:21:06,000 --> 00:21:13,000
It's like, let's say, a big one where I were in Stony Creek was the car noises.

329
00:21:13,000 --> 00:21:15,000
And that is just noise.

330
00:21:15,000 --> 00:21:18,000
And it's interesting because we don't make it irritated by it

331
00:21:18,000 --> 00:21:21,000
and distracted and it interrupts our concentration.

332
00:21:21,000 --> 00:21:24,000
But that's kind of a good thing.

333
00:21:24,000 --> 00:21:30,000
Let's see how you react to things.

334
00:21:30,000 --> 00:21:32,000
I think there's definitely a potential problem.

335
00:21:32,000 --> 00:21:38,000
Like, if in that case, you went and sought out a quieter spot.

336
00:21:38,000 --> 00:21:41,000
You're only going to hurt yourself in the end.

337
00:21:41,000 --> 00:21:44,000
You're going to be more of averse to noise.

338
00:21:44,000 --> 00:21:47,000
Like, when I had this alarm clock ticking.

339
00:21:47,000 --> 00:21:51,000
And I had to, I got more and more fed up.

340
00:21:51,000 --> 00:21:55,000
And finally, I just had to suppress the sound.

341
00:21:55,000 --> 00:22:00,000
And then when I got moved to another place that had two clocks on the wall

342
00:22:00,000 --> 00:22:03,000
and this ticking, it was in, it was insane.

343
00:22:03,000 --> 00:22:07,000
And then I realized, you know, this isn't the way I have to go another way.

344
00:22:07,000 --> 00:22:09,000
I learned just the blindfold.

345
00:22:09,000 --> 00:22:12,000
Like, it was actually affecting my walk and lifting.

346
00:22:12,000 --> 00:22:15,000
The thing, I think.

347
00:22:15,000 --> 00:22:18,000
Because I couldn't get out of sync with the tick.

348
00:22:18,000 --> 00:22:21,000
Until finally, I just said, okay, if I'm going to be in sync, I'm going to be in sync.

349
00:22:21,000 --> 00:22:24,000
And I have to learn to let go of this.

350
00:22:24,000 --> 00:22:25,000
I did.

351
00:22:25,000 --> 00:22:27,000
It was really quite rewarding.

352
00:22:39,000 --> 00:22:44,000
And I think with that, we're all caught up with questions.

353
00:22:44,000 --> 00:22:45,000
Okay.

354
00:22:45,000 --> 00:22:47,000
Well done.

355
00:22:47,000 --> 00:22:48,000
Good night, everyone.

356
00:22:48,000 --> 00:22:50,000
Thank you, Robin, for your help.

357
00:22:50,000 --> 00:22:53,000
It's everyone for your questions and for watching.

358
00:22:53,000 --> 00:22:55,000
Getting involved.

359
00:22:55,000 --> 00:22:57,000
I don't know, I'm going to plan it up soon.

360
00:22:57,000 --> 00:22:59,000
Thank you, Dante.

361
00:22:59,000 --> 00:23:00,000
Yeah.

362
00:23:00,000 --> 00:23:22,000
Thank you, Dante.

363
00:23:22,000 --> 00:23:31,000
Yeah.

