1
00:00:00,000 --> 00:00:05,000
How does one truly know that one has established a refuge in the Buddha?

2
00:00:05,000 --> 00:00:10,000
Is this done by seeing our remaining defilements and working on them accordingly?

3
00:00:10,000 --> 00:00:14,000
I was thinking of the story about Tutchapotila,

4
00:00:14,000 --> 00:00:18,000
where he came to realize that he had no refuge in afterwards he became enlightened.

5
00:00:18,000 --> 00:00:24,000
Should we see our remaining defilements and the lessening of these as our refuge? Thanks.

6
00:00:24,000 --> 00:00:34,000
Now, the true taking of refuge is the attainment of Sotapana.

7
00:00:34,000 --> 00:00:41,000
A person who attains Sotapana is said to have true refuge,

8
00:00:41,000 --> 00:00:45,000
because at that point they have full confidence in the Buddha and his teachings,

9
00:00:45,000 --> 00:00:49,000
and those who practice his teachings.

10
00:00:49,000 --> 00:00:54,000
They know that the Buddha was enlightened in the sense that the Buddha,

11
00:00:54,000 --> 00:00:57,000
I talked about this yesterday in the study group,

12
00:00:57,000 --> 00:01:01,000
how they know that the Buddha was enlightened,

13
00:01:01,000 --> 00:01:06,000
because they have realized that what the Buddha said was true.

14
00:01:06,000 --> 00:01:09,000
They realize Nibana, so they can verify, yes,

15
00:01:09,000 --> 00:01:12,000
Buddha was talking about something that I've actually realized.

16
00:01:12,000 --> 00:01:16,000
That realization is the most profound realization possible.

17
00:01:16,000 --> 00:01:19,000
It's cessation of suffering.

18
00:01:19,000 --> 00:01:22,000
Therefore, they see either the Buddha got it from somewhere,

19
00:01:22,000 --> 00:01:24,000
or he was enlightened himself.

20
00:01:24,000 --> 00:01:26,000
Could be that he had a tape recorder,

21
00:01:26,000 --> 00:01:30,000
or he had something in his ear and he was being fed it.

22
00:01:30,000 --> 00:01:36,000
But barring that, he was enlightened and knew what he was talking about.

23
00:01:36,000 --> 00:01:41,000
Because only an enlightened being would be able to explain such a thing.

24
00:01:41,000 --> 00:01:45,000
Your full confidence in the Dhamma, because you have experienced it for yourself.

25
00:01:45,000 --> 00:01:48,000
You know that it truly is the cessation of suffering.

26
00:01:48,000 --> 00:01:52,000
And you have full confidence in the Sangha.

27
00:01:52,000 --> 00:01:56,000
Because you know that if you hear of someone practicing in this way,

28
00:01:56,000 --> 00:01:59,000
you know that they are leading themselves closer to Nibana.

29
00:01:59,000 --> 00:02:02,000
If you know that someone has a Tain Nibana,

30
00:02:02,000 --> 00:02:05,000
then you know that they are enlightened.

31
00:02:05,000 --> 00:02:09,000
Because you know that the attainment of Nibana leads to enlightenment.

32
00:02:09,000 --> 00:02:12,000
For example, this comes with Sota Panda.

33
00:02:12,000 --> 00:02:18,000
What I say of taking refuge is to reach that level.

34
00:02:18,000 --> 00:02:25,000
Apart from that, the idea of taking refuge in a conventional sense

35
00:02:25,000 --> 00:02:30,000
just means that you accept the Buddha as your guide.

36
00:02:30,000 --> 00:02:33,000
And of course, it's still provisional,

37
00:02:33,000 --> 00:02:37,000
because you don't know whether the Buddha was correct or not.

38
00:02:37,000 --> 00:02:41,000
And you may pretend that your Buddhist and that you've taken the Buddha as your refuge.

39
00:02:41,000 --> 00:02:45,000
It's still only provisional, and you might still, as many Buddhists do,

40
00:02:45,000 --> 00:02:48,000
do many things that are contrary to the Buddha's teaching.

41
00:02:48,000 --> 00:02:51,000
So you can't really be said to have taken refuge,

42
00:02:51,000 --> 00:02:53,000
but except in the conventional sense,

43
00:02:53,000 --> 00:02:56,000
where you claim that the Buddha is your guide,

44
00:02:56,000 --> 00:03:00,000
and you intend to practice according to his teachings.

45
00:03:00,000 --> 00:03:07,000
And you therefore take his followers as your teachers,

46
00:03:07,000 --> 00:03:11,000
or you accept that a person who practices in this way is practicing in the right way.

47
00:03:11,000 --> 00:03:15,000
But it's all provisional, but it's done in the conventional sense.

48
00:03:15,000 --> 00:03:19,000
But that's just a determination that you make on your own,

49
00:03:19,000 --> 00:03:23,000
that you want to practice according to the Buddha's teaching.

50
00:03:23,000 --> 00:03:26,000
And that's what all meditators do, really.

51
00:03:26,000 --> 00:03:29,000
You may not do this chanting that we do,

52
00:03:29,000 --> 00:03:32,000
but by undertaking the Buddha's teaching,

53
00:03:32,000 --> 00:03:35,000
you're taking refuge in the sense that I just said,

54
00:03:35,000 --> 00:03:38,000
you're accepting that the Buddha has something to teach,

55
00:03:38,000 --> 00:03:40,000
at least, traditionally.

56
00:03:40,000 --> 00:03:43,000
And you're practicing it, putting it into practice,

57
00:03:43,000 --> 00:03:45,000
hopefully, wholeheartedly.

58
00:03:45,000 --> 00:04:12,000
That's what it means, as I understand, to take refuge.

