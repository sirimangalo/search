1
00:00:00,000 --> 00:00:08,560
the Burmese translation also I don't know it is called a new in Burmese but I

2
00:00:08,560 --> 00:00:15,240
don't know what that is so this is two pieces of flesh with a single ligature as

3
00:00:15,240 --> 00:00:22,120
to color it is dull red the color of barley but it can see as to shape it is a

4
00:00:22,120 --> 00:00:26,760
shape of a pair of giant spray balls or it is the shape of a pair of mango

5
00:00:26,760 --> 00:00:32,560
fruits attached to a single stock so they look like kidneys right

6
00:00:32,560 --> 00:00:39,200
the description of kidneys as to direction now it lies in the upper direction

7
00:00:39,200 --> 00:00:45,880
that is what what's boring me upper direction means above navel

8
00:00:45,880 --> 00:01:00,520
ah what are they located yes yes I think they are then you see upper

9
00:01:00,520 --> 00:01:08,280
but but please go on

10
00:01:08,280 --> 00:01:14,240
now please go on or it is a shape of a pair of mango fruits attached to a single

11
00:01:14,240 --> 00:01:19,320
stock as to direction it lies in the upper direction that's to location it is

12
00:01:19,320 --> 00:01:25,880
to be found on either side of the heart flesh but heart is here and kidneys are

13
00:01:25,880 --> 00:01:35,320
down there right yes but when you do echo gram or when you do cat skin you

14
00:01:35,320 --> 00:01:41,640
can see it real location because of the nature of the diagram being raised at

15
00:01:41,640 --> 00:01:49,760
the bank the diagram is not flat and diagram is in coverage and because of the

16
00:01:49,760 --> 00:01:59,400
liver and because of the kidney and because of the exchange extension

17
00:01:59,400 --> 00:02:10,720
and the expansion nature of the diagram is in such a nature that when you take

18
00:02:10,720 --> 00:02:20,160
the echo gram or cat skin or x-ray special technique x-rays the kidneys are on

19
00:02:20,160 --> 00:02:27,400
the sides of the heart so it is to be found on either side of the heart being

20
00:02:27,400 --> 00:02:31,920
fussened by the substance that starts out with one root from the base of the

21
00:02:31,920 --> 00:02:40,720
neck and divides into a tool after going a short way so the translation

22
00:02:40,720 --> 00:02:47,080
kidneys is acceptable right then I think it's the other the heart is also not

23
00:02:47,080 --> 00:02:56,200
stationary position you know the heart is pumping when it is contracted it's

24
00:02:56,200 --> 00:03:03,240
shortened and it is above the level of the kidney when it is expanded receiving

25
00:03:03,240 --> 00:03:11,240
the blood in flow then the heart is enlarged and pushed towards at that time the

26
00:03:11,240 --> 00:03:20,440
kidneys are on the sides of the heart so it is dynamic I see then the

27
00:03:20,440 --> 00:03:28,280
distribution is very scientific I wonder how they knew this you know

28
00:03:28,280 --> 00:03:34,720
if you cut cut I mean dead body you don't see this no if you yeah when you cut

29
00:03:34,720 --> 00:03:43,480
the dead body you can't see you cannot see it like like this that's why only in

30
00:03:43,480 --> 00:03:56,160
the living nature maybe they knew they knew with the help of supernorets

31
00:03:56,160 --> 00:04:07,880
then the next one is heart and in Buddhist Abirama the heart is said to be

32
00:04:07,880 --> 00:04:13,240
the seed of many types of consciousness and so about the middle of the

33
00:04:13,240 --> 00:04:20,840
paragraph there is a inside it there is a hollow the size of a bone that seeds

34
00:04:20,840 --> 00:04:28,200
pet where half a passata passata means handful so half a handful of blood

35
00:04:28,200 --> 00:04:34,560
half a passata measure of blood is kept with which as they are support the

36
00:04:34,560 --> 00:04:44,280
mind element and mind consciousness element or car that means there are 80

37
00:04:44,280 --> 00:05:01,920
89 types of consciousness and out of 89 10 have their own location or their

38
00:05:01,920 --> 00:05:11,320
own base for arising like two types of seeing consciousness have the eyes as

39
00:05:11,320 --> 00:05:17,800
their base and then two types of hearing consciousness has the ear as their base

40
00:05:17,800 --> 00:05:24,120
and so on with with five senses so the other types of the other types of

41
00:05:24,120 --> 00:05:37,000
consciousness all of them always all of them depend on the heart so here

42
00:05:37,000 --> 00:05:42,400
mind element and mind consciousness element means times of consciousness

43
00:05:42,400 --> 00:05:49,920
other than the 10 other than the 10 seeing hearing smelling tasting and

44
00:05:49,920 --> 00:05:57,200
touching to to to to each of seeing and so on so the heart or actually the

45
00:05:57,200 --> 00:06:04,320
blood and the heart is the seed of consciousness according to Abirama that in

46
00:06:04,320 --> 00:06:09,760
one of greedy temperament the blood is red that in one of hitting temperament

47
00:06:09,760 --> 00:06:16,520
is black that in one of diluted temperament is like water water that meat has been

48
00:06:16,520 --> 00:06:25,040
washed in that in one of speculative temperament is like lentil soup in color

49
00:06:25,040 --> 00:06:31,720
that in one of faithful temperament it's a color of yellow canigara flower that

50
00:06:31,720 --> 00:06:37,280
in one of understanding temperament or a wise man is limited clear and

51
00:06:37,280 --> 00:06:44,440
turbulent bright pure by the worst gem of pure water and it seems to shine so

52
00:06:44,440 --> 00:06:53,880
this is the heart and the next one is liver and about four lines in sluggish

53
00:06:53,880 --> 00:06:59,600
people it is single and large sluggish or what is sluggish

54
00:06:59,600 --> 00:07:14,960
lazy people slow slow slow slow so in what slow in understanding or slow in

55
00:07:14,960 --> 00:07:22,040
physical movement if it if it means slow in understanding it is it is

56
00:07:22,040 --> 00:07:29,400
correct here it is it is mentioned in contrast to those possessed of understanding

57
00:07:29,400 --> 00:07:35,320
that means who are not so intelligent and who are intelligent so yeah sluggish

58
00:07:35,320 --> 00:07:46,840
means who are not intelligent who who or maybe tell good that something so

59
00:07:46,840 --> 00:07:52,160
that is liver and then the next one also will have difficulty mid drip what is

60
00:07:52,160 --> 00:08:03,640
mid drip I look them up in the dictionary so mid-revis they define as the part

61
00:08:03,640 --> 00:08:10,280
of the body the diaphragm I think that will do the other meaning is the middle

62
00:08:10,280 --> 00:08:14,080
out of portion of the front of the human body extending roughly from just below

63
00:08:14,080 --> 00:08:21,680
the breast to the waistline and it's just out of it so in in bodies it is

64
00:08:21,680 --> 00:08:28,960
translated as just a membrane and this is a covering of the flesh which is of

65
00:08:28,960 --> 00:08:35,440
two kinds namely the conceals and the unconceal as to color both of both kinds

66
00:08:35,440 --> 00:08:40,760
of white color of two oula racks as to shape it is a shape of its location as

67
00:08:40,760 --> 00:08:46,040
to direction the conceals mid-revis in the upper direction the other in both

68
00:08:46,040 --> 00:08:51,040
directions as to location the conceals mid-revis to be found consuming the

69
00:08:51,040 --> 00:08:58,240
heart and kidney so it looks like diaphragm right and the unconceal is to be

70
00:08:58,240 --> 00:09:03,960
found covering the flesh under the inner skin throughout the whole body so

71
00:09:03,960 --> 00:09:16,920
between the flesh and the skin this this thing is located and every unconceal

72
00:09:16,920 --> 00:09:21,640
is to be found covering the flesh under the inner skin throughout the whole

73
00:09:21,640 --> 00:09:29,640
body that is not diaphragm and in this case it is not diaphragm so me is

74
00:09:29,640 --> 00:09:38,000
in bodies this better so some some membrane under the

75
00:09:38,000 --> 00:09:46,800
needle skin yeah and in a nanopony girl translates this as flura PLEURA

76
00:09:46,800 --> 00:09:55,160
and it is a covering of the lens either of two membrane effects each of which

77
00:09:55,160 --> 00:10:00,440
lines one side of the thoracic cavity and envelopes the conductuous

78
00:10:00,440 --> 00:10:05,640
learn reducing the friction of respiratory movements to a minimum so it

79
00:10:05,640 --> 00:10:16,360
means also be in the upper part of the body so by location the

80
00:10:16,360 --> 00:10:28,840
concealed mid-revis could be diaphragm the unconceal must take another

81
00:10:28,840 --> 00:10:35,800
make and then the next one is fleeing this is the flesh of the very

82
00:10:35,800 --> 00:10:44,480
tongue as the color it is blue the color of the gong tree flowers and so on and then

83
00:10:44,480 --> 00:10:55,400
the next one lights yeah now the lines is defined as the lungs especially of an

84
00:10:55,400 --> 00:11:04,080
animal used for food so I think it's better to translate this as lungs rather than

85
00:11:04,080 --> 00:11:19,400
lights it's an obscure what lights I don't know if it means not so I didn't know the

86
00:11:19,400 --> 00:11:25,600
flesh of the light is divided with two or three pieces of flesh as to

87
00:11:25,600 --> 00:11:36,960
color it is red and so on and then the next one bowel this is the bowel tube it is

88
00:11:36,960 --> 00:11:43,720
looked in 21 flavors and in a man it is 32 hands long and in a woman 28 hands

89
00:11:43,720 --> 00:11:51,920
how long is the it's the intestine so what in feet it is 32 feet that you do

90
00:11:51,920 --> 00:12:01,880
feet oh there's two hands 32 hands mean yeah that need to long a longer than 32

91
00:12:01,880 --> 00:12:11,800
feet but it made me feel I don't know and it's the difference between a man

92
00:12:11,800 --> 00:12:17,520
intestine in a woman's intestine difference in length

93
00:12:25,520 --> 00:12:36,760
there is a saying in Bummies so this excuse me seems it seems women's

94
00:12:36,760 --> 00:12:45,480
intestine a shorter the women are more jealous or something

95
00:12:46,480 --> 00:12:55,480
it'd be not be true but it is a saying so women according to this women have

96
00:12:55,480 --> 00:13:08,680
shorter intestines than man it may not be so but the next one the next one and

97
00:13:08,680 --> 00:13:14,760
three or messenger three let us read first this is the first name in the

98
00:13:14,760 --> 00:13:21,600
places where the bowels quiles as to color it is white the color of the kasi

99
00:13:21,600 --> 00:13:26,880
taliga quite edible water lily roots as to shape it as a shape of those roots too

100
00:13:26,880 --> 00:13:31,880
and as to direction it lies in the two directions as to location needed to be

101
00:13:31,880 --> 00:13:37,200
found inside the 21 quiles of the ball like the strings to be found inside

102
00:13:37,200 --> 00:13:42,240
bro greens for wiping the feet on sewing them together and it presents the

103
00:13:42,240 --> 00:13:48,000
bowels quiles together so that they do not slip down in those walking with

104
00:13:48,000 --> 00:13:53,640
those exits etc as the marionist strings to the marionist wooden limbs at the

105
00:13:53,640 --> 00:14:01,640
time of the marionist being pulled along so

106
00:14:01,640 --> 00:14:19,720
bowel means wood large and small intestines right but this one and three or

107
00:14:19,720 --> 00:14:26,280
messenger which is better messenger is better right so and I read the

108
00:14:26,280 --> 00:14:30,300
description of entrees tend to use the internal organs especially the

109
00:14:30,300 --> 00:14:37,760
intestines but messenger is defined as any of several peritoneal folds that

110
00:14:37,760 --> 00:14:42,840
connect the intestines to be dosa abdominal wall so that looks like

111
00:14:42,840 --> 00:14:53,880
messenger yeah messenger is better and then gorge that is food eaten in the stomach

112
00:14:53,880 --> 00:14:58,560
so that is what has been eaten brown chute and tasted and is been done in the

113
00:14:58,560 --> 00:15:04,880
stomach and then there's a description of stomach what is called the stomach is a

114
00:15:04,880 --> 00:15:10,080
part of the ball membrane which is like the swelling of air produced in the middle

115
00:15:10,080 --> 00:15:14,640
of the length of wet cloth when it is being twisted and run out from the two ends

116
00:15:14,640 --> 00:15:19,960
it is smooth outside inside it is like a balloon of cloth swires by wrapping up

117
00:15:19,960 --> 00:15:29,200
meat ratios now here in vermies it is translated as not like the balloon of the cloth

118
00:15:29,200 --> 00:15:39,520
but like like a tessal of the cloth I asked you last night and it's a Sarah

119
00:15:40,120 --> 00:15:46,080
so not not a you know the inside of the stomach is something like a tower

120
00:15:46,080 --> 00:15:51,400
the surface of the tower is a little run and then something something hanging

121
00:15:51,400 --> 00:15:59,900
down so it is like like a tessal inside of the stomach so not the balloon of

122
00:15:59,900 --> 00:16:07,560
cloth the nyanamori took that to mean balloon that the pali was pukkaka pukkaka

123
00:16:07,560 --> 00:16:19,480
really means something that resembles a flower so how was those things and I

124
00:16:19,480 --> 00:16:25,000
didn't know that the name of these things I asked Sarah last night and she said

125
00:16:25,000 --> 00:16:31,120
it could be called tessal so I looked it I looked it up the dictionary and I think

126
00:16:31,120 --> 00:16:43,840
that is that is correct you know in vermies are in you know dangerous people

127
00:16:43,840 --> 00:16:50,560
people eat meat and the stomach is also eaten and stomach is called in

128
00:16:50,560 --> 00:16:58,000
in Burma when it is cooked or when it is prepared for food it's called rough cloth

129
00:16:58,000 --> 00:17:09,760
in Burmies it is called persuasion rough cloth so it is rough inside and if you

130
00:17:09,760 --> 00:17:17,320
have seen some some some of these things you you know that

131
00:17:17,320 --> 00:17:36,920
yeah okay this is caught and then the next one is damp so it is

132
00:17:36,920 --> 00:17:42,640
excrement and they are also called the parts of the body actually they are not

133
00:17:42,640 --> 00:17:49,360
part of the body right they are refused but he is of the body and the next one is

134
00:17:49,360 --> 00:17:57,040
brain since brain is not supposed to be the seat of consciousness the

135
00:17:57,040 --> 00:18:05,360
description of brain is very very short here so it is like full lumps of

136
00:18:05,360 --> 00:18:13,720
to put together to correspond with the skulls full suture sections of yes

137
00:18:13,720 --> 00:18:19,200
they are they are the suture together like and then by there are two kinds of

138
00:18:19,200 --> 00:18:27,800
by local by or stationary by and free by that is moving by here and as to

139
00:18:27,800 --> 00:18:33,200
go at the local by is a color of thick material or the free by is a color of

140
00:18:33,200 --> 00:18:38,280
failure are cooling flowers and so on so when there are two kinds of

141
00:18:38,280 --> 00:18:46,160
bars and when one is disturbed I mean the just to

142
00:18:46,160 --> 00:18:51,040
negation the free by spreads like a drop of oil on water all over the body

143
00:18:51,040 --> 00:18:56,680
except for the fresh less fleshless parts of the head hairs body hairs

144
00:18:56,680 --> 00:19:02,440
teeth nails and the hard dry skin when it is disturbed the eyes become yellow

145
00:19:02,440 --> 00:19:09,760
and twitch and there is shivering and itching of the body that is when when the

146
00:19:09,760 --> 00:19:16,920
free by is disturbed when when the free by is not in not in good order the

147
00:19:16,920 --> 00:19:21,800
local by is situated near the flesh of the liver that is we are by liver

148
00:19:21,800 --> 00:19:25,800
between the heart and the luts it is to be found in the bio container called

149
00:19:25,800 --> 00:19:33,720
liver which is like a large cause at it is look for crown it crown it when it is

150
00:19:33,720 --> 00:19:40,040
disturbed being being too crazy and demanded the true of consciousness and

151
00:19:40,040 --> 00:19:45,400
conscience and shame and do the undoable speak the unsciguous and think the

152
00:19:45,400 --> 00:19:52,080
unthinkable when people go mad you say oh his bio is disturbed and

153
00:19:52,080 --> 00:20:03,200
something like that and the next one flam and it is described as

154
00:20:03,200 --> 00:20:10,600
you get it in this stomach over the you are what what we have eaten and then

155
00:20:10,600 --> 00:20:15,400
pus pus is produced by decaying blood it is not typical to understand that

156
00:20:15,400 --> 00:20:20,080
next one is blood so there are two kinds of blood mentioned here stored blood

157
00:20:20,080 --> 00:20:25,920
and mobile blood here as to gather stored blood is the color of cooked and

158
00:20:25,920 --> 00:20:36,840
thickens leg solution mobile blood is the color of clear legs solution and then

159
00:20:36,840 --> 00:20:43,040
but some lines down the mobile blood permeates the whole of the crown too

160
00:20:43,040 --> 00:20:50,360
comically acquired body it is a direct translation of the pali word

161
00:20:50,360 --> 00:20:59,480
buba dehagar that simply means the living body the crown too comically acquired

162
00:20:59,480 --> 00:21:02,960
body simply means living body

163
00:21:02,960 --> 00:21:16,240
try to be close to the world and trying to really means the result of kama

164
00:21:16,240 --> 00:21:19,840
you know the word pali word buba dehagar the literal meaning of the word buba

165
00:21:19,840 --> 00:21:28,480
dehagar is krantu it is krantu by kama that means the result of kama you know

166
00:21:28,480 --> 00:21:33,680
there are a few kinds of material properties those costs by karma those

167
00:21:33,680 --> 00:21:41,680
cost by consciousness those costs by food and those costs by temperature or

168
00:21:41,680 --> 00:21:53,320
climate so it simply means here the living body not not in dead body are not

169
00:21:53,320 --> 00:22:05,160
things like trees and rocks and others and then sweat so it is not difficult

170
00:22:05,160 --> 00:22:14,080
to understand and next one is fat and fat here means solid fat there are two

171
00:22:14,080 --> 00:22:19,720
kinds of fat mentioned here so this one is solid so this is a thick

172
00:22:19,720 --> 00:22:34,960
and wet and the next one is tears their location is in the eyes and then next

173
00:22:34,960 --> 00:22:42,600
green that is melted and wet it normally we call clear clear oil or

174
00:22:42,600 --> 00:22:53,280
something it is so the other one is the the fat the other one fat is thick

175
00:22:53,280 --> 00:22:59,840
ungrable thick oil and this is thin oil

176
00:23:05,160 --> 00:23:10,480
it is to be found mostly on the palms of the hands backs of the hands soles

177
00:23:10,480 --> 00:23:14,720
of the feed that sort of feed tip of the nose for it and points of the shoulders

178
00:23:14,720 --> 00:23:24,800
it's something like sweat not really sweat sometimes you your hands are wet so

179
00:23:24,800 --> 00:23:30,000
that is what we call grease yeah

180
00:23:30,000 --> 00:23:40,880
so when you put on shoes and you have that kind of grease there and then

181
00:23:40,880 --> 00:23:52,800
spittle or saliva and next one is snot now that's that's mucus I mean mucus and

182
00:23:52,800 --> 00:24:02,160
the nose does it come from the brain it may not but here I just said that

183
00:24:02,160 --> 00:24:10,640
that the this is impurity that trickles out from the brain there is a long

184
00:24:10,640 --> 00:24:16,200
explanation but it's true because there are fine

185
00:24:16,200 --> 00:24:24,360
as there's a brittle bones from which the brain has its own fluid because it's

186
00:24:24,360 --> 00:24:33,600
fine of fluid and it is it is its own canals and the formation of these brain

187
00:24:33,600 --> 00:24:38,800
fluids are somewhat filled and it's cream like guinea is

188
00:24:38,800 --> 00:24:46,440
creamy out blood to form the urine like these there's not a screen product of the

189
00:24:46,440 --> 00:24:54,240
brain fluid and urine is a screen product of the blood they are screen

190
00:24:54,240 --> 00:25:05,040
and the flexes is just about the nose and the blood so this is correct this is

191
00:25:05,040 --> 00:25:13,200
trickles out from the brain or oozes out from the brain and then the next one is

192
00:25:13,200 --> 00:25:24,480
oil of the joints oil that stays in the joints and it is it is translated as

193
00:25:24,480 --> 00:25:34,040
by Niana Pornika is sonovic fluid so I looked that word in the dictionary and

194
00:25:34,040 --> 00:25:41,000
it says sonovia a clear visit lubricating fluid secreted by membranes in joint

195
00:25:41,000 --> 00:25:48,560
cavities seeds of tendons and bussy so also call you a yeah so no real fluid

196
00:25:48,560 --> 00:25:55,920
fluid and the knee dry yeah full but when you call all you can the fluid is

197
00:25:55,920 --> 00:26:01,260
somewhat reduced and when you sit and stand up you can see the

198
00:26:01,260 --> 00:26:06,720
yeah yeah this mention yeah so when it when it gets up was it's down moves for

199
00:26:06,720 --> 00:26:12,880
the baby but then the stretches then his bones creak yeah when I find a

200
00:26:12,880 --> 00:26:21,360
water there's creak in the air and then the last one you lean there is one thing

201
00:26:21,360 --> 00:26:27,200
I'm not happy about it just question of blood earlier you know about five or

202
00:26:27,200 --> 00:26:32,480
six lines from bottom let's say while they usually you're only decreasing

203
00:26:32,480 --> 00:26:42,120
from the body and as the bladder it's way of entry is not evident but there are

204
00:26:42,120 --> 00:26:49,920
two two what about that two kites going into the bladder right from the kidneys

205
00:26:49,920 --> 00:26:58,640
but here you said it's way of entry is not evident but it is very very clear

206
00:26:58,640 --> 00:27:07,940
that during enters the bladder from the kidneys by way of that pipe

207
00:27:07,940 --> 00:27:18,280
it may be it may mean that it's way of entry is not evident actually it is

208
00:27:18,280 --> 00:27:25,840
difficult to explain it is not evident because even if every then there will

209
00:27:25,840 --> 00:27:34,300
be reflux you know it was the nature was so range that the entry is not

210
00:27:34,300 --> 00:27:40,780
visible the entry is not evident if it is evident that you ring from the

211
00:27:40,780 --> 00:27:49,500
bladder can refresh when it is full but it was so you know the mechanism is so

212
00:27:49,500 --> 00:28:05,820
rain that it is not evident so it is not like water and breathing from the pipe no

213
00:28:05,820 --> 00:28:11,820
the bladder is full the urine will be reflux back to the kidney it never

214
00:28:11,820 --> 00:28:22,560
happens in a normal person when only the entrance is brewing or damage

215
00:28:22,560 --> 00:28:30,700
damage then only there is a reflux so there's something something like a

216
00:28:30,700 --> 00:28:44,640
valve yeah something very very complicated but

217
00:28:44,640 --> 00:28:55,960
you know we are fortunate today okay you ring there and then arising of

218
00:28:55,960 --> 00:29:02,820
absorption so only first John again arrived through the practice of this

219
00:29:02,820 --> 00:29:07,480
meditation and not the other kinds of meditation I mean not not the other

220
00:29:07,480 --> 00:29:21,840
kinds of genres like second and food so and the the learning sign and the

221
00:29:21,840 --> 00:29:31,080
countable signs are described in paragraph 141 as it gives his attention to

222
00:29:31,080 --> 00:29:34,960
them again and again as repulsive repulsive in applying the process of

223
00:29:34,960 --> 00:29:39,840
successive leaving etc eventually absorption arises in him so he gets

224
00:29:39,840 --> 00:29:46,000
China here in the appearance of the head hairs etc as to color shake direction

225
00:29:46,000 --> 00:29:52,280
location and delimitization is a learning sign their appearance as repulsive in

226
00:29:52,280 --> 00:29:56,520
all aspects is the counterpart sign so there are two kinds of signs learning sign

227
00:29:56,520 --> 00:30:05,960
and cut up a sign and then he gets only first John and there is the story a very

228
00:30:05,960 --> 00:30:12,320
short story of the elder Malaga so that elder it seems to be elder Abaya and the

229
00:30:12,320 --> 00:30:16,600
eager resider by the hand and after say friend Abaya first learned this matter

230
00:30:16,600 --> 00:30:21,460
and he went on the elder Malaga is an obtainer of 32 journals indeed that he

231
00:30:21,460 --> 00:30:26,800
too fast if he enters upon one by night and one by day he goes on entering

232
00:30:26,800 --> 00:30:32,960
upon them for over 49 but if he enters upon one each day he goes on entering

233
00:30:32,960 --> 00:30:42,840
upon them for over a month and then there will be benefits of this meditation

234
00:30:42,840 --> 00:30:48,720
say it because who is devoted to this mindfulness occupied with the body is a

235
00:30:48,720 --> 00:30:53,600
conqueror of boredom and the life and boredom does not conquer him it was

236
00:30:53,600 --> 00:30:58,280
transcending boredom as a horizon it is a conqueror of fear and dread and fear

237
00:30:58,280 --> 00:31:02,920
and dread do not conquer him he dwells transcending fear and dread as they arrive

238
00:31:02,920 --> 00:31:08,440
he is one who bears cold and pee to endures a risen body feelings that are

239
00:31:08,440 --> 00:31:13,280
menacing to live and so on so on it is from the it is so that from Malaga

240
00:31:13,280 --> 00:31:18,480
Malaga middle length sayings it becomes an obtainer of the four journals based on

241
00:31:18,480 --> 00:31:26,960
the color aspect of the head hairs etc so although a person cannot get

242
00:31:26,960 --> 00:31:34,800
second cut and food genres by practicing this kind of meditation yet he can

243
00:31:34,800 --> 00:31:43,280
get food genres if he concentrate on the color aspect of these parts not on

244
00:31:43,280 --> 00:31:49,400
the responsiveness but on the color aspect say when you look at the hair you

245
00:31:49,400 --> 00:31:56,720
concentrate on the color color of the hair as blue blue or damp or something so

246
00:31:56,720 --> 00:32:02,160
in that case you can get all four kinds of channels that is why this mentioned

247
00:32:02,160 --> 00:32:09,240
here he becomes an obtainer of the four channels based on the color aspect of the

248
00:32:09,240 --> 00:32:13,280
head hairs etc and it becomes he comes to penetrate the six kinds of

249
00:32:13,280 --> 00:32:19,360
direct knowledge that is super normal knowledge so let a man if he is wise

250
00:32:19,360 --> 00:32:24,640
untaring me devote his days to mindfulness of body which rewards him in so

251
00:32:24,640 --> 00:32:29,320
many ways there's a section dealing with the mindfulness of the fact that the body

252
00:32:29,320 --> 00:32:41,280
in the detailed treaties so you are saying that or this study is saying that

253
00:32:41,280 --> 00:32:46,160
you obtain the first channel we have to go through as a kind of

254
00:32:46,160 --> 00:32:55,120
converted through 32 parts no no there are other other subjects of meditation but

255
00:32:55,120 --> 00:33:01,040
if you practice this meditation you will get only first channel but there are

256
00:33:01,040 --> 00:33:06,520
other kinds of meditation like a cinema meditation by which you can get all four

257
00:33:06,520 --> 00:33:16,960
channels and this this meditation can lead you only first channel because the object is so

258
00:33:16,960 --> 00:33:22,760
gross so gross that you need you need initial application with a car for your

259
00:33:22,760 --> 00:33:29,400
mind to be on the object just as you need you need a pole or something to keep

260
00:33:29,400 --> 00:33:41,240
the boat in a safe current that is why you cannot get second and four channels but

261
00:33:41,240 --> 00:33:47,680
if you concentrate on the color aspect of these things like when you see when

262
00:33:47,680 --> 00:33:51,520
you're gonna say no blood you don't say blood blood blood blood red red

263
00:33:51,520 --> 00:33:58,000
red then that becomes color color casino meditation when it becomes

264
00:33:58,000 --> 00:34:04,800
color casino meditation then you can get full full up to full time

265
00:34:08,960 --> 00:34:12,640
okay next week

266
00:34:18,480 --> 00:34:22,720
breathing meditation yeah

267
00:34:22,720 --> 00:34:29,640
we're not all let me see

268
00:34:29,640 --> 00:34:52,680
today

269
00:34:52,680 --> 00:35:12,680
paragraph, up to paragraph 185, that will be for next week, it is very interesting,

270
00:35:12,680 --> 00:35:32,060
but oh 30 students will tend the group king of these students, like touchdown

271
00:35:32,060 --> 00:35:48,020
The first set, they come there, they are the outgrowth from ectoderm, from the same tissue,

272
00:35:48,020 --> 00:36:00,940
from the femoral and ronical, as an embryo, from the fetus, being five things come from ectoderm.

273
00:36:00,940 --> 00:36:10,420
It's interesting, and there are this in the previous, I think, that we have to take the

274
00:36:10,420 --> 00:36:23,500
pally ones, and then make a kind of research, and correct all those mistakes, so if we are

275
00:36:23,500 --> 00:36:53,500
maintaining

276
00:37:23,500 --> 00:37:26,500
.

277
00:37:26,500 --> 00:37:29,500
..

278
00:37:29,500 --> 00:37:32,500
..

279
00:37:32,500 --> 00:37:35,500
..

280
00:37:35,500 --> 00:37:38,500
..

281
00:37:38,500 --> 00:37:41,500
...

282
00:37:41,500 --> 00:37:44,500
...

283
00:37:44,500 --> 00:37:47,500
...

284
00:37:47,500 --> 00:37:50,500
...

285
00:37:50,500 --> 00:37:51,500
..

