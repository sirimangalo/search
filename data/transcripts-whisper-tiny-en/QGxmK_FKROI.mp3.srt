1
00:00:00,000 --> 00:00:05,140
Hello and welcome back to our study of the Dhamapanda. Today we continue on with

2
00:00:05,140 --> 00:00:10,080
first number 96, which reads as follows.

3
00:00:10,080 --> 00:00:15,720
Santantasa manang hoti. Santa wa jajakam maja.

4
00:00:15,720 --> 00:00:29,640
Sama danya vimotasa. Upa santasa taddinoh, which means calm or peaceful is

5
00:00:29,640 --> 00:00:43,200
his or is that person's mind, peaceful their speech and their acts. Who with

6
00:00:43,200 --> 00:00:56,440
right knowledge is freed. Such a one who is such a one for such a one, you know

7
00:00:56,440 --> 00:01:07,000
such a one who is at peace themselves. So one who has right who is delivered

8
00:01:07,000 --> 00:01:21,200
through right knowledge, who is at peace themselves. Something like that.

9
00:01:21,200 --> 00:01:34,960
So talking about the three doors of karma, the mind or the speech door and the

10
00:01:34,960 --> 00:01:43,200
body door. So the three ways we can perform karma. And such a such a one who

11
00:01:43,200 --> 00:01:51,840
has become free is peaceful in all of these. This story is one of the more heartwarming

12
00:01:51,840 --> 00:02:05,520
stories, not a long story. The story about a certain monk who seems to have

13
00:02:05,520 --> 00:02:14,740
practiced well, learned all of the rules of the monkhood. He was known as

14
00:02:14,740 --> 00:02:19,760
kostambiwi wa sittisa. His name was tisa. Tisa was like a common name like John

15
00:02:19,760 --> 00:02:26,200
back then. It's a very common name. But kostambiwasi means he dwelled at kostambi.

16
00:02:26,200 --> 00:02:36,200
And he stayed there for the three rains for the three months of the

17
00:02:36,200 --> 00:02:42,200
rains retreat. And at the end the lay person who looked at who was looking

18
00:02:42,200 --> 00:02:52,360
after him came to him and offered him a set of robes and some medicines. So

19
00:02:52,360 --> 00:03:00,800
the kind of medicines like sugar or honey or ghee like butter. The kind of

20
00:03:00,800 --> 00:03:05,800
medicines that actually you can only keep for a certain amount of time. And so

21
00:03:05,800 --> 00:03:13,660
the monk didn't immediately receive them because monks can only have one set of

22
00:03:13,660 --> 00:03:20,840
robes and they can only keep these medicines for a short time and can only use

23
00:03:20,840 --> 00:03:25,040
them when they're sick and that kind of thing. So he says something that's

24
00:03:25,040 --> 00:03:29,000
actually quite interesting from a monastic point of view. He says yes what are they

25
00:03:29,000 --> 00:03:32,040
in the lay? He says oh these are the offering that we give to the monk whenever

26
00:03:32,040 --> 00:03:38,840
they whoever stays with us. Please accept them. And he says I'm sorry I have no

27
00:03:38,840 --> 00:03:46,280
need of these because he can't reasonably take them and use them. And he says

28
00:03:46,280 --> 00:03:50,040
why why can't you take them? And he says what he says is interesting. He says

29
00:03:50,040 --> 00:03:58,240
that he has no novice to perform the usual duties. And I think the

30
00:03:58,240 --> 00:04:03,240
meaning here is that the novice would take the robes and offer them to the

31
00:04:03,240 --> 00:04:08,040
elder if the need should arise. So they were using the novices. It's kind of

32
00:04:08,040 --> 00:04:12,040
odd to sort of get around the world rules by having a novice carry these

33
00:04:12,040 --> 00:04:15,320
things for you. And I'm not convinced that it's the best reading of the

34
00:04:15,320 --> 00:04:22,280
rules but these stories tend to have those sorts of practices in them that

35
00:04:22,280 --> 00:04:27,080
don't seem exactly to be how it was laid out in them by the Buddha. But

36
00:04:27,080 --> 00:04:32,280
anyway who am I to know? But that's that's sort of how it went whether right or

37
00:04:32,280 --> 00:04:39,880
wrong I don't know. And so the layman says oh well if that's true take my

38
00:04:39,880 --> 00:04:43,080
son. Take my son with you and have him or

39
00:04:43,080 --> 00:04:47,320
Dana's a novice and he'll look after you and I guess carry your sugar and

40
00:04:47,320 --> 00:04:52,520
your butter around for you. Because novices don't have those kind of rules that

41
00:04:52,520 --> 00:04:56,040
they can't keep this and can't keep it.

42
00:04:57,800 --> 00:05:04,760
And so he accepted and this his son who was seven years old he gives him

43
00:05:04,760 --> 00:05:11,480
over to the elder and says please accept him as a novice.

44
00:05:11,480 --> 00:05:17,000
So the other takes the boy, wets his hair, teaches him the

45
00:05:17,000 --> 00:05:22,200
tajapanjaka kamatana, the fivefold meditation with the skin as its fifth

46
00:05:22,200 --> 00:05:29,400
meaning quesa, hair on the head, loma, hair on the body, nakha, nails,

47
00:05:29,400 --> 00:05:38,280
tanta, teeth, tansu, is the skin, quesa, loma, nakha, tanta, tansu.

48
00:05:38,280 --> 00:05:41,880
These are what you teach them a new monk that's the tradition

49
00:05:41,880 --> 00:05:45,800
because it's a basic meditation focusing on the parts of the body to help

50
00:05:45,800 --> 00:05:52,760
see them as they are and not be partial towards them or against them.

51
00:05:54,280 --> 00:05:56,760
Useful for monks if they find themselves getting

52
00:05:56,760 --> 00:06:01,640
into thoughts of lust about the body, lust for the body.

53
00:06:02,920 --> 00:06:07,960
And as soon as the razor says as soon as the razor touched his head

54
00:06:07,960 --> 00:06:12,600
the novice became an hour hunt, the boy became an hour hunt.

55
00:06:12,600 --> 00:06:18,360
He was ready, he was clearly someone who had been cultivating

56
00:06:18,360 --> 00:06:22,680
perfections and we're cultivating good things for a long time because

57
00:06:22,680 --> 00:06:27,880
that was all that took just this click with teaching of this

58
00:06:27,880 --> 00:06:32,680
meditation and the changing the recognition,

59
00:06:32,680 --> 00:06:36,440
probably he was a monk in his past life.

60
00:06:36,440 --> 00:06:40,360
And so he became an hour hunt right away and didn't tell the

61
00:06:40,360 --> 00:06:43,880
elder, the elder had no idea because the elder actually being a good monk

62
00:06:43,880 --> 00:06:49,320
but he wasn't enlightened, he was just an ordinary world thing.

63
00:06:53,560 --> 00:06:57,400
And so he stayed there for another two weeks

64
00:06:57,400 --> 00:06:59,560
and then he decided to go back to see the Buddha.

65
00:06:59,560 --> 00:07:01,880
I thought, well, I'll bring this novice to go and see the Buddha.

66
00:07:01,880 --> 00:07:07,320
That'll be a good way to introduce them to the Buddha's sassanan,

67
00:07:07,320 --> 00:07:10,440
maybe the Buddha can teach them something, maybe it'll help him on his own

68
00:07:10,440 --> 00:07:14,120
path because who knows, maybe one day this novice will become

69
00:07:14,120 --> 00:07:18,360
a good meditator like me or something like that.

70
00:07:18,360 --> 00:07:23,160
And so on the way they stopped off at this lodging

71
00:07:23,160 --> 00:07:36,840
and at a monastery but they only had one kutti

72
00:07:36,840 --> 00:07:39,560
because the novice spent all this time looking after the elder.

73
00:07:39,560 --> 00:07:41,960
You got a kutti for the elder and he spent his time

74
00:07:41,960 --> 00:07:46,280
doing all these duties that the novice would have to do is sweeping it out

75
00:07:46,280 --> 00:07:57,560
and opening windows, closing windows, putting the bed down and so on.

76
00:07:57,560 --> 00:08:01,480
And by the time he had taken care of the elder's needs, it was too late

77
00:08:01,480 --> 00:08:04,040
for him to get a kutti for himself, a hut for himself.

78
00:08:07,880 --> 00:08:13,080
And so the elder said, oh, don't you have a kutti for yourself?

79
00:08:13,080 --> 00:08:16,680
No, I'm sorry, I don't. No opportunity.

80
00:08:16,680 --> 00:08:19,720
So the elder said, well, then fine, you just stay with me

81
00:08:19,720 --> 00:08:26,200
and you can just stay with me and in the morning, well, we'll look after it.

82
00:08:26,200 --> 00:08:30,920
We'll be better to stay with me than elsewhere.

83
00:08:32,840 --> 00:08:36,440
And then the elder goes and lies down on the bed and immediately falls asleep

84
00:08:36,440 --> 00:08:41,080
because he's just an ordinary world thing. But the novice thinking

85
00:08:41,080 --> 00:08:45,000
the novice realizes something and that is that this is

86
00:08:45,000 --> 00:08:48,200
for the past two nights, he has also spent a night with,

87
00:08:48,200 --> 00:08:51,640
no, this is have a rule. He's not a full monk.

88
00:08:51,640 --> 00:08:55,480
A novice takes on 10 precepts, so they're allowed to wear the robes,

89
00:08:55,480 --> 00:08:59,560
but they haven't yet become a monk. So they haven't,

90
00:08:59,560 --> 00:09:06,120
they are not considered to be in the inner circle, I guess you could say.

91
00:09:06,120 --> 00:09:09,480
But the point is, they still don't fit in with the monks.

92
00:09:09,480 --> 00:09:14,600
And so to be in too close association,

93
00:09:14,600 --> 00:09:21,160
can lead to problems and difficulties for the monks.

94
00:09:21,160 --> 00:09:27,720
So they are only so only allowed to stay with the monks for a maximum of two nights.

95
00:09:27,720 --> 00:09:30,360
After the third night, it's not that they're not allowed to.

96
00:09:30,360 --> 00:09:35,320
It's that the monk commits an offense if he stays with a non-ordained person,

97
00:09:35,320 --> 00:09:41,240
three nights in a row. So because the idea is monks have to stick to themselves,

98
00:09:41,240 --> 00:09:45,080
have to not get too close association with people who are not

99
00:09:45,080 --> 00:09:49,080
keeping the rules because they can drag them down and so on.

100
00:09:49,080 --> 00:09:54,520
People who haven't yet been trained as monks can often

101
00:09:54,520 --> 00:09:59,240
criticize unjustly. Well, the story about that the monks were snoring or something

102
00:09:59,240 --> 00:10:04,040
in the monks and the late people started getting critical of them and

103
00:10:04,040 --> 00:10:09,560
overly critical of that kind of thing. Now they lose any sense of respect.

104
00:10:09,560 --> 00:10:14,840
So anyway, there's some sense that they have to be a bit separate.

105
00:10:14,840 --> 00:10:17,720
And then obviously realized this and he realized if I stay in here overnight,

106
00:10:17,720 --> 00:10:22,680
as he told me, he'll be breaking a rule. Can't have that.

107
00:10:22,680 --> 00:10:28,040
So the novice sits up, sits cross-legged and comes from

108
00:10:28,040 --> 00:10:31,080
leaving because he's in our hunt, just does practice his sitting meditation

109
00:10:31,080 --> 00:10:36,120
all night. Because the loophole to the rule, again, it's all about loopholes.

110
00:10:36,120 --> 00:10:40,200
It seems, is that if he's sitting up, it's not considered to have

111
00:10:40,200 --> 00:10:46,200
slept, have stayed together overnight. Don't ask me, but that's

112
00:10:46,200 --> 00:10:49,720
somehow helps to fix his things.

113
00:10:51,080 --> 00:10:54,680
And he stayed there all night and the elder just snored away,

114
00:10:54,680 --> 00:10:58,600
totally oblivious, but the elder did wake up in the morning and was actually

115
00:10:58,600 --> 00:11:02,840
conscientious enough to think about this. And he woke up in a completely

116
00:11:02,840 --> 00:11:07,960
pitch dark, still the night, still still not a full night.

117
00:11:07,960 --> 00:11:12,840
And so he picks up his, he's on the bed and he reaches over and he picks up his

118
00:11:12,840 --> 00:11:16,440
fan. So in those times the monks would have a fan to

119
00:11:16,440 --> 00:11:21,240
drive away mosquitoes and to cool themselves off in the heat and that kind of thing.

120
00:11:21,240 --> 00:11:26,760
Different uses for a fan. And he, he took the fan and he pounded the mat

121
00:11:26,760 --> 00:11:31,000
where he knew the novice was. And he said, hey, wake up,

122
00:11:31,000 --> 00:11:35,400
we have to go outside. And then the novice didn't say any

123
00:11:35,400 --> 00:11:40,760
thing. So he threw the, he threw the fan at the novice.

124
00:11:40,760 --> 00:11:43,080
And he didn't see him. He didn't know where the novice was. He thought the

125
00:11:43,080 --> 00:11:45,880
novice was lying down. So he said, okay, you know, maybe

126
00:11:45,880 --> 00:11:50,360
him towards his feet or something. And the novice, because he was sitting up,

127
00:11:50,360 --> 00:11:57,400
the fan hit him square in the eye and poked his eye out,

128
00:11:57,400 --> 00:12:00,360
destroyed his eye.

129
00:12:08,040 --> 00:12:11,960
And the other had no idea, the other had no clue.

130
00:12:11,960 --> 00:12:16,360
The novice, knowing that his eye was poked out,

131
00:12:16,360 --> 00:12:20,280
what does he do? He says to the elder, what did you say?

132
00:12:20,280 --> 00:12:24,360
Because I guess he was a little distracted. I says, get out, get out, it's time to go

133
00:12:24,360 --> 00:12:29,080
out. So the novice, what does he do? Covers his eye,

134
00:12:29,080 --> 00:12:33,560
whichever eye it was, covers his eye with one hand

135
00:12:33,560 --> 00:12:38,840
and gets up and walks out. Doesn't say anything about it.

136
00:12:38,840 --> 00:12:43,240
Completely quiet. And on top of that, not only does he,

137
00:12:43,240 --> 00:12:47,800
is he totally, neutrals, only composed, having just

138
00:12:47,800 --> 00:12:51,960
had his eye poked out.

139
00:12:51,960 --> 00:12:56,360
He goes and does the duties of visiting monks. So he goes and

140
00:12:56,360 --> 00:13:02,040
sweeps the trail, sweeps the pads, sweeps out the

141
00:13:02,040 --> 00:13:07,160
outhouse, sets out water for the elder, washing. He sweeps,

142
00:13:07,160 --> 00:13:09,560
you know, he's got one handings, so he's sweeping with one hand with the

143
00:13:09,560 --> 00:13:13,480
broom, sweeping out the, I would have a send,

144
00:13:13,480 --> 00:13:17,720
putting out water for washing and water for drinking.

145
00:13:17,720 --> 00:13:21,800
And then he even goes and gets a tooth, these

146
00:13:21,800 --> 00:13:23,720
sticks that they would use to brush their teeth.

147
00:13:23,720 --> 00:13:26,120
You can still get them in India. It's kind of neat.

148
00:13:26,120 --> 00:13:29,560
On the Buddhist circuit, I got a whole bunch of them when I was there

149
00:13:29,560 --> 00:13:35,400
last year. And so he brings a toothbrush to the

150
00:13:35,400 --> 00:13:39,800
elder monk. And it's kind of dark. So the elder doesn't

151
00:13:39,800 --> 00:13:43,720
really see, but he sees the novice come with one hand and offer him

152
00:13:43,720 --> 00:13:52,360
a toothbrush. And apparently another one of the

153
00:13:52,360 --> 00:13:56,680
customs went offering to an elder in order to

154
00:13:56,680 --> 00:14:00,040
cultivate respect is to offer it with two hands.

155
00:14:00,040 --> 00:14:03,960
So this is protocol between monks and junior monks, senior monks,

156
00:14:03,960 --> 00:14:08,840
novices and senior monks. For lay people they can do as they like, but

157
00:14:08,840 --> 00:14:13,720
there's kind of a protocol in order to keep humility and not get arrogant

158
00:14:13,720 --> 00:14:20,840
and not, so we go with someone senior, then you offer to them with two hands.

159
00:14:20,840 --> 00:14:25,080
And so the elder got kind of mifted this. He said,

160
00:14:25,080 --> 00:14:28,760
well, what kind of a novice are you not properly trained

161
00:14:28,760 --> 00:14:32,440
offering toothbrush to your teacher with one hand? What kind of novice does

162
00:14:32,440 --> 00:14:38,120
that he's kind of? And then I thought I thought you better than that.

163
00:14:38,120 --> 00:14:43,480
And I was just, venerable sir, I know full well how to

164
00:14:43,480 --> 00:14:50,440
the rules that are to be performed, but one hand isn't free.

165
00:14:50,440 --> 00:14:56,200
My one hand isn't disengaged.

166
00:14:56,200 --> 00:15:02,600
What? What's wrong? Why can't you use your other hand?

167
00:15:02,600 --> 00:15:07,400
And then the novice has to tell them. So the novice relates to him

168
00:15:07,400 --> 00:15:10,840
from start to finish. I was sitting up because I was sitting up when you

169
00:15:10,840 --> 00:15:15,800
threw the fan at me. He poked my eye out.

170
00:15:17,160 --> 00:15:24,680
And the elder is understandably upset, moved, shaken

171
00:15:24,680 --> 00:15:29,640
by this, and he bows down. He goes down on his knees and says,

172
00:15:29,640 --> 00:15:35,160
the novice, please forgive me. I'm so sorry, please spare me, kind of thing.

173
00:15:35,160 --> 00:15:37,160
Be my refuge is what the Pauli says.

174
00:15:41,320 --> 00:15:43,400
I think Roch is down on the ground in front of him and

175
00:15:43,400 --> 00:15:47,880
begs him for forgiveness. And the novice, what does he do?

176
00:15:47,880 --> 00:15:53,720
Just perfection after perfection. This is a wonderful way of describing

177
00:15:53,720 --> 00:15:58,600
Ireland. I remember the seven-year-old kid is a fully enlightened being,

178
00:15:58,600 --> 00:16:03,240
or an enlightened being, not a Buddha, but an Ireland.

179
00:16:03,240 --> 00:16:05,960
He says, venerable sir, it isn't your fault.

180
00:16:05,960 --> 00:16:11,320
It was not because it was not in order to shame you that I spoke.

181
00:16:11,320 --> 00:16:14,120
I just wanted to let you know what happened. I knew

182
00:16:14,120 --> 00:16:17,960
I had to ease your mind, but I didn't tell you also to

183
00:16:17,960 --> 00:16:22,360
ease your mind. I didn't, it's not that I wanted to keep you from you.

184
00:16:22,360 --> 00:16:27,640
It's that I knew you might get upset. And so in order to spare you that, I didn't

185
00:16:27,640 --> 00:16:31,720
say anything, it's not your fault. You're not to blame.

186
00:16:31,720 --> 00:16:37,560
This is the fault of some satirizes. This is the fault of the rounds of rebirth.

187
00:16:37,560 --> 00:16:41,880
If you're going to be reborn, these things are going to happen.

188
00:16:41,880 --> 00:16:46,520
Anything's out of that. This seven-year-old kid can you imagine?

189
00:16:46,520 --> 00:16:51,960
And so the other is even further moved by that,

190
00:16:51,960 --> 00:16:57,320
and the novice tries to comfort him, but he won't be comforted.

191
00:16:57,320 --> 00:17:01,240
So he takes the novice to the Buddha. He picks up the novice's ball

192
00:17:01,240 --> 00:17:06,040
and requisites and carries them for the novice and brings him to the Buddha,

193
00:17:06,040 --> 00:17:10,520
shaking his head and weeping the whole way.

194
00:17:10,520 --> 00:17:14,840
And when they get to the Buddha, he goes straight to the Buddha, and

195
00:17:14,840 --> 00:17:18,920
bows down and prepares to tell the Buddha. And the Buddha says,

196
00:17:18,920 --> 00:17:23,160
oh, Bhikkhu is everything okay with you?

197
00:17:24,360 --> 00:17:29,000
What kind of, how do you answer that? He says, I trust you have not suffered any

198
00:17:29,000 --> 00:17:34,520
excess discomfort. And the monk says,

199
00:17:34,520 --> 00:17:44,760
Bhante, I indeed have lived quite an ease myself, but here I have this novice with me. Take a look

200
00:17:44,760 --> 00:17:50,920
at this novice. He is beyond anything that I've ever seen.

201
00:17:50,920 --> 00:17:54,440
And the Buddha said, what, why, what has he done?

202
00:17:54,440 --> 00:17:58,040
And the other told the whole story to the Buddha. And he said,

203
00:17:58,040 --> 00:18:03,240
I don't understand when Bhante, when I asked him to pardon, when I asked him to forgive me,

204
00:18:03,240 --> 00:18:06,680
he said, it's not your fault, it's the fault of some sara.

205
00:18:06,680 --> 00:18:11,800
Don't be disturbed, don't be upset. He tried to comfort me when

206
00:18:11,800 --> 00:18:16,440
here he is with his eye poked out, holding it with one hand,

207
00:18:16,440 --> 00:18:19,880
offering me a toothbrush.

208
00:18:21,080 --> 00:18:25,560
And the Buddha says, oh, those who have freed themselves from defilements

209
00:18:25,560 --> 00:18:30,600
have neither anger nor hatred towards anyone. On the contrary,

210
00:18:30,600 --> 00:18:35,320
they're in a state of calm, their senses, their mind, their speech, and their body.

211
00:18:39,080 --> 00:18:44,440
And then he taught this verse. His thoughts are calm, his speech is calm,

212
00:18:46,040 --> 00:18:52,600
santang dasa manang huti. The mind of such a person is calm,

213
00:18:52,600 --> 00:19:00,200
his speech is calm, his deeds are calm, who has, what sort of person is this?

214
00:19:00,200 --> 00:19:05,000
One who has freed themselves with right knowledge.

215
00:19:05,000 --> 00:19:09,240
Bhutan dasa dada, you know, someone who is tranquil or peaceful.

216
00:19:11,880 --> 00:19:13,880
So what can we learn from this?

217
00:19:13,880 --> 00:19:29,000
You know, it sets the bar, right? It holds no, doesn't hold any punches.

218
00:19:29,000 --> 00:19:33,880
This is telling us how far we intend to go, how far we can go,

219
00:19:33,880 --> 00:19:46,040
how perfect we expect to become through the practice, how far we believe it possible to go,

220
00:19:46,040 --> 00:19:55,320
maybe not in this life, but eventually through the practice, our understanding is this isn't

221
00:19:55,320 --> 00:20:04,600
some simple thing where, oh yes, you know, you don't complain about a toothache anymore, or when

222
00:20:04,600 --> 00:20:09,800
you get it, have a stomach ache, you're okay, you're okay skipping lunch now because you've let go.

223
00:20:16,280 --> 00:20:17,160
It's not like that.

224
00:20:17,160 --> 00:20:26,440
Where we're going is to do the end of suffering, where one really doesn't react,

225
00:20:26,440 --> 00:20:32,040
really doesn't get upset, where one is really and truly able to experience things as they are.

226
00:20:34,520 --> 00:20:38,920
So the mind and the speech and the body don't become tranquil because you don't

227
00:20:39,640 --> 00:20:44,760
encounter bad things, it's not about building up such good karma that you never experience bad things.

228
00:20:44,760 --> 00:20:50,280
It's not that kind of invincible, it's the invincibility of the mind,

229
00:20:53,480 --> 00:20:59,000
where you have seen clearly and the wonderful thing is that it just, it comes simply from

230
00:20:59,000 --> 00:21:06,280
seeing clearly, which is the point of this verse, that true peace, the kind that can

231
00:21:06,920 --> 00:21:11,000
see you through losing an eye or limb or even your life,

232
00:21:11,000 --> 00:21:20,920
that kind of peace and tranquility is only to be found through wisdom and it's to be found

233
00:21:20,920 --> 00:21:24,280
simply through wisdom with no other requirement whatsoever.

234
00:21:29,640 --> 00:21:34,040
Because when you see things as they are, there's no reason to get upset, there's no benefit

235
00:21:34,040 --> 00:21:40,680
to getting upset, there's no benefit to be had from this novice yelling and screaming and

236
00:21:40,680 --> 00:21:45,080
wailing and crying. It's reasonable if it would be reasonable if he did, you'd think, well, he's

237
00:21:45,080 --> 00:21:51,720
only human. But somehow this is the idea is that enlightened being becomes, it goes beyond

238
00:21:52,680 --> 00:21:58,840
the ordinary state of a human. We're able to attain a state of greatness,

239
00:21:58,840 --> 00:22:05,160
a state of profound peace and tranquility.

240
00:22:10,760 --> 00:22:15,080
So this is again something for us to emulate, but I think more here is something for us to

241
00:22:15,640 --> 00:22:20,120
compare. How would you do it fair if you lost an eye in that way?

242
00:22:21,560 --> 00:22:25,080
Sometimes it's good to think about these things, it's good to imagine the situation,

243
00:22:25,080 --> 00:22:32,280
it gives you a chance to see your own defilements, to get a taste of what you have left to accomplish.

244
00:22:33,000 --> 00:22:36,760
What is left inside of you that will cause you suffering? Because losing an eye doesn't cause you

245
00:22:36,760 --> 00:22:42,760
suffering. It's only when you react negatively to it. Oh, that's not what I wanted.

246
00:22:44,200 --> 00:22:48,360
When you had expectations for that eye, I wanted to use it for things.

247
00:22:48,360 --> 00:22:56,360
But this novice had no use for the eye, losing it was like losing a piece of trash,

248
00:22:59,560 --> 00:23:07,000
meaningless, of no consequence. Because his peace was, his happiness was independent of the eye,

249
00:23:07,400 --> 00:23:11,720
independent of the physical body, independent of anything in the world.

250
00:23:11,720 --> 00:23:17,160
That's where our practice leads us. It's not like we want to lose an eye.

251
00:23:18,840 --> 00:23:26,600
I mean, how could you criticize this? Wouldn't it be great if when the most horrific thing

252
00:23:26,600 --> 00:23:32,120
happened to us we could be happy? Those horrific things do happen to people and they're

253
00:23:32,120 --> 00:23:38,920
horrified by them. They can go crazy because of them. Wouldn't it be great if that weren't the

254
00:23:38,920 --> 00:23:44,280
case? Wouldn't it be great if we could deal with anything that we wouldn't have to live our

255
00:23:44,280 --> 00:23:49,960
lives in fear protecting our bodies constantly obsessing over them, worried about them,

256
00:23:51,960 --> 00:23:59,960
worried about wrinkles, worried about fat, worried about this and that. Well, here we see how far

257
00:23:59,960 --> 00:24:07,960
we are and how far we have to go and we can see the need for the work that we have worked to do.

258
00:24:07,960 --> 00:24:15,080
Because we really have a long way to go. It's not enough to say, oh, look at me. I had back pain

259
00:24:15,080 --> 00:24:21,560
and now I can deal with the back pain. You can deal with losing an eye and you know how far away

260
00:24:21,560 --> 00:24:29,880
you are from the goal. So, that's the Dhamma Padha for tonight. Thank you all for tuning in. Keep

261
00:24:29,880 --> 00:24:45,800
practicing and wishing.

