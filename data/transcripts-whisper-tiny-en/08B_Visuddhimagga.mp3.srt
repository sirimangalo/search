1
00:00:00,000 --> 00:00:08,340
Now, suppose you have all five Janas and also all Aruba Vajara Janas.

2
00:00:08,340 --> 00:00:15,380
And also you can experience what we call super normal knowledge of Abinya.

3
00:00:15,380 --> 00:00:21,740
Abinya is especially developed fifth Janas.

4
00:00:21,740 --> 00:00:35,600
And Abinas are the dead and able yogis to see things far away and to hear sounds far away and so on for to perform some miracles.

5
00:00:35,600 --> 00:00:44,040
Now, suppose you want to hear the sounds far away.

6
00:00:44,040 --> 00:00:51,180
Now, suppose you have all five Janas and also all Aruba Vajara Janas.

7
00:00:51,180 --> 00:00:58,060
And also you can experience what we call super normal knowledge of Abinya.

8
00:00:58,060 --> 00:01:04,460
Abinya is especially developed fifth Janas.

9
00:01:04,460 --> 00:01:18,620
And Abinas are those dead and able yogis to see things far away and to hear sounds far away and so on or to perform some miracles.

10
00:01:18,620 --> 00:01:33,900
Now, suppose you want to hear the sounds far away and it depends on how large you extend the casino sign.

11
00:01:33,900 --> 00:01:43,560
If you extend the casino sign as big as San Francisco City and you may hear sounds in San Francisco City and not outside.

12
00:01:43,560 --> 00:01:50,240
So, the 10 casinos among these 40 meditations subject need be extended.

13
00:01:50,240 --> 00:01:59,240
For it is within just so much space as one is intent upon that means one covers one extends over.

14
00:01:59,240 --> 00:02:08,400
With the casino that one can hear sound with the divine ear elements, see visible objects with the divine eye and know the minds of other beings with the mind.

15
00:02:08,400 --> 00:02:26,680
So, before you experience the super normal knowledge, you have to practice, you have to extend this sign with the counterpart sign.

16
00:02:26,680 --> 00:02:39,020
That means you are defining and the area within which your super normal knowledge will apply.

17
00:02:39,020 --> 00:02:42,340
So, they need be extended.

18
00:02:42,340 --> 00:02:47,340
Mindfulness occupied with the body and the 10 kinds of founders need not be extended.

19
00:02:47,340 --> 00:02:51,880
So, these subjects need not be extended why because they have a definite location.

20
00:02:51,880 --> 00:03:05,180
You have to define these things and so you cannot extend them and if you extend them there is no benefit because there is no benefit in it.

21
00:03:05,180 --> 00:03:10,680
A definiteness of their location will become clear in explaining the mental development.

22
00:03:10,680 --> 00:03:22,600
Now, when the other comes to explaining how to practice meditation on dead bodies and so on and it will become clear.

23
00:03:22,600 --> 00:03:30,080
If the letter are extended, it is only a quantity of corpses that is extended with regard to folders, meditation.

24
00:03:30,080 --> 00:03:38,120
You will extend the corpses, I mean in any of my causes, but there is no benefit.

25
00:03:38,120 --> 00:03:47,560
And it is said in answer to the question of soba, perception of visible forms is quite clear, there is one perception of bone is not clear.

26
00:03:47,560 --> 00:03:53,000
For here the perception of visible form is called quite clear in the sense of extension of the sign.

27
00:03:53,000 --> 00:04:01,720
When the perception of bone is called not quite clear, perception of bones has to do with fondness, meditation.

28
00:04:01,720 --> 00:04:14,520
In the sense, it is non-extension. For the words, I was in an upon this whole earth with the perception of a skeleton that was added by one, one elder.

29
00:04:14,520 --> 00:04:19,400
Now, they are said in the manner of appearance to one who has acquired dead perception.

30
00:04:19,400 --> 00:04:28,760
That means one who has acquired dead perception before and now he extends this perception.

31
00:04:28,760 --> 00:04:41,560
So, it is all right because he is not practicing to get that perception, he has already got that perception and so he extends.

32
00:04:41,560 --> 00:04:45,560
For just as in Damasoka's time, the Karavika but other is sweet.

33
00:04:45,560 --> 00:04:50,840
So, when it saw its own reflection in the looking glass walls all round and for seeds,

34
00:04:50,840 --> 00:05:00,680
Karavika in every direction, so the elder single appeared a thought when he saw the sign appearing in all directions through this acquisition of perception of his skeleton

35
00:05:00,680 --> 00:05:13,160
that the whole earth was covered with bones and then there is the foot non-Karaika but it is interesting but it is difficult to believe.

36
00:05:13,160 --> 00:05:27,400
It is a kind of word and it is said that its sound is very sweet and so the queen asked the community whose voice or whose sound is sweet

37
00:05:27,400 --> 00:05:37,240
as in the community said and the Karavika butts. So, the queen wanted to listen to the sound of the Karavika butts.

38
00:05:37,240 --> 00:05:47,320
So, he asked, she asked his king, King Ahsoka to bring a Karavika butts. So, what Ahsoka did was to stand the king.

39
00:05:49,320 --> 00:05:56,600
The king flew through the air and lay down near the bird and the bird got in the

40
00:05:56,600 --> 00:06:07,240
cage and the cage flew back to the city and so on but after reaching the city, he would not understand because he was depressed.

41
00:06:07,240 --> 00:06:22,040
So, the king asked why why why why did not make any sound because he was lonely if he had if the bird had a companion

42
00:06:22,040 --> 00:06:30,440
he would make noise. So, the king put the mirrors around him and so he did the bird saw his images in the

43
00:06:30,440 --> 00:06:39,640
mirror and he saw they were other birds. So, he was he was heavy and so he made a song and the queen when she heard the

44
00:06:39,640 --> 00:06:48,520
song she was so pleased and she was very heavy and and learning on that heaviness or practicing

45
00:06:48,520 --> 00:06:59,480
meditation on that heaviness. She practiced Vipasana and she became an extreme winner established

46
00:06:59,480 --> 00:07:08,360
in the fusion of string entry that means he became a soda banner. This is just just an example

47
00:07:08,360 --> 00:07:19,800
of just as the the kravika but so many other birds in the mirror and so make sound the the

48
00:07:19,800 --> 00:07:25,560
elder here when he saw the stand appearing in all directions through his acquisition of the perception

49
00:07:25,560 --> 00:07:33,960
of a skeleton about who was covered with bones. So, he appeared to him as fast. He it is not that

50
00:07:33,960 --> 00:07:43,080
he extend the extended the the casino sign and then next paragraph it is if that is so then

51
00:07:43,880 --> 00:07:48,360
is what is called the measurelessness of the object of journal produce on fallen as

52
00:07:48,360 --> 00:07:55,960
contradictory. Now, the the journal produce on fallness is mentioned as

53
00:07:55,960 --> 00:08:05,400
measureless or it is mentioned as with measure. So, is that contradicted then the answer is no.

54
00:08:09,160 --> 00:08:19,720
When a person looks at his small corpse then his his object is said to be with measure and if he

55
00:08:19,720 --> 00:08:27,640
looks at a big cause then his object is said to be measureless although it is not really measureless

56
00:08:27,640 --> 00:08:34,440
but measureless here means large. So, another large object and small object.

57
00:08:38,280 --> 00:08:44,840
And then paragraph 115 as regards the immaterial states as objects.

58
00:08:44,840 --> 00:08:55,960
It should read as regards objects of the immaterial states not immaterial states and objects

59
00:08:55,960 --> 00:09:06,840
but objects of the immaterials space need not be extended since it is the mere removal of the casino.

60
00:09:06,840 --> 00:09:16,360
So, with regard to the objects of Aruba Vajrayana now the object of the first Aruba Vajrayana it

61
00:09:16,360 --> 00:09:29,720
was infinite space. So, that cannot be extended because it is nothing it is obtained through the

62
00:09:29,720 --> 00:09:37,000
removal of casino and removal of casino means not paying attention to the casino sign.

63
00:09:38,200 --> 00:09:46,360
First there is casino sign in in his mind and then he stops paying attention to that casino sign.

64
00:09:46,360 --> 00:09:52,040
So, the casino scientist appears and in the place of that casino sign just the space remains.

65
00:09:52,040 --> 00:10:04,840
So, that space is fixed so that cannot be extended. If it extends it nothing further happens so nothing

66
00:10:04,840 --> 00:10:12,520
will happen. And consciousness need not be extended actually consciousness should not be or could

67
00:10:12,520 --> 00:10:18,280
not be extended since it is a state consisting in an individual essence. That means it is a parameter

68
00:10:18,280 --> 00:10:26,120
it is an ultimate reality a reality which is its own characteristic or individual essence.

69
00:10:27,560 --> 00:10:35,240
Only the concept can be extended not the real the ultimate reality. Ultimate reality is just

70
00:10:35,240 --> 00:10:45,640
ultimate reality and it does not land itself to being extended. So, this consciousness or the first

71
00:10:45,640 --> 00:10:52,360
consciousness cannot be the first Aruba Vajrayana consciousness cannot be extended and the

72
00:10:52,360 --> 00:10:59,240
disappearance of consciousness need not be extended because actually it is concept and it is non

73
00:10:59,240 --> 00:11:06,520
existence. So, what is non existent cannot be extended and then the last one the base consisting

74
00:11:06,520 --> 00:11:12,120
of net of perception or non perception. Here also the object of the base consisting of net of

75
00:11:12,120 --> 00:11:18,600
perception or non perception need not be extended since it is it through is a state consisting

76
00:11:18,600 --> 00:11:24,440
in an individual essence. Now, do you remember the object of the food Aruba Vajrayana?

77
00:11:30,120 --> 00:11:37,800
The object of the food Aruba Vajrayana is the third Aruba Vajrayana consciousness. So, the third

78
00:11:37,800 --> 00:11:45,400
Aruba Vajrayana consciousness is again an ultimate reality having its own individual essence.

79
00:11:45,400 --> 00:11:53,240
So, it cannot be extended because it is not not a concept. So, they cannot be extended.

80
00:11:53,240 --> 00:11:57,480
So, this is as to whether it can be extended or not.

81
00:11:57,480 --> 00:12:10,040
And then as to the object, paragraph 117 of these 40 meditations of those 22 health

82
00:12:10,040 --> 00:12:25,560
controlled signs as object. Now, all the charge under the column nimitta, counterpart sign pt means

83
00:12:25,560 --> 00:12:37,560
counterpart sign. So, let us say 22 health counterpart signs then casinos then

84
00:12:37,560 --> 00:12:44,200
as to boss or fallen as meditation and these two.

85
00:12:44,200 --> 00:12:49,720
That is to say the 10 casinos the 10 kinds of fallen as mindfulness of breathing and mindfulness

86
00:12:49,720 --> 00:12:53,240
occupied with the body. The rest do not have counterpart signs as object.

87
00:12:54,280 --> 00:13:01,320
Then 12 health states consisting an individual essence's object that is to say 8 of the 10 recollections

88
00:13:02,520 --> 00:13:06,680
except mindfulness of breathing and mindfulness occupied with the body, the perception of the

89
00:13:06,680 --> 00:13:11,080
persistence and nutrient method defining of the full elements. The base consisting of

90
00:13:11,080 --> 00:13:15,320
boundless consciousness and the base consisting of neither perception or non-perception.

91
00:13:18,360 --> 00:13:22,760
So, they have the ultimate reality as object.

92
00:13:25,560 --> 00:13:31,560
And the 22 health counterpart signs as object that is to say the 10 casinos in us, the 10 kinds

93
00:13:31,560 --> 00:13:36,120
of fallen as mindfulness of breathing and mindfulness of mindfulness occupied with the body,

94
00:13:36,120 --> 00:13:42,200
while the remaining six have not so classifiable objects. So, these are the

95
00:13:43,160 --> 00:13:46,120
discretion of the objects of meditation in different ways.

96
00:13:47,240 --> 00:13:52,200
And the age of mobile objects in the early states through the counterpart,

97
00:13:54,120 --> 00:14:01,320
though the counterpart is stationary. That is to say the fastering the bleeding,

98
00:14:01,320 --> 00:14:05,720
the warm first dead mindfulness of breathing and water cleaner, the fire guessing at the air

99
00:14:05,720 --> 00:14:10,680
guessing at the case of like guessing at the object consisting of a circle of sunlight, etc.

100
00:14:11,240 --> 00:14:17,240
So, they are shaking objects. They can be shaking objects. The rest have mobile objects.

101
00:14:19,400 --> 00:14:26,760
Now, they have shaking objects only in the preliminary states. But when the yogi reaches the

102
00:14:26,760 --> 00:14:34,520
counterpart signs states, then they are stationary. So, it is only in the preliminary states

103
00:14:34,520 --> 00:14:38,840
that they have shaking objects or mobile objects.

104
00:14:42,760 --> 00:14:47,640
And as to the plane, that means as to the different 31 planes of existence.

105
00:14:48,360 --> 00:14:53,400
First, namely the 10 kinds of fallen as mindfulness of the body and perception of robustness

106
00:14:53,400 --> 00:14:58,040
in new humans do not occur among the eating.

107
00:14:59,160 --> 00:15:02,600
Does that mean that in the fourth child of the breathing stops?

108
00:15:03,880 --> 00:15:07,640
That's right. Yes. You caught the bite.

109
00:15:13,320 --> 00:15:13,640
Yes.

110
00:15:13,640 --> 00:15:25,720
Yes. We come to that description of the breathing meditation.

111
00:15:26,520 --> 00:15:28,120
So, that's right.

112
00:15:31,320 --> 00:15:39,560
And then as to apprehending, that means as to taking the object by side,

113
00:15:39,560 --> 00:15:46,520
by hearing, by seeing, and so on. So, here, according to sight, text, and he is there.

114
00:15:47,560 --> 00:15:50,680
He is there means just hearing something.

115
00:15:51,880 --> 00:15:57,000
This 19, that is to say 9, because in us, permitting the air, because the 10 kinds of

116
00:15:57,000 --> 00:16:02,760
fallen as must be apprehended by sides. So, that means you look at something and practice meditation.

117
00:16:02,760 --> 00:16:09,000
The meaning is that in the early stage, the assigned must be apprehended by constant looking with

118
00:16:09,000 --> 00:16:14,840
the eye. In the case of mindfulness occupied with the body, the five parts, ending with skin,

119
00:16:14,840 --> 00:16:18,360
must be apprehended by sight and the rest by hearsay.

120
00:16:19,720 --> 00:16:22,840
Head hair, body hair, nose, teeth, skin.

121
00:16:24,280 --> 00:16:28,040
These you look at with your eyes and practice meditation on them.

122
00:16:28,040 --> 00:16:39,720
And the others, some you cannot see, like liver, intestines and other things, so that you practice

123
00:16:39,720 --> 00:16:41,240
through hearsay.

124
00:16:45,160 --> 00:16:50,360
Mindfulness of breathing must be apprehended by touch. So, when you practice mindfulness meditation,

125
00:16:50,360 --> 00:16:55,320
you keep your mind here and be mindful of the sensation of touch here,

126
00:16:55,320 --> 00:17:01,720
with the air going in and out of the nostrils.

127
00:17:03,320 --> 00:17:10,120
The air can see now by sight and touch. It will become clearer when we come to the description

128
00:17:10,120 --> 00:17:16,120
of how to practice air can see now. Sometimes you look at something moving,

129
00:17:16,120 --> 00:17:28,600
say, a branch of three or a banner in the wind and you practice air casino on that.

130
00:17:28,600 --> 00:17:35,240
But sometimes, the wind is blowing against your body and you have the feeling of touch here.

131
00:17:35,240 --> 00:17:40,360
And so, you concentrate on this and concentrate on the air element here.

132
00:17:40,360 --> 00:17:48,040
And so, in that case, you practice by the sense of touch. The remaining 18 by hearsay,

133
00:17:48,040 --> 00:17:55,960
that means just by hearing. The divine abiding of equanimity and the four immaterial states are

134
00:17:55,960 --> 00:18:04,280
not every handable by a beginner. So, you cannot practice obica and the four arugard,

135
00:18:04,280 --> 00:18:12,120
ojrajana, at the beginning. Because in order to get arugard, ojrajana, you must have,

136
00:18:12,120 --> 00:18:20,600
you must have got the five group of ojrajana. And in order to practice real upica,

137
00:18:22,680 --> 00:18:31,320
upica, brahma, we are. Then you have to practice the first three, loving kindness,

138
00:18:31,320 --> 00:18:43,080
compassion, and sympathetic joy. So, as a real divine abiding equanimity cannot be practiced at the

139
00:18:43,080 --> 00:18:48,280
beginning. Only after you have practiced the other three, can you practice equanimity?

140
00:18:48,280 --> 00:19:01,240
And then as to condition, the ten casinos, I mean nine casinos, omitting the space casino

141
00:19:01,240 --> 00:19:07,400
conditions for immaterial states. That means if you want to get arugard, ojrajana,

142
00:19:07,400 --> 00:19:20,120
and you practice one of the nine casinos, omitting the space casino. Because you have to practice

143
00:19:20,120 --> 00:19:28,680
the removing of the casino object and get the space. So, space cannot be removed, space is space.

144
00:19:28,680 --> 00:19:37,480
So, space casino is exempted from those that are conditioned for immaterial states or

145
00:19:37,480 --> 00:19:42,520
arugard, ojrajana. The ten casinos are conditioned for the kinds of direct knowledge.

146
00:19:45,160 --> 00:19:50,920
So, if you want to get the direct knowledge, or Abbina, that means supernormal power,

147
00:19:50,920 --> 00:19:59,160
then you practice first the one of the ten casinos. And actually, if you want to get different

148
00:20:00,360 --> 00:20:09,640
different results, then you practice different casinos.

149
00:20:09,640 --> 00:20:20,840
You know, if you want to shake something, suppose you want to shake the

150
00:20:23,400 --> 00:20:30,920
city hall building by your supernormal power, then first you must practice

151
00:20:30,920 --> 00:20:41,320
what a casino, but not the other casino. If you practice up casino and try to shake it,

152
00:20:41,320 --> 00:20:49,160
it will not shake. There is a story of a novice who went up to heaven, I mean a vote of course,

153
00:20:49,720 --> 00:20:56,840
and he said, I will shake your mention, and he tried to shake it and he could not.

154
00:20:56,840 --> 00:21:05,640
So, the celestial names make fun of him, and so he was ashamed and he went back to his

155
00:21:05,640 --> 00:21:12,440
teacher, and he told his teacher that he was ashamed by the names, because he could not shake the

156
00:21:12,440 --> 00:21:20,280
shake their mention, and he asked his teacher why. So, his teacher said, look at something there,

157
00:21:20,280 --> 00:21:28,760
so a cow done was floating in the river, so he got the hint, and so next time he went,

158
00:21:29,320 --> 00:21:36,120
he went back to the celestial airport, and this time he practiced water, I mean,

159
00:21:36,120 --> 00:21:47,800
water casino first, and then maybe mention shake. So, according to what you want from the casinos,

160
00:21:47,800 --> 00:21:52,200
you have to practice different kinds of casinos, and we have to mention in the little chapters.

161
00:21:54,920 --> 00:21:59,960
So, the tank and the casino are conditioned for the kinds of direct knowledge, three

162
00:21:59,960 --> 00:22:05,080
divine abidings and conditions for the fourth divine abidings. So, the fourth divine abidings cannot

163
00:22:05,080 --> 00:22:11,720
be practiced at the beginning, but only after the first three. Each lower and material state is

164
00:22:11,720 --> 00:22:16,440
a condition for each higher one. The base consisting of neither perception or non-possession

165
00:22:16,440 --> 00:22:22,280
is a condition for the attainment of suggestion, that means, a decision of mental activities,

166
00:22:23,240 --> 00:22:32,280
a decision of perception and feeling, actually, a suggestion of mental activities.

167
00:22:33,400 --> 00:22:39,400
All our conditions are living in bliss, that means living in bliss in this very life, for insight,

168
00:22:39,400 --> 00:22:45,160
and for the fortunate kinds of becoming, that means, for a good life in the future,

169
00:22:46,920 --> 00:22:52,040
as to suitability to temperament from their importance. Here, the existence should be understood

170
00:22:52,040 --> 00:23:00,760
according to what is suitable to the temperament, and then describe which subjects of meditation

171
00:23:00,760 --> 00:23:11,720
are suitable for which kind of temperaments. So, three divine abidings and conditions for the fourth.

172
00:23:11,720 --> 00:23:18,200
Yes. Does that mean that you can study the practice of third before the first,

173
00:23:18,200 --> 00:23:24,840
or practice the second before the first? If it doesn't say that you have to practice all of them at the third.

174
00:23:24,840 --> 00:23:38,120
Right. But the normal procedure is to practice the loving kind as first, and then practice

175
00:23:38,920 --> 00:23:47,640
the second one and the third one. This means you practice so that you get Jana from this practice.

176
00:23:47,640 --> 00:23:56,600
If you do not get to the stage of Jana, then even a quantumity you can practice.

177
00:23:57,880 --> 00:24:09,480
But here, it is meant for Jana, because the a quantumity leads to the fifth Jana. So, in order to get

178
00:24:09,480 --> 00:24:16,440
the fifth Jana, you need to have the first, again, that and fourth Jana, and those can be obtained

179
00:24:16,440 --> 00:24:21,720
through the practice of the lower three on the other three divine abidings.

180
00:24:24,760 --> 00:24:29,160
You reach Jana, but not to the fifth Jana. You reach to the fourth Jana owner.

181
00:24:31,560 --> 00:24:35,800
And by the practice of equanimity, you reach the fifth Jana.

182
00:24:35,800 --> 00:24:49,320
And now, the different kinds of meditation, suitable for different types of temperaments.

183
00:24:50,840 --> 00:24:57,320
And all this has been stated in the form of direct opposition and complete suitability.

184
00:24:57,320 --> 00:25:06,200
That means if you say this meditation is suitable for this temperament, it means that this

185
00:25:06,200 --> 00:25:10,920
meditation is the direct opposite of that temperament, and it is very suitable for it.

186
00:25:11,640 --> 00:25:16,840
But it does not mean that you cannot practice other meditation.

187
00:25:16,840 --> 00:25:22,360
But there is actually no profitable development, that does not suppress, creed, etc.

188
00:25:22,360 --> 00:25:27,640
and help for it and so on. So, in fact, you can practice any meditation.

189
00:25:28,840 --> 00:25:35,320
But here, the meditations, subjects of meditation and temperaments are given

190
00:25:36,120 --> 00:25:43,480
to show that they are the direct opposite of the temperament.

191
00:25:43,480 --> 00:25:55,000
And they are the most suitable, I am of diluted temperament, then the most suitable

192
00:25:56,200 --> 00:26:02,680
meditation for me is breathing meditation. But that does not mean that I cannot practice

193
00:26:02,680 --> 00:26:11,080
any other meditation, because any meditation will help me to suppress mental defilements

194
00:26:11,080 --> 00:26:15,960
and to develop wholesome mental states.

195
00:26:19,080 --> 00:26:25,800
Even in the sootas, Buddha was advising Rahula and other persons to practice

196
00:26:25,800 --> 00:26:30,040
different kinds of meditation, not only one meditation, but different kinds of meditation,

197
00:26:30,040 --> 00:26:36,200
to one and the only person. So, any meditation can be practiced by anyone.

198
00:26:36,200 --> 00:26:43,480
But if you want to get the best out of it, then you choose the one which is most suitable

199
00:26:43,480 --> 00:26:44,520
for your temperament.

200
00:26:46,920 --> 00:26:54,280
All kasiras, kasiras, you apply seeing consciousness before,

201
00:26:54,280 --> 00:27:07,240
all right? Yes, kasirasiras should be practiced first by looking at them.

202
00:27:07,800 --> 00:27:11,400
So, when you look at them, then you have to be seeing consciousness.

203
00:27:11,960 --> 00:27:19,560
And then you try to memorize or you try to take it into your mind. That means you close your eyes

204
00:27:19,560 --> 00:27:26,280
and try to take that off the image. So, when you can get the image clearly in your mind,

205
00:27:26,280 --> 00:27:33,160
then you are said to get the, what sign?

206
00:27:35,880 --> 00:27:40,680
Learning, we call learning sign, actually the cross sign.

207
00:27:40,680 --> 00:27:51,000
And after you get the learning sign and you don't, you no longer need the actual,

208
00:27:51,960 --> 00:28:00,040
actual disc, actual object. But you develop, you dwell on the, on the sign,

209
00:28:00,920 --> 00:28:07,960
you get in your mind. So, at that moment, at that time on, it is not seeing consciousness.

210
00:28:07,960 --> 00:28:18,840
But the other nonwara, I mean, through mind or not true, not true, I do all.

211
00:28:19,880 --> 00:28:27,560
So, first, through I do all, you look at the, the kasira and practice meditation. And after you get

212
00:28:27,560 --> 00:28:36,040
the learning sign, then your meditation is through mind or you see through mind, but not through the

213
00:28:36,040 --> 00:28:49,160
eye. And then, dedicating oneself to the blessed one or to the teacher. That means giving

214
00:28:51,160 --> 00:28:57,640
relevant, pushing oneself. And to the, to the blessed one or to the voter, it's all right.

215
00:28:57,640 --> 00:29:12,360
But to the teacher, I do not recommend, because, not all the teachers have to be trusted.

216
00:29:15,560 --> 00:29:23,000
Considering what's happening these days. So, on the, on phase 119,

217
00:29:23,000 --> 00:29:29,400
say, when he dedicates himself to a teacher, I run and quiz this, my person to you. So,

218
00:29:29,400 --> 00:29:37,400
I give, I give myself up to you. It, it, it may be dangerous if, if a teacher has,

219
00:29:38,600 --> 00:29:45,240
what, what are your motives? So, it is better to, to give yourself through the

220
00:29:45,240 --> 00:29:55,320
voter, not to the teacher, these days. Okay. And then, sincere,

221
00:29:58,760 --> 00:30:03,480
inclination or resolution. I think they are not so difficult. Now,

222
00:30:07,320 --> 00:30:14,920
in paragraph 128, about four lines, for it is one of such sincere inclination, which

223
00:30:14,920 --> 00:30:20,920
arrives at one of the three kinds of enlightenment. That means enlightenment as a Buddha,

224
00:30:22,040 --> 00:30:29,640
enlightenment as a pachigabura, enlightenment as an at a hand. So, these are three kinds of enlightenment.

225
00:30:31,240 --> 00:30:35,640
And then, six kinds of inclination lead to the maturing of the enlightenment of the body

226
00:30:35,640 --> 00:30:47,480
satras. This may be something like a parameters fall in Mahayana. This is non-grids, non-delution,

227
00:30:47,480 --> 00:30:54,360
and so on. These are the six inclinations. I mean, six, I mean, six qualities, which was

228
00:30:54,360 --> 00:31:03,720
a body satras, especially develop. So, with the inclination to non-grids, body satras see the fruit,

229
00:31:03,720 --> 00:31:09,640
for, for, for the fourth, in grief. With the inclination to non-hate, body satras see the fourth,

230
00:31:09,640 --> 00:31:18,840
in hate, and so on. They are not found, although it is a quotation, we cannot trace this

231
00:31:18,840 --> 00:31:25,640
quotation to any, any text available nowadays. So, some, some text may have been lost.

232
00:31:25,640 --> 00:31:33,960
Or it may refer to some, some other, some soul does not belong into Theravara.

233
00:31:37,960 --> 00:31:44,600
And then, the last paragraph, one that is two, apprehend the sign. Now,

234
00:31:46,760 --> 00:31:51,800
in, in Parli, the, what, Nimita is used in different, different meanings. So, here,

235
00:31:51,800 --> 00:31:57,960
apprehend the sign means just paying, paying close attention to what you hear from the teacher.

236
00:31:57,960 --> 00:32:02,760
So, this is the visiting clause, this is the subsequent clause, this is the meaning,

237
00:32:02,760 --> 00:32:08,040
this is the intention, this is the simile and so on. So, paying close attention and trying to

238
00:32:08,040 --> 00:32:15,080
understand, first, hear the words of the, the, the teacher, and then trying to understand it is

239
00:32:15,080 --> 00:32:22,120
called here, apprehending the sign. So, it is not like apprehending the sign when you practice meditation.

240
00:32:22,120 --> 00:32:28,840
So, apprehending or sign will come later in chapter, chapter 4. So, here, apprehending the sign

241
00:32:28,840 --> 00:32:38,440
means just paying close attention to, to what the teacher said. So, this is visiting clause,

242
00:32:38,440 --> 00:32:43,240
this is subsequent clause, this is, it's meaning and so on. When he listens attentively,

243
00:32:43,240 --> 00:32:50,040
apprehending the sign in this way, his meditation subject is well apprehended. So, he knows, he knows

244
00:32:50,040 --> 00:32:57,720
what he, what he should know about meditation. And, and because of that he successfully attains

245
00:32:57,720 --> 00:33:06,520
distinction, attains distinction means attains, genres, attains, supernormal knowledge and attains

246
00:33:06,520 --> 00:33:18,040
enlightenment, but not others, not otherwise, but not others. He will successfully attain

247
00:33:18,040 --> 00:33:24,520
his thinking, distinction, but not others who do not listen attentively and apprehend the

248
00:33:24,520 --> 00:33:33,960
sign and so on. So, otherwise should be corrected to others, not otherwise, on page 101, 101, 101,

249
00:33:33,960 --> 00:33:44,120
first line. He successfully attains distinction, but not others, on this page here.

250
00:33:47,000 --> 00:33:53,240
That is the end of that chapter. So, we are still preparing.

251
00:33:53,240 --> 00:34:10,040
Preparing is not, not over yet. You have to, you have to avoid 18, 40 monasteries and

252
00:34:10,040 --> 00:34:26,280
find a suitable place for meditation. It's interesting about the parmitas. It almost works.

253
00:34:27,400 --> 00:34:32,600
It works for four of them and then the two, it's a little bit of a stretch, but it's interesting.

254
00:34:32,600 --> 00:34:47,160
Thank you very much. We're going to continue in March, sometime in March. We'll do another 12

255
00:34:47,160 --> 00:35:02,840
a week and that will go from, we hope, chapter 4, chapter 8, chapter 3, through chapter 3.

