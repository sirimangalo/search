Good evening, everyone, broadcasting live Friday the 13th, Friday May 13th, today's quote
It's about good things, good things that are not good, there are things in this world
that we think are good, they turn out to be dangerous, dar uno, dar una means wicked or harsh
cruel.
What is it in this world that is harsh and cruel, dangerous?
What is his three things, lamba, game, sakara, praise, and see the sea loca, fame, sakara is
honor when we went through this, sakara is when people hold the esteem you, sea loca
is fame, lamba is the game, these things are very bad, not the sort of things you think
of is bad, it's problematic, why are these considered to be bad, but as I said, some people
I can see in their mind, they would not tell a lie, they would not knowingly tell a lie
from gold or jewels, golden ball, golden ball filled with pieces of silver, no, maybe, right.
There are people who would not normally do evil things, they don't think they're naturally
pure, this is the way of worldly goodness, you see people who are just naturally wholesome
and good and kind.
And then he says, by that same person I've seen telling lies, because his heart was obsessed
by gains honor and fame, though I've been corrupted, and there are like asops, fables
about that sort of thing, someone becomes, you know, the classic story or someone loses
sight, with a sight of what's good, becomes blinded by gains honor and fame.
Good things blind us, there's a danger in too much, undeserved, but often talked about
undeserved, when someone isn't capable, or isn't ready, or isn't worthy of gain, praise,
fame, or anything else good, you know, it's another reason why nature is such a useful
environment to meditate in, because it's quite harsh, so it forces you to be patient
and present, it's easy when things become, when we live in the world like in Asia, when
we went to meditate, it could be quite cold, and it's a colder than it is here now, but
here we turn on the heat when it gets below 20 degrees, when we have hot water showers,
we couldn't imagine taking a cold water shower, this kind of thing, it's easy to become
complacent and lazy, when you actually live in nature, if you've ever lived in nature,
it forces you to be content, to accept, to let go, because you can't run away, if you
don't have a hot water shower, then you have to take cold water shower, you know, if you're
live in the forest, you don't even have a hot water, a cold water shower, you have to bathe
in the river, or not bathe at all for days, so why we go on arms round, because it's
a way of being content, you know, with whatever food there is easily and freely available
for charity, bringing ourselves from the complacency of choosing and getting what we want
all the time, so why we sleep on the floor, it's why we give up money and give up luxury,
because these things are dangerous, where all the good things are problematic, and this
is another reason why many people are unable to progress and the practice, are unable
to even think of meditation, practice as a positive thing, there's many people in this
world who couldn't imagine or reason for meditating, right, why, why would you waste your
time, you could be enjoying life, right, because these people enjoy life, they have pleasant
lives and they have good, they have gains that other people do not, there are people who
think that all you have to do is think positively and good things come to you, and there's
something to not being depressed and negative, but it's also quite clear that some people
just have better luck and better situation than others, and they become blind and forgetful.
There's the story of Sakka, the King of the Gods, and became a Sotabana, so he really
understood the Dhamma, but then one day Mughalana went to check on him and found that
he was complacent, he had this beautiful palace and he showed Mughalana this palace
and Mughalana used his magical powers to shake the palace up with an earthquake, and Sakka
became afraid and he said, you've become complacent, right, and this gain of you, it's
gain of yours as an angel, these good things, we're like angels in many ways, we have such
luxury in this world, we just have cooked food, all the things that we have that we take
for granted, cooked food, soft clothes, that we can actually wear cotton on our bodies
to ward off heat and cold, that we have walls and a roof to keep out the insects and
the elements that we have medicine, so we don't have to put up with our illness, we can
just, whenever we're sick, for whatever reason we can take a pill and it goes away, very
easy to get lost in these things and not realize the limited nature of our luxury and
our contentment, it's easy to be content when things are good, when you have happy things
come to you, we practice meditation to be ready for anything, to be invincible, it's
not a reason to be proud that you can deal with, that you can enjoy life, so you can
do it.
It's not a reason to be proud of when you're famous or when you're rich or when you're
honored by others, something to be concerned about, now I will become complacent, these
good things will make me forget about the potential for bad things, when the bad things
come out completely unprepared, very important part aspect of Buddhism is our relationship
with worldly good things, it's a very important point in leading us to take the meditation
practice seriously, it's our relationship with worldly happiness, worldly pleasure, worldly
good things, the worldly dumbness and reminding ourselves that these are not permanent,
that the good and the bad are unpredictable, unreliable and sustainable, okay, anyway that's
our dhamma for this evening, if we have any questions, good morning, thanks for the broken
gong teaching, glad you appreciated it, just to remind her I asked a couple of questions,
well I don't see them, if they're before last night then I think they're lost.
What should you do when other people's suffering spills onto you, it's interesting how
you say spills onto you, you might have to give me an example but I can talk about some
of the ways it spills onto you, no, when you feel sympathetic for other people's suffering
is one way or when people get you caught up in their own problems is another way but
you can't actually, suffering can't actually spread, suffering is one thing that can't
spread, you can't suffer because someone else is suffering, it's very easy to suffer
when someone else is suffering with these reasons, right, someone gets in trouble, suppose
someone accumulates gambling debts or something and then their whole family suffers,
suppose someone is very ill, then their whole family suffers for another reason from sadness,
from loss, people taking problems out on you, right, so suppose someone is, apparently when
people are ill with cancer they can be very hard to deal with rather than becoming
sober they become more intoxicated or more caught up in themselves, more self-absorbed
and crazy, you know, become very self-centered, me, me, me, poor me, something like that
and it causes problems for other people or they're angry at you for not appreciating
them that kind of thing, but not appreciating their problems and you have to deal with it
is with everything else, with mindfulness, don't let other people see we're very connected
with others, we react very quickly to other people's problems, so it's a reason why we go on what
we call retreat, right, you retreat because being around people all day is tough, you know,
being around worldly people is very hard on your mind, very difficult for your meditation practice,
I mean you could call it the advanced practice, right, when you actually have to deal with people
meditating here in the monastery, it's a training, like boxing in a boxing gym, you know,
you go to the gym when you train, but when you step into the ring it gets real and you have to put
it all to use. Feeling sad for other people is a big problem, I think. People think compassion
is feeling sad for others, it's not really, it shouldn't be moved by other people's suffering,
not in that way, sorry, that's maybe wrong because you shouldn't be moved, but moved to help,
not moved to sadness, you shouldn't be upset by it, so it doesn't mean not in that sense,
it shouldn't be sad because of other people's suffering, and that's people will often use that
sort of thing to their advantage, pour me and they want everyone to feel sad for them and then it
drains everyone else and it doesn't actually help anyone, right, but you should help others,
and in fact you can help others, because of your contentment through the meditation practice,
when you have fewer needs of your own, you're much better able to help others,
you talk about other people abusing that in terms of using you and expecting you to help them,
I think for enlightened being there's a certain sense of willingness to let people use you,
certain willingness to help others and to not worry so much about yourself because
you don't get upset about things, you can be used and it doesn't bother you,
you don't mind when other people want you to help them, it's just easier to go along with
them so you just do it, and you can do things that most you're stronger, in fact invincible,
it's an enlightened being in our hunk, nothing bothers them, so they're able to help and work
for the benefit of others, untirely, untirely, so those are just some thoughts on that,
ultimately you have to be mindful and it's going to depend on the situation,
and depend on your own abilities, you shouldn't pretend that you're enlightened and try to act
like an hour a hunt, you should sometimes protect yourself and sometimes you have to retreat,
is eating meat considered killing, no it's not, you know, eating meat is considered eating,
chewing, nothing dies when you eat meat,
have all the Buddhas been men in born in India, apparently so,
but not just men there, it's a very special being, a Buddha is born as a very special being,
but yes is male, doesn't mean that women are inferior, although there is that kind of a
connotation, unfortunately I don't know what to say about that, maybe it's not true, maybe
there are women Buddhist, but there's a sense that the Buddha form is male, it's masculine,
the same goes for tomorrow, which is the evil angel, it's also a male form,
so it's a sense I think of the male being more powerful and I know that's kind of sexist or it's
outright sexist, so I don't know what to say about that, maybe it's not true, it wouldn't put too
much stock in it, it's not like it's an important teaching or anything, but yeah curiously they
seem, according to the commentaries, to have been all male born in India, and India seems to come
again and again, each universe has an India apparently, take that as you will,
in your booklet under the section on sitting practice, you're right, one should investigate
sensations other than the breath, after some minutes, what if the mind stays easily with the breath,
should one stick with the breath or pick other sensations, if the mind is with the breath,
then with the stomach, you focus on the stomach rise involved, but as soon as pain arises,
focus on the pain, as soon as happiness arises, focus on happiness, as soon as calm arises,
focus on the calm, maybe the book is poorly written, I don't know, shouldn't say you should look
for feelings, but if there's feelings, you should focus on them, because that means your mind
is already no longer on the stomach.
Yes, he actually shook the palace using a single toe, someone who knows their texts,
he touched it, so Sakha look, he said, look at this beautiful majestic palace and it's
blunder and it's magnificent, it's awesomeness, and Mogulana touched it with his big toe and
it shook as though it was going to fall apart, and Sakha was horrified and said,
maybe this grand palace of his is about to collapse, and Mogulana shook his head, and he
are being negligent, this is not permanent, this is not stable, this is not awesome, this is nothing.
It's the broken gong analogy used for people with preconceptions about you or a situation that
happened, the broken gong is when you don't react, it means don't react, that's all, it's quite
simple, I wouldn't read too much into it, it shouldn't be choose when to say no that is wrong,
guess that would be right at the time, I mean you wouldn't use the exclamation points, I think
it's the point, you wouldn't be upset about it, but you might say that's wrong,
but the broken gong means there's something missing, a gong is unable to ring,
that's actually kind of a negative thing, right, we think of a gong ringing as a good thing,
but no there's something missing from the mind of an enlightened being, they might say no,
that's wrong, but there's no upset, there's no reaction, what do the incentive for its continuance
via factory farming and consuming it, yes, but that's not immoral, that doesn't make it immoral,
it may make it immoral in a intellectual sense, but eating meat is just chewing, chewing,
chewing, nothing dies as you do it, and that's where morality is in Buddhism, because otherwise
you can never win, you can never be truly moral, because you don't know whether the food you're
eating is pesticides that they use, you don't know, driving and you get on the bus and you pay a bus
token, well more simply you get in your car and you drive and you know your car is going to hit
insects, right, well then you're responsible for the killing of the insects, okay, so then you take
a bus, well you're still paying for the bus, you're contributing to the dead insects on the windshield,
etc, etc, you can never be truly moral if your morality is based on an intellectual concept of
this is supporting this, this is supporting that, it's not like that, morality is the intentional
or the abstention from intentional acts of direct harm, so if you tell someone, you know,
go kill me a chicken or something or kill me a fish, then you're not guilty of murder,
but you're guilty of telling someone to commit murder, which is still a very harmful thing,
it's the state of your mind, you know, you don't want to be killed and yet you act in a way to
with the intention to bring about death, you don't do that when you eat, you don't have the intention
to kill this thing, you also don't have an intention that people should go out and do it more,
go out and kill, kill more, I know people look at this as a yeah, but yeah, I don't, don't overthink
things, keep it simple, otherwise you get lost,
what is the benefit of not eating afternoon, well it's just a line in the sand, there's not
there's nothing magical about noon, but you, you know, you need a certain amount of energy for
the day, a certain amount of calories and nutrients for the day, you use those during the day,
so eating in the morning gives you that, it means what do I need to survive, I need food,
if I eat in the evening it's not very useful, but if I eat in the morning that helps me get
through the day and I live and I have energy, so we only eat in the morning for that reason and
the artificial line in the sand is dawn to midday, which is reasonable, doesn't mean it's immoral
to eat after midday, but it's a reasonable, a reasonable convention, I mean a lot of the rules are
just that, they're not necessary, they're just rules, and that's the thing is they're not
thou shalt not, and then this is evil there, I undertake not to, so there's things that people
undertake, you undertake them because they're useful, whoa, there's a big one, recently came off
a medication, causes delirium, any tips on fear about dying incorrectly to let go or concentrate or
observe during these overwhelming experiences, well make sure you don't have these things in
these states in your mind, right, it's fearsome, and there's no way around it if you die with
these evil in your heart, you're going to a bad place. I know in fear itself is a very unwholesome
state, so it's something that you have to deal with, you die with fear in your mind, that's
not a good thing, that's one of the prime reasons for going to a hellish state or a ghostly
state, not a good state, now ordinary fear won't send you to hell, but but intense fear can,
make you more afraid, right, oh my gosh, you said my fear is going to leave me to hell,
well good, you shouldn't see how silly you're being in the end, sorry, I don't mean to truly
realize it, but in the end it's just fear, you know me, we make more out of these things than they
are, that's really the problem, if you're mindful of your fear, then it's no longer has power over
you, my mindfulness meditation, how should you do it, what is the tips, well have you read my
booklet on how to meditate, I think you have, that's about it, read my booklet, learn how to meditate,
practice that way, will help you deal with such things as fear and anxiety and so on,
how long should you focus on your feelings, well and as long as they last is good, but if after a
long time they don't go away then you can ignore them and come back to the stomach rising and
falling, but you can stay with them, we're just trying to see them as they are, to see that they're
not under our control, trying to learn to be objective, it's more about our state of mind than
the actual experience, so whatever you focus on, trying to cultivate this objective experience,
it doesn't matter what you focus on, if the feelings always there, just stay with it,
good object, I have had some serious brain injuries in the past, would it affect my meditation,
potentially, potentially not, I wouldn't worry about it, do what you can, as best you can,
that's all you can do, we can only start where we are, we're not all going to become enlightened
in this lifetime, but we take a step in that direction, it becomes our direction, you know,
we can turn ourselves in the right direction, we'll begin on the right path,
you see so much more of it right before death than you realize you have, yeah, I'll bet,
I don't think I've ever been in a situation where I was thinking I was going to die,
I mean it was in a car accident once, that was kind of scary, and I remember doing rock climbing
and when I got to the top, I tightened myself in to be let down, and halfway down I realized
the knot that was holding me was just a single knot, I had mistighted it, that was kind of
freaky, it's 60 feet up, but realizing it was about to slip loose and fall to my death,
but no, I've never been in such a situation, but talking to people who have, they can imagine,
and as a meditator you can imagine because you get these feelings, these things come up,
that people talk about coming up when you die, and so I'm in a situation so great for preparing
you for death because you've been there, seen it all, you know, you've gone through the
dregs and the depths of your mind, have you ever read the book? I, the four agreements, no, I haven't.
There's a way to Sooka refer to Nibanda, some very good questions, very difficult ones,
I have no idea, a way to Taksooka, let's look up the word, I don't even know that word,
it sounds like, sorry, put this, and when he was asked, the amakai thing, no, the amakai, the
monkey amakai asked, sorry, put that, how is it that Nibanda can be happiness? There's no feeling,
there's no feeling, how can it be happiness, right? Nibanda said it's precisely because there's no
feeling that it's happening, so what's a way to die? No, I don't know how way to die, I don't,
it's not in the dictionary, but a way to die is unf, is felt, so a way to die, I don't know,
you have to give me context, I could search for it,
it's not even in the, it's not even in the, it's not even in the, the bitticus,
the nikai is, so there, maybe it's in the commentaries or something, you have to give me some
context, way did that means experienced, a way did that means you haven't experienced it, so
I don't think it refers to Nibanda, but it's a way the nia probably, or it's without way in the
nia, what are you doing, are you traveling, what are you doing, are you traveling again soon
with the robes, the robes are in Bangkok, we're going to pick them up when I get to Bangkok,
travel to Chiang Mai with them, I have to get a ticket to Chiang Mai somehow,
have we worked that out, it's Robin here, you have to work that out at some point,
the ticket to Chiang Mai, maybe I'll just take a bus, a bus is so much cheaper,
probably much more environmentally friendly as well, yeah, so I'll be leaving here May 31st
and I'll be back again July 1st, I think.
Wasanka you can search for it on the DPR, I can search for it and they can find it in the
commentaries maybe, way data means where does that, that's very common, where do we see that,
way data, way data, if it's very common, the word way data on this, the body uses that a lot,
I can't think off the top of my head, one of the main suit does that's the word way data,
way data boys should be experienced that kind of thing, weekly interviews during June,
probably, I'm going to try my best to have internet wherever I go, I guess I can't say for sure,
but I'll try my best, I'll have to work it out with Satanya and Bangkok, I don't know if I have
my SIM card here or if it's still working, I don't remember how that goes, I have a not a Thai
number, a Thai cell phone number which would give me internet there, Wi-Fi is still not that big of a
thing there and anyway I wouldn't have my own Wi-Fi work where I'm going, a lot of people use
mobile internet, okay enough, that was lots of questions, let's not overdo it, save some questions
for tomorrow, tomorrow I might, tomorrow I won't be here, I don't think, I'm going to give a talk
in Mississauga, at a funeral, or not even a funeral, some kind of memorial service which
seems somewhat unbutous to me, I'm not really convinced that memorial services are terribly
butous, dead is dead, the person's born again, move on, I don't know, not convinced, it seems to
me somehow clinging to the past, right, death is something that's already happened, you know, the
person moved on, why can't you, but not something I've thought deeply about, anyway it's a good
opportunity to teach the dumbness of whatever, anyway, so probably not be here tomorrow evening,
but we'll see, it could be, no, I won't be back in time, and I missed next, I'm going to
miss tomorrow, so have a good night, wishing you all the best, be well, you know, we've got video
up here, and I'll turn that off.
