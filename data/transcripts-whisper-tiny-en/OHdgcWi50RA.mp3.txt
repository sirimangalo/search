Okay, good evening everyone, welcome to our evening dumber.
Thank you all for coming out. It's always great to see such a turnout locally.
Everyone has come out of their rooms. After a hard days work, struggling with their own minds,
all of you in second life and on YouTube will come out to listen.
I think a full house here in second life on YouTube. My channel has surpassed 50,000 subscribers apparently.
I appreciate the effort people are taking to practice the Buddha's teaching. It's not easy.
Sometimes it seems like Buddhism is all about suffering, no?
Anyway, didn't the Buddha talk about suffering? Wasn't that something he'd like to talk about?
It's not very encouraging.
I think a lot of people, of course, look at Buddhism and are not satisfied with it.
When the surface talking a lot about suffering, and then when they begin to practice meditation,
a lot of suffering. What the heck, right?
This isn't what we want. What do we want? What does everyone want?
We want this happiness. Everyone wants happiness. No one wants suffering.
So just spend all our time talking about suffering. Rather unpleasant.
It's a kind of a recipe for unhappiness, wouldn't it?
The question might come up. Why didn't the Buddha talk more about happiness?
If he was so enlightened and so happy, was the Buddha happy?
If so, why didn't he talk about it more?
Is this a suspicion that perhaps the Buddha wasn't happy?
Perhaps he was just denying the existence or the potential for happiness?
Why did he phrase it in a negative?
It's a question and it's led Buddha's teachers to try and reframe the four noble truths as happiness.
Happiness, the cause of happiness, the cessation of happiness, and the path which leads to the cessation of happiness.
And so to explain it in exactly the opposite way.
It's fine, but the question remains why didn't the Buddha do that? Why didn't the Buddha talk like that?
It comes down, I think. A big problem is the question of course what is happiness?
What do we mean when we say happiness?
We certainly don't mean physical pleasure, is that what we mean by happiness?
I think most of us have gotten beyond that.
That's children, we seek out the pleasure of ice cream, the pleasure of bright colors,
shiny objects, toys, games, tickling, snuggling, hugging, kissing.
The adults actually fall into that as well, but sensual happiness.
And I think for the most part adults come to see beyond that.
So as you grow up, you realize it's actually really just a recipe for disappointment, through addiction, right?
When children become addicted to pleasure, they're ultimately disappointed when they can't get what they want.
They throw a tantrum and they suffer a lot.
When they start to see them, maybe not consciously, but certainly through practice and through experience,
they see the nature of sensuality as being a mix, an inevitable mix of pleasure and pain.
You can't really have one without the other, because being partial to something is having expectations about it.
It's day and of course nothing lasts forever and when things eventually change, there's disappointment, there's pain, there's stress, there's suffering.
So beyond that, I think generally people will look at more refined forms of happiness.
And even though it's very hard to give up the addiction, essential pleasure, we tend to see that.
It might be fun, but it can't be considered true happiness.
And so we look beyond that to things like power, power, soap, or rebal, soap.
The calm is so kind, it's not the way.
But when one power, soap, and rebal, it's a good meaning, when about states are being
able to look at us sitting in this room.
How happy we are, how wonderful is this.
We have a roof over our heads, good company, good food.
I don't know how good the food is, but frozen food, warmth, our ability to sit here, together, and peace, not have to fight.
There's a Baba Sukhoi Baba soap that we have.
States are being, when you get a new job, for example.
It's funny, because why would you celebrate that?
It means you have to go work, but the idea of having a new job, the concept of it is a great thing.
Everyone's congratulatory.
There's nothing to do with sensuality, because essentially speaking it's a lot, you're just suffering because you have to do the work.
But conceptually, it's a great thing, because it ultimately allows you more sensual pleasure, potentially, or it allows you freedom from pain.
The concept of getting a job is a great happiness.
Having a family, someone sent me a...
I don't really want to tease this person too much, but they sent me an email.
Because they wanted to let me know of the wonderful occasion of having a child.
The way they expressed it was, they wanted to share the great moment with me.
I thought it was a...
From a Buddhist perspective, it's not really something we congratulate people.
When the Buddha left home, he found out he was a father.
And he's a rau-long jata.
A fetter has been born.
And the king heard this and the king said, well, let that be his name.
Kind of out of spite, it seems, but that's what he call it.
So it's a son ended up being called rau-la, which means a bond or a bind.
Because, before he left the Buddha, he sent us a rau-long jata.
But, you know, this is it.
When things happen, they please us.
This is what life is all about.
The juada vivka, for example, is kind of taking a walk in the forest.
It's not really the centrality per se.
It's the peace.
When you even say the vivka, the vivka is not having to deal with all the stress of life.
If you go to work, you go to school, you deal with your family.
And it's all stress and suffering.
And so you leave.
And just the freedom from all of that stress.
That's happiness.
And there are other ways of looking at happiness.
Some people are optimistic.
And they think optimism, positivity, is happiness.
So as long as you see the good in everything, it's an example, I think.
Polyanna is a bit of an example of that.
When there was this movie, life is wonderful, I think, when I was in university,
a long time ago.
About the Holocaust, and this Jewish man, and his son, and he looked at everything positively.
And as a result, good things happened to him.
But eventually he was killed in a concentration camp.
But up to the end, he had a positive attitude.
So we look at this kind of thing and we say, well, that's it.
Well, this is, I think, how neuroscientists tend to talk about happiness.
And that some people are more capable of being happy.
Because of their positive outlook.
And so there was this one researcher who was talking about how, through meditation,
you can become more positive.
And you can retrain your brain, to be more happy.
So a result of being more positive and rebounding from negative experience as quicker and so on.
I think that gets closer to Buddhist concept.
But of course, positivity has its problems as well.
It's less likely to lead to disappointment, but it's still likely to lead one to complacency.
In the sense that one suffers, but one doesn't take it seriously, and one refuses to.
It doesn't engage in dwelling on it.
At the same time, it doesn't engage in fixing it.
It doesn't free oneself.
So it keeps coming. The suffering does come back. It's just it doesn't last long.
So it's arguably superior to the other sort of happiness.
Moving on in Buddhism, the other type of happiness,
sort of in a category of its own, is the happiness of meditation.
It doesn't be together with this idea of positivity, but it's not exactly.
It's a change in mind state to be sure.
It's a change in mind state to the extent that one experience constant and prolonged states that are pleasurable are happy.
So like the first John, the first trans of tranquility meditation, is a company with happiness.
It's a kind of happiness that's removed from addiction.
You can't actually be addicted to the happiness, because the mind isn't cultivating addiction.
There's no it's so focused, but there's no room for clinging to it.
It's so powerful that the mind doesn't develop the addiction.
It's freeing, it's liberating in that sense.
And so often people who practice this will describe it as being a liberation or being a liberation.
It is possible to become attached to the idea of the John,
and those of them when you leave them behind want them again.
And so ultimately the Buddha noted about this type of happiness.
He said, well that's the pinnacle really of any kind of feeling, but it's still impermanent.
When you leave it, you can still be left wanting more.
You think about that, and you compare it to what you have when you're out at the John end.
You want to chat them back, but they're not permanent.
And ultimately this is the real problem with any of the types of happiness,
anything that we could call happiness, beyond the ultimate happiness,
which we're personally going to get to.
Is that, it doesn't last.
You can say, I've got the best life.
You know, this is working for me.
People who have good jobs, good cards, even while you start at the bottom,
people who have sensual height, the pinnacle of sensual pleasure, angels,
for example, human beings who are rich, powerful.
The drug addicts even will think for a time that they are in heaven,
but it doesn't last when you have a good job, a good status,
security, you feel secure.
It doesn't last.
People who have positive thoughts, if you're mind, if your brain is wired,
such that you are able to be positive about everything,
to be happy, even in the face of great suffering.
Even that doesn't last.
There's no reason to think that that state is going to be permanent.
The positivity leads.
That it's stable.
In fact, what we find through wisdom,
meaning as we truly understand the nature of all of this,
reality, happiness and suffering,
as that freedom from suffering is far preferable to any of these things.
And so we have to talk about technically and happiness as being a negative thing.
The negative in the sense of being free from something,
as opposed to gaining something.
It's not being free from anyone experience.
It's not like we bother so aware when something goes away,
you feel happy, all good, it's gone.
What a relief.
And that's a key point because that people misunderstand that freedom from suffering,
the happiness is because, oh, good, I'm not suffering anymore, but it's not.
It's not a relief.
It's not a feeling of relief.
Because the things that caused you happiness and caused you suffering before are still there.
The change is that you're no longer seeking them.
You're no longer disturbed by them.
You're no longer subject to their control.
So there's the happiness of freedom.
The happiness of being in a state or of gaining the knowledge,
the understanding that nothing is worth clinging to.
Basically, there is nothing that can make you happy.
It's a kind of happiness that goes beyond any type of happiness.
It's not dependent on anything.
It's not subject to change.
It's not vulnerable.
It's invincible.
And beyond that, it is experienced as far preferable,
far superior, anyone who has attained the state of freedom.
No longer holds any delusion about sensuality or becoming,
or even non-becoming, even the genres.
The idea that they might be somehow ultimate or true happiness.
And so this is the Buddha actually said, quite clearly,
is that Nati-santi-parang sukham.
Which isn't a quote that we often bring up,
because it sounds kind of contentious,
because it's hard to understand this,
with all of our ideas of what is happiness.
Nati-santi-parang sukham means there's no happiness outside of peace.
Peace is the only sort of happiness.
Which is sensible.
It's reasonable, but it's hard to understand
because it's a negative happiness.
Peace is a sublime state.
It's not really familiar to most people.
It's not really even all that desirable for a lot of people.
Except in Maimiya, an abstract sense,
so I wish I had some peace of mind for example.
But peace is something quite sublime.
It was once asked of, I'm sorry, put that
under the monks, Yamakai, I think was his name.
He asks, sorry, put that.
If there's no way, then there's no way to Nati-mana.
How can you say it's happiness?
How can you say Nati-mana is happiness when there's no way
that Nati-mana means feeling.
So happiness is a type of feeling, right?
That's the idea of happy feelings.
Pleasant feelings, unpleasant feelings, neutral feelings.
If there's no feelings in Nati-mana,
if freedom has no feelings,
how can you say it's happy?
Right, this is, I think, a sticking point for a lot of people.
If there's a fear of the idea of Nibana,
have to give up the world.
But there's so much in the world that I want.
Actually, Nibana isn't about giving up the world.
It's about leaving it behind, and it's something
that can be experienced for a short time.
It's not something that's not an all or nothing thing
that's what, like, whoops, wait, let me back.
I want to go back.
Let me off, let me off.
Something experienced and you're able to see.
Is it worth it?
Is it not worth it?
Sorry, but does reply was,
it's precisely because there is no way
that Nibana is happiness.
Some blind are difficult to understand.
Let's get practice meditation.
Of course, as you practice,
you start to shed more and more of your attachments,
more and more of your desires.
As you see, the things that you thought were happiness
are not really happiness.
And to finally, you truly let go,
there's an epiphany, and the mind says,
oh, yes, I think it's worth clinging to,
and then poof the cessation.
Then you can answer this question
whether Nibana is happiness or not,
when you experience it for yourself.
So, there you go.
There's the dhamma for this evening.
I hope the audio didn't cut out.
Thank you all here locally for sitting patiently,
coming up to listen,
and through everyone at home,
wish you all a good practice,
and thank you for tuning in.
