1
00:00:00,000 --> 00:00:04,600
Since thoughts are the main distractors from the present moment during daily

2
00:00:04,600 --> 00:00:09,280
activities, why did the Buddha put an emphasis on watching the body postures

3
00:00:09,280 --> 00:00:13,400
when you walk, when you're walking, etc. It seems like watching my thoughts

4
00:00:13,400 --> 00:00:21,520
helps me catch them. But he didn't put any particular emphasis on watching the

5
00:00:21,520 --> 00:00:26,240
body bodily postures. For instance, saying that one is that it's better than

6
00:00:26,240 --> 00:00:34,880
watching the mind. But he put it first and I think we can infer from that

7
00:00:34,880 --> 00:00:40,320
something important that it's easier and more basic. The body is always there

8
00:00:40,320 --> 00:00:48,560
easy to find and so it's what most meditation teachers advise their

9
00:00:48,560 --> 00:00:56,360
students to focus on because it's most obvious and most easy to put your

10
00:00:56,360 --> 00:01:02,960
attention on, keep your attention on. Thoughts are fleeting and hard to catch

11
00:01:02,960 --> 00:01:10,280
especially for a beginner and so they are better relegated to a leader more

12
00:01:10,280 --> 00:01:16,320
advanced. You know, somewhat more advanced, like let's say, for example in

13
00:01:16,320 --> 00:01:21,600
an instruction of meditation, we would explain the body first and then the

14
00:01:21,600 --> 00:01:24,880
feelings and then the mind. So by the time we got to our explanation of the

15
00:01:24,880 --> 00:01:29,840
mind, the meditator already had some basic idea of how to be mindful of

16
00:01:29,840 --> 00:01:35,920
something quite coarse and obvious, which is the body watching the breath

17
00:01:35,920 --> 00:01:40,000
watching the seated position. That's much easier than having them watch the

18
00:01:40,000 --> 00:01:45,680
mind. So I'd say he didn't put an emphasis on the body postures, but he put

19
00:01:45,680 --> 00:01:52,320
them first and we shouldn't ignore that fact and when teaching the meditation

20
00:01:52,320 --> 00:02:01,680
to others or with ourselves when determining a base to come back to

21
00:02:01,680 --> 00:02:05,640
in her meditation, to start at in her meditation, we should find something

22
00:02:05,640 --> 00:02:09,440
bodily, something physical because it's more obvious and more easy to base

23
00:02:09,440 --> 00:02:14,080
ourselves on it. At any given time you can watch the foot or you can watch the

24
00:02:14,080 --> 00:02:18,920
stomach or you can watch the body in whatever posture that it's in. The mind

25
00:02:18,920 --> 00:02:23,120
is not like that. The mind is fleeting and you don't you can't say what is

26
00:02:23,120 --> 00:02:30,040
going, what thought is going to come next. It's unstable in a way that the body

27
00:02:30,040 --> 00:02:46,520
is is is is is is not is is more stable.

