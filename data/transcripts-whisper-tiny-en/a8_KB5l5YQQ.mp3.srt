1
00:00:00,000 --> 00:00:07,040
Suicide is a big problem among troops returning from the conflicts in Iraq and Afghanistan.

2
00:00:07,040 --> 00:00:11,000
Can be possible to help those who have experienced the horrors of war come to terms with

3
00:00:11,000 --> 00:00:12,000
their experience?

4
00:00:12,000 --> 00:00:17,000
Would it be too risky without a teacher's guidance?

5
00:00:17,000 --> 00:00:18,000
Okay.

6
00:00:18,000 --> 00:00:22,200
There's two issues that are interesting to me here.

7
00:00:22,200 --> 00:00:28,600
The first one is the question of whether meditation can be risky.

8
00:00:28,600 --> 00:00:35,000
It can, meditation can in certain instances trigger, and by meditation throughout this, I'm

9
00:00:35,000 --> 00:00:39,480
going to be talking only about the meditation I teach, okay, so we're not going to be clear

10
00:00:39,480 --> 00:00:44,000
that what I say doesn't apply to all meditations, for sure.

11
00:00:44,000 --> 00:00:50,760
Some meditations are extremely dangerous, and from my point of view, but specifically

12
00:00:50,760 --> 00:01:00,280
speaking about the meditation teaching that I practice, it is admitted that it can trigger

13
00:01:00,280 --> 00:01:10,360
certain dangerous mind states that are already there, that are lying dormant inside.

14
00:01:10,360 --> 00:01:22,880
But far and away, the most like, how do I say this, in the vast majority of cases, like

15
00:01:22,880 --> 00:01:28,360
99% of the time, meditation won't be dangerous at all.

16
00:01:28,360 --> 00:01:35,520
This meditation will only serve to help any condition across the board, so the question

17
00:01:35,520 --> 00:01:40,840
of danger almost never comes out in this meditation tradition.

18
00:01:40,840 --> 00:01:48,080
If it's a thought correctly, be practiced correctly, and see maybe understood correctly,

19
00:01:48,080 --> 00:01:50,960
which, but understanding of course informs her.

20
00:01:50,960 --> 00:01:55,560
Let's just say if it's understood by the meditator correctly, whether that comes from

21
00:01:55,560 --> 00:02:05,680
yourself, or it comes from a teacher, or from books, or whatever.

22
00:02:05,680 --> 00:02:11,320
The question of whether one can do it without a teacher is a little bit more interesting

23
00:02:11,320 --> 00:02:20,240
because obviously the most likely place to gain confidence and understanding in the

24
00:02:20,240 --> 00:02:24,840
practice is from a teacher, so if you don't have a teacher, where are you going to get

25
00:02:24,840 --> 00:02:25,840
this understanding?

26
00:02:25,840 --> 00:02:33,080
How do you make sure that your meditation is proper?

27
00:02:33,080 --> 00:02:40,080
I would still say, well, I mean, the biggest answer here, or the biggest point to be

28
00:02:40,080 --> 00:02:47,320
made here in this regard is that this meditation is incredibly easy to understand.

29
00:02:47,320 --> 00:02:52,120
There isn't much room for error.

30
00:02:52,120 --> 00:02:57,520
Basically, what I'm trying to say is it's not something you should worry about, risk,

31
00:02:57,520 --> 00:03:01,640
or maybe I shouldn't practice meditation because it's too risky.

32
00:03:01,640 --> 00:03:12,600
I think it's an invalid or it's an improper thing to suggest or to think.

33
00:03:12,600 --> 00:03:17,200
The meditation is innocuous, it's more or less harmless.

34
00:03:17,200 --> 00:03:19,040
It's not something that's going to hurt you.

35
00:03:19,040 --> 00:03:26,280
The worst thing that happens in this tradition is people get bored, turned off, afraid

36
00:03:26,280 --> 00:03:29,400
of the meditation, and stopped meditating.

37
00:03:29,400 --> 00:03:36,120
That's far more common than any sort of risk because you're not creating anything in

38
00:03:36,120 --> 00:03:37,120
this meditation.

39
00:03:37,120 --> 00:03:43,440
You're just looking at what's there, and if there's horrible, horrible things there,

40
00:03:43,440 --> 00:03:49,840
you just don't want to look at them, it doesn't happen that you get excited about looking at them.

41
00:03:49,840 --> 00:03:55,320
If there's no harmful things there, then it's easy, and the meditation proceeds smoothly.

42
00:03:55,320 --> 00:04:00,800
But the question of whether it's risky is not one that I would be interested in entertaining.

43
00:04:00,800 --> 00:04:06,800
Now, for people who are PTSD, this is the second interesting aspect of this question, is

44
00:04:06,800 --> 00:04:18,200
what is PTSD, and what happens when a person who has PTSD, practice is meditation?

45
00:04:18,200 --> 00:04:26,160
I think we have to broadly separate this into two categories of people.

46
00:04:26,160 --> 00:04:38,560
Those people who have experienced traumatic events passively, and those people who have

47
00:04:38,560 --> 00:04:51,280
been active participants in creating traumatic experiences, and being specific.

48
00:04:51,280 --> 00:04:57,040
Those people who have done horrible things themselves versus those people who have just

49
00:04:57,040 --> 00:04:59,000
experienced horrible things.

50
00:04:59,000 --> 00:05:00,880
From a Buddhist point of view, there's a big difference.

51
00:05:00,880 --> 00:05:13,520
Our claim is that it matters quite strongly whether you've done bad things or whether

52
00:05:13,520 --> 00:05:15,320
you've just had them happen to you.

53
00:05:15,320 --> 00:05:18,000
It may not seem like that.

54
00:05:18,000 --> 00:05:24,400
But first blush, it appears to be the same thing you are traumatized, either way.

55
00:05:24,400 --> 00:05:31,680
So that is how it feels, how it appears on the outside.

56
00:05:31,680 --> 00:05:37,520
What you'll find is that's really only the shell of the problem.

57
00:05:37,520 --> 00:05:38,520
That's fine.

58
00:05:38,520 --> 00:05:41,360
In the beginning, it's all going to look the same as someone who's experienced trauma,

59
00:05:41,360 --> 00:05:46,920
whether they were the one causing it, or whether they were the victim or the perpetrator.

60
00:05:46,920 --> 00:05:49,040
There's little difference on the surface.

61
00:05:49,040 --> 00:05:54,560
But once you dig in deep, and once the person begins to get the hang of the meditation,

62
00:05:54,560 --> 00:06:02,320
that's where the two paths, fear, apart to a great extreme.

63
00:06:02,320 --> 00:06:10,200
A person who has experienced great trauma, no matter how great, is quite capable of progressing

64
00:06:10,200 --> 00:06:19,440
in the meditation, of healing, and finding themselves, and finding balance, and becoming centered.

65
00:06:19,440 --> 00:06:24,520
A person who has done horrible things is far less likely to find that sort of balance

66
00:06:24,520 --> 00:06:25,520
and center.

67
00:06:25,520 --> 00:06:27,560
They have a corruption inside.

68
00:06:27,560 --> 00:06:31,600
They've been hurt deeply by their own actions.

69
00:06:31,600 --> 00:06:44,440
The guilt that a person who's actually done bad deeds feels is far more real visceral intent

70
00:06:44,440 --> 00:06:51,200
than a victim who feels guilty or someone who feels guilty for whatever other reason.

71
00:06:51,200 --> 00:06:59,080
I had a woman who was raped by her father when she was a child, and she came to me when

72
00:06:59,080 --> 00:07:03,960
she was 40, and she looked like a ghost, and I've told this story before, but she was

73
00:07:03,960 --> 00:07:07,720
one of my greatest success stories, and I didn't do anything.

74
00:07:07,720 --> 00:07:10,160
I never had no idea what had happened to her.

75
00:07:10,160 --> 00:07:14,600
I just knew that she came and she looked like a ghost, and she had something she wanted

76
00:07:14,600 --> 00:07:15,600
to get off her chest.

77
00:07:15,600 --> 00:07:20,760
I mean, I guess she'd gone to therapists, and the idea was to always tell, and they would

78
00:07:20,760 --> 00:07:22,000
always ask what happened.

79
00:07:22,000 --> 00:07:23,000
I didn't ask.

80
00:07:23,000 --> 00:07:24,000
I wasn't interested.

81
00:07:24,000 --> 00:07:27,880
I kept her in the present moment, and when she was crying, I said, just be mindful of crying.

82
00:07:27,880 --> 00:07:30,200
It doesn't really matter why you're crying.

83
00:07:30,200 --> 00:07:34,680
Be aware of the sadness, be aware of what's happening now.

84
00:07:34,680 --> 00:07:40,240
Anyway, long story short after a couple of weeks, she looked like a different person.

85
00:07:40,240 --> 00:07:45,080
She left, she was smiling, and totally relieved.

86
00:07:45,080 --> 00:07:48,160
She came back, and she had color in her face.

87
00:07:48,160 --> 00:07:49,160
She looked human again.

88
00:07:49,160 --> 00:07:52,560
It was really, she looked like a ghost when she first, she didn't look human when she

89
00:07:52,560 --> 00:07:56,120
first came in.

90
00:07:56,120 --> 00:08:00,960
And she left, she looked alive.

91
00:08:00,960 --> 00:08:10,040
A person who has committed bad deeds, there are cases where a person can make a mistake

92
00:08:10,040 --> 00:08:11,800
and realize their mistake.

93
00:08:11,800 --> 00:08:21,880
But a person who has systematically and intentionally engaged in warfare, killing other people,

94
00:08:21,880 --> 00:08:24,160
there's something deeper there that you won't see at first.

95
00:08:24,160 --> 00:08:26,160
It's not readily apparent.

96
00:08:26,160 --> 00:08:36,160
The person might seem functional in society, but there is something that outer layers peeled

97
00:08:36,160 --> 00:08:38,680
off.

98
00:08:38,680 --> 00:08:39,680
You see the poison.

99
00:08:39,680 --> 00:08:42,240
You can see the corruption in their mind, and that's much more difficult.

100
00:08:42,240 --> 00:08:46,440
It can be healed, of course, but it's much more difficult.

101
00:08:46,440 --> 00:08:52,400
Now, these people are the ones for whom it is dangerous no matter what, and even with

102
00:08:52,400 --> 00:08:54,960
the teacher, the teacher has to be careful.

103
00:08:54,960 --> 00:09:09,160
Because these people are just acceptable to exploding, wreaking havoc on the meditation,

104
00:09:09,160 --> 00:09:12,880
on meditators, and the meditation center, and attacking the meditation teacher.

105
00:09:12,880 --> 00:09:19,920
I mean, there are stories of this, where a meditator has some commit suicide.

106
00:09:19,920 --> 00:09:27,080
These are the problems, so I don't have a quick solution for that one, and I think we

107
00:09:27,080 --> 00:09:30,720
have to separate the two out.

108
00:09:30,720 --> 00:09:41,240
The best thing that comes from that, I think, is a cautionary warning to people, anybody

109
00:09:41,240 --> 00:09:46,840
thinking of going into the army, anybody of thinking of going into the armed forces of any

110
00:09:46,840 --> 00:09:55,000
type, you can't really go back, and there's a long, long road to get back to the state

111
00:09:55,000 --> 00:10:03,520
where you before you had done the terrible things that are done in more, or any profession

112
00:10:03,520 --> 00:10:10,240
where killing is involved, even police work, becoming a police officer is, well, if

113
00:10:10,240 --> 00:10:16,960
you ever have to kill it, it's hard to go back.

114
00:10:16,960 --> 00:10:24,680
So people who have PTSD, who are traumatized, these sorts of people, I think, we should

115
00:10:24,680 --> 00:10:27,320
focus on, well, let's make two points here.

116
00:10:27,320 --> 00:10:33,520
The first point is those who have done, in regards to PTSD for those who have done bad deeds,

117
00:10:33,520 --> 00:10:38,120
we have a very difficult and long road, and these people, if they're committed, and

118
00:10:38,120 --> 00:10:42,320
then go for it, but they have to be careful themselves because their minds are going to

119
00:10:42,320 --> 00:10:50,400
be twisted and they'll be inclined to react improperly, for the same reasons that they reacted

120
00:10:50,400 --> 00:10:56,880
improperly before by killing, by hurting, by harming others.

121
00:10:56,880 --> 00:11:01,680
And as a cautionary tale for those thinking to do it, who think that killing is just an

122
00:11:01,680 --> 00:11:07,440
act, and it's just one moment, and you can decide any time, oh, I'm not going to kill

123
00:11:07,440 --> 00:11:08,440
anymore.

124
00:11:08,440 --> 00:11:10,120
It doesn't really work that way.

125
00:11:10,120 --> 00:11:15,200
What's done is done, the life can never be given back after it's taken.

126
00:11:15,200 --> 00:11:24,880
And the other point is in regards to those who have PTSD from seeing death, seeing horrible

127
00:11:24,880 --> 00:11:34,280
things, maybe even experiencing trauma such as rape or torture, starvation, whatever,

128
00:11:34,280 --> 00:11:41,880
any kind of horrible thing that the side effects of war.

129
00:11:41,880 --> 00:11:48,480
For these people, we have much more hope.

130
00:11:48,480 --> 00:11:52,760
Just one anecdote I want to say in regards before I forget, in regards to the first

131
00:11:52,760 --> 00:12:00,720
group, I spent some time with a man who was in war when I was in California.

132
00:12:00,720 --> 00:12:04,600
He was married to a Thai woman, so he drove us around a little bit.

133
00:12:04,600 --> 00:12:12,880
And he was probably the most horrible person I've ever had the pleasure of being in

134
00:12:12,880 --> 00:12:14,680
close quarters with.

135
00:12:14,680 --> 00:12:20,000
He said that people asked him what he missed most about being a soldier, and he said

136
00:12:20,000 --> 00:12:22,800
being able to kill people.

137
00:12:22,800 --> 00:12:29,120
I think he meant it, because his attitude to things was horrible.

138
00:12:29,120 --> 00:12:37,000
And the jokes he told were racist and really cruel and awful, and yet he would be sitting

139
00:12:37,000 --> 00:12:38,000
there.

140
00:12:38,000 --> 00:12:42,880
I think he was on heavy medication if I recall correctly for PTSD, I guess.

141
00:12:42,880 --> 00:12:48,400
But he was a sergeant, anyway, so that's what I think of in regards to this first group

142
00:12:48,400 --> 00:12:52,840
of people who have actually done bad things, it's quite difficult to ever hope to get

143
00:12:52,840 --> 00:12:56,880
to them, something that we should think about.

144
00:12:56,880 --> 00:13:05,800
The second group, those who have experienced the trauma, I don't see much problem in letting

145
00:13:05,800 --> 00:13:07,200
them meditate on their own.

146
00:13:07,200 --> 00:13:14,160
I would think if they have good theory and basic meditation practices, absolutely I would

147
00:13:14,160 --> 00:13:15,240
go for it.

148
00:13:15,240 --> 00:13:21,520
I would encourage them to pick up a book, start meditating, preferably in the tradition

149
00:13:21,520 --> 00:13:22,520
that I teach.

150
00:13:22,520 --> 00:13:33,320
Not my book, it's the one that I plug, it's the one that I encourage, but absolutely

151
00:13:33,320 --> 00:13:43,520
in this meditation I can pretty much guarantee that not 100%, but 99% guarantee that nothing

152
00:13:43,520 --> 00:13:47,560
bad is going to happen, and you'll only start to understand your problems better.

153
00:13:47,560 --> 00:13:52,720
In fact, the more you learn, the more you read, the more you study, the more you'll be able

154
00:13:52,720 --> 00:14:02,120
to deal with fear, anxiety, stress, paranoia, et cetera, and be able to overcome all sorts

155
00:14:02,120 --> 00:14:03,120
of stress.

156
00:14:03,120 --> 00:14:11,720
I mean, if this woman who was raped by her father can do it, it's hard to find something

157
00:14:11,720 --> 00:14:25,200
worse than that.

