1
00:00:00,000 --> 00:00:10,800
Today we come to the second chapter which deals with the ascetic practices.

2
00:00:10,800 --> 00:00:29,800
They are given here because the practice of these helps us to further scrape away the mental defilements.

3
00:00:29,800 --> 00:00:43,800
So, first we need the purity of virtue or moral conduct and in addition to that we need to practice some of these ascetic practices.

4
00:00:43,800 --> 00:00:52,800
So, that we can at least diminish in mental defilements.

5
00:00:52,800 --> 00:01:18,800
In this chapter 13 ascetic practices are treated and it is said in the book that these 13 practices were allowed by the Buddha.

6
00:01:18,800 --> 00:01:30,800
In Vinya Pitega and also in Sota Pitega these practices are mentioned but these are not precepts.

7
00:01:30,800 --> 00:01:46,800
We practice them not as precepts but as some in addition to the precepts and there are altogether 13 of them and the list is given in a book on page 59.

8
00:01:46,800 --> 00:01:56,800
The refuse break where as practice one through the triple rug where as practice three the arms food eaters practice for the house to house eager practice five.

9
00:01:56,800 --> 00:02:15,800
The one session as practice six the bowl food eaters practice seven the letter food refuses practice eight the forest dwellers practice nine the tree dwellers practice ten the open air dwellers practice eleven the channel ground dwellers practice twelve the any bad users practice and thirteen.

10
00:02:15,800 --> 00:02:38,800
The sitters practice and in Visori mother the meaning of the the words the meaning of the names are explained first and then the practice itself.

11
00:02:38,800 --> 00:03:00,800
The meaning the explanation of the names are mostly mostly with regard to party language so the translation may be a little awkward.

12
00:03:00,800 --> 00:03:16,800
The first one is called refuse break where whereas practice now before the roads were allowed by Buddha to be given by the people.

13
00:03:16,800 --> 00:03:36,800
The ones have to collect ropes themselves that means they have to go to places like a rubbish heap or a channel ground or whatever and then pick up pieces of cloth thrown away by people.

14
00:03:36,800 --> 00:03:51,800
They cut out the parts that are weak and then they take the good ones and they put them together into a rope so that is the way monks.

15
00:03:51,800 --> 00:04:07,800
There are no options roads before the road allowed ropes to be given by the people.

16
00:04:07,800 --> 00:04:34,800
The road for monks given by people but a no occasion arose so Buddha did not lead on any rule then at one time the the great physician Givaga he got two pieces of cloth very good cloth and so he went to the Buddha and requested the Buddha.

17
00:04:34,800 --> 00:04:40,800
Following these ropes through the Buddha and the request at him to allow monks to accept ropes given by the people.

18
00:04:40,800 --> 00:04:57,800
So from that time on monks are free to accept ropes given by the people or to collect pieces of cloth on the roads and make them into ropes.

19
00:04:57,800 --> 00:05:22,800
So the first one is the practice of the refuse wreck where up and that means if a monk undertakes to practice this this a certain practice and he he must not accept ropes from the people but he must pick up material for a rope and then make his own rope.

20
00:05:22,800 --> 00:05:48,800
And in the book on paragraph 15 there are 20 23 sources of ropes or actually rope material on the cloth that I believe made in the ropes.

21
00:05:48,800 --> 00:05:59,800
So the first one is one from eternal ground and the second one one from a shop and so on.

22
00:05:59,800 --> 00:06:03,800
So they are described in in Visotimaga.

23
00:06:03,800 --> 00:06:17,800
So these 23 other ropes allowed for monks as well as for those who practice who who do undertake these practices.

24
00:06:17,800 --> 00:06:40,800
Now after explaining these 23 sources of ropes the other give us some some explanation about the practice.

25
00:06:40,800 --> 00:06:53,800
So we go to paragraph 19.

26
00:06:53,800 --> 00:07:01,800
One given the we give it to the order of God by those who go out for arms cloth is not a refuse wreck.

27
00:07:01,800 --> 00:07:14,800
Now people people sometimes gave ropes through the community to the order not to an individual monk.

28
00:07:14,800 --> 00:07:28,800
So the order accepts these ropes and when there are enough ropes to to be distributed then distribution takes place.

29
00:07:28,800 --> 00:07:44,800
So a rope which has been given to Visotanga and which is got by a monk who who practices this practice for him.

30
00:07:44,800 --> 00:07:52,800
This kind of rope is not a refuse wreck so he must not accept or he must not use such throws.

31
00:07:52,800 --> 00:08:03,800
That is God as a share from the Sangha suppose there are 10 months living in a monastery and there are 10 ropes.

32
00:08:03,800 --> 00:08:09,800
Then one of the monks will will distribute these ropes to two different monks.

33
00:08:09,800 --> 00:08:23,800
And the best the best one goes to the the eldest one according to the seniority of the years to spend as monk.

34
00:08:23,800 --> 00:08:35,800
And then sometimes it seems that monks go out for arms cloth that means go out to to collect cloth instead of arms.

35
00:08:35,800 --> 00:08:47,800
And the usual thing is we go out every morning for arms we pick up our bowls and then go into the city to receive arms.

36
00:08:47,800 --> 00:08:54,800
So here they go out not for arms but for ropes.

37
00:08:54,800 --> 00:09:14,800
So a rope got in that way is not a refuse wreck then in the 23 sources of rope there is one one which is mentioned as a rope of a monk.

38
00:09:14,800 --> 00:09:24,800
That means a rope given by a monk to another monk who practices who undertakes this practice.

39
00:09:24,800 --> 00:09:36,800
With regard to that there are there is something to know and in this passage in about two sentences.

40
00:09:36,800 --> 00:09:48,800
I will explain not following his translation.

41
00:09:48,800 --> 00:09:58,800
A monk may give a rope to a monk who is undertaking this practice.

42
00:09:58,800 --> 00:10:10,800
But if the let us say non-dutanga monk and he is not practicing dutanga means a sadic practice.

43
00:10:10,800 --> 00:10:13,800
And one who practices I will call dutanga monk.

44
00:10:13,800 --> 00:10:25,800
Now a non-dutanga monk gives a rope to dutanga monk but when he gives rope to dutanga monks

45
00:10:25,800 --> 00:10:34,800
if he gives according to the seniority of the dutanga monks then that rope is not a refuse wreck.

46
00:10:34,800 --> 00:10:41,800
That rope is not not allowable for that monk.

47
00:10:41,800 --> 00:10:57,800
Sometimes people offer ropes to those who reside at their monastery.

48
00:10:57,800 --> 00:11:00,800
There are people who build a monastery or a building and then they say we will offer ropes to the monks who live in our monastery.

49
00:11:00,800 --> 00:11:07,800
So that kind of rope is also not a refuse wreck.

50
00:11:07,800 --> 00:11:19,800
So if a non-dutanga monk gives rope to dutanga monks he must give them not according to seniority.

51
00:11:19,800 --> 00:11:27,800
But just give them as their personnel gift.

52
00:11:27,800 --> 00:11:29,800
That is what is meant here.

53
00:11:29,800 --> 00:11:39,800
So in Yanomori's translation he misunderstood one word and that makes also no sense here.

54
00:11:39,800 --> 00:11:50,800
And in the case of one presented by Bhikkhu, one given after it has been got at a presentation of ropes by householders at the end of the rains.

55
00:11:50,800 --> 00:11:56,800
That is quite wrong.

56
00:11:56,800 --> 00:12:05,800
There is a Paliwan wasa and it means rain or it means year.

57
00:12:05,800 --> 00:12:12,800
The second remaining is year and also it means year spent by a monk.

58
00:12:12,800 --> 00:12:19,800
If somebody asked me how many wasa I have can I have say I have 41 wasa.

59
00:12:19,800 --> 00:12:24,800
That means I have been a monk for 41 years.

60
00:12:24,800 --> 00:12:30,800
And when the ropes are distributed distributed they were distributed according to the seniority.

61
00:12:30,800 --> 00:12:34,800
Suppose there are 100 monks but there are only 50 ropes.

62
00:12:34,800 --> 00:12:47,800
Then 50 ropes are distributed to the most senior 50 monks and then they stop there and when they get ropes later then they will distribute from the 51 and so on.

63
00:12:47,800 --> 00:13:03,800
So this is how the ropes given to monks are given to the order or are distributed to the monks.

64
00:13:03,800 --> 00:13:07,800
So when they distribute they must distribute according to seniority.

65
00:13:07,800 --> 00:13:20,800
So the Paliwan wasa here means year spent as a monk and not at the end of rainy season.

66
00:13:20,800 --> 00:13:33,800
It is interesting but it is funny because the word is wasa gah v-a-s-s-a-g-t-a.

67
00:13:33,800 --> 00:13:43,800
V-a-s-s-a-fasa can mean rains or year of year spent as a monk.

68
00:13:43,800 --> 00:13:55,800
And aga can mean the end but it is not useful that it means end but aga means the edge.

69
00:13:55,800 --> 00:14:02,800
So he took this to mean the edge of rains that means the end of rains but it is quite, quite wrong.

70
00:14:02,800 --> 00:14:12,800
Here aga does not mean the edge but aga is something like a portion of proportion as the wasa.

71
00:14:12,800 --> 00:14:18,800
And wasa he means not rainy season but the year spent as a monk.

72
00:14:18,800 --> 00:14:30,800
So when a non-dutanga monk gives rope to dutanga monk he must give not according to the seniority of the dutanga monks.

73
00:14:30,800 --> 00:14:43,800
But just give them away so that is the meaning here.

74
00:14:43,800 --> 00:14:50,800
That is the rope given by a monk to a dutanga monk.

75
00:14:50,800 --> 00:15:05,800
Now if that monk, a non-dutanga monk, got the rope when the, the label put the rope at his feet, not to into his hands.

76
00:15:05,800 --> 00:15:10,800
But they put the rope at the feet of the monk and the monk picks up.

77
00:15:10,800 --> 00:15:29,800
If it is that way and the non-dutanga monk gives into the hand of the dutanga monk, then it is called pure in one way.

78
00:15:29,800 --> 00:15:42,800
Because when the rope is put at the foot of, foot of a monk it is, it is called pure but when it is given to the hand of a monk, it is not called pure.

79
00:15:42,800 --> 00:15:48,800
So it is pure in one way.

80
00:15:48,800 --> 00:16:05,800
Then if lay people of the rope to the monk into the hands of a monk and that monk put the rope at the foot of a dutanga monk, it also is pure in one way only.

81
00:16:05,800 --> 00:16:34,800
But if the, the label put the rope at the feet of the monk and that monk gives that rope to the dutanga monk by putting the rope at the, at the feet of the dutanga monk, then it is called pure in both ways.

82
00:16:34,800 --> 00:16:54,800
Then the rope which is put into the hand of a dutanga monk and then put into the hand of a dutanga monk, that is called not so good rope.

83
00:16:54,800 --> 00:17:15,800
Let me see, here I just said in the hand it is not a strict man's rope and the practitioner of this practice, this dutanga are divided into three.

84
00:17:15,800 --> 00:17:28,800
The best one, the medium one and the least one, so there are three grades you are given.

85
00:17:28,800 --> 00:17:36,800
So the first grade, I mean the highest grade, one who takes it only from a channel ground is strict, that is, he is the best one.

86
00:17:36,800 --> 00:17:46,800
He must take the rope from a channel ground only, it is impossible now, it is even in our country, it is impossible.

87
00:17:46,800 --> 00:17:57,800
So in the, in the olden days people wrap the body with a cloth and then leave it at the, at the cemetery.

88
00:17:57,800 --> 00:18:07,800
It is not buried or it is not, they are not cremated.

89
00:18:07,800 --> 00:18:09,800
So monks can pick up such cloth from a channel grounds.

90
00:18:09,800 --> 00:18:19,800
So nowadays they can be no first class tutanga monk in this respect.

91
00:18:19,800 --> 00:18:31,800
So the second one takes one left, one left by someone thinking one gone forth will take it is medium.

92
00:18:31,800 --> 00:18:39,800
That means somebody leaves the rope somewhere so that some monks can see.

93
00:18:39,800 --> 00:18:52,800
So when you see it you can pick it up, so that if a dutanga monk takes that rope then he is set to be of medium, medium grade.

94
00:18:52,800 --> 00:19:01,800
And the mild one takes the rope put at his feet.

95
00:19:01,800 --> 00:19:12,800
So there are three, in every practice, every one of the 13 practices there are three crates of the practitioners.

96
00:19:12,800 --> 00:19:21,800
And then the benefits of the practice are given.

97
00:19:21,800 --> 00:19:25,800
He actually practices in conformity with the dependence.

98
00:19:25,800 --> 00:19:35,800
The dependence means that there are four kinds of dependence for monks, clothing, food, dwelling place, and medicine.

99
00:19:35,800 --> 00:19:37,800
They are called dependents.

100
00:19:37,800 --> 00:19:54,800
And when a monk depends on the cloth got from a channel ground and so on, then he is set to be practicing in conformity with the teaching of this dependence.

101
00:19:54,800 --> 00:19:59,800
And he is established in the first of the noble ones heritage.

102
00:19:59,800 --> 00:20:06,800
Now, and the reference is given here, a 227.

103
00:20:06,800 --> 00:20:15,800
There are four noble ones heritage and that is content with whatever ropes one gets.

104
00:20:15,800 --> 00:20:26,800
Content with whatever food that means a monk, whatever food one gets, content with whatever dwelling place one gets.

105
00:20:26,800 --> 00:20:34,800
And then practicing meditation.

106
00:20:34,800 --> 00:20:39,800
So these are called four heritage of the noble ones.

107
00:20:39,800 --> 00:20:50,800
So here the commentator said he is established in the first of the noble one's heritage.

108
00:20:50,800 --> 00:20:57,800
That is content with whatever ropes he gets.

109
00:20:57,800 --> 00:20:59,800
So this is the first.

110
00:20:59,800 --> 00:21:10,800
Then the second one, the second one is what?

111
00:21:10,800 --> 00:21:18,800
Triple rope whereas practice.

112
00:21:18,800 --> 00:21:26,800
Now, there are three ropes allowed for monks.

113
00:21:26,800 --> 00:21:43,800
And if a monk uses only three ropes, then he is set to undertake this aesthetic practice.

114
00:21:43,800 --> 00:21:52,800
Let me show you the three ropes.

115
00:21:52,800 --> 00:22:00,800
I need a demonstration.

116
00:22:00,800 --> 00:22:07,800
This is a lower, a lower garment.

117
00:22:07,800 --> 00:22:14,800
So this is one.

118
00:22:14,800 --> 00:22:29,800
And this is the upper rope, it's twice the size of that.

119
00:22:29,800 --> 00:22:35,800
These are the usual two ropes we use every day.

120
00:22:35,800 --> 00:22:44,800
And there is another one called a Sangati in Bali and it has two layers.

121
00:22:44,800 --> 00:22:46,800
We call it double rope.

122
00:22:46,800 --> 00:22:50,800
And it has more sections than the other rope.

123
00:22:50,800 --> 00:22:53,800
That has only five sections of five.

124
00:22:53,800 --> 00:22:56,800
We call them rooms, five rooms.

125
00:22:56,800 --> 00:23:01,800
But it may give me a year, about 25.

126
00:23:01,800 --> 00:23:04,800
Who makes these ropes?

127
00:23:04,800 --> 00:23:10,800
Many people know that they commercially make them.

128
00:23:10,800 --> 00:23:14,800
And this is actually, you use as a blanket.

129
00:23:14,800 --> 00:23:19,800
It is a double rope and so in winter, we use this as blanket.

130
00:23:19,800 --> 00:23:25,800
So these three ropes allowed by the Buddha.

131
00:23:25,800 --> 00:23:33,800
And when he wanted to allow the ropes, he tried it himself, the Buddha.

132
00:23:33,800 --> 00:23:38,800
He said in our books that during the coldest day in the year,

133
00:23:38,800 --> 00:23:44,800
that it may be in December, he put on only one rope during the night.

134
00:23:44,800 --> 00:23:46,800
And he tried it.

135
00:23:46,800 --> 00:23:52,800
And he could stay with one rope for the first watch of the night.

136
00:23:52,800 --> 00:23:57,800
Then he felt cold, so he took another rope.

137
00:23:57,800 --> 00:24:06,800
And he was able to keep himself warm enough with that rope until the second watch of the night.

138
00:24:06,800 --> 00:24:09,800
And then he take another rope.

139
00:24:09,800 --> 00:24:14,800
And then it could maintain him until the third watch.

140
00:24:14,800 --> 00:24:17,800
So at the end of the third watch, he felt cool again.

141
00:24:17,800 --> 00:24:19,800
So he took another rope.

142
00:24:19,800 --> 00:24:23,800
And so there were four ropes.

143
00:24:23,800 --> 00:24:29,800
And these four ropes, he allowed to do the marks.

144
00:24:29,800 --> 00:24:35,800
But these four BBB became three because two are made into one.

145
00:24:35,800 --> 00:24:39,800
And so we have now three ropes.

146
00:24:39,800 --> 00:24:44,800
So a monk who, under text, where are only three ropes?

147
00:24:44,800 --> 00:24:46,800
No, no, no, no more rope.

148
00:24:46,800 --> 00:24:47,800
Nothing to change.

149
00:24:47,800 --> 00:24:53,800
It's called a monk who practices this kind of a static practice or do dongle.

150
00:24:53,800 --> 00:24:56,800
I have a question for that one.

151
00:24:56,800 --> 00:25:04,800
You think that might be too limited for that particular area that, you know,

152
00:25:04,800 --> 00:25:06,800
I thought I was born and so on.

153
00:25:06,800 --> 00:25:10,800
So if someone in Alaska or something,

154
00:25:10,800 --> 00:25:18,800
I would like to become a monk and one thing in the fall is strictly, it might be a difficult one.

155
00:25:18,800 --> 00:25:21,800
I thought it was not in Alaska.

156
00:25:21,800 --> 00:25:23,800
That's right.

157
00:25:23,800 --> 00:25:29,800
You know, I'm wearing this shirt here.

158
00:25:29,800 --> 00:25:34,800
I would never use this in Bama.

159
00:25:34,800 --> 00:25:40,800
So we have to adapt to the climate of the place where we live in.

160
00:25:40,800 --> 00:25:42,800
But border lived in India.

161
00:25:42,800 --> 00:25:47,800
And India was not so cool as America or as Alaska.

162
00:25:47,800 --> 00:25:57,800
So we have to modify some of his sayings.

163
00:25:57,800 --> 00:26:06,800
So maybe the minimum amount of clothing.

164
00:26:06,800 --> 00:26:10,800
So you can survive with the least of the clothes.

165
00:26:10,800 --> 00:26:12,800
And that should be the minimum.

166
00:26:12,800 --> 00:26:21,800
Here, some people have say 10 or 12 sets of clothes, maybe.

167
00:26:21,800 --> 00:26:23,800
And they are actually not necessary.

168
00:26:23,800 --> 00:26:33,800
So what is a bare necessity for you maybe say one dress, one set of dress.

169
00:26:33,800 --> 00:26:41,800
So something like that.

170
00:26:41,800 --> 00:26:55,800
Now, we would regard to the practice itself.

171
00:26:55,800 --> 00:26:58,800
Now, months have to dye the ropes themselves in the old and days.

172
00:26:58,800 --> 00:27:05,800
So at the time of dying, first dye either the inner cloth or upper garment,

173
00:27:05,800 --> 00:27:08,800
inner cloth is the smallest one or the upper garment first.

174
00:27:08,800 --> 00:27:16,800
And having dyed it, you should wear that round the waist and dye the other.

175
00:27:16,800 --> 00:27:23,800
So when she dyes the rope, he put on one and then he dyes the other one.

176
00:27:23,800 --> 00:27:29,800
And then after dying, after he had finished dying, then he put on the other rope and then

177
00:27:29,800 --> 00:27:31,800
dye the other one and so on.

178
00:27:31,800 --> 00:27:40,800
So the two ropes, not not this, the other two ropes can be worn as an upper rope or

179
00:27:40,800 --> 00:27:44,800
as a lower garment at that time, at that moment.

180
00:27:44,800 --> 00:27:51,800
But this, he should never put on like a lower garment.

181
00:27:51,800 --> 00:27:56,800
So this is mentioned here.

182
00:27:56,800 --> 00:28:01,800
Then he can put them on over the shoulder and dye the cloak of patches or this,

183
00:28:01,800 --> 00:28:05,800
and the cloak of patches means this rope, Sangati.

184
00:28:05,800 --> 00:28:07,800
There are many patches here.

185
00:28:07,800 --> 00:28:11,800
But he is not allowed to wear the cloak of patches round the waist.

186
00:28:11,800 --> 00:28:20,800
So it should not be used as a lower garment even temporarily.

187
00:28:20,800 --> 00:28:25,800
But this is the duty one and about inside the village.

188
00:28:25,800 --> 00:28:29,800
That means inside the village are close to a village.

189
00:28:29,800 --> 00:28:33,800
But it is allowable for him and the forest to wash and dye two together.

190
00:28:33,800 --> 00:28:35,800
And he may have nothing to put on.

191
00:28:35,800 --> 00:28:38,800
But since he is in the forest, he could do that.

192
00:28:38,800 --> 00:28:45,800
However, he should sit in a place near to the rope so that if he sees anyone, he can pull a yellow cloth for himself.

193
00:28:45,800 --> 00:28:50,800
I think it is easy to get this color.

194
00:28:50,800 --> 00:29:14,800
And it may be considered as appropriate for those who have left behind their home or

195
00:29:14,800 --> 00:29:19,800
home lives and then go into a homeless life.

196
00:29:19,800 --> 00:29:29,800
And the color actually is something between yellow and yellow and brown.

197
00:29:29,800 --> 00:29:34,800
Not a specific color is mentioned.

198
00:29:34,800 --> 00:29:36,800
But it must not be red.

199
00:29:36,800 --> 00:29:39,800
It must not be bright yellow.

200
00:29:39,800 --> 00:29:42,800
It must not be blue, something like that.

201
00:29:42,800 --> 00:29:50,800
So it comes to something somewhere between yellow and brown.

202
00:29:50,800 --> 00:30:02,800
And we get the dye from the bark of a certain tree or the inner core of the jackfruit tree.

203
00:30:02,800 --> 00:30:08,800
I have asked people here whether they know jackfruit tree and they said no jackfruit.

204
00:30:08,800 --> 00:30:13,800
It is like similar to breadfruit tree.

205
00:30:13,800 --> 00:30:16,800
You don't know breadfruit either.

206
00:30:16,800 --> 00:30:19,800
The tree, you mean the trunk?

207
00:30:19,800 --> 00:30:22,800
Yeah, the fruit also be.

208
00:30:22,800 --> 00:30:24,800
It has...

209
00:30:24,800 --> 00:30:27,800
It's outer molecule.

210
00:30:27,800 --> 00:30:31,800
Skin has something like thorns.

211
00:30:31,800 --> 00:30:34,800
Not sharp thorns, but something like thorns.

212
00:30:34,800 --> 00:30:36,800
There is a tropical fruit.

213
00:30:36,800 --> 00:30:43,800
And the inner core of that tree is dark brown color.

214
00:30:43,800 --> 00:30:50,800
So we take that inner core and then chop it down to small pieces and then boil.

215
00:30:50,800 --> 00:30:51,800
So we get dye.

216
00:30:51,800 --> 00:30:59,800
So when it is dyed with that dye, it comes to resemble something like this color.

217
00:30:59,800 --> 00:31:21,800
Now, a monk who practices, who undertakes this practice, can have a fold, a rope or

218
00:31:21,800 --> 00:31:22,800
a foot piece of cloth.

219
00:31:22,800 --> 00:31:25,800
And that is mentioned here.

220
00:31:25,800 --> 00:31:28,800
The shoulder, it is called shoulder cloth.

221
00:31:28,800 --> 00:31:31,800
That is just a piece of cloth.

222
00:31:31,800 --> 00:31:41,800
One, one span, one span wide and three qubits long.

223
00:31:41,800 --> 00:31:44,800
So a piece of cloth to wrap around his body.

224
00:31:44,800 --> 00:31:49,800
That is to keep himself warm and also to keep...

225
00:31:49,800 --> 00:31:56,800
to so sweat so that it doesn't swipe the rope.

226
00:31:56,800 --> 00:32:05,800
So that is the only a foot piece of cloth allowed for him.

227
00:32:05,800 --> 00:32:13,800
Now, the next one is unfood eater's practice.

228
00:32:13,800 --> 00:32:19,800
That means if a monk undertakes this practice, he must go for arms every day.

229
00:32:19,800 --> 00:32:29,800
He cannot accept food from food or he must not accept invitations.

230
00:32:29,800 --> 00:32:44,800
And in this respect, the 14 kinds of food are mentioned.

231
00:32:44,800 --> 00:32:49,800
On page 67.

232
00:32:49,800 --> 00:32:55,800
Now, this unfood eater should not accept the following 14 kinds of meals.

233
00:32:55,800 --> 00:32:57,800
A meal offered to be ordered.

234
00:32:57,800 --> 00:33:06,800
A meal offered to be specified because that means there is one monk who assigned monks

235
00:33:06,800 --> 00:33:08,800
to accept food.

236
00:33:08,800 --> 00:33:17,800
So he may assign one monk to take food at a certain man's house.

237
00:33:17,800 --> 00:33:24,800
So that kind of food is called the specified, offered to specified monks.

238
00:33:24,800 --> 00:33:32,800
And then in rotation, a meal given by ticket.

239
00:33:32,800 --> 00:33:34,800
That means by lot of by ticket.

240
00:33:34,800 --> 00:33:40,800
And one each half-moon day, that means once and a fortnight.

241
00:33:40,800 --> 00:33:45,800
Some people offer food once and a fortnight.

242
00:33:45,800 --> 00:33:49,800
One each deposit a day, that means the same thing.

243
00:33:49,800 --> 00:33:55,800
But deposit a day means full-moon day or new-moon day.

244
00:33:55,800 --> 00:34:03,800
One each first of the half-moon, that means one day after full-moon and new-moon.

245
00:34:03,800 --> 00:34:06,800
A meal given for visitors when visiting monks.

246
00:34:06,800 --> 00:34:08,800
A meal for traveling monks.

247
00:34:08,800 --> 00:34:09,800
A meal for sick monks.

248
00:34:09,800 --> 00:34:12,800
A meal for those who are nothing sick monks.

249
00:34:12,800 --> 00:34:15,800
A meal supplied to a particular residence.

250
00:34:15,800 --> 00:34:17,800
A particular wehara.

251
00:34:17,800 --> 00:34:20,800
A meal given in a principal house.

252
00:34:20,800 --> 00:34:24,800
That means the first house in the village.

253
00:34:24,800 --> 00:34:26,800
A meal given in turn.

254
00:34:26,800 --> 00:34:27,800
That is by turn.

255
00:34:27,800 --> 00:34:32,800
A meal gives meals or food.

256
00:34:32,800 --> 00:34:42,800
So these are the 14 kinds of food, which a dudangab monk must not accept.

257
00:34:42,800 --> 00:34:54,800
So he must go out for arms and he must accept only those given by late people at each house or at the house as he wanted to go.

258
00:34:54,800 --> 00:35:04,800
And there are also three greats.

259
00:35:04,800 --> 00:35:11,800
One who is to fix arms brought food from before and from behind?

260
00:35:11,800 --> 00:35:13,800
Do you understand that?

261
00:35:13,800 --> 00:35:22,800
From before and from behind means, suppose a monk is standing in front of this house.

262
00:35:22,800 --> 00:35:25,800
If he is standing in front of this house.

263
00:35:25,800 --> 00:35:33,800
And if a person from the house behind brings food, that means a food from behind.

264
00:35:33,800 --> 00:35:38,800
And food from the next house.

265
00:35:38,800 --> 00:35:49,800
So one who is to fix arms brought food from before and from behind.

266
00:35:49,800 --> 00:35:54,800
And he gives the bull to those who take it while he stands outside the door.

267
00:35:54,800 --> 00:35:58,800
That means at the door he is standing.

268
00:35:58,800 --> 00:36:01,800
Then people come out and please give your bull.

269
00:36:01,800 --> 00:36:05,800
We want to fill your bull with food and offer to you something like that.

270
00:36:05,800 --> 00:36:06,800
Then he will give his bull.

271
00:36:06,800 --> 00:36:11,800
That is allowed for him.

272
00:36:11,800 --> 00:36:18,800
But he does not take arms by sitting and waiting for it to be brought later that day.

273
00:36:18,800 --> 00:36:21,800
But not, but he does not consent to it being brought next day.

274
00:36:21,800 --> 00:36:28,800
So he does not accept or consent to be waiting the next day.

275
00:36:28,800 --> 00:36:34,800
The my one consent to arms being brought on the next day and on the day after.

276
00:36:34,800 --> 00:36:38,800
Both these last missed the joy of an independent life.

277
00:36:38,800 --> 00:36:52,800
You know, sometimes I couldn't do what I want to do because I have accepted an invitation and I have to go to the invitation.

278
00:36:52,800 --> 00:36:55,800
So something like that is meant here.

279
00:36:55,800 --> 00:37:00,800
Both these last that the two monks missed the joy of an independent life.

280
00:37:00,800 --> 00:37:06,800
There is perhaps a preaching on the noble ones ages in some village.

281
00:37:06,800 --> 00:37:12,800
The strict one says to the others, let us go friends and listen to the dhamma.

282
00:37:12,800 --> 00:37:16,800
One of them says, I have been made to sit and wait by a man.

283
00:37:16,800 --> 00:37:21,800
The other I have consented to receive arms to more of an observer.

284
00:37:21,800 --> 00:37:23,800
So they are both losers.

285
00:37:23,800 --> 00:37:26,800
They did not get the opportunity to go to the dhamma talk.

286
00:37:26,800 --> 00:37:33,800
The other one was for arms in the morning and then he goes and save us the taste of the dhamma.

287
00:37:33,800 --> 00:37:55,800
Next one, let me see, in the benefits or let's read the benefits.

288
00:37:55,800 --> 00:38:02,800
He actually practices in conformity with the dependence because of the words the going forth by depending on the eating of lumps of arms.

289
00:38:02,800 --> 00:38:04,800
That is all right.

290
00:38:04,800 --> 00:38:07,800
He established in the second of the noble one's heritage.

291
00:38:07,800 --> 00:38:09,800
His existence is independent of others.

292
00:38:09,800 --> 00:38:12,800
It is a requisite recommended by the blessed one thus.

293
00:38:12,800 --> 00:38:14,800
Very less.

294
00:38:14,800 --> 00:38:16,800
Easy to get blameless.

295
00:38:16,800 --> 00:38:17,800
I don't know this eliminated.

296
00:38:17,800 --> 00:38:26,800
The practice of the minor framing rules of the party maga is fulfilled because these minor

297
00:38:26,800 --> 00:38:35,800
training rules said that you must go for arms and when you go for arms, you must be mindful of something like that.

298
00:38:35,800 --> 00:38:37,800
He is not maintained by another.

299
00:38:37,800 --> 00:38:39,800
So he does not defend one another.

300
00:38:39,800 --> 00:38:45,800
He helps others, pride is embedded and craving for taste is checked.

301
00:38:45,800 --> 00:38:50,800
The training precepts about eating as a group substituting one meal

302
00:38:50,800 --> 00:38:56,800
in petition for another and good behavior.

303
00:38:56,800 --> 00:39:01,800
Here also he misunderstood one word.

304
00:39:01,800 --> 00:39:08,800
This sentence refers to three precepts or three rules for months.

305
00:39:08,800 --> 00:39:19,800
One rule said that if a group is invited then they must not go any group and accept the food.

306
00:39:19,800 --> 00:39:23,800
They must go one by one but not as a group.

307
00:39:23,800 --> 00:39:28,800
So a group here means four months or more.

308
00:39:28,800 --> 00:39:34,800
Substituting one meal in petition for another means

309
00:39:34,800 --> 00:39:38,800
going to accept the later invitation.

310
00:39:38,800 --> 00:39:46,800
Suppose somebody came to me and invited to take food.

311
00:39:46,800 --> 00:39:49,800
Then another one came.

312
00:39:49,800 --> 00:39:56,800
And if I accepted the second man's invitation and actually accept the food of the second man,

313
00:39:56,800 --> 00:39:58,800
then I break this through.

314
00:39:58,800 --> 00:39:59,800
So this is what is meant here.

315
00:39:59,800 --> 00:40:08,800
Substituting here means just not accepting or taking the food of the first man

316
00:40:08,800 --> 00:40:10,800
but taking the food of the second man.

317
00:40:10,800 --> 00:40:14,800
That is why we have to be careful about the invitations.

318
00:40:14,800 --> 00:40:21,800
It is first come, first of basis.

319
00:40:21,800 --> 00:40:34,800
We are not to skip one invitation in favor of another or in favor of the later.

320
00:40:34,800 --> 00:40:38,800
And then good behavior is not good behavior.

321
00:40:38,800 --> 00:40:42,800
He misunderstood the word here that the word here is charita.

322
00:40:42,800 --> 00:40:50,800
Now there is another rule that forbid monks to visit houses.

323
00:40:50,800 --> 00:40:55,800
Either before or after taking meal at a house.

324
00:40:55,800 --> 00:40:59,800
Suppose I am invited to take meal at a house.

325
00:40:59,800 --> 00:41:05,800
Before taking meal at that house, I must not visit another house.

326
00:41:05,800 --> 00:41:10,800
And after taking meal at a house, I must not visit another house.

327
00:41:10,800 --> 00:41:14,800
If I want to visit, then I must inform another monk.

328
00:41:14,800 --> 00:41:18,800
I am going to visit this house, that house.

329
00:41:18,800 --> 00:41:29,800
So if he informs another monk who is close to him, then it is all right.

330
00:41:29,800 --> 00:41:39,800
If there are no monks or if he does not inform another monk and visits a house,

331
00:41:39,800 --> 00:41:46,800
whether before or after taking meal at that house, then he creates that rule.

332
00:41:46,800 --> 00:41:51,800
So that rule is called in pali charita.

333
00:41:51,800 --> 00:41:53,800
Charita can mean good behavior.

334
00:41:53,800 --> 00:41:58,800
And it can also mean wandering, going about.

335
00:41:58,800 --> 00:42:02,800
So here at the second morning, so visiting.

336
00:42:02,800 --> 00:42:09,800
So three rules are referred to here, accepting food as a group,

337
00:42:09,800 --> 00:42:12,800
and then accepting the later invitation.

338
00:42:12,800 --> 00:42:21,800
And visiting houses before and after taking meal at the appointed house.

339
00:42:21,800 --> 00:42:26,800
If you go for arms every day, you don't have to worry about these rules.

340
00:42:26,800 --> 00:42:28,800
You will not break any of these rules.

341
00:42:28,800 --> 00:42:34,800
Because you do not accept an invitation, but you go out for arms.

342
00:42:34,800 --> 00:42:38,800
So there can be no breach of these rules.

343
00:42:38,800 --> 00:42:46,800
If you practice, make sure you undertake the practice of going out for arms every day.

344
00:42:46,800 --> 00:42:54,800
The next one is house to house seeker.

345
00:42:54,800 --> 00:43:04,800
That means if a monk undertakes this practice, then he must not skip a house.

346
00:43:04,800 --> 00:43:08,800
Suppose he walks through one one one street.

347
00:43:08,800 --> 00:43:13,800
And if there are ten houses, he must stop at ten houses at each house.

348
00:43:13,800 --> 00:43:17,800
He must not skip this house and go to next house.

349
00:43:17,800 --> 00:43:23,800
So that is what is meant by house to house seeker.

350
00:43:23,800 --> 00:43:33,800
So in order for him to be convenient, the common leader gave advice here.

351
00:43:33,800 --> 00:43:38,800
First, he must look whether the road is clear.

352
00:43:38,800 --> 00:43:48,800
Then if it is not clear, he must not take that road, but take another road.

353
00:43:48,800 --> 00:43:58,800
And if he does not get food at a certain house or at some houses, say every day, then he can

354
00:43:58,800 --> 00:44:02,800
he can regard those houses as not houses.

355
00:44:02,800 --> 00:44:09,800
So he can skip those houses because every day they do not give him food.

356
00:44:09,800 --> 00:44:27,800
So in that case he can skip it.

357
00:44:27,800 --> 00:44:42,800
Then in the benefits, paragraph 33, about third or fourth land, he avoids the dangers

358
00:44:42,800 --> 00:44:45,800
in being supported by a family.

359
00:44:45,800 --> 00:44:50,800
And that is not correct here.

360
00:44:50,800 --> 00:44:53,800
He always makes this mistake.

361
00:44:53,800 --> 00:45:00,800
He avoids the dangers in frequenting families.

362
00:45:00,800 --> 00:45:08,800
A monk who frequent families is considered a bad monk and not good behaving monk.

363
00:45:08,800 --> 00:45:13,800
But if he goes for arms, then he does not have to visit them.

364
00:45:13,800 --> 00:45:28,800
And so he avoids the danger of being intimate with families or with lay people.

365
00:45:28,800 --> 00:45:47,800
The next one is one sashana.

366
00:45:47,800 --> 00:45:55,800
When the one sashana sits down in the sitting hall, instead of sitting on an elder seat,

367
00:45:55,800 --> 00:46:00,800
he should notice which seat is likely to fall to him and sit down on that.

368
00:46:00,800 --> 00:46:08,800
Now in the monastery, we are many monks live and there is a dining hall and monks sit according

369
00:46:08,800 --> 00:46:09,800
to see me already.

370
00:46:09,800 --> 00:46:21,800
So he must go a little earlier and then try to find a place where he would not be not

371
00:46:21,800 --> 00:46:26,800
there.

372
00:46:26,800 --> 00:46:32,800
He does not have to give a seat to a senior monk because the monks are to sit according

373
00:46:32,800 --> 00:46:41,800
to see me already.

374
00:46:41,800 --> 00:46:58,800
Thank you.

