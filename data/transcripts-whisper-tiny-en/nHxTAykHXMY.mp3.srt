1
00:00:00,000 --> 00:00:04,800
Good evening and welcome back to our study of the Dhamapada.

2
00:00:04,800 --> 00:00:11,320
Today we continue on with verse 101, which reads as follows.

3
00:00:11,320 --> 00:00:24,960
Saha sampi jiigatha, Anatapada sangita, yi kangatha padang sayo, yang sutwa upasamati, which means

4
00:00:24,960 --> 00:00:32,120
very much the same as what we, as the verse yesterday. So yesterday it was about a thousand

5
00:00:32,120 --> 00:00:44,160
words. Saha sampi jiigatha padang, no, a jiwa ja no, a different jiwa ja, which I guess would

6
00:00:44,160 --> 00:01:03,160
mean words. Let's see here. Saha sampi jiwa ja, so yesterday it was about words, a little

7
00:01:03,160 --> 00:01:07,360
bit of a different, almost the same verse, but a little bit different. The idea is the

8
00:01:07,360 --> 00:01:13,160
first one was about words today, talking about a gatha, which is a verse or a stanza.

9
00:01:13,160 --> 00:01:22,160
Gatha is a whole, like this is a gatha, that I've just said is a stanza, I guess, or a verse,

10
00:01:22,160 --> 00:01:35,160
whatever, however you say it. Saha sampi jiigatha, so the meaning is, if there should be a thousand

11
00:01:35,160 --> 00:01:46,280
words that are unconnected with, or not connected. Again, it's a nata panda, so I may have been

12
00:01:46,280 --> 00:01:55,800
right, a nata panda means the path of the useless path, that are connected with the useless

13
00:01:55,800 --> 00:02:00,600
path, they're not connected with the useful path, so they don't lead you in a good way,

14
00:02:00,600 --> 00:02:06,040
but they don't teach you something useful. They may be inclined you in the wrong direction

15
00:02:06,040 --> 00:02:15,560
or direct your mind towards confusion or towards delusion. Make you angry or upset.

16
00:02:15,560 --> 00:02:31,800
A kangata padangseyo, one gatha is better, one verse is better. Young supa opa sambati,

17
00:02:31,800 --> 00:02:46,120
and one hears it, which hearing, one hearing, makes one tranquil. So again, the whole

18
00:02:46,120 --> 00:02:50,920
idea is of the teaching not taking a lot of time. I'm not going to talk too much about

19
00:02:50,920 --> 00:02:54,600
this verse because it's very similar to the last one, but that's okay because we've

20
00:02:54,600 --> 00:02:59,120
got a whole story here to talk about, and there's another verse that we do want to talk

21
00:02:59,120 --> 00:03:07,920
about that occurs in the, another quote that we want to talk about that occurs in the story.

22
00:03:07,920 --> 00:03:15,440
So the story goes that there were a bunch of merchants or some group of men who went

23
00:03:15,440 --> 00:03:25,040
sailing, probably to find their fortune, and halfway through their trip, out in the ocean

24
00:03:25,040 --> 00:03:35,440
I guess, the ship sprung a leak and broke apart, and everyone was drowned except for a man

25
00:03:35,440 --> 00:03:44,560
called Bahia dau jia. And if any of you know the story of the elder Bahia, that's what

26
00:03:44,560 --> 00:03:54,720
the story is about. So Bahia being shipwrecked wound up naked, you know, in order to swim

27
00:03:54,720 --> 00:04:02,880
to shore, he had to lose his clothes or his robes or whatever he was wearing, and swim naked

28
00:04:02,880 --> 00:04:15,880
to shore, and he wound up in a place called Supagara, Supagara, Supagara. I don't know where

29
00:04:15,880 --> 00:04:29,280
that is, but some ocean port on the Indian subcontinent, and being naked, he figured

30
00:04:29,280 --> 00:04:37,360
he had to do something to cover his private parts. So he went and found a piece of bark

31
00:04:37,360 --> 00:04:45,600
or something sort of really simple to cover his private parts, and then he picked up a piece

32
00:04:45,600 --> 00:04:55,080
of a broken pot and went into the city, looking for arms, or went into the village looking

33
00:04:55,080 --> 00:05:05,560
for arms. So out of an act of desperation, he became a beggar, and the funny thing was that

34
00:05:05,560 --> 00:05:14,120
when he got to the village, people took one look at him in his sort of hard state of

35
00:05:14,120 --> 00:05:17,560
being, you know, maybe he'd been on the ship and he had let his hair grow and his beard

36
00:05:17,560 --> 00:05:24,760
grow, so he looked like an ascetic. And so they took one look at him wearing this bark,

37
00:05:24,760 --> 00:05:29,280
this piece of bark, I guess there was like sort of a soft bark that you could actually

38
00:05:29,280 --> 00:05:36,320
turn into cloth, so they had that wrapped around his private area, and using this simple,

39
00:05:36,320 --> 00:05:43,200
you know, piece of garbage as a bowl, they thought, wow, here's a real ascetic. And

40
00:05:43,200 --> 00:05:49,880
so the rumor started, they saw him, and so everyone gave him food and took care of him,

41
00:05:49,880 --> 00:05:57,480
and the rumor started spreading that he was in our hunt. And it was really because of

42
00:05:57,480 --> 00:06:08,240
the way he looked. So as time went on, people tried to give him good clothes, tried to

43
00:06:08,240 --> 00:06:15,000
give him things that would improve his state, and he refused them, because he realized,

44
00:06:15,000 --> 00:06:18,240
you know, the only reason I'm getting such good food from these people is because they

45
00:06:18,240 --> 00:06:29,080
think I'm a holy man. And so he totally played up this ruse, which, you know, it speaks

46
00:06:29,080 --> 00:06:38,680
volumes to, or it acts as sort of a commentary on the state of religion and India in

47
00:06:38,680 --> 00:06:45,920
the time of the Buddha, and even the state of religion today, where there's so many charlatans

48
00:06:45,920 --> 00:06:57,680
or phagocetics, who really have no interest in teaching, but put on the robes as a means

49
00:06:57,680 --> 00:07:03,040
of livelihood, or they put on errors and pretending to be in line, pretending to be mindful

50
00:07:03,040 --> 00:07:16,480
acting, saying things that are more mysterious or enigmatic to try and garner support.

51
00:07:16,480 --> 00:07:23,640
And so in the beginning, it was just a ruse, and he was playing it up, acting like an

52
00:07:23,640 --> 00:07:28,480
Arahan dressing like an, what people thought of as an Arahan, of course, in Buddhism,

53
00:07:28,480 --> 00:07:38,720
there's no sense of this being within Arahan looked like or anything. But eventually,

54
00:07:38,720 --> 00:07:45,560
people called him, people were so convinced that he was enlightened, that he himself

55
00:07:45,560 --> 00:07:53,000
became started to believe it. And he started to feel like maybe he did have some spiritual

56
00:07:53,000 --> 00:08:00,960
attainments after all. And so as time went on, he got right into the role and got a sense

57
00:08:00,960 --> 00:08:08,640
that, well, maybe I am, maybe I am an Arahan, and he was so well taken care of by the people

58
00:08:08,640 --> 00:08:17,680
that it just became sort of, he became a figure in this, this village or this society,

59
00:08:17,680 --> 00:08:30,160
and he himself started to get stuck there. And he would have been quite stuck. If it weren't

60
00:08:30,160 --> 00:08:36,040
for, the commentary says, one of his blood relatives, and they used this word blood relative

61
00:08:36,040 --> 00:08:46,400
actually sort of with a double meaning or not with a double meaning, but yeah, they don't

62
00:08:46,400 --> 00:08:55,320
really mean blood relative, but it has a different connotation. What they mean by blood

63
00:08:55,320 --> 00:09:00,720
relative, what it said, is actually not referring to his blood relative. It's referring

64
00:09:00,720 --> 00:09:09,680
to a fellow religious person in a past life, a fellow monk. So it happens that in the time

65
00:09:09,680 --> 00:09:16,600
of the Buddha Kasypa, there were a group of seven monks, and this is actually a fairly

66
00:09:16,600 --> 00:09:21,320
well-known story of these monks. This whole story is quite a well-known Buddhist story.

67
00:09:21,320 --> 00:09:25,400
So if you're hearing it for the first time, remember it well, you can tell that at Buddhist

68
00:09:25,400 --> 00:09:36,440
parties, tell it to all your Buddhist friends, it's a good story. So far, so good. So now

69
00:09:36,440 --> 00:09:41,400
we have to go back to the past, though. Here we leave Bahia, where he's just starting

70
00:09:41,400 --> 00:09:46,880
to give rise to this thought, maybe I am in our heart, and we have to explain what happened

71
00:09:46,880 --> 00:09:54,000
next. By going back to the past, it happens that there were these seven monks in the time

72
00:09:54,000 --> 00:10:00,600
of the Buddha Kasypa. It was actually after the time of the Buddha Kasypa, which is interesting.

73
00:10:00,600 --> 00:10:17,720
It's a unique description of the corruption of Buddhism. So there were seven monks who were

74
00:10:17,720 --> 00:10:27,180
noticing and really discouraged by the fact that the monks, the novices, even the lay

75
00:10:27,180 --> 00:10:32,280
people were quite corrupt. The whole society had become corrupt, and so they said to

76
00:10:32,280 --> 00:10:38,880
us themselves, and the quote is, so long as our religion has not yet disappeared, we will

77
00:10:38,880 --> 00:10:49,520
make our own salvation sure. So they paid respect to the Jaitia, the big golden monument

78
00:10:49,520 --> 00:10:57,840
in honor of the Buddha, and the Bodhi tree, and then went to the forest to a mountain,

79
00:10:57,840 --> 00:11:03,400
and put this ladder up so they could climb up to this cliff, sort of this isolated cliff

80
00:11:03,400 --> 00:11:11,560
halfway up to the cliff wall, just like a ledge, and then they cut off or threw down

81
00:11:11,560 --> 00:11:21,000
the ladder, and they agreed amongst themselves that they would practice until death.

82
00:11:21,000 --> 00:11:26,520
Either death or enlightenment was supernatural powers that would allow them to escape

83
00:11:26,520 --> 00:11:39,320
their faith. So I complete an utter dedication, it's like do or die, really do or die.

84
00:11:39,320 --> 00:11:42,520
And so they practiced, I guess the ledge was big enough for them all to do at least sitting

85
00:11:42,520 --> 00:11:48,720
meditation, maybe they didn't do walking meditation, don't know how big the ledge was.

86
00:11:48,720 --> 00:11:57,240
But the seven monks meditating arduously are with ardor, arduously, it's not the right word,

87
00:11:57,240 --> 00:12:06,160
ardently, that's the word. And within a day, they were totally dedicated, and you can

88
00:12:06,160 --> 00:12:10,720
imagine how that affected them, and encouraged them in their practice, actually could

89
00:12:10,720 --> 00:12:15,720
be a useful sort of thing. I mean, don't quote me on that, don't go off, and try this

90
00:12:15,720 --> 00:12:21,000
at home. That's one of those things you want to leave to the experts. We'll have to remember

91
00:12:21,000 --> 00:12:28,680
these guys were monks, and maybe they didn't, probably they didn't actually have contact

92
00:12:28,680 --> 00:12:34,720
with the Buddha Kasypa, this would have been after he had passed away. So nonetheless,

93
00:12:34,720 --> 00:12:41,040
it's something interesting if someone were so inclined to, I don't can't believe I'm

94
00:12:41,040 --> 00:12:44,280
saying that, probably I shouldn't encourage it. I mean, absolutely. I mean, it's something

95
00:12:44,280 --> 00:12:49,560
that we would encourage in Buddhist countries, but under the interests of not being

96
00:12:49,560 --> 00:12:58,840
sued, I think we will not encourage such activity ourselves. And the last interesting

97
00:12:58,840 --> 00:13:06,520
thing that had happened, an interesting concept, and within a day, the eldest of them,

98
00:13:06,520 --> 00:13:15,160
the most senior of them, became an arhat, and became an arhat with powers to levitate his

99
00:13:15,160 --> 00:13:21,360
body and fly down and get food. And so he came back with food, this part you don't have

100
00:13:21,360 --> 00:13:27,400
to believe, this part is, you know, where we can decide whether we want to believe or

101
00:13:27,400 --> 00:13:33,000
not believe, it's not important, it's not an important part of the story. So if it strikes

102
00:13:33,000 --> 00:13:41,480
you the wrong way, or if it makes you feel bad, or it makes me lose credibility in your

103
00:13:41,480 --> 00:13:48,320
eyes, well, please consider this isn't the important part. They came back with food for

104
00:13:48,320 --> 00:13:57,000
the other monks, and said to them, please eat with me. And the other, and you kind of get

105
00:13:57,000 --> 00:14:02,440
the sense he may have just been being kind, but there was something to what he said, because

106
00:14:02,440 --> 00:14:06,240
as an arhat, he could teach them, right? Especially if they were to take food, and then

107
00:14:06,240 --> 00:14:12,920
he could probably give them some instruction. I'd imagine that was part of his thinking.

108
00:14:12,920 --> 00:14:16,600
But the other monks took one look at him and said, excuse me, did we make this kind of

109
00:14:16,600 --> 00:14:21,320
deal that if you became enlightened and we would start eating your food, like living off

110
00:14:21,320 --> 00:14:27,680
of your enlightenment? And he said, no, we didn't make such a deal. And he said, well,

111
00:14:27,680 --> 00:14:35,920
then if we become enlightened, we will go and get alms food on our own power. And so

112
00:14:35,920 --> 00:14:42,560
sure enough, the second day, the second monk became an anagami, not an arhat, but an anagami.

113
00:14:42,560 --> 00:14:49,880
Anagami is someone who has freed themselves from desire, central desire, and aversion.

114
00:14:49,880 --> 00:14:56,600
So they still have some desire, desire for becoming wanting to be this, wanting to be that.

115
00:14:56,600 --> 00:15:03,520
They're still the concept of conceit, not exactly self, but kind of a bit of an attachment

116
00:15:03,520 --> 00:15:10,560
to ideas. But as a result, they are born in the purest of pure Brahma worlds. They're

117
00:15:10,560 --> 00:15:16,200
called the pure abodes, and they live there for billions of years. Until finally, they

118
00:15:16,200 --> 00:15:21,840
become an arhat into there. This realm is so pure that it can only lead to freedom. It might

119
00:15:21,840 --> 00:15:27,320
take billions of years, but I don't know how long I'm assuming it's billions, lots and

120
00:15:27,320 --> 00:15:33,760
lots of years. And he too gained magical power. So he too went down, brought food back,

121
00:15:33,760 --> 00:15:37,760
and the two of them tried to convince the other five to eat, because by now, two days

122
00:15:37,760 --> 00:15:45,400
without food or water, they were really doing badly. But the other five were adamant,

123
00:15:45,400 --> 00:15:53,240
and they refused the food. And so for the next five days, they strove ardently. And after

124
00:15:53,240 --> 00:15:59,280
the seventh day, they all died. All seven of them, actually, I think. It actually doesn't

125
00:15:59,280 --> 00:16:04,000
talk about the other two. You wouldn't think the other two would have died. Maybe they

126
00:16:04,000 --> 00:16:18,320
died as well. Let me see. Yeah, no, actually, the other two that became a light and probably

127
00:16:18,320 --> 00:16:22,760
didn't die there. There's no sense. It wouldn't make any sense that they didn't, because

128
00:16:22,760 --> 00:16:28,200
they were able to leave at that will. But the other five withered up and died. And

129
00:16:28,200 --> 00:16:32,520
were born in the heavens. They were born in a good place, because their minds were

130
00:16:32,520 --> 00:16:37,640
really set on good things, totally dedicated to goodness. I mean, there's nothing wrong

131
00:16:37,640 --> 00:16:44,280
with such a thing. I mean, acceptance of far as people think it's suicide, but not really.

132
00:16:44,280 --> 00:16:49,000
It's interesting, because in India, this was a big thing. Starving yourself to death was

133
00:16:49,000 --> 00:16:55,760
considered to be valid religious practice, not in Buddhism, but in other religions. So these

134
00:16:55,760 --> 00:17:02,480
monks weren't exactly starving themselves. They were just putting themselves in a position

135
00:17:02,480 --> 00:17:11,320
where there was nothing would deter them, probably still not not not not not exactly

136
00:17:11,320 --> 00:17:17,160
encouraged sort of practice for the reason that you've only got a but a week before

137
00:17:17,160 --> 00:17:24,120
you die. So inside really is either do or die. Anyways, an interesting story quite well

138
00:17:24,120 --> 00:17:35,020
known and discussed. But they were all born in heaven, and eventually became five of

139
00:17:35,020 --> 00:17:41,400
the Bhutagotamas, five people at a time of the Bhutagotama. I'm not sure if I can't remember

140
00:17:41,400 --> 00:17:47,880
about them all, but three of them, at least three of them became our hands. Anyway,

141
00:17:47,880 --> 00:18:03,480
one of the five was Bahia, Dauru, Dauruji, India, our guy. So here he is, after all that,

142
00:18:03,480 --> 00:18:07,920
kind of getting the idea that maybe I am an Rhaan, then it probably is coming partially

143
00:18:07,920 --> 00:18:16,120
from his intense desire to be an Rhaan. So he starts to think, hey, maybe this is it,

144
00:18:16,120 --> 00:18:23,520
maybe I found it, even after all these lifetimes it was still in his deep in his psyche.

145
00:18:23,520 --> 00:18:30,880
And at that moment, we are told this, the number two elder who remember was born in the

146
00:18:30,880 --> 00:18:38,280
Brahma realms in the Sudhavasa, the pure boats. And this is, I guess, lots and lots of

147
00:18:38,280 --> 00:18:44,880
years later, but he's still up there, kind of gets word of it, or maybe he's been keeping

148
00:18:44,880 --> 00:18:57,080
tabs on his brethren throughout Samsara. And so it just happens to get a sense of what's

149
00:18:57,080 --> 00:19:01,280
going on with Bahia and looks down and starts shaking his head. And I don't know, Brahma's

150
00:19:01,280 --> 00:19:11,800
have heads, but he realizes the need to go down and say something, go down and fix things,

151
00:19:11,800 --> 00:19:26,800
make things right. And he says, well, he is a friend of mine, he is a relative brother

152
00:19:26,800 --> 00:19:32,560
in the Dhamma. So I have an obligation, I should go and do something. So apparently this

153
00:19:32,560 --> 00:19:46,160
Brahma, this God basically came down, maybe he stirred up a storm or thunderbolts in lightning,

154
00:19:46,160 --> 00:19:55,920
thunder in lightningbolts. And says, Bahia, you are not in our hunt nor are you on the

155
00:19:55,920 --> 00:20:01,680
path to become in our hunt. The practice that you have undertaken is not the practice

156
00:20:01,680 --> 00:20:07,200
that leads one to an hour on. You don't have the qualities in our hunt. You are not an

157
00:20:07,200 --> 00:20:21,120
hour on period. And Bahia is looking up at the great Brahma, a God, I'm telling him this.

158
00:20:21,120 --> 00:20:28,120
And I think that would stir up just about anyone. And so he realizes, no, clearly I'm

159
00:20:28,120 --> 00:20:35,360
not an hour on. This is correct. Sort of give it's a wake up call. If you don't like that

160
00:20:35,360 --> 00:20:38,880
version of the story, you can pretend the story goes that someone came up to him and

161
00:20:38,880 --> 00:20:42,960
woke him up and said, look, you don't look like an hour on, buddy, you're not an hour

162
00:20:42,960 --> 00:20:52,040
on. Then he was, he was stirred and he asks his friend, do you know where there is? Okay,

163
00:20:52,040 --> 00:21:00,440
maybe I'm not an hour on, but do you know of anyone who is? Someone I can go to? Because

164
00:21:00,440 --> 00:21:06,120
it seems like a good thing to be. Something worth inclining towards. And you know, here

165
00:21:06,120 --> 00:21:09,960
I am, everyone thinks I'm an hour on. It would probably be a good idea to actually become

166
00:21:09,960 --> 00:21:18,040
one, so I'm not a hypocrite or a charlatan. And the deity tells him in Sawati, that's where

167
00:21:18,040 --> 00:21:26,560
you've got to go. To the north of here, it's to the north. So this port, Suwadipa Suparaka,

168
00:21:26,560 --> 00:21:38,880
Suparaka is in the south. And so he sends him on his way to the north. There's a city called

169
00:21:38,880 --> 00:21:50,840
Sawati, that's where you have to go. There's a Buddha. And by drop his belongings there

170
00:21:50,840 --> 00:22:00,720
and ran throughout the night and arrived at the city of Sawati. Just as he arrived at the

171
00:22:00,720 --> 00:22:11,160
city of Sawati and went, asked around and went to, went up to some monks and asked where

172
00:22:11,160 --> 00:22:16,640
the Buddha was. And they said, oh, he's just gone for arms into the city of Sawati. So

173
00:22:16,640 --> 00:22:25,640
by here, ran towards the city and got to the Buddha just as he was, or as he was walking

174
00:22:25,640 --> 00:22:33,400
through the streets. And when he sees the teacher there in the streets, he bows down,

175
00:22:33,400 --> 00:22:41,560
prostrate himself at the Buddha's feet and says, venerable sir, I am at your mercy, please

176
00:22:41,560 --> 00:22:53,440
teach me the dhamma. And he said, where have you, where are you from? He said, I come from

177
00:22:53,440 --> 00:23:02,080
Suparaka. And he said, oh, you've come a long ways and sit down, rest a while. I'm on

178
00:23:02,080 --> 00:23:09,200
arms around. Once I finish arms around, then we can, we can talk. When I return, just wait

179
00:23:09,200 --> 00:23:14,280
here and when I return, I would teach you. But by here is having none of it. He says,

180
00:23:14,280 --> 00:23:18,680
no, please now. I don't know when I'm going to die. I don't know when you're going to die.

181
00:23:18,680 --> 00:23:25,560
I don't know what the future holds. Please, I mean, I don't need anything big. Don't need

182
00:23:25,560 --> 00:23:38,960
a long teaching. Just give me a brief, brief teaching. It's fine. And his body is trembling

183
00:23:38,960 --> 00:23:44,120
and shaking. He's just so excited. And just having seen the Buddha, they said, I found

184
00:23:44,120 --> 00:23:47,920
finally also could probably have something to do with the fact that he lived with the

185
00:23:47,920 --> 00:23:55,920
Buddha before. But he is ecstatic about this. And it says that that's, for that reason,

186
00:23:55,920 --> 00:24:00,640
the Buddha turned him away and the Buddha put him off. The Buddha said, so again, the

187
00:24:00,640 --> 00:24:10,120
Buddha says, look, it's not the right time. I'm going on arms. Wait, wait. And again,

188
00:24:10,120 --> 00:24:13,720
Vahis says, please, please, please, please. And for a certain time, I'm going to put some

189
00:24:13,720 --> 00:24:23,520
off. And this for a third time, but he says, no, really not to be put off. He requests

190
00:24:23,520 --> 00:24:29,360
for a third time. But the Buddha, the Buddha actually, at this point, I'm going to

191
00:24:29,360 --> 00:24:37,400
do this for a purpose, not actually intending to put him off, but to calm him down. So

192
00:24:37,400 --> 00:24:43,680
by the third time, he had kind of focused his mind and calmed himself down and he was

193
00:24:43,680 --> 00:24:50,000
ready to learn. And so the Buddha did give him what amounts to one of the really one of

194
00:24:50,000 --> 00:24:57,000
the best simple explanations of Vipasana meditation. And so this is the teaching that I

195
00:24:57,000 --> 00:25:00,840
think we have, we can talk a little bit about rather than actually dealing with the

196
00:25:00,840 --> 00:25:12,280
Dhamma Pandavas tonight. And he says, ahuang bhahya, sikita bhang, what does it say?

197
00:25:12,280 --> 00:25:19,160
Tasmatiha tay bhahya ahuang sikita bhang. Well, then bhahya, this is how you should train

198
00:25:19,160 --> 00:25:28,280
yourself. And then he says, tay diktamatang bhavasati, suttay suttamatang bhavasati. The full verse

199
00:25:28,280 --> 00:25:32,760
and the full teaching is in the uddhana, not in the dhammapada. So this is just a recalling

200
00:25:32,760 --> 00:25:39,960
of the events that led up to the teaching. But the gist of it is, diktay diktamatang bhavasati,

201
00:25:39,960 --> 00:25:50,120
let's see, let what is seen, just be what is seen. Let the scene just be seen, mata, mata

202
00:25:50,120 --> 00:26:01,520
means strictly or just only, merely. And so what that, that really is the essence of

203
00:26:01,520 --> 00:26:08,640
insight meditation, just hearing this, suttay suttamatang bhavasati and then he goes on to say,

204
00:26:08,640 --> 00:26:12,440
if you do this, there will be no self for you. There will be no self in this, there

205
00:26:12,440 --> 00:26:19,880
will be no self from this. You will have no attachment to here or there or anywhere. And

206
00:26:19,880 --> 00:26:26,040
just from that very basic teaching, bhahya became an orat. I'll come back to the teaching

207
00:26:26,040 --> 00:26:31,400
in the second, but we'll just finish our story here. After becoming an oratang, it turns

208
00:26:31,400 --> 00:26:41,320
out that bhahya didn't have the good fortune. He had never given any gifts of monastic

209
00:26:41,320 --> 00:26:48,360
requisites. He'd never given robes. He'd never given a ball. And so there's an idea that

210
00:26:48,360 --> 00:26:58,720
this is somehow prerequisites for actually being able to ordain. It sounds kind of an improper

211
00:26:58,720 --> 00:27:02,320
for me to say this as a monk, but it is sort of the truth. And I think it's worth

212
00:27:02,320 --> 00:27:09,800
a bear's telling because it's the kind of thing that I personally think about. For those

213
00:27:09,800 --> 00:27:16,840
people who become monks and are keen to stay as a monk, it's also a very useful thing

214
00:27:16,840 --> 00:27:21,560
is to give robes to other monks and make that as a determination. So I'm always trying

215
00:27:21,560 --> 00:27:27,000
myself to give robes and give monastic requisites with the intention that it should allow

216
00:27:27,000 --> 00:27:34,560
me to always be to remain in this state of being a monk. It's a support for your own determination

217
00:27:34,560 --> 00:27:42,000
to stay as a monk. So I think it's a useful thing to talk about, not that I'm saying,

218
00:27:42,000 --> 00:27:49,200
you know, hey, give things to monks because, you know, wow, that's kind of crass, I suppose.

219
00:27:49,200 --> 00:27:53,520
But nonetheless, that's what the commentary says and it seems reasonable because he wasn't

220
00:27:53,520 --> 00:27:57,320
able to ordain. The Buddha said, he said, please ordain me and the Buddha didn't say,

221
00:27:57,320 --> 00:28:05,600
come monk because he didn't have the good karma. In fact, if you want to guess what happened

222
00:28:05,600 --> 00:28:12,480
to Bahia, it turns out he's one of the five that we talked about last night. If you

223
00:28:12,480 --> 00:28:18,000
remember, we talked about last night as he was searching for a robe for a bowl and robes

224
00:28:18,000 --> 00:28:26,640
to ordain with, he met our infamous cow, the cow that killed five, a number of people

225
00:28:26,640 --> 00:28:32,880
in the Buddha's time. Bahia being one of them, gored him on her horn. But as I said,

226
00:28:32,880 --> 00:28:45,840
it was supposedly not a cow. It was actually a demon in a cow form. Anyway, he died. And

227
00:28:45,840 --> 00:28:50,480
the Buddha on his way back from alms around saw Bahia's body lying in a ditch or on the

228
00:28:50,480 --> 00:28:58,160
side of the road and told the monks to take his body and to erect a stupa. And so they actually

229
00:28:58,160 --> 00:29:03,680
call him Bahia Terra, which is interesting. He's an elder, even though he never ordained.

230
00:29:03,680 --> 00:29:09,920
He's one of the 80 great disciples of the Buddha. He's considered to be the Buddha's chief

231
00:29:09,920 --> 00:29:19,280
disciple in regards to development.

232
00:29:28,880 --> 00:29:35,680
I thought it said here, probably does.

233
00:29:35,680 --> 00:29:44,240
Ahia, preeminent among my disciples who are quick to learn the truth. So he was the one who

234
00:29:44,240 --> 00:29:50,720
was able to learn the truth, the quickest, go from zero to Arahant in the shortest amount of time,

235
00:29:51,280 --> 00:29:56,160
and the quickest. He was remarkable in that respect because he got very little teaching. Even

236
00:29:56,160 --> 00:30:03,040
Sariputa heard a little bit of teaching and only became a sotapana. Kondaniya, when he heard

237
00:30:03,040 --> 00:30:07,440
the Dhamma Chakapawat in the sotap, became a sotapana. Usually it would take a little bit more

238
00:30:07,440 --> 00:30:13,760
teaching than this. But with Bahia, there was a brief teaching in the middle of the, in the middle

239
00:30:13,760 --> 00:30:19,760
of the road. He was so dedicated that even though the short teaching, he was remarkable in that he

240
00:30:19,760 --> 00:30:28,400
was able to become enlightened just by hearing. So the monks were surprised. They said, well,

241
00:30:28,400 --> 00:30:33,280
how did he become an Arahant? They said, well, when I just taught him this teaching. And they said,

242
00:30:33,280 --> 00:30:37,440
just with that short teaching, he became an Arahant. And again, the Buddha said, oh, no,

243
00:30:38,080 --> 00:30:44,160
it's not about how much. It's not about much or little. And then he taught this verse. So has some

244
00:30:44,160 --> 00:30:53,200
Pete. Jai Gata. You can teach someone a thousand Gata's, a thousand verses. And if they're useless,

245
00:30:53,200 --> 00:31:00,960
no benefit. But one verse, if it's associated with the verse, it's worth a thousand verses,

246
00:31:01,440 --> 00:31:08,240
thousand useless verses. So it's the quality, not the quantity. That's the verse. But what we have

247
00:31:08,240 --> 00:31:12,880
to learn has much more to do with the teaching that he gave to Bahia. Because we've already talked

248
00:31:12,880 --> 00:31:20,800
about this concept of expressed in the Dhamma Padavas. The teaching of Ditetitamat Dangbavi

249
00:31:20,800 --> 00:31:27,200
Siti and just briefly, just to remark at how important that verse is. Because it describes the

250
00:31:27,200 --> 00:31:33,600
essence of the practice of objectivity that we're trying to see things just as they are.

251
00:31:33,600 --> 00:31:38,480
It's incomprehensible to most people. You think, well, that's ridiculous. Seeing, of course,

252
00:31:38,480 --> 00:31:42,960
is just seeing. That's all it is. But that's only because we've never, if you've never practiced

253
00:31:42,960 --> 00:31:50,000
in sight meditation, you've never actually explored the, in a, actually, really deeply come to

254
00:31:50,000 --> 00:31:56,720
understand how the mind works. You know, we don't, so we don't connect our addictions, attachments,

255
00:31:56,720 --> 00:32:03,760
our aversions, and fears, and worries, and so on, with the actual moment-to-moment interactions

256
00:32:03,760 --> 00:32:14,720
of our mind, and that manner in which our mind interacts with reality. Once you have, it becomes clear

257
00:32:14,720 --> 00:32:20,320
that really, that's the problem, is Ditetit is not just Ditetit. Ditetit is not just, what it's seen,

258
00:32:20,320 --> 00:32:25,600
it's not just what it's seen. When you see something, you immediately make more of it than it

259
00:32:25,600 --> 00:32:40,400
actually is. It becomes good, bad, me, mine, scary, a problem, troublesome, the goal becomes so

260
00:32:40,400 --> 00:32:49,600
many different things. And we react accordingly, liking, disliking, becoming obsessed with it,

261
00:32:49,600 --> 00:32:57,200
attached to it, clinging to it. So, really, the essence of our practice is to change that, to

262
00:32:57,200 --> 00:33:05,200
change that habit, and in the process to see clearly that those things which we react to are not

263
00:33:05,200 --> 00:33:17,840
worth reacting to. So, becoming naturally objective, to the point where naturally we see things

264
00:33:17,840 --> 00:33:24,960
as they are, because we have no reason to see them as good or bad. You see that, because we see

265
00:33:24,960 --> 00:33:30,000
that that doesn't help. You see that treating something is good or bad only gets you caught up in

266
00:33:30,000 --> 00:33:37,840
problems. This is really essential to overcoming addiction, to overcoming aversion, to overcoming

267
00:33:37,840 --> 00:33:44,560
depression and fear and anxiety. It's not to chase it away, not to get upset about it, but just

268
00:33:44,560 --> 00:33:50,080
to see it as it is. To see every experience as it is when someone says something to you that you

269
00:33:50,080 --> 00:33:57,360
might like or dislike. It's not the hearing that's the problem. If you just hear it, there's no problem,

270
00:33:57,360 --> 00:34:04,160
it's the judging. Judging it as more than just sound, more than just words, more than just speech.

271
00:34:05,040 --> 00:34:09,520
And then even when you like or dislike it, you judge the liking or disliking as meaningful,

272
00:34:09,520 --> 00:34:16,560
you ascribe some extrinsic meaning to it. You say, oh, this is, I'd like this. Therefore,

273
00:34:16,560 --> 00:34:22,480
I should chase after it. I dislike it. Therefore, I should tell that person to be quiet or go away.

274
00:34:26,000 --> 00:34:31,520
So, the whole reason why we're practicing meditation is this. The whole essence of what we're

275
00:34:31,520 --> 00:34:39,040
trying to accomplish with saying to ourselves, seeing or hearing, hearing is just this, that it

276
00:34:39,040 --> 00:34:43,280
should just be what it is. Now, when you do this, it's going to be quite jarring in the beginning.

277
00:34:43,280 --> 00:34:48,720
Why people are uncomfortable often practicing this meditation is because it's not comfortable.

278
00:34:48,720 --> 00:34:59,280
It goes against our inclination to cling, to follow, to examine, to extrapolate, to continue.

279
00:34:59,280 --> 00:35:04,960
And so, we've gotten into this rat of our habits. And when you start to break that up and just

280
00:35:04,960 --> 00:35:08,960
say, seeing, seeing, it can be quite disruptive in the beginning.

281
00:35:12,640 --> 00:35:20,560
But quickly, you should be able to see the difference. Once you get skilled at it,

282
00:35:20,560 --> 00:35:26,240
which doesn't take very long, you should be able to see one moment where your mind is clear,

283
00:35:26,240 --> 00:35:35,600
where you just see, seeing is just seeing. And you can tell the difference between that and our

284
00:35:36,880 --> 00:35:40,720
and the alternative of attaching, of clinging, of making more of stings than they are.

285
00:35:42,720 --> 00:35:53,440
This state of objectivity is clear, is calm, is pure. The mind is not solid, is not attached,

286
00:35:53,440 --> 00:36:00,560
it's not suffering. The mind is at peace in that one moment. So, if you have pain and you see it

287
00:36:00,560 --> 00:36:06,560
just as pain, you no longer suffer from the pain, just in that moment. When you hear something like,

288
00:36:06,560 --> 00:36:11,280
see something, feel something that is unpleasant. When you say, when you experience it,

289
00:36:11,280 --> 00:36:16,720
remind yourself and then you just see it as it is. You can see that it actually wasn't a problem

290
00:36:16,720 --> 00:36:23,680
in the first place. With practice, this becomes our inclination, and eventually leads us to let go

291
00:36:23,680 --> 00:36:32,640
completely and not cling to anything. And that is what leads us to true freedom. So, that is useful

292
00:36:32,640 --> 00:36:38,320
to us, and great. This is a great example. It's a great, it's sort of an epic, one of those epic

293
00:36:38,320 --> 00:36:45,040
stories that wouldn't become a movie in and of itself, I guess, but could be part of a movie about

294
00:36:45,040 --> 00:36:55,200
the Buddha, the sort of vignette or this look at Bahia. That's a great story, especially the ending

295
00:36:55,200 --> 00:37:01,120
where this very profound and concise teaching of the Buddha reminds us that seeing should just be

296
00:37:01,120 --> 00:37:06,000
seeing, hearing should just be hearing. Feelings should just be hearing and thinking should be

297
00:37:06,000 --> 00:37:14,560
just thinking. If you can do that, there will be no identifying, there will be no clinging. There

298
00:37:14,560 --> 00:37:21,840
will be no suffering. So, that's the Dhammapada verse for today, and that's our teaching.

299
00:37:21,840 --> 00:37:51,680
So, thank you all for tuning in, wish you all the best. Keep practicing.

