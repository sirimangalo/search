1
00:00:00,000 --> 00:00:14,000
Okay, welcome everyone to a morning session in second life.

2
00:00:14,000 --> 00:00:16,000
Good morning.

3
00:00:16,000 --> 00:00:28,000
For those of you in Europe, good evening.

4
00:00:28,000 --> 00:00:44,000
Today I thought I would talk about some of the criteria

5
00:00:44,000 --> 00:00:55,000
by which we use to decide what is the Buddhist teaching and what is not the Buddhist teaching.

6
00:00:55,000 --> 00:01:05,000
I would give a talk I think last week or not so far in the past.

7
00:01:05,000 --> 00:01:19,000
But all of the different schools of Buddhism and how in my mind it is really

8
00:01:19,000 --> 00:01:23,000
going against the purpose of Buddhism to split up into different groups

9
00:01:23,000 --> 00:01:27,000
that really when you look at all of the different types of Buddhism.

10
00:01:27,000 --> 00:01:51,000
What they claim to be unique about their school is actually generally

11
00:01:51,000 --> 00:01:57,000
not unique to anyone's school.

12
00:01:57,000 --> 00:02:03,000
And in fact what it seems is that each of the schools,

13
00:02:03,000 --> 00:02:10,000
the one that I belong to included, tends to go to extremes in regards to what they

14
00:02:10,000 --> 00:02:13,000
in regards to their specialty.

15
00:02:13,000 --> 00:02:17,000
And so rather than having a balanced approach,

16
00:02:17,000 --> 00:02:24,000
a Buddhist approach, they have a very specialized approach towards Buddhism,

17
00:02:24,000 --> 00:02:32,000
which tends to really miss the point and miss the whole elephant as I said.

18
00:02:32,000 --> 00:02:38,000
So following up on that, I thought it would be of interest to people to know

19
00:02:38,000 --> 00:02:43,000
what exactly is a good criterion.

20
00:02:43,000 --> 00:02:48,000
If we accept the idea that we should all just be Buddhist,

21
00:02:48,000 --> 00:02:52,000
then how do we know what is true Buddhism when what is not true Buddhism?

22
00:02:52,000 --> 00:02:54,000
And it's really not that difficult.

23
00:02:54,000 --> 00:03:00,000
We actually have some guidelines as to what is the Buddhist teaching and what is not.

24
00:03:00,000 --> 00:03:05,000
And so today I thought I'd bring up one such list in the tradition that I follow.

25
00:03:05,000 --> 00:03:12,000
But I think there's nobody in any tradition who would be likely to go against

26
00:03:12,000 --> 00:03:21,000
the general feeling of encompassed in these teachings.

27
00:03:21,000 --> 00:03:28,000
So it's in Pali, I thought I'd give everyone a treat and bring up some Pali

28
00:03:28,000 --> 00:03:36,000
so that we can have some original words of the Buddha

29
00:03:36,000 --> 00:03:41,000
and sort of get a feel for the language that the Buddha spoke.

30
00:03:41,000 --> 00:03:45,000
And so probably most of you are not going to be able to understand this.

31
00:03:45,000 --> 00:03:48,000
But let's just go through it.

32
00:03:48,000 --> 00:03:54,000
Atakho Mahapachapati kotami, nabakawati nupasankami.

33
00:03:54,000 --> 00:04:02,000
Atakho means at that time Mahapachapati kotami, the Bhikuni,

34
00:04:02,000 --> 00:04:07,000
who was a female monk named Gautami, Mahapachapati kotami.

35
00:04:07,000 --> 00:04:16,000
Pachapati means the sort of like mother.

36
00:04:16,000 --> 00:04:18,000
It's an ancient Indian word.

37
00:04:18,000 --> 00:04:22,000
And so it could either be used as her name or as an epithet to mean

38
00:04:22,000 --> 00:04:26,000
that she was the one who looked after the Buddha as he was growing up.

39
00:04:26,000 --> 00:04:28,000
Maha just means great.

40
00:04:28,000 --> 00:04:32,000
Gautami was her clan name or her given name.

41
00:04:32,000 --> 00:04:34,000
The Buddha's clan name was also Gautama.

42
00:04:34,000 --> 00:04:40,000
So we also always hear about Buddha Gautama.

43
00:04:40,000 --> 00:04:46,000
Yay nabakawati nupasankami, where the blessed one was there she went up to.

44
00:04:46,000 --> 00:04:48,000
It means she went to see the Buddha.

45
00:04:48,000 --> 00:04:53,000
Bhakawantang is just means it's an epithet for a leader of a religion.

46
00:04:53,000 --> 00:04:58,000
And so in Buddhism it means the Buddha.

47
00:04:58,000 --> 00:05:04,000
Bhakawantang abhiva ditwa ekamantang atasi.

48
00:05:04,000 --> 00:05:11,000
Having approached the Buddha and abhiva ditwa paid respect to the Buddha.

49
00:05:11,000 --> 00:05:15,000
It means most likely bowed down to the Buddha.

50
00:05:15,000 --> 00:05:22,000
Ekamantang atasi stood to one side or stood in an appropriate place.

51
00:05:22,000 --> 00:05:26,000
Because when you go to see the Buddha there's always going to be people

52
00:05:26,000 --> 00:05:30,000
attending upon him and coming to ask him questions,

53
00:05:30,000 --> 00:05:32,000
coming to listen to what he says.

54
00:05:32,000 --> 00:05:37,000
He generally would be surrounded just as all famous teachers.

55
00:05:37,000 --> 00:05:39,000
Generally our would be surrounded by people.

56
00:05:39,000 --> 00:05:42,000
So she stood to one side.

57
00:05:42,000 --> 00:05:48,000
And while standing on one side,

58
00:05:48,000 --> 00:05:52,000
Ekamantang tita, standing to one side, Maha Pajapati,

59
00:05:52,000 --> 00:05:56,000
Kottami, Bhakawantang eta davoucha. She said this.

60
00:05:56,000 --> 00:06:01,000
Sadhu meh vante, Bhakawa sankita ena tamang taisetu.

61
00:06:01,000 --> 00:06:04,000
Sadhu is a word that means good.

62
00:06:04,000 --> 00:06:07,000
Many people recognize this.

63
00:06:07,000 --> 00:06:14,000
We use it or it's used in India in the present day to talk about a holy man.

64
00:06:14,000 --> 00:06:16,000
And so it has a double meaning here.

65
00:06:16,000 --> 00:06:20,000
It means someone who is who is a good person, a sadhu.

66
00:06:20,000 --> 00:06:23,000
And it just means good. So sadhu here means good.

67
00:06:23,000 --> 00:06:25,000
She said it would be good for me.

68
00:06:25,000 --> 00:06:29,000
Venerable sir, if the blessed one could teach the dhamma in brief,

69
00:06:29,000 --> 00:06:36,000
sankita ena in brief, dhamma is the dhamma.

70
00:06:36,000 --> 00:07:00,000
So she wants to teach by which having heard the dhamma from the Buddha,

71
00:07:00,000 --> 00:07:08,000
she would be able to go off alone into seclusion.

72
00:07:08,000 --> 00:07:15,000
Aka means one, mupakata means to go out into seclusion.

73
00:07:15,000 --> 00:07:19,000
Appamata means to be diligent.

74
00:07:19,000 --> 00:07:27,000
She would be diligent and put out effort in her end well,

75
00:07:27,000 --> 00:07:35,000
giving up abandoning the dhamma.

76
00:07:35,000 --> 00:07:38,000
So basically speaking, she wants a brief teaching.

77
00:07:38,000 --> 00:07:42,000
And just going through this, we can understand,

78
00:07:42,000 --> 00:07:43,000
okay, what is she asking?

79
00:07:43,000 --> 00:07:47,000
She's asking for the most concise teaching that the Buddha can give her.

80
00:07:47,000 --> 00:07:52,000
And the Buddha gives her something very interesting.

81
00:07:52,000 --> 00:08:14,000
And this is the subject of the talk today.

82
00:08:14,000 --> 00:08:36,000
She says, whatever dhammas, whatever teachings go to me, you understand,

83
00:08:36,000 --> 00:08:45,000
or you know to be, when you know of those teachings that, number one,

84
00:08:45,000 --> 00:08:57,000
sara gaya sambhatanti noiragaya, they are for the purpose of developing lust,

85
00:08:57,000 --> 00:09:01,000
not for the purpose of giving up lust.

86
00:09:01,000 --> 00:09:08,000
Sanyogaya sambhatanti noiragaya, they are for the purpose of becoming attached,

87
00:09:08,000 --> 00:09:13,000
not for the purpose of giving up attachment, or for the purpose of bondage,

88
00:09:13,000 --> 00:09:18,000
not for the purpose of freedom from bondage.

89
00:09:18,000 --> 00:09:24,000
Atayaya sambhatanti noiragaya, yeah.

90
00:09:24,000 --> 00:09:35,000
They are the purpose of building up, not for the purpose of giving up.

91
00:09:35,000 --> 00:09:49,000
Mahi chataya sambhatanti noiragaya, they are for the purpose of great wanting,

92
00:09:49,000 --> 00:09:57,000
having of great desires, having many desires, not for the purpose of being of little desire,

93
00:09:57,000 --> 00:10:01,000
or having few desires, fewness of wishes.

94
00:10:01,000 --> 00:10:08,000
Santutya sambhatanti noiragaya, they are for the purpose of discontent,

95
00:10:08,000 --> 00:10:12,000
not for the purpose of contentment.

96
00:10:12,000 --> 00:10:22,000
Sankanikaya sambhatanti noiragaya, they are for the purpose of congregation,

97
00:10:22,000 --> 00:10:28,000
or society, socializing, not for the purpose of seclusion.

98
00:10:28,000 --> 00:10:36,000
Kosa jaya sambhatanti noiragaya, for the purpose of laziness,

99
00:10:36,000 --> 00:10:46,000
not for the purpose of putting out effort.

100
00:10:46,000 --> 00:10:50,000
They are for the purpose of being difficult to support,

101
00:10:50,000 --> 00:10:55,000
not for the purpose of being easy to support.

102
00:10:55,000 --> 00:10:59,000
What do you think are these things likely to be the dumb of the Buddha?

103
00:10:59,000 --> 00:11:09,000
Kosa jaya sambhatanti noiragaya sambhatanti noiragaya sambhatanti noiragaya sambhatanti.

104
00:11:09,000 --> 00:11:33,000
They are totally on the side of, you can be sure completely, you can hold perfectly to be true that nisodhamma,

105
00:11:33,000 --> 00:11:42,000
not the teaching of the teacher, of our teacher.

106
00:11:42,000 --> 00:11:44,000
And then he goes on to say the exact opposite.

107
00:11:44,000 --> 00:11:52,000
He said if those teachings that are for the purpose of giving up lust,

108
00:11:52,000 --> 00:12:00,000
and so on, and not for the purpose of having lust, of gaining lust,

109
00:12:00,000 --> 00:12:06,000
and so on, then you know these are the teaching of the Buddha.

110
00:12:06,000 --> 00:12:13,000
And this is, I think, a really good list, something that we can always use to verify for ourselves,

111
00:12:13,000 --> 00:12:16,000
and to not get caught up in particulars.

112
00:12:16,000 --> 00:12:24,000
That really the Buddha's teaching is not something high in mighty or high in,

113
00:12:24,000 --> 00:12:33,000
what do you say? It's not something esoteric or hard to understand, it's not something that is philosophical or theoretical.

114
00:12:33,000 --> 00:12:42,000
It's something quite simple, it's just generally totally against the way we look at the world.

115
00:12:42,000 --> 00:12:45,000
It's totally against our ordinary way of life.

116
00:12:45,000 --> 00:12:54,000
And diametrically opposed to our approach to existence.

117
00:12:54,000 --> 00:13:00,000
So it's not something, it shouldn't be thought of as something that you have to read,

118
00:13:00,000 --> 00:13:06,000
or have to study, or have to spend years and years just to understand it.

119
00:13:06,000 --> 00:13:13,000
And the years and years should be spent in practice in trying to perfect it.

120
00:13:13,000 --> 00:13:22,000
So what do we have here? We have altogether eight criteria by which we can tell whether something is the Buddha's teaching.

121
00:13:22,000 --> 00:13:25,000
Sara Gaiya, someone who doesn't know we are Gaiya.

122
00:13:25,000 --> 00:13:32,000
Buddha's teaching is for the purpose of giving up lust, for the purpose of giving up all desire.

123
00:13:32,000 --> 00:13:37,000
And a simple explanation of this is that any time you desire something,

124
00:13:37,000 --> 00:13:43,000
it means that you're not happy with things as they are.

125
00:13:43,000 --> 00:13:52,000
It's a very simple fact that desire is basically saying to yourself that given two realities,

126
00:13:52,000 --> 00:13:57,000
the reality in front of us, and the reality that's not yet in front of us,

127
00:13:57,000 --> 00:14:01,000
the latter is more preferable.

128
00:14:01,000 --> 00:14:07,000
And so it's a basic state of discontent at its root.

129
00:14:07,000 --> 00:14:17,000
It's basically a state of suffering, a state of discontent, of dissatisfaction with things as they are.

130
00:14:17,000 --> 00:14:23,000
And this is totally contrary to the Buddha's teaching because the Buddha teaches us

131
00:14:23,000 --> 00:14:31,000
to find contentment in the here and now, to accept this reality,

132
00:14:31,000 --> 00:14:45,000
or to find the way by the means by which reality becomes the preferred state,

133
00:14:45,000 --> 00:14:54,000
or becomes a totally peaceful, totally blissful state, where there is no need,

134
00:14:54,000 --> 00:15:08,000
no hopes or dreams or wishes.

135
00:15:08,000 --> 00:15:16,000
And especially no lust.

136
00:15:16,000 --> 00:15:21,000
Lust is, I suppose, one of those things that is on the extreme end.

137
00:15:21,000 --> 00:15:24,000
It's the most basic teaching in Indian religion.

138
00:15:24,000 --> 00:15:27,000
This was widely accepted, not only in Buddhism,

139
00:15:27,000 --> 00:15:32,000
and not even something that the Buddha came up with himself.

140
00:15:32,000 --> 00:15:38,000
Even before the Buddha was born even, there were teachings on giving up lust.

141
00:15:38,000 --> 00:15:43,000
It was agreed that the passions of the sense, attachment to sights and sounds

142
00:15:43,000 --> 00:15:47,000
and smells and tastes and feelings were not a religious path,

143
00:15:47,000 --> 00:15:49,000
were not a spiritual path.

144
00:15:49,000 --> 00:15:52,000
They were not something that could lead one to spiritual contentment.

145
00:15:52,000 --> 00:15:59,000
And in fact, it was agreed upon through the various practices that were in place

146
00:15:59,000 --> 00:16:05,000
that there was a much greater happiness and peace that could be found by giving these things out,

147
00:16:05,000 --> 00:16:08,000
that this was the root of addiction.

148
00:16:08,000 --> 00:16:16,000
It's the same as a drug addict to be addicted to sensual pleasures.

149
00:16:16,000 --> 00:16:21,000
In fact, it is a drug addiction because sensual, according to science,

150
00:16:21,000 --> 00:16:36,000
it's clearly understood in this day and age that what occurs when we receive a pleasurable stimulus at one of the senses

151
00:16:36,000 --> 00:16:38,000
is a chemical reaction in the brain.

152
00:16:38,000 --> 00:16:50,000
There's a release of certain chemicals, dopamine and whatever else is involved with the pleasure receptor.

153
00:16:50,000 --> 00:17:02,000
And that leads to a pleasurable feeling.

154
00:17:02,000 --> 00:17:06,000
It's a drug that gives us happiness.

155
00:17:06,000 --> 00:17:18,000
It does that for a short time and then goes away and leaves the receptors in a less than optimal state

156
00:17:18,000 --> 00:17:26,000
and that the next time we receive the stimulus, there's a release of less of the chemical

157
00:17:26,000 --> 00:17:34,000
and the receptors are not able to not able to process as much and so on.

158
00:17:34,000 --> 00:17:41,000
And so the system is not able to bring as much pleasure as much happiness as the first time and so on and so on.

159
00:17:41,000 --> 00:17:44,000
So it leads to more work for less pleasure.

160
00:17:44,000 --> 00:17:47,000
It's a drug addiction.

161
00:17:47,000 --> 00:17:55,000
When we talk about sex, when we talk about even just ordinary sensual pleasures,

162
00:17:55,000 --> 00:18:00,000
at the very base it's a drug addiction.

163
00:18:00,000 --> 00:18:07,000
So this is the most course of the defilements.

164
00:18:07,000 --> 00:18:12,000
I guess you could say the most course of our addictions that we have to get rid of.

165
00:18:12,000 --> 00:18:15,000
It's one of the most widely accepted in religious traditions.

166
00:18:15,000 --> 00:18:20,000
And I'd say the problem is not an agreement on giving them up.

167
00:18:20,000 --> 00:18:26,000
The problem is once you've given them up, how to then overcome the drug addiction?

168
00:18:26,000 --> 00:18:32,000
Because as we can see many people who, for instance, become celibate, wind up repressing the emotions.

169
00:18:32,000 --> 00:18:41,000
And as a result, there are quite dire consequences to repression.

170
00:18:41,000 --> 00:18:51,000
The eventual explosion of desire, you know, the release in other ways that can often be quite perverted, perverse.

171
00:18:51,000 --> 00:18:54,000
But at any rate, the Buddhist teaching is for the purpose of giving this up.

172
00:18:54,000 --> 00:19:02,000
It's for the purpose of seeing the negative side of this and seeing that in actual fact lust and the happiness,

173
00:19:02,000 --> 00:19:05,000
the pleasure that comes from it is not sustainable.

174
00:19:05,000 --> 00:19:08,000
It's not really intrinsically positive in any way.

175
00:19:08,000 --> 00:19:11,000
It's a feeling that arises and ceases.

176
00:19:11,000 --> 00:19:13,000
There's nothing bad about it.

177
00:19:13,000 --> 00:19:20,000
And I think really the problem that comes from religious people is they start to feel guilty when they have these feelings.

178
00:19:20,000 --> 00:19:24,000
They hear that people like the Buddha gave these things up.

179
00:19:24,000 --> 00:19:35,000
So they feel guilty when they have these feelings arise rather than actually acknowledging them and coming to understand the feelings.

180
00:19:35,000 --> 00:19:44,000
But at any rate, we can understand that any teaching that is for the purpose of giving rise to lust.

181
00:19:44,000 --> 00:19:57,000
There are even some religious teachings that encourage people to give rise to passion, to give rise to states of central intoxication.

182
00:19:57,000 --> 00:20:06,000
In fact, there are quite a many religions from what I've seen visiting and taking part in other religious traditions.

183
00:20:06,000 --> 00:20:12,000
You can see that a lot of the ordinary service is geared towards central stimulus.

184
00:20:12,000 --> 00:20:18,000
I would say even in Buddhism, there's a lot of this in sort of cultural Buddhism,

185
00:20:18,000 --> 00:20:40,000
where the means of attracting people's attention is central stimulus, the encouragement of this sort of addiction and the stimulation of the addiction system in the brain.

186
00:20:40,000 --> 00:20:56,000
And so this clearly is not the Buddha's teaching, it's something that we should get straight for ourselves that the practice of the Buddha's teaching has to be very clearly for the purpose of giving this out.

187
00:20:56,000 --> 00:21:16,000
The second one is that Buddhism is for the purpose of freedom from bondage, not for the purpose of bondage, becoming bound.

188
00:21:16,000 --> 00:21:28,000
So obviously in our life we, in a worldly sense, this is easy to see the difference between being bound and being unbound.

189
00:21:28,000 --> 00:21:38,000
In our lives we become bound by so many things, we become bound by our work, bound by our debts, bound by other people.

190
00:21:38,000 --> 00:21:50,000
We get caught up in so many things, so much, frivolity, we get caught up in the world very easily.

191
00:21:50,000 --> 00:21:54,000
But in spirituality we also get caught up in this as well.

192
00:21:54,000 --> 00:22:03,000
We become bound to rights and rituals, we become bound to our duties and to our station.

193
00:22:03,000 --> 00:22:08,000
We can even become bound to certain practices.

194
00:22:08,000 --> 00:22:14,000
We get caught up in the idea of being this or being that.

195
00:22:14,000 --> 00:22:21,000
San yoga, which means to be bound.

196
00:22:21,000 --> 00:22:42,000
This is why I stressed last time about not identifying as this type of Buddhist or that type of Buddhist, that we follow the Buddhist teaching should really be enough and we understand that the Buddhist teaching is not to become anything, not to be bound to anything, to say that I am this.

197
00:22:42,000 --> 00:22:55,000
Often when talking with Buddhist meditators, there still is this re-attachment to a tradition or attachment to a teacher.

198
00:22:55,000 --> 00:23:04,000
And there's an immediate judgment when we hear someone else practicing in a certain way.

199
00:23:04,000 --> 00:23:17,000
And rather than using the Buddhist principles to say why we believe this is better than that, we cling instead to our tradition.

200
00:23:17,000 --> 00:23:23,000
And everything is seen through the lens of that tradition.

201
00:23:23,000 --> 00:23:43,000
When I'm talking to Buddhist meditators, they immediately begin to repeat the things that their teacher has said rather than actually looking at things from the point of view of the Buddha and the point of view of the Buddhist teaching.

202
00:23:43,000 --> 00:24:10,000
It's very difficult to speak with such people that these sorts of people tend to just regurgitate whatever their teacher has said and are unwilling to accept the fact that other traditions might actually be of great benefit or actually have viable practice for the purpose of realizing the truth.

203
00:24:10,000 --> 00:24:14,000
When we practice Buddhism, we're simply trying to see reality for what it is.

204
00:24:14,000 --> 00:24:21,000
We have right here in front of us certain phenomena that are rising and ceasing in our body and our mind.

205
00:24:21,000 --> 00:24:25,000
We're not interested in being anything or being attached to anything.

206
00:24:25,000 --> 00:24:28,000
There's no need to call yourself Buddhist.

207
00:24:28,000 --> 00:24:33,000
There's certainly no need to call you this or yourself this or that type of Buddhist.

208
00:24:33,000 --> 00:24:43,000
Even in the tradition that I follow, there are different nikayas or groups and these groups now don't talk with each other.

209
00:24:43,000 --> 00:24:50,000
I put a picture up of that one of my old students sent me a lower donation.

210
00:24:50,000 --> 00:24:53,000
She's ordained as a female monk.

211
00:24:53,000 --> 00:25:00,000
I put it up on my website and a monk from Europe sent me an email saying don't.

212
00:25:00,000 --> 00:25:07,000
He said, Monday, don't please don't get involved in this schism.

213
00:25:07,000 --> 00:25:13,000
I wrote back and said, well, these people look quite harmonious to me.

214
00:25:13,000 --> 00:25:15,000
There doesn't seem to be any schism going on at all.

215
00:25:15,000 --> 00:25:18,000
They were happy and smiling and there were monks and nuns.

216
00:25:18,000 --> 00:25:21,000
There's about 20 of them in the picture.

217
00:25:21,000 --> 00:25:26,000
I said, honestly, you sound more like a schismatic to me.

218
00:25:26,000 --> 00:25:33,000
We cling to this idea that this is not correct and that is not correct for really no reason,

219
00:25:33,000 --> 00:25:36,000
no purpose and without really even thinking.

220
00:25:36,000 --> 00:25:40,000
Without thinking, we create barriers.

221
00:25:40,000 --> 00:25:51,000
We cling to things and we compartmentalize reality into our way, your way, right way, wrong way.

222
00:25:51,000 --> 00:26:00,000
When all the Buddha was really trying to teach us is to understand reality.

223
00:26:00,000 --> 00:26:09,000
Something that we should think about rather than clinging to principles and ideas.

224
00:26:09,000 --> 00:26:14,000
It's sure as best we can, we should try to keep rules and the regulations of the community.

225
00:26:14,000 --> 00:26:17,000
There's no question about that.

226
00:26:17,000 --> 00:26:25,000
It goes beyond that and we cling to my practice, your practice, mine is right.

227
00:26:25,000 --> 00:26:31,000
This idea that our teachers are perfect and so on.

228
00:26:31,000 --> 00:26:34,000
Therefore they must be right all the time.

229
00:26:34,000 --> 00:26:39,000
The Buddha said we're not to cling and when you see something that leads to clinging, that's not the Buddha's teaching.

230
00:26:39,000 --> 00:26:44,000
The third one is for building up and not for taking down.

231
00:26:44,000 --> 00:26:52,000
Buddhism is not for building anything up, not for creating anything, not for becoming anything.

232
00:26:52,000 --> 00:26:55,000
We don't want to become this or become that.

233
00:26:55,000 --> 00:27:02,000
We don't want to become a Buddhist or a Buddhist teacher or even become a monk.

234
00:27:02,000 --> 00:27:10,000
I always looked at becoming a monk for example, is really about more about what you aren't than what you are.

235
00:27:10,000 --> 00:27:21,000
The Buddha called it giving up the house life, going forth, leaving behind.

236
00:27:21,000 --> 00:27:26,000
This is a good example because in the Buddha's teaching we don't want to become anything.

237
00:27:26,000 --> 00:27:29,000
We don't have any desire to become anything.

238
00:27:29,000 --> 00:27:41,000
We don't want to build up in our minds identification with anything.

239
00:27:41,000 --> 00:27:58,000
We're not trying to build a Buddhist empire or a Buddhist we're not trying to spread the Buddhist teaching to everyone or turn Buddhism into this.

240
00:27:58,000 --> 00:28:08,000
The missionary religion or so on.

241
00:28:08,000 --> 00:28:12,000
In fact we're trying to take down, we're trying to take down much of who we are.

242
00:28:12,000 --> 00:28:20,000
We're trying to give up our identifications with this and with who we are and with our status in life.

243
00:28:20,000 --> 00:28:28,000
With ideas like class, social class and gender and so on.

244
00:28:28,000 --> 00:28:33,000
We're trying to in our minds give up the attachment to self.

245
00:28:33,000 --> 00:28:36,000
We're not trying to build up anything.

246
00:28:36,000 --> 00:28:40,000
We don't have the idea that we want to become reborn again as this or that.

247
00:28:40,000 --> 00:28:48,000
We don't have the idea that in this life we want to become famous or rich.

248
00:28:48,000 --> 00:28:57,000
Even becoming enlightened in meditation practice it's very easily for people to get attached to this idea that we're going to become enlightened.

249
00:28:57,000 --> 00:29:04,000
As though enlightenment was this attainment, this badge that you get to wear.

250
00:29:04,000 --> 00:29:10,000
Oh you've achieved something and become enlightened.

251
00:29:10,000 --> 00:29:20,000
Actually enlightenment is all about what you aren't.

252
00:29:20,000 --> 00:29:25,000
In light and being is not greedy, it's not angry, it's not deluded.

253
00:29:25,000 --> 00:29:28,000
In light and being is given up, it hasn't taken anything on.

254
00:29:28,000 --> 00:29:36,000
In our meditation practice it's very important, extremely important that we give up this notion of trying to attain anything.

255
00:29:36,000 --> 00:29:43,000
We're not trying to reach some state where the mind is perfectly calm all the time.

256
00:29:43,000 --> 00:29:55,000
Never thinking anything, never feeling any pain.

257
00:29:55,000 --> 00:30:04,000
We're trying to give up all expectations, all wants and needs, all attachments whatsoever.

258
00:30:04,000 --> 00:30:06,000
We don't need to become anything.

259
00:30:06,000 --> 00:30:14,000
It's more of a giving up of the path than the taking on of any path.

260
00:30:14,000 --> 00:30:16,000
Giving up of all paths.

261
00:30:16,000 --> 00:30:22,000
The path of the Buddha is to be here and now, to stop going, to stop coming.

262
00:30:22,000 --> 00:30:27,000
Buddha said in Bhana there is no going and no coming.

263
00:30:27,000 --> 00:30:35,000
Bhana isn't somewhere else, enlightenment isn't a place that we go to.

264
00:30:35,000 --> 00:30:42,000
It's when we stop going and stop becoming.

265
00:30:42,000 --> 00:30:50,000
The fourth one, Mahitya Thaya, Samwatanti, Noah Pitya Thaya.

266
00:30:50,000 --> 00:30:59,000
Buddha said those things that are for the purpose of wanting many different things, not for the purpose of wanting little.

267
00:30:59,000 --> 00:31:09,000
I think this goes along with much of what I've already said. I'm not going to go in much detail here.

268
00:31:09,000 --> 00:31:16,000
It's just another reminder that our wants are really the problem.

269
00:31:16,000 --> 00:31:22,000
We have these in our minds at every moment we want things in our daily life.

270
00:31:22,000 --> 00:31:25,000
Every situation in our lives becomes a want.

271
00:31:25,000 --> 00:31:27,000
Wanting this, wanting that.

272
00:31:27,000 --> 00:31:30,000
Chasing after this, chasing after that.

273
00:31:30,000 --> 00:31:31,000
We can't even sit still.

274
00:31:31,000 --> 00:31:35,000
We can't be happy with things as they are because of our many wants.

275
00:31:35,000 --> 00:31:40,000
Buddhism is to be a fewness of wishes to have few wants.

276
00:31:40,000 --> 00:31:46,000
We're not want a lot to be able to live without.

277
00:31:46,000 --> 00:31:56,000
This is a very practical teaching in terms of our necessities.

278
00:31:56,000 --> 00:32:04,000
When we're not able to acquire the things that we would normally feel necessary.

279
00:32:04,000 --> 00:32:12,000
We need this and need that to be able to live with little, to be able to live a simple life, a content life.

280
00:32:12,000 --> 00:32:15,000
This is number five, so these go together.

281
00:32:15,000 --> 00:32:31,000
Number five is, if the teaching is for discontent and not for contentment, then it's not the Buddhist teaching.

282
00:32:31,000 --> 00:32:40,000
Number six is rather important that I think is often missed in Buddhist practice or in spiritual practice.

283
00:32:40,000 --> 00:32:44,000
It's the need for solitude.

284
00:32:44,000 --> 00:32:47,000
The need to take time off.

285
00:32:47,000 --> 00:32:54,000
You hear a lot about how important it is to meditate in a group.

286
00:32:54,000 --> 00:33:04,000
How important it is to sit together in a group when you meditate.

287
00:33:04,000 --> 00:33:13,000
I think it's really a sign of the importance to not be in a group.

288
00:33:13,000 --> 00:33:23,000
When we practice in a group, it's obviously much easier because your mind is constantly being reminded.

289
00:33:23,000 --> 00:33:34,000
By the people around, you're reminded to focus, to not become distracted.

290
00:33:34,000 --> 00:33:42,000
It's quite easy in that sense, but the problem with it is, and you can see this if you ever take the time to go off on your own,

291
00:33:42,000 --> 00:33:54,000
is that when you're in a group, your mind is constantly going out to the people in the room around you.

292
00:33:54,000 --> 00:33:57,000
You're not actually as focused as you think you are.

293
00:33:57,000 --> 00:34:11,000
We think it's so easy to sit in a group because you constantly think of the people around you and you feel embarrassed if you slouch or if you mind wanders or if you think bad thoughts and so on.

294
00:34:11,000 --> 00:34:21,000
But there's actually, I would say, far less focus when you're together than when you're alone.

295
00:34:21,000 --> 00:34:27,000
When you're alone, you can actually see how your mind works much better because there's nothing for your mind to cling to.

296
00:34:27,000 --> 00:34:35,000
There's no external stimulus.

297
00:34:35,000 --> 00:34:41,000
I think it goes without saying that in the Buddhist teaching, it's not for the purpose of socializing.

298
00:34:41,000 --> 00:34:45,000
I'm sitting around talking and meeting up with friends.

299
00:34:45,000 --> 00:34:53,000
And this is a very important teaching in and of itself that when we practice the Buddhist teaching, we're not.

300
00:34:53,000 --> 00:35:00,000
It's very important that we can give up our need to socialize.

301
00:35:00,000 --> 00:35:15,000
That actually, it's quite a detriment to our practice to be addicted to friendships and association and going out to parties and chatting and socializing.

302
00:35:15,000 --> 00:35:21,000
A Buddhist meditator should be content with solitude.

303
00:35:21,000 --> 00:35:28,000
There is no replacement for being alone spending time with your own mind.

304
00:35:28,000 --> 00:35:37,000
Because it's really the only time that we can really be true to ourselves, where we don't have to put up a front and pretend to be something.

305
00:35:37,000 --> 00:35:47,000
Even when we're meditating, when we meditating in a group, so often we...

306
00:35:47,000 --> 00:35:56,000
So often we pull on a front and we don't want other people to think that we're a bad meditator.

307
00:35:56,000 --> 00:36:06,000
So we sit up straight and we put on a front, even when we're meditating.

308
00:36:06,000 --> 00:36:21,000
But when we're alone, we give this all up and we're able to be true and completely with ourselves, not focusing on anything else.

309
00:36:21,000 --> 00:36:31,000
Number seven is that the Buddhist teaching is for putting out effort, not for being lazy.

310
00:36:52,000 --> 00:36:59,000
It's important when we meditate where...

311
00:36:59,000 --> 00:37:05,000
We're not just meditating to feel peaceful and calm, to sit down and to get something.

312
00:37:05,000 --> 00:37:07,000
Meditation in a Buddhist sense is a word.

313
00:37:07,000 --> 00:37:09,000
The word we use is kamatana.

314
00:37:09,000 --> 00:37:18,000
It means to focus your mind, put your mind to work.

315
00:37:18,000 --> 00:37:24,000
When we practice meditation, we're not going to simply just sit there and fall asleep.

316
00:37:24,000 --> 00:37:27,000
It's not just a relaxation time.

317
00:37:27,000 --> 00:37:32,000
It's not our vacation time, our time away from life.

318
00:37:32,000 --> 00:37:40,000
Meditation is for the purpose of clearing the mind, for developing the mind.

319
00:37:40,000 --> 00:37:47,000
I think it's very easy to become lazy in our practice, where we practice maybe once a day, twice a day.

320
00:37:47,000 --> 00:37:57,000
We're not really developing our minds, but simply falling back into a state of peace and calm.

321
00:37:57,000 --> 00:38:08,000
It's important when we meditate that we actually examine our state of mind that we're looking at it and trying to develop our minds to become a better person,

322
00:38:08,000 --> 00:38:19,000
to clear our minds, to go deeper and deeper and to understand our minds clear and clear, to understand reality.

323
00:38:19,000 --> 00:38:26,000
We have to examine ourselves when we practice, are we just practicing to feel states of peace and calm and be content with that?

324
00:38:26,000 --> 00:38:28,000
Or are we actually learning something?

325
00:38:28,000 --> 00:38:34,000
I would say if you're practicing correctly, every time you sit down, you should learn something new about yourself.

326
00:38:34,000 --> 00:38:38,000
This is really a test as to whether you actually meditate it.

327
00:38:38,000 --> 00:38:46,000
Meditation is the contemplation, the examination of oneself.

328
00:38:46,000 --> 00:38:54,000
Number eight, to be easy to support, not difficult to support.

329
00:38:54,000 --> 00:39:08,000
This one, taking it to fold first in terms of our daily lives.

330
00:39:08,000 --> 00:39:13,000
I would say it's very easy to become very difficult to support, for most of us.

331
00:39:13,000 --> 00:39:16,000
We are quite difficult to support, we need so many things.

332
00:39:16,000 --> 00:39:23,000
Our minds are constantly searching out, seeking out.

333
00:39:23,000 --> 00:39:26,000
It's like looking after a little child.

334
00:39:26,000 --> 00:39:29,000
Looking after ourselves is like looking after a little child.

335
00:39:29,000 --> 00:39:35,000
Our wants and our needs and our nagging and complaining.

336
00:39:35,000 --> 00:39:40,000
It's very difficult for us to, for instance, find a good position for meditation.

337
00:39:40,000 --> 00:39:47,000
We often need the perfect position and so rather than simply sitting on the floor,

338
00:39:47,000 --> 00:39:49,000
we need a cushion and the cushion is not enough.

339
00:39:49,000 --> 00:39:53,000
We need a backrest and a backrest, not enough.

340
00:39:53,000 --> 00:40:01,000
We need cushions under our legs and a pillow behind our neck and so on.

341
00:40:01,000 --> 00:40:14,000
We live our lives in a state of general discontent, not being able to stick with the here and the now.

342
00:40:14,000 --> 00:40:19,000
Even right now you can examine yourself.

343
00:40:19,000 --> 00:40:23,000
When things arise in the mind, when things arise in the world around you.

344
00:40:23,000 --> 00:40:26,000
You're just sitting here listening.

345
00:40:26,000 --> 00:40:35,000
Maybe suddenly you start to feel pain in the legs or it's too hot or there's an itching sensation in the body.

346
00:40:35,000 --> 00:40:41,000
Maybe it's been half an hour already and it's starting to get boring.

347
00:40:41,000 --> 00:40:49,000
The mind starts to wander, starts to think of other things.

348
00:40:49,000 --> 00:40:52,000
You can see the mind is quite difficult to take care of.

349
00:40:52,000 --> 00:40:54,000
It's not easily amused.

350
00:40:54,000 --> 00:40:56,000
It's not easily entertained.

351
00:40:56,000 --> 00:41:01,000
It always needs something new and exotic, something fun and interesting.

352
00:41:01,000 --> 00:41:05,000
It can't be content just being here and just being now.

353
00:41:05,000 --> 00:41:15,000
It can be content with reality.

354
00:41:15,000 --> 00:41:21,000
So the Buddha said the Buddha's teaching is for the purpose of being easy to take care of.

355
00:41:21,000 --> 00:41:23,000
We don't need other people to take care of us.

356
00:41:23,000 --> 00:41:24,000
We don't need so many things.

357
00:41:24,000 --> 00:41:32,000
We don't need constant attention, constant gratification.

358
00:41:32,000 --> 00:41:37,000
That we can accept reality for what it is.

359
00:41:37,000 --> 00:41:41,000
That we only need a certain amount of food to eat.

360
00:41:41,000 --> 00:41:45,000
Simple clothes to wear.

361
00:41:45,000 --> 00:41:53,000
We're able to put up with anything that arises.

362
00:41:53,000 --> 00:42:01,980
So I thought I'd point this list out to everybody and let you know that there are general principles by which we can say what is the Buddha's

363
00:42:01,980 --> 00:42:02,980
teaching.

364
00:42:02,980 --> 00:42:06,980
So that was a list of those things that aren't just for completeness.

365
00:42:06,980 --> 00:42:09,980
Here's the rest of it.

366
00:42:09,980 --> 00:42:17,980
And just to recapitulate, to recapitulate, to go over it again.

367
00:42:17,980 --> 00:42:20,980
Those things that are for the purpose of giving up lust.

368
00:42:20,980 --> 00:42:25,980
Those things that are for the purpose of giving up bondage.

369
00:42:25,980 --> 00:42:32,980
Those things that are for the purpose of not collecting or not building up oneself.

370
00:42:32,980 --> 00:42:40,980
Those things that are for the purpose of having a few wants or few wishes.

371
00:42:40,980 --> 00:42:49,980
Those things that are for the purpose of contentment, for the purpose of seclusion, for the purpose of putting out effort,

372
00:42:49,980 --> 00:42:56,980
and for the purpose of being easy to support.

373
00:42:56,980 --> 00:43:03,980
You can be sure, or go to me, that this is the dhamma, this is the bhinnaya, this is the teaching of our teacher.

374
00:43:03,980 --> 00:43:07,980
Okay, so that's the dhamma for today.

375
00:43:07,980 --> 00:43:11,980
Thank you all for staying the course.

376
00:43:11,980 --> 00:43:14,980
If anyone has any questions, I'm happy to take them.

377
00:43:14,980 --> 00:43:19,980
Otherwise, thanks for coming.

