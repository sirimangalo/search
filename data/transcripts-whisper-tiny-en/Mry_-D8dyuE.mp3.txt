Okay, hello everyone, back with more questions and answers.
So today's question, you can go and read it if you like, if you're watching this live.
It's actually a rather complicated question, but I find two points in it.
And about the noting practice or the use of what I call mantra, mindfulness mantra of sorts,
using a word for making a note.
And so the first part is about different ways of noting.
This person has their own way of noting, I think.
I don't quite understand.
It's not not easy to tell exactly what this person is doing, how they're meditating,
even quite what they're asking over the internet.
So I'm just going to talk generally.
First about that, and second is about whether noting can cause clinging.
This is a common question, the second one, but it's too distinct.
Points, though related.
I think I can answer them sort of together.
So this person talks about noting experiences as the five aggregates.
So this one is the first aggregate, this one is form is physical.
This one is feeling this is perception, this one is thought, this one is consciousness.
And so there's a question, I'm not even sure that they're asking this question,
but I have a question that I want to answer is whether this is a valid understanding of the technique
that we do or even a valid meditation at all.
So when we talk about meditation, this is a question I get sort of question I get is what is meditation
or is ex-meditation, is this thing that I do meditation or so on?
Meditation is some kind of mental practice.
It's where you do something with your mind, cultivating habits, cultivating or dealing with habits.
It can be about breaking down habits.
And so when you have an experience, there's many different things you can do.
You can think about it.
You can react to it.
You can try and suppress it or you can try and find a way to avoid it.
You can go with it or engage with it, try and encourage it to continue.
You can enjoy it.
You can ignore it.
And so what happens with some types of meditation or practice that appear to be meditation,
things that become a meditation in the Western sense where you mull or ponder something.
And I think this is what this person is talking about.
And so I don't consider it to be a valid sort of meditation of the sort that we're doing.
If you sit and when you have an experience, suppose you feel pain and you contemplate it from Buddhist perspective.
If you think about it, hey, this is impermanent.
Hey, this is suffering.
Hey, this is non-soul for you to think, oh, this is just pain.
And you try and tell yourself something about it.
If you, well, not this is just pain, actually, that would be a little better, I think.
But so I'll explain why, but if you make it out to be more than it is, you create abstract thought about it.
You're not actually cultivating mindfulness.
You're evoking the states of thought, states of contemplation.
So you're actually not paying attention to the experience, which turns out to be quite important from a meditative or a mindfulness perspective.
The difference between that and saying to yourself, this is pain or even just pain or the thought saying,
this is a thought or thinking, is you're actually reminding yourself something very fundamental and basic about the experience.
And what it evokes is a free understanding and a free consciousness.
A consciousness that is free from any kind of judgment, any kind of abstraction, any kind of diversification, making more out of the experience than it actually is.
So it's actually quite a special interaction in that it's meant to its design to, and it has the effect of creating an objective state of mind where you simply experience.
The object as it is.
When we talk about things like patience and equanimity, even peace and tranquility that are all wrapped up in this idea of not doing anything.
So what we're trying to do with that is to find that state where we don't create, where we don't proliferate, or we don't make anything out of it.
It's called Baba. Baba, if you're familiar with Buddhist theory, is the link in the chain that leads to suffering.
When you make something out of something, so if someone says a simple worldly example, if someone says something to you like, you're a jerk, you're a dummy, you're this, you're that.
If someone says you're fat, you're thin, or so on.
If someone says something, maybe even something innocent to you, and you make something out of it, you get upset about it.
You've created Baba means being or becoming, you make something, you cause something to become.
You make a deal out of it when you make a big deal out of something, or any kind of deal.
So the use of the mantra, when it's tautological in a sense of, it's not saying anything new about the experience.
It's just reminding you, in the way of trying to experience the thing, simply for what it is.
It's unique in that what it evokes again is nothing really.
It's an absence, and it should feel like that. It should feel like you've got a respite.
You've been given as a moment that is free from any of this thinking, or contemplating, or diversifying.
So the first part of this question, this person who talks about applying Buddhist theory to the experience, and trying to, a common one that he wasn't mentioned,
is saying to yourself, hey, that's impermanent. If you start to contemplate it as impermanent, it's a poor translation, or it's a misleading translation.
When we read text where it says, one contemplates something as impermanent.
It's generally better or more literal to say, seeds, or understands, or knows the thing as impermanent.
That gets into our second part.
But I'll just say a few more things about what else comes.
I say when you note, you're really doing nothing, or you're creating nothing.
The result is a state that is free, but there's much more to it than that.
It's not just simply nothing.
First of all, you're creating this objectivity, but second of all, you're creating a habit.
You're creating a habitual awareness, or a habitual interaction with experience.
So without meditating, a person who never has any idea to practice meditation is constantly engaging with experiences habitually.
We think, and this person even mentions it, I think, in their questions about how when you experience something immediately you react to it.
It's like this person explains it as being bound up in the experience.
There's liking and disliking, and it's just a part of the experience.
It's so immediate.
So we would see this as habit.
Our habits that we develop throughout our life in a kind of a similar way to the way we develop habits and meditation.
But the habits and meditation, especially mindfulness meditation, are much more simple and singular.
And so by saying to ourselves, pain, pain, or thinking, thinking, and evoking habit of experiencing things just as they are, it flows into our ordinary lives.
And so we'll find ourselves walking down the street and experiencing things without stress, having thoughts, having interactions with people.
We'll find ourselves at work in situations where we would be stressed, where we would be upset in family situations, where we would be reactive or reactionary.
And finding ourselves far less reactionary because of the habits that we're developing. It's not magic. And it's not quick. It's not a quick fix.
Because we're dealing with old habits that are years and years and even lifetimes perhaps old.
But it happens quite clearly. And the evidence you can see for yourself, right? The Buddha said, teaching was something in the sea. For this very reason, it's not magic.
It's not I'm talking about God or heaven or spirit or magical powers.
I'm talking about simple principles of cultivating habits and their effect on it.
The third thing it does is it breaks down bad habits. And that's, you know, as a part of building up good habits, is that all these reactions don't have the opportunity.
So when you do nothing, we decrease and weaken our capacity, our potential to react to things.
The fourth thing it does is it helps you to see more clearly your experience.
So you'll see clearly how bad habits are bad. You'll see how bad anger is and greed is.
And I can see it in arrogance and all that worry and restlessness and distraction and so on.
But you'll also see more clearly the things that you like, the things that you normally like, the things that you normally dislike.
You'll see clearly your thoughts and your rationalization or these investigations that go on in the mind.
You'll see them clearly as well.
And the great thing about that is that's where you see impermanence, suffering and nonself. You'll see that the things that you clung to or held onto or wanted are not worth wanting.
Because the stability you saw in them, the satisfaction you saw in them, the control that you thought you had over them goes all too pot.
It turns out to be an illusion. It turns out to be something you cooked up in your ignorance.
And when you look more clearly at the experiences, you see that it's all unpredictable, in constant impermanence.
It's unsatisfying and it's uncontrollable and there's no thing that you can control.
So the nature of things is not about things that are possessible or controllable.
And you'll see the chaos in your mind and in your body and so on.
So these are the things that happen when you are mindful.
When you just say to yourself, pain, pain, thinking, thinking.
Question of whether noting could lead to clinging.
It's often when someone first hears about this idea and we mentioned things like saying to yourself, angry, angry.
It's often a question where the person asks, wouldn't that just make the anger worse?
Wouldn't that just make me want the thing or create a thing more?
And I suppose it's a reasonable or it's unsurprising that people will ask this, but for someone who's done meditation,
it's hard to understand how someone could think that or how that could be possible.
You might as well think that the sun might rise in the west of something.
But to break it down intellectually, I'm going to give a talk, you know, talk a little bit intellectually.
Just why it isn't so because if you practice meditation, you'll see that it isn't.
So if you practice saying to yourself, pain, angry, angry or liking or whatever,
if you say to yourself, pain, pain, why it doesn't lead to bad things?
When we talk about why this is, it goes back to what I've just been talking about.
But more particularly is talking about what it is that leads to clinging.
Why is it that we cling to things? When we have something pleasant, why do we want it?
And when we have wanting, what is it that leads us to go?
And what is it that leads us to want more? When we have anger, what is it that leads to more anger?
So it's first words noting that we can't actually be mindful in the present moment.
And this is especially the case with anger and greed, for example, or unwholesome states.
So when you have an experience of thought, for example, the thought has to come first,
then you have to be mindful of it. You have to remind yourself that was thinking.
So it's actually being mindful of something that happened in the past.
And that's important because we have to understand what's going on here when we get angry or when we have clinging or so on.
When we have an experience and then we cling to it, what is the process by which that occurs?
So when we have anger, for example, anger and mindfulness can't exist in the same mindset.
So what we're actually being aware of is the fact that we were angry.
It's an acknowledgement of the fact that we're angry saying there was anger or there was anger and then saying to ourselves that was anger.
So how that's different or how that's different from escalating or why that would in no way escalate is because escalating is when you say that something is this or that.
When you have anger and you say that's bad.
Or when the anger leads to a headache or attention or pain or sadness or so on, and you react to that, you say that's bad.
So if someone says something to you and you get angry and the anger comes and then you think about what they said.
You remember again what they said and this happens very quickly.
You'll say, did you just say you're angry and angrily you'll say to them or to yourself, did you just say to me, I'm this or I'm bad.
And that makes you more angry because you're evoking another experience after the anger and you're going to angry about that.
And you're building up based on habits, you're escalating into more and more anger.
And the same goes with this is similar to how I talk about talked about anxiety when you're anxious and then there's the physical feelings that come from anxiety and they make you more anxious.
You get anxious because I'm anxious and you can feel it when it makes you more anxious and you can have a panic attack if it gets worse.
When you say to yourself, for example, angry, angry or when you say to yourself, pain, pain, why it doesn't lead to bad things?
Why the anger doesn't lead to more anger is because you're saying this is this rather than saying this is bad in creating a reaction, you're cutting the chain.
It's quite simple and quite obvious. I think the question is based on our ordinary use of words.
In an ordinary sense we might say, I'm so angry, but we don't mean it as an acceptance and an understanding that I am angry.
We mean it as I'm getting angry or I want to be more angry.
When you say I'm so angry, you're actually building more anger in the ordinary sense because you're not just being objective about it.
So I don't know. A couple of interesting ideas about the noting technique about mindfulness in general.
I think I take this approach because I think it's important to talk about this process of noting explicitly in detail.
Sometimes I make videos about Buddhism and about meditation, talking about concepts and ideas without explicitly talking about the technique.
It seems like sometimes people miss that and I've talked to people who are interested in my videos who have found them helpful.
But you don't yet understand how it is repractives and so it stays on this intellectual level.
I think it may have been a case with this person where they were engaged in analyzing the experiences which is different from being mindful of them.
So there you go. I'm just a video for tonight. Thank you all for tuning in.
