1
00:00:00,000 --> 00:00:24,120
Today, we begin with the ten full skill in giving attention, so the first set of instruction

2
00:00:24,120 --> 00:00:35,280
is the recitation, first, verbal recitation, and then mental recitation, and then view them as

3
00:00:35,280 --> 00:00:44,960
to color, as to shape, as to direction, as to location, as to delimitation.

4
00:00:44,960 --> 00:00:51,480
And these are called the seven full skill in learning.

5
00:00:51,480 --> 00:01:02,080
So first, we have to learn the meditation subject, and now come the ten full skill in giving

6
00:01:02,080 --> 00:01:03,760
attention.

7
00:01:03,760 --> 00:01:13,640
So, they are one, following the order, two, not too quickly, and three, not too slowly,

8
00:01:13,640 --> 00:01:21,280
so, to the wording of distraction, five, surmounting the concept, six successive leaving,

9
00:01:21,280 --> 00:01:29,840
seven, absorption, and eight to ten, the three discourses, or three sultantas.

10
00:01:29,840 --> 00:01:36,400
So first, following the order, so from the time of beginning, the recitation, the attention

11
00:01:36,400 --> 00:01:41,640
should be given following the serial order without skipping, so you must go from one to

12
00:01:41,640 --> 00:01:49,320
thirty-two, and then back from thirty-two to one, so you are not to skip any one of them

13
00:01:49,320 --> 00:01:54,520
when you recite.

14
00:01:54,520 --> 00:02:04,040
And the second is, when you recite, you are not to do too quickly, because when you do

15
00:02:04,040 --> 00:02:12,680
it, you are not to remember them, like a men point, going on a journey, even if it has

16
00:02:12,680 --> 00:02:17,320
already done the journey out and back a hundred times, probably without taking note of

17
00:02:17,320 --> 00:02:19,360
turning to be taken and avoided.

18
00:02:19,360 --> 00:02:23,920
Though, in the finishes journey, he still has to ask, how to get there.

19
00:02:23,920 --> 00:02:29,720
We will be going to replace many times, but we don't know, we will not know how to go

20
00:02:29,720 --> 00:02:33,320
there, especially when we are not driving.

21
00:02:33,320 --> 00:02:43,840
So, you have to go not too quickly, so that you remember all of them.

22
00:02:43,840 --> 00:02:51,160
And then the third is, not too slowly.

23
00:02:51,160 --> 00:02:59,920
If you do it too slowly, then you will not come to the end of the recitation, just

24
00:02:59,920 --> 00:03:05,200
like a man who lightens on the way among trees, rocks, pools, etc., will not finish the

25
00:03:05,200 --> 00:03:07,440
journey in the day.

26
00:03:07,440 --> 00:03:12,640
So, a metadata gives its attention to the metadata, subject to slowly, it does not get

27
00:03:12,640 --> 00:03:16,640
to be end, and it does not become a condition for distinction.

28
00:03:16,640 --> 00:03:27,560
A condition for distinction means a condition for progress, and distinction here means

29
00:03:27,560 --> 00:03:38,240
progress in meditation, including the realization of truth.

30
00:03:38,240 --> 00:03:46,040
The party word is recissor, and this word is used many times with regard to meditation.

31
00:03:46,040 --> 00:03:51,040
So, it means something new or something special.

32
00:03:51,040 --> 00:03:56,560
That means when we practice meditation, we discover one thing after another, and these

33
00:03:56,560 --> 00:04:02,440
discoveries are called, we say, special.

34
00:04:02,440 --> 00:04:08,600
Before we practice meditation, we did not do or know about the breathing or the parts

35
00:04:08,600 --> 00:04:09,600
of the body.

36
00:04:09,600 --> 00:04:14,320
Then after we practice meditation, we get samadhi or concentration, and so we begin to

37
00:04:14,320 --> 00:04:21,200
see things differently from when we did not meditate.

38
00:04:21,200 --> 00:04:28,760
So, these are called distinctions or special attachments or something.

39
00:04:28,760 --> 00:04:34,280
So this word will appear again and again in this book, and always it is translated as

40
00:04:34,280 --> 00:04:37,520
distinction in this translation.

41
00:04:37,520 --> 00:04:48,600
So, if we go too slowly, then we will not reach to the end of the meditation, and so

42
00:04:48,600 --> 00:04:57,920
it will not become a condition for progress, a condition for realization of truth.

43
00:04:57,920 --> 00:05:04,080
And then, the wording of distraction, he must work of temptation to drop the meditations

44
00:05:04,080 --> 00:05:08,480
subject and to let his mind get distracted among the variety of external objects.

45
00:05:08,480 --> 00:05:16,080
So, he must pay attention to the parts he is concentrating on, but not let his mind

46
00:05:16,080 --> 00:05:22,400
to be distracted to other objects.

47
00:05:22,400 --> 00:05:33,760
And when mind is often distracted, then you will not get to the stage of realization.

48
00:05:33,760 --> 00:05:39,320
And here it is compared to a man going on one foot by three feet.

49
00:05:39,320 --> 00:05:43,600
So, he must be very careful that he must not be distracted, because if he is distracted,

50
00:05:43,600 --> 00:05:49,760
then he will fall down into the clear of the clear, and then he will die.

51
00:05:49,760 --> 00:05:55,760
So, he shall give his attention to the wording of distraction.

52
00:05:55,760 --> 00:05:58,920
And then, some aren't in the concept.

53
00:05:58,920 --> 00:06:04,920
This name concept beginning with his body, he must be surmounted, and consciousness established

54
00:06:04,920 --> 00:06:07,040
on the aspect with response.

55
00:06:07,040 --> 00:06:13,480
Now, in the beginning, we cannot avoid concepts, because we lived in the convention

56
00:06:13,480 --> 00:06:22,120
of war, and we have to use conventional terms, like head hairs, body hairs, and so on.

57
00:06:22,120 --> 00:06:30,520
And in the beginning, we will be seeing the hairs, nails, teeth, skin, TV, what were

58
00:06:30,520 --> 00:06:33,680
which are called concepts.

59
00:06:33,680 --> 00:06:40,520
But we have to somehow, we have to go beyond the convention, beyond the concepts.

60
00:06:40,520 --> 00:06:50,360
If we just see the hair, hair, body hair, head hair, body hair, nail, skin, and so on,

61
00:06:50,360 --> 00:06:57,880
if we just see the appearance of picture of these parts, we will not get the notion that

62
00:06:57,880 --> 00:06:59,480
they are repulsive.

63
00:06:59,480 --> 00:07:08,840
And the purpose of this meditation is to allow the repulsiveness of these parts.

64
00:07:08,840 --> 00:07:15,840
So, we have to go beyond the concept, beyond the concept of head hairs, body hairs, and

65
00:07:15,840 --> 00:07:17,960
so on.

66
00:07:17,960 --> 00:07:30,320
So, when the person begins to meditate, and after some time he gets enough concentration,

67
00:07:30,320 --> 00:07:38,160
he will be able to surmount the concept and establish this mind on the repulsiveness of

68
00:07:38,160 --> 00:07:39,160
these parts.

69
00:07:39,160 --> 00:07:46,000
So, we have to see, we have to arrive at repulsiveness of these parts, not just of, not

70
00:07:46,000 --> 00:07:55,280
stop at just the parts, not like learning these parts, but trying to see repulsiveness

71
00:07:55,280 --> 00:08:03,560
in these parts, not like medical students.

72
00:08:03,560 --> 00:08:11,080
And then the next one is successive leaving, and in giving his attention, he should eventually

73
00:08:11,080 --> 00:08:17,680
leave out any parts that do not appear to him, that means that do not appear to his mind

74
00:08:17,680 --> 00:08:29,240
clearly, that he does not see clearly in his mind, although the metadata tries to see them

75
00:08:29,240 --> 00:08:35,760
in his mind, some parts may not become clear to him, or he may not see clearly some

76
00:08:35,760 --> 00:08:37,800
parts.

77
00:08:37,800 --> 00:08:44,520
So, his attention, in giving his attention, he should eventually leave out any parts that

78
00:08:44,520 --> 00:08:49,040
do not appear to him clearly.

79
00:08:49,040 --> 00:08:55,520
So, when he begins to give his attention to his head hairs, his attention then carries

80
00:08:55,520 --> 00:09:02,240
on till it arrives at the last part, so because he is familiar with these 32 parts, so

81
00:09:02,240 --> 00:09:08,080
he results and he pays attention and then his mind goes to the end and then comes back

82
00:09:08,080 --> 00:09:09,080
again four.

83
00:09:09,080 --> 00:09:16,000
But, some parts may not appear to his mind clearly, so he should drum, which would do

84
00:09:16,000 --> 00:09:24,200
not appear clearly in his mind, so he must take only those that are clear to him.

85
00:09:24,200 --> 00:09:31,200
So, this is called successive leaving, so leaving one after the other, until he arrives

86
00:09:31,200 --> 00:09:40,320
at the last one, as he persists in giving his attention to some parts appear to him and

87
00:09:40,320 --> 00:09:41,320
others do not.

88
00:09:41,320 --> 00:09:48,120
Then he should work on those that appear till one out of any two appear clear, so

89
00:09:48,120 --> 00:09:58,120
he drops one by one until there are only two dead dead, that are clear as to him, and

90
00:09:58,120 --> 00:10:10,000
then he tries to find out which is clear among these two, so he should arrive at absorption

91
00:10:10,000 --> 00:10:16,800
by again, by again and again giving attention to the one that is appear as thus.

92
00:10:16,800 --> 00:10:27,400
So, eventually he ended with one part, with his clear as to him, and then he is similarly

93
00:10:27,400 --> 00:10:37,760
given here with the 32 pounds and then among the hands and so on, and also another

94
00:10:37,760 --> 00:10:48,720
similarly is given in paragraph 17, a monk going for unsrounding in village, where there

95
00:10:48,720 --> 00:10:51,480
are 32 families or 32 houses.

96
00:10:51,480 --> 00:11:03,760
So, here unsfood, eater, simply mean a monk who goes for unsround, most monks in the

97
00:11:03,760 --> 00:11:12,960
golden days, and even now go for unsrounding every morning, so in such a monk is called

98
00:11:12,960 --> 00:11:22,360
here unsfood, eater, but actually whether a monk eats at the monastery from food brought

99
00:11:22,360 --> 00:11:30,960
by his devotees or he goes out collecting arms in the village, he eats unsfood, so every

100
00:11:30,960 --> 00:11:37,880
monk is an unsfood eater, but here unsfood eater really means a monk who goes out for

101
00:11:37,880 --> 00:11:38,880
arms.

102
00:11:38,880 --> 00:11:50,080
I do not want to use the one back, because monks do not back, although the one bikhu, the

103
00:11:50,080 --> 00:12:00,880
pali bikhu means one who backs, they are begging me just going round the village silently

104
00:12:00,880 --> 00:12:07,760
and whatever is given to him, he accepts, he does not say, please give me a food or something

105
00:12:07,760 --> 00:12:17,680
like that, that is not a lot of monks to do, so the unsfood eater really means a monk

106
00:12:17,680 --> 00:12:30,680
who goes for arms in the village every day, so he might go to a village with 32 families

107
00:12:30,680 --> 00:12:38,240
and then he got lots of pounds at the first house and he will drop one house, if he gets

108
00:12:38,240 --> 00:12:45,920
three lots at one house, he will drop another house and so on, and on that day he got

109
00:12:45,920 --> 00:12:51,920
his bowl full at the first house, then he will not go any further in here, he got

110
00:12:51,920 --> 00:13:03,560
sitting all at him, so he is at the same place, in the same way, they meditate on us,

111
00:13:03,560 --> 00:13:11,280
go through these 32 parts and then drop one by one that are not clear to him, now in

112
00:13:11,280 --> 00:13:20,720
the application of the stimuli, for a crop 71, the 32 aspects are like the village with

113
00:13:20,720 --> 00:13:26,080
the 32 families, the meditative is like the unsfood eater, the meditative is preliminary

114
00:13:26,080 --> 00:13:34,080
work is like the unsfood eater going to lift near the village, actually living near

115
00:13:34,080 --> 00:13:44,480
the village, or living depending on the village, what do you see there, say I will listen

116
00:13:44,480 --> 00:13:51,200
in the monastery, but I depend on this village for my food, how do you say that, living

117
00:13:51,200 --> 00:14:16,440
depending on the village, or what do you say, I will rely on the village for support,

118
00:14:16,440 --> 00:14:24,760
then number 7, S to absorption, absorption means Jana, so absorption part by part, there

119
00:14:24,760 --> 00:14:30,960
are the two parts, so taking one part and you practice meditation on it and you can get

120
00:14:30,960 --> 00:14:36,800
Jana, so the intention here is that it should be understood that absorption is brought

121
00:14:36,800 --> 00:14:49,120
above in each one of the parts, so you can get 32, 32 times of Jana, if you take one

122
00:14:49,120 --> 00:14:55,920
one part at a time, so you take the part head hairs and then practice on it, meditation

123
00:14:55,920 --> 00:15:01,840
on it and you get Jana and then you can take body hairs as the object of meditation

124
00:15:01,840 --> 00:15:08,840
and then practice meditation and you can get Jana again, something like that, so at the

125
00:15:08,840 --> 00:15:22,440
end of it we have a story of a monk who gains Jana, dwelling on all the 32 parts and the

126
00:15:22,440 --> 00:15:31,760
three sultantas, now a yogi must study these three sultas, the three discosas, and they

127
00:15:31,760 --> 00:15:42,120
are from the sulta, the collection of sultas, namely those on higher consciousness

128
00:15:42,120 --> 00:15:49,080
that is one sulta, on coolness that is another sulta and on skill in the enlightenment

129
00:15:49,080 --> 00:15:55,000
factors that is still another sultas, as they are published the linking of energy with

130
00:15:55,000 --> 00:16:03,320
concentration, so the first sulta is because there are three sines that should be given

131
00:16:03,320 --> 00:16:10,480
attention from time to time by a bikhu, intent on higher consciousness, higher consciousness

132
00:16:10,480 --> 00:16:22,000
which means shavata and bipasana, both, yeah, so a monk who is intent on higher consciousness

133
00:16:22,000 --> 00:16:28,520
that means who is practicing samata by meditation or bipasana meditation should give

134
00:16:28,520 --> 00:16:37,000
attention from time to time to the three sines, the first is the sign of concentration,

135
00:16:37,000 --> 00:16:46,640
now, the sign of concentration really means just concentration or the object of concentration,

136
00:16:46,640 --> 00:16:53,640
in the documentary it is common that the sign of concentration simply means concentration,

137
00:16:53,640 --> 00:17:03,520
or pedantic into concentration, or the object of concentration, and the sign of exertion,

138
00:17:03,520 --> 00:17:09,920
the same thing, the sign of energy or the sign of effort should be given attention from

139
00:17:09,920 --> 00:17:16,960
time to time, and the sign of equanimity, the same thing, equanimity itself or the object

140
00:17:16,960 --> 00:17:24,920
of equanimity, should be given attention from time to time, because if the bikhu, intent

141
00:17:24,920 --> 00:17:30,960
on higher consciousness gives attention only to the sign of concentration, then his consciousness

142
00:17:30,960 --> 00:17:40,040
may confuse the idleness, now too much concentration causes idleness, something like

143
00:17:40,040 --> 00:18:02,440
liberty, or something, sometimes, sleepiness, is that both the consonant samata or situation,

144
00:18:02,440 --> 00:18:12,240
too much concentration can cause, too much concentration means concentration in excess of exertion

145
00:18:12,240 --> 00:18:26,840
or effort, when you get too much concentration, you simply lose effort of exertion or exertion

146
00:18:26,840 --> 00:18:36,000
becomes less, so in that case, too much of concentration leads to idleness, leads to sleepiness

147
00:18:36,000 --> 00:18:44,880
or something, you are not higher, but you feel sleepy, or you feel, what about that,

148
00:18:44,880 --> 00:18:53,320
lethargic or something like that, so too much concentration is actually as bad as too little

149
00:18:53,320 --> 00:19:00,320
concentration, because as we have learned in these previous chapters, they must be balanced,

150
00:19:00,320 --> 00:19:09,600
the five faculties must be balanced when you practice meditation, and especially concentration

151
00:19:09,600 --> 00:19:22,160
must be balanced by effort, and too much effort will cause some of what, too much effort

152
00:19:22,160 --> 00:19:32,920
to agitation, now, if you have effort or exertion in excess of concentration, then the

153
00:19:32,920 --> 00:19:44,080
condition will come in, concentration is somebody, so this is mindfulness, so when there

154
00:19:44,080 --> 00:19:56,720
is too much energy, too much vivia, then there is agitation, the example, like unbeknownia

155
00:19:56,720 --> 00:20:05,960
is enlightened, oh yeah much, yes, Ananda was practicing meditation to become an other

156
00:20:05,960 --> 00:20:16,040
man, he was so too eager, and he saw he put too much effort, he walked up and down and

157
00:20:16,040 --> 00:20:21,800
practiced meditation, so that he would become an Arahan, but he couldn't achieve anything

158
00:20:21,800 --> 00:20:29,240
by that practice, so later on he stopped and reviewed his practice, and he saw that he

159
00:20:29,240 --> 00:20:36,400
had too much effort, so he slowed down, and then he was able to attain Arahan, so too

160
00:20:36,400 --> 00:20:44,720
much effort is also not good, and then if you pick too much attention to the sign of

161
00:20:44,720 --> 00:20:54,520
the quantumity, what's that, it's a big intent on higher consciousness of fear of

162
00:20:54,520 --> 00:20:58,880
attention, only with the sign of economic intent, this consciousness may not become rightly

163
00:20:58,880 --> 00:21:18,360
concentrated for the destruction of kangas, because it leads to, how to call that, the

164
00:21:18,360 --> 00:21:31,760
solution, it leads to not knowing ignorance, because the economy is close to ignoring,

165
00:21:31,760 --> 00:21:41,000
so when you ignore something, you become ignorant, but the economy is not ignoring, actually,

166
00:21:41,000 --> 00:21:51,640
it takes us object to think, and it is not attached to it or not imposed by it, that

167
00:21:51,640 --> 00:21:58,760
is what is called equality, and there is another thing which is wrongly called equality,

168
00:21:58,760 --> 00:22:05,720
but actually it is not, and that is ignored, sometimes you ignore something, and you

169
00:22:05,720 --> 00:22:26,480
ignore it, and then there can be no destruction of kangas or mental defalments, so it

170
00:22:26,480 --> 00:22:39,440
one should pay attention from time to time, sometimes to the sign of concentration, sometimes

171
00:22:39,440 --> 00:22:43,800
to the sign of exertion, and sometimes to the sign of equanimity, and then there is a

172
00:22:43,800 --> 00:22:55,080
summary of the Kolsnith. The second Suddha on page 267, now paragraph 77 is Vikhus, one of

173
00:22:55,080 --> 00:23:02,400
Vikhus has six things, he is able to realize as to bring coolness, coolness of his mind,

174
00:23:02,400 --> 00:23:07,520
what's it, here Vikhus, when consciousness should be restrained, he restrains it, so there

175
00:23:07,520 --> 00:23:16,040
are times when you have to restrain your mind, you are too elated or too heavy with your

176
00:23:16,040 --> 00:23:21,320
practice, and if you are too elated and too heavy, you will lose concentration, so you

177
00:23:21,320 --> 00:23:29,640
have to, something like suppress your mind or restrain your mind, so when consciousness

178
00:23:29,640 --> 00:23:35,040
should be restrained, he restrains it, when it should be exaggerated, when consciousness

179
00:23:35,040 --> 00:23:42,640
should be encouraged, when consciousness should be looked on earth with equanimity, he looks

180
00:23:42,640 --> 00:23:50,600
on earth, earth is with equanimity, and he restrains on the superior state to be attained,

181
00:23:50,600 --> 00:23:57,160
he delights in Nipana, possessing these six things, it is able to realize as to bring coolness,

182
00:23:57,160 --> 00:24:06,880
so it wasn't was no, when to encourage his mind, when to suppress his mind, when to

183
00:24:06,880 --> 00:24:18,600
exert and all these things, then the third soldier is called the skill in the enlightenment

184
00:24:18,600 --> 00:24:25,040
factors, it is already dealt with in the explanation of skill in a sub-social, so chapter

185
00:24:25,040 --> 00:24:32,720
4, paragraph 51, it is a tender passage, beginning because when the mind is slack, that

186
00:24:32,720 --> 00:24:39,120
is not the time for developing the tranquility and enlightenment factor and so on, so the

187
00:24:39,120 --> 00:24:43,520
medira should make sure that he has every hand with this seven full skill in learning

188
00:24:43,520 --> 00:24:49,920
well and has probably defined this ten full skill in giving attention, just learning

189
00:24:49,920 --> 00:25:02,400
the meditation subject probably with both kinds of skill, so whenever something is described

190
00:25:02,400 --> 00:25:09,360
in this book, it is described in detail and very serious manner, so before you practice

191
00:25:09,360 --> 00:25:14,160
this meditation first, you have to learn, you have to do parts by heart and then recite

192
00:25:14,160 --> 00:25:21,320
and then you have to learn these skills, skills in learning and skill in practice, so now

193
00:25:21,320 --> 00:25:38,240
how can we do real practice, and there is a fourth note here, P-M explains, P-M means the

194
00:25:38,240 --> 00:25:49,440
sub-conmentory of wisdom demaga, so this is the irony that is the Bali original, rendered

195
00:25:49,440 --> 00:25:57,400
here by whenever there is occasion, with that means that means P-Bali to the current

196
00:25:57,400 --> 00:26:06,080
icity, when there is this, that reason consisting in a previous course, actually it

197
00:26:06,080 --> 00:26:16,560
should be, when there is this or that course accumulated in previous lives, not previous

198
00:26:16,560 --> 00:26:22,840
course, but accumulated in previous lives, it is something like a part of it, a part of

199
00:26:22,840 --> 00:26:31,680
it, a part of it, yeah, we call bar of it, they accumulated experience through the previous

200
00:26:31,680 --> 00:26:42,480
lives, perfection, perfection, now we come to the real practice, if it is convenient for

201
00:26:42,480 --> 00:26:47,240
him to live in the same monastery as the teacher, then he need not get it explained in

202
00:26:47,240 --> 00:26:52,920
detail to us, but as he applies himself to the meditation subject after he has made quite

203
00:26:52,920 --> 00:27:00,240
sure about it, he can have each successive stage explained as he reaches each distinction,

204
00:27:00,240 --> 00:27:09,160
so as he reaches each stage, he may go to the stage and ask about the other aspect, what

205
00:27:09,160 --> 00:27:13,400
he wants to live elsewhere, however, must get it explained to him in detail in the way

206
00:27:13,400 --> 00:27:18,160
already given, and he must stand it over and over, getting all the difficulties solved,

207
00:27:18,160 --> 00:27:22,800
he should live in a board of an uncutable kind, just describe in the description of the

208
00:27:22,800 --> 00:27:29,800
advocacy, and go to live in a suitable one, so then he should serve the minor impediments

209
00:27:29,800 --> 00:27:36,440
and sell about the preliminary work from giving attention to the request of this, so

210
00:27:36,440 --> 00:27:45,600
he should prepare properly for the practice of meditation, avoid the unsuitable monasteries

211
00:27:45,600 --> 00:27:52,640
and live in a monasteries which is suitable for meditation, and then several minor impediments,

212
00:27:52,640 --> 00:27:59,040
if his hair is long, he should cut his hair, if his nails are long, he cut his nails,

213
00:27:59,040 --> 00:28:07,840
and if his clothes are swively, then he should wash them, clean them and so on, so there

214
00:28:07,840 --> 00:28:17,600
is no, no, not even lesser impediments for the practice of meditation, and then he should

215
00:28:17,600 --> 00:28:23,280
set about it, when he sets about it, he should first apprehend the learning sign and

216
00:28:23,280 --> 00:28:29,200
take that hair as how, and then comes with the expression of these, so the scholarship

217
00:28:29,200 --> 00:28:37,000
we defined first by plugging of one or two hairs and placing them on the front of the hand,

218
00:28:37,000 --> 00:28:47,200
he can look at them in hair cutting place, that means in the monastery he cut hair of each

219
00:28:47,200 --> 00:28:53,680
other, and so the cut hair may still be at that place, so you can go there and pick up

220
00:28:53,680 --> 00:28:59,440
it here or two and look at it, or in a bowl of water or rice coolers, sometimes there

221
00:28:59,440 --> 00:29:06,760
is hair in the rice cooler, in a bowl of water, if you look at it, if the ones he sees

222
00:29:06,760 --> 00:29:11,360
a black, when he sees them, they should be brought to minus black, if white is white, if

223
00:29:11,360 --> 00:29:17,160
it is mixed, he should be brought to mind in a garden with those most preserved, so there

224
00:29:17,160 --> 00:29:21,880
are more black, then he sees black, and I am more white, he sees white, and as the may

225
00:29:21,880 --> 00:29:26,440
of the case of that hair, so too the sign should be apprehended visually with the whole

226
00:29:26,440 --> 00:29:33,840
of the skin content, that is the first fight, having every hand at the sign does, and

227
00:29:33,840 --> 00:29:38,720
define all the other parts of the body by cover, shape, direction, location, and

228
00:29:38,720 --> 00:29:45,720
delimitation, they should then define robustiveness in five ways, that is by color, shade,

229
00:29:45,720 --> 00:29:54,480
border, habitat, and location, and now we come to the individual parts, that the first

230
00:29:54,480 --> 00:30:04,000
is head hair, and in the description of these 32 first, there was, which are very difficult,

231
00:30:04,000 --> 00:30:17,640
because here they become and describe each part, comparing with what was familiar to

232
00:30:17,640 --> 00:30:29,640
them in those days, and also there are names of trees and flowers, which we don't know.

233
00:30:29,640 --> 00:30:34,080
So let's see the head hair, first we head hair is a black Indian normal color, so the

234
00:30:34,080 --> 00:30:45,560
color is black, normal color, maybe because the other was an Indian, and most eastern people

235
00:30:45,560 --> 00:30:54,440
have black hair, they are normal color is black, but it may be different in the west,

236
00:30:54,440 --> 00:31:03,920
the color of fresh reinta seeds, the reinta seeds, so maybe we don't know what the name

237
00:31:03,920 --> 00:31:14,920
of the tree called reinta, from this tree we get seeds of beads, we make beads with

238
00:31:14,920 --> 00:31:24,280
the seeds of this tree, they are black, as to shade, they are the shape of long ground

239
00:31:24,280 --> 00:31:29,920
measuring dots, as to direction, they lie in the upper direction, direction means part

240
00:31:29,920 --> 00:31:37,920
of the body, and the neighbor is the middle of the body, and upper direction means above

241
00:31:37,920 --> 00:31:44,600
neighbor, and lower direction means below neighbor, as to location, their location is

242
00:31:44,600 --> 00:31:51,360
being wet in a skin that envelopes this car, it is bounded on both sides by the roots

243
00:31:51,360 --> 00:31:57,840
of the ears in front by the forehead and behind by the name of the neck, as to delimitation,

244
00:31:57,840 --> 00:32:02,440
they are bounded below by the surface of their own roots, which are fixed by tendering

245
00:32:02,440 --> 00:32:07,320
to the amount of the tip of the rice grain into the inner skin that envelopes the hair,

246
00:32:07,320 --> 00:32:13,000
just a little bit, they are bounded above by space and all around by each other, there

247
00:32:13,000 --> 00:32:20,120
are no two hairs together, this is their deep limitation by this shimmer, that hairs

248
00:32:20,120 --> 00:32:24,520
are not body hairs, and body hairs are not what hairs, they in light was not in the

249
00:32:24,520 --> 00:32:31,000
mixed with the remaining 31 parts, the hairs are a separate part, this is their deep limitation

250
00:32:31,000 --> 00:32:36,800
by this shimmer, they are two kinds of delimitation by similar and by this shimmer, so by

251
00:32:36,800 --> 00:32:42,040
the similar means hairs are not body hairs, body hairs are not hairs, they are separate

252
00:32:42,040 --> 00:32:50,600
but they are different, and then determination as to repulsiveness in five ways, hairs

253
00:32:50,600 --> 00:32:59,600
are repulsed in color as well as in shape or the habitat and location, what is habitat,

254
00:32:59,600 --> 00:33:10,200
what is the difference between habitat and location, habitat has to do with the interrelationship

255
00:33:10,200 --> 00:33:18,120
of other factors within the same area, location doesn't have that interrelated aspect

256
00:33:18,120 --> 00:33:27,160
of other factors within the same area, I think.

257
00:33:27,160 --> 00:33:41,520
In this sub commentary, habitat or the valley was as a yeah, is explained as origin

258
00:33:41,520 --> 00:33:55,040
of course, if you grow some plants, then the plant grows on the top and it gets nutrition

259
00:33:55,040 --> 00:34:02,040
from the from the earth, that is called ashia in Bali, because we have to differentiate

260
00:34:02,040 --> 00:34:14,400
ashia and the other one, I mean habitat and location, so we will read this paragraph.

261
00:34:14,400 --> 00:34:19,800
For one thing, the color of a head hair in a bowl of inviting rice, pure or cooked rice,

262
00:34:19,800 --> 00:34:24,520
people have discussed that and say this has got hairs and they take it away, so they are

263
00:34:24,520 --> 00:34:25,520
repulsive in color.

264
00:34:25,520 --> 00:34:30,600
Also, when people are eating at night, they are likewise disgusted by the mere sensation

265
00:34:30,600 --> 00:34:39,280
of the hairshakes, heck of bark from mockery bark fiber, they are names of trees, so they

266
00:34:39,280 --> 00:34:45,680
are repulsive in shape and the odor of head hairs, unless dressed with the smearing of

267
00:34:45,680 --> 00:34:50,600
oil, sanded with flowers, etc., is most offensive and it is still worse when they are put

268
00:34:50,600 --> 00:34:56,440
in the fire, even if head hairs are not directly repulsive in color and shape, still the

269
00:34:56,440 --> 00:35:01,920
odor is directly repulsive, just as a baby is excrement as to its color, as the color

270
00:35:01,920 --> 00:35:06,960
of turmeric and as to its shape, it is a shape of a piece of turmeric root, and just as

271
00:35:06,960 --> 00:35:12,360
a bloated carcass of a black dog thrown on a rubbish sheet as to its color, as the color

272
00:35:12,360 --> 00:35:19,880
of the ripe parmara fruit, and as to its shape is a shape of a mandolin shaped drum that

273
00:35:19,880 --> 00:35:26,880
face down and its fangs are like just in buds, so even if both these are not directly repulsive

274
00:35:26,880 --> 00:35:31,760
in color and shape, still they are odor is directly repulsive, so even if head hairs are

275
00:35:31,760 --> 00:35:37,600
not directly repulsive in color and shape, still they are odor is directly repulsive.

276
00:35:37,600 --> 00:35:47,200
Now, I think instead of budge, we can say end, or we can just leave the budge, just

277
00:35:47,200 --> 00:35:52,800
as part of that grow on village sewage, then they feel the place at this customer, because

278
00:35:52,800 --> 00:36:00,640
this is a description about what drug that you had it at, so just as part of the grow

279
00:36:00,640 --> 00:36:06,240
on village sewage and the food we face are disgusting to civilize people and unusable,

280
00:36:06,240 --> 00:36:10,640
so also head hairs are disgusting, since they grow on the sewage of fast blood during

281
00:36:10,640 --> 00:36:20,120
time by the plant and the like, but they grow out of that, that is repulsive with regard

282
00:36:20,120 --> 00:36:26,120
to habitat, and these head hairs grow on the heap of fat that the one part has found

283
00:36:26,120 --> 00:36:32,120
as doom on a damn hill, and owing to the filthy place they grow in, they are quite as

284
00:36:32,120 --> 00:36:38,080
unhappy-dodding as vegetables growing on each other, on a million, etc, and load as

285
00:36:38,080 --> 00:36:43,840
a world of it is growing in grain and so on, this is the repulsive aspect of their lubrication,

286
00:36:43,840 --> 00:36:49,600
so their lubrication is repulsive and also they are happy that their origin is repulsive,

287
00:36:49,600 --> 00:36:57,400
so repulsive with regard to origin and repulsive with regard to location, as in the case

288
00:36:57,400 --> 00:37:03,040
of head hairs, so also the reposiveness of all the bastard we defined in the same fight with

289
00:37:03,040 --> 00:37:11,680
my color, shade, coder, habitat and location, here also I want to leave the world however

290
00:37:11,680 --> 00:37:22,320
out, so you can see moreover and all must be defined individually by color, shade, direction,

291
00:37:22,320 --> 00:37:33,280
location and delimitation as follows, next body hairs, so they are not difficult to understand,

292
00:37:33,280 --> 00:37:45,440
that hair, body hairs also, and then teeth, there are 32 bones in one whose teeth are

293
00:37:45,440 --> 00:37:57,800
complete, is that right, 32, 32 teeth, they do a wide end color as to shade their various

294
00:37:57,800 --> 00:38:04,720
shapes and then just fine, so these test equations align it, take textbook of another,

295
00:38:04,720 --> 00:38:17,520
then the next one skin, and here the skin means inner skin, not the outer skin, because

296
00:38:17,520 --> 00:38:23,960
the inner skin enveloped the whole body outside is, is what is called the outer cuticle,

297
00:38:23,960 --> 00:38:29,960
which is black, brown or yellow in color, when that front of whole of the body is compressed

298
00:38:29,960 --> 00:38:41,320
together, it amounts to only as much as if you threw that small foot, so what is outer

299
00:38:41,320 --> 00:38:50,880
most is not called skin, it is called cuticle cuticle cuticle cuticle, and under that there

300
00:38:50,880 --> 00:39:05,840
is skin, sometimes we have a soul or something here, and then that cuticle, you can remove

301
00:39:05,840 --> 00:39:15,520
and then there is skin out, like a pig, when a pig is killed, put it into the pot water,

302
00:39:15,520 --> 00:39:26,280
it breaks out of the cuticle and the skin becomes white, they also say that another, to make

303
00:39:26,280 --> 00:39:32,360
them more repulsive, the cuticle is there, you know, for this, what we call skin, what we

304
00:39:32,360 --> 00:39:42,640
see is there, that is said in another commentary, I don't know, it's an audible shade,

305
00:39:42,640 --> 00:39:51,720
looko dama is, looko dama is a real skin, there is no pigmentation, lots of pigmentation,

306
00:39:51,720 --> 00:40:01,200
lots of cuticle, because looko dama and looko dama is white color, an eggshell skin.

307
00:40:01,200 --> 00:40:07,680
So here is the skin, it's white color, but as you color the skin itself is white, and

308
00:40:07,680 --> 00:40:11,520
its whiteness becomes evident when the outer cuticle is just dried by the conti, with

309
00:40:11,520 --> 00:40:18,400
the flame of a fire or the impact of a blow and so on, so when you cut yourself and

310
00:40:18,400 --> 00:40:30,160
you see the white color there, and then about five lines down from a background 94, we

311
00:40:30,160 --> 00:40:38,400
have a, the skin of the thighs is the shape of a long sack of a full of paddy, paddy really

312
00:40:38,400 --> 00:40:56,600
means and cooked rice, without husks, and the next one is flesh, there are 900 pieces of

313
00:40:56,600 --> 00:41:07,640
flesh, muscles and other things, as to color it is all red like in sugar flowers, as

314
00:41:07,640 --> 00:41:11,360
to shape the flesh of the cow, to the shape of cooked rice, and they found with paddy

315
00:41:11,360 --> 00:41:24,280
and so on, and then sinews, there are 900 sinews, I don't know how they, how they

316
00:41:24,280 --> 00:41:35,040
count these sinews, because maybe months, months do not just like bodies, they may have

317
00:41:35,040 --> 00:41:45,960
landed from, from physicians, and then color and so on, and about the middle of the

318
00:41:45,960 --> 00:42:03,680
background, there's the white tendons, are tendons large and sinews, you see the white

319
00:42:03,680 --> 00:42:19,880
tendons, but here the paddy word means larger, larger sinews, sinews small and larger

320
00:42:19,880 --> 00:42:27,900
sinews are called, yeah, the paddy kandara, it is translated as tendons, yeah, so I take

321
00:42:27,900 --> 00:42:50,400
the tendons like bigger than sinews, and the next one is bones, and there are, excepting

322
00:42:50,400 --> 00:43:01,460
the 32-feet bones, these consist of the remaining number of bones, did you add them up, did

323
00:43:01,460 --> 00:43:16,900
you get 300, and then, so they are 300, it's correct, so 64-hand bones, 64-foot bones,

324
00:43:16,900 --> 00:43:23,420
64-soft bones, depending on the flesh, two-healed bones, then in each leg, two-hand

325
00:43:23,420 --> 00:43:30,260
bones, so they become four, two-shan bones, four, one knee bone, two, and one thigh bone,

326
00:43:30,260 --> 00:43:39,540
two, and then two-head bones, eating spine bones, are there 18 segments in the spine?

327
00:43:39,540 --> 00:43:54,140
What do you call them, in the spine, vertebrae, are they 18, no, I'm asking them because

328
00:43:54,140 --> 00:44:11,900
he's a dancer, and they include the terminal bones, the major components, maybe they

329
00:44:11,900 --> 00:44:12,900
keep up.

330
00:44:12,900 --> 00:44:38,140
So 18 spine bones, 24 rib bones, 14 rib bones, 14 rib bones, two collar bones, two shoulder

331
00:44:38,140 --> 00:44:44,180
rib bones, two upper arm bones, two pairs of four arm bones, seven neck bones, two jaw

332
00:44:44,180 --> 00:44:50,820
bones, one nose bone, two eye bones, two ear bones, one finger bone, what's that?

333
00:44:50,820 --> 00:44:59,300
Obstacle bone, it's simple bone, I don't know, and it's sensible bones, so they are exactly

334
00:44:59,300 --> 00:45:14,060
300 bones, now, it doesn't mean exactly 300, but it means about 300, so we should correct

335
00:45:14,060 --> 00:45:16,260
exactly about 300 bones.

336
00:45:16,260 --> 00:45:35,260
So there are about 300 bones, now this you go back to page 201,

337
00:45:35,260 --> 00:45:58,740
this is the body's nature, it is a collection of over 300 bones, so they are more than 300

338
00:45:58,740 --> 00:45:59,740
bones.

339
00:45:59,740 --> 00:46:06,140
So here, the word does not mean exactly, but it means about 300 bones, because some bones

340
00:46:06,140 --> 00:46:20,380
are not mentioned in the list, so there are more than 300 bones in the body.

341
00:46:20,380 --> 00:46:36,620
So the bones are described in detail, and then the next one is bone marrow, this is the

342
00:46:36,620 --> 00:46:52,540
bone marrow inside the various bones, as to color, it is right and so on, it's going to

343
00:46:52,540 --> 00:47:11,060
be

