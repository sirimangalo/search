1
00:00:00,000 --> 00:00:20,640
and we're live. Good evening, everyone. Broadcasting live August 11th, today we're looking at a

2
00:00:20,640 --> 00:00:36,120
small quote on the difference between study and practice. When the Buddha was

3
00:00:36,120 --> 00:00:44,080
had to be Wija, Jiren Asampano, it's one of the great qualities of the Buddha that he was

4
00:00:44,080 --> 00:00:57,600
accomplished both in knowledge and conduct. By time and again, we see him reminding us that the

5
00:00:57,600 --> 00:01:03,680
important, the more important aspect of that is conduct.

6
00:01:03,680 --> 00:01:17,880
And so he says someone who even knows just a four line stands of them. If they

7
00:01:17,880 --> 00:01:26,600
result to live in accordance with it, they can be said to be called Bahu Sutta. Let's see what the

8
00:01:26,600 --> 00:01:40,880
how he is. Bahu Sutta, Dhamma, Tarou.

9
00:01:40,880 --> 00:02:05,720
It's just helping you work and ask me to translate it. It's not what he says here. This is the

10
00:02:05,720 --> 00:02:27,800
thing, the wrong source. Here he says, Bahu kobikumaya dhamma de Sita jutta, Jpbikumta. Oh, here we are.

11
00:02:27,800 --> 00:02:37,600
Yeah, it may be that someone has learned a lot of the dhamma. This means the sutta, the

12
00:02:37,600 --> 00:02:45,240
gaya, the nine types of dhamma. Sutta, gaya, waya, karna, gata, ullana, ityutta, khanjataka, ubutta,

13
00:02:45,240 --> 00:02:53,560
dhamma, wadla. These are the nine parts of the teachings. And then he says, or, or,

14
00:02:53,560 --> 00:03:09,040
or, but if Tatupadaya, Ambikhu, Ambikhu is known. Ambikhu can be a dhamma, nudhamma,

15
00:03:09,040 --> 00:03:18,560
but, Ambikhu is a dhamma, nudhamma, patipan, one who practices the dhamma to real, for the

16
00:03:18,560 --> 00:03:36,920
realization of the dhamma, means to realize nibanda. Even if they only, only know the four

17
00:03:36,920 --> 00:03:51,640
verses, or pada, or four words, maybe. No, four, four line stands around. Hotee bahu sutto, dhamma darou.

18
00:03:51,640 --> 00:04:05,680
He's called the bahu sutta, one who has learned a lot, and one who carries the dhamma,

19
00:04:05,680 --> 00:04:12,600
who pulls the dhamma. And the key here is dhamma, nudhamma, patipan, no one who practices the

20
00:04:12,600 --> 00:04:18,000
dhamma for the realization of the dhamma. The dhamma is of two parts. There is the practice,

21
00:04:18,000 --> 00:04:29,400
and there is the goal, or the ultimate truth, you can say. So you're practicing the dhamma,

22
00:04:29,400 --> 00:04:35,080
means you're practicing the truth in a way, you're practicing the true path, the right path.

23
00:04:35,080 --> 00:04:41,480
But it's to realize the dhamma to realize the truth, two different aspects of the dhamma.

24
00:04:41,480 --> 00:04:53,280
That's why it's called dhamma, nudhamma, dhamma for the realization of the dhamma.

25
00:04:53,280 --> 00:04:59,240
The Buddha said that there are five types of people who can call themselves. We say dhamma,

26
00:04:59,240 --> 00:05:12,600
we have the ones who live by the dhamma. We have one who has studied a lot, one who teaches, who recites a lot,

27
00:05:12,600 --> 00:05:22,480
one who thinks a lot, and one who teaches a lot. And then these are considered to be living

28
00:05:22,480 --> 00:05:30,520
by the dhamma. They said there are four qualities that you need to be living by the dhamma. One you need to

29
00:05:30,520 --> 00:05:41,040
not let the days and the names go by, not give a sanwati namati. It doesn't waste days and

30
00:05:41,040 --> 00:05:54,200
it doesn't waste days. Not in Chateepati Sala and it doesn't give up seclusion. It doesn't demand and secluded one's

31
00:05:54,200 --> 00:06:05,880
own dwelling, one's seclusion. It doesn't break through and get involved in society. One from time to time takes up seclusion.

32
00:06:05,880 --> 00:06:19,600
And you need to be in Chateepati Sala. You need to get involved in Chateepati.

33
00:06:19,600 --> 00:06:37,000
You need to get involved in Chateepati. You need to get involved in Chateepati. You need to get involved in Chateepati.

34
00:06:37,000 --> 00:06:43,360
So, practice is to tranquilize, to calm the mind, not to let the mind react and snowball and make

35
00:06:43,360 --> 00:06:52,080
more out of things than they actually are. They just experience things as they are in the moment.

36
00:06:52,080 --> 00:07:03,960
And, put the rindjasat. And the rindjasat. The rindjasat. The rindjasat. The rindjasat.

37
00:07:03,960 --> 00:07:17,960
With wisdom, one comes to understand the higher meaning of the dhamma. So, this one is quite interesting

38
00:07:17,960 --> 00:07:23,960
because it separates meaning into two categories. So, there's the lower meaning and there's

39
00:07:23,960 --> 00:07:29,640
the higher meaning. The dhamma that we study, if you understand it, you understand the words and

40
00:07:29,640 --> 00:07:34,920
you understand the concepts that are being discussed. That's one level of understanding. Most of us can get

41
00:07:34,920 --> 00:07:42,200
that just by when you read this quote and you think, okay, I understand what he's saying. He's got this view

42
00:07:42,200 --> 00:07:50,600
that better to practice a little than to learn a lot than not practice. But to really understand that,

43
00:07:50,600 --> 00:07:56,200
even this simple quote is quite different to actually have it, again, understanding with wisdom.

44
00:07:56,200 --> 00:08:01,000
And to know that for yourself, because right now it looks like a view, if you look at it, you think,

45
00:08:01,000 --> 00:08:07,160
well, maybe he's right. But once you've practiced, if you really understand and come to the

46
00:08:07,160 --> 00:08:21,000
understanding, the higher wisdom, which lets you see the truth. In this case of how

47
00:08:21,000 --> 00:08:30,600
practice a little thing, a little truth, a small truth, a core of the truth, a core of the dhamma

48
00:08:31,480 --> 00:08:36,760
that allows you to understand everything. It allows you to conquer everything, to be successful

49
00:08:36,760 --> 00:08:45,800
everywhere. We have this idea that to be successful, you have to have wide knowledge. You have to

50
00:08:45,800 --> 00:08:52,520
know how to react differently in different situations. So we think there's a different answer to

51
00:08:52,520 --> 00:09:00,760
each problem. And each problem has its unique solutions. But in fact, there are some very core

52
00:09:01,320 --> 00:09:06,360
principles that you're going to adopt. I mean, take mindfulness, for example. If you have

53
00:09:06,360 --> 00:09:14,680
mindfulness, it gives you all the tools you need to solve for the any problem, any challenge that

54
00:09:14,680 --> 00:09:26,520
may face you in life. On the other hand, someone who knows a lot, but isn't able to put it

55
00:09:26,520 --> 00:09:32,280
into practice. It's never going to help them. So if you have a lot of theory, you know, the theory,

56
00:09:32,280 --> 00:09:37,400
take the theory of public speaking, for example. If you know, you read these book, this book by Dale

57
00:09:37,400 --> 00:09:45,400
Carnegie, and then you get up in front of people, and you can't speak because you have a practice.

58
00:09:45,720 --> 00:09:50,760
You haven't learned. Or you learn more to the point, you learn how to meditate. You know,

59
00:09:50,760 --> 00:09:55,320
all these books on how to meditate. We can read the Risudhi manga together, and you know how

60
00:09:55,320 --> 00:09:59,880
to meditate. But if you've known like all these summative practices that lead to magical powers,

61
00:09:59,880 --> 00:10:06,280
I can teach them to you, but either useless to me. All that knowledge is useless, except to teach you

62
00:10:06,280 --> 00:10:12,680
and make you think I'm smart, because I don't practice them. But someone who practices them and

63
00:10:12,680 --> 00:10:17,720
actually remembers past lives and meets people's minds. They're the ones who the Buddha said it's

64
00:10:17,720 --> 00:10:25,640
like a difference between the cow herd and the owner of the cows. The cow herd can have a thousand

65
00:10:25,640 --> 00:10:34,280
cows under his or her care, but they never get to taste the milk. Whereas the person who owns

66
00:10:34,280 --> 00:10:44,280
even one cow, they get to taste the milk. So a simple quote, but important one, very important,

67
00:10:44,280 --> 00:10:48,760
it's an important principle in Buddhism. I think many of us are already familiar with this,

68
00:10:49,400 --> 00:10:55,240
at least intellectually, but it's an important point that we have to understand what is

69
00:10:55,240 --> 00:11:01,000
a mission to bow. So if we want to get clearly closer to the understanding of what is Buddhism,

70
00:11:01,000 --> 00:11:05,800
yeah, this is a part of it, understanding that from a Buddhist point of view,

71
00:11:06,520 --> 00:11:11,720
knowledge and practice are two very different things and practice is always to be preferred.

72
00:11:13,000 --> 00:11:16,280
Your practice according to the truth, it doesn't matter how much of the truth that you know.

73
00:11:23,960 --> 00:11:26,680
If you could go back 20, go back 10 years,

74
00:11:26,680 --> 00:11:33,800
when you start of your Buddhist journey, 15 years, what advice would you give to your 15 year

75
00:11:33,800 --> 00:11:52,760
younger self? I don't know, it's a tough one. I don't think I can say it's a little bit,

76
00:11:52,760 --> 00:12:04,600
I guess I would say it's hard to say, because you know, 15 years ago,

77
00:12:05,560 --> 00:12:11,400
I had to learn all those lessons. This is a good example of this quote, because I don't think

78
00:12:11,400 --> 00:12:18,920
anything I told myself would have really helped. If I went back 15 years, man, that was

79
00:12:18,920 --> 00:12:27,560
at least sometimes. This being here, whatever, this thread was suddenly to loop back in

80
00:12:29,080 --> 00:12:33,480
by helping this anything. There was nothing that I didn't know intellectually 15 years ago.

81
00:12:34,040 --> 00:12:37,160
I finished my first meditation course and everything was really clear.

82
00:12:37,880 --> 00:12:45,160
And it became clear over the course of a few months as I just delved right into Buddhism.

83
00:12:45,160 --> 00:12:51,560
The intellectual stuff was easy. It was learning the lessons that I already knew,

84
00:12:51,560 --> 00:13:00,600
and much of that came from seeing other people lean by example, watching people do the right

85
00:13:00,600 --> 00:13:06,200
thing, watching people do the wrong thing, learning a lot of, I mean, I mean, terrible mistakes.

86
00:13:06,200 --> 00:13:10,680
I can tell myself, by the way, when you go here and don't do this, because that's going to

87
00:13:10,680 --> 00:13:15,880
me for this. So there's lots of that kind of stuff that I can't really talk about, because

88
00:13:15,880 --> 00:13:20,600
I shouldn't really talk about because it involves other people. It's just way too specific.

89
00:13:21,400 --> 00:13:25,800
But general advice, I don't know that I have any.

90
00:13:25,800 --> 00:13:37,480
I suppose if I could go back, yeah, I don't know. I don't want to think about that. Past is past.

91
00:13:40,360 --> 00:13:45,240
You're making my brain hurt. Any questions? Any other questions?

92
00:13:45,240 --> 00:13:55,640
I'll have to think about that one. Maybe I will think of something.

93
00:13:56,680 --> 00:14:00,760
It's not something I ever thought, because it's not really a useful question.

94
00:14:01,800 --> 00:14:05,800
I mean, it's from your point of view, actually, it's interesting, because I don't know what

95
00:14:05,800 --> 00:14:08,600
is, what are things that a new Buddhist should know?

96
00:14:09,960 --> 00:14:13,320
And so it's an interesting question in that sense, and I don't really think I have an answer to

97
00:14:13,320 --> 00:14:19,160
the general idea of what advice I'd give to a young me or to a person like me.

98
00:14:22,200 --> 00:14:28,520
I don't know that I have any advice, because it's a learning process. It's not so much about

99
00:14:28,520 --> 00:14:29,000
advice.

100
00:14:29,000 --> 00:14:41,720
Are you meditating right now, like noting things as we speak? How can we do that? Yeah.

101
00:14:43,960 --> 00:14:47,240
And I'm not perfect, but yeah, indeed, even when talking,

102
00:14:47,240 --> 00:14:56,760
I can be mindful of each word, mindful of emotions, mindful of experiences.

103
00:15:05,000 --> 00:15:09,320
How can we reflect on our practice to make sure we're on the right track without having a teacher?

104
00:15:09,320 --> 00:15:17,000
My teacher always said, you ask yourself,

105
00:15:18,440 --> 00:15:24,440
read anger and delusion, do I have less of these than before?

106
00:15:26,360 --> 00:15:30,280
And if the answer is yes, then you're doing something right, you're doing well.

107
00:15:30,280 --> 00:15:37,800
You know that you're on the right path, your meditation is helping you.

108
00:15:41,720 --> 00:15:46,920
I mean, it's difficult that we were talking about earlier, the ads, right?

109
00:15:48,040 --> 00:15:53,720
You use it so much that the handle starts to wear away, but you don't know when it happened,

110
00:15:53,720 --> 00:15:58,520
because it's happening way too slowly. Meditation is a lot like that. It's so gradual

111
00:15:58,520 --> 00:16:05,880
that you could never, and because our minds are not one thing, their individual states are rising

112
00:16:05,880 --> 00:16:13,080
and multiple conflicting states that you can never look at one instance, like how am I doing

113
00:16:13,080 --> 00:16:23,320
right now and say this is indicative or this is exemplar exemplary, you know, simplifies

114
00:16:23,320 --> 00:16:35,880
the, or this is, don't remember the word, this is the way I am,

115
00:16:35,880 --> 00:16:51,800
because we change a lot, we can be perfectly calm and then suddenly erupt.

116
00:16:57,000 --> 00:17:02,200
So a lot of people ask this question, and I think it's not that useful of a question,

117
00:17:02,200 --> 00:17:05,560
it's not one we should be asking nearly as much as people want to ask you.

118
00:17:07,320 --> 00:17:13,080
You have to decide for yourself whether you're on the right path, and that has a little bit,

119
00:17:13,080 --> 00:17:19,800
that has less to do with results than you'd think, because as I said, results are difficult.

120
00:17:19,800 --> 00:17:24,040
First of all, without a mirror, you can't see yourself.

121
00:17:24,040 --> 00:17:40,040
So without wisdom, it's hard to see your own mind, and the results are so subtle and it's hard

122
00:17:40,040 --> 00:17:44,920
to tell what is the cause of what? So then the question is whether it's just, it sounds like it's

123
00:17:44,920 --> 00:17:49,640
just about faith, but it's not, there's something else that you can use as an indication and that's

124
00:17:49,640 --> 00:17:57,000
the momentary result. You can see in one moment what the meditation is doing to change that

125
00:17:57,000 --> 00:18:04,840
experience. You can see the relief, the peace, the freedom, the clarity in that moment, and that

126
00:18:04,840 --> 00:18:09,720
is what we should focus on. That should be enough. That is where our mind should be centered

127
00:18:09,720 --> 00:18:21,240
on that clarity right now, because when the question of results comes up, it's very much to do

128
00:18:21,240 --> 00:18:35,960
with doubt, maybe greed as well, worry, fear, just desire to know how you're doing, ego,

129
00:18:35,960 --> 00:18:45,960
it's not really wholesome. So it's important for the things we do to have an effect to be

130
00:18:45,960 --> 00:18:49,960
beneficial, and if meditation were useless and didn't change us, that would be

131
00:18:51,480 --> 00:18:57,160
that's something that we have to, that's an important aspect of it, obviously. But asking

132
00:18:57,160 --> 00:19:01,960
yourselves this question, have I, is this helping me? It can be very problematic.

133
00:19:01,960 --> 00:19:08,760
And it's a shock to hear that, I think, because you think about how, you know, I'm not just going

134
00:19:08,760 --> 00:19:12,760
to follow this out of faith, but you're not going to follow it in faith. You're going to follow

135
00:19:12,760 --> 00:19:18,920
it based on the, what you shouldn't be content with, and that's the momentary result.

136
00:19:20,280 --> 00:19:25,400
And if you wanted to intellectually extrapolate, you can say there's no question that these

137
00:19:25,400 --> 00:19:30,920
moments of clarity are going to help me in my life. That should be enough. That's where you should

138
00:19:30,920 --> 00:19:37,080
find contentment, and not worry about, does it actually pan out? Because that should be enough.

139
00:19:37,080 --> 00:19:43,080
That should be intellectually even enough for you to reasonably say this is helpful for me,

140
00:19:43,080 --> 00:19:48,200
without having to worry about if it's actually helping you. Because in that moment,

141
00:19:48,200 --> 00:19:54,600
it's, there's a change. That mind is better than the alternative mind if I hadn't been mindful.

142
00:19:54,600 --> 00:20:04,680
The categorically better, objectively better. No question better.

143
00:20:06,280 --> 00:20:12,200
And so we, we understand that our habits are formed by the mind states that we give rise to.

144
00:20:13,240 --> 00:20:19,960
So we don't worry about our habits. Let them form as they will. Start. Do they do the work,

145
00:20:19,960 --> 00:20:32,600
and the habits will form accordingly? When you please talk about see pain without

146
00:20:32,600 --> 00:20:39,880
get suffering from pain. Well, pain, right pain and suffering, I mean pain is the type of suffering,

147
00:20:39,880 --> 00:20:45,320
it's physical suffering, but physical suffering and mental suffering are two different things.

148
00:20:45,320 --> 00:20:53,880
You don't have to suffer mentally from physical pain. So that's how we understand or

149
00:20:53,880 --> 00:21:03,480
that the understanding we drop on in Buddhism. Our, our practice of insight meditation is to see

150
00:21:03,480 --> 00:21:10,840
the pain as just pain. Because there's, there's nothing unpleasant necessarily about the pain.

151
00:21:10,840 --> 00:21:17,800
We can experience pain and just be happy in peace. If we learn how to just see it as pain and not

152
00:21:17,800 --> 00:21:20,200
as bad or there's a problem.

153
00:21:25,160 --> 00:21:29,960
So the way we do this is to remind ourselves this is only pain. We say to ourselves pain,

154
00:21:31,320 --> 00:21:38,360
pain, pain. And if you do it right, you're clear in the mind, you can be free from any suffering

155
00:21:38,360 --> 00:21:45,480
in regards to the pain. So that's momentary. The habit that forms as a result, you become less

156
00:21:45,480 --> 00:21:54,280
inclined to react to the pain. So more inclined to just experience pain as pain.

157
00:21:54,280 --> 00:22:08,920
I think indicative was the word I was looking for. Robin, are you, I was, I was going to get you

158
00:22:08,920 --> 00:22:10,840
to ask these questions, right?

159
00:22:10,840 --> 00:22:17,080
Sure. What can we do when we feel our teacher is criticizing more than he or she is teaching?

160
00:22:17,080 --> 00:22:26,920
I mean, there's nothing wrong with criticism. It's good when it comes from a teacher.

161
00:22:27,720 --> 00:22:42,920
I wouldn't worry so much about whether a person is criticizing you as to whether they're helping you.

162
00:22:42,920 --> 00:22:50,120
So criticism can be quite useful if you, especially if you don't like it.

163
00:22:56,920 --> 00:22:59,560
So you have to ask, is the criticism helping you?

164
00:23:01,400 --> 00:23:05,480
Yeah, it's not, it doesn't help you if you don't take it. If you get angry about it, it certainly

165
00:23:05,480 --> 00:23:11,160
doesn't. Let's suppose you know something about yourself. I know I've got this problem and your

166
00:23:11,160 --> 00:23:16,280
teacher keeps criticizing you about it anyway. And so it doesn't help you in any way.

167
00:23:17,080 --> 00:23:20,600
And you don't get upset when they say it, you just think, well, why are they telling me something

168
00:23:20,600 --> 00:23:26,440
I already know, what benefit is it? Then you can say, well, that's not really beneficial.

169
00:23:26,440 --> 00:23:31,560
So then you see that your teacher has a flaw. And that's okay. I mean, teachers are not perfect,

170
00:23:31,560 --> 00:23:44,920
but you have to then let's mark against that teacher. You wouldn't want to teach your

171
00:23:44,920 --> 00:23:50,040
never criticized you. That's dangerous. Teacher who always flatters you.

172
00:23:50,040 --> 00:24:00,680
Is it typical for Buddhist teachers to be very critical?

173
00:24:02,680 --> 00:24:07,400
I don't know. It's hard to be very, it's hard to be well critical. It's hard not to,

174
00:24:07,400 --> 00:24:10,360
because it's easy to criticize people that we can all, everyone's a critic,

175
00:24:11,080 --> 00:24:15,400
but it's hard to do it in such a way that the person can take it and actually benefits from it.

176
00:24:15,400 --> 00:24:22,120
My teacher is not very critical, but when, but he is incredibly critical at the, at the

177
00:24:22,120 --> 00:24:26,520
bright point. I mean, everyone thinks, oh, he's so nice and so kind, but then I'll get hit by

178
00:24:26,520 --> 00:24:35,480
something and then he, he gets you. I mean, I think I'm hard to think, but he's very good at it.

179
00:24:35,480 --> 00:24:37,160
I mean, the good teacher is good at that.

180
00:24:37,160 --> 00:24:49,720
I've read some stories about the old Zen Masters and some of them seemed very,

181
00:24:51,400 --> 00:24:59,320
very critical and harsh, maybe, for somebody that didn't understand what was going on there.

182
00:25:01,480 --> 00:25:05,400
You know, as I said, I don't think a teacher should be terribly involved with their students,

183
00:25:05,400 --> 00:25:08,840
and I think Zen has a different idea. They are very involved with their students,

184
00:25:10,280 --> 00:25:16,280
but I don't want to give the idea that a teacher should be picking,

185
00:25:16,280 --> 00:25:22,280
picking the fault to their students.

186
00:25:30,200 --> 00:25:34,440
Anyway, as I said, I think you should ask yourself whether it's the, your teacher's benefiting

187
00:25:34,440 --> 00:25:38,440
you because they're different teaching styles. So is it helping you as a person? Are you

188
00:25:39,480 --> 00:25:45,320
seeing things about yourself that you didn't see before? Or is it just wasting your time? And yeah.

189
00:25:51,080 --> 00:25:56,600
I think this is a follow-up question to the question on pain. I feel suffering from time to time,

190
00:25:56,600 --> 00:26:05,720
focus on pain suffering just as a raw sensation. It's just a sensation, it's just pain.

191
00:26:10,920 --> 00:26:17,000
Depending on the sitting posture, with the cushion and without, I have observed a difference in

192
00:26:17,000 --> 00:26:27,720
the pain I feel during meditation. How does this affect the practice?

193
00:26:29,560 --> 00:26:34,920
It can, I mean, ultimately not at all, but for a beginner meditator, it can

194
00:26:36,120 --> 00:26:40,920
greatly affect the practice, either from good or for bad, being too comfortable for some people

195
00:26:40,920 --> 00:26:45,960
that's a problem, being too uncomfortable for some people that's a problem for beginners.

196
00:26:45,960 --> 00:26:53,640
Now, eventually you can get it either way. You become more comfortable with discomfort or comfort.

197
00:26:54,280 --> 00:26:59,480
And so you can do the same meditation lying down as you could standing on one foot or

198
00:27:03,400 --> 00:27:12,920
in doing any, in any posture. So in the beginning, the difference is just that some people

199
00:27:12,920 --> 00:27:21,080
can't take too much pain and they find it just leads to distraction and anger, you know, upset.

200
00:27:22,280 --> 00:27:26,840
And some people find that when they're too comfortable, they fall asleep, or they indulge,

201
00:27:26,840 --> 00:27:30,600
and they start daydreaming or so on. They can't stay alert.

202
00:27:30,600 --> 00:27:42,600
And another part of that is having to sit on a cushion.

203
00:27:43,400 --> 00:27:50,520
One of my friends in Bangkok, a monk, who teaches at what my entire day is, yeah.

204
00:27:52,760 --> 00:27:56,600
He was, we were talking just chatting about Western meditators and he said,

205
00:27:56,600 --> 00:28:02,040
people in the West don't know how to meditate. Meditators in the West, they don't have a clue.

206
00:28:02,920 --> 00:28:07,800
They go there and I see them on these cushions like this high. And everyone's just concerned

207
00:28:07,800 --> 00:28:11,240
about getting the right cushion and they've, it's become a real market, getting these

208
00:28:11,880 --> 00:28:19,000
croissant shaped cushions and donut shaped cushions and they've got names for them

209
00:28:19,000 --> 00:28:22,040
and then they've got the benches and the benches that fold.

210
00:28:22,040 --> 00:28:28,840
And I, you know, I've got, I know what he's talking about because we didn't have any of that.

211
00:28:28,840 --> 00:28:37,000
And it is kind of a whole other approach to things because when I started practicing,

212
00:28:37,000 --> 00:28:41,720
they gave us this thin piece of cloth and said, well, if it hurts at first, you can put some

213
00:28:41,720 --> 00:28:46,360
blankets under your knees, which I did.

214
00:28:46,360 --> 00:28:53,000
And you could sit higher, you know, sitting a little bit higher with your butt, a little bit

215
00:28:53,000 --> 00:28:56,520
higher is helpful in the beginning. So we did that as well. You know, we were trying to find

216
00:28:56,520 --> 00:29:01,320
all sorts of ways and make it comfortable in the beginning. But we got into it. You know,

217
00:29:01,320 --> 00:29:10,040
and we pushed for them and experienced a lot of pain, but didn't let it overpower us and just

218
00:29:10,040 --> 00:29:15,080
experienced it because it wasn't harmful pain. It was just the growing pains, the pains of

219
00:29:15,080 --> 00:29:20,920
learning to let your legs spread stretch and eventually your knees go down and they touch the floor

220
00:29:20,920 --> 00:29:27,000
and then you're sitting fine. I can sit on a wooden floor fine. You know, we do sitting sometimes

221
00:29:27,000 --> 00:29:32,360
it's just a piece of cloth on the wooden floor. They're monks who would sit sleep with cement pillows

222
00:29:32,360 --> 00:29:40,680
and kind of thing. I started out making myself really comfortable because I didn't want to

223
00:29:40,680 --> 00:29:47,720
get discouraged and not form the habit of meditation. But then once it was very regular with my

224
00:29:47,720 --> 00:29:51,720
habit, I've kind of pared down. I can sit on the bare floor now and meditate for an hour.

225
00:29:52,920 --> 00:29:56,360
I think it's a little bit, it's good to have a little bit of height. I don't usually, I

226
00:29:56,360 --> 00:30:07,400
usually just sit on the floor. That's it. But it has a lot more use in cement the meditation where

227
00:30:07,400 --> 00:30:13,720
the idea is to attain a very powerful state of mind and fixed. So you can be unmoving and that

228
00:30:13,720 --> 00:30:19,240
takes a lot of physical training.

229
00:30:26,440 --> 00:30:31,640
So what I would use to say to people is, it's fine. You can start high.

230
00:30:31,640 --> 00:30:36,040
Start meditating high. But the idea is to get lower as you go along, not get higher.

231
00:30:36,040 --> 00:30:40,520
And that's how you can gauge it because some meditators, they'll start on the cushion and then

232
00:30:41,640 --> 00:30:46,120
the reason for having the cushion is to avoid the pain. So when the pain comes, they go to a higher

233
00:30:46,120 --> 00:30:51,160
cushion. And then when they're actually cultivating a version and they get more and more reversed

234
00:30:51,160 --> 00:30:55,640
to less and less pain until they might as well just be sitting on a chair and then even

235
00:30:55,640 --> 00:31:00,760
the chair is not enough so they have to lie down and then they just go to sleep and forget about

236
00:31:00,760 --> 00:31:07,480
meditating.

237
00:31:07,480 --> 00:31:35,480
All right then, let's quit one more ahead. 9.30. So another half an hour or so.

238
00:31:35,480 --> 00:31:42,200
That's enough, right? Thank you all for tuning in. See you all tomorrow.

239
00:31:42,840 --> 00:31:46,600
Thank you, Bhante. We'll see you tomorrow.

240
00:31:46,600 --> 00:32:16,440
Okay, bye bye.

