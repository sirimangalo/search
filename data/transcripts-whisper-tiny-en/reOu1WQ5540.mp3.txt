Why is there so much emphasis on following which tradition or which lineage of praxing Buddhism?
If the dharma is taught by Gautama Buddha, should we only emphasize and refer as Gautama
Buddha teaching instead of lineage in tradition?
May I start with a short command on it?
This is because we have defiled minds, just that we need to be right, that we have the wanting
to be right, the wanting to know better than others.
This is how a lineage and tradition evolves, that some people meet and say, let's do it that
way, and everything who does otherwise is out of that lineage, is out of that tradition.
And then they start their own tradition and their own lineage according to what they think
is right, and the thing is that they insist on both of course, both groups or all the
groups that come up insist that they are right.
So since the Buddha, Gautama Buddha is not living anymore, there is no being that can say,
this is the truth, this is what has been said, this is what the Buddha said.
So but now are many, many people with defiled, more or less defiled minds there, telling
this has been said, that has been said, and want to be right.
So that's why there are lineages and lineages and traditions.
If we could agree on what were the Buddha's teaching or what were the practice of meditation
that the Buddha really told to practice, then there would probably be only one tradition
and one lineage.
I'd like to add a couple of things, and then as it goes a little bit beyond that, because
you have to understand, like in reference, for example, the Tibetan Buddhism, many aspects
of Tibetan Buddhism, I don't know about all of it, and I always don't like to talk about
even a country's Buddhism, but many of the types of Buddhism that you find in Tibet don't
even refer so much to the teachings of the Buddha, or about following the teaching the
45 years of the Buddha's teaching, they will instead strive to become Buddhas by themselves,
and still call this Buddhism.
So it even goes beyond simply disagreement over what the Buddha said, it's disagreement over
in what way we relate to the Buddha.
And so you might say, well, that's just because of defilements, but then it really goes
by.
So at the very least, there's room on a subjective level to suggest that there are different
ways of approaching the Buddha.
So you can say, you talk about the Gautama Buddha's teaching, well, some people don't
see that as the most important.
They take Gautama's Buddha's example as the core of Buddhism, and not even, no, and actually
many of them will not even refer to Gautama Buddha.
In fact, there are schools of Buddhism that don't refer so much, anyway, to him, they refer
to Avalokitesh for a Buddha.
Amitafas in China, they call him, who is in the Western Pure Land, who, if you chant his
name with the pure mind, he'll be born in his realm and become a Buddha there.
So, I mean, someone from our tradition might just say, well, that's just because of the
defilements of those people and the ignorance that they say that.
But subjectively, there is room for different interpretations.
And so on one level, someone can say, well, yes, the Buddha taught all this, but rather
than follow his teachings, I'm going to become a Buddha.
And they might, therefore, start a tradition, encouraging other people to do that and
saying, yeah, that's all the kombudas together, which seems to be the case in certain
instances, where this is even more clearly the case is within a harmonious, sort of
super tradition.
So, within a group of people who follow the same text, who follow the same teachings
and who can agree on what are the Buddha's teachings, I would say, I would submit the argument
that it still might be possible to have different traditions and lineages, like you might
consider Sariputas lineage to be a specific type of practice that was suitable for people
who had high intelligence and Mogulana's lineage for those people who were more inclined
towards magical powers.
You know, there's the story of the Buddha upon Gijakuta, Vulture's peak, and looking down
and watching the monks go and arms round.
And he said, you see those monks following Sariputta, that's because they're all really
engaged in the development of wisdom.
And you see those monks following Mogulana, that's because the reason they follow him
is because they are very keen on magical powers.
And you see those ones following Ananda, that's because they're all, those monks are all
Bajusutta, they're all ones who are keen on memorizing the texts.
And you see those ones following Devadatta, that's because they're all corrupt and evil.
And so he said something like birds of a feather flock together.
And so, while you could consider that, for example, in our tradition, we call, even let's
say within the Mahasi Sayada tradition, there's group of people who follow the same type
of practice.
Because you could make the argument that we don't find anything wrong with, say, Adjan
Chaz, tradition, they have a perfectly adequate and successful interpretation of the Buddha
's teaching or tradition of practice based on the Buddha's teaching.
We could say that.
I don't really know.
Could be the case, maybe not.
We certainly don't attack them or say they're wrong.
And we say they're still the same, they take the same body of texts.
And they seem to be practicing in a way that is in line with those texts.
But even within the Mahasi Sayada group, every monastery that you go to, not even every country,
every monastery that you go to, even within a monastery, within the monastery that I say
that, every teacher in that monastery had a different way of teaching.
And you could cynically say that it's because of the defilements of all of them, but
one, all but my teacher, everybody else is just because they have defilements.
But I don't think it necessarily has to be the case because the other argument was that
it has to do with the needs of the students or the nature of the students.
But it can also have to do with the, I would look at the teacher.
So for example, the teacher of Tuchabotilla, a really good example.
How did he teach Tuchabotilla?
This monk who thought he was the best of the best because he could memorize the whole
to Pitika?
He said, go run to the pond.
And so he had this guy with all his robes on, go and run to the middle of pond and floating
there in the pond and he'll come back out.
And then he sent them back down into the pond again.
Why?
Because he was seven years old.
He was seven year old.
I think he was an Arahant.
This seven year old Arahant, but this is how they think they, they run to the pond.
And then he's taught him in the middle of the pond about, but this, uh, there's Antil.
And the lizard goes into the Antil.
What do you do to get it out?
Plug up all the holes but one and then watch that hole and grab it.
And he said in the same way, this is how you watch your mind.
You close all the other senses and just watch the mind wherever the mind goes.
The means don't worry about what you see, what you hear, what you smell.
Just look at where the mind is.
If the mind goes to the eye, then you just see your hearing.
So you close them all and you're only concerned with where the mind is going, not with
anything else.
So, so totally different way of teaching from, from, you might say another Arahant.
The technique would be quite different.
So you have, you know, some, some people in Thailand who, we have this practice of mindful
frustration and my teacher teaches it one way, but I went to another center and they teach
in another way.
They have this really kind of crazy.
I mean, I don't, I don't go for it to me, it seems crazy, but they have another way of
doing mindful frustration.
There's nothing wrong with it.
It's kind of crazy, but it's still, it's a lot, I did it and I was doing it and you can
be mindful just the same.
We do these different parts of the walking step.
And some people say you do lifting, placing, lifting straight up.
Some people say you lift forward and then place it straight down.
So there's a million different ways of, of, of practicing the teaching and probably as
Paul and you said, many of them just come from defilements, but there is certainly room
for a subjective reason, which is that simply because we're, we're all, we're all coming
from different places that all, even all, I'm not, not to speak of us students, but to
speak of, say, the hypothetical hour hunts, those people who are, who are fully enlightened,
they're still quite different.
Even Buddhas are different in certain respects, most respects are the same, but some Buddhas
don't teach the party mocha, for example.
So would you say, so, but you, you can't say that, that it's because of the defilements of
that Buddha that he didn't teach the party mocha is because of his situation and because
of the students and that came to him and so on.
What is the same will be certain concepts.
So the, what will be based on defilements is whether they're actually teaching based on
the four noble truths, but to just sum upada, whether they're, they're clear in terms
of causing the fact.
So if someone starts to say that, know the Buddha taught that our hunts have angry
anger and delusion, then you say defilements, and you can check that one out, this is wrong.
But if someone says, well, you should prostrate like this, or you should prostrate like
that, or you should do Anapana Santi, or you should, you know, you should watch at
the nose, or you should watch at the stomach, or someone.
There's room for all of that, I think, in the differences of, we saw what it was, or
Wasana, which means the things that we bring into this life.
Maybe even the, the religions of, of a country before Buddhism came to that country still
have a, an influence on, on how it developed, like in Tibet there, I heard was kind
of ghost religion with shamanism called, and that, that, that there still has an influence
and in China, there was something different going on, so I think this can be taken in, in
consideration as well.
