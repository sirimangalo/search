1
00:00:00,000 --> 00:00:07,000
A boy asked, how do I know that I'm progressing in the meditation process?

2
00:00:15,000 --> 00:00:22,000
It's, I think, very easy, or you will feel the difference.

3
00:00:22,000 --> 00:00:29,000
You will feel changed more or less, and you will feel ongoing changes.

4
00:00:29,000 --> 00:00:41,000
Sometimes it might feel that you are regressive, that you don't advance so much anymore,

5
00:00:41,000 --> 00:00:44,000
and you don't make any progress.

6
00:00:44,000 --> 00:00:50,000
But in general, you will know that all this defilement is gone,

7
00:00:50,000 --> 00:00:58,000
and that defilement is weakened, and sometimes it goes in larger steps,

8
00:00:58,000 --> 00:01:01,000
and sometimes in smaller steps.

9
00:01:01,000 --> 00:01:17,000
But when you had an insight that was really a deep insight,

10
00:01:17,000 --> 00:01:22,000
then you will certainly know that change,

11
00:01:22,000 --> 00:01:30,000
and you will certainly know that you had made progress.

12
00:01:30,000 --> 00:01:39,000
One thing that I always try to claim about the practices,

13
00:01:39,000 --> 00:01:43,000
that everything you sit down, you should learn something about yourself,

14
00:01:43,000 --> 00:01:46,000
or in general you might see about reality.

15
00:01:46,000 --> 00:01:51,000
So if you're practicing and practicing and never learning anything no matter how small,

16
00:01:51,000 --> 00:01:55,000
then you can say that, well, I would say that maybe you're doing something wrong,

17
00:01:55,000 --> 00:02:02,000
because it should be that every time you sit down to meditate,

18
00:02:02,000 --> 00:02:04,000
you should learn something.

19
00:02:04,000 --> 00:02:09,000
The things that we try to learn just to give an idea of what we mean by progress,

20
00:02:09,000 --> 00:02:18,000
in brief the things that we intend to understand,

21
00:02:18,000 --> 00:02:21,000
or the knowledge that we intend to gain from the practice,

22
00:02:21,000 --> 00:02:23,000
there are four things.

23
00:02:23,000 --> 00:02:32,000
There is the three things I think, knowledge of body and mind.

24
00:02:32,000 --> 00:02:38,000
So the first thing that you should gain from the practice is an understanding of the reality.

25
00:02:38,000 --> 00:02:44,000
The reality of your experience inside and in the world around you,

26
00:02:44,000 --> 00:02:51,000
so realizing that reality is made up of seeing, hearing, smelling, tasting, feeling and thinking,

27
00:02:51,000 --> 00:02:56,000
and it's made up of physical and mental experiences, moments of experience.

28
00:02:56,000 --> 00:03:04,000
The second thing is that you come to see impermanence, suffering and non-self.

29
00:03:04,000 --> 00:03:09,000
So you come to weaken your grasp on these things.

30
00:03:09,000 --> 00:03:12,000
This is what leads the defilements to get weaker.

31
00:03:12,000 --> 00:03:16,000
You'll see that the things that you cling to are not worth clinging to,

32
00:03:16,000 --> 00:03:20,000
they're impermanent, they're unsatisfying, and they're uncontrollable.

33
00:03:20,000 --> 00:03:25,000
And finally, you'll come to see Nimana,

34
00:03:25,000 --> 00:03:27,000
which really you can break it up.

35
00:03:27,000 --> 00:03:31,000
So by saying there's four things, it's because there's the path,

36
00:03:31,000 --> 00:03:38,000
and then there's the results of the path is the weakening of the defilements,

37
00:03:38,000 --> 00:03:45,000
or the eradication of certain defilements, in the case of reaching the path

38
00:03:45,000 --> 00:03:49,000
of Soto Pandasakita Kamiana Kamiran,

39
00:03:49,000 --> 00:03:52,000
and the fourth is the fruition.

40
00:03:52,000 --> 00:03:55,000
But really, it's the same thing, the fruition in the path

41
00:03:55,000 --> 00:04:00,000
that take the same object and that is Nimana or cessation of suffering.

42
00:04:00,000 --> 00:04:06,000
So in the beginning of the practice,

43
00:04:06,000 --> 00:04:11,000
until you've realized, once you've realized the third and the fourth knowledge,

44
00:04:11,000 --> 00:04:14,000
this kind of question doesn't really have the same weight,

45
00:04:14,000 --> 00:04:18,000
because you already know it is the goal,

46
00:04:18,000 --> 00:04:27,000
so your only work is to attain it again and weaken the defilements more and more.

47
00:04:27,000 --> 00:04:32,000
But in the meantime, you should be looking to understand your mind

48
00:04:32,000 --> 00:04:36,000
or your experience in terms of body and mind,

49
00:04:36,000 --> 00:04:37,000
and to see things as they are.

50
00:04:37,000 --> 00:04:42,000
The more you can see, that the things that you cling to are not worth clinging to

51
00:04:42,000 --> 00:04:46,000
and how you're clinging is leading to suffering and so on.

52
00:04:46,000 --> 00:04:59,000
That's the sign of more of progress in the practice.

