1
00:00:00,000 --> 00:00:20,600
Good evening everyone, welcome to our daily Dhamma talk.

2
00:00:20,600 --> 00:00:28,640
I was thinking today about confidence.

3
00:00:28,640 --> 00:00:42,400
How confidence can mean so many different things or the qualities associated with confidence

4
00:00:42,400 --> 00:01:01,760
can be of quite a varied nature, some wholesome, some unwholesome, some perhaps neither.

5
00:01:01,760 --> 00:01:12,360
But really you could say the same about many Dhammas, many qualities of mind.

6
00:01:12,360 --> 00:01:24,720
Or not qualities of mind but many terms, it's the problem with language and it's the

7
00:01:24,720 --> 00:01:38,160
imprecision of words, because the word confidence for example refers to the number associated

8
00:01:38,160 --> 00:01:56,960
in mind states, or qualities of mind that arise together or arise in sequence.

9
00:01:56,960 --> 00:02:05,160
And so it's important with all of these concepts, take the five mental faculties for

10
00:02:05,160 --> 00:02:12,360
example, confidence, effort, mindfulness, concentration, wisdom, okay, so you know what

11
00:02:12,360 --> 00:02:24,560
these five are, but taking confidence as an example, all five of them can mean quite

12
00:02:24,560 --> 00:02:30,600
different things, depending how you understand the word and can actually become problematic

13
00:02:30,600 --> 00:02:40,000
as a result.

14
00:02:40,000 --> 00:02:47,160
The greatest problem for these comes especially when we try to develop one of them.

15
00:02:47,160 --> 00:02:59,520
And so this idea of balancing the faculties is quite important for this reason specifically.

16
00:02:59,520 --> 00:03:05,520
Because in trying to cultivate one of them, we misunderstand the nature of it and it's

17
00:03:05,520 --> 00:03:11,000
very easy to get caught up in the wrong kind of confidence.

18
00:03:11,000 --> 00:03:18,960
All five of these are the four of them anyway besides mindfulness.

19
00:03:18,960 --> 00:03:31,160
And they require a very specific sort of development, and they really require mindfulness,

20
00:03:31,160 --> 00:03:36,160
they require it to be developed alongside the other faculties, but most especially mindfulness,

21
00:03:36,160 --> 00:03:42,040
or what we call mindfulness, and again, misunderstanding of the word mindfulness can

22
00:03:42,040 --> 00:03:49,000
be problematic.

23
00:03:49,000 --> 00:03:53,480
So it's start with confidence.

24
00:03:53,480 --> 00:03:58,240
Confidence is so powerful.

25
00:03:58,240 --> 00:04:02,920
I think you could argue that's one of the big reasons why America has a very, very strange

26
00:04:02,920 --> 00:04:05,840
sort of president right now.

27
00:04:05,840 --> 00:04:11,200
You could argue with political figures in general why they become electives, quite bizarre

28
00:04:11,200 --> 00:04:12,200
something.

29
00:04:12,200 --> 00:04:18,720
There are other reasons, of course, there's a lot of technical reasons and rigging

30
00:04:18,720 --> 00:04:27,480
elections and that kind of thing, but there's this specific type of confidence that

31
00:04:27,480 --> 00:04:42,480
knows no shame, it's not even quite confidence, but it's a sense of self-assurance.

32
00:04:42,480 --> 00:04:48,800
And it's common, I think, to psychopaths or social paths, and enlightened beings, enlightened

33
00:04:48,800 --> 00:04:51,480
beings are completely self-assured.

34
00:04:51,480 --> 00:05:00,040
You couldn't criticize an enlightened being and make them feel the slightest bit embarrassed.

35
00:05:00,040 --> 00:05:07,280
Guilty disturbed upset, and you can bring up things from their past, and enlightened

36
00:05:07,280 --> 00:05:15,960
being would be unperturbed by that, but there's, of course, a great difference.

37
00:05:15,960 --> 00:05:22,960
And enlightened being would acknowledge, and completely accept, like, sorry, put up, for

38
00:05:22,960 --> 00:05:23,960
example.

39
00:05:23,960 --> 00:05:32,800
There's a story of, sorry, put the, he was, he's slighted, unknowingly, he's slighted

40
00:05:32,800 --> 00:05:37,880
some monk and some monk on very angry, sorry, put the developed a grudge against him.

41
00:05:37,880 --> 00:05:43,840
And so when they, sorry, put those walking, and the corner of his robe touched this monk

42
00:05:43,840 --> 00:05:47,840
as he walked past him, and just brushed past him, and the corner of his robe just touched

43
00:05:47,840 --> 00:05:48,840
him.

44
00:05:48,840 --> 00:05:55,320
And this robe went and told the Buddha, or went around telling people that, sorry, put

45
00:05:55,320 --> 00:06:03,600
to hit him.

46
00:06:03,600 --> 00:06:09,120
And so they came to the Buddha, and they brought, sorry, the Buddha called, sorry, put

47
00:06:09,120 --> 00:06:13,440
to him, and they came before the Buddha, and the Buddha said, well, sorry, put to this monk's

48
00:06:13,440 --> 00:06:20,160
as you hit him, and what is not even going to ask him, and did you hit him, because it's

49
00:06:20,160 --> 00:06:28,440
absurd, of course, but sorry, put to says, well, you know, for one who has any attachment

50
00:06:28,440 --> 00:06:36,160
to the body, or any sort of ego about self, in regards to this body, it's quite possible

51
00:06:36,160 --> 00:06:42,640
that they could strike someone with the body, but he said, for me, this body is like

52
00:06:42,640 --> 00:06:46,920
a carcass hanging around one's neck.

53
00:06:46,920 --> 00:06:52,560
It's like a imagine there were a young man or a young woman who was fond of adornment

54
00:06:52,560 --> 00:06:58,080
and dressed in fine clothes and jewelry, and then you would take a carcass of a dog

55
00:06:58,080 --> 00:07:02,720
and sling it around their neck, what would you, how would they think, what would they think

56
00:07:02,720 --> 00:07:03,720
of that?

57
00:07:03,720 --> 00:07:10,720
And that's how I view the body, it's a no attachment to the body whatsoever, it's a burden

58
00:07:10,720 --> 00:07:17,600
that I have to carry and care for, it's a sore with nine open wounds, the nine holes

59
00:07:17,600 --> 00:07:25,080
of the body that are oozing all sorts of foul substances, you can count the nine holes,

60
00:07:25,080 --> 00:07:28,720
if you like.

61
00:07:28,720 --> 00:07:37,760
Another time, the people came to the monastery and they brought sweets, some kind of

62
00:07:37,760 --> 00:07:48,080
pastries, I think, and everybody, every month got one, there was one pastry for all the

63
00:07:48,080 --> 00:07:53,000
month, each month, so each month took one pastry, sorry putovers at the head, sitting

64
00:07:53,000 --> 00:08:02,360
in the Donasala, the kitchen, the dining room, I guess, and everybody ate one pastry

65
00:08:02,360 --> 00:08:08,800
at the end of their meal, sort of a luxury, and all the meditators here are thinking

66
00:08:08,800 --> 00:08:15,160
of a whole pastry.

67
00:08:15,160 --> 00:08:20,440
Don't bring up food to meditators, it becomes kind of difficult situation because they've

68
00:08:20,440 --> 00:08:26,280
all been fasting since the morning, the one that's the eating once a day is not that

69
00:08:26,280 --> 00:08:40,560
big of a deal, but it's easy to get attached to food, and so the last one, there was

70
00:08:40,560 --> 00:08:45,880
a monk missing, all the monks got one, but there was one left over because one monk hadn't

71
00:08:45,880 --> 00:08:52,960
come back for a mom's room, and he was usually late maybe, so the monk said, well he's

72
00:08:52,960 --> 00:08:58,240
not coming or he's probably gone deep somewhere else, so they took the last one, they gave

73
00:08:58,240 --> 00:09:02,040
it to, sorry put this, they said, better or both, sorry you eat it, don't let it go

74
00:09:02,040 --> 00:09:07,720
to waste, of course they respect, sorry put it very much, and sorry about the looked around

75
00:09:07,720 --> 00:09:14,880
and the monk wasn't coming and so because they were urging him to eat it, he ate the

76
00:09:14,880 --> 00:09:23,200
last one, and just as he was eating it, this last monk shows up, and immediately, sorry

77
00:09:23,200 --> 00:09:28,760
put the ghost to him and says look, or sitting where he is maybe tells him, he says

78
00:09:28,760 --> 00:09:39,000
there was one left over and I ate it, it was supposed to be for you, but I ate it, and

79
00:09:39,000 --> 00:09:46,520
this monk looked at, sorry put to these and oh well, we all have a sweet tooth, or you

80
00:09:46,520 --> 00:09:51,240
can't help it if you have a sweet tooth, completely blaming, sorry put that, so sorry put

81
00:09:51,240 --> 00:10:01,400
that somehow is greedy for it, it's totally snide and terrible remark to accuse and enlighten

82
00:10:01,400 --> 00:10:08,600
being like that, but, sorry put to his response, it's quite touching, he made a vow, he

83
00:10:08,600 --> 00:10:16,800
vowed from that day forward never to eat pastry again, and so the monks from that time on would

84
00:10:16,800 --> 00:10:20,400
try to tempt, sorry put to with pastry, just to see, just to test them, because they'd

85
00:10:20,400 --> 00:10:26,320
heard that he made this vow never to eat pastry, and so they would bring a pastry to him,

86
00:10:26,320 --> 00:10:34,320
he would refuse to eat it, and so from that day forward to the rest of his life he never

87
00:10:34,320 --> 00:10:45,360
ate pastry, there's another story of Anara Han, there's lots of these stories, this butcher

88
00:10:45,360 --> 00:10:50,040
was cutting up meat and someone came and gave him a ruby and he picked up the ruby and

89
00:10:50,040 --> 00:10:54,960
he was looking at it but his hands were covered in blood and so he put it down in a tray

90
00:10:54,960 --> 00:11:01,800
and he looked at it later, some precious gem, and then he went to the back of the room,

91
00:11:01,800 --> 00:11:07,760
and then this Arahant came to the door and he wanted to give food to the Arahant, and so

92
00:11:07,760 --> 00:11:11,160
he went and he said wait here a venerable sir and he went back into the back room to get

93
00:11:11,160 --> 00:11:19,560
some cooked meat, and Arahant stood there at the door watching in this bird who was, they

94
00:11:19,560 --> 00:11:26,800
had this pet bird in the butcher shop or maybe some pet bird, went over and saw the ruby

95
00:11:26,800 --> 00:11:32,840
sitting in the dish covered in blood and thought it was a piece of meat and Snatch didn't

96
00:11:32,840 --> 00:11:34,160
hate it.

97
00:11:34,160 --> 00:11:38,720
They are a hundred watched this and didn't say anything, they didn't do anything just

98
00:11:38,720 --> 00:11:43,400
stood silently and then the man came back and he was about to give the Arahant some food

99
00:11:43,400 --> 00:11:50,120
and he looked and saw the ruby was gone and there was no one else there except the Arahant

100
00:11:50,120 --> 00:11:56,520
immediately he accused the Arahant and they are a hundred rather than, so you see this

101
00:11:56,520 --> 00:11:59,760
happened and you think well why isn't he telling him why doesn't he tell him that this

102
00:11:59,760 --> 00:12:06,800
bird ate it and the man starts beating this Arahant and ties him up and starts beating

103
00:12:06,800 --> 00:12:12,040
him and pounding him and saying where is it, give me the ruby and the Arahant just

104
00:12:12,040 --> 00:12:19,360
takes all the abuse and he beats him almost to death and then he starts bleeding,

105
00:12:19,360 --> 00:12:26,240
the Arahant starts bleeding and dropping blood on the floor and this bird seeing the blood

106
00:12:26,240 --> 00:12:34,800
waddled over and starts drinking, lapping at the blood maybe or pecking at it I don't

107
00:12:34,800 --> 00:12:45,480
know and the butcher seeing this gives the bird a swift hit and kills it and the Arahant

108
00:12:45,480 --> 00:12:50,000
lying bloody on the floor turns and sees the dead bird and turns these birds as it

109
00:12:50,000 --> 00:12:59,640
then they said the butcher says yeah the bird's dead and you're next and the Arahant said

110
00:12:59,640 --> 00:13:05,280
the bird ate the ruby immediately he tells him what happened right because if he hadn't

111
00:13:05,280 --> 00:13:08,720
the bird would have died he would have killed the bird but now that the bird was dead

112
00:13:08,720 --> 00:13:19,760
he was afraid to tell him and the butcher cut open the bird and sure enough there was

113
00:13:19,760 --> 00:13:31,200
a ruby, I don't think that butcher went to a good place when he died, confidence, lots

114
00:13:31,200 --> 00:13:40,240
of different kinds of confidence, confidence can be dangerous, confidence is not always a good

115
00:13:40,240 --> 00:13:47,480
thing but it can be so powerful, people respect you look up to you, then society it's the

116
00:13:47,480 --> 00:13:53,640
confident ones that are with all different types of confidence and all different sorts of

117
00:13:53,640 --> 00:14:00,120
confidence it's interesting to see how politicians can be taken down with just the slightest

118
00:14:00,120 --> 00:14:10,840
accusation and other politicians can have mud slung at them and they never flint because

119
00:14:10,840 --> 00:14:17,640
they're so confident and they just they don't react, they seem impervious to it, there's

120
00:14:17,640 --> 00:14:30,080
some greatness to a confident but such greatness can be abused and it can be used for

121
00:14:30,080 --> 00:14:36,400
wrong purposes so for meditators we'll demonstrate too far but clearly this is a problem

122
00:14:36,400 --> 00:14:57,480
for a meditator, a meditator will experience any number of any number of pleasant experiences,

123
00:14:57,480 --> 00:15:09,000
any number of attractive phenomena and can become quite attached to them, can cultivate

124
00:15:09,000 --> 00:15:17,480
such powerful mind states that they become intoxicated by them so with too much confidence

125
00:15:17,480 --> 00:15:33,680
the meditators prone to non-reflective or unthinking and healy as Shakespeare would say

126
00:15:33,680 --> 00:15:42,320
and healy confidence then they'll just follow whatever when strange experiences come

127
00:15:42,320 --> 00:15:46,840
they will encourage them when the meditator starts to experience rapture of all sorts

128
00:15:46,840 --> 00:15:54,040
of bodies shaking back and forth, crying, laughing, when energy comes, the meditator becomes

129
00:15:54,040 --> 00:16:00,720
quite confident and just follows after, comes and meshed and caught up and can drive themselves

130
00:16:00,720 --> 00:16:06,480
crazy, I've seen meditators drive themselves crazy without proper guidance, not my students,

131
00:16:06,480 --> 00:16:13,200
I've never had a student go crazy and I've seen other people's students who didn't have

132
00:16:13,200 --> 00:16:20,800
quite the best guidance, get into quite a bit of mental difficulty, I spent four nights

133
00:16:20,800 --> 00:16:28,840
in a mental hospital with a woman who had become temporarily insane through the meditator

134
00:16:28,840 --> 00:16:35,080
meditation practice and so instead of being mindful like seeing, seeing she would say wisdom,

135
00:16:35,080 --> 00:16:45,640
wisdom, all sorts of crazy mantras that she is, it's important to temper your confidence

136
00:16:45,640 --> 00:16:54,320
with wisdom, if you try to cultivate confidence it can lead you astray, it is influenced

137
00:16:54,320 --> 00:17:10,600
by our other mind states by defilement, effort, the second one is effort, equally problematic

138
00:17:10,600 --> 00:17:20,720
but person cultivates too much energy, too much effort, it can be a cause of great distraction

139
00:17:20,720 --> 00:17:28,320
diversion, it can even lead to, it can encourage the sort of temporary insanity in this

140
00:17:28,320 --> 00:17:33,000
talking about, and one meditator is so much energy, it's just got up and ran around the

141
00:17:33,000 --> 00:17:42,800
monastery, couldn't she say she couldn't even stop herself, and the meditator is if they

142
00:17:42,800 --> 00:17:49,240
practice too much walking meditation or they push themselves too hard, walking, sitting,

143
00:17:49,240 --> 00:17:56,040
walking, sitting without taking breaks, it can lead to great mental stress and wind you

144
00:17:56,040 --> 00:18:13,920
up, it can lead to problems in your practice, it's important effort is important on easy

145
00:18:13,920 --> 00:18:19,720
to misunderstand, if you think of effort, the word effort or energy we think is some kind

146
00:18:19,720 --> 00:18:30,080
of exertion, you push yourself, the energy exertion in Buddhism is quite strenuous and

147
00:18:30,080 --> 00:18:36,760
energetic but it's not really about pushing yourself, except pushing yourself to be mindful,

148
00:18:36,760 --> 00:18:44,040
not pushing yourself to walk more, sit more, not pushing your mind to be free from the

149
00:18:44,040 --> 00:18:52,400
fountains, stopping, preventing the defilements through sheer force of will, there are

150
00:18:52,400 --> 00:18:59,480
four great efforts, effort is to remove and prevent unwholesome states from entering the

151
00:18:59,480 --> 00:19:07,800
mind, and cultivate and keep wholesome states in the mind, that's it, we work in that

152
00:19:07,800 --> 00:19:18,000
way, it's quite a meticulous and sort of delicate act or work that we do, it's not just

153
00:19:18,000 --> 00:19:25,520
above barreling ahead like an ox, it's quite delicate, sort of a fragile sort of effort

154
00:19:25,520 --> 00:19:32,080
that you cultivate, at every moment, keep your mind bright, make your mind bright in the

155
00:19:32,080 --> 00:19:41,760
moment, make your mind clear in that moment, the effort to do that now, now, now each moment,

156
00:19:41,760 --> 00:19:49,440
that's right effort, when you say to yourself, rise, sing and you're aware of the rising,

157
00:19:49,440 --> 00:19:58,760
you have right effort, you have everything that really, it's very easy to misunderstand

158
00:19:58,760 --> 00:20:05,360
effort and push too hard, but danger, concentration as well, samadhi, samadhi is an interesting

159
00:20:05,360 --> 00:20:15,560
word, samadhi means, it's probably where we get the word same from, the same sort of

160
00:20:15,560 --> 00:20:26,200
idea as samadhi, so what it means is to have things balanced or focused, right, like

161
00:20:26,200 --> 00:20:36,600
this camera, you have to focus, it's easy to go out of focus, and then we give the talk

162
00:20:36,600 --> 00:20:54,280
on Monday, they push the button and when it completely, how to focus, so we often translate

163
00:20:54,280 --> 00:21:02,920
as concentration, but concentration as such a specific quality to it, right, concentration

164
00:21:02,920 --> 00:21:08,240
is to fix your mind on one object, it's sort of what we're talking about, but it becomes

165
00:21:08,240 --> 00:21:19,200
very heavy where you exclude reality, where you force your mind by sheer force of concentration

166
00:21:19,200 --> 00:21:27,000
we would say, ignoring everything else, not paying attention to anything else, keeping

167
00:21:27,000 --> 00:21:34,720
your mind fixed and focused on one object, which is a valid sort of concentration, but

168
00:21:34,720 --> 00:21:39,440
it's not the sort of concentration that allows you to see in vermin and suffering in

169
00:21:39,440 --> 00:21:45,760
oneself, you can't see the three characteristics, and you can't see the nature of reality

170
00:21:45,760 --> 00:21:51,400
because your mind isn't focused on reality, it's focused on whatever concept you've

171
00:21:51,400 --> 00:21:58,680
taken as the object, right, only concepts can be the object of such concentration, you couldn't

172
00:21:58,680 --> 00:22:05,760
fix your mind on a reality because they're born and die in a moment, I say I'm going

173
00:22:05,760 --> 00:22:13,240
to concentrate and fix my mind steadily on a single reality, you may have lost in the

174
00:22:13,240 --> 00:22:26,600
dust when it's gone in a finger snap, it's a reality as momentary, so what we mean here

175
00:22:26,600 --> 00:22:35,400
is focus, it's when you're clearly focused in aware and fixed on reality for the moment

176
00:22:35,400 --> 00:22:44,800
that it arises, each moment, like being in focus, when you're in focus you can see there's

177
00:22:44,800 --> 00:22:54,680
quality of mind that is not distracted, not superficial, the mind that deeply penetrates

178
00:22:54,680 --> 00:23:02,320
that moment just for a moment and then the next moment, so again it's very much tied

179
00:23:02,320 --> 00:23:07,360
up with mindfulness, all of these, this is why you can't really say I'm going to cultivate

180
00:23:07,360 --> 00:23:12,600
them apart from mindfulness, okay we'll do mindfulness but also on top of that I'm going

181
00:23:12,600 --> 00:23:18,920
to cultivate confidence or effort, you end up cultivating the wrong sort of confidence

182
00:23:18,920 --> 00:23:25,360
in effort and concentration and even wisdom to think that you could cultivate wisdom outside

183
00:23:25,360 --> 00:23:35,560
of mindfulness of septic, like the talking about this last night, the word wisdom is of

184
00:23:35,560 --> 00:23:41,720
course laden with all sorts of specific qualities that are not exactly what we mean by

185
00:23:41,720 --> 00:23:46,360
when the stomach rises, know that it's rising, when you see, know it just as seeing

186
00:23:46,360 --> 00:23:58,240
deep day, deep to matanga, you see, you can do the talk by here, let's see, you can just

187
00:23:58,240 --> 00:24:05,320
be seeing, that's banya, that's what we mean, so again completely tied up with septic

188
00:24:05,320 --> 00:24:09,920
as for septic itself which translated as mindfulness but the word mindfulness has a lot

189
00:24:09,920 --> 00:24:16,320
of baggage in its own, it can be mindful of a person's feelings, you can be mindful

190
00:24:16,320 --> 00:24:25,560
of the time, you can be mindful of what you're doing, you can have the sense that it

191
00:24:25,560 --> 00:24:34,560
means sort of a carefulness, conscientiousness or an attention, different things sometimes

192
00:24:34,560 --> 00:24:42,000
attention, sometimes conscientiousness but septic doesn't really mean that, it has these

193
00:24:42,000 --> 00:24:50,520
qualities to it but it really means to remember, septic means to remember yourself, it means

194
00:24:50,520 --> 00:24:59,840
to have your mind on the experience, on reality, how reality works is we'll see or hear

195
00:24:59,840 --> 00:25:05,440
or smell or taste or feel or think and then we're off, we're gone, the seeing occurs

196
00:25:05,440 --> 00:25:12,560
and then we get lost in it, not in it, not in the seeing but in what we think about it,

197
00:25:12,560 --> 00:25:18,480
we saw something and oh that was nice, what was that, well can I get that back, wonder

198
00:25:18,480 --> 00:25:23,320
what that meant, did that mean something, is that the path to alignment, maybe if I see

199
00:25:23,320 --> 00:25:28,440
it again then I'll become enlightened, maybe if I try to make it come then it will

200
00:25:28,440 --> 00:25:32,720
be surprised at how many meditators get caught up in these things that they see, for

201
00:25:32,720 --> 00:25:44,600
example, easy to get lost, easy to lose your way, but we do this as not in nature, it was

202
00:25:44,600 --> 00:25:54,920
teaching today, teaching meditation today at McMaster, teaching a study center, just a few

203
00:25:54,920 --> 00:26:03,040
people, trying to organize a bunch of people that ended up being four of us, but the head

204
00:26:03,040 --> 00:26:11,440
woman shoots, she asks, if I say pain, pain, what would the pain get worse, so I'd explain

205
00:26:11,440 --> 00:26:18,760
to her that pain isn't really the problem, there's a difference between pain and disliking,

206
00:26:18,760 --> 00:26:28,800
disliking is what causes us suffering, the mindfulness is reminding ourselves and remembering

207
00:26:28,800 --> 00:26:37,360
the actual experience instead of getting caught up in extrapolation, but buncha, which we

208
00:26:37,360 --> 00:26:47,880
also talked about recently, so again very simple, very simple qualities of might, not something

209
00:26:47,880 --> 00:26:59,560
you have to diversify, you don't have to really analyze, you don't have to analyze that,

210
00:26:59,560 --> 00:27:04,240
do I have enough of this or enough of that, you have to use mindfulness, when you use

211
00:27:04,240 --> 00:27:15,880
mindfulness they will balance out by themselves, the five faculties, they will come part

212
00:27:15,880 --> 00:27:23,720
and parcel to mindfulness to study, when you're clearly aware of things as they are objectively,

213
00:27:23,720 --> 00:27:30,160
all of the sorts of confidence, effort, concentration, wisdom that are not part of the path

214
00:27:30,160 --> 00:27:36,360
and especially the problematic ones, they'll be weaker and become weaker and weaker

215
00:27:36,360 --> 00:27:45,320
through your practice because you'll see how they're causing you stress and suffering, or at the very

216
00:27:45,320 --> 00:27:59,280
best to how they're and how useless they are, meaningless, pointless, so for this reason

217
00:27:59,280 --> 00:28:04,920
the would have focused especially on mindfulness, he said many, many talks about mindfulness

218
00:28:04,920 --> 00:28:13,720
and coined the term for satipatana, the four foundations of mindfulness, the aikai and

219
00:28:13,720 --> 00:28:27,760
amanga, the path, the one way path or the one path, so good for us to reflect on these

220
00:28:27,760 --> 00:28:34,160
things and to see and good to watch out for them, if you see yourself getting confident

221
00:28:34,160 --> 00:28:41,280
overly confident or too much effort, too much concentration, too much thinking and wisdom,

222
00:28:41,280 --> 00:28:47,040
sort of conventional wisdom, you can not apply mindfulness to them, to judge them or get rid

223
00:28:47,040 --> 00:28:53,320
of them, but bring yourself back on track so that you're mindful of them and not clinging

224
00:28:53,320 --> 00:29:00,480
or following or encouraging them, then we can continue on with our cultivation of what

225
00:29:00,480 --> 00:29:10,040
really matters is satipatana, so there you go, there's the dhamma for tonight, thank you

226
00:29:10,040 --> 00:29:37,960
all for tuning in.

