1
00:00:00,000 --> 00:00:02,000
You

2
00:00:30,000 --> 00:00:32,000
You

3
00:01:00,000 --> 00:01:02,000
You

4
00:01:23,000 --> 00:01:26,000
Good evening, everyone. Welcome to our

5
00:01:26,000 --> 00:01:30,000
My broadcast

6
00:01:34,000 --> 00:01:38,000
Today we're looking at

7
00:01:38,000 --> 00:01:42,000
The good tourniquet book of five

8
00:01:42,000 --> 00:01:46,000
So to forty eight

9
00:01:46,000 --> 00:01:50,000
Alabanea

10
00:01:50,000 --> 00:01:54,000
Thomas

11
00:01:54,000 --> 00:01:56,000
Alabanea

12
00:01:56,000 --> 00:02:00,000
Tana is a

13
00:02:00,000 --> 00:02:02,000
Tana means a place

14
00:02:02,000 --> 00:02:06,000
But here it means

15
00:02:06,000 --> 00:02:10,000
A state or a case

16
00:02:10,000 --> 00:02:14,000
Alabanea means unattainable

17
00:02:14,000 --> 00:02:18,000
And

18
00:02:18,000 --> 00:02:22,000
unattainable

19
00:02:22,000 --> 00:02:26,000
So there are five of the Buddhists

20
00:02:26,000 --> 00:02:30,000
Bantimani, Alabanea, Tana

21
00:02:30,000 --> 00:02:34,000
And there are these five

22
00:02:34,000 --> 00:02:36,000
unattainable states

23
00:02:36,000 --> 00:02:38,000
Things that are unattainable

24
00:02:38,000 --> 00:02:42,000
Some are need of a

25
00:02:42,000 --> 00:02:46,000
Brahmani, Nava, Brahmana, Nava, Brahmanava

26
00:02:46,000 --> 00:02:50,000
Tana looks at me by anyone in this world

27
00:02:50,000 --> 00:02:54,000
By a recluse or a Brahman

28
00:02:54,000 --> 00:02:58,000
No matter how religious or spiritually powerful

29
00:02:58,000 --> 00:03:02,000
They may be, or even if they're an angel

30
00:03:02,000 --> 00:03:06,000
Or a demon or a god

31
00:03:06,000 --> 00:03:10,000
Can't obtain

32
00:03:10,000 --> 00:03:12,000
No matter how powerful you are

33
00:03:12,000 --> 00:03:16,000
Certain things that we can't get

34
00:03:28,000 --> 00:03:32,000
And these four are five

35
00:03:32,000 --> 00:03:36,000
In regards to things that are of a nature

36
00:03:36,000 --> 00:03:42,000
To get old Jarada, Mahdiri

37
00:03:42,000 --> 00:03:44,000
May they not get old

38
00:03:44,000 --> 00:03:46,000
Can't have that

39
00:03:46,000 --> 00:03:48,000
You can't have that

40
00:03:48,000 --> 00:03:52,000
I won't come true, no matter what you do

41
00:03:52,000 --> 00:04:02,000
We are the Dhamma, Mahdiri

42
00:04:02,000 --> 00:04:08,000
What is of a nature to get sick?

43
00:04:08,000 --> 00:04:12,000
May they not get sick

44
00:04:12,000 --> 00:04:24,000
Number three, Marana, Dhamma, Mahdiri

45
00:04:24,000 --> 00:04:30,000
That which is of a nature to die may it not die

46
00:04:30,000 --> 00:04:32,000
Kaya, Dhamma, Mahdiri

47
00:04:32,000 --> 00:04:36,000
That which is of a nature to

48
00:04:36,000 --> 00:04:44,000
Be destroyed, may it not be destroyed

49
00:04:44,000 --> 00:04:48,000
That which is subject to decay may it not decay

50
00:04:48,000 --> 00:04:52,000
Nasana, Dhamma, Mahdiri

51
00:04:52,000 --> 00:04:58,000
Number five, that which is subject to destruction

52
00:04:58,000 --> 00:05:08,000
Kaya is more like decay or wasting away

53
00:05:08,000 --> 00:05:14,000
Nastana's destruction or

54
00:05:14,000 --> 00:05:22,000
nullification

55
00:05:22,000 --> 00:05:26,000
The point being here, the interesting thing about this

56
00:05:26,000 --> 00:05:30,000
is that this is very much worth stopping

57
00:05:30,000 --> 00:05:34,000
at and coming over

58
00:05:34,000 --> 00:05:44,000
Is that no matter how hard we try, we can't

59
00:05:44,000 --> 00:05:50,000
fix and we can't perfect life

60
00:05:50,000 --> 00:05:54,000
There are certain realities that we have to face

61
00:05:54,000 --> 00:06:00,000
That we will never be able to obtain

62
00:06:00,000 --> 00:06:04,000
a state of being whereby we always get what we want

63
00:06:04,000 --> 00:06:06,000
We always have what we want

64
00:06:06,000 --> 00:06:10,000
We can keep what we want and only what we want

65
00:06:10,000 --> 00:06:14,000
Where we aren't faced with

66
00:06:14,000 --> 00:06:18,000
realities that are unpleasant to us

67
00:06:18,000 --> 00:06:28,000
That reality is not based on our desires

68
00:06:28,000 --> 00:06:34,000
So he says an ordinary person, someone who doesn't realize

69
00:06:34,000 --> 00:06:38,000
this, people living in the world

70
00:06:38,000 --> 00:06:42,000
For them what is subject to old age

71
00:06:42,000 --> 00:06:46,000
get grows old, their body gets old

72
00:06:46,000 --> 00:06:50,000
and their loved ones grow old

73
00:06:50,000 --> 00:06:54,000
and the possessions age

74
00:07:02,000 --> 00:07:06,000
Because they don't see that this isn't part of nature

75
00:07:06,000 --> 00:07:09,000
We are talking about last night about funerals

76
00:07:09,000 --> 00:07:14,000
Because we are not clearly aware of this as being a part of nature

77
00:07:14,000 --> 00:07:17,000
We suffer because of old age

78
00:07:17,000 --> 00:07:19,000
We suffer because of sickness

79
00:07:19,000 --> 00:07:21,000
We suffer because of death

80
00:07:21,000 --> 00:07:24,000
We suffer unnecessarily

81
00:07:24,000 --> 00:07:29,000
We suffer based not simply on the nature of the experience

82
00:07:29,000 --> 00:07:32,000
But based on our expectations

83
00:07:32,000 --> 00:07:35,000
and our delusions that it could somehow be otherwise

84
00:07:35,000 --> 00:07:39,000
We work very hard to avoid death

85
00:07:39,000 --> 00:07:42,000
To avoid old age to avoid sickness

86
00:07:42,000 --> 00:07:46,000
We are very hard to escape its grasp

87
00:07:46,000 --> 00:07:52,000
People who are obsessed with health or obsessed with youth

88
00:07:52,000 --> 00:07:54,000
We work very hard

89
00:07:54,000 --> 00:07:57,000
People who are obsessed with life

90
00:07:57,000 --> 00:08:00,000
We fear death

91
00:08:00,000 --> 00:08:03,000
Old age sickness and death

92
00:08:03,000 --> 00:08:06,000
Very much a part of who we are

93
00:08:06,000 --> 00:08:08,000
But also with everything

94
00:08:08,000 --> 00:08:13,000
Even those things that don't actually die

95
00:08:13,000 --> 00:08:17,000
They are still subject to dissolution to Kaya

96
00:08:17,000 --> 00:08:21,000
and Nastana

97
00:08:21,000 --> 00:08:24,000
We have old age sickness and death

98
00:08:24,000 --> 00:08:28,000
But also simple dissolution and destruction

99
00:08:28,000 --> 00:08:32,000
With everything we hold dear

100
00:08:32,000 --> 00:08:37,000
We are saving besides our own body

101
00:08:37,000 --> 00:08:42,000
There is nothing else that can be a refuge for us

102
00:08:42,000 --> 00:08:51,000
So there is really in an ultimate sense

103
00:08:51,000 --> 00:08:55,000
Especially for meditators it is working on two levels

104
00:08:55,000 --> 00:08:57,000
The concept of death

105
00:08:57,000 --> 00:09:00,000
or the concept of losing

106
00:09:00,000 --> 00:09:02,000
What you hold dear

107
00:09:02,000 --> 00:09:04,000
The conceptual level is clear

108
00:09:04,000 --> 00:09:09,000
And those relatives, friends, loved ones, possessions

109
00:09:09,000 --> 00:09:12,000
Even your own body

110
00:09:12,000 --> 00:09:15,000
With it is all conceptual

111
00:09:15,000 --> 00:09:18,000
It doesn't actually describe what is really happening

112
00:09:18,000 --> 00:09:20,000
Because we get stuck on concepts

113
00:09:20,000 --> 00:09:26,000
We have this idea that somehow we can achieve a state

114
00:09:26,000 --> 00:09:30,000
whereby our concepts are

115
00:09:30,000 --> 00:09:33,000
And by concepts I mean things

116
00:09:33,000 --> 00:09:37,000
where the things we own can last forever

117
00:09:37,000 --> 00:09:39,000
It seems theoretically possible

118
00:09:39,000 --> 00:09:46,000
That you should work really hard so that you get to keep everything

119
00:09:46,000 --> 00:09:55,000
Or you might even say, well, at least I can keep things for years

120
00:09:55,000 --> 00:09:58,000
Or you'd say, well, at least I can keep things for months

121
00:09:58,000 --> 00:10:02,000
Or days or hours or minutes

122
00:10:02,000 --> 00:10:05,000
At least I have some good experiences that last

123
00:10:05,000 --> 00:10:09,000
And are dependable and are satisfied

124
00:10:09,000 --> 00:10:12,000
Unfortunately, the underlying reality

125
00:10:12,000 --> 00:10:15,000
Even doesn't allow for that

126
00:10:15,000 --> 00:10:23,000
Because the second level in which this works is ultimate reality

127
00:10:23,000 --> 00:10:28,000
The death is actually something that occurs every moment

128
00:10:28,000 --> 00:10:31,000
And our suffering isn't actually

129
00:10:31,000 --> 00:10:36,000
Based on concepts, it's based on ultimate reality

130
00:10:36,000 --> 00:10:42,000
And our inability to see how reality is working behind the scenes

131
00:10:42,000 --> 00:10:53,000
So we're unable to see how our emotions, how the objects of our emotions

132
00:10:53,000 --> 00:10:58,000
The things we like and dislike, how they change so quickly

133
00:10:58,000 --> 00:11:03,000
So we like something, what we really like is the experience

134
00:11:03,000 --> 00:11:05,000
The feeling of pleasure

135
00:11:05,000 --> 00:11:11,000
Or the thing that the experience that we recognize is bringing us pleasure

136
00:11:11,000 --> 00:11:17,000
Whether it changes immediately, it comes when it goes

137
00:11:17,000 --> 00:11:21,000
When it changes, we're left with stress

138
00:11:21,000 --> 00:11:25,000
Because we don't no longer have what we want

139
00:11:25,000 --> 00:11:31,000
When we want something that we've gotten, we want it more

140
00:11:31,000 --> 00:11:35,000
We become attached and addicted to these things

141
00:11:35,000 --> 00:11:49,000
But the real reason why we can't keep the concepts of people in places and things

142
00:11:49,000 --> 00:11:56,000
Is because the ultimate reality underneath it is changing moment by moment

143
00:11:56,000 --> 00:11:58,000
This is what we really see in meditation

144
00:11:58,000 --> 00:12:00,000
Not really so much

145
00:12:00,000 --> 00:12:03,000
About, oh dear, I will have to one day die

146
00:12:03,000 --> 00:12:06,000
It's much more visceral than that

147
00:12:06,000 --> 00:12:08,000
It's that I'm dying every moment

148
00:12:08,000 --> 00:12:11,000
Being born and dying

149
00:12:11,000 --> 00:12:16,000
Being born, die, born, die every moment

150
00:12:16,000 --> 00:12:20,000
And that reality is not something that should be clung to

151
00:12:20,000 --> 00:12:24,000
That there's not no part of reality

152
00:12:24,000 --> 00:12:30,000
That gives us any reason to think that it could be a refuge or satisfaction to us

153
00:12:30,000 --> 00:12:35,000
Because it lasts for the moment

154
00:12:35,000 --> 00:12:39,000
So this whole idea of clinging and why clinging and craving is wrong

155
00:12:39,000 --> 00:12:43,000
We sometimes doubt and wonder, why is it wrong to like things?

156
00:12:43,000 --> 00:12:46,000
What would it be like to not like things?

157
00:12:46,000 --> 00:12:53,000
It's really a complete misunderstanding of the very framework of reality

158
00:12:53,000 --> 00:12:57,000
There's nothing to do really with stopping liking things or people

159
00:12:57,000 --> 00:13:01,000
Those things and people don't exist

160
00:13:01,000 --> 00:13:07,000
About stopping to cling or concern or self with what really is born and dies

161
00:13:07,000 --> 00:13:11,000
That's our experiences

162
00:13:11,000 --> 00:13:14,000
But either way on both levels

163
00:13:14,000 --> 00:13:17,000
It clearly works on both levels

164
00:13:17,000 --> 00:13:27,000
And it's something that gives us a real shock

165
00:13:27,000 --> 00:13:31,000
Something that gives us some sense of alarm

166
00:13:31,000 --> 00:13:35,000
And should give us some sense of concern

167
00:13:35,000 --> 00:13:39,000
The things that we depend upon

168
00:13:39,000 --> 00:13:42,000
are not dependable

169
00:13:42,000 --> 00:13:47,000
Our own life, who we are, our identity

170
00:13:47,000 --> 00:13:55,000
Our very soul itself is subject to change, is subject to dissolution

171
00:13:55,000 --> 00:13:59,000
Our very being

172
00:13:59,000 --> 00:14:05,000
We are subject to death, all of our family is subject to death

173
00:14:05,000 --> 00:14:11,000
So to run away from old age, to run away from sickness, to run away from death

174
00:14:11,000 --> 00:14:17,000
Not even the gods have the power to stop

175
00:14:17,000 --> 00:14:21,000
It's a part of how nature is made

176
00:14:21,000 --> 00:14:27,000
Reality is a momentary experience that constantly creating the change

177
00:14:27,000 --> 00:14:31,000
That we see on a conceptual level of people in places and things

178
00:14:31,000 --> 00:14:37,000
So not to belabor the point, but we're remembering

179
00:14:37,000 --> 00:14:40,000
It's really a good way of understanding

180
00:14:40,000 --> 00:14:45,000
Why am I working so hard to better myself

181
00:14:45,000 --> 00:14:50,000
When I'm trying to cultivate spiritual realization

182
00:14:50,000 --> 00:14:55,000
To fear ourselves from the unreasonable expectations

183
00:14:55,000 --> 00:15:03,000
Of youth, health, life, stability, continuity, certainty

184
00:15:03,000 --> 00:15:07,000
To be able to deal with the truth of change

185
00:15:07,000 --> 00:15:13,000
That really is the big thing that we're not able to deal with

186
00:15:13,000 --> 00:15:16,000
It's really change, if everything stays the same all the time

187
00:15:16,000 --> 00:15:19,000
When we get used to it

188
00:15:19,000 --> 00:15:27,000
That we're constantly disappointed, and our expectations are not met because of change

189
00:15:27,000 --> 00:15:30,000
Because of chaos, but mostly because of our delusion

190
00:15:30,000 --> 00:15:32,000
Simple delusion, it's kind of silly

191
00:15:32,000 --> 00:15:36,000
Well, if you understood that everything changes constantly

192
00:15:36,000 --> 00:15:41,000
We're cultivating the expectation of stability

193
00:15:41,000 --> 00:15:44,000
or partiality

194
00:15:44,000 --> 00:15:52,000
And that's really what we come to see, we simply see that reality is chaotic

195
00:15:52,000 --> 00:15:57,000
And if that is a fact of life, which it turns out to be

196
00:15:57,000 --> 00:16:03,000
Then we must necessarily give up expectations

197
00:16:03,000 --> 00:16:15,000
And our attempts to live, to live long and be healthy and young

198
00:16:15,000 --> 00:16:19,000
And to keep the things that we like

199
00:16:19,000 --> 00:16:25,000
Eventually we see that even liking itself is part of the problem

200
00:16:25,000 --> 00:16:29,000
Really the source of our problems

201
00:16:29,000 --> 00:16:34,000
Because it means being partial to things that are outside of our control

202
00:16:34,000 --> 00:16:40,000
Ultimately, we can't stop them from changing and going

203
00:16:40,000 --> 00:16:46,000
So a good memory, a good reminder

204
00:16:46,000 --> 00:16:50,000
What ultimately circumscribes are happiness

205
00:16:50,000 --> 00:16:54,000
These things cannot be changed by us or anyone

206
00:16:54,000 --> 00:17:02,000
But there's no practice we could do or prayer we could say that would give us

207
00:17:02,000 --> 00:17:11,000
Eternal life, eternal youth, eternal health, eternal anything

208
00:17:11,000 --> 00:17:17,000
So there you go, that's the number for tonight

209
00:17:17,000 --> 00:17:25,000
Robin has not decided not to join tonight, just fine

210
00:17:25,000 --> 00:17:33,000
Certainly she is, she acts above and beyond

211
00:17:33,000 --> 00:17:37,000
So we'll give her a night off

212
00:17:37,000 --> 00:17:56,000
But nonetheless, I have a bunch of questions, so I'll answer that

213
00:17:56,000 --> 00:18:06,000
Michael, you should go

214
00:18:06,000 --> 00:18:11,000
During sitting meditation and arising, falling in practice of daily mindfulness

215
00:18:11,000 --> 00:18:14,000
I began to hear and feel my heart be with more clarity

216
00:18:14,000 --> 00:18:18,000
I've been identifying it with the mantra heartbeat heartbeat heartbeat

217
00:18:18,000 --> 00:18:22,000
Just a correct practice, say it's not terrible

218
00:18:22,000 --> 00:18:26,000
But the heart is just a concept, what you're really experiencing is a feeling

219
00:18:26,000 --> 00:18:31,000
So to be more correct, you'd want to say feeling, feeling

220
00:18:31,000 --> 00:18:35,000
You can also say something like beating, beating, but it's

221
00:18:35,000 --> 00:18:42,000
a feeling works just as well

222
00:18:42,000 --> 00:18:46,000
The experience of sitting lingers for longer than one noting period

223
00:18:46,000 --> 00:18:51,000
So that continues not to sitting until that one experience ceases

224
00:18:51,000 --> 00:18:57,000
So instead of one noting of it and move on, even if the experience hasn't ceased on its own

225
00:18:57,000 --> 00:19:03,000
Yeah, just do it once, just do it once and move on

226
00:19:03,000 --> 00:19:07,000
Oh, I didn't click

227
00:19:07,000 --> 00:19:12,000
It was to click, and I didn't work anyway

228
00:19:12,000 --> 00:19:34,000
It's not, it wouldn't be wrong, but it starts to get a little bit

229
00:19:34,000 --> 00:19:46,000
And then add Hawk, better off to stick to the program, I wouldn't worry if there's still a potential to focus on it more

230
00:19:46,000 --> 00:19:49,000
Just say sitting once and then move on to the next one

231
00:19:49,000 --> 00:19:54,000
But that's for that specific technique that we give to you

232
00:19:54,000 --> 00:20:02,000
When noting more, I'm going to click this, right?

233
00:20:02,000 --> 00:20:08,000
When noting more steps and walking meditation is it better to slow our pace in order to make the noting easier

234
00:20:08,000 --> 00:20:12,000
Those should be striving to speed up the noting to match the original pace

235
00:20:12,000 --> 00:20:17,000
Well each, no, it should, I mean the step itself will take longer

236
00:20:17,000 --> 00:20:23,000
Sure, but each movement should be an ordinary speed, it shouldn't be too quick or too slow

237
00:20:23,000 --> 00:20:30,000
But because there's more steps in it, and yeah, it will take longer, a little bit anyway

238
00:20:37,000 --> 00:20:40,000
I see you use the internet quite frequently

239
00:20:40,000 --> 00:20:44,000
Our all Buddhist monks allowed to use the internet to reasonable extent

240
00:20:44,000 --> 00:20:48,000
It really depends on where you are, your situation

241
00:20:48,000 --> 00:20:52,000
There's obviously no rule about using the internet though

242
00:20:52,000 --> 00:20:59,000
I can understand the potential desire to circumscribe access to the internet

243
00:20:59,000 --> 00:21:04,000
For the question that it's problematic

244
00:21:12,000 --> 00:21:19,000
Once the environment is realized, how can I be certain that I will be released for me, birth cycle for good

245
00:21:19,000 --> 00:21:24,000
I don't know the depressed state, but it's as though I don't want to come back here for another life

246
00:21:28,000 --> 00:21:31,000
Well, I mean the cause of rebirth is craving

247
00:21:31,000 --> 00:21:37,000
The benefit of seeing Nibana is it reduces your craving

248
00:21:37,000 --> 00:21:49,000
As you've seen in true peace, and so you crave less and less and you're more objective and neutral sense

249
00:21:49,000 --> 00:21:53,000
And so that's how you know that you're not going to be reborn

250
00:21:53,000 --> 00:21:58,000
because you have no more craving left

251
00:21:58,000 --> 00:22:05,000
No more desire

252
00:22:05,000 --> 00:22:08,000
What is the difference between realization, awareness, and judgment?

253
00:22:08,000 --> 00:22:15,000
How do you know that what you're saying to yourself when meditate is not a judgment of your experience?

254
00:22:15,000 --> 00:22:22,000
Well, judgment would be adding something to it

255
00:22:22,000 --> 00:22:28,000
If you feel pain and you say to yourself pain, pain, you're not adding anything to it, it is pain

256
00:22:28,000 --> 00:22:35,000
Of course if you say to yourself pain, you're adding the dislike to it, what you're saying is this is bad

257
00:22:35,000 --> 00:22:39,000
You're not saying it verbally, but you're cultivating that

258
00:22:39,000 --> 00:22:43,000
So there doesn't need to be a certain quality to your noting

259
00:22:43,000 --> 00:22:45,000
It has to be neutral

260
00:22:45,000 --> 00:23:07,000
But if you were to sit there and say bad, bad, bad, well that would be in judgment

261
00:23:07,000 --> 00:23:15,000
A couple of questions there that I'm not going to bother posting or adding

262
00:23:15,000 --> 00:23:20,000
But the questions here are meant to be about our tradition

263
00:23:20,000 --> 00:23:25,000
and for the benefit of people practicing our tradition

264
00:23:25,000 --> 00:23:29,000
So the ideas are about our meditation practice

265
00:23:29,000 --> 00:23:38,000
Or some general Buddhist principles, I'm happy to discuss to some extent

266
00:23:38,000 --> 00:23:46,000
The idea of speculative or other sorts of questions, questions, or other types of practice

267
00:23:46,000 --> 00:23:48,000
Not really the purpose

268
00:23:48,000 --> 00:23:52,000
Anyway, hope that was useful

269
00:23:52,000 --> 00:23:59,000
Just giving them a little bit of a talk every night

270
00:23:59,000 --> 00:24:02,000
It's good for the meditators to hear something

271
00:24:02,000 --> 00:24:07,000
So give them a reason and remind them of why they're doing it

272
00:24:07,000 --> 00:24:11,000
Thank you all for tuning in, which can you all good practice

273
00:24:11,000 --> 00:24:27,000
Have a good night

