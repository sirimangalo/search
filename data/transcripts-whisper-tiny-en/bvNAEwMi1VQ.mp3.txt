Adam asks, does Buddhism share any similarity with non-dualism, ideas like we are all one
consciousness experiencing itself subjectively, or that everything is nothing and nothing
is everything, etc. There are many different views about that. I would say there is, in Mahayana
Buddhism, certainly they do say these things that everything is one and we are all one
and there are other sects. There is nothing and everything is nothing and nothing is
everything and so on. It should probably not be just so generalized because it is both
true. It just depends on the level in which you look at it. There are certainly
experiences of being one with everything in meditation and there are experiences of
nothingness. I think to be fair with it to be objective, we should consider that everything
is possible but there is only one truth in the end and that is that things are arising
and seizing. No matter what we think about them, no matter if we think, oh we are all one
or if we think everything is nothing, it is just thinking and it is just an experience
and it is impermanent, so it is thinking. The argument kind of reminds me of this one
discourse, the way we bunkers it or something, I don't remember the name of horrible, this
scholarly stuff, but there is just one truth over these two monks. I think our monk and
the lay person were fighting over this. It is not the same question but a similar question.
Similar numbers question, similar in the sense that it is just numbers. One of them said
there are three types of feelings. The Buddha has taught that there are three types of
feelings. The monk said, no no, the Buddha taught that there are five types of weight
and feeling and they argued back and forth and back and forth and then they went to the
Buddha. They went to Ananda, I think and Ananda brought it to the Buddha and the Buddha
said actually there are 108 types of feelings and I think it goes because of the past
present future because of the six senses you can multiply it out. The idea of non-duality
is just a theory in the concept and there is nothing to do with what is real. Why I thought
of this is because I was thinking there are six things. We don't have one thing or two
things. We have six things, seeing, hearing, smelling, tasting, feeling, thinking. If you
want to say there is one thing and there is only experience or there is only reality. The
one sense that I don't agree with things like non-dualism is the idea that Nibana is
the same as Samsara because to me that is a fundamental misunderstanding of the difference
between an arising and a ceasing or an arising and a non-arising. Phenomena do arrives.
The non-arising is Nibana. The arising is Samsara. There are cases where it seems
fundamentally untrue but the whole idea of us being one, well, you know, you could say
that we are two or you could say that we are three or you could say that we are six. In
the end it is what it is. It is Panyan. He says it is just experience. I don't know.
Maybe that wasn't such an important point but the important most important point is that
it is all just thinking. It is all just ideas and views really but whether things are
dualistic or non-dualistic.
