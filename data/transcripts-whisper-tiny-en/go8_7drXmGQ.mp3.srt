1
00:00:00,000 --> 00:00:07,400
Instruction the mantra should not be at the mouth or in the head whenever I know something it appears in my head as a thought

2
00:00:07,800 --> 00:00:14,340
When I'm tired, I'm thinking tired tired tired is this important. Thank you

3
00:00:17,480 --> 00:00:20,060
Well, when you when you get better at it the thought is

4
00:00:21,360 --> 00:00:23,360
is really in the object

5
00:00:23,360 --> 00:00:33,120
The head and thinking is is usually a product of of people who are accustomed to thinking a lot and

6
00:00:34,120 --> 00:00:37,000
so when given this sort of exercise

7
00:00:37,600 --> 00:00:39,600
the mind's only way of

8
00:00:40,320 --> 00:00:42,760
the untrained mind's only way of of

9
00:00:44,480 --> 00:00:47,800
Implementing it is to create it as a thought in the head

10
00:00:47,800 --> 00:00:52,240
I mean when I started I was actually seeing the words like they were pasted to the inside of my forehead and

11
00:00:52,240 --> 00:00:54,240
and getting headaches

12
00:00:54,240 --> 00:00:58,160
Part of why people think it's in the head is because in the beginning they often get

13
00:00:58,720 --> 00:01:01,520
tension in the head from from forcing

14
00:01:02,080 --> 00:01:06,400
themselves to be with the object and so they they confuse that with the actual acknowledgement

15
00:01:06,400 --> 00:01:09,360
They think I'm acknowledging in my head, but they're not they're

16
00:01:10,560 --> 00:01:18,240
The head is just giving rise to tension or exhibiting tension as a result of the

17
00:01:19,040 --> 00:01:21,040
person's

18
00:01:21,040 --> 00:01:23,040
tense forceful

19
00:01:23,200 --> 00:01:25,200
controlling mind state

20
00:01:26,640 --> 00:01:29,640
But but it's all just a matter of

21
00:01:34,840 --> 00:01:37,600
Lack of proficiency once you get better at it

22
00:01:38,480 --> 00:01:39,640
the

23
00:01:39,640 --> 00:01:41,640
thoughts themselves

24
00:01:41,640 --> 00:01:48,360
Become natural become a part of the experience. So it's just experiencing it as it is

25
00:01:48,360 --> 00:01:50,360
It's

26
00:01:50,520 --> 00:01:52,520
It's

27
00:01:52,520 --> 00:02:00,760
The tone is different and it's like as though you're saying it in your head or saying it in your foot or saying it in your stomach

28
00:02:03,240 --> 00:02:09,760
The only time it should be in the head is when you're in the acknowledging pain or aching or tension or feeling

29
00:02:09,760 --> 00:02:19,760
It's one in the head

