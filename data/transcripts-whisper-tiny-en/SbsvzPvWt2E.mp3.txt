No, I lost you there, you're sound is turned up.
Yes.
Okay, sorry, go ahead.
I am having a difficult time reconciling not having with my words while telling the truth.
I know someone who asks me a question only to hear a soft response and gets upset when I answer honestly.
Do I look fat in this dress?
Yes.
I was saying here is this example.
Do I look fat in this dress?
Yes, yes, you look fat in that dress.
What do you do?
How can you can you really answer and tell someone that they look fat in a dress?
But his answer is...
Well, I mean, you have to be clever.
No, you have to be perfect.
And you have to let go.
There are two sides to this.
This is the other side of lying.
What do you do when you're in a position to lie?
What do you do when telling the truth is going to disrupt your way of life?
And so there's two sides.
One, you become skillful so you don't disrupt your life.
And two, you take it as a chance to point out to yourself that it's only because of your clinging that there's a problem here.
In some cases, it's only because you're clinging to a relationship that's based on lies, even even white lies.
Because people are so vain and self-conscious and so on.
And you really want to be around people who ask, how do I look?
How do I look?
All the time. Or even can be even more sinister than that. People who need to be told lies just to be happy.
And so is this really the kind of relationship that you want to have?
Sam Harris, I would look up this book by Sam Harris lying, lying by Sam Harris.
I think it's no longer for free, but you can find it for free on the script. I found it.
I've got it for free. I don't know. It'd be even worth buying it.
You know, this is things. See, I don't have money, so for me getting things for free is really important.
But if you've got money, there's no problem with buying this book.
I think it's $2 actually. That's ridiculous.
So go and buy it on Amazon. This book that he wrote, lying by Sam Harris.
And I agree with pretty much everything. Not pretty much almost everything he said.
You know, I go a little further and saying lying is never good.
But then I also would say that torture is never, never defensible.
Whereas he seems to, I think he's just intellectualizing. So to the point where he finds that torture could be useful.
Some could be beneficial and morally or ethically necessary or something.
But he's a very good writer and he wrote really well in lying.
Reality is not nearly as simple as we think. That goes without saying right.
So we think it's either, A, I heard the person, A, I tell the truth and hurt the person or B, I lie and make them happy.
It's never, it's not nearly as simple as that. What's the effect on your mind?
What's the effect on your relationship with this person when you tell them a white lie?
That your relationship is not, you don't feel totally free.
You don't feel free to be totally honest with this person.
And suppose it's your boss, right?
And they want you to somehow lie to them or lie to other people.
And so somehow you'll just say it's your boss happens to be a little bit overweight or heavier than most people.
And so your boss asks, do I look fat in this dress?
And you know if you're saying look fat that based on your past experiences with this person, they'll just fire you or they'll make your life very difficult.
It's a good chance for you to realize that you're in the wrong life.
You can't avoid these very, very difficult situations when you're surrounded by people who make things very, very difficult for you.
Why the conclusion I can only ever come to is why don't you just start dating?
Why don't you all just start dating?
Why don't you all just give it up?
What used to you find it? What good do you find it?
Yes, you have attachments. Yes, you have desires.
I have them. I'm not the perfect monk.
I'm not perfect. I didn't become a monk because I was perfect.
I'm not special. We are not special.
Why not just do it?
No, this is why I said there's two sides.
If you have the view that there's some benefit in being with such people and being in such situations, then this is...
I don't have much to say. We can never find a common ground in a certain case as I can persuade you otherwise, but I don't have that intention.
If you want to be free from this, but you feel you're so attached to the world, this isn't a problem.
This is why you become a monk. This is why you leave the world.
Maybe not become a monk. Why you go to a meditation center? Because you know I have too many defilements to live in this place.
If I continue this lifestyle, there's no way I'm going to progress in Dhamma.
There's no way I'm going to become a better person. No way I might just go and find clarity.
I can't progress here. This is why you leave and go off into first.
You know that you've got a problem. That's why you do it. That's why you leave.
This is the Buddha's teaching on gardening. You have to guard yourself.
You have to do away with these complexities of life.
This is why people become a monk. This is the best reason to become a monk.
Or whatever, find a way to live a more simple life as a layperson. It's perfectly valid.
People are able to live simpler lives than many monks. So, power to them.
