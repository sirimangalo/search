1
00:00:00,000 --> 00:00:29,840
Good evening everyone, broadcasting live, February 6th, oop that didn't even go out.

2
00:00:29,840 --> 00:00:38,440
Right, so now we are, I think it's all working.

3
00:00:38,440 --> 00:00:57,840
So we've got a Google Hangout up as well, if people want to come on and ask questions.

4
00:00:57,840 --> 00:01:06,720
If you just want to watch, you can watch from my YouTube channel, I think.

5
00:01:06,720 --> 00:01:18,720
So today's quote is about mindfulness of breathing, when the Buddha praised mindfulness

6
00:01:18,720 --> 00:01:29,560
of breathing, time and time again, it's considered to be the gold standard in meditation

7
00:01:29,560 --> 00:01:40,320
practices in Buddhism.

8
00:01:40,320 --> 00:01:44,800
And so there's a lot written about it, there's a lot of debate about it, discussion about

9
00:01:44,800 --> 00:02:00,920
it, people have their own ideas about it, lots and lots of different ideas about it, some

10
00:02:00,920 --> 00:02:10,840
of which include the fact that it has to be done at the nose, that among tell me, I explain

11
00:02:10,840 --> 00:02:16,040
to him that I do mindfulness of breathing, watching the stomach, or I do a meditation, watching

12
00:02:16,040 --> 00:02:23,560
the stomach, and he looked very serious and concerned and he said, through a translator,

13
00:02:23,560 --> 00:02:29,280
actually it was kind of interesting, he told his translator to tell him that he has to

14
00:02:29,280 --> 00:02:35,200
be careful with that because it's not the Buddha's teaching, and he had to be at the

15
00:02:35,200 --> 00:02:42,880
nose.

16
00:02:42,880 --> 00:02:48,240
In Thailand, they've got lots of interesting ideas about mindfulness of breathing.

17
00:02:48,240 --> 00:02:53,400
They mix it with mindfulness of the Buddha, usually.

18
00:02:53,400 --> 00:02:59,600
So when the breath goes in, they say, Buddha, because they say, the whole Buddha, Buddha.

19
00:02:59,600 --> 00:03:05,760
And as a result, they start to get the idea that the breath or the awareness of the breath

20
00:03:05,760 --> 00:03:08,360
is somehow the Buddha.

21
00:03:08,360 --> 00:03:13,320
In the sense of Buddha, meaning one who knows, so if you read some of these books, they

22
00:03:13,320 --> 00:03:20,880
talk about that being Buddha, that is Buddha, that knowing mind, somehow that that's equivalent

23
00:03:20,880 --> 00:03:30,680
to nibhana or something, that's ridiculous.

24
00:03:30,680 --> 00:03:34,840
But the Buddha had specific teachings on anapanasati.

25
00:03:34,840 --> 00:03:44,560
Some people say it has, it's only used for jhana.

26
00:03:44,560 --> 00:03:48,000
Other people say that you can use it for Vipasana.

27
00:03:48,000 --> 00:03:56,720
Some people say, Buddha goes to apparently send that it's, well, they kind of insinuates

28
00:03:56,720 --> 00:04:03,360
that it's only for the Buddha and highly special individuals.

29
00:04:03,360 --> 00:04:06,680
That's very difficult to practice on anapanasati.

30
00:04:06,680 --> 00:04:14,240
He may be right, it may be quite difficult to become, his argument is that it becomes

31
00:04:14,240 --> 00:04:16,440
more and more refined as you practice it.

32
00:04:16,440 --> 00:04:23,040
But he was talking about a specific type of anapanasati that's used to enter into the jhanas

33
00:04:23,040 --> 00:04:29,120
and the idea that eventually, through anapanasati, you start to see a light and that's

34
00:04:29,120 --> 00:04:35,040
the nimitta that you use to enter the jhanas, which is kind of interesting because the

35
00:04:35,040 --> 00:04:47,440
breath being, so the breath being based on ultimate reality, if you focus on the reality

36
00:04:47,440 --> 00:04:52,400
of say the stomach or the feeling here, you can't enter into jhana, obviously, because

37
00:04:52,400 --> 00:05:00,560
it's impermanent suffering in non-sal, it's not something stable that you can absorb

38
00:05:00,560 --> 00:05:02,680
in.

39
00:05:02,680 --> 00:05:08,120
So there has to be some conceptual object, like the breath or something, even that you

40
00:05:08,120 --> 00:05:11,000
would focus on.

41
00:05:11,000 --> 00:05:14,720
But the Buddha had, there's one passage that's really interesting, not this quote actually.

42
00:05:14,720 --> 00:05:16,680
There's a much better quote.

43
00:05:16,680 --> 00:05:21,600
This one is sort of practical explaining how to teach different ways, and there's lots

44
00:05:21,600 --> 00:05:26,560
of different ways you can watch, watch the breath, noticing it's long, noticing it's

45
00:05:26,560 --> 00:05:31,440
short, which is actually a part of how we, what we do when you watch the stomach, noticing

46
00:05:31,440 --> 00:05:35,920
that it's long and noticing that it's short is a part of seeing both the reality of

47
00:05:35,920 --> 00:05:41,560
the experience, being impermanent suffering in non-sal, and also the reality of your mind

48
00:05:41,560 --> 00:05:47,640
recognizing this is long, recognizing this is short because something is only long and short

49
00:05:47,640 --> 00:05:53,320
relatively speaking, so it's only the mind that decides and that, so that's an experience

50
00:05:53,320 --> 00:05:54,720
that arises in the mind.

51
00:05:54,720 --> 00:06:02,480
So seeing all of that is a part of your thoughts on the practice, but there's calming

52
00:06:02,480 --> 00:06:06,760
the mind using breath, they're seeing impermanent, choosing the breath, I need to handle

53
00:06:06,760 --> 00:06:14,800
bassy, we had a team, contemplating release or being released by focusing on the breath.

54
00:06:14,800 --> 00:06:22,000
It's not some different aspects, but the quote that I wanted to bring up was where the

55
00:06:22,000 --> 00:06:29,040
talks appraises Annapala said, so you can get an idea of what he thought of it.

56
00:06:29,040 --> 00:06:51,760
He says, so it's clearly something that calms the

57
00:06:51,760 --> 00:06:52,760
mind down.

58
00:06:52,760 --> 00:06:57,920
You know, even in Vipassana, we talk about how it's not really designed to calm you down,

59
00:06:57,920 --> 00:07:04,920
but you can definitely see that having the object of the stomach to come back to is calming,

60
00:07:04,920 --> 00:07:10,480
it's reassuring, it's a base, it was when you're dealing with the craziness of the mind

61
00:07:10,480 --> 00:07:16,040
having the stomach to go back to, having the breath to go back to, I mean, the breath

62
00:07:16,040 --> 00:07:22,000
is always there, something you can rely on.

63
00:07:22,000 --> 00:07:31,160
And it's something that can become very peaceful, it's very subtle, it's subtle, it can

64
00:07:31,160 --> 00:07:32,160
get subtle.

65
00:07:32,160 --> 00:07:39,160
I mean, that's more to do as a summit, I will focus on the subtle sensations that the

66
00:07:39,160 --> 00:07:41,160
nose or whatever.

67
00:07:41,160 --> 00:07:48,560
Assage an echo, this is an interesting one, and this is an assage an echo, it means mixed,

68
00:07:48,560 --> 00:07:56,720
so assage an echo, it means unmixed, unadulterated, so it might be translated.

69
00:07:56,720 --> 00:08:04,000
But it's interesting to use this word to rebut people, especially people, you know, meditators

70
00:08:04,000 --> 00:08:07,880
from the Thai tradition, you seem to think you can mix Buddha and mindfulness of the Buddha

71
00:08:07,880 --> 00:08:13,800
and mindfulness of the breath, I would argue that this word could be used against that

72
00:08:13,800 --> 00:08:18,720
along with the experiences that come from mixing them as well, but, you know, I shouldn't

73
00:08:18,720 --> 00:08:24,280
be too critical to each their own, but if you talk to monks in Sri Lanka and tell them

74
00:08:24,280 --> 00:08:27,240
that that's what they're doing in Thailand, they just shake their heads and where to think

75
00:08:27,240 --> 00:08:32,440
at that idea, mixing two meditations like that.

76
00:08:32,440 --> 00:08:37,800
Assage an echo means it's unmixed, so the breath is breath, it's pure, right?

77
00:08:37,800 --> 00:08:44,280
There's no baggage associated with it, I mean, for most people, and when you focus on

78
00:08:44,280 --> 00:08:53,680
it, it's such a pure object, there's no bias in the sense of being useful for some people

79
00:08:53,680 --> 00:09:00,200
and not useful for others, or being only useful for this type of person, you know, assage

80
00:09:00,200 --> 00:09:09,040
in a cup, it's unadulterated, there's no baggage associated with it, for most people.

81
00:09:09,040 --> 00:09:21,360
Assage an echo, suko tawiharo, it's a dwelling in happiness, so again, much more to do

82
00:09:21,360 --> 00:09:27,200
with the tranquility side of mindfulness of breath, where you're focusing on the concept

83
00:09:27,200 --> 00:09:33,400
of the breath, going in and going out, and becomes very calming.

84
00:09:33,400 --> 00:09:38,760
But no matter what, whether it's Vipassan or our summit, focusing on the breath is both peaceful

85
00:09:38,760 --> 00:09:42,680
and pleasant, happy.

86
00:09:42,680 --> 00:09:47,440
Now in Vipassan, another problem comes that you eventually have to deal with when it's unpleasant,

87
00:09:47,440 --> 00:09:52,880
where you have to deal with the tension in the stomach, where you have to deal with the fact

88
00:09:52,880 --> 00:09:57,760
that it's not under your control, it can be quite uncomfortable.

89
00:09:57,760 --> 00:10:03,360
But by focusing on it, by using it, by being objective, which it helps you to become objective

90
00:10:03,360 --> 00:10:16,160
because it's such an objective object, happiness comes regardless of the need discomfort.

91
00:10:16,160 --> 00:10:21,200
So it's important to separate happiness and comfort, happiness and pleasure.

92
00:10:21,200 --> 00:10:30,600
suko tawiharo doesn't have to mean it's pleasant, but it's peaceful, and you want to

93
00:10:30,600 --> 00:10:38,160
say, it's almost pleasant, but it's pleasant through unpleasantness.

94
00:10:38,160 --> 00:10:47,680
It's important to understand because Vipassan or meditation can be quite unpleasant.

95
00:10:47,680 --> 00:10:53,240
So think they're doing it wrong, or that the meditation is wrong, because they see impermanence,

96
00:10:53,240 --> 00:10:55,440
think changing chaotic.

97
00:10:55,440 --> 00:11:03,880
They see suffering, it's unpleasant, it's unsatisfying, it's unamenable to your wishes.

98
00:11:03,880 --> 00:11:14,600
It's unsatisfying, I guess, the lack of a better word.

99
00:11:14,600 --> 00:11:19,520
And it's uncontrollable, so you can't force the breath and you find yourself for trying

100
00:11:19,520 --> 00:11:23,600
to force it and suffering more as Vipassan.

101
00:11:23,600 --> 00:11:32,920
That's why you see that you might get discouraged and think this isn't happy, but the

102
00:11:32,920 --> 00:11:40,080
such happiness that comes when you learn to be objective, to bring this happiness.

103
00:11:40,080 --> 00:11:44,000
I think mostly this is referring to how calming it is in the beginning.

104
00:11:44,000 --> 00:11:53,880
We focus on the concept of the breath, it can be quite calming and pleasant.

105
00:11:53,880 --> 00:11:58,440
But the most important aspect is the final characteristics.

106
00:11:58,440 --> 00:12:06,280
We've got sukha, san'tho, jayva, pani doja, sukho javi, a sage in a kocha, sukho javi

107
00:12:06,280 --> 00:12:16,760
jayva, I think there's another one that I'm forgetting, but the final one is ukpa nukpani,

108
00:12:16,760 --> 00:12:28,760
a ku-suk, papa gai ku-sele, dhamme, whatever evil dhamme is, papa gai ku-sele, unhosem

109
00:12:28,760 --> 00:12:41,720
evil dhammas, have arisen, under adhapi, the ukpa-sele, it causes mindfulness of breathing

110
00:12:41,720 --> 00:12:55,080
destroys them basically, causes them to obey, causes them to cease, to be tranquilized, neutralized.

111
00:12:55,080 --> 00:13:01,400
It really forces you, this is much more on the vipasa of the side, it really forces

112
00:13:01,400 --> 00:13:07,040
you to give up, because you can't control it, you can't control the breath, it forces

113
00:13:07,040 --> 00:13:15,680
you to let go, and it helps you to let go, because it gives you something pure, there's

114
00:13:15,680 --> 00:13:19,640
no greed attached to it, there's no anger attached to it, it gives you something pure

115
00:13:19,640 --> 00:13:30,040
to focus on, and that all your anger and all your greed just pour out, like there's

116
00:13:30,040 --> 00:13:40,320
just like water washing it off, I think the Buddha likens it to rain, actually, just like

117
00:13:40,320 --> 00:13:48,920
in the hot season, when it rains, it's cool and refreshing, and it also washes away

118
00:13:48,920 --> 00:14:05,680
and all your defilement, and it is not to be mismanhat to be underestimated, because

119
00:14:05,680 --> 00:14:11,320
you can, you can, you can, you think that it was rising from us, it's just a simple exercise,

120
00:14:11,320 --> 00:14:15,360
you can become enlightened, just watching your stomach rise and fall, you see all three

121
00:14:15,360 --> 00:14:26,680
characteristics very clearly, it's impermanent suffering and non-self, it's all right there,

122
00:14:26,680 --> 00:14:34,000
and we're forced to let go of defilements, you know, any defilement you have for anything

123
00:14:34,000 --> 00:14:40,600
in the world, you will show up, it's just though you have a, it's a cloth, and you're wiping

124
00:14:40,600 --> 00:14:48,040
your hands and all the stains, show up on the cloth, and it wipes away all the stains.

125
00:14:48,040 --> 00:15:02,280
So, anyway, some little speech in praise of Anapana Sati, which is very, it's very possible

126
00:15:02,280 --> 00:15:09,640
to, very reasonable, just to suggest that what we practice is a form of Anapana Sati, it's

127
00:15:09,640 --> 00:15:15,920
also, you can also describe it as mindfulness of the aggregate, so the elements, because

128
00:15:15,920 --> 00:15:18,360
it's that as well.

129
00:15:18,360 --> 00:15:32,840
Okay, so do we, I've got, I've got a whole posse here, hey guys, some people joined the

130
00:15:32,840 --> 00:15:45,520
hangout, when you guys have mics, I can hear you, Larry, hey, this is my first visit,

131
00:15:45,520 --> 00:15:51,400
oh, welcome, thank you, we just, I've just started trying to get people to join the hangout,

132
00:15:51,400 --> 00:15:59,040
so I figure if people really want to ask, if they're willing to come on here, it means

133
00:15:59,040 --> 00:16:05,440
they really need to ask their question badly, so we'll, we'll call or we'll separate

134
00:16:05,440 --> 00:16:11,840
the wheat from the chap this way, if you're just idly typing it in, it's a sign that

135
00:16:11,840 --> 00:16:16,240
you don't really need an end, I mean, the problem is lots of questions and repeat questions

136
00:16:16,240 --> 00:16:23,520
more, so, and this is more real, you know, right, this is actually, we're now a community

137
00:16:23,520 --> 00:16:29,240
of four of us, we're a sangha, you can say, and I've got two guys sitting here in my room

138
00:16:29,240 --> 00:16:37,320
listening, so, so, anybody got any questions, or are you just here to say hi?

139
00:16:37,320 --> 00:16:45,840
I'm just here to say hi, and kind of get acquainted with the process, I guess I was thinking

140
00:16:45,840 --> 00:16:52,520
that this might be just a domicalk, not our question and answer opportunities, so I'm not

141
00:16:52,520 --> 00:16:53,520
prepared.

142
00:16:53,520 --> 00:16:58,960
That's fine, yeah, I mean, I haven't announced that or anything, I, you know, I just gave

143
00:16:58,960 --> 00:17:06,640
like 10 minutes, so that's our short domicalk for the day, and we can stop there, but,

144
00:17:06,640 --> 00:17:11,360
you know, we can talk for a few minutes anyway, you guys alive on the internet in case

145
00:17:11,360 --> 00:17:22,440
you didn't realize it, like that 23 viewers, I have a question back, go for it.

146
00:17:22,440 --> 00:17:31,600
Is it, is it important not to move while you're meditating with a stage of meditation?

147
00:17:31,600 --> 00:17:35,760
It's important to learn while you're meditating, it's important that we understand what

148
00:17:35,760 --> 00:17:43,080
we're doing, because we're not trying to get into some ecstatic state or trans state,

149
00:17:43,080 --> 00:17:51,520
and we're passing over trying to learn about our minds and about our bodies, so there's

150
00:17:51,520 --> 00:17:56,880
not so many shoulds and shouldn'ts, musts, and mustn'ts, you just have to ask yourself

151
00:17:56,880 --> 00:18:02,080
logically, you know, what happens when I, for example, when I move.

152
00:18:02,080 --> 00:18:12,280
So if you're moving without being mindful, then it's a moment of delusion, or it's a moment

153
00:18:12,280 --> 00:18:23,360
of, you know, following your habits, moreover, the question is, why are you moving?

154
00:18:23,360 --> 00:18:32,240
So the cause of your moving is potentially a problem, so we move because we're uncomfortable.

155
00:18:32,240 --> 00:18:37,240
Uncomfortable means disliking, disliking means anger, anger is one of the defoundments,

156
00:18:37,240 --> 00:18:43,920
so acting on it is problematic, it's cultivating the habit of aversion, something bad

157
00:18:43,920 --> 00:18:48,520
happens, you find a way to escape it, that's a big reason why we move.

158
00:18:48,520 --> 00:18:56,360
Another reason why we move is we get tired and maybe lazy, and so we start to slouch.

159
00:18:56,360 --> 00:19:01,240
And so again, you might want to address that if you're tired or so, to say to yourself

160
00:19:01,240 --> 00:19:12,680
tired, tired, I do follow, but that being said, you can be that the pain is just too intense

161
00:19:12,680 --> 00:19:16,160
and you have no choice, you're going to move.

162
00:19:16,160 --> 00:19:21,880
So I guess the two parts, you note whatever it is that making you want to lose, angry,

163
00:19:21,880 --> 00:19:29,760
and I'm sorry, disliking, disliking or tired, but when you do move, that's not, the answer

164
00:19:29,760 --> 00:19:34,680
to the short answer to the question, no, it's not wrong, but we don't really have wrong

165
00:19:34,680 --> 00:19:44,400
and right, it's best if you can be free from the need to move, if you can free yourself

166
00:19:44,400 --> 00:19:52,800
from the aversion to the pain, but there's no intrinsic reason why moving should be wrong

167
00:19:52,800 --> 00:19:57,480
because you can be mindful of it, so absolutely when you need to move, when you feel like

168
00:19:57,480 --> 00:20:06,520
okay enough, I have to back off, it's just too intense, absolutely, the technique would

169
00:20:06,520 --> 00:20:12,120
be to say to yourself, wanting to move, wanting to move is a couple of times, noting

170
00:20:12,120 --> 00:20:17,000
the intention, sometimes you do that and then you don't have to move, but probably it's

171
00:20:17,000 --> 00:20:24,040
the one move, lift your hand up if you see moving your foot, if lifting, if moving, grab

172
00:20:24,040 --> 00:20:33,680
a leg grabbing, or holding, touching, then lifting, moving, moving, moving, releasing,

173
00:20:33,680 --> 00:20:53,360
just being mindful, that's helpful, who's this guy in the middle, nine, eight, eight,

174
00:20:53,360 --> 00:21:01,720
and what does 90s say, can you hear me, I can hear you, oh, yeah, it just takes

175
00:21:01,720 --> 00:21:10,080
for you, so I'm Simon, oh, hi Simon, hey nice to meet you, but you don't know what

176
00:21:10,080 --> 00:21:17,400
can I guess, no, I'm sorry, I just got the mic for now, did you record today's second

177
00:21:17,400 --> 00:21:23,800
life talk, yep, we recorded it at Hollow Hill, because I kind of messed up the time, so

178
00:21:23,800 --> 00:21:31,320
I wasn't there for the last session, so we just listened when actually just 10 minutes

179
00:21:31,320 --> 00:21:40,560
after I finished there, I just messed up the time, so you didn't record the video, yeah,

180
00:21:40,560 --> 00:21:46,280
I made a video, but it was not recorded at the Deer Park, and it was recorded at another

181
00:21:46,280 --> 00:21:55,080
place where we just sat down and did meditation and listened to, okay, at our place, actually,

182
00:21:55,080 --> 00:22:02,400
I don't know if you've been there, no, okay, now I haven't been on second life much, I

183
00:22:02,400 --> 00:22:08,920
was thinking, you all have places on second life, so there's no point, but at one point

184
00:22:08,920 --> 00:22:14,160
we had set up an open sim, if you know, open sim, it's that you can set up your own

185
00:22:14,160 --> 00:22:23,560
second life for free, just in the server, yeah, so that's also an open and public space as

186
00:22:23,560 --> 00:22:30,000
well, where we were today, full of, but like with open sim, you have it on your own server,

187
00:22:30,000 --> 00:22:34,320
you can have up to, you guys can go ahead, you guys can go meditate, don't need to listen

188
00:22:34,320 --> 00:22:44,120
to this, this is more general stuff, okay, then you can have as many people on as what I

189
00:22:44,120 --> 00:22:50,600
once as you like, you can have as many shapes and stuff as you like, it's all free,

190
00:22:50,600 --> 00:22:57,800
it tends to be less laggy depending on your server, then second life, second life is,

191
00:22:57,800 --> 00:23:05,640
or used to be second life used to be pretty problematic, you can do a lot more on it,

192
00:23:05,640 --> 00:23:10,440
and the thing is it would be just our group, you wouldn't have, I mean that's good and

193
00:23:10,440 --> 00:23:17,080
bad, but a good side is it would just be for this group, the way to get everybody kind

194
00:23:17,080 --> 00:23:25,720
of in the same room together, sitting down, you know, it's it's fake, but virtual, there's

195
00:23:25,720 --> 00:23:30,920
potential for good there, I think it was very interesting listening to the talk today, definitely.

196
00:23:30,920 --> 00:23:45,480
I've got a question, second life, I'm not clear, I've never participated in any of those

197
00:23:45,480 --> 00:23:52,760
kinds of applications or activities, so I'm really ignorant of what it's all about,

198
00:23:53,480 --> 00:23:58,680
I'm getting the impression, but I am curious, I'm getting the impression that perhaps it's

199
00:23:58,680 --> 00:24:11,240
kind of a way to give the virtual Sanga opportunity to have some more interface or activity

200
00:24:11,240 --> 00:24:19,800
outside of the meditation, your mind alone side. Yeah, I mean it's a play, what people will say,

201
00:24:19,800 --> 00:24:23,880
good about it is it's a chance to meet other people, meet other Buddhists, talk with them,

202
00:24:23,880 --> 00:24:29,960
meditate with them, encourage each other, people really like it, it's kind of like a game,

203
00:24:29,960 --> 00:24:35,960
I think, kind of, you know, it's got it's good and it's bad, the bad is it's kind of like a game.

204
00:24:43,640 --> 00:24:50,280
Yeah, I'm, it's been good the past couple of weeks, I've had really good audiences,

205
00:24:50,280 --> 00:25:06,200
20 seconds. Is there, does it encompass Dharma talks teaching, or is it kind of a peer-to-peer sharing

206
00:25:06,840 --> 00:25:16,200
kind of a thing? It's virtual reality, so you actually see cartoon characters of each other,

207
00:25:16,200 --> 00:25:21,320
sitting in a group, sitting in a deer park. If you look on YouTube, you can see I've recorded,

208
00:25:22,120 --> 00:25:30,360
Google me, you to demo second life, you'll probably find my videos that I get in second life,

209
00:25:30,360 --> 00:25:40,760
lots of good talks back in the day. They're very good. Okay, thanks. I guess I shouldn't be saying

210
00:25:40,760 --> 00:25:47,320
they're good, but I think they were in, I was really keen on me at the time and preparing for

211
00:25:47,320 --> 00:25:55,960
them instead. I listened to those problem way more than anything else, and I listened to them

212
00:25:55,960 --> 00:26:07,080
repeatedly. Well, never really good talks. That's good to hear. I have another question.

213
00:26:07,080 --> 00:26:21,480
I have registered for a teacher meet at this week, early one morning. I think perhaps Friday,

214
00:26:21,480 --> 00:26:30,120
there was an available slide, and this will be my first occasion, so it is pretty much

215
00:26:30,120 --> 00:26:37,800
your bird's audio visual hanging out right now. Very much like what you're doing right now.

216
00:26:38,760 --> 00:26:46,200
You have to call me as the only difference, so you can do that using my Gmail address,

217
00:26:46,200 --> 00:26:51,720
you can also, on the meditation side, on the page where you signed up for it,

218
00:26:52,360 --> 00:26:57,080
if you're there at the right time, a little button will show up and do it for you. But if you

219
00:26:57,080 --> 00:27:02,680
can't do it that way, you can go to hangouts.google.com, and if there's a link to that as well,

220
00:27:03,560 --> 00:27:10,680
I think on the page, right? Yeah, there you can just links to Google Hangout.

221
00:27:11,560 --> 00:27:18,520
Go there and find me. You can't use in my email address. You have to download Gmail.

222
00:27:18,520 --> 00:27:33,320
Right, and I presume that it's particularly an opportunity. I've been meditating

223
00:27:34,360 --> 00:27:43,320
on my own down here in South Mississippi, pretty well isolated from Buddhism for about three years,

224
00:27:43,320 --> 00:27:53,960
and basses reading and YouTube videos watched a lot of your videos, a number of authors,

225
00:27:54,840 --> 00:28:04,360
and just kind of groping my way through the process. This would be my first opportunity to

226
00:28:04,360 --> 00:28:14,360
actually tell somebody who understands what I'm up to and ask, how can I do it better? What should I do

227
00:28:14,360 --> 00:28:24,200
differently? I'm open to criticism, etc. Well, what a lot of people don't realize who got involved

228
00:28:24,200 --> 00:28:29,320
to read my booklet or whatever is that there's more than just the first step. Like what you

229
00:28:29,320 --> 00:28:35,560
will read in my booklet or in my videos is the first day of a meditation course. It gets a lot

230
00:28:35,560 --> 00:28:43,000
more involved in that, and there's a lot more exercises to give you, but I can't really do that

231
00:28:43,000 --> 00:28:50,280
just by putting them out on the internet because people are potential to go too far too fast on

232
00:28:50,280 --> 00:28:56,280
your own that kind of thing. You're probably not, but we don't really do it. It's just falling

233
00:28:56,280 --> 00:29:01,640
in tradition. You have to get the exercises for me, so that's what these before. You have to

234
00:29:01,640 --> 00:29:08,120
meditate at least an hour a day to be considered for it. Otherwise, I won't talk to you. I'll

235
00:29:08,120 --> 00:29:18,360
say, go back next week. And then I'll give you, you know, I'd talk to you and then I give you

236
00:29:18,360 --> 00:29:23,400
a new exercise. If you have questions, you ask them if I have questions, I'll ask them just to

237
00:29:23,400 --> 00:29:31,720
make sure you're on track, give you the new exercise. So we go through what would be the equivalent

238
00:29:31,720 --> 00:29:40,120
of like 90% of a foundation course. If you were coming to stay here and doing like many hours a

239
00:29:40,120 --> 00:29:52,040
day. And so you'll do like one hour a day to start with the expectation of the expectation

240
00:29:52,040 --> 00:29:58,360
that you're eventually getting up to two hours a day. That's what you do preparing for. Start

241
00:29:58,360 --> 00:30:03,480
at one hour, get up to two hours. That's just made that up up there, but that seems reasonable.

242
00:30:03,480 --> 00:30:08,840
And then we meet once a week as you can see and you do in one week what would be doing in the day.

243
00:30:10,520 --> 00:30:19,160
As a result, it takes about 15, 18 weeks to finish that course. And then to actually finish the

244
00:30:19,160 --> 00:30:22,840
foundation course, you'd still have to come here. And like right now, there's one man who did that

245
00:30:22,840 --> 00:30:31,400
face online course, Patrick. And now he's come here for 10 days. The foundation course.

246
00:30:33,480 --> 00:30:40,520
Okay, so it is, I wasn't clear on that. So I appreciate it. There's the expectation that I would

247
00:30:40,520 --> 00:30:50,520
make arrangements for a weekly calling in. Yes. Good. Okay, good deal. Yeah, once you've got

248
00:30:50,520 --> 00:30:55,640
this slide, it's yours every week until we finish the process or until you decide you don't want to

249
00:30:55,640 --> 00:31:07,880
do it anymore, then take yourself off list. Okay. Now my life schedule can be, can vary a little bit

250
00:31:07,880 --> 00:31:17,080
with sometimes having grandbaby responsibilities, things like that. So I didn't realize that I was

251
00:31:18,200 --> 00:31:23,960
needing to commit to that particular time slot each week. Is that comfortable?

252
00:31:23,960 --> 00:31:29,560
Yeah, you can just say like, I mean, you still want to give yourself a week of practice in between

253
00:31:29,560 --> 00:31:35,320
meetings, but yeah, sure. As long as there's a slot open, just take a new slot.

254
00:31:35,320 --> 00:31:41,480
Okay. All right. Again, we can get from there's no slot. Just send me an email and we can work something

255
00:31:41,480 --> 00:31:49,560
up. Okay. Potentially. Okay. Super. Thanks for all that information. Great. Appreciate that.

256
00:31:52,280 --> 00:31:58,120
Anybody else? Otherwise, I'm going to head off. Good night.

257
00:31:58,120 --> 00:32:08,120
Good night, everyone. Good night. Thanks for tuning in. Thank you. Good night.

