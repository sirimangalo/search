1
00:00:00,000 --> 00:00:05,960
How should one deal with with things he hasn't experienced yet? For example,

2
00:00:05,960 --> 00:00:11,400
rebas, heaven and hell, realms, sex, powers.

3
00:00:11,400 --> 00:00:21,000
What do you think? I think we should then care about them.

4
00:00:21,000 --> 00:00:23,000
I think I'm with you.

5
00:00:23,000 --> 00:00:30,000
We should only deal with what you can experience.

6
00:00:30,000 --> 00:00:33,000
I think that you have already experienced rebirth.

7
00:00:33,000 --> 00:00:38,000
You just don't remember.

8
00:00:38,000 --> 00:00:41,000
Yeah, I mean, there's something to edit it.

9
00:00:41,000 --> 00:00:44,000
How do you know you've experienced them if you haven't?

10
00:00:44,000 --> 00:00:47,000
You don't remember.

11
00:00:47,000 --> 00:00:54,000
Well, where did you come from? There's no beginning to this cycle of life.

12
00:00:54,000 --> 00:00:57,000
There's no beginning, but there is an end.

13
00:00:57,000 --> 00:01:04,000
And we're clearly not at the end of where I still experience it.

14
00:01:04,000 --> 00:01:07,000
You just said that there is an end?

15
00:01:07,000 --> 00:01:12,000
Yes, there is.

16
00:01:12,000 --> 00:01:16,000
I don't understand how you can know that there is no beginning, but there is an end.

17
00:01:16,000 --> 00:01:21,000
If you have faith, I guess.

18
00:01:21,000 --> 00:01:24,000
I mean, I haven't experienced it personally.

19
00:01:24,000 --> 00:01:26,000
But that's the question.

20
00:01:26,000 --> 00:01:38,000
If you already have faith, then you need to experience something to think about it or to believe in it.

21
00:01:38,000 --> 00:01:46,000
I mean, when I feel like on the path, you start to see the results come pretty much.

22
00:01:46,000 --> 00:01:49,000
Be it on more and more evident.

23
00:01:49,000 --> 00:02:00,000
And you start to see the first three, the first second, and then eventually you'll see the third.

24
00:02:00,000 --> 00:02:04,000
So what you mean by the end, you're talking about Nimana that's just to clarify.

25
00:02:04,000 --> 00:02:09,000
It's really an end of the universe, because the universe keeps going on, but the end for that.

26
00:02:09,000 --> 00:02:10,000
Oh, not a person.

27
00:02:10,000 --> 00:02:12,000
Is that what you're talking about?

28
00:02:12,000 --> 00:02:14,000
The cycle of rebirth.

29
00:02:14,000 --> 00:02:15,000
Right.

30
00:02:15,000 --> 00:02:17,000
The ending to the cycle of rebirth.

31
00:02:17,000 --> 00:02:18,000
Yeah.

32
00:02:18,000 --> 00:02:19,000
Before one person.

33
00:02:19,000 --> 00:02:20,000
Before one person.

34
00:02:20,000 --> 00:02:21,000
Beginning.

35
00:02:21,000 --> 00:02:22,000
There's no beginning.

36
00:02:22,000 --> 00:02:23,000
Well, there's a potential end.

37
00:02:23,000 --> 00:02:33,000
I think the curious thing, if I'm not mistaken, is that there's no guarantee that every being is going to reach Nimana.

38
00:02:33,000 --> 00:02:39,000
There's an idea that perhaps beings are also infinite, which is interesting.

39
00:02:39,000 --> 00:02:43,000
That's all kind of mind-blowing and gets us very much off the topic.

40
00:02:43,000 --> 00:02:50,000
So you can see how ignoring all this stuff can actually be useful.

41
00:02:50,000 --> 00:02:53,000
What I don't like is when people get bigoted about it.

42
00:02:53,000 --> 00:03:01,000
It seems to me, as soon as anyone brings up rebirth, it just gets, oh, that's not Buddhism and so on.

43
00:03:01,000 --> 00:03:06,000
Or when people talk about magic powers and say that's not Buddhism.

44
00:03:06,000 --> 00:03:09,000
Well, maybe magical powers you could have a point.

45
00:03:09,000 --> 00:03:16,000
But the Buddha, why then does the Buddha seem to, according to our scriptures again and again and again, talk about these things?

46
00:03:16,000 --> 00:03:26,000
When the Buddha outlines the entire path, both remembering your past lives and gaining magical powers are a part of that.

47
00:03:26,000 --> 00:03:31,000
Or every time a part of that path, every time the Buddha talks about it.

48
00:03:31,000 --> 00:03:35,000
When he condenses it, you can see that they disappear.

49
00:03:35,000 --> 00:03:42,000
But when he talks about the full and entire path, they seem to be very much a part of...

50
00:03:42,000 --> 00:03:46,000
You can't say they're not a part of the Buddha's teaching because he teaches them.

51
00:03:46,000 --> 00:03:53,000
He obviously doesn't say they're the end and he says they're not the end, but he teaches them.

52
00:03:53,000 --> 00:03:59,000
So, we always try to make the point that you don't need these things and you don't have to think about them.

53
00:03:59,000 --> 00:04:04,000
And if they're causing your problems, well, then you're focusing on, you're barking up the wrong tree.

54
00:04:04,000 --> 00:04:14,000
But they do have a place in Buddhism. They're quite interesting and it might even be something to talk about.

55
00:04:14,000 --> 00:04:19,000
The rebirth can be quite useful to help you to gain a more...

56
00:04:19,000 --> 00:04:23,000
an understanding of reality that's more in line with reality.

57
00:04:23,000 --> 00:04:37,000
I'm supposed to the Western mindset of physical death equals death, which is totally out of line with experience and experience of reality.

58
00:04:41,000 --> 00:04:44,000
Are we okay with that?

59
00:04:44,000 --> 00:04:49,000
Yeah, I have nothing else to say.

60
00:04:49,000 --> 00:05:14,000
Let's quit.

