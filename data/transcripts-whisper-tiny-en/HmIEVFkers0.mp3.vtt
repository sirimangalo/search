WEBVTT

00:00.000 --> 00:02.400
Hi, welcome back to Ask a Monk.

00:02.400 --> 00:08.360
Today's question comes from Michael Art, who asks, how quickly should I switch between

00:08.360 --> 00:14.880
the different things, feelings, breathing, and so on that calls for my attention when meditating?

00:14.880 --> 00:20.440
In your how to meditate videos, you're doing it quite slowly, but is this only for demonstration

00:20.440 --> 00:21.440
purposes?

00:21.440 --> 00:27.800
No, I don't think it is just for demonstration purposes.

00:27.800 --> 00:34.320
It's not important how many times you say the word or that you catch everything.

00:34.320 --> 00:37.840
It's not important that everything that comes up, you have a label for it.

00:37.840 --> 00:41.960
What's important is that your mind is in this place, your mind is in this zone, your

00:41.960 --> 00:47.720
mind is clearly aware of things, and this word that we use is just to pull the mind

00:47.720 --> 00:56.840
back to that reality, to fix the mind, to straighten the mind, and keep the mind straight

00:56.840 --> 00:59.360
and aware of things as they are.

00:59.360 --> 01:03.880
You only need to do acknowledge maybe one time every second, and it's not a rhythmic thing,

01:03.880 --> 01:08.520
it's kind of just comfortable at a comfortable pace.

01:08.520 --> 01:15.560
So when you feel pain, just say to yourself, pain, pain, and it should be comfortable, there

01:15.560 --> 01:19.640
should be no feeling that you're saying it quickly, or you're saying it slowly, it should

01:19.640 --> 01:21.640
be just saying it.

01:21.640 --> 01:24.640
If you get the idea that you're saying it quickly, then yeah, probably it's too quick.

01:24.640 --> 01:27.640
If you get the idea that you're saying it slowly, probably it's too slow.

01:27.640 --> 01:32.320
It should be so natural that you don't even notice the speed, that there's no sense of

01:32.320 --> 01:34.240
slower or fast.

01:34.240 --> 01:41.080
It should be neutral, it should be just saying it when you feel, when you're thinking,

01:41.080 --> 01:47.000
thinking, thinking, thinking, just as you would normally speak, when you speak to someone,

01:47.000 --> 01:52.000
you speak at a certain rhythm, the acknowledgement should be done in that manner.

01:52.000 --> 02:00.080
As far as switching from object to object, this is also not productive in the meditation

02:00.080 --> 02:04.000
practice, it's not productive to try to go, okay, now I'm hearing, okay, now I'm seeing,

02:04.000 --> 02:10.640
okay, now it's on, pick one thing and focus on it, look at it clearly and just use it as

02:10.640 --> 02:14.880
a tool to train your mind to see things for what they are.

02:14.880 --> 02:17.720
It doesn't matter which thing you use, whatever's there in front of you, whatever's

02:17.720 --> 02:21.880
clearest, whatever your mind takes as the object.

02:21.880 --> 02:25.880
So suppose now I'm listening to the sounds outside, then I just focus on the sounds,

02:25.880 --> 02:28.320
I don't have to pay attention to everything else.

02:28.320 --> 02:32.840
When you're walking with your foot, you also feel your knee moving, your leg straightening,

02:32.840 --> 02:36.600
the tension and the movement in the back, the shifting and so on, but you don't have to

02:36.600 --> 02:41.280
acknowledge all of those things, the reason that you are aware of all of those is because

02:41.280 --> 02:46.760
you're saying step being right or lifting your foot, lifting, placing or so on, because

02:46.760 --> 02:51.040
you're focusing on the foot, you then are aware of everything as it arises quite clearly

02:51.040 --> 03:00.520
and you don't have to catch everything as long as you're keeping your mind in this mode,

03:00.520 --> 03:05.120
bringing your mind back again and again, until finally the mind starts to look at things

03:05.120 --> 03:08.920
naturally in this way, because that's what's going to happen.

03:08.920 --> 03:12.800
Once you develop this to a great extent, you'll find that it just comes naturally, you're

03:12.800 --> 03:13.800
just aware of things.

03:13.800 --> 03:18.120
When you move moving, when you brush your teeth, brushing, when you eat your foot chewing,

03:18.120 --> 03:25.400
the scooping, chewing, swallowing and so on, and you don't need to use the words.

03:25.400 --> 03:29.280
The word is the training that we do in meditation and if you keep training yourself and

03:29.280 --> 03:34.400
training, you'll get to such a high level that your mind just lets go and you're able

03:34.400 --> 03:40.200
to see things clearly and it's such a letting extreme letting go that the mind actually

03:40.200 --> 03:46.760
leaves behind the experiential world and enters into a state of Nubana through this practice.

03:46.760 --> 03:54.880
So the state that we're aiming for is not this constant labeling, but the labeling is what

03:54.880 --> 04:01.600
increases our awareness and if we can use the labeling again and again and again and clearing

04:01.600 --> 04:07.200
our mind to a higher and higher degree, we can eventually be able to let go of everything

04:07.200 --> 04:12.960
and so that our mind is able to experience true freedom.

04:12.960 --> 04:18.840
So not quickly, just try to do it naturally, that's the short answer.

04:18.840 --> 04:35.640
Thanks for the question, keep practicing.

