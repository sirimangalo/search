1
00:00:00,000 --> 00:00:08,000
In intellectual terms, I can not grasp the implementation of Buddhist philosophy to motivation.

2
00:00:08,000 --> 00:00:14,000
What is wrong with the claim that successful practice would ultimately lead to being a vegetable?

3
00:00:14,000 --> 00:00:17,000
Who if you're a vegetable?

4
00:00:17,000 --> 00:00:21,000
It depends what type.

5
00:00:21,000 --> 00:00:26,000
Okay.

6
00:00:26,000 --> 00:00:36,000
It means that ultimately lead you to just sit in a puddle of your own urine and feces and die.

7
00:00:36,000 --> 00:00:39,000
Well, the Buddha almost did just that, didn't he?

8
00:00:39,000 --> 00:00:44,000
Not the urine and feces work.

9
00:00:44,000 --> 00:00:47,000
The Buddha was ready to just pass away.

10
00:00:47,000 --> 00:00:55,000
I mean, the first point I think is that not everything comes from a karmic position.

11
00:00:55,000 --> 00:01:03,000
When you let go, you don't stop acting like a human being, you are a human being.

12
00:01:03,000 --> 00:01:06,000
I always like to refer to quantum physics because it's just so Western.

13
00:01:06,000 --> 00:01:11,000
A lot of these questions are just so Western.

14
00:01:11,000 --> 00:01:17,000
Maybe even not positive. Let's just think of a theory of reality where it's mostly physical.

15
00:01:17,000 --> 00:01:25,000
And where the mind has a very limited potential to interact with the reality.

16
00:01:25,000 --> 00:01:32,000
It does have an ability to interact and change, but not ultimate free will in the sense of saying,

17
00:01:32,000 --> 00:01:42,000
cool, if you're a vegetable or so.

18
00:01:42,000 --> 00:01:51,000
And so even when the mind stops actively changing, even if the mind were to stop actively changing the physical reality,

19
00:01:51,000 --> 00:01:53,000
much of it would still continue.

20
00:01:53,000 --> 00:01:59,000
So there would still be lots of walking and lots of even talking, for example.

21
00:01:59,000 --> 00:02:10,000
There would still be the ordinary activities that were habitual and appropriate at the time to see the thing.

22
00:02:10,000 --> 00:02:16,000
And the light and being doesn't stop acting, doesn't stop thinking, doesn't even necessarily stop acting mentally.

23
00:02:16,000 --> 00:02:28,000
They don't stop acting mentally in terms of changing the physical reality, but they stop trying to or they stop.

24
00:02:28,000 --> 00:02:37,000
They stop forcing it and they stop creating volitional tendencies, like desires.

25
00:02:37,000 --> 00:02:43,000
They can still have intention, intention to do this, the intention to do that.

26
00:02:43,000 --> 00:02:48,000
Because that's all, you could say habitual, but I think better is it's appropriate.

27
00:02:48,000 --> 00:02:53,000
And in light and being is someone who does things that are appropriate.

28
00:02:53,000 --> 00:03:00,000
The key here, I think, is you're in intellectual terms, because you're thinking in intellectual terms, and it's not intellectual.

29
00:03:00,000 --> 00:03:01,000
It's real.

30
00:03:01,000 --> 00:03:02,000
It just happens.

31
00:03:02,000 --> 00:03:04,000
You let go and you're free.

32
00:03:04,000 --> 00:03:16,000
When you use terms like motivation and vegetable, Buddhist philosophy, you're creating motivation.

33
00:03:16,000 --> 00:03:23,000
You're creating this idea of being motivated and creates dichotomies or dichotomies.

34
00:03:23,000 --> 00:03:29,000
It sets you up for either this or that, either motivated or not, either successful or not.

35
00:03:29,000 --> 00:03:30,000
Which is really not the case.

36
00:03:30,000 --> 00:03:34,000
A person who sees clearly, let's go and go, they're free.

37
00:03:34,000 --> 00:03:37,000
That has nothing to do with whether or not they do this or do that.

38
00:03:37,000 --> 00:03:44,000
And in fact, they can be highly motivated people to an extent or something akin to motivation.

39
00:03:44,000 --> 00:03:49,000
I would say dedicated and energetic people.

40
00:03:49,000 --> 00:03:58,000
Because they have these qualities in their mind, not because they want them to be there or because they need them or they actively seek them out.

41
00:03:58,000 --> 00:04:01,000
But because it's appropriate, it's proper.

42
00:04:01,000 --> 00:04:03,000
It's natural.

43
00:04:03,000 --> 00:04:06,000
Really, Buddhism is about coming back to nature.

44
00:04:06,000 --> 00:04:16,000
I can say it's the most natural human being or being.

