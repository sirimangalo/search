1
00:00:00,000 --> 00:00:13,320
Okay good evening everyone, welcome to our evening dhamma, tonight we're looking at

2
00:00:13,320 --> 00:00:41,360
right effort, so the Buddha was quite adamant about the importance of effort, he said

3
00:00:41,360 --> 00:00:51,840
effort, it is by effort, well that one over comes the free, we need a nandu kamun jidh,

4
00:00:51,840 --> 00:00:58,600
he said at one point I think something like tell me monks what cannot be achieved by striving,

5
00:00:58,600 --> 00:01:07,280
first the Buddha had such a memory of the things he had accomplished by striving and the realization

6
00:01:07,280 --> 00:01:17,800
that there's very little that can't be accomplished if you work at it, it's an interesting

7
00:01:17,800 --> 00:01:31,200
aspect of the world view of rebirth that takes away the limits on what's possible

8
00:01:31,200 --> 00:01:41,520
for being to accomplish right, because a human being can only accomplish so much, no human

9
00:01:41,520 --> 00:01:55,200
being is ever going to travel to the edge of the universe, not physically, well and not

10
00:01:55,200 --> 00:02:04,520
today anyway, it's not going to happen the way things are, but the potential for it is

11
00:02:04,520 --> 00:02:15,920
always there, far beyond what we normally think possible, of course, because based on our state

12
00:02:15,920 --> 00:02:27,000
of mind, our next existence, our state of being might be quite different and we talk about

13
00:02:27,000 --> 00:02:36,000
beings like angels and gods, the potential for that sort of an existence, it changes

14
00:02:36,000 --> 00:02:47,400
everything really as far as what's possible, so the Buddha has stressed this quite a bit

15
00:02:47,400 --> 00:03:01,320
how through effort things are accomplished, not by lack of effort, but it's important

16
00:03:01,320 --> 00:03:10,640
to understand this in a sort of a nuanced way, because the ordinary way of approaching

17
00:03:10,640 --> 00:03:21,680
effort is to just push ahead, just work, work, push yourself, push yourself to limit

18
00:03:21,680 --> 00:03:28,040
and push yourself beyond your limit, and to some extent that's what we do in meditation,

19
00:03:28,040 --> 00:03:35,160
our limits of pain, we push ourselves beyond them, our limits of patience, we certainly

20
00:03:35,160 --> 00:03:47,840
push ourselves beyond those limits, but the goal really isn't to be working, working, working,

21
00:03:47,840 --> 00:03:54,040
I mean it's not to feel like work, a lot of people talk about meditation being effortless

22
00:03:54,040 --> 00:04:01,280
and of course that's a very dangerous thing to say, because it's very easy to get lazy,

23
00:04:01,280 --> 00:04:10,840
it's easy to mix effortlessness with laziness, being lazy is quite effortless, doesn't

24
00:04:10,840 --> 00:04:19,560
take a lot of effort, so it seems, and then ultimately the word effortlessness is a useful

25
00:04:19,560 --> 00:04:27,920
description, but effortlessness only comes when you have perfect effort, there's laziness

26
00:04:27,920 --> 00:04:36,360
in fact is quite stressful, we ever tried lying in bed all day, I can't do it for even

27
00:04:36,360 --> 00:04:49,240
an hour or two, it's quite stressful, in fact the meditation isn't stressful at all, it's

28
00:04:49,240 --> 00:05:01,960
our, to some extent our laziness or our inefficiency, the problem is not the meditation,

29
00:05:01,960 --> 00:05:08,480
it's that we're not very good at it, like anything, like any training, and when you get

30
00:05:08,480 --> 00:05:13,760
really good at it, it is quite seemingly effortless, but it's misleading because there's

31
00:05:13,760 --> 00:05:21,000
a lot of effort, it's just the effort is effortless, you don't have to work to work because

32
00:05:21,000 --> 00:05:27,960
you're good at working, that's an important point that I think we don't often, that we

33
00:05:27,960 --> 00:05:35,400
don't realize is that the natural state, the perfect natural state is not one of, it's

34
00:05:35,400 --> 00:05:44,920
not lazy, it's not a state of zero, it's a state of 100, perfect efficiency, and enlightened

35
00:05:44,920 --> 00:06:02,160
being has perfect energy, they are unflagging, unrelentingly mindful, they are constantly

36
00:06:02,160 --> 00:06:10,840
engaged in wholesomeness, their mind is so, so full of energy, it's an important point

37
00:06:10,840 --> 00:06:24,760
to realize, if you feel tired, if you feel like you're pushing yourself to meditate, this

38
00:06:24,760 --> 00:06:29,920
isn't the problem with the meditation, this is, this is what we have to get rid of, energy

39
00:06:29,920 --> 00:06:38,560
effort is not feeling like you're pushing yourself, effort is getting rid of the laziness,

40
00:06:38,560 --> 00:06:43,800
it's getting rid of the imbalance, once everything is balanced, it feels like there's

41
00:06:43,800 --> 00:06:55,080
nothing there, it feels like a perfectly tuned musical instrument, right, when you only

42
00:06:55,080 --> 00:06:59,680
notice when the instrument is out of tune, if you untune one of the strings then you

43
00:06:59,680 --> 00:07:07,520
notice, something's there, once it's in tune you stop noticing, there's harmony, that's

44
00:07:07,520 --> 00:07:19,920
what it feels like, so right effort, right effort isn't so much about pushing yourself,

45
00:07:19,920 --> 00:07:33,160
much more about focusing and directing your attention, directing your efforts rightly,

46
00:07:33,160 --> 00:07:41,120
so all the striving to gain and acquire things that can't possibly satisfy us, we give

47
00:07:41,120 --> 00:07:55,320
that up in favor of striving for, striving for not striving really, striving for a perfectly

48
00:07:55,320 --> 00:08:04,120
tuned state of being, so there are four right efforts, and it's quite a simple formula,

49
00:08:04,120 --> 00:08:10,120
you have wholesomeness and you have unwholesomeness, so it's about orienting our efforts

50
00:08:10,120 --> 00:08:19,120
properly, unwholesomeness that hasn't arisen, it takes effort to keep it from arising,

51
00:08:19,120 --> 00:08:33,320
right, when you see something that you want, it takes effort to just see it, when you hear

52
00:08:33,320 --> 00:08:41,640
some noise that it would disturb you, take effort, take effort to stay mindful so that

53
00:08:41,640 --> 00:08:47,240
everything we experience, when pain arises, to experience it just as pain, and not let

54
00:08:47,240 --> 00:08:56,240
the unwholesomeness arise, that's right effort, unwholesomeness is of three kinds, there's

55
00:08:56,240 --> 00:09:00,720
unwholesome actions, this is when you act or speak, that's what we've been talking about

56
00:09:00,720 --> 00:09:10,720
recently, when you act or speak unwholesomeness, this is wrong action, wrong speech, this

57
00:09:10,720 --> 00:09:20,560
is the way to come, this means things that are expressed, I talk about this, the second

58
00:09:20,560 --> 00:09:34,040
type of unwholesomeness is called Barutana, this is in the mind, thoughts, so preventing

59
00:09:34,040 --> 00:09:43,320
the thoughts from turning to unwholesomeness, takes effort, and Anusaya, the third one is Anusaya,

60
00:09:43,320 --> 00:09:54,400
these are defilements that haven't yet arisen, or these are the tendencies, rooting them

61
00:09:54,400 --> 00:10:05,840
out, anyway, so the first one is to not have them arise, mainly the first two, I guess,

62
00:10:05,840 --> 00:10:11,320
put out effort not to kill and not to steal that kind of thing, put out effort to prevent

63
00:10:11,320 --> 00:10:20,920
bad thoughts, unwholesomen thoughts, and those that have arisen unwholesomeness that has arisen,

64
00:10:20,920 --> 00:10:30,000
you make effort to dispel them, so an anger, greed, arise, do which you can to get rid

65
00:10:30,000 --> 00:10:35,160
of them, there's a question, how do you get rid of bad thoughts, how do you get rid

66
00:10:35,160 --> 00:10:43,600
of your unpleasant unwholesomeness, how do you get rid of anger, it's an important question,

67
00:10:43,600 --> 00:10:49,120
it's one that is dealt with in the Buddha's teaching, there are different ways, our ordinary

68
00:10:49,120 --> 00:10:55,840
way is to try and push it away, this is what you might say as effort, you might think

69
00:10:55,840 --> 00:11:03,560
it's right effort to force yourself not to get angry, you might think it's right effort

70
00:11:03,560 --> 00:11:12,560
to cultivate love when you're angry, for example, or cultivate dispassion when you're

71
00:11:12,560 --> 00:11:28,120
passionate, find some way to cultivate the opposite, and this works temporarily, I mean

72
00:11:28,120 --> 00:11:36,200
the sort of effort is beneficial in a temporary sense, but in insight meditation and mindfulness

73
00:11:36,200 --> 00:11:43,840
meditation, right effort for us is to understand things like greed and anger, to understand

74
00:11:43,840 --> 00:11:49,840
our defilements, so the effort that we put out is not to get rid of things, it's not to

75
00:11:49,840 --> 00:11:56,960
change, it's not to fix, it's to understand, when you have something that's causing your

76
00:11:56,960 --> 00:12:02,600
stress or suffering, if you have pain, try to understand the pain, and when you don't like

77
00:12:02,600 --> 00:12:09,720
it, try to understand the disliking, rather than reacting to it, getting upset by it,

78
00:12:09,720 --> 00:12:21,200
when you have emotions of fear or worry, liking or disliking, try and learn about those emotions,

79
00:12:21,200 --> 00:12:26,920
this takes effort, you know, mindfulness, the quality of mindfulness, it manifests itself

80
00:12:26,920 --> 00:12:34,400
as confronting the objective field, so it doesn't waver, it doesn't run away from it,

81
00:12:34,400 --> 00:12:45,480
it doesn't chase it away, doesn't cling to it, the effort to just stand your ground,

82
00:12:45,480 --> 00:12:53,600
not be moved, not be shaken by experiences, to just stay with it, it takes quite effort

83
00:12:53,600 --> 00:12:59,320
just to understand something, this is the change that has to come about, so that eventually

84
00:12:59,320 --> 00:13:10,600
through over time we see things objectively as they are without reacting to them, and

85
00:13:10,600 --> 00:13:17,200
the other two, so these are the first two types of right effort, preventing unwholesumness

86
00:13:17,200 --> 00:13:27,840
from arising, and removing unwholesumness that is already arisen, the other two are regards

87
00:13:27,840 --> 00:13:36,120
to wholesumness, wholesumness that is not yet arisen, work to give rise to it, work to

88
00:13:36,120 --> 00:13:43,920
do good deeds is a very simple way, you know, you want to learn right effort, do good things,

89
00:13:43,920 --> 00:13:56,360
be charitable, be ethical, practice meditation, wholesome mindset, loving kindness, for

90
00:13:56,360 --> 00:14:04,240
example, concentration, practice, tranquility meditation, these are all good things, of course

91
00:14:04,240 --> 00:14:13,600
in an ultimate sense it means cultivating mindfulness and wisdom, the highest, best, wholesumness,

92
00:14:13,600 --> 00:14:24,360
your mindfulness and wisdom, so the right effort to be mindful, this is the awakening

93
00:14:24,360 --> 00:14:28,840
that happens during the course, you start to see yourself, you start to really be here

94
00:14:28,840 --> 00:14:36,480
and now, this is getting rise to a new sort of wholesumness, it's not just being a kind

95
00:14:36,480 --> 00:14:50,600
or nice person, it's being a wise and clear-minded person, so when you close your eyes

96
00:14:50,600 --> 00:15:03,960
and you see yourself physical and the mental, it's giving rise to something new, a new state,

97
00:15:03,960 --> 00:15:16,320
a state of mind that is objective, clear, wise, that sees things as they are and then

98
00:15:16,320 --> 00:15:22,720
protect that, the fourth one is when wholesomeness is arisen, once you're being mindful,

99
00:15:22,720 --> 00:15:26,320
you know, work to keep it, this is really the trick as you go on in the practice, you'll

100
00:15:26,320 --> 00:15:32,920
be meditating, you'll be mindful, and your mind slips off, it's very easy to get distracted,

101
00:15:32,920 --> 00:15:39,320
very hard to pull yourself back again, so you work to stay on that razor's edge of the

102
00:15:39,320 --> 00:15:47,000
present moment, it's not something you can get stuck on, if you get lazy and you could

103
00:15:47,000 --> 00:15:55,000
carry it away, swept away, you need to keep yourself constantly here, now, here, now, again

104
00:15:55,000 --> 00:16:02,640
and again and again, that's why we use the mantra, it's kind of like a rope to pull us

105
00:16:02,640 --> 00:16:25,600
back to the present moment, so when we engage in good deeds, when we avoid unwholesomeness,

106
00:16:25,600 --> 00:16:36,600
this is right effort, we need an aducoma JIT through effort, we overcome suffering, it's

107
00:16:36,600 --> 00:16:40,720
just important to understand what is right effort, again, not just pushing yourself,

108
00:16:40,720 --> 00:16:54,800
but building up the effort to just be the effort to observe objectively and mindfully

109
00:16:54,800 --> 00:17:05,920
in the present moment, so that's the brief teaching on right effort, thank you all for

110
00:17:05,920 --> 00:17:21,320
tuning in, let's see if there are questions, oh a bunch of questions right I've been

111
00:17:21,320 --> 00:17:28,320
away for a few days, you all should just go, don't stay for that,

112
00:17:51,320 --> 00:18:10,520
once the delay when I become aware of a thought after the rise is it possible to reduce

113
00:18:10,520 --> 00:18:19,360
that delay, it's for sure as you as you cultivate the practice over time you can get more

114
00:18:19,360 --> 00:18:27,000
skilled at it, but it takes work and it takes time, if you're interested in cultivating

115
00:18:27,000 --> 00:18:33,400
that I'd encourage you to do a meditation course, you'll find your better able to catch

116
00:18:33,400 --> 00:18:42,800
the thoughts earlier, especially during the course itself, I have a lot of anger, sometimes

117
00:18:42,800 --> 00:18:47,800
I feel only you'll never be able to remove it, be meditating for three years, but decide

118
00:18:47,800 --> 00:18:57,680
to give it up because it's too difficult, what can I do, I feel hopeless, well the first

119
00:18:57,680 --> 00:19:03,440
step or one of the important steps of meditation is to see that you can't remove it,

120
00:19:03,440 --> 00:19:12,680
to stop trying to remove it, or that self, that's ego, bads, this control, stop trying

121
00:19:12,680 --> 00:19:18,880
to get rid of the anger, start trying to understand it, that's not an easy thing and it's

122
00:19:18,880 --> 00:19:25,400
not just gonna fix your problem, stop trying to fix it, stop seeing it as a problem, anger

123
00:19:25,400 --> 00:19:31,280
is just anger, and break it up, it's not just the anger, it's also the pain that it

124
00:19:31,280 --> 00:19:39,320
brings you in the physical sensations of heat and stress, and break it up with the thoughts,

125
00:19:39,320 --> 00:19:44,040
the thoughts about the anger, the thoughts that lead to the anger that kind of thing,

126
00:19:44,040 --> 00:19:49,280
they're separate, all these things are separate from each other, the experiences that

127
00:19:49,280 --> 00:20:02,640
make you angry seeing, hearing, whatever, as you can probably see now that giving up wasn't

128
00:20:02,640 --> 00:20:09,520
the answer either, giving up didn't give it, didn't free you from the anger, but giving

129
00:20:09,520 --> 00:20:15,400
up can be useful, when you give up meditating for example, that can be a useful moment

130
00:20:15,400 --> 00:20:20,760
because it's a moment of realising, I can't fix this, it's important realization of

131
00:20:20,760 --> 00:20:26,200
meditation, so when that happens, start meditating again, you'll find it's probably easier

132
00:20:26,200 --> 00:20:31,600
because you're less inclined to try to get rid of it, of course then you're less angry

133
00:20:31,600 --> 00:20:39,040
as a result, said something like, it's not about feeling happy, happiness comes from not

134
00:20:39,040 --> 00:20:49,440
feeling at all, well that's not really, not quite what I said, but so feeling happy

135
00:20:49,440 --> 00:20:59,960
means to feel something that we like, and that we're attached to where we desire, that

136
00:20:59,960 --> 00:21:06,360
we like, no, but that's not real happiness because those feelings come and they go and

137
00:21:06,360 --> 00:21:11,920
they actually don't mean anything in the end, it's just our judgment that leads us to

138
00:21:11,920 --> 00:21:16,920
like them and that liking of course leads to further clinging and frustration when we don't

139
00:21:16,920 --> 00:21:23,400
get what we want and so we're constantly in this push and pull and it's not really happiness,

140
00:21:23,400 --> 00:21:27,800
happiness comes when you're free from all that, and freedom from all that has nothing to do

141
00:21:27,800 --> 00:21:37,320
with feeling, so that's sort of a basic answer to your question without going into too much

142
00:21:37,320 --> 00:21:48,280
detail, is it problematic to have ambitions, what drives someone to meditate, well ambition

143
00:21:48,280 --> 00:21:52,720
often drives someone to meditate but I think ambition is probably not the right word in

144
00:21:52,720 --> 00:22:01,280
most cases, what drives people to meditate is suffering and a desire to be free from suffering

145
00:22:01,280 --> 00:22:09,240
and it is desire in many cases, it's aversion to suffering and that's not good but it's

146
00:22:09,240 --> 00:22:12,800
good in that it leads you to meditate, once you start to meditate you start to see that

147
00:22:12,800 --> 00:22:18,320
that aversion is the real problem, a version from suffering, if you free yourself from

148
00:22:18,320 --> 00:22:22,600
that and you can learn to look at the suffering, you find it doesn't cause you suffering

149
00:22:22,600 --> 00:22:32,320
and you'll really just let go of it, but yes, ambitions generally are problematic because

150
00:22:32,320 --> 00:22:39,440
they're clinging, it's not being objective and you're unable to see things as they

151
00:22:39,440 --> 00:22:46,280
are when you have ambition, are all forms of sexual indulgence inherently dukha or

152
00:22:46,280 --> 00:22:52,200
can sexual experiences give rise to positive mind states, if express with loving kindness

153
00:22:52,200 --> 00:22:58,360
and non-attachment can sex be a liberating practice of compassion and equanimity, I think

154
00:22:58,360 --> 00:23:10,200
you're stretching it, I don't know, can it be a sure, is it likely to be, I'm not convinced

155
00:23:10,200 --> 00:23:17,200
that because see an enlightened being, let's take our prime example of the enlightened

156
00:23:17,200 --> 00:23:37,920
being, I'm really talk about the enlightened being, the problem with it is that it's

157
00:23:37,920 --> 00:23:46,080
something that leads to great pleasure and for two enlightened beings, it wouldn't

158
00:23:46,080 --> 00:23:52,760
be a problem to engage in sexual individuals, I mean they'd have no reason to, it's

159
00:23:52,760 --> 00:24:01,160
a sort of something that provides no purpose and service no purpose for enlightened

160
00:24:01,160 --> 00:24:06,320
being, but for enlightened people, for anyone who wants to have sex, there's an attachment

161
00:24:06,320 --> 00:24:27,560
to the pleasure, so technically, yes, it's possible to do anything, I mean it's possible,

162
00:24:27,560 --> 00:24:31,800
it's possible to do pretty much anything, I'm just trying to think how far you can go

163
00:24:31,800 --> 00:24:36,560
with that, I mean the point isn't enlightened being wouldn't have sex, because there's

164
00:24:36,560 --> 00:24:50,360
no point, there's no benefit to it, but for anyone who's not enlightened, it's not

165
00:24:50,360 --> 00:25:01,360
the action, let's put it that way, put it really simple, it's the state of mind, so I mean

166
00:25:01,360 --> 00:25:07,440
your question is kind of, I'm not clear on, I think the wording could be changed a bit

167
00:25:07,440 --> 00:25:15,960
sexual indulgence is not inherently duke, and an act can never be, an act can never

168
00:25:15,960 --> 00:25:22,640
be unwholesome in itself, it's the mind states associated with the action that are unwholesome,

169
00:25:22,640 --> 00:25:28,800
so, well I mean for example, take rape, and there's this case of an enlightened bikwini

170
00:25:28,800 --> 00:25:36,320
who was raped, and it was nothing to her, well it was happening, she said to the person

171
00:25:36,320 --> 00:25:44,120
doing it, this is bad for you, I didn't think of herself, she said, think about what

172
00:25:44,120 --> 00:25:52,680
you're doing, this is really a terrible thing that you're doing to yourself, so there

173
00:25:52,680 --> 00:25:59,760
was no defilement there, but your question is whether it's inherently duke, and if you're

174
00:25:59,760 --> 00:26:04,080
talking about it inherently being suffering, well every, and again getting back to the

175
00:26:04,080 --> 00:26:10,600
meaning of duke, but every sankara is duke, so of course sexual indulgence falls under

176
00:26:10,600 --> 00:26:21,320
that category, meaning it's none of it going to make you happy, okay, so can sexual

177
00:26:21,320 --> 00:26:26,920
experience give rise to wholesome mind states, so here's the question, I mean who can have

178
00:26:26,920 --> 00:26:38,560
sexual intercourse without it getting rise to unwholesome if you're asking whether it's entirely

179
00:26:38,560 --> 00:26:45,200
unwholesome, the answer of course is no, I mean many people who have sex do it out of

180
00:26:45,200 --> 00:26:50,320
love as well, they love the other person, so there are positive mind states there as

181
00:26:50,320 --> 00:26:58,040
well, could you be mindful having sex, I think absolutely yes, it doesn't take away the

182
00:26:58,040 --> 00:27:05,720
fact that it's probably quite unwholesome, but I would say if you're going to have sexual

183
00:27:05,720 --> 00:27:14,720
intercourse do it mindfully, for sure, and I would for sure say that there is a potential

184
00:27:14,720 --> 00:27:20,640
for wholesome, it's not a hyper potential of course because I have very much caught up in

185
00:27:20,640 --> 00:27:28,560
the pleasure of the act, but the point being that there is no act that is either wholesome

186
00:27:28,560 --> 00:27:34,160
run wholesome and acts don't exist in themselves, what exists is moments of experience,

187
00:27:34,160 --> 00:27:39,120
so suppose you're having sexual intercourse, there's going to be moment after moment after

188
00:27:39,120 --> 00:27:44,520
moment, and each moment has a potential to be wholesome run wholesome, what we look

189
00:27:44,520 --> 00:27:53,400
at is on the whole sexual intercourse is generally unwholesome, so that's the reason for

190
00:27:53,400 --> 00:28:01,400
eventually giving it up, and if you're dedicated to the practice, giving it up, becoming

191
00:28:01,400 --> 00:28:11,520
a monastic and that kind of thing, why did it be could shave their eyebrows in Thailand?

192
00:28:11,520 --> 00:28:21,080
There's no real good reason, I mean these aren't the question, not a bad question, it's

193
00:28:21,080 --> 00:28:25,280
against the rules, we don't answer these sorts of questions, but I'm not going to just

194
00:28:25,280 --> 00:28:31,720
outright dismiss it, but there's no good reason, it's pretty a mundane answer to that

195
00:28:31,720 --> 00:28:38,840
question, I wouldn't spend too much time worrying about that.

196
00:28:38,840 --> 00:28:44,200
What livelihood is it immoral to do sometimes work without paying taxes, and you have

197
00:28:44,200 --> 00:28:53,800
more money while you need less work, so you have more energy, avoiding taxes is problematic,

198
00:28:53,800 --> 00:29:02,840
some people would say it's stealing, you're stealing money, it's theft has a lot to do

199
00:29:02,840 --> 00:29:07,120
with the government, it has a lot to do with the society, but it's a very complicated

200
00:29:07,120 --> 00:29:13,200
question because sometimes governments steal money from people, many people think of taxes

201
00:29:13,200 --> 00:29:23,040
as theft, so the only record that we have regarding taxes is seeing tax evasion as

202
00:29:23,040 --> 00:29:30,400
theft, and the idea being that you really have to follow the government rule in that

203
00:29:30,400 --> 00:29:37,760
regard, if you don't pay taxes, you're stealing, so I'd be careful there, I don't

204
00:29:37,760 --> 00:29:48,560
know, I mean I'm not convinced entirely of that, but something to keep in mind, I mean

205
00:29:48,560 --> 00:29:54,680
I don't think as with many questions like this there's no real set answer, it's a question

206
00:29:54,680 --> 00:30:00,600
about being mindful in the present moment and reacting and interacting mindfully with

207
00:30:00,600 --> 00:30:06,640
your situation, but if it's just about you know more money less work that's not very

208
00:30:06,640 --> 00:30:12,120
good reason, because you can rob a bank that's not so much work, yeah it's a lot of work,

209
00:30:12,120 --> 00:30:20,920
but find ways to get very very rich doing very very bad things, to talk a little bit

210
00:30:20,920 --> 00:30:34,200
on what it's like to be a monk, sorry, I mean there's not much secret about it, amongst

211
00:30:34,200 --> 00:30:38,800
too many different kinds of things, but the important thing is that you're meditating,

212
00:30:38,800 --> 00:30:46,520
the important thing is that you're cultivating mindfulness and morality and so on, can monks

213
00:30:46,520 --> 00:30:56,440
adopt children, again probably the sort of question we're looking at, but the answer I think

214
00:30:56,440 --> 00:31:04,920
is pretty much no, it's actually not adopt, but take care of kids, yes, monks can take

215
00:31:04,920 --> 00:31:15,280
care of kids, that's true, my mind wanders to old feelings especially for old lovers,

216
00:31:15,280 --> 00:31:19,920
these feelings do not bother me, but we're meditating they can tear me away from my focus,

217
00:31:19,920 --> 00:31:24,640
we'll focus on them, that should be your focus when these thoughts arise, take them as

218
00:31:24,640 --> 00:31:29,240
your meditation object, you're seeing how your mind works, that the mind is chaotic, that

219
00:31:29,240 --> 00:31:34,400
the mind is unpredictable and that helps you to let go, to stop trying to keep your mind

220
00:31:34,400 --> 00:31:41,320
from thinking certain things and so on, so when you say it tears you away from your focus

221
00:31:41,320 --> 00:31:47,160
well, welcome to reality, that's the way neat, the way the world works, we're constantly

222
00:31:47,160 --> 00:31:52,200
being pulled and pushed and so on and that helps us let go, helps us see the stress inherent

223
00:31:52,200 --> 00:31:59,400
in some sarin and let go, it's having passion a good or a bad thing, passion generally

224
00:31:59,400 --> 00:32:06,400
a bad thing, it's again like ambition, it drags you on and cultivates preference and

225
00:32:06,400 --> 00:32:14,320
so on, ego, it cuts with thoughts and bad things, should awareness be observed as something

226
00:32:14,320 --> 00:32:19,920
that is already there or as something that arises with objects of mind, awareness arises

227
00:32:19,920 --> 00:32:27,360
with objects of mind, consciousness is one of the five aggregates and they arise together

228
00:32:27,360 --> 00:32:35,440
with every experience, but if by awareness you mean meditative awareness that's even

229
00:32:35,440 --> 00:32:40,760
different because that doesn't always arise with every mind state, but when you're mindful,

230
00:32:40,760 --> 00:32:48,400
when you cultivate mindfulness then it will arise with the mind state, or with the object

231
00:32:48,400 --> 00:32:58,080
okay and that's the questions for tonight, thank you, I hope I wasn't overly dismissive

232
00:32:58,080 --> 00:33:09,600
or smug, thank you.

