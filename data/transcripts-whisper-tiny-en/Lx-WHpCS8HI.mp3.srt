1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Dhamupada.

2
00:00:05,000 --> 00:00:14,000
Today we continue with verse 241, which reads as follows.

3
00:00:14,000 --> 00:00:34,000
A sajaya, malamanta, anutana, malagara, malangwanasakosa jang, pamado rakato malam,

4
00:00:34,000 --> 00:00:52,000
which means lack of recitation is a blemish on spiritual teachings.

5
00:00:52,000 --> 00:01:03,000
Lack of industry is a blemish on the household life.

6
00:01:03,000 --> 00:01:17,000
Laziness is a blemish on beauty, and negligence is a blemish of a guard.

7
00:01:17,000 --> 00:01:24,000
Again we're in the malawaga, which malam means blemish or stain or corruption.

8
00:01:24,000 --> 00:01:31,000
Something that detracts from or undermines or weakens something.

9
00:01:31,000 --> 00:01:47,000
So this verse was apparently taught in regards to a story about la ludei.

10
00:01:47,000 --> 00:01:58,000
La ludei was a monk in the time of the Buddha, and he was well known for his general lack of wisdom or even common sense.

11
00:01:58,000 --> 00:02:02,000
He was always getting into trouble of some sort or another.

12
00:02:02,000 --> 00:02:14,000
He doesn't seem to have been terribly evil, just didn't think things through very well or didn't have a sense of mindfulness or self-awareness.

13
00:02:14,000 --> 00:02:20,000
Pretty good example of how not to be, but not necessarily evil.

14
00:02:20,000 --> 00:02:31,000
Just said the wrong things and didn't have any self-position or self-awareness to help him to see what was appropriate to stay and appropriate to do.

15
00:02:31,000 --> 00:02:34,000
Very inappropriate sort of person.

16
00:02:34,000 --> 00:02:46,000
Apparently fairly arrogant as well, or he had his defilements because one day a group of

17
00:02:46,000 --> 00:02:54,000
householders came to the monastery and offered arms in the morning and then listened to teachings from the monks,

18
00:02:54,000 --> 00:02:58,000
probably apparently Sariputa and Mogulana were teaching.

19
00:02:58,000 --> 00:03:10,000
So after the talk they were standing around gathered and they started remarking to themselves how wonderful was the teachings of Sariputa.

20
00:03:10,000 --> 00:03:21,000
The Buddha's chief disciple and Mogulana, his second chief disciple, this pair of very special monks,

21
00:03:21,000 --> 00:03:25,000
how they were praising them, how great they were and how great their teaching was.

22
00:03:25,000 --> 00:03:32,000
How wonderful it was to hear them when the Buddha wasn't able to teach.

23
00:03:32,000 --> 00:03:40,000
And Waludai heard this and he said to them, that's because you've only heard them speak.

24
00:03:40,000 --> 00:03:56,000
Imagine if you heard me teach the Dhamma and they heard this and they thought to themselves wow this must be some great teacher for him to be that sure of himself.

25
00:03:56,000 --> 00:04:02,000
They thought to himself, it would be great if someday we could hear him teach.

26
00:04:02,000 --> 00:04:17,000
So on some other day, as lay people are in Buddhism or want to do, they approached Waludai and invited him to give the teaching.

27
00:04:17,000 --> 00:04:21,000
They said, we're going to give arms today, give food to the monks today.

28
00:04:21,000 --> 00:04:29,000
We'd like you to be the one to give us a teaching after the meal.

29
00:04:29,000 --> 00:04:36,000
And Waludai accepted it, said sure.

30
00:04:36,000 --> 00:04:48,000
And the day came and they offered food to the monks and invited Waludai to ascend to this very special Dhamma seat that they made.

31
00:04:48,000 --> 00:04:57,000
They would often have this very elaborate ornamental chair to signify the appreciation and the reverence to the Dhamma.

32
00:04:57,000 --> 00:05:08,000
And it was the one example where a junior monk could sit higher than a senior monk because we put the Dhamma up as the highest.

33
00:05:08,000 --> 00:05:13,000
The teachings and the truth.

34
00:05:13,000 --> 00:05:26,000
And so he got up on this chair, a very special, very special occasion and they have this fan that you put in front of your face because when teaching you don't want, there was a tradition to not have people look at you.

35
00:05:26,000 --> 00:05:28,000
You don't want to be focused on the person.

36
00:05:28,000 --> 00:05:37,000
The idea is for the teacher to be a vessel, a conduit, a catalyst for the teaching, not the source of it.

37
00:05:37,000 --> 00:05:43,000
You don't want to be focused on the person and to be focused on the teachings.

38
00:05:43,000 --> 00:05:49,000
This is why we're often encouraged when we listen to a Dhamma talk to close our eyes.

39
00:05:49,000 --> 00:05:56,000
Try and be as mindful as we can so our mind is clear and we're focused on the teaching.

40
00:05:56,000 --> 00:06:05,000
Put the fan in front of his face and fiddled with it a little bit and fidgeted.

41
00:06:05,000 --> 00:06:11,000
But he knew nothing of the Dhamma, he knew nothing of the teachings and so he couldn't teach a single thing.

42
00:06:11,000 --> 00:06:24,000
So he just sat there and everyone's wondering what's happening and then he said, you know, I think it's better if someone else teaches, I will instead do a recitation.

43
00:06:24,000 --> 00:06:34,000
So I guess this was a tradition, I don't really understand it, but it seems there was a tradition of also reciting the Dhamma rather than explaining it in the talk.

44
00:06:34,000 --> 00:06:40,000
There would be someone who would recite the Buddhist teaching for the lay people.

45
00:06:40,000 --> 00:06:43,000
So that was another tradition, I guess, on the opposite time.

46
00:06:43,000 --> 00:06:46,000
It even occurs now in modern times in Thailand.

47
00:06:46,000 --> 00:06:54,000
They'll have these sorts of recitations or special teachings where they just recite a teaching, reading it from wrote,

48
00:06:54,000 --> 00:06:58,000
and toning it kind of.

49
00:06:58,000 --> 00:07:04,000
And the people were like, okay, and so they found some other monk invited someone else to teach.

50
00:07:04,000 --> 00:07:12,000
That monk gave the teaching and then invited Alaludai to give a recitation.

51
00:07:12,000 --> 00:07:19,000
Alaludai got up, but he couldn't recite any better than he could teach.

52
00:07:19,000 --> 00:07:24,000
He hadn't memorized any of the teachings, so it was useless.

53
00:07:24,000 --> 00:07:29,000
And he said, you know, this isn't the right time for me, he said, I will do the recitation tonight.

54
00:07:29,000 --> 00:07:33,000
Find someone else to do it this morning.

55
00:07:33,000 --> 00:07:37,000
So, okay, very well, they got found someone else to do it.

56
00:07:37,000 --> 00:07:46,000
And they practiced during the day, practiced the teachings, and maybe discussed the teachings as skin questions or spending their time in the monastery.

57
00:07:46,000 --> 00:07:53,000
Maybe going home to do work, but then in the evening coming back for the evening teaching,

58
00:07:53,000 --> 00:07:57,000
and they invited him to give a recitation.

59
00:07:57,000 --> 00:08:01,000
He stood up there and he said, you know, I can't do it at night.

60
00:08:01,000 --> 00:08:03,000
He said, tomorrow morning, have someone else to do it.

61
00:08:03,000 --> 00:08:09,000
Now I'll give the teaching, I'll give the recitation a more morning.

62
00:08:09,000 --> 00:08:14,000
And here they were getting a little bit fed up, but they said, okay, and so in the morning,

63
00:08:14,000 --> 00:08:19,000
again they invited him up there, and again he was silent.

64
00:08:19,000 --> 00:08:27,000
He could no more teach it in the evening or in the morning than he could the day before.

65
00:08:27,000 --> 00:08:36,000
And at this point they were completely fed up, and the text says they picked up dirt and stones and sticks, maybe,

66
00:08:36,000 --> 00:08:47,000
in a threatening manner and they were like, look, you talk down about the Buddha's chief disciples,

67
00:08:47,000 --> 00:08:51,000
bragging about you being a better teacher than them.

68
00:08:51,000 --> 00:08:55,000
You certainly better have something to teach us.

69
00:08:55,000 --> 00:09:00,000
And the Lunei saw these people who honestly weren't acting all that Buddhist,

70
00:09:00,000 --> 00:09:03,000
but we're doing so.

71
00:09:03,000 --> 00:09:13,000
We're certainly passionate about protecting the chief disciples' reputation and calling this monk to account.

72
00:09:13,000 --> 00:09:18,000
And so he got up and ran, and the people ran after him.

73
00:09:18,000 --> 00:09:28,000
He ran and stumbled and fell into a cesspool.

74
00:09:28,000 --> 00:09:34,000
And later on the monks were talking about this state of affairs and what happened.

75
00:09:34,000 --> 00:09:37,000
Apparently this was a thing that had happened in a past life.

76
00:09:37,000 --> 00:09:42,000
There's a jot to Kastori, and the Buddha heard them, and he said to them, oh, this wasn't the first thing.

77
00:09:42,000 --> 00:09:46,000
He said, in a past life, another day it was a jackal.

78
00:09:46,000 --> 00:09:52,000
And I don't remember the exact story, but it's something about a jackal.

79
00:09:52,000 --> 00:10:01,000
A jackal was challenging a lion to a duel, and said, I'm the king of beasts who are not the king of beasts.

80
00:10:01,000 --> 00:10:03,000
And so the lions said, okay, well that's fight.

81
00:10:03,000 --> 00:10:08,000
And then the jackal defecated on it itself or something.

82
00:10:08,000 --> 00:10:12,000
And the lion was like, you know what?

83
00:10:12,000 --> 00:10:15,000
If you want to win so bad, I give it to you.

84
00:10:15,000 --> 00:10:18,000
If this is your way of winning, then you can have the win.

85
00:10:18,000 --> 00:10:20,000
Anyway, there's a jot to Kastori.

86
00:10:20,000 --> 00:10:23,000
And the Laudet was the jackal in that birth.

87
00:10:23,000 --> 00:10:30,000
And then the Buddha said, you know, Laudet, his big problem was even though he'd learned.

88
00:10:30,000 --> 00:10:34,000
Or even if he had learned the teaching, he'd never spent any time memorizing it.

89
00:10:34,000 --> 00:10:39,000
And so when called to teach it, he couldn't teach.

90
00:10:39,000 --> 00:10:46,000
And then he taught this verse.

91
00:10:46,000 --> 00:10:51,000
So the big lesson we get from the story, obviously, is don't be like Laudet.

92
00:10:51,000 --> 00:10:56,000
Don't be arrogant.

93
00:10:56,000 --> 00:11:04,000
I think the subtle lesson is one of self-awareness, as I said, self-possession.

94
00:11:04,000 --> 00:11:10,000
It shows a remarkable lack of self-possession, self-awareness, whatever the word is.

95
00:11:10,000 --> 00:11:19,000
On his behalf, on his part, in insulting and demeaning the cheap disciples.

96
00:11:19,000 --> 00:11:29,000
And in propping himself up to an extent that he couldn't deliver on.

97
00:11:29,000 --> 00:11:38,000
And so I think it's rare for someone to be so unself aware or such a lack of self-awareness

98
00:11:38,000 --> 00:11:41,000
to come to that sort of state of affairs.

99
00:11:41,000 --> 00:11:48,000
But it does show how important it is that when we do or say anything,

100
00:11:48,000 --> 00:11:56,000
that we are in possession of mindfulness and self-awareness.

101
00:11:56,000 --> 00:12:00,000
That we often do or say things unthinkingly and they get us into trouble.

102
00:12:00,000 --> 00:12:05,000
Not because we were malicious necessarily, but on a whim, we might say something,

103
00:12:05,000 --> 00:12:13,000
we might promise something, we might cause insult or expectation in others,

104
00:12:13,000 --> 00:12:24,000
or misunderstanding in others, and just in general do and say things that cause suffering and trouble.

105
00:12:24,000 --> 00:12:27,000
So that is a lesson.

106
00:12:27,000 --> 00:12:31,000
The lesson of the verse is in four parts.

107
00:12:31,000 --> 00:12:39,000
Each one of them has its own lesson, I think.

108
00:12:39,000 --> 00:12:45,000
And so the first part is relating to what we call mantra,

109
00:12:45,000 --> 00:12:47,000
which is an interesting word.

110
00:12:47,000 --> 00:12:50,000
It's where we get the modern word mantra.

111
00:12:50,000 --> 00:12:56,000
The Sanskrit for mantra would be mantra, and that's where the word mantra comes from.

112
00:12:56,000 --> 00:13:06,000
And it's a word that's used to describe, used to refer to spiritual teachings,

113
00:13:06,000 --> 00:13:11,000
even magical words, magical verses.

114
00:13:11,000 --> 00:13:21,000
So there would have been certain teachings that became considered to be in and of themselves powerful.

115
00:13:21,000 --> 00:13:30,000
And by reciting those verses, it was a magical words, incantations,

116
00:13:30,000 --> 00:13:36,000
like abracadabra or something, and people would recite them to themselves.

117
00:13:36,000 --> 00:13:41,000
You see this even in Buddhism today, there is groups of people who call themselves Buddhist,

118
00:13:41,000 --> 00:13:48,000
whose whole practices to recite mantras, without any sense of understanding the mantra necessarily,

119
00:13:48,000 --> 00:13:55,000
but just the idea that the mantras somehow powerful and reciting it a certain number of times a day has some power to it.

120
00:13:55,000 --> 00:14:02,000
Now, it's not to disparage those teachings entirely, though it does seem somewhat limited,

121
00:14:02,000 --> 00:14:10,000
but an important lesson here is the difference between intellectual learning,

122
00:14:10,000 --> 00:14:16,000
which is much more favored in modern times and recitation,

123
00:14:16,000 --> 00:14:20,000
which is quite often looked down upon in modern times.

124
00:14:20,000 --> 00:14:26,000
And the difference is really one of a practical nature.

125
00:14:26,000 --> 00:14:30,000
In university and in school, we learn so many things,

126
00:14:30,000 --> 00:14:35,000
and we are asked to memorize some of them,

127
00:14:35,000 --> 00:14:43,000
but we're not expected to recite or commit all of the teachings,

128
00:14:43,000 --> 00:14:50,000
or commit teachings to memory as a main source of learning for most things,

129
00:14:50,000 --> 00:15:00,000
very specialized careers like engineering and some sciences.

130
00:15:00,000 --> 00:15:05,000
But if we look at things like religious teachings or philosophical teachings,

131
00:15:05,000 --> 00:15:09,000
we're rarely expected in modern times to memorize them.

132
00:15:09,000 --> 00:15:14,000
And yet that was exactly what they would be expected to do in the Buddhist time.

133
00:15:14,000 --> 00:15:16,000
And the difference is striking.

134
00:15:16,000 --> 00:15:22,000
Recitation is really how religion works to indoctrinate its followers,

135
00:15:22,000 --> 00:15:25,000
and that can be seen as a very dangerous thing, of course,

136
00:15:25,000 --> 00:15:27,000
and indoctrination can be very dangerous.

137
00:15:27,000 --> 00:15:31,000
But if you think about a good teaching,

138
00:15:31,000 --> 00:15:36,000
something that you have learned and you're sure that there's no reason to think of it

139
00:15:36,000 --> 00:15:40,000
as a bad teaching, some general principle, this is good.

140
00:15:40,000 --> 00:15:45,000
This leads me to be a better person, this is a wholesome teaching.

141
00:15:45,000 --> 00:15:50,000
The indoctrination that comes from memorizing that can only be a good support.

142
00:15:50,000 --> 00:15:54,000
You see, if you learn about mindfulness and so on,

143
00:15:54,000 --> 00:15:59,000
then you have this intellectual appreciation of it or understanding of it.

144
00:15:59,000 --> 00:16:03,000
But it's very easy to have a disconnect between that and your daily life.

145
00:16:03,000 --> 00:16:07,000
If you've learned this and you agree with it, and then you go about your life without it,

146
00:16:07,000 --> 00:16:09,000
ever thinking about it.

147
00:16:09,000 --> 00:16:14,000
If, on the other hand, you recite the four Satipatana daily,

148
00:16:14,000 --> 00:16:19,000
or, you know, many people learn about mindfulness and never memorize the four Satipatana.

149
00:16:19,000 --> 00:16:22,000
Don't even know what they are.

150
00:16:22,000 --> 00:16:26,000
And that's a problem because it leads to a disconnect.

151
00:16:26,000 --> 00:16:31,000
It's a potential problem because there's a disconnect between your thoughts,

152
00:16:31,000 --> 00:16:36,000
your ordinary daily thoughts, and the teachings that you've learned.

153
00:16:36,000 --> 00:16:39,000
By reciting them, of course, it doesn't enlighten you.

154
00:16:39,000 --> 00:16:44,000
There's no magical power to the teachings,

155
00:16:44,000 --> 00:16:48,000
but it certainly makes you think about them more.

156
00:16:48,000 --> 00:16:52,000
This is really how our meditation practice works,

157
00:16:52,000 --> 00:16:58,000
not exactly, but the same principle applies by repeating to yourselves, pain, pain,

158
00:16:58,000 --> 00:17:01,000
or thinking, thinking.

159
00:17:01,000 --> 00:17:06,000
It brings your mind closer to the actual truth, the actual experience.

160
00:17:06,000 --> 00:17:11,000
So it's not to equate the two, but the same sort of principle applies.

161
00:17:11,000 --> 00:17:15,000
This is what a mantra, the power of it, how it's more powerful

162
00:17:15,000 --> 00:17:20,000
than just an ordinary script that you may have read once.

163
00:17:20,000 --> 00:17:24,000
When it becomes something that you repeat to yourself,

164
00:17:24,000 --> 00:17:28,000
it is internalized and it's regularized, it's habitualized.

165
00:17:28,000 --> 00:17:33,000
It becomes a part of your psyche, becomes something you think about often.

166
00:17:33,000 --> 00:17:36,000
And so it becomes a part of how you incline your mind.

167
00:17:36,000 --> 00:17:40,000
It's a very easy way if you want to learn an easy way

168
00:17:40,000 --> 00:17:42,000
to support your meditation practice.

169
00:17:42,000 --> 00:17:49,000
Memorize some Buddhist texts, even just verses like I'm memorizing these verses every week.

170
00:17:49,000 --> 00:17:52,000
It's a great support for your meditation practice,

171
00:17:52,000 --> 00:17:56,000
though it isn't a replacement and you shouldn't equate the two.

172
00:17:56,000 --> 00:18:00,000
When we talk about noting practice as being a mantra,

173
00:18:00,000 --> 00:18:07,000
it's important to understand it's not exactly repeating the word in your mind.

174
00:18:07,000 --> 00:18:12,000
But it's not so far off because in order to repeat the proper word,

175
00:18:12,000 --> 00:18:14,000
you have to have a connection to the experience.

176
00:18:14,000 --> 00:18:18,000
And so it challenges you to be connected to the experience,

177
00:18:18,000 --> 00:18:21,000
but ultimately there should be always a connection.

178
00:18:21,000 --> 00:18:24,000
It should never be this disconnect where you would say rising,

179
00:18:24,000 --> 00:18:29,000
falling, rising, falling in your mind without ever really being aware of the stomach moving.

180
00:18:29,000 --> 00:18:36,000
It should be related to an awareness and there should be a certain amount of striving for that.

181
00:18:36,000 --> 00:18:41,000
So this part of the verse is specifically related to learning,

182
00:18:41,000 --> 00:18:44,000
but it does tie in with meditation practice

183
00:18:44,000 --> 00:18:48,000
and is something that has long-term practitioners we should consider.

184
00:18:48,000 --> 00:18:53,000
Not just as many people do reading the suit as some people I know who have read,

185
00:18:53,000 --> 00:18:59,000
the entire Dopitika, and they can remember certain teachings,

186
00:18:59,000 --> 00:19:06,000
but actually memorizing some of it, not long parts, but you'll feel a great power

187
00:19:06,000 --> 00:19:11,000
in memorizing parts and understanding them just verses or so on.

188
00:19:11,000 --> 00:19:13,000
If you understand them and memorize them,

189
00:19:13,000 --> 00:19:18,000
they become very close to your heart.

190
00:19:18,000 --> 00:19:26,000
The second part of the verse Anutana Mala-Gara doesn't relate to meditation practice.

191
00:19:26,000 --> 00:19:28,000
It's the lack of industry.

192
00:19:28,000 --> 00:19:31,000
It is a blemish for the household life,

193
00:19:31,000 --> 00:19:36,000
but as meditators, it's an important point

194
00:19:36,000 --> 00:19:40,000
that we need livelihood to survive.

195
00:19:40,000 --> 00:19:44,000
We need a certain amount of income of some sort,

196
00:19:44,000 --> 00:19:47,000
even if it's just food and shelter and the requisite.

197
00:19:47,000 --> 00:19:52,000
Even monks, we have to have these things to survive.

198
00:19:52,000 --> 00:19:56,000
And this requires a certain amount of industry.

199
00:19:56,000 --> 00:20:00,000
And not only that, but industry in general

200
00:20:00,000 --> 00:20:04,000
is an important part of how we live our lives, even as meditators.

201
00:20:04,000 --> 00:20:08,000
One great benefit of practicing meditation,

202
00:20:08,000 --> 00:20:12,000
especially walking meditation, is that it cultivates industry.

203
00:20:12,000 --> 00:20:14,000
It cultivates industriousness.

204
00:20:14,000 --> 00:20:17,000
It cultivates patience, endurance.

205
00:20:17,000 --> 00:20:21,000
It cultivates many qualities that are useful and helpful

206
00:20:21,000 --> 00:20:27,000
in living our lives with a sense of well-being.

207
00:20:27,000 --> 00:20:30,000
Lack of industry destroys our livelihood.

208
00:20:30,000 --> 00:20:36,000
It causes us to neglect many things about our lives.

209
00:20:36,000 --> 00:20:40,000
Many meditators will find that when they go back to work

210
00:20:40,000 --> 00:20:45,000
after practicing meditation, they're much better able to deal

211
00:20:45,000 --> 00:20:49,000
with activities they would have found boring.

212
00:20:49,000 --> 00:20:55,000
You know, we have this culture of affirming

213
00:20:55,000 --> 00:21:00,000
and justifying and believing somehow in the appropriateness

214
00:21:00,000 --> 00:21:04,000
or the rightness of being bored and dissatisfied

215
00:21:04,000 --> 00:21:08,000
with repetitive activities, mundane monotonous activities.

216
00:21:08,000 --> 00:21:12,000
But for a meditator, they can become quite soothing and peaceful

217
00:21:12,000 --> 00:21:16,000
and so there's very little difficulty after you've done walking meditation.

218
00:21:16,000 --> 00:21:20,000
Walking meditation can be quite unpleasant

219
00:21:20,000 --> 00:21:24,000
in the beginning because it's monotonous.

220
00:21:24,000 --> 00:21:27,000
The same thing over and over again in that.

221
00:21:27,000 --> 00:21:29,000
We have a viewer, an opinion,

222
00:21:29,000 --> 00:21:34,000
an inclination, a bias against that sort of thing.

223
00:21:34,000 --> 00:21:42,000
That it's wrong, that it's boring, that it's just a bad thing to engage in.

224
00:21:42,000 --> 00:21:46,000
It's a negative quality or something to be repetitive.

225
00:21:46,000 --> 00:21:49,000
And of course there's no reason for that to be,

226
00:21:49,000 --> 00:21:55,000
except that in order to feed our chemical receptors in our brain

227
00:21:55,000 --> 00:21:57,000
we need diversity.

228
00:21:57,000 --> 00:21:59,000
If you get the same thing over and over again,

229
00:21:59,000 --> 00:22:01,000
it doesn't feed the receptors.

230
00:22:01,000 --> 00:22:04,000
It ceases to have the same effect

231
00:22:04,000 --> 00:22:09,000
and you need a new and more exciting stimulus.

232
00:22:09,000 --> 00:22:12,000
That's why monotonous things seem boring,

233
00:22:12,000 --> 00:22:14,000
not because they are,

234
00:22:14,000 --> 00:22:17,000
but because we want our chemicals.

235
00:22:17,000 --> 00:22:20,000
We want our drugs.

236
00:22:20,000 --> 00:22:23,000
For someone who is free from that sort of want,

237
00:22:23,000 --> 00:22:28,000
they find monotony can be quite peaceful.

238
00:22:28,000 --> 00:22:31,000
Even the word monotony has such a negative connotation,

239
00:22:31,000 --> 00:22:35,000
but it really does literally just mean monotone.

240
00:22:35,000 --> 00:22:38,000
It's a singular flavor,

241
00:22:38,000 --> 00:22:40,000
which sounds dreadful to most people,

242
00:22:40,000 --> 00:22:44,000
but quite peaceful to someone who has realized

243
00:22:44,000 --> 00:22:47,000
the higher sense of happiness in peace.

244
00:22:47,000 --> 00:22:52,000
It has a sense of strength and composure

245
00:22:52,000 --> 00:23:00,000
of one who has found peace.

246
00:23:00,000 --> 00:23:04,000
The third part of the verse relates to beauty,

247
00:23:04,000 --> 00:23:07,000
which again is not something meditators are all that concerned with,

248
00:23:07,000 --> 00:23:10,000
but it does bear considering.

249
00:23:10,000 --> 00:23:15,000
Malanwanasakos and junk,

250
00:23:15,000 --> 00:23:19,000
laziness is a blemish on beauty.

251
00:23:19,000 --> 00:23:22,000
No matter how attractive you are or beautiful you are,

252
00:23:22,000 --> 00:23:25,000
there's something ugly about laziness.

253
00:23:25,000 --> 00:23:28,000
So it's not to say that we should be concerned with beauty,

254
00:23:28,000 --> 00:23:33,000
but it is a good point that physical beauty only gets you so far.

255
00:23:33,000 --> 00:23:37,000
For someone who is attached to their physical beauty,

256
00:23:37,000 --> 00:23:40,000
it's always going to be limited by your qualities of mind,

257
00:23:40,000 --> 00:23:43,000
one of which is laziness.

258
00:23:43,000 --> 00:23:47,000
Someone is lazy that they lose some attractiveness.

259
00:23:47,000 --> 00:23:52,000
They lose some of their appeal, there's an ugliness to laziness.

260
00:23:52,000 --> 00:23:57,000
And this is important because we often overlook the fact

261
00:23:57,000 --> 00:24:01,000
that laziness is a blemish, laziness is a problem,

262
00:24:01,000 --> 00:24:04,000
and we can become quite complacent.

263
00:24:04,000 --> 00:24:08,000
We engage in activities that are relaxing.

264
00:24:08,000 --> 00:24:14,000
We favor them over those that are taxing or those that train us,

265
00:24:14,000 --> 00:24:19,000
or anyone who does physical training can tell you how beneficial

266
00:24:19,000 --> 00:24:24,000
and how gratifying it is to gain something from exertion

267
00:24:24,000 --> 00:24:30,000
and from industry and how laziness actually is inferior.

268
00:24:30,000 --> 00:24:36,000
Any relaxation or sense of happiness or pleasure you might get from being lazy

269
00:24:36,000 --> 00:24:40,000
is ultimately corrupted by your addiction to it

270
00:24:40,000 --> 00:24:45,000
or just the lazy state of mind.

271
00:24:45,000 --> 00:24:53,000
Someone who is industrious has experienced this state of energetic alertness

272
00:24:53,000 --> 00:25:01,000
and readiness of mind, wheelliness of mind that comes from training.

273
00:25:01,000 --> 00:25:05,000
It's another one great thing that you get from meditation.

274
00:25:05,000 --> 00:25:16,000
Cultivates this alertness, this readiness, this energetic quality.

275
00:25:16,000 --> 00:25:19,000
The fourth part of the verse is, of course,

276
00:25:19,000 --> 00:25:25,000
the deepest part relating to pamada negligence.

277
00:25:25,000 --> 00:25:36,000
pamado rakatomalang, rakatou is one who guards the pamada negligence.

278
00:25:36,000 --> 00:25:40,000
Negligence is a blemish for one who guards.

279
00:25:40,000 --> 00:25:45,000
It's a corrupts, it's a problem for one who guards.

280
00:25:45,000 --> 00:25:51,000
Someone is guarding the commentary gives the example of someone who is guarding cows

281
00:25:51,000 --> 00:25:54,000
or sheep or livestock.

282
00:25:54,000 --> 00:25:56,000
And you have to keep your eye on them.

283
00:25:56,000 --> 00:25:59,000
If you don't keep your eye on them, they'll wander off and get into trouble

284
00:25:59,000 --> 00:26:03,000
and then you get into trouble as a result.

285
00:26:03,000 --> 00:26:06,000
If a person is guarding a gate, we talk about it.

286
00:26:06,000 --> 00:26:10,000
We know how we use the word guard in a conventional sense.

287
00:26:10,000 --> 00:26:14,000
Think of a castle or a fort or a city.

288
00:26:14,000 --> 00:26:17,000
Someone who is guarding maybe a security guard.

289
00:26:17,000 --> 00:26:23,000
If they're negligent and they fall asleep or they don't keep track of their surroundings,

290
00:26:23,000 --> 00:26:34,000
they can easily be overcome by enemies or bandits or criminals and so on.

291
00:26:34,000 --> 00:26:36,000
They can't do their job.

292
00:26:36,000 --> 00:26:43,000
And the Buddha used this sort of metaphor or analogy quite often

293
00:26:43,000 --> 00:26:52,000
because our mind is similar. Our mind is so precious and so pure.

294
00:26:52,000 --> 00:26:56,000
It isn't in and of itself corrupt,

295
00:26:56,000 --> 00:27:01,000
but it becomes corrupted by invasion.

296
00:27:01,000 --> 00:27:05,000
The invasion of things like greed and anger.

297
00:27:05,000 --> 00:27:07,000
These things are not intrinsic to the mind.

298
00:27:07,000 --> 00:27:11,000
We aren't an angry person or a greedy person.

299
00:27:11,000 --> 00:27:14,000
We develop them as habits.

300
00:27:14,000 --> 00:27:18,000
And their constructs, their artificial, their artifices,

301
00:27:18,000 --> 00:27:24,000
they develop, they build and they collapse,

302
00:27:24,000 --> 00:27:28,000
depending on how we incline the mind.

303
00:27:28,000 --> 00:27:31,000
And so the Buddha taught upamada.

304
00:27:31,000 --> 00:27:35,000
Upamada, you might say, is the very core of the Buddha's teaching.

305
00:27:35,000 --> 00:27:42,000
Vigilance doesn't mean anxiety or hyper awareness or anything.

306
00:27:42,000 --> 00:27:45,000
It means not ever letting our guard down.

307
00:27:45,000 --> 00:27:51,000
Not ever.

308
00:27:51,000 --> 00:27:56,000
It means constant awareness or sort of constancy.

309
00:27:56,000 --> 00:28:04,000
It doesn't mean you have to be anxious or worried about any laps of awareness.

310
00:28:04,000 --> 00:28:09,000
It means we have to train not only to practice,

311
00:28:09,000 --> 00:28:18,000
but to create a consistency of practice, a continuity of practice.

312
00:28:18,000 --> 00:28:21,000
This is why we teach meditators to be mindful during the day

313
00:28:21,000 --> 00:28:26,000
when they're walking or standing or eating,

314
00:28:26,000 --> 00:28:30,000
working, whatever you do, the Buddha said,

315
00:28:30,000 --> 00:28:33,000
when you're in the washroom even,

316
00:28:33,000 --> 00:28:38,000
when you're cleaning, when you're cooking, when you're doing anything.

317
00:28:38,000 --> 00:28:42,000
Because defilements don't take a break.

318
00:28:42,000 --> 00:28:44,000
You don't leave them behind and say,

319
00:28:44,000 --> 00:28:48,000
okay, I'll come back for you when I come to my next meditation session.

320
00:28:48,000 --> 00:28:51,000
They go with you throughout the day,

321
00:28:51,000 --> 00:28:56,000
and they continue to grow into flourish as habits.

322
00:28:56,000 --> 00:29:02,000
Unless we retrain our minds, unless we cultivate new habits.

323
00:29:02,000 --> 00:29:07,000
It was the last words of the Buddha,

324
00:29:07,000 --> 00:29:09,000
somebody at that.

325
00:29:09,000 --> 00:29:14,000
They said, fulfill yourself in this one teaching.

326
00:29:14,000 --> 00:29:16,000
Fulfill this one teaching.

327
00:29:16,000 --> 00:29:26,000
Bring yourself to perfection in this one thing, upper-modern vigilance.

328
00:29:26,000 --> 00:29:30,000
And so we try to be vigilant, we try to constantly come back

329
00:29:30,000 --> 00:29:36,000
whenever we can remember to the cultivation of

330
00:29:36,000 --> 00:29:43,000
self-awareness, clarity of mind.

331
00:29:43,000 --> 00:29:45,000
Just to remember who we are and what we're doing,

332
00:29:45,000 --> 00:29:46,000
just to remember not who we are.

333
00:29:46,000 --> 00:29:49,000
Remember what we're doing and what we're experiencing.

334
00:29:49,000 --> 00:30:01,000
Remember the reality of our experience and our situation.

335
00:30:01,000 --> 00:30:11,000
Because our minds need guards as well.

336
00:30:11,000 --> 00:30:14,000
When we don't guard the mind, then we develop bad habits.

337
00:30:14,000 --> 00:30:17,000
So when we do guard the mind,

338
00:30:17,000 --> 00:30:22,000
it's not just that it keeps the mind out of trouble.

339
00:30:22,000 --> 00:30:28,000
The cultivating of pot proper habits, the cultivating of mindfulness as a habit.

340
00:30:28,000 --> 00:30:35,000
The retraining of the mind in this way changes us, of course.

341
00:30:35,000 --> 00:30:38,000
And it's not just any type of,

342
00:30:38,000 --> 00:30:42,000
no, it's different from an ordinary type of training.

343
00:30:42,000 --> 00:30:47,000
Because the change that comes about allows us to see clearly

344
00:30:47,000 --> 00:30:57,000
everything about who we are and about the nature of reality.

345
00:30:57,000 --> 00:31:00,000
It's not even just about guarding the mind,

346
00:31:00,000 --> 00:31:06,000
but it's about straightening the mind and purifying the mind.

347
00:31:06,000 --> 00:31:13,000
When we clean our houses, we have to keep the house clean.

348
00:31:13,000 --> 00:31:17,000
When we clean our minds, the process of keeping the mind clean,

349
00:31:17,000 --> 00:31:19,000
of protecting the mind.

350
00:31:19,000 --> 00:31:23,000
It's not just defending the mind from these invading defilements,

351
00:31:23,000 --> 00:31:27,000
but it's a process of cleansing the mind,

352
00:31:27,000 --> 00:31:34,000
cleansing the mind of the habits that give rise to defilements.

353
00:31:34,000 --> 00:31:46,000
It just means seeing reality as it is, seeing our experience as it is.

354
00:31:46,000 --> 00:31:51,000
Training ourselves to have an objective experience of reality.

355
00:31:51,000 --> 00:31:54,000
This is what we're doing in mindfulness practice.

356
00:31:54,000 --> 00:32:03,000
We're someone who guards, but someone who trains and purifies our mind as well.

357
00:32:03,000 --> 00:32:11,000
And so the teaching here, the most pertinent part of the teaching,

358
00:32:11,000 --> 00:32:15,000
is that that practice, that practice of training the mind, purifying the mind,

359
00:32:15,000 --> 00:32:20,000
requires vigilance for it to be complete.

360
00:32:20,000 --> 00:32:26,000
It has to be continuous, and it has to be whole-hearted.

361
00:32:26,000 --> 00:32:29,000
You can't just practice as a hobby,

362
00:32:29,000 --> 00:32:33,000
coming to do a meditation course as the best thing is a great thing.

363
00:32:33,000 --> 00:32:36,000
But it shouldn't be seen as a temper, I think.

364
00:32:36,000 --> 00:32:43,000
It should be seen as a practice as a means of really getting a foundation

365
00:32:43,000 --> 00:32:48,000
by which to live your life, to re-train the mind,

366
00:32:48,000 --> 00:32:52,000
to really work on some deep-seated habits,

367
00:32:52,000 --> 00:32:56,000
like what you see in the course, anger that comes up,

368
00:32:56,000 --> 00:33:03,000
fear, greed, desire, anxiety, arrogance, anything.

369
00:33:03,000 --> 00:33:10,000
So many things that you'll see throughout the course.

370
00:33:10,000 --> 00:33:17,000
To change these about ourselves, to change our inclination,

371
00:33:17,000 --> 00:33:28,000
and to develop the skill that we can take back into our lives

372
00:33:28,000 --> 00:33:32,000
and practice regularly, practice consistently,

373
00:33:32,000 --> 00:33:36,000
to make a part of who we are,

374
00:33:36,000 --> 00:33:40,000
to be able to guard and protect our mind,

375
00:33:40,000 --> 00:33:47,000
and to nourish and to grow good qualities of mind,

376
00:33:47,000 --> 00:33:51,000
that will lead us to peace, happiness, and freedom from suffering.

377
00:33:51,000 --> 00:33:54,000
So that's the Dhammapada for tonight.

378
00:33:54,000 --> 00:33:55,000
Thank you all for listening.

379
00:33:55,000 --> 00:34:19,000
I wish you all the best.

