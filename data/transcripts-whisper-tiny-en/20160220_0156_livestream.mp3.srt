1
00:00:00,000 --> 00:00:27,400
Good evening, everyone.

2
00:00:27,400 --> 00:00:39,960
It's, quote, there's about wealth.

3
00:00:39,960 --> 00:00:56,080
Many of you probably are aware Buddhism is not a well-thorean to religion, doesn't place

4
00:00:56,080 --> 00:01:02,680
much importance on wealth.

5
00:01:02,680 --> 00:01:06,680
Can't think of any religion that does.

6
00:01:06,680 --> 00:01:19,680
I think Buddhism especially has some very fairly pointed things to say, or at least has

7
00:01:19,680 --> 00:01:26,680
a reputation of probably being anti-wealth.

8
00:01:26,680 --> 00:01:34,680
I think, I mean monks are supposed to be monks who are the spokespeople of the religion

9
00:01:34,680 --> 00:01:38,680
are supposed to live in poverty.

10
00:01:38,680 --> 00:01:49,480
They are not allowed to touch money or valuable objects and gold and jewels.

11
00:01:49,480 --> 00:02:08,480
They have to wear these robes, they have to eat the food that were given, they have poverty

12
00:02:08,480 --> 00:02:19,480
is a big part of Buddhism.

13
00:02:19,480 --> 00:02:34,480
Contentment is the greatest wealth.

14
00:02:34,480 --> 00:02:57,480
Oh, I had my timer set right in the drum.

15
00:02:57,480 --> 00:03:20,480
Contentment is the greatest form of wealth.

16
00:03:20,480 --> 00:03:41,360
What is, what wealth is supposed to do is fulfill our wants, our needs, and so ordinary wealth

17
00:03:41,360 --> 00:03:50,360
is able to temporarily fulfill our wants and our desires and our needs, which our needs

18
00:03:50,360 --> 00:03:59,760
anyway make it an important thing.

19
00:03:59,760 --> 00:04:04,960
So in this quote that we have tonight, the Buddha acknowledges that wealth is good for you.

20
00:04:04,960 --> 00:04:09,480
It can make you happy, it can make your parents happy, it can make your spouse, your children,

21
00:04:09,480 --> 00:04:17,480
your children, some workers happy, your friends in companions happy, and you can use it

22
00:04:17,480 --> 00:04:32,480
to give charity or religious donations is what the quote refers to, but charity is general.

23
00:04:32,480 --> 00:04:43,680
So if you do, if obviously poverty is a problem, but the dangers of that wealth is that

24
00:04:43,680 --> 00:04:49,480
it's temporary, it disappears.

25
00:04:49,480 --> 00:04:52,080
Wealth can be incredibly dangerous, right?

26
00:04:52,080 --> 00:04:57,880
More wealth you are, the more trapped you are by it can be.

27
00:04:57,880 --> 00:05:02,880
Anyways wealthy people are trapped or can be trapped by their money.

28
00:05:02,880 --> 00:05:10,680
There was an interesting story read about people who were in the lottery that apparently

29
00:05:10,680 --> 00:05:14,680
winning the lottery is one of the worst things that can happen to you.

30
00:05:14,680 --> 00:05:20,600
You look it up, it's quite interesting what happens to people with the statistics are for

31
00:05:20,600 --> 00:05:34,400
people who have won the lottery, homicide, suicide, a lot of robbery, you lose friends,

32
00:05:34,400 --> 00:05:43,600
you lose your family, lose your life, wealth that kind of wealth can be dangerous,

33
00:05:43,600 --> 00:05:56,480
but even putting that aside, worldly wealth, though it's necessary to maintain livelihood

34
00:05:56,480 --> 00:06:03,400
and this is where Buddhism does acknowledge the need for wealth, even monks have to have

35
00:06:03,400 --> 00:06:15,680
balls, they have to have things, they have to have tools, the things that allow them to continue

36
00:06:15,680 --> 00:06:16,520
their life.

37
00:06:16,520 --> 00:06:26,520
But beyond that wealth isn't actually able to provide contentment, it isn't actually able

38
00:06:26,520 --> 00:06:31,440
to satisfy, it isn't able to bring happiness.

39
00:06:31,440 --> 00:06:36,000
So the happiness that it can bring is the ability to live one's life without worry

40
00:06:36,000 --> 00:06:45,920
without fear, and potentially without having to go hungry, without having to be cold

41
00:06:45,920 --> 00:06:58,200
or hot, without having to suffer through lack of the amenities or the necessities, but

42
00:06:58,200 --> 00:07:04,520
as far as fulfilling our wants, our desires, there's no wealth, no amount of wealth that

43
00:07:04,520 --> 00:07:07,560
can do that.

44
00:07:07,560 --> 00:07:19,280
And so in terms of our wants, this is why contentment is the greatest form of wealth.

45
00:07:19,280 --> 00:07:25,200
Because that's what we're looking for from wealth, we have to understand that the problem

46
00:07:25,200 --> 00:07:28,400
is we want something, this is an issue.

47
00:07:28,400 --> 00:07:32,040
When you want something, it's actually a problem.

48
00:07:32,040 --> 00:07:38,720
If you don't get it, to the extent that you want it, it's going to cause suffering for

49
00:07:38,720 --> 00:07:40,400
you until you get it.

50
00:07:40,400 --> 00:07:44,120
If you want it alive, it can cause great suffering.

51
00:07:44,120 --> 00:07:48,240
If you want it a little about just a little suffering until you get it, of course, once

52
00:07:48,240 --> 00:07:51,800
you get it, then you're pleased.

53
00:07:51,800 --> 00:07:58,040
With that pleasure, reinforces the desire, and so you want it more, and it becomes more

54
00:07:58,040 --> 00:08:07,200
acute, and eventually you don't get what you want and do suffer, even so you're constantly

55
00:08:07,200 --> 00:08:17,920
living your life chasing after your desires, contentment on the other hand accomplishes

56
00:08:17,920 --> 00:08:25,040
the same thing, but it does it by going the other way, by destroying the wanting.

57
00:08:25,040 --> 00:08:34,840
When you remove the wanting, without reinforcing it, without following it, then you weaken

58
00:08:34,840 --> 00:08:37,040
it.

59
00:08:37,040 --> 00:08:42,320
When you can find peace without the things you want, and you can give up your wants and

60
00:08:42,320 --> 00:08:47,080
find happiness without needing this or that to make you happy.

61
00:08:47,080 --> 00:08:58,480
Before in concept, to many of us, we're so used to finding happiness in things, in experiences.

62
00:08:58,480 --> 00:09:07,200
It becomes kind of religious or a view that we have, our belief, and if you challenge

63
00:09:07,200 --> 00:09:16,040
that, people will...there's a tendency to react quite negatively, we will get upset,

64
00:09:16,040 --> 00:09:25,080
you know, again, turned off by the things you're saying, you say, find happiness by letting

65
00:09:25,080 --> 00:09:31,960
go, by not clinging, by giving up your desires, where a few people will be interested.

66
00:09:31,960 --> 00:09:36,720
As I've talked about recently, most of us don't even understand that statement.

67
00:09:36,720 --> 00:09:45,120
Again, we can't even comprehend it, or so fix, fixated on our desires, and it hard to

68
00:09:45,120 --> 00:09:51,040
even comprehend the idea of being happy without...it's not maybe that's not giving people

69
00:09:51,040 --> 00:09:58,080
enough credit, but especially all of you here, anyone who's watching this, has an understanding

70
00:09:58,080 --> 00:10:06,080
of some understanding of the problem, they've had suffering in their lives, and so they

71
00:10:06,080 --> 00:10:13,800
can see how our wants will be one thing to be a certain way when we don't get that, it's

72
00:10:13,800 --> 00:10:18,800
quite suffering, quite a bit of suffering, great suffering.

73
00:10:18,800 --> 00:10:28,160
And so we turn the meditation thinking to find contentment, this is really what it is.

74
00:10:28,160 --> 00:10:34,000
Because it hasn't come to you while here's the...here's an idea, the reason why meditation

75
00:10:34,000 --> 00:10:45,000
might be interesting is to find contentment, to be able to live without need.

76
00:10:45,000 --> 00:10:52,880
So how does it do this meditation, teaches us objectivity, it's like straightening the mind.

77
00:10:52,880 --> 00:11:00,200
The mind is bent and crooked all out of shape, so you have to strike it and straighten it.

78
00:11:00,200 --> 00:11:04,200
And every moment that you see things as they are, you're straightening the mind.

79
00:11:04,200 --> 00:11:08,760
Not just every moment you're meditating, that's not the case, but well you're meditating

80
00:11:08,760 --> 00:11:13,680
when you find a moment where you just see something as it is when you say rise, and you're

81
00:11:13,680 --> 00:11:15,760
just aware of the rising.

82
00:11:15,760 --> 00:11:22,400
That moment is a pure state, and it's just straight.

83
00:11:22,400 --> 00:11:33,200
The mind is straight, is wholesome as pure, and you cultivate this cultivation of this

84
00:11:33,200 --> 00:11:43,080
state these states, which is...this is the path to contentment.

85
00:11:43,080 --> 00:11:48,880
There's an at time in that moment, there's no need for anything, there's no want for anything.

86
00:11:48,880 --> 00:11:56,960
When you cultivate these moments after moments consecutively, you're slowly become content.

87
00:11:56,960 --> 00:12:02,520
Now you're dealing with so many likes and dislikes, so it's not easy, and it's not very

88
00:12:02,520 --> 00:12:03,520
comfortable.

89
00:12:03,520 --> 00:12:10,080
It's mostly not content, but in those moments you can see the contentment.

90
00:12:10,080 --> 00:12:12,560
That's what we're fighting for, it's what we're working for.

91
00:12:12,560 --> 00:12:17,720
It takes time, but through practice, you call this is what you cultivate.

92
00:12:17,720 --> 00:12:28,960
This is the direction that you steer yourself towards.

93
00:12:28,960 --> 00:12:33,080
I don't want to talk too much about wealth, I don't like talking about money, and so on,

94
00:12:33,080 --> 00:12:41,880
but contentment, that's where it's at.

95
00:12:41,880 --> 00:12:47,440
There's your dhamma bit for tonight.

96
00:12:47,440 --> 00:12:50,640
Open up to hang out, if anybody wants to.

97
00:12:50,640 --> 00:13:02,640
Come on and ask questions, if you don't have any questions, then just say good night.

98
00:13:02,640 --> 00:13:06,240
Met a couple of Sri Lankan people on the street today, and they're coming tomorrow morning

99
00:13:06,240 --> 00:13:07,240
to visit.

100
00:13:07,240 --> 00:13:13,640
It's nice, people in the community, there actually are people on this area.

101
00:13:13,640 --> 00:13:19,320
There's a Sri Lankan monk, or Bangladeshian monk in Toronto, said he was going to get

102
00:13:19,320 --> 00:13:23,640
me in touch with this Sri Lankan community here, and he never did.

103
00:13:23,640 --> 00:13:31,920
He's very busy though, but apparently there's quite a few Sri Lankan people in Hamilton.

104
00:13:31,920 --> 00:13:40,760
There's a some western people coming out to meditate to visit, so this place could become

105
00:13:40,760 --> 00:13:47,000
a real part of the community to see how it goes.

106
00:13:47,000 --> 00:13:52,880
I gave meditation booklet to the woman who serves me at Tim Hortons, a really good group

107
00:13:52,880 --> 00:13:53,880
of people at Tim Hortons.

108
00:13:53,880 --> 00:13:54,880
That's funny.

109
00:13:54,880 --> 00:13:56,880
They've gotten to know them.

110
00:13:56,880 --> 00:13:59,880
How many of you have to do with the pain?

111
00:13:59,880 --> 00:14:05,360
Probably when you're echoing me, do you have something else on, like YouTube or the audio

112
00:14:05,360 --> 00:14:10,360
stream?

113
00:14:10,360 --> 00:14:11,360
I don't know.

114
00:14:11,360 --> 00:14:14,360
They're rubbing.

115
00:14:14,360 --> 00:14:20,360
She doesn't hear me, I don't think.

116
00:14:20,360 --> 00:14:21,360
Hello.

117
00:14:21,360 --> 00:14:22,360
Hello.

118
00:14:22,360 --> 00:14:23,360
Salty Banthay.

119
00:14:23,360 --> 00:14:24,360
Hi.

120
00:14:24,360 --> 00:14:25,360
Hi, sorry.

121
00:14:25,360 --> 00:14:29,360
It's been so long, I forgot that I have to turn the abona.

122
00:14:29,360 --> 00:14:31,360
Sorry about that.

123
00:14:31,360 --> 00:14:33,360
So I had a question.

124
00:14:33,360 --> 00:14:34,360
Okay.

125
00:14:34,360 --> 00:14:35,360
Okay.

126
00:14:35,360 --> 00:14:37,360
So we have a teaching that we hear all the time.

127
00:14:37,360 --> 00:14:39,360
We heard it pretty very similarly.

128
00:14:39,360 --> 00:14:40,360
So let's see.

129
00:14:40,360 --> 00:14:43,360
I'll only be seeing, like hearing, only be hearing.

130
00:14:43,360 --> 00:14:44,360
Let's see.

131
00:14:44,360 --> 00:14:45,360
I'll only be smiling.

132
00:14:45,360 --> 00:14:48,360
And I mean, it sounds good.

133
00:14:48,360 --> 00:14:50,360
But with my heart, yeah.

134
00:14:50,360 --> 00:14:53,360
I forgot that I had to turn the abona.

135
00:14:53,360 --> 00:14:56,360
I had a question.

136
00:14:56,360 --> 00:14:57,360
Okay.

137
00:14:57,360 --> 00:15:03,360
If you're going to come on the hangout, you have to turn the other thing off first.

138
00:15:03,360 --> 00:15:06,360
Let's see, I'll only be seeing.

139
00:15:06,360 --> 00:15:09,360
Hello.

140
00:15:09,360 --> 00:15:10,360
Okay.

141
00:15:10,360 --> 00:15:11,360
Okay.

142
00:15:11,360 --> 00:15:16,360
So with this teaching, I mean it sounds good.

143
00:15:16,360 --> 00:15:24,360
But my common sense tells me that we have to judge what comes in contact with our senses

144
00:15:24,360 --> 00:15:31,360
to be safe if you're truly just tasting, tasting, but not judging the taste.

145
00:15:31,360 --> 00:15:33,360
You could be eating rancid food.

146
00:15:33,360 --> 00:15:40,360
If you're smelling, but not recognizing the smell, your house could be burning.

147
00:15:40,360 --> 00:15:50,360
I mean, how does this teaching work in the real world where we do have to evaluate our surroundings?

148
00:15:50,360 --> 00:15:51,360
Yeah.

149
00:15:51,360 --> 00:15:55,360
I mean, it's not the whole truth.

150
00:15:55,360 --> 00:16:02,360
It's not like a way of living your life, but it's the necessary activity that you need

151
00:16:02,360 --> 00:16:05,360
to perform during a meditation practice.

152
00:16:05,360 --> 00:16:10,360
Because if you don't get to the point where seeing is just seeing, you'll never get to

153
00:16:10,360 --> 00:16:13,360
the root.

154
00:16:13,360 --> 00:16:21,360
But I mean, even in Iran, has to think and has to process as the talk has to interact with

155
00:16:21,360 --> 00:16:22,360
people.

156
00:16:22,360 --> 00:16:24,360
Has to judge.

157
00:16:24,360 --> 00:16:31,360
Seeing is not just seeing, seeing is a pit in front of you that you have to go around.

158
00:16:31,360 --> 00:16:37,360
But you don't have that in minutes.

159
00:16:37,360 --> 00:16:42,360
It's still seeing, you see the pit and you say seeing, seeing.

160
00:16:42,360 --> 00:16:47,360
And then there's still the thoughts that arise based on the pit.

161
00:16:47,360 --> 00:16:49,360
They arise anyway.

162
00:16:49,360 --> 00:16:56,360
The point is your mind is straight.

163
00:16:56,360 --> 00:17:01,360
So those thoughts are free from, oh, my gosh, it's a pit.

164
00:17:01,360 --> 00:17:04,360
They can still arise and they still will arise.

165
00:17:04,360 --> 00:17:07,360
They weren't as bad.

166
00:17:07,360 --> 00:17:11,360
So it's only part of the picture.

167
00:17:11,360 --> 00:17:13,360
Those thoughts of, oh, that's a pit.

168
00:17:13,360 --> 00:17:15,360
Well, hey, wait, you're not seeing seeing.

169
00:17:15,360 --> 00:17:21,360
But when you said seeing is just seeing, when you said seeing seeing, it neutralizes the experience.

170
00:17:21,360 --> 00:17:29,360
At least for a moment, you could still get react, but the first moment of seeing the pit or the tiger or whatever

171
00:17:29,360 --> 00:17:34,360
has neutralized the fear and the shock.

172
00:17:34,360 --> 00:17:41,360
I mean, that being said, to some extent, there is a sense of having to stay neutral.

173
00:17:41,360 --> 00:17:44,360
Like, of course, if it's rancid food, you know that it's rancid food.

174
00:17:44,360 --> 00:17:46,360
You have no reason to eat it.

175
00:17:46,360 --> 00:17:51,360
But if it's a tiger, and the tiger is going to eat you.

176
00:17:51,360 --> 00:18:01,360
Well, you run, but once it catches you, seeing, let's see, just be seeing pain, just be pain.

177
00:18:01,360 --> 00:18:05,360
So you kind of have to go in and out of this.

178
00:18:05,360 --> 00:18:08,360
Is that would that be correct?

179
00:18:08,360 --> 00:18:12,360
Not even.

180
00:18:12,360 --> 00:18:17,360
It's arguable that, yeah, you'd have to go a little, but it's not really out of it.

181
00:18:17,360 --> 00:18:19,360
Because your mind is pure.

182
00:18:19,360 --> 00:18:24,360
It's like when you walk to walking meditation, you're only saying,

183
00:18:24,360 --> 00:18:27,360
step being right, stepping left, but you're aware of a lot else.

184
00:18:27,360 --> 00:18:30,360
You're aware of the shifting of the body.

185
00:18:30,360 --> 00:18:35,360
You know, you may be aware of stray thoughts, but you don't have to note them all.

186
00:18:35,360 --> 00:18:40,360
As long as you keep your mind straight, as long as you keep your mind pure.

187
00:18:40,360 --> 00:18:50,360
And so by noting every second or so, you know, frequently, your mind will say, stay straight.

188
00:18:50,360 --> 00:19:01,360
So it's important to do this, but it's not everything, especially if you're live.

189
00:19:01,360 --> 00:19:08,360
But that being said, when you actually do a meditation, or the best way to understand it,

190
00:19:08,360 --> 00:19:14,360
is during a meditation, if you want to follow it, where you can follow it perfectly strictly.

191
00:19:14,360 --> 00:19:21,360
When you're sitting with your eyes closed, well, that's the time where you're going to look at these things.

192
00:19:21,360 --> 00:19:31,360
But all it takes is a moment where I tell the story about this monk who is teaching,

193
00:19:31,360 --> 00:19:39,360
I was watching, and he says, hearing, hearing, and then...

194
00:19:39,360 --> 00:19:41,360
Well, he was teaching.

195
00:19:41,360 --> 00:19:46,360
It's probably one of the most impressive things I've ever seen.

196
00:19:46,360 --> 00:19:53,360
So all it was is in that moment for him hearing was just hearing, as he was teaching it.

197
00:19:53,360 --> 00:19:58,360
And that's what it means.

198
00:19:58,360 --> 00:20:09,360
My challenge is just to make it enough of a part of my day to ever, to ever get to that point.

199
00:20:09,360 --> 00:20:12,360
Okay, I'm doing it.

200
00:20:12,360 --> 00:20:22,360
One of our recent meditators finished, of course, did okay, finished it.

201
00:20:22,360 --> 00:20:26,360
It was a little bit disappointed, I think, with the results.

202
00:20:26,360 --> 00:20:31,360
I was pretty sure he got where he was supposed to be.

203
00:20:31,360 --> 00:20:33,360
I mean, you can't tell.

204
00:20:33,360 --> 00:20:37,360
It's hard for him to tell, it's hard for me to tell.

205
00:20:37,360 --> 00:20:44,360
But then he emailed me, and he'd gotten stuck on a flight.

206
00:20:44,360 --> 00:20:51,360
And he was on this plane for like nine hours or something, on the runway.

207
00:20:51,360 --> 00:20:56,360
It's an insane amount of time sitting on the runway.

208
00:20:56,360 --> 00:21:03,360
And what he described to me, what happened on the runway, suddenly he came back and...

209
00:21:03,360 --> 00:21:04,360
I was just listening.

210
00:21:04,360 --> 00:21:09,360
I said, that sounds exactly like that.

211
00:21:09,360 --> 00:21:12,360
On the plane, leaving to go home.

212
00:21:12,360 --> 00:21:13,360
You never know.

213
00:21:13,360 --> 00:21:15,360
You never know.

214
00:21:15,360 --> 00:21:16,360
And when you're ready.

215
00:21:16,360 --> 00:21:20,360
Sometimes you get so stressed during the course.

216
00:21:20,360 --> 00:21:24,360
Or you get stressed during the course, and it inhibits it prevents.

217
00:21:24,360 --> 00:21:28,360
You're from your worried, or near the end it can get stressful.

218
00:21:28,360 --> 00:21:37,360
And if you let it get to you, if you're not mindful, then it just happens when you relax.

219
00:21:37,360 --> 00:21:39,360
And he was like, Anna, right?

220
00:21:39,360 --> 00:21:44,360
Anna was doing walking meditation all night, trying to become enlightened.

221
00:21:44,360 --> 00:21:46,360
And nothing.

222
00:21:46,360 --> 00:21:50,360
And then it was almost dawn, and he felt like it got nowhere.

223
00:21:50,360 --> 00:21:52,360
And he was really stressed out.

224
00:21:52,360 --> 00:21:54,360
And then he thought, and he said, oh, I'm pushing too hard.

225
00:21:54,360 --> 00:21:56,360
And so he laid down.

226
00:21:56,360 --> 00:22:00,360
And before his head touched the pillow, he was not.

227
00:22:06,360 --> 00:22:07,360
Hi, Tom.

228
00:22:07,360 --> 00:22:09,360
Hi, Bata.

229
00:22:09,360 --> 00:22:11,360
Do you have a question?

230
00:22:11,360 --> 00:22:18,360
Wow.

231
00:22:18,360 --> 00:22:20,360
All right.

232
00:22:20,360 --> 00:22:21,360
Nice to see you.

233
00:22:21,360 --> 00:22:27,360
Good to see you here.

234
00:22:27,360 --> 00:22:30,360
Thank you, Bante.

235
00:22:30,360 --> 00:22:31,360
All right.

236
00:22:31,360 --> 00:22:31,360
Welcome.

237
00:22:31,360 --> 00:22:32,360
Have a good night, everyone.

238
00:22:32,360 --> 00:22:47,360
Good morning, Bata.

