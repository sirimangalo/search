1
00:00:00,000 --> 00:00:28,560
Okay, I... I'd like to continue talking about the manganese with the third year blessings

2
00:00:28,560 --> 00:00:32,560
and start by the word Buddha.

3
00:00:40,560 --> 00:00:50,560
We've gotten through several days already. I had a bunch of town

4
00:00:50,560 --> 00:01:00,560
and now I'd like to continue hearing it.

5
00:01:00,560 --> 00:01:08,560
Now I've got a little bit of tension and something in the home side.

6
00:01:08,560 --> 00:01:18,560
Just to recap, the mongolus with the discourse on blessings was taught in

7
00:01:18,560 --> 00:01:23,560
the one that...

8
00:01:23,560 --> 00:01:25,560
Mahawiyara.

9
00:01:25,560 --> 00:01:30,560
He saw it in India.

10
00:01:30,560 --> 00:01:35,560
From one night,

11
00:01:35,560 --> 00:01:41,560
at the very end of the night, darkest part of the night.

12
00:01:41,560 --> 00:01:48,560
There's suddenly a rose, a great light, in jitha warmer,

13
00:01:48,560 --> 00:01:55,560
covering a light covering the whole of the park.

14
00:01:55,560 --> 00:01:57,560
There's such a thing where to come to our center.

15
00:01:57,560 --> 00:02:01,560
We might be quite excited and surprised.

16
00:02:01,560 --> 00:02:05,560
We might think it was a UFO,

17
00:02:05,560 --> 00:02:12,560
we might be quite agitated by it to see such a sight.

18
00:02:12,560 --> 00:02:14,560
But in the time of the Buddha,

19
00:02:14,560 --> 00:02:21,560
this was a very common scene in jitha warmer.

20
00:02:21,560 --> 00:02:31,560
Because all of the angels wanted to come down and hear the teaching of the Lord Buddha.

21
00:02:31,560 --> 00:02:36,560
Even today we make an invitation to the angels when we teach the Dhamma.

22
00:02:36,560 --> 00:02:43,560
Before we do it, we're reciting of the Dhamma.

23
00:02:43,560 --> 00:02:46,560
We invite the angels to listen,

24
00:02:46,560 --> 00:02:52,560
because it was such a rare thing to hear.

25
00:02:52,560 --> 00:02:56,560
The angels are being very special in the angels.

26
00:02:56,560 --> 00:03:00,560
Many of them look quite interested to hear the teaching of the Lord Buddha.

27
00:03:00,560 --> 00:03:09,560
Then this angel came down having lit up the whole of jitha warmer.

28
00:03:09,560 --> 00:03:15,560
As the Lord Buddha, what are the blessings?

29
00:03:15,560 --> 00:03:23,560
Of course, in that time everyone said this was a blessing or that was a blessing.

30
00:03:23,560 --> 00:03:29,560
Some people thought it was a blessing was to have lots of cows.

31
00:03:29,560 --> 00:03:41,560
Some people thought it was a blessing when you heard an owl in the morning.

32
00:03:41,560 --> 00:03:45,560
When you had a horseshoe or when you had a rabbit's foot,

33
00:03:45,560 --> 00:03:52,560
that these things were good luck, auspicious.

34
00:03:52,560 --> 00:03:58,560
The blessings of the Lord Buddha have quite different for moments.

35
00:03:58,560 --> 00:04:07,560
Now we get to the verse that goes,

36
00:04:07,560 --> 00:04:13,540
and ha-machar-ya-ta-ta-ta-ta-ta-ta-ta-ta-ta-ta-ta-ta-ta-ta-ta-ta-ta-ta-ta-ta-ta-ta.

37
00:04:13,540 --> 00:04:14,040
And now, what?

38
00:04:14,040 --> 00:04:17,040
Nika-mani-ta-mam-kala-makimam.

39
00:04:17,040 --> 00:04:19,540
No.

40
00:04:19,540 --> 00:04:22,240
Tan-antak.

41
00:04:22,240 --> 00:04:27,960
Tan-mani-ta-ta means charity.

42
00:04:27,960 --> 00:04:29,320
Tama-ta-ta-ta-ta.

43
00:04:29,320 --> 00:04:30,320
Tama-ta-ta-ta-ta-ta.

44
00:04:30,320 --> 00:04:35,480
Tama-ta-ta-ta-ta is the practice of tama-ta-mak.

45
00:04:35,480 --> 00:04:44,400
I'm living by the tama-ta-ta-ta-ta-ta-ta-ta-ta-ta-ta-ta.

46
00:04:44,400 --> 00:04:49,000
To be helpful towards one's relatives.

47
00:04:49,000 --> 00:04:55,540
Nama-ta-ta-ta-ta-ta-ta-ta-ta-ta-ta-ta-ta-ta-ta.

48
00:04:55,540 --> 00:04:57,280
To have deeds which are blameless.

49
00:04:57,280 --> 00:04:59,200
To have actions which are blameless.

50
00:04:59,200 --> 00:05:00,440
Blameless action.

51
00:05:00,440 --> 00:05:02,820
Eight-tama-tama-tama-makimam-tamam.

52
00:05:02,820 --> 00:05:03,920
This is the blessing.

53
00:05:03,920 --> 00:05:06,320
This is this is a blessing in the highest sense

54
00:05:08,720 --> 00:05:10,220
Danna

55
00:05:10,220 --> 00:05:12,220
Good charity

56
00:05:12,520 --> 00:05:16,440
Charity means to give something, which is what you have

57
00:05:18,000 --> 00:05:20,560
To someone else who might find a useful

58
00:05:22,600 --> 00:05:28,240
It's a way of giving up our greed and our stinginess and our attachment to self

59
00:05:28,240 --> 00:05:40,440
When we give it's for not just for the benefit of a person who receives

60
00:05:41,840 --> 00:05:46,120
Don't do it when we do it. We should think of it as a benefit for ourselves as well

61
00:05:47,480 --> 00:05:52,280
If it helps us to destroy our attachment to ourselves and attachment to

62
00:05:52,280 --> 00:05:56,440
To our possessions and so on

63
00:05:58,480 --> 00:06:00,480
And Danna is of two kinds

64
00:06:01,200 --> 00:06:05,440
Danna is a misadonna and a madonna

65
00:06:07,000 --> 00:06:13,240
First there are many kinds we'll keep it short. We'll say there are two kinds. A misadonna is

66
00:06:14,840 --> 00:06:16,840
When you give objects

67
00:06:18,000 --> 00:06:20,000
things that you own

68
00:06:20,000 --> 00:06:22,000
You have something

69
00:06:23,920 --> 00:06:25,920
Useful

70
00:06:25,920 --> 00:06:30,640
Normally we have we have it's a base for things when we give one we give

71
00:06:32,000 --> 00:06:34,000
clothing to other people

72
00:06:35,400 --> 00:06:37,400
Two we give food to other people

73
00:06:39,560 --> 00:06:42,080
Three we give shelter to other people and four

74
00:06:43,760 --> 00:06:45,760
We give medicines to other people

75
00:06:45,760 --> 00:06:51,280
These four things are things which which people really can't do without even monks and

76
00:06:51,840 --> 00:06:57,920
Nans we have these four requisites and every day we do chanting to remind ourselves that we should use these

77
00:06:59,120 --> 00:07:01,120
requisites properly

78
00:07:01,520 --> 00:07:03,520
To give these things is a great blessing

79
00:07:04,720 --> 00:07:10,720
It makes the person who gives feel happy it creates friendship between the first two people

80
00:07:10,720 --> 00:07:16,480
It creates love and it creates harmony and it creates happiness in the world

81
00:07:18,480 --> 00:07:23,600
So this is considered to be a great blessing, but the second kind of gift is called Tama Danna

82
00:07:25,680 --> 00:07:27,680
It's the gift of the truth

83
00:07:29,360 --> 00:07:32,440
To give a gift of the teaching of the enlightened ones

84
00:07:32,440 --> 00:07:38,680
You see Zapatana and Tama Tana and Tsunaki

85
00:07:39,800 --> 00:07:43,000
The gift of the truth is greater than all other gifts

86
00:07:47,640 --> 00:07:53,000
So I I've always encouraged other people when they when someone comes to understand the truth

87
00:07:53,720 --> 00:07:55,720
It's also very important that we go and

88
00:07:56,440 --> 00:08:00,760
Share this gift with other people not to be stingy and just keep it for ourselves

89
00:08:00,760 --> 00:08:04,440
We'll come and meditate so then we got a benefit and

90
00:08:05,160 --> 00:08:07,880
We're going to go back and share it with anyone

91
00:08:10,680 --> 00:08:14,200
When you realize what is what is the truth when you come to understand

92
00:08:14,760 --> 00:08:20,760
In permanence you come to understand why we suffer all the time where we have this suffering or that suffering

93
00:08:21,720 --> 00:08:25,560
You come to let go of things you have to teach other people how to that go as well

94
00:08:26,200 --> 00:08:28,200
If you want to really be happy

95
00:08:28,200 --> 00:08:31,000
You have to bring happiness to other people

96
00:08:34,840 --> 00:08:38,680
This doesn't mean you just go around teaching everybody else and teaching

97
00:08:41,160 --> 00:08:44,200
Teaching people who don't want to hear sometimes people are not interested

98
00:08:45,000 --> 00:08:47,560
They don't really want to hear what you have to say

99
00:08:49,320 --> 00:08:51,320
You have to

100
00:08:51,960 --> 00:08:55,720
You have to give it to people who want to want something from you

101
00:08:55,720 --> 00:08:57,720
And someone asks you to teach

102
00:08:59,800 --> 00:09:01,800
Teach them

103
00:09:02,440 --> 00:09:05,720
This is in brief. This is a description of them and

104
00:09:05,720 --> 00:09:07,720
Danna

105
00:09:07,720 --> 00:09:11,320
When we give we should give to suitable people people who are appropriate to give to

106
00:09:12,920 --> 00:09:15,720
When we see that someone is in need of something

107
00:09:16,440 --> 00:09:19,640
We really don't understand this so they don't understand that we can teach them

108
00:09:20,440 --> 00:09:23,400
Or maybe they don't have any food or they don't have any shelter

109
00:09:23,400 --> 00:09:26,600
So you can keep them food or give them shelter or so

110
00:09:27,320 --> 00:09:29,320
Medicine when they need so

111
00:09:30,040 --> 00:09:32,280
This is the first this is a great blessing

112
00:09:32,760 --> 00:09:36,280
I'm blessing in the ultimate sense much more of a blessing than cows

113
00:09:36,840 --> 00:09:39,880
If you have lots of cows not a real intro blessing

114
00:09:41,240 --> 00:09:44,760
But when you give away your cows, then you can consider this a blessing

115
00:09:45,560 --> 00:09:49,000
And you give away things that you that other people might find useful

116
00:09:49,000 --> 00:09:51,000
I

117
00:09:52,600 --> 00:09:54,600
Tama Tariya it's a

118
00:09:54,600 --> 00:09:56,600
It's the practice of the dhamma

119
00:09:57,880 --> 00:09:59,880
Living by the dhamma

120
00:10:01,320 --> 00:10:07,160
The dhamma of the Lord Buddha the 84,000 teachings. I can't explain all of how we how we practice all the dhamma

121
00:10:07,640 --> 00:10:11,320
Even the mongala sutta it takes many days just one sutta

122
00:10:12,120 --> 00:10:14,120
Takes many days to explain

123
00:10:14,120 --> 00:10:18,760
How to practice all of these blessings and I have these blessings

124
00:10:21,160 --> 00:10:23,160
But we have some guidelines as to

125
00:10:25,800 --> 00:10:31,400
What is the dhamma? What is the windy of the Lord Buddha? What is the teaching of the Lord Buddha of the enlightened one?

126
00:10:36,360 --> 00:10:38,360
The characteristics of the dhamma are

127
00:10:39,720 --> 00:10:42,040
It's for for

128
00:10:42,040 --> 00:10:44,040
Giving up of craving

129
00:10:45,960 --> 00:10:49,960
Whatever it is that leads us to give up craving you can say that is the dhamma of the Lord Buddha

130
00:10:52,040 --> 00:10:57,640
Whatever leads us to be content with what we have or content even with little when we only have a very little to be

131
00:10:58,040 --> 00:11:00,040
When we can be content

132
00:11:00,600 --> 00:11:02,600
Whatever leads to contentment

133
00:11:04,600 --> 00:11:07,000
Whatever leads to give up suffering

134
00:11:07,000 --> 00:11:13,480
Whatever leads us to give up suffering. This is the dhamma of the Lord Buddha

135
00:11:17,640 --> 00:11:19,640
Whatever leads us to

136
00:11:21,720 --> 00:11:23,720
Develop effort

137
00:11:24,440 --> 00:11:27,480
To be able to work harder to be

138
00:11:29,000 --> 00:11:31,000
More energetic

139
00:11:31,000 --> 00:11:38,280
Whatever leads us to put out effort. This is the teaching of the Lord Buddha. Whatever makes us easy to

140
00:11:41,000 --> 00:11:42,760
Easy to satisfy and so on

141
00:11:44,920 --> 00:11:47,560
Whatever leads us to nibana to freedom from suffering

142
00:11:52,120 --> 00:11:59,000
We want to put it all in brief. We can summarize the dhamma of the Buddha as the four foundations of mindfulness

143
00:11:59,000 --> 00:12:03,400
When we practice mindfulness in the body watching the

144
00:12:04,600 --> 00:12:06,600
The feet watching the hands

145
00:12:06,920 --> 00:12:08,920
Watching the stomach rising

146
00:12:10,040 --> 00:12:12,280
This is the teaching of this is the dhamma

147
00:12:17,880 --> 00:12:22,760
This is the practice. We should all feel very proud that some people they never come to practice that I'm the only

148
00:12:22,760 --> 00:12:31,400
Listen and study the dhamma, but when it comes to practicing, they never had the thought that they should come and actually practice

149
00:12:34,600 --> 00:12:37,800
So here we're coming in practice all the time rising

150
00:12:38,840 --> 00:12:40,840
walking

151
00:12:40,840 --> 00:12:49,800
We practice mindfulness in the feelings when we have pain or aching or so we say pain

152
00:12:52,760 --> 00:12:54,760
We feel happy

153
00:13:00,120 --> 00:13:02,120
Thinking about the past or future

154
00:13:02,120 --> 00:13:09,880
Thinking and liking this liking grouseiness distraction doubt and knowledge liking

155
00:13:11,960 --> 00:13:13,960
Growsing

156
00:13:15,640 --> 00:13:17,640
This is the teaching of the Lord Buddha

157
00:13:21,720 --> 00:13:26,280
We practice this. This is a great blessing for us. It leads us to see it clearly

158
00:13:26,280 --> 00:13:33,320
Both inside of ourselves and in the world around us and it leads us to find freedom

159
00:13:34,200 --> 00:13:36,040
Everyone wants to be free and

160
00:13:37,880 --> 00:13:43,480
It leads us to peace and everyone wants to have peace. It's just a happiness everyone wants to be happy

161
00:13:43,480 --> 00:13:52,120
Yeah, dependent just on goal

162
00:13:53,800 --> 00:13:56,840
We have to help our be helpful towards our relatives

163
00:13:58,120 --> 00:14:02,360
Our relatives are not going to go into great detail. It's obvious that when we live in the world

164
00:14:02,360 --> 00:14:05,320
We have to be helpful towards our relatives never give them up

165
00:14:05,960 --> 00:14:08,200
Never throw away or forget about our relatives

166
00:14:08,920 --> 00:14:10,920
Even monks have to rely on their relatives

167
00:14:10,920 --> 00:14:17,480
Everyone person who has relatives is a very special thing

168
00:14:18,200 --> 00:14:21,800
But here when we're in the dhamma we have to consider each other to be relatives

169
00:14:22,440 --> 00:14:27,400
We have to rely on each other. How do we rely on each other? Sometimes we can teach each other

170
00:14:28,200 --> 00:14:29,800
right way

171
00:14:29,960 --> 00:14:32,840
But most important is we give a supportive environment

172
00:14:32,840 --> 00:14:40,280
That we invite people here to practice make it a suitable environment to practice

173
00:14:41,160 --> 00:14:43,160
and everyone take on

174
00:14:43,800 --> 00:14:47,640
The practice of the dhamma in the wind. They have to keep certain rules

175
00:14:50,600 --> 00:14:54,120
They have to make it conducive to follow these rules

176
00:14:56,520 --> 00:15:00,200
We have to support each other to follow the rules that we have in place

177
00:15:00,200 --> 00:15:03,560
We have to support each other to practice the dhamma which we have

178
00:15:04,280 --> 00:15:06,280
Which we teach

179
00:15:06,840 --> 00:15:10,520
When we see even we see each other practicing we see each other

180
00:15:14,840 --> 00:15:18,600
Keeping to the practice which we follow them. It's a great support for us

181
00:15:19,720 --> 00:15:23,880
If we just had to go and practice off in the forest by ourselves it may be very difficult

182
00:15:24,600 --> 00:15:26,600
We don't have the support of our

183
00:15:26,600 --> 00:15:31,400
Relatives. We can say we're sisters and brothers in the dhamma

184
00:15:34,040 --> 00:15:36,040
This is a very important. It's a great blessing

185
00:15:37,000 --> 00:15:41,640
You can meet as immeditators have even expressed their feeling of how

186
00:15:41,640 --> 00:15:57,400
How great it is to practice in the group how much support it gives to you when you practice together

187
00:15:57,400 --> 00:16:06,680
And that's what tani kamani is means that whatever we do

188
00:16:06,680 --> 00:16:11,160
Whatever we do has to be free from blame

189
00:16:12,520 --> 00:16:15,880
Free from treachery free from bad intentions

190
00:16:21,080 --> 00:16:24,920
That are actions before we speak before we act

191
00:16:28,280 --> 00:16:30,280
We have to be sure that our mind is pure

192
00:16:31,960 --> 00:16:34,520
We should never act or speak with a heart which is

193
00:16:34,520 --> 00:16:39,320
Covered over with a good beat or with anger

194
00:16:41,400 --> 00:16:43,400
With delusion

195
00:16:44,040 --> 00:16:46,840
When we speak or when we act our minds should be clear

196
00:16:48,360 --> 00:16:52,120
We should know that this is for the benefit of ourselves and the benefit of the other person

197
00:16:52,680 --> 00:16:56,600
This is for happiness and for peace for freedom from suffering

198
00:16:58,760 --> 00:17:02,520
This is not in order to bring hardship to the other person to bring suffering

199
00:17:02,520 --> 00:17:05,240
The other person who's suffering to anyone

200
00:17:11,480 --> 00:17:13,640
We can see how important it is to be mindful

201
00:17:14,760 --> 00:17:18,840
It's very easy to forget and to say and to do things which are

202
00:17:19,960 --> 00:17:21,960
a cause for suffering

203
00:17:23,080 --> 00:17:26,440
There's no need to hate ourselves for bad things we've done but

204
00:17:26,440 --> 00:17:31,720
We only took to wake up wake up wake ourselves up

205
00:17:33,000 --> 00:17:35,000
to bring ourselves to see

206
00:17:37,240 --> 00:17:43,240
Everything that we do to know before we act before we speak is this a good thing to say a good thing to do

207
00:17:45,000 --> 00:17:47,000
To have mindfulness

208
00:17:48,520 --> 00:17:51,000
We haven't trained ourselves every moment to be mindful

209
00:17:51,000 --> 00:17:56,840
Because when we practice in this way we can see when anger arises we can see when greed arises

210
00:18:00,280 --> 00:18:02,600
At least when we practice then we can

211
00:18:04,360 --> 00:18:08,200
Cut off the link between our bad intentions and our actions

212
00:18:09,400 --> 00:18:12,520
So that when we act it will be free from greed of freedom and anger

213
00:18:13,800 --> 00:18:16,280
And then when we take this into our life we can also

214
00:18:16,280 --> 00:18:21,080
And act and speak in a way that is free from

215
00:18:22,920 --> 00:18:24,920
From evil and then to from bad intention

216
00:18:27,320 --> 00:18:28,360
This is a great blessing

217
00:18:30,280 --> 00:18:32,760
It leads us to be free from bad karma

218
00:18:33,800 --> 00:18:36,680
And free from the results of bad karma, free from guilt

219
00:18:39,080 --> 00:18:41,400
Free from suffering

220
00:18:41,400 --> 00:18:46,200
Since we are truly mindful we can be free from suffering

221
00:18:49,320 --> 00:18:51,320
So these are the

222
00:18:51,800 --> 00:18:53,800
And the latest set of blessings

223
00:18:53,800 --> 00:19:11,320
Among the 38 blessings

