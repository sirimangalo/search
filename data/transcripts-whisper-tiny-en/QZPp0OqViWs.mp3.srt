1
00:00:00,000 --> 00:00:08,640
Hi, welcome back to Ask a Monk. Today's question is from Isanim.

2
00:00:08,640 --> 00:00:14,360
Meditation seems investigative and empirical, but Buddhist texts claim supernatural entities

3
00:00:14,360 --> 00:00:20,600
realms and laws exist. Are these claims relevant to Buddhist practice today? How do you

4
00:00:20,600 --> 00:00:26,280
reconcile seeing things as they are with taking things on faith?

5
00:00:26,280 --> 00:00:34,840
Well, first a note that as I understand the word supernatural, Buddhist texts don't claim

6
00:00:34,840 --> 00:00:40,720
the existence of anything supernatural. What Buddhist texts claim is that these entities

7
00:00:40,720 --> 00:00:49,840
realms and laws are perfectly natural. And the claim is then that the laws of physics

8
00:00:49,840 --> 00:00:59,760
are only a small part of the natural reality. And this is something that we often take

9
00:00:59,760 --> 00:01:07,400
on faith, and I want to explain how we answer this idea of taking things on faith versus

10
00:01:07,400 --> 00:01:18,840
seeing things as they are. There are two answers to it. The first one obviously is that

11
00:01:18,840 --> 00:01:23,880
when we understand these things to be natural, we are not taking them on faith and we are

12
00:01:23,880 --> 00:01:34,440
investigating them. So much of the claims that seem to be supernatural or are extraordinary

13
00:01:34,440 --> 00:01:41,440
in Buddhism are actually investigateable that you can empirically investigate things

14
00:01:41,440 --> 00:01:47,400
like past lives, you can investigate heaven, you can investigate magical powers, so-called

15
00:01:47,400 --> 00:01:54,040
magical powers. All of these super mundane or extraordinary experiences and realities

16
00:01:54,040 --> 00:01:59,800
can all be investigated through practice. And so the reason why we might take those things

17
00:01:59,800 --> 00:02:06,880
on faith is in the beginning is because we are studying to realize some of them. Now

18
00:02:06,880 --> 00:02:11,960
obviously most of these things are not necessary to become free from suffering or to see

19
00:02:11,960 --> 00:02:17,640
things clearly or to reach the goal of Buddhism, but there are many people who do practice

20
00:02:17,640 --> 00:02:24,360
to realize them. And there are many things which are a part of the path as far as gaining

21
00:02:24,360 --> 00:02:29,920
high states of concentration. And even things like remembering one's past lives could

22
00:02:29,920 --> 00:02:36,760
be helpful. Although none of these things are necessary for the path, but we take them

23
00:02:36,760 --> 00:02:42,240
on faith. We can take them on faith and if we are interested in them, then we take them

24
00:02:42,240 --> 00:02:52,080
on faith in the same way as a physics student. We'll take on the theories and laws proposed

25
00:02:52,080 --> 00:02:57,040
by those above them, those who have come before them, in the well they study them and

26
00:02:57,040 --> 00:03:01,920
well they examine them and do experiments on them and well they try to understand them.

27
00:03:01,920 --> 00:03:09,000
And many of these things, once you come to understand the basic building blocks of reality

28
00:03:09,000 --> 00:03:14,120
and you come to realize that the best way to approach what is the meaning of reality or

29
00:03:14,120 --> 00:03:19,560
the best way to understand reality is from a basis of the mind, basis of consciousness

30
00:03:19,560 --> 00:03:25,080
and that our consciousness actually creates our reality which is perfectly in line with

31
00:03:25,080 --> 00:03:31,680
the laws of quantum physics for example. Then you start to open up to such things

32
00:03:31,680 --> 00:03:36,200
and start to realize that these things are perfectly in line with reality. There's no

33
00:03:36,200 --> 00:03:40,440
reason why they shouldn't be able to exist, though we may not have proof we can take them

34
00:03:40,440 --> 00:03:49,720
on faith in the meantime or we can suspend our judgment and then practice to realize them.

35
00:03:49,720 --> 00:03:57,360
I think the other more important and more immediately applicable or practical answer to this

36
00:03:57,360 --> 00:04:01,800
question is that we take a lot of these things on faith because they're not of much meaning

37
00:04:01,800 --> 00:04:09,000
to us. For instance when someone asks said, do you believe in Australia? And if you're

38
00:04:09,000 --> 00:04:13,120
from America you've probably never been to Australia so you can say yes I believe in Australia

39
00:04:13,120 --> 00:04:17,840
and you've never been there you've never seen it. And we can say well I believe people

40
00:04:17,840 --> 00:04:21,960
because they've been there and they've told me about it but we also believe it because

41
00:04:21,960 --> 00:04:26,240
we don't have any reason not to believe it. We don't have any reason to be concerned

42
00:04:26,240 --> 00:04:31,560
about its existence or non-existence. If we're wrong it doesn't influence our life in any

43
00:04:31,560 --> 00:04:42,040
way. It doesn't mean anything for the daily. It isn't a big problem for us if we turn

44
00:04:42,040 --> 00:04:45,640
out to be wrong. So we say yeah I believe these people they've said that and we don't

45
00:04:45,640 --> 00:04:49,960
investigate it because we say to ourselves if it's wrong then it's not a big deal.

46
00:04:49,960 --> 00:04:54,920
So most of these things the belief in gods, the belief in angels, the belief in other realms

47
00:04:54,920 --> 00:05:01,440
of existence and so on. On the one hand they make sense from the point of view of the

48
00:05:01,440 --> 00:05:07,200
creation of existence or the progress of existence based on the mind that if we can exist

49
00:05:07,200 --> 00:05:13,560
as human beings there's nothing special about this reality that makes it more likely than

50
00:05:13,560 --> 00:05:19,360
say the angel realms or so on. So we can understand it in terms of the probability of

51
00:05:19,360 --> 00:05:23,560
its existence. But then we can also say well even if they don't exist doesn't mean that

52
00:05:23,560 --> 00:05:29,200
much to me. I see that the meditation practice is helping me and I'm here to practice meditation

53
00:05:29,200 --> 00:05:34,760
and I'm following the Buddha's teaching not specifically because maybe one day I'll gain

54
00:05:34,760 --> 00:05:40,040
magic powers or because I've been born before or so on and so on. But because when I practice

55
00:05:40,040 --> 00:05:46,240
meditation I see that it does good things for me. I see that it helps me and so we don't

56
00:05:46,240 --> 00:05:51,080
worry or concern too much ourselves too much about these things. So I would say that's two

57
00:05:51,080 --> 00:05:55,480
ways of answering this question. On the one hand they're not that supernatural and they're

58
00:05:55,480 --> 00:06:01,000
not that hard to understand if you practice meditation. And on the other hand they're

59
00:06:01,000 --> 00:06:07,560
not of that big of that great importance for us. It's important for us to have some sort

60
00:06:07,560 --> 00:06:14,640
of willingness to investigate some of the claims that the Buddha made in terms of the cause

61
00:06:14,640 --> 00:06:20,160
of suffering and the nature of the mind so that we can then experiment and say is it true

62
00:06:20,160 --> 00:06:24,080
is it real and come to see it clearly. But there's many things that the Buddha said that

63
00:06:24,080 --> 00:06:28,960
we may choose not to investigate and we may just take on faith and say you know if it

64
00:06:28,960 --> 00:06:34,600
were a big deal for me I'd investigate it. But for my benefit for my practice it's not

65
00:06:34,600 --> 00:06:40,720
that important. So I hope that helps to clear up the or my understanding of the Buddha's

66
00:06:40,720 --> 00:06:57,600
position on these issues. Thanks for the question.

