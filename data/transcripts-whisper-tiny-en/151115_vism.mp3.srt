1
00:00:00,000 --> 00:00:13,360
Hi, and welcome to our Visudimaga class, or anyone who's new this week, we just go right

2
00:00:13,360 --> 00:00:20,680
down through and take turn to reading, and we're on chapter 10 on page 321, starting at

3
00:00:20,680 --> 00:00:25,520
the beginning of the chapter.

4
00:00:25,520 --> 00:00:30,480
And I'll see on, I'm not sure if you might set up, are you able to read, or would you prefer

5
00:00:30,480 --> 00:00:33,280
to listen this week?

6
00:00:33,280 --> 00:00:42,080
Okay, that's fine, we're glad you're here, Aurora, can you start us off?

7
00:00:42,080 --> 00:00:44,280
Yes, I can.

8
00:00:44,280 --> 00:00:48,920
Now, as to the four immaterial states mentioned next to the divine abidings.

9
00:00:48,920 --> 00:00:54,240
One who wants, firstly, to develop the base consisting of boundless space, sees and grows

10
00:00:54,240 --> 00:00:59,640
physical matter, danger through the wielding of sticks, et cetera, because of the words,

11
00:00:59,640 --> 00:01:05,240
it is in virtue of matter, that wielding of sticks, wielding of knives, corals, brawls in

12
00:01:05,240 --> 00:01:11,760
this huge stake place, but that does not exist at all in the immaterial state.

13
00:01:11,760 --> 00:01:18,480
And in this expectation, he enters upon the way to dispassion for only material things,

14
00:01:18,480 --> 00:01:21,560
for the fading and cessation of only those.

15
00:01:21,560 --> 00:01:27,480
And he sees danger in it, through the thousand of afflictions beginning with eye disease.

16
00:01:27,480 --> 00:01:32,080
So in order to surmount that, he enters upon the fourth genre, in any one of the nine

17
00:01:32,080 --> 00:01:38,840
casinos beginning with the earth casino, and emitting the limited space casino.

18
00:01:38,840 --> 00:01:47,040
Now, also, if he has already surmounted rose physical matter by means of the fourth

19
00:01:47,040 --> 00:01:56,320
genre of the fine material sphere, nevertheless, he still wants also to surmount the casino

20
00:01:56,320 --> 00:02:01,960
materiality, since it is a counterpart of the form.

21
00:02:01,960 --> 00:02:06,800
How does he do this?

22
00:02:06,800 --> 00:02:11,640
Suppose a timid man is pursued by a snake in a forest and pleased from it as fast as

23
00:02:11,640 --> 00:02:18,000
he can, then if he sees in the place he has fled to a palm leaf with a streak painted on

24
00:02:18,000 --> 00:02:23,000
it or a creeper or a rope or crack in the ground, he is fearful, anxious, and will not even

25
00:02:23,000 --> 00:02:24,000
look at it.

26
00:02:24,000 --> 00:02:30,240
Suppose again, a man is living in the same village as a hostile man who ill uses him and

27
00:02:30,240 --> 00:02:35,080
unbeing threatened by him with a clogging and the burning down of his house, he goes away

28
00:02:35,080 --> 00:02:37,160
to live in another village.

29
00:02:37,160 --> 00:02:42,200
Even if he meets another man there of a similar appearance, voice and manner, he is fearful,

30
00:02:42,200 --> 00:02:45,240
anxious, and will not even look at him.

31
00:02:45,240 --> 00:02:50,400
He is the application of this man.

32
00:02:50,400 --> 00:02:57,800
The time when the beaker has a girl's physical matter as his object is like the time when

33
00:02:57,800 --> 00:03:06,960
the men were respectively threatened by the snake and by the enemy.

34
00:03:06,960 --> 00:03:15,680
The time when the beaker surmounts a girl's physical matter by means of the false jana of

35
00:03:15,680 --> 00:03:24,160
the fine material sphere is like the fierce man fleeing as fast as he can and the other

36
00:03:24,160 --> 00:03:28,200
man is going away to another village.

37
00:03:28,200 --> 00:03:35,320
The beaker subsurbing that even the matter of the casino is the counterpart of that

38
00:03:35,320 --> 00:03:42,240
girl's physical matter and he is quanting to surmount that also is like the fierce

39
00:03:42,240 --> 00:03:55,920
man fleeing in the place he had.

40
00:03:55,920 --> 00:04:04,720
To the palm leaves, reflect to the palm leaves with the strict painting on it etc and the

41
00:04:04,720 --> 00:04:13,920
other man fleeing the man who resembles the enemy in the village he had left and the

42
00:04:13,920 --> 00:04:26,560
wounded witness to look all into fear and anxiety and hear the smile of the duck attacked

43
00:04:26,560 --> 00:04:40,120
by a boar and that of the pisakka going and the mind and the teeny man should be understood

44
00:04:40,120 --> 00:04:41,120
too.

45
00:04:41,120 --> 00:04:50,000
I did not know the work out once in the city of Maga.

46
00:04:50,000 --> 00:04:56,080
So when he has thus become disgusted with dispassionate towards the casino materiality,

47
00:04:56,080 --> 00:05:01,840
the object of the false jana and wants to get away from it he achieves mastery in the

48
00:05:01,840 --> 00:05:03,960
five ways.

49
00:05:03,960 --> 00:05:10,160
Then on emerging from the now familiar fourth jana of the fine materials view, he sees

50
00:05:10,160 --> 00:05:13,880
the danger in that jana in this way.

51
00:05:13,880 --> 00:05:20,240
This makes its object the materiality with which I have become disgusted and it has

52
00:05:20,240 --> 00:05:27,120
joy as its near enemy and it is grosser than the peaceful liberation.

53
00:05:27,120 --> 00:05:33,440
There is however no comparative grossness of factors here as in the case of the four fine

54
00:05:33,440 --> 00:05:35,800
material janas.

55
00:05:35,800 --> 00:05:46,240
For the immaterial states have the same two factors as this fine material jana.

56
00:05:46,240 --> 00:05:51,960
When he has seen the danger in that fine material fourth jana, jana in this way and has

57
00:05:51,960 --> 00:05:56,520
ended his attachment to it, he gives his attention to the base consisting of boundless

58
00:05:56,520 --> 00:05:58,560
spaces peaceful.

59
00:05:58,560 --> 00:06:02,880
Then when he has spread out the casino to the limits of the world's fear or as far

60
00:06:02,880 --> 00:06:09,080
as he likes, he removes the casino materiality by giving his attention to the space touched

61
00:06:09,080 --> 00:06:15,920
by it regarding that as space or boundless space.

62
00:06:15,920 --> 00:06:21,520
Then he is removing it, he neither falls it up like a mat, no it knows it like a cake

63
00:06:21,520 --> 00:06:30,480
from a tin, it is simply that he does not advert to it or give attention to it or review

64
00:06:30,480 --> 00:06:37,120
it, it is when he needs the adverts to it, no gives attention to it, no reviews it.

65
00:06:37,120 --> 00:06:45,400
But gives his attention exclusively to this exposing to the space touched by it, regarding

66
00:06:45,400 --> 00:07:00,320
that as space space that he is set to remove the casino.

67
00:07:00,320 --> 00:07:05,640
And when the casino is being removed, it does not roll up or roll away, it is simply

68
00:07:05,640 --> 00:07:10,600
that it is called removed on account of his non-attention to it, his attention being given

69
00:07:10,600 --> 00:07:13,360
to space space.

70
00:07:13,360 --> 00:07:21,120
This is conceptualized as the mere space left by the removal of the casino or materiality.

71
00:07:21,120 --> 00:07:25,680
Whether it is called space left by the removal of the casino or space touched by the

72
00:07:25,680 --> 00:07:34,000
casino or space occluded from the casino, it is all the same.

73
00:07:34,000 --> 00:07:39,000
The adverts again and again to the sign of the space left by the removal of the casino

74
00:07:39,000 --> 00:07:44,520
as space space and strikes at it with thought and applied thought.

75
00:07:44,520 --> 00:07:49,520
As he adverts to it again and again and strikes at it with thought and applied thought,

76
00:07:49,520 --> 00:07:54,720
the hindrances are suppressed, mindfulness is established and his mind becomes concentrated

77
00:07:54,720 --> 00:07:55,720
in access.

78
00:07:55,720 --> 00:08:06,280
He cultivates that sign again and again, develops and repeatedly practices it.

79
00:08:06,280 --> 00:08:12,760
As he again and again adverts to it and gives attention to it in this way, consciousness

80
00:08:12,760 --> 00:08:20,400
belonging to the base consisting of fondness and space arises in absorption with the space

81
00:08:20,400 --> 00:08:22,720
as its object.

82
00:08:22,720 --> 00:08:28,600
As the consciousness belonging to the fine material sphere did in the case of the air

83
00:08:28,600 --> 00:08:31,280
casino and so on.

84
00:08:31,280 --> 00:08:38,160
And here too in the prior stage there are either three or four central spheres, the sphere

85
00:08:38,160 --> 00:08:48,200
inclusions associated with a quantum filling while the four or the fifth is of the immaterial

86
00:08:48,200 --> 00:08:49,360
sphere.

87
00:08:49,360 --> 00:08:53,600
The rest is the same as in the case of the air casino.

88
00:08:53,600 --> 00:09:03,320
That is, however, this difference, when the immaterial sphere consciousness has arisen

89
00:09:03,320 --> 00:09:10,780
in this way, the vehicle who has been formally looking at the casino disk with the

90
00:09:10,780 --> 00:09:20,160
Janae fine skin self looking at the only space after that sign has been abruptly removed

91
00:09:20,160 --> 00:09:28,400
by the attention given in the preliminary work of space space.

92
00:09:28,400 --> 00:09:39,240
He is like a man who has blocked an opening in a covert vehicle, a sack or a pot with

93
00:09:39,240 --> 00:09:50,120
a piece of blue rack or with a piece of a rack of some social color as jello, red or white

94
00:09:50,120 --> 00:09:59,240
and if looking at that and then when the rack is removed by the force of the wind or by

95
00:09:59,240 --> 00:10:07,480
some other agency he finds himself looking at space.

96
00:10:07,480 --> 00:10:16,480
Rest and commentary and at this point it is said, with the complete surmounting, sama tikama

97
00:10:16,480 --> 00:10:22,840
of perceptions of matter, with the disappearance of perceptions of resistance, with

98
00:10:22,840 --> 00:10:30,700
non-attentioned perceptions of variety, aware of unbounded space, he enters upon and

99
00:10:30,700 --> 00:10:38,680
wells in the base consisting of boundless space.

100
00:10:38,680 --> 00:10:46,200
Here in, complete is in all aspects or of all perceptions without exception is the meaning

101
00:10:46,200 --> 00:10:51,320
of perceptions of matter, both A of the fine material Janae's mentioned here under the

102
00:10:51,320 --> 00:10:56,520
heading of perception and B of those things that are their object.

103
00:10:56,520 --> 00:11:02,760
For A, the Janae of the fine material sphere is called matter, in such passages as possessed

104
00:11:02,760 --> 00:11:08,040
of visible matter, he sees instances of matter.

105
00:11:08,040 --> 00:11:14,040
And B, it is its object 2, that is called matter in such passages as he sees instances

106
00:11:14,040 --> 00:11:18,680
of visible matter externally, fair and ugly.

107
00:11:18,680 --> 00:11:26,160
Consequently, here the words perception of matter, Rupa, Sanya, literally matter perceptions

108
00:11:26,160 --> 00:11:31,360
in the sense of perceptions about matter are used, A for fine material Janae stated thus

109
00:11:31,360 --> 00:11:39,040
under the headings of perceptions, also B, it has the label Sanya, matter, Rupa, thus

110
00:11:39,040 --> 00:11:46,800
it is the Janae's object, is labeled matter, Rupa Sanya, which is meant for that matter,

111
00:11:46,800 --> 00:11:52,440
it is its name, so it should be understood that this is also a term for B, what is class

112
00:11:52,440 --> 00:12:01,440
does for classina, et cetera, which is the object of that Janae.

113
00:12:01,440 --> 00:12:07,600
With the surmounting, with the fading away and with the cessation, what is meant, with

114
00:12:07,600 --> 00:12:12,760
the fading away and with the cessation, both because of the fading away and because of

115
00:12:12,760 --> 00:12:20,880
the cessation, either in all aspects or without exception of these perceptions of matter,

116
00:12:20,880 --> 00:12:29,400
Rupa Sanya and Sanya, which number 15 with the five each of the profitable, resultant

117
00:12:29,400 --> 00:12:35,840
and functional, and also of these things labeled matter, Rupa Sanya's objects of those

118
00:12:35,840 --> 00:12:42,400
perceptions, which number nine with the Rupa Sanya, et cetera, he enters upon and wears

119
00:12:42,400 --> 00:12:49,560
in the base consisting of boundless space, for he cannot enter upon and dwell in that without

120
00:12:49,560 --> 00:12:52,920
completely surmounting perceptions of matter.

121
00:12:52,920 --> 00:12:58,320
I can just interrupt, I think we've been skipping over Samantha, Samantha, did you want

122
00:12:58,320 --> 00:13:00,120
to read 15?

123
00:13:00,120 --> 00:13:22,840
Okay, we'll check next time around, thank you.

124
00:13:22,840 --> 00:13:29,840
15, right?

125
00:13:29,840 --> 00:13:31,760
Here and there is no surmounting of these perceptions and what has agreed for the object

126
00:13:31,760 --> 00:13:37,480
of those perceptions has not faded away, and when the perceptions have been surmounted,

127
00:13:37,480 --> 00:13:41,320
their object objects have been surmounted as well.

128
00:13:41,320 --> 00:13:45,800
That is why in the Vibanga, only the surmounting of the perceptions and not that of the

129
00:13:45,800 --> 00:13:48,200
objects is mentioned as follows.

130
00:13:48,200 --> 00:13:54,200
Here in what are perceptions of matter, they are the perceptions perceiving perceiviveness

131
00:13:54,200 --> 00:13:58,960
in one who has attained the fine material sphere attainment, or in one who has been reborn

132
00:13:58,960 --> 00:14:04,320
there, or in one who is abiding in bliss there in this present life.

133
00:14:04,320 --> 00:14:06,840
These are what are called perceptions of matter.

134
00:14:06,840 --> 00:14:11,080
These perceptions of matter are past, surpassed, surmounted.

135
00:14:11,080 --> 00:14:15,360
Hence with the complete surmounting of perceptions of matter is said.

136
00:14:15,360 --> 00:14:20,520
But this commentary should be understood to deal also with the surmounting of the object,

137
00:14:20,520 --> 00:14:24,040
because these attainments have to be reached by surmounting the object.

138
00:14:24,040 --> 00:14:28,200
They are not to be reached by retaining the same object as the first and subsequent

139
00:14:28,200 --> 00:14:31,400
challenge.

140
00:14:31,400 --> 00:14:36,520
Our page 324 is 16.

141
00:14:36,520 --> 00:14:41,160
With the disappearance of perception of resistance, perceptions of resistance are

142
00:14:41,160 --> 00:14:46,600
perceptions arisen through the impact of the physical base consisting of the eye, et cetera,

143
00:14:46,600 --> 00:14:52,520
and the respective objects consisting of visible objects, et cetera, and this is the term

144
00:14:52,520 --> 00:14:58,120
for perception of visible objects, and so on, according as it is said.

145
00:14:58,120 --> 00:15:01,520
Here are what are perceptions of resistance?

146
00:15:01,520 --> 00:15:07,520
Perceptions of visible objects, perceptions of sounds, perceptions of odors, perceptions

147
00:15:07,520 --> 00:15:15,400
of flavors, perceptions of tangible objects, these are called perceptions of resistance.

148
00:15:15,400 --> 00:15:20,600
The complete disappearance, the abandoning, the non-arising of these 10 kinds of perceptions

149
00:15:20,600 --> 00:15:26,080
of resistance, that is to say of the five profitable, resultant, and five unprofitable

150
00:15:26,080 --> 00:15:32,280
resultant, closing their non-occurrence as what is meant.

151
00:15:32,280 --> 00:15:40,480
Of course, these are not to be found in one who has entered from the Thursday night

152
00:15:40,480 --> 00:15:48,240
setting, either, for consciousness at that time does not occur by way of the five fellows.

153
00:15:48,240 --> 00:15:54,600
Still, the mention of them here should be understood as a recommendation of this Jana for

154
00:15:54,600 --> 00:15:58,760
the purpose of arousing interest in it.

155
00:15:58,760 --> 00:16:04,600
Chooses, in the case of the four Jana, there is mention of inflation and pain, already

156
00:16:04,600 --> 00:16:12,240
abandoned elsewhere, and in the case of the teapat, there is mention of the false view,

157
00:16:12,240 --> 00:16:20,320
personality, et cetera, already abandoned here.

158
00:16:20,320 --> 00:16:25,440
Or alternatively, those of these are also not to be found in one who has attained the

159
00:16:25,440 --> 00:16:31,080
fine material spill sphere, still, they're not being there, is not due to their having

160
00:16:31,080 --> 00:16:32,720
been abandoned.

161
00:16:32,720 --> 00:16:37,840
For development of the fine material sphere does not lead to fading of greed from materiality.

162
00:16:37,840 --> 00:16:44,120
And the occurrence of those, the fine material Jana's, is actually dependent on materiality.

163
00:16:44,120 --> 00:16:51,160
But this development of the material does lead to the fading of greed from materiality.

164
00:16:51,160 --> 00:16:55,040
Therefore it is allowed, we'll say, that they are actually abandoned here.

165
00:16:55,040 --> 00:17:01,080
Not only to say it, but to maintain it, absolutely.

166
00:17:01,080 --> 00:17:08,720
In fact, it's because they have not been abandoned already before this, that it was said

167
00:17:08,720 --> 00:17:17,760
by the blessed one, that soon is a storm to one who has the fierce Jana.

168
00:17:17,760 --> 00:17:26,040
And it is precisely because they are abandoned here, that they import the ability of

169
00:17:26,040 --> 00:17:35,160
the material attainment and the state of peaceful, peaceful liberation, I mentioned.

170
00:17:35,160 --> 00:17:46,800
And the Alana Kalama neither saw the 500 cars that passed close by him, nor had the sound

171
00:17:46,800 --> 00:17:56,920
of them when he was, when he was in the, I mean, material attainment.

172
00:17:56,920 --> 00:18:03,160
With non-attention to perceptions of variety, either to perceptions occurring with variety

173
00:18:03,160 --> 00:18:08,120
as their domain or to perceptions themselves varies.

174
00:18:08,120 --> 00:18:14,280
For perceptions of variety are also called for two reasons, firstly because the kinds

175
00:18:14,280 --> 00:18:19,840
of perception included along with the mind element and the mind consciousness element

176
00:18:19,840 --> 00:18:25,800
in one who has not attained, which kinds are intended here as described in the Vipanga

177
00:18:25,800 --> 00:18:27,760
does.

178
00:18:27,760 --> 00:18:34,400
Here in what are perceptions of variety, the perception, perceiving, perceivingness in

179
00:18:34,400 --> 00:18:41,120
one who has not attained and possesses either mind element or mind consciousness element,

180
00:18:41,120 --> 00:18:48,440
in one who has not attained and possesses either mind element or mind consciousness element.

181
00:18:48,440 --> 00:18:54,560
These are called perceptions of variety, occur with respect to a domain that is varied

182
00:18:54,560 --> 00:19:02,200
in individual essence, with the variety class as visible objects, sound, etc.

183
00:19:02,200 --> 00:19:09,600
And secondly, because the 44 kinds of perception, there is to say eight kinds of sense

184
00:19:09,600 --> 00:19:16,960
sphere profitable perception, twelve kinds of unprofitable perception, eleven kinds of

185
00:19:16,960 --> 00:19:25,400
sense sphere profitable result and perception, two kinds of unprofitable result and perception,

186
00:19:25,400 --> 00:19:30,240
and eleven kinds of sense sphere functional perception.

187
00:19:30,240 --> 00:19:35,800
Themselves have variety, have various individual essences and are dissimilar from each

188
00:19:35,800 --> 00:19:36,800
other.

189
00:19:36,800 --> 00:19:44,400
With the complete non-attention to, non-adverting to, non-reaction to, non-reviewing of

190
00:19:44,400 --> 00:19:47,360
these perceptions of variety.

191
00:19:47,360 --> 00:19:54,360
What is meant is that because he does not advert to them, give them attention or review

192
00:19:54,360 --> 00:19:56,360
them, therefore.

193
00:19:56,360 --> 00:20:02,360
But I had a question on 18 and 19.

194
00:20:02,360 --> 00:20:08,840
I remember reading that what the John is, the five hindrances are significantly lessened.

195
00:20:08,840 --> 00:20:15,200
Is that what it's talking about when it says that the John is actually dependent on

196
00:20:15,200 --> 00:20:33,840
materiality, but the development of it does lead to the fading of greed for materiality?

197
00:20:33,840 --> 00:20:37,800
I'm sorry, I was away for a second, there was that a question?

198
00:20:37,800 --> 00:20:38,800
It was.

199
00:20:38,800 --> 00:20:42,000
Yes, I had a question on 18 and 19.

200
00:20:42,000 --> 00:20:48,160
I remember reading that achieving the John is lessened the five hindrances, and I'm wondering

201
00:20:48,160 --> 00:20:53,360
if that's what it's referring to with this passage about that the John is dependent

202
00:20:53,360 --> 00:21:02,600
on materiality, but the development of it does lead to the fading of greed for materiality?

203
00:21:02,600 --> 00:21:19,760
What the hindrances have now happened in the band in the already, he's talking about

204
00:21:19,760 --> 00:21:20,760
something else.

205
00:21:20,760 --> 00:21:24,760
I'm not really sure what he's referring to that he can say that this does lead to the

206
00:21:24,760 --> 00:21:27,440
fading of greed for materiality.

207
00:21:27,440 --> 00:21:29,960
It's interesting, it must lead some more than directly.

208
00:21:29,960 --> 00:21:39,560
I know why the development of the material would actually lead, because he's talking about

209
00:21:39,560 --> 00:21:42,560
I think the difference between some attempt to be passed in there.

210
00:21:42,560 --> 00:21:47,880
So some of this is going to, I don't know, or he's talking about something else.

211
00:21:47,880 --> 00:21:52,800
It's an odd thing to say.

212
00:21:52,800 --> 00:21:55,640
Thank you.

213
00:21:55,640 --> 00:21:59,680
Thank you.

214
00:21:59,680 --> 00:22:05,920
And two things should be understood, firstly, that their absence is stated here in two

215
00:22:05,920 --> 00:22:11,520
ways as surmounting and disappearance, because the earlier perceptions of matter and perceptions

216
00:22:11,520 --> 00:22:17,720
of resistance do not exist even in the kind of existence produced by this John on rebirth.

217
00:22:17,720 --> 00:22:21,840
Let alone when this John is entered upon and dwelt in that existence.

218
00:22:21,840 --> 00:22:28,320
And secondly, in the case of perceptions of variety, nonattention to them is said because

219
00:22:28,320 --> 00:22:34,720
twenty-seven kinds of perception, that is to say eight kinds of sense-sphere profitable

220
00:22:34,720 --> 00:22:40,960
perception, nine kinds of functional perception, and ten kinds of unprofitable perception

221
00:22:40,960 --> 00:22:45,200
still exist in the kind of existence produced by this John.

222
00:22:45,200 --> 00:22:49,840
For when he enters upon and dwells in this John, there too, he does so by nonattention

223
00:22:49,840 --> 00:23:01,240
to them also, but he has not attained when he does give attention to them.

224
00:23:01,240 --> 00:23:07,720
And here briefly, it should be understood that the branding of all fine materials fear states

225
00:23:07,720 --> 00:23:15,320
he's signified by the worlds with the surmounting of perceptions of matter and the abandoning

226
00:23:15,320 --> 00:23:24,640
of a nonattention to all sense-sphere consciousness and its concomitant is signified by

227
00:23:24,640 --> 00:23:32,320
the words with the disappearance of perceptions of resistance with nonattention to perceptions

228
00:23:32,320 --> 00:23:33,320
of variety.

229
00:23:33,320 --> 00:23:36,120
I'm just going back to your question.

230
00:23:36,120 --> 00:23:42,960
I guess one way it's easy to see what he's saying is it's really part of the attainment

231
00:23:42,960 --> 00:23:50,960
of the immaterials fear is the perception of them as being more refined than the materials

232
00:23:50,960 --> 00:23:51,960
fear.

233
00:23:51,960 --> 00:23:58,840
So there's two types of summative John's and they're the material ones and the immaterial

234
00:23:58,840 --> 00:23:59,840
ones.

235
00:23:59,840 --> 00:24:05,200
So up until this point we've dealt with the material ones, this section deals with their

236
00:24:05,200 --> 00:24:18,680
call there, the thing that comes after them, the immaterials fears, which are attained

237
00:24:18,680 --> 00:24:25,880
by observing or finding that the material is coarse.

238
00:24:25,880 --> 00:24:31,600
So when you find that the material is coarse and never refined, there is a clear sense

239
00:24:31,600 --> 00:24:36,920
that that's going to change your perception about it and so that could be what he's talking

240
00:24:36,920 --> 00:24:37,920
about.

241
00:24:37,920 --> 00:24:43,600
He's just saying, you know, the material, John, those don't help you overcome your attachment

242
00:24:43,600 --> 00:24:50,880
to material, materiality, and materiality here is meant just in the most general sense

243
00:24:50,880 --> 00:24:59,040
possible that it includes even the first four or five John, but when you enter the material,

244
00:24:59,040 --> 00:25:04,560
the immaterials fears, it does actually change your mind about the, about materiality

245
00:25:04,560 --> 00:25:11,960
because you start to prefer immateriality, it's not, so it's not we pass and I think that

246
00:25:11,960 --> 00:25:16,200
somehow has to do with fading away of desire.

247
00:25:16,200 --> 00:25:20,560
That makes sense to me, Bante, thank you.

248
00:25:20,560 --> 00:25:26,680
Unbounded space, here it is called unbounded and then that literally endless because neither

249
00:25:26,680 --> 00:25:32,960
its end as it's arising nor its end as its fall are made known.

250
00:25:32,960 --> 00:25:40,320
It is the space left by the removal of the casino that is called space and here unboundedness

251
00:25:40,320 --> 00:25:45,440
or endlessness should be understood as referring to the attention also, which is why it

252
00:25:45,440 --> 00:25:52,160
is said in the Vibankar, he places settles his consciousness in that space, he pervades

253
00:25:52,160 --> 00:25:59,160
unboundedly, unbounded, unbounded and then those space is set.

254
00:25:59,160 --> 00:26:03,160
I can just break in for one moment, Samantha, we haven't been able to hear you at all if

255
00:26:03,160 --> 00:26:07,600
you were trying to read, if you can just go into your settings and make sure that your microphone

256
00:26:07,600 --> 00:26:13,640
is hooked up, there's up on top, there's a configure button and just go in through the

257
00:26:13,640 --> 00:26:18,760
settings and if you want to test it out, you know, go ahead when you get that set up because

258
00:26:18,760 --> 00:26:23,440
we'd love to have you read along.

259
00:26:23,440 --> 00:26:31,400
Bante, I have a question and we, I did notice that the Asana sphere was covered under

260
00:26:31,400 --> 00:26:46,440
the 4th material journey, was it Asana Tala, the sphere of no mind, just the body is there

261
00:26:46,440 --> 00:26:56,800
that is sat in with the 4th material journey, the 4th journey, I didn't see this described

262
00:26:56,800 --> 00:27:06,040
here or the before this, the one under the 4th journey.

263
00:27:06,040 --> 00:27:09,240
What's your question?

264
00:27:09,240 --> 00:27:18,520
The question is, how do you get into the Asana sphere, I mean, this is describing how

265
00:27:18,520 --> 00:27:26,840
do you get to the material sphere, but that was discussed here, I was wondering whether

266
00:27:26,840 --> 00:27:29,240
it is discussed as well.

267
00:27:29,240 --> 00:27:43,640
If I remember correctly, there's a specific contemplate, something like, you're cutting out

268
00:27:43,640 --> 00:27:52,240
quite a bit, oh, hello test test, that's better.

269
00:27:52,240 --> 00:28:03,200
So, there's some special meditation you practice, whereby you see the immaterial as repulsive

270
00:28:03,200 --> 00:28:10,480
or something, I can't remember it so very specific, I think there's a lot of conflicting

271
00:28:10,480 --> 00:28:32,760
opinion about the Asana santay, I see, yeah and it is attained based on the 4th journey,

272
00:28:32,760 --> 00:28:54,600
I don't know, Samantha any luck with your mic, I think we heard you, did you try that again?

273
00:28:54,600 --> 00:28:58,280
You may have to hold down on the key, whatever you're pushed to talk key is the whole time

274
00:28:58,280 --> 00:29:13,920
you're talking, it's up for just a moment that I can turn the little lip string red there

275
00:29:13,920 --> 00:29:30,000
at the mouth turns red when the person is connected.

276
00:29:30,000 --> 00:29:38,320
I can page 326 24, he enters upon and dwells in the base consisting of boundless space,

277
00:29:38,320 --> 00:29:45,320
it has no bound and thus it is unbounded, but especially unbounded is unbounded space,

278
00:29:45,320 --> 00:29:51,920
unbounded space is the same as boundless space, that boundless space is a base in the sense

279
00:29:51,920 --> 00:29:58,000
of habitat for a genre whose nature it is to be associated with it, and the deity's base

280
00:29:58,000 --> 00:30:05,280
is for deities, thus it is the base consisting of boundless space, he enters and dwells in having

281
00:30:05,280 --> 00:30:11,200
reached that base consisting of boundless space, having caused it to be produced, he dwells

282
00:30:11,200 --> 00:30:16,960
with an abiding consisting and postures that are in conformity with it, this is the detailed

283
00:30:16,960 --> 00:30:27,360
explanation of the base consisting of boundless space as a meditation object.

284
00:30:27,360 --> 00:30:33,520
When he wants to develop the base consisting of boundless functionless, he must first achieve

285
00:30:33,520 --> 00:30:41,760
mastery in the five ways in the attainment of the base consisting of boundless space,

286
00:30:41,760 --> 00:30:48,600
then he should see the danger in the base consisting of boundless space in this way.

287
00:30:48,600 --> 00:30:56,360
This attainment has fine material, Jana, as is near enemy, and it is not as peaceful as

288
00:30:56,360 --> 00:31:00,560
the base consisting of boundless consciousness.

289
00:31:00,560 --> 00:31:06,880
So having ended his attachment to that, he should give his attention to the base consisting

290
00:31:06,880 --> 00:31:15,760
of boundless consciousness as peaceful, at berting again and again as consciousness consciousness.

291
00:31:15,760 --> 00:31:23,360
To the consciousness that of cure berating that space as is object, he should give it

292
00:31:23,360 --> 00:31:31,600
attention, review it, and strike at it with a polite and sustained thought.

293
00:31:31,600 --> 00:31:40,320
But he should not give attention simply in this way, boundless fungus.

294
00:31:40,320 --> 00:31:46,240
As he directs his mind again and again onto that sign in this way, the hindrances are

295
00:31:46,240 --> 00:31:54,160
suppressed, mindfulness is established, and his mind becomes concentrated in access.

296
00:31:54,160 --> 00:32:00,000
He coat the base that sign again and again, develops and repeatedly practices it.

297
00:32:00,000 --> 00:32:06,160
As he does so, consciousness belonging to the base consisting of boundless consciousness

298
00:32:06,160 --> 00:32:13,320
arises in absorption with the past consciousness that pervaded this space as is object.

299
00:32:13,320 --> 00:32:18,280
Just as that belonging to the base consisting of boundless space did with this space as

300
00:32:18,280 --> 00:32:24,720
its object, but the method of explaining the process of absorption should be understood

301
00:32:24,720 --> 00:32:29,240
in the way already described.

302
00:32:29,240 --> 00:32:41,960
At this point itself, it is set by completely smonching samad ticama, the base consisting of

303
00:32:41,960 --> 00:32:52,600
boundless space aware of on both the consciousness, he enters open and well and wealth in the

304
00:32:52,600 --> 00:32:58,200
base consisting of boundless consciousness.

305
00:32:58,200 --> 00:33:02,440
Heron, completely, is as already explained.

306
00:33:02,440 --> 00:33:08,440
By surmounting the base consisting of boundless space, the chana is called the base consisting

307
00:33:08,440 --> 00:33:15,400
of boundless space in the way already stated and its object is so called to.

308
00:33:15,400 --> 00:33:22,760
For the object to is boundless space, Akasana in chana, in the way already stated.

309
00:33:22,760 --> 00:33:29,320
And then because it is the object of the first immaterial chana, it is its base in the sense

310
00:33:29,320 --> 00:33:37,760
of habitat as the dearies base is for dearies, thus it is the base consisting of boundless

311
00:33:37,760 --> 00:33:38,760
space.

312
00:33:38,760 --> 00:33:45,080
Likewise, it is boundless space and then because it is the cause of the chana as being

313
00:33:45,080 --> 00:33:52,680
of that species, it is its base in the sense of locality of the species, a scumboldt

314
00:33:52,680 --> 00:34:00,280
chana is the base of horses, thus it is the base consisting of boundless space in this

315
00:34:00,280 --> 00:34:02,880
way also.

316
00:34:02,880 --> 00:34:08,800
So it should be understood that the words by surmounting the base consisting of boundless

317
00:34:08,800 --> 00:34:14,040
space include both the chana and its object together.

318
00:34:14,040 --> 00:34:20,000
Since this base consisting of boundless consciousness is to be entered upon and wealth

319
00:34:20,000 --> 00:34:27,360
in precisely by surmounting, by causing the non-occurrence of and by not giving attention

320
00:34:27,360 --> 00:34:31,080
to both the chana and its object.

321
00:34:31,080 --> 00:34:41,160
Marilynne, I am so sorry I am so used to jumping in right after a last though, I think

322
00:34:41,160 --> 00:35:01,160
I am skipping over you too, are you able to reach 29?

323
00:35:01,160 --> 00:35:20,880
I am sorry I don't think we are able to hear you.

324
00:35:32,160 --> 00:35:40,680
I am sorry you might be muted somewhere perhaps in your settings if you can check that

325
00:35:40,680 --> 00:35:45,800
we will get back to you on the next time around.

326
00:35:45,800 --> 00:35:50,760
Unbounded consciousness, what is meant is that he gives his attention less, unbounded

327
00:35:50,760 --> 00:35:55,600
consciousness to that same consciousness that occurred in pervading as its object

328
00:35:55,600 --> 00:36:01,680
the space as unbounded space or unbounded refers to the attention for when he gives attention

329
00:36:01,680 --> 00:36:06,120
without reserve to its consciousness that had the space as its object then the attention

330
00:36:06,120 --> 00:36:13,280
he gives to it as unbounded.

331
00:36:13,280 --> 00:36:19,680
What is said in the rebound, unbounded consciousness he gives attention to that same space

332
00:36:19,680 --> 00:36:28,240
pervaded by consciousness, he pervades boundlessly hence unbounded consciousness is said

333
00:36:28,240 --> 00:36:43,960
but in the passage, tameva, akasam, vignane, vignane, potham, the instrumental case by

334
00:36:43,960 --> 00:36:53,840
consciousness must be understood in the sense of acusative for the teachers or the commentary

335
00:36:53,840 --> 00:37:02,520
explain its meaning in that way what is meant by he pervades boundlessly is that he gives

336
00:37:02,520 --> 00:37:12,880
attention to that same consciousness which had pervaded that space tameva, akasam, potham,

337
00:37:12,880 --> 00:37:20,160
vignaneva.

338
00:37:20,160 --> 00:37:24,880
He enters upon and dwells in the base consisting of boundless consciousness.

339
00:37:24,880 --> 00:37:30,000
It has no bound, until literally end, thus it is unbounded.

340
00:37:30,000 --> 00:37:36,360
What is unbounded is boundless and unbounded consciousness is called boundless consciousness,

341
00:37:36,360 --> 00:37:45,720
that is, Vignaneaneaneaneaneaneaneaneane, in contracted form instead of Vignaneaneaneaneaneaneaneaneme

342
00:37:45,720 --> 00:37:48,400
which is the full number of syllables.

343
00:37:48,400 --> 00:37:54,760
This is an idiomatic form that boundless consciousness is the base in the sense of foundation

344
00:37:54,760 --> 00:38:00,560
for the Jana whose nature it is to be associated with it as the deities basis for deities,

345
00:38:00,560 --> 00:38:05,680
betuses it is the base consisting of boundless consciousness, the rest is the same as before.

346
00:38:05,680 --> 00:38:12,680
This is the detailed explanation of the base consisting of boundless consciousness as a meditation subject.

347
00:38:12,680 --> 00:38:22,680
Again, this chapter does this neat thing of showing us how ancient this practice of using a mantra is.

348
00:38:22,680 --> 00:38:26,680
And it's why I started to use the word mantra to describe our practice.

349
00:38:26,680 --> 00:38:35,680
We were having a discussion on the internet some time ago, and one of the one Biquini was on it.

350
00:38:35,680 --> 00:38:37,680
She said, we don't use mantras.

351
00:38:37,680 --> 00:38:45,680
She said, she kind of joked about it and said, you know, you have to understand that it's not a mantra.

352
00:38:45,680 --> 00:38:46,680
We're not talking about it.

353
00:38:46,680 --> 00:38:50,680
And I said, well, actually that is how I describe it.

354
00:38:50,680 --> 00:39:01,680
And it's useful because it puts in mind the context as opposed to just being something new that we've just thought up in modern times.

355
00:39:01,680 --> 00:39:03,680
It's actually quite the opposite.

356
00:39:03,680 --> 00:39:14,680
It's like people have given up the idea of using a mantra they've gotten kind of lazy, you could say, and just want to sit and be mindful, whatever that means.

357
00:39:14,680 --> 00:39:22,680
The word mantra also gives the meaning of a spell, right?

358
00:39:22,680 --> 00:39:25,680
That's a modern meaning, I think.

359
00:39:25,680 --> 00:39:28,680
Well, there may be something, but that's a conflation.

360
00:39:28,680 --> 00:39:31,680
It's not what the word actually means.

361
00:39:31,680 --> 00:39:33,680
Actually, I'm not even sure what it means.

362
00:39:33,680 --> 00:39:43,680
I've looked it up before, but a mantra is just a word that is used to focus the mind.

363
00:39:43,680 --> 00:39:53,680
But, yeah, in Thai they call, one is, comes from mantra, one means a spell.

364
00:39:53,680 --> 00:39:57,680
But, say, there were mantras that were used in Hinduism and the Vedas.

365
00:39:57,680 --> 00:40:01,680
Later Vedas, I think, has spells.

366
00:40:01,680 --> 00:40:08,680
And those are picked up by Buddhist cultures as well.

367
00:40:08,680 --> 00:40:18,680
So, I'm wondering if we should stop there, because we're almost halfway, and don't think we'll get through all of it today.

368
00:40:18,680 --> 00:40:24,680
That sounds good. Thank you, Bhante. Thank you, everyone.

369
00:40:24,680 --> 00:40:27,680
Yeah, thanks, everyone.

370
00:40:27,680 --> 00:40:29,680
Sajapanta.

371
00:40:29,680 --> 00:40:32,680
Sajapanta. Thank you, everyone.

372
00:40:32,680 --> 00:40:33,680
Sajapanta.

373
00:40:33,680 --> 00:40:35,680
Sajapanta.

374
00:40:35,680 --> 00:40:37,680
Sajapanta.

375
00:40:37,680 --> 00:40:40,680
Sajapanta.

376
00:40:40,680 --> 00:41:08,680
I'm Marilyn Maak.

