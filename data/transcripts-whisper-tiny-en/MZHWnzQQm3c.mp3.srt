1
00:00:00,000 --> 00:00:05,000
I find it very hard to concentrate on things unless I am highly interested in something.

2
00:00:05,000 --> 00:00:12,000
It leads me to procrastinate a lot. Is that just lack of discipline?

3
00:00:12,000 --> 00:00:20,000
It can be. It has to do a lot with our desires and the versions.

4
00:00:20,000 --> 00:00:23,000
If you're a person who has a lot of desires and the versions,

5
00:00:23,000 --> 00:00:28,000
then it's quite likely that you'll procrastinate in a great number of things.

6
00:00:28,000 --> 00:00:33,000
If you have less desire and desire and less aversion,

7
00:00:33,000 --> 00:00:37,000
then you'll be able to focus on many more things.

8
00:00:37,000 --> 00:00:42,000
Because your interest will be your ability to develop interest.

9
00:00:42,000 --> 00:00:49,000
It will be heightened because you'll be able to just be interested in whatever is in the present moment.

10
00:00:49,000 --> 00:00:57,000
So you won't have a desire to go and do things, but you'll be interested in whatever the task is in front of you.

11
00:00:57,000 --> 00:01:00,000
Now, this is one aspect.

12
00:01:00,000 --> 00:01:08,000
The other aspect is that your lack of interest is actually a natural outcome of the meditation.

13
00:01:08,000 --> 00:01:15,000
So it can also have to do with the uselessness of the things that you're doing.

14
00:01:15,000 --> 00:01:20,000
So I found it quite difficult to focus on school.

15
00:01:20,000 --> 00:01:22,000
I went after his breakfast meditation.

16
00:01:22,000 --> 00:01:24,000
I went back and did two different years.

17
00:01:24,000 --> 00:01:31,000
The first year I was quite easy because I was really keen on it because there were some Buddhist courses I can take and Sanskrit and so on.

18
00:01:31,000 --> 00:01:33,000
So actually I was quite into it.

19
00:01:33,000 --> 00:01:37,000
But the second year going back, totally the opposite.

20
00:01:37,000 --> 00:01:44,000
There were no more Buddhist courses to take, and I just totally found it impossible to focus on it.

21
00:01:44,000 --> 00:01:46,000
There was no interest there.

22
00:01:46,000 --> 00:01:49,000
And I don't think that's necessarily a bad thing.

23
00:01:49,000 --> 00:01:59,000
It just means that those, if the things that you're supposed to concentrate on are useless,

24
00:01:59,000 --> 00:02:10,000
then it just becomes a factor of the matter that it's very difficult to become interested.

25
00:02:10,000 --> 00:02:18,000
And it's unwholesome and unhealthy actually to become interested or to force yourself to do things that are of no benefit.

26
00:02:18,000 --> 00:02:26,000
So yeah, for most of us, it's just defile minutes.

27
00:02:26,000 --> 00:02:34,000
But once we start to meditate, there's that aspect as well that it can have to do with our meditation practice.

28
00:02:34,000 --> 00:02:50,000
You'll find that as a result of meditating, you don't want to get involved with the great number of things that you before found easy to get involved in.

