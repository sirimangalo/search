1
00:00:00,000 --> 00:00:07,000
Hello, and welcome back to our study of the Damapada.

2
00:00:07,000 --> 00:00:16,000
Today, we continue on with verse number 76, which reads as follows.

3
00:00:16,000 --> 00:00:26,000
Today, we continue on with verse number 76, which reads as follows.

4
00:00:26,000 --> 00:00:35,000
Today, we continue on with verse number 76.

5
00:00:35,000 --> 00:01:04,000
One who like one who unearth or uncovers buried treasure.

6
00:01:04,000 --> 00:01:13,000
And whatever person seeing one's faults,

7
00:01:13,000 --> 00:01:17,000
paste when seeing one's faults,

8
00:01:17,000 --> 00:01:21,000
what did I see now?

9
00:01:21,000 --> 00:01:23,000
Let's then be known.

10
00:01:23,000 --> 00:01:25,000
That's one no one's fault.

11
00:01:25,000 --> 00:01:27,000
That's one no one's own fault.

12
00:01:27,000 --> 00:01:30,000
So point out one's fault.

13
00:01:30,000 --> 00:01:35,000
Someone who niggas, this person, niggae,

14
00:01:35,000 --> 00:01:42,000
wanting one who speaks in a censure or gives one

15
00:01:42,000 --> 00:01:45,000
admonishment, admonishes one.

16
00:01:45,000 --> 00:01:47,000
Made having a wise,

17
00:01:47,000 --> 00:01:51,000
should be considered a wise person.

18
00:01:51,000 --> 00:01:54,000
That is some banditangpati.

19
00:01:54,000 --> 00:01:58,000
Such a person should be associated with, by the wise.

20
00:01:58,000 --> 00:02:01,000
If a person is wise,

21
00:02:01,000 --> 00:02:08,000
such a person is to be associated with,

22
00:02:08,000 --> 00:02:11,000
by the wise people.

23
00:02:11,000 --> 00:02:14,000
When one associates with such a person,

24
00:02:14,000 --> 00:02:16,000
things get better, not worse.

25
00:02:16,000 --> 00:02:19,000
Say you'll hold in a bop you, they get better.

26
00:02:19,000 --> 00:02:21,000
You don't go to evil.

27
00:02:21,000 --> 00:02:24,000
You don't get worse.

28
00:02:24,000 --> 00:02:27,000
So associate with people who point out your faults

29
00:02:27,000 --> 00:02:33,000
when you do, things get better, not worse.

30
00:02:33,000 --> 00:02:39,000
This verse was told in regards to a student of Sariputa,

31
00:02:39,000 --> 00:02:42,000
named Radha.

32
00:02:42,000 --> 00:02:47,000
Radha was a poor brahman,

33
00:02:47,000 --> 00:02:50,000
who listened, heard the Buddha's teaching,

34
00:02:50,000 --> 00:02:53,000
and went to live with the monks.

35
00:02:53,000 --> 00:02:57,000
But the monks were unwilling to give him the higher,

36
00:02:57,000 --> 00:02:59,000
or to give him any ordination.

37
00:02:59,000 --> 00:03:03,000
I think somewhere there was a comment that

38
00:03:03,000 --> 00:03:07,000
it was because he took such great care of the monks,

39
00:03:07,000 --> 00:03:10,000
and they were reluctant to give him the ordination

40
00:03:10,000 --> 00:03:13,000
because they needed someone to look after them.

41
00:03:13,000 --> 00:03:16,000
Which is sort of an interesting situation.

42
00:03:16,000 --> 00:03:19,000
It doesn't say that in this version of the story,

43
00:03:19,000 --> 00:03:22,000
but for some reason it may have been that he was old.

44
00:03:22,000 --> 00:03:28,000
It may have been that they had no trust in him.

45
00:03:28,000 --> 00:03:32,000
But the Buddha saw this, and the Buddha

46
00:03:32,000 --> 00:03:38,000
discerned through his vast knowledge and understanding

47
00:03:38,000 --> 00:03:42,000
that Radha would actually be capable of becoming an Arat

48
00:03:42,000 --> 00:03:45,000
if he were to ordain.

49
00:03:45,000 --> 00:03:48,000
So he went to the monks, and he said,

50
00:03:48,000 --> 00:03:53,000
why is this man pretending that he didn't know

51
00:03:53,000 --> 00:03:56,000
or not letting on that he knew?

52
00:03:56,000 --> 00:03:58,000
And they told them who he was.

53
00:03:58,000 --> 00:04:03,000
He said, why isn't he or why aren't you ordaining him?

54
00:04:03,000 --> 00:04:05,000
I think maybe that's where it says it may actually

55
00:04:05,000 --> 00:04:07,000
say why they didn't ordain him.

56
00:04:07,000 --> 00:04:08,000
But anyway.

57
00:04:08,000 --> 00:04:10,000
So he said,

58
00:04:10,000 --> 00:04:14,000
does anyone, do any of you think of a reason

59
00:04:14,000 --> 00:04:17,000
why you might be inclined to ordain him?

60
00:04:17,000 --> 00:04:21,000
Can any of you remember something that he did for you?

61
00:04:21,000 --> 00:04:25,000
And sorry, put a pipe up and said,

62
00:04:25,000 --> 00:04:28,000
I remember once when he was living at home,

63
00:04:28,000 --> 00:04:33,000
he gave me a spoonful of rice.

64
00:04:33,000 --> 00:04:36,000
And the Buddha said, well, sorry, put that.

65
00:04:36,000 --> 00:04:40,000
Don't you think it's appropriate to be grateful

66
00:04:40,000 --> 00:04:44,000
and to repay people's kindness?

67
00:04:44,000 --> 00:04:46,000
Which is interesting because he didn't do that

68
00:04:46,000 --> 00:04:49,000
of a thing in a grand scheme of things.

69
00:04:49,000 --> 00:04:52,000
All he did was give him a single spoonful of rice.

70
00:04:52,000 --> 00:04:55,000
But it's a testament to the Buddha's sensitivity

71
00:04:55,000 --> 00:05:00,000
and sorry, put us gratitude in remembering.

72
00:05:00,000 --> 00:05:04,000
So when you ask someone, did they do something

73
00:05:04,000 --> 00:05:07,000
that has this person done anything for you?

74
00:05:07,000 --> 00:05:10,000
It's not common for people to think

75
00:05:10,000 --> 00:05:13,000
to keep that in their mind that this person did

76
00:05:13,000 --> 00:05:16,000
such a simple thing, but sorry, put it was a person

77
00:05:16,000 --> 00:05:21,000
who hadn't this great sensitivity.

78
00:05:21,000 --> 00:05:25,000
He didn't take for granted the fact

79
00:05:25,000 --> 00:05:28,000
that he had been given even a single spoonful of rice.

80
00:05:28,000 --> 00:05:30,000
And so he spoke up and the Buddha said,

81
00:05:30,000 --> 00:05:33,000
well, that case, don't you think it's worth ordaining him?

82
00:05:33,000 --> 00:05:35,000
And sorry, put to immediately said in that case,

83
00:05:35,000 --> 00:05:37,000
I will ordain him.

84
00:05:37,000 --> 00:05:40,000
And so sorry, put to give him the ordination.

85
00:05:40,000 --> 00:05:42,000
And I guess there was some skepticism

86
00:05:42,000 --> 00:05:44,000
that this monk would actually,

87
00:05:44,000 --> 00:05:47,000
whether this man would actually make a good monk.

88
00:05:47,000 --> 00:05:52,000
But sorry, put to was fair with him

89
00:05:52,000 --> 00:05:56,000
and honest with him and found that actually

90
00:05:56,000 --> 00:05:58,000
Radha was quite amenable to training.

91
00:05:58,000 --> 00:06:01,000
So sorry, put to a tell him, don't do this, don't do that.

92
00:06:01,000 --> 00:06:03,000
And he would take everything,

93
00:06:03,000 --> 00:06:04,000
sorry, put to said to heart.

94
00:06:04,000 --> 00:06:06,000
When there was only you have to do this,

95
00:06:06,000 --> 00:06:07,000
you have to do that.

96
00:06:07,000 --> 00:06:09,000
He would only have to hear it once

97
00:06:09,000 --> 00:06:13,000
and he would immediately adjust himself.

98
00:06:13,000 --> 00:06:15,000
And when sorry, put to admonish him saying,

99
00:06:15,000 --> 00:06:17,000
you're doing this wrong, you're doing that wrong.

100
00:06:17,000 --> 00:06:19,000
This is not right.

101
00:06:19,000 --> 00:06:22,000
You have this fault, you have that fault.

102
00:06:22,000 --> 00:06:25,000
He was completely amenable.

103
00:06:25,000 --> 00:06:28,000
And so the Buddha asked him at one time,

104
00:06:28,000 --> 00:06:29,000
how's your student going?

105
00:06:29,000 --> 00:06:30,000
Do you find him amenable?

106
00:06:30,000 --> 00:06:33,000
Sorry, put to said he is the perfect student.

107
00:06:33,000 --> 00:06:37,000
When I tell him not to do something,

108
00:06:37,000 --> 00:06:40,000
he stops doing it and never have to tell him twice.

109
00:06:40,000 --> 00:06:44,000
And the same goes with telling him what he should do.

110
00:06:44,000 --> 00:06:46,000
He is the perfect student.

111
00:06:46,000 --> 00:06:50,000
And the Buddha said, if you could have other students

112
00:06:50,000 --> 00:06:53,000
like him, what do you ordain?

113
00:06:53,000 --> 00:06:55,000
People, others, if you knew that they were going to be like him

114
00:06:55,000 --> 00:06:58,000
and he said, if I could have a thousand students,

115
00:06:58,000 --> 00:07:04,000
I would gladly take them on if they were all like Radha.

116
00:07:04,000 --> 00:07:07,000
And the monks got talking about this and they said

117
00:07:07,000 --> 00:07:10,000
how amazing it was, how wonderful it was.

118
00:07:10,000 --> 00:07:12,000
That sorry, put to had found such a wonderful student.

119
00:07:12,000 --> 00:07:15,000
And that Radha also had found such a wonderful teacher

120
00:07:15,000 --> 00:07:18,000
who was willing to point out his faults.

121
00:07:18,000 --> 00:07:24,000
And in no long times, Radha became an Arahad as predicted.

122
00:07:24,000 --> 00:07:29,000
And the Buddha heard the monks talking like this

123
00:07:29,000 --> 00:07:35,000
and then he, as a result, spoke this verse,

124
00:07:35,000 --> 00:07:41,000
saying indeed, Radha is very lucky.

125
00:07:41,000 --> 00:07:44,000
Yes, sorry, put is very lucky, but Radha also is very lucky

126
00:07:44,000 --> 00:07:48,000
because Sariput is someone who is like a person

127
00:07:48,000 --> 00:07:51,000
who points out very treasure.

128
00:07:51,000 --> 00:07:55,000
And he spoke this verse.

129
00:07:55,000 --> 00:07:57,000
So that's the backstory.

130
00:07:57,000 --> 00:08:02,000
It's a fairly well-known Buddhist story among Buddhists.

131
00:08:02,000 --> 00:08:08,000
And it relates to our practice in regards to the role

132
00:08:08,000 --> 00:08:12,000
of a teacher in one's relationship with one's teacher.

133
00:08:12,000 --> 00:08:17,000
You could also say it relates to our own ability

134
00:08:17,000 --> 00:08:22,000
to receive criticism in general

135
00:08:22,000 --> 00:08:26,000
and therefore to our relationship with anyone

136
00:08:26,000 --> 00:08:28,000
who gives us criticism.

137
00:08:28,000 --> 00:08:33,000
Because we receive criticism from all sorts of sources,

138
00:08:33,000 --> 00:08:40,000
from many people who are not qualified to criticize.

139
00:08:40,000 --> 00:08:44,000
How do you deal with people who criticize you unjustly, for example?

140
00:08:44,000 --> 00:08:46,000
This doesn't directly relate to that,

141
00:08:46,000 --> 00:08:52,000
but that's the general subject that we're dealing with.

142
00:08:52,000 --> 00:08:58,000
And it relates because this is so contrary

143
00:08:58,000 --> 00:09:01,000
to the state of an ordinary human being,

144
00:09:01,000 --> 00:09:08,000
which is to incline towards hiding one's faults

145
00:09:08,000 --> 00:09:14,000
and becoming upset when people point out our faults,

146
00:09:14,000 --> 00:09:18,000
from criticizing people for being critical.

147
00:09:18,000 --> 00:09:22,000
Everyone's a critic, they say.

148
00:09:22,000 --> 00:09:29,000
And to some, there are teachings that instructs students.

149
00:09:29,000 --> 00:09:31,000
Teachers will sometimes instruct students

150
00:09:31,000 --> 00:09:34,000
or there's books that are written that say you should accept criticism

151
00:09:34,000 --> 00:09:37,000
and thank anyone who gives you criticism.

152
00:09:37,000 --> 00:09:40,000
And I think there's some truth to that.

153
00:09:40,000 --> 00:09:45,000
But the response and the skepticism is that

154
00:09:45,000 --> 00:09:49,000
leads you to allow others to walk all over you.

155
00:09:49,000 --> 00:09:54,000
And to give unjust criticism and to leave unjust criticism

156
00:09:54,000 --> 00:09:56,000
unchallenged.

157
00:09:56,000 --> 00:09:59,000
And I think that also is a good point.

158
00:09:59,000 --> 00:10:03,000
And it goes against the Buddhist teaching.

159
00:10:03,000 --> 00:10:07,000
In the monastic society,

160
00:10:07,000 --> 00:10:13,000
there's a monk should be criticized,

161
00:10:13,000 --> 00:10:17,000
not criticized, but among is wrong.

162
00:10:17,000 --> 00:10:25,000
Is that fault when they get angry at criticism?

163
00:10:25,000 --> 00:10:29,000
So respond angrily or criticize in return.

164
00:10:29,000 --> 00:10:31,000
So you tell me, I did this wrong.

165
00:10:31,000 --> 00:10:33,000
And I said, well, you did this other thing wrong.

166
00:10:33,000 --> 00:10:39,000
Or to ignore people's criticism and not accept

167
00:10:39,000 --> 00:10:40,000
their criticism.

168
00:10:40,000 --> 00:10:41,000
All of this is faulty.

169
00:10:41,000 --> 00:10:45,000
But what's also faulty is to not speak in one's defense.

170
00:10:45,000 --> 00:10:50,000
One is at fault when one doesn't speak in one's defense.

171
00:10:50,000 --> 00:10:59,000
So there's a sense of having to be mindful

172
00:10:59,000 --> 00:11:02,000
and to be wise and to be balanced.

173
00:11:02,000 --> 00:11:07,000
In many issues that we deal with in life,

174
00:11:07,000 --> 00:11:11,000
Buddhism doesn't have a be-all-and-all answer.

175
00:11:11,000 --> 00:11:14,000
Buddhism is much more dealing with the building blocks.

176
00:11:14,000 --> 00:11:18,000
What are the things that make up any given situation?

177
00:11:18,000 --> 00:11:21,000
So there are much, much more general principles.

178
00:11:21,000 --> 00:11:24,000
And the general principle is to be able to discern

179
00:11:24,000 --> 00:11:28,000
the truth from falsehood and right from wrong.

180
00:11:28,000 --> 00:11:33,000
But here we come to this specific example

181
00:11:33,000 --> 00:11:37,000
of when the criticism is warranted.

182
00:11:37,000 --> 00:11:41,000
So we're dealing with someone who actually points out

183
00:11:41,000 --> 00:11:43,000
true faults.

184
00:11:43,000 --> 00:11:50,000
Because I think it can be said that there's quite often

185
00:11:50,000 --> 00:11:52,000
a grain of truth in every criticism.

186
00:11:52,000 --> 00:11:55,000
There's a reason why we're being criticized.

187
00:11:55,000 --> 00:11:58,000
Often people will criticize us unwarranted

188
00:11:58,000 --> 00:12:00,000
and not wishing to help us.

189
00:12:00,000 --> 00:12:04,000
So people criticize us often looking to upset us

190
00:12:04,000 --> 00:12:14,000
or looking to humiliate us or looking to hide their own fault.

191
00:12:14,000 --> 00:12:18,000
We criticize others to hide our own faults, this kind of thing.

192
00:12:18,000 --> 00:12:21,000
But even still, even in those cases,

193
00:12:21,000 --> 00:12:24,000
it's much easier to see the faults of others.

194
00:12:24,000 --> 00:12:28,000
And we hide our own faults, but we're very good at picking out

195
00:12:28,000 --> 00:12:31,000
the faults of others.

196
00:12:31,000 --> 00:12:36,000
But here, in the case of a teacher,

197
00:12:36,000 --> 00:12:38,000
we have another dilemma.

198
00:12:38,000 --> 00:12:42,000
And that's our inability to accept reasonable

199
00:12:42,000 --> 00:12:47,000
and our inability to accept criticism,

200
00:12:47,000 --> 00:12:51,000
not because it's not correct,

201
00:12:51,000 --> 00:12:59,000
because it shows a fault.

202
00:12:59,000 --> 00:13:03,000
It hurts our ego.

203
00:13:03,000 --> 00:13:07,000
It attacks something that we are protecting,

204
00:13:07,000 --> 00:13:08,000
and that is ourself.

205
00:13:08,000 --> 00:13:11,000
We hold ourselves dear.

206
00:13:11,000 --> 00:13:16,000
We cling to an image of who we are or who we want to be.

207
00:13:16,000 --> 00:13:19,000
And when that image is threatened,

208
00:13:19,000 --> 00:13:23,000
like anything, any belonging, anything we hold dear,

209
00:13:23,000 --> 00:13:25,000
we react, we get upset.

210
00:13:25,000 --> 00:13:31,000
And so this teaching is actually quite important

211
00:13:31,000 --> 00:13:35,000
for meditators in a meditative setting,

212
00:13:35,000 --> 00:13:38,000
because we need to take advice from others,

213
00:13:38,000 --> 00:13:42,000
unless you're the kind of person who can become enlightened

214
00:13:42,000 --> 00:13:44,000
miraculously by oneself.

215
00:13:44,000 --> 00:13:46,000
We have to take advice from others.

216
00:13:46,000 --> 00:13:51,000
And as a teacher, this is something that is quite familiar.

217
00:13:51,000 --> 00:13:57,000
It often, it becomes so difficult

218
00:13:57,000 --> 00:14:01,000
that teachers are often afraid to give advice.

219
00:14:01,000 --> 00:14:06,000
And much of a teacher's role in duty

220
00:14:06,000 --> 00:14:11,000
is to find ways to admonish one's students

221
00:14:11,000 --> 00:14:14,000
without upsetting them.

222
00:14:14,000 --> 00:14:18,000
Part of the great part of the skill of being a good teacher

223
00:14:18,000 --> 00:14:22,000
is the ability to not upset one's students,

224
00:14:22,000 --> 00:14:25,000
because anyone can give advice,

225
00:14:25,000 --> 00:14:28,000
and a real sign, unfortunately,

226
00:14:28,000 --> 00:14:33,000
of a poor teacher is not being able to.

227
00:14:33,000 --> 00:14:34,000
It's not that they can't give advice,

228
00:14:34,000 --> 00:14:36,000
but not being able to couch the advice

229
00:14:36,000 --> 00:14:44,000
in such a delicate terms or means

230
00:14:44,000 --> 00:14:47,000
that the student is actually able to accept it.

231
00:14:47,000 --> 00:14:49,000
So you'll often hear people giving advice

232
00:14:49,000 --> 00:14:51,000
and as a teacher, anyway, you cringe

233
00:14:51,000 --> 00:14:53,000
because that's not going to work

234
00:14:53,000 --> 00:14:55,000
and you're just making student upset.

235
00:14:55,000 --> 00:14:57,000
What you're saying is, correct,

236
00:14:57,000 --> 00:14:59,000
but it's not going to get through.

237
00:14:59,000 --> 00:15:04,000
And that's unfortunate because it often makes it,

238
00:15:04,000 --> 00:15:08,000
it makes the teacher's job more difficult

239
00:15:08,000 --> 00:15:10,000
and it hampers the teacher's ability

240
00:15:10,000 --> 00:15:13,000
to be frank with a student.

241
00:15:13,000 --> 00:15:17,000
And often, a teacher's duty

242
00:15:17,000 --> 00:15:20,000
is require subsetting the student

243
00:15:20,000 --> 00:15:22,000
and sometimes you have no choice.

244
00:15:22,000 --> 00:15:25,000
Your choice is to not teach them or to hurt them,

245
00:15:25,000 --> 00:15:28,000
to upset them, and you have to gauge

246
00:15:28,000 --> 00:15:31,000
how far you can push a student,

247
00:15:31,000 --> 00:15:34,000
which is, unfortunately,

248
00:15:34,000 --> 00:15:36,000
usually not very far.

249
00:15:36,000 --> 00:15:39,000
And usually, unless you're dealing with a special person,

250
00:15:39,000 --> 00:15:42,000
like Radha, which is why Sariputum is so happy,

251
00:15:42,000 --> 00:15:44,000
but for most people,

252
00:15:44,000 --> 00:15:47,000
it's very difficult for us to take criticism.

253
00:15:47,000 --> 00:15:51,000
It's very hard for us to prevent the anger

254
00:15:51,000 --> 00:15:54,000
and the self-righteousness

255
00:15:54,000 --> 00:15:57,000
when people try to even well-meaning

256
00:15:57,000 --> 00:15:59,000
try to help us.

257
00:15:59,000 --> 00:16:02,000
And so we have to be aware of this.

258
00:16:02,000 --> 00:16:06,000
And that's one of the important aspects of this teaching

259
00:16:06,000 --> 00:16:09,000
that I think a lot of people react favorably to

260
00:16:09,000 --> 00:16:12,000
when this imagery of pointing out very treasure.

261
00:16:12,000 --> 00:16:16,000
So we try to remind ourselves of this

262
00:16:16,000 --> 00:16:20,000
and to think of it as someone pointing out very treasure.

263
00:16:20,000 --> 00:16:23,000
And that's why we'll come up with these teachings

264
00:16:23,000 --> 00:16:25,000
where we tell people

265
00:16:25,000 --> 00:16:27,000
that anyone who criticizes you,

266
00:16:27,000 --> 00:16:28,000
you should thank them.

267
00:16:28,000 --> 00:16:32,000
You should thank people who criticize you.

268
00:16:34,000 --> 00:16:37,000
I don't think, I don't think it's,

269
00:16:37,000 --> 00:16:39,000
as I said,

270
00:16:39,000 --> 00:16:42,000
when a criticism is faulty,

271
00:16:42,000 --> 00:16:45,000
I don't think it should be hesitant to point out

272
00:16:45,000 --> 00:16:47,000
when it's faulty.

273
00:16:47,000 --> 00:16:48,000
Like my teacher said,

274
00:16:48,000 --> 00:16:50,000
if someone calls you a buffalo,

275
00:16:50,000 --> 00:16:52,000
you just turn around and feel if you have a tail.

276
00:16:52,000 --> 00:16:53,000
If you don't have a tail,

277
00:16:53,000 --> 00:16:55,000
you should tell them I don't have a tail.

278
00:16:55,000 --> 00:17:00,000
I didn't quite say it like that.

279
00:17:00,000 --> 00:17:03,000
In fact, it's more annoying for yourself.

280
00:17:03,000 --> 00:17:07,000
And I think that's the key that we should point out is

281
00:17:07,000 --> 00:17:09,000
it requires wisdom.

282
00:17:09,000 --> 00:17:11,000
You have to be wise and know

283
00:17:11,000 --> 00:17:13,000
who is this person,

284
00:17:13,000 --> 00:17:16,000
who is their motive for doing this?

285
00:17:16,000 --> 00:17:17,000
First of all,

286
00:17:17,000 --> 00:17:19,000
but regardless of their motive is their truth

287
00:17:19,000 --> 00:17:22,000
behind what they're saying.

288
00:17:22,000 --> 00:17:27,000
Criticism is a very big part of

289
00:17:27,000 --> 00:17:30,000
the teaching dynamic, as mentioned,

290
00:17:30,000 --> 00:17:33,000
but also a very big part of our practice

291
00:17:33,000 --> 00:17:37,000
because of course it's dealing with ego.

292
00:17:37,000 --> 00:17:43,000
It's a good test of our state of mind

293
00:17:43,000 --> 00:17:45,000
and our purity of mind,

294
00:17:45,000 --> 00:17:47,000
how well we're able to take criticism.

295
00:17:47,000 --> 00:17:52,000
So, Criticism is actually a useful tool in our practice

296
00:17:52,000 --> 00:17:54,000
to put ourselves in position

297
00:17:54,000 --> 00:17:57,000
or to allow others to criticize us

298
00:17:57,000 --> 00:18:00,000
and to be mindful and to meditate

299
00:18:00,000 --> 00:18:04,000
on the criticism, on our reactions to the criticism,

300
00:18:04,000 --> 00:18:08,000
seeing how our mind reacts.

301
00:18:08,000 --> 00:18:12,000
And enlightened being is the same in both praise and blame.

302
00:18:12,000 --> 00:18:14,000
When people praise them,

303
00:18:14,000 --> 00:18:16,000
it's as though they didn't even hear it.

304
00:18:16,000 --> 00:18:18,000
They don't have any pleasure.

305
00:18:18,000 --> 00:18:20,000
They don't take pleasure when others.

306
00:18:20,000 --> 00:18:24,000
They aren't excited when other people praise them.

307
00:18:24,000 --> 00:18:28,000
But it's the same when others have insulted them

308
00:18:28,000 --> 00:18:30,000
or criticized them.

309
00:18:30,000 --> 00:18:33,000
It's also as though they didn't hear.

310
00:18:33,000 --> 00:18:36,000
In a sense, they take it for what it is.

311
00:18:36,000 --> 00:18:37,000
They take it at face value.

312
00:18:37,000 --> 00:18:40,000
This person just said I'm doing something wrong

313
00:18:40,000 --> 00:18:43,000
and said, okay, well, they look at it

314
00:18:43,000 --> 00:18:45,000
and right, I was doing that thing wrong

315
00:18:45,000 --> 00:18:48,000
or this person's upset at me

316
00:18:48,000 --> 00:18:50,000
and they want me to do this

317
00:18:50,000 --> 00:18:52,000
or they want me to stop doing that.

318
00:18:52,000 --> 00:18:53,000
Okay, well, I'll stop doing it

319
00:18:53,000 --> 00:18:55,000
because that would upset them.

320
00:18:55,000 --> 00:18:56,000
You know, to a certain extent,

321
00:18:56,000 --> 00:18:58,000
but they do everything mindfully

322
00:18:58,000 --> 00:19:01,000
and with wisdom, knowing which is the right state.

323
00:19:01,000 --> 00:19:02,000
It's not that there are pushover

324
00:19:02,000 --> 00:19:04,000
and someone tells them, you know,

325
00:19:04,000 --> 00:19:06,000
you're too fat, you should go on a diet.

326
00:19:06,000 --> 00:19:09,000
They don't really see the point of that.

327
00:19:09,000 --> 00:19:12,000
Why would I concern myself with my weight?

328
00:19:12,000 --> 00:19:14,000
You're too skinny, you should eat more.

329
00:19:14,000 --> 00:19:17,000
Well, that's not why I eat, so,

330
00:19:17,000 --> 00:19:19,000
but they don't get upset, you see.

331
00:19:19,000 --> 00:19:21,000
The point is they're in perturbed

332
00:19:21,000 --> 00:19:23,000
in both praise and blame

333
00:19:23,000 --> 00:19:24,000
and we have to look at both

334
00:19:24,000 --> 00:19:27,000
because this is relating to the ego.

335
00:19:27,000 --> 00:19:31,000
It's relating to our conceit,

336
00:19:31,000 --> 00:19:34,000
our self, our view of self.

337
00:19:34,000 --> 00:19:37,000
It's a good indicator.

338
00:19:37,000 --> 00:19:39,000
And the other part of this quote

339
00:19:39,000 --> 00:19:41,000
is in regards to staying with such a person.

340
00:19:41,000 --> 00:19:43,000
The point, the specific point that things get better

341
00:19:43,000 --> 00:19:46,000
and that's a very useful advice to give oneself

342
00:19:46,000 --> 00:19:47,000
when one is angry

343
00:19:47,000 --> 00:19:49,000
because you'll often get angry at your teacher.

344
00:19:49,000 --> 00:19:50,000
It's a common thing.

345
00:19:50,000 --> 00:19:54,000
I had one student recently who said,

346
00:19:54,000 --> 00:19:56,000
I'm very angry at you.

347
00:19:56,000 --> 00:19:57,000
You know, the reason I think that,

348
00:19:57,000 --> 00:19:59,000
those are the words,

349
00:19:59,000 --> 00:20:02,000
I'm very angry at you right now

350
00:20:02,000 --> 00:20:04,000
because I wasn't even criticism.

351
00:20:04,000 --> 00:20:06,000
It was asking to do something

352
00:20:06,000 --> 00:20:08,000
that just seems that it seemed

353
00:20:08,000 --> 00:20:09,000
that early ridiculous.

354
00:20:09,000 --> 00:20:11,000
I've seen them totally above and beyond

355
00:20:11,000 --> 00:20:15,000
what they were capable of.

356
00:20:15,000 --> 00:20:19,000
And I said, well, I didn't.

357
00:20:19,000 --> 00:20:20,000
What was it?

358
00:20:20,000 --> 00:20:21,000
I didn't.

359
00:20:21,000 --> 00:20:22,000
This was my idea.

360
00:20:22,000 --> 00:20:24,000
I wasn't the one who created this decision.

361
00:20:24,000 --> 00:20:25,000
And they said,

362
00:20:25,000 --> 00:20:27,000
I'm very angry.

363
00:20:27,000 --> 00:20:30,000
They're very angry about it.

364
00:20:30,000 --> 00:20:34,000
And this is quite common.

365
00:20:34,000 --> 00:20:36,000
It's quite common for meditators

366
00:20:36,000 --> 00:20:38,000
to get angry at their teacher.

367
00:20:38,000 --> 00:20:42,000
And so there's no shame in that.

368
00:20:42,000 --> 00:20:44,000
And there's no reason to get upset.

369
00:20:44,000 --> 00:20:46,000
It's why we ask forgiveness

370
00:20:46,000 --> 00:20:48,000
when we do a meditation course.

371
00:20:48,000 --> 00:20:50,000
We often formally ask forgiveness of the teacher

372
00:20:50,000 --> 00:20:53,000
and the teacher asks forgiveness of the student

373
00:20:53,000 --> 00:20:55,000
both when we start the course

374
00:20:55,000 --> 00:20:56,000
and when we finish the course.

375
00:20:56,000 --> 00:21:00,000
It's sort of clear the air.

376
00:21:00,000 --> 00:21:02,000
But the point is,

377
00:21:02,000 --> 00:21:04,000
this verse is quite useful.

378
00:21:04,000 --> 00:21:06,000
It's been useful for me

379
00:21:06,000 --> 00:21:10,000
to remind myself that no matter how difficult things get,

380
00:21:10,000 --> 00:21:13,000
staying with a teacher and an meditation center.

381
00:21:13,000 --> 00:21:15,000
We have to remember that

382
00:21:15,000 --> 00:21:20,000
we want to do that which makes things better,

383
00:21:20,000 --> 00:21:23,000
makes it better, makes us better people,

384
00:21:23,000 --> 00:21:26,000
improves our situation, makes us happier.

385
00:21:26,000 --> 00:21:29,000
We have to remember that we know this.

386
00:21:29,000 --> 00:21:31,000
We know that staying in the meditation center

387
00:21:31,000 --> 00:21:33,000
with a teacher is going to make things better.

388
00:21:33,000 --> 00:21:35,000
We often make excuses.

389
00:21:35,000 --> 00:21:41,000
I'm not ready or the environment's not right.

390
00:21:41,000 --> 00:21:43,000
I'm not at the right point in my life.

391
00:21:43,000 --> 00:21:45,000
I remember that this is all excuses

392
00:21:45,000 --> 00:21:49,000
that the truth is staying

393
00:21:49,000 --> 00:21:50,000
in the meditation center

394
00:21:50,000 --> 00:21:51,000
with the meditation center.

395
00:21:51,000 --> 00:21:53,000
Things will get better.

396
00:21:53,000 --> 00:21:57,000
And whatever excuses we have for not staying

397
00:21:57,000 --> 00:21:59,000
are invalid because here is a person

398
00:21:59,000 --> 00:22:02,000
who is willing to help us with our faults.

399
00:22:02,000 --> 00:22:05,000
The faults are what we're focusing on.

400
00:22:05,000 --> 00:22:09,000
And people looking to criticize Buddhism

401
00:22:09,000 --> 00:22:11,000
will use this as a criticism

402
00:22:11,000 --> 00:22:13,000
and Buddhism is very pessimistic,

403
00:22:13,000 --> 00:22:14,000
which is ridiculous.

404
00:22:14,000 --> 00:22:19,000
Underly, ridiculous harm was a terrible, terrible criticism.

405
00:22:19,000 --> 00:22:23,000
I mean by often by lazy people

406
00:22:23,000 --> 00:22:27,000
who aren't interested in looking at their faults.

407
00:22:27,000 --> 00:22:30,000
Because it's very damaging to say that.

408
00:22:30,000 --> 00:22:33,000
The faults are the only reason

409
00:22:33,000 --> 00:22:35,000
for self-development.

410
00:22:35,000 --> 00:22:38,000
It is our faults that we want to focus on.

411
00:22:38,000 --> 00:22:40,000
Not so that we can feel bad about ourselves

412
00:22:40,000 --> 00:22:42,000
or feel horrible people,

413
00:22:42,000 --> 00:22:44,000
but to actually fix something.

414
00:22:44,000 --> 00:22:45,000
To make something better.

415
00:22:45,000 --> 00:22:47,000
How do you make something better

416
00:22:47,000 --> 00:22:49,000
without looking at what's wrong?

417
00:22:49,000 --> 00:22:50,000
Looking at the problem.

418
00:22:50,000 --> 00:22:51,000
And this is what we do.

419
00:22:51,000 --> 00:22:53,000
If you don't have someone

420
00:22:53,000 --> 00:22:54,000
who can show you your fault,

421
00:22:54,000 --> 00:22:55,000
so you can point them out to.

422
00:22:55,000 --> 00:22:58,000
You can help you fix them.

423
00:22:58,000 --> 00:23:02,000
Obviously, it's not enough to point out one's faults,

424
00:23:02,000 --> 00:23:04,000
but to actually help you fix them.

425
00:23:04,000 --> 00:23:06,000
This is where things get better.

426
00:23:06,000 --> 00:23:15,000
This is where true development and progress and goodness comes from.

427
00:23:15,000 --> 00:23:20,000
So a very useful verse, something that we should remember.

428
00:23:20,000 --> 00:23:22,000
Someone who points out your faults.

429
00:23:22,000 --> 00:23:24,000
When they see them,

430
00:23:24,000 --> 00:23:26,000
you should liken them to a person

431
00:23:26,000 --> 00:23:28,000
who points out very treasure.

432
00:23:28,000 --> 00:23:29,000
When you stay with,

433
00:23:29,000 --> 00:23:31,000
you should associate with such a person.

434
00:23:31,000 --> 00:23:34,000
The wise should associate with such a person.

435
00:23:34,000 --> 00:23:36,000
When such a person,

436
00:23:36,000 --> 00:23:38,000
when one associates with such a person,

437
00:23:38,000 --> 00:23:39,000
things get better.

438
00:23:39,000 --> 00:23:40,000
Not worse.

439
00:23:40,000 --> 00:23:42,000
Say you whole team about you.

440
00:23:42,000 --> 00:23:45,000
That's the verse for today.

441
00:23:45,000 --> 00:23:47,000
That's our teaching of the Dhamupada.

442
00:23:47,000 --> 00:23:49,000
Thank you for tuning in.

443
00:23:49,000 --> 00:23:56,000
Wishing you all the best.

