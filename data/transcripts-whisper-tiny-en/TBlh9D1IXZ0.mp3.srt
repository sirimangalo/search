1
00:00:00,000 --> 00:00:07,000
Can you talk about how to develop friendliness? It is very hard for me to be open-hearted.

2
00:00:07,000 --> 00:00:11,000
I always have a hard and unfriendly approach.

3
00:00:16,000 --> 00:00:20,000
What do you guys think?

4
00:00:20,000 --> 00:00:23,000
I want to have...

5
00:00:23,000 --> 00:00:27,000
Do you want to develop more friendliness? Is that the question?

6
00:00:27,000 --> 00:00:31,000
Yeah, how do develop friendliness make that?

7
00:00:31,000 --> 00:00:33,000
It is very hard for me to be open-hearted.

8
00:00:33,000 --> 00:00:36,000
I always have a hard and unfriendly approach.

9
00:00:36,000 --> 00:00:41,000
Maybe Owen can tell us what is going on in the Brahma Vihara Dhamma and Mahasizaya Dha.

10
00:00:41,000 --> 00:00:44,000
What is his call on this?

11
00:00:47,000 --> 00:00:48,000
He is Owen there.

12
00:00:48,000 --> 00:00:50,000
Oh, he left. Who is it?

13
00:00:50,000 --> 00:00:51,000
Owen, he is.

14
00:00:51,000 --> 00:00:53,000
I don't hear Owen.

15
00:00:53,000 --> 00:00:56,000
It is because I have taken my microphone off and I have a real nice one.

16
00:00:56,000 --> 00:00:59,000
I am talking away, giving you the answer.

17
00:00:59,000 --> 00:01:02,000
And my microphone is turned off.

18
00:01:02,000 --> 00:01:05,000
Well done.

19
00:01:05,000 --> 00:01:07,000
Was it good in?

20
00:01:07,000 --> 00:01:10,000
Yeah, well, maybe.

21
00:01:10,000 --> 00:01:19,000
Now what I am saying is the best thing to do would be to go and read the discourse

22
00:01:19,000 --> 00:01:31,000
on about half way through it and develop in a such loving kindness for all beings,

23
00:01:31,000 --> 00:01:40,000
where there are the closest people to you or your worst enemies.

24
00:01:40,000 --> 00:01:50,000
It is building up that general sense of loving kindness to all.

25
00:01:50,000 --> 00:01:59,000
From reading the book, it states that you should start to develop a meta towards the people

26
00:01:59,000 --> 00:02:03,000
furthest away from you as such.

27
00:02:03,000 --> 00:02:09,000
If you have enemies or people you dislike, you should start off by developing

28
00:02:09,000 --> 00:02:16,000
loving kindness towards those kind of people first of all.

29
00:02:16,000 --> 00:02:25,000
And then follow through a process of trying to develop for all others.

30
00:02:25,000 --> 00:02:29,000
Are you sure it doesn't say normally they say to go for people you love first?

31
00:02:29,000 --> 00:02:34,000
No, I was really sure it said because I was quite surprised when it was saying

32
00:02:34,000 --> 00:02:41,000
that you should develop loving kindness for those who aren't as closest to you

33
00:02:41,000 --> 00:02:49,000
because you naturally give loving kindness to those very close to you as family as such.

34
00:02:49,000 --> 00:02:54,000
I know you are not supposed to develop it towards someone of the opposite gender

35
00:02:54,000 --> 00:02:59,000
if you are a heterosexual person because that can have problems.

36
00:02:59,000 --> 00:03:04,000
But it is very good and I definitely recommend

37
00:03:04,000 --> 00:03:11,000
if you are seriously trying to develop an area in your life

38
00:03:11,000 --> 00:03:21,000
to start reading up and practice in the techniques that are in there.

39
00:03:21,000 --> 00:03:26,000
Like you said earlier, once you start reading a bit of a Mahasi side or book

40
00:03:26,000 --> 00:03:34,000
you instantly want to go away and start either meditating or practicing

41
00:03:34,000 --> 00:03:39,000
what is teaching.

42
00:03:39,000 --> 00:03:46,000
Actually science is actually showing how the brain actually rewires itself

43
00:03:46,000 --> 00:03:51,000
from doing these practices. So the meta meditation, compassion,

44
00:03:51,000 --> 00:03:54,000
meditations actually help the brain.

45
00:03:54,000 --> 00:03:59,000
So if you are looking for like you asked to develop friendliness,

46
00:03:59,000 --> 00:04:04,000
it is probably the best way.

47
00:04:04,000 --> 00:04:08,000
Method meditation, compassion, meditations.

48
00:04:08,000 --> 00:04:13,000
You can actually rewire your transmitters in your brain.

49
00:04:13,000 --> 00:04:16,000
Take some time to develop it.

50
00:04:16,000 --> 00:04:19,000
But the other thing is not worry about it so much.

51
00:04:19,000 --> 00:04:23,000
Don't hate yourself because you are hard and unfriendly.

52
00:04:23,000 --> 00:04:27,000
If you have got anger, that is the reason to develop.

53
00:04:27,000 --> 00:04:32,000
Some people are just hard by nature and will end up being very much alone

54
00:04:32,000 --> 00:04:38,000
because their very nature is unfriendly.

55
00:04:38,000 --> 00:04:45,000
In a non-emotional way, they have an abrupt way about them.

56
00:04:45,000 --> 00:04:50,000
That is just who they are. We are all different.

57
00:04:50,000 --> 00:04:58,000
If you judge yourself and let people's reactions determine your actions

58
00:04:58,000 --> 00:05:05,000
then you are just always going to be disappointed and upset.

59
00:05:05,000 --> 00:05:09,000
So there is nothing wrong with.

60
00:05:09,000 --> 00:05:13,000
It goes back to talking about conflict in relationships.

61
00:05:13,000 --> 00:05:16,000
Sometimes your behavior will cause conflict.

62
00:05:16,000 --> 00:05:20,000
People will not like your behavior even though you are not.

63
00:05:20,000 --> 00:05:25,000
You have no bad intentions in your heart.

64
00:05:25,000 --> 00:05:28,000
People will still be upset at what you do.

65
00:05:28,000 --> 00:05:30,000
That was one big thing.

66
00:05:30,000 --> 00:05:32,000
One big relief in the meta meditation.

67
00:05:32,000 --> 00:05:37,000
I don't really care if they don't like what I do.

68
00:05:37,000 --> 00:05:41,000
There is no logical reason to be concerned.

69
00:05:41,000 --> 00:05:46,000
If other people get angry because of the way I behave.

70
00:05:46,000 --> 00:05:49,000
You are not loving enough.

71
00:05:49,000 --> 00:05:50,000
You are not loving enough.

72
00:05:50,000 --> 00:05:51,000
You are not.

73
00:05:51,000 --> 00:05:53,000
I had one monkey let me.

74
00:05:53,000 --> 00:05:57,000
I did something kind of wrong but looking back it was really just.

75
00:05:57,000 --> 00:06:04,000
I just expressed my opinion about something that concerned me very publicly

76
00:06:04,000 --> 00:06:07,000
on my web blog and got in trouble for it.

77
00:06:07,000 --> 00:06:10,000
So he called me into his room and said,

78
00:06:10,000 --> 00:06:14,000
you are a real troublemaker or something like that.

79
00:06:14,000 --> 00:06:18,000
He said, meditation teachers have to have loving kind and he was yelling at me.

80
00:06:18,000 --> 00:06:20,000
They have loving kindness for people.

81
00:06:20,000 --> 00:06:23,000
They have made that for people.

82
00:06:23,000 --> 00:06:28,000
I am like looking at them.

83
00:06:28,000 --> 00:06:37,000
It is not the right lesson of loving kindness.

84
00:06:37,000 --> 00:06:38,000
But yeah.

85
00:06:38,000 --> 00:06:48,000
So it is not really necessary to be open-hearted or this or that.

86
00:06:48,000 --> 00:06:53,000
It is not the right lesson.

87
00:06:53,000 --> 00:06:58,000
People might hate you even though you are a nice person.

88
00:06:58,000 --> 00:07:04,000
It can happen.

89
00:07:04,000 --> 00:07:06,000
Sometimes conflicts just happen.

90
00:07:06,000 --> 00:07:08,000
Misunderstandings are so easy.

91
00:07:08,000 --> 00:07:11,000
It is amazing how easy your misunderstandings are.

92
00:07:11,000 --> 00:07:13,000
Some of them misunderstandings I have seen.

93
00:07:13,000 --> 00:07:14,000
Think about it.

94
00:07:14,000 --> 00:07:17,000
How you say something and you mean one thing.

95
00:07:17,000 --> 00:07:23,000
It is totally innocent and how it just can destroy relationships.

96
00:07:23,000 --> 00:07:28,000
It didn't even mean that.

97
00:07:28,000 --> 00:07:34,000
It is horrible in Thailand because my tie is,

98
00:07:34,000 --> 00:07:36,000
I had to learn ties.

99
00:07:36,000 --> 00:07:38,000
Sometimes I would say things.

100
00:07:38,000 --> 00:07:41,000
And they would be interpreted totally the wrong way.

101
00:07:41,000 --> 00:07:47,000
Because tie is very nuanced and when you say something,

102
00:07:47,000 --> 00:07:51,000
the words you use have incredible meanings.

103
00:07:51,000 --> 00:07:56,000
Simple words can have incredible meaning because it is all in the,

104
00:07:56,000 --> 00:08:02,000
not in the meaning of the words but in the meaning that is implied by them.

105
00:08:02,000 --> 00:08:06,000
As a Westerner, you just don't have that implication.

106
00:08:06,000 --> 00:08:08,000
You don't mean that.

107
00:08:08,000 --> 00:08:11,000
But it is taken very much the wrong way.

108
00:08:11,000 --> 00:08:12,000
It can be taken very much the wrong way.

109
00:08:12,000 --> 00:08:17,000
I had real headaches because of that.

110
00:08:17,000 --> 00:08:18,000
But that is it.

111
00:08:18,000 --> 00:08:20,000
You just learn that and all that.

112
00:08:20,000 --> 00:08:21,000
That is the way.

113
00:08:21,000 --> 00:08:24,000
I might wind up having an in fact idea.

114
00:08:24,000 --> 00:08:34,000
For example, there was one case where someone,

115
00:08:34,000 --> 00:08:39,000
I had these Buddha images in my possession.

116
00:08:39,000 --> 00:08:44,000
And someone came, someone told me and said,

117
00:08:44,000 --> 00:08:46,000
oh, yes.

118
00:08:46,000 --> 00:08:49,000
Make those intonically into mala's.

119
00:08:49,000 --> 00:08:52,000
And that is what they are for.

120
00:08:52,000 --> 00:08:54,000
It shouldn't be the person they belong to.

121
00:08:54,000 --> 00:08:56,000
That is what they are for.

122
00:08:56,000 --> 00:08:59,000
And this was someone who was in a position of authority.

123
00:08:59,000 --> 00:09:01,000
And so I did this.

124
00:09:01,000 --> 00:09:03,000
I got them all prepared and I made them up.

125
00:09:03,000 --> 00:09:06,000
And then I got in real trouble from the person that they belong to.

126
00:09:06,000 --> 00:09:09,000
You know, look, he didn't even say it because people will never

127
00:09:09,000 --> 00:09:11,000
ever confront you.

128
00:09:11,000 --> 00:09:16,000
But I mean, very clear that basically I was stealing or using

129
00:09:16,000 --> 00:09:17,000
incorrectly.

130
00:09:17,000 --> 00:09:20,000
It's something that I had no permission to use.

131
00:09:20,000 --> 00:09:24,000
And I never told them that it was someone else who had told me to do this.

132
00:09:24,000 --> 00:09:28,000
Someone else in authority because I didn't want to create

133
00:09:28,000 --> 00:09:30,000
problems between those two people.

134
00:09:30,000 --> 00:09:37,000
That if I wasn't in trouble because of it, this other person won't get in trouble because of it.

135
00:09:37,000 --> 00:09:42,000
I only bring it up because it was a good example of where my relationship

136
00:09:42,000 --> 00:09:47,000
with this other person was strained because of something I had never done,

137
00:09:47,000 --> 00:09:49,000
because something I wasn't even guilty of.

138
00:09:49,000 --> 00:09:55,000
And that's common in my, I can think of a dozen instances where that's happened to me.

139
00:09:55,000 --> 00:09:59,000
I know that even my teacher at times, there were times where my teacher

140
00:09:59,000 --> 00:10:00,000
got the wrong impression.

141
00:10:00,000 --> 00:10:03,000
People gave him wrong information about me so that I'd done this or the

142
00:10:03,000 --> 00:10:04,000
math.

143
00:10:04,000 --> 00:10:07,000
And then he rather than confront me about it because that's the thing in

144
00:10:07,000 --> 00:10:09,000
time that they don't say, why did you do this?

145
00:10:09,000 --> 00:10:10,000
Why did you do that?

146
00:10:10,000 --> 00:10:13,000
He just said, don't, don't do X.

147
00:10:13,000 --> 00:10:14,000
Don't go and do this.

148
00:10:14,000 --> 00:10:16,000
And I said, okay.

149
00:10:16,000 --> 00:10:17,000
And I knew what he was relating to.

150
00:10:17,000 --> 00:10:21,000
I knew that someone had accused me of doing something that I hadn't done.

151
00:10:21,000 --> 00:10:22,000
And I just said, I won't do it.

152
00:10:22,000 --> 00:10:24,000
And that was that.

153
00:10:24,000 --> 00:10:28,000
So potentially, if my teacher hadn't been the wonderful person that he was,

154
00:10:28,000 --> 00:10:35,000
he could have been very much turned off or upset about that.

155
00:10:35,000 --> 00:10:36,000
That just happens in life.

156
00:10:36,000 --> 00:10:41,000
So I just wanted to point out that you don't have to

157
00:10:41,000 --> 00:10:44,000
endear yourself to people.

158
00:10:44,000 --> 00:10:47,000
And you don't have to worry about your behavior.

159
00:10:47,000 --> 00:10:51,000
Worry about your intentions.

160
00:10:51,000 --> 00:10:55,000
Some people are very nice and very kind to people and actually miserable

161
00:10:55,000 --> 00:10:58,000
inside or terrible people inside.

162
00:10:58,000 --> 00:11:01,000
Very good at winning friends.

163
00:11:01,000 --> 00:11:04,000
You know, the four kinds of meetapati,

164
00:11:04,000 --> 00:11:07,000
Rupati, Rupati, Rupati, Rupati.

165
00:11:07,000 --> 00:11:12,000
Fake friendings.

166
00:11:12,000 --> 00:11:13,000
I don't know.

167
00:11:13,000 --> 00:11:16,000
It wasn't even quite what you were asking, but certainly the answer is correct.

168
00:11:16,000 --> 00:11:19,000
Developing kindness, that's the best way.

169
00:11:19,000 --> 00:11:22,000
I just wanted to say, don't worry about it so much.

170
00:11:22,000 --> 00:11:29,000
As long as you have your personal people might hate you.

171
00:11:29,000 --> 00:11:31,000
I had everyone in the monastery.

172
00:11:31,000 --> 00:11:34,000
Not many people.

173
00:11:34,000 --> 00:11:39,000
And it seemed like the whole domestic community was upset with me, except my teacher.

174
00:11:39,000 --> 00:11:40,000
And so I was like, well, that's fine.

175
00:11:40,000 --> 00:11:42,000
I don't know what I care about this, my teacher.

176
00:11:42,000 --> 00:11:45,000
And that's how a lot of people are in my monastery.

177
00:11:45,000 --> 00:11:48,000
They fight and they have lots of problems with each other, but they don't care

178
00:11:48,000 --> 00:11:52,000
because they're only there for one person.

179
00:11:52,000 --> 00:11:56,000
And so that's kind of how the place gets along.

180
00:11:56,000 --> 00:11:59,000
But it was interesting to feel so much hatred from people.

181
00:11:59,000 --> 00:12:02,000
Like really vicious mean looks and everything.

182
00:12:02,000 --> 00:12:05,000
And I was so happy because I was with my teacher.

183
00:12:05,000 --> 00:12:08,000
And I was like, this is the...

184
00:12:08,000 --> 00:12:11,000
I wouldn't have traded it for anything.

185
00:12:11,000 --> 00:12:16,000
Which is an interesting situation.

186
00:12:16,000 --> 00:12:20,000
So I'm not just getting a wish for...

187
00:12:20,000 --> 00:12:21,000
No.

188
00:12:21,000 --> 00:12:22,000
That other person too.

189
00:12:22,000 --> 00:12:24,000
It might be mad at you, whatever.

190
00:12:24,000 --> 00:12:26,000
And just mental.

191
00:12:26,000 --> 00:12:27,000
Yeah.

192
00:12:27,000 --> 00:12:32,000
Well, I hope that person gets more compassionate for themselves.

193
00:12:32,000 --> 00:12:38,000
Kind of eases off the reaction to what they're giving to you.

194
00:12:38,000 --> 00:12:39,000
Yeah.

195
00:12:39,000 --> 00:12:41,000
I'm going to let me kind this...

196
00:12:41,000 --> 00:12:43,000
I'm going to kind this meditation concert.

197
00:12:43,000 --> 00:12:46,000
I will. Yeah.

198
00:12:46,000 --> 00:12:48,000
If you have to really deal with them, you just kind of...

199
00:12:48,000 --> 00:12:51,000
You're in tensions there.

200
00:12:51,000 --> 00:13:14,000
You can just let it be.

