1
00:00:00,000 --> 00:00:08,960
and we're live here in the Buddhist Center at Deer Park on a wonderful Sunday afternoon

2
00:00:09,680 --> 00:00:18,160
evening morning midnight depending on your location and the settings you've chosen for second life

3
00:00:18,160 --> 00:00:33,360
so everyone let's get in the mood no everyone put your meditator face on sit down make sure

4
00:00:33,360 --> 00:00:45,040
you're sitting in real life turned off the music shut down Facebook across your legs straighten

5
00:00:45,040 --> 00:01:00,320
your back close your eyes you can open your eyes if you want but let's get meditator

6
00:01:00,320 --> 00:01:16,160
the teachings of the Lord Buddha are meant to address a problem

7
00:01:20,960 --> 00:01:26,240
that's really I think how we should look at the teachings of the Buddha

8
00:01:26,240 --> 00:01:32,800
it proposes that there's a problem and offers a solution

9
00:01:37,280 --> 00:01:41,840
I suppose you could generalize and say that's the nature of religion

10
00:01:44,560 --> 00:01:52,080
and you might even be so if you want it to be negative about it

11
00:01:52,080 --> 00:02:02,880
you can say it's it's the religion game it's how people how how religions attract followers

12
00:02:05,120 --> 00:02:12,080
sometimes by pointing out a problem

13
00:02:14,720 --> 00:02:17,840
well always pointing out a problem that people weren't aware of

14
00:02:17,840 --> 00:02:23,600
but it seems often actually creating a problem where there was

15
00:02:25,360 --> 00:02:28,720
proposing a problem where it isn't obvious that there is one

16
00:02:32,320 --> 00:02:37,840
in order to make people afraid or are concerned

17
00:02:37,840 --> 00:02:45,920
and then proposing to have the answer in the way out

18
00:02:50,480 --> 00:02:54,160
and I think it's true that yes there there's a great potential for abuse here

19
00:02:54,880 --> 00:02:57,120
and certainly there is a lot of abuse that goes on

20
00:02:57,120 --> 00:03:04,000
many religious systems cults and so on

21
00:03:05,280 --> 00:03:16,560
exploit people's fears and their attachments

22
00:03:19,920 --> 00:03:20,800
in order to

23
00:03:20,800 --> 00:03:29,680
become rich and powerful and famous and so on

24
00:03:31,600 --> 00:03:37,360
but it's essentially the

25
00:03:43,360 --> 00:03:44,560
the role of a physician

26
00:03:44,560 --> 00:03:54,320
it's the the way of a doctor to be able to point out a problem

27
00:03:56,160 --> 00:03:57,840
and offer a solution

28
00:04:03,680 --> 00:04:07,760
and sometimes the patient isn't even aware there's a problem they go to the doctor for a checkup

29
00:04:07,760 --> 00:04:23,680
and leave the check out the checkup with some bad news

30
00:04:38,560 --> 00:04:48,000
other times they come to the doctor with a problem not knowing what the cause is and the

31
00:04:48,000 --> 00:04:54,160
doctors able to explain the cause and offer a solution so the both of these

32
00:04:57,600 --> 00:05:02,080
examples work as in comparison to the Buddhist teaching

33
00:05:02,080 --> 00:05:07,760
some people live their lives with no thought that anything's wrong

34
00:05:08,880 --> 00:05:14,080
and they might go to see the Buddha or go to see a Buddhist teacher

35
00:05:19,360 --> 00:05:24,880
just for for discussion or to learn something or so on

36
00:05:24,880 --> 00:05:33,360
and find out through their dialogue and through practicing you know through practicing meditation

37
00:05:33,360 --> 00:05:39,360
and so on that actually there was a big problem that they weren't aware of that they were

38
00:05:39,360 --> 00:05:44,320
setting themselves up for a great amount of suffering in the future

39
00:05:44,320 --> 00:05:55,920
but I think for most of us the other form is the one that that we're best able to relate to

40
00:05:55,920 --> 00:06:02,000
it's the idea that we have suffering we have a problem we know there's a problem we know that

41
00:06:02,000 --> 00:06:15,600
life's not perfect we're able to see suffering in our lives we don't think

42
00:06:15,600 --> 00:06:21,360
I don't think many people would deny that that it exists in in our lives so I think this is

43
00:06:21,360 --> 00:06:31,840
important because it then dictates the way we approach the Buddhist teaching and it will

44
00:06:31,840 --> 00:06:43,520
shape our our practice our way of relating and engaging in the practice of the Buddhist teaching

45
00:06:43,520 --> 00:07:03,760
in the way that we ask ourselves what's the problem and we remind ourselves that

46
00:07:03,760 --> 00:07:15,680
that what we're doing here is trying to discover the problem and find a way to remove the

47
00:07:15,680 --> 00:07:24,400
problem why I think we should we have to keep this in mind is because it's easy to lose

48
00:07:24,400 --> 00:07:34,160
side of it in the practice of Buddhism and the Buddha was the Buddha gave several examples of this

49
00:07:34,160 --> 00:07:41,360
that some people come to the Buddhist teaching and simply study it engage in intensive

50
00:07:42,640 --> 00:07:47,520
study memorization intellectual discussion and debate

51
00:07:47,520 --> 00:07:58,960
many people nowadays engage in reading studying this is this is for many people

52
00:08:00,640 --> 00:08:07,600
the way of of practicing religion is to study it

53
00:08:07,600 --> 00:08:18,240
for many people in the practice is not so important as the study and the intellectual

54
00:08:18,240 --> 00:08:26,080
appreciation of the teachings for other people

55
00:08:26,080 --> 00:08:41,840
it can be the the memorization of the teachings there are in Buddhist circles there are

56
00:08:41,840 --> 00:08:51,600
people who actually are able to remember vast tracts of of of discourses and teachings of the

57
00:08:51,600 --> 00:09:01,520
Buddha right now we have a project on the in over the over email it's a group project to

58
00:09:01,520 --> 00:09:11,200
memorize part of the Buddha's teaching starting from the very the first sutta in the in in the

59
00:09:11,200 --> 00:09:22,480
poly sutta pitika and people do this as a religious practice this is common in other religions

60
00:09:22,480 --> 00:09:36,880
as well I know in in Judaism it's common or it's you can find people who are actually able to

61
00:09:36,880 --> 00:09:51,520
apparently take a Jewish Bible the Torah stick a needle through it or or theoretically you know

62
00:09:52,480 --> 00:10:01,360
pretend that if they stuck a needle through the Torah they would be able to tell you which word

63
00:10:01,360 --> 00:10:06,320
based on the first word that it punctured they would be able to tell you the word on each page

64
00:10:06,320 --> 00:10:20,640
from beginning to end so I think this is this has been lost in the modern era there isn't so

65
00:10:20,640 --> 00:10:30,000
much of this reliance on on the memorization of texts but it certainly has been and continues to

66
00:10:30,000 --> 00:10:37,200
be in religious circles an important religious practice as though memorizing and chanting being

67
00:10:37,200 --> 00:10:45,280
able to chant being able to recite the teachings of the Buddha being able to pair it back I suppose

68
00:10:45,280 --> 00:10:53,280
in modern days you find this and what we call quote dropping where people are able to quote famous

69
00:10:53,280 --> 00:11:02,080
teachers and I find this actually quite common in many schools of Buddhism that you'll you'll find

70
00:11:02,080 --> 00:11:09,280
people rather than even quoting the Buddha they're constantly quoting their teachers while my

71
00:11:09,280 --> 00:11:17,680
teacher says this my teacher says that as though somehow it it it made it true or it had some

72
00:11:17,680 --> 00:11:30,240
bearing on on the person that they were talking to so so without perhaps the ability to explain it

73
00:11:30,240 --> 00:11:36,880
or or even just this this reliance on that as a as a religious practice to be able to quote

74
00:11:36,880 --> 00:11:47,600
scripture and to recite and to chant and to memorize then the Buddha said there are other people

75
00:11:47,600 --> 00:12:00,960
who who teach and even become great teachers without or by at the same time neglecting their own

76
00:12:00,960 --> 00:12:07,760
practice people who take all of their time to help other people or to teach to become famous

77
00:12:07,760 --> 00:12:14,160
teachers there was a story in the Buddha's time or maybe it was after the Buddha's time and

78
00:12:14,160 --> 00:12:21,520
story in ancient time but this monk who who fell into this trap I think I've told this story

79
00:12:21,520 --> 00:12:30,480
before he was a great teacher and all of his students became enlightened to some degree or

80
00:12:30,480 --> 00:12:37,360
rather and some of them became so tapana some of them became Sakitakami some of them became

81
00:12:37,360 --> 00:12:50,320
Anakami and he himself had had neglected his own practice and was actually just a guy who

82
00:12:50,320 --> 00:12:56,800
happened to be able to teach quite well he was able to explain the Buddha's teaching in such a way

83
00:12:56,800 --> 00:13:01,120
that people were able to practice and he had an intellectual appreciation of it but he himself

84
00:13:01,120 --> 00:13:08,160
had never practiced or never exerted himself in intensive meditation practice

85
00:13:13,200 --> 00:13:20,080
and so all of his students were reaching these higher states of enlightenment and he himself

86
00:13:20,080 --> 00:13:27,120
was running around teaching and helping other people and one day one of his students decided

87
00:13:27,840 --> 00:13:32,640
asked himself one of his students who was an Anakami which means someone who has attained the third

88
00:13:32,640 --> 00:13:37,520
stage of enlightenment and is destined when they died to never come back to the world again

89
00:13:38,080 --> 00:13:46,080
but instead be reborn in the god realms or the Brahma realms and from there become fully enlightened

90
00:13:46,080 --> 00:13:53,360
and fully released and turned to the pure bones they're called and he asked himself

91
00:13:56,880 --> 00:14:01,920
wonder you know here I am I've I've realized this stage of enlightenment and all of these other

92
00:14:01,920 --> 00:14:06,960
people have I wonder what stage of enlightenment our teacher has realized and through his

93
00:14:06,960 --> 00:14:16,400
incredible strength of mind he was able to discern for himself just by thinking about it

94
00:14:17,360 --> 00:14:20,800
just by examining the situation and by sending his mind to the

95
00:14:21,840 --> 00:14:28,320
in this direction he was able to ascertain that his teacher had indeed not attained any level

96
00:14:28,320 --> 00:14:36,720
enlightenment and he thought oh well this won't do this is not a proper state of affairs

97
00:14:36,720 --> 00:14:40,400
and he's he thought to himself out of gratitude for his teacher that he owed it to him to

98
00:14:43,120 --> 00:14:44,720
to help him become enlightened as well

99
00:14:47,120 --> 00:14:51,280
so he went up to his teacher and and right when his teacher was was very very busy

100
00:14:52,480 --> 00:14:57,680
waited for when his teacher was busy helping people meeting with students and so on

101
00:14:57,680 --> 00:15:09,200
and he came up to him and he said teacher I'm I think I would like to request that it's time

102
00:15:09,200 --> 00:15:15,520
for a lesson and the teacher said I'm sorry I don't how can how can I possibly have time for

103
00:15:15,520 --> 00:15:20,560
a lesson right now I have many duties and people coming to see me and so on I'm far too busy for

104
00:15:20,560 --> 00:15:33,600
that and so this student of his sat down crossed his legs and went into a and absorbed an absorbed

105
00:15:33,600 --> 00:15:39,360
state and started floating off the ground and he opened his eyes and said to the teacher he said

106
00:15:39,360 --> 00:15:45,200
you don't have time even for yourself how could you possibly have time to teach anything to me

107
00:15:45,200 --> 00:15:49,920
and he floated out the room out of the room

108
00:15:52,960 --> 00:15:58,160
showing to the teacher that that indeed to his students who are on a higher level than he was

109
00:16:00,080 --> 00:16:03,520
and this of course shook the teacher up invisibly

110
00:16:05,440 --> 00:16:12,400
and he decided that he'd had enough this teaching business was

111
00:16:12,400 --> 00:16:17,920
it was obviously not the proper way to approach the Buddha's teaching as the Buddha said

112
00:16:17,920 --> 00:16:26,080
set yourself in what is right set yourself in what is right first only then should you help others

113
00:16:27,360 --> 00:16:33,680
and so he decided to go off into the forest but he was such a famous teacher that it was very

114
00:16:33,680 --> 00:16:38,640
difficult he had to find a forest where there was no other people around where he wouldn't be

115
00:16:38,640 --> 00:16:44,080
bothered by people coming to ask him about meditation practice and finally he found this place

116
00:16:44,080 --> 00:16:51,680
no humans in the area and so he started his meditation and he he worked very hard walking back and

117
00:16:51,680 --> 00:17:03,440
forth sitting down and little did you know that that you know you can't really go anywhere

118
00:17:03,440 --> 00:17:07,520
where you're going to be alone that that whether there are humans or not there are always beings

119
00:17:07,520 --> 00:17:12,080
around whether they be animals or in this case angels there were angels in the forest

120
00:17:13,120 --> 00:17:19,680
watching him and that actually came down and started meditating with him when he walked

121
00:17:19,680 --> 00:17:23,680
they walked when he said they said when he sat they sat

122
00:17:27,680 --> 00:17:32,160
and so he practiced and practiced and gained nothing from it he was pushing very very hard

123
00:17:32,160 --> 00:17:37,600
and someone said he was probably practicing too hard pushing himself too hard wanting too much to

124
00:17:37,600 --> 00:17:44,400
become enlightened of course the wanting the needing the desire was blocking him it was stopping

125
00:17:44,400 --> 00:17:52,000
him from attaining the very thing he was looking for and so he got more and more frustrated and

126
00:17:52,000 --> 00:17:57,920
more and more upset until finally he broke down you know he's he he was under such pressure that

127
00:17:57,920 --> 00:18:03,280
no I'm this great teacher if I don't become enlightened what did I say to my students and

128
00:18:04,640 --> 00:18:10,080
until he finally snapped and he sat down and he started crying and

129
00:18:13,280 --> 00:18:16,960
he was crying and suddenly he heard someone else crying and he opened his eyes and there's

130
00:18:16,960 --> 00:18:24,000
this angel sitting down beside him crying crying and crying and crying and he stopped crying

131
00:18:24,000 --> 00:18:30,080
and he says what are you doing and the angel stopped crying and said oh I was crying and he said

132
00:18:30,080 --> 00:18:35,760
why and he said well you know you're the great teacher whatever you do that's got to be the way

133
00:18:35,760 --> 00:18:39,680
to become enlightened so I've just been following you when you walk you walk I walked when you sat

134
00:18:39,680 --> 00:18:44,320
I sat when you started crying I started crying I figured that's got to be the way to become enlightened

135
00:18:44,320 --> 00:18:56,880
and this hit him quite hard as well made him quite ashamed of his behavior and as a result of

136
00:18:56,880 --> 00:19:03,280
that he was able to wake up let go and and practice in such a way as to become enlightened

137
00:19:04,240 --> 00:19:11,840
and the point of the story being teaching people is not the most important point and it's easy

138
00:19:11,840 --> 00:19:17,600
to get off track in this way going out and trying to explain to other people how great Buddhism is

139
00:19:19,520 --> 00:19:25,120
it's something you can remember it's always a sign of immaturity when people are trying to

140
00:19:25,120 --> 00:19:33,520
teach when people approach you and start teaching you uninvited, unasked when people start giving advice

141
00:19:33,520 --> 00:19:44,560
without you having asked or even intimated a desire for any but this is this is very common

142
00:19:44,560 --> 00:19:52,320
especially when people begin to practice it's called it's an upiculate it's a defilement of

143
00:19:52,320 --> 00:19:57,040
insight it's something that arises when you start practicing correctly you start to think wow

144
00:19:57,040 --> 00:20:02,800
everyone else should practice this way and you lose sight of your own practice and start trying to

145
00:20:02,800 --> 00:20:05,440
teach others

146
00:20:13,520 --> 00:20:19,600
another thing that people often do in that the Buddha mentioned that the people will often do

147
00:20:20,240 --> 00:20:29,360
in approaching the teachings of the Buddha is to think about it to ponder on it and to work it out

148
00:20:29,360 --> 00:20:38,960
lot work it out in their mind and people who debate or people who like to think and like to

149
00:20:38,960 --> 00:20:50,960
create people who write stories or write books people who relate to the Buddha's teaching intellectually

150
00:20:50,960 --> 00:20:57,760
so these are these are the sorts of ways that people interact with the Buddha's teaching that

151
00:20:57,760 --> 00:21:09,440
are not getting closer to a solution to the problem and so this is why it's important for us to

152
00:21:09,440 --> 00:21:13,840
always be asking yourselves what is the problem now the problem is not that we don't know enough

153
00:21:13,840 --> 00:21:18,160
that we haven't read enough of the Buddha's teaching it isn't that we don't know how to explain

154
00:21:18,160 --> 00:21:23,360
to other people there are some meditators who are never able to explain to others who find it

155
00:21:23,360 --> 00:21:29,920
very difficult to put into words the realizations that they have and on the other hand there are

156
00:21:29,920 --> 00:21:34,960
people who work very hard to be able to explain things to others and yet themselves are gaining

157
00:21:34,960 --> 00:21:53,760
nothing and are not progressing on the path the the real problem I guess you could say is

158
00:21:53,760 --> 00:22:02,960
is twofold and the problem that we have is that one our minds are not our minds are not

159
00:22:02,960 --> 00:22:16,960
tranquil enough and to our minds are lacking in wisdom and understanding so the Buddha's approach

160
00:22:16,960 --> 00:22:25,360
to the problem and defining a solution is twofold is one to to calm the mind to the practice of

161
00:22:25,360 --> 00:22:44,240
meditation to focus on an object focus on on reality in a in a way that stops the mind from

162
00:22:44,240 --> 00:22:54,720
flitting around superficially that allows us to grasp reality or grasp the object in front of us

163
00:22:54,720 --> 00:23:01,040
in a meaningful way to focus our minds and to calm our minds down and you can do this either

164
00:23:01,040 --> 00:23:10,000
with a a conceptual object or a part of reality so a conceptual object would be something that

165
00:23:10,000 --> 00:23:17,920
you create in your mind whether it be a light or a sound or a concept of being a god an angel

166
00:23:20,400 --> 00:23:30,160
an idea and reality would be something that is arising and ceasing that is coming and going

167
00:23:30,160 --> 00:23:39,840
in the in in experiential reality already that isn't created by the mind so watching the breath

168
00:23:39,840 --> 00:23:46,080
watching the stomach watching the feet watching pain or thoughts or emotions

169
00:23:52,160 --> 00:23:58,240
and this serves to calm the mind down the the the the solution here is that our minds no longer

170
00:23:58,240 --> 00:24:07,040
give rise to things like greed anger depression worry fear and so on our minds become fixed

171
00:24:07,040 --> 00:24:18,080
focused happy calm peaceful relaxed and for all intents and purposes become free from suffering

172
00:24:18,080 --> 00:24:29,360
for a time now the reason why this is not enough of course is it's still a contrived state

173
00:24:30,160 --> 00:24:38,560
that we've created this state through the work that we've done through the the pressure

174
00:24:38,560 --> 00:24:44,640
of our concentration which is able to suppress the defilements it's a good thing because it makes

175
00:24:44,640 --> 00:24:52,320
our mind very clear and very pure but it's a good thing primarily because it then allows us to

176
00:24:53,680 --> 00:25:02,960
fix things decisively completely and irrevocably or permanently

177
00:25:02,960 --> 00:25:12,560
because no matter how much focus and concentration you you apply to reality

178
00:25:14,400 --> 00:25:20,000
that that focus and concentration alone is not going to change your mind it's not going to change

179
00:25:20,000 --> 00:25:30,320
the way you think it's not going to change your habits it's it's not going to cut off the potential

180
00:25:30,320 --> 00:25:41,360
for the arising of defilements wisdom on the other hand does wisdom is the the final solution

181
00:25:41,360 --> 00:25:46,800
in the final cure and wisdom is something that comes about through this clarity of mind once you

182
00:25:46,800 --> 00:25:57,920
have a clear mind it's imperative that you focus entirely solely on ultimate reality whether you've

183
00:25:57,920 --> 00:26:03,680
been practicing to calm the mind based on a concept or whether ultimate reality when you're

184
00:26:03,680 --> 00:26:10,720
practicing to gain wisdom and insight you have to focus on ultimate reality but because you can't

185
00:26:10,720 --> 00:26:19,600
under you can't come to understand from reality in the other way you can't come to understand

186
00:26:19,600 --> 00:26:35,760
things as they are when you're creating the object of your attention so in essence then we

187
00:26:35,760 --> 00:26:40,320
have a twofold problem we have the problem of the defilements of the negative mind states and then

188
00:26:40,320 --> 00:26:46,080
we have the problem of the reason why they arise to stop them from arising is easy you focus

189
00:26:46,080 --> 00:26:53,280
the mind but to remove the cause of their arising you have to go deeper and you have to come to

190
00:26:53,280 --> 00:27:00,400
understand why they arise the the reason why defilements arises through a misunderstanding of

191
00:27:00,400 --> 00:27:07,600
reality for what it is not seeing things as they are a superficial grasp of the experience in

192
00:27:07,600 --> 00:27:15,760
front of us at every moment as an example or there's many examples one example is pain when you're

193
00:27:15,760 --> 00:27:23,520
sitting and you start to feel pain it's a very superficial experience where right away

194
00:27:25,840 --> 00:27:31,040
convinced that it's a negative experience that it's unpleasant it's negative that and we've

195
00:27:31,040 --> 00:27:40,480
right away acting on our our habit or habitual way of approaching problems we've already

196
00:27:40,480 --> 00:27:49,840
decided and and begun to react and switch positions or find a solution a way to get rid of the pain

197
00:27:52,000 --> 00:27:57,040
when we focus on things like pain on a deeper level when we look at them clearly and objectively

198
00:27:57,680 --> 00:28:02,480
we come to see that actually it's not so at all there's nothing intrinsically negative about things

199
00:28:02,480 --> 00:28:17,120
like pain or people yelling at us or smells or tastes or sound sites the experience of the

200
00:28:17,120 --> 00:28:26,320
sense even even when there are people yelling at us or attacking us or unpleasant sites or

201
00:28:26,320 --> 00:28:35,920
sounds or smells tastes feelings thoughts bad memories worries about the future problems in our

202
00:28:35,920 --> 00:28:45,280
life not having enough money not having a job not having a future and so on and so on hunger

203
00:28:45,280 --> 00:28:56,720
there's hot cold when we folk when we examine these things on a deeper level which is exactly

204
00:28:56,720 --> 00:29:01,600
what we're doing through meditation we come to see that it's it's it's it's not we come to see

205
00:29:01,600 --> 00:29:07,840
things in an in a quite a different way that there actually is nothing negative about any of these

206
00:29:07,840 --> 00:29:19,760
things and we don't react we we lose this habitual reaction and tendency to compartmentalize reality

207
00:29:19,760 --> 00:29:26,800
into the good and the bad into what is acceptable and what is unacceptable you know some people

208
00:29:26,800 --> 00:29:36,240
like it hot some like it cold some people like loud music some people hate loud music it's actually

209
00:29:36,240 --> 00:29:41,520
a habit that we gain in our mind and these can change and develop over time obviously you know

210
00:29:41,520 --> 00:29:46,800
people when you take your first sip of beer or when you take your first cigarette it's terrible but

211
00:29:46,800 --> 00:29:52,320
over time you begin to change your habits and decide for yourself that it's a positive a pleasant

212
00:29:52,320 --> 00:29:59,920
experience people can go to the extent of of believing that or of of perceiving smoke and

213
00:29:59,920 --> 00:30:12,800
entering their lungs as being a pleasant experience and so really this is all we're doing in

214
00:30:12,800 --> 00:30:19,360
meditation we're not trying to change things once we have this level of calm we simply start to

215
00:30:19,360 --> 00:30:25,600
see things for what they are and we start to understand things on a far deeper level to the point

216
00:30:25,600 --> 00:30:31,120
that everything that arises we're able to see it for what it is we lose this sense of attachment

217
00:30:31,120 --> 00:30:39,040
to it this habitual reaction that it's good or it's bad this identification of it being me or mine

218
00:30:44,160 --> 00:30:47,840
and the mind is able to find peace and freedom

219
00:30:47,840 --> 00:30:57,360
and simply through wisdom and through understanding because there's nobody that I think we

220
00:30:57,360 --> 00:31:07,120
can all agree that nobody wants suffering everybody wants to find peace and happiness and so the

221
00:31:07,120 --> 00:31:15,120
reason that we create suffering for us is not because we on a deep down level want to suffer

222
00:31:15,120 --> 00:31:19,520
it because we don't understand that that's what we're doing to ourselves and once we can teach

223
00:31:19,520 --> 00:31:25,920
ourselves once we can convince our minds that that's what we're doing that that this reaction

224
00:31:26,960 --> 00:31:32,480
this mental process is creating suffering for us then we'll stop we stop naturally there's no

225
00:31:32,480 --> 00:31:38,240
question about that it's a theory this is the theory I'm proposing to you but it's a perfectly

226
00:31:38,240 --> 00:31:45,680
absolutely testable and verifiable theory if you start to meditate you'll see that all it takes is a

227
00:31:45,680 --> 00:31:52,960
deeper more comprehensive understanding of the things that you're already experiencing and already

228
00:31:52,960 --> 00:31:58,720
reacting to once you see them for what they are and you see the process and you see the way your

229
00:31:58,720 --> 00:32:08,480
mind is reacting slowly but surely you'll change that you'll convince yourself over time and with

230
00:32:08,480 --> 00:32:15,200
effort that what you're doing is hurting yourself and you'll change it's a natural process you

231
00:32:15,200 --> 00:32:21,360
don't have to want to change you don't have to wish for yourself to become enlightened or even

232
00:32:21,360 --> 00:32:27,440
strive in any way the striving is just to see things for what they are and the closer you become

233
00:32:27,440 --> 00:32:35,600
you come to seeing things as they are the more free and at ease and at peace with yourself you

234
00:32:35,600 --> 00:32:48,480
become so there you have the problem and a I think fairly clear solution I think that's

235
00:32:48,480 --> 00:32:57,120
a that's enough for today I'll give everyone some time to ask questions if you have any

236
00:32:57,120 --> 00:33:04,000
and otherwise we can just sit here and meditate together and you're all welcome to go on with

237
00:33:04,000 --> 00:33:12,640
your lives I'd like to thank you all for coming I appreciate to have such an interested group of

238
00:33:12,640 --> 00:33:18,000
people who return again and again to hear these talks and to support the Buddha Center in its

239
00:33:18,000 --> 00:33:24,320
projects I'd like to wish for you all to be able to put into practice these teachings

240
00:33:24,320 --> 00:33:31,120
and to be able to practice meditation for the development of your own selves and for the attainment

241
00:33:31,120 --> 00:33:37,360
of real and true peace happiness and freedom from suffering for you all thank you all for coming

242
00:33:37,360 --> 00:33:53,280
have a great day

