Okay, good evening, welcome to our evening dhama session.
We have a full house here, I think.
Good to see.
Tonight we're looking at the Aurya Parjesana Sutta,
with Jimmy Kaya number 26.
So there's some dhama in this Sutta, of course.
There's some, I think, fairly useful and
to the point teachings, but it also has one of the best
descriptions of the Buddha story.
Well, it has one of the good stories of
retellings of the story of how our teacher became a Buddha.
Aurya Parjesana means Parjesana is seeking or
looking for it.
Aurya is noble.
Aurya probably meant victorious or something like that.
I'm assuming it does actually come from the word Aurya,
which means enemy.
Aurya means one who conquers one's enemy,
so they're not sure about that.
That's how they describe it, but
where it actually comes from.
I'm not sure anyway.
The word Aurya is an ancient Sanskrit word.
And the Buddha co-opted it because it was
in vogue at his, in his time to use it to mean
the upper class.
The idea is that it came from outside of India.
There were these conquering types who came and
massacred, and maybe not massacred, but came and
took over the Indus Valley and they called themselves
the Auryas.
Eventually, they became the upper class,
the Brahmins and the nobles.
And so there was this question of how you define an
Aurya and the Buddha put in his word about what it
means to be an Aurya and he said it has nothing to do with
one's birth, everything to do with one's actions,
which makes one a high class individual.
So this famous iconic teaching in the Buddha.
But here we have Aurya Parajees and it's a quest or a
search that is noble.
And I think that well sums up sort of the question that
we pose in Buddhism and in spirituality in general is
what are we looking for?
What is the meaning of life?
What is the goal of life?
We're trying to understand what we should strive for.
The Buddha starts with a bunch of monks sitting around talking
and the Buddha comes in and asks them what are you talking about?
They say what we're talking about you actually.
That's a good one.
I'm going to tell them very good.
Consider that that's a proper topic because the story of the
Buddha is something valuable.
The Buddha appears on various occasions to be somewhat
egotistical, if you were to take it at face value,
it appears that way.
It's not clear, agree with it or not, but it was quite clear
that he was a special individual.
And I think it's bold, but indisputable.
I mean, what's disputable is whether the Buddha was actually
enlightened and perfect and seen different ways and so on.
But what's indisputable is that if you are,
there's no reason or, in fact, there's good reason to let
people know that you are.
When you're like the Buddha letting people know,
hey, I'm an exceptional individual.
It has some great benefits to it for the people who hear you.
So in a way, we put the Buddha beyond this sort of concerns
of ego and humility because they wouldn't serve any purpose.
I mean, some sorts of humility, the Buddha does, obviously.
I mean, all humility, the Buddha had no ego whatsoever,
but keeping quiet about his attainments,
we put him as an exception because he could handle it.
He could handle people scrutiny completely.
And he was so, I mean, this is the idea,
is that he was so radiant that you couldn't help it,
except the fact that it was true.
Wow, this is someone who is quite special.
And obviously, the benefit is then you can learn from and gain
from associating with such a person.
That this is the Savior, right?
You're talking about, you're looking for a guru,
you're looking for someone who can teach you.
That in and of itself is a great quest.
So talking about the Buddha, thinking about the Buddha is considered
to be proper.
It's a part of the Dharma.
The Buddha says, when you meet, you should talk about the Dharma
or not talk at all, enter into meditative states.
But then he started, so in order to help them out,
he gives his story.
And that's as part of this.
But the preface that he gives them a teaching,
and it's a teaching on the noble, the noble.
Sort of, it's really a good preface to the Buddha story.
And it's why anyone takes up religion
because they start to ask these questions
that what they're looking for.
Quite often, we never asked these questions.
Before we know it, we're caught up in some ambition or gold
that we didn't ever think through or consider carefully
why we were aiming for that.
Right?
We're taught from an early age.
So very simple goals, like sensual pleasure.
Babies look at how we coddle infants with sensual pleasures
to get them to be quiet or to put a smile on their face,
which makes us happy.
And so from a very early age, we're taught
the benefit of these things that we're taught
how to get.
We're taught to try and get.
We're taught that this is desirable.
And so the babies become addicted to these pleasures
and it's not a must, it's not an evil, evil addiction
but it's still addictive.
And then we learn about things like money,
we learn about things like sex as we grow older.
All this means to gain further sensual pleasure mostly.
Food, obviously, foods, a big one.
Music, etc, etc.
It diversifies, but it doesn't really ever leave.
But it generally centers around sensual pleasure.
For a little bit more high-minded or for our parents
are a little bit more intellectual.
They might have a sort of a broader sense
or a more abstract sense of happiness.
And so there's this quest for power,
for knowledge, for wisdom even,
and by wisdom most likely philosophy.
And there's the ideas of social activism and so on.
Many, many quests and many, many journeys, many, many ambitions.
Often we don't get beyond the most simple ones,
like food, essential pleasure and money or ambition.
So we find ourselves putting so much effort into
just trying to be maybe not rich, but well-off.
It's to stable stability.
It's intoxicating as a goal, seeking out to stable life.
The illusion, right?
It's a really an illusion.
Or it's a, well, the stability is an illusion,
but it's precarious as more to the point.
Because someone can live many, many years
if they're lucky in a very stable situation, right?
But it's like a house of cards.
Any moment it can fall.
This is one disaster, and you can lose everything.
But sometimes we do go beyond that,
and we're seeking for social justice,
or other things like fame or power, so on.
But even that, then we get caught up in,
what turns out to really be delusion,
because we're trying to change the world
or we're trying to become something.
And you still can't escape this question.
Why? Why?
More importantly,
the Buddha puts it in a much better way.
Because you've got a problem.
You've got several problems.
What are our problems?
Our problems are birth.
Birth meaning rebirth will be born again.
Old age, sickness, death.
Old age sickness, death,
easy to understand, easier than rebirth or birth.
This is really what we understand to set the Buddha going, right?
This realization that,
yeah, all those things that I thought of as valent goals
become invalid because I'm going to get old sick and die.
And in the face of old age sickness and death,
they're all meaningless.
All those lies I was taught about the right way,
the right search, the right ambition.
Turn out to be wrong, turn out to be deluded, ignorant.
Worthless meaningless,
because I'm going to get old sick and die,
just because of that, right?
In the face of eternity and the face of reality,
they're meaningless.
And be a bum on the street, live under a picnic table.
It's no less valent and existence.
It's no less meaningful.
Living in a mansion or even let's go to the bigger ones,
like helping people, curing cancer,
curing cancer in the face of reality doesn't actually mean anything.
You might even say,
and this is where I get into dangerous territory,
you might even say that becoming a Buddha is meaningless,
becoming enlightened is meaningless.
And I think, but I think to a certain extent,
from one point of view, it is meaningless.
Of course, not from our point of view.
Meaning, you should never think it's pointless to become enlightened,
like why bother kind of thing.
But part of becoming enlightened is to see that this idea of meaning,
in fact, to seek this search is based on delusion and ignorance.
But he talks about the noble search.
So clearly, there is a search that is right,
and it's important to understand that, I think,
because people always ask,
well, if you're talking about giving up desire,
what about desiring for nibhana or desiring for enlightenment?
How could you ever practice meditation if you don't want to practice meditation?
And I've talked about this before,
and it's really just a mind game, right?
The logic of it doesn't relate at all to reality.
This is like, it's a whole other situation.
It's more like your head is on fire,
and your intention is to put the fire out.
Or more to the point,
and we're full of, as you say, desires, wants, ambitions.
And when we realize the problem,
we want to get rid of the want thing.
But it's just a word.
There may be moments, and there will be moments
of desiring to become enlightened,
but that's not really how it works.
It's more like an eye opening and saying,
oh, my head's on fire, well, I better put that out.
It's like seeing that you're, as I've said before,
internally inconsistent.
You want certain things,
but you're doing things that don't lead to that.
And this is what the Buddha says in a different way.
He says, being my style,
he says, when I was unenlightened,
being myself,
subject to old age sickness and death,
I sought out that which was subject to old age sickness and death.
Meaning, I,
I had, basically,
I had this catalyst or this source
for that would lead to, could lead to my suffering.
I'm going to get old sick and die,
and I purposefully sought out things that didn't lead me out of that.
But let me only tomorrow suffering, basically.
Myself being subject to suffering,
I sought out that which was caught up in suffering.
Any lists a bunch of things,
a wife and children,
servants,
goats, goats and chickens,
elephants, gold and silver,
etc.
All sorts of possessions,
all sorts of acquisitions, a stable life and so on.
All to ward off suffering and to bring about happiness.
And yet these things that I sought out
did not, could not, can never
bring me, bring about happiness.
Because they're subject to old age sickness, death,
soil, lamentation, despair, impermanence, really.
Change, loss.
You go through all of this, get all of this,
the life, the house and family and money
and even power, wealth, whatever.
You go through it and in the end you're just old,
sick, dying.
Maybe you lived a good life, maybe not.
But it's over.
That's it.
And then it starts all over again.
You forget it all and come right back and do it again.
Not really a noble sort of activity
of a Buddhist perspective.
So he taught the noble search.
And this is again this clear,
a fork in the road or this sort,
like the perpendicularity,
the opposite nature of the Buddhist path
and the way of the world.
It's hard to, it's hard to become inclined
towards seeking out freedom from suffering
because it goes very much against wanting.
How can you want something that goes against wanting?
The only way is when you see
that wanting is causing you suffering.
And actually this is very much what our practice is about
on a day to day moment, a moment basis.
People talk about letting go.
We had this question of someone having trouble letting go.
Which is, it's a common thing to say,
but it's indicative of a misunderstanding
of what it means to let go
because you can't struggle to let go.
The path to freedom from suffering
is nothing to do with wanting.
You can't want to be free from suffering.
You can only see that wanting this wanting that
is a cause of suffering and thereby let it go.
That's the reason why we let go.
But the Buddha was before he learned
how to practice meditation.
He started with his realization,
hey, these things that I'm seeking out,
these things that I'm striving for.
They're not bringing me happiness.
They can't bring me happiness.
And so we know this much about the story.
He left home.
When I was still young,
black-haired young man,
endowed with the blessing of youth,
though my mother and father wished otherwise
and wept with tearful faces.
I shaved off my hair and beard,
put on the yellow robe,
and went forth from the home life into homelessness.
Now, this wasn't such a radical thing, of course,
in the time it would have got the idea
from someone else.
Apparently he saw a monk on a sedic,
wearing a simple cloth.
I guess it was a tradition to wear orange,
but it was probably more that was the kind of a simple dye,
I don't know.
But wearing a logo much like this,
I think this is probably similar to how they weren't,
but I have no idea.
Just a guess, no evidence.
Yeah.
And so what did he do?
He went forth and he saw it out of the teacher.
He saw that someone who could teach him this way.
And again, this is why we consider thinking
about the Buddha such a good thing,
because he was a very special sort of teacher.
But now, of course, didn't find a good teacher.
He found two teachers who were,
well, actually as it goes,
quite good teachers.
Al-a-la-la-la-la-la,
and Uddhaka-a-la-la-la-la.
Al-a-la-la was a teacher in his own right,
or a practitioner in his own right,
and he practiced what we now call summit meditation.
Some sort of practice that led him all the way to the base of nothingness.
Natikinji, we will lead to the formless Brahma realms,
which we're just talking about last night in our Abhidhamma study.
If your practice certain types of jhana based on these things,
like nothingness, take nothingness as your object,
it comes in a progression.
Your mind is contemplating infinite space,
and then, or an infinite object,
then an infinite space,
and then infinite consciousness,
and then take away the consciousness,
and there's nothing in contemplation of nothingness.
Emptyness.
This is you hear a lot about emptiness,
and I think it's just for many people
they confuse emptiness with nothingness.
Of course, rather, it's a philosophical thing,
but this experience that people have of this
profound nothingness is just this,
with the Buddha talks about.
The Buddha was not very impressed,
even though having learned it all,
he was able to master it in a very short time.
He came back to Allah and he said,
you know, is this it?
And he described his experience,
and Allah said, oh yes,
now you are equal to me.
Come and teach this, teach my students with me.
We will be equals.
And he said, I'm sorry,
this isn't freedom from suffering,
I know where this leads,
and I guess there was a sense that he kind of
remembered his past lives
because he knew somehow that this only leads
to the Brahma realms,
because of course he'd packed practice this life after life,
but I think it was sort of at least a sense
from coming from past lives
that he knew that this didn't lead,
and it's quite profound from to realize that,
but logically,
is what you have to understand about mindfulness.
It's quite, it's a categorically different sort of meditation.
There's no reason why
trans meditation should lead to enlightenment.
You can talk about it, you can fudge it,
you can talk about how when your mind is very calm,
the defilements go away or something like that,
but going into a trance doesn't teach you anything
about suffering.
It doesn't change the way we look at
the things that we cling to.
It gives us a contrast, which is useful.
This is better than that.
But it doesn't tell you that all of those things
we cling to are wrong.
In fact, you can cling to John,
and you can cling to it and say,
oh, I like that state I was in and so on,
and want more and so on.
You can even become egotistical about it.
So he left and he went to Utakarama Buddha
and Utakatat, something that Rama,
his teacher had taught him,
but the practice that leads to realm of neither perception
nor non-perception, which is one level higher
than nothingness.
Some state where you're not even really
recipient or you're not even really aware.
You can't say that you are aware,
you can't say that you're not.
There's some very, very subtle awareness.
It's like the state of a pilot light.
The fire has gone out, but there's still something.
Very subtle state.
It's about as subtle as it gets.
When he said, is this it?
He said, oh, yes, wow, you're even better than me
because he hadn't even mastered it.
Rama had.
But Utakarama Buddha hadn't had not.
So he said, here, you take over a teacher,
teach these people, teach them how to gain this.
And he said, I'm sorry, but this is also not
the path that leads to enlightenment.
This again only leads to some high Brahma realm.
It's interesting because if you read like the Upanishad,
it's all about enlightenment is oneness with Brahma.
And I think that it makes very clear
that Buddhism is distinct.
Buddhist enlightenment is distinct from all this talk
about becoming one with the universe, one with God, one with
et cetera, et cetera.
Ramana is not becoming one with everything,
which is apparently, which is, I think, many years ago
when I looked it up in the dictionary.
And that's what the dictionary said.
Himanas is like putting out a fire.
So different.
He said, no.
And so he left him as well.
And then he wandered around.
And this is where the different,
different stories diverged.
This story doesn't talk about the many years
he spent torturing himself.
It says he wandered and eventually came to Uruweh that.
To the Magadhan country and where I came to Uruwehland,
Sainani Gammur.
Sainani Gammur would be a village, I think.
And he found a place where the clear flowing river
and pleasant smooth banks and this village nearby.
Well, this is a good place to describe.
And he sat down under the Bodhi tree
and, well, long story short, he became enlightened.
And being myself subject to old age sickness and death,
I attained the security.
I attained to that which was not subject to old age sickness and death,
which is nibana.
Nibana is something that is not subject or not subject to change,
not subject to impermanent.
It's unshakeable.
And it shouldn't be hard to understand.
It's unshakeable because it doesn't arise.
Nibana isn't the state of non-arising.
Through the mind enters into complete peace.
Hard to fathom.
I think it's hard to be too excited about it either because,
obviously, it's totally counter to our ideas of getting this
and getting that.
But when someone has realized nibana,
they're all nature changes.
It changes you because suddenly you've seen true peace.
And you can't deny that it's categorically superior.
It's superior to any other kind of happiness.
I don't know if you guys could actually hear me out there.
Sorry about that.
So in brief, he attained nibana.
Having attained nibana, he realized for himself that his enlightenment
was unshakeable.
The wonderful quality of seeing nibana is that unlike other kinds of
wisdom inside realization, just the simple experience of nibana
because it's in a whole other category.
So for a whole other nature than anything else,
it changes you irrevocably unshakeably.
And he knew that this was his last birth.
There was no renewal of being.
So the Buddha had not only seen nibana,
he had attained to what we call,
I guess, São Padhi says, nibana.
I mean, he had become free from all the violence.
Normally people become enlightened in stages,
meaning they see nibana,
and some of the defilements are removed
and then slowly, slowly through repeated
experience, the mind becomes increasingly
subtle, increasingly in tune with peace
until eventually defilements are vanquished entirely,
but the Buddha, all at once,
they came free from defilements very quickly.
In fact, there's this idea that it
didn't have taken him six years to become enlightened,
but he had some bad karma that he had to atone for in a past life,
and actually once he had atoned for that,
on that last day after those six years,
he only took him one night to become enlightened,
and it should have only taken him one night from beginning to end.
But those six years were because he had insulted an enlightened being
and said they were torturing themselves.
He was kind of silly person who
saw a wrong-headed individual who tortures themselves.
And so his mind was corrupt, still in that way,
and so he had to work through that bad karma.
His mind, I guess, wasn't corrupt,
but that corrupt mind led him to,
well, yes, to this misunderstanding.
It was a corruption, the misunderstanding that he had torture himself.
So very quickly, he became very from suffering.
That's won't go into much detail there.
There's not too much to say about that.
That's interesting, of course, as this next part,
where what happened when he became enlightened,
his outlook seems to have changed,
where, or even perhaps up into that point,
he hadn't thought about whether he should teach.
So it may not have changed so much,
but then there came the question of what he should do.
Now that I become enlightened, what should I do about it?
Is there anything I should do about it?
The obvious question is, should I teach?
And he thought to himself,
that people aren't interested in this.
People don't want this.
People aren't looking for this.
Isn't that bizarre?
No, people aren't looking for Buddhism.
So he said to himself,
people aren't looking for the truth.
We're looking for this thing that we've now come to value,
so so greatly.
I think you could make an argument that if Buddhism hadn't been found in the world.
You might sort of feel the same as the Buddha here.
Of course, now when we hear, once we've learned that meditation exists
and enlightenment exists, it's interesting for people.
It's something that people are inclined to study and practice,
but can you imagine if there wasn't any idea about this way?
I'd best people go for God or Brahma.
Look at all the other religions out there.
I don't think Buddhism is categorically different.
It bears repeating.
The practice is very much different from all those other ways.
He said enough.
Enough with teaching the Dhamma that I even I found hard to reach
for it will never be perceived by those who live in lust and hate.
He spoke in verse apparently.
Those died in lust, died meanings.
Means their minds are colored or like wearing rose-colored glasses.
Wrapped in darkness.
We'll never discern this upstairs Dhamma,
which goes against the worldly stream subtle deep and difficult to see.
Look at that, goes against the stream.
Parthisota gami nipunang.
Parthi, Parthi means against Sota, is the stream gami, it goes.
Parthisota gami nipunang is sublime.
Mmm, abstruse, whatever that means, subtle, subtle.
Gambirang dudasang, anu.
It is deep, hard to see dudasa.
Raghara tana dakati, those who delight in passion,
will not see it.
The mokandena out wrapped up in massive darkness,
I think I'm not quidger.
So the buddha thinks he's not going to teach.
I think it's the idea that it would be worthless,
meaningless, it would be obvious.
It would be troublesome.
He uses the sword behingset.
I perceive the abuse almost.
It's not abuse, but how vexing it would be for me to teach.
It would just be a bother, you know.
What a bother it would be to teach.
And he decided not to teach.
So that doesn't really sound good.
It sounds like he was just lazy or something like that.
But it's more of a question of why would you teach?
There's a good question.
Why help another person?
Why do it?
And there has to be a line there.
If it means going out of your way to help someone else,
what question, why are you doing it?
I think there's clearly room,
and it's not always necessary,
but there certainly is room logically
to help someone who asks you to teach.
And so that's what happens.
Brahma, Sahampati Brahma.
This is the story.
Here's this and gets very, very concerned.
Sahampati was, I think, one of the,
he was a disciple of the Buddha in the past.
He was an anana gami, I believe.
And he said, he was up in the Brahma realms,
and he, well, apparently at this time,
everyone was watching the Buddha,
and he became enlightened,
and all the, all the angels and Brahma's,
they had taken up audience to watch the Buddha,
to observe and to pay homage to the,
this enlightened being,
because none of them were enlightened,
not fully.
And Brahma's watching and he was,
oh no, we can't let this happen.
The world will be lost, the world will perish,
since the mind of the tatagata,
accomplished and fully enlightened,
inclines to inaction,
rather than to teaching the dhamma.
And he transported himself,
beamed himself down,
vanished from the Brahma world
and appeared before the Buddha.
Arranged his upper robe on one shoulder.
You see, this is a,
why we wear the robe on one shoulder
is out of respect, in fact.
When paying respect to the Buddha
and paying respect to another monk,
we always open one shoulder.
And so it just became a standard
to have one shoulder open,
or, yeah.
And it appears to have been just a tradition,
a way of paying respect at that time.
And he bowed down before the Buddha
and he said, let the blessed one teach.
And he gave some verses.
It's quite a beautiful verse.
He says,
in Magadha, until now,
there have been impure teachings
devised by those still stained.
Open the doors to the deathless.
Let them hear the dhamma
that the stainless one has found.
It's quite a good translation.
Just as one who stands on a mountain peak,
can see below the people all around.
So, oh, why is one all seeing sage?
I send to the palace of the dhamma.
Go up into your mountain palace
and look out on the balcony.
Let the soerless one survey this human breed
engulfed in sorrow
overcome by birth and old age.
Look at these people,
wretched individuals who are suffering
gaze upon them.
A rise of victorious hero caravan leader,
deadless one.
Deadless one.
You don't need to help anyone.
But do it, please.
Wonder in the world.
Let the blessed one teach the dhamma.
There will be those who understand
and this is really the important line.
Apparently, because the Buddha then
does a survey,
you listen to the Brahma's pleading
and out of compassion for beings.
Well, let's take a look then.
And he looked and he saw
four types of people.
Well, he doesn't explicitly say that,
but he saw beings with little dust in their eyes
and with much dust in their eyes.
I mean, delusion.
It's a metaphor for delusion.
With keen faculties and dull faculties,
with good qualities or with bad qualities,
this is in his mind's eye,
considering or perhaps even somehow
with extra sensory perception
encompassing the minds of others.
Easy to teach and hard to teach
and some who dwells seeing fear and blame
in the other world.
And then he gives this simile of four types of lotus.
Some types of lotus grow up
and stay underwater.
They grow there.
If you know anything about lotus,
some of them are quite beautiful,
but they just do nothing.
They stay in the water.
Some go up to the water's surface.
Some grow and,
oh no, this isn't the four.
He said the doubts were,
but here he's only three
and some rise above the water.
And rise clear,
stand clear,
unwetted by the water.
It means even though they come from the water,
they leave it without taking any water with them.
They become free from the water.
So he saw that there were people who would understand
and who would be very interested
and who were in fact at that moment looking for it.
Sorry put on Mogulanar to very good examples.
When the Buddha was thinking this,
he must have seen them because they were searching
for him far and wide.
They were looking for the Buddha.
They didn't know it,
but they were looking for the teacher,
the teaching paths.
He must have seen the five ascetics
who he had left behind,
who had left him behind, sorry,
when they saw that he was no longer torturing himself.
And he must have seen that they were looking so diligently
and not much for difficulty
would be able to understand.
And so he said,
he says, open for them are the doors to the deadless.
This is a beautiful verse as well.
Apparutatisangamatasadwara,
open to them is the door to the deadless.
Amata.
He saw Tawanto pamunchanto sadhan.
I'm not quite sure what this means,
but he says to answer it,
it is like those who have ears.
So Tawanto, those who can hear.
Those with ears, those can hear.
I mean, those who will listen.
Pamunchanto sadhan.
He tries to show their faith,
but Pamunchanto is relating to become free.
But maybe Pamunchanto sadhan
may they find what they're looking for,
right?
This doubt about which way to go,
let them become free from that,
so that they may be confident
that they're in the right path.
And then he says,
we Hingsasanyi,
thinking it would be troublesome,
Bhagun-nam-nabasing.
I didn't speak when I had learned.
Damang-pani-tang-manu-ji-subrami.
And here I think he gets the translation actually wrong.
I don't agree with his translation at all.
And it looks like one of these instances
where he was maybe in a hurry,
and so he just kind of fudged it.
In a hurry, or didn't quite understand it,
and you know how it can be.
I think it's a mistake.
Damang-pani-tang-manu-ji-subrami means Brahma.
The Damma is, it's quite simple.
The Damma is sublime amongst humans.
Meaning humans see the Damma as being sublime.
Beneath it's something good.
It's a good sort of sublime.
This is something that is valued so highly by humans.
That's how I understand it.
And this satisfied Brahma,
because now the Buddha was going to teach,
and so he left.
I don't know.
I'm going to stop there.
I'm going to quickly go through the rest
because I don't want to get too much.
And until he went and taught the five ascetics,
I don't know, first he met.
I do.
I do what got.
It's an interesting little story,
but not that important.
He came to the five monks, and then he taught them
the Damma-chaka-pawat in the Sutta,
and began this teaching.
Then we come to the last part of the Sutta,
which is where the Buddha having given this story,
which is meant to rouse them
and inspire them.
It's inspiring to think.
You build this up,
and then lead the logical conclusion
of going forth and becoming monks and so on.
It's meant to inspire the monks and what they're doing
on the same path that the Buddha followed,
as are we all.
And then having inspired them
and gets down to business,
and he talks about sensual pleasure.
Let's cut to the chase,
get to the point.
There are the five chords of sensual pleasure.
Things you see,
things you hear,
things you smell,
that you wish for desire,
that are agreeable
and likable, connected with sensual desire,
and provocative of lust.
And he says,
those religious types,
seminars or romenas,
who are tied to these sensual things,
infatuated with them
and utterly committed to them.
We use them without seeing the danger in them
or understanding the escape from them.
It may be understood of them.
They have met with calamity,
met with disaster.
Mara may do with them as he likes.
This is how we describe someone who has caught up
and becomes like a,
you become trapped.
It builds up and builds up
and you just don't know how to get out of it.
It's like a wild animal.
It doesn't want to be trapped.
It wants to be free, wants to be happy,
and finds itself caught up in the hunters trap.
This is the simile the Buddha gives.
The allegory, I think it's called.
Just like a deer that is trapped by hunter,
so to one who is trapped by sensuality,
caught up in so many traps, right?
You have to find money.
You have to create this stability just so you can enjoy your pleasure.
And then of course, the trap is that you need it.
You need it more and more and more
and it gives less and less and less pleasure every time.
So you have to seek out new pleasures and get trapped
before you realize that you're just caught up in
a meaningless pointless activity.
Sometimes even evil, oftentimes even evil,
unholes someone as a collectivity.
Just to get what you want.
Trapped.
Can't do anywhere, can't become anything.
Life's not in any way impressive or valuable or meaningful.
Just caught up in meaningless things.
That's the idea.
And then the other example is suppose there is a deer
that is not caught up.
A wild deer, free to roam as it will.
There's something romantic about this idea of
looking in the wild without any, you know,
how caught up people are in their needs,
just to get by, just to get food and shelter and so on.
So it's not like deer have an easy life.
In fact, it can be in terrible, awful hard life,
but in Canada anyway.
But in warmer climates, perhaps, for a time,
the idea is just of being free and having,
not having to answer to anyone.
In the same way a person who is not caught up
in this sensuality, this is what they're like.
So we talk about freedom and happiness and peace.
And the Buddha's pinpointing the problem.
You can't be at peace, even you live off in the forest like a deer.
If you're still caught up in desire, you can never be happy.
Even in the most peaceful setting.
Even in the happiest setting.
But a person who is not tied down to sensuality
can be happy anywhere in any situation.
And then he goes through the stages of,
you know, really, in this case, he goes through all the Johnus.
And eventually comes beyond them.
And he talks about how enlightenment is beyond the Johnus.
Having left them behind, one enters into the cessation of perception
and feeling, which is description of Nibbana.
And this is where the taints are destroyed.
And one is said to have gone beyond the range of Mara.
So in the case of the Johnus, in the case of an ordinary meditation.
Even in the case of insight meditation.
One is said to have blindfolded Mara.
One is sort of put up a smoke screen.
So Mara can't see you.
You are invisible to Mara.
When someone enters into trance states of meditation,
peaceful states of meditation, it's like Mara is blindfolded.
Can't get at you.
Why? Because you have no greed, no desire.
You're content, you're at peace.
But you haven't gone beyond the range of Mara because it has to end.
When one enters into Nibbana, cessation of perception and feeling,
one has gone beyond the realm of Mara because it changes one.
This is the idea that it being fundamentally different.
It adds this quality to your mind, this stability to your mind.
It puts that kernel of enlightenment, this idea into your mind.
It leads to a profound and complete state of letting go,
where you're unable even if you tried to want anything.
There's no where there comes to be no potential.
No link, no cause, no ability to become attached to anything.
That's the idea.
And that, in a nutshell, is the Ariaparesana.
Again, if you're really interested, I encourage you to read this to yourself.
This is just a means for us to inject a little bit of background and some theory
and some of the ideas into our practice to give encouragement and direction.
So that's the demo for tonight.
Thank you all for tuning in. Have a good night.
Thank you very much.
