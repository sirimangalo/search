1
00:00:00,000 --> 00:00:07,360
We have two questions and deleted a couple and they were relating to other spiritual practices

2
00:00:07,360 --> 00:00:17,360
and I'm not sure they up to commenting on that sort of thing, so we have two left.

3
00:00:17,360 --> 00:00:21,600
Should we be on our Robin?

4
00:00:21,600 --> 00:00:22,600
I would.

5
00:00:22,600 --> 00:00:27,000
Yeah, I think we probably both are clicking on it.

6
00:00:27,000 --> 00:00:28,600
No, I haven't clicked yet.

7
00:00:28,600 --> 00:00:29,600
Oh, okay.

8
00:00:29,600 --> 00:00:30,600
I can't.

9
00:00:30,600 --> 00:00:31,600
No, I can't.

10
00:00:31,600 --> 00:00:32,600
No, I can't.

11
00:00:32,600 --> 00:00:33,600
It doesn't look.

12
00:00:33,600 --> 00:00:39,400
I can't go stale if you leave this tab open for too long.

13
00:00:39,400 --> 00:00:40,400
That could be.

14
00:00:40,400 --> 00:00:45,840
Let the file bug report be.

15
00:00:45,840 --> 00:00:51,040
Would considering having children be courting trouble if there's no desire for procreation,

16
00:00:51,040 --> 00:00:53,240
but because I'm afraid.

17
00:00:53,240 --> 00:00:57,280
Oh, sorry.

18
00:00:57,280 --> 00:01:02,000
My screen just went blank there.

19
00:01:02,000 --> 00:01:07,320
I think the rest of it was, I don't want to necessarily procreate, but I'm afraid I'll

20
00:01:07,320 --> 00:01:09,920
be lonely in my old age.

21
00:01:09,920 --> 00:01:12,440
Only in the room.

22
00:01:12,440 --> 00:01:13,440
Yes.

23
00:01:13,440 --> 00:01:23,760
I mean, any kind of fear or any kind of neurosy, neurosis should not be encouraged.

24
00:01:23,760 --> 00:01:25,080
It's a habit.

25
00:01:25,080 --> 00:01:40,760
So whatever you do to feed into it and to nourish it, to assuage it, it nourishes it,

26
00:01:40,760 --> 00:01:47,080
it actually ends up promoting it, so that's the trouble that you get into there.

27
00:01:47,080 --> 00:01:55,000
I mean, you're not ever going to come to terms with your fear or loneliness.

28
00:01:55,000 --> 00:02:03,680
And of course, having children is so much more than assuaging your loneliness or so much

29
00:02:03,680 --> 00:02:09,280
more trouble involved with having children, so there's a lot of that's your opening

30
00:02:09,280 --> 00:02:26,520
a Pandora's box of sorts.

31
00:02:26,520 --> 00:02:29,760
We've got some troubles, our website is down.

32
00:02:29,760 --> 00:02:32,280
Are you getting this?

33
00:02:32,280 --> 00:02:33,280
I was.

34
00:02:33,280 --> 00:02:38,560
That's when my screen went blank, but now it's working again, I'm getting a 503R.

35
00:02:38,560 --> 00:02:43,600
That's what I had before when I started a capacity problem, so I think we have to increase

36
00:02:43,600 --> 00:02:44,600
the capacity.

37
00:02:44,600 --> 00:02:45,600
Oh, okay.

38
00:02:45,600 --> 00:02:51,520
It's a talk to our IT team and get us a higher tier or something, and we know what service

39
00:02:51,520 --> 00:02:52,520
this is on.

40
00:02:52,520 --> 00:02:53,520
This is on a special service.

41
00:02:53,520 --> 00:02:56,400
I think we have to start with upgrade it.

42
00:02:56,400 --> 00:02:58,800
Is that the digital ocean?

43
00:02:58,800 --> 00:03:01,200
No, I don't think it is actually.

44
00:03:01,200 --> 00:03:03,200
I think it's something else.

45
00:03:03,200 --> 00:03:04,200
Okay.

46
00:03:04,200 --> 00:03:07,080
I know we have a contract with digital ocean.

47
00:03:07,080 --> 00:03:11,880
We do, but that's for, it's the whole sting, actually, I'm not sure what that's for.

48
00:03:11,880 --> 00:03:13,480
Oh, yeah, that is for, okay.

49
00:03:13,480 --> 00:03:16,080
It's a web hosting, okay.

50
00:03:16,080 --> 00:03:19,480
Maybe there is, so this is on it.

51
00:03:19,480 --> 00:03:23,440
I don't actually know.

52
00:03:23,440 --> 00:03:26,680
Thank you for the opportunity for the Q&A session, Bante.

53
00:03:26,680 --> 00:03:30,880
Do it to enlighten to beings, absolve bad karma?

54
00:03:30,880 --> 00:03:36,640
If someone were to lightly assault and enlighten to being, would that bad karma be absolved?

55
00:03:36,640 --> 00:03:37,640
No.

56
00:03:37,640 --> 00:03:43,320
That would be worse karma in assaulted and enlightened being, oh, that's quick road

57
00:03:43,320 --> 00:03:44,320
to hell.

58
00:03:44,320 --> 00:03:45,320
Yeah.

59
00:03:45,320 --> 00:03:53,120
That's actually the danger of hanging out with enlightened people, greater potential

60
00:03:53,120 --> 00:04:02,800
for you to do something bad done to enlighten being, hard to recover from.

61
00:04:02,800 --> 00:04:04,640
How could you get angry at an enlightened being?

62
00:04:04,640 --> 00:04:05,640
That's the thing.

63
00:04:05,640 --> 00:04:15,960
That would, who could want to harm such a pure individual after a fairly corrupt mind?

64
00:04:15,960 --> 00:04:21,120
Do terrafara Buddhists acknowledge Amitabha Buddha claims and aspire to be reborn in

65
00:04:21,120 --> 00:04:24,120
Pureland?

66
00:04:24,120 --> 00:04:26,520
No, why would we?

67
00:04:26,520 --> 00:04:30,840
I mean, I guess people don't know the differences in Buddhism.

68
00:04:30,840 --> 00:04:36,680
No, Amitabha Buddha, it's something I actually can't, I was arguing with the Chinese

69
00:04:36,680 --> 00:04:37,680
monk.

70
00:04:37,680 --> 00:04:50,880
Again, this is my friend,

71
00:04:50,880 --> 00:05:00,480
they really are smug, you know, they say we just don't understand their suit, they understand

72
00:05:00,480 --> 00:05:06,280
all of ours, I know they don't, but they think they understand all of ours, but we don't

73
00:05:06,280 --> 00:05:07,280
understand all of theirs.

74
00:05:07,280 --> 00:05:15,480
He's a little bit smug, sorry, shouldn't say bad things about people, you know, we

75
00:05:15,480 --> 00:05:18,560
were talking about this.

76
00:05:18,560 --> 00:05:23,440
Actually, he's not Pureland, but we're talking about these sorts of things.

77
00:05:23,440 --> 00:05:28,040
He says he wants to be born in the Pureland, but he'd, so he, he, he, he, he inspires

78
00:05:28,040 --> 00:05:32,840
to be born in the Pureland, but he doesn't recite Amitabha Buddha, so I guess yes, he

79
00:05:32,840 --> 00:05:33,840
does.

80
00:05:33,840 --> 00:05:44,000
This is the sort of thing we were talking about, and yeah, I mean, it's a way of far-and-left

81
00:05:44,000 --> 00:05:51,800
field concept, this idea of Amitabha Buddha, the idea that there is a, I mean, it just

82
00:05:51,800 --> 00:06:01,600
smacks of, this was made up, right, so you start out with a teaching of a Buddha that

83
00:06:01,600 --> 00:06:09,400
is teaching people to become enlightened, right, here and now, then that Buddha passes

84
00:06:09,400 --> 00:06:18,640
away, and there's no Buddha here, so you, you revert to deism, or theism, or theism,

85
00:06:18,640 --> 00:06:19,640
really.

86
00:06:19,640 --> 00:06:26,800
Well, these Buddha's didn't actually go into Nibbana, they're hanging out in Pureland.

87
00:06:26,800 --> 00:06:36,600
Well, in each Buddha has their own Pureland, and they've made vows to save us all, because

88
00:06:36,600 --> 00:06:41,560
that's what Buddha's do, they hang around, they hang out in some sort, it's just not my

89
00:06:41,560 --> 00:06:49,640
cup of tea, I'm not terrified of Buddhism, I mean, I mean, it's very different from the

90
00:06:49,640 --> 00:07:00,640
Buddhism that I know, yeah.

91
00:07:00,640 --> 00:07:05,000
Have you considered having the Suthanambras to the YouTube title, it would make it easier

92
00:07:05,000 --> 00:07:08,240
to figure out which videos I have watched and which I happen.

93
00:07:08,240 --> 00:07:11,400
I can livestream only two talks a week because of my schedule.

94
00:07:11,400 --> 00:07:16,880
That's a good idea, wish I could let other people in to edit my videos, someone else can

95
00:07:16,880 --> 00:07:22,040
do it for me, but to go back and do all the ones I've already done, maybe not, but yeah,

96
00:07:22,040 --> 00:07:24,400
something I probably should have been doing.

97
00:07:24,400 --> 00:07:31,760
Or is it possible to let people know ahead of time which ones you will be, which Suthas

98
00:07:31,760 --> 00:07:35,960
you will be, and I have to set the title before I broadcast anyway, so I can just include

99
00:07:35,960 --> 00:07:38,400
it in the number.

100
00:07:38,400 --> 00:07:46,040
I was wondering about actually reading this Sutha before we heard the Dhamma talk, or

101
00:07:46,040 --> 00:07:49,480
do you kind of pick them out at last minute?

102
00:07:49,480 --> 00:08:00,480
No, well, yeah, sort of last minute, but this person is, I guess, when you go to YouTube,

103
00:08:00,480 --> 00:08:05,880
it's not clear, but they're in chronological order, or some way to get them in chronological

104
00:08:05,880 --> 00:08:06,880
order.

105
00:08:06,880 --> 00:08:13,040
There should be, maybe not, but if you're just curious, I'll upload them, and I'll order

106
00:08:13,040 --> 00:08:19,200
them according to date, and that's really the way you would do it.

107
00:08:19,200 --> 00:08:28,480
I know YouTube is not an incredibly user-friendly in that way, it's limited for our purposes.

108
00:08:28,480 --> 00:08:37,680
I mean, maybe someone could go through and make links to them on a wiki or something,

109
00:08:37,680 --> 00:08:51,200
maybe that would work better.

110
00:08:51,200 --> 00:08:52,200
Thanks for the answer.

111
00:08:52,200 --> 00:08:56,480
Why is there so much division among different Buddhists and versions?

112
00:08:56,480 --> 00:09:00,920
Because there's our Buddhism, which is correct, and then there's all those other Buddhism

113
00:09:00,920 --> 00:09:17,320
that are wrong, that's why, instead of reading the answer, that's really the short answer.

114
00:09:17,320 --> 00:09:21,960
There are some other Buddhism out there that I would say are, okay, they're teaching

115
00:09:21,960 --> 00:09:32,680
okay things, but today, the big problem, I mean, it's not even one that, from my point

116
00:09:32,680 --> 00:09:44,200
of view, requires much, has much room for controversy.

117
00:09:44,200 --> 00:09:50,480
The only, only Teravada Buddhism, Teravada Buddhism is the only one, because it was different

118
00:09:50,480 --> 00:09:56,240
in India, and in India, there were, in the time that Teravada Buddhism was growing, there

119
00:09:56,240 --> 00:10:01,040
were many other schools that had different, quite an acclaim, you know, this was a Buddhist

120
00:10:01,040 --> 00:10:05,840
teaching, and they had different texts and different commentaries.

121
00:10:05,840 --> 00:10:12,320
So that's one thing, but Teravada Buddhism is the only one of all the Buddhist schools

122
00:10:12,320 --> 00:10:21,480
that sticks to those old teachings, all the other groups, and teachings on, that it's

123
00:10:21,480 --> 00:10:28,560
really hard to believe were taught by the historic Buddha.

124
00:10:28,560 --> 00:10:36,800
Very few scholars have a sense that there even, you know, even just altered teachings,

125
00:10:36,800 --> 00:10:45,000
that there's a sense, I think, among scholars, not that that's an academic scholars,

126
00:10:45,000 --> 00:10:51,800
but it does say something, I mean, impartial observers, it's really hard to stomach the

127
00:10:51,800 --> 00:10:58,960
idea that these suitors are originally, especially with the origin stories of them being

128
00:10:58,960 --> 00:11:05,840
hidden, and then coming up later, considering how different they are from the earlier

129
00:11:05,840 --> 00:11:11,400
teachings, which actually have some potential of being something that was taught by the

130
00:11:11,400 --> 00:11:20,440
Buddha, or, at the very least, a codification, or an extrapolation of something, Buddhakshu

131
00:11:20,440 --> 00:11:30,000
said a standardization, and so all schools, most schools tend to recognize these old texts,

132
00:11:30,000 --> 00:11:34,600
but then they add something, they add all this other stuff, and the stuff that they add

133
00:11:34,600 --> 00:11:41,600
is usually very different from the original, and so the quality of it is different, so

134
00:11:41,600 --> 00:11:45,560
anyone is most care about the Buddhists who have studied Mahayana Buddhism or other types

135
00:11:45,560 --> 00:11:49,600
of Buddhism, just how you know this stuff that they say was written by the Buddha was said

136
00:11:49,600 --> 00:11:57,120
by the Buddha, it's quite different from the original stuff, hard to believe that it was

137
00:11:57,120 --> 00:12:02,840
written, it was spoken by the same person, it's describing the same group of people, because

138
00:12:02,840 --> 00:12:07,320
you've got, sorry, put a dance thing in that kind of thing, it's just bizarre, some of

139
00:12:07,320 --> 00:12:13,680
the things that these other students claim, and it's for a terabyte of Buddhists to think

140
00:12:13,680 --> 00:12:18,880
of, sorry, put a dance thing, and talking about how bummed out he was before he heard

141
00:12:18,880 --> 00:12:27,200
this new suit, this is like, wow, how far can you get from the original 13?

142
00:12:27,200 --> 00:12:35,800
There could be big differences with monastics as well, I go to some local Mahayana centers

143
00:12:35,800 --> 00:12:40,440
basically to get together with friends, we go for meditation events at different centers,

144
00:12:40,440 --> 00:12:48,960
we're at one this morning, and in this tradition Koreans and the monks are married, I believe

145
00:12:48,960 --> 00:12:55,000
they only hold the five precepts, I don't believe they hold anything other than their

146
00:12:55,000 --> 00:13:03,240
ordinary five precepts, they marry, they work, they handle money, they have families, it's

147
00:13:03,240 --> 00:13:09,000
a whole different thing, but that's the expression of religion, I mean, yes, probably that

148
00:13:09,000 --> 00:13:15,720
has something to do with some texts, but more modern texts, it's hard to go by that, because

149
00:13:15,720 --> 00:13:21,680
even in many Mahayana countries there are monks, like we were talking about this as well

150
00:13:21,680 --> 00:13:27,480
in Taiwan, there are some Mahayana monasteries that, and there have always been in Chinese

151
00:13:27,480 --> 00:13:33,120
Buddhism, these groups who actually kept the original precepts, which actually in Mahayana

152
00:13:33,120 --> 00:13:39,600
are very similar to teravada precepts, it's just most of them don't keep it, but in general

153
00:13:39,600 --> 00:13:44,680
in regards to practice of the dhamma or practice of the vigna, it's hard to go by that,

154
00:13:44,680 --> 00:13:50,600
because it's so diverse, in Thailand there's a lot of monks who don't even keep private precepts,

155
00:13:50,600 --> 00:13:59,240
well, openly they pretend to, but I think the only real way to describe the difference

156
00:13:59,240 --> 00:14:07,120
is to go by the texts, as far as Buddhism goes, it's a text-based religion, it has texts

157
00:14:07,120 --> 00:14:14,640
that are understood to be canonical, now Zen might argue against that and say no, it

158
00:14:14,640 --> 00:14:21,640
was handed down from teacher to teacher, there's a strong lineage and you've got the

