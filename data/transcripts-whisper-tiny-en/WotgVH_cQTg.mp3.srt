1
00:00:00,000 --> 00:00:15,700
Hello, this is Adder, a volunteer for Syrian Mongolo International, the organization that

2
00:00:15,700 --> 00:00:19,600
supports Bontayutadamobiku in its teaching.

3
00:00:19,600 --> 00:00:24,120
I'd like to take some time today to share with you information about the ways you can support

4
00:00:24,120 --> 00:00:26,320
this organization.

5
00:00:26,320 --> 00:00:31,520
All of the teachings and services provided by you to Damo and Syrian Mongolo International

6
00:00:31,520 --> 00:00:37,560
are offered free of charge, and we never expect any payment or services in return.

7
00:00:37,560 --> 00:00:43,200
However, many of those within our community seek ways to support the organization, and

8
00:00:43,200 --> 00:00:49,680
our continued operation depends on donations and volunteer time of supporters.

9
00:00:49,680 --> 00:00:55,240
Visit SyrianMongolo.org slash support at any time for current information about the ways you

10
00:00:55,240 --> 00:00:57,120
can offer support.

11
00:00:57,120 --> 00:01:03,000
You may also reach out to us through our contact page, on the site, or join our Discord

12
00:01:03,000 --> 00:01:04,000
server.

13
00:01:04,000 --> 00:01:11,200
If you'd like more info, or would like to offer support in other ways, not mentioned.

14
00:01:11,200 --> 00:01:13,960
One way to offer support is financially.

15
00:01:13,960 --> 00:01:18,280
We are Canadian registered charity and can receive donations through PayPal Giving

16
00:01:18,280 --> 00:01:24,520
Fund, Facebook, or Bank Transfer through transferwise.com.

17
00:01:24,520 --> 00:01:26,680
Our support page has more info.

18
00:01:26,680 --> 00:01:31,640
Please feel free to reach out to us if you're having trouble transferring funds.

19
00:01:31,640 --> 00:01:35,200
Dinations are tax-deductible for residents of Canada.

20
00:01:35,200 --> 00:01:39,680
The financial costs of our organization come entirely from donations.

21
00:01:39,680 --> 00:01:42,880
This year, we're currently operating at a slight deficit.

22
00:01:42,880 --> 00:01:47,600
Any donations received are greatly appreciated.

23
00:01:47,600 --> 00:01:50,720
Another way to support is through a meal offering.

24
00:01:50,720 --> 00:01:55,520
When keeping with Theravada Buddhist tradition, all of Bantayutidama's meals are offered

25
00:01:55,520 --> 00:01:57,160
by lay supporters.

26
00:01:57,160 --> 00:02:02,000
Often, this is the meal prepared by the center's current house steward, though some

27
00:02:02,000 --> 00:02:05,440
individuals wish to provide meals directly.

28
00:02:05,440 --> 00:02:10,640
If you live in the Niagara or St. Catharines region of Ontario, Canada, you may come to

29
00:02:10,640 --> 00:02:13,160
offer a meal in person.

30
00:02:13,160 --> 00:02:18,600
We're also adding the opportunity for supporters to offer meals from afar, using the

31
00:02:18,600 --> 00:02:22,400
restaurant delivery service door dash.

32
00:02:22,400 --> 00:02:28,080
Supporters in Canada, US, and Australia can order a meal online to be delivered to the

33
00:02:28,080 --> 00:02:29,240
center.

34
00:02:29,240 --> 00:02:33,680
In either case, please visit our contact page and let us know you would like to offer a

35
00:02:33,680 --> 00:02:37,720
meal so we can schedule appropriately.

36
00:02:37,720 --> 00:02:43,480
Finally, you can support Siri Mangalow as a volunteer.

37
00:02:43,480 --> 00:02:48,920
Siri Mangalow International has a lot of different projects going on, such as running a meditation

38
00:02:48,920 --> 00:02:55,000
center and housing among, publishing teachings on YouTube, disseminating meditation

39
00:02:55,000 --> 00:03:01,080
booklets, developing software to aid in the study of scripture, hosting a weekly domless

40
00:03:01,080 --> 00:03:07,600
study group, and hosting an online meditation community called Meditation Plus.

41
00:03:07,600 --> 00:03:10,880
We are a community of volunteers with no paid staff.

42
00:03:10,880 --> 00:03:17,600
The efforts of our volunteers serve as a valuable gift to our organization.

43
00:03:17,600 --> 00:03:22,880
If you're interested in volunteering, please join our Discord server to plug in.

44
00:03:22,880 --> 00:03:29,400
I'd like presently to speak to a few of our current areas needing volunteers.

45
00:03:29,400 --> 00:03:32,720
The first is our need for a steward for the center.

46
00:03:32,720 --> 00:03:37,040
We are always on the lookout for individuals engaged with our tradition who have completed

47
00:03:37,040 --> 00:03:42,520
live course under you to download instruction to serve as steward, which involves taking

48
00:03:42,520 --> 00:03:48,000
care of the house, preparing meals, and helping with some of the local logistics of the

49
00:03:48,000 --> 00:03:49,000
center.

50
00:03:49,000 --> 00:03:53,720
Due to the global pandemic, our center is currently closed to the public, but we are

51
00:03:53,720 --> 00:03:59,640
still responsible for providing the material requisites to our resident monk.

52
00:03:59,640 --> 00:04:04,240
Because international travel is restricted, we are currently seeking an individual within

53
00:04:04,240 --> 00:04:07,200
Canada to serve as steward.

54
00:04:07,200 --> 00:04:12,000
If you have completed courses under Bantayu to Damo, and would like to take this opportunity

55
00:04:12,000 --> 00:04:17,720
to be of service, while living simply and abiding by the aid precepts, please let us know

56
00:04:17,720 --> 00:04:22,880
on our Discord server or through the contact page.

57
00:04:22,880 --> 00:04:28,280
We also benefit when we have a robust network of local volunteers in Ontario.

58
00:04:28,280 --> 00:04:32,480
Local volunteers coordinate with our center steward, as well as our volunteers from

59
00:04:32,480 --> 00:04:39,000
afar, to help with various logistical tasks, such as delivering groceries or other items,

60
00:04:39,000 --> 00:04:42,360
and helping with occasional on-site jobs.

61
00:04:42,360 --> 00:04:46,880
At times, the center is operated without a steward, and we would like to make sure to

62
00:04:46,880 --> 00:04:52,000
have a network of volunteers ready for such a time, so we can ensure that the center is

63
00:04:52,000 --> 00:04:56,160
cared for, and Bantayu has his basic needs met.

64
00:04:56,160 --> 00:05:01,400
If you are in the Niagara-Sane Catherine's region and are interested in serving as a local

65
00:05:01,400 --> 00:05:08,280
volunteer, please fill out the form in the link in the description.

66
00:05:08,280 --> 00:05:12,280
We are currently working on various projects to support the distribution of Bantayu

67
00:05:12,280 --> 00:05:14,240
to Damo's teachings.

68
00:05:14,240 --> 00:05:18,920
We recently began a book project, with the intent to convert some of Bantayu's talks

69
00:05:18,920 --> 00:05:24,520
to text, then edit and arrange them to have a broad and deep text touching on key ideas

70
00:05:24,520 --> 00:05:26,080
of the Damo.

71
00:05:26,080 --> 00:05:31,640
We need more individuals to help with selecting and editing these texts.

72
00:05:31,640 --> 00:05:37,000
We've also kicked off an audio library project, working on creating easily accessible

73
00:05:37,000 --> 00:05:41,480
and downloadable audio files of Bantayu to Damo's videos.

74
00:05:41,480 --> 00:05:47,520
The goal is to have organized audio files offered as podcasts on various platforms.

75
00:05:47,520 --> 00:05:52,000
We would welcome any volunteers who would like to help with this project, and especially

76
00:05:52,000 --> 00:05:57,640
any volunteers who have background or knowledge relating to managing audio files or creating

77
00:05:57,640 --> 00:06:00,200
podcasts.

78
00:06:00,200 --> 00:06:04,920
To volunteer, please click the link below and join the Discord channel.

79
00:06:04,920 --> 00:06:08,920
Send a message in the general channel to be added to the project you're interested in

80
00:06:08,920 --> 00:06:13,920
and introduce yourself in the corresponding channel.

81
00:06:13,920 --> 00:06:19,120
Thank you all for listening, and thanks to those who support the work we do.

82
00:06:19,120 --> 00:06:21,960
May you be happy and peaceful.

83
00:06:21,960 --> 00:06:22,960
Thank you.

