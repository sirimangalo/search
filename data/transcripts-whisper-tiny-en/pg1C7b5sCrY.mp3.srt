1
00:00:00,000 --> 00:00:05,000
Hello and welcome back to our study of the Damapana.

2
00:00:05,000 --> 00:00:13,000
Today we continue on with the first 170 which rains as follows.

3
00:00:13,000 --> 00:00:20,000
We are going to have a very long time.

4
00:00:20,000 --> 00:00:27,000
We are going to have a very long time.

5
00:00:27,000 --> 00:00:36,000
Which means as one would look at a bubble,

6
00:00:36,000 --> 00:00:42,000
as one would look at a mirage.

7
00:00:42,000 --> 00:00:45,000
One who looks at the world in this way,

8
00:00:45,000 --> 00:00:47,000
the king of death never sees.

9
00:00:47,000 --> 00:00:54,000
Doesn't see.

10
00:00:54,000 --> 00:00:58,000
So here we have a short put-sweet story.

11
00:00:58,000 --> 00:01:03,000
I think it's quite useful for instructive purposes.

12
00:01:03,000 --> 00:01:06,000
There's a good message here.

13
00:01:06,000 --> 00:01:08,000
Especially for meditators.

14
00:01:08,000 --> 00:01:14,000
This is a story about 500 Vipasa-kei-bikhu.

15
00:01:14,000 --> 00:01:17,000
Vipasa-kei-bikhu.

16
00:01:17,000 --> 00:01:22,000
So Vipasa-kei-bikhu, monks who saw clearly

17
00:01:22,000 --> 00:01:25,000
practicing this equally.

18
00:01:25,000 --> 00:01:32,000
Vipasa-nei-bipasa-kei.

19
00:01:32,000 --> 00:01:37,000
Now, 500, I'm not sure if I would take that literally.

20
00:01:37,000 --> 00:01:41,000
There's been some talk about how you should see 500.

21
00:01:41,000 --> 00:01:46,000
It's just meaning a large group.

22
00:01:46,000 --> 00:01:53,000
This was a large group of monks who obtained meditation subject from the Buddha.

23
00:01:53,000 --> 00:01:57,000
They taught them how to meditate.

24
00:01:57,000 --> 00:02:02,000
Probably some sort of combination of summit meditation.

25
00:02:02,000 --> 00:02:06,000
Vipasa-nei-bipasa-nei teaching them how to calm their minds

26
00:02:06,000 --> 00:02:13,000
and how to clear their minds, clean the cleanse their minds.

27
00:02:13,000 --> 00:02:20,000
They went to the forest and they practiced meditation, devoted themselves to it,

28
00:02:20,000 --> 00:02:24,000
but they couldn't gain any attainment.

29
00:02:24,000 --> 00:02:28,000
They practiced for quite some time.

30
00:02:28,000 --> 00:02:32,000
And they thought, well, you know, on this,

31
00:02:32,000 --> 00:02:36,000
we gained this, all this knowledge from the Buddha.

32
00:02:36,000 --> 00:02:40,000
We gained all these meditation subjects or whatever subject they gained

33
00:02:40,000 --> 00:02:42,000
and they thought, it's just not working.

34
00:02:42,000 --> 00:02:47,000
Let's go back to the Buddha and tell them it didn't work.

35
00:02:47,000 --> 00:02:51,000
That he needs to give us a new meditation subject.

36
00:02:51,000 --> 00:02:55,000
Now, on the way back, they came upon a mirage,

37
00:02:55,000 --> 00:02:59,000
which I'm not sure I've never seen a mirage,

38
00:02:59,000 --> 00:03:03,000
but I imagine it's something you'd see in a desert right in the movies you see.

39
00:03:03,000 --> 00:03:12,000
Some sort of mirage, the text actually doesn't elaborate on what sort of a mirage they saw.

40
00:03:12,000 --> 00:03:18,000
But it is possible that you see that you saw them when you're

41
00:03:18,000 --> 00:03:22,000
normally you'd see them when you're dying or when you're dying of thirst

42
00:03:22,000 --> 00:03:27,000
and suddenly the delusion meditators see mirages in the middle of the night.

43
00:03:27,000 --> 00:03:35,000
You look at the floor and you see all sorts of crazy things.

44
00:03:35,000 --> 00:03:41,000
And you're practicing all night sometimes when you see things in the wooden floors.

45
00:03:41,000 --> 00:03:46,000
I think it kind of crazy.

46
00:03:46,000 --> 00:03:51,000
But I suppose in nature there's possible for them to see something,

47
00:03:51,000 --> 00:03:55,000
hey, it doesn't that look like maybe a windstorm and it looks like something

48
00:03:55,000 --> 00:04:00,000
or a cloud. You see clouds that look like bunny rabbits and so on.

49
00:04:00,000 --> 00:04:04,000
Not really important. Some other song mirage.

50
00:04:04,000 --> 00:04:08,000
And this picture, this looking at the mirage,

51
00:04:08,000 --> 00:04:13,000
they found themselves in transpire.

52
00:04:13,000 --> 00:04:18,000
It must have been something very strange in nature that they found themselves

53
00:04:18,000 --> 00:04:23,000
transfixed by it. And they focused on it,

54
00:04:23,000 --> 00:04:26,000
just standing there or walking or whatever.

55
00:04:26,000 --> 00:04:29,000
They were standing by the side of the road.

56
00:04:29,000 --> 00:04:36,000
I saw this mirage and they just stared at it for a while and it really calmed their mind down.

57
00:04:36,000 --> 00:04:44,000
And it calmed their mind to such an extent that they had this image in their mind

58
00:04:44,000 --> 00:04:46,000
the whole way home I guess.

59
00:04:46,000 --> 00:04:51,000
And they were able to become very focused in their mind.

60
00:04:51,000 --> 00:04:55,000
And then they got back to the monastery.

61
00:04:55,000 --> 00:04:59,000
And as soon as they got back to the monastery it started raining.

62
00:04:59,000 --> 00:05:05,000
And so they all quickly walked under some cover and they stood there

63
00:05:05,000 --> 00:05:08,000
and they stood there watching the rain.

64
00:05:08,000 --> 00:05:14,000
And they watched as the rain would hit the ground and explode.

65
00:05:14,000 --> 00:05:19,000
The rain drops would explode or be the splattering of rain.

66
00:05:19,000 --> 00:05:30,000
And again with this quieted mind, this calm mind they watched this incessant

67
00:05:30,000 --> 00:05:40,000
bursting of rain, of rain drops.

68
00:05:40,000 --> 00:05:44,000
And it somehow resonated with them.

69
00:05:44,000 --> 00:05:47,000
And again this time I didn't calm them down.

70
00:05:47,000 --> 00:05:52,000
It kind of agitated them and they started to feel like this is what life is.

71
00:05:52,000 --> 00:06:03,000
This is what life is like this incessant experience of everything

72
00:06:03,000 --> 00:06:08,000
that we experience ourselves and the world around us.

73
00:06:08,000 --> 00:06:11,000
It's just this arising and ceasing.

74
00:06:11,000 --> 00:06:20,000
And so because it was so strong rain, it was such a violent experience

75
00:06:20,000 --> 00:06:26,000
and it really resonated this with them, this arising and ceasing.

76
00:06:26,000 --> 00:06:29,000
And so that's where this story comes from.

77
00:06:29,000 --> 00:06:35,000
The Buddha then came and told them this verse, taught them this verse.

78
00:06:35,000 --> 00:06:38,000
And so we have to unpack what's going on here.

79
00:06:38,000 --> 00:06:45,000
If you look at the two experiences, it's setting up two specific types of meditation.

80
00:06:45,000 --> 00:06:51,000
They saw something interesting on their way home and it attracted their attention

81
00:06:51,000 --> 00:06:54,000
and they were able to use it as an object of tranquility meditation.

82
00:06:54,000 --> 00:06:56,000
It calmed their minds out.

83
00:06:56,000 --> 00:07:01,000
The rain drops or another experience of nature that rain,

84
00:07:01,000 --> 00:07:04,000
there's no one object that you can fix on.

85
00:07:04,000 --> 00:07:09,000
And so rather than attaining tranquility by focusing on it,

86
00:07:09,000 --> 00:07:16,000
there's constant bursts of experience.

87
00:07:16,000 --> 00:07:23,000
They were able to make it resonate with their own experience of everything arising and ceasing.

88
00:07:23,000 --> 00:07:25,000
All of their experiences.

89
00:07:25,000 --> 00:07:34,000
And so gain insight from it or use it as a catalyst to help them focus their minds on gaining insight.

90
00:07:34,000 --> 00:07:39,000
And when the Buddha comes and tells them the verse,

91
00:07:39,000 --> 00:07:45,000
it seems to be capitalizing on their state of mind certainly,

92
00:07:45,000 --> 00:07:50,000
but also taking what's in their experience and applying it.

93
00:07:50,000 --> 00:08:03,000
Re-shifting their focus and realigning their attention to what is really important.

94
00:08:03,000 --> 00:08:08,000
So let's go through what's really important.

95
00:08:08,000 --> 00:08:14,000
I think the first, the important lesson that comes from the story itself

96
00:08:14,000 --> 00:08:20,000
is that first of all, don't be discouraged when your meditation doesn't bear fruit

97
00:08:20,000 --> 00:08:29,000
because the work that you do in cultivating mindfulness and all the good faculties,

98
00:08:29,000 --> 00:08:36,000
confidence, effort, concentration, wisdom, the most importantly mindfulness.

99
00:08:36,000 --> 00:08:42,000
The work that you do in cultivating this isn't lost.

100
00:08:42,000 --> 00:08:49,000
I talk often about not being discouraged or concerned with results,

101
00:08:49,000 --> 00:08:54,000
but ask yourself whether your quality of mind is wholesome.

102
00:08:54,000 --> 00:09:03,000
And the further you get in meditation, the more clear that will be as a source of confidence and inspiration.

103
00:09:03,000 --> 00:09:08,000
In the beginning you want results, you need some sign that it's working,

104
00:09:08,000 --> 00:09:14,000
but a sign of progress is to stop looking for results and start looking at now.

105
00:09:14,000 --> 00:09:23,000
You're being able to have complete conviction about what states of mind are wholesome

106
00:09:23,000 --> 00:09:28,000
and what states of mind are wholesome, which are good for you and which are bad for you.

107
00:09:28,000 --> 00:09:37,000
And to be clear when your mind isn't a good state.

108
00:09:37,000 --> 00:09:42,000
And so the cultivation of this goodness will bring results.

109
00:09:42,000 --> 00:09:46,000
These monks got discouraged and you might say they got discouraged too early

110
00:09:46,000 --> 00:09:51,000
because they were ripe for insight and tranquility on their way home.

111
00:09:51,000 --> 00:09:55,000
All it took was some specific experiences.

112
00:09:55,000 --> 00:09:57,000
And it awakened.

113
00:09:57,000 --> 00:10:01,000
I wouldn't say you wouldn't want to read this as all that time was a waste

114
00:10:01,000 --> 00:10:06,000
and then all they should have done is just walk back and forth on this road until they saw these specific things.

115
00:10:06,000 --> 00:10:11,000
No, their minds were primed for this through all the practice they had done.

116
00:10:11,000 --> 00:10:16,000
And you find this can be the case. You go home and when you least expect it,

117
00:10:16,000 --> 00:10:24,000
some insight arises or even attainment of cessation, people can attain nibana and doing anything.

118
00:10:24,000 --> 00:10:31,000
But not because of how their mind was at that moment, but because of how much work they had done.

119
00:10:31,000 --> 00:10:36,000
It builds up, it gathers up, it doesn't go away.

120
00:10:36,000 --> 00:10:42,000
It doesn't disappear.

121
00:10:42,000 --> 00:10:55,000
But the second lesson, I think, is that

122
00:10:55,000 --> 00:11:02,000
we shouldn't disparage ordinary experience as an object in meditation.

123
00:11:02,000 --> 00:11:06,000
I guess it's really an extension of that.

124
00:11:06,000 --> 00:11:18,000
And watching ordinary things is often or sometimes better than watching a formal technique,

125
00:11:18,000 --> 00:11:24,000
a formal object and a technique because it may resonate with you more, because it's more natural.

126
00:11:24,000 --> 00:11:30,000
If you can become mindful in your daily life, it can often be more powerful

127
00:11:30,000 --> 00:11:34,000
because it changes something very deep in your heart.

128
00:11:34,000 --> 00:11:40,000
If you come here and practice meditation, this is very far removed from that side of your life

129
00:11:40,000 --> 00:11:44,000
that you're familiar with for so many years.

130
00:11:44,000 --> 00:11:50,000
And so it can lead to this sort of split personality where you're one way

131
00:11:50,000 --> 00:11:55,000
in the meditation center, but you go home and you're another way.

132
00:11:55,000 --> 00:12:00,000
If you manage to be mindful in your daily life, that's no longer the case.

133
00:12:00,000 --> 00:12:06,000
You're able to change the very core of your personality.

134
00:12:06,000 --> 00:12:12,000
So natural experiences like this that were familiar to these monks

135
00:12:12,000 --> 00:12:21,000
actually may have been very important or seem to be very important in their attainment of enlightenment.

136
00:12:21,000 --> 00:12:25,000
I think that's about it for the story, for the verse.

137
00:12:25,000 --> 00:12:31,000
I mean, the other thing that we can notice is that there's, let me mention, there's clear distinction

138
00:12:31,000 --> 00:12:33,000
between summit to meditation in sight meditation.

139
00:12:33,000 --> 00:12:40,000
But the verse, I think, is much more focused on insight meditation.

140
00:12:40,000 --> 00:12:49,000
So it takes these two, in the story, the mirage was used to cultivate inside a tranquility meditation.

141
00:12:49,000 --> 00:12:59,000
The bubbles, they're not really bubbles, but the raindrops were used to cultivate inside.

142
00:12:59,000 --> 00:13:05,000
But in the verse, he talks about the world being like a bubble and the world being like a mirage.

143
00:13:05,000 --> 00:13:08,000
This is how one should see the world.

144
00:13:08,000 --> 00:13:12,000
And this highlights two important aspects of insight meditation.

145
00:13:12,000 --> 00:13:14,000
A bubble.

146
00:13:14,000 --> 00:13:18,000
A bubble is something, actually, let's put a mirage first.

147
00:13:18,000 --> 00:13:21,000
I think the commentary does that as well.

148
00:13:21,000 --> 00:13:24,000
Yeah, let's look at the mirage first.

149
00:13:24,000 --> 00:13:28,000
A mirage is something that's not real.

150
00:13:28,000 --> 00:13:34,000
When you look at a cloud and you see a bunny rabbit, there's no bunny rabbit there.

151
00:13:34,000 --> 00:13:41,000
And you might say, well, who's to say there is no bunny rabbit, right?

152
00:13:41,000 --> 00:13:46,000
Well, I think if you went close to the cloud, if you investigated everything about the cloud,

153
00:13:46,000 --> 00:13:51,000
we're all pretty clear that many characteristics of bunny rabbits would be missing.

154
00:13:51,000 --> 00:14:00,000
You wouldn't be able to feed it salary or you wouldn't be able to pet it and feel it for...

155
00:14:00,000 --> 00:14:04,000
It wouldn't have to get pregnant and have other baby rabbits and so on.

156
00:14:04,000 --> 00:14:08,000
The idea that there's a bunny rabbit there is an illusion.

157
00:14:08,000 --> 00:14:11,000
That's what a mirage is.

158
00:14:11,000 --> 00:14:17,000
So there is some sense that you're more right to say there is no bunny rabbit.

159
00:14:17,000 --> 00:14:20,000
There is no mirage.

160
00:14:20,000 --> 00:14:25,000
And that's important because if you base it on...

161
00:14:25,000 --> 00:14:28,000
If you base your reality on, say, there's...

162
00:14:28,000 --> 00:14:32,000
In India, they thought the image of the moon was of a bunny rabbit as well.

163
00:14:32,000 --> 00:14:37,000
So they talk about it in the old texts about a bunny rabbit in the moon.

164
00:14:37,000 --> 00:14:43,000
But if you base your life on it, there's going to be consequences.

165
00:14:43,000 --> 00:14:47,000
Not very far reaching ones, but to believe there actually is a bunny in the moon.

166
00:14:47,000 --> 00:14:54,000
It's going to distort your perception of reality to a fairly minimal level.

167
00:14:54,000 --> 00:15:00,000
But it gets more important when you have illusions about important things, right?

168
00:15:00,000 --> 00:15:07,000
If you have an illusion that everyone respects you and looks up to you,

169
00:15:07,000 --> 00:15:12,000
but in fact they just fear you or maybe they're disgusted by you.

170
00:15:12,000 --> 00:15:16,000
If you think like some men think all the women find them attractive

171
00:15:16,000 --> 00:15:19,000
or some women think all the men find them attractive,

172
00:15:19,000 --> 00:15:21,000
I mean this becomes more important.

173
00:15:21,000 --> 00:15:29,000
These illusions that we have are the way we perceive things becomes very important.

174
00:15:29,000 --> 00:15:51,000
And on the deepest level, our relationship with concepts as a whole is the most important sort of...

175
00:15:51,000 --> 00:16:01,000
part of this concept of illusion because a concept is an illusion in some sense.

176
00:16:01,000 --> 00:16:04,000
And by concept let's understand what we mean by concept.

177
00:16:04,000 --> 00:16:18,000
A concept is the concept of a microphone or the concept of a carpet, a concept of a person.

178
00:16:18,000 --> 00:16:28,000
It's a concept because it only exists in the minds of the people who think in the minds of sentient beings.

179
00:16:28,000 --> 00:16:35,000
A person doesn't exist in ultimate reality.

180
00:16:35,000 --> 00:16:43,000
If you take apart a car, you don't find anything, you don't find a car in there.

181
00:16:43,000 --> 00:16:57,000
If you take apart a human being, you don't find human being.

182
00:16:57,000 --> 00:17:05,000
And so to some extent concepts and entity are things, all things,

183
00:17:05,000 --> 00:17:14,000
well most things are just illusions, they're just like a mirage.

184
00:17:14,000 --> 00:17:24,000
And why this is the most important, why this gets important is because

185
00:17:24,000 --> 00:17:33,000
the way that entities work because they're based entirely on our mental perception of them

186
00:17:33,000 --> 00:17:39,000
is that they are then subject to and distorted by our minds.

187
00:17:39,000 --> 00:17:42,000
So all the people around us are family, let's say.

188
00:17:42,000 --> 00:17:47,000
We think of our family in a certain way and we become fond of them.

189
00:17:47,000 --> 00:17:53,000
And we have the idea that they're going to make us happy and we have expectations about them.

190
00:17:53,000 --> 00:17:56,000
We have narratives about them.

191
00:17:56,000 --> 00:18:04,000
Our loved ones, our romantic partner, I think our romantic partner is the most,

192
00:18:04,000 --> 00:18:12,000
is one of the clearest because you have conceptions about who they are based on this entity that you've created.

193
00:18:12,000 --> 00:18:14,000
But in fact, what are they?

194
00:18:14,000 --> 00:18:25,000
They are a mass bundle of confused, often convoluted, messed-up habits that they've developed

195
00:18:25,000 --> 00:18:29,000
over their lives.

196
00:18:29,000 --> 00:18:39,000
And so a big reason why we're disappointed in our family and our friends and our lovers and so on

197
00:18:39,000 --> 00:18:45,000
is because we had a view of what they were and that view was shattered

198
00:18:45,000 --> 00:18:50,000
or that the reality did not align with that view.

199
00:18:50,000 --> 00:18:56,000
This is a way of describing death when a person dies, well, your idea of them is as alive.

200
00:18:56,000 --> 00:19:02,000
And so reality is not as you want it to be.

201
00:19:02,000 --> 00:19:12,000
And we're constantly out of sync with reality as a result of our living in a world of concepts.

202
00:19:12,000 --> 00:19:17,000
You can see this as a meditator when you practice meditation, this changes.

203
00:19:17,000 --> 00:19:25,000
And you look at people and you have an understanding based on your own experience of what they are.

204
00:19:25,000 --> 00:19:33,000
That this is just a complex organism, a system of experiences.

205
00:19:33,000 --> 00:19:39,000
And so you have very little expectations because you're clear that you don't know what's going to come next.

206
00:19:39,000 --> 00:19:43,000
You're much clearer that things are much more complicated than just saying,

207
00:19:43,000 --> 00:19:47,000
oh, this is so-and-so in this sort of person.

208
00:19:47,000 --> 00:19:51,000
They're like this, like that.

209
00:19:51,000 --> 00:20:03,000
You start to see at least that things are very complicated.

210
00:20:03,000 --> 00:20:08,000
So you see the distinction between concepts and reality.

211
00:20:08,000 --> 00:20:16,000
A person who studies reality is able to see things experience as a rising and ceasing.

212
00:20:16,000 --> 00:20:24,000
And it's able to see that the world of entities is just a mirage.

213
00:20:24,000 --> 00:20:27,000
The other side of it is the bubble side.

214
00:20:27,000 --> 00:20:29,000
And this is the arising and ceasing.

215
00:20:29,000 --> 00:20:32,000
This is once you look at the world as a bubble.

216
00:20:32,000 --> 00:20:38,000
Bubbles are things that arise and cease about the imagery of a bubble

217
00:20:38,000 --> 00:20:44,000
is to give you the sense of something that pops, something that has just disappeared.

218
00:20:44,000 --> 00:20:49,000
Where did the bubble go? It's just going.

219
00:20:49,000 --> 00:20:59,000
And this is the other side of this idea of the problem with concepts

220
00:20:59,000 --> 00:21:05,000
because experiences are chaotic, impermanent.

221
00:21:05,000 --> 00:21:08,000
And concepts are not.

222
00:21:08,000 --> 00:21:11,000
The concepts of, I have concepts of people who have died.

223
00:21:11,000 --> 00:21:13,000
I have concepts of my grandparents.

224
00:21:13,000 --> 00:21:18,000
I can conceive of them in my mind and think of them.

225
00:21:18,000 --> 00:21:23,000
But that person is only exists in our minds, the minds of the people who knew them.

226
00:21:23,000 --> 00:21:28,000
The reality, the reality, who knows where they are.

227
00:21:28,000 --> 00:21:32,000
Certainly, their body is totally gone.

228
00:21:32,000 --> 00:21:36,000
I was away in Asia for a long time and when I came back, my parents,

229
00:21:36,000 --> 00:21:39,000
they had all gotten older.

230
00:21:39,000 --> 00:21:43,000
My parents, my family, they had all changed.

231
00:21:43,000 --> 00:21:46,000
Someone you haven't seen in a while.

232
00:21:46,000 --> 00:21:50,000
It really can shake you up. It can shock you, surprise you.

233
00:21:50,000 --> 00:21:52,000
When you see how much they've changed.

234
00:21:52,000 --> 00:21:54,000
How much I had changed.

235
00:21:54,000 --> 00:21:59,000
When you go home as a meditator, sometimes you find your family is quite shocked.

236
00:21:59,000 --> 00:22:04,000
It feels like you're a new person and they sometimes mourn the loss

237
00:22:04,000 --> 00:22:08,000
of the person they love because they were clinging to them.

238
00:22:08,000 --> 00:22:11,000
I mean, these are just simple examples.

239
00:22:11,000 --> 00:22:16,000
But this goes on incessantly throughout our day, not to mention our lives.

240
00:22:16,000 --> 00:22:23,000
We're constantly surprised and taken off guard by experience.

241
00:22:23,000 --> 00:22:26,000
It's because our focus isn't there.

242
00:22:26,000 --> 00:22:31,000
Our focus during a day is on, let's say, the meal.

243
00:22:31,000 --> 00:22:35,000
So we have, okay, here's our meal and this is set out and I have good.

244
00:22:35,000 --> 00:22:37,000
I got some soup.

245
00:22:37,000 --> 00:22:39,000
I got some salad or some rice or whatever.

246
00:22:39,000 --> 00:22:47,000
But then you take a bite and it's not how you expect it and there's disappointment.

247
00:22:47,000 --> 00:22:51,000
It's too cold when you want it to be hot.

248
00:22:51,000 --> 00:22:56,000
When we're dwelling in concepts, I say remind us preoccupied.

249
00:22:56,000 --> 00:23:02,000
When we're dwelling in reality, when we're with this experience moment by moment.

250
00:23:02,000 --> 00:23:06,000
When we learn, when we become familiar with this.

251
00:23:06,000 --> 00:23:12,000
Then this idea of being surprised and upset is just totally foreign.

252
00:23:12,000 --> 00:23:15,000
And there's no place.

253
00:23:15,000 --> 00:23:20,000
Our relationship with reality is it arises and ceases, it comes and goes.

254
00:23:20,000 --> 00:23:28,000
It's important to understand that reality is quite simple in that way.

255
00:23:28,000 --> 00:23:33,000
And our relationship with reality never has to be one of fixing, changing,

256
00:23:33,000 --> 00:23:38,000
and certainly not getting upset or reacting to it.

257
00:23:38,000 --> 00:23:46,000
And our relationship should be seeing that it comes and goes.

258
00:23:46,000 --> 00:23:51,000
It comes and goes like a bubble.

259
00:23:51,000 --> 00:23:54,000
And the second part of the verse, if one does this, looks upon the world in this way.

260
00:23:54,000 --> 00:23:58,000
The king of death will not see them.

261
00:23:58,000 --> 00:23:59,000
So what does this mean?

262
00:23:59,000 --> 00:24:01,000
The king of death's not.

263
00:24:01,000 --> 00:24:02,000
It won't see them.

264
00:24:02,000 --> 00:24:05,000
It's an interesting phrase.

265
00:24:05,000 --> 00:24:11,000
What it means is they won't die, obviously.

266
00:24:11,000 --> 00:24:16,000
But it's interesting because, well, they will die.

267
00:24:16,000 --> 00:24:20,000
Even the enlightened beings die.

268
00:24:20,000 --> 00:24:25,000
But I think there's some sense where they wouldn't want to call it death.

269
00:24:25,000 --> 00:24:27,000
Where they would have said, no, no.

270
00:24:27,000 --> 00:24:30,000
An enlightened person would go through his own death.

271
00:24:30,000 --> 00:24:33,000
I've heard monks say this sort of thing.

272
00:24:33,000 --> 00:24:40,000
I think the reasoning is this because death is defined by the process of

273
00:24:40,000 --> 00:24:45,000
going from this existence into another existence.

274
00:24:45,000 --> 00:24:49,000
It's that transfer.

275
00:24:49,000 --> 00:24:51,000
That's not how I would define death.

276
00:24:51,000 --> 00:24:56,000
But if you talk about death as being the breakup of the body,

277
00:24:56,000 --> 00:24:59,000
when the body stops working, that's death.

278
00:24:59,000 --> 00:25:02,000
But that's why they call it death.

279
00:25:02,000 --> 00:25:09,000
What they call near death experiences is actually after death.

280
00:25:09,000 --> 00:25:11,000
The body dies.

281
00:25:11,000 --> 00:25:13,000
And someone has these experiences.

282
00:25:13,000 --> 00:25:15,000
But then the body is revived.

283
00:25:15,000 --> 00:25:17,000
So it comes back to life.

284
00:25:17,000 --> 00:25:24,000
And they can relate those experiences that they had when they were dead.

285
00:25:24,000 --> 00:25:26,000
But what it really means.

286
00:25:26,000 --> 00:25:27,000
It means it's a big point.

287
00:25:27,000 --> 00:25:32,000
But if you just want to understand this, what it really means is that they won't die again.

288
00:25:32,000 --> 00:25:37,000
When a person who deaths will pass over and will never see,

289
00:25:37,000 --> 00:25:40,000
this is because they're not born again.

290
00:25:40,000 --> 00:25:43,000
If you're not born, you can't die.

291
00:25:43,000 --> 00:25:46,000
What it means is that they'll become free from suffering.

292
00:25:46,000 --> 00:25:50,000
What's really important about this is that they won't,

293
00:25:50,000 --> 00:25:57,000
like everyone else have to come back and do this again and again and again.

294
00:25:57,000 --> 00:26:02,000
Most people would be excited at the idea of having to come back,

295
00:26:02,000 --> 00:26:10,000
but do this, and I can live another life and isn't that interesting.

296
00:26:10,000 --> 00:26:13,000
But the problem is that we've been doing this again and again.

297
00:26:13,000 --> 00:26:15,000
And this is all we've come up with.

298
00:26:15,000 --> 00:26:23,000
And if you look back on your life, I think most people would admit that it wasn't all rainbows and flowers.

299
00:26:23,000 --> 00:26:29,000
It's a lot of trouble to do this again and again and again.

300
00:26:29,000 --> 00:26:36,000
That's what it means. One would be free from suffering because they're not born again.

301
00:26:36,000 --> 00:26:39,000
Anyway, point is it's a good thing.

302
00:26:39,000 --> 00:26:41,000
You see the world in this way?

303
00:26:41,000 --> 00:26:43,000
Things get better, not worse.

304
00:26:43,000 --> 00:26:46,000
Happiness comes, peace comes.

305
00:26:46,000 --> 00:26:49,000
Lots of good things come.

306
00:26:49,000 --> 00:26:53,000
So in the end, you never focus on happiness.

307
00:26:53,000 --> 00:27:00,000
Always focus on goodness and an ultimate, ultimately, wisdom.

308
00:27:00,000 --> 00:27:01,000
This kind of wisdom.

309
00:27:01,000 --> 00:27:06,000
The wisdom of seeing that everything that we look at in the world is just concept.

310
00:27:06,000 --> 00:27:09,000
People, places, things.

311
00:27:09,000 --> 00:27:15,000
It's a different world from what's actually going on underneath in the background.

312
00:27:15,000 --> 00:27:25,000
And if we focus and come to see this background activity, which is what's really going on in our experience,

313
00:27:25,000 --> 00:27:29,000
as we're caught up in the concept.

314
00:27:29,000 --> 00:27:39,000
If we see it as just arising and ceasing, there's nothing else and there's no hook that can keep us tied down.

315
00:27:39,000 --> 00:27:49,000
There's such a power and a freedom and a safety. You immune to any danger.

316
00:27:49,000 --> 00:27:54,000
This is what true freedom from suffering is.

317
00:27:54,000 --> 00:28:17,000
So that's the dumb part of verse for tonight. Thank you all for listening.

