1
00:00:00,000 --> 00:00:27,600
Good evening, broadcasting from the Stony Creek Ontario, here too, you have a daily teaching

2
00:00:27,600 --> 00:00:40,160
on the Dhamma.

3
00:00:40,160 --> 00:00:56,040
When we practice the Dhamma, we focus our attention on Dooka, this is where we should greatly

4
00:00:56,040 --> 00:01:01,080
focus our attention.

5
00:01:01,080 --> 00:01:11,400
It is the object of study, the Buddha said we must know for suffering, we must understand

6
00:01:11,400 --> 00:01:24,600
the, we must understand the things that are causing us suffering or causing us suffering.

7
00:01:24,600 --> 00:01:34,880
We think that we wrongly think of bringing us happiness, that we see truth, that it is not

8
00:01:34,880 --> 00:01:43,480
that they have not actually bringing us happiness, and that we see the suffering caused

9
00:01:43,480 --> 00:01:58,160
the suffering inherent in the things that we cling to, this is the task, the main task

10
00:01:58,160 --> 00:02:06,680
in Buddhism is quite simple actually, I think Buddhism quite simple.

11
00:02:06,680 --> 00:02:15,920
So then we have to ask ourselves what is meant by suffering, and so we look to the Buddha's

12
00:02:15,920 --> 00:02:29,920
words and find out what the Buddha said on suffering, in the words of the Buddha are

13
00:02:29,920 --> 00:02:40,280
Some key thing up in tobhadana kandar tokata and the five bhadana kandata, this is suffering

14
00:02:40,280 --> 00:02:54,460
in brief, some key thing about a situation of life after sphere.

15
00:02:54,460 --> 00:03:08,420
I have aggregates of clinging, sort of, or the five aggregates of things, the five groups

16
00:03:08,420 --> 00:03:15,180
of things which are the object of clinging.

17
00:03:15,180 --> 00:03:36,820
So this is where our study must take place, we must learn about these five things.

18
00:03:36,820 --> 00:03:43,100
So the first one, Rupa, we have to learn about form, so we have to understand the physical

19
00:03:43,100 --> 00:03:53,140
form, physical form, and Buddhism is still just a product of experience, sort of a part

20
00:03:53,140 --> 00:03:54,140
of experience.

21
00:03:54,140 --> 00:04:02,860
So what we're talking about here is the four elements of Earth, air, water, and

22
00:04:02,860 --> 00:04:10,020
fighter, which are the four aspects of our experience of, of matter.

23
00:04:10,020 --> 00:04:19,220
When we experience hardness, our softness is Earth, when we experience heat or cold, this

24
00:04:19,220 --> 00:04:32,780
is fire, when we experience pressure, this is air, and water is the cohesion and the stickiness,

25
00:04:32,780 --> 00:04:45,740
traction, draws into these together, this is the physical, the physical that underlines

26
00:04:45,740 --> 00:04:53,820
the whole of the conceptual physical world around us, so objects, people, places, things,

27
00:04:53,820 --> 00:05:04,180
all of this is conceptual that we put together in our minds, based on the experiences

28
00:05:04,180 --> 00:05:05,180
that we have.

29
00:05:05,180 --> 00:05:14,140
So we see certain things are here, smell, taste, feel on the world around us, and we

30
00:05:14,140 --> 00:05:23,140
extrapolate and create entities out of it, and these entities give us the illusion

31
00:05:23,140 --> 00:05:33,140
of permanence, stability, of pleasure, satisfaction, when we look underneath and see

32
00:05:33,140 --> 00:05:46,980
the volatile building blocks that make up the experience, make up the entities.

33
00:05:46,980 --> 00:05:57,660
This experiences that arise and sees, without warning, out of our control, then we see

34
00:05:57,660 --> 00:06:03,900
that this is the means of satisfaction, because what happens is found is actually a source

35
00:06:03,900 --> 00:06:13,340
of great suffering, when we cling to it, when we try to fix it, we try to hold on to or

36
00:06:13,340 --> 00:06:21,580
push away various aspects of the physical world, pushing away people, clinging to people,

37
00:06:21,580 --> 00:06:28,340
pushing away places, things, clinging to places and things, all this.

38
00:06:28,340 --> 00:06:39,340
So we study in inside meditation, we study the second one, feelings, we study our pleasure,

39
00:06:39,340 --> 00:06:53,100
we study our pain, we study our feelings, we study our feelings, we study it, we study

40
00:06:53,100 --> 00:07:03,460
we see that even happy feelings, we can't find satisfaction in, even happy feelings,

41
00:07:03,460 --> 00:07:15,100
have no benefit or use to us, no purpose, when they come and they go, it's not happiness

42
00:07:15,100 --> 00:07:22,020
that makes you more happy, holding on to happiness doesn't make you happy, it's not the

43
00:07:22,020 --> 00:07:29,260
cause of happiness, it's not happiness that causes happiness, in other words, we can't

44
00:07:29,260 --> 00:07:38,020
hold on to happiness, hold on to it, it doesn't make you happy, it just makes you want

45
00:07:38,020 --> 00:07:46,060
it more, need it, naturally this is upana, I need it, it is satisfied when you can't

46
00:07:46,060 --> 00:08:04,020
have it, which is inevitable, because happiness doesn't bring happiness, and we support

47
00:08:04,020 --> 00:08:12,220
study the painful feelings, the neutral feelings, the nature of feelings, and the speaking

48
00:08:12,220 --> 00:08:29,260
and how to refuge, if I refuge because they change, we study our memories and perceptions,

49
00:08:29,260 --> 00:08:38,180
the recognitions, the similarities, the reception of the world around us, that we perceive

50
00:08:38,180 --> 00:08:44,580
things to be object, so our actual perception of entities, people, places, things, this

51
00:08:44,580 --> 00:08:54,660
actual perception, we study that, and this isn't a mind, you see, there's nothing to do

52
00:08:54,660 --> 00:08:59,540
with the world around us, see something in your mind, process is it, it gets the idea

53
00:08:59,540 --> 00:09:04,140
of an entity, so you close your eyes and you think of people, and when you don't see

54
00:09:04,140 --> 00:09:15,180
them you can give rise to the idea of the person in your mind, and so we cling to these

55
00:09:15,180 --> 00:09:27,620
entities, because they're illusions, because they're only in the mind, you don't have any

56
00:09:27,620 --> 00:09:36,260
bearing on actual reality, so the entity that we hold on to changes, because it's made

57
00:09:36,260 --> 00:09:47,460
up of impermanent experiences, and as these experiences change, the entity changes, so we become

58
00:09:47,460 --> 00:09:53,140
disappointed in people when they don't act according to our image of them, disappointed

59
00:09:53,140 --> 00:10:02,380
in the objects of experience when they break, when they fall apart, when they disappear,

60
00:10:02,380 --> 00:10:09,860
when they can't happen, clinging to people, places, things like obviously this is a big

61
00:10:09,860 --> 00:10:16,980
one where we cry, when we lose someone we love, when we're upset, when we're near someone

62
00:10:16,980 --> 00:10:24,900
and then we don't like, kind of being afraid of certain people or so, all this suffering,

63
00:10:24,900 --> 00:10:32,860
you know, study this, you study how these things make us suffer, and the entity that

64
00:10:32,860 --> 00:10:43,660
can truly satisfy us comes and it goes, now come and go, we study our thoughts, our

65
00:10:43,660 --> 00:10:51,740
judgments, what we think of things, our partialities, our likes and dislikes, things that

66
00:10:51,740 --> 00:10:59,220
we would say make us most who we are, make us individuals, our preferences, our personality,

67
00:10:59,220 --> 00:11:09,420
our emotion, study all these and we see that the likes and our dislikes are not helping

68
00:11:09,420 --> 00:11:19,660
us, doesn't make us, doesn't bring happiness to like something, to want something to

69
00:11:19,660 --> 00:11:30,980
make you happy, sort of the other way around, right, sort of the vicious cycle and you

70
00:11:30,980 --> 00:11:42,300
want something and you get what you want, this pleasure associated with it, this an endless

71
00:11:42,300 --> 00:11:49,900
quest that is ever and ever, a less fulfilling, until it doesn't fulfill and you have

72
00:11:49,900 --> 00:11:55,460
to jump to something else and try to find fulfillment which of course, last eye there

73
00:11:55,460 --> 00:12:09,220
and so you jump in jumping from one, plus a pleasure search to the name, of course,

74
00:12:09,220 --> 00:12:16,140
there's the weighing when we like something, we are disinclined to things that are not

75
00:12:16,140 --> 00:12:28,220
like it and so it breeds aversion to anything, it's impossible to like everything, a lot

76
00:12:28,220 --> 00:12:33,300
of it has to do in brain chemistry it seems, some people are more inclined to like lots

77
00:12:33,300 --> 00:12:43,980
of different things, but it's only a temporary thing, like a like, some are like you have

78
00:12:43,980 --> 00:12:51,860
some more dislike you better, more intolerance you build up to, or to not having with

79
00:12:51,860 --> 00:13:11,500
you all, so all that changes as well, yet we want for a while and you lose it, sometimes

80
00:13:11,500 --> 00:13:20,180
for many years you can be in a perpetual chase, we're getting what you want, for a whole

81
00:13:20,180 --> 00:13:27,540
lifetime, you know, there are angels or humans, you can great luck with their preferences

82
00:13:27,540 --> 00:13:34,980
and be these sorts of people who seem to get what they want and live a life of positive

83
00:13:34,980 --> 00:13:43,540
thinking, even all you have to do is think positively, you talk to self-made, some quote

84
00:13:43,540 --> 00:13:49,540
unquote, self-made millionaire who do you know, they develop these philosophies that

85
00:13:49,540 --> 00:13:55,780
all you have to do is try, you know, you have to do is do what I did because it works,

86
00:13:55,780 --> 00:14:01,260
I know because I didn't, but they don't see what happened behind the scenes, much of

87
00:14:01,260 --> 00:14:07,740
it had to do with past lives and it was certainly deserved, but I also wouldn't see exactly

88
00:14:07,740 --> 00:14:13,940
what caused it and I realized that they're not perpetuating the cause, wondering for

89
00:14:13,940 --> 00:14:26,300
rude awakening, and they lose what they've gained and they're not able to get it back.

90
00:14:26,300 --> 00:14:43,500
So we study these thoughts, we study our mental, mental ecosystem, there are many different

91
00:14:43,500 --> 00:14:51,260
things that grow in our minds, study and we see how suffering is caused here, not always,

92
00:14:51,260 --> 00:14:58,820
so many of these are actually, many of us in cars actually useful and lead us to towards

93
00:14:58,820 --> 00:15:08,460
freedom, but we see how they're suffering arises and see what we're doing wrong, that's

94
00:15:08,460 --> 00:15:10,740
more important.

95
00:15:10,740 --> 00:15:19,300
I suppose equally important, I mean, focus on the good one to learn what's useful and

96
00:15:19,300 --> 00:15:28,140
learn what is harmful, and so just by learning, just by studying, we incline more towards

97
00:15:28,140 --> 00:15:37,980
what is helpful and away from what is harmful, and finally, we study consciousness,

98
00:15:37,980 --> 00:15:49,380
consciousness is the big one, that's us, that makes us real, you know, it makes us alive.

99
00:15:49,380 --> 00:15:56,460
Consciousness, we would be computers, brains that were working, operating, computer,

100
00:15:56,460 --> 00:16:05,740
artificial intelligence that appears to be thinking and playing chess and so on, but has

101
00:16:05,740 --> 00:16:11,460
no clue what it's doing, is there's no awareness, is that awareness that quite clearly

102
00:16:11,460 --> 00:16:22,140
out of quality, whatever you call it, it really has neuroscientists, I understand still

103
00:16:22,140 --> 00:16:29,300
because you can't, no matter how much you look at the brain and so the brain works, you

104
00:16:29,300 --> 00:16:37,580
can't find the mind in the brain, you can't find experience in the brain, you can't translate

105
00:16:37,580 --> 00:16:43,400
or no one knows how to translate, this is impossible to translate, physical and

106
00:16:43,400 --> 00:16:51,980
mental, and this belief that the brain causes the mind to exist, it gives rise to the

107
00:16:51,980 --> 00:17:02,460
mind, but taking, you can't translate cells and atoms into experience, what we don't

108
00:17:02,460 --> 00:17:08,780
have to actually, this is a corner that materialist scientists have paid themselves into

109
00:17:08,780 --> 00:17:15,460
physicalists who believe the mind to be a part of the brain, which is very easy in meditation

110
00:17:15,460 --> 00:17:22,980
to just bypass that and say, well, consciousness exists, so it's just studying it, it's

111
00:17:22,980 --> 00:17:31,820
a study of consciousness, full around with cells and atoms that are all conceptually,

112
00:17:31,820 --> 00:17:44,820
so we study experience, study our consciousness, sort of the overarching phenomenon that

113
00:17:44,820 --> 00:17:53,180
gives rise to all of the other aggregates, they're all dependent on consciousness, the

114
00:17:53,180 --> 00:18:06,860
mind that allows it all to happen, the awareness consciousness, so we study that, awareness,

115
00:18:06,860 --> 00:18:13,340
we at first we think it's self, we think it's I, it's an entity, we start to see that

116
00:18:13,340 --> 00:18:19,020
even this consciousness is not really under control, can't be conscious only of the things

117
00:18:19,020 --> 00:18:25,620
that I want to be conscious of, can't keep myself in being conscious of unwholesome things,

118
00:18:25,620 --> 00:18:31,820
I've earned pleasant things, can't shut out sounds that I don't want to hear, sites

119
00:18:31,820 --> 00:18:37,980
that I don't want to see, lots that I don't want to have, if we can, if consciousness

120
00:18:37,980 --> 00:18:43,580
or ours, if it was us who was conscious, there was an eye in the sense of a cell, then we

121
00:18:43,580 --> 00:18:47,980
could control, we could say, okay, I don't know how I'm going to not think about that,

122
00:18:47,980 --> 00:18:53,580
but of course we can do, I'm not going to hear that, but of course we hear it, or don't,

123
00:18:53,580 --> 00:18:58,740
and we want to hear something and don't, we want to see something and don't, we want to

124
00:18:58,740 --> 00:19:05,940
remember something and don't call this, so we study that, study how none of these things

125
00:19:05,940 --> 00:19:15,380
can satisfy us, so we start to let go of them, meditation is about freedom, freeing ourselves

126
00:19:15,380 --> 00:19:26,300
from this constant search, content, quest for satisfaction in that would cannot satisfy

127
00:19:26,300 --> 00:19:34,220
us, that's the core practice in Buddhism, so that's what we do, we practice the force

128
00:19:34,220 --> 00:19:38,820
at the time of the force, at the time of our, basically the five aggregate, it's in another

129
00:19:38,820 --> 00:19:46,220
form, to the other way of talking about terms we have, guy has, group focus on the body

130
00:19:46,220 --> 00:19:55,100
we're focusing on group, feelings is feeling, it's the same word, chita is probably best understood

131
00:19:55,100 --> 00:20:05,820
as the same as we do, and dhamma, dhamma focuses mainly on suny and sun kara, memories,

132
00:20:05,820 --> 00:20:15,300
and thought, mainly, it's not an exact overlap, but it's a full overlap, it's just not,

133
00:20:15,300 --> 00:20:21,420
it's a bit messy, especially with the force, it's at the time, that's a different time

134
00:20:21,420 --> 00:20:28,420
or a practical way of approaching the five aggregate, it's practical, so we read what the

135
00:20:28,420 --> 00:20:33,860
Buddha taught, about this atigatana, it was how to practice, to my practicing them,

136
00:20:33,860 --> 00:20:43,500
we were able to understand that, I have come this, so there you are, it should be encouragement

137
00:20:43,500 --> 00:20:52,100
for practice of satigatana, insight through a mindfulness, we're going to understand

138
00:20:52,100 --> 00:20:58,900
the noble truth of suffering, which is really, really important, it allows us to be free

139
00:20:58,900 --> 00:21:06,140
from suffering, when we see that we're crossing ourselves suffering, thereby able to escape

140
00:21:06,140 --> 00:21:15,220
that, it's always a good thing, so that's the dhamma for tonight, thank you all for

141
00:21:15,220 --> 00:21:41,580
time, thank you very much, thank you very much, thank you, thank you, thank you,

