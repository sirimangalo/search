1
00:00:30,000 --> 00:00:42,240
Good evening, everyone, and welcome to our live demo broadcast, and our nightly demo

2
00:00:42,240 --> 00:00:47,880
for those of you here in Hamilton.

3
00:00:47,880 --> 00:01:16,440
Today, I thought to talk about fear, and by it, and by it, and by it's fear, danger,

4
00:01:16,440 --> 00:01:33,240
fear, some of this terror, those of you who are still there to turn on the news, I'm sure

5
00:01:33,240 --> 00:01:50,000
are inundated by fear-mongering, we call terrorism, the intentional cultivation of fear and

6
00:01:50,000 --> 00:02:03,200
others.

7
00:02:03,200 --> 00:02:18,560
Sometimes there are other reasons for other goals, but quite often, it's quite common now

8
00:02:18,560 --> 00:02:40,760
to see the seeming single-minded purpose of psychological warfare or terrorism.

9
00:02:40,760 --> 00:03:09,360
And the first thing for us to understand about terror and terrorism.

10
00:03:09,360 --> 00:03:28,640
Is that being psychological warfare, of course, it relies upon the victim, it relies upon

11
00:03:28,640 --> 00:03:39,120
the vulnerability of the victim, and the sensitivity of the victim doesn't work against

12
00:03:39,120 --> 00:03:56,160
people who are not afraid, and obviously it seems so much trite or simplistic to suggest

13
00:03:56,160 --> 00:04:07,040
that part of the solution is to not be afraid, but it's important to understand, it's

14
00:04:07,040 --> 00:04:13,080
really important to understand as a Buddhist, the mechanics of suffering, the mechanics

15
00:04:13,080 --> 00:04:28,320
of conflict, as we know, if you go deeply into the meditation practice, even physical

16
00:04:28,320 --> 00:04:36,960
pain is only true suffering when you let it upset you, even physical pain, even true

17
00:04:36,960 --> 00:04:57,160
as a direct physical violence is only truly successful when the victim is psychologically

18
00:04:57,160 --> 00:05:06,820
vulnerable, but terrorism, terrorism which seems to usually involve a lot of physical

19
00:05:06,820 --> 00:05:07,820
violence.

20
00:05:07,820 --> 00:05:18,280
It's conducted in such a way as to have overwhelming collateral damage psychologically.

21
00:05:18,280 --> 00:05:29,320
So, of course, not to minimize the terrible tragedy of death, the physical pain that

22
00:05:29,320 --> 00:05:42,280
comes from these acts of terror, but to remark upon what makes them so effective, that's

23
00:05:42,280 --> 00:05:53,820
how it scares people, and of course, I don't think it's the solution, it's not the entire

24
00:05:53,820 --> 00:06:06,500
solution for us to stop being afraid of such things, but it is worth remarking that between

25
00:06:06,500 --> 00:06:16,420
being afraid of them and not afraid of them, not being afraid of or frightened by acts

26
00:06:16,420 --> 00:06:36,900
of gross and horrific violence as far preferable to be unshaken, to be impervious, as far

27
00:06:36,900 --> 00:06:46,380
preferable, not just for one's own self, but for one's ability to react properly and to

28
00:06:46,380 --> 00:06:55,460
consider the situation wisely.

29
00:06:55,460 --> 00:06:59,900
In the Buddhist time, I mean, I can't think of any, obviously, there were no suicide bombers

30
00:06:59,900 --> 00:07:07,220
in the Buddhist time, there were no violent shootings, I can't think of any violent killings

31
00:07:07,220 --> 00:07:13,540
of innocent individuals simply for the purpose of cultivating terror or targeting a specific

32
00:07:13,540 --> 00:07:14,540
group.

33
00:07:14,540 --> 00:07:23,220
Imagine if I thought for a while I could come up with some, but there are specific acts

34
00:07:23,220 --> 00:07:35,060
of terrorism, mild acts, but give you the general, they still give the general idea of

35
00:07:35,060 --> 00:07:43,780
responses and means of coping with or dealing with fear.

36
00:07:43,780 --> 00:07:47,640
First of all, about fear in general, we have the Dejika Sutta, which is a very important

37
00:07:47,640 --> 00:07:54,100
Sutta, really, not in terms of having any core value for leading one to enlightenment,

38
00:07:54,100 --> 00:08:01,940
but it's very important in creating Buddhist culture and creating a Buddhist outlook

39
00:08:01,940 --> 00:08:13,460
on life, as it deals directly with fear, and it deals directly with a triple gem, these

40
00:08:13,460 --> 00:08:19,980
three powerful objects of reflection, the Buddha, the Dhamma, and the Sangha, so the Buddha

41
00:08:19,980 --> 00:08:26,740
said, he talked about this war and he was telling a story about Indra, how Indra said,

42
00:08:26,740 --> 00:08:32,140
well, if you're afraid in battle on the angels fighting against the, whatever the other

43
00:08:32,140 --> 00:08:38,900
guys were, the non-angels, Asura, as they call them, fighting against them, he said,

44
00:08:38,900 --> 00:08:46,460
if you're afraid, look at my banner, look at the Dhanjika, the top of my banner, you see

45
00:08:46,460 --> 00:08:51,660
the flag, and when you see my standard there and you know that I haven't fallen, he

46
00:08:51,660 --> 00:08:56,620
will give you courage and your fear will disappear, and he said, if you don't look at

47
00:08:56,620 --> 00:09:03,300
mine, well, look at this, this general and that general and these God, look at their

48
00:09:03,300 --> 00:09:14,020
banners, and the Buddha said, you know, he can say this all he wants, but truth is, they

49
00:09:14,020 --> 00:09:21,540
look at his banner, maybe the fear will disappear, maybe it won't, because Indra is not

50
00:09:21,540 --> 00:09:26,020
a very good role model, he's not himself free from fear, he's not someone that when you

51
00:09:26,020 --> 00:09:31,740
think of him, you're mind is calm and you have a good example and a reminder of right

52
00:09:31,740 --> 00:09:40,020
and wrong and an example of a pure and unshaken individual that's not afraid of anything,

53
00:09:40,020 --> 00:09:46,900
to remind you that this is the best way, they said, but if you, I say to you monks, if

54
00:09:46,900 --> 00:09:53,500
you ever often the forest and you get afraid, or let's say those of us who are living

55
00:09:53,500 --> 00:09:59,740
in society and we're afraid, he said, think of the Buddha, and this is where we actually

56
00:09:59,740 --> 00:10:05,380
get these main chants, he said, think of all the qualities of the Buddha, and so if you

57
00:10:05,380 --> 00:10:10,540
ever hear Buddhist chanting in the teravada tradition, in any country, they all recite

58
00:10:10,540 --> 00:10:20,660
these, it'd be so Bhagavada, around some, some Buddha, and so on, he said, or if you don't

59
00:10:20,660 --> 00:10:30,180
think about me, think about the Dhamma, I think about the Dhamma, think about the Sangha.

60
00:10:30,180 --> 00:10:37,260
So I mean, not to suggest that that's a solution to terrorism or it's a way for us to be

61
00:10:37,260 --> 00:10:47,020
free from fear of gross physical violence, but on the other hand, as Buddhists as Buddhist

62
00:10:47,020 --> 00:10:52,660
meditators, and even as people who are not Buddhists, but just as meditators, the idea

63
00:10:52,660 --> 00:11:04,380
of remembering, the idea of recollecting yourself and remembering the path that we're

64
00:11:04,380 --> 00:11:09,780
on, remembering those who have trod the path, remembering the ones who have taught the

65
00:11:09,780 --> 00:11:20,540
path, remembering their greatness, their nobility, their freedom from fear gives us a good

66
00:11:20,540 --> 00:11:28,100
grounding and reminds us that fear is not useful, doesn't help us, it makes us a victim.

67
00:11:28,100 --> 00:11:38,020
And if we can free ourselves from our reactions, then really terrorism loses a lot of

68
00:11:38,020 --> 00:11:44,700
its strength, a lot of its power, so certainly it is part of the solution, I think.

69
00:11:44,700 --> 00:11:51,700
But this concept, I mean, this is a key concept in Buddhism, the idea that reactions are

70
00:11:51,700 --> 00:12:01,620
the problem, it also applies to terrorists, those who create terror, they create terror

71
00:12:01,620 --> 00:12:12,540
because we have goals, we have ambitions, evil, goals, evil ambitions, or else evil means,

72
00:12:12,540 --> 00:12:20,100
evil intentions, evil minds, evil minds states, we have these inside, you know, when a bully

73
00:12:20,100 --> 00:12:25,620
picks on someone weaker than them, when older siblings frighten their younger siblings,

74
00:12:25,620 --> 00:12:31,500
when parents scare their children, yell at them or shout at them, raise their fists

75
00:12:31,500 --> 00:12:40,140
or even hit them, all of this is terrorism, I mean, part of it is terrorism, sometimes

76
00:12:40,140 --> 00:12:53,780
it's just the desire to inflict pain, sometimes it's the desire to frighten.

77
00:12:53,780 --> 00:13:01,580
All of this is reaction as well, it's based on reaction, it's based on an inability

78
00:13:01,580 --> 00:13:11,660
to be at peace, so I mean, really the solution, if one can call it that because there's

79
00:13:11,660 --> 00:13:21,780
no question that it's not likely to be successful not anytime soon, but the work that

80
00:13:21,780 --> 00:13:33,940
we do to fix and to solve these problems is to teach, so a lot of terrorism is based

81
00:13:33,940 --> 00:13:45,820
on antagonism, the enmity towards between groups, religious groups is what we're seeing

82
00:13:45,820 --> 00:14:00,020
now, we see Islam, Islam is not Islam, but people in Muslims are very angry and not just

83
00:14:00,020 --> 00:14:11,460
Muslim, some people from these countries, Muslim countries in general, very angry, angry at

84
00:14:11,460 --> 00:14:18,820
Christians, angry at Americans, there've been religious wars going back centuries, millennia

85
00:14:18,820 --> 00:14:30,660
maybe, angry against Jews, and then you have the other way, Americans are, and many

86
00:14:30,660 --> 00:14:40,260
Europeans, Canadians have anger and antagonism towards Muslims, towards people who come

87
00:14:40,260 --> 00:14:47,260
from Muslim countries, regardless of whether they're Muslim or not, so we have racism,

88
00:14:47,260 --> 00:14:56,180
we have whatever it is to be prejudiced against another person's religion, this is all

89
00:14:56,180 --> 00:15:06,660
reactions, right, we have going back generations, this bad blood, where we can't stop

90
00:15:06,660 --> 00:15:15,340
a cycle, people from these religious or cultural ethnic backgrounds are fighting with

91
00:15:15,340 --> 00:15:25,380
each other, white against black, against brown, against red, yellow, Muslim, against

92
00:15:25,380 --> 00:15:43,420
Christian, against Jew, Buddhist, against Hindu, we haven't learned to just be, we haven't

93
00:15:43,420 --> 00:15:49,660
learned objectivity, we haven't learned to experience life without reacting, without

94
00:15:49,660 --> 00:16:08,460
building up these prejudices and these cruel intentions, for the real solution, it bears

95
00:16:08,460 --> 00:16:17,340
repeating that suffering comes from our reactions, not from our experiences, we can learn

96
00:16:17,340 --> 00:16:23,420
to just experience things as they are, we let go of them, we wouldn't cling, we'd fly

97
00:16:23,420 --> 00:16:33,500
away, we've all of our suffering behind, and then whatever happened, we have two other

98
00:16:33,500 --> 00:16:47,060
stories, the first one is about these monks who went off into the forest to practice

99
00:16:47,060 --> 00:17:00,060
meditation, and the angels up in the trees had to come down, they were Buddhist, I guess,

100
00:17:00,060 --> 00:17:04,340
they were, they were probably not Buddhist, but they would have been respectful towards

101
00:17:04,340 --> 00:17:08,860
recklessness, and when the monks went into the forest, the angels, oh we have to come down

102
00:17:08,860 --> 00:17:14,740
from the trees, they were tree angels or sprites or whatever, and they had to leave their

103
00:17:14,740 --> 00:17:19,620
homes up in the trees, because out of respect for the monks, out of respect for these

104
00:17:19,620 --> 00:17:25,780
recklessness, I guess, some sort of, you know, maybe it was because Indra had instituted

105
00:17:25,780 --> 00:17:32,860
from the high heavens, because he was Buddhist, maybe he had said, well you have to,

106
00:17:32,860 --> 00:17:37,700
the monks go into the forest, it's a law in the angel world, maybe I don't know, but

107
00:17:37,700 --> 00:17:42,940
they came down and it kind of irked them and they weren't really happy about it, so they

108
00:17:42,940 --> 00:17:51,060
would have a, what can we do to get these monks to leave our, our, our area, and so

109
00:17:51,060 --> 00:17:58,060
all day and night they cultivated fear, they gave, they sent these visions to the monks

110
00:17:58,060 --> 00:18:06,420
of headless bodies and bodyless heads, and gruesome ghosts and apparitions of all sorts

111
00:18:06,420 --> 00:18:12,020
and sounds and so on, and the monks were unable to be, unable to focus, they were totally

112
00:18:12,020 --> 00:18:16,660
out of their minds, freaking out and they said, we can't stay here, and so they went

113
00:18:16,660 --> 00:18:22,700
back to the Buddha, and the Buddha said to them, oh well, first time you went to the forest

114
00:18:22,700 --> 00:18:30,460
you didn't have a, a weapon, a weapon to fight this terror, this terrorism, this is

115
00:18:30,460 --> 00:18:38,820
one, a good example of Buddhist terrorism, smile, they know there was no, there was some sort

116
00:18:38,820 --> 00:18:46,140
of significant, you can imagine one of those being in the horror film, that this, these

117
00:18:46,140 --> 00:18:49,860
terrible visions, they didn't know what was going on, they thought these were actually

118
00:18:49,860 --> 00:18:58,900
demons maybe able to get them, and Buddha said we need a weapon to fight this, and he

119
00:18:58,900 --> 00:19:05,460
taught them what we, we call the God or Nehemite does it, God or Nehemite does it, they said

120
00:19:05,460 --> 00:19:12,820
go back, and as you walk into the forest, chant this, and so they chanted the loving

121
00:19:12,820 --> 00:19:19,420
kindness, the sutta and loving kindness, God or Nehemite, takusaleana yatang, yantang santang

122
00:19:19,420 --> 00:19:41,060
be samedia, saco juju jusury Baretha, soutjoo jusury

123
00:19:41,060 --> 00:19:42,060
I don't know.

124
00:19:42,060 --> 00:19:44,260
It's a base that I want to soak it at time.

125
00:19:44,260 --> 00:19:47,060
We all beings will be happy in their mind.

126
00:20:00,660 --> 00:20:06,460
Which speaks of one sort of conventional way of dealing

127
00:20:06,460 --> 00:20:15,460
with antagonism, dealing with enmity.

128
00:20:15,460 --> 00:20:21,460
A good way to change reactions is to apply the opposite

129
00:20:21,460 --> 00:20:25,460
when confronted by hate, reply with love.

130
00:20:25,460 --> 00:20:26,460
It's conventional.

131
00:20:26,460 --> 00:20:32,060
It's not a deep Buddhist teaching, but it is a Buddhist teaching.

132
00:20:32,060 --> 00:20:36,060
That when you supplant something with its opposite,

133
00:20:36,060 --> 00:20:39,060
you are able to change the course of events.

134
00:20:39,060 --> 00:20:40,060
It takes work.

135
00:20:40,060 --> 00:20:41,060
It takes effort.

136
00:20:41,060 --> 00:20:45,060
It's not something that's sustainable over the long term.

137
00:20:45,060 --> 00:20:49,060
It takes effort to constantly have a loving attitude

138
00:20:49,060 --> 00:20:55,060
when people are throwing hate at you.

139
00:20:55,060 --> 00:20:56,060
But it worked.

140
00:20:56,060 --> 00:21:00,060
These monks, what they did is they taught.

141
00:21:00,060 --> 00:21:01,060
It was a teaching.

142
00:21:01,060 --> 00:21:04,060
It wasn't just sending love to these angels.

143
00:21:04,060 --> 00:21:11,060
They were chanting it, and it was a reminder.

144
00:21:11,060 --> 00:21:17,060
A reminder of the suffering that comes from these bad intentions.

145
00:21:17,060 --> 00:21:29,060
The intent to cause fear is the best way we can defeat our enemies.

146
00:21:29,060 --> 00:21:32,060
It's to give them what is most precious.

147
00:21:32,060 --> 00:21:42,060
If you give them knowledge and wisdom, give them truth.

148
00:21:42,060 --> 00:21:48,060
The other example, it's a very small sort of insignificant example,

149
00:21:48,060 --> 00:21:56,060
but it speaks to the larger picture of the context of these acts.

150
00:21:56,060 --> 00:21:59,060
I think there's more than one actually.

151
00:21:59,060 --> 00:22:02,060
There's examples of Mara.

152
00:22:02,060 --> 00:22:05,060
The monks would be sitting in meditation,

153
00:22:05,060 --> 00:22:14,060
and suddenly an ox would come along and walk up near where the bowls,

154
00:22:14,060 --> 00:22:19,060
their ceramic bowls were stacked.

155
00:22:19,060 --> 00:22:21,060
The monks would freak out.

156
00:22:21,060 --> 00:22:22,060
We have to get up.

157
00:22:22,060 --> 00:22:23,060
There's big ox coming.

158
00:22:23,060 --> 00:22:25,060
It's going to break all our bowls.

159
00:22:25,060 --> 00:22:28,060
They all got agitated, and the Buddha said to them,

160
00:22:28,060 --> 00:22:29,060
that's not an ox.

161
00:22:29,060 --> 00:22:30,060
That's Mara.

162
00:22:30,060 --> 00:22:33,060
Apparently this happens several times in different ways.

163
00:22:33,060 --> 00:22:38,060
Mara would do anything he couldn't to cause fear.

164
00:22:38,060 --> 00:22:43,060
I only bring that insignificant example up.

165
00:22:43,060 --> 00:22:46,060
Seems kind of a strange thing to happen.

166
00:22:46,060 --> 00:22:56,060
To sort of think about what aspect of Samsara we're talking about here.

167
00:22:56,060 --> 00:23:02,060
Fear is Mara's domain.

168
00:23:02,060 --> 00:23:04,060
Fear is part of this part of Samsara.

169
00:23:04,060 --> 00:23:12,060
Those who delight in chaos, those who delight in suffering.

170
00:23:12,060 --> 00:23:19,060
There are angels, human beings who are bent on this.

171
00:23:19,060 --> 00:23:28,060
Just to be clear that we don't want to be one of those people.

172
00:23:28,060 --> 00:23:32,060
We don't want to be involved with that part of Samsara.

173
00:23:32,060 --> 00:23:35,060
It's a part of Samsara that will probably always be around.

174
00:23:35,060 --> 00:23:39,060
Probably not something that's ever going to disappear completely.

175
00:23:39,060 --> 00:23:47,060
It might have been swell.

176
00:23:47,060 --> 00:23:52,060
But the universe is a big place.

177
00:23:52,060 --> 00:23:57,060
So really our goal is to, well, the Buddha said,

178
00:23:57,060 --> 00:23:59,060
keep your minds calm.

179
00:23:59,060 --> 00:24:02,060
Keep your minds set.

180
00:24:02,060 --> 00:24:05,060
Keep your mind subjective.

181
00:24:05,060 --> 00:24:12,060
So bring it up as well because it's really an instruction to meditate.

182
00:24:12,060 --> 00:24:17,060
A reminder to us that many things will come to disturb our state of mind,

183
00:24:17,060 --> 00:24:21,060
both in meditation and out.

184
00:24:21,060 --> 00:24:23,060
And they're the problem.

185
00:24:23,060 --> 00:24:24,060
It's Mara.

186
00:24:24,060 --> 00:24:25,060
I think it's Mara.

187
00:24:25,060 --> 00:24:26,060
It's Satan.

188
00:24:26,060 --> 00:24:29,060
It's that part of Samsara that wants to pull us back in.

189
00:24:29,060 --> 00:24:31,060
It doesn't want to see us free.

190
00:24:31,060 --> 00:24:34,060
It doesn't want to see us happy.

191
00:24:34,060 --> 00:24:41,060
It doesn't want to bring it to allow us peace.

192
00:24:41,060 --> 00:24:48,060
So our practice is to free ourselves from the hooks,

193
00:24:48,060 --> 00:24:53,060
from the grasp, free ourselves from the vulnerability

194
00:24:53,060 --> 00:25:01,060
to these forces, forces of Mara and to help others

195
00:25:01,060 --> 00:25:05,060
in the best way we can overcome terrorists.

196
00:25:05,060 --> 00:25:08,060
It's by freeing them from the need,

197
00:25:08,060 --> 00:25:13,060
helping them be free from this need to torture others

198
00:25:13,060 --> 00:25:17,060
and thereby harm themselves and corrupt their own mind.

199
00:25:17,060 --> 00:25:23,060
Sentence themselves to great suffering.

200
00:25:23,060 --> 00:25:24,060
There you go.

201
00:25:24,060 --> 00:25:29,060
Just some thoughts on fear, terrorism.

202
00:25:29,060 --> 00:25:33,060
Think of somewhat apropos.

203
00:25:33,060 --> 00:25:35,060
There's the demo for tonight.

204
00:25:35,060 --> 00:25:43,060
Thank you all for tuning in.

205
00:25:43,060 --> 00:26:07,060
May you only free from fear.

206
00:26:07,060 --> 00:26:16,060
Thank you.

207
00:26:16,060 --> 00:26:43,060
Thank you.

208
00:26:43,060 --> 00:27:02,060
So are there any questions?

209
00:27:02,060 --> 00:27:29,060
Thank you.

210
00:27:29,060 --> 00:27:56,060
Thank you.

211
00:27:56,060 --> 00:28:23,060
Thank you.

212
00:28:23,060 --> 00:28:32,060
Does this a question?

213
00:28:32,060 --> 00:28:35,060
Are you all just spamming me here?

214
00:28:35,060 --> 00:28:37,060
What's going on?

215
00:28:37,060 --> 00:28:46,060
Here's a question.

216
00:28:46,060 --> 00:28:59,060
It's interesting that you will never gain insight into the nature of reality

217
00:28:59,060 --> 00:29:03,060
unless you're either on retreat or keeping the seventh precept.

218
00:29:03,060 --> 00:29:06,060
You don't even have to keep the five precepts.

219
00:29:06,060 --> 00:29:09,060
You don't have to keep any precepts to gain insight.

220
00:29:09,060 --> 00:29:14,060
Precepts are a fence post so you don't do anything.

221
00:29:14,060 --> 00:29:23,060
Just because poof I'm keeping the precepts doesn't mean I'm wise or going to become enlightened.

222
00:29:23,060 --> 00:29:28,060
The eight full noble path is right action, right speech, right livelihood.

223
00:29:28,060 --> 00:29:30,060
That's the morality.

224
00:29:30,060 --> 00:29:33,060
And those are all mind states.

225
00:29:33,060 --> 00:29:36,060
Nothing actually to do with action or speech.

226
00:29:36,060 --> 00:29:50,060
It's the intention to, or it's the aspect of the path that makes you disinclined to kill or steal or so.

227
00:29:50,060 --> 00:30:03,060
Anyone who says whatever this person is saying is wrong, most definitely wrong.

228
00:30:03,060 --> 00:30:07,060
You think of Mimi Sara becoming a Sotapana when he met the Buddha.

229
00:30:07,060 --> 00:30:10,060
This was the king decked out in all his finery.

230
00:30:10,060 --> 00:30:14,060
He came to see the Buddha for a Santatee.

231
00:30:14,060 --> 00:30:20,060
Santatee was this guy who was drunk and he meant to see the Buddha.

232
00:30:20,060 --> 00:30:22,060
I don't know if he was still drunk.

233
00:30:22,060 --> 00:30:25,060
He probably sobered up a little bit.

234
00:30:25,060 --> 00:30:28,060
He had been watching this woman dance all week.

235
00:30:28,060 --> 00:30:31,060
He'd been watching dancing all week and then he becomes an haran.

236
00:30:31,060 --> 00:30:36,060
I don't know, these are stories, you believe them or not, but there's certainly no sense

237
00:30:36,060 --> 00:30:47,060
that we need to keep all the precepts.

238
00:30:47,060 --> 00:30:52,060
Can you give a teaching on the cognitive and emotional obscuration in regards to all

239
00:30:52,060 --> 00:31:02,060
know of both phenomena and know of both persons?

240
00:31:02,060 --> 00:31:10,060
That makes my brain hurt.

241
00:31:10,060 --> 00:31:16,060
I have no idea what that person is saying.

242
00:31:16,060 --> 00:31:22,060
What is it that's stopping us from knowing things, cognitive and emotional obscuration and

243
00:31:22,060 --> 00:31:25,060
defilements that are stopping us from knowing everything?

244
00:31:25,060 --> 00:31:30,060
I don't know what you want me to talk about.

245
00:31:30,060 --> 00:31:34,060
There's no question that entertainment is a problem.

246
00:31:34,060 --> 00:31:40,060
It's quite clear, but it suggests that you can't gain insight just because of it.

247
00:31:40,060 --> 00:31:47,060
It just makes it more difficult, less likely, more problematic.

248
00:31:47,060 --> 00:31:49,060
It's much more about your state of mind.

249
00:31:49,060 --> 00:31:54,060
Someone can watch entertainment mindfully and become enlightened while watching dancing

250
00:31:54,060 --> 00:31:58,060
and listening to singing and so on.

251
00:31:58,060 --> 00:32:24,060
It's not about what you're doing, it's about your state of mind.

252
00:32:24,060 --> 00:32:28,060
It's the mind that the moment of death is so important, which should one do if they know

253
00:32:28,060 --> 00:32:30,060
they're about to be killed or die suddenly?

254
00:32:30,060 --> 00:32:35,060
I'll be very mindful, of course.

255
00:32:35,060 --> 00:32:38,060
That will be the best.

256
00:32:38,060 --> 00:32:41,060
Of course, by that time it's a little late.

257
00:32:41,060 --> 00:32:43,060
Excuse me, could you hold off?

258
00:32:43,060 --> 00:32:47,060
I have to go and train myself and mindfulness first.

259
00:32:47,060 --> 00:32:52,060
I guess it's true that the moment of death is very important, but the question is what's

260
00:32:52,060 --> 00:32:54,060
going to condition it?

261
00:32:54,060 --> 00:33:07,060
Well, in our whole lives everything we've done to make us who we are is going to condition that moment.

262
00:33:07,060 --> 00:33:12,060
Besides that, of course, there are other things you can do less effective and have

263
00:33:12,060 --> 00:33:17,060
calm your mind with some kind of mindfulness that the Buddha would be useful.

264
00:33:17,060 --> 00:33:22,060
May the meditation, of course, be useful.

265
00:33:47,060 --> 00:33:54,060
Thank you.

266
00:34:17,060 --> 00:34:33,060
Would you say that the 4D kamatanas are special as opposed to focusing on worldly objects,

267
00:34:33,060 --> 00:34:38,060
like music and entertainment, as they rarely give rise to the fountains?

268
00:34:38,060 --> 00:34:44,060
There's nothing that special about the 4D, but most of the 4D, some of them are quite special,

269
00:34:44,060 --> 00:34:47,060
but take the kasinas.

270
00:34:47,060 --> 00:34:53,060
There's no real reason why you couldn't pick a different color than are listed there.

271
00:34:53,060 --> 00:34:59,060
Obviously they've been chosen and selected for reasons, some of them are very special.

272
00:34:59,060 --> 00:35:06,060
Mindfulness of the Buddha, the Dhamma of Sangha, the 4 Brahma Vihara, the Dattus,

273
00:35:06,060 --> 00:35:08,060
the Arupa kamatanas are unique.

274
00:35:08,060 --> 00:35:16,060
I mean, they're fairly unique, a lot of them, but some of the meditation is infinite.

275
00:35:16,060 --> 00:35:19,060
There's so many, any number of objects.

276
00:35:19,060 --> 00:35:21,060
Music would make a bad one, of course.

277
00:35:21,060 --> 00:35:24,060
Entertainment would make a bad one.

278
00:35:24,060 --> 00:35:27,060
None of those could really bring samaatam.

279
00:35:27,060 --> 00:35:32,060
The 4D kamatanas bring samaatanas to be a single and changing object.

280
00:35:32,060 --> 00:35:40,060
It has to be stable, satisfying, controllable, music isn't that.

281
00:35:40,060 --> 00:35:46,060
Unless, you know, maybe trans music or, you know, electronic stuff or drum beats.

282
00:35:46,060 --> 00:35:58,060
They'd be helpful.

283
00:35:58,060 --> 00:36:03,060
They're really boring music, something like that.

284
00:36:03,060 --> 00:36:05,060
But sound can certainly be an object.

285
00:36:05,060 --> 00:36:29,060
You can enter into a Jana based on sound.

286
00:36:29,060 --> 00:36:58,060
Okay, well thank you all for coming out.

287
00:36:58,060 --> 00:37:02,060
I wish you have a good night.

288
00:37:02,060 --> 00:37:18,060
See you all next time.

