1
00:00:00,000 --> 00:00:11,000
From time to time, I feel insecure about my physical appearance and feel hopeless by all the materialism, corruption, anger I see around me.

2
00:00:11,000 --> 00:00:20,000
This affects my meditation practice and study. I feel I go steps back and fall back into old ways. What am I doing wrong?

3
00:00:20,000 --> 00:00:35,000
Well, I read this question and one thing did occur to me. First thing to say is that it's quite a broad question.

4
00:00:35,000 --> 00:00:45,000
You do have some points there, but in general, this is the kind of question where the best answer that you can give theoretically is practice meditation.

5
00:00:45,000 --> 00:00:55,000
Again, and again, we have these questions about I have a problem in my life. What should I do?

6
00:00:55,000 --> 00:01:02,000
And the answer is the practice of the continuation of practice of meditation.

7
00:01:02,000 --> 00:01:25,000
But the idea that these insecurities and the hopelessness and so on are affecting your meditation is sort of a common misunderstanding about the practice of meditation.

8
00:01:25,000 --> 00:01:42,000
I'm assuming that you're practicing correctly and you have good knowledge about the background of meditation, but it's quite often that we'll miss these important factors of these important mind states.

9
00:01:42,000 --> 00:01:57,000
And rather than meditate on them, we'll see them as a problem. So you have this idea that it's affecting the bad things are affecting your meditation practice. When, in fact, the point is to focus on them as your meditation practice.

10
00:01:57,000 --> 00:02:05,000
When you feel insecure, then insecurity becomes your meditation. When you feel hopeless and hopelessness becomes your meditation.

11
00:02:05,000 --> 00:02:15,000
In a sense, I'm going to assume that you're aware of this and that you feel that there's something greater than this, that is the problem.

12
00:02:15,000 --> 00:02:25,000
And so what struck me about the phrasing of this question is the idea of self and the idea of entity.

13
00:02:25,000 --> 00:02:41,000
And so I think something useful that you might be useful for you is an understanding of the order in which we do away with the negative states in our mind because we don't get rid of anger.

14
00:02:41,000 --> 00:02:45,000
We don't get rid of greed.

15
00:02:45,000 --> 00:03:02,000
We don't get rid of our insecurities and our hopelessness right away. These aren't the first to go. The first thing to go is the idea of the existence of an entity, of any sort of entity.

16
00:03:02,000 --> 00:03:23,000
The first thing to realize is that there is no eye in all of this. The idea that your meditation is going backwards, the idea that you're falling back into your old ways, the idea that you're doing something wrong or that something is wrong like there is a problem.

17
00:03:23,000 --> 00:03:29,000
All has to do with the idea of self or the idea of an entity.

18
00:03:29,000 --> 00:03:45,000
And so it may very well be that you're not doing anything wrong, but what is wrong is this idea that when you put all of these mind states together, you come up with itself, you come up with the idea that there's a problem.

19
00:03:45,000 --> 00:04:14,000
So the first thing that you have to work on is being able to separate or being able to acknowledge the different parts of the experience, one by one by one and see them for what they are, rather than thinking about my meditation practice, my study.

20
00:04:14,000 --> 00:04:22,000
My old ways, a problem that I have, something that I'm doing wrong, my physical appearance, obviously.

21
00:04:22,000 --> 00:04:39,000
The key one there is the answer to feeling insecure about yourself and feeling bad about yourself, that somehow I am a bad meditator or I am having problems in my meditator.

22
00:04:39,000 --> 00:04:45,000
What we often get is people saying they're stuck in their meditation, people who practice for years feel like they're not progressing.

23
00:04:45,000 --> 00:04:53,000
And it can generally be, or it can often be because they still have this attachment to an eye to itself.

24
00:04:53,000 --> 00:05:04,000
So they're thinking in terms of themselves as a being and they get this idea that somehow they are staying the same or they are stuck.

25
00:05:04,000 --> 00:05:14,000
Because it's like a hole and you've got this hole and that's the way out, but the self is too big to fit in the hole.

26
00:05:14,000 --> 00:05:16,000
This is the problem.

27
00:05:16,000 --> 00:05:24,000
So what we're trying to do is fit ourselves in the exit or through the door, through the way out.

28
00:05:24,000 --> 00:05:27,000
It's kind of a good metaphor to think of.

29
00:05:27,000 --> 00:05:32,000
You have to throw away the self, the eye can't become enlightened.

30
00:05:32,000 --> 00:05:41,000
You have to give up this whole idea that I am trying to progress somewhere or I am trying to become something or I have this quality.

31
00:05:41,000 --> 00:05:44,000
I have that quality.

32
00:05:44,000 --> 00:05:56,000
Give up this whole idea of progress and begin to look at what's right here and now, whether it's a unwholesome state of mind or a wholesome state of mind.

33
00:05:56,000 --> 00:06:05,000
Whether it's a state of good concentration, a state of poor concentration, whether it's a state of worldliness or a state of spirituality or so on.

34
00:06:05,000 --> 00:06:13,000
And get rid of this idea that I am falling back, that I am regressing or so on.

35
00:06:13,000 --> 00:06:19,000
Or that we have, if you look up, there was a man here recently who's been coming on this chat room and says I'm an alcoholic.

36
00:06:19,000 --> 00:06:31,000
I mean, even that is a good example of how we begin to identify with mind states and we identify with individual phenomena.

37
00:06:31,000 --> 00:06:38,000
You are way forward and all of our way forward is to give all that up and look at what's here right here and right now.

38
00:06:38,000 --> 00:06:45,000
Give up the whole idea of I'm progressing, I'm getting better or I'm not progressing, I'm getting worse and so on.

39
00:06:45,000 --> 00:06:51,000
I mean, that's not obviously the key is the meditation practice meditating on those things that you're talking about.

40
00:06:51,000 --> 00:07:09,000
But I think that that's an important point that I wanted to bring up, the whole idea of the over, over, over arch, over arching the self which is over over top of all of your problems.

41
00:07:09,000 --> 00:07:16,000
And when you give that up, you'll see that your problems are not really as big as they seem to me, so.

42
00:07:16,000 --> 00:07:33,000
I just want to add to that moment that you can give up yourself that you have the deep inside and understand that there is not a self within the body or the mind.

43
00:07:33,000 --> 00:07:53,000
It is very normal to feel like stepping back or falling back into all ways. That happens to everyone until the inside knowledge really is established in the mind, really is understood.

44
00:07:53,000 --> 00:08:05,000
And until that it will be like that from time to time and then you will progress and then fall back again, so that's very normal.

45
00:08:05,000 --> 00:08:22,000
And there was one thing that along the same lines that I wanted to say is that it's often not falling back and meditators will repeatedly become discouraged when they think they've overcome a problem and see it pop up again.

46
00:08:22,000 --> 00:08:36,000
And quite often it's not falling back at all, it's the echoes or the cycles that are still very much a part of our old habits.

47
00:08:36,000 --> 00:09:00,000
And until these habits work their way out and until you, as Paul and Yanny said, until you have that realization that allows you to break the habits, they're going to keep coming back again and again and part of what I was trying to say is the practice is not necessarily in the beginning to get rid of the habits, but to see the habits is not self.

48
00:09:00,000 --> 00:09:08,000
To see this slipping back, it's not used slipping back. It's perfectly normal because it's the habit of the mind.

49
00:09:08,000 --> 00:09:20,000
The beginning basic practice is to see that all of these things are impermanent. You can't push them away and say they're never going to come back.

50
00:09:20,000 --> 00:09:30,000
Not sure what they might come back in the next moment. They're suffering. You can't form them into something that is going to satisfy you.

51
00:09:30,000 --> 00:09:45,000
And they're uncontrollable that they're not really you. These negative emotions that are coming up are not really, they really have nothing to do with you.

52
00:09:45,000 --> 00:09:52,000
An external thing that, or an external there, an impersonal thing that arises again and again.

53
00:09:52,000 --> 00:10:02,000
Part of the practice is seeing that and so it's kind of a shift from getting upset at yourself for bad things to seeing that those bad things are not really yours or not really you.

54
00:10:02,000 --> 00:10:07,000
And that comes from seeing moment to moment to moment looking at them.

55
00:10:07,000 --> 00:10:17,000
You want to jump in here?

