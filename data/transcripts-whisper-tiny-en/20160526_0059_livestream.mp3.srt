1
00:00:00,000 --> 00:00:19,360
Good evening everyone, my guest in live, May 25th.

2
00:00:19,360 --> 00:00:29,280
Today's quote is somewhat suspect.

3
00:00:29,280 --> 00:00:39,440
Looks like he's loosely translated something problematic.

4
00:00:39,440 --> 00:00:42,680
This is the word love here.

5
00:00:42,680 --> 00:00:52,320
As far as I can see, the only instance of this quote doesn't use the word love.

6
00:00:52,320 --> 00:01:02,240
This is the word patisan tara, which, some reason I think we've already had as a quote.

7
00:01:02,240 --> 00:01:11,080
I think at some point we had this list of six, maybe I'm wrong.

8
00:01:11,080 --> 00:01:21,000
Pretty sure though, we got a lot, we talked about got a lot, got a lot, right?

9
00:01:21,000 --> 00:01:27,520
This time it has the backstory of an angel coming to see the Buddha, telling him about

10
00:01:27,520 --> 00:01:34,760
these six things, and the Buddha affirming that it's a deep truth.

11
00:01:34,760 --> 00:01:49,160
Gauravata, six kinds of respect, Gauravata comes from the root, the root would be Gaur I guess,

12
00:01:49,160 --> 00:02:05,560
it's where the word guru comes from Gauravata means heavy, or someone who is important, a teacher,

13
00:02:05,560 --> 00:02:14,960
and Gauravata is a teacher, a guru, but Gauravata also means heavy, so it means taking

14
00:02:14,960 --> 00:02:21,960
something serious, and Gauravata means taking something seriously, and there are six things

15
00:02:21,960 --> 00:02:30,480
that according to this angel and then according to the Buddha are important for not falling

16
00:02:30,480 --> 00:02:43,360
away, for not declining, we want to progress in this life, we want to succeed, we're

17
00:02:43,360 --> 00:02:51,800
talking specifically about spirituality, about Buddhism, and the Buddha's teaching, what

18
00:02:51,800 --> 00:02:57,440
is it that leads to success, what is it that leads to progress, and there are these

19
00:02:57,440 --> 00:03:03,200
six things, but number six I don't think is as translated, so let's go through them,

20
00:03:03,200 --> 00:03:14,760
but to Gauravata, such to means the teacher, so the Buddha, having respect for and taking

21
00:03:14,760 --> 00:03:21,360
the Buddha seriously, so people traditionally will take the Buddha so seriously that they

22
00:03:21,360 --> 00:03:26,920
make images of the Buddha and they bow down to them and they give offerings, that's

23
00:03:26,920 --> 00:03:37,000
so seriously they take the Buddha, some might argue a little too seriously, but the most

24
00:03:37,000 --> 00:03:45,160
important is appreciating the Buddha and respecting his enlightenment, respecting his state,

25
00:03:45,160 --> 00:03:54,760
his perfection, his greatness, it's important because it keeps you in line with his

26
00:03:54,760 --> 00:04:02,680
teachings, it's easy for the mind to get lost, for the mind to lose its way, and if you

27
00:04:02,680 --> 00:04:09,920
don't take the Buddha seriously, your mind is very easily diverted and caught up by other

28
00:04:09,920 --> 00:04:16,560
things, number two of course Dhamma Gauravata, number three Sangha Gauravata, so the Buddha,

29
00:04:16,560 --> 00:04:22,120
the Dhamma and the Sangha, we should take them seriously, the Dhamma of the Buddha refers

30
00:04:22,120 --> 00:04:31,760
to his teachings, should take them seriously, should not look lightly upon the meditation

31
00:04:31,760 --> 00:04:44,000
practice and the teachings, we should appreciate some people will be so appreciative that

32
00:04:44,000 --> 00:04:57,320
they don't put books about the Buddha's teaching on the floor, keep them up high, treat

33
00:04:57,320 --> 00:05:05,480
the books with respect, and so some might argue that that's not really necessary, but

34
00:05:05,480 --> 00:05:11,120
it's all about the sense of respect, so treating the Buddha in the Dhamma in this way is

35
00:05:11,120 --> 00:05:17,280
useful for cultivating wholesome mind state, it's respectful mind state, mind states that

36
00:05:17,280 --> 00:05:32,320
are conducive to practice, conducive to progress, we, the most important is that we respect

37
00:05:32,320 --> 00:05:38,440
the teaching, we appreciate how important it is, sometimes studying the Buddha's teaching

38
00:05:38,440 --> 00:05:43,280
is good because it, not just for the analogy gives you, but because it gives you a deeper

39
00:05:43,280 --> 00:05:50,280
spec, so that you don't have to doubt what little you know, if you know a lot more, it

40
00:05:50,280 --> 00:05:55,680
can help to solidify your practice and make it clear the bigger picture, why are we doing

41
00:05:55,680 --> 00:06:04,520
this, why is it important to let go, why is it wrong to claim, sometimes studying is

42
00:06:04,520 --> 00:06:11,520
useful, but we could also talk about appreciation of meditation practice, taking that seriously,

43
00:06:11,520 --> 00:06:19,720
here I think it just means the teachings of doctrine, number three, the sangha, so appreciating

44
00:06:19,720 --> 00:06:24,080
teachers, appreciating enlightened beings, even if they don't teach, just appreciating

45
00:06:24,080 --> 00:06:31,200
their enlightenment, appreciating, looking up to someone who you'd like to emulate,

46
00:06:31,200 --> 00:06:37,760
when you take them seriously, it becomes easy to emulate them, to follow their, their

47
00:06:37,760 --> 00:06:48,440
lead, to follow the way that they've gone, following their footsteps as well.

48
00:06:48,440 --> 00:06:56,200
Number four, sika garawata, the training, taking the training seriously, training in morality,

49
00:06:56,200 --> 00:07:04,800
taking the precepts seriously, training in concentration, so taking meditation seriously,

50
00:07:04,800 --> 00:07:11,840
and training in wisdom, so taking wisdom seriously, the practice of insight meditation.

51
00:07:11,840 --> 00:07:20,120
So then it shouldn't be a hobby, there shouldn't be something that, something that changes

52
00:07:20,120 --> 00:07:25,560
your life, it should be a part of your life, everyday meditating, everyday being mindful

53
00:07:25,560 --> 00:07:30,080
or whatever you do, when you eat, eat mindfully, when you brush your teeth, brush

54
00:07:30,080 --> 00:07:36,400
when you eat mindfully, when you use the toilet, use the toilet and mindfully, when

55
00:07:36,400 --> 00:07:45,120
you lie down to sleep, lie down to sleep, mindfully, take it seriously.

56
00:07:45,120 --> 00:07:54,600
Number five, apamada garawata, so this is heedfulness, apamada is a trademark teaching

57
00:07:54,600 --> 00:08:03,920
of the Buddha, it's very core teaching, it means being mindful, being vigilant, being constantly

58
00:08:03,920 --> 00:08:12,880
aware, consistent, continuously, if you don't have continuity in your practice, it's like

59
00:08:12,880 --> 00:08:18,240
two steps forward, one step back, it slows you down, it can even take you backwards if

60
00:08:18,240 --> 00:08:26,520
you're not consistent in it, you should try to be vigilant, heedful, on the side of

61
00:08:26,520 --> 00:08:36,520
which, right, what's good, what's positive, don't get lost and negative.

62
00:08:36,520 --> 00:08:44,440
And number six, patisantara, patisantara, this problematic word, which doesn't mean love,

63
00:08:44,440 --> 00:08:51,920
patisant, patisant, patisantara, specifically, some means together, they don't really have

64
00:08:51,920 --> 00:09:03,120
meaning, but tara, stir comes from, it's literally spreading before, the patisantara is

65
00:09:03,120 --> 00:09:11,680
understood to refer to hospitality, there's two kinds of patisantara, amisantara and

66
00:09:11,680 --> 00:09:17,000
tama, patisantara, so they don't have anything to do with love, not directly, a good

67
00:09:17,000 --> 00:09:23,960
will, perhaps, friendlyness, but hospitality is probably a bit.

68
00:09:23,960 --> 00:09:29,920
So when you put things out for strangers, one is when someone, so when people come to

69
00:09:29,920 --> 00:09:38,920
visit you're being hospitable, it's curious that the Buddha uses this, but it is, it

70
00:09:38,920 --> 00:09:47,240
is what keeps the religion alive, it keeps Buddhism alive, good will, so good will to

71
00:09:47,240 --> 00:09:59,320
others, sharing with others, receiving others, hospitable, hospitably, amisantara is with

72
00:09:59,320 --> 00:10:04,800
gifts, with physical things, with people that need food, giving them food, when people

73
00:10:04,800 --> 00:10:14,160
need shelter, giving them shelter, this kind of thing, dama, patisantara means offering

74
00:10:14,160 --> 00:10:19,920
the dama, offering teachings, sharing the teachings with others, even if to be a teacher,

75
00:10:19,920 --> 00:10:28,760
to share teachings, you can explain or demonstrate meditation practice, just as good will

76
00:10:28,760 --> 00:10:43,600
as a gift, as a help for others, so taking that seriously is, all of these are said to

77
00:10:43,600 --> 00:10:53,800
be important, and the Buddha says, abhabhu, pariha naya, nibhana, sayva, santiki, they're

78
00:10:53,800 --> 00:11:05,600
not capable of feeding away, they are close, they're in the presence of nibhana, nibhana,

79
00:11:05,600 --> 00:11:18,760
sayva, santiki, things that lead you to nibhana, so Buddhism sort of has a rep for

80
00:11:18,760 --> 00:11:26,000
being fairly dour and serious, and here's more about taking things seriously, because

81
00:11:26,000 --> 00:11:30,880
you take something seriously, doesn't mean you have to be dour or serious, means you have

82
00:11:30,880 --> 00:11:38,440
to be conscientious, I think is a very Buddhist word, conscientious, don't be lazy,

83
00:11:38,440 --> 00:11:52,560
don't be negligent, don't be flippant or laxadesical, be conscientious, mindful, these

84
00:11:52,560 --> 00:11:59,760
are good words, so again I think we've already had this quote somehow, it's very familiar,

85
00:11:59,760 --> 00:12:04,800
I mean I know this quote from my own studies, but it seems like we've gone through this

86
00:12:04,800 --> 00:12:16,440
recently, anyway, so that's the dhamma for this evening, we have questions, people have questions

87
00:12:16,440 --> 00:12:22,200
about our site, the site's going to undergo a big change, so that's not, don't worry too

88
00:12:22,200 --> 00:12:33,000
much about it yet, once we change over to the new format, we've got an IT team diligently

89
00:12:33,000 --> 00:12:57,480
working on the site, so hopefully that'll all come together this summer.

90
00:12:57,480 --> 00:13:11,280
No questions today, we don't have to have lots of questions every day, there will be questions,

91
00:13:11,280 --> 00:13:19,720
there will be more questions.

92
00:13:19,720 --> 00:13:26,120
When the Lord said, kustalasarupa sampada was here referring to all good deeds, or the good

93
00:13:26,120 --> 00:13:46,200
deeds associated with noble aidful power, I don't know the mind of the Buddha, I would

94
00:13:46,200 --> 00:13:53,400
venture that he meant all also deeds, because Buddha's do teach, that which is not directly

95
00:13:53,400 --> 00:13:58,160
related to the aidful noble path, not directly, of course you could argue that all

96
00:13:58,160 --> 00:14:04,680
good deeds are supportive of the aidful noble path, so I would say it's more than that

97
00:14:04,680 --> 00:14:11,920
case, Buddha's teach things that are supportive, but not directly a part of the aidful

98
00:14:11,920 --> 00:14:18,120
noble path, I mean charity is not in the aidful noble path, but it is supportive of the

99
00:14:18,120 --> 00:14:29,720
aidful noble path, and Buddha's do teach it, so on the other hand, the word upa sampada,

100
00:14:29,720 --> 00:14:39,160
you could argue means referring to the, it's not just saying, perform wholesome deeds and

101
00:14:39,160 --> 00:14:49,600
say, and get to the pinnacle, get to the highest, become complete, become full, and

102
00:14:49,600 --> 00:14:56,000
so there it's actually, it's actually specifically referring to the aidful noble path,

103
00:14:56,000 --> 00:15:08,640
I would say, which difficult questions are given, good, keep me on my toes, and you talk

104
00:15:08,640 --> 00:15:14,640
about restraint, how much and in what way should one restrain oneself, there's a good

105
00:15:14,640 --> 00:15:19,360
quote of the Buddha that I can't remember exactly where it is, Mahasi Sayyada talks about

106
00:15:19,360 --> 00:15:34,480
it, restraint, sanyata is restraint, or it's one word, sanyata, sanyama, yama means, sanyama means

107
00:15:34,480 --> 00:15:51,080
restraint, restraint, restraint, restraint, restraint of the feet, restraint of the body of

108
00:15:51,080 --> 00:16:02,720
the self, all kinds of restraint are good, so there are different kinds, he talks about

109
00:16:02,720 --> 00:16:10,000
the five things, the five sorts of restraint, restraint through morality, restraint

110
00:16:10,000 --> 00:16:16,960
through effort, restraint through knowledge, restraint through mindfulness and restraint

111
00:16:16,960 --> 00:16:22,280
through wisdom, I think those are the five, so there are different kinds of restraint,

112
00:16:22,280 --> 00:16:29,360
now most, the word restraint kind of implies forcing, but that's not really it, you think

113
00:16:29,360 --> 00:16:35,960
of these five, you restrain yourself in different ways, with morality, with just by, and

114
00:16:35,960 --> 00:16:42,640
that's kind of forcing, so that's kind of the corsus way, just saying no, no, no, I'm

115
00:16:42,640 --> 00:16:51,360
not going to do that, I'm not going to do that, but then, so with effort, maybe somehow

116
00:16:51,360 --> 00:16:57,880
related morality and effort, but effort can also refer to the effort in meditation, so

117
00:16:57,880 --> 00:17:04,880
if you work hard in like, summit to meditation, I think it's effort, I'm actually

118
00:17:04,880 --> 00:17:15,680
kept right, remember, media, media somewhere I think, with knowledge, no, it's not

119
00:17:15,680 --> 00:17:20,440
knowledge, nyana, sanwara, so it's not banyas, not the fifth one, there's five of them,

120
00:17:20,440 --> 00:17:30,360
I can't remember the most, should know them, with mindfulness is probably the most important,

121
00:17:30,360 --> 00:17:40,120
so not exactly forcing yourself, but straightening your mind, so when the mind is, when

122
00:17:40,120 --> 00:17:44,920
the mind experiences something, see it clearly, when you feel pain and you say to yourself

123
00:17:44,920 --> 00:17:52,280
pain, pain, you're restraining yourself in the sense of, you're diverting your attention,

124
00:17:52,280 --> 00:17:59,680
not to the reaction, so with the mind things of it, but by fixing on the actual experience

125
00:17:59,680 --> 00:18:05,200
itself, so that restraints in the sense that it keeps the mind from getting upset about

126
00:18:05,200 --> 00:18:19,200
the pain, or attached to things we enjoy the things as well, sub-be, sub-be, sub-be, sub-be,

127
00:18:19,200 --> 00:18:28,760
sub-be, sub-be, sub-be, sub-be, sub-be, sub-be, sub-be is for the first line, I don't

128
00:18:28,760 --> 00:18:39,560
want to remember, sub-be, sub-be, sub-be, sub-be, sub-be, sub-be, sub-be, sub-be, sub-be, sub-be,

129
00:18:39,560 --> 00:18:55,240
sub-be, sub-be, sub-be, sub-be, sub-be, sub-be, sub-be, sub-be, sub-be, sub-be, sub-be, sub-be, sub-be type, sub-be,

130
00:18:55,240 --> 00:19:06,720
It's a mabi kabe baita punya naan and don't be afraid of punya, it's just kusula.

131
00:19:06,720 --> 00:19:12,480
During walking meditation, sometimes we hear some joints making popping sounds out of nowhere.

132
00:19:12,480 --> 00:19:14,200
Why does that hearing arise?

133
00:19:14,200 --> 00:19:22,960
I don't have any take on that, that's the body, you have to talk to someone who knows

134
00:19:22,960 --> 00:19:28,080
stuff about bones, I believe it's gas releasing in the body.

135
00:19:28,080 --> 00:19:32,320
You're thinking philosophically, why does that, what is the karma that leads to that?

136
00:19:32,320 --> 00:19:37,320
It's very complicated, it's the karma being a human, we've put together this very

137
00:19:37,320 --> 00:19:46,040
complex being that has lots of complex things happen to it, including our joints popping.

138
00:19:46,040 --> 00:19:50,560
It's just part of being human.

139
00:19:50,560 --> 00:19:54,240
Perhaps we're going to have to close that window when we record, not it's okay the next

140
00:19:54,240 --> 00:19:55,240
time.

141
00:19:55,240 --> 00:20:02,000
It's unfortunate because it's so hot up here.

142
00:20:02,000 --> 00:20:11,760
Why does the mind go to the ear, or the mind's not focused, the mind is the nature of the

143
00:20:11,760 --> 00:20:13,560
mind.

144
00:20:13,560 --> 00:20:20,320
When there's an experience, it is drawn to the experience.

145
00:20:20,320 --> 00:20:28,760
Technically in the biobidama it's all caused in effect, so there's result in seeing hearing

146
00:20:28,760 --> 00:20:41,840
smelling, tasting, feeling, thinking they're the results of karma, and they're related

147
00:20:41,840 --> 00:20:46,840
to things that happen in the past.

148
00:20:46,840 --> 00:20:57,600
What is it, we're not so concerned about why it's happening, we're concerned about what

149
00:20:57,600 --> 00:21:04,120
is happening in the nature of what is happening, because it's not who cares why if it is

150
00:21:04,120 --> 00:21:11,560
happening, that's the issue, and of course the popping isn't the problem, and the issue

151
00:21:11,560 --> 00:21:15,440
is how you react to it, that's what's really important, it's important that we learn

152
00:21:15,440 --> 00:21:19,760
how we react to these things, does it frustrate you, do you feel curious, do you wonder

153
00:21:19,760 --> 00:21:24,120
why you're the popping, well that wondering, that's what we want to learn about, is that

154
00:21:24,120 --> 00:21:30,000
good, is that bad, is that problematic, is that the best way to use my mental attention,

155
00:21:30,000 --> 00:21:36,840
my mental power, that's what we're focused on, that's what's important, why isn't such

156
00:21:36,840 --> 00:21:59,120
a big deal, okay, no more questions, then good night everyone, see you next time.

