1
00:00:00,000 --> 00:00:19,240
Okay, good evening everyone, welcome back to our live broadcast, out of the time for

2
00:00:19,240 --> 00:00:29,360
all of us to act together as a community, to come together as a community, take the

3
00:00:29,360 --> 00:00:42,240
time to reflect on our practice, as my teachers used to say, or as you often hear monks

4
00:00:42,240 --> 00:00:57,400
in Thailand, keep Banchi, keep Banchi which is a colloquialism for looking at

5
00:00:57,400 --> 00:01:21,560
our bank account, or calculating our profit, looking at what we've gotten out of the practice,

6
00:01:21,560 --> 00:01:39,160
let's come up our practice, what we've gained from the practice, we come together to get

7
00:01:39,160 --> 00:01:50,640
direction on our practice, the direct us in the right direction to adjust our focus

8
00:01:50,640 --> 00:02:09,160
but we also come together to talk about what we've gained from the practice, subjective

9
00:02:09,160 --> 00:02:15,640
constant concern, right, what am I getting out of this, you have to look at the bottom

10
00:02:15,640 --> 00:02:34,640
line, you're putting a lot of effort, what good is it, what good does it do, the Buddhism

11
00:02:34,640 --> 00:02:43,240
is all about cause and effect results, actions and their results, come as actions we

12
00:02:43,240 --> 00:02:50,240
pack, means results, so we have results of many different things, first of all there's

13
00:02:50,240 --> 00:03:02,120
the results of our ordinary efforts to try and solve our problems, when something comes

14
00:03:02,120 --> 00:03:26,160
to mind that is undesirable, we work very hard to avoid it, to quench it, to be free from

15
00:03:26,160 --> 00:03:37,280
it, and something good comes to mind, the actions are to obtain it, to sustain it, to

16
00:03:37,280 --> 00:03:48,040
maintain it, to get what we want, to always get what we want, and we have strategies

17
00:03:48,040 --> 00:03:55,520
for this, we have just talking about yesterday we have ways of thinking positive for

18
00:03:55,520 --> 00:04:03,520
ways of setting up our lives in such a way that we are protected from the things that

19
00:04:03,520 --> 00:04:14,960
we don't like, and that we're able to protect the things that we do like, so that we can

20
00:04:14,960 --> 00:04:39,320
obtain and experience them frequently, repeatedly at our pleasure.

21
00:04:39,320 --> 00:04:43,080
And this is the worst thing to do of course, this is what we're starting to realize through

22
00:04:43,080 --> 00:04:55,240
the practice, this is the word disaster comes, if you spend all your time cultivating

23
00:04:55,240 --> 00:05:03,800
this attachment and aversion, these habits of avoiding the unpleasant and obtaining

24
00:05:03,800 --> 00:05:11,640
the pleasant, highly unstable, the sort of thing that can come crashing down on you

25
00:05:11,640 --> 00:05:27,400
and when your defenses are down and lead to great stress and suffering, when you don't

26
00:05:27,400 --> 00:05:36,720
get what you want, when you get what you don't want, and we're learning about these

27
00:05:36,720 --> 00:05:42,840
habits when we practice as you begin to meditate and you start to see these habits, these

28
00:05:42,840 --> 00:05:48,000
strategies, these practices and you realize it's not really the best way the results are

29
00:05:48,000 --> 00:06:10,360
not so good.

30
00:06:10,360 --> 00:06:16,520
Another strategy, of course, is to cultivate tranquility meditation, so we cultivate

31
00:06:16,520 --> 00:06:33,520
state and habits that replace our reactions, sort of avoid the problem entirely, right?

32
00:06:33,520 --> 00:06:39,840
So if you're angry at someone, immerse yourself in universal love and the problem never

33
00:06:39,840 --> 00:06:49,560
comes up, spend time sitting, cultivating love for all beings, love for those that you hate,

34
00:06:49,560 --> 00:06:54,560
it's a great method, works wonders.

35
00:06:54,560 --> 00:07:05,120
If you have lust, passion for the body, for bodily, carnal pleasure, cultivate mindfulness

36
00:07:05,120 --> 00:07:12,320
of the body, look at the different body parts and see the most unpleasant as disgusting

37
00:07:12,320 --> 00:07:23,840
is not worth clinging to and it suppresses the desire, replaces it with a sobering awareness

38
00:07:23,840 --> 00:07:29,960
of the true nature of the body is not being in any way desirable, worthy of clinging

39
00:07:29,960 --> 00:07:41,040
to and so on, of course, the more universal meditations like fixing your mind on a

40
00:07:41,040 --> 00:07:54,720
color, for example, or any sort of concept meditating on the Buddha, for example, and

41
00:07:54,720 --> 00:08:10,760
these are much better results, the results here are lasting stable and unadulterated, they're

42
00:08:10,760 --> 00:08:20,360
not tainted by defoundment, so when you stop practicing, you're not any worse off than

43
00:08:20,360 --> 00:08:28,440
you were before, in fact better because you now have this skill, this talent of entering

44
00:08:28,440 --> 00:08:34,440
into these states and so it's a great thing to do, people often wonder about this sort

45
00:08:34,440 --> 00:08:40,520
of meditation whether it's actually any good or any use, well this is the use, it provides

46
00:08:40,520 --> 00:08:55,440
you with a way out, provides you with sort of a freedom from suffering and yet it too

47
00:08:55,440 --> 00:09:03,480
is not permanent and so as great of a temporary solution as it might be, it's not by any means

48
00:09:03,480 --> 00:09:11,360
a permanent solution, it can't actually free you from suffering because when you, when

49
00:09:11,360 --> 00:09:22,880
and if you ever stop or lose, you lose the interest or the initiative or the impulsion

50
00:09:22,880 --> 00:09:31,480
to continue, you just wind up where you started, you begin to cultivate bad habits again

51
00:09:31,480 --> 00:09:41,480
or your bad habits crop up again, so even that's not the solution, good results but not

52
00:09:41,480 --> 00:09:53,600
the results we're looking for, so we go further, this is really why we bother to torture

53
00:09:53,600 --> 00:10:01,440
ourselves with insight meditation, as we see there's got to be a

54
00:10:01,440 --> 00:10:12,400
more permanent solution, more stable solution, something that can allow us to truly change

55
00:10:12,400 --> 00:10:19,040
and truly become free and so cultivate insight meditation now, insight meditation has this

56
00:10:19,040 --> 00:10:28,560
great power of universal applicability, it's not just when you're angry you have your

57
00:10:28,560 --> 00:10:39,760
cultivate love or passion or lustful you cultivate mindfulness of the body, insight meditation

58
00:10:39,760 --> 00:10:48,600
cultivate wisdom and wisdom gives you an understanding of all aspects of reality that frees

59
00:10:48,600 --> 00:11:02,880
you from clinging, it's an awareness, a presence, it's like a solution, a solvent that

60
00:11:02,880 --> 00:11:20,220
dissolves all of the stickiness in the mind, it's far more lasting and stable and requires

61
00:11:20,220 --> 00:11:29,100
so much less effort to upkeep because it's innate, it's pure, it's not based on effort

62
00:11:29,100 --> 00:11:34,960
which say, it's based on wisdom, meaning it's not something you have to force, it's not

63
00:11:34,960 --> 00:11:44,340
artificial, it's natural, that being said something that is not acknowledged nearly enough

64
00:11:44,340 --> 00:11:54,200
is that even insight does not permanent, even insight having been gained can be lost,

65
00:11:54,200 --> 00:12:04,560
a person on the path of insight, can gain profound insights into reality and think maybe

66
00:12:04,560 --> 00:12:14,000
they've really succeeded in the practice only to fall away when they give up the practice

67
00:12:14,000 --> 00:12:25,240
and lose everything they've gained.

68
00:12:25,240 --> 00:12:29,000
Makes one kind of thing, why go through all the effort, maybe I'll go back to summit

69
00:12:29,000 --> 00:12:33,640
meditation, if that's the case, I know this was going to be a one-time thing where I don't

70
00:12:33,640 --> 00:12:38,480
have to go through all the torture of actually dealing with my problems and figuring them

71
00:12:38,480 --> 00:12:47,240
out, I'm just going to get them all back again, but to be honest it probably lasts a lot

72
00:12:47,240 --> 00:12:53,640
longer and it's a lot more stable, it's the most stable of all of them all so far, that's

73
00:12:53,640 --> 00:13:00,520
not permanent, this life maybe you keep it, if you get really strong insight, there's

74
00:13:00,520 --> 00:13:05,520
certain things that you won't lose throughout your life, you can lose them in the next

75
00:13:05,520 --> 00:13:17,040
life or the next life or some life in the future, and I'm permanent, but not to despair,

76
00:13:17,040 --> 00:13:22,040
there is light at the end of the tunnel and in fact insight meditation leads to something

77
00:13:22,040 --> 00:13:30,920
else, it leads to freedom, it leads to nibana, it leads to the experience of what we call

78
00:13:30,920 --> 00:13:41,680
the deathless or the under reason perhaps leads to an experience that is outside of some

79
00:13:41,680 --> 00:13:50,920
sorrow, and so we have to distinguish between these two being on the path of insight, it's

80
00:13:50,920 --> 00:13:59,280
not something you accumulate and never lose, something you accumulate, and if you accumulate

81
00:13:59,280 --> 00:14:13,280
enough of it you can break through to the point where you reach another level of action

82
00:14:13,280 --> 00:14:26,280
with another level of results, so the results of insight are stable and lasting, but

83
00:14:26,280 --> 00:14:33,280
it's not permanent, but the results of nibana, the results of entering into the manganjana

84
00:14:33,280 --> 00:14:43,440
polynjana, of experiencing nibana, even just a moment, even for just three moments of time

85
00:14:43,440 --> 00:14:49,440
that's apparently how the shortest amount of time you can experience nibana or something

86
00:14:49,440 --> 00:14:57,200
or maybe two moments, but for the first time I think it's three, of course it can be longer,

87
00:14:57,200 --> 00:15:10,400
it can be hours or days, but even for just that moment this is and again it requires perhaps

88
00:15:10,400 --> 00:15:20,800
a little faith, but something you can verify for yourself that this state is the point

89
00:15:20,800 --> 00:15:28,400
of no return, you can't go back, person who has seen nibana, maybe for just a moment,

90
00:15:28,400 --> 00:15:41,400
it can be changed permanently, permanently they've set in motion the cessation of suffering,

91
00:15:41,400 --> 00:15:49,560
the escape from samsara, they have permanently altered and removed from them from their

92
00:15:49,560 --> 00:15:58,960
very being, sort of the lynch pinned to this whole mass of suffering, ignorance, they've

93
00:15:58,960 --> 00:16:11,560
come to see outside in samsara and this happiness, this peace, this great freedom is lasting,

94
00:16:11,560 --> 00:16:23,680
there's no return for this, so to be clear our daily practice of meditation is great,

95
00:16:23,680 --> 00:16:28,040
it's lasting, it's great quite beneficial and it is what eventually leans to true freedom

96
00:16:28,040 --> 00:16:37,680
but it's not true freedom, to truly be free and to have a perfect release, you need to truly

97
00:16:37,680 --> 00:16:43,360
understand samsara, you need to get to the point where you really get it, where you have

98
00:16:43,360 --> 00:16:50,960
the sapiphany that tells you that nothing is worth clinging to, that ceases where the mind drops

99
00:16:50,960 --> 00:17:10,200
out, unplugs, ceases to reach out, ceases to advert to samsara and there's an experience

100
00:17:10,200 --> 00:17:17,560
that is beyond samsara that is outside of it, and that moment on one is called a sotapana

101
00:17:17,560 --> 00:17:21,640
which means they have attained the stream, they have entered into the stream meaning they're

102
00:17:21,640 --> 00:17:32,800
on the current, leading them to complete freedom from suffering, so it doesn't mean to say

103
00:17:32,800 --> 00:17:42,880
that we should not all be praised and appreciated for all the good merit that we're gaining

104
00:17:42,880 --> 00:17:49,800
in our practices of all sorts, we practice the Buddhist teaching on all levels, but it's

105
00:17:49,800 --> 00:17:55,120
a reminder for us not to be complacent, that there is a goal, that this isn't just

106
00:17:55,120 --> 00:18:01,840
a hobby or something that you take up to do on a daily basis, it's something that you

107
00:18:01,840 --> 00:18:10,000
use to achieve true understanding and realization of the ultimate state and the ultimate

108
00:18:10,000 --> 00:18:20,280
freedom, so what we're aiming for, it has the greatest result, the greatest effect, it

109
00:18:20,280 --> 00:18:32,280
has a true benefit, true purpose, true meaning, so there you go, there's our dhamma

110
00:18:32,280 --> 00:18:41,360
for today, the results, getting results, thank you all for tuning in, for coming out,

111
00:18:41,360 --> 00:19:10,240
have a good day.

112
00:19:10,240 --> 00:19:37,220
Thank you.

113
00:19:40,240 --> 00:19:44,240
.

114
00:19:44,240 --> 00:19:48,240
.

115
00:19:48,240 --> 00:19:52,240
.

116
00:19:52,240 --> 00:19:58,240
.

117
00:19:58,240 --> 00:20:04,240
..

118
00:20:04,240 --> 00:20:08,240
..

119
00:20:08,240 --> 00:20:13,240
.

120
00:20:13,240 --> 00:20:18,240
.

121
00:20:18,240 --> 00:20:23,240
.

122
00:20:23,240 --> 00:20:26,240
.

123
00:20:26,240 --> 00:20:33,240
.

124
00:20:33,240 --> 00:20:35,240
.

125
00:20:35,240 --> 00:20:36,240
.

126
00:20:36,240 --> 00:20:46,240
.

127
00:20:46,240 --> 00:20:51,240
.

128
00:20:51,240 --> 00:20:52,240
.

129
00:20:52,240 --> 00:20:53,240
.

130
00:20:53,240 --> 00:20:54,240
.

131
00:20:54,240 --> 00:20:55,240
.

132
00:20:55,240 --> 00:20:56,240
.

133
00:20:56,240 --> 00:20:57,240
.

134
00:20:57,240 --> 00:20:58,240
.

135
00:20:58,240 --> 00:20:59,240
.

136
00:20:59,240 --> 00:21:00,240
.

137
00:21:00,240 --> 00:21:01,240
.

138
00:21:01,240 --> 00:21:02,240
.

139
00:21:02,240 --> 00:21:03,240
.

140
00:21:03,240 --> 00:21:04,240
.

141
00:21:04,240 --> 00:21:05,240
.

142
00:21:05,240 --> 00:21:08,240
.

143
00:21:08,240 --> 00:21:11,240
.

144
00:21:11,240 --> 00:21:12,240
.

145
00:21:12,240 --> 00:21:13,240
.

146
00:21:13,240 --> 00:21:16,240
.

147
00:21:16,240 --> 00:21:19,240
.

148
00:21:19,240 --> 00:21:20,240
.

149
00:21:20,240 --> 00:21:23,240
.

150
00:21:23,240 --> 00:21:27,240
.

151
00:21:27,240 --> 00:21:29,240
.

152
00:21:29,240 --> 00:21:30,240
.

153
00:21:30,240 --> 00:21:31,240
.

154
00:21:31,240 --> 00:21:32,240
.

155
00:21:32,240 --> 00:21:33,240
.

156
00:21:33,240 --> 00:21:34,240
.

157
00:21:34,240 --> 00:21:34,240
–

158
00:21:34,240 --> 00:21:39,240
Have you read my booklet on how to meditate?

159
00:21:39,240 --> 00:21:46,240
I think that might help.

160
00:22:09,240 --> 00:22:34,240
I don't know, there's no real, I don't have any real comment on how we see this to attain one of the stages or another.

161
00:22:34,240 --> 00:22:39,240
I don't, I can't think of any text to back that up.

162
00:22:39,240 --> 00:22:43,240
I don't know why someone would say that.

163
00:22:43,240 --> 00:22:47,240
It really depends on the individual.

164
00:22:47,240 --> 00:22:53,240
Some people have sought up on it as all they're going to get for seven lifetimes, right?

165
00:22:53,240 --> 00:23:01,240
Once one reaches Sakadakami, it's much quicker.

166
00:23:01,240 --> 00:23:06,240
It's only one lifetime.

167
00:23:06,240 --> 00:23:14,240
I think probably what they're thinking of is how an anagami can stay an anagami for millions or billions of years,

168
00:23:14,240 --> 00:23:19,240
or whatever up in the Sudawasa Brahma realms.

169
00:23:19,240 --> 00:23:29,240
An anagami has special realms that they go through that can take a huge amount of time for them to become an aran.

170
00:23:29,240 --> 00:23:32,240
The middle way is not balanced.

171
00:23:32,240 --> 00:23:34,240
This is a talk I could talk about.

172
00:23:34,240 --> 00:23:41,240
Maybe I'll talk about it tomorrow if you like, go over in the middle way.

173
00:23:41,240 --> 00:23:47,240
Might be good to save it for that, then I have something to talk about tomorrow.

174
00:23:47,240 --> 00:23:49,240
I've talked about this before.

175
00:23:49,240 --> 00:23:58,240
I'm sure I've got videos that are talking about it in the ways that I'll probably talk about it again.

176
00:23:58,240 --> 00:24:01,240
But let's save it.

177
00:24:01,240 --> 00:24:25,240
I'll talk about the middle way.

178
00:24:25,240 --> 00:24:32,240
Okay, I'm going to go for now and see you all next time. Have a good night.

