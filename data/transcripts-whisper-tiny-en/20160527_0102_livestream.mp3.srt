1
00:00:00,000 --> 00:00:18,240
Good evening everyone, we're broadcasting live, how's the sound is that too loud?

2
00:00:18,240 --> 00:00:29,200
Someone said it was too quiet last night, so I'm trying something louder, is it distorted?

3
00:00:29,200 --> 00:00:53,200
I can't tell, anyway, so tonight's quote is sort of the good one, it's about treatment.

4
00:00:53,200 --> 00:01:15,000
Everything is worth it, getting sick.

5
00:01:15,000 --> 00:01:26,460
with a sick person who knows their sick, right?

6
00:01:26,460 --> 00:01:33,500
We just... we just... we just... we just...

7
00:01:33,500 --> 00:01:43,840
And we just... when there is a tikitaka, which is a physician,

8
00:01:43,840 --> 00:01:49,340
someone who treats sickness from the verb tikititit.

9
00:01:49,340 --> 00:01:56,440
Just as with a sick person, when a physician or a doctor is there,

10
00:01:56,440 --> 00:02:01,920
nakit nakit kitja paititang bhyadhi,

11
00:02:01,920 --> 00:02:04,840
but he doesn't have the...

12
00:02:04,840 --> 00:02:07,640
he doesn't have the doctor cure him.

13
00:02:07,640 --> 00:02:12,180
Nada also so tikitit kaitit,

14
00:02:12,180 --> 00:02:18,680
and it's not the fault of that doctor.

15
00:02:18,680 --> 00:02:28,680
A1, even so, kile sambiadhi, he tukitopari paito.

16
00:02:28,680 --> 00:02:33,580
For one, for those who are sick with the defilements,

17
00:02:33,580 --> 00:02:38,280
with greed, with anger, with delusion,

18
00:02:38,280 --> 00:02:43,080
who suffer, who are feverish.

19
00:02:43,080 --> 00:02:48,680
Naga aetitang achariyam, who don't seek out a teacher.

20
00:02:48,680 --> 00:02:54,780
Nado so, nado so so vina iyaka,

21
00:02:54,780 --> 00:03:00,580
and it's not the fault of the one who would lead them out, vina iyaka,

22
00:03:00,580 --> 00:03:06,880
the one who would free them.

23
00:03:06,880 --> 00:03:14,180
This is from the jataka.

24
00:03:14,180 --> 00:03:16,180
I'm not going to go through the jataka.

25
00:03:16,180 --> 00:03:21,180
In fact, it's part of a larger quote.

26
00:03:21,180 --> 00:03:26,980
But an important, it's important here is the...

27
00:03:26,980 --> 00:03:29,580
this is a good quote.

28
00:03:29,580 --> 00:03:37,380
We sometimes put too much of an emphasis on a teacher.

29
00:03:37,380 --> 00:03:43,080
Someone came recently to see me and said he was looking for a good teacher.

30
00:03:43,080 --> 00:03:45,980
And I said, well that's not the most important thing actually.

31
00:03:45,980 --> 00:03:49,880
Most important is a good teaching.

32
00:03:49,880 --> 00:03:53,780
Can you say it's important to have a good teacher? Yes, it's important.

33
00:03:53,780 --> 00:03:56,880
But it's much more important to have a good teaching.

34
00:03:56,880 --> 00:03:59,380
The real focus should be on the teaching.

35
00:03:59,380 --> 00:04:03,480
And ultimately, because if you have a good teacher teaching you the wrong thing,

36
00:04:03,480 --> 00:04:05,780
it's even more dangerous.

37
00:04:05,780 --> 00:04:08,480
Because they're so good at teaching something wrong

38
00:04:08,480 --> 00:04:14,380
that you go the wrong way even quicker.

39
00:04:14,380 --> 00:04:17,880
And in the end, especially in this teaching,

40
00:04:17,880 --> 00:04:20,380
a teacher can only go so far.

41
00:04:20,380 --> 00:04:25,880
I get questions. People ask them questions, send me questions.

42
00:04:25,880 --> 00:04:29,080
Some questions you just can't answer.

43
00:04:29,080 --> 00:04:33,080
Many questions. Many questions people are asking,

44
00:04:33,080 --> 00:04:35,880
you know, questions how to solve their problems, questions.

45
00:04:35,880 --> 00:04:39,480
What is... someone was saying, what is it like to perceive Nibana?

46
00:04:39,480 --> 00:04:40,280
Or what is it like?

47
00:04:40,280 --> 00:04:44,980
What is it like to... what is the perception of a soda pun?

48
00:04:44,980 --> 00:04:46,980
How can you answer that?

49
00:04:46,980 --> 00:04:50,080
You could. You can answer to some extent that I get...

50
00:04:50,080 --> 00:04:55,780
I look at these questions and think, no, this isn't to come from a teacher.

51
00:04:55,780 --> 00:05:00,280
These questions have to come... the answers have to come from practice.

52
00:05:00,280 --> 00:05:08,280
The fact that we're asking questions is sometimes a sign that we're not really practicing.

53
00:05:08,280 --> 00:05:13,780
What this is leading to...

54
00:05:13,780 --> 00:05:15,680
is that the teacher only shows the way...

55
00:05:15,680 --> 00:05:20,780
a kata road at Hagata, even at the target of the Buddha

56
00:05:20,780 --> 00:05:25,580
is just one who shows the way, who points the way.

57
00:05:25,580 --> 00:05:30,080
He gets up to you.

58
00:05:30,080 --> 00:05:38,580
The work must be done by you.

59
00:05:38,580 --> 00:05:42,580
So we have to see this in meditation.

60
00:05:42,580 --> 00:05:44,580
It's not going to be fun. It's not going to be easy.

61
00:05:44,580 --> 00:05:48,580
The teacher doesn't have any answers or tricks or means.

62
00:05:48,580 --> 00:05:51,580
The best teacher can do is encourage you.

63
00:05:51,580 --> 00:05:54,580
That's good. It's good to get encouragement.

64
00:05:54,580 --> 00:05:57,580
And the end, even that is a crunch.

65
00:05:57,580 --> 00:06:00,580
Even that is something you have to grow.

66
00:06:00,580 --> 00:06:02,580
You're completely alone in this practice.

67
00:06:02,580 --> 00:06:04,580
It'll make you cry.

68
00:06:04,580 --> 00:06:09,580
You know, it's good if it's bringing you to tears with a frustration

69
00:06:09,580 --> 00:06:17,580
and just being overwhelmed by the enormity of the problem that you have in your mind.

70
00:06:17,580 --> 00:06:20,580
Blood sweating tears.

71
00:06:20,580 --> 00:06:25,580
It's not even a difficult thing while you're doing is walking and sitting.

72
00:06:25,580 --> 00:06:28,580
It's not like that should be difficult.

73
00:06:28,580 --> 00:06:32,580
But that's the point. It's all about your sickness.

74
00:06:32,580 --> 00:06:36,580
If you're sick, walking and sitting is painful.

75
00:06:36,580 --> 00:06:39,580
When you're sick with defamance, equally so.

76
00:06:39,580 --> 00:06:42,580
You're sick with greed, anger, and delusion.

77
00:06:42,580 --> 00:06:45,580
Just sitting still is problematic.

78
00:06:45,580 --> 00:06:47,580
It's difficult.

79
00:06:47,580 --> 00:07:00,580
It's unpleasant.

80
00:07:00,580 --> 00:07:03,580
So the doctor is there.

81
00:07:03,580 --> 00:07:05,580
The doctor is telling you what to do.

82
00:07:05,580 --> 00:07:09,580
If you don't do it, if you don't practice,

83
00:07:09,580 --> 00:07:12,580
if you don't cure your sickness,

84
00:07:12,580 --> 00:07:17,580
you're not going to do so so to teach a game.

85
00:07:17,580 --> 00:07:22,580
It's not the fault of that doctor.

86
00:07:22,580 --> 00:07:24,580
That's not actually what this quote says.

87
00:07:24,580 --> 00:07:27,580
It says if you don't seek out the teacher.

88
00:07:27,580 --> 00:07:30,580
So it is actually putting emphasis on the teacher.

89
00:07:30,580 --> 00:07:36,580
I kind of misinterpreted it.

90
00:07:36,580 --> 00:07:40,580
So there's even a bigger fault if you didn't seek out a teacher.

91
00:07:40,580 --> 00:07:42,580
Absolutely.

92
00:07:42,580 --> 00:07:46,580
If the teacher is present and if you don't listen to them,

93
00:07:46,580 --> 00:07:48,580
that's more important.

94
00:07:48,580 --> 00:07:51,580
You don't follow their teachings.

95
00:07:51,580 --> 00:07:53,580
But right.

96
00:07:53,580 --> 00:07:56,580
So actually what this quote says is for those people who don't

97
00:07:56,580 --> 00:07:59,580
ever think to come in meditation.

98
00:07:59,580 --> 00:08:03,580
People in the world who are suffering from stress and anxiety

99
00:08:03,580 --> 00:08:06,580
and where do they turn for a solution?

100
00:08:06,580 --> 00:08:11,580
They turn to pills, they turn to drugs, alcohol,

101
00:08:11,580 --> 00:08:15,580
they turn to entertainment, they turn to diversion,

102
00:08:15,580 --> 00:08:17,580
they turn to work or something,

103
00:08:17,580 --> 00:08:22,580
anything to avoid having to deal with the problem.

104
00:08:22,580 --> 00:08:26,580
They hear about meditation, they hear about the Buddha,

105
00:08:26,580 --> 00:08:30,580
they hear about Buddhist teachers and so on.

106
00:08:30,580 --> 00:08:34,580
They don't understand the use.

107
00:08:34,580 --> 00:08:39,580
I can't sometimes don't even know that they're sick.

108
00:08:44,580 --> 00:08:47,580
I think there's more to this than that.

109
00:08:47,580 --> 00:08:51,580
In the context of a monastery, it makes good sense.

110
00:08:51,580 --> 00:08:55,580
I've got monks in the monastery who never go to see the teacher.

111
00:08:55,580 --> 00:08:57,580
I never go to ask for advice.

112
00:08:57,580 --> 00:09:02,580
I never go to undertake a meditation course.

113
00:09:02,580 --> 00:09:11,580
But I think the deeper meaning or the deeper point has to be made is

114
00:09:11,580 --> 00:09:13,580
it's not enough to go and see the teacher.

115
00:09:13,580 --> 00:09:21,580
It's not enough to listen to the Buddha's teaching.

116
00:09:21,580 --> 00:09:24,580
If you don't put it into practice.

117
00:09:24,580 --> 00:09:28,580
The corollary there is that it's going to be tough.

118
00:09:28,580 --> 00:09:30,580
It's not something someone else can do for you.

119
00:09:30,580 --> 00:09:32,580
The teacher can't help you much.

120
00:09:32,580 --> 00:09:34,580
It's not much that can help you.

121
00:09:34,580 --> 00:09:40,580
It's not much that you can use as a trick or not to make it easier.

122
00:09:40,580 --> 00:09:44,580
In fact, just trying to make it easier is a problem.

123
00:09:44,580 --> 00:09:53,580
Trying to make it more pleasant, more stable, more agreeable.

124
00:09:53,580 --> 00:09:55,580
It just makes it harder in the end.

125
00:09:55,580 --> 00:09:58,580
It makes it less effective.

126
00:09:58,580 --> 00:10:02,580
What we're looking for is something called Anulomika Kantim.

127
00:10:02,580 --> 00:10:07,580
Kantim means patience.

128
00:10:07,580 --> 00:10:11,580
For parents and patients really.

129
00:10:11,580 --> 00:10:15,580
Anulomika means loma is the grain.

130
00:10:15,580 --> 00:10:18,580
Anulomika means with the grain.

131
00:10:18,580 --> 00:10:23,580
So, patience that accords with...

132
00:10:23,580 --> 00:10:28,580
Well, not even just with reality, but also with the path.

133
00:10:28,580 --> 00:10:32,580
What it means is it's in line with...

134
00:10:32,580 --> 00:10:34,580
I'm not in line with the truth.

135
00:10:34,580 --> 00:10:39,580
I guess we'll be the best way to put it.

136
00:10:39,580 --> 00:10:43,580
Meaning when you experience something, you have patience.

137
00:10:43,580 --> 00:10:52,580
Your patience brings you in line with the truth.

138
00:10:52,580 --> 00:10:57,580
Because without that patience, you have judgment rather than seeing things as they are.

139
00:10:57,580 --> 00:10:59,580
You think only of what they should be.

140
00:10:59,580 --> 00:11:02,580
What you'd like them to be.

141
00:11:02,580 --> 00:11:05,580
The change, you wish they were.

142
00:11:05,580 --> 00:11:08,580
You've been an enact.

143
00:11:08,580 --> 00:11:12,580
How you'd rather they were.

144
00:11:12,580 --> 00:11:15,580
So, what you want, what you don't want.

145
00:11:15,580 --> 00:11:20,580
Your urges, your desires, your needs, that kind of thing.

146
00:11:20,580 --> 00:11:23,580
Rather than just seeing things as they are.

147
00:11:23,580 --> 00:11:26,580
So, we need patience.

148
00:11:26,580 --> 00:11:29,580
That's the biggest one.

149
00:11:29,580 --> 00:11:32,580
And it comes with practice.

150
00:11:32,580 --> 00:11:35,580
It comes with taking the regimen.

151
00:11:35,580 --> 00:11:40,580
And that is given by the teacher.

152
00:11:40,580 --> 00:11:41,580
Right?

153
00:11:41,580 --> 00:11:42,580
So, that's my take on the quote.

154
00:11:42,580 --> 00:11:44,580
And actually, it's not quite what the quote says.

155
00:11:44,580 --> 00:11:49,580
It's from the Jataka, so it's one of these past life stories.

156
00:11:49,580 --> 00:11:51,580
And there's definitely that aspect.

157
00:11:51,580 --> 00:11:54,580
You know, how many people don't even think to come and do a meditation course.

158
00:11:54,580 --> 00:12:01,580
We would never think, here we've put up these online meditation courses.

159
00:12:01,580 --> 00:12:05,580
And they're mostly empty these days.

160
00:12:05,580 --> 00:12:08,580
And I don't know how well they're received.

161
00:12:08,580 --> 00:12:10,580
I'm not giving that much on them.

162
00:12:10,580 --> 00:12:12,580
I just give you the next exercise.

163
00:12:12,580 --> 00:12:14,580
But it puts the onus on the meditator.

164
00:12:14,580 --> 00:12:15,580
You have to do the work.

165
00:12:15,580 --> 00:12:17,580
I'm not going to give you much.

166
00:12:17,580 --> 00:12:20,580
I'll give you as the next exercise.

167
00:12:20,580 --> 00:12:23,580
Either you do it or you don't.

168
00:12:23,580 --> 00:12:27,580
But we don't see a lot of people in these days even coming to meditate.

169
00:12:27,580 --> 00:12:37,580
Another tip they're speaking is it's not so it's hugely popular as one would hope.

170
00:12:37,580 --> 00:12:41,580
We're very good at going to the doctor for our physical ailments.

171
00:12:41,580 --> 00:12:45,580
But we're not so concerned about our mental ailments.

172
00:12:45,580 --> 00:12:51,580
So we go to a physical doctor for them to fix our brains.

173
00:12:51,580 --> 00:12:54,580
Thinking that the brain is the problem.

174
00:12:54,580 --> 00:12:55,580
When is the brain?

175
00:12:55,580 --> 00:12:59,580
The brain is just a bunch of fat and cells.

176
00:12:59,580 --> 00:13:03,580
There's nothing in the brain.

177
00:13:03,580 --> 00:13:08,580
It's just a mindless organ.

178
00:13:08,580 --> 00:13:15,580
If we dump more chemicals in it, it just mixes up.

179
00:13:15,580 --> 00:13:20,580
It mixes up if the chemicals.

180
00:13:20,580 --> 00:13:26,580
It's not the answer.

181
00:13:26,580 --> 00:13:35,580
So, few are those who, few are those who are even stirred by things that are stirring.

182
00:13:35,580 --> 00:13:38,580
Who are moved by things that are moving.

183
00:13:38,580 --> 00:13:44,580
Who are upset by things that they should be agitated by.

184
00:13:44,580 --> 00:13:52,580
But even fewer are those people who once they've been moved to action actually do act.

185
00:13:52,580 --> 00:14:01,580
They actually do go and find a teacher and listen carefully and appreciate and retain the teaching.

186
00:14:01,580 --> 00:14:06,580
And then actually put it into practice.

187
00:14:06,580 --> 00:14:09,580
That's where that's really curious.

188
00:14:09,580 --> 00:14:10,580
It's quite simple.

189
00:14:10,580 --> 00:14:12,580
So anyway, that's the quote.

190
00:14:12,580 --> 00:14:15,580
Some things to think about.

191
00:14:15,580 --> 00:14:18,580
Don't neglect going to see teachers.

192
00:14:18,580 --> 00:14:27,580
But my addition is don't place too much emphasis on the teacher.

193
00:14:27,580 --> 00:14:28,580
Do the work.

194
00:14:28,580 --> 00:14:31,580
That's all there is to it.

195
00:14:31,580 --> 00:14:38,580
A lot of people just like to listen to talks and read books.

196
00:14:38,580 --> 00:14:43,580
It's very hollow, very empty in the end.

197
00:14:43,580 --> 00:14:49,580
It doesn't fill you up with wisdom.

198
00:14:49,580 --> 00:14:54,580
Okay, so what do we have questions?

199
00:14:54,580 --> 00:14:59,580
Is there a difference between karma and superstition making assumptions?

200
00:14:59,580 --> 00:15:05,580
It seems a lot of time people can use the law of universe karma past actions to do evils

201
00:15:05,580 --> 00:15:10,580
and make excuses for their own actions.

202
00:15:10,580 --> 00:15:15,580
You made me do this or you used to be like this, so that's your fault.

203
00:15:15,580 --> 00:15:19,580
I'm doing the best to make your life a living health.

204
00:15:19,580 --> 00:15:24,580
Yeah, well there's lots of misunderstandings about karma.

205
00:15:24,580 --> 00:15:25,580
People have all sorts of ideas.

206
00:15:25,580 --> 00:15:28,580
But you're getting onto something which is important.

207
00:15:28,580 --> 00:15:38,580
Is there anyone who thinks they know cause and effect on the nature of karma is deluding themselves, unless they're a Buddha?

208
00:15:38,580 --> 00:15:40,580
It's very complicated.

209
00:15:40,580 --> 00:15:44,580
So no one can say this is the reason why you're like this.

210
00:15:44,580 --> 00:15:51,580
We get a general sense and that general sense holds up, but it's not sure.

211
00:15:51,580 --> 00:15:54,580
We can still only guess and estimate.

212
00:15:54,580 --> 00:16:00,580
Like if someone is born sick, well there's often the reason, a mental reason for that.

213
00:16:00,580 --> 00:16:08,580
They were the mind that came into the womb was messed up and so it got messed up.

214
00:16:08,580 --> 00:16:16,580
It messed up the fetus and not only that, but it was drawn to a potentially problematic situation.

215
00:16:16,580 --> 00:16:19,580
It all came together wrong, which is karma.

216
00:16:19,580 --> 00:16:25,580
So we have this general idea that this is the way things go.

217
00:16:25,580 --> 00:16:33,580
But to actually understand how it works, it's not even necessary to understand how the intricacies of it go.

218
00:16:33,580 --> 00:16:36,580
What is the cause of this? What is the cause of that?

219
00:16:36,580 --> 00:16:48,580
What's important about karma is to see how your mind states affect the next moment, affect your mind, change your mind.

220
00:16:48,580 --> 00:16:51,580
When you get greedy, what is that like when you're angry?

221
00:16:51,580 --> 00:16:53,580
What's the result of these things?

222
00:16:53,580 --> 00:16:55,580
When you're mindful, what's the result of that?

223
00:16:55,580 --> 00:17:01,580
And to understand that they change you are good and bad deeds, change us.

224
00:17:01,580 --> 00:17:10,580
They're habitual, they make their mark, they leave their mark on us.

225
00:17:10,580 --> 00:17:15,580
So that's karma is our important.

226
00:17:15,580 --> 00:17:21,580
Like the general sort of folk, understanding of karma is useful to understand.

227
00:17:21,580 --> 00:17:30,580
Evil leads to evil, good leads to good, but it's very general and vague.

228
00:17:30,580 --> 00:17:34,580
As meditators, it's important, it's this momentary understanding.

229
00:17:34,580 --> 00:17:50,580
When you watch your body and mind, then you see how they interact and how they change based on your action.

230
00:17:50,580 --> 00:17:57,580
During sitting meditation, they might occur a loss of any definitively clear thoughts.

231
00:17:57,580 --> 00:18:08,580
There's awareness of tactile sensations and various ambient sounds that a chaotic stream of extremely brief images.

232
00:18:08,580 --> 00:18:21,580
It becomes necessary to arbitrarily return to the clear thoughts of the rising form.

233
00:18:21,580 --> 00:18:31,580
They can be physical, they can be mental.

234
00:18:31,580 --> 00:18:36,580
You're not always going to be able to be mindful clearly.

235
00:18:36,580 --> 00:18:46,580
For our purposes, the only thing we have to concern ourselves with is once we realize that it's happened.

236
00:18:46,580 --> 00:18:57,580
You realize that when you realize, wow, my mind is kind of cloudy.

237
00:18:57,580 --> 00:19:12,580
It's to say to yourself, feeling, feeling, or knowing, cloudy, cloudy, even unclear, so I'm confused.

238
00:19:12,580 --> 00:19:16,580
And that creates clarity as you do that.

239
00:19:16,580 --> 00:19:22,580
So don't just return to the rising and falling in the abdomen, try and be aware of your mind state.

240
00:19:22,580 --> 00:19:29,580
In doing that, you'll create clarity based on it.

241
00:19:29,580 --> 00:19:30,580
But what's causing them?

242
00:19:30,580 --> 00:19:36,580
I mean, those kind of questions, again, it's like trying to understand karma.

243
00:19:36,580 --> 00:19:42,580
Or it's just trying to, it is what it is basically.

244
00:19:42,580 --> 00:19:46,580
And from a Buddhist point of view, we're not concerned about some kind of explanation of what it is.

245
00:19:46,580 --> 00:19:47,580
It's an experience.

246
00:19:47,580 --> 00:19:48,580
It's impermanent.

247
00:19:48,580 --> 00:19:49,580
It's unsatisfying.

248
00:19:49,580 --> 00:19:50,580
It's uncontrollable.

249
00:19:50,580 --> 00:19:55,580
That's all we need to know about it.

250
00:19:55,580 --> 00:20:10,580
And so by cultivating this remembrance, reminding yourself it is, you'll get that clarity of mind.

251
00:20:10,580 --> 00:20:14,580
Any thoughts on explaining memory since things are impermanent?

252
00:20:14,580 --> 00:20:18,580
Where and how does the memory of them remain?

253
00:20:18,580 --> 00:20:25,580
Nothing remains. Everything arises and sees memory as like an echo.

254
00:20:25,580 --> 00:20:26,580
That's it.

255
00:20:26,580 --> 00:20:32,580
I mean, it's a very complicated echo.

256
00:20:32,580 --> 00:20:38,580
It's like the ripples, the changes that something makes.

257
00:20:38,580 --> 00:20:44,580
And when you apply the mind to that, there's the imprints in the universe, really.

258
00:20:44,580 --> 00:20:49,580
I mean, everything is, it's all one thing.

259
00:20:49,580 --> 00:20:51,580
The universe is one thing.

260
00:20:51,580 --> 00:20:56,580
So when something changes, when something occurs, it changes everything else.

261
00:20:56,580 --> 00:21:01,580
And you can see the echo or the imprint of it.

262
00:21:01,580 --> 00:21:04,580
Much of this goes on in the brain.

263
00:21:04,580 --> 00:21:06,580
The brain is imprinted by experience.

264
00:21:06,580 --> 00:21:08,580
The brain doesn't store memories.

265
00:21:08,580 --> 00:21:11,580
The brain is just cells.

266
00:21:11,580 --> 00:21:15,580
It's proteins and fats and blood and that's what the brain is.

267
00:21:15,580 --> 00:21:24,580
But it's able to store changes, you know, an echo or an imprint of the experience

268
00:21:24,580 --> 00:21:27,580
that the mind then access.

269
00:21:27,580 --> 00:21:32,580
And it creates a new experience, which is like the old experience.

270
00:21:32,580 --> 00:21:36,580
That's how memory occurs.

271
00:21:36,580 --> 00:21:41,580
And basically just how you think it occurs.

272
00:21:41,580 --> 00:21:46,580
Something special about it.

273
00:21:46,580 --> 00:21:53,580
But as far as impermanence, if you're talking about from a momentary point of view,

274
00:21:53,580 --> 00:21:56,580
everything is still just cause an effect.

275
00:21:56,580 --> 00:22:00,580
That's one thing after another.

276
00:22:00,580 --> 00:22:07,580
It's complicated enough to store as memories, meaning to give a new experience

277
00:22:07,580 --> 00:22:09,580
that is like an old experience.

278
00:22:09,580 --> 00:22:12,580
So one experience causes an experience that is like that.

279
00:22:12,580 --> 00:22:19,580
And then also the knowledge, this is like that, which is sun, yeah.

280
00:22:19,580 --> 00:22:23,580
But again, knowledge of how things work is not so useful.

281
00:22:23,580 --> 00:22:28,580
What's important is that they do what we're seeing, how they're seeing the way that they work.

282
00:22:28,580 --> 00:22:35,580
Not understanding why they work that way.

283
00:22:35,580 --> 00:22:48,580
I can't wrap my head around the scientific certainty of the idea of reincarnation.

284
00:22:48,580 --> 00:22:51,580
Scientific certainty of the death of the universe.

285
00:22:51,580 --> 00:22:58,580
It is once all the stars burn out and all energy is lost.

286
00:22:58,580 --> 00:23:01,580
Well, all energy is not lost.

287
00:23:01,580 --> 00:23:04,580
But energy is just a concept.

288
00:23:04,580 --> 00:23:13,580
It's just a word that we use to explain something that we're experiencing actually.

289
00:23:13,580 --> 00:23:21,580
Some orderliness, some aspect of the nature of reality.

290
00:23:21,580 --> 00:23:24,580
But I've talked about rebirth a lot.

291
00:23:24,580 --> 00:23:28,580
It's not that we believe in reincarnation, it's that we don't believe in death.

292
00:23:28,580 --> 00:23:31,580
Death is just a concept.

293
00:23:31,580 --> 00:23:41,580
When you die, the mind and the body experience doesn't cease, experience just continues.

294
00:23:41,580 --> 00:23:45,580
There's no scientific certainty of it until you see it for yourself.

295
00:23:45,580 --> 00:23:48,580
When you're reborn, then you'll have certainty of it.

296
00:23:48,580 --> 00:23:51,580
I mean, until you forget it, until you lose that.

297
00:23:51,580 --> 00:23:55,580
Because you don't have any of the echoes reminding you.

298
00:23:55,580 --> 00:23:59,580
It's very hard to remember past lives.

299
00:23:59,580 --> 00:24:07,580
For humans, anyway, because we were caught up in these cycles of birth.

300
00:24:07,580 --> 00:24:11,580
I'm not like an angel or something or God.

301
00:24:11,580 --> 00:24:14,580
There's much more smooth to transition.

302
00:24:14,580 --> 00:24:19,580
For us, the new brain is quite new.

303
00:24:19,580 --> 00:24:25,580
So all the old imprints that create echoes of past lives are gone.

304
00:24:25,580 --> 00:24:27,580
There's not about certainty.

305
00:24:27,580 --> 00:24:29,580
It's about understanding the nature.

306
00:24:29,580 --> 00:24:30,580
It's about arguing.

307
00:24:30,580 --> 00:24:32,580
It's basically logic.

308
00:24:32,580 --> 00:24:37,580
The idea that there's death is what's uncertain.

309
00:24:37,580 --> 00:24:48,580
There's no real good evidence for death, except obviously it appears that the body has stopped working.

310
00:24:48,580 --> 00:24:56,580
But from a point of view, to experience that body didn't exist in the first place.

311
00:24:56,580 --> 00:24:58,580
There's only experiences.

312
00:24:58,580 --> 00:25:03,580
And they continue on.

313
00:25:03,580 --> 00:25:12,580
The only time they die is if you retain the bond.

314
00:25:12,580 --> 00:25:15,580
Hm.

315
00:25:15,580 --> 00:25:18,580
I'm glad some people are appreciating the online course.

316
00:25:18,580 --> 00:25:26,580
It's a shame not more people join up, but maybe once I get back from Thailand to Asia.

317
00:25:26,580 --> 00:25:30,580
So this Saturday, we have the big Buddha's celebration.

318
00:25:30,580 --> 00:25:34,580
And then next week, I'm off to Thailand and Sri Lanka.

319
00:25:34,580 --> 00:25:40,580
And maybe Taiwan actually looks like I may possibly take a trip to Taiwan.

320
00:25:40,580 --> 00:25:42,580
But nothing certain yet.

321
00:25:42,580 --> 00:25:46,580
I'll be back July 1st, I think.

322
00:25:46,580 --> 00:25:48,580
And then we'll start.

323
00:25:48,580 --> 00:25:58,580
No, before July 1st, May 29th, maybe.

324
00:25:58,580 --> 00:26:00,580
So that's all for tonight then.

325
00:26:00,580 --> 00:26:02,580
Thank you all for tuning in.

326
00:26:02,580 --> 00:26:22,580
I'll see you all in the best.

