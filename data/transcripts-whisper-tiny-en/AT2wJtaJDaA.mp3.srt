1
00:00:00,000 --> 00:00:10,000
Hi, everyone. This is Adder, a volunteer for Siri Mangalo International, the organization

2
00:00:10,000 --> 00:00:15,880
that supports Bantayu to Domel Biku in his teaching. I'd like to talk today about our

3
00:00:15,880 --> 00:00:23,160
ongoing need for volunteers. Siri Mangalo International runs with the continued support

4
00:00:23,160 --> 00:00:28,080
of volunteers from our community, and I'd like to share with you a project we're currently

5
00:00:28,080 --> 00:00:36,400
working on. That is, the digital poly reader, also known as the DPR. The DPR is a tool,

6
00:00:36,400 --> 00:00:41,480
a piece of software that works much like a hard copy language reader, facilitating the

7
00:00:41,480 --> 00:00:47,480
study of the poly language as well as the tptaca at an advanced level. It is heavily

8
00:00:47,480 --> 00:00:54,200
used the world over by various faults, such as researchers, poly language professors,

9
00:00:54,200 --> 00:01:02,680
monks, and amateurs. You can check out the current version at poly.seriamongalo.org.

10
00:01:02,680 --> 00:01:09,520
The DPR was initially developed by Bantayu to Domel himself as an extension for Firefox,

11
00:01:09,520 --> 00:01:15,320
and can still be used with the WaterFox and Pale Moon web browsers. However, users keep

12
00:01:15,320 --> 00:01:21,200
requesting new features, and application usage has changed over time. That is why we

13
00:01:21,200 --> 00:01:26,080
would like to give new life to this valuable tool by building a web application with

14
00:01:26,080 --> 00:01:32,560
the same feature set, but with the modern UI technology stack. For the above, we wish to

15
00:01:32,560 --> 00:01:39,640
invite volunteers who can help with development, those skills with React, Redux, TypeScript,

16
00:01:39,640 --> 00:01:48,120
and Material UI, UX, such as those with skills in UX design and tools, preferably with

17
00:01:48,120 --> 00:01:55,120
the basic knowledge of the tptaca, and beta testing, those who wish to be early adopters

18
00:01:55,120 --> 00:02:01,400
and help shape the next version of the DPR. If you are interested in helping with this project,

19
00:02:01,400 --> 00:02:06,360
or want to find out other ways you can plug in and volunteer for Seriamongalo, please

20
00:02:06,360 --> 00:02:23,160
join our Discord server. The links below.

