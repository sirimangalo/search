1
00:00:00,000 --> 00:00:20,000
I would like to use meditation for the purpose of being rested, but I'd also still want to be alert.

2
00:00:20,000 --> 00:00:30,000
Answer, I would like to caution that your intention to rest may be the reason why you were falling asleep.

3
00:00:30,000 --> 00:00:38,000
If that is the only intention that you go into the meditation with, then that is the direction your mind will take.

4
00:00:38,000 --> 00:00:44,000
With the intention to feel rested, your mind will not incline towards cultivating effort.

5
00:00:44,000 --> 00:00:54,000
Rather than becoming more awake or alert, it will tend towards the only way that it knows how to rest, which is to fall asleep.

6
00:00:54,000 --> 00:01:05,000
Meditation practiced properly certainly does improve your mind's restfulness, but it can only do this if you are alert enough to confront the causes of your exhaustion.

7
00:01:05,000 --> 00:01:18,000
The purpose of meditating is to understand reality, especially relating to the difficulties and challenges you face in your mind and your life in general, which requires you to pay attention.

8
00:01:18,000 --> 00:01:23,000
Try to reevaluate what you expect to get out of meditation.

9
00:01:23,000 --> 00:01:28,000
Resting is not the purpose of practicing mindfulness.

10
00:01:28,000 --> 00:01:44,000
That being said, even meditators who understand what it is that meditation is for and use it for the purpose of coming to understand reality as it is, may find themselves feeling drowsy and even falling asleep.

11
00:01:44,000 --> 00:01:59,000
The Buddha taught seven ways of dealing with drowsiness in the Pachala Iamana Sutta, or Pachala Sutta of the Gutturanikaya Book of sevens that one could use to overcome various kinds of drowsiness.

12
00:01:59,000 --> 00:02:06,000
The first method the Buddha taught is to change or examine the object of your attention.

13
00:02:06,000 --> 00:02:13,000
One of the most obvious reasons why someone might become drowsy is because their mind has begun to wander.

14
00:02:13,000 --> 00:02:29,000
You may begin meditating by focusing on a specific object, but over time your mind may start to wander and slowly may fall into a trance-like state that is bordering on sleep and eventually leading you to actually nod off.

15
00:02:29,000 --> 00:02:37,000
When that is the case, it is important to refocus your attention on the original object of meditation.

16
00:02:37,000 --> 00:02:45,000
Be careful not to let yourself fall into reflection on speculative thoughts that lead your mind to wander.

17
00:02:45,000 --> 00:02:57,000
It is often the case that we remember things that we are worried or concerned about and they lead our minds to wander and speculate and eventually get tired and fall asleep.

18
00:02:57,000 --> 00:03:06,000
The Buddha said to be careful not to give those sorts of thoughts any ground and to try not to cultivate the states of mind that lead to drowsiness.

19
00:03:06,000 --> 00:03:15,000
The best way to accomplish this task is through mindfulness, especially mindfulness of the drowsiness itself.

20
00:03:15,000 --> 00:03:24,000
Instead of allowing the drowsiness to lead you away from the present moment, focus on the drowsiness and look at it as the present object.

21
00:03:24,000 --> 00:03:35,000
Being mindful in this way breaks the cycle that increases the drowsiness and if you are persistent, sometimes the drowsiness will disappear by itself.

22
00:03:35,000 --> 00:03:47,000
The attention and the alert awareness of the drowsiness is the opposite of drowsiness and so the drowsiness decreases because of the change in one's mind state.

23
00:03:47,000 --> 00:03:58,000
The second method if mindfulness does not work is to recall the teachings that the Buddha taught or that your teacher gave you and to go over them in your mind.

24
00:03:58,000 --> 00:04:12,000
For instance, think about the four foundations of mindfulness, the body, the feelings, the mind and the various demos like the hindrances, the senses and so on.

25
00:04:12,000 --> 00:04:25,000
You can consider the body, think about your level of awareness of the body, ask yourself if you are actually able to watch the movements of the stomach or be aware of the sitting posture.

26
00:04:25,000 --> 00:04:29,000
You can ask yourself whether you are mindful of the feelings.

27
00:04:29,000 --> 00:04:38,000
When there is a painful feeling, are you actually paying attention to it, saying to yourself, pain, pain?

28
00:04:38,000 --> 00:04:49,000
If you feel happy or calm, are you actually paying attention or are you instead letting it drag you down into a state of lethargy, fatigue and drowsiness?

29
00:04:49,000 --> 00:04:59,000
The same goes for the mind and mind objects, refer back to the teachings and reflect on them in your mind.

30
00:04:59,000 --> 00:05:06,000
Examine the teachings and relate them back to your practice and compare your practice to the teachings.

31
00:05:06,000 --> 00:05:16,000
If you do this first just thinking about these good things might energize the mind, reminding you of what you should be doing.

32
00:05:16,000 --> 00:05:24,000
And second, it will of course allow you to adjust your practice.

33
00:05:24,000 --> 00:05:32,000
If reflection does not work, the third way to deal with the drowsiness is to actually recite the teachings.

34
00:05:32,000 --> 00:05:39,000
This can be quite useful for light people, for example, when you are driving.

35
00:05:39,000 --> 00:05:49,000
Mindfulness practice while driving, especially at night, can unfortunately involve periods of drowsiness and even nodding off.

36
00:05:49,000 --> 00:05:55,000
In such cases, it is safer to switch to reciting the Buddhist teachings.

37
00:05:55,000 --> 00:05:59,000
Recitation is not really an intellectual exercise.

38
00:05:59,000 --> 00:06:08,000
It is something akin to singing, like when non-meditators turn on the radio and sing along when they are driving late at night.

39
00:06:08,000 --> 00:06:16,000
Recitation of the Buddhist teaching will similarly wake you up, but also because it is the Buddhist teachings.

40
00:06:16,000 --> 00:06:21,000
It will stimulate you and give you the encouragement you might need.

41
00:06:21,000 --> 00:06:30,000
Recitation inclines the mind towards the Buddha and his teachings and the meditation practice.

42
00:06:30,000 --> 00:06:36,000
If recitation does not work, the fourth method the Buddha taught is more physical.

43
00:06:36,000 --> 00:06:49,000
The Buddha suggested pulling your ears, rubbing your arms, massaging yourself into maybe stretching a bit to wake up to get the blood flowing and to give yourself some physical energy.

44
00:06:49,000 --> 00:07:05,000
Some people might even go so far as to practice yoga, which could be a very useful technique for waking you up, but because it is a different sort of spiritual practice, I would not recommend extensive practice of yoga and combination with insight meditation.

45
00:07:05,000 --> 00:07:20,000
Still, there is nothing inherently harmful in practicing yoga and it may well be beneficial in moderation for the purposes of building energy necessary to practice meditation.

46
00:07:20,000 --> 00:07:29,000
In any case, the Buddha advised massaging, rubbing, pulling your ears and so on.

47
00:07:29,000 --> 00:07:40,000
If physical intervention does not work, the fifth method to deal with drowsiness is to stand up splash water on your face and look in all directions.

48
00:07:40,000 --> 00:07:44,000
Drowsiness usually comes at night, so up at the stars.

49
00:07:44,000 --> 00:07:56,000
This sort of external focus can help break the imbalance between concentration and effort, making your mind more energetic, throwing off the weight of lethargy and drowsiness.

50
00:07:56,000 --> 00:08:02,000
One of the reasons for walking meditation is that it helps cultivate effort.

51
00:08:02,000 --> 00:08:07,000
If you only practice sitting meditation does not uncommon to feel drowsy.

52
00:08:07,000 --> 00:08:16,000
If you do walking first and then sitting, you will generally find the sitting meditation more energetic.

53
00:08:16,000 --> 00:08:31,000
The sixth method is a specific meditation technique to think of night as day and to resolve on and awareness of light, even though it is dark, as perception of darkness can be a trigger for drowsiness in the mind.

54
00:08:31,000 --> 00:08:39,000
The idea is that by imagining light with your eyes closed, it will trigger more energetic mind states.

55
00:08:39,000 --> 00:08:47,000
If you practice meditation for extended periods, you may actually begin to see bright lights unintentionally.

56
00:08:47,000 --> 00:08:53,000
Some meditators become distracted by such lights, colors, or even pictures.

57
00:08:53,000 --> 00:09:05,000
We have to remind them not to be distracted by them to just remind themselves by seeing, seeing, remembering that they are just visual stimuli.

58
00:09:05,000 --> 00:09:10,000
They are not magical and certainly not the path that leads to enlightenment.

59
00:09:10,000 --> 00:09:17,000
They can, however, provide the benefit of bringing about energy and effort.

60
00:09:17,000 --> 00:09:26,000
If none of these methods work, the Buddha said one should appreciate the persistence of the fatigue to state and lie down.

61
00:09:26,000 --> 00:09:36,000
The Buddha's way of lying down was not on the stomach or back, but on one side, propping the head up with the hand on the head and elbow on the floor.

62
00:09:36,000 --> 00:09:39,000
This is known as the lion's posture.

63
00:09:39,000 --> 00:09:44,000
It is a technique that meditators and monks also use to keep them awake.

64
00:09:44,000 --> 00:09:51,000
If you start to fall asleep, your head starts to fall off your arm and you wake up quite easily.

65
00:09:51,000 --> 00:09:56,000
It can also be quite painful in the beginning.

66
00:09:56,000 --> 00:10:01,000
It is a physical technique that you have to develop, just like sitting cross-legged.

67
00:10:01,000 --> 00:10:08,000
The Buddha also noted, however, that you have to accept that you might fall asleep.

68
00:10:08,000 --> 00:10:18,000
If it is not the proper time to sleep, you can lie down and make the determination, I am going to get up in so many minutes or so many hours.

69
00:10:18,000 --> 00:10:29,000
If it is time to sleep, you say to yourself, I am going to sleep for so many minutes or so many hours or I am going to get up at such and such a time.

70
00:10:29,000 --> 00:10:39,000
Before you go to sleep, think only about the time of getting up so that when that time comes, your mind will wake you up at that time.

71
00:10:39,000 --> 00:10:44,000
It is amazing how powerful the mind can be in this regard.

72
00:10:44,000 --> 00:10:55,000
You will often find yourself waking up a few minutes before the time that you were going to wake up or after however many minutes you intended to sleep.

73
00:10:55,000 --> 00:11:03,000
Once the time has come to get up, the Buddha said to resolve in your mind, I will not give in to drowsiness.

74
00:11:03,000 --> 00:11:08,000
I will not become attached to the pleasure that comes from lying down.

75
00:11:08,000 --> 00:11:15,000
One of the great addictions that we cling to is the addiction to lying down and to sleep.

76
00:11:15,000 --> 00:11:22,000
We like to lie down whenever we can and sleep a lot because of the physical pleasure that comes from it.

77
00:11:22,000 --> 00:11:27,000
The problem with this physical pleasure, of course, is that it is not happiness.

78
00:11:27,000 --> 00:11:32,000
It is not the case that the more you sleep, the more happy you feel.

79
00:11:32,000 --> 00:11:38,000
With too much sleep, you in fact feel more depressed, lethargic and distracted.

80
00:11:38,000 --> 00:11:44,000
You feel less energetic, less alert and less at peace with yourself.

81
00:11:44,000 --> 00:11:52,000
With this last technique, you cannot just lie down and say, I am going to go to sleep and fall asleep.

82
00:11:52,000 --> 00:11:59,000
You have to be strict about it, saying to yourself, I am going to set a definite period of time to sleep.

83
00:11:59,000 --> 00:12:05,000
When I wake up, I am going to get up and go on with my practice.

