Now, we come to the third three of the third of purification.
The other gives us information on the three steps of three stages,
Sila, Samadhi and Panya. So, up to chapter 13, the first two stages Sila and Samadhi are explained.
And so, with this chapter begins understanding or Panya, the third stage in the spiritual development,
the description of the soil in which understanding grows. So, this is for the soil of understanding or the soil of Panya.
So, the first chapter 14, but the first chapter of this part deals with the aggregates.
But first, about Panya. So, now, concentration was described under the heading of consciousness in this
kanga when a wise man established well in virtue of developed consciousness and understanding.
And this stands up here at the beginning of the book.
So, there we find the word consciousness. And by consciousness is meant Samadhi or concentration.
So, concentration has been explained with different kinds of meditation.
So, now, it's a time to talk about Panya. All that has been developed in all its aspects,
like the Bhikkhu is just possessed of the more advanced development of concentration
that is acquired with direct knowledge, the benefits and understanding,
but understanding comes next. And that is still to be developed.
So, up to this point, we have not yet developed understanding.
We have developed concentration. So, now, that is not easy development of Panya.
Firstly, even to know about, that is allowed to develop when it is taught very briefly.
In order therefore, to deal with the detailed method of its development,
there is the following set of questions. So, this question will be answered one by one.
What is understanding? So, understanding Panya is of many sorts and has various aspects
and in different kinds of Panya's. And also, the attempt to explain it or what accomplished
neither its intention or its purpose and what besides lead to destruction.
So, we shall confine ourselves to be kind intended here, which is understanding,
consisting in insights knowledge associated with profitable consciousness.
So, here understanding means kusara, wholesome or profitable, which accompanies the wholesome consciousness
and which consists in insights knowledge. The word, we persona in party is, in fact,
is synonym for Panya. So, we persona and Panya are synonyms in many places.
So, here, by Panya is meant, we persona inside knowledge.
In what sense is it understanding? It is a definition of the word Panya.
It is understanding in the sense of act, of understanding.
These piece definitions are actually very difficult to translate into any language.
So, understanding is Panya, understanding is understanding.
What is this act of understanding? It is knowing in a particular mode,
separate from the modes of perceiving and cognizing.
Now, there are three kinds of understanding or knowing.
The knowing of the Panya, knowing of Sanya, and knowing of consciousness or Wiener.
So, these three are differentiated here.
So, Panya is said to be called understanding or knowing because it knows in a particular mode,
separate from that means different from the modes of perceiving and cognizing.
For though the state of knowing is equally present in perception, in consciousness,
and in understanding nevertheless, perception is only the mere perceiving of an object.
It is a blue or yellow. So, just mere perceiving of an object is called perception.
Sanya, it cannot bring about the penetration of its characteristic as impermanent, painful, and not self.
So, it is maybe it is the first reaction to the object.
The next one is consciousness.
Consciousness knows the object as blue or yellow, and it brings about the penetration of its characteristics.
So, at the consciousness stage, it knows the characteristics, but it cannot bring about by endeavoring the manifestation of Superman.
In fact, understanding knows the object in the way already stated.
It means both mere perception and penetration of characteristics.
It brings about the penetration of characteristics and it brings about by endeavoring the manifestation of the path.
So, Panya is the best of the three. Only through Panya can there be penetration into the nature of things.
And then, it similarly is given, suppose there were three people and so on.
So, each child, a villager and a money changer or someone who is familiar with coins and money and so on.
So, the understanding of these three persons are different. It is different.
It knows that it is the wrong thing. It may not know that it can be used to exchange with some other things, but it will know that it is the wrong thing.
It is yellow or it is white or something like that.
But, a villager and ordinary person knows it is the wrong thing made of copper or silver or gold.
And, he also knows that it can be used as money.
But, the money changer or a person who is familiar with money knows all these two and also knows that it is a coin and he knows when it was trapped, when it was made.
And then, he knows the value of the coins and so on.
So, he knows everything there is to know about the coin.
So, in the same way, pioneer is like the child.
So, without discretion seeing the coin because it every ends the mere mode of fear and subject.
Consciousness is like the villagers seeing the coin.
And understanding is like the money changer, seeing the coin and then understanding everything about the coin.
So, this is the difference between three kinds of knowing, knowing of sin, knowing of consciousness, knowing of pioneer.
In the definition of these three words, there is the root, nia, san nia, there is nia.
And, when nia, there is nia.
And, pioneer, there is nia.
So, the root nia means to know.
So, all these three know the object, but they are knowing is different.
So, knowing of pioneer is the most penetrating.
That is why this act of understanding should be understood as knowing any particular mode, separate form, or different form, the mode of pursuing and recognizing.
So, that is what the word.
It is understanding in the sense of act of understanding referred to.
However, it is not always to be found where perception and consciousness are.
You know, there are, say, 89 types of consciousness and not all are accompanied by pioneer.
Only some are accompanied by pioneer.
Say, we act as a mean and wholesome consciousness, types of consciousness are not accompanied by pioneer.
Even among the wholesome and say, the function, there are some, there are not accompanied by pioneer.
So, it is not always to be found where perception and consciousness are.
But when it is, it is not disconnected from those days.
So, pioneer can be found with some kind, some types of consciousness, but perception accompanies every type of consciousness.
So, wherever there is perception and consciousness, there is not necessarily pioneer.
But wherever there is pioneer, there is also perception and consciousness.
And when it is found with perception and consciousness, it is not disconnected from these days.
That means it arise with these days.
It is not mixed up with, say, perception and consciousness, mixed up by a way of its characteristic and so on.
So, because it cannot be taken as disconnected, thus, this is perception, this is consciousness, this is understanding.
Its difference is consequently subtle and hard to see.
Hence, the vulnerable nervous in a set, a difficult thing opening has been done by the blessed one and so on.
So, it is for ordinary people.
It is very difficult to see, to differentiate, say, this is perception, this is consciousness, this is understanding, this is contact and so on.
And here, vulnerable nervous in a set to King Melinda, that the Buddha has done a difficult thing.
And what is the difficult thing defining or differentiating the immaterial states of consciousness and kids from confidence, which occur with a single object.
That means consciousness and mental confidence arise at the same time, taking the same object.
So, although they take the same object, they are bi-characteristic and so on or by nature, different.
So, defining the immaterial states of consciousness and its commitments, which occur with a single object and which he declared that this is contact, this is feeling, this is perception, this is coalition, this is consciousness.
So, it is very difficult for ordinary people to see these mental states clearly and to say, and this is contact, this is feeling and so on.
But following his teachings and through practice of wibasana meditation, many of these mental states can be seen or known or experienced.
Many meditators come to see these mental states clearly through the practice of wibasana meditation.
And Venerable Nagasana gave it seemingly that you take a handful of water from the ocean and it would be easier to see that this water is from this river and this water is from another river and so on.
It is much more difficult to say that this is contact, this is feeling and so on when consciousness and its commitments arise, taking the same same object.
So, Panya when it arises with consciousness and perception is a different dhamma, not mixed with the other competence.
Like people say, working together, so each person is different from another one, but they come together and they do some job together.
And what are its characteristic function manifestation and approximate cause, so we have to understand these things taught in a bhidhamma with reference to these four aspects characteristic function manifest is mode of manifestation and approximate cause.
So, we will find a lot of these in this chapter and we make chapters understanding has to be characteristic of penetrating the individual essence of states, that means penetrating the nature of things, penetrating the individual nature of characteristic of things as well as penetrating the common characteristic of things.
Each state has its own characteristic and if you watch closely and you will come to see the characteristic of these states and then there is the common characteristic of all states, all conditioned states and that is impermanence and so on.
So, that also you see through Panya or understanding.
So, Panya the characteristic of Panya is penetrating the individual essence of states or the nature of states.
Its function is to abolish the darkness of delusion, it is like lightning, when lightning flashes, darkness is dispelled.
So, dispelling of darkness or here, darkness of delusion or ignorance is its function, the delusion which conceives the individual essence of states.
So, delusion or ignorance is compared to darkness.
So, when there is darkness, we cannot see things in this room, if this room is dark, then we cannot see things in this room and when there is light, we can see things.
So, darkness, just as darkness hides things from being seen, dilution or ignorance conceives the individual essence of states.
We cannot, we do not see or know the individual essence of states because they are dilution or ignorance.
It is manifested as non-delution. So, when you watch Panya itself, it will appear to you, to your mind as, it is non-delution.
Because of the words one who is concentrated knows and sees correctly, its proximate cause is concentration.
So, the Panya meant here that is Vipasana, Panya, and its proximate cause is what? Concentration.
That is why I always say concentration is important and when there is no concentration, no Panya or penetration into the nature of things can arise.
And then many kinds of Panya understanding. First one, then two, three, four, and so on.
So, Panya is one, according to its characteristic of penetrating the individual essence.
But it is two, page 482, paragraph 9. As regards the painful section, the mundane is dead associated with the mundane path.
Mandain path means especially during Vipasana meditation. So, during Vipasana meditation, the factors of path arise in your mind.
Right effort, right mindfulness, right concentration, and right understanding.
So, mundane understanding is that associated with the mundane path. That means Vipasana meditation.
And super mundane is that associated with the super mundane path. That is at the moment of enlightenment.
At the moment of enlightenment, what is called path consciousness arises. And Panya accompanying that path consciousness is called here, super mundane Panya.
So, it is of two kinds. In the second diet, that subject to kangas is that which is the object of kangas.
So, we can just see mental defilements. Which can be the object of mental defilements. That free from kangas is not their object.
So, which cannot be the object of mental defilements. So, this diet is the same in meaning as the mundane and super mundane.
So, subject to kangas means mundane and free from kangas means super mundane.
The same as that applies to the diet, subject to kangas and free from kangas and so on.
Now, in the third diet, when a man wants to begin inside his understanding of the defining of the four in material aggregates is understanding as defining of mentality.
So, defining of mental ready. That means clearly understanding the mental ready is defining of mental ready and clearly seeing or understanding the material aggregates understanding as defining of material ready.
So, it is of two kinds. So, in short, it is understanding of mind and understanding of matter.
In the fourth diet, understanding belong to two of the kinds of sensory or profitable consciousness and belonging to sixteen of the kinds of fat consciousness with four of the jhanas and the full full method is accompanied by joy.
Now, what is sixteen? The four paths with the first jhanas and those with the second, third and full.
Out of the five. Now, now here, the common data is using the five full method, five jhanas, not four jhanas.
So, the first, second, third and full jhanas are accompanied by joy and the field is accompanied by equanimity.
So, accompanied by joining jhanas up to the fold. Understanding belonging to two of the kinds of sensory or profitable consciousness and belonging to the remaining four kinds of fat consciousness that is a fifth jhanas is accompanied by equanimity.
So, Panya accompanied by joy and Panya accompanied by equanimity. In the sense of fear, consciousness to accompanied by joy and to accompanied by equanimity.
In the fifth jaya, understanding belonging to the first part is called plane of seeing.
Now, here seeing means seeing a neighbor in the first place or initial seeing of neighbor.
So, plane of seeing means that belonging to the first path.
The first path is called seeing or dasana in Bali, because it sees Nivana first.
Understanding belonging to the remaining three paths, that is, second and third path, they are called plane of development.
Bhavana. So, it is of two kinds. And then, that regards the triads. Understanding a quiet without fearing from another is that consisting in what is reasoned.
That means understanding from one's own thinking. Understanding which arises from one's own thinking and not heard from others or not having read books.
And that is understanding, understanding, consisting in what is what is reasoned or what is thought.
And then, second one is understanding, consisting in what is heard. That means understanding, obtaining through listening to others or through reading books.
And the last one is understanding, consisting in development. Here, development means practice of meditation.
So, understanding, obtained through the practice of meditation, is that which consists in development.
So, there are three kinds of panya mentioned here. And then, there is quotation.
In this quotation, about five lines down, that concerns ownership of deeds. That means understanding that beings have come only as their own.
Or is in conformity with truth. That means just Vipasana. Because Vipasana helps us to see the true nature of pains. And so, in conformity with truth, here means just Vipasana.
And then, paragraph 15, say, the understanding that occurs contingent upon Sanskrit states. That's a limited object. That means understanding that takes Sanskrit states as object.
That is called understanding which has a limited object. And then understanding, taking the fine material sphere states for immaterial sphere states as an object is called.
An object is called exalted upon having an exalted object. And this is mundane inside. That which occurs contingent upon Nibana has a measured subject. And that is super mundane inside.
Now, inside means Vipasana. And Vipasana is mundane, not super mundane. When you reach the enlightenment stage and enlightenment moment, it becomes super mundane.
So, in fact, there is no super mundane inside. Inside is always mundane. But here, by super mundane inside, the other meant the threat understanding and the standing of the path. And not Vipasana understanding.
So, it is of three kinds. And then, it is increased, that is called improvement. And that is true full as the elimination of hum and the arousing of good.
Here an improvement is killed in these, according as it says. So, this is skill and improvement. Skill in, say, development. The next one is skill in detriment.
Means understanding, understanding the diminution of good and the arousing of hum. And the third one is what?
But in either of these cases, any skill in means, then in causes of production means and cause of production are the same here. Means means just the cause of production of such and such things which skill august at that moment.
Because on that occasion is what is called skill in means. I think you are familiar with the word upaia. It is now used a lot by people here. So, upaia.
Upaia is translated as means. So, skill in means means. In Bali, it is upaia cosala. Skill in understanding how to do things at the given moment. And it is aroused on that occasion.
So, it just popped up when there is some occasion. So, it is not pre meditated. So, it is of three kinds. And then the other in the full triad, insight understanding initiated by apprehending one's own aggregated, interpreting the internal.
Here, interpreting means just reflecting upon or paying attention to that is internal. And external is taking other people's aggregate as object of meditation. And that is external.
And both are interpreting the internal and external. And there is a footnote for the for the what a big new reason part is. And the other said at the end that it is interpretation or misinterpretation and insistence that being chosen here as a rendering.
So, interpretation and just means something like a view on a given object. And then tetraps, full kinds of knowledge or full kinds of understanding.
Understanding of suffering, first noble truth, understanding of origin of suffering, second noble truth, understanding of cessation of suffering and understanding of the way leading to the cessation of suffering.
And then the next tetraps is four kinds of discriminations. And these four kinds of discriminations are mentioned in the commentaries very often.
And sometimes when the enlightenment of a person is described and the books would say he gained enlightenment along with the four discriminations.
That is, this is a special knowledge. But you don't have to practice in a special way, but you just practice as, say, we pass on our meditation. And then when you become enlightened, these four kinds of knowledge automatically automatically come to you.
So these are the called food discriminations, or participant videos. Knowledge about meaning is the discrimination of meaning. Knowledge about law is the discrimination of law.
Knowledge about enunciation of language dealing with meaning and law is the discrimination of language. Knowledge about kinds of knowledge. Knowledge about knowledge.
is discrimination of pospicuity, so there are four kinds of discriminations.
Here in meaning, Arthur is briefly a term for the fruit of the cause.
So, the pali word is atta, and the pali word atta can mean
a result of purpose, and also fruit, also meaning,
and also, yes, also a result. So, if you translate it as just meaning,
it may create some misunderstanding, but it is difficult to translate this word into any language,
because as we will see, the word atta here means five things,
anything conditionally produced, that is one, two nibana, three, the meaning of what is spoken,
that means the meaning of words, and four, result of comma, and five functional consciousness
should be understood as meaning. So, one you say, knowledge about meaning
discrimination of meaning means knowledge about these five things.
When anyone reviews that meaning, any knowledge of his falling within the category,
concerned with meaning is the discrimination of meaning. So, discrimination of meaning means
understanding of the things conditionally produced, understanding of nibana,
understanding of a meaning of a word, understanding of the result of the comma,
and understanding of the functional consciousness. And then, second one is dhamma law.
It is briefly a term for condition. For sense, the condition necessitates,
whatever it may be, makes it over allows it to help and adjust their book called law or dhamma.
So, discrimination of law means understanding of the condition or understanding of the cause,
but in particular, the five things namely are meant by the word dhamma here, one, any cause
that produces fruit, two, the noble fat. It is a cause for a cause of the fruition,
three, what is spoken? That means the words of language, and four, what is profitable,
cusala, and five, what is unprofitable, a cusala should be understood as law. So, by the word dhamma,
these five things are meant. When anyone reviews that knowledge and that law, any knowledge of his
falling within the category concerned with law, it is discrimination of law. So, discrimination of
condition or discrimination of cause or understanding of causes, and this
meaning is shown in the dhamma by the falling analysis. So, there is a quotation from the second
book of the dhamma. Now, let us go to 25, paragraph 25, knowledge about enunciation of language
dealing with meaning and law. That is the language that is individual essence. Now, the word
subhava is translated, normally translated as individual essence, but here the word subhava
means is explained in the sub commentary as something like correct or not,
that which does not change or that which does not deteriorate. That is called the subhava,
neurotic and parley. The usage that has no exceptions, that means that is definite,
that is not ambiguous, something like that, and deals with that meaning and that law. Any knowledge
falling within the category concerned with the enunciation of that, with speaking with the
utterance of that concerned with the root speech of all beings, the margarine language that is
individual essence. Now, in theorem of Buddhism, the language of margarine is said to be the
root speech of all beings, the original language of all beings. So, the belief of theorem of
the Buddhist is that the margarine language or the parley language as we know it now, parley
language is the original language of all beings. Only after many, many years, different people
develop different dialects and then they became languages. So, this is the belief of ancient, ancient
teachers. Hindu. So, it is a margarine language is taken to be parley. I think the present day parley
is the closest to the language used by the Buddha. It is difficult to say that it is the language
of the Buddha, because we really don't know with which language the Buddha used, but Buddha used
the language which was current at its time and which was understood by all people, not by
high-class people only, because once two Brahmins who became monks asked him to turn his teachings
into Vedic Sanskrit and Buddha rejected them. Buddha said, my demise for all people, not for a selected
few. So, he used the language with us understood by all people, the common language, basically.
So, we now think that actually the language of India would share many languages in the
India now and very dialects, Buddha traveled greatly throughout India and maybe at that time there
was more common. We don't know, but there may be different languages in different parts,
or maybe dialects in different parts of the country. And the language Buddha used maybe
understood by all people, where he roamed, and that language is said to be the mulebhasa,
we call it mulebhasa, root speech of all beings. In other words, the language of law, as soon
as it hears its spoken, pronouns, other notes. This is the individual essence language, this is not
the individual essence language. That means it is a correct language, it is not a correct language,
something like that. So, it was in who has got this kind of discrimination, knows that this is
correct, and this is not, it is a prophetically correct, and this is not. So, the example given here is,
one who has reached the discrimination of language knows on hearing the words faço,
within a etc. That this is the individual essence language, but that means this is the correct language.
And on hearing faça, we don't know, etc. He knows that this is not the individual essence
language, it is not the correct word or correct language, because the word faça is masculine
gender. So, it is in the nominative case, it must be faço, and it can never be faça in nominative
case. So, when you say faça, you know this is wrong, this is chromatically wrong. The word
we don't know belongs to feminine gender, it is never in the masculine gender. So, if you say
we don't know, then you put, you put a feminine word into masculine gender, and so it is wrong.
So, if you do not know Pali, then you try to get enlightenment and get these four kinds of
discrimination, and then you will automatically understand Pali. That is what what, what is meant here.
So, Pali language is said to be, said to be the root speech of all beings, and said to be the
best of the languages. Now, the language we call Pali is never mentioned as Pali language
in the commentaries. In the commentaries, it is referred to as marketer language or root language.
The word Pali simply means the text as against commentaries and sub commentaries.
But later on, since it is the language in which the text were recorded, we call it a Pali language.
So, the word Pali language is of recent origin, not used by commentaries and sub commentaries.
So, and what they used is the word mulabhasa or magatah or magatibasa, the language of the magatah,
magatah country. It is, it is maybe like a state in this country.
During the time of the border, there was 16, we call them 16 countries, but not 16 countries, 16
states or districts. So, magatah was one of them.
And knowledge about kinds of knowledge, that is knowledge of knowledge. So, that is
discrimination of persecuity.
And these four kinds of discrimination, we can make them
clear, we can make them sharp, or we can make them, we can make ourselves
adepts in these four by five means. And these five means are given in paragraph 28.
And though they come into the categories of the two planes, that they are nevertheless distinguishable
in five aspects, that is to see as achievement and so on. Actually, they can be brought to
perfection by these five things, by achievement, that means by enlightenment, by achievement,
by mastery of scriptures, by hearing, by questioning, and by prior effort.
So, achievement is the reaching of our handship, but it is the best dimension here.
So, reaching of other stages of enlightenment is also meant here.
Mastery of scriptures is must be of Buddha's word. Hearing is learning the damn carefully
and then attentively. Questioning is discussion of naughty passages and explanatory
passages and historical commentaries and so on. Prior effort is devotion to insight in the dispensation
of former Buddhas. So, it is like a barometer. Up to the vicinity of the stages of conformity
and change of lineage by one who has practiced the duty of going with the meditation subject
on Armstrong and coming back with it. Now, months have described as of different kinds and there are
months who take meditation even when they go to the village for arms and they take that
meditation to the monastery. That means when they go to the village, they go with meditation
and when they come back from village, they come back with meditation. So, such persons. So,
they have practiced meditation in this way. So, that is what is made here by the duty of going
with the meditation subject on Armstrong and coming back with it. And they may have reached
the stages of we bus an hour. Just short of enlightenment. If they are enlightened, they don't have to
they don't have to worry about practicing again to gain enlightenment. So, here up to the
visanity of the stages of conformity and change of lineage simply means just short of enlightenment.
So, those who have practiced we bus an hour division in the former Buddhas. That means in the
in the former life just short of enlightenment. So, that is called prior effort. So, such people
may get enlightenment in this life and may get these four kinds of discriminations.
And then some other set in a different way but more or less the same. So, prior effort,
great learning, and then dialects, scriptures, questioning, achievements, with…
