1
00:00:00,000 --> 00:00:05,000
Here's a good one. Thank you for your erratic question.

2
00:00:05,000 --> 00:00:09,000
Question is, it's not better to suffer in future lives.

3
00:00:09,000 --> 00:00:16,000
It's not better to suffer in future lives instead of reaching enlightenment and not live anymore.

4
00:00:16,000 --> 00:00:26,000
I hear you. It's quite clear and I think this is on a lot of people's minds.

5
00:00:26,000 --> 00:00:33,000
There was a queen once in the Buddha's time and she became an Arahant.

6
00:00:33,000 --> 00:00:39,000
And the Buddha said to the king, well either you,

7
00:00:39,000 --> 00:00:47,000
either you let her become a bikunin or she's going to pass away within seven days.

8
00:00:47,000 --> 00:00:52,000
Because there's no way she could survive. There's no way she could continue.

9
00:00:52,000 --> 00:00:57,000
It's not really possible.

10
00:00:57,000 --> 00:00:59,000
So she would pass away and he was like,

11
00:00:59,000 --> 00:01:02,000
oh no, then an ordainer, ordainer right away.

12
00:01:02,000 --> 00:01:05,000
Because he couldn't bear to have her disappear.

13
00:01:05,000 --> 00:01:08,000
He's not bear to have her gone.

14
00:01:08,000 --> 00:01:16,000
He said, enough of this talk of very nibana.

15
00:01:16,000 --> 00:01:26,000
The question, sorry, I'm sorry to find it funny, but it really answers itself.

16
00:01:26,000 --> 00:01:32,000
What you're saying is you want to suffer in future lives.

17
00:01:32,000 --> 00:01:38,000
So there's nothing really to worry about because you have no chance.

18
00:01:38,000 --> 00:01:45,000
With that mind-state remaining, you have no chance of not living anymore.

19
00:01:45,000 --> 00:01:54,000
You will have to come back because there is still this cause for future rebirth.

20
00:01:54,000 --> 00:01:58,000
At the moment of death, you don't say, okay enough.

21
00:01:58,000 --> 00:02:03,000
You say more or more or more. What about this? What about that?

22
00:02:03,000 --> 00:02:09,000
So don't you don't have to wrestle with this one.

23
00:02:09,000 --> 00:02:12,000
You don't have to think, mmm, should I practice Buddhism?

24
00:02:12,000 --> 00:02:15,000
Because if I practice Buddhism maybe I won't live anymore.

25
00:02:15,000 --> 00:02:17,000
It's not possible.

26
00:02:17,000 --> 00:02:23,000
The only way that you could not come back and live again to suffer more and more

27
00:02:23,000 --> 00:02:28,000
is if you decided for yourself through what I would understand

28
00:02:28,000 --> 00:02:36,000
to be an incredible realization of the truth, but it depends who you ask.

29
00:02:36,000 --> 00:02:39,000
That there were no reason to come back.

30
00:02:39,000 --> 00:02:46,000
You will come to realize that there was no benefit.

31
00:02:46,000 --> 00:02:49,000
And you come to realize that in fact there is no one living at all.

32
00:02:49,000 --> 00:02:52,000
There is only actually these experiences.

33
00:02:52,000 --> 00:02:56,000
None of which are of any benefit to you.

34
00:02:56,000 --> 00:02:58,000
Or of any benefit in general.

35
00:02:58,000 --> 00:03:02,000
None of which have no intrinsic value.

36
00:03:02,000 --> 00:03:06,000
And when you realize that you're not born anymore,

37
00:03:06,000 --> 00:03:09,000
it just happens.

38
00:03:09,000 --> 00:03:12,000
So for yeah, for as long as you,

39
00:03:12,000 --> 00:03:16,000
you still think that there's some benefit to existence.

40
00:03:16,000 --> 00:03:17,000
Don't worry about it.

41
00:03:17,000 --> 00:03:22,000
You got lots of time to come back again and again.

42
00:03:22,000 --> 00:03:24,000
Which a lot of people like about Buddhism.

43
00:03:24,000 --> 00:03:27,000
They think, well great, I'll just come back again and again and again.

44
00:03:27,000 --> 00:03:32,000
Eventually I'll learn everything and become enlightened.

45
00:03:32,000 --> 00:03:37,000
All in good time, but first let me explore this kind of this novel idea

46
00:03:37,000 --> 00:03:42,000
that I can actually do good deeds and go to heaven and so on.

47
00:03:42,000 --> 00:03:44,000
Which is fine.

48
00:03:44,000 --> 00:03:49,000
It's kind of unshaky ground because you never know where you're going to go.

49
00:03:49,000 --> 00:03:52,000
And maybe this life, next life you go to heaven,

50
00:03:52,000 --> 00:03:59,000
but once you forget all of the stuff that you've learned in regards to goodness and badness,

51
00:03:59,000 --> 00:04:04,000
you might come back as one of those evil people and go to hell as well.

52
00:04:04,000 --> 00:04:09,000
So yeah, but really that's the point is that

53
00:04:09,000 --> 00:04:18,000
to answer your question directly, it's because there's nothing good about any piece of existence.

54
00:04:18,000 --> 00:04:21,000
It's actually a delusion that we have.

55
00:04:21,000 --> 00:04:25,000
It's not understanding reality as it is.

56
00:04:25,000 --> 00:04:31,000
But for as long as you understand reality to have some intrinsic benefit or realities,

57
00:04:31,000 --> 00:04:37,000
as long as you understand experience,

58
00:04:37,000 --> 00:04:41,000
seeing, hearing, smelling, tasting, feeling, thinking,

59
00:04:41,000 --> 00:04:46,000
to have some intrinsic value, you come back and there's no need to worry about that.

60
00:04:46,000 --> 00:04:49,000
The other thing I'd say is that it's kind of like,

61
00:04:49,000 --> 00:04:54,000
it kind of like works like pulling the thread of a sweater.

62
00:04:54,000 --> 00:04:57,000
Because you've got this loose thread and you say, well, that's no good.

63
00:04:57,000 --> 00:04:59,000
I've got to get rid of that.

64
00:04:59,000 --> 00:05:04,000
But as you pull it, you realize that it's attached to so much more.

65
00:05:04,000 --> 00:05:09,000
And then you keep pulling and pulling and pulling and eventually you're left without a sweater.

66
00:05:09,000 --> 00:05:12,000
Suffering is really in that vein.

67
00:05:12,000 --> 00:05:15,000
We come to practice not because we want to become enlightened,

68
00:05:15,000 --> 00:05:17,000
but because we've got a lot of problems.

69
00:05:17,000 --> 00:05:22,000
So we figure, get rid of those problems and life will be fine.

70
00:05:22,000 --> 00:05:24,000
Then I won't need to come embedded.

71
00:05:24,000 --> 00:05:25,000
Then I won't have any interest embedded.

72
00:05:25,000 --> 00:05:26,000
That's enough.

73
00:05:26,000 --> 00:05:29,000
Let me get that far and then quit.

74
00:05:29,000 --> 00:05:34,000
Problem is, as you explore these questions or these problems,

75
00:05:34,000 --> 00:05:39,000
deeper and deeper into you get to the root of the problem.

76
00:05:39,000 --> 00:05:42,000
You find that the root is actually the root of who you are.

77
00:05:42,000 --> 00:05:46,000
It's the root of your identification with existence.

78
00:05:46,000 --> 00:05:51,000
And once that's gone, then you have no interest.

79
00:05:51,000 --> 00:05:56,000
Either way, there's no interest in coming back as this or that.

80
00:05:56,000 --> 00:05:59,000
There's no aversion to existing.

81
00:05:59,000 --> 00:06:03,000
And so there's just peace.

82
00:06:03,000 --> 00:06:07,000
And then at the end there's freedom.

83
00:06:07,000 --> 00:06:23,000
So I hope that answers your question.

