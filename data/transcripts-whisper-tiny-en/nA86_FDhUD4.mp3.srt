1
00:00:00,000 --> 00:00:05,880
On your Facebook page, you mentioned putting a system in place for supporting the monastery

2
00:00:05,880 --> 00:00:06,880
food-wise.

3
00:00:06,880 --> 00:00:11,640
Could you please elaborate on how this system should be set up for a teravado monks and

4
00:00:11,640 --> 00:00:15,600
how people could be of service to monks in general?

5
00:00:15,600 --> 00:00:26,080
I think I've commented on this question or a similar question, but it's interesting because

6
00:00:26,080 --> 00:00:33,480
obviously I've been thinking about this and it occurs to me that this kind of system

7
00:00:33,480 --> 00:00:37,680
is not really ideal, right?

8
00:00:37,680 --> 00:00:46,760
The ideal would be to, well, the principle, forgetting about the ideal for a second, the

9
00:00:46,760 --> 00:01:05,000
principle is applying to those people who are interested or looking to do a good deed, looking

10
00:01:05,000 --> 00:01:08,880
to give already.

11
00:01:08,880 --> 00:01:13,960
That is actually, that's an important principle, it's not readily evident.

12
00:01:13,960 --> 00:01:23,440
Some people think that the way Buddhism works is they see a monk, people would see a monk

13
00:01:23,440 --> 00:01:27,360
going on arms round and think, oh, here's someone who needs food.

14
00:01:27,360 --> 00:01:31,640
Let me support him or her because they need it.

15
00:01:31,640 --> 00:01:41,480
That's not actually why the arms round was created or was instated in Buddhism, it was

16
00:01:41,480 --> 00:01:50,760
instated because there were people who had set up such offerings.

17
00:01:50,760 --> 00:02:10,600
They would stand, it was a tradition, and even that notwithstanding, it's an important distinction

18
00:02:10,600 --> 00:02:21,640
because it otherwise slips into begging or advertising and so there's actually a curious

19
00:02:21,640 --> 00:02:32,560
rule as I recall, if my memory serves me, where monks are, yes, the protocol is quite

20
00:02:32,560 --> 00:02:41,560
clear that the bull, amongst bull, should be under their robe at all times except when someone

21
00:02:41,560 --> 00:02:42,560
is offering food.

22
00:02:42,560 --> 00:02:51,000
The point being, you can't advertise that you're actually looking for food, and regardless

23
00:02:51,000 --> 00:02:59,040
of how it happens, this is an important principle to keep in mind at all times and specifically

24
00:02:59,040 --> 00:03:10,440
when going on arms round is that you're not looking to encourage people to give.

25
00:03:10,440 --> 00:03:15,120
Even though it may encourage them to give, obviously that happens definitely and most of

26
00:03:15,120 --> 00:03:19,160
the time that's what happens is people see it and they say, oh, here's someone who needs

27
00:03:19,160 --> 00:03:24,640
food, let me give them some food or at the very least, oh, here's a chance for me to

28
00:03:24,640 --> 00:03:32,880
support Buddhism and then they support it, but nonetheless in the monks mind there has

29
00:03:32,880 --> 00:03:41,040
to be a sense of non-advertisement.

30
00:03:41,040 --> 00:03:49,800
So to some extent we're skirting this line, and so I thought of this this morning I thought

31
00:03:49,800 --> 00:03:58,080
this isn't quite how I'd envision living, you know, to have this set up a system where

32
00:03:58,080 --> 00:04:03,800
we're no longer directly dealing because people want to order food from restaurants for

33
00:04:03,800 --> 00:04:04,800
us.

34
00:04:04,800 --> 00:04:12,400
Of course that doesn't work really because people come to the monastery and offer food just

35
00:04:12,400 --> 00:04:14,960
on whim.

36
00:04:14,960 --> 00:04:18,040
And so someone comes today and offers food and then we get food to the restaurant suddenly

37
00:04:18,040 --> 00:04:24,080
we've got twice as much food as we can eat and often it would get thrown out and furthermore

38
00:04:24,080 --> 00:04:28,920
someone orders for four people, there's only three people, et cetera, et cetera.

39
00:04:28,920 --> 00:04:38,280
So we switch now our ideas to order food from the grocery from the grocery stores.

40
00:04:38,280 --> 00:04:43,160
So people want to order food from the grocery stores for us, but this also is problematic

41
00:04:43,160 --> 00:04:48,640
because what if we get lots of fresh produce and we can't keep it and we can't use it

42
00:04:48,640 --> 00:04:52,120
all then again we're throwing out food.

43
00:04:52,120 --> 00:05:01,280
So potentially you know there's these issues but ideally we want to try and arrange

44
00:05:01,280 --> 00:05:11,160
it ourselves and the only input people other people have is money which is awful, you

45
00:05:11,160 --> 00:05:17,760
know this isn't how you want to run things, you don't want to just you know be a financial

46
00:05:17,760 --> 00:05:23,840
you know or want a business where you run things and you require inputs of money it's

47
00:05:23,840 --> 00:05:26,520
I mean where did the arms run go?

48
00:05:26,520 --> 00:05:34,640
It was so much nicer when people ordering from the restaurant or from the grocery store.

49
00:05:34,640 --> 00:05:39,600
But so this thought came up that this isn't exactly the direction we want to go.

50
00:05:39,600 --> 00:05:44,320
But then the thought came up that I'm missing the point.

51
00:05:44,320 --> 00:05:48,800
The point is not food for me or the other monks here.

52
00:05:48,800 --> 00:05:50,800
The point is we have meditators.

53
00:05:50,800 --> 00:06:01,880
We're dealing with a different kind of a system, oops recording this, sorry about that.

54
00:06:01,880 --> 00:06:06,840
The system we're dealing with is a meditation center and we're dealing with lay people.

55
00:06:06,840 --> 00:06:12,400
So in fact what we're talking about is lay people supporting lay people, meditators supporting

56
00:06:12,400 --> 00:06:17,360
other meditators which is also very wholesome but it works in a different way.

57
00:06:17,360 --> 00:06:25,000
These people are not monks and they're certainly not able to go on arms run.

58
00:06:25,000 --> 00:06:31,520
So we're dealing with we have a in that extent we have a lay organization, Siri Mongol

59
00:06:31,520 --> 00:06:39,800
International that is running these courses and that's lay people dealing with lay people.

60
00:06:39,800 --> 00:06:46,480
And as a monk I really, I'm probably not acting entirely appropriately by getting involved

61
00:06:46,480 --> 00:06:49,840
with this but in the modern day this is how we run.

62
00:06:49,840 --> 00:06:58,160
We do things the monks are kind of overseeing laymont lay meditation centers, even in

63
00:06:58,160 --> 00:07:00,120
Thailand you know.

64
00:07:00,120 --> 00:07:05,720
The monks are quite involved with lay meditation, lay people coming to the monasteries.

65
00:07:05,720 --> 00:07:10,520
In some places, some places will not do it that way, they will not have anything to do

66
00:07:10,520 --> 00:07:13,240
with lay people.

67
00:07:13,240 --> 00:07:16,920
But our tradition has always, even in the time of Mahasi Saiyanda lay people went to the

68
00:07:16,920 --> 00:07:23,600
monasteries and were sort of taken care of the monks were certainly at least partially involved.

69
00:07:23,600 --> 00:07:26,480
So practically speaking we kind of have that.

70
00:07:26,480 --> 00:07:29,680
This isn't quite answering your question but just giving some background and sort of the

71
00:07:29,680 --> 00:07:32,800
philosophies involved here.

72
00:07:32,800 --> 00:07:39,360
So this is a question that I'm asking is in regards to the monks what is a proper system

73
00:07:39,360 --> 00:07:49,120
and I think it's important to not lose sight of that connection where you find people

74
00:07:49,120 --> 00:07:58,080
who want to give, where you rely on people who want to give and all you're doing with

75
00:07:58,080 --> 00:08:02,360
this technical organization is facilitating that.

76
00:08:02,360 --> 00:08:11,560
How do you connect and the person to be supported with the person doing the supporting?

77
00:08:11,560 --> 00:08:21,840
The philosophy is, I've talked about these three kinds of philosophies, the reciprocal

78
00:08:21,840 --> 00:08:32,280
system, the pro-syprical system and what was the third one?

79
00:08:32,280 --> 00:08:38,360
I don't have a name for it, it could be associative or applicative or something.

80
00:08:38,360 --> 00:08:43,960
The reciprocal we understand is, if I give to you you have to give me something back

81
00:08:43,960 --> 00:08:50,360
and return, this is a capitalist system, it doesn't work because I want something from

82
00:08:50,360 --> 00:08:53,960
you, I don't want to give you anything.

83
00:08:53,960 --> 00:08:58,040
So it's not logical.

84
00:08:58,040 --> 00:09:05,720
Suppose you have a bunch of people, if someone suddenly needs something, that need has

85
00:09:05,720 --> 00:09:13,760
nothing to do with giving something, there's arguments against this, but from a simplistic

86
00:09:13,760 --> 00:09:20,760
point of view, the problem isn't solved by requiring something in return because people

87
00:09:20,760 --> 00:09:27,960
who can't pay it, it sets you up for this problem where a person needs something and

88
00:09:27,960 --> 00:09:35,480
can't afford it or certain people can afford more, it sets you out for this problem.

89
00:09:35,480 --> 00:09:43,360
The pro-syprical system is where, if I give to you, you're under an obligation to

90
00:09:43,360 --> 00:09:49,320
give to someone else, this also doesn't make any sense because then the only people giving

91
00:09:49,320 --> 00:09:58,520
are those who have been given to and you're never going to solve the problem of needs.

92
00:09:58,520 --> 00:10:01,400
The only system that really makes sense is where when someone needs something, you give

93
00:10:01,400 --> 00:10:04,120
it to them.

94
00:10:04,120 --> 00:10:11,560
Any time anyone gives, it's a one-sided transaction, wherever it's needed, you associate

95
00:10:11,560 --> 00:10:19,800
or you allocate, maybe a locative would be a good name, you allocate resources where they're

96
00:10:19,800 --> 00:10:20,800
needed.

97
00:10:20,800 --> 00:10:25,160
Of course, whether that works, it could work in the society or not.

98
00:10:25,160 --> 00:10:33,600
I don't know if that describes communism in any way, but I don't think exactly.

99
00:10:33,600 --> 00:10:41,000
If you apply resources exactly as they're needed, then all needs are met.

100
00:10:41,000 --> 00:10:52,920
This is the ideal, probably the ideal, similar to the ideal of communism, but in a system

101
00:10:52,920 --> 00:10:55,280
such as Buddhism, that's the idea.

102
00:10:55,280 --> 00:11:02,720
When I teach, I'm giving, there's no, I cannot have the sense of requiring something

103
00:11:02,720 --> 00:11:05,160
in return.

104
00:11:05,160 --> 00:11:10,000
As well, everyone else in the system supports me because I need food.

105
00:11:10,000 --> 00:11:14,080
I need food, and I'm part of the system.

106
00:11:14,080 --> 00:11:16,160
Give me that food.

107
00:11:16,160 --> 00:11:21,120
The system, in any thing you could say, is the system requires me to have food.

108
00:11:21,120 --> 00:11:26,640
Maybe even more than it requires other people to have food because in our organization,

109
00:11:26,640 --> 00:11:30,640
I play a large role as far as teaching.

110
00:11:30,640 --> 00:11:36,240
This isn't about me, but the idea of a monk, you support the monk because they play an

111
00:11:36,240 --> 00:11:37,240
important role.

112
00:11:37,240 --> 00:11:44,400
You support the teacher because they play an important role, and you allocate, not more

113
00:11:44,400 --> 00:11:48,840
resources, but you concern yourself with that, especially because that's an important

114
00:11:48,840 --> 00:11:52,800
part of the system.

115
00:11:52,800 --> 00:12:01,120
I think that's an important, where we start with this kind of system.

116
00:12:01,120 --> 00:12:06,920
The question is, how in modern day society, how do we apply these ideas of people ordering

117
00:12:06,920 --> 00:12:13,600
food online and so on?

118
00:12:13,600 --> 00:12:17,800
I think you could potentially set up, if we were only dealing with, suppose it was

119
00:12:17,800 --> 00:12:23,080
only me, and I was living alone in a house.

120
00:12:23,080 --> 00:12:30,120
I could potentially set up a system, and that's the thing, is chaotic here, and there's

121
00:12:30,120 --> 00:12:38,400
the Cambodian community, and so on, so we can't easily predict from day to day what's

122
00:12:38,400 --> 00:12:40,360
going to happen.

123
00:12:40,360 --> 00:12:45,000
There's the Cambodian people, there's a group of alloshan people, and there's the scattered

124
00:12:45,000 --> 00:12:54,560
streets, Sri Lankan, and even western people, even that can be problematic because our

125
00:12:54,560 --> 00:13:00,760
fridge space is limited, we can't just accept groceries any which way, it has to be still

126
00:13:00,760 --> 00:13:06,200
systematic, but that's specifically because of the nature of it, we're dealing with two

127
00:13:06,200 --> 00:13:08,680
systems here, the meditators and the monks.

128
00:13:08,680 --> 00:13:12,600
If it was just me living in a house, probably easy to regulate, make sure that this monk

129
00:13:12,600 --> 00:13:17,960
has food every day, so he can do his online talks and continue, etc, etc.

130
00:13:17,960 --> 00:13:23,960
But I think really in this case, our system has to be a little more systematic, and I think

131
00:13:23,960 --> 00:13:31,840
we're really going to have to put the foot down, put our foot down and sort of require

132
00:13:31,840 --> 00:13:42,560
the grocery shopping to be systematic and not just acceptable, but not just accept random groceries

133
00:13:42,560 --> 00:13:49,880
from people, because we threw around the idea of just accepting groceries from California,

134
00:13:49,880 --> 00:13:50,880
actually.

135
00:13:50,880 --> 00:13:55,840
Someone said, oh contact my Thai people, because I have a whole bunch of Thai ex-students

136
00:13:55,840 --> 00:13:59,880
in California from when I was there, and they'll all send many of them or restaurant owners,

137
00:13:59,880 --> 00:14:04,200
they would all send groceries to Canada, no problem, they might get boxes, in fact I

138
00:14:04,200 --> 00:14:09,760
yet may, we yet may get boxes of groceries from California, I don't know, actually sending

139
00:14:09,760 --> 00:14:18,400
across the border might be difficult, I'm not sure, but unless it's dry, non-perishable

140
00:14:18,400 --> 00:14:27,440
goods, it's not going to really work, so I think in this instance we're going to have

141
00:14:27,440 --> 00:14:35,840
to be systematic about it, sorry anyway that's not quite, so the point is to how the system

142
00:14:35,840 --> 00:14:40,640
should be set up for a terabyte of monks, I mean ideally the monks should go on arms round,

143
00:14:40,640 --> 00:14:45,960
we had to set up here and we could yet do that again, I think the problem is here in

144
00:14:45,960 --> 00:14:51,920
Hamilton, you know it's not even a problem, we could do this, we just have to, you know

145
00:14:51,920 --> 00:14:58,000
I have to get, I have to push the head monk and we have to do this, we drive to their houses,

146
00:14:58,000 --> 00:15:03,400
we used to do this, we did this for a while, we got in the car with our bowls and everything

147
00:15:03,400 --> 00:15:10,040
and someone drove us to people's houses, every day we actually had a schedule set up,

148
00:15:10,040 --> 00:15:14,440
this day we go to this house, this day, every day we would go to someone's house and

149
00:15:14,440 --> 00:15:17,800
they would have food there, it's actually quite easy because there's Buddhists all over

150
00:15:17,800 --> 00:15:25,760
the city even here in Stony Creek, within the not such a wide range, we can find many

151
00:15:25,760 --> 00:15:32,640
people who are just, would love to offer us food, it's quite inspiring to see how people

152
00:15:32,640 --> 00:15:37,680
are, how keen people are to do something, and if you consider for many people that's

153
00:15:37,680 --> 00:15:45,680
their only connection with the religious life is to give charity and to have that

154
00:15:45,680 --> 00:15:53,600
aspiration of supporting Buddhism, would you bring the meditators with you? No, no I wouldn't

155
00:15:53,600 --> 00:15:59,400
do that, that's the thing that you know I'm wondering because beyond the monks you're

156
00:15:59,400 --> 00:16:05,800
establishing a meditation center and they have to eat too, well we wouldn't eat in these

157
00:16:05,800 --> 00:16:12,120
people's houses, we'd collect a binto, we'd need to call these things the stacked plates,

158
00:16:12,120 --> 00:16:23,320
it's a very big thing in Asia, it's stainless steel, full of food and bring that back

159
00:16:23,320 --> 00:16:27,000
to the monastery and eat, we never would stay at their house, or rarely ever would eat at

160
00:16:27,000 --> 00:16:36,040
their houses, another way is we've gone to restaurants, we have standing invitation

161
00:16:36,040 --> 00:16:46,640
at a Vietnamese restaurant, Cambodian but international restaurant, and at least one other

162
00:16:46,640 --> 00:16:56,360
restaurant, so we can always go there, we can always do that, but this requires coordination,

163
00:16:56,360 --> 00:17:05,160
vehicles, you know, driver, that's not entirely possible, but the point being that requires

164
00:17:05,160 --> 00:17:11,440
to your question, the ideal is to approach those people, to not be any inconvenience

165
00:17:11,440 --> 00:17:19,640
to them, the key is this verse where the Buddha said the wise ascetic is like a bee,

166
00:17:19,640 --> 00:17:24,000
the bee goes to the flower and takes something very valuable but not to the flower, the

167
00:17:24,000 --> 00:17:31,720
flower doesn't miss the pollen and that's crucial, you know, it shouldn't be painful,

168
00:17:31,720 --> 00:17:37,640
it shouldn't be an imposition, the flower actually wants the bee, it wants the bee to

169
00:17:37,640 --> 00:17:50,440
take the pollen, it benefits and that's key, it shouldn't benefit, so barring the

170
00:17:50,440 --> 00:17:57,160
arms round, there are ways of setting up systems like they do in Sri Lanka, of people

171
00:17:57,160 --> 00:18:06,520
bringing food daily to the monastery, that also happens, not as inspiring as the arms

172
00:18:06,520 --> 00:18:11,480
round, something we should keep in mind, but I don't think it's going to work here because

173
00:18:11,480 --> 00:18:17,080
I have to deal with meditators and so it puts you, being a meditation teacher always puts

174
00:18:17,080 --> 00:18:22,480
you in a special situation, and even in the windy it's mentioned, special circumstances

175
00:18:22,480 --> 00:18:29,280
for someone who is teaching, they, you know, you have to treat them differently and so

176
00:18:29,280 --> 00:18:35,440
as a teacher, I have to, you know, I can't have the luxury always of living my life the way

177
00:18:35,440 --> 00:18:42,400
away, an ordinary monk, an ordinary monk who's not in that position, I mean, I've had

178
00:18:42,400 --> 00:18:47,320
to deal with that many times, you know, whenever, when I go to, when I'm traveling, I live

179
00:18:47,320 --> 00:18:52,000
as a monk but when you get in the position of a teacher, things change and this is why you'll

180
00:18:52,000 --> 00:19:00,880
see the head of monasteries, often live his life quite different from the, this, the ordinary

181
00:19:00,880 --> 00:19:09,320
or the non-official monks, monks who don't have official capacity, non-teachers, it's not

182
00:19:09,320 --> 00:19:17,520
even teachers, it's having an official, being a monastic official, you tend to have certain

183
00:19:17,520 --> 00:19:23,000
duties, that kind of thing.

184
00:19:23,000 --> 00:19:26,760
So that, that, there's that complication that not all monks are in the same position and

185
00:19:26,760 --> 00:19:31,600
you shouldn't criticize the head monk because he doesn't go on on John, he can't, he has,

186
00:19:31,600 --> 00:19:35,800
often he can't, he has other things he has to do and it's just not convenient and not

187
00:19:35,800 --> 00:19:40,240
suitable for him to do that, okay, anyway, enough time.

188
00:19:40,240 --> 00:20:09,800
Thank you very much.

