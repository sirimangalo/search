1
00:00:00,000 --> 00:00:26,320
Okay, good evening, everyone, welcome to our evening dhamma, hmm, it didn't work.

2
00:00:26,320 --> 00:00:38,240
Okay, turning again, looks good.

3
00:00:38,240 --> 00:00:53,920
So tonight I was thinking about a verse, a dhamma panda verse, I think, that arogaya paramalabha,

4
00:00:53,920 --> 00:01:02,480
freedom from sickness, freedom from illness is the greatest gain.

5
00:01:02,480 --> 00:01:11,800
Santu tea paramang dhanang, contentment is the greatest wealth.

6
00:01:11,800 --> 00:01:29,080
Every sasa paramang dhanang dhanang, the familiarity is the greatest relation.

7
00:01:29,080 --> 00:01:49,480
I'm thinking about this verse, you can really get a sense of the Buddha saying this, looking

8
00:01:49,480 --> 00:01:57,480
out on the community of monks, living in the forest, reflecting on all that he'd been

9
00:01:57,480 --> 00:02:14,280
through to become a Buddha, and expressing his, expressing his perception, his outlook

10
00:02:14,280 --> 00:02:20,920
on the world in this way.

11
00:02:20,920 --> 00:02:26,480
Arogaya means not having rulga, rulga means that would root chati, that would disturb

12
00:02:26,480 --> 00:02:45,880
you, afflict you, affliction, it's the greatest gain, the greatest possession, the greatest

13
00:02:45,880 --> 00:02:57,720
gain, actually, lava means gain, ask anyone who's physically sick, physically ill, that

14
00:02:57,720 --> 00:03:12,960
they want money, no fame, glory, mansion, power, all the riches and all the world won't

15
00:03:12,960 --> 00:03:16,000
save you from sickness.

16
00:03:16,000 --> 00:03:23,200
Four kinds of sickness in Buddhism, there's sickness that comes from food, sickness that

17
00:03:23,200 --> 00:03:31,560
sickness that comes from the environment, sickness that comes from karma, and sickness

18
00:03:31,560 --> 00:03:36,640
that comes from the mind.

19
00:03:36,640 --> 00:03:43,240
So, actually, it's quite likely that the Buddha wasn't thinking of physical illness when

20
00:03:43,240 --> 00:03:51,640
he said this, and I think we can all agree that if you're physically unwell, all the

21
00:03:51,640 --> 00:04:04,160
material possessions in the world are not going to compare to the gain of becoming free

22
00:04:04,160 --> 00:04:11,400
from that sickness, spend a lot of money and a lot of effort just to free ourselves

23
00:04:11,400 --> 00:04:18,320
from illness, everything else becomes meaningless because you can't of course enjoy any

24
00:04:18,320 --> 00:04:25,000
of the pleasures or riches or gains, accomplishments in life, physically ill.

25
00:04:25,000 --> 00:04:32,160
But I don't think that was exactly what the Buddha was talking about, I mean, it actually

26
00:04:32,160 --> 00:04:40,400
isn't the greatest gain, not in the Buddhist sense because you have to understand sickness

27
00:04:40,400 --> 00:04:47,760
of the mind, you have to understand the full range of sickness and person can be perfectly

28
00:04:47,760 --> 00:04:56,680
healthy in the body, but if their mind is ill, physical body, physical health does them

29
00:04:56,680 --> 00:05:11,160
no good either. So, sickness from food, sickness from the environment, these are physical.

30
00:05:11,160 --> 00:05:17,040
Sickness from karma, well, that can be physical, can be mental, but sometimes we're

31
00:05:17,040 --> 00:05:26,440
born with diseases, with illnesses, physical illnesses, some people are born deficient

32
00:05:26,440 --> 00:05:36,120
in some way in the body, some people are born with all kinds of illness, some people

33
00:05:36,120 --> 00:05:44,440
die, some humans die shortly after childbirth because they're so ill.

34
00:05:44,440 --> 00:05:50,560
This can be because of karma, I mean, sickness because of food is quite obvious, you

35
00:05:50,560 --> 00:05:55,560
eat too much, you eat the wrong kinds of food, this is quite clear, and the environment

36
00:05:55,560 --> 00:06:08,520
could be anything from bacteria, viruses, infections, radiation, perhaps, chemicals, everything's

37
00:06:08,520 --> 00:06:17,600
got these terrible chemicals in it, making us all sick. But karma, karma, that one's

38
00:06:17,600 --> 00:06:23,520
not so clear, karma is the Buddhist answer to why we're born certain ways, and in fact

39
00:06:23,520 --> 00:06:28,600
it's not even karma, not just karma, right? Because while the environment can have a

40
00:06:28,600 --> 00:06:36,040
factor, or play a part, karma certainly plays a part, but it's much more complicated

41
00:06:36,040 --> 00:06:44,240
than that. Sometimes we perform certain karma and as a byproduct, bad or good things

42
00:06:44,240 --> 00:06:49,120
happen that are totally, I mean, things that we like, but we're totally nothing to do

43
00:06:49,120 --> 00:07:00,280
with the karma, right? Like, perhaps you did some very bad things and as a result, you

44
00:07:00,280 --> 00:07:09,520
have this bad karma in your life, but as a result, let's say a person who is very poor

45
00:07:09,520 --> 00:07:16,880
and so they have to go and live off in the countryside, and then there's a fire that

46
00:07:16,880 --> 00:07:23,600
burns down the city, right? I mean, just the point is complicated is the point, but karma

47
00:07:23,600 --> 00:07:33,880
certainly plays a part in this life and from life to life as an extension. But it's,

48
00:07:33,880 --> 00:07:47,880
I think, I think we all...

