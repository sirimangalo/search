1
00:00:00,000 --> 00:00:26,360
Good evening, everyone broadcasting live December 29th, 2015, we have a little over two

2
00:00:26,360 --> 00:00:43,160
days left in the year. I don't worry, there's another year to come. I had an argument with

3
00:00:43,160 --> 00:01:12,840
my youngest brother about rebirth, stubborn. He said,

4
00:01:12,840 --> 00:01:32,080
I can't remember. We'll see who's right when we die. We'll see who's right. It's going to be a shame for him

5
00:01:32,080 --> 00:01:46,880
if there are consequences to his actions. He can't escape them by dying. He said,

6
00:01:46,880 --> 00:01:53,880
well, yeah, if I'm right, you'll have to deal with the consequences. If I'm right,

7
00:01:53,880 --> 00:02:03,040
and I still don't know, actually, if you're right, there are no consequences. If you're right, it'd be

8
00:02:03,040 --> 00:02:09,560
great. It means we can do whatever we want, but it's quite a dangerous, quite a dangerous belief,

9
00:02:09,560 --> 00:02:22,080
belief that belief in death is a dangerous belief because of the consequences of it. Even in

10
00:02:22,080 --> 00:02:37,640
this life, the consequences are less likely to do good deeds, more likely to do evil deeds. Even if

11
00:02:37,640 --> 00:02:47,640
it was true, it would be better for us to believe that there were consequences. It would be

12
00:02:47,640 --> 00:02:56,840
better for the world. That's a funny situation. If it were true that when we died, there was nothing.

13
00:02:56,840 --> 00:03:05,720
It would still be better for all of us and for the world and for society, for us to believe that there

14
00:03:05,720 --> 00:03:23,480
were consequences. Before I left this morning, I said, so we'll have a safe travels, and I said,

15
00:03:23,480 --> 00:03:45,200
thank you. I said, so I'll see you. If not in this life, then I'm in next. We laughed. So I had one

16
00:03:45,200 --> 00:03:53,320
announcement today that seems a lot of people didn't know about the appointment schedule that we

17
00:03:53,320 --> 00:04:01,800
have. If you go to meditation.ceremungalow.org, there's a meet page, and it's got slots where you can sign

18
00:04:01,800 --> 00:04:11,240
up for a meditation course. We use Google Hangouts, so you have to make sure you've got Google Hangouts

19
00:04:11,240 --> 00:04:24,480
installed and working, and then if you show up at the right time, you can meet with me, but you must be

20
00:04:24,480 --> 00:04:38,480
practicing at least an hour of meditation per day. We try to get you up to two hours, and you have to

21
00:04:38,480 --> 00:04:44,800
keep with this preset and stuff, but it's for people who are serious about the meditation practice and want

22
00:04:44,800 --> 00:05:08,360
to go the next take the next step. Right now we have nine slots available. Still, I just got

23
00:05:08,360 --> 00:05:19,320
back from Florida today. I'm not planning on sticking around too long this evening. Make a short

24
00:05:19,320 --> 00:05:25,560
night of it. Do you have any questions? We have questions. We have lots of questions, Bante.

25
00:05:25,560 --> 00:05:33,160
Let's skip to the good ones. Okay. Regarding meetings, is it required to have a camera and a

26
00:05:33,160 --> 00:05:38,760
microphone to make an individual meeting? I have social anxiety, so it's very difficult for me,

27
00:05:38,760 --> 00:05:46,120
and my English is bad too. Thanks. Yeah, we can do it just. You need a microphone absolutely,

28
00:05:46,120 --> 00:05:57,000
but I'd like to have a camera. I think it's important because normally you would come into the

29
00:05:57,000 --> 00:06:03,080
room and meet me, and we're trying to make it as close to that as possible. I really would

30
00:06:03,080 --> 00:06:15,080
like for you to have a camera. Every time I do sitting meditation, I seem to keep dropping off

31
00:06:15,080 --> 00:06:20,600
to sleep. This can be any time of day apart from walking meditation. What can I do to help with

32
00:06:20,600 --> 00:06:32,520
my sitting meditation? Well, walking meditation is good. What's wrong with that? You're mindful

33
00:06:32,520 --> 00:06:41,560
tired, tired helps, but probably has a lot to do with your lifestyle. You have to put up with

34
00:06:41,560 --> 00:06:54,600
falling asleep because of the state of your mind is in. During sitting meditation, I find it

35
00:06:54,600 --> 00:07:00,520
hard to knit thoughts that arise and sees quickly while I'm noting primary objectives. However,

36
00:07:00,520 --> 00:07:06,360
I am aware that this happens and it is interrupting my concentration. How it happens is that while I'm

37
00:07:06,360 --> 00:07:11,560
waiting for the rising, I thought I'll manifest and see and I will go on noting the rising.

38
00:07:12,200 --> 00:07:17,640
My question is, should I in this case be noting thinking, thinking after the fact,

39
00:07:17,640 --> 00:07:23,320
or should I note distracted? If so, how many times should I note before returning to my objectives?

40
00:07:24,200 --> 00:07:30,360
You can, just once, where you can ignore it's knowing a couple of times.

41
00:07:30,360 --> 00:07:37,240
There's no, it's not magic or something. Just do it as it seems appropriate.

42
00:07:39,720 --> 00:07:41,640
Knowing would be knowing that you were thinking.

43
00:07:47,000 --> 00:07:51,480
Is there a certain time of day in which meditation is best to be practiced in the morning,

44
00:07:51,480 --> 00:08:02,280
for instance? How would you convince a non-butters that reincarnation is real?

45
00:08:03,320 --> 00:08:09,160
Is there a way one can know who one was in a past life and thank you so much for taking the

46
00:08:09,160 --> 00:08:15,240
time to answer? Yeah, I'm going to skip that one. Okay, it sounds like you've already had that.

47
00:08:16,200 --> 00:08:20,200
I just wanted to say about consequences. You don't even have to wait until the next life

48
00:08:20,200 --> 00:08:24,680
for the consequences. I mean, you feel them right now when you try to do anything.

49
00:08:25,240 --> 00:08:29,480
Yeah, I was arguing that with the many said, I get that. He said, my younger brother said,

50
00:08:29,480 --> 00:08:37,000
he knows that, but he said, but then I said, but you think that there are no

51
00:08:37,000 --> 00:08:56,840
content in the end when you die, you get that Scott friends. When I'm sitting, I know sitting,

52
00:08:56,840 --> 00:09:02,600
sitting, but how far do I need to go? Do I need to be mindful of the pressure below my butt

53
00:09:02,600 --> 00:09:08,520
or just today or just know that I'm sitting? Is sitting a concept? When my abdomen is rising,

54
00:09:08,520 --> 00:09:14,360
I note rising, rising, but do I need to follow the rising of the abdomen at every position?

55
00:09:15,080 --> 00:09:20,840
Thanks. Yeah, you should follow the rising from beginning to end. You're not just saying,

56
00:09:20,840 --> 00:09:27,480
and it's not just words in your head. Sitting as well, sitting is an expression of the

57
00:09:27,480 --> 00:09:34,680
sensations. So yeah, you should be aware, but you don't have to focus on it. The only reason

58
00:09:34,680 --> 00:09:44,280
you know that you're sitting is because of the sensations in your back and in your bottom.

59
00:09:51,800 --> 00:09:55,880
Is there a difference between mindfulness achieved during meditation and mindfulness during

60
00:09:55,880 --> 00:10:01,720
everyday activities? Is mindfulness achieved in formal meditation practice more beneficial?

61
00:10:01,720 --> 00:10:02,600
Thank you, Montay.

62
00:10:05,560 --> 00:10:07,160
Mindfulness is mindfulness.

63
00:10:13,640 --> 00:10:18,120
Hello, Montay. I'd like to start off by saying thank you for all your teachings and dedication.

64
00:10:18,120 --> 00:10:22,920
They truly have helped me put things in better perspective. I'll be taking a couple of college

65
00:10:22,920 --> 00:10:28,840
classes in a city called Sioux Falls, South Dakota, starting February 2016. And I'm thinking of

66
00:10:28,840 --> 00:10:34,440
forming a small meditation group on campus with interested individuals. If you'll be willing to meet

67
00:10:34,440 --> 00:10:39,320
with us for about an hour or less once a week via Skype or another form of video communication,

68
00:10:42,120 --> 00:10:46,760
I think that was a question. I'm not certain of the number of people who will be interested

69
00:10:46,760 --> 00:10:51,560
in such a group, but if any, it would be wonderful to have you guide us along this path to freedom

70
00:10:51,560 --> 00:10:52,280
from suffering.

71
00:10:58,040 --> 00:11:03,320
So I think the question was would you be potentially able to do such a thing?

72
00:11:05,320 --> 00:11:09,720
I don't know. Probably not.

73
00:11:12,760 --> 00:11:14,040
I'm doing a lot already.

74
00:11:14,040 --> 00:11:26,040
You just go according to the booklet and people want to learn more. They can contact me,

75
00:11:26,040 --> 00:11:27,000
they can do a course.

76
00:11:30,360 --> 00:11:32,200
They can join a room here as well.

77
00:11:32,200 --> 00:11:36,920
Great that you're doing and I think I want to encourage you in it, but I don't think I'd be

78
00:11:36,920 --> 00:11:42,840
I'm already doing too much.

79
00:11:44,760 --> 00:11:49,640
Hi, Bunte. I have a job interview in one week. I study a lot when I need to talk and have difficulty

80
00:11:49,640 --> 00:11:55,000
to breathe. Do you have any advice besides noting? And is it a good thing to imagine the interview

81
00:11:55,000 --> 00:11:58,280
beforehand and observe my reaction? Thanks.

82
00:11:58,280 --> 00:12:07,240
No, I mean, I teach meditation, so you know what I've got to offer. If it helps, take

83
00:12:07,240 --> 00:12:10,840
it. If it doesn't help, leave it.

84
00:12:20,120 --> 00:12:22,760
I guess this is a question. Do you know that you're just the best?

85
00:12:22,760 --> 00:12:26,920
I think that was a compliment, but I had a question mark.

86
00:12:31,320 --> 00:12:32,280
We're all worthless.

87
00:12:34,280 --> 00:12:39,240
Is it better to meditate in a group rather than alone? If so, why? Thank you, Bunte.

88
00:12:41,160 --> 00:12:44,760
You're always alone. You've never been a group.

89
00:12:44,760 --> 00:12:53,080
But more much of meditation is an artifice. It's artificial.

90
00:12:54,040 --> 00:13:00,920
So there's lots of artificial supports for meditation and one of them is having a teacher,

91
00:13:01,640 --> 00:13:09,560
being in a group. These are artificial supports. So yeah, I think they can support your meditation.

92
00:13:09,560 --> 00:13:15,160
Providing encouragement and so on. Lots of things that can support your meditation.

93
00:13:21,560 --> 00:13:26,840
I'm sitting every day in making progress. Is there any benefit in doing your one-on-one course?

94
00:13:26,840 --> 00:13:29,560
Or is it just for those who need advice? Thank you.

95
00:13:30,840 --> 00:13:36,680
Why, if you're just practicing according to my booklet, that's only the first step.

96
00:13:36,680 --> 00:13:43,080
And so yeah, we take it through the steps and we guide you through the course.

97
00:13:44,440 --> 00:13:48,120
So I would say there's a lot of benefit in taking an online course.

98
00:13:56,600 --> 00:13:58,680
It's just the very beginning.

99
00:13:58,680 --> 00:14:04,280
And with that, you've made it through all the questions.

100
00:14:15,720 --> 00:14:21,960
So we have tomorrow. We might do Dhamma Padha probably.

101
00:14:24,840 --> 00:14:25,720
Should be okay.

102
00:14:25,720 --> 00:14:31,080
And then on Thursday, we have the last day of the year.

103
00:14:33,160 --> 00:14:34,920
I'll be here. We'll see who shows up.

104
00:14:41,560 --> 00:14:43,080
Thanks for your muffins, Bhante.

105
00:14:44,120 --> 00:14:44,840
What?

106
00:14:44,840 --> 00:14:47,640
Thanks for your muffins, Bhante. Are you giving out muffins?

107
00:14:48,360 --> 00:14:48,600
No.

108
00:14:49,320 --> 00:14:49,640
Okay.

109
00:14:49,640 --> 00:14:51,560
Okay.

110
00:14:54,600 --> 00:14:55,480
Muffins.

111
00:14:55,480 --> 00:14:57,000
Someone thanked you for your muffins.

112
00:15:02,120 --> 00:15:03,080
I don't get it.

113
00:15:03,080 --> 00:15:03,880
You don't get it.

114
00:15:04,520 --> 00:15:05,240
I think it's a joke.

115
00:15:07,880 --> 00:15:08,360
Maybe.

116
00:15:08,360 --> 00:15:19,240
And anyway, good night, everyone. Good night, Bhante.

117
00:15:20,040 --> 00:15:20,840
See you tomorrow.

118
00:15:21,560 --> 00:15:22,120
Thank you.

119
00:15:22,120 --> 00:15:38,760
Yeah. Thank you.

