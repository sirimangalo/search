Okay, good evening, and welcome to our live broadcast, October 16th, 2015.
Yesterday I was back in Stony Creek, so didn't work out, I've moved most of the stuff
over here, so I didn't have the required equipment really to do a live broadcast, but that
will only be one more week next week, I'll be going again, and that should be the end of it, I think.
How's my volume on my loud enough? Yes.
Okay, so we're going to do a demo pad at tonight, we have another short one.
So, it's a good one, so testing, I don't know.
It would be good if we had a memory card, let me see.
Hello, and welcome back to our study of the demo pad, today we continue on with first number 90,
which reads as follows.
We're going to do a demo pad, and we're going to do a demo pad, and we're going to do a demo pad,
which means we've got a demo one who has completed their journey, completed the going.
We saw Casa, one who is free from sorrow.
We promote Casa, somebody, one who is liberated everywhere, it is everywhere free, everywhere they go.
Sabagantat, Bajinas, one who has abandoned all fetters, all forms of clinging
every object of attachment, Bali-la-ho nui-jati, no fever can be found.
No distress, distress is probably the best translation, Bali-la-ha.
I've got Dandino, we saw Casa, very beautiful little verse, it talks about the fever,
Bali-la-ha is often translated as fever, but in this specific instance, because of the story,
and because of the context of the verse itself, it's not a fever, it's mental,
fever, mental, burning, so we would translate it as distress or fever of passion,
but literally it means burning a thing, daa, daa, putting daa-ha becomes Bali-la-ha.
So the story, this was told in regards to a question posed by Devaka,
it was a doctor, and the doctor looked after the Buddha, and his story is not told here,
I can't remember if it's actually told in the in the demo pod, I don't think so, maybe in the
jatika it occurs, but in the vinaya, definitely you find his story. He was a master doctor, an exceptional
medicine man, they say that he had the final task given to him by his teacher,
was to stake out a 16 kilometer by 16 kilometer square piece of land, which is a league,
a square league, nojuna of land, and bring to his teacher every plant in the entire square league
that had no medicinal properties.
And this was the task, so he set out to accomplish this task, came back some time later and said
to the teacher, there are no such plants, there are no plants in that 16, I couldn't find any
plants that were without medicinal properties, and that's how his teacher knew that he passed,
because he was able to identify medicinal properties in all plants.
It's the story, so he was exceptional, he got, he caused a little bit of trouble,
or he got into a little bit of trouble, not of his own, not on his own, he was actually an awesome
wonderful person, he donated a mango grove to the Buddha, and you can still go and see
the place that's supposed to be where this mango grove was, right at the bottom of
Vulture's Peak, because Vulture's Peak was difficult to go up constantly to take care of the Buddha,
so in order to make it easier for him to take care of the Buddha when the Buddha was sick,
and also to make it easier for the Buddha to go on Armstrong when the Buddha was ill,
he gave him this mango grove at the bottom of the hill, the bottom of the mountain.
But as he was looking after the Buddha, he also took on the role of looking after the monks,
and as a result of taking, paying so much attention, taking so much time to look after the sangha,
his other patients started to miss him, and so people who were sick would actually ordain his monks,
just to try and get this level of care, and this was actually the reason why the Buddha
instated the role that someone who is sick, someone who has this sickness, has any
terrible sickness, shouldn't be allowed to ordain. I mean, there are other reasons for it,
obviously it's problematic, but it's one of those things that leads people to ordain for the
wrong reasons, and this happens in Buddhist countries where they become lax about this,
and really because people become a burden on the monastic sangha rather than actually
promoting Buddhism and doing good things, so there's a few stories about him.
But this story actually concerns Davidata in tangentially, because the story goes that
Davidata was trying all sorts of ways to kill the Buddha. Davidata was the Buddha's cousin,
and he got upset and jealous of the Buddha, and he wanted to be the leader of the sangha,
just kind of an all-around mean and nasty sort of fellow.
So he tried, he sent an elephant after the Buddha, and the elephant, the Buddha tamed the elephant,
it was his mad elephant that was supposed to trample the Buddha, and the Buddha tamed him with loving
kindness. He sent some archers after the Buddha, some mercenaries after the Buddha, and they all
converted to become monks, and finally he just got fed up, and as the Buddha was coming down
from Vulture's peak, he dropped a rock, a big boulder on the Buddha, trying to crush him
from up on high, and as the rock was falling down, it hit some sort of outcropping, of course,
because you can't call the Buddha, it just doesn't happen. His karma is too good,
and but a little splinter, a small splinter, broke off to the rest of the rock,
geared out of the way, and didn't come close to the Buddha, but a small splinter came and hit
the Buddha's foot, and Jivaka looked after the Buddha, and the Buddha laid down, and Jivaka
bandaged him up, put some pulters on the wound. I think the commentary has tried to say that
he didn't actually bleed, because it's impossible to make the Buddha bleed, but it's about
the translation as to what exactly as meant to happen anyway. Whatever, the Buddha was injured,
he put some herbs on it, some something strong that would take the pain away and make the
swelling go down, but it was quite strong, and then Jivaka had to go into the city to look after
his patients in the city, and he was late, I was looking after some rich person, or maybe even
the king, and he was late coming back and they closed the gates to the city before he could get out,
and so he had to stay all night in the city, and he was worried all night, and concerned,
because he had to go back and take this pulters off of the Buddha's ankle, or else it would cause
harm, it would actually cause harm to the Buddha, and so he was unable to let the Buddha know.
Fortunately, that's the benefit of having all sorts of supernatural powers. The Buddha was
able to discern that it was time to take the pulters off and took it and had Ananda help him take it
off, and waited there for Jivaka to come, so no harm done, but harm that was done to Jivaka,
who was worrying and fretting, who was distressed. Jivaka was actually a sotapana, I think at
this time he was already a sotapana, though I'm not sure of the actual timeline to have to look
deeper into that, but I think he was already a sotapana at this point, so he was already an
Aria Bukhala, but he was still subject to distress, and so he friended about this for the long time
that he was kept in the city, and finally when the gates opened in the morning he ran over,
ran, sprinted his way over to Vulture's Peak, and came to the Buddha and asked him,
I'm sorry, the noble surah, that I wasn't able to come, were you distressed by the pain,
were you distressed by your illness, and that's where this verse comes from. Jivaka asked him
the simple question, and the Buddha said, for one who has gone to the end, who is free from sorrow,
one who is free everywhere, gone to live, who has become liberated everywhere,
who has abandoned all ties, Pernilahou namitati, there's no fever to be found,
no distress to be found, the word fever is too ambiguous, but
whenever becomes distressed, so this was, I think last night we had the question whether an
Arahan can suffer, whether it can actually be called suffering, when an Arahan feels pain,
because they don't have, and this is what they don't have, the translation that we've got in the
book actually says it translates, but Arilahah is suffering, but that's probably not
accurate, it's again too ambiguous, to general, the specific meaning is a
burning up, so it's often referring to the body, but here it means burning up in the mind,
were you distressed, did it distress you, upset, did it upset you, were you upset by it,
were you distressed by it, were you vexed by it, and the Buddha said there is no vexation for one
who has become free, and so that's the answer to that question, that they do actually feel
nook away in the body, they're not vexed by it, a very important point that all of us as
meditators, hopefully by this time have heard or have come to be familiar with, but it's a point
that has to be made especially for people who are new to the meditation, the difference between
not only physical suffering, but external conditions, like the state of Giwaka who was stuck in
the city was incredibly vexed and disturbed and distressed, but he had a good heart, but he wasn't
helping the Buddha by being vexed and distressed by being upset, and in fact he was simply doing
damage to his own mind, and so to some extent the Buddha was using this as a lesson to Giwaka as
well, not only to reassure him, but also to remind him, not to get upset, I wasn't upset,
why did you get upset, and so this is because of the difference between the physical, the external
and our reactions, pain doesn't have to be a bad thing, excruciating pain doesn't have to be a bad
thing, situations don't have to cause us anger and frustration and upset or boredom or worry or fear,
you know, when people do things to us, when others hurt us, we do the worst thing when we respond
with anger, when someone gets angry at us and we respond with anger, we're the worst,
we are doing, we are creating the worst evil, what is it,
but this is a clear indication of the difference between our experiences and our reactions to them,
so what do we have, one who has gone to the end means, actually I don't know,
get Adino, that's translated as one who's gone to the end of the journey,
well that's what they say here, but let's see exactly what it means,
Adino, one who has gone the distance, so who has completed the path,
I think it meditators asking how you know how far you're progressing and it's all this idea of
the path and I'm often cynical about whether we should really focus too much on the idea
of a path, it's misleading I think, you know, it gets fine to talk about the idea of a path,
but when we are concerned about our progress, it can become quite obsessive as we're worried
about where we are, wondering when we're going to get there, that kind of thing.
On the other hand, having the idea of the path is a reminder that we can't be complacent,
that we do have to walk in journey, but it's important as a teacher and as a meditator to not be
too focused on progress, so this idea of the path or the journey is poetic and it's useful to
know that there is a journey, but when you're walking in journey, you don't want to be the person
in the backseat saying, are we there yet, are we there yet, right? That part of the mind should
be silenced, the walking has to continue. All you have to do is put one foot in front of the
other, you know that there's a path and you follow it. It's much simpler than we think, we think of
it as being some kind of a paved road with road signs telling you 10 kilometers left or something
like that, but it's not really. A path we've never gone and a path that has no clear signs that we
can distinguish because we've never, you know, we have nothing, no frame of reference.
So our job is yes to follow the path, but that simply means putting one foot in front of the
other metaphorically speaking. We soak a sad, so enlightened being has no soka, no sadness, no sorrow,
they don't long for anything, they don't pine away at loss,
they don't worry about about losing the things that they have, no soka, no sadness.
We promote us as somebody in all directions, on all sides, everywhere. We promote
free, liberated, completely liberated. Sabaganta, pajinas having abandoned or destroyed or
gotten rid of all vines or ties, severed all ties. So people talk a lot about detachment,
Buddhism is a clear indication of how Buddhism does
encourage the idea of detachment. But people get the wrong idea that it somehow translates
into a sense of, or a state of zombie-like, nothingness or meaninglessness.
All it means is to not cling to things as they go by because the problem is we have this idea
that things exist constantly, or in some stable form, and so we're talking about
shying away from them, that people think non-attachment means it's there, don't go near it,
but that's not it, it's not there actually. What is there is a moment, and that arises and
sees it, that's fine, be as close and as clearly aware of that as you can, that's the point.
But when it's gone, don't go with it, don't let it pull you into the past,
don't let thoughts about the future, pull you into the future, and you end up like this
pulling being pulled to both sides past and future, and you're never really here and now.
That's what non-attachment means, it would make sense if there were things here that were stable
to attach to them, because you could depend upon them, but there's nothing like that.
Nothing is of that sort. Everything arises and sees as comes and goes uncertain,
even all the possessions that we have, that we think we have constantly throughout our lives,
the people who are with us. The reason we're so sad is because we take them as
some stable entity that can be controlled and that is long lasting and so on.
So we bind ourselves to them with this attachment,
non-attachment or detachment, non-attachment, some better way of phrasing it.
Non-attachment means experiencing, but experiencing as experience, seeing people as individual
experiences, seeing things as individual experiences, because that's what they are. That's what you
really have. How can you say you have a car when you don't see it, when you don't hear it,
when you're not in front of it, when it's not in front of you? How can you say that you even have
people in your life, when they're not here and now? All you have is a thought that arises and
sees as even when they're with you, all you have is seeing, hearing, smelling, tasting, feeling, and thinking.
Non-attachment just means seeing things as they are and being with things as, so being with,
there's people around you, there's people around you, there's no one around you, there's no one around you.
It's not pining away after something that isn't there or clinging to something that is.
With such a person who is of this sort, there is no fever, so what it means is they have no
likes or dislikes, they have no vexation, no upset when things don't go as expected because they
have no expectations, they're perfectly at peace and perfectly flexible, flexible is a very
important aspect, just naturally flexible because they have no preference,
they're content the way things are, however they are, so that's the dumbapada for tonight,
thank you all for tuning in and keep practicing and be well.
So Robin, do we have any questions?
Mr. Second, I'll turn you off, okay.
Hello, Ponte. I have a question regarding livelihood. The money interests of the banks in my
country are so high because of the bad and unmanaged economic situation that some people live their
lives with the interests that they get from investing money in the banks. Do you think it is
right livelihood because one is not really working for the money that he gets by considering the
fact that the invested money was worked for? No, I don't think that's wrong livelihood.
No, I don't think it's that's an issue of wrong livelihood. You're not actually taking money,
you're being given money. That's different. If you go out of your way to like, let's take a similar
example. So I would say that's my answer is that it's being given to you. Now let's take the
example of royalties on copyright. I have no problem with that. If people give you money
for each book that is being sold that you produce, that's not a problem. But when you go out of
your way to prevent other people from copying that work, so you can make money, so you can make
more money, that I have a problem. So it's a question of that. Now you have to ask yourself
them, am I doing something to prevent other people from gaining this sort of, you know,
state of well-being? It's funny, every time I talk it, the static arises, wonder what's causing
that. I'm not sure, you sound a little louder. You started out kind of normal, it seems to have
gotten a little louder. Every time I talk there's static, there's like in the background.
I don't hear it, but I don't have headset on either. Maybe I need a headset on. What if I
use a headset to go down? No, because it's nothing to do with the speakers.
I think it may be something to do with the noise cancellation.
So yeah, I mean, if somehow that means that you are, like if in order to attain that state,
you had to manipulate the markets or manipulate the economic structure or you had to lobby
for a low capital gains tax, so that kind of thing. Like what rich people do to try and
Jerry Reek, the elections and lobby their politicians to get their capital gains tax
Lord. Like I think in the US, the capital gains tax is absurdly low. That's what some people say,
which means if you are living off of your investments, you pay very little taxes on it.
You pay less taxes than ordinary people. And that's, I understand. I mean, I'm not an economist,
but I understand. Some people say that that's been caused by simply lobbying rich people have
all the money, so they use that money to influence politics and then they get away with it.
And they're now obscenely rich. Some people say, so I'd say that aspect of it is wrong livelihood.
But simply living off interest isn't wrong. I would say that the fact that you can,
it's not wrong livelihood. It's probably untenable in the long run. Like it's not something you
could continue and do so ethically. Because of the need to give to the other people, you know.
Now let's suppose there was a society where everyone was taken care of.
Because you see somehow, if you're taking money from the society, somehow I have a responsibility to
help with the running of it. I don't know. I mean, it's a very complicated issue at that point.
But I'd say, no, you can't say that that's wrong livelihood. But I think there's going to come
situations where you're going to be sort of having to give it up. I mean, there are other
examples like Social Security, where you've worked for a long time when you have a retirement plan.
And that's the sense that you've done your job and society says, well, this person has done
their part and now we take care of them. You know, they have, they worked for this money.
You know, as I understand, the money has gone into, you've been taken out of your paycheck or
something like that. I don't really know how it works. But then you're given good interest on
or something. In Canada, we have that. My dad gets now not much, but he gets something every month.
So that does an example, I think of living off interest that is, so you see it's how it's
societally acceptable. But if you're doing it off the backs of people who are just barely able
to scrape by and who are treated terribly unfairly, then you're still not convinced that it's
wrong livelihood, but it's maybe you'd have a new category of wrong livelihood, but it's more
like unethical, because there's no way you can live with yourself reasonably and do it off the
backs of others. You know, I mean, I don't think it's certainly not considered wrong livelihood to
be richer than other people, but there is something to being unfairly rich, you know, to the point
where you're doing it at the expense of others. As I've said, like in cases where income disparity
is just so absurd that it's clear that there's no reason for this society as unfairly. When do
you say unfairly gamed in favor of rich people? That kind of thing. So I'd say definitely something
to be mindful of. Ultimately, as I said, you know, the best is the only way you can be truly
curious to give up everything. Read the way Santa Rajataka, it's an incredible story of this guy
who did just give up everything. He gave up his kingdom. He gave up his belongings. He ended up
giving up his wife and children, gave to the extreme. Like literally put himself at the mercy of the
universe and just was content with what ever happened. I mean, the body sat and did this in so
many lifetimes, gave up everything and just let what happened happened. If he got leaves to eat,
you know, fruit, wild fruit in the forest, that's what he would eat. That's the only way to be
truly, that sort of thing is the only way to be truly pure. So you don't have to be too extreme
about it. But definitely there are issues, societal issues that do involve morality, ethics,
and are very important things for Buddhists to talk about. Again, that's why I'm not just hearing
some of the things that certain politicians have been saying, well, one certain politician
that I've been kind of interested in has been saying, whether right or not, the intention seems to
be good to change these sorts of things. And I think as Buddhists, we have to say that's part of it.
How can you live with yourself knowing that you are obscenely rich and the system has been
gameed in order to allow you to get so obscenely rich without any benefit? So the system just
has to be fair. I don't think there's anything wrong with being born with a trust fund or that kind
of thing. I think in the long term and in light and being when give up a trust fund or use it for
good purposes maybe. But it gets a lot worse than that depending on the society. So complicated
issue. But something in the end, as I've said before, a lot of societies issues simply have to do
with mindfulness. If you're mindful and you're aware of how it's affecting you and how you feel,
if you feel guilty about it, there's one of two reasons. Either you're overestimating the
situation, like you step on an end and you didn't realize it and then you feel really bad about it.
Or there's a reason, or you're doing something that's unconscionable.
Is it just me or does the alarm that sound that should sound in between walking and sitting
meditation at work anymore? I don't know, does it work Robin? It works. Okay, so this is the question,
if it doesn't work for you, let us know what type of thing you're using, like is it a computer
or is it a iPad or an iPhone? Because if it's an iPad or an iPhone, that's probably your problem.
If you can use Chrome, they have Chrome for iOS or Firefox, I don't think there's
Firefox. If it may be Chrome, try using a different browser, but let us know if it's not working,
what your device is and what your browser is, and we can look into it. But if it's working
in Firefox on Linux, then that's what you should all be using. I think it maybe works on Android as
well, but on Android, we have an app. And hopefully on iOS, we'll have an app soon as well.
What happened to that guy who was working on? It sort of disappeared. Maybe I should get in touch with you.
Yeah, I have noticed when I don't use the app, if I just try to sign in through the browser
on a phone, it doesn't work. Yeah, and Android, if I just sign in through the browser, the
alarm's not working, but it's not really a good reason to do that, since we have the app, so
it's not a big deal. Yeah, I mean, sound like that in a website, it's not easy to code, it's not really,
it's new. It's complicated because some browsers use MP3 files, some browsers use OG files,
it's a bit of a mess. So yeah, anyway, let us know. We'll take a look at it.
Mark, if there's anybody out there who knows about these things, please let us know.
Masaka Dagami is a once-or-turner in a human realm, correct?
What if there is no human realm to go to at the time of the physical death?
What kind of a question is that?
I mean, it's an honest question, but it's not really a meditative question.
I guess in a sense, what cares?
But I'm sorry, I don't mean to really make fun of the question, but maybe a software
command is an order. We really need the answer to that question.
It's a bit speculative, I guess it's the point. So I could give an answer, and there is a
probably fairly interesting answer, but I think it's a bit too speculative. Maybe we'll
pass that one. Maybe I'm missing something about how important that question, but we'll skip it for
now. Okay. During walking meditation, I got a foot cramp. It is very painful, but goes away
when you literally walk it off. Should I keep walking in no pain, pain, pain, or stop and stand
and not pain? Yeah, I mean, foot cramps are a bit interesting because they can, I don't know,
can they lead the injury? I mean, they certainly get worse if you don't shake them out, right?
Sometimes. So I would say it's reasonable to shake them out or do whatever it takes to get
it to overcome it. That's fine. But while you're doing it, just be mindful. That's probably better
than, you know, it's not just being stupid about pain and injury and that kind of thing. The Buddha
said one of the ways of overcoming of continuing on the path is by avoiding. Can't remember what
Paul is. We ask. Yeah, maybe we ask him on you. Yeah, remember by avoiding. Sometimes you have to fix
things. Sometimes. Don't just sit there and say, well, sometimes it causes problems for you
physically and mentally. Yeah, I think if it gets bad, you can probably fall. If you can't really
look your foot properly. Okay, my apologies. The question regarding the sake of the gummy with
no room on the human realm, it was retracted later. I didn't notice that. So the person asking
realized that he didn't really want to ask that. Hey, we've all asked. Yeah, sometimes they just
come out in a moment later. You say, huh? Why did I say that? Yep. Okay, better ask. I'm not
afraid to ask questions. Just be prepared that we might. We might have some choice words about them.
It's nothing personal.
Bhanti, how do you work on a defilement, which you don't have a lot of exposure to,
but you know can occur. Well, meditation is supposed to help with that. Meditation tests you.
That's why we do intensive meditation courses because they bring everything up.
But it can take years. You know, I mean, you're not going to get everything in one go.
It's more like layers of an onion. You slowly, slowly, untie one knot after another.
But it cannot be found. It's all based on simple building blocks. So eventually, it cannot
be found with quite simple exercises walking back and forth and sitting should show you everything.
But you don't have to go looking for the defilements. They will come.
Bhanti, during walking meditation, sometimes my mind stays with one foot. For example,
I note stepping right. But after that, my mind wants to stay with the right foot. So I note
knowing, knowing, sometimes I am mindful of the pressure on my right foot. Is that a good thing
to do or do I need to be mindful of my left foot? Sorry. Say again.
During walking meditation, sometimes my mind stays with one foot. For example, I note stepping
right. But after that, my mind wants to stay with the right foot. So I note knowing, knowing,
sometimes I am mindful of the pressure on my right foot. Is that a good thing to do?
Or do I need to be mindful of my left foot?
That's fine. There's no need to. So when that comes up, that's fine. Just be mindful of it.
And then eventually move your mind to the left foot and continue on.
In Pali, our visa pronounced as W such as Vipassana. Sometimes it sounds like you say Vipassana.
It's about halfway between a V and a W. But in modern times, it's more closer to a W.
But it should be more like Vipassana. Vipassana. Halfway between.
That's apparently how it should be.
Halfway between is hard. That takes some practice. I don't do that too well.
In Latin, it was like this. The V, the letter V that we have, as I understand it,
is actually supposed to be pronounced W. There's no W in Latin. It's the V.
So I understand.
Like the famous Caesar's famous Vinny Vide Wiki. I thought it was Vitchi, but it's Vicky.
Vinny Vide Wiki became my son, I conquered. Vinny Vide Wiki.
Vinny is part of the progress in the path to know our defilements more clearly and struggle
more with our desires. I have noted my wrong view about the beauty of the body, but it is very
difficult to change it. As well, it should be. You've had that desire and that attachment
for a long time. There you can see the depth of the problem. The analogy of a thousand miles,
the journey of a thousand miles, a step-by-step journey of a thousand miles isn't that far off.
So it just takes work.
But aren't certain things at least temporarily stable, like possessions? Wouldn't it be wise to
create a method to attach temporarily? No, that would not be wise. Aren't things certain things
at least temporarily stable? No, because the things don't exist.
It's a deeper point that's being made, not that my teapot might break some time. That my teapot
doesn't even exist. When the teapot breaks, the idea of the teapot can no longer be found,
and the experience is associated with the idea of the teapot can no longer be experienced,
but the teapot itself never exists. During meditation sometimes I sneeze. I note feeling
feeling and then no sneezing if I end up sneezing. Is this the proper way to deal with such
things when they arise? Sure. That sounds good. I'm just noticing here people listening are also
hearing the static. I don't hear it, but I think it's because I don't have headphones on right now.
It only is happening now when I talk. I can try a different mic, but it doesn't seem like that
would be the answer. It sounds very much like a Google thing. One thing they're doing that's a little weird.
Which politician have you been paying attention to? Bernie Sanders. Why? Because
he seems to care about people. I don't know. I'd be interested to hear if there are others out there,
but I never got this feeling from anybody else that I hadn't been told about or heard about.
I heard something, we've got an election here in Canada, and I heard something about the politicians
here, but I never really got the sense that they actually cared. I mean, I'm sure the
sum of them are nice people. Someone who has been fighting for a long time to help people. I
got this. I actually wanted to tell you all this story, and I don't mean to sound like promoting
anything, but no, I don't want to get into politics. But it was a good story, actually. It was a
story, this story about Sandra Bland. Is that her name? This woman who was pulled from her car
arrested and died in jail a few days later, sort of mysteriously committed suicide.
And
many made it. I'm not going to go into it. We'll get it. We'll get me sidetracked.
I live in a noisy city. Sometimes when I try to do things mindfully in daily life,
the noises distracting, and I try different things. For example, closing the windows,
using white noise, etc. Is this the proper way to deal with distracting noises?
No, I mean, it's like cheating a little bit, so if you want at first you can do that, but
that's not really the practice. In fact, are you even a meditator?
Yeah, someone with a yellow here. Yes. Some meditation. But yeah, if you've been reading my
booklet and following along, then that's not really what we're aiming for. When you hear the noise,
this noise in the background here, you acknowledge it, hearing. We don't like it. You've
knowledge disliking this like it. You're frustrated by it. You've knowledge frustrated, frustrated,
and you let it go. But it's a training. That's the training. That's what we're aiming for.
Not to otherwise you become. I can tell you a story when I started practicing.
I learned this quite well. I was given a alarm clock, and the alarm clock was ticking.
And I was doing, when I did walking meditation, I found myself sinking to the sound, lifting
here. Doing walking meditation in time with the timer and with the clock. That drove me crazy.
Day after day after day. Well, not day after day after, but in day of this, I shoved it under my
blanket and went to pillow on top of it and kept walking. At first, I could still hear it. The slightest
light is sound, but I could still hear it and it still drove me the lifting here, lifting,
placing to be in tune with the ticking. Finally, I've got enough blankets on top of it and the pillow
on top of it that I couldn't hear it at all. That way, whenever I needed it, I'd pull it out,
and then I'd put it back underneath everything. That means of avoiding it. I managed to finish my
course. I learned a lot in my first course. I don't think I succeeded terribly well, but learned
quite a bit. Then I did a second course. Before I did the second course, though, they moved me into
a room with two other people. It was a long room, but there were three of us, and there were
two clocks on the wall. They were ticking. By that time, I learned something and so I gave up,
and I remember just laughing at myself and saying, just realizing there's no way to
... I've been going about this all wrong, and so I let the noise continue. I did the walking
meditation anyway, and I let myself. What I did eventually is set to myself, okay, so you're in
sync big deal, so you're walking in sync, and I would just watch the movements in sync or not,
and eventually my mind started to let go, and it would just started walking as it was, and
ignoring the sound, or I would stop sometimes and say hearing or if I got frustrated about it,
and just work through it. You know, it's messy, but there's no trick. There's no way around it.
Anytime you find yourself looking for a trick to make it easier, you're going the wrong way.
You're headed in the wrong direction.
Could we have a FAQ to group together specific technical meditation answers?
Like, how do I note this? Maybe a Google document or something that can be edited by more people?
We have an FAQ that I put together some time ago. It's kind of buried, but I'm hoping they included
in another volume of my book, so added as an extra chapter of the first volume.
I don't know that it's complete, and there are probably always questions that could and
should be added to it, but that's something that would answer probably a lot of the questions
that we get here, and you can find it on the meditation page, hdm.seriblogo.org.
There are tons of meditation questions in the video wiki, as well, all sorted out by
different meditation aspects.
During meditation, I felt a pressure on my throat like a mind, like a mild construction.
I noted feeling, feeling, is there something to be concerned about or just know and move on?
Constriction, I think, is what they're mean.
Okay, that makes more sense.
No, being concerned about something is just sign of worry and worry is going to cause you
problems, so you would say worry and worry.
Just know and move on.
Nothing is worth clinging to, nothing is worth getting upset about.
Tina, pasted the demo, the video wiki in there, lots of meditation questions there.
Well, there's also the FAQ that I don't think many people have read
and it's useful. Yeah, that's a little hard to find. Well, it's right there.
It's probably should be up higher on the page above all the translations, but it's at the bottom of
the hdm.
Just to suggest you maybe we could put links in a nav bar.
Yeah, if you wanted to do the coding for it, go for it.
Join the group, join the committee.
Yes, come to our volunteer meetings on Sunday at 1600 UTC.
We have people working on technical aspects and food issues and administrative issues.
Always, more welcome, always.
What other than that, you're all caught up on questions, won't they?
Okay, let's stop there. Thank you all coming out. Thanks Robin for your help.
Thank you, Banthay.
Have a good night.
Good night.
