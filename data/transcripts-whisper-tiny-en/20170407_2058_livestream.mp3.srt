1
00:00:00,000 --> 00:00:12,400
And there won't be a live stream on there won't be a live YouTube tonight because I'm going

2
00:00:12,400 --> 00:00:39,400
to upload the high quality video later on.

3
00:00:42,400 --> 00:01:07,400
Okay, looks good.

4
00:01:07,400 --> 00:01:10,800
Hello and welcome back to our study of the Dhamapanda.

5
00:01:10,800 --> 00:01:18,800
Today we continue on with verse 158, which reads as follows,

6
00:01:40,800 --> 00:01:59,400
First, first one should set oneself in what is right.

7
00:01:59,400 --> 00:02:09,920
Then should one, then if one should teach others, the wise one will not

8
00:02:09,920 --> 00:02:23,480
sully the teaching or sully themselves, will not do something corrupt, will not be guilty.

9
00:02:23,480 --> 00:02:31,240
So set yourself in what is right first, then when you teach others, it won't be wrong.

10
00:02:31,240 --> 00:02:44,320
It won't be defiled by that, won't be marred by the fact that you're not practicing yourself.

11
00:02:44,320 --> 00:02:51,320
So this is the story based on the story of Upa Nanda.

12
00:02:51,320 --> 00:03:01,400
Upa Nanda was the sake of Buddha, he was a relative of the Buddha, and he became a monk.

13
00:03:01,400 --> 00:03:10,240
But he still had not only did he have some fairly greedy or luxurious tendencies,

14
00:03:10,240 --> 00:03:22,240
some attachments to comfort from being royalty, but he also seems to have been a little bit crooked beyond that.

