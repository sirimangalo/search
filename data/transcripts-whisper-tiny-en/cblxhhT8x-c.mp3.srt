1
00:00:00,000 --> 00:00:05,000
Hi, and welcome to my latest installment of Ask a Monk.

2
00:00:05,000 --> 00:00:10,000
So the latest question or set of questions comes from India.

3
00:00:10,000 --> 00:00:16,000
Would I in asks, when do we say someone is enlightened?

4
00:00:16,000 --> 00:00:20,000
Then what is the difference between a monk and enlightened?

5
00:00:20,000 --> 00:00:24,000
When do we say someone is enlightened?

6
00:00:24,000 --> 00:00:27,000
Well, we don't generally say when someone is enlightened,

7
00:00:27,000 --> 00:00:31,000
when we don't generally say when we are enlightened,

8
00:00:31,000 --> 00:00:36,000
if someone becomes enlightened, they generally wouldn't try to make that known.

9
00:00:36,000 --> 00:00:42,000
But the meaning here is, I think, how do you know if you are enlightened

10
00:00:42,000 --> 00:00:46,000
or what should be the criteria for enlightenment?

11
00:00:46,000 --> 00:00:52,000
And basically speaking, the best way to tell if someone is enlightened

12
00:00:52,000 --> 00:00:57,000
is that they have no greed, no anger, and no delusion.

13
00:00:57,000 --> 00:01:04,000
So you have to look and see whether they are actions in their speech,

14
00:01:04,000 --> 00:01:09,000
be lie states of greed, states of anger, states of delusion,

15
00:01:09,000 --> 00:01:12,000
states of mental, defilement or pollution.

16
00:01:12,000 --> 00:01:17,000
And more so in yourself, you have to look and ask yourself,

17
00:01:17,000 --> 00:01:20,000
do I still have greed, do I still have anger, do I still have delusion?

18
00:01:20,000 --> 00:01:23,000
Do these states have the potential still to arise?

19
00:01:23,000 --> 00:01:25,000
Even though at certain times they disappear,

20
00:01:25,000 --> 00:01:29,000
you have to ask yourself whether these things still have the potential to arise.

21
00:01:29,000 --> 00:01:36,000
Is there still ignorance? Is there still conceit and so on?

22
00:01:36,000 --> 00:01:41,000
The difference between a monk and an enlightened one is quite profound.

23
00:01:41,000 --> 00:01:45,000
A monk is a conceptual state.

24
00:01:45,000 --> 00:01:53,000
You know, you say you are a monk based on the ceremony and the social status

25
00:01:53,000 --> 00:02:01,000
or the constructed process that you have gone through.

26
00:02:01,000 --> 00:02:06,000
Becoming enlightened is totally ultimate reality.

27
00:02:06,000 --> 00:02:14,000
It is brought about by meditation practice and realization of things as they are.

28
00:02:14,000 --> 00:02:20,000
Becoming a monk is often compared to a vehicle.

29
00:02:20,000 --> 00:02:25,000
So when we become a monk, it is like getting a super powered vehicle

30
00:02:25,000 --> 00:02:37,000
or a very rugged and high powered automobile as opposed to an ordinary person

31
00:02:37,000 --> 00:02:47,000
whose vehicle might be smaller and less rugged and coalesce quickly.

32
00:02:47,000 --> 00:02:56,000
And why this is because for lay people there are many other external cares and worries

33
00:02:56,000 --> 00:02:58,000
and concerns that they have to deal with.

34
00:02:58,000 --> 00:03:05,000
So they have to get involved often with things that have no relationship with meditation practice

35
00:03:05,000 --> 00:03:07,000
whereas a monk theoretically.

36
00:03:07,000 --> 00:03:15,000
And again it is theoretical because many monks might be quite far from realizing the truth and becoming enlightened.

37
00:03:15,000 --> 00:03:22,000
But theoretically they are surrounded by meditators and meditation teachings.

38
00:03:22,000 --> 00:03:32,000
And so on a regular basis they are in touch with these things and it is quite conducive towards the development of tranquility and insight.

39
00:03:32,000 --> 00:03:37,000
Question number two, how far do you think I as a lay person would go in the process?

40
00:03:37,000 --> 00:03:39,000
Well that is totally up to you.

41
00:03:39,000 --> 00:03:41,000
I mean lay people can become enlightened.

42
00:03:41,000 --> 00:03:43,000
There is no question about that.

43
00:03:43,000 --> 00:03:47,000
In fact there are many lay people who practice better than most monks.

44
00:03:47,000 --> 00:03:53,000
But it has to be said that for most lay people it is an uphill struggle

45
00:03:53,000 --> 00:03:56,000
because there are so many other concerns and it depends on you.

46
00:03:56,000 --> 00:03:58,000
It depends on your life as a lay person.

47
00:03:58,000 --> 00:04:09,000
If you have a family and children and a job and maybe even are involved in things which are antithetical to the meditation practice,

48
00:04:09,000 --> 00:04:14,000
then it is always going to be a question of how many steps forward and how many steps back.

49
00:04:14,000 --> 00:04:22,000
If on the other hand you can keep yourself away from these things then being a lay person shouldn't be a hindrance at all for you.

50
00:04:22,000 --> 00:04:28,000
So it is all about how you organize your life and it is what you make it.

51
00:04:28,000 --> 00:04:32,000
Being a lay person is not a barrier to enlightenment.

52
00:04:32,000 --> 00:04:42,000
Number three, would I be able to visualize past lives and future lives if we succeeded in we pass in the meditation, insight meditation?

53
00:04:42,000 --> 00:04:56,000
Short answer, no. It is true that sometimes in we pass in the meditation kind of accidentally meditators will bump into such things.

54
00:04:56,000 --> 00:05:04,000
So when you are practicing we pass in the meditation because of your old character or maybe old meditation practice you have done,

55
00:05:04,000 --> 00:05:15,000
or even just practicing sort of in a round about fashion, getting caught up in states of concentration, states of tranquility.

56
00:05:15,000 --> 00:05:25,000
It is possible to fall into some memory, especially if there are strong memories or if you have been in a fatal accident in your past life or something like that.

57
00:05:25,000 --> 00:05:28,000
These things would be very strong in this psyche.

58
00:05:28,000 --> 00:05:36,000
Then these can come up and I have seen meditators who have related these sorts of things and they say they are quite sure that it is a memory.

59
00:05:36,000 --> 00:05:44,000
It is nothing like a dream or a fantasy. It is quite certain in their minds that it is a past life.

60
00:05:44,000 --> 00:05:48,000
So there are a lot of people who don't believe in this obviously you do.

61
00:05:48,000 --> 00:05:58,000
And I think there is no question that there are people who can remember past lives as for future lives.

62
00:05:58,000 --> 00:06:08,000
That is much more difficult. I do know cases of people who are able to predict the near future, like talking about the next five minutes or the next half hour.

63
00:06:08,000 --> 00:06:22,000
As far as predicting future lives, I think it is a case of predicting something that is very uncertain, a lot like predicting the weather which can be predicted for more than seven days.

64
00:06:22,000 --> 00:06:32,000
So past lives, yes, though indirectly and know not in terms of your success in the past and the meditation.

65
00:06:32,000 --> 00:06:41,000
If you are more successful, you become probably the less of an issue it becomes though again there are people who incidentally develop this awareness.

66
00:06:41,000 --> 00:06:44,000
As for future lives, probably not.

67
00:06:44,000 --> 00:06:52,000
Okay, so those are answers to your questions. Please leave any further questions as comments, private message or send to my email.

68
00:06:52,000 --> 00:07:08,000
Thanks for tuning in.

