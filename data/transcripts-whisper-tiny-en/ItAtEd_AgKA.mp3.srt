1
00:00:00,000 --> 00:00:06,600
How can one cope with having a glimpse of about three weeks of Nibana and then loosen it?

2
00:00:06,800 --> 00:00:08,800
Why does this happen?

3
00:00:10,000 --> 00:00:13,600
I got a question. What is a glimpse of Nibana?

4
00:00:14,400 --> 00:00:21,600
Yeah, I think he claims he had an enlightenment, some kind of experience of enlightenment.

5
00:00:22,000 --> 00:00:25,200
He blasted for three weeks and then faded off.

6
00:00:25,200 --> 00:00:34,200
Do you understand that? Do you understand that? You don't understand it.

7
00:00:34,400 --> 00:00:39,200
Well, I understand it to probably not be Nibana or enlightenment.

8
00:00:39,400 --> 00:00:44,400
Because remember, there's a lot of things that seem to be that and are not that.

9
00:00:46,600 --> 00:00:50,800
What was happening for the experience of Nibana can't be for three weeks,

10
00:00:50,800 --> 00:00:56,400
unless you're a Nibana. I mean, it could be, but the technical manual say,

11
00:00:56,600 --> 00:01:06,000
seven days is the maximum of a total Nibana for a non beside the spare Nibana.

12
00:01:06,200 --> 00:01:10,200
For someone who still has a body safe,

13
00:01:10,400 --> 00:01:16,200
who still has something left to be with them.

14
00:01:16,200 --> 00:01:21,400
But that seven days is there's no conduct, there's no aggregate, there's no physical,

15
00:01:21,600 --> 00:01:26,000
there's no feelings, there's no perceptions, there's no thoughts,

16
00:01:26,200 --> 00:01:34,000
and there's no consciousness except in the technical sense of a consciousness

17
00:01:34,200 --> 00:01:37,000
that takes Nibana as an object. What is that?

18
00:01:37,200 --> 00:01:42,200
I'm skeptical, but it's just a technicality.

19
00:01:42,200 --> 00:01:49,400
The three weeks after that, some of the aspects of that experience,

20
00:01:49,600 --> 00:01:52,600
if it is an experience of Nibana will wear off,

21
00:01:52,800 --> 00:01:59,000
won't wear off at the very least, or what are called the three lower feathers.

22
00:01:59,200 --> 00:02:04,000
That person has no more view of self.

23
00:02:04,200 --> 00:02:11,600
It's not possible for them to give rise to hold on to or to ascribe to a view of self.

24
00:02:11,600 --> 00:02:19,000
It's no longer possible for that person to consider as essential,

25
00:02:19,200 --> 00:02:21,000
that which is unessential.

26
00:02:21,200 --> 00:02:25,000
For instance, you can get rights or rituals, you're going to make them enlighten.

27
00:02:25,200 --> 00:02:31,200
And it's no longer possible for that person to have doubt about the Buddha that I'm in the center.

28
00:02:31,400 --> 00:02:34,400
And then, of course, it goes on and on.

29
00:02:34,600 --> 00:02:37,200
If you see Nibana again and again and again,

30
00:02:37,400 --> 00:02:40,200
or if you get to the point where you're an anigami,

31
00:02:40,200 --> 00:02:42,000
then you have no more greed and anger.

32
00:02:42,200 --> 00:02:46,400
And if you practice and become an arrow hunt,

33
00:02:46,600 --> 00:02:49,000
then there's no more greed and anger or delusion.

34
00:02:49,200 --> 00:02:52,600
And that doesn't fade away.

35
00:02:52,600 --> 00:03:17,000
And that doesn't fade away.

