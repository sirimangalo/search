Hello, Bante, which meditation method do you recommend the most? From which
Suta is this, thanks. Very easy question with a very simple answer. The practice
that I recommend the most, almost exclusively, is the practice of the four
foundations of mindfulness, which is found in three general places, I mean other
places as well, but most explicitly in the Deganikaya number 10, no, Deganikaya
no, no, there's... Magimini kai number 10, Deganikaya number, I want to say 12, but I
can't remember. It's a shameful thing that I can't even remember. No, it's not
12. Where's the Mahasati Patanasita? 16 is Paryni Bhanasuta. No, I'm terrible. Don't even
remember. 22 is that it, I can't remember. My mind is going, and here I was
bragging about having a good memory. Anyway, I remember what the Suta is all
about. Why am I getting those mixed up? No. Anyway, Magimini kai number 10, and the
third place is in the Senyutinikaya, the Satipatanasan Yuta, the group. A whole
group of discourses based on the Satipatanasuta, based on the four Satipatanas,
the four foundations of mindfulness, and these are the body, the feeling, the
feelings, the mind, and dhammas, which are... I asked my teacher about the
dhammas recently, because he was still using the English, he's one of the few
English words that he knows is the translation of the four foundations of mindfulness,
and the translation they've given him is the usual mind objects, which doesn't
at all encompass what is meant by these, by it in this context. The dhamma, as a
mind object, is only dhamma, or the objects of thought. So the sixth sense, which
is thinking. So when you think about something, the object of your thought, that's
a mind object, that's called dhamma in a minute. But the dhammas here are... my
teacher said, things that support you, things that support you, that keep you
from falling into evil, because dhamma comes from dar, which is to to hold
dhammas are those things that hold you up, keep you from falling. So these are
the things, another way to look, a way to translate it, is those things that
exist in reality. So reality is a good translation. Anyway, fourth one has... the
problem is it has many groups in it, and it looks to me very much like it's
simply talking about teachings, you know, the various aspects of the Buddha's
teaching, things that are important, dhammas in the sense of things that will
carry you through to enlightenment, teachings that will carry you through to
enlightenment. Anyway, that's the teaching, the four foundations of mindfulness.
There's an excellent book out there that everyone should read, called The Way of
Mindfulness, and it's simply a translation of the Maasati Patanasuda with
a translation of most, I think, or some at least, of the commentary to the
Sati Patanasuda, and even some of the sub-commentary. So it's an excellent
look at how the ancient teachers, you know, 2,000 years ago, 1,500 to 2,000 years
ago, were teaching and were interpreting the Sati Patanasuda. It's an excellent
book, excellent resource, it's got lots of neat stories in it and insights in it,
but even the Sutta itself is a great place to start to learn about meditation, so
very, very much worth reading. I mean, on the other hand, it may not be perfect
place to start because, of course, the Sutta's are coming from a whole
other era designed for a whole other audience in India, so it's sometimes
difficult, and then they're in a whole other language, completely different from
English. So sometimes difficult to really understand what's being said, so
but if you take what I'm passing along in tandem with the, with that study, I
think it would be indispensable, those two things together. I'm not bragging
about my own teaching, but I'm talking about the teaching that has been
passed on to me. You take that teaching and it's basically an explanation in
modern terms of the Sati Patanasuda. Take those two together. In this
tradition, that is indispensable. Of course, every tradition has its own
explanations, but in this tradition, I would say that's where I get it from, so if
you're looking for something, some sort of source, go there and really take
these two things together, the teachings that are modern and the teachings
that they come from, the Sati Patanasuda. And also read the Sutta, the Sati
Patanas and you did excellent sort of additional resource, something very
much worth reading.
